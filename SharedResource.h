#ifndef __SHAREDRESURCE_H_TEMPLATE_ID_14489465308740ABC_BREDY_
#define __SHAREDRESURCE_H_TEMPLATE_ID_14489465308740ABC_BREDY_

#include <Es/Types/pointers.hpp>

class SharedResourceInfo: public RefCount
{
  bool _closed;
public:
    SharedResourceInfo() {Reopen();}
    void CloseResource() {_closed=1;}
    bool IsClosed() const {return _closed;}
    void Reopen() {_closed=0;}
};

template<class T, class Traits>
class SharedResource: public Traits
{
  T _resource;                        ///<prom�n� rukojeti
  mutable Ref<SharedResourceInfo> _counter;   ///<ukazatel na ��ta�

    void ReleaseResource()
    {
      if (_counter.IsNull()) return;
      if (_counter->RefCounter()==1) Traits::Release(_resource);
      _counter=0;
    }
  public:
    
    ///Konstrukce t��dy z rukojeti
    /**POZOR: T��da nepozn�, �e rukoje� je n�kde sd�lena, a v�dy pro n� zalo�� nov� ��ta�.
      Pokud je pot�eba zajistit sd�len� v�ude, je t�eba p�ed�vat rukoje� zabalenou do t��dy
      SharedResource, nikoliv samostatn�.
      V p��pad�, �e je t�eba zabr�nit uzav�en� rukojeti po ukon�en� sd�l�n�, lze p�esv�d�it
      t��du SharedResource, �e rukoje� je ji� uzav�en� pomoc� SetClosed(). Nedojde k uzav�en�
      resource, ale bude ji� pova�ovan� za uzav�enou.
      @param src rukoje� kterou je t�eba sd�let. Rukoje� je kop�rov�na do _resource, a nem�la
      by p�ejit do stavu "uzav�en�" po zni�en� src
      */
    SharedResource(const T& src):_resource(src),_counter(new  SharedResourceInfo()) {}
    
    
 
    ///Konstrukce t��dy oper�torem p�i�azen�
    /** Zalo�� novou sd�lenou rukoje� pomoc� oper�toru p�i�azen�
      @param other reference na zdrojovou sd�lenou rukoje�. Funkce sn�� referenci
       sou�asn� rukojeti a pot� p�ep�e rukoje� a zalo�� j� ��ta� s referenc� 1
      */
    SharedResource<T,Traits>& operator=(const T& other)
    {
       ReleaseResource();
       _resource=other;
      _counter=new SharedResourceInfo();
      return *this;
    }
    
    ///Umoznuje zm�nit natvrdo vlastni resource, coz muze nastat v pripade, ze 
    /**doslo k presunu resource pod jinou referenci, nebo v jinych speci�ln�ch p��padech */
    void SetResource(const T& value)
    {
      _resource=value;
    }

    ///Konstrukce t��dy oper�torem p�i�azen�
    /** Duplikuje rukoje� a zv��� referen�n� ��ta� 
      @param other reference na zdrojovou sd�lenou rukoje�. Funkce sn�� referenci
       sou�asn� rukojeti a pot� duplikuje rukoje� a zv��� ji ��ta� referenc�
      */
    SharedResource<T,Traits>& operator=(const SharedResource<T,Traits>&other)
    {
      if (_counter==other._counter) return *this;
       ReleaseResource();
       _resource=other._resource;
      _counter=other._counter;
      return *this;
    }
    
    ///Oper�tor umo��uje p��m� p��stup k rukojeti resource
    operator T& () 
      {return _resource;}
    ///Oper�tor umo��uje p��m� p��stup k rukojeti resource av�ak jako konstantn� objekt
    operator const T&() const 
      {return _resource;}
    ///Oper�tor umo��uje porovnat dve rukojeti. Mus� to rukoje� podporovat!
    bool operator==(const SharedResource<T,Traits>&other) const
      {return _resource==other._resource;}
    ///Oper�tor umo��uje porovnat dve rukojeti. Mus� to rukoje� podporovat!
    bool operator!=(const SharedResource<T,Traits>&other) const
      {return _resource!=other._resource;}
    ///Oper�tor umo��uje porovnat dve rukojeti. Mus� to rukoje� podporovat!
    bool operator==(const T& other) const
      {return _resource==other;}
    ///Oper�tor umo��uje porovnat dve rukojeti. Mus� to rukoje� podporovat!
    bool operator!=(const T& other) const
      {return _resource!=other;}
    ///Oper�tor je zaveden pro rukoje� typu ukazatel, nebo pokud podporuje oper�tor ->
    T operator->() const 
      {return _resource;}
    ///Sn�� referenci rukoje�i a p��padn� ji uzav�e, pot� zlikviduje tento objekt
    ~SharedResource()
      {ReleaseResource();}
    ///Funkce uzav�r� rukoje�
    /** Po zavol�n� t�to funkce p�ejde rukoje� do stavu "uzav�en�". To se doc�l�
      zavol�n�m funkce Traits::Release. Pot� se v ��ta�i nastav� stav "uzav�en�" tak
      aby v�echny dal�� objekty, jen� rukoje� sd�l�, se dozv�d�li, �e rukoje� ji� byla uzav�ena.
      Zabr�n� se t�m tak� dal��mu v�cen�sobn� vol�n� Release.
      */
    void CloseResource() 
      {if (!IsClosed()) 
        {_counter->CloseResource(); Traits::Release(_resource); }}
    ///Funkce nastav� ��ta� tak aby informoval, �e rukoje� je ji� ve stavu "uzav�en�".
    /**Pou��v� se pokud rukoje� p�e�la do stavu "uzav�en�" jinou cestou (nap��klad chybou).
      V tomto okam�iku se u� nesm� volat Release. Funkce sesynchronizuje t��du, aby rukoje� byla
      vedena jako "uzav�en�". Tuto funkci lze tak� vyu�it v p��pad�, �e z n�jak�ho d�vodu
      si nep�ejeme po ukon�en� sd�len� aby se rukoje� uzav�ela. M� to v�ak nev�hodu, �e
      pak funkce IsClosed vrac� true a pokud s t�m ostatn� objekty nepo��taj�, mohou odm�tnout
      pracovat s uzav�enou rukojet�.*/
    void SetClosed() 
      {_counter->CloseResource();}
    ///Funkce vrac� true, pokud t��da eviduje rukoje� jako uzav�enou.
    bool IsClosed() const
      {return _counter->IsClosed();}
    ///Funkce obnov� stav "platn�" pro rukoje�. 
    /** Pou��v� se u rukojet�, jen� mohou b�t nov� otev�eny. Ov�em pozor, plat� pro
      rukojeti, kter� po otev�en� nezm�n� svou hodnotu! (nap��klad re-use socket). V�hodou
      je, �e po nov�m otev�en� ne neztrat� dosavadn� sd�len�, jak by tomu do�lo otev�en�m
      nov� rukojeti (kterou po otev�en� nikdo nesd�l�).<p>
      POZN�MKA: Funkce neotev�r� rukoje�, pouze sesynchronizuje t��du SharedResource
      */
    void Reopen() 
      {_counter->Reopen();}
};



#endif
