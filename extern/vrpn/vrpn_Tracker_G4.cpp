#include "vrpn_Tracker_G4.h"

using namespace std;

#define	CM_TO_METERS	(1/100.0)

//This function is used to determine how long it has been since the last report was sent.
static	unsigned long	duration(struct timeval t1, struct timeval t2)
{
	return (t1.tv_usec - t2.tv_usec) +
	       1000000L * (t1.tv_sec - t2.tv_sec);
}

// Constructor
vrpn_Tracker_G4::vrpn_Tracker_G4 (const char *name, vrpn_Connection *c, const char *filepath,
	            vrpn_float64 Hz, const char *rcmd) :
  vrpn_Tracker(name, c), update_rate(Hz){
		srcCalPath = LPCTSTR(filepath);
	    cmd = (char*)(rcmd); // extra commands - See SendCommand(char *scmd)
		register_server_handlers();
		if(!(Initialize())){
			cout<<"G4: Could not initialize\r\n";
			status = vrpn_TRACKER_FAIL;
		}
		else if(!(Connect())){
			cout<<"G4: Could not connect\r\n";
			isCont = false;
			status = vrpn_TRACKER_FAIL;
      return;
		}
		else if(!(SetupDevice())){
			cout<<"G4: Could not setup device\r\n";
			status = vrpn_TRACKER_FAIL;
      return;
		}
		else if(!(StartCont())){
			cout<<"G4: Failed to enter continuous mode\r\n";
			status = vrpn_TRACKER_FAIL;
      return;
		}
    else if(!pdiG4.CnxReady())
    {
      // if none of the above are done and complete then things go really bad!
      cout<<"G4: device is not ready\r\n";
      status = vrpn_TRACKER_FAIL;
    }
		else {
			cout<<"G4: Initialization Complete\r\n";
			status = vrpn_TRACKER_REPORT_READY;
		}
  }

// Deconstructor
vrpn_Tracker_G4::~vrpn_Tracker_G4(void){
	if(isCont)
		StopCont();
	Disconnect();
}


// Called by the vrpn_Generic_Sever class in order to report its status.
void	vrpn_Tracker_G4::mainloop()
{
	struct timeval current_time;

	// Call the generic server mainloop routine, since this is a server
	server_mainloop();

	// See if its time to generate a new report
	vrpn_gettimeofday(&current_time, NULL);
	if ( duration(current_time,timestamp) >= 1000000.0/update_rate) {
		DisplayCont(current_time); // Sending a report is handled in ParseG4NativeFrame
	}
}

  // NOTE: you need to be sure that if you are sending vrpn_float64 then 
//       the entire array needs to remain aligned to 8 byte boundaries
//	 (malloced data and static arrays are automatically alloced in
//	  this way).  Assumes that there is enough room to store the
//	 entire message.  Returns the number of characters sent.
int	vrpn_Tracker_G4::encode_to(char *buf)
{
	char *bufptr = buf;
	int  buflen = 1000;

	// Message includes: long sensor, long scrap, vrpn_float64 pos[3], vrpn_float64 quat[4]
	// Byte order of each needs to be reversed to match network standard

	vrpn_buffer(&bufptr, &buflen, d_sensor);
	vrpn_buffer(&bufptr, &buflen, d_sensor); // This is just to take up space to align

	vrpn_buffer(&bufptr, &buflen, pos[0]);
	vrpn_buffer(&bufptr, &buflen, pos[1]);
	vrpn_buffer(&bufptr, &buflen, pos[2]);

	vrpn_buffer(&bufptr, &buflen, quat[0]);
	vrpn_buffer(&bufptr, &buflen, quat[1]);
	vrpn_buffer(&bufptr, &buflen, quat[2]);
	vrpn_buffer(&bufptr, &buflen, quat[3]);

	return 1000 - buflen;
}

//////////////////////////////// PDI Stuff /////////////////////////////////////////////////////

// Initialize the device and some variables
BOOL vrpn_Tracker_G4::Initialize(VOID){

	hContEvent = NULL;
	hwnd = NULL;
	dwOverflowCount = 0;

	BOOL bRet = TRUE;

	pdiG4.Trace(TRUE, 7);

	bCnxReady = FALSE;
	dwStationMap = 0;

	return bRet;
}

// Connect to the G4 using the filepath provided in the config file.
BOOL vrpn_Tracker_G4::Connect(VOID)
{
  if(pdiG4.ConnectG4(srcCalPath))
  {
    cout<<"G4: Connected\r\n";
    Sleep(1000);
    return true;
  }
  else
  {
    cout<<"could not connect to G4\r\n";
    return false;
  }
}

// End the connection to the G4
VOID vrpn_Tracker_G4::Disconnect(VOID)
{
    string msg;
    if (!(pdiG4.CnxReady()))
    {
        cout << "G4: Already disconnected\r\n";
    }
    else
    {
        pdiG4.Disconnect();
        cout<< "G4: Disconnected\r\n";
    }
    
}

// Set G4 to collect correct data, eg. cm and quaternions
BOOL vrpn_Tracker_G4::SetupDevice( VOID )
{
	int i = 0;
	pdiG4.GetStationMap( dwStationMap );
	while(dwStationMap == 0 && i<30){ // Make sure that the G4 can see its hubs and sensors
		Sleep(50);// RF signals take a moment to work themselves out after connection
		pdiG4.GetStationMap( dwStationMap );
		i++;// Escape in the case that it can not find hubs 7 sensors
	}

	OriUnits = E_PDI_ORI_QUATERNION;
	pdiG4.SetPNOOriUnits( OriUnits );

	PosUnits = ePDIposUnits(2);
	pdiG4.SetPNOPosUnits( PosUnits );

	pdiG4.SetPnoBuffer( pMotionBuf, BUFFER_SIZE );
	
	pdiG4.StartPipeExport();

	if(strlen(cmd)>0){
		bool end = true;
		char *pch;
		pch = strtok (cmd,"\n");
		while (pch != NULL)
		{
			sendCommand(pch);
			//printf ("%s\n",pch);
			pch = strtok (NULL, "\n");
		}
	}

	CPDIbiterr cBE;
	pdiG4.GetBITErrs( cBE );

	CHAR szc[100];
	LPTSTR sz = LPTSTR(szc);
	cBE.Parse( sz, 100 );

	if (!(cBE.IsClear()))
	{
		pdiG4.ClearBITErrs();
	}

	UpdateStationMap();
	if(dwStationMap != 0)
		return TRUE;
	return FALSE; // G4 has connected but cannot see any hubs or sensors
}

// Updates the map of hubs and sensors
VOID vrpn_Tracker_G4::UpdateStationMap( VOID )
{
	pdiG4.GetStationMap( dwStationMap );
}

// Send additional commands to the G4
//	Originally called during setup, but can be called anytime by client software
//	since it is a public function.
void vrpn_Tracker_G4::sendCommand(char *scmd){
	int action;
	char command = scmd[0];
	float f[4];
	PDIori threeVec = {0.0,0.0,0.0};// default Euler amounts
	PDI4vec fourVec = {0.0,0.0,0.0,1.0};// default Quaternion amounts
	int numparms, hub, sensor;
	printf("G4: Received Command: %s\n",scmd);
	switch(command){
	case 'B':// Boresight
		action = scmd[1] - 48;
		numparms = sscanf(&scmd[3],"%d,%d,%f,%f,%f,%f",
			&hub,&sensor,&f[0],&f[1],&f[2],&f[3]);
		switch(numparms){
		case 2:
			if(!(action-1)){
				if(OriUnits == E_PDI_ORI_QUATERNION){
					pdiG4.SetSBoresight(hub,sensor,fourVec);
					break;
				}
				pdiG4.SetSBoresight(hub,sensor,threeVec);
				break;
			}
			pdiG4.ResetSBoresight(hub,sensor);
			break;
		case 5:
			if(!(action-1)){
				threeVec[0] = f[0]; threeVec[1] = f[1]; threeVec[2] = f[2]; 
				pdiG4.SetSBoresight(hub,sensor,threeVec);
				break;
			}
			pdiG4.ResetSBoresight(hub,sensor);
			break;
		case 6:
			if(!(action-1)){
				fourVec[0] = f[0]; fourVec[1] = f[1]; fourVec[2] = f[2]; fourVec[3] = f[3]; 
				pdiG4.SetSBoresight(hub,sensor,fourVec);
				break;
			}
			pdiG4.ResetSBoresight(hub,sensor);
			break;
		default:
			cout<<"cd\n";
			printf("Improper Boresight Command\r\n");
			break;
		}
		break;
	case 'U':// Set Position Units
		action = scmd[1] - 48;
		if(action<4){
			PosUnits = ePDIposUnits(action);
			pdiG4.SetPNOPosUnits( PosUnits );
		}
		else
			printf("Improper Set Units Command\r\n");
		break;
	case 'O':// Set Orientaion Units
		action = scmd[1] - 48;
		if(action<3){
			OriUnits = ePDIoriUnits(action);
			pdiG4.SetPNOOriUnits(OriUnits);
		}
		else
			printf("Improper Set Orientation Command\r\n");
		break;
	default:
		printf("Improper Command");
	}
	Sleep(50);
}

// Start Continuous Mode for the G4
BOOL vrpn_Tracker_G4::StartCont( VOID )
{
	cout<<"G4: Start Continuous Mode\r\n";
	BOOL bRet = FALSE;


	if (!(pdiG4.StartContPnoG4(hwnd)))
	{
	}
	else
	{
		dwOverflowCount = 0;
		bRet = TRUE;
	}

	isCont = true;
	return bRet;
}

// Stops Continuous Mode for the G4
BOOL vrpn_Tracker_G4::StopCont( VOID )
{
	cout<<"G4: Stop Continuous Mode\r\n";
	BOOL bRet = FALSE;

	if (!(pdiG4.StopContPnoG4()))
	{
	}
	else
	{
		bRet = TRUE;
		Sleep(1000);
	}

	isCont = false;
	::ResetEvent(hContEvent);
	return bRet;
}

// Displays a frame of information taken from the continuous stream of 
// Continuous Mode by calling ParseG4NativeFrame - not functioning now
BOOL vrpn_Tracker_G4::DisplayCont( timeval ct )
{
	//cout<<"DisplayCont\r\n";
	BOOL bRet = FALSE;

	PBYTE pBuf;
	DWORD dwSize;
	PBYTE pLastBuf = 0;
	DWORD dwLastSize = 0;


	if (!(pdiG4.LastPnoPtr(pBuf, dwSize)))
	{
	}
	else if ((pBuf == 0) || (dwSize == 0))
	{
	}
	else if (pLastBuf && (pBuf > pLastBuf))
	{
		ParseG4NativeFrame( pLastBuf+dwLastSize, dwSize+(pBuf-pLastBuf-dwLastSize), ct );
		pLastBuf = pBuf;
		dwLastSize = dwSize;
		bRet = TRUE;
	}
	else if (pBuf != pLastBuf) // it wrapped in buffer
	{
		if (pLastBuf)
			cout << "wrapped" << endl;

		pLastBuf = pBuf;
		dwLastSize = dwSize;
		ParseG4NativeFrame( pBuf, dwSize, ct );
		bRet = TRUE;
	}

	//cout<<"Leaving DisplayCont\r\n";
	return bRet;
}

// Displays a single frame of information from the G4 by collecting data
//	and calling ParseG4NativeFrame
BOOL vrpn_Tracker_G4::DisplaySingle( timeval ct )
{
	BOOL bRet = FALSE;
	PBYTE pBuf;
	DWORD dwSize;

	if (!(pdiG4.ReadSinglePnoBufG4(pBuf, dwSize)))
	{
		//cout<<"ReadSinglePno\r\n";
	}
	else if ((pBuf == 0) || (dwSize == 0))
	{
	}
	else 
	{
		ParseG4NativeFrame( pBuf, dwSize, ct );
		bRet = TRUE;
	}

	return bRet;

}

// Parses the data collected from a P or C command, packages it and sends it out to clients
// calling for it
void vrpn_Tracker_G4::ParseG4NativeFrame( PBYTE pBuf, DWORD dwSize, timeval current_time )
{
	DWORD dw= 0;
	char msgbuf[1000];
	vrpn_int32 len;
	LPG4_HUBDATA pHubFrame;

	while (dw < dwSize )
	{
		pHubFrame = (LPG4_HUBDATA)(&pBuf[dw]);

		dw += sizeof(G4_HUBDATA);

		UINT	nHubID = pHubFrame->nHubID;
		UINT	nFrameNum =  pHubFrame->nFrameCount;
		UINT	nSensorMap = pHubFrame->dwSensorMap;
		UINT	nDigIO = pHubFrame->dwDigIO;

		UINT	nSensMask = 1;

		for (int j=0; j<G4_MAX_SENSORS_PER_HUB; j++)
		{
			if (((nSensMask << j) & nSensorMap) != 0)
			{
				G4_SENSORDATA * pSD = &(pHubFrame->sd[j]);
				d_sensor = (nHubID*10)+j;
				// transfer the data from the G4 data array to the vrpn_Tracker array
				pos[0] = pSD->pos[0]; pos[1] = pSD->pos[1]; pos[2] = pSD->pos[2];
				if(OriUnits == ePDIoriUnits(2)){
					quat[0] = pSD->ori[0]; 
          quat[1] = pSD->ori[1]; 
          quat[2] = pSD->ori[2]; 
          quat[3] = pSD->ori[3];
				} else{
					// Since the callback is expecting a certain sized packet, we just fill out the last
					//	orientation slot with a zero. If the client plans on using Euler angles, they
					//	can disregard the zero in the final slot.
					quat[0] = pSD->ori[0]; quat[1] = pSD->ori[1]; quat[2] = pSD->ori[2]; quat[3] = 0;
				}
				
				// Grab the current time and create a timestamp
				timestamp.tv_sec = current_time.tv_sec;
				timestamp.tv_usec = current_time.tv_usec;
				// check the connection and then send a message out along it.
				if (d_connection) {
					// Pack position report
					len = encode_to(msgbuf);
					d_connection->pack_message(len, timestamp,
						position_m_id, d_sender_id, msgbuf,
						vrpn_CONNECTION_LOW_LATENCY);
				}
			}
		}

	} // end while dwsize
}