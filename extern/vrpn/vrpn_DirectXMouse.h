#ifndef VRPN_DIRECTXMOUSE_H
#define VRPN_DIRECTXMOUSE_H

#include "vrpn_Connection.h"
#include "vrpn_Analog.h"
#include "vrpn_Button.h"

class VRPN_API vrpn_DirectXMouse :
	public vrpn_Analog,
	public vrpn_Button_Filter
{
public:
    vrpn_DirectXMouse( const char* name, vrpn_Connection* cxn, bool bAbs = false);
    virtual ~vrpn_DirectXMouse();

    virtual void mainloop();

protected:  // methods
	HWND  _hWnd;	// Handle to the console window
    int	  _status;
	int no_poll_cnt;

	HRESULT InitDirectMouse( void );
    LPDIRECTINPUT8	  _DirectInput;   // Handle to Direct Input
	LPDIRECTINPUTDEVICE8 _Mouse;
	/// Try to read reports from the device.
    /// Returns 1 if msg received, or 0 if none received.
    virtual int get_report();

    /// send report iff changed
    virtual void report_changes( vrpn_uint32 class_of_service
		    = vrpn_CONNECTION_LOW_LATENCY );

    /// send report whether or not changed
    virtual void report( vrpn_uint32 class_of_service
		    = vrpn_CONNECTION_LOW_LATENCY );

protected:  // data
    struct timeval timestamp;	///< time of last report from device

private:  // disable unwanted default methods
    vrpn_DirectXMouse();
    vrpn_DirectXMouse(const vrpn_DirectXMouse&);
    const vrpn_DirectXMouse& operator=(const vrpn_DirectXMouse&);
};

#endif
