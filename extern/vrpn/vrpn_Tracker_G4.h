#ifndef VRPN_TRACKER_G4_H
#define VRPN_TRACKER_G4_H

#include "tchar.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <iostream>

#include "vrpn_Tracker.h"
#include "vrpn_Tracker_G4.h"
#include "PDI.h"

#define BUFFER_SIZE 0x1FA400   

class VRPN_API vrpn_Tracker_G4: public vrpn_Tracker {
  public:
	vrpn_Tracker_G4 (const char *name, vrpn_Connection *cn, const char *filepath,
		vrpn_float64 Hz = 10.0, const char *rcmd = NULL);
   
	~vrpn_Tracker_G4(void);
	virtual void mainloop();
	int encode_to(char *buf);	
	BOOL Initialize ( VOID );
	BOOL Connect ( VOID );
	VOID Disconnect ( VOID );
	BOOL SetupDevice ( VOID );
	VOID UpdateStationMap ( VOID );
	BOOL DisplaySingle( timeval ct );
	BOOL StartCont ( VOID );
	BOOL StopCont ( VOID );	
	BOOL DisplayCont ( timeval ct );
	void sendCommand(char * scmd);
	void ParseG4NativeFrame( PBYTE pBuf, DWORD dwSize, timeval current_time );

  protected:
	vrpn_float64 update_rate;
	vrpn_float64 pos[3], quat[4];
	int status;
	char *cmd;	// additional commands for the tracker
	bool isCont;	// Keeps track of whether or not device is in continuous mode
	CPDIg4 pdiG4;	// PDI object for the device
	CPDImdat pdiMDat;	// Map of output format
	LPCTSTR srcCalPath;	// Filepath of the Source Calibration file
	BOOL bCnxReady;	// Keeps track of wheter the connection is active
	DWORD dwStationMap;	// map of the hubs and sensors
	ePDIoriUnits OriUnits;	// Orientaion Units e.g. Euler, Quaternion
	ePDIposUnits PosUnits;	// Positions Units e.g. Inches, Meters

	HANDLE  hContEvent;
	HWND	hwnd;
	DWORD	dwOverflowCount;
	BYTE	pMotionBuf[BUFFER_SIZE];
};

#endif