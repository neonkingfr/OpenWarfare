// vrpn_Tracker_mAHRS.h
//	This file contains the class header for a Polhemus Liberty Tracker.
// This file is based on the vrpn_Tracker_Fastrak.h file, with modifications made
// to allow it to operate a Liberty instead.
//	It has been tested on Linux and relies on being able to open the USB
// port as a named port and using serial commands to communicate with it.

#ifndef VRPN_Tracker_mAHRS_H
#define VRPN_Tracker_mAHRS_H

#include <time.h>
#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#ifndef _WIN32
#include <sys/time.h>
#endif

#include "vrpn_Tracker.h"

class VRPN_API vrpn_Tracker_mAHRS: public vrpn_Tracker_Serial
{
  
public:

    /// The constructor is given the name of the tracker (the name of
    /// the sender it should use), the connection on which it is to
    /// send its messages, the name of the serial port it is to open
    /// (default is /dev/ttyS0 (first serial port in Linux)), the baud
    /// rate at which it is to communicate (default 19200), whether
    /// filtering is enabled (default yes), and the number of stations
    /// that are possible on this Liberty (default 4). The station select
    /// switches on the front of the Liberty determine which stations are
    /// active. The final parameter is a string that can contain additional
    /// commands that are set to the tracker as part of its reset routine.
    /// These might be used to set the hemisphere or other things that are
    /// not normally included; see the Liberty manual for a list of these.
    /// There can be multiple lines of them but putting <CR> into the string.

    vrpn_Tracker_mAHRS(const char *name, vrpn_Connection *c, 
		      const char *port = "/dev/ttyS0");
    ~vrpn_Tracker_mAHRS();   
protected:
  
    virtual int get_report(void);
    virtual void reset();
    vrpn_uint32	REPORT_LEN;	    //< The length that the current report should be
};

#endif
