//#include "G4_setup.h"
#include "vrpn_Generic_server_object.h"
#include "vrpn_Tracker_G4.h"

#define next() pch += strlen(pch) + 1


int vrpn_Generic_Server_Object::setup_Tracker_G4(char * &pch, char * line, FILE * config_file) {
	const int LINESIZE = 512;
	char name [LINESIZE], filepath [LINESIZE];
	vrpn_Tracker_G4 *mytracker;
	int numparms;
	int Hz = 60;
	char rcmd[5000];

	next();
	// Get the arguments (class, tracker_name)
	numparms = sscanf(pch,"%511s%d",name,&Hz);
	if(numparms==0)
		return -1;

	if(name[strlen(name)-1] == '\\' ){
		name[strlen(name)-1] = '\0';
	}

	printf("\n%s Connecting with .g4c file located at:\n",name);

	//get the filepath to the .g4c file
	if (fgets(line, LINESIZE, config_file) == NULL) {
            fprintf(stderr,"Ran past end of config file in G4 description\n");
                return -1;
    }

	filepath[0] = 0;
	strncat(filepath, line, LINESIZE);

	if(filepath[strlen(filepath)-2] == '\\' ){
		filepath[strlen(filepath)-2] = '\0';
	}else 
		filepath[strlen(filepath)-1] = '\0';

	printf("%s\n",filepath);

	// Make sure there's room for a new tracker
	if (num_trackers >= 100) {
		fprintf(stderr,"Too many trackers in config file");
	return -1;
	}

	// If the last character in the line is a backslash, '\', then
    // the following line is an additional command to send to the
    // G4 at reset time. So long as we find lines with slashes
    // at the ends, we add them to the command string to send. Note
    // that there is a newline at the end of the line, following the
    // backslash.
	rcmd[0] = 0;
    while (line[strlen(line)-2] == '\\') {
        // Read the next line
        if (fgets(line, LINESIZE, config_file) == NULL) {
            fprintf(stderr,"Ran past end of config file in G4 description\n");
                return -1;
        }

        // Copy the line into the remote command,
        // then replace \ with \0
        strncat(rcmd, line, LINESIZE);

    }

    if (strlen(rcmd) > 0) {
            printf("Additional reset commands found\n");
    }

	if ( (trackers[num_trackers] = mytracker =
	new vrpn_Tracker_G4(name, connection, filepath, Hz, rcmd)) == NULL) {
		fprintf(stderr,"Can't create new vrpn_Tracker_G4\n");
		return -1;
	}

	num_trackers = num_trackers+1;

	return 0;
}