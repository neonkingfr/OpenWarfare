#ifndef _EVENTHANDLERLIST_H_
#define _EVENTHANDLERLIST_H_


//! entity event enum defined using enum factory
#define ENTITY_EVENT_ENUM(type,prefix,XX) \
  XX(type, prefix, Killed) /*object:killer*/ \
  XX(type, prefix, Hit) /*object:causedBy,scalar:howmuch*/ \
  XX(type, prefix, Engine) /*bool:engineState*/ \
  \
  XX(type, prefix, GetIn) /*string:position,object:unit*/ \
  XX(type, prefix, GetOut) /*string:position,object:unit*/ \
  \
  XX(type, prefix, Fired) /*string:weapon,string:muzzle,string:mode,string:ammo*/ \
  XX(type, prefix, IncomingMissile) /*string:ammo,object:whoFired*/ \
  XX(type, prefix, Dammaged) /*string:name,scalar:howmuch*/ \
  XX(type, prefix, Gear) /*bool:gearState*/ \
  XX(type, prefix, Fuel) /*bool:fuelState*/ \
  \
  XX(type, prefix, AnimChanged) /*string:animation*/ \
  XX(type, prefix, AnimDone) /*string:animation*/ \
  \
  XX(type, prefix, Init) /**/ \
  \
  XX(type, prefix, Created) /**/ \
  XX(type, prefix, Delete) /*object:deletedUnit*/ \
  XX(type, prefix, Deleted) /*object:deletedUnit*/ \
  XX(type, prefix, LandedTouchDown) /*scalar:airport*/ \
  XX(type, prefix, LandedStopped) /*scalar:airport*/ \
  XX(type, prefix, HitPart) \
  XX(type, prefix, Suppressed) \
  XX(type, prefix, LoadOutChanged ) \
  XX(type, prefix, CargoChanged ) \
  XX(type, prefix, GetInMan) \
  XX(type, prefix, GetOutMan) \
  XX(type, prefix, Respawn) /* _VBS3 */ \
  XX(type, prefix, TurnOut) \
  XX(type, prefix, TurnIn) \
  XX(type, prefix, AttachTo) \
  XX(type, prefix, PositionChanged) \
  XX(type, prefix, Paste) \
  XX(type, prefix, Collision) /* Collision event for a named selection in genometry */ \
  XX(type, prefix, Revived) \
  XX(type, prefix, AfterGetIn)   /* Define _VBS3_AFTER_BEFORE_GETIN_GETOUT */ \
  XX(type, prefix, AfterGetOut)  /* Define _VBS3_AFTER_BEFORE_GETIN_GETOUT */ \
  XX(type, prefix, BeforeGetIn)  /* Define _VBS3_AFTER_BEFORE_GETIN_GETOUT */ \
  XX(type, prefix, BeforeGetOut) /* Define _VBS3_AFTER_BEFORE_GETIN_GETOUT */ \
  XX(type, prefix, AfterGetInMan)   /* Define _VBS3_AFTER_BEFORE_GETIN_GETOUT */ \
  XX(type, prefix, AfterGetOutMan)  /* Define _VBS3_AFTER_BEFORE_GETIN_GETOUT */ \
  XX(type, prefix, BeforeGetInMan)  /* Define _VBS3_AFTER_BEFORE_GETIN_GETOUT */ \
  XX(type, prefix, BeforeGetOutMan) /* Define _VBS3_AFTER_BEFORE_GETIN_GETOUT */ \
  XX(type, prefix, BeforeKilled)  /* Define _VBS3_BEFORE_KILLED */ \
  XX(type, prefix, DamagedHitPart) /* Define _VBS3_DAMAGEDHITPART_EVENTHANDLER */ \
  XX(type, prefix, LaserFired) /* Define _VBS3_TURRET_LASE_EVENT_HANDLER */ \
  \
  XX(type, prefix, Explosions)  /* _VBS3_DBOX_SCRIPTS_2 */ \

#define GROUP_EVENT_ENUM(type,prefix,XX) \
  XX(type, prefix, WaypointComplete) /*number:waypointIndex*/ \
  XX(type, prefix, GroupChangedRTE) /*string:groupName*/ \
  XX(type, prefix, GroupChanged) /*string:groupName*/ \

#define AMMO_EVENT_ENUM(type,prefix,XX) \
  XX(type, prefix, AmmoHit) /*string:weapon,string:muzzle,string:mode,string:ammo*/ \
  XX(type, prefix, AmmoExplode) /*string:weapon,string:muzzle,string:mode,string:ammo*/ \
  XX(type, prefix, AmmoDelete)

#define WEAPON_EVENT_ENUM(type,prefix,XX) \
  XX(type, prefix, ChangedWeapon) /*object:whoChanged,string:weapon,bool:added*/ \

#define MUZZLE_EVENT_ENUM(type,prefix,XX) \
  XX(type, prefix, Fired) /*string:weapon,string:muzzle,string:mode,string:ammo*/ 

#define GLOBAL_EVENT_ENUM(type,prefix,XX) \
  XX(type, prefix, CameraOnChanged)       /* CameraOn object changed: object - new CameraOn */ \
  XX(type, prefix, SideChannel)           /* Strings sent on the side channel: object - unit that sent the message, string - the message */ \

#define CONTROL_EVENT_ENUM(type,prefix,XX) \
  XX(type, prefix, MouseEnter) /* mouse pointer enter control area */ \
  XX(type, prefix, MouseExit) /* mouse pointer exit control area */ \
  XX(type, prefix, MouseButtonDown) \
  XX(type, prefix, MouseButtonUp) \
  XX(type, prefix, MouseButtonClick) \
  XX(type, prefix, MouseButtonDblClick) \
  XX(type, prefix, MouseMoving) \
  XX(type, prefix, MouseHolding) \
  XX(type, prefix, MouseZChanged) \
  \
  XX(type, prefix, KeyDown) \
  XX(type, prefix, KeyUp) \
  XX(type, prefix, Char) \
  XX(type, prefix, IMEChar) \
  XX(type, prefix, IMEComposition) \
  XX(type, prefix, JoystickButton) \
  \
  XX(type, prefix, SetFocus) \
  XX(type, prefix, KillFocus) \
  XX(type, prefix, Timer) \
  XX(type, prefix, CanDestroy) \
  XX(type, prefix, Destroy) \
  \
  XX(type, prefix, ButtonDown) /* button is pressed */ \
  XX(type, prefix, ButtonUp) /* button is released outside control area */ \
  XX(type, prefix, ButtonClick) /* button is activated */ \
  \
  XX(type, prefix, LBSelChanged) \
  XX(type, prefix, LBListSelChanged) \
  XX(type, prefix, LBDblClick) \
  XX(type, prefix, LBDrag) \
  XX(type, prefix, LBDragging) \
  XX(type, prefix, LBDrop) \
  \
  XX(type, prefix, TreeSelChanged) \
  XX(type, prefix, TreeDblClick) \
  XX(type, prefix, TreeLButtonDown) \
  XX(type, prefix, TreeMouseMove) \
  XX(type, prefix, TreeMouseHold) \
  XX(type, prefix, TreeMouseExit) \
  XX(type, prefix, TreeExpanded) \
  XX(type, prefix, TreeCollapsed) \
  \
  XX(type, prefix, ToolBoxSelChanged) \
  XX(type, prefix, CheckBoxesSelChanged) \
  XX(type, prefix, HTMLLink) \
  XX(type, prefix, SliderPosChanged) \
  XX(type, prefix, ObjectMoved) \
  XX(type, prefix, MenuSelected) \
  \
  XX(type, prefix, Draw) \
  XX(type, prefix, SetVisible)

#define EDITOR_EVENT_ENUM(type,prefix,XX) \
  XX(type, prefix, SelectObject) \
  XX(type, prefix, UnselectObject) \
  XX(type, prefix, ClearSelected) \
  XX(type, prefix, EditOverlay) \
  XX(type, prefix, CloseOverlay) \
  XX(type, prefix, RestartCamera) \
  XX(type, prefix, MouseOver) \
  XX(type, prefix, SwitchCommandMenu) \
  XX(type, prefix, ShowMap) \
  XX(type, prefix, StopMovingObject) \
  XX(type, prefix, SwitchToVehicle) \
  XX(type, prefix, RestartMission) \
  XX(type, prefix, LinkFromObject) \
  XX(type, prefix, ClearMission)

#define DISPLAY_EVENT_ENUM(type,prefix,XX) \
  XX(type, prefix, Load) \
  XX(type, prefix, Unload) \
  XX(type, prefix, ChildDestroyed) \
  \
  XX(type, prefix, KeyDown) \
  XX(type, prefix, KeyUp) \
  XX(type, prefix, Char)


#define MISC_EVENT_ENUM(type,prefix,XX) \
  XX(type, prefix, OnCheat) \
  XX(type, prefix, Waypoint_ExpCond) \
  XX(type, prefix, Waypoint_ExpActiv) \
  XX(type, prefix, Detector_ExpCond) \
  XX(type, prefix, Detector_Activate) \
  XX(type, prefix, InteractWithVehicle) \
  XX(type, prefix, QuickEnterVehicle) \
  XX(type, prefix, UserActionStatement) \
  XX(type, prefix, UserActionCondition) \
  XX(type, prefix, UIActionTypeScript) \
  XX(type, prefix, PublicExecEventCond) \
  XX(type, prefix, PublicExecEventCmd) \
  XX(type, prefix, PublicVariableChange) \
  XX(type, prefix, MenuItemEventHandler)

// Possible event groups 16       (high WORD)
// Possible events per group 2^16  (low WORD)
//
// For example when event Fired happens, the id becomes
// MUZZLE_EVENT_MASK | Fired
#define ENTITY_EVENT_MASK  (0<<16)
#define GROUP_EVENT_MASK   (1<<16)
#define AMMO_EVENT_MASK    (2<<16)
#define WEAPON_EVENT_MASK  (3<<16)
#define MUZZLE_EVENT_MASK  (4<<16)
#define GLOBAL_EVENT_MASK  (5<<16)
#define CONTROL_EVENT_MASK (6<<16)
#define EDITOR_EVENT_MASK  (7<<16)
#define DISPLAY_EVENT_MASK (8<<16)
#define MISC_EVENT_MASK    (9<<16)
#define SCRIPT_GENETERIC   (-1)


/* Fusion:

  The above events are routed to the corresponding functions of FusionHandler.dll, such as

     bool OnEntityKilled(const GameValue& pars); // entity events start with "OnEntity"
     bool OnEntityHit(const GameValue& pars);
     ...
     bool OnEntityCreated(const GameValue& pars);
     bool OnEntityDeleted(const GameValue& pars);
     ...
     bool OnEntityBeforeKilled(const GameValue& pars);

     bool OnWaypointComplete(const GameValue& pars); // (unfortunately the word "Group" is missing here)
     bool OnGroupChangedRTE(const GameValue& pars);
     bool OnGroupChanged(const GameValue& pars);

     bool OnAmmoHit(const GameValue& pars);
     bool OnAmmoExplode(const GameValue& pars);
     bool OnAmmoDelete(const GameValue& pars);

     bool OnChangedWeapon(const GameValue& pars);

     bool OnMuzzleFired(const GameValue& pars);

  As an optimization, each of these is called first with zero parameters in "pars",
  to check whether FusionHandler.dll is listening for the event at all. If that is the case,
  the function must return true (quickly) and VBS goes on to construct the array "pars".
  After that, the function is called again with the actual content of "pars" (this time
  the return value is ignored). This process repeats with every event because it is assumed
  that handlers can be freely added and removed on the side of FusionHandler.dll.
  Please note that this is just an optimizaiton and VBS may choose to call the handlers
  directly (without the "empty" call) in some cases.

  The parameters in "pars" are the same as for the scripting event handlers, see
  http://community.bistudio.com/wiki/VBS2:_Event_Handlers.
  The first item of the array is always the object to which the event pertains.
  More items may follow which are specific to each event.

*/


#endif
