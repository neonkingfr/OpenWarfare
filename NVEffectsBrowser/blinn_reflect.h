/******************************************************************************

  Copyright (C) 1999, 2000 NVIDIA Corporation
  This file is provided without support, instruction, or implied warranty of any
  kind.  NVIDIA makes no guarantee of its fitness for a particular purpose and is
  not liable under any circumstances for any damages or loss whatsoever arising
  from the use or inability to use this file or items derived from it.
  
    Comments:
    
      
        
******************************************************************************/

#define CV_ZERO 0
#define CV_ONE 1

#define CV_WORLDVIEWPROJ_0 2
#define CV_WORLDVIEWPROJ_1 3
#define CV_WORLDVIEWPROJ_2 4
#define CV_WORLDVIEWPROJ_3 5

#define CV_VOLTEXTGEN_0 8
#define CV_VOLTEXTGEN_1 9
#define CV_VOLTEXTGEN_2 10
#define CV_VOLTEXTGEN_3 11


