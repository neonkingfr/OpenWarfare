/******************************************************************************

  Copyright (C) 1999, 2000 NVIDIA Corporation
  This file is provided without support, instruction, or implied warranty of any
  kind.  NVIDIA makes no guarantee of its fitness for a particular purpose and is
  not liable under any circumstances for any damages or loss whatsoever arising
  from the use or inability to use this file or items derived from it.
  
    Comments:
	Essential utility stuff for the dot3 shader examples
	Dot3Vertex - a 'fat' vertex with the S,T and SxT components in it
	    
      
        
******************************************************************************/
#ifndef __SHADER_DOT3_UTIL_H
#define __SHADER_DOT3_UTIL_H



// A special vertex with basis vectors included
class BasVertex
{
public:
	BasVertex(const D3DXVECTOR3& _Position, const D3DXVECTOR3& _Normal, const D3DXVECTOR2& _Texture)
		: Position(_Position), 
			Normal(_Normal),
			Texture(_Texture)
	{};

	D3DXVECTOR3 Position;
	D3DXVECTOR3 Normal;
	D3DXVECTOR3 Diffuse;
	D3DXVECTOR2 Texture;
};


#endif //__SHADER_DOT3_UTIL_H

