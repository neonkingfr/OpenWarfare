/******************************************************************************

  Copyright (C) 1999, 2000 NVIDIA Corporation
  This file is provided without support, instruction, or implied warranty of any
  kind.  NVIDIA makes no guarantee of its fitness for a particular purpose and is
  not liable under any circumstances for any damages or loss whatsoever arising
  from the use or inability to use this file or items derived from it.
  
    Comments:
    
      
        
******************************************************************************/

#include "eb_effect.h"
#include "nvdevice.h"
#include "shader_dot3_util.h"
#include "shader_BlinnReflect.h"
#include "blinn_reflect.h"
#include "nvtexture.h"

#include <vector>
#include <stdlib.h>

using namespace nv_objects;
using namespace std;

DECLARE_EFFECT_MAIN()

extern "C"
{

__declspec(dllexport) unsigned int GetNumEffects() { return 1; }

__declspec(dllexport) EBEffect* CreateEffect(unsigned int EffectNum)
{
	return new CShaderSphereVolume();
}

}


HRESULT CShaderSphereVolume::ConfirmDevice(D3DCAPS8* pCaps, DWORD dwBehavior, D3DFORMAT Format)
{
	if (!(pCaps->TextureCaps & D3DPTEXTURECAPS_CUBEMAP))
	{
		m_strLastError = "Device does not support cubemaps!";
		return E_FAIL;
	}

	if (!(pCaps->TextureCaps & D3DPTEXTURECAPS_PROJECTED))
	{
		m_strLastError = "Device does not support 3 element texture coordinates!";
		return E_FAIL;
	}

	if (!(pCaps->MaxTextureBlendStages >= 4))
	{
		m_strLastError = "Not enough texture blend stages!";
		return E_FAIL;
	}

	if(D3DSHADER_VERSION_MAJOR(pCaps->PixelShaderVersion) < 1)
	{
		m_strLastError = "Device does not support pixel shaders!";
		return E_FAIL;
	}

    if(!(pCaps->DevCaps & D3DDEVCAPS_RTPATCHES) )
    {
        m_strLastError = "Device does not support RTPATCHES!";
        return E_FAIL;
    }

	return S_OK;
}

void CShaderSphereVolume::UpdateProperties()
{
	EBEffect::UpdateProperties();

	AddProperty(new EBProperty("Wireframe", OBJECT_MEMBER(m_bWireframe), EBTYPE_BOOL_PROP));
	AddProperty(new EBProperty("Pause", OBJECT_MEMBER(m_bPause), EBTYPE_BOOL_PROP));
	AddProperty(new EBProperty("Diamond", OBJECT_MEMBER(m_bDiamond), EBTYPE_BOOL_PROP));

	// Vertex shaders
	m_pVertexShaderEnum->AddEnumerant(new EBEnumValue(m_pVertexShaderEnum, "Texture Space", GetFilePath("blinn_reflect.nvv"), EBTYPE_STRING_PROP));

	// Pixel shaders
	m_pPixelShaderEnum->AddEnumerant(new EBEnumValue(m_pPixelShaderEnum, "Per-pixel Bump Reflections", GetFilePath("blinn_reflect.nvp"), EBTYPE_STRING_PROP));

	SetAboutInfo( NULL, _T("Spherical volume"));
	SetAboutInfo( NULL, _T("(C) 2001, Ondrej Spanel") );
	SetAboutInfo( NULL, _T("Based on NVIDIA BlinnBump Effect"));
	SetAboutInfo( _T("Date"), _T(__DATE__));
}

CShaderSphereVolume::CShaderSphereVolume()
:	m_pPatchBuffer(NULL),
	m_pVertexBuffer(NULL),
    m_pIndexBuffer(NULL),
	m_fAngle(0.0f),
	m_pNVDevice(NULL),
	m_pGlossMap(NULL),
	m_dwBlinnPixelShader(0),
	m_dwBlinnVertexShader(0),
	m_pCubeTexture(NULL),
	m_pSphereTexture(NULL),
	m_pDiamondTexture(NULL),
	m_pUI(NULL),
	m_bWireframe(false),
	m_bDiamond(false),
	m_bPause(false)
{
	m_strEffectName = "Spherical Volume";	// A string holding the name of the effect
	m_strEffectLocation = "Effects\\Volume";
	m_strEffectPixelShader = GetFilePath("blinn_reflect.nvp");
	m_strEffectVertexShader = GetFilePath("blinn_reflect.nvv");
	#if _DEBUG
	m_strEffectVersion  = "Debug 1.0";
	#else
	m_strEffectVersion  = "1.0";
	#endif
}

CShaderSphereVolume::~CShaderSphereVolume()
{
	Free();	
}

#define TEX_SCALE 4.0f

HRESULT CShaderSphereVolume::GenerateSphere(D3DXVECTOR3& vCenter, FLOAT fRadius, WORD wNumRings, WORD wNumSections, FLOAT sx, FLOAT sy, FLOAT sz)
{
    FLOAT x, y, z, v, rsintheta; // Temporary variables
    WORD  i, j, n, m;            // counters
    D3DXVECTOR3 vPoint;
	HRESULT hr;

	SAFE_RELEASE(m_pVertexBuffer);
    SAFE_RELEASE(m_pIndexBuffer);

    //Generate space for the required triangles and vertices.
    WORD       wNumTriangles = (wNumRings + 1) * wNumSections * 2;
    DWORD      dwNumIndices   = wNumTriangles*3;
    DWORD      dwNumVertices  = (wNumRings + 1) * wNumSections + 2;

	m_dwNumVertices = dwNumVertices;
	m_dwNumIndices = wNumTriangles * 3;
	m_dwNumFaces = wNumTriangles;	

	hr = m_pD3DDev->CreateVertexBuffer(dwNumVertices * sizeof(BasVertex), 0, 0, D3DPOOL_MANAGED, &m_pVertexBuffer);
	if (FAILED(hr))
	{
		return hr;
	}

	hr = m_pD3DDev->CreateIndexBuffer(3 * wNumTriangles * sizeof(WORD), 0, D3DFMT_INDEX16, D3DPOOL_MANAGED, &m_pIndexBuffer);
	if (FAILED(hr))
	{
		return hr;
	}

	BasVertex* pVertices;
	WORD* pIndices;
	
	m_pVertexBuffer->Lock(0, 0, (BYTE**)&pVertices, 0);
	m_pIndexBuffer->Lock(0, 0, (BYTE**)&pIndices, 0);

    // Generate vertices at the top and bottom points.
    D3DXVECTOR3 vTopPoint  = vCenter + D3DXVECTOR3( 0.0f, +sy*fRadius, 0.0f);
    D3DXVECTOR3 vBotPoint  = vCenter + D3DXVECTOR3( 0.0f, -sy*fRadius, 0.0f);
    D3DXVECTOR3 vNormal = D3DXVECTOR3( 0.0f, 1.0f, 0.0f );

    pVertices[0]               = BasVertex
		(
			D3DXVECTOR3(vTopPoint.x, vTopPoint.y, vTopPoint.z),  D3DXVECTOR3(vNormal.x, vNormal.y, vNormal.z),
			D3DXVECTOR2(0.0f, 0.0f)
		);
    pVertices[dwNumVertices-1] = BasVertex
		(
			D3DXVECTOR3(vBotPoint.x, vBotPoint.y, vBotPoint.z), D3DXVECTOR3(-vNormal.x, -vNormal.y, -vNormal.z),
			D3DXVECTOR2(0.0f, 0.0f)
		);

    // Generate vertex points for rings
    FLOAT dtheta = (float)(D3DX_PI / (wNumRings + 2));     //Angle between each ring
    FLOAT dphi   = (float)(2*D3DX_PI / (wNumSections)); //Angle between each section
    FLOAT theta  = dtheta;
    n = 1; //vertex being generated, begins at 1 to skip top point

    for( i = 0; i < (wNumRings+1); i++ )
    {
        y = fRadius * (float)cos(theta); // y is the same for each ring
        v = theta / D3DX_PI;     // v is the same for each ring
        rsintheta = fRadius * (float)sin(theta);
        FLOAT phi = 0.0f;

        for( j = 0; j < wNumSections; j++ )
        {
            x = rsintheta * (float)sin(phi);
            z = rsintheta * (float)cos(phi);
        
            FLOAT u = 1.0f - (FLOAT)(phi / (2 * D3DX_PI) );
            assert(u <= 1.001f);
						assert(u >= 0.0f);
            vPoint        = vCenter + D3DXVECTOR3( sx*x, sy*y, sz*z );
            vNormal       = D3DXVECTOR3( x/fRadius, y/fRadius, z/fRadius );
						D3DXVec3Normalize(&vNormal, &vNormal);
						pVertices[n] = BasVertex
						(
							D3DXVECTOR3(vPoint.x, vPoint.y, vPoint.z), D3DXVECTOR3(vNormal.x, vNormal.y, vNormal.z),
							D3DXVECTOR2(u * TEX_SCALE, v * TEX_SCALE)
						);

            phi += dphi;
            ++n;
        }
        theta += dtheta;
    }

    // Generate triangles for top and bottom caps.
    for( i = 0; i < wNumSections; i++ )
    {
        pIndices[3*i+0] = 0;
        pIndices[3*i+1] = i + 1;
        pIndices[3*i+2] = 1 + ((i + 1) % wNumSections);

        pIndices[3*(wNumTriangles - wNumSections + i)+0] = (WORD)( dwNumVertices - 1 );
        pIndices[3*(wNumTriangles - wNumSections + i)+1] = (WORD)( dwNumVertices - 2 - i );
        pIndices[3*(wNumTriangles - wNumSections + i)+2] = (WORD)( dwNumVertices - 2 - 
                ((1 + i) % wNumSections) );
    }

    // Generate triangles for the rings
    m = 1;            // first vertex in current ring,begins at 1 to skip top point
    n = wNumSections; // triangle being generated, skip the top cap 
        
    for( i = 0; i < wNumRings; i++ )
    {
        for( j = 0; j < (wNumSections); j++ )
        {
            pIndices[3*n+0] = m + j;
            pIndices[3*n+1] = m + wNumSections + j;
            pIndices[3*n+2] = m + wNumSections + ((j + 1) % wNumSections);
            
            pIndices[3*(n+1)+0] = pIndices[3*n+0];
            pIndices[3*(n+1)+1] = pIndices[3*n+2];
            pIndices[3*(n+1)+2] = m + ((j + 1) % wNumSections);
            
            n += 2;
        }
      
        m += wNumSections;
    }

	m_pVertexBuffer->Unlock();
	m_pIndexBuffer->Unlock();

    return S_OK;
}

HRESULT CShaderSphereVolume::GenerateQuad(D3DXVECTOR3& vCenter, FLOAT fRadius)
{
	SAFE_RELEASE(m_pVertexBuffer);
	SAFE_RELEASE(m_pIndexBuffer);

	m_dwNumVertices = 4;
	m_dwNumIndices = 6;
	m_dwNumFaces = m_dwNumIndices / 3;	

	m_pD3DDev->CreateVertexBuffer(m_dwNumVertices * sizeof(BasVertex), 0, 0, D3DPOOL_MANAGED, &m_pVertexBuffer);
	m_pD3DDev->CreateIndexBuffer(m_dwNumIndices * sizeof(WORD), 0, D3DFMT_INDEX16, D3DPOOL_MANAGED, &m_pIndexBuffer);

	BasVertex* pVertices;
	WORD* pIndices;
	
	m_pVertexBuffer->Lock(0, 0, (BYTE**)&pVertices, 0);
	m_pIndexBuffer->Lock(0, 0, (BYTE**)&pIndices, 0);

	pVertices[0] = BasVertex(D3DXVECTOR3(vCenter.x - fRadius, vCenter.y + fRadius, 0.0f), D3DXVECTOR3(0.0f, 0.0f, -1.0f), D3DXVECTOR2(0.0f, 0.0f));
	pVertices[1] = BasVertex(D3DXVECTOR3(vCenter.x + fRadius, vCenter.y + fRadius, 0.0f), D3DXVECTOR3(0.0f, 0.0f, -1.0f), D3DXVECTOR2(1.0f * TEX_SCALE, 0.0f));
	pVertices[2] = BasVertex(D3DXVECTOR3(vCenter.x - fRadius, vCenter.y - fRadius, 0.0f), D3DXVECTOR3(0.0f, 0.0f, -1.0f), D3DXVECTOR2(0.0f, 1.0f * TEX_SCALE));
	pVertices[3] = BasVertex(D3DXVECTOR3(vCenter.x + fRadius, vCenter.y - fRadius, 0.0f), D3DXVECTOR3(0.0f, 0.0f, -1.0f), D3DXVECTOR2(1.0f * TEX_SCALE, 1.0f * TEX_SCALE));

	pIndices[0] = 0;
	pIndices[1] = 1;
	pIndices[2] = 2;
	pIndices[3] = 2;
	pIndices[4] = 1;
	pIndices[5] = 3;

	m_pVertexBuffer->Unlock();
	m_pIndexBuffer->Unlock();

    return S_OK;
}



HRESULT CShaderSphereVolume::Initialize(IDirect3DDevice8* pDev)
{
	HRESULT hr;
	vector<DWORD> Declaration;

	m_pD3DDev = pDev;
	pDev->AddRef();

	m_pNVDevice = new NVBlinnReflectDevice(pDev, this);

	//initialize mouse UI
	RECT rect;
	rect.left = rect.top = 0;
	D3DVIEWPORT8 viewport;
	m_pD3DDev->GetViewport(&viewport);
	rect.bottom = viewport.Height;
	rect.right  = viewport.Width;
	m_pUI = new MouseUI((const RECT)rect);
	m_pUI->SetTranslationalSensitivityFactor(0.1f);

	D3DDEVICE_CREATION_PARAMETERS CreationParams;
	pDev->GetCreationParameters(&CreationParams);


    hr = GenerateSphere(D3DXVECTOR3(0.0f, 0.0f, 0.0f), 0.5f, 30, 40, 1.0f, 1.0f, 1.0f);
    //hr = GenerateQuad(D3DXVECTOR3(0.0f, 0.0f, 0.0f), 0.5f);
	if (FAILED(hr))
	{
		m_strLastError = "Failed to generate sphere";
		return hr;
	}

     //GenerateQuad(D3DXVECTOR3(0.0f, 0.0f, 0.0f), 1.0f);

	Declaration.clear();
  Declaration.push_back(D3DVSD_STREAM(0));
	Declaration.push_back(D3DVSD_REG(0, D3DVSDT_FLOAT3)); // Position
	Declaration.push_back(D3DVSD_REG(1, D3DVSDT_FLOAT3)); // Normal
	Declaration.push_back(D3DVSD_REG(2, D3DVSDT_FLOAT3)); // Diffuse
	Declaration.push_back(D3DVSD_REG(3, D3DVSDT_FLOAT2)); // Texture
	//Declaration.push_back(D3DVSD_REG(5, D3DVSDT_FLOAT3)); // S
	//Declaration.push_back(D3DVSD_REG(6, D3DVSDT_FLOAT3)); // T
	//Declaration.push_back(D3DVSD_REG(4, D3DVSDT_FLOAT3)); // SxT
  Declaration.push_back(D3DVSD_END());


	// Create the shaders, 2 versions - one for the sphere, one for the patch

	hr = LoadAndCreateShader(GetFilePath("blinn_reflect.pso").c_str(), NULL, 0, SHADERTYPE_PIXEL, &m_dwBlinnPixelShader);
		if (FAILED(hr))
			return hr;
		
	hr = LoadAndCreateShader(GetFilePath("blinn_reflect.vso"), &Declaration[0], 0, SHADERTYPE_VERTEX, &m_dwBlinnVertexShader);
	if (FAILED(hr))
		return hr;


	hr = D3DXCreateTextureFromFileEx(m_pD3DDev, 
		GetFilePath("gloss_map_nvidia.dds").c_str(),
		D3DX_DEFAULT,
		D3DX_DEFAULT,
		0,
		0,
		D3DFMT_UNKNOWN,
		D3DPOOL_MANAGED,
		D3DX_FILTER_LINEAR,
		D3DX_FILTER_LINEAR,
		0,
		NULL,
		NULL,
		&m_pGlossMap);
	if (FAILED(hr))
	{
		m_strLastError = "Could not load texture (gloss_map_nvidia.dds)";
		return hr;
	}

	hr = D3DXCreateCubeTextureFromFileExA( pDev,
		GetFilePath("sky_cube_mipmap.dds").c_str(), 
		D3DX_DEFAULT,
		0,
		0,
		D3DFMT_UNKNOWN,
		D3DPOOL_MANAGED,
		D3DX_FILTER_LINEAR,
		D3DX_FILTER_LINEAR,
		0,
		NULL,
		NULL,
		&m_pCubeTexture);
	if (FAILED(hr))
	{
		m_strLastError = "Could not load texture (sky_cube_mipmap.dds)";
		return hr;
	}

	hr = D3DXCreateTextureFromFileEx(m_pD3DDev, 
		GetFilePath("diamond.dds").c_str(),
		D3DX_DEFAULT,
		D3DX_DEFAULT,
		0,
		0,
		D3DFMT_UNKNOWN,
		D3DPOOL_MANAGED,
		D3DX_FILTER_LINEAR,
		D3DX_FILTER_LINEAR,
		0,
		NULL,
		NULL,
		&m_pDiamondTexture);
	if (FAILED(hr))
	{
		m_strLastError = "Could not load texture (diamond.dds)";
		return hr;
	}
	hr = D3DXCreateTextureFromFileEx(m_pD3DDev, 
		GetFilePath("sphere.dds").c_str(),
		D3DX_DEFAULT,
		D3DX_DEFAULT,
		0,
		0,
		D3DFMT_UNKNOWN,
		D3DPOOL_MANAGED,
		D3DX_FILTER_LINEAR,
		D3DX_FILTER_LINEAR,
		0,
		NULL,
		NULL,
		&m_pSphereTexture);
	if (FAILED(hr))
	{
		m_strLastError = "Could not load texture (sphere.dds)";
		return hr;
	}

	// Setup constants
	m_pD3DDev->SetVertexShaderConstant(CV_ZERO,   D3DXVECTOR4(0.0f, 0.0f, 0.0f, 0.0f), 1);
	m_pD3DDev->SetVertexShaderConstant(CV_ONE,    D3DXVECTOR4(1.0f, 1.0f, 1.0f, 1.0f), 1);

	// Camera stuff

	D3DXVECTOR3 vEyePt    = D3DXVECTOR3( 0.0f, 0.0f, -1.0f );
	D3DXVECTOR3 vLookatPt = D3DXVECTOR3( 0.0f, 0.0f,  0.0f );
	D3DXVECTOR3 vUp       = D3DXVECTOR3( 0.0f, 1.0f,  0.0f );

	// View
	D3DXMATRIX matView;
	D3DXMatrixLookAtLH(&matView, &vEyePt, &vLookatPt, &vUp);

	// Projection
	D3DXMATRIX matProj;
	D3DXMatrixPerspectiveFovLH(&matProj, D3DXToRadian(60.0f), 1.0f, 0.1f, 50.0f);

	m_pNVDevice->SetViewTransform(&matView);
	m_pNVDevice->SetProjectionTransform(&matProj);

	m_pD3DDev->SetRenderState(D3DRS_SPECULARENABLE, FALSE);
	
	// Point at the index data
	hr = m_pD3DDev->SetIndices(m_pIndexBuffer, 0);
	if (FAILED(hr))
	{
		m_strLastError = "Could not set Index source";
		return hr;
	}

	return S_OK;

}

HRESULT CShaderSphereVolume::Free()
{
    SAFE_RELEASE(m_pPatchBuffer);
    SAFE_RELEASE(m_pVertexBuffer);
    SAFE_RELEASE(m_pIndexBuffer);
	SAFE_RELEASE(m_pGlossMap);
	SAFE_RELEASE(m_pCubeTexture);
	SAFE_RELEASE(m_pSphereTexture);
	SAFE_RELEASE(m_pDiamondTexture);
	
	SAFE_DELETE(m_pUI);

	SAFE_DELETE(m_pNVDevice);

	if (m_pD3DDev)
	{
		if (m_dwBlinnPixelShader)
			m_pD3DDev->DeletePixelShader(m_dwBlinnPixelShader);
		if (m_dwBlinnVertexShader)
			m_pD3DDev->DeleteVertexShader(m_dwBlinnVertexShader);

		m_dwBlinnPixelShader = 0;
		m_dwBlinnVertexShader = 0;

		SAFE_RELEASE(m_pD3DDev);
	}
	
	return S_OK;
}

HRESULT CShaderSphereVolume::Start()
{
	m_fAngle = 0.0f;
	return S_OK;
}

  
HRESULT CShaderSphereVolume::SetTransform()
{

	const D3DXMATRIX &matWorld = m_pNVDevice->GetWorldTransform();
	const D3DXMATRIX &matProj = m_pNVDevice->GetProjectionTransform();
	const D3DXMATRIX &matView = m_pNVDevice->GetViewTransform();

	D3DXMATRIX matWorldView = matWorld * matView;
	D3DXMATRIX matWorldViewProj = matWorldView * matProj;
		
	// Projection to clip space
	D3DXMatrixTranspose(&matWorldViewProj, &matWorldViewProj);
	m_pD3DDev->SetVertexShaderConstant(CV_WORLDVIEWPROJ_0, &matWorldViewProj(0, 0), 4);
	D3DXMatrixTranspose(&matWorldViewProj, &matWorldViewProj);


	/*
	// setup texture coordinate generation matrix
	D3DXVECTOR3 vEyePt    = D3DXVECTOR3( 0.0f, 0.0f, -1.0f );
	D3DXVECTOR3 vLookatPt = D3DXVECTOR3( 0.0f, 0.0f,  0.0f );
	D3DXVECTOR3 vUp       = D3DXVECTOR3( 0.0f, 1.0f,  0.0f );

	// View
	D3DXMATRIX matViewAtInstance;
	D3DXMatrixLookAtLH(&matViewAtInstance, &vEyePt, &vLookatPt, &vUp);
	D3DXMATRIX texGen = matWorld * matViewAtInstance;
	*/

	D3DXMATRIX texGen = matWorld * matView;
	texGen._41 = 0;
	texGen._42 = 0;
	texGen._43 = 0;
	texGen._44 = 1;
	D3DXMATRIX offset;
	D3DXMATRIX scale;
	D3DXMatrixTranslation(&offset,0.5,0.5,0);
	//D3DXMatrixScaling(&scale,1,1,1);
	texGen = texGen * offset;
	D3DXMatrixTranspose(&texGen, &texGen);
	m_pD3DDev->SetVertexShaderConstant(CV_VOLTEXTGEN_0, &texGen(0, 0), 4);
	D3DXMatrixTranspose(&texGen, &texGen);

	return S_OK;

}

struct SortInstance
{
	const D3DXVECTOR3 *vec;
	float z;
};

static int CmpSortInstance(const void *s1, const void *s2)
{
	const SortInstance *si1 = (const SortInstance *)s1;
	const SortInstance *si2 = (const SortInstance *)s2;
	if (si1->z<si2->z) return +1;
	if (si1->z>si2->z) return -1;
	return 0;
}

HRESULT CShaderSphereVolume::Tick(EBTimer* pTimer)
{
	HRESULT hr = S_OK;

	// Clear to grey
	hr = m_pD3DDev->Clear(0, NULL, D3DCLEAR_TARGET|D3DCLEAR_ZBUFFER, D3DCOLOR_XRGB( 0xAA, 0x55, 0x55 ), 1.0, 0);

	m_pD3DDev->SetRenderState(D3DRS_FILLMODE, m_bWireframe ? D3DFILL_WIREFRAME : D3DFILL_SOLID);

	// Set the bump scale
	D3DXMATRIX matWorld = m_pUI->GetRotationMatrix() * m_pUI->GetTranslationMatrix();

	const D3DXMATRIX &matView = m_pNVDevice->GetViewTransform();

	D3DXMATRIX matWorldView = matWorld * matView;
			
	// Load transform data.
	// draw several instances
	// sort instances by distance

	static const D3DXVECTOR3 instances[]=
	{
		D3DXVECTOR3(+5,+0,+7),
		D3DXVECTOR3(-2,+0,+5),
		D3DXVECTOR3(+2,+0,+5),
		D3DXVECTOR3(-2,+0,+2),
		D3DXVECTOR3(0,0,0),
		D3DXVECTOR3(+5,+0,-1),
		D3DXVECTOR3(-2,+0,+2),
		D3DXVECTOR3(+2,+0,-2),
		D3DXVECTOR3(-2,+0,-3),
	};
	const int nInstances = sizeof(instances)/sizeof(*instances);
	SortInstance sort[nInstances];
	int i;
	for (i=0; i<nInstances; i++)
	{
		sort[i].vec = &instances[i];
		// aply world + view transformation

		D3DXVECTOR4 viewInst;
		D3DXVec3Transform(&viewInst,&instances[i],&matWorldView);

		sort[i].z = viewInst.z;
	}
	qsort(sort,nInstances,sizeof(*sort),CmpSortInstance);

	for (i=0; i<nInstances; i++)
	{
		D3DXMATRIX inst, instW;
		D3DXMatrixTranslation(&inst,sort[i].vec->x,sort[i].vec->y,sort[i].vec->z);
		instW = inst * matWorld;

		m_pNVDevice->SetWorldTransform(&instW);
		SetTransform();
		DrawReflection();
	}

	return hr;
}
	
HRESULT CShaderSphereVolume::DrawReflection()
{
	D3DXMATRIX matWorld;
	D3DXMATRIX matTemp;

	m_pD3DDev->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
	m_pD3DDev->SetRenderState(D3DRS_SRCBLEND , D3DBLEND_SRCALPHA);
	m_pD3DDev->SetRenderState(D3DRS_DESTBLEND , D3DBLEND_INVSRCALPHA);

	// General setup
	m_pD3DDev->SetTextureStageState(0, D3DTSS_ADDRESSU, D3DTADDRESS_CLAMP);
	m_pD3DDev->SetTextureStageState(0, D3DTSS_ADDRESSV, D3DTADDRESS_CLAMP);
	m_pD3DDev->SetTextureStageState(1, D3DTSS_ADDRESSU, D3DTADDRESS_WRAP);
	m_pD3DDev->SetTextureStageState(1, D3DTSS_ADDRESSV, D3DTADDRESS_WRAP);

	m_pD3DDev->SetRenderState( D3DRS_WRAP0, D3DWRAP_U | D3DWRAP_V);
	m_pD3DDev->SetRenderState( D3DRS_WRAP1, D3DWRAP_U | D3DWRAP_V);

	// Point at the vertex data
  m_pD3DDev->SetStreamSource(0, m_pVertexBuffer, sizeof(BasVertex));

	// Backface cull the sphere
	m_pD3DDev->SetRenderState(D3DRS_CULLMODE, D3DCULL_CCW);

	// Point at the index data
	m_pD3DDev->SetIndices(m_pIndexBuffer, 0);

	if (m_bDiamond) m_pD3DDev->SetTexture(0, m_pDiamondTexture);
	else m_pD3DDev->SetTexture(0, m_pSphereTexture);
	m_pD3DDev->SetTexture(1, m_pGlossMap);
	m_pD3DDev->SetTexture(2, NULL);
	m_pD3DDev->SetTexture(3, NULL);
	
	// Setup the vertex shader
  m_pD3DDev->SetVertexShader(m_dwBlinnVertexShader);
	m_pD3DDev->SetPixelShader(m_dwBlinnPixelShader);


  // Draw 
  m_pD3DDev->DrawIndexedPrimitive(D3DPT_TRIANGLELIST, 0, m_dwNumVertices, 0, m_dwNumFaces);

	return S_OK;
}

void CShaderSphereVolume::MouseButton(HWND hWnd, eButtonID button, bool bDown, int x, int y)
{
	if(button == MOUSE_LEFTBUTTON)
	{
		if(bDown)
		{
			m_pUI->OnLButtonDown(x, y);
		}
		else
		{
			m_pUI->OnLButtonUp(x, y);
		}
	}
	return;
}

void CShaderSphereVolume::MouseMove(HWND hWnd, int x, int y)
{
	m_pUI->OnMouseMove(x, y);
	return;
}

#define STR_TOGGLE_TEXTURE "Toggle shape texture"

void CShaderSphereVolume::Keyboard(DWORD dwKey, UINT nFlags, bool bDown)
{
	eEBKeyAction Action = TranslateEffectKey(dwKey, nFlags, bDown);
	
    switch ( Action )
    {
		case EB_HELP:
		{
			::MessageBoxEx( NULL, " Help : F1 - Help \n\n Home - Reset To Defaults \n\n W - Wireframe Toggle \n\n Space\\Pause - Toggle Pause/Resume \n\n Left Button & Mouse - Rotate Object\n\n Shift Left Button & Mouse - Pan Camera \n\n Ctrl Left Button & Mouse - Move Camera In & Out\n\n +\\- Change Bump Scale\n\n",
				   "Help", MB_ICONINFORMATION | MB_TASKMODAL, MAKELANGID( LANG_NEUTRAL, SUBLANG_DEFAULT ) );
		}
		break;

		case EB_WIREFRAME:
        {
            m_bWireframe = !m_bWireframe;
			m_dwEffectDirtyFlags |= EBEFFECT_DIRTY_PUBLICSTATE;
            
        }
		break;

		case EB_RESET:
        {
            m_pUI->Reset();

//@@@

            m_bWireframe = false;
			// Reset mesh control points
			m_dwEffectDirtyFlags |= EBEFFECT_DIRTY_PUBLICSTATE;
            
        }
		break;

		case EB_TEXTURES:
			m_bDiamond = !m_bDiamond;
			m_dwEffectDirtyFlags |= EBEFFECT_DIRTY_PUBLICSTATE;
			break;
		case EB_PAUSE:
      m_bPause = !m_bPause;
			m_dwEffectDirtyFlags |= EBEFFECT_DIRTY_PUBLICSTATE;
     break;

        default :
            break;
    }
}

void CShaderSphereVolume::PropertyUpdateCallback(const EBProperty* pProperty, bool bWritten)
{
	if (!bWritten)
		return;


	if (pProperty->IsKindOf(EBTYPE_TRIGGER_PROP))
	{
		EBString	name = pProperty->GetPropertyName();
		/*
		if(name == EBString(STR_TOGGLE_TEXTURE))
		{
			Keyboard('t', 0, true);
		}
		*/
		/*
		else if (name == EBString(STR_DECREASEBUMPSCALE))
		{
			Keyboard(VK_SUBTRACT, 0, true);
		}
		else if (name == EBString(STR_RESETPATCH))
		{
			Keyboard(VK_HOME, 0, true);
		}
		*/
	}
}



