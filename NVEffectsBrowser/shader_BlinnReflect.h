/******************************************************************************

  Copyright (C) 1999, 2000 NVIDIA Corporation
  This file is provided without support, instruction, or implied warranty of any
  kind.  NVIDIA makes no guarantee of its fitness for a particular purpose and is
  not liable under any circumstances for any damages or loss whatsoever arising
  from the use or inability to use this file or items derived from it.
  
    Comments:
    
      
        
******************************************************************************/
#ifndef __SHADER_BLINNREFLECT_H
#define __SHADER_BLINNREFLECT_H

#include "MouseUI.h"


class CShaderSphereVolume;


// Fog device overrides
class NVBlinnReflectDevice : public nv_objects::NVDevice
{
public:
	NVBlinnReflectDevice(LPDIRECT3DDEVICE8 pDev = NULL, CShaderSphereVolume* pDot3 = NULL)
		: NVDevice(pDev),
			m_pDot3Shader(pDot3)
	{ };

	CShaderSphereVolume* m_pDot3Shader;
};

class CShaderSphereVolume : public EBEffect
{
public:
	CShaderSphereVolume();
	~CShaderSphereVolume();
	virtual void UpdateProperties();
	virtual HRESULT Initialize(IDirect3DDevice8* pDev);
	virtual HRESULT Free();
	virtual HRESULT Start();
	virtual HRESULT Tick(EBTimer* pTimer);
	virtual HRESULT ConfirmDevice(D3DCAPS8* pCaps, DWORD dwBehavior, D3DFORMAT Format);
	virtual void MouseButton(HWND hWnd, eButtonID button, bool bDown, int x, int y);
	virtual void MouseMove(HWND hWnd, int x, int y);
	virtual void Keyboard(DWORD dwKey, UINT nFlags, bool bDown);
	virtual void PropertyUpdateCallback(const EBProperty* pProperty, bool bWritten);

protected:
	friend NVBlinnReflectDevice;
	NVBlinnReflectDevice* m_pNVDevice;

	HRESULT SetTransform();
	HRESULT GenerateSphere(D3DXVECTOR3& vCenter, FLOAT fRadius, WORD wNumRings, WORD wNumSections, FLOAT sx, FLOAT sy, FLOAT sz);
	HRESULT GenerateQuad(D3DXVECTOR3& vCenter, FLOAT fRadius);

	HRESULT DrawReflection();

	bool m_bWireframe;
	MouseUI* m_pUI;
	
	LPDIRECT3DTEXTURE8 m_pGlossMap;
	LPDIRECT3DCUBETEXTURE8 m_pCubeTexture;
	LPDIRECT3DTEXTURE8 m_pSphereTexture;
	LPDIRECT3DTEXTURE8 m_pDiamondTexture;
	
	LPDIRECT3DVERTEXBUFFER8 m_pPatchBuffer;
	LPDIRECT3DVERTEXBUFFER8 m_pVertexBuffer;
  LPDIRECT3DINDEXBUFFER8  m_pIndexBuffer;
	
	DWORD m_dwNumFaces;
	DWORD m_dwNumVertices;
	DWORD m_dwNumIndices;
	
	float m_fAngle;

	bool m_bPause;
	bool m_bDiamond;

	DWORD m_dwBlinnPixelShader;
	DWORD m_dwBlinnVertexShader;
};


#endif // __SHADER_BLINNREFLECT
