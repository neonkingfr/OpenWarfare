/* priprava monochromnich spritu - bez masky */
/* SUMA, 5/1992 */

#include <macros.h>
#include <tos.h>
#include <stdio.h>

#include "sprite.h"

#define NazevSpr "notyimg"

typedef struct
{
	char Nam[8];
	int h,w;
	int xi,yi;
} SprInfo;

static int SejmiSprite( FILE *F, FILE *I, SprInfo *SI )
{
	word *IA=&VRA[SI->yi*40+(SI->xi>>4)];
	int xi,yi;
	int ll=20;
	fprintf(F,"Sprite %sSpr={%d,%d};\n",SI->Nam,SI->h,SI->w);
	fprintf(I,"extern Sprite %sSpr;\n",SI->Nam);
	fprintf(F,"word %sImg[]={",SI->Nam);
	for( yi=0; yi<SI->h; yi++ ) for( xi=0; xi<SI->w; xi++ )
	{
		int i=yi*40+xi;
		fprintf(F,"0x%x,",IA[i]);
		ll+=7;
		if( ll>=220 ) {ll=0;fprintf(F,"\n");}
	}
	fprintf(F,"};\n");
	return 0;
}

enum {YI=15,XM=128,WM=16/16,HM=32,MS=32}; /* noty */
enum {      KS=32, WK=32/16,HK=80      }; /* osnovy */
enum {             WT=16/16            }; /* takt */
enum {      XB=320                     }; /* odrazky ... */
enum {YP=64                            }; /* pauzy */

static SprInfo Sprity[]=
{
	"Nota1H",HM,WM,MS*0+XM,YI,
	"Nota2H",HM,WM,MS*1+XM,YI,
	"Nota4H",HM,WM,MS*2+XM,YI,
	"Nota8H",HM,WM,MS*3+XM,YI,
	"Not16H",HM,WM,MS*4+XM,YI,
	"Not32H",HM,WM,MS*5+XM,YI,
	"Nota1D",HM,WM,MS*0+XM,YP,
	"Nota2D",HM,WM,MS*1+XM,YP,
	"Nota4D",HM,WM,MS*2+XM,YP,
	"Nota8D",HM,WM,MS*3+XM,YP,
	"Not16D",HM,WM,MS*4+XM,YP,
	"Not32D",HM,WM,MS*5+XM,YP,
	"Klice2",74, 4,0,     96,
	"Takt",  HK,WT,KS+64,  YI,
	"Konec", HK,WT,KS+80,  YI,
	"ZacRep",HK,WT,KS,     YI,
	"EndRep",HK,WT,KS+16,  YI,
	"ZacSek",HK,WT,KS+32,  YI,
	"KonSek",HK,WT,KS+48,  YI,
	"Becko", HM,WM,XB,     YI,
	"Odraz", HM,WM,XB+1*MS,YI,
	"Krizek",HM,WM,XB+2*MS,YI,
	"Pauza4",HM,WM,XB+0*MS,YP,
	"Pauza1",HM,WM,XB+1*MS,YP,
	"Pauza8",HM,WM,XB+2*MS,YP,
	"Pauz16",HM,WM,XB+3*MS,YP,
	"Pauz32",HM,WM,XB+4*MS,YP,
	"Tecka", HM,WM,XB+3*MS,YI,
	"Triola",HM,WM,XB+3*MS+16,YI,
	"PomLin",1,WM,XB+4*MS,YI+21,
	"TempNot",HM,WM,XB+4*MS+16,YI,
	"Legato", HM,WM,XB+4*MS+32,YI,
	"ZacOkt",14,1, XB+7*MS,YI,
	""
};

static int VezmiSprity( FILE *F, FILE *I, SprInfo *SI )
{
	for( ; SI->Nam[0]!=0; SI++ )
	{
		SejmiSprite(F,I,SI);
	}
	return 0;
}

static int InitSprites( void )
{
	int FIn=Fopen(NazevSpr".PC3",0);
	if( FIn>=0 )
	{
		word Pal[16];
		int Err=LoadPc(VRA,Pal,FIn);
		Fclose(FIn);
		return Err;
	}
	return FIn;
}

int main( int argc, const char *argv[] )
{
	int ret=-1;
	(void)argc,(void)argv;
	VRA=Physbase();
	if( InitSprites()>=0 )
	{
		FILE *F=fopen("..\\"NazevSpr".C","w");
		if( F )
		{
			FILE *I;
			setvbuf(F,NULL,_IOFBF,18*1024);
			fprintf(F,"#include <macros.h>\n#include \"spritem.h\"\n#include \""NazevSpr".h\"\n\n");
			I=fopen("..\\"NazevSpr".H","w");
			if( I )
			{
				setvbuf(I,NULL,_IOFBF,18*1024);
				ret=VezmiSprity(F,I,Sprity);
				fclose(I);
			}
			fclose(F);
		}
	}
	return ret;
}
