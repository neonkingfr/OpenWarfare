// CfgConvert.cpp : Defines the entry point for the console application.
//

#include "apppch.hpp"
#include <El/paramFile/paramFile.hpp>

#include <string.h>
#include <stdio.h>

int main(int argc, char* argv[])
{
	argv++;
	argc--;
	const char *arg = *argv;
	bool toBin = true;
	if (argc < 1) goto Usage;
	if (*arg == '-' || *arg == '/')
	{
		argv++; // skip argument
		argc--;
		arg++; // skip dash
		if (!stricmp(arg,"bin")) toBin = true;
		else if (!stricmp(arg,"txt")) toBin = false;
		else goto Usage;
	}
	if (argc < 1) goto Usage;
	if (toBin)
	{
		// from open to binary format
		while (--argc >= 0)
		{
			RString name = *argv++;
			ParamFile f;
			if (f.Parse(name) != LSOK) continue;
			f.SaveBin(name);
		}
	}
	else
	{
		// from binary to open format
		while (--argc >= 0)
		{
			RString name = *argv++;
			ParamFile f;
			if (!f.ParseBin(name)) continue;
			f.Save(name);
		}
	}
	return 0;

Usage:
	printf("Usage\n");
	printf("   cfgConvert [-bin | -txt] source [source]\n");
	return 0;		
}

#include <Es/Memory/normalNew.hpp>
/*
void *operator new ( size_t size, const char *file, int line )
{
	return malloc(size);
}*/
