/*
    @file   netlog.cpp
    @brief  NetLog - General purpose logging object.
    
    Copyright &copy; 2002 by BIStudio (www.bistudio.com)
    @author PE
    @since  12.3.2002
    @date   19.9.2002
*/

#include <Es/essencepch.hpp>
#include <time.h>

//------------------------------------------------------------
//  Logging (thread-safe):

bool netLogValid = true;

#if !defined(_WIN32) || defined NET_LOG

NetLogger::NetLogger ()
    : LockInit(cs,"NetLogger",true)
{
    cs.enter();
    f = NULL;
    startSystemTime();
    startTime = getSystemTime();
    netLogValid = true;
    f = fopen("nolog.txt","rt");
    if ( f ) {
        fclose(f);
        f = NULL;
        netLogValid = false;
        }
    cs.leave();
}

void NetLogger::open ()
{
    if ( f ) return;
    cs.enter();
    int i = 1;
    char buf[32];
    do {
        sprintf(buf,"net%03d.log",i++);
        f = fopen(buf,"rt");
        if ( !f ) break;
        fclose(f);
        } while ( true );
    f = fopen(buf,"wt");
    counter = 0;
        // initial message:
    time_t timet;
    time(&timet);
    timet -= (time_t)((getSystemTime()-startTime)/1000000);
    strcpy(buf,ctime(&timet));
    buf[strlen(buf)-1] = (char)0;           // remove trailing '\n'
    fprintf(f,"%10.5f: NetLogger: start - %s\n",0.0,buf);
    fprintf(f,"%10.5f: Clock frequency = %u Hz\n",0.0,getClockFrequency());
    cs.leave();
}

NetLogger::~NetLogger ()
{
    if ( !netLogValid || !f ) return;
    char buf[32];
    time_t timet;
    time(&timet);
    strcpy(buf,ctime(&timet));
    buf[strlen(buf)-1] = (char)0;           // remove trailing '\n'
    netLog("NetLogger: stop - %s",buf);
    netLogValid = false;
    cs.enter();
    if ( f ) {
        fclose(f);
        f = NULL;
        }
    cs.leave();
}

double NetLogger::getTime () const
{
    return( 1.e-6 * (getSystemTime() - startTime) );
}

void NetLogger::log ( const char *format, va_list argptr )
{
    cs.enter();
    if ( netLogValid ) {
        if ( !f ) open();                   // deferred file open
        Assert( f );
        fprintf(f,"%10.5f: ",getTime());
        vfprintf(f,format,argptr);
        fputc('\n',f);
#ifndef IMMEDIATE_NET_LOG
        if ( !(++counter & 0x1ff) )
#endif
        fflush(f);
        }
    cs.leave();
}

#ifdef EXTERN_NET_LOG
  extern
#endif
NetLogger netLogger
#ifndef EXTERN_NET_LOG
INIT_PRIORITY_URGENT
#endif
;

void netLog ( const char *format, ... )
{
    if ( !netLogValid ) return;
	va_list arglist;
	va_start(arglist,format);
	netLogger.log(format,arglist);
	va_end(arglist);
}

double getLogTime ()
{
    if ( !netLogValid ) return 0.0;
	return netLogger.getTime();
}

#else

double getLogTime ()
{
    return 0.0;
}

#endif
