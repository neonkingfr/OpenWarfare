#include <Es/essencepch.hpp>
#include "appFrame.hpp"

#include <stdio.h>
#include <string.h>
#include "Es/Common/win.h"

#if _MSC_VER && !defined INIT_SEG_COMPILER
	#pragma warning(disable:4074)
	#pragma init_seg(compiler)
	#define INIT_SEG_COMPILER
#endif


static AppFrameFunctions GAppFrameFunctions INIT_PRIORITY_URGENT;
static AppFrameFunctions *CurrentAppFrameFunctions = &GAppFrameFunctions;

void SetAppFrameFunctions(AppFrameFunctions *f)
{
	CurrentAppFrameFunctions = f;
}

#if _ENABLE_REPORT

void AppFrameFunctions::LogF(const char *format, va_list argptr)
{
	char buf[512];
	vsprintf(buf,format,argptr);
	strcat(buf,"\n");
#ifdef _WIN32
	OutputDebugString(buf);
#else
	fputs(buf,stderr);
#endif
}

void AppFrameFunctions::LstF(const char *format, va_list argptr)
{
	LogF(format, argptr);
}

void AppFrameFunctions::ErrF(const char *format, va_list argptr)
{
	LogF(format, argptr);
}

void LogF(char const *format,...)
{
	va_list arglist;
	va_start(arglist, format);

	CurrentAppFrameFunctions->LogF(format, arglist);

	va_end(arglist);
}

void LstF(char const *format,...)
{
	va_list arglist;
	va_start(arglist, format);

	CurrentAppFrameFunctions->LstF(format, arglist);

	va_end(arglist);
}

void ErrF(char const *format,...)
{
	va_list arglist;
	va_start(arglist, format);

	CurrentAppFrameFunctions->ErrF(format, arglist);

	va_end(arglist);
}
#endif

void ErrorMessage(ErrorMessageLevel level, char const *format,...)
{
	va_list arglist;
	va_start(arglist, format);

	CurrentAppFrameFunctions->ErrorMessage(level, format, arglist);

	va_end(arglist);
}

void ErrorMessage(char const *format,...)
{
	va_list arglist;
	va_start(arglist, format);

	CurrentAppFrameFunctions->ErrorMessage(format, arglist);

	va_end(arglist);
}

void WarningMessage(char const *format,...)
{
	va_list arglist;
	va_start(arglist, format);

	CurrentAppFrameFunctions->WarningMessage(format, arglist);

	va_end(arglist);
}

void GlobalShowMessage(int timeMs, const char *msg, ...)
{
	va_list arglist;
	va_start(arglist, msg);

	CurrentAppFrameFunctions->ShowMessage(timeMs, msg, arglist);

	va_end(arglist);
}

DWORD GlobalTickCount()
{
	return CurrentAppFrameFunctions->TickCount();
}
