#ifdef _MSC_VER
#  pragma once
#endif

/*
    @file   netlog.hpp

    @brief  NetLogger - General purpose logging object.

    Copyright &copy; 2002 by BIStudio (www.bistudio.com)
    @author PE
    @since 13.3.2002
    @date  10.6.2002
*/

#ifndef _NETLOG_H
#define _NETLOG_H

//-------------------------------------------------------------------------
//  NetLogger object:

#if !defined(_WIN32) || defined NET_LOG

/**
    General-purpose logger.
    Writes into "netNNN.log" text files (NNN is 001, 002, ..).
*/
class NetLogger {

protected:

    FILE *f;

    unsigned counter;                       ///< for fflush.

    PoCriticalSection cs;                   ///< for exclusivity.

    unsigned64 startTime;                   ///< in getSystemTime() format (in microseconds).

    void open ();

public:

    NetLogger ();

    ~NetLogger ();

    void log ( const char *format, va_list argptr );

    double getTime () const;

    };

#endif  // NET_LOG

#endif
