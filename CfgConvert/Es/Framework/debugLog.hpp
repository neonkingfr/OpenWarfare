#ifdef _MSC_VER
#pragma once
#endif

#ifndef _DEBUGLOG_HPP
#define _DEBUGLOG_HPP

#ifndef CCALL

#ifdef _MSC_VER
#define CCALL __cdecl
#else
#define CCALL
#endif

#endif
// critical error - terminate application
void CCALL ErrorMessage( const char *format, ... );

// noncritical error - may terminate application
void CCALL WarningMessage( const char *format, ... );

// assertion failed 
void CCALL FailHook( const char *text );

#define NoLog (void)

#ifdef _MSC_VER
	#if _RELEASE
		#define FailHook(text)
	#else
		#define FailHook(text) __asm {int 3}
	#endif
#elif __MWERKS__
	#if _RELEASE
		#define FailHook(text)
	#else
		#define FailHook(text) __asm {int 3}
	#endif	
#else
	#define FailHook(text)
#endif

#ifndef _ENABLE_REPORT
	// assume default value true
	#define _ENABLE_REPORT 1
#endif

#if _ENABLE_REPORT
	#define DoVerify(expr) \
	{ \
		if( !(expr) ) \
		{ \
			DebF("%s(%d) : Assertion failed '%s'",__FILE__,__LINE__,#expr); \
			FailHook(#expr); \
		} \
	}
	#define DoAssert(expr) DoVerify(expr)
	void CCALL LogF( const char *format, ... );
	void CCALL ErrF( const char *format, ... ); // does produce callstack report
	void CCALL LstF( const char *format, ... ); // no call stack report
#else
	#define DoVerify(expr) (expr)
	#define DoAssert(expr)
	#define LogF NoLog
	#define ErrF NoLog
	#define LstF NoLog
#endif

#ifdef _DEBUG
	#define DebugLog LogF
	#define AssertDebug( expr ) Verify(expr)
	#define DebF LogF
	#define RptF LogF
#else
	#define DebugLog NoLog
	#define AssertDebug( expr )
	#define DebF ErrF
	#define RptF LstF
#endif

#ifdef NDEBUG
	#define Assert( expr )
	#define Verify( expr ) (expr)
	#define Fail(text) DebF("%s(%d) : %s",__FILE__,__LINE__,text)
	#define Log NoLog
#else
	#define Assert( expr ) DoAssert(expr)
	#define Verify( expr ) DoAssert(expr)
	#define Fail(text) {DebF("%s(%d) : %s",__FILE__,__LINE__,text);FailHook(text);}
	#define Log LogF
#endif

#include <stdio.h>

#pragma warning(disable:4996)
inline const char *FileLineF( const char *file, int line, const char *postfix )
{
	static char buf[512];
	#if (defined __GNUC__ || defined __INTEL_COMPILER)
	sprintf(buf,"%s(%d): %s",file,line,postfix);
	#else
	_snprintf(buf,sizeof(buf),"%s(%d): %s",file,line,postfix);
	#endif
	buf[sizeof(buf)-1]=0;
	return buf;
}

#define FileLine(postfix) FileLineF(__FILE__,__LINE__,postfix)

#endif

