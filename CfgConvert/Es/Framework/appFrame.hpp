#ifdef _MSC_VER
#pragma once
#endif

#ifndef __APP_FRAME_HPP
#define __APP_FRAME_HPP

#include <stdarg.h>

#ifndef _ENABLE_REPORT
#define _ENABLE_REPORT 1
#endif

//! error level
enum ErrorMessageLevel
{
	//! may be ignored, marginal impact (like only slight performance degradation)
	EMNote,
	//! application is able to continue, but with limited functionality
	EMWarning,
	//! application is not able to perform requested task, but is able to continue
	EMError,
	//! application is not able to continue
	EMCritical,
	//! no error level - used to disable all errors
	EMDisableAll
};

class AppFrameFunctions
{
public:
	AppFrameFunctions() {};
	virtual ~AppFrameFunctions() {};

#if _ENABLE_REPORT
	virtual void LogF(const char *format, va_list argptr);
	virtual void LstF(const char *format, va_list argptr);
	virtual void ErrF(const char *format, va_list argptr);
#endif

	virtual void ErrorMessage(ErrorMessageLevel level, const char *format, va_list argptr) {}
	virtual void ErrorMessage(const char *format, va_list argptr) {}
	virtual void WarningMessage(const char *format, va_list argptr) {}
	
	virtual void ShowMessage(int timeMs, const char *msg, va_list argptr) {}
	virtual unsigned long TickCount() {return 0;}
};

void SetAppFrameFunctions(AppFrameFunctions *f);

#if _ENABLE_REPORT
void LogF(const char *format,...);
void LstF(const char *format,...);
void ErrF(const char *format,...);
#endif

void ErrorMessage(ErrorMessageLevel level, const char *format,...);
void ErrorMessage(const char *format,...);
void WarningMessage(const char *format,...);

void GlobalShowMessage(int timeMs, const char *msg, ...);
unsigned long GlobalTickCount();

// void *operator new ( size_t size, const char *file, int line );

#endif
