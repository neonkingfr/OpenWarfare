#ifndef _CHECKMEM_HPP
#define _CHECKMEM_HPP

#if defined MFC_NEW
	// MFC application needs to use MFC new
	// use MS VC++ memory debuging
	//#define _CRTDBG_MAP_ALLOC 1
	//#include <crtdbg.h>

	#define safeNew (size ) new(size)
	#define safeDelete (mem) delete(mem)

#else

	class MemFunctions
	{
		public:
		virtual void *New(size_t size) {return malloc(size);}
		virtual void *New(size_t size, const char *file, int line) {return malloc(size);}
		virtual void Delete(void *mem){free(mem);}
		virtual void Delete(void *mem, const char *file, int line) {free(mem);}


		//! base of memory allocation space, NULL is unknown/undefined
		virtual void *HeapBase() {return NULL;}

		//! approximate total ammount of commited memory
		virtual size_t HeapUsed() {return 0;}

		//! approximate total count of free memory blocks (used to determine fragmentation)
		virtual int FreeBlocks() {return 1;}

		//! approximate total count of allocated memory blocks (used to determine fragmentation)
		virtual int MemoryAllocatedBlocks() {return 1;}

		//! list all allocated blocks to debug log
		virtual void Report() {}

		//! check heap integrity
		virtual bool CheckIntegrity() {return true;}
		//! check if out of memory is signalized
		//! if true, application should limit memory allocation as much as possible
		virtual bool IsOutOfMemory() {return false;}

		//! clean-up any cached memory
		virtual void CleanUp() {}
	};

	extern MemFunctions *GMemFunctions;
	extern MemFunctions *GSafeMemFunctions;

	inline void * CCALL operator new (size_t size, int addr, char const *file, int line)
	{ // placement new
		return (void *)addr;
	}
	#ifdef _CPPUNWIND
		inline void CCALL operator delete (void *mem, int addr, char const *file, int line)
		{ // placement delete
		}
	#endif

	// note: global array versions ([]) are ignored in VC5.0

	inline void * CCALL operator new ( size_t size ) {return GMemFunctions->New(size);}
	inline void * CCALL operator new[] ( size_t size ) {return GMemFunctions->New(size);}

	inline void * CCALL operator new ( size_t size, const char *file, int line )
	{
		return GMemFunctions->New(size,file,line);
	}
	inline void * CCALL operator new[] ( size_t size, const char *file, int line )
	{
		return GMemFunctions->New(size,file,line);
	}
	inline void CCALL operator delete ( void *ptr )
	{
		GMemFunctions->Delete(ptr);
	}
	inline void CCALL operator delete[] ( void *ptr )
	{
		GMemFunctions->Delete(ptr);
	}

	#ifdef _CPPUNWIND
		inline void CCALL operator delete ( void *ptr, const char *file, int line )
		{
			GMemFunctions->Delete(ptr,file,line);
		}
	#endif

	#if !_RELEASE

		#define ALLOC_DEBUGGER 1

		#ifndef CCALL
			#define CCALL
		#endif

		// memory errors tracking for custom new handlers


		#define debugNew new(__FILE__,__LINE__)
		#define debugNewAddr(addr) new(addr,__FILE__,__LINE__)

		#define stdNew new()
		#define stdNewAddr new(addr)

		#define newAddr(addr) new(addr)

		#include <Es/Memory/debugNew.hpp>
	#endif

	// required interface for MT safe heap
	inline void *safeNew ( size_t size ){return GSafeMemFunctions->New(size);}
	inline void safeDelete ( void *mem ){GSafeMemFunctions->Delete(mem);}

	inline bool IsOutOfMemory() {return GMemFunctions->IsOutOfMemory();}
	inline size_t MemoryUsed() {return GMemFunctions->HeapUsed();}

#endif

#endif
