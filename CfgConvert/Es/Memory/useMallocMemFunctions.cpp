#include <Es/essencepch.hpp>
#include "checkMem.hpp"

#if !defined MFC_NEW

static MemFunctions MemFunctionsMalloc;

MemFunctions *GMemFunctions = &MemFunctionsMalloc;
MemFunctions *GSafeMemFunctions = &MemFunctionsMalloc;

#endif