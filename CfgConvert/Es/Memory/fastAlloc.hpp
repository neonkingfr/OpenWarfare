///////////////////////////////////////////////////////////////////////////////
// based on
// FastFixedAllocator.h
// from CGDC'98
//
///////////////////////////////////////////////////////////////////////////////


#ifndef _FAST_ALLOC_HPP
#define _FAST_ALLOC_HPP

#include "normalNew.hpp"
#ifdef _CPPRTTI
#include <typeinfo.h>
#endif

#include <Es/Containers/cachelist.hpp>

#include <Es/Framework/debugLog.hpp>

///////////////////////////////////////////////////////////////////////////////
// FastFixedAllocator
//
// This version of the allocator is very similar to the one in Stroustrup's
// "The C++ Programming Language", 3rd edition. We have another version that
// we sometimes use that is based on an array instead of a linked list. It
// is slightly faster, but slightly less flexible, as well.
//
/*
	Example usage:
	//Create a class and define a custom allocator for it.

	//or use predefined macro
	struct A
	{
		USE_FIXED_ALLOCATOR
	};
	DEFINE_FIXED_ALLOCATOR(A);
*/

class FastCAlloc; // used for classes
class FastAlloc;

#ifndef PREPROCESS_DOCUMENTATION

// real implementation

#ifdef _WIN32

#if defined __INTEL_COMPILER || _MSC_VER>=1200

#define USE_FAST_ALLOCATOR_ID(x) \
	private: \
	static FastCAlloc _allocator##x; \
	void* operator new[]   (size_t n); \
	void  operator delete[](void* ptr, size_t n); \
	public: \
	void *operator new( size_t n ){return _allocator##x.CAlloc(n);} \
	void operator delete(void* ptr){_allocator##x.CFree(ptr);} \
	void *operator new( size_t n, const char *file, int line ){return _allocator##x.CAlloc(n);} \
	void operator delete(void* ptr, const char *file, int line){_allocator##x.CFree(ptr);}
#else

#define USE_FAST_ALLOCATOR_ID(x) \
 \
	private: \
	static FastCAlloc _allocator##x; \
	void* operator new[]   (size_t n); \
	void  operator delete[](void* ptr, size_t n); \
	public: \
	void *operator new( size_t n ){return _allocator##x.CAlloc(n);} \
	void *operator new( size_t n, const char *file, int line ){return _allocator##x.CAlloc(n);} \
	void operator delete( void* ptr, size_t ){_allocator##x.CFree(ptr);}

#endif
#else
#define USE_FAST_ALLOCATOR_ID(x)
#endif

#ifdef _WIN32
  #define DEFINE_FAST_ALLOCATOR_ID(className,x) \
          FastCAlloc className::_allocator##x(sizeof(className),#className);
  #define USE_FAST_ALLOCATOR USE_FAST_ALLOCATOR_ID(F)
  #define DEFINE_FAST_ALLOCATOR(className) DEFINE_FAST_ALLOCATOR_ID(className,F)
#else
  #define DEFINE_FAST_ALLOCATOR_ID(className,x)
  #define USE_FAST_ALLOCATOR
  #define DEFINE_FAST_ALLOCATOR(className)
#endif

void * CCALL operator new( size_t size, const char *file, int line );
void * CCALL operator new( size_t size );
void CCALL operator delete( void *ptr );

#define USE_NORMAL_ALLOCATOR \
 \
	public: \
	void* operator new[] (size_t n){return ::operator new(n);} \
	void  operator delete[](void* ptr, size_t n){::operator delete(ptr);} \
	void* operator new (size_t n){return ::operator new(n);} \
	void  operator delete(void* ptr ){::operator delete(ptr);} \
	void* operator new (size_t n, const char *file, int line){return ::operator new (n,file,line);} \
	void operator delete(void* ptr, const char *file, int line){::operator delete(ptr);}

#else

// macro definitions for documentation purposes (Doxygen)

//! Use fast fixed size allocation (declare)
#define USE_FAST_ALLOCATOR public: FastCAlloc _allocator;
//! Use fast fixed size allocation (define)
#define DEFINE_FAST_ALLOCATOR(className)

//! Use fast fixed size allocation (declare with unique ID)
#define USE_FAST_ALLOCATOR_ID(x) public: FastCAlloc _allocator;
//! Use fast fixed size allocation (define with unique ID)
#define DEFINE_FAST_ALLOCATOR_ID(className,x)

//! Use global allocation
#define USE_NORMAL_ALLOCATOR public: NormalAlloc _allocator;

//! Normal new / delete operators used
/*!
	User to overload FastCAlloc allocators inherited from base class
*/

class NormalAlloc
{
};

#endif

enum {FastAllocChunkSize=8*1024-8};

//! Fast fixed block allocation
/*!
	Used as base class for different fixed block allocators
*/
class FastAlloc
{

	public:
	enum {chunkSize=FastAllocChunkSize};
	struct Chunk;

	FastAlloc( size_t n, const char *name="", int alignOffset=0 );
	~FastAlloc();

	// only counted allocation left
	// uncounted is too problematic - leads to fragmentation

	//! allocate block
	void *Alloc(size_t n);
	//! free block
	void Free(void *pAlloc);

	//! free unused chunks
	void CleanUp();

	static FastAlloc *WhichAllocator( void*pAlloc );
	void *AllocCounted( size_t n );
	void FreeCounted(void*pAlloc, bool fast=true);

	const char *Name() const {return _name;}
	size_t ItemSize() const {return esize;}
	static int ChunkSize() {return sizeof(Chunk);}


	protected:
	struct Link //: public CLDLink
	{
		Link *next; // next free in chunk
		Chunk *chunk; // which chunk is it in
	};
	struct ChunkHead
	{
		int null1; // null - to distinguish this block type
		FastAlloc *allocator; // which allocator this chunk serves for
		int allocated; // number of allocated items
		// align mem to 16 B boundary
		// each block will have short 4B description on the beginning
		Link *head;
		void *align[2];
	};

	public:
	struct Chunk: public ChunkHead,public CLDLink
	{
		enum {size = chunkSize-sizeof(ChunkHead)-sizeof(CLDLink)};
		char mem[size];
	};

	protected:

	// no free items - add new chunk
	void Grow();

	// remove specific chunks and all its items
	// there shoulb be no item allocated in chunk
	//virtual void ChunkRemove( Chunk *chunk );
	void ChunkRemove( Chunk *chunk );

	//! 	
	// parent memory manager
	virtual Chunk *NewChunk();
	virtual void DeleteChunk( Chunk *chunk );

	// !destruction - free all chunks
	virtual void FreeChunks();


	CLList<Chunk> chunksFree; // some Link in chunk is free
	CLList<Chunk> chunksBusy; // all Links busy
	const char *_name; // for debugging purposes
	int _alignOffset; // align each chunk
	unsigned int esize;
	int _nElemPerChunk;
	//Link *head;

	int allocated; // how much is allocated

	friend void Dammage_FastAlloc(FastAlloc *alloc);

	bool CheckIfFree(Chunk *chunk, void *data) const;
};

//! Fast fixed block allocation with automatic cleanup
/*!
	User to define fast allocators for different classes (see USE_FAST_ALLOCATOR)
*/

class FastCAlloc: public FastAlloc
{
	typedef FastAlloc base;

	public:
	FastCAlloc( size_t n, const char *name="" );
	~FastCAlloc();

	void *CAlloc( size_t n );
	void CFree( void*pAlloc );

	//void ChunkRemove( Chunk *chunk );

	static void CleanUpAll(); // free unused chunks in all FastCAlloc instances

	private: // hide parent members
	static FastAlloc *WhichAllocator( void*pAlloc );
	void *AllocCounted(size_t n);
	void FreeCounted(void*pAlloc, bool fast=true);

};

#include "debugNew.hpp"

#endif //sentry








