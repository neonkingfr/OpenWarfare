#ifdef _MSC_VER
#pragma once
#endif

#ifndef __MEM_ALLOC_HPP
#define __MEM_ALLOC_HPP

// MemAlloc template sample
// default implementation usign new char

#include <Es/Framework/debugLog.hpp>

#include <Es/Memory/normalNew.hpp>

class MemAllocD
{
	public:
	#if ALLOC_DEBUGGER
	static void *Alloc( int &size, const char *file, int line, const char *postfix )
	{
		return new(FileLineF(file,line,postfix),0) char[size];
	}
	#else
	static void *Alloc( int &size )
	{
		// size is not changed
		return new char[size];
	}
	#endif
	static void Free( void *mem, int size )
	{
		delete[] (char *)mem;
	}
	static void Free( void *mem )
	{
		delete[] (char *)mem;
	}
	static void Unlink( void *mem ) {}
};

class MemAllocS
{
	void *_mem;
	int _size;
	MemAllocD _fallBack;

	public:
	#if ALLOC_DEBUGGER
	void *Alloc( int &size, const char *file, int line, const char *postfix )
	{
		if( size<=_size )
		{
			size=_size;
			_size=-_size; // mark as allocated
			return _mem;
		}
		else return _fallBack.Alloc(size,file,line,postfix);
	}
	#else
	void *Alloc( int &size )
	{
		// size is not changed
		if( size<=_size )
		{
			size=_size;
			_size=-_size; // mark as allocated
			return _mem;
		}
		else return _fallBack.Alloc(size);
	}
	#endif
	void Free( void *mem, int size )
	{
		//if( size<=-_size )
		if( mem==_mem )
		{
			Assert( _size<0 );
			_mem=mem;
			_size=-_size;
		}
		else
		{
			_fallBack.Free(mem,size);
		}
	}
	MemAllocS():_mem(NULL),_size(0){}
	~MemAllocS(){Clear();}

	MemAllocS *InitStorage( int size )
	{
		if( !_mem )
		{
			#if ALLOC_DEBUGGER
			_mem=_fallBack.Alloc(size,__FILE__,__LINE__,"");
			#else
			_mem=_fallBack.Alloc(size);
			#endif
			_size=size;
		}
		return this;
	}

	void *GetMemory() const {return _mem;} // for diagnostic purposes only

	MemAllocS *InitStorage( int size, const char *name )
	{
		if( !_mem )
		{
			#if ALLOC_DEBUGGER
			_mem=_fallBack.Alloc(size,__FILE__,__LINE__,name);
			#else
			_mem=_fallBack.Alloc(size);
			#endif
			_size=size;
		}
		return this;
	}

	void Clear()
	{
		if( _mem )
		{
			DoAssert( _size>=0 );
			_fallBack.Free(_mem); // _size does not matter for MemAllocD
			_mem=NULL;
			_size=0;
		}
	}

	void Unlink( void *mem )
	{
		if( _mem==mem )
		{
			// memory is now dynamic - leave it to _fallback
			_mem=NULL;
			// allocate new static memory
			InitStorage(_size<0 ? -_size : _size,"Unlink");
		}
	}
};

class MemAllocSS // static shared
{
	MemAllocS *_alloc;

	public:
	void Init( MemAllocS *alloc ) {_alloc=alloc;}
	MemAllocSS():_alloc(NULL) {}
	MemAllocSS( MemAllocS *alloc ):_alloc(alloc) {}
	//return _alloc->Alloc(

	#if ALLOC_DEBUGGER
	void *Alloc( int &size, const char *file, int line, const char *postfix )
	{
		if( _alloc ) return _alloc->Alloc(size,file,line,postfix);
		else return MemAllocD::Alloc(size,file,line,postfix);
	}
	#else
	void *Alloc( int &size )
	{
		if( _alloc ) return _alloc->Alloc(size);
		else return MemAllocD::Alloc(size);
	}
	#endif
	void Free( void *mem, int size )
	{
		if( _alloc ) _alloc->Free(mem,size);
		else MemAllocD::Free(mem);
	}

	void Unlink( void *mem )
	{
		if( _alloc )
		{
			_alloc->Unlink(mem);
			_alloc=NULL;
		}
	}
};

class MemAllocSA
{
	// memory allocated on stack

	void *_mem;
	int _size;
	MemAllocD _fallBack;

	public:
	#if ALLOC_DEBUGGER
	void *Alloc( int &size, const char *file, int line, const char *postfix )
	{
		if( size<=_size )
		{
			// we have enough memory
			size=_size;
			_size=-_size; // mark as allocated
			return _mem;
		}
		else return _fallBack.Alloc(size,file,line,postfix);
	}
	#else
	void *Alloc( int &size )
	{
		// size is not changed
		if( size<=_size )
		{
			// we have enough memory
			size=_size;
			_size=-_size; // mark as allocated
			return _mem;
		}
		else return _fallBack.Alloc(size);
	}
	#endif
	void Free( void *mem, int size )
	{
		//if( size<=-_size )
		if( mem==_mem )
		{
			// make size negative - this will disable any other requests
			Assert( _size<0 );
			_mem=mem;
			_size=-_size;
		}
		else
		{
			_fallBack.Free(mem,size);
		}
	}
	MemAllocSA():_mem(NULL),_size(0){}
	void Init( void *mem, int size ){_mem=mem,_size=size;}
	~MemAllocSA(){Clear();}

	void Clear()
	{
		if( _mem )
		{
			// only non-allocated memory may be released
			DoAssert( _size>=0 );
			_mem=NULL;
			_size=0;
		}
	}

	void Unlink( void *mem )
	{
		if( _mem==mem )
		{
			Fail("Unable to unlink");
		}
	}
};

template <class Type>
class MStorage: public MemAllocS
{
	public:
	MemAllocS *Init( int size )
	{
		const int MinSize=32; // must be higher or equal MinGrow from AutoArray
		if( size<MinSize ) size=MinSize;
		#if ALLOC_DEBUGGER && defined _CPPRTTI
		return InitStorage(size*sizeof(Type),typeid(Type).name());
		#else
		return InitStorage(size*sizeof(Type));
		#endif
	}
};

#include <Es/Memory/debugNew.hpp>

#endif
