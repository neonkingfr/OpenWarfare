/**
    @file   platform.cpp
    @brief  Linux-port helpers.

    Copyright &copy; 2002 by BIStudio (www.bistudio.com)
    @author PE
    @since  21.7.2002
    @date   27.9.2002
*/

#ifndef _WIN32

#include "Es/platform.hpp"

#include <ctype.h>
#include <sys/types.h>
#include <utime.h>
#include <string.h>
#include <stdlib.h>

void unixPath ( char *path )
{
    if ( !path ) return;
    do {
        if ( *path == '\\' )
        *path = '/';
    else
    if ( isupper(*path) )
        *path = tolower(*path);
    } while ( *(++path) );
}

bool isSuffix ( const char *str, const char *suffix )
{
    if ( !suffix || !suffix[0] ) return true;
    if ( !str || !str[0] ) return false;
    int strLen = strlen(str);
    int suffLen = strlen(suffix);
    if ( strLen < suffLen ) return false;
    return( !strcmp(str+strLen-suffLen,suffix) );
}

long long atoi64 ( const char *str )
{
    if ( !str || !str[0] ) return 0;
    long long result = 0;
    while ( isdigit(*str) ) {
        result *= 10;
        result += *str++ - '0';
        }
    return result;
}

char *i64toa ( long long i, char *buf, int radix )
{
    if ( !buf || radix < 2 || radix > 10 ) return NULL;
    int len = 0;
    bool sign = false;
    if ( i < 0 ) {
        sign = true;
    len = 1;
    i = -i;
    }
    long long tmp = i;
    do
    len++;
    while ( (tmp /= radix) );
    char *ptr = buf + len;
    *ptr-- = (char)0;
    do
        *ptr-- = '0' + (i % radix);
    while ( (i /= radix) );
    if ( sign ) *ptr = '-';
    return buf;
}

char *strDup ( const char *src )
{
    if ( !src ) return NULL;
    char *res = (char*)malloc(strlen(src)+1);
    if ( res ) strcpy(res,src);
    return res;
}

const long long Origin = 0x19db1ded53e8000LL;

bool fileTime ( const char *fileName, long long win32Time )
    // returns 'true' on success
{
    if ( !fileName || !fileName[0] ) return false;
    LocalPath(fn,fileName);
    struct stat st;
    stat(fn,&st);
    struct utimbuf utim;
    if ( win32Time > Origin )
        win32Time = (win32Time - Origin) / 10000000LL;
    else
        win32Time = 1;
    utim.actime  = st.st_atime;
    utim.modtime = (time_t)win32Time;
    return( utime(fn,&utim) == 0 );
}

const size_t copyBuf = 1024;

bool fileCopy ( const char *src, const char *dest )
    // returns 'true' on success
{
    if ( !src || !src[0] || !dest || !dest[0] ) return false;
    LocalPath(sfn,src);
    LocalPath(dfn,dest);
    int f1 = open(sfn,O_RDONLY);
    if ( f1 < 0 ) return false;
    int f2 = open(dfn,O_CREAT|O_WRONLY|O_TRUNC);
    if ( f2 < 0 ) {
        close(f1);
    return false;
        }
    char buf[copyBuf];
    bool result = true;
    do {
        size_t rd = read(f1,buf,copyBuf);
    if ( !rd ) break;
    size_t wr = write(f2,buf,rd);
    if ( wr != rd ) {
        result = false;
        break;
        }
    } while ( true );
    close(f1);
    close(f2);
        // copy file attributes:
    struct stat sstat;
    struct stat dstat;
    stat(sfn,&sstat);
    stat(dfn,&dstat);
    struct utimbuf dtim;
    dtim.actime  = dstat.st_atime;   // not changed!
    dtim.modtime = sstat.st_mtime;
    return( utime(dfn,&dtim) == 0 && result );
}

void createDirectory ( const char *dir )
{
    if ( !dir || !dir[0] ) return;
    LocalPath(udir,dir);
    mkdir(udir,NEW_DIRECTORY_MODE);
}

void deleteFile ( const char *path )
{
    if ( !path || !path[0] ) return;
    LocalPath(upath,path);
    unlink(upath);
}

#endif
