#ifndef _FLTOPTS_HPP
#define _FLTOPTS_HPP

#include <limits.h>
#include <math.h>

// different tricks to increase speed on Pentium

inline float floatMax( float a, float b ) {return ( a>b ? a : b);}
inline float floatMin( float a, float b ) {return ( a<b ? a : b);}

#define saturateAbove saturateMin 
#define saturateBelow saturateMax 

inline void saturateMin( float &a, float b ) {if( a>b ) a=b;}
inline void saturateMax( float &a, float b ) {if( a<b ) a=b;}

inline void saturateMin( int &a, int b ) {if( a>b ) a=b;}
inline void saturateMax( int &a, int b ) {if( a<b ) a=b;}

#ifdef __ICL

#define floatMaxFast floatMax
#define floatMinFast floatMin

inline void saturate( float &a, float min, float max )
{
	if( a<min ) a=min;
	if( a>max ) a=max;
}

#define saturateFast saturate

// int macros

inline void saturate( int &a, int min, int max )
{
	if( a<min ) a=min;
	if( a>max ) a=max;
}

#else

inline float floatMaxFast( double a, double b )
{
	return float( (a+b+fabs(a-b))*0.5 );
}
inline float floatMinFast( double a, double b )
{
	return float( (a+b-fabs(a-b))*0.5 );
}

inline void saturate( float &a, float min, float max )
{
	if( a<min ) a=min;
	else if( a>max ) a=max;
}

inline void saturateFast( float &a, float min, float max )
{
	a=floatMaxFast(a,min);
	a=floatMinFast(a,max);
}

// int macros

inline void saturate( int &a, int min, int max )
{
	if( a<min ) a=min;
	else if( a>max ) a=max;
}

#endif


static const float Inv2=0.5;
static const float Snapper=3<<22;

#if _MSC_VER


inline float fastRound( float x )
{
	// x must be: -1<<21 < x < 1<<21
	volatile float retVal;
	retVal=x+Snapper;
	retVal-=Snapper;
	return retVal;
}

// fast convert float to int - take care to have rounding mode set

inline int toLargeInt( float f )
{
	int retVal;
	_asm
	{
		fld f;
		fistp retVal;
	}
	return retVal;
}


#else

// portable (non-optimized) version

inline float fastRound( float x ) {return (int)x;}
inline int toLargeInt( float f ){return (int)f;}

#endif

// by Vlad Kaipetsky <vlad@TEQUE.CO.UK>

inline int toInt( float fval )
{
	#if _DEBUG
	Assert( fabs(fval)<=0x003fffff );
	#endif
	fval += Snapper;
	return ( (*(int *)&fval)&0x007fffff ) - 0x00400000;
}

inline int toInt( double f ){return toInt(float(f));}


#define toIntFloor(x) toInt((x)-Inv2)
#define toIntCeil(x) toInt((x)+Inv2)


#define fastCeil(x) fastRound((x)+Inv2)
#define fastFloor(x) fastRound((x)-Inv2)


inline float fastFmod( float x, const float n )
{ // n is often constant expression
	x*=1/n;
	x-=toIntFloor(x); // nearest int
	return x*n;
}

#define FP_BITS(fp) (*(DWORD *)&(fp))
#define FP_ABS_BITS(fp) (FP_BITS(fp)&0x7FFFFFFF)
#define FP_SIGN_BIT(fp) (FP_BITS(fp)&0x80000000)
#define FP_ONE_BITS 0x3F800000

// float tricks (by NVIDIA?)

inline float FastInv( float p )
{
	// about 6b precision?
	int _i = 2 * FP_ONE_BITS - *(int *)&(p);
	float r = *(float *)&_i;
	return r * (2.0f - (p) * r);
}

// some tricks to have more safe classes

// this macro should be used on the end of class definition
// copy constructor and operator are declared but not defined

#define NoCopy(type) \
	private: \
	type( const type & ); \
	type &operator = ( const type & );

// some tricks to speed-up class handling

template <class Type>
inline void swap( Type &a, Type &b ){Type p=a;a=b;b=p;}

//inline void *operator new ( size_t len, void *placement ){return placement;}

inline char myLower( char c )
{
	c+=char(CHAR_MIN-'A');
	if( c<=CHAR_MIN+'Z'-'A' ) c+='a'-'A';
	c-=char(CHAR_MIN-'A');
	return c;
}

inline char myUpper( char c )
{
	c+=char(CHAR_MIN-'a');
	if( c<=CHAR_MIN+'z'-'a' ) c+='A'-'a';
	c-=char(CHAR_MIN-'a');
	return c;
}

template <int Size>
class SizedMemory
{
	char _data[Size];
};

#define FastCpy(dst,src,size) \
	*(SizedMemory<size>*)(dst)=*(SizedMemory<size>*)(src)

// PIII prefetch instructions

// off must be <0x80
// adr should be pointer or reference

//                     0F1800         prefetchnta  BYTE PTR [eax]      1                                   
//                     0F1808         prefetcht0  BYTE PTR [eax]       1                                   
//                     0F184020       prefetchnta  BYTE PTR [eax+020h] 1                                   
//                     0F184040       prefetchnta  BYTE PTR [eax+040h] 1                                   


#if _PIII

#define PrefetchNTAOff(adr,off) \
__asm mov eax,adr \
__asm __emit 0x0f \
__asm __emit 0x18 \
__asm __emit 0x40 \
__asm __emit off

#define PrefetchNTA(adr) \
__asm mov eax,adr \
__asm __emit 0x0f \
__asm __emit 0x18 \
__asm __emit 0x00

#define PrefetchT0Off(adr,off) \
__asm mov eax,adr \
__asm __emit 0x0f \
__asm __emit 0x18 \
__asm __emit 0x48 \
__asm __emit off

#define PrefetchT0(adr) \
__asm mov eax,adr \
__asm __emit 0x0f \
__asm __emit 0x18 \
__asm __emit 0x08

#else

// no prefetch on PII

#define PrefetchNTAOff(adr,off)
#define PrefetchNTA(adr)
#define PrefetchT0Off(adr,off)
#define PrefetchT0(adr)

#endif

#endif

