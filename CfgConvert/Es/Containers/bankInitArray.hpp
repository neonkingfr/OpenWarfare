#ifdef _MSC_VER
#pragma once
#endif

#ifndef _BANK_INIT_ARRAY_HPP
#define _BANK_INIT_ARRAY_HPP

#include <string.h>
#include <Es/Framework/debugLog.hpp>
#include <Es/Containers/array.hpp>
#include <Es/Containers/bankArray.hpp>

template <class Type,class Traits=BankTraits<Type> >
class BankInitArray: public Traits::ContainerType
{
	typedef typename Traits::NameType TypeNameType;

	protected:
	int Find( TypeNameType name );
	int Load( TypeNameType name );
	
	public:
	Type *New( TypeNameType name );
};

template <class Type,class Traits>
int BankInitArray<Type,Traits>::Find( TypeNameType name )
{
	for( int i=0; i<Size(); i++ )
	{
		Type *ii = Get(i);
		if( ii && !Traits::CompareNames(ii->GetName(),name) ) return i;
	}
	return -1;
}

template <class Type,class Traits>
int BankInitArray<Type,Traits>::Load( TypeNameType name )
{
	Type *item = new Type();
	item->Init(name);
	return Add(item);
}

template <class Type,class Traits>
Type *BankInitArray<Type,Traits>::New( TypeNameType name )
{
	//if( !name || !*name ) return NULL; // no name - return NULL
	int index=Find(name);
	if( index<0 ) index=Load(name);
	if( index<0 ) return NULL;
	return Get(index);
}

#endif
