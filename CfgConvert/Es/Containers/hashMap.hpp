#ifdef _MSC_VER
#pragma once
#endif

#ifndef __HASHMAP_HPP
#define __HASHMAP_HPP

#include <Es/Framework/debugLog.hpp>
#include <Es/Containers/typeOpts.hpp>
#include <Es/Common/fltopts.hpp>
#include <Es/Types/pointers.hpp>

#ifdef _CPPRTTI
	#include "typeinfo.h"
#endif

#include <Es/Containers/array.hpp>

const int CoefExpand = 32;

//! define properties of class stored in MapStringToClass
template <class Type>
struct MapClassTraits
{
	//! key type
	typedef const char *KeyType;
	//! calculate hash value
	static unsigned int CalculateHashValue(KeyType key)
	{
		unsigned int nHash = 0;
		while (*key)
			nHash = nHash*32 + nHash + *key++;
		return nHash;
	}

	//! compare keys, return negative when k1<k2, positive when k1>k2, zero when equal
	static int CmpKey(KeyType k1, KeyType k2)
	{
		return strcmp(k1,k2);
	}

  /// get a key for given a item
  static KeyType GetKey(const Type &item) {return item.GetKey();}
};

//! Hashing table
/*!
  This class implements a hashing method called hashing with chains. If we are inserting new items
  then the hash table is being resized, if we are removing items the size of the hash table remains
  unchanged. Class Container must have the ability to store class Type values. Class Type must
  implement following functions: GetKey(). Class Container must implement following functions: Size(),
  MaxSize(), Realloc(int), Add(Type). A good example is to use RString as a Type and
  AutoArray<RString> as a Container.
*/
template<class Type, class Container, class Traits=MapClassTraits<Type> >
class MapStringToClass
{
	typedef typename Traits::KeyType KeyType;

protected:
  //! Hash table like an array of containers.
	Container *_hashTable;
  //! Size of the hash table in containers (or number of containers in the array if you please...).
	int _tableSize;
  //! Number of items in the hash table.
	int _count;
  //! Declaration of static value for each type which will be considered as a NULL value.
	static Type _null;
  //! Constant which defines default size of the table.
	enum {DefTableSize = 15};
public:
	class Iterator
	{
	protected:
		int _table;
		int _item;
		const MapStringToClass<Type, Container, Traits> *_base;

		void FindNextValid()
		{
			while (_table < _base->NTables() && _item >= _base->GetTable(_table).Size())
			{
				_table++;
				_item = 0;
			}
			
		}
	public:
		Iterator(const MapStringToClass<Type, Container, Traits> &base)
		{
			_table = 0; _item = 0; _base = &base;
			FindNextValid();
		}
		operator bool () const {return _table < _base->NTables();}
		void operator ++ ()
		{
			if (_table >= _base->NTables()) return;
			++_item;
			FindNextValid();
		}
		const Type &operator * ()
		{
			return _base->GetTable(_table)[_item];
		}
	};

	//! Constructor which will initialize hash table to empty table and set the size of the table (optional).
  /*!
    \param tableSize Size of the hash table.
  */
	MapStringToClass(int tableSize = DefTableSize)
	{
		_hashTable = NULL;
		Init(tableSize);
	}
  //! Destructor will free the allocated memory for the hash table.
	~MapStringToClass() { Clear(); }
  //! Initializes hash table.
  /*!
    This method will free previousely allocated space.
    \param tableSize New size of the hash table.
  */
	void Init(int tableSize);
  //! Free hash table from memory.
	void Clear();
  //! Rebuilding of the hash table with new specified size.
  /*!
    Previous data will be rehashed.
    \param tableSize Size of the new hash table.
  */
	void Rebuild(int tableSize);
  //! Calling of callback function for each item in the hash table.
  /*!
    Using this method you can change both class Type members and this class MapStringToClass members.
    \param Func Callback function to call.
    \param context Parameters data to pass to the callback function.
  */
	void ForEach(void (*Func)(Type &, MapStringToClass *, void *), void *context=NULL);
  //! Calling of callback function for each item in the hash table.
  /*!
    Using this method you cannot change neither class Type members nor this class MapStringToClass members.
    \param Func Callback function to call.
    \param context Parameters data to pass to the callback function.
  */
	void ForEach(void (*Func)(const Type &, const MapStringToClass *, void *), void *context=NULL) const;
  //! According to specified key this method will return desired item.
  /*!
    Using this method you cannot change retrieved item.
    \param key The key according to find the item.
    \return Desired item.
  */
	const Type &Get(KeyType key) const;
  //! According to specified key this method will return desired item.
  /*!
    Using this method you can change retrieved item.
    \param key The key according to find the item.
    \return Desired item.
  */
	Type &Set(KeyType key);
  //! Using this operator you can acces desired item directly by name.
	Type &operator [] (KeyType key) {return Set(key);}
  //! Using this operator you can acces desired item directly by name.
	const Type &operator [] (KeyType key) const {return Get(key);}
	//! Adding of specified item into hash table.
  /*!
    Item with equal key will be replaced. In case there are too items on average position, then
    it will resize entire table.
    \param value Value to add.
    \return Index of the container the item have been added to or -1 in case some error occured.
  */
	int Add(const Type &value);
  //! Removing of specified item from the hash table.
  /*!
    In case there is no item with such key in the hash table, nothing will happen. Note that size
    of the hash table (number of containers) will not be changed.
    \param key Key which specifies the item to remove.
  */
	void Remove(KeyType key);
  //! Comparing of specified value to a NULL value.
  /*!
    \param value Value to be compared.
    \return True in case value is NULL, false otherwise.
  */
	static bool IsNull(const Type &value) {return &value == &_null;}
  //! Comparing of specified value to a NULL value.
  /*!
    \param value Value to be compared.
    \return False in case value is NULL, true otherwise.
  */
	static bool NotNull(const Type &value) {return &value != &_null;}
  //! Retrieving of null value.
  /*!
    \return Instance of type Type from heap which is considered as a NULL.
  */
	static Type &Null() {return _null;}

public:
// Direct access - used only for serialization
  //! Retrieves number of items in the hash table.
  /*!
    \return Number of items in the hash table.
  */
	int NItems() const {return _count;}
  //! Retrieves number of containers.
  /*!
    \return Number of containers.
  */
	int NTables() const {return _hashTable ? _tableSize : 0;}
  //! Retrieves desired container.
  /*!
    \param i Index of the container to return.
    \return Desired container.
  */
	Container &GetTable(int i) const {return _hashTable[i];}

// Implementation
protected:
  //! This method will hash the key into integer number from range <0 - n>
  /*!
    Note that in case n <= 0 it will return number from range <0 - _tableSize>
    \param key String to extract the key from.
    \param n Upper bound of the range the desired value to be within.
    \return Index of the desired container.
  */
	int HashKey(KeyType key, int n = 0) const;

	ClassIsMovableZeroed(MapStringToClass);
};

template<class Type, class Container,class Traits>
Type MapStringToClass<Type, Container, Traits>::_null;

template<class Type, class Container,class Traits>
int MapStringToClass<Type, Container, Traits>::HashKey(KeyType key, int n) const
{
	if (n <= 0) n = _tableSize;
	unsigned int nHash = Traits::CalculateHashValue(key);
	// well desinged hash function should be different
	// for strings of any length that start with different letters
	// this forces nMash multiplier not equal to power of 2
	// in this case actual nHash multiplier is 33, which is OK
	return nHash % n;
}

template<class Type, class Container,class Traits>
void MapStringToClass<Type, Container, Traits>::Init(int tableSize)
{
	Clear();
	_tableSize = tableSize;
}

template<class Type, class Container,class Traits>
void MapStringToClass<Type, Container, Traits>::Clear()
{
	if (_hashTable)
	{
		delete [] _hashTable;
		_hashTable = NULL;
	}
	_count = 0;
}

template<class Type, class Container,class Traits>
void MapStringToClass<Type, Container, Traits>::Rebuild(int tableSize)
{
//LogF("Rebuilding hash table: %d items, new size %d", _count, tableSize);
  // In case hash table is empty
	if (!_hashTable)
	{
		_tableSize = tableSize;
		return;
	}
	Container *newTable = new Container[tableSize];
  // For all old containers
	for (int i=0; i<_tableSize; i++)
	{
		const Container &container = _hashTable[i];
    // For all items in container
		for (int j=0; j<container.Size(); j++)
		{
      // Get single item from container
			const Type &item = container[j];
      // Get new position of the item
			int nHashKey = HashKey(Traits::GetKey(item), tableSize);
      // Get reference to the destination container
			Container &dest=newTable[nHashKey];
      // In case there is not enough space in the destination, then resize it (double the current size)
			int need=dest.Size()+1;
			int haveSize=dest.MaxSize();
			if( need>haveSize )
			{
				if( haveSize<1 ) haveSize=1;
				while( need>haveSize ) haveSize=haveSize*2;
				dest.Realloc(haveSize);
			}
      // Add the item itself
			dest.Add(item);
		}
	}
  // Delete the previous hash table and assign the new one
	delete [] _hashTable;
	_hashTable = newTable;
	_tableSize = tableSize;
}


template<class Type, class Container,class Traits>
void MapStringToClass<Type, Container, Traits>::ForEach
(
	void (*Func)(Type &, MapStringToClass *, void *), void *context
)
{
	if( !_hashTable ) 
  {
		// TODO: verify if this is a bug
		LogF("Caution: no hash table");
		return;
  }
RestartI:
	int n = _tableSize;
	for (int i=0; i<n; i++)
	{
		Container &container = _hashTable[i];
RestartJ:
		int m = container.Size();
		for (int j=0; j<m; j++)
		{
			Type &item = container[j];
			Func(item, this, context);
			if (_tableSize != n) goto RestartI;
			if (container.Size() != m) goto RestartJ;
		}
	}
}

template<class Type, class Container,class Traits>
void MapStringToClass<Type, Container, Traits>::ForEach
(
	void (*Func)(const Type &, const MapStringToClass *, void *), void *context
) const
{
	if( !_hashTable )
	{
		// TODO: verify if this is a bug
		LogF("Caution: no hash table");
		return;
	}
	int n = _tableSize;
	for (int i=0; i<n; i++)
	{
		const Container &container = _hashTable[i];
		int m = container.Size();
		for (int j=0; j<m; j++)
		{
			const Type &item = container[j];
			Func(item, this, context);
		}
	}
}

template<class Type, class Container,class Traits>
const Type &MapStringToClass<Type, Container, Traits>::Get(KeyType key) const
{
	if (_count <= 0) return Null();
	Assert(_hashTable);

	int nHashKey = HashKey(key);
	for (int i=0; i<_hashTable[nHashKey].Size(); i++)
	{
		const Type &item = _hashTable[nHashKey][i];
		if (Traits::CmpKey(Traits::GetKey(item), key) == 0)
			return item;
	}
	return Null();
}

template<class Type, class Container,class Traits>
Type &MapStringToClass<Type, Container, Traits>::Set(KeyType key)
{
	if (_count <= 0) return Null();
	Assert(_hashTable);

	int nHashKey = HashKey(key);
	for (int i=0; i<_hashTable[nHashKey].Size(); i++)
	{
		Type &item = _hashTable[nHashKey][i];
		if (Traits::CmpKey(Traits::GetKey(item), key) == 0)
			return item;
	}
	return Null();
}

template<class Type, class Container,class Traits>
int MapStringToClass<Type, Container, Traits>::Add(const Type &value)
{

  // Retrieve the key of the item
	KeyType key = Traits::GetKey(value);
	Type &old = Set(key);
  // In case there exists some value already, replace it
	if (!IsNull(old))
	{
		// replace old value
		old = value;
		return HashKey(key);
	}
  // Retrieve maximum of acceptable number of items in the hash table
	int maxCount = CoefExpand * _tableSize;
  // In case we will have to resize the hash table
	if (_count + 1 > maxCount)
	{
		int tableSize = _tableSize + 1;
		while (_count + 1 > maxCount)
		{
			tableSize *= 2;
			maxCount = CoefExpand * (tableSize - 1);
		}
		Rebuild(tableSize - 1);
	}
  // In case the hash table is empty then create array of containers
	if (!_hashTable)
	{
		Assert(_tableSize > 0);
		if (_tableSize <= 0)
			return -1;
		_hashTable = new Container[_tableSize];
	}
  // Retrieve number of desired container from the key
	int nHashKey = HashKey(key);
  // Get desired container
	Container &dest=_hashTable[nHashKey];
  // In case there is not enough space in the destination, then resize it (double the current size)
	int need=dest.Size()+1;
	int haveSize=dest.MaxSize();
	if( need>haveSize )
	{
		if( haveSize<1 ) haveSize=1;
		while( need>haveSize ) haveSize=haveSize*2;
		dest.Realloc(haveSize);
	}
  // Add the item Eventually
	dest.Add(value);
	// Increase number of items and return the index of the changed container
	_count++;
	return nHashKey;
}

template<class Type, class Container,class Traits>
void MapStringToClass<Type, Container, Traits>::Remove(KeyType key)
{
	if (_count <= 0) return;
	Assert(_hashTable);

	int nHashKey = HashKey(key);
	for (int i=0; i<_hashTable[nHashKey].Size(); i++)
	{
		Type &item = _hashTable[nHashKey][i];
		if (Traits::CmpKey(Traits::GetKey(item), key) == 0)
		{
			_hashTable[nHashKey].Delete(i, 1);
			_count--;
			Assert(_count >= 0);
			if (_count == 0)
				Clear();
			return;
		}
	}
}

#endif
