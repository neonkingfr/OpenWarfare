#ifdef _MSC_VER
#pragma once
#endif

#ifndef _COMPACT_BUF_HPP
#define _COMPACT_BUF_HPP

#include <Es/Containers/typeOpts.hpp>

#define CB_LOG_NEW_DELETE 0

#pragma warning(disable:4200)

// note: new and delete must never be used on CompactBuffer
template <class Type>
class CompactBuffer
{
	// include RefCount functionality to avoid virtual functions table and delete

  private:
  mutable int _count;

  public:
  int AddRef() const
	{
		return ++_count;
	}
  int Release() const
  {
    int ret=--_count;
    if( ret==0 ) Delete();
    return ret;
  }
	int RefCounter() const {return _count;}

	// buffer implementation
	private:
	int _size;
	Type _data[1];

	public:
	explicit CompactBuffer( int count )
	{
		_count=0;
		_size=count;
		ConstructArray(_data,_size);
	}
	CompactBuffer( Type const *data, int count )
	{
		_count=0;
		_size=count;
		CopyConstruct(_data,data,_size);
	}
	~CompactBuffer()
	{
		DestructArray(_data,_size);
	}

	private: // disable copy
	void operator = ( const CompactBuffer &src );
	CompactBuffer( const CompactBuffer &src );

	#include <Es/Memory/normalNew.hpp>

	void *operator new( size_t size ) {Fail("new: Should be never used.");}
	void operator delete( void *mem ) {Fail("delete: Should be never used.");}

	inline void *operator new(size_t size, void *placement) {return placement;}
	inline void operator delete(void *mem, void *placement) {}

	#include <Es/Memory/debugNew.hpp>

	public:

	Type *Data() {return _data;}
	const Type *Data() const {return _data;}
	int Size() const {return _size;}

	static CompactBuffer *New( int n )
	{
		Assert( n>0 );
		int size = sizeof(CompactBuffer)-sizeof(Type)+sizeof(Type)*n;

		//#if ALLOC_DEBUGGER && defined _CPPRTTI
		//#else
		CompactBuffer *buffer=(CompactBuffer *)new char[size];
		//#endif

		#include <Es/Memory/normalNew.hpp>

		// placement new required
		new(buffer) CompactBuffer(n);

		#include <Es/Memory/debugNew.hpp>

		#if CB_LOG_NEW_DELETE
		LogF("New %x: Size %d",buffer,size);
		#endif
		return buffer;
	}

	void Delete() const
	{
		#if CB_LOG_NEW_DELETE
		LogF
		(
			"Delete %x: Size %d",
			this,_size*sizeof(Type)+sizeof(CompactBuffer)-sizeof(Type)
		);
		#endif
		this->~CompactBuffer();
		delete[] (char *)this;
	}

	static CompactBuffer *Copy( CompactBuffer *str )
	{
		int len=str->Size();
		Assert(len>0);
		CompactBuffer *buffer=(CompactBuffer *)new char
		[
			sizeof(CompactBuffer)-sizeof(Type)+sizeof(Type)*len
		];
		new(buffer) CompactBuffer(str->Data(),len);
		return buffer;
	}
	static CompactBuffer *Copy( CompactBuffer *str, int len )
	{
		if( len>str->Size() ) len=str->Size();
		Assert(len>0);
		int size = sizeof(CompactBuffer)-sizeof(Type)+sizeof(Type)*len;
		CompactBuffer *buffer=(CompactBuffer *)new char[size];
		#include <Es/Memory/normalNew.hpp>
		new(buffer) CompactBuffer(str->Data(),len);
		#include <Es/Memory/debugNew.hpp>
		#if CB_LOG_NEW_DELETE
		LogF("Copy %x: Size %d",buffer,size);
		#endif
		return buffer;
	}
	static CompactBuffer *Copy( Type const *data, int len )
	{
		Assert(len>0);
		int size = sizeof(CompactBuffer)-sizeof(Type)+sizeof(Type)*len;
    #include <Es/Memory/normalNew.hpp>
		CompactBuffer *buffer=(CompactBuffer *)new char[size];
		new(buffer) CompactBuffer(data,len);
		#include <Es/Memory/debugNew.hpp>
		#if CB_LOG_NEW_DELETE
		LogF("Copy %x: Size %d",buffer,size);
		#endif
		return buffer;
	}
};

//#pragma warning(default:4200)

#include <Es/Memory/debugNew.hpp>

typedef CompactBuffer<char> CompactString;


#endif

