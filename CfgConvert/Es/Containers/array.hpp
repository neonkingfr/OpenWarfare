#ifdef _MSC_VER
#pragma once
#endif

#ifndef __ARRAY_HPP
#define __ARRAY_HPP

#include <Es/Framework/debugLog.hpp>
#include <Es/Containers/typeOpts.hpp>
#include <Es/Types/pointers.hpp>

#ifdef _CPPRTTI
	#include "typeinfo.h"
#endif

#include <Es/Memory/memAlloc.hpp>
#include <Es/Memory/normalNew.hpp>

//! Template class suitable for advanced array management.
/*!
  If you need to make an array of some class, you must specify the type
  of the class using predefined macros (See the macros TypeIs*(...)).
  It is a custom to write it right beneath the class declaration.
*/
template<class Type, class Allocator=MemAllocD>
class AutoArray
{
  typedef ConstructTraits<Type> CTraits;

protected:
  //! Allocator which will be used for this class. It is set to MemAllocD by default.
	Allocator _alloc; // size of _alloc is often 0
  //! Pointer to array data
	Type *_data;
  //! Number of allocated items. This value must be greater or equal to _n. \sa _n
	int _maxN;
  //! Number of items in array. This value must be less or equal to _maxN. \sa _maxN
	int _n;
  //! Function called usually by constructors.
	void DoConstruct();
  //! Function called usually by destructors.
	void DoDestruct();
public:
  //! Minimum of items will be added during resizing.
	enum {MinGrow=32};
  //! Implicit constructor.
	AutoArray();
  //! Constructor which will allocate space for n items.
  /*!
    \param n Number of items to be allocated.
  */
	explicit AutoArray( int n );
  //! Constructor which will set a particular storage.
  /*!
    \param alloc Storage to be set.
  */
	explicit AutoArray( const Allocator &alloc )
	{
		DoConstruct();
		SetStorage(alloc);
	}
  //! Constructor which will copy n items from another array.
  /*!
    \param src Source array to be copied from.
    \param n Number of items to copy.
  */
	AutoArray( const Type *src, int n );
  //! Constructor which will copy whole source array.
  /*!
    \param src Source array to be copied.
  */
	AutoArray( const AutoArray &src );
  //! Guess what ;-)
	~AutoArray();
  //! Copying of one array to another.
	void operator = ( const AutoArray &src );
  //! Fast moving of one array into another.
  /*!
    After calling this function source array will be destroyed.
    \param src Source array.
  */
	void Move( AutoArray &src );
  //! Destruction and deallocation
	void Clear(){DoDestruct();}
  //! Copying of n items from the source array.
  /*!
    \param src Source array.
    \param n Number of items.
  */
	void Copy( const Type *src, int n );
  //! Using allocator of this array this function allocates a specified memory.
  /*!
    \param n Number of items to be allocated.
    \return Reference to allocated memory.
  */
	Type *MemAlloc( int &n )
	{
		int size=n*sizeof(Type),size0=size;
		#if ALLOC_DEBUGGER && defined _CPPRTTI
		Type *ret=(Type *)_alloc.Alloc(size,__FILE__,__LINE__,typeid(Type).name());
		//return (Type *)new(FileLine(typeid(Type).name()),sizeof(Type)) char[n*sizeof(Type)];
		#else
		Type *ret=(Type *)_alloc.Alloc(size);
		#endif
		if( size!=size0 ) n=size/sizeof(Type);
		return ret;
	}
  //! Setting of a particular storage.
  /*!
    \param alloc Storage to be set.
  */
	void SetStorage( const Allocator &alloc )
	{
		_alloc=alloc;
	}
  //! This method will destroy current allocator of this object and replaces it with default memory management.
  /*!
    Data remains valid and unchanged
  */
	void UnlinkStorage()
	{
		_alloc.Unlink(_data);
	}
  //! Using allocator of this array this function deallocates a specified memory.
  /*!
    \param mem Memory to be deallocated.
    \n Number of items to be deleted.
  */
	void MemFree( Type *mem, int n ) {_alloc.Free(mem,n*sizeof(Type));}
	//! Allocation of space for n items.
  /*!
    \param n Number of items the space will be allocated for.
  */
	void Init( int n );
  //! Resizing of array to n+1 items.
  /*!
    Access(i) == Resize(i+1)
    \param n Last item which can be accessed.
    \sa Resize()
  */
	void Access( int n );
  //! Reallocation of array only in case actual size is smaller than sizeNeed. This function never shrinks the size of the array. Therefore is it more faster than Realloc(int int).
  /*!
    \param sizeNeed Lower bound of the interval.
    \param sizeWan Upper bound of the interval.
    \sa Realloc
  */
	void Reserve( int sizeNeed, int sizeWant );
  //! Reallocation of array only in case actual size is out of interval <sizeNeed, sizeWant>.
  /*!
    In case there will be a new allocation, new size will be sizeWant.
    \param sizeNeed Lower bound of the interval.
    \param sizeWan Upper bound of the interval.
    \sa Reserve
  */
	void Realloc( int sizeNeed, int sizeWant );
  //! Reallocation of array to new length specified by size.
  /*!
    \param size New number of items to be allocated.
  */
	void Realloc( int size );
  //! Resizing of this array to n items.
  /*!
    In case we will need new allocation there will be allocated at least MinGrow items.
    \param n New number of items to be allocated.
  */
	void Resize( int n );
  //! Resize the array w/o construction/destruction control. Dangerous! You MUST know what are you doing!
  /*!
    Does no array-resizing!
    \param n New number of controlled items in the array.
  */
    void UnsafeResize( int n );

  //! Shrink the length of the array just to fit actual number of items.
  /*!
    \sa _n, _maxN
  */
	void Compact(){Realloc(_n);}
  //! This function will call first destructors on all items and consequently default constructors.
  /*!
    Note that this method is currently used nowhere.
  */
	void InitEmpty(){DestructArray(_data,_n);ConstructArray(_data,_n);}
  //! Adding of an item without resizing the field.
  /*!
    \param src Source item to be added.
    \return New number of items in the array.
  */
	int AddFast(const Type &src)
	{
		Assert( _n<_maxN );
		CTraits::CopyConstruct(_data[_n],src);
		return _n++;
	}
  //! Adding of an item without resizing the field.
  /*!
    \return New number of items in the array.
  */
	int AddFast()
	{
		Assert( _n<_maxN );
		CTraits::Construct(_data[_n]);
		return _n++;
	}
  //! Adding of an item with resizing the field.
  /*!
    \param src Source item to be added.
    \return New number of items in the array.
  */
	int Add(const Type &src);
  //! Adding of an item with resizing the field.
  /*!
    \return New number of items in the array.
  */
	int Add();
  //! Adding of an item with resizing the field.
  /*!
    \return Reference to new item.
  */
	Type &Append() {return Set(Add());}
#ifdef _WIN32
  //! Deleting of specified number of items at the specified position.
  /*!
    \param index Index where the data to be deleted lies.
    \param count Number of items to be deleted.
  */
  void Delete( int index, int count=1 );
#else
  //! Deleting of specified number of items at the specified position.
  /*!
    \param index Index where the data to be deleted lies.
    \param count Number of items to be deleted.
  */
  void Delete( int index, int count );
  //! Deleting one item at the specified position.
  /*!
    \param index Index where the data to be deleted lies.
  */
  inline void Delete( int index )
  {
      Delete(index,1);
  }
#endif
  //! Inserting of one item at specified position.
  /*!
    \param index Index to position where new item should be inserted.
    \param src Item to be inserted.
  */
	void Insert( int index, const Type &src );
  //! Inserting of one position at specified position.
  /*!
    \param index Index to position where new item should be inserted.
  */
	void Insert( int index );
  //! Setting of specified item.
  /*!
    Returned item can be modified.
    \param i Index to desirable item.
    \return Desirable item
  */
	Type &Set( int i )
	{
		AssertDebug( i>=0 && i<_n );
		return _data[i];
	}
  //! Getting of specified item.
  /*!
    Returned item cannot be modified.
    \param i Index to desirable item.
    \return Desirable item.
  */
	const Type &Get( int i ) const
	{
		AssertDebug( i>=0 && i<_n );
		return _data[i];
	}
  //! Defined operator [] for modifying a value.
	Type &operator [] ( int i ) {return Set(i);}
  //! Defined operator [] for reading a value.
	const Type &operator [] ( int i ) const {return Get(i);}
  //! Returns the reference to array data for reading purpose.
	const Type *Data() const {return _data;}
  //! Returns the reference to array data for modifying purpose.
	Type *Data() {return _data;}
	typedef Type DataType;
  //! Retrieving a number of items used in the array.
	int Size() const {return _n;}
  //! Retrieving already allocated size of the array.
	int MaxSize() const {return _maxN;}
  //! QuickSort
  /*!
    Note that QSortBin is very dangerous. Use QSort from <class/QSort.hpp> if possible.
    \param compare Reference to function which compare 2 items v0 and v1
    \param count Number of items to sort. -1 means that whole array should be sorted.
  */
	void QSortBin( int (*compare)( const void *v0, const void *v1 ), int count=-1 );

	ClassIsMovable(AutoArray);
};

#include <Es/Memory/debugNew.hpp>

template<class Type,class Allocator>
void AutoArray<Type,Allocator>::DoConstruct()
{
	_data=NULL,_n=_maxN=0;
}

template<class Type,class Allocator>
AutoArray<Type,Allocator>::~AutoArray()
{
	DoDestruct();
}

template<class Type,class Allocator>
AutoArray<Type,Allocator>::AutoArray(){DoConstruct();}

template<class Type,class Allocator>
AutoArray<Type,Allocator>::AutoArray( int n ){DoConstruct();Realloc(n);}

template<class Type,class Allocator>
AutoArray<Type,Allocator>::AutoArray( const Type *src, int n ){DoConstruct();Copy(src,n);}

template<class Type,class Allocator>
AutoArray<Type,Allocator>::AutoArray( const AutoArray &src )
//:_alloc(src._alloc)
// note: original allocator is kept
{
	DoConstruct();Copy(src.Data(),src.Size());
}

template<class Type,class Allocator>
void AutoArray<Type,Allocator>::operator = ( const AutoArray &src )
{
	Copy(src.Data(),src.Size());
	//_alloc=src._alloc;
	// note: original allocator is kept
}

template<class Type,class Allocator>
void AutoArray<Type,Allocator>::Move( AutoArray &src )
{
	Clear(); // free any data in target
  // transfer control from src to target
	_data=src._data;
	_n=src._n;
	_maxN=src._maxN;
	_alloc=src._alloc;
	// mark src as clear
	src._data=NULL;
	src._n=0;
	src._maxN=0;
}

template<class Type,class Allocator>
void AutoArray<Type,Allocator>::Copy( const Type *src, int n )
{
	Reserve(n,n);
	Resize(n);
	CTraits::CopyData(_data,src,_n);
}

template<class Type,class Allocator>
void AutoArray<Type,Allocator>::Realloc( int size )
{
	if( _maxN!=size )
	{
		Type *newData=MemAlloc(size);
		if( _data )
		{
			if( _n>size ) Resize(size);
			// move data
      CTraits::MoveData(newData,_data,_n);
			MemFree(_data,_maxN);
		}
		_data=newData;
		_maxN=size;
	}
}

template<class Type,class Allocator>
void AutoArray<Type,Allocator>::Realloc( int sizeNeed, int sizeWant )
{
	if( _maxN<sizeNeed || _maxN>sizeWant )
	{
		Realloc(sizeWant);
	}
}

template<class Type,class Allocator>
inline void AutoArray<Type,Allocator>::Reserve( int sizeNeed, int sizeWant )
{
	if( _maxN<sizeNeed ) Realloc(sizeNeed,sizeWant);
}

template<class Type,class Allocator>
inline void AutoArray<Type,Allocator>::Access( int i )
{
	if( _n<i+1 ) Resize(i+1);
}

template<class Type,class Allocator>
void AutoArray<Type,Allocator>::Init( int size )
{
	if( _maxN!=size )
	{
		Clear();
		Realloc(size);
	}
	else
	{
		Resize(0);
	}
}

template<class Type,class Allocator>
inline void AutoArray<Type,Allocator>::Resize( int n )
{
	if( n>_maxN )
	{
		int grow=_maxN;
		//const int maxGrowCount=MaxGrow/sizeof(Type);
		//if( grow>maxGrowCount ) grow=maxGrowCount;
		if( grow<MinGrow ) grow=MinGrow;
		Reserve(n,n-1+grow);
	}
	// change length of controlled sequence
	if( n>_n ) CTraits::ConstructArray(_data+_n,n-_n);
	else if( _n>n ) CTraits::DestructArray(_data+n,_n-n);
	_n=n;
}

template<class Type,class Allocator>
void AutoArray<Type,Allocator>::UnsafeResize( int n )
{
	if( n>_maxN )
	{
		int grow=_maxN;
		if( grow<MinGrow ) grow=MinGrow;
		Reserve(n,n-1+grow);
	}
    if ( (_n = n) < 0 ) _n = 0;
}

template<class Type,class Allocator>
int AutoArray<Type,Allocator>::Add( const Type &src )
{
	if( _n<_maxN )
	{
		return AddFast(src);
	}
	else
	{
		int s=_n;
		Resize(s+1);
		_data[s]=src;
		return s;
	}
}

template<class Type,class Allocator>
int AutoArray<Type,Allocator>::Add()
{
	if( _n<_maxN )
	{
		return AddFast();
	}
	else
	{
		int s=_n;
		Resize(s+1);
		return s;
	}
}

template<class Type,class Allocator>
void AutoArray<Type,Allocator>::Delete(int index, int count)
{
	if( index<0 || index+count>_n )
	{
		Fail("Array index out of range");
		return;
	}
	CTraits::DestructArray(_data+index,count);
	CTraits::DeleteData(_data+index,_n-index,count);
	_n-=count;
}

template<class Type,class Allocator>
void AutoArray<Type,Allocator>::Insert( int index, const Type &src )
{
	Resize(_n+1); // note: _n is changed by Resize
	CTraits::InsertData(_data+index,_n-index);
	CTraits::CopyConstruct(_data[index],src);
}

template<class Type,class Allocator>
void AutoArray<Type,Allocator>::Insert( int index )
{
	Resize(_n+1); // note: _n is changed by Resize
	CTraits::InsertData(_data+index,_n-index);
	CTraits::Construct(_data[index]);
}

template<class Type,class Allocator>
void AutoArray<Type,Allocator>::QSortBin
(
	int (*compare)( const void *v0, const void *v1 ), int count
)
{
	int size=Size();
	if( count<0 ) count=size;
	else if( count>size) count=size;
	qsort(_data,count,sizeof(_data[0]),compare);
}


template<class Type,class Allocator>
void AutoArray<Type,Allocator>::DoDestruct()
{
	Resize(0);
	if( _data )
	{
		MemFree(_data,_maxN);
		_data=NULL,_maxN=0;
	}
}

template <class Type, class Allocator=MemAllocD>
class FindArray: public AutoArray<Type,Allocator>
{
	public:
	int AddUnique( const Type &src );
	int Find( const Type &src ) const;
	bool Delete( const Type &src );
	void Delete( int index ) {AutoArray<Type,Allocator>::Delete(index,1);}
	void DeleteAt( int index ) {AutoArray<Type,Allocator>::Delete(index,1);}

	ClassIsMovable(FindArray);
};

template<class Type,class Allocator>
int FindArray<Type,Allocator>::Find( const Type &src ) const
{
	for( int i=0; i<Size(); i++ ) if( (*this)[i]==src ) return i;
	return -1;
}

template<class Type,class Allocator>
int FindArray<Type,Allocator>::AddUnique( const Type &src )
{
	if( Find(src)>=0 ) return -1;
	int s=Size();
	Resize(s+1);
	(*this)[s]=src;
	return s;
}

template<class Type,class Allocator>
bool FindArray<Type,Allocator>::Delete( const Type &src )
{
	int index=Find(src);
	if( index<0 ) return false;
	Delete(index);
	return true;
}

template <class Type>
struct FindArrayKeyTraits
{
	typedef const Type &KeyType;
	static bool IsEqual(KeyType a, KeyType b)
	{
		return a==b;
	}
	static KeyType GetKey(const Type &a) {return a;}
};

template <class Type, class Traits=FindArrayKeyTraits<Type>, class Allocator=MemAllocD>
class FindArrayKey: public AutoArray<Type,Allocator>
{
	typedef typename Traits::KeyType KeyType;

	public:
	int AddUnique( const Type &src );
	int Find( const Type &src ) const {return FindKey(Traits::GetKey(src));}
	int FindKey(KeyType key) const;
	bool Delete( const Type &src ){return DeleteKey(Traits::GetKey(src));}
	bool DeleteKey(KeyType src);
	void DeleteAt( int index ) {AutoArray<Type,Allocator>::Delete(index,1);}

	ClassIsMovable(FindArrayKey);
};

template<class Type,class Traits,class Allocator>
int FindArrayKey<Type,Traits,Allocator>::FindKey(KeyType key) const
{
	//for( int i=0; i<Size(); i++ ) if( (*this)[i]==src ) return i;
	for( int i=0; i<Size(); i++ ) if( Traits::IsEqual(Traits::GetKey(Get(i)),key) ) return i;
	return -1;
}

template<class Type,class Traits,class Allocator>
int FindArrayKey<Type,Traits,Allocator>::AddUnique( const Type &src )
{
	if( Find(src)>=0 ) return -1;
	int s=Size();
	Resize(s+1);
	(*this)[s]=src;
	return s;
}

template<class Type,class Traits,class Allocator>
bool FindArrayKey<Type,Traits,Allocator>::DeleteKey(KeyType key)
{
	int index=FindKey(key);
	if( index<0 ) return false;
	DeleteAt(index);
	return true;
}

template <class Type,class Allocator=MemAllocD>
class RefArray: public FindArrayKey< Ref<Type>, FindArrayKeyTraits< Ref<Type> >, Allocator >
{
	typedef Ref<Type> RefType;
	typedef FindArrayKey< RefType, FindArrayKeyTraits< RefType >, Allocator > base;

	public:
	bool Delete( const RefType &src ){return base::Delete(src);}
	void Delete(int index){base::DeleteAt(index);}

	ClassIsMovable(RefArray);
};

// HeapArray
// Type is pointer (*, Ptr, Ref etc.)
// need operators <, <= on *Type
template <class Type>
struct HeapTraits
{
	// default traits: Type is pointer to actual value
	static bool IsLess(const Type &a, const Type &b)
	{
		return *a < *b;
	}
	static bool IsLessOrEqual(const Type &a, const Type &b)
	{
		return *a <= *b;
	}
};

template <>
struct HeapTraits<int>
{
	// integer heap traits: Type is actual value
	static bool IsLess(int a, int b) {return a<b;}
	static bool IsLessOrEqual(int a, int b){return a <= b;}
};

template <class Type,class Allocator=MemAllocD,class Traits=HeapTraits<Type> >
class HeapArray: public FindArray<Type,Allocator>
{
public:
	bool HeapRemoveFirst(Type &item);
	void HeapInsert(const Type &item);
	void HeapUpdateUp(const Type &item);
	void HeapDelete(const Type &item);
	void HeapDelete(int index);
protected:
	void Swap(int i, int j);
	int GetParent(int i);
	int GetFirstSon(int i);
	int GetBrother(int i);
	void HeapDown(int i);
	void HeapUp(int i);

	ClassIsMovable(HeapArray);
};

template <class Type,class Allocator,class Traits>
inline void HeapArray<Type,Allocator,Traits>::Swap(int i, int j)
{
	Type temp = Get(i);
	Set(i) = Get(j);
	Set(j) = temp;
}

template <class Type,class Allocator,class Traits>
inline int HeapArray<Type,Allocator,Traits>::GetParent(int i)
{
	if (i <= 0)
		return -1;
	else
		return (i - 1) / 2;
}

template <class Type,class Allocator,class Traits>
inline int HeapArray<Type,Allocator,Traits>::GetFirstSon(int i)
{
	int n = 2 * i + 1;
	if (n >= Size())
		return -1;
	else
		return n;
}

template <class Type,class Allocator,class Traits>
inline int HeapArray<Type,Allocator,Traits>::GetBrother(int i)
{
	int n = i + 1;
	if (n >= Size())
		return -1;
	else
		return n;
}

template <class Type,class Allocator,class Traits>
void HeapArray<Type,Allocator,Traits>::HeapDown(int i)
{
	while (1)
	{
		int n = GetFirstSon(i);
		if (n < 0) return;
		int m = GetBrother(n);
		if (m >= 0)
		{
			if (Traits::IsLess(Get(m),Get(n))) n = m;
		}
		if (Traits::IsLessOrEqual(Get(i),Get(n))) return;
		Swap(i, n);
		i = n;
	}
}

template <class Type,class Allocator,class Traits>
void HeapArray<Type,Allocator,Traits>::HeapUp(int i)
{
	while (1)
	{
		int n = GetParent(i);
		if (n < 0) return;
		if (Traits::IsLessOrEqual(Get(n),Get(i))) return;
		Swap(n, i);
		i = n;
	}
}

template <class Type,class Allocator,class Traits>
bool HeapArray<Type,Allocator,Traits>::HeapRemoveFirst(Type &item)
{
	int n = Size() - 1;
	if (n < 0) return false;
	item = Get(0);
	Swap(0, n);
	Delete(n);
	HeapDown(0);
	return true;
}

template <class Type,class Allocator,class Traits>
void HeapArray<Type,Allocator,Traits>::HeapInsert(const Type &item)
{
	int n = Add(item);
	HeapUp(n);
}

template <class Type,class Allocator,class Traits>
void HeapArray<Type,Allocator,Traits>::HeapUpdateUp(const Type &item)
{
	int n = Find(item);
	if (n >= 0)
		HeapUp(n);
}

template <class Type,class Allocator,class Traits>
void HeapArray<Type,Allocator,Traits>::HeapDelete(const Type &item)
{
	int n = Find(item);
	if (n >= 0)
		HeapDelete(n);
}

template <class Type,class Allocator,class Traits>
void HeapArray<Type,Allocator,Traits>::HeapDelete(int index)
{
	int n = Size() - 1;
	if (index > n) return;
	Swap(index, n);
	Delete(n);

	int parent = GetParent(index);
	if (parent >= 0 && Traits::IsLessOrEqual(Get(index),Get(parent)))
		HeapUp(index);
	else
		HeapDown(index);
}

#endif
