#ifdef _MSC_VER
#pragma once
#endif

#ifndef _BANK_ARRAY_HPP
#define _BANK_ARRAY_HPP

#include <string.h>
#include <Es/Framework/debugLog.hpp>
#include <Es/Containers/array.hpp>

// default traits (used for most types)
template <class Type>
struct BankTraits
{
	// default name is character
	typedef const char *NameType;
	// default name comparison
	static int CompareNames( NameType n1, NameType n2 )
	{
		return strcmpi(n1,n2);
	}
	// default container is RefArray
	typedef RefArray<Type> ContainerType;
};

template <class Type,class Traits=BankTraits<Type> >
class BankArray: public Traits::ContainerType
{
	typedef typename Traits::NameType TypeNameType;
	typedef typename Traits::ContainerType base;

	protected:
	int Find( TypeNameType name );
	int Load( TypeNameType name );
	
	public:
	Type *New( TypeNameType name );
};

template <class Type,class Traits>
int BankArray<Type,Traits>::Find( TypeNameType name )
{
	for( int i=0; i<Size(); i++ )
	{
		Type *ii = Get(i);
		if( ii && !Traits::CompareNames(ii->GetName(),name) ) return i;
	}
	return -1;
}

template <class Type,class Traits>
int BankArray<Type,Traits>::Load( TypeNameType name )
{
	int free = base::Find(NULL);
	if (free>=0)
	{
		base::Set(free) = new Type(name);
		return free;
	}
	return Add(new Type(name));
}

template <class Type,class Traits>
Type *BankArray<Type,Traits>::New( TypeNameType name )
{
	//if( !name || !*name ) return NULL; // no name - return NULL
	int index=Find(name);
	if( index<0 ) index=Load(name);
	if( index<0 ) return NULL;
	return Get(index);
}

#endif
