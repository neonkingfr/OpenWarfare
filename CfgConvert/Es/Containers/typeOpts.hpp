#ifdef _MSC_VER
#pragma once
#endif

#ifndef _TYPE_OPTS_HPP
#define _TYPE_OPTS_HPP


// default general implemetation in common
// to both define/template approach to overloading
#include <Es/Memory/normalNew.hpp>

#if !__WATCOMC__ || !defined WNew // check for Optima++ declarations

#ifndef __PLACEMENT_NEW_INLINE
#define __PLACEMENT_NEW_INLINE
	inline void *__cdecl operator new (size_t size, void *addr)
	{
		return addr;
	}
	// delete required for unwinding exceptions
	inline void __cdecl operator delete (void *mem, void *addr)
	{
	}
#endif

#endif

// constructor variants
template <class Type>
void ConstructAt( Type &dst ) {new(&dst) Type;}

template <class Type>
void ConstructAt( Type &dst, const Type &src ) {new(&dst) Type(src);}

#include <Es/Memory/debugNew.hpp>

#include "typeDefines.hpp"

#include <string.h>

TypeIsSimple(bool);
TypeIsSimple(char);
TypeIsSimple(signed char);
TypeIsSimple(unsigned char);
TypeIsSimple(short);
TypeIsSimple(unsigned short);
TypeIsSimple(int);
TypeIsSimple(unsigned int);
TypeIsSimple(long);
TypeIsSimple(unsigned long);
TypeIsSimple(float);
TypeIsSimple(double);

#endif

