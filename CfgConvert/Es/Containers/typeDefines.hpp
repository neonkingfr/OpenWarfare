#ifdef _MSC_VER
#pragma once
#endif

#ifndef _TYPE_DEFINES_HPP
#define _TYPE_DEFINES_HPP

#include <string.h>

#ifndef PREPROCESS_DOCUMENTATION

// real implementation of type properties

#define TypeHasGenericConstructor(Type,decl) \
  decl void Construct( Type &dst ) {ConstructAt(dst);} \
  decl void ConstructArray( Type *dst, int count ) \
  {for( int i=0; i<count; i++ ) ConstructAt(dst[i]);}

#define TypeHasEmptyConstructor(Type,decl) \
  decl void Construct( Type &dst ) {} \
  decl void ConstructArray( Type *dst, int n ) {}

#define TypeHasZeroConstructor(Type,decl) \
  decl void Construct( Type &dst ) {memset(&dst,0,sizeof(Type));} \
  decl void ConstructArray( Type *dst, int n ) {memset(dst,0,sizeof(Type)*n);}


// destructor variants

#define TypeHasGenericDestructor(Type,decl) \
  decl void Destruct( Type &dst ) {(dst).~Type();} \
  decl void DestructArray( Type *dst, int n ) \
  {for( int i=0; i<n; i++ ) (dst[i]).~Type();}

#define TypeHasEmptyDestructor(Type,decl) \
  decl void Destruct( Type &dst ) {} \
  decl void DestructArray( Type *dst, int n ) {}

// copy variants

#define TypeHasGenericCopy(Type,decl) \
  decl void CopyData( Type *dst, Type const *src, int n ) \
  {for( int i=0; i<n; i++ ) dst[i]=src[i];} \
  decl void CopyConstruct( Type *dst, Type const *src, int n ) \
  {for( int i=0; i<n; i++ ) ConstructAt(dst[i],src[i]);} \
  decl void CopyConstruct( Type &dst, Type const &src ) {ConstructAt(dst,src);}

#define TypeHasBinaryCopy(Type,decl) \
  decl void CopyData( Type *dst, Type const *src, int n ) \
  {memcpy(dst,src,n*sizeof(Type));} \
  decl void CopyConstruct( Type *dst, Type const *src, int n ) \
  {memcpy(dst,src,n*sizeof(Type));} \
  decl void CopyConstruct( Type &dst, Type const &src ) \
  {memcpy(&dst,&src,sizeof(Type));}

// move variants

#define TypeHasGenericMove(Type,decl) \
  decl void MoveData( Type *dst, Type *src, int n ) \
  { \
  CopyConstruct(dst,src,n); \
  DestructArray(src,n); \
  } \
  decl void InsertData( Type *dst, int n ) \
  { \
  /* dst[n-1] is controlled before function call */ \
  for( int i=n-1; --i>=0; ) dst[i+1]=dst[i]; \
  Destruct(dst[0]); \
  /* dst[0] is not controlled after function call */ \
  } \
  decl void DeleteData( Type *dst, int n, int count ) \
  { \
  /* dst[0]..dst[count-1] is not controlled before function call */ \
  int i; \
  for (i=0; i<count; i++) Construct(dst[i]); \
  for (i=count; i<n; i++) dst[i-count]=dst[i]; \
  for (i=n-count; i<n; i++) Destruct(dst[i]); \
  /* dst[n-count]..dst[n-1] is not controlled after function call */ \
  }


#define TypeHasBinaryMove(Type,decl) \
  decl void MoveData( Type *dst, Type *src, int n ) \
  { \
  memcpy(dst,src,n*sizeof(Type)); \
  } \
  decl void InsertData( Type *dst, int n ) \
  { \
  /* dst[n-1] is controlled before function call */ \
  Destruct(dst[n-1]); \
  memmove(dst+1,dst,(n-1)*sizeof(Type)); \
  /* dst[0] is not controlled after function call */ \
  } \
  decl void DeleteData( Type *dst, int n, int count ) \
  { \
  /* dst[0]..dst[count-1] is not controlled before function call */ \
  memcpy(dst,dst+count,(n-count)*sizeof(Type)); \
  /* dst[n-count]..dst[n-1] is not controlled after function call */ \
  }


//! traits - type can provide effective implementation of corresponding functions
template <class Type>
struct ConstructTraits
{
  // constructor implementation
  static void Construct( Type &dst ) {Type::Construct(dst);}
  // constructor (array) implementation
  static void ConstructArray( Type *dst, int count ){Type::ConstructArray(dst,count);}

  // destructor implementation
  static void Destruct( Type &dst ) {Type::Destruct(dst);}
  // destructor (array) implementation
  static void DestructArray( Type *dst, int n ) {Type::DestructArray(dst,n);}

  // operator = implementation (array)
  static void CopyData( Type *dst, Type const *src, int n )
  {
    Type::CopyData(dst,src,n);
  }
  // copy constructor implementation (array)
  static void CopyConstruct( Type *dst, Type const *src, int n )
  {
    Type::CopyConstruct(dst,src,n);
  }
  // copy constructor implementation
  static void CopyConstruct( Type &dst, Type const &src )
  {
    Type::CopyConstruct(dst,src);
  }

  // contruct at dst, destruct at src 
  static void MoveData( Type *dst, Type *src, int n )
  {
    Type::MoveData(dst,src,n);
  }
  // move data
  // dst[n-1] is controlled before function call
  // dst[0] is not controlled after function call
  static void InsertData( Type *dst, int n )
  {
    Type::InsertData(dst,n);
  }
  // move data
  // dst[0]..dst[count-1] is not controlled before function call
  // dst[n-count]..dst[n-1] is not controlled after function call
  static void DeleteData( Type *dst, int n, int count )
  {
    Type::DeleteData(dst,n,count);
  }
};

template <class Type>
struct ConstructGlobalFunctions
{
  // constructor implementation
  static void Construct( Type &dst ) {::Construct(dst);}
  // constructor (array) implementation
  static void ConstructArray( Type *dst, int count ){::ConstructArray(dst,count);}

  // destructor implementation
  static void Destruct( Type &dst ) {::Destruct(dst);}
  // destructor (array) implementation
  static void DestructArray( Type *dst, int n ) {::DestructArray(dst,n);}

  // operator = implementation (array)
  static void CopyData( Type *dst, Type const *src, int n ){::CopyData(dst,src,n);}
  // copy constructor implementation (array)
  static void CopyConstruct( Type *dst, Type const *src, int n ){::CopyConstruct(dst,src,n);}
  // copy constructor implementation
  static void CopyConstruct( Type &dst, Type const &src ){::CopyConstruct(dst,src);}

  // contruct at dst, destruct at src 
  static void MoveData( Type *dst, Type *src, int n ){::MoveData(dst,src,n);}
  // move data
  // dst[n-1] is controlled before function call
  // dst[0] is not controlled after function call
  static void InsertData( Type *dst, int n ){::InsertData(dst,n);}
  // move data
  // dst[0]..dst[count-1] is not controlled before function call
  // dst[n-count]..dst[n-1] is not controlled after function call
  static void DeleteData( Type *dst, int n, int count ){::DeleteData(dst,n,count);}
};

#else


// documentation only - do not expand any functions

#define TypeHasGenericConstructor(Type,decl) \
  void Type_Has_GenericConstructor(Type type);
#define TypeHasEmptyConstructor(Type,decl) \
  void Type_Has_EmptyConstructor(Type type);
#define TypeHasZeroConstructor(Type,decl) \
  void Type_Has_ZeroConstructor(Type type);
#define TypeHasGenericDestructor(Type,decl) \
  void Type_Has_GenericDestructor(Type type);
#define TypeHasEmptyDestructor(Type,decl)\
  void Type_Has_EmptyDestructor(Type type);
#define TypeHasGenericCopy(Type,decl)\
  void Type_Has_GenericCopy(Type type);
#define TypeHasBinaryCopy(Type,decl)\
  void Type_Has_BinaryCopy(Type type);
#define TypeHasGenericMove(Type,decl)\
  void Type_Has_GenericMove(Type type);
#define TypeHasBinaryMove(Type,decl)\
  void Type_Has_BinaryMove(Type type);

#endif

#define TypeIsGenericDecl(Type,decl) \
  TypeHasGenericConstructor(Type,decl) \
  TypeHasGenericDestructor(Type,decl) \
  TypeHasGenericCopy(Type,decl) \
  TypeHasGenericMove(Type,decl)

#define TypeIsMovableDecl(Type,decl) \
  TypeHasGenericConstructor(Type,decl) \
  TypeHasGenericDestructor(Type,decl) \
  TypeHasGenericCopy(Type,decl) \
  TypeHasBinaryMove(Type,decl)

#define TypeIsMovableZeroedDecl(Type,decl) \
  TypeHasZeroConstructor(Type,decl) \
  TypeHasGenericDestructor(Type,decl) \
  TypeHasGenericCopy(Type,decl) \
  TypeHasBinaryMove(Type,decl)

#define TypeIsBinaryDecl(Type,decl) \
  TypeHasGenericConstructor(Type,decl) \
  TypeHasGenericDestructor(Type,decl) \
  TypeHasBinaryCopy(Type,decl) \
  TypeHasBinaryMove(Type,decl)

#define TypeIsBinaryZeroedDecl(Type,decl) \
  TypeHasZeroConstructor(Type,decl) \
  TypeHasGenericDestructor(Type,decl) \
  TypeHasBinaryCopy(Type,decl) \
  TypeHasBinaryMove(Type,decl)


#define TypeIsSimpleDecl(Type,decl) \
  TypeHasEmptyConstructor(Type,decl) \
  TypeHasEmptyDestructor(Type,decl) \
  TypeHasBinaryCopy(Type,decl) \
  TypeHasBinaryMove(Type,decl)

#define TypeIsSimpleZeroedDecl(Type,decl) \
  TypeHasZeroConstructor(Type,decl) \
  TypeHasEmptyDestructor(Type,decl) \
  TypeHasBinaryCopy(Type,decl) \
  TypeHasBinaryMove(Type,decl)

////
#define TemplateIsGenericDecl(Type,decl) \
  TemplateHasGenericConstructor(Type,decl) \
  TemplateHasGenericDestructor(Type,decl) \
  TemplateHasGenericCopy(Type,decl) \
  TemplateHasGenericMove(Type,decl)

#define TemplateIsMovableDecl(Type,decl) \
  TemplateHasGenericConstructor(Type,decl) \
  TemplateHasGenericDestructor(Type,decl) \
  TemplateHasGenericCopy(Type,decl) \
  TemplateHasBinaryMove(Type,decl)

#define TemplateIsMovableZeroedDecl(Type,decl) \
  TemplateHasZeroConstructor(Type,decl) \
  TemplateHasGenericDestructor(Type,decl) \
  TemplateHasGenericCopy(Type,decl) \
  TemplateHasBinaryMove(Type,decl)

#define TemplateIsBinaryDecl(Type,decl) \
  TemplateHasGenericConstructor(Type,decl) \
  TemplateHasGenericDestructor(Type,decl) \
  TemplateHasBinaryCopy(Type,decl) \
  TemplateHasBinaryMove(Type,decl)

#define TemplateIsBinaryZeroedDecl(Type,decl) \
  TemplateHasZeroConstructor(Type,decl) \
  TemplateHasGenericDestructor(Type,decl) \
  TemplateHasBinaryCopy(Type,decl) \
  TemplateHasBinaryMove(Type,decl)


#define TemplateIsSimpleDecl(Type,decl) \
  TemplateHasEmptyConstructor(Type,decl) \
  TemplateHasEmptyDestructor(Type,decl) \
  TemplateHasBinaryCopy(Type,decl) \
  TemplateHasBinaryMove(Type,decl)

#define TemplateIsSimpleZeroedDecl(Type,decl) \
  TemplateHasZeroConstructor(Type,decl) \
  TemplateHasEmptyDestructor(Type,decl) \
  TemplateHasBinaryCopy(Type,decl) \
  TemplateHasBinaryMove(Type,decl)

#ifdef NO_TYPE_DEFINES
#include "typeGenTemplates.hpp"

#define TypeIsGeneric(Type)

#define TypeIsMovable(Type)
#define TypeIsBinary(Type)
#define TypeIsSimple(Type)

#define TypeIsMovableZeroed(Type)
#define TypeIsBinaryZeroed(Type)
#define TypeIsSimpleZeroed(Type)

// use this form in class declaration (usefull esp. for templates)

#define ClassIsGeneric(Type)

#define ClassIsMovable(Type)
#define ClassIsBinary(Type)
#define ClassIsSimple(Type)

#define ClassIsMovableZeroed(Type)
#define ClassIsBinaryZeroed(Type)
#define ClassIsSimpleZeroed(Type)

#else

#ifndef PREPROCESS_DOCUMENTATION

// use this form outside of class declaration

#define TypeIsGeneric(Type) TypeIsGenericDecl(Type,inline); \
  template <> struct ConstructTraits< Type >: public ConstructGlobalFunctions< Type > {};

#define TypeIsMovable(Type) TypeIsMovableDecl(Type,inline) \
  template <> struct ConstructTraits< Type >: public ConstructGlobalFunctions< Type > {};

#define TypeIsBinary(Type) TypeIsBinaryDecl(Type,inline) \
  template <> struct ConstructTraits< Type >: public ConstructGlobalFunctions< Type > {};

#define TypeIsSimple(Type) TypeIsSimpleDecl(Type,inline) \
  template <> struct ConstructTraits< Type >: public ConstructGlobalFunctions< Type > {};


#define TypeIsMovableZeroed(Type) TypeIsMovableZeroedDecl(Type,inline) \
  template <> struct ConstructTraits< Type >: public ConstructGlobalFunctions< Type > {};

#define TypeIsBinaryZeroed(Type) TypeIsBinaryZeroedDecl(Type,inline) \
  template <> struct ConstructTraits< Type >: public ConstructGlobalFunctions< Type > {};

#define TypeIsSimpleZeroed(Type) TypeIsSimpleZeroedDecl(Type,inline) \
  template <> struct ConstructTraits< Type >: public ConstructGlobalFunctions< Type > {};

// use this form in class declaration (usefull esp. for templates)

#define ClassIsGeneric(Type) TypeIsGenericDecl(Type,static)

#define ClassIsMovable(Type) TypeIsMovableDecl(Type,static)
#define ClassIsBinary(Type) TypeIsBinaryDecl(Type,static)
#define ClassIsSimple(Type) TypeIsSimpleDecl(Type,static)

#define ClassIsMovableZeroed(Type) TypeIsMovableZeroedDecl(Type,static)
#define ClassIsBinaryZeroed(Type) TypeIsBinaryZeroedDecl(Type,static)
#define ClassIsSimpleZeroed(Type) TypeIsSimpleZeroedDecl(Type,static)

#else

// documentation only
#define TypeIsGeneric(Type) void Type_Is_Generic(Type type);
#define TypeIsMovable(Type) void Type_Is_Movable(Type type);
#define TypeIsBinary(Type) void Type_Is_Binary(Type type);
#define TypeIsSimple(Type) void Type_Is_Simple(Type type);

#define TypeIsMovableZeroed(Type) void Type_Is_MovableZeroed(Type type);
#define TypeIsBinaryZeroed(Type) void Type_Is_BinaryZeroed(Type type);
#define TypeIsSimpleZeroed(Type) void Type_Is_SimpleZeroed(Type type);

#define ClassIsGeneric(Type) void Class_Is_Generic(Type type);

#define ClassIsMovable(Type) void Class_Is_Movable(Type type);
#define ClassIsBinary(Type) void Class_Is_Binary(Type type);
#define ClassIsSimple(Type) void Class_Is_Simple(Type type);

#define ClassIsMovableZeroed(Type) void Class_Is_MovableZeroed(Type type);
#define ClassIsBinaryZeroed(Type) void Class_Is_BinaryZeroed(Type type);
#define ClassIsSimpleZeroed(Type) void Class_Is_SimpleZeroed(Type type);

#endif

#endif

#ifndef __forceinline

#ifdef __MWERKS__
#define __forceinline
#elif !defined _MSC_VER || _MSC_VER<1200
#define __forceinline inline
#endif

#endif

#endif