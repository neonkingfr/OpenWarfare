#ifndef _express_hpp
#define _express_hpp

#include <Es/Strings/rString.hpp>

#include <El/Common/enumNames.hpp>

//#define NVar ( 'z'-'a'+1 )

enum EvalError
{ // see also: "stringIds.hpp" and "stringtable.csv" (EVAL_...)
	EvalOK,
	EvalGen, // generic error
	EvalExpo, // exponent out of range or invalid
	EvalNum, // invalid number
	EvalVar, // undefined variable
	EvalBadVar, // undefined variable
	EvalDivZero, // zero divisor
	EvalTg90, // tg 90
	EvalOpenB, // missing (
	EvalCloseB, // missing )
	EvalEqu, // missing =
	EvalSemicolon, // missing =
	EvalOper, // unknown operator
	EvalLineLong, // line too long
	EvalType, // type mismatch
	EvalNamespace, // no name space
	EvalDim, // array dimension
};

#include <Es/Containers/hashMap.hpp>

#if 0 //_DEBUG
	// this part makes problems in VC.NET (XBox)
	#define DERIVED(type,basic) \
	class type \
	{ \
		basic _val; \
		public: \
		operator basic() const {return _val;} \
		operator basic&() {return _val;} \
		type(){} \
		explicit type( basic val ){_val=val;} \
	};
#else
	#define DERIVED(type,basic) typedef basic type;
#endif

DERIVED(GameType,int);

const GameType GameScalar(1);
const GameType GameArray(2);
const GameType GameBool(4);
const GameType GameString(8);
const GameType GameNothing(16);
//const GameType GameObject(2);
const GameType GameIf(0x1000000);
const GameType GameWhile(0x2000000);

const GameType FakeTypes(GameIf|GameWhile);
const GameType GameVoid(~(GameNothing|FakeTypes)); // any value type
const GameType GameAny(~FakeTypes); // any type including nothing

#ifndef ACCESS_ONLY
//DECL_ENUM(LSError)
class ParamArchive;

LSError Serialize(ParamArchive &ar, RString name, GameType &value, int minVersion);
#endif

class GameValue;

typedef float GameScalarType;
typedef bool GameBoolType;
typedef RString GameStringType;
typedef AutoArray<GameValue> GameArrayType;

extern const GameArrayType GameArrayEmpty;
extern GameArrayType GameArrayDummy;

class GameData: public RefCount
{
	public:
	virtual GameType GetType() const = 0;
	virtual ~GameData(){}

	virtual GameBoolType GetBool() const {return false;}
	virtual GameScalarType GetScalar() const {return 0;}
	virtual GameStringType GetString() const {return "";}
	virtual const GameArrayType &GetArray() const {return GameArrayEmpty;}
	virtual GameArrayType &GetArray() {return GameArrayDummy;}
	virtual GameData *Clone() const {return NULL;}
	//! if there are any shared data (see GameDataArray), we may wish to set them as read only
	virtual void SetReadOnly(bool val) {}
	//! check read only status
	virtual bool GetReadOnly() const {return false;}

	virtual RString GetText() const = 0;
	virtual bool IsEqualTo(const GameData *data) const = 0;

	virtual const char *GetTypeName() const {return "unknown";}
	virtual bool GetNil() const {return false;}

	#ifndef ACCESS_ONLY
	static GameData *CreateObject(ParamArchive &ar);
	virtual LSError Serialize(ParamArchive &ar);
	#endif
};

#include <Es/Memory/normalNew.hpp>

class GameDataNil: public GameData
{
	GameType _type;

	public:
	GameDataNil( GameType type ):_type(type){}
	GameType GetType() const {return _type;}

	RString GetText() const;
	bool IsEqualTo(const GameData *data) const;
	bool GetNil() const {return true;}
	const char *GetTypeName() const {return "void";}
	GameData *Clone() const {return new GameDataNil(*this);}

	#ifndef ACCESS_ONLY
	LSError Serialize(ParamArchive &ar);
	#endif

	USE_FAST_ALLOCATOR
};

class GameDataScalar: public GameData
{
	typedef GameData base;

	GameScalarType _value;

	public:
	GameDataScalar():_value(0){}
	GameDataScalar( GameScalarType value ):_value(value){}
	~GameDataScalar(){}

	GameType GetType() const {return GameScalar;}
	GameScalarType GetScalar() const {return _value;}

	RString GetText() const;
	bool IsEqualTo(const GameData *data) const;
	const char *GetTypeName() const {return "float";}
	GameData *Clone() const {return new GameDataScalar(*this);}

	#ifndef ACCESS_ONLY
	LSError Serialize(ParamArchive &ar);
	#endif

	USE_FAST_ALLOCATOR
};

class GameDataNothing: public GameData
{
	typedef GameData base;

	public:
	GameDataNothing(){}
	~GameDataNothing(){}

	GameType GetType() const {return GameNothing;}

	RString GetText() const {return "nothing";}
	bool IsEqualTo(const GameData *data) const;
	const char *GetTypeName() const {return "nothing";}
	GameData *Clone() const {return new GameDataNothing(*this);}

	USE_FAST_ALLOCATOR
};

class GameDataString: public GameData
{
	typedef GameData base;

	GameStringType _value;

	public:
	GameDataString():_value(0){}
	GameDataString( GameStringType value ):_value(value){}
	~GameDataString(){}

	GameType GetType() const {return GameString;}
	GameStringType GetString() const {return _value;}

	RString GetText() const;
	bool IsEqualTo(const GameData *data) const;
	const char *GetTypeName() const {return "float";}
	GameData *Clone() const {return new GameDataString(*this);}

	#ifndef ACCESS_ONLY
	LSError Serialize(ParamArchive &ar);
	#endif

	USE_FAST_ALLOCATOR
};

class GameDataBool: public GameData
{
	typedef GameData base;

	GameBoolType _value;

	public:
	GameDataBool():_value(false){}
	GameDataBool( GameBoolType value ):_value(value){}
	~GameDataBool(){}

	GameType GetType() const {return GameBool;}
	GameBoolType GetBool() const {return _value;}
	GameScalarType GetScalar() const {return _value;}

	RString GetText() const;
	bool IsEqualTo(const GameData *data) const;
	const char *GetTypeName() const {return "bool";}
	GameData *Clone() const {return new GameDataBool(*this);}

	#ifndef ACCESS_ONLY
	LSError Serialize(ParamArchive &ar);
	#endif

	USE_FAST_ALLOCATOR
};

class GameDataIf: public GameDataBool
{
	public:
	GameDataIf(){}
	GameDataIf( GameBoolType value ):GameDataBool(value){}
	GameType GetType() const {return GameIf;}
	const char *GetTypeName() const {return "if";}
	GameData *Clone() const {return new GameDataIf(*this);}
};

class GameDataWhile: public GameDataString
{
	public:
	GameDataWhile(){}
	GameDataWhile( GameStringType value ):GameDataString(value){}
	GameType GetType() const {return GameWhile;}
	const char *GetTypeName() const {return "while";}
	GameData *Clone() const {return new GameDataWhile(*this);}
};

class GameDataArray: public GameData
{
	typedef GameData base;

	//! actual array data
	GameArrayType _value;
	//! some array may be read only
	bool _readOnly;

	public:
	GameDataArray();
	GameDataArray(const GameArrayType &value);
	~GameDataArray();

	GameType GetType() const {return GameArray;}
	const GameArrayType &GetArray() const {return _value;}
	GameArrayType &GetArray() {return _value;}
	//! change or crate given element
	void SetReadOnly(bool val){_readOnly=val;}
	bool GetReadOnly()const {return _readOnly;}

	RString GetText() const;
	bool IsEqualTo(const GameData *data) const;
	const char *GetTypeName() const {return "array";}
	GameData *Clone() const;

	#ifndef ACCESS_ONLY
	LSError Serialize(ParamArchive &ar);
	#endif

	USE_FAST_ALLOCATOR
};

#include <Es/Memory/debugNew.hpp>

class GameValue : public SerializeClass
{
	friend class GameState;
	friend class GameVariable;

	// only one of these should be valid
	protected:
	Ref<GameData> _data;

	public:
	explicit GameValue( GameData *data );

	public:
	GameValue();

	GameValue( GameBoolType value ){_data=new GameDataBool(value);}
	GameValue( GameScalarType value ){_data=new GameDataScalar(value);}
	GameValue( const GameArrayType &value ){_data=new GameDataArray(value);}
	GameValue( GameStringType value ){_data=new GameDataString(value);}
	GameValue( const char *value ){_data=new GameDataString(value);}

	void operator = ( bool value ){_data=new GameDataBool(value);}
	void operator = ( float value ){_data=new GameDataScalar(value);}
	void operator = ( const GameArrayType &value ){_data=new GameDataArray(value);}
	void operator = ( GameStringType value ){_data=new GameDataString(value);}
	void operator = ( const char *value ){_data=new GameDataString(value);}

	// special handling of copy
	GameValue( const GameValue &value );
	void operator = ( const GameValue &value );

	void Duplicate( const GameValue &value );
	bool SharedInstance() const {return _data ? _data->RefCounter()>1 : false;}

	// access
	GameData *GetData() const {return _data;}
	GameType GetType() const {return _data ? _data->GetType() : GameAny;}
	bool GetNil() const {return _data ? _data->GetNil() : true;}

	operator GameBoolType() const {return _data ? _data->GetBool() : false;}
	operator GameScalarType() const {return _data ? _data->GetScalar() : 0;}
	operator const GameArrayType &() const {return _data ? _data->GetArray() : GameArrayEmpty;}
	operator GameArrayType &() {return _data ? _data->GetArray() : GameArrayDummy;}
	operator GameStringType() const {return _data ? _data->GetString() : "";}

	bool GetReadOnly() const {return _data ? _data->GetReadOnly() : false;}
	void SetReadOnly(bool val) {if (_data) _data->SetReadOnly(val);}

	RString GetText() const;
	RString GetDebugText() const;
	bool IsEqualTo(const GameValue &value) const;

	#ifndef ACCESS_ONLY
	LSError Serialize(ParamArchive &ar);
	#else
	LSError Serialize(ParamArchive &ar) {return LSOK;}
	#endif
};

//GameValue CreateGameValue( GameType type );

TypeIsMovable(GameValue)

typedef const GameValue &GameValuePar;

class GameVariable
{
public:
	RString _name;
	GameValue _value;
	bool _readOnly;

	GameVariable()
	:_name(NULL),_readOnly(false)
	{
	}
	GameVariable( RString name, GameValuePar value, bool readOnly=false )
	:_name(name),_readOnly(readOnly)
	{
		_value=value;
	};
	const char *GetKey() const {return _name;}
	LSError Serialize(ParamArchive &ar);
	
	RString GetName() const {return _name;}
	RString GetValueText() const {return _value.GetText();}

	bool IsReadOnly() const {return _readOnly;}
};

TypeIsMovable(GameVariable);

typedef MapStringToClass<GameVariable, AutoArray<GameVariable> > VarBankType;

//typedef double Real;
typedef float Real;

#define SL 256 // stack levels

class GameState;


//#define GameValuePar const GameValue &

class GameState;
typedef GameValue (*ProcBinary)(const GameState *state, GameValuePar o1, GameValuePar o2);
typedef GameValue (*ProcUnary)(const GameState *state, GameValuePar o1);
typedef GameValue (*ProcNular)(const GameState *state);

struct GameOpName
{
	RString _opName;
	GameOpName(){}
	GameOpName(RString name):_opName(name){}
};

struct GameNular: public GameOpName
{
	RString _name;
	ProcNular _proc;
	GameType _retType;

	GameNular(){}
	GameNular
	(
		GameType retType,
		RString name, ProcNular proc
	)
	:GameOpName(name),_retType(retType),_name(name),_proc(proc)
	{_name.Lower();}
	const char *GetKey() const {return _name;}
};
TypeIsMovableZeroed(GameNular)

typedef MapStringToClass<GameNular, AutoArray<GameNular> > GameNularsType;

struct GameFunction: public GameOpName
{
	RString _name;
	ProcUnary _proc;
	GameType _retType;
	GameType _argType;

	GameFunction(){}
	GameFunction
	(
		GameType retType,
		RString name, ProcUnary proc,
		GameType argType
	)
	:GameOpName(name),_retType(retType),_name(name),_proc(proc),_argType(argType)
	{_name.Lower();}
};
TypeIsMovableZeroed(GameFunction)

struct GameFunctions : public AutoArray<GameFunction>, public GameOpName
{
	RString _name;
	GameFunctions(){}
	GameFunctions(RString name)
	:GameOpName(name),_name(name)
	{_name.Lower();}
	const char *GetKey() const {return _name;}
};
TypeIsMovable(GameFunctions)

typedef MapStringToClass<GameFunctions, AutoArray<GameFunctions> > GameFunctionsType;

enum GamePriority
{
	nula,
	logicOr, logicAnd, compare, function, functionFirst,
	soucet, soucin, unar, mocnina, zavorky
};

struct GameOperator: public GameOpName
{
	RString _name;
	ProcBinary _proc;
	GamePriority _priority;
	GameType _retType;
	GameType _argType1,_argType2;

	GameOperator(){}
	GameOperator
	(
		GameType retType, 
		RString name, GamePriority priority, ProcBinary proc,
		GameType argType1, GameType argType2
	)
	:GameOpName(name),_retType(retType),_name(name),_proc(proc),_priority(priority),
	_argType1(argType1),_argType2(argType2)
	{_name.Lower();}
};
TypeIsMovableZeroed(GameOperator)

struct GameOperators : public AutoArray<GameOperator>, public GameOpName
{
	RString _name;
	GamePriority _priority;
	GameOperators(){}
	GameOperators(RString name, GamePriority priority)
	:GameOpName(name),_name(name),_priority(priority)
	{_name.Lower();}
	const char *GetKey() const {return _name;}
};
TypeIsMovable(GameOperators)

typedef MapStringToClass<GameOperators, AutoArray<GameOperators> > GameOperatorsType;

typedef MapStringToClass<RString, AutoArray<RString> > GameNameType;

class GameVarSpace
{
	public:
	VarBankType _vars;
	const GameVarSpace *_parent;

	//! no local variables inherited
	GameVarSpace();
	//! inherit all local variables from some parent scope
	GameVarSpace(GameVarSpace *parent);

	static bool IsNull(const GameVariable &value) {return VarBankType::IsNull(value);}
	static bool NotNull(const GameVariable &value) {return VarBankType::NotNull(value);}
	static GameVariable &Null() {return VarBankType::Null();}

	//! set variable in this and all parent scopes
	void VarSet(const char *name, GameValuePar value, bool readOnly=false);
	//! get variable in this and all parent scopes
	bool VarGet(const char *name, GameValue &ret) const;
	//! create undefined variable in the innermost scope
	void VarLocal( const char *name);
};


#include <Es/Memory/normalNew.hpp>

class GameEvaluator: public RefCount
{
	friend class GameState;

	GameVarSpace *local; // local variables
	
	const char *_pos,*_pos0;
	const char *_subexpBeg,*_subexpEnd;
	GameValue _stack[SL]; // value stack
	int UB[SL]; // unary/binary flag
	int _priorStack[SL]; // prioriry stack
	RString _operStack[SL];
	int SP; // stack pointer
	int _parPrior; // parenthesis priority
	int _listPrior; // parenthesis priority
	bool _checkOnly;
	EvalError _error;
	RString _errorText; // user friendly error text
	int _errorCarretPos;
	
	GameEvaluator( GameVarSpace *vars=NULL );
	~GameEvaluator();

	USE_FAST_ALLOCATOR
};

#include <Es/Memory/debugNew.hpp>

//! class of callback functions
class ArchiveFunctions
{
public:
	ArchiveFunctions() {}
	virtual ~ArchiveFunctions() {}

	//! callback function to serialize boolean value
	virtual LSError Serialize(ParamArchive &ar, const RStringB &name, bool &value, int minVersion) {return (LSError)0;}
	//! callback function to serialize boolean value
	virtual LSError Serialize(ParamArchive &ar, const RStringB &name, bool &value, int minVersion, bool defValue) {return (LSError)0;}
	//! callback function to serialize GameType value
	virtual LSError Serialize(ParamArchive &ar, const RStringB &name, GameType &value, int minVersion) {return (LSError)0;}
	//! callback function to serialize float value
	virtual LSError Serialize(ParamArchive &ar, const RStringB &name, float &value, int minVersion) {return (LSError)0;}
	//! callback function to serialize string value
	virtual LSError Serialize(ParamArchive &ar, const RStringB &name, RString &value, int minVersion) {return (LSError)0;}
	//! callback function to serialize array value
	virtual LSError Serialize(ParamArchive &ar, const RStringB &name, AutoArray<GameValue> &value, int minVersion) {return (LSError)0;}
	//! callback function to serialize game data value
	virtual LSError Serialize(ParamArchive &ar, const RStringB &name, Ref<GameData> &value, int minVersion) {return (LSError)0;}
	//! callback function to serialize game state
	virtual LSError Serialize(ParamArchive &ar, GameState *value) {return (LSError)0;}
	//! callback function to create game data
	virtual GameData *CreateGameData(ParamArchive &ar, GameType type) {return NULL;}
	//! callback function to check load/save status of archive
	virtual bool IsSaving(ParamArchive &ar) {return false;}
};

//! class of callback functions
class GameStateInfoFunctions
{
public:
	GameStateInfoFunctions() {}
	virtual ~GameStateInfoFunctions() {}

	//! callback function to return error description
	virtual const char *GetErrorString(EvalError error) const {return "";}
	//! callback function to return type name
	virtual RString GetTypeName(GameType type) const {return "";}
	//! callback function to display error message
	virtual void DisplayErrorMessage(const char *position, const char *error) const {}
};

typedef GameData *CreateGameDataFunction();

struct GameTypeType : public EnumName
{
	CreateGameDataFunction *createFunction;
	RString typeName;

	GameTypeType(int v, RStringB n, CreateGameDataFunction *f, RString tname)
		: EnumName(v, n) {createFunction = f; typeName = tname;}
	GameTypeType() {createFunction = NULL;}
	GameData *CreateGameData() {return createFunction();}
};
TypeIsMovableZeroed(GameTypeType)

class GameState: public SerializeClass, public GameVarSpace
{
	private:
	AutoArray<GameTypeType> _typeNames; // symbolic name list

	//GameNameType _lexElems; // non-identifier lex. elements
//	GameNameType _functionNames; // function name bank
//	GameNameType _operatorNames; // operator name bank
//	GameNameType _nularNames; // operator name bank

	// TODO: use MapStringToClass
//	AutoArray<GameFunction> _functions;
	GameFunctionsType _functions;
//	AutoArray<GameOperator> _operators;
	GameOperatorsType _operators;
//	AutoArray<GameNular> _nulars;
	GameNularsType _nulars;
	//AutoArray<GameVariable> _vars;

	//mutable GameEvaluator *_e;
	enum {MaxContexts=64};
	mutable Ref<GameEvaluator> _contextStack[MaxContexts];
	mutable int _actContext;

	mutable GameEvaluator *_e;

	static ArchiveFunctions *_defaultArchiveFunctions;
	static GameStateInfoFunctions *_defaultInfoFunctions;
	
	// evaluator temporary storage
	
protected:
	bool VerifyArg( GameValuePar par, GameType type );

	void vynech() const;
	Real sejmid() const;

	void VyhCast( int Prio ) const; // evaluate to priority
	GameValue Vyhod() const; // partial evaluation
	GameValue Const() const; // term evaluation
	void CleanStack() const;

	// report argument types incompatible
	void TypeErrorOperator(const char *name, GameType left, GameType right) const;
	void TypeErrorFunction(const char *name, GameType right) const;
	
public:

public:	
	void NewNularOp( const GameNular & f);
	void NewFunction( const GameFunction & f);
	void NewOperator( const GameOperator & f);

	void NewNularOps( const GameNular *f, int count);
	void NewFunctions( const GameFunction *f, int count);
	void NewOperators( const GameOperator *f, int count);

	//! append all nular op names passed by filter to given string array
	void AppendNularOpList
	(
		AutoArray<RStringS> &dic, bool (*filter)(const char *word)
	) const;
	//! append all function names passed by filter to given string array
	void AppendFunctionList
	(
		AutoArray<RStringS> &dic, bool (*filter)(const char *word)
	) const;
	//! append all operator names passed by filter to given string array
	void AppendOperatorList
	(
		AutoArray<RStringS> &dic, bool (*filter)(const char *word)
	) const;

	// name must exist during whole existance of GameState object
	// string constant expected
	void NewType( const char *name, GameType val, CreateGameDataFunction *func, RString tname );
public:
	GameState();
	~GameState();
	
	void Init();
	void Reset();

	static void SetDefaultArchiveFunctions(ArchiveFunctions *f) {_defaultArchiveFunctions = f;}
	template <class Type>
	static LSError Serialize(ParamArchive &ar, const RStringB &name, Type &value, int minVersion)
	{
		return _defaultArchiveFunctions->Serialize(ar, name, value, minVersion);
	}
	template <class Type>
	static LSError Serialize(ParamArchive &ar, const RStringB &name, Type &value, int minVersion, bool defValue)
	{
		return _defaultArchiveFunctions->Serialize(ar, name, value, minVersion, defValue);
	}
	static GameData *CreateGameData(ParamArchive &ar, GameType type)
	{
		return _defaultArchiveFunctions->CreateGameData(ar, type);
	}
	static bool IsSaving(ParamArchive &ar)
	{
		return _defaultArchiveFunctions->IsSaving(ar);
	}
	static void SetDefaultInfoFunctions(GameStateInfoFunctions *f) {_defaultInfoFunctions = f;}

	bool IdtfGoodName( const char *name ) const;	
	bool VarGoodName( const char *name ) const;
	bool LValueGoodName( const char *name ) const;
	GameValue VarGet( const char *name ) const;
	bool VarReadOnly( const char *name ) const;
  //! Creates a variable and assignes it a value
  /*!
    If the name of the variable starts with the _ char, then it will be local
    for the script.
    \param name Name of the value
    \param value Initial value of the variable
    \param readOnly Variable is read only if true
    \param forceLocal Variable is local even if its name doesn't start with the _ char, if true
  */
	void VarSet( const char *name, GameValuePar value, bool readOnly=false, bool forceLocal=false );
	void VarSetLocal( const char *name, GameValuePar value, bool readOnly=false, bool forceLocal=false ) const;
	//! create undefined variable in the innermost scope
	void VarLocal( const char *name);
	void VarDelete( const char *name );

	bool IsVisible( const GameVariable &var ) const;
	
	void SetError( EvalError error, ... ) const;
	void TypeError(GameType exp, GameType was, const char *name=NULL ) const; 
	
	EvalError GetLastError() const;
	RString GetLastErrorText() const;
	int GetLastErrorPos() const;

	// evaluate an expression
	//! get actual context (started by BeginContext)
	GameVarSpace *GetContext() const;

	//! start context with given local variable space
	void BeginContext( GameVarSpace *vars ) const;
	//! close context
	void EndContext() const;

	struct EvaluateTerm
	{
		char *terminationBy;
	};

	GameValue Evaluate( const char *expression ) const;
	GameValue EvaluateMultiple(const char *expression, bool localOnly=false) const;
	bool EvaluateMultipleBool(const char *expression, bool localOnly=false) const;
	bool EvaluateBool( const char *expression ) const;
	//! execute one assignment command
	bool PerformAssignment(bool localOnly=false);
	//! execute command or list of commands
	void Execute( const char *expression );

	void ShowError() const;
	
	// check syntax
	bool CheckEvaluate( const char *expression ) const;
	bool CheckEvaluateBool( const char *expression ) const;
	bool CheckExecute( const char *expression ) const;

	// serialization support

	virtual GameData *CreateGameData( GameType type ) const;
	virtual GameValue CreateGameValue( GameType type ) const;
	virtual RString GetTypeName(GameType type) const;

	#if !ACCESS_ONLY
	virtual LSError Serialize(ParamArchive &ar);
	static GameState *CreateObject(ParamArchive &ar) {return new GameState();}
	#else
	virtual LSError Serialize(ParamArchive &ar) {return LSOK;}
	//static GameState *CreateObject(ParamArchive &ar) {return NULL;}
	#endif

	// diagnostic support
	const VarBankType &GetVariables() const {return _vars;}
	VarBankType &GetVariables() {return _vars;}
};

extern GameState GGameState;

#endif
