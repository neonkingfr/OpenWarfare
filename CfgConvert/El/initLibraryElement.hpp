#ifdef _MSC_VER
#pragma once
#endif

#ifndef _INIT_LIBRARY_ELEMENT_HPP
#define _INIT_LIBRARY_ELEMENT_HPP

void InitParamFilePreprocess();
void InitParamFileStringtable();
void InitParamFileEvaluator();

inline void InitLibraryElement()
{
	InitParamFilePreprocess();
	InitParamFileStringtable();
	InitParamFileEvaluator();
}

#endif
