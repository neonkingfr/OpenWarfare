#ifdef _MSC_VER
#pragma once
#endif

#ifndef _LIB_INCLUDES_HPP
#define _LIB_INCLUDES_HPP
// precompiled header files - many files use something of Windows API

#include <Es\Types\memtype.h> // basic memory types

// user general include
#include <Es\Framework/debugLog.hpp>
#include <Es\Containers\typeOpts.hpp>
#include <Es\Types\pointers.hpp>
#include <Es\Containers\array.hpp>
#include <Es\Memory\fastAlloc.hpp>
#include <Es\Types\removeLinks.hpp>

// project general include

#endif
