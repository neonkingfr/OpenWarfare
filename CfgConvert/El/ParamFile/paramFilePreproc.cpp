// Add preprocessor functionality to ParamFile

#include <elementpch.hpp>

#include <qstream/QBStream.hpp>
#include "paramFile.hpp"

#include <preprocessor/preproc.h>

//! class of callback functions
static class BredyPreprocessorFunctions : public PreprocessorFunctions
{
public:
	//! callback function to preprocess of stream content
	virtual bool Preprocess(QOStream &out, const char *name);

	BredyPreprocessorFunctions() {ParamFile::SetDefaultPreprocFunctions(this);}
} GBredyPreprocessorFunctions;

class Preprocessor : public Preproc
{
protected:
	QIStream *OnEnterInclude(const char *filename)
	{
		if (!QIFStreamB::FileExist(filename))
			return NULL;
		QIFStreamB *stream = new QIFStreamB();
		stream->AutoOpen(filename);
		return stream;
	}
	void OnExitInclude(QIStream *stream)
	{
		if (stream) delete (QIFStreamB *)stream;
	}
};

bool BredyPreprocessorFunctions::Preprocess(QOStream &out, const char *name)
{
	Preprocessor preprocessor;
	if (!preprocessor.Process(&out, name))
	{
		ErrorMessage("Preprocessor failed on file %s - error %d.", name, preprocessor.error);
		return false;
	}
	return true;
}

