// Add evaluator functionality to ParamFile

#include <El/elementpch.hpp>

#include <El/QStream/QBStream.hpp>
#include "paramFile.hpp"

#include <El/QStream/serializeBin.hpp>
#include <El/Evaluator/express.hpp>

//! class of callback functions
class GameStateEvaluatorFunctions : public EvaluatorFunctions
{
public:
	//! callback function to create variables space
	virtual GameVarSpace *CreateVariables();
	//! callback function to destroy variables space
	virtual void DeleteVariables(GameVarSpace *vars);
	//! callback function to initialize expression evaluator
	virtual void InitEvaluator(GameVarSpace *vars);
	//! callback function to deinitialize expression evaluator
	virtual void DoneEvaluator();
	//! callback function to load variables from binary stream
	virtual GameVarSpace *LoadVariables(SerializeBinStream &f);
	//! callback function to save variables into binary stream
	virtual void SaveVariables(SerializeBinStream &f, GameVarSpace *vars);
	//! callback function to evaluate float expression
	virtual float EvaluateFloat(const char *expr, GameVarSpace *vars);
	//! callback function to evaluate float expression without game state initialization and deinitialization
	virtual float EvaluateFloatInternal(const char *expr);
	//! callback function to evaluate string expression without game state initialization and deinitialization
	virtual RString EvaluateStringInternal(const char *expr);
	//! callback function to execute expression without game state initialization and deinitialization
	virtual void ExecuteInternal(const char *expr);
	//! callback function to set value of float variable without game state initialization and deinitialization
	virtual void VarSetFloatInternal(const char *name, float value, bool readOnly, bool forceLocal);
	
	GameStateEvaluatorFunctions() {ParamFile::SetDefaultEvalFunctions(this);}
} GGameStateEvaluatorFunctions;

void InitParamFileEvaluator ()
{
	volatile void* dummy = &GGameStateEvaluatorFunctions;
}

GameVarSpace *GameStateEvaluatorFunctions::CreateVariables()
{
	return new GameVarSpace();
}

void GameStateEvaluatorFunctions::DeleteVariables(GameVarSpace *vars)
{
	delete vars;
}

/*!
- DoneEvaluator must be done
*/
void GameStateEvaluatorFunctions::InitEvaluator(GameVarSpace *vars)
{
	GGameState.BeginContext(vars);
}

void GameStateEvaluatorFunctions::DoneEvaluator()
{
	GGameState.EndContext();
}

GameVarSpace *GameStateEvaluatorFunctions::LoadVariables(SerializeBinStream &f)
{
	GameVarSpace *vars = new GameVarSpace();
	int n;
	f.Transfer(n);
	for (int i=0; i<n; i++)
	{
		RString name;
		int value;
		f.Transfer(name);
		f.Transfer(value);
		GameVariable var(name, (float)value, true);
		vars->_vars.Add(var);
	}
	return vars;
}

void GameStateEvaluatorFunctions::SaveVariables(SerializeBinStream &f, GameVarSpace *vars)
{
	int n = vars ? vars->_vars.NItems() : 0;
	f.Transfer(n);
	if (n > 0)
		for (int i=0; i<vars->_vars.NTables(); i++)
		{
			AutoArray<GameVariable> &table = vars->_vars.GetTable(i);
			for (int j=0; j<table.Size(); j++)
			{
				RString name = table[j]._name;
				int value = toInt((float)table[j]._value);
				f.Transfer(name);
				f.Transfer(value);
			}
		}
}

float GameStateEvaluatorFunctions::EvaluateFloat(const char *expr, GameVarSpace *vars)
{
	GGameState.BeginContext(vars);
	GameValue result = GGameState.Evaluate(expr);
	GGameState.EndContext();
	return result;
}

float GameStateEvaluatorFunctions::EvaluateFloatInternal(const char *expr)
{
	GameValue result = GGameState.Evaluate(expr);
	return result;
}

RString GameStateEvaluatorFunctions::EvaluateStringInternal(const char *expr)
{
	return GGameState.Evaluate(expr).GetText();
}

void GameStateEvaluatorFunctions::ExecuteInternal(const char *expr)
{
	GGameState.Execute(expr);
}

void GameStateEvaluatorFunctions::VarSetFloatInternal(const char *name, float value, bool readOnly, bool forceLocal)
{
	GGameState.VarSet(name, GameValue(value), readOnly, forceLocal);
}
