// Add stringtable functionality to ParamFile

#include <elementpch.hpp>
#include "paramFile.hpp"
#include <stringtable/stringtable.hpp>

//! class of callback functions
static class StringtableFunctions : public LocalizeStringFunctions
{
public:
	//! callback function to load string from stringtable
	virtual RString LocalizeString(const char *str);

	StringtableFunctions() {ParamFile::SetDefaultLocalizeStringFunctions(this);}
} GStringtableFunctions;

RString StringtableFunctions::LocalizeString(const char *str)
{
	return ::LocalizeString(str);
}



