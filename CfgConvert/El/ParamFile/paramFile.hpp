#ifdef _MSC_VER
#pragma once
#endif

#ifndef _PARAM_FILE_HPP
#define _PARAM_FILE_HPP

#include <Es/Strings/rstring.hpp>
#include <Es/Containers/array.hpp>
#include <El/qstream/qstream.hpp>

class SerializeBinStream;

class IParamArrayValue;

#include <Es/Memory/normalNew.hpp>

class ParamRawValue;

class PASumCalculator;


//! Access mode for ParamFile entries

/*!
Used for protection against updating config by add-on mechanism.
*/

enum ParamAccessMode
{
	PAReadAndWrite, //! any modifications enabled
	PAReadAndCreate, //! only adding new class members is allowed
	PAReadOnly, //! no modifications enabled
	PAReadOnlyVerified //! no modifications enabled, CRC test applied
};

class ParamEntry;
class ParamFile;

//! functor interface - used to check if entry can be accessed
class IParamVisibleTest
{
	public:
	//! check if given entry is visible
	/*!
	\param entry check if this entry is visible
	\return true if visible
	*/
	virtual bool operator () (const ParamEntry &entry) = NULL;
	//! check if given entry is visible
	/*!
	\param parent parent of entry, is guranteed to be visible
	\param entry check if this entry is visible
	\return true if visible
	*/
	virtual bool operator () (const ParamEntry &parent, const ParamEntry &entry) = NULL;
};

extern IParamVisibleTest &DefaultAccess;

//! most common implementation of context visibility
//! any entry that belong to one of the listed owners can be accessed

class ParamOwnerList: public IParamVisibleTest
{
	FindArray<RStringB> _addons;

	public:
	void Clear(){_addons.Clear();}
	void Add(RString addon)
	{
		addon.Lower();
		_addons.AddUnique(addon);
	}
	int GetSize() const {return _addons.Size();}
	const RStringB &Get(int i) const {return _addons[i];}
	//! check if given entry is visible
	/*!
	\param parent parent of entry, is guranteed to be visible
	\param entry check if this entry is visible
	\return true if visible
	*/
	bool operator () (const ParamEntry &parent, const ParamEntry &entry);
	bool operator () (const ParamEntry &entry);
};


class ParamEntry: public RefCount
{
	friend class ParamClass;

	protected:
	RStringB _name;
	//bool _overload;

	void SetName( const RStringB &name ) {_name=name;}
	//void SetOverload( bool overload ) {_overload=overload;}

	Ref<ParamEntry> Merge( const ParamEntry *ovld ) const; // merge in base class/member

	public:
	explicit ParamEntry( const RStringB &name );
	const RStringB &GetName() const {return _name;}
	RString GetContext( const char *member=NULL ) const; // fully qualified name

	virtual bool IsClass() const {return false;}
	virtual bool IsArray() const {return false;}
	virtual bool IsError() const {return false;}

	/*!
	\name Access control
	\patch_internal 1.11 Date 08/03/2001 by Ondra
	- New: access protection of ParamClass
	*/
	//@{
	virtual void SetAccessMode(ParamAccessMode mode) {}
	virtual void SetAccessModeForAll(ParamAccessMode mode){}
	virtual ParamAccessMode GetAccessMode() const {return PAReadAndWrite;}
	//@}
	//! owner - used to check if given entry can be accessed
	virtual const RStringB &GetOwner() const;
	virtual void SetOwner(RString owner, bool subentries=true);
	virtual bool CheckVisible(IParamVisibleTest &visible) const;

	// value conversions
	virtual operator RStringB() const;
	virtual operator float() const;
	virtual operator int() const;
	virtual operator bool() const;
	virtual int GetInt() const;
	virtual RStringB GetValueRaw() const;

	virtual void SetValue( const RStringB &string );
	virtual void SetValue( float val );
	virtual void SetValue( int val );
	virtual void SetFile(ParamFile *file) = NULL;

	virtual void Add( const RStringB &name, const RStringB &val );
	virtual void Add( const RStringB &name, float val );
	virtual void Add( const RStringB &name, int val );
	virtual ParamEntry *AddClass( const RStringB &name);
	virtual ParamEntry *AddArray( const RStringB &name);
	virtual void Clear();
	virtual void Delete(const RStringB &name);

	virtual void AddValue(float val);
	virtual void AddValue(int val);
	virtual void AddValue(bool val);
	virtual void AddValue(const RStringB &val);
	virtual void AddValue(const char *val);

	virtual int GetEntryCount() const;
	virtual const ParamEntry &GetEntry( int i ) const;

	virtual const IParamArrayValue &operator [] ( int index ) const;
	virtual void SetValue( int index, const RStringB &string );
	virtual void SetValue( int index, float val );
	virtual void SetValue( int index, int val );
	virtual int GetSize() const;

	virtual ParamEntry *FindEntryNoInheritance
	(
		const char *name, IParamVisibleTest &visible = DefaultAccess
	) const;
	virtual ParamEntry *FindEntry
	(
		const char *name, IParamVisibleTest &visible = DefaultAccess
	) const;
	virtual const ParamEntry &operator >> ( const char *name ) const;
	//virtual const ParamEntry &operator >> ( RString name ) const;

	virtual void Save( QOStream &f, int indent ) const {}

	virtual void SerializeBin(SerializeBinStream &f);
	virtual void Compact();

	//! calculate checksum of entry and all its verified children 
	/*!
	\patch_internal 1.11 Date 08/03/2001 by Ondra
	- New: CRC/checksum verification of ParamClass
	*/
	virtual void CalculateCheckValue(PASumCalculator &sum) const = NULL;
	//! check if entry or some of its children is verified protected
	virtual bool HasChecksum() const;
};


class IParamArrayValue: public RefCount //,public ParamRawValue
{
	public:
	// param array value interface

	virtual RStringB GetValue() const = NULL;
	virtual float GetFloat() const = NULL;
	virtual int GetInt() const = NULL;

	virtual void SetValue(const RStringB &val )= NULL;
	virtual void SetValue(float val)= NULL;
	virtual void SetValue(int val)= NULL;
	virtual void SetFile(ParamFile *file)= NULL;

	virtual void Save(QOStream &f, int indent) const = NULL;
	virtual void SerializeBin(SerializeBinStream &f) = NULL;

/*
	virtual PackedColor GetPackedColor() const = NULL;
	virtual SoundPars GetSoundPars() const = NULL;
*/

	// some handy conversions using virtual functions
	operator float() const {return GetFloat();}
	operator int() const {return GetInt();}
	operator RStringB() const {return GetValue();}
	operator bool() const {return GetInt()!=0;}
/*
	operator PackedColor() const {return GetPackedColor();}
	operator SoundPars() const {return GetSoundPars();}
*/

	// may be array of values
	virtual const IParamArrayValue *GetItem(int i) const = NULL;
	virtual int GetItemCount() const = NULL;
	virtual void CalculateCheckValue(PASumCalculator &sum) const = NULL;

	const IParamArrayValue &operator [] (int i) const {return *GetItem(i);}
};


class ParamClass: public ParamEntry
{
	friend class ParamEntry;
	friend class ParamFile;

	RefArray<ParamEntry> _entries;
	InitPtr<ParamClass> _base;

	InitPtr<ParamClass> _parent;
	//! access right for modification by merge
	ParamAccessMode _access;
	//! owner - can be used to determine if class can be accessed
	RStringB _owner;

public:
	bool IsClass() const {return true;}

	const ParamClass *GetClass( const char *name ) const;
	RStringB GetValue( const char *name ) const;
	RStringB GetValue( const char *name, int i ) const;

	const ParamClass *GetParent() const {return _parent;}

	void Parse( QIStream &f, ParamFile *file); // read until close bracket
	void Save( QOStream &f, int indent ) const; // read until close bracket
	void SerializeBin(SerializeBinStream &f);
	void Compact();

	void Update(ParamClass &source, ParamFile *file);

	ParamEntry *FindEntry
	(
		const char *name, IParamVisibleTest &visible = DefaultAccess
	) const;
	ParamEntry *FindEntryNoInheritance
	(
		const char *name,IParamVisibleTest &visible = DefaultAccess
	) const;
	const ParamEntry &operator >> ( const char *name ) const;
	//const ParamEntry &operator >> ( RString name ) const;

	void Add( const RStringB &name, const RStringB &val );
	void Add( const RStringB &name, float val );
	void Add( const RStringB &name, int val );

	void Delete(const RStringB &name);

	ParamEntry *AddClass( const RStringB &name);
	ParamEntry *AddArray( const RStringB &name);

	void AccessDenied(const char *name);

	virtual void SetAccessModeForAll(ParamAccessMode mode);
	virtual void SetAccessMode(ParamAccessMode mode) {_access=mode;}
	virtual ParamAccessMode GetAccessMode() const {return _access;}
	//! owner - used to check if given entry can be accessed
	virtual const RStringB &GetOwner() const;
	virtual void SetOwner(RString owner, bool subentries=true);
	virtual bool CheckVisible(IParamVisibleTest &visible) const;

	void SetFile(ParamFile *file);

	int GetEntryCount() const {return _entries.Size();}
	const ParamEntry &GetEntry( int i ) const {return *_entries[i];}
	const char *GetBaseName() const {return _base.NotNull() ? _base->GetName() : NULL;}

	bool IsDerivedFrom( const ParamClass &parent ) const;

	virtual void CalculateCheckValue(PASumCalculator &sum) const;
	virtual bool HasChecksum() const;

	//! select class for checking
	const ParamClass *SelectClassForChecking(int index) const;
	int GetNumberOfClassesForChecking() const;

	protected:
	ParamClass();
	ParamClass( const RStringB &name );
	~ParamClass();

	void NewEntry( Ref<ParamEntry> entry );

	ParamEntry *Find
	(
		const char *name, bool parent, bool base,
		IParamVisibleTest &visible
	) const;
	int FindIndex( const char *name ) const;

	void Diagnostics( int indent );


	USE_FAST_ALLOCATOR
};

class GameVarSpace;

//! class of callback functions
class EvaluatorFunctions
{
public:
	EvaluatorFunctions() {}
	virtual ~EvaluatorFunctions() {}

	//! callback function to create variables space
	virtual GameVarSpace *CreateVariables() {return NULL;}
	//! callback function to destroy variables space
	virtual void DeleteVariables(GameVarSpace *vars) {}
	//! callback function to initialize expression evaluator
	virtual void InitEvaluator(GameVarSpace *vars) {}
	//! callback function to deinitialize expression evaluator
	virtual void DoneEvaluator() {}
	//! callback function to load variables from binary stream
	virtual GameVarSpace *LoadVariables(SerializeBinStream &f);
	//! callback function to save variables into binary stream
	virtual void SaveVariables(SerializeBinStream &f, GameVarSpace *vars);
	//! callback function to evaluate float expression
	virtual float EvaluateFloat(const char *expr, GameVarSpace *vars) {return 0;}
	//! callback function to evaluate float expression without game state initialization and deinitialization
	virtual float EvaluateFloatInternal(const char *expr) {return 0;}
	//! callback function to evaluate string expression without game state initialization and deinitialization
	virtual RString EvaluateStringInternal(const char *expr) {return expr;}
	//! callback function to execute expression without game state initialization and deinitialization
	virtual void ExecuteInternal(const char *expr) {}
	//! callback function to set value of float variable without game state initialization and deinitialization
	virtual void VarSetFloatInternal(const char *name, float value, bool readOnly, bool forceLocal) {}
};

//! class of callback functions
#include <El/PreprocC/preprocC.hpp>
    /*class PreprocessorFunctions
{
public:
	PreprocessorFunctions() {}
	virtual ~PreprocessorFunctions() {}

	//! callback function to preprocess of stream content
	virtual bool Preprocess(QOStream &out, const char *name);
};*/

//! class of callback functions
class LocalizeStringFunctions
{
public:
	LocalizeStringFunctions() {}
	virtual ~LocalizeStringFunctions() {}

	//! callback function to load string from stringtable
	virtual RString LocalizeString(const char *str) {return str;}
};

//! class of callback functions
class CRCFunctions
{
public:
	CRCFunctions() {}
	virtual ~CRCFunctions() {}

	//! callback function to add buffer to CRC sum
	virtual void Add(PASumCalculator &sum, const void *buffer, int size) {}
};


class ParamFile: public ParamClass
{
protected:
 	GameVarSpace *_vars;

	static EvaluatorFunctions *_defaultEvalFunctions;
	static PreprocessorFunctions *_defaultPreprocFunctions;
	static LocalizeStringFunctions *_defaultLocalizeStringFunctions;
	static CRCFunctions *_defaultCRCFunctions;

public:
	ParamFile();
	~ParamFile();

	GameVarSpace *GetVariables() {return _vars;}
	static void SetDefaultEvalFunctions(EvaluatorFunctions *f) {_defaultEvalFunctions = f;}
	static void SetDefaultPreprocFunctions(PreprocessorFunctions *f) {_defaultPreprocFunctions = f;}
	static void SetDefaultLocalizeStringFunctions(LocalizeStringFunctions *f) {_defaultLocalizeStringFunctions = f;}
	static void SetDefaultCRCFunctions(CRCFunctions *f) {_defaultCRCFunctions = f;}

	LSError Parse( const char *name);
	LSError Save( const char *name ) const;
	void Parse( QIStream &f);
	void Save( QOStream &f, int ident ) const;

	LSError ParsePlainText( const char *name);
	void ParsePlainText( QIStream &f);

	bool ParseBin( const char *name);
	bool SaveBin( const char *name );

	bool ParseBinOrTxt(const char *name);
	bool ParseBin(QFBank &bank, const char *name);

	void SerializeBin(SerializeBinStream &f);

	void Clear();

	void Reload();

	GameVarSpace *CreateVariables() {return _defaultEvalFunctions->CreateVariables();}
	void DeleteVariables()
	{
		if (_vars)
		{
			_defaultEvalFunctions->DeleteVariables(_vars);
			_vars = NULL;
		}
	}
	void InitEvaluator() {_defaultEvalFunctions->InitEvaluator(_vars);}
	void DoneEvaluator() {_defaultEvalFunctions->DoneEvaluator();}
	void LoadVariables(SerializeBinStream &f) {DeleteVariables(); _vars = _defaultEvalFunctions->LoadVariables(f);}
	void SaveVariables(SerializeBinStream &f) {_defaultEvalFunctions->SaveVariables(f, _vars);}
	float EvaluateFloat(const char *expr) {return _defaultEvalFunctions->EvaluateFloat(expr, _vars);}
	float EvaluateFloatInternal(const char *expr) {return _defaultEvalFunctions->EvaluateFloatInternal(expr);}
	RString EvaluateStringInternal(const char *expr) {return _defaultEvalFunctions->EvaluateStringInternal(expr);}
	void ExecuteInternal(const char *expr) {_defaultEvalFunctions->ExecuteInternal(expr);}
	void VarSetFloatInternal(const char *name, float value, bool readOnly = false, bool forceLocal = false)
	{_defaultEvalFunctions->VarSetFloatInternal(name, value, readOnly, forceLocal);}

	bool Preprocess(QOStream &out, const char *name) {return _defaultPreprocFunctions->Preprocess(out, name);}

	RString LocalizeString(const char *str) {return _defaultLocalizeStringFunctions->LocalizeString(str);}

	static void AddCRC(PASumCalculator &sum, const void *buffer, int size) {_defaultCRCFunctions->Add(sum, buffer, size);}

	USE_FAST_ALLOCATOR
};

#include <Es/Memory/debugNew.hpp>

#endif
