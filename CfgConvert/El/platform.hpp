#ifdef _MSC_VER
#  pragma once
#endif

/**
    @file   platform.hpp
    @brief  Platform-specific definitions/declarations.
    
    Copyright &copy; 2002 by BIStudio (www.bistudio.com)
    @author PE
    @since  4.6.2002
    @date   10.6.2002
*/

#ifndef _PLATFORM_HPP
#define _PLATFORM_HPP

#if _MSC_VER

#  define CCALL __cdecl

    // make for variable local
#  define for if( false ) {} else for

#else

#  define CCALL 
#  define __int64 long long

#endif

#ifdef __GNUC__

#  define __cdecl

#ifndef _GNU_SOURCE
#  define _GNU_SOURCE
#endif
#define _REENTRANT

#define _DED_SERVER 1

#include <typeinfo>

#define OutputDebugString(s) fputs(s,stderr)

#define USE_MALLOC 1

#endif

/* __BEGIN_DECLS should be used at the beginning of your declarations,
   so that C++ compilers don't mangle their names.  Use __END_DECLS at
   the end of C declarations.
*/
#undef __BEGIN_DECLS
#undef __END_DECLS
#ifdef __cplusplus
#  define __BEGIN_DECLS extern "C" {
#  define __END_DECLS }
#else
#  define __BEGIN_DECLS /* empty */
#  define __END_DECLS /* empty */
#endif

#endif
