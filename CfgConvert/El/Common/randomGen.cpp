#include <El/elementpch.hpp>
#include "randomGen.hpp"
//#include "global.hpp"

#include "isaac.hpp"

float RandomGenerator::RandomValue( int seed ) const
{
	return _valueTable(_seedTable(seed))*(1.0/RandomTable::Size);
}

float RandomGenerator::RandomValue() const
{
	return _valueTable(_seed++)*(1.0/RandomTable::Size);
}

int RandomTable::Seed( int x, int z, int y ) const
{
	// make x, z out of order
	const int mask=Size-1;
	x=_table[x&mask];
	z=_table[z&mask];
	y=_table[y&mask];
	// bitwise interleave x, z, and level
	int xz=0;
	// only 12 bits is significant
	for( int i=0; i<4; i++ )
	{
		xz<<=1,xz|=x&1,x>>=1;
		xz<<=1,xz|=z&1,z>>=1;
		xz<<=1,xz|=y&1,y>>=1;
	}
	return xz;
}

#if _MSC_VER
/*!
\patch 1.04 Date 7/16/2001 by Ondra.
- Fixed: random crash during ground collision test
\patch_internal 1.04 Date 7/16/2001 by Ondra.
- _table entries were supposed to be 4B in assembly,
but actually they are 2B
*/

int __declspec(naked) RandomTable::Seed( int x, int z ) const
{
	enum {mask=Size-1};
	_asm
	{
		push ebx
		mov ebx,[esp+4+4]
		push edx
		mov edx,[esp+8+8]
		//mov edx,z
		//mov ebx,x
		//mov edx,z
		and ebx,mask
		and edx,mask
		// fix: ecx table is short, not int
		xor eax,eax
		mov ax,[ecx+ebx*2] // ecx is this
		mov ebx,eax

		xor eax,eax
		mov ax,[ecx+edx*2] // ecx is this
		mov edx,eax

		xor eax,eax // note: shifts can be executed only in V-pipe on Pentium
		shr ebx,1 // one iteration of interleaving
		adc eax,eax
		shr edx,1
		adc eax,eax
		shr ebx,1 // one iteration of interleaving
		adc eax,eax
		shr edx,1
		adc eax,eax
		shr ebx,1 // one iteration of interleaving
		adc eax,eax
		shr edx,1
		adc eax,eax
		shr ebx,1 // one iteration of interleaving
		adc eax,eax
		shr edx,1
		adc eax,eax
		shr ebx,1 // one iteration of interleaving
		adc eax,eax
		shr edx,1
		adc eax,eax
		shr ebx,1 // one iteration of interleaving
		adc eax,eax
		shr edx,1
		rcl eax,1
		pop edx
		pop ebx
		ret 8
		// result returned in eax
	}
}

#else
int RandomTable::Seed( int x, int z ) const
{
	// make x, z out of order
	enum {mask=Size-1};
	// bitwise interleave x and z
	int xz;
	// only 12 bits is significant
	// use inline assembly (use rotation with cary for bit interleaving) 
	x=_table[x&mask];
	z=_table[z&mask];
	for( int i=0; i<6; i++ )
	{
		xz<<=1,xz|=x&1,x>>=1;
		xz<<=1,xz|=z&1,z>>=1;
	}
	return xz;
}
#endif

const int A = 48271;
const int M = 0x7fffffff;
const int Q = M / A;
const int R = M % A;

static int RandomInt( int &seed )
{
	seed&=M; // make seed positive
	int seedDivQ=seed/Q;
	int seedModQ=seed-seedDivQ*Q;

	//seed = A * ( seed % Q ) - R * ( seed / Q );
	seed = A*seedDivQ - R*seedModQ;

	seed&=M; // make seed positive
	return seed;
}


struct RandomOrder
{
	int index;
	int value;
};

static int CmpRandomOrder( const void *o0, const void *o1 )
{
	const RandomOrder *r0=static_cast<const RandomOrder *>(o0);
	const RandomOrder *r1=static_cast<const RandomOrder *>(o1);
	return r0->value-r1->value;
}

RandomTable::RandomTable( int seed )
{
	QTIsaac<> gen;
	gen.srand(seed);
	//RandomOrder mix[Size];
	int i;
	// create 
	//int seed=GlobalTickCount();
	int sum = 0;
	for( i=0; i<Size; i++ )
	{
		//_table[i]=RandomInt(seed)&(Size-1);
		int val = gen.rand()&(Size-1);
		sum += val;
		_table[i] = val;
	}
	//LogF("Mean value %.3f, size %d",sum*(1.0/Size),Size);
	/*
	bool timer=true;
	for( int attempt=16; --attempt>=0; )
	{
		// create random permutation
		//srand(seed);
		for( i=0; i<Size; i++ )
		{
			mix[i].index=i;
			mix[i].value=RandomInt(seed);
		}
		qsort(mix,Size,sizeof(*mix),CmpRandomOrder);
		// permute our table
		int tempTable[Size];
		for( i=0; i<Size; i++ ) tempTable[i]=_table[i];
		for( i=0; i<Size; i++ ) _table[i]=tempTable[mix[i].index];
		if( timer ) seed=GlobalTickCount(),timer=false;
	}
	*/


}

RandomGenerator GRandGen INIT_PRIORITY_NORMAL;

extern DWORD GlobalTickCount();

/*!
\patch_internal 1.05 Date 7/17/2001 by Ondra.
- New: Random generator may be constructed with given seeds.
This was required to have reproducible generator for landscape bumps.
*/

RandomGenerator::RandomGenerator(int seed1, int seed2)
:_seedTable(seed1),_valueTable(120),_seed(seed2)
{
}

RandomGenerator::RandomGenerator()
:_seedTable(GlobalTickCount()),_valueTable(120),_seed(GlobalTickCount()+3256)
{
}

float RandomGenerator::Gauss(float min, float mid, float max) const
{
	float gauss =
	(
		RandomValue() + RandomValue() +
		RandomValue() + RandomValue()
	) * 0.25;
	float delay = 0;
	if (gauss < 0.5)
	{
		float coef = gauss * 2;
		delay = min + (mid - min) * coef;
	}
	else
	{
		float coef = gauss * 2 - 1;
		delay = mid + (max - mid) * coef;
	}
	return delay;
}

float RandomGenerator::PlusMinus(float a, float b) const
{
	return a - b + 2.0f * b * RandomValue();
}
