#ifdef _MSC_VER
#pragma once
#endif

#ifndef _PERFLOG_HPP
#define _PERFLOG_HPP

#ifdef PREPROCESS_DOCUMENTATION
#include "networkObject.hpp"
#endif

#if _ENABLE_PERFLOG

class LogFile
{
	FILE *_file;

	public:
	LogFile();
	LogFile( const char *name );
	void Open( const char *name );
	void Close();
	~LogFile();

	bool IsOpen() const {return _file!=NULL;}

	void Append( const char *text );
	void PrintF( const char *text, ... );
	LogFile &operator << ( const char *txt );
	LogFile &operator << ( int i );
	LogFile &operator << ( float f );
};


struct PerfCounterSlot
{
	char name[64];
	int value;
	int scale; // print scale
	int lastNonZero; // how much frames age counter was non-zero
	bool disabled; // slot printing disabled
	bool enabled; // slot printing forced

	void Frame()
	{
		if (value) lastNonZero = 0;
		else lastNonZero++;
	}

	PerfCounterSlot();
};

TypeIsBinary(PerfCounterSlot);

#include <Es/Containers/smallArray.hpp>

class PerfCounters
{
	bool _constructed;
	bool _first,_line;
	int _lines;
	int _skip;

	bool _enabled;

	VerySmallArray<PerfCounterSlot,128*1024> _counters;
	LogFile _logFile;

	int Find( const char *name );

	public:
	PerfCounters();
	PerfCounters( const char *name );
	~PerfCounters();

	void Open( const char *name );
	void Close();
	void Save( float fps );

	void Enable( bool value=true );
	void Disable(){Enable(false);}
	bool GetEnabled() const {return _enabled;}

	void Reinit();
	void Reset();

	int N() const;
	const char *Name( int i ) const;
	int Value( int i ) const;
	bool Show( int i ) const;
	bool WasNonZero( int i, int frames=30 ) const;

	void SavePCHeaders();
	void SavePCValues();

	PerfCounterSlot *New( const char *name, int scale );

	void Enable( const char *name );
	void Disable( const char *name );
	void SetScale( const char *name, int scale );
};

extern PerfCounters GPerfCounters;
extern PerfCounters GPerfProfilers;

class PerfCounter
{
	PerfCounters *_bank;
	const char *_name;
	PerfCounterSlot *_slot;
	int _scale;

	public:
	PerfCounter( const char *name, PerfCounters *counters, int scale=1 )
	:_bank(counters),_name(name),_scale(scale),_slot(counters->New(name,scale))
	{
	}

	void operator +=( int value );
	int GetValue() const;
	const char *GetName() const {return _name;}

};

// performance counters helper macros

#define COUNTER(name) static PerfCounter Counter_##name(#name,&GPerfCounters)
#define DEF_COUNTER(name,scale) static PerfCounter Counter_##name(#name,&GPerfCounters,scale)

#define ADD_COUNTER(name,value) {COUNTER(name);Counter_##name+=(value);}
#define COUNTER_VALUE(name) (Counter_##name.GetValue())


// performance profilers helper macros

#define COUNTER_P(name) static PerfCounter Counter_##name(#name,&GPerfProfilers)
#define DEF_COUNTER_P(name,scale) static PerfCounter Counter_##name(#name,&GPerfProfilers,scale)

#define ADD_COUNTER_P(name,value) {COUNTER_P(name);Counter_##name+=(value);}
#define COUNTER_VALUE_P(name) (Counter_##name.GetValue())


#else
// not _ENABLE_PERFLOG

#define COUNTER(name)

#define ADD_COUNTER(name,value)
#define COUNTER_VALUE(name) 0

#define DEF_COUNTER(name,scale)

#define COUNTER_P(name)

#define ADD_COUNTER_P(name,value)
#define COUNTER_VALUE_P(name) 0

#define DEF_COUNTER_P(name,scale)

#endif

#endif
