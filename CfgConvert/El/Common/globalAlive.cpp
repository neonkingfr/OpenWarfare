#include <El/elementpch.hpp>

static GlobalAliveInterface GGlobalAliveInterface;
static GlobalAliveInterface *GlobalAlivePtr = &GGlobalAliveInterface;

void GlobalAliveInterface::Set(GlobalAliveInterface *functor)
{
	GlobalAlivePtr = functor;
}

void GlobalAlive()
{
	GlobalAlivePtr->Alive();
}
