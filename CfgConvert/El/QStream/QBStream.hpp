#ifdef _MSC_VER
#pragma once
#endif

#ifndef _QBSTREAM_HPP
#define _QBSTREAM_HPP

#include "QStream.hpp"


class QFBank;

//! check if given bank/file is accessible in given context
/*!
	In future this will probably be used also to open the file using given access.
*/

class IQFBankContext
{
	public:
	//! check if file can be accessed
	virtual bool IsAccessible(QFBank *bank) const = NULL;
};

//! stream that is opened from some file bank
class QIFStreamB: public QIFStream
{
	const QFBank *_bank;


	public:
	static QFBank *AutoBank( const char *name );
	static bool FileExist( const char *name, IQFBankContext *context=NULL );

	static void ClearBanks();
	
	QIFStreamB();

	void AutoOpen( const char *name, IQFBankContext *context=NULL ); // autoselect bank/file

	void open( const QFBank &bank, const char *name ); // open and preload file
	bool IsFromBank(const QFBank *bank) const;
};


class FindBank
{
#ifdef _WIN32
	char _wild[1024];
	// check all "pb?" file banks
	void *_info;
	long _handle;

#else
    DIR *dir;
    struct dirent *entry;
#endif
	public:
	FindBank();
	~FindBank();

	bool First(const char *path);
	bool Next();
	void Close();

	const char *GetName() const;
};

class BankContextBase: public RefCount
{
};

typedef bool (*OpenCallback)(QFBank *bank, BankContextBase *context);

class BankList: public AutoArray<QFBank>
{
	public: 
	void Load
	(
		const RString &path,
		const RString &bankPrefix,const RString &bName, bool emptyPrefix,
		OpenCallback beforeOpen=NULL, OpenCallback afterOpen=NULL,
		BankContextBase *context=NULL
	);
	void Unload
	(
		const RString & bankPrefix,const RString &bName, bool emptyPrefix
	);
	void Lock(const RString &prefix);
	void Unlock(const RString &prefix);
	void SetLockable(const RString &prefix, bool lockable);

	//! unload all unused bank files
	bool UnloadUnused();
};

extern BankList GFileBanks;

int CmpStartStr( const char *str, const char *start );

#include "fileinfo.h"

struct FileInfoO
{
	//char name[NAME_LEN];
	RStringB name;

	int compressedMagic;
	int uncompressedSize;
	long startOffset;
	long time;
	long length;
	#if _ENABLE_PATCHING
	bool loadFromFile; // fast patching enabled
	#endif

	FileInfoO()
	{
		#if _ENABLE_PATCHING
		loadFromFile=false;
		#endif
	}

	const char *GetKey() const {return name;}
};

#include <Es/Containers/array.hpp>

TypeIsMovable(FileInfoO);

typedef MapStringToClass<FileInfoO, AutoArray<FileInfoO> > FileBankType;

typedef void *WINHANDLE;

// interface for creating file log

class IBankLog: public RefCount
{
	public:
	virtual void Init(const char *bankName) = NULL;
	virtual void LogFileOp(const char *name) = NULL;
	virtual void Flush(const char *bankName) = NULL;
};

class FileBufferMapped;
class FileBufferOverlapped;

//! any bank may have several properties attached
struct QFProperty
{
	RString name;
	RString value;
};

TypeIsMovableZeroed(QFProperty)

//! virtual read-only file system
/*!
Access to files in QFBank is much faster and
they are stored more efficiently than when using OS services.
*/

class QFBank;

//! class of callback functions
class QFBankFunctions
{
public:
	QFBankFunctions() {}
	virtual ~QFBankFunctions() {}

	//! callback function to return error description
	virtual bool FreeUnusedBanks(size_t sizeNeeded) const;
};

//! filebank compression/encryption interface
/*!
Instance of IFilebankEncryption already should contain any information
required to encrypt/decrypt data
*/

class IFilebankEncryption: public RefCount
{
	public:
	//! decode block of data
	/*!
	\param dst pointer to data destination
	\param lensb destination length
	\param in encoded data stored in stream
	*/
	virtual bool Decode( char *dst, long lensb, QIStream &in ) = NULL;
	//! encode block of data
	/*!
	\param out stream into which encoding is done
	\param dst pointer to source data
	\param lensb length of source data
	*/
	virtual void Encode( QOStream &out, const char *dst, long lensb ) = NULL;
};

//! create encryptor that is able to encrypt/decrypt given bank
Ref<IFilebankEncryption> CreateFilebankEncryption(const char *name, const void *context);

//! register another encryptor
void RegisterFilebankEncryption
(
	const char *name, IFilebankEncryption *(*createFunction)(const void *context)
);

#define MT_SAFE 0
class QFBank
{

	#if MT_SAFE
	mutable CriticalSection _lock;
	#endif
	#if !_RELASE
		mutable bool _serialize; // help finding bugs
	#endif
	Ref<IBankLog> _log;

	friend class QIFStream;
	private:

	// remember parameters necessary for opening
	//! name provided by open call
	RString _openName;
	//! callback provided by open call
	OpenCallback _openBeforeOpenCallback;
	//! context provided by open call
	Ref<BankContextBase> _openContext;

	//! time of last open - can be used for automated unloading of unused banks
	//DWORD _lastOpen;
	//! during some operations (file from bank being used) unload is not possible
	//int _openCount;
	// ! lock bank - it cannot be unloaded while locked
	mutable bool _locked;
	//! only lockable bank can be locked/unlocked
	bool _lockable;
			
	RString _prefix;
	FileBankType _files;
	AutoArray<QFProperty> _properties;

	//int _handle;
	Ref<IFileBuffer> _fileAccess;
	//! handle used for normal file access
	WINHANDLE _handle;
	//! handle used for overlapped file access
	WINHANDLE _handleOverlapped;
	mutable long _pos;
	mutable long _wantPos;

	//! set true when attempt to open (DoOpen) failed
	bool _error;

	//! modular functions for various event
	static QFBankFunctions *_defaultFunctions;
	static void SetDefaultFunctions(QFBankFunctions *f) {_defaultFunctions = f;}
	
	public:
	QFBank();
	~QFBank();
	RString GetPrefix() const {return _prefix;}
	void ScanPatchFiles(RString prefix, RString subdir);
	void SetPrefix(RString prefix);
	//! open bank
	bool open
	(
		RString name, OpenCallback beforeOpen=NULL,
		BankContextBase *context=NULL
	);
	//! load bank - physically performs action
	bool Load();
	//! open bank and mark is as locked (cannot be un-opened)
	void Lock() const;
	//! set lockable starus
	void SetLockable(bool lockable) {_lockable=lockable;}
	//! get lockable starus
	bool GetLockable() const {return _lockable;}
	//! mark bank as unlocked and unload if possible
	void Unlock() const;
	//! check if it can be unloaded
	bool IsLocked() const {return _locked;}
	//! check if it can be unloaded (if it is used, it cannot be unloaded)
	bool CanBeUnloaded() const;
	//! load bank - enable modification of necessary fields
	bool Load() const
	{
		return const_cast<QFBank *>(this)->Load();
	}
	//! unload bank
	void Unload();
	//! unload bank - enable modification of necessary fields
	void Unload() const
	{
		if (!_handle) return;
		const_cast<QFBank *>(this)->Unload();
	}
	void Clear(); // release all files
	void close() {Clear();}

	bool error() const;

	const RString &GetProperty(const RString &name) const;

	const FileInfoO &FindFileInfo( const char *name ) const; // seek to beginning of some file
	bool FileExists( const char *name ) const; // check if file exists
	// low level access to bank
	//int AlignedRead( char *buf, long size ) const; // read from bank
	//int AlignSize() const {return _align;}
	//int InSectorOffset() const {return _wantPos&(_align-1);}
	void Read( char *buf, long size, const char *name ) const; // read from bank
	void Seek( long pos ) const; // seek to given position

	//! read file - uncompress if necessary
	Ref<IFileBuffer> Read( const char *file ) const;
	//! read file using overlapped I/O - uncompress if necessary
	Ref<IFileBuffer> ReadOverlapped( const char *file ) const;
	//! get file order in the bank
	int GetFileOrder(const char *file);

	void ForEach
	(
		void (*Func)(const FileInfoO &fi, const FileBankType *files, void *context),
		void *context
	) const; // call Func for all files
	static bool IsNull(const FileInfoO &value) {return FileBankType::IsNull(value);}
	static bool NotNull(const FileInfoO &value) {return FileBankType::NotNull(value);}
	static FileInfoO &Null() {return FileBankType::Null();}

	bool BufferOwned(const FileBufferMapped *buffer) const; // check if file exists
	bool BufferOwned(const FileBufferOverlapped *buffer) const; // check if file exists

	static bool FreeUnusedBanks(size_t sizeNeeded)
	{
		return _defaultFunctions->FreeUnusedBanks(sizeNeeded);
	}
};
TypeIsMovable(QFBank)

extern bool GUseFileBanks;
extern bool GLogFileOps;
extern RString GFileBankPrefix;

#endif


