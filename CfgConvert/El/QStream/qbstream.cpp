#include <El/elementpch.hpp>
#include "QBStream.hpp"
#ifdef _WIN32
  #include <direct.h>
  #include <io.h>
#endif
#include <Es/Strings/bstring.hpp>

#include <Es/Common/win.h>

#if MT_SAFE
#define EXCLUSIVE() ScopeLockSection lock(_lock)
#else
#define EXCLUSIVE()
#endif

QFBank::QFBank()
{
	EXCLUSIVE();
	_handle=NULL;
	_handleOverlapped=NULL;
	#if !_RELEASE
		_serialize=false;
	#endif
	_error = true; // no open called yet
	_locked = true;
	_lockable = false;
}

#define WIN_DIR '\\'
#define UNIX_DIR '/'

#if !_RELEASE
#define BEG_SERIALIZE {Assert(!_serialize);_serialize=true;}
#define END_SERIALIZE {Assert(_serialize);_serialize=false;}
#else
#define BEG_SERIALIZE {}
#define END_SERIALIZE {}
#endif

#if defined _XBOX
  #define USE_MAPPING 0
#else
  #ifdef NO_MMAP
    #define USE_MAPPING 0
  #else
    #define USE_MAPPING 1
  #endif
#endif

#if USE_MAPPING
  #include "fileMapping.hpp"
#endif

#ifdef _WIN32
  #include "fileOverlapped.hpp"
#endif

static int LoadInt(HANDLE f )
{
	int i = 0;
	DWORD rd = 0;
	ReadFile(f,&i,sizeof(i),&rd,NULL);
	if (rd!=sizeof(i)) i=0;
	return i;
}

static int LoadInt(QIStream &f)
{
	int i = 0;
	f.read(&i,sizeof(i));
	if (f.fail()) i = 0;
	return i;
}

// see also FileBank.cpp
/*!
	\patch_internal 1.26 Date 10/03/2001 by Ondra
	- Fixed: Crash when bank header was corrupted.
*/

static void LoadFileInfo( FileInfoO &i, HANDLE f )
{
	// read zero terminated name
	char name[512];
	char *n=name;
	int maxLen=sizeof(name)-1;
	DWORD rd;
	for (int l=0; l<maxLen; l++)
	{
		char c;
		ReadFile(f,&c,sizeof(c),&rd,NULL);
		if (rd!=1)
		{
			// error during file reading
			i.name = "";
			i.startOffset = 0;
			i.length = 0;
			return;
		}
		if (!c) break;
		*n++=c;
	}
	*n=0; // zero terminate in any case
	strlwr(name);
	i.name=name;
	i.compressedMagic = LoadInt(f);
	i.uncompressedSize = LoadInt(f);
	i.startOffset = LoadInt(f);
	i.time = LoadInt(f);
	i.length = LoadInt(f);
}

static void LoadFileInfo( FileInfoO &i, QIStream &f)
{
	// read zero terminated name
	char name[512];
	char *n=name;
	int maxLen=sizeof(name)-1;
	for (int l=0; l<maxLen; l++)
	{
		int c = f.get();
		if (c<0)
		{
			// error during file reading
			i.name = "";
			i.startOffset = 0;
			i.length = 0;
			return;
		}
		if (!c) break;
		*n++=c;
	}
	*n=0; // zero terminate in any case
	strlwr(name);
	i.name=name;
	i.compressedMagic = LoadInt(f);
	i.uncompressedSize = LoadInt(f);
	i.startOffset = LoadInt(f);
	i.time = LoadInt(f);
	i.length = LoadInt(f);
}

bool GLogFileOps=false;

// simple logging - log what is used, do not check for double files

class QFBankLog: public IBankLog
{
	FILE *_file;
	FindArray<RString> _files; // files aready there

	public:
	void Init(const char *bankName);
	void LogFileOp(const char *name);
	void Flush(const char *bankName);

	QFBankLog();
	~QFBankLog();

	private:
	void Close();
};

void QFBankLog::Init(const char *bankName)
{
	Close();

	char logName[256];
	strcpy(logName,"FilesUsed");
	::CreateDirectory(logName,NULL);
	strcat(logName,"\\");
	strcat(logName,bankName);
	if (logName[strlen(logName)-1]=='\\') logName[strlen(logName)-1] = 0;
	strcat(logName,".log");

#ifndef _WIN32
  unixPath(logName);
#endif
	FILE *file = fopen(logName,"r");
	if (file)
	{
		// read filenames already there
		for(;;)
		{
			char buf[1024];
			*buf = 0;
			fgets(buf,sizeof(buf),file);
			if (!*buf) break;
			if (buf[strlen(buf)-1]=='\n') buf[strlen(buf)-1]=0;
			_files.Add(buf);
		}
		fclose(file),file = NULL;
	}
	_file = fopen(logName,"a+");
}

void QFBankLog::LogFileOp(const char *name)
{
	if (!_file) return;
	// check if file is already there
	if (_files.Find(name)>=0) return;
	fprintf(_file,"%s\n",name);
}

void QFBankLog::Flush(const char *bankName)
{
	if (_file) fflush(_file);
}

QFBankLog::QFBankLog()
{
	_file = NULL;
}

void QFBankLog::Close()
{
	if (_file)
	{
		fclose(_file);
		_file = NULL;
	}
}

QFBankLog::~QFBankLog()
{
	Close();
}

RString LoadFromFile(HANDLE file)
{
	BString<1024> buf;
	for(;;)
	{
		char c[2];
		DWORD size = sizeof(char);
		BOOL ok = ReadFile(file,c,size,&size,NULL);
		if (!ok || size!=1) break;
		if (c[0]==0) break;
		buf += c;
	}
	return RString(buf);
}

bool SaveToFile(HANDLE file, RString value)
{
	int len = value.GetLength();
	DWORD size = sizeof(len);
	BOOL ok = WriteFile(file,&len,size,&size,NULL);
	return ok!=FALSE;
}
/*!
\param name filename of the bank
\param callback function called after header is loaded,
used to determine if bank should be loaded
\param context context passed to callback
\return true when bank was succesfully opened
Note: bank opening may be deferred.
Bank may only remmeber all necessary parameter when this function is called
and actual "opening" may be performed later. This should be almost transparent to user.
*/

bool QFBank::open
(
	RString name, OpenCallback beforeOpen, BankContextBase *context
)
{
	char fullName[1024];
	#ifndef _XBOX
	strcpy(fullName,name);
	#else
	strcpy(fullName,"d:\\");
	strcat(fullName,name);
	#endif
	strcat(fullName,".pbo");

#ifdef _WIN32
        
	HANDLE handle=CreateFile
	(
		fullName,GENERIC_READ,FILE_SHARE_READ,
		NULL,OPEN_EXISTING,FILE_FLAG_RANDOM_ACCESS,NULL
	);

	if( !handle || handle==INVALID_HANDLE_VALUE )
	{
		ErrorMessage("Cannot open file '%s'",fullName);
		return false;
	}
	else
	{
		CloseHandle(handle);
	}
	_openName = fullName;

#else

  _openName = fullName;
	unixPath(fullName);
	int handle = ::open(fullName,O_RDONLY);
	if ( handle < 0 ) {
	    ErrorMessage("Cannot open file '%s': %s",fullName,strerror(errno));
	    _openName = "";
	    return false;
	    }
	::close(handle);
	
#endif

	_openBeforeOpenCallback = beforeOpen;
	_openContext = context;
	_files.Clear();
	_error = false; // no open called yet
	//_openCount = 0;
	return true;
}

/*!
All banks are locked by default. If application bank can be unloaded, it must unlock it.
*/

void QFBank::Lock() const
{
	_locked = true;
	Load();
}

void QFBank::Unlock() const
{
	_locked = false;
	if (_fileAccess && _fileAccess->RefCounter()==1)
	{
		Unload();
	}
}

bool QFBank::CanBeUnloaded() const
{
	return _fileAccess && _fileAccess->RefCounter()==1;
}


static QFBankFunctions GQFBankFunctions;

QFBankFunctions *QFBank::_defaultFunctions = &GQFBankFunctions;

static inline RStringB ConvertDirSlash(RStringB name)
{
  const char *change=strchr(name,UNIX_DIR);
  if (!change) return name;
  // make sure name is mutable
  RString mutableName = name;
  char *mutName = mutableName.MutableData();
  for(;;)
  {
    char *change=strchr(mutName,UNIX_DIR);
    if (!change) break;
    *change=WIN_DIR;
  }
  return mutableName;
}

bool QFBank::Load()
{
	if (!_locked)
	{
		RptF("Cannot open bank %s that is not locked",(const char *)_prefix);
		return false;
	}
	if (_error) return false;
	// TODO: implement some timing. GlobalTickCount is not available here.
	//_lastOpen = GlobalTickCount();
	if (_handle) return true;
	_files.Clear();
	EXCLUSIVE();
	// note: name should not contain extension
	// automatic optimal bank type is performed with different extensions
	// like .pbf and .pbo
	LogF("Load bank %s",(const char *)_openName);
	Assert(!_fileAccess);

#ifdef _WIN32

	_handle=CreateFile
	(
		_openName,GENERIC_READ,FILE_SHARE_READ,
		NULL,OPEN_EXISTING,FILE_FLAG_RANDOM_ACCESS,NULL
	);

	if( !_handle || _handle==INVALID_HANDLE_VALUE )
	{
		ErrorMessage("Cannot open file '%s'",(const char *)_openName);
		_handle = NULL;
	}
	else
	{
		_handleOverlapped=CreateFile
		(
			_openName,GENERIC_READ,FILE_SHARE_READ,
			NULL,OPEN_EXISTING,FILE_FLAG_OVERLAPPED,NULL
		);
		if( !_handleOverlapped || _handleOverlapped==INVALID_HANDLE_VALUE )
		{
			_handleOverlapped = NULL;
			LogF("Cannot open overlapped file access on %s",(const char *)_openName);
		}
	}

#else
  LocalPath(fileName,(const char*)_openName);
	int handle = ::open(fileName,O_RDONLY);
	if ( handle < 0 ) {
	    ErrorMessage("Cannot open file '%s': %s",fileName,strerror(errno));
	    _handle = NULL;
	    }
	else
	    _handle = (WINHANDLE)handle;
#endif

	_pos=_wantPos=0;
	int startOffset=0;
	FileInfoO info;
	for(;;)
	{
		// from optimized bank different loading
		LoadFileInfo(info,(HANDLE)_handle);
		
		info.startOffset = startOffset;
		startOffset+=info.length;
		if( info.name.GetLength()<=0 ) break;

		#ifdef _WIN32
    info.name = ConvertDirSlash(info.name);
		#endif
		_files.Add(info);
	}
	// if bank is empty, it may be "new bank" with product identification
	// check if there is normal terminator, or special
	if
	(
		_files.NItems()==0 &&
		info.compressedMagic==VersionMagic &&
		info.length==0 &&
		info.time==0
	)
	{
		// read properties
		for(;;)
		{
			RString name = LoadFromFile((HANDLE)_handle);
			if (name.GetLength()==0)
			{
				break;
			}
			RString value = LoadFromFile((HANDLE)_handle);
			QFProperty &prop = _properties.Append();
			prop.name = name;
			prop.value = value;
		}

		// call callback function
		if(_openBeforeOpenCallback)
		{
			bool ok = _openBeforeOpenCallback(this,_openContext);
			if (!ok)
			{
				CloseHandle(_handle),_handle=NULL;
				_error = true;
				return false;
			}
		}

		// read normal file headers
		RString encryption = GetProperty("encryption");
		if (encryption.GetLength()>0)
		{
			// we need to load encrypted headers
			// for this we need to know headers encrypted size
			// load decoded size
			int headersSize = LoadInt((HANDLE)_handle);
			int headersEncodedSize = LoadInt((HANDLE)_handle);
			// read encoded headers into memory
			Temp<char> headers(headersEncodedSize);
			DWORD rd = 0;
			ReadFile(_handle,headers.Data(),headers.Size(),&rd,NULL);
			if (rd==headers.Size())
			{
				Ref<IFilebankEncryption> ss = CreateFilebankEncryption(encryption,NULL);
				if (ss)
				{
					QIStream headersEncoded(headers.Data(),headers.Size());
					Temp<char> headerDecodedData(headersSize);
					ss->Decode(headerDecodedData.Data(),headerDecodedData.Size(),headersEncoded);
					QIStream headersDecoded(headerDecodedData.Data(),headerDecodedData.Size());
					for(;;)
					{
						// from optimized bank different loading
						LoadFileInfo(info,headersDecoded);
						
						info.startOffset = startOffset;
						startOffset+=info.length;
						if( info.name.GetLength()<=0 ) break;

						#if 1 //_MSC_VER
            info.name = ConvertDirSlash(info.name);
						#endif
						_files.Add(info);
					}
				}
			}
		}
		else
		{
			for(;;)
			{
				// from optimized bank different loading
				LoadFileInfo(info,(HANDLE)_handle);
				
				info.startOffset = startOffset;
				startOffset+=info.length;
				if( info.name.GetLength()<=0 ) break;

				#if 1 //_MSC_VER
        info.name = ConvertDirSlash(info.name);
				#endif
				_files.Add(info);
			}
		}
	}
	else
	{
		// call callback function
		if(_openBeforeOpenCallback)
		{
			bool ok = _openBeforeOpenCallback(this,_openContext);
			if (!ok)
			{
				_files.Clear();
				CloseHandle(_handle),_handle=NULL;
				_error = true;
				return false;
			}
		}
	}


	int headerSize = SetFilePointer(_handle,0,NULL,FILE_CURRENT);
	BEG_SERIALIZE
	//Assert( _pos==SetFilePointer(_handle,0,NULL,FILE_CURRENT) );
	// filemapping uses offset from end of header
	// direct access uses file offset
	/**/
	if (_files.NItems() > 0)
	{
		// !!! avoid GetTable when NItems == 0
		for (int i=0; i<_files.NTables(); i++)
		{
			AutoArray<FileInfoO> &container = _files.GetTable(i);
			for (int j=0; j<container.Size(); j++)
			{
				FileInfoO &info = container[j];
				info.startOffset += headerSize;
			}
			//container.Compact();
		}
	}
	/**/
	startOffset+=headerSize;
	// check integrity - try to seek end of file
	DWORD checkEof=SetFilePointer(_handle,startOffset,NULL,FILE_BEGIN);
	if( (int)checkEof!=startOffset ) ErrorMessage("Data file too short '%s'.",(const char *)_openName);
	else _pos=checkEof;

	// get file mapping access
	#if USE_MAPPING
	Retry:
	// do not map headers, only file content
	_fileAccess = new FileBufferMapped((HANDLE)_handle);
	if (_fileAccess->GetError())
	{
		// try to close any bank that may be closed
		_fileAccess.Free();
		bool ok = _defaultFunctions->FreeUnusedBanks(checkEof-headerSize);
		if (ok) goto Retry;
		// nothing can be freed now, we have to terminate
		ErrorMessage("Cannot memory-map file '%s'",(const char *)_openName);

		CloseHandle(_handle),_handle=NULL;
		END_SERIALIZE
		_error = true;
	}
	#endif

	ScanPatchFiles(_prefix,RString());

	END_SERIALIZE
	return true;
}

void QFBank::Unload()
{
	// if error, there is nothing to undo
	if (_error) return;
	// if there is no handle, there is nothing to undo
	if (!_handle) return;
	// unload this bank
	if (_fileAccess && _fileAccess->RefCounter()>1)
	{
		// if some file from bank is still used, we cannot unload it
		LogF
		(
			"Cannot unload bank %s, %d files are still open",
			(const char *)_openName,_fileAccess->RefCounter()-1
		);
		return;
	}
	LogF("Unload bank %s",(const char *)_openName);
	Clear();
}

bool QFBank::error() const
{
	// if bank was not opened yet, it cannot have any fatal errors
	if (!_error) return false;
	// if there is no handle, there is some fatal error
 	if( !_handle || (HANDLE)_handle==INVALID_HANDLE_VALUE ) return true;
	return false;
}

const RString &QFBank::GetProperty(const RString &name) const
{
	for (int i=0; i<_properties.Size(); i++)
	{
		if (!strcmpi(_properties[i].name,name))
		{
			return _properties[i].value;
		}
	}
	const static RString empty;
	return empty;
}

void QFBank::ScanPatchFiles(RString prefix, RString subdir)
{
#if _ENABLE_PATCHING
	// check if there is folder containing patch files
	BString<1024> wildname;
	strcpy(wildname,prefix);
	//TerminateBy(wildname,"\\");
	strcat(wildname,subdir);
	
#ifdef _WIN32

	strcat(wildname,"*.*");
	_finddata_t find;
	long hf = _findfirst(wildname,&find);
	if (hf>=0)
	{
		do
		{
			char lowName[256];
			strcpy(lowName,find.name);
			strlwr(lowName);
			RString name = lowName;
			//LogF("Checking file %s",find.name);
			if (find.attrib&_A_SUBDIR)
			{
				if (!strcmp(find.name,".") || !strcmp(find.name,"..")) continue;
				ScanPatchFiles(prefix,subdir + name + RString("\\"));
			}
			else
			{
				RString subname = subdir+name;
				const FileInfoO &file = _files[subname];
				if (_files.NotNull(file))
				{
					LogF("Plain file version of %s used",(const char *)(subname));
					FileInfoO fileSet = file;
					fileSet.loadFromFile = true;
					_files.Add(fileSet);
				}
			}


		} while (_findnext(hf,&find)==0);
		
		_findclose(hf);
	}

#else

    unixPath((char*)(const char*)wildname);
    int len = strlen(wildname);
    if ( len > 0 && wildname[len-1] == UNIX_DIR )
        wildname[--len] = (char)0;
    DIR *dir = opendir(wildname);
    if ( !dir ) return;
    struct dirent *entry;
    while ( (entry = readdir(dir)) ) {  // process one directory entry
        if ( entry->d_name[0] == '.' &&
             (!entry->d_name[1] ||
              entry->d_name[1] == '.' &&
              !entry->d_name[2]) )
            continue;
        // stat the entry <= subdirectories must be handled differently
        wildname += "/";
        wildname += entry->d_name;
        struct stat st;
        if ( !stat(wildname,&st) ) {
            if ( S_ISDIR(st.st_mode) ) {// directory
                ScanPatchFiles(prefix,subdir + entry->d_name);
                }
            else {                      // regular file
                RString subname = subdir + entry->d_name;
                const FileInfoO &file = _files[subname];
                if (_files.NotNull(file)) {
                    LogF("Plain file version of %s used",(const char *)(subname));
                    FileInfoO fileSet = file;
                    fileSet.loadFromFile = true;
                    _files.Add(fileSet);
                    }
                }
            }
        wildname[len] = (char)0;
        }
    closedir(dir);

#endif

#endif
}

void QFBank::SetPrefix(RString prefix)
{
	_prefix = prefix;
	// create log
	if (GLogFileOps)
	{
		_log = new QFBankLog();
		_log->Init(prefix);
	}

}

const FileInfoO &QFBank::FindFileInfo( const char *name ) const // check if file exists
{
	EXCLUSIVE();
	char lowName[128];
	strcpy(lowName,name);
	strlwr(lowName);
	return _files[lowName];
}

bool QFBank::FileExists( const char *name ) const // check if file exists
{
	if (!Load()) return false;
	const FileInfoO &info = FindFileInfo(name);
	return NotNull(info);
}

void QFBank::Seek( long pos ) const
{
	EXCLUSIVE();
	_wantPos=pos;
}

void QFBank::Read( char *buf, long size, const char *name ) const
{
	Assert(!_error);
	BEG_SERIALIZE
	EXCLUSIVE();	// seek to wanted position
	if( _pos!=_wantPos )
	{
		DWORD nPos=SetFilePointer(_handle,_wantPos,0,FILE_BEGIN);
		if( (int)nPos!=_wantPos )
		{
			ErrorMessage("Read: Data file seek error (%s: %d,%d).",name,nPos,_wantPos);
		}
		else _pos=nPos;
	}
	// read into the temporary buffer
	DWORD bytes;
	if( !ReadFile(_handle,buf,size,&bytes,NULL) || (int)bytes!=size )
	{
		ErrorMessage("Data file read error (%s).",name);
	}
	_pos+=bytes;
	_wantPos+=size;
	END_SERIALIZE
}

#include "fileCompress.hpp"


/*!
Function result can be used to determine relative order of two files in bank.
When two files return same result, their order is unknown.
When one file returns higher smaller than another one,
it is nearer the beginning of the bank.

Note: Function results need not be continuous for any bank. When i is returned
for some file, there is no guranntee i+1 or i-1 will be returned for any other file.

The purpose of this function is to give anyone loading many files opportunity
to optimize access order to achieve as sequential access as possible.
*/

int QFBank::GetFileOrder(const char *file)
{
	const FileInfoO &info = FindFileInfo(file);
	if (IsNull(info)) return 0;
	return info.startOffset;
}

/*!
	\patch 1.05 Date 07/17/2001 by Ondra
	- Improved: Better diagnostics of corrupted datafiles.
*/

Ref<IFileBuffer> QFBank::Read( const char *name ) const
{
	if (!Load()) return NULL;
	// log information about file being opened
	// even when operation is not sucessful, file is logged
	if (_log)
	{
		// log name
		_log->LogFileOp(name);
	}

	const FileInfoO &info = FindFileInfo(name);
	if (IsNull(info)) return NULL;
	#if _ENABLE_PATCHING
	if (info.loadFromFile)
	{
		// patch file provided - use it
		char fullName[512];
		strcpy(fullName,GetPrefix());
		strcat(fullName,name);
		#if USE_MAPPING
		Ref<FileBufferMapped> data = new FileBufferMapped(fullName);
		#else
		Ref<FileBufferLoaded> data = new FileBufferLoaded(fullName);
		#endif
		if (!data->GetError()) return (IFileBuffer*)data;
	}
	#endif

	if( info.compressedMagic==CompMagic ) // some compression
	{
		Seek(info.startOffset);
		QIStream inBuf;
		// read compressed data into temporary buffer
		Temp<char> cData(info.length);
		inBuf.init(cData,info.length);
		Read(cData.Data(),info.length,name);
		// uncompress
		Ref<FileBufferMemory> data=new FileBufferMemory(info.uncompressedSize);
		SSCompress ss;
		if (!ss.Decode(data->GetWritableData(),info.uncompressedSize,inBuf))
		{
			RptF("Error decoding %s from %s",name,(const char *)_prefix);
			return NULL;
		}
		return (IFileBuffer*)data;
	}
	else if (info.compressedMagic==EncrMagic)
	{
		Seek(info.startOffset);
		QIStream inBuf;
		// read compressed data into temporary buffer
		Temp<char> cData(info.length);
		inBuf.init(cData,info.length);
		Read(cData.Data(),info.length,name);
		// ask compression manager to create encryptor/decryptor
		// uncompress
		Ref<FileBufferMemory> data=new FileBufferMemory(info.uncompressedSize);
		Ref<IFilebankEncryption> ss = CreateFilebankEncryption(GetProperty("encryption"),NULL);
		if (!ss || !ss->Decode(data->GetWritableData(),info.uncompressedSize,inBuf))
		{
			RptF("Error decoding %s from %s",name,(const char *)_prefix);
			return NULL;
		}
		return (IFileBuffer*)data;
	}
	else if (info.compressedMagic==0)
	{
		// no compression

		#if USE_MAPPING
			// use memory mapped file subsection
			Ref<IFileBuffer> data = new FileBufferSub
			(
				_fileAccess,info.startOffset,info.length
			);
			return data;

		#else
			Seek(info.startOffset);
			// read directly from file 
			Ref<FileBufferMemory> data=new FileBufferMemory(info.length);
			Read(data->GetWritableData(),info.length,name);
			return (FileBufferMemory*)data;
		#endif

	}
	else
	{
		// some unknown compression manager
		Fail("Unknown compression manager");
		return NULL;
	}
}

Ref<IFileBuffer> QFBank::ReadOverlapped( const char *file ) const
{
	if (!Load()) return NULL;
	if (!_handleOverlapped)
	{
		// overlapped IO not supported - fall back to normal case
		return Read(file);
	}
#ifdef _WIN32
	// TODO: support overlapped IO compression
	const FileInfoO &info = FindFileInfo(file);
	if (IsNull(info)) return NULL;
	#if _ENABLE_PATCHING
	if (info.loadFromFile)
	{
		return Read(file);
	}
	#endif
	if (info.compressedMagic==CompMagic)
	{
		Ref<IFileBuffer> data = new FileBufferOverlapped
        (
            _handleOverlapped,info.uncompressedSize,info.startOffset,info.length
        );
		return data;
	}
	// create overlapped IO file buffer
	Ref<IFileBuffer> data = new FileBufferOverlapped
	(
		_handleOverlapped,info.startOffset,info.length
	);
	return data;
#else
    return Read(file);
#endif
}

#if USE_MAPPING
bool QFBank::BufferOwned(const FileBufferMapped *buffer) const
{
	return buffer->GetFileHandle()==(HANDLE)_handle;
}
#endif

bool QFBank::BufferOwned(const FileBufferOverlapped *buffer) const
{
#ifdef _WIN32
	return buffer->GetFileHandle()==_handleOverlapped;
#else
    return false;
#endif
}

//typedef MapStringToClass<FileInfoO, AutoArray<FileInfoO> > FileBankType;
//ForEach(void (*Func)(const Type &, const MapStringToClass *, void *), void *context=NULL) const;

void QFBank::ForEach
(
	void (*Func)(const FileInfoO &fi, const FileBankType *files, void *context),
	void *context
) const
{
	if (!Load()) return;
	// call Func for all files
	EXCLUSIVE();
	_files.ForEach(Func,context);
}

void QFBank::Clear()
{
	EXCLUSIVE();
	// clear variables that are not part of Global structure
	_files.Clear();
	_fileAccess.Free();
	if (_handle) CloseHandle(_handle),_handle=NULL;
	if (_handleOverlapped) CloseHandle(_handleOverlapped),_handleOverlapped=NULL;
}

QFBank::~QFBank()
{
	EXCLUSIVE();
	Clear();
}

void QIFStreamB::open( const QFBank &bank, const char *name )
{
	_error=LSUnknownError;
	_sharedData=bank.Read(name);
	if( !_sharedData )
	{
		return;
	}
	init(_sharedData->GetData(),_sharedData->GetSize());
	_bank = &bank;
}


BankList GFileBanks;
bool GUseFileBanks;
RString GFileBankPrefix; // HW config dependent banks

bool QFBankFunctions::FreeUnusedBanks(size_t sizeNeeded) const
{
	return GFileBanks.UnloadUnused();
}

#ifdef _WIN32

FindBank::FindBank()
{
	_handle = -1;
}

FindBank::~FindBank()
{
	Close();
}

bool FindBank::First(const char *path)
{
	#ifdef _XBOX
		strcpy(_wild,"d:\\");
		strcat(_wild,path);
	#else
		strcpy(_wild,path);
	#endif
	strcat(_wild,"\\*.pbo");
	_info = new _finddata_t;
	_handle = _findfirst(_wild, (_finddata_t *)_info);
	return _handle != -1;
}

bool FindBank::Next()
{
	if (!_info || _handle==-1) return false;
	return _findnext(_handle, (_finddata_t *)_info)==0;
}

void FindBank::Close()
{
	if (_info)
	{
		delete (_finddata_t *)_info;
		_info = NULL;
	}
	if (_handle!=-1)
	{
		_findclose(_handle);
		_handle = -1;
	}
}

const char *FindBank::GetName() const
{
	if (!_info) return "";
	return ((_finddata_t *)_info)->name;
}

#else

FindBank::FindBank()
{
	dir = NULL;
  entry = NULL;
}

FindBank::~FindBank()
{
	Close();
}

bool FindBank::First(const char *path)
{
    Close();
    LocalPath(dirName,path);
    dir = opendir(dirName);
    return Next();
}

bool FindBank::Next()
{
    if ( !dir ) return false;
    while ( (entry = readdir(dir)) ) {
        int len = strlen(entry->d_name);
        if ( len <= 4 ) continue;
        if ( !strcmp(entry->d_name+len-4,".pbo") )
            return true;
        }
    closedir(dir);
    dir = NULL;
    return false;
}

void FindBank::Close()
{
    if ( dir ) {
        closedir(dir);
        dir = NULL;
        entry = NULL;
        }
}

const char *FindBank::GetName() const
{
    if ( !dir || !entry ) return "";
	return entry->d_name;
}

#endif

/*!
	Note: callback may be called even after Load returns
	Context is stored meanwhile inside the bank.
*/

void BankList::Load
(
	const RString &path,
	const RString &bankPrefix,const RString &bName,
	bool emptyPrefix,
	OpenCallback beforeOpen, OpenCallback afterOpen, BankContextBase *context
)
{
	int oldSize = Size();
	int index = Add();
	QFBank &bank = Set(index);
	// path can differ from bankPrefix (for example if Mod is used)
	if (!bank.open(path+bName,beforeOpen,context))
	{
		Resize(oldSize);
		return;
	}
	Log("Open bank %s",(const char *)(bankPrefix+bName));

	if (bank.error())
	{
		Resize(oldSize);
		return;
	}

	RString prefix;
	if (emptyPrefix) prefix = bName;
	else prefix = bankPrefix+bName;

	RString prefixPath = prefix+RString("\\");
	bank.SetPrefix(prefixPath);

	if (afterOpen && !afterOpen(&bank,context))
	{
		Resize(oldSize);
		return;
	}
}

void BankList::Unload
(
	const RString & bankPrefix,const RString &bName,
	bool emptyPrefix
)
{
	// find corresponding bank
	RString prefix;
	if (emptyPrefix) prefix = bName;
	else prefix = bankPrefix+bName;

	RString prefixPath = prefix+RString("\\");

	for (int i=0; i<Size(); i++)
	{
		const QFBank &b = Get(i);
		if(b.GetPrefix()==prefixPath)
		{
			Delete(i);
			return;
		}
	}
}

void BankList::Lock(const RString &prefix)
{
	for (int i=0; i<Size(); i++)
	{
		const QFBank &b = Get(i);
		if(b.GetPrefix()==prefix)
		{
			b.Lock();
			return;
		}
	}
	LogF("Lock: Bank %s not found",(const char *)prefix);
}
void BankList::Unlock(const RString &prefix)
{
	for (int i=0; i<Size(); i++)
	{
		const QFBank &b = Get(i);
		if(b.GetPrefix()==prefix)
		{
			b.Unlock();
			return;
		}
	}
	LogF("Unlock: Bank %s not found",(const char *)prefix);
}

void BankList::SetLockable(const RString &prefix, bool lockable)
{
	for (int i=0; i<Size(); i++)
	{
		QFBank &b = Set(i);
		if(b.GetPrefix()==prefix)
		{
			b.SetLockable(lockable);
			return;
		}
	}
	LogF("MakeLockable: Bank %s not found",(const char *)prefix);
}

/*!
\param before unload all banks that have been last used before this time
*/

bool BankList::UnloadUnused()
{
	// if it is not locked, is is probably already unloaded
	// but we still may want to try it first
	for (int i=0; i<GFileBanks.Size(); i++)
	{
		QFBank &bank = GFileBanks[i];
		if (bank.IsLocked()) continue;
		if (!bank.CanBeUnloaded()) continue;
		LogF("Unloading bank %s",(const char *)bank.GetPrefix());
		bank.Unload();
		return true;
	}
	// if no unlocked bank is available for unloading, try locked banks
	for (int i=0; i<GFileBanks.Size(); i++)
	{
		QFBank &bank = GFileBanks[i];
		//if (bank.IsLocked()) continue;
		if (!bank.CanBeUnloaded()) continue;
		LogF("Unloading locked bank %s",(const char *)bank.GetPrefix());
		bank.Unload();
		return true;
	}
	return false;
}

void QIFStreamB::ClearBanks()
{
	GFileBanks.Clear();
}


QFBank *QIFStreamB::AutoBank( const char *name )
{
	if( !*name ) return NULL;
	if( name[1]==':' ) return NULL;
	for (int i=0; i<GFileBanks.Size(); i++)
	{
		QFBank &bank = GFileBanks[i];
		if (!CmpStartStr(name, bank.GetPrefix()))
			return &bank;
	}
	return NULL;
}

QIFStreamB::QIFStreamB()
:_bank(NULL)
{
}

void QIFStreamB::AutoOpen( const char *name, IQFBankContext *context )
{
	const char *name0=name;
	if( GUseFileBanks )
	{
		QFBank *bank=AutoBank(name);
		if( bank )
		{
			if (context && !context->IsAccessible(bank))
			{
				RptF("AutoOpen %s: access denied",name0);
				return;
			}
			// check if we should use bank version of the file
			// skip bank name
			name += bank->GetPrefix().GetLength();
			open(*bank,name);
			if( _sharedData )
			{
				_bank = bank;
				return;
			}
			// if file does not exist in bank, try to open it from file
			LogF("File %s not in bank",name0);
		}
	}
	QIFStream::open(name0);
}

bool QIFStreamB::IsFromBank(const QFBank *bank) const
{
	// check request to flush all banks
	if (!bank) return true;
	return bank==_bank;
}


bool QIFStreamB::FileExist( const char *name, IQFBankContext *context)
{
	if( GUseFileBanks )
	{
		QFBank *bank=AutoBank(name);
		if( bank )
		{
			if (context && !context->IsAccessible(bank))
			{
				const char *rName = name + bank->GetPrefix().GetLength();
				if( bank->FileExists(rName) ) return false;
				LogF("FileExist %s: access denied",name);
				return false;
			}
			const char *rName = name + bank->GetPrefix().GetLength();
			if( bank->FileExists(rName) ) return true;
		}
	}
	return QIFStream::FileExists(name);
}

struct EncryptorInformation
{
	RString name;
	IFilebankEncryption *(*createFunction)(const void *context);
};

TypeIsMovableZeroed(EncryptorInformation)

template<>
struct FindArrayKeyTraits<EncryptorInformation>
{
	typedef const char *KeyType;
	static bool IsEqual(const char * a, const char *b)
	{
		return !strcmpi(a,b);
	}
	static const char *GetKey(const EncryptorInformation &a) {return a.name;}
};

static FindArrayKey<EncryptorInformation> GEncryptors;

void RegisterFilebankEncryption
(
	const char *name, IFilebankEncryption *(*createFunction)(const void *context)
)
{
	// check if given encyption already exists
	int index = GEncryptors.FindKey(name);
	if (index>=0)
	{
		ErrF("Ecryption %s already registered",name);
		return;
	}
	EncryptorInformation &ei = GEncryptors.Append();
	ei.name = name;
	ei.createFunction = createFunction;
}

Ref<IFilebankEncryption> CreateFilebankEncryption(const char *name, const void *context)
{
	int index = GEncryptors.FindKey(name);
	if (index<0)
	{
		ErrF("Unknown encryption %s",name);
		return NULL;
	}
	return GEncryptors[index].createFunction(context);
}
