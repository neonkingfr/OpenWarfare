#ifdef _MSC_VER
#pragma once
#endif

#ifndef _SERIALIZE_BIN_HPP
#define _SERIALIZE_BIN_HPP

#include <Es/Strings/rString.hpp>
#include "QStream.hpp"

#ifdef PREPROCESS_DOCUMENTATION
#include "networkObject.hpp"
#endif

//! stream used to fast "binary" serialization

class SerializeBinStream
{
	public:
	enum ErrorCode
	{
		EOK,
		EBadFileType,
		EBadVersion,
		EFileStructure,
		EGeneric
	};

	private:
	QIStream *_in;
	QOStream *_out;
	ErrorCode _error;
	void *_context;

	public:
	SerializeBinStream( QIStream *in );
	SerializeBinStream( QOStream *out );
	bool IsSaving() const {return _out!=NULL;}
	bool IsLoading() const {return _in!=NULL;}

	ErrorCode GetError() const {return _error;}
	void SetError( ErrorCode code ) {_error=code;}

	void *GetContext() const {return _context;}
	void SetContext( void *context ) {_context=context;}

	//! get current reading position
	int TellG();
	//! set current reading position
	void SeekG(int offset);

	// load/save helpers
	bool Version(int ver);
	void Load(void *data, int size) {_in->read(data,size);}
	int LoadInt() {int t=0;_in->read(&t,sizeof(t));return t;}
	short LoadShort() {short t=0;_in->read(&t,sizeof(t));return t;}
	char LoadChar() {return _in->get();}

	int GetRest() {return _in ? _in->rest() : 1;}

	void Save(const void *data, int size) {_out->write(data,size);}
	void SaveInt( int t ) {_out->write(&t,sizeof(t));}
	void SaveShort( short t ) {_out->write(&t,sizeof(t));}
	void SaveChar( char t ) {_out->put(t);}

	// generic helpers
	void TransferBinary(void *data, int size)
	{
		if (_in) _in->read(data,size);
		else _out->write(data,size);
	}
	void TransferBinaryCompressed(void *data, int size);
	void SaveCompressed(const void *data, int size);
	void LoadCompressed(void *data, int size);
	#define BIN_TRANSFER(x) TransferBinary(&(x),sizeof(x))
	#define BIN_TRANSFER_COMPRESS(x) TransferBinaryCompressed(&(x),sizeof(x))
	// transfer basic types
	void operator << ( int &data ) {TransferBinary(&data,sizeof(data));}
	void operator << ( float &data ) {TransferBinary(&data,sizeof(data));}
	void operator << ( bool &data ) {TransferBinary(&data,sizeof(data));}
	void operator << ( char &data ) {TransferBinary(&data,sizeof(data));}
	void operator << ( signed char &data ) {TransferBinary(&data,sizeof(data));}
	void operator << ( unsigned char &data ) {TransferBinary(&data,sizeof(data));}

	void operator << ( RString &data );
	void operator << ( RStringB &data );
	//void operator << ( RStringIB &data );

	/*
	void Transfer( Vector3 &data );
	void Transfer( Matrix4 &data );
	void Transfer( Matrix3 &data );
	*/
	
	template <class Type>
	void Transfer(Type &val)
	{
		(*this)<< val;
	}

	template <class ArrayType>
	void TransferBinaryArray( ArrayType &data )
	{
		if (_in)
		{
			int size = LoadInt();
			data.Realloc(size);
			data.Resize(size);
		}
		else
		{
			SaveInt(data.Size());
		}
		TransferBinaryCompressed(data.Data(),data.Size()*sizeof(*data.Data()));
	}

	template <class ArrayType>
	void TransferBasicArray( ArrayType &data )
	{
		if (_in)
		{
			int size = LoadInt();
			data.Realloc(size);
			data.Resize(size);
		}
		else
		{
			SaveInt(data.Size());
		}
		// transfer array data
		for (int i=0; i<data.Size(); i++)
		{
			Transfer(data[i]);
		}
	}
	template <class ArrayType>
	void TransferArray( ArrayType &data )
	{
		if (_in)
		{
			int size = LoadInt();
			data.Realloc(size);
			data.Resize(size);
		}
		else
		{
			SaveInt(data.Size());
		}
		// transfer array data
		for (int i=0; i<data.Size(); i++)
		{
			data[i].SerializeBin(*this);
		}
	}

	template <class Type,class ArrayType>
	void TransferRefArrayT( ArrayType &data, Type *type )
	{
		if (_in)
		{
			int size = LoadInt();
			data.Realloc(size);
			data.Resize(size);
			for (int i=0; i<data.Size(); i++)
			{
				data[i] = new Type;
			}
		}
		else
		{
			SaveInt(data.Size());
		}
		// transfer array data
		for (int i=0; i<data.Size(); i++)
		{
			data[i]->SerializeBin(*this);
		}
	}
	template <class Type>
	void TransferRefArray( RefArray<Type> &data )
	{
		TransferRefArrayT(data,(Type *)NULL);
	}

};


#endif
