// Quick file stream implementation

#include <El/elementpch.hpp>
//#include "winpch.hpp"

#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>

#ifdef _WIN32
	#include <io.h>
	#include <stdio.h>
	#ifndef ACCESS_ONLY 
//		#include "win.h"
    #include <El/Common/perfLog.hpp>
	#endif
	#ifdef _XBOX
	#define USE_MAPPING 0
	#else
	#define USE_MAPPING 1
	#endif
#else
	#include <stdio.h> 
	#include <unistd.h> 
	#define POSIX_FILES 1
	#ifdef NO_MMAP
	  #define USE_MAPPING 0
	#else
	  #define USE_MAPPING 1
	#endif
#endif

#include "QStream.hpp"

#ifdef POSIX_FILES
	#define NO_FILE(file) ( file<0 )
	#define NO_FILE_SET -1
#else
	#define NO_FILE(file) ( file==NULL )
	#define NO_FILE_SET NULL
#endif

void QIStream::Close()
{
	_buf=NULL;
	_len=0;
	_readFrom=0;
	_fail=true,_eof=false;
}

void QIStream::Assign( const QIStream &src )
{
	// this points to same data as from
	_buf=src._buf;
	_len=src._len;
	_readFrom=src._readFrom;
	_fail=src._fail,_eof=src._eof;
}

bool QIStream::nextLine ()
{
    int c1;
    while ( !eof() )
        if ( (c1 = get()) == 0x0D || c1 == 0x0A ) {
            if ( !eof() && c1 == 0x0D ) {
                int c2 = get();
                if ( c2 != 0x0A ) unget();
                }
            return true;
            }
    return false;
}

bool QIStream::readLine ( char *buf, int bufLen )
{
    Assert( buf );
    int left = bufLen - 1;                  // regular chars to read
    int c1;
    while ( !eof() ) {
        if ( (c1 = get()) == 0x0D || c1 == 0x0A ) {
                // EOLN:
            if ( !eof() && c1 == 0x0D ) {
                int c2 = get();
                if ( c2 != 0x0A ) unget();
                }
            *buf = (char)0;
            return true;
            }
            // regular char:
        if ( bufLen > 0 && left == 0 ) {    // buffer overflow
            *buf = (char)0;
            return nextLine();
            }
        *buf++ = c1;
        left--;
        }
    *buf = (char)0;
    return false;                           // EOF reached before EOLN
}

void QIFStream::DoConstruct( const QIFStream &from )
{
	_sharedData=from._sharedData;
	// make this to contains same data as from
	QIStream::Assign(from);
}

#ifndef _WIN32

int FileSize ( int handle )
{
	struct stat buf;
	fstat(handle,&buf);
	return buf.st_size;
}

#endif

#include "fileMapping.hpp"

#ifndef POSIX_FILES
  #include "fileCompress.hpp"
#endif

FileBufferLoaded::FileBufferLoaded( const char *name )
{
	#ifdef POSIX_FILES
	#ifndef _WIN32
	LocalPath(fn,name);
	int file=::open(fn,O_RDONLY);
	#else
	int file=::open(name,O_RDONLY|O_BINARY);
	#endif
	if( file>=0 )
	{
		int size=FileSize(file);
		if( size!=-1 )
		{
			_data.Init(size);
			//::ReadFile(file,_sharedData->Data(),size,&sizeRead,NULL);
			int sizeRead=::read(file,_data.Data(),size);
			if( sizeRead!=size )
			{
				_data.Delete();
				::WarningMessage("File '%s' read error",name);
			}
		}
		else
		{
			//::WarningMessage("File '%s' not found.",name);
		}
		::close(file);
	}
	#else
	HANDLE file=::CreateFile
	(
		name,GENERIC_READ,FILE_SHARE_READ,
    NULL, // security
    OPEN_EXISTING,
    FILE_ATTRIBUTE_NORMAL,
    NULL // template
  );
	if( file!=INVALID_HANDLE_VALUE )
	{
		DWORD size=::GetFileSize(file,NULL);
		if( size>0 && size!=0xffffffff )
		{
			_data.Init(size);
			DWORD sizeRead;
			::ReadFile(file,_data.Data(),size,&sizeRead,NULL);
			if( sizeRead!=size )
			{
				_data.Delete();
				::WarningMessage("File '%s' read error",name);
			}
		}
		::CloseHandle(file);
	}	
	#endif
}

FileBufferLoaded::~FileBufferLoaded()
{
	_data.Delete();
}

#if _XBOX
const char *FullXBoxName(const char *name, char *temp)
{
	if (!name[0] || name[1]!=':')
	{
		strcpy(temp,"d:\\");
		strcat(temp,name);
		return temp;
	}
	else
	{
		return name;
	}
}
#endif

void QIFStream::open( const char *name )
{
	//ScopeLock lock(_serialize);
	_fail=true;
	_error=LSUnknownError;
	// open and preload file
	#if !USE_MAPPING
		#if _XBOX
			char temp[1024];
			name = FullXBoxName(name,temp);
			_sharedData = new FileBufferLoaded(name);
		#else
			_sharedData = new FileBufferLoaded(name);
		#endif
	#else
	_sharedData = new FileBufferMapped(name);
	#endif
	if (!_sharedData->GetError())
	{
		QIStream::init(_sharedData->GetData(),_sharedData->GetSize());
	}
}

void QIFStream::OpenBuffer( Ref<IFileBuffer> buffer )
{
	_error=LSUnknownError;
	// attach stream to some memory data
	_sharedData=buffer;
	QIStream::init(_sharedData->GetData(),_sharedData->GetSize());
}

void QIFStream::DoDestruct()
{
	// destroy buffer
	_sharedData.Free();
	_len=_readFrom=0;
}

QIFStream::~QIFStream()
{
	DoDestruct();
} // close file and destroy buffer

int CmpStartStr( const char *str, const char *start )
{
	while( *start )
	{
		if( myLower(*str++)!=myLower(*start++) ) return 1;
	}
	return 0;
}

bool QIFStream::FileReadOnly( const char *name )
{
	// file exists and is read only
	#ifdef POSIX_FILES
	LocalPath(fn,name);
	struct stat st;
	if ( stat(fn,&st) ) return false;
	return( (st.st_mode & S_IWUSR) == 0 );
	#else
	DWORD attrib=::GetFileAttributes(name);
	// check for cases where write would fail
	if( attrib&FILE_ATTRIBUTE_READONLY ) return true;
	if( attrib&FILE_ATTRIBUTE_DIRECTORY ) return true;
	if( attrib&FILE_ATTRIBUTE_SYSTEM ) return true;
	return false; // no file
	#endif
}

bool QIFStream::FileExists( const char *name )
{
	// check normal file existence
	#ifdef POSIX_FILES
	LocalPath(fn,name);
	int file=::open(fn,O_RDONLY);
	if( NO_FILE(file) ) return false;
	::close(file);
	#else
	#if _XBOX
		char temp[1024];
		name = FullXBoxName(name,temp);
	#endif
	HANDLE check=::CreateFile
	(
		name,GENERIC_READ,FILE_SHARE_READ,
		NULL,OPEN_EXISTING,0,NULL
	);
	if( check==INVALID_HANDLE_VALUE ) return false;
	::CloseHandle(check);
	#endif
	return true;
}

#define WIN_DIR '\\'
#define UNIX_DIR '/'

#if __GNUC__
	#define INVAL_DIR WIN_DIR
	#define VAL_DIR UNIX_DIR
#else
	#define INVAL_DIR UNIX_DIR
	#define VAL_DIR WIN_DIR
#endif

// helper: open in file or in file bank
static RString ConvertFileName( const char *name, int inval, int valid )
{
	if( !strchr(name,inval) ) return name; // no conversion required
	// convert directory characters depending on platform
	char cname[512];
	strcpy(cname,name);
	for( char *cc=cname; *cc; cc++ )
	{
		if( *cc==inval ) *cc=valid;
	}
	return cname;
}

inline RString PlatformFileName( const char *name )
{
	return ConvertFileName(name,INVAL_DIR,VAL_DIR);
}

inline RString UniversalFileName( const char *name )
{ // filenames are normally stored with backslash '\\'
	return ConvertFileName(name,UNIX_DIR,WIN_DIR);
}

void QOFStream::open( const char *file )
{
	_file=PlatformFileName(file);
	_fail=false;
	_error=LSUnknownError;
	rewind();
}

#ifndef POSIX_FILES
static LSError LSErrorCode( DWORD eCode )
{
	switch( eCode )
	{
		case ERROR_HANDLE_DISK_FULL: return LSDiskFull;
		case ERROR_NETWORK_ACCESS_DENIED:
		case ERROR_LOCK_VIOLATION:
		case ERROR_SHARING_VIOLATION:
		case ERROR_WRITE_PROTECT:
		case ERROR_ACCESS_DENIED: return LSAccessDenied;
		case ERROR_FILE_NOT_FOUND: return LSFileNotFound;
		case ERROR_READ_FAULT:
		case ERROR_WRITE_FAULT:
		case ERROR_CRC: return LSDiskError;
		default:
		{
			#ifndef _XBOX
			char buffer[256];
			FormatMessage
			(
				FORMAT_MESSAGE_FROM_SYSTEM,
				NULL, // source
				eCode, // requested message identifier 
				0, // languague
				buffer,sizeof(buffer),
				NULL
			);
			Log("Unknown error %d %s",eCode,buffer);
			#else
			Log("Unknown error %d",eCode);
			#endif
			
		}
		return LSUnknownError;
	}
}

#endif

void QIFStream::import( const char *name )
{
	// import from file or clipboard
	// easy way: file import
	open(name);
	// TODO: hard way: windows clipboard
}

static ClipboardFunctions GClipboardFunctions;
ClipboardFunctions *QOFStream::_defaultClipboardFunctions = &GClipboardFunctions;

void QOFStream::export_clip( const char *name )
{
	// export to file or clipboard
	#ifndef _WIN32
	LocalPath(fn,name);
	int handle = ::open(fn,O_CREAT|O_APPEND|O_WRONLY,S_IREAD|S_IWRITE);
	#else
	int handle = ::open(name,O_CREAT|O_APPEND|O_BINARY|O_WRONLY,S_IREAD|S_IWRITE);
	#endif
	if (handle>=0)
	{
		::write(handle,_buffer.Data(),_buffer.Size());
		::close(handle);
	}

	_defaultClipboardFunctions->Copy(_buffer.Data(), _buffer.Size());
}

/*!
\patch 1.85 Date 9/9/2002 by Jirka
- Fixed: Writing into big file sometimes failed with error #1450:
  "Insufficient system resources exist to complete the requested service."
*/
void QOFStream::close( const void *header, int headerSize )
{
	if( !_file ) return; // no file
	// open and preload file
	_error=LSOK;
	#ifdef POSIX_FILES
	#ifndef _WIN32
	LocalPath(fn,(const char*)_file);
	int file=::open(fn,O_CREAT|O_WRONLY|O_TRUNC,S_IREAD|S_IWRITE);
	#else
	int file=::open(_file,O_CREAT|O_BINARY|O_WRONLY|O_TRUNC,S_IREAD|S_IWRITE);
	#endif
	if( file )
	{
		int sizeWritten=::write(file,header,headerSize);
		if( sizeWritten!=headerSize )
		{
			_fail=true;
			goto Error;
		}
		sizeWritten=::write(file,_buffer.Data(),_buffer.Size());
		if( sizeWritten!=_buffer.Size() )
		{
			_fail=true;
			goto Error;
		}
		Error:
		int eCode=0;
		int success=::close(file);
		if( success<0 ) _fail=true;
		if( _fail )
		{
			_error=LSUnknownError;
		}
	}
	#else
	DWORD eCode=0;
	HANDLE file=::CreateFile
	(
		_file,GENERIC_WRITE,0,
    NULL, // security
    CREATE_ALWAYS,
    FILE_ATTRIBUTE_NORMAL,
    NULL // template
    );
	if( file!=INVALID_HANDLE_VALUE )
	{
		DWORD sizeWritten;
		::WriteFile(file,header,headerSize,&sizeWritten,NULL);
		if( (int)sizeWritten!=headerSize )
		{
			_fail=true;
			goto Error;
		}
		// FIX: write only 1MB at once
		{
			const char *data = _buffer.Data();
			int size = _buffer.Size();
			// write 1 MB at once
			static const int writeAtOnce = 1024 * 1024;
			while (size > writeAtOnce)
			{
				::WriteFile(file,data,writeAtOnce,&sizeWritten,NULL);
				if ((int)sizeWritten != writeAtOnce)
				{
					_fail=true;
					goto Error;
				}
				data += writeAtOnce;
				size -= writeAtOnce;
			}
			::WriteFile(file,data,size,&sizeWritten,NULL);
			if ((int)sizeWritten != size)
			{
				_fail=true;
				goto Error;
			}
		}
		/*
		::WriteFile(file,_buffer.Data(),_buffer.Size(),&sizeWritten,NULL);
		if( (int)sizeWritten!=_buffer.Size() )
		{
			_fail=true;
			goto Error;
		}
		*/
		Error:
		DWORD eCode=0;
		if( _fail ) eCode=::GetLastError();
		BOOL success=::CloseHandle(file);
		if( !success )
		{
			if( eCode==0 ) eCode=::GetLastError();
			_fail=true;
		}
	}
	else
	{
		eCode=::GetLastError();
		_fail=true;
	}
	if( eCode )
	{
		_error=LSErrorCode(eCode);
	}
	#endif
	// forget any data
	rewind();
	_file=NULL;
}

void QOFStream::close()
{
	close(NULL,0);
}

QOFStream::~QOFStream()
{
	//close();
}
