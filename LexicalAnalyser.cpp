#include "LexicalAnalyser.hpp"

char* GTokenString[] =
{
  TOKEN_STRINGS(TOKEN_STRING)
};

GTokenEnum FindTokenEnum(std::string unknown)
{
  for (int i=0; i<T_END; ++i)
  {  
    // find the corresponding enum
    if (_strcmpi(unknown.data(),GTokenString[i])==0) return (GTokenEnum)i;
  }

  return T_UNKNOWN;
}

bool LexicalAnalyser::DetermineReserveToken(std::string unknown)
{
  // check first the single characters, normally this is more complex.
  if (unknown.length()==1)
  {
    GTokenEnum findEnum = FindTokenEnum(unknown);
    if (findEnum == T_UNKNOWN) return false;

    // Get the next next token, so we can look ahead.
    TokenTypes ttype = NextStreamToken();

    bool doubleSymbolFound = false;
    if (ttype == '=')
    {
      switch (findEnum)
      {
        case T_TIMES: 
          _currentToken._type = T_TIMES_ASSIGN;
          doubleSymbolFound = true;
          break;
        case T_PLUS:  
          _currentToken._type = T_PLUS_ASSIGN;
          doubleSymbolFound = true;
          break;
        case T_MINUS: 
          _currentToken._type = T_MINUS_ASSIGN;
          doubleSymbolFound = true;
          break;
        case T_DIVIDE:
          _currentToken._type = T_DIVIDE_ASSIGN;
          doubleSymbolFound = true;
          break;
        case T_GREATERTHAN: 
          _currentToken._type = T_GREATERTHAN_EQUALTO;
          doubleSymbolFound = true;
          break;
        case T_LESSTHAN: 
          _currentToken._type = T_LESSTHAN_EQUALTO;
          doubleSymbolFound = true;
          break;
        case T_ASSIGN: 
          _currentToken._type = T_EQUALITY;
          doubleSymbolFound = true;
          break;
        case T_NOT: 
          _currentToken._type = T_NOT_ASSIGN;
          doubleSymbolFound = true;
          break;
      }
    }

    if (doubleSymbolFound) return true;
    
    // Jump back a token.
    PreviousStreamToken();
  }

  GTokenEnum findEnum = FindTokenEnum(unknown);
  if (findEnum != T_UNKNOWN)
  {
    _currentToken._type = findEnum;
    _currentToken._identifier = std::string(GTokenString[findEnum]);
    
    return true;
  }

  return false;
}

void LexicalAnalyser::ProccessToken(const TokenTypes foundType,Token& myNewToken)
{
  switch (foundType)
  {
  case TT_FLOAT_CONST:
    {   
      myNewToken._type = T_FLOATCONST;
      myNewToken._identifier = _stringVal;

      float f = (float)atof(_stringVal.data());
      myNewToken._floatConst = f;
    }
    break;
  case TT_INT_CONST:
    {   
      // convert float to a number
      myNewToken._type = T_INTCONST;
      myNewToken._identifier = _stringVal;

      int i = atoi(_stringVal.data());
      myNewToken._intConst = (int) i;
    }
    break;
  case TT_STRING_LITERAL:
    {
      // we have a string literal on our hands.
      myNewToken._type = T_STRINGLITERAL;
      myNewToken._stringLiteralVal = _stringVal;
    }
    break;
  case TT_WORD:
    // Need to determine what token we're dealing with.
    if (!DetermineReserveToken(_stringVal))
    {
      // Not part of reserve token names.
      myNewToken._type = T_IDENTIFIER;
      myNewToken._identifier = _stringVal;
    }
    else
    {
      // Used for determining if true/false.
      if (_currentToken._type == T_BOOLTRUE || _currentToken._type == T_BOOLFALSE)
      {
        myNewToken._boolConst = _currentToken._type == T_BOOLTRUE ?  true : false;
        myNewToken._type = T_BOOLEANCONST;
      }
    }
    break;
  case TT_EOF:
    myNewToken._type = T_EOF;
    break;
  case TT_SOF:
    myNewToken._type = T_SOF;
    break;
  default:
    {
      // Remember that, our type is actually the ascii representation
      char unknownChar = (char) foundType;
      std::string unknowntoken;
      unknowntoken.append(1,unknownChar);

      // Determine if we have a Reserved token
      if (!DetermineReserveToken(unknowntoken))
      {
        myNewToken._type = T_UNKNOWN;
      }
    }
    break;
  }
}


const Token& LexicalAnalyser::PreviousToken()
{
  TokenTypes tokenType = PreviousStreamToken();
  // Set the new token as empty.
  _currentToken = Token();
  ProccessToken(tokenType,_currentToken);

  return _currentToken;
}

const Token& LexicalAnalyser::NextToken()
{
  // Set the new token as empty.
  _currentToken = Token();
  
  TokenTypes tokenType = NextStreamToken();
  ProccessToken(tokenType,_currentToken);

  return _currentToken;
}

