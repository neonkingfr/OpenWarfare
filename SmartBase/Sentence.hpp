
#ifndef _SENTENCE_HPP
#define _SENTENCE_HPP

#include "KnowledgeBase.hpp"

//! Sentence mood code.
#define UNKNOWN 0
#define INTERROGATIVE 1 // tazaci
#define IMPERATIVE 2 // rozkazovaci
#define INDICATIVE 3 // oznamovaci

//! Low level class. Contains semantic information, hierarchy and mood of the sentence part.
class SentenceAtom : public RefCount
{
protected:
  //! Hiearchy.
  RefArray< SentenceAtom > _subAtoms;
  
  //! Semantic knowledge.
  Ref< SemanticKnowledge > _knowledgeData;
  
  //! Sentence mood.
  short _mood;
  
public:
  inline SentenceAtom( SemanticKnowledge *knowledgeData )
    : _knowledgeData( knowledgeData ), _mood( UNKNOWN )
  {
    AssertDebug( _knowledgeData.NotNull() );
    _knowledgeData->GetWordBaseId();
  }
  
  inline void SetMood( short mood )
  {
    _mood = mood;
  }
  
  inline short GetMood() const
  {
    return _mood;
  }

  inline void ClearSubAtoms()
  {
    _subAtoms.Clear();
  }
  
  //! Delete one sentence atom in the hierarchy.
  inline void DeleteAtom( int i )
  {
    _subAtoms.DeleteAt( i );
  }

  inline void CopyAtoms( const RefArray< SentenceAtom > &atoms )
  {
    _subAtoms = atoms;
  }
  
  inline void AddSubAtom( SentenceAtom *subAtom )
  {
    _subAtoms.Add( subAtom );
  }

  inline void AddSubAtom( SemanticKnowledge *knowledge )
  {
    Ref< SentenceAtom > subAtom = new SentenceAtom( knowledge );
    AssertDebug( subAtom.NotNull() );
    AddSubAtom( subAtom );
  }
  
  inline int GetSubAtomsSize() const
  {
    return _subAtoms.Size();
  }

  inline const SentenceAtom *GetSubAtom( int i ) const
  {
    return _subAtoms[ i ];
  }
  
  inline void GetText( const WordBase *wb, RString &desc ) const
  {
    _knowledgeData->GetText( wb, desc );
  }
  
  inline WordBaseId GetWordBaseId() const
  {
    return _knowledgeData->GetWordBaseId();
  }
  
  inline const SemanticKnowledge *GetKnowledgeData() const
  {
    return _knowledgeData;
  }

  inline void SetKnowledgeData( SemanticKnowledge *knowledgeData )
  {
    _knowledgeData = knowledgeData;
  }
  
  inline bool HasSpaceInFront( const WordBase *wb ) const
  {
    return wb->HasSpaceInFront( _knowledgeData->GetWordBaseId() );
  }
  
  inline bool HasNeverSpaceBehind( const WordBase *wb ) const
  {
    return wb->HasNeverSpaceBehind( _knowledgeData->GetWordBaseId() );
  }
  
  void GetSentenceText( const WordBase *wb, RString &text ) const
  {
    int n = _subAtoms.Size();
    if( n == 0 )
    {
      GetText( wb, text );
      AddPunctuation( text );
      return;
    }
    else
    {
      RString t0, t1;
      RString space( " " );
      SentenceAtom *prevAtom = _subAtoms[ 0 ];
      prevAtom->GetSentenceText( wb, t0 );
      for( int i = 1; i < n; )
      {
        SentenceAtom *subAtom = _subAtoms[ i++ ];
        subAtom->GetSentenceText( wb, t1 );
        if( !prevAtom->HasNeverSpaceBehind( wb ) && subAtom->HasSpaceInFront( wb ) )
        {
          t0 = RString( t0, space );
        }
        prevAtom = subAtom;
        t0 = RString( t0, t1 );
      }
      text = t0;
      AddPunctuation( text );
      return;
    }
  }

  inline static void CreateBasicSentenceAtom( const WordBase &wb, 
    Ref< SentenceAtom > &sentence, short mood )
  {
    sentence = new SentenceAtom( 
      new SemanticKnowledge( wb._wordIdCustomId, "Sentence", FORM_DEFAULT, wb ) );
    sentence->SetMood( mood );
  }

  inline void AddPunctuation( RString &text ) const
  {
    AddPunctuation( _mood, text );
  }
  
  static inline void AddPunctuation( short mood, RString &text )
  {
    switch( mood )
    {
    case INTERROGATIVE:
      Capitalize( text );
      text = RString( text, "?" );
      break;

    case IMPERATIVE:
      Capitalize( text );
      text = RString( text, "!" );
      break;

    case INDICATIVE:
      Capitalize( text );
      text = RString( text, "." );
      break;

    // case UNKNOWN:
    }
  }

  inline static void Capitalize( RString &text )
  {
    char *c;
    if( text.GetLength() != 0 && ( c = text.MutableData() ) != NULL &&
      ( *c >= 'a' ) && ( *c <= 'z' ) )
    {
      *c += 'A' - 'a';
    }
  }
};

#endif