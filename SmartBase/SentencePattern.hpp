
#pragma once

#include "KnowledgeBase.hpp"
#include "SentenceUi.hpp"

class SentenceWildcard : public RefCount
{
private:
  Ref< const SentenceAtom > _atom;
  RefArray< SentenceWildcard > _wildcards;

public:
  virtual bool CompareAtom( const SentenceAtom *atom, bool &contDeeper, const WordBase &wordBase ) = 0;
  
  inline const SentenceAtom *GetSentenceAtom() const
  {
    return _atom;
  }
  
  inline void AddWildcard( SentenceWildcard *wildcard )
  {
    _wildcards.Add( wildcard );
  }

  bool CompareHierarchy( const SentenceAtom *atom, const WordBase &wordBase )
  {
    bool contDeeper;
    if( !( CompareAtom( atom, contDeeper, wordBase ) ) )
    {
      return false;
    }
    
    if( contDeeper )
    {
      int nw = _wildcards.Size();
      int na = atom->GetSubAtomsSize();
      if( nw == na )
      {
        int i = 0;
        while( i < nw )
        {
          if( _wildcards[ i ]->CompareHierarchy( atom->GetSubAtom( i ), wordBase ) )
          {
            ++i;
            continue;
          }
          return false;
        }
      }
      else
      {
        return false;
      }
    }
    return true;
  }
  
  inline SentenceWildcard *GetWildcard( int i )
  {
    return _wildcards[ i ];
  }

  inline int GetWildcardsSize() const
  {
    return _wildcards.Size();
  }

protected:
  inline void SetSentenceAtom( const SentenceAtom *atom )
  {
    _atom = atom;
  }
};

class SentenceWildcardIn : public SentenceWildcard
{
public:
  virtual bool CompareAtom( const SentenceAtom *atom, bool &contDeeper, const WordBase &wordBase ) = 0;
};

class SentenceWildcardOut : public SentenceWildcard
{
public:
  virtual bool CompareAtom( const SentenceAtom *atom, bool &contDeeper, const WordBase &wordBase ) = 0;
};

class SentenceWildcardInWordBaseId : public SentenceWildcardIn
{
protected:
  Ref< SemanticKnowledge > _atom;

public:
  inline SentenceWildcardInWordBaseId( WordBaseId wordBaseId )
    : _atom( new SemanticKnowledge( wordBaseId ) )
  {
  }

  inline SentenceWildcardInWordBaseId( WordBaseId wordBaseId, const RString &text )
    : _atom( new SemanticKnowledge( wordBaseId, text ) )
  {
  }

  virtual bool CompareAtom( const SentenceAtom *atom, bool &contDeeper, const WordBase &wordBase )
  {
    SetSentenceAtom( atom );
    contDeeper = true;
    return _atom->IsEqual( *atom->GetKnowledgeData(), wordBase );
  }
};

class SentenceWildcardOutWordBaseId : public SentenceWildcardOut
{
public:
  inline SentenceWildcardOutWordBaseId()
  {
  }

  virtual bool CompareAtom( const SentenceAtom *atom, bool &contDeeper, const WordBase &wordBase )
  {
    SetSentenceAtom( atom );
    contDeeper = false;
    return true;
  }
};
