#ifdef _MSC_VER
#pragma once
#endif

#ifndef _SEMANTIC_NET_OFP_HPP
#define _SEMANTIC_NET_OFP_HPP

#include "SemanticNet.hpp"

class SemanticNetOFP : public SemanticNet
{
public:
  virtual void React
  (
    KnowledgeBase *participant, const SentenceFragment *sentence, 
    AutoArray< SentenceResultCondition > &complementaryConditions, 
    AutoArray< SentenceResultCondition > &complementarySubjects, Ref< SentenceAtom > &reaction,
    const RefArray< KnowledgeBase > &listeners
  ) {}
  virtual void FilterDefinedPronouns
  (
    const WordBase &wb, AutoArray< WordBaseId > &outcomingPronouns
    ) const {}
  virtual void InitConversationContext(const AutoArray< SemanticNetId > &youPronoun) {}
  virtual void InitSpeechContext() {}
  virtual int GetId() const {return 0;}
  virtual void GetDefinedPlaces( const WordBase &wb, AutoArray< WordBaseId > &places ) const {}
  virtual void GetUnitTypes( const WordBase &wb, AutoArray< WordBaseId > &unitTypes ) const {}
  virtual void GetSpecRules( const KnowledgeBase *player, RefArray< SentenceRule > &specRules ) const {}
  virtual bool IsNotionDefined( WordBaseId notionId ) const { return false; }
  virtual int GetNotionsSize() const {return 0;}
  virtual const SemanticKnowledge *GetNotion( int i ) const {return NULL;}
};

#endif