#ifdef _MSC_VER
#pragma once
#endif

#ifndef _CONVERSATION_CENTER_OFP_HPP
#define _CONVERSATION_CENTER_OFP_HPP

#include "ConversationCenter.hpp"
#include "SentenceRuleDefault.hpp"

#include "KnowledgeBaseDefault.hpp"

class ConversationCenterOFP : public ConversationCenter
{
protected:
  const ParamEntry &_rules;
  const RString _rulesRoot;
  Ref< WordBase > _wordBase;
  // Ref< SentenceReaction > _sentenceReaction;
  Ref< KnowledgeBase > _player;

public:
  ConversationCenterOFP(const ParamEntryVal &words, const ParamEntry &rules, const RString &rulesRoot, KnowledgeBase *player)
    : _rules(rules), _rulesRoot(rulesRoot), _player(player)
  {
    _wordBase = new WordBaseDefault(words);
    Assert(_wordBase.NotNull());
  }

  virtual void GetPlayerName( RString &name )
  {
    // name = Glob.header.playerName;
    Fail("Not implemented");
  }

  virtual KnowledgeBase *GetPlayer()
  {
    return _player;
  }

  virtual void GetParticipantName( int i, RString &name )
  {
    Fail("Not implemented");
  }

  virtual void GetParticipant( int i, Ref< KnowledgeBase > &kb )
  {
    Fail("Not implemented");
  }

  virtual int GetParticipantsSize()
  {
    Fail("Not implemented");
    return 0;
  }

  virtual void GetSentenceRuleRoot( const KnowledgeBase *player, 
    const RefArray< KnowledgeBase > &participants, Ref< SentenceFragment > &root )
  {
    root = new SentenceFragmentDefault(player, _rules, _rulesRoot);
    Assert(root.NotNull());
  }

  virtual WordBase *GetWordBase()
  {
    return _wordBase;
  }
};

#endif