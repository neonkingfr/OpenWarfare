#if _ENABLE_CONVERSATION

#include "El/elementpch.hpp"
#include "El/Modules/modules.hpp"
#include "El/ParamArchive/paramArchive.hpp"

#include "PlaceArea.hpp"

DEFINE_FAST_ALLOCATOR( GameDataArea )

RString GameDataArea::GetText() const
{
  return "Area";
}

bool GameDataArea::IsEqualTo( const GameData *data ) const
{
  const PlaceArea &val1 = GetArea();
  const PlaceArea &val2 = static_cast<const GameDataArea *>( data )->GetArea();
  return val1 == val2;
}

LSError GameDataArea::Serialize( ParamArchive &ar )
{
  CHECK( base::Serialize( ar ) );
  return LSOK;
}

GameData *CreateGameDataArea()
{
  return new GameDataArea();
}

INIT_MODULE( PlaceAreaMain, 3 )
{
  GGameState.NewType( "AREA", GameArea, CreateGameDataArea, "Area" );
};

#endif