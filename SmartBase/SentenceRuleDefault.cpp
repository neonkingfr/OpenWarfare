#if _ENABLE_CONVERSATION

#include "El/elementpch.hpp"
#include "El/Modules/modules.hpp"
#include "El/Evaluator/express.hpp"

#include "SentenceRuleDefault.hpp"
#include "SemanticNet.hpp"

void SentenceFragmentDefault::ReadRules( RefArray< SentenceRule > &rules ) const
{
  int n = _generators.Size();
  for( int i = 0; i < n; ++i )
  {
    const GeneratorInfo &gi = _generators[ i ];
    if( !gi._isScalar )
    {
      WordBaseId wordId;
      if( ( wordId = _player->GetWordBase()->ConvertNameToId( gi._name ) ) != -1 )
      {
        Ref< SemanticKnowledge > know = new SemanticKnowledge( wordId, gi._form );
        Ref< SentenceAtom > atom = new SentenceAtom( know );
        Ref< SentenceFragmentDefault > frag = 
          new SentenceFragmentDefault( _player, atom );
        Ref< SentenceRule > rule = new SentenceRule();
        rule->AddSubfragment( frag );
        rules.Add( rule );
        // WordId has no conditions.
      }
      else
      {
        ParamEntryVal generVal = *_paramFile >> gi._name;
        ReadGeneratorRules( _player, *_paramFile, generVal, rules );
      }
    }
    else
    {
      Ref< SemanticKnowledge > know = new SemanticKnowledge( _player->GetWordBase()->_wordIdScalarId, 
        gi._scalar, gi._form );
      Ref< SentenceAtom > atom = new SentenceAtom( know );
      Ref< SentenceFragmentDefault > frag = 
        new SentenceFragmentDefault( _player, atom );
      Ref< SentenceRule > rule = new SentenceRule();
      rule->AddSubfragment( frag );
      rules.Add( rule );
      // Scalars has no conditions.
    }
  }
  
  if( _generateRules.GetRefCount() != 0 )
  {
    // Create space for variables.
    GameVarSpace vars;
    GGameState.BeginContext(&vars);

    // Register variable of the replaying participant.
    GGameState.VarSet( "_me", GameValue( new GameDataKnowledgeBase( _player ) ), true );
    
    // Generate rules.
    GameValue newRules = GGameState.Evaluate( _generateRules );
    AssertDebug( GGameState.GetLastError() == EvalOK );
    
    GameArrayType &arr = newRules;
    int n = arr.Size();
    for( int i = 0; i < n; ++i )
    {
      SentenceRule *rule;
    	if( ( rule = GetRule( arr[ i ] ) ) != NULL )
    	{
    	  rules.Add( rule );
    	}
    }
    
    // Delete knowledge base variable.
    GGameState.VarDelete( "_me" );
    GGameState.EndContext();
  }
}

bool SentenceFragmentDefault::RulesNotEmpty()
{
  if( base::RulesNotEmpty() )
  {
    return true;
  }

  if( !IsTerminator() && _generateRules.GetRefCount() != 0 )
  {
    // Create space for variables.
    GameVarSpace vars;
    GGameState.BeginContext(&vars);

    // Register variable of the replaying participant.
    GGameState.VarSet( "_me", GameValue( new GameDataKnowledgeBase( _player ) ), true );

    // Generate rules.
    GameValue newRules = GGameState.Evaluate( _generateRules );
    AssertDebug( GGameState.GetLastError() == EvalOK );

    // Delete knowledge base variable.
    GGameState.VarDelete( "_me" );

    GameArrayType &arr = newRules;
    int n = arr.Size();
    if( n != 0 )
    {
      bool result = false;
      int i = 0;
      do {
        SentenceRule *r = GetRule( arr[ i ] );
        if( r != NULL )
        {
          result = true;
          if( !r->RulesNotEmpty() )
          {
            GGameState.EndContext();
            return false;
          }
        }
        ++i;
      } while( i < n );
      GGameState.EndContext();
      return result;
    }
  }
  GGameState.EndContext();
  return false;
}

GameValue SentenceRule_GeneratePronouns( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
  const KnowledgeBase *participant;
  if( ( participant = GetKnowledgeBase( oper1 ) ) != NULL )
  {
    AutoArray< WordBaseId > outcomingPronouns;
    participant->GetWordBase()->GetDemonstrativePronouns( outcomingPronouns );
    participant->GetSemanticNet()->FilterDefinedPronouns( *participant->GetWordBase(), outcomingPronouns );
    
    Ref< GameDataArray > result = new GameDataArray();
    AssertDebug( result.NotNull() );
    
    long form = static_cast< float >( oper2 );
    int n = outcomingPronouns.Size();
    for( int i = 0; i < n; ++i )
    {
      Ref< SentenceRule > newRule = new SentenceRule();
      newRule->AddSubfragment( new SentenceFragmentDefault( participant, new SentenceAtom( 
        new SemanticKnowledge( outcomingPronouns[ i ], form ) ) ) );
    	result->GetArray().Add( GameValue( new GameDataRule( newRule ) ) );
    }
    
    return GameValue( result );
  }

  return GameValue( new GameDataArray() );
}

GameValue SentenceRule_GeneratePronounsWithoutI( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
  const KnowledgeBase *participant;
  if( ( participant = GetKnowledgeBase( oper1 ) ) != NULL )
  {
    AutoArray< WordBaseId > outcomingPronouns;
    participant->GetWordBase()->GetDemonstrativePronounsWithoutI( outcomingPronouns );
    participant->GetSemanticNet()->FilterDefinedPronouns( *participant->GetWordBase(), outcomingPronouns );

    Ref< GameDataArray > result = new GameDataArray();
    AssertDebug( result.NotNull() );

    long form = static_cast< float >( oper2 );
    int n = outcomingPronouns.Size();
    for( int i = 0; i < n; ++i )
    {
      Ref< SentenceRule > newRule = new SentenceRule();
      newRule->AddSubfragment( new SentenceFragmentDefault( participant, new SentenceAtom( 
        new SemanticKnowledge( outcomingPronouns[ i ], form ) ) ) );
      result->GetArray().Add( GameValue( new GameDataRule( newRule ) ) );
    }

    return GameValue( result );
  }

  return GameValue( new GameDataArray() );
}

GameValue SentenceRule_GenerateContextWithoutI( const GameState *state, GameValuePar oper )
{
  const KnowledgeBase *participant;
  if( ( participant = GetKnowledgeBase( oper ) ) != NULL )
  {
    Ref< const SemanticNet > net = participant->GetSemanticNet();
    Ref< GameDataArray > result = new GameDataArray();
    AssertDebug( result.NotNull() );

    int notionsN = net->GetNotionsSize();
    for( int notionsI = 0; notionsI < notionsN; ++notionsI )
    {
      Ref< const SemanticKnowledge > notion( net->GetNotion( notionsI ) );
      if( notion.NotNull() && notion->GetWordBaseId() != participant->GetWordBase()->_wordIdI )
      {
        Ref< SentenceRule > newRule = new SentenceRule();
        newRule->AddSubfragment( new SentenceFragmentDefault( participant, new SentenceAtom( 
          new SemanticKnowledge( *notion ) ) ) );
        result->GetArray().Add( GameValue( new GameDataRule( newRule ) ) );
      }
    }
    return GameValue( result );
  }

  return GameValue( new GameDataArray() );
}

GameValue SentenceRule_GeneratePlaces( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
  const KnowledgeBase *participant;
  if( ( participant = GetKnowledgeBase( oper1 ) ) != NULL )
  {
    AutoArray< WordBaseId > places;
    participant->GetSemanticNet()->GetDefinedPlaces( *participant->GetWordBase(), places );

    Ref< GameDataArray > result = new GameDataArray();
    AssertDebug( result.NotNull() );

    long form = static_cast< float >( oper2 );
    int n = places.Size();
    for( int i = 0; i < n; ++i )
    {
      Ref< SentenceRule > newRule = new SentenceRule();
      newRule->AddSubfragment( new SentenceFragmentDefault( participant, new SentenceAtom( 
        new SemanticKnowledge( places[ i ], form ) ) ) );
      result->GetArray().Add( GameValue( new GameDataRule( newRule ) ) );
    }

    return GameValue( result );
  }

  return GameValue( new GameDataArray() );
}

GameValue SentenceRule_GenerateUnitTypes( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
  const KnowledgeBase *participant;
  if( ( participant = GetKnowledgeBase( oper1 ) ) != NULL )
  {
    AutoArray< WordBaseId > unitTypes;
    participant->GetSemanticNet()->GetUnitTypes( *participant->GetWordBase(), unitTypes );

    Ref< GameDataArray > result = new GameDataArray();
    AssertDebug( result.NotNull() );

    long form = static_cast< float >( oper2 );
    int n = unitTypes.Size();
    for( int i = 0; i < n; ++i )
    {
      Ref< SentenceRule > newRule = new SentenceRule();
      newRule->AddSubfragment( new SentenceFragmentDefault( participant, new SentenceAtom( 
        new SemanticKnowledge( unitTypes[ i ], form ) ) ) );
      result->GetArray().Add( GameValue( new GameDataRule( newRule ) ) );
    }

    return GameValue( result );
  }

  return GameValue( new GameDataArray() );
}

INIT_MODULE( SentenceRuleDefaultMain, 3 )
{
  GGameState.NewOperator( GameOperator( GameArray,      "generatePronouns",           function, SentenceRule_GeneratePronouns,         GameKnowledgeBase, GameScalar ) );
  GGameState.NewOperator( GameOperator( GameArray,      "generatePronounsWithoutI",   function, SentenceRule_GeneratePronounsWithoutI, GameKnowledgeBase, GameScalar ) );
  GGameState.NewFunction( GameFunction( GameArray,      "generateContextWithoutI",              SentenceRule_GenerateContextWithoutI,  GameKnowledgeBase ) );
  GGameState.NewOperator( GameOperator( GameArray,      "generatePlaces",             function, SentenceRule_GeneratePlaces,           GameKnowledgeBase, GameScalar ) );
  GGameState.NewOperator( GameOperator( GameArray,      "generateUnitTypes",          function, SentenceRule_GenerateUnitTypes,        GameKnowledgeBase, GameScalar ) );
};

#endif
