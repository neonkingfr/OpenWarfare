
#ifndef _SENTENCERULE_HPP
#define _SENTENCERULE_HPP

#include "El/elementpch.hpp"
#include "El/Evaluator/express.hpp"
#include "Es/essencepch.hpp"
#include "Es/Strings/bstring.hpp"

#include "KnowledgeBase.hpp"
#include "Sentence.hpp"

//! No aggregation (the simplest one).
#define AGG_NONE                         0
//! Default aggregation (the most complex one).
#define AGG_DEFAULT                      1
//! Aggregation when we are asking for number.
#define AGG_QUANTITY                     2
//! The simplest format - just write out values one behind other.
#define FORMAT_SIMPLE 0
//! Format values into list separated by commas and 'and' before the last one.
#define FORMAT_LIST 1

#define VT_UNKNOWN                       0
#define VT_PLACE                         1
#define VT_SPEEDSIZE                     2
#define VT_SPEEDDIRECTION                3
#define VT_ENTITYTYPE                    4
#define VT_SIDE                          5
#define VT_TIME                          6
#define VT_STRING                        7 // e.g. name

class SentenceFragment;
class SentenceRule;

//! Possible sentence fragment types.
enum
{
  //! Sentence fragment is optional.
  NotRequired = 0,
  
  //! Required sentence fragment.
  Required,
  
  //! Not more branching fragment.
  Terminator,
};

//! This script condition must be fulfilled to be rule displayed.
class RuleVisibleTest
{
protected:
  //! Is this record original or copy (copied during only one rule unwrapping)?
  bool _original;
  
  //! Text of the particular script condition.
  RString _visibleTest;
  
public:
  inline RuleVisibleTest( RString &visibleTest )
    : _original( true ), _visibleTest( visibleTest )
  {
  }
  
  inline bool GetOriginal() const
  {
    return _original;
  }
  
  inline void SetOriginal( bool b )
  {
    _original = b;
  }
  
  const RString &GetVisibleTest() const
  {
    return _visibleTest;
  }
};
TypeIsMovableZeroed( RuleVisibleTest );

//! Struct describing conditions for result searching.
struct SentenceResultCondition
{
  //! Is this record original or copy (copied during only one rule unwrapping)?
  bool _original;
  
  //! Index of the subfragment this record is belonging to.
  int _index;
  
  //! Variable described by script expression.
  RString _constant;

  //! Value of "_this" variable in the script expressions.
  Ref< SemanticKnowledge > _thisVariable;
  
  //! Variable of the condition described by script expression.
  RString _variable;

  //! Name of the variable for the condition test of the result (if conditional result).
  RString _variableName;

  //! Condition described by script expression.
  RString _condition;

  //! Metrics described by script expression.
  RString _metrics;
  
  //! Predecessor described by script expression.
  RString _predecessor;
  
  //! Priority of this condition, 1.0 is the highest 0.0 is the lowest.
  float _priority;
  
  //! Required form of the evaluated variable of this condition.
  RString _expression;

  //! Aggregation style code.
  int _aggregation;
  
  //! Aggregation threshold - when split one group into two.
  float _aggregationThreshold;
  
  //! Required condition (only candidates pass this condition can be results).
  bool _required;

  //! How to separate values of this condition?
  int _formatId;
  
  /*! Is this condition representing subject? 
      Subject must be written out always, even if it isn't part of successfulness.
  */
  bool _subject;
  
  //! The kind of part of sentence.
  PartOfSentence _partOfSentence;

  //! Script to transfer information of this condition from speaker's candidate to the listener's candidate.
  RString _transfer;

  //! The result context subject of the question.
  WordBaseId _contextSubject;

  //! Type of the value to convert condition variable to the context notion and format value to the reply.
  short _valueType;
};
TypeIsMovableZeroed( SentenceResultCondition );

//! Struct describing script expression of candidates.
struct SentenceResultCandidates
{
  //! Is this record original or copy (copied during only one rule unwrapping)?
  bool _original;

  //! Candidates are defined by this script expression.
  RString _candidates;
  
  //! Index of the subfragment this record is belonging to.
  int _index;
  
  //! Value of "_this" variable in the script expression.
  Ref< SemanticKnowledge > _thisVariable;
};
TypeIsMovableZeroed( SentenceResultCandidates );

//! Describes name of the semantic table of which candidates must be part.
struct SentenceResultCandidatesTable
{
  //! Is this record original or copy (copied during only one rule unwrapping)?
  bool _original;

  //! Required table for candidates.
  RString _candidatesFromTable;
};
TypeIsMovableZeroed( SentenceResultCandidatesTable );

//! Class describing result formatting.
class FormattedSentenceResult : public RefCount
{
protected:
  //! Threshold - when to use the format.
  float _threshold;
  
  //! What kind of the response ought to be used.
  RString _responseId;
  
  //! What pattern should be used to write out the results (good and bad conditions).
  RString _patternId;
  
  //! Condition for conditional result.
  RString _condition;
  
  //! Action to execute.
  RString _action;
  
  //! Is this record original or copy (copied during only one rule unwrapping)?
  bool _original;

public:
  inline FormattedSentenceResult( float threshold, const RString &responseId, 
    const RString &patternId, const RString &condition, const RString &action )
    : _threshold( threshold ), _responseId( responseId ), _patternId( patternId ), 
    _condition( condition ), _action( action ), _original( true )
  {
  }
  
  inline float GetThreshold() const
  {
    return _threshold;
  }
  
  inline const RString &GetResponseId() const
  {
    return _responseId;
  }
  
  inline const RString &GetPatternId() const
  {
    return _patternId;
  }
  
  inline bool IsOriginal() const
  {
    return _original;
  }
  
  inline void SetOriginal( bool b )
  {
    _original = b;
  }
  
  inline const RString &GetCondition() const
  {
    return _condition;
  }

  inline const RString &GetAction() const
  {
    return _action;
  }

  // Sort the array in descending order.
  static int CompareThreshold( const Ref< const FormattedSentenceResult > *c1, 
    const Ref< const FormattedSentenceResult > *c2 )
  {
    if( ( *c1 )->_threshold < ( *c2 )->_threshold )
    {
      return 1;
    }
    else if( ( *c1 )->_threshold > ( *c2 )->_threshold )
    {
      return -1;
    }
    return 0;
  }
};

/*! Abstract sentence fragment. Derived classes must implement the method "ReadRules" and "Copy".
*/
class SentenceFragment : public RefCount
{
protected:
  //! Key information of this fragment.
  Ref< SentenceAtom > _sentenceAtom;

  //! Kind of the called fragment (doesn't have to be always same for the same fragment).
  short _type;

  //! Knowledge base to store knowledges (semantic net) of the player.
  Ref< const KnowledgeBase > _player;
  
  //! Conditions to search the result of the current sentence in semantic net.
  AutoArray< SentenceResultCondition > _conditions;
  
  //! This record should be only once in whole sentence, but it can be defined also in unwrapped only one rule.
  AutoArray< SentenceResultCandidates > _candidates;
  
  //! This record should be only once in whole sentence, but it can be defined also in unwrapped only one rule.
  AutoArray< SentenceResultCandidatesTable > _candidatesTables;
  
  //! Sentence sub-fragments in the hierarchy.
  RefArray< SentenceFragment > _subfragments;
  
  //! Chosen rule for this sentence fragment.
  Ref< SentenceRule > _chosenRule;

  //! Special rules, added through interface and returned in GetRules() at the beginning.
  RefArray< SentenceRule > _specRules;

protected:
  inline SentenceFragment( const KnowledgeBase *player )
    : _player( player )
  {
  }

public:
  inline SentenceFragment( const KnowledgeBase *player, SentenceAtom *atom, short type, short orderNumber )
    : _player( player ), _sentenceAtom( atom ), _type( type )
  {
  }

  //! Copy constructor
  inline SentenceFragment( const SentenceFragment &src )
  {
    operator =( src );
  }

  //! Assign operator
  inline void operator =( const SentenceFragment &src )
  {
    _sentenceAtom = src._sentenceAtom;
    _type = src._type;
    _player = src._player;
    _conditions = src._conditions;
    _candidates = src._candidates;
    _candidatesTables = src._candidatesTables;

    //.. I hope this is enough to copy just references (is used in GetRules method).
    _subfragments = src._subfragments;

    _chosenRule = src._chosenRule;
    _specRules = src._specRules;
  }
  
  inline void SetSpecRules( const RefArray< SentenceRule > &specRules )
  {
    _specRules = specRules;
  }

  inline const SentenceAtom *GetSentenceAtom() const
  {
    return _sentenceAtom;
  }

  inline SentenceAtom *GetModSentenceAtom()
  {
    return _sentenceAtom;
  }
  
  inline const KnowledgeBase *GetPlayer()
  {
    return _player;
  }
  
  void GetProbableResultsRecursive( RefArray< const FormattedSentenceResult > &results ) const;
  
  void GetConditionalResultsRecursive( RefArray< const FormattedSentenceResult > &results ) const;

  void GetConditionsRecursive( AutoArray< SentenceResultCondition > &conditions ) const;

  void GetCandidatesRecursive( AutoArray< SentenceResultCandidates > &candidates ) const;

  void GetCandidatesTablesRecursive( AutoArray< SentenceResultCandidatesTable > &candidatesTables ) const;

  void GetComplementaryConditionsRecursive( AutoArray< SentenceResultCondition > &conditions );

  void GetComplementarySubjectsRecursive( AutoArray< SentenceResultCondition > &conditions );

  inline int GetConditionsSize() const
  {
    return _conditions.Size();
  }
  
  inline const SentenceResultCondition &GetCondition( int i ) const
  {
    return _conditions[ i ];
  }
  
  inline int GetCandidatesSize() const
  {
    return _candidates.Size();
  }
  
  inline const SentenceResultCandidates &GetCandidates( int i ) const
  {
    return _candidates[ i ];
  }
  
  inline void AddCandidates( const SentenceResultCandidates &src )
  {
    _candidates.Add( src );
  }

  inline int GetCandidatesTablesSize() const
  {
    return _candidatesTables.Size();
  }
  
  inline const SentenceResultCandidatesTable &GetCandidatesTable( int i ) const
  {
    return _candidatesTables[ i ];
  }
  
  inline void AddCandidatesTable( const SentenceResultCandidatesTable &src )
  {
    _candidatesTables.Add( src );
  }
  
  inline void SetChooseRule( SentenceRule *rule )
  {
    _chosenRule = rule;
  }

  inline const SentenceRule *GetChosenRule() const
  {
    return _chosenRule;
  }

  //! Add new sentence fragment into sentence hierarchy.
  inline void AddFragment( SentenceFragment *newFragment )
  {
    _subfragments.Add( newFragment );
    _sentenceAtom->AddSubAtom( newFragment->GetModSentenceAtom() );
  }

  //! Clear the subfragments.
  inline void ClearFragments()
  {
    _subfragments.Clear();
    _sentenceAtom->ClearSubAtoms();
  }
  
  inline void ResetFragment()
  {
    ClearFragments();
    SetMood( UNKNOWN );
    _chosenRule.Free();
  }

  //! Delete one sentence fragment in the hierarchy.
  inline void DeleteFragment( int i )
  {
    _subfragments.DeleteAt( i );
    _sentenceAtom->DeleteAtom( i );
  }

  //! Set subfragments of this fragment.
  inline void CopyFragments( const RefArray< SentenceFragment > &srcFragments )
  {
    _subfragments = srcFragments;
    RefArray< SentenceAtom > atoms;
    int n = srcFragments.Size();
    for( int i = 0; i < n; ++i )
    {
      atoms.Add( srcFragments[ i ]->GetModSentenceAtom() );
    }
    _sentenceAtom->CopyAtoms( atoms );
  }

  //! Fragment description (used during sentence creation).
  inline void GetDescription( const WordBase *wb, RString &desc ) const
  {
    if( _type == Terminator )
    {
      _sentenceAtom->GetText( wb, desc );
    }
    else
    {
      RString inside;
      _sentenceAtom->GetText( wb, inside );
      RString left( "<", inside );
      desc = RString( left, ">" );
    }
  }

  inline WordBaseId GetWordBaseId() const
  {
    return _sentenceAtom->GetWordBaseId();
  }

  /*! Custom data for custom words (generated from code not from word base)
      Must be descendant of RefCount!
  */
  inline const SemanticKnowledge *GetKnowledgeData() const
  {
    return _sentenceAtom->GetKnowledgeData();
  }

  inline void SetKnowledgeData( SemanticKnowledge *customData )
  {
    return _sentenceAtom->SetKnowledgeData( customData );
  }

  inline bool HasSpaceInFront() const
  {
    return _sentenceAtom->HasSpaceInFront( _player->GetWordBase() );
  }
  
  inline bool HasNeverSpaceBehind() const
  {
    return _sentenceAtom->HasNeverSpaceBehind( _player->GetWordBase() );
  }

  //! Is it a leaf?
  inline bool IsTerminator() const
  {
    return _type == Terminator;
  }

  //! Get the fragment type.
  inline short GetType() const
  {
    return _type;
  }

  //! Is required this fragment?
  inline void SetType( short type )
  {
    _type = type;
  }

  //! Fragment text (used during sentence creation).
  inline void GetText( RString &desc ) const
  {
    _sentenceAtom->GetText( _player->GetWordBase(), desc );
  }
  
  //! Add punctuation to the sentence text.
  inline void AddPunctuation( RString &text )
  {
    _sentenceAtom->AddPunctuation( text );
  }
  
  //! Set the mood of this fragment.
  inline void SetMood( short mood )
  {
    _sentenceAtom->SetMood( mood );
  }
  
  //! Get the mood of this fragment.
  inline short GetMood() const
  {
    return _sentenceAtom->GetMood();
  }
  
  virtual bool RulesNotEmpty();

  //! Creates copy; doesn't have to create new SentenceAtom for this copy.
  virtual void Copy( Ref< SentenceFragment > &dst ) const
  {
    dst = new SentenceFragment( *this );
  }

  /*! Fragment can be set of rules. Returns index of rule, which was applied last time.
      -1 if none was yet applied.
  */
  int GetRules( RefArray< SentenceRule > &rules );

protected:
  //! Fragment can be set of rules.
  virtual void ReadRules( RefArray< SentenceRule > &rules ) const
  {
    // First implementation generates no rule.
  }
};

//! Sentence fragment can be replaced by one rule from a set of them.
class SentenceRule : public RefCount
{
protected:
  //! Sub-fragments of the rule.
  RefArray< SentenceFragment > _subfragments;
  
  //! New mood code for the fragment.
  short _mood;
  
  //! Conditions for result searching.
  AutoArray< SentenceResultCondition > _conditions;

  //! Formats for different results (e.g. I know./I don't know.).
  RefArray< FormattedSentenceResult > _results;

  //! This record should be only once in whole sentence, but it can be defined also in unwrapped only one rule.
  AutoArray< SentenceResultCandidates > _candidates;
  
  //! This record should be only once in whole sentence, but it can be defined also in unwrapped only one rule.
  AutoArray< SentenceResultCandidatesTable > _candidatesTables;
  
  //! Array of visible tests required to be passed to be this rule displayed.
  AutoArray< RuleVisibleTest > _visibleTests;

public:
  SentenceRule()
    : _mood( UNKNOWN )
  {
  }

  inline void AddResult( FormattedSentenceResult *result )
  {
    _results.Add( result );
  }
  
  inline RefArray< FormattedSentenceResult > &GetModResults()
  {
    return _results;
  }

  inline const FormattedSentenceResult *GetResult( int i ) const
  {
    return _results[ i ];
  }
  
  inline int GetResultsSize() const
  {
    return _results.Size();
  }

  inline void AddCondition( const SentenceResultCondition &cond )
  {
    _conditions.Add( cond );
  }

  inline int GetConditionsSize() const
  {
    return _conditions.Size();
  }
  
  inline const SentenceResultCondition &GetCondition( int i ) const
  {
    return _conditions[ i ];
  }

  inline AutoArray< SentenceResultCondition > &GetModConditions()
  {
    return _conditions;
  }

  //! Add conditions from this sentence rule into array parameter.
  inline void GetConditions( AutoArray< SentenceResultCondition > &conditions ) const
  {
    int n = _conditions.Size();
    for( int i = 0; i < n; ++i )
    {
    	int newIdx = conditions.Add( _conditions[ i ] );
    	SentenceResultCondition &newCon = conditions[ newIdx ];
    	if( newCon._index >= 0 )
    	{
        AssertDebug( _subfragments[ newCon._index ]->IsTerminator() );
        newCon._thisVariable = new SemanticKnowledge( *_subfragments[ newCon._index ]->GetSentenceAtom()->GetKnowledgeData() );
    	}
    }
  }
  
  inline void GetModCondition( int i, SentenceResultCondition &c ) const
  {
    c = _conditions[ i ];
    if( c._index >= 0 )
    {
      AssertDebug( _subfragments[ c._index ]->IsTerminator() );
      c._thisVariable = new SemanticKnowledge( *_subfragments[ c._index ]->GetSentenceAtom()->GetKnowledgeData() );
    }
  }
  
  inline void AddCandidates( const SentenceResultCandidates &src )
  {
    _candidates.Add( src );
  }

  inline int GetCandidatesSize() const
  {
    return _candidates.Size();
  }
  
  inline const SentenceResultCandidates &GetCandidates( int i ) const
  {
    return _candidates[ i ];
  }

  inline AutoArray< SentenceResultCandidates > &GetModCandidates()
  {
    return _candidates;
  }
  
  inline void GetCandidates( AutoArray< SentenceResultCandidates > &candidates )
  {
    int n = _candidates.Size();
    for( int i = 0; i < n; ++i )
    {
    	int newIdx = candidates.Add( _candidates[ i ] );
    	SentenceResultCandidates &newCan = candidates[ newIdx ];
    	if( newCan._index >= 0 )
    	{
        AssertDebug( _subfragments[ newCan._index ]->IsTerminator() );
        newCan._thisVariable = new SemanticKnowledge( *_subfragments[ newCan._index ]->GetSentenceAtom()->GetKnowledgeData() );
    	}
    }
  }

  inline void AddCandidatesTable( const SentenceResultCandidatesTable &src )
  {
    _candidatesTables.Add( src );
  }

  inline int GetCandidatesTablesSize() const
  {
    return _candidatesTables.Size();
  }

  inline const SentenceResultCandidatesTable &GetCandidatesTable( int i ) const
  {
    return _candidatesTables[ i ];
  }

  inline AutoArray< SentenceResultCandidatesTable > &GetModCandidatesTables()
  {
    return _candidatesTables;
  }
  
  inline void GetCandidatesTables( AutoArray< SentenceResultCandidatesTable > &tables )
  {
    int n = _candidatesTables.Size();
    for( int i = 0; i < n; ++i )
    {
    	tables.Add( _candidatesTables[ i ] );
    }
  }
  
  inline AutoArray< RuleVisibleTest > &GetModVisibleTests()
  {
    return _visibleTests;
  }
  
  inline int GetVisibleTestsSize() const
  {
    return _visibleTests.Size();
  }
  
  inline const RuleVisibleTest &GetVisibleTest( int i ) const
  {
    return _visibleTests[ i ];
  }
  
  inline void AddVisibleTest( const RuleVisibleTest &vt )
  {
    _visibleTests.Add( vt );
  }
  
  bool IsVisible( const KnowledgeBase *participant ) const
  {
    int n = _visibleTests.Size();
    if( n != 0 )
    {
      // Create space for variables.
      GameVarSpace vars;
      GGameState.BeginContext(&vars);

      // Register variable of the replaying speaker.
      GGameState.VarSet( "_me", GameValue( new GameDataKnowledgeBase( participant ) ), true );

      int i = 0;
      do {
        const RString &vt = _visibleTests[ i ].GetVisibleTest();
        GameValue gv = GGameState.Evaluate( vt );
        AssertDebug( GGameState.GetLastError() == EvalOK );
        AssertDebug( gv.GetType() == GameBool );
#ifdef _DEBUG
        BString< 128 > buf;
        buf.PrintF( "VisibleTest \"%s\": %s", vt.Data(), static_cast< GameBoolType >( gv ) ? "true" : "false" );
        Log( buf );
#endif

        if( !static_cast< GameBoolType >( gv ) )
        {
          GGameState.EndContext();
          return false;
        }
        
        ++i;
      } while( i < n );

      GGameState.VarDelete( "_me" );
      GGameState.EndContext();
    }
    
    // Test generated terminators (e.g. pronouns).
    return RulesNotEmpty();
  }
  
  bool RulesNotEmpty() const
  {
    int n = _subfragments.Size();
    if( n != 0 )
    {
      int i = 0;
      do {
        Ref< SentenceFragment > sf = _subfragments[ i ];
        if( sf->GetType() == Required && !sf->RulesNotEmpty() )
        {
          return false;
        }
      	++i;
      } while( i < n );
      return true;
    }

    return false;
  }

  //! Get text of this rule.
  void GetText( const WordBase *wb, RString &text ) const
  {
    int n = _subfragments.Size();
    if( n != 0 )
    {
      SentenceFragment *pf = _subfragments[ 0 ];
      pf->GetDescription( wb, text );
      RString tmpDesc;
      RString space( " " );
      for( int i = 1; i < n; ++i )
      {
        SentenceFragment *sf = _subfragments[ i ];
        if( !pf->HasNeverSpaceBehind() && sf->HasSpaceInFront() )
        {
          text = RString( text, space );
        }
        pf = sf;
        sf->GetDescription( wb, tmpDesc );
        text = RString( text, tmpDesc );
      }
    }
    SentenceAtom::AddPunctuation( _mood, text );
  }
  
  inline const RefArray< SentenceFragment > &GetSubfragments() const
  {
    return _subfragments;
  }

  //! Get the number of sentence fragments in this rule.
  inline int GetSubfragmentsSize() const
  {
    return _subfragments.Size();
  }

  //! Get particular sentence fragment in this rule.
  inline const SentenceFragment *GetSubfragment( int i ) const
  {
    return _subfragments.Get( i );
  }

  //! Get particular sentence fragment in this rule.
  inline SentenceFragment *GetModSubfragment( int i )
  {
    return _subfragments.Set( i );
  }

  //! Add next fragment into sentence rule.
  inline void AddSubfragment( SentenceFragment *subfragment )
  {
    _subfragments.Add( subfragment );
  }

  //! Replace sub-fragments in <from; from + count) by new sub-fragments.
  inline void ReplaceSubfragments( int from, int count, const RefArray< SentenceFragment > &newFrags )
  {
    static_cast< AutoArray< Ref< SentenceFragment > >& >( _subfragments ).Delete( from, count );
    int n = newFrags.Size();
    for( int i = 0; i < n; ++i )
    {
      _subfragments.Insert( from + i, newFrags[ i ] );
    }
  }

  //! Set the mood of this rule (mood of the fragment replaced by this rule will be changed to this value).
  inline void SetMood( short mood )
  {
    _mood = mood;
  }

  //! Get the mood of this rule (mood of the fragment replaced by this rule will be changed to this value).
  inline short GetMood() const
  {
    return _mood;
  }
  
  inline bool IsEqual( const SentenceRule *rule, const WordBase &wordBase ) const
  {
    if( rule != NULL )
    {
      int n = _subfragments.Size();
      if( n == rule->GetSubfragmentsSize() )
      {
        for( int i = 0; i < n; ++i )
        {
      	  if( !rule->GetSubfragment( i )->GetSentenceAtom()->GetKnowledgeData()->IsEqual( 
      	    *_subfragments[ i ]->GetSentenceAtom()->GetKnowledgeData(), wordBase ) )
      	  {
      	    return false;
      	  }
        }
        return true;
      }
    }
    return false;
  }
  
  inline void Copy( Ref< SentenceRule > &dst )
  {
    dst = new SentenceRule();
    dst->SetMood( GetMood() );
    dst->_conditions = _conditions;
    dst->_results = _results;
    dst->_candidates = _candidates;
    dst->_candidatesTables = _candidatesTables;
    dst->_visibleTests = _visibleTests;
    Ref< SentenceFragment > fragCopy;
    int n = _subfragments.Size();
    for( int i = 0; i < n; ++i )
    {
      _subfragments[ i ]->Copy( fragCopy );
    	dst->AddSubfragment( fragCopy );
    }
  }
};

const GameType GameRule( 0x400000 );

#include <Es/Memory/normalNew.hpp>

class GameDataRule : public GameData
{
  typedef GameData base;

protected:
  Ref< SentenceRule > _rule;
  
public:
  GameDataRule()
  {
  }
  
  GameDataRule( SentenceRule *rule )
    : _rule( rule )
  {
  }

  ~GameDataRule()
  {
  }

  GameType GetType() const
  {
    return GameRule;
  }

  SentenceRule *GetRule() const
  {
    return _rule;
  }

  RString GetText() const;

  bool IsEqualTo( const GameData *data ) const;

  const char *GetTypeName() const
  {
    return "rule";
  }

  GameData *Clone() const
  {
    return new GameDataRule( *this );
  }

  LSError Serialize( ParamArchive &ar );

  USE_FAST_ALLOCATOR
};

#include <Es/Memory/debugNew.hpp>

static inline SentenceRule *GetRule( GameValuePar oper )
{
  return ( oper.GetType() == GameRule ) ? static_cast< GameDataRule* >( oper.GetData() )->GetRule() : NULL;
}

GameData *CreateGameDataRule();

#endif