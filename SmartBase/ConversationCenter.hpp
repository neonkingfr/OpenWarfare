
#ifndef _CONVERSATIONCENTER_HPP
#define _CONVERSATIONCENTER_HPP

#include "KnowledgeBase.hpp"
#include "SentenceRule.hpp"

//! Class providing information in dialog initialization. Player can choose who does he want to talk to.
class ConversationCenter : public RefCount
{
protected:
  inline ConversationCenter()
  {
  }
  
public:
  virtual void GetParticipantName( int i, RString &name ) = 0;
  
  //! Get KnowledgeBase of particular conversation participant.
  virtual void GetParticipant( int i, Ref< KnowledgeBase > &kb ) = 0;
  
  //! Get the number of the available conversation participants.
  virtual int GetParticipantsSize() = 0;
  
  //! Get SentenceFragment root for particular conversation participant.
  virtual void GetSentenceRuleRoot( const KnowledgeBase *player, const RefArray< KnowledgeBase > &participants, 
    Ref< SentenceFragment > &root ) = 0;
  
  virtual WordBase *GetWordBase() = 0;
};

#endif