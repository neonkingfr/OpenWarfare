
#ifndef _KNOWLEDGEBASE_HPP
#define _KNOWLEDGEBASE_HPP

#include "Es/essencepch.hpp"
#include "Es/Strings/rString.hpp"
#include "Es/Strings/bstring.hpp"
#include "El/elementpch.hpp"
#include "El/Evaluator/express.hpp"

#define FORM_DEFAULT                     0 // default is singular, no article, present time, infinitive
// Required word class
#define FORM_INDEFINITE                  1 // 1 >> 0; ",", ".", "(", ")", ...
#define FORM_NOUN                        2 // 1 >> 1
#define FORM_ADJECTIVE                   4 // 1 >> 2
#define FORM_PRONOUN                     8 // 1 >> 3
#define FORM_NUMERAL                    16 // 1 >> 4
#define FORM_VERB                       32 // 1 >> 5
#define FORM_ADVERB                     64 // 1 >> 6
#define FORM_PREPOSITION               128 // 1 >> 7
#define FORM_CONJUNCTION               256 // 1 >> 8
#define FORM_PARTICLE                  512 // 1 >> 9
#define FORM_INTERJECTION             1024 // 1 >> 10
#define FORM_ALL_WORDCLASSES              ( FORM_INDEFINITE + FORM_NOUN + FORM_ADJECTIVE + FORM_PRONOUN + FORM_NUMERAL + FORM_VERB +\
                                          FORM_ADVERB + FORM_PREPOSITION + FORM_CONJUNCTION + FORM_PARTICLE + FORM_INTERJECTION )
// Nouns.
#define FORM_PLURAL                   2048 // 1 >> 11; singular is default
#define FORM_INDEFINITE_ARTICLE       4096 // 1 >> 12; show indefinite article
#define FORM_DEFINITE_ARTICLE         8192 // 1 >> 13; show definite article
// Verbs.
#define FORM_FIRST_PERSON            16384 // 1 >> 14
#define FORM_SECOND_PERSON           32768 // 1 >> 15
#define FORM_THIRD_PERSON            65536 // 1 >> 16
#define FORM_PAST_TENSE             131072 // 1 >> 17
#define FORM_PAST_PARTICIPLE        262144 // 1 >> 18
#define FORM_PRESENT_TENSE          524288 // 1 >> 19
#define FORM_FUTURE_TENSE          1048576 // 1 >> 20
#define FORM_CONDITIONAL_MOOD      2097152 // 1 >> 21
#define FORM_GERUND                4194304 // 1 >> 22
// Pronouns. [default is demonstrative pronoun]
#define FORM_PERSONAL              8388608 // 1 >> 23

#define GENDER_HE                        1 // 1 >> 0
#define GENDER_SHE                       2 // 1 >> 1
#define GENDER_IT                        4 // 1 >> 2

class SemanticNet;

typedef int WordBaseId;

//! IDs of parts of sentence to define word order.
enum PartOfSentence
{
  POS_Object = 0,
  POS_PlaceAdverbial,
  POS_TimeAdverbial,
  POS_Size,
};

template< typename Type, typename ParamType >
struct RefArrayKeyWithParamTraits
{
  typedef const Type &KeyType;
  inline static bool IsEqual(KeyType a, KeyType b, ParamType param)
  {
    return a.NotNull() && b.NotNull() && a->IsEqual( *b, param );
  }
  inline static KeyType GetKey(const Type &a) {return a;}
};

template< class Type, typename ParamType, class Traits=FindArrayKeyTraits< Type >, class Allocator=MemAllocD >
class FindArrayKeyWithParam : public FindArrayKey< Type, Traits, Allocator >
{
  typedef typename Traits::KeyType KeyType;

public:
  //! add unless there already is an item with the same key
  int AddUnique( const Type &src, ParamType param );
  //! find first item with the same key
  int Find( const Type &src, ParamType param ) const {return FindKey(Traits::GetKey(src), param);}
  //! find item by key
  int FindKey(KeyType key, ParamType param) const;
  //! delete first item with the same key
  bool Delete( const Type &src, ParamType param ){return DeleteKey(Traits::GetKey(src), param);}
  //! delete first item with given key
  bool DeleteKey(KeyType src, ParamType param);
  //! delete all items with given key
  void DeleteAllKey(KeyType src, ParamType param);
  void DeleteAt( int index ) {AutoArray<Type,Allocator>::Delete(index,1);}

  ClassIsMovable(FindArrayKeyWithParam);
};

template< class Type, typename ParamType, class Traits, class Allocator >
int FindArrayKeyWithParam<Type,ParamType,Traits,Allocator>::FindKey( KeyType key, ParamType param ) const
{
  //for( int i=0; i<Size(); i++ ) if( (*this)[i]==src ) return i;
  for( int i=0; i<Size(); i++ ) if( Traits::IsEqual(Traits::GetKey(Get(i)),key,param) ) return i;
  return -1;
}

template< class Type, typename ParamType, class Traits, class Allocator >
int FindArrayKeyWithParam<Type,ParamType,Traits,Allocator>::AddUnique( const Type &src, ParamType param )
{
  if( Find(src,param)>=0 ) return -1;
  int s=Size();
  Resize(s+1);
  (*this)[s]=src;
  return s;
}

template< class Type, typename ParamType, class Traits, class Allocator >
bool FindArrayKeyWithParam<Type,ParamType,Traits,Allocator>::DeleteKey(KeyType key, ParamType param)
{
  int index=FindKey(key, param);
  if( index<0 ) return false;
  DeleteAt(index);
  return true;
}

template< class Type, typename ParamType, class Traits, class Allocator >
void FindArrayKeyWithParam<Type,ParamType,Traits,Allocator>::DeleteAllKey(KeyType key, ParamType param)
{
  int d=0;
  for( int s=0; s<Size(); s++ )
  {
    if( !Traits::IsEqual(Traits::GetKey(Get(s)),key,param) )
    {
      if( s!=d ) Set(d)=Get(s);
      d++;
    }
  }
  Resize(d);
}

//! Implementation of word dictionary.
class WordBase : public RefCount
{
public:
  WordBaseId _wordIdScalarId;
  WordBaseId _wordIdCustomId;
  WordBaseId _wordIdVariableId;
  
  WordBaseId _wordIdCancel;

  WordBaseId _wordIdPeriodId;
  WordBaseId _wordIdCommaId;
  WordBaseId _wordIdQuestionMarkId;
  WordBaseId _wordIdExclamationMarkId;
  WordBaseId _wordIdLeftBracketId;
  WordBaseId _wordIdRightBracketId;
  WordBaseId _wordIdAnd;
  
  WordBaseId _wordIdMinute;
  WordBaseId _wordIdHour;
  WordBaseId _wordIdDay;
  
  WordBaseId _wordIdNorth;
  WordBaseId _wordIdSouth;
  WordBaseId _wordIdWest;
  WordBaseId _wordIdEast;
  WordBaseId _wordIdNorthEast;
  WordBaseId _wordIdNorthWest;
  WordBaseId _wordIdSouthEast;
  WordBaseId _wordIdSouthWest;
  
  WordBaseId _wordIdMeter;
  WordBaseId _wordIdKilometer;
  WordBaseId _wordIdKmph;

  WordBaseId _wordIdI;
  WordBaseId _wordIdYou;
  WordBaseId _wordIdHe;
  WordBaseId _wordIdShe;
  WordBaseId _wordIdIt;
  WordBaseId _wordIdWe;
  WordBaseId _wordIdThey;

  //! Converts name of the word to the WordBaseId.
  virtual WordBaseId ConvertNameToId( const char *name ) const = 0;
  
  //! Tries to convert text to WordBaseId.
  virtual WordBaseId ConvertTextToId( const RStringB &text ) const = 0;

  //! The text of the word.
  virtual void GetWordText( WordBaseId id, long form, RString &text ) const = 0;

  //! The word class.
  virtual short GetWordClass( WordBaseId id ) const = 0;

  //! Get noun gender.
  virtual short GetNounGender( WordBaseId id ) const = 0;
  
  //! Get all possible demonstrative pronouns of this dictionary.
  virtual void GetDemonstrativePronouns( AutoArray< WordBaseId > &pronouns ) const = 0;

  //! Get all possible demonstrative pronouns of this dictionary without "I".
  virtual void GetDemonstrativePronounsWithoutI( AutoArray< WordBaseId > &pronouns ) const = 0;

  //! Get WordBaseId of the personal pronoun of the parameter word.
  inline WordBaseId GetDemonstrativePronounOfWord( WordBaseId id, long form ) const
  {
    return GetDemonstrativePronounOfGender( GetNounGender( id ), form );
  }
  
  //! Get WordBaseId of the personal pronoun for given noun gender.
  virtual WordBaseId GetDemonstrativePronounOfGender( short gender, long form ) const = 0;
  
  //! Converts outcoming demonstrative pronoun to incoming.
  virtual void SwitchIncomingAndOutcoming( WordBaseId incoming, AutoArray< WordBaseId > &outcoming ) const = 0;
  
  //! Has word space in front of it?
  virtual bool HasSpaceInFront( WordBaseId id ) const = 0;

  //! Word has never space behind if true.
  virtual bool HasNeverSpaceBehind( WordBaseId id ) const = 0;

  //! Return the semantic key - immune to localization.
  virtual void GetSemanticKey( WordBaseId id, RString &key ) const = 0;

  //! Get order number of the particular part of sentence.
  virtual short GetWordOrder( PartOfSentence pos ) const = 0;
  
  //! Get a hypernym of the passed word.
  virtual WordBaseId GetHypernym( WordBaseId wordId, long form ) const = 0;
};

/*! The only one implementation of knowledge stored in semantic net. 
    Probably will be changed or even changed to the abstract interface.
*/
class SemanticKnowledge : public RefCount
{
protected:
  //! WordBaseId of this knowledge.
  WordBaseId _wordBaseId;
  
  //! Custom text representation.
  RStringB _text;
  
  //! Float representation.
  float _scalar;

  //! Code of the particular form.
  long _form;

public:
  //! Constructor for not custom word base id.
  inline SemanticKnowledge( WordBaseId wordBaseId, long form )
    : _wordBaseId( wordBaseId ), _scalar( 0.0 ), _form( form )
  {
    AssertDebug( _wordBaseId >= 0 );
  }

  //! Constructor for custom word base id.
  inline SemanticKnowledge( WordBaseId wordBaseId, const RStringB &text, long form, const WordBase &wb )
    : _scalar( 0.0 ), _form( form )
  {
    AssertDebug( wordBaseId == 1 );
    AssertDebug( text.GetRefCount() != 0 );

    WordBaseId id = wb.ConvertTextToId( text );
    if( id == -1 )
    {
      _wordBaseId = wordBaseId;
      _text = text;
    }
    else
    {
      _wordBaseId = id;
    }
  }

  //! Constructor for floats.
  inline SemanticKnowledge( WordBaseId wordBaseId, const float scalar, long form )
    : _wordBaseId( wordBaseId ), _scalar( scalar ), _form( form )
  {
    AssertDebug( wordBaseId == 0 );
  }

  //! Copy constructor.
  inline SemanticKnowledge( const SemanticKnowledge &src )
  {
    operator =( src );
  }

  //! Copy constructor with new form code.
  inline SemanticKnowledge( const SemanticKnowledge &src, long form )
  {
    operator =( src );
    _form = form;
  }

  //! Assign operator.
  inline void operator =( const SemanticKnowledge &src )
  {
    _wordBaseId = src._wordBaseId;
    _text = src._text;
    _scalar = src._scalar;
    _form = src._form;
  }

  //! Receive the text of this knowledge.
  inline void GetText( const WordBase *wb, RString &text ) const
  {
    GetText( wb, _form, text );
  }
  
  //! Receive the text of this knowledge.
  inline void GetText( const WordBase *wb, long form, RString &text ) const
  {
    if( _wordBaseId != wb->_wordIdCustomId )
    {
      if( _wordBaseId != wb->_wordIdScalarId )
      {
        wb->GetWordText( _wordBaseId, form, text );
        return;
      }
      
      BString< 13 > x; // size: 1 for sign, 7 for digits, 1 for point, 3 for digits, 1 for termination
      x.PrintF( "%g", _scalar );
      text = static_cast< const char* >( x );
      return;
    }

    text = _text;
    return;
  }

  // Get the possible word classes.
  inline short GetWordClass( const WordBase *wb ) const
  {
    return wb->GetWordClass( _wordBaseId );
  }

  //! Required method.
  inline WordBaseId GetWordBaseId() const
  {
    return _wordBaseId;
  }
  
  //! Get the scalar representation.
  inline float GetScalar() const
  {
    return _scalar;
  }

  //! Return the code of the particular word form.
  inline long GetForm() const
  {
    return _form;
  }

  /*! Determine if current word can have required form. 
      (if no word class is required it is same as all word classes are possible)
  */
  inline bool CanHaveForm( const WordBase &wb, long requiredForm ) const
  {
    requiredForm = requiredForm & FORM_ALL_WORDCLASSES;
    return requiredForm != 0 ? ( requiredForm & wb.GetWordClass( _wordBaseId ) ) != 0 : true;
  }

  //! Set the code of the particular word form.
  inline void SetForm( long form )
  {
    _form = form;
  }

  //! Required operator.
  inline bool IsEqual( const SemanticKnowledge &src, const WordBase &wordBase ) const
  {
    return _wordBaseId == src._wordBaseId && 
      ( _wordBaseId != wordBase._wordIdCustomId || _text == src._text ) &&
      ( _wordBaseId != wordBase._wordIdScalarId || _scalar == src._scalar);
  }

  //! Return the semantic key - immune to localization.
  inline void GetSemanticKey( const WordBase *wb, RString &key ) const
  {
    if( _wordBaseId != wb->_wordIdCustomId )
    {
      if( _wordBaseId != wb->_wordIdScalarId )
      {
        return wb->GetSemanticKey( _wordBaseId, key );
      }

      char x[ 13 ]; // size: 1 for sign, 7 for digits, 1 for point, 3 for digits, 1 for termination
      static const xLast = sizeof( x ) / sizeof( *x ) - 1;
      ::snprintf( x, xLast, "%g", _scalar );
      x[ xLast ] = '\0';
      key = x;
      return;
    }

    key = _text;
    return;
  }

  //! Receive the hypernyms of this knowledge.
  inline void GetHypernyms( long form, const WordBase &wb, RefArray< const SemanticKnowledge > &hypernyms ) const
  {
    hypernyms.Add( this );
    WordBaseId hypId = wb.GetHypernym( _wordBaseId, form );
    while( hypId != -1 )
    {
      hypernyms.Add( new SemanticKnowledge( hypId, form ) );
      hypId = wb.GetHypernym( hypId, form );
    }
  }
  
  //! Receive the hypernyms of this knowledge.
  inline void GetHypernyms( const WordBase &wb, RefArray< const SemanticKnowledge > &hypernyms ) const
  {
    GetHypernyms( _form, wb, hypernyms );
  }
  
  static void SwitchIncomingAndOutcoming( 
    const RefArray< const SemanticKnowledge > &incomings,
    RefArray< const SemanticKnowledge > &outcomings, const WordBase &wb )
  {
    FindArrayKeyWithParam< Ref< const SemanticKnowledge >, const WordBase &, 
      RefArrayKeyWithParamTraits< Ref< const SemanticKnowledge >, const WordBase & > > uniques;
    AutoArray< WordBaseId > switched;
    int incomingsN = incomings.Size();
    for( int incomingsI = 0; incomingsI < incomingsN; ++incomingsI )
    {
      const SemanticKnowledge *know = incomings[ incomingsI ];
    	wb.SwitchIncomingAndOutcoming( know->GetWordBaseId(), switched );
    	int switchedN = switched.Size();
    	for( int switchedI = 0; switchedI < switchedN; ++switchedI )
    	{
    	  Ref< const SemanticKnowledge > switchedKnow = 
    	    new SemanticKnowledge( switched[ switchedI ], know->GetForm() );
    		uniques.AddUnique( switchedKnow, wb );
    	}
    	switched.Clear();
    }
    outcomings.Copy( uniques.Data(), uniques.Size() );
  }
};

//! This is knowledge's representation and knowledge's type in script language.
const GameType GameKnowledge( 0x20000 );

#include <Es/Memory/normalNew.hpp>

class GameDataKnowledge : public GameData
{
  typedef GameData base;

  Ref< const SemanticKnowledge > _knowledge;

public:
  inline GameDataKnowledge()
  {
  }

  GameDataKnowledge( const SemanticKnowledge *knowledge )
    : _knowledge( knowledge )
  {
  }

  ~GameDataKnowledge()
  {
  }

  GameType GetType() const
  {
    return GameKnowledge;
  }

  const SemanticKnowledge *GetKnowledge() const
  {
    return _knowledge;
  }

  RString GetText() const;

  bool IsEqualTo( const GameData *data ) const;

  const char *GetTypeName() const
  {
    return "knowledge";
  }

  GameData *Clone() const
  {
    return new GameDataKnowledge( *this );
  }

  LSError Serialize( ParamArchive &ar );

  USE_FAST_ALLOCATOR
};

#include <Es/Memory/debugNew.hpp>

static inline const SemanticKnowledge *GetKnowledge( GameValuePar oper )
{
  return ( oper.GetType() == GameKnowledge ) ? static_cast< GameDataKnowledge* >( oper.GetData() )->GetKnowledge() : NULL;
}

GameData *CreateGameDataKnowledge();

static inline RString GetSemanticKey( GameValuePar oper, const WordBase &wb )
{
  if( oper.GetType() == GameKnowledge )
  {
    RString result;
    static_cast< GameDataKnowledge* >( oper.GetData() )->GetKnowledge()->GetSemanticKey( &wb, result );
    return result;
  }
  if( oper.GetType() == GameString )
  {
    return oper;
  }
  AssertDebug( 0 );
  return RString();
}

//! Implementation of knowledge base using word dictionary and semantic net.
class KnowledgeBase : public RefCount
{
protected:
  Ref< WordBase > _wordBase;
  Ref< SemanticNet > _semanticNet;

public:
  inline void SetWordBase( WordBase *wordBase )
  {
    _wordBase = wordBase;
  }

  inline const WordBase *GetWordBase() const
  {
    return _wordBase;
  }

  void SetSemanticNet( SemanticNet *semanticNet );

  inline const SemanticNet *GetSemanticNet() const
  {
    return _semanticNet;
  }

  inline SemanticNet *GetModSemanticNet()
  {
    return _semanticNet;
  }
};

//! This is knowledge base's (containing semantic net) representation and its type in script language.
const GameType GameKnowledgeBase( 0x40000 );

#include <Es/Memory/normalNew.hpp>

class GameDataKnowledgeBase : public GameData
{
  typedef GameData base;

  Ref< const KnowledgeBase > _participant;

public:
  inline GameDataKnowledgeBase()
  {
  }

  GameDataKnowledgeBase( const KnowledgeBase *participant )
    : _participant( participant )
  {
  }

  ~GameDataKnowledgeBase()
  {
  }

  GameType GetType() const
  {
    return GameKnowledgeBase;
  }

  const KnowledgeBase *GetParticipant() const
  {
    return _participant;
  }

  RString GetText() const;

  bool IsEqualTo( const GameData *data ) const;

  const char *GetTypeName() const
  {
    return "knowledgeBase";
  }

  GameData *Clone() const
  {
    return new GameDataKnowledgeBase( *this );
  }

  LSError Serialize( ParamArchive &ar );

  USE_FAST_ALLOCATOR
};

#include <Es/Memory/debugNew.hpp>

static inline const KnowledgeBase *GetKnowledgeBase( GameValuePar oper )
{
  return ( oper.GetType() == GameKnowledgeBase ) ? static_cast< GameDataKnowledgeBase* >( oper.GetData() )->GetParticipant() : NULL;
}

GameData *CreateGameDataKnowledgeBase();

#endif