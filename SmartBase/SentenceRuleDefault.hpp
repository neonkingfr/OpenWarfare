
#ifndef _SENTENCERULEDEFAULT_HPP
#define _SENTENCERULEDEFAULT_HPP

#include "El/elementpch.hpp"
#include "El/ParamFile/paramFile.hpp"

#include "SentenceRule.hpp"

//! Struct describing generator of terminator or generator link.
struct GeneratorInfo
{
  //! Is the terminating generator a float value?
  bool _isScalar;
  
  //! Name of the generator.
  RString _name;
  
  //! Value of the scalar terminating generator.
  float _scalar;
  
  //! Required form of the terminating generator.
  long _form;
};
TypeIsMovableZeroed( GeneratorInfo );

//! Default implementation of fragment reading its definition from ParamFile.
class SentenceFragmentDefault : public SentenceFragment
{
  typedef SentenceFragment base;
  
protected:
  const ParamEntry *_paramFile;
  RString _ruleClassName;
  int _phrasePartIndex;
  AutoArray< GeneratorInfo > _generators;
  RString _generateRules;

public:
  //! Copy constructor
  inline SentenceFragmentDefault( const SentenceFragmentDefault &src )
    : SentenceFragment( src ), _paramFile( src._paramFile ), _ruleClassName( src._ruleClassName ),
    _phrasePartIndex( src._phrasePartIndex ), _generators( src._generators )
  {
  }
  
  //! Constructor for terminator fragments
  inline SentenceFragmentDefault( const KnowledgeBase *player, SentenceAtom *atom )
    : SentenceFragment( player, atom, Terminator, 0 ), _paramFile( NULL )
  {
  }

  //! Constructor for branch fragments initialized from list of phrase parts.
  inline SentenceFragmentDefault( const KnowledgeBase *player, const ParamEntry &paramFile, const RString &ruleClassName, 
    int phrasePartIndex )
    : SentenceFragment( player ), _paramFile( &paramFile ), _ruleClassName( ruleClassName ), 
    _phrasePartIndex( phrasePartIndex )
  {
    ParamEntryVal ruleClass = *_paramFile >> ruleClassName;
    const IParamArrayValue &call = ( ruleClass >> "phraseParts" )[ _phrasePartIndex ];
    ParamEntryVal branchClass = *_paramFile >> call[ 0 ].GetValue();
    _sentenceAtom = new SentenceAtom( new SemanticKnowledge( _player->GetWordBase()->_wordIdCustomId, 
      ( branchClass >> "description" ).GetValue(), FORM_DEFAULT, *_player->GetWordBase() ) );
    _type = call[ 1 ].GetInt();
    
    ReadConditions( *_player->GetWordBase(), branchClass, 0, _conditions );
    ReadCandidates( branchClass, 0, _candidates );
    ReadCandidatesTables( branchClass, _candidatesTables );

    // Read rules generators names
    ReadGeneratorNames( branchClass );
  }
  
  //! Constructor for root class (it must be branch and is required).
  inline SentenceFragmentDefault( const KnowledgeBase *player, const ParamEntry &paramFile, const RString &rootClassName )
    : SentenceFragment( player ), _paramFile( &paramFile )
  {
    ParamEntryVal branchClass = *_paramFile >> rootClassName;
    Ref< SemanticKnowledge > know = new SemanticKnowledge( _player->GetWordBase()->_wordIdCustomId,
      ( branchClass >> "description" ).GetValue(), FORM_DEFAULT, *_player->GetWordBase() );
    _sentenceAtom = new SentenceAtom( know );
    _type = Required;
    
    ReadConditions( *_player->GetWordBase(), branchClass, 0, _conditions );
    ReadCandidates( branchClass, 0, _candidates );
    ReadCandidatesTables( branchClass, _candidatesTables );

    ReadGeneratorNames( branchClass );
  }

  //! Read names of generators from ParamFile.
  inline void ReadGeneratorNames( const ParamEntry &branchClass )
  {
    ConstParamEntryPtr rules = branchClass.FindEntry( "rules" );
    if( rules && rules->IsArray() )
    {
      GeneratorInfo gi;
      int n = rules->GetSize();
      for( int i = 0; i < n; ++i )
      {
        const IParamArrayValue &rule = ( *rules )[ i ];
        if( !rule.IsArrayValue() )
        {
          // It is not an array - it has default form.
          gi._isScalar = rule.IsFloatValue() || rule.IsIntValue();
          if( !gi._isScalar )
          {
            // Non scalar expression.
            gi._name = rule.GetValue();
            gi._form = FORM_DEFAULT;
          }
          else
          {
            // Scalar expression.
            gi._name = NULL;
            gi._scalar = rule.GetFloat();
            gi._form = FORM_DEFAULT;
          }
        }
        else
        {
          // It is an array - form is defined.
          const IParamArrayValue &ruleValue = rule[ 0 ];
          gi._isScalar = ruleValue.IsFloatValue() || ruleValue.IsIntValue();
          if( !gi._isScalar )
          {
            // Not a scalar.
            gi._name = ruleValue.GetValue();
            gi._form = rule[ 1 ].GetInt();
          }
          else
          {
            // A scalar expression.
            gi._name = NULL;
            gi._scalar = ruleValue.GetFloat();
            gi._form = rule[ 1 ].GetInt();
          }
        }
        _generators.Add( gi );
      }
    }
    rules = branchClass.FindEntry( "generateRules" );
    if( rules )
    {
      _generateRules = rules->GetValue();
    }
  }

  //! Read conditions of the result from ParamFile.
  inline static void ReadConditions( const WordBase &wb, const ParamEntry &parent, int defIndex, AutoArray< SentenceResultCondition > &conds )
  {
    // Find subclass 'conditions'.
    ConstParamEntryPtr entry = parent.FindEntry( "conditions" );
    if( entry && entry->IsClass() )
    {
      // Each subclass of 'conditions' is a condition.
      int n = entry->GetEntryCount();
      for( int i = 0; i < n; ++i )
      {
      	ParamEntryVal conditionVal = entry->GetEntry( i );
      	if( conditionVal.IsClass() )
      	{
          SentenceResultCondition cond;
          
          // It is an original.
          cond._original = true;

          // Index of the sentence sub element.
          cond._index = defIndex;

          // Read script expression of the 'constant' of the condition.
          ConstParamEntryPtr constEntry = conditionVal.FindEntry( "constant" );
          if( constEntry )
          {
            cond._constant = constEntry->GetValue();
          }

          ConstParamEntryPtr condEntry = conditionVal.FindEntry( "condition" );
          ConstParamEntryPtr subEntry = conditionVal.FindEntry( "priority" );
          if( subEntry )
          {
            cond._priority = static_cast< float >( *subEntry );
          }
          else
          {
            if( condEntry || constEntry )
            {
              cond._priority = 1.0f;
            }
            else
            {
              // Conditions used to display information have zero priority.
              cond._priority = 0.0f;
            }
          }

          // Read script expression of the 'variable' of the condition.
          subEntry = conditionVal.FindEntry( "variable" );
          if( subEntry )
          {
            cond._variable = subEntry->GetValue();
          }
          
          // Read name of the variable for conditional results.
          subEntry = conditionVal.FindEntry( "variableName" );
          if( subEntry )
          {
            cond._variableName = subEntry->GetValue();
          }

          // Read script expression of the condition.
          if( condEntry )
          {
            cond._condition = condEntry->GetValue();
          }

          // Read script expression of the 'metrics' of the condition.
          subEntry = conditionVal.FindEntry( "metrics" );
          if( subEntry )
          {
            cond._metrics = subEntry->GetValue();
          }

          // Read script expression of the 'predecessor' of the condition.
          subEntry = conditionVal.FindEntry( "predecessor" );
          if( subEntry )
          {
            cond._predecessor = subEntry->GetValue();
          }
          
          // Read required form of the evaluated 'variable' to written in reply.
          subEntry = conditionVal.FindEntry( "expression" );
          if( subEntry )
          {
            cond._expression = subEntry->GetValue();
          }

          // Read aggregation style of this condition metrics.
          subEntry = conditionVal.FindEntry( "aggregation" );
          if( subEntry )
          {
            cond._aggregation = subEntry->GetInt();
          }
          else
          {
            cond._aggregation = AGG_DEFAULT;
          }
          
          // Read aggregation threshold of this condition metrics.
          subEntry = conditionVal.FindEntry( "aggregationThreshold" );
          if( subEntry )
          {
            cond._aggregationThreshold = static_cast< float >( *subEntry );
          }
          else
          {
            cond._aggregationThreshold = 0.5f;
          }
          
          // Read 'required' flag of this condition.
          subEntry = conditionVal.FindEntry( "required" );
          if( subEntry )
          {
            cond._required = static_cast< bool >( *subEntry );
          }
          else
          {
            cond._required = false;
          }
          
          subEntry = conditionVal.FindEntry( "format" );
          if( subEntry )
          {
            cond._formatId = static_cast< int >( *subEntry );
          }
          else
          {
            cond._formatId = FORMAT_LIST;
          }

          subEntry = conditionVal.FindEntry( "subject" );
          if( subEntry )
          {
            cond._subject = static_cast< bool >( *subEntry );
          }
          else
          {
            cond._subject = false;
          }
          
          subEntry = conditionVal.FindEntry( "partOfSentence" );
          if( subEntry )
          {
            cond._partOfSentence = static_cast< PartOfSentence >( subEntry->GetInt() );
          }
          else
          {
            cond._partOfSentence = POS_Object;
          }

          subEntry = conditionVal.FindEntry( "transfer" );
          if( subEntry )
          {
            cond._transfer = subEntry->GetValue();
          }

          subEntry = conditionVal.FindEntry( "resultContextSubject" );
          if( subEntry )
          {
            cond._contextSubject = wb.ConvertNameToId( subEntry->GetValue() );
          }
          else
          {
            cond._contextSubject = -1;
          }
          
          subEntry = conditionVal.FindEntry( "valueType" );
          if( subEntry )
          {
            cond._valueType = subEntry->GetInt();
          }
          else
          {
            cond._valueType = VT_UNKNOWN;
          }

          conds.Add( cond );
      	}
      }
    }
  }

  //! Read script expression to get candidates set.
  inline static void ReadCandidates( const ParamEntry &parent, int defIndex, AutoArray< SentenceResultCandidates > &cands )
  {
    ConstParamEntryPtr entry = parent.FindEntry( "candidates" );
    if( entry )
    {
      SentenceResultCandidates cand;
      cand._original = true;
      cand._candidates = entry->GetValue();
      cand._index = defIndex;
      cands.Add( cand );
    }
  }

  //! Read the table name of which candidates are part.
  inline static void ReadCandidatesTables( const ParamEntry &parent, AutoArray< SentenceResultCandidatesTable > &candsTables )
  {
    ConstParamEntryPtr entry = parent.FindEntry( "candidatesFromTable" );
    if( entry )
    {
      SentenceResultCandidatesTable table;
      table._original = true;
      table._candidatesFromTable = entry->GetValue();
      candsTables.Add( table );
    }
  }

  //! Read results description from param class.
  inline static void ReadResults( const ParamEntry &parent, RefArray< FormattedSentenceResult > &results )
  {
    ConstParamEntryPtr entry = parent.FindEntry("result");
    if (entry && entry->IsClass())
    {
      int n = entry->GetEntryCount();
      for( int i = 0; i < n; ++i )
      {
      	ParamEntryVal r = entry->GetEntry( i );
      	if( r.IsClass() )
      	{
          float threshold = -1.0f;
          ConstParamEntryPtr entry = r.FindEntry("threshold");
          if( entry )
          {
            threshold = *entry;
          }

          RString responseId;
          entry = r.FindEntry("sentence");
          if( entry )
          {
            responseId = *entry;
          }

          RString patternId;
          entry = r.FindEntry("pattern");
          if( entry )
          {
            patternId = *entry;
          }
          
          RString condition;
          entry = r.FindEntry( "condition" );
          if( entry )
          {
            condition = *entry;
          }

          RString action;
          entry = r.FindEntry( "action" );
          if( entry )
          {
            action = *entry;
          }

      	  Ref<FormattedSentenceResult> newRes = new FormattedSentenceResult( threshold, responseId, 
      	    patternId, condition, action );
      	  results.Add( newRes );
      	}
      }
    }
  }
  
  inline static void ReadVisibleTests( const ParamEntry &parent, AutoArray< RuleVisibleTest > &tests )
  {
    ConstParamEntryPtr entry = parent.FindEntry( "visibleTest" );
    if( entry )
    {
      tests.Add( RuleVisibleTest ( entry->GetValue() ) );
    }
  }
  
  virtual bool RulesNotEmpty();

  // Creates copy; doesn't have to create new SentenceAtom for this copy.
  virtual void Copy( Ref< SentenceFragment > &dst ) const
  {
    dst = new SentenceFragmentDefault( *this );
  }
  
  //! Reads rules of one generator from ParamFile.
  static void ReadGeneratorRules( const KnowledgeBase *player, const ParamEntry &paramFile, 
    const ParamEntry &generator, RefArray< SentenceRule > &rules )
  {
    AssertDebug( !generator.IsError() );
    ParamEntryVal phraseParts = generator >> "phraseParts";
    if( phraseParts.GetPointer()->IsError() || !phraseParts.IsArray() )
    {
      ReadTerminatorGeneratorRules( player, generator, rules );
    }
    else
    {
      ReadBranchGeneratorRule( player, paramFile, generator, phraseParts, rules );
    }
  }

protected:
  //! Reads rules from ParamFile if this fragment has any generator.
  virtual void ReadRules( RefArray< SentenceRule > &rules ) const;
  
  //! Reads rules of one branch generator from ParamFile.
  static void ReadBranchGeneratorRule( const KnowledgeBase *player, const ParamEntry &paramFile, 
    const ParamEntry &generator, const ParamEntry &phraseParts, RefArray< SentenceRule > &rules )
  {
    Ref< SentenceRule > elem = new SentenceRule();
    AssertDebug( elem.NotNull() );
    GeneratorInfo gi;
    int m = phraseParts.GetSize();
    for( int j = 0; j < m; ++j )
    {
      const IParamArrayValue &phraseElem = phraseParts[ j ];
      const IParamArrayValue *part = phraseElem.GetItem( 0 );
      AssertDebug( part != NULL );
      if( part != NULL )
      {
        gi._isScalar = part->IsFloatValue() || part->IsIntValue();
        if( !gi._isScalar )
        {
          gi._name = part->GetValue();

          WordBaseId wordId;
          if( ( wordId = player->GetWordBase()->ConvertNameToId( gi._name ) ) != -1 )
          {
            // Phrase part - terminator (WordBaseId)
            gi._form = phraseElem[ 1 ].GetInt();
            Ref< SemanticKnowledge > know = new SemanticKnowledge( wordId, gi._form );
            Ref< SentenceAtom > atom = new SentenceAtom( know );
            Ref< SentenceFragmentDefault > newFrag = new SentenceFragmentDefault( player, atom );
            AssertDebug( newFrag.NotNull() );
            elem->AddSubfragment( newFrag );
          }
          else
          {
            // Phrase part - branch (common or generating procedural rules)
            Ref< SentenceFragmentDefault > newFrag = new SentenceFragmentDefault( player, 
              paramFile, generator.GetName(), j );
            AssertDebug( newFrag.NotNull() );
            if( newFrag->GetType() != NotRequired || newFrag->RulesNotEmpty() )
            {
              elem->AddSubfragment( newFrag );
            }
          }
        }
        else
        {
          gi._name = NULL;
          gi._scalar = part->GetFloat();
          gi._form = phraseElem[ 1 ].GetInt();
          
          // Phrase part - terminator (scalar)
          Ref< SemanticKnowledge > know = new SemanticKnowledge( player->GetWordBase()->_wordIdScalarId, 
            gi._scalar, gi._form );
          Ref< SentenceAtom > atom = new SentenceAtom( know );
          Ref< SentenceFragmentDefault > newFrag = new SentenceFragmentDefault( player, atom );
          AssertDebug( newFrag.NotNull() );
          elem->AddSubfragment( newFrag );
        }
      }
    }
    ConstParamEntryPtr entry = generator.FindEntry("mood");
    if (entry)
    {
      elem->SetMood(entry->GetInt());
    }
    // Read conditions written in rule.
    ReadConditions( *player->GetWordBase(), generator, -1, elem->GetModConditions() );
    ReadCandidates( generator, -1, elem->GetModCandidates() );
    ReadCandidatesTables( generator, elem->GetModCandidatesTables() );
    ReadResults( generator, elem->GetModResults() );
    ReadVisibleTests( generator, elem->GetModVisibleTests() );
    rules.Add( elem );
  }

  //! Reads rules of one terminator generator from ParamFile.
  static void ReadTerminatorGeneratorRules( const KnowledgeBase *player, const ParamEntry &generator, RefArray< SentenceRule > &rules )
  {
    ConstParamEntryPtr entry = generator.FindEntry( "wordid" );
    if( entry )
    {
      Ref< SemanticKnowledge > know = new SemanticKnowledge(
        player->GetWordBase()->ConvertNameToId( entry->GetValue() ), FORM_DEFAULT );
      Ref< SentenceAtom > atom = new SentenceAtom( know );
      Ref< SentenceFragmentDefault > frag = 
        new SentenceFragmentDefault( player, atom );
      Ref< SentenceRule > rule = new SentenceRule();
      rule->AddSubfragment( frag );
      rules.Add( rule );
      return;
    }

    entry = generator.FindEntry( "description" );
    if( entry )
    {
      Ref< SemanticKnowledge > know = new SemanticKnowledge(
        player->GetWordBase()->_wordIdCustomId, entry->GetValue(), FORM_DEFAULT,
        *player->GetWordBase() );
      Ref< SentenceAtom > atom = new SentenceAtom( know );
      Ref< SentenceFragmentDefault > frag = 
        new SentenceFragmentDefault( player, atom );
      Ref< SentenceRule > rule = new SentenceRule();
      rule->AddSubfragment( frag );
      rules.Add( rule );
      return;
    }
  }
};

#endif