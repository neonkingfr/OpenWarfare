
#ifndef _PLACEAREA_HPP_
#define _PLACEAREA_HPP_

#include "El/elementpch.hpp"
#include "El/Evaluator/express.hpp"
#include "El/Math/math3d.hpp"

//! Struct describing area.
struct PlaceArea
{
  Vector3 _center;
  float _maxDist;

  int _size;
  
  float _deviation;

  /*! Average quadratic deviation coefficients.
      Average quadratic deviation can be obtained as _kx1 - 2 * _center.x * _kx2 + _center.x * _center.x
  */
  float _kx1, _kx2;
  float _ky1, _ky2;
  float _kz1, _kz2;

  inline PlaceArea()
    : _maxDist( 0.0f ), _size( 0 ), _kx1( 0.0f ), _kx2( 0.0f ), _ky1( 0.0f ), _ky2( 0.0f ), 
    _kz1( 0.0f ), _kz2( 0.0f ), _deviation( 0.0f )
  {
  }

  inline PlaceArea( const Vector3 &center )
    : _center( center ), _maxDist( 0.0f ), _size( 1 ), _kx1( center.X() * center.X() ), _kx2( center.X() ), 
    _ky1( center.Y() * center.Y() ), _ky2( center.Y() ), _kz1( center.Z() * center.Z() ), _kz2( center.Z() ), 
    _deviation( 0.0f )
  {
  }

  inline PlaceArea( Coord x, Coord y, Coord z )
    : _center( x, y, z ), _maxDist( 0.0f ), _size( 1 ), _kx1( x * x ), _kx2( x ), 
    _ky1( y * y ), _ky2( y ), _kz1( z * z ), _kz2( z ), _deviation( 0.0f )
  {
  }

  inline PlaceArea( const PlaceArea &src )
    : _center( src._center ), _maxDist( src._maxDist ), _size( src._size ), _kx1( src._kx1 ), _kx2( src._kx2 ), 
    _ky1( src._ky1 ), _ky2( src._ky2 ), _kz1( src._kz1 ), _kz2( src._kz2 ), _deviation( src._deviation )
  {
  }

  //! Compare two areas.
  inline bool operator ==( const PlaceArea &src ) const
  {
    return _center == src._center && _maxDist == src._maxDist && 
      _size == src._size && _kx1 == src._kx1 && _kx2 == src._kx2 &&
      _ky1 == src._ky1 && _ky2 == src._ky2 && _kz1 == src._kz1 && _kz2 == src._kz2;
  }

  //! Compare two areas.
  inline bool operator !=( const PlaceArea &src ) const
  {
    return !operator ==( src );
  }

  // Calculate union of two areas.
  inline void Add( const PlaceArea &src )
  {
    Vector3 tmpCenter( _center );
    int size = _size;

    // Calculate the number of elements.
    _size += src._size;
    // Calculate the position.
    float rate1 = ( float ) size / ( float ) ( _size );
    float rate2 = ( float ) src._size / ( float ) ( _size );
    _center[ 0 ] = rate1 * tmpCenter.X() + rate2 * src._center.X();
    _center[ 1 ] = rate1 * tmpCenter.Y() + rate2 * src._center.Y();
    _center[ 2 ] = rate1 * tmpCenter.Z() + rate2 * src._center.Z();
    // Calculate the maximum distance.
    float leftDist = _center.Distance( tmpCenter );
    float rightDist = _center.Distance( src._center );
    float tmpLeft = _maxDist + leftDist;
    float tmpRight = src._maxDist + rightDist;
    _maxDist = tmpLeft >= tmpRight ? tmpLeft : tmpRight;
    // Calculate the average quadratic deviation.
    _kx1 = rate1 * _kx1 + rate2 * src._kx1;
    _kx2 = rate1 * _kx2 + rate2 * src._kx2;
    _ky1 = rate1 * _ky1 + rate2 * src._ky1;
    _ky2 = rate1 * _ky2 + rate2 * src._ky2;
    _kz1 = rate1 * _kz1 + rate2 * src._kz1;
    _kz2 = rate1 * _kz2 + rate2 * src._kz2;

    float deviation2x = _kx1 - 2 * _center.X() * _kx2 + _center.X() * _center.X();
    float deviation2y = _ky1 - 2 * _center.Y() * _ky2 + _center.Y() * _center.Y();
    float deviation2z = _kz1 - 2 * _center.Z() * _kz2 + _center.Z() * _center.Z();
    _deviation = ::sqrt( deviation2x + deviation2y + deviation2z );
  }
};
TypeIsSimpleZeroed( PlaceArea );

//! This is area's representation and area's type in script language.
const GameType GameArea( 0x100000 );

class GameDataArea : public GameData
{
  typedef GameData base;

  PlaceArea _value;

public:
  GameDataArea()
  {
  }

  GameDataArea( const PlaceArea &value )
    : _value( value )
  {
  }

  ~GameDataArea()
  {
  }

  GameType GetType() const
  {
    return GameArea;
  }

  const PlaceArea &GetArea() const 
  {
    return _value;
  }

  RString GetText() const;

  bool IsEqualTo( const GameData *data ) const;

  const char *GetTypeName() const 
  {
    return "area";
  }

  GameData *Clone() const 
  {
    return new GameDataArea( *this );
  }

  LSError Serialize( ParamArchive &ar );

  USE_FAST_ALLOCATOR
};

static inline const PlaceArea *GetArea( GameValuePar oper )
{
  return ( oper.GetType() == GameArea ) ? &static_cast< GameDataArea* >( oper.GetData() )->GetArea() : NULL;
}

GameData *CreateGameDataArea();

#endif
