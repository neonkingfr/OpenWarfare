
#ifndef _SEMANTICNETBASE_HPP
#define _SEMANTICNETBASE_HPP

#include "El/elementpch.hpp"
#include "El/Evaluator/express.hpp"
#include "El/ParamFile/paramFile.hpp"
#include "Es/essencepch.hpp"
#include "Es/Containers/array2D.hpp"
#include "Es/Containers/staticArray.hpp"

#include "SemanticNet.hpp"

//! Independent pointer into semantic net defined by table name and knowledge index.
class SemanticQuery
{
protected:
  RStringB _table;
  int _index;

public:
  inline SemanticQuery()
    : _index( -1 )
  {
  }

  inline SemanticQuery( const RStringB &table )
    : _table( table ), _index( 0 )
  {
  }

  inline SemanticQuery( const RStringB &table, int index )
    : _table( table ), _index( index )
  {
  }

  inline SemanticQuery( const SemanticQuery &src )
  {
    operator =( src );
  }

  //! Compare two knowledge pointers.
  inline bool operator ==( const SemanticQuery &src ) const
  {
    return _table == src._table && _index == src._index;
  }

  //! Compare two knowledge pointers.
  inline bool operator !=( const SemanticQuery &src ) const
  {
    return _table != src._table || _index != src._index;
  }

  inline void operator =( const SemanticQuery &src )
  {
    _table = src._table;
    _index = src._index;
  }

  inline const RStringB GetTable() const
  {
    return _table;
  }

  inline void SetTable( const RStringB &tableName )
  {
    _table = tableName;
  }

  inline int GetIndex() const
  {
    return _index;
  }

  inline void SetIndex( int index )
  {
    _index = index;
  }

  inline void IncIndex()
  {
    ++_index;
  }

  inline bool IsValid() const
  {
    return _index != -1;
  }
};
TypeIsMovableZeroed( SemanticQuery );

//! Connects set of queries (candidates) with two views - incoming and outcoming knowledges.
class SemanticContextElement
{
public:
  //! Outcoming knowledge (what I say).
  Ref< const SemanticKnowledge > _outcoming;

  /*! All the pointers to the candidates in semantic net that can be represented by the incoming 
  and outcoming knowledges.
  */
  FindArray< SemanticQuery > _queries;

public:
  inline SemanticContextElement()
  {
  }

  inline SemanticContextElement( const SemanticKnowledge *outcoming, 
    const RString &table, int index )
    : _outcoming( outcoming )
  {
    _queries.Add( SemanticQuery( table, index ) );
  }

  inline SemanticContextElement( const SemanticKnowledge *outcoming, 
    const SemanticQuery &query )
    : _outcoming( outcoming )
  {
    _queries.Add( query );
  }

  inline SemanticContextElement( const SemanticKnowledge *outcoming, 
    const AutoArray< SemanticQuery > &queries )
    : _outcoming( outcoming )
  {
    _queries.Copy( queries.Data(), queries.Size() );
  }

  inline const SemanticKnowledge *GetOutcoming() const
  {
    return _outcoming;
  }

  inline void SetOutcoming( const SemanticKnowledge *outcoming )
  {
    _outcoming = outcoming;
  }

  inline const AutoArray< SemanticQuery > &GetQueries() const
  {
    return _queries;
  }
  
  inline int GetQueriesSize() const
  {
    return _queries.Size();
  }
  
  inline const SemanticQuery &GetQuery( int i ) const
  {
    return _queries[ i ];
  }

  inline void SetQueries( const SemanticQuery &src )
  {
    _queries.Clear();
    _queries.Add( src );
  }

  inline void SetQueries( const AutoArray< SemanticQuery > &src )
  {
    _queries.Copy( src.Data(), src.Size() );
  }

  inline void AddQueries( const SemanticQuery &src )
  {
    _queries.AddUnique( src );
  }

  inline void AddQueries( const AutoArray< SemanticQuery > &src )
  {
    int srcN = src.Size();
    _queries.Realloc( _queries.Size() + srcN );
    for( int srcI = 0; srcI < srcN; ++srcI )
    {
    	_queries.AddUnique( src[ srcI ] );
    }
  }

  //! Compare queries (candidates).
  inline bool operator ==( const SemanticQuery &src ) const
  {
    return _queries.Size() == 1 && _queries[ 0 ] == src;
  }

  //! Compare queries (candidates).
  inline bool operator !=( const SemanticQuery &src ) const
  {
    return !operator ==( src );
  }

  //! Compare queries (candidates).
  inline bool operator ==( const AutoArray< SemanticQuery > &src ) const
  {
    int n = _queries.Size();
    if( n != src.Size() )
    {
      return false;
    }

    StaticArray< int > idxs;
    idxs.Resize( n );
    for( int i = 0; i < n; ++i )
    {
    	idxs[ i ] = i;
    }
    int m = idxs.Size();
    for( int i = 0; i < n; ++i )
    {
      for( int j = 0;; )
      {
        if( j < m )
        {
          if( _queries[ idxs[ j ] ] == src[ i ] )
          {
            idxs.Delete( j );
            --m;
            break;
          }
          ++j;
          continue;
        }
        return false;
      }
    }
    return true;
  }

  //! Compare queries (candidates).
  inline bool operator !=( const AutoArray< SemanticQuery > &src ) const
  {
    return !operator ==( src );
  }
};
TypeIsMovable( SemanticContextElement );

/*! Requires SemanticNet::QueryType and SemanticNet::KnowledgeType to have '==' and '=' operator.
*/
class SemanticContext : public RefCount
{
protected:
  AutoArray< SemanticContextElement > _notions;

public:
  inline void Clear()
  {
    _notions.Clear();
  }
  
  inline int GetNotionsSize() const
  {
    return _notions.Size();
  }
  
  inline const SemanticContextElement &GetNotion( int i ) const
  {
    return _notions[ i ];
  }

  //! Returns true if outcoming token is defied in this context.
  bool IsOutcomingDefined( WordBaseId outcoming ) const
  {
    int n = _notions.Size();
    int i = 0;
    while( i < n )
    {
      const SemanticContextElement &e = _notions[ i ];
      if( outcoming != e.GetOutcoming()->GetWordBaseId() )
      {
        ++i;
        continue;
      }
      return true;
    }
    return false;
  }

  //! Returns true if outcoming token is defied in this context.
  bool IsOutcomingDefined( const SemanticKnowledge &outcoming, const WordBase &wordBase ) const
  {
    int n = _notions.Size();
    int i = 0;
    while( i < n )
    {
      const SemanticContextElement &e = _notions[ i ];
      if( !( outcoming.IsEqual( *( e.GetOutcoming() ), wordBase ) ) )
      {
        ++i;
        continue;
      }
      return true;
    }
    return false;
  }

  //! Transform incoming knowledge to query using this context.
  inline bool TransformIncomingToQueries( WordBaseId incoming, 
    AutoArray< SemanticQuery > &queries, const WordBase &wordBase ) const
  {
    AutoArray< WordBaseId > outcoming;
    wordBase.SwitchIncomingAndOutcoming( incoming, outcoming );
    bool result = false;
    int n = outcoming.Size();
    for( int i = 0; i < n; ++i )
    {
      result |= TransformOutcomingToQueries( outcoming[ i ], queries );
    }
    return result;
  }

  //! Transform outcoming knowledge to query using this context.
  bool TransformOutcomingToQueries( WordBaseId outcoming, 
    AutoArray< SemanticQuery > &queries ) const
  {
    int n = _notions.Size();
    int i = 0;
    while( i < n )
    {
      const SemanticContextElement &e = _notions[ i ];
      if( outcoming != e.GetOutcoming()->GetWordBaseId() )
      {
        ++i;
        continue;
      }
      queries = e.GetQueries();
      return true;
    }
    return false;
  }

  //! Transform incoming knowledge to query using this context.
  inline bool TransformIncomingToQueries( const SemanticKnowledge &incoming, 
    AutoArray< SemanticQuery > &queries, const WordBase &wordBase ) const
  {
    AutoArray< WordBaseId > outcomingPronouns;
    wordBase.SwitchIncomingAndOutcoming( incoming.GetWordBaseId(), outcomingPronouns );
    bool result = false;
    int n = outcomingPronouns.Size();
    for( int i = 0; i < n; ++i )
    {
      SemanticKnowledge outcoming( outcomingPronouns[ i ], incoming.GetForm() );
    	result |= TransformOutcomingToQueries( outcoming, queries, wordBase );
    }
    return result;
  }

  //! Transform outcoming knowledge to query using this context.
  bool TransformOutcomingToQueries( const SemanticKnowledge &outcoming, 
    AutoArray< SemanticQuery > &queries, const WordBase &wordBase ) const
  {
    int n = _notions.Size();
    int i = 0;
    while( i < n )
    {
      const SemanticContextElement &e = _notions[ i ];
      if( !( outcoming.IsEqual( *( e.GetOutcoming() ), wordBase ) ) )
      {
        ++i;
        continue;
      }
      queries = e.GetQueries();
      return true;
    }
    return false;
  }

  //! Transform query to outcoming representation using this context.
  template< typename QueriesType >
  void TransformQueriesToOutcoming( const QueriesType &query,
    Ref< SemanticKnowledge > &outcoming ) const
  {
    int n = _notions.Size();
    int i = 0;
    while( i < n )
    {
      const SemanticContextElement &e = _notions[ i ];
      if( e != query )
      {
        ++i;
        continue;
      }
      outcoming = new SemanticKnowledge( *e.GetOutcoming() );
      return;
    }
  }

  //! Transform query to outcoming representation using this context.
  template< typename QueriesType >
  void TransformQueriesToOutcoming( const QueriesType &query, const WordBase &wb, long requiredForm,
    Ref< SemanticKnowledge > &outcoming ) const
  {
    int n = _notions.Size();
    int i = 0;
    while( i < n )
    {
      const SemanticContextElement &e = _notions[ i ];
      if( e != query )
      {
        ++i;
        continue;
      }
      const SemanticKnowledge *outCandidate = e.GetOutcoming();
      if( outCandidate->CanHaveForm( wb, requiredForm ) )
      {
        outcoming = new SemanticKnowledge( *outCandidate );
        outcoming->SetForm( requiredForm );
        return;
      }
      ++i;
    }
  }

  //! Be sure to not pass 'query' allocated on the stack!
  template< typename QueriesType >
  inline void SetWithHypernyms( const SemanticKnowledge *outcoming, const QueriesType &query, 
    const WordBase &wordBase )
  {
    RefArray< const SemanticKnowledge > hypernyms;
    outcoming->GetHypernyms( wordBase, hypernyms );
    Set( hypernyms, query, wordBase );
  }

  template< typename QueriesType >
  inline void Set( const RefArray< const SemanticKnowledge > &outcomings, const QueriesType &query, 
    const WordBase &wordBase )
  {
    Clear( outcomings, wordBase );
    Add( outcomings, query, wordBase );
  }

  inline void Clear( const RefArray< const SemanticKnowledge > &outcomings, const WordBase &wordBase )
  {
    int outcomingsN = outcomings.Size();
    for( int outcomingsI = 0; outcomingsI < outcomingsN; ++outcomingsI )
    {
    	Clear( outcomings[ outcomingsI ], wordBase );
    }
  }
  
  inline void Clear( const SemanticKnowledge *outcoming, const WordBase &wordBase )
  {
    int notionsN = _notions.Size();
    int notionsI = 0;
    while( notionsI < notionsN )
    {
      SemanticContextElement &e = _notions[ notionsI ];
      if( !( outcoming->IsEqual( *( e.GetOutcoming() ), wordBase ) ) )
      {
        ++notionsI;
        continue;
      }
      _notions.Delete( notionsI );
      return;
    }
  }

  void ClearWithHypernyms( const SemanticKnowledge *outcoming, const WordBase &wordBase )
  {
    RefArray< const SemanticKnowledge > hypernyms;
    outcoming->GetHypernyms( wordBase, hypernyms );
    Clear( hypernyms, wordBase );
  }
  
  void ClearWithHypernyms( const RefArray< const SemanticKnowledge > &outcomings, const WordBase &wordBase )
  {
    int outcomingsN = outcomings.Size();
    if( outcomingsN != 1 )
    {
      FindArrayKeyWithParam< Ref< const SemanticKnowledge >, const WordBase &, 
        RefArrayKeyWithParamTraits< Ref< const SemanticKnowledge >, const WordBase & > > allHypernyms;
      RefArray< const SemanticKnowledge > hypernyms;
      int outcomingsN = outcomings.Size();
      for( int outcomingsI = 0; outcomingsI < outcomingsN; ++outcomingsI )
      {
        outcomings[ outcomingsI ]->GetHypernyms( wordBase, hypernyms );
        int hypernymsN = hypernyms.Size();
        for( int hypernymsI = 0; hypernymsI < hypernymsN; ++hypernymsI )
        {
          allHypernyms.AddUnique( hypernyms[ hypernymsI ], wordBase );
        }
      }

      int allHypernymsN = allHypernyms.Size();
      for( int allHypernymsI = 0; allHypernymsI < allHypernymsN; ++allHypernymsI )
      {
        Clear( allHypernyms[ allHypernymsI ], wordBase );
      }
    }
    else
    {
      ClearWithHypernyms( outcomings[ 0 ], wordBase );
    }
  }

  template< typename QueriesType >
  inline void Add( const RefArray< const SemanticKnowledge > &outcomings, const QueriesType &query, 
    const WordBase &wordBase )
  {
    int outcomingsN = outcomings.Size();
    for( int outcomingsI = 0; outcomingsI < outcomingsN; ++outcomingsI )
    {
    	Add( outcomings[ outcomingsI ], query, wordBase );
    }
  }
  
  template< typename QueriesType >
  void Add( const SemanticKnowledge *outcoming, const QueriesType &query, 
    const WordBase &wordBase )
  {
    int notionsN = _notions.Size();
    int notionsI = 0;
    while( notionsI < notionsN )
    {
      SemanticContextElement &e = _notions[ notionsI ];
      if( !( outcoming->IsEqual( *( e.GetOutcoming() ), wordBase ) ) )
      {
        ++notionsI;
        continue;
      }
      e.AddQueries( query );
      return;
    }

    SemanticContextElement ne( outcoming, query );
    _notions.Add( ne );
  }

  template< typename QueriesType >
  void AddWithHypernyms( const SemanticKnowledge *outcoming, const QueriesType &query, 
    const WordBase &wordBase )
  {
    RefArray< const SemanticKnowledge > hypernyms;
    outcoming->GetHypernyms( wordBase, hypernyms );
    Add( hypernyms, query, wordBase );
  }
  
  template< typename QueriesType >
  void SetWithHypernyms( const RefArray< const SemanticKnowledge > &outcomings, const QueriesType &query,
    const WordBase &wordBase )
  {
    int outcomingsN = outcomings.Size();
    if( outcomingsN != 1 )
    {
      FindArrayKeyWithParam< Ref< const SemanticKnowledge >, const WordBase &, 
        RefArrayKeyWithParamTraits< Ref< const SemanticKnowledge >, const WordBase & > > allHypernyms;
      RefArray< const SemanticKnowledge > hypernyms;
      int outcomingsN = outcomings.Size();
      for( int outcomingsI = 0; outcomingsI < outcomingsN; ++outcomingsI )
      {
        outcomings[ outcomingsI ]->GetHypernyms( wordBase, hypernyms );
        int hypernymsN = hypernyms.Size();
        for( int hypernymsI = 0; hypernymsI < hypernymsN; ++hypernymsI )
        {
          allHypernyms.AddUnique( hypernyms[ i ], wordBase );
        }
      }

      Clear( allHypernyms, wordBase );
      Add( allHypernyms, query, wordBase );
    }
    else
    {
      SetWithHypernyms( outcomings[ 0 ], query, wordBase );
    }
  }
};

//! This is candidate's representation and candidate's type in script language.
const GameType GameQuery( 0x10000 );

class GameDataQuery : public GameData
{
  typedef GameData base;

  SemanticQuery _query;

public:
  inline GameDataQuery()
  {
  }

  GameDataQuery( const SemanticQuery &query )
    : _query( query )
  {
  }

  ~GameDataQuery()
  {
  }

  GameType GetType() const
  {
    return GameQuery;
  }

  const SemanticQuery &GetQuery() const
  {
    return _query;
  }

  RString GetText() const;

  bool IsEqualTo( const GameData *data ) const;

  const char *GetTypeName() const
  {
    return "query";
  }

  GameData *Clone() const
  {
    return new GameDataQuery( *this );
  }

  LSError Serialize( ParamArchive &ar );

  USE_FAST_ALLOCATOR
};

static inline const SemanticQuery *GetQuery( GameValuePar oper )
{
  return ( oper.GetType() == GameQuery ) ? &static_cast< GameDataQuery* >( oper.GetData() )->GetQuery() : NULL;
}

//! Index of the successful candidate and its group binary code.
struct IndexedGroupedResult
{
  //! Candidate's index.
  int _index;

  //! Group binary code.
  unsigned long _conditionBits;
  
  //! Number of the subgroup for given '_conditionBits' group.
  unsigned int _subgroupNumber;

  //! Compare group codes.
  static int CompareGroup( const IndexedGroupedResult *c1, const IndexedGroupedResult *c2 )
  {
    if( c1->_conditionBits < c2->_conditionBits )
    {
      return -1;
    }
    else if( c1->_conditionBits > c2->_conditionBits )
    {
      return 1;
    }
    return 0;
  }

  //! Compare group codes.
  static int CompareGroupAndSubgroup( const IndexedGroupedResult *c1, const IndexedGroupedResult *c2 )
  {
    if( c1->_conditionBits < c2->_conditionBits )
    {
      return -1;
    }
    else if( c1->_conditionBits > c2->_conditionBits )
    {
      return 1;
    }
    else if( c1->_subgroupNumber < c2->_subgroupNumber )
    {
      return -1;
    }
    else if( c1->_subgroupNumber > c2->_subgroupNumber )
    {
      return 1;
    }
    return 0;
  }
};
TypeIsSimple( IndexedGroupedResult );

//! Group of candidates indices.
typedef AutoArray< int > CandidatesIndicesGroup;
TypeIsSimple( CandidatesIndicesGroup* );

//! Struct of aggregated values and the number of original values.
struct MergedValuesGroup
{
  //! Aggregated value.
  GameValue _value;
  
  //! Group of candidates indices.
  CandidatesIndicesGroup _candidatesIndices;
};
TypeIsMovable( MergedValuesGroup )

//! Implementation of semantic net through ParamFile.
class SemanticNetBase : public SemanticNet
{
protected:
  static const RStringB _tablePlaces;
  static const RStringB _tableEntityTypes;
  static const RStringB _tableEntities;
  static const RStringB _keywordBase;
  static const RStringB _keywordValue;
  static const RStringB _keywordSingularValue;
  static const RStringB _keywordQuatity;
  static const RStringB _keywordPluralQuatity;
  static const RStringB _keywordRoundedPluralQuatity;
  static const RStringB _keywordBadValues;
  static const RStringB _keywordGoodValues;
  static const RStringB _keywordAllValues;
  static const RStringB _keywordCandidate;

  Ref< const WordBase > _wordBase;
  const ParamEntryVal _responses;
  SemanticContext _semanticContext;
  ConstParamEntryPtr _specRules;

public:
  SemanticNetBase( const WordBase *wordBase, const ParamEntryVal &responses, 
    const ConstParamEntryPtr &specRules )
    : _wordBase( wordBase ), _responses( responses ), _specRules( specRules )
  {
    AssertDebug( _wordBase.NotNull() );
  }

  //! Method required by every semantic net.
  inline void Get( ConstParamEntryPtr query, Ref< SemanticKnowledge > &result, const WordBase &wb ) const
  {
    if( query )
    {
      if( !query->IsFloatValue() && !query->IsIntValue() )
      {
        // The value is looked up in word base in the constructor of SemanticKnowledge.
        result = new SemanticKnowledge( _wordBase->_wordIdCustomId, query->GetValue(), FORM_DEFAULT, wb );
      }
      else
      {
        result = new SemanticKnowledge( _wordBase->_wordIdScalarId, query->operator float(), FORM_DEFAULT );
      }
    }
  }

  inline SemanticContext &GetModSemanticContext()
  {
    return _semanticContext;
  }

  inline const SemanticContext &GetSemanticContext() const
  {
    return _semanticContext;
  }

  //! 'participant' creates a reaction to the 'sentence'.
  virtual void React( KnowledgeBase *speaker, const SentenceFragment *sentence, 
    AutoArray< SentenceResultCondition > &complementaryConditions, 
    AutoArray< SentenceResultCondition > &complementarySubjects, Ref< SentenceAtom > &reaction,
    const RefArray< KnowledgeBase > &listeners );

  void ReactConditionally( KnowledgeBase *speaker, const SentenceFragment *sentence, 
    Ref< SentenceAtom > &reaction );

  void ReactConditionally( KnowledgeBase *speaker, const SentenceFragment *sentence, 
    Ref< SentenceAtom > &reaction, const AutoArray< SemanticQuery > &candidates, 
    const AutoArray< SentenceResultCondition > &conditions, 
    const Array2D< GameValue > &evaluatedVariables, const RefArray< KnowledgeBase > &listeners );

  virtual void InitConversationContext( const AutoArray< SemanticNetId > &youPronoun )
  {
    _semanticContext.Clear();
    SemanticQuery query;
    GetSelfQuery( query );
    _semanticContext.SetWithHypernyms( 
      new SemanticKnowledge( _wordBase->ConvertNameToId( "WordIdI" ), FORM_DEFAULT ), query, *_wordBase );
    
    int n = youPronoun.Size();
    if( n != 0 )
    {
      AutoArray< SemanticQuery > you;
      int i = 0;
      do {
        GetIdQuery( youPronoun[ i ], query );
        you.Add( query );
        ++i;
      } while( i < n );
      _semanticContext.SetWithHypernyms( 
        new SemanticKnowledge( _wordBase->ConvertNameToId( "WordIdYou" ), FORM_DEFAULT ), you, *_wordBase );
    }
  }

  virtual void InitSpeechContext()
  {
  }

  //! Is this notion defined in the context of this semantic net?
  virtual bool IsNotionDefined( WordBaseId notionId ) const;

  //! Return the number of notions defined in this semantic net (or its context).
  virtual int GetNotionsSize() const;

  //! Return the notion defined in this semantic net (or its context).
  virtual const SemanticKnowledge *GetNotion( int i ) const;

  //! Get sentence rules specific for this semantic net.
  virtual void GetSpecRules( const KnowledgeBase *player, RefArray< SentenceRule > &specRules ) const;

  //! Method to determine the number of knowledges in the current table.
  virtual int GetKnowledgesSize( const RStringB &tableName ) const = 0;

  //! Get pointer to the myself in my semantic net.
  virtual void GetSelfQuery( SemanticQuery &query ) const = 0;

  //! Get pointer to the object with ID in my semantic net.
  virtual bool GetIdQuery( const SemanticNetId id, SemanticQuery &query ) const = 0;
  
  //! Fills the array with enemy sides.
  virtual GameValue GetEnemySides() const = 0;

  //! Converts name of place into PlaceArea struct.
  virtual GameValue GetArea( const RString &areaName ) const = 0;

  // Get area of the candidate.
  virtual GameValue GetArea( const SemanticQuery &candidate ) const = 0;

  // Get distance between two areas.
  virtual GameValue GetDistance( GameValuePar area1, GameValuePar area2 ) const = 0;

  //! Calculates time current time minus given number of seconds.
  virtual GameValue GetTimeLastSecs( float secs ) const = 0;

  //! Finds side of the candidate in this semantic net.
  virtual GameValue GetSide( const SemanticQuery &candidate ) const = 0;

  //! Finds last seen time of the candidate in this semantic net.
  virtual GameValue GetLastSeenTime( const SemanticQuery &candidate ) const = 0;

  //! Finds candidate's name in this semantic net.
  virtual GameValue GetName( const SemanticQuery &candidate ) const = 0;

  //! Finds unit type of the candidate in this semantic net.
  virtual GameValue GetUnitType( const SemanticQuery &candidate ) const = 0;

  //! Are the sides equal from this semantic net view?
  virtual GameValue SideEqual( GameValuePar left, GameValuePar right ) const = 0;

  //! Are the times equal from this semantic net view?
  virtual GameValue TimeEqual( GameValuePar left, GameValuePar right ) const;

  //! Is the left time greater or equal than right time from this semantic net view?
  virtual GameValue TimeIntersected( GameValuePar left, GameValuePar right ) const;

  //! Are the names equal from this semantic net view?
  virtual GameValue NameEqual( const RString left, const RString right ) const;

  //! Are the unit types equal from this semantic net view?
  virtual GameValue UnitEqual( const RString left, const RString right ) const = 0;

  //! Is the first type subtype of the other?
  virtual GameValue UnitSubtype( const RString subtype, const RString type ) const = 0;

  //! Are the areas equal from the view of this semantic net?
  virtual GameValue PlaceEqual( GameValuePar left, GameValuePar right ) const;

  //! Are the speeds equal from the view of this semantic net?
  virtual GameValue SpeedSizeEqual( GameValuePar left, GameValuePar right ) const;

  //! Are the speeds equal from the view of this semantic net?
  virtual GameValue SpeedDirectionEqual( GameValuePar left, GameValuePar right ) const;

  //! Finds predecessor of sides in array.
  virtual GameValue SidePred( GameValuePar sides ) const = 0;

  //! Finds predecessor of times in array.
  virtual GameValue TimePred( GameValuePar times ) const;

  //! Finds predecessor of names in array.
  virtual GameValue NamePred( const WordBase &wb, GameValuePar names ) const;

  //! Finds predecessor of unit types in array.
  virtual GameValue UnitPred( const WordBase &wb, GameValuePar units ) const = 0;

  //! Finds predecessor of areas in array.
  virtual GameValue PlacePred( GameValuePar areas ) const;

  //! Finds predecessor of speeds in array.
  virtual GameValue SpeedSizePred( GameValuePar speeds ) const;

  //! Finds predecessor of speeds in array.
  virtual GameValue SpeedDirectionPred( GameValuePar speeds ) const;

  //! Finds last seen place of the candidate in this semantic net.
  virtual GameValue GetLastSeenPlace( const SemanticQuery &candidate ) const = 0;

  //! Finds speed size of the candidate in this semantic net.
  virtual GameValue GetSpeedSize( const SemanticQuery &candidate ) const = 0;

  //! Return id (GameDataString) of the entity type.
  virtual GameValue GetEntityType( const RString &typeName ) const = 0;

  //! Return id (GameDataString) of the entity type.
  virtual GameValue GetEntityType( const SemanticQuery &candidate ) const = 0;

  //! Return base id (GameDataString) of the entity type.
  virtual GameValue GetEntityTypeBase( const RString &typeName ) const = 0;

  //! Fill the array with available meaningful pronouns.
  virtual void FilterDefinedPronouns( const WordBase &wb, AutoArray< WordBaseId > &outcomingPronouns ) const;

  //! Returns ID of particular object in this semantic net.
  virtual SemanticNetId GetId( const SemanticQuery &query ) const = 0;
  
  //! Copy information about side into listener's semantic net.
  virtual void TransferEnemySide( GameValuePar listener, GameValuePar src, GameValuePar dst ) const = 0;
  
  //! Copy information about place into listener's semantic net.
  virtual void TransferPlace( GameValuePar listener, GameValuePar src, GameValuePar dst ) const = 0;
  
  //! Copy information about type into listener's semantic net.
  virtual void TransferType( GameValuePar listener, GameValuePar src, GameValuePar dst ) const = 0;
  
  //! Copy information about last seen time into listener's semantic net.
  virtual void TransferTime( GameValuePar listener, GameValuePar src, GameValuePar dst ) const = 0;
  
  //! Copy information about name into listener's semantic net.
  virtual void TransferName( GameValuePar listener, GameValuePar src, GameValuePar dst ) const = 0;
  
  //! Copy information about speed into listener's semantic net.
  virtual void TransferSpeed( GameValuePar listener, GameValuePar src, GameValuePar dst ) const = 0;

  //! Convert direction to the cardinal point.
  static WordBaseId ExpressDirection( float dx, float dy, const WordBase &wordBase );

protected:
  void CalcCandidateSubgroups( AutoArray< IndexedGroupedResult > &successfulCandidates, 
    const Array2D< GameValue > &evaluatedVariables, const AutoArray< SentenceResultCondition > &conditions,
    const float conditionsPrioritiesSum, const AutoArray< int > &candidateGroup );
    
  //! Add next sentence into reaction.
  void ComposeResponseSentence( Ref< SentenceAtom > &parentReaction, 
    const RString responseId, const WordBase &wb ) const;
    
  //! Add next sentence into reaction.
  void ComposeResponseSentence( Ref< SentenceAtom > &parentReaction, 
    const RString responseId, const WordBase &wb, const AutoArray< SemanticQuery > &candidates ) const;
    
  //! Add next sentence into reaction.
  void ComposeResponseSentence( Ref< SentenceAtom > &parentReaction, 
    const RString responseId, const WordBase &wb, 
    const AutoArray< int > &successfulCandidatesIndices, 
    const Array2D< float > &candidatesConditionsResults,
    const AutoArray< SentenceResultCondition > &conditions, 
    const Array2D< GameValue > &evaluatedVariables,
    const AutoArray< SemanticQuery > &successfulCandidatesQueries, unsigned long conditionBits,
    RefArray< const SemanticKnowledge > &newContextGroupsSubjects,
    AutoArray< CandidatesIndicesGroup > &newContextGroupsCandidates ) const;

  //! Format and add unsuccessful conditions into reaction.
  void AddResult( Ref< SentenceAtom > &reaction, 
    const WordBase &wordBase, const AutoArray< int > &successfulCandidates, 
    const Array2D< float > &candidatesConditionsResults,
    const AutoArray< SentenceResultCondition > &conditions, 
    const Array2D< GameValue > &evaluatedVariables, unsigned long conditionBits,
    RefArray< const SemanticKnowledge > &newContextGroupsSubjects,
    AutoArray< CandidatesIndicesGroup > &newContextGroupsCandidates ) const;

  //! Format and add result values into reaction.
  void AddResultValues( Ref< SentenceAtom > &reaction, int valuesFormatId,
    const WordBase &wordBase, const RString expression, short valueType,
    const AutoArray< MergedValuesGroup > &results ) const;

  //! Put GameValue into sentence expression.
  void FormatResultExpression( Ref< SentenceAtom > &reaction, const WordBase &wordBase, 
    short valueType, const MergedValuesGroup &value, const RString expression ) const;

  //! Convert GameValue to the WordBaseId to be used as notion in context.
  WordBaseId ConvertValueForContext( const WordBase &wordBase, short valueType, const GameValue &value ) const;
  
  //! Format GameValue into required format.
  virtual void FormatResultValue( Ref< SentenceAtom > &reaction, const WordBase &wordBase, 
    short valueType, const GameValue &value, long form ) const;
    
  //! Split selected group into two if necessary.
  static void SplitGroup( const Array2D< float > &similarities, 
    AutoArray< CandidatesIndicesGroup* > &groupedResults, int splitGroupIdx, float splitThreshold );

  //! Calculate aggregated values.
  static void CalcAggregation( const AutoArray< int > &successfulCandidates, 
    const Array2D< GameValue > &evaluatedVariables, const SentenceResultCondition &condition, 
    int conditionIdx, AutoArray< MergedValuesGroup > &mergedResults );
  
  static inline void RoundTime( const float secs, float &outMins, float &outHours, float &outDays )
  {
    if( secs < 60.0f )
    {
      // less than one minute
      outMins = 1.0f;
      outHours = 0.0f;
      outDays = 0.0f;
      return;
    }
    else if( secs < 86400.0f )
    {
      // less than one day
      outDays = 0.0f;
      float mins = secs / 60.0f;
      static const float limits[] = { 5.0f, 15.0f, 30.0f, 45.0f, 60.0f, 90.0f, 120.0f };
      static const int limitsSize = sizeof( limits ) / sizeof( *limits );
      float lastLimit = 1.0f;
      for( int i = 0; i < limitsSize; ++i )
      {
        float l = limits[ i ];
        if( mins <= l )
        {
          if( mins - lastLimit < l - mins )
          {
            l = lastLimit;
          }
          outMins = ::fmodf( l, 60.0f );
          outHours = ::floor( l / 60.0f );
          return;
        }
        lastLimit = l;
      }
      outMins = 0.0f;
      outHours = ::floor( mins / 60.0f + 0.5f );
      return;
    }
    else
    {
      // more than 1 day
      outMins = 0.0f;
      outHours = 0.0f;
      outDays = ::floor( secs / 86400.0f + 0.5 );
      return;
    }
  }
  
  static inline void RoundDistance( const float meters, float &outMeters, float &outKms )
  {
    if( meters < 10.0f )
    {
      outMeters = 10.0f;
      outKms = 0.0f;
      return;
    }
    else if( meters < 1000.0f )
    {
      static const float limits[] = { 50.0f, 100.0f, 200.0f, 500.0f, 1000.0f };
      static const int limitsSize = sizeof( limits ) / sizeof( *limits );
      float lastLimit = 10.0f;
      for( int i = 0; i < limitsSize; ++i )
      {
        float l = limits[ i ];
        if( meters <= l )
        {
          if( meters - lastLimit < l - meters )
          {
            l = lastLimit;
          }
          outMeters = ::fmodf( l, 1000.0f );
          outKms = ::floor( l / 1000.0f );
          return;
        }
        lastLimit = l;
      }
      outMeters = 0.0f;
      outKms = ::floor( meters / 1000.0f + 0.5f );
      return;
    }
    else
    {
      // more than one kilometer
      outMeters = 0.0f;
      float kms = meters / 1000.0f;
      static const float limits[] = { 2.0f, 5.0f, 10.0f };
      static const int limitsSize = sizeof( limits ) / sizeof( *limits );
      float lastLimit = 1.0f;
      for( int i = 0; i < limitsSize; ++i )
      {
        float l = limits[ i ];
        if( kms <= l )
        {
          if( kms - lastLimit < l - kms )
          {
            l = lastLimit;
          }
          outKms = l;
          return;
        }
        lastLimit = l;
      }
      outKms = ::floor( kms / 10.0f + 0.5f ) * 10;
      return;
    }
  }

  static inline int RoundQuatity( const int quantity )
  {
    if( quantity <= 10 )
    {
      return quantity;
    }
    else
    {
      static const int limits[] = { 15, 20, 25, 30, 40, 50, 70, 100, 150, 200, 250, 300, 400, 500, 700, 1000 };
      static const int limitsSize = sizeof( limits ) / sizeof( *limits );
      int lastLimit = 10;
      for( int i = 0; i < limitsSize; ++i )
      {
        float l = limits[ i ];
        if( quantity <= l )
        {
          if( quantity - lastLimit < l - quantity )
          {
            l = lastLimit;
          }
          return l;
        }
        lastLimit = l;
      }
      return ( ( quantity / 500 + 1 ) >> 1 ) * 1000; // Round at every 1000.
    }
  }
  
  static inline float RoundKmph( const float speed )
  {
    if( speed <= 10.0f )
    {
      return 10.0f;
    }
    else
    {
      int intSpeed = speed + 0.5f;
      return ( ( intSpeed / 5 + 1 ) >> 1 ) * 10; // Round at every 10.
    }
  }

  inline static void AddContextNotion( WordBaseId wordId, long form, 
    int successfulCandidatesN, 
    RefArray< const SemanticKnowledge > &newContextGroupsSubjects,
    AutoArray< CandidatesIndicesGroup > &newContextGroupsCandidates )
  {
    // Add all successful candidates as this notion to the context.
    newContextGroupsSubjects.Add( new SemanticKnowledge( wordId, form ) );
    CandidatesIndicesGroup &newIdxs = newContextGroupsCandidates.Append();
    newIdxs.Realloc( successfulCandidatesN );
    newIdxs.Resize( successfulCandidatesN );
    for( int successfulCandidatesI = 0; successfulCandidatesI < successfulCandidatesN; 
      ++successfulCandidatesI )
    {
    	newIdxs[ successfulCandidatesI ] = successfulCandidatesI;
    }
  }

  inline void AddContextNotion( const AutoArray< MergedValuesGroup > &mergedResults,
    const WordBase &wordBase, short valueType, const AutoArray< int > &successfulCandidates,
    RefArray< const SemanticKnowledge > &newContextGroupsSubjects,
    AutoArray< CandidatesIndicesGroup > &newContextGroupsCandidates ) const
  {
    // Add each group of successful candidates as a notion of the particular variable value.
    int mergedResultsN = mergedResults.Size();
    for( int mergedResultsI = 0; mergedResultsI < mergedResultsN; ++mergedResultsI )
    {
      const MergedValuesGroup &merGrp = mergedResults[ mergedResultsI ];
      // Convert merGrp._value to the WordBaseId!
      WordBaseId wordId = ConvertValueForContext( wordBase, valueType, merGrp._value );
      newContextGroupsSubjects.Add( 
        new SemanticKnowledge( 
        wordId, merGrp._candidatesIndices.Size() > 1 ? FORM_PLURAL : FORM_DEFAULT ) );
      CandidatesIndicesGroup &newGrp = newContextGroupsCandidates.Append();
      int merCandidatesIdxsN = merGrp._candidatesIndices.Size();
      newGrp.Realloc( merCandidatesIdxsN );
      newGrp.Resize( merCandidatesIdxsN );
      for( int merCandidatesIdxsI = 0; merCandidatesIdxsI < merCandidatesIdxsN; ++merCandidatesIdxsI )
      {
        newGrp[ merCandidatesIdxsI ] = merGrp._candidatesIndices[ merCandidatesIdxsI ];
      }
    }
  }
};

#endif