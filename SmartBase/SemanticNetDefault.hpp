
#ifndef _SEMANTICNETDEFAULT_HPP
#define _SEMANTICNETDEFAULT_HPP

#include <io.h>

#include "Es/essencepch.hpp"
#include "Es/Strings/bstring.hpp"

#include "SemanticNetBase.hpp"

//! Copied from "/Poseidon/lib/object.hpp"
enum TargetSide
{
  TEast,
  TWest,
  TGuerrila,
  TCivilian,
  TSideUnknown,	// !!! TSideUnknown must be the last item
  TEnemy, // we are sure it is enemy (do not know which)
  TFriendly, // we are sure it is friendly (do not know which)
  TLogic,
  TEmpty, // used for serialize
  NTargetSide,
};
TypeIsSimple( TargetSide );

class SemanticNetDefault : public SemanticNetBase
{
protected:
  static const RStringB _keywordMe;
  static const RStringB _keywordId;
  static const RStringB _keywordName;
  static const RStringB _keywordSide;
  static const RStringB _keywordPosition;
  static const RStringB _keywordType;
  static const RStringB _keywordLastSeen;
  static const RStringB _keywordSpeed;

  const ParamEntryVal _knowledges;
  ParamEntryVal _globalKnowledges;

public:
  SemanticNetDefault( const ParamEntryVal &knowledges, const ParamEntryVal &globalKnowledges, 
    const WordBase *wordBase, const ParamEntryVal &responses, const ConstParamEntryPtr &specRules )
    : SemanticNetBase( wordBase, responses, specRules ), _knowledges( knowledges ), _globalKnowledges( globalKnowledges )
  {
  }
  
  static inline ConstParamEntryPtr GetEntry( const SemanticNetDefault &sn, const SemanticQuery &q, 
    const RStringB &entryName )
  {
    ConstParamEntryPtr table = sn._knowledges.FindEntry( q.GetTable() );
    if( !table )
    {
      table = sn._globalKnowledges.FindEntry( q.GetTable() );
    }
    if( table )
    {
      return table->GetEntry( q.GetIndex() ).FindEntry( entryName );
    }
    return ConstParamEntryPtr();
  }

  //! Method to determine the number of knowledges in the current table.
  virtual int GetKnowledgesSize( const RStringB &tableName ) const
  {
    ConstParamEntryPtr currentTable = _knowledges.FindEntry( tableName );
    if( !currentTable )
    {
      currentTable = _globalKnowledges.FindEntry( tableName );
    }
    return currentTable ? currentTable->GetEntryCount() : 0;
  }

  //! Get pointer to the myself in my semantic net.
  virtual void GetSelfQuery( SemanticQuery &query ) const
  {
    query.SetTable( _tableEntities );
    ParamEntryVal entities = _knowledges >> _tableEntities;
    int n = entities.GetEntryCount();
    for( int i = 0; i < n; ++i )
    {
      if( entities.GetEntry( i ).GetName() == _keywordMe )
      {
        query.SetIndex( i );
        return;
      }
    }
    AssertDebug( 0 );
  }

  //! Get pointer to the object with ID in my semantic net.
  virtual bool GetIdQuery( const SemanticNetId id, SemanticQuery &query ) const
  {
    ParamEntryVal entities = _knowledges >> _tableEntities;
    ConstParamEntryPtr entryId;
    int n = entities.GetEntryCount();
    for( int i = 0; i < n; ++i )
    {
      entryId = entities.GetEntry( i ).FindEntry( _keywordId );
    	if( entryId && id == entryId->GetInt() )
    	{
#ifdef _DEBUG
        BString< 256 > buf;
        buf.PrintF( "Object of id=%d found [%s,%d].", id, _tableEntities.Data(), i );
        Log( buf.cstr() );
#endif

    	  query.SetTable( _tableEntities );
    	  query.SetIndex( i );
    	  return true;
    	}
    }

    ParamClass *pc = entities.GetModPointer()->GetClassInterface();
    AssertDebug( pc != NULL );
    if( pc != NULL )
    {
      BString< 32 > nameBuf;
      RStringB name;
      ParamClassPtr newClass;
      int tc = id;
      do {
        nameBuf.PrintF( "NewEntity%d", tc );
        name = RStringB( nameBuf );
        newClass = pc->AddClass( name );
        ++tc;
      } while( !newClass );
      newClass->Add( _keywordId, id );
      
      ConstParamEntryPtr parent = newClass->FindParent();
      int n = parent->GetEntryCount();
      for( int i = 0; i < n; ++i )
      {
      	if( parent->GetEntry( i ).GetName() == name )
      	{
      	  query.SetTable( _tableEntities );
          query.SetIndex( i );

#ifdef _DEBUG
          BString< 256 > buf;
          buf.PrintF( "Object of id=%d not found, creating new one [%s,%d].", id, _tableEntities.Data(), i );
          Log( buf.cstr() );
#endif
          return true;
      	}
      }
    }
    
    AssertDebug( 0 );
    return false;
  }

  //! Fills the array with enemy sides.
  virtual GameValue GetEnemySides() const;

  //! Converts name of place into PlaceArea struct.
  virtual GameValue GetArea( const RString &areaName ) const;

  // Get area of the candidate.
  virtual GameValue GetArea( const SemanticQuery &candidate ) const;

  // Get distance between two areas.
  virtual GameValue GetDistance( GameValuePar area1, GameValuePar area2 ) const;

  //! Calculates time current time minus given number of seconds.
  virtual GameValue GetTimeLastSecs( float secs ) const;

  //! Finds side of the candidate in this semantic net.
  virtual GameValue GetSide( const SemanticQuery &candidate ) const;

  //! Finds last seen time of the candidate in this semantic net.
  virtual GameValue GetLastSeenTime( const SemanticQuery &candidate ) const;

  //! Finds candidate's name in this semantic net.
  virtual GameValue GetName( const SemanticQuery &candidate ) const;

  //! Finds unit type of the candidate in this semantic net.
  virtual GameValue GetUnitType( const SemanticQuery &candidate ) const;

  //! Are the sides equal from this semantic net view?
  virtual GameValue SideEqual( GameValuePar left, GameValuePar right ) const;

  //! Is the first type subtype of the other?
  virtual GameValue UnitSubtype( const RString subtype, const RString type ) const;

  //! Are the unit types equal from this semantic net view?
  virtual GameValue UnitEqual( const RString left, const RString right ) const;

  //! Finds predecessor of sides in array.
  virtual GameValue SidePred( GameValuePar sides ) const;

  //! Finds predecessor of unit types in array.
  virtual GameValue UnitPred( const WordBase &wb, GameValuePar units ) const;

  //! Finds last seen place of the candidate in this semantic net.
  virtual GameValue GetLastSeenPlace( const SemanticQuery &candidate ) const;

  //! Finds speed size of the candidate in this semantic net.
  virtual GameValue GetSpeedSize( const SemanticQuery &candidate ) const;

  //! Return id (GameDataString) of the entity type.
  virtual GameValue GetEntityType( const RString &typeName ) const;

  //! Return id (GameDataString) of the entity type.
  virtual GameValue GetEntityType( const SemanticQuery &candidate ) const;

  //! Return base id (GameDataString) of the entity type.
  virtual GameValue GetEntityTypeBase( const RString &typeName ) const;

  //! Returns ID of this semantic net.
  virtual SemanticNetId GetId() const;
  
  //! Returns ID of particular object in this semantic net.
  virtual SemanticNetId GetId( const SemanticQuery &query ) const;

  //! Get all defined places in this semantic net.
  virtual void GetDefinedPlaces( const WordBase &wb, AutoArray< WordBaseId > &places ) const;

  //! Get all unit types in this semantic net.
  virtual void GetUnitTypes( const WordBase &wb, AutoArray< WordBaseId > &unitTypes ) const;

  //! Copy information about side into listener's semantic net.
  virtual void TransferEnemySide( GameValuePar listener, GameValuePar src, GameValuePar dst ) const;

  //! Copy information about place into listener's semantic net.
  virtual void TransferPlace( GameValuePar listener, GameValuePar src, GameValuePar dst ) const;

  //! Copy information about type into listener's semantic net.
  virtual void TransferType( GameValuePar listener, GameValuePar src, GameValuePar dst ) const;

  //! Copy information about last seen time into listener's semantic net.
  virtual void TransferTime( GameValuePar listener, GameValuePar src, GameValuePar dst ) const;

  //! Copy information about name into listener's semantic net.
  virtual void TransferName( GameValuePar listener, GameValuePar src, GameValuePar dst ) const;

  //! Copy information about speed into listener's semantic net.
  virtual void TransferSpeed( GameValuePar listener, GameValuePar src, GameValuePar dst ) const;

protected:
  //! Format GameValue into required format.
  virtual void FormatResultValue( Ref< SentenceAtom > &reaction, const WordBase &wordBase, 
    int valueType, const GameValue &value, long form ) const;
    
  static inline int GetUnitTypeDepth( ConstParamEntryPtr tableEntry, const RString unitType )
  {
    ConstParamEntryPtr entry = tableEntry->FindEntry( unitType );
    if( entry )
    {
      return GetUnitTypeDepth( tableEntry, entry );
    }
    return 0;
  }
  
  static inline int GetUnitTypeDepth( ConstParamEntryPtr tableEntry, ConstParamEntryPtr unitType )
  {
    int depth = 0;
    ConstParamEntryPtr te = unitType->FindEntry( _keywordBase );
    while( te )
    {
      te = tableEntry->FindEntry( te->GetValue() )->FindEntry( _keywordBase );
      ++depth;
    }
    return depth;
  }
  
  static inline ConstParamEntryPtr GetUnitTypePred( ConstParamEntryPtr tableEntry, const RString unitType, int n )
  {
    return GetUnitTypePred( tableEntry, tableEntry->FindEntry( unitType ), n );
  }

  static inline ConstParamEntryPtr GetUnitTypePred( ConstParamEntryPtr tableEntry, 
    ConstParamEntryPtr unitType, int n )
  {
    while( n != 0 && unitType )
    {
      unitType = unitType->FindEntry( _keywordBase );
      if( unitType )
      {
        unitType = tableEntry->FindEntry( unitType->GetValue() );
        --n;
      }
    }
    return unitType;
  }

  static inline ConstParamEntryPtr GetUnitTypePred( ConstParamEntryPtr tableEntry, ConstParamEntryPtr unitType )
  {
    ConstParamEntryPtr baseEntry = unitType->FindEntry( _keywordBase );
    if( baseEntry )
    {
      return tableEntry->FindEntry( baseEntry->GetValue() );
    }
    return baseEntry;
  }
  
  template< typename ValueType >  
  static inline void SetEntry( const SemanticNetDefault &sn, const SemanticQuery &q,
    const RStringB &entryName, const ValueType &val )
  {
    ConstParamEntryPtr table = sn._knowledges.FindEntry( q.GetTable() );
    if( table )
    {
      ParamEntryVal v = table->GetEntry( q.GetIndex() );
      v.GetModPointer()->Add( entryName, val );
    }
  }
  
  static inline void SetEntryArrayOfFloats( const SemanticNetDefault &sn, const SemanticQuery &q,
    const RStringB &entryName, ParamEntryVal &val )
  {
    ConstParamEntryPtr table = sn._knowledges.FindEntry( q.GetTable() );
    if( table )
    {
      ParamEntryVal v = table->GetEntry( q.GetIndex() );
      ParamEntryPtr arr = v.GetModPointer()->AddArray( entryName );
      if( arr )
      {
        int n = val.GetSize();
        for( int i = 0; i < n; ++i )
        {
          arr->AddValue( val[ i ].GetFloat() );
        }
      }
    }
  }

  inline void TransferEntryFloat( const SemanticNetDefault &listener, const SemanticQuery &src,
    const SemanticQuery &dst, const RStringB &entryName ) const
  {
    ConstParamEntryPtr pSrc = GetEntry( *this, src, entryName );
    if( pSrc )
    {
      SetEntry( listener, dst, entryName, static_cast< float >( *pSrc ) );
    }
  }

  inline void TransferEntryInt( const SemanticNetDefault &listener, const SemanticQuery &src,
    const SemanticQuery &dst, const RStringB &entryName ) const
  {
    ConstParamEntryPtr pSrc = GetEntry( *this, src, entryName );
    if( pSrc )
    {
      SetEntry( listener, dst, entryName, static_cast< int >( *pSrc ) );
    }
  }

  inline void TransferEntryString( const SemanticNetDefault &listener, const SemanticQuery &src,
    const SemanticQuery &dst, const RStringB &entryName ) const
  {
    ConstParamEntryPtr pSrc = GetEntry( *this, src, entryName );
    if( pSrc )
    {
      SetEntry( listener, dst, entryName, pSrc->GetValue() );
    }
  }

  inline void TransferEntryArrayOfFloats( const SemanticNetDefault &listener, const SemanticQuery &src,
    const SemanticQuery &dst, const RStringB &entryName ) const
  {
    ConstParamEntryPtr pSrc = GetEntry( *this, src, entryName );
    if( pSrc )
    {
      SetEntryArrayOfFloats( listener, dst, entryName, *pSrc );
    }
  }
};

#endif