#if _ENABLE_CONVERSATION

#include "El/elementpch.hpp"
#include "El/Modules/modules.hpp"
#include "El/ParamArchive/paramArchive.hpp"

#include "SemanticNetDefault.hpp"
#include "PlaceArea.hpp"
#include "TimeInterval.hpp"

//! Copied from "/Poseidon/lib/object.cpp"
static const EnumName SideNames[]=
{
  EnumName(TWest,"WEST"),
  EnumName(TEast,"EAST"),
  EnumName(TGuerrila,"GUER"),
  EnumName(TCivilian,"CIV"),
  EnumName(TSideUnknown,"UNKNOWN"),
  EnumName(TEnemy,"ENEMY"),
  EnumName(TFriendly,"FRIENDLY"),
  EnumName(TLogic,"LOGIC"),
  EnumName(TEmpty,"EMPTY"),
  EnumName()
};

template<>
const EnumName *GetEnumNames( TargetSide dummy )
{
  return SideNames;
}

//! Copied from "/Poseidon/lib/gameStateExt.hpp"
const GameType GameSide(0x1000);

typedef TargetSide GameSideType;

class GameDataSide: public GameData
{
  typedef GameData base;

  GameSideType _value;

public:
  GameDataSide()
    :_value (TSideUnknown)
  {
  }

  GameDataSide( GameSideType value )
    :_value( value )
  {
  }

  ~GameDataSide()
  {
  }

  GameType GetType() const
  {
    return GameSide;
  }

  GameSideType GetSide() const 
  {
    return _value;
  }

  RString GetText() const;

  bool IsEqualTo( const GameData *data ) const;

  const char *GetTypeName() const 
  {
    return "side";
  }

  GameData *Clone() const 
  {
    return new GameDataSide( *this );
  }

  LSError Serialize( ParamArchive &ar );

  USE_FAST_ALLOCATOR
};

DEFINE_FAST_ALLOCATOR( GameDataSide )

RString GameDataSide::GetText() const
{
  return FindEnumName( _value );
}

bool GameDataSide::IsEqualTo( const GameData *data ) const
{
  const TargetSide val1 = GetSide();
  const TargetSide val2 = static_cast<const GameDataSide *>( data )->GetSide();
  return val1 == val2;
}

LSError GameDataSide::Serialize( ParamArchive &ar )
{
  CHECK( base::Serialize( ar ) );
  CHECK( ar.SerializeEnum( "value", _value, 1 ) );
  return LSOK;
}

static inline TargetSide GetSide( GameValuePar oper )
{
  return ( oper.GetType() == GameSide ) ? static_cast< GameDataSide* >( oper.GetData() )->GetSide() : TSideUnknown;
}

GameData *CreateGameDataSide()
{
  return new GameDataSide();
}

GameValue SemanticNetDefault::GetEnemySides() const
{
  Ref< GameDataArray > resultData = new GameDataArray();
  GameArrayType &resultArray = resultData->GetArray();
  ConstParamEntryPtr entry = ( ( _knowledges >> "Entities" ) >> "Me" ).FindEntry( "side" );
  if( entry )
  {
    switch( GetEnumValue< TargetSide >( entry->GetValue() ) )
    {
    case TWest:
    case TGuerrila:
      resultArray.Add( GameValue( new GameDataSide( TEast ) ) );
      break;

    case TEast:
      resultArray.Add( GameValue( new GameDataSide( TWest ) ) );
      resultArray.Add( GameValue( new GameDataSide( TGuerrila ) ) );
      break;
    }
  }
  return GameValue ( resultData );
}

GameValue SemanticNetDefault::GetArea( const RString &areaName ) const
{
  int n = GetKnowledgesSize( _tablePlaces );
  SemanticQuery place( _tablePlaces );
  for( ; place.GetIndex() < n; place.IncIndex() )
  {
  	if( areaName == GetName( place ) )
  	{
  	  return GetArea( place );
  	}
  }
  
  AssertDebug( 0 );
  return GameValue( new GameDataArea() );
}

GameValue SemanticNetDefault::GetArea( const SemanticQuery &candidate ) const
{
  ConstParamEntryPtr entry = _knowledges.FindEntry( candidate.GetTable() );
  if( !entry )
  {
    entry = _globalKnowledges.FindEntry( candidate.GetTable() );
  }
  if( entry )
  {
    entry = entry->GetEntry( candidate.GetIndex() ).FindEntry( "position" );
    if( entry && entry->IsArray() && entry->GetSize() > 2 )
    {
      return GameValue( new GameDataArea( PlaceArea( (*entry)[ 0 ], (*entry)[ 1 ], (*entry)[ 2 ] ) ) );
    }
  }
  
  AssertDebug( 0 );
  return GameValue( new GameDataArea() );
}

GameValue SemanticNetDefault::GetDistance( GameValuePar area1, GameValuePar area2 ) const
{
  const PlaceArea *pt1 = ::GetArea( area1 );
  const PlaceArea *pt2 = ::GetArea( area2 );
  if( pt1 != NULL && pt2 != NULL )
  {
    float dx = pt1->_center.X() - pt2->_center.X();
    float dy = pt1->_center.Y() - pt2->_center.Y();
    float d = ::sqrt( dx * dx + dy * dy );
    float dev = pt1->_deviation + pt2->_deviation;
    d = d > dev ? d - dev : 0.0f;
    return GameValue( new GameDataScalar( d ) );
  }
  
  AssertDebug( 0 );
  return GameValue( new GameDataScalar( FLT_MAX ) );
}

GameValue SemanticNetDefault::GetTimeLastSecs( float secs ) const
{
  return GameValue( new GameDataTime( TimeInterval( -secs, 0 ) ) );
}

GameValue SemanticNetDefault::GetSide( const SemanticQuery &candidate ) const
{
  ConstParamEntryPtr entry = _knowledges.FindEntry( candidate.GetTable() );
  if( !entry )
  {
    entry = _globalKnowledges.FindEntry( candidate.GetTable() );
  }
  if( entry )
  {
    entry = entry->GetEntry( candidate.GetIndex() ).FindEntry( "side" );
    if( entry )
    {
      return GameValue( new GameDataSide( GetEnumValue< TargetSide >( entry->GetValue() ) ) );
    }
  }

  return GameValue( new GameDataSide( TSideUnknown ) );
}

GameValue SemanticNetDefault::GetLastSeenTime( const SemanticQuery &candidate ) const
{
  ConstParamEntryPtr entry = _knowledges.FindEntry( candidate.GetTable() );
  if( !entry )
  {
    entry = _globalKnowledges.FindEntry( candidate.GetTable() );
  }
  if( entry )
  {
    entry = entry->GetEntry( candidate.GetIndex() ).FindEntry( "lastSeen" );
    if( entry )
    {
      return GameValue( new GameDataTime( TimeInterval( static_cast< float >( *entry ) ) ) );
    }
  }

  AssertDebug( 0 );
  return GameValue( new GameDataTime( TimeInterval() ) );
}

GameValue SemanticNetDefault::GetName( const SemanticQuery &candidate ) const
{
  ConstParamEntryPtr entry = _knowledges.FindEntry( candidate.GetTable() );
  if( !entry )
  {
    entry = _globalKnowledges.FindEntry( candidate.GetTable() );
  }
  if( entry )
  {
    entry = entry->GetEntry( candidate.GetIndex() ).FindEntry( _keywordName );
    if( entry )
    {
      return GameValue( new GameDataString( RString( entry->GetValue() ) ) );
    }
  }
  
  // AssertDebug( 0 );
  return GameValue( new GameDataString( RString() ) );
}

GameValue SemanticNetDefault::GetUnitType( const SemanticQuery &candidate ) const
{
  ConstParamEntryPtr entry = _knowledges.FindEntry( candidate.GetTable() );
  if( !entry )
  {
    entry = _globalKnowledges.FindEntry( candidate.GetTable() );
  }
  if( entry )
  {
    entry = entry->GetEntry( candidate.GetIndex() ).FindEntry( "type" );
    if( entry )
    {
      return GameValue( new GameDataString( RString( entry->GetValue() ) ) );
    }
  }

  //... The value "All" should be found in configuration file.
  return GameValue( new GameDataString( "All" ) );
}

GameValue SemanticNetDefault::GetLastSeenPlace( const SemanticQuery &candidate ) const
{
  ConstParamEntryPtr entry = _knowledges.FindEntry( candidate.GetTable() );
  if( !entry )
  {
    entry = _globalKnowledges.FindEntry( candidate.GetTable() );
  }
  if( entry )
  {
    entry = entry->GetEntry( candidate.GetIndex() ).FindEntry( "position" );
    if( entry && entry->IsArray() && entry->GetSize() > 2 )
    {
      return GameValue( new GameDataArea( PlaceArea( (*entry)[ 0 ], (*entry)[ 1 ], (*entry)[ 2 ] ) ) );
    }
  }
  
  return GameValue( new GameDataArea( PlaceArea( FLT_MAX, FLT_MAX, FLT_MAX ) ) );
}

GameValue SemanticNetDefault::GetSpeedSize( const SemanticQuery &candidate ) const
{
  ConstParamEntryPtr entry = _knowledges.FindEntry( candidate.GetTable() );
  if( !entry )
  {
    entry = _globalKnowledges.FindEntry( candidate.GetTable() );
  }
  if( entry )
  {
    entry = entry->GetEntry( candidate.GetIndex() ).FindEntry( "speed" );
    if( entry && entry->IsArray() && entry->GetSize() > 2 )
    {
      Vector3 speed( (*entry)[ 0 ], (*entry)[ 1 ], 0.0f );
      return GameValue( new GameDataScalar( speed.Size() ) );
    }
  }

  return GameValue( new GameDataArea( PlaceArea( FLT_MAX, FLT_MAX, FLT_MAX ) ) );
}

GameValue SemanticNetDefault::GetEntityType( const SemanticQuery &candidate ) const
{
  ConstParamEntryPtr entry = _knowledges.FindEntry( candidate.GetTable() );
  if( !entry )
  {
    entry = _globalKnowledges.FindEntry( candidate.GetTable() );
  }
  if( entry )
  {
    ParamEntryVal val = entry->GetEntry( candidate.GetIndex() );
    if( !val.GetPointer()->IsError() && val.IsClass() )
    {
      return GameValue( new GameDataString( val.GetName() ) );
    }
  }

  AssertDebug( 0 );
  return GameValue( new GameDataString() );
}

GameValue SemanticNetDefault::GetEntityType( const RString &typeName ) const
{
  int n = GetKnowledgesSize( _tableEntityTypes );
  SemanticQuery entityType( _tableEntityTypes );
  for( ; entityType.GetIndex() < n; entityType.IncIndex() )
  {
    RString str = static_cast< RString >( GetName( entityType ) );
    str.Lower();
    if( typeName == str )
    {
      return GetEntityType( entityType );
    }
  }

  AssertDebug( 0 );
  return GameValue( new GameDataString() );
}

GameValue SemanticNetDefault::SideEqual( GameValuePar left, GameValuePar right ) const
{
  GameType leftType = left.GetType();
  GameType rightType = right.GetType();
  if( leftType == GameSide && rightType == GameSide )
  {
    TargetSide sideLeft = static_cast< GameDataSide* >( left.GetData() )->GetSide();
    TargetSide sideRight = static_cast< GameDataSide* >( right.GetData() )->GetSide();
    return GameValue( new GameDataScalar( sideLeft == sideRight ? 1.0f : 0.0f ) );
  }
  else if( ( leftType == GameArray || rightType == GameArray ) &&
    ( leftType == GameSide || rightType == GameSide ) )
  {
    TargetSide side = leftType == GameSide ? static_cast< GameDataSide* >( left.GetData() )->GetSide() :
  static_cast< GameDataSide* >( right.GetData() )->GetSide();
  const GameArrayType &sides = leftType == GameArray ? left : right;
  int n = sides.Size();
  int i = 0;
  while( i < n )
  {
    const GameValue &tmpSide = sides[ i ];
    if( tmpSide.GetType() != GameSide || 
      static_cast< GameDataSide* >( tmpSide.GetData() )->GetSide() != side )
    {
      ++i;
      continue;
    }
    return GameValue( new GameDataScalar( 1.0f ) );
  }
  return GameValue( new GameDataScalar( 0.0f ) );
  }
  return GameValue( new GameDataScalar( 0.0f ) );
}

GameValue SemanticNetDefault::SidePred( GameValuePar sides ) const
{
  const GameArrayType &arr = sides;
  int n = arr.Size();
  if( n != 0 )
  {
    TargetSide result( ::GetSide( arr[ 0 ] ) );
    int i = 1;
    while( i < n )
    {
      if( result == ::GetSide( arr[ i ] ) )
      {
        ++i;
        continue;
      }
      return GameValue( new GameDataSide( TSideUnknown ) );
    }
    return GameValue( new GameDataSide( result ) );
  }
  return GameValue( new GameDataSide( TSideUnknown ) );
}

GameValue SemanticNetDefault::UnitPred( const WordBase &wb, GameValuePar units ) const
{
  const GameArrayType &arr = units;
  int n = arr.Size();
  if( n != 0 )
  {
    ConstParamEntryPtr tableEntry = _knowledges.FindEntry( _tableEntityTypes );
    if( !tableEntry )
    {
      tableEntry = _globalKnowledges.FindEntry( _tableEntityTypes );
    }
    if( tableEntry )
    {
      ConstParamEntryPtr final = tableEntry->FindEntry( GetSemanticKey( arr[ 0 ], wb ) );
      int finalDepth = GetUnitTypeDepth( tableEntry, final );
      --n;
      for( int i = 0; i < n; ++i )
      {
        ConstParamEntryPtr work = tableEntry->FindEntry( GetSemanticKey( arr[ i + 1 ], wb ) );
      	int workDepth = GetUnitTypeDepth( tableEntry, work );
      	if( finalDepth > workDepth )
      	{
      	  final = GetUnitTypePred( tableEntry, final, finalDepth - workDepth );
      	  finalDepth = workDepth;
      	}
      	else if( finalDepth < workDepth )
      	{
          work = GetUnitTypePred( tableEntry, work, workDepth - finalDepth );
          workDepth = finalDepth;
      	}
      	
      	while( finalDepth != 0 && final->GetName() != work->GetName() )
      	{
          final = GetUnitTypePred( tableEntry, final );
          work = GetUnitTypePred( tableEntry, work );
          --finalDepth;
      	}
      }
      return GameValue( new GameDataString( final->GetName() ) );
    }
  }

  return GameValue( new GameDataString() );
}

GameValue SemanticNetDefault::UnitEqual( const RString left, const RString right ) const
{
  ConstParamEntryPtr tableEntry = _knowledges.FindEntry( _tableEntityTypes );
  if( !tableEntry )
  {
    tableEntry = _globalKnowledges.FindEntry( _tableEntityTypes );
  }
  if( tableEntry )
  {
    int leftDepth = GetUnitTypeDepth( tableEntry, left );
    int rightDepth = GetUnitTypeDepth( tableEntry, right );
    int depth;
    ConstParamEntryPtr leftEntry;
    if( leftDepth > rightDepth )
    { 
      depth = rightDepth;
      leftEntry = GetUnitTypePred( tableEntry, left, leftDepth - rightDepth );
    }
    else
    {
      depth = leftDepth;
      leftEntry = tableEntry->FindEntry( left );
    }
    ConstParamEntryPtr rightEntry = leftDepth < rightDepth ? 
      GetUnitTypePred( tableEntry, right, rightDepth - leftDepth ) : tableEntry->FindEntry( right );
    AssertDebug( leftEntry && rightEntry );

    while( depth != 0 )
    {
      if( leftEntry->GetName() != rightEntry->GetName() )
      {
        leftEntry = GetUnitTypePred( tableEntry, leftEntry );
        rightEntry = GetUnitTypePred( tableEntry, rightEntry );
        --depth;
        continue;
      }
      
      float maxDepth = tableEntry->GetEntryCount();
      leftDepth -= depth;
      rightDepth -= depth;
      float leftSim = ( 1.0f - 1.0f / ( maxDepth - 1.0f - leftDepth ) ) * 0.5f * ( maxDepth - 1.0f ) / ( maxDepth - 2.0f );
      float rightSim = ( 1.0f - 1.0f / ( maxDepth - 1.0f - rightDepth ) ) * 0.5f * ( maxDepth - 1.0f ) / ( maxDepth - 2.0f );
      AssertDebug( leftSim + rightSim >= 0.0f && leftSim + rightSim <= 1.0f );
//#ifdef _DEBUG
//      char buf[ 256 ];
//      sprintf( buf, "UnitEqual: %g", leftSim + rightSim );
//      Log( buf );
//#endif
      return GameValue( new GameDataScalar( leftSim + rightSim ) );
    }
  }

  return GameValue( new GameDataScalar( left == right ? 1.0f : 0.0f ) );
}

GameValue SemanticNetDefault::UnitSubtype( const RString subtype, const RString type ) const
{
  ConstParamEntryPtr tableEntry = _knowledges.FindEntry( _tableEntityTypes );
  if( !tableEntry )
  {
    tableEntry = _globalKnowledges.FindEntry( _tableEntityTypes );
  }
  if( tableEntry )
  {
    ConstParamEntryPtr entry = tableEntry->FindEntry( subtype );
    while( entry )
    {
      if( type == entry->GetName() )
      {
      	return GameValue( new GameDataScalar( 1.0f ) );
      }
      entry = entry->FindEntry( _keywordBase );
      if( entry )
      {
        entry = tableEntry->FindEntry( entry->GetValue() );
        continue;
      }
      // No more base type.
      break;
    }
  }
  
  return GameValue( new GameDataScalar() );
}

GameValue SemanticNetDefault::GetEntityTypeBase( const RString &typeName ) const
{
  ConstParamEntryPtr entry = _knowledges.FindEntry( _tableEntityTypes );
  if( !entry )
  {
    entry = _globalKnowledges.FindEntry( _tableEntityTypes );
  }
  if( entry )
  {
    entry = ( ( *entry ) >> typeName ).FindEntry( _keywordBase );
    if( entry )
    {
      return GameValue( new GameDataString( entry->GetValue() ) );
    }
  }
  
  AssertDebug( 0 );
  return GameValue( new GameDataString() );
}

void SemanticNetDefault::FormatResultValue( Ref< SentenceAtom > &reaction, const WordBase &wordBase, 
  int valueType, const GameValue &value, long form ) const
{
  switch( valueType )
  {
    case VT_SIDE:
      {
        RStringB name = FindEnumName( static_cast< GameDataSide* >( value.GetData() )->GetSide() );
        reaction->AddSubAtom( new SemanticKnowledge( wordBase._wordIdCustomId, name, form, wordBase ) );
      }
      break;
    
    default:
      SemanticNetBase::FormatResultValue( reaction, wordBase, valueType, value, form );
      break;
  }
}

int SemanticNetDefault::GetId() const
{
  ConstParamEntryPtr entry = _knowledges.FindEntry( _tableEntities );
  if( !entry )
  {
    entry = _globalKnowledges.FindEntry( _tableEntities );
  }
  if( entry )
  {
    entry = entry->FindEntry( _keywordMe );
    if( entry )
    {
      entry = entry->FindEntry( _keywordId );
      if( entry )
      {
        return entry->GetInt();
      }
    }
  }

  AssertDebug( 0 );
  return -1;
}

int SemanticNetDefault::GetId( const SemanticQuery &query ) const
{
  ConstParamEntryPtr entry = _knowledges.FindEntry( query.GetTable() );
  if( !entry )
  {
    entry = _globalKnowledges.FindEntry( query.GetTable() );
  }
  if( entry )
  {
    ParamEntryVal v = entry->GetEntry( query.GetIndex() );
    entry = v.FindEntry( _keywordId );
    if( entry )
    {
      return entry->GetInt();
    }
  }

  AssertDebug( 0 );
  return -1;
}

void SemanticNetDefault::GetDefinedPlaces( const WordBase &wb, AutoArray< WordBaseId > &places ) const
{
  ConstParamEntryPtr entry = _knowledges.FindEntry( _tablePlaces );
  if( !entry )
  {
    entry = _globalKnowledges.FindEntry( _tablePlaces );
  }
  if( entry )
  {
    int n = entry->GetEntryCount();
    ConstParamEntryPtr name;
    for( int i = 0; i < n; ++i )
    {
    	name = entry->GetEntry( i ).FindEntry( _keywordName );
    	if( name )
    	{
        WordBaseId id = wb.ConvertTextToId( name->GetValue() );
        // AssertDebug( id != -1 );

        if( id != -1 )
        {
          places.Add( id );
        }
#ifdef _DEBUG
        else
        {
          BString< 128 > buf;
          buf.PrintF( "Place \"%s\" not in WordBase.", name->GetValue().Data() );
          Log( buf );
        }
#endif
    	}
    }
  }
}

void SemanticNetDefault::GetUnitTypes( const WordBase &wb, AutoArray< WordBaseId > &unitTypes ) const
{
  int n = GetKnowledgesSize( _tableEntityTypes );
  SemanticQuery entityType( _tableEntityTypes );
  for( ; entityType.GetIndex() < n; entityType.IncIndex() )
  {
    RString str = static_cast< RString >( GetName( entityType ) );
    str.Lower();
    WordBaseId id = wb.ConvertTextToId( str );
    if( id != -1 )
    {
      int n = unitTypes.Size();
      int i = 0;
      while( true )
      {
        if( i < n )
        {
          if( unitTypes[ i ] != id )
          {
            ++i;
            continue;
          }
          break;
        }
        unitTypes.Add( id );
        break;
      }
    }
#ifdef _DEBUG
    else
    {
      //...
      //BString< 128 > buf;
      //buf.PrintF( "Unit type name \"%s\" not defined in word base.", str.Data() );
      //Log( buf );
    }
#endif
  }
}

void SemanticNetDefault::TransferEnemySide( GameValuePar listener, GameValuePar src, GameValuePar dst ) const
{
  const KnowledgeBase *l;
  if( ( l = GetKnowledgeBase( listener ) ) != NULL )
  {
    const SemanticQuery *s;
    const SemanticQuery *d;
    if( ( s = GetQuery( src ) ) != NULL && ( d = GetQuery( dst ) ) != NULL )
    {
      TransferEntryString( *static_cast< const SemanticNetDefault* >( l->GetSemanticNet() ), *s, *d, _keywordSide );
    }
  }
}

void SemanticNetDefault::TransferPlace( GameValuePar listener, GameValuePar src, GameValuePar dst ) const
{
  const KnowledgeBase *l;
  if( ( l = GetKnowledgeBase( listener ) ) != NULL )
  {
    const SemanticQuery *s;
    const SemanticQuery *d;
    if( ( s = GetQuery( src ) ) != NULL && ( d = GetQuery( dst ) ) != NULL )
    {
      TransferEntryArrayOfFloats( *static_cast< const SemanticNetDefault* >( l->GetSemanticNet() ), *s, *d, 
        _keywordPosition );
    }
  }
}

void SemanticNetDefault::TransferType( GameValuePar listener, GameValuePar src, GameValuePar dst ) const
{
  const KnowledgeBase *l;
  if( ( l = GetKnowledgeBase( listener ) ) != NULL )
  {
    const SemanticQuery *s;
    const SemanticQuery *d;
    if( ( s = GetQuery( src ) ) != NULL && ( d = GetQuery( dst ) ) != NULL )
    {
      TransferEntryString( *static_cast< const SemanticNetDefault* >( l->GetSemanticNet() ), *s, *d, _keywordType );
    }
  }
}

void SemanticNetDefault::TransferTime( GameValuePar listener, GameValuePar src, GameValuePar dst ) const
{
  const KnowledgeBase *l;
  if( ( l = GetKnowledgeBase( listener ) ) != NULL )
  {
    const SemanticQuery *s;
    const SemanticQuery *d;
    if( ( s = GetQuery( src ) ) != NULL && ( d = GetQuery( dst ) ) != NULL )
    {
      TransferEntryFloat( *static_cast< const SemanticNetDefault* >( l->GetSemanticNet() ), *s, *d, _keywordLastSeen );
    }
  }
}

void SemanticNetDefault::TransferName( GameValuePar listener, GameValuePar src, GameValuePar dst ) const
{
  const KnowledgeBase *l;
  if( ( l = GetKnowledgeBase( listener ) ) != NULL )
  {
    const SemanticQuery *s;
    const SemanticQuery *d;
    if( ( s = GetQuery( src ) ) != NULL && ( d = GetQuery( dst ) ) != NULL )
    {
      TransferEntryString( *static_cast< const SemanticNetDefault* >( l->GetSemanticNet() ), *s, *d, _keywordName );
    }
  }
}

void SemanticNetDefault::TransferSpeed( GameValuePar listener, GameValuePar src, GameValuePar dst ) const
{
  const KnowledgeBase *l;
  if( ( l = GetKnowledgeBase( listener ) ) != NULL )
  {
    const SemanticQuery *s;
    const SemanticQuery *d;
    if( ( s = GetQuery( src ) ) != NULL && ( d = GetQuery( dst ) ) != NULL )
    {
      TransferEntryArrayOfFloats( *static_cast< const SemanticNetDefault* >( l->GetSemanticNet() ), *s, *d, 
        _keywordSpeed );
    }
  }
}

const RStringB SemanticNetDefault::_keywordMe( "Me" );
const RStringB SemanticNetDefault::_keywordId( "id" );
const RStringB SemanticNetDefault::_keywordName( "name" );
const RStringB SemanticNetDefault::_keywordSide( "side" );
const RStringB SemanticNetDefault::_keywordPosition( "position" );
const RStringB SemanticNetDefault::_keywordType( "type" );
const RStringB SemanticNetDefault::_keywordLastSeen( "lastSeen" );
const RStringB SemanticNetDefault::_keywordSpeed( "speed" );

/*! Script operator to get the side of the candidate.
\param oper1 Is type of GameKnowledgeBase.
\param oper2 Is type of GameQuery (candidate).
\return Value of the type GameSide.
*/
GameValue KnowledgeBase_Side( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
  const SemanticQuery *candidate;
  const KnowledgeBase *participant;
  if( ( candidate = GetQuery( oper2 ) ) != NULL && ( participant = GetKnowledgeBase( oper1 ) ) != NULL )
  {
    return static_cast< const SemanticNetBase* >( participant->GetSemanticNet() )->GetSide( *candidate );
  }

  return GameValue();
}

/*! Script operator to find predecessor of the sides.
\param oper1 Is type of GameKnowledgeBase.
\param oper2 Is type of GameArray with GameSide elements.
\return Value of the type GameSide.
*/
GameValue KnowledgeBase_SidePred( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
  const KnowledgeBase *participant;
  if( ( participant = GetKnowledgeBase( oper1 ) ) != NULL )
  {
    return static_cast< const SemanticNetBase* >( participant->GetSemanticNet() )->SidePred( oper2 );
  }

  return GameValue();
}

INIT_MODULE( SemanticNetDefaultMain, 3 )
{
  GGameState.NewType( "SIDE", GameSide, CreateGameDataSide, "Side" );

  // 'Variable' operators.
  GGameState.NewOperator( GameOperator( GameSide,       "side",            function, KnowledgeBase_Side,            GameKnowledgeBase, GameQuery ) );

  // 'Predecessor' operator.
  GGameState.NewOperator( GameOperator( GameSide,       "sidePred",        function, KnowledgeBase_SidePred,        GameKnowledgeBase, GameArray ) );
};

#endif
