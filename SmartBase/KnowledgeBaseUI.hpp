
#ifndef _KNOWLEDGEBASEUI_HPP
#define _KNOWLEDGEBASEUI_HPP

#include "KnowledgeBase.hpp"
#include "SentenceRule.hpp"
#include "SentenceUi.hpp"

//! Abstract class for options during sentence creation.
class KnowledgeBaseUiItem : public RefCount
{
public:
  virtual void GetText( RString &text ) const = 0;
};
typedef KnowledgeBaseUiItem *PtrKnowledgeBaseUiItem;
typedef Ref< KnowledgeBaseUiItem > RefKnowledgeBaseUiItem;

//! Abstract class for item in menu of players.
class KnowledgeBasePlayerUiItem : public KnowledgeBaseUiItem
{
public:
  virtual void GetText( RString &text ) const = 0;
};
typedef KnowledgeBasePlayerUiItem *PtrKnowledgeBasePlayerUiItem;
typedef Ref< KnowledgeBasePlayerUiItem > RefKnowledgeBasePlayerUiItem;

//! Abstract class for item in menu of possible next words.
class KnowledgeBaseSentenceUiItem : public KnowledgeBaseUiItem
{
protected:
  Ref< WordBase > _wordBase;

  Ref< SentenceRule > _sentenceRule;
  
public:
  KnowledgeBaseSentenceUiItem( WordBase *wordBase, SentenceRule *rule )
    : _wordBase( wordBase ), _sentenceRule( rule )
  {
  }

  virtual void GetText( RString &text ) const
  {
    AssertDebug( _sentenceRule.NotNull() );
    _sentenceRule->GetText( _wordBase, text );
  }
  
  SentenceRule *GetSentenceRule()
  {
    return _sentenceRule;
  }
};

//! Struct of conversation history (the speech spoken by speaker).
struct ConversationSpeech
{
  Ref< KnowledgeBase > _speaker;
  RString _speech;
};
TypeIsMovableZeroed( ConversationSpeech );

//! Abstract inteface of knowledge base Ui modul.
class KnowledgeBaseUi : public RefCount
{
public:
  //! Reset sentence to starting point. This method is called from constructor and SentenceTell.
  virtual void ResetSentence() = 0;

  //! Get the text of the whole sentence.
  virtual void GetSentenceText( RString &text ) const = 0;

  virtual bool CanUndo() const = 0;
  virtual bool CanTell() const = 0;

  virtual int GetSentenceUiItems( RefArray< KnowledgeBaseSentenceUiItem > &items ) const = 0;
  virtual void ChooseSentenceUiItem( KnowledgeBaseSentenceUiItem *item ) = 0;

  //! Calls ResetSentence and updates the text of the conversation.
  virtual void SentenceTell() = 0;
  virtual void GetConversation( AutoArray< ConversationSpeech > &conversation ) const = 0;
  
  virtual void SentenceUndo() = 0;
  virtual void SentenceSelectAtPoint( int x, int y ) = 0;
  virtual void SentenceMoveSelectionLeft() = 0;
  virtual void SentenceMoveSelectionRight() = 0;
  virtual void SentenceSetModified( bool sentenceModified ) = 0;
  virtual bool SentenceIsModified() const = 0;

  virtual SentenceUiFragment *SentenceGetUiFragment() const = 0;
  virtual SentenceUiFragment *SentenceGetSelection() const = 0;
};

#endif