
#ifndef _SEMANTICNET_HPP
#define _SEMANTICNET_HPP

#include "KnowledgeBase.hpp"
#include "SentenceRule.hpp"

//! ID of the semantic net.
typedef int SemanticNetId;

//! Necessary interface of semantic net. How it is stored depends on particular implementation.
class SemanticNet : public RefCount
{
public:
  //! Init context at the beginning of any conversation.
  virtual void InitConversationContext( const AutoArray< SemanticNetId > &youPronoun ) = 0;

  //! Init conversation at the beginning of each speech (of the owner of this semantic net).
  virtual void InitSpeechContext() = 0;

  //! 'participant' (should be the owner of this semantic net) reacts to the 'sentence'.
  virtual void React( KnowledgeBase *speaker, const SentenceFragment *sentence, 
    AutoArray< SentenceResultCondition > &complementaryConditions, 
    AutoArray< SentenceResultCondition > &complementarySubjects, Ref< SentenceAtom > &reaction,
    const RefArray< KnowledgeBase > &listeners ) = 0;

  //! Fill the array with available meaningful pronouns.
  virtual void FilterDefinedPronouns( const WordBase &wb, AutoArray< WordBaseId > &outcomingPronouns ) const = 0;

  //! Returns ID of this semantic net.
  virtual int GetId() const = 0;
  
  //! Get all defined places in this semantic net.
  virtual void GetDefinedPlaces( const WordBase &wb, AutoArray< WordBaseId > &places ) const = 0;
  
  //! Get all unit types in this semantic net.
  virtual void GetUnitTypes( const WordBase &wb, AutoArray< WordBaseId > &unitTypes ) const = 0;
  
  //! Get sentence rules specific for this semantic net.
  virtual void GetSpecRules( const KnowledgeBase *player, RefArray< SentenceRule > &specRules ) const = 0;
  
  //! Is this notion defined in the context of this semantic net?
  virtual bool IsNotionDefined( WordBaseId notionId ) const = 0;
  
  //! Return the number of notions defined in this semantic net (or its context).
  virtual int GetNotionsSize() const = 0;
  
  //! Return the notion defined in this semantic net (or its context).
  virtual const SemanticKnowledge *GetNotion( int i ) const = 0;
};

#endif