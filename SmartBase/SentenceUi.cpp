#if _ENABLE_CONVERSATION

#include "SentenceUi.hpp"

void SegmentUiUndo::SetOpenInfo( SentenceUiFragment *fragment )
{
  _fragment = fragment;
  _oldChildren = fragment->GetModFragments();
  _oldMood = fragment->GetMood();
}

void SegmentUiUndo::SetCloseInfo( SentenceUiFragment *closeSelection )
{
  _closeSelection = closeSelection;
  _newChildren = _fragment->GetModFragments();
  _newMood = _fragment->GetMood();
}

void SegmentUiUndo::Undo()
{
  _fragment->CopyUiFragments( _oldChildren );
  _fragment->SetMood( _oldMood );
}

#endif