#if _ENABLE_CONVERSATION

#include "El/elementpch.hpp"
#include "El/Modules/modules.hpp"
#include "El/ParamArchive/paramArchive.hpp"

#include "TimeInterval.hpp"

DEFINE_FAST_ALLOCATOR( GameDataTime )

RString GameDataTime::GetText() const
{
  return "Time";
}

bool GameDataTime::IsEqualTo( const GameData *data ) const
{
  const TimeInterval &val1 = GetTime();
  const TimeInterval &val2 = static_cast<const GameDataTime *>( data )->GetTime();
  return val1 == val2;
}

LSError GameDataTime::Serialize( ParamArchive &ar )
{
  CHECK( base::Serialize( ar ) );
  return LSOK;
}

GameData *CreateGameDataTime()
{
  return new GameDataTime();
}

INIT_MODULE( TimeIntervalMain, 3 )
{
  GGameState.NewType( "TIME", GameTime, CreateGameDataTime, "Time" );
};

#endif
