
#ifndef _KNOWLEDGEBASEDEFAULT_HPP
#define _KNOWLEDGEBASEDEFAULT_HPP

#include "El/elementpch.hpp"
#include "El/Enum/dynEnum.hpp"
#include "El/ParamFile/paramFile.hpp"

#include "KnowledgeBase.hpp"

//! Information stored in ParamFile to the every word.
struct WordInfo
{
  //! Text of the word.
  RStringB _text;

  //! Put space in front of this sentence atom?
  bool _spaceInFront;

  //! No space behind this sentence atom? (overrides _spaceInFront)
  bool _neverSpaceBehind;
  
  //! Word class (to read only necessary forms; and to check the required form)
  short _wordClass;
  
  //! Form of the verb in present tense, singular, 1st person.
  RStringB _verbPresentTenseSingular1;

  //! Form of the verb in present tense, singular, 3rd person.
  RStringB _verbPresentTenseSingular3;
  
  //! Form of the verb in present tense.
  RStringB _verbPresentTense;

  //! Form of the verb in past tense, singular.
  RStringB _verbPastTenseSingular;
  
  //! Form of the verb in past tense.
  RStringB _verbPastTense;
  
  //! Form of the verb in past participle.
  RStringB _verbPastParticiple;
  
  //! Form of the verb in gerund.
  RStringB _verbGerund;
  
  //! Code of the noun gender.
  short _nounGender;
  
  //! Form of the noun in plural.
  RStringB _nounPlural;
  
  //! Personal form of the demonstrative pronoun.
  RStringB _pronounPersonal;
  
  //! Article used with this noun.
  WordBaseId _nounArticle;
  
  //! Semantic key which is language independent.
  RStringB _semanticKey;
};
TypeIsMovableZeroed( WordInfo );

//! Implementation of word dictionary.
class WordBaseDefault : public WordBase
{
protected:
  DynEnumCS _wordIds;
  AutoArray< WordInfo > _wordInfo;

public:
  WordBaseDefault( const ParamEntryVal &words )
  {
    int n = words.GetEntryCount();
    _wordInfo.Realloc( n );
    _wordInfo.Resize( n );
    for( int i = 0; i < n; ++i )
    {
    	ParamEntryVal v = words.GetEntry( i );
    	int ni = _wordIds.AddValue( v.GetName() );
    	_wordInfo.Access( ni );
    	_wordInfo[ ni ]._text = ( v >> "text" ).GetValue();
    	
    	// Read "space in front".
    	ConstParamEntryPtr entry = v.FindEntry( "spaceInFront" );
    	if( entry )
    	{
    	  _wordInfo[ ni ]._spaceInFront = entry->GetInt() != 0;
    	}
    	else
    	{
    	  _wordInfo[ ni ]._spaceInFront = 1;
    	}
    	
      // Read "never space behind".
      entry = v.FindEntry( "neverSpaceBehind" );
      if( entry )
      {
        _wordInfo[ ni ]._neverSpaceBehind = entry->GetInt() != 0;
      }
      else 
      {
        _wordInfo[ ni ]._neverSpaceBehind = false;
      }

      // Read "word class".
      _wordInfo[ ni ]._wordClass = ( v >> "wordClass" ).GetInt();

      // Read "the singular first person present tense verb form".
      entry = v.FindEntry( "verbPresentTenseSingular1" );
      if( entry )
      {
        _wordInfo[ ni ]._verbPresentTenseSingular1 = entry->GetValue();
      }

      // Read "the singular third person present tense verb form".
      entry = v.FindEntry( "verbPresentTenseSingular3" );
      if( entry )
      {
        _wordInfo[ ni ]._verbPresentTenseSingular3 = entry->GetValue();
      }

      // Read "the plural present tense verb form (also 2nd person singular)".
      entry = v.FindEntry( "verbPresentTense" );
      if( entry )
      {
        _wordInfo[ ni ]._verbPresentTense = entry->GetValue();
      }
      
      // Read "past tense singular verb form".
      entry = v.FindEntry( "verbPastTenseSingular" );
      if( entry )
      {
        _wordInfo[ ni ]._verbPastTenseSingular = entry->GetValue();
      }

      // Read "past tense verb form".
      entry = v.FindEntry( "verbPastTense" );
      if( entry )
      {
        _wordInfo[ ni ]._verbPastTense = entry->GetValue();
      }

      // Read "past participle verb form".
      entry = v.FindEntry( "verbPastParticiple" );
      if( entry )
      {
        _wordInfo[ ni ]._verbPastParticiple = entry->GetValue();
      }

      // Read "gerund form".
      entry = v.FindEntry( "verbGerund" );
      if( entry )
      {
        _wordInfo[ ni ]._verbGerund = entry->GetValue();
      }

      entry = v.FindEntry( "nounGender" );
      if( entry )
      {
        _wordInfo[ ni ]._nounGender = entry->GetInt();
      }
      else
      {
        _wordInfo[ ni ]._nounGender = 0;
      }

      // Read noun plural.
      entry = v.FindEntry( "nounPlural" );
      if( entry )
      {
        _wordInfo[ ni ]._nounPlural = entry->GetValue();
      }

      // Read personal pronoun.
      entry = v.FindEntry( "pronounPersonal" );
      if( entry )
      {
        _wordInfo[ ni ]._pronounPersonal = entry->GetValue();
      }

      // Read noun article.
      entry = v.FindEntry( "nounArticle" );
      _wordInfo[ ni ]._nounArticle = !entry ? -1 : ConvertNameToId( entry->GetValue() );
      
      // Read semantic key.
      entry = v.FindEntry( "semanticKey" );
      if( entry )
      {
        _wordInfo[ ni ]._semanticKey = entry->GetValue();
      }
    }
    
    _wordIdScalarId = ConvertNameToId( "WordIdScalarId" );
    AssertDebug( _wordIdScalarId != -1 );
    _wordIdCustomId = ConvertNameToId( "WordIdCustomId" );
    AssertDebug( _wordIdCustomId != -1 );
    _wordIdVariableId = ConvertNameToId( "WordIdVariableId" );
    AssertDebug( _wordIdVariableId != -1 );

    _wordIdCancel = ConvertNameToId( "WordIdCancel" );
    AssertDebug( _wordIdCancel != -1 );

    _wordIdPeriodId = ConvertNameToId( "WordIdPeriodId" );
    AssertDebug( _wordIdPeriodId != -1 );
    _wordIdCommaId = ConvertNameToId( "WordIdCommaId" );
    AssertDebug( _wordIdCommaId != -1 );
    _wordIdQuestionMarkId = ConvertNameToId( "WordIdQuetionMarkId" );
    AssertDebug( _wordIdQuestionMarkId != -1 );
    _wordIdExclamationMarkId = ConvertNameToId( "WordIdExclamationMarkId" );
    AssertDebug( _wordIdExclamationMarkId != -1 );
    _wordIdLeftBracketId = ConvertNameToId( "WordIdLeftBracketId" );
    AssertDebug( _wordIdLeftBracketId != -1 );
    _wordIdRightBracketId = ConvertNameToId( "WordIdRightBracketId" );
    AssertDebug( _wordIdRightBracketId != -1 );
    _wordIdAnd = ConvertNameToId( "WordIdAnd" );
    AssertDebug( _wordIdAnd != -1 );

    _wordIdMinute = ConvertNameToId( "WordIdMinute" );
    AssertDebug( _wordIdMinute != -1 );
    _wordIdHour = ConvertNameToId( "WordIdHour" );
    AssertDebug( _wordIdHour != -1 );
    _wordIdDay = ConvertNameToId( "WordIdDay" );
    AssertDebug( _wordIdDay != -1 );

    _wordIdNorth = ConvertNameToId( "WordIdNorth" );
    AssertDebug( _wordIdNorth != -1 );
    _wordIdSouth = ConvertNameToId( "WordIdSouth" );
    AssertDebug( _wordIdSouth != -1 );
    _wordIdWest = ConvertNameToId( "WordIdWest" );
    AssertDebug( _wordIdWest != -1 );
    _wordIdEast = ConvertNameToId( "WordIdEast" );
    AssertDebug( _wordIdEast != -1 );
    _wordIdNorthEast = ConvertNameToId( "WordIdNorthEast" );
    AssertDebug( _wordIdNorthEast != -1 );
    _wordIdNorthWest = ConvertNameToId( "WordIdNorthWest" );
    AssertDebug( _wordIdNorthWest != -1 );
    _wordIdSouthEast = ConvertNameToId( "WordIdSouthEast" );
    AssertDebug( _wordIdSouthEast != -1 );
    _wordIdSouthWest = ConvertNameToId( "WordIdSouthWest" );
    AssertDebug( _wordIdSouthWest != -1 );

    _wordIdMeter = ConvertNameToId( "WordIdMeter" );
    AssertDebug( _wordIdMeter != -1 );
    _wordIdKilometer = ConvertNameToId( "WordIdKilometer" );
    AssertDebug( _wordIdKilometer != -1 );
    _wordIdKmph = ConvertNameToId( "WordIdKmph" );
    AssertDebug( _wordIdKmph != -1 );
    
    _wordIdI = ConvertNameToId( "WordIdI" );
    AssertDebug( _wordIdI != -1 );
    _wordIdYou = ConvertNameToId( "WordIdYou" );
    AssertDebug( _wordIdYou != -1 );
    _wordIdHe = ConvertNameToId( "WordIdHe" );
    AssertDebug( _wordIdHe != -1 );
    _wordIdShe = ConvertNameToId( "WordIdShe" );
    AssertDebug( _wordIdShe != -1 );
    _wordIdIt = ConvertNameToId( "WordIdIt" );
    AssertDebug( _wordIdIt != -1 );
    _wordIdWe = ConvertNameToId( "WordIdWe" );
    AssertDebug( _wordIdWe != -1 );
    _wordIdThey = ConvertNameToId( "WordIdThey" );
    AssertDebug( _wordIdThey != -1 );
  }

  virtual ~WordBaseDefault()
  {
  }

  virtual WordBaseId ConvertNameToId( const char *name ) const
  {
    WordBaseId id = _wordIds.GetValue( name );
    // AssertDebug( id >= 0 );
    return id;
  }
  
  virtual WordBaseId ConvertTextToId( const RStringB &text ) const
  {
    int n = _wordInfo.Size();
    int i = 0;
    while( i < n )
    {
    	if( _wordInfo[ i ]._text != text )
    	{
    	  ++i;
    	  continue;
    	}
    	return i;
    }
    return -1;
  }

  virtual void GetWordText( WordBaseId id, long form, RString &text ) const
  {
    const WordInfo &wi = _wordInfo[ id ];
    if( ( wi._wordClass & FORM_NOUN ) != 0 )
    {
      if( ( form & FORM_PLURAL ) != 0 )
      {
        // Plural.
        if( wi._nounPlural.GetRefCount() != 0 )
        {
          // Irregular.
          text = wi._nounPlural;
          return;
        }
        // Regular.
        int last = wi._text.GetLength() - 1;
        if( last > 0 )
        {
          switch( wi._text[ last ] )
          {
          case 's':
          case 'x':
          case 'o': // -s, -ss, -x, -o
            text = RString( wi._text, "es" );
            return;

          case 'h':
            switch( wi._text[ last - 1 ] )
            {
            case 's':
            case 'c': // -sh, -ch
              text = RString( wi._text, "es" );
              return;
            }
            break;

          case 'y': // -y
            if( !IsVowel( wi._text[ last - 1 ] ) )
            {
              text = RString( wi._text.Data(), last );
              text = RString( text, "ies" );
              return;
            }
            break;
          }
        }
        // classic
        text = RString( wi._text, "s" );
        return;
      }
    }
    if( ( wi._wordClass & FORM_VERB ) != 0 )
    {
      if( ( form & FORM_PRESENT_TENSE ) != 0 )
      {
        // Present tense.
        if( ( form & FORM_PLURAL ) == 0 )
        {
          // Singular.
          if( ( form & FORM_FIRST_PERSON ) != 0 )
          {
            if( wi._verbPresentTenseSingular1.GetRefCount() != 0 )
            {
              // Irregular.
              text = wi._verbPresentTenseSingular1;
              return;
            }
            else if( wi._verbPresentTense.GetRefCount() != 0 )
            {
              // Irregular.
              text = wi._verbPresentTense;
              return;
            }
            else
            {
              // Regular.
              text = wi._text;
              return;
            }
          }
          else if( ( form & FORM_THIRD_PERSON ) != 0 )
          {
            if( wi._verbPresentTenseSingular3.GetRefCount() != 0 )
            {
              // Irregular.
              text = wi._verbPresentTenseSingular3;
              return;
            }
            else if( wi._verbPresentTense.GetRefCount() != 0 )
            {
              // Irregular.
              text = wi._verbPresentTense;
              return;
            }
            else
            {
              // Regular.
              int last = wi._text.GetLength() - 1;
              if( last > 0 )
              {
                switch( wi._text[ last ] )
                {
                case 'o':
                case 'x':
                case 's': // -o, -s, -x, -ss
                  text = RString( wi._text, "es" );
                  return;

                case 'h':
                  switch( wi._text[ last - 1 ] )
                  {
                  case 's':
                  case 'c': // -sh, -ch
                    text = RString( wi._text, "es" );
                    return;
                  }
                  break;

                case 'y': // -y
                  if( !IsVowel( wi._text[ last - 1 ] ) )
                  {
                    text = RString( wi._text.Data(), last );
                    text = RString( text, "ies" );
                    return;
                  }
                  break;
                }
              }
              // classic
              text = RString( text, "s" );
              return;
            }
          }
          else // FORM_SECOND_PERSON
          {
            if( wi._verbPresentTense.GetRefCount() != 0 )
            {
              // Irregular.
              text = wi._verbPresentTense;
              return;
            }
            else
            {
              // Regular.
              text = wi._text;
              return;
            }
          }
        }
        else
        {
          // Plural.
          if( wi._verbPresentTense.GetRefCount() != 0 )
          {
            // Irregular.
            text = wi._verbPresentTense;
            return;
          }
          else
          {
            // Regular.
            text = wi._text;
            return;
          }
        }
      }
      else if( ( form & FORM_PAST_TENSE ) != 0 )
      {
        if( ( form & FORM_PLURAL ) == 0 && 
          ( form & ( FORM_FIRST_PERSON | FORM_THIRD_PERSON ) ) != 0 )
        {
          // Singular.
          if( wi._verbPastTenseSingular.GetRefCount() != 0 )
          {
            // Irregular.
            text = wi._verbPastTenseSingular;
            return;
          }
        }
        if( wi._verbPastTense.GetRefCount() != 0 )
        {
          // Irregular.
          text = wi._verbPastTense;
          return;
        }
        // Regular.
        int last = wi._text.GetLength() - 1;
        if( last > 0 )
        {
          switch( wi._text[ last ] )
          {
          case 'e': // -e, -ie
            text = RString( wi._text, "d" );
            return;

          case 'l': // -l
            if( IsVowel( wi._text[ last - 1 ] ) )
            {
              text = RString( text, "led" );
              return;
            }
            break;

          case 'y': // -y
            if( !IsVowel( wi._text[ last - 1 ] ) )
            {
              text = RString( wi._text.Data(), last );
              text = RString( text, "ied" );
              return;
            }
            break;

          case 'c': // -ic
            if( wi._text[ last - 1 ] == 'i' )
            {
              text = RString( wi._text, "ked" );
              return;
            }
            break;

          case 'r': // -r
            text = RString( wi._text, "red" );
            return;
          }
        }
        
        text = RString( wi._text, "ed" );
        return;
      }
      else if( ( form & FORM_PAST_PARTICIPLE ) != 0 )
      {
        if( wi._verbPastParticiple.GetRefCount() != 0 )
        {
          // Irregular.
          text = wi._verbPastParticiple;
          return;
        }
        // Regular.
        int last = wi._text.GetLength() - 1;
        if( last > 0 )
        {
          switch( wi._text[ last ] )
          {
          case 'e': // -e, -ie
            text = RString( wi._text, "d" );
            return;

          case 'l': // -l
            if( IsVowel( wi._text[ last - 1 ] ) )
            {
              text = RString( text, "led" );
              return;
            }
            break;

          case 'y': // -y
            if( !IsVowel( wi._text[ last - 1 ] ) )
            {
              text = RString( wi._text.Data(), last );
              text = RString( text, "ied" );
              return;
            }
            break;

          case 'c': // -ic
            if( wi._text[ last - 1 ] == 'i' )
            {
              text = RString( wi._text, "ked" );
              return;
            }
            break;

          case 'r': // -r
            text = RString( wi._text, "red" );
            return;
          }
        }

        text = RString( wi._text, "ed" );
        return;
      }
      else if( ( form & FORM_GERUND ) != 0 )
      {
        if( wi._verbGerund.GetRefCount() != 0 )
        {
          // Irregular.
          text = wi._verbGerund;
          return;
        }
        
        // Regular.
        int last = wi._text.GetLength() - 1;
        if( last > 0 )
        {
          switch( wi._text[ last ] )
          {
          case 'e': // -e, -ie
            if( wi._text[ last - 1 ] == 'i' )
            {
              text = RString( wi._text.Data(), last - 1 );
              text = RString( wi._text, "ying" );
              return;
            }
            text = RString( wi._text.Data(), last );
            text = RString( text, "ing" );
            return;
          }
        }
        text = RString( wi._text, "ing" );
        return;
      }
      else if( ( form & ( FORM_CONDITIONAL_MOOD | FORM_FUTURE_TENSE ) ) != 0 )
      {
        text = wi._text;
        return;
      }
    }
    if( ( wi._wordClass & FORM_PRONOUN ) != 0 )
    {
      if( ( form & FORM_PERSONAL ) != 0 )
      {
        if( wi._pronounPersonal.GetRefCount() != 0 )
        {
          text = wi._pronounPersonal;
          return;
        }
        
        text = RString( wi._text, "'s" );
        return;
      }
    }

    text = wi._text;
  }

  virtual short GetWordClass( WordBaseId id ) const
  {
    return _wordInfo[ id ]._wordClass;
  }
  
  virtual short GetNounGender( WordBaseId id ) const
  {
    return _wordInfo[ id ]._nounGender;
  }
  
  //! Get all possible demonstrative pronouns of this dictionary.
  virtual void GetDemonstrativePronouns( AutoArray< WordBaseId > &pronouns ) const
  {
    pronouns.Add( _wordIdI );
    pronouns.Add( _wordIdYou );
    pronouns.Add( _wordIdHe );
    pronouns.Add( _wordIdShe );
    pronouns.Add( _wordIdIt );
    pronouns.Add( _wordIdWe );
    pronouns.Add( _wordIdThey );
  }
  
  //! Get all possible demonstrative pronouns of this dictionary without "I".
  virtual void GetDemonstrativePronounsWithoutI( AutoArray< WordBaseId > &pronouns ) const
  {
    pronouns.Add( _wordIdYou );
    pronouns.Add( _wordIdHe );
    pronouns.Add( _wordIdShe );
    pronouns.Add( _wordIdIt );
    pronouns.Add( _wordIdWe );
    pronouns.Add( _wordIdThey );
  }
  
  virtual WordBaseId GetDemonstrativePronounOfGender( short gender, long form ) const
  {
    switch( gender )
    {
    case GENDER_HE:
      return ( form & FORM_PLURAL ) == 0 ? _wordIdHe : _wordIdThey;
    	
    case GENDER_SHE:
      return ( form & FORM_PLURAL ) == 0 ? _wordIdShe : _wordIdThey;

    case GENDER_IT:
      return ( form & FORM_PLURAL ) == 0 ? _wordIdIt : _wordIdThey;
    
    default:
      return -1;
    }
  }

  //! Converts outcoming demonstrative pronoun to incoming.
  virtual void SwitchIncomingAndOutcoming( WordBaseId incoming, AutoArray< WordBaseId > &outcoming ) const
  {
    if( incoming == _wordIdI )
    {
    	outcoming.Add( _wordIdYou );
    }
    else if( incoming == _wordIdYou )
    {
    	outcoming.Add( _wordIdI );
    	outcoming.Add( _wordIdWe );
    }
    else if( incoming == _wordIdWe )
    {
      outcoming.Add( _wordIdYou );
    }
    else
    {
      outcoming.Add( incoming );
    }
  }

  virtual bool HasSpaceInFront( WordBaseId id ) const
  {
    return _wordInfo[ id ]._spaceInFront;
  }
  
  virtual bool HasNeverSpaceBehind( WordBaseId id ) const
  {
    return _wordInfo[ id ]._neverSpaceBehind;
  }
  
  virtual void GetSemanticKey( WordBaseId id, RString &key ) const
  {
    const WordInfo &wi = _wordInfo[ id ];
    AssertDebug( wi._semanticKey.GetRefCount() != 0 );
    key = wi._semanticKey.GetRefCount() != 0 ? wi._semanticKey : wi._text;
  }
  
  virtual short GetWordOrder( PartOfSentence pos ) const
  {
    static const short wordOrder[ POS_Size ] = { 0, 1, 2 };
    return wordOrder[ pos ];
  }

  virtual WordBaseId GetHypernym( WordBaseId wordId, long form ) const
  {
    //... It just might be changed one day.
    return WordBaseDefault::GetDemonstrativePronounOfGender( WordBaseDefault::GetNounGender( wordId ), form );
  }

  static inline bool IsVowel( char c )
  {
    return c == 'a' || c == 'e' || c == 'i' || c == 'o' || c == 'u' || c == 'y';
  }
};

#endif