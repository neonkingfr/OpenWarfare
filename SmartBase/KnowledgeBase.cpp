#if _ENABLE_CONVERSATION

#include "El/elementpch.hpp"
#include "El/Modules/modules.hpp"

#include "KnowledgeBase.hpp"
#include "SemanticNet.hpp"

void KnowledgeBase::SetSemanticNet( SemanticNet *semanticNet )
{
  _semanticNet = semanticNet;
}

GameData *CreateGameDataKnowledge()
{
  return new GameDataKnowledge();
}

DEFINE_FAST_ALLOCATOR( GameDataKnowledge )

RString GameDataKnowledge::GetText() const
{
  if( _knowledge.IsNull() )
  {
    return "<NULL-object>";
  }

  return "Knowledge";
}

bool GameDataKnowledge::IsEqualTo( const GameData *data ) const
{
  const SemanticKnowledge *k1 = GetKnowledge();
  const SemanticKnowledge *k2 = static_cast< const GameDataKnowledge* >( data )->GetKnowledge();
  return k1 == k2;
}

LSError GameDataKnowledge::Serialize( ParamArchive &ar )
{
  LSError err = base::Serialize( ar );
  if( err != 0 )
  {
    return err;
  }
  return ( LSError ) 0;
}

GameData *CreateGameDataKnowledgeBase()
{
  return new GameDataKnowledgeBase();
}

DEFINE_FAST_ALLOCATOR( GameDataKnowledgeBase )

RString GameDataKnowledgeBase::GetText() const
{
  if( _participant.IsNull() )
  {
    return "<NULL-object>";
  }

  return "KnowledgeBase";
}

bool GameDataKnowledgeBase::IsEqualTo( const GameData *data ) const
{
  const KnowledgeBase *p1 = GetParticipant();
  const KnowledgeBase *p2 = static_cast< const GameDataKnowledgeBase* >( data )->GetParticipant();
  return p1 == p2;
}

LSError GameDataKnowledgeBase::Serialize( ParamArchive &ar )
{
  LSError err = base::Serialize( ar );
  if( err != 0 )
  {
    return err;
  }
  return ( LSError ) 0;
}

INIT_MODULE( KnowledgeBaseMain, 3 )
{
  GGameState.NewType( "KNOWLEDGE", GameKnowledge, CreateGameDataKnowledge, "Knowledge" );
  GGameState.NewType( "KNOWLEDGEBASE", GameKnowledgeBase, CreateGameDataKnowledgeBase, "KnowledgeBase" );
};

#endif