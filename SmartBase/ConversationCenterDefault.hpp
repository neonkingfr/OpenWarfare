
#ifndef _CONVERSATIONCENTERDEFAULT_HPP
#define _CONVERSATIONCENTERDEFAULT_HPP

#include <Windows.h>
#include "ConversationCenter.hpp"
#include "KnowledgeBaseDefault.hpp"
#include "SemanticNetDefault.hpp"
#include "SentenceRuleDefault.hpp"

TypeIsSimpleZeroed( ParamFile* );

/*! Class providing information in dialog initialization. Player can choose who does he want to talk to.
    KnowledgeBases of the available conversation participants are stored in files represented by 
    wildcard mask passed to the class constructor.
*/
class ConversationCenterDefault : public ConversationCenter
{
protected:
  AutoArray< RString > _knowledgeBases;
  AutoArray< RString > _names;
  ParamEntryVal _rules;
  ParamEntryVal _responses;
  const RString _rulesRoot;
  Ref< WordBase > _wordBase;
  ParamEntryVal _globalKnowledges;
  ParamEntryVal _playerQuestions;
  AutoArray< ParamFile* > _knowledgeStores;
  
public:
  virtual void GetParticipantName( int i, RString &name )
  {
    name = _names[ i ];
  }
  
  //! Finds available conversation participants.
  ConversationCenterDefault( const ParamEntryVal &globalKnowledges, 
    const ParamEntryVal &words, const ParamEntryVal &rules, const ParamEntryVal &responses, 
    const RString &dir, const RString &wildcard, const RString &rulesRoot, 
    const ParamEntryVal &playerQuestions, ConstParamEntryPtr initScript )
    : _globalKnowledges( globalKnowledges ), _rules( rules ), _rulesRoot( rulesRoot ), 
    _responses( responses ), _playerQuestions( playerQuestions )
  {
    _wordBase = new WordBaseDefault( words );
    AssertDebug( _wordBase.NotNull() );

    WIN32_FIND_DATAA findFileData;
    HANDLE find;
    RString wildcardDir( dir, wildcard );

    find = FindFirstFileA( wildcardDir, &findFileData);
    if( find != INVALID_HANDLE_VALUE )
    {
      do {
        _names.Add( findFileData.cFileName );
        _knowledgeBases.Add( RString( dir, findFileData.cFileName ) );
      } while( FindNextFileA( find, &findFileData ) );
    }
    FindClose( find );
    
    _knowledgeStores.Resize( _knowledgeBases.Size() );
    _knowledgeStores.Realloc( _knowledgeBases.Size() );
    
    if( initScript )
    {
      // Execute init script stored in spec rules.
      GGameState.Execute( initScript->GetValue() );
      AssertDebug( GGameState.GetLastError() == EvalOK );
    }
  }
  
  virtual ~ConversationCenterDefault()
  {
    DeleteKnowledgeStores();
  }
  
  inline void DeleteKnowledgeStores()
  {
    int n = _knowledgeStores.Size();
    for( int i = 0; i < n; ++i )
    {
    	ParamFile *&store = _knowledgeStores[ i ];
    	if( store != NULL )
    	{
    	  store->Clear();
    	  delete store;
    	  store = NULL;
    	}
    }
  }

  //! Get KnowledgeBase of particular conversation participant.
  virtual void GetParticipant( int i, Ref< KnowledgeBase > &kb )
  {
    kb = new KnowledgeBase();
    AssertDebug( kb.NotNull() );

    AssertDebug( _access( _knowledgeBases[ i ], 04 ) == 0 );
    ParamFile *&knowledgeStore = _knowledgeStores[ i ];
    if( knowledgeStore != NULL )
    {
      delete knowledgeStore;
    }
    knowledgeStore = new ParamFile();
    knowledgeStore->Parse( _knowledgeBases[ i ] );
    
    ConstParamEntryPtr pq = knowledgeStore->FindEntry( "playerQuestions" );
    if( pq )
    {
      pq = _playerQuestions.FindEntry( pq->GetValue() );
    }

    kb->SetSemanticNet( new SemanticNetDefault( *knowledgeStore, _globalKnowledges, _wordBase, 
      _responses, pq ) );
    kb->SetWordBase( _wordBase );
  }

  //! Get the number of the available conversation participants.
  virtual int GetParticipantsSize()
  {
    return _knowledgeBases.Size();
  }
  
  virtual void GetSentenceRuleRoot( const KnowledgeBase *player, const RefArray< KnowledgeBase > &participants,
    Ref< SentenceFragment > &root )
  {
    root = new SentenceFragmentDefault( player, _rules, _rulesRoot );
    AssertDebug( root.NotNull() );
    
    RefArray< SentenceRule > specRules;
    int n = participants.Size();
    for( int i = 0; i < n; ++i )
    {
    	participants[ i ]->GetSemanticNet()->GetSpecRules( player, specRules );
    }
    root->SetSpecRules( specRules );
  }

  virtual WordBase *GetWordBase()
  {
    return _wordBase;
  }
};

#endif