
#ifndef _SENTENCEUI_HPP
#define _SENTENCEUI_HPP

#include "SentenceRule.hpp"
#include "Es/essencepch.hpp"
#include "Es/Containers/forEach.hpp"

class SentenceUiFragment;

//! Class of sentence fragment undo.
class SegmentUiUndo : public RefCount
{
protected:
  //! Where new fragments were added.
  Ref< SentenceUiFragment > _fragment;

  //! Fragments that were added.
  RefArray< SentenceUiFragment > _newChildren;

  //! New mood of the parent fragment.
  short _newMood;

  //! Fragments that were removed.
  RefArray< SentenceUiFragment > _oldChildren;
  
  //! Current mood of the parent fragment (will be changed by the rule application).
  short _oldMood;

  //! Selection in open undo time.
  Ref< SentenceUiFragment > _closeSelection;
  
public:
  //! Set open info for this undo.
  void SetOpenInfo( SentenceUiFragment *fragment );

  //! Set close info for this undo.
  void SetCloseInfo( SentenceUiFragment *closeSelection );
  
  inline SentenceUiFragment *GetCloseSelection()
  {
    return _closeSelection;
  }

  //! Do UNDO.
  void Undo();

  //! Empty undos aren't counted to enable UNDO.
  inline bool IsEmpty()
  {
    if( _oldMood != _newMood )
    {
      return false;
    }
    int n = _oldChildren.Size();
    if( n != _newChildren.Size() )
    {
      return false;
    }
    for( int i = 0; i < n; ++i )
    {
      if( _oldChildren[ i ] != _newChildren[ i ] )
      {
        return false;
      }
    }
    return true;
  }
};

//! Segment (node with history) of hierarchical sentence structure.
class SentenceUiSegment : public RefCount
{
protected:
  //! Available undos in this segment.
  RefArray< SegmentUiUndo > _undos;

  //! Current opened undo object. Not yet in the list of available undos.
  Ref< SegmentUiUndo > _openedUndo;

public:
  SentenceUiSegment()
  {
  }

  virtual ~SentenceUiSegment()
  {
  }

  inline void ResetUiSegment()
  {
    _undos.Clear();
  }

  bool CanUndo() const
  {
    return _undos.Size() != 0;
  }

  //! Return number of available undos.
  int GetHistoryLength() const
  {
    return _undos.Size();
  }

  void Undo( SentenceUiFragment *&selectFragment )
  {
    AssertDebug( _openedUndo.NotNull() );
    bool isEmpty = _openedUndo->IsEmpty();
    if( !isEmpty )
    {
      _openedUndo->Undo();
    }
    AssertDebug( _undos.Size() != 0 );
    int n = _undos.Size();
    if( n != 0 )
    {
      _openedUndo = _undos[ --n ];
      _undos.DeleteAt( n );
      SentenceUiFragment *newSelection = _openedUndo->GetCloseSelection();
      if( !isEmpty || selectFragment != newSelection || n == 0 )
      {
        selectFragment = newSelection;
        return;
      }
      // If opened undo was empty and selection would be the same as now, 
      // then undo next step to change something.
      _openedUndo->Undo();
      _openedUndo = _undos[ --n ];
      _undos.DeleteAt( n );
      selectFragment = _openedUndo->GetCloseSelection();
      return;
    }

    selectFragment = NULL;
  }

  void OpenUndo()
  {
    AssertDebug( _openedUndo.IsNull() );
    _openedUndo = new SegmentUiUndo();
    AssertDebug( _openedUndo.NotNull() );
  }
  
  inline SegmentUiUndo *GetOpenedUndo()
  {
    return _openedUndo;
  }

  void CloseUndo( SentenceUiFragment *closeSelection )
  {
    AssertDebug( _openedUndo.NotNull() );
    _openedUndo->SetCloseInfo( closeSelection );
    if( !_openedUndo->IsEmpty() )
    {
      _undos.Add( _openedUndo );
    }
    _openedUndo.Free();
  }
};

struct MoodInfo
{
  short _mood;
  bool _upperCase;
};
TypeIsSimple( MoodInfo );

class SentenceDrawIterator;

//! Abstract class of sentence fragment displayed in Ui.
class SentenceUiFragment : public RefCount
{
protected:
  //! Which sentence fragment does this UI item represent?
  Ref< SentenceFragment > _sentenceFragment;

  //! It is a hierarchical data structure.
  SentenceUiFragment *_parentUiFragment;

  //! Child fragments.
  RefArray< SentenceUiFragment > _subUiFragments;

  //! Rectangle of this fragment (from the last layout recalculation).
  int _left, _top, _right, _bottom;

  //! Is this fragment selected (and need special highlighting)?
  bool _selected;

public:
  typedef SentenceDrawIterator DrawIterator;
  
  inline SentenceUiFragment( SentenceFragment *fragment, SentenceUiFragment *parent )
    : _sentenceFragment( fragment ), _parentUiFragment( parent ), _selected( false )
  {
  }

  virtual ~SentenceUiFragment()
  {
  }

  inline const SentenceUiFragment *GetParentUiFragment() const
  {
    return _parentUiFragment;
  }

  inline SentenceUiFragment *GetModParentUiFragment()
  {
    return _parentUiFragment;
  }

  inline const SentenceFragment *GetSentenceFragment() const
  {
    AssertDebug( _sentenceFragment.NotNull() );
    return _sentenceFragment;
  }

  inline SentenceFragment *GetModSentenceFragment()
  {
    AssertDebug( _sentenceFragment.NotNull() );
    return _sentenceFragment;
  }
  
  inline const KnowledgeBase *GetPlayer()
  {
    return _sentenceFragment->GetPlayer();
  }
  
  inline void SetChooseRule( SentenceRule *rule )
  {
    _sentenceFragment->SetChooseRule( rule );
  }
  
  inline void AddUiFragment( SentenceUiFragment *newUiFragment )
  {
    _subUiFragments.Add( newUiFragment );
    _sentenceFragment->AddFragment( newUiFragment->GetModSentenceFragment() );
  }

  inline void CopyUiFragments( const RefArray< SentenceUiFragment > &srcUiFragments )
  {
    _subUiFragments = srcUiFragments;
    RefArray< SentenceFragment > fragments;
    int n = srcUiFragments.Size();
    for( int i = 0; i < n; ++i )
    {
    	fragments.Add( srcUiFragments[ i ]->GetModSentenceFragment() );
    }
    _sentenceFragment->CopyFragments( fragments );
  }

  inline void ClearUiFragments()
  {
    _subUiFragments.Clear();
    _sentenceFragment->ClearFragments();
  }

  inline void DeleteUiFragment( int i )
  {
    _subUiFragments.DeleteAt( i );
    _sentenceFragment->DeleteFragment( i );
  }

  inline void ResetUiFragment()
  {
    _subUiFragments.Clear();
    _sentenceFragment->ResetFragment();
  }

  SentenceUiFragment *GetAtPoint( int x, int y )
  {
    int n = _subUiFragments.Size();
    if( n == 0 )
    {
      if( _left <= x && x < _right && _top <= y && y < _bottom )
      {
        return this;
      }
      else
      {
        return NULL;
      }
    }
    else
    {
      SentenceUiFragment *out;
      for( int i = 0; i < n; ++i )
      {
        if( ( out = _subUiFragments[ i ]->GetAtPoint( x, y ) ) != NULL )
        {
          return out;
        }
      }
      return NULL;
    }
  }

  inline bool HasNoSubfragment() const
  {
    return _subUiFragments.Size() == 0;
  }

  inline bool IsTerminator() const
  {
    return _sentenceFragment->IsTerminator();
  }

  inline bool HasSpaceInFront() const
  {
    return _sentenceFragment->HasSpaceInFront();
  }
  
  inline bool HasNeverSpaceBehind() const
  {
    return _sentenceFragment->HasNeverSpaceBehind();
  }

  //! Is this required branch fragment? Terminators are not required.
  inline short GetType() const
  {
    return _sentenceFragment->GetType();
  }

  inline RefArray< SentenceUiFragment > &GetModFragments()
  {
    return _subUiFragments;
  }

  //! Can be canceled if is finished and is optional.
  inline bool CanBeCanceled() const
  {
    const SentenceUiFragment *cur = this;
    while( cur != NULL )
    {
      if( cur->GetType() == NotRequired )
      {
        return true; // Also NotRequired inside brackets has <cancel> // !IsInBrackets()
      }
      cur = cur->GetParentUiFragment();
    }
    return false;
  }

  //! In brackets means the fragment is not required and is not finished (or is part of that).
  inline bool IsInBrackets() const
  {
    if( GetType() == NotRequired )
    {
      return HasNoSubfragment() || !IsFinished();
    }
    else
    {
      const SentenceUiFragment *parent = GetParentUiFragment();
      while( parent != NULL )
      {
        if( parent->GetType() == NotRequired )
        {
          return !parent->IsFinished();
        }
        parent = parent->GetParentUiFragment();
      }
      return false;
    }
  }

  void GetComplementaryConditionsRecursive( AutoArray< SentenceResultCondition > &conditions )
  {
    if( IsInBrackets() )
    {
      _sentenceFragment->GetComplementaryConditionsRecursive( conditions ); // Not recursively.
    }
    else
    {
      int n = _subUiFragments.Size();
      for( int i = 0; i < n; ++i )
      {
        _subUiFragments[ i ]->GetComplementaryConditionsRecursive( conditions );
      }
    }
  }
  
  void GetComplementarySubjectsRecursive( AutoArray< SentenceResultCondition > &conditions )
  {
    if( IsInBrackets() )
    {
      _sentenceFragment->GetComplementarySubjectsRecursive( conditions ); // Not recursively.
    }
    else
    {
      int n = _subUiFragments.Size();
      for( int i = 0; i < n; ++i )
      {
        _subUiFragments[ i ]->GetComplementarySubjectsRecursive( conditions );
      }
    }
  }

  SentenceUiFragment *GetRightPrefixInBrackets()
  {
    if( !IsTerminator() && IsInBrackets() )
    {
      return this;
    }

    int n = _subUiFragments.Size();
    for( int i = 0; i < n; ++i )
    {
      SentenceUiFragment *f = _subUiFragments[ i ]->GetRightPrefixInBrackets();
      if( f != NULL )
      {
        return f;
      }
    }

    return NULL;
  }

  SentenceUiFragment *GetLeftPostfixOutBrackets()
  {
    int i = _subUiFragments.Size();
    while( i != 0 )
    {
      --i;
      SentenceUiFragment *f = _subUiFragments[ i ]->GetLeftPostfixOutBrackets();
    	if( f != NULL )
    	{
    	  return f;
    	}
    }
    
    if( !IsTerminator() && !IsInBrackets() )
    {
      return this;
    }

    return NULL;
  }

  SentenceUiFragment *GetLeftPostfixInBrackets()
  {
    int i = _subUiFragments.Size();
    while( i != 0 )
    {
      --i;
      SentenceUiFragment *f = _subUiFragments[ i ]->GetLeftPostfixInBrackets();
      if( f != NULL )
      {
        return f;
      }
    }

    if( !IsTerminator() && IsInBrackets() )
    {
      return this;
    }

    return NULL;
  }

  inline SentenceUiFragment *GetLeftPostfixNeighbour( SentenceUiFragment *top, bool inBrackets )
  {
    if( inBrackets )
    {
      SentenceUiFragment *f = GetLeftPostfixNeighbour( true );
      if( f != NULL )
      {
        return f;
      }

      // Find last out brackets
      return top->GetLeftPostfixOutBrackets();
    }
    return GetLeftPostfixNeighbour( false );
  }

  SentenceUiFragment *GetLeftPostfixNeighbour( bool inBrackets )
  {
    // Find upward.
    SentenceUiFragment *curFrg = this;
    do {
      for( ; ; )
      {
        if( curFrg->_parentUiFragment != NULL )
        {
          RefArray< SentenceUiFragment > &frgs = curFrg->_parentUiFragment->GetModFragments();
          int i = 0;
          int n = frgs.Size();
          for( ; ; ++i )
          {
            if( i != n )
            {
              if( frgs.Get( i ) == curFrg )
              {
                break;
              }
            }
            else
            {
              return NULL;
            }
          }
          if( i != 0 )
          {
            curFrg = frgs.Get( i - 1 );
            
            // Find downward.
            int n = curFrg->_subUiFragments.Size();
            while( n != 0 )
            {
              curFrg = curFrg->_subUiFragments[ n - 1 ];
              n = curFrg->_subUiFragments.Size();
            }
            // curFrg can be also non terminator here - it is better test it at the end of the cycle.
            break;
          }
          // It is postfix - parent is processed at the end.
          curFrg = curFrg->_parentUiFragment;
          break;
        }
        else
        {
          return NULL;
        }
      }
    } while( curFrg->IsTerminator() || ( inBrackets != curFrg->IsInBrackets() ) );

    return curFrg;
  }

  inline SentenceUiFragment *GetRightPrefixNeighbour( SentenceUiFragment *top, bool inBrackets )
  {
    if( !inBrackets )
    {
      SentenceUiFragment *f = GetRightPrefixNeighbour( false );
      if( f != NULL )
      {
        return f;
      }

      // Find first in brackets
      return top->GetRightPrefixInBrackets();
    }
    return GetRightPrefixNeighbour( true );
  }

  SentenceUiFragment *GetRightPrefixNeighbour( bool inBrackets )
  {
    // First find downward - the most left non terminator.
    int n = _subUiFragments.Size();
    for( int i = 0; i < n; ++i )
    {
      SentenceUiFragment *f = _subUiFragments[ i ];
      if( !f->IsTerminator() && f->IsInBrackets() == inBrackets )
      {
        // It is prefix - parent is processed at the beginning.
        return f;
      }
    }

    // Second find upward.
    SentenceUiFragment *curFrg = this;
    do {
      for( ; ; )
      {
        if( curFrg->_parentUiFragment != NULL )
        {
          RefArray< SentenceUiFragment > &frgs = curFrg->_parentUiFragment->GetModFragments();
          int i = 0;
          int n = frgs.Size();
          for( ; ; ++i )
          {
            if( i != n )
            {
              if( frgs.Get( i ) == curFrg )
              {
                break;
              }
            }
            else
            {
              return NULL;
            }
          }
          if( i != n - 1 )
          {
            curFrg = frgs.Get( i + 1 );
            break;
          }
          curFrg = curFrg->_parentUiFragment;
        }
        else
        {
          return NULL;
        }
      }
    } while ( curFrg->IsTerminator() || inBrackets != curFrg->IsInBrackets() );

    return curFrg;
  }

  //! Is this part of sentence complete? (Required parts are also complete)
  inline bool IsFinished() const
  {
    int n;
    if( ( n = _subUiFragments.Size() ) != 0 )
    {
      for( int i = 0; i < n; ++i )
      {
        SentenceUiFragment *f = _subUiFragments[ i ];
        if( f->GetType() == Required && !f->IsFinished() )
        {
          return false;
        }
      }
      return true;
    }
    else
    {
      return GetType() != Required;
    }
  }

  inline void GetText( RString &text ) const
  {
    _sentenceFragment->GetText( text );
  }

  bool GetSentenceText( RString &text )
  {
    int n = _subUiFragments.Size();
    if( n == 0 )
    {
      if( GetType() == Terminator )
      {
        GetText( text );
        _sentenceFragment->AddPunctuation( text );
        return true;
      }
      return false;
    }
    else
    {
      RString t0, t1;
      RString space( " " );
      int i = 0;
      SentenceUiFragment *prevFrag;
      for( ;; )
      {
        if( i < n )
        {
          prevFrag = _subUiFragments[ i++ ];
          if( prevFrag->IsFinished() && prevFrag->GetSentenceText( t0 ) )
          {
            break;
          }
          continue;
        }
        else
        {
          return false;
        }
      }
      for( ;; )
      {
        SentenceUiFragment *frag;
        for( ;; )
        {
          if( i < n )
          {
            frag = _subUiFragments[ i++ ];
            if( frag->IsFinished() && frag->GetSentenceText( t1 ) )
            {
              break;
            }
            continue;
          }
          else
          {
            text = t0;
            _sentenceFragment->AddPunctuation( text );
            return true;
          }
        }
        if( !prevFrag->HasNeverSpaceBehind() && frag->HasSpaceInFront() )
        {
          t0 = RString( t0, space );
        }
        prevFrag = frag;
        t0 = RString( t0, t1 );
      }
    }
  }

  inline void Select( bool select )
  {
    _selected = select;
    int n = _subUiFragments.Size();
    for( int i = 0; i < n; ++i )
    {
    	_subUiFragments[ i ]->Select( select );
    }
  }
  
  inline bool IsSelected() const
  {
    return _selected;
  }

  inline void SetClickRect( int left, int top, int right, int bottom )
  {
    _left = left;
    _top = top;
    _right = right;
    _bottom = bottom;
  }
  
  inline void SetMood( short mood )
  {
    _sentenceFragment->SetMood( mood );
  }
  
  inline short GetMood() const
  {
    return _sentenceFragment->GetMood();
  }
  
  inline void RemoveAllInBrackets()
  {
    int n = _subUiFragments.Size();
    int i = 0;
    while( i < n )
    {
    	if( !_subUiFragments[ i ]->IsInBrackets() )
    	{
    	  ++i;
    	  continue;
    	}
    	else
    	{
    	  DeleteUiFragment( i );
    	  --n;
    	}
    }
  }
};

class SentenceDrawIterator
{
protected:
  enum DrawStates
  {
    DrawBasic = 0,
    DrawTemporary,
    DrawNotRequiredStart,
    DrawNotRequiredEnd,
    DrawNotRequiredComma,
    DrawNotRequired,
    DrawNotRequiredHierarchy,
    DrawEnd,
  };
  
  Ref< SentenceUiFragment > _start;
  Ref< SentenceUiFragment > _previous;
  Ref< SentenceUiFragment > _current;
  bool _first;
  Ref< SentenceUiFragment > _temporarySave;
  DrawStates _temporaryState;

  AutoArray< MoodInfo > _moods;
  
  RefArray< SentenceUiFragment > _notRequired;
  int _notRequiredIndex;
  Ref< SentenceUiFragment > _notRequiredStart;
  
  DrawStates _state;

public:
  inline SentenceDrawIterator( SentenceUiFragment *start )
    : _start( start ), _current( start ), _state( DrawBasic ), _notRequiredIndex( 0 )
  {
    MoodInfo m;
    m._mood = _current->GetMood();
    m._upperCase = m._mood != UNKNOWN;
    _moods.Add( m );
    GotoFirstBasicLeaf( _current );
    if( SkipBasicPosition( _current ) )
    {
      GotoNextPosition( _current );
    }
  }

  inline bool HasSpaceInFront() const
  {
    if( _previous.NotNull() )
    {
      AssertDebug( _current.NotNull() );
      return !_previous->HasNeverSpaceBehind() && _current->HasSpaceInFront();
    }
    return false;
  }

  inline operator bool() const
  {
    return _current.NotNull();
  }

  void operator ++(int)
  {
    Ref< SentenceUiFragment > current( _current );
    GotoNextPosition( current );
    _previous = _current;
    _current = current;
  }

  inline void GetText( RString &text )
  {
    AssertDebug( _current.NotNull() );
    _current->GetText( text );

    if( _state == DrawBasic )
    {
      int n = _moods.Size();
      for( int i = 0; i < n; ++i )
      {
        if( _moods[ i ]._upperCase )
        {
          SentenceAtom::Capitalize( text );
          for( ; i < n; ++i )
          {
            _moods[ i ]._upperCase = false;
          }
          break;
        }
      }
    }
  }

  inline bool IsSelected() const
  {
    AssertDebug( _current.NotNull() );
    return _current->IsSelected();
  }

  inline bool IsTerminator() const
  {
    AssertDebug( _current.NotNull() );
    return _current->IsTerminator();
  }

  inline short GetType() const
  {
    AssertDebug( _current.NotNull() );
    return _current->GetType();
  }

  inline void SetClickRect( int left, int top, int right, int bottom )
  {
    AssertDebug( _current.NotNull() );
    _current->SetClickRect( left, top, right, bottom );
  }

protected:
  inline bool SkipBasicPosition( Ref< SentenceUiFragment > &current )
  {
    if( current.NotNull() )
    {
      // The same as IsInBrackets, but this fast version just for none terminators.
      if( current->GetType() == NotRequired && ( current->HasNoSubfragment() || !current->IsFinished() ) )
      {
        _notRequired.Add( current );
        return true;
      }
    }
    return false;
  }
  
  inline void GotoNextPosition( Ref< SentenceUiFragment > &current )
  {
    switch( _state )
    {
    case DrawBasic:
      if( current != _start )
      {
        do {
          do {
            GotoNextBasicPostfix( current );
            AssertDebug( current.NotNull() );
          } while( !current->HasNoSubfragment() && current != _start );

          if( current == _start && !current->IsTerminator() )
          {
            _state = DrawNotRequiredStart;
            GotoNextPosition( current );
            break;
          }
        } while( SkipBasicPosition( current ) );
      }
      else
      {
        _state = DrawNotRequiredStart;
        GotoNextPosition( current );
      }
      break;

    case DrawTemporary:
      current = _temporarySave;
      _temporarySave.Free();
      _state = _temporaryState;
      GotoNextPosition( current );
      break;

    case DrawNotRequiredStart:
      if( _notRequired.Size() != 0 )
      {
        Ref< const KnowledgeBase > player = _start->GetPlayer();
        current = new SentenceUiFragment( 
          new SentenceFragment( player, 
            new SentenceAtom( 
              new SemanticKnowledge( player->GetWordBase()->_wordIdLeftBracketId, FORM_DEFAULT ) ), NotRequired, 0 ),
          NULL );
        _state = DrawNotRequired;
      }
      else
      {
        current.Free();
        _state = DrawEnd;
      }
      break;

    case DrawNotRequiredEnd:
      {
        Ref< const KnowledgeBase > player = _start->GetPlayer();
        current = new SentenceUiFragment( 
          new SentenceFragment( player, 
            new SentenceAtom( 
              new SemanticKnowledge( player->GetWordBase()->_wordIdRightBracketId, FORM_DEFAULT ) ), NotRequired, 0 ),
          NULL );
        _state = DrawEnd;
      }
      break;

    case DrawNotRequiredComma:
      {
        Ref< const KnowledgeBase > player = _start->GetPlayer();
        current = new SentenceUiFragment( 
          new SentenceFragment( player, 
            new SentenceAtom( 
              new SemanticKnowledge( player->GetWordBase()->_wordIdCommaId, FORM_DEFAULT ) ), NotRequired, 0 ),
          NULL );
        _state = DrawNotRequired;
      }
      break;

    case DrawNotRequired:
      {
        _notRequiredStart = _notRequired[ _notRequiredIndex++ ];
        current = _notRequiredStart;
        _state = DrawNotRequiredHierarchy;
       
        _moods.Clear();
        MoodInfo m;
        m._mood = current->GetMood();
        m._upperCase = m._mood != UNKNOWN;
        _moods.Add( m );
        
        GotoFirstNotRequiredLeaf( current );
      }
      break;
    
    case DrawNotRequiredHierarchy:
      if( current != _notRequiredStart )
      {
        do {
          GotoNextNotRequiredPostfix( current );
          AssertDebug( current.NotNull() );
        } while( !current->HasNoSubfragment() && current != _notRequiredStart );

        if( current == _notRequiredStart && !current->IsTerminator() )
        {
          _state = _notRequiredIndex < _notRequired.Size() ? DrawNotRequiredComma : DrawNotRequiredEnd;
          GotoNextPosition( current );
          break;
        }
      }
      else
      {
        _state = _notRequiredIndex < _notRequired.Size() ? DrawNotRequiredComma : DrawNotRequiredEnd;
        GotoNextPosition( current );
      }
      break;
    
    case DrawEnd:
      current.Free();
      return;
    }
  }

  inline void GotoNextBasicPostfix( Ref< SentenceUiFragment > &current )
  {
    SentenceUiFragment *parent = current->GetModParentUiFragment();
    if( parent != NULL )
    {
      RefArray< SentenceUiFragment > &arr = parent->GetModFragments();
      int n = arr.Size();
      for( int i = 0; i < n; )
      {
        if( arr[ i++ ] == current )
        {
          int lastMood = _moods.Size() - 1;
          _moods.Delete( lastMood );
          if( i != n )
          {
            current = arr[ i ];

            MoodInfo m;
            m._mood = current->GetMood();
            m._upperCase = m._mood != UNKNOWN;
            _moods.Add( m );

            GotoFirstBasicLeaf( current );
            if( SkipBasicPosition( current ) )
            {
              GotoNextPosition( current );
            }
            return;
          }
          current = parent;
          short m = _moods[ --lastMood ]._mood;
          switch( m )
          {
          case INTERROGATIVE:
            {
              _state = DrawTemporary;
              _temporaryState = DrawBasic;
              Ref< const KnowledgeBase > player = _start->GetPlayer();
              _temporarySave = current;
              current = new SentenceUiFragment( 
                new SentenceFragment( player, 
                  new SentenceAtom( 
                    new SemanticKnowledge( player->GetWordBase()->_wordIdQuestionMarkId, FORM_DEFAULT ) ), Terminator, 0 ),
                parent );
              current->Select( parent->IsSelected() );
            }
            break;

          case IMPERATIVE:
            {
              _state = DrawTemporary;
              _temporaryState = DrawBasic;
              Ref< const KnowledgeBase > player = _start->GetPlayer();
              _temporarySave = current;
              current = new SentenceUiFragment( 
                new SentenceFragment( player, 
                  new SentenceAtom( 
                    new SemanticKnowledge( player->GetWordBase()->_wordIdExclamationMarkId, FORM_DEFAULT ) ), Terminator, 0 ), 
                parent );
              current->Select( parent->IsSelected() );
            }
            break;

          case INDICATIVE:
            {
              _state = DrawTemporary;
              _temporaryState = DrawBasic;
              Ref< const KnowledgeBase > player = _start->GetPlayer();
              _temporarySave = current;
              current = new SentenceUiFragment( 
                new SentenceFragment( player, 
                  new SentenceAtom( 
                    new SemanticKnowledge( player->GetWordBase()->_wordIdPeriodId, FORM_DEFAULT ) ), Terminator, 0 ), 
                parent );
              current->Select( parent->IsSelected() );
            }
            break;
          }
          return;
        }
      }
    }
    current.Free();
  }

  inline void GotoFirstBasicLeaf( Ref< SentenceUiFragment > &current )
  {
    while( !current->HasNoSubfragment() && 
      ( current->GetType() != NotRequired || current->IsFinished() ) )
    {
      current = current->GetModFragments()[ 0 ];

      MoodInfo m;
      m._mood = current->GetMood();
      m._upperCase = m._mood != UNKNOWN;
      _moods.Add( m );
    }
  }

  inline void GotoNextNotRequiredPostfix( Ref< SentenceUiFragment > &current )
  {
    SentenceUiFragment *parent = current->GetModParentUiFragment();
    if( parent != NULL )
    {
      RefArray< SentenceUiFragment > &arr = parent->GetModFragments();
      int n = arr.Size();
      for( int i = 0; i < n; )
      {
        if( arr[ i++ ] == current )
        {
          int lastMood = _moods.Size() - 1;
          _moods.Delete( lastMood );
          if( i != n )
          {
            current = arr[ i ];

            MoodInfo m;
            m._mood = current->GetMood();
            m._upperCase = m._mood != UNKNOWN;
            _moods.Add( m );

            GotoFirstNotRequiredLeaf( current );
            return;
          }
          current = parent;
          short m = _moods[ --lastMood ]._mood;
          switch( m )
          {
          case INTERROGATIVE:
            {
              _state = DrawTemporary;
              _temporaryState = DrawNotRequiredHierarchy;
              Ref< const KnowledgeBase > player = _start->GetPlayer();
              _temporarySave = current;
              current = new SentenceUiFragment( 
                new SentenceFragment( player, 
                new SentenceAtom( 
                new SemanticKnowledge( player->GetWordBase()->_wordIdQuestionMarkId, FORM_DEFAULT ) ), Terminator, 0 ),
                parent );
              current->Select( parent->IsSelected() );
            }
            break;

          case IMPERATIVE:
            {
              _state = DrawTemporary;
              _temporaryState = DrawNotRequiredHierarchy;
              Ref< const KnowledgeBase > player = _start->GetPlayer();
              _temporarySave = current;
              current = new SentenceUiFragment( 
                new SentenceFragment( player, 
                new SentenceAtom( 
                new SemanticKnowledge( player->GetWordBase()->_wordIdExclamationMarkId, FORM_DEFAULT ) ), Terminator, 0 ), 
                parent );
              current->Select( parent->IsSelected() );
            }
            break;

          case INDICATIVE:
            {
              _state = DrawTemporary;
              _temporaryState = DrawNotRequiredHierarchy;
              Ref< const KnowledgeBase > player = _start->GetPlayer();
              _temporarySave = current;
              current = new SentenceUiFragment( 
                new SentenceFragment( player, 
                new SentenceAtom( 
                new SemanticKnowledge( player->GetWordBase()->_wordIdPeriodId, FORM_DEFAULT ) ), Terminator, 0 ), 
                parent );
              current->Select( parent->IsSelected() );
            }
            break;
          }
          return;
        }
      }
    }
    current.Free();
  }

  inline void GotoFirstNotRequiredLeaf( Ref< SentenceUiFragment > &current )
  {
    while( !current->HasNoSubfragment() )
    {
      current = current->GetModFragments()[ 0 ];

      MoodInfo m;
      m._mood = current->GetMood();
      m._upperCase = m._mood != UNKNOWN;
      _moods.Add( m );
    }
  }
};

#endif