
#ifndef _TIMEINTERVAL_HPP_
#define _TIMEINTERVAL_HPP_

#include "El/elementpch.hpp"
#include "El/Evaluator/express.hpp"

//! Struct of time interval.
struct TimeInterval
{
  float _start;
  float _end;

  inline TimeInterval()
  {
  }

  inline TimeInterval( float time )
    : _start( time ), _end( time )
  {
  }

  inline TimeInterval( float start, float end )
    : _start( start ), _end( end )
  {
  }

  inline bool operator ==( const TimeInterval &src ) const
  {
    return _start == src._start && _end == src._end;
  }

  inline bool operator !=( const TimeInterval &src ) const
  {
    return !operator ==( src );
  }

  //! Add next time interval to this time interval.
  inline void Add( const TimeInterval &src )
  {
    if( _start > src._start )
    {
      _start = src._start;
    }
    if( _end < src._end )
    {
      _end = src._end;
    }
  }
};
TypeIsSimpleZeroed( TimeInterval );

//! This is time's representation and time's type in script language.
const GameType GameTime( 0x80000 );

class GameDataTime : public GameData
{
  typedef GameData base;

  TimeInterval _value;

public:
  GameDataTime()
  {
  }

  GameDataTime( const TimeInterval &value )
    : _value( value )
  {
  }

  ~GameDataTime()
  {
  }

  GameType GetType() const
  {
    return GameTime;
  }

  const TimeInterval &GetTime() const 
  {
    return _value;
  }

  RString GetText() const;

  bool IsEqualTo( const GameData *data ) const;

  const char *GetTypeName() const 
  {
    return "time";
  }

  GameData *Clone() const 
  {
    return new GameDataTime( *this );
  }

  LSError Serialize( ParamArchive &ar );

  USE_FAST_ALLOCATOR
};

static inline const TimeInterval *GetTime( GameValuePar oper )
{
  return ( oper.GetType() == GameTime ) ? &static_cast< GameDataTime* >( oper.GetData() )->GetTime() : NULL;
}

GameData *CreateGameDataTime();

#endif
