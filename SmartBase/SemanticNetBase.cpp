#if _ENABLE_CONVERSATION

#include "El/elementpch.hpp"
#include "El/Modules/modules.hpp"
#include "El/ParamArchive/paramArchive.hpp"
#include "Es/essencepch.hpp"
#include "Es/Algorithms/qsort.hpp"

#include "SemanticNetBase.hpp"
#include "TimeInterval.hpp"
#include "PlaceArea.hpp"
#include "SentenceRuleDefault.hpp"

//! Struct for finding the result (successful candidates) of the question.
struct IndexedCandidateSuccess
{
  //! Candidate's index.
  int _index;

  //! Fuzzy AND among evaluated (selected) conditions.
  float _fuzzyAnd;

  //! Sum of priorities of successful conditions.
  float _successfulness;

  //! Number of fulfilled conditions.
  int _fulfilledConditions;
  
  //! Group binary code.
  unsigned long _conditionBits;
  
  //! Sort IndexedCandidateSuccess by its total success.
  static int CompareSuccessfulness( const IndexedCandidateSuccess *c1, const IndexedCandidateSuccess *c2 )
  {
    if( c1->_fulfilledConditions < c2->_fulfilledConditions )
    {
      return 1;
    }
    else if( c1->_fulfilledConditions > c2->_fulfilledConditions )
    {
      return -1;
    }
    if( c1->_successfulness < c2->_successfulness )
    {
      return 1;
    }
    else if( c1->_successfulness > c2->_successfulness )
    {
      return -1;
    }
    return 0;
  }

};
TypeIsSimple( IndexedCandidateSuccess );

//! Auxiliary constants.
const RStringB SemanticNetBase::_keywordValue( "_value" );
const RStringB SemanticNetBase::_keywordSingularValue( "_singularValue" );
const RStringB SemanticNetBase::_keywordQuatity( "_quantity" );
const RStringB SemanticNetBase::_keywordPluralQuatity( "_pluralQuatity" );
const RStringB SemanticNetBase::_keywordRoundedPluralQuatity( "_roundedPluralQuatity" );
const RStringB SemanticNetBase::_keywordBadValues( "_badValues" );
const RStringB SemanticNetBase::_keywordGoodValues( "_goodValues" );
const RStringB SemanticNetBase::_keywordAllValues( "_allValues" );
const RStringB SemanticNetBase::_keywordCandidate( "_candidate" );
const RStringB SemanticNetBase::_tablePlaces( "Places" );
const RStringB SemanticNetBase::_tableEntityTypes( "EntityTypes" );
const RStringB SemanticNetBase::_tableEntities( "Entities" );
const RStringB SemanticNetBase::_keywordBase( "base" );

//! Is this notion defined in the context of this semantic net?
bool SemanticNetBase::IsNotionDefined( WordBaseId notionId ) const
{
  return _semanticContext.IsOutcomingDefined( notionId );
}

//! Return the number of notions defined in this semantic net (or its context).
int SemanticNetBase::GetNotionsSize() const
{
  return _semanticContext.GetNotionsSize();
}

//! Return the notion defined in this semantic net (or its context).
const SemanticKnowledge *SemanticNetBase::GetNotion( int i ) const
{
  return _semanticContext.GetNotion( i ).GetOutcoming();
}

//! Get sentence rules specific for this semantic net.
void SemanticNetBase::GetSpecRules( const KnowledgeBase *player, RefArray< SentenceRule > &specRules ) const
{
  AssertDebug( !_specRules || _specRules->IsClass() );
  if( _specRules )
  {
    ParamEntryVal addRules = *_specRules >> "addRules";
    AssertDebug( addRules.IsArray() );
    ParamEntryVal kbRules = *_specRules >> "CfgKBRules";
    AssertDebug( kbRules.IsClass() );
    int n = addRules.GetSize();
    for( int i = 0; i < n; ++i )
    {
    	ParamEntryVal generator = kbRules >> addRules[ i ].GetValue();
      SentenceFragmentDefault::ReadGeneratorRules( player, kbRules, generator, specRules );
    }
  }
}

void SemanticNetBase::ReactConditionally( KnowledgeBase *speaker, 
  const SentenceFragment *sentence, Ref< SentenceAtom > &reaction )
{
  // Read all possible result formats.
  RefArray< const FormattedSentenceResult > formats;
  sentence->GetConditionalResultsRecursive( formats );
  int n = formats.Size();
  AssertDebug( n != 0 );

  if( n != 0 )
  {
    // Space for variables with "_me" is created in upper function (React).

    int i = 0;
    do {
      const FormattedSentenceResult *f = formats[ i ];
    	bool b = GGameState.Evaluate( f->GetCondition() );
    	AssertDebug( GGameState.GetLastError() == EvalOK );
    	if( b )
    	{
    	  // Compose reply.
        SentenceAtom::CreateBasicSentenceAtom( *speaker->GetWordBase(), reaction, UNKNOWN );
    	  ComposeResponseSentence( reaction, f->GetResponseId(), *speaker->GetWordBase() );
    	  GGameState.Execute( formats[ i ]->GetAction() );
    	  AssertDebug( GGameState.GetLastError() == EvalOK );
    	  break;
    	}
    	++i;
    } while( i < n );
  }
}

void SemanticNetBase::ReactConditionally( KnowledgeBase *speaker, const SentenceFragment *sentence, 
  Ref< SentenceAtom > &reaction, const AutoArray< SemanticQuery > &candidates, 
  const AutoArray< SentenceResultCondition > &conditions, const Array2D< GameValue > &evaluatedVariables,
  const RefArray< KnowledgeBase > &listeners )
{
  int candidatesN = candidates.Size();
  int conditionsN = conditions.Size();
  
  AssertDebug( candidatesN != 0 && candidatesN != 0 );
  if( candidatesN == 0 || candidatesN == 0 )
  {
    ReactConditionally( speaker, sentence, reaction );
    return;
  }
  
  // Read all possible result formats.
  RefArray< const FormattedSentenceResult > formats;
  sentence->GetConditionalResultsRecursive( formats );
  int formatsN = formats.Size();
  AssertDebug( formatsN != 0 );

  if( formatsN != 0 )
  {
    // Space for variables with "_me" is created in upper function (React).

    int i = 0;
    do {
      bool b = true;
      
      const FormattedSentenceResult *f = formats[ i ];

      for( int candidatesI = 0; candidatesI < candidatesN; ++candidatesI )
      {
        for( int conditionsI = 0; conditionsI < conditionsN; ++conditionsI )
        {
          const SentenceResultCondition &c = conditions[ conditionsI ];
          if( c._variableName.GetRefCount() != 0 )
          {
            GGameState.VarSet( c._variableName, evaluatedVariables.Get( conditionsI, candidatesI ) );
          }
        }

        b = static_cast< GameBoolType >( GGameState.Evaluate( f->GetCondition() ) );
        AssertDebug( GGameState.GetLastError() == EvalOK );

        for( int conditionsI = 0; conditionsI < conditionsN; ++conditionsI )
        {
          const SentenceResultCondition &c = conditions[ conditionsI ];
          if( c._variableName.GetRefCount() != 0 )
          {
            GGameState.VarDelete( c._variableName );
          }
        }
        
        if( !b )
        {
          // All candidates must pass the condition of the format to be used.
          break;
        }
      }

      if( b )
      {
        // Compose reply.
        SentenceAtom::CreateBasicSentenceAtom( *speaker->GetWordBase(), reaction, UNKNOWN );
        ComposeResponseSentence( reaction, f->GetResponseId(), *speaker->GetWordBase(), candidates );
        GGameState.Execute( formats[ i ]->GetAction() );
        AssertDebug( GGameState.GetLastError() == EvalOK );

        // Transfer information from speaker's semantic net into listener's net.
        // Define variable of the speaker ("_me") is still defined.
        SemanticQuery dstCandidate;
        AutoArray< SemanticQuery > transformedSuccessfulQueries;
        int n = listeners.Size();
        for( int i = 0; i < n; ++i )
        {
          KnowledgeBase *l = listeners[ i ];
          SemanticNetBase *lsn = static_cast< SemanticNetBase* >( l->GetModSemanticNet() );

          // Define variable of the listener.
          GGameState.VarSet( "_listener", GameValue( new GameDataKnowledgeBase( l ) ), true );

          for( int candidatesI = 0; candidatesI < candidatesN; ++candidatesI )
          {
            const SemanticQuery &srcCandidate = candidates[ candidatesI ];

            // Transform queries from the speaker's semantic net to the listener's semantic net based on 
            // semantic knowledge id.
            SemanticNetId snid = GetId( srcCandidate );
            if( snid != -1 )
            {
              if( lsn->GetIdQuery( snid, dstCandidate ) )
              {
                // Define variable of the source candidate.
                GGameState.VarSet( "_srcCandidate", GameValue( new GameDataQuery( srcCandidate ) ), true );

                // Define variable of the destination candidate.
                GGameState.VarSet( "_dstCandidate", GameValue( new GameDataQuery( dstCandidate ) ), true );

                // Transfer the information of each condition.
                for( int conditionsI = 0; conditionsI < conditionsN; ++conditionsI )
                {
                  const RString &transfer = conditions[ conditionsI ]._transfer;
                  if( transfer.GetRefCount() != 0 )
                  {
                    GGameState.Evaluate( transfer );
                    AssertDebug( GGameState.GetLastError() == EvalOK );
                  }
                }

                // Delete no more necessary variables.
                GGameState.VarDelete( "_srcCandidate" );
                GGameState.VarDelete( "_dstCandidate" );
              }
            }
          }
          GGameState.VarDelete( "_listener" );
        }
        break;
      }
      ++i;
    } while( i < formatsN );
  }
}

void SemanticNetBase::ComposeResponseSentence( Ref< SentenceAtom > &parentReaction, 
  const RString responseId, const WordBase &wb ) const
{
  AssertDebug( !_specRules || _specRules->IsClass() );
  ConstParamEntryPtr responseClass;
  if( _specRules )
  {
    ConstParamEntryPtr section = _specRules->FindEntry( "CfgKBResponses" );
    if( section )
    {
      responseClass = section->FindEntry( responseId );
    }
  }
  if( !responseClass )
  {
    responseClass = &( _responses >> responseId );
  }
  if( responseClass )
  {
    // Create new sentence of the reaction.
    Ref< SentenceAtom > reaction;
    SentenceAtom::CreateBasicSentenceAtom( wb, reaction, UNKNOWN );
    parentReaction->AddSubAtom( reaction );

    // Set mood of the response.
    ParamEntryVal mood = *responseClass >> "mood";
    if( !mood.GetPointer()->IsError() )
    {
      reaction->SetMood( mood.GetInt() );
    }

    // Add particular words.
    ParamEntryVal words = *responseClass >> "parts";
    if( words.IsArray() && !words.GetPointer()->IsError() )
    {
      int wordsSize = words.GetSize();
      for( int wordsIdx = 0; wordsIdx < wordsSize; ++wordsIdx )
      {
        const IParamArrayValue &wordElem = words[ wordsIdx ];
        if( !wordElem.IsArrayValue() )
        {
          //! Word has default form.
          if( !wordElem.IsFloatValue() && !wordElem.IsIntValue() )
          {
            // It is a text. It can be: name of WordBaseId, variable or keyword '_result'.
            RStringB value = wordElem.GetValue();

            WordBaseId wordid;
            if( ( wordid = wb.ConvertNameToId( value ) ) != -1 )
            {
              // It is a name of WordBaseId.
              reaction->AddSubAtom( new SemanticKnowledge( wordid, FORM_DEFAULT ) );
            }
            else
            {
              // Nothing else should not be here.
              AssertDebug( 0 );
            }
          }
          else
          {
            // It is a number.
            reaction->AddSubAtom( new SemanticKnowledge( 
              wb._wordIdScalarId, wordElem.GetFloat(), FORM_DEFAULT ) );
          }
        }
        else
        {
          //! Word has custom form.
          const IParamArrayValue &word = wordElem[ 0 ];
          if( !word.IsFloatValue() && !word.IsIntValue() )
          {
            // It is a text. It can be: name of WordBaseId, variable or keyword '_result'.
            RStringB value = word.GetValue();

            WordBaseId wordid;
            if( ( wordid = wb.ConvertNameToId( value ) ) != -1 )
            {
              // It is a name of WordBaseId.
              reaction->AddSubAtom( new SemanticKnowledge( wordid, wordElem[ 1 ].GetInt() ) );
            }
            else
            {
              // Nothing else should not be here.
              AssertDebug( 0 );
            }
          }
          else
          {
            // It is a number.
            reaction->AddSubAtom( new SemanticKnowledge( 
              wb._wordIdScalarId, word.GetFloat(), wordElem[ 1 ].GetInt() ) );
          }
        }
      }
    }
  }
}

void SemanticNetBase::ComposeResponseSentence( Ref< SentenceAtom > &parentReaction, 
  const RString responseId, const WordBase &wb, const AutoArray< SemanticQuery > &candidates ) const
{
  AssertDebug( !_specRules || _specRules->IsClass() );
  ConstParamEntryPtr responseClass;
  if( _specRules )
  {
    ConstParamEntryPtr section = _specRules->FindEntry( "CfgKBResponses" );
    if( section )
    {
      responseClass = section->FindEntry( responseId );
    }
  }
  if( !responseClass )
  {
    responseClass = &( _responses >> responseId );
  }
  if( responseClass )
  {
    // Create new sentence of the reaction.
    Ref< SentenceAtom > reaction;
    SentenceAtom::CreateBasicSentenceAtom( wb, reaction, UNKNOWN );
    parentReaction->AddSubAtom( reaction );

    // Set mood of the response.
    ParamEntryVal mood = *responseClass >> "mood";
    if( !mood.GetPointer()->IsError() )
    {
      reaction->SetMood( mood.GetInt() );
    }

    // Add particular words.
    ParamEntryVal words = *responseClass >> "parts";
    if( words.IsArray() && !words.GetPointer()->IsError() )
    {
      int wordsSize = words.GetSize();
      for( int wordsIdx = 0; wordsIdx < wordsSize; ++wordsIdx )
      {
        const IParamArrayValue &wordElem = words[ wordsIdx ];
        if( !wordElem.IsArrayValue() )
        {
          //! Word has default form.
          if( !wordElem.IsFloatValue() && !wordElem.IsIntValue() )
          {
            // It is a text. It can be: name of WordBaseId, variable or keyword '_result'.
            RStringB value = wordElem.GetValue();

            WordBaseId wordid;
            if( ( wordid = wb.ConvertNameToId( value ) ) != -1 )
            {
              // It is a name of WordBaseId.
              reaction->AddSubAtom( new SemanticKnowledge( wordid, FORM_DEFAULT ) );
            }
            else if( value == _keywordCandidate )
            {
              if( candidates.Size() != 0 )
              {
                Ref< SemanticKnowledge > transformed;
                _semanticContext.TransformQueriesToOutcoming( candidates, transformed );
                reaction->AddSubAtom( transformed );
              }
            }
            else
            {
              // Nothing else should not be here.
              AssertDebug( 0 );
            }
          }
          else
          {
            // It is a number.
            reaction->AddSubAtom( new SemanticKnowledge( 
              wb._wordIdScalarId, wordElem.GetFloat(), FORM_DEFAULT ) );
          }
        }
        else
        {
          //! Word has custom form.
          const IParamArrayValue &word = wordElem[ 0 ];
          if( !word.IsFloatValue() && !word.IsIntValue() )
          {
            // It is a text. It can be: name of WordBaseId, variable or keyword '_result'.
            RStringB value = word.GetValue();

            WordBaseId wordid;
            if( ( wordid = wb.ConvertNameToId( value ) ) != -1 )
            {
              // It is a name of WordBaseId.
              reaction->AddSubAtom( new SemanticKnowledge( wordid, wordElem[ 1 ].GetInt() ) );
            }
            else if( value == _keywordCandidate )
            {
              if( candidates.Size() != 0 )
              {
                Ref< SemanticKnowledge > transformed;
                _semanticContext.TransformQueriesToOutcoming( candidates, wb, wordElem[ 1 ].GetInt(), 
                  transformed );
                reaction->AddSubAtom( transformed );
              }
            }
            else
            {
              // Nothing else should not be here.
              AssertDebug( 0 );
            }
          }
          else
          {
            // It is a number.
            reaction->AddSubAtom( new SemanticKnowledge( 
              wb._wordIdScalarId, word.GetFloat(), wordElem[ 1 ].GetInt() ) );
          }
        }
      }
    }
  }
}

void SemanticNetBase::React( KnowledgeBase *speaker, const SentenceFragment *sentence, 
  AutoArray< SentenceResultCondition > &complementaryConditions, 
  AutoArray< SentenceResultCondition > &complementarySubjects, Ref< SentenceAtom > &reaction, 
  const RefArray< KnowledgeBase > &listeners )
{
  // Read script candidates.
  AutoArray< SentenceResultCandidates > ruleCandidates;
  sentence->GetCandidatesRecursive( ruleCandidates );

  // Read script candidates tables.
  AutoArray< SentenceResultCandidatesTable > ruleTables;
  sentence->GetCandidatesTablesRecursive( ruleTables );
  
  // Read script conditions.
  AutoArray< SentenceResultCondition > ruleConditions;
  sentence->GetConditionsRecursive( ruleConditions );

  // Calculate all the candidates that met the required conditions (table and script expressions).
  AutoArray< SemanticQuery > candidates;
  Array2D< GameValue > evaluatedVariables;

  // Candidate table or sentence result condition doesn't exist.
  int conditionsSize = ruleConditions.Size();
  if( ruleTables.Size() == 0 || conditionsSize == 0 )
  {
    // Conditional result.
    ReactConditionally( speaker, sentence, reaction );
    return;
  }

  // Create space for variables.
  GameVarSpace vars;
  GGameState.BeginContext(&vars);

  // Register variable of the replaying speaker.
  GGameState.VarSet( "_me", GameValue( new GameDataKnowledgeBase( speaker ) ), true );

  // Calculate all candidates.
  int ruleCandidatesSize = ruleCandidates.Size();
  if( ruleCandidatesSize != 0 )
  {
    // Candidates are given by a script expression and by the required table name.
    if( ruleTables.Size() > 0 )
    {
      // Find the required table name.
      RString candidatesFromTable = ruleTables[ 0 ]._candidatesFromTable;

      // Go through the set of script expressions determining candidates.
      for( int ruleCandidatesIdx = 0; ruleCandidatesIdx < ruleCandidatesSize; ++ruleCandidatesIdx )
      {
        SentenceResultCandidates &ruleCandidatesElem = ruleCandidates[ ruleCandidatesIdx ];
        if( ruleCandidatesElem._thisVariable.NotNull() )
        {
          // Register variable of the sentence fragment, where the expression appeared.
          GGameState.VarSet( "_this", GameValue( new GameDataKnowledge( ruleCandidatesElem._thisVariable ) ), true );
        }

        // Evaluate the script expression.
        GameArrayType scriptCandidates = GGameState.Evaluate( ruleCandidatesElem._candidates );
        AssertDebug( GGameState.GetLastError() == EvalOK );

        // Delete variable corresponding to the current sentence fragment.
        GGameState.VarDelete( "_this" );

        // Go through all candidates expressed by the script and check if they belong to the required table.
        int scriptCandidatesSize = scriptCandidates.Size();
        for( int scriptCandidatesIdx = 0; scriptCandidatesIdx < scriptCandidatesSize; ++scriptCandidatesIdx )
        {
          GameValue &candidateValue = scriptCandidates[ scriptCandidatesIdx ];
          if( candidateValue.GetType() == GameQuery )
          {
            const SemanticQuery &candidateQuery = static_cast< GameDataQuery* >( candidateValue.GetData() )->GetQuery();
            if( candidateQuery.GetTable() == candidatesFromTable )
            {
              candidates.Add( candidateQuery );
            }
          }
        }
      }
    }
  }
  else if( ruleTables.Size() > 0 )
  {
    // Candidates are determined only by the required table name.
    RString candidatesFromTable = ruleTables[ 0 ]._candidatesFromTable;

    // Go through whole table.
    int candidatesSize = GetKnowledgesSize( candidatesFromTable );
    SemanticQuery candidate( candidatesFromTable );
    for( int candidateIdx = 0; candidateIdx < candidatesSize; ++candidateIdx )
    {
      candidate.SetIndex( candidateIdx );
      candidates.Add( candidate );
    }
  }

  int candidatesSize = candidates.Size();
  if( candidatesSize == 0 )
  {
    //! This should never happen, such queries should never be displayed.
    AssertDebug( 0 );

    // Destroy space for variables
    GGameState.EndContext();
    return;
  }

  // Float value for every condition for every candidate of current AI.
  Array2D< float > candidatesConditionsResults;
  int complementaryConditionsSize = complementaryConditions.Size();
  int complementarySubjectsSize = complementarySubjects.Size();
  candidatesConditionsResults.Dim( conditionsSize, candidatesSize );
  evaluatedVariables.Dim( conditionsSize + 
    complementaryConditionsSize /* + complementarySubjectsSize - is subgroup of complementary conditions */, 
    candidatesSize );

  // Go through all conditions.
  int realConditionsSize = 0;
  for( int conditionsIdx = 0; conditionsIdx < conditionsSize; ++conditionsIdx )
  {
    // Is it a condition? - 'constant' must be defined.
    SentenceResultCondition &condition = ruleConditions[ conditionsIdx ];

    // 1st step - set up '_this' to evaluate 'constant'.
    if( condition._thisVariable.NotNull() )
    {
      // Register variable of the sentence fragment, where the expression of the condition appeared.
      GGameState.VarSet( "_this", GameValue( new GameDataKnowledge( condition._thisVariable ) ), true );
    }

    // 2nd step - evaluate the 'constant'.
    if( condition._constant.GetRefCount() != 0 )
    {
      GameValue constantValue = GGameState.Evaluate( condition._constant );
      AssertDebug( GGameState.GetLastError() == EvalOK );

      // 3rd step - set the value of 'constant' to '_op1' and '_constant'.
      GGameState.VarSet( "_op1", constantValue, true );
      GGameState.VarSet( "_constant", constantValue, true );
    }

    // Go through all candidates.
    for( int candidatesIdx = 0; candidatesIdx < candidatesSize; ++candidatesIdx )
    {
      // 4th step - set up '_candidate' variable.
      GGameState.VarSet( "_candidate", GameValue( new GameDataQuery( candidates[ candidatesIdx ] ) ), true );

      // 5th step - evaluate the 'variable'.
      GameValue variableValue = GGameState.Evaluate( condition._variable );
      AssertDebug( GGameState.GetLastError() == EvalOK );

      evaluatedVariables.Set( conditionsIdx, candidatesIdx ) = variableValue;
      // AssertDebug( variableValue.GetType() != GameString || static_cast< RString >( variableValue ).GetLength() != 0 );

      if( condition._condition.GetRefCount() != 0 )
      {
        ++realConditionsSize;
        
        // Either constant is defined or condition expression doesn't contain reference to constant.
        AssertDebug( condition._constant.GetRefCount() != 0 || strstr( condition._condition, "_constant" ) == NULL );
        
        // 6th step - set the value of 'variable' to '_variable'.
        GGameState.VarSet( "_variable", variableValue, true );

        // 7th step - evaluate the 'condition'.
        candidatesConditionsResults.Set( conditionsIdx, candidatesIdx ) = GGameState.Evaluate( condition._condition );
        AssertDebug( GGameState.GetLastError() == EvalOK );

        // Delete '_variable'.
        GGameState.VarDelete( "_variable" );
      }
      else if( condition._constant.GetRefCount() != 0 )
      {
        ++realConditionsSize;

        // 6th step - set the value of 'variable' to '_op2'.
        GGameState.VarSet( "_op2", variableValue, true );

        // 7th step - evaluate the 'metrics'.
        candidatesConditionsResults.Set( conditionsIdx, candidatesIdx ) = GGameState.Evaluate( condition._metrics );
        AssertDebug( GGameState.GetLastError() == EvalOK );

        // Delete '_op2'.
        GGameState.VarDelete( "_op2" );
      }
      else
      {
        /* No 'constant' -> no condition -> every candidate won't pass this condition, 
        because it used to display information (and only unsuccessful conditions are displayed).
        */
        candidatesConditionsResults.Set( conditionsIdx, candidatesIdx ) = 0.0f;
      }

      // Delete '_candidate'.
      GGameState.VarDelete( "_candidate" );
    }

    // Delete '_op1', '_constant' and '_this'.
    if( condition._constant.GetRefCount() != 0 )
    {
      GGameState.VarDelete( "_op1" );
      GGameState.VarDelete( "_constant" );
    }
    GGameState.VarDelete( "_this" );
  }

  // If all conditions are used only to display variable then use condition results.
  if( realConditionsSize == 0 )
  {
    ReactConditionally( speaker, sentence, reaction, candidates, ruleConditions, evaluatedVariables, listeners );

    // Destroy space for variables
    GGameState.EndContext();
    return;
  }

  // Read all possible result formats.
  RefArray< const FormattedSentenceResult > formats;
  sentence->GetProbableResultsRecursive( formats );

  // Format doesn't exist.
  if( formats.Size() == 0 )
  {
    // Conditional result.
    ReactConditionally( speaker, sentence, reaction, candidates, ruleConditions, evaluatedVariables, listeners );

    // Destroy space for variables
    GGameState.EndContext();
    return;
  }

  // Calculate the sum of conditions priorities.
  float conditionsPrioritiesSum = 0.0f;
  for( int conditionsIdx = 0; conditionsIdx < conditionsSize; ++conditionsIdx )
  {
    conditionsPrioritiesSum += ruleConditions[ conditionsIdx ]._priority;
  }

  // Calculate candidates results.
  AutoArray< IndexedCandidateSuccess > candidatesResults;
  candidatesResults.Realloc( candidatesSize );
  candidatesResults.Resize( candidatesSize );

  // Initialize 'candidatesResults'.
  for( int candidatesIdx = 0; candidatesIdx < candidatesSize; ++candidatesIdx )
  {
    IndexedCandidateSuccess &a = candidatesResults[ candidatesIdx ];
    a._index = candidatesIdx;
    a._fuzzyAnd = 1.0f; // default - all candidates pass
    a._successfulness = 0.0f;
    a._fulfilledConditions = 0;
    a._conditionBits = 0UL;
    unsigned long conditionBit = 1;

    // Calculate total success of each candidate.
    for( int conditionsIdx = 0; conditionsIdx < conditionsSize; ++conditionsIdx )
    {
      float f = candidatesConditionsResults.Get( conditionsIdx, candidatesIdx );
      a._fuzzyAnd *= f;
      a._successfulness += f * ruleConditions[ conditionsIdx ]._priority;
      if( f == 1.0f )
      {
        // Add extra bonus when condition is fulfilled for 100%.
        ++a._fulfilledConditions;
        a._conditionBits |= conditionBit;
      }
      else if( ruleConditions[ conditionsIdx ]._required )
      {
        a._fuzzyAnd = 0.0f;
        a._successfulness = 0.0f;
        a._fulfilledConditions = 0;
        break;
      }
      conditionBit <<= 1;
    }
  }

  // Sort candidatesResults.
  QSort( candidatesResults.Data(), candidatesSize, &IndexedCandidateSuccess::CompareSuccessfulness );

  // Sort formats.
  QSort( formats.Data(), formats.Size(), &FormattedSentenceResult::CompareThreshold );

  // Read successfulness of the most successful result and normalize it.
  float maxSuccess = 0.0f;
  if( candidatesResults[ 0 ]._fulfilledConditions != 0 )
  {
    if( conditionsPrioritiesSum != 0.0f )
    {
      // Use epsilon=1e-6 to eliminate round error on FPU.
      maxSuccess = candidatesResults[ 0 ]._successfulness / conditionsPrioritiesSum + 1e-6;
    }
    else
    {
      maxSuccess = 1.0f;
    }
  }

  // Go through formats until at least one result is enough successful.
  const FormattedSentenceResult *useFormat = NULL;
  int formatsSize = formats.Size();
  for( int formatsIdx = 0; formatsIdx < formatsSize; ++formatsIdx )
  {
    useFormat = formats[ formatsIdx ];
    if( useFormat->GetThreshold() <= maxSuccess )
    {
      break;
    }
    // If no result can pass any threshold, then use the last one.
  }

  // Find candidatesResults passed the threshold.
  AutoArray< IndexedGroupedResult > successfulCandidates;
  // And calculate "main condition bit" mask.
  // The init value remain only when no candidate will pass -> mask won't be used.
  unsigned long mainConditionBits = ~0;
  int successfulCandidatesSize = 0;
  if( candidatesResults[ 0 ]._fulfilledConditions != 0 )
  {
    int resultsSize = candidatesResults.Size();
    IndexedGroupedResult tmpResult;
    // Add the 1st (the most successful) candidate.
    const IndexedCandidateSuccess *lastResult = &candidatesResults[ 0 ];
    tmpResult._index = lastResult->_index;
    tmpResult._conditionBits = lastResult->_conditionBits;
    successfulCandidates.Add( tmpResult );
    mainConditionBits &= lastResult->_conditionBits;
    // Add next ones.
    for( int resultsIdx = 1; resultsIdx < resultsSize; ++resultsIdx )
    {
      const IndexedCandidateSuccess &result = candidatesResults[ resultsIdx ];
      if( result._fulfilledConditions >= lastResult->_fulfilledConditions &&
        ( result._successfulness == lastResult->_successfulness ||
          result._successfulness / lastResult->_successfulness >= 0.95f ) )
      {
        // This one passed.
        tmpResult._index = result._index;
        tmpResult._conditionBits = result._conditionBits;
        successfulCandidates.Add( tmpResult );
        mainConditionBits &= result._conditionBits;
        lastResult = &result;
        continue;
      }
      // The following did not pass.
      break;
    }
    successfulCandidatesSize = successfulCandidates.Size();
    QSort( successfulCandidates.Data(), successfulCandidatesSize, &IndexedGroupedResult::CompareGroup );
  }

  // Create response sentence.
  SentenceAtom::CreateBasicSentenceAtom( *speaker->GetWordBase(), reaction, UNKNOWN );

  AutoArray< int > successfulCandidatesIndices;
  AutoArray< SemanticQuery > successfulCandidatesQueries;
  RefArray< const SemanticKnowledge > newContextGroupsSubjects;
  AutoArray< CandidatesIndicesGroup > newContextGroupsCandidates;
  
  // Compose 1st sentence - from response id.
  if( useFormat->GetResponseId().GetRefCount() != 0 )
  {
    for( int i = 0; i < successfulCandidatesSize; ++i )
    {
      int idx = successfulCandidates[ i ]._index;
    	successfulCandidatesIndices.Add( idx );
    	successfulCandidatesQueries.Add( candidates[ idx ] );
    }
    ComposeResponseSentence( reaction, useFormat->GetResponseId(), 
      *speaker->GetWordBase(), successfulCandidatesIndices, 
      candidatesConditionsResults, ruleConditions, evaluatedVariables, successfulCandidatesQueries,
      mainConditionBits, newContextGroupsSubjects, newContextGroupsCandidates );
    
    successfulCandidatesQueries.Clear();
    successfulCandidatesIndices.Clear();
  }
  
  // When all conditions were passed ...
  if( candidatesResults[ 0 ]._fulfilledConditions >= conditionsSize )
  {
    AssertDebug( candidatesResults[ 0 ]._fulfilledConditions == conditionsSize );

    // Add complementary conditions.
    // Calculate new variables.
    // Go through all candidates.
    for( int j = 0; j < successfulCandidatesSize; ++j )
    {
      int candidateIdx = successfulCandidates[ j ]._index;
      
      // Set up '_candidate' variable.
      GGameState.VarSet( "_candidate", GameValue( new GameDataQuery( candidates[ candidateIdx ] ) ), true );

      // Go through all conditions to evaluate and add them.
      for( int i = 0; i < complementaryConditionsSize; ++i )
      {
    	  SentenceResultCondition &cc = complementaryConditions[ i ];
      	
        // Evaluate the script expression.
        GameValue complementaryVariable = GGameState.Evaluate( cc._variable );
        AssertDebug( GGameState.GetLastError() == EvalOK );
        
        AssertDebug( complementaryVariable.GetType() != GameString || 
          static_cast< RString >( complementaryVariable ).GetLength() != 0 );

        // Add new complementary variable value to array.
        evaluatedVariables.Set( conditionsSize + i, candidateIdx ) = complementaryVariable;
      }
      
      GGameState.VarDelete( "_candidate" );
    }
    
    // Add complementary conditions to all conditions.
    for( int i = 0; i < complementaryConditionsSize; ++i )
    {
      const SentenceResultCondition &condition = complementaryConditions[ i ];
      ruleConditions.Add( condition );
      conditionsPrioritiesSum += condition._priority;
    }
    conditionsSize += complementaryConditionsSize;
  }
  else
  {
    // Add complementary subjects.
    // Calculate new variables.
    // Go through all candidates.
    for( int j = 0; j < successfulCandidatesSize; ++j )
    {
      int candidateIdx = successfulCandidates[ j ]._index;

      // Set up '_candidate' variable.
      GGameState.VarSet( "_candidate", GameValue( new GameDataQuery( candidates[ candidateIdx ] ) ), true );

      // Go through all conditions to evaluate and add them.
      for( int i = 0; i < complementarySubjectsSize; ++i )
      {
        SentenceResultCondition &cc = complementarySubjects[ i ];

        // Evaluate the script expression.
        GameValue complementaryVariable = GGameState.Evaluate( cc._variable );
        AssertDebug( GGameState.GetLastError() == EvalOK );

        AssertDebug( complementaryVariable.GetType() != GameString || 
          static_cast< RString >( complementaryVariable ).GetLength() != 0 );

        // Add new complementary variable value to array.
        evaluatedVariables.Set( conditionsSize + i, candidateIdx ) = complementaryVariable;
      }

      GGameState.VarDelete( "_candidate" );
    }

    // Add complementary subjects to all conditions.
    for( int i = 0; i < complementarySubjectsSize; ++i )
    {
      const SentenceResultCondition &subject = complementarySubjects[ i ];
      ruleConditions.Add( subject );
      conditionsPrioritiesSum += subject._priority;
    }
    conditionsSize += complementarySubjectsSize;
  }
  
  // Divide candidates grouped by passed conditions into subgroups by distance (similarity) among them.
  if( successfulCandidatesSize != 0 )
  {
    int i = 0;
    do {
      unsigned long curGroup = successfulCandidates[ i ]._conditionBits;
      do {
        successfulCandidatesIndices.Add( i );
        ++i;
      } while( i < successfulCandidatesSize && curGroup == successfulCandidates[ i ]._conditionBits );

      CalcCandidateSubgroups( successfulCandidates, evaluatedVariables, ruleConditions,
        conditionsPrioritiesSum, successfulCandidatesIndices );

      successfulCandidatesIndices.Clear();
    } while( i < successfulCandidatesSize );
    
    QSort( successfulCandidates.Data(), successfulCandidatesSize, &IndexedGroupedResult::CompareGroupAndSubgroup );
  }
  
  // Compose other sentences - from pattern id.
  if( successfulCandidatesSize != 0 )
  {
    if( useFormat->GetPatternId().GetRefCount() != 0 )
    {
      int i = 0;
      do {
        unsigned long curGroup = successfulCandidates[ i ]._conditionBits;
        unsigned int curSubgroup = successfulCandidates[ i ]._subgroupNumber;
      	do {
          int idx = successfulCandidates[ i ]._index;
          successfulCandidatesIndices.Add( idx );
          successfulCandidatesQueries.Add( candidates[ idx ] );
          ++i;
      	} while( i < successfulCandidatesSize && curGroup == successfulCandidates[ i ]._conditionBits &&
      	  curSubgroup == successfulCandidates[ i ]._subgroupNumber );
      	
        ComposeResponseSentence( reaction, useFormat->GetPatternId(), 
          *speaker->GetWordBase(), successfulCandidatesIndices, 
          candidatesConditionsResults, ruleConditions, evaluatedVariables, successfulCandidatesQueries,
          curGroup, newContextGroupsSubjects, newContextGroupsCandidates );

        successfulCandidatesIndices.Clear();
        successfulCandidatesQueries.Clear();
      } while( i < successfulCandidatesSize );
    }
  }

  // Set the context subject. There must be something to put in context.
  int newContextGroupsSubjectsN = newContextGroupsSubjects.Size();
  AssertDebug( newContextGroupsSubjectsN == newContextGroupsCandidates.Size() );

  // Clear (prepare) particular notions in speaker's context.
  GetModSemanticContext().ClearWithHypernyms( newContextGroupsSubjects, *speaker->GetWordBase() );
  
  // Change context in speaker's semantic net.
  for( int newContextGroupsSubjectsI = 0; newContextGroupsSubjectsI < newContextGroupsSubjectsN; ++newContextGroupsSubjectsI )
  {
    const SemanticKnowledge *token = newContextGroupsSubjects[ newContextGroupsSubjectsI ];
    const CandidatesIndicesGroup &group = newContextGroupsCandidates[ newContextGroupsSubjectsI ];
    int groupN = group.Size();
    for( int groupI = 0; groupI < groupN; ++groupI )
    {
      successfulCandidatesQueries.Add( candidates[ successfulCandidates[ group[ groupI ] ]._index ] );
    }
    GetModSemanticContext().AddWithHypernyms( token, successfulCandidatesQueries, 
      *speaker->GetWordBase() );
    successfulCandidatesQueries.Clear();
  }
  
  // Transform incomings of the listeners to the outcomings.
  //... This transformation should be calculated with dictionary of each listener.
  RefArray< const SemanticKnowledge > listenerOutcomings;
  SemanticKnowledge::SwitchIncomingAndOutcoming( newContextGroupsSubjects,
    listenerOutcomings, *speaker->GetWordBase() );
  
  // Clear (prepare) particular notions in each listener's context.
  int listenersN = listeners.Size();
  for( int listenersI = 0; listenersI < listenersN; ++listenersI )
  {
    KnowledgeBase *l = listeners[ listenersI ];
    SemanticNetBase *lsn = static_cast< SemanticNetBase* >( l->GetModSemanticNet() );
    // Delete outcomings not incomings! It's different for listeners than for speaker!
    lsn->GetModSemanticContext().ClearWithHypernyms( listenerOutcomings, *l->GetWordBase() );
  }

  // Transfer information.
  // Define variable of the speaker ("_me") is still defined.
  SemanticQuery dstCandidate;
  AutoArray< SemanticQuery > transformedSuccessfulQueries;
  AutoArray< SemanticQuery > listenerSuccessfuleCandidates;
  listenerSuccessfuleCandidates.Realloc( successfulCandidatesSize );
  listenerSuccessfuleCandidates.Resize( successfulCandidatesSize );
  int n = listeners.Size();
  for( int i = 0; i < n; ++i )
  {
    KnowledgeBase *l = listeners[ i ];
    SemanticNetBase *lsn = static_cast< SemanticNetBase* >( l->GetModSemanticNet() );

    // Define variable of the listener.
    GGameState.VarSet( "_listener", GameValue( new GameDataKnowledgeBase( l ) ), true );

    for( int successfulCandidatesI = 0; successfulCandidatesI < successfulCandidatesSize; 
      ++successfulCandidatesI )
    {
      const SemanticQuery &srcCandidate = candidates[ successfulCandidates[ successfulCandidatesI ]._index ];

      // Transform queries from the speaker's semantic net to the listener's semantic net based on 
      // semantic knowledge id.
      SemanticNetId snid = GetId( srcCandidate );
      if( snid != -1 )
      {
        if( lsn->GetIdQuery( snid, dstCandidate ) )
        {
          // Define variable of the source candidate.
          GGameState.VarSet( "_srcCandidate", GameValue( new GameDataQuery( srcCandidate ) ), true );

          // Define variable of the destination candidate.
          GGameState.VarSet( "_dstCandidate", GameValue( new GameDataQuery( dstCandidate ) ), true );

          // Transfer the information of each condition.
          for( int conditionIdx = 0; conditionIdx < conditionsSize; ++conditionIdx )
          {
            const RString &transfer = ruleConditions[ conditionIdx ]._transfer;
            if( transfer.GetRefCount() != 0 )
            {
              GGameState.Evaluate( transfer );
              AssertDebug( GGameState.GetLastError() == EvalOK );
            }
          }

          // Delete no more necessary variables.
          GGameState.VarDelete( "_srcCandidate" );
          GGameState.VarDelete( "_dstCandidate" );
          
          listenerSuccessfuleCandidates[ successfulCandidatesI ] = dstCandidate;
        }
      }
    }
    GGameState.VarDelete( "_listener" );
    
    // Change context in listeners' semantic nets
    for( int newContextGroupsSubjectsI = 0; newContextGroupsSubjectsI < newContextGroupsSubjectsN; ++newContextGroupsSubjectsI )
    {
      // Change context in speaker's semantic net.
      const SemanticKnowledge *token = newContextGroupsSubjects[ newContextGroupsSubjectsI ];
      const CandidatesIndicesGroup &group = newContextGroupsCandidates[ newContextGroupsSubjectsI ];
      int groupN = group.Size();

      // Change context in listeners' semantic nets and transfer information.
      // Define variable of the speaker ("_me") is still defined.
      for( int groupI = 0; groupI < groupN; ++groupI )
      {
        transformedSuccessfulQueries.Add( listenerSuccessfuleCandidates[ group[ groupI ] ] );
      }

      // Define listener's context. Transfer listener's incoming to the listener's outcoming.
      if( transformedSuccessfulQueries.Size() != 0 )
      {
        AutoArray< WordBaseId > incomingWbids;
        l->GetWordBase()->SwitchIncomingAndOutcoming( token->GetWordBaseId(), incomingWbids );
        int incomingWbidsN = incomingWbids.Size();
        for( int incomingWbidsI = 0; incomingWbidsI < incomingWbidsN; ++incomingWbidsI )
        {
          Ref< const SemanticKnowledge > know = 
            new SemanticKnowledge( incomingWbids[ incomingWbidsI ], token->GetForm() );
          lsn->GetModSemanticContext().AddWithHypernyms( know, 
            transformedSuccessfulQueries, *l->GetWordBase() );
        }

        // Prepare array for the next use.
        transformedSuccessfulQueries.Clear();
      }
    }
  }

  // Destroy space for variables
  GGameState.EndContext();
}

void SemanticNetBase::CalcCandidateSubgroups( AutoArray< IndexedGroupedResult > &successfulCandidates, 
  const Array2D< GameValue > &evaluatedVariables, const AutoArray< SentenceResultCondition > &conditions,
  const float conditionsPrioritiesSum, const AutoArray< int > &candidateGroup )
{
  int conditionsSize = conditions.Size();

  // Prepare array of similarities among successful candidates.
  Array2D< float > similarities;
  int candidateGroupSize = candidateGroup.Size();
  similarities.Dim( candidateGroupSize, candidateGroupSize );

  // Calculate similarity between each pair of successful candidates.
  similarities.Set( 0, 0 ) = 1.0f;
  for( int j = 1; j < candidateGroupSize; ++j )
  {
    similarities.Set( j, j ) = 1.0f;
    int successfulCandidateJ = successfulCandidates[ candidateGroup[ j ] ]._index;
  	for( int i = 0; i < j; ++i )
  	{
      int successfulCandidateI = successfulCandidates[ candidateGroup[ i ] ]._index;
  		
  		// Calculate similarity between selected pair of successful candidates.
  		float pairSimilarity = 0.0f;
  		
  		// First calculate similarity of single condition of the pair of candidates.
  		for( int k = 0; k < conditionsSize; ++k )
  		{
  		  const SentenceResultCondition &condition = conditions[ k ];
  		  
        // Register variable of the sentence fragment, where the expression appeared.
        GGameState.VarSet( "_op1", evaluatedVariables.Get( k, successfulCandidateJ ), true );
  		  
        // Register variable of the sentence fragment, where the expression appeared.
        GGameState.VarSet( "_op2", evaluatedVariables.Get( k, successfulCandidateI ), true );

        // Evaluate the 'metrics'.
        // Variable '_me' is still defined.
        GameValue metricsValue = GGameState.Evaluate( condition._metrics );
        AssertDebug( GGameState.GetLastError() == EvalOK );
        
        float mv = static_cast< float >( metricsValue );
        AssertDebug( mv >= 0.0f && mv <= 1.0f );
        AssertDebug( condition._priority >= 0.0f && condition._priority <= 1.0f );
        pairSimilarity += mv * condition._priority;
        
        GGameState.VarDelete( "_op2" );
        GGameState.VarDelete( "_op1" );
  		}
  		
  		float s = pairSimilarity / conditionsPrioritiesSum;
  		AssertDebug( s >= 0.0f && s <= 1.0f );
      similarities.Set( j, i ) = similarities.Set( i, j ) = s;
  	}
  }
  
  // Calculate subgroups.
  CandidatesIndicesGroup *initialSubgroup = new CandidatesIndicesGroup();
  initialSubgroup->Realloc( candidateGroupSize );
  initialSubgroup->Resize( candidateGroupSize );
  for( int i = 0; i < candidateGroupSize; ++i )
  {
    ( *initialSubgroup )[ i ] = i;
  }
  AutoArray< CandidatesIndicesGroup* > subgroupedResults;
  int initialSubgroupIdx = subgroupedResults.Add( initialSubgroup );
  SplitGroup( similarities, subgroupedResults, initialSubgroupIdx, 0.75f );
  
  // Sign subgroups (set _subgroupNumber of IndexedGroupedResult).
  int subgroupedResultsSize = subgroupedResults.Size();
  for( int i = 0; i < subgroupedResultsSize; ++i )
  {
    CandidatesIndicesGroup &g = *( subgroupedResults[ i ] );
    int m = g.Size();
  	for( int j = 0; j < m; ++j )
  	{
  		successfulCandidates[ candidateGroup[ g[ j ] ] ]._subgroupNumber = i;
  	}
  }
}

void SemanticNetBase::ComposeResponseSentence( Ref< SentenceAtom > &parentReaction, 
  const RString responseId, const WordBase &wb, 
  const AutoArray< int > &successfulCandidatesIndices, 
  const Array2D< float > &candidatesConditionsResults,
  const AutoArray< SentenceResultCondition > &conditions, 
  const Array2D< GameValue > &evaluatedVariables,
  const AutoArray< SemanticQuery > &successfulCandidatesQueries, unsigned long conditionBits,
  RefArray< const SemanticKnowledge > &newContextGroupsSubjects,
  AutoArray< CandidatesIndicesGroup > &newContextGroupsCandidates ) const
{
  AssertDebug( !_specRules || _specRules->IsClass() );
  ConstParamEntryPtr responseClass;
  if( _specRules )
  {
    ConstParamEntryPtr section = _specRules->FindEntry( "CfgKBResponses" );
    if( section )
    {
      responseClass = section->FindEntry( responseId );
    }
  }
  if( !responseClass )
  {
    responseClass = &( _responses >> responseId );
  }
  if( responseClass )
  {
    // Create new sentence of the reaction.
    Ref< SentenceAtom > reaction;
    SentenceAtom::CreateBasicSentenceAtom( wb, reaction, UNKNOWN );
    parentReaction->AddSubAtom( reaction );

    // Set mood of the response.
    ParamEntryVal mood = *responseClass >> "mood";
    if( !mood.GetPointer()->IsError() )
    {
      reaction->SetMood( mood.GetInt() );
    }

    // Add particular words.
    ParamEntryVal words = *responseClass >> "parts";
    if( words.IsArray() && !words.GetPointer()->IsError() )
    {
      int wordsSize = words.GetSize();
      for( int wordsIdx = 0; wordsIdx < wordsSize; ++wordsIdx )
      {
        const IParamArrayValue &wordElem = words[ wordsIdx ];
        if( !wordElem.IsArrayValue() )
        {
          //! Word has default form.
          if( !wordElem.IsFloatValue() && !wordElem.IsIntValue() )
          {
            // It is a text. It can be: name of WordBaseId, variable or keyword '_result'.
            RStringB value = wordElem.GetValue();

            WordBaseId wordid;
            if( ( wordid = wb.ConvertNameToId( value ) ) != -1 )
            {
              // It is a name of WordBaseId.
              reaction->AddSubAtom( new SemanticKnowledge( wordid, FORM_DEFAULT ) );
            }
            else if( value == _keywordBadValues )
            {
              // Here write result.
              AddResult( reaction, wb, successfulCandidatesIndices, 
                candidatesConditionsResults, conditions, evaluatedVariables, ~conditionBits,
                newContextGroupsSubjects, newContextGroupsCandidates );
            }
            else if( value == _keywordGoodValues )
            {
              // Here write result.
              AddResult( reaction, wb, successfulCandidatesIndices, 
                candidatesConditionsResults, conditions, evaluatedVariables, conditionBits,
                newContextGroupsSubjects, newContextGroupsCandidates );
            }
            else if( value == _keywordAllValues )
            {
              // Here write result.
              AddResult( reaction, wb, successfulCandidatesIndices, 
                candidatesConditionsResults, conditions, evaluatedVariables, ~0,
                newContextGroupsSubjects, newContextGroupsCandidates );
            }
            else if( value == _keywordCandidate )
            {
              if( successfulCandidatesQueries.Size() != 0 )
              {
                Ref< SemanticKnowledge > transformed;
                _semanticContext.TransformQueriesToOutcoming( successfulCandidatesQueries, transformed );
                reaction->AddSubAtom( transformed );
              }
            }
            else if( value == _keywordQuatity )
            {
              // Add a number.
              reaction->AddSubAtom( new SemanticKnowledge( 
                wb._wordIdScalarId, successfulCandidatesQueries.Size(), FORM_DEFAULT ) );
            }
            else
            {
              // Nothing else should not be here.
              AssertDebug( 0 );
            }
          }
          else
          {
            // It is a number.
            reaction->AddSubAtom( new SemanticKnowledge( 
              wb._wordIdScalarId, wordElem.GetFloat(), FORM_DEFAULT ) );
          }
        }
        else
        {
          //! Word has custom form.
          const IParamArrayValue &word = wordElem[ 0 ];
          if( !word.IsFloatValue() && !word.IsIntValue() )
          {
            // It is a text. It can be: name of WordBaseId, variable or keyword '_result'.
            RStringB value = word.GetValue();

            WordBaseId wordid;
            if( ( wordid = wb.ConvertNameToId( value ) ) != -1 )
            {
              // It is a name of WordBaseId.
              reaction->AddSubAtom( new SemanticKnowledge( wordid, wordElem[ 1 ].GetInt() ) );
            }
            else if( value == _keywordBadValues )
            {
              // Here write result.
              AddResult( reaction, wb, successfulCandidatesIndices, 
                candidatesConditionsResults, conditions, evaluatedVariables, ~conditionBits,
                newContextGroupsSubjects, newContextGroupsCandidates );
            }
            else if( value == _keywordGoodValues )
            {
              // Here write result.
              AddResult( reaction, wb, successfulCandidatesIndices, 
                candidatesConditionsResults, conditions, evaluatedVariables, conditionBits,
                newContextGroupsSubjects, newContextGroupsCandidates );
            }
            else if( value == _keywordAllValues )
            {
              // Here write result.
              AddResult( reaction, wb, successfulCandidatesIndices, 
                candidatesConditionsResults, conditions, evaluatedVariables, ~0,
                newContextGroupsSubjects, newContextGroupsCandidates );
            }
            else if( value == _keywordCandidate )
            {
              if( successfulCandidatesQueries.Size() != 0 )
              {
                Ref< SemanticKnowledge > transformed;
                _semanticContext.TransformQueriesToOutcoming( successfulCandidatesQueries, 
                  wb, wordElem[ 1 ].GetInt(), transformed );
                reaction->AddSubAtom( transformed );
              }
            }
            else if( value == _keywordQuatity )
            {
              // Add a number.
              reaction->AddSubAtom( new SemanticKnowledge( 
                wb._wordIdScalarId, successfulCandidatesQueries.Size(), wordElem[ 1 ].GetInt() ) );
            }
            else
            {
              // Nothing else should not be here.
              AssertDebug( 0 );
            }
          }
          else
          {
            // It is a number.
            reaction->AddSubAtom( new SemanticKnowledge( 
              wb._wordIdScalarId, word.GetFloat(), wordElem[ 1 ].GetInt() ) );
          }
        }
      }
    }
  }
}

void SemanticNetBase::SplitGroup( const Array2D< float > &similarities, 
  AutoArray< CandidatesIndicesGroup* > &groupedResults, int splitGroupIdx, float splitThreshold )
{
  // Split particular group.
  CandidatesIndicesGroup *splitGroup = groupedResults[ splitGroupIdx ];
  int splitGroupSize = splitGroup->Size();
  AssertDebug( splitGroupSize != 0 );

  // Find the centres of two subgroups.
  float minSimilarity = 1.0f;
  int groupCenterIdxI = 0;
  int groupCenterIdxJ = 0;
  for( int j = 1; j < splitGroupSize; ++j )
  {
    int candidateJ = ( *splitGroup )[ j ];
    for( int i = 0; i < j; ++i )
    {
      int candidateI = ( *splitGroup )[ i ];
      float curSimilarity = similarities.Get( candidateI, candidateJ );
      if( curSimilarity < minSimilarity )
      {
        minSimilarity = curSimilarity;
        groupCenterIdxI = i;
        groupCenterIdxJ = j;
      }
    }
  }

  // Is the minimal similarity small enough to split the group?
  if( minSimilarity >= splitThreshold )
  {
    return;
  }

  int groupCenterI = ( *splitGroup )[ groupCenterIdxI ];
  int groupCenterJ = ( *splitGroup )[ groupCenterIdxJ ];

  // Create new subgroup.
  CandidatesIndicesGroup *newSubgroup = new CandidatesIndicesGroup();
  int newSubgroupIdx = groupedResults.Add( newSubgroup );
  newSubgroup->Add( groupCenterI );
  splitGroup->Delete( groupCenterIdxI );
  --splitGroupSize;

  // Move elements in the new subgroup.
  for( int i = 0; i < splitGroupSize; )
  {
    int t = ( *splitGroup )[ i ];
    float simIT = similarities.Get( groupCenterI, t );
    float simJT = similarities.Get( groupCenterJ, t );
    AssertDebug( groupCenterJ != t || simJT > simIT );
    AssertDebug( groupCenterI != t || simJT < simIT );
    if( simIT > simJT )
    {
      AssertDebug( groupCenterJ != t );
      newSubgroup->Add( t );
      splitGroup->Delete( i );
      --splitGroupSize;
    }
    else
    {
      AssertDebug( groupCenterI != t );
      ++i;
    }
  }

  // Recursive call.
  SplitGroup( similarities, groupedResults, splitGroupIdx, splitThreshold );
  SplitGroup( similarities, groupedResults, newSubgroupIdx, splitThreshold );
}

void SemanticNetBase::CalcAggregation( const AutoArray< int > &successfulCandidates, 
  const Array2D< GameValue > &evaluatedVariables, const SentenceResultCondition &condition, 
  int conditionIdx, AutoArray< MergedValuesGroup > &mergedResults )
{
  // Calculate similarities.
  Array2D< float > similarities;
  int successfulCandidatesSize = successfulCandidates.Size();
  similarities.Dim( successfulCandidatesSize, successfulCandidatesSize );
  similarities.Set( 0, 0 ) = 1.0f;
  for( int j = 1; j < successfulCandidatesSize; ++j )
  {
    similarities.Set( j, j ) = 1.0f;

    // Register variable of the sentence fragment, where the expression appeared.
    GGameState.VarSet( "_op1", evaluatedVariables.Get( conditionIdx, successfulCandidates[ j ] ), true );

    for( int i = 0; i < j; ++i )
    {
      // Register variable of the sentence fragment, where the expression appeared.
      GGameState.VarSet( "_op2", evaluatedVariables.Get( conditionIdx, successfulCandidates[ i ] ), true );

      // Evaluate the 'metrics'.
      // Variable '_me' is still defined.
      GameValue metricsValue = GGameState.Evaluate( condition._metrics );
      AssertDebug( GGameState.GetLastError() == EvalOK );
      similarities.Set( j, i ) = similarities.Set( i, j ) = metricsValue;
      
      GGameState.VarDelete( "_op2" );
    }
    
    GGameState.VarDelete( "_op1" );
  }

  // Calculate groups.
  CandidatesIndicesGroup *initialGroup = new CandidatesIndicesGroup();
  initialGroup->Realloc( successfulCandidatesSize );
  initialGroup->Resize( successfulCandidatesSize );
  for( int i = 0; i < successfulCandidatesSize; ++i )
  {
  	( *initialGroup )[ i ] = i;
  }
  AutoArray< CandidatesIndicesGroup* > groupedResults;
  int initialGroupIdx = groupedResults.Add( initialGroup );
  SplitGroup( similarities, groupedResults, initialGroupIdx, condition._aggregationThreshold );
  
  // Calculate predecessor of the members of each group.
  Ref< GameDataArray > groupValues = new GameDataArray();
  GameArrayType &groupValuesArray = groupValues->GetArray();
  GGameState.VarSet( "_array", GameValue( groupValues ), true );
  MergedValuesGroup mergedValues;
  int n = groupedResults.Size();
  for( int i = 0; i < n; ++i )
  {
    CandidatesIndicesGroup &group = *groupedResults[ i ];
    mergedValues._candidatesIndices = group;
    int groupN = group.Size();
    for( int j = 0; j < groupN; ++j )
    {
      groupValuesArray.Add( 
        evaluatedVariables.Get( conditionIdx, successfulCandidates[ group[ j ] ] ) );
    }
    
    // Execute script to find the predecessor.
    mergedValues._value = GGameState.Evaluate( condition._predecessor );
    AssertDebug( GGameState.GetLastError() == EvalOK );
    AssertDebug( mergedValues._value.GetType() != GameString || static_cast< RString >( mergedValues._value ).GetLength() != 0 );

    // Put the predecessor in aggregated values.
    mergedResults.Add( mergedValues );
    
    // Prepare the '_array' variable for next group predecessor.
    groupValuesArray.Clear();
  }
  GGameState.VarDelete( "_array" );
  
  // Clear the groups.
  n = groupedResults.Size();
  for( int i = 0; i < n; ++i )
  {
    delete groupedResults[ i ];
  }
  groupedResults.Clear();
}

/*! Adds result (all evaluated variables of unsuccessful conditions of all successful candidates).
    \param evaluatedVariables Is a 2D array of dimension [conditionsSize, candidatesSize].
*/
void SemanticNetBase::AddResult( Ref< SentenceAtom > &reaction, 
  const WordBase &wordBase, const AutoArray< int > &successfulCandidates, 
  const Array2D< float > &candidatesConditionsResults,
  const AutoArray< SentenceResultCondition > &conditions, 
  const Array2D< GameValue > &evaluatedVariables, unsigned long conditionBits,
  RefArray< const SemanticKnowledge > &newContextGroupsSubjects,
  AutoArray< CandidatesIndicesGroup > &newContextGroupsCandidates ) const
{
  int successfulCandidatesSize = successfulCandidates.Size();
  int conditionsSize = conditions.Size();
  if( successfulCandidatesSize == 0 )
  {
    AssertDebug( 0 );
    return;
  }
  
  short minWordOrder = 0;
  const SentenceResultCondition *nextCondition = NULL;
  int nextConditionIdx = 0;
  for( ;; )
  {
    unsigned long curConditionBit = 1;
    int conditionIdx = 0;
    AutoArray< MergedValuesGroup > mergedResults;
    short nextWordOrder = POS_Size;
    while( conditionIdx < conditionsSize)
    {
      if( ( conditionBits & curConditionBit ) != 0 )
      {
        // Current condition.
        const SentenceResultCondition &c = conditions[ conditionIdx ];
        // Required conditions are not written out.
        if( !c._required )
        {
          short wordOrder = wordBase.GetWordOrder( c._partOfSentence );
          AssertDebug( wordOrder != nextWordOrder );
          if( wordOrder < nextWordOrder && wordOrder >= minWordOrder )
          {
            nextCondition = &c;
            nextConditionIdx = conditionIdx;
            nextWordOrder = wordOrder;
          }
        }
        else
        {
          // Define context subject in speaker's and listener's semantic nets.
          if( c._contextSubject != -1 )
          {
            if( c._contextSubject != wordBase._wordIdVariableId )
            {
              // Add all successful candidates as this notion to the context.
              AddContextNotion( c._contextSubject, 
                successfulCandidatesSize > 1 ? FORM_PLURAL : FORM_DEFAULT,
                successfulCandidatesSize, newContextGroupsSubjects, newContextGroupsCandidates );
            }
            else
            {
              // Do aggregation for this condition.
              CalcAggregation( successfulCandidates, evaluatedVariables, c, conditionIdx, mergedResults );
              
              // Add each group of successful candidates as a notion of the particular variable value.
              AddContextNotion( mergedResults, wordBase, c._valueType, successfulCandidates, 
                newContextGroupsSubjects, newContextGroupsCandidates );
              
              // Clear results (prepare for next condition values).
              mergedResults.Clear();
            }
          }
        }
      }
      curConditionBit <<= 1;
      ++conditionIdx;
    }

    if( nextCondition != NULL )
    {
      // Do aggregation for this condition and write out the result.
      CalcAggregation( successfulCandidates, evaluatedVariables, *nextCondition, nextConditionIdx, mergedResults );

      // Write out aggregated values.
      AddResultValues( reaction, nextCondition->_formatId, wordBase, nextCondition->_expression, 
        nextCondition->_valueType, mergedResults );
      
      // Define context subject in speaker's and listener's semantic nets.
      if( nextCondition->_contextSubject != -1 )
      {
        if( nextCondition->_contextSubject != wordBase._wordIdVariableId )
        {
          // Add all successful candidates as this notion to the context.
          AddContextNotion( nextCondition->_contextSubject, 
            successfulCandidatesSize > 1 ? FORM_PLURAL : FORM_DEFAULT,
            successfulCandidatesSize, newContextGroupsSubjects, newContextGroupsCandidates );
        }
        else
        {
          // Add each group of successful candidates as a notion of the particular variable value.
          AddContextNotion( mergedResults, wordBase, nextCondition->_valueType, successfulCandidates, 
            newContextGroupsSubjects, newContextGroupsCandidates );
        }
      }

      // Clear results (prepare for next condition values).
      mergedResults.Clear();
      nextCondition = NULL;
      minWordOrder = nextWordOrder + 1;
    }
    else
    {
      break;
    }
  }
}

/*! Adds result (all evaluated variables of unsuccessful conditions of one successful candidate).
    \param evaluatedVariables Is a 2D array of dimension [conditionsSize, candidatesSize].
*/
void SemanticNetBase::AddResultValues( Ref< SentenceAtom > &reaction, int valuesFormatId,
  const WordBase &wordBase, const RString expression, short valueType,
  const AutoArray< MergedValuesGroup > &results ) const
{
  int n = results.Size();
  if( n == 0 )
  {
    return;
  }

  switch( valuesFormatId )
  {
  case FORMAT_LIST:
    {
      if( n == 1 )
      {
        // Add only one result value.
        FormatResultExpression( reaction, wordBase, valueType, results[ 0 ], expression );
      }
      else if( n > 1 )
      {
        // Add 1st result.
        FormatResultExpression( reaction, wordBase, valueType, results[ 0 ], expression );

        --n;
        for( int i = 1; i < n; ++i )
        {
          // Add comma.
          reaction->AddSubAtom( new SemanticKnowledge( wordBase._wordIdCommaId, FORM_DEFAULT ) );

          // Add next result.
          FormatResultExpression( reaction, wordBase, valueType, results[ i ], expression );
        }

        // Add 'and' word before the last value.
        reaction->AddSubAtom( new SemanticKnowledge( wordBase._wordIdAnd, FORM_DEFAULT ) );

        // Add the last result value.
        FormatResultExpression( reaction, wordBase, valueType, results[ n ], expression );
      }
    }
    break;

  default: // FORMAT_SIMPLE
    {
      for( int i = 0; i < n; ++i )
      {
        FormatResultExpression( reaction, wordBase, valueType, results[ i ], expression );
      }
    }
    break;
  }
}

void SemanticNetBase::FormatResultExpression( Ref< SentenceAtom > &reaction, const WordBase &wordBase, 
  short valueType, const MergedValuesGroup &value, const RString expression ) const
{
  if( expression.GetRefCount() != 0 )
  {
    AssertDebug( !_specRules || _specRules->IsClass() );
    ConstParamEntryPtr expressionClass;
    if( _specRules )
    {
      ConstParamEntryPtr section = _specRules->FindEntry( "CfgKBResponses" );
      if( section )
      {
        expressionClass = section->FindEntry( expression );
      }
    }
    if( !expressionClass )
    {
      expressionClass = &( _responses >> expression );
    }
    if( expressionClass )
    {
      ParamEntryVal words = *expressionClass >> "parts";
      if( words.IsArray() && !words.GetPointer()->IsError() )
      {
        int wordsSize = words.GetSize();
        for( int wordsIdx = 0; wordsIdx < wordsSize; ++wordsIdx )
        {
          long form;
          const IParamArrayValue *wordElem = &words[ wordsIdx ];
          if( wordElem->IsArrayValue() )
          {
            //! Word has custom form.
            form = wordElem->GetItem( 1 )->GetInt();
            wordElem = wordElem->GetItem( 0 );
          }
          else
          {
            //! Word has default form.
            form = FORM_DEFAULT;
          }
                    
          if( !wordElem->IsFloatValue() && !wordElem->IsIntValue() )
          {
            // It is a text. It can be: name of WordBaseId, variable or keyword '_result'.
            RStringB wordValue = wordElem->GetValue();

            WordBaseId wordid;
            if( ( wordid = wordBase.ConvertNameToId( wordValue ) ) != -1 )
            {
              // It is a name of WordBaseId.
              reaction->AddSubAtom( new SemanticKnowledge( wordid, form ) );
            }
            else if( wordValue == _keywordValue )
            {
              // Here write result.
              FormatResultValue( reaction, wordBase, valueType, value._value, value._candidatesIndices.Size() > 1 ? ( form | FORM_PLURAL ) : form );
            }
            else if( wordValue == _keywordSingularValue )
            {
              // Here write result.
              FormatResultValue( reaction, wordBase, valueType, value._value, form );
            }
            else if( wordValue == _keywordQuatity )
            {
              reaction->AddSubAtom( new SemanticKnowledge( wordBase._wordIdScalarId, value._candidatesIndices.Size(), FORM_DEFAULT ) );
            }
            else if( wordValue == _keywordPluralQuatity )
            {
              if( value._candidatesIndices.Size() > 1 )
              {
                reaction->AddSubAtom( new SemanticKnowledge( wordBase._wordIdScalarId, value._candidatesIndices.Size(), FORM_DEFAULT ) );
              }
            }
            else if( wordValue == _keywordRoundedPluralQuatity )
            {
              if( value._candidatesIndices.Size() > 1 )
              {
                reaction->AddSubAtom( new SemanticKnowledge( wordBase._wordIdScalarId, RoundQuatity( value._candidatesIndices.Size() ), FORM_DEFAULT ) );
              }
            }
            else
            {
              // Nothing else should not be here.
              AssertDebug( 0 );
            }
          }
          else
          {
            // It is a number.
            reaction->AddSubAtom( new SemanticKnowledge( wordBase._wordIdScalarId, wordElem->GetFloat(), form ) );
          }
        }
      }
    }
  }
  else
  {
    // reaction->AddSubAtom( new SemanticKnowledge( wordBase._wordIdScalarId, value._size, FORM_DEFAULT ) );
    FormatResultValue( reaction, wordBase, valueType, value._value, value._candidatesIndices.Size() > 1 ? ( FORM_DEFAULT | FORM_PLURAL ) : FORM_DEFAULT );
  }
}

WordBaseId SemanticNetBase::ConvertValueForContext( const WordBase &wordBase, 
  short valueType, const GameValue &value ) const
{
  switch( valueType )
  {
  case VT_ENTITYTYPE:
    {
      RString valueStr = static_cast< RString >( value );
      int n = GetKnowledgesSize( _tableEntityTypes );
      if( n != 0 )
      {
        SemanticQuery entityType( _tableEntityTypes );
        while( entityType.GetIndex() < n )
        {
          RString typeStr = static_cast< RString >( GetEntityType( entityType ) );
          if( typeStr != valueStr )
          {
            entityType.IncIndex();
            continue;
          }
          RString typeName = static_cast< RString >( GetName( entityType ) );
          typeName.Lower();
          return wordBase.ConvertTextToId( typeName );
        }
        AssertDebug( 0 );
        return -1;
      }
      else
      {
        return wordBase.ConvertTextToId( valueStr );
      }
    }
  
  case VT_STRING:
    {
      if( value.GetType() == GameKnowledge )
      {
        return static_cast< GameDataKnowledge* >( value.GetData() )->GetKnowledge()->GetWordBaseId();
      }
      else
      {
        return wordBase.ConvertTextToId( static_cast< RString >( value ) );
      }
    }
    break;
  	
  default:
    AssertDebug( 0 );
    return -1;
  }
}

void SemanticNetBase::FormatResultValue( Ref< SentenceAtom > &reaction, const WordBase &wordBase, 
  short valueType, const GameValue &value, long form ) const
{
  switch( valueType )
  {
  case VT_PLACE:
    {
      if( value.GetType() == GameArea )
      {
        const PlaceArea &area = static_cast< GameDataArea* >( value.GetData() )->GetArea();
        int n = GetKnowledgesSize( _tablePlaces );
        if( n != 0 )
        {
          SemanticQuery place( _tablePlaces );
          SemanticQuery nearest( place );
          GameValue minAreaValue = GetArea( place );
          float minDist = GetDistance( value, minAreaValue );
          for( place.IncIndex(); place.GetIndex() < n; place.IncIndex() )
          {
            GameValue newArea = GetArea( place );
            float newDist = GetDistance( value, newArea );
            if( newDist < minDist )
            {
              nearest.SetIndex( place.GetIndex() );
              minDist = newDist;
              minAreaValue = newArea;
            }
          }

          minDist = ::ceil( minDist );
          RString nearestName = GetName( nearest );
          const PlaceArea &minArea = static_cast< GameDataArea* >( minAreaValue.GetData() )->GetArea();

          if( minDist > 0.0f )
          {
            float distMeters, distKms;
            RoundDistance( minDist, distMeters, distKms );
            if( distKms != 0.0f )
            {
              if( distKms < 2 )
              {
                reaction->AddSubAtom( new SemanticKnowledge( wordBase._wordIdKilometer, FORM_DEFAULT ) );
              }
              else
              {
                reaction->AddSubAtom( new SemanticKnowledge( wordBase._wordIdScalarId, distKms, FORM_DEFAULT ) );
                reaction->AddSubAtom( new SemanticKnowledge( wordBase._wordIdKilometer, FORM_PLURAL ) );
              }
            }
            if( distMeters != 0.0f )
            {
              if( distMeters < 2 )
              {
                reaction->AddSubAtom( new SemanticKnowledge( wordBase._wordIdMeter, FORM_DEFAULT ) );
              }
              else
              {
                reaction->AddSubAtom( new SemanticKnowledge( wordBase._wordIdScalarId, distMeters, FORM_DEFAULT ) );
                reaction->AddSubAtom( new SemanticKnowledge( wordBase._wordIdMeter, FORM_PLURAL ) );
              }
            }
            reaction->AddSubAtom( new SemanticKnowledge( wordBase.ConvertNameToId( "WordIdTo" ), FORM_DEFAULT ) );
            reaction->AddSubAtom( new SemanticKnowledge( wordBase.ConvertNameToId( "WordIdThe" ), FORM_DEFAULT ) );

            float dx = area._center.X() - minArea._center.X();
            float dy = area._center.Y() - minArea._center.Y();
            WordBaseId dir = ExpressDirection( dx, dy, wordBase );
            
            reaction->AddSubAtom( new SemanticKnowledge( dir, FORM_DEFAULT ) );
            reaction->AddSubAtom( new SemanticKnowledge( wordBase.ConvertNameToId( "WordIdOf" ), FORM_DEFAULT ) );
            reaction->AddSubAtom( new SemanticKnowledge( wordBase._wordIdCustomId, nearestName, FORM_DEFAULT, wordBase ) );
          }
          else
          {
            reaction->AddSubAtom( new SemanticKnowledge( wordBase.ConvertNameToId( "WordIdIn" ), FORM_DEFAULT ) );
            reaction->AddSubAtom( new SemanticKnowledge( wordBase._wordIdCustomId, nearestName, FORM_DEFAULT, wordBase ) );
          }
        }
        else
        {
          reaction->AddSubAtom( new SemanticKnowledge( wordBase.ConvertNameToId( "WordIdAt" ), FORM_DEFAULT ) );
          reaction->AddSubAtom( new SemanticKnowledge( wordBase.ConvertNameToId( "WordIdCoordinate" ), FORM_PLURAL ) );
          reaction->AddSubAtom( new SemanticKnowledge( wordBase._wordIdLeftBracketId, FORM_DEFAULT ) );
          reaction->AddSubAtom( new SemanticKnowledge( wordBase._wordIdScalarId, area._center.X(), FORM_DEFAULT ) );
          reaction->AddSubAtom( new SemanticKnowledge( wordBase._wordIdCommaId, FORM_DEFAULT ) );
          reaction->AddSubAtom( new SemanticKnowledge( wordBase._wordIdScalarId, area._center.Y(), FORM_DEFAULT ) );
          reaction->AddSubAtom( new SemanticKnowledge( wordBase._wordIdCommaId, FORM_DEFAULT ) );
          reaction->AddSubAtom( new SemanticKnowledge( wordBase._wordIdScalarId, area._center.Z(), FORM_DEFAULT ) );
          reaction->AddSubAtom( new SemanticKnowledge( wordBase._wordIdRightBracketId, FORM_DEFAULT ) );
        }
      }
      else
      {
        AssertDebug( 0 );
      }
    }
  	break;
  	
  case VT_SPEEDSIZE:
    {
      float kmph = static_cast< float >( value ) / 3.6f;
      kmph = RoundKmph( kmph );
      reaction->AddSubAtom( new SemanticKnowledge( wordBase._wordIdScalarId, kmph, FORM_DEFAULT ) );
      reaction->AddSubAtom( new SemanticKnowledge( wordBase._wordIdKmph, FORM_DEFAULT ) );
    }
  	break;
  	
  case VT_SPEEDDIRECTION:
    {
      if( value.GetType() == GameArray )
      {
        const GameArrayType &arr = value;
        WordBaseId dir = ExpressDirection( arr[ 0 ], arr[ 1 ], wordBase );
        reaction->AddSubAtom( new SemanticKnowledge( wordBase.ConvertNameToId( "WordIdTo" ), FORM_DEFAULT ) );
        reaction->AddSubAtom( new SemanticKnowledge( wordBase.ConvertNameToId( "WordIdThe" ), FORM_DEFAULT ) );
        reaction->AddSubAtom( new SemanticKnowledge( dir, FORM_DEFAULT ) );
      }
    }
    break;

  case VT_ENTITYTYPE:
    {
      RString valueStr = static_cast< RString >( value );
      int n = GetKnowledgesSize( _tableEntityTypes );
      if( n != 0 )
      {
        SemanticQuery entityType( _tableEntityTypes );
        while( entityType.GetIndex() < n )
        {
          RString typeStr = static_cast< RString >( GetEntityType( entityType ) );
          if( typeStr != valueStr )
          {
            entityType.IncIndex();
            continue;
          }
          RString typeName = static_cast< RString >( GetName( entityType ) );
          typeName.Lower();
          reaction->AddSubAtom( new SemanticKnowledge( wordBase._wordIdCustomId, typeName, form, wordBase ) );
          break;
        }
        AssertDebug( entityType.GetIndex() < n );
      }
      else
      {
        reaction->AddSubAtom( new SemanticKnowledge( wordBase._wordIdCustomId, valueStr, form, wordBase ) );
      }
    }
    break;

  case VT_SIDE:
    {
      //... Where to store information about all sides? I would use it here.
      AssertDebug( 0 );
    }
    break;

  case VT_TIME:
    {
      if( value.GetType() == GameTime )
      {
        const TimeInterval &time = static_cast< GameDataTime* >( value.GetData() )->GetTime();
        float mins, hours, days;
        RoundTime( -time._start, mins, hours, days );
        if( days != 0.0f )
        {
          if( days < 2 )
          {
            reaction->AddSubAtom( new SemanticKnowledge( wordBase._wordIdDay, FORM_DEFAULT ) );
          }
          else
          {
            reaction->AddSubAtom( new SemanticKnowledge( wordBase._wordIdScalarId, days, FORM_DEFAULT ) );
            reaction->AddSubAtom( new SemanticKnowledge( wordBase._wordIdDay, FORM_PLURAL ) );
          }
        }
        if( hours != 0.0f )
        {
          if( hours < 2 )
          {
            reaction->AddSubAtom( new SemanticKnowledge( wordBase._wordIdHour, FORM_DEFAULT ) );
          }
          else
          {
            reaction->AddSubAtom( new SemanticKnowledge( wordBase._wordIdScalarId, hours, FORM_DEFAULT ) );
            reaction->AddSubAtom( new SemanticKnowledge( wordBase._wordIdHour, FORM_PLURAL ) );
          }
        }
        if( mins != 0.0f )
        {
          if( mins < 2 )
          {
            reaction->AddSubAtom( new SemanticKnowledge( wordBase._wordIdMinute, FORM_DEFAULT ) );
          }
          else
          {
            reaction->AddSubAtom( new SemanticKnowledge( wordBase._wordIdScalarId, mins, FORM_DEFAULT ) );
            reaction->AddSubAtom( new SemanticKnowledge( wordBase._wordIdMinute, FORM_PLURAL ) );
          }
        }
      }
      else
      {
        AssertDebug( 0 );
      }
    }
    break;

  case VT_STRING:
    {
      GameType gameType = value.GetType();
      if( gameType == GameKnowledge )
      {
        Ref< SemanticKnowledge > knowledge = new SemanticKnowledge( *static_cast< GameDataKnowledge* >( value.GetData() )->GetKnowledge() );
        knowledge->SetForm( form );
        reaction->AddSubAtom( knowledge );
      }
      else if( gameType == GameScalar )
      {
        reaction->AddSubAtom( new SemanticKnowledge( wordBase._wordIdScalarId, static_cast< float >( value ), form ) );
      }
      else
      {
        reaction->AddSubAtom( new SemanticKnowledge( wordBase._wordIdCustomId, static_cast< RString >( value ), form, wordBase ) );
      }
    }
    break;

  default:
    AssertDebug( 0 );
    break;
  }
}

WordBaseId SemanticNetBase::ExpressDirection( float dx, float dy, const WordBase &wordBase )
{
  float adx = ::abs( dx );
  float ady = ::abs( dy );
  if( adx > ady )
  {
    float tg = dy / adx;
    if( dx < 0 )
    {
      if( tg < -0.4142135623730950488016887242097f ) // tg( -22.5 )
      {
        // to the south-west
        return wordBase._wordIdSouthWest;
      }
      else if( tg > 0.4142135623730950488016887242097f ) // tg( 22.5 )
      {
        // to the north-west
        return wordBase._wordIdNorthWest;
      }
      else
      {
        // to the west
        return wordBase._wordIdWest;
      }
    }
    else
    {
      if( tg < -0.4142135623730950488016887242097f ) // tg( -22.5 )
      {
        // to the south-east
        return wordBase._wordIdSouthEast;
      }
      else if( tg > 0.4142135623730950488016887242097f ) // tg( 22.5 )
      {
        // to the north-east
        return wordBase._wordIdNorthEast;
      }
      else
      {
        // to the east
        return wordBase._wordIdEast;
      }
    }
  }
  else
  {
    float tg = dx / ady;
    if( dy < 0 )
    {
      if( tg < -0.4142135623730950488016887242097f ) // tg( -22.5 )
      {
        // to the south-west
        return wordBase._wordIdSouthWest;
      }
      else if( tg > 0.4142135623730950488016887242097f ) // tg( 22.5 )
      {
        // to the south-east
        return wordBase._wordIdSouthEast;
      }
      else
      {
        // to the south
        return wordBase._wordIdSouth;
      }
    }
    else
    {
      if( tg < -0.4142135623730950488016887242097f ) // tg( -22.5 )
      {
        // to the north-west
        return wordBase._wordIdNorthWest;
      }
      else if( tg > 0.4142135623730950488016887242097f ) // tg( 22.5 )
      {
        // to the north-east
        return wordBase._wordIdNorthEast;
      }
      else
      {
        // to the north
        return wordBase._wordIdNorth;
      }
    }
  }
}

DEFINE_FAST_ALLOCATOR( GameDataQuery )

RString GameDataQuery::GetText() const
{
  if( !_query.IsValid() )
  {
    return "<NULL-object>";
  }

  return "Query";
}

bool GameDataQuery::IsEqualTo( const GameData *data ) const
{
  const SemanticQuery &q1 = GetQuery();
  const SemanticQuery &q2 = static_cast< const GameDataQuery* >( data )->GetQuery();
  return q1 == q2;
}

LSError GameDataQuery::Serialize( ParamArchive &ar )
{
  LSError err = base::Serialize( ar );
  if( err != 0 )
  {
    return err;
  }
  return ( LSError ) 0;
}

GameData *CreateGameDataQuery()
{
  return new GameDataQuery();
}

static inline float GetScalar( GameValuePar oper )
{
  if( oper.GetType() == GameKnowledge )
  {
    return static_cast< GameDataKnowledge* >( oper.GetData() )->GetKnowledge()->GetScalar();
  }
  if( oper.GetType() == GameScalar )
  {
    return oper;
  }
  AssertDebug( 0 );
  return 0.0f;
}

static inline RString GetText( GameValuePar oper, const WordBase &wb )
{
  if( oper.GetType() == GameKnowledge )
  {
    RString result;
    static_cast< GameDataKnowledge* >( oper.GetData() )->GetKnowledge()->GetText( &wb, result );
    return result;
  }
  if( oper.GetType() == GameString )
  {
    return oper;
  }
  AssertDebug( 0 );
  return RString();
}

/*! Script operator to get set of candidates from context.
\param oper1 Is type of GameKnowledgeBase.
\param oper2 Is type of GameKnowledge (incoming knowledge).
\return Value of the type GameArray with elements GameQuery.
*/
GameValue KnowledgeBase_ContextItem( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
  const KnowledgeBase *participant;
  if( ( participant = GetKnowledgeBase( oper1 ) ) != NULL )
  {
    const SemanticKnowledge *knowledge;
    if( ( knowledge = GetKnowledge( oper2 ) ) != NULL )
    {
      AutoArray< SemanticQuery > contextItems;
      static_cast< const SemanticNetBase* >( participant->GetSemanticNet() )->GetSemanticContext().TransformIncomingToQueries( *knowledge, contextItems, *participant->GetWordBase() );

      Ref< GameDataArray > results( new GameDataArray() );
      GameArrayType &resultArray = results->GetArray();
      int n = contextItems.Size();
      for( int i = 0; i < n; ++i )
      {
        resultArray.Add( GameValue( new GameDataQuery( contextItems[ i ] ) ) );
      }
      return GameValue( results );
    }
    
    WordBaseId id = participant->GetWordBase()->ConvertNameToId( static_cast< const GameStringType& >( oper2 ) );
    if( id != -1 )
    {
      AutoArray< SemanticQuery > contextItems;
      static_cast< const SemanticNetBase* >( participant->GetSemanticNet() )->GetSemanticContext().TransformIncomingToQueries( id, contextItems, *participant->GetWordBase() );

      Ref< GameDataArray > results( new GameDataArray() );
      GameArrayType &resultArray = results->GetArray();
      int n = contextItems.Size();
      for( int i = 0; i < n; ++i )
      {
        resultArray.Add( GameValue( new GameDataQuery( contextItems[ i ] ) ) );
      }
      return GameValue( results );
    }
  }

  AssertDebug( 0 );
  return GameValue( new GameDataArray() );
}

/*! Unary script operator to get set of enemy sides.
\param oper Is type of GameKnowledgeBase.
\return Value of the type GameArray with elements GameSide.
*/
GameValue KnowledgeBase_EnemySides( const GameState *state, GameValuePar oper )
{
  const KnowledgeBase *participant;
  if( ( participant = GetKnowledgeBase( oper ) ) != NULL )
  {
    return static_cast< const SemanticNetBase* >( participant->GetSemanticNet() )->GetEnemySides();
  }

  AssertDebug( 0 );
  return GameValue( new GameDataArray() );
}

/*! Binary script operator to convert name of place into type GameArea.
\param oper1 Is type of GameKnowledgeBase.
\param oper2 Is type of RString or GameKnowledge.
\return Value of the type GameArea.
*/
GameValue KnowledgeBase_PlaceName( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
  const KnowledgeBase *participant;
  if( ( participant = GetKnowledgeBase( oper1 ) ) != NULL )
  {
    return static_cast< const SemanticNetBase* >( participant->GetSemanticNet() )->GetArea( GetSemanticKey( oper2, *participant->GetWordBase() ) );
  }

  AssertDebug( 0 );
  return GameValue( new GameDataArea() );
}

/*! Binary script operator to convert name of entity type into type entity type id.
\param oper1 Is type of GameKnowledgeBase.
\param oper2 Is type of RString or GameKnowledge.
\return Value of the type GameString.
*/
GameValue KnowledgeBase_EntityTypeName( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
  const KnowledgeBase *participant;
  if( ( participant = GetKnowledgeBase( oper1 ) ) != NULL )
  {
    return static_cast< const SemanticNetBase* >( participant->GetSemanticNet() )->GetEntityType( GetSemanticKey( oper2, *participant->GetWordBase() ) );
  }

  AssertDebug( 0 );
  return GameValue( new GameDataString() );
}

/*! Script operator to calculate time some seconds ago.
\param oper1 Is type of GameKnowledgeBase.
\param oper2 Is type of GameScalar or GameKnowledge.
\return Value of the type GameTime.
*/
GameValue KnowledgeBase_TimeLastSecs( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
  const KnowledgeBase *participant;
  if( ( participant = GetKnowledgeBase( oper1 ) ) != NULL )
  {
    return static_cast< const SemanticNetBase* >( participant->GetSemanticNet() )->GetTimeLastSecs( GetScalar( oper2 ) );
  }

  AssertDebug( 0 );
  return GameValue( new GameDataTime() );
}

/*! Script operator to get the last seen time of particular candidate.
\param oper1 Is type of GameKnowledgeBase.
\param oper2 Is type of GameQuery (candidate).
\return Value of the type GameTime.
*/
GameValue KnowledgeBase_LastSeenTime( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
  const SemanticQuery *candidate;
  const KnowledgeBase *participant;
  if( ( candidate = GetQuery( oper2 ) ) != NULL && ( participant = GetKnowledgeBase( oper1 ) ) != NULL )
  {
    return static_cast< const SemanticNetBase* >( participant->GetSemanticNet() )->GetLastSeenTime( *candidate );
  }

  AssertDebug( 0 );
  return GameValue( new GameDataTime() );
}

/*! Script operator to get the name of particular candidate.
\param oper1 Is type of GameKnowledgeBase.
\param oper2 Is type of GameQuery (candidate).
\return Value of the type GameString.
*/
GameValue KnowledgeBase_GetName( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
  const SemanticQuery *candidate;
  const KnowledgeBase *participant;
  if( ( candidate = GetQuery( oper2 ) ) != NULL && ( participant = GetKnowledgeBase( oper1 ) ) != NULL )
  {
    return static_cast< const SemanticNetBase* >( participant->GetSemanticNet() )->GetName( *candidate );
  }

  AssertDebug( 0 );
  return GameValue( new GameDataString() );
}

/*! Script operator to get the unit type of particular candidate.
\param oper1 Is type of GameKnowledgeBase.
\param oper2 Is type of GameQuery (candidate).
\return Value of the type GameString.
*/
GameValue KnowledgeBase_UnitType( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
  const SemanticQuery *candidate;
  const KnowledgeBase *participant;
  if( ( candidate = GetQuery( oper2 ) ) != NULL && ( participant = GetKnowledgeBase( oper1 ) ) != NULL )
  {
    return static_cast< const SemanticNetBase* >( participant->GetSemanticNet() )->GetUnitType( *candidate );
  }

  AssertDebug( 0 );
  return GameValue( new GameDataString() );
}

/*! Script operator to get the last seen place of particular candidate.
\param oper1 Is type of GameKnowledgeBase.
\param oper2 Is type of GameQuery (candidate).
\return Value of the type GameArea.
*/
GameValue KnowledgeBase_LastSeenPlace( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
  const SemanticQuery *candidate;
  const KnowledgeBase *participant;
  if( ( candidate = GetQuery( oper2 ) ) != NULL && ( participant = GetKnowledgeBase( oper1 ) ) != NULL )
  {
    return static_cast< const SemanticNetBase* >( participant->GetSemanticNet() )->GetLastSeenPlace( *candidate );
  }

  AssertDebug( 0 );
  return GameValue( new GameDataArea() );
}

/*! Script operator to get the speed size of particular candidate.
\param oper1 Is type of GameKnowledgeBase.
\param oper2 Is type of GameQuery (candidate).
\return Value of the type GameScalar.
*/
GameValue KnowledgeBase_SpeedSize( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
  const SemanticQuery *candidate;
  const KnowledgeBase *participant;
  if( ( candidate = GetQuery( oper2 ) ) != NULL && ( participant = GetKnowledgeBase( oper1 ) ) != NULL )
  {
    return static_cast< const SemanticNetBase* >( participant->GetSemanticNet() )->GetSpeedSize( *candidate );
  }

  AssertDebug( 0 );
  return GameValue( new GameDataScalar() );
}

/*! Script operator to compare two sides.
\param oper1 Is type of GameKnowledgeBase.
\param oper2 Is type of GameArray with two GameSide.
\return Value of the type GameScalar.
*/
GameValue KnowledgeBase_SideEqual( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
  const KnowledgeBase *participant;
  if( ( participant = GetKnowledgeBase( oper1 ) ) != NULL )
  {
    const GameArrayType &arr = oper2;
    if( arr.Size() == 2 )
    {
      return static_cast< const SemanticNetBase* >( participant->GetSemanticNet() )->SideEqual( arr[ 0 ], arr[ 1 ] );
    }
  }

  AssertDebug( 0 );
  return GameValue( new GameDataScalar() );
}

/*! Script operator to compare two times (greater or equal than).
\param oper1 Is type of GameKnowledgeBase.
\param oper2 Is type of GameArray with two GameTime.
\return Value of the type GameScalar.
*/
GameValue KnowledgeBase_TimeIntersected( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
  const KnowledgeBase *participant;
  if( ( participant = GetKnowledgeBase( oper1 ) ) != NULL )
  {
    const GameArrayType &arr = oper2;
    if( arr.Size() == 2 )
    {
      return static_cast< const SemanticNetBase* >( participant->GetSemanticNet() )->TimeIntersected( arr[ 0 ], arr[ 1 ] );
    }
  }

  AssertDebug( 0 );
  return GameValue( new GameDataScalar() );
}

/*! Script operator to find if one type is subtype of the other.
\param oper1 Is type of GameKnowledgeBase.
\param oper2 Is type of GameArray with two entity types stored in GameString-s.
\return Value of the type GameScalar.
*/
GameValue KnowledgeBase_UnitSubtype( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
  const KnowledgeBase *participant;
  if( ( participant = GetKnowledgeBase( oper1 ) ) != NULL )
  {
    const GameArrayType &arr = oper2;
    if( arr.Size() == 2 )
    {
      return static_cast< const SemanticNetBase* >( participant->GetSemanticNet() )->UnitSubtype( 
        static_cast< RString >( arr[ 0 ] ), static_cast< RString >( arr[ 1 ] ) );
    }
  }

  AssertDebug( 0 );
  return GameValue( new GameDataScalar() );
}

/*! Script operator to determine if the string is empty.
\param oper1 Is type of GameKnowledgeBase.
\param oper2 Is type of GameString or GameKnowledge.
\return Value of the type GameScalar.
*/
GameValue KnowledgeBase_NotEmpty( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
  const KnowledgeBase *participant;
  if( ( participant = GetKnowledgeBase( oper1 ) ) != NULL )
  {
    RString txt = GetText( oper2, *participant->GetWordBase() );
    return txt.GetRefCount() != 0 ? 1.0f : 0.0f;
  }
  
  return GameValue();
}

/*! Script operator to compare two times.
\param oper1 Is type of GameKnowledgeBase.
\param oper2 Is type of GameArray with two GameTime.
\return Value of the type GameScalar.
*/
GameValue KnowledgeBase_TimeEqual( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
  const KnowledgeBase *participant;
  if( ( participant = GetKnowledgeBase( oper1 ) ) != NULL )
  {
    const GameArrayType &arr = oper2;
    if( arr.Size() == 2 )
    {
      return static_cast< const SemanticNetBase* >( participant->GetSemanticNet() )->TimeEqual( arr[ 0 ], arr[ 1 ] );
    }
  }

  AssertDebug( 0 );
  return GameValue( new GameDataScalar() );
}

/*! Script operator to compare two names.
\param oper1 Is type of GameKnowledgeBase.
\param oper2 Is type of GameArray with two GameString or GameKnowledge.
\return Value of the type GameScalar.
*/
GameValue KnowledgeBase_NameEqual( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
  const KnowledgeBase *participant;
  if( ( participant = GetKnowledgeBase( oper1 ) ) != NULL )
  {
    const GameArrayType &arr = oper2;
    if( arr.Size() == 2 )
    {
      const WordBase &wb = *participant->GetWordBase();
      return static_cast< const SemanticNetBase* >( participant->GetSemanticNet() )->NameEqual( GetSemanticKey( arr[ 0 ], wb ), GetSemanticKey( arr[ 1 ], wb ) );
    }
  }

  AssertDebug( 0 );
  return GameValue( new GameDataScalar() );
}

/*! Script operator to compare two unit types.
\param oper1 Is type of GameKnowledgeBase.
\param oper2 Is type of GameArray with two GameString or GameKnowledge.
\return Value of the type GameScalar.
*/
GameValue KnowledgeBase_UnitEqual( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
  const KnowledgeBase *participant;
  if( ( participant = GetKnowledgeBase( oper1 ) ) != NULL )
  {
    const GameArrayType &arr = oper2;
    if( arr.Size() == 2 )
    {
      const WordBase &wb = *participant->GetWordBase();
      return static_cast< const SemanticNetBase* >( participant->GetSemanticNet() )->UnitEqual( GetSemanticKey( arr[ 0 ], wb ), GetSemanticKey( arr[ 1 ], wb ) );
    }
  }

  AssertDebug( 0 );
  return GameValue( new GameDataScalar() );
}

/*! Script operator to compare two areas.
\param oper1 Is type of GameKnowledgeBase.
\param oper2 Is type of GameArray with two GameArea.
\return Value of the type GameScalar.
*/
GameValue KnowledgeBase_PlaceEqual( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
  const KnowledgeBase *participant;
  if( ( participant = GetKnowledgeBase( oper1 ) ) != NULL )
  {
    const GameArrayType &arr = oper2;
    if( arr.Size() == 2 )
    {
      return static_cast< const SemanticNetBase* >( participant->GetSemanticNet() )->PlaceEqual( arr[ 0 ], arr[ 1 ] );
    }
  }

  AssertDebug( 0 );
  return GameValue( new GameDataScalar() );
}

/*! Script operator to compare two speed values.
\param oper1 Is type of GameKnowledgeBase.
\param oper2 Is type of GameArray with two GameSpeed.
\return Value of the type GameScalar.
*/
GameValue KnowledgeBase_SpeedSizeEqual( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
  const KnowledgeBase *participant;
  if( ( participant = GetKnowledgeBase( oper1 ) ) != NULL )
  {
    const GameArrayType &arr = oper2;
    if( arr.Size() == 2 )
    {
      return static_cast< const SemanticNetBase* >( participant->GetSemanticNet() )->SpeedSizeEqual( arr[ 0 ], arr[ 1 ] );
    }
  }

  AssertDebug( 0 );
  return GameValue( new GameDataScalar() );
}

/*! Script operator to compare two speed values.
\param oper1 Is type of GameKnowledgeBase.
\param oper2 Is type of GameArray with two GameSpeed.
\return Value of the type GameScalar.
*/
GameValue KnowledgeBase_SpeedDirectionEqual( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
  const KnowledgeBase *participant;
  if( ( participant = GetKnowledgeBase( oper1 ) ) != NULL )
  {
    const GameArrayType &arr = oper2;
    if( arr.Size() == 2 )
    {
      return static_cast< const SemanticNetBase* >( participant->GetSemanticNet() )->SpeedDirectionEqual( arr[ 0 ], arr[ 1 ] );
    }
  }

  AssertDebug( 0 );
  return GameValue( new GameDataScalar() );
}

/*! Script operator to find predecessor of the times.
\param oper1 Is type of GameKnowledgeBase.
\param oper2 Is type of GameArray with GameTime elements.
\return Value of the type GameTime.
*/
GameValue KnowledgeBase_TimePred( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
  const KnowledgeBase *participant;
  if( ( participant = GetKnowledgeBase( oper1 ) ) != NULL )
  {
    return static_cast< const SemanticNetBase* >( participant->GetSemanticNet() )->TimePred( oper2 );
  }

  AssertDebug( 0 );
  return GameValue( new GameDataTime() );
}

/*! Script operator to find predecessor of the names.
\param oper1 Is type of GameKnowledgeBase.
\param oper2 Is type of GameArray with GameString or GameKnowledge elements.
\return Value of the type GameString.
*/
GameValue KnowledgeBase_NamePred( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
  const KnowledgeBase *participant;
  if( ( participant = GetKnowledgeBase( oper1 ) ) != NULL )
  {
    return static_cast< const SemanticNetBase* >( participant->GetSemanticNet() )->NamePred( 
      *participant->GetWordBase(), oper2 );
  }

  AssertDebug( 0 );
  return GameValue( new GameDataString() );
}

/*! Script operator to find predecessor of the unit types.
\param oper1 Is type of GameKnowledgeBase.
\param oper2 Is type of GameArray with entity type (GameString) or GameKnowledge elements.
\return Value of the type GameSide.
*/
GameValue KnowledgeBase_UnitPred( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
  const KnowledgeBase *participant;
  if( ( participant = GetKnowledgeBase( oper1 ) ) != NULL )
  {
    return static_cast< const SemanticNetBase* >( participant->GetSemanticNet() )->UnitPred( 
      *participant->GetWordBase(), oper2 );
  }

  AssertDebug( 0 );
  return GameValue( new GameDataString() );
}

/*! Script operator to find predecessor of the areas.
\param oper1 Is type of GameKnowledgeBase.
\param oper2 Is type of GameArray with GameArea elements.
\return Value of the type GameArea.
*/
GameValue KnowledgeBase_PlacePred( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
  const KnowledgeBase *participant;
  if( ( participant = GetKnowledgeBase( oper1 ) ) != NULL )
  {
    return static_cast< const SemanticNetBase* >( participant->GetSemanticNet() )->PlacePred( oper2 );
  }

  AssertDebug( 0 );
  return GameValue( new GameDataArea() );
}

/*! Script operator to find predecessor of the speeds.
\param oper1 Is type of GameKnowledgeBase.
\param oper2 Is type of GameArray with GameSpeed elements.
\return Value of the type GameArea.
*/
GameValue KnowledgeBase_SpeedSizePred( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
  const KnowledgeBase *participant;
  if( ( participant = GetKnowledgeBase( oper1 ) ) != NULL )
  {
    return static_cast< const SemanticNetBase* >( participant->GetSemanticNet() )->SpeedSizePred( oper2 );
  }

  AssertDebug( 0 );
  return GameValue( new GameDataArea() );
}

/*! Script operator to find predecessor of the speeds.
\param oper1 Is type of GameKnowledgeBase.
\param oper2 Is type of GameArray with GameSpeed elements.
\return Value of the type GameArea.
*/
GameValue KnowledgeBase_SpeedDirectionPred( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
  const KnowledgeBase *participant;
  if( ( participant = GetKnowledgeBase( oper1 ) ) != NULL )
  {
    return static_cast< const SemanticNetBase* >( participant->GetSemanticNet() )->SpeedDirectionPred( oper2 );
  }

  AssertDebug( 0 );
  return GameValue( new GameDataArea() );
}

GameValue KnowledgeBase_TransferEnemySide( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
  const KnowledgeBase *participant;
  if( ( participant = GetKnowledgeBase( oper1 ) ) != NULL )
  {
    const GameArrayType &arr = oper2;
    if( arr.Size() == 3 )
    {
      static_cast< const SemanticNetBase* >( participant->GetSemanticNet() )->TransferEnemySide( arr[ 0 ], arr[ 1 ], arr[ 2 ] );
      return GameValue();
    }
  }

  AssertDebug( 0 );
  return GameValue();
}

GameValue KnowledgeBase_TransferPlace( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
  const KnowledgeBase *participant;
  if( ( participant = GetKnowledgeBase( oper1 ) ) != NULL )
  {
    const GameArrayType &arr = oper2;
    if( arr.Size() == 3 )
    {
      static_cast< const SemanticNetBase* >( participant->GetSemanticNet() )->TransferPlace( arr[ 0 ], arr[ 1 ], arr[ 2 ] );
      return GameValue();
    }
  }

  AssertDebug( 0 );
  return GameValue();
}

GameValue KnowledgeBase_TransferType( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
  const KnowledgeBase *participant;
  if( ( participant = GetKnowledgeBase( oper1 ) ) != NULL )
  {
    const GameArrayType &arr = oper2;
    if( arr.Size() == 3 )
    {
      static_cast< const SemanticNetBase* >( participant->GetSemanticNet() )->TransferType( arr[ 0 ], arr[ 1 ], arr[ 2 ] );
      return GameValue();
    }
  }

  AssertDebug( 0 );
  return GameValue();
}

GameValue KnowledgeBase_TransferTime( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
  const KnowledgeBase *participant;
  if( ( participant = GetKnowledgeBase( oper1 ) ) != NULL )
  {
    const GameArrayType &arr = oper2;
    if( arr.Size() == 3 )
    {
      static_cast< const SemanticNetBase* >( participant->GetSemanticNet() )->TransferTime( arr[ 0 ], arr[ 1 ], arr[ 2 ] );
      return GameValue();
    }
  }

  AssertDebug( 0 );
  return GameValue();
}

GameValue KnowledgeBase_TransferName( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
  const KnowledgeBase *participant;
  if( ( participant = GetKnowledgeBase( oper1 ) ) != NULL )
  {
    const GameArrayType &arr = oper2;
    if( arr.Size() == 3 )
    {
      static_cast< const SemanticNetBase* >( participant->GetSemanticNet() )->TransferName( arr[ 0 ], arr[ 1 ], arr[ 2 ] );
      return GameValue();
    }
  }

  AssertDebug( 0 );
  return GameValue();
}

GameValue KnowledgeBase_TransferSpeed( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
  const KnowledgeBase *participant;
  if( ( participant = GetKnowledgeBase( oper1 ) ) != NULL )
  {
    const GameArrayType &arr = oper2;
    if( arr.Size() == 3 )
    {
      static_cast< const SemanticNetBase* >( participant->GetSemanticNet() )->TransferSpeed( arr[ 0 ], arr[ 1 ], arr[ 2 ] );
      return GameValue();
    }
  }

  AssertDebug( 0 );
  return GameValue();
}

//! Math operations.
GameValue Attribute_Power( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
  float right = GetScalar( oper2 );
  float left = GetScalar( oper1 );
  float result = ( float ) pow ( left, right );
  return GameValue( new GameDataScalar( result ) );
}

GameValue Attribute_Product( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
  float right = GetScalar( oper2 );
  float left = GetScalar( oper1 );
  float result = left * right;
  return GameValue( new GameDataScalar( result ) );
}

GameValue Attribute_Quotient( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
  float right = GetScalar( oper2 );
  float left = GetScalar( oper1 );
  float result;
  if( right != 0 )
  {
    result = left / right;
  }
  else
  {
    state->SetError( EvalDivZero );
    result = 0.0f;
  }
  return GameValue( new GameDataScalar( result ) );
}

GameValue Attribute_Remainder( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
  float right = GetScalar( oper2 );
  float left = GetScalar( oper1 );
  float result;
  if( right != 0 )
  {
    result = ( float ) fmod ( left, right );
  }
  else
  {
    state->SetError( EvalDivZero );
    result = 0.0f;
  }
  return GameValue( new GameDataScalar( result ) );
}

GameValue Attribute_Sum( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
  float right = GetScalar( oper2 );
  float left = GetScalar( oper1 );
  float result = left + right;
  return GameValue( new GameDataScalar( result ) );
}

GameValue Attribute_Difference( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
  float right = GetScalar( oper2 );
  float left = GetScalar( oper1 );
  float result = left - right;
  return GameValue( new GameDataScalar( result ) );
}

INIT_MODULE( SemanticNetBaseMain, 3 )
{
  GGameState.NewType( "QUERY", GameQuery, CreateGameDataQuery, "Query" );

  // 'Candidates' operators.
  GGameState.NewOperator( GameOperator( GameArray,      "contextItem",        function, KnowledgeBase_ContextItem,     GameKnowledgeBase, GameKnowledge ) );
  GGameState.NewOperator( GameOperator( GameArray,      "contextItem",        function, KnowledgeBase_ContextItem,     GameKnowledgeBase, GameString ) );

  // 'Constant' operators.
  GGameState.NewFunction( GameFunction( GameArray,      "enemySides",                   KnowledgeBase_EnemySides,      GameKnowledgeBase ) );
  GGameState.NewOperator( GameOperator( GameArea,       "placeName",          function, KnowledgeBase_PlaceName,       GameKnowledgeBase, GameKnowledge ) );
  GGameState.NewOperator( GameOperator( GameArea,       "placeName",          function, KnowledgeBase_PlaceName,       GameKnowledgeBase, GameString ) );
  GGameState.NewOperator( GameOperator( GameTime,       "timeLastSecs",       function, KnowledgeBase_TimeLastSecs,    GameKnowledgeBase, GameKnowledge ) );
  GGameState.NewOperator( GameOperator( GameTime,       "timeLastSecs",       function, KnowledgeBase_TimeLastSecs,    GameKnowledgeBase, GameScalar ) );
  GGameState.NewOperator( GameOperator( GameString,     "entityTypeName",     function, KnowledgeBase_EntityTypeName,  GameKnowledgeBase, GameKnowledge ) );
  GGameState.NewOperator( GameOperator( GameString,     "entityTypeName",     function, KnowledgeBase_EntityTypeName,  GameKnowledgeBase, GameString ) );

  // 'Variable' operators.
  GGameState.NewOperator( GameOperator( GameTime,       "lastSeenTime",       function, KnowledgeBase_LastSeenTime,    GameKnowledgeBase, GameQuery ) );
  GGameState.NewOperator( GameOperator( GameString,     "getName",            function, KnowledgeBase_GetName,         GameKnowledgeBase, GameQuery ) );
  GGameState.NewOperator( GameOperator( GameString,     "unitType",           function, KnowledgeBase_UnitType,        GameKnowledgeBase, GameQuery ) );
  GGameState.NewOperator( GameOperator( GameArea,       "lastSeenPlace",      function, KnowledgeBase_LastSeenPlace,   GameKnowledgeBase, GameQuery ) );
  GGameState.NewOperator( GameOperator( GameScalar,     "speedSize",          function, KnowledgeBase_SpeedSize,       GameKnowledgeBase, GameQuery ) );

  // 'Condition' operators.
  GGameState.NewOperator( GameOperator( GameScalar,     "timeIntersected",    function, KnowledgeBase_TimeIntersected, GameKnowledgeBase, GameArray ) );
  GGameState.NewOperator( GameOperator( GameScalar,     "unitSubtype",        function, KnowledgeBase_UnitSubtype,     GameKnowledgeBase, GameArray ) );
  GGameState.NewOperator( GameOperator( GameScalar,     "notEmpty",           function, KnowledgeBase_NotEmpty,        GameKnowledgeBase, GameString ) );
  GGameState.NewOperator( GameOperator( GameScalar,     "notEmpty",           function, KnowledgeBase_NotEmpty,        GameKnowledgeBase, GameKnowledge ) );

  // 'Metrics' operators.
  GGameState.NewOperator( GameOperator( GameScalar,     "sideEqual",          function, KnowledgeBase_SideEqual,       GameKnowledgeBase, GameArray ) );
  GGameState.NewOperator( GameOperator( GameScalar,     "timeEqual",          function, KnowledgeBase_TimeEqual,       GameKnowledgeBase, GameArray ) );
  GGameState.NewOperator( GameOperator( GameScalar,     "nameEqual",          function, KnowledgeBase_NameEqual,       GameKnowledgeBase, GameArray ) );
  GGameState.NewOperator( GameOperator( GameScalar,     "unitEqual",          function, KnowledgeBase_UnitEqual,       GameKnowledgeBase, GameArray ) );
  GGameState.NewOperator( GameOperator( GameScalar,     "placeEqual",         function, KnowledgeBase_PlaceEqual,      GameKnowledgeBase, GameArray ) );
  GGameState.NewOperator( GameOperator( GameScalar,     "speedSizeEqual",     function, KnowledgeBase_SpeedSizeEqual,  GameKnowledgeBase, GameArray ) );
  GGameState.NewOperator( GameOperator( GameScalar,     "speedDirectionEqual",function, KnowledgeBase_SpeedDirectionEqual,GameKnowledgeBase,GameArray ) );

  // 'Predecessor' operator.
  GGameState.NewOperator( GameOperator( GameTime,       "timePred",           function, KnowledgeBase_TimePred,        GameKnowledgeBase, GameArray ) );
  GGameState.NewOperator( GameOperator( GameString,     "namePred",           function, KnowledgeBase_NamePred,        GameKnowledgeBase, GameArray ) );
  GGameState.NewOperator( GameOperator( GameString,     "unitPred",           function, KnowledgeBase_UnitPred,        GameKnowledgeBase, GameArray ) );
  GGameState.NewOperator( GameOperator( GameArray,      "placePred",          function, KnowledgeBase_PlacePred,       GameKnowledgeBase, GameArray ) );
  GGameState.NewOperator( GameOperator( GameArray,      "speedSizePred",      function, KnowledgeBase_SpeedSizePred,   GameKnowledgeBase, GameArray ) );
  GGameState.NewOperator( GameOperator( GameArray,      "speedDirectionPred", function, KnowledgeBase_SpeedDirectionPred,GameKnowledgeBase,GameArray ) );
  
  // 'Transfer' operators.
  GGameState.NewOperator( GameOperator( GameVoid,       "transferEnemySide",  function, KnowledgeBase_TransferEnemySide, GameKnowledgeBase, GameArray ) );
  GGameState.NewOperator( GameOperator( GameVoid,       "transferPlace",      function, KnowledgeBase_TransferPlace,   GameKnowledgeBase, GameArray ) );
  GGameState.NewOperator( GameOperator( GameVoid,       "transferType",       function, KnowledgeBase_TransferType,    GameKnowledgeBase, GameArray ) );
  GGameState.NewOperator( GameOperator( GameVoid,       "transferTime",       function, KnowledgeBase_TransferTime,    GameKnowledgeBase, GameArray ) );
  GGameState.NewOperator( GameOperator( GameVoid,       "transferName",       function, KnowledgeBase_TransferName,    GameKnowledgeBase, GameArray ) );
  GGameState.NewOperator( GameOperator( GameVoid,       "transferSpeed",      function, KnowledgeBase_TransferSpeed,   GameKnowledgeBase, GameArray ) );

  // Math operators.
  GGameState.NewOperator( GameOperator( GameScalar,     "^",                  mocnina,  Attribute_Power,         GameKnowledge,      GameKnowledge ) );
  GGameState.NewOperator( GameOperator( GameScalar,     "^",                  mocnina,  Attribute_Power,         GameKnowledge,      GameScalar    ) );
  GGameState.NewOperator( GameOperator( GameScalar,     "^",                  mocnina,  Attribute_Power,         GameScalar,         GameKnowledge ) );

  GGameState.NewOperator( GameOperator( GameScalar,     "*",                  soucin,   Attribute_Product,       GameKnowledge,      GameKnowledge ) );
  GGameState.NewOperator( GameOperator( GameScalar,     "*",                  soucin,   Attribute_Product,       GameKnowledge,      GameScalar    ) );
  GGameState.NewOperator( GameOperator( GameScalar,     "*",                  soucin,   Attribute_Product,       GameScalar,         GameKnowledge ) );

  GGameState.NewOperator( GameOperator( GameScalar,     "/",                  soucin,   Attribute_Quotient,      GameKnowledge,      GameKnowledge ) );
  GGameState.NewOperator( GameOperator( GameScalar,     "/",                  soucin,   Attribute_Quotient,      GameKnowledge,      GameScalar    ) );
  GGameState.NewOperator( GameOperator( GameScalar,     "/",                  soucin,   Attribute_Quotient,      GameScalar,         GameKnowledge ) );

  GGameState.NewOperator( GameOperator( GameScalar,     "%",                  soucin,   Attribute_Remainder,     GameKnowledge,      GameKnowledge ) );
  GGameState.NewOperator( GameOperator( GameScalar,     "%",                  soucin,   Attribute_Remainder,     GameKnowledge,      GameScalar    ) );
  GGameState.NewOperator( GameOperator( GameScalar,     "%",                  soucin,   Attribute_Remainder,     GameScalar,         GameKnowledge ) );

  GGameState.NewOperator( GameOperator( GameScalar,     "+",                  soucet,   Attribute_Sum,           GameKnowledge,      GameKnowledge ) );
  GGameState.NewOperator( GameOperator( GameScalar,     "+",                  soucet,   Attribute_Sum,           GameKnowledge,      GameScalar    ) );
  GGameState.NewOperator( GameOperator( GameScalar,     "+",                  soucet,   Attribute_Sum,           GameScalar,         GameKnowledge ) );

  GGameState.NewOperator( GameOperator( GameScalar,     "-",                  soucet,   Attribute_Difference,    GameKnowledge,      GameKnowledge ) );
  GGameState.NewOperator( GameOperator( GameScalar,     "-",                  soucet,   Attribute_Difference,    GameKnowledge,      GameScalar    ) );
  GGameState.NewOperator( GameOperator( GameScalar,     "-",                  soucet,   Attribute_Difference,    GameScalar,         GameKnowledge ) );
};

GameValue SemanticNetBase::TimeEqual( GameValuePar left, GameValuePar right ) const
{
  const TimeInterval *t0;
  const TimeInterval *t1;
  if( ( t0 = GetTime( left ) ) != NULL && ( t1 = GetTime( right ) ) != NULL )
  {
    float mins, hours, days;
    RoundTime( -t0->_start, mins, hours, days );
    float startMinutes0 = days * 1440 + hours * 60.0f + mins;
    RoundTime( -t0->_end, mins, hours, days );
    float endMinutes0 = days * 1440 + hours * 60.0f + mins;
    RoundTime( -t1->_start, mins, hours, days );
    float startMinutes1 = days * 1440 + hours * 60.0f + mins;
    RoundTime( -t1->_end, mins, hours, days );
    float endMinutes1 = days * 1440 + hours * 60.0f + mins;

    float startSimilarity = startMinutes1 < startMinutes0 ? startMinutes1 / startMinutes0 : startMinutes0 / startMinutes1;
    float endSimilarity = endMinutes1 < endMinutes0 ? endMinutes1 / endMinutes0 : endMinutes0 / endMinutes1;
    // Final similarity is the average of the two.
    return GameValue( new GameDataScalar( ( startSimilarity + endSimilarity ) * 0.5f ) );
  }
  return GameValue( new GameDataScalar() );
}

GameValue SemanticNetBase::TimeIntersected( GameValuePar left, GameValuePar right ) const
{
  const TimeInterval *t0;
  const TimeInterval *t1;
  if( ( t0 = GetTime( left ) ) != NULL && ( t1 = GetTime( right ) ) != NULL )
  {
    float start = t0->_start < t1->_start ? t1->_start : t0->_start;
    float end = t0->_end > t1->_end ? t1->_end : t0->_end;
    return GameValue( new GameDataScalar( end >= start ? 1.0f : 0.0f ) );
  }
  return GameValue( new GameDataScalar() );
}

GameValue SemanticNetBase::NameEqual( const RString left, const RString right ) const
{
  return GameValue( new GameDataScalar( left == right ? 1.0f : 0.0f ) );
}

GameValue SemanticNetBase::PlaceEqual( GameValuePar left, GameValuePar right ) const
{
  const PlaceArea *a0;
  const PlaceArea *a1;
  if( ( a0 = ::GetArea( left ) ) != NULL && ( a1 = ::GetArea( right ) ) != NULL )
  {
    float dx = a0->_center.X() - a1->_center.X();
    float dy = a0->_center.Y() - a1->_center.Y();
    float d = ::sqrt( dx * dx + dy * dy );
    float dev = a0->_deviation + a1->_deviation;
    d = d > dev ? d - dev : 0.0f;
    float meters, kms;
    RoundDistance( d, meters, kms );
    d = kms * 1000.0f + meters;
    float result = 1.0f / ( 1.0f + 0.005f * d ); // Similarity comes down to 0.5 in 200 metes.
//#ifdef _DEBUG
//    BString< 256 > buf;
//    buf.PrintF( "PlaceEqual: %g", result );
//    Log( buf );
//#endif
    return GameValue( new GameDataScalar( result ) );
  }
  
  AssertDebug( 0 );
  return GameValue( new GameDataScalar() );
}

GameValue SemanticNetBase::SpeedSizeEqual( GameValuePar left, GameValuePar right ) const
{
  float defSp = static_cast< float >( left ) - static_cast< float >( right );
  defSp = defSp / 3.6; // mps -> kmph
  defSp = RoundKmph( defSp );
  float result = 1.0f / ( 1.0f + 0.033f * defSp ); // Similarity comes down to 0.5 in 30 kmph.
  return GameValue( new GameDataScalar( result ) );
}

GameValue SemanticNetBase::SpeedDirectionEqual( GameValuePar left, GameValuePar right ) const
{
  if( left.GetType() == GameArray && right.GetType() == GameArray )
  {
    const GameArrayType &leftArr = left;
    const GameArrayType &rightArr = right;
    Vector3 sp1( leftArr[ 0 ], leftArr[ 1 ], 0.0f );
    Vector3 sp2( rightArr[ 0 ], rightArr[ 1 ], 0.0f );
    if( sp1.X() == 0.0f && sp1.Y() == 0.0f || sp2.X() == 0.0f && sp2.Y() == 0.0f )
    {
      return GameValue( new GameDataScalar( 0.0f ) );
    }
    sp1.Normalize();
    sp2.Normalize();
    Coord dp = sp1.DotProduct( sp2 );
    return GameValue( new GameDataScalar( ( dp + 1 ) * 0.5f ) );
  }
  return GameValue( new GameDataScalar() );
}

GameValue SemanticNetBase::TimePred( GameValuePar times ) const
{
  const GameArrayType &arr = times;
  int n = arr.Size();
  if( n != 0 )
  {
    TimeInterval result;
    int i = 0;
    while( i < n )
    {
      const TimeInterval *ti = ::GetTime( arr[ i ] );
      ++i;
      if( ti != NULL )
      {
        result = *ti;
        break;
      }
    }
    for( ; i < n; ++i )
    {
      const TimeInterval *ti = ::GetTime( arr[ i ] );
      if( ti != NULL )
      {
        result.Add( *ti );
      }
    }
    return GameValue( new GameDataTime( result ) );
  }
  return GameValue( new GameDataTime( TimeInterval() ) );
}

GameValue SemanticNetBase::NamePred( const WordBase &wb, GameValuePar names ) const
{
  const GameArrayType &arr = names;
  int n = arr.Size();
  if( n != 0 )
  {
    RString result( GetSemanticKey( arr[ 0 ], wb ) );
    int i = 1;
    while( i < n )
    {
      if( result == GetSemanticKey( arr[ i ], wb ) )
      {
        ++i;
        continue;
      }
      return GameValue( new GameDataString( RString( "Unknown" ) ) );
    }
    return GameValue( new GameDataString( result ) );
  }
  return GameValue( new GameDataString( RString( "Unknown" ) ) );
}

GameValue SemanticNetBase::PlacePred( GameValuePar areas ) const
{
  const GameArrayType &arr = areas;
  int n = arr.Size();
  if( n != 0 )
  {
    PlaceArea result;
    int i = 0;
    for( ;; )
    {
      if( i < n )
      {
        const PlaceArea *ai = ::GetArea( arr[ i ] );
        ++i;
        if( ai != NULL )
        {
          result = *ai;
          break;
        }
      }
      else
      {
        return GameValue( new GameDataArea() );
      }
    }
    for( ; i < n; ++i )
    {
      const PlaceArea *ai = ::GetArea( arr[ i ] );
      if( ai != NULL )
      {
        result.Add( *ai );
      }
    }
    return GameValue( new GameDataArea( result ) );
  }
  return GameValue( new GameDataArea() );
}

GameValue SemanticNetBase::SpeedSizePred( GameValuePar speeds ) const
{
  //...
  AssertDebug( 0 );
  return GameValue( new GameDataScalar() );
}

GameValue SemanticNetBase::SpeedDirectionPred( GameValuePar speeds ) const
{
  if( speeds.GetType() == GameArray )
  {
    float x = 0.0f, y = 0.0f, z = 0.0f, n = 0.0f;
    const GameArrayType &srcArr = speeds;
    int srcArrN = srcArr.Size();
    for( int srcArrI = 0; srcArrI < srcArrN; ++srcArrI )
    {
    	const GameValue &sp = srcArr[ srcArrI ];
    	if( sp.GetType() == GameArray )
    	{
    	  const GameArrayType &spArr = sp;
    	  Vector3 spDir( spArr[ 0 ], spArr[ 1 ], 0.0f );
    	  if( !( spDir.X() == 0.0f && spDir.Y() == 0.0f ) )
    	  {
          spDir.Normalize();
          x = n / ( n + 1.0f ) * x + 1 / ( n + 1.0f ) * spDir.X();
          y = n / ( n + 1.0f ) * y + 1 / ( n + 1.0f ) * spDir.Y();
          n += 1.0f;
    	  }
    	}
    }
    
    Ref< GameDataArray > dataArr = new GameDataArray();
    GameArrayType &arr = dataArr->GetArray();
    arr.Add( GameValue( new GameDataScalar( x ) ) );
    arr.Add( GameValue( new GameDataScalar( y ) ) );
    arr.Add( GameValue( new GameDataScalar( z ) ) );
    return GameValue( dataArr );
  }
  
  Ref< GameDataArray > dataArr = new GameDataArray();
  GameArrayType &arr = dataArr->GetArray();
  arr.Add( GameValue( new GameDataScalar( 0.0f ) ) );
  arr.Add( GameValue( new GameDataScalar( 0.0f ) ) );
  arr.Add( GameValue( new GameDataScalar( 0.0f ) ) );
  return GameValue( dataArr );
}

void SemanticNetBase::FilterDefinedPronouns( const WordBase &wb, AutoArray< WordBaseId > &outcomingPronouns ) const
{
  int n = outcomingPronouns.Size();
  for( int i = 0; i < n; )
  {
    SemanticKnowledge sk( outcomingPronouns[ i ], FORM_DEFAULT );
  	if( _semanticContext.IsOutcomingDefined( sk, wb ) )
  	{
  	  ++i;
  	}
  	else
  	{
  	  outcomingPronouns.Delete( i );
  	  --n;
  	}
  }
}

#endif