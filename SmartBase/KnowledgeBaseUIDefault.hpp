
#ifndef _KNOWLEDGEBASEUIDEFAULT_HPP
#define _KNOWLEDGEBASEUIDEFAULT_HPP

#include "KnowledgeBaseUI.hpp"
#include "SemanticNet.hpp"

//! Simple default implementation.
class KnowledgeBasePlayerUiItemDefault : public KnowledgeBasePlayerUiItem
{
protected:
  RString _text;

public:
  KnowledgeBasePlayerUiItemDefault( const RString &text )
    : _text( text )
  {
  }

  virtual void GetText( RString &text ) const
  {
    text = _text;
  }
};
typedef KnowledgeBasePlayerUiItemDefault *PtrKnowledgeBasePlayerUiItemDefault;
typedef Ref< KnowledgeBasePlayerUiItemDefault > RefKnowledgeBasePlayerUiItemDefault;

/*! Simple implementation of knowledge base user interface module.
    See KnowledgeBaseUI class declaration.
*/
class KnowledgeBaseUiDefault : public KnowledgeBaseUi
{
protected:
  //! Fragment of whole sentence.
  Ref< SentenceUiFragment > _sentenceUiFragment;
  
  //! Selected Fragment.
  Ref< SentenceUiFragment > _selectedUiFragment;
  
  //! Was the sentence modified? (to force layout recalculation)
  bool _sentenceModified;
  
  //! Sentence creation rule system.
  Ref< SentenceFragment > _ruleRoot;
  
  //! Conversation history.
  AutoArray< ConversationSpeech > _conversation;

  //! Knowledge base of the player.
  Ref< KnowledgeBase > _player;
  
  //! Knowledge base of the AI who I talk to.
  RefArray< KnowledgeBase > _participants;

  //! Control unit.
  Ref< ConversationCenter > _conversationCenter;
  
  //! Special command UI item (displayed if selected fragment has parent)
  Ref< KnowledgeBaseSentenceUiItem > _cancelCommand;

  //! Where are stored undos and redos for this fragment.
  Ref< SentenceUiSegment > _sentenceUiSegment;

public:
  KnowledgeBaseUiDefault( KnowledgeBase *player,
    RefArray< KnowledgeBase > &participants, 
    ConversationCenter *conversationCenter )
    : _player( player ), _participants( participants ), _conversationCenter( conversationCenter ), 
    _sentenceUiSegment( new SentenceUiSegment() )
  {
    // Init conversation context for the player.
    AutoArray< SemanticNetId > participantsIds;
    int n = _participants.Size();
    for( int i = 0; i < n; ++i )
    {
      KnowledgeBase *kb = _participants[ i ];
      participantsIds.Add( kb->GetSemanticNet()->GetId() );
    }
    _player->GetModSemanticNet()->InitConversationContext( participantsIds );
    
    // Init conversation context for every participant.
    participantsIds.Clear();
    participantsIds.Add( _player->GetSemanticNet()->GetId() );
    for( int i = 0; i < n; ++i )
    {
      KnowledgeBase *kb = _participants[ i ];
      kb->GetModSemanticNet()->InitConversationContext( participantsIds );
    }

    // Create <cancel>.
    Ref< SentenceRule > cancelRule = new SentenceRule();
    cancelRule->AddSubfragment( new SentenceFragment( _player, 
      new SentenceAtom( new SemanticKnowledge( _conversationCenter->GetWordBase()->_wordIdCancel, FORM_DEFAULT ) ), 
        NotRequired, 1 ) );
    _cancelCommand = new KnowledgeBaseSentenceUiItem(
      _conversationCenter->GetWordBase(), cancelRule );
    
    // Create root.
    _conversationCenter->GetSentenceRuleRoot( _player, _participants, _ruleRoot );
    AssertDebug( _ruleRoot.NotNull() );
    _sentenceUiFragment = new SentenceUiFragment( _ruleRoot, NULL );
    
    // Select root
    _selectedUiFragment = _sentenceUiFragment;
    _selectedUiFragment->Select( true );
    
    // Save initial state
    _sentenceUiSegment->OpenUndo();
    _sentenceUiSegment->GetOpenedUndo()->SetOpenInfo( _selectedUiFragment );
  }

  virtual void ResetSentence()
  {
    SentenceSetModified( true );
    _sentenceUiFragment->ResetUiFragment();
    _sentenceUiSegment->ResetUiSegment();

    SentenceSelectUiFragment( _sentenceUiFragment );
    AutoSelect( true );
    _sentenceUiSegment->GetOpenedUndo()->SetOpenInfo( _selectedUiFragment );

    _player->GetModSemanticNet()->InitSpeechContext();
  }

  virtual void GetSentenceText( RString &text ) const
  {
    _sentenceUiFragment->GetSentenceText( text );
  }
  
  virtual bool CanUndo() const
  {
    AssertDebug( _sentenceUiSegment.NotNull() );
    return _sentenceUiSegment->CanUndo();
  }

  virtual bool CanTell() const
  {
    AssertDebug( _sentenceUiFragment.NotNull() );
    return _sentenceUiFragment->IsFinished();
  }

  virtual void GetConversation( AutoArray< ConversationSpeech > &conversation ) const
  {
    conversation = _conversation;
  }

  virtual void SentenceTell()
  {
    ConversationSpeech speech;
    speech._speaker = _player;
    GetSentenceText( speech._speech );
    _conversation.Add( speech );

    RefArray< KnowledgeBase > listeners( _participants );
    listeners.Add( _player );
    Ref< SentenceAtom > reaction;
    int n = _participants.Size();
    for( int i = 0; i < n; ++i )
    {
      KnowledgeBase *participant = _participants[ i ];
      participant->GetModSemanticNet()->InitSpeechContext();
      
      AutoArray< SentenceResultCondition > complementaryConditions;
      _sentenceUiFragment->GetComplementaryConditionsRecursive( complementaryConditions );
      AutoArray< SentenceResultCondition > complementarySubjects;
      _sentenceUiFragment->GetComplementarySubjectsRecursive( complementarySubjects );

      listeners.DeleteAt( i );

      _sentenceUiFragment->RemoveAllInBrackets();
      participant->GetModSemanticNet()->React( participant, _sentenceUiFragment->GetSentenceFragment(), 
        complementaryConditions, complementarySubjects, reaction, listeners );
      
      listeners.Insert( i, participant );

      if( reaction.NotNull() )
      {
        speech._speaker = participant;
        reaction->GetSentenceText( participant->GetWordBase(), speech._speech );
        _conversation.Add( speech );
        reaction.Free();
      }
    }

    ResetSentence();
  }

  virtual int GetSentenceUiItems( RefArray< KnowledgeBaseSentenceUiItem > &items ) const
  {
    AssertDebug( _selectedUiFragment.NotNull() );
    int index = 0;

    // Add <cancel> command.
    if( _selectedUiFragment->CanBeCanceled() )
    {
      ++index;
      items.Add( _cancelCommand );
    }

    RefArray< SentenceRule > rules;
    index += _selectedUiFragment->GetModSentenceFragment()->GetRules( rules );
    int n = rules.Size();
    for( int i = 0; i < n; ++i )
    {
      items.Add( new KnowledgeBaseSentenceUiItem( _conversationCenter->GetWordBase(), rules[ i ] ) );
    }
    
    return index;
  }

  virtual void ChooseSentenceUiItem( KnowledgeBaseSentenceUiItem *item )
  {
    AssertDebug( _selectedUiFragment.NotNull() );
    if( _cancelCommand != item )
    {
      Ref< SentenceRule > rule = item->GetSentenceRule();
      if( !rule->IsEqual( _selectedUiFragment->GetSentenceFragment()->GetChosenRule(), 
        *_selectedUiFragment->GetPlayer()->GetWordBase() ) )
      {
        // The selected rule is not the same as was applied last time
        int n = rule->GetSubfragmentsSize();
        if( n != 0 )
        {
          _selectedUiFragment->ClearUiFragments();
          Ref< SentenceUiFragment > newUiFrag;

          int i = 0;
          do {
            SentenceFragment *subfrag = rule->GetModSubfragment( i );
            // Plain undo
            newUiFrag = new SentenceUiFragment( subfrag, _selectedUiFragment );
            // Hierarchical undo
            // newUiFrag = new SentenceUiFragment( _conversationCenter->GetWordBase(), subfrag, undoFrag, 
            // new SentenceUiSegment( undoFrag->GetModSegment() ) );
            _selectedUiFragment->AddUiFragment( newUiFrag );
            ++i;
          } while ( i < n );

          _selectedUiFragment->SetMood( rule->GetMood() );

          SentenceSelectUiFragment( _selectedUiFragment );

          SentenceSetModified( true );
        }
        
        _selectedUiFragment->SetChooseRule( rule );
      }
    }
    else
    {
      // Cancel - as in finished optional part as in not finished optional part
      _selectedUiFragment->ClearUiFragments();
      _selectedUiFragment->SetMood( UNKNOWN );

      SentenceSelectUiFragment( _selectedUiFragment );

      SentenceSetModified( true );
    }

    _sentenceUiSegment->GetOpenedUndo()->SetCloseInfo( _selectedUiFragment );
  }

  virtual void SentenceMoveSelectionLeft()
  {
    AssertDebug( _selectedUiFragment.NotNull() );
    Ref< SentenceUiFragment > tmp = 
      _selectedUiFragment->GetLeftPostfixNeighbour( _sentenceUiFragment, _selectedUiFragment->IsInBrackets() );
    if( tmp.NotNull() )
    {
      _sentenceUiSegment->CloseUndo( _selectedUiFragment );
      _sentenceUiSegment->OpenUndo();
      _sentenceUiSegment->GetOpenedUndo()->SetOpenInfo( _selectedUiFragment );

      SentenceSelectUiFragment( tmp );
      AutoSelect( false );
      _sentenceUiSegment->GetOpenedUndo()->SetOpenInfo( _selectedUiFragment );
    }
  }

  virtual void SentenceMoveSelectionRight()
  {
    AssertDebug( _selectedUiFragment.NotNull() );
    Ref< SentenceUiFragment > tmp = 
      _selectedUiFragment->GetRightPrefixNeighbour( _sentenceUiFragment, _selectedUiFragment->IsInBrackets() );
    if( tmp.NotNull() )
    {
      _sentenceUiSegment->CloseUndo( _selectedUiFragment );
      _sentenceUiSegment->OpenUndo();
      _sentenceUiSegment->GetOpenedUndo()->SetOpenInfo( _selectedUiFragment );

      SentenceSelectUiFragment( tmp );
      AutoSelect( true );
      _sentenceUiSegment->GetOpenedUndo()->SetOpenInfo( _selectedUiFragment );
    }
  }

  inline void AutoSelect( bool goRight )
  {
    if( _selectedUiFragment->HasNoSubfragment() )
    {
      RefArray< SentenceRule > rules;
      _selectedUiFragment->GetModSentenceFragment()->GetRules( rules );
      if( rules.Size() == 1 )
      {
        Ref< KnowledgeBaseSentenceUiItem > autoItem = 
          new KnowledgeBaseSentenceUiItem( _conversationCenter->GetWordBase(), rules[ 0 ] );
        AssertDebug( autoItem.NotNull() );
        ChooseSentenceUiItem( autoItem );
        if( goRight )
        {
          SentenceMoveSelectionRight();
        }
        else
        {
          SentenceMoveSelectionLeft();
        }
      }
    }
  }

  virtual int GetHistoryLength() const
  {
    AssertDebug( _sentenceUiSegment.NotNull() );
    return _sentenceUiSegment->GetHistoryLength();
  }

  virtual void SentenceUndo()
  {
    AssertDebug( _sentenceUiSegment.NotNull() );
    SentenceSetModified( true );
    
    SentenceUiFragment *selectFragment = _selectedUiFragment;
    _sentenceUiSegment->Undo( selectFragment );
    AssertDebug( selectFragment != NULL );
    SentenceSelectUiFragment( selectFragment );
    AutoSelect( true );
  }

  virtual void SentenceSelectAtPoint( int x, int y )
  {
    AssertDebug( _sentenceUiFragment.NotNull() );
    SentenceUiFragment *tmp = _sentenceUiFragment->GetAtPoint( x, y );
    if( tmp != NULL )
    {
      SentenceSelectUiFragment( tmp );
      AutoSelect( true );
      _sentenceUiSegment->GetOpenedUndo()->SetOpenInfo( _selectedUiFragment );
    }
  }

  virtual bool SentenceIsModified() const
  {
    return _sentenceModified;
  }

  virtual void SentenceSetModified( bool sentenceModified )
  {
    _sentenceModified = sentenceModified;
  }

  virtual SentenceUiFragment *SentenceGetUiFragment() const
  {
    return _sentenceUiFragment;
  }

  virtual SentenceUiFragment *SentenceGetSelection() const
  {
    return _selectedUiFragment;
  }

  void SentenceSelectUiFragment( SentenceUiFragment *uiFragment )
  {
    if( uiFragment != _selectedUiFragment )
    {
      SentenceUiFragment *parent;
      if( uiFragment->IsTerminator() && 
        ( parent = uiFragment->GetModParentUiFragment() ) != NULL )
      {
        uiFragment = parent;
        if( uiFragment == _selectedUiFragment )
        {
          _selectedUiFragment->Select( true );
          return;
        }
      }

      SentenceSetModified( true );
      _selectedUiFragment->Select( false );
      _selectedUiFragment = uiFragment;
      _selectedUiFragment->Select( true );
    }
    else
    {
      _selectedUiFragment->Select( true );
      return;
    }
  }
};

#endif