#if _ENABLE_CONVERSATION

#include "El/elementpch.hpp"
#include "El/Modules/modules.hpp"

#include "SentenceRule.hpp"
#include "SemanticNet.hpp"

bool SentenceFragment::RulesNotEmpty()
{
  if( !IsTerminator() )
  {
    RefArray< SentenceRule > rules;
    GetRules( rules );
    int n = rules.Size();
    if( n != 0 )
    {
      int i = 0;
      do {
        if( !rules[ i ]->RulesNotEmpty() )
        {
          return false;
        }
        ++i;
      } while( i < n );
      return true;
    }
    return false;
  }
  return true;
}

int SentenceFragment::GetRules( RefArray< SentenceRule > &rules )
{
  // Init output array of rules with special rules.
  rules = _specRules;
  
  // Read rules in virtual function (particular implementation).
  ReadRules( rules );

  // Remove invisible rules.
  int rulesSize = rules.Size();
  for( int i = 0; i < rulesSize; )
  {
    if( rules[ i ]->IsVisible( _player ) )
    {
      ++i;
    }
    else
    {
      rules.DeleteAt( i );
      --rulesSize;
    }
  }

  // Clear not original candidates.
  int n = _candidates.Size();
  for( int i = 0; i < n; )
  {
    if( _candidates[ i ]._original )
    {
      ++i;
    }
    else
    {
      _candidates.Delete( i );
      --n;
    }
  }

  // Clear not original conditions.
  n = _conditions.Size();
  for( int i = 0; i < n; )
  {
    if( _conditions[ i ]._original )
    {
      ++i;
    }
    else
    {
      _conditions.Delete( i );
      --n;
    }
  }

  // Clear not original candidates tables.
  n = _candidatesTables.Size();
  for( int i = 0; i < n; )
  {
    if( _candidatesTables[ i ]._original )
    {
      ++i;
    }
    else
    {
      _candidatesTables.Delete( i );
      --n;
    }
  }
  
  // Check size while some sub fragment was unwrapped.
  bool checkSize = true;
  // If there is only one rule try to unwrap the 1st branch fragment.
  while( checkSize && rules.Size() == 1 )
  {
    checkSize = false;
    // The only one rule.
    SentenceRule *r = rules[ 0 ];
    int n = r->GetSubfragmentsSize();
    for( int i = 0; i < n; ++i )
    {
      Ref< SentenceFragment > sf = r->GetModSubfragment( i );
      // Find the first branch sub fragment to unwrap it.
    	if( !sf->IsTerminator() )
    	{
    	  // Read sub-rules of the first branch sub fragment..
        RefArray< SentenceRule > subRules;
        sf->GetRules( subRules );
        int m = subRules.Size();
        // Create 'm' copies of the rule.
    	  RefArray< SentenceRule > newRules;
    	  Ref< SentenceRule > tmpRule;
    	  for( int j = 0; j < m; ++j )
    	  {
    	    SentenceRule *subRule = subRules[ j ];
    	    r->Copy( tmpRule );
          // Apply sub-rules to the new rules
          tmpRule->ReplaceSubfragments( i, 1, subRule->GetSubfragments() );
          // Copy conditions from replaced fragment to the new rule.
          int tn = sf->GetConditionsSize();
          for( int ti = 0; ti < tn; ++ti )
          {
            SentenceResultCondition src = sf->GetCondition( ti );
            if( src._index >= 0 )
            {
              src._index += i;
            }
            src._original = false;
            tmpRule->AddCondition( src );
          }
          // Copy conditions from used rule to the new rule.
          tn = subRule->GetConditionsSize();
          for( int ti = 0; ti < tn; ++ti )
          {
            SentenceResultCondition src = subRule->GetCondition( ti );
            if( src._index >= 0 )
            {
              src._index += i;
            }
            src._original = false;
            tmpRule->AddCondition( src );
          }
          // Copy candidates from replaced fragment to the new rule.
          tn = sf->GetCandidatesSize();
          for( int ti = 0; ti < tn; ++ti )
          {
            SentenceResultCandidates src = sf->GetCandidates( ti );
            if( src._index >= 0 )
            {
              src._index += i;
            }
            src._original = false;
            tmpRule->AddCandidates( src );
          }
          // Copy candidates from used rule to the new rule.
          tn = subRule->GetCandidatesSize();
          for( int ti = 0; ti < tn; ++ti )
          {
            SentenceResultCandidates src = subRule->GetCandidates( ti );
            if( src._index >= 0 )
            {
              src._index += i;
            }
            src._original = false;
            tmpRule->AddCandidates( src );
          }
          // Copy candidates tables from replaced fragment to the new rule.
          tn = sf->GetCandidatesTablesSize();
          for( int ti = 0; ti < tn; ++ti )
          {
            SentenceResultCandidatesTable src = sf->GetCandidatesTable( ti );
            src._original = false;
            tmpRule->AddCandidatesTable( src );
          }
          // Copy candidates tables from used rule to the new rule.
          tn = subRule->GetCandidatesTablesSize();
          for( int ti = 0; ti < tn; ++ti )
          {
            SentenceResultCandidatesTable src = subRule->GetCandidatesTable( ti );
            src._original = false;
            tmpRule->AddCandidatesTable( src );
          }
          // Copy results from used rule to the new rule.
          // In the replaced fragment there are no results.
          tn = subRule->GetResultsSize();
          if( tn != 0 )
          {
            tmpRule->GetModResults().Clear();
            int ti = 0;
            do {
          	  FormattedSentenceResult *r = new FormattedSentenceResult( *subRule->GetResult( ti ) );
          	  r->SetOriginal( false );
          	  tmpRule->AddResult( r );
            } while ( ti < tn );
          }
          // Copy visible tests.
          tn = subRule->GetVisibleTestsSize();
          for( int ti = 0; ti < tn; ++ti )
          {
            RuleVisibleTest src = subRule->GetVisibleTest( ti );
            src.SetOriginal( false );
            tmpRule->AddVisibleTest( src );
          }
          // Add new rule if it is visible.
          if( tmpRule->IsVisible( _player ) )
          {
            newRules.Add( tmpRule );
          }
    	  }
    	  rules = newRules;
    	  rulesSize = rules.Size();
        checkSize = true;
    	  break;
    	}
    }
  }
  
  // Find auto-select rule.
  if( _chosenRule != NULL )
  {
    for( int i = 0; i < rulesSize; ++i )
    {
      const SentenceRule *ri = rules[ i ];
      if( ri->IsEqual( _chosenRule, *_player->GetWordBase() ) )
      {
        return i;
      }
    }
  }
  return -1;
}

void SentenceFragment::GetConditionsRecursive( AutoArray< SentenceResultCondition > &conditions ) const
{
  int n = _conditions.Size();
  for( int i = 0; i < n; ++i )
  {
    int newIdx = conditions.Add( _conditions[ i ] );
    SentenceResultCondition &newCon = conditions[ newIdx ];
    if( newCon._index >= 0 )
    {
      AssertDebug( _subfragments[ newCon._index ]->IsTerminator() );
      newCon._thisVariable = new SemanticKnowledge( *_subfragments[ newCon._index ]->GetSentenceAtom()->GetKnowledgeData() );
    }
  }
  
  if( _chosenRule.NotNull() )
  {
    _chosenRule->GetConditions( conditions );
  }

  n = _subfragments.Size();
  for( int i = 0; i < n; ++i )
  {
    _subfragments[ i ]->GetConditionsRecursive( conditions );
  }
}

void SentenceFragment::GetProbableResultsRecursive( RefArray< const FormattedSentenceResult > &results ) const
{
  if( _chosenRule.NotNull() && _chosenRule->GetResultsSize() != 0 )
  {
    results.Clear();
    int n = _chosenRule->GetResultsSize();
    for( int i = 0; i < n; ++i )
    {
      const FormattedSentenceResult *f = _chosenRule->GetResult( i );
      if( f->GetThreshold() >= 0.0f )
      {
        results.Add( f );
      }
    }
  }
  
  int n = _subfragments.Size();
  for( int i = 0; i < n; ++i )
  {
  	_subfragments[ i ]->GetProbableResultsRecursive( results );
  }
}

void SentenceFragment::GetConditionalResultsRecursive( RefArray< const FormattedSentenceResult > &results ) const
{
  if( _chosenRule.NotNull() && _chosenRule->GetResultsSize() != 0 )
  {
    results.Clear();
    int n = _chosenRule->GetResultsSize();
    for( int i = 0; i < n; ++i )
    {
      const FormattedSentenceResult *f = _chosenRule->GetResult( i );
      if( f->GetCondition().GetRefCount() != 0 )
      {
        results.Add( f );
      }
    }
  }

  int n = _subfragments.Size();
  for( int i = 0; i < n; ++i )
  {
    _subfragments[ i ]->GetProbableResultsRecursive( results );
  }
}

void SentenceFragment::GetCandidatesRecursive( AutoArray< SentenceResultCandidates > &candidates ) const
{
  int n = _candidates.Size();
  for( int i = 0; i < n; ++i )
  {
    int newIdx = candidates.Add( _candidates[ i ] );
    SentenceResultCandidates &newCan = candidates[ newIdx ];
    if( newCan._index >= 0 )
    {
      AssertDebug( _subfragments[ newCan._index ]->IsTerminator() );
      newCan._thisVariable = new SemanticKnowledge( *_subfragments[ newCan._index ]->GetSentenceAtom()->GetKnowledgeData() );
    }
  }

  if( _chosenRule.NotNull() )
  {
    _chosenRule->GetCandidates( candidates );
  }

  n = _subfragments.Size();
  for( int i = 0; i < n; ++i )
  {
    _subfragments[ i ]->GetCandidatesRecursive( candidates );
  }
}

void SentenceFragment::GetCandidatesTablesRecursive( AutoArray< SentenceResultCandidatesTable > &candidatesTables ) const
{
  int n = _candidatesTables.Size();
  for( int i = 0; i < n; ++i )
  {
    candidatesTables.Add( _candidatesTables[ i ] );
  }

  if( _chosenRule.NotNull() )
  {
    _chosenRule->GetCandidatesTables( candidatesTables );
  }

  n = _subfragments.Size();
  for( int i = 0; i < n; ++i )
  {
    _subfragments[ i ]->GetCandidatesTablesRecursive( candidatesTables );
  }
}

void SentenceFragment::GetComplementaryConditionsRecursive( AutoArray< SentenceResultCondition > &conditions )
{
  int n = GetConditionsSize();
  for( int i = 0; i < n; ++i )
  {
  	conditions.Add( GetCondition( i ) );
  }
  
  if( _chosenRule.IsNull() )
  {
    RefArray< SentenceRule > complementaryRules;
    GetRules( complementaryRules );
    if( complementaryRules.Size() != 0 )
    {
      SentenceRule *rule = complementaryRules[ 0 ];
      rule->GetConditions( conditions );
      
      int n = rule->GetSubfragmentsSize();
      for( int i = 0; i < n; ++i )
      {
      	SentenceFragment *frg = rule->GetModSubfragment( i );
      	frg->GetComplementaryConditionsRecursive( conditions );
      }
    }
  }
  else
  {
    _chosenRule->GetConditions( conditions );
    
    int n = _subfragments.Size();
    for( int i = 0; i < n; ++i )
    {
    	_subfragments[ i ]->GetComplementaryConditionsRecursive( conditions );
    }
  }
}

void SentenceFragment::GetComplementarySubjectsRecursive( AutoArray< SentenceResultCondition > &conditions )
{
  int n = GetConditionsSize();
  for( int i = 0; i < n; ++i )
  {
    const SentenceResultCondition &c = GetCondition( i );
    if( c._subject )
    {
      conditions.Add( c );
    }
  }

  if( _chosenRule.IsNull() )
  {
    RefArray< SentenceRule > complementaryRules;
    GetRules( complementaryRules );
    if( complementaryRules.Size() != 0 )
    {
      SentenceRule *rule = complementaryRules[ 0 ];
      rule->GetConditions( conditions );

      int n = rule->GetSubfragmentsSize();
      for( int i = 0; i < n; ++i )
      {
        SentenceFragment *frg = rule->GetModSubfragment( i );
        frg->GetComplementarySubjectsRecursive( conditions );
      }
    }
  }
  else
  {
    n = _chosenRule->GetConditionsSize();
    for( int i = 0; i < n; ++i )
    {
    	if( _chosenRule->GetCondition( i )._subject )
    	{
    	  _chosenRule->GetModCondition( i, conditions.Append() );
    	}
    }

    int n = _subfragments.Size();
    for( int i = 0; i < n; ++i )
    {
      _subfragments[ i ]->GetComplementarySubjectsRecursive( conditions );
    }
  }
}

GameData *CreateGameDataRule()
{
  return new GameDataRule();
}

DEFINE_FAST_ALLOCATOR( GameDataRule )

RString GameDataRule::GetText() const
{
  if( _rule.IsNull() )
  {
    return "<NULL-object>";
  }

  return "Rule";
}

bool GameDataRule::IsEqualTo( const GameData *data ) const
{
  SentenceRule *k1 = GetRule();
  SentenceRule *k2 = static_cast< const GameDataRule* >( data )->GetRule();
  return k1 == k2;
}

LSError GameDataRule::Serialize( ParamArchive &ar )
{
  LSError err = base::Serialize( ar );
  if( err != 0 )
  {
    return err;
  }
  return ( LSError ) 0;
}

GameValue KnowledgeBase_ContextDefined( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
  const KnowledgeBase *participant;
  if( ( participant = GetKnowledgeBase( oper1 ) ) != NULL )
  {
    WordBaseId id = participant->GetWordBase()->ConvertNameToId( static_cast< const GameStringType& >( oper2 ) );
    if( id != -1 )
    {
      return participant->GetSemanticNet()->IsNotionDefined( id );
    }
  }
  
  return GameValue( new GameDataBool( false ) );
}

INIT_MODULE( SentenceRuleMain, 3 )
{
  GGameState.NewType( "RULE", GameRule, CreateGameDataRule, "Rule" );
  
  GGameState.NewOperator( GameOperator( GameBool,      "contextDefined",        function, KnowledgeBase_ContextDefined,     GameKnowledgeBase, GameString ) );
};

#endif