// DlgLinkProperties.cpp : implementation file
//

#include "../stdafx.h"
#include "../GraphView.h"
#include "DlgLinkProperties.h"
#include "designtlbresource.h"

/*
#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif
*/

/////////////////////////////////////////////////////////////////////////////
// DlgLinkProperties dialog


DlgLinkProperties::DlgLinkProperties(CWnd* pParent /*=NULL*/)
	: CDialog(DlgLinkProperties::IDD, pParent)
    , vUseBKColor(FALSE)
{
	//{{AFX_DATA_INIT(DlgLinkProperties)
	vLineWidth = 1;
	vArrowSize = 1;
	vLineStyle = 0;
	vArrowType = 0;
	vUseArrows = FALSE;
	//}}AFX_DATA_INIT
}


void DlgLinkProperties::DoDataExchange(CDataExchange* pDX)
{
  CDialog::DoDataExchange(pDX);
  //{{AFX_DATA_MAP(DlgLinkProperties)
  DDX_Control(pDX, IDC_LINESTYLE, wLineStyle);
  DDX_Control(pDX, IDC_LINESTYLESPIN, wLineSizeSpin);
  DDX_Control(pDX, IDC_ARROWTYPE, wArrowType);
  DDX_Text(pDX, IDC_LINEWIDTH, vLineWidth);
  DDX_Radio(pDX, IDC_SMALLARROW, vArrowSize);
  DDX_Check(pDX, IDC_USERARROWS, vUseArrows);
  //}}AFX_DATA_MAP
  DDX_Check(pDX, IDC_USEBKCOLOR, vUseBKColor);
}


BEGIN_MESSAGE_MAP(DlgLinkProperties, CDialog)
	//{{AFX_MSG_MAP(DlgLinkProperties)
	ON_BN_CLICKED(IDC_USERARROWS, OnUserarrows)
	ON_WM_DRAWITEM()
	ON_NOTIFY(UDN_DELTAPOS, IDC_LINESTYLESPIN, OnDeltaposLinestylespin)
	ON_EN_CHANGE(IDC_LINEWIDTH, OnChangeLinewidth)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// DlgLinkProperties message handlers

void DlgLinkProperties::OnUserarrows() 
  {
  DialogRules();	
  }

void DlgLinkProperties::DialogRules()
  {
  BOOL ok=IsDlgButtonChecked(IDC_USERARROWS)!=0;
  wLineStyle.EnableWindow(ok);
  wLineSizeSpin.EnableWindow(ok);
  wArrowType.EnableWindow(ok);
  GetDlgItem(IDC_LINEWIDTH)->EnableWindow(ok);
  GetDlgItem(IDC_SMALLARROW)->EnableWindow(ok);
  GetDlgItem(IDC_MEDIUMARROW)->EnableWindow(ok);
  GetDlgItem(IDC_BIGARROW)->EnableWindow(ok);
  GetDlgItem(IDC_VERYBIGARROW)->EnableWindow(ok);
  GetDlgItem(IDC_USEBKCOLOR)->EnableWindow(ok);
  }

BOOL DlgLinkProperties::OnInitDialog() 
{
	CDialog::OnInitDialog();
	for (int i=0;i<14;i++) wArrowType.AddString((LPCTSTR)i);
	wArrowType.SetItemHeight(0,20);
	wArrowType.SelectString(-1,(LPCTSTR)vArrowType);
	wLineStyle.AddString((LPCTSTR)PS_SOLID);
	wLineStyle.AddString((LPCTSTR)PS_DASH );
	wLineStyle.AddString((LPCTSTR)PS_DOT );
	wLineStyle.AddString((LPCTSTR)PS_DASHDOT );
	wLineStyle.AddString((LPCTSTR)PS_DASHDOTDOT );
	wLineStyle.AddString((LPCTSTR)PS_INSIDEFRAME );
	wLineStyle.SelectString(-1,(LPCTSTR)vLineStyle);
	wLineStyle.SetItemHeight(0,25);	
	DialogRules();
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void NewSipka(CDC &dc, CPoint pos, CPoint dir, int style, int size);

void DlgLinkProperties::OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct) 
{
	if (nIDCtl==IDC_LINESTYLE)
	  {
	  int itemID=lpDrawItemStruct->itemID;
	  if (itemID<0) return;
	  CDC dc;
	  dc.Attach(lpDrawItemStruct->hDC);
	  RECT &bound=lpDrawItemStruct->rcItem;
	  COLORREF pencolor;
	  COLORREF bgcolor;
	  int width=GetDlgItemInt(IDC_LINEWIDTH);
	  if (lpDrawItemStruct->itemState & ODS_SELECTED)
		{
		bgcolor=GetSysColor(COLOR_HIGHLIGHT);
		pencolor=GetSysColor(COLOR_HIGHLIGHTTEXT);
		}
	  else
		{
		bgcolor=GetSysColor(COLOR_WINDOW);
		pencolor=GetSysColor(COLOR_WINDOWTEXT);
		}
	  if (lpDrawItemStruct->itemState & ODS_DISABLED)
		pencolor=GetSysColor(COLOR_GRAYTEXT);
	  CPen pen(lpDrawItemStruct->itemData,width,pencolor);
	  CPen *oldpen=dc.SelectObject(&pen);
	  dc.FillSolidRect(&bound,bgcolor);
	  CRect rc=bound;
	  rc-=CRect(7,7,7,7);
	  dc.Rectangle(&rc);
	  dc.SelectObject(oldpen);
	  dc.Detach();
	  }
	else if (nIDCtl==IDC_ARROWTYPE)
	  {
	  int itemID=lpDrawItemStruct->itemID;
	  if (itemID<0) return;
	  CDC dc;
	  dc.Attach(lpDrawItemStruct->hDC);
	  RECT &bound=lpDrawItemStruct->rcItem;
	  COLORREF pencolor;
	  COLORREF bgcolor;
	  if (lpDrawItemStruct->itemState & ODS_SELECTED)
		{
		bgcolor=GetSysColor(COLOR_HIGHLIGHT);
		pencolor=GetSysColor(COLOR_HIGHLIGHTTEXT);
		}
	  else
		{
		bgcolor=GetSysColor(COLOR_WINDOW);
		pencolor=GetSysColor(COLOR_WINDOWTEXT);
		}
	  if (lpDrawItemStruct->itemState & ODS_DISABLED)
		pencolor=GetSysColor(COLOR_GRAYTEXT);
	  CPen pen(PS_SOLID,1,pencolor);
	  CBrush brsh(pencolor);
	  CPen *oldpen=dc.SelectObject(&pen);
	  CBrush *oldbrsh=dc.SelectObject(&brsh);
	  dc.FillSolidRect(&bound,bgcolor);
	  CRect rc=bound;

	  dc.MoveTo(rc.left,(rc.top+rc.bottom)>>1);
	  dc.LineTo(rc.right,(rc.top+rc.bottom)>>1);
	  NewSipka(dc,CPoint(rc.right,(rc.top+rc.bottom)>>1),CPoint(-1,0),lpDrawItemStruct->itemData,0);
	  dc.SelectObject(oldpen);
	  dc.SelectObject(oldbrsh);
	  dc.Detach();
	  }
}

void DlgLinkProperties::OnDeltaposLinestylespin(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	int pos=GetDlgItemInt(IDC_LINEWIDTH);
	pos-=pNMUpDown->iDelta;
	if (pos<1) pos=1;
	if (pos>20) pos=20;
	SetDlgItemInt(IDC_LINEWIDTH,pos);
	*pResult = 0;
}

void DlgLinkProperties::OnChangeLinewidth() 
{
  wLineStyle.Invalidate();
	
}

void DlgLinkProperties::OnOK() 
  {
  vArrowType=wArrowType.GetItemData(wArrowType.GetCurSel());
  vLineStyle=wLineStyle.GetItemData(wLineStyle.GetCurSel());
  CDialog::OnOK();
  }
