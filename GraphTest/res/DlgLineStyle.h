#if !defined(AFX_DLGLINESTYLE_H__56DAAB60_4575_4D44_98FC_7BC03A9F72A6__INCLUDED_)
#define AFX_DLGLINESTYLE_H__56DAAB60_4575_4D44_98FC_7BC03A9F72A6__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DlgLineStyle.h : header file
//

#include "designtlbresource.h"

/////////////////////////////////////////////////////////////////////////////
// DlgLineStyle dialog

class DlgLineStyle : public CDialog
{
// Construction
public:
	DlgLineStyle(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(DlgLineStyle)
	enum { IDD = IDD_LINESTYLE };
	CComboBox	wLineStyle;
	int		vLineStyle;
	int		vLineWidth;
	//}}AFX_DATA	

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(DlgLineStyle)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(DlgLineStyle)
	afx_msg void OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct);
	virtual BOOL OnInitDialog();
	afx_msg void OnDeltaposLinestylespin(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnChangeLinewidth();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DLGLINESTYLE_H__56DAAB60_4575_4D44_98FC_7BC03A9F72A6__INCLUDED_)
