// CMainFrmToolbar.cpp : implementation of the CMainFrmToolbar class
//

#include "../stdafx.h"
#include ".\maintlb.h"

/////////////////////////////////////////////////////////////////////////////
// CMainFrmToolbar

IMPLEMENT_DYNAMIC(CMainFrmToolbar, CToolBar)

BEGIN_MESSAGE_MAP(CMainFrmToolbar, CToolBar)
  //{{AFX_MSG_MAP(CMainFrmToolbar)
    ON_WM_CREATE()

    ON_COMMAND(ID_FILE_NEW, OnFileNew)
    ON_COMMAND(ID_FILE_OPEN, OnFileOpen)
    ON_COMMAND(ID_FILE_SAVE, OnFileSave)
    ON_COMMAND(ID_FILE_SAVEAS, OnFileSaveas)
    ON_COMMAND(ID_FILE_SAVEEMF, OnFileSaveemf)

    ON_COMMAND(ID_FILE_PRINT, OnFilePrint)

    ON_COMMAND(ID_EDIT_SELECT_ALL, OnSelectAll)
    ON_COMMAND(ID_EDIT_UNDO, OnEditUndo)
    ON_UPDATE_COMMAND_UI(ID_EDIT_UNDO, OnUpdateEditUndo)
    ON_COMMAND(ID_EDIT_EXTERNALEDITOR, OnExternalEditor)
    ON_COMMAND(ID_EDIT_DELETE, OnEditDelete)
    ON_UPDATE_COMMAND_UI(ID_EDIT_DELETE, OnUpdateEditDelete)
    ON_COMMAND(ID_EDIT_CUT, OnEditCut)
    ON_UPDATE_COMMAND_UI(ID_EDIT_CUT, OnUpdateSelected)
    ON_COMMAND(ID_EDIT_COPY, OnEditCopy)
    ON_UPDATE_COMMAND_UI(ID_EDIT_COPY, OnUpdateSelected)
    ON_COMMAND(ID_EDIT_PASTE, OnEditPaste)
    ON_UPDATE_COMMAND_UI(ID_EDIT_PASTE, OnUpdatePaste)
    ON_COMMAND(ID_EDIT_DUPLICATE, OnEditDuplicate)
    ON_UPDATE_COMMAND_UI(ID_EDIT_DUPLICATE, OnUpdateSelected)
    ON_COMMAND(ID_EDIT_GROUP, OnEditGroup)
    ON_UPDATE_COMMAND_UI(ID_EDIT_GROUP, OnUpdateEditGroup)
    ON_COMMAND(ID_EDIT_MOVEFRONT, OnEditMovefront)
    ON_UPDATE_COMMAND_UI(ID_EDIT_MOVEFRONT, OnUpdateSelected)
    ON_COMMAND(ID_EDIT_MOVEBACK, OnEditMoveback)
    ON_UPDATE_COMMAND_UI(ID_EDIT_MOVEBACK, OnUpdateSelected)
    ON_COMMAND(ID_ATTR_EDIT, OnAttrEdit)
  //}}AFX_MSG_MAP
  ON_COMMAND(ID_EDIT_REDO, OnEditRedo)
END_MESSAGE_MAP()



/////////////////////////////////////////////////////////////////////////////
// CMainFrmToolbar message handlers

BOOL CMainFrmToolbar::OnInitDialog(void)
{
  return false;
}

int CMainFrmToolbar::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
  if (CToolBar::OnCreate(lpCreateStruct) == -1)
    return -1;
  //load accelerator table

  hAccelerator = LoadAccelerators(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDR_MAINTLBACC));

  return 0;
}

/*
void CMainFrmToolbar::OnUpdateCmdUI(CFrameWnd *pTarget, BOOL bDisableIfNoHndler)
{
}
*/

void CMainFrmToolbar::OnUpdateEditDelete(CCmdUI* pCmdUI) 
{
  pCmdUI->Enable(wGraph.IsNormalOrDesignMode() && (wGraph.EnumItems(-1,true)!=-1 || wGraph._curlink!=-1));
}

void CMainFrmToolbar::OnUpdateSelected(CCmdUI* pCmdUI) 
{
  pCmdUI->Enable(wGraph.EnumItems(-1,true)!=-1);
}

BOOL CMainFrmToolbar::PreTranslateMessage(MSG* pMsg) 
{
//  if (pMsg->message==WM_KEYDOWN)
//    _asm nop;
  if (TranslateAccelerator(*this,hAccelerator,pMsg)) return true;
  return CToolBar::PreTranslateMessage(pMsg);
}

CMainFrmToolbar::CMainFrmToolbar(CGraphCtrlExt &wGraph1, CString &curfname1)
: CToolBar(), wGraph(wGraph1), curfname(curfname1)
{
}

CMainFrmToolbar::~CMainFrmToolbar()
{
}

BOOL CMainFrmToolbar::PreCreateWindow(CREATESTRUCT& cs)
{
  if( !CToolBar::PreCreateWindow(cs) )
    return FALSE;

  //  the CREATESTRUCT cs

  //cs.lpszClass = AfxRegisterWndClass(0); //???
  return TRUE;
}

/////////////////////////////////////////////////////////////////////////////
// CMainFrmToolbar message handlers

/*
BOOL CMainFrmToolbar::OnCmdMsg(UINT nID, int nCode, void* pExtra, AFX_CMDHANDLERINFO* pHandlerInfo)
{
  // let the view have first crack at the command
  if (wGraph.OnCmdMsg(nID, nCode, pExtra, pHandlerInfo))
    return TRUE;

  // otherwise, do default handling
  return CFrameWnd::OnCmdMsg(nID, nCode, pExtra, pHandlerInfo);
}
*/

void CMainFrmToolbar::OnEditDelete() 
{
  if (wGraph._curlink!=-1) wGraph.OnMenuUnlink();
  else wGraph.DeleteSelected();	
}
void CMainFrmToolbar::OnEditCut() 
{
  if (wGraph._curlink!=-1) wGraph.OnMenuUnlink();
  else {
    OnEditCopy();
    wGraph.DeleteSelected();	
  }
}
//#include <el/QStream/QStream.hpp>
void CMainFrmToolbar::OnEditCopy() 
{
  if (OpenClipboard()==FALSE) {AfxMessageBox("OpenClipboard failed");return;}
  if (EmptyClipboard()==FALSE) {AfxMessageBox("EmptyClipboard failed");CloseClipboard();return;}
  QOStrStream *newClipboardData = wGraph.GetDataForClipboard();
  int size = newClipboardData->pcount()+sizeof(int);
  HANDLE glob=GlobalAlloc(GMEM_MOVEABLE,size);
  int *ptr=(int *)GlobalLock(glob);
  *ptr = size;
  memcpy(ptr+1,newClipboardData->str(),newClipboardData->pcount());
  GlobalUnlock(glob);
  if (SetClipboardData(CF_FSM_EDITOR,glob)==0) AfxMessageBox("SetClipboardData failed");
  CloseClipboard();
  delete newClipboardData;
  LogF("***Clipboard copy: size=%d",size);
}
void CMainFrmToolbar::OnEditPaste() 
{
  if (OpenClipboard()==TRUE)
  {
    HANDLE glob=GetClipboardData(CF_FSM_EDITOR);
    if (glob)
    {
      wGraph.SaveUndo();
      wGraph.Deselect(); //for (id=-1;(id = wGraph.EnumItems(id,true)) != -1;)  ...
      int *data=(int *)GlobalLock(glob);
      QIStrStream in(data, *data);
      wGraph.PasteFromClipboard(in);
      GlobalUnlock(glob);
    }
    CloseClipboard();
  }
  wGraph.Mode(SGRM_DESIGN);
}
void CMainFrmToolbar::OnUpdatePaste(CCmdUI* pCmdUI)
{
  pCmdUI->Enable(IsClipboardFormatAvailable(CF_FSM_EDITOR));
}

void CMainFrmToolbar::OnEditDuplicate() 
{
 wGraph.DuplicateSelected();	
}

void CMainFrmToolbar::OnEditGroup() 
{
  wGraph.GroupSelect(!wGraph.IsGroupSelect());
}

void CMainFrmToolbar::OnUpdateEditGroup(CCmdUI* pCmdUI) 
{
  pCmdUI->Enable(wGraph.EnumItems(-1,true)!=-1);
  pCmdUI->SetCheck(wGraph.IsGroupSelect());	
}

void CMainFrmToolbar::OnEditMovefront() 
{
  wGraph.OrderSelected(1);	
}

void CMainFrmToolbar::OnEditMoveback() 
{
  wGraph.OrderSelected(-1);  	
}

void CMainFrmToolbar::OnAttrEdit()
{
  AfxGetMainWnd()->SendMessage(MSG_ATTR_EDIT,0,0);
}

void CMainFrmToolbar::OnFileSaveemf() 
{
  CString filter,defext,title;
  filter.LoadString(IDS_EMFFILEFILTER);
  defext.LoadString(IDS_EMFDEFEXT);
  title.LoadString(IDS_EMFTITLE);
  CFileDialog dlg(FALSE,defext,NULL,OFN_PATHMUSTEXIST|OFN_OVERWRITEPROMPT|OFN_HIDEREADONLY,filter);
  dlg.m_ofn.lpstrTitle=title;
  if (dlg.DoModal()==IDOK)
  {
    CRect bound;

    if (!wGraph._pageClip)
    {
      SFloatRect frc=wGraph.GetBoundingBox();
      bound=wGraph.MapRectToWindow(frc);
      bound+=CRect(10,10,10,10);
    }
    else
    {
      bound=wGraph.GetClipRect();
    }
    CRect pxbound=bound;
    CDC *dc=GetDC();
    int iWidthMM = GetDeviceCaps(*dc, HORZSIZE); 
    int iHeightMM = GetDeviceCaps(*dc, VERTSIZE); 
    int iWidthPels = GetDeviceCaps(*dc, HORZRES); 
    int iHeightPels = GetDeviceCaps(*dc, VERTRES); 

    bound.left = (bound.left * iWidthMM * 100)/iWidthPels; 
    bound.top = (bound.top * iHeightMM * 100)/iHeightPels; 
    bound.right = (bound.right * iWidthMM * 100)/iWidthPels; 
    bound.bottom = (bound.bottom * iHeightMM * 100)/iHeightPels; 

    CDC metafile;
    metafile.Attach(CreateEnhMetaFile(*dc,dlg.GetPathName(),&bound,NULL));
    ReleaseDC(dc);
    if (metafile.GetSafeHdc()==NULL)
    {
      AfxMessageBox(IDS_EXPORTFAILED);
      return;
    }
    wGraph.DrawExport(metafile,pxbound);
    HENHMETAFILE mf=CloseEnhMetaFile(metafile.Detach());
    DeleteEnhMetaFile(mf);
  }
}

#include "../MainFrm.h"
#include "../GraphView.h"
#include "../ExtEditorManager.h"
void CMainFrmToolbar::NewFile()
{
  curfname="";
  AfxGetMainWnd()->SendMessage(MSG_WNDSERIALIZE,0,0);  //untouch edit window
  wGraph.ResetContent();
  // read page setup from Registry
  extern CGraphViewApp theApp;
  CString entryVal;
  entryVal = theApp.GetProfileString("PageSetup","grid","0");
  wGraph._grid = atof(entryVal);
  wGraph._gridshow = (theApp.GetProfileInt("PageSetup","gridShow", 0)!=0) && (theApp.GetProfileInt("PageSetup","gridEnable", 0)!=0);
  wGraph._pageColor = (theApp.GetProfileInt("PageSetup","pageColor", 0) !=0 );
  wGraph._pageColorVal = theApp.GetProfileInt("PageSetup","pageColorVal", 0);

  FSMEditor &ed = static_cast<FSMEditor &>(wGraph);
  ed.AddStartState();
  gExtEdFileManager.Init((FSMEditor *)&wGraph);
  wGraph.Update();
}

void CMainFrmToolbar::OnFileNew()
{
  if (AfxMessageBox(IDS_NEWASK,MB_YESNO|MB_ICONQUESTION|MB_DEFBUTTON2)==IDYES)
  {
    NewFile();
  }
}

#include "es/Files/filenames.hpp"
void CMainFrmToolbar::OnFileSave()
{
  if (curfname=="") { OnFileSaveas(); return; }
  
  FSMEditor &ed = static_cast<FSMEditor &>(wGraph);
  RString ext(GetFileExt(curfname)); ext.Lower();
  CString bifsmExt; bifsmExt.LoadString(IDS_DIAGRAMDEFEXT);
  if (ext==RString(bifsmExt))
  {
    if (wGraph.Save(curfname)==false) AfxMessageBox(IDS_SAVEFAILED);
  }
  else 
  {
    SaveThroughFSMCompiler();
  }
  ((CMainFrame *)AfxGetMainWnd())->UpdateCaption();
//  if (wGraph.Save(curfname)==false) AfxMessageBox(IDS_SAVEFAILED);
}

#include <io.h>
#include <fcntl.h>
#include <process.h>
#include "el/QStream/QStdStream.hpp"
#include "../SaveLoadDlgs.h"
#define   OUT_BUFF_SIZE 16384
#define   READ_FD  0
#define   WRITE_FD 1
static char szBuffer[OUT_BUFF_SIZE+1];
static char BisFSMCompilerPath[] = "c:\\bis\\fsmeditor\\fsmcompiler.exe";
static char CurPathFSMCompiler[] = "FSMCompiler.exe";
static CString OtherCompiler;
static const bool CompileButNotSave = false;

void CMainFrmToolbar::SaveThroughFSMCompiler()
{ //save using compile config
  // fsmcompiler.exe 
  RString configName=static_cast<FSMEditor *>(&wGraph)->compileConfigFileName;
  if (configName==RString(""))
  {
    AfxMessageBox("Save using compilation impossible!\r\nNo compile config specified!", MB_OK |MB_ICONWARNING);
    return;
  }
  if (!curfname.GetLength()) return;
  if (!LoadFSMCompilerArgv()) return;

  //fdStdInPipe is used to send data to fsmCompiler, which reads it using stdin
  int fdStdIn;
  //1. save current STDIN
  fdStdIn = _dup(0);
  //2. create anonymous pipe to be STDIN for child process NOINHERITABLE
  //   as write handle must be noinheritable
  int fdStdInPipe[2];
  if(_pipe(fdStdInPipe, 4096, O_TEXT | O_NOINHERIT) == -1) return;
  //3. set STDIN of the parent to be the read handle to the pipe, so it is INHERITED
  //   by the child process
  if(_dup2(fdStdInPipe[READ_FD], 0) != 0) return;
  //4. close the inheritable read handle
  close(fdStdInPipe[READ_FD]);

  //fdStdOutPipe is used to get stdout from fsmCompiler
  int fdStdOut;
  int fdStdOutPipe[2];
  if(_pipe(fdStdOutPipe, 4096, O_TEXT | O_NOINHERIT) == -1) return;
  fdStdOut = _dup(1);
  if(_dup2(fdStdOutPipe[WRITE_FD], 1) != 0) return;
  close(fdStdOutPipe[WRITE_FD]);

  //fill in the commandline parameters for fsmCompiler
  Compileargv[1]="-stdin";
  Compileargv[2]="-o";
  CString fname2 = CString("\"") + curfname + CString("\"");
  Compileargv[3]=fname2.GetBuffer();
  Compileargv[4]="-c";
  RString configFile2=RString("\"")+configName+RString("\"");
  Compileargv[5]=unconst_cast(configFile2.Data());
  Compileargv[6]=NULL;

  //run FSMCompiler -stdin -o outputFileName -c configFileName
  //and read possible Error report from FSMEditor's stdout
  HANDLE hProcess = (HANDLE)spawnvp(P_NOWAIT, Compileargv[0], Compileargv);
  //fsmCompiler had to be spawned before writing, or writing must be done by fork()
  //  as process writing into pipe is due to buffering waiting, until receiver reads it
  //write FSM in *.bifsm format into pipe
  QOStrStream outStream;
  wGraph.Save(outStream); //this must be changed to check *.bifsm ext or other
  write(fdStdInPipe[WRITE_FD], outStream.str(), outStream.pcount());
  close(fdStdInPipe[WRITE_FD]);

  if(_dup2(fdStdIn, 0) != 0) return; //restore stdin
  close(fdStdIn);
  if(_dup2(fdStdOut, 1) != 0) return; //restore stdout
  close(fdStdOut);

  int nExitCode = STILL_ACTIVE;
  if(hProcess && (unsigned int)hProcess!=0xffffffff)
  {
#ifdef _DEBUG
    QStdDebugStream out;
#endif
    CString errorReport;
    FILE *infile = _fdopen(fdStdOutPipe[READ_FD], "rt");
    while (nExitCode == STILL_ACTIVE)
    {
      //nOutRead = read(fdStdOutPipe[READ_FD], szBuffer, OUT_BUFF_SIZE);
      while(fgets(szBuffer, OUT_BUFF_SIZE, infile))
      {
        szBuffer[strlen(szBuffer)-1]=0;
        errorReport += szBuffer;
        errorReport += "\r\n";
#ifdef _DEBUG
        out << szBuffer; //for debug purposes
#endif
      }
      if(!GetExitCodeProcess(hProcess,(unsigned long*)&nExitCode)) return;
    }
    if (nExitCode)
    {
      CMainFrame *frm=(CMainFrame *)AfxGetMainWnd();
      frm->compResultDlg->editText = errorReport;
      CEdit *edit = (CEdit*)frm->compResultDlg->GetDlgItem(IDC_COMPILE_EDIT);
      frm->compResultDlg->ShowWindow(SW_SHOW);
      frm->compResultDlg->UpdateData(FALSE);
      frm->compResultDlg->SetFocus();
      edit->SetSel(0,0);
    }
  }
}

#include "el/Pathname/Pathname.h"
#include "../FsmNameDlg.h"
void CMainFrmToolbar::OnFileSaveas()
{
  FSMEditor &ed = static_cast<FSMEditor &>(wGraph);
  //allow fsmName change
  FsmNameDlg fsmdlg;
  fsmdlg.fsmName= ed.fsmName;
  if (fsmdlg.DoModal()==IDOK) 
    ed.fsmName = fsmdlg.fsmName;
  //browse for file name
  CString defext; defext.LoadString(IDS_DIAGRAMDEFEXT);
  CString filter;filter.LoadString(IDS_DIAGRAMFILTER);
  CString name;name.LoadString(IDS_SAVEDIAGRAMTITLE);
  char fname[1024];
  GetFilename(fname, curfname.GetBuffer());
  CSelectFileDialog sfdlg(FALSE,defext,fname,OFN_PATHMUSTEXIST|OFN_OVERWRITEPROMPT|OFN_HIDEREADONLY,filter);
  sfdlg.m_ofn.lpstrTitle=name;
  if (curfname!="")
  { //if curFileName has defext extension, set filter to second pair (ie *.*)
    RString ext(GetFileExt(curfname.GetBuffer())); ext.Lower();
    if (ext!=RString(defext)) sfdlg.m_ofn.nFilterIndex=2;
  }
  RString fullPath, fileTitle;
  if (sfdlg.DoModal()==IDOK)
  {
    Pathname path(sfdlg.GetPathName());
    curfname=path.GetFullPath();
    fileTitle = path.GetTitle();
  } else return; //cancel or other failure
  if (ed.fsmName=="") ed.fsmName = fileTitle;
  RString ext(GetFileExt(ed.fileName));
  ext.Lower();
  CString bifsmExt; bifsmExt.LoadString(IDS_DIAGRAMDEFEXT);
  if (ext==RString(bifsmExt))
  {
    wGraph.Save(curfname); //this must be changed to check *.bifsm ext or other
  }
  else 
  {
    SaveThroughFSMCompiler();
  }
  ((CMainFrame *)AfxGetMainWnd())->UpdateCaption();
}

//!argv determines the process to run
bool GetStdoutFromProcess(char **argv, QOStrStream &output)
{
  //fdStdOutPipe is used to get stdout from process
  int fdStdOut, fdStdIn;
  int fdStdOutPipe[2];
  if(_pipe(fdStdOutPipe, 4096, O_TEXT | O_NOINHERIT) == -1) return false;
  fdStdOut = _dup(1);
  if(_dup2(fdStdOutPipe[WRITE_FD], 1) != 0) return false;
  close(fdStdOutPipe[WRITE_FD]);
  fdStdIn = _dup(0);
  if(_dup2(fdStdOutPipe[READ_FD], 0) != 0) return false;
  close(fdStdOutPipe[READ_FD]);

  HANDLE hProcess = (HANDLE)spawnvp(P_NOWAIT, Compileargv[0], Compileargv);
  if(_dup2(fdStdOut, 1) != 0) return false; //restore stdout
  close(fdStdOut);

  int nExitCode = STILL_ACTIVE;
  if(hProcess && (unsigned int)hProcess!=0xffffffff)
  {
    QStdInStream qstdin;
    while (nExitCode == STILL_ACTIVE)
    {
      while (!qstdin.eof())
      {
        qstdin.readLine(szBuffer, OUT_BUFF_SIZE);
        output << szBuffer << "\r\n";
      }
      if(!GetExitCodeProcess(hProcess,(unsigned long*)&nExitCode)) return false;
    }
    if(_dup2(fdStdIn, 0) != 0) return false; //restore stdout
    close(fdStdIn);
/*  //debug purpose:
    LogF("*** Loaded file length: %d", output.pcount());
    QOFStream debugOut("debugOut1.bifsm");
    debugOut.write(output.str(), output.pcount());
    debugOut.close();
*/
    return (nExitCode==0);
  }
  return false;
}

bool CMainFrmToolbar::OpenThroughFSMCompiler()
{ // open using fsmcompiler.exe -d filename -stdout
  CString &fsmName=static_cast<FSMEditor *>(&wGraph)->fsmName;
  if (!curfname.GetLength()) return false;
  if (!LoadFSMCompilerArgv()) return false;
  QOStrStream fsmInside;
  Compileargv[1]="-l";
  CString fname = CString("\"")+curfname.GetBuffer()+CString("\"");
  Compileargv[2]=fname.GetBuffer();
  Compileargv[3]=NULL;
  if (GetStdoutFromProcess(Compileargv, fsmInside))
  {
    SelectFSMNameDlg dlg(fsmInside,this);
    if (dlg.fsmNameCount>1)
    {
      if (dlg.DoModal()==IDOK) fsmName=dlg.fsmName;
    }
    else if (dlg.fsmNameCount==1)
    {
      const int maxLineLen=1024;
      QIStrStream in; in.init(fsmInside);
      RString nextLine; nextLine.CreateBuffer(maxLineLen);
      while (!in.eof())
      {
        in.readLine(nextLine.MutableData(), maxLineLen);
        if (nextLine!=RString("")) { fsmName=nextLine.Data(); break; }
      }    
    }
    else 
    {
      AfxMessageBox("There are no FSM inside this file!");
      return false;
    }
  }
  else
  {
    AfxMessageBox("Cannot open!", MB_OK);
    return false;
  }

  //fill in the commandline parameters for fsmCompiler
  Compileargv[1]="-stdout";
  Compileargv[2]="-d";
  //CString fnameQt = CString("\"") + fname + CString("\"");
  Compileargv[3]=fname.GetBuffer();
  Compileargv[4]="-f";
  CString fsmnameQt = CString("\"") + fsmName + CString("\"");
  Compileargv[5]=fsmnameQt.GetBuffer();
  Compileargv[6]=NULL;
  QOStrStream output;
  if (GetStdoutFromProcess(Compileargv, output))
  {
    QIStrStream bifsmStream(output.str(), output.pcount());
    if (wGraph.Load(bifsmStream)==false)
    {
      AfxMessageBox(IDS_LOADFAILED);
      return false;
    }
    AfxGetMainWnd()->SendMessage(MSG_WNDSERIALIZE,0,0);  //untouch edit window
    wGraph.Update();
  }
  else
  {
    AfxMessageBox("Cannot open!", MB_OK);
    return false;
  }
  return true;
}

void CMainFrmToolbar::OnFileOpen()
{
  AfxGetMainWnd()->SendMessage(MSG_PREPARE_FOR_SAVING,0,0);
  int answer = wGraph.IsDirty() ? AfxMessageBox(IDS_ASKONLOAD,MB_YESNOCANCEL|MB_ICONQUESTION|MB_DEFBUTTON2) : IDNO;
  if (answer!=IDCANCEL)
  {
    if (answer==IDYES) OnFileSave();
    CString defext;defext.LoadString(IDS_DIAGRAMDEFEXT);
    CString filter;filter.LoadString(IDS_DIAGRAMFILTER);
    CString name;name.LoadString(IDS_OPENDIAGRAMTITLE);
    CFileDialog fdlg(TRUE,defext,curfname,OFN_FILEMUSTEXIST|OFN_HIDEREADONLY,filter);
    fdlg.m_ofn.lpstrTitle=name;
    if (curfname!="")
    { //if curFileName has defext extension, set filter to second pair (ie *.*)
      RString ext(GetFileExt(curfname.GetBuffer())); ext.Lower();
      if (ext!=RString(defext)) fdlg.m_ofn.nFilterIndex=2;
    }
    if (fdlg.DoModal()==IDOK)
    {
      CString backupFileName = curfname;
      curfname=fdlg.GetPathName();

      RString ext(GetFileExt(curfname.GetBuffer())); ext.Lower();
      if (ext==RString(defext))
      {
        if (wGraph.Load(curfname)==false) 
        {
          AfxMessageBox(IDS_LOADFAILED);
          curfname=backupFileName;
        }
      }
      else 
      {
        if (!OpenThroughFSMCompiler()) curfname = backupFileName;
      }
      AfxGetMainWnd()->SendMessage(MSG_WNDSERIALIZE,0,0);  //untouch edit window
      gExtEdFileManager.Init((FSMEditor *)&wGraph);
      wGraph.Update();
    }
  }
  ((CMainFrame *)AfxGetMainWnd())->UpdateCaption();
}

void CMainFrmToolbar::OnUpdateEditUndo(CCmdUI *pCmdUI)
{
  //pCmdUI->Enable(undo!=NULL);
}

//#pragma optimize ("g",off) //Problems with optimalization in MSVC 6.0 in following function frame


void CMainFrmToolbar::OnEditUndo()
{
  if (wGraph.undo.RedoUninitialized()) 
  {
    wGraph.SaveUndo();
    wGraph.undo.Undo();
    wGraph.undo.DecrementRedo();
  }
  Ref<ParamArchiveSaveUndo> undoar = wGraph.undo.Undo();
  if (!undoar) return;
  wGraph.ResetContent(); // do not save undo inside (it has caused deletion of undoar content
  ParamArchiveLoad arch(*undoar);
  wGraph.Serialize(arch);
  AfxGetMainWnd()->SendMessage(MSG_WNDSERIALIZE,0,(LPARAM)&arch);
  wGraph.Update();
}

void CMainFrmToolbar::OnEditRedo()
{
  ParamArchiveSave *undoar = wGraph.undo.Redo();
  if (!undoar) return;
  wGraph.ResetContent();
  ParamArchiveLoad arch(*undoar);
  wGraph.Serialize(arch);
  AfxGetMainWnd()->SendMessage(MSG_WNDSERIALIZE,0,(LPARAM)&arch);
  wGraph.Update();
}

CString gExternalEditor;
CString gServiceDDE;
BOOL gUseDDE;
BOOL gVertSplit;
void CMainFrmToolbar::OnExternalEditor()
{
  ExternalEditorDlg dlg;
  dlg.externalEditor = gExternalEditor;
  dlg.useDDE = gUseDDE;
  dlg.serviceDDE = gServiceDDE;
  if (dlg.DoModal()==IDOK) 
  {
   gExternalEditor = dlg.externalEditor;
   gUseDDE = dlg.useDDE;
   gServiceDDE = dlg.serviceDDE;
   ((CMainFrame *)AfxGetMainWnd())->TextEdit.Invalidate();
  }
}

//#pragma optimize ("",on)

void CMainFrmToolbar::OnFilePrint()
{
}
