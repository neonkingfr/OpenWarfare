#ifndef _DESIGNTLB_H
#define _DESIGNTLB_H

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "designtlbresource.h"
#include "../GraphCtrlExt.h"
#include "DlgLineStyle.h"

class CMainFrame;
class CDesignToolbar : public CToolBar
{
public:
  DECLARE_DYNAMIC(CDesignToolbar)

  CDesignToolbar(CGraphCtrlExt &wGraph1);   // standard constructor
  virtual ~CDesignToolbar();

  //CMainFrame *_parentWnd;
  CGraphCtrlExt &wGraph;
  HACCEL hAccelerator;
  // Dialog Data

  // Overrides
  // ClassWizard generated virtual function overrides
  //{{AFX_VIRTUAL(CDesignToolbar)
  //}}AFX_VIRTUAL

  // Generated message map functions
  //{{AFX_MSG(CDesignToolbar)
  afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
  afx_msg void OnUpdateSelected(CCmdUI* pCmdUI);

  afx_msg void OnDiagramInsertnew();
  afx_msg void OnModeDesign();
  afx_msg void OnUpdateModeDesign(CCmdUI* pCmdUI);
  afx_msg void OnModeZoom();
  afx_msg void OnUpdateModeZoom(CCmdUI* pCmdUI);
  afx_msg void OnModeLinks();
  afx_msg void OnUpdateModeLinks(CCmdUI* pCmdUI);
  afx_msg void OnDiagramShapeDiamond();
  afx_msg void OnDiagramShapeEllipse();
  afx_msg void OnViewResetzoom();
  afx_msg void OnDiagramShapeBox();
  afx_msg void OnDiagramShapeParallelogramleft();
  afx_msg void OnDiagramShapeParallelogramright();
  afx_msg void OnDiagramShapeDoublesidedbox();
  afx_msg void OnDiagramLinesStyle();
  afx_msg void OnDiagramShapeRoundedbox();
  afx_msg void OnDiagramBackgroundColor();
  afx_msg void OnDiagramBackgroundTransparent();
  afx_msg void OnUpdateDiagramBackgroundTransparent(CCmdUI* pCmdUI);
  afx_msg void OnDiagramLinesColor();
  afx_msg void OnDiagramLinesTransparent();
  afx_msg void OnUpdateDiagramLinesTransparent(CCmdUI* pCmdUI);
  afx_msg void OnDiagramEdittext();
  afx_msg void OnUpdateDiagramInsertnew(CCmdUI* pCmdUI);
  //}}AFX_MSG
  DECLARE_MESSAGE_MAP()
public:
  BOOL OnInitDialog(void);
  BOOL PreTranslateMessage(MSG* pMsg);
  BOOL PreCreateWindow(CREATESTRUCT& cs);
  BOOL Create(CWnd * parent, UINT id);
  //virtual void OnUpdateCmdUI(CFrameWnd *pTarget, BOOL bDisableIfNoHndler);
};

#endif
