//{{NO_DEPENDENCIES}}
#ifndef WM_APP
#define WM_APP       0x8000
#endif

#define ID_EDIT_DELETE                  (WM_APP+880)
#define ID_EDIT_DUPLICATE               (WM_APP+881)
#define ID_FILE_SAVEEMF                 (WM_APP+882)
#define ID_FILE_DISCHARGE               (WM_APP+883)
#define ID_FILE_SAVEAS                  (WM_APP+884)
#define ID_EDIT_GROUP                   (WM_APP+885)
#define ID_EDIT_MOVEFRONT               (WM_APP+886)
#define ID_EDIT_MOVEBACK                (WM_APP+887)
#define ID_FILE_PAGESETUP               (WM_APP+888)
#define ID_ATTR_EDIT			(WM_APP+889)

#define ID_MAINTLB_START                880
//the following shouldn't be defined as (ID_MAINTLB_START+nn) {because of resource definition, resource is not found then}
#define IDR_MAINTLB                     880
#define IDR_MAINTLBACC                  881

#define IDS_EMFFILEFILTER               (ID_MAINTLB_START+5)
#define IDS_EMFDEFEXT                   (ID_MAINTLB_START+6)
#define IDS_EMFTITLE                    (ID_MAINTLB_START+7)
#define IDS_EXPORTFAILED                (ID_MAINTLB_START+8)
#define IDS_NEWASK                      (ID_MAINTLB_START+9)
#define IDS_DIAGRAMDEFEXT               (ID_MAINTLB_START+10)
#define IDS_DIAGRAMFILTER               (ID_MAINTLB_START+11)
#define IDS_SAVEDIAGRAMTITLE            (ID_MAINTLB_START+12)
#define IDS_SAVEFAILED                  (ID_MAINTLB_START+13)
#define IDS_LOADFAILED                  (ID_MAINTLB_START+14)
#define IDS_OPENDIAGRAMTITLE            (ID_MAINTLB_START+15)
#define IDS_NOPRINTERSELECTED           (ID_MAINTLB_START+16)
#define IDS_CANNOTINICIALIZEPRINTER     (ID_MAINTLB_START+17)

