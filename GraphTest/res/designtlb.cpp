// CDesignToolbar.cpp : implementation of the CDesignToolbar class
//

#include "../stdafx.h"
#include "designtlb.h"

/////////////////////////////////////////////////////////////////////////////
// CDesignToolbar

IMPLEMENT_DYNAMIC(CDesignToolbar, CToolBar)

BEGIN_MESSAGE_MAP(CDesignToolbar, CToolBar)
  //{{AFX_MSG_MAP(CDesignToolbar)
  ON_WM_CREATE()

  ON_COMMAND(ID_DIAGRAM_INSERTNEW, OnDiagramInsertnew)
  ON_UPDATE_COMMAND_UI(ID_DIAGRAM_INSERTNEW, OnUpdateDiagramInsertnew)
  ON_COMMAND(ID_MODE_DESIGN, OnModeDesign)
  ON_UPDATE_COMMAND_UI(ID_MODE_DESIGN, OnUpdateModeDesign)
  ON_COMMAND(ID_MODE_LINKS, OnModeLinks)
  ON_UPDATE_COMMAND_UI(ID_MODE_LINKS, OnUpdateModeLinks)
  ON_COMMAND(ID_MODE_ZOOM, OnModeZoom)
  ON_UPDATE_COMMAND_UI(ID_MODE_ZOOM, OnUpdateModeZoom)
  ON_COMMAND(ID_VIEW_RESETZOOM, OnViewResetzoom)

  ON_COMMAND(ID_DIAGRAM_SHAPE_BOX, OnDiagramShapeBox)
   ON_UPDATE_COMMAND_UI(ID_DIAGRAM_SHAPE_BOX, OnUpdateSelected)
  ON_COMMAND(ID_DIAGRAM_SHAPE_DIAMOND, OnDiagramShapeDiamond)
   ON_UPDATE_COMMAND_UI(ID_DIAGRAM_SHAPE_DIAMOND, OnUpdateSelected)
  ON_COMMAND(ID_DIAGRAM_SHAPE_ELLIPSE, OnDiagramShapeEllipse)
   ON_UPDATE_COMMAND_UI(ID_DIAGRAM_SHAPE_ELLIPSE, OnUpdateSelected)
  ON_COMMAND(ID_DIAGRAM_SHAPE_PARALLELOGRAMLEFT, OnDiagramShapeParallelogramleft)
   ON_UPDATE_COMMAND_UI(ID_DIAGRAM_SHAPE_PARALLELOGRAMLEFT, OnUpdateSelected)
  ON_COMMAND(ID_DIAGRAM_SHAPE_PARALLELOGRAMRIGHT, OnDiagramShapeParallelogramright)
   ON_UPDATE_COMMAND_UI(ID_DIAGRAM_SHAPE_PARALLELOGRAMRIGHT, OnUpdateSelected)
  ON_COMMAND(ID_DIAGRAM_SHAPE_ROUNDEDBOX, OnDiagramShapeRoundedbox)
   ON_UPDATE_COMMAND_UI(ID_DIAGRAM_SHAPE_ROUNDEDBOX, OnUpdateSelected)
  ON_COMMAND(ID_DIAGRAM_SHAPE_DOUBLESIDEDBOX, OnDiagramShapeDoublesidedbox)
   ON_UPDATE_COMMAND_UI(ID_DIAGRAM_SHAPE_DOUBLESIDEDBOX, OnUpdateSelected)
  
  ON_COMMAND(ID_DIAGRAM_LINES_STYLE, OnDiagramLinesStyle)
  ON_COMMAND(ID_DIAGRAM_BACKGROUND_COLOR, OnDiagramBackgroundColor)
  ON_COMMAND(ID_DIAGRAM_BACKGROUND_TRANSPARENT, OnDiagramBackgroundTransparent)
  ON_UPDATE_COMMAND_UI(ID_DIAGRAM_BACKGROUND_TRANSPARENT, OnUpdateDiagramBackgroundTransparent)
  ON_COMMAND(ID_DIAGRAM_LINES_COLOR, OnDiagramLinesColor)
  ON_COMMAND(ID_DIAGRAM_LINES_TRANSPARENT, OnDiagramLinesTransparent)
  ON_UPDATE_COMMAND_UI(ID_DIAGRAM_LINES_TRANSPARENT, OnUpdateDiagramLinesTransparent)
  ON_COMMAND(ID_DIAGRAM_EDITTEXT, OnDiagramEdittext)
  ON_UPDATE_COMMAND_UI(ID_DIAGRAM_EDITTEXT, OnUpdateSelected)
  //}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDesignToolbar message handlers

BOOL CDesignToolbar::OnInitDialog(void)
{
  return false;
}

int CDesignToolbar::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
  if (CToolBar::OnCreate(lpCreateStruct) == -1)
    return -1;
  //load accelerator table

  hAccelerator = LoadAccelerators(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDR_DESIGNTLBACC));

  return 0;
}

/*
void CDesignToolbar::OnUpdateCmdUI(CFrameWnd *pTarget, BOOL bDisableIfNoHndler)
{
}
*/

BOOL CDesignToolbar::PreTranslateMessage(MSG* pMsg) 
{
  if (TranslateAccelerator(*this,hAccelerator,pMsg)) return true;
  return CToolBar::PreTranslateMessage(pMsg);
}

CDesignToolbar::CDesignToolbar(CGraphCtrlExt &wGraph1)
: CToolBar(), wGraph(wGraph1)
{
}

CDesignToolbar::~CDesignToolbar()
{
}

BOOL CDesignToolbar::PreCreateWindow(CREATESTRUCT& cs)
{
  if( !CToolBar::PreCreateWindow(cs) )
    return FALSE;

  //  the CREATESTRUCT cs

  //cs.lpszClass = AfxRegisterWndClass(0); //???
  return TRUE;
}

/////////////////////////////////////////////////////////////////////////////
// CDesignToolbar message handlers

void CDesignToolbar::OnUpdateSelected(CCmdUI* pCmdUI) 
{
  //LogF("updateSelected: %d", pCmdUI->m_nIndex);
  pCmdUI->Enable(wGraph.EnumItems(-1,true)!=-1);
}

void CDesignToolbar::OnDiagramInsertnew() 
{
  wGraph.Mode(SGRM_NEWITEM);
}

void CDesignToolbar::OnModeDesign() 
{
  wGraph.Mode(SGRM_DESIGN);
}

void CDesignToolbar::OnUpdateModeDesign(CCmdUI* pCmdUI) 
{
  pCmdUI->SetCheck(wGraph.Mode(SGRM_GETCURRENT)==SGRM_DESIGN);
}

void CDesignToolbar::OnModeZoom() 
{
  wGraph.Mode(SGRM_ZOOM);
}

void CDesignToolbar::OnUpdateModeZoom(CCmdUI* pCmdUI) 
{
  pCmdUI->SetCheck(wGraph.Mode(SGRM_GETCURRENT)==SGRM_ZOOM);
}

void CDesignToolbar::OnModeLinks() 
{
  wGraph.Mode(SGRM_NORMAL);
}

void CDesignToolbar::OnUpdateModeLinks(CCmdUI* pCmdUI) 
{
  pCmdUI->SetCheck(wGraph.Mode(SGRM_GETCURRENT)==SGRM_NORMAL);
}

void CDesignToolbar::OnDiagramShapeBox() 
{
  wGraph.ChangeShape(stBox);

}

void CDesignToolbar::OnDiagramShapeDiamond() 
{
  wGraph.ChangeShape(stDiamond);

}

void CDesignToolbar::OnDiagramShapeEllipse() 
{
  wGraph.ChangeShape(stEllipse);

}

void CDesignToolbar::OnDiagramShapeParallelogramleft() 
{
  wGraph.ChangeShape(stParallelogramL);

}

void CDesignToolbar::OnDiagramShapeRoundedbox() 
{
  wGraph.ChangeShape(stRoundedBox);	
}

void CDesignToolbar::OnDiagramShapeParallelogramright() 
{
  wGraph.ChangeShape(stParallelogramR);

}

void CDesignToolbar::OnViewResetzoom() 
{
  SFloatRect rc=wGraph.GetBoundingBox();
  float xs=(rc.right-rc.left)*0.2f;
  float ys=(rc.bottom-rc.top)*0.2f;
  rc.left-=xs;
  rc.top-=ys;
  rc.right+=xs;
  rc.bottom+=ys;
  wGraph.SetPanZoom(rc,true);
  wGraph.Update();
}

void CDesignToolbar::OnDiagramShapeDoublesidedbox() 
{

}

void CDesignToolbar::OnDiagramLinesStyle() 
{
  DlgLineStyle dlg;
  ItemInfo *nfo=wGraph.GetRefItem();
  dlg.vLineStyle=nfo->lStyle;
  dlg.vLineWidth=nfo->lWidth;
  if (dlg.DoModal())
  {
    wGraph.ChangeLineWidth(dlg.vLineWidth,dlg.vLineStyle);
  }  
}

void CDesignToolbar::OnDiagramBackgroundColor() 
{
  CColorDialog colorDialog(0,CC_FULLOPEN|CC_RGBINIT|CC_ANYCOLOR);
  colorDialog.m_cc.rgbResult=wGraph.GetRefColor();
  if (colorDialog.DoModal())
  {
    wGraph.ChangeBkColor(colorDialog.GetColor());
  }
}

void CDesignToolbar::OnDiagramBackgroundTransparent() 
{
  wGraph.ChangeBkColor(TRANSPARENTCOLOR);
}

void CDesignToolbar::OnUpdateDiagramBackgroundTransparent(CCmdUI* pCmdUI) 
{
  pCmdUI->SetCheck(wGraph.GetRefColor()==TRANSPARENTCOLOR);
}

void CDesignToolbar::OnDiagramLinesColor() 
{
  CColorDialog colorDialog(0,CC_FULLOPEN|CC_RGBINIT|CC_ANYCOLOR);
  colorDialog.m_cc.rgbResult=wGraph.GetRefItem()->lineColor;
  if (colorDialog.DoModal())
  {
    wGraph.ChangeLineColor(colorDialog.GetColor());
  }
}

void CDesignToolbar::OnDiagramLinesTransparent() 
{
  wGraph.ChangeLineColor(TRANSPARENTCOLOR);	
}

void CDesignToolbar::OnUpdateDiagramLinesTransparent(CCmdUI* pCmdUI) 
{
  pCmdUI->SetCheck(wGraph.GetRefItem()->lineColor==TRANSPARENTCOLOR);
}

/*
bool CDesignToolbar::CallDiagramEditText(void)
{
  ItemInfo *info=wGraph.GetRefItem();
  int align[3]=
  {DT_LEFT,DT_CENTER,DT_RIGHT};
  DlgText dlg;
  dlg.vText=info->text;
  dlg.vFont=info->lgFont;
  dlg.vColor=info->fontColor;
  for (dlg.vAlign=0;dlg.vAlign<3 && align[dlg.vAlign]!=info->textAlign;dlg.vAlign++);
  if (dlg.DoModal()==IDOK)
  {
    wGraph.SetTextContentAndStyle(dlg.vText,dlg.vFont,dlg.vColor,align[dlg.vAlign]);
    return true;
  }
  return false;
}
*/
void CDesignToolbar::OnDiagramEdittext() 
{
  wGraph.BeginEditText();
}

void CDesignToolbar::OnUpdateDiagramInsertnew(CCmdUI* pCmdUI) 
{
  pCmdUI->SetCheck(wGraph.Mode(SGRM_GETCURRENT)==SGRM_NEWITEM );	
}

