// DlgLineStyle.cpp : implementation file
//

#include "../stdafx.h"
#include "../GraphCtrlExt.h"
#include "DlgLineStyle.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// DlgLineStyle dialog


DlgLineStyle::DlgLineStyle(CWnd* pParent /*=NULL*/)
	: CDialog(DlgLineStyle::IDD, pParent)
{
	//{{AFX_DATA_INIT(DlgLineStyle)
	vLineStyle = -1;
	vLineWidth = 1;
	//}}AFX_DATA_INIT
}


void DlgLineStyle::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(DlgLineStyle)
	DDX_Control(pDX, IDC_LINESTYLE, wLineStyle);
	DDX_CBIndex(pDX, IDC_LINESTYLE, vLineStyle);
	DDX_Text(pDX, IDC_LINEWIDTH, vLineWidth);
	DDV_MinMaxInt(pDX, vLineWidth, 1, 20);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(DlgLineStyle, CDialog)
	//{{AFX_MSG_MAP(DlgLineStyle)
	ON_WM_DRAWITEM()
	ON_NOTIFY(UDN_DELTAPOS, IDC_LINESTYLESPIN, OnDeltaposLinestylespin)
	ON_EN_CHANGE(IDC_LINEWIDTH, OnChangeLinewidth)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// DlgLineStyle message handlers

void DlgLineStyle::OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct) 
{
	if (nIDCtl==IDC_LINESTYLE)
	  {
	  int itemID=lpDrawItemStruct->itemID;
	  if (itemID<0) return;
	  CDC dc;
	  dc.Attach(lpDrawItemStruct->hDC);
	  RECT &bound=lpDrawItemStruct->rcItem;
	  COLORREF pencolor;
	  COLORREF bgcolor;
	  int width=GetDlgItemInt(IDC_LINEWIDTH);
	  if (lpDrawItemStruct->itemState & ODS_SELECTED)
		{
		bgcolor=GetSysColor(COLOR_HIGHLIGHT);
		pencolor=GetSysColor(COLOR_HIGHLIGHTTEXT);
		}
	  else
		{
		bgcolor=GetSysColor(COLOR_WINDOW);
		pencolor=GetSysColor(COLOR_WINDOWTEXT);
		}
	  CPen pen(lpDrawItemStruct->itemData,width,pencolor);
	  CPen *oldpen=dc.SelectObject(&pen);
	  dc.FillSolidRect(&bound,bgcolor);
	  CRect rc=bound;
	  rc-=CRect(3,3,3,3);
	  dc.Rectangle(&rc);
	  dc.SelectObject(oldpen);
	  dc.Detach();
	  }
	else
	  CDialog::OnDrawItem(nIDCtl, lpDrawItemStruct);
}

BOOL DlgLineStyle::OnInitDialog() 
{
	CDialog::OnInitDialog();

	wLineStyle.AddString((LPCTSTR)PS_SOLID);
	wLineStyle.AddString((LPCTSTR)PS_DASH );
	wLineStyle.AddString((LPCTSTR)PS_DOT );
	wLineStyle.AddString((LPCTSTR)PS_DASHDOT );
	wLineStyle.AddString((LPCTSTR)PS_DASHDOTDOT );
	wLineStyle.AddString((LPCTSTR)PS_INSIDEFRAME );
	wLineStyle.SelectString(-1,(LPCTSTR)vLineStyle);
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}



void DlgLineStyle::OnDeltaposLinestylespin(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_UPDOWN* pNMUpDown = (NM_UPDOWN*)pNMHDR;
	int pos=GetDlgItemInt(IDC_LINEWIDTH);
	pos-=pNMUpDown->iDelta;
	if (pos<1) pos=1;
	if (pos>20) pos=20;
	SetDlgItemInt(IDC_LINEWIDTH,pos);
	*pResult = 0;
}

void DlgLineStyle::OnChangeLinewidth() 
  {
  wLineStyle.Invalidate();
  }

void DlgLineStyle::OnOK() 
  {
  UpdateData();
  int p=wLineStyle.GetItemData(vLineStyle);
  CDialog::OnOK();
  vLineStyle=p;
  }
