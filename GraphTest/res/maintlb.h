#ifndef _MAINTLB_H
#define _MAINTLB_H

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

//#include "maintlbresource.h"
#include "../resource.h"
#include "../GraphCtrlExt.h"

#define CF_FSM_EDITOR (CF_PRIVATEFIRST+0x3b)

class CMainFrame;
class CMainFrmToolbar : public CToolBar
{
public:
  DECLARE_DYNAMIC(CMainFrmToolbar)

  CMainFrmToolbar(CGraphCtrlExt &wGraph1, CString &cuffname1);   // standard constructor
  virtual ~CMainFrmToolbar();

  //CMainFrame *_parentWnd;
  CGraphCtrlExt &wGraph;
  CString &curfname;
  HACCEL hAccelerator;
  // Dialog Data

  // Overrides
  // ClassWizard generated virtual function overrides
  //{{AFX_VIRTUAL(CMainFrmToolbar)
  //}}AFX_VIRTUAL

  // Generated message map functions
  //{{AFX_MSG(CMainFrmToolbar)
  afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
  afx_msg void OnUpdateSelected(CCmdUI* pCmdUI);

  afx_msg void OnFileNew();
  afx_msg void OnFileOpen();
  afx_msg void OnFileSave();
  afx_msg void OnFileSaveas();
  afx_msg void OnFileSaveemf();

  afx_msg void OnFilePrint();

  afx_msg void OnEditUndo();
  afx_msg void OnUpdateEditUndo(CCmdUI *pCmdUI);
  afx_msg void OnExternalEditor();
  afx_msg void OnEditDelete();
  afx_msg void OnUpdateEditDelete(CCmdUI* pCmdUI);
  afx_msg void OnSelectAll() {wGraph.SelectAllItems();}
  afx_msg void OnEditCut();
  afx_msg void OnEditCopy();
  afx_msg void OnEditPaste();
  afx_msg void OnUpdatePaste(CCmdUI* pCmdUI);
  afx_msg void OnEditDuplicate();
  afx_msg void OnEditGroup();
  afx_msg void OnUpdateEditGroup(CCmdUI* pCmdUI);
  afx_msg void OnEditMovefront();
  afx_msg void OnEditMoveback();
  afx_msg void OnAttrEdit();
  //}}AFX_MSG
  DECLARE_MESSAGE_MAP()
public:
  BOOL OnInitDialog(void);
  BOOL PreTranslateMessage(MSG* pMsg);
  BOOL PreCreateWindow(CREATESTRUCT& cs);
  BOOL Create(CWnd * parent, UINT id);
//virtual void OnUpdateCmdUI(CFrameWnd *pTarget, BOOL bDisableIfNoHndler);
  afx_msg void OnEditRedo();
  bool OpenThroughFSMCompiler();
  void SaveThroughFSMCompiler();
  void NewFile();
};

#endif
