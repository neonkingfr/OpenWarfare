#ifndef _DLGBARFONT_H
#define _DLGBARFONT_H

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

// DlgBarFont dialog
#include "../StdAfx.h"
#include "dlgbarfontresource.h"

class DlgBarFont : public CDialogBar
{
  DECLARE_DYNAMIC(DlgBarFont)
  bool _enablechange;

public:
  DlgBarFont(CWnd* pParent = NULL);   // standard constructor
  virtual ~DlgBarFont();

  COLORREF vColor;

  // Dialog Data
  //{{AFX_DATA(DlgBarFont)

  enum { IDD = IDD_FONTPROPERTIESTOOLBAR };
  //}}AFX_DATA	


  // Overrides
  // ClassWizard generated virtual function overrides
  //{{AFX_VIRTUAL(DlgBarFont)
  //}}AFX_VIRTUAL

  void OnDrawFontFace(LPDRAWITEMSTRUCT lpDrawItemStruct);
  void OnDrawColorButton(LPDRAWITEMSTRUCT lpDis);

  // Generated message map functions
  //{{AFX_MSG(DlgBarFont)
  afx_msg void OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct);
  afx_msg void OnMeasureItem(int nIDCtl, LPMEASUREITEMSTRUCT lpMeasureItemStruct);
  afx_msg void OnBnStyleClicked();
  afx_msg void OnEditchangeFontface();
  afx_msg void OnChangeFontsize();
  afx_msg void OnChangeWidth();
  afx_msg void OnChangeAngle();
  afx_msg void OnSelchangeFontface();
  afx_msg void OnColorbutt();
  afx_msg void OnSetAlign();
  afx_msg void OnDeltaposFontsizespin(NMHDR* pNMHDR, LRESULT* pResult);
  afx_msg void OnDeltaposAnglespin(NMHDR* pNMHDR, LRESULT* pResult);
  afx_msg void OnDeltaposWidthspin(NMHDR* pNMHDR, LRESULT* pResult);
  //}}AFX_MSG
  DECLARE_MESSAGE_MAP()
public:
  virtual void OnUpdateCmdUI( CFrameWnd* pTarget, BOOL bDisableIfNoHndler );
  BOOL OnInitDialog(void);
  BOOL Create(CWnd * parent, UINT id);

  //UpdateFields is templatized for every structure T with the following members:
  //struct FontInfo
  //{
  // LOGFONT lgFont;
  // COLORREF fontColor;
  // int textAlign;
  // float ffHeight;
  // float ffWidth;
  //};
  template<class T>
  void UpdateFields(T * info)
  {
    _enablechange=false;
    CDataExchange exch(this,FALSE);
    int h=(int)info->ffHeight;
    int ang=info->lgFont.lfOrientation/10;
    int w=(int)(info->ffWidth*100/info->ffHeight);
    DDX_Text(&exch,IDC_FONTSIZE,h);
    DDX_Text(&exch,IDC_ANGLE,ang);
    DDX_Text(&exch,IDC_WIDTH,w);
    BOOL b=info->lgFont.lfWeight>500;
    DDX_Check(&exch,IDC_FONTBOLD,b);
    b=info->lgFont.lfItalic;
    DDX_Check(&exch,IDC_FONTITALIC,b);
    b=info->lgFont.lfUnderline;
    DDX_Check(&exch,IDC_FONTUNDERLINE,b);
    b=info->lgFont.lfStrikeOut;
    DDX_Check(&exch,IDC_FONTSTRIKEOUT,b);
    DDX_Radio(&exch,IDC_LALIGN,info->textAlign);
    vColor=info->fontColor;
    GetDlgItem(IDC_COLORBUTT)->Invalidate();
    if (info->lgFont.lfFaceName[0]==0) //no font selected, say it is Arial
    {
      strcpy(info->lgFont.lfFaceName,"Arial");
    }
    SendDlgItemMessage(IDC_FONTFACE,CB_SELECTSTRING,-1,(LPARAM)(info->lgFont.lfFaceName));
    _enablechange=true;
  }

  void DeltaPosSpin(int idc, int min, int max, int delta);
};

#endif
