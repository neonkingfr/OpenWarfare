// GraphCtrlExt.h: interface for the CGraphCtrlExt class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(_GRAPH_CTRL_EXT)
#define _GRAPH_CTRL_EXT

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <el/ParamArchive/paramArchiveDb.hpp>
#include "undo.h"
#include "GraphCtrl.h"

#define LINKACTIVE (SGRI_CANSELFLINKED|SGRI_CANBELINKED|SGRI_CANMAKELINKS|SGRI_CANMOVE|SGRI_CANRESIZE)

#define TRANSPARENTCOLOR 0xFFFFFFFF

#define SGRM_NEWITEM 0x4
#define MSG_NEWITEM (WM_APP+10)
//#define MSG_SAVEUNDO (WM_APP+11)
#define MSG_WNDSERIALIZE (WM_APP+11)
#define MSG_NOTIFYDBLCLICK (WM_APP+12)
#define MSG_NOTIFYENDSEL (WM_APP+13)
#define DLGEDIT_STARTUP (WM_APP+14)
#define IDC_TEXTINPUT (WM_APP+15)
#define MSG_DIRTINESS_CHANGED (WM_APP+16)
#define MSG_PREPARE_FOR_SAVING (WM_APP+17)
#define MSG_ATTR_EDIT (WM_APP+18)

//#define MSG_SERIALIZEWINDOW (WM_APP+15)


#define MAXITERS 10000

struct SDLinkFlags1
{
  unsigned int frozen:1;
  unsigned int custom:1;
  unsigned int dontfocus:1;
  unsigned int arrowtype:2;
  unsigned int arrowsize:3;
  unsigned int linestyle:4;
  unsigned int linewidth:4;
};

struct SDLinkInfo : public SerializeClass
{
  unsigned char arrowtype;
  unsigned char arrowsize;
  unsigned char linestyle;
  unsigned char linewidth;
  bool usebkcolor;
  unsigned char pinid;

  SDLinkInfo():
  arrowtype(0),
    arrowsize(1),
    linestyle(0),
    linewidth(1),
    pinid(0),
    usebkcolor(false)
  {}
  SDLinkInfo(const SDLinkInfo& other) {memcpy(this,&other,sizeof(*this));}

  void Convert(SDLinkFlags1 &flgs, int pin)
  {
    arrowtype=flgs.arrowtype;
    linestyle=flgs.linestyle;
    linewidth=flgs.linewidth;
    arrowsize=flgs.arrowsize;
    pinid=pin;
  }

  LSError Serialize(ParamArchive &ar)
  {
    ar.Serialize("ArrowType",arrowtype,1,0);
    ar.Serialize("ArrowSize",arrowsize,1,1);
    ar.Serialize("LineStyle",linestyle,1,0);
    ar.Serialize("LineWidth",linewidth,1,1);
    ar.Serialize("UseBKColor",usebkcolor,1,false);
    ar.Serialize("Pin",pinid,1,0);
    return LSOK;
  }
};

enum ShapeType 
{
  stBox, stEllipse, stDiamond, stParallelogramR, stParallelogramL,stBox2, stRoundedBox
};
struct ItemInfo : public SerializeClass
{
  CString text;
  ShapeType shape;
  COLORREF lineColor;
  LOGFONT lgFont;
  COLORREF fontColor;
  int textAlign;
  int lWidth;
  int lStyle;
  int group;
  float ffHeight;
  float ffWidth;
  ItemInfo();
  virtual ~ItemInfo() {}
  virtual ItemInfo &operator=(ItemInfo &itm2);
  virtual ItemInfo *Clone();
  virtual LSError Serialize(ParamArchive &ar);
  virtual void SerializeBin(SerializeBinStream &f);
  virtual bool GetShouldBeAlignedToGrid() {return true;}
private:
  ItemInfo(ItemInfo &) 
  {
    ASSERT(false);
  }  //no copy
};

class QOStream;
class QIStream;
class CGraphCtrlExt : public CGraphCtrl, public SerializeClass
{
  bool _newitemmode;
  CRect dragrc;
  int grpcnt;
  bool _grpselected;

  HPEN _oldpen;
  HFONT _oldfont, _basicOldFont;
  HBRUSH _oldbrush;

  bool _exportdraw;

  float _grid;
  bool _gridshow;
  bool _dirty;    //true, iff document is changed and not saved
//  bool _showVariables;

  CEdit	   wTextEdit;
  CFont	   wEditFont;
  CBrush   wEditBrush;

//  CFont	   wVarFont;

//  int _expressContext;	//expression context
//  bool _expressChanged; //expression calculations changed
  bool _savingText;

  int _pixelSize;  //for GetPixelSize configuration (need for proper Font sizes)
  DWORD _flags;       //data for GetSelectionFlags()
  friend class CMainFrmToolbar;
  friend class DefaultItems;
  UndoRedo undo;
  bool undosaved;

public:
  bool _pageClip;
  bool _pageColor;
  COLORREF _pageColorVal;
  UINT _pageImageX;
  UINT _pageImageY;
  int _curlink;
  SDLinkInfo defLinkFlags;
  COLORREF defLinkColor;
  COLORREF defLinkColorToSelected, defLinkColorFromSelected;  // different colors to highlight links connected with selected items
  bool defLinkUseCustom;
  float _mouseHotSpotX, _mouseHotSpotY;
  
public:
  void ChangeShape(ShapeType shape);
  //! CGraphCtrl::InsertItem overlay
  int InsertItem(DWORD flags, DWORD color, const char *text, float width, float height, void *data=NULL);
  void NewItem();
  virtual void DeleteItem(int id);
  virtual void DeleteLink(int &enm);
  //ItemInfo maintenance
  virtual ItemInfo *CreateItemInfo();   //like virtual constructor
  virtual ItemInfo *GetDefItemInfo();   //get default item info
  virtual ItemInfo *GetItemExtraData(int itemid);

  SDLinkInfo * GetLinkExtraData(int itemid);
  virtual void ResetContent();
  virtual void OnLinkItems(int beginitem, int enditem);
  virtual void OnChangeLink(int lnk, int beginitem, int enditem, int newitem, void *data);
  virtual void OnUnlinkItems(int lnk, int beginitem, int enditem, void *data);
  virtual void OnTouchLink(int lnk, int beginitem, int enditem, void *data) 
  {_curlink=lnk;}
  virtual bool OnCustomDraw(int item, SGraphItem &itm, CDC &dc, CRect &rc);
  virtual bool OnCustomDrawLink(int lnk, int beginitem, int enditem, int order, void *data, CDC &dc, CRect rc) ;
  virtual void OnEndSelection();
  virtual void OnResizeItem(int item, int corner, float xs, float ys) {}
  virtual void OnMoveItem(int item, float xs, float ys) {Update();UpdateWindow();}
  virtual void OnEndMoveResize(int corner);
  virtual void BeforeMoveResize() { }
  virtual void BeforeResizeItem(int corner);
  virtual void BeforeMoveItem() { SaveUndo(); }
  virtual void OnLinkNewItem(int beginitem, float x, float y);
  virtual bool CalcLinkRect(int from, int to, int order, CRect &rc);
  bool TestMouseOffset(int cnt, int *mapId, float curmousex, float curmousey);
  void DrawExport(CDC &dc, const CRect &cliprc)
  {_exportdraw=true;Draw(dc,cliprc);_exportdraw=false;}
  void Draw(CDC &dc, const CRect &cliprc);
protected:
  static void DrawSingleLine(CDC &dc,const char *line,CRect &rc, int align, int curline, int totallines, int angle);
  static void DrawMultipleLines(CDC &dc, const char *text, CRect &rc, int align, int angle);
  //{{AFX_MSG(CGraphCtrlExt)
  afx_msg void OnKillFocus(CWnd* pNewWnd);
  afx_msg void OnLButtonDown( UINT nFlags, CPoint point );
  afx_msg void OnLButtonUp( UINT nFlags, CPoint point );
  afx_msg void OnMouseMove( UINT nFlags, CPoint point );
  afx_msg HBRUSH OnCtlColor( CDC* pDC, CWnd* pWnd, UINT nCtlColor );
public:  
  afx_msg void OnMenuUnlink();
  afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
  //}}AFX_MSG
  DECLARE_MESSAGE_MAP()

public:
  void SetArrowType(int lnk, SDLinkInfo *flags);
  int GetNextItem(int current, bool prev);
  void SaveEditedText();
  void BeginEditText();
  bool IsEditing() {return wTextEdit.GetSafeHwnd()!=NULL;}
  void ChangeText(const CString &text);
  SFloatRect GetFirstSelectItemRect();
  void ChangeTextAlign(int align);
  void ChangeTextColor(DWORD color);
  void ChangeFontFaceSize(const char *facename=NULL, int height=-1,int width=-1, int angle=-1);
  void OrderSelected(int order);
  int OrderItem(int item, int order);
  void SelectGroup();
  void GroupSelect(bool group);
  bool IsGroupSelect() {return _grpselected;}
  void DuplicateSelected();
  QOStrStream *GetDataForClipboard();
  void PasteFromClipboard(QIStrStream &in);
  virtual void AdjustAfterDuplication(ItemInfo *newinfo) {}
  void DeleteSelected();
  int Mode(int mode);
  void SetTextContentAndStyle(CString text, LOGFONT &lg, COLORREF color, int align);
  void ChangeLineColor(COLORREF color);
  COLORREF GetRefColor();
  void ChangeBkColor(COLORREF color);
  ItemInfo * GetRefItem();
  void ChangeLineWidth(int width, int style);
  bool IsNormalOrDesignMode() {return (mode==e_normal || mode==e_design);}

//  bool IsVariablesShown() {return _showVariables;}
//  void ShowVariables(bool show) {_showVariables=show;Update();}

  CGraphCtrlExt();
  virtual ~CGraphCtrlExt();

//  void PrepareToOptimize(CDC * optimizeDC);
  void PrepareToDrawingCell(CDC &dc, ItemInfo *info, COLORREF bgcolor);
  void UndoLastPrepareToDrawingCell(CDC &dc);
  CRect GetClipRect(void);
  void SetLinkColor(int link, COLORREF color);

  void SetLinkToLastColor(int link);

  float GetGrid() 
  {return _grid;}
  bool IsGridVisible() 
  {return _gridshow;}
  void SetGrid(float grid,bool show) 
  {
    _grid=grid;
    _gridshow=show;
  }
  void AlignToGrid(SFloatRect & rc);
  void AlignKneeToGrid(SFloatRect & rc);
  bool GetShouldBeAlignedToGrid(int item);
  void ChangeTextFontChanges(LOGFONT & srcfont, LOGFONT & trgfont, COLORREF srccolor , COLORREF trgcolor, int trgalign);
  void GetPixelSize(float & x, float & y);
  void SetPixelSize(int newPixel) {_pixelSize=newPixel;}  //for GetPixelSize configuration
 
  BOOL PreTranslateMessage(MSG* pMsg);

  DWORD GetSelectionFlags() {return _flags;}
  void SetFlagSelection(DWORD invert, DWORD zero);

  //Special normal selection drawing
  virtual void DrawNormalSelection(CDC &dc, int x, int y);

  //Serialization
  LSError Serialize(ParamArchive &ar);
  bool SerializeItem(int ix, SGraphItem &data, ParamArchive &asect);
  virtual bool SerializeExtra(ParamArchive &ar) {return true;};
  void SerializeItemBin(SGraphItem &data, SerializeBinStream &f);
  bool IsDirty() { return _dirty; }
  void SetDirty(bool val);
  bool Save(const char * filename);
  bool Save(QOStream &out);
  bool Load(const char * filename);
  bool Load(QIStream &in);
  void SaveUndo(void);
  void SelectAndMoveToItem(int item);
};

#endif
