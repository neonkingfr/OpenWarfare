// GraphCtrlExample.cpp: implementation of the CGraphCtrlExample class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "GraphView.h"
#include "GraphCtrlExample.h"
#include <math.h>

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CGraphCtrlExample::CGraphCtrlExample()
{
  bmp.LoadBitmap(IDB_COMPUTER);
  bmpmask.LoadBitmap(IDB_COMPUTERMASK);
}

CGraphCtrlExample::~CGraphCtrlExample()
{

}

void CGraphCtrlExample::OnLinkItems(int beginitem, int enditem)
{
  CString q;
  q.Format(IDS_QCREATELINK,beginitem,enditem);
  if (AfxMessageBox(q,MB_YESNO|MB_ICONQUESTION)==IDYES)
  {
    InsertLink(beginitem,enditem,RGB(255,0,0));
    OrderLinks();
  }
}

void CGraphCtrlExample::OnChangeLink(int lnk, int beginitem, int enditem, int newitem, void *data)
{
  SGraphLink &lk=GetLink(lnk);
  CString q;
  q.Format(IDS_QCHANGELINK,lnk,beginitem,enditem,beginitem,newitem);
  if (AfxMessageBox(q,MB_YESNO|MB_ICONQUESTION)==IDYES)
  {
    lk.toitem=newitem;
    OrderLinks();
  }
}

void CGraphCtrlExample::OnUnlinkItems(int lnk, int beginitem, int enditem, void *data)
{  
  CString q;
  q.Format(IDS_QDELETELINK,lnk,beginitem,enditem);
  if (AfxMessageBox(q,MB_YESNO|MB_ICONQUESTION)==IDYES)
  {
    DeleteLink(lnk);	
    OrderLinks();
  }
}

void CGraphCtrlExample::OnTouchLink(int lnk, int beginitem, int enditem, void *data)
{
  if (lnk==-1)
    Beep(200,10);
  else
    Beep(100,10);
}

bool CGraphCtrlExample::OnCustomDraw(int item, SGraphItem &itm, CDC &dc, CRect &rc)
{
  return CGraphCtrlExt::OnCustomDraw(item, itm, dc, rc);  //trial to use CGraphCtrlExt::OnCustomDraw
  CRect nrc=rc;
  nrc.right=rc.left+rc.bottom-rc.top;
  nrc.left+=2;
  nrc.right-=2;
  nrc.top+=2;
  nrc.bottom-=2;
  CDC bdc;
  BITMAP binfo;
  bmp.GetBitmap(&binfo);
  bdc.CreateCompatibleDC(&dc);
  //  dc.SetStretchBltMode(HALFTONE);
  //  bdc.SetStretchBltMode(HALFTONE);
  CBitmap *old=bdc.SelectObject(&bmpmask);
  dc.StretchBlt(nrc.left,nrc.top,nrc.Size().cx,nrc.Size().cy,&bdc,0,0,binfo.bmWidth,binfo.bmHeight,SRCAND);
  bdc.SelectObject(&bmp);
  dc.StretchBlt(nrc.left,nrc.top,nrc.Size().cx,nrc.Size().cy,&bdc,0,0,binfo.bmWidth,binfo.bmHeight,SRCPAINT);
  bdc.SelectObject(old);
  rc.left=nrc.right+5;
  return false;
}


bool CGraphCtrlExample::OnCustomDrawLink(int lnk, int beginitem, int enditem, int order, void *data, CDC &dc, CRect rc)
{
  CFont fnt;
  CSize sz=rc.Size();
  float dg=(float)atan2(float(sz.cy),float(sz.cx))*180.0f/3.141592f;
  if (beginitem==enditem) dg=0;
  float q=(float)sqrt(float(sz.cx)*sz.cx+sz.cy*sz.cy);
  if (dg>90.0) dg-=180.0f;
  if (dg<-90.0) dg+=180.0f;
  fnt.CreateFont(15,0,-(int)(dg*10),0,0,0,0,0,EASTEUROPE_CHARSET,0,0,0,0,"Arial");
  dc.SetTextAlign(TA_BOTTOM|TA_CENTER);
  dc.SetBkMode(TRANSPARENT);
  CPoint cnt=rc.CenterPoint();
  CFont *old=dc.SelectObject(&fnt);
  CString text("��len� �esky?");
  CSize p=dc.GetTextExtent(text);
  while (p.cx>(int)q) 
  {
    text.Delete(text.GetLength()-1);
    p=dc.GetTextExtent(text);
  }
  dc.TextOut(cnt.x,cnt.y,text);
  dc.SelectObject(old);
  return false;
}


void CGraphCtrlExample::OnEndSelection()
{
  DWORD q=0;
  for (int i=-1;(i=EnumItems(i,true))!=-1;)	q|=GetItemFlags(i);
  flags=q;  
}

void CGraphCtrlExample::SetFlagSelection(DWORD invert, DWORD zero)
{
  for (int i=-1;(i=EnumItems(i,true))!=-1;)	
    SetItemFlags(i,invert,zero);
  OnEndSelection();
}
