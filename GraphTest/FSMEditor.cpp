#include "StdAfx.h"
#include "FSMEditor.h"

#define DEFWIDTH 20
#define DEFHEIGHT 10
#define DEFITEMFLAGS SGRI_CANMOVE|SGRI_CANRESIZE|SGRI_CANSELFLINKED|SGRI_CANBELINKED|SGRI_CANMAKELINKS|SGRI_CUSTOMDRAW

#pragma warning( push )
#pragma warning( disable : 4355 )
FSMEditor::FSMEditor(CString &file) : CGraphCtrlExt(), defItems(*this), fileName(file)
{
  _debugState = -1;
  _oldColor = 0;
}
#pragma warning( pop )

//RepairNewLines is used only because of possible bug in ParamFileUsePreprocC...
//very slow solution
CString RepairNewLines(RString &rtxt)
{
  CString text = rtxt.Data();
  text.Replace("\r","");
  text.Replace("\n","\r\n");
  return text;
}

LSError FSMStateValue::Serialize(ParamArchive &ar)
{
  ar.Serialize("Id", _value.id, 1, -1);
  RString initCode = _value.initCode;
  ar.Serialize("InitCode", initCode, 1, "");
  _value.initCode = RepairNewLines(initCode);
  RString preCondition = _value.preCondition;
  ar.Serialize("PreCondition", preCondition, 1, "");
  _value.preCondition = RepairNewLines(preCondition);
  ar.Serialize("StateTab", _value.stateTab, 1, 0);
  return LSOK;
}
void FSMStateValue::SerializeBin(SerializeBinStream &f)
{
  f.TransferBinary(&(_value.id), sizeof(_value.id));
  RString initCode = _value.initCode; f << initCode; _value.initCode = initCode;
  RString preCondition = _value.preCondition;  f << preCondition; _value.preCondition = preCondition;
  f.TransferBinary(&(_value.stateTab), sizeof(_value.stateTab));
}

LSError FSMConditionValue::Serialize(ParamArchive &ar)
{
  ar.Serialize("Id", _value.id, 1, -1);
  ar.Serialize("Priority", _value.priority, 1, 0.0f);
  RString condition=_value.condition;
  ar.Serialize("Condition", condition, 1, "");
  _value.condition=RepairNewLines(condition);
  RString action=_value.action;
  ar.Serialize("Action", action, 1, "");
  _value.action=RepairNewLines(action);
  RString preCondition=_value.preCondition;
  ar.Serialize("PreCondition", preCondition, 1, "");
  _value.preCondition=RepairNewLines(preCondition);
  ar.Serialize("ConditionTab", _value.conditionTab, 1, 0);
  return LSOK;
}
void FSMConditionValue::SerializeBin(SerializeBinStream &f)
{
  f.TransferBinary(&(_value.id),sizeof(_value.id));
  f.TransferBinary(&(_value.priority), sizeof(_value.priority));
  RString condition=_value.condition; f<<condition; _value.condition=condition;
  RString action=_value.action; f<<action; _value.action=action;
  RString preCondition=_value.preCondition; f<<preCondition; _value.preCondition=preCondition;
  f.TransferBinary(&(_value.conditionTab), sizeof(_value.conditionTab));
}

//ItemInfo maintenance
FSMItemInfo::FSMItemInfo() : ItemInfo()
{
  lStyle=PS_DASH;
  fsmData=NULL;
}
FSMItemInfo::~FSMItemInfo()
{
  delete fsmData;
  fsmData=NULL;
}

#include "MainFrm.h"
void FSMEditor::ResetContent()
{
  //reset nextID
  nextID=1;
  //reset FSMName and compileConfig
  ((CMainFrame *)AfxGetMainWnd())->TextEdit.ReleaseTouch();
  fsmName = compileConfigFileName = "";
  //reset the attributes
  attributes.compileConfigAttr.Clear();
  attributes.attrIx=0;
  CGraphCtrlExt::ResetContent();
}

static FSMItemInfo FSMDefItemInfo;
FSMItemInfo *FSMEditor::CreateItemInfo()
{
  return new FSMItemInfo;
}
ItemInfo *FSMEditor::GetDefItemInfo()
{
  return &FSMDefItemInfo;
}
ItemInfo *FSMEditor::GetDefItemInfoStatic()
{
  return &FSMDefItemInfo;
}
FSMItemInfo *FSMEditor::GetItemExtraData(int itemid)
{
  return (FSMItemInfo *)CGraphCtrlExt::GetItemExtraData(itemid);
}
LSError FSMItemInfo::Serialize(ParamArchive &ar)
{
  LSError err = ItemInfo::Serialize(ar);
  if (ar.IsLoading()) 
  {
    DefaultItemType type;  //DF_UNDEF == not set
    ar.Serialize("ItemType", (int &) type, 1, DF_UNDEF);
    if (type!=-1)
    {
      fsmData = CreateByType(type);
      if (fsmData)
      {
        ar.Serialize("ItemInfo", *fsmData, 1);
      }
//      fsmData->Serialize(ar);
    }
  }
  else if (fsmData)
  {
    ar.Serialize("ItemType", (int &)fsmData->type, 1, DF_UNDEF);
    ar.Serialize("ItemInfo", *fsmData, 1);
  }
  return err;
}

void FSMItemInfo::SerializeBin(SerializeBinStream &f)
{
  ItemInfo::SerializeBin(f);
  if (f.IsLoading()) 
  {
    DefaultItemType type;  //DF_UNDEF == not set
    f.TransferBinary(&type, sizeof(type));
    if (type!=-1)
    {
      fsmData = CreateByType(type);
      if (fsmData)
      {
        fsmData->SerializeBin(f);
      }
    }
  }
  else if (fsmData)
  {
    f.TransferBinary(&(fsmData->type), sizeof(fsmData->type));
    fsmData->SerializeBin(f);
  }
  else //some unspecified item
  {
    DefaultItemType type=DF_UNDEF;
    f.TransferBinary(&type, sizeof(type));
  }
}

bool FSMItemInfo::IsCondition(DefaultItemType type)
{
  return type==DF_CONDITION||type==DF_USER_CONDITION||type==DF_TRUE_CONDITION||type==DF_USER_INPUT_CONDITION;
}

bool FSMItemInfo::IsState(DefaultItemType type)
{
  return type==DF_START_ITEM||type==DF_END_ITEM||type==DF_ITEM||type==DF_USER_ITEM;
}

void FSMItemInfo::CopyFsmData(FSMItemValue *data)
{
  if (data->GetType()==FSMStateType)
  {
    delete fsmData;
    fsmData = new FSMStateValue(*(data->GetFSMState()));
  }
  else if (data->GetType()==FSMConditionType)
  {
    delete fsmData;
    fsmData = new FSMConditionValue(*(data->GetFSMCondition()));
  }
}

FSMItemValue * FSMItemInfo::CreateByType(DefaultItemType type)
{
  FSMItemValue *retval = NULL;
  if (IsCondition(type))
    retval = new FSMConditionValue;
  else //if (IsState(type))
    retval = new FSMStateValue;
  if (retval)
  {
    retval->type=type;
  }
  return retval;
}

FSMItemInfo *FSMItemInfo::Clone()
{
  FSMItemInfo *newinfo = new FSMItemInfo;
  (*newinfo).ItemInfo::operator=(*this);
  if (fsmData!=NULL) newinfo->fsmData=fsmData->Clone();
  else newinfo->fsmData=NULL;
  return newinfo;
}

bool FSMItemInfo::GetShouldBeAlignedToGrid()
{
  if (fsmData && fsmData->type==DF_KNEE) return false;
  return ItemInfo::GetShouldBeAlignedToGrid();
} 

bool FSMEditor::UseLinkOrder(int item)
{
  FSMItemInfo *info = GetItemExtraData(item);
  if (info)
  {
    return info->GetShouldBeAlignedToGrid();
  }
  return CGraphCtrl::UseLinkOrder(item);
}

//OnLinkNewItem with ctrl+mouse drag functionality (making knee item)
void FSMEditor::OnLinkNewItem(int beginitem, float x, float y)
{
  SaveUndo();
  SGraphItem item;
  GetItem(beginitem,&item);
  float xs2=(item.rect.right-item.rect.left)*0.5f;
  float ys2=(item.rect.bottom-item.rect.top)*0.5f;
  SFloatRect newrc(x-xs2,y-ys2,x+xs2,y+ys2);

  int itemp=InsertItem(DEFITEMFLAGS|SGRI_NOGUESSPOSITION,item.color,"",DEFWIDTH,DEFHEIGHT,NULL);
  SetItemRect(itemp,newrc);

  FSMItemValue *itVal = ((FSMItemInfo*)item.data)->fsmData;
  DefaultItemType targetType = DF_UNDEF;
  if (GetKeyState(VK_LCONTROL)&0x8000) targetType=DF_KNEE; //ctrl+drag yields to knee items making
  else if (itVal)
    if (FSMItemInfo::IsCondition(itVal->type)) targetType=DF_ITEM;
    else if (itVal->type==DF_KNEE) targetType=DF_KNEE;
    else targetType=DF_CONDITION;
  SGraphItem defitem;
  if (GetItem(itemp, &defitem))
  {
    if (targetType==DF_UNDEF) 
    {
      delete (ItemInfo*)defitem.data;
      defitem.data = ((ItemInfo*)item.data)->Clone();
      ((ItemInfo*)defitem.data)->text="";
    }
    else defItems.ChangeToDefaultItem(targetType, defitem);
    if (GetKeyState(VK_LCONTROL)&0x8000) //ctrl+drag yields to knee items making
    {
      float dx=(defitem.rect.right-defitem.rect.left)*0.5f;
      float dy=(defitem.rect.bottom-defitem.rect.top)*0.5f;
      defitem.rect=SFloatRect(x-dx,y-dy,x+dx,y+dx);
    }
    SetItem(itemp, &defitem);
    if (GetShouldBeAlignedToGrid(itemp))
    {
      SFloatRect rc=GetItemRect(itemp);
      AlignToGrid(rc);
      SetItemRect(itemp,rc);
    }
    else
    {
      SFloatRect rc=GetItemRect(itemp);
      AlignKneeToGrid(rc);
      SetItemRect(itemp,rc);
    }
  }
  InsertLink(beginitem,itemp,defLinkColor,defLinkUseCustom?SGRI_CUSTOMDRAW:0,(void *)new SDLinkInfo(defLinkFlags));
  OrderLinks();
  OnEndSelection();
}

void FSMEditor::OnEndSelection()
{
  RefreshDataDlg();
  CGraphCtrlExt::OnEndSelection();
}

void FSMEditor::RefreshDataDlg(bool refreshOnly)
{
  int p=EnumItems(-1,true);
  if (p!=-1 && EnumItems(p,true)==-1)
  {
    SGraphItem item;
    GetItem(p,&item);
    if (item.data) 
    {
      if (((FSMItemInfo *)item.data)->fsmData)
      {
        ((CMainFrame *)AfxGetMainWnd())->TextEdit.ActivateDlg(((FSMItemInfo *)item.data)->fsmData, refreshOnly);
        return;
      }
    }
  }
  ((CMainFrame *)AfxGetMainWnd())->TextEdit.ActivateDlg(NULL);
}

FSMItemInfo *FSMEditor::GetItemExtraDataById(int idWanted)
{
  for (int id=-1;(id = EnumItems(id)) != -1;) 
  {
    FSMItemInfo *info = GetItemExtraData(id);
    if (info && info->fsmData)
    {
      int *pid = info->fsmData->GetID();
      if (pid && *pid==idWanted) return info;
    }
  }
  return NULL;
}

void FSMEditor::DeleteItem(int id)
{
  ((CMainFrame *)AfxGetMainWnd())->TextEdit.ReleaseTouch();
  CGraphCtrlExt::DeleteItem(id);
}

#include "el/ParamFile/paramFile.hpp"
bool FSMEditor::SerializeExtra(ParamArchive &ar)
{ 
  ar.Serialize("NextID", nextID, 1,1);
  RString fsmName2=fsmName;
  ar.Serialize("FSMName", fsmName2, 1, "");
  fsmName=fsmName2;
  RString compileConfigFileName2=compileConfigFileName;
  ar.Serialize("CompileConfig", compileConfigFileName2, 1, "");
  compileConfigFileName=compileConfigFileName2;
  ar.Serialize("Attributes", attributes.compileConfigAttr, 1);
  if (ar.IsLoading())
    for (int i=0, n=attributes.compileConfigAttr.Size(); i<n; ++i) 
    {
      attributes.compileConfigAttr[i].name.Lower();
      attributes.compileConfigAttr[i].value = RepairNewLines(attributes.compileConfigAttr[i].value).GetBuffer();
    }
  return true; 
};

void FSMEditor::AdjustAfterDuplication(ItemInfo *newinfo)
{
  if (((FSMItemInfo*)newinfo)->fsmData)
    *(((FSMItemInfo*)newinfo)->fsmData->GetID())=GetNewID();
}

void FSMAttributes::SetActiveText(RString text)
{
  compileConfigAttr[attrIx].value = text;
}

//if attr with name doesn't exist, get empty string
RString FSMAttributes::GetValue(RString name)
{
  name.Lower();
  for (int i=0, n=compileConfigAttr.Size(); i<n; ++i)
  {
    if (compileConfigAttr[i].name==name) return compileConfigAttr[i].value;
  }
  return "";
}

//if attr with name doesn't exist, create it
int FSMAttributes::SetValue(RString name, RString value)
{
  name.Lower();
  for (int i=0, n=compileConfigAttr.Size(); i<n; ++i)
  {
    if (compileConfigAttr[i].name==name) { compileConfigAttr[i].value=value; return i; }
  }
  return compileConfigAttr.Add(FSMNamedAttribute(name, value));
}

void FSMEditor::AddStartState()
{
  CRect rect;
  GetWindowRect(&rect);
  int sizx = rect.right-rect.left, sizy = rect.bottom - rect.top;
  CRect dragrc(sizx/2, sizy/2, sizx/2+10, sizy/2+10);
  SFloatRect rc=MapRectFromWindow(dragrc);
  ItemInfo *newinfo = GetDefItemInfo()->Clone();
  int item=InsertItem(DEFITEMFLAGS,RGB(255,255,255),"",DEFWIDTH,DEFHEIGHT,(void *)newinfo);
  AlignToGrid(rc);
  SetItemRect(item,rc);
  SGraphItem grItem; GetItem(item, &grItem);
  ((ItemInfo *)grItem.data)->text="";
  defItems.ChangeToDefaultItem(DF_START_ITEM, grItem);
  { 
    float sizx = grItem.rect.right-grItem.rect.left, sizy = grItem.rect.bottom - grItem.rect.top;
    grItem.rect.right -= sizx/2; grItem.rect.left -= sizx/2; grItem.rect.top -= sizy/2; grItem.rect.bottom -= sizy/2;
  }
  SetItem(item, &grItem);
  AfxGetMainWnd()->PostMessage(MSG_NEWITEM,item);
  SetFocus();
  Update();
  UpdateScrollbars();
}

int FSMEditor::FindItemByStateIx(int stateIx)
{
  if (stateIx < 0)
    return (-stateIx-1);
  // find the item by the state index
  int index = -1;
  for (int i=0; i<NItems(); i++)
  {
    FSMItemInfo *info = reinterpret_cast<FSMItemInfo *>(GetItemData(i));
    if (
      info && info->fsmData && info->fsmData->GetFSMState() &&
      FSMItemInfo::IsState(info->fsmData->type)
      )
    {
      if (stateIx == 0)
      {
        index = i;
        break;
      }
      stateIx--;
    }
  }
  return index;
}

void FSMEditor::OnStateChanged(WPARAM wParam, LPARAM lParam)
{
  if (_debugState >= 0 && _debugState < NItems())
  {
    // restore the previous state of item
    SGraphItem item;
    GetItem(_debugState, &item);
    item.color = _oldColor;
    SetItem(_debugState, &item);
    _debugState = -1;
    _oldColor = 0;
  }

  int index = FindItemByStateIx(wParam);

  if (index >= 0 && index < NItems())
  {
    SGraphItem item;
    GetItem(index, &item);
    _debugState = index;
    _oldColor = item.color;
    item.color = RGB(0, 192, 0);
    SetItem(index, &item);
  }

  Update();

  (void)lParam;
}
