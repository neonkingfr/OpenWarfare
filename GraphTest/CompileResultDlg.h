#pragma once


// CompileResultDlg dialog

#define MSG_FOCUS_ERROR_ITEM (WM_APP+779)

class CompileResultDlg : public CDialog
{
	DECLARE_DYNAMIC(CompileResultDlg)

public:
	CompileResultDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CompileResultDlg();

// Dialog Data
	enum { IDD = IDD_COMPILE_RESULT };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
  CString editText;
  afx_msg void OnSize(UINT nType, int cx, int cy);
  afx_msg void OnClose();
  virtual BOOL PreTranslateMessage(MSG* pMsg);
};
