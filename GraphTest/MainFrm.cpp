// MainFrm.cpp : implementation of the CMainFrame class
//

#include "stdafx.h"
#include "GraphView.h"
#include "MainFrm.h"
#include "InsertItemDlg.h"
#include "res\DlgLinkProperties.h"
#include "DlgPageSetup.h"
#include ".\mainfrm.h"

extern CGraphViewApp theApp;

// BARS for change toolbars state (saved to the registry)
#define BARS "BARS"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMainFrame

IMPLEMENT_DYNAMIC(CMainFrame, CFrameWnd)

BEGIN_MESSAGE_MAP(CMainFrame, CFrameWnd)
  //{{AFX_MSG_MAP(CMainFrame)
  ON_WM_SETFOCUS()
  ON_WM_CREATE()
  ON_WM_CLOSE()
  //ON_COMMAND(ID_EDIT_INSERTITEM, OnEditInsertitem)
  ON_COMMAND(ID_EDIT_DELETEITEMS, OnEditDeleteitems)
  ON_UPDATE_COMMAND_UI(ID_EDIT_DELETEITEMS, OnUpdateEditDeleteitems)
  ON_COMMAND(ID_PROPERTIES_EDITITEM, OnPropertiesEdititem)
  ON_UPDATE_COMMAND_UI(ID_PROPERTIES_EDITITEM, OnUpdatePropertiesEdititem)
  ON_WM_CONTEXTMENU()
  ON_UPDATE_COMMAND_UI(ID_PROPERTIES_CANBELINKED, OnUpdatePropertiesCanbelinked)
  ON_UPDATE_COMMAND_UI(ID_PROPERTIES_CANMAKELINKS, OnUpdatePropertiesCanmakelinks)
  ON_UPDATE_COMMAND_UI(ID_PROPERTIES_CANMOVE, OnUpdatePropertiesCanmove)
  ON_UPDATE_COMMAND_UI(ID_PROPERTIES_CANSELFLINKED, OnUpdatePropertiesCanselflinked)
  ON_UPDATE_COMMAND_UI(ID_PROPERTIES_CANSIZE, OnUpdatePropertiesCansize)
  ON_UPDATE_COMMAND_UI(ID_PROPERTIES_CUSTOMDRAW, OnUpdatePropertiesCustomdraw)
  ON_COMMAND(ID_PROPERTIES_CUSTOMDRAW, OnPropertiesCustomdraw)
  ON_COMMAND(ID_PROPERTIES_CANSIZE, OnPropertiesCansize)
  ON_COMMAND(ID_PROPERTIES_CANSELFLINKED, OnPropertiesCanselflinked)
  ON_COMMAND(ID_PROPERTIES_CANMOVE, OnPropertiesCanmove)
  ON_COMMAND(ID_PROPERTIES_CANMAKELINKS, OnPropertiesCanmakelinks)
  ON_COMMAND(ID_PROPERTIES_CANBELINKED, OnPropertiesCanbelinked)
  ON_COMMAND(ID_LINK_NORMAL, OnLinkNormal)
  ON_COMMAND(ID_LINK_DESIGN, OnLinkDesign)
  ON_COMMAND(ID_LINK_ZOOM, OnLinkZoom)
  ON_UPDATE_COMMAND_UI(ID_LINK_NORMAL, OnUpdateLinkNormal)
  ON_UPDATE_COMMAND_UI(ID_LINK_ZOOM, OnUpdateLinkZoom)
  ON_UPDATE_COMMAND_UI(ID_LINK_DESIGN, OnUpdateLinkDesign)
  ON_WM_CLOSE()
  // CGraphCtrlExt messages handling
  ON_MESSAGE(MSG_NOTIFYENDSEL ,OnNotifyEndSel)
  // font bar messages handling
  ON_MESSAGE(DBFM_BOOLSTYLECHANGED,OnBoolStyleChanged)
  ON_MESSAGE(DBFM_FONTSTYLECHANGED,OnFontChanged)
  ON_MESSAGE(DBFM_FONTCOLORCHANGED,OnFontColorChanged)
  ON_MESSAGE(DBFM_TEXTALIGNCHANGED,OnTextAlignChanged)
  // view menu commands
  ON_COMMAND_EX(ID_VIEW_DESIGNTOOLBAR, OnBarCheck)
  ON_COMMAND_EX(ID_VIEW_TEXTPROPERTIES, OnBarCheck)
  ON_COMMAND_EX(ID_VIEW_DEBUGFSMLOG, OnDebugFSMLogCheck)
  ON_UPDATE_COMMAND_UI(ID_VIEW_DESIGNTOOLBAR, OnUpdateControlBarMenu)
  ON_UPDATE_COMMAND_UI(ID_VIEW_TEXTPROPERTIES, OnUpdateControlBarMenu)
  // popup link menu commands
  ON_COMMAND(ID_LINK_USELASTSETTINGS, OnLinkUselastsettings)
  ON_COMMAND(ID_LINK_CHANGECOLOR, OnLinkChangecolor)
  ON_COMMAND(ID_LINK_ARROWTYPE, OnLinkArrowtype)
  // Design menu 
  ON_COMMAND(ID_DIAGRAM_INSERTNEW, OnDesignInsertnew)
  //inactive items
  ON_COMMAND(ID_DIAGRAM_INACTIVE, OnDiagramInactive)
  ON_UPDATE_COMMAND_UI(ID_DIAGRAM_INACTIVE, OnUpdateDiagramInactive)
  //messages from graphCtrlExt
  ON_MESSAGE(MSG_WNDSERIALIZE,OnWndSerialize)
  ON_MESSAGE(MSG_DIRTINESS_CHANGED,OnDirtinessChanged)
  ON_MESSAGE(MSG_ATTR_EDIT, OnMsgAttrEdit)
  ON_MESSAGE(MSG_PREPARE_FOR_SAVING,OnPrepareForSaving)
  //message from CompileResultDlg or DebugFSMLogDlg
  ON_MESSAGE(MSG_FOCUS_ERROR_ITEM,OnFocusItem)
  //}}AFX_MSG_MAP
  ON_COMMAND(ID_DESIGN_STATE, OnDesignState)
  ON_COMMAND(ID_DESIGN_USERSTATE, OnDesignUserState)
  ON_COMMAND(ID_DESIGN_CONDITION, OnDesignCondition)
  ON_COMMAND(ID_DESIGN_INPUTCONDITION, OnDesignInputcondition)
  ON_COMMAND(ID_DESIGN_USERCONDITION, OnDesignUsercondition)
  ON_COMMAND(ID_DESIGN_STARTSTATE, OnDesignStartstate)
  ON_COMMAND(ID_DESIGN_ENDSTATE, OnDesignEndstate)
  ON_COMMAND(ID_DESIGN_KNEE, OnDesignKnee)
  ON_COMMAND(ID_SETASDEFAULT_STATE, OnSetasdefaultState)
  ON_COMMAND(ID_SETASDEFAULT_USERSTATE, OnSetasdefaultUserstate)
  ON_COMMAND(ID_SETASDEFAULT_CONDITION, OnSetasdefaultCondition)
  ON_COMMAND(ID_SETASDEFAULT_INPUTCONDITION, OnSetasdefaultInputcondition)
  ON_COMMAND(ID_SETASDEFAULT_USERCONDITION, OnSetasdefaultUsercondition)
  ON_COMMAND(ID_SETASDEFAULT_STARTSTATE, OnSetasdefaultStartstate)
  ON_COMMAND(ID_SETASDEFAULT_ENDSTATE, OnSetasdefaultEndstate)
  ON_COMMAND(ID_SETASDEFAULT_KNEE, OnSetasdefaultKnee)
  ON_COMMAND(ID_DEFAULTITEMS_SAVETOFILE, OnDefaultsSaveas)
  ON_COMMAND(ID_DEFAULTITEMS_LOADFROMFILE, OnDefaultsLoad)
  // Dialog PageSetup
  ON_COMMAND(ID_FILE_PAGESETUP, OnFilePagesetup)
  ON_COMMAND(ID_DESIGN_TRUECONDITION, OnDesignTruecondition)
  ON_COMMAND(ID_SETASDEFAULT_TRUECONDITION, OnSetasdefaultTruecondition)
  ON_COMMAND(ID_CHANGEACTIVEWND, OnChangeactivewnd)
  ON_COMMAND(ID_FILE_COMPILE, OnFileCompile)
  ON_MESSAGE(MSG_STARTUP_UPDATE, OnInitialUpdate)
  // fsmattributes menu commands
  ON_COMMAND(ID_FSMATTRIBUTES_FSMNAME, OnFsmattributesFsmname)
  ON_COMMAND(ID_FSMATTRIBUTES_COMPILECONFIG, OnFsmattributesCompileconfig)
  ON_COMMAND(ID_FSMATTRIBUTES_ATTRIBUTESFROMCOMPILECONFIG, OnFsmattributesAttributesfromcompileconfig)
  ON_MESSAGE(MSG_STATE_CHANGED, OnStateChanged2)
  // options menu
  ON_COMMAND(ID_OPTIONS_SELECTEDITFONT, OnChangeFont)
  ON_COMMAND(ID_OPTIONS_SPLITVERTICAL, &CMainFrame::OnOptionsSplitvertical)
  ON_UPDATE_COMMAND_UI(ID_OPTIONS_SPLITVERTICAL, &CMainFrame::OnUpdateOptionsSplitvertical)
END_MESSAGE_MAP()

//status bar indicators
static UINT indicators[] =
{
  ID_SEPARATOR,           // status line indicator
  ID_INDICATOR_CAPS,
  ID_INDICATOR_NUM,
  ID_INDICATOR_SCRL
};

/////////////////////////////////////////////////////////////////////////////
// CMainFrame construction/destruction

#include "ExtEditorManager.h"
extern BOOL gVertSplit;
CMainFrame::CMainFrame() : View(curfname), TextEdit(View)
{
  // TODO: add member initialization code here
  // get profile info
  extern CString gExternalEditor;
  extern CString gServiceDDE;
  extern BOOL gUseDDE;
  gExternalEditor = theApp.GetProfileString("Setup","EXTEDITOR", "");
  gUseDDE = (theApp.GetProfileString("Setup","USEDDE", "").Compare("1")==0);
  gVertSplit = (theApp.GetProfileString("Setup","HORZSPLIT", "").Compare("1")!=0);
  gServiceDDE = theApp.GetProfileString("Setup","SERVICEDDE", "");
  // init global ext editor manager
  gExtEdFileManager.Init(&View);
}

CMainFrame::~CMainFrame()
{
  delete wToolBar;
  delete wDesignToolbar;
  delete compResultDlg;
  delete split;
  // write profile info
  extern CString gExternalEditor;
  extern CString gServiceDDE;
  extern BOOL gUseDDE;
  theApp.WriteProfileString("Setup","EXTEDITOR", gExternalEditor.GetString());
  theApp.WriteProfileString("Setup","USEDDE", gUseDDE ? "1" : "0");
  theApp.WriteProfileString("Setup","HORZSPLIT", gVertSplit ? "0" : "1");
  theApp.WriteProfileString("Setup","SERVICEDDE", gServiceDDE.GetString());
}

BOOL CMainFrame::PreCreateWindow(CREATESTRUCT& cs)
{
  if( !CFrameWnd::PreCreateWindow(cs) )
    return FALSE;
  // TODO: Modify the Window class or styles here by modifying
  //  the CREATESTRUCT cs

  cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
  //cs.lpszClass = AfxRegisterWndClass(0);
  //for some unknown reason, proper icon is not loaded (so do it manually)
  cs.lpszClass = AfxRegisterWndClass(0,0,0,::LoadIcon(AfxGetInstanceHandle(), MAKEINTRESOURCE(IDR_MAINFRAME)));
  return TRUE;
}

LRESULT CMainFrame::WindowProc(UINT message, WPARAM wParam, LPARAM lParam)
{
  return CWnd::WindowProc(message, wParam, lParam);
}

BOOL CMainFrame::PreTranslateMessage(MSG* pMsg) 
{
  //LogF("Msg: %i, %i, %i", pMsg->message, pMsg->wParam, pMsg->lParam);
  if (pMsg->hwnd==TextEdit.GetSafeHwnd()) //if Edit window active, translate message
  {
//    if (pMsg->message==WM_KEYDOWN)
//      _asm nop;
     TranslateMessage(pMsg);
    DispatchMessage(pMsg);
    return TRUE;
  }
//  if (pMsg->message==WM_KEYDOWN)
//    _asm nop;
  if (TranslateAccelerator(wToolBar->GetSafeHwnd(), wToolBar->hAccelerator, pMsg)) return TRUE;
  if (TranslateAccelerator(wDesignToolbar->GetSafeHwnd(), wDesignToolbar->hAccelerator, pMsg)) return TRUE;
  return CFrameWnd::PreTranslateMessage(pMsg);
}

/////////////////////////////////////////////////////////////////////////////
// CMainFrame diagnostics

#ifdef _DEBUG
void CMainFrame::AssertValid() const
{
  CFrameWnd::AssertValid();
}

void CMainFrame::Dump(CDumpContext& dc) const
{
  CFrameWnd::Dump(dc);
}

#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CMainFrame message handlers
#include "ExtEditorManager.h"
void CMainFrame::OnSetFocus(CWnd* pOldWnd)
{
  // forward focus to the view window
  gExtEdFileManager.Update();
  View.SetFocus();
}

BOOL CMainFrame::OnCmdMsg(UINT nID, int nCode, void* pExtra, AFX_CMDHANDLERINFO* pHandlerInfo)
{
  // let the toolbars have first crack at the command
  if (wToolBar->OnCmdMsg(nID, nCode, pExtra, pHandlerInfo))
    return TRUE;
  if (wDesignToolbar->OnCmdMsg(nID, nCode, pExtra, pHandlerInfo))
    return TRUE;
  // let the view have first crack at the command
  if (View.OnCmdMsg(nID, nCode, pExtra, pHandlerInfo))
    return TRUE;

  // otherwise, do default handling
  return CFrameWnd::OnCmdMsg(nID, nCode, pExtra, pHandlerInfo);
}

void CMainFrame::OnClose()
{
  int askval = IDNO;
  SendMessage(MSG_PREPARE_FOR_SAVING,0,0);
  if (View.IsDirty()) askval=AfxMessageBox(IDS_ASKONEXIT, MB_YESNOCANCEL|MB_ICONWARNING);
  if (askval==IDYES || askval==IDNO)
  {
    SaveBarState(BARS);
    if (askval==IDYES)
    {
      wToolBar->OnFileSave();
    }
    DestroyWindow();
  }
  else return;  //no action
}

#include "el/Pathname/Pathname.h"
void CMainFrame::UpdateCaption()
{
  if (View.fsmName!="")
  {
    CString text = _basicWindowText + ": " + View.fsmName;
    SetWindowText(text);
  }
  else
  {
    Pathname path(curfname.GetBuffer());
    CString text = _basicWindowText + ": " + path.GetFilename();
    SetWindowText(text);
  }
}


int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
  if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
    return -1;

  GetWindowText(_basicWindowText); //fill it

  extern BOOL gVertSplit;
  if (gVertSplit)
  {
    CSplitWindowVert *splitVert = new CSplitWindowVert();
    splitVert->Create(this,0.75f,AFX_IDW_PANE_FIRST);
    splitVert->EnableResize1(true);
    splitVert->EnableResize2(true);
    split = splitVert;
  }
  else
  {
    CSplitWindowHorz *splitHorz = new CSplitWindowHorz();
    splitHorz->Create(this,0.75f,AFX_IDW_PANE_FIRST);
    splitHorz->EnableResize1(true);
    splitHorz->EnableResize2(true);
    split = splitHorz;
  }

  //create CGraphCtrl instance (View)
  View.Create(NULL,"",WS_VISIBLE|WS_CHILD,CRect(0,0,0,0),split,AFX_IDW_PANE_FIRST,NULL);
  //TextEdit.Create(WS_CHILD |WS_VISIBLE|WS_BORDER|WS_HSCROLL|WS_VSCROLL|ES_AUTOHSCROLL|ES_AUTOVSCROLL|ES_MULTILINE|ES_WANTRETURN|ES_LEFT,CRect(0,0,0,0),&split,100);	
  TextEdit.Create(split);

  //create main toolbar (new, save, load, delete, ..., group, moveback, ...)
  wToolBar = new CMainFrmToolbar(View, curfname);
  if (!wToolBar->CreateEx(this, TBSTYLE_FLAT, WS_CHILD | WS_VISIBLE | CBRS_TOP
    | CBRS_GRIPPER | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC) ||
    !wToolBar->LoadToolBar(MAKEINTRESOURCE(IDR_MAINTLB)))
  {
    TRACE0("Failed to create toolbar\n");
    return -1;      // fail to create
  }
  wToolBar->EnableDocking(CBRS_ALIGN_ANY);
  EnableDocking(CBRS_ALIGN_ANY);
  DockControlBar(wToolBar,AFX_IDW_DOCKBAR_TOP);
  //main toolbar created
  //create design toolbar (shapes, linestyle, modes)
  wDesignToolbar = new CDesignToolbar(View);
  if (!wDesignToolbar->CreateEx(this, TBSTYLE_FLAT, WS_CHILD | WS_VISIBLE | CBRS_TOP
    | CBRS_GRIPPER | CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC,CRect(0,0,0,0),ID_VIEW_DESIGNTOOLBAR) ||
    !wDesignToolbar->LoadToolBar(IDR_DESIGNTLB))
  {
    TRACE0("Failed to create toolbar\n");
    return -1;      // fail to create
  }
  wDesignToolbar->EnableDocking(CBRS_ALIGN_ANY);
  DockControlBar(wDesignToolbar,AFX_IDW_DOCKBAR_LEFT);
  //design toolbar created
  //create font bar
  wFontBar.Create(this,ID_VIEW_TEXTPROPERTIES);
  DockControlBar(&wFontBar,AFX_IDW_DOCKBAR_TOP);  
  //font bar created
  //create status bar
  if (!wStatusBar.Create(this) || !wStatusBar.SetIndicators(indicators, sizeof(indicators)/sizeof(UINT)))
  {
    TRACE0("Failed to create status bar\n");
    return -1;      // fail to create
  }
  //status bar created
  //loading my icon
  /*
  //done with alternate way in PrecreateWindow()
  SetClassLong(*this,GCL_HICON,
    (LONG)LoadImage(AfxGetInstanceHandle(),MAKEINTRESOURCE(IDR_MAINFRAME),IMAGE_ICON,32,32,0));
  SetClassLong(*this,GCL_HICONSM,
    (LONG)LoadImage(AfxGetInstanceHandle(),MAKEINTRESOURCE(IDR_MAINFRAME),IMAGE_ICON,16,16,0));
  */
  //load bar docking from registry
  LoadBarState(BARS);
  //create dialog for compile result
  compResultDlg = new CompileResultDlg();
  compResultDlg->Create(IDD_COMPILE_RESULT);
  compResultDlg->EnableWindow(true);
  compResultDlg->ShowWindow(SW_HIDE);
  //create dialog for DebugFSMLog
  debugFSMLogDlg = new DebugFSMLogDlg();
  debugFSMLogDlg->Create(IDD_DEBUG_FSM_LOG);
  debugFSMLogDlg->EnableWindow(true);
  debugFSMLogDlg->ShowWindow(SW_HIDE);
  //load default configuration
  View.defItems.LoadDefaults();
  View.SetPixelSize(10);
  View.SetDirty(false);
  SetFocus();
  return 0;
}

/*
void CMainFrame::OnEditInsertitem() 
{
  static DWORD defflags=SGRI_NORMAL;
  static float defdimx=10;
  static float defdimy=5;
  static DWORD defcolor=RGB(255,255,128);
  CInsertItemDlg dlg;
  dlg.flags=defflags;
  dlg.m_dimx=defdimx;
  dlg.m_dimy=defdimy;
  dlg.color=defcolor;
  if (dlg.DoModal()==IDOK)
  {
    defdimx=dlg.m_dimx;
    defdimy=dlg.m_dimy;
    defflags=dlg.flags;
    defcolor=dlg.color;
    int i=View.InsertItem(dlg.flags|SGRI_NOGUESSPOSITION|SGRI_FILLBKCOLOR,defcolor,dlg.text,dlg.m_dimx,dlg.m_dimy);
    SFloatRect rc=View.GetPanZoom();
    float xc=(rc.left+rc.right)*0.5f;
    float yc=(rc.bottom+rc.top)*0.5f;
    rc=View.GetItemRect(i);
    rc.left+=xc;
    rc.right+=xc;
    rc.top+=yc;
    rc.bottom+=yc;
    View.SetItemRect(i,rc);	
    View.Update();
  }
}
*/

void CMainFrame::ResetZoom()
{
  SFloatRect rc=View.GetBoundingBox();
  float xs=(rc.right-rc.left)*0.2f;
  float ys=(rc.bottom-rc.top)*0.2f;
  rc.left-=xs;
  rc.top-=ys;
  rc.right+=xs;
  rc.bottom+=ys;
  View.SetPanZoom(rc,true);
}

void CMainFrame::OnEditDeleteitems() 
{
  if (AfxMessageBox(IDS_QDELETE,MB_YESNO|MB_ICONQUESTION)==IDNO) return;
  for (int i=-1;(i=View.EnumItems(i,true))!=-1;) 
    View.DeleteItem(i);
  View.Update();
}

void CMainFrame::OnUpdateEditDeleteitems(CCmdUI* pCmdUI) 
{
  pCmdUI->Enable(View.EnumItems(-1,true)!=-1);	
}

void CMainFrame::OnPropertiesEdititem() 
{
  int i=View.EnumItems(-1,true);
  CInsertItemDlg dlg;
  SGraphItem it;
  View.GetItem(i,&it);
  dlg.color=it.color;
  dlg.text=it.text;
  dlg.flags=it.flags;
  dlg.m_dimx=it.rect.right-it.rect.left;
  dlg.m_dimy=it.rect.bottom-it.rect.top;
  if (dlg.DoModal()==IDOK)
  {
    it.color=dlg.color;
    strncpy(it.text,dlg.text,SGRI_MAXTEXTLEN);
    it.text[SGRI_MAXTEXTLEN-1]=0;
    it.flags=dlg.flags;//|SGRI_FILLBKCOLOR;
    it.rect.right=it.rect.left+dlg.m_dimx;
    it.rect.bottom=it.rect.top+dlg.m_dimy;
    View.SetItem(i,&it);
    View.Update();
  }
  SetFocus();
}

void CMainFrame::OnUpdatePropertiesEdititem(CCmdUI* pCmdUI) 
{
  int i=View.EnumItems(-1,true);
  pCmdUI->Enable(i!=-1 && View.EnumItems(i,true)==-1);	
} 

void CMainFrame::OnContextMenu(CWnd* pWnd, CPoint point) 
{
  static bool zoomwarn=true;
  if (pWnd==&View)
  {
    int l=View.GetFocusedLink();
    if (l==-1 && (GetKeyState(VK_SHIFT) & 0x80))
    {
      CPoint q=point;
      View.ScreenToClient(&q);
      l=View.LinkFromPoint(q);
    }
    CMenu mnu;
    if (l==-1) //not link menu
    {
      CPoint pt=point;
      View.ScreenToClient(&pt);
      float x,y;
      View.MapPointFromWindow(pt,&x,&y);
      int i=View.ItemFromPoint(x,y);
      if (i!=-1) 
      {
        if (!(GetKeyState(VK_CONTROL) & 0x8000)) View.SelectItems(SFloatRect(0,0,0,0),true);
        View.SelectItem(i);
        View.Update();
      }
      mnu.LoadMenu(IDR_MAINFRAME);
      CMenu *popup=mnu.GetSubMenu(3);
      View.SaveEditedText();
      popup->TrackPopupMenu(TPM_RIGHTBUTTON,point.x,point.y,this,NULL);    
    }
    else
    {
      mnu.LoadMenu(IDR_LINKMENU);
      CMenu *popup=mnu.GetSubMenu(0);
      short flg=View.GetLinkFlags(l);
      if (flg & SGRI_FROZEN) popup->CheckMenuItem(ID_LINK_FROZEN,MF_CHECKED);
      if (flg & SGRI_DONTFOCUS) popup->CheckMenuItem(ID_LINK_DONTFOCUS,MF_CHECKED);
      int cmd=popup->TrackPopupMenu(TPM_LEFTALIGN|TPM_RIGHTBUTTON|TPM_NONOTIFY|TPM_RETURNCMD,point.x,point.y,this,NULL);
      switch (cmd)
      {
      case ID_LINK_FROZEN: View.SetLinkFlags(l,SGRI_FROZEN,0);break;
      case ID_LINK_DONTFOCUS: if (~View.GetLinkFlags(l) & SGRI_DONTFOCUS)AfxMessageBox(IDS_DONTFOCUSWARNING,MB_OK);
        View.SetLinkFlags(l,SGRI_DONTFOCUS,0);break;
      case ID_LINK_USELASTSETTINGS: OnLinkUselastsettings(); break;
      case ID_LINK_ARROWTYPE: OnLinkArrowtype(); break;
      case ID_LINK_CHANGECOLOR:
        //AfxGetMainWnd()->SendNotifyMessage(cmd, NULL, NULL); //nor SendMessage
        OnLinkChangecolor();  //this works, but why upper doesn't?
        break;
      case ID_LINK_UNLINKITEMS: View.OnMenuUnlink(); break;
      }
      View.Update();
    }
  }
}

void CMainFrame::OnUpdatePropertiesCanbelinked(CCmdUI* pCmdUI) 
{
  pCmdUI->SetCheck((View.GetSelectionFlags() & SGRI_CANBELINKED)!=0);
}

void CMainFrame::OnUpdatePropertiesCanmakelinks(CCmdUI* pCmdUI) 
{
  pCmdUI->SetCheck((View.GetSelectionFlags() & SGRI_CANMAKELINKS)!=0);
}

void CMainFrame::OnUpdatePropertiesCanmove(CCmdUI* pCmdUI) 
{
  pCmdUI->SetCheck((View.GetSelectionFlags() & SGRI_CANMOVE)!=0);
}

void CMainFrame::OnUpdatePropertiesCanselflinked(CCmdUI* pCmdUI) 
{
  pCmdUI->SetCheck((View.GetSelectionFlags() & SGRI_CANSELFLINKED)!=0);
}

void CMainFrame::OnUpdatePropertiesCansize(CCmdUI* pCmdUI) 
{
  pCmdUI->SetCheck((View.GetSelectionFlags() & SGRI_CANRESIZE)!=0);
}

void CMainFrame::OnUpdatePropertiesCustomdraw(CCmdUI* pCmdUI) 
{
  pCmdUI->SetCheck((View.GetSelectionFlags() & SGRI_CUSTOMDRAW)!=0);
}

void CMainFrame::OnPropertiesCustomdraw() 
{
  View.SetFlagSelection(SGRI_CUSTOMDRAW,0 );	
  View.Update();
}

void CMainFrame::OnPropertiesCansize() 
{
  View.SetFlagSelection(SGRI_CANRESIZE,0);	
  View.Update();
}

void CMainFrame::OnPropertiesCanselflinked() 
{
  View.SetFlagSelection(SGRI_CANSELFLINKED,0);	
  View.Update();
}

void CMainFrame::OnPropertiesCanmove() 
{
  View.SetFlagSelection(SGRI_CANMOVE,0);	
  View.Update();
}

void CMainFrame::OnPropertiesCanmakelinks() 
{
  View.SetFlagSelection(SGRI_CANMAKELINKS,0);	
  View.Update();
}

void CMainFrame::OnPropertiesCanbelinked() 
{
  View.SetFlagSelection(SGRI_CANBELINKED,0);	
  View.Update();
}

void CMainFrame::OnLinkNormal() 
{
  View.Mode(SGRM_NORMAL);	
}

void CMainFrame::OnLinkDesign() 
{
  View.Mode(SGRM_DESIGN);
}

void CMainFrame::OnLinkZoom() 
{
  View.Mode(SGRM_ZOOM);	
}

void CMainFrame::OnUpdateLinkNormal(CCmdUI* pCmdUI) 
{
  pCmdUI->SetCheck(View.Mode(SGRM_GETCURRENT)==SGRM_NORMAL);	
}

void CMainFrame::OnUpdateLinkZoom(CCmdUI* pCmdUI) 
{
  pCmdUI->SetCheck(View.Mode(SGRM_GETCURRENT)==SGRM_ZOOM);	
}

void CMainFrame::OnUpdateLinkDesign(CCmdUI* pCmdUI) 
{
  pCmdUI->SetCheck(View.Mode(SGRM_GETCURRENT)==SGRM_DESIGN);	
}

//Messages from CGraphCtrlExt handling
LPARAM CMainFrame::OnNotifyEndSel(WPARAM,LPARAM)
{
  ItemInfo *info=View.GetRefItem();
  wFontBar.UpdateFields(info);
  return 0;
}

//Messages from DlgFontBar handling
LPARAM CMainFrame::OnBoolStyleChanged(WPARAM wParam,LPARAM lParam)
{
  ItemInfo *info=View.GetRefItem();
  LOGFONT reffont=info->lgFont;
  LOGFONT newfont=reffont;
  switch (wParam)
  {
  case IDC_FONTBOLD: newfont.lfWeight=lParam?700:400;break;
  case IDC_FONTITALIC: newfont.lfItalic=(BYTE)lParam;break;
  case IDC_FONTSTRIKEOUT: newfont.lfStrikeOut=(BYTE)lParam;break;
  case IDC_FONTUNDERLINE: newfont.lfUnderline=(BYTE)lParam;break;
  }
  View.ChangeTextFontChanges(reffont,newfont,0,0,-1);   
  View.Update();        
  return 0;
}

LPARAM CMainFrame::OnFontChanged(WPARAM wParam,LPARAM lParam)
{
  CDialog *dlg=(CDialog *)lParam;
    CComboBox *lsb=static_cast<CComboBox *>(dlg->GetDlgItem(IDC_FONTFACE));

  CString font;
  lsb->GetLBText(lsb->GetCurSel(),font);
  int h=dlg->GetDlgItemInt(IDC_FONTSIZE,NULL,FALSE);
  int w=dlg->GetDlgItemInt(IDC_WIDTH,NULL,FALSE);  
  int a=dlg->GetDlgItemInt(IDC_ANGLE,NULL,FALSE)*10;
  const char *fc=font;
  if (~wParam & 1) fc=NULL;
  if (~wParam & 2) h=-1;
  if (~wParam & 4) w=-1;
  if (~wParam & 8) a=-1;
  View.ChangeFontFaceSize(fc,h,w,a);
  return 0;
}

LPARAM CMainFrame::OnFontColorChanged(WPARAM wParam,LPARAM lParam)
{
  View.ChangeTextColor(lParam);
  return 0;
}

LPARAM CMainFrame::OnTextAlignChanged(WPARAM wParam,LPARAM lParam)
{
  View.ChangeTextAlign(lParam);
  return 0;
}

void CMainFrame::OnLinkUselastsettings()
{
  if (View._curlink!=-1)  
  {
    View.SetLinkToLastColor(View._curlink);  
    View.Update();
  }
}

void CMainFrame::OnLinkChangecolor()
{
  if (View._curlink!=-1)
  {
    CColorDialog colorDialog(0,CC_FULLOPEN|CC_RGBINIT|CC_ANYCOLOR);
    colorDialog.m_cc.rgbResult=View.GetLink(View._curlink).color;
    if (colorDialog.DoModal()==IDOK)
    {
      View.SetLinkColor(View._curlink,colorDialog.GetColor());
      View.Update();
    }
  }
}

void CMainFrame::OnLinkArrowtype()
{
  if (View._curlink!=-1)
  {
    DlgLinkProperties dlg;
    SDLinkInfo *flags;
    flags=View.GetLinkExtraData(View._curlink);
    dlg.vUseArrows=flags!=NULL;
    if (flags)
    {
      dlg.vArrowSize=flags->arrowsize;
      dlg.vLineStyle=flags->linestyle;
      dlg.vLineWidth=flags->linewidth;
      dlg.vArrowType=flags->arrowtype;
      dlg.vUseBKColor=flags->usebkcolor;
    }
    if (dlg.DoModal()==IDOK)
    {
      if (dlg.vUseArrows)
      {      
        SDLinkInfo *sflags;
        if (flags) sflags=new SDLinkInfo(*flags);else sflags=new SDLinkInfo;
        sflags->arrowsize=dlg.vArrowSize;
        sflags->arrowtype=dlg.vArrowType;
        sflags->linestyle=dlg.vLineStyle;
        sflags->linewidth=dlg.vLineWidth;
        sflags->usebkcolor=dlg.vUseBKColor!=FALSE;
        View.SetArrowType(View._curlink,sflags);
      }
      else
        View.SetArrowType(View._curlink,NULL);
      View.Update();
    }
  }  
}

void CMainFrame::OnDesignInsertnew() 
{
  View.Mode(SGRM_NEWITEM);
}

static bool HasInactiveItems(CGraphCtrlExt &View)
{  
  for (int i=-1;(i=View.EnumItems(i,true))!=-1;)
  {
    DWORD flags=View.GetItemFlags(i);
    if ((flags & LINKACTIVE) != LINKACTIVE) return true;
  }
  return false; 
}

void CMainFrame::OnDiagramInactive()
{
  bool ina=HasInactiveItems(View);
  for (int i=-1;(i=View.EnumItems(i,true))!=-1;)
  {
    View.SetItemFlags(i,ina?LINKACTIVE:0,LINKACTIVE);
  }
}

void CMainFrame::OnUpdateDiagramInactive(CCmdUI *pCmdUI)
{
  pCmdUI->SetCheck(HasInactiveItems(View));
  pCmdUI->Enable(View.EnumItems(-1,true)!=-1);
}

LPARAM CMainFrame::OnFocusItem(WPARAM wParam, LPARAM lParam)
{
  View.Deselect();
  int item1 = unsigned(lParam)%65536;
  if (item1!=0xffff) View.SelectAndMoveToItem(item1);
  int item2 = unsigned(lParam)/65536;
  if (item2!=0xffff) View.SelectItem(item2);

  View.Update();
  if (wParam) // it is message from DebugFSMLogDlg
  {
    View.OnEndSelection();

    WINDOWPLACEMENT wndpl;
    if (debugFSMLogDlg->GetWindowPlacement(&wndpl))
    {
      switch (wndpl.showCmd)
      {
      case SW_SHOWNORMAL:
      case SW_SHOWMAXIMIZED:
      case SW_SHOW:
        {
          SetFocus();
          CEdit *edit = (CEdit*)debugFSMLogDlg->GetDlgItem(IDC_EDIT);
          if (edit)
          {
            edit->SetFocus();
          }
        }
      }
    }
  }

  return 0;
}

LPARAM CMainFrame::OnStateChanged2(WPARAM wParam, LPARAM lParam)
{
  View.OnStateChanged(wParam, lParam);
  return 0;
}

LPARAM CMainFrame::OnWndSerialize(WPARAM wParam,LPARAM lParam)
{
  //split.SetSplitPos(0.75f);
  if (lParam)
  {
    ParamArchive *arch = (ParamArchive *)lParam;
    if (arch->IsLoading())
    {
      int splitpos=0;
      arch->Serialize("SplitPos", splitpos, 1);
      if (splitpos) 
      {
        TextEdit.ReleaseTouch(false);
        split->SetSplitPos(splitpos);
        split->RecalcLayout();
        View.OnEndSelection();
      }
    }
    else
    {
      int splitpos = split->GetSplitPos();
      arch->Serialize("SplitPos", splitpos, 1);
    }
  }
  else //untouch (null value is used for other purpose)
  {
    TextEdit.ReleaseTouch(false);
  }
  return 0;
}

LPARAM CMainFrame::OnDirtinessChanged(WPARAM wParam,LPARAM lParam)
{
  int dirty = lParam;
  CString text; GetWindowText(text);
  if (text[text.GetLength()-1]=='*') text = text.Left(text.GetLength()-2);
  if (dirty) text += " *";
  SetWindowText(text);
  return 0;
}

LPARAM CMainFrame::OnPrepareForSaving(WPARAM wParam,LPARAM lParam)
{
  TextEdit.SaveData();
  return 0;
}

//////////////////////////////////////////////////////////////////////////
//
// Default State Handling
//
void CMainFrame::OnDesignStartstate()
{
  DesignLoadFromDefault(DF_START_ITEM);
}

void CMainFrame::OnDesignEndstate()
{
  DesignLoadFromDefault(DF_END_ITEM);
}

void CMainFrame::OnDesignState()
{
  DesignLoadFromDefault(DF_ITEM);
}

void CMainFrame::OnDesignUserState()
{
  DesignLoadFromDefault(DF_USER_ITEM);
}

void CMainFrame::OnDesignCondition()
{
  DesignLoadFromDefault(DF_CONDITION);
}

void CMainFrame::OnDesignTruecondition()
{
  DesignLoadFromDefault(DF_TRUE_CONDITION);
}

void CMainFrame::OnDesignInputcondition()
{
  DesignLoadFromDefault(DF_USER_INPUT_CONDITION);
}

void CMainFrame::OnDesignUsercondition()
{
  DesignLoadFromDefault(DF_USER_CONDITION);
}

void CMainFrame::OnDesignKnee()
{
  DesignLoadFromDefault(DF_KNEE);
}

void CMainFrame::DesignLoadFromDefault(DefaultItemType type)
{
  View.SaveUndo();
  for(int i=-1;(i = View.EnumItems(i,true)) != -1;) 
  {
    SGraphItem itm;
    View.GetItem(i, &itm);
    //FSMDataDlgs must release touch on this item
    TextEdit.ReleaseTouch();
    View.defItems.ChangeToDefaultItem(type, itm);
    TextEdit.ActivateDlg(/*type,*/itm, true); //refresh only
    View.SetItem(i, &itm);
  }
  if (View.Mode(SGRM_GETCURRENT)==SGRM_DESIGN) View.Mode(SGRM_DESIGN); //cancel e_begdragdesign
  View.Update();
}

void CMainFrame::OnSetasdefaultState()
{
  DesignSetasdefault(DF_ITEM);
}

void CMainFrame::OnSetasdefaultUserstate()
{
  DesignSetasdefault(DF_USER_ITEM);
}

void CMainFrame::OnSetasdefaultCondition()
{
  DesignSetasdefault(DF_CONDITION);
}

void CMainFrame::OnSetasdefaultTruecondition()
{
  DesignSetasdefault(DF_TRUE_CONDITION);
}

void CMainFrame::OnSetasdefaultInputcondition()
{
  DesignSetasdefault(DF_USER_INPUT_CONDITION);
}

void CMainFrame::OnSetasdefaultUsercondition()
{
  DesignSetasdefault(DF_USER_CONDITION);
}

void CMainFrame::OnSetasdefaultStartstate()
{
  DesignSetasdefault(DF_START_ITEM);
}

void CMainFrame::OnSetasdefaultEndstate()
{
  DesignSetasdefault(DF_END_ITEM);
}

void CMainFrame::OnSetasdefaultKnee()
{
  DesignSetasdefault(DF_KNEE);
}

void CMainFrame::DesignSetasdefault(DefaultItemType type)
{
  // TODO: Add your command handler code here
  //if there are more then one selected items, then announce, that task will not be done
  int cnt=0;
  for (int i=-1; (i=View.EnumItems(i,true))!=-1; cnt++);
  if (cnt!=1) 
  {
    AfxMessageBox("More than one item selected!\nAction canceled.", MB_OK|MB_ICONSTOP);
    return;
  }
  int curitem=View.EnumItems(-1,true);
  if (curitem!=-1) View.defItems.SetDefaultItem(type, curitem);
}

void CMainFrame::OnDefaultsSaveas()
{
  CString defext;defext.LoadString(IDS_DEFAULTSEXT);
  CString filter;filter.LoadString(IDS_DEFAULTSFILTER);
  CString name;name.LoadString(IDS_DEFAULTSAVETITLE);
  CString fname; fname.LoadString(IDS_DEFAULTFILENAME);
  CFileDialog fdlg(FALSE,defext,fname,OFN_PATHMUSTEXIST|OFN_OVERWRITEPROMPT|OFN_HIDEREADONLY,filter);
  fdlg.m_ofn.lpstrTitle=name;
  if (fdlg.DoModal()==IDOK)
  {
    CString fname=fdlg.GetPathName();
    if (View.defItems.Save(fname)==false) AfxMessageBox(IDS_SAVEFAILED);
  }
}

void CMainFrame::OnDefaultsLoad()
{
  CString defext;defext.LoadString(IDS_DEFAULTSEXT);
  CString filter;filter.LoadString(IDS_DEFAULTSFILTER);
  CString name;name.LoadString(IDS_DEFAULTSOPENTITLE);
  CString fname; fname.LoadString(IDS_DEFAULTFILENAME);
  CFileDialog fdlg(TRUE,defext,fname,OFN_FILEMUSTEXIST|OFN_HIDEREADONLY,filter);
  fdlg.m_ofn.lpstrTitle=name;
  if (fdlg.DoModal()==IDOK)
  {
    fname=fdlg.GetPathName();
    if (View.defItems.Load(fname)==false) AfxMessageBox(IDS_LOADDEFAULTSFAILED);
  }
}

//////////////////////////////////////////////////////////////////////////
//
// PageSetup Dialog
//
void CMainFrame::OnFilePagesetup()
{
  DlgPageSetup dlg;
  dlg.vColor=View._pageColorVal;
  dlg.vForceColor=View._pageColor;
  dlg.vImageX=View._pageImageX;
  dlg.vImageY=View._pageImageY;
  dlg.vForceDim=View._pageClip;
  dlg.vEnableGrid=View.GetGrid()!=0.0f;
  dlg.vGrid=dlg.vEnableGrid?View.GetGrid():25.0f;
  dlg.vShowGrid=View.IsGridVisible();
  if (dlg.DoModal()==IDOK)
  {
    View._pageColorVal=dlg.vColor;
    View._pageColor=dlg.vForceColor!=FALSE;
    View._pageImageX=dlg.vImageX;
    View._pageImageY=dlg.vImageY;
    View._pageClip=dlg.vForceDim!=FALSE;
    View.SetGrid(dlg.vEnableGrid?dlg.vGrid:0,dlg.vShowGrid!=FALSE);
    View.Update();
  }
}

void CMainFrame::OnChangeactivewnd()
{
  CWnd *fcs = GetFocus();
  if (fcs==&View)
  {
    if (TextEdit.conditionDlg.IsWindowVisible()) TextEdit.conditionDlg.GotoDlgCtrl(TextEdit.conditionDlg.GetDlgItem(IDC_CONDITIONEDIT));
    else if (TextEdit.stateDlg.IsWindowVisible()) TextEdit.stateDlg.GotoDlgCtrl(TextEdit.stateDlg.GetDlgItem(IDC_STATEEDIT));
  }
  else if (fcs==&TextEdit || fcs->GetParent()==&TextEdit || fcs->GetParent()->GetParent()==&TextEdit)
    ::SetFocus(View.GetSafeHwnd());
}

//////////////////////////////////////////////////////////////////////////
//
// OnFileCompile
//   Spawns FSMCompiler and parse its stdout
//
#define   OUT_BUFF_SIZE 512
#define   READ_FD  0
#define   WRITE_FD 1
char szBuffer[OUT_BUFF_SIZE+1];
static char BisFSMCompilerPath[] = "c:\\bis\\fsmeditor\\fsmcompiler.exe";
static char CurPathFSMCompiler[] = "FSMCompiler.exe";
static CString OtherCompiler;
char * Compileargv[] = {NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL};

//#include <stdlib.h>
#include <io.h>
#include <fcntl.h>
#include <process.h>
#include "el/QStream/QStdStream.hpp"
#include "CompileResultDlg.h"
#include "EL/Pathname/Pathname.h"

static bool FileExistsTest(const char *file)
{
  int ftest = _open(file, _O_RDONLY); //only file exist test
  if (ftest!=-1) 
  {
    _close(ftest);
    return true;
  }
  return false;
}

bool LoadFSMCompilerArgv()
{
  if (Compileargv[0]) //if initialized yet, try it
  {
    if (FileExistsTest(Compileargv[0])) return true;
  }
  CString fname = CurPathFSMCompiler;
  if (FileExistsTest(fname)) Compileargv[0] = CurPathFSMCompiler; //use compiler in current directory
  else //try compiler directory of exeFile
  {
    Pathname exepath = Pathname::GetExePath();
    exepath.SetFilename(fname);
    const char *path = exepath.GetFullPath();
    if (FileExistsTest(path))
    {
      OtherCompiler = path;
      Compileargv[0]=OtherCompiler.GetBuffer();
    }
    else //use compiler on path c:\bis\...
    {
      fname = BisFSMCompilerPath;
      if (FileExistsTest(fname)) Compileargv[0]=BisFSMCompilerPath;
      else //No FSMCompiler found yet, let the user select one
      {
        if (AfxMessageBox("Couldn't find FSMCompiler.exe!\nDo you want to specify path?", MB_YESNOCANCEL)==IDYES)
        {
          CString defext("exe");
          CString filter("Executable (*.exe)|*.exe||");
          CString name("Select FSMCompiler.exe path");
          CString fname("");
          CFileDialog fdlg(TRUE,defext,fname,OFN_PATHMUSTEXIST|OFN_OVERWRITEPROMPT|OFN_HIDEREADONLY,filter);
          fdlg.m_ofn.lpstrTitle=name;
          if (fdlg.DoModal()==IDOK)
          {
            OtherCompiler=fdlg.GetPathName();
            Compileargv[0]=OtherCompiler.GetBuffer();
            return true;
          }
          return false;
        }
        return false;
      }
    }
  }
  return true;
}

#include <Es/Files/filenames.hpp>
void CMainFrame::OnFileCompile()
{
  if (!curfname.GetLength()) return;
  if (!LoadFSMCompilerArgv()) return;

  //fdStdInPipe is used to send data to fsmCompiler, which reads it using stdin
  int fdStdIn;
  fdStdIn = _dup(0);
  int fdStdInPipe[2];
  if(_pipe(fdStdInPipe, 4096, O_TEXT | O_NOINHERIT) == -1) return;
  if(_dup2(fdStdInPipe[READ_FD], 0) != 0) return;
  close(fdStdInPipe[READ_FD]);

  //fdStdOutPipe is used to get stdout from fsmCompiler
  int fdStdOut;
  int fdStdOutPipe[2];
  if(_pipe(fdStdOutPipe, 4096, O_TEXT | O_NOINHERIT) == -1) return;
  fdStdOut = _dup(1);
  if(_dup2(fdStdOutPipe[WRITE_FD], 1) != 0) return;
  close(fdStdOutPipe[WRITE_FD]);

  //fill in the commandline parameters for fsmCompiler
  View.SaveUndo();
  wToolBar->OnEditUndo();
  Compileargv[1]="-stdin";
  Compileargv[2]="-n";
  Compileargv[3]="-nocompile";
  Compileargv[4]=NULL;

  //run FSMCompiler [-n] -stdin
  //and read report from FSMEditor's stdout
  HANDLE hProcess = (HANDLE)spawnvp(P_NOWAIT, Compileargv[0], Compileargv);
  //fsmCompiler had to be spawned before writing, or writing must be done by fork()
  //  as process writing into pipe is due to buffering waiting, until receiver reads it
  //write FSM in *.bifsm format into pipe
  QOStrStream outStream;
  View.Save(outStream); //this must be changed to check *.bifsm ext or other
  write(fdStdInPipe[WRITE_FD], outStream.str(), outStream.pcount());
  close(fdStdInPipe[WRITE_FD]);

  if(_dup2(fdStdIn, 0) != 0) return; //restore stdin
  close(fdStdIn);
  if(_dup2(fdStdOut, 1) != 0) return; //restore stdout
  close(fdStdOut);

  int nExitCode = STILL_ACTIVE;
  if(hProcess && (unsigned int)hProcess!=0xffffffff)
  {
#ifdef _DEBUG
    QStdDebugStream out;
#endif
    CString errorReport;
    FILE *infile = _fdopen(fdStdOutPipe[READ_FD], "rt");
    while (nExitCode == STILL_ACTIVE)
    {
      //nOutRead = read(fdStdOutPipe[READ_FD], szBuffer, OUT_BUFF_SIZE);
      while(fgets(szBuffer, OUT_BUFF_SIZE, infile))
      {
        szBuffer[strlen(szBuffer)-1]=0;
        errorReport += szBuffer;
        errorReport += "\r\n";
#ifdef _DEBUG
        out << szBuffer; //for debug purposes
#endif
      }
      if(!GetExitCodeProcess(hProcess,(unsigned long*)&nExitCode)) return;
    }
    compResultDlg->editText = errorReport;
    CEdit *edit = (CEdit*)compResultDlg->GetDlgItem(IDC_COMPILE_EDIT);
    compResultDlg->ShowWindow(SW_SHOW);
    compResultDlg->UpdateData(FALSE);
    compResultDlg->SetFocus();
    edit->SetSel(0,0);
  }
}

LPARAM CMainFrame::OnInitialUpdate(WPARAM,LPARAM)
{
  if (__argc>1)
  {
    int idx=1;
    while (idx<__argc && __argv[idx][0]=='-') idx++;
    if (idx<__argc)
    {
      curfname=__argv[idx];
      CString defext;defext.LoadString(IDS_DIAGRAMDEFEXT);
      RString ext(GetFileExt(curfname.GetBuffer())); ext.Lower();
      if (ext==RString(defext))
      {
        if (View.Load(curfname)==false) AfxMessageBox(IDS_LOADFAILED);
      }
      else 
      {
        wToolBar->OpenThroughFSMCompiler();
      }
      if (IsRelativePath(curfname.GetBuffer()))
      {
        char buf[1024];
        GetCurrentDirectory(1024,buf);
        strcat(buf,"\\"); strcat(buf,curfname.GetBuffer());
        curfname=buf;
      };
      View.Update();
      UpdateCaption();
    }
  }
  else //View.AddStartState();
  {
    wToolBar->NewFile();
  }

  theApp.ProcessReady();
  return 0;
}

void CMainFrame::OnStateChanged(WPARAM wParam, LPARAM lParam)
{
  View.OnStateChanged(wParam, lParam);
}

//FsmAttributes menu commands
#include "AttrEditDlg.h"
#include "FsmNameDlg.h"
void CMainFrame::OnFsmattributesFsmname()
{
  // TODO: Add your command handler code here
  FsmNameDlg fsmdlg;
  fsmdlg.fsmName= View.fsmName;
  if (fsmdlg.DoModal()==IDOK) 
    View.fsmName = fsmdlg.fsmName;
}

void CMainFrame::OnFsmattributesCompileconfig()
{
  // TODO: Add your command handler code here
  CompileConfigDlg compiledlg;
  compiledlg.configFile = View.compileConfigFileName;
  if (compiledlg.DoModal()==IDOK)
    View.compileConfigFileName = compiledlg.configFile;
}

LPARAM CMainFrame::OnMsgAttrEdit(WPARAM wParam,LPARAM lParam)
{
  OnFsmattributesAttributesfromcompileconfig();
  return 0;
}

//Options menu commands
void CMainFrame::OnChangeFont()
{
  TextEdit.ChangeFont();
  TextEdit.StoreFontToProfile(theApp);
}

#include "el/ParamFile/paramFile.hpp"
void CMainFrame::OnFsmattributesAttributesfromcompileconfig()
{
  // TODO: Add your command handler code here
  //open and parse compileConfig to get the names of specific attributes
  RString FindFilePath(RString);
  RString configPath = FindFilePath(View.compileConfigFileName.GetBuffer());
  if (configPath!=RString(""))
  {
    ParamFile compileConfig;
    if (compileConfig.Parse(configPath)==LSOK)
    {
      ParamEntryVal attributes = compileConfig >> "Attributes" >> "names";
      if (attributes.IsArray())
      {
        int n = attributes.GetSize();
        if (n==1)
        {
          RString value = View.attributes.GetValue(attributes[0].GetValue().Data());
          View.attributes.attrIx = View.attributes.SetValue(attributes[0].GetValue().Data(), value);
          CAttrEditDlg dlg(this, &View);
          dlg.DoModal();
          return;
        }
      }
    }
  }

  FsmAttributesDlg dlg(View.compileConfigFileName.GetBuffer());
  if (dlg.DoModal()==IDOK && dlg.attributeName!="")
  {
    RString value = View.attributes.GetValue(dlg.attributeName.GetBuffer());
    View.attributes.attrIx = View.attributes.SetValue(dlg.attributeName.GetBuffer(), value);
    CAttrEditDlg dlg(this, &View);
    dlg.DoModal();
  }
}

BOOL CMainFrame::OnDebugFSMLogCheck(UINT nID)
{
  debugFSMLogDlg->ShowWindow(SW_SHOW);
  return TRUE;
}

void CMainFrame::OnOptionsSplitvertical()
{
  // TODO: Add your command handler code here
  gVertSplit = !gVertSplit;
  // try to change it immediately
  CSplitWindow *old = split;
  if (gVertSplit)
  {
    CSplitWindowVert *splitVert = new CSplitWindowVert();
    splitVert->Create(this,0.75f,AFX_IDW_PANE_FIRST);
    splitVert->EnableResize1(true);
    splitVert->EnableResize2(true);
    split = splitVert;
  }
  else
  {
    CSplitWindowHorz *splitHorz = new CSplitWindowHorz();
    splitHorz->Create(this,0.75f,AFX_IDW_PANE_FIRST);
    splitHorz->EnableResize1(true);
    splitHorz->EnableResize2(true);
    split = splitHorz;
  }

  TextEdit.SetParent(split);
  View.SetParent(split);

  split->SetSplitPos(0.75f);
  split->RecalcLayout();

  old->SendMessage(WM_CLOSE, 0, 0);
  delete old;
  RecalcLayout();
}

void CMainFrame::OnUpdateOptionsSplitvertical(CCmdUI *pCmdUI)
{
  // TODO: Add your command update UI handler code here
  pCmdUI->SetCheck(gVertSplit);
}
