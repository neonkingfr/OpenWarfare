// CompileResultDlg.cpp : implementation file
//

#include "stdafx.h"
#include "GraphView.h"
#include "CompileResultDlg.h"
#include ".\compileresultdlg.h"


// CompileResultDlg dialog

IMPLEMENT_DYNAMIC(CompileResultDlg, CDialog)
CompileResultDlg::CompileResultDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CompileResultDlg::IDD, pParent)
  , editText(_T(""))
{
}

CompileResultDlg::~CompileResultDlg()
{
}

void CompileResultDlg::DoDataExchange(CDataExchange* pDX)
{
  CDialog::DoDataExchange(pDX);
  DDX_Text(pDX, IDC_COMPILE_EDIT, editText);
}


BEGIN_MESSAGE_MAP(CompileResultDlg, CDialog)
  ON_WM_SIZE()
  ON_WM_CLOSE()
END_MESSAGE_MAP()


// CompileResultDlg message handlers

void CompileResultDlg::OnSize(UINT nType, int cx, int cy)
{
  CDialog::OnSize(nType, cx, cy);

  // TODO: Add your message handler code here
  CWnd *edit = GetDlgItem(IDC_COMPILE_EDIT);
  if (edit)
  {
    CRect rect;
    edit->GetWindowRect(&rect);
    CPoint origin(0,0);
    ClientToScreen(&origin);
    edit->SetWindowPos(NULL, 0,0, cx+origin.x-rect.left, cy+origin.y-rect.top, SWP_NOMOVE);
  }
}

void CompileResultDlg::OnClose()
{
  // TODO: Add your message handler code here and/or call default
  ShowWindow(SW_HIDE);
  //CDialog::OnClose();
}

BOOL CompileResultDlg::PreTranslateMessage(MSG* pMsg)
{
  // TODO: Add your specialized code here and/or call the base class
  static int pos = -1;
  static int curLine = -1;
  if (CDialog::PreTranslateMessage(pMsg))
  {
    CEdit *edit = (CEdit*)GetDlgItem(IDC_COMPILE_EDIT);
    if (edit)
    {
      int dummy, newPos;
      edit->GetSel(newPos, dummy);
      if (newPos!=pos)
      {
        pos = newPos;
        int newline = edit->LineFromChar(pos);
        if (curLine!=newline)
        {
          curLine = newline;
          char *line = editText.GetBuffer()+edit->LineIndex(curLine);
          int itemNumber=-1;
          int secondNumber=0xffff;
          if (*line=='(') 
          {
            line++;
            if (*line>='0' && *line<='9') sscanf(line, "%d,%d", &itemNumber, &secondNumber);
            if (itemNumber != -1) 
            {
              AfxGetMainWnd()->PostMessage(MSG_FOCUS_ERROR_ITEM, 0, itemNumber+secondNumber*65536);
            }
          }
          else AfxGetMainWnd()->PostMessage(MSG_FOCUS_ERROR_ITEM, 0, 0xffffffff); //Deselect
        }
      }
    }
    return true;
  }
  return false;
}
