// FSMEditor.h extension of CGraphCtrlExt class
//
//////////////////////////////////////////////////////////////////////

#if !defined(_FSMEDITOR)
#define _FSMEDITOR

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "GraphCtrlExt.h"
#include "DefaultItem.h"

#define MSG_STATE_CHANGED (WM_USER + 0)
#define MSG_EVENT_HANDLE  (WM_USER + 1)


class CCodeEdit : public CEdit
{
public:
  int selBeg, selEnd;
  int lScroll;

  CCodeEdit() : selBeg(0), selEnd(0), lScroll(0), CEdit()
  {}

  void SetFocusData();
  void SaveFocusData();

  DECLARE_MESSAGE_MAP()

  afx_msg void OnLButtonDblClk(UINT nFlags, CPoint point);
  afx_msg void OnSetFocus(CWnd* pNewWnd);
  afx_msg void OnKillFocus(CWnd* pNewWnd);
};

class StringEditValue : public CString
{
public:
  typedef CString base;
  int selBeg, selEnd;
  int lScroll;

  
  StringEditValue() : selBeg(0), selEnd(0), lScroll(0) {}
  StringEditValue &operator=(CString val) { (void) base::operator=(val); return *this; }
  StringEditValue &operator=(RString val) { (void) base::operator=(val); return *this; }

 void LoadFrom(const CString stateEdit, const CCodeEdit &stateEditCtrl)
 {
   (void) base::operator=(stateEdit);
   selBeg = stateEditCtrl.selBeg;
   selEnd = stateEditCtrl.selEnd;
   lScroll = stateEditCtrl.lScroll;
 }
 void SaveTo(CString &stateEdit, CCodeEdit &stateEditCtrl)
 {
   stateEdit = *this;
   stateEditCtrl.selBeg = selBeg;
   stateEditCtrl.selEnd = selEnd;
   stateEditCtrl.lScroll = lScroll;
   stateEditCtrl.SetFocusData();
 }
};

typedef int FSMItemType ;
const FSMItemType FSMStateType(1);
const FSMItemType FSMConditionType(2);

struct FSMCondition
{
  int   id;
  float priority;
  StringEditValue condition;
  StringEditValue action;
  StringEditValue preCondition;
  int   conditionTab;  //true, iff selected Conditon in ConditionDataDlg
};
struct FSMState
{
  int   id;
  StringEditValue initCode;
  StringEditValue preCondition;
  int stateTab;
};
//virtual interface for FSMState and FSMCondition
class FSMItemValue : public SerializeClass
{
  public:
    DefaultItemType type;
    virtual FSMItemType GetType() const = 0;
    virtual ~FSMItemValue() {}

    virtual FSMState const *GetFSMState() const {return NULL;}
    virtual FSMCondition const *GetFSMCondition() const {return NULL;}
    virtual FSMItemValue *Clone() const {return NULL;};
    virtual LSError Serialize(ParamArchive &) {return LSOK;}
    virtual void SerializeBin(SerializeBinStream &f) {}
    virtual int *GetID() {return NULL;}
};
class FSMStateValue : public FSMItemValue
{
  FSMState _value;
public:
  FSMStateValue() {type=DF_UNDEF;}
  FSMStateValue(FSMState state) : _value(state) {type=DF_UNDEF;}
  virtual ~FSMStateValue() {}
  FSMItemType GetType() const {return FSMStateType;}
  FSMState const *GetFSMState() const { return &_value; }
  virtual FSMStateValue *Clone() const
  {
    FSMStateValue *stat=new FSMStateValue(_value);
    stat->type = type;
    return stat;
  }
  virtual LSError Serialize(ParamArchive &ar);
  virtual void SerializeBin(SerializeBinStream &f);
  virtual int *GetID() {return &_value.id;}
};
class FSMConditionValue : public FSMItemValue
{
  FSMCondition _value;
public:
  FSMConditionValue() {type=DF_UNDEF;}
  FSMConditionValue(FSMCondition cond) : _value(cond) {type=DF_UNDEF;}
  virtual ~FSMConditionValue() {}
  FSMItemType GetType() const {return FSMConditionType;}
  FSMCondition const *GetFSMCondition() const { return &_value; }
  virtual FSMConditionValue *Clone() const 
  {
    FSMConditionValue *cond=new FSMConditionValue(_value);
    cond->type = type;
    return cond;
  }
  virtual LSError Serialize(ParamArchive &ar);
  virtual void SerializeBin(SerializeBinStream &f);
  virtual int *GetID() {return &_value.id;}
};

struct FSMItemInfo : public ItemInfo
{
  FSMItemValue *fsmData;
  FSMItemInfo();
  virtual ~FSMItemInfo();
#ifdef DEBUG
//#pragma message ("adding FSMItemInfo::operator=")
  virtual FSMItemInfo &operator=(ItemInfo &itm2) {ASSERT(FALSE); return *this;}
  FSMItemInfo &operator=(FSMItemInfo &itm2) {ASSERT(FALSE); return *this;}
#endif
  virtual LSError Serialize(ParamArchive &ar);
  virtual void SerializeBin(SerializeBinStream &f);
  virtual bool GetShouldBeAlignedToGrid();

  static bool IsCondition(DefaultItemType type);
  static bool IsState(DefaultItemType type);
  virtual FSMItemInfo *Clone();
  FSMItemValue * CreateByType(DefaultItemType type);
  void CopyFsmData(FSMItemValue *data);
private:
  FSMItemInfo(FSMItemInfo &)
  {
    ASSERT(false);
  }  //no copy
};

struct FSMNamedAttribute : public SerializeClass
{
  RString name;
  RString value;
  FSMNamedAttribute(RString nm="", RString val="") : name(nm), value(val) {}

  virtual LSError Serialize(ParamArchive &ar)
  {
    ar.Serialize("name", name, 1);
    ar.Serialize("value", value, 1);
    return LSOK;
  }
};
TypeIsMovable(FSMNamedAttribute);

struct FSMAttributes
{
  //array of attributes from compile config
  AutoArray<FSMNamedAttribute> compileConfigAttr;
  //index into compileConfigAttr
  int attrIx;
  //if attr with name doesn't exist, get empty string
  RString GetValue(RString name);
  //if attr with name doesn't exist, create it
  //return index where set
  int SetValue(RString name, RString value); 

  FSMAttributes() {}
  void SetActiveText(RString text);
};

class FSMEditor : public CGraphCtrlExt
{
public:
  CString fsmName, compileConfigFileName;
  CString &fileName;
  FSMAttributes attributes;
  DefaultItems defItems;
  int          nextID;  //next free ID value

  int _debugState;
  DWORD _oldColor;

  FSMEditor(CString &file);
  virtual void OnLinkNewItem(int beginitem, float x, float y);
  //ItemInfo maintenance
  virtual FSMItemInfo *CreateItemInfo();    //like virtual constructor
  virtual ItemInfo *GetDefItemInfo();       //get default item info
  static  ItemInfo *GetDefItemInfoStatic(); //get default item info, but static member
  virtual FSMItemInfo *GetItemExtraData(int itemid);
  int GetNewID() { return nextID++; };
  virtual void ResetContent();

  void RefreshDataDlg(bool refreshOnly=false);
  // Id in GetItemExtraDataById is different from item number. It is an Id inside FSM state or condition.
  FSMItemInfo *GetItemExtraDataById(int id);
  virtual void OnEndSelection();
  virtual void DeleteItem(int id);
  virtual bool SerializeExtra(ParamArchive &ar);
  virtual void AdjustAfterDuplication(ItemInfo *newinfo);

  //True when more links can be used from the same from->to items
  virtual bool UseLinkOrder(int item);

  void AddStartState();

  void OnStateChanged(WPARAM,LPARAM);

  int FindItemByStateIx(int stateIx);
  
//  int InsertItem(DWORD flags, DWORD color, const char *text, float width, float height, void *data=NULL);
//  void NewItem();
//  ItemInfo * GetItemExtraData(int itemid);
//  SDLinkInfo * GetLinkExtraData(int itemid);
//  void ResetContent();
//  virtual void OnLinkItems(int beginitem, int enditem);
//  virtual void OnChangeLink(int lnk, int beginitem, int enditem, int newitem, void *data);
//  virtual void OnUnlinkItems(int lnk, int beginitem, int enditem, void *data);
//  virtual void OnTouchLink(int lnk, int beginitem, int enditem, void *data) 
//  {_curlink=lnk;}
//  virtual bool OnCustomDraw(int item, SGraphItem &itm, CDC &dc, CRect &rc);
//  virtual bool OnCustomDrawLink(int lnk, int beginitem, int enditem, int order, void *data, CDC &dc, CRect rc) ;
//  virtual void OnResizeItem(int item, int corner, float xs, float ys) {}
//  virtual void OnMoveItem(int item, float xs, float ys) {Update();UpdateWindow();}
//  {SaveUndo();}
//  virtual void OnLinkNewItem(int beginitem, float x, float y);
//
//  bool Save(const char * filename);
//  bool Load(const char * filename);
//  void SaveUndo(void);
};

#endif

//
// The following is not possible, as union members mustnot have the copy constructor (CString has)
// It was replaced by implementation using virtual interface
//
//struct FSMCondition
//{
//  float priority;
//  CString condition;
//  CString action;
//};
//struct FSMState
//{
//  float priority;
//  CString initCode;
//};
//class FSMItemInfo
//{
//private:
//  union {
//    FSMCondition condition;
//    FSMState state;
//  } _data;
//public:
//  int id;
//
//  FSMCondition &condition;        //alias to union member _data.condition
//  struct FSMState &state;         //alias to union member _data.state
//  FSMItemInfo(): condition(_data.condition), state(_data.state) {}
//};
