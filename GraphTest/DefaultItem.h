// DefaultItem.h ... default item (shapes) handling
//
//////////////////////////////////////////////////////////////////////

#if !defined(_DEFAULTITEM_H)
#define _DEFAULTITEM_H

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "GraphCtrlExt.h"

class DefaultItem
{
  friend class DefaultItems;
  SGraphItem item;
public:
  DefaultItem();
  DefaultItem(SGraphItem &grItem);   //deep constructor
  ~DefaultItem();

/*
  struct SelFeatures //only size stands to reason
  {
    unsigned int size:1;      //size of rectangle sides
    unsigned int flags:1;     //SGRI_CANMAKELINKS, SGRI_CANBELINKED, etc  ALWAYS
    unsigned int color:1;     //background color ALWAYS
    //unsigned int text:1;    //basic CGraphCtrl::text (not text under CGraphCtrlExt)
    unsigned int shape:1;     //ALWAYS
    //line properties (border) ALWAYS
    unsigned int lineColor:1; 
    unsigned int lWidth;
    unsigned int lStyle;
    //text align ALWAYS
    unsigned int textAlign:1;
    //font properties ALWAYS
    unsigned int lgFont:1;
    unsigned int fontColor:1;
    unsigned int ffHeight;
    unsigned int ffWidth;
    //text NEVER
    unsigned int text:1;
  };
*/
};

typedef enum
{ 
  DF_UNDEF=-1, DF_START_ITEM, DF_END_ITEM, DF_ITEM, DF_USER_ITEM, 
  DF_CONDITION, DF_USER_INPUT_CONDITION, DF_USER_CONDITION,
  DF_KNEE, DF_TRUE_CONDITION, DF_DEFAULT_ITEM_NUM
} DefaultItemType;

class FSMEditor;
class DefaultItems : public SerializeClass
{
  FSMEditor &wGraph;
  DefaultItem item[DF_DEFAULT_ITEM_NUM];
public:
  ///Constructor must specify underlying CGraphCtrlExt
  DefaultItems(FSMEditor &graph);
  ///Does not copy all data (eg: text)
  void ChangeToDefaultItem(DefaultItemType itemType, SGraphItem &grItem); 
  ///Inserts default item to fsm editor
  void InsertDefaultItem(DefaultItemType itemType, float x, float y);
  ///change default item appearance to item with index itm
  void SetDefaultItem(DefaultItemType itemType, int itm);
  ///Save to the file filename
  bool Save(RString filename);
  ///Load from the file filename
  bool Load(RString filename);
  ///Serialize to ParamArchive
  LSError Serialize(ParamArchive &ar);
  bool Save(const char * filename);
  bool Load(const char * filename);
  ///Load defaults from default config
  void LoadDefaults();
};

#endif