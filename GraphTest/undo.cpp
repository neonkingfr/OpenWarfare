#include "StdAfx.h"
#include "undo.h"

UndoRedo::UndoRedo(int max) : maxCount(max)
{
  undoBuf = new Ref<ParamArchiveSaveUndo>[maxCount];
  for (int i=0; i<maxCount; i++) undoBuf[i]=NULL;
  ix = redoIx = undoCount = 0;
}

UndoRedo::~UndoRedo()
{
  for (int i=0; i<maxCount; i++)
    if (undoBuf[i]) undoBuf[i]=NULL;
  delete[] undoBuf;
}

void UndoRedo::SaveUndo(ParamArchiveSaveUndo *arch)
{
  if (++undoCount>maxCount) undoCount--; //cyclic
  if (undoBuf[ix]) undoBuf[ix]=NULL;
  undoBuf[ix]=arch;
  ix=redoIx=(ix+1)%maxCount;
}

Ref<ParamArchiveSaveUndo> UndoRedo::Undo()
{
  Ref<ParamArchiveSaveUndo> retval=NULL;
  if (undoCount>0)
  {
    ix=(ix+maxCount-1)%maxCount;
    undoCount--;
    retval=undoBuf[ix];
  }
  return retval;
}

Ref<ParamArchiveSaveUndo> UndoRedo::Redo()
{
  Ref<ParamArchiveSaveUndo> retval=NULL;
  if (redoIx!=ix)
  {
    undoCount++;
    ix=(ix+1)%maxCount;
    retval=undoBuf[ix];
  }
  return retval;
}

void UndoRedo::ClearUndos()
{
  for (int i=0; i<maxCount; i++) undoBuf[i]=NULL;
  ix = redoIx = undoCount = 0;
}
