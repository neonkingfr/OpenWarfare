#ifndef _UNDO_H
#define _UNDO_H

#include <el/ParamArchive/paramArchiveDb.hpp>

class ParamArchiveSaveUndo : public RefCount, public ParamArchiveSave
{
public:
  ParamArchiveSaveUndo(int ver) : ParamArchiveSave(ver) {}
};

class UndoRedo
{
public:
  const int maxCount;    //number of undo items
  Ref<ParamArchiveSaveUndo> *undoBuf;
  int ix;             //current ix (points to empty place)
  int redoIx;         //current redoIx (redo can be done as far as this index)
  int undoCount;      //number of undo values
  UndoRedo(int max);
  ~UndoRedo();
  void SaveUndo(ParamArchiveSaveUndo *arch);
  Ref<ParamArchiveSaveUndo> Undo();
  Ref<ParamArchiveSaveUndo> Redo();
  bool RedoUninitialized() {return redoIx==ix;}
  void DecrementRedo()
  {
    if (undoCount>0) redoIx=(redoIx+maxCount-1)%maxCount;
  }
  void ClearUndos();
};

#endif
