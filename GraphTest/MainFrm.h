// MainFrm.h : interface of the CMainFrame class
//
/////////////////////////////////////////////////////////////////////////////

#if !defined(AFX_MAINFRM_H__52D46D01_7EF9_4CCC_A7C3_F16833820380__INCLUDED_)
#define AFX_MAINFRM_H__52D46D01_7EF9_4CCC_A7C3_F16833820380__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "FSMEditor.h"
#include "res/maintlb.h"
#include "res/designtlb.h"
#include "res/DlgBarFont.h"
//#include "StateDataDlg.h"
//#include "ConditionDataDlg.h"
#include "FSMDataDlgs.h"
#include "SplitWindow.h"
#include "CompileResultDlg.h"
#include "DebugFSMLogDlg.h"

#define MSG_STARTUP_UPDATE (WM_APP+770)
class CMainFrame : public CFrameWnd
{
private:
  CString _basicWindowText;
public:
  CSplitWindow *split;
  CString curfname;
  FSMEditor View;
  //CEdit TextEdit;
  //CConditionDataDlg TextEdit;
  FSMDataDlgs TextEdit;
  CompileResultDlg *compResultDlg;
  DebugFSMLogDlg *debugFSMLogDlg;
  CMainFrmToolbar *wToolBar;
  CDesignToolbar *wDesignToolbar;
  CStatusBar wStatusBar;
  DlgBarFont wFontBar;
  CMainFrame();
protected: 
	DECLARE_DYNAMIC(CMainFrame)

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMainFrame)
	virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
	virtual BOOL OnCmdMsg(UINT nID, int nCode, void* pExtra, AFX_CMDHANDLERINFO* pHandlerInfo);
  virtual BOOL PreTranslateMessage(MSG* pMsg);
  //}}AFX_VIRTUAL

// Implementation
public:
	void ResetZoom();
  void UpdateCaption();
	virtual ~CMainFrame();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif
virtual LRESULT WindowProc(UINT message, WPARAM wParam, LPARAM lParam);

// Generated message map functions
protected:
	//{{AFX_MSG(CMainFrame)
	afx_msg void OnSetFocus(CWnd *pOldWnd);
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
  afx_msg void OnClose();
	//afx_msg void OnEditInsertitem();
	afx_msg void OnEditDeleteitems();
	afx_msg void OnUpdateEditDeleteitems(CCmdUI* pCmdUI);
	afx_msg void OnPropertiesEdititem();
	afx_msg void OnUpdatePropertiesEdititem(CCmdUI* pCmdUI);
	afx_msg void OnContextMenu(CWnd* pWnd, CPoint point);
	afx_msg void OnUpdatePropertiesCanbelinked(CCmdUI* pCmdUI);
	afx_msg void OnUpdatePropertiesCanmakelinks(CCmdUI* pCmdUI);
	afx_msg void OnUpdatePropertiesCanmove(CCmdUI* pCmdUI);
	afx_msg void OnUpdatePropertiesCanselflinked(CCmdUI* pCmdUI);
	afx_msg void OnUpdatePropertiesCansize(CCmdUI* pCmdUI);
	afx_msg void OnUpdatePropertiesCustomdraw(CCmdUI* pCmdUI);
	afx_msg void OnPropertiesCustomdraw();
	afx_msg void OnPropertiesCansize();
	afx_msg void OnPropertiesCanselflinked();
	afx_msg void OnPropertiesCanmove();
	afx_msg void OnPropertiesCanmakelinks();
	afx_msg void OnPropertiesCanbelinked();
	afx_msg void OnLinkNormal();
	afx_msg void OnLinkDesign();
	afx_msg void OnLinkZoom();
	afx_msg void OnUpdateLinkNormal(CCmdUI* pCmdUI);
	afx_msg void OnUpdateLinkZoom(CCmdUI* pCmdUI);
	afx_msg void OnUpdateLinkDesign(CCmdUI* pCmdUI);
  //Messages from CGraphCtrlExt
  afx_msg LPARAM OnNotifyEndSel(WPARAM,LPARAM);
  //Messages from DlgFontBar
  afx_msg LPARAM OnBoolStyleChanged(WPARAM wParam,LPARAM lParam);
  afx_msg LPARAM OnFontChanged(WPARAM wParam,LPARAM lParam);
  afx_msg LPARAM OnFontColorChanged(WPARAM wParam,LPARAM lParam);
  afx_msg LPARAM OnTextAlignChanged(WPARAM wParam,LPARAM lParam);
  //design menu
  afx_msg void OnDesignInsertnew(); 
  //messages from GraphCtrlExt	
  afx_msg LPARAM OnWndSerialize(WPARAM wParam,LPARAM lParam);
  afx_msg LPARAM OnDirtinessChanged(WPARAM wParam,LPARAM lParam);
  afx_msg LPARAM OnMsgAttrEdit(WPARAM wParam,LPARAM lParam);
  afx_msg LPARAM OnPrepareForSaving(WPARAM wParam,LPARAM lParam);
  //message from CompileResultDlg
  afx_msg LPARAM OnFocusItem(WPARAM wParam, LPARAM lParam);
  //}}AFX_MSG
  //Messages from link popup menu
  afx_msg void OnLinkUselastsettings();
  afx_msg void OnLinkChangecolor();
  afx_msg void OnLinkArrowtype();
  afx_msg void OnDiagramInactive();
  afx_msg void OnUpdateDiagramInactive(CCmdUI *pCmdUI);
  DECLARE_MESSAGE_MAP()
public:
  // Design menu: Default item load/save
  afx_msg void OnDesignState();
  afx_msg void OnDesignUserState();
  afx_msg void OnDesignCondition();
  afx_msg void OnDesignInputcondition();
  afx_msg void OnDesignUsercondition();
  afx_msg void OnDesignStartstate();
  afx_msg void OnDesignEndstate();
  afx_msg void OnDesignKnee();
  void DesignLoadFromDefault(DefaultItemType type);
  afx_msg void OnSetasdefaultState();
  afx_msg void OnSetasdefaultUserstate();
  afx_msg void OnSetasdefaultCondition();
  afx_msg void OnSetasdefaultInputcondition();
  afx_msg void OnSetasdefaultUsercondition();
  afx_msg void OnSetasdefaultStartstate();
  afx_msg void OnSetasdefaultEndstate();
  afx_msg void OnSetasdefaultKnee();
  void DesignSetasdefault(DefaultItemType type);
  afx_msg void OnDefaultsLoad();
  afx_msg void OnDefaultsSaveas();
  // Dialog PageSetup
  afx_msg void OnFilePagesetup();
  afx_msg void OnDesignTruecondition();
  afx_msg void OnSetasdefaultTruecondition();
  afx_msg void OnChangeactivewnd();
  afx_msg void OnFileCompile();
  afx_msg LPARAM OnInitialUpdate(WPARAM,LPARAM);
  //fsmattributes menu commands
  afx_msg void OnFsmattributesFsmname();
  afx_msg void OnFsmattributesCompileconfig();
  afx_msg void OnFsmattributesAttributesfromcompileconfig();
  //options menu commands
  afx_msg void OnChangeFont();

  afx_msg BOOL OnDebugFSMLogCheck(UINT nID);

  void OnStateChanged(WPARAM,LPARAM);
  LPARAM OnStateChanged2(WPARAM,LPARAM);
  afx_msg void OnOptionsSplitvertical();
  afx_msg void OnUpdateOptionsSplitvertical(CCmdUI *pCmdUI);
};

//global functions and variables
//! array for Compile using FSMCompiler.exe
extern char * Compileargv[];
//! fill the array item Compileargv[0] by fsmCompiler.exe path
extern bool LoadFSMCompilerArgv();

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MAINFRM_H__52D46D01_7EF9_4CCC_A7C3_F16833820380__INCLUDED_)
