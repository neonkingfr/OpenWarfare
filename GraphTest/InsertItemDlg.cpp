// InsertItemDlg.cpp : implementation file
//

#include "stdafx.h"
#include "GraphView.h"
#include "InsertItemDlg.h"
#include "GraphCtrl.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CInsertItemDlg dialog


CInsertItemDlg::CInsertItemDlg(CWnd* pParent /*=NULL*/)
: CDialog(CInsertItemDlg::IDD, pParent)
{
  //{{AFX_DATA_INIT(CInsertItemDlg)
  m_canbelinked = FALSE;
  m_canlinkself = FALSE;
  m_canmakelinks = FALSE;
  m_canmove = FALSE;
  m_canresize = FALSE;
  m_customdraw = FALSE;
  m_itemtext = _T("");
  m_dimx = 0.0f;
  m_dimy = 0.0f;
  m_colortxt = _T("");
  //}}AFX_DATA_INIT
}


void CInsertItemDlg::DoDataExchange(CDataExchange* pDX)
{
  CDialog::DoDataExchange(pDX);
  //{{AFX_DATA_MAP(CInsertItemDlg)
  DDX_Check(pDX, IDC_CANBELINKED, m_canbelinked);
  DDX_Check(pDX, IDC_CANLINKSELF, m_canlinkself);
  DDX_Check(pDX, IDC_CANMAKELINKS, m_canmakelinks);
  DDX_Check(pDX, IDC_CANMOVE, m_canmove);
  DDX_Check(pDX, IDC_CANRESIZE, m_canresize);
  DDX_Check(pDX, IDC_CUSTOMDRAW, m_customdraw);
  DDX_Text(pDX, IDC_ITEMTEXT, m_itemtext);
  DDX_Text(pDX, IDC_DIMX, m_dimx);
  DDV_MinMaxFloat(pDX, m_dimx, 0.f, 99999.f);
  DDX_Text(pDX, IDC_DIMY, m_dimy);
  DDV_MinMaxFloat(pDX, m_dimy, 0.f, 99999.f);
  DDX_Text(pDX, IDC_COLOR, m_colortxt);
  DDV_MaxChars(pDX, m_colortxt, 6);
  //}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CInsertItemDlg, CDialog)
  //{{AFX_MSG_MAP(CInsertItemDlg)
  ON_BN_CLICKED(IDC_PICK, OnPick)
  //}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CInsertItemDlg message handlers

BOOL CInsertItemDlg::OnInitDialog() 
{
  m_itemtext=text;
  m_colortxt.Format("%06X",color);
  m_canbelinked=(flags & SGRI_CANBELINKED)!=0;
  m_canlinkself=(flags & SGRI_CANSELFLINKED )!=0;
  m_canmakelinks=(flags & SGRI_CANMAKELINKS)!=0;
  m_canmove=(flags & SGRI_CANMOVE)!=0;
  m_canresize=(flags & SGRI_CANRESIZE)!=0;
  m_customdraw=(flags & SGRI_CUSTOMDRAW)!=0;  
  CDialog::OnInitDialog();

  // TODO: Add extra initialization here

  return TRUE;  // return TRUE unless you set the focus to a control
  // EXCEPTION: OCX Property Pages should return FALSE
}

void CInsertItemDlg::OnOK() 
{
  CDialog::OnOK();
  text=m_itemtext;
  flags=0;
  if (m_canbelinked) flags |=SGRI_CANBELINKED;
  if (m_canmakelinks) flags |=SGRI_CANMAKELINKS;
  if (m_canlinkself) flags |=SGRI_CANSELFLINKED;
  if (m_canmove) flags |= SGRI_CANMOVE;
  if (m_canresize) flags |= SGRI_CANRESIZE;
  if (m_customdraw) flags |=SGRI_CUSTOMDRAW;
  sscanf(m_colortxt,"%X",&color);

}



void CInsertItemDlg::OnPick() 
{
  static CColorDialog dlg(0,CC_ANYCOLOR|CC_RGBINIT|CC_FULLOPEN);
  sscanf(m_colortxt,"%X",&dlg.m_cc.rgbResult);
  if (dlg.DoModal()==IDOK)
  {
    m_colortxt.Format("%06X",dlg.GetColor());
    SetDlgItemText(IDC_COLOR,m_colortxt);
  }
}




