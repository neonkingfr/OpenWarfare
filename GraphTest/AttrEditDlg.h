#pragma once
#include "FSMEditor.h"
#include "afxwin.h"

// CAttrEditDlg dialog

class CAttrEditDlg : public CDialog
{
	DECLARE_DYNAMIC(CAttrEditDlg)

  FSMEditor *_fsm;
public:
	CAttrEditDlg(CWnd* pParent = NULL, FSMEditor *fsm = NULL);   // standard constructor
	virtual ~CAttrEditDlg();

// Dialog Data
	enum { IDD = IDD_ATTR_EDIT_DLG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
  CString attrEdit;
  afx_msg void OnSize(UINT nType, int cx, int cy);
  afx_msg void OnClose();
  virtual BOOL OnInitDialog();
  CEdit editCtrl;
protected:
  virtual void OnCancel();
};
