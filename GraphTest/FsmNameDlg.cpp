// FsmNameDlg.cpp : implementation file
//

#include "stdafx.h"
#include "FSMEditor.h"
#include "FsmNameDlg.h"
#include ".\fsmnamedlg.h"


// FsmNameDlg dialog

IMPLEMENT_DYNAMIC(FsmNameDlg, CDialog)
FsmNameDlg::FsmNameDlg(CWnd* pParent /*=NULL*/)
	: CDialog(FsmNameDlg::IDD, pParent)
  , fsmName(_T(""))
{
}

FsmNameDlg::~FsmNameDlg()
{
}

void FsmNameDlg::DoDataExchange(CDataExchange* pDX)
{
  CDialog::DoDataExchange(pDX);
  DDX_Text(pDX, IDC_FSMNAME, fsmName);
}


BEGIN_MESSAGE_MAP(FsmNameDlg, CDialog)
END_MESSAGE_MAP()


// FsmNameDlg message handlers

// CompileConfigDlg dialog

IMPLEMENT_DYNAMIC(CompileConfigDlg, CDialog)
CompileConfigDlg::CompileConfigDlg(CWnd* pParent /*=NULL*/)
: CDialog(CompileConfigDlg::IDD, pParent)
, configFile(_T(""))
{
}

CompileConfigDlg::~CompileConfigDlg()
{
}

void CompileConfigDlg::DoDataExchange(CDataExchange* pDX)
{
  CDialog::DoDataExchange(pDX);
  DDX_Text(pDX, IDC_CONFIGFILEEDIT, configFile);
}


BEGIN_MESSAGE_MAP(CompileConfigDlg, CDialog)
  ON_BN_CLICKED(IDC_CONFIG_BROWSE, OnBnClickedConfigBrowse)
END_MESSAGE_MAP()


// CompileConfigDlg message handlers

#include "SaveLoadDlgs.h"
void CompileConfigDlg::OnBnClickedConfigBrowse()
{
  // TODO: Add your control notification handler code here
  UpdateData();
  CString defext;defext.LoadString(IDS_CONFIGEXT);
  CString filter;filter.LoadString(IDS_CONFIGFILTER);
  CString name;name.LoadString(IDS_CONFIGTITLE);
  CSelectFileDialog sfdlg(TRUE,defext,configFile,OFN_PATHMUSTEXIST|OFN_OVERWRITEPROMPT|OFN_HIDEREADONLY,filter);
  sfdlg.m_ofn.lpstrTitle=name;
  if (sfdlg.DoModal()==IDOK)
  {
    configFile=sfdlg.GetPathName();
    UpdateData(false);
  }
}

// FsmAttributesDlg : implementation file
//

#include "stdafx.h"
#include "FSMEditor.h"

// FsmAttributesDlg dialog

IMPLEMENT_DYNAMIC(FsmAttributesDlg, CDialog)
FsmAttributesDlg::FsmAttributesDlg(RString compileConfig, CWnd* pParent /*=NULL*/)
: CDialog(FsmAttributesDlg::IDD, pParent)
, attributeName(_T(""))
, _compileConfig(compileConfig)
{
}

FsmAttributesDlg::~FsmAttributesDlg()
{
}

void FsmAttributesDlg::DoDataExchange(CDataExchange* pDX)
{
  CDialog::DoDataExchange(pDX);
  DDX_Control(pDX, IDC_LIST1, attributesList);
  DDX_LBString(pDX, IDC_LIST1, attributeName);
}


BEGIN_MESSAGE_MAP(FsmAttributesDlg, CDialog)
  ON_LBN_DBLCLK(IDC_LIST1, OnLbnDblclkList1)
END_MESSAGE_MAP()

// FsmAttributesDlg message handlers

#include "El/Pathname/Pathname.h"
#include "el/QStream/QBStream.hpp"
static const int BisFSMPathCount = 1;
static char *BisFSMPath[BisFSMPathCount] = {"c:\\bis\\fsmeditor\\fsmeditor.exe"};
RString FindFilePath(RString fname)
{ 
  if (QFBankQueryFunctions::FileExists(fname)) return fname; //use file in the current directory
  else //try directory of exeFile
  {
    RString fileNameOnly = Pathname(fname).GetFilename();
    if (QFBankQueryFunctions::FileExists(fileNameOnly)) return fileNameOnly;
    Pathname exepath = Pathname::GetExePath();
    exepath.SetFilename(fileNameOnly);
    const char *path = exepath.GetFullPath();
    if (QFBankQueryFunctions::FileExists(path)) return path;
    else //use file on path c:\bis\...
    {
      for (int i=0; i<BisFSMPathCount; ++i)
      {
        Pathname comppath(BisFSMPath[i]);
        comppath.SetFilename(fileNameOnly);
        if (QFBankQueryFunctions::FileExists(comppath)) return comppath.GetFullPath();
      }
    }
  }
  return RString("");
}

#include "el/ParamFile/paramFile.hpp"
BOOL FsmAttributesDlg::OnInitDialog()
{
  CDialog::OnInitDialog();

  // TODO:  Add extra initialization here
  
  //open and parse compileConfig to get the names of specific attributes
  RString configPath = FindFilePath(_compileConfig);
  if (configPath!=RString(""))
  {
    ParamFile compileConfig;
    if (compileConfig.Parse(configPath)==LSOK)
    {
      ParamEntryVal attributes = compileConfig >> "Attributes" >> "names";
      if (attributes.IsArray())
      {
        int n = attributes.GetSize();
        for (int i=0; i<n; ++i)
          if (attributes[i].IsTextValue()) attributesList.AddString(attributes[i].GetValue().Data());
      }
    }
  }

  return TRUE;  // return TRUE unless you set the focus to a control
  // EXCEPTION: OCX Property Pages should return FALSE
}

void FsmAttributesDlg::OnLbnDblclkList1()
{
  // TODO: Add your control notification handler code here
  UpdateData();
  OnOK();
}

// ExternalEditorDlg dialog

IMPLEMENT_DYNAMIC(ExternalEditorDlg, CDialog)
ExternalEditorDlg::ExternalEditorDlg(CWnd* pParent /*=NULL*/)
: CDialog(ExternalEditorDlg::IDD, pParent)
, externalEditor(_T("")), useDDE(false), serviceDDE(_T(""))
{
}

ExternalEditorDlg::~ExternalEditorDlg()
{
}

void ExternalEditorDlg::DoDataExchange(CDataExchange* pDX)
{
  CDialog::DoDataExchange(pDX);
  DDX_Text(pDX, IDC_EDIT1, externalEditor);
  DDX_Text(pDX, IDC_EDIT2, serviceDDE);
  DDX_Check(pDX, IDC_DDE, useDDE);
}


BEGIN_MESSAGE_MAP(ExternalEditorDlg, CDialog)
END_MESSAGE_MAP()
