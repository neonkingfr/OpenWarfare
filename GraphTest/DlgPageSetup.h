#pragma once
#include "afxwin.h"



// DlgPageSetup form view


class DlgPageSetup : public CDialog
{
	DECLARE_DYNAMIC(DlgPageSetup)

public:
	DlgPageSetup(CWnd* pParent = NULL);   // standard constructor
	virtual ~DlgPageSetup();

// Dialog Data
	enum { IDD = IDD_PAGESETUP };
  CButton wForceColor;
  BOOL vForceColor;
  CButton wForceDim;
  BOOL vForceDim;
  UINT vImageX;
  UINT vImageY;
  CEdit wImageX;
  CEdit wImageY;
  COLORREF vColor;
  CButton wColorButt;

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support


	DECLARE_MESSAGE_MAP()
public:
  virtual BOOL OnInitDialog();
  void DialogRules(void);
  afx_msg void OnBnClickedForcebkcolor();
  afx_msg void OnBnClickedForceimagedim();
  afx_msg void OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct);
  void OnDrawColorButton(LPDRAWITEMSTRUCT lpDis);
  afx_msg void OnBnClickedSelectcolor();
  afx_msg void OnBnClickedEnablegrid();
  CButton wShowGrid;
  BOOL vShowGrid;
  float vGrid;
  CButton wEnableGrid;
  BOOL vEnableGrid;
  CEdit wGrid;
public:
  afx_msg void OnBnClickedSavePage();
};
