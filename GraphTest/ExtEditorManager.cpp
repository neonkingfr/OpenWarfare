#include "ExtEditorManager.h"
#include "MainFrm.h"
#include "FSMEditor.h"
#include "DevEnvDDE.h"
#include <io.h>
#include <sys/stat.h>

void DataFileItem::DeleteFile()
{
  chmod(fileName,_S_IREAD | _S_IWRITE);
  unlink(fileName);
}

void DataFileItem::SaveData(FSMEditor *wGraph)
{
  //Determine whether item is state or condition
  FSMItemInfo *info = wGraph->GetItemExtraDataById(id);
  CString *saveInto = NULL;
  if (info->fsmData->GetType()==FSMStateType)
  {
    switch (selectedTab)
    {
    case 0: //initCode
      saveInto = &(const_cast<FSMState *>(info->fsmData->GetFSMState()))->initCode;
      break;
    case 1: //preCondition
      saveInto = &(const_cast<FSMState *>(info->fsmData->GetFSMState()))->preCondition;
      break;
    }
  }
  else //condition type
  {
    switch (selectedTab)
    {
    case 0: //condition
      saveInto = &(const_cast<FSMCondition *>(info->fsmData->GetFSMCondition()))->condition;
      break;
    case 1: //action
      saveInto = &(const_cast<FSMCondition *>(info->fsmData->GetFSMCondition()))->action;
      break;
    case 2: //preCondition
      saveInto = &(const_cast<FSMCondition *>(info->fsmData->GetFSMCondition()))->preCondition;
      break;
    }
  }
  //Collect data from the file and save it
  FILE *in = fopen(fileName, "rb");
  if (in)
  {
    int len = QIFileFunctions::GetFileSize(fileName);
    Temp<char> buf;
    buf.Realloc(len+1); buf[len]=0;
    fread(buf, 1, len, in);
    fclose(in);
    if (saveInto) *saveInto = buf; //saved at last
  }
}

void ExtEditorFileManager::Close()
{
  for (int i=0; i<_files.Size(); i++)
  {
    if (_files[i])
    {
      _files[i]->DeleteFile();
      // close the handles, we no longer need them
      if (_files[i]->hprocess) {::CloseHandle(_files[i]->hprocess); _files[i]->hprocess=0; }
      if (_files[i]->hthread) {::CloseHandle(_files[i]->hthread); _files[i]->hthread=0; }
      _files[i]=NULL;
    }
  }
}

bool ExtEditorFileManager::IsEdited(int id, int tab)
{
  Update();
  extern BOOL gUseDDE;
  if (gUseDDE) return false; // DDE Edited files cannot be tested by this >;-(
  for (int i=0; i<_files.Size(); i++)
  {
    if (_files[i] && _files[i]->id==id && _files[i]->selectedTab==tab) return true;
  }
  return false;
}

ExtEditorFileManager::~ExtEditorFileManager()
{
  Close();
}

void ExtEditorFileManager::Init(FSMEditor *fsm)
{
  if (wGraph)
  { //we should delete everything created by last one
    Close();
  }
  wGraph = fsm;
}

void ExtEditorFileManager::Update()
{
  if (!wGraph) return; //not initialized yet
  ((CMainFrame *)AfxGetMainWnd())->TextEdit.SaveData(); //save current state
  for (int i=0; i<_files.Size(); i++)
  {
    if (_files[i])
    {
      if (WAIT_OBJECT_0==WaitForSingleObject(_files[i]->hprocess,0))
      { //signaled
        ::CloseHandle(_files[i]->hprocess);
        ::CloseHandle(_files[i]->hthread);
        _files[i]->SaveData(wGraph);
        _files[i]->DeleteFile();
        _files[i]=NULL;
      }
      else
      { //we should test, whether the file time stamp has changed or not
        HANDLE check=::CreateFile
          (
          _files[i]->fileName,GENERIC_READ,FILE_SHARE_READ,
          NULL,OPEN_EXISTING,0,NULL
          );
        if( check==INVALID_HANDLE_VALUE ) continue;
        FILETIME ftime;
        ::GetFileTime(check, NULL, NULL, &ftime);
        if (ftime.dwHighDateTime!=_files[i]->time.dwHighDateTime || ftime.dwLowDateTime!=_files[i]->time.dwLowDateTime)
        { //the file write time has changed
          _files[i]->SaveData(wGraph);
        }
        ::CloseHandle(check);
      }
    }
  }
  wGraph->RefreshDataDlg(true); //refresh to take possible changes into account
}

void ExtEditorFileManager::Submit(int ix, int tab, RString fname)
{
  DataFileItem* item = new DataFileItem;
  item->id = ix;
  item->selectedTab = tab;
  item->fileName = fname;
  HANDLE check=::CreateFile
    (
    fname,GENERIC_READ,FILE_SHARE_READ,
    NULL,OPEN_EXISTING,0,NULL
    );
  if( check==INVALID_HANDLE_VALUE ) return; //no success
  ::GetFileTime(check, NULL, NULL, &item->time);
  ::CloseHandle(check);

  // try DDE first
  extern BOOL gUseDDE;
  extern CString gServiceDDE;
  bool done = false;
  if (gUseDDE && !gServiceDDE.IsEmpty())
  {
    if (MSDEV_OpenFile(cc_cast(fname), gServiceDDE.GetString()))
    {
      item->hprocess = item->hthread = 0;
      done = true;
    }
  }

  if (!done)
  {
    // run the external editor
    extern CString gExternalEditor;
    RString cmdLine = Format("\"%s\" \"%s\"", gExternalEditor.GetString(), cc_cast(fname));

    STARTUPINFO si;
    PROCESS_INFORMATION pi;

    ZeroMemory( &si, sizeof(si) );
    si.cb = sizeof(si);
    ZeroMemory( &pi, sizeof(pi) );
    // Start the child process. 
    if( !CreateProcess( NULL,  // Module name
      cmdLine.MutableData(),          // Command line. 
      NULL,             // Process handle not inheritable. 
      NULL,             // Thread handle not inheritable. 
      FALSE,            // Set handle inheritance to FALSE. 
      0,                // No creation flags. 
      NULL,             // Use parent's environment block. 
      NULL,             // Use parent's starting directory. 
      &si,              // Pointer to STARTUPINFO structure.
      &pi )             // Pointer to PROCESS_INFORMATION structure.
      ) 
    {
      return;
    }
    item->hprocess = pi.hProcess;
    item->hthread = pi.hThread;
  }

  // add item into _files array
  for (int i=0, siz=_files.Size(); i<siz; i++)
  {
    if (_files[i]==NULL) 
    {
      _files[i]=item;
      return; //Done
    }
  }
  _files.Add(item);
}

//global variable
ExtEditorFileManager gExtEdFileManager;
