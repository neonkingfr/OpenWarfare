#pragma once

#include "FSMEditor.h"
#include "afxwin.h"

// CSelectFileDialog

//! custom CFileDialog in order to use SetControlText
class CSelectFileDialog : public CFileDialog
{
public:
  explicit CSelectFileDialog(
    BOOL bOpenFileDialog,
    LPCTSTR lpszDefExt = NULL,
    LPCTSTR lpszFileName = NULL,
    DWORD dwFlags = OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT,
    LPCTSTR lpszFilter = NULL,
    CWnd* pParentWnd = NULL,
    DWORD dwSize = 0
    ) : CFileDialog(bOpenFileDialog, lpszDefExt, lpszFileName, dwFlags, lpszFilter, pParentWnd, dwSize)
  {}
  virtual void OnInitDone( ) 
  {
    SetControlText(IDOK,"Select");
  }
};

// SaveAsDlg dialog

class SaveAsDlg : public CDialog
{
	DECLARE_DYNAMIC(SaveAsDlg)

public:
  CString  &curFileName;
  CGraphCtrlExt &wGraph;
  
  SaveAsDlg(CGraphCtrlExt &graph, CString &curfnm, CWnd* pParent = NULL);   // standard constructor
	virtual ~SaveAsDlg();

// Dialog Data
	enum { IDD = IDD_SAVEAS };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
  CString fileName, fullPath;
  CString fsmName;

  //MFC methods
  virtual BOOL OnInitDialog();
  afx_msg void OnBnClickedBrowseFilenameSa();
  afx_msg void OnBnClickedOk();
  afx_msg void OnEnKillfocusFilenameSa();
};

// SelectFSMNameDlg dialog

class SelectFSMNameDlg : public CDialog
{
  DECLARE_DYNAMIC(SelectFSMNameDlg)
private:
  QOStrStream & _fsmNames;

public:
  SelectFSMNameDlg(QOStrStream &fsmNames, CWnd* pParent = NULL);   // standard constructor
  virtual ~SelectFSMNameDlg();

  // Dialog Data
  enum { IDD = IDD_SELECT_FSMNAME_DLG };

protected:
  virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

  DECLARE_MESSAGE_MAP()
public:
  // selected FSM name
  CString fsmName;
  // listControlVariable
  CListBox fsmNameList;
  int fsmNameCount;
  virtual BOOL OnInitDialog();
  afx_msg void OnLbnDblclkList1();
};
