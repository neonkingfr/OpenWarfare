#pragma once


// DebugFSMLogDlg dialog

class DebugFSMLogDlg : public CDialog
{
	DECLARE_DYNAMIC(DebugFSMLogDlg)

public:
	DebugFSMLogDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~DebugFSMLogDlg();

// Dialog Data
	enum { IDD = IDD_DEBUG_FSM_LOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

  int GetLastValidItemIx(int lnNum, int &lnIx);
  int GetItemIx(int lnNum);

	DECLARE_MESSAGE_MAP()
public:
  // FSM Log read from debug.log file
  CString fsmLogText;
  // FSM Id to distinguish between more possible FSM logs inside one logfile
  CString fsmId;

  afx_msg void OnSize(UINT nType, int cx, int cy);
  afx_msg void OnClose();
  virtual BOOL PreTranslateMessage(MSG* pMsg);
  virtual BOOL OnInitDialog();

  afx_msg void OnChangeFSMId();
  afx_msg void OnBnClickedBrowse();
  afx_msg void OnBnClickedButtonPrev();
  afx_msg void OnBnClickedButtonNext();
};
