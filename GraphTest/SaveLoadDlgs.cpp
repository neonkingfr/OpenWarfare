// SaveLoadDlgs.cpp : implementation file
//

#include "stdafx.h"
#include "FSMEditor.h"
#include "SaveLoadDlgs.h"
#include "el/Pathname/Pathname.h"

// SaveAsDlg dialog

IMPLEMENT_DYNAMIC(SaveAsDlg, CDialog)
SaveAsDlg::SaveAsDlg(CGraphCtrlExt &graph, CString &curfnm, CWnd* pParent /*=NULL*/)
	: CDialog(SaveAsDlg::IDD, pParent)
  , curFileName(curfnm)
  , wGraph(graph)
{
  Pathname path(curfnm);
  fileName=path.GetFilename();
  fullPath=path.GetFullPath();
  fsmName = static_cast<FSMEditor &>(wGraph).fsmName;
}

SaveAsDlg::~SaveAsDlg()
{
}

void SaveAsDlg::DoDataExchange(CDataExchange* pDX)
{
  CDialog::DoDataExchange(pDX);
  DDX_Text(pDX, IDC_FILENAME_SA, fileName);
  DDX_Text(pDX, IDC_FSMNAME_SA, fsmName);
}

BEGIN_MESSAGE_MAP(SaveAsDlg, CDialog)
  ON_BN_CLICKED(IDC_BROWSE_FILENAME_SA, OnBnClickedBrowseFilenameSa)
  ON_BN_CLICKED(IDOK, OnBnClickedOk)
  ON_EN_KILLFOCUS(IDC_FILENAME_SA, OnEnKillfocusFilenameSa)
END_MESSAGE_MAP()

// SaveAsDlg message handlers

//#include "res/maintlbresource.h"
#include "resource.h"
#include ".\saveloaddlgs.h"
#include "es/Files/filenames.hpp"
void SaveAsDlg::OnBnClickedBrowseFilenameSa()
{
  // TODO: Add your control notification handler code here
  UpdateData(); 
  CString defext; defext.LoadString(IDS_DIAGRAMDEFEXT);
  CString filter;filter.LoadString(IDS_DIAGRAMFILTER);
  CString name;name.LoadString(IDS_SAVEDIAGRAMTITLE);
  CSelectFileDialog sfdlg(FALSE,defext,curFileName,OFN_PATHMUSTEXIST|OFN_OVERWRITEPROMPT|OFN_HIDEREADONLY,filter);
  sfdlg.m_ofn.lpstrTitle=name;
  if (curFileName!="")
  { //if curFileName has defext extension, set filter to second pair (ie *.*)
    RString ext(GetFileExt(curFileName.GetBuffer())); ext.Lower();
    if (ext!=RString(defext)) sfdlg.m_ofn.nFilterIndex=2;
  }
  if (sfdlg.DoModal()==IDOK)
  {
    Pathname path(sfdlg.GetPathName());
    fileName=path.GetFilename();
    fullPath=path.GetFullPath();
    UpdateData(false);
  }
}

void SaveAsDlg::OnEnKillfocusFilenameSa()
{
  // TODO: Add your control notification handler code here
  UpdateData();
  Pathname path(fileName);
  fullPath=path.GetFullPath();
}

void SaveAsDlg::OnBnClickedOk()
{
  // TODO: Add your control notification handler code here
  UpdateData();
  OnOK();
}

BOOL SaveAsDlg::OnInitDialog()
{
  CDialog::OnInitDialog();
  // TODO:  Add extra initialization here
  return TRUE;  // return TRUE unless you set the focus to a control
}

// SelectFSMNameDlg: implementation file
//
// SelectFSMNameDlg dialog

IMPLEMENT_DYNAMIC(SelectFSMNameDlg, CDialog)
SelectFSMNameDlg::SelectFSMNameDlg(QOStrStream &fsmNames, CWnd* pParent /*=NULL*/)
: CDialog(SelectFSMNameDlg::IDD, pParent)
, fsmName(_T(""))
, _fsmNames(fsmNames)
{
  fsmNameCount=0;
  const int maxLineLen=1024;
  QIStrStream in; in.init(_fsmNames);
  RString nextLine; nextLine.CreateBuffer(maxLineLen);
  while (!in.eof())
  {
    in.readLine(nextLine.MutableData(), maxLineLen);
    if (nextLine!=RString("")) fsmNameCount++;
  }
}

SelectFSMNameDlg::~SelectFSMNameDlg()
{
}

void SelectFSMNameDlg::DoDataExchange(CDataExchange* pDX)
{
  CDialog::DoDataExchange(pDX);
  DDX_LBString(pDX, IDC_LIST1, fsmName);
  DDX_Control(pDX, IDC_LIST1, fsmNameList);
}


BEGIN_MESSAGE_MAP(SelectFSMNameDlg, CDialog)
  ON_LBN_DBLCLK(IDC_LIST1, OnLbnDblclkList1)
END_MESSAGE_MAP()


// SelectFSMNameDlg message handlers

BOOL SelectFSMNameDlg::OnInitDialog()
{
  CDialog::OnInitDialog();

  // TODO:  Add extra initialization here
  const int maxLineLen=1024;
  QIStrStream in; in.init(_fsmNames);
  RString nextLine; nextLine.CreateBuffer(maxLineLen);
  while (!in.eof())
  {
    in.readLine(nextLine.MutableData(), maxLineLen);
    if (nextLine!=RString("")) fsmNameList.AddString(nextLine.Data());
  }
  
  return TRUE;  // return TRUE unless you set the focus to a control
  // EXCEPTION: OCX Property Pages should return FALSE
}

void SelectFSMNameDlg::OnLbnDblclkList1()
{
  // TODO: Add your control notification handler code here
  UpdateData();
  OnOK();
}
