// AttrEditDlg.cpp : implementation file
//

#include "stdafx.h"
#include "FSMEditor.h"
#include "AttrEditDlg.h"
#include ".\attreditdlg.h"


// CAttrEditDlg dialog

IMPLEMENT_DYNAMIC(CAttrEditDlg, CDialog)
CAttrEditDlg::CAttrEditDlg(CWnd* pParent /*=NULL*/, FSMEditor *fsm)
	: CDialog(CAttrEditDlg::IDD, pParent)
  , attrEdit(_T(""))
  , _fsm(fsm)
{
}

CAttrEditDlg::~CAttrEditDlg()
{
}

void CAttrEditDlg::DoDataExchange(CDataExchange* pDX)
{
  CDialog::DoDataExchange(pDX);
  DDX_Text(pDX, IDC_ATTR_EDIT, attrEdit);
  DDX_Control(pDX, IDC_ATTR_EDIT, editCtrl);
}


BEGIN_MESSAGE_MAP(CAttrEditDlg, CDialog)
  ON_WM_SIZE()
  ON_WM_CLOSE()
END_MESSAGE_MAP()


// CAttrEditDlg message handlers

void CAttrEditDlg::OnSize(UINT nType, int cx, int cy)
{
  CDialog::OnSize(nType, cx, cy);

  // TODO: Add your message handler code here
  CWnd *edit = GetDlgItem(IDC_ATTR_EDIT);
  if (edit)
  {
    CRect rect;
    edit->GetWindowRect(&rect);
    CPoint origin(0,0);
    ClientToScreen(&origin);
    edit->SetWindowPos(NULL, 0,0, cx+origin.x-rect.left, cy+origin.y-rect.top, SWP_NOMOVE);
  }
}

BOOL CAttrEditDlg::OnInitDialog()
{
  RString caption = "FSM Attribute: ";
  FSMAttributes &attr = _fsm->attributes;
  caption = caption + attr.compileConfigAttr[attr.attrIx].name;
  attrEdit = attr.compileConfigAttr[attr.attrIx].value;
  SetWindowText(caption.Data());
  UpdateData(false);
  editCtrl.SetFocus();
  editCtrl.SetSel(-1,0);
  return false; //focus set, return false!
}

void CAttrEditDlg::OnClose()
{
  // TODO: Add your message handler code here and/or call default
  if (_fsm)
  {
    UpdateData();
    _fsm->attributes.SetActiveText(attrEdit.GetBuffer());
  }
  EndDialog(IDOK);
  CDialog::OnClose();
}

void CAttrEditDlg::OnCancel()
{
  // TODO: Add your specialized code here and/or call the base class
  if (_fsm)
  {
    UpdateData();
    _fsm->attributes.SetActiveText(attrEdit.GetBuffer());
  }
  CDialog::OnCancel();
}
