// DevEnvDDE.cpp : Defines the entry point for the console application.
//
#include "DevEnvDDE.h"
#include <ddeml.h>

static DWORD dwInstId=0L;   /* address of instance identifier       */
	
HDDEDATA CALLBACK DdeCallback(
    UINT type,      /* transaction type     */
		UINT fmt,       /* clipboard data format        */
		HCONV hconv,    /* handle of conversation       */
		HSZ hsz1,       /* handle of string     */
		HSZ hsz2,       /* handle of string     */
		HDDEDATA hData, /* handle of global memory object       */
		DWORD dwData1,  /* transaction-specific data    */
		DWORD dwData2   /* transaction-specific data    */
    )
{
	switch (type)
	{
		case XTYP_DISCONNECT:
			//printf("DDE Callback receiving XTYP_DISCONNECT");
			break;

		case XTYP_ERROR: ;
			//printf("DDEML fatal error !",0);
	}

	return(0);   
}

void DdeSend(HCONV hconvConv,LPSTR lpszCommand,DWORD dwSize)
{
	DdeClientTransaction (
      (LPBYTE)lpszCommand,
			dwSize,
			hconvConv,
			0L,
			CF_TEXT,
			XTYP_EXECUTE,
			5000,
			0L
  );

}

bool MSDEV_OpenFile(const char* pszFileName, const char *vsServiceName )
{
	UINT  uRC;           /* return code */      
	DWORD afCmd;         /* array of command and filter flags    */
	DWORD uRes = 0;      /* reserved     */

	HCONV hconvConv;
	HSZ   hszService;
	HSZ   hszTopic;
	char  acCmd[240];
	char  acWinDir[240];

	FARPROC lpfnDdeCallback=NULL;
	UINT uErrCode=0;

	GetWindowsDirectory(acWinDir,MAX_PATH);

	if (!(lpfnDdeCallback=MakeProcInstance((FARPROC)DdeCallback,hiDLL))){
    //printf("MakeProcInstance() error %u",uErrCode);
		return false;
	}

	afCmd=APPCMD_CLIENTONLY;

	dwInstId = 0;
  uRC=DdeInitialize((DWORD FAR*)  &dwInstId,
			(PFNCALLBACK)  lpfnDdeCallback,
			afCmd,
			uRes);

	if (DMLERR_NO_ERROR!=uRC)
	{
		uErrCode=DdeGetLastError(dwInstId);
		//printf("DdeInitialize() error %u", uErrCode);
		return false;
	} 

	hszService = DdeCreateStringHandle(dwInstId,vsServiceName, CP_WINANSI);
	hszTopic   = DdeCreateStringHandle(dwInstId,"system",CP_WINANSI);

	if (!(hconvConv=DdeConnect(dwInstId,hszService,hszTopic,0L)))
	{
		uErrCode=DdeGetLastError(dwInstId);
		//printf("DdeConnect() error %u",uErrCode);
		return false;
	}

	wsprintf(acCmd,"Open(\"%s\")",pszFileName);
	DdeSend(hconvConv,acCmd,(DWORD)strlen(acCmd)+1);

	DdeFreeStringHandle(dwInstId,hszService);    
	DdeFreeStringHandle(dwInstId,hszTopic);    

	DdeDisconnect(hconvConv);
	DdeUninitialize(dwInstId);

	FreeProcInstance(lpfnDdeCallback);

  return true;
}
