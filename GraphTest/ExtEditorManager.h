#ifndef EXTERNAL_EDITOR_FILE_MANAGER
#define EXTERNAL_EDITOR_FILE_MANAGER

#include "StdAfx.h"
#include "Es/Strings/rString.hpp"
#include "Es/Threads/pocritical.hpp"
#include "FSMEditor.h"

struct DataFileItem : public RefCount
{
  // corresponding item id
  int id;
  // correspond to tab in data dlg (Condition, Precondition, ...)
  int selectedTab;
  // handle of running instance of External Editor
  HANDLE hprocess;
  HANDLE hthread;
  // full path to temp file name of the file containing edited data
  RString fileName;
  // time of last write (to check whether the file has changed or not)
  FILETIME time;

public:
  // update data using current file content
  void SaveData(FSMEditor *wGraph);
  void DeleteFile();
};

/// External Editor File Manager
class ExtEditorFileManager
{
protected:
/*
  /// working thread (waits on the external editors terminations)
  HANDLE _thread;

  /// synchronizations helpers
  PoCriticalSection _filesLock;
  HANDLE _submitSemaphore;
  HANDLE _terminateEvent;

  //@{ _filesLock access
  void EnterLock(){_filesLock.Lock();}
  void LeaveLock(){_filesLock.Unlock();}
  //@}
*/

  /// corresponding FSM
  FSMEditor *wGraph;
  /// item can never be destroyed unless already processed
  RefArray<DataFileItem> _files;
public:
  ExtEditorFileManager() : wGraph(NULL) {};
  ~ExtEditorFileManager();

  int Size() const {return _files.Size();}

/*
  void DoWork();
  static DWORD WINAPI DoWorkCallback(void *context);
*/
  void Init(FSMEditor *fsm);
  void Close();

  /// submit new item to the files
  void Submit(int ix, int tab, RString fname);
  /// update data using possibly changed 
  void Update();
  /// return, whether item with this id is being edited by External editor
  bool IsEdited(int id, int tab);
};

//global
extern ExtEditorFileManager gExtEdFileManager;

#endif
