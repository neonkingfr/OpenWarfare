// GraphView.h : main header file for the GRAPHVIEW application
//

#if !defined(AFX_GRAPHVIEW_H__D24E001C_F580_4D7C_BE7F_56D7656AECED__INCLUDED_)
#define AFX_GRAPHVIEW_H__D24E001C_F580_4D7C_BE7F_56D7656AECED__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // main symbols

/////////////////////////////////////////////////////////////////////////////
// CGraphViewApp:
// See GraphView.cpp for the implementation of this class
//

class CGraphViewApp : public CWinApp
{
protected:
  HANDLE _processReady;

public:
	CGraphViewApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CGraphViewApp)
	public:
	virtual BOOL InitInstance();
  virtual BOOL PreTranslateMessage(MSG *pMsg);
	//}}AFX_VIRTUAL

// Implementation
  void ProcessReady();

public:
	//{{AFX_MSG(CGraphViewApp)
	afx_msg void OnAppAbout();
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
  afx_msg void OnStateChanged(WPARAM,LPARAM);
  afx_msg void OnEventHandle(WPARAM,LPARAM);

	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_GRAPHVIEW_H__D24E001C_F580_4D7C_BE7F_56D7656AECED__INCLUDED_)
