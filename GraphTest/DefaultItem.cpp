#include <direct.h>
#include "StdAfx.h"
#include "FSMEditor.h"

DefaultItem::DefaultItem()
{ 
  ItemInfo *info = FSMEditor::GetDefItemInfoStatic()->Clone();

  item.data=info;
  item.color=RGB(255,255,255);
  item.flags=SGRI_ALL | SGRI_CUSTOMDRAW;
  item.rect = SFloatRect(0,0,20,10);
}

DefaultItem::DefaultItem(SGraphItem &grItem)
{
  item=grItem;
  //data
  ItemInfo *newinfo=((ItemInfo *)grItem.data)->Clone();  //copy

  item.data=newinfo;
}

DefaultItem::~DefaultItem()
{
  delete (ItemInfo *)item.data;  //virtual
}

DefaultItems::DefaultItems(FSMEditor &graph) : wGraph(graph) 
{
}

void DefaultItems::ChangeToDefaultItem(DefaultItemType itemType, SGraphItem &grItem)
{
  if (itemType>=0 && itemType<DF_DEFAULT_ITEM_NUM) 
  {
    DefaultItemType origType = DF_KNEE;
    if (grItem.data && ((FSMItemInfo *)grItem.data)->fsmData) origType = ((FSMItemInfo *)grItem.data)->fsmData->type;
    if (origType!=DF_KNEE && (FSMItemInfo::IsCondition(itemType)!=FSMItemInfo::IsCondition(origType) || itemType==DF_KNEE))
    {
      if (AfxMessageBox(IDS_WARNCHANGE, MB_YESNO|MB_ICONWARNING) != IDYES) return;
    }
    SGraphItem &itm = item[itemType].item;
    grItem.color = itm.color;
    grItem.flags = itm.flags;
    //change only size of rectangle
    grItem.rect.bottom = grItem.rect.top + itm.rect.bottom - itm.rect.top;
    grItem.rect.right = grItem.rect.left + itm.rect.right - itm.rect.left;
    //copy ItemInfo data completelly, except for text
    CString txt = "";
    int id=-1;  //not initialized
    if (grItem.data) 
    {
      txt=((FSMItemInfo *)grItem.data)->text;    //it mustn't be changed
      if (((FSMItemInfo *)grItem.data)->fsmData) 
        id = *((FSMItemInfo *)grItem.data)->fsmData->GetID();
    }
    FSMItemInfo *info = ((FSMItemInfo *)itm.data)->Clone();
    info->text=txt;  //set it back
    if (origType!=DF_KNEE && (FSMItemInfo::IsCondition(itemType)==FSMItemInfo::IsCondition(origType) && itemType!=DF_KNEE))
    {
      if (grItem.data && ((FSMItemInfo *)grItem.data)->fsmData)
      {
        //set back specific data, which are not to be changed
        FSMItemValue* fsmdata = ((FSMItemInfo *)grItem.data)->fsmData;
        info->CopyFsmData(fsmdata);
      }
      info->fsmData->type=itemType;
    }
    if (grItem.data) delete (ItemInfo *)grItem.data;
    if (id!=-1) *(info->fsmData->GetID())=id;
    else 
    {
      *(info->fsmData->GetID())=wGraph.GetNewID();
    }
    grItem.data=info;
    if (info->GetShouldBeAlignedToGrid())
    {
//      int itId = *(info->fsmData->GetID());
//      SFloatRect rc=wGraph.GetItemRect(itId);
      wGraph.AlignToGrid(grItem.rect);
//      wGraph.SetItemRect(itId,rc);
    }
    else
    {
      wGraph.AlignKneeToGrid(grItem.rect);
    }
  }
}

void DefaultItems::SetDefaultItem(DefaultItemType itemType, int itmIx)
{
  if (itemType>=0 && itemType<DF_DEFAULT_ITEM_NUM)
  {
    SGraphItem *targetitem = &item[itemType].item;
    FSMItemInfo *targetinfo=(FSMItemInfo *)targetitem->data;
    if (wGraph.GetItem(itmIx, targetitem))
    {
      FSMItemInfo *sourceinfo=(FSMItemInfo *)targetitem->data;
      if (sourceinfo)
      {
        delete targetinfo;
        targetinfo = sourceinfo->Clone();
      }
      targetitem->data=targetinfo;
      //text data are irrelevant here
    }
  }
}

//the following is probably redundant (ChangeToDefaultItem is sufficient)
#define DEFWIDTH 20
#define DEFHEIGHT 10
void DefaultItems::InsertDefaultItem(DefaultItemType itemType, float x, float y)
{
  //text (and possible other data) should be reset
  SGraphItem defitem=item[itemType].item;
  ItemInfo *newinfo = ((ItemInfo *)defitem.data)->Clone();
  defitem.data=newinfo;

  defitem.rect.bottom = y + defitem.rect.bottom - defitem.rect.top;
  defitem.rect.top = y;
  defitem.rect.right = x + defitem.rect.right - defitem.rect.left;
  defitem.rect.left = x;
  defitem.text[0] = 0;
  newinfo->text = CString("");

  int itix=wGraph.InsertItem(SGRI_NOGUESSPOSITION,TRANSPARENTCOLOR,"",DEFWIDTH,DEFHEIGHT,&newinfo);
  wGraph.SetItem(itix,&defitem);
}

LSError DefaultItems::Serialize(ParamArchive &ar)
{
  int cnt=0;
  int i;
  ParamArchive asect;
  //Default Items
  if (!ar.OpenSubclass("DefaultItems",asect, true))
  {
    return LSUnknownError;
  }
  for (i=0; i<DF_DEFAULT_ITEM_NUM; i++)
  {
    wGraph.SerializeItem(i, item[i].item, asect);
  }
  ar.CloseSubclass(asect);
  //Globals
  if (ar.OpenSubclass("DefaultGlobals", asect))
  {
/*
    asect.Serialize("Grid",wGraph._grid,1,0.0f);
    asect.Serialize("GridShow",wGraph._gridshow,1,false);
    asect.Serialize("PageColor",wGraph._pageColor,1,false);
    asect.Serialize("PageColorVal",(int &)wGraph._pageColorVal,1,0);
*/
    //Default link
    asect.Serialize("DefaultLink", wGraph.defLinkFlags, 1);
    asect.Serialize("DefaultLinkColor", (int &)wGraph.defLinkColor, 1);
    asect.Serialize("DefaultLinkColorFromSelected", (int &)wGraph.defLinkColorFromSelected, 1);
    asect.Serialize("DefaultLinkColorToSelected", (int &)wGraph.defLinkColorToSelected, 1);
    asect.Serialize("DefaultLinkUseCustom", wGraph.defLinkUseCustom, 1);  
  }
  return LSOK;
}

#include <el/ParamArchive/paramArchive.hpp>
#include <el/ParamArchive/paramArchiveDb.hpp>
bool DefaultItems::Save(const char * filename)
{
  ParamArchiveSave ars(1);
  Serialize(ars);
  bool res=ars.Save(filename);
  return res;
}

bool DefaultItems::Load(const char * filename)
{
  for (int i=0; i<DF_DEFAULT_ITEM_NUM; i++) 
  {
    delete (ItemInfo *)item[i].item.data;
    item[i].item.data = NULL;
  }
  ParamArchiveLoad arl(filename);
  bool retval = Serialize(arl)==LSOK;
  if (!retval)
  {
    for (int i=0; i<DF_DEFAULT_ITEM_NUM; i++) 
    {
      item[i].item.data = wGraph.GetDefItemInfo()->Clone();
    }
    AfxMessageBox(IDS_LOADDEFAULTSFAILED, MB_OK|MB_ICONWARNING);
  }
  wGraph.Update();  //_grid or sth. else can be visible
  return retval;
}

#include <io.h>
#include <fcntl.h>
#include <EL/Pathname/Pathname.h>
void DefaultItems::LoadDefaults()
{
  CString fname; fname.LoadString(IDS_DEFAULTFILENAME);
  int ftest = _open(fname, _O_RDONLY); //only file exist test
  if (ftest!=-1) //use config file in current directory
  {
    _close(ftest);
    Load(fname);
  }
  else //use config file in the directory of exeFile
  {
    Pathname exepath = Pathname::GetExePath();
    exepath.SetFilename(fname);
    Load(exepath.GetFullPath());
  }
}
