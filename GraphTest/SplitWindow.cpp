// SplitWindow.cpp : implementation file
//

#include "stdafx.h"
#include "SplitWindow.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSplitWindow

LPCTSTR CSplitWindow::classname=NULL;


CSplitWindow::CSplitWindow()
{
  first=second=NULL;
  touchsize=3;
  fflags=sflags=0;
  if (classname==NULL) 
    classname=AfxRegisterWndClass(CS_HREDRAW|CS_VREDRAW,NULL,(HBRUSH)COLOR_BTNFACE,NULL);
  spliten=true;
  autodelete=false;
}

CSplitWindow::~CSplitWindow()
{
  if (fflags & CSPW_AUTODESTROY)
  {
    delete first;
  }
  if (sflags & CSPW_AUTODESTROY)
  {
    delete second;
  }
}


BEGIN_MESSAGE_MAP(CSplitWindow, CWnd)
  //{{AFX_MSG_MAP(CSplitWindow)
  ON_WM_PAINT()
  //}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// CSplitWindow message handlers

void CSplitWindow::Create(CWnd *parent, float splitpos, UINT uid)
{
  CRect rc;
  parent->GetClientRect(&rc);
  CWnd::Create(classname,"",WS_VISIBLE|WS_CHILD|WS_CLIPCHILDREN|WS_CLIPSIBLINGS,rc,parent,uid,NULL);
  SetSplitPos(splitpos);
}

void CSplitWindow::Create(CWnd *parent, int splitpos, UINT uid)
{
  CRect rc;
  parent->GetClientRect(&rc);
  CWnd::Create(classname,"",WS_VISIBLE|WS_CHILD|WS_CLIPCHILDREN|WS_CLIPSIBLINGS,rc,parent,uid,NULL);
  SetSplitPos(splitpos);
}

/////////////////////////////////////////////////////////////////////////////
// CSplitWindowVert

CSplitWindowVert::CSplitWindowVert()
{
  lastcx=-1;
}

CSplitWindowVert::~CSplitWindowVert()
{
}


BEGIN_MESSAGE_MAP(CSplitWindowVert, CSplitWindow)
  //{{AFX_MSG_MAP(CSplitWindowVert)
  ON_WM_SIZE()
  ON_WM_LBUTTONDOWN()
  ON_WM_LBUTTONUP()
  ON_WM_MOUSEMOVE()
  ON_WM_SETCURSOR()

  //}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// CSplitWindowVert message handlers

void CSplitWindowVert::SetSplitPos(int pos)
{
  CRect rc;
  GetClientRect(&rc);
  CSize sz=rc.Size();  
  splitpospx=splitpos=pos;
  if (spliten)
  {
    if (splitpos<touchsize) splitpos=touchsize;
    if (splitpos>sz.cx-touchsize) splitpos=sz.cx-touchsize;
  }
  splitposper=((float)splitpos/(float)sz.cx);
}

void CSplitWindowVert::SetSplitPos(float pos)
{
  CRect rc;
  GetClientRect(&rc);
  CSize sz=rc.Size();
  splitpos=(int)(sz.cx*pos);
  SetSplitPos(splitpos);
  splitposper=pos;
}

void CSplitWindowVert::RecalcLayout()
{
  LoadChilds();
  CRect rc;
  GetClientRect(&rc);
  CSize sz=rc.Size();
  int left=splitpos-touchsize/2;
  int right=left+touchsize;
  if (first)
    first->MoveWindow(0,0,left,sz.cy,TRUE);
  if (second)
    second->MoveWindow(right,0,sz.cx-right,sz.cy,TRUE);	
}


void CSplitWindowVert::OnSize(UINT nType, int cx, int cy) 
{
  if (lastcx==-1) 
  {
    lastcx=cx;RecalcLayout();return;
  }
  bool fs=(fflags & CSPW_RESIZE)!=0;
  bool ss=(sflags & CSPW_RESIZE)!=0;
  if (ss && fs) SetSplitPos(splitposper);
  else if (fs) 
  {
    SetSplitPos(splitpospx+(cx-lastcx));	
  }
  else
    SetSplitPos(splitpospx);	
  lastcx=cx;
  RecalcLayout();
}


void CSplitWindowVert::OnLButtonDown(UINT nFlags, CPoint point) 
{
  if (spliten==false) return;
  lastmsx=point.x-splitpos;
  SetCapture();
}

void CSplitWindowVert::OnLButtonUp(UINT nFlags, CPoint point) 
{
  ReleaseCapture();
}

void CSplitWindowVert::OnMouseMove(UINT nFlags, CPoint point) 
{
  if (GetCapture()==this)
  {
    if (nFlags & MK_LBUTTON)
    {
      SetSplitPos(point.x-lastmsx);
      RecalcLayout();
    }
    else
      OnLButtonUp(nFlags,point);
  }    
}

CWnd * CSplitWindow::FindChild(UINT uid)
{
  if (first &&  first->GetDlgCtrlID()==(int)uid) return first;
  if (second && second->GetDlgCtrlID()==(int)uid) return second;
  CSplitWindow *fsplit=dynamic_cast<CSplitWindow *>(first);
  CSplitWindow *ssplit=dynamic_cast<CSplitWindow *>(second);
  if (fsplit) return fsplit->FindChild(uid);
  if (ssplit) return ssplit->FindChild(uid);
  return NULL;
}

void CSplitWindow::LoadChilds()
{
  first=GetWindow(GW_CHILD);
  if (first) 
    second=first->GetWindow(GW_HWNDNEXT);
}

BOOL CSplitWindow::OnCmdMsg(UINT nID, int nCode, void* pExtra, AFX_CMDHANDLERINFO* pHandlerInfo) 
{
  if (first && first->OnCmdMsg(nID, nCode, pExtra, pHandlerInfo)) return TRUE;
  if (second && second->OnCmdMsg(nID, nCode, pExtra, pHandlerInfo)) return TRUE;	
  return FALSE;
}

void CSplitWindow::OnPaint() 
{
  CPaintDC dc(this); // device context for painting
  CRect rc;
  GetClientRect(&rc);
  dc.FillSolidRect(&rc,GetSysColor(COLOR_BTNFACE));
}

BOOL CSplitWindowVert::OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message) 
{
  if (pWnd==this && spliten)
    SetCursor(LoadCursor(NULL,IDC_SIZEWE));
  else
    CWnd::OnSetCursor(pWnd,nHitTest,message);
  return TRUE;
}
/////////////////////////////////////////////////////////////////////////////
// CSplitWindowHorz

CSplitWindowHorz::CSplitWindowHorz()
{
  lastcy=-1;
}

CSplitWindowHorz::~CSplitWindowHorz()
{
}


BEGIN_MESSAGE_MAP(CSplitWindowHorz, CSplitWindow)
  //{{AFX_MSG_MAP(CSplitWindowHorz)
  ON_WM_SIZE()
  ON_WM_LBUTTONDOWN()
  ON_WM_LBUTTONUP()
  ON_WM_MOUSEMOVE()
  ON_WM_SETCURSOR()
  //}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// CSplitWindowHorz message handlers

void CSplitWindowHorz::SetSplitPos(int pos)
{
  CRect rc;
  GetClientRect(&rc);
  CSize sz=rc.Size();  
  splitpospx=splitpos=pos;
  if (spliten)
  {
    if (splitpos<touchsize) splitpos=touchsize;
    if (splitpos>sz.cy-touchsize) splitpos=sz.cy-touchsize;
  }
  splitposper=((float)splitpos/(float)sz.cy);
}

void CSplitWindowHorz::SetSplitPos(float pos)
{
  CRect rc;
  GetClientRect(&rc);
  CSize sz=rc.Size();
  splitpospx=(int)(sz.cy*pos);
  SetSplitPos(splitpospx);
  splitposper=pos;
}

void CSplitWindowHorz::RecalcLayout()
{
  LoadChilds();
  CRect rc;
  GetClientRect(&rc);
  CSize sz=rc.Size();
  int top=splitpos-touchsize/2;
  int bottom=top+touchsize;
  if (first)
    first->MoveWindow(0,0,sz.cx,top,TRUE);
  if (second)
    second->MoveWindow(0,bottom,sz.cx,sz.cy-bottom,TRUE);	
}


void CSplitWindowHorz::OnSize(UINT nType, int cx, int cy) 
{
  if (lastcy==-1) 
  {
    lastcy=cy;RecalcLayout();return;
  }
  bool fs=(fflags & CSPW_RESIZE)!=0;
  bool ss=(sflags & CSPW_RESIZE)!=0;
  if (ss && fs) SetSplitPos(splitposper);
  else if (fs) 
  {
    SetSplitPos(splitpospx+(cy-lastcy));	
  }
  else
    SetSplitPos(splitpospx);	
  lastcy=cy;
  RecalcLayout();
}

void CSplitWindowHorz::OnLButtonDown(UINT nFlags, CPoint point) 
{
  if (spliten==false) return;
  lastmsy=point.y-splitpos;
  SetCapture();
}

void CSplitWindowHorz::OnLButtonUp(UINT nFlags, CPoint point) 
{
  ReleaseCapture();
}

void CSplitWindowHorz::OnMouseMove(UINT nFlags, CPoint point) 
{
  if (GetCapture()==this)
  {
    if (nFlags & MK_LBUTTON)
    {
      SetSplitPos(point.y-lastmsy);
      RecalcLayout();
    }
    else
      OnLButtonUp(nFlags,point);
  }    
}

BOOL CSplitWindowHorz::OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message) 
{
  if (pWnd==this && spliten)
    SetCursor(LoadCursor(NULL,IDC_SIZENS));
  else
    CWnd::OnSetCursor(pWnd,nHitTest,message);
  return TRUE;
}



LRESULT CSplitWindow::WindowProc(UINT message, WPARAM wParam, LPARAM lParam) 
{
  if (message==WM_NOTIFY || message==WM_CONTEXTMENU)
    return GetParent()->SendMessage(message,wParam,lParam);
  else
    return CWnd::WindowProc(message, wParam, lParam);
}

void CSplitWindow::ChangeFrame(int frmnum, CWnd *frame)
{
  ASSERT(frmnum==0 || frmnum==1);
  if (frmnum==0)
  {
    if (first) 
    {
      first->DestroyWindow();
      delete first;
    }
    if (frame) frame->SetWindowPos(&wndTop,0,0,0,0,SWP_NOMOVE|SWP_NOSIZE);
  }
  else
  {
    if (second)
    {
      second->DestroyWindow();
      delete second;
    }
  }
  LoadChilds();
}

void CSplitWindow::PostNcDestroy()
{
  if (autodelete) delete this;
}
