// GraphCtrlExample.h: interface for the CGraphCtrlExample class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_GRAPHCTRLEXAMPLE_H__AE3B7A4A_8E61_495D_AF1C_E51EBD3DA6F4__INCLUDED_)
#define AFX_GRAPHCTRLEXAMPLE_H__AE3B7A4A_8E61_495D_AF1C_E51EBD3DA6F4__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "GraphCtrlExt.h"

class CGraphCtrlExample : public CGraphCtrlExt
{
  CBitmap bmp;
  CBitmap bmpmask;
  DWORD flags;
public:
	void SetFlagSelection(DWORD invert, DWORD zero);
	DWORD GetSelectionFlags() {return flags;}
  //
  //CGraphCtrl methods overloading
    virtual void OnLinkItems(int beginitem, int enditem);
    virtual void OnChangeLink(int lnk, int beginitem, int enditem, int newitem, void *data);
    virtual void OnUnlinkItems(int lnk, int beginitem, int enditem, void *data);
    virtual void OnTouchLink(int lnk, int beginitem, int enditem, void *data);
    virtual bool OnCustomDraw(int item, SGraphItem &itm, CDC &dc, CRect &rc);
    virtual bool OnCustomDrawLink(int lnk, int beginitem, int enditem, int order, void *data, CDC &dc, CRect rc);
    virtual void OnEndSelection();
    virtual void OnResizeItem(int item, int corner, float xs, float ys) {Update();UpdateWindow();}
    virtual void OnMoveItem(int item, float xs, float ys) {Update();UpdateWindow();}
    virtual void OnEndMoveResize(int corner) {Beep(3000,100);}
  //end of CGraphCtrl methods overloading
  //
	CGraphCtrlExample();
	virtual ~CGraphCtrlExample();
};

#endif // !defined(AFX_GRAPHCTRLEXAMPLE_H__AE3B7A4A_8E61_495D_AF1C_E51EBD3DA6F4__INCLUDED_)
