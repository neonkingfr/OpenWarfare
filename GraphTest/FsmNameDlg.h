#pragma once
#include "afxwin.h"
#include "Es/Strings/rString.hpp"


// FsmNameDlg dialog

class FsmNameDlg : public CDialog
{
	DECLARE_DYNAMIC(FsmNameDlg)

public:
	FsmNameDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~FsmNameDlg();

// Dialog Data
	enum { IDD = IDD_FSMNAME };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
  CString fsmName;
};

// CompileConfigDlg dialog

class CompileConfigDlg : public CDialog
{
  DECLARE_DYNAMIC(CompileConfigDlg)

public:
  CompileConfigDlg(CWnd* pParent = NULL);   // standard constructor
  virtual ~CompileConfigDlg();

  // Dialog Data
  enum { IDD = IDD_COMPILECONFIG };

protected:
  virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

  DECLARE_MESSAGE_MAP()
public:
  CString configFile;
  afx_msg void OnBnClickedConfigBrowse();
};

// FsmAttributesDlg dialog

class FsmAttributesDlg : public CDialog
{
  DECLARE_DYNAMIC(FsmAttributesDlg)
protected:
  RString _compileConfig;

public:
  FsmAttributesDlg(RString compileConfig, CWnd* pParent = NULL);   // standard constructor
  virtual ~FsmAttributesDlg();

  // Dialog Data
  enum { IDD = IDD_FSMATTRIBUTESDLG };

protected:
  virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

  DECLARE_MESSAGE_MAP()
public:
  // list of FSM attributes from compile config
  CListBox attributesList;
  // selected FSM attribute from the list
  CString attributeName;
  virtual BOOL OnInitDialog();
  afx_msg void OnLbnDblclkList1();
};

// External Editor Dialog

class ExternalEditorDlg : public CDialog
{
  DECLARE_DYNAMIC(ExternalEditorDlg)

public:
  ExternalEditorDlg(CWnd* pParent = NULL);   // standard constructor
  virtual ~ExternalEditorDlg();

  // Dialog Data
  enum { IDD = IDD_EXTERNALEDITOR };

protected:
  virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

  DECLARE_MESSAGE_MAP()
public:
  CString externalEditor;
  CString serviceDDE;
  BOOL useDDE;
};
