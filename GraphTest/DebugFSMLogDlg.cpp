// DebugFSMLogDlg.cpp : implementation file
//

#include "stdafx.h"
#include "FSMEditor.h"
#include "MainFrm.h"
#include "DebugFSMLogDlg.h"


// DebugFSMLogDlg dialog

IMPLEMENT_DYNAMIC(DebugFSMLogDlg, CDialog)

DebugFSMLogDlg::DebugFSMLogDlg(CWnd* pParent /*=NULL*/)
	: CDialog(DebugFSMLogDlg::IDD, pParent)
  , fsmLogText(_T(""))
  , fsmId(_T(""))
{

}

DebugFSMLogDlg::~DebugFSMLogDlg()
{
}

BOOL DebugFSMLogDlg::OnInitDialog()
{
  CDialog::OnInitDialog();

  // TODO:  Add extra initialization here
  fsmId = CString(_T("1"));
  UpdateData(FALSE);

  return TRUE;  // return TRUE unless you set the focus to a control
  // EXCEPTION: OCX Property Pages should return FALSE
}


void DebugFSMLogDlg::DoDataExchange(CDataExchange* pDX)
{
  CDialog::DoDataExchange(pDX);
  DDX_Text(pDX, IDC_EDIT, fsmLogText);
  DDX_Text(pDX, IDC_FSM_ID, fsmId);
}

BEGIN_MESSAGE_MAP(DebugFSMLogDlg, CDialog)
  ON_WM_SIZE()
  ON_WM_CLOSE()
  ON_EN_CHANGE(IDC_FSM_ID, OnChangeFSMId)
  ON_BN_CLICKED(IDC_BROWSE, &DebugFSMLogDlg::OnBnClickedBrowse)
  ON_BN_CLICKED(IDC_BUTTON_PREV, &DebugFSMLogDlg::OnBnClickedButtonPrev)
  ON_BN_CLICKED(IDC_BUTTON_NEXT, &DebugFSMLogDlg::OnBnClickedButtonNext)
END_MESSAGE_MAP()

// DebugFSMLogDlg message handlers

void DebugFSMLogDlg::OnSize(UINT nType, int cx, int cy)
{
  CDialog::OnSize(nType, cx, cy);

  // TODO: Add your message handler code here
  CWnd *edit = GetDlgItem(IDC_EDIT);
  if (edit)
  {
    CRect rect;
    edit->GetWindowRect(&rect);
    CPoint origin(0,0);
    ClientToScreen(&origin);
    edit->SetWindowPos(NULL, 0,0, cx+origin.x-rect.left, cy+origin.y-rect.top, SWP_NOMOVE);
  }
}

void DebugFSMLogDlg::OnClose()
{
  // TODO: Add your message handler code here and/or call default
  ShowWindow(SW_HIDE);
  //CDialog::OnClose();
}

BOOL DebugFSMLogDlg::PreTranslateMessage(MSG* pMsg)
{
  // TODO: Add your specialized code here and/or call the base class
  // The incoming ENTER and ESCAPE key down messages are to be nullified in order not to close the dialog
  if(pMsg->message==WM_KEYDOWN)
  {
    if(pMsg->wParam==VK_RETURN || pMsg->wParam==VK_ESCAPE)
      pMsg->wParam=NULL ;
  }
  
  // update FSMEditor highlighted items according to possibly changed cursor position inside EDIT control of this debug dialog
  static int pos = -1;
  static int curLine = -1;
  if (CDialog::PreTranslateMessage(pMsg))
  {
    CEdit *edit = (CEdit*)GetDlgItem(IDC_EDIT);
    if (edit)
    {
      int dummy, newPos;
      edit->GetSel(newPos, dummy);
      if (newPos!=pos)
      {
        pos = newPos;
        int newline = edit->LineFromChar(pos);
        if (curLine!=newline)
        {
          // highlight possibly different debugged item
          int dummy;
          int stateIx = GetLastValidItemIx(newline, dummy);
          if (stateIx!=INT_MIN && stateIx<0)
          { // it is -(stateIx+1)
            int itemIx = ((CMainFrame *)GetParent())->View.FindItemByStateIx(-stateIx-1);
            AfxGetMainWnd()->PostMessage(MSG_STATE_CHANGED, -stateIx-1, 0);
            AfxGetMainWnd()->PostMessage(MSG_FOCUS_ERROR_ITEM, 1, itemIx+65536*0xffff);
          }
          else
          {
            AfxGetMainWnd()->PostMessage(MSG_STATE_CHANGED, -stateIx-1, 0);
            AfxGetMainWnd()->PostMessage(MSG_FOCUS_ERROR_ITEM, 1, stateIx+65536*0xffff);
          }
          // update line number
          CStatic *lnNo = (CStatic *)GetDlgItem(IDC_LN_NUM);
          if (lnNo)
          {
            char lnNoBuf[32];
            itoa(newline+1, lnNoBuf, 10);
            lnNo->SetWindowText(lnNoBuf);
            //UpdateData(FALSE);
          }
        }
      }
    }
    return true;
  }
  return false;
}

/**
* Gets the item index to be highlighted
*
* @param  lnNum is line number inside fsmLogText
* @return nonNegative value is the final index into FSMEditor items array
          negative value is -(stateNo+1) and corresponding state should be found only inside states, not conditions
          INT_MIN when no debug line is under the lnNum line index
*/
int DebugFSMLogDlg::GetItemIx(int lnNum)
{
  int nothingHere = INT_MIN;
  CEdit *edit = (CEdit*)GetDlgItem(IDC_EDIT);
  if (edit)
  {
    char *line = fsmLogText.GetBuffer()+edit->LineIndex(lnNum);
    char *nextLine = fsmLogText.GetBuffer()+edit->LineIndex(lnNum+1);
    char backupChar;
    if (nextLine>line) { backupChar = *(nextLine-1); *(nextLine-1)=0; /*end line*/ }
    int itemNumber=-1;
    // 1. Test whether line contains any DebugFSMLog
    // sample: DebugFSM: 1 "mission" state: 5 "C0b"
    // Optional: there can be possible leading timestamp prefix which we should skip
    // we just try to find the "DebugFSM: " string
    // sample: 
    // 251.593: DebugFSM: 1,8 "ca\missions\campaign\missions\C0_FirstToFight.Utes\mission.fsm" state: 4 "Armstrong_s_speech"
    char *line2 = strstr(line, "DebugFSM: ");
    if (!line2) return nothingHere;
    else 
    {
      if (nextLine>line) { *(nextLine-1)=backupChar; /*restore end line*/ }
      line = line2 + 10; //skip "DebugFSM: ";
    }
    // 2. Test fsmId is matching
    if ( strncmp(line, fsmId.GetBuffer(), fsmId.GetLength()) ) return nothingHere;
    // 3. maybe there is itemNo
    // sample: DebugFSM: 1,8 "mission" state: 5 "C0b"
    int dummyFsmId, itemNo = -1;
    sscanf(line, "%d,%d", &dummyFsmId, &itemNo);
    if (itemNo != -1)
      return itemNo; //there was itemNo inside the debug line
    // 4. Find the state or condition identification
    line += fsmId.GetLength(); //skip fsmId
    int quoteCount = 0;
    do {
      switch (*line)
      {
      case '\0': return nothingHere;
      case '\"': quoteCount++;
      }
      line++;
    } while (quoteCount < 2); //skip text between "here"
    while (*line==' ') line++; //skip spaces
    // ignore CONDITIONS for this time
    if ( strncmp(line, "state: ", 7) ) return nothingHere;
    line += 7; //skip "state: "
    // 5. Get the state index
    if (!*line) return nothingHere;
    int stateIx = 0;
    sscanf(line, "%d", &stateIx);
    return -stateIx-1; //-1 is necessary, see @return description
  }
  return nothingHere; //nothing inside this line
}

void DebugFSMLogDlg::OnChangeFSMId()
{
  UpdateData();
}

#include "SaveLoadDlgs.h"

void DebugFSMLogDlg::OnBnClickedBrowse()
{
  // TODO: Add your control notification handler code here
  UpdateData();
  CString defext;defext.LoadString(IDS_DEBUGLOGEXT);
  CString filter;filter.LoadString(IDS_DEBUGLOGFILTER);
  CString name;name.LoadString(IDS_DEBUGLOGTITLE);
  CString fileName;
  CSelectFileDialog sfdlg(TRUE,defext,fileName,OFN_PATHMUSTEXIST|OFN_OVERWRITEPROMPT|OFN_HIDEREADONLY,filter);
  sfdlg.m_ofn.lpstrTitle=name;
  if (sfdlg.DoModal()==IDOK)
  {
    fileName=sfdlg.GetPathName();
    // Load the content of the file into IDC_EDIT box
    FILE *infile = fopen(fileName.GetBuffer(), "rt");
    if (infile)
    {
      CString caption1; caption1.LoadString(IDS_DEBUGLOGTITLE);
      RString newCaption = Format("%s - %s", caption1.GetBuffer(), cc_cast(fileName));
      SetWindowText(cc_cast(newCaption));
      fsmLogText = CString(); //clear
      char szBuffer[2048];
      while(fgets(szBuffer, 2048, infile))
      {
        szBuffer[strlen(szBuffer)-1]=0;
        fsmLogText += szBuffer;
        fsmLogText += "\r\n";
      }
      fclose(infile);
    }
    UpdateData(FALSE);
  }
}

int DebugFSMLogDlg::GetLastValidItemIx(int lnNum, int &i)
{
  for (i=lnNum; i>0; i--)
  {
    int itemIx = GetItemIx(i);
    if (itemIx != INT_MIN)
      return itemIx;
  }
  return INT_MIN; //no valid line found
}

void DebugFSMLogDlg::OnBnClickedButtonPrev()
{
  // TODO: Add your control notification handler code here
  CEdit *edit = (CEdit*)GetDlgItem(IDC_EDIT);
  if (edit)
  {
    int dummy, pos;
    edit->GetSel(pos, dummy);
    int lnNum = edit->LineFromChar(pos);
    int lnIx;
    int itemIx = GetLastValidItemIx(lnNum-1, lnIx);
    if (itemIx!=INT_MIN)
    {
      pos = edit->LineIndex(lnIx); //new pos
      edit->SetSel(pos,pos+8);
    }
    edit->SetFocus();
  }
}

void DebugFSMLogDlg::OnBnClickedButtonNext()
{
  // TODO: Add your control notification handler code here
  CEdit *edit = (CEdit*)GetDlgItem(IDC_EDIT);
  if (edit)
  {
    int dummy, pos;
    edit->GetSel(pos, dummy);
    int lnNum = edit->LineFromChar(pos);
    for (int i=lnNum+1; i<edit->GetLineCount(); i++)
    {
      int itemIx = GetItemIx(i);
      if (itemIx != INT_MIN)
      {
        pos = edit->LineIndex(i); //new pos
        edit->SetSel(pos,pos+8);
        break;
      }
    }
    edit->SetFocus();
  }
}
