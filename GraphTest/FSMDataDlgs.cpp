// FSMDataDlgs.cpp : implementation file
//

#include "stdafx.h"
//#include "GraphView.h"
#include "FSMDataDlgs.h"
#include "MainFrm.h"
#include ".\fsmdatadlgs.h"

// CCodeEdit special edit to change doubleClick behaviour of CEdit

BEGIN_MESSAGE_MAP(CCodeEdit, CEdit)
  ON_WM_LBUTTONDBLCLK()
  ON_WM_SETFOCUS()
  ON_WM_KILLFOCUS() 
END_MESSAGE_MAP()

bool IsDblClkSelectableChar(TCHAR c)
{
  return ( isalnum(c) || c==_T('.') || c==_T('_') );
}

void CCodeEdit::OnLButtonDblClk(UINT nFlags, CPoint point)
{
  int selBeg, selEnd;
  GetSel(selBeg, selEnd);
  int line = LineFromChar(selBeg);
  CString text;
  GetWindowText(text);
  int start = selBeg;
  while(start>0 && IsDblClkSelectableChar(text.GetAt(start-1))) start--;
  int end = selBeg;
  int len = text.GetLength();
  while(end<len && IsDblClkSelectableChar(text.GetAt(end))) end++;
  SetSel(start,end);
  //CEdit::OnLButtonDblClk(nFlags, point);
}

void CCodeEdit::SetFocusData()
{
  SetSel(selBeg, selEnd);
  int curLScroll = GetFirstVisibleLine();
  LineScroll(lScroll-curLScroll);
}

void CCodeEdit::SaveFocusData()
{
  GetSel(selBeg, selEnd);
  lScroll = GetFirstVisibleLine();
}

afx_msg void CCodeEdit::OnSetFocus(CWnd* pNewWnd)
{
  SetFocusData();
  CEdit::OnSetFocus(pNewWnd);
}

afx_msg void CCodeEdit::OnKillFocus(CWnd* pNewWnd)
{
  //SaveFocusData();
  ((CMainFrame*)AfxGetMainWnd())->TextEdit.UpdateFocus(); //Save into variables
  CEdit::OnKillFocus(pNewWnd);
}

// CConditionDataDlg dialog

IMPLEMENT_DYNAMIC(CConditionDataDlg, CDialog)
CConditionDataDlg::CConditionDataDlg(CWnd* pParent /*=NULL*/)
: CDialog(CConditionDataDlg::IDD, pParent)
, conditionEdit(_T(""))
, condValue(NULL)
, priority(0)
{
}

BEGIN_MESSAGE_MAP(CConditionDataDlg, CDialog)
  ON_WM_SIZE()
  ON_BN_CLICKED(IDC_SAVECONDITIONEDIT, OnBnClickedSaveconditionedit)
  ON_BN_CLICKED(IDC_EDITCONDITION, OnBnClickedEditCondition)
  ON_NOTIFY(TCN_SELCHANGE, IDC_CONDITIONTAB, OnTcnSelchangeConditiontab)
  ON_NOTIFY(TCN_SELCHANGING, IDC_CONDITIONTAB, OnTcnSelchangingConditiontab)
END_MESSAGE_MAP()

CConditionDataDlg::~CConditionDataDlg()
{
}

void CConditionDataDlg::DoDataExchange(CDataExchange* pDX)
{
  CDialog::DoDataExchange(pDX);
  DDX_Control(pDX, IDC_CONDITIONEDIT, conditionEditCtrl);
  DDX_Text(pDX, IDC_CONDITIONEDIT, conditionEdit);
  DDX_Text(pDX, IDC_EDIT1, priority);
  DDX_Control(pDX, IDC_CONDITIONTAB, conditionTab);
}

BOOL CConditionDataDlg::OnInitDialog()
{
  CDialog::OnInitDialog();
  TC_ITEM TabCtrlItem;
  TabCtrlItem.mask = TCIF_TEXT;
  TabCtrlItem.pszText = "Condition";
  conditionTab.InsertItem( 0, &TabCtrlItem );
  TabCtrlItem.pszText = "Action";
  conditionTab.InsertItem( 1, &TabCtrlItem );
  TabCtrlItem.pszText = "PreCondition";
  conditionTab.InsertItem( 2, &TabCtrlItem );
//   CEdit *edit = (CEdit *)GetDlgItem(IDC_CONDITIONEDIT);
//   edit->SetTabStops(12);
  conditionEditCtrl.SetTabStops(12);
  extern CGraphViewApp theApp;
  ((FSMDataDlgs *)GetParent())->LoadFontFromProfile(theApp);
  return true;
}

void CConditionDataDlg::OnTcnSelchangingConditiontab(NMHDR *pNMHDR, LRESULT *pResult)
{
  // TODO: Add your control notification handler code here
  UpdateData();
  int nTab = conditionTab.GetCurSel();
  FSMCondition *cond = const_cast<FSMCondition *>(condValue->GetFSMCondition());
  conditionEditCtrl.SaveFocusData();
  switch(nTab)
  {
  case 0: //condition
    cond->condition.LoadFrom(conditionEdit, conditionEditCtrl);
    break;
  case 1: //action
    cond->action.LoadFrom(conditionEdit, conditionEditCtrl);
    break;
  case 2: //preCondition
    cond->preCondition.LoadFrom(conditionEdit, conditionEditCtrl);
    break;
  }
  *pResult = 0;
}

void CConditionDataDlg::OnTcnSelchangeConditiontab(NMHDR *pNMHDR, LRESULT *pResult)
{
  UpdateData();
  int nTab = conditionTab.GetCurSel();
  FSMCondition *cond = const_cast<FSMCondition *>(condValue->GetFSMCondition());
  cond->conditionTab=nTab;
  conditionEditCtrl.SaveFocusData();
  switch(nTab)
  {
  case 0: //condition
    cond->condition.SaveTo(conditionEdit, conditionEditCtrl);
    break;
  case 1: //action
    cond->action.SaveTo(conditionEdit, conditionEditCtrl);
    break;
  case 2: //preCondition
    cond->preCondition.SaveTo(conditionEdit, conditionEditCtrl);
    break;
  }
  UpdateData(false);
  CheckEditDisable();
  conditionEditCtrl.SetFocusData();
  conditionEditCtrl.SetFocus();
  *pResult = 0;
}

// CConditionDataDlg message handlers

void CConditionDataDlg::OnSize(UINT nType, int cx, int cy)
{
  CDialog::OnSize(nType, cx, cy);

  // TODO: Add your message handler code here
  CWnd *tabctrl = GetDlgItem(IDC_CONDITIONTAB);
  if (!tabctrl) return;
  CRect wrect;
  GetWindowRect(&wrect);
  CRect edrect;
  tabctrl->GetWindowRect(&edrect);
  CRect saverect;
  CWnd *saveButton = GetDlgItem(IDC_SAVECONDITIONEDIT);
  int bott;
  if (saveButton)
  {
    saveButton->GetWindowRect(&saverect);
    saveButton->SetWindowPos(&CWnd::wndBottom, 0, cy-saverect.Height(), 0, 0, SWP_NOSIZE);
    saverect.bottom = wrect.bottom - saverect.Height() - 2;
  }
  else saverect=wrect;
  bott = saverect.bottom;

  CWnd *editButton = GetDlgItem(IDC_EDITCONDITION);
  if (editButton)
  {
    CRect editrect;
    editButton->GetWindowRect(&editrect);
    editButton->SetWindowPos(&CWnd::wndBottom, editrect.Width()*1.05f, cy-editrect.Height(), 0, 0, SWP_NOSIZE);
  }

  tabctrl->SetWindowPos(&CWnd::wndBottom, 0, 0, cx, bott-edrect.top, SWP_NOMOVE);
  //CWnd *editbox = GetDlgItem(IDC_CONDITIONEDIT);
  CWnd *editbox = &conditionEditCtrl;
  tabctrl->GetWindowRect(&edrect);
  editbox->GetWindowRect(&wrect);
  editbox->SetWindowPos(&CWnd::wndBottom, 0, 0, cx-10, edrect.bottom-wrect.top-5, SWP_NOMOVE);
}

void CConditionDataDlg::SaveData()
{
  FSMCondition *cond = const_cast<FSMCondition *>(condValue->GetFSMCondition());
  UpdateData();
  bool changed = false;
  if (conditionTab.GetCurSel()==0) //action
  {
    if ( changed |= (cond->condition!=conditionEdit) ) cond->condition = conditionEdit;
  }
  else if (conditionTab.GetCurSel()==1) //condition
  {
    if ( changed |= (cond->action!=conditionEdit) ) cond->action = conditionEdit;
  }
  else if ( changed |= (cond->preCondition!=conditionEdit) ) cond->preCondition = conditionEdit;
  if ( changed |= (cond->priority!=priority) ) cond->priority = priority;
  cond->conditionTab = conditionTab.GetCurSel();

  if (changed) ((FSMDataDlgs*)GetParent())->wGraph.SaveUndo();
}

void CConditionDataDlg::OnBnClickedSaveconditionedit()
{
  SaveData();
}

RString ConvertNameToFileName(CString name)
{
  char buf[65]; *buf=0;
  int siz = name.GetLength();
  if (siz>64) siz=64;
  buf[siz]=0; //end it
  for(int i=0; i<siz; i++)
  {
    if (isalnum(name[i])) buf[i]=name[i];
    else buf[i]='_';
  }
  return buf;
}

/// Creates new temp file and fill it with the initContent
#include <io.h>
#include <sys/stat.h>
RString CreateTempFile(CString suffix, CString initContent)
{
  char bufshort[512], buf[512];
  GetTempPath(512, bufshort); //it contains trailing backslash
  if (!GetLongPathName(bufshort, buf, 512)) strcpy(buf, bufshort);
  // find first file, which does not exist in the form fsmEdFile_####.sqf
  RString sufName = ConvertNameToFileName(suffix);
  for (int i=0; i<10000; i++)
  {
    RString fName = Format("%sfsmEdFile_%s_%04d.sqf",buf, cc_cast(sufName), i);
    if (!QIFileFunctions::FileExists(fName))
    { //we have the temp file name, so create and finish
      FILE *tmpFile = fopen(fName, "w+b");
      if (tmpFile)
      {
        fwrite(initContent.GetString(), 1, initContent.GetLength(), tmpFile);
        fclose(tmpFile);
        return fName;
      }
    }
  }
  return RString(); //no file created!
}

#include "ExtEditorManager.h"
void CStateDataDlg::OnBnClickedEditState()
{
  SaveData();
  FSMState *state = const_cast<FSMState *>(stateValue->GetFSMState());
  FSMItemInfo *info = ((FSMDataDlgs*)GetParent())->wGraph.GetItemExtraDataById(state->id);
  RString tmpFileName = CreateTempFile(info ? info->text:"", stateEdit);
  gExtEdFileManager.Submit(state->id, stateTab.GetCurSel(), tmpFileName);
}

void CConditionDataDlg::OnBnClickedEditCondition()
{
  SaveData();
  FSMCondition *cond = const_cast<FSMCondition *>(condValue->GetFSMCondition());
  FSMItemInfo *info = ((FSMDataDlgs*)GetParent())->wGraph.GetItemExtraDataById(cond->id);
  RString tmpFileName = CreateTempFile(info ? info->text:"", conditionEdit);
  gExtEdFileManager.Submit(cond->id, conditionTab.GetCurSel(), tmpFileName);
}

BOOL CConditionDataDlg::PreTranslateMessage(MSG* pMsg)
{
  if (pMsg->message==WM_KEYDOWN && (GetKeyState(VK_CONTROL) & 0x80)!=0)
  {
    switch (pMsg->wParam)
    {
    case VK_TAB:
      ((CMainFrame*)AfxGetMainWnd())->OnChangeactivewnd();
      return TRUE;
    case 'S':
      ((CMainFrame*)AfxGetMainWnd())->wToolBar->OnFileSave();
      return TRUE;
    case 'A':
      conditionEditCtrl.SetSel(0,-1); //select all
      return TRUE;
    }
  }
  else
  {
    if ((pMsg->message==WM_KEYDOWN) && (pMsg->wParam==VK_TAB))
    {
      //CEdit *edit = (CEdit *)GetDlgItem(IDC_CONDITIONEDIT);
      conditionEditCtrl.SetTabStops(12);
      if (pMsg->hwnd==conditionEditCtrl.GetSafeHwnd()) conditionEditCtrl.ReplaceSel("\t");
      return TRUE;
    }
  }

  return CDialog::PreTranslateMessage(pMsg);
}


// CStateDataDlg dialog

IMPLEMENT_DYNAMIC(CStateDataDlg, CDialog)
CStateDataDlg::CStateDataDlg(CWnd* pParent /*=NULL*/)
: CDialog(CStateDataDlg::IDD, pParent)
, stateEdit(_T(""))
, stateValue(NULL)
{
}

CStateDataDlg::~CStateDataDlg()
{
}

void CStateDataDlg::DoDataExchange(CDataExchange* pDX)
{
  CDialog::DoDataExchange(pDX);
  DDX_Text(pDX, IDC_STATEEDIT, stateEdit);
  DDX_Control(pDX, IDC_STATETAB, stateTab);
  DDX_Control(pDX, IDC_STATEEDIT, stateEditCtrl);
}


BEGIN_MESSAGE_MAP(CStateDataDlg, CDialog)
  ON_WM_SIZE()
  ON_BN_CLICKED(IDC_SAVEINITCODE, OnBnClickedSaveinitcode)
  ON_BN_CLICKED(IDC_EDITSTATE, OnBnClickedEditState)
  ON_NOTIFY(TCN_SELCHANGE, IDC_STATETAB, OnTcnSelchangeStatetab)
END_MESSAGE_MAP()

BOOL CStateDataDlg::OnInitDialog()
{
  CDialog::OnInitDialog();
  TC_ITEM TabCtrlItem;
  TabCtrlItem.mask = TCIF_TEXT;
  TabCtrlItem.pszText = "InitCode";
  stateTab.InsertItem( 0, &TabCtrlItem );
  TabCtrlItem.pszText = "PreCondition";
  stateTab.InsertItem( 1, &TabCtrlItem );
//   CEdit *edit = (CEdit *)GetDlgItem(IDC_STATEEDIT);
//   edit->SetTabStops(12);
  stateEditCtrl.SetTabStops(12);
  return true;
}

// Check whether the shown data are edited in the External editor.
// If so, edit window and Edit button are disabled.
void CStateDataDlg::CheckEditDisable()
{
  int *id = stateValue->GetID();
  if (id && gExtEdFileManager.IsEdited(*id, stateTab.GetCurSel()))
  {
    stateEditCtrl.SetReadOnly(true);
    CWnd *editButton = GetDlgItem(IDC_EDITSTATE);
    if (editButton) editButton->EnableWindow(false);
  }
  else
  {
    stateEditCtrl.SetReadOnly(false);
    CWnd *editButton = GetDlgItem(IDC_EDITSTATE);
    extern CString gExternalEditor;
    if (editButton) editButton->EnableWindow(true && !gExternalEditor.IsEmpty());
  }
}

void CConditionDataDlg::CheckEditDisable()
{
  int *id = condValue->GetID();
  if (id && gExtEdFileManager.IsEdited(*id, conditionTab.GetCurSel()))
  {
    conditionEditCtrl.SetReadOnly(true);
    CWnd *editButton = GetDlgItem(IDC_EDITCONDITION);
    if (editButton) editButton->EnableWindow(false);
  }
  else
  {
    conditionEditCtrl.SetReadOnly(false);
    CWnd *editButton = GetDlgItem(IDC_EDITCONDITION);
    extern CString gExternalEditor;
    if (editButton) editButton->EnableWindow(true && !gExternalEditor.IsEmpty());
  }
}

void CStateDataDlg::OnTcnSelchangeStatetab(NMHDR *pNMHDR, LRESULT *pResult)
{
  UpdateData();
  int nTab = stateTab.GetCurSel();
  FSMState *state = const_cast<FSMState *>(stateValue->GetFSMState());
  state->stateTab=nTab;
  stateEditCtrl.SaveFocusData();
  switch(nTab)
  {
  case 0: //initCode
    state->preCondition.LoadFrom(stateEdit, stateEditCtrl);
    state->initCode.SaveTo(stateEdit, stateEditCtrl);
    break;
  case 1: //preCondition
    state->initCode.LoadFrom(stateEdit, stateEditCtrl);
    state->preCondition.SaveTo(stateEdit, stateEditCtrl);
    break;
  }
  UpdateData(false);
  CheckEditDisable();
  stateEditCtrl.SetFocusData();
  stateEditCtrl.SetFocus();
  *pResult = 0;
}

// CStateDataDlg message handlers

void CStateDataDlg::OnSize(UINT nType, int cx, int cy)
{
  CDialog::OnSize(nType, cx, cy);

  // TODO: Add your message handler code here
  CWnd *tabctrl = GetDlgItem(IDC_STATETAB);
  if (!tabctrl) return;
  CRect wrect;
  GetWindowRect(&wrect);
  CRect edrect;
  tabctrl->GetWindowRect(&edrect);
  CRect saverect;
  CWnd *saveButton = GetDlgItem(IDC_SAVEINITCODE);
  int bott;
  if (saveButton)
  {
    saveButton->GetWindowRect(&saverect);
    saveButton->SetWindowPos(&CWnd::wndBottom, 0, cy-saverect.Height(), 0, 0, SWP_NOSIZE);
    saverect.bottom = wrect.bottom - saverect.Height() - 2;
  }
  else saverect=wrect;
  bott = saverect.bottom;

  CWnd *editButton = GetDlgItem(IDC_EDITSTATE);
  if (editButton)
  {
    CRect editrect;
    editButton->GetWindowRect(&editrect);
    editButton->SetWindowPos(&CWnd::wndBottom, editrect.Width()*1.05f, cy-editrect.Height(), 0, 0, SWP_NOSIZE);
  }

  tabctrl->SetWindowPos(&CWnd::wndBottom, 0, 0, cx, bott-edrect.top, SWP_NOMOVE);
  CWnd *editbox = GetDlgItem(IDC_STATEEDIT);
  tabctrl->GetWindowRect(&edrect);
  editbox->GetWindowRect(&wrect);
  editbox->SetWindowPos(&CWnd::wndBottom, 0, 0, cx-10, edrect.bottom-wrect.top-5, SWP_NOMOVE);
}

void CStateDataDlg::SaveData()
{
  FSMState *state = const_cast<FSMState *>(stateValue->GetFSMState());
  UpdateData();
  bool changed = false;
  if (stateTab.GetCurSel()==0)
  {
    if (changed |= (state->initCode!=stateEdit) ) state->initCode = stateEdit;
  }
  else if (changed |= (state->preCondition!=stateEdit) ) state->preCondition = stateEdit;

  if (changed) ((FSMDataDlgs*)GetParent())->wGraph.SaveUndo();
}

void CStateDataDlg::OnBnClickedSaveinitcode()
{
  SaveData();
}

BOOL CStateDataDlg::PreTranslateMessage(MSG* pMsg)
{
  if (pMsg->message==WM_KEYDOWN && (GetKeyState(VK_CONTROL) & 0x80)!=0)
  {
    switch (pMsg->wParam)
    {
    case VK_TAB:
      ((CMainFrame*)AfxGetMainWnd())->OnChangeactivewnd();
      return TRUE;
    case 'S':
      ((CMainFrame*)AfxGetMainWnd())->wToolBar->OnFileSave();
      return TRUE;
    case 'A':
      stateEditCtrl.SetSel(0,-1); //select all
      return TRUE;
    }
  }
  else
  {
    if ((pMsg->message==WM_KEYDOWN) && (pMsg->wParam==VK_TAB))
    {
//       CEdit *edit = (CEdit *)GetDlgItem(IDC_STATEEDIT);
//       edit->SetTabStops(12);
      stateEditCtrl.SetTabStops(12);
      if (pMsg->hwnd==stateEditCtrl.GetSafeHwnd()) stateEditCtrl.ReplaceSel("\t");
      return TRUE;
    }
  }

  return CDialog::PreTranslateMessage(pMsg);
}

// FSMDataDlgs dialog container

void FSMDataDlgs::Create(CWnd *parent)
{
  CWnd::Create(NULL, "sth", WS_CHILD |WS_VISIBLE,CRect(0,0,0,0),parent,AFX_IDW_PANE_FIRST);
  activeDlg=DLG_NONE;
  stateDlg.Create(IDD_STATEDATA, this);
  conditionDlg.Create(IDD_CONDITIONDATA, this);
  stateDlg.ShowWindow(activeDlg==DLG_STATE ? SW_SHOW : SW_HIDE);
  conditionDlg.ShowWindow(activeDlg==DLG_CONDITION ? SW_SHOW : SW_HIDE);
  _initialized=true;
}

IMPLEMENT_DYNAMIC(FSMDataDlgs, CWnd)

BEGIN_MESSAGE_MAP(FSMDataDlgs, CWnd)
  ON_WM_SIZE()
  ON_WM_PAINT()
END_MESSAGE_MAP()

void FSMDataDlgs::OnSize(UINT nType, int cx, int cy)
{
  CWnd::OnSize(nType, cx, cy);

  // TODO: Add your message handler code here
  if (_initialized)
  {
    if (activeDlg==DLG_STATE) stateDlg.CheckEditDisable();
    else if (activeDlg==DLG_CONDITION) conditionDlg.CheckEditDisable();
    stateDlg.MoveWindow(0, 0, cx, cy);
    conditionDlg.MoveWindow(0, 0, cx, cy);
  }
}

void FSMDataDlgs::OnPaint() 
{
  CPaintDC dc(this); // device context for painting
  CRect rc;
  GetClientRect(&rc);
  dc.FillSolidRect(&rc,GetSysColor(COLOR_BTNFACE));
  if (activeDlg==DLG_STATE) stateDlg.CheckEditDisable();
  else if (activeDlg==DLG_CONDITION) conditionDlg.CheckEditDisable();
}

void FSMDataDlgs::SaveData()
{
  if (activeDlg==DLG_STATE) stateDlg.SaveData();
  else if (activeDlg==DLG_CONDITION) conditionDlg.SaveData();
}

void FSMDataDlgs::UpdateFocus(bool save)
{
  if (save) //default action - save data from dialog into variables
  {
    if (activeDlg==DLG_STATE)
    {
      stateDlg.stateEditCtrl.SaveFocusData();
      FSMState *state = const_cast<FSMState *>(stateDlg.stateValue->GetFSMState());
      switch (state->stateTab)
      {
      case 0: //initCode
        state->initCode.LoadFrom(stateDlg.stateEdit, stateDlg.stateEditCtrl);
        break;
      case 1: //preCondition
        state->preCondition.LoadFrom(stateDlg.stateEdit, stateDlg.stateEditCtrl);
        break;
      }
    }
    else if (activeDlg==DLG_CONDITION) 
    {
      conditionDlg.conditionEditCtrl.SaveFocusData();
      FSMCondition *cond = const_cast<FSMCondition *>(conditionDlg.condValue->GetFSMCondition());
      switch (cond->conditionTab)
      {
      case 0: //condition
        cond->condition.LoadFrom(conditionDlg.conditionEdit, conditionDlg.conditionEditCtrl);
        break;
      case 1: //action
        cond->action.LoadFrom(conditionDlg.conditionEdit, conditionDlg.conditionEditCtrl);
        break;
      case 2: //preCondition
        cond->preCondition.LoadFrom(conditionDlg.conditionEdit, conditionDlg.conditionEditCtrl);
        break;
      }
    }
  }
  else
  {
    if (activeDlg==DLG_STATE)
    {
      FSMState *state = const_cast<FSMState *>(stateDlg.stateValue->GetFSMState());
      switch (state->stateTab)
      {
      case 0: //initCode
        state->initCode.SaveTo(stateDlg.stateEdit, stateDlg.stateEditCtrl);
        break;
      case 1: //preCondition
        state->preCondition.SaveTo(stateDlg.stateEdit, stateDlg.stateEditCtrl);
        break;
      }
    }
    else if (activeDlg==DLG_CONDITION) 
    {
      FSMCondition *cond = const_cast<FSMCondition *>(conditionDlg.condValue->GetFSMCondition());
      switch (cond->conditionTab)
      {
      case 0: //condition
        cond->condition.SaveTo(conditionDlg.conditionEdit, conditionDlg.conditionEditCtrl);
        break;
      case 1: //action
        cond->action.SaveTo(conditionDlg.conditionEdit, conditionDlg.conditionEditCtrl);
        break;
      case 2: //preCondition
        cond->preCondition.SaveTo(conditionDlg.conditionEdit, conditionDlg.conditionEditCtrl);
        break;
      }
    }
  }
}

static char *typeTexts[] = {"Start State", "End State", "State", "User State", 
    "Condition", "Input Condition", "User Condition", "", "True Condition", "*** Wrong ***"};
LPCTSTR FSMDataDlgs::GetTypeName(DefaultItemType type)
{
//  DF_UNDEF=-1, DF_START_ITEM, DF_END_ITEM, DF_ITEM, DF_USER_ITEM, 
//  DF_CONDITION, DF_USER_INPUT_CONDITION, DF_USER_CONDITION,
//  DF_KNEE, DF_TRUE_CONDITION, DF_DEFAULT_ITEM_NUM
  if (type>=0 && type<DF_DEFAULT_ITEM_NUM) return typeTexts[type];
  else return typeTexts[DF_DEFAULT_ITEM_NUM];
}

const int ID_BUF_SIZE = 10;
static char idbuf[ID_BUF_SIZE];
void FSMDataDlgs::ActivateDlg(FSMItemValue *data, bool refreshOnly)
{
  FSMItemType type = data ? data->GetType() : -1;
  if (type == FSMStateType && data->type!=DF_KNEE)
  {
    if (activeDlg!=FSMStateType || stateDlg.stateValue!=data || refreshOnly) 
    {
      if (!refreshOnly) 
      {
        SaveData();
        UpdateFocus(); //-> save into variables
      }

      activeDlg=DLG_STATE;
      stateDlg.stateValue = (FSMStateValue *)data;
      int nTab = stateDlg.stateValue->GetFSMState()->stateTab;
      stateDlg.stateTab.SetCurSel(nTab);
      if (nTab==0) stateDlg.stateEdit = stateDlg.stateValue->GetFSMState()->initCode;
      else stateDlg.stateEdit = stateDlg.stateValue->GetFSMState()->preCondition;
      stateDlg.SetDlgItemText(IDC_STATETYPE, GetTypeName(data->type));
      stateDlg.SetDlgItemText(IDC_STATEID, itoa(stateDlg.stateValue->GetFSMState()->id, idbuf, ID_BUF_SIZE));
      stateDlg.UpdateData(FALSE);

      UpdateFocus(FALSE); //<- load from variables

      conditionDlg.ShowWindow(SW_HIDE);
      stateDlg.ShowWindow(SW_SHOW);
    }
  }
  else if (type == FSMConditionType)
  {
    if (activeDlg!=FSMConditionType || conditionDlg.condValue!=data || refreshOnly) 
    {
      if (!refreshOnly) 
      {
        SaveData();
        UpdateFocus();
      }

      activeDlg=DLG_CONDITION;
      conditionDlg.condValue = (FSMConditionValue *)data;
      int nTab = conditionDlg.condValue->GetFSMCondition()->conditionTab;
      conditionDlg.conditionTab.SetCurSel(nTab);
      if (nTab==0) conditionDlg.conditionEdit = conditionDlg.condValue->GetFSMCondition()->condition;
      else if (nTab==1) conditionDlg.conditionEdit = conditionDlg.condValue->GetFSMCondition()->action;
      else conditionDlg.conditionEdit = conditionDlg.condValue->GetFSMCondition()->preCondition;
      conditionDlg.priority = conditionDlg.condValue->GetFSMCondition()->priority;
      conditionDlg.SetDlgItemText(IDC_CONDITIONTYPE, GetTypeName(data->type));
      conditionDlg.SetDlgItemText(IDC_CONDITIONID, itoa(conditionDlg.condValue->GetFSMCondition()->id, idbuf, ID_BUF_SIZE));
      conditionDlg.UpdateData(FALSE);

      UpdateFocus(FALSE);

      stateDlg.ShowWindow(SW_HIDE);
      conditionDlg.ShowWindow(SW_SHOW);
    }
  }
  else if (activeDlg!=DLG_NONE)
  {
    if (!refreshOnly) 
    {
      SaveData();
      UpdateFocus();
    }

    stateDlg.ShowWindow(SW_HIDE);
    conditionDlg.ShowWindow(SW_HIDE);
    activeDlg = DLG_NONE;
  }
}

void FSMDataDlgs::ActivateDlg(SGraphItem &itm, bool refreshOnly)
{
  if (itm.data && ((FSMItemInfo *)itm.data)->fsmData) 
    ActivateDlg(((FSMItemInfo *)itm.data)->fsmData, refreshOnly);
  else 
    ActivateDlg(NULL, refreshOnly);
}

void FSMDataDlgs::ReleaseTouch(bool saveit)
{
  if (saveit) SaveData();
  stateDlg.ShowWindow(SW_HIDE);
  conditionDlg.ShowWindow(SW_HIDE);
  activeDlg = DLG_NONE;
}

void FSMDataDlgs::ChangeFont()
{
  // old font as stateEditCtrl font from stateDlg
  LOGFONT oldFont; 
  stateDlg.stateEditCtrl.GetFont()->GetLogFont(&oldFont);
  CFontDialog selectFontDlg(&oldFont);
  if (selectFontDlg.DoModal()==IDOK)
  {
    LOGFONT fontInfo;
    selectFontDlg.GetCurrentFont(&fontInfo);
    if (font) delete font;
    font = new CFont();
    font->CreateFontIndirect(&fontInfo);
    stateDlg.stateEditCtrl.SetFont(font);
    conditionDlg.conditionEditCtrl.SetFont(font);
  }
}

void FSMDataDlgs::StoreFontToProfile(CGraphViewApp &app)
{
  // old font as stateEditCtrl font from stateDlg
  LOGFONT font; 
  stateDlg.stateEditCtrl.GetFont()->GetLogFont(&font);
  app.WriteProfileBinary("Font", "DATA", (LPBYTE)&font, sizeof(font));
}

void FSMDataDlgs::LoadFontFromProfile(CGraphViewApp &app)
{
  LOGFONT *fontInfo;
  UINT size;
  if (app.GetProfileBinary("Font", "DATA", (LPBYTE *)&fontInfo, &size))
  {
    if (font) delete font;
    font = new CFont();
    font->CreateFontIndirect(fontInfo);
    stateDlg.stateEditCtrl.SetFont(font);
    conditionDlg.conditionEditCtrl.SetFont(font);
    delete fontInfo;
  }
}

