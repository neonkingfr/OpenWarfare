#ifndef FSMDATADLGS_H
#define FSMDATADLGS_H

#pragma once

#include "GraphView.h"
#include "FSMEditor.h"
#include "afxcmn.h"

class CEditEx:public CEdit
{
  long uservalue;
public:
  void SetFloat(float f, int precision);
  float GetFloat();
  CEditEx():CEdit() 
  {}
  long GetLong() 
  {return uservalue;}
  void SetLong(long val) 
  {uservalue=val;}
};

// CStateDataDlg dialog

class CStateDataDlg : public CDialog
{
  DECLARE_DYNAMIC(CStateDataDlg)

public:
  CStateDataDlg(CWnd* pParent = NULL);   // standard constructor
  virtual ~CStateDataDlg();

  // Dialog Data
  enum { IDD = IDD_STATEDATA };

protected:
  virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

  DECLARE_MESSAGE_MAP()
public:
  FSMStateValue *stateValue;
  CString  stateEdit;
  CCodeEdit stateEditCtrl;
  CTabCtrl stateTab;

  afx_msg void OnSize(UINT nType, int cx, int cy);
  afx_msg void OnBnClickedSaveinitcode();
  afx_msg void OnBnClickedEditState();
  void SaveData();
  virtual BOOL OnInitDialog();
  afx_msg void OnTcnSelchangeStatetab(NMHDR *pNMHDR, LRESULT *pResult);  
  BOOL PreTranslateMessage(MSG* pMsg);
  void CheckEditDisable();
};


// CConditionDataDlg dialog

class CConditionDataDlg : public CDialog
{
  DECLARE_DYNAMIC(CConditionDataDlg)

public:
  CConditionDataDlg(CWnd* pParent = NULL);   // standard constructor
  virtual ~CConditionDataDlg();

  // Dialog Data
  enum { IDD = IDD_CONDITIONDATA };

protected:
  virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

  DECLARE_MESSAGE_MAP()
public:
  FSMConditionValue *condValue;
  CString conditionEdit;
  CCodeEdit conditionEditCtrl;
  float priority;
  CTabCtrl conditionTab;

  afx_msg void OnSize(UINT nType, int cx, int cy);
  afx_msg void OnBnClickedSaveconditionedit();
  afx_msg void OnBnClickedEditCondition();

  void SaveData();
  virtual BOOL OnInitDialog();
  afx_msg void OnTcnSelchangingConditiontab(NMHDR *pNMHDR, LRESULT *pResult);
  afx_msg void OnTcnSelchangeConditiontab(NMHDR *pNMHDR, LRESULT *pResult);
  BOOL PreTranslateMessage(MSG* pMsg);
  void CheckEditDisable();
};

// FSMDataDlgs dialog container

class FSMDataDlgs : public CWnd
{
  // Construction
  DECLARE_DYNAMIC(FSMDataDlgs)

  bool _initialized;
  FSMEditor &wGraph;
  CFont *font;
//protected:
public:
  enum ActiveDialog {
    DLG_NONE, DLG_STATE, DLG_CONDITION
  } activeDlg;
  CStateDataDlg stateDlg;
  CConditionDataDlg conditionDlg;
  LPCTSTR GetTypeName(DefaultItemType type);

public:
  FSMDataDlgs(FSMEditor &fsmed) : _initialized(false), wGraph(fsmed), font(NULL) {};
  virtual ~FSMDataDlgs() {};

public:
  void ActivateDlg(FSMItemValue *data, bool refreshOnly=false);
  void ActivateDlg(SGraphItem &itm, bool refreshOnly=false);
  void SaveData();
  void UpdateFocus(bool save=true);
  void ReleaseTouch(bool saveit=true);

  // Implementation
public:
  void Create(CWnd *parent);

  void ChangeFont();
  void StoreFontToProfile(CGraphViewApp &app);
  void LoadFontFromProfile(CGraphViewApp &app);

protected:
  afx_msg void OnPaint();

public:
  DECLARE_MESSAGE_MAP()
  afx_msg void OnSize(UINT nType, int cx, int cy);
};

#endif
