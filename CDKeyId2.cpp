// CDKeyId2.cpp : Defines the entry point for the console application.
//

#include <El/elementpch.hpp>
#include <El/CDKey/serial.hpp>
#include <Wincrypt.h>

#define ENUM_NAME(name) #name,

// ArmA 2 distributions
#define A2_DISTRIBUTIONS(XX) \
  XX(US) \
  XX(Czech) \
  XX(German) \
  XX(Hungarian) \
  XX(Polish) \
  XX(Russian) \
  XX(International) \
  XX(Online) \
  XX(Brasilian)
const char *A2DistributionNames[] =
{
  A2_DISTRIBUTIONS(ENUM_NAME)
};

// OA distributions
#define OA_DISTRIBUTIONS(XX) \
  XX(US) \
  XX(English) \
  XX(German) \
  XX(Polish) \
  XX(Russian) \
  XX(Czech) \
  XX(Euro) \
  XX(Hungarian) \
  XX(Brasilian)
const char *OADistributionNames[] =
{
  OA_DISTRIBUTIONS(ENUM_NAME)
};

// BAF distributions
#define BAF_DISTRIBUTIONS(XX) \
  XX(US) \
  XX(English) \
  XX(German) \
  XX(Polish) \
  XX(Russian) \
  XX(Czech) \
  XX(Euro) \
  XX(Hungarian) \
  XX(Brasilian)
const char *BAFDistributionNames[] =
{
  BAF_DISTRIBUTIONS(ENUM_NAME)
};

// PMC distributions
#define PMC_DISTRIBUTIONS(XX) \
  XX(US) \
  XX(English) \
  XX(German) \
  XX(Polish) \
  XX(Russian) \
  XX(Czech) \
  XX(Euro) \
  XX(Hungarian) \
  XX(Brasilian)
const char *PMCDistributionNames[] =
{
  PMC_DISTRIBUTIONS(ENUM_NAME)
};

// Arma2 Free distributions
#define A2FREE_DISTRIBUTIONS(XX) \
  XX(Online)
const char *A2FreeDistributionNames[] =
{
  A2FREE_DISTRIBUTIONS(ENUM_NAME)
};

// RFT distributions
#define RFT_DISTRIBUTIONS(XX) \
  XX(US) \
  XX(English) \
  XX(German) \
  XX(Polish) \
  XX(Russian) \
  XX(Czech) \
  XX(Euro) \
  XX(Hungarian) \
  XX(Brasilian)
const char *RFTDistributionNames[] =
{
  RFT_DISTRIBUTIONS(ENUM_NAME)
};

// TOH distributions
#define TOH_DISTRIBUTIONS(XX) \
  XX(US) \
  XX(English) \
  XX(German) \
  XX(Polish) \
  XX(Russian) \
  XX(Czech) \
  XX(Euro) \
  XX(International) \
  XX(Japanese) \
  XX(Internal) \
  XX(Italian) \
  XX(Spanish)
const char *TOHDistributionNames[] =
{
  TOH_DISTRIBUTIONS(ENUM_NAME)
};

// RFT distributions
#define ARMAX_DISTRIBUTIONS(XX) \
  XX(US) \
  XX(English) \
  XX(German) \
  XX(Polish) \
  XX(Russian) \
  XX(Czech) \
  XX(Euro) \
  XX(Hungarian) \
  XX(Brasilian)
const char *ArmaXDistributionNames[] =
{
  ARMAX_DISTRIBUTIONS(ENUM_NAME)
};

// Liberation distributions
#define LIBERATION_DISTRIBUTIONS(XX) \
  XX(US) \
  XX(English) \
  XX(German) \
  XX(Polish) \
  XX(Russian) \
  XX(Czech) \
  XX(Euro) \
  XX(Hungarian) \
  XX(Brasilian)
const char *LiberationDistributionNames[] =
{
  LIBERATION_DISTRIBUTIONS(ENUM_NAME)
};

// TOH distributions
#define TOHind_DISTRIBUTIONS(XX) \
  XX(US) \
  XX(English) \
  XX(German) \
  XX(Polish) \
  XX(Russian) \
  XX(Czech) \
  XX(Euro) \
  XX(International) \
  XX(Japanese) \
  XX(Internal) \
  XX(Italian) \
  XX(Spanish)
const char *TOHindDistributionNames[] =
{
  TOHind_DISTRIBUTIONS(ENUM_NAME)
};

// CC distributions
#define CARRIER_COMMAND_DISTRIBUTIONS(XX) \
  XX(US) \
  XX(English) \
  XX(German) \
  XX(Polish) \
  XX(Russian) \
  XX(Czech) \
  XX(Euro) \
  XX(International) \
  XX(Japanese) \
  XX(Internal) \
  XX(Italian) \
  XX(Spanish)
const char *CarrierCommandDistributionNames[] =
{
  CARRIER_COMMAND_DISTRIBUTIONS(ENUM_NAME)
};

// ACR distributions
#define ACR_DISTRIBUTIONS(XX) \
  XX(US) \
  XX(English) \
  XX(German) \
  XX(Polish) \
  XX(Russian) \
  XX(Czech) \
  XX(Euro) \
  XX(International) \
  XX(Japanese) \
  XX(Internal) \
  XX(Italian) \
  XX(Spanish)
const char *ACRDistributionNames[] =
{
  ACR_DISTRIBUTIONS(ENUM_NAME)
};

// List of supported applications
struct AppInfo
{
  const char *_appName;
  const char **_distributions;
  int _distributionsCount;
  int _productId;
};
#define APP_INFO(name, distributions, productId) {#name, distributions, lenof(distributions), productId}
AppInfo Applications[] =
{
  APP_INFO(A2, A2DistributionNames, 0),
  APP_INFO(OA, OADistributionNames, 1),
  APP_INFO(BAF, BAFDistributionNames, 2),
  APP_INFO(PMC, PMCDistributionNames, 3),
  APP_INFO(A2FREE, A2FreeDistributionNames, 4),
  APP_INFO(RFT, RFTDistributionNames, 5),
  APP_INFO(TOH, TOHDistributionNames, 6),
  APP_INFO(AX, ArmaXDistributionNames, 7),
  APP_INFO(LIB, LiberationDistributionNames, 8),
  APP_INFO(HIND, TOHindDistributionNames, 9),
  APP_INFO(CC, CarrierCommandDistributionNames, 10),
};

// number of bits for a distribution id
#define DISTR_BITS 6
// number of bits for a simple hash
#define SIMPLE_HASH_BITS 4
// number of bits for a player id
#define PLAYER_BITS (32 - DISTR_BITS - SIMPLE_HASH_BITS)

/// Update 8-bit CRC value using polynomial X^8 + X^5 + X^4 + 1
static void UpdateCRC8(unsigned char &crc, unsigned char src)
{
  static const int polyVal = 0x8C;

  for (int i=0; i<8; i++)
  {
    if ((crc ^ src) & 1) crc = (crc >> 1 ) ^ polyVal;
    else crc >>= 1;
    src >>= 1;
  }
}

static bool AcquireContext(HCRYPTPROV *provider, bool privateKeyAccess)
{
  DWORD flags = privateKeyAccess ? 0 : CRYPT_VERIFYCONTEXT;
  if (CryptAcquireContext(provider, NULL, NULL, PROV_RSA_FULL, flags)) return true;
  // create a new key container
  if (GetLastError() == NTE_BAD_KEYSET)
  {
    flags |= CRYPT_NEWKEYSET;
    if (CryptAcquireContext(provider, NULL, NULL, PROV_RSA_FULL, flags)) return true;
  }
  return false;
}

static unsigned char GetKeySignature(unsigned int message)
{
  unsigned char hashByte = 0;

  // initialize the MS CryptoAPI
  HCRYPTPROV provider = NULL;
  if (AcquireContext(&provider, false))
  {
    // calculate the SHA hash
    HCRYPTHASH handle = NULL;
    if (CryptCreateHash(provider, CALG_SHA, NULL, 0, &handle))
    {
      if (CryptHashData(handle, (BYTE *)&message, sizeof(message), 0))
      {
        // export the hash
        DWORD size = 0;
        if (CryptGetHashParam(handle, HP_HASHVAL, NULL, &size, 0)) 
        {
          Temp<unsigned char> buffer(size);
          if (CryptGetHashParam(handle, HP_HASHVAL, buffer.Data(), &size, 0))
          {
            hashByte = buffer[0];
          }
        }
      }
    }
    if (handle) CryptDestroyHash(handle);
  }

  if (provider) CryptReleaseContext(provider, 0);
  return hashByte;
}

int main(int argc, char* argv[])
{
  const AppInfo *app = NULL;
  const char *key;
  
  if (argc == 2)
  {
    app = &(Applications[0]);
    printf("Warning, application name is not given, default '%s' is used.\n", app->_appName);
    key = argv[1];
  }
  else if (argc == 3)
  {
    for (int i=0; i<lenof(Applications); i++)
    {
      if (stricmp(Applications[i]._appName, argv[1]) == 0)
      {
        app = &(Applications[i]);
        break;
      }
    }
    if (app == NULL)
    {
      printf("Unsupported application: '%s'\n", argv[1]);
      printf("  list of supported applications: ");
      for (int i=0; i<lenof(Applications); i++)
      {
        if (i != 0) printf(", ");
        printf("'%s'", Applications[i]._appName);
      }
      printf("\n");
      return 1;		
    }
    key = argv[2];
  }
  else
  {
    printf("Usage:\n");
    printf("   CDKeyId2 [<Application name>] <CD Key string>\n");
    return 2;		
  }

  CDKey cdkey;
  unsigned char buffer[KEY_BYTES];
  if (!cdkey.DecodeMsg(buffer, key))
  {
    printf("   Invalid CD Key\n");
    return 3;
  }

  unsigned int message = *(unsigned int *)buffer;
  int val = message & ((1 << (PLAYER_BITS + DISTR_BITS)) - 1);
  
  printf("Player ID: %d (0x%x)\n", val, val);
  printf("Blacklist value: %u (0x%x)\n", message, message);
  int distr = message & ((1 << DISTR_BITS) - 1);
  if (distr >= app->_distributionsCount)
    printf("Unknown distribution %d\n", distr);
  else
    printf("Distribution %s\n", app->_distributions[distr]);

  unsigned char crc = 0;
  for (int j=0; j<KEY_BYTES-1; j++) UpdateCRC8(crc, buffer[j]);
  if (crc != (buffer[KEY_BYTES-1] ^ app->_productId))
    printf("- wrong key CRC (Setup check)\n");

  unsigned int signature = message >> (DISTR_BITS + PLAYER_BITS);
  unsigned int check = message & ((1 << (DISTR_BITS + PLAYER_BITS)) - 1);
  if (signature != ((((unsigned char *)&check)[0] * 7 +
    ((unsigned char *)&check)[1] * 11 +
    ((unsigned char *)&check)[2] * 13 +
    ((unsigned char *)&check)[3] * 17) & ((1 << SIMPLE_HASH_BITS) - 1)))
  {
    printf("- wrong simple signature (Game will not start)\n", distr);
  }

  if (GetKeySignature(message) != buffer[4])
  {
    printf("- wrong SHA hash (Game will FADE)\n", distr);
  }

  return 0;
}