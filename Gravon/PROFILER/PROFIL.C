#include <tos.h>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <macros.h>
#include <stdc\fileutil.h>
#include <stdc\infosoub.h>

#include "profint.h"

static char Header[]=
"PROFIL M680x0 Real Time Profiler       SUMA, Praha\n"
"(C) 1992-1994                          All Rights Reserved\n"
"Release 2-5-94                         Version for ATARI TOS\n"
"\n";
static char Usage[]=
"Usage: PROFIL [-m] | ([-f=<freqency>] [-r] <executable file> [<output file>])\n"
"\n"
"-f=NNN      frequency (50..12000 Hz)\n"
"-r=NNN      ROM analysis degree (0 - off - default)\n"
"-s=xxxxxxxx ROM area start\n"
"-e=xxxxxxxx ROM area end\n"
"-m          show manual\n"
"\n"
"Options can be set also in file PROFIL.INF in default directory.\n"
"\n";
static char Exam[]=
"Example: PROFIL PROGRAM.TOS\n"
"         PROFIL -f=1000 -r=10 OTHER.TOS\n"
"         PROFIL -m\n"
"\n";
static char Comm[]=
"PROFIL generates a time analysis (profile) showing how many per cent of the\n"
"total execution time sigle parts of the program took.\n"
"\n"
"Executable file may be any ordinary ST program with a symbol table.\n"
"Each symbol marks one part of the program, global symbols mark groups of parts.\n"
"The default frequency depends on the number of symbols in the executable file\n"
"(ordinary it is about 1000 Hz).\n"
"The default output file is located in default directory, has the same name as\n"
"the executable file and the extension .PRF.\n"
"\n"
"If you wish to profile the GEM applications, simply drop your application on\n"
"the PROFIL.GTP icon.\n"
"\n";
static char Wait[]=
"Hit any key to continue ...\n";
static char Comm2[]=
"When profiling the executable file, use it normally and try the functions you\n"
"want to optimize. When you want to finish the investigation, quit the profiled\n"
"program.\n"
"PROFIL is based on timer B (Display Enable) interrupt - so this interrupt should\n"
"not be used by the tested program.\n"
"\n";

#define Ost1Nam "other_1"
#define Ost2Nam "other_2"

typedef struct
{
	int branch;
	long tlen,dlen,blen,slen;
	long res1,res2;
	int flag;
} PH;

typedef struct
{
	char name[8];
	int type;
	long value;
} Symbol;

TextS *TSym;
long TSN;

static int FreqHz;
static Flag InRom;
static long RomDeg;
static int MinD=20; /* min v setinach procent */
static long RomStart=0xE00000L;
static long RomEnd=0x1000000L;

#define STAT 0x7fffffffL
#define GLOBAL (~STAT)

static int ComparAdr( const TextS *T, const TextS *t )
{
	long dif=T->value-t->value;
	if( dif>0 ) return +1;
	if( dif<0 ) return -1;
	return 0;
}
static int ComparSta( const TextS *T, const TextS *t )
{
	long dif=(T->stat&STAT)-(t->stat&STAT);
	if( dif>0 ) return -1;
	if( dif<0 ) return +1;
	return 0;
}

static void Sort( void )
{
	qsort(TSym,TSN-1,sizeof(*TSym),ComparAdr);
}

static void SortSym( void )
{
	qsort(TSym,TSN-1,sizeof(*TSym),ComparSta);
}

static Flag JmenoNeniVROM( const char *T )
{
	return strcmp(T,"ROM") && strcmp(T,Ost1Nam) && strcmp(T,Ost2Nam);
}

static long RSum;
static long PrintSym( FILE *out, Flag Glob )
{
	long Sum=0;
	long DSum=0;
	RSum=0;
	For( TextS *, TS, TSym, &TSym[TSN], TS++ )
		if( InRom || JmenoNeniVROM(TS->name) ) Sum+=TS->stat&STAT;
		else RSum+=TS->stat&STAT;
	Next
	For( TextS *, TS, TSym, &TSym[TSN], TS++ )
		if( InRom || JmenoNeniVROM(TS->name) )
		{
			long ts=TS->stat&STAT;
			Flag G=TS->stat&GLOBAL ? 1 : 0;
			char n[9];
			long p=ts*100/Sum;
			long pp=(ts*100-p*Sum)*100/Sum;
			strncpy(n,TS->name,8);
			n[8]=0;
			if( Glob )
			{
				if( G )
				{
					TextS *WS,*ES=&TSym[TSN];
					DSum=ts;
					for( WS=&TS[1]; WS<ES; WS++ )
					{
						long ts=WS->stat&STAT;
						Flag G=WS->stat&GLOBAL ? 1 : 0;
						if( G ) break;
						DSum+=ts;
					}
					{
						long q=DSum*100/Sum,qq=(DSum*100-q*Sum)*100/Sum;
						if( q*100+qq<MinD ) TS=&WS[-1];
						else
						{
							fprintf
							(
								out,
								"%8s = %6lx, %2ld.%02ld%%  > %2ld.%02ld%%\n",
								n,TS->value,p,pp,q,qq
							);
						}
					}
				}
				else /* !G */
				{
					if( p*100+pp>=MinD )
					{
						fprintf
						(
							out,
							"%8s = %6lx, %2ld.%02ld%%\n",
							n,TS->value,p,pp
						);
					}
				}
			}
			else
			{
				DSum+=ts;
				{
					long q=DSum*100/Sum,qq=(DSum*100-q*Sum)*100/Sum;
					if( p*100+pp>MinD || q*100+qq<90 )
					{
						fprintf
						(
							out,
							"%8s = %6lx, %2ld.%02ld%%  � %2ld.%02ld%%\n",
							n,TS->value,p,pp,q,qq
						);
					}
				}
			}
		}
	Next
	return Sum;
}

TextS *HledSym( long PC );

static int RoundHz( int log2, word Hz )
{
	lword MaxHz=24000L/log2;
	if( Hz>MaxHz ) return (int)MaxHz;
	return Hz;
}

static int FrqWas;

static int NewSym( Symbol *Sym, long nsym, int Hz )
{
	long i;
	long nsymo;
	long li;
	int log2;
	for( i=0,nsymo=0; i<nsym; i++ ) if( Sym[i].type&0x200 ) nsymo++;
	nsymo+=3;
	nsymo+=RomDeg; /* povinna analyza ROM */
	for( log2=1,li=1; li<(1L<<30); li<<=1,log2++ ) if( li>=nsymo ) break;
	TSN=nsymo=li;
	TSym=malloc(nsymo*sizeof(*TSym));
	if( TSym )
	{
		if( Hz ) Hz=RoundHz(log2,Hz);
		else Hz=RoundHz(log2,0xffffU)/4;
	}
	return Hz;
}

static TextS *ZacSym( Symbol *Sym, long nsym, long Beg, int Hz, BASPAG *BP )
{
	long i,is;
	long nsymo=TSN;
	if( TSym )
	{
		for( i=0,is=0; i<nsym; i++ )
		{
			Symbol *S=&Sym[i];
			if( S->type&0x200 )
			{
				TextS *TS=&TSym[is++];
				memcpy(TS->name,S->name,8);
				TS->value=S->value+Beg;
				TS->stat= (S->type&0x2000) ? GLOBAL : 0;
			}
		}
		{
			static TextS TSZer={Ost1Nam,0,GLOBAL};
			static TextS TSEnd={Ost2Nam,0,GLOBAL};
			static TextS TSROM={"ROM",0,0x00000000L};
			TSROM.value=RomStart;
			TSEnd.value=(long)BP->p_tbase+BP->p_tlen;
			TSym[is++]=TSZer;
			TSym[is++]=TSEnd;
			TSym[is++]=TSROM;
		}
		{
			long St=(RomEnd-TSym[is-1].value)/(nsymo-is);
			if( St<1 ) St=1;
			for( i=is; i<nsymo; i++ )
			{
				TSym[i]=TSym[i-1];
				TSym[i].value+=St;
				if( TSym[i].value>RomEnd ) TSym[i].value=RomEnd;
			}
			TSym[is-1].stat=GLOBAL;
			is=nsymo;
			TSym[nsymo-1].value=0x1000000L; /* mimo adr. prostor */
		}
		Sort();
		{
			#define MODBIN 4
			#define MODDEL 50L
			int FRQCONST=(int)(2457600L/(MODDEL*Hz))+1;
			if( FRQCONST>=256 ) FRQCONST=255;
			FrqWas=(int)(2457600L/(MODDEL*FRQCONST));
			xbios(0x1f,1,MODBIN,FRQCONST,TimerB);
		}
	}
	return TSym;
}

typedef struct
{
	unsigned year : 7;
	unsigned month : 4;
	unsigned day : 5;
	unsigned hour : 5;
	unsigned min : 6;
	unsigned sec : 5;
} DaTi;
typedef union
{
	DaTi dt;
	struct
	{
		unsigned int Dat;
		unsigned int Tim;
	} DaT;
} DT;

static void TiskSym( char *Nam, char *Prg )
{
	FILE *Out;
	Out=fopen(Nam,"w");
	if( !Out ) return;
	fprintf(Out,Header);
	fprintf(Out,"Profile of %s - frequency %d Hz, ROM analysis %s\n",strupr(Prg),FrqWas,InRom ? "on" : "off" );
	{
		DT d;
		d.DaT.Dat=Tgetdate();
		d.DaT.Tim=Tgettime();
		fprintf(Out,"%d.%d.%d ",d.dt.day,d.dt.month,d.dt.year+1980);
		fprintf(Out,"%d:%02d\n",d.dt.hour,d.dt.min);
	}
	fprintf(Out,"\nSymbol list\n");
	fprintf(Out,"  Symbol   Address\n");
	fprintf(Out,"--------  --------\n\n");
	{
		PrintSym(Out,True);
	}
	fclose(Out);
}

static void KonSym( char *Nam, char *Prg )
{
	FILE *Out;
	xbios(0x1f,1,0,0,ERRP);
	Out=fopen(Nam,"w");
	if( !Out ) return;
	fprintf(Out,Header);
	fprintf(Out,"Profile of %s - frequency %d Hz, ROM analysis %s\n",strupr(Prg),FrqWas,InRom ? "on" : "off" );
	{
		DT d;
		d.DaT.Dat=Tgetdate();
		d.DaT.Tim=Tgettime();
		fprintf(Out,"%d.%d.%d ",d.dt.day,d.dt.month,d.dt.year+1980);
		fprintf(Out,"%d:%02d\n",d.dt.hour,d.dt.min);
	}
	fprintf(Out,"\n------------- Sorted by address\n\n");
	fprintf(Out,"  Symbol   Address   Time    Group time\n");
	fprintf(Out,"--------  -------- ------    ------\n\n");
	{
		long Sum=PrintSym(Out,True);
		long p=(Sum+RSum)*100/FrqWas;
		fprintf(Out,"\nTotal execution time %ld.%0ld sec\n",p/100,p%100);
		if( RSum>0 )
		{
			p=Sum*1000/(Sum+RSum);
			fprintf(Out,"In user program %ld.01%ld%%\n",p/10,p%10);
		}
	}
	SortSym();
	fprintf(Out,"\n------------- Sorted by time\n\n");
	fprintf(Out,"  Symbol   Address   Time    Sum\n");
	fprintf(Out,"--------  -------- ------    ------\n\n");
	PrintSym(Out,False);
}

#define read(File,Co) ( sizeof(Co)==Fread(FIn,sizeof(Co),&(Co)) )

extern BASPAG *_BasPag;

static int ProcOpt( char *arg )
{
	switch( toupper(arg[1]) )
	{
		case 'F':
			if( arg[2]=='=' )
			{
				sscanf(&arg[3],"%d",&FreqHz);
				if( FreqHz<50 ) FreqHz=50;
				if( FreqHz>12000 ) FreqHz=12000;
			}
			return 0;
		case 'R':
			if( arg[2]=='=' )
			{
				sscanf(&arg[3],"%ld",&RomDeg);
				if( !RomDeg ) InRom=False;
				else InRom=True;
			}
			return 0;
		case 'S':
			if( arg[2]=='=' )
			{
				sscanf(&arg[3],"%lx",&RomStart);
			}
			return 0;
		case 'E':
			if( arg[2]=='=' )
			{
				sscanf(&arg[3],"%lx",&RomEnd);
			}
			return 0;
		case 'M':
			printf(Header);
			printf(Comm);
			printf(Comm2);
			printf(Wait);bios(2,2);
			return 1;
		default: return -1;
	}
}

int main( int argc, char *argv[] )
{
	int FIn;
	int ret=0;
	PH Head;
	char *OutNam;
	char *PrgNam;
	int argp=1;
	char *arg;
	PathName Work;
	if( argc<=1 )
	{
		Usage:
		printf(Header);
		printf(Usage);
		printf(Exam);
		printf(Wait);bios(2,2);
		return -1;
	}
	FreqHz=0;
	InRom=False;
	RomDeg=0;
	{
		FILE *f=fopen("PROFIL.INF","r");
		if( f )
		{
			char Buf[256];
			while( arg=fgetl(Buf,(int)sizeof(Buf),f),arg && arg[0]=='-' )
			{
				int c=ProcOpt(arg);
				if( c<0 ) goto Usage;
				ef( c>0 ) return 0;
			}
			fclose(f);
		}
	}
	while( arg=argv[argp],arg[0]=='-' )
	{
		int c=ProcOpt(arg);
		if( c<0 ) goto Usage;
		ef( c>0 ) return 0;
		argp++;
	}
	if( argp>=argc ) goto Usage;
	PrgNam=argv[argp++];
	if( argp<argc ) OutNam=argv[argp++];
	else
	{
		char *N;
		OutNam=Work;
		strcpy(OutNam,PrgNam);
		N=NajdiPExt(NajdiNazev(OutNam));
		strcpy(N,".PRF");
	}
	
	FIn=Fopen(PrgNam,0);
	if( FIn>=0 )
	{
		Symbol *Sym;
		static char Env[]={0,0};
		static COMMAND Com={0,""};
		BASPAG *BP;
		long Err;
		if( !read(FIn,Head) ) goto Error;
		Fseek(Head.tlen+Head.dlen,FIn,1);
		Sym=malloc(Head.slen);
		if( Sym )
		{
			long NSym;
			int NHz;
			if( Fread(FIn,Head.slen,Sym)!=Head.slen ) goto Error2;
			NSym=Head.slen/sizeof(*Sym);
			Fclose(FIn);
			NHz=NewSym(Sym,NSym,FreqHz);
			if( !TSym ) ret=-39;
			else
			{
				BP=(BASPAG *)Pexec(3,PrgNam,&Com,Env);
				Err=(long)BP;
				if( Err<0 ) {free(Sym);return (int)Err;}
				ZacSym(Sym,NSym,(long)BP->p_tbase,NHz,BP);
				if( TSym )
				{
					Pexec(4,NULL,(COMMAND *)BP,NULL);
					KonSym(OutNam,PrgNam);
				}
				else ret=-39;
			}
			free(Sym);
			return ret;
			Error2:
			free(Sym);
		}
		else ret=-39;
		Error:
		Fclose(FIn);
		return ret;
	}
	return FIn;
}