/* Gravon- akcni 3D hra */
/* SUMA 9/1994-9/1994 */

#include <tos.h>
#include <stdlib.h>
#include <stdio.h>
#include <sndbind.h>
#include <dspbind.h>
#include <macros.h>
#include <stdc\memspc.h>
#include "ginsram.h"
#include "\c\compo\cocload.h"

#define NO_LOCK 0 /* zvuk je locknuty globalne - v takovem pripade staci nastavit patricnou frekvenci */

void *myalloc( long a, Flag F ){(void)F;return malloc(a);}
void myfree( void *a ){free(a);}
void *myallocSpc( long a, Flag F, long Idtf ){(void)F,(void)Idtf;return malloc(a);}
void myfreeSpc( void *a ){free(a);}
void *mallocSpc( size_t a, long Idtf ){(void)Idtf;return malloc(a);}
void freeSpc( void *a ){free(a);}

static Flag Hraje=False;

Flag HudbaHraje( void )
{
	return Hraje;
}

int Speed=1;

void ZacHudba( const char *Nam, const char *Sam )
{
	void *SSP;
	long mt=Super((void *)1);
	if( !mt ) SSP=(void *)Super(NULL); /* __xxcmp pouziva MOVE SR,... */
	Plati( !Hraje );
	F_Display=False;
	F_Mode2=False;
	AutoRepeat=True;
	#if NO_LOCK
		SetSndFreq(CLK25K);
	#else
		if( FGloballocksnd(CLK25K)>=0 )
	#endif
	{
		FILE *f=fopen(Nam,"rb");
		Hraje=False;
		if( f )
		{
			if( NactiPisen(f,Sam,CLK25K)>=0 )
			{
				int ret=ZacHrajPisen();
				if( ret>=0 ) Hraje=True;
				else
				{
					PustPisen();
				}
			}
			else Plati( False );
			fclose(f);
		}
		else Plati( False );
		#if !NO_LOCK
		if( !Hraje ) FGlobalunlocksnd();
		#endif
	}
	if( !mt ) Super(SSP);
}
void KonHudba( void )
{
	if( Hraje )
	{
		FadeOutHudba(1500);
		Hraje=False;
		/* mel bys fade-outovat */
		KonHrajPisen();
		PustPisen();
		#if !NO_LOCK
			FGlobalunlocksnd();
		#endif
	}
}
