/* COPYINF */

int CopyRad( const char *Do, const char *Z, const char *Wild, Flag Safety, const char **NoExts, Flag Recurse );
int DoCopy( Flag BackUp, const char *CInf );

/* REORGINF */

typedef struct
{
	Flag Uzit;
	long VolnoClus; /* VolnoClus==0 -> nepouzivej tento disk */
	long ObsazClus;
	long VelClus;
} DiskInfo;

int DoReorg( int DiskR, DiskInfo *Disky );

/* import */

void MCSetNazev( const char *Naz );
void MCSetDo( const char *Text );
void MCSetZ( const char *Text );
void MCSetName( const char *Text );

enum Chyby {SyntaxErr=-100,NoRamErr,RFileErr,WFileErr,NoDiskErr};
void MCChyba( const char *Text );
void DChyba( enum Chyby Kod, const char *N );

void MCEvents( void ); /* pro obcasne prekreslovani */
