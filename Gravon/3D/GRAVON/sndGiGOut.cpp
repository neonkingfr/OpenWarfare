#include "sndGiGOut.h"

SoundCallbackFunction *SoundCallback;
FileCallbackFunction *FileCallback;
HANDLE SoundReset;

typedef size_t SoundCallbackFunction(void *buffer, size_t bufferSize, int bufferFreq);

void StartSoundGiG(int freq, SoundCallbackFunction *callback)
{
  SoundCallback = callback;
}


void EndSoundGiG()
{
  if (!SoundCallback) return;
  SoundReset = CreateEvent(NULL,TRUE,FALSE,NULL);
  SoundCallback = nullptr;
  // wait until sound callback change is confirmed
  WaitForSingleObject(SoundReset,INFINITE);
  CloseHandle(SoundReset), SoundReset = NULL;
}

void StartFileOut(FileCallbackFunction *callback)
{
  FileCallback = callback;
}
