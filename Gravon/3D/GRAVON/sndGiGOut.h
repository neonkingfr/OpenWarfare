#ifndef sndGiG_h__
#define sndGiG_h__

#include <windows.h>

#include "GiG/stdfile.h"
#ifdef __cplusplus
extern "C"
{
#endif

typedef size_t SoundCallbackFunction(void *buffer, size_t bufferSize, int bufferFreq);

extern SoundCallbackFunction *SoundCallback;
extern HANDLE SoundReset;

void StartSoundGiG(int freq, SoundCallbackFunction *callback);
void EndSoundGiG();

  
typedef int FileCallbackFunction( const char* sFileName, FileStd* pFileData );

extern FileCallbackFunction *FileCallback;

void StartFileOut(FileCallbackFunction *callback);
  
#ifdef __cplusplus
};
#endif

#endif // sndWaveOut_h__
