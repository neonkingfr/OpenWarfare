/* Gravon - uvodni a zaverecne animace */
/* SUMA 5/1994 */

#include <macros.h>
#include <stdlib.h>
#include "letvisi.h"
#include "typyvozu.h"
#include "..\predmety\predmety.h"
#include "zvuky.h"

static Flag PocAnimace,KonAnimace;
static long StupenVzniku;
static Flag Priblizeno;
#if __DEBUG
Flag RychlaAnimace=True;
#else
Flag RychlaAnimace=False;
#endif

static Flag CekamVezne;
static Flag NemesisOdletel;
static Flag RampaLock;
static Flag FZrusDym;
static int ModulHan,StojHan,DymHan;
static Vozidlo MPodvozek,MDym;

static void *OldSimProc;
static void *OldAniProc;
static void *OldZvukProc;
static void *OldZasProc;
static void *OldDesProc;
static IKresliScena *OldPodoba;

static long CasKOdletu;

#define RAK_ODP_K 40 /* prevrac. hodnota aerodyn. odporu - u kvadratu */
#define RAK_ODP_L_K 60 /* prevrac. hodnota aerodyn. odporu - u lin. clenu */
#define RAK_K_M 20 /* napodobuje hmotnost */

#define MAX_PAD -fxmetr(6)
#define MIN_PAD -fxmetr(3)
#define MAX_TAH fxmetr(230)
#define MIN_TAH fxmetr(130)

/* na rovnovazny stav staci asi 145 m/s */

#define START_MAX_TAH fxmetr(220)
#define START_ODP_K 60 /* prevrac. hodnota aerodyn. odporu - u kvadratu */
#define START_ODP_L_K 90 /* prevrac. hodnota aerodyn. odporu - u lin. clenu */
#define START_K_M 10 /* napodobuje hmotnost */

#define ZANOR_SE fxmetr(3)

static void TlumOdpor( PosT *AO, long y )
{ /* s vyskou klesa atmosfer. tlak */
	/* hustota ro= ro0 * ( 1 - 0.0000226 h ) ^ 4.3 */
	/* upravime na ro = ro0 * ( 1 - 0.0000226 h ) ^ 4 */
	long HV=HustotaVzduchu(y);
	if( HV!=fx(1) )
	{
		AO->x=MF(AO->x,HV);
		AO->y=MF(AO->y,HV);
		AO->z=MF(AO->z,HV);
	}
}

static Flag SimulujPodvozek( KrajInfo *KI, Vozidlo *Vuz, long T1, int MX, int MY, int MB, int TahD )
{
	Vozidlo *Grv=KI->Vozy[ModulHan];
	(void)MX,(void)MY,(void)MB,(void)TahD;
	(void)KI,(void)T1;
	if( PocAnimace )
	{ /* pri odletu uz podovzek neleti */
		Vuz->NPos=Grv->NPos;
		Vuz->NUhel=Grv->NUhel;
		Vuz->Kres.Rychlost=Grv->Kres.Rychlost;
	}
	return False;
}

static int RotaceDY=0,DymUhel=0;
static Flag SimulujDym( KrajInfo *KI, Vozidlo *Vuz, long T1, int MX, int MY, int MB, int TahD )
{
	Vozidlo *Grv=KI->Vozy[ModulHan];
	VznasedloT *GSpc=Grv->SpecInfo;
	(void)MX,(void)MY,(void)MB,(void)TahD;
	if( PocAnimace || KonAnimace )
	{
		Vuz->NPos=Grv->NPos;
		Vuz->NUhel=Grv->NUhel;
		Vuz->Kres.Rychlost=Grv->Kres.Rychlost;
	}
	if( Vuz->Kres.Zvetseni>fx(0.1) )
	{
		int RotDym=(KRand(&KI->KD.Seed)%31-15)*UhelPom;
		long ZvZm;
		ZvZm=DF(GSpc->Tah,MAX_TAH)+fx(0.5)-Vuz->Kres.Zvetseni;
		if( ZvZm>+fx(0.5) ) ZvZm=+fx(0.5);
		ef( ZvZm<-fx(0.5) ) ZvZm=-fx(0.5);
		ZvZm+=DF(KRand(&KI->KD.Seed)%1000-500,1000);
		RotaceDY+=RotDym;
		if( RotaceDY>+45*UhelPom ) RotaceDY=+45*UhelPom;
		ef( RotaceDY<-45*UhelPom ) RotaceDY=-45*UhelPom;
		DymUhel=NormUhel(DymUhel+(int)MF(RotaceDY,T1));
		Vuz->NUhel.y=NormUhel(Vuz->NUhel.y+DymUhel);
		ZvZm=Vuz->Kres.Zvetseni+MF(ZvZm,T1);
		if( ZvZm>fx(1.8) ) ZvZm=fx(1.8);
		ef( ZvZm<fx(0.5) ) ZvZm=fx(0.5);
		Vuz->Kres.Zvetseni=ZvZm;
	}
	if( FZrusDym ) return True;
	return False;
}

/* Dym ma animovane body */

static const int DymDela[]={0,1,2,3,4,5,6,7,8,9,10,11,12,-1};
#define N_DYM ( (int)lenof(DymDela)-1 )
static PosT DDymPos0[N_DYM]; /* orig. pozice bodu */
static PosT DDymStred;
#define DYM_NUL 13

void InitDym( void )
{
	const int *I;
	int NI;
	IBod *VB=Moddym.IK.Body;
	DDymStred=VB[DYM_NUL].Pos;
	for( NI=0,I=DymDela; *I>=0; I++,NI++ )
	{
		int i=*I;
		DDymPos0[NI].x=VB[i].Pos.x-DDymStred.x;
		DDymPos0[NI].y=VB[i].Pos.y-DDymStred.y;
		DDymPos0[NI].z=VB[i].Pos.z-DDymStred.z;
	}
}

void ZrusDym( void )
{
	const int *I;
	int NI;
	IBod *VB=Moddym.IK.Body;
	for( NI=0,I=DymDela; *I>=0; I++,NI++ )
	{
		int i=*I;
		VB[i].Pos.x=DDymPos0[NI].x+DDymStred.x;
		VB[i].Pos.y=DDymPos0[NI].y+DDymStred.y;
		VB[i].Pos.z=DDymPos0[NI].z+DDymStred.z;
	}
}

void AnimujDym( IKresliScena *KS, long VelDymuXZ, long VelDymuY )
{
	int NI;
	const int *I;
	IBod *VB=KS->Body;
	for( NI=0,I=DymDela; *I>=0; I++,NI++ )
	{
		int i=*I;
		IBod *D=&VB[i];
		D->Pos.x=MF(DDymPos0[NI].x,VelDymuXZ)+DDymStred.x;
		D->Pos.y=MF(DDymPos0[NI].y,VelDymuY)+DDymStred.y;
		D->Pos.z=MF(DDymPos0[NI].z,VelDymuXZ)+DDymStred.z;
	}
  TouchObj(KS);
}

void RotujDym( IKresliScena *KS, int DymUhel )
{
	PosT M[3];
	SmerT u;
	int NI;
	const int *I;
	IBod *VB=KS->Body;
	u.x=u.z=0;
	u.y=DymUhel;
	MRot(M,&u);
	for( NI=0,I=DymDela; *I>=0; I++,NI++ )
	{
		int i=*I;
		IBod *D=&VB[i];
		Rotace(&D->Pos,M);
	}
  TouchObj(KS);
}

void InitMorf( void )
{
	InitDym();
}
void TermMorf( void )
{
	ZrusDym();
}

static MorfInfo VznikMI;
static Flag MorfujiSe=False;

static void SaveGravon( Vozidlo *Vuz )
{
	OldSimProc=Vuz->Simulace;
	OldZasProc=Vuz->Zasah;
	OldDesProc=Vuz->Destructor;
	OldZvukProc=Vuz->Zvuky;
	OldAniProc=Vuz->Animace;
	OldPodoba=Vuz->Kres.Podoba;
}

static void OpetGravon( Vozidlo *Vuz )
{
	Vuz->Animace=OldAniProc; /* zacni simulovat normalne */
	Vuz->Simulace=OldSimProc; /* zacni simulovat normalne */
	Vuz->Zasah=OldZasProc; /* zacni simulovat normalne */
	Vuz->Destructor=OldDesProc; /* zacni simulovat normalne */
	Vuz->Zvuky=OldZvukProc; /* zacni simulovat normalne */
	Vuz->Druh=DVznasedlo;
	UvolniKopPredm(Vuz->Kres.Podoba);
	Vuz->Kres.Podoba=OldPodoba;
}

static Flag SimulujVznik( KrajInfo *KI, Vozidlo *Vuz, long T1, int MX, int MY, int MB, int TahD )
{
	(void)MX,(void)MY,(void)MB,(void)TahD;
	if( StupenVzniku>=fx(1) )
	{
		OpetGravon(Vuz);
		CasKOdletu=CasSimulace+30000L; /* odlet nejdrive za 30 sec */
		StrihDoKokpitu();
		Promluv(SpReady);
		RychlaAnimace=True; /* priste uz to zkrat */
		return False;
	}
	{
		long DV=MF(T1,fx(0.15));
		if( AktMorf(&VznikMI,DV) ) MorfujiSe=False;
		MPodvozek.NPos.y-=MF(ZANOR_SE,DV); /* zapust podvozek */
		Vuz->NPos.y+=MF(VZN_SILA_POLSTARE,DV); /* vypust vznasedlo */
		StupenVzniku+=DV;
		(void)KI;
	}
	return False;
}

static Flag SimulujPristani( KrajInfo *KI, Vozidlo *Vuz, long T1, int MX, int MY, int MB, int TahD )
{
	KrajDef *KD=&KI->KD;
	PosT AMotor,AOdpor,AG; /* zrychleni - motor, odpor, gravitace */
	long dx,dz,py;
	VznasedloT *Spc=Vuz->SpecInfo;
	KresliVozidlo *V=&Vuz->Kres;
	/* muze byt i mrtvy */
	/* simuluj gravitaci a pady */
	(void)MX,(void)MY,(void)MB,(void)TahD;
	if( !Priblizeno )
	{
		static Kamera KamPribliz=
		{
			{0,fxmetr(-1),-fxmetr(70.0)},
			{15*UhelPom,6*UhelPom,0},
			PohStopuj,
		};
		StrihKamera(&KamPribliz,RychlaAnimace ? fx(5) : fx(30) );
		Priblizeno=True;
	}
	py=UrovenVBode(KD,V->PosVozu.x,V->PosVozu.z,&dx,&dz);
	py=V->PosVozu.y-py;
	if( py<fxmetr(800) ) /* zacni brzdit */
	{
		long t;
		long MaxPad,MinPad;
		if( MDym.Kres.Zvetseni<=fx(0.1) )
		{
			MDym.Kres.Zvetseni=fx(0.2);
		}
		/* udrzuj rychlost */
		#define PAD_VYS fxmetr(150)
		#define SEST_VYS fxmetr(10)
		if( py>PAD_VYS ) MaxPad=MAX_PAD*3,MinPad=MIN_PAD*3;
		ef( py>SEST_VYS )
		{
			long k;
			k=MKDK(py-SEST_VYS,fx(2),PAD_VYS-SEST_VYS)+fx(1);
			MaxPad=MF(MAX_PAD,k),MinPad=MF(MIN_PAD,k);
		}
		else MinPad=MIN_PAD,MaxPad=MAX_PAD;
		if( Spc->LSpd.y<MaxPad ) t=MAX_TAH;
		ef( Spc->LSpd.y>MinPad ) t=MIN_TAH;
		else
		{
			long d=MinPad-MaxPad;
			t=MKDF(-Spc->LSpd.y+MinPad,MAX_TAH-MIN_TAH,d)+MIN_TAH;
		}
		Spc->Tah=t;
	}
	else Spc->Tah=0;
	/* odpor je o=v * |v| * k * brzdny koef */
	Spc->VelSpd=VelVec(&Spc->LSpd)<<MerLog; /* chceme pro nasobeni */
	AOdpor.z=MF(Spc->LSpd.z,Spc->VelSpd)/(RAK_ODP_K*RAK_K_M)+Spc->LSpd.z/(RAK_ODP_L_K*RAK_K_M);
	AOdpor.x=MF(Spc->LSpd.x,Spc->VelSpd)/(RAK_ODP_K*RAK_K_M)+Spc->LSpd.x/(RAK_ODP_L_K*RAK_K_M);
	AOdpor.y=MF(Spc->LSpd.y,Spc->VelSpd)/(RAK_ODP_K*RAK_K_M)+Spc->LSpd.y/(RAK_ODP_L_K*RAK_K_M);
	TlumOdpor(&AOdpor,V->PosVozu.y);
	Rotace(&AOdpor,Vuz->AR); /* rel. vzhledem k lodi */
	/* bocni napor zpusobuje i otaceni */
	AOdpor.z*=4; /* brzdne desky */
	AOdpor.x*=4; /* vyrazne brzdi dopredu, trochu i do stran */
	Rotace(&AOdpor,Vuz->RA); /* zpatky - absolutne */
	AMotor.y=Spc->Tah/RAK_K_M;
	AMotor.x=AMotor.z=0;
	Rotace(&AMotor,Vuz->RA); /* jedn. vektor - ve smeru pohledu */
	/* simulace zasob */
	/* zrychleni */
	AG.y=-K_G;
	AG.x=AG.z=0; /* pridej zrychleni do stran */
	Spc->LSpd.x+=MF(AMotor.x-AOdpor.x+AG.x,T1);
	Spc->LSpd.y+=MF(AMotor.y-AOdpor.y+AG.y,T1);
	Spc->LSpd.z+=MF(AMotor.z-AOdpor.z+AG.z,T1);
	Spc->RotaceV.y+=(KRand(&KI->KD.Seed)%11-5)*UhelPom;
	if( Spc->RotaceV.y>+30*UhelPom ) Spc->RotaceV.y=+30*UhelPom;
	ef( Spc->RotaceV.y<-30*UhelPom ) Spc->RotaceV.y=-30*UhelPom;
	Vuz->NUhel.x=NormUhel(V->UhelVozu.x+(int)MF(Spc->RotaceV.x,T1));
	Vuz->NUhel.z=NormUhel(V->UhelVozu.z+(int)MF(Spc->RotaceV.z,T1));
	Vuz->NUhel.y=NormUhel(V->UhelVozu.y+(int)MF(Spc->RotaceV.y,T1));
	/* odhadni posun */
	Vuz->NPos.x=V->PosVozu.x+MF(Spc->LSpd.x,T1);
	Vuz->NPos.y=V->PosVozu.y+MF(Spc->LSpd.y,T1);
	Vuz->NPos.z=V->PosVozu.z+MF(Spc->LSpd.z,T1);
	/* zjisti parametry povrchu pod (nebo i nad?) nami */
	py=UrovenVBode(KD,Vuz->NPos.x,Vuz->NPos.z,&dx,&dz);
	/* uroven povrchu */
	if( Vuz->NPos.y<py )
	{ /* dopadli jsme na povrch */
		Spc->LSpd.x=Spc->LSpd.y=Spc->LSpd.z=0;
		Spc->Tah=0;
		Spc->TahW=0;
		Spc->TahT=VZN_TAH_MAX/2;
		Spc->RotaceV.x=Spc->RotaceV.y=Spc->RotaceV.z=0;
		Vuz->NPos.y=py; /* nedovol byt pod */
		MPodvozek.NPos=Vuz->NPos;
		Spc->VolnyPad=False;
		FZrusDym=True;
		StupenVzniku=0;
		ZacMorf(&VznikMI,&Modtrasg.IK,Vuz->Kres.Podoba);
		MorfujiSe=True;
		PocAnimace=False;
		Vuz->Simulace=SimulujVznik;
		StrihDozadu(Vuz);
	}
	V->Rychlost=Spc->LSpd;
	return False;
}

static void ZvukyModulu( Vozidlo *V, const StatickaKamera *K, Flag Uvnitr )
{
	VznasedloT *Spc=V->SpecInfo;
	PosT Motor={fxmetr(0),fxmetr(+2.0),fxmetr(0.1)};
	PosT Svist={fxmetr(0),fxmetr(+1.8),fxmetr(+0.0)};
	PosT Jede=Spc->LSpd;
	long Hlasitost = 0;
	long Frekvence = 0;
	AbsKVozu(&Motor,V);RelKeKamere(&Motor,K);
	AbsKVozu(&Svist,V);RelKeKamere(&Svist,K);
	JedeKeKamere(&Jede,K);
	if( Spc->Tah )
	{
		long TahK=DF(labs(Spc->Tah),MAX_TAH);
		Frekvence=MF(DefZvuky[ILaser].KrokFrq>>7,(TahK>>1)+fx(0.75));
		Hlasitost=MF(MAX_HLASITOST/6,TahK)+MAX_HLASITOST/60;
	}
	if( Uvnitr ) Hlasitost>>=3;
	Spc->MotorHan=SpustZvuk
	(
		ILaser,&Motor,&Jede,
		Hlasitost,Frekvence,DefZvuky[ILaser].KrokVol<<8,
		fx(1.0),Spc->MotorHan
	);
	{
		/* VelSpd je fx( rychlost v m/s ) */
		long RychK=DF(Spc->VelSpd,fx(60));
		Frekvence=MF(DefZvuky[ISvist].KrokFrq>>7,(RychK>>1)+fx(0.75));
		Hlasitost=MF(RychK,RychK);
		Hlasitost=MF(Hlasitost,MAX_HLASITOST/6);
	}
	if( Uvnitr ) Hlasitost>>=4;
	Spc->SvistHan=SpustZvuk
	(
		ISvist,&Svist,&Jede,
		Hlasitost,Frekvence,DefZvuky[ISvist].KrokVol<<8,
		fx(1.0),Spc->SvistHan
	);
}

static void ZasahModulu( KrajInfo *KI, Vozidlo *V, Vozidlo *Cim, long Spd, long T1 )
{
	(void)Cim;
	(void)T1;
	(void)Spd;
	if( Cim->VHandle!=DymHan && Cim->VHandle!=StojHan )
	{
		PosT Vyb;
		Vyb=V->Kres.PosVozu;
		Vyb.x+=V->ObalS.x;
		Vyb.y+=V->ObalS.y;
		Vyb.z+=V->ObalS.z;
		ZalozVybuch(KI,&Vyb,fx(1));
	}
}

void ZaverAnimaci( KrajInfo *KI, int GravH, IKresliScena *Tvar );

static void ZasahPodvozku( KrajInfo *KI, Vozidlo *V, Vozidlo *Cim, long Spd, long T1 )
{
	if( PocAnimace ) return;
	(void)T1;
	(void)V;
	if( RampaLock ) return;
	if( StupenVzniku>=fx(1.0) && CasSimulace-CasKOdletu>0 && Spd<fxmetr(12) )
	{
		if( Cim->VHandle==GravHandle ) ZaverAnimaci(KI,Cim->VHandle,&Modtrasg.IK);
		ef( Cim->VHandle==VezenHandle ) ZaverAnimaci(KI,Cim->VHandle,&Modtrasg.IK);
		ef( Cim->VHandle==NemesHandle ) ZaverAnimaci(KI,Cim->VHandle,&Modtrasn.IK);
		/* narazil do nas Gravon nebo spojenec */
	}
}

int PrivesNaVuz( KrajInfo *OI, Vozidlo *Vuz, Vozidlo *Na, IKresliScena *Druh )
{
	memset(Vuz,0,sizeof(*Vuz));
	Vuz->Simulace=NULL;
	Vuz->Kres.Podoba=Druh;
	Vuz->Kres.Zvetseni=fx(1.0);
	Vuz->Druh=DDym;
	UrciObalVozu(Vuz);
	Vuz->Kres.PosVozu=Na->NPos;
	Vuz->Kres.UhelVozu=Na->NUhel;
	Vuz->NPos=Vuz->Kres.PosVozu;
	Vuz->NUhel=Vuz->Kres.UhelVozu;
	return PridejVuz(OI,Vuz);
}

static void ModulDestruct( KrajInfo *KI, Vozidlo *V )
{
	if( MorfujiSe ) PustMorf(&VznikMI);
	OpetGravon(V);
	V->Destructor(KI,V);
}

static Kamera KamPocat=
{
	{fxmetr(100),fxmetr(-50),-fxmetr(100.0)},
	{15*UhelPom,0*UhelPom,0},
	PohZvenku,
};

void ZacniAnimaci( KrajInfo *KI, int GravH )
{
	Vozidlo *Vuz=KI->Vozy[GravH];
	VznasedloT *Spc=Vuz->SpecInfo;
	ModulHan=GravH;
	SaveGravon(Vuz);
	Vuz->Simulace=SimulujPristani;
	Vuz->Zasah=ZasahModulu;
	Vuz->Destructor=ModulDestruct;
	Vuz->Zvuky=ZvukyModulu;
	Vuz->Animace=NULL;
	Vuz->Kres.Podoba=KopiePredm(&Modmodul.IK);
	Plati( Vuz->Kres.Podoba );
	Spc->VolnyPad=True;
	if( RychlaAnimace )
	{
		Spc->LSpd.y=-K_G*2; /* uz zabrzdeno */
		Vuz->NPos.y+=fxmetr(200);
	}
	else
	{
		Vuz->NPos.y+=fxmetr(3700); /* pristavame z vysky */
		Spc->LSpd.y=-fxmetr(100); /* prilitame hodne rychle */
	}
	Vuz->Kres.PosVozu=Vuz->NPos;
	Vuz->Kres.Rychlost=Spc->LSpd;
	Vuz->Druh=DUFO; /* neznamy druh */
	PodvHandle=StojHan=PrivesNaVuz(KI,&MPodvozek,Vuz,&Modstoj.IK);
	MPodvozek.Druh=DPodv;
	MPodvozek.ObalRY=fxmetr(6); /* ucinkuje nahoru i dolu */
	MPodvozek.ObalRXZ=fxmetr(8);
	MPodvozek.Velikost=fxmetr(8);
	MPodvozek.Druh=DPodv;
  MPodvozek.KolizeBezMinmax = True;
	MinMaxZObalu(&MPodvozek);
	MPodvozek.Simulace=SimulujPodvozek;
	MPodvozek.Zasah=ZasahPodvozku;
	DymHan=PrivesNaVuz(KI,&MDym,Vuz,&Moddym.IK);
	if( RychlaAnimace )	MDym.Kres.Zvetseni=fx(1.0);
	else MDym.Kres.Zvetseni=fx(0.0);
	AnimujDym(&Moddym.IK,fx(1.0),fx(1.0));
	MDym.Simulace=SimulujDym;
	MDym.ObalRY=MDym.ObalRXZ=0;
	MDym.ObalS.x=MDym.ObalS.y=MDym.ObalS.z=0;
	MDym.Velikost=fxmetr(8);
  MDym.KolizeBezMinmax = False;
	FZrusDym=False;
	Priblizeno=False;
	PocAnimace=True;
	KonAnimace=False;
	RampaLock=False;
	CekamVezne=VezenHandle>=0;
	NemesisOdletel=False;
	StrihKamera(&KamPocat,fx(0));
}

void UmistiPodvozek( KrajInfo *OI, const PosT *Pos )
{ /* pro Nemesis */
	memset(&MPodvozek,0,sizeof(MPodvozek));
	MPodvozek.Simulace=NULL;
	MPodvozek.Kres.Podoba=&Modstoj.IK;
	MPodvozek.Kres.Zvetseni=fx(1.0);
	MPodvozek.Druh=DPodv;
	UrciObalVozu(&MPodvozek);
	MPodvozek.Kres.PosVozu=*Pos;
	MPodvozek.Kres.PosVozu.y=UrovenVBode(&OI->KD,Pos->x,Pos->z,NULL,NULL);
	MPodvozek.Kres.PosVozu.y-=ZANOR_SE;
	MPodvozek.Kres.UhelVozu.x=MPodvozek.Kres.UhelVozu.y=MPodvozek.Kres.UhelVozu.z=0;
	MPodvozek.NPos=MPodvozek.Kres.PosVozu;
	MPodvozek.NUhel=MPodvozek.Kres.UhelVozu;
	PodvHandle=StojHan=PridejVuz(OI,&MPodvozek);
	MPodvozek.ObalRY=fxmetr(8); /* ucinkuje nahoru i dolu */
	MPodvozek.ObalRXZ=fxmetr(8);
	MPodvozek.Velikost=fxmetr(8);
	MPodvozek.Druh=DPodv;
  MPodvozek.KolizeBezMinmax = True;
	MinMaxZObalu(&MPodvozek);
	MPodvozek.Simulace=SimulujPodvozek;
	MPodvozek.Zasah=ZasahPodvozku;
	CasKOdletu=CasSimulace+30000L; /* odlet nejdrive za 30 sec */
	StupenVzniku=fx(1);
	NemesisOdletel=False;
	RampaLock=False;
	PocAnimace=False;
	KonAnimace=False;
	FZrusDym=False;
	DymHan=-1;
}

static long CasKonce;
static Flag Vzdaleno;

static Flag SimulujOdlet( KrajInfo *KI, Vozidlo *Vuz, long T1, int MX, int MY, int MB, int TahD )
{
	PosT AMotor,AOdpor,AG; /* zrychleni - motor, odpor, gravitace */
	VznasedloT *Spc=Vuz->SpecInfo;
	KresliVozidlo *V=&Vuz->Kres;
	static Flag Zvetsen;
	/* simuluj gravitaci a pady */
	(void)KI;
	(void)MX,(void)MY,(void)MB,(void)TahD;
	if( MDym.Kres.Zvetseni<=fx(0.1) ) MDym.Kres.Zvetseni=fx(0.2),Zvetsen=False;
	{
		long DT=START_MAX_TAH-Spc->Tah;
		if( DT>0 )
		{
			Spc->Tah+=ZaCasSMezemi(DT,0,START_MAX_TAH/25,T1);
			if( Spc->Tah>START_MAX_TAH ) Spc->Tah=START_MAX_TAH;
		}
	}
	/* odpor je o=v * |v| * k * brzdny koef */
	Spc->VelSpd=VelVec(&Spc->LSpd)<<MerLog; /* chceme pro nasobeni */
	AOdpor.z=MF(Spc->LSpd.z,Spc->VelSpd)/(START_ODP_K*START_K_M)+Spc->LSpd.z/(START_ODP_L_K*START_K_M);
	AOdpor.x=MF(Spc->LSpd.x,Spc->VelSpd)/(START_ODP_K*START_K_M)+Spc->LSpd.x/(START_ODP_L_K*START_K_M);
	AOdpor.y=MF(Spc->LSpd.y,Spc->VelSpd)/(START_ODP_K*START_K_M)+Spc->LSpd.y/(START_ODP_L_K*START_K_M);
	TlumOdpor(&AOdpor,V->PosVozu.y);
	AMotor.y=Spc->Tah/START_K_M;
	AMotor.x=AMotor.z=0;
	Rotace(&AMotor,Vuz->RA); /* jedn. vektor - ve smeru pohledu */
	/* simulace zasob */
	/* zrychleni */
	AG.y=-K_G;
	AG.x=AG.z=0; /* pridej zrychleni do stran */
	Spc->LSpd.x+=MF(AMotor.x-AOdpor.x+AG.x,T1);
	Spc->LSpd.y+=MF(AMotor.y-AOdpor.y+AG.y,T1);
	Spc->LSpd.z+=MF(AMotor.z-AOdpor.z+AG.z,T1);
	Spc->RotaceV.y+=(KRand(&KI->KD.Seed)%11-5)*UhelPom;
	{
		long ProtY,ZuzXZ;
		ProtY=DF(Spc->LSpd.y,fxmetr(20));
		if( ProtY<fx(0.7) )  ProtY=fx(0.7);
		if( ProtY>fx(2.0) )  ProtY=fx(2.0);
		if( Spc->LSpd.y>fxmetr(10) ) ZuzXZ=DF(fxmetr(10),Spc->LSpd.y);
		else ZuzXZ=fx(1.0);
		AnimujDym(&Moddym.IK,ZuzXZ,ProtY);
		if( Zvetsen && MDym.Kres.Zvetseni<fx(0.9) ) MDym.Kres.Zvetseni=fx(0.9);
		ef( MDym.Kres.Zvetseni>fx(1.1) )
		{
			Zvetsen=True;
			MDym.Kres.Zvetseni=fx(1.1);
		}
	}
	if( Spc->RotaceV.y>+30*UhelPom ) Spc->RotaceV.y=+30*UhelPom;
	ef( Spc->RotaceV.y<-30*UhelPom ) Spc->RotaceV.y=-30*UhelPom;
	Vuz->NUhel.x=NormUhel(V->UhelVozu.x+(int)MF(Spc->RotaceV.x,T1));
	Vuz->NUhel.z=NormUhel(V->UhelVozu.z+(int)MF(Spc->RotaceV.z,T1));
	Vuz->NUhel.y=NormUhel(V->UhelVozu.y+(int)MF(Spc->RotaceV.y,T1));
	/* odhadni posun */
	Vuz->NPos.x=V->PosVozu.x+MF(Spc->LSpd.x,T1);
	Vuz->NPos.y=V->PosVozu.y+MF(Spc->LSpd.y,T1);
	Vuz->NPos.z=V->PosVozu.z+MF(Spc->LSpd.z,T1);
	/* zjisti parametry povrchu pod (nebo i nad?) nami */
	if( Vuz->NPos.y<MPodvozek.NPos.y )
	{
		Vuz->NPos.y=MPodvozek.NPos.y;
		Spc->LSpd.y=0;
	}
	if( CasSimulace>CasKonce )
	{
		if( ModulHan==GravHandle )
		{
			if( Spc->MaSplnit.Vsechny==Spc->Splneno.Vsechny && !CekamVezne && !NemesisOdletel && NemesHandle<0 ) UkonciMisi(MisSplnil,0);
			else UkonciMisi(MisUstup,0);
			Terminate();
		}
		ef( ModulHan==VezenHandle )
		{
			/* zase zabo� rampu */
			if( CasSimulace<CasKonce+4000 )
			{
				MPodvozek.NPos.y-=MF(ZANOR_SE,T1>>2); /* vynor podvozek */
			}
			else
			{
				StupenVzniku=fx(1);
				RampaLock=False;
				ZrusVuz(KI,DymHan);
				DymHan=-1;
				OpetGravon(Vuz);
				if( GravHandle>=0 )
				{
					Vozidlo *G=KI->Vozy[GravHandle];
					VznasedloT *GSpc=G->SpecInfo;
					GSpc->Splneno.Jedn.Vysvobod=True;
					Promluv(SpDestFailed);
				}
				CekamVezne=False;
				return True; /* zru� se */
			}
		}
		ef( ModulHan==NemesHandle )
		{
			if( CasSimulace<CasKonce+4000 )
			{
				MPodvozek.NPos.y-=MF(ZANOR_SE,T1>>2); /* vynor podvozek */
			}
			else
			{
				StupenVzniku=fx(1);
				RampaLock=False;
				ZrusVuz(KI,DymHan);
				DymHan=-1;
				OpetGravon(Vuz);
				NemesisOdletel=True; /* prohr�l jsi */
				Promluv(SpFailed);
				return True; /* zru� se */
			}
		}
	}
	V->Rychlost=Spc->LSpd;
	return False;
}

static PosT SGPos,EGPos;
static SmerT SGSmer;

static Flag SimulujZabal( KrajInfo *KI, Vozidlo *Vuz, long T1, int MX, int MY, int MB, int TahD )
{
	(void)MX,(void)MY,(void)MB,(void)TahD;
	if( StupenVzniku<=fx(0) )
	{
		if( ModulHan==GravHandle )
		{
			static const Kamera KamZez= /* pro velke efekty */
			{
				{fxmetr(-5),fxmetr(10),-fxmetr(35.0)}, /* nade dnem a za prumetnou */
				{-8*UhelPom,0*UhelPom,0},
				PohHledaStat,
			};
			static Kamera KamZezS;
			KamZezS=KamZez;
			HledamVuz=GravHandle;
			StrihKamera(&KamZezS,fx(2));
		}
		Vuz->Simulace=SimulujOdlet; /* zacni simulovat normalne */
		UvolniKopPredm( Vuz->Kres.Podoba );
		Vuz->Kres.Podoba=KopiePredm(&Modmodul.IK);
		Plati( Vuz->Kres.Podoba );
		UrciObalVozu(Vuz);
		Vuz->Animace=NULL;
		Vuz->NPos=MPodvozek.NPos;
		CasKonce=CasSimulace+33000L;
		KonAnimace=True;
		Vzdaleno=False;
		return False;
	}
	{
		long DV=MF(T1,fx(0.15));
		if( AktMorf(&VznikMI,DV) ) MorfujiSe=False;
		MPodvozek.NPos.y+=MF(ZANOR_SE,DV); /* vynor podvozek */
		if( StupenVzniku>fx(0.75) )
		{
			long StZaniku=(fx(1.0)-StupenVzniku)*4;
			Vuz->NPos.x=MF(StZaniku,EGPos.x-SGPos.x)+SGPos.x;
			Vuz->NPos.y=MF(StZaniku,EGPos.y-SGPos.y)+SGPos.y;
			Vuz->NPos.z=MF(StZaniku,EGPos.z-SGPos.z)+SGPos.z;
			Vuz->NUhel.x=SGSmer.x-(int)MF(StZaniku,SGSmer.x);
			Vuz->NUhel.y=SGSmer.y-(int)MF(StZaniku,SGSmer.y);
			Vuz->NUhel.z=SGSmer.z-(int)MF(StZaniku,SGSmer.z);
		}
		else
		{
			Vuz->NPos=EGPos;
			Vuz->NUhel.x=Vuz->NUhel.y=Vuz->NUhel.z=0;
		}
		StupenVzniku-=DV;
		(void)KI;
	}
	return False;
}

static void ZaverAnimaci( KrajInfo *KI, int Handle, IKresliScena *Tvar )
{
	Vozidlo *Vuz=KI->Vozy[Handle];
	VznasedloT *Spc=Vuz->SpecInfo;
	ModulHan=Handle;
	SaveGravon(Vuz);
	Vuz->Simulace=SimulujZabal;
	Vuz->Zasah=ZasahModulu;
	Vuz->Destructor=ModulDestruct;
	Vuz->Zvuky=ZvukyModulu;
	Vuz->Animace=NULL;
	Vuz->Kres.Podoba=KopiePredm(Tvar);
	Vuz->Druh=DUFO;
	Plati( Vuz->Kres.Podoba );
	Spc->LSpd.x=Spc->LSpd.y=Spc->LSpd.z=0;
	Spc->RotaceV.x=Spc->RotaceV.y=Spc->RotaceV.z=0;
	Spc->TahW=Spc->Tah=0;
	ZacMorf(&VznikMI,&Modmodul.IK,Vuz->Kres.Podoba);
	UrciObalVozu(Vuz);
	MorfujiSe=True;
	Spc->MotorHan=VypniZvuk(Spc->MotorHan);
	Spc->SvistHan=VypniZvuk(Spc->SvistHan);
	Spc->SvetloHan=ZrusLampu(Spc->SvetloHan);
	DymHan=PrivesNaVuz(KI,&MDym,Vuz,&Moddym.IK);
	MDym.Kres.Zvetseni=fx(0.0);
	MDym.Simulace=SimulujDym;
	MDym.ObalRY=MDym.ObalRXZ=0;
	MDym.ObalS.x=MDym.ObalS.y=MDym.ObalS.z=0;
	MDym.Velikost=fxmetr(8);
  MDym.KolizeBezMinmax = False;
	SGPos=Vuz->NPos;
	SGSmer=Vuz->NUhel;
	EGPos=MPodvozek.NPos;
	EGPos.y+=ZANOR_SE;
	StupenVzniku=fx(1); /* zaciname zanik */
	if( Handle==GravHandle )
	{
		static Kamera KamZez= /* pro velke efekty */
		{
			{fxmetr(-5),fxmetr(10),-fxmetr(25.0)}, /* nade dnem a za prumetnou */
			{-5*UhelPom,0*UhelPom,0},
			PohHleda,
		};
		HledamVuz=Handle;
		LzePrep=False;
		StrihKamera(&KamZez,fx(2));
		if( Spc->MaSplnit.Vsechny==Spc->Splneno.Vsechny && !CekamVezne && !NemesisOdletel && NemesHandle<0 ){}
		else Promluv(SpFailed);
	}
	FZrusDym=False;
	RampaLock=True;
}
