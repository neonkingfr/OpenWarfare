#include <string.h>
#include <assert.h>
#include <limits.h>
#include "msgPass.h"
#include "soundPlayer.h"
#include "ZVUKY.H"
#include "sndGiGOut.h"
#include "macros.h"

extern "C"
{
	Flag Terminated(void);
}

/* DSP-like sound engine for Gravon */


/// DSP-like interface

enum MessageType
{
  MT_setbuffer,
  MT_SyntSoundStart,
  MT_SyntSoundChange,
};

struct MessageBase
{
  MessageType type;
  MessageBase(MessageType type):type(type){}
};

struct MessageSetbuffer: MessageBase
{
  const void *beg,*end;
  int mode;
  int freq;
  MessageSetbuffer(const void *beg, const void *end, int mode, int freq):MessageBase(MT_setbuffer),beg(beg),end(end),mode(mode),freq(freq){}
};

void setbuffer(int playRec, int mode, int freq, const void *beg, const void *end)
{
  assert(playRec==PLAY); // play only, no record functionality
  // changing the buffer needs to be atomic
  MessageSetbuffer msg(beg,end,mode,freq);
  SendMsg(&msg,sizeof(msg));
}

static char SampleData[16*1024]; // 8b signed mono
static size_t SampleSize;

struct Kanal
{
  int counter; // X(R0), init to 0
  int freq; // Y(R0)
  int freqChng; // Y(R0)+1, init to 0
  int acoustDataL, acoustDataR; // last data, L(R0)+2, init to 0
  int envelopeStep; // X(R0)+3
  size_t envelopeOffset; // Y(R0)+3 
  int *envelope; // X(R0)+4
  bool procNoiseSample; // Y(R0)+4
  int seed; // X(R0)+5
  size_t sampleOffset; // Y(R0)+5
  int volL,volR; // X(R0)+6, Y(R0)+6
  int dVolL,dVolR; // X(R0)+7, Y(R0)+7
};

static Kanal Kanaly[DSP_NKANALU];

const int SoundFreq = 44100;

#define MF23(a,b) ((int)((float(a)*float(b))/float(1<<23)+0.5f))

static void datafill(int &sndL, int &sndR)
{
  int accumL = 0, accumR = 0;
  for (int ki=0; ki<DSP_NKANALU; ki++)
  {
    Kanal &k = Kanaly[ki];
    int counter = k.counter;
    k.counter += k.freq;
    k.freq += k.freqChng;
    if (counter<0)
    { // next sample not reached yet, repeat the last value
      accumL += k.acoustDataL, accumR += k.acoustDataR;
      continue;
    }
    if (k.freq<=0)
    { // no sound, output zero
      k.acoustDataL = k.acoustDataR = 0;
      continue;
    }

    k.counter -= 1<<23;

    k.envelopeOffset += k.envelopeStep;
    k.envelopeOffset &= VolLen*256-1;
    int off = k.envelopeOffset>>8;
    if (!k.envelope)
      continue;
    int vol = k.envelope[off];
    int data = 0;
    if (k.procNoiseSample)
    { // sample
      data = SampleData[k.sampleOffset++]<<(24-8);
      if (k.sampleOffset>=SampleSize)
       k.sampleOffset = 0;
    }
    else
    { // noise
      /* original DSP56K source
	    move X:(R0),X0
	    clr B #>$5a5235,X1
	    move #>2,B0
	    mac X0,X1,B
	    asr B
	    move B0,X:(R0)+
      */
      // 0x5a5235 is 23b from KRand 22696501L (which is 0x15a5235)
      int rand = (k.seed*0x5a5235)+2;
      rand <<= 32-24; // simulate 1.23 numeric format modulo
      rand >>= 32-24 + 1; // 
      k.seed = rand;
      data = rand;
    }

    data = MF23(data,vol);

    k.acoustDataL = MF23(k.volL,data), k.acoustDataR = MF23(k.volR,data);

    accumL += k.acoustDataL, accumR += k.acoustDataR;

    k.volL += k.dVolL, k.volR += k.dVolR;

  }
  // convert from 23b signed frac to 16b signed (i.e. 15b frac)
  sndL = accumL; //>>(24-16);
  sndR = accumR; //>>(24-16);
}

void SendSample( void *Data, size_t Len )
{
  memcpy(SampleData,Data,Len);
  SampleSize = Len;
}


struct MessageSyntSoundStart: MessageBase
{
  int handle;
  enum indzvuku index;
  long volL, volR;
  long freq;
  long envStep;
  MessageSyntSoundStart(int handle, enum indzvuku index, long volL, long volR, long freq, long envStep):
  MessageBase(MT_SyntSoundStart),handle(handle),index(index),volL(volL),volR(volR),freq(freq),envStep(envStep)
  {}
};

struct MessageSyntSoundChange: MessageBase
{
  int handle;
  long volL, volR;
  long freq,dFreq;
  long envStep;
  long dVolL, dVolR;

  MessageSyntSoundChange(int handle, long volL, long volR, long freq, long envStep, long dFreq, long dVolL, long dVolR):
  MessageBase(MT_SyntSoundChange),handle(handle),volL(volL),volR(volR),freq(freq),envStep(envStep),dFreq(dFreq),dVolL(dVolL),dVolR(dVolR)
  {}
};

/**
@param freq sampling frequency relative to SND_FRQ, DSP56K representation (1<<23 => 1)
@param envStep 256*sampling frequency change in each SND_FRQ step, DSP56K representation (1<<23 => 1)

*/
void SyntSoundStart(int handle, enum indzvuku index, long volL, long volR, long freq, long envStep)
{
  MessageSyntSoundStart msg(handle,index,volL,volR,freq,envStep>>8);
  SendMsg(&msg,sizeof(msg));
}

/***
@param dFreq frequency delta for each SND_FRQ step

*/
void SyntSoundChange(int handle, long volL, long volR, long freq, long envStep, long dFreq, long dVolL, long dVolR)
{
  MessageSyntSoundChange msg(handle,volL,volR,freq,envStep>>8,dFreq,dVolL,dVolR);
  SendMsg(&msg,sizeof(msg));
}


static const char *playBeg,*playEnd;
static const char *playPtr;
static int playFrac; // fractional part skipped (1<<16 == 1)

static int playMode = STEREO16;
static int playFreq = SND_FRQ;

static const size_t sampleSize[3]={2,4,1}; // STEREO8, STEREO16, MONO8
const int sampleSizeTgt = sizeof(short)*2;

static inline short Sat16b( int x ) 
{
  if (x<SHRT_MIN) return SHRT_MIN;
  if (x>SHRT_MAX) return SHRT_MAX;
  return x;
}

static void PlayOneSample(char *tgt, int tgtFreq)
{
  int frac = playFrac;
  const char *ptr = playPtr;
  short *tgt16 = (short *)tgt;
  int step = int(((long long)playFreq<<16)/tgtFreq);
  size_t i = 0;
  short sample16b;
  switch (playMode)
  {
    case MONO8:
      // 8b unsigned
      sample16b = (short)*playPtr<<7; // TODO: bilinear filtering
      break;
    case STEREO16:
      assert(16!=16); // not implemented yet
      break;
    case STEREO8:
      assert(8!=8); // not implemented yet
      break;
  }

  int sndL = 0, sndR = 0;
  datafill(sndL,sndR);
  *tgt16++ = Sat16b(sndL+sample16b);
  *tgt16++ = Sat16b(sndR+sample16b);
  frac += step;

  i += frac>>16;
  frac &= 0xffff;

  playFrac = frac;
  playPtr = ptr+i;
}

// function called by the low-level sound engine
static size_t SoundPlayerCallback(void *buffer, size_t bufferSize, int bufferFreq)
{
  char message[512];
  while(size_t rd = GetMsg(message,sizeof(message)))
  {
    // process the message
    const MessageBase &msg = *(const MessageBase*)message;
    switch (msg.type)
    {
      case MT_setbuffer:
      {
        const MessageSetbuffer &m = static_cast<const MessageSetbuffer &>(msg);
        playBeg = (const char *)m.beg;
        playEnd = (const char *)m.end;
        playPtr = playBeg;
        playMode = m.mode;
        playFreq = m.freq;
        playFrac = 0;
        break;
      }
      case MT_SyntSoundStart:
      {
        const MessageSyntSoundStart &m = static_cast<const MessageSyntSoundStart &>(msg);
        Kanal &k = Kanaly[m.handle];
        assert(m.handle>=0 && m.handle<DSP_NKANALU);
        k.counter = 0;
        k.freq = int(m.freq*float(SND_FRQ)/bufferFreq);
        k.freqChng = 0;
        k.acoustDataL = k.acoustDataR = 0;
        k.envelopeStep = m.envStep;
        k.envelopeOffset = 0;
        k.envelope = DefZvuky[m.index].Prubeh;
        assert(k.envelope);
        k.procNoiseSample = DefZvuky[m.index].Druh!=0;
        assert(k.procNoiseSample==(m.index==IGravon));
        k.sampleOffset = 0;
        // k.seed = 0; // seed not reset, better sound variety
        k.volL = m.volL, k.volL = m.volR;
        k.dVolL = k.dVolR = 0;

        break;
      }
      case MT_SyntSoundChange:
      {
        const MessageSyntSoundChange &m = static_cast<const MessageSyntSoundChange &>(msg);
        Kanal &k = Kanaly[m.handle];
        //assert(k.envelope);
        if (!k.envelope) // ignore "change" coming before "set"
          break; // TODO: fix the cause instead

        k.freq = int(m.freq*float(SND_FRQ)/bufferFreq);
        k.freqChng = int(m.dFreq*float(SND_FRQ)/bufferFreq);
        k.envelopeStep = m.envStep;
        k.volL = m.volL, k.volR = m.volR;
        k.dVolL = m.dVolL, k.dVolR = m.dVolR;

        break;
      }
      default:
        assert(false); // unexpected message type
    }
  }
  if (playEnd>playBeg)
  {
    size_t bufferOffset = 0;
    int sSize = sampleSize[playMode];
    while (bufferOffset<bufferSize)
    {
      // TODO: sampling ratio
      assert(playPtr>=playBeg);
      assert(playPtr<playEnd);

      PlayOneSample((char *)buffer+bufferOffset,bufferFreq);
      bufferOffset += sampleSizeTgt;

      bool looped = false;
      if (looped)
      {
        while (playPtr>=playEnd)
        {
          playPtr -= playEnd-playBeg;
        }
      }
      else
      {
        if (playPtr>=playEnd)
        {
          playEnd = playBeg = playPtr = NULL;
          playFrac = 0;
          short *buf16 = (short *)((char *)buffer+bufferOffset);
          while (bufferOffset<bufferSize)
          {
            int sndL=0, sndR=0;
            datafill(sndL, sndR);
            buf16[0] = Sat16b(sndL), buf16[1] = Sat16b(sndR);
            buf16 += 2;
            bufferOffset += sampleSizeTgt;
          }
          return bufferSize;
        }
      }
    }
  }
  else
  {
    size_t bufferOffset = 0;
    short *buf16 = (short *)buffer;
    while (bufferOffset<bufferSize)
    {
      int sndL=0, sndR=0;
      datafill(sndL, sndR);
      buf16[0] = Sat16b(sndL), buf16[1] = Sat16b(sndR);
      buf16 += 2;
      bufferOffset += sampleSizeTgt;
    }
  }
  return bufferSize;
}

void StartSound()
{
  StartSoundGiG(SoundFreq,SoundPlayerCallback);
}

void EndSound()
{
	if(Terminated())
		return;

  EndSoundGiG();
  // pump out any message which may be still pending to avoid leaks
  char message[512];
  while(size_t rd = GetMsg(message,sizeof(message)))
  {
    __asm nop;
  }
}
