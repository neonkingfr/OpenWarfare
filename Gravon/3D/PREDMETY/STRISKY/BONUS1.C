#include "predmety.h"
#include "colors.h"
static IBod BBonus1[]=
{
	{fxmetr( 12.3328),fxmetr( -6.5896),fxmetr( -7.5015),},
	{fxmetr(-12.3345),fxmetr( -6.5906),fxmetr( -7.5015),},
	{fxmetr( 10.2166),fxmetr(  8.8616),fxmetr( -3.8650),},
	{fxmetr(-10.6763),fxmetr(  8.8611),fxmetr( -3.8650),},
	{fxmetr( 12.3328),fxmetr( -6.5896),fxmetr(  8.1792),},
	{fxmetr(-12.3345),fxmetr( -6.5906),fxmetr(  8.1792),},
	{fxmetr( 10.2166),fxmetr(  8.8616),fxmetr(  4.5430),},
	{fxmetr(-10.6763),fxmetr(  8.8611),fxmetr(  4.5430),},
	{fxmetr(  9.3870),fxmetr(  7.4983),fxmetr( -4.2056),},
	{fxmetr( -9.7561),fxmetr(  7.4954),fxmetr( -4.2056),},
	{fxmetr(  9.3870),fxmetr(  7.4983),fxmetr(  4.8843),},
	{fxmetr( -9.7561),fxmetr(  7.4954),fxmetr(  4.8843),},
};

static IPlocha PBonus1[]=
{
	{Gold,(0),(4),(6),},
	{Gold,(0),(6),(2),},
	{Gold,(2),(6),(7),},
	{Gold,(2),(7),(3),},
	{Gold,(3),(7),(5),},
	{Gold,(1),(3),(5),},
	{Gold,(8),(11),(10),},
	{Gold,(8),(9),(11),},
	{Silver,(6),(10),(11),},
	{Silver,(6),(11),(7),},
	{Silver,(5),(7),(11),},
	{Gold,(5),(11),(9),},
	{Gold,(1),(5),(9),},
	{Silver,(1),(9),(3),},
	{Silver,(2),(3),(9),},
	{Silver,(2),(9),(8),},
	{Silver,(0),(2),(8),},
	{Silver,(4),(10),(6),},
	{Gold,(0),(10),(4),},
	{Gold,(0),(8),(10),},
};

KKresliScena Bonus1={
	NULL,BBonus1,PBonus1,
	30,(bindex)lenof(BBonus1),(bindex)lenof(PBonus1)
};
