#include "predmety.h"
#include "colors.h"
static IBod BPod[]=
{
	{fxmetr( -0.1138),fxmetr(  5.5967),fxmetr( -1.1348),},
	{fxmetr( -1.3069),fxmetr(  5.5955),fxmetr( -0.5718),},
	{fxmetr( -1.3069),fxmetr(  5.5955),fxmetr(  0.4961),},
	{fxmetr( -0.2273),fxmetr(  5.5962),fxmetr(  1.0088),},
	{fxmetr(  0.9656),fxmetr(  5.5955),fxmetr(  0.4883),},
	{fxmetr(  0.9656),fxmetr(  5.5955),fxmetr( -0.5762),},
	{fxmetr( -0.1138),fxmetr(  2.8125),fxmetr( -2.0601),},
	{fxmetr( -2.3865),fxmetr(  2.8125),fxmetr( -1.4622),},
	{fxmetr( -2.3865),fxmetr(  2.8125),fxmetr(  1.1602),},
	{fxmetr( -0.2842),fxmetr(  2.8125),fxmetr(  1.9983),},
	{fxmetr(  1.9878),fxmetr(  2.8125),fxmetr(  1.1602),},
	{fxmetr(  1.9883),fxmetr(  2.8125),fxmetr( -1.4622),},
	{fxmetr( -0.1138),fxmetr(  0.4114),fxmetr( -1.0779),},
	{fxmetr( -1.3069),fxmetr(  0.4263),fxmetr( -0.5154),},
	{fxmetr( -1.3069),fxmetr(  0.4263),fxmetr(  0.5525),},
	{fxmetr( -0.2273),fxmetr(  0.4258),fxmetr(  1.0657),},
	{fxmetr(  0.9656),fxmetr(  0.4263),fxmetr(  0.5452),},
	{fxmetr(  0.9656),fxmetr(  0.4263),fxmetr( -0.5193),},
};

static IPlocha PPod[]=
{
	{Yellow,(0),(5),(4),},
	{Yellow,(0),(4),(3),},
	{Yellow,(0),(3),(2),},
	{Yellow,(0),(2),(1),},
	{Yellow,(3),(10),(9),},
	{Yellow,(3),(4),(10),},
	{Yellow,(4),(5),(10),},
	{Yellow,(5),(11),(10),},
	{Yellow,(0),(11),(5),},
	{Yellow,(0),(6),(11),},
	{Yellow,(0),(7),(6),},
	{Yellow,(0),(1),(7),},
	{Yellow,(1),(2),(8),},
	{Yellow,(1),(8),(7),},
	{Yellow,(2),(3),(8),},
	{Yellow,(3),(9),(8),},
	{Yellow,(12),(16),(17),},
	{Yellow,(12),(15),(16),},
	{Yellow,(12),(14),(15),},
	{Yellow,(12),(13),(14),},
	{Yellow,(9),(10),(15),},
	{Yellow,(10),(16),(15),},
	{Yellow,(10),(17),(16),},
	{Yellow,(10),(11),(17),},
	{Yellow,(11),(12),(17),},
	{Yellow,(6),(12),(11),},
	{Yellow,(6),(7),(12),},
	{Yellow,(7),(13),(12),},
	{Yellow,(8),(14),(13),},
	{Yellow,(7),(8),(13),},
	{Yellow,(8),(15),(14),},
	{Yellow,(8),(9),(15),},
};

KKresliScena Pod={
	NULL,BPod,PPod,
	48,(bindex)lenof(BPod),(bindex)lenof(PPod)
};
