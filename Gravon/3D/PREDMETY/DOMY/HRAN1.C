#include "predmety.h"
#include "colors.h"
static IBod _BHran1[]=
{
	{fxmetr( -7.5000),fxmetr( -0.5000),fxmetr( -3.7500),},
	{fxmetr( -7.5000),fxmetr(  6.1357),fxmetr( -3.7500),},
	{fxmetr(  7.5000),fxmetr(  6.1357),fxmetr( -3.7500),},
	{fxmetr(  7.5000),fxmetr( -0.5000),fxmetr( -3.7500),},
	{fxmetr( -7.5000),fxmetr( -0.5000),fxmetr(  3.9189),},
	{fxmetr( -7.5000),fxmetr(  6.1357),fxmetr(  3.9189),},
	{fxmetr(  7.5000),fxmetr(  6.1357),fxmetr(  3.9189),},
	{fxmetr(  7.5000),fxmetr( -0.5000),fxmetr(  3.9189),},
};

static IPlocha _PHran1[]=
{
	{STENA,(0),(2),(1),},
	{STENA,(0),(3),(2),},
	{STENA,(4),(5),(6),},
	{STENA,(4),(6),(7),},
	{Asfalt,(1),(2),(6),},
	{Asfalt,(1),(6),(5),},
	{STENA,(2),(7),(6),},
	{STENA,(2),(3),(7),},
	{STENA,(0),(1),(5),},
	{STENA,(0),(5),(4),},
};

KKresliScena Hran1={
	NULL,_BHran1,_PHran1,
	17,(bindex)lenof(_BHran1),(bindex)lenof(_PHran1)
};
