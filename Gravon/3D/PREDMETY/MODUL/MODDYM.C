#include "predmety.h"
#include "colors.h"
static IBod BModdym[]=
{
	{fxmetr( -2.4563),fxmetr(  1.9197),fxmetr(-15.0774),},
	{fxmetr( -9.5950),fxmetr(  3.3687),fxmetr( -1.8599),},
	{fxmetr( 20.6575),fxmetr( -2.0879),fxmetr( -0.0232),},
	{fxmetr(  7.5918),fxmetr( -1.3096),fxmetr(-15.7876),},
	{fxmetr( -6.4712),fxmetr( -1.4241),fxmetr(-16.9258),},
	{fxmetr(-18.2698),fxmetr( -2.6084),fxmetr( -0.2981),},
	{fxmetr(  2.3101),fxmetr( -5.4043),fxmetr(  2.0088),},
	{fxmetr( -0.0906),fxmetr( -3.4307),fxmetr(-13.5879),},
	{fxmetr( 10.9424),fxmetr(  3.1829),fxmetr(  0.2422),},
	{fxmetr( -4.2605),fxmetr(  1.7454),fxmetr( 12.1802),},
	{fxmetr( 12.3401),fxmetr(  0.2913),fxmetr( 16.2649),},
	{fxmetr( -8.3992),fxmetr(  0.0154),fxmetr( 15.7266),},
	{fxmetr(  0.5237),fxmetr( -2.9961),fxmetr( 13.9170),},
	{fxmetr( -0.0022),fxmetr(  4.0762),fxmetr(  0.0027),},
	{fxmetr(  0.6038),fxmetr(  1.7813),fxmetr( -0.9512),},
	{fxmetr( -1.0613),fxmetr(  1.7813),fxmetr(  0.0110),},
	{fxmetr(  0.5955),fxmetr(  1.7813),fxmetr(  0.9700),},
	{fxmetr(  0.0081),fxmetr( -3.6934),fxmetr(  0.0027),},
};

static IPlocha PModdym[]=
{
	{DYM,(4),(5),(6),},
	{DYM,(4),(6),(7),},
	{DYM,(3),(4),(7),},
	{DYM,(0),(4),(3),},
	{DYM,(0),(1),(5),},
	{DYM,(0),(5),(4),},
	{DYM,(0),(8),(1),},
	{DYM,(0),(3),(8),},
	{DYM,(2),(8),(3),},
	{DYM,(10),(12),(11),},
	{DYM,(9),(10),(11),},
	{DYM,(8),(10),(9),},
	{DYM,(2),(10),(8),},
	{DYM,(1),(8),(9),},
	{DYM,(1),(9),(11),},
	{DYM,(1),(11),(5),},
	{DYM,(5),(11),(12),},
	{DYM,(5),(12),(6),},
	{DYM,(2),(6),(12),},
	{DYM,(2),(12),(10),},
	{DYM,(3),(7),(6),},
	{DYM,(2),(3),(6),},
	{OHEN,(13),(15),(14),},
	{OHEN,(13),(16),(15),},
	{OHEN,(14),(15),(17),},
	{OHEN,(15),(16),(17),},
	{OHEN,(13),(14),(16),},
	{OHEN,(14),(17),(16),},
};

KKresliScena Moddym={
	NULL,BModdym,PModdym,
	42,(bindex)lenof(BModdym),(bindex)lenof(PModdym)
};
