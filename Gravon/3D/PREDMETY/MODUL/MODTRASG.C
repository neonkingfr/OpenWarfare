#include "predmety.h"
#include "colors.h"
static IBod BModtrasg[]=
{
	{fxmetr( -0.1663),fxmetr(  2.4727),fxmetr( -2.6553),},
	{fxmetr( -0.9622),fxmetr(  2.5569),fxmetr( -2.1548),},
	{fxmetr( -1.1890),fxmetr(  2.7273),fxmetr( -0.8423),},
	{fxmetr( -0.1663),fxmetr(  2.8130),fxmetr( -0.1851),},
	{fxmetr(  1.0828),fxmetr(  2.7273),fxmetr( -0.8572),},
	{fxmetr(  0.8555),fxmetr(  2.5569),fxmetr( -2.1626),},
	{fxmetr( -0.2522),fxmetr(  0.9949),fxmetr( -3.0496),},
	{fxmetr( -1.4397),fxmetr(  0.9949),fxmetr( -2.4534),},
	{fxmetr( -1.8716),fxmetr(  0.9949),fxmetr(  0.0464),},
	{fxmetr( -0.2522),fxmetr(  0.9949),fxmetr(  1.4231),},
	{fxmetr(  1.3660),fxmetr(  0.9949),fxmetr(  0.0464),},
	{fxmetr(  1.1514),fxmetr(  0.9949),fxmetr( -2.4534),},
	{fxmetr(  0.0601),fxmetr(  0.7671),fxmetr( -4.0752),},
	{fxmetr( -2.4397),fxmetr(  0.7112),fxmetr( -2.9495),},
	{fxmetr( -3.0076),fxmetr(  0.7112),fxmetr(  0.7771),},
	{fxmetr( -0.1663),fxmetr(  0.7671),fxmetr(  4.2461),},
	{fxmetr(  2.9009),fxmetr(  0.7112),fxmetr(  0.6487),},
	{fxmetr(  2.3328),fxmetr(  0.7112),fxmetr( -2.9578),},
};

static IPlocha PModtrasg[]=
{
	{Gray35,(0),(5),(4),},
	{Gray35,(0),(4),(3),},
	{Gray35,(0),(3),(2),},
	{Gray35,(0),(2),(1),},
	{Silver,(3),(10),(9),},
	{Silver,(3),(4),(10),},
	{Silver,(4),(5),(10),},
	{Silver,(5),(11),(10),},
	{Silver,(0),(11),(5),},
	{Silver,(0),(6),(11),},
	{Silver,(0),(7),(6),},
	{Silver,(0),(1),(7),},
	{Silver,(1),(2),(8),},
	{Silver,(1),(8),(7),},
	{Silver,(2),(3),(8),},
	{Silver,(3),(9),(8),},
	{Gray35,(12),(16),(17),},
	{Gray35,(12),(15),(16),},
	{Gray35,(12),(14),(15),},
	{Gray35,(12),(13),(14),},
	{Silver,(9),(10),(15),},
	{Silver,(10),(16),(15),},
	{Silver,(10),(17),(16),},
	{Silver,(10),(11),(17),},
	{Silver,(11),(12),(17),},
	{Silver,(6),(12),(11),},
	{Silver,(6),(7),(12),},
	{Silver,(7),(13),(12),},
	{Silver,(8),(14),(13),},
	{Silver,(7),(8),(13),},
	{Silver,(8),(15),(14),},
	{Silver,(8),(9),(15),},
};

KKresliScena Modtrasg={
	NULL,BModtrasg,PModtrasg,
	48,(bindex)lenof(BModtrasg),(bindex)lenof(PModtrasg)
};
