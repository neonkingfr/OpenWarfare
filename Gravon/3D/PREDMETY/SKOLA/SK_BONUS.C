#include "predmety.h"
#include "colors.h"
static IBod _BSk_bonus[]=
{
	{fxmetr(320.4854),fxmetr( -5.2227),fxmetr(-516.4561),},
	{fxmetr(295.8184),fxmetr( -5.2256),fxmetr(-516.4561),},
	{fxmetr(318.3682),fxmetr( 10.2256),fxmetr(-512.8184),},
	{fxmetr(297.4766),fxmetr( 10.2227),fxmetr(-512.8184),},
	{fxmetr(320.4854),fxmetr( -5.2227),fxmetr(-500.7764),},
	{fxmetr(295.8184),fxmetr( -5.2256),fxmetr(-500.7764),},
	{fxmetr(318.3682),fxmetr( 10.2256),fxmetr(-504.4141),},
	{fxmetr(297.4766),fxmetr( 10.2227),fxmetr(-504.4141),},
	{fxmetr(317.5391),fxmetr(  8.8604),fxmetr(-513.1602),},
	{fxmetr(298.3975),fxmetr(  8.8594),fxmetr(-513.1602),},
	{fxmetr(317.5391),fxmetr(  8.8604),fxmetr(-504.0723),},
	{fxmetr(298.3975),fxmetr(  8.8594),fxmetr(-504.0723),},
};

static IPlocha _PSk_bonus[]=
{
	{Gray60,(0),(4),(6),},
	{Gray60,(0),(6),(2),},
	{Gray60,(2),(6),(7),},
	{Gray60,(2),(7),(3),},
	{Gray60,(3),(7),(5),},
	{Gray60,(1),(3),(5),},
	{Gray60,(8),(11),(10),},
	{Gray60,(8),(9),(11),},
	{Gray60,(6),(10),(11),},
	{Gray60,(6),(11),(7),},
	{Gray60,(5),(7),(11),},
	{Gray60,(5),(11),(9),},
	{Gray60,(1),(5),(9),},
	{R_STRECHA,(1),(9),(3),},
	{Gray60,(2),(3),(9),},
	{Gray60,(2),(9),(8),},
	{SkyBlue,(0),(2),(8),},
	{Gray60,(4),(10),(6),},
	{Gray60,(0),(10),(4),},
	{Gray60,(0),(8),(10),},
};

KKresliScena Sk_bonus={
	NULL,_BSk_bonus,_PSk_bonus,
	30,(bindex)lenof(_BSk_bonus),(bindex)lenof(_PSk_bonus)
};
