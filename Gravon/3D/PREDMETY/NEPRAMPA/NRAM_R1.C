#include "predmety.h"
#include "colors.h"
static IBod BNram_r1[]=
{
	{fxmetr(  4.4048),fxmetr(  0.0000),fxmetr(-36.3589),},
	{fxmetr(  2.5872),fxmetr( 59.3020),fxmetr(-40.9043),},
	{fxmetr(  1.1094),fxmetr( 58.3928),fxmetr(-37.3589),},
	{fxmetr(  2.7002),fxmetr(  0.0000),fxmetr(-31.4502),},
	{fxmetr( -4.4043),fxmetr(  0.0000),fxmetr(-36.3589),},
	{fxmetr( -2.9275),fxmetr( 59.3020),fxmetr(-40.9043),},
	{fxmetr( -1.4495),fxmetr( 58.3928),fxmetr(-37.3589),},
	{fxmetr( -3.1543),fxmetr(  0.0000),fxmetr(-31.4502),},
	{fxmetr(  3.8838),fxmetr( 31.3960),fxmetr(-45.9722),},
	{fxmetr(  3.8838),fxmetr( 33.3960),fxmetr(-45.9722),},
	{fxmetr(  1.2190),fxmetr( 33.3960),fxmetr(-36.6084),},
	{fxmetr(  1.2190),fxmetr( 31.3960),fxmetr(-36.6084),},
	{fxmetr( -3.9961),fxmetr( 31.3960),fxmetr(-45.9722),},
	{fxmetr( -3.9961),fxmetr( 33.3960),fxmetr(-45.9722),},
	{fxmetr( -1.6731),fxmetr( 33.3960),fxmetr(-36.6084),},
	{fxmetr( -1.6731),fxmetr( 31.3960),fxmetr(-36.6084),},
	{fxmetr(  3.1985),fxmetr( 51.3960),fxmetr(-45.8594),},
	{fxmetr(  3.1985),fxmetr( 53.3960),fxmetr(-45.8594),},
	{fxmetr(  0.9302),fxmetr( 53.3960),fxmetr(-38.3135),},
	{fxmetr(  0.9302),fxmetr( 51.3960),fxmetr(-38.3135),},
	{fxmetr( -3.3101),fxmetr( 51.3960),fxmetr(-45.8594),},
	{fxmetr( -3.3101),fxmetr( 53.3960),fxmetr(-45.8594),},
	{fxmetr( -1.3831),fxmetr( 53.3960),fxmetr(-38.3135),},
	{fxmetr( -1.3831),fxmetr( 51.3960),fxmetr(-38.3135),},
};

static IPlocha PNram_r1[]=
{
	{Gray70,(0),(2),(1),},
	{Gray70,(0),(3),(2),},
	{Gray70,(4),(5),(6),},
	{Gray70,(4),(6),(7),},
	{Gray70,(1),(2),(6),},
	{Gray70,(1),(6),(5),},
	{Gray70,(2),(7),(6),},
	{Gray70,(2),(3),(7),},
	{Gray70,(0),(1),(5),},
	{Gray70,(0),(5),(4),},
	{Gray60,(8),(10),(9),},
	{Gray60,(8),(11),(10),},
	{Gray60,(12),(13),(14),},
	{Gray60,(12),(14),(15),},
	{Gray60,(9),(10),(14),},
	{Gray60,(9),(14),(13),},
	{Gray60,(11),(12),(15),},
	{Gray60,(8),(12),(11),},
	{Gray60,(8),(9),(13),},
	{Gray60,(8),(13),(12),},
	{Gray60,(16),(18),(17),},
	{Gray60,(16),(19),(18),},
	{Gray60,(20),(21),(22),},
	{Gray60,(20),(22),(23),},
	{Gray60,(17),(18),(22),},
	{Gray60,(17),(22),(21),},
	{Gray60,(19),(20),(23),},
	{Gray60,(16),(20),(19),},
	{Gray60,(16),(17),(21),},
	{Gray60,(16),(21),(20),},
};

KKresliScena Nram_r1={
	NULL,BNram_r1,PNram_r1,
	51,(bindex)lenof(BNram_r1),(bindex)lenof(PNram_r1)
};
