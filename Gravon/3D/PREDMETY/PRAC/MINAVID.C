#include "predmety.h"
#include "colors.h"
static IBod BMinavid[]=
{
	{fxmetr(  0.0571),fxmetr(  0.2827),fxmetr(  0.1016),},
	{fxmetr( -0.6699),fxmetr(  0.0327),fxmetr( -0.3638),},
	{fxmetr(  0.7388),fxmetr(  0.0327),fxmetr( -0.3638),},
	{fxmetr(  0.0674),fxmetr(  0.0327),fxmetr(  0.5674),},
};

static IPlocha PMinavid[]=
{
	{LightSteelBlue,(0),(1),(2),},
	{LightSteelBlue,(0),(2),(3),},
	{LightSteelBlue,(0),(3),(1),},
};

KKresliScena Minavid={
	NULL,BMinavid,PMinavid,
	6,(bindex)lenof(BMinavid),(bindex)lenof(PMinavid)
};
