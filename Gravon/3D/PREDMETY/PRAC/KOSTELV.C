#define _BC(n) (n)
static IBod BKostelv[]=
{
	{fxmetr(-10.0000),fxmetr(  0.0000),fxmetr( -5.0000),},
	{fxmetr( 10.0000),fxmetr(  0.0000),fxmetr( -5.0000),},
	{fxmetr( 10.0000),fxmetr(  0.0000),fxmetr(  5.0000),},
	{fxmetr(-10.0000),fxmetr(  0.0000),fxmetr(  5.0000),},
	{fxmetr(-10.0000),fxmetr(  6.0000),fxmetr( -5.0000),},
	{fxmetr( 10.0000),fxmetr(  6.0000),fxmetr( -5.0000),},
	{fxmetr( 10.0000),fxmetr(  6.0000),fxmetr(  5.0000),},
	{fxmetr(-10.0000),fxmetr(  6.0000),fxmetr(  5.0000),},
	{fxmetr(-10.0000),fxmetr( 12.0000),fxmetr(  0.0000),},
	{fxmetr(  5.0000),fxmetr( 12.0000),fxmetr(  0.0000),},
	{fxmetr(-16.0000),fxmetr(  0.0000),fxmetr( -3.0000),},
	{fxmetr(-10.0000),fxmetr(  0.0000),fxmetr( -3.0000),},
	{fxmetr(-10.0000),fxmetr(  0.0000),fxmetr(  3.0000),},
	{fxmetr(-16.0000),fxmetr(  0.0000),fxmetr(  3.0000),},
	{fxmetr(-16.0000),fxmetr( 14.0000),fxmetr( -3.0000),},
	{fxmetr(-10.0000),fxmetr( 14.0000),fxmetr( -3.0000),},
	{fxmetr(-10.0000),fxmetr( 14.0000),fxmetr(  3.0000),},
	{fxmetr(-16.0000),fxmetr( 14.0000),fxmetr(  3.0000),},
	{fxmetr(-13.0000),fxmetr( 24.0000),fxmetr(  0.0000),},
};

static IPlocha PKostelv[]=
{
	{W_STENA,_BC(0),_BC(1),_BC(5),},
	{W_STENA,_BC(1),_BC(2),_BC(5),},
	{W_STENA,_BC(5),_BC(2),_BC(6),},
	{W_STENA,_BC(2),_BC(7),_BC(6),},
	{W_STENA,_BC(2),_BC(3),_BC(7),},
	{W_STENA,_BC(0),_BC(7),_BC(3),},
	{W_STENA,_BC(0),_BC(4),_BC(7),},
	{W_STENA,_BC(0),_BC(5),_BC(4),},
	{STRECHA,_BC(4),_BC(5),_BC(8),},
	{STRECHA,_BC(5),_BC(9),_BC(8),},
	{STRECHA,_BC(5),_BC(6),_BC(9),},
	{STRECHA,_BC(9),_BC(6),_BC(8),},
	{STRECHA,_BC(6),_BC(7),_BC(8),},
	{W_STENA,_BC(7),_BC(4),_BC(8),},
	{W_STENA,_BC(10),_BC(11),_BC(15),},
	{W_STENA,_BC(11),_BC(12),_BC(15),},
	{W_STENA,_BC(15),_BC(12),_BC(16),},
	{W_STENA,_BC(12),_BC(17),_BC(16),},
	{W_STENA,_BC(12),_BC(13),_BC(17),},
	{W_STENA,_BC(10),_BC(17),_BC(13),},
	{W_STENA,_BC(10),_BC(14),_BC(17),},
	{W_STENA,_BC(10),_BC(15),_BC(14),},
	{STRECHA,_BC(14),_BC(15),_BC(18),},
	{STRECHA,_BC(15),_BC(16),_BC(18),},
	{STRECHA,_BC(16),_BC(17),_BC(18),},
	{STRECHA,_BC(17),_BC(14),_BC(18),},
};

#undef _BC

IKresliScena Kostelv={
	NULL,BKostelv,PKostelv,
	43,(bindex)lenof(BKostelv),(bindex)lenof(PKostelv)
};
