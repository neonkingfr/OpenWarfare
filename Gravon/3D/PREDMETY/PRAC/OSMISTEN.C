#include "predmety.h"
#include "colors.h"
static IBod BOsmisten[]=
{
	{fxmetr(  0.2271),fxmetr( -9.9998),fxmetr(  0.0000),},
	{fxmetr(  0.0000),fxmetr( 10.0000),fxmetr(  0.0000),},
	{fxmetr( -9.7727),fxmetr(  0.0000),fxmetr(  0.0000),},
	{fxmetr( -0.2271),fxmetr(  0.0000),fxmetr(  9.7727),},
	{fxmetr(  9.7727),fxmetr(  0.0000),fxmetr(  0.0000),},
	{fxmetr(  0.0000),fxmetr(  0.0000),fxmetr(-10.0000),},
};

static IPlocha POsmisten[]=
{
	{White,(2),(0),(5),},
	{White,(0),(4),(5),},
	{White,(0),(3),(4),},
	{White,(0),(2),(3),},
	{White,(2),(1),(3),},
	{White,(3),(1),(4),},
	{White,(4),(1),(5),},
	{White,(1),(2),(5),},
};

IKresliScena Osmisten={
	NULL,BOsmisten,POsmisten,
	12,(bindex)lenof(BOsmisten),(bindex)lenof(POsmisten)
};
