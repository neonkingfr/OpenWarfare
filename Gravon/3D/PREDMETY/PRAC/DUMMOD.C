#define _BC(n) (n)
static IBod BDummod[]=
{
	{fxmetr(  0.0000),fxmetr(  4.1665),fxmetr(  0.0000),},
	{fxmetr( -1.9434),fxmetr(  2.7363),fxmetr( -1.1484),},
	{fxmetr(  0.0000),fxmetr(  2.7363),fxmetr(  1.7363),},
	{fxmetr(  1.8037),fxmetr(  2.7363),fxmetr( -1.1484),},
	{fxmetr(  0.0000),fxmetr( -4.5630),fxmetr(  0.0000),},
	{fxmetr( -2.4561),fxmetr(  1.3086),fxmetr(-14.2444),},
	{fxmetr( -9.5952),fxmetr(  2.7576),fxmetr( -1.0266),},
	{fxmetr( 20.6577),fxmetr( -2.6992),fxmetr(  0.8098),},
	{fxmetr(  7.5918),fxmetr( -1.9207),fxmetr(-14.9543),},
	{fxmetr( -6.4712),fxmetr( -2.0354),fxmetr(-16.0925),},
	{fxmetr(-18.2700),fxmetr( -3.2192),fxmetr(  0.5354),},
	{fxmetr(  2.3105),fxmetr( -6.0151),fxmetr(  2.8420),},
	{fxmetr( -0.0908),fxmetr( -4.0417),fxmetr(-12.7546),},
	{fxmetr( 10.9424),fxmetr(  2.5720),fxmetr(  1.0754),},
	{fxmetr( -4.2603),fxmetr(  1.1343),fxmetr( 13.0134),},
	{fxmetr( 12.3403),fxmetr( -0.3193),fxmetr( 17.0984),},
	{fxmetr( -8.3994),fxmetr( -0.5955),fxmetr( 16.5598),},
	{fxmetr(  0.5239),fxmetr( -3.6074),fxmetr( 14.7502),},
};

static IPlocha PDummod[]=
{
	{OHEN,_BC(0),_BC(2),_BC(1),},
	{OHEN,_BC(0),_BC(3),_BC(2),},
	{OHEN,_BC(1),_BC(2),_BC(4),},
	{OHEN,_BC(2),_BC(3),_BC(4),},
	{OHEN,_BC(0),_BC(1),_BC(3),},
	{OHEN,_BC(3),_BC(1),_BC(4),},
	{DYM,_BC(9),_BC(10),_BC(11),},
	{DYM,_BC(9),_BC(11),_BC(12),},
	{DYM,_BC(8),_BC(9),_BC(12),},
	{DYM,_BC(8),_BC(5),_BC(9),},
	{DYM,_BC(5),_BC(6),_BC(10),},
	{DYM,_BC(9),_BC(5),_BC(10),},
	{DYM,_BC(6),_BC(5),_BC(13),},
	{DYM,_BC(5),_BC(8),_BC(13),},
	{DYM,_BC(8),_BC(7),_BC(13),},
	{DYM,_BC(16),_BC(15),_BC(17),},
	{DYM,_BC(14),_BC(15),_BC(16),},
	{DYM,_BC(14),_BC(13),_BC(15),},
	{DYM,_BC(13),_BC(7),_BC(15),},
	{DYM,_BC(6),_BC(13),_BC(14),},
	{DYM,_BC(6),_BC(14),_BC(16),},
	{DYM,_BC(10),_BC(6),_BC(16),},
	{DYM,_BC(10),_BC(16),_BC(17),},
	{DYM,_BC(11),_BC(10),_BC(17),},
	{DYM,_BC(7),_BC(11),_BC(17),},
	{DYM,_BC(15),_BC(7),_BC(17),},
	{DYM,_BC(11),_BC(8),_BC(12),},
	{DYM,_BC(7),_BC(8),_BC(11),},
};

#undef _BC

IKresliScena Dummod={
	NULL,BDummod,PDummod,
	42,(bindex)lenof(BDummod),(bindex)lenof(PDummod)
};
