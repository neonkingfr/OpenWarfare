#define _BC(n) (n)
static IBod BKrychle[]=
{
	{fxmetr( -9.9998),fxmetr(-10.0000),fxmetr(-10.0000),},
	{fxmetr( -9.9998),fxmetr(  9.9998),fxmetr(-10.0000),},
	{fxmetr( 10.0000),fxmetr(  9.9998),fxmetr(-10.0000),},
	{fxmetr( 10.0000),fxmetr(-10.0000),fxmetr(-10.0000),},
	{fxmetr( -9.9998),fxmetr(-10.0000),fxmetr(  9.9998),},
	{fxmetr( -9.9998),fxmetr(  9.9998),fxmetr(  9.9998),},
	{fxmetr( 10.0000),fxmetr(  9.9998),fxmetr(  9.9998),},
	{fxmetr( 10.0000),fxmetr(-10.0000),fxmetr(  9.9998),},
};

static IPlocha PKrychle[]=
{
	{Gold,_BC(1),_BC(0),_BC(2),},
	{Gold,_BC(2),_BC(0),_BC(3),},
	{Gold,_BC(4),_BC(5),_BC(6),},
	{Gold,_BC(4),_BC(6),_BC(7),},
	{Gold,_BC(1),_BC(2),_BC(6),},
	{Gold,_BC(5),_BC(1),_BC(6),},
	{Gold,_BC(3),_BC(4),_BC(7),},
	{Gold,_BC(3),_BC(0),_BC(4),},
	{Gold,_BC(6),_BC(2),_BC(7),},
	{Gold,_BC(2),_BC(3),_BC(7),},
	{Gold,_BC(0),_BC(1),_BC(5),},
	{Gold,_BC(4),_BC(0),_BC(5),},
};

#undef _BC

IKresliScena Krychle={
	NULL,BKrychle,PKrychle,
	18,(bindex)lenof(BKrychle),(bindex)lenof(PKrychle)
};
