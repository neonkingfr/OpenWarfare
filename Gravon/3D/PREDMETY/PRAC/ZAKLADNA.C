#define _BC(n) (n)
static IBod BZakladna[]=
{
	{fxmetr(-28.6406),fxmetr(  0.0967),fxmetr(-10.0000),},
	{fxmetr(-28.6406),fxmetr(  6.1160),fxmetr(-10.0000),},
	{fxmetr( -8.6406),fxmetr(  6.1160),fxmetr(-10.0000),},
	{fxmetr( -8.6406),fxmetr(  0.0967),fxmetr(-10.0000),},
	{fxmetr(-28.6406),fxmetr(  0.0967),fxmetr(  9.9998),},
	{fxmetr(-28.6406),fxmetr(  6.1160),fxmetr(  9.9998),},
	{fxmetr( -8.6406),fxmetr(  6.1160),fxmetr(  9.9998),},
	{fxmetr( -8.6406),fxmetr(  0.0967),fxmetr(  9.9998),},
	{fxmetr(  9.0288),fxmetr(  0.4851),fxmetr(-10.0000),},
	{fxmetr(  9.0288),fxmetr(  6.5044),fxmetr(-10.0000),},
	{fxmetr( 29.0288),fxmetr(  6.5044),fxmetr(-10.0000),},
	{fxmetr( 29.0288),fxmetr(  0.4851),fxmetr(-10.0000),},
	{fxmetr(  9.0288),fxmetr(  0.4851),fxmetr(  9.9998),},
	{fxmetr(  9.0288),fxmetr(  6.5044),fxmetr(  9.9998),},
	{fxmetr( 29.0288),fxmetr(  6.5044),fxmetr(  9.9998),},
	{fxmetr( 29.0288),fxmetr(  0.4851),fxmetr(  9.9998),},
	{fxmetr( -9.6892),fxmetr( -0.3691),fxmetr( -2.3882),},
	{fxmetr( -9.6892),fxmetr(  2.2910),fxmetr( -2.3882),},
	{fxmetr( 11.0100),fxmetr(  2.2910),fxmetr( -2.3882),},
	{fxmetr( 11.0100),fxmetr( -0.3691),fxmetr( -2.3882),},
	{fxmetr( -9.6892),fxmetr( -0.3691),fxmetr(  1.6116),},
	{fxmetr( -9.6892),fxmetr(  2.2910),fxmetr(  1.6116),},
	{fxmetr( 11.0100),fxmetr(  2.2910),fxmetr(  1.6116),},
	{fxmetr( 11.0100),fxmetr( -0.3691),fxmetr(  1.6116),},
};

static IPlocha PZakladna[]=
{
	{Gold,_BC(1),_BC(0),_BC(2),},
	{Gold,_BC(2),_BC(0),_BC(3),},
	{Gold,_BC(4),_BC(5),_BC(6),},
	{Gold,_BC(4),_BC(6),_BC(7),},
	{Gold,_BC(1),_BC(2),_BC(6),},
	{Gold,_BC(5),_BC(1),_BC(6),},
	{Gold,_BC(6),_BC(2),_BC(7),},
	{Gold,_BC(2),_BC(3),_BC(7),},
	{Gold,_BC(0),_BC(1),_BC(5),},
	{Gold,_BC(4),_BC(0),_BC(5),},
	{Gold,_BC(9),_BC(8),_BC(10),},
	{Gold,_BC(10),_BC(8),_BC(11),},
	{Gold,_BC(12),_BC(13),_BC(14),},
	{Gold,_BC(12),_BC(14),_BC(15),},
	{SteelBlue,_BC(9),_BC(10),_BC(14),},
	{SteelBlue,_BC(13),_BC(9),_BC(14),},
	{Gold,_BC(14),_BC(10),_BC(15),},
	{Gold,_BC(10),_BC(11),_BC(15),},
	{Gold,_BC(8),_BC(9),_BC(13),},
	{Gold,_BC(12),_BC(8),_BC(13),},
	{Silver,_BC(17),_BC(16),_BC(18),},
	{Silver,_BC(18),_BC(16),_BC(19),},
	{Silver,_BC(20),_BC(21),_BC(22),},
	{Silver,_BC(20),_BC(22),_BC(23),},
	{Silver,_BC(17),_BC(18),_BC(22),},
	{Silver,_BC(21),_BC(17),_BC(22),},
};

#undef _BC

IKresliScena Zakladna={
	NULL,BZakladna,PZakladna,
	47,(bindex)lenof(BZakladna),(bindex)lenof(PZakladna)
};
