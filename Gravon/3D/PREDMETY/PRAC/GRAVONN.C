#define _BC(n) (n)
static IBod BGravonn[]=
{
	{fxmetr( -2.2729),fxmetr( -0.0002),fxmetr( -2.2161),},
	{fxmetr( -2.8411),fxmetr( -0.0002),fxmetr(  1.5908),},
	{fxmetr(  0.1133),fxmetr( -0.0002),fxmetr(  4.8865),},
	{fxmetr(  3.0674),fxmetr( -0.0002),fxmetr(  1.4773),},
	{fxmetr(  2.5566),fxmetr( -0.0002),fxmetr( -2.2158),},
	{fxmetr(  0.1133),fxmetr( -0.0002),fxmetr( -3.2952),},
	{fxmetr(  0.1133),fxmetr( -0.9092),fxmetr(  0.0000),},
	{fxmetr(  0.1133),fxmetr(  0.9087),fxmetr(  0.0000),},
	{fxmetr(  0.1702),fxmetr(  1.7048),fxmetr( -1.9868),},
	{fxmetr( -0.7954),fxmetr(  1.7615),fxmetr( -1.4807),},
	{fxmetr( -1.0227),fxmetr(  1.9319),fxmetr( -0.1001),},
	{fxmetr(  0.1135),fxmetr(  2.0457),fxmetr(  0.4980),},
	{fxmetr(  1.2498),fxmetr(  1.9319),fxmetr( -0.1924),},
	{fxmetr(  1.0793),fxmetr(  1.7615),fxmetr( -1.3425),},
	{fxmetr(  0.1704),fxmetr(  0.0000),fxmetr( -2.4011),},
	{fxmetr( -1.2500),fxmetr(  0.0000),fxmetr( -1.8027),},
	{fxmetr( -1.7046),fxmetr(  0.0000),fxmetr(  0.8203),},
	{fxmetr(  0.0566),fxmetr(  0.0000),fxmetr(  2.5391),},
	{fxmetr(  1.7607),fxmetr(  0.0000),fxmetr(  0.8203),},
	{fxmetr(  1.4771),fxmetr(  0.0000),fxmetr( -1.8027),},
};

static IPlocha PGravonn[]=
{
	{Copper,_BC(1),_BC(2),_BC(6),},
	{Copper,_BC(2),_BC(3),_BC(6),},
	{Copper,_BC(3),_BC(4),_BC(6),},
	{Copper,_BC(4),_BC(5),_BC(6),},
	{Copper,_BC(5),_BC(0),_BC(6),},
	{Copper,_BC(0),_BC(1),_BC(6),},
	{Silver,_BC(2),_BC(1),_BC(7),},
	{Silver,_BC(3),_BC(2),_BC(7),},
	{Silver,_BC(4),_BC(3),_BC(7),},
	{Silver,_BC(5),_BC(4),_BC(7),},
	{Silver,_BC(0),_BC(5),_BC(7),},
	{Silver,_BC(1),_BC(0),_BC(7),},
	{Gray35,_BC(12),_BC(8),_BC(13),},
	{Gray35,_BC(11),_BC(8),_BC(12),},
	{Gray35,_BC(10),_BC(8),_BC(11),},
	{Gray35,_BC(9),_BC(8),_BC(10),},
	{Gray75,_BC(17),_BC(11),_BC(18),},
	{Gray75,_BC(11),_BC(12),_BC(18),},
	{Gray75,_BC(12),_BC(13),_BC(18),},
	{Gray75,_BC(18),_BC(13),_BC(19),},
	{Gray75,_BC(13),_BC(8),_BC(19),},
	{Gray75,_BC(8),_BC(14),_BC(19),},
	{Gray75,_BC(14),_BC(8),_BC(15),},
	{Gray75,_BC(8),_BC(9),_BC(15),},
	{Gray75,_BC(9),_BC(10),_BC(16),},
	{Gray75,_BC(15),_BC(9),_BC(16),},
	{Gray75,_BC(10),_BC(11),_BC(16),},
	{Gray75,_BC(16),_BC(11),_BC(17),},
};

#undef _BC

IKresliScena Gravonn={
	NULL,BGravonn,PGravonn,
	45,(bindex)lenof(BGravonn),(bindex)lenof(PGravonn)
};
