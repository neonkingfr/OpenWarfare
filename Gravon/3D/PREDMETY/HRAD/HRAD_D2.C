#include "predmety.h"
#include "colors.h"
static IBod _BHrad_d2[]=
{
	{fxmetr( 12.2012),fxmetr( -1.0938),fxmetr(-23.8584),},
	{fxmetr(-14.1406),fxmetr( -1.0938),fxmetr(-13.4131),},
	{fxmetr(-14.1406),fxmetr(  8.0410),fxmetr(-13.4131),},
	{fxmetr( 12.2012),fxmetr(  8.0410),fxmetr(-23.8584),},
};

static IPlocha _PHrad_d2[]=
{
	{Gray75,(0),(2),(1),},
	{Gray75,(0),(1),(2),},
	{Gray75,(0),(3),(2),},
	{Gray75,(0),(2),(3),},
};

KKresliScena Hrad_d2={
	NULL,_BHrad_d2,_PHrad_d2,
	7,(bindex)lenof(_BHrad_d2),(bindex)lenof(_PHrad_d2)
};
