#version 410

in vec3 vColor;
in float dist;
in vec3 lit;
in vec3 emit;

in vec3 worldNormal;
in vec4 worldPos;

//out vec4 gl_FragColor;
uniform vec3 fogColor = vec3(1,1,1);

const float SpotExponent = 5.0f;
const float pi = 3.1415926536f;
const float SpotCosCutOff = 0.8; // cos(30deg);
const float SpotCosFull = 0.9; // cos(25deg);
const float SpotAtten0 = 0.0f;
const float SpotAtten1 = 0.05f;
const float SpotAtten2 = 0.06f;

struct SpotLight
{
  vec3 position;
  vec3 direction;
  vec3 diffuse;
  vec3 ambient;
  bool on;
};

const int NSpots = 7;
uniform SpotLight spotLight[NSpots];

in float material;

layout(pixel_center_integer) in vec4 gl_FragCoord;

void main()
{
  vec3 matColor = vColor;
  /*
  if (material<0) // -1 means terrain 
  { 
    float d_z = 20.0*8/64;
    float d_v = d_z*sqrt(3)/2;

    vec2 triCoord; 
    triCoord.x = (worldPos.x+worldPos.z*d_z/d_v/2.0)/d_z; 
    triCoord.y = worldPos.z/d_v; 
    triCoord += vec2(0.4,+0.55);
    vec2 relDot = mod(triCoord*4,1)-vec2(0.5,0.5); 
    if (relDot.x<0 && relDot.y>0 && -relDot.x+relDot.y<0.2) 
    {
      matColor *= 0.95; 
    }
  } 
  */
  float up = max(worldNormal.y,0);
  float down = max(-worldNormal.y,0);
  float fog = 1-exp(-0.1*dist);
  vec3 litSum = lit;
  for (int i=0; i<NSpots; i++) if (spotLight[i].on)
  {
    vec3 lightDir = worldPos.xyz-spotLight[i].position;
	  float pDotD = dot(spotLight[i].direction,normalize(lightDir));
	  if (pDotD > SpotCosCutOff)
    {
      float spotFactor = min(1,(pDotD-SpotCosCutOff)/(SpotCosFull-SpotCosCutOff));
      float dist1 = length(lightDir);
      float dist2 = dot(lightDir,lightDir);
      float dist0 = 1;
      float attenuation = min(1,1/(dist0*SpotAtten0+dist1*SpotAtten1+dist2*SpotAtten2));

      float diffuse = max(dot(normalize(lightDir),-worldNormal),0.0f);
      vec3 lightColor = diffuse*spotLight[i].diffuse+spotLight[i].ambient;
      litSum += lightColor*attenuation*spotFactor;
	 }
  }
  vec3 litColor = litSum*matColor+emit;
  vec3 fogged = mix(litColor,fogColor,fog);
  float alpha = 1;
  if (material>1.5) // 2 means stipple
  {
    // odd / even
    if (mod(gl_FragCoord.y,2)<1)
    {
      alpha = 0.35;
    }
    else
    {
      alpha = 0.65;
    }
  }
  gl_FragColor = vec4(fogged,alpha);
}