/* Visitor - projekce */
/* vytvoreni systemu hran pro dalsi algoritmus */
/* SUMA 11/1993-11/1993 */

#include <macros.h>
#include <math.h>
#include "horizont.h"

/* vypis z knihovny:
rand:
	MOVE.L	_rseed,D0
	; v D0 tvori bity - zhora:
	MOVE.L	D0,D1 ; 1
	LSL.L	#2,D0
	ADD.L	D1,D0 ; 01
	LSL.L	#2,D0
	ADD.L	D1,D0 ; 01
	ADD.L	D0,D0
	ADD.L	D1,D0 ; 1
	LSL.L	#2,D0
	ADD.L	D1,D0 ; 01
	LSL.L	#3,D0
	ADD.L	D1,D0 ; 001
	LSL.L	#2,D0
	ADD.L	D1,D0 ; 01
	LSL.L	#3,D0
	SUB.L	D1,D0 ; 001
	LSL.L	#4,D0
	ADD.L	D1,D0 ; 0001
	ADD.L	D0,D0
	ADD.L	D1,D0 ; 1
	LSL.L	#2,D0
	ADD.L	D1,D0 ; 01
	LSL.L	#2,D0
	ADD.L	D1,D0 ; 01
	ADDQ.L	#1,D0
	MOVE.L	D0,_rseed
	SWAP	D0
	AND.W	#$7FFF,D0
	RTS
	; tj. 0x15a5235=22696501
	; dohromady 1 0101 1010 0101 0010 0011 0101
*/

int KRand( unsigned long *seed )
{ /* 0..0x7fff */
	unsigned long r;
	r=(*seed)*22696501L+1;
	/* to je asi prvocislo */
	*seed=r;
	return (int)(r>>16)&0x7fff;
}

/* korekce na umisteni kamery */
void Posuv( PosT *D, const PosT *O )
{
	D->x-=O->x;
	D->y-=O->y;
	D->z-=O->z;
}

/**/
void Rotace( PosT *D, const PosT *MT )
{
	/* MT je matice rotace */
	long x,y;
	x=VM(D,MT++);
	y=VM(D,MT++);
	D->z=VM(D,MT);
	D->y=y,D->x=x;
}
/**/

#define MAX_TAN 16
#define KROK_TAN ( fx(MAX_TAN)/lenof(ATanTab) )

#include "sinetab.h"
#include "atantab.h"

#define CDF(a,b) fx( (double)(a)/(b) )

void InitProj( void ){} /* p�esunuto do tabulek */

int fxatan( long x )
{
	Flag sg=x<0;
	int ret;
	if( sg ) x=-x;
	if( x>fx(MAX_TAN) ) ret=89*UhelPom;
	else
	{
		long ip=x/KROK_TAN;
		if( ip>=lenof(ATanTab) ) ret=ATanTab[lenof(ATanTab)-1];
		else
		{
			long zb=x-ip*KROK_TAN;
			ret=ATanTab[ip]+(int)MKDF(ATanTab[ip+1]-ATanTab[ip],zb,KROK_TAN);
		}
	}
	return sg ? -ret : ret;
}

long fxsin( int x )
{
	while( x>=360*UhelPom ) x-=360*UhelPom;
	while( x<0 ) x+=360*UhelPom;
	if( x<=180*UhelPom )
	{
		if( x>90*UhelPom ) x=180*UhelPom-x;
		return +SineTab[x];
	}
	else
	{
		x-=180*UhelPom;
		if( x>90*UhelPom ) x=180*UhelPom-x;
		return -SineTab[x];
	}
}

long fxcos( int x )
{
	return fxsin(90*UhelPom-x);
}

#define FLOAT_ROT 0

#if !FLOAT_ROT

#define MF3(a,b,c) ( MF(MF(a,b),c) )

void MRot( PosT *M, const SmerT *U )
{
	/* vyrob M - matice rotace */
	/* rotace postupne kolem os y, z, x */
	/* M=O(x)*O(z)*O(y) */
	long sx=fxsin(U->x),sy=fxsin(U->y),sz=fxsin(U->z);
	long cx=fxcos(U->x),cy=fxcos(U->y),cz=fxcos(U->z);
	M[0].x=MF(cy,cz)-MF3(sy,sx,sz);
	M[0].y=MF(cx,sz);
	M[0].z=MF(sy,cz)+MF3(sx,cy,sz );
	M[1].x=MF(-cy,sz)-MF3(sy,sx,cz);
	M[1].y=MF(cx,cz);
	M[1].z=MF(-sy,sz)+MF3(sx,cy,cz);
	M[2].x=MF(-sy,cx);
	M[2].y=-sx;
	M[2].z=MF(cy,cx);
}

/* chyba - nov� oprava dne 1.9.95 */
/* inverzn� matice je transponov� matice norm�ln� */

void IMRot( PosT *M, const SmerT *U )
{
	/* vyrob M - matice rotace */
	/* rotace postupne kolem os y, z, x */
	/* M=O(x)*O(z)*O(y) */
	long sx=fxsin(U->x),sy=fxsin(U->y),sz=fxsin(U->z);
	long cx=fxcos(U->x),cy=fxcos(U->y),cz=fxcos(U->z);
	M[0].x=MF(cy,cz)-MF3(sy,sx,sz);
	M[1].x=MF(cx,sz);
	M[2].x=MF(sy,cz)+MF3(sx,cy,sz );
	M[0].y=MF(-cy,sz)-MF3(sy,sx,cz);
	M[1].y=MF(cx,cz);
	M[2].y=MF(-sy,sz)+MF3(sx,cy,cz);
	M[0].z=MF(-sy,cx);
	M[1].z=-sx;
	M[2].z=MF(cy,cx);
}

#else

void MRot( PosT *M, const SmerT *U )
{
	/* vyrob M - matice rotace */
	/* rotace postupne kolem os y, z, x */
	/* M=O(x)*O(z)*O(y) */
	double degk=M_PI/(180L*UhelPom);
	double x=(double)U->x*degk;
	double y=(double)U->y*degk;
	double z=(double)U->z*degk;
	double sx,cx,sy,cy,sz,cz;
	sincos(x,&sx,&cx);
	sincos(y,&sy,&cy);
	sincos(z,&sz,&cz);
	M[0].x=(long)( ( cy*cz-sy*sx*sz ) * PosPom );
	M[0].y=(long)( ( cx*sz ) *PosPom );
	M[0].z=(long)( ( sy*cz+sx*cy*sz ) * PosPom );
	M[1].x=(long)( (-cy*sz-sy*sx*cz ) * PosPom );
	M[1].y=(long)( ( cx*cz ) * PosPom );
	M[1].z=(long)( (-sy*sz+sx*cy*cz ) * PosPom );
	M[2].x=(long)( (-sy*cx ) * PosPom );
	M[2].y=(long)( (-sx ) * PosPom );
	M[2].z=(long)( ( cy*cx ) * PosPom );
}

void IMRot( PosT *M, const SmerT *U )
{
	/* vyrob M - inverzni matice rotace */
	/* rotace postupne kolem os x, z, y */
	/* M=O(-y)*O(-z)*O(-x) */
	double degk=M_PI/(180L*UhelPom);
	double x=(double)-U->x*degk;
	double y=(double)-U->y*degk;
	double z=(double)-U->z*degk;
	double sx,cx,sy,cy,sz,cz;
	sincos(x,&sx,&cx);
	sincos(y,&sy,&cy);
	sincos(z,&sz,&cz);
	M[0].x=(long)( ( cy*cz ) * PosPom );
	M[0].y=(long)( ( cx*cy*sz-sx*sy ) *PosPom );
	M[0].z=(long)( ( sx*cy*sz+sy*cx ) * PosPom );
	M[1].x=(long)( (-sz ) * PosPom );
	M[1].y=(long)( ( cx*cz ) * PosPom );
	M[1].z=(long)( ( sx*cz ) * PosPom );
	M[2].x=(long)( (-sy*cz ) * PosPom );
	M[2].y=(long)( (-cx*sy*sz-sx*cy ) * PosPom );
	M[2].z=(long)( (-sx*sy*sz+cx*cy ) * PosPom );
}

#endif

void IVektRot( PosT *v, const SmerT *u )
{
	PosT M[3];
	IMRot(M,u);
	Rotace(v,M);
}
void VektRot( PosT *v, const SmerT *u )
{
	PosT M[3];
	MRot(M,u);
	Rotace(v,M);
}

void MatMul( PosT *r, const PosT *a, const PosT *b )
{
	/* r=a x b; */
	PosT bt[3];
	int y;
	/* abychom mohli nas. radky, vytvorime transponovanou matici */
	bt[0].x=b[0].x,bt[1].x=b[0].y,bt[2].x=b[0].z;
	bt[0].y=b[1].x,bt[1].y=b[1].y,bt[2].y=b[1].z;
	bt[0].z=b[2].x,bt[1].z=b[2].y,bt[2].z=b[2].z;
	for( y=0; y<3; y++ )
	{
		r[y].x=VM(&a[y],&bt[0]);
		r[y].y=VM(&a[y],&bt[1]);
		r[y].z=VM(&a[y],&bt[2]);
	}
}

int NormUhel( int Uhel )
{ /* do rozsahu -180, +180 */
	while( Uhel<-180*UhelPom ) Uhel+=360*UhelPom;
	while( Uhel>+180*UhelPom ) Uhel-=360*UhelPom;
	return Uhel;
}

/* reseni soustav lin. rovnic */

int Vyres( Soustava *S, long *res )
{ /* pouzije celkem 9 deleni a 12 nasobeni */
	int i,j,k,l;
	int BylPivot[LS_RNG]; /* kde v radku byl pivot */
	for( i=0; i<LS_RNG; i++ ) BylPivot[i]=-1;
	for( i=0; i<LS_RNG; i++ )
	{ /* postupne nuluj sloupce */
		int JePivot=0;
		for( j=0; j<LS_RNG; j++ )
		{ /* najdi pivota */
			if( S->L[j][i] && BylPivot[j]<0 )
			{
				BylPivot[j]=i;
				JePivot=1;
				for( k=0; k<LS_RNG; k++ ) if( k!=j )
				{
					long K=DF(S->L[k][i],S->L[j][i]);
					for( l=0; l<LS_RNG; l++ )
					{
						if( l!=i ) S->L[k][l]-=MF(K,S->L[j][l]);
						else S->L[k][l]=0; /* nula by stejne mela vyjit */
					}
					S->P[k]-=MF(K,S->P[j]);
				}
				break;
			}
		}
		if( !JePivot ) return -1; /* degradovana */
	}
	for( j=0; j<LS_RNG; j++ )
	{
		int i=BylPivot[j];
		res[i]=DF(S->P[j],S->L[j][i]);
	}
	return 0;
}


long VelVec( const PosT *V )
{
	long x=V->x,y=V->y,z=V->z;
	long m=x,v;
	int log;
	if( m<0 ) m=-m;
	v=y;if( v<0 ) v=-v;m+=v;
	v=z;if( v<0 ) v=-v;m+=v;
	/* m=|x|+|y|+|z| */
	/* plati m*m>=x*x+y*y+z*z */
	/* pro bezpecny prubeh musi byt x*x*+y*y+z*z < 2^31 */
	/* to jiste plati, je-li m*m < 2^31, a to je jiste, kdyz m < 2^15 */
	for( log=0; m>(1L<<15); log++ ) m>>=1;
	x>>=log;
	y>>=log;
	z>>=log;
	x=isqrt(x*x+y*y+z*z,16);
	x<<=log;
	return x;
}

long Vzdalenost( const PosT *B, const PosT *b )
{
	PosT r;
	r.x=B->x-b->x;
	r.y=B->y-b->y;
	r.z=B->z-b->z;
	return VelVec(&r);
}

Flag VzdXZVetsi( const PosT *B, const PosT *b, long max )
{
	long v,s;
	PosT d;
	v=B->x-b->x;d.x=v;if( v<0 ) v=-v;
	if( v>max ) return True;
	s=v;
	v=B->z-b->z;d.z=v;if( v<0 ) v=-v;
	if( v>max ) return True;
	s+=v;
	if( s<max ) return False;
	d.y=0;
	v=VelVec(&d);
	return v>max;
}
Flag VzdVetsi( const PosT *B, const PosT *b, long max )
{
	long v,s;
	v=B->x-b->x;if( v<0 ) v=-v;
	if( v>max ) return True;
	s=v;
	v=B->z-b->z;if( v<0 ) v=-v;
	if( v>max ) return True;
	s+=v;
	if( s<max ) return False;
	v=Vzdalenost(B,b);
	if( v>max ) return True;
	return False;
}
Flag AVzdVetsi( const PosT *B, const PosT *b, long max )
{
	long v;
	v=B->x-b->x;if( v<0 ) v=-v;
	if( v>max ) return True;
	v=B->z-b->z;if( v<0 ) v=-v;
	if( v>max ) return True;
	v=B->y-b->y;if( v<0 ) v=-v;
	if( v>max ) return True;
	return False;
}

#if __DEBUG
	#include <stdio.h>
	#include <string.h>
	
	FILE *Rep=NULL;
	
	void RepFixed( const char *S, long f )
	{
		if( !Rep ) Rep=fopen("REPORT.TXT","a");
		if( Rep )
		{
			long des,cela;
			int sg;
			char Buf[80];
			if( f<0 ) f=-f,sg='-';
			else sg='+';
			cela=f/fx(1);
			des=f%fx(1);
			des=MKDK(des,100000L,fx(1));
			sprintf(Buf,"%3s = %c%6ld.%05ld",S,sg,cela,des);
			fprintf(Rep,"%s\n",Buf);
			fclose(Rep),Rep=NULL;
		}
	}
	
	static void SMetry( char *S, long f )
	{
		long des,cela;
		int sg;
		if( f<0 ) f=-f,sg=-1;
		else sg=+1;
		cela=f/fxmetr(1);
		des=f%fxmetr(1);
		cela*=sg;
		des=MKDK(des,100000L,fxmetr(1));
		sprintf(S+strlen(S)," %+6ld.%05ld",cela,des);
	}
	void RepMetry( const char *S, long f )
	{
		if( !Rep ) Rep=fopen("REPORT.TXT","a");
		if( Rep )
		{
			char Buf[80];
			strcpy(Buf,S);
			SMetry(Buf,f);
			fprintf(Rep,"%s\n",Buf);
			fclose(Rep),Rep=NULL;
		}
	}
	void RepLong( const char *S, long f )
	{
		if( !Rep ) Rep=fopen("REPORT.TXT","a");
		if( Rep )
		{
			fprintf(Rep,"%3s=%ld\n",S,f);
			fclose(Rep),Rep=NULL;
		}
	}
	void RepPos( const char *S, const PosT *P )
	{
		if( !Rep ) Rep=fopen("REPORT.TXT","a");
		if( Rep )
		{
			char Buf[80];
			strcpy(Buf,S);
			SMetry(Buf,P->x);
			SMetry(Buf,P->y);
			SMetry(Buf,P->z);
			fprintf(Rep,"%s\n",Buf);
			fclose(Rep),Rep=NULL;
		}
	}
#endif
