/* Horizont - komunikace s DSP */
/* SUMA 3/1994-11/1994 */
/* funk�n� verze - s blitterem (prioritn� je blitter) */

include "macros.i"

import DVAdr
import LinePixels
import VWid

HalfTone equ $FFFF8A00 ; word

SrcXInc equ $FFFF8A20 ; int
SrcYInc equ $FFFF8A22 ; int
SrcAdr equ $FFFF8A24 ; long

EndMask1 equ $FFFF8A28 ; word ;
EndMask2 equ $FFFF8A2A ; word ;
EndMask3 equ $FFFF8A2C ; word ;

DstXInc equ $FFFF8A2E ; int ;
DstYInc equ $FFFF8A30 ; int ;
DstAdr equ $FFFF8A32 ; int ;

Width equ $FFFF8A36 ; word
Height equ $FFFF8A38 ; word ;
HalfOp equ $FFFF8A3A ; byte ;
LogOp equ $FFFF8A3B ; byte ;
LineNum equ $FFFF8A3C ; byte ;
SKEW equ $FFFF8A3D ; byte ;

Dsp_IRQ =$ffffa200
Dsp_HSR =$ffffa202
Dsp_IVec=$ffffa203
Dsp_HRX =$ffffa204
Dsp_HTX =$ffffa204

Dsp_IRQOnRcv=0

/* ��sla bit� - pro BTST, ne jako v .H, kde jsou to masky pro AND */

Dsp_RBF=0 /* Data Ready */
Dsp_CTS=1 /* Clear To Send */

MC68030

import DspBusy

import PrevezmiCnt
export NextAETDsp
import DspProg

super
export RemReceive

module RemReceive
	bclr.b #Dsp_IRQOnRcv,Dsp_IRQ
	clr DspBusy
	rts
endmod

; #define Dsp_WaitHSR(HSR,TF) while( !(TF&(*HSR)) );
macro Dsp_WaitHSR HSR,TF
	local Lop
	Lop:
		btst.b TF,(HSR)
	beq.b Lop
endm

module BlitInit
	WaitFreBlit:
		btst #7,LineNum
	bne.b WaitFreBlit
	
	move #-1,EndMask1
	move #-1,EndMask2
	move #-1,EndMask3
	
	
	move #2,DstXInc
	move #2,DstYInc

	move #0,Width
	move #1,Height
	
	move.b #$40,SKEW
	move.b #1,HalfOp
	move.b #3,LogOp
	rts
endmod

HTA equ a0
HSR equ a1

LowPrior equ 0 ; dobr� pro Profiler, jen nen�-li IRQDriven
IRQDriven equ 1
BlitHOG equ 1 ; HOG b�v� rychlej�

export XCnt

module DspReceive
	if IRQDriven==0
		btst #0,Dsp_HSR
		beq.b NoRcv
	endif
		V=d0
		x=d1
		Bit=d2
		
		if IRQDriven
			NextRcv:
			move.l V,-(a7)
		else
			UsedRegs reg HTA/HSR/x/V/Bit
			movem.l #UsedRegs,-(a7)
		endif
		
		
		if LowPrior
			bclr.b #Dsp_IRQOnRcv,Dsp_IRQ ; zaka� p�eru�en� od DSP
			
			move sr,d0 ; povol preruseni s nizsi prior.
			sub #$100,d0
			move d0,sr
		endif
		
		if IRQDriven
			move.l Dsp_HRX,V
			
			if BlitHOG==0
				WaitBlit:
					btst.b #7,LineNum
				bne.b WaitBlit
			endif
			
			move V,HalfTone
			swap V
			and.w #$ff,V
			move V,Width
			
			move #1,Height

			if BlitHOG
				move.b #$C0,LineNum
			else
				move.b #$80,LineNum
			endif

			sub V,XCnt
		 	bgt.b NextStripe ; neskon�il ��dek - nepos�lej dal�
		 	
		 	move VWid,XCnt

		else
			lea Dsp_HRX,HTA
			lea Dsp_HSR,HSR
			moveq #Dsp_RBF,Bit
			move VWid,x
			bra.b Blit ; 1. slovo jsme ur�it� dostali
			XLop:
				Dsp_WaitHSR HSR,Bit
				Blit:
				move.l (HTA),V
				
				move V,HalfTone
				swap V
				and.w #$ff,V
				move V,Width
				
				move #1,Height
				move.b #$C0,LineNum
				
				sub V,x
		 	bgt.b XLop
		endif
		
		subq #1,PrevezmiCnt
		bgt.b NebylPosledni
			bclr.b #Dsp_IRQOnRcv,Dsp_IRQ
			clr DspBusy
		if IRQDriven
			move.l (a7)+,V
		else
			movem.l (a7)+,#UsedRegs
		endif
		rte
		NebylPosledni:
			if IRQDriven
				movem.l HTA/HSR/Bit/x,-(a7)
				lea Dsp_HRX,HTA
				lea Dsp_HSR,HSR
				bsr.b NextAETDsp
				movem.l (a7)+,HTA/HSR/Bit/x
				NextStripe:
			else
				bsr.b NextAETDsp
			endif
		if IRQDriven
			move.l (a7)+,V
		else
			movem.l (a7)+,#UsedRegs
		endif

		if LowPrior
			bset.b #Dsp_IRQOnRcv,Dsp_IRQ ; povol p�eru�en� od DSP
			if IRQDriven
				btst #0,Dsp_HSR
				bne NextRcv
			endif
		endif
	NoRcv:
	rte
NextAETDsp:
	DB=a2
	N=d0
	CTS=d1
	move.l a2,d2
	move.l DspProg,DB
	moveq #Dsp_CTS,CTS
	Dsp_WaitHSR HSR,CTS
	move.l (DB)+,(HTA) ; povel
	Dsp_WaitHSR HSR,CTS
	move.l (DB)+,(HTA) ; clipw
	move.l (DB)+,N
	Dsp_WaitHSR HSR,CTS
	move.l N,(HTA)
	bra.b NWhile
	NLop:
		rept 9 ; x,z, L,P, dxl,dxh, dzl,dzh, hh
			Dsp_WaitHSR HSR,CTS
			move.l (DB)+,(HTA)
		endm
	NWhile:
	dbf N,NLop
	move.l DB,DspProg
	move.l d2,a2
	rts
	if IRQDriven
		bss
		XCnt: ds.w 1
		text
	endif
endmod

; v syst�mu je DspNumVec 255
DspNumVec=0xff ; ��d� ��slo vektoru, mo�n� i prioritu
DspVec=DspNumVec*4

export SetReceive

module SetReceive
	bsr BlitInit
	move.l DVAdr,DstAdr
	move.l #DspReceive,DspVec
	move.b #DspNumVec,Dsp_IVec
	bset.b #Dsp_IRQOnRcv,Dsp_IRQ
	move #1,DspBusy
	if IRQDriven
		move VWid,XCnt
	endif
	lea Dsp_HTX,HTA
	lea Dsp_HSR,HSR
	jmp NextAETDsp
endmod
