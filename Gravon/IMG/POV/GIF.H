#define FALSE 0
#define TRUE 1

typedef struct {unsigned short Red, Green, Blue, Filter;} IMAGE_COLOUR;

struct Image_Line
  {
   unsigned char *red, *green, *blue;
  };

typedef struct Image_Line IMAGE_LINE;

/* Legal image attributes */
#define GIF_FILE   1
#define POT_FILE   2
#define DUMP_FILE  4
#define IFF_FILE   8
#define TGA_FILE  16
#define GRAD_FILE 32

#define IMAGE_FILE    GIF_FILE+DUMP_FILE+IFF_FILE+GRAD_FILE+TGA_FILE
#define NORMAL_FILE   GIF_FILE+DUMP_FILE+IFF_FILE+GRAD_FILE+TGA_FILE
#define MATERIAL_FILE GIF_FILE+DUMP_FILE+IFF_FILE+GRAD_FILE+TGA_FILE
#define HF_FILE       GIF_FILE+POT_FILE+TGA_FILE

typedef struct Image_Struct
  {
   int Map_Type;
   int File_Type;
   int Interpolation_Type;
   short Once_Flag;
   short Use_Colour_Flag;
   /*VECTOR Gradient;*/
   /*double width, height;*/
   int iwidth, iheight;
   short Colour_Map_Size;
   IMAGE_COLOUR *Colour_Map;
   unsigned char **map_lines;
  } IMAGE;

/* Prototypes for functions defined in gif.c */
int out_line( unsigned char *pixels, int linelen );
int get_byte( FILE *f );

/* Prototypes for functions defined in gifdecod.c */
void cleanup_gif_decoder( void );
int init_exp( int i_size );   /* changed param to int to avoid
					 problems with 32bit int ANSI
					 compilers. */
int get_next_code( FILE *f );
int decoder( FILE *f, int i_linewidth ); /* same as above */

 void Read_Gif_Image( IMAGE *Image, const char *filename );
