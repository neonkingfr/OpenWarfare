/* �ten� do True Color */
/* SUMA 1993/4-1995/12 */

#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <macros.h>
#include <stdc\imgload.h>
#include <stdc\memspc.h>
#include <stdc\fileutil.h>

#include "obrazky.h"

struct jag_rgb565
{
	unsigned r:5;
	unsigned b:5;
	unsigned g:6;
};

struct rgb565
{
	unsigned r:5;
	unsigned g:6;
	unsigned b:5;
};

static void Konvert888to565( Obrazek *Obr )
{
	long L=(long)Obr->W*Obr->H,l=L;
	byte *B=(byte *)Obr->Obr;
	word *D=Obr->Obr;
	word *Buf;
	while( --L>=0 )
	{
		word r=*B++;
		word g=*B++;
		word b=*B++;
		*D++=((r>>3)<<11)|((g>>2)<<5)|(b>>3);
	}
	Obr->NPlanu=16;
	Buf=reallocSpc(Obr->Obr,l*2,'_888');
	if( Buf ) Obr->Obr=Buf;
}

static int Konvert565to888( Obrazek *Obr )
{
	long N=(long)Obr->W*Obr->H;
	struct rgb565 *S=Obr->Obr;
	char *D=malloc(N*3),*DD=D;
	if( DD )
	{
		struct rgb565 *SS=S;
		while( --N>=0 )
		{
			*D++=S->r<<3;
			*D++=S->g<<2;
			*D++=S->b<<3;
			S++;
		}
		free(SS);
		Obr->Obr=DD;
	}
	if( DD ) return 0;
	return -1;
}

static void KonvertJag2Falc( Obrazek *Obr )
{
	long N=(long)Obr->W*Obr->H;
	struct jag_rgb565 *S=Obr->Obr;
	while( --N>=0 )
	{
		int r=S->r;
		int g=S->g;
		int b=S->b;
		struct rgb565 *D=(struct rgb565 *)S;
		D->r=r;
		D->g=g;
		D->b=b;
		S++;
	}
}

static int NactiPJC( const char *N, Obrazek *Obr )
{
	Obr->Obr=RAWPCCLoad(N,&Obr->W,&Obr->H);
	if( !Obr->Obr ) return -1;
	KonvertJag2Falc(Obr);
	Obr->NPlanu=16;
	Obr->Direct=True;
	Obr->Gray=False;
	return 0;
}

static int NactiPCC( const char *N, Obrazek *Obr )
{
	Obr->Obr=PCCLoad(N,&Obr->W,&Obr->H);
	Obr->NPlanu=16;
	if( !Obr->Obr ) return -1;
	Obr->Direct=True;
	Obr->Gray=False;
	return 0;
}

static int NactiTPI( const char *N, Obrazek *Obr )
{
	int NCol;
	Obr->Obr=TPILoad(N,&Obr->W,&Obr->H,&Obr->NPlanu,&NCol,Obr->RGBPal);
	if( !Obr->Obr ) return -1;
	if( NCol!=0 )
	{
		freeSpc(Obr->Obr);
		return -1;
	}
	Obr->Direct=True;
	Obr->Gray=False;
	return 0;
}

static int NactiIMG( const char *N, Obrazek *Obr )
{
	Obr->Obr=IMGLoad(N,&Obr->W,&Obr->H,&Obr->NPlanu);
	if( !Obr->Obr ) return -1;
	Obr->Direct=True;
	Obr->Gray=False;
	return 0;
}
static int NactiTGA( const char *N, Obrazek *Obr )
{
	Obr->Obr=TGALoad(N,&Obr->W,&Obr->H,&Obr->NPlanu);
	if( !Obr->Obr ) return -1;
	Obr->Direct=True;
	Obr->Gray=False;
	return 0;
}

int NactiTC( const char *S, Obrazek *Obr )
{
	int ret;
	const char *E=NajdiExt(NajdiNazev(S));
	if( !strcmpi(E,"IMG") ) ret=NactiIMG(S,Obr);
	ef( !strcmpi(E,"TPI") ) ret=NactiTPI(S,Obr);
	ef( !strcmpi(E,"PCC") ) ret=NactiPCC(S,Obr);
	ef( !strcmpi(E,"PJC") ) ret=NactiPJC(S,Obr);
	ef( !strcmpi(E,"TGA") ) ret=NactiTGA(S,Obr);
	else ret=-1;
	if( ret>=0 )
	{
		printf("Source: %s %dx%dx%db\n",S,Obr->W,Obr->H,Obr->NPlanu);
	}
	return ret;
}

int NactiTC16( const char *S, Obrazek *Obr )
{
	int ret=NactiTC(S,Obr);
	if( ret>=0 )
	{
		if( Obr->NPlanu==24 )
		{
			printf("Transforming format from 24b to 16b\n");
			Konvert888to565(Obr);
		}
		ef( Obr->NPlanu==16 ){}
		else
		{
			printf("Input format must be 16b or 24b\n");
			freeSpc(Obr->Obr);
			return -1;
		}
	}
	return ret;
}

int NactiTC24( const char *S, Obrazek *Obr )
{
	int ret=NactiTC(S,Obr);
	if( ret>=0 )
	{
		if( Obr->NPlanu==24 )
		{
		}
		ef( Obr->NPlanu==16 )
		{
			printf("Transforming format from 16b to 24b.\n");
			if( Konvert565to888(Obr) )
			{
				printf("Not enough memory.\n");
				exit(1);
			}
		}
		else
		{
			printf("Input format must be 16b or 24b.\n");
			freeSpc(Obr->Obr);
			return -1;
		}
	}
	return ret;
}
