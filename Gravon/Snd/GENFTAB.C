/* krok. tabulka pro Componium (Jag) */
/* SUMA 1995/12 */

#include <macros.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <stdio.h>
#include <sndbind.h>
#include <sttos\samples.h>
#include <stdc\memspc.h>
#include <stdc\fileutil.h>

void *mallocSpc( size_t A, long Idtf )
{
	(void)Idtf;
	return malloc(A);
}
void *reallocSpc( void *Co, size_t Kolik, long Idtf )
{
	(void)Idtf;
	return realloc(Co,Kolik);
}

void freeSpc( void *B )
{
	free(B);
}

void DebugError( int Line, const char *File )
{
	printf("Debug: %s %d",File,Line);
}

static double FreqIs=16620;

const static double FTab[12]=
{
	2093.00,2217.46,2349.31,2489.01,2637.02,2793.82,
	2959.95,3135.95,3322.43,3520.00,3729.30,3951.06,
}; /* frekvence tonu - podle fyz. tab. okt. c4-h4 (7) */

static double ZjistiFreq( int Nota )
{
	enum {BasOkt=7};
	int OMK=Nota/12-BasOkt;
	double Frq=FTab[Nota%12];
	if( OMK<0 ) Frq/=1<<-OMK;
	else Frq*=1<<OMK;
	return Frq;
}

enum /* viz UTLCOMPO.H */
{
	FyzOkt=9, /* oktavovy rozsah vyst. zarizeni */
	CisMinOkt=0, /* vzdal. prvni oktavy od C2 */
};

#define MinNKrok (FyzOkt*12)
#define NKrok ( (MinNKrok+3)/4*4 )

long TonKrok[NKrok];

static void SpoctiKonv( avr_t *H )
{
	int PT;
	int O;
	double K1;
	double FMK=ZjistiFreq((char)H->avr_midinote);
	K1=((double)CompoFreqSam(H)/1000)/FreqIs*0x10000L;
	K1*=0x100; /* na Jagu�ru pou��v�me 8.24 b */
	K1/=FMK;
	for( O=0; O<FyzOkt; O++ ) for( PT=0; PT<12; PT++ )
	{
		int off=FyzOkt-O-(FyzOkt-6);
		double FT;
		if( off>=0 ) FT=FTab[PT]/(1<<off);
		else FT=FTab[PT]*(1<<-off);
		TonKrok[O*12+PT]=(long)( K1*FT+0.5 );
	}
}

int main( int argc, const char *argv[] )
{
	PathName out;
	avr_t H;
	void *B;
	int f;
	if( argc<2 )
	{
		printf("genftab [-f=xxxx] sourcefile\n");
		exit(1);
	}
	while( *argv[1]=='-' )
	{
		/* options */
		if( argv[1][1]=='f' )
		{
			if( argv[1][2]!='=' ) exit(1);
			FreqIs=atol(&argv[1][3]);
			if( FreqIs<=10 || FreqIs>=1000000L ) exit(1);
		}
		else exit(1);
		argv++;
	}
	if( !argv[1] || !*argv[1] ) exit(1);
	if( argv[2] && *argv[2] ) exit(1);
	B=ZvukLoad(&H,argv[1]);
	if( !B ) exit(1);
	SpoctiKonv(&H);
	strcpy(out,argv[1]);
	strcpy(NajdiPExt(NajdiNazev(out)),".stp");
	f=creat(out);
	if( f>=0 )
	{
		if( sizeof(TonKrok)!=write(f,TonKrok,sizeof(TonKrok)) ) exit(1);
		if( close(f)!=0 ) exit(1);
		return 0;
	}
	return 1;
}
