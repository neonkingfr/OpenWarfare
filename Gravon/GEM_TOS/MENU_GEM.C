/* GEM */
/* SUMA 6/1993-10/1993 */

#include <macros.h>
#include <stdlib.h>
#include <string.h>
#include <aes.h>
#include <gem\interf.h> /* zpetne vyuziva interf */
#include "aesglobl.h"
#include "gembios.h"

#pragma warn -par

OBJECT *AESMenu;

int menu_bar( OBJECT *me_btree, int me_bshow )
{
	if( me_bshow )
	{
		WindRect W;
		ObjcRRect(me_btree,0,&W);
		objc_draw(me_btree,1,2,W.x,W.y,W.w,W.h);
		AESMenu=me_btree;
		AESFullDesk.h=AESFull.h-(me_btree[1].ob_height+1);
		AESFullDesk.y=AESFull.y+(me_btree[1].ob_height+1);
		Mouse(Off);
		HCara(0,AESFullDesk.w-1,AESFullDesk.y-1,7);
		Mouse(On);
	}
	else
	{
		AESMenu=NULL;
	}
	return 1;
}

int menu_icheck( OBJECT *me_ctree, int me_citem, int me_ccheck )
{
	OBJECT *O=&me_ctree[me_citem];
	if( me_ccheck ) O->ob_state|=CHECKED;
	else O->ob_state&=~CHECKED;
	return 1;
}

int menu_ienable( OBJECT *me_etree, int me_eitem, int me_eenable )
{
	OBJECT *O=&me_etree[me_eitem];
	if( me_eenable ) O->ob_state&=~DISABLED;
	else O->ob_state|=DISABLED;
	return 1;
}

int menu_tnormal( OBJECT *me_ntree, int me_ntitle, int me_nnormal )
{
	int NS=me_ntree[me_ntitle].ob_state;
	WindRect W;
	if( me_nnormal ) NS&=~SELECTED;
	else NS|=SELECTED;
	ObjcRRect(me_ntree,me_ntitle,&W);
	objc_change(me_ntree,me_ntitle,0,W.x,W.y,W.w,W.h,NS,True);
	Mouse(On);
	HCara(0,AESFullDesk.w-1,AESFullDesk.y-1,7);
	Mouse(Off);
	return 1;
}

int menu_text( OBJECT *me_ttree, int me_titem, const char *me_ttext )
{
	OBJECT *O=&me_ttree[me_titem];
	strcpy(O->ob_spec.free_string,me_ttext);
	return 1;
}

int menu_register( int me_rapid, const char *me_rpstring ){return 0;}

