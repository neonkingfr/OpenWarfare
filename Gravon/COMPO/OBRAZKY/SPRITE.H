#define BPL 80

enum
{
	MaxSprN=10,
	NScr=2 /* pocet obrazovek v shotsystemu */
};

typedef word S_Image[1];

typedef struct
{
	int s_h,s_w; /* vyska, sirka ve wordech */
	S_Image Img; /* obraz i maska */
	/* monochr. - strida se word obraz/maska */
	/* color    - 3 wordy plany/word maska - 4 plan doplnen podle tymu */
} Sprite;

/* SPRITE.S */
void Draw( int x, int y, Sprite *Def );

void TiskC( word *VAdr, int Num );
void TiskS( word *VAdr, char *Str );
void TiskSG( int x, word *VAdr, char *Str );
void TiskSGO( int x, word *VAdr, char *Str );

void Sups( void *Obsah );
void Prolni( void *T, void *s, void *S, word *R0 );

/* SPRITES.C */

int LoadPc( word *Scr, word *Pal, int FIn );

extern word RozsirB[256];

extern word *VRA;
extern byte Font[0x1800+0x80*3];
extern Flag Mono;
