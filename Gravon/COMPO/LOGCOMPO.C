/* Componium - vst/vyst operace */
/* SUMA, 9/1992-1/1994 */

#include <macros.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <tos.h>
#include <gem\interf.h>
#include <gem\multiitf.h>
#include <stdc\infosoub.h>

#include "digill.h"
#include "ramcompo.h"
#include "compo.h"

#define DEMO 0 /* z�kaz ukl�d�n� a importu */

#if !DEMO
	#define IMPORT_MID 1 /* je�t� nen� hotov */
#endif


Flag Demo=DEMO; /* pro ABOUT */

static Err Neum;
static Flag NeumSet;

static int NeumimF( const char *Slovo, SekvKursor *s )
{
	if( !NeumSet )
	{
		char Sl[40];
		FileName Nz;
		strlncpy(Sl,Slovo,sizeof(Sl));
		strlncpy(Nz,NajdiNazev(s->S->Pis->NazevS),sizeof(Nz));
		Transf(Sl),Transf(Nz);
		switch( AlertRF(EFORMA,2,Sl,Nz) )
		{
			case 1: Neum=OK;NeumSet=True;break;
			case 2: Neum=OK;break;
			default: Neum=ERR;break;
		}
	}
	return Neum;
}

Pisen *CtiMelF( const char *Nam )
{
	Pisen *P;
	NeumSet=False;
	Mouse(HOURGLASS);
	{
		void *CC=ChybaC;
		ChybaC=NeumimF;
		P=CtiPisen(Nam);
		ChybaC=CC;
		{
			int i=ToNOkno();
			if( i>=0 ) ForceKresli(i);
		}
	}
	if( !P )
	{ /* zalozit novou */
		Sekvence *S;
		P=NovaPisen(Nam);
		if( P ) S=NovaSekv(P,MainMel);
		if( !P || !S ) {Chyba(ERAM);if( P ) MazPisen(P);return NULL;}
		P->Zmena=False;
		return P;
	}
	if( (long)P<0 ) switch( (int)P )
	{
		FileName FN;
		case EFIL: strlncpy(FN,NajdiNazev(Nam),sizeof(FN));Transf(FN);AlertRF(IERRA,1,FN);return NULL;
		case ERAM: Chyba(ERAM);return NULL;
		default: if( Neum!=ERR )
		{
			strlncpy(FN,NajdiNazev(Nam),sizeof(FN));
			Transf(FN);
			AlertRF(EKFORMA,1,FN);
		}
		return NULL;
	}
	return P;
}

static Err PisSekv( FILE *f, Sekvence *S )
{
	int c;
	SekvText *s=OtevriT(S);
	if( !s ) return ERR;
	if( EOF==fprintf(f,"%s\n{\n",ZBanky(S->Pis->NazvySekv,S->INazev)) ) return ERR;
	while( (c=CtiT(s))>=OK ) if( EOF==fputc(c,f) ) return ERR;
	if( EOF==fprintf(f,"\n}\n") ) return ERR;
	ZavriT(s);
	return OK;
}

Flag PisMelF( const char *Nam, Pisen *P )
{
	#if !DEMO
	FILE *f;
	Sekvence *S,*H;
	int ret=OK;
	Mouse(HOURGLASS);
	f=fopen(Nam,"w");
	if( !f ) goto Error;
	if( !Rezerva ) setvbuf(f,NULL,_IOFBF,18*1024);
	for( S=PrvniSekv(P); S; S=DalsiSekv(S) )
	{
		for( H=S; H; H=DalsiHlas(H) ) if( (ret=PisSekv(f,H))!=EOK ) break;
	}
	if( ret==EOK )
	{ /* jeste nazvy nastroju na klvesach */
		int i;
		for( i=0; i<NFNum; i++ )
		{
			if( EOF==fprintf(f,"#Instr %d %s\n",i,NasF(P,i)) ) {ret=ERR;break;}
		}
	}
	if( ret==EOK ) if( EOF==fprintf(f,"#Comment %s\n",P->Popis1) ) ret=ERR;
	if( ret==EOK ) if( EOF==fprintf(f,"#Comment %s\n",P->Popis2) ) ret=ERR;
	if( ret==EOK ) ret=UlozLocaly(f,P);
	if( ret==EOK ) if( fclose(f)==EOF ) ret=ERR;
	else fclose(f);
	if( ret )
	{
		FileName FN;
		Error:
		strlncpy(FN,NajdiNazev(Nam),sizeof(FN));
		Transf(FN);
		AlertRF(WERRA,1,FN);
		return False;
	}
	return True;
	#else
	DoMenu(-1); /* hod about */
	(void)Nam,(void)P;
	return True;
	#endif
}

PathName PisenP="A:\\*.CPS";
static PathName PisenW;
static FileName PisenF="";

void NastavExtPisne( const char *E )
{
	strcpy(NajdiPExt(NajdiNazev(PisenP)),E);
}

Pisen *NajdiPisen( const char *N )
{
	Pisen **PP;
	PathName PN;
	char *LE;
	strcpy(PN,N);
	LE=strchr(NajdiNazev(PN),'.');
	if( LE ) *LE=0;
	for( PP=Pisne; PP<&Pisne[lenof(Pisne)]; PP++ )
	{
		Pisen *P=*PP;
		if( P )
		{
			if( !strcmp(P->NazevS,PN) ) return P;
		}
	}
	return NULL;
}

static Pisen *OtevriFPisen( const char *PisenW, Flag Nova )
{
	Pisen **PP;
	Pisen *P=NULL;
	PovolCinMenu(False);
	Mouse(HOURGLASS);
	P=NajdiPisen(PisenW);
	if( !P ) for( PP=Pisne; PP<&Pisne[lenof(Pisne)]; PP++ ) if( !*PP )
	{
		if( !Nova )
		{
			#if IMPORT_MID
			char *E=NajdiExt(NajdiNazev(PisenW));
			if( !strcmpi(E,"MID") ) P=MdfImport(PisenW);
			else
			#endif
			{
				P=CtiMelF(PisenW);
			}
		}
		else
		{
			Sekvence *S;
			P=NajdiPisen(PisenW);
			if( P ) ZavriFPisen(P);
			P=NovaPisen(PisenW);
			if( P ) S=NovaSekv(P,MainMel);
			if( !P || !S ) {Chyba(ERAM);if( P ) MazPisen(P);P=NULL;}
			if( P ) P->Zmena=False;
		}
		*PP=P;
		break;
	}
	/* v�ce ne� lenof(Pisne) nedovol�me otev��t */
	PovolCinMenu(True);
	return P;
}

static Flag DovolNazev( const char *W )
{
	W=NajdiNazev(W);
	if( strchr(W,'/') || strchr(W,'-') || strchr(W,':') ) return False;
	return True;
}

Pisen *OtevriPisen( Flag DefNazev )
{
	if( DefNazev )
	{
		FileName Def;
		static int Cnt=0;
		strcpy(Def,FreeString(DEFPISFS));
		sprintf(Def+4,"%04d",++Cnt);
		CestaNazev(PisenP,Def,PisenW);
		if( DovolNazev(PisenW) )
		{
			return OtevriFPisen(PisenW,True);
		}
	}
	else
	{
		VsePrekresli();
		if( EFSel(FreeString(OTESKLFS),PisenP,PisenF,PisenW) && DovolNazev(PisenW) )
		{
			VsePrekresli();
			return OtevriFPisen(PisenW,False);
		}
	}
	return NULL;
}

Flag ZavriFFPisen( Pisen *P )
{
	Flag ret=False;
	Pisen **PP;
	Mouse(HOURGLASS);
	PovolCinMenu(False);
	MazPisen(P);
	for( PP=Pisne; PP<&Pisne[lenof(Pisne)]; PP++ ) if( *PP==P )
	{
		*PP=NULL;
		ret=True;
		break;
	}
	PovolCinMenu(True);
	Plati( ret );
	return ret;
}

Flag ZavriFPisen( Pisen *P )
{
	Flag ret;
	int LHan;
	PovolCinMenu(False);
	if( P->Zmena )
	{
		int a;
		do
		{
			VsePrekresli();
			a=AlertRF(CLOSEA,1,NajdiNazev(P->NazevS));
			if( a==1 ) UlozFPisen(P,False);
			ef( a==3 ) {ret=False;goto Konec;}
		}
		while( a==1 && P->Zmena );
	}
	Mouse(HOURGLASS);
	for( LHan=NMaxOken-1; LHan>=0; LHan-- ) if( Okna[LHan] ) if( OknoJeSekv(LHan) )
	{ /* zavreni otevrenych oken */
		OknoInfo *OI=OInfa[LHan];
		if( SekvenceOI(OI)->Pis==P ) FZavri(LHan);
	}
	ret=ZavriFFPisen(P);
	Konec:
	PovolCinMenu(True);
	return ret;
}

Flag UlozFPisen( Pisen *P, Flag PtejSe )
{
	Flag ret;
	PovolCinMenu(False);
	if( PtejSe || !strcmp(NajdiNazev(P->NazevS),FreeString(DEFPISFS)) )
	{
		VsePrekresli();
		VyrobCesty(PisenP,PisenF,P->Cesta);
		if( !EFSel(FreeString(ULOSKLFS),PisenP,PisenF,PisenW) || !DovolNazev(PisenW) ) return False;
		NazevPisne(P,PisenW);
		NastavNazvy();
		VsePrekresli();
	}
	{
		#if IMPORT_MID
		char *E=NajdiExt(NajdiNazev(P->Cesta));
		if( !strcmpi(E,"MID") ) ret=MdfExport(P->Cesta,P);
		else
		#endif
		{
			ret=PisMelF(P->Cesta,P);
		}
	}
	if( ret ) P->Zmena=False;
	PovolCinMenu(True);
	return ret;
}
void UlozPisen( void )
{
	Pisen *P=VrchniPisen();
	if( !P ) return;
	UlozFPisen(P,False);
}
void UlozJako( void )
{
	Pisen *P=VrchniPisen();
	Pisen *S;
	if( !P ) return;
	UlozFPisen(P,True);
	S=NajdiPisen(P->Cesta);
	if( S && S!=P )
	{
		if( !ZavriFPisen(S) ) return;
		VsePrekresli();
	}
}

void VymazSekvenci( Sekvence *S )
{
	int LHan;
	for( LHan=NMaxOken-1; LHan>=0; LHan-- ) if( Okna[LHan] ) if( OknoJeSekv(LHan) )
	{ /* zavreni otevrenych oken */
		OknoInfo *OI=OInfa[LHan];
		if( SekvenceOI(OI)==S ) FZavri(LHan);
	}
	MazSekv(S);
}

void ZavriPisen( void )
{
	Pisen *P=VrchniPisen();
	if( P ) ZavriFPisen(P);
}

char *GetS( FILE *f )
{
	static char b[80];
	char *ret;
	ret=fgets(b,(int)sizeof(b),f);
	if( ret )
	{
		b[strlen(b)-1]=0;
		return b;
	}
	return ret;
}

static void Ignorovat( void ) {}

static const char CFGNam[]="COMPO.INF";

static Info ComInfo;

static const char PisNaz[]="Composition";
/*static const char PlyNaz[]="Output";*/

static void UklidSamply( void )
{
	FAktHNastr();
}

Err CtiCFG( void )
{
	Flag Mels=False;
	UklidPamet=UklidSamply;
	AktHraj=NULL;
	*PisenP=0;
	if( !CtiInfo(CFGNam,&ComInfo) )
	{
		AlertRF(IERRA,1,CFGNam);
		if( !NovInfo(CFGNam,&ComInfo) )
		{
			AlertRF(RAMEA,1);
			return ERR;
		}
	}
	{ /* at uz stare nebo nove info */
		int i;
		argc--,argv++;
		while( argc>0 )
		{
			const char *N=*argv;
			Pisen *P;
			VyrobCesty(PisenP,PisenF,N);
			P=OtevriFPisen(N,False);
			if( P )
			{
				Sekvence *S=NajdiSekv(P,MainMel,0);
				if( !S ) S=PrvniSekv(P);
				if( S )
				{
					SekvOtevri(P,S,NULL,-1,-1,NULL,NULL),VsePrekresli();
					Mels=True;
				}
				else ZavriFPisen(P);
			}
			argv++,argc--;
		}
		if( !Mels ) for( i=0; i<NMaxPisni && i<=NMaxOken; i++ )
		{
			const char *t=GetText(&ComInfo,PisNaz,i);
			PathName r;
			if( !t ) break;
			AbsCesta(r,t,CompoRoot);
			if( !strchr(r,'*') )
			{
				Pisen *P;
				VyrobCesty(PisenP,PisenF,r);
				if( !strcmpi(NajdiPExt(PisenF),".CPS") )
				{
					P=OtevriFPisen(r,False);
					if( P )
					{
						Sekvence *S=NajdiSekv(P,MainMel,0);
						if( !S ) S=PrvniSekv(P);
						if( S )
						{
							SekvOtevri(P,S,NULL,-1,-1,NULL,NULL),VsePrekresli();
							Mels=True;
						}
						else ZavriFPisen(P);
					}
				}
			}
			else /* jen cesta */
			{
				strlncpy(PisenP,r,sizeof(PisenP));
			}
		}
		{
			LLHraj *LH;
			Err ret;
			const char *r=GetText(&ComInfo,"Benchmark",0);
			if( r )
			{
				long BV=atol(r);
				F_BenchTune(BV);
			}
			LH=&AYBufFalc;
			AYBufFalc.CtiCFG(&ComInfo);
			ret=LH->ZacSNastr();
			if( ret<EOK ) Chyba(ret); /* neni zadne AktHraj */
			if( !Mels )
			{
				OtevriAbout();
			}
		}
		{
			const char *r=GetText(&ComInfo,"Compile",0);
			if( r )
			{
				AbsCesta(CocPath,r,CompoRoot);
			}
		}
	}
	if( !*PisenP )
	{
		GetPath(PisenP);
		EndChar(PisenP,'\\');
		strcat(PisenP,"*.CPS");
	}
	return EOK;
}

void UlozCFG( void )
{
	int p,Ind;
	Flag ret=True;
	Flag Pis=False;
	if( ret )
	{
		ret=AYBufFalc.UlozCFG(&ComInfo) /*&& Midi.UlozCFG(&ComInfo)*/;
	}
	if( ret ) for( Ind=p=0; p<NMaxPisni; p++ )
	{
		Pisen *P=Pisne[p];
		if( P )
		{
			PathName r;
			Pis=True;
			RelCesta(r,P->Cesta,CompoRoot);
			if( !SetText(&ComInfo,PisNaz,r,Ind++) ) {ret=False;break;}
		}
	}
	if( ret ) MazIPolOd(&ComInfo,PisNaz,Ind);
	if( ret && !Pis )
	{
		if( *PisenP )
		{
			PathName r;
			RelCesta(r,PisenP,CompoRoot);
			if( !SetText(&ComInfo,PisNaz,r,Ind++) ) {ret=False;}
		}
	}
	if( ret )
	{
		if( !SetText(&ComInfo,"Compile",CocPath,0) ) ret=False;
	}
	/*
	if( ret )
	{
		if( !SetText(&ComInfo,PlyNaz,AktHraj==&Midi ? "MIDI" : "Internal",0) ) ret=False;
	}
	*/
	if( !ret ) AlertRF(RAMEA,1);
	if( ComInfo.Zmena && !PisInfo(ComInfo.Soub,&ComInfo) )
	{
		AlertRF(WERRA,1,CFGNam);
	}
}

void KonCFG( void )
{
	FreeInfo(&ComInfo);
}
