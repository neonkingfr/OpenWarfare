#ifndef sndWaveOut_h__
#define sndWaveOut_h__

#include "GiG/stdfile.h"
typedef size_t SoundCallbackFunction(void *buffer, size_t bufferSize, int bufferFreq);

void StartSoundWaveOut(int freq, SoundCallbackFunction *callback);
void EndSoundWaveOut();


typedef int FileCallbackFunction( const char* sFileName, FileStd* pFileData );

void StartFileOut(FileCallbackFunction *callback);

#endif // sndWaveOut_h__
