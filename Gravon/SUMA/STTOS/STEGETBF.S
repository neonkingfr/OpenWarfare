text

export SupSTEGetPlayBuf

;void *SupSTEGetPlayBuf( void )
module SupSTEGetPlayBuf
	bclr #7,$ff8901

	moveq #0,d2
	move.b $ff890D,d0
	move.b $ff890B,d1
	move.b $ff8909,d2
	lsl.l #8,d2
	move.b d1,d2
	lsl.l #8,d2
	move.b d0,d2
	move.l d2,a0
	
	rts
endmod
