/* Atari system variables */
/* TOS 1.0 .. 4.04 */

#define phystop ( *(long *)0x42e )
#define _bootdev ( *(int *)0x446 )
#define sshiftmd ( *(char *)0x44c )
#define _v_bas_ad ( *(word **)0x44e )
#define screenpt ( *(word **)0x45e )
#define _frclock ( *(long *)0x466 )
#define swv_vec ( *(void **)0x46e )
#define nvbls ( *(int *)0x454 )
#define _vblqueue ( *(void ***)0x456 )
#define _hz_200 ( *(long *)0x4ba )
#define _sysbase ( *(SYSHDR **)0x4F2 ) /* SYSHDR v TOS.H */
#define scr_dump ( *(void **)0x502 )
#define dump_vec scr_dump
#define prv_lst ( *(long cdecl (**)( int c ) ) 0x50a )
#define prt_vec prv_lst
