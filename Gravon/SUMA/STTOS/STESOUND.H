#ifndef __STESOUND
#define __STESOUND

#define STE_FREQ_6  6258L
#define STE_FREQ_12 12517L
#define STE_FREQ_25 25033L
#define STE_FREQ_50 50066L


void STEStereo8( long STEFreq );
void STEsetbuffer( int *FPlayBuf, int *FEndPlayBuf );
void STEbuffoper( int Mode );

/* Supervisor - porty */

void SupSTEStereo8( long STEFreq );
void SupSTEsetbuffer( int *FPlayBuf, int *FEndPlayBuf );
void SupSTEbuffoper( int Mode );
void *SupSTEGetPlayBuf( void );

#endif