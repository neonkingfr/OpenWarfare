/* zaklad gemovskych programu */
/* definice nepovinnych procedur */
/* SUMA 10/1992 */

#include <macros.h>
#include <gem\interf.h>

Flag ZacProg( void ){return True;}
void KonProg( void ){}
void NastavMenu( void ){}
void GetTop( void ){}
Flag StiskTlac( Flag Dvoj, Flag Dlouhe, int MX, int MY )
{
	(void)Dvoj,(void)Dlouhe,(void)MX,(void)MY;
	return !Dlouhe;
}
Flag StiskTlac2( Flag Dvoj, Flag Dlouhe, int MX, int MY )
{
	(void)Dvoj,(void)Dlouhe,(void)MX,(void)MY;
	return !Dlouhe;
}

Flag PustTlac( int MX, int MY ){(void)MX,(void)MY;return True;}
Flag PustTlac2( int MX, int MY ){(void)MX,(void)MY;return True;}

void DoZnak( int Asc, int Scan, int Shf, Flag Buf )
{
	(void)Asc,(void)Scan,(void)Shf,(void)Buf;
}
void DoZprava( int *M ){(void)M;}
void DoCas( void ){}
void VsePrekresli(){}
int Kursor( Flag Inside, Flag Obrys ){(void)Inside,(void)Obrys;return ARROW;}

void ZmenNormu( int NTab, int OTab ){(void)NTab,(void)OTab;}
void DebugError( int Line, const char *File )
{
	(void)Line,(void)File;
}

int main( int argc, const char *argv[] )
{
	(void)argc,(void)argv;
	return MenuMain();
}
