/* zaklad gemovskych programu */
/* SUMA 10/1992-7/1993 */

#include <macros.h>
#include <stdlib.h>
#include <string.h>
#include <tos.h>
#include <ctype.h>
#include <stdio.h>
#include <aes.h>
#include <stdarg.h>
#include <stdc\fileutil.h>
#ifndef __MSDOS__
#include <sttos\cookie.h>
#include <sttos\cszvaz.h>
#endif

#include <gem\interf.h>

#ifdef __MSDOS__
	#define __WSS 0
	#define MIDI 0
#else
	#define __WSS 1
	#define MIDI 1
#endif

/* workstations */

int ApId;
Flag NechciBut2=False;

#if __WSS

MFDB screen;

Flag FastInit( void )
{
	int r,snp,sw,sh;
	r=Getrez();
	switch(r)
	{
		case 0: snp=4;sw=320;sh=200;break;
		case 1: snp=2;sw=640;sh=200;break;
		case 2: snp=1;sw=640;sh=400;break;
		default:return False;
	}
	screen.fd_addr = Logbase();
	screen.fd_nplanes = snp;
	screen.fd_w = sw;
	screen.fd_h = sh;
	screen.fd_wdwidth = snp*(sw+15)/16;
	screen.fd_stand = 0;
	return True;
}

#if !_CLIPFAST

int OpenGraf( void )
{
	int i,handle;
	witype wi;
	wotype wo;
	int d;
	handle=graf_handle(&d,&d,&d,&d);
	for( i=0; i<10; i++ ) wi[i]=1;
	wi[7]=0;
	wi[10]=2;
	v_opnvwk(wi,&handle,wo);
	if( handle==0 ) return 0;
	screen.fd_addr = NULL;
	screen.fd_w = wo[0]+1;
	screen.fd_h = wo[1]+1;
	vq_extnd(handle,1,wo);
	screen.fd_nplanes = wo[4];
	screen.fd_wdwidth = (screen.fd_w+15)/16; /* dle TUTORIAL */
	screen.fd_stand = 0;
	return handle;
}

void CloseGraf( int handle )
{
	v_clsvwk(handle);
}

#endif

#endif

/* windows */
void wtocr(ClipRect *CR,const WindRect *W)
{
	CR->x1= W->x ;
	CR->y1= W->y ;
	CR->x2= W->x + W->w -1;
	CR->y2= W->y + W->h -1;
}
void crtow(WindRect *W,const ClipRect *CR)
{
	W->x = CR->x1;
	W->y = CR->y1;
	W->w = CR->x2 - CR->x1 +1;
	W->h = CR->y2 - CR->y1 +1;
}
void GetWind( int wind, int What, WindRect *W )
{
	if( wind_get(wind,What,&W->x,&W->y,&W->w,&W->h)==0 )
	{
		W->x=W->y=W->w=W->h=0; /* okno jiz zaniklo */
	}
}
void SetWind( int wind, int What, WindRect *W )
{
	PlatiProc( wind_set(wind,What,W->x,W->y,W->w,W->h),!=0 );
}
void WindCalc( int Calc, int wind, const WindRect *In, WindRect *Out )
{
	PlatiProc( wind_calc(Calc,wind,In->x,In->y,In->w,In->h,&Out->x,&Out->y,&Out->w,&Out->h), !=0 );
}
void Redraw( int wind, RdrProc Redr, const WindRect *RW )
{
	WindRect W,F;
	ClipRect DC,RC;
	WindRect rw;
	GetWind(0,WF_FULLXYWH,&rw);
	if( RW ) WindSect(&rw,RW);
	wtocr(&RC,&rw);
	BegUpdate(); /* nesmim dovolit zmenu tvaru */
	GetWind(wind,WF_WORKXYWH,&F);
	for( GetWind(wind,WF_FIRSTXYWH,&W); W.h+W.w>0; GetWind(wind,WF_NEXTXYWH,&W) )
	{
		wtocr(&DC,&W);
		InterSect(&DC,&RC);
		if( ClipOK(&DC) )
		{
			crtow(&W,&DC);
			W.x= DC.x1-F.x;
			W.y= DC.y1-F.y;
			W.w= DC.x2-DC.x1+1;
			W.h= DC.y2-DC.y1+1;
			Redr(&DC,&W);
		}
	}
	EndUpdate();
}

#ifndef __MSDOS__
/* v MS DOSu bez oken - viz NOWNFORM.C */

void SendDraw( int Ap, int FHan, const WindRect *W )
{
	int M[8];
	M[0]=WM_REDRAW;
	M[1]=ApId;
	M[2]=(int)sizeof(M);
	M[3]=FHan;
	*(WindRect *)&M[4]=*W;
	appl_write(Ap,(int)sizeof(M),M);
}

void SendODraw( int FHan, OBJECT *D, int I )
{
	WindRect W;
	ObjcRRect(D,I,&W);
	SendDraw(ApId,FHan,&W);
}
void SendDeskDraw( const WindRect *S )
{
	WindRect W;
	BegUpdate();
	for( GetWind(0,WF_FIRSTXYWH,&W); W.h+W.w>0; GetWind(0,WF_NEXTXYWH,&W) )
	{
		WindSect(&W,S);
		if( W.w>0 && W.h>0 )
		{
			form_dial(FMD_START,0,0,0,0,W.x,W.y,W.w,W.h);
			form_dial(FMD_FINISH,0,0,0,0,W.x,W.y,W.w,W.h);
		}
	}
	EndUpdate();
}
#endif

/* mouse */

static int TvarKursoru=-1;
static MFORM *UsrTvarKursoru;
static MFORM *UsrTvarSet;

static void MouseS( void )
{
	BegUpdate();
	graf_mouse(TvarKursoru,UsrTvarKursoru);
	EndUpdate();
}

int MouseIs( void ) {return TvarKursoru;}
MFORM *MUsrIs( void ) {return UsrTvarKursoru;}

static int MouseSem=1;

static Flag MysSkryta=False;
void MouseShow( void )
{ /* n�kde chce my� */
	if( MysSkryta ) Mouse(On),MysSkryta=False;
}

void Mouse( int Com )
{
	if( Com==M_OFF ) {if( --MouseSem!=0 ) return;}
	ef( Com==M_ON ) {if( MouseSem++!=0 ) return;}
	if( Com!=TvarKursoru || UsrTvarKursoru!=UsrTvarSet )
	{
		if( Com!=M_OFF && Com!=M_ON ) MouseShow();
		BegUpdate();
		graf_mouse(Com,UsrTvarKursoru);
		EndUpdate();
		UsrTvarSet=UsrTvarKursoru;
		if( Com!=Off && Com!=On ) TvarKursoru=Com;
	}
}

static int MysSkrytaNaMX,MysSkrytaNaMY;
void MouseHide( void )
{
	/* my� n�s nezaj�m� */
	if( !MysSkryta )
	{
		MysSkryta=True;
		MysXY(&MysSkrytaNaMX,&MysSkrytaNaMY);
		Mouse(Off);
	}
}
void MouseHideInRect( const WindRect *WR )
{
	/* my� n�s nezaj�m� */
	if( !MysSkryta )
	{
		int MX,MY;
		MysXY(&MX,&MY);
		if( MX>=WR->x && MX<=WR->x+WR->w && MY>=WR->y && MY<=WR->y+WR->h )
		{
			MysSkrytaNaMX=MX;
			MysSkrytaNaMY=MY;
			MysSkryta=True;
			Mouse(Off);
		}
	}
}

void MouseUsr( MFORM *Obr ) {UsrTvarKursoru=Obr;}

static int UpdateSem=1;

void BegUpdate( void )
{
	if( --UpdateSem!=0 ) return;
	wind_update(BEG_UPDATE);
}
void EndUpdate( void )
{
	if( UpdateSem++!=0 ) return;
	wind_update(END_UPDATE);
}

void MysXY( int *X, int *Y )
{
	int mkm,mkk;
	graf_mkstate(X,Y,&mkm,&mkk);
}

void Wait( long ms )
{
	evnt_timer((int)ms,(int)(ms>>16));
}

int TestEvntKeyb( void )
{
	int M[8];
	int D,KR;
	int Jev=evnt_multi(MU_TIMER|MU_KEYBD,0,0,0,0,0,0,0,0,0,0,0,0,0,M,0,0,&D,&D,&D,&D,&KR,&D);
	if( Jev&MU_KEYBD ) return KR;
	return 0;
}

/* object */

void PovolMenu( OBJECT *M, int I, Flag F )
{
	if( PovoleneMenu(M,I)!=F ) menu_ienable(M,I,F);
}
void Set01( OBJECT *O, int Wh, Flag Y )
{
	if( Y ) Set1(O,Wh);
	else Set0(O,Wh);
}

void ObjcDraw( OBJECT *Form, int Start, const WindRect *B )
{
	BegUpdate(); /* up NEW */
	PlatiProc( objc_draw(Form,Start,MAX_DEPTH,B->x,B->y,B->w,B->h), !=0 );
	EndUpdate(); /* up NEW */
}

static int FrameSize( const OBJECT *O )
{
	int w;
	switch( O->ob_type )
	{
		case G_BOX:
		case G_IBOX:
		case G_BOXCHAR:
			w=O->ob_spec.obspec.framesize;
			break;
		case G_BOXTEXT:
		case G_FBOXTEXT:
		{
			TEDINFO *T=O->ob_spec.tedinfo;
			w=T->te_thickness;
			break;
		}
		case G_TEXT:
		case G_FTEXT:
		case G_TITLE:
			w=0;
			break;
		/*
		case G_IMAGE:
		case G_USERDEF:
		case G_BUTTON:
		case G_STRING:
		case G_ICON:
		*/
		default: w=0; break;
	}
	return w;
}

void ObjcRRect( OBJECT *F, int I, WindRect *W )
{
	OBJECT *O;
	int w;
	ObjcRect(F,I,W);
	O=&F[I];
	w=FrameSize(O);
	if( O->ob_state&OUTLINED ) w=-3;
	if( w>0 ) w=0;
	W->x+=w;
	W->y+=w;
	W->w-=w+w;
	W->h-=w+w;
}

void ObjcIRect( OBJECT *F, int I, WindRect *W )
{
	OBJECT *O;
	int w;
	ObjcRect(F,I,W);
	O=&F[I];
	w=FrameSize(O);
	if( w<0 ) w=0;
	W->x-=w;
	W->y-=w;
	W->w+=w+w;
	W->h+=w+w;
}

void ObjcRect( OBJECT *F, int I, WindRect *W )
{
	objc_offset(F,I,&W->x,&W->y);
	W->w=F[I].ob_width;
	W->h=F[I].ob_height;
}

static OBJECT *ORed;
static int Wind,Str,NS,OS;
static void RdrObj( ClipRect *A, WindRect *R )
{
	crtow(R,A);
	BegUpdate(); /* up NEW */
	PlatiProc( objc_draw(ORed,Str,MAX_DEPTH,R->x,R->y,R->w,R->h), !=0 );
	EndUpdate(); /* up NEW */
}
static void ChgObj( ClipRect *A, WindRect *R )
{
	crtow(R,A);
	ORed[Str].ob_state=OS;
	BegUpdate(); /* up NEW */
	PlatiProc( objc_change(ORed,Str,0,R->x,R->y,R->w,R->h,NS,1), !=0 );
	EndUpdate(); /* up NEW */
}
void ObjcRedraw( OBJECT *O, int Start, int W, const WindRect *RW )
{
	ORed=O;Wind=W;Str=Start;
	Redraw(W,RdrObj,RW);
}
void ObjcChange( OBJECT *O, int Start, int W, int NewState )
{
	if( O[Start].ob_state!=NewState )
	{ 
		ORed=O;Wind=W;Str=Start;NS=NewState;
		OS=O[Start].ob_state;
		Redraw(W,ChgObj,NULL);
		O[Start].ob_state=NewState;
	}
}

/* form */
const char *FreeString( int I )
{
	char *Text;
	PlatiProc( rsrc_gaddr(R_STRING,I,(OBJECT **)&Text), !=0);
	return Text;
}
OBJECT *RscTree( int F )
{
	OBJECT *O;
	PlatiProc( rsrc_gaddr(R_TREE,F,&O),!=0 );
	return O;
}

/* operace s nekt. objekty */
void RedrawI( OBJECT *Form, int i )
{
	WindRect O;
	O.x=Form->ob_x;
	O.y=Form->ob_y;
	O.w=Form->ob_width;
	O.h=Form->ob_height;
	ObjcDraw(Form,i,&O);
}

void SetStrTed( TEDINFO *T, const char *Text )
{
	strlncpy(T->te_ptext,Text,T->te_txtlen);
}

long IntTed( const TEDINFO *ted )
{
	char *s=ted->te_ptext;
	if( *s )
	{
		char *p;
		long i=strtol(s,&p,10);
		return i;
	}
	else return 0;
}
void SetIntTed( TEDINFO *ted, long i )
{
	char b[16];
	sprintf(b,"%ld",i);
	SetStrTed(ted,b);
}

void IncTed( OBJECT *F, int I, long HMez )
{
	TEDINFO *Ted=F[I].ob_spec.tedinfo;
	long T=IntTed(Ted);
	if( T<HMez )
	{
		T++;
		SetIntTed(Ted,T);
		RedrawI(F,I);
	}
}
void DecTed( OBJECT *F, int I, long DMez )
{
	TEDINFO *Ted=F[I].ob_spec.tedinfo;
	long T=IntTed(Ted);
	if( T>DMez )
	{
		T--;
		SetIntTed(Ted,T);
		RedrawI(F,I);
	}
}
void AddTed( OBJECT *F, int I, long Add, long DMez, long HMez )
{
	TEDINFO *Ted=F[I].ob_spec.tedinfo;
	long T=IntTed(Ted);
	if( T>DMez && Add<0 || T<HMez && Add>0 )
	{
		T+=Add;
		if( T<DMez ) T=DMez;
		ef( T>HMez ) T=HMez;
		SetIntTed(Ted,T);
		RedrawI(F,I);
	}
}

int MysSlide( OBJECT *F, int Bkg, int Slid, Flag Vertical )
{
	int C,c,Dum,Rng;
	if( Vertical )
	{
		MysXY(&Dum,&C);
		objc_offset(F,Bkg,&Dum,&c);
		Rng=F[Bkg].ob_height-F[Slid].ob_height;
	}
	else
	{
		MysXY(&C,&Dum);
		objc_offset(F,Bkg,&c,&Dum);
		Rng=F[Bkg].ob_width-F[Slid].ob_width;
	}
	if( Rng==0 ) return 0;
	C-=c;
	if( C<0 ) C=0;
	if( C>Rng ) C=Rng;
	return (int)(C*1000L/Rng);
}
int PosSlide( OBJECT *F, int Bkg, int Slid, Flag Vertical )
{
	int C,c,Dum,Rng;
	if( Vertical )
	{
		objc_offset(F,Bkg,&Dum,&C);
		objc_offset(F,Slid,&Dum,&c);
		Rng=F[Bkg].ob_height-F[Slid].ob_height;
	}
	else
	{
		objc_offset(F,Bkg,&C,&Dum);
		objc_offset(F,Slid,&c,&Dum);
		Rng=F[Bkg].ob_width-F[Slid].ob_width;
	}
	if( Rng==0 ) return 0;
	return (int)((c-C)*1000L/Rng);
}

void SetSlideBox( OBJECT *F, int Bkg, int Slid, Flag Vertical, int Val, Flag Draw )
{
	int Rng;
	int OC;
	if( Vertical ) Rng=F[Bkg].ob_height-F[Slid].ob_height;
	else Rng=F[Bkg].ob_width-F[Slid].ob_width;
	Rng=(int)(Val*(long)Rng/1000);
	if( Vertical ) OC=F[Slid].ob_y,F[Slid].ob_y=Rng;
	else OC=F[Slid].ob_x,F[Slid].ob_x=Rng;
	if( Draw && OC!=Rng )
	{
		RedrawI(F,Bkg);
	}
}

void RedrawS( OBJECT *F, int B, int I )
{
	WindRect O;
	objc_offset(F,I,&O.x,&O.y);
	O.w=F[I].ob_width;
	O.h=F[I].ob_height;
	ObjcDraw(F,B,&O);
}

int AlertRF( int OI, int But, ... )
{
	char a[256];
	int ret;
	va_list v;
	va_start(v,But);
	vsprintf(a,FreeString(OI),v);
	ret=form_alert(But,a);
	MouseS();
	va_end(v);
	return ret;
}

#ifndef __MSDOS__
const void Transf( char *Z )
{
	for( ;*Z; Z++ )
	{
		switch( *Z )
		{
			case '[': *Z='(';break;
			case ']': *Z=')';break;
			case '|': *Z='!';break;
		}
	}
}
#endif

int AlertF( const char *Text, int But, ... )
{
	char a[256];
	int ret;
	va_list v;
	va_start(v,But);
	vsprintf(a,Text,v);
	ret=form_alert(But,a);
	MouseS();
	va_end(v);
	return ret;
}

void MessageF( char *Format, ... )
{
	char a[256];
	char b[256]="[1][";
	va_list v;
	va_start(v,Format);
	strcat(b,Format);
	strcat(b,"][  OK  ]");
	vsprintf(a,b,v);
	form_alert(1,a);
	va_end(v);
	MouseS();
}

void FormStart( const OBJECT *Form, const WindRect *W )
{
	form_dial(FMD_START,0,0,0,0,W->x,W->y,W->w,W->h);
	(void)Form;
}
void FormFinish( const OBJECT *Form, const WindRect *W )
{
	form_dial(FMD_FINISH,0,0,0,0,W->x,W->y,W->w,W->h);
	(void)Form;
}

void FormCenter( OBJECT *Form, WindRect *W )
{
	form_center(Form,&W->x,&W->y,&W->w,&W->h);
}

void FormAt( OBJECT *F, WindRect *W, int x, int y )
{
	int d,max;
	WindRect D;
	ObjcRRect(F,0,W);
	GetWind(0,WF_FULLXYWH,&D);
	max=D.x+D.w-W->w;
	if( x>max ) x=max;if( x<D.x ) x=D.x;
	d=x-W->x;
	W->x=x;F->ob_x+=d;
	max=D.y+D.h-W->h;
	if( y<D.y+8 ) y=D.y+8;if( y>max ) y=max;
	d=y-W->y;
	W->y=y;F->ob_y+=d;
}
void FormCAt( OBJECT *F, WindRect *W, int x, int y )
{
	int d,max;
	WindRect D;
	ObjcRRect(F,0,W);
	GetWind(0,WF_FULLXYWH,&D);
	max=D.x+D.w-W->w;
	x-=W->w/2;
	y-=W->h/2;
	if( x>max ) x=max;if( x<D.x ) x=D.x;
	d=x-W->x;
	W->x=x;F->ob_x+=d;
	max=D.y+D.h-W->h;
	if( y<D.y+8 ) y=D.y+8;if( y>max ) y=max;
	d=y-W->y;
	W->y=y;F->ob_y+=d;
}

void FormAtMouse( OBJECT *F, WindRect *W )
{
	int x,y;
	MysXY(&x,&y);
	FormCAt(F,W,x,y);
}

void FormAtRTMouse( OBJECT *F, WindRect *W )
{
	int x,y;
	MysXY(&x,&y);
	FormAt(F,W,x-32,y-8);
}

Flag Unselect( OBJECT *Bt, Flag Dvoj, int Asc )
{
	(void)Dvoj,(void)Asc;
	Set0(Bt,SELECTED);
	return True;
}
Flag NoButProc( OBJECT *Bt, Flag Dvoj, int Asc )
{
	(void)Bt,(void)Dvoj,(void)Asc;
	return False;
}

#ifndef __MSDOS__
void GrowBox( const WindRect *Z, const WindRect *D )
{
	WindRect F;
	WindRect z=*Z,d=*D;
	GetWind(0,WF_FULLXYWH,&F);
	WindSect(&z,&F),WindSect(&d,&F);
	graf_growbox(z.x,z.y,z.w,z.h,d.x,d.y,d.w,d.h);
}

void ShrinkBox( const WindRect *D, const WindRect *Z )
{
	WindRect F;
	WindRect z=*Z,d=*D;
	GetWind(0,WF_FULLXYWH,&F);
	WindSect(&z,&F),WindSect(&d,&F);
	graf_shrinkbox(d.x,d.y,d.w,d.h,z.x,z.y,z.w,z.h);
}
#endif

enum {SM=K_LSHIFT|K_RSHIFT};
#define EquShift(s,S) ( (s)==(S) || ( (((s)^(S))&~SM)==0 && !((s)&SM)==!((S)&SM) ) )

int AltKey( int s ) /* zjisti, jakemu pismenu prislusi dany scan */
{
	int a;
	#ifndef __MSDOS__
	void *SSP=(void *)Super(NULL);
	KEYTAB *KT=(KEYTAB *)Keytbl((char *)ERRP,(char *)ERRP,(char *)ERRP);
	if( !KT ) a=0;
	else a=((char *)KT->capslock)[s];
	Super(SSP);
	#else
		(void)s;
		a=0;
	#endif
	return a;
}

Flag TestZrychl( const KeyInfo *KI, int s, int a, int h )
{
	if( KI->Shift==-1 ) return
	(
		( toupper(a)==toupper(KI->Asci) || KI->Asci<0 ) &&
		( s==KI->Scan || KI->Scan<0 )
	);
	if( (KI->Shift&K_ALT) && KI->Asci>0 && a==0 ) a=AltKey(s);
	if( (KI->Shift&K_CTRL) && isalpha(KI->Asci) ) return (KI->Asci&0x1f)==a && EquShift(KI->Shift,h);
	return 
	(
		( a==KI->Asci || KI->Asci<0 ) &&
		( s==KI->Scan || KI->Scan<0 ) &&
		EquShift(h,KI->Shift)
	);
}

ButProc *NajdiProc( const Button *Bt, int Dial )
{
	if( Bt )
	{
		const Button *I;
		for( I=Bt; I->Proc && I->Num!=Dial; I++ );
		if( I->Proc ) return I->Proc;
		for( I=Bt; I->Proc && I->Num!=-1; I++ );
		if( I->Proc && Dial!=SPECBUT ) return I->Proc;
	}
	if( Dial==SPECBUT ) return NoButProc;
	return Unselect;
}

/* Button konci {NULL,?}, def. obsluha je {Proc,-1} (nepovinna) */

int Form( OBJECT *Form, int Start, const Button *Bt )
{ /* obsluha s vyuzitim form_do - jednoducha, nema zatim v AESu klavesnicove rizeni */
	OBJECT *AB;
	WindRect B;
	Flag Konec;
	int Mys=MouseIs();
	int Dial;
	FormCenter(Form,&B);
	ObjcRRect(Form,0,&B);
	PovolCinMenu(False);
	form_dial(FMD_START,0,0,0,0,B.x,B.y,B.w,B.h);
	ObjcDraw(Form,0,&B);
	Mouse(ARROW);
	while( Buttons()&1 );
	do
	{
		Flag D;
		Mouse(ARROW);
		Dial=form_do(Form,Start);
		D=(Dial&0x8000)!=0;
		Dial&=0x7fff;
		AB=&Form[Dial];
		Konec=NajdiProc(Bt,Dial)(AB,D,0);
	}
	while( !Konec );
	form_dial(FMD_FINISH,0,0,0,0,B.x,B.y,B.w,B.h);
	PovolCinMenu(True);
	Mouse(Mys);
	return Dial;
}

void DesktopDraw( int I )
{
	WindRect F;
	ObjcRect(Desktop,I,&F);
	SendDeskDraw(&F);
}

void NastavDesktop( OBJECT *NDesk )
{
	WindRect W;
	GetWind(0,WF_FULLXYWH,&W);
	NDesk->ob_x=W.x;NDesk->ob_y=W.y;
	NDesk->ob_width=W.w;NDesk->ob_height=W.h;
	BegUpdate(); /* up NEW */
	wind_set(0,WF_NEWDESK,NDesk,0,0);
	EndUpdate(); /* up NEW */
	SendDeskDraw(&W);
}
void VratDesktop( void )
{
	NastavDesktop(Desktop);
}

/* window Buffers */

void ClipInPat( ClipRect *C, const MFDB *P )
{
	C->x1=max(C->x1,0);
	C->y1=max(C->y1,0);
	C->x2=min(C->x2,P->fd_w-1);
	C->y2=min(C->y2,P->fd_h-1);
}

void WindSect( WindRect *W, const WindRect *D )
{
	int d;
	d=D->x-W->x;
	if( d>0 ) W->x+=d,W->w-=d;
	d=D->y-W->y;
	if( d>0 ) W->y+=d,W->h-=d;
	d=D->w+D->x-(W->w+W->x);
	if( d<0 ) W->w+=d;
	d=D->h+D->y-(W->h+W->y);
	if( d<0 ) W->h+=d;
}

void InterSect( ClipRect *C, const ClipRect *L )
{
	C->x1=max(C->x1,L->x1);
	C->y1=max(C->y1,L->y1);
	C->x2=min(C->x2,L->x2);
	C->y2=min(C->y2,L->y2);
}
void EqWH( ClipRect *C,ClipRect *D )
{
	WindRect V,W;
	crtow(&V,C);
	crtow(&W,D);
	V.w=W.w= min(W.w,V.w);
	V.h=W.h= min(W.h,V.h);
	wtocr(C,&V);
	wtocr(D,&W);
}
int ClipOK( const ClipRect *C )
{
	return C->x1 <= C->x2 && C->y1 <= C->y2;
}

#if __WSS

void FClipCpyfm( MFDB P[2], ClipRect C[2] )
{
	int GH,d;
	GH=graf_handle(&d,&d,&d,&d);
	BegUpdate(); /* up NEW */
	Mouse(Off);
	vro_cpyfm(GH,S_ONLY,&C[0].x1,&P[0],&P[1]);
	Mouse(On);
	EndUpdate(); /* up NEW */
}

void ClipFast( MFDB P[2], ClipRect C[2] )
{
	ClipInPat(&C[0],&P[0]);
	ClipInPat(&C[1],&P[1]);
	EqWH(&C[0],&C[1]);
	if( ClipOK(&C[0]) && ClipOK(&C[1]) )
	{
		FClipCpyfm(P,C);
	}
}
#if !_CLIPFAST
void ClipCopy( int ws, MFDB P[2], ClipRect C[2] )
{
	ClipInPat(&C[0],&P[0]);
	ClipInPat(&C[1],&P[1]);
	EqWH(&C[0],&C[1]);
	if( ClipOK(&C[0]) && ClipOK(&C[1]) )
	{
		static int ci[2]={1,0};
		BegUpdate(); /* up NEW */
		Mouse(Off);
		if( P[0].fd_nplanes==P[1].fd_nplanes ) vro_cpyfm(ws,S_ONLY,&C[0].x1,&P[0],&P[1]);
		else vrt_cpyfm(ws,MD_REPLACE,&C[0].x1,&P[0],&P[1],ci);
		Mouse(On);
		EndUpdate(); /* up NEW */
	}
}
#endif

#endif

/* file selector */

Flag FSel( PathName FPC, FileName FNC, PathName FW )
{
	int ret;
	fsel_input(FPC,FNC,&ret);
	MouseS();
	if( ret )
	{
		if( FNC[0]==0 || FNC[0]=='.' ) ret=False;
		else ret=CestaNazev(FPC,FNC,FW);
	}
	return ret;
}

#ifndef __MSDOS__
Flag EFSel( const char *Lab, PathName FPC, FileName FNC, PathName FW )
{
	int ret;
	if( VTos()>=0x104 ) fsel_exinput(FPC,FNC,&ret,(char *)Lab);
	else fsel_input(FPC,FNC,&ret);
	MouseS();
	if( ret )
	{
		if( FNC[0]==0 || FNC[0]=='.' ) ret=False;
		else ret=CestaNazev(FPC,FNC,FW);
	}
	return ret;
}
Flag EPSel( const char *Lab, PathName FPC )
{
	int ret;
	FileName FNC;
	*FNC=0;
	if( VTos()>=0x104 ) fsel_exinput(FPC,FNC,&ret,(char *)Lab);
	else fsel_input(FPC,FNC,&ret);
	MouseS();
	if( ret ) ret=True;
	return ret;
}

#endif

/* telo zakladniho programu */

Flag Konec;
OBJECT *Menu,*Desktop=NULL;
WindRect VrchOkno,VrchObrys; /* pro zmeny tvaru kursoru */
Flag JeOkno; /* zda je vrchni okno nase */
long Casovac=-1;

static void DoMenuF( int T, int I )
{
	Mouse(HOURGLASS);
	NastavMenu();
	if( !Test(&Menu[I],DISABLED) && SmiSeMenu() )
	{
		if( T>0 )
		{
			if( Test(&Menu[T],DISABLED) ) return;
			BegUpdate(); /* up NEW */
			menu_tnormal(Menu,T,False);
			EndUpdate(); /* up NEW */
		}
		DoMenu(I);
	}
	if( T>0 )
	{
		BegUpdate(); /* up NEW */
		menu_tnormal(Menu,T,True);
		EndUpdate(); /* up NEW */
	}
}


static void DoKlav( int MKS, int MKR )
{
	Flag Buff=False;
	int MKRA=-1,MKSA=-1;
	for(;;)
	{
		KlavInfo *KI;
		int Jev,D;
		int M[8];
		int S=MKR>>8,A=MKR&0xff;
		for( KI=MenuTab; ; KI++ )
		{
			if( KI->Item<=0 ) {DoZnak(A,S,MKS,Buff);break;}
			if( TestZrychl(&KI->KI,S,A,MKS) ) {if( !Buff ) DoMenuF(KI->Title,KI->Item);break;}
		}
		if( Konec ) return;
		if( Buff ) MKS=MKSA,MKR=MKRA;
		else
		{
			Jev=evnt_multi(MU_TIMER|MU_KEYBD,0,0,0,0,0,0,0,0,0,0,0,0,0,M,0,0,&D,&D,&D,&MKSA,&MKRA,&D);
			if( Jev&MU_KEYBD ) MKS=MKSA,MKR=MKRA;
			else break;
		}
		Jev=evnt_multi(MU_TIMER|MU_KEYBD,0,0,0,0,0,0,0,0,0,0,0,0,0,M,0,0,&D,&D,&D,&MKSA,&MKRA,&D);
		Buff=(Jev&MU_KEYBD)!=0;
	}
}

static void DoZprav( int *M )
{
	switch( M[0] )
	{
		case MN_SELECTED: DoMenuF(M[3],M[4]); break;
		#ifndef __MSDOS__
		case MS_CS: if( M[3]==CS_CHANGE ) ZmenNormu(M[4],M[5]);break;
		#endif
		default: DoZprava(M);break;
	}
}

static void Cinnost( void )
{
	int M[8],Jev;
	Flag Inside=False,Obrys=False,Pusteno1=True,Pusteno2=True;
	int KurTvar=ARROW;
	int MX,MY,MB,MKS,MKR,MBR;
	Konec=False;
	while( !Konec )
	{
		if( !JeOkno ) Inside=Obrys=False;
		GetTop();
		NastavMenu();
		KurTvar=Kursor(Inside,Obrys);
		Mouse(KurTvar);
		Jev=JeOkno ? MU_KEYBD|MU_MIDI|MU_MESAG|MU_BUTTON|MU_BUTTO2|MU_M1|MU_M2 : MU_KEYBD|MU_MESAG|MU_BUTTON|MU_BUTTO2;
		if( NechciBut2 ) Jev&=~MU_BUTTO2; /* leckdo nepot�ebuje prav� tla��tko */
		if( Casovac>=0 ) Jev|=MU_TIMER;
		if( !MysSkryta )
		{
			Jev=DvojEvntMulti
			(
				Jev,
				2,Pusteno1,
				2,Pusteno2,
				Inside,VrchOkno.x,VrchOkno.y,VrchOkno.w,VrchOkno.h,
				Obrys,VrchObrys.x,VrchObrys.y,VrchObrys.w,VrchObrys.h,
				M,
				(int)Casovac,(int)(Casovac>>16), /* lo,hi */
				&MX,&MY,&MB,&MKS,&MKR,&MBR
			);
		}
		else
		{
			WindRect OM;
			OM.x=MysSkrytaNaMX;
			OM.y=MysSkrytaNaMY;
			OM.w=OM.h=1;
			Jev=DvojEvntMulti
			(
				(Jev&~MU_M2)|MU_M1,
				2,Pusteno1,
				2,Pusteno2,
				True,OM.x,OM.y,OM.w,OM.h,
				True,OM.x,OM.y,OM.w,OM.h,
				M,
				(int)Casovac,(int)(Casovac>>16), /* lo,hi */
				&MX,&MY,&MB,&MKS,&MKR,&MBR
			);
			if( Jev&MU_M1 ) Jev&=~MU_M1,MouseShow();
		}
		GetTop();
		if( Jev&MU_M2 )
		{
			Obrys^=True;
			KurTvar=Kursor(Inside,Obrys);
			Mouse(KurTvar);
		}
		if( Jev&MU_M1 )
		{
			Inside^=True;
			KurTvar=Kursor(Inside,Obrys);
			Mouse(KurTvar);
		}
		if( Jev&MU_MESAG  ) DoZprav(M);
		if( Jev&MU_KEYBD  ) DoKlav(MKS,MKR);
		if( Jev&(MU_BUTTON|MU_BUTTO2) )
		{
			if( Jev&MU_BUTTON )
			{
				if( Pusteno1 )
				{
					int MXN,MYN,Dum,MJ; /* muzeme rozlisit i tazeni! */
					MJ=evnt_multi /* zajima nas jen ten jeden button */
					(
						MU_BUTTON|MU_TIMER,
						1,1,0, /* cekame na pusteni */
						0,0,0,0,0,0,0,0,0,0,
						M,150,0, /* lo,hi */
						&MXN,&MYN,&Dum,&Dum,&Dum,&Dum
					);
					if( MJ&MU_BUTTON ) Pusteno1=StiskTlac(MBR&2,False,MX,MY);
					else Pusteno1=StiskTlac(MBR&2,True,MX,MY);
				}
				else Pusteno1=PustTlac(MX,MY);
			}
			if( Jev&MU_BUTTO2 )
			{
				if( Pusteno2 ) Pusteno2=StiskTlac2(MBR&8,True,MX,MY); /* nerozlisujeme dlouhe a kratke - jakoby vzdy dlouhy */
				else Pusteno2=PustTlac2(MX,MY);
			}
		}
		if( Jev&MU_TIMER ) DoCas();
	}
}

/*
-------+----------------------------------------------------
_IDT   | International Date/Time Format
       | Time Format                                  BIT 12
       | 0 - AM/PM, 1 - 24 hours --------------------------+
       | Date Format                                 BIT 9 8
       | 00 - MMDDYY ------------------------------------+-+
       | 01 - DDMMYY ------------------------------------+-+
       | 10 - YYMMDD ------------------------------------+-+
       | 11 - YYDDMM ------------------------------------+-'
       | Separator for date                          BIT 7-0
       | ASCII Value (i.e. "." or "/") --------------------'
-------+----------------------------------------------------
*/

void AutoDatum( char *Buf, const char *N, const char *dat )
{
	char dt[32];
	long val;
	char *m1,*m2;
	char *n;
	char SepS[2]=".";
	int df;
	strcpy(dt,dat);
	m1=strchr(dt,' ');
	Plati( m1 );
	*m1++=0;
	if( *m1=='0' ) m1++;
	m2=strchr(m1,' ');
	Plati( m2 );
	*m2++=0;
	#ifndef __MSDOS__
	if( getcookie('_IDT',&val) )
	{
		SepS[0]=(char)val;
		df=(int)(val>>8)&3;
	}
	else
	#endif
	{
		df=1;
	}
	if( !stricmp(dt,"Jan") ) n="1";
	ef( !stricmp(dt,"Feb") ) n="2";
	ef( !stricmp(dt,"Mar") ) n="3";
	ef( !stricmp(dt,"Apr") ) n="4";
	ef( !stricmp(dt,"May") ) n="5";
	ef( !stricmp(dt,"Jun") ) n="6";
	ef( !stricmp(dt,"Jul") ) n="7";
	ef( !stricmp(dt,"Aug") ) n="8";
	ef( !stricmp(dt,"Sep") ) n="9";
	ef( !stricmp(dt,"Oct") ) n="10";
	ef( !stricmp(dt,"Nov") ) n="11";
	else n="12";
	switch( df )
	{ /* poradi je m1 n m2 - puvodne d.m.r*/ 
		char *s;
		case 0: /* MMDDYY */
			s=m1,m1=n,n=s;
		break;
		case 2: /* YYMMDD */
			s=m1,m1=m2,m2=s;
		break;
		case 3: /* YYDDMM */
			s=m1,m1=n,n=s;
			s=m1,m1=m2,m2=s;
		break;
	}
	strcpy(Buf,N);
	strcat(Buf,m1);
	strcat(Buf,SepS);
	strcat(Buf,n);
	strcat(Buf,SepS);
	strcat(Buf,m2);
}

int MenuMain( void )
{
	if( (ApId=appl_init())>=0 )
	{
		if( ZacBut2() )
		{
			Mouse(HOURGLASS);
			if( rsrc_load(NazevRsc) )
			{
				if( ZacRsc() )
				{
					WindRect FD;
					Plati( Menu );
					GetWind(0,WF_FULLXYWH,&FD);
					if( Menu[0].ob_width>FD.w ) Menu[0].ob_width=FD.w;
					if( Menu[1].ob_width>FD.w ) Menu[1].ob_width=FD.w;
					BegUpdate(); /* up NEW */
					menu_bar(Menu,True);
					EndUpdate(); /* up NEW */
					if( Desktop ) NastavDesktop(Desktop);
					if( ZacProg() )
					{
						Cinnost();
						KonProg();
					}
					BegUpdate(); /* up NEW */
					menu_bar(Menu,False);
					EndUpdate(); /* up NEW */
				}
				rsrc_free();
				/* ZacBut2 ma konec atexit */
			}
			else AlertF("[1][%s?][   OK   ]",1,NazevRsc);
		}
		appl_exit();
	}
	return 0;
}
