#ifndef __FILEINTERFACE
#define __FILEINTERFACE

#include <stdio.h>

#include "stdfile.h"


int file_open_directly( const char* sName, FileStd* pFileData );
int file_open( const char* sNam, FileStd* pFileData );
void file_close( FileStd* pFile );
int file_read( void* pWhere, int iCount, FileStd* pFile );

long file_seek( FileStd* pFile, long iPos );
long file_seekcurrent( FileStd* pFile, long iPos );
void file_seekend( FileStd* pFile );
long file_tell( FileStd* pFile );

int file_getc( FileStd* pFile );
long file_getbel( FileStd* pFile );

long file_getw( FileStd* pFile );
long file_getiw( FileStd* pFile );

long file_geti24( FileStd* pFile );
long file_getil( FileStd* pFile );

int LoadAndCopyFile( const char* sFileName, FileStd* pFileData );

#endif
