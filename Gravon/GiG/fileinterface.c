
#include <stdc\memspc.h>

#include "fileinterface.h"

int file_open_directly( const char* sName, FileStd* pFileData )
{
	int length = 0;

	FILE* file = fopen(sName,"rb");
	if( file )
	{
		fseek( file, 0, SEEK_END );

		pFileData->length = ftell(file);
		pFileData->pos = 0;

		length = pFileData->length;

		fseek( file, 0, SEEK_SET );

		pFileData->data = mallocSpc(pFileData->length+32,'CHAR');

		fread(pFileData->data,pFileData->length,1,file);

		pFileData->data[pFileData->length] = 0;

		fclose(file);
	}

	return length;
}


#ifdef FILEINTERFACE_DIRECT_READ
int file_open( const char* sName, FileStd* pFileData )
{
	return file_open_directly(sName,pFileData);
}
#else
int file_open( const char* sName, FileStd* pFileData )
{
	return LoadAndCopyFile(sName,pFileData);
}
#endif

void file_close( FileStd* pFile )
{

	if( pFile && pFile->data )
	{
		freeSpc(pFile->data);
	};

}

int file_read( void* pWhere, int iCount, FileStd* pFile )
{
	unsigned char* dest = (unsigned char*)pWhere;

	if((pFile->pos + iCount) > pFile->length)
		iCount = pFile->length - pFile->pos;

	memcpy(pWhere, pFile->data + pFile->pos, iCount);
	pFile->pos += iCount;
	return iCount;
}

long file_seek( FileStd* pFile, long iPos )
{
	pFile->pos = iPos;
	if( pFile->pos > pFile->length )
	{
		pFile->pos = pFile->length;
		return -1;
	}
	else if( pFile->pos < 0 )
	{
		pFile->pos = 0;
		return -1;
	}

	return pFile->pos;
}
long file_seekcurrent( FileStd* pFile, long iPos )
{
	pFile->pos = pFile->pos+iPos;
	if( pFile->pos > pFile->length )
	{
		pFile->pos = pFile->length;
		return -1;
	}
	else if( pFile->pos < 0 )
	{
		pFile->pos = 0;
		return -1;
	}

	return pFile->pos;
}
void file_seekend( FileStd* pFile )
{
	pFile->pos = pFile->length;
}
long file_tell( FileStd* pFile )
{
	return pFile->pos;
}

int file_getc( FileStd* pFile )
{
	int ch = -1;

	if( pFile->pos < pFile->length )
	{
		ch = pFile->data[pFile->pos++];
	}

	return ch;
}


long file_getbel( FileStd* pFile )
{
  long c,r;
  c=file_getc(pFile);if( c<0 ) return c;
  r=file_getc(pFile);if( r<0 ) return r;
  c <<= 8;
  c|=r;
  r=file_getc(pFile);if( r<0 ) return r;
  c <<= 8;
  c|=r;
  r=file_getc(pFile);if( r<0 ) return r;
  c <<= 8;
  c|=r;
  return c;
}


long file_getw( FileStd* pFile )
{
	unsigned char b[2];
	if(file_read(b, 2, pFile) < 2)
		return -1;
	
	return ((long)b[0]<<8)|b[1];
}

long file_getiw( FileStd* pFile )
{
	unsigned char b[2];
	if(file_read(b, 2, pFile) < 2)
		return -1;

	return ((long)b[1]<<8)|b[0];
}


long file_geti24( FileStd* pFile )
{
  long c,r;
  c=file_getc(pFile);if( c<0 ) return c;
  r=file_getc(pFile);if( r<0 ) return r;
  c|=r<<8;
  r=file_getc(pFile);if( r<0 ) return r;
  c|=r<<16;
  return c;
}
long file_getil( FileStd* pFile )
{
	unsigned char b[4];
	if(file_read(b, 4, pFile) < 4)
		return -1;

	return ((long)b[3]<<24)|((long)b[2]<<16)|((long)b[1]<<8)|b[0];
}


