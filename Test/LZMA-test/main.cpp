#include <El/elementpch.hpp>
#include <El/QStream/qGzStream.hpp>

#define LZMA_API_STATIC
#include <El/QStream/qLzmaStream.hpp>

/*
FuseCompress
Copyright (C) 2006 Milan Svoboda <milan.svoboda@centrum.cz>
LZMA module (C) 2008 Ulrich Hecht <uli@suse.de>
*/

#include <assert.h>
#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <io.h>

#define BUF_SIZE 4096

static const lzma_stream lzma_stream_init = LZMA_STREAM_INIT;
/**
* Decompress data from fd_source into fd_dest.
*
* @param fd_source	Source file descriptor
* @param fd_dest	Destination file descriptor
* @return 		Number of bytes written to fd_dest or (off_t)-1 on error
*/
static off_t lzmaDecompress(int fd_source, int fd_dest)
{
  unsigned char bufin[BUF_SIZE];	/* compressed input buffer */
  unsigned char bufout[BUF_SIZE];	/* uncompressed output buffer */
  int wr;		/* uncompressed bytes written */
  int rd;		/* compressed bytes read */
  off_t size = 0;	/* total uncompressed bytes written */
  int ret;

  int dup_fd = dup(fd_source);
  if(dup_fd == -1) return (off_t)-1;

  /* init LZMA decoder */
  lzma_stream lstr = lzma_stream_init;
  ret = lzma_auto_decoder(&lstr, -1, 0);
  if(ret != LZMA_OK) {
    close(dup_fd);
    return (off_t)-1;
  }

  while ((rd = read(dup_fd, bufin, BUF_SIZE)) > 0)
  {
    lstr.next_in = bufin;
    lstr.avail_in = rd;

    while(lstr.avail_in) {
      lstr.next_out = bufout;
      lstr.avail_out = BUF_SIZE;
      ret = lzma_code(&lstr, LZMA_RUN);
      if(ret < 0) {	/* decompression error */
        lzma_end(&lstr);
        close(dup_fd);
        return (off_t)-1;
      }
      wr = write(fd_dest, bufout, BUF_SIZE - lstr.avail_out);
      if (wr == -1) {
        lzma_end(&lstr);
        close(dup_fd);
        return (off_t) -1;
      }
      size += wr;
      if(ret == LZMA_STREAM_END) break;
    }
    if (ret == LZMA_STREAM_END) break;
  }

  lzma_end(&lstr);

  close(dup_fd);

  if (rd == -1)
  {
    return (off_t) -1;
  }

  return size;
}

int main(int argc, char *argv)
{
  {
    // test QIGzStream
    QIFStream *in = new QIFStream();
    in->open("bebulek.gz");
    QIGzStream inStr(in);
    int  nBytes;
    char buff[1024];
    nBytes = inStr.read(buff,1024);
    printf("GZIP Read %d Bytes\n", nBytes);
    buff[nBytes] = 0;
    printf("TEXT:\n%s\nEND\n", buff);
    in->close();
  }

  {
    // test xz
    int fdin = _open("bebulek.xz", _O_BINARY | _O_RDWR);
    int fdout = _open("bebulek.txt", _O_BINARY | _O_RDWR | _O_CREAT, _S_IREAD | _S_IWRITE );

    lzmaDecompress(fdin, fdout);

    close(fdout);
    close(fdin);
  }

  {
    // test 
    QIFStream *in = new QIFStream();
    in->open("bebulek.xz");
    QILzmaStream inLzmaStr(in);
    char buff[1025];
    int nBytes = 0;
    int newBytes = 0;
    printf("TEXT:\n");
    do 
    {
      newBytes = inLzmaStr.read(buff,1024);
      buff[newBytes] = 0;
      nBytes += newBytes;
      printf("%s", buff);
    } while (newBytes>0);
    printf("\nEND\n");
    printf("LZMA Read %d Bytes\n", nBytes);
    in->close();
  }

  return 0;
}
