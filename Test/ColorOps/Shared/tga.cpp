/*
	tga.cpp
	-------

	Antonin Malik, duben 2003

	Load()    - Nacteni souboru formatu targa do datove struktury BitMap z "BitMap.h"
	Save()    - Ulozeni obrazku z BitMap do souboru formatu targa
	LoadSgm() - Nacteni masky
	SaveSgm() - Ulozeni masky
	Funkce vyuzivaji knihovny libtarga.h
*/


#include <stdio.h>
#include <stdlib.h>

#include "tga.h"
#include "libtarga.h"


//nacteni obrazku ze souboru formatu targa (*.tga)
void Load( BitMap<Color>* bmp, const char* file_name )
{
	unsigned char* buf;
	int w, h, i, j;
	Color p;

	buf= (unsigned char *) tga_load( file_name, &w, &h, TGA_TRUECOLOR_24 );
  if ( buf == NULL ) {
    printf( "\nThe file '%s' does not exist or has not TARGA format.\n", file_name );
    exit(1);
  };

	bmp->Create( w, h );
	//musim zaroven prevratit radky, obrazek je nacteny hore nohama
	for ( i= 0; i < h; i++ ) {
		for ( j= 0; j < w; j++ ) {
			p.r= buf[ ((h-i-1)*w+j)*3   ];
			p.g= buf[ ((h-i-1)*w+j)*3+1 ];
			p.b= buf[ ((h-i-1)*w+j)*3+2 ]; 
			bmp->Set( i, j, p );
		};
	};
};


//ulozeni obrazku bmp do souboru formatu targa (*.tga)
void Save( const BitMap<Color>* bmp, const char* file_name )
{
	//prevratim obrazek hore nohama, aby byl 1. pixel vlevo nahore a ne vlevo dole
	int i, j;
	Color p;
	int h= bmp->H();
	int w= bmp->W();
	unsigned char* buf= new unsigned char[h*w*3];

	//zaroven musim prevratit radky, obrazek se uklada ve formatu, kdy jsou radky hore nohama
  for ( i=0; i<h; i++ ) {
    for ( j=0; j<w; j++ ) {
			p= bmp->Get( i, j );
			buf[ ((h-i-1)*w+j)*3   ]= p.r;
			buf[ ((h-i-1)*w+j)*3+1 ]= p.g;
			buf[ ((h-i-1)*w+j)*3+2 ]= p.b;
		};
	};
	if( !tga_write_raw( file_name, w, h, buf, TGA_TRUECOLOR_24 ) ) 
		printf( "\nCouldn't write TGA image: %s\n", tga_error_string(tga_get_last_error()) );
	free( buf );
};


//nacteni masky ze souboru formatu targa (*.tga), pixely masky se skladaji
//z 0 (pozadi) nebo 1 (objekt).
void LoadSgm( BitMap<unsigned char>* sgm, const char* file_name )
{
	int i, j;
	BitMap<Color>* bmp= new BitMap<Color>;
  Load( bmp, file_name );

	sgm->Create( bmp->W(), bmp->H() );
	for ( i= 0; i<sgm->H(); i++ ) {
		for ( j= 0; j<sgm->W(); j++ ) {
			if ( bmp->Get( i, j ).r )
				sgm->Set( i, j, 1 );
			else
				sgm->Set( i, j, 0 );
		};
	};
	delete bmp;
};


//ulozeni masky do souboru formatu targa (*.tga), pixely masky se skladaji
//z 0 (pozadi) nebo 1 (objekt).
void SaveSgm( const BitMap<unsigned char>* sgm, const char* file_name )
{
	int i, j;
	Color pix;
	BitMap<Color>* show= new BitMap<Color>;

	show->Create( sgm->W(), sgm->H() );
	for ( i= 0; i<sgm->H(); i++ ) {
		for ( j= 0; j<sgm->W(); j++ ) {
			if ( sgm->Get( i, j ) ) 
				pix.r= pix.g= pix.b= 250;
			else
				pix.r= pix.g= pix.b= 0;
			show->Set( i, j, pix );
		};
	};
 	Save( show, file_name );

	delete show;
};

