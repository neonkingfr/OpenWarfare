/*
	SegmentBcg.h
	------------

	Antonin Malik, kveten 2003

	Hlavni funkce - SegmentBcgSqr()
	-------------------------------

	Fce odsegmentufe pozadi od objektu.
*/


#ifndef _segment_bcg_h_
#define _segment_bcg_h_


#include "BitMap.h"
#include "defs.h"


//Pri vysi hodnote prahu muzou byt nektere hrany velmi tenkte, nebo se muzou
//uplne ztracet, to se deje hlavne u obrazku z fotaku.
//Pri nizke hodnote prahu dochazi ke vzniku skvrnek i o velikosti nekolika pixelu
//experimenty byly delane s 90
#define H_EDGE						 80      


//segmentace pozadi od objektu
//vstup  - bmp    - obrazek v kterem fce vyhleda objekt
//       - h      - prah pro hranovy detektor
//       - type   - PHOTO nebo SCAN - u SCAN se neprovadi zaverecna eroze
//vystup - BitMap - maska s vysegmentovanym objektem, 0 - pixel nalezi pozadi, >0 - pixel nalezi objektu
BitMap<unsigned char>* SegmentBcg( const BitMap<Color>* bmp, const int h= H_EDGE,
																	 const int type= PHOTO );


//segmentace pozadi od objektu
//navic vraci souradnice obdelniku, ktery je objektu opsany
BitMap<unsigned char>* SegmentBcg( const BitMap<Color>* bmp,
																	 Pixel& lefttop, Pixel& rightdown,
																	 const int h= H_EDGE, const int type= PHOTO );


//segmentace pozadi od objektu, objektu opise ctverec
//vystup - leftop, rightdown - souradnice vysegmentovaneho obdelniku
BitMap<unsigned char>* SegmentBcgSqr( const BitMap<Color>* bmp, Pixel& lefttop, Pixel& rightdown,
							                        const int h= H_EDGE, const int type= PHOTO );


//detekuje hranove pixely
//tak abych mel bitmapu z 'unsigned char'
void EdgeDtcSobel( const BitMap<Color>* bmp, BitMap<unsigned char>* sgm,
									 const int h= H_EDGE );

//dilatace
// - zvetsi objekt o d pixelu, zahladi diry velikosti d pixelu
//src, dest - pozadi==0, objekty==1
//src, dest - musi met stejny rozmer
//d - velikost strukturniho elementu, ktery je ve tvaru ctverce
void Dilatation( const BitMap<unsigned char>* src,
								 BitMap<unsigned char>* dest, int d );


//eroze = diletace pozadi
// - zmensi objekt od d pixelu, zrusi necistoty velikosti maximalne d pixelu
//source, dest - pozadi==0, objekty==1
//src, dest - musi met stejny rozmer
//d - velikost strukturniho elementu, ktery je ve tvaru ctverce
void Erosion( const BitMap<unsigned char>* src,
							BitMap<unsigned char>* dest, int d );


#endif
