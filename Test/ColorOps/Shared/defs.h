/*
	defs.h
	------

	Antonin Malik, kveten 2003

	Pomocne definice.
*/


#ifndef _defs_h_
#define _defs_h_


#define PHOTO						1
#define SCAN						2


//barva pixelu
typedef struct Color {
  unsigned char r, g, b;
} Color;


//treba jeden zaznam v histogramu
typedef struct Hist {
	int r, g, b;
} Hist;


//souradnice pixelu - levy horni roh je bod [0,0]
typedef struct Pixel {
	int row, col;
} Pixel;


#endif
