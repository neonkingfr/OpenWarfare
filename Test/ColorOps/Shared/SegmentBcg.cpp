/*
	SegmentBcg.cpp
	--------------

	Antonin Malik, kveten 2003

	Fce odsegmentufe pozadi od objektu.
*/


#include <stdlib.h>

/**///#include "tga.h"
#include "SegmentBcg.h"
#include "defs.h"


//velikost otevreni - zrusi vsechny diry nebo mezery velikoti mesi nez D
#define D									 3
//minimalni velikost objektu, objekty mensi velikosti mazu
#define MIN_OBJ						 500
//barvy
#define C_NOT							 0        //musi byt nutne 0, znamena to, ze tu neni hrana
#define C_EDG              1  		  //oznaceni hrany	
#define C_EOB              2        //oznaceni hranoveho objektu (vytvoren pouze z hran)
#define C_BCG              3        //oznaceni pozadi, okoli objektu


//zasobnik pouziva se pri barveni objektu
Pixel* stack;


//vytvori d okraju obrazku, tim, ze je zkopiruje z vnitrni zname casti obrazku
void CopyMargins( BitMap<unsigned char> *bmp, int d )
{
  int i, j, p;
	unsigned char pix;

  //horni okraj
  for ( i= 0;  i<d;  i++ ) {
    for ( j= d;  j<bmp->W()-d;  j++ ) {
			pix= bmp->Get( d, j );
			bmp->Set( i, j, pix );
		};
	};

  //dolni okraj
  p= bmp->H()-d-1;
  for ( i= bmp->H()-d;  i < bmp->H();  i++ ) {
    for ( j= d;  j<bmp->W()-d;  j++ ) {
			pix= bmp->Get( p, j );
			bmp->Set( i, j, pix );
		};
	};

  //levy okraj
  for ( i= 0;  i<bmp->H();  i++ ) {
    for ( j= 0;  j<d;  j++ ) {
			pix= bmp->Get( i, d );
			bmp->Set( i, j, pix );
		};
	};

  //pravy okraj
  p= bmp->W()-d-1;
  for ( i= 0;  i<bmp->H();  i++ ) {
    for ( j= bmp->W()-d;  j<bmp->W();  j++ ) {
			pix= bmp->Get( i, p );
			bmp->Set( i, j, pix );
		};
	};
};


//---------------------------   Detekce Hran   ------------------------------------------


/* - prilis citlive
// |   1  1  1  |
// |   1 -8  1  |
// |   1  1  1  |
//detekuje hranove pixely
void EdgeDtcLaplac( const BitMap<Color>* bmp, BitMap<unsigned char>* sgm )
{
	int i, j;
	Color pix;
	Hist p;

  //detekce
  for ( i= 1;  i<bmp->H()-1;  i++ ) {
    for ( j= 1;  j<bmp->W()-1;  j++ ) {
			p.r= bmp->Get( i-1, j-1 ).r 
				 + bmp->Get( i-1, j ).r
				 + bmp->Get( i-1, j+1 ).r
				 + bmp->Get( i, j-1 ).r
				 - 8 * bmp->Get( i, j ).r
				 + bmp->Get( i, j+1 ).r						
				 + bmp->Get( i+1, j-1 ).r
				 + bmp->Get( i+1, j ).r					
				 + bmp->Get( i+1, j+1 ).r;
			p.g= bmp->Get( i-1, j-1 ).g 
				 + bmp->Get( i-1, j ).g
				 + bmp->Get( i-1, j+1 ).g
				 + bmp->Get( i, j-1 ).g
				 - 8 * bmp->Get( i, j ).g
				 + bmp->Get( i, j+1 ).g						
				 + bmp->Get( i+1, j-1 ).g
				 + bmp->Get( i+1, j ).g					
				 + bmp->Get( i+1, j+1 ).g;
			p.b= bmp->Get( i-1, j-1 ).b 
				 + bmp->Get( i-1, j ).b
				 + bmp->Get( i-1, j+1 ).b
				 + bmp->Get( i, j-1 ).b
				 - 8 * bmp->Get( i, j ).b
				 + bmp->Get( i, j+1 ).b						
				 + bmp->Get( i+1, j-1 ).b
				 + bmp->Get( i+1, j ).b					
				 + bmp->Get( i+1, j+1 ).b;
			if (  abs(p.r)+abs(p.g)+abs(p.b) > 3*H_EDGE  )
    		sgm->Set(i, j, 1 );
			else
  		  sgm->Set(i, j, 0 );
		};
	};
  //uprava okraju
  CopyMargins( sgm, 1 );
};
*/


// |  1  2  1 |   | 1  0 -1 |   | 2  1  0 |   |  0  1  2 |
// |  0  0  0 |   | 2  0 -2 |   | 1  0 -1 |   | -1  0  1 |
// | -1 -2 -1 |   | 1  0 -1 |   | 0 -1 -2 |   | -2 -1  0 |
//detekuje hranove pixely
//tak abych mel bitmapu z 'unsigned char'
void EdgeDtcSobel( const BitMap<Color>* bmp, BitMap<unsigned char>* sgm, const int h )
{
	int i, j;
	Hist p;

	//detekce
	for ( i= 1;  i<bmp->H()-1;  i++ ) {
    for ( j= 1;  j<bmp->W()-1;  j++ ) {
			//vodorovna -
			p.r= bmp->Get( i-1, j-1 ).r 
				 + 2 * bmp->Get( i-1, j ).r
				 + bmp->Get( i-1, j+1 ).r
				 - bmp->Get( i+1, j-1 ).r
				 - 2 * bmp->Get( i+1, j ).r					
				 - bmp->Get( i+1, j+1 ).r;
			if (  abs(p.r) > h  ) {
  			sgm->Set(i, j, C_EDG );
				continue;
			};
			p.g= bmp->Get( i-1, j-1 ).g 
				 + 2 * bmp->Get( i-1, j ).g
				 + bmp->Get( i-1, j+1 ).g
				 - bmp->Get( i+1, j-1 ).g
				 - 2 * bmp->Get( i+1, j ).g					
				 - bmp->Get( i+1, j+1 ).g;
			if (  abs(p.g) > h  ) {
  			sgm->Set(i, j, C_EDG );
				continue;
			};
			p.b= bmp->Get( i-1, j-1 ).b 
				 + 2 * bmp->Get( i-1, j ).b
				 + bmp->Get( i-1, j+1 ).b
				 - bmp->Get( i+1, j-1 ).b
				 - 2 * bmp->Get( i+1, j ).b					
				 - bmp->Get( i+1, j+1 ).b;
			if (  abs(p.b) > h  ) {
  			sgm->Set(i, j, C_EDG );
				continue;
			};
			//svisla |
  		p.r= bmp->Get( i-1, j-1 ).r 
				 + 2 * bmp->Get( i, j-1 ).r
				 + bmp->Get( i+1, j-1 ).r
			   - bmp->Get( i-1, j+1 ).r
			   - 2 * bmp->Get( i, j+1 ).r					
			   - bmp->Get( i+1, j+1 ).r;
			if (  abs(p.r) > h  ) {
  			sgm->Set(i, j, C_EDG );
	  		continue;
			};
		  p.g= bmp->Get( i-1, j-1 ).g 
			   + 2 * bmp->Get( i, j-1 ).g
			   + bmp->Get( i+1, j-1 ).g
 				 - bmp->Get( i-1, j+1 ).g
  			 - 2 * bmp->Get( i, j+1 ).g					
	  		 - bmp->Get( i+1, j+1 ).g;
			if (  abs(p.g) > h  ) {
  			sgm->Set(i, j, C_EDG );
				continue;
			};
 			p.b= bmp->Get( i-1, j-1 ).b 
  			 + 2 * bmp->Get( i, j-1 ).b
	  		 + bmp->Get( i+1, j-1 ).b
		  	 - bmp->Get( i-1, j+1 ).b
			   - 2 * bmp->Get( i, j+1 ).b					
 				 - bmp->Get( i+1, j+1 ).b;
			if (  abs(p.b) > h  ) {
  			sgm->Set(i, j, C_EDG );
				continue;
			};
			//sikma /
 			p.r= bmp->Get( i, j-1 ).r 
  			 + 2 * bmp->Get( i-1, j-1 ).r
	  		 + bmp->Get( i-1, j ).r
		  	 - bmp->Get( i, j+1 ).r
			   - 2 * bmp->Get( i+1, j+1 ).r					
			   - bmp->Get( i+1, j ).r;
			if (  abs(p.r) > h  ) {
  			sgm->Set(i, j, C_EDG );
				continue;
			};
 			p.g= bmp->Get( i, j-1 ).g 
  			 + 2 * bmp->Get( i-1, j-1 ).g
	  		 + bmp->Get( i-1, j ).g
		  	 - bmp->Get( i, j+1 ).g
			   - 2 * bmp->Get( i+1, j+1 ).g					
 				 - bmp->Get( i+1, j ).g;
			if (  abs(p.g) > h  ) {
  			sgm->Set(i, j, C_EDG );
				continue;
			};
 			p.b= bmp->Get( i, j-1 ).b 
  			 + 2 * bmp->Get( i-1, j-1 ).b
 				 + bmp->Get( i-1, j ).b
  			 - bmp->Get( i, j+1 ).b
	  		 - 2 * bmp->Get( i+1, j+1 ).b					
		  	 - bmp->Get( i+1, j ).b;
			if (  abs(p.b) > h  ) {
  			sgm->Set(i, j, C_EDG );
				continue;
			};
			//sikma \                    //
 			p.r= bmp->Get( i-1, j ).r 
  			 + 2 * bmp->Get( i-1, j+1 ).r
	  		 + bmp->Get( i, j+1 ).r
		  	 - bmp->Get( i, j-1 ).r
			   - 2 * bmp->Get( i+1, j-1 ).r					
 				 - bmp->Get( i+1, j ).r;
			if (  abs(p.r) > h  ) {
  			sgm->Set(i, j, C_EDG );
				continue;
			};
 			p.g= bmp->Get( i-1, j ).g 
  			 + 2 * bmp->Get( i-1, j+1 ).g
	  		 + bmp->Get( i, j+1 ).g
		  	 - bmp->Get( i, j-1 ).g
			   - 2 * bmp->Get( i+1, j-1 ).g					
 				 - bmp->Get( i+1, j ).g;
			if (  abs(p.g) > h  ) {
  			sgm->Set(i, j, C_EDG );
				continue;
			};
 			p.b= bmp->Get( i-1, j ).b 
  			 + 2 * bmp->Get( i-1, j+1 ).b
	  		 + bmp->Get( i, j+1 ).b
		  	 - bmp->Get( i, j-1 ).b
			   - 2 * bmp->Get( i+1, j-1 ).b					
			   - bmp->Get( i+1, j ).b;
			if (  abs(p.b) > h  ) {
  		  sgm->Set(i, j, C_EDG );
				continue;
			};
			//kdyz to proslo az sem, tak tam zadna hrana neni
  		sgm->Set(i, j, C_NOT );
		};  //for
	};  //for
  //uprava okraju
  CopyMargins( sgm, 1 );
};


//---------------------------   Matematicka morfologie   --------------------------------


//dilatace
//src, dest - pozadi==0, objekty==1
//src, dest - musi met stejny rozmer
//d - velikost strukturniho elementu, ktery je ve tvaru ctverce
void Dilatation( const BitMap<unsigned char>* src,
								 BitMap<unsigned char>* dest, int d )
{
  int i, j, k, l;
	int h= src->H(),
		  w= src->W();

  //init
  for ( i= 0;  i<h;  i++ )
    for ( j= 0;  j<w;  j++ )
      dest->Set( i, j, C_NOT );

  //vlastni diletace
  for ( i= d;  i<h-d;  i++ ) {
    for ( j= d;  j<w-d;  j++ ) {
      if ( src->Get( i, j ) ) {
				//projdu okoli bodu [i,j]
				for ( k= -d; k<=d; k++ ) {
					for ( l= -d; l<=d; l++ ) {
						//x= j+k;
						//y= i+l;
		        //if (  x >= 0  &&  y >= 0  &&  x < dest->W()  &&  y < dest->H()  )
	          dest->Set( i+l, j+k, C_EDG );
					};
				};
      };
    };
  };
};


//eroze = diletace pozadi
//source, dest - pozadi==0, objekty==1
//src, dest - musi met stejny rozmer
//d - velikost strukturniho elementu, ktery je ve tvaru ctverce
void Erosion( const BitMap<unsigned char>* src,
							BitMap<unsigned char>* dest, int d )
{
	int li= dest->H()-1;
	int lj= dest->W()-1;
  int i, j, k, l;
	int h= src->H(),
		  w= src->W();

  //init
  for ( i= 1;  i<li;  i++ )
    for ( j= 1;  j<lj;  j++ )
      dest->Set( i, j, C_EDG );
	//zacnu erozi okraje
	for ( j= 0; j<w; j++ ) {
		dest->Set( 0, j, C_NOT );
		dest->Set( li, j, C_NOT );
	};
	for ( i= 0; i<h; i++ ) {
		dest->Set( i, 0, C_NOT );
		dest->Set( i, lj, C_NOT );
	};
  //vlastni eroze = dilatace pozadi
  for ( i= d;  i<h-d;  i++ ) {
    for ( j= d;  j<w-d;  j++ ) {
      if ( !src->Get( i, j ) ) {
				//projdu okoli bodu [i,j]
				for ( k= -d; k<=d; k++ ) {
					for ( l= -d; l<=d; l++ ) {
						//x= j+k;
						//y= i+l;
		        //if (  x >= 0  &&  y >= 0  &&  x < dest->W()  &&  y < dest->H()  )
	          dest->Set( i+l, j+k, C_NOT );
					};
				};
      };
    };
  };
};


//---------------------------   Segmentace pozadi   ------------------------------------


//obarvi pixel [i,j] barvou 'c' a pokracuje s obarvovanim sousedu (ctyrsousednost)
int	GetSizeObj( BitMap<unsigned char>* bmp, int i, int j )
{
	int s= 0;
	int v= -1;

	v++;
	stack[v].row= i;
	stack[v].col= j;
	bmp->Set( i, j, C_EOB );
	s++;
	while (  v >= 0  ) {
		i= stack[v].row;
		j= stack[v].col;
		v--;
		//navrch
		if ( i > 0 ) {
			if (  bmp->Get( i-1, j ) == C_EDG  ) {
		  	v++;
  			stack[v].row= i-1;
	  	  stack[v].col= j;
				bmp->Set( i-1, j, C_EOB );
				s++;
			};
		};
		//doprava
		if (  j+1 < bmp->W()  ) {
			if (  bmp->Get( i, j+1 ) == C_EDG  ) {
		  	v++;
				stack[v].row= i;
			  stack[v].col= j+1;
				bmp->Set( i, j+1, C_EOB );
				s++;
			};
		};
		//dule
		if (  i+1 < bmp->H()  ) {
			if (  bmp->Get( i+1, j ) == C_EDG  ) {
		  	v++;
				stack[v].row= i+1;
			  stack[v].col= j;
				bmp->Set( i+1, j, C_EOB );
				s++;
			};
		};
		//doleva
		if ( j > 0 ) {
			if (  bmp->Get( i, j-1 ) == C_EDG  ) {
		  	v++;
				stack[v].row= i;
				stack[v].col= j-1;
				bmp->Set( i, j-1, C_EOB );
				s++;
			};
		};
	};

	return s;
};


//smaze objekt oznaceny 2
void DeleteObj( BitMap<unsigned char>* bmp, int i, int j )
{
	int v= -1;

	v++;
	stack[v].row= i;
	stack[v].col= j;
	bmp->Set( i, j, 0 );
	while (  v >= 0  ) {
		i= stack[v].row;
		j= stack[v].col;
		v--;
		//navrch
		if ( i > 0 ) {
			if (  bmp->Get( i-1, j ) == C_EOB  ) {
		  	v++;
  			stack[v].row= i-1;
	  	  stack[v].col= j;
				bmp->Set( i-1, j, C_NOT );
			};
		};
		//doprava
		if (  j+1 < bmp->W()  ) {
			if (  bmp->Get( i, j+1 ) == C_EOB  ) {
		  	v++;
				stack[v].row= i;
			  stack[v].col= j+1;
				bmp->Set( i, j+1, C_NOT );
			};
		};
		//dule
		if (  i+1 < bmp->H()  ) {
			if (  bmp->Get( i+1, j ) == C_EOB  ) {
		  	v++;
				stack[v].row= i+1;
			  stack[v].col= j;
				bmp->Set( i+1, j, C_NOT );
			};
		};
		//doleva
		if ( j > 0 ) {
			if (  bmp->Get( i, j-1 ) == C_EOB  ) {
		  	v++;
				stack[v].row= i;
				stack[v].col= j-1;
				bmp->Set( i, j-1, C_NOT );
			};
		};
	};
};


//obarvim vsechny hranove objekty, abych mohl ty male smazat
void DeleteSmallObj( BitMap<unsigned char>* bmp )
{
	int i, j, s;

	for ( i= 0; i<bmp->H(); i++ ) {
		for ( j= 0; j<bmp->W(); j++ ) {
			//je to neobarveny objekt
			if (  bmp->Get( i, j ) == C_EDG  ) {
				//vrati velikost objektu, ktery tu zacina, priste ho uz neprochazim
				s= GetSizeObj( bmp, i, j );
				if (  s < MIN_OBJ  )
					DeleteObj( bmp, i, j );
			};
		};
	};
};


//obarvi pozadi objektu z pixelu [i,j] az kam to jde
void SetBcg( BitMap<unsigned char>* bmp, int i, int j )
{
	int v= -1;

	v++;
	stack[v].row= i;
	stack[v].col= j;
	bmp->Set( i, j, 0 );
	while (  v >= 0  ) {
		i= stack[v].row;
		j= stack[v].col;
		v--;
		//navrch
		if ( i > 0 ) {
			if (  bmp->Get( i-1, j ) == C_NOT  ) {
		  	v++;
  			stack[v].row= i-1;
	  	  stack[v].col= j;
				bmp->Set( i-1, j, C_BCG );
			};
		};
		//doprava
		if (  j+1 < bmp->W()  ) {
			if (  bmp->Get( i, j+1 ) == C_NOT  ) {
		  	v++;
				stack[v].row= i;
			  stack[v].col= j+1;
				bmp->Set( i, j+1, C_BCG );
			};
		};
		//dule
		if (  i+1 < bmp->H()  ) {
			if (  bmp->Get( i+1, j ) == C_NOT  ) {
		  	v++;
				stack[v].row= i+1;
			  stack[v].col= j;
				bmp->Set( i+1, j, C_BCG );
			};
		};
		//doleva
		if ( j > 0 ) {
			if (  bmp->Get( i, j-1 ) == C_NOT  ) {
		  	v++;
				stack[v].row= i;
				stack[v].col= j-1;
				bmp->Set( i, j-1, C_BCG );
			};
		};
	};
};


//obarvim pozadi, kolem objektu
//vysledek: kazdy pixel ma barvu pozadi C_BCG, nebo objektu C_EOB
void ColorBcg( BitMap<unsigned char>* bmp )
{
	int i, j;
	int l_i= bmp->H()-1;
	int l_j= bmp->W()-1;

	//obarvim pozadi
	//zacnu: prvni radek, posledni radek
	for ( j= 0; j<bmp->W(); j++ ) {
		if (  bmp->Get( 0, j ) == C_NOT  )
			SetBcg( bmp, 0, j );
  	if (  bmp->Get( l_i, j ) == C_NOT  )
			SetBcg( bmp, l_i, j );
	};
	//zacnu: levy radek, pravy radek
	for ( i= 0; i<bmp->H(); i++ ) {
		if (  bmp->Get( i, 0 ) == C_NOT  )
			SetBcg( bmp, i, 0 );
  	if (  bmp->Get( i, l_j ) == C_NOT  )
			SetBcg( bmp, i, l_j );
	};

	//obarvim objekt, i znova pozadi na pozadovany vystup - 0 pozadi, 1 objekt
	for ( i= 0; i<bmp->H(); i++ ) {
		for ( j= 0; j<bmp->W(); j++ ) {
			if (  bmp->Get( i, j ) == C_BCG  )
				bmp->Set( i, j, 0 );
			else
				bmp->Set( i, j, 1 );
		};
	};
};


//vysegmentovanemu objektu opise ctverec a vrati jeho rozmery
void GetSize( const BitMap<unsigned char>* sgm, Pixel& lefttop, Pixel& rightdown )
{
	int i, j;

	lefttop.row= sgm->H();
	lefttop.col= sgm->W();
	rightdown.row= rightdown.col= 0;
	for ( i= 0; i<sgm->H(); i++ ) {
		for ( j= 0; j<sgm->W(); j++ ) {
			//objekt
			if ( sgm->Get( i, j ) ) {
				if (  i < lefttop.row  )
					lefttop.row= i;
				if (  j < lefttop.col  )
					lefttop.col= j;
				if (  i > rightdown.row  )
					rightdown.row= i;
				if (  j > rightdown.col  )
					rightdown.col= j;
			};
		};
	};
};


//vyplni cely obdelnik ohraniceny body lefttop a rightdown
void FullSgm( BitMap<unsigned char>* sgm, Pixel lefttop, Pixel rightdown )
{
  int i, j;

  for ( i= lefttop.row; i<=rightdown.row; i++  ) {
    for ( j= lefttop.col; j<=rightdown.col; j++ ) {
      sgm->Set( i, j, C_EOB );
    };
  };
};


//segmentace pozadi od objektu
BitMap<unsigned char>* SegmentBcg( const BitMap<Color>* bmp, const int h,
																	 const int type )
{
	BitMap<unsigned char>* sgm= new BitMap<unsigned char>;
	BitMap<unsigned char>* dil= new BitMap<unsigned char>;

	sgm->Create( bmp->W(), bmp->H() );
	dil->Create( bmp->W(), bmp->H() );
	EdgeDtcSobel( bmp, sgm, h );
	/**///SaveSgm( sgm, "krabka_scan_01.tga" );
	Dilatation( sgm, dil, D );
	Erosion( dil, sgm, D );
	/**///SaveSgm( sgm, "krabka_scan_02.tga" );
	stack= (Pixel *) malloc( sgm->H()*sgm->W() * sizeof(Pixel) );
	DeleteSmallObj( sgm );
	/**///SaveSgm( sgm, "krabka_scan_03.tga" );
	ColorBcg( sgm );
	/**///SaveSgm( sgm, "krabka_scan_04.tga" );
	free( stack );
	Erosion( sgm, dil, D );
	if (  type == PHOTO  )
		Dilatation( dil, sgm, D );
	/**///SaveSgm( sgm, "sgm.tga" );
	delete dil;

	return sgm;
};


//segmentace pozadi od objektu
BitMap<unsigned char>* SegmentBcg( const BitMap<Color>* bmp,
																	 Pixel& lefttop, Pixel& rightdown, const int h,
																	 const int type )
{
	BitMap<unsigned char>* sgm= new BitMap<unsigned char>;
	BitMap<unsigned char>* dil= new BitMap<unsigned char>;

	sgm->Create( bmp->W(), bmp->H() );
	dil->Create( bmp->W(), bmp->H() );
	EdgeDtcSobel( bmp, sgm, h );
	/**///SaveSgm( sgm, "krabka_scan_01.tga" );
	Dilatation( sgm, dil, D );
	Erosion( dil, sgm, D );
	/**///SaveSgm( sgm, "krabka_scan_02.tga" );
	stack= (Pixel *) malloc( sgm->H()*sgm->W() * sizeof(Pixel) );
	DeleteSmallObj( sgm );
	/**///SaveSgm( sgm, "krabka_scan_03.tga" );
	ColorBcg( sgm );
	/**///SaveSgm( sgm, "krabka_scan_04.tga" );
	free( stack );
	Erosion( sgm, dil, D );
	if (  type == PHOTO  )
		Dilatation( dil, sgm, D );
	/**///SaveSgm( sgm, "sgm.tga" );
	GetSize( sgm, lefttop, rightdown );
	delete dil;

	return sgm;
};


//segmentace pozadi od objektu
BitMap<unsigned char>* SegmentBcgSqr( const BitMap<Color>* bmp, Pixel& lefttop, Pixel& rightdown,
							                        const int h, const int type )
{
	BitMap<unsigned char>* sgm= new BitMap<unsigned char>;
	BitMap<unsigned char>* dil= new BitMap<unsigned char>;

	sgm->Create( bmp->W(), bmp->H() );
	dil->Create( bmp->W(), bmp->H() );
	EdgeDtcSobel( bmp, sgm, h );
	/**///SaveSgm( sgm, "krabka_scan_01.tga" );
	Dilatation( sgm, dil, D );
	Erosion( dil, sgm, D );
	/**///SaveSgm( sgm, "krabka_scan_02.tga" );
	stack= (Pixel *) malloc( sgm->H()*sgm->W() * sizeof(Pixel) );
	DeleteSmallObj( sgm );
	/**///SaveSgm( sgm, "krabka_scan_03.tga" );
	ColorBcg( sgm );
	/**///SaveSgm( sgm, "krabka_scan_04.tga" );
	free( stack );
	Erosion( sgm, dil, D );
	if (  type == PHOTO  )
		Dilatation( dil, sgm, D );
	/**///SaveSgm( sgm, "sgm.tga" );
	GetSize( sgm, lefttop, rightdown );
  FullSgm( sgm, lefttop, rightdown );

	delete dil;

  return sgm;
};
