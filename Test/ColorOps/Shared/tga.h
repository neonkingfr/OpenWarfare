/*
	tga.h
	-----

	Antonin Malik, duben 2003

	Load() - Nacteni souboru formatu targa do datove struktury BitMap z "BitMap.h"
	Save() - Ulozeni obrazku z BitMap do souboru formatu targa
	LoadSgm() - Nacteni masky
	SaveSgm() - Ulozeni masky
	Funkce vyuzivaji knihovny libtarga.h
*/


#ifndef _tga_h_
#define _tga_h_


#include "BitMap.h"
#include "defs.h"


//nacteni obrazku ze souboru formatu targa (*.tga)
void Load( BitMap<Color>* bmp, const char* file_name );


//ulozeni obrazku bmp do souboru formatu targa (*.tga)
void Save( const BitMap<Color>* bmp, const char* file_name );


//nacteni masky ze souboru formatu targa (*.tga), pixely masky se skladaji
//z 0 (pozadi) nebo 1 (objekt).
void LoadSgm( BitMap<unsigned char>* sgm, const char* file_name );


//ulozeni masky do souboru formatu targa (*.tga), pixely masky se skladaji
//z 0 (pozadi) nebo 1 (objekt).
void SaveSgm( const BitMap<unsigned char>* sgm, const char* file_name );


#endif
