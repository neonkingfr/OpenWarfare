/*
  Test.cpp
	--------

	Antonin Mali, duben 2003

	Testovani funkce TransfHist() z TransfHist.h
*/


#include <stdio.h>
#include <stdlib.h>

#include "TransfHist.h"
#include "SegmentBcg.h"
#include "tga.h"


/*
void LoadTest( BitMap<Color>* photo, BitMap<Color>* scan )
{
	int h, w, i, j, p;
	Color pix;

	FILE* f= fopen( "test.txt", "r" );
	fscanf( f, "%d%d", &h, &w );
	photo->Create( w, h );
	for ( i= 0; i<h; i++ ) {
		for ( j= 0; j<w; j++ ) {
			fscanf( f, "%d", &p );
			pix.r= pix.g= pix.b= p;
			photo->Set( i, j, pix );
		};
	};
	fscanf( f, "%d%d", &h, &w );
	scan->Create( w, h );
	for ( i= 0; i<h; i++ ) {
		for ( j= 0; j<w; j++ ) {
			fscanf( f, "%d", &p );
			pix.r= pix.g= pix.b= p;
			scan->Set( i, j, pix );
		};
	};
	fclose( f );
};


//save test
void SaveTest( const BitMap<Color>* bmp )
{
	int i, j;
	Color pix;
	FILE* f= fopen( "test-result.txt", "w" );

	fprintf( f, "%d %d\n", bmp->H(), bmp->W() );
	for ( i= 0; i<bmp->H(); i++ ) {
		for ( j= 0; j<bmp->W(); j++ ) {
			pix= bmp->Get( i, j );
			fprintf( f, "%3d ", pix.r );
		};
		fprintf( f, "\n" );
	};
	fclose( f );
};
*/


//transfomace jednotlivych souboru
void Transf( const char* name_photo, const char* name_scan, const char* name_new )
{
	BitMap<Color>* photo= new BitMap<Color>;
	BitMap<Color>* scan = new BitMap<Color>;
	BitMap<Color>* scan_new;

	printf( "\n%s:\n", name_scan );
	Load( photo, name_photo );
	Load( scan, name_scan );
/**///	LoadTest( photo, scan );
	scan_new= TransfHistSpecBcg( photo, scan );
	Save( scan_new, name_new );
/**///	SaveTest( scan_new );
	printf( "Transformace probehla.\n" );

	delete photo;
	delete scan;
	delete scan_new;
};


/*
//porovna obrazek se svou segmentaci
void CompareSgm( const char* name_bmp, const char* name_new, int h )
{
	BitMap<Color>* bmp= new BitMap<Color>;
	BitMap<unsigned char>* sgm= new BitMap<unsigned char>;
	BitMap<Color>* bmp_new= new BitMap<Color>;
	Color pix;
	int i, j;

	printf( "%s\n", name_bmp );
	Load( bmp, name_bmp );
	sgm= SegmentBcg( bmp, h );
	bmp_new->Create( bmp->W(), bmp->H() );
	for ( i= 0; i<bmp->H(); i++ ) {
		for ( j= 0; j<bmp->W(); j++ ) {
			if ( sgm->Get( i, j ) )			
				pix= bmp->Get( i, j );
			else
				pix.r= pix.g= pix.b= 250;
			bmp_new->Set( i, j, pix );
		};
	};
	Save( bmp_new, name_new );

	delete bmp;
	delete sgm;
	delete bmp_new;
};
*/


//main
int main() 
{
	Transf( "cd_foto.tga", "cd_scan.tga", "cd_new.tga" );
//  Transf( "disketa_foto.tga", "disketa_scan.tga", "disketa_new.tga" );
//	Transf( "eso_foto.tga", "eso_scan.tga", "eso_new.tga" );
//  Transf( "kniha_foto.tga", "kniha_scan.tga", "kniha_new.tga" );
//	Transf( "krabka_foto.tga", "krabka_scan.tga", "krabka_new.tga" );
//	Transf( "krystal_foto.tga", "krystal_scan.tga", "krystal_new.tga" );
//	Transf( "kura_foto.tga", "kura_scan.tga", "kura_new.tga" );
//	Transf( "papir_foto.tga", "papir_scan.tga", "papir_new.tga" );
/*
	CompareSgm( "cd_foto.tga", "cd_foto_06.tga", 30 );
	CompareSgm( "cd_scan.tga", "cd_scan_06.tga", 130 );
	CompareSgm( "disketa_foto.tga", "disketa_foto_06.tga", 30 );
	CompareSgm( "disketa_scan.tga", "disketa_scan_06.tga", 130 );
	CompareSgm( "eso_foto.tga", "eso_foto_06.tga", 30 );
	CompareSgm( "eso_scan.tga", "eso_scan_06.tga", 130 );
	CompareSgm( "kniha_foto.tga", "kniha_foto_06.tga", 30 );
	CompareSgm( "kniha_scan.tga", "kniha_scan_06.tga", 130 );
	CompareSgm( "krabka_foto.tga", "krabka_foto_06.tga", 30 );
	CompareSgm( "krabka_scan.tga", "krabka_scan_06.tga", 130 );
	CompareSgm( "krystal_foto.tga", "krystal_foto_06.tga", 30 );
	CompareSgm( "krystal_scan.tga", "krystal_scan_06.tga", 130 );
	CompareSgm( "kura_foto.tga", "kura_foto_06.tga", 30 );
	CompareSgm( "kura_scan.tga", "kura_scan_06.tga", 130 );
	CompareSgm( "papir_foto.tga", "papir_foto_06.tga", 30 );
	CompareSgm( "papir_scan.tga", "papir_scan_06.tga", 130 );
*/
  printf( "%c%c%c------------------------------\n", 7, 7, 7 );

	return 0;
}
