/*
	TransfHist.h
	------------

	Antonin Malik, duben 2003

  Hlavni funkce - TransfHistMatchBcg()
	------------------------------------

	Vstup:
  - photo - obrazek vyfoceny fotakem
	- scan  - obrazek nascenovany 3D-scannerem

	Vystup:
	- upraveny "scan", aby barevne odpovidal "photo"

  Obe funkce alokuje pamet pro vystupni obrazek.
*/


#ifndef _trans_hist_h_
#define _trans_hist_h_


#include "..\Shared\BitMap.h"
#include "..\Shared\defs.h"


//transformace histogramu - Histogram obrazku ze scaneru se transformuje na histogram
//obrazku z fotaku. Pomer barev u vysledneho obrazku je podobny jako u obrazku
//z fotaku. 
//Transformace pouze meni paletu.
//Transormace se automaticky ulozi do transf.txt.
//Funkce alokuje pamet pro vystupni obrazek.
//photo   - obrazek vyfoceny fotakem
//scan    - obrazek nascenovany 3D-scannerem
//return: - upraveny "scan", aby barevne odpovidal "photo"
BitMap<Color>* TransfHistSpec( const BitMap<Color>* photo, const BitMap<Color>* scan );


//transformace histogramu - Histogram obrazku ze scaneru se transformuje na histogram
//obrazku z fotaku. Pomer barev u vysledneho obrazku je tedy stejny jako u obrazku
//z fotaku (histogramy si temer uplne odpovidaji)
//Funkce alokuje pamet pro vystupni obrazek.
//photo   - obrazek vyfoceny fotakem
//scan    - obrazek nascenovany 3D-scannerem
//return: - upraveny "scan", aby barevne odpovidal "photo"
BitMap<Color>* TransfHistMatch( const BitMap<Color>* photo, const BitMap<Color>* scan );


//Jako fce 'TransfHistSpec', ale nejdriv odsegmentuje pozadi a dal uz pracuje jen
//s objektem
//photo   - obrazek vyfoceny fotakem
//scan    - obrazek nascenovany 3D-scannerem
//return: - upraveny "scan", aby barevne odpovidal "photo"
BitMap<Color>* TransfHistSpecBcg( const BitMap<Color>* photo, const BitMap<Color>* scan,
                 const BitMap<unsigned char>* photo_sgm, const BitMap<unsigned char>* scan_sgm );


//Jako fce 'TransfHistSpec', ale nejdriv odsegmentuje pozadi a dal uz pracuje jen
//s objektem
//photo   - obrazek vyfoceny fotakem
//scan    - obrazek nascenovany 3D-scannerem
//return: - upraveny "scan", aby barevne odpovidal "photo"
BitMap<Color>* TransfHistMatchBcg( const BitMap<Color>* photo, const BitMap<Color>* scan,
                 const BitMap<unsigned char>* photo_sgm, const BitMap<unsigned char>* scan_sgm );


//vrati histogramy obrazku, funkce alokuje pamet
//vystup: hist - histogram obrazku bmp
Hist* GetHist( const BitMap<Color>* bmp );


//vrati histogramy vyrezaneho obdelniku v obrazku, funkce alokuje pamet
//vstup : - bmp - obrazek, jehoz histgram chci zjistit
//        - bmp_lt, bmp_rd - souradnice obdelniku
//vystup: - hist_(r,g,b) - histogram obrazku bmp
//        - n            - velikost objektu v obrazku
Hist* GetHistBcg( const BitMap<Color>* bmp, const BitMap<Color>* sgm, int& n );


//vytvori obrazek s histogrami
//vystup: soubor - obrazek, v nemz jsou zaneseny vsechny tri histogramy
BitMap<Color>* SaveHist( const char* file_name, Hist* hist );


#endif
