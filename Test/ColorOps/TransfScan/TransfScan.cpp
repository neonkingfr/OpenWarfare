/*
  TransfScan.cpp
  --------------

  Antonin Malik, srpen 2003

  Transformace obrazku z 3D-scanneru, aby barevne odpovidal obrazku z fotaku.
  Vstup:  - obrazek z 3D-scanneru
          - obrazek z fotaku
  Vystup: - pretransformovany obrazek z 3D-scanneru
*/


#include <stdio.h>
#include <string.h>

#include "..\Shared\tga.h"
#include "TransfHist.h"

//pro parametry programu
#define P_WRONG                 0             //chybne parametry, nebo chybny pocet parametru
#define P_3                     3             //2 parametry
#define P_5                     5             //3 parametry 


//check arguments
int CheckArguments( int argc, char* argv[] )
{
  if (  argc != 4  &&  argc != 6  ) {
    printf( "TransfScan: Wrong number of parameters. Enter 3 parameters - 3D-scanner, photo and output file (*.tga).\n" );
    return P_WRONG;
  };
  if (  strstr( argv[1], ".tga" ) == NULL  ) {
    printf( "TransfScan: The 1. parametr (scan file name) isn't in TARGA format (*.tga).\n" );
    return P_WRONG;
  };
  if (  strstr( argv[2], ".tga" ) == NULL  ) {
    printf( "TransfScan: The 2. parametr (photo file name) isn't in TARGA format (*.tga).\n" );
    return P_WRONG;
  };
  if (  strstr( argv[3], ".tga" ) == NULL  ) {
    printf( "TransfScan: The 3. parametr (output file name) isn't in TARGA format (*.tga).\n" );
    return P_WRONG;
  };
  if (  argc == 6  ) {
    if (  strstr( argv[4], ".tga" ) == NULL  ) {
      printf( "TransfScan: The 4. parametr (mask scan file name) isn't in TARGA format (*.tga).\n" );
      return P_WRONG;
    };
    if (  strstr( argv[5], ".tga" ) == NULL  ) {
      printf( "TransfScan: The 5. parametr (mask photo file name) isn't in TARGA format (*.tga).\n" );
      return P_WRONG;
    };
    return P_5;
  };
  return P_3;    
};

 
//main
void main( int argc, char* argv[] )
{
  int param= CheckArguments( argc, argv );

  if ( param == P_WRONG  ) 
    return;

  BitMap<Color>* photo= new BitMap<Color>;
  BitMap<Color>* scan = new BitMap<Color>;
  BitMap<Color>* scan_new;
  BitMap<unsigned char>* photo_sgm= new BitMap<unsigned char>;
  BitMap<unsigned char>* scan_sgm= new BitMap<unsigned char>;

  printf( "Transformation of '%s' is running ...", argv[1] );
  Load( scan, argv[1] );
  Load( photo, argv[2] );
  if (  param == P_3 )
    scan_new= TransfHistMatch( photo, scan );
  else {              // param == P_5
    photo_sgm= new BitMap<unsigned char>;
    scan_sgm= new BitMap<unsigned char>;
    LoadSgm( scan_sgm, argv[4] );
    LoadSgm( photo_sgm, argv[5] );
    if (  scan->W()  != scan_sgm->W()   &&  scan->H()  != scan_sgm->H()  ) {
        printf( "\nMask %s has another size than %s", argv[4], argv[1] );
        return;
      };
    if (  photo->W() != photo_sgm->W()  &&  photo->H() != photo_sgm->H()  ) {
      printf( "\nMask %s has another size than %s", argv[5], argv[2] );
      return;
    };
    scan_new= TransfHistSpecBcg( photo, scan, photo_sgm, scan_sgm );
    delete photo_sgm;
    delete scan_sgm;
  };
  Save( scan_new, argv[3] );
  printf( " done\n" );
  
  delete photo;
  delete scan;
  delete scan_new;
};
