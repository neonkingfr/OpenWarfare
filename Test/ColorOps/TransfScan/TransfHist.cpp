/*
	TransfHist.cpp
	--------------

	Antonin Malik, duben 2003

	Funkce pro transformaci barev:
	- TransfHistMax()
	- TransfHistSpec()
	- TransfHistMatch()

	Vstup:
  - photo - obrazek vyfoceny fotakem
	- scan  - obrazek nascenovany 3D-scannerem

	Vystup:
	- upraveny "scan", aby barevne odpovidal "photo"

	Poznamky:
  - Obe funkce alokuje pamet pro vystupni obrazek.
	- Radky oznacene /** / by meli byt v komentari slouzi pouze k ladeni
*/


/**///#include <stdio.h>
#include <stdlib.h>
#include <math.h>

/**///#include "tga.h"
#include "TransfHist.h"
#include "..\Shared\SegmentBcg.h"


#define N_HIST					256
//Pri vysi hodnote prahu muzou byt nektere hrany velmi tenkte, nebo se muzou
//uplne ztracet, to se deje hlavne u obrazku z fotaku.
//Pri nizke hodnote prahu dochazi ke vzniku skvrnek i o velikosti nekolika pixelu
//experimenty byly delane s 90
#define H_EDGE_PHOTO    80       
#define H_EDGE_SCAN			80


#define MAX(a,b) ( ((a)>(b)) ? (a) : (b) )
#define MIN(a,b) ( ((a)<(b)) ? (a) : (b) )


//HistPix* hist_pix - slouzi pro transformaci ve funkci TransfHistMatch,
//jako tri pole pixelu
typedef struct HistPix {
	Pixel r, g, b;
} HistPix;


//---------------------------   Pomocne fce.   ---------------------------------------


//zaokrouhleni na cela cisla
int round( float x )
{
  return (int) floor(x + 0.5);
};


//zkopirovani obrazku
BitMap<Color>* Copy( const BitMap<Color>* bmp )
{
	int i, j;
	BitMap<Color>* bmp_new= new BitMap<Color>;

	//zkopirovani obrazku
	bmp_new->Create( bmp->W(), bmp->H() );
	for ( i= 0; i<bmp_new->H(); i++ ) {
		for ( j= 0; j<bmp_new->W(); j++ ) {
			bmp_new->Set( i, j, bmp->Get( i, j ) );
		};
	};

	return bmp_new;
};


//pomocna funkce - ulozi do souboru transformace
void SaveTransf( Color* transf )
{
  int i;
  FILE* f= fopen( "tranfs.txt", "w" );

/*
  fprintf( f, "R:              G:              B:\n" );
  for ( i= 0; i < N_HIST; i++ ) {
    fprintf( f, "%3d ---> %3d   %3d ---> %3d   %3d ---> %3d\n",
		  i, transf[i].r, i, transf[i].g, i, transf[i].b );
	};
*/
  for ( i= 0; i < N_HIST; i++ ) {
    fprintf( f, "%3d %3d %3d\n", transf[i].r, transf[i].g, transf[i].b );
	};
  fclose( f );
};


//vrati maxima histogramu
//vstup : hist
//vystup: max - hodnota maxima, ind - index maxima
//D - vzdalenost od zacatku v niz nehledam maximum, takove maimum by
//    odpovidalo cerne barve (barva pozadi), tomu se chci vyhnout
void GetMaxHist( Hist* hist, Hist& max, Color& ind, int D= 20 )
{
	int i;

	max.r= max.g= max.b= 0;
	for ( i= D; i<N_HIST; i++ ) {
    if (  hist[i].r > max.r  ) {
			max.r= hist[i].r;
      ind.r= i;
    };
    if (  hist[i].g > max.g  ) {
			max.g= hist[i].g;
      ind.g= i;
    };
    if (  hist[i].b > max.b  ) {
			max.b= hist[i].b;
      ind.b= i;
    };
	};
};


//vytvori obrazek s histogrami
//vystup: BitMap - obrazek, v nemz jsou ulozeny vsechny tri histogramy
BitMap<Color>* SaveHist( const char* file_name, Hist* hist )
{
	int s;
	int i, j, h, w;
	Hist max, max2;
	Color p, ind, ind2;
	BitMap<Color>* bmp= new BitMap<Color>;

	//zjistim potrebnou vysku obrazku
	GetMaxHist( hist, max, ind, 0 );		//prohledavam od zacatku
	GetMaxHist( hist, max2, ind2 ); 		//hledam skutecne maximu (bez cerne barvy pozadi)
	h= max.r + max.g + max.b;
	w= N_HIST;
	s= h/700;
	bmp->Create( w, h/s+1 );
	//R
	p.r= 255;
	p.g= p.b= 0;
	for ( j= 0; j < w; j++ ) {
		for ( i= max.r-1; i>=max.r-hist[j].r; i= i-s )	{
			bmp->Set( i/s, j, p );
		};
	};
  //maximum
	p.r= p.g= p.b= 0;
  for ( i= max.r-1; i>=max.r-hist[ind2.r].r; i= i-s )	
		bmp->Set( i/s, ind2.r, p );
	//G
	p.g= 255;
	p.r= p.b= 0;
	for ( j= 0; j < w; j++ ) {
		for ( i= max.r+max.g-1; i>=max.r+max.g-hist[j].g; i= i-s )	{
			bmp->Set( i/s, j, p );
		};
	};
  //maximum
	p.r= p.g= p.b= 0;
	for ( i= max.r+max.g-1; i>=max.r+max.g-hist[ind2.g].g; i=i-s )
		bmp->Set( i/s, ind2.g, p );
	//B
	p.b= 255;
	p.r= p.g= 0;
	for ( j= 0; j < w; j++ ) {
		for ( i= max.r+max.g+max.b-1; i>=max.r+max.g+max.b-hist[j].b; i=i-s )	{
			bmp->Set( i/s, j, p );
		};
	};
  //maximum
	p.r= p.g= p.b= 0;
	for ( i= max.r+max.g+max.b-1; i>=max.r+max.g+max.b-hist[ind2.b].b; i=i-s )	
		bmp->Set( i/s, ind2.b, p );
	//save
	/**///Save( bmp, file_name );
	
	return bmp;
};


//vrati histogramy obrazku, funkce alokuje pamet
//vystup: hist_(r,g,b) - histogram obrazku bmp
Hist* GetHist( const BitMap<Color>* bmp )
{
	int i, j;
	Color p;
	Hist* hist= (Hist *) malloc( N_HIST * sizeof(Hist) );

	for ( i= 0; i<N_HIST; i++ )
		hist[i].r= hist[i].g= hist[i].b= 0;

	for ( i= 0; i<bmp->H(); i++ ) {
		for ( j= 0; j<bmp->W(); j++ ) {
			p= bmp->Get( i, j );
			hist[p.r].r++;
			hist[p.g].g++;
			hist[p.b].b++;
		};
	};

	return hist;
};


//upravi histogram 'hist', aby byl celkovy pocet pixelu roven 'n_scan', zatim je roven 'n_photo'
//zachovavam pri tom pomer barev v histogramu
//a pritom, aby pomer barev zustal zachovan
//n_photo - celkovy pocet pixelu v 'photo'
//n_scan  - celkovy pocet pixelu v 'scan'
void AdaptHist( Hist* hist, int n_photo, int n_scan )
{
	float c= (float) n_scan / (float) n_photo;
	int i;
	Hist n_dif;

	n_dif.r= n_dif.g= n_dif.b= 0;
	for ( i= 0; i<N_HIST; i++ ) {
		hist[i].r= (int) (c * (float) hist[i].r);
		hist[i].g= (int) (c * (float) hist[i].g);
		hist[i].b= (int) (c * (float) hist[i].b);
		n_dif.r+= hist[i].r;
		n_dif.g+= hist[i].g;
		n_dif.b+= hist[i].b;
	};
	//n_scan >= n_dif
	n_dif.r= n_scan - n_dif.r;			       
	n_dif.g= n_scan - n_dif.g;			       
	n_dif.b= n_scan - n_dif.b;			       
	//zyvajici pixely priradim barvam, ktere jsou prvni, spravne bych je mel 
	//priradit tem, ktere mely nejvetsi zbytek (musel bych tridit a pamatovat si zbytky)
	//n_dif < N_HIST
	for (	i= 0; i<n_dif.r; i++ )        
		hist[i].r++;
	for (	i= 0; i<n_dif.g; i++ )        
		hist[i].g++;
	for (	i= 0; i<n_dif.b; i++ )        
		hist[i].b++;
};


//vlastni transformace obrazku
//vstup  - bmp     - obrazek, ktery se pretransformuje podle tranformacni fce. transf
//       - transft - transformacni fce..
//vystup - bmp     - pretransformovany obrazek
void Transform( BitMap<Color>* bmp, Color* transf )
{
  int i, j;
	Color p;

  //samotna transformace
  for ( i= 0; i<bmp->H(); i++ ) {
    for ( j= 0; j<bmp->W(); j++ ) {
			p= bmp->Get( i, j );
			p.r= transf[p.r].r;
			p.g= transf[p.g].g;
			p.b= transf[p.b].b;
			bmp->Set( i, j, p );
    };
  };
};


/*
//---------------------------   TransfHistMax   ---------------------------------------


//vyhlazeni histogram - provadi se, nez se zacne hledat maximum
void SmoothHist( Hist*& hist )
{
	const int K= 5;           //okoli pro vyhlazovani
	int i, j, k, sum_r, sum_g, sum_b;
	Hist* hist_new= (Hist *) malloc( N_HIST * sizeof(Hist) );

	for ( i= 0; i<N_HIST; i++ ) { 
		if (  i < K  ) {						//na okrajich vyhlazuju v mensim okoli
			k= i;
		}
		else {
		  if (  i > N_HIST-K-1  )
			  k= N_HIST - i - 1;
			else
				k= K;
		};
		sum_r= sum_g= sum_b= 0;
		for ( j= i-k; j<=i+k; j++ ) {
			sum_r+= hist[j].r;
			sum_g+= hist[j].g;
			sum_b+= hist[j].b;
		};
		hist_new[i].r= sum_r / (2*k+1);
		hist_new[i].g= sum_g / (2*k+1);
		hist_new[i].b= sum_b / (2*k+1);
	};
	free( hist );
	hist= hist_new;
};


//vrati transformacni fci
//transformace - zlomeni
Color* GetTransf1( Hist* hist_1, Hist* hist_2 )
{
	int i;
	float k_r, k_g, k_b;
	Color* transf= (Color *) malloc( N_HIST * sizeof(Color) );
	Hist max;
	Color ind_1, ind_2;

  GetMaxHist( hist_1, max, ind_1 );
  GetMaxHist( hist_2, max, ind_2 );

	//1. cast
	transf[0].r= transf[0].g= transf[0].b= 0;
	k_r= ((float) ind_2.r) / ((float) ind_1.r);
	k_g= ((float) ind_2.g) / ((float) ind_1.g);
	k_b= ((float) ind_2.b) / ((float) ind_1.b);
	//R
	for ( i= 1; i<ind_1.r; i++ ) 
		transf[i].r= round(k_r*i);
	transf[ind_1.r].r= ind_2.r;
	//G
	for ( i= 1; i<ind_1.g; i++ )
		transf[i].g= round(k_g*i);
	transf[ind_1.g].g= ind_2.g;
	//B
	for ( i= 1; i<ind_1.b; i++ )
		transf[i].b= round(k_b*i);
	transf[ind_1.b].b= ind_2.b;

	//2. cast
  k_r= ((float) (N_HIST-ind_2.r-1)) / ((float) (N_HIST-ind_1.r-1));
  k_g= ((float) (N_HIST-ind_2.g-1)) / ((float) (N_HIST-ind_1.g-1));
  k_b= ((float) (N_HIST-ind_2.b-1)) / ((float) (N_HIST-ind_1.b-1));
	//R
  for ( i= ind_1.r+1; i<N_HIST; i++ )
    transf[i].r= round( k_r * ((float) (i-ind_1.r)) + (float) ind_2.r );
	//G
  for ( i= ind_1.g+1; i<N_HIST; i++ )
    transf[i].g= round( k_g * ((float) (i-ind_1.g)) + (float) ind_2.g );
	//B
  for ( i= ind_1.b+1; i<N_HIST; i++ )
    transf[i].b= round( k_b * ((float) (i-ind_1.b)) + (float) ind_2.b );
	transf[N_HIST-1].r= transf[N_HIST-1].g= transf[N_HIST-1].b=
	  (unsigned char) (N_HIST-1);

	return transf;
};


//vrati transformacni fci
//jednoduche posunuti
Color* GetTransf2( Hist* hist_1, Hist* hist_2 )
{
	int i;
	Hist k;
	Color s, e;
	Color* transf= (Color *) malloc( N_HIST * sizeof(Color) );
	Hist max;
	Color ind_1, ind_2;

  GetMaxHist( hist_1, max, ind_1 );
  GetMaxHist( hist_2, max, ind_2 );
	//1. cast
	k.r= ind_1.r-ind_2.r;
	k.g= ind_1.g-ind_2.g;
	k.b= ind_1.b-ind_2.b;
	s.r= MAX( k.r, 0 );
	s.g= MAX( k.g, 0 );
	s.b= MAX( k.b, 0 );
	for ( i= 0; i<=s.r; i++ )
		transf[i].r= 0;
	for ( i= 0; i<=s.g; i++ )
		transf[i].g= 0;
	for ( i= 0; i<=s.b; i++ )
		transf[i].b= 0;
	//2. cast
	e.r= MIN( N_HIST+k.r-1, N_HIST-1 );
	e.g= MIN( N_HIST+k.g-1, N_HIST-1 );
	e.b= MIN( N_HIST+k.b-1, N_HIST-1 );
	for ( i= s.r; i<=e.r; i++ )
		transf[i].r= i - k.r;
	for ( i= s.g; i<=e.g; i++ )
		transf[i].g= i - k.g;
	for ( i= s.b; i<=e.b; i++ )
		transf[i].b= i - k.b;
	//3. cast
	for ( i= e.r; i<N_HIST; i++ )
		transf[i].r= N_HIST-1;
	for ( i= e.g; i<N_HIST; i++ )
		transf[i].g= N_HIST-1;
	for ( i= e.b; i<N_HIST; i++ )
		transf[i].b= N_HIST-1;

	return transf;
};


//vrati transformacni fci
//kombinace zlomeni a jednoduche posunuti
Color* GetTransf3( Hist* hist_1, Hist* hist_2 )
{
	int i;
	float k;
	Color p;
	Color* transf= (Color *) malloc( N_HIST * sizeof(Color) );
	Hist max;
	Color ind_1, ind_2;

  GetMaxHist( hist_1, max, ind_1 );
  GetMaxHist( hist_2, max, ind_2 );
	transf[0].r= transf[0].g= transf[0].b= 0;

	//R
	if (  ind_1.r > ind_2.r  ) {
		//1. cast - zlomeni
		k= ((float) ind_2.r) / ((float) ind_1.r);
		for ( i= 1; i<ind_1.r; i++ ) 
			transf[i].r= round(k*i);
		//2. cast - posunuti
		p.r= ind_1.r - ind_2.r;
	  for ( i= ind_1.r; i<N_HIST; i++ ) 
			transf[i].r= i - p.r;
	}
	else {
		//1. cast - posunuti
		p.r= ind_2.r - ind_1.r;
		for ( i= 0; i<=ind_1.r; i++ )
			transf[i].r= i + p.r;
		//2. cast - zlomeni
		k= ((float) (N_HIST-ind_2.r-1)) / ((float) (N_HIST-ind_1.r-1));		
	  for ( i= ind_1.r+1; i<N_HIST; i++ )
		  transf[i].r= round( k * ((float) (i-ind_1.r)) + (float) ind_2.r );
	};

	//G
	if (  ind_1.g > ind_2.g  ) {
		//1. cast - zlomeni
		k= ((float) ind_2.g) / ((float) ind_1.g);
		for ( i= 1; i<ind_1.g; i++ ) 
			transf[i].g= round(k*i);
		//2. cast - posunuti
		p.g= ind_1.g - ind_2.g;
	  for ( i= ind_1.g; i<N_HIST; i++ ) 
			transf[i].g= i - p.g;
	}
	else {
		//1. cast - posunuti
		p.g= ind_2.g - ind_1.g;
		for ( i= 0; i<=ind_1.g; i++ )
			transf[i].g= i + p.g;
		//2. cast - zlomeni
		k= ((float) (N_HIST-ind_2.g-1)) / ((float) (N_HIST-ind_1.g-1));		
	  for ( i= ind_1.g+1; i<N_HIST; i++ )
		  transf[i].g= round( k * ((float) (i-ind_1.g)) + (float) ind_2.g );
	};

	//B
	if (  ind_1.b > ind_2.b  ) {
		//1. cast - zlomeni
		k= ((float) ind_2.b) / ((float) ind_1.b);
		for ( i= 1; i<ind_1.b; i++ ) 
			transf[i].b= round(k*i);
		//2. cast - posunuti
		p.b= ind_1.b - ind_2.b;
	  for ( i= ind_1.b; i<N_HIST; i++ ) 
			transf[i].b= i - p.b;
	}
	else {
		//1. cast - posunuti
		p.b= ind_2.b - ind_1.b;
		for ( i= 0; i<=ind_1.b; i++ )
			transf[i].b= i + p.b;
		//2. cast - zlomeni
		k= ((float) (N_HIST-ind_2.b-1)) / ((float) (N_HIST-ind_1.b-1));		
	  for ( i= ind_1.b+1; i<N_HIST; i++ )
		  transf[i].b= round( k * ((float) (i-ind_1.b)) + (float) ind_2.b );
	};

	return transf;
};
*/

//samotna transformace barev
//void TransfMax( BitMap<Color>* bmp, Hist* hist_photo, Hist* hist_scan )
//{
//  Color* transf= GetTransf3( hist_scan, hist_photo );

//  /**///printf( "Transformace spocteny.\n" );
//	Transform( bmp, transf );
//	SaveTransf( transf );

//	free( transf );
//};


//Transformace histogramu - Maximum histogramu obrazku ze scaneru se transformuje 
//na maximum histogramu z fotaku.
//Transformace pouze meni paletu.
//Transormace se automaticky ulozi do transf.txt.
//Funkce alokuje pamet pro vystupni obrazek.
//photo   - obrazek vyfoceny fotakem
//scan    - obrazek nascenovany 3D-scannerem
//return: - upraveny "scan", aby barevne odpovidal "photo"
//BitMap<Color>* TransfHistMax( const BitMap<Color>* photo, const BitMap<Color>* scan )
//{
//	Hist* hist_photo= GetHist( photo );
//	Hist* hist_scan = GetHist( scan );
//	/**///Hist* hist_scan_new;
//	BitMap<Color>* scan_new= Copy( scan );

//	/**///SaveHist( "kura_foto_hist.tga", hist_photo );
//	/**///SaveHist( "kura_scan_hist.tga", hist_scan );
//	//SmoothHist( hist_photo );
//	//SmoothHist( hist_scan );
//	/**///SaveHist( "cd_foto_hist_s.tga", hist_photo );
//	/**///SaveHist( "cd_scan_hist_s.tga", hist_scan );
//	/**///printf( "Histogramy spocteny.\n" );

//  TransfMax( scan_new, hist_photo, hist_scan );
//  /**///printf( "Transformace probehla.\n" );

//	/**///hist_scan_new= GetHist( scan_new );
//	/**///SaveHist( "kura_new_hist.tga", hist_scan_new );

//	//uvolneni pameti
//	free( hist_photo );
//	free( hist_scan );
//	/**///free( hist_scan_new );

//	return scan_new;
//};


//---------------------------   TransfHistSpec   ---------------------------------------


//kumuluje histogram - potreba pro transformaci
void CumulateHist( Hist* hist )
{
	int i;

	for ( i=1; i < N_HIST; i++ ) {
		hist[i].r+= hist[i-1].r;
		hist[i].g+= hist[i-1].g;
		hist[i].b+= hist[i-1].b;		
	};
};


//histogram specification - spocte transformacni fci.
Color* HistogramSpecification( const Hist* hist_photo, const Hist* hist_scan )
{
	int i, j;
	Color* transf= (Color *) malloc( N_HIST * sizeof(Color) );

	transf[0].r= transf[0].g= transf[0].b= 0;
	//R
	j= 0;
	for ( i= 0; i<N_HIST; i++ ) {
		while (  hist_scan[j].r < hist_photo[i].r  ) {
			j++;
			transf[j].r= i;
		};
	};
	for ( j=j+1; j<N_HIST; j++ )     //dodelam zbytek
		transf[j].r= j;
	//G
	j= 0;
	for ( i= 0; i<N_HIST-1; i++ ) {
		while (  hist_scan[j].g < hist_photo[i].g  ) {
			j++;
			transf[j].g= i;
		};
	};
	for ( j=j+1; j<N_HIST; j++ )     //dodelam zbytek
		transf[j].g= j;
	//B
	j= 0;
	for ( i= 0; i<N_HIST-1; i++ ) {
		while (  hist_scan[j].b < hist_photo[i].b  ) {
			j++;
			transf[j].b= i;
		};
	};
	for ( j=j+1; j<N_HIST; j++ )     //dodelam zbytek
		transf[j].b= j;

	return transf;
};


//Transformace histogramu. Funkce alokuje pamet pro vystupni obrazek.
//photo   - obrazek vyfoceny fotakem
//scan    - obrazek nascenovany 3D-scannerem
//return: - upraveny "scan", aby barevne odpovidal "photo"
BitMap<Color>* TransfHistSpec( const BitMap<Color>* photo, const BitMap<Color>* scan )
{
	Hist* hist_photo= GetHist( photo );
	Hist* hist_scan = GetHist( scan );
	BitMap<Color>* scan_new= Copy( scan );
  Color* transf;

	//upravim velikost histogramu
	AdaptHist( hist_photo, photo->H()*photo->W(), scan->H()*scan->W() );

	//kumuluju hist.
	CumulateHist( hist_photo );
	CumulateHist( hist_scan );    //hist_photo[N_HIST-1] == hist_scan[N_HIST]
	//spocitam transf.
	transf= HistogramSpecification( hist_photo, hist_scan );

	//provedu vlastni transf.
  Transform( scan_new, transf );
	SaveTransf( transf );	

	//uvolneni pameti
	free( hist_photo );
	free( hist_scan );
	free( transf );

	return scan_new;
};


//---------------------------   TransfHistMatch   ---------------------------------------


//nastaveni seznamu pixelu
HistPix* SetList( const BitMap<Color>* scan )
{
	HistPix* hist_pix= (HistPix *) malloc( scan->H()*scan->W() * sizeof(HistPix) );  //seznam pixelu
	Hist* hist_cur= (Hist *) malloc( N_HIST * sizeof(Hist) );		//slouzi k naplneni seznamu pixelu
	Hist* hist_scan= GetHist( scan );
	Color p;
	int i, j;

	//nastaveni seznamu pixelu
	hist_cur[0].r= hist_cur[0].g= hist_cur[0].b= 0;
	for ( i= 1; i<N_HIST; i++ ) {
		hist_cur[i].r= hist_cur[i-1].r + hist_scan[i-1].r;
		hist_cur[i].g= hist_cur[i-1].g + hist_scan[i-1].g;
		hist_cur[i].b= hist_cur[i-1].b + hist_scan[i-1].b;
	};
	for ( i= 0; i<scan->H(); i++ ) {
		for ( j= 0; j<scan->W(); j++ ) {
			p= scan->Get( i, j );
			hist_pix[ hist_cur[p.r].r ].r.row=  		
			hist_pix[ hist_cur[p.g].g ].g.row=  			
			hist_pix[ hist_cur[p.b].b ].b.row= i; 		
			hist_pix[ hist_cur[p.r].r++ ].r.col=  		
			hist_pix[ hist_cur[p.g].g++ ].g.col=  			
			hist_pix[ hist_cur[p.b].b++ ].b.col= j; 		
		};
	};
	free( hist_cur );
	free( hist_scan );

	return hist_pix;
};


//zmena seznamu pixelu
//return: hist_transf, ukazuje do hist_pix
Hist* ChangeList( const BitMap<Color>* photo, const int n_scan )
{
	int i;
	Hist* hist_transf= (Hist *) malloc( N_HIST * sizeof(Hist) );  //transformace - ukazuje do hist_pix
	Hist* hist_dest= GetHist( photo );				//tvar ciloveho histogramu

	AdaptHist( hist_dest, photo->H()*photo->W(), n_scan );

	hist_transf[0].r= hist_transf[0].g= hist_transf[0].b= 0;
	for ( i= 1; i<N_HIST; i++ ) {
		hist_transf[i].r= hist_transf[i-1].r + hist_dest[i-1].r;
		hist_transf[i].g= hist_transf[i-1].g + hist_dest[i-1].g;
		hist_transf[i].b= hist_transf[i-1].b + hist_dest[i-1].b;
	};
	free( hist_dest );

	return hist_transf;
};


//transformace histogramu - Histogram obrazku ze scaneru se transformuje na histogram
//obrazku z fotaku. Pomer barev u vysledneho obrazku je tedy stejny jako u obrazku
//z fotaku (histogramy si prakticky uplne odpovidaji)
//funkce alokuje pamet pro vystupni obrazek
//tranformace probiha podle hist_pix a hist_transf
//photo   - obrazek vyfoceny fotakem
//scan    - obrazek nascenovany 3D-scannerem
//return: - upraveny "scan", aby barevne odpovidal "photo"
BitMap<Color>* TransfMatch( const BitMap<Color>* scan, const HistPix* hist_pix,
													  const Hist* hist_transf )
{
	int n_scan= scan->H() * scan->W();
	BitMap<Color>* scan_new= new BitMap<Color>;
	Color p;
	int i, j;

	scan_new->Create( scan->W(), scan->H() );
	//prochazim histogram v podobe hist_transf
	for ( i= 0; i<N_HIST-1; i++ ) {
		for ( j= hist_transf[i].r; j<hist_transf[i+1].r; j++ ) {
			p= scan_new->Get( hist_pix[j].r.row, hist_pix[j].r.col );
			p.r= (unsigned char) i;
			scan_new->Set( hist_pix[j].r.row, hist_pix[j].r.col, p );
		};
		for ( j= hist_transf[i].g; j<hist_transf[i+1].g; j++ ) {
			p= scan_new->Get( hist_pix[j].g.row, hist_pix[j].g.col );
			p.g= (unsigned char) i;
			scan_new->Set( hist_pix[j].g.row, hist_pix[j].g.col, p );
		};
		for ( j= hist_transf[i].b; j<hist_transf[i+1].b; j++ ) {
			p= scan_new->Get( hist_pix[j].b.row, hist_pix[j].b.col );
			p.b= (unsigned char) i;
			scan_new->Set( hist_pix[j].b.row, hist_pix[j].b.col, p );
		};
	};
	//posledni barva
  for ( j= hist_transf[i].r; j<n_scan; j++ ) {
		p= scan_new->Get( hist_pix[j].r.row, hist_pix[j].r.col );
		p.r= (unsigned char) i;
		scan_new->Set( hist_pix[j].r.row, hist_pix[j].r.col, p );
	};
	for ( j= hist_transf[i].g; j<n_scan; j++ ) {
		p= scan_new->Get( hist_pix[j].g.row, hist_pix[j].g.col );
		p.g= (unsigned char) i;
		scan_new->Set( hist_pix[j].g.row, hist_pix[j].g.col, p );
	};
	for ( j= hist_transf[i].b; j<n_scan; j++ ) {
		p= scan_new->Get( hist_pix[j].b.row, hist_pix[j].b.col );
		p.b= (unsigned char) i;
		scan_new->Set( hist_pix[j].b.row, hist_pix[j].b.col, p );
	};

	return scan_new;
};


//transformace histogramu, funkce alokuje pamet pro vystupni obrazek
//photo   - obrazek vyfoceny fotakem
//scan    - obrazek nascenovany 3D-scannerem
//return: - upraveny "scan", aby barevne odpovidal "photo"
BitMap<Color>* TransfHistMatch( const BitMap<Color>* photo, const BitMap<Color>* scan )
{
	HistPix* hist_pix= SetList( scan );
	Hist* hist_transf= ChangeList( photo, scan->H()*scan->W() );

	BitMap<Color>* scan_new= TransfMatch( scan, hist_pix, hist_transf );

	return scan_new;
};


//---------------------------   TransfHistSpecBcg   ------------------------------------


//vrati histogramy objektu, ktery je vyznaceny maskou
//vstup : - bmp - obrazek, jehoz histgram chci zjistit
//        - sgm - maska, 1 znamena objekt, 0 je pozadi
//vystup: - hist_(r,g,b) - histogram obrazku bmp
//        - n            - velikost objektu v obrazku
Hist* GetHistBcg( const BitMap<Color>* bmp, const BitMap<unsigned char>* sgm, int& n )
{
	int i, j;
	Color p;
	Hist* hist= (Hist *) malloc( N_HIST * sizeof(Hist) );

	for ( i= 0; i<N_HIST; i++ )
		hist[i].r= hist[i].g= hist[i].b= 0;

	n= 0;
	for ( i= 0; i<bmp->H(); i++ ) {
		for ( j= 0; j<=bmp->W(); j++ ) {
      if ( sgm->Get( i, j ) ) {
  			p= bmp->Get( i, j );
	  		hist[p.r].r++;
		  	hist[p.g].g++;
			  hist[p.b].b++;
			  n++;
      };
		};
	};

	return hist;
};


/*
//vlastni transformace obrazku, transformuji pouze barvy objektu, pozadi nastavim
//na cernou
//vstup  - bmp     - obrazek, ktery se pretransformuje podle tranformacni fce. transf
//       - transft - transformacni fce..
//vystup - bmp     - pretransformovany obrazek
void TransformBcg( BitMap<Color>* bmp, Pixel bmp_lt, Pixel bmp_rd, Color* transf )
{
  int i, j;
	Color p, black;

	black.r= black.g= black.b= 250;
  //samotna transformace
  for ( i= 0; i<bmp->H(); i++ ) {
    for ( j= 0; j<bmp->W(); j++ ) {
			if (  i >= bmp_lt.row  &&  i <= bmp_rd.row  &&
				    j >= bmp_lt.col  &&  j <= bmp_rd.col  ) {
				p= bmp->Get( i, j );
				p.r= transf[p.r].r;
				p.g= transf[p.g].g;
				p.b= transf[p.b].b;
				bmp->Set( i, j, p );
			}
			else
				bmp->Set( i, j, black );
    };
  };
};
*/


//vlastni transformace obrazku, transformuje pouze pixely masky, pixely pozadi nastavi na 0
//vstup  - bmp     - obrazek, ktery se pretransformuje podle tranformacni fce. transf
//       - transft - transformacni fce..
//vystup - bmp     - pretransformovany obrazek
void TransformBcg( BitMap<Color>* bmp, const BitMap<unsigned char>* sgm, Color* transf )
{
  int i, j;
  Color p, black;

  black.r= black.g= black.b= 0;
  //samotna transformace
  for ( i= 0; i<bmp->H(); i++ ) {
    for ( j= 0; j<bmp->W(); j++ ) {
      if ( sgm->Get( i, j ) ) {
        p= bmp->Get( i, j );
        p.r= transf[p.r].r;
        p.g= transf[p.g].g;
        p.b= transf[p.b].b;
        bmp->Set( i, j, p );
      }
      else
        bmp->Set( i, j, black );
    };
  };
};


//Jako fce 'TransfHistSpec', ale nejdriv odsegmentuje pozadi a dal uz pracuje jen
//s objektem
//photo   - obrazek vyfoceny fotakem
//scan    - obrazek nascenovany 3D-scannerem
//return: - upraveny "scan", aby barevne odpovidal "photo"
BitMap<Color>* TransfHistSpecBcg( const BitMap<Color>* photo, const BitMap<Color>* scan,
                                  const BitMap<unsigned char>* photo_sgm, const BitMap<unsigned char>* scan_sgm )
{
	int n_photo, n_scan;
	Hist* hist_photo= GetHistBcg( photo, photo_sgm, n_photo );
	Hist* hist_scan = GetHistBcg( scan, scan_sgm, n_scan );
	BitMap<Color>* scan_new= Copy( scan );
  Color* transf;

	//upravim velikost histogramu, oba histogramy musi mit stejny pocet pixelu
	AdaptHist( hist_photo, n_photo, n_scan );

	//kumuluju hist.
	CumulateHist( hist_photo );
	CumulateHist( hist_scan );    //hist_photo[N_HIST-1] == hist_scan[N_HIST]
	//spocitam transf.
	transf= HistogramSpecification( hist_photo, hist_scan );

	//provedu vlastni transf.
  TransformBcg( scan_new, scan_sgm, transf );
	SaveTransf( transf );	

	//uvolneni pameti
	free( hist_photo );
	free( hist_scan );
	free( transf );

	return scan_new;
};



/*! tato cast pada
//---------------------------   TransfHistMatchBcg   ------------------------------------


//upravi obrazek 'photo', na misto pozadi nastavi cernou barvu
BitMap<Color>* PreparePhoto( const BitMap<Color>* bmp, const BitMap<unsigned char>* sgm )
{
	int i, j;
	Color black;
	BitMap<Color>* prp= new BitMap<Color>;

	prp->Create( bmp->W(), bmp->H() );
	black.r= black.g= black.b= 0;
	for ( i= 0; i<sgm->H(); i++ ) {
		for ( j= 0; j<sgm->W(); j++ ) {
			if ( sgm->Get( i, j ) )
				prp->Set( i, j, bmp->Get( i, j ) );
			else
				prp->Set( i, j, black );
		};
	};
	return prp;
};


//nastaveni seznamu pixelu
HistPix* SetListBcg( const BitMap<Color>* scan, const BitMap<unsigned char>* scan_sgm,
                     int& scan_n, int& mask_n )
{
  scan_n= scan->W() * scan->H();
	Hist* hist_scan= GetHistBcg( scan, scan_sgm, mask_n );
	HistPix* hist_pix= (HistPix *) malloc( scan_n * sizeof(HistPix) );  //seznam pixelu
	Hist* hist_cur= (Hist *) malloc( N_HIST * sizeof(Hist) );		//slouzi k naplneni seznamu pixelu
	Color p;
	int i, j;

	//nastaveni seznamu pixelu
	hist_cur[0].r= hist_cur[0].g= hist_cur[0].b= 0;
	for ( i= 1; i<N_HIST; i++ ) {
		hist_cur[i].r= hist_cur[i-1].r + hist_scan[i-1].r;
		hist_cur[i].g= hist_cur[i-1].g + hist_scan[i-1].g;
		hist_cur[i].b= hist_cur[i-1].b + hist_scan[i-1].b;
	};
  mask_n= 0;
	for ( i= 0; i<=scan->H(); i++ ) {
    for ( j= 0; j<=scan->W(); j++ ) {
      if ( scan_sgm->Get( i, j ) ) {
        p= scan->Get( i, j );
        hist_pix[ hist_cur[p.r].r ].r.row=  		
        hist_pix[ hist_cur[p.g].g ].g.row=  			
        hist_pix[ hist_cur[p.b].b ].b.row= i; 		
        hist_pix[ hist_cur[p.r].r++ ].r.col=  		
        hist_pix[ hist_cur[p.g].g++ ].g.col=  			
        hist_pix[ hist_cur[p.b].b++ ].b.col= j; 		
        mask_n++;
      };
    };
	};
	free( hist_cur );
	free( hist_scan );

	return hist_pix;
};


//zmena seznamu pixelu
//return: hist_transf, ukazuje do hist_pix
Hist* ChangeListBcg( const BitMap<Color>* photo, const BitMap<unsigned char>* photo_sgm,  const int scan_n )
{
	int i, photo_n;
	Hist* hist_transf= (Hist *) malloc( N_HIST * sizeof(Hist) );  //transformace - ukazuje do hist_pix
	Hist* hist_dest= GetHistBcg( photo, photo_sgm, photo_n );				//tvar ciloveho histogramu

	AdaptHist( hist_dest, photo_n, scan_n );

	hist_transf[0].r= hist_transf[0].g= hist_transf[0].b= 0;
	for ( i= 1; i<N_HIST; i++ ) {
		hist_transf[i].r= hist_transf[i-1].r + hist_dest[i-1].r;
		hist_transf[i].g= hist_transf[i-1].g + hist_dest[i-1].g;
		hist_transf[i].b= hist_transf[i-1].b + hist_dest[i-1].b;
	};
	free( hist_dest );

	return hist_transf;
};


//transformace histogramu - Histogram obrazku ze scaneru se transformuje na histogram
//obrazku z fotaku. Pomer barev u vysledneho obrazku je tedy stejny jako u obrazku
//z fotaku (histogramy si prakticky uplne odpovidaji)
//funkce alokuje pamet pro vystupni obrazek
//tranformace probiha podle hist_pix a hist_transf
//photo   - obrazek vyfoceny fotakem
//scan    - obrazek nascenovany 3D-scannerem
//return: - upraveny "scan", aby barevne odpovidal "photo"
BitMap<Color>* TransfMatchBcg( const BitMap<Color>* scan, const HistPix* hist_pix,
													     const Hist* hist_transf, const int scan_n )
{
	BitMap<Color>* scan_new= new BitMap<Color>;
	Color p;
	int i, j;

	scan_new->Create( scan->W(), scan->H() );
	//prochazim histogram v podobe hist_transf
	for ( i= 0; i<N_HIST-1; i++ ) {
		for ( j= hist_transf[i].r; j<hist_transf[i+1].r; j++ ) {
			p= scan_new->Get( hist_pix[j].r.row, hist_pix[j].r.col );
			p.r= (unsigned char) i;
			scan_new->Set( hist_pix[j].r.row, hist_pix[j].r.col, p );
		};
		for ( j= hist_transf[i].g; j<hist_transf[i+1].g; j++ ) {
			p= scan_new->Get( hist_pix[j].g.row, hist_pix[j].g.col );
			p.g= (unsigned char) i;
			scan_new->Set( hist_pix[j].g.row, hist_pix[j].g.col, p );
		};
		for ( j= hist_transf[i].b; j<hist_transf[i+1].b; j++ ) {
			p= scan_new->Get( hist_pix[j].b.row, hist_pix[j].b.col );
			p.b= (unsigned char) i;
			scan_new->Set( hist_pix[j].b.row, hist_pix[j].b.col, p );
		};
	};
	//posledni barva
  for ( j= hist_transf[i].r; j<scan_n; j++ ) {
		p= scan_new->Get( hist_pix[j].r.row, hist_pix[j].r.col );
		p.r= (unsigned char) i;
		scan_new->Set( hist_pix[j].r.row, hist_pix[j].r.col, p );
	};
	for ( j= hist_transf[i].g; j<scan_n; j++ ) {
		p= scan_new->Get( hist_pix[j].g.row, hist_pix[j].g.col );
		p.g= (unsigned char) i;
		scan_new->Set( hist_pix[j].g.row, hist_pix[j].g.col, p );
	};
	for ( j= hist_transf[i].b; j<scan_n; j++ ) {
		p= scan_new->Get( hist_pix[j].b.row, hist_pix[j].b.col );
		p.b= (unsigned char) i;
		scan_new->Set( hist_pix[j].b.row, hist_pix[j].b.col, p );
	};

	return scan_new;
};


//Jako fce 'TransfHistSpec', ale nejdriv odsegmentuje pozadi a dal uz pracuje jen
//s objektem
//photo   - obrazek vyfoceny fotakem
//scan    - obrazek nascenovany 3D-scannerem
//return: - upraveny "scan", aby barevne odpovidal "photo"
BitMap<Color>* TransfHistMatchBcg( const BitMap<Color>* photo, const BitMap<Color>* scan,
                 const BitMap<unsigned char>* photo_sgm, const BitMap<unsigned char>* scan_sgm )
{
	int scan_n, mask_n;
	HistPix* hist_pix= SetListBcg( scan, scan_sgm, scan_n, mask_n );
	Hist* hist_transf= ChangeListBcg( photo, photo_sgm, scan_n );
	BitMap<Color>* scan_new= TransfMatchBcg( scan, hist_pix, hist_transf, scan_n );


//  BitMap<Color>* scan_new= new BitMap<Color>;
//  scan_new->Create( scan->W(), scan->H() );



	free( hist_pix );
	free( hist_transf );

	return scan_new;
};
*/
