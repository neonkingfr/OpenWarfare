/*
  Segmentation.cpp
  ----------------

  Tonda Malik, srpen 2003

  Segmentace objektu od pozadi.
*/

#include <stdio.h>
#include <string.h>

#include "..\Shared\tga.h"
#include "..\Shared\SegmentBcg.h"


//pro parametry programu
#define P_WRONG                 0             //chybne parametry, nebo chybny pocet parametru
#define P_2                     2             //2 parametry
#define P_3                     3             //3 parametry 


//check arguments
int CheckArguments( int argc, char* argv[] )
{
  if (  argc != 3  &&  argc != 4  ) {
    printf( "Segmentation: Wrong number of parameters.\n" );
    return P_WRONG;
  };
  if (  strstr( argv[1], ".tga" ) == NULL  ) {
    printf( "Segmentation: The 1. parametr (input file name) isn't in TARGA format (*.tga).\n" );
    return P_WRONG;
  };
  if (  strstr( argv[2], ".tga" ) == NULL  ) {
    printf( "Segmentation: The 2. parametr (output file name) isn't in TARGA format (*.tga).\n" );
    return P_WRONG;
  };
  if (  argc == 4  ) {
    if (  strcmp( argv[3], "-s" )  ) {
      printf( "Segmentation: The 3. parametr is not '-s' and will be ignored.\n" );
      return P_WRONG;
    }
    else
      return P_3;
  };
  return P_2;    
};


//main
//the program must have 2 parametrs - input and output file
void main( int argc, char *argv[] )
{
  int param= CheckArguments( argc, argv );

  if (  param == P_WRONG  ) 
    return;

  Pixel lt, rd;
  BitMap<Color>* bmp= new BitMap<Color>;
  BitMap<unsigned char>* sgm= new BitMap<unsigned char>;

  printf( "Segmentation of '%s' is running ...", argv[1] );
  Load( bmp, argv[1] );
  if (  param == P_2  )
    sgm= SegmentBcg( bmp, lt, rd );
  else       //param == P_3
    sgm= SegmentBcgSqr( bmp, lt, rd );
  SaveSgm( sgm, argv[2] );
  printf( " done\n" );

  delete bmp;
  delete sgm;
};
