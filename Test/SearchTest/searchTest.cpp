#include <stdlib.h>
#include <stddef.h>
#include <stdio.h>
#include <windows.h>
#include <xmmintrin.h>
#include <emmintrin.h>

struct TrialType
{
  int key;
  //char data[256-4];
  bool operator<(const TrialType &x) const {return key<x.key; }
  //bool operator>(const TrialType &x) const {return a>x.a; }
  bool operator==(const TrialType &x) const {return key==x.key; }
};


int BinarySearch(TrialType *array, int key, int num)
{
  int left=0, right=num-1;
  int ix;
  do {
    ix=(left+right)/2;
    if (key<array[ix].key) right=ix;
    else left=ix;
  } while(array[ix].key!=key && left<right-1);
  if (array[ix].key==key) return ix;
  if (array[right].key==key) return right;
  return -1;
}

void TrashCache()
{
  const int trashSize = 2000000;
  static volatile char trash[trashSize];
  for (int i=0; i<trashSize; i++)
  {
    trash[i] = char(i);
  }
}
#ifdef _DEBUG
const int MaxItems = 1024;
const int TestStep = 16;
#else
const int MaxItems = 4096;
const int TestStep = 1;
#endif

TrialType array[MaxItems][MaxItems];

void Test0(int j, int numItems)
{
}

void Test1(int j, int numItems)
{
  int key = j&(numItems-1);
  BinarySearch(array[j], key, numItems);
}

void Test2(int j, int numItems)
{
  int key = j&(numItems-1);
  //sequentiall search - simple
  TrialType *ix = array[j];
  int ii = numItems;
  while (--ii>=0)
  {
    if (ix->key>=key) break;
    ++ix;
  }
}

void Test3(int j, int numItems)
{
  int key = j&(numItems-1);
  //sequentiall search - block + prefetch
  TrialType *ix = array[j];
  #if 1
  const int block = 32/sizeof(TrialType);
  const int sseBlock = sizeof(__m128i)/sizeof(TrialType);
  const int scanBlock = numItems/block;
  int ii = scanBlock;
  int scan = ii*block;
  int rest = numItems-ii*block;
  __m128i keyQ = _mm_set1_epi32(key);
  int index = -1;
  while (--ii>=0)
  {
    //_mm_prefetch((const char *)(ix+block*2), _MM_HINT_T0);
    //if (!(ix->key<key)) goto Break;
    __m128i match0 = _mm_cmplt_epi32(*(__m128i *)(ix+0*sseBlock),keyQ);
    __m128i match1 = _mm_cmplt_epi32(*(__m128i *)(ix+1*sseBlock),keyQ);
      //int anyMatch = _mm_movemask_epi8(match);
    unsigned anyMatch = (unsigned)_mm_movemask_epi8(match0)|((unsigned)_mm_movemask_epi8(match1)<<16);
    if (anyMatch!=~0)
    {
      // check which one is correct
      index = (scanBlock-ii-1)*block;
      // we could calculate index directly here?
      while (anyMatch>0)
      {
        index++;
        anyMatch>>=4;
      }
      goto Break;
    }
    ix += block;
  }
  #else
  const int block = sizeof(__m128i)/sizeof(TrialType);
  const int scanBlock = numItems/block;
  int ii = scanBlock;
  int scan = ii*block;
  int rest = numItems-ii*block;
  __m128i keyQ = _mm_set1_epi32(key);
  int index = -1;
  while (--ii>=0)
  {
    //_mm_prefetch((const char *)(ix+block), _MM_HINT_T0);
    //if (!(ix->key<key)) goto Break;
    // unroll the loop
    __m128i match = _mm_cmplt_epi32(*(__m128i *)ix,keyQ);
    int anyMatch = _mm_movemask_ps(*(__m128 *)&match);
    if (anyMatch!=0xf)
    {
      // check which one is correct
      index = (scanBlock-ii-1)*block;
      while (anyMatch>0)
      {
        index++;
        anyMatch>>=1;
      }
      goto Break;
    }
    ix += block;
  }
  #endif
  while (--rest>=0)
  {
    if (ix->key>=key)
    {
      index = numItems-rest-1;
      break;
    }
    ++ix;
  }
  Break:;
  // search rest of the data
  #if _DEBUG
    if (index!=key)
    {
      __asm int 3;
    }
  #endif
}

int main()
{
  #ifndef _DEBUG
    SetPriorityClass(GetCurrentProcess(),REALTIME_PRIORITY_CLASS);
	  SetThreadPriority(GetCurrentThread(), THREAD_PRIORITY_HIGHEST);
    // calm down
    Sleep(3000);
    TrashCache();
  #endif
  //create sorted array
  for (int t=0; t<MaxItems; t++)
  for (int i=0; i<MaxItems; i++)
  {
    array[t][i].key=i;
  }
  LARGE_INTEGER timeStart, timeEnd;

  double diffBin, diffSeq1, diffSeq2;
  LARGE_INTEGER pcf;
  QueryPerformanceFrequency(&pcf);
  double invPcFreq = 1e6/pcf.QuadPart;

  // measure overhead
  double overhead = 0;
  const int nOverhead = 1000;
  for (int i=0; i<nOverhead; i++)
  {
    QueryPerformanceCounter( &timeStart );
    Test0(0,0);
    QueryPerformanceCounter( &timeEnd );
    overhead += double(timeEnd.QuadPart-timeStart.QuadPart)*invPcFreq;
  }
  overhead /= nOverhead;

  const int stepMul = 2;
  for (int numItems=2; numItems<=MaxItems; numItems*= stepMul)
  {
    Test1(numItems/2,numItems);
    Test2(numItems/2,numItems);
    Test3(numItems/2,numItems);
  }

  for (int numItems=2; numItems<=MaxItems; numItems*= stepMul)
  {
    TrashCache();
    //binary search
    QueryPerformanceCounter( &timeStart );
    for (int j=0; j<MaxItems; j+=TestStep)
    {
      Test1(j,numItems);
    }
    QueryPerformanceCounter( &timeEnd );
    diffBin = double(timeEnd.QuadPart-timeStart.QuadPart)*invPcFreq-overhead;
    //sequentiall search
    TrashCache();
    QueryPerformanceCounter( &timeStart );
    for (int j=0; j<MaxItems; j+=TestStep)
    {
      Test2(j,numItems);
    }
    QueryPerformanceCounter( &timeEnd );
    diffSeq1 = double(timeEnd.QuadPart-timeStart.QuadPart)*invPcFreq-overhead;
    TrashCache();
    QueryPerformanceCounter( &timeStart );
    for (int j=0; j<MaxItems; j+=TestStep)
    {
      Test3(j,numItems);
    }
    QueryPerformanceCounter( &timeEnd );
    diffSeq2 = double(timeEnd.QuadPart-timeStart.QuadPart)*invPcFreq-overhead;
    printf(
      "items=%4d  size=%3d  seq1: %6.0fus  seq2: %6.0fus bin: %6.0fus\n",
      numItems, sizeof(TrialType), diffSeq1, diffSeq2, diffBin
    );
  }
  printf("Press Enter...\n");
  getchar();
  return 0;
}

