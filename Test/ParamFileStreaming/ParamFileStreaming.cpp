// CfgConvert.cpp : Defines the entry point for the console application.
//

#include <El/elementpch.hpp>
#include <El/ParamFile/paramFile.hpp>

#include <string.h>
#include <stdio.h>


int consoleMain(int argc, const char* argv[])
{
	// initialization of Element library
	//InitParamFilePreprocess();
	//GGameState.Init();

	argv++;
	argc--;
	if (argc < 1) goto Usage;
	while (argc>0)
	{
		const char *arg = *argv++;
		argc--;
		if (*arg == '-' || *arg == '/')
		{
			arg++; // skip dash
			goto Usage;
		}
		else
		{
			ParamFile f;
			ParamClassOwnerFilename *source = new ParamClassOwnerFilename("",&f,arg);
			if (!f.ParseBinOrTxt(arg,source))
			{
				fprintf(stderr,"Error reading text file '%s'\n",arg);
				continue;
			}
			// we will try some loading / unloading
			//f.UnloadAll();
			const ParamEntryVal &aaa = f>>"aaa";
			printf("aaa=%s\n",(const char *)(aaa>>"name").GetValue());
			const ParamEntryVal &bbb = f>>"bbb";
			printf("bbb=%s\n",(const char *)(bbb>>"name").GetValue());
			const ParamEntryVal &ccc = f>>"ccc";
			printf("ccc=%s\n",(const char *)(ccc>>"name").GetValue());
			const ParamEntryVal &cccSub = ccc>>"CccSubClass";
			printf("cccSub=%s\n",(const char *)(cccSub>>"name").GetValue());
			
			const ParamEntryVal &cccSubSub = ccc>>"CccSubClass">>"CccSubSubBase";
			printf("cccSubSub=%s\n",(const char *)(cccSubSub>>"name").GetValue());
		}
	}
	#if _DEBUG
	return 1;
	#else
	return 0;
	#endif

Usage:
	printf("Usage\n");
	printf("   ParamFileStreaming {<source>}\n");
	return 1;		
}
