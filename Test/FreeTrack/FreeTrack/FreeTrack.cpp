/************************************************************************
 *	freetrack_c_interface.c
 *	
 *  A simple command line application which reads the data from FreeTrack
 *	using the FreeTrackClient.dll interface.
 *
 *	Assumes that a copy of the FreeTrackClient.dll is in the same folder,
 *	thought this does not necessarily have to be the same folder as the 
 *	FreeTrack application itself.
 *
 *	Based on code from http://en.wikipedia.org/wiki/Dynamic-link_library
 *
 *	Alastair Moore, December 2007
 *
 ************************************************************************/

//#include <iostream>
//#include <tchar.h>
#include <windows.h>
#include <stdio.h>
#include <conio.h>

typedef struct
{
	float yaw;
	float pitch;
	float roll;
	float x;
	float y;
	float z;
	int dataID;
  char buf[1024];
}FreeTrackData;

typedef struct  
{
  unsigned int DataID;
  int CamWidth;
  int CamHeight;
  // virtual pose
  float Yaw;   // positive yaw to the left
  float Pitch; // positive pitch up
  float Roll;  // positive roll to the left
  float X;
  float Y;
  float Z;
  // raw pose with no smoothing, sensitivity, response curve etc. 
  float RawYaw;
  float RawPitch;
  float RawRoll;
  float RawX;
  float RawY;
  float RawZ;
  // raw points, sorted by Y, origin top left corner
  float X1;
  float Y1;
  float X2;
  float Y2;
  float X3;
  float Y3;
  float X4;
  float Y4;
} FreeTrackData2;

// DLL function signatures
// These match those given in FTTypes.pas
// WINAPI is macro for __stdcall defined somewhere in the depths of windows.h
typedef bool (WINAPI *importGetData)(FreeTrackData2 * data);
typedef char *(WINAPI *importGetDllVersion)(void);
typedef void (WINAPI *importReportName)(int name);
typedef char *(WINAPI *importProvider)(void);

int main(int argc, char **argv)
{
		//declare imported function pointers
    importGetData getData;
		importGetDllVersion getDllVersion;
		importReportName	reportName;
		importProvider provider;

		// create variables for exchanging data with the dll
    FreeTrackData data;
    FreeTrackData2 data2;
		FreeTrackData *pData;
		pData = &data;
		char *pDllVersion;
		int name = 453;
		char *pProvider;


        // Load DLL file
        HINSTANCE hinstLib = LoadLibrary("FreeTrackClient.dll");
        if (hinstLib == NULL) {
                printf("ERROR: unable to load DLL\n");
                return 1;
        }
		else
		{
			printf("dll loaded\n");
		}

        // Get function pointers
    getData = (importGetData)GetProcAddress(hinstLib, "FTGetData");
		getDllVersion = (importGetDllVersion)GetProcAddress(hinstLib, "FTGetDllVersion");
		reportName = (importReportName)GetProcAddress(hinstLib, "FTReportName");
		provider = (importProvider)GetProcAddress(hinstLib, "FTProvider");

		// Check they are valid
        if (getData == NULL) {
                printf("ERROR: unable to find 'FTGetData' function\n");
               FreeLibrary(hinstLib);
                return 1;
        }
		if (getDllVersion == NULL){
				printf("ERROR: unable to find 'FTGetDllVersion' function\n");
               FreeLibrary(hinstLib);
                return 1;
		}
		if (reportName == NULL){
				printf("ERROR: unable to find 'FTReportName' function\n");
               FreeLibrary(hinstLib);
                return 1;
		}
		if (reportName == NULL){
				printf("ERROR: unable to find 'FTProvider' function\n");
               FreeLibrary(hinstLib);
                return 1;
		}

		//	Print the address of each function
		printf("FTGetData is at address: 0x%x\n",getData);
		printf("FTGetDllVersion is at address: 0x%x\n",getDllVersion);
		printf("FTReportName is at address: 0x%x\n",reportName);
		printf("FTProvider is at address: 0x%x\n",provider);

		//	Call each function and display result
		pDllVersion = getDllVersion();
		printf("Dll Version: %s\n", pDllVersion);

		pProvider = provider();
		printf("Provider: %s\n", pProvider);
		
		reportName(name);	//not sure what this does - I guess it tells the dll that I am using it.
		
		system("pause"); //wait till keyboard is pressed before entering main loop
		while( kbhit() != 1)
		{
			system("cls"); //clear screen
      if (getData(&data2))
			{
				printf("Record ID: %d\n" , data2.DataID );
				printf("Yaw: %5.2f\n" , data2.Yaw );
        printf("Pitch: %5.2f\n" , data2.Pitch );
				printf("Roll: %5.2f\n" , data2.Roll );
				printf("X: %5.2f\n" , data2.X );
				printf("Y: %5.2f\n" , data2.Y );
				printf("Z: %5.2f\n" , data2.Z );
			}
			else
			{
				printf("Nothing returned from getData\n");
				break;
			}
		}

        // Unload DLL file
        FreeLibrary(hinstLib);

		return 0;
}

