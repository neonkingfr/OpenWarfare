@ECHO OFF

Rem Global settings (path to appmap differs on WinXP and Vindows Vista)
set appmapExe="C:\Program Files (x86)\Microsoft Product Feedback Mapping Tool\appmap.exe"

if [%6]==[] GOTO Help
goto CreateMappingXML 

:Help
echo Usage: 
echo  UploadWinQual.bat sourceDir pathToXMLOutput productName productVer user passwd
echo    sourceDir ... path to directory containing EXE files to upload
echo    pathToXMLOutput ... mapping file XML destination
echo    productName ... product name [such as "ArmA 2"]
echo    productVer ... product version [such as "1.4.50000"]
echo    user ... username to login to WinQual
echo    passwd ... password to login to WinQual
echo Example:
echo  UploadWinQual.bat myArmAFiles arma2WQ.xml "ArmA 2" 1.4.60150 bebul heslo 
goto Fail

:CreateMappingXML
echo %appmapExe% -s %1 -d %2 -n %3 -v %4
%appmapExe% -s %1 -d %2 -n %3 -v %4 -f 
if NOT EXIST %2 goto Fail

rem upload XML file to WinQual 
echo WinQualClient.exe -upload -u %5 -p %6 -f %2
echo on
WinQualClient.exe -upload -u %5 -p %6 -f %2
goto End

:Fail
Rem Something has gone wrong
echo Failed: the XML was not created or upload failed.
exit /b 1

:End
