UploadWinQual.bat servers to automate WinQual Mapping File creation 
and upload to WinWual Microsoft site.
ErrorLevel 1 is set on FAIL.

Steps to use it:
1. create a directory and copy there the EXE files which are to be tracked by WinQual
  * such as MyProductFolder
2. trigger the command line:
  UploadWinQual.bat MyProductFolder outputXMLFile productName productVersion userName password
 where:
  MyProductFolder is a directory mentioned in step 1
  outputXMLFile is path to output file XML which is to be created and uploaded to WinQual
  productName is for instance "ArmA 2"
  productVersion is for instance 1.4.60141
  userName is name to login into WinQual site, such as bebul
  password is the password needed to connect to WinQual

Example:
 UploadWinQual.bat armaTest winQualMapping.xml "arma 2" "1.4.60141" bebul secretPasswd

NOTICE: it uses WinQualClient, which is C# application compiled from 
  https://dev.bistudio.com/svn/pgm/trunk/Test/WinQualClient
