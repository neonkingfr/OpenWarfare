using System;

namespace winqualmapping
{
  class Program
  {
    static void UploadMapping(string username, string password, string fileName)
    {
      WinQualClient client = new WinQualClient();
      if (client.Login(username, password))
      {
        client.UploadMapping(fileName);
      }
    }

    static void PrintUsage()
    {
      Console.Out.WriteLine("Usage:");
      Console.Out.WriteLine("winqual-client <command> <options>");
      Console.Out.WriteLine("Options:");
      Console.Out.WriteLine("  -upload");
      Console.Out.WriteLine("  -u <username>");
      Console.Out.WriteLine("  -p <password>");
      Console.Out.WriteLine("  -f <mapping-file>");
      Console.Out.WriteLine("For example to upload 'product.xml' use the following command:");
      Console.Out.WriteLine("  winqual-client -upload -u test -p secret -f product.xml");
    }

    static void Main(string[] args)
    {
      string username = null;
      string password = null;
      string command = null;
      string fileName = null;

      try
      {
        for (int i = 0; i < args.Length; i++)
        {
          if (args[i].StartsWith("-") || args[i].StartsWith("/"))
          {
            string option = args[i].Substring(1);
            switch (option.ToLower())
            {
              case "upload":
                command = "upload";
                break;
              case "?":
              case "h":
                PrintUsage();
                return;
              case "u":
                username = args[++i];
                break;
              case "p":
                password = args[++i];
                break;
              case "f":
                fileName = args[++i];
                break;
              default:
                throw new Exception(string.Format("Unknown option '{0}'", option));
            }
          }
        }

        CheckRequredArg(command, "Please specify command. Run 'winqual-client /?' for more information.");

        switch (command.ToLower())
        {
          case "upload":
            CheckRequredArg(username, "Please specify username using '-u' option.");
            CheckRequredArg(password, "Please specify password using '-p' option.");
            CheckRequredArg(fileName, "Please specify mapping file using '-f' option.");
            UploadMapping(username, password, fileName);
            break;
        }
      }
      catch (Exception e)
      {
        Console.Error.WriteLine(e.Message);
        return;
      }
    }

    private static void CheckRequredArg(string arg, string message)
    {
      if (arg == null)
      {
        throw new Exception(message);
      }
    }
  }
}
