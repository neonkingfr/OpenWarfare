﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Collections;
using System.Text;
using System.Net;
using System.IO;
using System.Xml.Serialization;
using System.Xml;
using System.Diagnostics;
using System.Security;
using Microsoft.WindowsErrorReporting.Services.Data.API;
using Microsoft.WindowsErrorReporting.Services.Mapping.API;

[XmlRoot("GetBasicTicketResponse", Namespace = "https://winqual.microsoft.com/Services/Authentication/")]
public class GetBasicTicketResponse
{
    [XmlElement("GetBasicTicketResult")]
    public GetBasicTicketResult Result;
}

public class GetBasicTicketResult
{
    [XmlText]
    public string Ticket;

    [XmlAttribute("nil", Namespace = "http://www.w3.org/2001/XMLSchema-instance")]
    public bool IsNull = false;
}

[XmlRoot("feed", Namespace = "http://www.w3.org/2005/Atom")]
public class AtomFeed
{
    [XmlAttribute("status", Namespace="http://schemas.microsoft.com/windowserrorreporting")]
    public string Status;

    [XmlElement("entry")]
    public FeedEntry[] Entries;
}

public class FeedEntry
{
    [XmlElement("title")]
    public string Title;
}

namespace winqualmapping
{
    class WinQualClient
    {
        HttpLiveClient liveClient;

        public bool Login(string winqualUsername, string winqualPassword)
        {
          liveClient = null;

          //
          // The service requires 2 parameters - 
          // - the username 
          // - the password
          // in this order.
          //

          if (string.IsNullOrEmpty(winqualUsername) == true)
          {
            //
            // log the error and return false;
            //
            this.LogException(
                new ArgumentNullException(
                    "username"
                    , "The username is either null or empty."
                    )
                );

            return false;
          }

          if (string.IsNullOrEmpty(winqualPassword) == true)
          {
            //
            // log the error and return false;
            //
            this.LogException(
                new ArgumentNullException(
                    "password"
                    , "The password is either null or empty."
                    )
                );

            return false;
          }

          SecureString ss = new SecureString();
          winqualPassword.ToList().ForEach((c) => ss.AppendChar(c));

          // Attempt login the credentials to ensure they are 
          // valid at service startup. If the credentials
          // are not valid, the service will stop.
          try
          {
            liveClient = new HttpLiveClient(winqualUsername, ss);
            liveClient.EnsureLogin();
          }
          catch (Exception ex)
          {
            //
            // log the error and return false;
            //
            this.LogException(ex);
            return false;
          }

          return true;
        }

        public void UploadMapping(string fileName)
        {
          FileUpload fileUpload = new FileUpload(fileName);
          fileUpload.Upload(liveClient);
        }

        /// <summary>
        /// Method to populate the unhandled exception in the event log.
        /// </summary>
        /// <param name="ex">Exception object to populate the log with.</param>
        private void LogException(Exception ex)
        {
          Exception baseException = ex.GetBaseException();
          StringBuilder exceptionMessage = new StringBuilder();
          exceptionMessage.AppendFormat(
              "Exception: Type - {0}, Message - {1}, Source - {2}, StackTrace - {3}, TargetSite - {4}"
                  , baseException.GetType().ToString()
                  , baseException.Message
                  , baseException.Source
                  , baseException.StackTrace
                  , baseException.TargetSite
          );

          if (baseException.Data.Count > 0)
          {
            exceptionMessage.Append(", Data - ");
            foreach (DictionaryEntry de in baseException.Data)
            {
              exceptionMessage.AppendFormat(
                  "Key - {0}, Value - {1} :: "
                  , de.Key
                  , de.Value.ToString()
              );
            }
          }

          Console.Out.WriteLine(exceptionMessage.ToString());
/*
          EventLog.WriteEntry(
              exceptionMessage.ToString()
              , EventLogEntryType.Error
          );
*/
        }
    }
}
