﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net;
using System.IO;
using System.Xml.Serialization;
using System.Xml;

[XmlRoot("GetBasicTicketResponse", Namespace = "https://winqual.microsoft.com/Services/Authentication/")]
public class GetBasicTicketResponse
{
    [XmlElement("GetBasicTicketResult")]
    public GetBasicTicketResult Result;
}

public class GetBasicTicketResult
{
    [XmlText]
    public string Ticket;

    [XmlAttribute("nil", Namespace = "http://www.w3.org/2001/XMLSchema-instance")]
    public bool IsNull = false;
}

[XmlRoot("feed", Namespace = "http://www.w3.org/2005/Atom")]
public class AtomFeed
{
    [XmlAttribute("status", Namespace="http://schemas.microsoft.com/windowserrorreporting")]
    public string Status;

    [XmlElement("entry")]
    public FeedEntry[] Entries;
}

public class FeedEntry
{
    [XmlElement("title")]
    public string Title;
}

namespace winqualmapping
{
    class WinQualClient
    {
        private string baseUrl = "https://winqual.microsoft.com";
        private string encryptedTicket;

        private byte[] LoginRequestBody(string username, string password)
        {
            using (MemoryStream stream = new MemoryStream())
            {
                using (XmlWriter xml = new XmlTextWriter(stream, Encoding.UTF8))
                {
                    xml.WriteStartDocument();
                    xml.WriteStartElement("soap", "Envelope", "http://schemas.xmlsoap.org/soap/envelope/");
                    //xml.WriteAttributeString("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
                    //xml.WriteAttributeString("xmlns:xsd", "http://www.w3.org/2001/XMLSchema");
                    xml.WriteStartElement("soap", "Body", "http://schemas.xmlsoap.org/soap/envelope/");
                    xml.WriteStartElement("GetBasicTicket", "https://winqual.microsoft.com/Services/Authentication/");
                    xml.WriteElementString("userName", username);
                    xml.WriteElementString("password", password);
                    xml.WriteEndElement();
                    xml.WriteEndElement();
                    xml.WriteEndDocument();
                }
                return stream.ToArray();
            }
        }

        private string GetLoginURL()
        {
            return baseUrl + "/services/Authentication/Authentication.svc/BasicTicket";
        }

        private string GetFileUploadURL()
        {
            return baseUrl + "/services/wer/user/fileupload.aspx";
        }

        public void Login(string userName, string password)
        {
            WebClient loginClient = new WebClient();
            loginClient.Headers.Add(HttpRequestHeader.ContentType, "text/xml");
            loginClient.Headers.Add("SOAPAction", "https://winqual.microsoft.com/Services/Authentication/IBasicTicket/GetBasicTicket");

            byte[] request = LoginRequestBody(userName, password);
            byte[] response = loginClient.UploadData(GetLoginURL(), request);

            GetBasicTicketResponse loginResponse = ParseLoginResponse(response);
            if (loginResponse.Result.IsNull)
            {
                throw new Exception("Login failed. Please check username and password.");
            }

            encryptedTicket = loginResponse.Result.Ticket;
        }

        private GetBasicTicketResponse ParseLoginResponse(byte[] loginResponse)
        {
            GetBasicTicketResponse responseObject;
            using (MemoryStream stream = new MemoryStream(loginResponse))
            {
                XmlSerializer s = new XmlSerializer(typeof(GetBasicTicketResponse));
                using (XmlReader xml = new XmlTextReader(stream))
                {
                    xml.ReadStartElement("Envelope", "http://schemas.xmlsoap.org/soap/envelope/");
                    xml.ReadStartElement("Body", "http://schemas.xmlsoap.org/soap/envelope/");
                    responseObject = (GetBasicTicketResponse)s.Deserialize(xml);
                    xml.ReadEndElement();
                    xml.ReadEndElement();
                }
            }

            return responseObject;
        }

        private AtomFeed ParseUploadResponse(byte[] response)
        {
            using (MemoryStream stream = new MemoryStream(response))
            {
                XmlSerializer s = new XmlSerializer(typeof(AtomFeed));
                return (AtomFeed)s.Deserialize(stream);
            }
        }

        public void UploadMapping(string fileName)
        {
            WebClient webClient = new WebClient();
            webClient.Headers.Add("encryptedTicket", encryptedTicket);
            byte[] responseBytes = webClient.UploadFile(GetFileUploadURL(), fileName);

            //
            // the response that the WebClient gets may contain an updated ticket, so get that
            //
            if (webClient.ResponseHeaders["encryptedTicket"] != null)
            {
                encryptedTicket = webClient.ResponseHeaders["encryptedTicket"];
            }

            // string ss = Encoding.UTF8.GetString(responseBytes);
            AtomFeed feed = ParseUploadResponse(responseBytes);
            if (feed.Status != "ok")
            {
                if (feed.Entries.Length > 0)
                {
                    throw new Exception(feed.Entries[0].Title);
                }
                else
                {
                    throw new Exception();
                }
            }
        }
    }
}
