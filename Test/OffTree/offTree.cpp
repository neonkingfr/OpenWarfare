#include <Es/essencepch.hpp>
#include <float.h>
#include <Es/Common/fltopts.hpp>
#include <Es/Strings/bstring.hpp>
#include <Es/Types/pointers.hpp>
#include <Es/Containers/array.hpp>
#include <Es/Containers/offTree.hpp>

//using namespace System;

//using namespace OffsetTreeVisualizer;


static float randN(float n)
{
  // make sure we return the most random bits
  return rand()*(1.0f/RAND_MAX)*n;
}

struct TreeItem: public RefCount
{
  int _id;
  OffTreeRegion _region;
  
  TreeItem(int id, float begX, float begZ, float endX, float endZ)
  :_id(id),_region(begX,begZ,endX,endZ)
  {
  }
  
  bool operator == (const TreeItem &item) const {return _id==item._id;}
  const OffTreeRegion &GetRegion() const {return _region;}
  bool IsIntersection(const OffTreeRegion &q) const {return _region.IsIntersection(q);}
};

TypeIsSimple(TreeItem)

#pragma warning(disable:4584)

template <class Base>
class CountAllocations: private Base, private NoCopy
{
  int count;
  
  public:
  CountAllocations()
  {
    count = 0;
  }
  ~CountAllocations()
  {
    Assert(count==0);
  }
  #if ALLOC_DEBUGGER
  void *Alloc( int &size, const char *file, int line, const char *postfix )
  {
    count++;
    return Base::Alloc(size,file,line,postfix);
  }
  #else
  void *Alloc( int &size )
  {
    count++;
    return Base::Alloc(size);
  }
  #endif
  void Free( void *mem, int size )
  {
    count--;
    Base::Free(mem,size);
  }
  static void Unlink( void *mem ) {}

  static inline const int MinGrow() {return Base::MinGrow();}
};

int consoleMain(int argc, const char *argv[])
{
#if 0
  // 12x12 km, 500 k objects, 100 k queries
  float range = 12000;
  float minObjSize = 1;
  float maxObjSize = 10;
  const int nObj = 500000;
  const int nQuery = 100000;
#elif 1
  // 12x12 km, 100k objects
  float range = 12000;
  float minObjSize = 1;
  float maxObjSize = 10;
  const int nObj = 100000;
  const int nQuery = 10000;
#else
  // 60x60m, 25 objects
  float range = 64;
  float minObjSize = 1;
  float maxObjSize = 10;
  //const int nObj = 25;
  const int nObj = 10;
  const int nQuery = 100;
#endif

#define STATS 1

#if STATS
  typedef OffTree<
    TreeItem, OffTreeStatTraits<TreeItem>,
    CountAllocations< MemAllocSimpleDirect<int,16*1024> >
  > OffTreeType;
#else
  typedef OffTree<
    TreeItem, CountAllocations<MemAllocSimpleDirect< int,16*1024> >
  > OffTreeType;
#endif
  OffTreeType tree(0,0,range);
  
  AutoArray<TreeItem> regs;
  srand(1);
  LogF("Generating %d objects",nObj);
  for (int i=0; i<nObj; i++)
  //for (int i=0; i<40; i++)
  {
    float s = floatMax(randN(maxObjSize),minObjSize);
    float rx = randN(range-s);
    float rz = randN(range-s);

    #if 0 // simulate ideal results
      // align object so that its fits based on its size
      float level = log(range/s)/log(2);
      int levelRound = toIntFloor(level);
      float levelGridSize = range/pow(2.0f,levelRound);
      float levelGridPosX = (rx+s*0.5f)/levelGridSize;
      float levelGridPosZ = (rz+s*0.5f)/levelGridSize;
      
      rx = toIntFloor(levelGridPosX)*levelGridSize+levelGridSize*0.5f-s*0.5f;
      rz = toIntFloor(levelGridPosZ)*levelGridSize+levelGridSize*0.5f-s*0.5f;
    #endif
    
    TreeItem item(i,rx,rz,rx+s,rz+s);
        
    //LogF("Add %g,%g..%g (%g)",rx,rz,rx+s,rz+s,s);
    tree.Add(item);
  	regs.Add(item);
  }
  
  //OffTreeWrapper test(tree);
  //OffTreeVisualizer::TestShowVisualizer(%test);

  
  //tree.Dump();

  LogF("Issuing %d queries",nQuery);
  OffTreeType::QueryResult sumQ;
  for (int i=0; i<nQuery; i++)
  {
    float s = floatMax(randN(maxObjSize),minObjSize);
    float rx = randN(range-s);
    float rz = randN(range-s);

    #if 0 // simulate ideal queries
      // align object so that its fits based on its size
      float level = log(range/s)/log(2);
      int levelRound = toIntFloor(level);
      float levelGridSize = range/pow(2.0f,levelRound);
      float levelGridPosX = (rx+s*0.5f)/levelGridSize;
      float levelGridPosZ = (rz+s*0.5f)/levelGridSize;
      
      rx = toIntFloor(levelGridPosX)*levelGridSize+levelGridSize*0.5f-s*0.5f;
      rz = toIntFloor(levelGridPosZ)*levelGridSize+levelGridSize*0.5f-s*0.5f;
    #endif

    OffTreeType::QueryResult q;
    tree.Query(q,rx,rz,rx+s,rz+s);
    //LogF("Find %g,%g (%g) - cost %d",r,r+s,s,q);
    
    #if STATS
      sumQ += q;
    #endif
  }
  LogF("range %g, objects %d, queries: %d",range,nObj,nQuery);
  #if STATS
  LogF(
    "relBorder %.2f, maxBorder %.0g, Node tests: %g, Region tests: %g",
    tree.GetRelBorder(),tree.GetMaxBorder(),sumQ.nodeTests/float(nQuery),sumQ.regionTests/float(nQuery)
  );
  #endif

  /*
  // remove all items
  for (int i=0; i<regs.Size(); i++)
  {
    tree.Remove(regs[i]);
  }
  */
  return 0;
}

