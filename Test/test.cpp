/*
    @file   test.cpp
    @brief  Test main routine.
    
    Copyright &copy; 2001-2003 by BIStudio (www.bistudio.com)
    @author PE
    @since  29.11.2001
    @date   12.3.2003
*/

//   Define one of the symbols externally (in project file):
//#define JP_TEST_CRITICAL
//#define JP_TEST_SEMAPHORE
//#define JP_TEST_SEMAPHORE_TITBIT
//#define JP_TEST_MAPS_IMPLICIT
//#define JP_TEST_MAPS_EXPLICIT
//#define JP_TEST_MAPS_EXPLICIT_MT
//#define JP_TEST_BITMASK
//#define JP_TEST_CALENDAR

#if defined JP_TEST_CRITICAL         || defined JP_TEST_SEMAPHORE     || \
    defined JP_TEST_SEMAPHORE_TITBIT || defined JP_TEST_MAPS_IMPLICIT || \
    defined JP_TEST_MAPS_EXPLICIT    || defined JP_TEST_BITMASK       || \
    defined JP_TEST_CALENDAR

#include "Es/essencepch.hpp"
#include <stdarg.h>
#include "Es/Framework/appFrame.hpp"

//---------------------------------------------------------------------------
//  Support:

static unsigned64 origin = getSystemTime();

void logF ( const char *format, ... )
{
    va_list arglist;
    va_start(arglist,format);

    unsigned64 now = getSystemTime();
    fprintf(stderr,"%10.5f - ",1.e-6*(now-origin));
    fprintf(stderr,format,arglist);
    fputc('\n',stderr);

    va_end( arglist );
}

bool createSafeHeap ()
{
    return true;
}

void destroySafeHeap ()
{
}

void *safeNew ( size_t size )
{
    return malloc(size);
}

void safeDelete ( void *mem )
{
    free(mem);
}

static char msgBuf[512];

#if _ENABLE_REPORT

void OFPFrameFunctions::LogF ( const char *format, va_list argptr )
{
    sprintf(msgBuf,"LogF: ");
    vsprintf(msgBuf+6,format,argptr);
    logF(msgBuf);
}

void OFPFrameFunctions::LstF ( const char *format, va_list argptr )
{
    sprintf(msgBuf,"LstF: ");
    vsprintf(msgBuf+6,format,argptr);
    logF(msgBuf);
}

void OFPFrameFunctions::ErrF ( const char *format, va_list argptr )
{
    sprintf(msgBuf,"ErrF: ");
    vsprintf(msgBuf+6,format,argptr);
    logF(msgBuf);
}

#endif

void OFPFrameFunctions::ErrorMessage ( const char *format, va_list argptr )
{
    sprintf(msgBuf,"Error: ");
    vsprintf(msgBuf+7,format,argptr);
    logF(msgBuf);
}

void OFPFrameFunctions::ErrorMessage ( ErrorMessageLevel level, const char *format, va_list argptr )
{
    sprintf(msgBuf,"Error(%02d): ",(int)level);
    vsprintf(msgBuf+11,format,argptr);
    logF(msgBuf);
}

void OFPFrameFunctions::WarningMessage ( const char *format, va_list argptr )
{
    sprintf(msgBuf,"Warning: ");
    vsprintf(msgBuf+9,format,argptr);
    logF(msgBuf);
}

void OFPFrameFunctions::ShowMessage ( int timeMs, const char *msg, va_list argptr )
{
    sprintf(msgBuf,"Message(%8.3f): ",1e-3*timeMs);
    vsprintf(msgBuf+19,msg,argptr);
    logF(msgBuf);
}

DWORD OFPFrameFunctions::TickCount ()
{
    return( (DWORD)( (getSystemTime() - origin + 500) / 1000 ) );
}

#endif

//------------------------------------------------------------
//  Portable critical section:

#ifdef JP_TEST_CRITICAL

#define ITER     5000
#define DELAY   50000

volatile int guarded;                       // guarded variable

THREAD_PROC_RETURN THREAD_PROC_MODE add ( void *param )
{
    Ref<PoCriticalSection> cs = (PoCriticalSection*)param;
    int i, j;
    for ( i = 0; i < ITER; i++ ) {
        if ( cs ) cs->enter();
        for ( j = 0; j < DELAY; j++ ) guarded += j;
        for ( j = 0; j < DELAY; j++ ) guarded -= j;
        if ( cs ) cs->leave();
        }
    return (THREAD_PROC_RETURN)i;
}

THREAD_PROC_RETURN THREAD_PROC_MODE multiply ( void *param )
{
    Ref<PoCriticalSection> cs = (PoCriticalSection*)param;
    int i, j;
    for ( i = 0; i < ITER; i++ ) {
        if ( cs ) cs->enter();
        guarded *= 213;
        for ( j = 0; j < DELAY; j++ ) guarded += j;
        for ( j = 0; j < DELAY; j++ ) guarded -= j;
        guarded /= 213;
        if ( cs ) cs->leave();
        }
    return (THREAD_PROC_RETURN)i;
}

int main ( int argc, char **argv )
{
    Ref<PoCriticalSection> cs = new PoCriticalSection;
    ThreadId t1;
    ThreadId t2;
    THREAD_PROC_RETURN ret;
    guarded = 0;
    unsigned64 st = getSystemTime();
    printf("\nSafe run:\n");
    if ( poThreadCreate(&t1,0,&add,(void*)cs.GetRef()) ) {
        printf("'Add' thread was created successfully (%u)\n",(unsigned)t1);
        if ( poThreadCreate(&t2,0,&multiply,(void*)cs.GetRef()) ) {
            printf("'Multiply' thread was created successfully (%u)\n",(unsigned)t2);
            if ( poThreadJoin(t2,&ret) )
                printf("'Multiply' thread has joined successfully with return code %u\n",(unsigned)ret);
            }
        if ( poThreadJoin(t1,&ret) )
            printf("'Add' thread has joined successfully with return code %u\n",(unsigned)ret);
        }
    st = getSystemTime() - st;
    printf("  Guarded variable value = %d (should be 0) [%.5f sec]\n",
           guarded,1.e-6*st);
    
    guarded = 0;
    st = getSystemTime();
    printf("\nUnsafe run:\n");
    if ( poThreadCreate(&t1,0,&add,(void*)NULL) ) {
        printf("'Add' thread was created successfully (%u)\n",(unsigned)t1);
        if ( poThreadCreate(&t2,0,&multiply,(void*)NULL) ) {
            printf("'Multiply' thread was created successfully (%u)\n",(unsigned)t2);
            if ( poThreadJoin(t2,&ret) )
                printf("'Multiply' thread has joined successfully with return code %u\n",(unsigned)ret);
            }
        if ( poThreadJoin(t1,&ret) )
            printf("'Add' thread has joined successfully with return code %u\n",(unsigned)ret);
        }
    st = getSystemTime() - st;
    printf("  Guarded variable value = %d (should be 0) [%.5f sec]\n\n",
           guarded,1.e-6*st);
    return 0;
}

#endif

//------------------------------------------------------------
//  Portable simple semaphore:

#ifdef JP_TEST_SEMAPHORE

#define ITER    100
#define DELAY1  1000000
#define DELAY2  9000000

THREAD_PROC_RETURN THREAD_PROC_MODE producer ( void *param )
{
    Ref<PoSemaphore> sem = (PoSemaphore*)param;
    int i, j;
    for ( i = 0; i < ITER; i++ ) {
        for ( j = 0; j < DELAY1; j++ )
	    i += j;
        for ( j = 0; j < DELAY1; j++ )
	    i -= j;
        printf("P%6d ",i);
        sem->signal();
        }
    return (THREAD_PROC_RETURN)i;
}

THREAD_PROC_RETURN THREAD_PROC_MODE consumer ( void *param )
{
    Ref<PoSemaphore> sem = (PoSemaphore*)param;
    int i, j;
    for ( i = 0; i < ITER; i++ ) {
        for ( j = 0; j < DELAY2; j++ )
	    i += j;
        for ( j = 0; j < DELAY2; j++ )
	    i -= j;
        sem->wait();
        printf("C%6d ",i);
        }
    return (THREAD_PROC_RETURN)i;
}

int main ( int argc, char **argv )
{
    Ref<PoSemaphore> sem = new PoSemaphore(0,144);
    ThreadId prod;
    if ( poThreadCreate(&prod,0,&producer,(void*)(sem.GetRef())) ) {
        printf("\nProducer thread was created successfully (%u)\n",(unsigned)prod);
        THREAD_PROC_RETURN ret;
        ThreadId cons;
        if ( poThreadCreate(&cons,0,&consumer,(void*)(sem.GetRef())) ) {
            printf("\nConsumer thread was created successfully (%u)\n",(unsigned)cons);
            if ( poThreadJoin(cons,&ret) )
                printf("\nConsumer thread has joined successfully with return code %u\n",(unsigned)ret);
            }
        if ( poThreadJoin(prod,&ret) )
            printf("\nProduceer thread has joined successfully with return code %u\n",(unsigned)ret);
        }
    return 0;
}

#endif

//------------------------------------------------------------
//  Portable simple semaphore with titbits:

#ifdef JP_TEST_SEMAPHORE_TITBIT

#define ITER    180
#define DELAY1  1000000
#define DELAY2  9000000

THREAD_PROC_RETURN THREAD_PROC_MODE producer ( void *param )
{
    Ref< PoSemaphoreTitbit<int> > sem = (PoSemaphoreTitbit<int>*)param;
    int i, j;
    for ( i = 0; i++ < ITER; ) {
        for ( j = 0; j < DELAY1; j++ )
	    i += j;
        for ( j = 0; j < DELAY1; j++ )
	    i -= j;
        printf("p%6d ",i);
        sem->signal(i);
        }
    sem->signal(0);
    return (THREAD_PROC_RETURN)(i+1);
}

THREAD_PROC_RETURN THREAD_PROC_MODE consumer ( void *param )
{
    Ref< PoSemaphoreTitbit<int> > sem = (PoSemaphoreTitbit<int>*)param;
    int counter = 0;
    int titbit, j;
    do {
        counter++;
        for ( j = 0; j < DELAY2; j++ )
	    counter += j;
        for ( j = 0; j < DELAY2; j++ )
	    counter -= j;
        sem->wait(titbit);
        if ( !titbit ) break;
        if ( counter == titbit )
            printf("c%6d ",titbit);                 // consumer: OK
        else {
            printf("E%3d%3d ",counter,titbit);      // consumer: error!
            fflush(stdout);
            getchar();
            }
        } while ( true );
    return (THREAD_PROC_RETURN)counter;
}

int main ( int argc, char **argv )
{
    Ref< PoSemaphoreTitbit<int> > sem = new PoSemaphoreTitbit<int>(0,200);
    ThreadId prod;
    if ( poThreadCreate(&prod,0,&producer,(void*)(sem.GetRef())) ) {
        printf("\nProducer thread was created successfully (%u)\n",(unsigned)prod);
        THREAD_PROC_RETURN ret;
        ThreadId cons;
        if ( poThreadCreate(&cons,0,&consumer,(void*)(sem.GetRef())) ) {
            printf("\nConsumer thread was created successfully (%u)\n",(unsigned)cons);
            if ( poThreadJoin(cons,&ret) )
                printf("\nConsumer thread has joined successfully with return code %u\n",(unsigned)ret);
            }
        if ( poThreadJoin(prod,&ret) )
            printf("\nProduceer thread has joined successfully with return code %u\n",(unsigned)ret);
        }
    return 0;
}

#endif

//------------------------------------------------------------
//  ImplicitMap: numeric -> object instance:

#ifdef JP_TEST_MAPS_IMPLICIT

#include <float.h>

//  unsigned -> double map:

unsigned doubleToUnsigned ( double &value )
{
    return (unsigned)(value*31.41592654);
}

double ImplicitMapTraits<double>::null   = DBL_MIN;
double ImplicitMapTraits<double>::zombie = DBL_MIN*(1.0-DBL_EPSILON);

//  unsigned -> Test map:

class Test : public RefCount {

public:

    unsigned a;

    Test ( unsigned init =12 )
    { a = init; }

    ~Test () {}

    };

unsigned testToUnsigned ( Ref<Test> &value )
{
    return value->a;
}

    // complete instantiation of the used maps:
template ImplicitMap<unsigned,double,doubleToUnsigned>;
template ImplicitMap<unsigned,Ref<Test>,testToUnsigned>;

#define CARD    500000
#define PRINTED    100
#define LOAD_F    0.5f

int main ( int argc, char **argv )
{
    printf("\nImplicitMap<unsigned,double>, loadFactor=%.2f:\n",LOAD_F);
    ImplicitMap<unsigned,double,doubleToUnsigned> map(12,LOAD_F);
    int i;
    for ( i = 0; i < CARD; i++ ) {
        map.removeKey((unsigned)(i*1.2345));
        map.removeKey((unsigned)(i*2.3456));
        map.put(i+0.0);
        }
    double item = i - 1.0;
    printf("Card: %d, last item: [%u,%.0f]\n",map.card(),doubleToUnsigned(item),(float)item);
#ifdef MAP_STAT
    printf("Resize=%u get=%u put=%u repl=%u remove=%u zombie=%u collis=%u\n",
           map.statResize,map.statGet,map.statPut,map.statReplace,map.statRemove,map.statZombie,map.statCollision);
#endif
    getchar();
    IteratorState it;
    map.getFirst(it,item);
    i = 0;
    while ( it != ITERATOR_NULL && i < PRINTED ) {
        printf("%8u:%6.0f ",doubleToUnsigned(item),(float)item);
        map.getNext(it,item);
        i++;
        }
    getchar();

    printf("ImplicitMap<unsigned,Ref<Test>>, loadFactor=%.2f:\n",LOAD_F);
    ImplicitMap<unsigned,Ref<Test>,testToUnsigned> mapt(2,LOAD_F);
    for ( i = 0; i < CARD; i++ ) {
        mapt.removeKey(i);
        mapt.removeKey(2*i);
        Ref<Test> x = new Test(i*12);
        mapt.put(x);
        }
    Ref<Test> t;
    mapt.get((CARD-1)*12,t);
    printf("Card: %d, last item: %u\n",mapt.card(),t.IsNull() ? 0 : t->a);
#ifdef MAP_STAT
    printf("Resize=%u get=%u put=%u repl=%u remove=%u zombie=%u collis=%u\n",
           mapt.statResize,mapt.statGet,mapt.statPut,mapt.statReplace,mapt.statRemove,mapt.statZombie,mapt.statCollision);
#endif

    return 0;
}

#endif

//------------------------------------------------------------
//  ExplicitMap: key -> object instance:

#ifdef JP_TEST_MAPS_EXPLICIT

#include <float.h>
#include "common/randomJames.h"

//  unsigned -> double map:

double   ExplicitMapTraits<unsigned,double>::null    = DBL_MIN;
unsigned ExplicitMapTraits<unsigned,double>::keyNull = 0xffffffff;
unsigned ExplicitMapTraits<unsigned,double>::zombie  = 0xfffffffe;

//  unsigned -> Test map:

class Test : public RefCount {

public:

    int a;

    Test ( int init =12 )
    {  a = init; }

    ~Test () {}

    };

unsigned ExplicitMapTraits<unsigned,Ref<Test> >::keyNull = 0xffffffff;
unsigned ExplicitMapTraits<unsigned,Ref<Test> >::zombie  = 0xfffffffe;
unsigned ExplicitMapTraits<unsigned,Ref<Test> >::getHash ( unsigned &key )
{
    return key+1;
}

    // complete instantiation of both used maps:
template ExplicitMap<unsigned,double>;
template ExplicitMap<unsigned,Ref<Test> >;

#define CARD       900000
#define MAX_KEY  10000000
#define PRINTED       100
#define LOAD_F       0.5f

int main ( int argc, char **argv )
{
    printf("\nExplicitMap<unsigned,double>, loadFactor=%.2f:\n",LOAD_F);
    ExplicitMap<unsigned,double> map(12,LOAD_F);
    int i;
    for ( i = 0; i < CARD; i++ ) {
        map.removeKey((unsigned)(i*1.2345));
        map.removeKey((unsigned)(i*2.3456));
        map.put((unsigned)(i*23.3452),i+0.0);
        }
    unsigned key = (unsigned)((i-1)*23.3452);
    double item;
    map.get(key,item);
    printf("Card: %d, last item: [%u,%.0f]\n",map.card(),key,(float)item);
#ifdef MAP_STAT
    printf("Resize=%u get=%u put=%u repl=%u remove=%u zombie=%u coll=%u\n",
           map.statResize,map.statGet,map.statPut,map.statReplace,map.statRemove,map.statZombie,map.statCollision);
#endif
    getchar();
    IteratorState it;
    map.getFirst(it,item,&key);
    i = 0;
    while ( it != ITERATOR_NULL && i < PRINTED ) {
        printf("%8u:%6.0f ",key,(float)item);
        map.getNext(it,item,&key);
        i++;
        }
    getchar();

    printf("ExplicitMap<unsigned,Ref<Test>>, loadFactor=%.2f:\n",LOAD_F);
    ExplicitMap<unsigned,Ref<Test> > mapt(2,LOAD_F);
    RandomJames rnd;
    for ( i = 0; i < CARD/2; i++ ) {
        Ref<Test> x = new Test(i);
        key = (unsigned)(rnd.uniformNumber()*MAX_KEY);
        mapt.put(key,x);
        }
    Ref<Test> t;
    mapt.get(key,t);
    printf("Card: %d, last item: [%u,%d]\n",mapt.card(),key,t->a);
#ifdef MAP_STAT
    printf("Resize=%u get=%u put=%u repl=%u remove=%u zombie=%u coll=%u\n",
           mapt.statResize,mapt.statGet,mapt.statPut,mapt.statReplace,mapt.statRemove,mapt.statZombie,mapt.statCollision);
#endif
    getchar();
    mapt.getFirst(it,t,&key);
    i = 0;
    while ( it != ITERATOR_NULL && i < PRINTED ) {
        printf("%7u:%7d ",key,t->a);
        mapt.getNext(it,t,&key);
        i++;
        }
    getchar();
    for ( i = 0; i++ < CARD; ) {
        key = (unsigned)(rnd.uniformNumber()*MAX_KEY);
        mapt.remove(key);
        key = (unsigned)(rnd.uniformNumber()*MAX_KEY);
        t = new Test(-i);
        mapt.put(key,t);
        }
    printf("Card: %d, last item: [%u,%d]\n",mapt.card(),key,t->a);
#ifdef MAP_STAT
    printf("Resize=%u get=%u put=%u repl=%u remove=%u zombie=%u coll=%u\n",
           mapt.statResize,mapt.statGet,mapt.statPut,mapt.statReplace,mapt.statRemove,mapt.statZombie,mapt.statCollision);
#endif
    getchar();
    mapt.getFirst(it,t,&key);
    i = 0;
    while ( it != ITERATOR_NULL && i < PRINTED ) {
        printf("%7u:%7d ",key,t->a);
        mapt.getNext(it,t,&key);
        i++;
        }
    putchar('\n');

    return 0;
}

#endif

//------------------------------------------------------------
//  BitMask object:

#ifdef JP_TEST_BITMASK

//#define PRIME_ORDER     100000
//#define PRIME_ASSERT    650000

#define PRIME_ORDER     200000
#define PRIME_ASSERT   1400000

//#define PRIME_ORDER     400000
//#define PRIME_ASSERT   2900100

//#define PRIME_ORDER    5555555
//#define PRIME_ASSERT  50000000

int main ( int argc, char **argv )
{
    Ref<BitMask> sieve = new BitMask;
    // only odd numbers are stored in this bit-mask
    // sieve->get(i) ... 2*i-1

        // 1st run: {get|set} (safe):
    unsigned64 st = getSystemTime();
    printf("Computing the %d-th prime number (safe):   ",PRIME_ORDER);
    int n = 2;                          // order of the next prime number to be found (1st prime number is 2)
    int prime = 1;                      // (2*prime-1) will be the next prime number
    do {                                // find the next prime
        do prime++;
        while ( sieve->get(prime) );
        int k = prime;
        int step = prime + prime - 1;
        do {
            sieve->on(k);
            k += step;
            } while ( k <= PRIME_ASSERT );
        } while ( n++ < PRIME_ORDER );
    prime += prime - 1;
    st = getSystemTime() - st;
    printf("%d [%9.5f sec]\n",prime,1.e-6*st);
        // 2nd run: {get|set}unsafe:
    st = getSystemTime();
    printf("Computing the %d-th prime number (unsafe): ",PRIME_ORDER);
    sieve->empty();
    sieve->assert(2);
    sieve->assert(PRIME_ASSERT);
    n = 2;
    prime = 1;
    do {                                // find the next prime
        do prime++;
        while ( sieve->getUnsafe(prime) );
        int k = prime;
        int step = prime + prime - 1;
        do {
            sieve->setUnsafe(k,true);
            k += step;
            } while ( k <= PRIME_ASSERT );
        } while ( n++ < PRIME_ORDER );
    prime += prime - 1;
    st = getSystemTime() - st;
    printf("%d [%9.5f sec]\n",prime,1.e-6*st);
    return 0;
}

#endif

//------------------------------------------------------------
//  PoCalendar object:

#ifdef JP_TEST_CALENDAR

#include "essencepch.hpp"

class StopWatch : public RefCountSafe {

protected:

	Ref<PoCalendar> calendar;				///< Associated calendar instance.

	unsigned eventType;						///< Registered event type for PoCalendar.

public:

	bool running;

	int time;								///< Time in seconds.

	int timeDeci;

	int timeCenti;

	int timeMilli;

	int increment;							///< The smallest increment used (in milliseconds).

	StopWatch ( PoCalendar *cal );
	
	void start ( int inc =0 );

	void stop ();

	size_t print ( char *buf );

	~StopWatch ();

	};

void secondCallback ( CalendarEvent &ev )
	// ev.dataPtr -> StopWatch instance
{
	StopWatch *stop = (StopWatch*)ev.dataPtr;
	Assert( stop );
	if ( !stop->running ) return;
	if ( stop->increment > 1000 ) {
		fprintf(stderr,"secondCallback: Error - too small increment\n");
		return;
		}
	stop->enter();
	stop->time++;
	if ( stop->time != ev.dataInt ) {
		fprintf(stderr,"secondCallback: not synchronized (%d, %d)",stop->time,ev.dataInt);
		}
	stop->leave();
	ev.dataInt++;
}

void deciCallback ( CalendarEvent &ev )
	// ev.dataPtr -> StopWatch instance
{
	StopWatch *stop = (StopWatch*)ev.dataPtr;
	Assert( stop );
	if ( !stop->running ) return;
	if ( stop->increment > 100 ) {
		fprintf(stderr,"deciCallback: Error - too small increment\n");
		return;
		}
	stop->enter();
	if ( ++(stop->timeDeci) > 9 ) stop->timeDeci = 0;
	if ( stop->timeDeci != ev.dataInt ) {
		fprintf(stderr,"deciCallback: not synchronized (%d, %d)",stop->timeDeci,ev.dataInt);
		}
	stop->leave();
	if ( ++ev.dataInt > 9 ) ev.dataInt = 0;
}

void centiCallback ( CalendarEvent &ev )
	// ev.dataPtr -> StopWatch instance
{
	StopWatch *stop = (StopWatch*)ev.dataPtr;
	Assert( stop );
	if ( !stop->running ) return;
	if ( stop->increment > 10 ) {
		fprintf(stderr,"centiCallback: Error - too small increment\n");
		return;
		}
	stop->enter();
	if ( ++(stop->timeCenti) > 9 ) stop->timeCenti = 0;
	if ( stop->timeCenti != ev.dataInt ) {
		fprintf(stderr,"centiCallback: not synchronized (%d, %d)",stop->timeCenti,ev.dataInt);
		}
	stop->leave();
	if ( ++ev.dataInt > 9 ) ev.dataInt = 0;
}

StopWatch::StopWatch ( PoCalendar *cal )
{
	time = timeDeci = timeCenti = timeMilli = 0;
	Assert( cal );
	calendar = cal;
	eventType = calendar->registerType();
	running = false;
}

void StopWatch::start ( int inc )
{
	if ( running ) stop();
	time = timeDeci = timeCenti = timeMilli = 0;
    if ( inc ) increment = inc;
	running = true;
	unsigned64 now = getSystemTime();
	CalendarEventCallback *cb;

		// centi-seconds:
	if ( increment < 100 ) {
		cb = new CalendarEventCallback(&centiCallback);
		Assert( cb );
		cb->dataPtr   = this;
		cb->userType  = eventType;
		cb->time      = now + 10000;
		cb->timeDelta = 10000;
		cb->dataInt   = 1;
		cb->flags    |= EV_REVOLVING_FLAG;
		cb->flags    &= ~EV_ROBUST_FLAG;
		calendar->insertEvent(cb);
		}

		// deci-seconds
	if ( increment < 1000 ) {
		cb = new CalendarEventCallback(&deciCallback);
		Assert( cb );
		cb->dataPtr   = this;
		cb->userType  = eventType;
		cb->time      = now + 100000;
		cb->timeDelta = 100000;
		cb->dataInt   = 1;
		cb->flags    |= EV_REVOLVING_FLAG;
		cb->flags    &= ~EV_ROBUST_FLAG;
		calendar->insertEvent(cb);
		}

		// seconds:
	cb = new CalendarEventCallback(&secondCallback);
	Assert( cb );
	cb->dataPtr   = this;
	cb->userType  = eventType;
	cb->time      = now + 1000000;
	cb->timeDelta = 1000000;
	cb->dataInt   = 1;
	cb->flags    |= EV_REVOLVING_FLAG;
	cb->flags    &= ~EV_ROBUST_FLAG;
	calendar->insertEvent(cb);
}

void StopWatch::stop ()
{
	if ( !running ) return;
	running = false;
	calendar->removeEvents(eventType);
}

size_t StopWatch::print ( char *buf )
{
	Assert( buf );
	enter();
	if ( time >= 3600 )						// HH:MM:SS.mmm
		sprintf(buf,"%2d:%02d:%02d.%01d%01d%01d %c",
			    time/3600,(time%3600)/60,time%60,timeDeci,timeCenti,timeMilli,running?'R':'S');
	else
	if ( time >= 60 )						//    MM:SS.mmm
		sprintf(buf,"%5d:%02d.%01d%01d%01d %c",
		        time/60,time%60,timeDeci,timeCenti,timeMilli,running?'R':'S');
	else									//       SS.mmm
		sprintf(buf,"%8d.%01d%01d%01d %c",
		        time,timeDeci,timeCenti,timeMilli,running?'R':'S');
	leave();
	return strlen(buf);
}

StopWatch::~StopWatch ()
{
	stop();
}

//------------------------------------------------------------
//  stop-watch test:

struct StopWatches {

	Ref<PoCalendar> cal;

	int number;

	Ref<StopWatch> w[16];

	};

void printCallback ( CalendarEvent &ev )
	// ev.dataPtr -> StopWatches record
{
	StopWatches *sw = (StopWatches*)ev.dataPtr;
	Assert( sw );
	int i;
	char buf[256];
	char *ptr = buf;
	for ( i = 0; i < sw->number; ) { // print one stop-watch
		ptr += sw->w[i]->print(ptr);
		if ( ++i < sw->number )
			ptr += sprintf(ptr,",  ");
		}
	printf("\r%s",buf);
}

int main ( int argc, char **argv )
{
	StopWatches sw;
		// create the calendar:
	sw.cal = new PoCalendar(1000,5);
		// create a bunch of stop-watches:
	sw.w[0] = new StopWatch(sw.cal);
	sw.w[0]->increment = 1000;
	sw.w[1] = new StopWatch(sw.cal);
	sw.w[1]->increment =  100;
	sw.w[2] = new StopWatch(sw.cal);
	sw.w[2]->increment =   10;
	sw.number = 3;
		// create and start the print-event:
	CalendarEventCallback *cb = new CalendarEventCallback(&printCallback);
	Assert( cb );
	cb->dataPtr   = &sw;
	cb->userType  = sw.cal->registerType();
	cb->time      = getSystemTime() + 12345;
	cb->timeDelta = 32481;
	cb->flags    |= EV_REVOLVING_FLAG;
	cb->flags    &= ~EV_ROBUST_FLAG;
	sw.cal->insertEvent(cb);
		// start the stop-watches:
	SLEEP_MS(3000);
	sw.w[0]->start();
	SLEEP_MS(4000);
	sw.w[1]->start();
	SLEEP_MS(5000);
	sw.w[2]->start();
	SLEEP_MS(100000);
	sw.w[0]->stop();
	SLEEP_MS(4000);
	sw.w[1]->stop();
	SLEEP_MS(5000);
	sw.w[2]->stop();
	SLEEP_MS(1000);
	sw.w[0]->start();
	SLEEP_MS(1000);
	sw.w[1]->start();
	SLEEP_MS(1000);
	sw.w[2]->start();
		// and finally stop the watches:
	int i;
	for ( i = 0; i < sw.number; i++ )
		sw.w[i]->stop();
	sw.cal->removeAllEvents();

    return 0;
}

#endif

//------------------------------------------------------------
//  ExplicitMap: key -> object instance:

#ifdef JP_TEST_MAPS_EXPLICIT_MT

#include "Es/essencepch.hpp"
#include "Es/Types/pointers.hpp"
#include "Es/Common/global.hpp"
#include "Es/Framework/potime.hpp"
#include "El/Common/randomJames.h"
#include <stdarg.h>
#include "Poseidon/lib/MemHeap.hpp"

//---------------------------------------------------------------------------
//  Support:

static unsigned64 origin = getSystemTime();

void logF ( const char *format, ... )
{
    va_list arglist;
    va_start(arglist,format);

    unsigned64 now = getSystemTime();
    fprintf(stderr,"%10.5f - ",1.e-6*(now-origin));
    fprintf(stderr,format,arglist);
    fputc('\n',stderr);

    va_end( arglist );
}

#define SAFE_HEAP_SIZE   64                 // 64MB of memory

static RandomJames rnd;

#ifdef _WIN32
extern RefD<MemHeapLocked> safeHeap;
#endif

#ifdef NET_LOG_SAFE_HEAP
unsigned safeHeapCounter = 0;
#endif

bool createSafeHeap ()
{
#ifdef _WIN32
    if ( safeHeap ) return true;
    safeHeap = new MemHeapLocked;
    safeHeap->Init("SafeHeap",SAFE_HEAP_SIZE<<20); // X MB of memory
#ifdef NET_LOG_SAFE_HEAP
    NetLog("Initializing SafeHeap: %d MB of memory",SAFE_HEAP_SIZE);
#endif
#endif
    return true;
}

void destroySafeHeap ()
{
#ifdef _WIN32
    if ( !safeHeap ) return;
    safeHeap = NULL;
#endif
}

void *safeNew ( size_t size )
{
#ifdef _WIN32
    if ( !safeHeap ) {
        Verify( createSafeHeap() );
        }
    void *mem = safeHeap->Alloc(size);
#ifdef NET_LOG_SAFE_HEAP
    if ( !(++safeHeapCounter & 0xff) )
        NetLog("safeNew (%s): %u bytes of safe-heap remains (in %u blocks)",
               mem?"success":"fail",(unsigned)safeHeap->TotalFreeLeft(),(unsigned)safeHeap->CountFreeLeft());
#endif
    Assert( mem );
    return mem;
#else
    return malloc(size);
#endif
}

void safeDelete ( void *mem )
{
#ifdef _WIN32
    Assert( safeHeap );
    safeHeap->Free(mem);
#ifdef NET_LOG_SAFE_HEAP
    if ( !(++safeHeapCounter & 0xff) )
        NetLog("safeDelete: %u bytes of safe-heap remains (in %u blocks)",
               (unsigned)safeHeap->TotalFreeLeft(),(unsigned)safeHeap->CountFreeLeft());
#endif
#else
    free(mem);
#endif
}

static char msgBuf[512];

#if _ENABLE_REPORT

void OFPFrameFunctions::LogF ( const char *format, va_list argptr )
{
    sprintf(msgBuf,"LogF: ");
    vsprintf(msgBuf+6,format,argptr);
    //NetLog(msgBuf);
}

void OFPFrameFunctions::LstF ( const char *format, va_list argptr )
{
    sprintf(msgBuf,"LstF: ");
    vsprintf(msgBuf+6,format,argptr);
    NetLog(msgBuf);
}

void OFPFrameFunctions::ErrF ( const char *format, va_list argptr )
{
    sprintf(msgBuf,"ErrF: ");
    vsprintf(msgBuf+6,format,argptr);
    NetLog(msgBuf);
}

#endif

void OFPFrameFunctions::ErrorMessage ( const char *format, va_list argptr )
{
    sprintf(msgBuf,"Error: ");
    vsprintf(msgBuf+7,format,argptr);
    NetLog(msgBuf);
}

void OFPFrameFunctions::ErrorMessage ( ErrorMessageLevel level, const char *format, va_list argptr )
{
    sprintf(msgBuf,"Error(%02d): ",(int)level);
    vsprintf(msgBuf+11,format,argptr);
    NetLog(msgBuf);
}

void OFPFrameFunctions::WarningMessage ( const char *format, va_list argptr )
{
    sprintf(msgBuf,"Warning: ");
    vsprintf(msgBuf+9,format,argptr);
    NetLog(msgBuf);
}

void OFPFrameFunctions::ShowMessage ( int timeMs, const char *msg, va_list argptr )
{
    sprintf(msgBuf,"Message(%8.3f): ",1e-3*timeMs);
    vsprintf(msgBuf+19,msg,argptr);
    NetLog(msgBuf);
}

DWORD OFPFrameFunctions::TickCount ()
{
    static unsigned64 origin = getSystemTime();
    return( (DWORD)( (getSystemTime() - origin + 500) / 1000 ) );
}

//  unsigned -> Test map (multi-thread-safe):

class Test : public RefCountSafe {

public:

    int a;

    Test ( int init =12 )
    {  a = init; }

    ~Test () {}

    /// MT-safe new operator.
    static void* operator new ( size_t size );

    /// MT-safe new operator.
    static void* operator new ( size_t size, const char *file, int line );

    /// MT-safe delete operator.
    static void operator delete ( void *mem );

#ifdef __INTEL_COMPILER

    /// MT-safe delete operator.
    static void operator delete ( void *mem, const char *file, int line );
    
#endif

    };

#include "class/normalNew.hpp"

void* Test::operator new ( size_t size )
{
    return safeNew(size);
}

void* Test::operator new ( size_t size, const char *file, int line )
{
    return safeNew(size);
}

void Test::operator delete ( void *mem )
{
    safeDelete(mem);
}

#ifdef __INTEL_COMPILER

void Test::operator delete ( void *mem, const char *file, int line )
{
    safeDelete(mem);
}

#endif

#include "class/debugNew.hpp"

unsigned ExplicitMapTraits<unsigned,RefD<Test> >::keyNull = 0xffffffff;
unsigned ExplicitMapTraits<unsigned,RefD<Test> >::zombie  = 0xfffffffe;
unsigned ExplicitMapTraits<unsigned,RefD<Test> >::getHash ( unsigned &key )
{
    return key+1;
}

    // complete instantiation of the map:
template ExplicitMap<unsigned,RefD<Test>,true,MemAllocSafe>;

ExplicitMap<unsigned,RefD<Test>,true,MemAllocSafe> map(2,0.5);

BitMaskMTS global;

PoCriticalSection LockDecl(globalLock,"::globalLock");

int Max = 1000;

const int MAX_THREADS = 16;

THREAD_PROC_RETURN THREAD_PROC_MODE jam ( void *param )
{
    bool *cont = (bool*)param;
    RandomJames rnd;
    rnd.randomize();
    BitMaskMTS local;
    int operations = 0;
    do {
        int command = (int)(rnd.uniformNumber() * 5.0);
        int id, ord;
        switch ( command ) {
            case 0:                         // insert
                id = -1;
                globalLock.enter();
                if ( global.card() < Max ) { // there is an empty slot
                    do
                        id = (int)(rnd.uniformNumber() * Max);
                    while ( global.get(id) );
                    global.on(id);
                    }
                globalLock.leave();
                if ( id >= 0 ) {
                    operations++;
                    local.on(id);
                    if ( map.put(id,new Test(id)) )
                        RptF("global <-> map inconsistency (map.put replaces an item %d)",id);
                    }
                break;
            case 1:                         // delete
                if ( local.card() ) {
                    operations++;
                    ord = (int)(rnd.uniformNumber() * local.card());
                    id = local.getFirst();
                    while ( ord-- )
                        id = local.getNext(id);
                    Assert( id != BitMask::END );
                    local.off(id);
                    if ( !map.remove(id) )
                        RptF("map inconsistency (map.remove couldn't find the item %d)",id);
                    globalLock.enter();
                    global.off(id);
                    globalLock.leave();
                    }
                break;
            default:                        // lookup
                if ( local.card() ) {
                    operations++;
                    ord = (int)(rnd.uniformNumber() * local.card());
                    id = local.getFirst();
                    while ( ord-- )
                        id = local.getNext(id);
                    Assert( id != BitMask::END );
                    RefD<Test> item;
                    if ( !map.get(id,item) )
                        RptF("map inconsistency (map.get couldn't find the item %d)",id);
                    else
                    if ( item->a != id )
                        RptF("map inconsistency (map.get found the wrong item %d /should be %d/)",item->a,id);
                    }
                else
                    SLEEP_MS(50);
            }
        } while ( *cont );
    return (THREAD_PROC_RETURN)operations;
}

int main ( int argc, char **argv )
{
    int threads = 2;                        // number of jam threads
    if ( argc > 1 )
        threads = atoi(argv[1]);
    if ( threads < 1 ) threads = 1;
    if ( threads > MAX_THREADS ) threads = MAX_THREADS;

    int slp = 12;                           // run time in seconds
    if ( argc > 2 )
        slp = atoi(argv[2]);
    if ( slp < 2 ) slp = 2;

    if ( argc > 3 )
        Max = atoi(argv[3]);
    if ( Max < 5 ) Max = 5;
    if ( Max > 2000 ) Max = 2000;

    bool cont[MAX_THREADS];
    ThreadId tid[MAX_THREADS];
    int i;
    for ( i = 0; i < threads; i++ ) {
        cont[i] = true;
        if ( poThreadCreate(tid+i,0,&jam,(void*)(cont+i)) )
            printf("Jam thread (%u) was created successfully\n",(unsigned)tid[i]);
        SLEEP_MS(1234);
        }

    for ( i = 0; i < slp; i++ ) {
        SLEEP_MS(1000);
        printf("\r%3d %%",(i*100)/slp);
        }
    putchar('\n');

    for ( i = 0; i < threads; i++ ) {
        cont[i] = false;
        THREAD_PROC_RETURN result;
        if ( poThreadJoin(tid[i],&result) )
            printf("Jam thread (%u) finished successfully, operations =%10u\n",(unsigned)tid[i],(unsigned)result);
        }

    return threads;
}

#endif
