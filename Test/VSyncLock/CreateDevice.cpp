//-----------------------------------------------------------------------------
// File: CreateDevice.cpp
//
// Desc: This is the first tutorial for using Direct3D. In this tutorial, all
//       we are doing is creating a Direct3D device and using it to clear the
//       window.
//
// Copyright (c) Microsoft Corporation. All rights reserved.
//-----------------------------------------------------------------------------
#include <d3d9.h>
#include <strsafe.h>




//-----------------------------------------------------------------------------
// Global variables
//-----------------------------------------------------------------------------
LPDIRECT3D9             g_pD3D       = NULL; // Used to create the D3DDevice
LPDIRECT3DDEVICE9       g_pd3dDevice = NULL; // Our rendering device




//-----------------------------------------------------------------------------
// Name: InitD3D()
// Desc: Initializes Direct3D
//-----------------------------------------------------------------------------
HRESULT InitD3D( HWND hWnd )
{
  // Create the D3D object, which is needed to create the D3DDevice.
  if( NULL == ( g_pD3D = Direct3DCreate9( D3D_SDK_VERSION ) ) )
    return E_FAIL;

  // Set up the structure used to create the D3DDevice. Most parameters are
  // zeroed out. We set Windowed to TRUE, since we want to do D3D in a
  // window, and then set the SwapEffect to "discard", which is the most
  // efficient method of presenting the back buffer to the display.  And 
  // we request a back buffer format that matches the current desktop display 
  // format.
  D3DPRESENT_PARAMETERS d3dpp; 
  ZeroMemory( &d3dpp, sizeof(d3dpp) );
  #if 1
    d3dpp.Windowed = TRUE;
    d3dpp.BackBufferFormat = D3DFMT_UNKNOWN;
  #else
    d3dpp.Windowed = FALSE;
    d3dpp.BackBufferFormat = D3DFMT_X8R8G8B8;
    d3dpp.BackBufferWidth=1024;
    d3dpp.BackBufferHeight=768;
  #endif
  d3dpp.SwapEffect = D3DSWAPEFFECT_DISCARD;
  d3dpp.BackBufferCount = 2;
  d3dpp.PresentationInterval = D3DPRESENT_INTERVAL_ONE;

  // Create the Direct3D device. Here we are using the default adapter (most
  // systems only have one, unless they have multiple graphics hardware cards
  // installed) and requesting the HAL (which is saying we want the hardware
  // device rather than a software one). Software vertex processing is 
  // specified since we know it will work on all cards. On cards that support 
  // hardware vertex processing, though, we would see a big performance gain 
  // by specifying hardware vertex processing.
  if( FAILED( g_pD3D->CreateDevice( D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, hWnd,
    D3DCREATE_SOFTWARE_VERTEXPROCESSING,
    &d3dpp, &g_pd3dDevice ) ) )
  {
    return E_FAIL;
  }

  // Device state would normally be set here

  return S_OK;
}




//-----------------------------------------------------------------------------
// Name: Cleanup()
// Desc: Releases all previously initialized objects
//-----------------------------------------------------------------------------
VOID Cleanup()
{
  if( g_pd3dDevice != NULL) 
    g_pd3dDevice->Release();

  if( g_pD3D != NULL)
    g_pD3D->Release();
}

class EventDrivenCode
{
  public:
  enum Result {
    /// stay in the same state
    Stay,
    /// state done, rendering done, advance in the next simulation step
    Advance,
    /// state done, rendering not done - process next simulation step now
    AdvanceNow
  };
  typedef Result (EventDrivenCode::*Member)();
  
  private:
  const static int stackDepth = 32;
  Member *_currentInList[stackDepth];
  int _stackPointer;

  //@{ helper functions
  bool BeginFrame(D3DCOLOR color=D3DCOLOR_XRGB(0,0,255));
  /// beginOk true when BeginFrame succeeded
  void EndFrame(bool beginOk);

  void EmptyFrame(D3DCOLOR color);
  //@}

  //@{ FSM state management  
  void PushStateList(Member *newtop);
  void PopStateList();
  Result ExecuteStateList(Member *statelist);
  //@}
  
private:
  //@{ variables used by state functions
  long long _startTime;
  long long _waitUntil;
  int _vsyncCounter;
  
  int _vsyncStart;
  
  long long _timeBase;
  long long _lastPresent;
  //@}
  
public:

  //@{ state functions
  Result StartWaiting();
  Result Wait();
  Result DisplayResults();
  
  Result EstablishVSyncLock();
  Result InitVSyncLock();
  Result TrackVSyncLock();

  Result MeasureRefreshRate();
  
  Result InitRefreshRate();
  Result TrackRefreshRate();
  
  Result EmptyFrame1();
  Result EmptyFrame2();
  //@}

  Result PerformVSyncMeasurement();
  
  EventDrivenCode();

  Result StateListRender();
};

void EventDrivenCode::PushStateList(Member *newtop)
{
  if (_stackPointer>0)
  {
    // we should return to the instruction after the current one
    ++_currentInList[_stackPointer-1];
  }
  _currentInList[_stackPointer++] = newtop;
}
void EventDrivenCode::PopStateList()
{
  --_stackPointer;
}

EventDrivenCode::Result EventDrivenCode::ExecuteStateList(Member *statelist)
{
  PushStateList(statelist);
  return StateListRender();
}

EventDrivenCode::Result EventDrivenCode::StateListRender()
{
  // if stack is empty, we are done
  if (_stackPointer<=0)
  {
    EmptyFrame(D3DCOLOR_XRGB(255,255,255));
    return Advance;
  }
  Member fn = *_currentInList[_stackPointer-1];
  Result advance = (this->*fn)();
  if (advance>Stay)
  {
    ++_currentInList[_stackPointer-1];
    while (_stackPointer>0 && *_currentInList[_stackPointer-1]==NULL)
    {
      PopStateList();
    }
    if (advance==AdvanceNow)
    {
      return StateListRender();
    }
  }
  return Stay;
}

EventDrivenCode::Result EventDrivenCode::InitVSyncLock()
{
  // for several frames we will be monitoring when present is done
  EmptyFrame(D3DCOLOR_XRGB(255,255,0));
  _vsyncStart = _vsyncCounter;
  return Advance;
}
EventDrivenCode::Result EventDrivenCode::TrackVSyncLock()
{
  // assume refresh rate is known, we are only establishing a phase base
  EmptyFrame(D3DCOLOR_XRGB(255,0,0));
  long long time = HiResTime();
  int vsyncId = _vsyncCounter-_vsyncStart;
  if (_vsyncCounter<_vsyncStart+30)
  {
    return Stay;
  }
  // enough frames - analyze what we have
  return Advance;
}

EventDrivenCode::Result EventDrivenCode::EstablishVSyncLock()
{
  static Member statelist[]={
    InitVSyncLock,TrackVSyncLock,
    NULL
  };
  return ExecuteStateList(statelist);
}

EventDrivenCode::Result EventDrivenCode::MeasureRefreshRate()
{
  static Member statelist[]={
    InitRefreshRate,StartWaiting,Wait,
    TrackRefreshRate,
    NULL
  };
  return ExecuteStateList(statelist);
}
EventDrivenCode::Result EventDrivenCode::InitRefreshRate()
{
  // for several frames we will be monitoring when present is done
  EmptyFrame(D3DCOLOR_XRGB(255,255,0));
  return Advance;
}
EventDrivenCode::Result EventDrivenCode::TrackRefreshRate()
{
  OutputDebugString("***Refresh rate measured\n");
  // display how many frames were between
  long long timeExact = HiResTime()-_startTime;
  float time = HiResToTime(timeExact);
  int vsync = _vsyncCounter-_vsyncStart;
  char buf[1024];
  StringCbPrintf(buf,sizeof(buf),"refresh %7g\n",vsync/time);
  OutputDebugString(buf);
  return AdvanceNow;
}


long long TimeToHiRes(float time)
{
  LARGE_INTEGER freq;
  QueryPerformanceFrequency(&freq);
  return (long long)(time*freq.QuadPart);
}
long long HiResTime()
{
  LARGE_INTEGER time;
  QueryPerformanceCounter(&time);
  return time.QuadPart;
}

float HiResToTime(long long hires)
{
  LARGE_INTEGER freq;
  QueryPerformanceFrequency(&freq);
  return float(hires)/float(freq.QuadPart);
}

EventDrivenCode::Result EventDrivenCode::StartWaiting()
{
  OutputDebugString("***Waiting\n");
  _startTime = HiResTime();
  _waitUntil = _startTime+TimeToHiRes(0.1f);
  _vsyncStart = _vsyncCounter;

  return AdvanceNow;
}
EventDrivenCode::Result EventDrivenCode::Wait()
{
  long long time = HiResTime();
  if (time<_waitUntil)
  {
    //long long rest = _waitUntil-time;
    EmptyFrame(D3DCOLOR_XRGB(0,255,0));
    return Stay;
  }
  return AdvanceNow;
}
EventDrivenCode::Result EventDrivenCode::DisplayResults()
{
  OutputDebugString("***Done\n");
  // display how many frames were between
  long long timeExact = HiResTime()-_startTime;
  float time = HiResToTime(timeExact);
  int vsync = _vsyncCounter-_vsyncStart;
  char buf[1024];
  StringCbPrintf(
    buf,sizeof(buf),
    "Time %g, vsync %d, vsync period %7g, refresh %7g\n",
    time,vsync,time/vsync,vsync/time
  );
  OutputDebugString(buf);
  return AdvanceNow;
}

EventDrivenCode::Result EventDrivenCode::PerformVSyncMeasurement()
{
  static Member statelist[]={
    MeasureRefreshRate,
    EstablishVSyncLock,
    NULL
  };
  return ExecuteStateList(statelist);
}

EventDrivenCode::EventDrivenCode()
{
  static Member statelist[]={
    PerformVSyncMeasurement,
    PerformVSyncMeasurement,
    PerformVSyncMeasurement,
    NULL
  };
  _currentInList[0] = statelist;
  _stackPointer = 1;
  _vsyncCounter = 0;
  _timeBase = HiResTime();
  _lastPresent = _timeBase;
}

bool EventDrivenCode::BeginFrame(D3DCOLOR color)
{
  if( NULL == g_pd3dDevice )
    return false;

  // Clear the backbuffer to a given color
  g_pd3dDevice->Clear(
    //0, NULL, D3DCLEAR_TARGET|D3DCLEAR_ZBUFFER|D3DCLEAR_STENCIL, color, 1.0f, 0
    0, NULL, D3DCLEAR_TARGET, color, 1.0f, 0
  );

  // Begin the scene
  return SUCCEEDED( g_pd3dDevice->BeginScene() );
}

void EventDrivenCode::EndFrame(bool beginOk)
{
  if (beginOk)
  {
    // End the scene
    g_pd3dDevice->EndScene();
  }

  long long time = HiResTime();
  // Present the backbuffer contents to the display
  g_pd3dDevice->Present( NULL, NULL, NULL, NULL );
  
  long long duration = time-_lastPresent;
  _lastPresent = time;
  if (_stackPointer>0)
  {
    long long presentTime = HiResTime()-_timeBase;
    float timeDisp = HiResToTime(presentTime);
    float durDisp = HiResToTime(duration);
    char buf[256];
    StringCbPrintf(
      buf,sizeof(buf),
      "Time %-8g, duration %-8g, refresh %-8g\n",
      timeDisp,durDisp,1/durDisp
    );
    OutputDebugString(buf);
    _vsyncCounter++;
  }
}

void EventDrivenCode::EmptyFrame(D3DCOLOR color)
{
  bool ok = BeginFrame(color);
  EndFrame(ok);
}

EventDrivenCode::Result EventDrivenCode::EmptyFrame1()
{
  EmptyFrame(D3DCOLOR_XRGB(255,255,0));
  return Advance;
}

EventDrivenCode::Result EventDrivenCode::EmptyFrame2()
{
  EmptyFrame(D3DCOLOR_XRGB(255,255,255));
  return Advance;
}


static EventDrivenCode ControlLogic;

//-----------------------------------------------------------------------------
// Name: Render()
// Desc: Draws the scene
//-----------------------------------------------------------------------------
bool Render()
{
  EventDrivenCode::Result res = ControlLogic.StateListRender();
  return res!=EventDrivenCode::Stay;
}




//-----------------------------------------------------------------------------
// Name: MsgProc()
// Desc: The window's message handler
//-----------------------------------------------------------------------------
LRESULT WINAPI MsgProc( HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam )
{
  switch( msg )
  {
  case WM_DESTROY:
    Cleanup();
    PostQuitMessage( 0 );
    return 0;

  case WM_PAINT:
    Render();
    ValidateRect( hWnd, NULL );
    return 0;
  }

  return DefWindowProc( hWnd, msg, wParam, lParam );
}




//-----------------------------------------------------------------------------
// Name: WinMain()
// Desc: The application's entry point
//-----------------------------------------------------------------------------
INT WINAPI WinMain( HINSTANCE hInst, HINSTANCE, LPSTR, INT )
{
  // Register the window class
  WNDCLASSEX wc = { sizeof(WNDCLASSEX), CS_CLASSDC, MsgProc, 0L, 0L, 
    GetModuleHandle(NULL), NULL, NULL, NULL, NULL,
    "BIS VSync Lock", NULL };
  RegisterClassEx( &wc );

  //SetPriorityClass(GetCurrentProcess(),HIGH_PRIORITY_CLASS);
  //SetPriorityClass(GetCurrentProcess(),REALTIME_PRIORITY_CLASS);
  // Create the application's window
  HWND hWnd = CreateWindow( "BIS VSync Lock", "VSync Lock Experiment", 
    WS_OVERLAPPEDWINDOW, 100, 100, 300, 300,
    GetDesktopWindow(), NULL, wc.hInstance, NULL );

  // Initialize Direct3D
  if( SUCCEEDED( InitD3D( hWnd ) ) )
  { 
    // Show the window
    ShowWindow( hWnd, SW_SHOWDEFAULT );
    UpdateWindow( hWnd );

    // Enter the message loop
    MSG msg;
    ZeroMemory( &msg, sizeof(msg) );
    while( msg.message!=WM_QUIT )
    {
      if( PeekMessage( &msg, NULL, 0U, 0U, PM_REMOVE ) )
      {
        TranslateMessage( &msg );
        DispatchMessage( &msg );
      }
      else
        if (Render())
        {
          DestroyWindow(hWnd);
        }
    }

  }

  UnregisterClass( "BIS VSync Lock", wc.hInstance );
  return 0;
}
