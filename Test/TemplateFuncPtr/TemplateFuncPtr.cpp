// TemplateFuncPtr.cpp : Defines the entry point for the console application.
//

#include <stdio.h>

class CountComponents
{
public:
  mutable int _num;
  mutable int lastX, lastZ;

  CountComponents()
  {
  }
  void operator ()(int x, int z) const
  {
    lastX=x; lastZ=z;
    _num++;
  }
};

template <typename Function>
static void ForComponent(int x, int z, const Function &func)
{
  printf("x=%d  z=%d\n",x,z);
  func(x,z);
}

template <typename Function>
static void ForEachComponent
(
 int x, int z, const Function &func, 
 void (*forComponent)(int x, int z, const Function &func)
)
{
  //for all components ...
  (*forComponent)(x, z, func);
}

int main(int argc, char* argv[])
{
  CountComponents f;
  for (int x=0; x<5; x++)
    for (int y=0; y<5; y++)
      ForEachComponent(x, y, f, &ForComponent<CountComponents>);

  return 0;
}

