/**
    @file   test3.cpp
    @brief  Test program for GNU c++ (main).
    
    Copyright &copy; 2002 by Josef Pelikan (Josef.Pelikan@mff.cuni.cz)
    @author PE
    @since  9.10.2002
    @date   9.10.2002
*/

#include <stdio.h>
#include "test3m.hpp"

//-------------------------------------------------------------------------------------
//  main routine:

int main ()
{
    int a = globalAPI->f();
    printf("a = %d\n",a);
    return 0;
}
