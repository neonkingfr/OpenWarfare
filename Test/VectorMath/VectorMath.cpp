// VectorMath.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include <Es/essencepch.hpp>
#include <Es/Containers/array.hpp>
#include <El/Math/math3d.hpp>
#include <El/Math/math3dK.hpp>
#include <El/Math/math3dT.hpp>
#include <El/Math/mathStore.hpp>
#ifdef _XBOX
# include <pmcpb.h>
# include <pmcpbsetup.h>
# pragma comment(lib, "libpmcpb.lib") // Link in the PMC library.
#endif

# if PROFILE
//# define TRACE 1
#else
//# define TRACE 1
#endif

#ifdef _XBOX
# include <xboxmath.h>
# if TRACE
#   include <Tracerecording.h>
#   pragma comment(lib,"tracerecording.lib")
#   pragma comment(lib,"xbdm.lib")
# endif
#endif
#include <omp.h>

#ifdef _XBOX
  #define ProfileTime() __mftb32()
#else
  static inline __int64 ProfileTime()
  {
    __asm rdtsc
    // return value in edx:eax
  }
#endif

template <class Type> bool CheckCorrectResult(const Vector3P *ref, const Type *dst, int n)
{
  for (int i=0; i<n; i++)
  {
    float xDif = ref[i][0]-dst[i][0];
    float yDif = ref[i][1]-dst[i][1];
    float zDif = ref[i][2]-dst[i][2];
    const float eps = 1e-5f;
    if (!(fabs(xDif)<eps*fabs(ref[i][0])) || !(fabs(yDif)<eps*fabs(ref[i][1])) || !(fabs(zDif)<eps*fabs(ref[i][2])))
    {
      LogF("%d: %g,%g,%g -> %g,%g,%g",i,ref[i][0],ref[i][1],ref[i][2],dst[i][0],dst[i][1],dst[i][2]);
      return false;
    }
  }
  return true;
}

#ifdef _XBOX
void FastTransform( Vector3P &tgt, const XMMATRIX &a, const Vector3P &o )
{
  // Power PC VMX optimization
  // similar to XMVector3Transform(o,a);
  XMVECTOR oVec = XMLoadVector3(o);
  XMVECTOR ox = __vspltw(oVec,0);
  XMVECTOR oy = __vspltw(oVec,1);
  XMVECTOR oz = __vspltw(oVec,2);
  // 4 columns, each with 4 elements
  /* numericaly more stable, however slower
  XMVECTOR r = XMVectorMultiply(ox,a.r[0]);
  r = XMVectorMultiplyAdd(oy,a.r[1],r);
  r = XMVectorMultiplyAdd(oz,a.r[2],r);
  r = XMVectorAdd(r,a.r[3]);
  */
  XMVECTOR r = XMVectorMultiplyAdd(ox,a.r[0],a.r[3]);
  r = XMVectorMultiplyAdd(oy,a.r[1],r);
  r = XMVectorMultiplyAdd(oz,a.r[2],r);
  XMStoreVector3(&tgt,r);
}


XMMATRIX MatrixSetRotationX(Coord angle )
{
  XMMATRIX m = XMMatrixIdentity();
  Coord s=(Coord)sin(angle),c=(Coord)cos(angle);
  m._22 = +c;
  m._23 = +s;
  m._32 = -s;
  m._33 = +c;
  return m;
}

XMMATRIX MatrixSetRotationY(Coord angle)
{
  XMMATRIX m = XMMatrixIdentity();
  Coord s=(Coord)sin(angle),c=(Coord)cos(angle);
  m._11 = +c;
  m._13 = +s;
  m._31 = -s;
  m._33 = +c;
  return m;
}

XMMATRIX MatrixSetRotationZ(Coord angle)
{
  XMMATRIX m = XMMatrixIdentity();
  Coord s=(Coord)sin(angle),c=(Coord)cos(angle);
  m._11 = +c;
  m._12 = +s;
  m._21 = -s;
  m._22 = +c;
  return m;
}

TypeIsSimple(XMVECTOR)

template <> bool CheckCorrectResult<XMVECTOR>(const Vector3P *ref, const XMVECTOR *dst, int n)
{
  for (int i=0; i<n; i++)
  {
    float xDif = ref[i][0]-dst[i].x;
    float yDif = ref[i][1]-dst[i].y;
    float zDif = ref[i][2]-dst[i].z;
    const float eps = 1e-5f;
    if (!(fabs(xDif)<eps*fabs(ref[i][0])) || !(fabs(yDif)<eps*fabs(ref[i][1])) || !(fabs(zDif)<eps*fabs(ref[i][2])))
    {
      LogF("%d: %g,%g,%g -> %g,%g,%g",i,ref[i][0],ref[i][1],ref[i][2],dst[i].x,dst[i].y,dst[i].z);
      return false;
    }
  }
  return true;
}

#endif

TypeIsSimple(Quaternion<Float16bFixed<14> >)

extern DataStack GDataStack(16*1024);

static void StartXTrace(const char *name)
{
#if TRACE
  char filename[256];
  strcpy(filename,name);
  for (char *c=filename; *c; c++)
  {
    if (isalnum(*c)) continue;
    *c = '-';
  }
  char fullname[256];
  sprintf(fullname,"e:\\trace-%s.pix2",filename);
  XTraceStartRecording(fullname);
#endif
}
static void StopXTrace()
{
#if TRACE
  XTraceStopRecording();
#endif

}

struct PMCContext
{
  bool _running;

  PMCContext()
  {
    _running = false;
    #ifdef _XBOX
    // Select a set of sixteen counters.
    //PMCInstallSetup( &PMCDefaultSetups[PMC_SETUP_OVERVIEW_PB0T0] );
    PMCInstallSetup( &PMCDefaultSetups[PMC_SETUP_FLUSHREASONS_PB0T0] );
    #endif
  }

  void Start()
  {
    #ifdef _XBOX
    // Reset the Performance Monitor counters in preparation for a new sampling run.
    PMCResetCounters();
    // Select the current processor as the trigger source.
    PMCSetTriggerProcessor( GetCurrentProcessorNumber() / 2 );
    // Start up the Performance Monitor counters.
    PMCStart(); // Start up counters
    #endif
    _running = true;
  }
  void Stop()
  {
    if (!_running) return;
    _running = false;
    #ifdef _XBOX
    PMCStop();
    #endif
  }
} SPMCContext;

struct Timing
{
  DWORD _start;
  const char *_name;
  int _nVec;
  int _nLoops;
  
  Timing(const char *name, int nVec, int nLoops)
  {
    _name = name;
    _start = GetTickCount();
    _nVec = nVec;
    _nLoops = nLoops;
  }
  void Restart()
  {
    _start = GetTickCount();
    SPMCContext.Start();
    StartXTrace(_name);
  }
  ~Timing()
  {
    StopXTrace();
    DWORD end = GetTickCount();
    SPMCContext.Stop();
    #ifdef _XBOX
    PMCState pmcstate;
    PMCGetCounters( &pmcstate );
    #endif
    if (end>=_start)
    {
      // end-_start is in ms
      // we want us, per iteration
      double us = (end - _start) * 1e3;
      LogF(" %s %d, time %d ms, %.3f us/iter",_name,toInt(15.0f*_nVec/(end - _start)),end - _start,us/(_nVec*_nLoops));
      #if 0 //def _XBOX
      for (int cc=0; cc<lenof(pmcstate.pmc); cc++)
      {
        long long value = pmcstate.pmc[cc];
        int cost = PMCGetCounterCostEstimate(cc);
        const char *name=PMCGetCounterName(cc);
        LogF("%d %32s %10lld x %3d %6.0f",PMCGetCounterSource(cc),name,value,cost,double(value*cost)/3.2e4f);
      }
      LogF("-----");
      // Print out detailed information about all 16 counters.
      //PMCDumpCountersVerbose( &pmcstate, PMC_VERBOSE_NOL2ECC );
      //PMCDumpCountersVerbose( &pmcstate, PMC_VERBOSE_MAX );
      #endif
    }
    
  }
};
static void Benchmark()
{
  #if _DEBUG
  const int NVec = 100;
  const int NVecQ = 100;
  #elif TRACE
  const int NVec = 9000;
  const int NVecQ = 3000;
  #else
  const int NVec = 1000000;
  const int NVecQ = 100000;
  #endif

  #ifdef _XBOX

  //{
    int b = 1;

    Matrix4P ref_translation(MTranslation, Vector3P(0.5623f * b, 253.2f, -569.3f));
    Matrix4P ref_rotationY(MRotationY, 1.25f * b);
    Matrix4P ref_rotationZ(MRotationZ, -0.66f * b);
    
    Matrix4P ref_transform = ref_translation * (ref_rotationY * ref_rotationZ);
    
    XMMATRIX translation0 = XMMatrixTranslation(0.5623f * b, 253.2f, -569.3f);
    XMMATRIX rotationY0 = MatrixSetRotationY(1.25f * b);
    XMMATRIX rotationZ0 = MatrixSetRotationZ(-0.66f * b);
    
    XMMATRIX transform0 =  rotationZ0 * rotationY0 * translation0;
    
    //transform = ConvertPToX(ref_transform);

    Vector3P srcP(0.2f, 0.6f, -0.5f);
    Vector3P dstP;
    dstP.SetFastTransform(ref_transform, srcP);

    Vector3P dstT0;
    FastTransform(dstT0,transform0, srcP);
    // store reference result
    Assert(CheckCorrectResult(&dstP,&dstT0,1));

    XMFLOAT3A init(0.2f, 0.6f, -0.5f);
    static const XMVECTOR src = XMLoadFloat3A(&init);
    XMVECTOR dst;
    dst = XMVector3Transform(src, transform0);

    Assert(CheckCorrectResult(&dstP,&dst,1));
    
    Vector3 normalized = Vector3(1,1,1).Normalized();
    
    LogF("Passed?");
  //}
  #endif
  
  #if _DEBUG
  const bool verify = true;
  #else
  const bool verify = false;
  #endif
  // reference result
  const int maxB = 20;
  Temp<Vector3P> ref[maxB];
  Temp<Vector3P> refQ[maxB];
  
  if (verify)
  {
    for (int b=0; b<maxB; b++)
    {
      ref[b].Realloc(NVec);
      refQ[b].Realloc(NVecQ);
    }
  }

  {
    Timing time("QPC",NVec,maxB-2);
    for (int b=0; b<maxB; b++)
    {
      if (b == 2) time.Restart();
      LARGE_INTEGER pc;
      for (int i=0; i<NVec; i++)
      {
        volatile int a = ProfileTime();
        //QueryPerformanceCounter(&pc);
        //QueryPerformanceCounter(&pc);
      }
    }
  }
  
  {

    AutoArray<Vector3P,MemAllocDataStack<Vector3P,4*1024> > src(NVec), dst(NVec);
    src.Resize(NVec);
    dst.Resize(NVec);
    Timing time("Vector3P",NVec,maxB-2);
    for (int b=0; b<maxB; b++)
    {
      if (b == 2) time.Restart();
      for (int i=0; i<NVec; i++)
      {
        src[i] = Vector3P(0.2f, 0.6f, -0.5f);
      }
      Matrix4P translation(MTranslation, Vector3P(0.5623f * b, 253.2f, -569.3f));
      Matrix4P rotationY(MRotationY, 1.25f * b);
      Matrix4P rotationZ(MRotationZ, -0.66f * b);
      for (int i=0; i<NVec; i++)
      {
        Matrix4P transform = translation * (rotationY * rotationZ);
        dst[i].SetFastTransform(transform, src[i]);
        //FastTransform(dst[i],transform, src[i]);
      }
      if (verify) memcpy(ref[b].Data(),dst.Data(),NVec*sizeof(Vector3P));
    }
  }

  
  {
    Temp<Quaternion<Float16bFixed<14> > > rot(NVecQ);
    Temp<Vector3P> src(NVecQ), dst(NVecQ);
    for (int i=0; i<NVecQ; i++)
    {
      src[i] = Vector3P(0.2f, 0.6f, -0.5f);
      rot[i] = Quaternion< Float16bFixed<14> >(0,0.1,0.2,0.3);
    }
    Timing time("Vector3P*Quaternion",NVecQ,maxB-2);
    for (int b=0; b<maxB; b++)
    {
      int i;
      if (b == 2) time.Restart();
      for (i=0; i<NVecQ; i++)
      {
        Matrix4P transform;
        rot[i].ToMatrix(transform);
        transform.SetPosition(VZeroP);
        dst[i].SetFastTransform(transform, src[i]);
        //FastTransform(dst[i],transform, src[i]);
      }
      if (verify) memcpy(refQ[b].Data(),dst.Data(),NVecQ*sizeof(Vector3P));
    }
  }
  
  {
    
    Temp<Quaternion<Float16bFixed<14> > > rot(NVecQ);
    Temp<Vector3> src(NVecQ), dst(NVecQ);
    Timing time("Vector3*Quaternion",NVecQ,maxB-2);
    for (int i=0; i<NVecQ; i++)
    {
      src[i] = Vector3(0.2f, 0.6f, -0.5f);
      rot[i] = Quaternion< Float16bFixed<14> >(0,0.1,0.2,0.3);
    }
    for (int b=0; b<maxB; b++)
    {
      int i;
      if (b == 2) time.Restart();
      for (i=0; i<NVecQ; i++)
      {
        Matrix4 transform;
        rot[i].ToMatrix(transform);
        transform.SetPosition(VZero);
        dst[i].SetFastTransform(transform, src[i]);
        //FastTransform(dst[i],transform, src[i]);
      }
      if (verify) {Assert(CheckCorrectResult(refQ[b].Data(),dst.Data(),NVecQ));}
    }
  }

  
  {
    Temp<Vector3T> src(NVec), dst(NVec);
    Timing time("Vector3T",NVec,maxB-2);
    for (int b=0; b<maxB; b++)
    {
      int i;
      if (b == 2) time.Restart();
      for (i=0; i<NVec; i++)
      {
        src[i] = Vector3T(0.2f, 0.6f, -0.5f);
      }
      Matrix4T translation(MTranslation, Vector3T(0.5623f * b, 253.2f, -569.3f));
      Matrix4T rotationY(MRotationY, 1.25f * b);
      Matrix4T rotationZ(MRotationZ, -0.66f * b);
      for (i=0; i<NVec; i++)
      {
        Matrix4T transform = translation * (rotationY * rotationZ);
        dst[i].SetFastTransform(transform, src[i]);
        //FastTransform(dst[i],transform, src[i]);
      }
      if (verify) memcpy(ref[b].Data(),dst.Data(),NVec*sizeof(Vector3P));
    }
  }

  #if _OPENMP
  {
    
    Temp<Vector3P> src(NVec), dst(NVec);
    Timing time("Vector3P OMP",NVec,maxB-2);
    for (int b=0; b<maxB; b++)
    {
      int i;
      if (b == 2) time.Restart();
      Vector3P *srcData = src.Data();
      Vector3P *dstData = dst.Data();
      #pragma omp parallel for firstprivate(srcData)
      //#pragma omp parallel for
      for (i=0; i<NVec; i++)
      {
        srcData[i] = Vector3P(0.2f, 0.6f, -0.5f);
      }
      Matrix4P translation(MTranslation, Vector3P(0.5623f * b, 253.2f, -569.3f));
      Matrix4P rotationY(MRotationY, 1.25f * b);
      Matrix4P rotationZ(MRotationZ, -0.66f * b);
      #pragma omp parallel for firstprivate(rotationZ,rotationY,translation,srcData,dstData)
      //#pragma omp parallel for
      for (i=0; i<NVec; i++)
      {
        Matrix4P transform = translation * (rotationY * rotationZ);
        dstData[i].SetFastTransform(transform, srcData[i]);
        //FastTransform(dst[i],transform, src[i]);
      }
      if (verify) memcpy(ref[b].Data(),dst.Data(),NVec*sizeof(Vector3P));
    }
    
  }
  #endif
  
  {

    Temp<Vector3> src(NVec), dst(NVec);
    Timing time("Vector3",NVec,maxB-2);

    for (int b=0; b<maxB; b++)
    {
      int i;
      if (b == 2) time.Restart();
      for (i=0; i<NVec; i++)
      {
        src[i] = Vector3(0.2f, 0.6f, -0.5f);
      }
      Matrix4 translation(MTranslation, Vector3(0.5623f * b, 253.2f, -569.3f));
      Matrix4 rotationY(MRotationY, 1.25f * b);
      Matrix4 rotationZ(MRotationZ, -0.66f * b);
      for (i=0; i<NVec; i++)
      {
        Matrix4 transform = translation * (rotationY * rotationZ);
        dst[i].SetFastTransform(transform, src[i]);
        //FastTransform(dst[i],transform, src[i]);
      }
      if (verify) {Assert(CheckCorrectResult(ref[b].Data(),dst.Data(),NVec));}
    }

  }
  
  #ifdef _MATH3DK_HPP
  {

    Temp<Vector3K> src(NVec), dst(NVec);
    Timing time("Vector3K",NVec,maxB-2);

    for (int b=0; b<maxB; b++)
    {
      int i;
      if (b == 2) time.Restart();
      for (i=0; i<NVec; i++)
      {
        src[i] = Vector3K(0.2f, 0.6f, -0.5f);
      }
      Matrix4K translation(MTranslation, Vector3K(0.5623f * b, 253.2f, -569.3f));
      Matrix4K rotationY(MRotationY, 1.25f * b);
      Matrix4K rotationZ(MRotationZ, -0.66f * b);
      for (i=0; i<NVec; i++)
      {
        Matrix4K transform = translation * (rotationY * rotationZ);
        dst[i].SetFastTransform(transform, src[i]);
        //FastTransform(dst[i],transform, src[i]);
      }
      if (verify) {Assert(CheckCorrectResult(ref[b].Data(),dst.Data(),NVec));}
    }
  }

  #if _OPENMP
  {

    Temp<Vector3K> src(NVec), dst(NVec);
    Timing time("Vector3K OMP",NVec,maxB-2);

    for (int b=0; b<maxB; b++)
    {
      int i;
      if (b == 2) time.Restart();
      Vector3K *srcData = src.Data();
      Vector3K *dstData = dst.Data();
      #pragma omp parallel for firstprivate(srcData)
      for (i=0; i<NVec; i++)
      {
        srcData[i] = Vector3K(0.2f, 0.6f, -0.5f);
      }
      Matrix4K translation(MTranslation, Vector3K(0.5623f * b, 253.2f, -569.3f));
      Matrix4K rotationY(MRotationY, 1.25f * b);
      Matrix4K rotationZ(MRotationZ, -0.66f * b);
      #pragma omp parallel for firstprivate(srcData,dstData,translation,rotationY,rotationZ)
      for (i=0; i<NVec; i++)
      {
        Matrix4K transform = translation * (rotationY * rotationZ);
        dstData[i].SetFastTransform(transform, srcData[i]);
        //FastTransform(dst[i],transform, src[i]);
      }
      if (verify) {Assert(CheckCorrectResult(ref[b].Data(),dst.Data(),NVec));}
    }
  }
  #endif
  #endif
  

  #ifdef _XBOX
  {
    
    Temp<Vector3P> src(NVec), dst(NVec);
    Timing time("Vector3P * XMMATRIX",NVec,maxB-2);

    for (int b=0; b<maxB; b++)
    {
      int i;
      if (b == 2) time.Restart();
      for (i=0; i<NVec; i++)
      {
        src[i] = Vector3P(0.2f, 0.6f, -0.5f);
      }
      XMMATRIX translation = XMMatrixTranslation(0.5623f * b, 253.2f, -569.3f);
      XMMATRIX rotationY = MatrixSetRotationY(+1.25f * b);
      XMMATRIX rotationZ = MatrixSetRotationZ(-0.66f * b);
      for (i=0; i<NVec; i++)
      {
        XMMATRIX transform = rotationZ * rotationY * translation;
        //dst[i].SetFastTransform(transform, src[i]);
        FastTransform(dst[i],transform, src[i]);
      }

      if (verify) {Assert(CheckCorrectResult(ref[b].Data(),dst.Data(),NVec));}
    }
  }

  { // XMVector3Transform numeric precision is somewhat worse
    // this is because position add is done before computing rotation
    Temp<XMVECTOR> src(NVec), dst(NVec);
    Timing time("XMVECTOR*XMMATRIX",NVec,maxB-2);

    for (int b=0; b<maxB; b++)
    {
      int i;
      if (b == 2) time.Restart();
      for (i=0; i<NVec; i++)
      {
        static const XMFLOAT3A init = XMFLOAT3(0.2f, 0.6f, -0.5f);
        src[i] = XMLoadFloat3A(&init);
      }
      XMMATRIX translation = XMMatrixTranslation(0.5623f * b, 253.2f, -569.3f);
      XMMATRIX rotationY = MatrixSetRotationY(1.25f * b);
      XMMATRIX rotationZ = MatrixSetRotationZ(-0.66f * b);
      for (i=0; i<NVec; i++)
      {
        XMMATRIX transform =  (rotationZ * rotationY) * translation;
        //dst[i].SetFastTransform(transform, src[i]);
        dst[i] = XMVector3Transform(src[i], transform);
      }
      if (verify) {Assert(CheckCorrectResult(ref[b].Data(),dst.Data(),NVec));}
    }
  }

  { // XMVector3Transform numeric precision is somewhat worse
    // this is because position add is done before computing rotation
    
    //omp_set_num_threads(6);
    //omp_set_num_threads(3);
    //omp_set_num_threads(5);
    
    Temp<XMVECTOR> src(NVec), dst(NVec);
    Timing time("OpenMP XMVECTOR*XMMATRIX",NVec,maxB-2);

    for (int b=0; b<maxB; b++)
    {
      int i;
      if (b == 2) time.Restart();
      XMVECTOR *srcData = src.Data();
      XMVECTOR *dstData = src.Data();
      #pragma omp parallel for firstprivate(srcData)
      for (i=0; i<NVec; i++)
      {
        static const XMFLOAT3A init = XMFLOAT3(0.2f, 0.6f, -0.5f);
        srcData[i] = XMLoadFloat3A(&init);
      }
      XMMATRIX translation = XMMatrixTranslation(0.5623f * b, 253.2f, -569.3f);
      XMMATRIX rotationY = MatrixSetRotationY(1.25f * b);
      XMMATRIX rotationZ = MatrixSetRotationZ(-0.66f * b);
      
      
      float sum=0;
      #pragma omp parallel for firstprivate(rotationZ,rotationY,translation,srcData,dstData)
      for (i=0; i<NVec; i++)
      {
        XMMATRIX transform =  (rotationZ * rotationY) * translation;
        //dst[i].SetFastTransform(transform, src[i]);
        dstData[i] = XMVector3Transform(srcData[i], transform);
      }
      if (verify) {Assert(CheckCorrectResult(ref[b].Data(),dst.Data(),NVec));}
    }
  }
  #endif

};


//-------------------------------------------------------------------------------------
// Name: main()
// Desc: The application's entry point
//-------------------------------------------------------------------------------------
void __cdecl main()
{
  Benchmark();
  // Initialize Direct3D
  #ifdef _XBOX
  OutputDebugString("**** Done\n");
  for(;;)
  {
  
  }
  #endif
}

