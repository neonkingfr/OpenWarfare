using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Net.Sockets;
using System.IO;
using System.Text.RegularExpressions;
using System.Threading;

namespace NNTPReader
{
  public partial class Form1 : Form
  {
    // Used for receiving info
    byte[] downBuffer = new byte[2048];
    // Used for sending commands
    byte[] byteSendInfo = new byte[2048];
    // Used for connecting a socket to the NNTP server
    TcpClient tcpClient;
    // Used for sending and receiving information
    NetworkStream strRemote;
    // Stores various responses
    string Response;
    // Number of bytes in the buffer
    int bytesSize;
    // Stores the ID of the first message in a newsgroup
    int firstID;
    // Stores the ID of the last message in a newsgroup
    int lastID;
    // Stores chunks of the articles from the buffer
    string NewChunk;

    // Thread for converting to SQL
    Thread saveThread;
    SaveNews2SqlThread saveNews2SqlThread;
    SaveNewsThread saveNewsThread;

    public Form1()
    {
      InitializeComponent();
    }

    private void btnGo_Click(object sender, EventArgs e)
    {
      // Get the Autentification login and password
      NNTPReader.Login loginForm = new NNTPReader.Login();
      DialogResult result = loginForm.ShowDialog(this);
      if (result != DialogResult.OK)
      {
        return;
      }

      // Open the socket to the server
      tcpClient = new System.Net.Sockets.TcpClient(txtNNTPServer.Text, 119);
      strRemote = tcpClient.GetStream();
      // Read the bytes
      bytesSize = strRemote.Read(downBuffer, 0, 2048);
      // Retrieve the response
      Response = System.Text.Encoding.GetEncoding(1250).GetString(downBuffer, 0, bytesSize);
      // Just as in HTTP, if code 200 is not returned, something's not right
      if (Response.Substring(0, 3) != "200")
      {
        MessageBox.Show("The server returned an unexpected response.", "Connection failed", MessageBoxButtons.OK, MessageBoxIcon.Error);
      }
      // Show the response
      txtLog.Text = Response + "\n";

      byteSendInfo = StringToByteArr("AUTHINFO USER "+ loginForm.user + "\r\n");
      strRemote.Write(byteSendInfo, 0, byteSendInfo.Length);
      bytesSize = strRemote.Read(downBuffer, 0, 2048);
      Response = System.Text.Encoding.GetEncoding(1250).GetString(downBuffer, 0, bytesSize); //381 PASS required
      if (Response.Substring(0, 3) != "381")
      {
          MessageBox.Show("The server returned an unexpected response.\r\n" + Response, "Connection failed", MessageBoxButtons.OK, MessageBoxIcon.Error);
          return;
      }

      byteSendInfo = StringToByteArr("AUTHINFO PASS "+ loginForm.passwd +"\r\n");
      //String ahoj = String.Format("AUTHINFO PASS {0}\r\n", loginForm.passwd);
      strRemote.Write(byteSendInfo, 0, byteSendInfo.Length);
      bytesSize = strRemote.Read(downBuffer, 0, 2048);
      Response = System.Text.Encoding.GetEncoding(1250).GetString(downBuffer, 0, bytesSize); //281 Ok
      if (Response.Substring(0, 3) != "281")
      {
          MessageBox.Show("The server returned an unexpected response.\r\n" + Response, "Connection failed", MessageBoxButtons.OK, MessageBoxIcon.Error);
          return;
      }

      // Make the request to list all newsgroups
      byteSendInfo = StringToByteArr("LIST\r\n");
      strRemote.Write(byteSendInfo, 0, byteSendInfo.Length);
      Response = "";

      // Loop to retrieve a list of newsgroups
      while ((bytesSize = strRemote.Read(downBuffer, 0, downBuffer.Length)) > 0)
      {
        // Get the chunk of string
        NewChunk = Encoding.GetEncoding(1250).GetString(downBuffer, 0, bytesSize);
        Response += NewChunk;
        // If the string ends in a "\r\n.\r\n" then the list is over
        if (NewChunk.Substring(NewChunk.Length - 5, 5) == "\r\n.\r\n")
        {
          // Remove the "\r\n.\r\n" from the end of the string
          Response = Response.Substring(0, Response.Length - 3);
          break;
        }
      }
      // Split lines into an array
      string[] ListLines = Response.Split('\n');
      // Loop line by line
      foreach (String ListLine in ListLines)
      {
        // If the response starts with 215, it's the line that indicates the status
        if (ListLine.Length > 3 && ListLine.Substring(0, 3) == "215")
        {
          // Add the status response line to the log window
          txtLog.Text += ListLine + "\r\n";
        }
        else
        {
          // Add the newsgroup to the combobox
          string[] Newsgroup = ListLine.Split(' ');
          cmbNewsgroups.Items.Add(Newsgroup[0]);
        }
      }
    }

    public static byte[] StringToByteArr(string str)
    {
      System.Text.ASCIIEncoding encoding = new System.Text.ASCIIEncoding();
      return encoding.GetBytes(str);
    }

    private void btnGetNews_Click(object sender, EventArgs e)
    {
      // If a newsgroup is selected in the ComboBox
      if (cmbNewsgroups.SelectedIndex != -1)
      {
        // Request a certain newsgroup
        byteSendInfo = StringToByteArr("GROUP " + cmbNewsgroups.SelectedItem.ToString() + "\r\n");
        strRemote.Write(byteSendInfo, 0, byteSendInfo.Length);
        Response = "";
        bytesSize = strRemote.Read(downBuffer, 0, 2048);
        Response = System.Text.Encoding.GetEncoding(1250).GetString(downBuffer, 0, bytesSize);
        // Split the information about the newsgroup by blank spaces
        string[] Group = System.Text.Encoding.GetEncoding(1250).GetString(downBuffer, 0, bytesSize).Split(' ');
        // Show information about the newsgroup in the txtLog TextBox
        Response += Group[1] + " messages in the group (messages " + Group[2] + " through " + Group[3] + ")\r\n";
        txtLog.Text += Response;
        Response = "";
        // The ID of the first article in this newsgroup
        firstID = Convert.ToInt32(Group[2]);
        // The ID of the last article in this newsgroup
        lastID = Convert.ToInt32(Group[3]);
        // Messages are chosen, enable button to savenews
        saveNews.Enabled = true;
      }
      else
      {
        MessageBox.Show("Please connect to a server and select a newsgroup from the dropdown list first.", "Newsgroup retrieval", MessageBoxButtons.OK, MessageBoxIcon.Error);
      }
    }

    private void btnNext_Click(object sender, EventArgs e)
    {
      NextArticle();
    }

    public struct HeaderInfo
    {
      //messageID, references, from, newsgroups, subject, dateSQL
      public string messageID;
      public string references;
      public string from;
      public string newsgroups;
      public string subject;
      public string dateSQL;
      public HeaderInfo(string messageID, string references, string from, string newsgroups, string subject, string dateSQL)
      {
        this.messageID = messageID;
        this.references = references;
        this.from = from;
        this.newsgroups = newsgroups;
        this.subject = subject;
        this.dateSQL = dateSQL;
      }
    }
    HeaderInfo _headerInfo;

    public static HeaderInfo ParseNNTPHeader(string head)
    {
      string[] rows = head.Split('\n');
      string messageID = "";
      string references = "";
      string from = "";
      string newsgroups = "";
      string subject = "";
      DateTime date;
      string dateSQL = "2000-01-01 00:00:00";
      for (int i = 0; i < rows.Length; i++)
      {
        // Read an XML file into the DataSet
        int colonIx = rows[i].IndexOf(':');
        if (colonIx >= 0)
        {
          string header = rows[i].Substring(0, colonIx);
          switch (header)
          {
            case "Message-ID":
              {
                string pattern = @"\<([^\>]*)\>";
                Match match = Regex.Match(rows[i], pattern);
                messageID = match.Groups[1].Value;
              }
              break;
            case "References":
              {
                string pattern = @"\<([^\>]*)\>"; //take only first reference, ignore others
                Match match = Regex.Match(rows[i], pattern);
                references = match.Groups[1].Value;
              }
              break;
            case "From":
              {
                from = rows[i].Substring(colonIx + 1);
                from = from.Trim();
                from = from.Replace("'", "''"); //double single quotes
              }
              break;
            case "Newsgroups":
              {
                newsgroups = rows[i].Substring(colonIx + 1);
                newsgroups = newsgroups.Trim();
              }
              break;
            case "Subject":
              {
                subject = rows[i].Substring(colonIx + 1);
                subject = subject.Trim();
                subject = subject.Replace("'", "''"); //double single quotes
              }
              break;
            case "Date":
              {
                string dateStr = rows[i].Substring(colonIx + 1);
                dateStr = dateStr.Trim();
                // it looks like 
                // Mon, 9 Nov 2009 15:09:21 +0000 (UTC)
                // we need to delete the +0000 offset and (UTC) and to add it again
                // but it can contain also "Date: 7 Sep 2001 12:31:54 GMT\r"
                string pattern = @"(.*) ([+-].*)";
                Match match = Regex.Match(dateStr, pattern);
                string dateOnly = match.Groups[1].Value;
                if (dateOnly == "") //different format, possibly without +-0100
                {
                  pattern = @"(.* ..\:..\:..) (.*)";
                  match = Regex.Match(dateStr, pattern);
                  dateStr = match.Groups[1].Value;
                  date = DateTime.Parse(dateStr);
                }
                else
                {
                  string addStr = match.Groups[2].Value;
                  pattern = @"([+-]....).*";
                  match = Regex.Match(addStr, pattern);
                  int addVal = Int32.Parse(match.Groups[1].Value) / 100;
                  date = DateTime.Parse(dateOnly);
                  date = date.AddHours(-addVal); // +0700 means 7 was added, so we substract it
                }
                dateSQL = String.Format("{0:D4}-{1:D2}-{2:D2} {3:D2}:{4:D2}:{5:D2}", date.Year, date.Month, date.Day, date.Hour, date.Minute, date.Second);
              }
              break;
          }
        }
      }
      return new HeaderInfo(messageID, references, from, newsgroups, subject, dateSQL);
    }

    private void NextArticle()
    {
      if (tcpClient != null && tcpClient.Connected == true && firstID >= 0)
      {
        // Get the header
        txtHead.Text = "";
        // Initialize the buffer to 2048 bytes
        downBuffer = new byte[2048];
        // Request the headers of the article
        byteSendInfo = StringToByteArr("HEAD " + firstID + "\r\n");
        // Send the request to the NNTP server
        strRemote.Write(byteSendInfo, 0, byteSendInfo.Length);
        while ((bytesSize = strRemote.Read(downBuffer, 0, downBuffer.Length)) > 0)
        {
          NewChunk = System.Text.Encoding.GetEncoding(1250).GetString(downBuffer, 0, bytesSize);
          txtHead.Text += NewChunk;
          // No such article in the group
          if (NewChunk.Substring(0, 3) == "423")
          {
            // Ready for the next article, unless there is nothing else there...
            if (firstID <= lastID)
            {
              firstID++;
            }
            // Next article please
            NextArticle();
            // End this method because it's retrieving a nonexistent article
            return;
          }
          else if (NewChunk.Substring(NewChunk.Length - 5, 5) == "\r\n.\r\n")
          {
            // If the last thing in the buffer is "\r\n.\r\n" the message's finished
            break;
          }
        }

        _headerInfo = ParseNNTPHeader(NewChunk);

        // Get the body
        txtBody.Text = "";
        // Initialize the buffer to 2048 bytes
        downBuffer = new byte[2048];
        // Request the headers of the article
        byteSendInfo = StringToByteArr("BODY " + firstID + "\r\n");
        // Send the request to the NNTP server
        strRemote.Write(byteSendInfo, 0, byteSendInfo.Length);
        while ((bytesSize = strRemote.Read(downBuffer, 0, downBuffer.Length)) > 0)
        {
          NewChunk = System.Text.Encoding.GetEncoding(1250).GetString(downBuffer, 0, bytesSize);
          txtBody.Text += NewChunk;
          // If the last thing in the buffer is "\r\n.\r\n" the message's finished
          if (NewChunk.Substring(NewChunk.Length - 5, 5) == "\r\n.\r\n")
          {
            break;
          }
        }

        // Ready for the next article, unless there is nothing else there...
        if (firstID <= lastID)
        {
          firstID++;
        }
      }
      else
      {
        MessageBox.Show("Please select a newsgroup from the dropdown list and click on 'Get News' first.", "Newsgroup retrieval", MessageBoxButtons.OK, MessageBoxIcon.Error);
      }
    }

    private class SaveNews2SqlThread
    {
      Form1 parent;
      int firstID, lastID;
      byte[] byteSendInfo = new byte[2048];
      int bytesSize;
      public SaveNews2SqlThread(Form1 parent) { this.parent = parent; firstID = parent.firstID; lastID = parent.lastID;  }
      public void ThreadRun()
      {
        if (parent.tcpClient != null && parent.tcpClient.Connected == true && firstID >= 0)
        {
          //Note: one cannot use direct parent.saveNews.Enabled = false;
          //      for runtime error: Cross-thread operation not valid: Control accessed from a thread other than the thread it was created on.
          // The delegate and Invoke should be used
          parent.Invoke(new MethodInvoker(
            delegate
            {
              parent.saveNews.Enabled = false;
              parent.btnNext.Enabled = false;
              parent.btnGetNews.Enabled = false;
              parent.btnGo.Enabled = false;
            }));
          FileStream myFile = File.Create("newsGroupExport.sql");
          Encoding utf8 = Encoding.UTF8;
          // Initialize the buffer to 2048 bytes
          byte[] buf = new byte[16384];
          while (firstID <= lastID)
          {
            parent.Invoke(new MethodInvoker(delegate { parent.labelSaveProgress.Text = firstID + "/" + lastID; }));
            /// Get the header
            // Request the headers of the article
            byteSendInfo = StringToByteArr("HEAD " + firstID + "\r\n");
            // Send the request to the NNTP server
            parent.strRemote.Write(byteSendInfo, 0, byteSendInfo.Length);
            string newsArticle = "";
            string newChunk = "";
            while ((bytesSize = parent.strRemote.Read(buf, 0, buf.Length)) > 0)
            {
              newChunk = System.Text.Encoding.GetEncoding(1250).GetString(buf, 0, bytesSize);
              newsArticle += newChunk;
              // No such article in the group
              if (newChunk.Substring(0, 3) == "423")
              {
                // Ready for the next article, unless there is nothing else there...
                break;
              }
              else if (newChunk.Substring(newChunk.Length - 5, 5) == "\r\n.\r\n")
              {
                string sql = NNTPHeader2SQL(newsArticle);
                byte[] bytes = utf8.GetBytes(sql);
                myFile.Write(bytes, 0, bytes.Length);
                break;
              }
            }
            firstID++; //next article
          }
          myFile.Close();
          parent.Invoke(new MethodInvoker(
            delegate {
              parent.labelSaveProgress.Text = "DONE";
              parent.saveNews.Enabled = true;
              parent.btnNext.Enabled = true;
              parent.btnGetNews.Enabled = true;
              parent.btnGo.Enabled = true;
            }));
        }
      }

      private string NNTPHeader2SQL(string head)
      {
        HeaderInfo hi = ParseNNTPHeader(head);
        return String.Format("INSERT INTO News\r\nVALUES ('{0}', '{1}', '{2}', '{3}', '{4}', '{5}');\r\n",
                        hi.messageID, hi.references, hi.from, hi.newsgroups, hi.subject, hi.dateSQL);
      }
    } //class SaveNews2SqlThread

    private class SaveNewsThread
    {
      Form1 parent;
      int firstID, lastID;
      byte[] byteSendInfo = new byte[2048];
      int bytesSize;
      public SaveNewsThread(Form1 parent) { this.parent = parent; firstID = parent.firstID; lastID = parent.lastID; }
      public void ThreadRun()
      {
        if (parent.tcpClient != null && parent.tcpClient.Connected == true && firstID >= 0)
        {
          //Note: one cannot use direct parent.saveNews.Enabled = false;
          //      for runtime error: Cross-thread operation not valid: Control accessed from a thread other than the thread it was created on.
          // The delegate and Invoke should be used
          parent.Invoke(new MethodInvoker(
            delegate
            {
              parent.saveNews.Enabled = false;
              parent.btnNext.Enabled = false;
              parent.btnGetNews.Enabled = false;
              parent.btnGo.Enabled = false;
            }));
          // Initialize the buffer to 2048 bytes
          byte[] buf = new byte[16384];
          while (firstID <= lastID)
          {
            parent.Invoke(new MethodInvoker(delegate { parent.labelSaveProgress.Text = firstID + "/" + lastID; }));
            /// Get the header
            // Request the headers of the article
            byteSendInfo = StringToByteArr("HEAD " + firstID + "\r\n");
            // Send the request to the NNTP server
            parent.strRemote.Write(byteSendInfo, 0, byteSendInfo.Length);
            string newsArticle = "";
            bool articleReady = false;
            string newChunk = "";
            while ((bytesSize = parent.strRemote.Read(buf, 0, buf.Length)) > 0)
            {
              newChunk = System.Text.Encoding.GetEncoding(1250).GetString(buf, 0, bytesSize);
              newsArticle += newChunk;
              // No such article in the group
              if (newChunk.Substring(0, 3) == "423")
              {
                // Ready for the next article, unless there is nothing else there...
                break;
              }
              else if (newChunk.Substring(newChunk.Length - 5, 5) == "\r\n.\r\n")
              {
                articleReady = true;
                break;
              }
            }
            string articleBody = "";
            if (articleReady)
            {
              // Get the article body
              // Initialize the buffer to 2048 bytes
              byte[] bodyBuffer = new byte[2048];
              // Request the headers of the article
              byteSendInfo = StringToByteArr("BODY " + firstID + "\r\n");
              // Send the request to the NNTP server
              parent.strRemote.Write(byteSendInfo, 0, byteSendInfo.Length);
              while ((bytesSize = parent.strRemote.Read(buf, 0, buf.Length)) > 0)
              {
                newChunk = System.Text.Encoding.GetEncoding(1250).GetString(buf, 0, bytesSize);
                // If the last thing in the buffer is "\r\n.\r\n" the message's finished
                if (newChunk.Substring(newChunk.Length - 5, 5) == "\r\n.\r\n")
                {
                  break;
                }
                articleBody += newChunk;
              }
            }
            if (articleReady)
            {
              // Do the action
              HeaderInfo hi = ParseNNTPHeader(newsArticle);
              parent.SaveToFiles(hi, articleBody);
            }
            firstID++; //next article
          }
          parent.Invoke(new MethodInvoker(
            delegate
            {
              parent.labelSaveProgress.Text = "DONE";
              parent.saveNews.Enabled = true;
              parent.btnNext.Enabled = true;
              parent.btnGetNews.Enabled = true;
              parent.btnGo.Enabled = true;
            }));
        }
      }
    } //class SaveNewsThread
    
    private void saveNews_Click(object sender, EventArgs e)
    {
//       saveNews2SqlThread = new SaveNews2SqlThread(this);
//       saveThread = new Thread(new ThreadStart(saveNews2SqlThread.ThreadRun));
      saveNewsThread = new SaveNewsThread(this);
      saveThread = new Thread(new ThreadStart(saveNewsThread.ThreadRun));
      saveThread.Start();
    }

    private string GetMimeType(string fileName)
    {
      string mimeType = "application/unknown";
      string ext = System.IO.Path.GetExtension(fileName).ToLower();
      Microsoft.Win32.RegistryKey regKey = Microsoft.Win32.Registry.ClassesRoot.OpenSubKey(ext);
      if (regKey != null && regKey.GetValue("Content Type") != null)
        mimeType = regKey.GetValue("Content Type").ToString();
      return mimeType;
    }

    public void SaveToFiles(HeaderInfo hi, string body)
    {
      LNStates state = LNStates.LNText;
      string[] lines = body.Split('\n');
      string uuEncodedFile = "";  // the attachment's uuEncoded content
      string fileName = "";       // filename of possible attachment found
      string markdownBody = "";   // main text of the article
      string attachments = "";    // markdown links to saved attachments 
      int attachmentNo = 0;
      for (int i = 0; i < lines.Length; i++)
      {
        string line = lines[i].TrimEnd(new char[] { '\r' });
        switch (state)
        {
          case LNStates.LNText:
            string beginPattern = @"begin [0-9a-fA-F]{3} ([^ ].*)"; //something like "begin 666 myPic.jpg"
            Match match = Regex.Match(line, beginPattern);
            string filen = match.Groups[1].Value;
            if (filen.CompareTo("") == 0)
            {
              // store the line
              if (line.CompareTo("..") == 0) line = "."; //fix double dots to represent single dots (NNTP)
              if (i > 0) //skip first line, containing answer: 222 1906 <hj95ed$alk$1@new-server.localdomain> body
              {
                markdownBody += line + "\r\n";
              }
              continue; //no attachment match
            }
            fileName = filen; //remember the filename
            uuEncodedFile = "";
            // attachment found
            state = LNStates.LNFile;
            System.Diagnostics.Debug.WriteLine(fileName);
            break;
          case LNStates.LNFile:
            if (line.CompareTo(" ") == 0 || line.CompareTo("`") == 0)
            {
              state = LNStates.LNEmpty;
              continue;
            }
            // prepare data for uudecode
            uuEncodedFile += line + "\r\n";
            break;
          case LNStates.LNEmpty:
            if (line.CompareTo("end") != 0)
            {
              System.Diagnostics.Debug.WriteLine("Bad attachment {0}, does not ends with single space line and end-line!", fileName);
              state = LNStates.LNText;
            }
            else
            {
              // uudecode the file stored in uuEncodedFile
              // create outputStream file
              string attachmentFileName = hi.messageID + "_" + fileName;
              FileStream outputStream = File.Create(attachmentFileName);
              // convert string to stream
              byte[] bytes = Encoding.ASCII.GetBytes(uuEncodedFile);
              MemoryStream stream = new MemoryStream(bytes);
              Codecs.UUDecode(stream, outputStream);
              outputStream.Close();
              string mime = GetMimeType(attachmentFileName);
              string format = "[Attachment {0}]({1})\r\n";
              string attURL = "https://dev.bistudio.com/svn/sandbox/data/" + attachmentFileName;
              if (mime.StartsWith("image")) format = "!" + format;
              attachments += string.Format(format, ++attachmentNo, attURL);
            }
            break;
        }
      }
      // save the article body
      // convert string to stream
      if (attachmentNo > 0)
      {
        markdownBody += "\r\n---------------------\r\n" + attachments;
      }
      // generate a file stream with UTF8 characters
      FileStream fw = File.Create(hi.messageID);
      StreamWriter sw = new StreamWriter(fw, System.Text.Encoding.UTF8);
      sw.Write(markdownBody);
      sw.Close();
      sw = null;
    }

    enum LNStates { LNText, LNFile, LNEmpty };
    private void saveBodyButton_Click(object sender, EventArgs e)
    {
      string body = txtBody.Text;
      SaveToFiles(_headerInfo, body);
    }
  }
}
