#include <Es/essencepch.hpp>
#include <Es/Containers/array.hpp>
#include <Es/Containers/boolArray.hpp>

bool IsAnotherEmpty()
{
  typedef PackedBoolAutoArrayTT< FixedArray<PackedBoolArray,1> > RenderFlagsType;
  RenderFlagsType _renderFlags;
  if (_renderFlags.IsEmpty())
  {
    printf("Another is Empty too\n");
    return true;
  }
  return false;
}
