// MakefileGen.cpp : Defines the entry point for the console application.
//

#include <El/elementpch.hpp>
#include <Es/Files/fileContainer.hpp>
#include <Es/Containers/boolArray.hpp>

static bool PrintFile(const FileItem &file, int &dummy)
{
  RString fullPath = file.path + file.filename;
  puts(fullPath);
  return false; // interrupt enumeration
}

extern bool IsAnotherEmpty();

int main(int argc, char* argv[])
{
  typedef PackedBoolAutoArrayTT< FixedArray<PackedBoolArray,1> > RenderFlagsType;
  RenderFlagsType _renderFlags;
  if (_renderFlags.IsEmpty())
    printf("It is Empty\n");
  IsAnotherEmpty();
#ifdef _WIN32
  RString path("w:\\c\\aitools\\graphtest\\");
#else
  RString path("/home/bebul/sandbox/");
#endif
  int ahoj;
  puts("All files:");
  ForEachFileR(path, PrintFile, ahoj);
  puts("*.cpp subset of files:");
  ForMaskedFileR(path, "*.cpp",PrintFile, ahoj);
  
  return 2;
}
