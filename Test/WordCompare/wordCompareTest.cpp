#include <El/Text/wordCompare.hpp>
#include <stdio.h>


int main()
{
	/**/
	static const char *words[] =
	{
		//"Volant", "Volanty", "Volant�", "volant",
		//"Vrtule",
		"Vrtacka", "Vrt��ek", "Vrtak", "Vrt",
		//"Pt�k", "Ptak", "Strom", "Podstrom", "Polostrom", "S",
	};
	const int nWords = sizeof(words)/sizeof(*words);

	for (int i=0; i<nWords; i++) for (int j=0; j<nWords; j++)
	{
		const char *iWord = words[i];
		const char *jWord = words[j];
		float diff = WordDifferenceSubstring(jWord,iWord);
		printf
		(
			"%10s-%-10s = %.4f%s\n",
			jWord,iWord,
			diff,diff<0.3 ? " ***" : ""
		);
	}
	return 0;
}

