#include <Es/essencepch.hpp>
#include "checkMem.hpp"

#ifndef MFC_NEW
#pragma warning(disable:4074)
#pragma init_seg(compiler)

#error MemFunctionsMalloc is currently broken - use MFC_NEW instead

static MemFunctions MemFunctionsMalloc INIT_PRIORITY_URGENT;

MemFunctions *GMemFunctions = &MemFunctionsMalloc;
MemFunctions *GSafeMemFunctions = &MemFunctionsMalloc;
#endif

