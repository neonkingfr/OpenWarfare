#include <Es/essencepch.hpp>
#include <Es/Common/win.h>
#include <Es/Framework/debugLog.hpp>
#include "multisync.hpp"
#include "threadSync.hpp"

#define MT_LOG 0

#ifdef _WIN32

int SignaledObject::WaitForMultiple( SignaledObject *events[], int n, int timeout )
{
	const int maxHandles=32;
	HANDLE handles[maxHandles];
	Assert( n<maxHandles );
	for( int i=0; i<n; i++ ) handles[i]=events[i]->_handle;
	if (timeout<0) timeout=INFINITE;
	DWORD ret=::WaitForMultipleObjects
	(
		n,handles,
		FALSE, // wait for all or wait for one 
		timeout // time-out interval in milliseconds 
	);
	if( ret<WAIT_OBJECT_0 || ret>=WAIT_OBJECT_0+n )
	{
		//ErrorMessage("MsgWaitForMultipleObjects error.");
		return -1; // 
	}
	return ret-WAIT_OBJECT_0;
}

#endif

#ifdef _MT
/*
static unsigned int __stdcall StartThread( void *context )
{
	SecondaryThread *thread=static_cast<SecondaryThread *>(context);
	thread->StartSecondaryBackground();
	LogF("End background thread.");
	return 0;
}
*/
#endif

/*
SecondaryThread::SecondaryThread()
:_secondaryDone(true),_secondaryStarted(true)
{
	_secondaryFunction=NULL;
	_secondaryContext=NULL;
	_primary=NULL;
	_secondary=NULL;
	#ifdef _MT
		// get my own handle
		BOOL res=::DuplicateHandle
		(
			GetCurrentProcess(),GetCurrentThread(),
			GetCurrentProcess(),&_primary,
			0,FALSE,DUPLICATE_SAME_ACCESS
		);
		if( !res ) ErrorMessage("Thread handle not duplicated");
		unsigned int id;
		LogF("Start SecondaryThread Thread.");
		_secondary=(HANDLE)_beginthreadex(NULL,0,StartThread,this,0,&id);
		// _secondary will wait until _secondaryStarted is set
		// make sure _handle is valid before thread runs
		if( !_secondary )
		{
			ErrorMessage("Cannot create Thread.");
		}
	#endif
}

SecondaryThread::~SecondaryThread()
{
	#ifdef _MT
		_terminate.Set();
		if( _secondary )
		{
			::WaitForSingleObject(_secondary,INFINITE); // wait until terminated
			::CloseHandle(_secondary);
			::CloseHandle(_primary);
		}
	#endif
}

#ifdef _MT
void SecondaryThread::StartSecondaryBackground()
{
	for(;;)
	{
		// wait until thread is initialized
		// run callback
		SignaledObject *events[]={&_terminate,&_secondaryStarted};
		int which=SignaledObject::WaitForMultiple(events,2);
		if( which==0 ) break; // terminated
		(*_secondaryFunction)(_secondaryContext);
		// signal function have finished
		_secondaryFunction=NULL;
		_secondaryContext=NULL;
		_secondaryStarted.Reset();
		_secondaryDone.Set(); 
		::SetThreadPriority(_primary,THREAD_PRIORITY_NORMAL);
	}
}

inline void SecondaryThread::SelectPrimary()
{
	::SetThreadPriority(_secondary,THREAD_PRIORITY_IDLE);
	::SetThreadPriority(_primary,THREAD_PRIORITY_NORMAL);
}

inline void SecondaryThread::SelectSecondary()
{
	::SetThreadPriority(_primary,THREAD_PRIORITY_IDLE);
	::SetThreadPriority(_secondary,THREAD_PRIORITY_NORMAL);
}


#endif

// primary interface

void SecondaryThread::StartSecondary( BackgroundFunction *function, void *context )
{
	#ifdef _MT
		#if MT_LOG
			perfLog << "##PRI: Start secondary\n";
		#endif
		// make secondary idle until explicitly allowed to run
		_secondaryFunction=function;
		_secondaryContext=context;
		_secondaryDone.Reset();
		_secondaryStarted.Set();
		SelectPrimary();
	#else
		function(context);
	#endif
}

void SecondaryThread::RunSecondary( DWORD ms )
{
	#ifdef _MT
		DWORD startTime=::GlobalTickCount();
		#if MT_LOG
			perfLog << "##PRI: Run secondary for " << (int)ms << " ms\n";
		#endif
		_secondaryAllowedUntil=startTime+ms;
		SelectSecondary();
		#if MT_LOG
			perfLog << "##PRI: Run secondary took " << int(::GlobalTickCount()-startTime) << " ms\n";
		#endif
	#endif
}

void SecondaryThread::FinishSecondary()
{
	#ifdef _MT

	#if MT_LOG
		DWORD startTime=::GlobalTickCount();
		perfLog << "##PRI: Finish secondary.\n";
	#endif
	// note: _secondary may already be suspended
	SelectSecondary();
	// thread should suspend itself
	_secondaryDone.Wait();
	SelectPrimary();
	#if MT_LOG
		perfLog << "##PRI: Secondary finished in " << int(::GlobalTickCount()-startTime) << " ms\n";
	#endif

	#endif
}

// secondary interface

void SecondaryThread::AllowSwitch()
{
	#ifdef _MT

	// safe: if secondary is not running, it does no harm to call this
	DWORD time=::GlobalTickCount();
	if( time>=_secondaryAllowedUntil )
	{
		::SetThreadPriority(_secondary,THREAD_PRIORITY_IDLE);
		::SetThreadPriority(_primary,THREAD_PRIORITY_NORMAL);
	}

	#endif
}


*/
