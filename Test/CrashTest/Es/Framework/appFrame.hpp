#ifdef _MSC_VER
#pragma once
#endif

#ifndef __APP_FRAME_HPP
#define __APP_FRAME_HPP

#include <stdarg.h>

#ifndef _ENABLE_REPORT
#error _ENABLE_REPORT must be defined when appFrame is used
#endif

// Application framework

//! error level
enum ErrorMessageLevel
{
	//! may be ignored, marginal impact (like only slight performance degradation)
	EMNote,
	//! application is able to continue, but with limited functionality
	EMWarning,
	//! application is not able to perform requested task, but is able to continue
	EMError,
	//! application is not able to continue
	EMCritical,
	//! no error level - used to disable all errors
	EMDisableAll
};

#if _ENABLE_REPORT
void LogF(const char *format,...);
void vaLogF(const char *format, va_list argptr);
void LstF(const char *format,...);
void vaLstF(const char *format, va_list argptr);
void ErrF(const char *format,...);
void vaErrF(const char *format, va_list argptr);
#endif

void LogDebugger(const char *format, va_list argptr);

void ErrorMessage(ErrorMessageLevel level, const char *format,...);
void ErrorMessage(const char *format,...);
void WarningMessage(const char *format,...);

void GlobalShowMessage(int timeMs, const char *msg, ...);
void GlobalDiagMessage(int &handle, int timeMs, const char *msg, ...);
unsigned long GlobalTickCount();

#if _ENABLE_REPORT
	// use Format function to preformat if necessary
	#define DIAG_MESSAGE(time,text) do \
	{	\
		static int handle=-1; \
		GlobalDiagMessage(handle, time, text); \
	} while (false);
#else
	#define DIAG_MESSAGE(time,text)
#endif
// Default implementation

class AppFrameFunctions
{
public:
	AppFrameFunctions() {};
	virtual ~AppFrameFunctions() {};

#if _ENABLE_REPORT
	virtual void LogF(const char *format, va_list argptr);
	virtual void LstF(const char *format, va_list argptr);
	virtual void ErrF(const char *format, va_list argptr);
#endif
	virtual void LogDebugger(const char *format, va_list argptr);

	virtual void ErrorMessage(ErrorMessageLevel level, const char *format, va_list argptr) {LogF(format,argptr);}
	virtual void ErrorMessage(const char *format, va_list argptr) {LogF(format,argptr);}
	virtual void WarningMessage(const char *format, va_list argptr) {LogF(format,argptr);}
	
	virtual void ShowMessage(int timeMs, const char *msg, va_list argptr) {}
	virtual void DiagMessage(int &handle, int timeMs, const char *msg, va_list argptr) {}
	virtual unsigned long TickCount() {return 0;}
};

extern AppFrameFunctions *CurrentAppFrameFunctions;

#endif
