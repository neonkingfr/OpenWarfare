#ifdef _MSC_VER
#pragma once
#endif

#ifndef _QSTREAM_HPP
#define _QSTREAM_HPP

#include <Es/Memory/checkMem.hpp>
#include <Es/Strings/rString.hpp>
#include <Es/Memory/fastAlloc.hpp>
#include <Es/Types/enum_decl.hpp>

DEFINE_ENUM_BEG(LSError)
	LSOK,
	LSFileNotFound, // no such file
	LSBadFile, // error in loaded file (CRC error...)
	LSStructure, // fire structure error - caused by programm bug
	LSUnsupportedFormat, // attempt to load other file format
	LSVersionTooNew, // attempt to load unknown version
	LSVersionTooOld, // attempt to load version that is no longer supported
	LSDiskFull, // cannot save - disk full
	LSAccessDenied, // read only, directory permiss...
	LSDiskError, // some disk error
	LSNoEntry,		// entry in ParamArchive not found
	LSNoAddOn,		// ADDED in patch 1.01 - AddOns check
	LSUnknownError,
DEFINE_ENUM_END(LSError)

typedef size_t QFileSize;
const int QFileSizeMax = INT_MAX;

const char *GetErrorName(LSError err);

class QIOS
{
	public:
	// namespace for definitions
	enum {beg,cur,end};
	enum {binary=1,text=2,in=4};
	typedef int seekdir;
	typedef int openmode;
};

// istream like simple and fast implementaion of file access
class QIStreamSimple
{
	protected:
	char *_buf;
	int _len;
	int _readFrom;
	bool _fail,_eof;
	LSError _error;

	protected:
	void Assign( const QIStreamSimple &src ); // this points to same data as from
	void Close();

	private: // disable copying
	QIStreamSimple &operator = ( const QIStreamSimple &src );
	QIStreamSimple( const QIStreamSimple &src );

	public:
	QIStreamSimple()
	:_buf(NULL),_len(0),_readFrom(0),_fail(true),_eof(false)
	{}
	QIStreamSimple( const void *buf, int len )
	:_buf((char *)buf),_len(len),_readFrom(0),_fail(false),_eof(false)
	{}
	void init( const void *buf, int len )
	{
		_buf=(char *)buf;
		_len=len;
		_readFrom=0;
		_fail=false;
		_eof=false;
	}
	int get()
	{
		// get single character
		if( _readFrom>=_len ) {_eof=true;return EOF;}
		return (unsigned char)_buf[_readFrom++];
	}
	void unget()
	{
		if( _readFrom>0 ) _readFrom--;
	}
	void read( void *buf, int n )
	{
		int left=_len-_readFrom;
		if( n>left )
		{
			if( left==0 ) _eof=true;
			_fail=true;
			return;
		}
		// note: buf and _buf may be unalligned - we cannot avoid this
		memcpy(buf,_buf+_readFrom,n);
		//for( int i=0; i<n; i++ ) buf[i]=_buf[_readFrom+i];
		_readFrom+=n;
	}
	void seekg( int pos, QIOS::seekdir dir )
	{
		int nPos;
		switch( dir )
		{
			case QIOS::beg:
				nPos=pos;
			break;
			case QIOS::end:
				nPos=_len+pos;
			break;
			default:
				nPos=_readFrom+pos;
			break;
		}
		if( nPos<0 || nPos>_len ) _fail=true;
		else _readFrom=nPos,_fail=false;
	}
	bool fail() const {return _fail;}
	bool eof() const {return _eof;}
	LSError error() const {return _error;}

  //! Reads the stream until EOLN (returns true) or EOF (returns false) is reached.
  //! Stops AFTER the EOLN.
  bool nextLine ();

  /**
      Reads the actual line into the supplied buffer.
      @param  buf External buffer.
      @param  bufLen Size of the buffer (including \0 terminator), 0 for unrestricted read.
              Too long lines will be truncated to fit in <code>buffer</code>.
      @return <code>true</code> if the line was read successfully.
  */
  bool readLine ( char *buf, int bufLen =0 );

	int tellg() const {return _readFrom;}

	// caution: following functions are not available in istream
	// use them with care
	const char *act() const {return _buf+_readFrom;}
	int rest() const {return _len-_readFrom;}
};

#include <Es/Memory/normalNew.hpp>

//! one part of data loaded
/*!
This class makes sharing of buffers between various streams and caches possible
*/
class QIStreamBuffer: public RefCount
{
  friend class QIStream;

  protected:
  char *_buffer; //! data buffer
  QFileSize _bufferSize; // data buffer size

  public:
  QIStreamBuffer();
  //QIStreamBuffer(const void *data, int size);
  explicit QIStreamBuffer(int size);

  virtual ~QIStreamBuffer();

  void *DataLock(QFileSize pos, QFileSize size)
  {
    Assert(pos+size<=_bufferSize);
    //_bufLen = size;
    return _buffer+pos;
  }
  void DataUnlock(QFileSize pos, QFileSize size)
  {
    Assert(pos+size<=_bufferSize);
  }
  void FillWithData(const void *data, QFileSize size)
  {
    Assert(size<=_bufferSize);
    memcpy(_buffer,data,size);
  }
  QFileSize ReadData(void *data, QFileSize pos, QFileSize size)
  {
    Assert(pos+size<=_bufferSize);
    memcpy(data,_buffer+pos,size);
    return size;
  }
  __forceinline QFileSize GetSize() const {return _bufferSize;}
  //! change size, but copy "used" data 
  void Realloc(int size, int used);

  USE_FAST_ALLOCATOR

  private:
  //! no copy
  QIStreamBuffer(const QIStreamBuffer &src);
  void operator = (const QIStreamBuffer &src);
};

//! buffer with which we work
struct RefQIStreamBuffer: public Ref<QIStreamBuffer>
{
  QFileSize _start; //!< position of the buffer in source stream
  QFileSize _len; //!< position of the buffer in source stream
  
  RefQIStreamBuffer(QIStreamBuffer *ref, QFileSize pos, QFileSize size)
  :Ref<QIStreamBuffer>(ref),_start(pos),_len(size)
  {
  }
};

//! hide OS specific handle types

typedef struct _fileServerHandle{} *FileServerHandle;

//! interface with the file server

class QIFileServerFunctions
{
  public:

  //! release handle - we need to write the file
  virtual void FlushReadHandle(const char *name);
  //! read single buffer
	virtual RefQIStreamBuffer Read(FileServerHandle handle, QFileSize start, QFileSize size);
  //! open read handle
  virtual FileServerHandle OpenReadHandle(const char *name);
  //! close read handle
  virtual void CloseReadHandle(const char *name, FileServerHandle handle);
  //! check if handle was opened OK
  virtual bool CheckReadHandle(FileServerHandle handle);
  //! get page size used for file buffers
  virtual int GetPageSize();
};

extern QIFileServerFunctions *GFileServerFunctions;


#include <Es/Memory/debugNew.hpp>

//! istream like simple and fast implementaion of file access
class QIStream
{
	protected:
  RefQIStreamBuffer _buf;
  //! position of current read in the buffer - relative to the buffer start
  /*!
  must always be in range 0.._buffer.Size(), both included.
  If _bufLen is nonzero, it must also be in range 0.._bufLen (both included)
  */

	QFileSize _readFromBuf;

	bool _fail,_eof;
	LSError _error;

	private: // disable copying
	QIStream &operator = ( const QIStream &src );
	QIStream( const QIStream &src );

  protected:
  //! source can read more, but it always has to read at least on byte on pos
  virtual RefQIStreamBuffer ReadBuffer(QFileSize pos) = 0; 
  //! get total data size
  virtual QFileSize GetDataSize() const = 0;

  //! forget all information from the buffer, maintain read positions
  void InvalidateBuffer()
  {
    _buf._len = 0;
  }

	QIStream();

	public:
  virtual ~QIStream();

	int get()
	{
		// get single character
		if( _readFromBuf>=_buf._len )
    {
      Assert(_readFromBuf==_buf._len)
      // skip current buffer
      QFileSize actPos = _buf._start+_readFromBuf;

      _buf.Free();
      _buf = ReadBuffer(actPos);

      _readFromBuf = actPos-_buf._start;

			Assert(_readFromBuf<=_buf._len);
  		if( _readFromBuf>=_buf._len )
      {
        // no data to return - return error (eof)
        _eof = true;
        return EOF;
      }
    }
		return (unsigned char)_buf->_buffer[_readFromBuf++];
	}
	void unget()
	{
    Assert(_readFromBuf>0);
		if( _readFromBuf>0 ) _readFromBuf--;
	}
	int read( void *buf, int n )
	{
    int rd = 0;
    for(;;)
    {
      int toRead = n;
  		int left = _buf._len-_readFromBuf;
      if (left<=0)
      {
        Assert(_readFromBuf==_buf._len)
        // skip current buffer

        QFileSize actPos = _buf._start+_readFromBuf;

        _buf.Free();
        _buf = ReadBuffer(actPos);

        _readFromBuf = actPos-_buf._start;

				Assert(_readFromBuf<=_buf._len);
				
        left = _buf._len-_readFromBuf;
        if (left<=0)
        {
          _eof = true;
			    if (rd>0) _fail = true;
			    return rd;
        }
      }
      if (toRead>left) toRead = left;
      // read what we can
  		memcpy(buf,_buf->_buffer+_readFromBuf,toRead);
      rd += toRead;
      _readFromBuf += toRead;
      
			Assert(_readFromBuf<=_buf._len);
			
      n -= toRead;
      if (n==0) return rd;
      // we need to continue - advance all data pointers
      // advance target pointer
      buf = (char *)buf + toRead;
    }
	}
	void seekg( int pos, QIOS::seekdir dir )
	{
		QFileSize nPos;
		QFileSize size = GetDataSize();
		switch( dir )
		{
			case QIOS::beg:
				nPos=pos;
			break;
			case QIOS::end:
        nPos = size+pos;
			break;
			default:
				nPos=_buf._start+_readFromBuf+pos;
			break;
		}
    if (nPos<0) nPos=0;
    if (nPos>size) nPos = size;
    // check if we are withing current buffer
    if (nPos>=_buf._start && nPos<=_buf._start+_buf._len)
    {
      // adjust position within the current buffer
      _readFromBuf = nPos - _buf._start;
			Assert(_readFromBuf<=_buf._len);
    }
    else
    {
      // buffer needs to be invalidated
      // try to keep aligned to buffer size
      _buf._len = 0;
      _buf._start = nPos;
      _readFromBuf = 0;
      _fail = false;
      _eof = false;
    }
	}
	bool fail() const {return _fail;}
  //it is hard to provide eof() with the same semantics as seen in the QIStreamSimple class
	bool eof() const {return _eof;}
	LSError error() const {return _error;}

  //! Reads the stream until EOLN (returns true) or EOF (returns false) is reached.
  //! Stops AFTER the EOLN.
  bool nextLine ();

  /**
      Reads the actual line into the supplied buffer.
      @param  buf External buffer.
      @param  bufLen Size of the buffer (including \0 terminator), 0 for unrestricted read.
              Too long lines will be truncated to fit in <code>buffer</code>.
      @return <code>true</code> if the line was read successfully.
  */
  bool readLine ( char *buf, int bufLen =0 );

  int tellg() const {return _buf._start+_readFromBuf;}
  int rest() const {return GetDataSize()-_buf._start-_readFromBuf;}

  //! copy all data to anything supporting write
  template <class OStr>
  void copy(OStr &out)
  {
    char buf[4*1024];
    for(;;)
    {
      int rd = read(buf,sizeof(buf));
      out.write(buf,rd);
      if (eof() || fail()) break;
    }
  }

  //! pass all data to any given function
  template <class Function>
  void Process(Function f)
  {
    char buf[4*1024];
    for(;;)
    {
      int rd = read(buf,sizeof(buf));
      f(buf,rd);
      if (eof() || fail()) break;
    }
  }
};

struct WriteAutoArrayChar
{
  AutoArray<char> &tgt;

  WriteAutoArrayChar(AutoArray<char> &b):tgt(b){}
  void operator () (const void *buf, int size)
  {
    int oldSize = tgt.Size();
    tgt.Resize(oldSize+size);
    memcpy(tgt.Data()+oldSize,buf,size);
  }
};


/*!
\patch_internal 1.31 Date 11/22/2001 by Jirka
- Added: Encryption using RSA
*/
class QOStream
{
	protected:
  Ref<QIStreamBuffer> _buf;
	QFileSize _writeTo;
	QFileSize _written;

  
	private: // disable copying
	QOStream &operator = ( const QOStream &src );
	QOStream( const QOStream &src );

	public:
	QOStream():_writeTo(0),_written(0){_buf = new QIStreamBuffer(64*1024);}

	void rewind() {_written = 0,_writeTo=0;}
	void setbuffer( int size ) {_buf->Realloc(size,_written);}

	void put( char c )
	{
		if( _writeTo+1>(int)_buf->GetSize() )
    {
      // realloc needed
      _buf->Realloc(_buf->GetSize()*2,_written);
    }
    char *putto = (char *)_buf->DataLock(_writeTo,1);
    *putto = c;
    _buf->DataUnlock(_writeTo,1);
    _writeTo++;

    if (_written<_writeTo)
    {
      _written = _writeTo;
      Assert(_written<=_buf->GetSize());
    }
	}
	void write( const void *buf, int n )
	{
		if( _writeTo+n>(int)_buf->GetSize() )
    {
      int needed = _writeTo+n;
      int size = _buf->GetSize()*2;
      while (size<needed)
      {
        size *= 2;
      }
      // realloc needed
      _buf->Realloc(size,_written);
    }
    void *putto = _buf->DataLock(_writeTo,n);
		memcpy(putto,buf,n);
    _buf->DataUnlock(_writeTo,n);
		_writeTo += n;
    if (_written<_writeTo)
    {
      _written = _writeTo;
      Assert(_written<=_buf->GetSize());
    }
	}

	int tellp() const {return _writeTo;}
	
	void seekp( int pos, QIOS::seekdir dir )
	{
		int nPos;
		switch( dir )
		{
			case QIOS::beg:
				nPos=pos;
			break;
			case QIOS::end:
				nPos=_written+pos;
			break;
			default:
				nPos=_writeTo+pos;
			break;
		}
		_writeTo=nPos;
	}
	// str and pcount: see ostrstream
	const char *str() const {return (const char *)_buf->DataLock(0,_written);} // get data
	int pcount() const {return _written;} // get data size

  //! optimal way to access data without any need to copy them
  RefQIStreamBuffer GetBuffer() const
  {
    return RefQIStreamBuffer(_buf,0,_written);
  }

	QOStream &operator <<(const char *buf)
	{
		write(buf, strlen(buf));
		return *this;
	}
};

class QOFStream: public QOStream
{
protected:
	RString _file;
	bool _fail;
	LSError _error;

public:
	QOFStream():_file(NULL),_fail(false){}

	QOFStream( const char *file )
	:_file(file),_fail(false)
	{}
	void open( const char *file );
	void close();
	void close( const void *header, int headerSize );
	bool fail() const {return _fail;}
	LSError error() const {return _error;}

	~QOFStream(); // perform actual save
};

//#include "multiSync.hpp"

class QFBank;

//! file buffer interface
class IFileBuffer: public RefCount
{
	private:
	//! no copy allowed
	IFileBuffer( const IFileBuffer & );
	//! no copy allowed
	IFileBuffer &operator = ( const IFileBuffer & );

	public:
	IFileBuffer() {}

	virtual bool GetError() const = NULL;
	//! get data
	virtual const char *GetData() const = NULL;
	//! get data size
	virtual int GetSize() const = NULL;
  //! read part of the file
  virtual RefQIStreamBuffer ReadBuffer(QFileSize pos);
  //! read part of the file
  virtual QFileSize Read(void *buf, QFileSize pos, QFileSize size);
	//! check if buffer is from given bank
	virtual bool IsFromBank(QFBank *bank) const = NULL;
	//! check if buffer is from given bank
	virtual FileServerHandle GetHandle() const {return NULL;}
	//! some data sources (overlapped IO) may need some time to get ready
	virtual bool IsReady() const = NULL;
};

class FileBufferMemory: public IFileBuffer
{
	protected:
	Buffer<char> _data;

	public:
	FileBufferMemory() {}
	FileBufferMemory( int size ) {_data.Init(size);}

	void Realloc( int size ) {_data.Init(size);}
	const char *GetData() const {return _data.Data();}
	// non-virtual writable access
	char *GetWritableData() {return _data.Data();}

	int GetSize() const {return _data.Size();}
	bool GetError() const {return false;}
	bool IsFromBank(QFBank *bank) const {return false;}
	bool IsReady() const {return true;}
};

class FileBufferError: public IFileBuffer
{
  public:
	bool GetError() const {return true;}
	//! get data
	const char *GetData() const {return NULL;}
	//! get data size
	int GetSize() const {return 0;}
  //! read part of the file
  RefQIStreamBuffer ReadBuffer(QFileSize pos)
  {
    return RefQIStreamBuffer(new QIStreamBuffer,0,pos);
  }
	//! check if buffer is from given bank
	bool IsFromBank(QFBank *bank) const {return false;}
	//! some data sources (overlapped IO) may need some time to get ready
	bool IsReady() const {return true;}
};


#include "iQFBank.hpp"

//! cooperation between file buffers and file banks
class FileBufferBankFunctions
{
  static QFBankFunctions *_defaultQFBankFunctions;

  public:
  #ifndef _XBOX
	static bool BufferOwned(const QFBank *bank, const FileBufferMapped *buffer)
  {
    return _defaultQFBankFunctions->BufferOwned(bank,buffer);
  }
  #endif
	static bool BufferOwned(const QFBank *bank, const FileBufferOverlapped *buffer)
  {
    return _defaultQFBankFunctions->BufferOwned(bank,buffer);
  }
};

//! QIFileFunctions - namespace would be much more logical, but we are still afraid of namespaces
/*!
Caution: if QIFStream was defined as
class QIFStream: public QIStreamDataSource, public QIFileFunctions,
compiler error in MSVC caused bad automatic copy constructor to be generated.
*/
class QIFileFunctions
{
  public:
	static long TimeStamp( const char *name); //!< get file timestamp
	static bool FileExists( const char *name ); //!< check normal file existence
	static bool FileReadOnly( const char *name ); //!< check if the file can be written or not
  static QFileSize GetFileSize(const char *name); //!< get file size
  static bool Unlink(const char *name); //!< delete file
  static bool CleanUpFile(const char *name); //!< ensure file does not exist
	static bool DirectoryExists(const char *name); //!< check directory existence
};

class QIFStreamSimple: public QIStreamSimple
{
	private:
	Ref<IFileBuffer> _sharedData;

	friend class QIFStreamBSimple;

  
	public:
	QIFStreamSimple(){}
	QIFStreamSimple( const QIFStreamSimple &src ){DoConstruct(src);}
	void operator =( const QIFStreamSimple &src ){DoDestruct();DoConstruct(src);}

	//! open some memory data
	void open( Ref<IFileBuffer> buffer ){OpenBuffer(buffer);}

	//! open some memory data
	void OpenBuffer( Ref<IFileBuffer> buffer );

	IFileBuffer *GetBuffer() const {return _sharedData;}

	void import( const char *name ); // import from file or clipboard
	void open( const char *name, QFileSize offset=0, QFileSize size=QFileSizeMax ); // open and preload file

	void DoDestruct(); // close file and destroy buffer
	~QIFStreamSimple();

	void DoConstruct( const QIFStreamSimple &from ); // close from, open this

};

//! basic most compatible file buffer implementation
/*!
	This implementation does not require any special features like file memory mapping
	or overlapped IO, but general performance is not very good.
*/

class FileBufferLoaded: public FileBufferMemory
{

	public:
	FileBufferLoaded( const char *name, QFileSize offset=0, QFileSize size=QFileSizeMax );
	~FileBufferLoaded();

	const char *GetData() const {return (char *)_data.Data();}
	int GetSize() const {return _data.Size();}
};

#include <Es/Memory/normalNew.hpp>
class QIStreamBufferPage: public QIStreamBuffer
{
  public:
  explicit QIStreamBufferPage(int size);
  ~QIStreamBufferPage();

  USE_FAST_ALLOCATOR
};
#include <Es/Memory/debugNew.hpp>

//! load data as necessary
class FileBufferLoading: public IFileBuffer
{

  //@{ define source data name
  RString _name;
  FileServerHandle _handle;
	QFileSize _size;
  //@}

  public:
	FileBufferLoading( const char *name);
  ~FileBufferLoading();

	const char *GetData() const;
	int GetSize() const;
  RefQIStreamBuffer ReadBuffer(QFileSize pos);

	bool GetError() const;
	FileServerHandle GetHandle() const {return _handle;}
	bool IsFromBank(QFBank *bank) const {return false;}
	bool IsReady() const {return true;}
};

//! subbuffer based on whole buffer
class FileBufferSub: public IFileBuffer
{
	Ref<IFileBuffer> _whole;
	QFileSize _start,_size;

	public:
	FileBufferSub( IFileBuffer *buf, QFileSize start, QFileSize size );

  RefQIStreamBuffer ReadBuffer(QFileSize pos);
  
	const char *GetData() const {return _whole->GetData()+_start;}
	int GetSize() const {return _size;}
	bool GetError() const {return _whole->GetError();}
	bool IsFromBank(QFBank *bank) const {return _whole->IsFromBank(bank);}
	bool IsReady() const {return true;}
};


class SSCompress
{
	enum {N=4096};
	enum {F=18};
	enum {THRESHOLD=2};

	unsigned char text_buf[N + F - 1];
	int match_position,match_len;
	int lsons[N+1],rsons[N+257],dads[N+1];

	void InitTree();
	void InsertNode( int p );
	void DeleteNode( int p );
	
	public:
	bool Skip(long lensb, QIStream &in );
	bool Decode( char *dst, long lensb, QIStream &in );
	void Encode( QOStream &out, const char *dst, long lensb );
	void Encode( QOStream &out, QIStream &in);
};

#ifndef _WIN32
int FileSize ( int handle );
#endif

//! fixed buffer implementation
class QIStrStream: public QIStream
{
  protected:
  virtual RefQIStreamBuffer ReadBuffer(QFileSize pos)
  {
    // no more data to read
    return RefQIStreamBuffer(new QIStreamBuffer,pos,0);
  }
  virtual QFileSize GetDataSize() const
  {
    return _buf._len;
  }

  public:
  QIStrStream(){}
  QIStrStream(const void *data, int size)
  {
    init(data,size);
  }
  void init(const void *data, int size);
  void init(QIStreamBuffer *buf)
  {
    _buf = RefQIStreamBuffer(buf,0,buf->GetSize());
  }
  void init(QOStream &stream);
  
  ~QIStrStream();

};

class QIStreamDataSource: public QIStream
{
  Ref<IFileBuffer> _source;
  
  void Copy(const QIStreamDataSource &src);

  protected:
  virtual RefQIStreamBuffer ReadBuffer(QFileSize pos)
  {
    return _source->ReadBuffer(pos);
  }
  virtual QFileSize GetDataSize() const
  {
    return _source->GetSize();
  }

  public:
  QIStreamDataSource();
  ~QIStreamDataSource();

	QIStreamDataSource &operator = ( const QIStreamDataSource &src );
	QIStreamDataSource( const QIStreamDataSource &src );

  void AttachSource(IFileBuffer *src)
  {
    InvalidateBuffer();
    _source = src;
  }
  IFileBuffer *GetSource() const
  {
    return _source;
  }
	bool IsFromBank(QFBank *bank) const
  {
    return GetSource()->IsFromBank(bank);
  }

};

class QIFStream: public QIStreamDataSource
{
  public:
  //! open file 
	void open( const char *name, QFileSize offset=0, QFileSize size=QFileSizeMax );
  //! close file
  void close();

};


//! determine if file mapping can be used
extern bool GUseFileMapping;

#endif
