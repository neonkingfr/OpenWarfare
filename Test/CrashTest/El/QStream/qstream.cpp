// Quick file stream implementation

#include <El/elementpch.hpp>
//#include "winpch.hpp"

#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>

#ifdef _WIN32
	#include <io.h>
	#include <stdio.h>
	#ifndef ACCESS_ONLY 
//		#include "win.h"
    #include <El/Common/perfLog.hpp>
	#endif
	#ifdef _XBOX
	#define USE_MAPPING 0
	#else
	#define USE_MAPPING 1
	#endif
#else
	#include <stdio.h> 
	#include <unistd.h> 
	#define POSIX_FILES 1
	#ifdef NO_MMAP
	  #define USE_MAPPING 0
	#else
	  #define USE_MAPPING 1
	#endif
#endif

#include "QStream.hpp"

#ifdef POSIX_FILES
	#define NO_FILE(file) ( file<0 )
	#define NO_FILE_SET -1
#else
	#define NO_FILE(file) ( file==NULL )
	#define NO_FILE_SET NULL
#endif

#if USE_MAPPING
// by default file mapping is enabled, when supported on given platform
bool GUseFileMapping = true;
#else
bool GUseFileMapping = false;
#endif

const char *GetErrorName(LSError err)
{
	switch (err)
	{
	case LSOK:
		return "No error";
	case LSFileNotFound:
		return "No such file";
	case LSBadFile:
		return "Bad file (CRC, ...)";
	case LSStructure:
		return "Bad file structure";
	case LSUnsupportedFormat:
		return "Unsupported format";
	case LSVersionTooNew:
		return "Version is too new";
	case LSVersionTooOld:
		return "Version is too old";
	case LSDiskFull:
		return "No such file";
	case LSAccessDenied:
		return "Access denied";
	case LSDiskError:
		return "Disk error";
	case LSNoEntry:
		return "No entry";
	default:
		Fail("LSError");
	case LSUnknownError:
		return "Unknown error";
	}
}

void QIStreamSimple::Close()
{
	_buf=NULL;
	_len=0;
	_readFrom=0;
	_fail=true,_eof=false;
}

void QIStreamSimple::Assign( const QIStreamSimple &src )
{
	// this points to same data as from
	_buf=src._buf;
	_len=src._len;
	_readFrom=src._readFrom;
	_fail=src._fail,_eof=src._eof;
}

bool QIStreamSimple::nextLine ()
{
    int c1;
    while ( !eof() )
        if ( (c1 = get()) == 0x0D || c1 == 0x0A ) {
            if ( !eof() && c1 == 0x0D ) {
                int c2 = get();
                if ( c2 != 0x0A ) unget();
                }
            return true;
            }
    return false;
}

bool QIStreamSimple::readLine ( char *buf, int bufLen )
{
    Assert( buf );
    int left = bufLen - 1;                  // regular chars to read
    int c1;
    while ( !eof() ) {
        if ( (c1 = get()) == 0x0D || c1 == 0x0A ) {
                // EOLN:
            if ( !eof() && c1 == 0x0D ) {
                int c2 = get();
                if ( c2 != 0x0A ) unget();
                }
            *buf = (char)0;
            return true;
            }
            // regular char:
        if ( bufLen > 0 && left == 0 ) {    // buffer overflow
            *buf = (char)0;
            return nextLine();
            }
        *buf++ = c1;
        left--;
        }
    *buf = (char)0;
    return false;                           // EOF reached before EOLN
}

void QIFStreamSimple::DoConstruct( const QIFStreamSimple &from )
{
	_sharedData=from._sharedData;
	// make this to contains same data as from
	QIStreamSimple::Assign(from);
}

#ifndef _WIN32

int FileSize ( int handle )
{
	struct stat buf;
	fstat(handle,&buf);
	return buf.st_size;
}

#endif

#include "fileMapping.hpp"

#ifndef POSIX_FILES
  #include "fileCompress.hpp"
#endif

/*!
Default implementation - assume you can access whole file.
In older implementations of IFileBuffer GetData() and GetSize() were the only way to get data
This is 
*/
RefQIStreamBuffer IFileBuffer::ReadBuffer(QFileSize pos)
{
  const char *wholeData = GetData();
  QFileSize wholeSize = GetSize();
  if (pos>=wholeSize) return RefQIStreamBuffer(new QIStreamBuffer,pos,0);

  // optimizal buffer size is system dependednt variable
  // on XBox and PC (Intel 32b) this is 4 KB
  int size = 4*1024;
  QFileSize maxSize = wholeSize-pos;
  if (size>maxSize) size=maxSize;

  QIStreamBuffer *buf = new QIStreamBuffer(size);

  buf->FillWithData(wholeData+pos,size);

  return RefQIStreamBuffer(buf,pos,size);
}

/*!
Read given data region using ReadBuffer
*/
QFileSize IFileBuffer::Read(void *buf, QFileSize pos, QFileSize size)
{
  QFileSize rdTotal = 0;
  char *dta = (char *)buf;
  while (size>0)
  {
    RefQIStreamBuffer buffer = ReadBuffer(pos);

    int bufferPos = pos-buffer._start;
    int readSize = buffer._len-bufferPos;
    if (readSize>size) readSize = size;
    QFileSize rd = buffer->ReadData(dta,bufferPos,readSize);

    if (rd<=0) break;
    size -= rd;
    dta += rd;
    pos += rd;
    rdTotal += rd;
  }
  return rdTotal;
}

FileBufferLoaded::FileBufferLoaded( const char *name, QFileSize offset, QFileSize size)
{
	#ifdef POSIX_FILES
	#ifndef _WIN32
	LocalPath(fn,name);
	int file=::open(fn,O_RDONLY);
	#else
	int file=::open(name,O_RDONLY|O_BINARY);
	#endif
	if( file>=0 )
	{
		int sizeMax=FileSize(file);
		if( sizeMax!=-1 )
		{
			if (size>sizeMax-offset) size = sizeMax-offset;
			_data.Init(size);
			::lseek(file,offset,SEEK_CUR);
			int sizeRead=::read(file,_data.Data(),size);
			if( sizeRead!=size )
			{
				_data.Delete();
				::WarningMessage("File '%s' read error",name);
			}
		}
		else
		{
			//::WarningMessage("File '%s' not found.",name);
		}
		::close(file);
	}
	#else
	FileServerHandle file = GFileServerFunctions->OpenReadHandle(name);
	if( GFileServerFunctions->CheckReadHandle(file))
	{
		DWORD sizeMax=::GetFileSize(file,NULL);
		if( sizeMax>0 && sizeMax!=0xffffffff )
		{
			if (size>sizeMax-offset) size = sizeMax-offset;
			_data.Init(size);
			DWORD sizeRead;
			::SetFilePointer(file,offset,NULL,FILE_CURRENT);
			::ReadFile(file,_data.Data(),size,&sizeRead,NULL);
			if( sizeRead!=size )
			{
				_data.Delete();
				::WarningMessage("File '%s' read error",name);
			}
		}
		GFileServerFunctions->CloseReadHandle(name,file);
	}	
	#endif
}

FileBufferLoaded::~FileBufferLoaded()
{
	_data.Delete();
}

FileBufferSub::FileBufferSub( IFileBuffer *buf, QFileSize start, QFileSize size )
:_whole(buf)
{
	QFileSize bufSize = buf->GetSize();
	if (start>bufSize) start = bufSize;
  QFileSize maxSize = bufSize-start;
  if (size>maxSize) size = maxSize;
  
	_start = start;
	_size = size;
}

RefQIStreamBuffer FileBufferSub::ReadBuffer(QFileSize pos)
{
  if (pos>_size) pos = _size;
  RefQIStreamBuffer buf = _whole->ReadBuffer(_start+pos);
  // we may need to skrink data a little bit
  Assert(buf._start<=_start+_size)
  QFileSize maxEnd = _start+_size-buf._start;
  QFileSize end = buf._len;
  if (end>maxEnd)
  {
    end = maxEnd;
  }

  return RefQIStreamBuffer(buf,buf._start-_start,end);
}

FileBufferLoading::FileBufferLoading( const char *name)
{
  _name = name;

	FileServerHandle handle = GFileServerFunctions->OpenReadHandle(name);
	if( GFileServerFunctions->CheckReadHandle(handle))
  {
		DWORD fileSize = ::GetFileSize(handle,NULL);

    // adjust offset and size based on actual file size
    _handle = handle;
    _size = fileSize;
  }
  else
  {
    _handle = NULL;
    _size = 0;
  }
    
}
FileBufferLoading::~FileBufferLoading()
{
  if (GFileServerFunctions->CheckReadHandle(_handle))
  {
    GFileServerFunctions->CloseReadHandle(_name,_handle);
  }
  _handle = NULL;
}

const char *FileBufferLoading::GetData() const
{
  Fail("Cannot return data");
  return NULL;
}
int FileBufferLoading::GetSize() const
{
  return _size;
}


DEFINE_FAST_ALLOCATOR(QIStreamBufferPage)

QIStreamBufferPage::QIStreamBufferPage(int size)
{
  // allocate one page
	int pageSize = GFileServerFunctions->GetPageSize();
  Assert(size ==pageSize);
  _buffer = (char *)NewPage(pageSize,pageSize);
  if (!_buffer)
  {
    _bufferSize = 0;
    // error - no memory
  }
  else
  {
    _bufferSize = size;
  }
}
QIStreamBufferPage::~QIStreamBufferPage()
{
  // release one page
  if (_buffer)
  {
		int pageSize = GFileServerFunctions->GetPageSize();
    DeletePage(_buffer,pageSize);
	  _buffer = NULL;
  }
  _bufferSize = 0;
}

RefQIStreamBuffer QIFileServerFunctions::Read(FileServerHandle handle, QFileSize start, QFileSize size)
{
  int pageSize =  GFileServerFunctions->GetPageSize();
  Assert(size<=pageSize);

  QIStreamBuffer *buffer = new QIStreamBufferPage(pageSize);

  void *data = buffer->DataLock(0,size);
  if (!data)
  {
    //! error - out of memory
    return RefQIStreamBuffer(buffer,start,0);
  }

  HANDLE filehandle = (HANDLE)handle;
  ::SetFilePointer(filehandle,start,NULL,FILE_BEGIN);
  DWORD rd = 0;
  ::ReadFile(filehandle,data,size,&rd,NULL);
  buffer->DataUnlock(0,rd);

  return RefQIStreamBuffer(buffer,start,rd);
}

int QIFileServerFunctions::GetPageSize()
{
  /*
  SYSTEM_INFO info;
  GetSystemInfo(&info);
  return info.dwPageSize;
  */
  // unless some file server is used, there any page size will do
  // ask memory manager what page size it likes
  return GetPageRecommendedSize();
}

void QIFileServerFunctions::FlushReadHandle(const char *name)
{
}

FileServerHandle QIFileServerFunctions::OpenReadHandle(const char *name)
{
	return (FileServerHandle)::CreateFile
	(
		name,GENERIC_READ,FILE_SHARE_READ,
    NULL, // security
    OPEN_EXISTING,
    FILE_ATTRIBUTE_NORMAL,
    NULL // template
  );
}
void QIFileServerFunctions::CloseReadHandle(const char *name, FileServerHandle handle)
{
	::CloseHandle((FileServerHandle)handle);
}

bool QIFileServerFunctions::CheckReadHandle(FileServerHandle handle)
{
  HANDLE winHandle = (HANDLE)handle;
  return winHandle!=NULL && winHandle!=INVALID_HANDLE_VALUE;
}



bool FileBufferLoading::GetError() const
{
  return !GFileServerFunctions->CheckReadHandle(_handle);
}

RefQIStreamBuffer FileBufferLoading::ReadBuffer(QFileSize pos)
{
  if (pos>_size) pos = _size;

  int pageSize = GFileServerFunctions->GetPageSize();

  pos &= ~(pageSize-1);


	Assert(_size>=pos);
  QFileSize maxSize = _size - pos;

  QFileSize size = pageSize;
  if (size>maxSize) size = maxSize;

  return RefQIStreamBuffer(GFileServerFunctions->Read(_handle,pos,size));
}

#if _XBOX
const char *FullXBoxName(const char *name, char *temp)
{
	if (!name[0] || name[1]!=':')
	{
		strcpy(temp,"d:\\");
		strcat(temp,name);
		return temp;
	}
	else
	{
		return name;
	}
}
#endif

#if USE_MAPPING
Ref<IFileBuffer> GetFileBufferMappedPart(const char *fullName, QFileSize offset, QFileSize size)
{
	// round offset, cr
	if (offset==0)
	{
		return new FileBufferMapped(fullName,0,size);
	}
	SYSTEM_INFO info;
	GetSystemInfo(&info);
	int page = info.dwAllocationGranularity;
	int align = offset % page;
	Ref<IFileBuffer> aligned = new FileBufferMapped(fullName,offset-align,size+align);
	return new FileBufferSub(aligned,align,size);
}
#endif


void QIFStreamSimple::open( const char *name, QFileSize offset, QFileSize size )
{
	//ScopeLock lock(_serialize);
	_fail=true;
	_error=LSUnknownError;
	// open and preload file
	#if USE_MAPPING
  if (GUseFileMapping)
  {
		_sharedData = GetFileBufferMappedPart(name,offset,size);
  }
  else
	#endif
  {
		#if _XBOX
			char temp[1024];
			name = FullXBoxName(name,temp);
		#endif
		_sharedData = new FileBufferLoaded(name,offset,size);
  }
	if (!_sharedData->GetError())
	{
		QIStreamSimple::init(_sharedData->GetData(),_sharedData->GetSize());
	}
}

void QIFStreamSimple::OpenBuffer( Ref<IFileBuffer> buffer )
{
	_error=LSUnknownError;
	// attach stream to some memory data
	_sharedData=buffer;
	QIStreamSimple::init(_sharedData->GetData(),_sharedData->GetSize());
}

void QIFStreamSimple::DoDestruct()
{
	// destroy buffer
	_sharedData.Free();
	_len=_readFrom=0;
}

QIFStreamSimple::~QIFStreamSimple()
{
	DoDestruct();
} // close file and destroy buffer

int CmpStartStr( const char *str, const char *start )
{
	while( *start )
	{
		if( myLower(*str++)!=myLower(*start++) ) return 1;
	}
	return 0;
}

bool QIFileFunctions::FileReadOnly( const char *name )
{
	// file exists and is read only
	#ifdef POSIX_FILES
	LocalPath(fn,name);
	struct stat st;
	if ( stat(fn,&st) ) return false;
	return( (st.st_mode & S_IWUSR) == 0 );
	#else
	DWORD attrib=::GetFileAttributes(name);
	// check for cases where write would fail
	if( attrib&FILE_ATTRIBUTE_READONLY ) return true;
	if( attrib&FILE_ATTRIBUTE_DIRECTORY ) return true;
	if( attrib&FILE_ATTRIBUTE_SYSTEM ) return true;
	return false; // no file
	#endif
}

long QIFileFunctions::TimeStamp( const char *name)
{
	#ifdef POSIX_FILES
		LocalPath(fn,name);
		int file=::open(fn,O_RDONLY);
		if( NO_FILE(file) ) return 0;
		::close(file);
		stat st;
		fstat(file,&st)
		return st.st_mtime;
	#elif _XBOX
		return 0;
	#else
		FileServerHandle check = GFileServerFunctions->OpenReadHandle(name);
		if( check==INVALID_HANDLE_VALUE ) return false;
		FILETIME filetime;
		::GetFileTime(check,NULL,NULL,&filetime);
		WORD date,time;
		FileTimeToDosDateTime(&filetime,&date,&time);
		GFileServerFunctions->CloseReadHandle(name,check);
		return (date<<16)|time;
	#endif
}

QFileSize QIFileFunctions::GetFileSize(const char *name)
{
	#ifdef POSIX_FILES
		LocalPath(fn,name);
		int file=::open(fn,O_RDONLY);
		if( NO_FILE(file) ) return 0;
		::close(file);
		stat st;
		fstat(file,&st)
		return st.st_size;
	#else
		FileServerHandle check = GFileServerFunctions->OpenReadHandle(name);
		if( check==INVALID_HANDLE_VALUE ) return 0;
		DWORD size = ::GetFileSize(check,NULL);
		GFileServerFunctions->CloseReadHandle(name,check);
    if (size==0xffffffff) return 0;
		return size;
	#endif
}

bool QIFileFunctions::FileExists( const char *name )
{
	// check normal file existence
	#ifdef POSIX_FILES
	LocalPath(fn,name);
	int file=::open(fn,O_RDONLY);
	if( NO_FILE(file) ) return false;
	::close(file);
	#else
	#if _XBOX
		char temp[1024];
		name = FullXBoxName(name,temp);
	#endif
	FileServerHandle check=GFileServerFunctions->OpenReadHandle(name);
	if( check==INVALID_HANDLE_VALUE ) return false;
	GFileServerFunctions->CloseReadHandle(name,check);
	#endif
	return true;
}

bool QIFileFunctions::Unlink(const char *name)
{
#ifdef POSIX_FILES
	LocalPath(fn, name);
	name = fn;
#endif
	GFileServerFunctions->FlushReadHandle(name);
	chmod(name, _S_IREAD | _S_IWRITE);
	return unlink(name) == 0;
}

bool QIFileFunctions::CleanUpFile(const char *name)
{
	if (!FileExists(name)) return true;
	return Unlink(name);
}

bool QIFileFunctions::DirectoryExists(const char *name)
{
	return _access(name, 6) != -1;
}

#define WIN_DIR '\\'
#define UNIX_DIR '/'

#if __GNUC__
	#define INVAL_DIR WIN_DIR
	#define VAL_DIR UNIX_DIR
#else
	#define INVAL_DIR UNIX_DIR
	#define VAL_DIR WIN_DIR
#endif

// helper: open in file or in file bank
static RString ConvertFileName( const char *name, int inval, int valid )
{
	if( !strchr(name,inval) ) return name; // no conversion required
	// convert directory characters depending on platform
	char cname[512];
	strcpy(cname,name);
	for( char *cc=cname; *cc; cc++ )
	{
		if( *cc==inval ) *cc=valid;
	}
	return cname;
}

inline RString PlatformFileName( const char *name )
{
	return ConvertFileName(name,INVAL_DIR,VAL_DIR);
}

inline RString UniversalFileName( const char *name )
{ // filenames are normally stored with backslash '\\'
	return ConvertFileName(name,UNIX_DIR,WIN_DIR);
}

void QOFStream::open( const char *file )
{
	_file=PlatformFileName(file);
	_fail=false;
	_error=LSUnknownError;
	rewind();
}

#ifndef POSIX_FILES
static LSError LSErrorCode( DWORD eCode )
{
	switch( eCode )
	{
		case ERROR_HANDLE_DISK_FULL: return LSDiskFull;
		case ERROR_NETWORK_ACCESS_DENIED:
		case ERROR_LOCK_VIOLATION:
		case ERROR_SHARING_VIOLATION:
		case ERROR_WRITE_PROTECT:
		case ERROR_ACCESS_DENIED: return LSAccessDenied;
		case ERROR_FILE_NOT_FOUND: return LSFileNotFound;
		case ERROR_READ_FAULT:
		case ERROR_WRITE_FAULT:
		case ERROR_CRC: return LSDiskError;
		default:
		{
			#ifndef _XBOX
			char buffer[256];
			FormatMessage
			(
				FORMAT_MESSAGE_FROM_SYSTEM,
				NULL, // source
				eCode, // requested message identifier 
				0, // languague
				buffer,sizeof(buffer),
				NULL
			);
			Log("Unknown error %d %s",eCode,buffer);
			#else
			Log("Unknown error %d",eCode);
			#endif
			
		}
		return LSUnknownError;
	}
}

#endif

void QIFStreamSimple::import( const char *name )
{
	// import from file or clipboard
	// easy way: file import
	open(name);
	// TODO: hard way: windows clipboard
}

/*!
\patch 1.85 Date 9/9/2002 by Jirka
- Fixed: Writing into big file sometimes failed with error #1450:
  "Insufficient system resources exist to complete the requested service."
*/
void QOFStream::close( const void *header, int headerSize )
{
	if( !_file ) return; // no file
	// open and preload file
	_error=LSOK;
	#ifdef POSIX_FILES
	#ifndef _WIN32
	LocalPath(fn,(const char*)_file);
	int file=::open(fn,O_CREAT|O_WRONLY|O_TRUNC,S_IREAD|S_IWRITE);
	#else
	int file=::open(_file,O_CREAT|O_BINARY|O_WRONLY|O_TRUNC,S_IREAD|S_IWRITE);
	#endif
	if( file )
	{
		int sizeWritten=::write(file,header,headerSize);
		if( sizeWritten!=headerSize )
		{
			_fail=true;
			goto Error;
		}
		sizeWritten=::write(file,_buffer.Data(),_buffer.Size());
		if( sizeWritten!=_buffer.Size() )
		{
			_fail=true;
			goto Error;
		}
		Error:
		int eCode=0;
		int success=::close(file);
		if( success<0 ) _fail=true;
		if( _fail )
		{
			_error=LSUnknownError;
		}
	}
	#else
	// check if file is not open for reading
	GFileServerFunctions->FlushReadHandle(_file);

	DWORD eCode=0;
	HANDLE file=::CreateFile
	(
		_file,GENERIC_WRITE,0,
    NULL, // security
    CREATE_ALWAYS,
    FILE_ATTRIBUTE_NORMAL,
    NULL // template
    );
	if( file!=INVALID_HANDLE_VALUE )
	{
		DWORD sizeWritten;
		::WriteFile(file,header,headerSize,&sizeWritten,NULL);
		if( (int)sizeWritten!=headerSize )
		{
			_fail=true;
			goto Error;
		}
		// FIX: write only 1MB at once
		{
			int size = _written;
			const char *data = (const char *) _buf->DataLock(0,size);
			// write 1 MB at once
			static const int writeAtOnce = 1024 * 1024;
			while (size > writeAtOnce)
			{
				::WriteFile(file,data,writeAtOnce,&sizeWritten,NULL);
				if ((int)sizeWritten != writeAtOnce)
				{
					_fail=true;
					goto Error;
				}
				data += writeAtOnce;
				size -= writeAtOnce;
			}
			::WriteFile(file,data,size,&sizeWritten,NULL);
			_buf->DataUnlock(0,size);
			if ((int)sizeWritten != size)
			{
				_fail=true;
				goto Error;
			}
		}
		/*
		::WriteFile(file,_buffer.Data(),_buffer.Size(),&sizeWritten,NULL);
		if( (int)sizeWritten!=_buffer.Size() )
		{
			_fail=true;
			goto Error;
		}
		*/
		Error:
		DWORD eCode=0;
		if( _fail ) eCode=::GetLastError();
		BOOL success=::CloseHandle(file);
		if( !success )
		{
			if( eCode==0 ) eCode=::GetLastError();
			_fail=true;
		}
	}
	else
	{
		eCode=::GetLastError();
		_fail=true;
	}
	if( eCode )
	{
		_error=LSErrorCode(eCode);
	}
	#endif
	// forget any data
	rewind();
	_file=NULL;
}

void QOFStream::close()
{
	close(NULL,0);
}

QOFStream::~QOFStream()
{
}

DEFINE_FAST_ALLOCATOR(QIStreamBuffer)

QIStreamBuffer::QIStreamBuffer(int size)
{
	_bufferSize = size;
  _buffer = new char[size];
}

/*
QIStreamBuffer::QIStreamBuffer(const void *data, int size)
{
	_bufferSize = size;
  _buffer = new char[size];
  memcpy(_buffer,data,size);
}
*/

void QIStreamBuffer::Realloc(int size, int used)
{
  if (_bufferSize==size) return;
  if (used>0)
  {
    Assert(_bufferSize>=used);
    char *oldBuffer = _buffer;
    _buffer = new char[size];
    memcpy(_buffer,oldBuffer,used);
    delete oldBuffer;
  }
  else
  {
    if (_buffer) delete _buffer;
    _buffer = new char[size];
  }
  _bufferSize = size;
}

QIStreamBuffer::QIStreamBuffer()
{
  // create an empty buffer
  _bufferSize = 0; // default buffer size - one XBox/PCx86 system page
  _buffer = NULL;
}
QIStreamBuffer::~QIStreamBuffer()
{
  if (_buffer)
  {
    delete[] _buffer;
    _buffer = NULL;
  }
}


QIStream::QIStream()
:_buf(new QIStreamBuffer,0,0)
{
	_readFromBuf = 0;

	_fail = false;
  _eof = false;
	_error = LSOK;
}

QIStream::~QIStream()
{
  // make sure buffer was released by the implementation
  Assert(_buf==NULL);
}


bool QIStream::nextLine ()
{
  for(;;)
  {
    int c1 = get();
    if (c1==EOF) return false;
    if ( c1== 0x0D || c1 == 0x0A )
    {
      if (c1 == 0x0D )
      {
        int c2 = get();
        if ( c2 != 0x0A && c2!=EOF ) unget();
      }
      return true;
    }
  }
}

bool QIStream::readLine ( char *buf, int bufLen )
{
  Assert( buf );
  int left = bufLen - 1;                  // regular chars to read
  for(;;)
  {
    int c1 = get();
    if (c1==EOF)
    {
      *buf = (char)0;
      return false;                           // EOF reached before EOLN
    }
    if ( c1== 0x0D || c1 == 0x0A )
    {
      // EOLN:
      if ( c1 == 0x0D )
      {
        int c2 = get();
        if ( c2 != 0x0A && c2!=EOF ) unget();
      }
      *buf = (char)0;
      return true;
    }
    // regular char:
    if ( bufLen > 0 && left == 0 )
    {    // buffer overflow
      *buf = (char)0;
      return nextLine();
    }
    *buf++ = c1;
    left--;
  }
}

#include <Es/Memory/normalNew.hpp>
class QIStreamBufferTemp: public QIStreamBuffer
{
  public:
  QIStreamBufferTemp(const void *data, int size);
  ~QIStreamBufferTemp();

  USE_FAST_ALLOCATOR
};
#include <Es/Memory/debugNew.hpp>

DEFINE_FAST_ALLOCATOR(QIStreamBufferTemp)

QIStreamBufferTemp::QIStreamBufferTemp(const void *data, int size)
{
  _buffer = (char *)data;
  _bufferSize = size;
}
QIStreamBufferTemp::~QIStreamBufferTemp()
{
  _buffer = NULL;
  _bufferSize = 0;
}


void QIStrStream::init(const void *data, int size)
{
  // data are touched by ReadData, but our ReadData will not touch them
  _buf = RefQIStreamBuffer(new QIStreamBufferTemp(data,size),0,size);
  _readFromBuf = 0;
}

void QIStrStream::init(QOStream &stream)
{
  _buf = stream.GetBuffer();
}


QIStrStream::~QIStrStream()
{
  _buf = RefQIStreamBuffer(NULL,0,0);
}

void QIStreamDataSource::Copy(const QIStreamDataSource &src)
{
  _buf = src._buf;
  _source = src._source;

	_readFromBuf = src._readFromBuf;

	_fail = src._fail;
  _eof = src._eof;
	_error = src._error;
}

QIStreamDataSource &QIStreamDataSource::operator = ( const QIStreamDataSource &src )
{
  Copy(src);
  return *this;
}
QIStreamDataSource::QIStreamDataSource( const QIStreamDataSource &src )
{
  Copy(src);
}


QIStreamDataSource::QIStreamDataSource()
{
}
QIStreamDataSource::~QIStreamDataSource()
{
  _buf = RefQIStreamBuffer(NULL,0,0);
}

void QIFStream::open(const char *dta, QFileSize offset, QFileSize size)
{
	Assert(!GetSource());
	_fail = true;
	InvalidateBuffer();
	_buf._start = 0;
	_readFromBuf = 0;

	#if _XBOX
	char temp[1024];
	dta = FullXBoxName(dta,temp);
	#endif
	// open file handle

	Ref<IFileBuffer> whole = new FileBufferLoading(dta);
	if (whole->GetError())
	{
		InvalidateBuffer();
		AttachSource(new FileBufferError);
		_fail = true;
	}
	else
	{
		QFileSize wholeSize = whole->GetSize();
		if (offset>0 || size<wholeSize)
		{
			Ref<IFileBuffer> buffer = new FileBufferSub(whole,offset,size);
			Assert (!buffer->GetError());
			InvalidateBuffer();
			AttachSource(buffer);
			_fail = false;
		}
		else
		{
			InvalidateBuffer();
			AttachSource(whole);
			_fail = false;
		}
	}
}

void QIFStream::close()
{
  InvalidateBuffer();
  AttachSource(NULL);
}


