#include <El/elementpch.hpp>

#include "fileMapping.hpp"

#if defined _WIN32
#if !defined _XBOX

#include "QStream.hpp"

void FileBufferMapped::Open( HANDLE fileHandle, int start=0, int size=INT_MAX )
{
  DoAssert(GUseFileMapping);
	int fileSize = ::GetFileSize(fileHandle,NULL);

	saturate(start,0,fileSize);
	saturate(size,0,fileSize-start);

	_mapHandle = ::CreateFileMapping
	(
		fileHandle,NULL,
		PAGE_READONLY,
		0,0, // all file
		NULL
	);
	if (_mapHandle)
	{
		_view = ::MapViewOfFile
		(
			_mapHandle,FILE_MAP_READ,0,start,size
		);
		if (!_view)
		{
			HRESULT hr = GetLastError();
			LogF("Error %x",hr);
		}
		_size = size;
	}
}

FileBufferMapped::FileBufferMapped( HANDLE file, int start, int size )
{
  DoAssert(GUseFileMapping);
	_size = 0;
	_mapHandle = NULL;
	_view = NULL;
	_fileHandle = NULL;
	if (size<=0) return; // zero sized - no data
	
	Open(file,start,size);
}

FileBufferMapped::FileBufferMapped( const char *name, int start, int size )
{
  DoAssert(GUseFileMapping);
	_size = 0;
	_mapHandle = NULL;
	_view = NULL;
	_fileHandle = NULL;
	if (size<=0) return; // zero sized - no data
	_fileHandle=::CreateFile
	(
		name,GENERIC_READ,
		//0, // exclusive access
		FILE_SHARE_READ, // enable reading
    NULL, // security
    OPEN_EXISTING,
    FILE_ATTRIBUTE_NORMAL,
    NULL // template
  );
	if( _fileHandle==INVALID_HANDLE_VALUE)
	{
		_fileHandle = NULL;
	}
	else
	{
		Open(_fileHandle,start,size);
	}
}

FileBufferMapped::~FileBufferMapped()
{
	if (_view) ::UnmapViewOfFile(_view), _view = NULL;
	if (_fileHandle) ::CloseHandle(_fileHandle),_fileHandle = NULL;
	if (_mapHandle) ::CloseHandle(_mapHandle),_mapHandle = NULL;
}

/*!
\patch_internal 1.11 Date 07/30/2001 by Ondra
- Fixed: when file mapping failed, application may crash.
*/

bool FileBufferMapped::GetError() const
{
	// FIX: no view opened
	return _mapHandle==NULL || _view==NULL;
	// FIX END
}


bool FileBufferMapped::IsFromBank(QFBank *bank) const
{
	return FileBufferBankFunctions::BufferOwned(bank,this);
}

bool FileBufferMapped::IsReady() const
{
	return true;
}

HANDLE FileBufferMapped::GetFileHandle() const
{
	return _fileHandle;
}

#endif  // !defined _XBOX

#else

    // POSIX implementation:
#include <sys/mman.h>
#include "QStream.hpp"

void FileBufferMapped::Open ( HANDLE fileHandle, int start, int size )
{
	int fileSize = FileSize(fileHandle);

	saturate(start,0,fileSize);
	saturate(size,0,fileSize-start);

    _view = mmap(0,size,PROT_READ,MAP_PRIVATE,fileHandle,start);
    if ( _view == MAP_FAILED )
        _view = NULL;
    else {
        _size = size;
#ifdef LOG_FILEMAP
        LogF("OK mapping: start=%d, size=%d",start,size);
#endif
	}
}

FileBufferMapped::FileBufferMapped ( HANDLE file, int start, int size )
{
	_size = 0;
	_view = NULL;
	_fileHandle = 0;
	if ( size <= 0 ) return; // zero sized - no data
	
	Open(file,start,size);
}

FileBufferMapped::FileBufferMapped ( const char *name, int start, int size )
{
	_size = 0;
	_view = NULL;
	_fileHandle = 0;
	if ( size <= 0 ) return; // zero sized - no data
	LocalPath(fn,name);
	_fileHandle = open(fn,O_RDONLY);
	if ( _fileHandle < 0 )
		_fileHandle = 0;
	else {
#ifdef LOG_FILEMAP
		LogF("Mapping: '%s'",fn);
#endif
		Open(_fileHandle,start,size);
		}
}

FileBufferMapped::~FileBufferMapped()
{
	if ( _view )
        munmap(_view,_size), _size = 0, _view = NULL;
	if ( _fileHandle )
        close(_fileHandle), _fileHandle = 0;
}

bool FileBufferMapped::GetError() const
{
	return( _view == NULL );
}

bool FileBufferMapped::IsFromBank ( QFBank *bank ) const
{
	return QIFStream::_defaultQFBankFunctions->BufferOwned(bank,this);
}

bool FileBufferMapped::IsReady () const
{
	return true;
}

HANDLE FileBufferMapped::GetFileHandle() const
{
	return _fileHandle;
}

#endif
