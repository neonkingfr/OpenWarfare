#ifdef _MSC_VER
#pragma once
#endif

#ifndef _DEBUG_TRAP_HPP
#define _DEBUG_TRAP_HPP

class DebugThreadWatch;

class Debugger
{
	bool _isDebugger;
  bool _enableThreadWatch;

	SRef<DebugThreadWatch> _watch;

public:
	Debugger();
	~Debugger();

	bool IsDebugger() const {return _isDebugger;}
	void ForceLogging();
	void ProcessAlive();
	void NextAliveExpected( int timeout );

	bool CheckingAlivePaused();

	void PauseCheckingAlive();
	void ResumeCheckingAlive();

protected:
	void StartWatchThread();
};

#define BREAK() {static bool disableBreak;if (!disableBreak) __asm {int 3};}

extern Debugger GDebugger;

#endif
