#ifndef __IMEXHND_H__
#define __IMEXHND_H__
//-----------------------------------------------------------------------------
//
// FILE: IMEXHND.H
//
//
// (c) Interactive Magic (1997)
//
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------
// Version tracking information
//-----------------------------------------------------------------------

#include <Es/Common/win.h>
#include <El/Interfaces/iAppInfo.hpp>

class MapFile;

//! information about single call
struct CallInfo
{
	void *retAddr; //!< return address
	void *fStart; //!< address of function containing return address
	void *calledAddr; //!< called address found at given return address
};

TypeIsSimple(CallInfo)

class DebugExceptionTrap
{
	TCHAR m_szLogFileName[MAX_PATH];
	LPTOP_LEVEL_EXCEPTION_FILTER m_previousFilter;
	HANDLE m_hReportFile;
	bool m_header;
	bool m_configHeader;
	void *m_stackBottom;

	enum {MaxCalls = 2048};

	//! callstack information
	CallInfo _callstack[MaxCalls];
	int _calls; // actual number of stored calls

	static void OptimizeCalls(void *startFunction, CallInfo *callstack, int &calls);

	void OptimizeCalls(void *startFunction);
	void PrintCalls(MapFile &map, void *pc);
	void DeleteCall(int i);
	static void DeleteCall(CallInfo *callstack, int &calls, int i);

public:
	DebugExceptionTrap();
	~DebugExceptionTrap();
	
	void SetLogFileName( const char *pszLogFileName );
	static void LogFFF( const char *text, bool stack=true );

	//! extract callstack information from current context
	//! no new/delete guaranteed
	void ExtractCallstack(CallInfo *callstack, int &calls, bool validate=true, MapFile *map=NULL);

	//! extract callstack information from current context
	//! no new/delete guaranteed
	void ExtractCallstack(void **callstack, int &calls, bool validate=true, MapFile *map=NULL);

	void LogLine(const char *text, ...);
	void SaveContext();
	void PrintConfig();

	void LogFSP( void *eip, DWORD *ebp, DWORD *esp, const char *text, bool stack=true );
	void SetStackBottom( void *stackBottom ){m_stackBottom=stackBottom;}

	void ReportContext( const char *text, CONTEXT *context );

	static void DDTerm() {CurrentAppInfoFunctions->DDTerm();}
private:

	void OpenLogFile();
	void CloseLogFile();
	void PrintHeader();

	// entry point where control comes on an unhandled exception
	static LONG WINAPI ExceptionCallback( PEXCEPTION_POINTERS pExceptionInfo );
	LONG UnhandledExceptionFilter( PEXCEPTION_POINTERS pExceptionInfo );

	// where report info is extracted and generated	
	void PrintBanner();
	void PrintContext( void *ip, CONTEXT *ctx );
	void GenerateExceptionReport( PEXCEPTION_POINTERS pExceptionInfo );

	// Helper functions
	static LPTSTR GetExceptionString( DWORD dwCode );
	static BOOL GetLogicalAddress( 	PVOID addr, PTSTR szModule, DWORD len,
									DWORD& section, DWORD& offset );
	
	void IntelStackWalk
	(
		DWORD eip, DWORD *ebp, DWORD *pStackTop, DWORD *pStackBot
	);
	void IntelStackWalk( PCONTEXT pContext );

	//! extract callstack information from given stack
	static void ExtractCallstack
	(
		CallInfo *callstack, int &calls, void *eip, DWORD *ebp, DWORD *stackTop, DWORD *stackBot,
		bool validate=true, MapFile *map=NULL, bool firstEntryEIP=true
	);


	void IntelStackSave
	(
		DWORD eip, DWORD *pStackTop, DWORD *pStackBot
	);
	void IntelStackSave( PCONTEXT pContext );

	void IntelPCPrint( class MapFile &map, int pc, bool doEol=true );
	int CCALL _tprintf(const TCHAR * format, ...);
	int CCALL _tlprintf(const TCHAR * format, ...);
};

extern DebugExceptionTrap GDebugExceptionTrap;	//  global instance of class

#endif
