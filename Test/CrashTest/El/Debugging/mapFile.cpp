// Map File parsing

#include <El/elementpch.hpp>
#include "mapFile.hpp"

//#include <fstream.h>
#include <Es/Algorithms/qsort.hpp>
#include <El/QStream/QStream.hpp>
#include <Es/Common/win.h>

#if REM
// typical map file looks like:
//...some other stuff...
//...followed by:
  Address         Publics by Value              Rva+Base     Lib:Object

 0001:00000000       ??_H@YGXPAXIHP6EX0@Z@Z     00401000 f i engGlide.obj
 0001:00000030       ??_I@YGXPAXIHP6EX0@Z@Z     00401030 f i engGlide.obj
 0001:00000070       ??0EngineGlide@@QAE@PAX0HHH_N@Z 00401070 f   engGlide.obj
#endif

#if ALLOC_DEBUGGER
void *AllocatorWin::Alloc( int &size, const char *file, int line, const char *postfix )
{
	void *mem = GlobalAlloc(GMEM_FIXED,size);
  if (!mem)
  {
    OutputDebugString("Out of memory\n");
    __asm {int 3}
    // issue some low level warning - windows memory is not available
  }
  return mem;
}
#else
void *AllocatorWin::Alloc( int &size )
{
	// size is not changed
	void *mem = GlobalAlloc(GMEM_FIXED,size);
  if (!mem)
  {
    // issue some low level warning - windows memory is not available
    OutputDebugString("Out of memory\n");
    __asm {int 3};
  }
  return mem;
}
#endif
void AllocatorWin::Free( void *mem, int size )
{
	GlobalFree(mem);
}
void AllocatorWin::Free( void *mem )
{
	GlobalFree(mem);
}

static int CmpMaps( const MapInfo *f0, const MapInfo *f1 )
{
	int d = f0->section-f1->section;
	if (d) return d;
	return f0->physAddress-f1->physAddress;
}

static void GetLine( char *line, int size, QIStream &in)
{
	char * l = line;
	char c = in.get();
	while (c!=EOF && c!='\n')
	{
		if (size>1)
		{
			if (c!='\r')
			{
				*l++ = c;
				size--;
			}
		}
		c = in.get();
	}
	if (size>0) *l=0;
}

MapFile::MapFile()
{
	_parseDone = false;
}

MapFile::~MapFile()
{
}

void MapFile::Clear()
{
	_map.Clear();
	_parseDone = false;
}

/*!
\param name if name is NULL, mapfile based on current exe name is loaded.
*/
void MapFile::ParseMapFile(const char *name)
{
	if (_parseDone) return;
	#if defined _WIN32
	//QIFStream in;
  if (!name)
  {
    #if !defined _XBOX
	  BString<256> sourceName;
	  // use program name as given on command line
	  const char *cLine=GetCommandLine();
	  if( *cLine=='"' )
	  {
		  const char *eName=strchr(cLine+1,'"');
		  if( !eName )
		  {
			  LogF("Cannot get program name from '%s'.",cLine);
			  return;
		  }
		  strncpy(sourceName,cLine+1,eName-1-cLine),sourceName[eName-1-cLine]=0;
	  }
	  else
	  {
		  const char *eName=strchr(cLine,' ');
		  if( !eName ) strcpy(sourceName,cLine);
		  else strncpy(sourceName,cLine,eName-cLine),sourceName[eName-cLine]=0;
	  }
	  // replace .exe with .map
	  //LogF("Mapfile for '%s'.",sourceName.cstr());
	  SYSTEMTIME syst;
	  GetLocalTime(&syst);
	  /*
	  LogF
	  (
		  "%4d/%02d/%02d, %2d:%02d:%02d",
		  syst.wYear,syst.wMonth,syst.wDay,
		  syst.wHour,syst.wMinute,syst.wSecond
	  );
	  */
	  char *ext=strrchr(sourceName,'.');
	  if( ext ) strcpy(ext,".map");
	  //LogF("Loading map file '%s'.",sourceName);

	  strcpy(_name,sourceName);
    #else
	  strcpy(_name,"d:\\*.map");
    #endif
  }
  else
  {
	  strcpy(_name,name);
  }

	QIFStream in;

	in.open(_name);
	char line[1024];
	for(;;)
	{
		GetLine(line,sizeof(line),in);
		//in.getline(line,sizeof(line));
		if( in.eof() || in.fail() ) return;
		if( in.eof() ) return;
		if
		(
			!strcmpi
			(
				line,
				"  Address         Publics by Value              Rva+Base     Lib:Object"
			)
		) break;
	}
	// 
	for(;;)
	{ // search for first nonempty line
		char name[sizeof(line)];
		GetLine(line,sizeof(line),in);
		if( in.eof() || in.fail() ) break;
		// remove terminating EOL
		if (*line && line[strlen(line)-1]=='\n') line[strlen(line)-1]=0;
		if( !strchr(line,':') ) continue;
		if( line[0]==0 ) continue;
		// scan line for: <rel addr> <name> <abs addr> <......>
		const char *c=line;
		while( isspace(*c) ) c++;
		int logSection=strtoul(c,(char **)&c,16);
		if (logSection<1 || logSection>MaxSections) continue;
		
		while( *c==':' || isspace(*c) ) c++;
		int logAddress=strtoul(c,(char **)&c,16);
		while( isspace(*c) ) c++;
		char *d=name;
		while( !isspace(*c) && *c ) *d++=*c++; // get name
		*d=0;
		while( isspace(*c) ) c++;
		int physAddress=strtoul(c,(char **)&c,16);
		if( *name==0 ) continue; // no name
		// we can ignore rest of the line
		// store name/address information
		MapInfo info;
		info.section = logSection;
		info.name = name;
		info.physAddress=physAddress;
		info.logAddress=logAddress;
		_map.Add(info);
	}
	QSort(_map.Data(),_map.Size(),CmpMaps);
	#endif

	int curSection = 0;

  //_sectionEnd
	for (int i=0; i<_map.Size(); i++)
	{
		const MapInfo &info = _map[i];
		if (info.section>curSection)
		{
      for (int s=curSection; s<info.section; s++)
      {
        _sectionEnd[s] = i;
      }
      curSection = info.section;
		}
	}

  for (int s=curSection; s<MaxSections; s++)
  {
    _sectionEnd[s] = _map.Size()-1;
  }
	_parseDone = true;
}

void MapFile::OffsetPhysicalAddress(int offset)
{
	for (int i=0; i<_map.Size(); i++)
	{
		MapInfo &info = _map[i];
		info.physAddress += offset;
	}
}

int MapFile::FunctionStart( int address, MapAddressId id) const
{
	for( int i=0; i<_map.Size(); i++ )
	{
		//if( _map[i].*id>=address )
		if( _map[i].*id<=address && ( i>=_map.Size()-1 || _map[i+1].*id>address ) )
		{
			return _map[i].*id;
		}
	}
	return 0;
}

const char *MapFile::MapRawName( int address, int section, MapAddressId id, int *lower ) const
{
	for( int i=0; i<_map.Size(); i++ )
	{
		//if( _map[i].*id>=address )
		if (section>=0 && _map[i].section!=section) continue;
		if( _map[i].*id<=address && ( i>=_map.Size()-1 || _map[i+1].*id>address ) )
		{
			if( lower ) *lower=_map[i].*id;
			return _map[i].name;
		}
	}
	return "???";
}

static void DecodeTemplate(BString<512> &resClass)
{
	if (resClass[0]=='?' && resClass[1]=='$')
	{
		char *eName = strchr(resClass,'@');
		if (eName)
		{
			char templateName[256];
			strcpy(templateName,resClass+2);
			templateName[eName-resClass-2] = 0;
			BString<512> templateArg;
			strcpy(templateArg,eName+2);
			DecodeTemplate(templateArg);
			strcat(templateName,"<");
			strcat(templateName,templateArg);
			strcat(templateName,">");
			strcpy(resClass,templateName);
		}
	}
}

#if _ENABLE_REPORT && !defined _XBOX

#define IMAGEHLP_SYMS 1

#endif


#if IMAGEHLP_SYMS

#include <imagehlp.h>
// UnDecorateSymbolName

typedef DWORD __stdcall WINAPI UnDecorateSymbolNameF
(
    LPCSTR   DecoratedName,         // Name to undecorate
    LPSTR    UnDecoratedName,       // If NULL, it will be allocated
    DWORD    UndecoratedLength,     // The maximym length
    DWORD    Flags                  // See above.
);

static HMODULE ImageHlpLib = LoadLibrary("imagehlp.dll");

static UnDecorateSymbolNameF *SUnDecorateSymbolName = (UnDecorateSymbolNameF *)
(
  ImageHlpLib ? GetProcAddress(ImageHlpLib,"UnDecorateSymbolName") : NULL
);

#endif

static char *Skip(char *name, const char **skiplist)
{
  // skip mid strings as necessary
  for(;;)
  {
    bool skipped = false;
    for (const char **skip=skiplist; *skip; skip++)
    {
      if (**skip!=*name) continue;
      int len = strlen(*skip);
      if (strncmp(*skip,name,len)) continue;
      name += len;
      skipped = true;
      break;
    }
    if (!skipped) break;
  }
  return name;
}

static void Skip(char *name, const char **skipStart, const char **skipMid)
{
  char *retName = name;
  char dstBuff[512];
  char *dst = dstBuff;
  if (strlen(name)>sizeof(dstBuff))
  {
    name[sizeof(dstBuff)-1] = 0;
  }
  // skip start strings as necessary
  name = Skip(name,skipStart);
  // copy what is not matching skipMid
  while (*name)
  {
    // skip mid strings as necessary
    name = Skip(name,skipMid);
    if (!__iscsym(*name))
    {
      *dst++ = *name++;
    }
    else
    {
      // skip whole identifiers at once
      *dst++ = *name++;
      while (__iscsymf(*name))
      {
        *dst++ = *name++;
      }
    }
    
  }
  *dst = 0;
  strcpy(retName,dstBuff);
}


static const char *ReadableName(const char *name)
{
  #if IMAGEHLP_SYMS
    if (SUnDecorateSymbolName)
    {
      static const char *skipStart[] = 
      {
        "public: ", "private: ", "protected: ",
        "virtual ", "static ",
        NULL
      };
      static const char *skipMid[] = 
      {
        "__thiscall ","__cdecl ",
        "struct ","class ",
        NULL
      };
      static char readableName[512];
      SUnDecorateSymbolName(name,readableName,sizeof(readableName),UNDNAME_COMPLETE);
      readableName[sizeof(readableName)-1]=0;
      Skip(readableName,skipStart,skipMid);
      return readableName;
    }
  #endif
  // extract class and function name
	// starting character is calling convention
	if( *name=='?' ) // C++ decorated name
	{
		const char *eName;
		static BString<512> resName;
		static BString<512> resClass;
		name++;
		if( *name=='?' )
		{
			name++;
			#if 0
			if( *name=='_' )
			{
				name++;
				strcpy(resName,"operator ");
				char *opName=strchr(resName,0);
				*opName++=*name++;
				*opName=0;
			}
			else
			#endif
			switch( *name )
			{
				case '0': strcpy(resName,"constructor");name++;break;
				case '1': strcpy(resName,"destructor");name++;break;
				case '2': strcpy(resName,"operator new");name++;break;
				case '3': strcpy(resName,"operator delete");name++;break;
				case '4': strcpy(resName,"operator =");name++;break;
				//case '4': strcpy(resName,"operator ->");name++;break;
				case 'R': strcpy(resName,"operator ()");name++;break;
				default:
					strcpy(resName,"###");
					char *opName=strchr(resName,0);
					*opName++=*name++;
					*opName=0;
				break;
			}
		}
		else
		{
			eName=strchr(name,'@');
			if( !eName ) return name;
			strcpy(resName,name);
			resName[eName-name]=0;
			name=eName+1;
		}
		// all before @@ is class name
		eName = strstr(name,"@@");
		if (eName)
		{
			// if name starts with ?, it is template
			// 
			strcpy(resClass,name);
			resClass[eName-name]=0;
			DecodeTemplate(resClass);
		}
		else
		{
			eName=strchr(name,'@');
			if( !eName ) strcpy(resClass,"");
			else
			{
				strcpy(resClass,name);
				resClass[eName-name]=0;
			}
		}
		if( *resClass )
		{
			strcat(resClass,"::");
			strcat(resClass,resName);
			return resClass;
		}
		else return resName;
	}
	else
	{
		return name;
	}
}

const MapInfo *MapFile::FindByPhysical( int fAddress ) const
{
  if (_map.Size()<1) return NULL;
  // check first
  if (_map[0].physAddress>fAddress) return NULL;
  // check last
  int last = _map.Size()-1;
	if( _map[last].physAddress<=fAddress ) return &_map[last];
  // it is somewhere in between
  // we know physical address is monotone
  // we may fast iterate
  // check if 1. section is enough
  int hi = _map.Size()-1;
  if (fAddress<=_map[_sectionEnd[1]].physAddress) hi = _sectionEnd[1];
  int lo = 0;
	if( _map[hi].physAddress<=fAddress ) return &_map[hi];
  // always: _map[lo].physAddress<=fAddress, _map[hi].physAddress>fAddress
  while (hi>lo)
  {
    Assert( _map[lo].physAddress<=fAddress );
    
    Assert( _map[hi].physAddress>fAddress );
    // estimate where could the result be
    int hiAddr = _map[hi].physAddress;
    int loAddr = _map[lo].physAddress;

    if (_map[lo+1].physAddress>fAddress) {hi=lo;break;}
    float approxIndex = (fAddress-loAddr)/float(hiAddr-loAddr)*(hi-lo)+lo;
    int approxI = toInt(approxIndex);
    if (approxI>=hi) approxI = hi-1;
    if (approxI<=lo) approxI = lo+1;
    const MapInfo &info = _map[approxI];
    if (info.physAddress>fAddress) hi = approxI;
    else lo = approxI;
  }
  Assert(hi == lo);
  int fastIndex = lo;
  //Assert(fastIndex == slowIndex);
  if (fastIndex>=0) return &_map[fastIndex];
	return NULL;
}

/*!
commonly used - optimized for best speed
*/

const char *MapFile::MapNameFromPhysical( int fAddress, int *lower) const
{
  const MapInfo *info = FindByPhysical(fAddress);
  if (!info)
  {
    if (lower) *lower = 0;
    return "???";
  }
  if (lower) *lower = info->physAddress;
  return ReadableName(info->name);
}

/*!
commonly used - optimized for best speed
*/

void *MapFile::FunctionStartFromPhysical(void *pAddress) const
{
  const MapInfo *info = FindByPhysical((int)pAddress);
  if (!info)
  {
    return 0;
  }
  return (void *)info->physAddress;
}

const char *MapFile::MapName( int address, MapAddressId id, int *lower ) const
{
	for( int i=0; i<_map.Size(); i++ )
	{
		//if( _map[i].*id>=address )
		if( _map[i].*id<=address && ( i>=_map.Size()-1 || _map[i+1].*id>address ) )
		{
			if( lower ) *lower=_map[i].*id;
			const char *name=_map[i].name;
      return ReadableName(name);

		}
	}
	return "???";
}

int MapFile::MinAddress( MapAddressId id ) const
{
	if( _map.Size()<=0 ) return 0;
	return _map[0].*id;
}

int MapFile::MaxAddress( MapAddressId id, int section ) const 
{
	if( _map.Size()<=0 ) return 0x7fffffff;
	return _map[_sectionEnd[section]-1].*id;
}

int MapFile::Address( const char *name, MapAddressId id ) const 
{
	for( int i=0; i<_map.Size(); i++ )
	{
		if( !strcmp(_map[i].name,name) ) return _map[i].*id;
	}
	return 0;
}

int MapFile::AddressBySubstring(const char *name, MapAddressId id) const 
{
	for( int i=0; i<_map.Size(); i++ )
	{
		if (strstr(_map[i].name,name)) return _map[i].*id;
	}
	return 0;
}

