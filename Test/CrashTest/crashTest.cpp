#include <El/elementPch.hpp>
#include <El/Debugging/imexhnd.h>
#include <stdlib.h>

int Function1(int counter);
int Function2(int counter);
int Function3(int counter);
int Function4(int counter);


typedef int RandomFunction(int counter);

static RandomFunction *Functions[]=
{
  Function1,Function2,Function3,Function4
};

int FuncionsNum = sizeof(Functions)/sizeof(*Functions);

static void InitStack(int *stack, int nStack)
{
  if (nStack>=1)
  {
    stack[0] = 0xaa01aa01;
  }
  for (int i=1; i<nStack-1; i++)
  {
    int randomF = rand()%(FuncionsNum*3);
    if (randomF<FuncionsNum)
    {
      stack[i] = (int)Functions[randomF];
    }
    /*
    else
    {
      stack[i] = rand()+(rand()<<16);
    }
    */
  }
  if (nStack>=2)
  {
    stack[nStack-1] = 0xaa0faa0f;
  }
}

static void InitStackEmpty(int *stack, int nStack)
{
  volatile int a;
  a=1;
  if (nStack>=1)
  {
    stack[0] = 0xaa11aa11;
  }
  if (nStack>=2)
  {
    stack[nStack-1] = 0xaaffaaff;
  }
}

static int RandomCall(int counter)
{
  // crash randomly
  int crash = rand()%100; //RAND_MAX
  if (crash<2)
  {
    LogF("Crashing");
	GDebugExceptionTrap.LogFFF("Error stack");
  exit(1);
  }
  // return randomly - the deeper nested, the higher chance of returning
  int ret = rand()%100; //RAND_MAX
  if (ret<20+counter) return counter;

  // call two functions (randomly selected)
  int func1 = rand()%FuncionsNum;
  LogF("Calling %d:%d",counter,func1);
  int ret1 = Functions[func1](counter+1);

  int func2 = rand()%FuncionsNum;
  LogF("Calling %d:%d",counter,func2);
  int ret2 = Functions[func2](counter+1);

  return ret1>ret2 ? ret1 : ret2;
}

int Function1(int counter)
{
  int stack[8];
  InitStackEmpty(stack,sizeof(stack)/sizeof(*stack));
  return RandomCall(counter);
}

int Function2(int counter)
{
  int stack[12];
  InitStack(stack,sizeof(stack)/sizeof(*stack));
  return RandomCall(counter);
}

#pragma optimize("s",on)

int Function3(int counter)
{
  int stack[9];
  InitStack(stack,sizeof(stack)/sizeof(*stack));
  return RandomCall(counter);
}

#pragma optimize("sy",on)

int Function4(int counter)
{
  int stack[10];
  InitStackEmpty(stack,sizeof(stack)/sizeof(*stack));
  return RandomCall(counter);
}

#pragma optimize("", on)

int main(int argc, const char *argv[])
{
  int stackBottom;
  GDebugExceptionTrap.SetStackBottom(&stackBottom);
  srand(5);
  RandomCall(0);
  return 0;
}