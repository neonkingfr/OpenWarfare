// SSLConnection.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "SSLCon.h"
#include <iostream>
using namespace std;

int main(int argc, char* argv[])
{
	if (argc!=3)
  {
    cout << "Usage: SSLConnection.exe username password" << endl;
    return 0;
  }
  CSslConnection inetSec;
	string sAgentName("Bistudio");
  string sServerName("wiki.bistudio.com"); //Can be any https server address
	string sUserName=argv[1];//if required
	string sPass=argv[2]; //if required
	string sObjectName("/index.php/Special:CategoryExport/Category:ArmA");//there should be an object to send a verb
	
	//You may choose any field of a certificate to perform a context search, 
	//i just implemented the OU field of the Issuer here
	string sOrganizationUnitName("WikiTasks");
	//end	
	string strVerb = "POST";//My sample verb 	

	inetSec.SetAgentName(sAgentName);
	inetSec.SetCertStoreType(certStoreMY);
	inetSec.SetObjectName(sObjectName);	
  inetSec.SetUserName(sUserName);
  inetSec.SetPass(sPass);
	//Sample field
	inetSec.SetOrganizationName(sOrganizationUnitName);
	//End

	inetSec.SetPort(443/*9660*/);//443 is the default HTTPS port
	inetSec.SetServerName(sServerName); 

	//you should better assign a unique number for each internet connection
	inetSec.SetRequestID(0);
	//end

	if (!inetSec.ConnectToHttpsServer(strVerb)) {
		cout << inetSec.GetLastErrorString() << " Code: " << inetSec.GetLastErrorCode();
		return 0;
	}
	if (!inetSec.SendHttpsRequest()) {
		cout << inetSec.GetLastErrorString() << " Code: " << inetSec.GetLastErrorCode();
		return 0;
	}
	string response = inetSec.GetRequestResult();
	
	cout << response.c_str() << endl;
	
	return 0;
}
