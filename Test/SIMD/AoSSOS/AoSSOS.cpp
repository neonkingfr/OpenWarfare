#include <windows.h>
#include <stdio.h>
#include <math.h>
#include <float.h>

#include <xmmintrin.h>

#include "../ConCmn/ConCmn.h"

// Array of structures (AoS): xyzw,xyzw,xyzw,xyzw,...
struct XYZW
{
	float m_x, m_y, m_z, m_w;

	XYZW(){}
	XYZW( float x, float y, float z, float w )
	:m_x(x),m_y(y),m_z(z),m_w(w)
	{}
	float X() const {return m_x;}
	float Y() const {return m_y;}
	float Z() const {return m_z;}
	float W() const {return m_w;}
};

XYZW *aosIn, *aosOut1, *aosOut2; // AoS in and out arrays (out for Test1 and Test2)

// "SOS" Structure
struct X4Y4Z4W4
{
	float m_ax[4];
	float m_ay[4];
	float m_az[4];
	float m_aw[4];
};

class XYZWElem
{
	float x,x2,x3,x4;
	float y,y2,y3,y4;
	float z,z2,z3,z4;
	float w;

	public:
	// basic cooperation with Vector3P class
	operator XYZW() const
	{
		return XYZW(x,y,z,w);
	}
	void operator = ( const XYZW &val )
	{
		x=val.X(),y=val.Y(),z=val.Z(),w=val.W();
	}
	void operator += ( const XYZW &val )
	{
		x+=val.X(),y+=val.Y(),z+=val.Z(),w+=val.W();
	}
	void operator -= ( const XYZW &val )
	{
		x-=val.X(),y-=val.Y(),z-=val.Z(),w-=val.W();
	}

	// basic arithmetics with itself
	void operator = ( const XYZWElem &val )
	{
		x=val.x,y=val.y,z=val.z,w=val.w;
	}
	void operator += ( const XYZWElem &val )
	{
		x+=val.x,y+=val.y,z+=val.z,w+=val.w;
	}
	void operator -= ( const XYZWElem &val )
	{
		x-=val.x,y-=val.y,z-=val.z,w-=val.w;
	}

	private:
	// no initialized construction possible
	XYZWElem( const XYZWElem &val );
	//void operator = ( const V3QElement &val );


};

X4Y4Z4W4 *sosIn; // "SOS" in array

// Macros to access single elements of "SOS" structure
#define	AX(a, i)		(a[i / 4].m_ax[i % 4])
#define	AY(a, i)		(a[i / 4].m_ay[i % 4])
#define	AZ(a, i)		(a[i / 4].m_az[i % 4])
#define	AW(a, i)		(a[i / 4].m_aw[i % 4])

// The matrix
__declspec(align(16)) float matrix[4][4];	// matrix of floats
__m128 qatrix[4][4];	// matrix of Katmai operands (quads)

// Macro to cast from float to __m128
//  Usage example:
//		float a[], b[], c[];
//		M128(c[i]) = _mm_and_ps(M128(a[i]), M128(b[i]))
//  Note: make sure the argument is 16-byte aligned!
#define M128(a)					(*(__m128*)&(a))

// Macro to cast from float to __m64
// Macro to cast from float to __m128
//  Usage example:
//		float a[];
//		__m128 t;
//		_mm_storel_pi(M64(a[i]), t);
#define M64(a)					(*(__m64*)&(a))

//| | | | | | | | | |  Please scroll down | | | | | | | | | |
//V V V V V V V V V V      to Step 1      V V V V V V V V V V

#ifdef _DEBUG
const int g_nTestCount = 1;
#else
const int g_nTestCount = 40000;
#endif

#ifdef _DEBUG
#define ARRAY_COUNT	8 // dividable by 8
#else
#define ARRAY_COUNT	1000 // dividable by 8
#endif

void PrepTest1(void)
{
	// Allocate and initialize array to some values
	int i;

	aosIn = (XYZW*)_mm_malloc(ARRAY_COUNT * sizeof(XYZW), 32);
	aosOut1 = (XYZW*)_mm_malloc(ARRAY_COUNT * sizeof(XYZW), 32);
	aosOut2 = (XYZW*)_mm_malloc(ARRAY_COUNT * sizeof(XYZW), 32);

	srand(0);
	
	for (i = 0; i < ARRAY_COUNT; i++)
	{
		aosIn[i].m_x = (float)rand();
		aosIn[i].m_y = (float)rand();
		aosIn[i].m_z = (float)rand();
		aosIn[i].m_w = (float)rand();
	}

	for (i = 0; i < 4; i++)
	{
		matrix[i][0] = (float)rand() / (float)(rand() + 1);
		matrix[i][1] = (float)rand() / (float)(rand() + 1);
		matrix[i][2] = (float)rand() / (float)(rand() + 1);
		matrix[i][3] = (float)rand() / (float)(rand() + 1);
	}


	Test1(); // warming up the cache
}

void PrepTest2(void)
{
	// Allocate and initialize array to some values
	int i;

	sosIn = (X4Y4Z4W4*)aosIn;

	srand(0);
	
	for (i = 0; i < ARRAY_COUNT; i++)
	{
		AX(sosIn, i) = (float)rand();
		AY(sosIn, i) = (float)rand();
		AZ(sosIn, i) = (float)rand();
		AW(sosIn, i) = (float)rand();
	}

	for (i = 0; i < 4; i++)
	{
		qatrix[i][0] = _mm_set_ps1(matrix[i][0]);
		qatrix[i][1] = _mm_set_ps1(matrix[i][1]);
		qatrix[i][2] = _mm_set_ps1(matrix[i][2]);
		qatrix[i][3] = _mm_set_ps1(matrix[i][3]);
	}

	Test2(); // warming up the cache
}

//111111111111111111111111111111111111111111111111111111111111111111111111111111111111

// This is the implementation of AoS from Lab3a. Please scroll down to Step 1

__inline __m128 nrrcp_ss(__m128 a)
{
	__m128 r0 = _mm_rcp_ss(a);
	return _mm_sub_ss(_mm_add_ss(r0, r0), _mm_mul_ss(a, _mm_mul_ss(r0, r0)));
}

#define SELECT(m3, m2, m1, m0)	(((m3) << 6) | ((m2) << 4) | ((m1) << 2) | (m0))

__inline __m128 sumup(__m128 a)
{
	a = _mm_add_ps(_mm_shuffle_ps(a, a, SELECT(1,0,3,2)), a);
	return _mm_add_ss(_mm_shuffle_ps(a, a, SELECT(3,2,0,1)), a);
}

void mv1(XYZW* aosIn, XYZW* aosOut) // clocks/vertex: 61
{
	int i;
	__m128 v, t, wr;
	
	for (i = 0; i < ARRAY_COUNT; i += 2)
	{
		_mm_prefetch((char*)&aosIn[i + 4].m_x, _MM_HINT_NTA); // effective if nta, + 4

		v = _mm_load_ps(&aosIn[i].m_x);	

		t = _mm_mul_ps(v, M128(matrix[3]));
		t = sumup(t);

		_mm_prefetch((char*)&aosOut[i + 4].m_x, _MM_HINT_NTA); // marginally effective

		wr = nrrcp_ss(t); //_mm_rcp_ss(w);
		_mm_store_ss(&aosOut[i].m_w, wr);

		t = _mm_mul_ps(v, M128(matrix[0]));
		t = sumup(t);
		t = _mm_mul_ss(wr, t);
		_mm_store_ss(&aosOut[i].m_x, t);

		t = _mm_mul_ps(v, M128(matrix[1]));
		t = sumup(t);
		t = _mm_mul_ss(wr, t);
		_mm_store_ss(&aosOut[i].m_y, t);

		t = _mm_mul_ps(v, M128(matrix[2]));
		t = sumup(t);
		t = _mm_mul_ss(wr, t);
		_mm_store_ss(&aosOut[i].m_z, t);

		v = _mm_load_ps(&aosIn[i + 1].m_x);	

		t = _mm_mul_ps(v, M128(matrix[3]));
		t = sumup(t);

		wr = nrrcp_ss(t); //_mm_rcp_ss(w);
		_mm_store_ss(&aosOut[i + 1].m_w, wr);

		t = _mm_mul_ps(v, M128(matrix[0]));
		t = sumup(t);
		t = _mm_mul_ss(wr, t);
		_mm_store_ss(&aosOut[i + 1].m_x, t);

		t = _mm_mul_ps(v, M128(matrix[1]));
		t = sumup(t);
		t = _mm_mul_ss(wr, t);
		_mm_store_ss(&aosOut[i + 1].m_y, t);

		t = _mm_mul_ps(v, M128(matrix[2]));
		t = sumup(t);
		t = _mm_mul_ss(wr, t);
		_mm_store_ss(&aosOut[i + 1].m_z, t);
	}
}

//222222222222222222222222222222222222222222222222222222222222222222222222222222222222

__inline __m128 nrrcp_ps(__m128 a)
{
	__m128 r0 = _mm_rcp_ps(a);
	return _mm_sub_ps(_mm_add_ps(r0, r0), _mm_mul_ps(a, _mm_mul_ps(r0, r0)));
}

// Step 1: Consider the "naive" data swizzling solution:
void mv2_A(X4Y4Z4W4* sosIn, XYZW* aosOut) // clocks/vertex: 30
{
	int i;
	__m128 v, t, wr;
	__m128 x, y, z, w;
	__m128 xt, yt, zt; // declare temporary storage for the results xt, yt, zt
	
	// Loop for all SOS structures
	for (i = 0; i < ARRAY_COUNT / 4; i++)
	{
		int k;

		x = _mm_load_ps(sosIn[i].m_ax);
		t = _mm_mul_ps(qatrix[3][0], x);
_mm_prefetch((char*)sosIn[i + 2].m_ax, _MM_HINT_NTA); // effective nta + 2
		y = _mm_load_ps(sosIn[i].m_ay);
		t = _mm_add_ps(t, _mm_mul_ps(qatrix[3][1], y));
		z = _mm_load_ps(sosIn[i].m_az);
		t = _mm_add_ps(t, _mm_mul_ps(qatrix[3][2], z));
		w = _mm_load_ps(sosIn[i].m_aw);
		t = _mm_add_ps(t, _mm_mul_ps(qatrix[3][3], w));

		wr = nrrcp_ps(t);

		t = _mm_mul_ps(qatrix[0][0], x);
_mm_prefetch((char*)sosIn[i + 2].m_az, _MM_HINT_NTA); // effective nta + 2
		t = _mm_add_ps(t, _mm_mul_ps(qatrix[0][1], y));
		t = _mm_add_ps(t, _mm_mul_ps(qatrix[0][2], z));
		t = _mm_add_ps(t, _mm_mul_ps(qatrix[0][3], w));
		xt = _mm_mul_ps(t, wr); // the results are held in temporary variables

		t = _mm_mul_ps(qatrix[1][0], x);
_mm_prefetch((char*)&aosOut[i + 2].m_x, _MM_HINT_NTA); // effective nta + 2
		t = _mm_add_ps(t, _mm_mul_ps(qatrix[1][1], y));
		t = _mm_add_ps(t, _mm_mul_ps(qatrix[1][2], z));
		t = _mm_add_ps(t, _mm_mul_ps(qatrix[1][3], w));
		yt = _mm_mul_ps(t, wr); // the results are held in temporary variables

		t = _mm_mul_ps(qatrix[2][0], x);
//_mm_prefetch((void*)&aosOut[k + 12].m_x, _MM_HINT_NTA); // not effective
		t = _mm_add_ps(t, _mm_mul_ps(qatrix[2][1], y));
		t = _mm_add_ps(t, _mm_mul_ps(qatrix[2][2], z));
		t = _mm_add_ps(t, _mm_mul_ps(qatrix[2][3], w));
		zt = _mm_mul_ps(t, wr); // the results are held in temporary variables
		
		// Index i accesses SOS structures --
		//   Index k for accessing AoS requires scaling
		k = i * 4;

		// "Naive" data swizzling
		// vector #0
		aosOut[k].m_x = ((float*)&xt)[0];
		aosOut[k].m_y = ((float*)&yt)[0];
		aosOut[k].m_z = ((float*)&zt)[0];
		aosOut[k].m_w = ((float*)&wr)[0];

		// vector #1
		aosOut[k + 1].m_x = ((float*)&xt)[1];
		aosOut[k + 1].m_y = ((float*)&yt)[1];
		aosOut[k + 1].m_z = ((float*)&zt)[1];
		aosOut[k + 1].m_w = ((float*)&wr)[1];

		// vector #2
		aosOut[k + 2].m_x = ((float*)&xt)[2];
		aosOut[k + 2].m_y = ((float*)&yt)[2];
		aosOut[k + 2].m_z = ((float*)&zt)[2];
		aosOut[k + 2].m_w = ((float*)&wr)[2];

		// vector #3
		aosOut[k + 3].m_x = ((float*)&xt)[3];
		aosOut[k + 3].m_y = ((float*)&yt)[3];
		aosOut[k + 3].m_z = ((float*)&zt)[3];
		aosOut[k + 3].m_w = ((float*)&wr)[3];
	}
}

// Step 2: Implement data swizzling using Katmai intrinsics
void mv2_B(X4Y4Z4W4* sosIn, XYZW* aosOut) // clocks/vertex: 26
{
	int i;
	__m128 v, t, xt, yt, zt, wr;
	__m128 x, y, z, w; // declare temporary storage for the results xt, yt, zt
	
	// Loop for all SOS structures
	for (i = 0; i < ARRAY_COUNT / 4; i++)
	{
		int k;

		x = _mm_load_ps(sosIn[i].m_ax);
		t = _mm_mul_ps(qatrix[3][0], x);
_mm_prefetch((char*)sosIn[i + 2].m_ax, _MM_HINT_NTA); // effective nta + 2
		y = _mm_load_ps(sosIn[i].m_ay);
		t = _mm_add_ps(t, _mm_mul_ps(qatrix[3][1], y));
		z = _mm_load_ps(sosIn[i].m_az);
		t = _mm_add_ps(t, _mm_mul_ps(qatrix[3][2], z));
		w = _mm_load_ps(sosIn[i].m_aw);
		t = _mm_add_ps(t, _mm_mul_ps(qatrix[3][3], w));

		wr = nrrcp_ps(t);

		t = _mm_mul_ps(qatrix[0][0], x);
_mm_prefetch((char*)sosIn[i + 2].m_az, _MM_HINT_NTA); // effective nta + 2
		t = _mm_add_ps(t, _mm_mul_ps(qatrix[0][1], y));
		t = _mm_add_ps(t, _mm_mul_ps(qatrix[0][2], z));
		t = _mm_add_ps(t, _mm_mul_ps(qatrix[0][3], w));
		xt = _mm_mul_ps(t, wr);

		t = _mm_mul_ps(qatrix[1][0], x);
//_mm_prefetch((void*)&aosOut[i + 2].m_x, _MM_HINT_NTA); // effective nta + 2
		t = _mm_add_ps(t, _mm_mul_ps(qatrix[1][1], y));
		t = _mm_add_ps(t, _mm_mul_ps(qatrix[1][2], z));
		t = _mm_add_ps(t, _mm_mul_ps(qatrix[1][3], w));
		yt = _mm_mul_ps(t, wr);

		t = _mm_mul_ps(qatrix[2][0], x);
//_mm_prefetch((void*)&aosOut[k + 12].m_x, _MM_HINT_NTA); // not effective
		t = _mm_add_ps(t, _mm_mul_ps(qatrix[2][1], y));
		t = _mm_add_ps(t, _mm_mul_ps(qatrix[2][2], z));
		t = _mm_add_ps(t, _mm_mul_ps(qatrix[2][3], w));
		zt = _mm_mul_ps(t, wr);

		// Index i accesses SOS structures --
		//   Index k for accessing AoS requires scaling
		k = i * 4;

		// Now we have:
		// xt = (x3,x2,x1,x0)
		// yt = (y3,y2,y1,y0)
		// zt = (x3,x2,x1,x0)
		// wr = (w3,w2,w1,w0)
		
		// Use _mm_unpacklo_ps, _mm_unpackhi_ps, _mm_storel_pi, _mm_storeh_pi
		//   to perform data swizzling
		
		// Unpack xt and yt
		t = _mm_unpacklo_ps(xt, yt); // set t = (y1,x1,y0,x0) from yt and xt
		_mm_storel_pi(&M64(aosOut[k].m_x), t); // write out lower halve of t (y0,x0) to aosOut[k]
		_mm_storeh_pi(&M64(aosOut[k + 1].m_x), t); // write out higher halve of t (y1,x1) to aosOut[k + 1]
		t = _mm_unpackhi_ps(xt, yt); // set xt = (y3,x3,y2,x2) from yt, xt
		_mm_storel_pi(&M64(aosOut[k + 2].m_x), t); // write out lower halve of t (y2,x2)
		_mm_storeh_pi(&M64(aosOut[k + 3].m_x), t); // write out higher halve of t (y3,x3)
		
		// Do similarly with zt and wr
		t = _mm_unpacklo_ps(zt, wr); 
		_mm_storel_pi(&M64(aosOut[k].m_z), t);
		_mm_storeh_pi(&M64(aosOut[k + 1].m_z), t);
		t = _mm_unpackhi_ps(zt, wr);
		_mm_storel_pi(&M64(aosOut[k + 2].m_z), t);
		_mm_storeh_pi(&M64(aosOut[k + 3].m_z), t);
	}
}

// Step 3: Use NT Store after data swizzling
void mv2(X4Y4Z4W4* sosIn, XYZW* aosOut) // clocks/vertex: 20
{
	int i;
	__m128 v, t, xt, yt, zt, wr;
	__m128 x, y, z, w; // declare temporary storage for the results xt, yt, zt
	__m128 t1, t2; // additional temp. variables
	
	// Loop for all SOS structures
	for (i = 0; i < ARRAY_COUNT / 4; i++)
	{
		int k;

		x = _mm_load_ps(sosIn[i].m_ax);
		t = _mm_mul_ps(qatrix[3][0], x);
_mm_prefetch((char*)sosIn[i + 2].m_ax, _MM_HINT_NTA); // effective nta + 2
		y = _mm_load_ps(sosIn[i].m_ay);
		t = _mm_add_ps(t, _mm_mul_ps(qatrix[3][1], y));
		z = _mm_load_ps(sosIn[i].m_az);
		t = _mm_add_ps(t, _mm_mul_ps(qatrix[3][2], z));
		w = _mm_load_ps(sosIn[i].m_aw);
		t = _mm_add_ps(t, _mm_mul_ps(qatrix[3][3], w));

		wr = nrrcp_ps(t);

		t = _mm_mul_ps(qatrix[0][0], x);
_mm_prefetch((char*)sosIn[i + 2].m_az, _MM_HINT_NTA); // effective nta + 2
		t = _mm_add_ps(t, _mm_mul_ps(qatrix[0][1], y));
		t = _mm_add_ps(t, _mm_mul_ps(qatrix[0][2], z));
		t = _mm_add_ps(t, _mm_mul_ps(qatrix[0][3], w));
		xt = _mm_mul_ps(t, wr);

		t = _mm_mul_ps(qatrix[1][0], x);
//_mm_prefetch((void*)&aosOut[i + 2].m_x, _MM_HINT_NTA); // effective nta + 2
		t = _mm_add_ps(t, _mm_mul_ps(qatrix[1][1], y));
		t = _mm_add_ps(t, _mm_mul_ps(qatrix[1][2], z));
		t = _mm_add_ps(t, _mm_mul_ps(qatrix[1][3], w));
		yt = _mm_mul_ps(t, wr);

		t = _mm_mul_ps(qatrix[2][0], x);
//_mm_prefetch((void*)&aosOut[k + 12].m_x, _MM_HINT_NTA); // not effective
		t = _mm_add_ps(t, _mm_mul_ps(qatrix[2][1], y));
		t = _mm_add_ps(t, _mm_mul_ps(qatrix[2][2], z));
		t = _mm_add_ps(t, _mm_mul_ps(qatrix[2][3], w));
		zt = _mm_mul_ps(t, wr);

		// Index i accesses SOS structures --
		//   Index k for accessing AoS requires scaling
		k = i * 4;

		// Now we have:
		// xt = (x3,x2,x1,x0)
		// yt = (y3,y2,y1,y0)
		// zt = (x3,x2,x1,x0)
		// wr = (w3,w2,w1,w0)
		
		// Use _mm_unpacklo_ps, _mm_unpackhi_ps, _mm_storel_pi, _mm_storeh_pi
		//   to perform data swizzling
		
		// Unpack xt and yt
		t = _mm_unpacklo_ps(xt, yt); // set t = (y1,x1,y0,x0) from yt and xt
		_mm_storel_pi(&M64(aosOut[k].m_x), t); // write out lower halve of t (y0,x0) to aosOut[k]
		_mm_storeh_pi(&M64(aosOut[k + 1].m_x), t); // write out higher halve of t (y1,x1) to aosOut[k + 1]
		t1 = _mm_unpackhi_ps(xt, yt); // set xt = (y3,x3,y2,x2) from yt, xt
		
		// Do similarly with zt and wr
		t = _mm_unpacklo_ps(zt, wr); 
		_mm_storel_pi(&M64(aosOut[k].m_z), t);
		_mm_storeh_pi(&M64(aosOut[k + 1].m_z), t);
		t2 = _mm_unpackhi_ps(zt, wr);

//		_mm_storel_pi(&M64(aosOut[k + 2].m_x), t1); // write out lower halve of t (y2,x2)
//		_mm_storeh_pi(&M64(aosOut[k + 3].m_x), t1); // write out higher halve of t (y3,x3)

//		_mm_storel_pi(&M64(aosOut[k + 2].m_z), t2);
//		_mm_storeh_pi(&M64(aosOut[k + 3].m_z), t2);

		t = _mm_shuffle_ps(t1, t2, _MM_SHUFFLE(1,0,1,0));
		_mm_stream_ps(&aosOut[k + 2].m_x, t);
		t = _mm_shuffle_ps(t1, t2, _MM_SHUFFLE(3,2,3,2));
		_mm_stream_ps(&aosOut[k + 3].m_x, t);
	}
}

// Data swizzling using MMX(TM) Technology
void mv2_D(X4Y4Z4W4* sosIn, XYZW* aosOut) // clocks/vertex: 28
{
	int i;
	__m128 v, t, xt, yt, zt, wr;
	__m128 x, y, z, w; // declare temporary storage for the results xt, yt, zt
	__m64 x01, y01, z01, w01, x23, y23, z23, w23;
	
	// Loop for all SOS structures
	for (i = 0; i < ARRAY_COUNT / 4; i++)
	{
		int k;

		x = _mm_load_ps(sosIn[i].m_ax);
		t = _mm_mul_ps(qatrix[3][0], x);
_mm_prefetch((char*)sosIn[i + 2].m_ax, _MM_HINT_NTA); // effective nta + 2
		y = _mm_load_ps(sosIn[i].m_ay);
		t = _mm_add_ps(t, _mm_mul_ps(qatrix[3][1], y));
		z = _mm_load_ps(sosIn[i].m_az);
		t = _mm_add_ps(t, _mm_mul_ps(qatrix[3][2], z));
		w = _mm_load_ps(sosIn[i].m_aw);
		t = _mm_add_ps(t, _mm_mul_ps(qatrix[3][3], w));

		wr = nrrcp_ps(t);

		t = _mm_mul_ps(qatrix[0][0], x);
_mm_prefetch((char*)sosIn[i + 2].m_az, _MM_HINT_NTA); // effective nta + 2
		t = _mm_add_ps(t, _mm_mul_ps(qatrix[0][1], y));
		t = _mm_add_ps(t, _mm_mul_ps(qatrix[0][2], z));
		t = _mm_add_ps(t, _mm_mul_ps(qatrix[0][3], w));
		xt = _mm_mul_ps(t, wr);

		t = _mm_mul_ps(qatrix[1][0], x);
_mm_prefetch((char*)&aosOut[i + 2].m_x, _MM_HINT_NTA); // effective nta + 2
		t = _mm_add_ps(t, _mm_mul_ps(qatrix[1][1], y));
		t = _mm_add_ps(t, _mm_mul_ps(qatrix[1][2], z));
		t = _mm_add_ps(t, _mm_mul_ps(qatrix[1][3], w));
		yt = _mm_mul_ps(t, wr);

		t = _mm_mul_ps(qatrix[2][0], x);
//_mm_prefetch((void*)&aosOut[k + 12].m_x, _MM_HINT_NTA); // not effective
		t = _mm_add_ps(t, _mm_mul_ps(qatrix[2][1], y));
		t = _mm_add_ps(t, _mm_mul_ps(qatrix[2][2], z));
		t = _mm_add_ps(t, _mm_mul_ps(qatrix[2][3], w));
		zt = _mm_mul_ps(t, wr);

		// Index i accesses SOS structures --
		//   Index k for accessing AoS requires scaling
		k = i * 4;

		// Now we have:
		// xt = (x3,x2,x1,x0)
		// yt = (y3,y2,y1,y0)
		// zt = (x3,x2,x1,x0)
		// wr = (w3,w2,w1,w0)
		
		// Use _mm_storel_pi, _mm_storeh_pi, _m_punpckldq, _m_punpckhdq
		//   to perform data swizzling
		// Blending MMX(TM) code with Katmai code brings further speedup
		
		_mm_storel_pi(&x01, xt); _mm_storeh_pi(&x23, xt); // OR: x01 = M64(xt); x23 = M64(((float*)&xt)[2]);
		_mm_storel_pi(&y01, yt); _mm_storeh_pi(&y23, yt);
		M64(aosOut[k].m_x) = _m_punpckldq(x01, y01);
		M64(aosOut[k + 1].m_x) = _m_punpckhdq(x01, y01);
		M64(aosOut[k + 2].m_x) = _m_punpckldq(x23, y23);
		M64(aosOut[k + 3].m_x) = _m_punpckhdq(x23, y23);

//		_mm_stream_pi(&M64(aosOut[k].m_x), _m_punpckldq(x01, y01));
//		_mm_stream_pi(&M64(aosOut[k + 1].m_x), _m_punpckhdq(x01, y01));
//		_mm_stream_pi(&M64(aosOut[k + 2].m_x), _m_punpckldq(x23, y23));
//		_mm_stream_pi(&M64(aosOut[k + 3].m_x), _m_punpckhdq(x23, y23));

		_mm_storel_pi(&z01, zt); _mm_storeh_pi(&z23, zt);
		_mm_storel_pi(&w01, wr); _mm_storeh_pi(&w23, wr);
		M64(aosOut[k].m_z) = _m_punpckldq(z01, w01);
		M64(aosOut[k + 1].m_z) = _m_punpckhdq(z01, w01);
		M64(aosOut[k + 2].m_z) = _m_punpckldq(z23, w23);
		M64(aosOut[k + 3].m_z) = _m_punpckhdq(z23, w23);

//		_mm_stream_pi(&M64(aosOut[k].m_z), _m_punpckldq(z01, w01));
//		_mm_stream_pi(&M64(aosOut[k + 1].m_z), _m_punpckhdq(z01, w01));
//		_mm_stream_pi(&M64(aosOut[k + 2].m_z), _m_punpckldq(z23, w23));
//		_mm_stream_pi(&M64(aosOut[k + 3].m_z), _m_punpckhdq(z23, w23));
	}

	_m_empty();
}

void Test1(void)
{
	mv1(aosIn, aosOut1);
}

void Test2(void)
{
	mv2(sosIn, aosOut2);
}

__inline BOOL EpsEq(float a, float b)
{
	return abs(a - b) <= (a * 1e-5);
}

void Finalize(void)
{
	int i;

#ifdef _DEBUG
	// compare results of Test1 and Test2
	for (i = 0; i < ARRAY_COUNT; i++)
	{
		if (!EpsEq(aosOut1[i].m_x, aosOut2[i].m_x))
			printf("x1[%d] != x2[%d] (%g != %g)\n", i, i, aosOut1[i].m_x, aosOut2[i].m_x);
		if (!EpsEq(aosOut1[i].m_y, aosOut2[i].m_y))
			printf("y1[%d] != y2[%d] (%g != %g)\n", i, i, aosOut1[i].m_y, aosOut2[i].m_y);
		if (!EpsEq(aosOut1[i].m_z, aosOut2[i].m_z))
			printf("z1[%d] != z2[%d] (%g != %g)\n", i, i, aosOut1[i].m_z, aosOut2[i].m_z);
		if (!EpsEq(aosOut1[i].m_w, aosOut2[i].m_w))
			printf("w1[%d] != w2[%d] (%g != %g)\n", i, i, aosOut1[i].m_w, aosOut2[i].m_w);
	}
#else
	BOOL bCorrect = TRUE;

	for (i = 0; i < ARRAY_COUNT; i++)
	{
		if (!EpsEq(aosOut1[i].m_x, aosOut2[i].m_x))
			{ bCorrect = FALSE; break; }
		if (!EpsEq(aosOut1[i].m_y, aosOut2[i].m_y))
			{ bCorrect = FALSE; break; }
		if (!EpsEq(aosOut1[i].m_z, aosOut2[i].m_z))
			{ bCorrect = FALSE; break; }
		if (!EpsEq(aosOut1[i].m_w, aosOut2[i].m_w))
			{ bCorrect = FALSE; break; }
	}

	if (bCorrect)
	{
		printf("\nResult check passed.\n");
	}
	else
	{
		printf("\n>>>> Incorrect result. Please run Debug build for more information.\n");
	}
#endif

	_mm_free((char*)aosIn);
	_mm_free((char*)aosOut1);
	_mm_free((char*)aosOut2);
}

