TITLE	ReadTsc

.386
.MODEL FLAT
.LIST

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; Declarations

PUBLIC C CanReadTsc
PUBLIC C ReadTsc
PUBLIC C ReadTscSync

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; _CanCpuid

.CODE
.386  ; it isn't safe yet to use 486/586 instructions
_CanCpuid	PROC				; uses eax, ecx
			pushfd				; push original EFLAGS
			pop		eax			; get original EFLAGS
			mov		ecx, eax	; save original EFLAGS
			xor		eax, 200000h; flip ID bit in EFLAGS
			push	eax			; save new EFLAGS value on stack
			popfd				; replace current EFLAGS value
			pushfd				; get new EFLAGS
			pop		eax			; store new EFLAGS in EAX
			xor		eax, ecx	; can toggle ID bit (no CPUID support)?..
								;  ...eax == 0 if no, eax != 0 if yes...
								;  ...exit with this value
			ret
_CanCpuid	ENDP

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; CanReadTsc

.CODE
.586
CanReadTsc	PROC C USES ebx		; cpuid spoils ebx
			call	_CanCpuid
			and		eax, eax	; if no CPUID support...
			jz		@@exit		;  ...exit with eax == 0

			xor		eax, eax
			cpuid				; eax = max arg for CPUID
			and		eax, eax	; if CPUID only callable with eax == 0...
			jz		@@exit		;  ...exit with eax == 0

			mov		eax, 1
			cpuid				; eax = signature, edx = feature flags

			test	edx, 0010h	; check TSC support bit...
			jnz		@@canTsc	;  ...if not supported,...
			xor		eax, eax	;  ...exit with eax == 0
			jmp		SHORT @@exit
@@canTsc:
			mov		eax, 1		; exit with eax == TRUE
@@exit:
			ret
CanReadTsc	ENDP

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; ReadTsc

.CODE
.586
ReadTsc		PROC C, pull: PTR QWORD

			mov		ecx, pull

			rdtsc

			mov		DWORD PTR [ecx], eax
			mov		DWORD PTR [ecx + SIZEOF DWORD], edx

			ret
ReadTsc		ENDP 

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; ReadTscSync

.CODE
.586
ReadTscSync	PROC C USES ebx,	; cpuid spoils ebx
				pull: PTR QWORD

			xor		eax, eax
			cpuid
			
			mov		ecx, pull

			rdtsc

			mov		DWORD PTR [ecx], eax
			mov		DWORD PTR [ecx + SIZEOF DWORD], edx

			ret
ReadTscSync	ENDP 

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

END
