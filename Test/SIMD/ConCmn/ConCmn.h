#ifdef __cplusplus
extern "C"
{
#endif

extern const int g_nTestCount;
extern void PrepTest1(void);
extern void PrepTest2(void);
extern int Test1(void);
extern int Test2(void);
extern void Finalize(void);
extern void TrashCache(void);

#ifdef __cplusplus
}
#endif
