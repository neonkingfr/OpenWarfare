#include <stdio.h>
#include <windows.h>

#include "ConCmn.h"
#include "ReadTsc.h"

#define timeGetTime GetTickCount

#define DECLARE_PRIORITY	\
	HANDLE hProc = GetCurrentProcess();\
	HANDLE hThrd = GetCurrentThread();\
	DWORD dwPrtyCls = GetPriorityClass(hProc);\
	DWORD dwPrty = GetThreadPriority(hThrd)

#define BOOST_PRIORITY	\
	if (dwPrtyCls)\
		if (!SetPriorityClass(hProc, REALTIME_PRIORITY_CLASS  /*HIGH_PRIORITY_CLASS*/))\
			dwPrtyCls = 0;\
	if (dwPrty)\
		if (!SetThreadPriority(hThrd, THREAD_PRIORITY_HIGHEST))\
			dwPrty = 0

#define RESTORE_PRIORITY	\
	if (dwPrty)\
		SetThreadPriority(hThrd, dwPrty);\
	if (dwPrtyCls)\
		SetPriorityClass(hProc, dwPrtyCls)

DWORD _a[400000];
DWORD _dummy;

void TrashCache(void)
{
	LONGLONG cDummy;
	int i;

	ReadTscSync(&cDummy);
	for (i = 0; i < 400000; i += 8)
	{
		_dummy = _a[i];
	}
	ReadTscSync(&cDummy);
}

void Overhead(void)
{
}

static int CPUOptimization = _M_IX86;

int main()
{
	DWORD tT, tA, tB, t1, t2, tO;
	LONGLONG cA, cB;
	double cT, c1, c2, cO;

	int i;
	int t1Sum,t2Sum;

	DECLARE_PRIORITY;

#ifndef _DEBUG
	printf("\nCalming down...");
	Sleep(3000);
#endif

#ifdef TRASH_CACHE
	printf("\nEstimating cache trashing time...");
	BOOST_PRIORITY;
	tA = timeGetTime();
	ReadTscSync(&cA);
	for (i = 0; i < g_nTestCount; i++)
	{
		TrashCache();
	}
	ReadTscSync(&cB);
	tB = timeGetTime();
	RESTORE_PRIORITY;
	cT = (double)(cB - cA);
	tT = (tB - tA);
	printf("\nCache trashing will take %g s, %g clocks/cycle\n",
		tT / 1000.0, cT / g_nTestCount);
#else
	tT = 0;
	cT = 0.0;
#endif

	printf("\nPerforming Test1...");
	TrashCache();
	PrepTest1();

//	printf("\nTesting Test1, please wait...");
	BOOST_PRIORITY;
	tA = timeGetTime();
	ReadTscSync(&cA);
	t1Sum = 0;
	t2Sum = 0;
	for (i = 0; i < g_nTestCount; i++)
	{
#ifdef TRASH_CACHE
		TrashCache();
#endif
		t1Sum += Test1();
	}
	ReadTscSync(&cB);
	tB = timeGetTime();
	RESTORE_PRIORITY;
	
	c1 = (double)(cB - cA);
	t1 = tB - tA;
	printf("\nTest1 took %g s, %g clocks/cycle, %g clocks/iter\n",
		t1 / 1000.0, c1 / g_nTestCount, c1 / t1Sum);

	printf("\nPerforming Test2...");
	TrashCache();
	PrepTest2();

//	printf("\nTesting Test2, please wait...");
	BOOST_PRIORITY;
	tA = timeGetTime();
	ReadTscSync(&cA);
	for (i = 0; i < g_nTestCount; i++)
	{
#ifdef TRASH_CACHE
		TrashCache();
#endif
		t2Sum += Test2();
	}
	ReadTscSync(&cB);
	tB = timeGetTime();
	RESTORE_PRIORITY;
	
	c2 = (double)(cB - cA);
	t2 = tB - tA;
	printf("\nTest2 took %g s, %g clocks/cycle, %g clocks/iter\n",
		t2 / 1000.0, c2 / g_nTestCount, c2 / t2Sum);

	printf("\nEstimating overhead...");
	
	BOOST_PRIORITY;
	tA = timeGetTime();
	ReadTscSync(&cA);
	for (i = 0; i < g_nTestCount; i++)
	{
		Overhead();
	}
	ReadTscSync(&cB);
	tB = timeGetTime();
	RESTORE_PRIORITY;

	cO = (double)(cB - cA);
	tO = tB - tA;
	
	printf("\nOverhead took %g s, %g clocks/cycle\n",
		tO / 1000.0, cO / g_nTestCount);

	Finalize();

	printf("\n**** Test2 took %6.2f%% of Test1 ****\n",
		(c2 - cO - cT) * 100.0 / (c1 - cO - cT));

	if (t1 + t2 > 500)	// has to run at least ~500ms to estimate CPU frequency
	{
		printf("\nCPU frequency measured %6.1f MHz\n", (c1 + c2) * 1.e-3 / (t1 + t2));
	}
	printf("Code optimized for CPU %d\n",CPUOptimization);

	printf("Press ENTER to exit");
	getchar();
	printf("\n");

	return 0;
}