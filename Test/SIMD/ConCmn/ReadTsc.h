#ifdef __cplusplus
extern "C"
{
#endif

BOOL CanReadTsc(void);
void ReadTsc(LONGLONG* pTsc);
void ReadTscSync(LONGLONG* pTsc);

#ifdef __cplusplus
}
#endif
