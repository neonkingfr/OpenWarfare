//#include "wpch.hpp"
//#include "vertex.hpp"
//#include "win.h"
//#include "randomGen.hpp"
#include <El/elementPch.hpp>
#include "../ConCmn/ConCmn.h"
#include <Es/Common/win.h>
#include <El/Math/math3D.hpp>
#include <El/Math/math3D.hpp>
#include <El/Common/randomGen.hpp>

DWORD GlobalTickCount()
{
	return GetTickCount();
}

#ifdef _DEBUG
	extern const int g_nTestCount = 1;
	extern const int nPos=128;
#else
	extern const int g_nTestCount = 8000;
	extern const int nPos=2000;
#endif

static Matrix4 transform;
static Matrix3 rotate;
static AutoArray<Vector3> PosTableSrc;
static AutoArray<Vector3> NormTableSrc;
static AutoArray<Vector3> PosTableRes1;
static AutoArray<Vector3> NormTableRes1;
static AutoArray<Vector3> PosTableRes2;
static AutoArray<Vector3> NormTableRes2;

// non-swizzle version

void Prep(
  AutoArray<Vector3> &posTableRes, AutoArray<Vector3> &normTableRes,
  AutoArray<Vector3> &posTable, AutoArray<Vector3> &normTable
)
{
	posTableRes.Resize(nPos);
	normTableRes.Resize(nPos);
	posTable.Resize(nPos);
	normTable.Resize(nPos);

	GRandGen.SetSeed(1);
	int i;
	for( i=0; i<nPos; i++ )
	{
		posTable[i]=Vector3(
			GRandGen.RandomValue()*2-1,
			GRandGen.RandomValue()*2-1,
			GRandGen.RandomValue()*2-1
		);
	}
	for( i=0; i<nPos; i++ )
	{
		normTable[i]=Vector3(
			GRandGen.RandomValue()*2-1,
			GRandGen.RandomValue()*2-1,
			GRandGen.RandomValue()*2-1
		);
	}

	Matrix4 m1(MRotationY,15);
	Matrix4 m2(MRotationX,15);
	Matrix4 m3(MTranslation,Vector3(2,-1,3));

	transform=m1*m2*m3;

	Matrix3 r1(MRotationY,-15);
	Matrix3 r2(MRotationX,-10);
	Matrix3 r3(MRotationZ,+5);

	rotate=r1*r2*r3;
}
void Trans
(
	const Matrix4 &transform,
	AutoArray<Vector3> &resPos, AutoArray<Vector3> &resNorm,
	const AutoArray<Vector3> &srcPos, const AutoArray<Vector3> &srcNorm
)
{
	int i;
	// calculate using normal P arithmetics
	for( i=0; i<nPos; i++ )
	{
		resPos[i]=transform.FastTransform(srcPos[i]);
	}
	/*
	for( i=0; i<nPos; i++ )
	{
		resNorm[i]=transform.Rotate(srcNorm[i]);
	}
	*/
}

void PrepTest1()
{
	Prep(PosTableRes1,NormTableRes1,PosTableSrc,NormTableSrc);

	Test1(); // warm up cache
}

void PrepTest2()
{
	Prep(PosTableRes2,NormTableRes2,PosTableSrc,NormTableSrc);

	Test2(); // warm up cache
}


int Test1()
{
	Trans(transform,PosTableRes1,NormTableRes1,PosTableSrc,NormTableSrc);
	return nPos;
}

int Test2()
{
	Trans(transform,PosTableRes2,NormTableRes2,PosTableSrc,NormTableSrc);
	return nPos;
}

void Finalize()
{
	bool bCorrect = true;

	int i;
	for( i=0; i<nPos; i++ )
	{
		// compare results
		Vector3 p1=PosTableRes1[i];
		Vector3 p2=PosTableRes2[i];
		if( (p1-p2).SquareSize()>1e-9 ) bCorrect=false;
	}
	for( i=0; i<nPos; i++ )
	{
		// compare results
		Vector3 p1=NormTableRes1[i];
		Vector3 p2=NormTableRes2[i];
		if( (p1-p2).SquareSize()>1e-9 ) bCorrect=false;
	}

	if (bCorrect)
	{
		printf("\nResult check passed.\n");
	}
	else
	{
		printf("\n>>>> Incorrect result. Please run Debug build for more information.\n");
	}
}

void __cdecl ErrorMessage(char const *,...)
{
}

void __cdecl WarningMessage(char const *,...)
{
}

void __cdecl LogF( const char *format, ... )
{
	char buf[512];
		
	va_list arglist;
	va_start( arglist, format );

	vsprintf(buf,format,arglist);
	strcat(buf,"\n");
	OutputDebugString(buf);
	Sleep(0);

	va_end( arglist );

}

void __cdecl LstF( const char *format, ... )
{
	char buf[512];
		
	va_list arglist;
	va_start( arglist, format );

	vsprintf(buf,format,arglist);
	strcat(buf,"\n");
	OutputDebugString(buf);

	va_end( arglist );
}

void __cdecl ErrF( const char *format, ... )
{
	char buf[512];
		
	va_list arglist;
	va_start( arglist, format );

	vsprintf(buf,format,arglist);
	strcat(buf,"\n");
	OutputDebugString(buf);

	va_end( arglist );

}
