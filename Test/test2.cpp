/**
    @file   test2.cpp
    @brief  Test program for GNU c++.
    
    Copyright &copy; 2002 by Josef Pelikan (Josef.Pelikan@mff.cuni.cz)
    @author PE
    @since  9.10.2002
    @date   9.10.2002
*/

#include <stdio.h>
#include <time.h>

int initInt ()
{
    return (int)time(NULL);
}

int a __attribute__ ((init_priority(5000))) = initInt();
    // error:  can only use `init_priority' attribute on file-scope definitions 
    // of objects of class type

int main ()
{
    printf("a = %d\n",a);
    return 0;
}
