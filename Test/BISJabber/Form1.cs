using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace BISJabber
{
  public partial class RosterForm : Form
  {
    public RosterForm()
    {
      _conversations = new List<Conversation>();
      InitializeComponent();
    }

    private void Form1_Load(object sender, EventArgs e)
    {
      jabberClient1.Connect();
    }

    List<Conversation> _conversations;
    private Conversation FindConversation(jabber.JID jid)
    {
      // find the conversation the message is related to
      Conversation conv = null;
      for (int i = 0; i < _conversations.Count; i++)
      {
        if (_conversations[i].ID == jid)
        {
          conv = _conversations[i];
          break;
        }
      }
      return conv;
    }

    private Conversation StartConversation(jabber.JID jid, bool activateWindow)
    {
      List<jabber.JID> participants = new List<jabber.JID>();
      participants.Add(jid.Bare);
      Conversation conv = new Conversation(jabberClient1, participants);
      conv.ActivateOnNewMessage = false;
      _conversations.Add(conv);
      conv.Start(activateWindow);
      return conv;
    }

    private void jabberClient1_OnMessage(object sender, jabber.protocol.client.Message msg)
    {
      // omit the resource, use only user and server part of JID to identify user
      jabber.JID from = msg.From.Bare;
      Conversation conv = FindConversation(from);
      if (conv!=null)
      {
        conv.AddMessage(msg);
      }
      else
      {
        conv = StartConversation(from, false); //do not activate window on new message
        conv.AddMessage(msg);
      }
    }

    private void jabberClient1_OnPresence(object sender, jabber.protocol.client.Presence pres)
    {
      //System.Diagnostics.Debug.WriteLine(string.Format("presenceType {0} ... {1}-{2}",pres.From.User, pres.Type, pres.Show));
      // set the same presence as the from has had
/*
      if (pres.From.User=="bebul")
        jabberClient1.Presence(pres.Type, pres.Show, null, 15);
*/
    }

    private void jabberClient1_OnIQ(object sender, jabber.protocol.client.IQ iq)
    {
/*
      System.Xml.XmlElement query = iq.Query;
      if (query == null)
        return;

      System.Diagnostics.Debug.WriteLine(string.Format("QUERY:\r\n{0}", query.ToString()));
      if (iq.Type != jabber.protocol.client.IQType.get)
        return;


      // <iq id="jcl_8" to="me" from="you" type="get"><query xmlns="jabber:iq:version"/></iq>
      if (query is jabber.protocol.iq.Version)
      {
        iq = iq.GetResponse(jabberClient1.Document);
        jabber.protocol.iq.Version ver = iq.Query as jabber.protocol.iq.Version;
        if (ver != null)
        {
          ver.OS = Environment.OSVersion.ToString();
          ver.EntityName = Application.ProductName;
          ver.Ver = Application.ProductVersion;
        }
        jabberClient1.Write(iq);
        return;
      }
*/
    }

    private void rosterTree1_DoubleClick(object sender, EventArgs e)
    {
      System.Diagnostics.Debug.WriteLine(string.Format("{0} - {1}", sender, e));
      muzzle.RosterTree.ItemNode n = this.rosterTree1.SelectedNode as muzzle.RosterTree.ItemNode;
      if (n == null)
        return;
      // if there is not active chat dialog with this JID, start it
      Conversation conv = FindConversation(n.JID);
      if (conv != null)
      {
        conv.Start(true);
      }
      else
      {
        StartConversation(n.JID, true);  //always activate window on double click
      }
    }
  }
}
