using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;

namespace BISJabber
{
  public partial class ChatDialog : Form, IMessageFilter
  {
    // Conversation parent
    Conversation _conversation;

    public ChatDialog(Conversation conversation)
    {
      _conversation = conversation;
      InitializeComponent();
      ActiveControl = this.chatMessageText;
      // hook to handle keyboard events
      Application.AddMessageFilter(this);
    }

    public void BISAppendMaybeScroll(muzzle.BottomScrollRichText rt, Color tagColor, string tag, string text)
    {
      rt.Select(rt.Text.Length, 0); //unselect all
      //rt.SelectionLength = 0; 
      rt.SelectionColor = tagColor;
      Font oldFont = rt.SelectionFont;
      FontStyle style = rt.SelectionFont.Style | FontStyle.Bold;
      rt.SelectionFont = new Font(rt.SelectionFont, style);
      rt.AppendText(tag);
      rt.AppendText(" ");
      rt.SelectionFont = oldFont;
      rt.SelectionColor = rt.ForeColor;
      rt.AppendText(text);
      rt.AppendText("\r\n");
    }

    public void AddMessage(jabber.protocol.client.Message msg)
    {
      BISAppendMaybeScroll(bottomScrollRichText1, Color.DarkBlue, msg.From.User + ":\r\n   ", msg.Body);
      //jabberClient1.Presence(jabber.protocol.client.PresenceType.available, "Away", null, 0);
    }

    public void SendMessage()
    {
      if (string.IsNullOrEmpty(this.chatMessageText.Text)) 
        return; //no action for empty text
      jabber.protocol.client.Message reply = new jabber.protocol.client.Message(_conversation.Client.Document);
      reply.Body = this.chatMessageText.Text;
      this.chatMessageText.Text = ""; //empty the text
      reply.To = _conversation.ID;
      _conversation.Client.Write(reply);
      BISAppendMaybeScroll(bottomScrollRichText1, Color.Crimson, _conversation.Client.User + ":\r\n   ", reply.Body);
    }

    private void ChatDialog_FormClosed(object sender, FormClosedEventArgs e)
    {
      // inform parent the dialog is no longer "alive"
      // TODO: store the history?
      _conversation.DialogClosed();
    }

    // IMessageFilter implementation
    // this will help us to handle keyboard event before offered to focused control
    public bool PreFilterMessage(ref Message m)
    {
      if (Form.ActiveForm != this) return false;

      if (m.Msg == 0x100) // WM_KEYDOWN
      {
        Keys keys = ((Keys)(int)m.WParam & Keys.KeyCode) | Control.ModifierKeys;
        return HandleKeyDown(keys);
      }
      else return false;
    }
    private bool KeyShouldFocusChatEdit(Keys keyData)
    {
      if (this.chatMessageText.Focused) return false;
      if ( (keyData & Keys.Modifiers) == 0 )
      {
        Keys code = keyData & Keys.KeyCode;
        if ((code >= Keys.A && code <= Keys.Z) || //some character 
             (code == Keys.Tab || code == Keys.Space) || //space or tab
             (code >= Keys.NumPad0 && code <= Keys.Divide)) //another non navigating keys
          return true;
      }
      return false;
    }

    private bool HandleKeyDown(Keys keyData)
    {
      if ((keyData & Keys.Modifiers) == Keys.Control && (keyData & Keys.KeyCode) == Keys.Enter)
      { // Ctrl+Enter for send
        SendMessage();
        return true;
      }
      else if ( KeyShouldFocusChatEdit(keyData) )
      {
        this.chatMessageText.Focus();
/*
        string appendChar = keyData.ToString();
        if (appendChar.Length==1)
          this.chatMessageText.SelectedText = appendChar;
*/
      }
      return false; // not handled
    }

    private void ChatDialog_Activated(object sender, EventArgs e)
    {
      // Make the window NO LONGER flash
      BISJabber.Conversation.FLASHWINFO fInfo = new BISJabber.Conversation.FLASHWINFO();

      fInfo.cbSize = Convert.ToUInt32(Marshal.SizeOf(fInfo));
      fInfo.hwnd = this.Handle;  //hWnd
      fInfo.dwFlags = BISJabber.Conversation.FLASHW_STOP;
      fInfo.uCount = UInt32.MaxValue;
      fInfo.dwTimeout = 0;

      BISJabber.Conversation.FlashWindowEx(ref fInfo);
    }

    public void FocusMsgTextBox()
    {
      // focus the chat message editbox
      this.chatMessageText.Focus();
    }

    private void ChatDialog_Shown(object sender, EventArgs e)
    {
      FocusMsgTextBox();
    }
  }

}
