namespace BISJabber
{
  partial class ChatDialog
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.splitContainer1 = new System.Windows.Forms.SplitContainer();
      this.bottomScrollRichText1 = new muzzle.BottomScrollRichText();
      this.chatMessageText = new System.Windows.Forms.TextBox();
      this.splitContainer1.Panel1.SuspendLayout();
      this.splitContainer1.Panel2.SuspendLayout();
      this.splitContainer1.SuspendLayout();
      this.SuspendLayout();
      // 
      // splitContainer1
      // 
      this.splitContainer1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
      this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
      this.splitContainer1.Location = new System.Drawing.Point(0, 0);
      this.splitContainer1.Name = "splitContainer1";
      this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
      // 
      // splitContainer1.Panel1
      // 
      this.splitContainer1.Panel1.Controls.Add(this.bottomScrollRichText1);
      // 
      // splitContainer1.Panel2
      // 
      this.splitContainer1.Panel2.Controls.Add(this.chatMessageText);
      this.splitContainer1.Size = new System.Drawing.Size(382, 464);
      this.splitContainer1.SplitterDistance = 413;
      this.splitContainer1.TabIndex = 0;
      // 
      // bottomScrollRichText1
      // 
      this.bottomScrollRichText1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(250)))), ((int)(((byte)(236)))), ((int)(((byte)(190)))));
      this.bottomScrollRichText1.Dock = System.Windows.Forms.DockStyle.Fill;
      this.bottomScrollRichText1.Location = new System.Drawing.Point(0, 0);
      this.bottomScrollRichText1.Name = "bottomScrollRichText1";
      this.bottomScrollRichText1.ReadOnly = true;
      this.bottomScrollRichText1.Size = new System.Drawing.Size(378, 409);
      this.bottomScrollRichText1.TabIndex = 0;
      this.bottomScrollRichText1.Text = "";
      // 
      // chatMessageText
      // 
      this.chatMessageText.Dock = System.Windows.Forms.DockStyle.Fill;
      this.chatMessageText.Location = new System.Drawing.Point(0, 0);
      this.chatMessageText.Multiline = true;
      this.chatMessageText.Name = "chatMessageText";
      this.chatMessageText.Size = new System.Drawing.Size(378, 43);
      this.chatMessageText.TabIndex = 0;
      // 
      // ChatDialog
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(382, 464);
      this.Controls.Add(this.splitContainer1);
      this.Name = "ChatDialog";
      this.Text = "Chat Dialog";
      this.Shown += new System.EventHandler(this.ChatDialog_Shown);
      this.Activated += new System.EventHandler(this.ChatDialog_Activated);
      this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.ChatDialog_FormClosed);
      this.splitContainer1.Panel1.ResumeLayout(false);
      this.splitContainer1.Panel2.ResumeLayout(false);
      this.splitContainer1.Panel2.PerformLayout();
      this.splitContainer1.ResumeLayout(false);
      this.ResumeLayout(false);

    }

    #endregion

    private System.Windows.Forms.SplitContainer splitContainer1;
    private muzzle.BottomScrollRichText bottomScrollRichText1;
    private System.Windows.Forms.TextBox chatMessageText;


  }
}