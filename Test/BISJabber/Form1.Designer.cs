namespace BISJabber
{
  partial class RosterForm
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      this.jabberClient1 = new jabber.client.JabberClient(this.components);
      this.presenceManager1 = new jabber.client.PresenceManager(this.components);
      this.rosterManager1 = new jabber.client.RosterManager(this.components);
      this.rosterTree1 = new muzzle.RosterTree();
      this.SuspendLayout();
      // 
      // jabberClient1
      // 
      this.jabberClient1.AutoReconnect = 30F;
      this.jabberClient1.AutoStartCompression = true;
      this.jabberClient1.AutoStartTLS = true;
      this.jabberClient1.InvokeControl = this;
      this.jabberClient1.KeepAlive = 30F;
      this.jabberClient1.LocalCertificate = null;
      this.jabberClient1.Password = "colabebul";
      this.jabberClient1.Server = "jabbim.com";
      this.jabberClient1.User = "bebul2";
      this.jabberClient1.OnIQ += new jabber.client.IQHandler(this.jabberClient1_OnIQ);
      this.jabberClient1.OnPresence += new jabber.client.PresenceHandler(this.jabberClient1_OnPresence);
      this.jabberClient1.OnMessage += new jabber.client.MessageHandler(this.jabberClient1_OnMessage);
      // 
      // presenceManager1
      // 
      this.presenceManager1.CapsManager = null;
      this.presenceManager1.Stream = this.jabberClient1;
      // 
      // rosterManager1
      // 
      this.rosterManager1.Stream = this.jabberClient1;
      // 
      // rosterTree1
      // 
      this.rosterTree1.AllowDrop = true;
      this.rosterTree1.Client = this.jabberClient1;
      this.rosterTree1.Dock = System.Windows.Forms.DockStyle.Fill;
      this.rosterTree1.DrawMode = System.Windows.Forms.TreeViewDrawMode.OwnerDrawText;
      this.rosterTree1.ImageIndex = 1;
      this.rosterTree1.Location = new System.Drawing.Point(0, 0);
      this.rosterTree1.Name = "rosterTree1";
      this.rosterTree1.PresenceManager = this.presenceManager1;
      this.rosterTree1.RosterManager = this.rosterManager1;
      this.rosterTree1.SelectedImageIndex = 0;
      this.rosterTree1.ShowLines = false;
      this.rosterTree1.ShowRootLines = false;
      this.rosterTree1.Size = new System.Drawing.Size(137, 464);
      this.rosterTree1.Sorted = true;
      this.rosterTree1.StatusColor = System.Drawing.Color.Teal;
      this.rosterTree1.TabIndex = 0;
      this.rosterTree1.DoubleClick += new System.EventHandler(this.rosterTree1_DoubleClick);
      // 
      // RosterForm
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(137, 464);
      this.Controls.Add(this.rosterTree1);
      this.Name = "RosterForm";
      this.Text = "BIS Jabber";
      this.Load += new System.EventHandler(this.Form1_Load);
      this.ResumeLayout(false);

    }

    #endregion

    private jabber.client.JabberClient jabberClient1;
    private muzzle.RosterTree rosterTree1;
    private jabber.client.PresenceManager presenceManager1;
    private jabber.client.RosterManager rosterManager1;
  }
}

