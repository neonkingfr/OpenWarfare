using System;
using System.Collections.Generic;
using System.Text;
using System.Runtime.InteropServices;

namespace BISJabber
{
  /*
   * Encapsulation of one conversation with connection to its chat window
  */
  public class Conversation
  {
    // jabber client to communicate through
    private jabber.client.JabberClient _jabberClient;
    // chat dialog with active talk
    private ChatDialog _chatDlg;
    // TODO: conversation must have some ID, but what could server best?
    //    for two person conversations it can be the pair of their JIDs
    //    but when these two participant invite another one into the conversation?
    //    It seems, each conversation should be a MUC (Multi User Chat) in principle
    //    So the ID of conversation should be some JID of its Room.
    private jabber.JID _jid;
    // title of the chat 
    //   can be set by IRC style command: /topic <foo>
    //   see http://xmpp.org/extensions/xep-0045.html#schemas-muc
    string _title;
    // Chat window should be activated when new message arives
    private bool _activateOnNewMessage = false;
    public bool ActivateOnNewMessage
    {
      get { return _activateOnNewMessage; }
      set { _activateOnNewMessage = value; }
    }

    public Conversation(jabber.client.JabberClient jabberClient, List<jabber.JID> participants)
    {
      _jabberClient = jabberClient;
      if (participants.Count != 1)
      {
        System.Diagnostics.Debug.WriteLine("Error: chat is currently possible in between TWO participants. No conference chats are possible atm.");
        _jid = null;
      }
      else _jid = participants[0];
    }

    public jabber.JID ID
    {
      get { return _jid;  }
    }

    public jabber.client.JabberClient Client
    {
      get { return _jabberClient; }
    }

    // Activate the chat dialog window (creates new or bring the existing one to the focus)
    public bool Start(bool bringToForward)
    {
      if (_chatDlg==null)
      {
        _chatDlg = new ChatDialog(this);
        if (bringToForward)
          _chatDlg.WindowState = System.Windows.Forms.FormWindowState.Normal;
        else
          _chatDlg.WindowState = System.Windows.Forms.FormWindowState.Minimized;
        _chatDlg.Show();
      }
      else
      {
        if (_chatDlg.WindowState == System.Windows.Forms.FormWindowState.Minimized && bringToForward)
          _chatDlg.WindowState = System.Windows.Forms.FormWindowState.Normal;
      }
      return true;
    }

    public void DialogClosed()
    {
      _chatDlg = null;
    }
    
#region Flash Conversation Window
    // Make the chat window flash on the taskbar
    [DllImport("user32.dll")]
    [return: MarshalAs(UnmanagedType.Bool)]
    static public extern bool FlashWindowEx(ref FLASHWINFO pwfi);

    [StructLayout(LayoutKind.Sequential)]
    public struct FLASHWINFO
    {
      public UInt32 cbSize;
      public IntPtr hwnd;
      public UInt32 dwFlags;
      public UInt32 uCount;
      public UInt32 dwTimeout;
    }
    public const UInt32 FLASHW_ALL = 3;
    public const UInt32 FLASHW_STOP = 0;

    public void FlashWindow(UInt32 flashMode, UInt32 count)
    {
      // Make the window flash
      FLASHWINFO fInfo = new FLASHWINFO();

      fInfo.cbSize = Convert.ToUInt32(Marshal.SizeOf(fInfo));
      fInfo.hwnd = _chatDlg.Handle;  //hWnd
      fInfo.dwFlags = flashMode;
      fInfo.uCount = count;
      fInfo.dwTimeout = 0;

      FlashWindowEx(ref fInfo);
    }
    // flash window with the largest count possible
    public void FlashWindow(UInt32 flashMode)
    {
      FlashWindow(flashMode, UInt32.MaxValue);
    }
#endregion

    // process incomming message, start new conversation window, make it flash etc.
    public void AddMessage(jabber.protocol.client.Message msg)
    {
      if (_chatDlg==null)
      { // dialog was closed, new message should open it again
        Start(ActivateOnNewMessage);
        FlashWindow(FLASHW_ALL);
      }
      else
      {
        if (_chatDlg.WindowState == System.Windows.Forms.FormWindowState.Minimized)
        {
          if (ActivateOnNewMessage)
          {
            _chatDlg.WindowState = System.Windows.Forms.FormWindowState.Normal;
            _chatDlg.Activate();
            FlashWindow(FLASHW_ALL, 5);
          }
          else
          {
            FlashWindow(FLASHW_ALL, 10);
          }
        }
        else FlashWindow(FLASHW_ALL, 2);
      }
      _chatDlg.AddMessage(msg);
    }
  }
}
