#line 2 "W:\c\Poseidon\lib\d3d9\ShaderSources_3_0\PS.hlsl"

#include "ps.h"

// we need a value which we can add 4x together with no overflow, and with some more headroom for error
#define MAX_HALF_4 (MAX_HALF/10/4)
// similar to MAX_HALF_4, but can be used when adding 16 values
#define MAX_HALF_16 (MAX_HALF_4/4)

// ===================================================================================
// Shader for universal blur
// ===================================================================================
#define PSBLUR_MAX_KERNEL_SIZE 11

sampler PSScene: register(s0);

static const int PSBlurKernelSize = 9;//: register(i0);
float4 PSBlurKernel[PSBLUR_MAX_KERNEL_SIZE]: register(c0);

float4 PSPostProcessGaussBlur(in float2 uv:TEXCOORD0): COLOR0
{
	float4 p = float4(0.0f, 0.0f, 0.0f, 0.0f);
	for(int s = 0; s < PSBlurKernelSize; s++)
	{
		p += tex2D( PSScene, float2(uv.x + PSBlurKernel[s].x, uv.y + PSBlurKernel[s].y)) * PSBlurKernel[s].a;
	}
	return p;
}

sampler PSGamma: register(s1);
float4 PSSceneOffsets2[4]: register(c0);
float PSGammaBias: register(c4);

// input: HDR scene

float4 PSPostProcessBloomDownsample2(in half2 uv:TEXCOORD0): COLOR0
{
#if 1 //_VBS3 // Hotfix - input values are sometimes very wrong (infinite?) which break the whole downsizing and avg/max color determination
	float4 p =	(saturate(tex2D( PSScene, uv + PSSceneOffsets2[0] )) +
       			   saturate(tex2D( PSScene, uv + PSSceneOffsets2[1] )) +
       			   saturate(tex2D( PSScene, uv + PSSceneOffsets2[2] )) +
       			   saturate(tex2D( PSScene, uv + PSSceneOffsets2[3] )) ) * 0.25f;
#else
	float4 p =	(tex2D( PSScene, uv + PSSceneOffsets2[0] ) +
       			tex2D( PSScene, uv + PSSceneOffsets2[1] ) +
       			tex2D( PSScene, uv + PSSceneOffsets2[2] ) +
       			tex2D( PSScene, uv + PSSceneOffsets2[3] )) * 0.25f;
#endif
  
  p = min(p,MAX_HALF_4);
  
  
  float aperture = ApertureControl(tex2D(sampler1,float2(0,0)).x);
  //float aperture = 1;
  
  // the p will become p*aperture
  p *= aperture;
  
  // approximate perceived brightness
  half i = p.r*0.299+p.g*0.587+p.b*0.114;
  
  float overbrightF = saturate((i-PSGammaBias)*5);
 	return min(p*overbrightF/aperture,MAX_HALF_4);
 	//return p * tex1D( PSGamma, imax + PSGammaBias);
}

float4 PSSceneOffsets4[4][4]: register(c0);

// input: output of previous bloom downsampling step

float4 PSPostProcessBloomDownsample4(in half2 uv:TEXCOORD0): COLOR0
{
	half4 p = float4(0.0f, 0.0f, 0.0f, 0.0f);

	for(int v = 0; v < 4; v++)
	{
		for(int h = 0; h < 4; h++)
		{
			p += tex2D( PSScene, uv + PSSceneOffsets4[v][h]);
		}
	}
	return p * (1.0/16);
}

sampler PSBlurredScene: register(s1);
float PSBlurScale: register(c0);
float PSImageScale: register(c1);

float4 PSPostProcessBloomCombine(in half2 uv:TEXCOORD0, in half2 uv2:TEXCOORD1): COLOR0
{
	return tex2D(PSBlurredScene, uv) * PSBlurScale + tex2D(PSScene, uv2) * PSImageScale;
}

// ===================================================================================
// Shaders designed for SB rendering
// ===================================================================================

#define VSM 0

float4 PSShadowBufferAlpha_Default(float4 tDiffuseMap:TEXCOORD0, float4 tPosition:TEXCOORD7) : COLOR0
{
  // Sample the diffuse map
  half4 diffuseMap = tex2D(sampler0, tDiffuseMap.xy);

  // Kill output in case of alfa
  clip(diffuseMap.a - 0.5);

  #if VSM
  // Write out the Z and Z^2 (for VSM)
  return float4(tPosition.z,tPosition.z*tPosition.z,1,1);
  #else
  return float4(tPosition.z,1,1,1);
  #endif
}

//////////////////////////////////////////////

half4 PSShadowBufferAlpha_nVidia(float4 tDiffuseMap:TEXCOORD0) : COLOR0
{
  // Sample the diffuse map
  half4 diffuseMap = tex2D(sampler0, tDiffuseMap.xy);

  // It doesn't matter what we write to color map
  return diffuseMap;
}

// ===================================================================================
// Shaders designed for depth map rendering
// ===================================================================================

float4 PSInvDepthAlphaTest(float4 tDiffuseMap:TEXCOORD0, float4 tPosition:TEXCOORD7) : COLOR0
{
  // Kill output in case it is too close
  clip(tPosition.z - PSC_DepthClip.w);

  // Sample the diffuse map
  half4 diffuseMap = tex2D(sampler0, tDiffuseMap.xy);

  // Kill output in case of alfa
  clip(diffuseMap.a - 0.5);

  // Write out the inverse of Z  
  return float4(1.0f / tPosition.zzz, 1);
}

// we are interested in primary texture alpha only, nothing else
#ifdef _XBOX
float4 PSAlphaOnly(float2 tDiffuseMap:TEXCOORD0) : COLOR0
#else
float4 PSAlphaOnly(float2 tDiffuseMap:TEXCOORD0, float4 tPosition:TEXCOORD7) : COLOR0
#endif
{
  // Sample the diffuse map
  half4 diffuseMap = tex2D(sampler0, tDiffuseMap.xy);

  // Kill output, consider the alphaRef value
  clip(diffuseMap.a - PSC_X_X_X_AlphaRef.a);

  #ifdef _XBOX
  return diffuseMap;
  #else
  return float4(1.0f / tPosition.zzz, diffuseMap.a);
  #endif
}

#ifdef _XBOX
float4 PSAlphaOnlyMod(float2 tDiffuseMap:TEXCOORD0, half4 alpha:TEXCOORDAMBIENT) : COLOR0
#else
float4 PSAlphaOnlyMod(float2 tDiffuseMap:TEXCOORD0, half4 alpha:TEXCOORDAMBIENT, float4 tPosition:TEXCOORD7) : COLOR0
#endif
{
  // Sample the diffuse map
  half4 diffuseMap = tex2D(sampler0, tDiffuseMap.xy);

  // Calculate the final alpha
  half finalAlpha = diffuseMap.a*alpha.a;

  // Kill output, consider the alphaRef value
  clip(finalAlpha - PSC_X_X_X_AlphaRef.a);

  #ifdef _XBOX
  return diffuseMap;
  #else
  return half4(1.0f / tPosition.zzz, finalAlpha);
  #endif
}

#ifdef _XBOX
float4 PSAlphaOnlyTree(float2 tDiffuseMap:TEXCOORD0, half4 alpha:TEXCOORDAMBIENT) : COLOR0
#else
float4 PSAlphaOnlyTree(float4 tDiffuseMap:TEXCOORD0, half4 alpha:TEXCOORDAMBIENT, float4 tPosition:TEXCOORD7) : COLOR0
#endif
{
  // Sample the diffuse map
  half4 diffuseMap = tex2D(sampler0, tDiffuseMap.xy);

#ifdef _XBOX
  // Calculate the final alpha
  half finalAlpha = diffuseMap.a*alpha.a;
  // Kill output, consider the alphaRef value
  // no need for this - we are using alpha to mask
  //clip(finalAlpha - PSC_X_X_X_AlphaRef.a);
  return diffuseMap;
#else
  #if _VBS3
  half dalpha = (diffuseMap.a*alpha.a) > 0.5;
  clip(dalpha-0.5);
  return half4(1.0f / tPosition.zzz, 1);
  #else
  half4 normalMap = tex2D(sampler1, tDiffuseMap.wzyx);
  half dalpha = (diffuseMap.a*alpha.a+normalMap.a) > 1;
  clip(dalpha-0.5);
  return half4(1.0f / tPosition.zzz, 1);
  #endif
#endif
}

/// light version of STerrain - only values output by alpha vertex shader are used here
struct STerrainAlphaOnly
{
#ifndef _XBOX
  float4 tShadowBuffer           : TEXCOORDSR; //5
#endif
  float2 vPos                    : VPOS;
//   half4 ambientColor             : TEXCOORDAMBIENT; //6** not for alpha
//   half4 specularColor            : TEXCOORDSPECULAR; //7** not for alpha
//   half3 tLightLocal              : COLOR0; // ** not for alpha
//   half4 tPSFog                   : COLOR1; // ** not for alpha
  float4 tSatAndMask_GrassMap    : TEXCOORD4;
  float4 tColorMap_NormalMap     : TEXCOORD0;
  float4 tDetailMap_SpecularMap  : TEXCOORD1;
  float4 grassAlpha              : TEXCOORD2;
//   half3 tEyeLocal                : TEXCOORD3; // // ** not for alpha
};

float4 PSTerrainGrassAlphaX(STerrainAlphaOnly input): COLOR0
{
  // extend real input to match the signature of the body
  // the values we are are not used anyway, therefore they should not matter
  STerrain inBody;
  inBody.vPos  = input.vPos;
  inBody.ambientColor = 0;
  inBody.specularColor = 0;
  inBody.tShadowBuffer = 0;
  inBody.tLightLocal = 0;
  inBody.tPSFog  = 0;
  inBody.tSatAndMask_GrassMap = input.tSatAndMask_GrassMap;
  inBody.tColorMap_NormalMap = input.tColorMap_NormalMap;
  inBody.tDetailMap_SpecularMap  = input.tDetailMap_SpecularMap;
  inBody.grassAlpha  = input.grassAlpha;
  inBody.tEyeLocal = 0;
  
  const bool layers[6]={true,true,true,true,true,true};
  // texkill is the only thing we are interested in
  PFTerrainBodyX(inBody, layers, true, true, false, true, false);
  // besides of that, we also need to return projected z for depth map
  // we assume input.specularColor is receiving projected position instead of usual specular
  //return float4(1.0f / input.specularColor.zzz, 1);
  #ifdef _XBOX
  return 1;
  #else
  return float4(1.0f / input.tShadowBuffer.zzz, 1);
  #endif
}


// ===================================================================================
// Post process
// ===================================================================================

VSPP_OUTPUT VSPostProcess(
  in float3 vPosition : POSITION,
  in float2 vTexCoord : TEXCOORD
)
{
	VSPP_OUTPUT output;
	output.Position.xyzw = vPosition.xyzz;
	output.TexCoord = vTexCoord;
  return output;
}

/// pass input directly to output
float4 PSPostProcessCopy(VSPP_OUTPUT input) : COLOR
{
  return tex2D(sampler0, input.TexCoord);
}

// Shader to present thermal image (compose it from blurred and original version)
// _VBS3_TI
float4 BlurFactor : register(c0);
float4 PSPostProcessThermalPresent(VSPP_OUTPUT input) : COLOR
{
  float4 c = lerp(tex2D(sampler0, input.TexCoord), tex2D(sampler1, input.TexCoord), BlurFactor.w);
  c.rgb = pow((c.rgb+0.055)/1.055, 2.4);
  return c;
}

// Thermal visualisation shader
// _VBS3_TI
float4 TempRangeMin01_TempRangeMax01_InvTempRangeDiff_TIMode : register(c0);
float4 PSPostProcessThermal(VSPP_OUTPUT input) : COLOR
{
  // Decode the temperature from the RGB into 01 range
  float3 rgbEncodedTemperature = tex2D(sampler0, input.TexCoord).rgb;
  float temperature01 = (rgbEncodedTemperature.r + rgbEncodedTemperature.g + rgbEncodedTemperature.b) * 1.0f / 3.0f;

  // Stretch the temperature into the range that should be displayed and display it
  float intensity = saturate((temperature01 - TempRangeMin01_TempRangeMax01_InvTempRangeDiff_TIMode.x) * TempRangeMin01_TempRangeMax01_InvTempRangeDiff_TIMode.z);

  // Do the color conversion, write the output
  return float4(tex2D(sampler1, float2(intensity + (0.5f/TIConversionDimensionW), TempRangeMin01_TempRangeMax01_InvTempRangeDiff_TIMode.w / TIConversionDimensionH + (0.5f/TIConversionDimensionH))).rgb, 1.0f);
}

/////////////////////////////////////////////////////////////////////////////////////////////

float invWidth : register(c0);
float4 PSPostProcessGaussianBlurH(VSPP_OUTPUT input) : COLOR
{
  const int gaussSize = 5;
  half gaussV[5] = { 0.1016,  0.25,   0.2969, 0.25,   0.1016};
  float gaussP[5] = {-4,      -2,      0,      2,      4};

  half4 result = half4(0, 0, 0, 0);
  for (int i = 0; i < gaussSize; i++)
  {
    result += (half4)tex2D(sampler0, input.TexCoord + float2(invWidth * gaussP[i], 0)) * gaussV[i];
  }
  return result;
}

float invHeight : register(c0);
float4 PSPostProcessGaussianBlurV(VSPP_OUTPUT input) : COLOR
{
  const int gaussSize = 5;
  half gaussV[5] = { 0.1016,  0.25,   0.2969, 0.25,   0.1016};
  float gaussP[5] = {-4,      -2,      0,      2,      4};

  half4 result = half4(0, 0, 0, 0);
  for (int i = 0; i < gaussSize; i++)
  {
    result += (half4)tex2D(sampler0, input.TexCoord + float2(0, invHeight * gaussP[i])) * gaussV[i];
  }
  return result;
}

/////////////////////////////////////////////////////////////////////////////////////////////

float4 FOV : register(c0);
float4 SBTSize_invSBTSize_X_AlphaMult : register(c1);
float4x3 ShadowmapMatrix : register(c20);
float4 MaxDepth : register(c23);
float4 ShadowmapLayerBorder : register(c40);

half4 PSPostProcessSSSM_Default(VSPP_OUTPUT input) : COLOR
{
#if _SM2
  return half4(1, 1, 0, 1);
#endif

  // Retrieve the transformed position
  float4 transformedPosition;
  {
    // Z is what is written in the depth map (depth map contains 1/z - this is done to make DOF effects easy)
    // TODO: shadows processed in more passes - prefer optimization here over DOF
    transformedPosition.z = 1.0f / tex2D(sampler0, input.TexCoord).r;

    // X and Y depends on the Z and FOV
    {
      // Get the screenspace XY in <-1,1>
      float2 screenspaceXY;
      screenspaceXY.x = input.TexCoord.x * 2.0f - 1.0f;
      //screenspaceXY.y = (1.0f - input.TexCoord.y) * 2.0f - 1.0f;
      screenspaceXY.y = 1.0f - input.TexCoord.y * 2.0f;

      // Get the transformed position XY
      transformedPosition.xy = screenspaceXY * FOV.xy * transformedPosition.z;
    }

    // W is supposed to be 1
    transformedPosition.w = 1.0;
  }

  // Calculate the shadow coefficient
  half shadowCoef = 1;
  half blurCoef = 0;
  if ((transformedPosition.z >= ShadowmapLayerBorder.w) && (transformedPosition.z < ShadowmapLayerBorder.x))
  {
    // Convert point into the shadow map
    float4 shadowMap;
    shadowMap.xyz = mul(transformedPosition, ShadowmapMatrix);
    shadowMap.w = transformedPosition.z;

#if VSM
    // VSM  - simply fetch z and z^2 filtered from the map
    float4 vsm = tex2Dlod(samplerShadowMap, shadowMap.xyzw);
    
    float  fAvgZ  = vsm.r; // Filtered z
    float  fAvgZ2 = vsm.g; // Filtered z-squared

    // Standard shadow map comparison
    shadowCoef = ( shadowMap.z <= fAvgZ );
    

    // Use variance shadow mapping to compute the maximum probability that the
    // pixel is in shadow
    float variance = ( fAvgZ2 ) - ( fAvgZ * fAvgZ );
    
    const float epsilonVSM = 0.000020f;
    variance       = min( 1.0f, max( 0.0f, variance + epsilonVSM ) );
    
    float mean     = fAvgZ;
    float d        = shadowMap.z - mean;
    float p_max    = variance / ( variance + d*d );

    // To combat light-bleeding, experiment with raising p_max to some power
    // (Try values from 0.1 to 100.0, if you like.)
    shadowCoef = max(shadowCoef,pow( p_max, 2.0f ));

    // special case - shadow map is empty (farthers possible)
    if (fAvgZ>=1) shadowCoef = 1;
    
#else

#ifdef _XBOX
    // Fetch the bilinear filter fractions and four samples from the depth texture. The LOD for the 
    // fetches from the depth texture is computed using aniso filtering so that it is based on the 
    // minimum of the x and y gradients (instead of the maximum).  
    float4 Weights;
    float4 SampledDepth;
    asm {
      tfetch2D SampledDepth.x___, shadowMap.xy, samplerShadowMap, OffsetX = -0.5, OffsetY = -0.5, UseComputedLOD=true, UseRegisterLOD=false
        tfetch2D SampledDepth._x__, shadowMap.xy, samplerShadowMap, OffsetX =  0.5, OffsetY = -0.5, UseComputedLOD=true, UseRegisterLOD=false
        tfetch2D SampledDepth.__x_, shadowMap.xy, samplerShadowMap, OffsetX = -0.5, OffsetY =  0.5, UseComputedLOD=true, UseRegisterLOD=false
        tfetch2D SampledDepth.___x, shadowMap.xy, samplerShadowMap, OffsetX =  0.5, OffsetY =  0.5, UseComputedLOD=true, UseRegisterLOD=false
        getWeights2D Weights, shadowMap.xy, samplerShadowMap, MagFilter=linear, MinFilter=linear, UseComputedLOD=true, UseRegisterLOD=false
    };

    
  //   float3 pnz = float3(+1,-1,0); // XXX this computation is not correct for this version of depth sampling (but it could be updated easily)
  //   float4 fxy = Weights.xyxy * pnz.xxyy + pnz.zzxx;
  //   Weights = fxy.zzxx * fxy.wywy;
    Weights = float4( (1-Weights.x)*(1-Weights.y), Weights.x*(1-Weights.y), (1-Weights.x)*Weights.y, Weights.x*Weights.y );
    float4 Attenuation = step(shadowMap.z, SampledDepth);
    shadowCoef = dot(Attenuation, Weights);
#else
    // Precomputed constants related to texture size
    const float texsize = PSC_SBTSize_invSBTSize_X_AlphaMult.x;
    const float invTexsize = PSC_SBTSize_invSBTSize_X_AlphaMult.y;

    // Calculate the fractional part of the texel addressing  
    float2 dxy = frac(shadowMap.xy * texsize);

    // Calculate weights for bilinear interpolation

    //float4 fxy = half4(dxy, half2(1,1)-dxy);

    float3 pnz = float3(+1,-1,0);
    float4 fxy = dxy.xyxy * pnz.xxyy + pnz.zzxx;

    // float4((1 - dxy.x) * (1 - dxy.y), (1 - dxy.x) * dxy.y, dxy.x * (1 - dxy.y), dxy.x * dxy.y)
    // float4(fxy.z       * fxy.w,       fxy.z       * fxy.y, fxy.x * fxy.w,       fxy.x * fxy.y)
    float4 weights = fxy.zzxx*fxy.wywy;
    // Get 4 shadow depths
    float4 shadowDepth = float4(
      tex2Dlod(samplerShadowMap, shadowMap.xyzw).r,
      tex2Dlod(samplerShadowMap, shadowMap.xyzw + float4(0,           invTexsize,0,0)).r,
      tex2Dlod(samplerShadowMap, shadowMap.xyzw + float4(invTexsize,  0,0,0)).r,
      tex2Dlod(samplerShadowMap, shadowMap.xyzw + float4(invTexsize,  invTexsize,0,0)).r
    );

    // where 1 is contained in the shadow map, we want to handle it as a practical infinity
    // any large number should do
    shadowDepth += (shadowDepth>=1)*1000;

    // Get 4 shadow/not shadow values
    float4 sc = shadowDepth>=shadowMap.z;

    // Use weights to get the shadow coefficient
    shadowCoef = dot(sc, weights);
#endif

#endif

  #ifdef SSSMBLUR
    // Get the blur coefficient
    const float maxDiff = 0.1; // 10 cm difference decides
    const float invW = 1.0/1174.0;
    const float invH = 1.0/881.0;
    bool disableBlur = false;
    if (abs(1.0f / tex2D(sampler0, input.TexCoord + float2( invW * 5,  invH * 5)).r - transformedPosition.z) > maxDiff) disableBlur = true;
    if (abs(1.0f / tex2D(sampler0, input.TexCoord + float2(-invW * 5,  invH * 5)).r - transformedPosition.z) > maxDiff) disableBlur = true;
    if (abs(1.0f / tex2D(sampler0, input.TexCoord + float2(-invW * 5, -invH * 5)).r - transformedPosition.z) > maxDiff) disableBlur = true;
    if (abs(1.0f / tex2D(sampler0, input.TexCoord + float2( invW * 5, -invH * 5)).r - transformedPosition.z) > maxDiff) disableBlur = true;
    if (!disableBlur)
    {
      blurCoef = saturate(/*(1.0/10.0) **/ MaxDepth.w * dot(max(shadowMap.zzzz - shadowDepth, 0), 0.25) /*/ transformedPosition.z*/);
    }
  #endif
  
  }
  else
  {
#ifdef _XBOX
    return tex2D(sampler1, input.TexCoord);
#else
    clip(-1);
#endif
  }
#ifdef _XBOX
  shadowCoef *= tex2D(sampler1, input.TexCoord).r;
#endif

  // Write shadow value to output
  return half4(shadowCoef, blurCoef, 0, 1);
}

half4 PSPostProcessSSSM_nVidia(VSPP_OUTPUT input) : COLOR
{
#if _SM2
  return half4(1, 1, 0, 1);
#endif

  // Retrieve the transformed position
  float4 transformedPosition;
  {
    // Z is what is written in the depth map
    transformedPosition.z = 1.0f / tex2D(sampler0, input.TexCoord).r;

    // X and Y depends on the Z and FOV
    {
      // Get the screenspace XY in <-1,1>
      float2 screenspaceXY;
      screenspaceXY.x = input.TexCoord.x * 2.0f - 1.0f;
      screenspaceXY.y = (1.0f - input.TexCoord.y) * 2.0f - 1.0f;

      // Get the transformed position XY
      transformedPosition.xy = screenspaceXY * FOV.xy * transformedPosition.z;
    }

    // W is supposed to be 1
    transformedPosition.w = 1.0;
  }

  // Calculate shadow coefficient, sample the particular layer
  half shadowCoef = 1;
  if ((transformedPosition.z >= ShadowmapLayerBorder.w) && (transformedPosition.z < ShadowmapLayerBorder.x))
  {
    float3 shadowMap = mul(transformedPosition, ShadowmapMatrix);
    float4 nVidiaSTRQ = float4(shadowMap, 1);
    shadowCoef = tex2Dlod(samplerShadowMap, nVidiaSTRQ).r;
  }
  else
  {
#ifdef _XBOX
    return tex2D(sampler1, input.TexCoord);
#else
    clip(-1);
#endif
  }
#ifdef _XBOX
  shadowCoef *= tex2D(sampler1, input.TexCoord).r;
#endif

  // Return the shadow coefficient
  return half4(shadowCoef, 0, 0, 1);

//   // Visualise chessboard
//   float2 rounded = round(transformedPosition.xz);
//   return half4(fmod(rounded.x + rounded.y, 2) < 0.5, 0, 0, 1);
}

/////////////////////////////////////////////////////////////////////////////////////////////

half4 PSPostProcessSSSMStencil(VSPP_OUTPUT input) : COLOR
{
#ifdef SSSMBLUR
  return half4(0, 1, 0, 1);
#else
  return half4(0, 0, 0, 1);
#endif
}

/////////////////////////////////////////////////////////////////////////////////////////////

float4 PSPostProcessSSSMBH(VSPP_OUTPUT input) : COLOR
{
  const int gaussSize = 5;
  half gaussV[5] = { 0.1016,  0.25,   0.2969, 0.25,   0.1016};
  float gaussP[5] = {-4,      -2,      0,      2,      4};

  half4 result = half4(0, 0, 0, 1);
  for (int i = 0; i < gaussSize; i++)
  {
    half4 original = (half4)tex2D(sampler0, input.TexCoord + float2(invWidth * gaussP[i], 0));
    result.r += original.r * gaussV[i];
    if (i == 0)
    {
      result.g = original.g;
    }
    else
    {
      result.g = min(result.g, original.g);
    }
  }
  return result;
}

float4 PSPostProcessSSSMBV(VSPP_OUTPUT input) : COLOR
{
  const int gaussSize = 5;
  half gaussV[5] = { 0.1016,  0.25,   0.2969, 0.25,   0.1016};
  float gaussP[5] = {-4,      -2,      0,      2,      4};

  half4 result = half4(0, 0, 0, 1);
  for (int i = 0; i < gaussSize; i++)
  {
    half4 original = (half4)tex2D(sampler0, input.TexCoord + float2(0, invHeight * gaussP[i]));
    result.r += original.r * gaussV[i];
    if (i == 0)
    {
      result.g = original.g;
    }
    else
    {
      result.g = min(result.g, original.g);
    }
  }
  return result;
}

float4 PSPostProcessSSSMBFinalBlur(VSPP_OUTPUT input) : COLOR
{
  //return tex2D(sampler0, input.TexCoord);
  float4 sssmOriginal = tex2D(sampler0, input.TexCoord);
  float4 sssmBlurred = tex2D(sampler1, input.TexCoord);
  return lerp(sssmOriginal, sssmBlurred, sssmOriginal.g) * float4(2, 1, 1, 1);
}

/////////////////////////////////////////////////////////////////////////////////////////////

half Gauss(half2 sample)
{
  return 1.0f - sqrt(sample.x*sample.x + sample.y*sample.y)/* * 0.5*/;
}

// Blur radius for X and Y direction (values can be like this: float2(1/800 * 2, 1/600 * 2))
float3 blurRadius : register(c0);

// Inverse of the focal plane distance
float invFocalPlane : register(c1);

half4 PSPostProcessDOF(VSPP_OUTPUT input) : COLOR
{
//  if (input.TexCoord.y > 0.5)
//  {
//    return float4(tex2D(sampler0, input.TexCoord).rgb, 1);
//  }
//  else
  {
    //return float4(tex2D(sampler1, input.TexCoord).rgb, 1);
    
    // Poisson samples  
    const int samples = 4;
    const float2 poisson[4] = {float2(0.5, 0.5), float2(0.5, -0.5), float2(-0.5, -0.5), float2(-0.5, 0.5)};

    // Common constants
    const half baseBlur = 0.0f;
    const half blurScale = blurRadius.z; // Coefficient to influence range of the blur effect
    const half smartBlurScale = 5.0f; // Coefficient to influence the blurrines with samples behind the center

    // Calculate the depth of the center
    float centerInvDepth = tex2D(sampler2, input.TexCoord).r;

    // Calculate the blurFactor
    half blurFactor = saturate(blurScale * abs(centerInvDepth - invFocalPlane.x) + baseBlur);

    // Fetch the color from Hi and Lo textures from the center
    half3 colorHi = tex2D(sampler0, input.TexCoord).rgb;
    half3 colorLo = tex2D(sampler1, input.TexCoord).rgb;

    // Mix Hi and Lo color
    half3 color = lerp(colorHi, colorLo, blurFactor);

    // Center is the only place where weight doesn't depend on blur factor
    half centerWeight = Gauss(float2(0, 0));

    // Create the center color
    half4 cOut = float4(color * centerWeight, centerWeight);

    // Go through the poisson samples and accumulate the color
    for (int i = 0; i < samples; i++)
    {
      // Texture coordinates for both Hi and Lo textures
      float2 coord = input.TexCoord + (poisson[i] * blurRadius.xy);

      // Calculate the depth of the sample
      float sampleInvDepth = tex2D(sampler2, coord).r;

      // Calculate the smart blur factor (if sample depth is closer than center depth, then the factor is 1,
      // else it slides linearly to 0 with some coefficient)
      // float smartBlurFactor = (sampleInvDepth >= centerInvDepth) ? 1.0f : 1.0f - (sampleDepth - centerDepth) * smartBlurScale;
      half smartBlurFactor = 1.0f - saturate(smartBlurScale * (centerInvDepth - sampleInvDepth));

      // Calculate the sample blur factor
      half blurFactor = saturate(blurScale * abs(sampleInvDepth - invFocalPlane) + baseBlur);

      // Fetch the color from Hi and Lo textures from the sample
      half3 colorHi = tex2D(sampler0, coord);
      half3 colorLo = tex2D(sampler1, coord);

      // Mix Hi and Lo color
      half3 color = lerp(colorHi, colorLo, blurFactor);

      // Weight of the sample depends on blurFactor and on weight of the sample according to it's distance
      // to center (with Gaussian values)
      half sampleWeight = blurFactor * Gauss(poisson[i]) * smartBlurFactor;

      // Add sample color with weight
      cOut.rgb += color * sampleWeight;
      cOut.a += sampleWeight;
    }

    // Normalize the color
    return half4(cOut.rgb / cOut.a, 1);
  }
}

/////////////////////////////////////////////////////////////////////////////////////////////

half4 PSPostProcessDistanceDOF(VSPP_OUTPUT input) : COLOR
{
  // Poisson samples  
  const int samples = 4;
  const float2 poisson[4] = {float2(0.5, 0.5), float2(0.5, -0.5), float2(-0.5, -0.5), float2(-0.5, 0.5)};

  // Common constants
  const half baseBlur = 0.0f;
  const half blurScale = 1.5f; // Coefficient to influence range of the blur effect
  
  // Calculate the depth of the center
  float centerInvDepth = tex2D(sampler2, input.TexCoord).r;

  // Calculate the blurFactor
  //float blurFactor = saturate(blurScale * (invFocalPlane - centerInvDepth));
  half blurFactor = min(max(blurScale * (invFocalPlane - centerInvDepth), 0) + baseBlur, 1);

  // Fetch the color from Hi and Lo textures from the center
  half3 colorHi = tex2D(sampler0, input.TexCoord).rgb;
  half3 colorLo = tex2D(sampler1, input.TexCoord).rgb;

  // Mix Hi and Lo color
  half3 color = lerp(colorHi, colorLo, blurFactor);

  // Center is the only place where weight doesn't depend on blur factor
  half centerWeight = Gauss(float2(0, 0));

  // Create the center color
  half4 cOut = float4(color * centerWeight, centerWeight);

  // Go through the poisson samples and accumulate the color
  for (int i = 0; i < samples; i++)
  {
    // Texture coordinates for both Hi and Lo textures
    float2 coord = input.TexCoord + (poisson[i] * blurRadius);

    // Calculate the depth of the sample
    float sampleInvDepth = tex2D(sampler2, coord).r;

    // Calculate the sample blur factor
    //float blurFactor = saturate(blurScale * (invFocalPlane - sampleInvDepth));
    half blurFactor = min(max(blurScale * (invFocalPlane - sampleInvDepth), 0) + baseBlur, 1);

    // Fetch the color from Hi and Lo textures from the sample
    half3 colorHi = tex2D(sampler0, coord);
    half3 colorLo = tex2D(sampler1, coord);

    // Mix Hi and Lo color
    half3 color = lerp(colorHi, colorLo, blurFactor);

    // Weight of the sample depends on blurFactor and on weight of the sample according to it's distance
    // to center (with Gaussian values)
    half sampleWeight = blurFactor * Gauss(poisson[i]);

    // Add sample color with weight
    cOut.rgb += color * sampleWeight;
    cOut.a += sampleWeight;
  }

  // Normalize the color
  return half4(cOut.rgb / cOut.a, 1);
}

/////////////////////////////////////////////////////////////////////////////////////////////


/// calculate and downsample luminace
/**
input: HDR image (RGB respresents color)
output: luminance in all channels
*/
half4 PSPostProcessDownSampleLuminance(VSPP4T_OUTPUT input) : COLOR
{
  // note: PS 2.0 have 16 texture samplers
  // using this and bilinear filtering we could perform 8x8 downsampling in one pass
  // this would require some offset calculation in the shader
  half3 x1 = tex2D(sampler0, input.TexCoord0);
  half3 x2 = tex2D(sampler1, input.TexCoord1);
  half3 x3 = tex2D(sampler2, input.TexCoord2);
  half3 x4 = tex2D(sampler3, input.TexCoord3);
#if 1 //_VBS3 // Hotfix - input values are sometimes very wrong (infinite?) which break the whole downsizing and avg/max color determination
  x1 = saturate(x1);
  x2 = saturate(x2);
  x3 = saturate(x3);
  x4 = saturate(x4);
#endif
  // note: this is daytime sensitivity, night time would be different (blue shift)
  half3 eyeSensitivityD4 = half3(0.299f/4, 0.587f/4, 0.114f/4);
  half luminance = dot(x1+x2+x3+x4,eyeSensitivityD4);
  // make sure the result fits in the half range
  luminance = min(luminance,MAX_HALF_4);
  return half4(luminance.xxx,1);
  // debugging - preserve colors from original image
  //return float4((x1+x2+x3+x4)*0.25,luminance);
}

/// calculate and downsample luminace, where source is encoded temperature
/**
output: luminance in all channels
*/
half4 PSPostProcessDownSampleThermalLuminance(VSPP4T_OUTPUT input) : COLOR
{
  // Get encoded temperature and scale it to <0,1> interval
  half x1 = dot(tex2D(sampler0, input.TexCoord0).rgb, 1) * 1.0f / 3.0f;
  half x2 = dot(tex2D(sampler1, input.TexCoord1).rgb, 1) * 1.0f / 3.0f;
  half x3 = dot(tex2D(sampler2, input.TexCoord2).rgb, 1) * 1.0f / 3.0f;
  half x4 = dot(tex2D(sampler3, input.TexCoord3).rgb, 1) * 1.0f / 3.0f;

  // Calculate the average temperature and return it
  half luminance = (x1 + x2 + x3 + x4) * 1.0f/4.0f;
  luminance = min(luminance,MAX_HALF_4);
  return half4(luminance.xxx,1);
}

/// luminance downsampling shader
/**
input: average in all components (only r really used)
output: average in all components
*/

half4 PSPostProcessDownSampleAvgLuminance(VSPP4T_OUTPUT input) : COLOR
{
  half4 x1 = tex2D(sampler0, input.TexCoord0);
  half4 x2 = tex2D(sampler1, input.TexCoord1);
  half4 x3 = tex2D(sampler2, input.TexCoord2);
  half4 x4 = tex2D(sampler3, input.TexCoord3);
  half luminance = (x1.r+x2.r+x3.r+x4.r)*0.25;
  return half4(luminance.xxx,1);
  // debugging - preserve colors from original image
  //return half4((x1.rgb+x2.rgb+x3.rgb+x4.rgb)*0.25,luminance);
}

/// luminance gathering shader
/**
Downsample for average, min and max luminance in r,g,b
Assume input has the same structure (avg,min,max) in r,g,b

Point sampling should be used here - min/max cannot be interpolated
*/

half4 PSPostProcessDownSampleMaxAvgMinLuminance(VSPP4T_OUTPUT input) : COLOR
{
  half3 x1 = tex2D(sampler0, input.TexCoord0);
  half3 x2 = tex2D(sampler1, input.TexCoord1);
  half3 x3 = tex2D(sampler2, input.TexCoord2);
  half3 x4 = tex2D(sampler3, input.TexCoord3);
  return half4(
    max(max(x1.r,x2.r),max(x3.r,x4.r)),
    (x1.g+x2.g+x3.g+x4.g)*0.25,
    min(min(x1.b,x2.b),min(x3.b,x4.b)),
    1
  );
}

float AssumedLuminance(float3 maxAvgMin)
{
  // use mostly maximum to avoid overexposure
  // let average value have some influence as well
  // desiredLuminance tells us where we want the max. luminace to be
  // average luminace should probably be something like half of this
  // The result must not be 0, because it will be used as a divisor
  // - that's why epsilon is introduced
  float maxFactor = 0.9;
  float epsilon = 0.0001f;
  return max(maxAvgMin.r*maxFactor+maxAvgMin.g*2*(1-maxFactor), epsilon);
}

/*
  To transform from last frame space to this frame space we need:
    relValueLastFrame*eyeAccomOld/eyeAccom

/// time used to time-dependend eye adaptation
*/
float4 AssumedLuminancePars1: register(c8);
float4 AssumedLuminancePars2: register(c9);

/// calculate "assumed" luminance value based on avg/min/max
/**
  constant: deltaT - frameTime
  constant: eyeAccomOld/eyeAccom - convert from last frame to this frame

  sampler0: 1x1 texture - current avg/min/max in this frame space
  sampler1: 1x1 texture - aperture used for the last frame in last frame space
  
  To get absolute value we need to use relValue*eyeAccom
  
  This shader works in full precision.
*/

float4 PSPostProcessAssumedLuminance(VSPP4T_OUTPUT input) : COLOR
{
  float eyeAccomLastToThis = AssumedLuminancePars1[0];
  //float deltaT = AssumedLuminancePars1[1];
  float desiredLuminance = AssumedLuminancePars1[2];
  
  float eyeAccomMin = AssumedLuminancePars2[0];
  float eyeAccomMax = AssumedLuminancePars2[1];
  float minRatio = AssumedLuminancePars2[2];
  float maxRatio = AssumedLuminancePars2[3];
  
  float3 maxAvgMin = tex2D(sampler0, input.TexCoord0);
  
  // check for any possible overflow
  float maxAperture = 1e3;
  // when any overflow is detected, truncate
  if (!(maxAvgMin.r<maxAperture)) maxAvgMin.r = maxAperture;
  if (!(maxAvgMin.g<maxAperture)) maxAvgMin.g = maxAperture;
  if (!(maxAvgMin.b<maxAperture)) maxAvgMin.b = maxAperture;
  
  float assumedLuminance = AssumedLuminance(maxAvgMin);
  
  // sample previous aperture texture
  float newEyeAccom = desiredLuminance/assumedLuminance;
  if (eyeAccomLastToThis>0)
  {
    // last aperture valid - update it
    // caculate desired apreture (in current render target space)
    // this is desired aperture = desired luminance / measure luminance
    // precision is critical here - partial precision not enough

    float lastFrameEyeAccom = DecodeApertureChange(tex2D(sampler1, float2(0,0)).x)*eyeAccomLastToThis;
    float ratio = newEyeAccom/lastFrameEyeAccom;

    float logRatio = abs(log2(ratio));
    // for small ratios make the change very slow
    if (logRatio<1)
    {
      logRatio = lerp(0,logRatio,logRatio);
    }
    float maxChange = exp2(logRatio);
    ratio = max(-maxChange,min(ratio,maxChange));
    // perform change based on deltaT and lastFrameAperture here
    ratio = max(minRatio,min(ratio,maxRatio));

    newEyeAccom = lastFrameEyeAccom*ratio;
  }
  // saturate into a valid range
  newEyeAccom = max(eyeAccomMin,min(newEyeAccom,eyeAccomMax));

  // if there is any NAN or infinity, make sure we truncate it to prevent spreading into following frames

  return float4(EncodeApertureChange(newEyeAccom).xxx,1);
}

/// Simplified version of PSPostProcessAssumedLuminance
/**
if there is no float render target support,
we cannot calculate change on the GPU
*/
half4 PSPostProcessAssumedLuminanceDirect(VSPP4T_OUTPUT input) : COLOR
{
  half desiredLuminance = AssumedLuminancePars1[2];
  
  half eyeAccomMin = AssumedLuminancePars2[0];
  half eyeAccomMax = AssumedLuminancePars2[1];
  
  half3 maxAvgMin = tex2D(sampler0, input.TexCoord0);
  
  half assumedLuminance = AssumedLuminance(maxAvgMin);
  
  // sample previous aperture texture
  half newEyeAccom = desiredLuminance/assumedLuminance;
  // saturate into a valid range
  newEyeAccom = max(eyeAccomMin,min(newEyeAccom,eyeAccomMax));
  return half4(EncodeApertureChange(newEyeAccom).xxx,1);
}

/// apply night blue shift + black and white vision
half3 NightBlueShift(half3 c, half3 maxAvgMin)
{
  half4 intensity;
  intensity.a = dot(c, rgbEyeCoef.rgb);
  intensity.rgb = c;
  
  half4 factor = saturate(intensity*nightControl.x + nightControl.y);
  // players (and Holywood) like the night blue - let them have it (to certain extent)
  half3 color = half3(intensity.a*0.95, intensity.a*0.95, intensity.a*1.05)*(1-factor.a) + c*factor.rgb;
  return ToneMapping(color,intensity.a,maxAvgMin);
}

/**
textures:
  sampler 0 - source image
  sampler 1 - glow image
  sampler 2 - aperture modificator (1x1)
  sampler 3 - 1x1 texture - current avg/min/max in this frame space
*/

half4 PSPostProcessGlow(VSPP4T_OUTPUT input) : COLOR
{
  half4 cOriginal = tex2D(sampler0, input.TexCoord0);
  half4 cGlow = tex2D(sampler1, input.TexCoord1);
  
  float aperture = ApertureControl(tex2D(sampler2,float2(0,0)).x);
  
  // read min/max/avg, transform it into the new color space
  half3 maxAvgMin = tex2D(sampler3,float2(0,0))*aperture;


  //half3 colorWGlow = max(cGlow.rgb, cOriginal.rgb);
  half3 colorWGlow = cGlow.rgb+ cOriginal.rgb*saturate(1-cGlow.rgb);
  //half3 colorWGlow = cGlow.rgb+ cOriginal.rgb*saturate(1-(cGlow.rgb-cOriginal.rgb));
  half3 color = colorWGlow.rgb*aperture;
 
  return half4(ToneMapping(color, dot(color, rgbEyeCoef.rgb),maxAvgMin),cOriginal.a);
}

half4 PSPostProcessGlowNight(VSPP4T_OUTPUT input) : COLOR
{
  half4 cOriginal = tex2D(sampler0, input.TexCoord0);
  half4 cGlow = tex2D(sampler1, input.TexCoord1);
  float aperture = ApertureControl(tex2D(sampler2,float2(0,0)).x);
  half3 maxAvgMin = tex2D(sampler3,float2(0,0))*aperture;
  return half4(NightBlueShift(max(cOriginal,cGlow)*aperture,maxAvgMin),cOriginal.a);
}

/// B&W, transformed to a single color

half4 PSPostProcessNVG(VSPP4T_OUTPUT input) : COLOR
{
  half4 cOriginal = tex2D(sampler0, input.TexCoord0);
  half4 cGlow = tex2D(sampler1, input.TexCoord1);

  float aperture = ApertureControl(tex2D(sampler2,float2(0,0)).x);
  half3 afterAperture = max(cOriginal.rgb,cGlow.rgb)*aperture;
  half3 maxAvgMin = tex2D(sampler3,float2(0,0))*aperture;

  half intensity = dot(afterAperture, rgbEyeCoef.rgb);
  
  // simulate different response in different RGB components
  //half3 color = half3(intensity*0.3, intensity*1, intensity*0.1); // yellowish
  half3 color = half3(intensity*0.15, intensity*1, intensity*0.2); // cyanish

  // used by _VBS3_TI
  //half3 color = cOriginal.rgb;

  return half4(color,cOriginal.a);
}


////////////////////////////////////////////////////////////////////////////////////////////////////

// generic vertex shader output structure
struct VSPP_OUT
{
  float4 pos : POSITION;
  float4 tc0 : TEXCOORD0;
  float4 tc1 : TEXCOORD1;
  float4 tc2 : TEXCOORD2;
  float4 tc3 : TEXCOORD3;
  float4 tc4 : TEXCOORD4;
  float4 tc5 : TEXCOORD5;
  float4 tc6 : TEXCOORD6;
};

// vertex shader data for RotBlur PP effect
float4 mA0 : register(c0);
float4 mA1 : register(c1);
float4 mA2 : register(c2);
float4 mB0 : register(c3);
float4 mB1 : register(c4);
float4 mB2 : register(c5);
// projection data for aspect correction
float4 projConsts : register(c6);

/*
////////////////////////////////////////////////////////////////////////////////////////////////////
float4x4 GetRMatrix( float t )
{
  float4 Speeds = float4( 1.55, 0, 1.55, 1 );
   float4x4 mA = (float4x4)0;
   float4x4 mB = (float4x4)0;
   mA[0].x = 1; mA[1].y = 1; mA[2].z = 1; mA[3].w = 1;
   mB[0].x = 1; mB[1].y = 1; mB[2].z = 1; mB[3].w = 1;

   float angleA = 0.0;
   float angleB = 0.0;
   angleA = Speeds.x * t;
   angleB = Speeds.z * cos( t * Speeds.y );
   angleB = 1.57;
   
   float sinA = sin( angleA );
   float cosA = cos( angleA );
   float sinB = sin( angleB );
   float cosB = cos( angleB );
   
   mA[0].x = cosA;
   mA[2].z = cosA;
   mA[0].z = sinA;
   mA[2].x = -sinA;
   
   mB[1].y = cosB;
   mB[2].z = cosB;
   mB[1].z = sinB;
   mB[2].y = -sinB;
   
   mA = mul( mA, mB );
   return mA;
}
*/
/*
//////////////////////////////////////////////////////////////////////////////////////////
float4x4 MirrorMat( float4x4 m )
{
   float4x4 n;
   n[0][0] = m[0][0]; n[0][1] = m[1][0]; n[0][2] = m[2][0]; n[0][3] = m[3][0];
   n[1][0] = m[0][1]; n[1][1] = m[1][1]; n[1][2] = m[2][1]; n[1][3] = m[3][1];
   n[2][0] = m[0][2]; n[2][1] = m[1][2]; n[2][2] = m[2][2]; n[2][3] = m[3][2];
   n[3][0] = m[0][3]; n[3][1] = m[1][3]; n[3][2] = m[2][3]; n[3][3] = m[3][3];
   return n;
}
*/
float3x3 MirrorMat3( float3x3 m )
{
   float3x3 n;
   n[0][0] = m[0][0]; n[0][1] = m[1][0]; n[0][2] = m[2][0];
   n[1][0] = m[0][1]; n[1][1] = m[1][1]; n[1][2] = m[2][1];
   n[2][0] = m[0][2]; n[2][1] = m[1][2]; n[2][2] = m[2][2];
   return n;
}

/*
//////////////////////////////////////////////////////////////////////////////////////////
float3x3 GetRotMatrix( float x, float y, float z )
{
   float3x3 mA = (float3x3)0;
   float3x3 mB = (float3x3)0;
   mA[0].x = 1; mA[1].y = 1; mA[2].z = 1; //mA[3].w = 1;
   mB[0].x = 1; mB[1].y = 1; mB[2].z = 1; //mB[3].w = 1;

   float angleA = -y;
   float angleB = x;
   
   float sinA = sin( angleA );
   float cosA = cos( angleA );
   float sinB = sin( angleB );
   float cosB = cos( angleB );
   
   mA[0].x = cosA;
   mA[2].z = cosA;
   mA[0].z = sinA;
   mA[2].x = -sinA;
   
   mB[1].y = cosB;
   mB[2].z = cosB;
   mB[1].z = sinB;
   mB[2].y = -sinB;
   
   mA = mul( mB, mA );
   return mA;
}*/


//==================================================================================================
// POST PROCESS RotBlur
//==================================================================================================
VSPP_OUT VsPpRotBlur(
  in float3 inPos : POSITION,
  in float2 inTc0 : TEXCOORD
)
{
	VSPP_OUT o = (VSPP_OUT)0;
  half3 pos = inPos.xyz;
  half depthThreshold = mA0.w;
  half blurPower = mA1.w;
 
  // ------------------------------------------------------------------------------
  // description of modification can be found in file postProccess.cpp at line 3844
  // ------------------------------------------------------------------------------
  
  float3x3 AinvB;
  AinvB[0] = mA0.xyz;
  AinvB[1] = mA1.xyz;
  AinvB[2] = mA2.xyz;

  pos.xy *= projConsts.xy;  
  
  float3 dir = pos - mul(pos, AinvB);
    
  dir.y *= projConsts.z; // aspect correction
    
  o.pos.xyzw = float4( inPos.xyzz );
  o.tc0.xy = inTc0.xy;
  o.tc0.z = depthThreshold;
  o.tc1.xy = half2( dir.x, -dir.y ) * blurPower;
  
  //o.tc1.zw = half2( mA0.w, mA1.w );
  
  return o;
}

//==================================================================================================
float4 PsPpRotBlur( VSPP_OUT i ) : COLOR
{
#if _SM2
  return tex2D( sampler0, i.tc0.xy );
#else
  const int STEPS = 7;
  half4 final;
  half zt = i.tc0.z; // depthThreshold
  half2 dir0 = i.tc1.xy;
  half2 tcP = i.tc0.xy;
  half2 tcN = i.tc0.xy;
  half z0 = tex2D( sampler1, i.tc0.xy ).r;
  half3 accu = tex2D( sampler0, i.tc0.xy ).rgb;
  half d = 1;
  half z1, z2;
  if( z0 < zt )
  {
    for( int i = 0; i < STEPS; i++ )
    {
      tcP += dir0;
      if( tex2D( sampler1, tcP ).r < zt )
      {
        accu += tex2D( sampler0, tcP ).rgb;
        d += 1;
      }
      tcN -= dir0;
      if( tex2D( sampler1, tcN ).r < zt )
      {
        accu += tex2D( sampler0, tcN ).rgb;
        d += 1;
      }
    }
  }
  final.a = 1;
  final.rgb = accu.rgb / d;

  return final;
#endif
}

float4 radialBlurPars : register(c0); // xy... blur scale, zw... blur offset

//==================================================================================================
float4 PsPpRadialBlur( VSPP_OUTPUT i ) : COLOR
{
  const int STEPS = 7;
  half4 final;
  half2 tc = i.TexCoord.xy;
  half2 dir = tc - 0.5;
  dir.xy = -radialBlurPars.xy * saturate( abs(dir.xy) - radialBlurPars.zw ) * sign( dir.xy );
  dir.y *= projConsts.z; // aspect correction
  half3 accu = tex2D( sampler0, tc ).rgb;
  for( int i = 0; i < STEPS; i++ )
  {
    tc += dir;
    accu += tex2D( sampler0, tc ).rgb;
  }
  final.rgb = accu.rgb / (half)( STEPS + 1 );
  final.a = 1;
  return final;
}

//==================================================================================================
VSPP_OUT VsPpColors(
  in float3 inPos : POSITION,
  in float2 inTc0 : TEXCOORD
)
{
	VSPP_OUT o = (VSPP_OUT)0;
  half4 pos = half4( inPos.xy, 1.0, 1.0 );
  half2 tc;
  tc.x = 0.5 * inPos.x + 0.5;
  tc.y = -0.5 * inPos.y + 0.5;
  
  o.pos.xyzw = pos;
	o.tc0.xy = tc;
  
  return o;
}

float4 ppColorsPars : register(c0); // x ..brightness, y ...contrast, z ...offset ( default: 1, 1, 0 )
float4 lerpColor : register(c2);
float4 colorizeColor : register(c3); // a ...saturation
float4 rgbWeights : register(c4); // 0.299, 0.587, 0.114

//==================================================================================================
float4 PsPpColors( VSPP_OUT i ) : COLOR
{
  half4 final;
  half3 src = tex2D( sampler0, i.tc0.xy ).rgb;
// CONTRAST + OFFSET
  src.rgb = saturate( src.rgb * ppColorsPars.y + ppColorsPars.z );
  // 
  half mono = src.r * rgbWeights.r + src.g * rgbWeights.g + src.b * rgbWeights.b;
  half3 mono3 = mono * colorizeColor.rgb;
// SATURATION / COLORIZATION
  final.rgb = lerp( mono3.rgb, src.rgb, colorizeColor.a );
// BRIGHTNESS
  half brightness = ppColorsPars.x;
  final.rgb *= saturate( brightness );
  final.rgb = lerp( final.rgb, half3( 1.0, 1.0, 1.0 ), saturate( brightness - 1.0 ) );
// COLOR BLEND
  final.rgb = lerp( final.rgb, lerpColor.rgb, lerpColor.a );
  
  final.a = 1;
  return final;
}


float4 chromAbberPars : register(c0); // xy... abberation power

//==================================================================================================
float4 PsPpChromAber( VSPP_OUTPUT i ) : COLOR
{
  // color masks for samples
  const half3 mask1 = half3( 0.8, 0.2, 0.0 );
  const half3 mask2 = half3( 0.2, 0.6, 0.2 );
  const half3 mask3 = half3( 0.0, 0.2, 0.8 );
  //
  half4 final;
  half2 tc = i.TexCoord.xy;
  half2 dir = tc - 0.5;
  dir.xy *= chromAbberPars.xy;
  //dir.y *= projConsts.z; // aspect correction
  //dir.x *= projConsts.x; // aspect correction
  
  final.rgb = tex2D( sampler0, tc - dir ).rgb * mask1.rgb;
  final.rgb += tex2D( sampler0, tc ).rgb * mask2.rgb;
  final.rgb += tex2D( sampler0, tc + dir ).rgb * mask3.rgb;
  final.a = 1;
  return final;
}

float4 ppWetDistortPars : register(c0); // time, blur, powerTop, powerBottom
float4 ppWDSpeeds : register(c1); // speeds
float4 ppWDAmps : register(c2); // amplitudes
float4 ppWDPhases : register(c3); // randX, randY, posX, posY
//
float4 ppWDAberration : register(c4);

//==================================================================================================
VSPP_OUT VsPpWetDistort(
  in float3 inPos : POSITION,
  in float2 inTc0 : TEXCOORD
)
{
	VSPP_OUT o = (VSPP_OUT)0;
  half4 pos = half4( inPos.xy, 1.0, 1.0 );
  half2 tc;
  tc.x = 0.5 * inPos.x + 0.5;
  tc.y = -0.5 * inPos.y + 0.5;
  
  o.pos.xyzw = pos;
	o.tc0.xy = tc;
  
  float t = ppWetDistortPars.x;
  float a0 = sin( t * ppWDSpeeds.x + inTc0.x * ppWDPhases.x + inPos.x * ppWDPhases.z ) * ppWDAmps.x;
  float a1 = sin( t * ppWDSpeeds.y + inTc0.y * ppWDPhases.x + inPos.x * ppWDPhases.z ) * ppWDAmps.y;
  float a2 = sin( t * ppWDSpeeds.z + inPos.z * ppWDPhases.y + inPos.y * ppWDPhases.w ) * ppWDAmps.z;
  float a3 = sin( t * ppWDSpeeds.w + inTc0.x * ppWDPhases.y + inPos.y * ppWDPhases.w ) * ppWDAmps.w;
  half gPhase = lerp( ppWetDistortPars.z, ppWetDistortPars.w, tc.y );
  o.tc0.w = ppWetDistortPars.y * gPhase;
  o.tc1.x = ( a0 + a1 ) * gPhase;
  o.tc1.y = ( a2 + a3 ) * gPhase;
  o.tc1.zw = inPos.xy;
  o.tc2.xyz = half3( inTc0.x, inTc0.y, inPos.z );
  o.tc2.w = t;
  
  return o;
}

//==================================================================================================
float4 PsPpWetDistort( VSPP_OUT i ) : COLOR
{
  half4 final;
  half2 tc = i.tc0.xy;
  half2 dtc = i.tc1.xy;
  
  half3 src;
  src.r = tex2D( sampler0, tc + dtc * ppWDAberration.x ).r;
  src.g = tex2D( sampler0, tc + dtc * ppWDAberration.y ).g;
  src.b = tex2D( sampler0, tc + dtc * ppWDAberration.z ).b;
  
  half3 blur;
  blur.r = tex2D( sampler1, tc + dtc * ppWDAberration.x ).r;
  blur.g = tex2D( sampler1, tc + dtc * ppWDAberration.y ).g;
  blur.b = tex2D( sampler1, tc + dtc * ppWDAberration.z ).b;
  
  final.rgb = lerp( src.rgb, blur.rgb, i.tc0.w );
  final.a = 1;
  return final;
}

////////////////////////////////////////////////////////////////////////////////////////////////////
// DYNAMIC BLUR
////////////////////////////////////////////////////////////////////////////////////////////////////

float4 ppDynBlurPars0 : register(c0);
float4 ppDynBlurPars1 : register(c1);
float4 ppDynBlurWeights[6] : register(c2);

//==================================================================================================
VSPP_OUT VsPpDynamicBlur(
  in float3 inPos : POSITION,
  in float2 inTc0 : TEXCOORD
)
{
	VSPP_OUT o = (VSPP_OUT)0;
  float4 pos = half4( inPos.xy, 1.0, 1.0 );
  float2 tc = inTc0.xy;
  tc.y *= ppDynBlurPars0.y;
  o.tc0.zw = tc;
  tc += ppDynBlurPars1.zw;
  float2 dir = ppDynBlurPars1.xy;
  
  o.pos.xyzw = pos;
	o.tc0.xy = tc;
  o.tc1.xy = tc + dir;
  o.tc1.zw = tc - dir;
  o.tc2.xy = o.tc1.xy + dir;
  o.tc2.zw = o.tc1.zw - dir;
  o.tc3.xy = o.tc2.xy + dir;
  o.tc3.zw = o.tc2.zw - dir;
  o.tc4.xy = o.tc3.xy + dir;
  o.tc4.zw = o.tc3.zw - dir;
  o.tc5.xy = o.tc4.xy + dir;
  o.tc5.zw = o.tc4.zw - dir;
  o.tc6.xyzw = float4( 1, ppDynBlurPars0.z, 1, ppDynBlurPars0.z );
  return o;
}

//==================================================================================================
float4 PsPpDynamicBlur( VSPP_OUT i ) : COLOR
{
  half4 final;
  float4 lim = i.tc6.xyzw;
  float4 tc0 = min( i.tc0, lim );
  float4 tc1 = min( i.tc1, lim );
  float4 tc2 = min( i.tc2, lim );
  float4 tc3 = min( i.tc3, lim );
  float4 tc4 = min( i.tc4, lim );
  float4 tc5 = min( i.tc5, lim );
  final = tex2D( sampler0, tc0.xy ) * ppDynBlurWeights[0].x;
  final += tex2D( sampler0, tc1.xy ) * ppDynBlurWeights[1].x;
  final += tex2D( sampler0, tc1.zw ) * ppDynBlurWeights[1].x;
  final += tex2D( sampler0, tc2.xy ) * ppDynBlurWeights[2].x;
  final += tex2D( sampler0, tc2.zw ) * ppDynBlurWeights[2].x;
  final += tex2D( sampler0, tc3.xy ) * ppDynBlurWeights[3].x;
  final += tex2D( sampler0, tc3.zw ) * ppDynBlurWeights[3].x;
  final += tex2D( sampler0, tc4.xy ) * ppDynBlurWeights[4].x;
  final += tex2D( sampler0, tc4.zw ) * ppDynBlurWeights[4].x;
  final += tex2D( sampler0, tc5.xy ) * ppDynBlurWeights[5].x;
  final += tex2D( sampler0, tc5.zw ) * ppDynBlurWeights[5].x;
  return final;
}

//==================================================================================================
VSPP_OUT VsPpDynamicBlurFinal(
  in float3 inPos : POSITION,
  in float2 inTc0 : TEXCOORD
)
{
	VSPP_OUT o = (VSPP_OUT)0;
  float4 pos = half4( inPos.xy, 1.0, 1.0 );
  float2 tc = inTc0.xy;
  half weight0 = saturate( ppDynBlurPars0.x );
  float2 aspect = float2( 0, ppDynBlurPars0.y );
  o.pos.xyzw = pos;
	o.tc0.xy = tc + ppDynBlurPars0.zw;
  o.tc1.xy = tc + ppDynBlurPars1.xy * aspect + ppDynBlurPars1.zw;
  o.tc2.xy = tc - ppDynBlurPars1.xy * aspect + ppDynBlurPars1.zw;
  o.tc3.xy = tc + ppDynBlurPars1.yx * aspect + ppDynBlurPars1.zw;
  o.tc4.xy = tc - ppDynBlurPars1.yx * aspect + ppDynBlurPars1.zw;
  o.tc0.w = weight0;
  return o;
}

//==================================================================================================
float4 PsPpDynamicBlurFinal( VSPP_OUT i ) : COLOR
{
  half4 final;
  half weight = i.tc0.w;
  half4 src0 = tex2D( sampler0, i.tc0.xy );
  half4 src1 = tex2D( sampler1, i.tc1.xy );
  src1 += tex2D( sampler1, i.tc2.xy );
  src1 += tex2D( sampler1, i.tc3.xy );
  src1 += tex2D( sampler1, i.tc4.xy );
  src1 *= 0.25;
  final.rgb = lerp( src0.rgb, src1.rgb, saturate( weight ) );
  final.a = 1;
  return final;
}