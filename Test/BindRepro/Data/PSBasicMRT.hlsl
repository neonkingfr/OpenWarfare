#line 2 "W:\c\Poseidon\lib\d3d9\ShaderSources_3_0\PSBasicMRT.hlsl"

#include "ps.h"

//! Macro to create the specified PS
#define CREATEPSSN(name,structureName) \
void PSMRT##name(structureName input, out PSOUTMRT oColor) \
{ \
  FOut fout = PF##name(input,false); \
  oColor = GetFinalColorMRT(fout, input.tShadowBuffer); \
}

//! Macro to create the specified PS
#define CREATEPSSN_DIRECT(name,structureName) \
void PSMRT##name(structureName input, out PSOUTMRT oColorMRT) \
{ \
  half4 oColor; /*dummy*/ \
  PF##name(input,false,input.tShadowBuffer,input.vPos,FinalColor,true,oColor,oColorMRT); \
}


//! Define the shaders from list stored in header
#define PS_DEFC(name) CREATEPSSN(name,S##name)
#define PS_DEFS(name,structureName) CREATEPSSN(name,structureName)
#define PS_DEFN(name,structureName) CREATEPSSN_NSMRT(name,structureName)
#define PS_DEFD(name) CREATEPSSN_DIRECT(name,S##name)
#define PS_DEFDS(name,structureName) CREATEPSSN_DIRECT(name,structureName)
PS_LIST(PS_DEFC,PS_DEFS,PS_DEFN,PS_DEFD,PS_DEFDS)