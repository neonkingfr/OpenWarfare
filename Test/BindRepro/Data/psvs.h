#line 2 "W:\c\Poseidon\lib\d3d9\ShaderSources_3_0\psvs.h"

//! Register designed for retrieving the diffuse color from VS
#define TEXCOORDAMBIENT TEXCOORD6
#define TEXCOORDSPECULAR TEXCOORD7
#define TEXCOORDDIFFUSE TEXCOORD4

//! Register designed for retrieving the shadow map coordinates from VS
#define TEXCOORDSR TEXCOORD5

/// VSShaderPool out declaration - rendering mode Common
struct VSShaderPoolOutCommon // seems unused ???
{
  float4 oColor0              : COLOR0;
  float4 oFog                 : COLOR1;
  float4 oUpLocal             : TEXCOORD4;
  float4 oShadowMap           : TEXCOORD5;
  float4 oOutAmbient          : TEXCOORDAMBIENT; // 6
  float4 oOutSpecular         : TEXCOORDSPECULAR; // 7
  float4 oTexCoord[4]         : TEXCOORD0; //0..3
};

struct VSShaderPoolOutShadowBuffer // seems unused ???
{
  float4 oPosition            : TEXCOORD7;
  float4 oTexCoord[4]         : TEXCOORD0;
};

struct VSShaderPoolOutDepthMap // seems unused ???
{
  float4 oPosition            : TEXCOORD7;
  float4 oTexCoord[4]         : TEXCOORD0;
};

struct SDetail
{
  half4 tPSFog                    : COLOR1;
  float4 tDiffuseMap_DetailMap    : TEXCOORD0;
  float4 x_x_nDotU_nDotL          : TEXCOORD3;
  float4 tShadowBuffer            : TEXCOORDSR;
  half4 ambientColor              : TEXCOORDAMBIENT;
  half4 landShadow                : TEXCOORDSPECULAR;
  float2 vPos                     : VPOS;
};

struct SDetailSpecularAlpha
{
  half4 tPSFog                   : COLOR1;
  float4 tDiffuseMap_DetailMap   : TEXCOORD0;
  float4 x_x_nDotU_nDotL         : TEXCOORD3;
  float4 tShadowBuffer           : TEXCOORDSR;
  half4 ambientColor             : TEXCOORDAMBIENT;
  half4 specularColor_landShadow : TEXCOORDSPECULAR;
  float2 vPos                    : VPOS;
};

struct SNormal
{
  half4 tPSFog                   : COLOR1;
  float4 tDiffuseMap             : TEXCOORD0;
  float4 x_x_nDotU_nDotL         : TEXCOORD3;
  float4 tShadowBuffer           : TEXCOORDSR;
  half4 ambientColor             : TEXCOORDAMBIENT;
  half4 landShadow               : TEXCOORDSPECULAR;
  float2 vPos                    : VPOS;
};

struct SGrass
{
  // TEXCOORD1 normally used for texture uv
  half4 tPSFog                   : COLOR1;
  float2 tDiffuseMap             : TEXCOORD0;
  half4 lightingColor            : TEXCOORD1;
  float4 x_x_nDotU_nDotL         : TEXCOORD3;
  float4 tShadowBuffer           : TEXCOORDSR;
  half4 ambientColor             : TEXCOORDAMBIENT;
  half4 landShadow               : TEXCOORDSPECULAR;
  float2 vPos                    : VPOS;
};

struct SSpecularAlpha
{
  half4 tPSFog                   : COLOR1;
  float2 tDiffuseMap             : TEXCOORD0;
  float4 x_x_nDotU_nDotL         : TEXCOORD3;
  float4 tShadowBuffer           : TEXCOORDSR;
  half4 ambientColor             : TEXCOORDAMBIENT;
  half4 specularColor_landShadow : TEXCOORDSPECULAR;
  float2 vPos                    : VPOS;
};

struct SSpecularNormalMapGrass
{
  half4 tPSFog                  : COLOR1;
  float4 tDiffuseMap_NormalMap  : TEXCOORD0;
  half4 tUpLocal                : TEXCOORD4;
  float4 tShadowBuffer          : TEXCOORDSR;
  half4 ambientColor            : TEXCOORDAMBIENT;
  half4 lightLocal_landShadow   : TEXCOORDSPECULAR;
  float2 vPos                   : VPOS;
};

struct SSpecularNormalMapThrough
{
  half4 tPSFog                      : COLOR1;
  float4 tDiffuseMap_NormalMap      : TEXCOORD0;
  float4 tDiffuseAtten_AmbientAtten : TEXCOORD1;
  half4 tUpLocal                    : TEXCOORD4;
  float4 tShadowBuffer              : TEXCOORDSR;
  half4 ambientColor                : TEXCOORDAMBIENT;
  half4 lightLocal_landShadow       : TEXCOORDSPECULAR;
  float2 vPos                       : VPOS;
};

struct SSpecularNormalMapSpecularThrough
{
  half4 tPSFog                      : COLOR1;
  float4 tDiffuseMap_NormalMap      : TEXCOORD0;
  float4 tDiffuseAtten_AmbientAtten : TEXCOORD1;
  half3 tHalfway                    : TEXCOORD3;
  half4 tUpLocal                    : TEXCOORD4;
  float4 tShadowBuffer              : TEXCOORDSR;
  half4 ambientColor                : TEXCOORDAMBIENT;
  half4 lightLocal_landShadow       : TEXCOORDSPECULAR;
  float2 vPos                       : VPOS;
};

struct STree
{
  half4 tPSFog                      : COLOR1;
  float4 tDiffuseMap_NormalMap      : TEXCOORD0;
  float4 tShadowMap                 : TEXCOORD1;
  half3 tHalfway                    : TEXCOORD3;
  half4 tUpLocal                    : TEXCOORD4;
  float4 tShadowBuffer              : TEXCOORDSR;
  half4 ambientColor                : TEXCOORDAMBIENT;
  half4 lightLocal_landShadow       : TEXCOORDSPECULAR;
  float2 vPos                       : VPOS;
};

struct STreePRT
{
  half4 tPSFog                      : COLOR1;
  float4 tDiffuseMap_prtMap         : TEXCOORD0;
  float4 tMacroMap                  : TEXCOORD1;
  half4 tPRTOffsfet                 : TEXCOORD4;
  float4 tShadowBuffer              : TEXCOORDSR;
  half4 ambientColor                : TEXCOORDAMBIENT;
  half4 lightLocal_landShadow       : TEXCOORDSPECULAR;
  float2 vPos                       : VPOS;
};

struct SDetailMacroAS
{
  half4 tPSFog                 : COLOR1;
  float4 tDiffuseMap_DetailMap : TEXCOORD0;
  float4 tMacroMap_ShadowMap   : TEXCOORD1;
  float4 x_x_nDotU_nDotL       : TEXCOORD3;
  float4 tShadowBuffer         : TEXCOORDSR;
  half4 ambientColor           : TEXCOORDAMBIENT;
  half4 landShadow             : TEXCOORDSPECULAR;
  float2 vPos                  : VPOS;
};

struct SDetailSpecularAlphaMacroAS
{
  half4 tPSFog                    : COLOR1;
  float4 tDiffuseMap_DetailMap    : TEXCOORD0;
  float4 tMacroMap_ShadowMap      : TEXCOORD1;
  float4 x_x_nDotU_nDotL          : TEXCOORD3;
  float4 tShadowBuffer            : TEXCOORDSR;
  half4 ambientColor              : TEXCOORDAMBIENT;
  half4 specularColor_landShadow  : TEXCOORDSPECULAR;
  float2 vPos                     : VPOS;
};

struct SNormalMap
{
  half3 tLightLocal             : COLOR0;
  half4 tPSFog                  : COLOR1;
  float4 tDiffuseMap_NormalMap  : TEXCOORD0;
  half3 tHalfway                : TEXCOORD3;
  half4 tUpLocal                : TEXCOORD4;
  float4 tShadowBuffer          : TEXCOORDSR;
  half4 ambientColor            : TEXCOORDAMBIENT;
  half4 landShadow              : TEXCOORDSPECULAR;
  float2 vPos                   : VPOS;
};

struct SNormalMapDetailMacroASSpecularMap
{
  half3 tLightLocal             : COLOR0;
  half4 tPSFog                  : COLOR1;
  float4 tDiffuseMap_NormalMap  : TEXCOORD0;
  float4 tDetailMap_MacroMap    : TEXCOORD1;
  float4 tShadowMap_SpecularMap : TEXCOORD2;
  half3 tHalfway                : TEXCOORD3;
  half4 tUpLocal                : TEXCOORD4;
  float4 tShadowBuffer          : TEXCOORDSR;
  half4 ambientColor            : TEXCOORDAMBIENT;
  half4 landShadow              : TEXCOORDSPECULAR;
  float2 vPos                   : VPOS;
};

#define SNormalMapDetailMacroASSpecularDIMap SNormalMapDetailMacroASSpecularMap

struct SNormalMapDetailSpecularMap
{
  half3 tLightLocal             : COLOR0;
  half4 tPSFog                  : COLOR1;
  float4 tDiffuseMap_NormalMap  : TEXCOORD0;
  float4 tDetailMap_SpecularMap : TEXCOORD1;
  half3 tHalfway                : TEXCOORD3;
  half4 tUpLocal                : TEXCOORD4;
  float4 tShadowBuffer          : TEXCOORDSR;
  half4 ambientColor            : TEXCOORDAMBIENT;
  half4 landShadow              : TEXCOORDSPECULAR;
  float2 vPos                   : VPOS;
};

#define SNormalMapDetailSpecularDIMap SNormalMapDetailSpecularMap

struct SNormalMapMacroAS
{
  half3 tLightLocal            : COLOR0;
  half4 tPSFog                 : COLOR1;
  float4 tDiffuseMap_NormalMap : TEXCOORD0;
  float4 tMacroMap             : TEXCOORD1;
  float4 tShadowMap            : TEXCOORD2;
  half3 tHalfway               : TEXCOORD3;
  half4 tUpLocal               : TEXCOORD4;
  float4 tShadowBuffer         : TEXCOORDSR;
  half4 ambientColor           : TEXCOORDAMBIENT;
  half4 landShadow             : TEXCOORDSPECULAR;
  float2 vPos                  : VPOS;
};

struct SNormalMapMacroASSpecularMap
{
  float4 tShadowBuffer         : TEXCOORDSR;
  half4 ambientColor           : TEXCOORDAMBIENT;
  half4 landShadow             : TEXCOORDSPECULAR;
  half3 tLightLocal            : COLOR0;
  half4 tPSFog                 : COLOR1;
  half4 tUpLocal               : TEXCOORD4;
  float4 tDiffuseMap_NormalMap : TEXCOORD0;
  float4 tMacroMap_ShadowMap   : TEXCOORD1;
  float4 tSpecularMap          : TEXCOORD2;
  half3 tHalfway               : TEXCOORD3;
  float2 vPos                  : VPOS;
};

struct SNormalMapSpecularMap
{
  half3 tLightLocal            : COLOR0;
  half4 tPSFog                 : COLOR1;
  float4 tDiffuseMap_NormalMap : TEXCOORD0;
  float4 tSpecularMap          : TEXCOORD1;
  half3 tHalfway               : TEXCOORD3;
  half4 tUpLocal               : TEXCOORD4;
  float4 tShadowBuffer         : TEXCOORDSR;
  half4 ambientColor           : TEXCOORDAMBIENT;
  half4 landShadow             : TEXCOORDSPECULAR;
  float2 vPos                  : VPOS;
};

#define SNormalMapMacroASSpecularDIMap SNormalMapMacroASSpecularMap
#define SNormalMapSpecularDIMap SNormalMapSpecularMap

struct SSpecularNormalMapDiffuse
{
  half3 tLightLocal              : COLOR0;
  half4 tPSFog                   : COLOR1;
  float4 tColorMap_NormalMap     : TEXCOORD0;
  float4 tDetailMap              : TEXCOORD1;
  half4 tUpLocal                 : TEXCOORD4;
  float4 tShadowBuffer           : TEXCOORDSR;
  half4 ambientColor             : TEXCOORDAMBIENT;
  half4 specularColor_landShadow : TEXCOORDSPECULAR;
  float2 vPos                    : VPOS;
};

struct SSpecularNormalMapDiffuseMacroAS
{
  half3 tLightLocal              : COLOR0;
  half4 tPSFog                   : COLOR1;
  float4 tColorMap_NormalMap     : TEXCOORD0;
  float4 tDetailMap_MacroMap     : TEXCOORD1;
  float4 tShadowMap              : TEXCOORD2;
  half4 tUpLocal                 : TEXCOORD4;
  float4 tShadowBuffer           : TEXCOORDSR;
  half4 ambientColor             : TEXCOORDAMBIENT;
  half4 specularColor_landShadow : TEXCOORDSPECULAR;
  float2 vPos                    : VPOS;
};

struct SSprite
{
  half4 tPSFog                    : COLOR1;
  float4 tShadowBuffer            : TEXCOORDSR;
  half4 ambientColor              : TEXCOORDAMBIENT;
  half4 specularColor_landShadow  : TEXCOORDSPECULAR;
  half4 diffuseColor              : TEXCOORDDIFFUSE;
  float4 tDiffuseMap              : TEXCOORD0;
  half4 modulateColor             : TEXCOORD2;
  float4 tPosition                : TEXCOORD3;
  float2 vPos                     : VPOS;
};

struct STerrain
{
  float4 tShadowBuffer           : TEXCOORDSR; //5
  half4 ambientColor             : TEXCOORDAMBIENT; //6
  half4 specularColor            : TEXCOORDSPECULAR; //7
  half3 tLightLocal              : COLOR0;
  half4 tPSFog                   : COLOR1;
  float4 tSatAndMask_GrassMap    : TEXCOORD4;
  float4 tColorMap_NormalMap     : TEXCOORD0;
  float4 tDetailMap_SpecularMap  : TEXCOORD1;
  float4 grassAlpha              : TEXCOORD2;
  half3 tEyeLocal                : TEXCOORD3;
  float2 vPos                    : VPOS;
};

struct SShore
{
  float4 tNormalMap1_2          : TEXCOORD1;
  float2 tNormalMap3            : TEXCOORD3;
  half4 ambientColor            : TEXCOORDAMBIENT;
  float4 tFresnelView           : TEXCOORD2;
  half3 specularHalfway         : COLOR0;
  half4 tPSFog                  : COLOR1;
  float4 tHeight                : TEXCOORD0;
  float4 stn2lws0               : TEXCOORD4;
  float4 stn2lws1               : TEXCOORD5;
  float4 stn2lws2               : TEXCOORDSPECULAR;
};

#define SShoreWet SShore

struct SGlass
{
  half4 tPSFog                   : COLOR1;
  float4 tDiffuseMap             : TEXCOORD0;
  float4 lwsPosition             : TEXCOORD1;
  half4 lwsNormal                : TEXCOORD2;
  float4 x_x_nDotU_nDotL         : TEXCOORD3;
  float4 tShadowBuffer           : TEXCOORDSR;
  half4 ambientColor             : TEXCOORDAMBIENT;
  half4 specularColor_landShadow : TEXCOORDSPECULAR;
  float2 vPos                    : VPOS;
};

struct SCrater // seems unused ???
{
  float2 tDiffuseMap            : TEXCOORD0;
  float4 originalPosition       : TEXCOORDSPECULAR;
  float4 tShadowBuffer          : TEXCOORDSR;
  half4 tPSFog                  : COLOR1;
  float2 vPos                   : VPOS;
};

struct SSuper
{
  half4 stn2lws0                : COLOR0;
  half4 tPSFog                  : COLOR1;
  float4 tDiffuseMap_NormalMap  : TEXCOORD0;
  float4 tDetailMap_MacroMap    : TEXCOORD1;
  float4 tShadowMap_SpecularMap : TEXCOORD2;
  half4 stn2lws1                : TEXCOORD3;
  half4 stn2lws2                : TEXCOORD4;
  float4 tShadowBuffer          : TEXCOORDSR;
  half4 ambientColor            : TEXCOORDAMBIENT;
  half4 landShadow              : TEXCOORDSPECULAR;
  float2 vPos                   : VPOS;
};

struct SMulti
{
  half3 tLightLocal            : COLOR0;
  half4 tPSFog                 : COLOR1;
  float4 tCO0_CO1              : TEXCOORD0;
  float4 tCO2_CO3              : TEXCOORD1;
  float4 tMSK_DT0              : TEXCOORD2;
  float4 tDT1_DT2              : TEXCOORD3;
  half4 tUpLocal               : TEXCOORD4;
  float4 tShadowBuffer         : TEXCOORDSR;
  half4 ambientColor           : TEXCOORDAMBIENT;
  half4 halfway_landShadow     : TEXCOORDSPECULAR;
  float2 vPos                  : VPOS;
};

struct VSPP_OUTPUT
{
  float4 Position   : POSITION;
  float2 TexCoord   : TEXCOORD;
};
struct VSPP_OUTPUT_SHADOWS
{
  float4 Position   : POSITION;
  float2 TexCoord   : TEXCOORD0;
  float4 Color      : COLOR0;
};
struct VSPP4T_OUTPUT
{
  float4 Position   : POSITION;
  float2 TexCoord0  : TEXCOORD0;
  float2 TexCoord1  : TEXCOORD1;
  float2 TexCoord2  : TEXCOORD2;
  float2 TexCoord3  : TEXCOORD3;
};
