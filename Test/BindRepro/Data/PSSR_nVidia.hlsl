#line 2 "W:\c\Poseidon\lib\d3d9\ShaderSources_3_0\PSSR_nVidia.hlsl"

#include "ps.h"

#if _SM2
  //! Macro to create the specified PS
  #define CREATEPSSN(name,structureName) \
  OPT_INPUTS void PSSR##name##_nVidia(structureName input, out half4 oColor: COLOR) \
  { \
    FOut fout = PF##name(input,false,false); \
    oColor = GetFinalColor(fout); \
  }
  //! Macro to create the specified PS
  #define CREATEPSSN_DIRECT(name,structureName) \
  OPT_INPUTS void PSSR##name##_nVidia(structureName input, out half4 oColor: COLOR) \
  { \
    PF##name(input,false,input.tShadowBuffer,input.vPos,FinalColor,oColor); \
  }
#else
  //! Macro to create the specified PS
  #define CREATEPSSN(name,structureName) \
  OPT_INPUTS void PSSR##name##_nVidia(structureName input, out half4 oColor: COLOR) \
  { \
    FOut fout = PF##name(input,true,false); \
    oColor = GetFinalColorSR_nVidia(fout, input.tShadowBuffer, input.vPos); \
  }
  //! Macro to create the specified PS
  #define CREATEPSSN_DIRECT(name,structureName) \
  OPT_INPUTS void PSSR##name##_nVidia(structureName input, out half4 oColor: COLOR) \
  { \
    PF##name(input,true,input.tShadowBuffer,input.vPos,FinalColorSR_nVidia,oColor); \
  }
#endif

//! Define the shaders from list stored in header
#define PS_DEFC(name) CREATEPSSN(name,S##name)
#define PS_DEFS(name,structureName) CREATEPSSN(name,structureName)
#define PS_DEFN(name,structureName) CREATEPSSN_NS(name,structureName)
#define PS_DEFD(name) CREATEPSSN_DIRECT(name,S##name)
#define PS_DEFDS(name,structureName) CREATEPSSN_DIRECT(name,structureName)
PS_LIST(PS_DEFC,PS_DEFS,PS_DEFN,PS_DEFD,PS_DEFDS)