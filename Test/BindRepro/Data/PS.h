#line 2 "W:\c\Poseidon\lib\d3d9\ShaderSources_3_0\PS.h"

#include "psvs.h"
#include "common.h"

#ifdef _XBOX
# define OPT_INPUTS [removeUnusedInputs]
#else
# define OPT_INPUTS
#endif

//! Samplers used by all the PS's
sampler2D samplers[15]            : register(s0);
#define sampler0              samplers[0]
#define sampler1              samplers[1]
#define sampler2              samplers[2]
#define sampler3              samplers[3]
#define sampler4              samplers[4]
#define sampler5              samplers[5]
#define sampler6              samplers[6]
#define sampler7              samplers[7]
#define sampler8              samplers[8]
#define sampler9              samplers[9]
#define sampler10             samplers[10]
#define sampler11             samplers[11]
#define sampler12             samplers[12]
#define sampler13             samplers[13]
#define sampler14             samplers[14]
#define samplerTI             samplers[14]
sampler2D samplerShadowMap        : register(s15); // samplerShadowMap must be performed via register (not an array), otherwise the X360 compilation will fail - the ASM in shadow buffers cannot read an array
sampler2D samplerTc               : register(s15);
//sampler2D samplerLightIntensity   : register(s15);
#define samplerLightIntensity   sampler12

//! Constant definition
#define PSC_HLSLDEFS(type,name,regtype,regnum) type name : register(regtype##regnum);
#define PSC_HLSLDEFA(type,name,regtype,regnum,dim) type name[dim] : register(regtype##regnum);
PSC_LIST(PSC_HLSLDEFS,PSC_HLSLDEFA)

//! An alternative to tex2D method, includes PSC_MaxColor multiplication
half4 tex2D0(half2 t)
{
  return tex2D(sampler0, t) * PSC_MaxColor;
}

//! An alternative to tex2D method, includes PSC_MaxColor multiplication and uses the mip level 0 only
half4 tex2DLOD0(half2 t)
{
#if _SM2
  half4 texCoord = half4(t.x, t.y, 0, -20);
  return tex2Dbias(sampler0, texCoord) * PSC_MaxColor;
#else
  half4 texCoord = half4(t.x, t.y, 0, 0);
  return tex2Dlod(sampler0, texCoord) * PSC_MaxColor;
#endif
}

//! Structure to hold separated components of light - GetLightX functions returns this
struct Light
{
  //! Light from the environment - roughly Ambient, DForced and Emmisive
  half3 indirect;
  //! Light from the main light source (light that disappears in a shadow) - roughly the diffuse light
  half3 direct;
  //! Specular light
  half3 specular;
  //! Specular light reflected from the environment (shadows doesn't affect it)
  half3 specularEnv;
  //! Diffuse shadow value, used where no ordinary shadows are available (f.i. for objects in distance)
  half diffuseShadowCoef;
};

/////////////////////////////////////////////////////////////////////////////////////////////
// Functions to calculate the light components

Light GetLightSimple(half3 ambient, half3 diffuse, half3 specular)
{
  Light light;
  light.indirect = ambient;
  light.direct = diffuse;
  light.specular = specular;
  light.specularEnv = 0;
  light.diffuseShadowCoef = 1;
  return light;
}

#if 1
half3 GetIndirectLight(half nDotU)
{
  return lerp(PSC_GE.rgb, PSC_AE.rgb, (nDotU + 1) * 0.5);
}
#else
half3 GetIndirectLight(half nDotU)
{
  return PSC_AE.rgb;
}
#endif

Light GetLight(half3 ambient, half3 specular, half4 x_x_nDotU_nDotL)
{
  // Get the diffuse coefficient
  half coefDiffuse = max(x_x_nDotU_nDotL.w, 0);
  half coefDiffuseBack = max(-x_x_nDotU_nDotL.w, 0);

  // Fill out the light structure
  Light light;
  light.indirect = ambient + GetIndirectLight(x_x_nDotU_nDotL.z) + PSC_LDirectionGround * coefDiffuseBack;
  light.direct = PSC_DForced + PSC_Diffuse * coefDiffuse;
  light.specular = specular;
  light.specularEnv = 0;
  light.diffuseShadowCoef = 1;
  return light;
}

Light GetLightAS(half3 ambient, half3 specular, half4 x_x_nDotU_nDotL, half4 ambientShadow)
{
  // Get the diffuse coefficient
  half coefDiffuse = max(x_x_nDotU_nDotL.w, 0);
  half coefDiffuseBack = max(-x_x_nDotU_nDotL.w, 0);

  // Fill out the light structure
  Light light;
  light.indirect = ambient + (GetIndirectLight(x_x_nDotU_nDotL.z) + PSC_LDirectionGround * coefDiffuseBack) * ambientShadow.g;
  light.direct = PSC_DForced + PSC_Diffuse * coefDiffuse;
  light.specular = specular;
  light.specularEnv = 0;
  light.diffuseShadowCoef = ambientShadow.b;
  return light;
}

half4 GetIrradianceData(half3 lightLocal, half3 upLocal, half3 halfway,
                        half4 normal4, sampler2D samplerIrradiance, out half nDotL, out half nDotU)
{
  // Get the values from the irradiance map
  half3 normal = normal4.rgb;
  normal = normal * 2 - 1;
  nDotL = dot(normal, lightLocal);
  nDotU = dot(normal, upLocal);
  half u = nDotL;
  half v = dot(normal, halfway);
  return tex2D(samplerIrradiance, half2(u, v));
}

Light GetLightIrradiance(half3 ambient, half4 irradiance, half specularCoef, half nDotL, half nDotU)
{
  // Fill out the light structure
  half coefDiffuseBack = max(-nDotL, 0);
  Light light;
  light.indirect = ambient + GetIndirectLight(nDotU) + PSC_LDirectionGround * coefDiffuseBack;
  light.direct = PSC_DForced + PSC_Diffuse.rgb * irradiance.rgb;
  light.specular = PSC_Specular.rgb * irradiance.a * specularCoef;
  light.specularEnv = 0;
  light.diffuseShadowCoef = 1;
  return light;
}

Light GetLightNormal(half3 ambient, half3 specular,
                     half4 normal4, half3 lightLocal, half3 upLocal, out half coefDiffuse)
{
  // Get the normal
  half3 dxt5FixedNormal = half3(1 - normal4.a, 0, 0) + normal4.rgb;
  half3 normal = normalize(dxt5FixedNormal * 2 - 1);

  // Get the diffuse coefficient  
  half nDotL = dot(normal,lightLocal);
  coefDiffuse = max(nDotL, 0);
  half coefDiffuseBack = max(-nDotL, 0);

  // Fill out the light structure
  Light light;
  light.indirect = ambient + GetIndirectLight(dot(normal,upLocal)) + PSC_LDirectionGround * coefDiffuseBack;
  light.direct = PSC_DForced + PSC_Diffuse * coefDiffuse;
  light.specular = specular;
  light.specularEnv = 0;
  light.diffuseShadowCoef = 1;
  return light;
}

Light GetLightNormalAS(half3 ambient, half3 specular,
                       half4 normal4, half3 lightLocal, half3 upLocal, half4 ambientShadow, out half coefDiffuse)
{
  // Get the normal
  half3 dxt5FixedNormal = half3(1 - normal4.a, 0, 0) + normal4.rgb;
  half3 normal = normalize(dxt5FixedNormal * 2 - 1);

  // Get the diffuse coefficient  
  half nDotL = dot(normal,lightLocal);
  coefDiffuse = max(nDotL, 0);
  half coefDiffuseBack = max(-nDotL, 0);

  // Fill out the light structure
  Light light;
  light.indirect = ambient + (GetIndirectLight(dot(normal,upLocal)) + PSC_LDirectionGround * coefDiffuseBack) * ambientShadow.g;
  light.direct = PSC_DForced + PSC_Diffuse * coefDiffuse;
  light.specular = specular;
  light.specularEnv = 0;
  light.diffuseShadowCoef = ambientShadow.b;
  return light;
}

Light GetLightNormalSpecular(half3 ambient,
                             half4 normal4, half3 lightLocal, half3 halfway, half3 upLocal,
                             half4 specular, out half coefDiffuse)
{
  // Get the normal
  half3 dxt5FixedNormal = half3(1 - normal4.a, 0, 0) + normal4.rgb;
  half3 normal = normalize(dxt5FixedNormal * 2 - 1);

  // Get the diffuse and specular coefficients
  half nDotL = dot(normal,lightLocal);
  half4 litVector = lit(nDotL, dot(normal, halfway), PSC_Specular.a * specular.b);
  coefDiffuse = litVector.y;
  half coefSpecular = litVector.z;
  coefSpecular = min(coefSpecular, 1); // Specular power can be very high and this can probably lead to infinity (if the coefSpecular happen to be just slightly above 1)
  half coefDiffuseBack = max(-nDotL, 0);

  // Fill out the light structure
  Light light;
  light.indirect = ambient + GetIndirectLight(dot(normal,upLocal)) + PSC_LDirectionGround * coefDiffuseBack;
  light.direct = PSC_DForced + PSC_Diffuse * coefDiffuse * specular.r;
  light.specular = PSC_Specular * coefSpecular * specular.g;
  light.specularEnv = 0;
  light.diffuseShadowCoef = 1;
  return light;
}

Light GetLightNormalSpecularDI(half3 ambient,
                               half4 normal4, half3 lightLocal, half3 halfway, half3 upLocal,
                               half4 specular, out half coefDiffuse)
{
  // Get the normal
  half3 dxt5FixedNormal = half3(1 - normal4.a, 0, 0) + normal4.rgb;
  half3 normal = normalize(dxt5FixedNormal * 2 - 1);

  // Get the diffuse and specular coefficients
  half nDotL = dot(normal,lightLocal);
  half4 litVector = lit(nDotL, dot(normal, halfway), PSC_Specular.a * specular.b);
  coefDiffuse = litVector.y;
  half coefSpecular = litVector.z;
  coefSpecular = min(coefSpecular, 1); // Specular power can be very high and this can probably lead to infinity (if the coefSpecular happen to be just slightly above 1)
  half coefDiffuseBack = max(-nDotL, 0);

  // Fill out the light structure
  Light light;
  light.indirect = ambient + GetIndirectLight(dot(normal,upLocal)) + PSC_LDirectionGround * coefDiffuseBack;
  light.direct = PSC_DForced + PSC_Diffuse * coefDiffuse * (1 - specular.g);
  light.specular = PSC_Specular * coefSpecular * specular.g;
  light.specularEnv = 0;
  light.diffuseShadowCoef = 1;
  return light;
}

Light GetLightNormalSpecularAS(half3 ambient,
                               half4 normal4, half3 lightLocal, half3 halfway, half3 upLocal,
                               half4 specular, half4 ambientShadow, out half coefDiffuse)
{
  // Get the normal
  half3 dxt5FixedNormal = half3(1 - normal4.a, 0, 0) + normal4.rgb;
  half3 normal = normalize(dxt5FixedNormal * 2 - 1);

  // Get the diffuse and specular coefficients
  half nDotL = dot(normal,lightLocal);
  half4 litVector = lit(nDotL, dot(normal, halfway), PSC_Specular.a * specular.b);
  coefDiffuse = litVector.y;
  half coefSpecular = litVector.z;
  coefSpecular = min(coefSpecular, 1); // Specular power can be very high and this can probably lead to infinity (if the coefSpecular happen to be just slightly above 1)
  half coefDiffuseBack = max(-nDotL, 0);

  // Fill out the light structure
  Light light;
  light.indirect = ambient + (GetIndirectLight(dot(normal,upLocal)) + PSC_LDirectionGround * coefDiffuseBack) * ambientShadow.g;
  light.direct = PSC_DForced + PSC_Diffuse * coefDiffuse * specular.r;
  light.specular = PSC_Specular * coefSpecular * specular.g;
  light.specularEnv = 0;
  light.diffuseShadowCoef = ambientShadow.b;
  return light;
}

Light GetLightNormalSpecularDIAS(half3 ambient,
                                 half4 normal4, half3 lightLocal, half3 halfway, half3 upLocal,
                                 half4 specular, half4 ambientShadow, out half coefDiffuse)
{
  // Get the normal
  half3 dxt5FixedNormal = half3(1 - normal4.a, 0, 0) + normal4.rgb;
  half3 normal = normalize(dxt5FixedNormal * 2 - 1);

  // Get the diffuse and specular coefficients
  half nDotL = dot(normal,lightLocal);
  half4 litVector = lit(nDotL, dot(normal, halfway), PSC_Specular.a * specular.b);
  coefDiffuse = litVector.y;
  half coefSpecular = litVector.z;
  coefSpecular = min(coefSpecular, 1); // Specular power can be very high and this can probably lead to infinity (if the coefSpecular happen to be just slightly above 1)
  half coefDiffuseBack = max(-nDotL, 0);

  // Fill out the light structure
  Light light;
  light.indirect = ambient + (GetIndirectLight(dot(normal,upLocal)) + PSC_LDirectionGround * coefDiffuseBack) * ambientShadow.g;
  light.direct = PSC_DForced + PSC_Diffuse * coefDiffuse * (1 - specular.g);
  light.specular = PSC_Specular * coefSpecular * specular.g;
  light.specularEnv = 0;
  light.diffuseShadowCoef = ambientShadow.b;
  return light;
}

Light GetLightSuper(half3 ambient, half3 lwsNormal, half3 lwsHalfway, half3 envColor, half fresnelCoef,
                    half4 specular, half4 ambientShadow, out half coefDiffuse)
{
  // Get the diffuse and specular coefficients
  half nDotL = dot(lwsNormal, PSC_WLight);
  half4 litVector = lit(nDotL, dot(lwsNormal, lwsHalfway), PSC_Specular.a * specular.b);
  coefDiffuse = litVector.y;
  half coefSpecular = litVector.z;
  coefSpecular = min(coefSpecular, 1); // Specular power can be very high and this can probably lead to infinity (if the coefSpecular happen to be just slightly above 1)
  half coefDiffuseBack = max(-nDotL, 0);

  // Get the specular coefficient
  half specularCoef = fresnelCoef * specular.g;

  // Fill out the light structure
  Light light;
  light.indirect = ambient + (GetIndirectLight(lwsNormal.y) + PSC_LDirectionGround * coefDiffuseBack) * ambientShadow.g;
  light.direct = PSC_DForced + PSC_Diffuse * coefDiffuse * (1 - specularCoef);
  light.specular = PSC_Specular * coefSpecular * specularCoef;
  light.specularEnv = envColor * PSC_GlassMatSpecular * 2.0f * specularCoef; // 2 is here because specular color is halved
  light.diffuseShadowCoef = ambientShadow.b;
  return light;
}

/////////////////////////////////////////////////////////////////////////////////////////////
// Functions to retrieve the color and alpha from textures

half4 GetColorDiffuse(half4 diffuse, half alphaMul, half alphaAdd)
{
  return half4(diffuse.rgb, diffuse.a * alphaMul + alphaAdd);
}

half4 GetColorDiffuseDXTADiscrete(half4 diffuse, half alphaMul)
{
  half dalpha = (diffuse.a * alphaMul) > 0.5;
  return half4((1 - diffuse.a) * PSC_T0AvgColor.rgb + diffuse.rgb, dalpha);
}

/** needs to match PSAlphaOnlyTree */
half4 GetColorDiffuseDiscreteNoise(half4 diffuse, half mul, half add)
{
#ifdef _XBOX
  // Label: AlphaToCoverage
  // on X360 we use alpha to coverage (D3DRS_ALPHATOMASKENABLE)
  // TODO: adding some little noise could help
  // note: alpha to coverage mask may be slightly inaccurate, as it will contain shadow intensity as well
  // adding 1/N noise, where N is number of samples (4 with no AA)
  //return half4(diffuse.rgb, (diffuse.a*mul+add*PSC_DitherCoef.w) * PSC_SBTSize_invSBTSize_X_AlphaMult.w);

//   float desiredAlpha = diffuse.a * mul;
//   float alpha = (desiredAlpha <= 0.5) ? desiredAlpha * add * 2.0f : 2.0f * (desiredAlpha + add - desiredAlpha * add) - 1;
//   return half4(diffuse.rgb, alpha);

  return half4(diffuse.rgb, diffuse.a * mul);

#else
  #if _VBS3
  // we are adding noise - random value from 0 to 1
  //half dalpha = (diffuse.a*mul+add) > 1;
  half dalpha = diffuse.a*mul>0.5;
  clip(dalpha-0.5);
  return half4(diffuse.rgb, dalpha);
  #else
  // we are adding noise - random value from 0 to 1
  half dalpha = (diffuse.a*mul+add) > 1;
  clip(dalpha-0.5);
  //half dalpha = (diffuse.a * mul) > 0.5;
  return half4(diffuse.rgb, dalpha);
  #endif
#endif
}

half4 GetColorDiffuseDetail(half4 diffuse, half3 detail, half alphaMul, half alphaAdd)
{
  return half4(diffuse.rgb * detail * 2.0f, diffuse.a * alphaMul + alphaAdd);
}

half4 GetColorDiffuseMacroAS(half4 diffuse, half4 macro, half alphaMul, half alphaAdd)
{
  return half4(lerp(diffuse.rgb, macro.rgb, macro.a), diffuse.a * alphaMul + alphaAdd);
}

half4 GetColorDiffuseDetailMacroAS(half4 diffuse, half3 detail, half4 macro, half alphaMul, half alphaAdd)
{
  return half4(lerp(diffuse.rgb, macro.rgb, macro.a) * detail.rgb * 2, diffuse.a * alphaMul + alphaAdd);
}

half4 GetColorSuper(half4 diffuse, half3 detail, half4 macro, half alphaMul, half specularCoef)
{
  return half4(lerp(diffuse.rgb, macro.rgb, macro.a) * detail.rgb * 2, diffuse.a * alphaMul);
}

/// used to implement pixel shader-side blending (see also PS_BLEND in cpp sources)
#define PS_BLEND 1

half3 ToRTColorSpace(half3 color)
{
  #if 0 // PS_BLEND
  // caution: pow is exp+log, each of them scalar - following code is 6 instructions
  return pow(color,1.0/2.2);
  #else
  return color;
  #endif
}
half4 ToRTColorSpace(half4 color)
{
  return half4(ToRTColorSpace(color.rgb),color.a);
  //return half4(color.rgb,color.a);
}
half3 ApplySrcAlpha(half3 color, half alpha)
{
  #if PS_BLEND
    // used when PS controlled blending is in use
    return color*alpha;
  #else
    return color;
  #endif
}

half4 ApplySrcAlpha(half4 color)
{
  return half4(ApplySrcAlpha(color.rgb,color.a),color.a);
}

//! Structure to hold output of the pixel fragment
struct FOut
{
  Light light;           // Light contribution
  half4 color;           // Diffuse/Ambient color (like texture content), alpha
  bool psBlend;          // blending mode (add/invsrc) controlled in the pixel shader
  bool addBlend;         // use add blending (use invsrc blending otherwise)
  half fog;              // Fog parameter
  half landShadow;       // Landscape shadow coefficient
  float htCoef;           // Thermal capacity related coefficient // _VBS3_TI
  float addTemp;          // Temperature the shader wish to add // _VBS3_TI
  float airCoolingFactor; // Some objects like grass are well cooled and specify this coefficient to be greater than 0 // _VBS3_TI
  float2 tTIMap;          // Mapping coordinates of the TI map // _VBS3_TI
  half nDotL;             // Dot product between normal and main light - used in thermal imaging to determine orientation towards sun // _VBS3_TI
};

//! FOut initializer
FOut InitFOut()
{
  FOut fout;
  fout.light.indirect = 0;
  fout.light.direct = 0;
  fout.light.specular = 0;
  fout.color = 0;
  fout.psBlend = true;
  fout.addBlend = false;
  fout.fog = 0;
  fout.landShadow = 0;
  fout.htCoef = 1.0f;
  fout.addTemp = 0.0f;
  fout.airCoolingFactor = 0.0f;
  fout.tTIMap = half2(0.5f, 0.5f);
  fout.nDotL = 1;
  return fout;
}

//! Fog calculation
half3 ApplyFog(half3 color, half fog)
{
  color.rgb = lerp(PSC_FogColor.rgb, color.rgb, fog);
  return color;
}

//! Function to retrieve the final color of the shader
half4 GetFinalColorWithShadow(FOut fout, half shadowCoef)
{
  half3 colorBlend = (fout.light.indirect + fout.light.direct * shadowCoef) * fout.color.rgb;
  half3 colorAdd = fout.light.specular * shadowCoef + fout.light.specularEnv;
  //half3 colorAdd = 0;
  // See label FINALBLENDING
  #if PS_BLEND
  if (fout.psBlend)
  {
    // blending control implemented in pixel shader
    // D3DRS_SRCBLEND = D3DBLEND_ONE, D3DRS_DESTBLEND = D3DBLEND_INVSRCALPHA
    // ax * fogged(color) + (1-a) * background
    
    if (fout.addBlend)
    {
      // add blending: ax=a, a=0
      return half4(ToRTColorSpace(ApplyFog(colorBlend+colorAdd, fout.fog))*fout.color.a,min(fout.color.a,0.01));
      //return half4(half3(0.01,0,0),0.01);
    }
    else
    {
      // use default blending for normal color: ax = a
      // for additive components (reflections) use add blending (no influence at target alpha)
      return half4(ToRTColorSpace(ApplyFog(colorBlend*fout.color.a+colorAdd, fout.fog)),fout.color.a);
      //return half4(ToRTColorSpace(ApplyFog(colorBlend+colorAdd, fout.fog)*fout.color.a),fout.color.a);
    }
  }
  #endif
  // blending controlled using render states
  // default blending: a * fogged(color) + (1-a) * background
  // D3DRS_SRCBLEND = D3DBLEND_SRCALPHA, D3DRS_DESTBLEND = D3DBLEND_INVSRCALPHA
  
  // add blending: a * fogged(color) + background
  // D3DRS_SRCBLEND = D3DBLEND_SRCALPHA, D3DRS_DESTBLEND = D3DBLEND_ONE
  return half4(ToRTColorSpace(ApplyFog(colorBlend+colorAdd, fout.fog)),fout.color.a);
  
  
}

//! Function to retrieve the final color of the shader
half4 GetFinalColor(FOut fout)
{
  half shadowCoef = fout.light.diffuseShadowCoef * fout.landShadow;
  return GetFinalColorWithShadow(fout,shadowCoef);
}

//! Function to retrieve the final color of the shader
half4 GetFinalColorSSSM(FOut fout, float4 tShadowBuffer, float2 vPos)
{
  const half shadowsZHalf = PSC_Shadow_Factor_ZHalf.w;
  half shadowDisappear = saturate((tShadowBuffer.w - shadowsZHalf) / shadowsZHalf);
#ifdef _XBOX
  half shadowCoef = shadowDisappear + tex2Dlod(samplerShadowMap, float4((vPos.xy + PSCGPU_TOffX_TOffY_X_X[0].xy + float2(0.5, 0.5))*PSC_InvW_InvH_X_X.xy, 0, 1)).r * (1 - shadowDisappear);
#else
  half shadowCoef = shadowDisappear + tex2Dlod(samplerShadowMap, float4((vPos.xy + float2(0.5, 0.5))*PSC_InvW_InvH_X_X.xy, 0, 1)).r * (1 - shadowDisappear);
#endif

  // Involve diffuse map content (use the diffuse shadow map where no shadow is present)
  shadowCoef -= (1 - fout.light.diffuseShadowCoef) * shadowDisappear;

  // Involve land shadow to shadow coefficient
  shadowCoef *= fout.landShadow;

  // Control shadow intensity via PSC_Shadow_Factor_ZHalf.x
  shadowCoef = 1 + PSC_Shadow_Factor_ZHalf.x * (shadowCoef - 1);

  return GetFinalColorWithShadow(fout,shadowCoef);
}


//! Function to retrieve the final color of the shader
half4 GetFinalColorSR_Default(FOut fout, float4 tShadowBuffer, float2 vPos)
{
  // Precomputed constants related to texture size
  const float texsize = PSC_SBTSize_invSBTSize_X_AlphaMult.x;
  const float invTexsize = PSC_SBTSize_invSBTSize_X_AlphaMult.y;

  // Calculate the fractional part of the texel addressing  
  float2 dxy = frac(tShadowBuffer.xy * texsize);

  // Calculate weights for bilinear interpolation

  //float4 fxy = half4(dxy, half2(1,1)-dxy);

  float3 pnz = float3(+1,-1,0);
  float4 fxy = dxy.xyxy * pnz.xxyy + pnz.zzxx;

  // float4((1 - dxy.x) * (1 - dxy.y), (1 - dxy.x) * dxy.y, dxy.x * (1 - dxy.y), dxy.x * dxy.y)
  // float4(fxy.z       * fxy.w,       fxy.z       * fxy.y, fxy.x * fxy.w,       fxy.x * fxy.y)
  float4 weights = fxy.zzxx*fxy.wywy;

  // ddx/ddy are constant for shadow maps?
  
  // Get 4 shadow depths
  float4 shadowDepth = float4(
    tex2Dlod(samplerShadowMap, tShadowBuffer.xyzw).r,
    tex2Dlod(samplerShadowMap, tShadowBuffer.xyzw + float4(0,           invTexsize,0,0)).r,
    tex2Dlod(samplerShadowMap, tShadowBuffer.xyzw + float4(invTexsize,  0,0,0)).r,
    tex2Dlod(samplerShadowMap, tShadowBuffer.xyzw + float4(invTexsize,  invTexsize,0,0)).r
    );

  // Get 4 shadow/not shadow values
  float4 sc = shadowDepth>=tShadowBuffer.z;

  // Use weights to get the shadow coefficient
  half shadowCoef = dot(sc, weights);
  const half shadowsZHalf = PSC_Shadow_Factor_ZHalf.w;
  half shadowDisappear = saturate((tShadowBuffer.w - shadowsZHalf) / shadowsZHalf);
  shadowCoef = shadowDisappear + shadowCoef * (1 - shadowDisappear);

  // Involve diffuse map content (use the diffuse shadow map where no shadow is present)
  shadowCoef -= (1 - fout.light.diffuseShadowCoef) * shadowDisappear;

  // Involve land shadow to shadow coefficient
  shadowCoef *= fout.landShadow;

  // Control shadow intensity via PSC_Shadow_Factor_ZHalf.x
  shadowCoef = 1 + PSC_Shadow_Factor_ZHalf.x * (shadowCoef - 1);

  //  float shadowDisappear = saturate((tShadowBuffer.w - 50) / 50);
  //  float shadowCoef = (tex2D(samplerShadowMap, tShadowBuffer.xy).r < tShadowBuffer.z) ? shadowDisappear : 1;
  return GetFinalColorWithShadow(fout,shadowCoef);
}

//! Function to retrieve the final color of the shader
half4 GetFinalColorSR_nVidia(FOut fout, float4 tShadowBuffer, float2 vPos)
{
  float4 nVidiaSTRQ = float4(tShadowBuffer.xyz, 1);
  const half shadowsZHalf = PSC_Shadow_Factor_ZHalf.w;
  half shadowDisappear = saturate((tShadowBuffer.w - shadowsZHalf) / shadowsZHalf);
  half shadowCoef = shadowDisappear + tex2Dlod(samplerShadowMap, nVidiaSTRQ).r * (1 - shadowDisappear);

  // Involve diffuse map content (use the diffuse shadow map where no shadow is present)
  shadowCoef -= (1 - fout.light.diffuseShadowCoef) * shadowDisappear;

  // Involve land shadow to shadow coefficient
  shadowCoef *= fout.landShadow;

  // Control shadow intensity via PSC_Shadow_Factor_ZHalf.x
  shadowCoef = 1 + PSC_Shadow_Factor_ZHalf.x * (shadowCoef - 1);

  return GetFinalColorWithShadow(fout,shadowCoef);
}

//! Function to retrieve the empty color of the shader
half4 GetFinalColorEmpty()
{
  return 0;
}

//! Function to get the e-based logarithm
// _VBS3_TI
float logE(float x)
{
  return log(x);
}

//! Return the temperature of the air
float AirTemperature()
{
  float offset = 0.5f / 16.0f;
  return tex2D(samplerTc, float2(0 + offset, 2.0f/16.0f + offset)).r;
}

//! Function to retrieve the final color of the shader
// _VBS3_TI
#if _SM2
half4 GetFinalThermal(FOut fout)
{
  // Get the temperature coefficients stored in texture
  // R - thermal capacity factor
  // G - alive factor
  // B - movement factor
  // A - metabolism factor
  float4 tempCoef = tex2D(samplerTI, fout.tTIMap) * float4(fout.htCoef, 1, 1, 1);

  // Calculate the coefficient K
  float halfTime = lerp(PSC_HTMin_HTMax_AFMax_MFMax.x, PSC_HTMin_HTMax_AFMax_MFMax.y, tempCoef.r);

  // The Tc table thermal coefficients are in the exponential scale expE. We have to use the logE to get the table linear addressing
  float halfTimeAddress = logE(halfTime);
  halfTimeAddress = min(max(halfTimeAddress, 0.0f), 11.999f);

  // Load values out of the Tc table
  float halfTimeU = halfTimeAddress * (1.0f/16.0f);
  float offset = 0.5f / 16.0f;
  half tableTcBU = tex2D(samplerTc, float2(halfTimeU + offset,          0 + offset)).r;
  half tableTcBL = tex2D(samplerTc, float2(halfTimeU + offset, 1.0f/16.0f + offset)).r;
  half tableTcWU = tex2D(samplerTc, float2(halfTimeU + offset, 2.0f/16.0f + offset)).r;
  half tableTcWL = tex2D(samplerTc, float2(halfTimeU + offset, 3.0f/16.0f + offset)).r;

  // Do the first of bilinear interpolation - grey
  half greyscale = dot(fout.color.rgb, half3(0.299, 0.587, 0.114));
  half tableTcL = lerp(tableTcBL, tableTcWL, greyscale);
  half tableTcU = lerp(tableTcBU, tableTcWU, greyscale);

  // Do the second of bilinear interpolation - dot
  half tableTc;
  {
    float lightIntensity = lerp(0.5f, max(fout.nDotL, 0), PSC_MFact_TBody_X_HSDiffCoef.w);
    tableTc = lerp(tableTcU, tableTcL, lightIntensity);
  }

  // Calculate the final temperature
  float temperature = lerp(tableTc, PSC_MFact_TBody_X_HSDiffCoef.y, saturate(PSC_MFact_TBody_X_HSDiffCoef.x * tempCoef.a))
    + PSC_HTMin_HTMax_AFMax_MFMax.z * tempCoef.g
    + PSC_HTMin_HTMax_AFMax_MFMax.w * tempCoef.b
    + fout.addTemp;

  // Cool the temperature according to airCoolingFactor (f.i. grass is cooled down a lot)
  temperature = lerp(temperature, AirTemperature(), fout.airCoolingFactor);

  // Encode the temperature into RGB
  float3 rgbEncodedTemperature;
  EncodeTemperatureToRGB(temperature, rgbEncodedTemperature.r, rgbEncodedTemperature.g, rgbEncodedTemperature.b);

  // Write temperature into output, consider alpha and proper alpha mode
  // See label FINALBLENDING
#if PS_BLEND
  if (fout.psBlend)
  {
    if (fout.addBlend)
    {
      return half4(ApplyFog(rgbEncodedTemperature, fout.fog)*fout.color.a,min(fout.color.a,0.01));
    }
    else
    {
      return half4(ApplyFog(rgbEncodedTemperature*fout.color.a, fout.fog),fout.color.a);
    }
  }
#endif
  return half4(ApplyFog(rgbEncodedTemperature, fout.fog),fout.color.a);
}
#else
half4 GetFinalThermal(FOut fout)
{
  // Get the temperature coefficients stored in texture
  // R - thermal capacity factor
  // G - alive factor
  // B - movement factor
  // A - metabolism factor
  float4 tempCoef = tex2D(samplerTI, fout.tTIMap) * float4(fout.htCoef, 1, 1, 1);

  // Calculate the coefficient K
  float halfTime = lerp(PSC_HTMin_HTMax_AFMax_MFMax.x, PSC_HTMin_HTMax_AFMax_MFMax.y, tempCoef.r);

  // The Tc table thermal coefficients are in the exponential scale expE. We have to use the logE to get the table linear addressing
  float halfTimeAddress = logE(halfTime);
  halfTimeAddress = min(max(halfTimeAddress, 0.0f), 11.999f);
  float halfTimeFloor = floor(halfTimeAddress);
  float halfTimeFactor = frac(halfTimeAddress);

  // Load values out of the Tc table
  float u0 = halfTimeFloor * (1.0f/16.0f);
  float u1 = u0 + (1.0f/16.0f);
  float offset = 0.5f / 16.0f;
  half tableTcBU = lerp(tex2D(samplerTc, float2(u0 + offset,          0 + offset)).r, tex2D(samplerTc, float2(u1 + offset,          0 + offset)).r, halfTimeFactor);
  half tableTcBL = lerp(tex2D(samplerTc, float2(u0 + offset, 1.0f/16.0f + offset)).r, tex2D(samplerTc, float2(u1 + offset, 1.0f/16.0f + offset)).r, halfTimeFactor);
  half tableTcWU = lerp(tex2D(samplerTc, float2(u0 + offset, 2.0f/16.0f + offset)).r, tex2D(samplerTc, float2(u1 + offset, 2.0f/16.0f + offset)).r, halfTimeFactor);
  half tableTcWL = lerp(tex2D(samplerTc, float2(u0 + offset, 3.0f/16.0f + offset)).r, tex2D(samplerTc, float2(u1 + offset, 3.0f/16.0f + offset)).r, halfTimeFactor);

  // Do the first of bilinear interpolation - grey
  half greyscale = dot(fout.color.rgb, half3(0.299, 0.587, 0.114));
  half tableTcL = lerp(tableTcBL, tableTcWL, greyscale);
  half tableTcU = lerp(tableTcBU, tableTcWU, greyscale);

  // Do the second of bilinear interpolation - dot
  half tableTc;
  {
    float lightIntensity = lerp(0.5f, max(fout.nDotL, 0), PSC_MFact_TBody_X_HSDiffCoef.w);
    tableTc = lerp(tableTcU, tableTcL, lightIntensity);
  }

  // Calculate the final temperature
  float temperature = lerp(tableTc, PSC_MFact_TBody_X_HSDiffCoef.y, saturate(PSC_MFact_TBody_X_HSDiffCoef.x * tempCoef.a))
    + PSC_HTMin_HTMax_AFMax_MFMax.z * tempCoef.g
    + PSC_HTMin_HTMax_AFMax_MFMax.w * tempCoef.b
    + fout.addTemp;

  // Cool the temperature according to airCoolingFactor (f.i. grass is cooled down a lot)
  temperature = lerp(temperature, AirTemperature(), fout.airCoolingFactor);

  // Encode the temperature into RGB
  float3 rgbEncodedTemperature;
  EncodeTemperatureToRGB(temperature, rgbEncodedTemperature.r, rgbEncodedTemperature.g, rgbEncodedTemperature.b);

  // Write temperature into output, consider alpha and proper alpha mode
  // See label FINALBLENDING
#if PS_BLEND
  if (fout.psBlend)
  {
    if (fout.addBlend)
    {
      return half4(ApplyFog(rgbEncodedTemperature, fout.fog)*fout.color.a,min(fout.color.a,0.01));
    }
    else
    {
      return half4(ApplyFog(rgbEncodedTemperature*fout.color.a, fout.fog),fout.color.a);
    }
  }
#endif
  return half4(ApplyFog(rgbEncodedTemperature, fout.fog),fout.color.a);
}
#endif

static const int FinalColor=0;
static const int FinalColorSSSM=2;
static const int FinalColorSR_Default=3;
static const int FinalColorSR_nVidia=4;
static const int FinalThermal=5;

void CallFinalColor(FOut fout, float4 tShadowBuffer, float2 vPos, int type, out half4 oColor)
{
  if (type==FinalColor) {oColor = GetFinalColor(fout);return;}
  if (type==FinalColorSSSM) {oColor = GetFinalColorSSSM(fout,tShadowBuffer,vPos);return;}
  if (type==FinalColorSR_Default) {oColor = GetFinalColorSR_Default(fout,tShadowBuffer,vPos);return;}
  if (type==FinalColorSR_nVidia) {oColor = GetFinalColorSR_nVidia(fout,tShadowBuffer,vPos);return;}
  if (type==FinalThermal) {oColor = GetFinalThermal(fout);return;}
  oColor = GetFinalColorEmpty();
}

//! Macro to create the specified PS - no shadow variants
#define CREATEPSSN_NS(name,structureName) \
  OPT_INPUTS void PS##name(structureName input, out half4 oColor: COLOR) \
  { \
    FOut fout = PF##name(input,false); \
    oColor = GetFinalColor(fout); \
  }

//! List of shaders - every pixel shader we want to create various versions for (shadow, no shadow, ...), must be registered here
#if _VBS3
#define PS_LIST(XXC,XXS,XXN,XXD,XXDS) \
  XXC(Detail)                                                                     \
  XXC(DetailSpecularAlpha)                                                        \
  XXC(Normal)                                                                     \
  XXD(Grass)                                                                      \
  XXS(NormalDXTA,                             SNormal)                            \
  XXC(SpecularAlpha)                                                              \
  XXC(SpecularNormalMapGrass)                                                     \
  XXD(SpecularNormalMapThrough)                                                   \
  XXDS(SpecularNormalMapThroughSimple,         SSpecularNormalMapThrough)         \
  XXD(SpecularNormalMapSpecularThrough)                                           \
  XXDS(SpecularNormalMapSpecularThroughSimple, SSpecularNormalMapSpecularThrough) \
  XXDS(NormalMapThroughLowEnd,                 SSpecularNormalMapThrough)         \
  XXC(DetailMacroAS)                                                              \
  XXC(DetailSpecularAlphaMacroAS)                                                 \
  XXC(NormalMap)                                                                  \
  XXC(NormalMapDetailMacroASSpecularMap)                                          \
  XXC(NormalMapDetailMacroASSpecularDIMap)                                        \
  XXC(NormalMapDetailSpecularMap)                                                 \
  XXC(NormalMapDetailSpecularDIMap)                                               \
  XXC(NormalMapMacroAS)                                                           \
  XXC(NormalMapMacroASSpecularMap)                                                \
  XXC(NormalMapMacroASSpecularDIMap)                                              \
  XXC(NormalMapSpecularMap)                                                       \
  XXC(NormalMapSpecularDIMap)                                                     \
  XXC(SpecularNormalMapDiffuse)                                                   \
  XXC(SpecularNormalMapDiffuseMacroAS)                                            \
  XXS(Terrain15,                              STerrain)                           \
  XXS(TerrainSimple15,                        STerrain)                           \
  XXS(TerrainGrass15,                         STerrain)                           \
  XXS(Road,                                   STerrain)                           \
  XXS(Road2Pass,                              STerrain)                           \
  XXN(ShoreFoam,                              SShore)                             \
  XXN(ShoreWet,                               SShore)                             \
  XXC(Glass)                                                                      \
  XXS(Crater1,                                SCrater)                            \
  XXS(Crater2,                                SCrater)                            \
  XXS(Crater3,                                SCrater)                            \
  XXS(Crater4,                                SCrater)                            \
  XXS(Crater5,                                SCrater)                            \
  XXS(Crater6,                                SCrater)                            \
  XXS(Crater7,                                SCrater)                            \
  XXS(Crater8,                                SCrater)                            \
  XXS(Crater9,                                SCrater)                            \
  XXS(Crater10,                               SCrater)                            \
  XXS(Crater11,                               SCrater)                            \
  XXS(Crater12,                               SCrater)                            \
  XXS(Crater13,                               SCrater)                            \
  XXS(Crater14,                               SCrater)                            \
  XXC(Sprite)                                                                     \
  XXS(SpriteSimple,                           SSprite)                            \
  XXC(Super)                                                                      \
  XXC(Multi)                                                                      \
  XXS(TerrainX,                               STerrain)                           \
  XXS(TerrainSimpleX,                         STerrain)                           \
  XXS(TerrainGrassX,                          STerrain)                           \
  XXD(Tree)                                                                       \
  XXD(TreePRT)                                                                    \
  XXDS(TreeSimple,                             STree)                             \

#else

#define PS_LIST(XXC,XXS,XXN,XXD,XXDS) \
  XXC(Detail)                                                                     \
  XXC(DetailSpecularAlpha)                                                        \
  XXC(Normal)                                                                     \
  XXD(Grass)                                                                      \
  XXS(NormalDXTA,                             SNormal)                            \
  XXC(SpecularAlpha)                                                              \
  XXC(SpecularNormalMapGrass)                                                     \
  XXD(SpecularNormalMapThrough)                                                   \
  XXDS(SpecularNormalMapThroughSimple,         SSpecularNormalMapThrough)         \
  XXD(SpecularNormalMapSpecularThrough)                                           \
  XXDS(SpecularNormalMapSpecularThroughSimple, SSpecularNormalMapSpecularThrough) \
  XXDS(NormalMapThroughLowEnd,                 SSpecularNormalMapThrough)         \
  XXC(DetailMacroAS)                                                              \
  XXC(DetailSpecularAlphaMacroAS)                                                 \
  XXC(NormalMap)                                                                  \
  XXC(NormalMapDetailMacroASSpecularMap)                                          \
  XXC(NormalMapDetailMacroASSpecularDIMap)                                        \
  XXC(NormalMapDetailSpecularMap)                                                 \
  XXC(NormalMapDetailSpecularDIMap)                                               \
  XXC(NormalMapMacroAS)                                                           \
  XXC(NormalMapMacroASSpecularMap)                                                \
  XXC(NormalMapMacroASSpecularDIMap)                                              \
  XXC(NormalMapSpecularMap)                                                       \
  XXC(NormalMapSpecularDIMap)                                                     \
  XXC(SpecularNormalMapDiffuse)                                                   \
  XXC(SpecularNormalMapDiffuseMacroAS)                                            \
  XXS(Terrain15,                              STerrain)                           \
  XXS(TerrainSimple15,                        STerrain)                           \
  XXS(TerrainGrass15,                         STerrain)                           \
  XXS(Road,                                   STerrain)                           \
  XXS(Road2Pass,                              STerrain)                           \
  XXN(Shore,                                  SShore)                             \
  XXN(ShoreFoam,                              SShore)                             \
  XXN(ShoreWet,                               SShore)                             \
  XXC(Glass)                                                                      \
  XXS(Crater1,                                SCrater)                            \
  XXS(Crater2,                                SCrater)                            \
  XXS(Crater3,                                SCrater)                            \
  XXS(Crater4,                                SCrater)                            \
  XXS(Crater5,                                SCrater)                            \
  XXS(Crater6,                                SCrater)                            \
  XXS(Crater7,                                SCrater)                            \
  XXS(Crater8,                                SCrater)                            \
  XXS(Crater9,                                SCrater)                            \
  XXS(Crater10,                               SCrater)                            \
  XXS(Crater11,                               SCrater)                            \
  XXS(Crater12,                               SCrater)                            \
  XXS(Crater13,                               SCrater)                            \
  XXS(Crater14,                               SCrater)                            \
  XXC(Sprite)                                                                     \
  XXS(SpriteSimple,                           SSprite)                            \
  XXC(Super)                                                                      \
  XXC(Multi)                                                                      \
  XXS(TerrainX,                               STerrain)                           \
  XXS(TerrainSimpleX,                         STerrain)                           \
  XXS(TerrainGrassX,                          STerrain)                           \
  XXD(Tree)                                                                       \
  XXD(TreePRT)                                                                    \
  XXDS(TreeSimple,                             STree)                             \

#endif

/////////////////////////////////////////////////////////////////////////////////////////////
// P&S - Point and Spot lights

//! Macro to create cX register format
#define REG(regnum) c##regnum

//! Structure to hold one P&S light stuff
struct SLPS
{
  float4 A;
  float4 B;
  float4 C;
  float4 D;
};

#define LPOINT_TransformedPos A
#define LPOINT_Atten          B
#define LPOINT_D              C
#define LPOINT_A              D

#define LSPOT_TransformedPos_CosPhiHalf   A
#define LSPOT_TransformedDir_CosThetaHalf B
#define LSPOT_D_InvCTHMCPH                C
#define LSPOT_A_Atten                     D

//! P&S lights array
SLPS LPSData[MAX_LIGHTS] : register(REG(LIGHTSPACE_START_REGISTER_PS));

float3 PointSpotLighting(in float3 skinnedPosition, in float3 skinnedNormal)
{
  // Color accumulator
  float3 sumColor = float3(0, 0, 0);

  // Saturation value
#if _VBS3_PERPIXELPSLIGHTS
  const float saturationCoef = 3;
#else
  const float saturationCoef = 0.3;
#endif

  // Point lights
  for (int i = 0; i < PSC_PointLoopCount.x; i++)
  {
    float3 pointToLightVector = LPSData[i].LPOINT_TransformedPos.xyz - skinnedPosition.xyz;
    float lightSquareDistance = dot(pointToLightVector, pointToLightVector);
    float invLightDistance = rsqrt(lightSquareDistance);
    float3 lightDirection = pointToLightVector * invLightDistance;
    float attenuation = LPSData[i].LPOINT_Atten.x * invLightDistance * invLightDistance;
    sumColor += (max(0, dot(skinnedNormal, lightDirection)) * LPSData[i].LPOINT_D.xyz + LPSData[i].LPOINT_A.xyz) * min(saturationCoef, attenuation);
  }

  // Spot lights
  for (int ii = 0; ii < PSC_SpotLoopCount.x; ii++)
  {
    int i = MAX_LIGHTS - ii - 1;
    float3 pointToLightVector = LPSData[i].LSPOT_TransformedPos_CosPhiHalf.xyz - skinnedPosition.xyz;
    float lightSquareDistance = dot(pointToLightVector, pointToLightVector);
    float invLightDistance = rsqrt(lightSquareDistance);
    float3 lightDirection = pointToLightVector * invLightDistance;
    float rho = dot(-LPSData[i].LSPOT_TransformedDir_CosThetaHalf.xyz, lightDirection);
    float spotlightFactor = (rho > LPSData[i].LSPOT_TransformedDir_CosThetaHalf.w) ? 1 : (rho <= LPSData[i].LSPOT_TransformedPos_CosPhiHalf.w) ? 0 : (rho - LPSData[i].LSPOT_TransformedPos_CosPhiHalf.w) * LPSData[i].LSPOT_D_InvCTHMCPH.w;
    float attenuation = LPSData[i].LSPOT_A_Atten.w * invLightDistance * invLightDistance;
    sumColor += (max(0, dot(skinnedNormal, lightDirection)) * LPSData[i].LSPOT_D_InvCTHMCPH.xyz + LPSData[i].LSPOT_A_Atten.xyz) * min(saturationCoef, attenuation * spotlightFactor);
  }

  // Return the ambient light
  return sumColor;
}

/////////////////////////////////////////////////////////////////////////////////////////////
// Pixel shaders

FOut PFDetail(SDetail input, bool sbShadows, bool thermal)
{
  FOut fout = InitFOut();

  // Calculate the lights
  fout.light = GetLight(
    input.ambientColor,
    half3(0, 0, 0),
    input.x_x_nDotU_nDotL);

  // Calculate the color
  fout.color = GetColorDiffuseDetail(
    tex2D0(input.tDiffuseMap_DetailMap.xy),
    tex2D(sampler1, input.tDiffuseMap_DetailMap.wzyx),
    input.ambientColor.a, 0);

  // Calculate the fog
  fout.fog = input.tPSFog.w;

  // Calculate the land shadow
  fout.landShadow = input.landShadow.a;

  // Remember the nDotL
  fout.nDotL = input.x_x_nDotU_nDotL.w;

  return fout;
}

//////////////////////////////////////////////

FOut PFDetailSpecularAlpha(SDetailSpecularAlpha input, bool sbShadows, bool thermal)
{
  FOut fout = InitFOut();

  // Calculate the lights
  fout.light = GetLight(
    input.ambientColor,
    input.specularColor_landShadow.rgb,
    input.x_x_nDotU_nDotL);

  // Calculate the color
  half3 s = input.specularColor_landShadow.rgb;
  half specIntensity = dot(s, s) * 2.0f;
  fout.color = GetColorDiffuseDetail(
    tex2D0(input.tDiffuseMap_DetailMap.xy),
    tex2D(sampler1, input.tDiffuseMap_DetailMap.wzyx),
    input.ambientColor.a, specIntensity);

  // Calculate the fog
  fout.fog = input.tPSFog.w;

  // Calculate the land shadow
  fout.landShadow = input.specularColor_landShadow.a;

  // Remember the nDotL
  fout.nDotL = input.x_x_nDotU_nDotL.w;

  return fout;
}

//////////////////////////////////////////////

FOut PFNormal(SNormal input, bool sbShadows, bool thermal)
{
  FOut fout = InitFOut();

  // Calculate the lights
  fout.light = GetLight(
    input.ambientColor,
    half3(0, 0, 0),
    input.x_x_nDotU_nDotL);

  // Calculate the color
  fout.color = GetColorDiffuse(
    tex2D0(input.tDiffuseMap.xy),
    input.ambientColor.a, 0);

  // Calculate the fog
  fout.fog = input.tPSFog.w;

  // Calculate the land shadow
  fout.landShadow = input.landShadow.a;

  // Remember the nDotL
  fout.nDotL = input.x_x_nDotU_nDotL.w;

  return fout;
}

//////////////////////////////////////////////

//#define EARLY_OUT

#ifdef EARLY_OUT
const static bool EarlyOut = true;
const static bool RejectAll = false;
  #define FORCEBRANCH [branch]
#else
const static bool EarlyOut = false;
const static bool RejectAll = false;
  #define FORCEBRANCH
#endif

void PFGrass(
  SGrass input, bool sbShadows, float4 tShadowBuffer, float2 vPos,
  int finalColorType, out half4 oColor
)
{
  FOut fout = InitFOut();

  // Calculate the lights
  // Note: for ground clutter we are not using lighting based on normal at all
  fout.light = GetLight(
    input.ambientColor,
    half3(0, 0, 0),
    input.x_x_nDotU_nDotL);

  // Calculate the color
  fout.color = GetColorDiffuse(
    tex2D0(input.tDiffuseMap.xy),
    input.ambientColor.a, 0);
    
  fout.color.rgb *= input.lightingColor.rgb;

  FORCEBRANCH if (!EarlyOut || input.ambientColor.a > 0 && !RejectAll)
  {
    // perform alpha discretization
#ifdef _XBOX
    // Label: AlphaToCoverage
    // note: alpha to coverage mask may be slightly inaccurate, as it will contain shadow intensity as well
#else
    fout.color.a = fout.color.a>0.5;
#endif

    // Calculate the shadow intensity

    //if (sbShadows) Optimization in else branch (indirect light set to 0 causes the shader to have less instructions) is not correct
    {
      // Calculate the land shadow
      fout.landShadow = input.landShadow.a;
    }
//     else
//     {
//       // we can provide result value directly - separate direct/indirect not needed
//       // set up color coefficients so that Final functions use the color we have already calculated
//       fout.light.indirect = 0;
//       fout.light.direct *= input.lightingColor.w;
//       fout.landShadow = 1;
//     }
    fout.fog = input.tPSFog.w;

    // Remember the nDotL
    fout.nDotL = input.x_x_nDotU_nDotL.w;

    CallFinalColor(fout,tShadowBuffer,vPos,finalColorType,oColor);
  }
  else
  {
    oColor = GetFinalColorEmpty();
  }
}

//////////////////////////////////////////////

FOut PFNormalDXTA(SNormal input, bool sbShadows, bool thermal)
{
  FOut fout = InitFOut();

  // Calculate the lights
  fout.light = GetLight(
    input.ambientColor,
    half3(0, 0, 0),
    input.x_x_nDotU_nDotL);

  // Calculate the color
  fout.color = GetColorDiffuseDXTADiscrete(tex2D0(input.tDiffuseMap.xy), input.ambientColor.a);

  // Calculate the fog
  fout.fog = input.tPSFog.w;

  // Calculate the land shadow
  fout.landShadow = input.landShadow.a;

  // Remember the nDotL
  fout.nDotL = input.x_x_nDotU_nDotL.w;

  return fout;
}

//////////////////////////////////////////////

FOut PFSpecularAlpha(SSpecularAlpha input, bool sbShadows, bool thermal)
{
  FOut fout = InitFOut();

  // Calculate the lights
  fout.light = GetLight(
    input.ambientColor,
    input.specularColor_landShadow.rgb,
    input.x_x_nDotU_nDotL);

  // Calculate the color
  half3 s = input.specularColor_landShadow.rgb;
  half specIntensity = dot(s, s) * 2.0f;
  fout.color = GetColorDiffuse(
    tex2D0(input.tDiffuseMap.xy),
    input.ambientColor.a, specIntensity) /** half4(0, 1, 0, 1)*/;

  // Calculate the fog
  fout.fog = input.tPSFog.w;

  // Calculate the land shadow
  fout.landShadow = input.specularColor_landShadow.a;

  // Remember the nDotL
  fout.nDotL = input.x_x_nDotU_nDotL.w;

  return fout;
}

//////////////////////////////////////////////

FOut PFSpecularNormalMapGrass(SSpecularNormalMapGrass input, bool sbShadows, bool thermal)
{
  FOut fout = InitFOut();

  // Calculate the lights
  {
    // Indirect light
    fout.light.indirect = input.ambientColor.rgb;

    // Calculate the direct light
    half4 normalMap = tex2D(sampler1, input.tDiffuseMap_NormalMap.wzyx);
    half frontCoef = max(dot(normalMap.rgb * 2 - 1, input.lightLocal_landShadow.rgb), 0);
    half backCoef = max(dot(-(normalMap.rgb * 2 - 1), input.lightLocal_landShadow.rgb), 0);
    fout.light.direct = frontCoef * PSC_Diffuse.rgb + backCoef * PSC_DiffuseBack.rgb;

    // No specular light
    fout.light.specular = 0;
    fout.light.specularEnv = 0;
    
    fout.light.diffuseShadowCoef = 1;
  }

  // Calculate the color
  fout.color = GetColorDiffuse(
    tex2D0(input.tDiffuseMap_NormalMap.xy),
    input.ambientColor.a, 0);

  // Calculate the fog
  fout.fog = input.tPSFog.w;

  // Calculate the land shadow
  fout.landShadow = input.lightLocal_landShadow.a;

  // Remember the nDotL
  fout.nDotL = 1; // Grass is a special object - it actually doesn't matter where the sun is shining from

  // We know we don't want blending for grass
  fout.psBlend = false;

  return fout;
}

//////////////////////////////////////////////

void PFSpecularNormalMapThrough(
  SSpecularNormalMapThrough input, bool sbShadows, float4 tShadowBuffer, float2 vPos,
  int finalColorType, out half4 oColor
)
{
  FOut fout = InitFOut();
  
  half4 primTex = tex2D0(input.tDiffuseMap_NormalMap.xy);
  half4 ambientAtten = tex2D(sampler3, input.tDiffuseAtten_AmbientAtten.wzyx);
  half4 normalMap = tex2D(sampler1, input.tDiffuseMap_NormalMap.wzyx);
  half diffuseAtten = tex2D(sampler2, input.tDiffuseAtten_AmbientAtten.xy).a;

  FORCEBRANCH if (!EarlyOut || primTex.a>0 && !RejectAll)
  {
    // Calculate the lights and diffuse attenuation
    half da;
    {
      // perform ambient attenuation
      fout.light.indirect = ambientAtten.rgb * input.ambientColor.rgb + GetIndirectLight(dot(normalMap.rgb * 2 - 1, input.tUpLocal.rgb));

      // perform diffuse attenuation via rescaling diffuse result
      half frontCoef = max(dot(normalMap.rgb * 2 - 1, input.lightLocal_landShadow.rgb), 0);
      half backCoef = max(dot(-(normalMap.rgb * 2 - 1), input.lightLocal_landShadow.rgb), 0);
      half lightCoef = backCoef * PSC_DiffuseBack.a + frontCoef;
      fout.light.direct = diffuseAtten * lightCoef * PSC_Diffuse.rgb + PSC_DForced;
      da = diffuseAtten * frontCoef;

      // No specular light
      fout.light.specular = 0;
      fout.light.specularEnv = 0;
      
      fout.light.diffuseShadowCoef = 1;
    }

    // Calculate the color
    fout.color = GetColorDiffuseDiscreteNoise(
      primTex,
      input.ambientColor.a,
      normalMap.a
      );

    // Calculate the fog
    fout.fog = input.tPSFog.w;

    // Remember the nDotL
    fout.nDotL = da;

    // We know we don't want blending for trees
    fout.psBlend = false;

    // Calculate the land shadow
    fout.landShadow = input.lightLocal_landShadow.a;
    CallFinalColor(fout,tShadowBuffer,vPos,finalColorType,oColor);
  }
  else
  {
    oColor = GetFinalColorEmpty();
  }
}

//////////////////////////////////////////////

void PFSpecularNormalMapThroughSimple(
  SSpecularNormalMapThrough input, bool sbShadows, float4 tShadowBuffer, float2 vPos,
  int finalColorType, out half4 oColor
)
{
  FOut fout = InitFOut();

  half4 primTex = tex2D0(input.tDiffuseMap_NormalMap.xy);
  half4 normalMap = tex2D(sampler1, input.tDiffuseMap_NormalMap.wzyx);
  FORCEBRANCH if (!EarlyOut || primTex.a>0 && !RejectAll)
  {
    // Calculate the lights and diffuse attenuation
    half da;
    {
      // Ambient light
      fout.light.indirect = input.ambientColor.rgb + GetIndirectLight(dot(normalMap.rgb * 2 - 1, input.tUpLocal.rgb));

      // perform diffuse attenuation via rescaling diffuse result
      half frontCoef = max(dot(normalMap.rgb * 2 - 1, input.lightLocal_landShadow.rgb), 0);
      half backCoef = max(dot(-(normalMap.rgb * 2 - 1), input.lightLocal_landShadow.rgb), 0);
      half lightCoef = backCoef * PSC_DiffuseBack.a + frontCoef;
      fout.light.direct = lightCoef * PSC_Diffuse.rgb + PSC_DForced;
      da = frontCoef;

      // No specular light
      fout.light.specular = 0;
      fout.light.specularEnv = 0;
      
      fout.light.diffuseShadowCoef = 1;
    }

    // Calculate the color
    fout.color = GetColorDiffuseDiscreteNoise(
      primTex,
      input.ambientColor.a,
      normalMap.a
      );

    // Calculate the fog
    fout.fog = input.tPSFog.w;

    // Remember the nDotL
    fout.nDotL = da;

    // We know we don't want blending for trees
    fout.psBlend = false;

    // Calculate the land shadow
    fout.landShadow = input.lightLocal_landShadow.a;
    CallFinalColor(fout,tShadowBuffer,vPos,finalColorType,oColor);
  }
  else
  {
    oColor = GetFinalColorEmpty();
  }
}

void PFSpecularNormalMapSpecularThrough(
  SSpecularNormalMapSpecularThrough input, bool sbShadows, float4 tShadowBuffer, float2 vPos,
  int finalColorType, out half4 oColor
)
{
  FOut fout = InitFOut();

  half4 primTex = tex2D0(input.tDiffuseMap_NormalMap.xy);
  half4 ambientAtten = tex2D(sampler3, input.tDiffuseAtten_AmbientAtten.wzyx);
  half4 normalMap = tex2D(sampler1, input.tDiffuseMap_NormalMap.wzyx);
  half diffuseAtten = tex2D(sampler2, input.tDiffuseAtten_AmbientAtten.xy).a;
  FORCEBRANCH if (!EarlyOut || primTex.a>0 && !RejectAll)
  {
    // Calculate the lights and diffuse attenuation
    half da;
    {
      half3 normal = normalMap.rgb * 2 - 1;

      // perform ambient attenuation
      
      fout.light.indirect = ambientAtten.rgb * input.ambientColor.rgb + GetIndirectLight(dot(normal, input.tUpLocal.rgb));

      // perform diffuse attenuation via rescaling diffuse result
      half frontBack = dot(normal, input.lightLocal_landShadow.rgb);
      half frontCoef = max(frontBack, 0);
      half backCoef = max(-frontBack, 0);
      half lightCoef = backCoef * PSC_DiffuseBack.a + frontCoef;
      fout.light.direct = diffuseAtten * lightCoef * PSC_Diffuse.rgb + PSC_DForced;
      da = diffuseAtten * lightCoef;

      // Calculate specular light
      half4 litVector = lit(frontBack, dot(normal, input.tHalfway), PSC_Specular.a);
      half coefSpecular = litVector.z;
      coefSpecular = min(coefSpecular, 1); // Specular power can be very high and this can probably lead to infinity (if the coefSpecular happen to be just slightly above 1)
      fout.light.specular = PSC_Specular * coefSpecular * diffuseAtten;
      fout.light.specularEnv = 0;

      fout.light.diffuseShadowCoef = 1;
    }

    // Calculate the color
    fout.color = GetColorDiffuseDiscreteNoise(
      primTex,
      input.ambientColor.a,
      normalMap.a
      );

    // Calculate the fog
    fout.fog = input.tPSFog.w;

    // Remember the nDotL
    fout.nDotL = da;

    // We know we don't want blending for trees
    fout.psBlend = false;

    // Calculate the land shadow
    fout.landShadow = input.lightLocal_landShadow.a;
    CallFinalColor(fout,tShadowBuffer,vPos,finalColorType,oColor);
  }
  else
  {
    oColor = GetFinalColorEmpty();
  }
}

void PFSpecularNormalMapSpecularThroughSimple(
  SSpecularNormalMapSpecularThrough input, bool sbShadows, float4 tShadowBuffer, float2 vPos,
  int finalColorType, out half4 oColor
)
{
  FOut fout = InitFOut();

  half4 primTex = tex2D0(input.tDiffuseMap_NormalMap.xy);
  half4 normalMap = tex2D(sampler1, input.tDiffuseMap_NormalMap.wzyx);
  FORCEBRANCH if (!EarlyOut || primTex.a>0 && !RejectAll)
  {
    // Calculate the lights and diffuse attenuation
    half da;
    {
      half3 normal = normalMap.rgb * 2 - 1;

      // Ambient light
      fout.light.indirect = input.ambientColor.rgb + GetIndirectLight(dot(normal, input.tUpLocal.rgb));

      // perform diffuse attenuation via rescaling diffuse result
      half frontBack = dot(normal, input.lightLocal_landShadow.rgb);
      half frontCoef = max(frontBack, 0);
      half backCoef = max(-frontBack, 0);
      half lightCoef = backCoef * PSC_DiffuseBack.a + frontCoef;
      fout.light.direct = lightCoef * PSC_Diffuse.rgb + PSC_DForced;
      da = lightCoef;

      // Calculate specular light
      half4 litVector = lit(frontBack, dot(normal, input.tHalfway), PSC_Specular.a);
      half coefSpecular = litVector.z;
      coefSpecular = min(coefSpecular, 1); // Specular power can be very high and this can probably lead to infinity (if the coefSpecular happen to be just slightly above 1)
      fout.light.specular = PSC_Specular * coefSpecular;
      fout.light.specularEnv = 0;

      fout.light.diffuseShadowCoef = 1;
    }

    // Calculate the color
    fout.color = GetColorDiffuseDiscreteNoise(
      primTex,
      input.ambientColor.a,
      normalMap.a
      );

    // Calculate the fog
    fout.fog = input.tPSFog.w;

    // Remember the nDotL
    fout.nDotL = da;

    // We know we don't want blending for trees
    fout.psBlend = false;

    // Calculate the land shadow
    fout.landShadow = input.lightLocal_landShadow.a;
    CallFinalColor(fout,tShadowBuffer,vPos,finalColorType,oColor);
  }
  else
  {
    oColor = GetFinalColorEmpty();
  }
}

void PFNormalMapThroughLowEnd(
  SSpecularNormalMapThrough input, bool sbShadows, float4 tShadowBuffer, float2 vPos,
  int finalColorType, out half4 oColor
)
{
  FOut fout = InitFOut();

  half4 primTex = tex2D0(input.tDiffuseMap_NormalMap.xy);
  FORCEBRANCH if (!EarlyOut || primTex.a>0 && !RejectAll)
  {

    // Calculate the lights and diffuse attenuation
    half da;
    {
      half3 normal = half3(0, 0, 1);

      // Ambient light
      fout.light.indirect = input.ambientColor.rgb + GetIndirectLight(dot(normal, input.tUpLocal.rgb));

      // perform diffuse attenuation via rescaling diffuse result
      half frontBack = dot(normal, input.lightLocal_landShadow.rgb);
      half frontCoef = max(frontBack, 0);
      half backCoef = max(-frontBack, 0);
      half lightCoef = backCoef * PSC_DiffuseBack.a + frontCoef;
      fout.light.direct = lightCoef * PSC_Diffuse.rgb + PSC_DForced;
      da = lightCoef;

      // Calculate specular light
      fout.light.specular = 0;
      fout.light.specularEnv = 0;

      fout.light.diffuseShadowCoef = 1;
    }

    // Calculate the color
    fout.color = GetColorDiffuseDiscreteNoise(
      primTex,
      input.ambientColor.a,
      0.5f);

    // Calculate the fog
    fout.fog = input.tPSFog.w;

    // Remember the nDotL
    fout.nDotL = da;

    // We know we don't want blending for trees
    fout.psBlend = false;

    // Calculate the land shadow
    fout.landShadow = input.lightLocal_landShadow.a;
    CallFinalColor(fout,tShadowBuffer,vPos,finalColorType,oColor);
  }
  else
  {
    oColor = GetFinalColorEmpty();
  }
}

void PFTreeBody(STree input, bool sbShadows, float4 tShadowBuffer, float2 vPos, int finalColorType, out half4 oColor, bool simple)
{
  FOut fout = InitFOut();

  half4 primTex = tex2D0(input.tDiffuseMap_NormalMap.xy);
  half4 normalMap = tex2D(sampler1, input.tDiffuseMap_NormalMap.wzyx);
  half4 macroMap = tex2D(sampler2, input.tShadowMap.xy);
  half ambientAtten = macroMap.a;
  half diffuseAtten = lerp(1, macroMap.a, PSC_Diffuse.a);
  
  if (simple) ambientAtten = 1, diffuseAtten = 1;
  FORCEBRANCH if (!EarlyOut || primTex.a>0 && !RejectAll)
  {
    // Calculate the lights and diffuse attenuation
    half da;
    {
      half3 normal = normalMap.rgb * 2 - 1;

      // perform ambient attenuation

      fout.light.indirect = ambientAtten * input.ambientColor.rgb + GetIndirectLight(dot(normal, input.tUpLocal.rgb));

      // perform diffuse attenuation via rescaling diffuse result
      half frontBack = dot(normal, input.lightLocal_landShadow.rgb);
      half frontCoef = max(frontBack, 0);
      half backCoef = max(-frontBack, 0);
      half lightCoef = backCoef * PSC_DiffuseBack.a + frontCoef;
      fout.light.direct = diffuseAtten * lightCoef * PSC_Diffuse.rgb + PSC_DForced;
      da = diffuseAtten * lightCoef;

      // Calculate specular light
      half4 litVector = lit(frontBack, dot(normal, input.tHalfway), PSC_Specular.a);
      half coefSpecular = litVector.z;
      coefSpecular = min(coefSpecular, 1); // Specular power can be very high and this can probably lead to infinity (if the coefSpecular happen to be just slightly above 1)
      fout.light.specular = PSC_Specular * coefSpecular * diffuseAtten;
      fout.light.specularEnv = 0;
      
      if (simple) fout.light.specular = 0;

      fout.light.diffuseShadowCoef = 1;
    }

    // Calculate the color
    fout.color = GetColorDiffuseDiscreteNoise(
      primTex * half4(macroMap.rgb, 1),
      input.ambientColor.a,
      normalMap.a
      );

    // Calculate the fog
    fout.fog = input.tPSFog.w;

    // Remember the nDotL
    fout.nDotL = da;

    // We know we don't want blending for trees
    fout.psBlend = false;

    // Calculate the land shadow
    fout.landShadow = input.lightLocal_landShadow.a;
    CallFinalColor(fout,tShadowBuffer,vPos,finalColorType,oColor);
  }
  else
  {
    oColor = GetFinalColorEmpty();
  }
}

void PFTree(STree input, bool sbShadows, float4 tShadowBuffer, float2 vPos, int finalColorType, out half4 oColor)
{
  PFTreeBody(input,sbShadows,tShadowBuffer,vPos,finalColorType,oColor,false);
}

void PFTreeSimple(STree input, bool sbShadows, float4 tShadowBuffer, float2 vPos, int finalColorType, out half4 oColor)
{
  PFTreeBody(input,sbShadows,tShadowBuffer,vPos,finalColorType,oColor,true);
}

void PFTreePRT(STreePRT input, bool sbShadows, float4 tShadowBuffer, float2 vPos, int finalColorType, out half4 oColor)
{
  FOut fout = InitFOut();

#if _SM2
  oColor = (1, 1, 0, 1); // Purple color will show the shaders are not implemented
#else
  half4 primTex = tex2D0(input.tDiffuseMap_prtMap.xy);
  half4 prtMap = tex2D(sampler1, input.tDiffuseMap_prtMap.wzyx + input.tPRTOffsfet.xy);
  half4 macroMap = tex2D(sampler2, input.tMacroMap.xy + input.tPRTOffsfet.xy);
  FORCEBRANCH if (!EarlyOut || primTex.a>0 && !RejectAll)
  {
    // Calculate direct light
    float4 vAccumDirectR = prtMap * PSC_PRTConstantDirect[0];
    float4 vAccumDirectG = prtMap * PSC_PRTConstantDirect[1];
    float4 vAccumDirectB = prtMap * PSC_PRTConstantDirect[2];
    float3 vLightDirect;
    vLightDirect.r = dot(vAccumDirectR,1);
    vLightDirect.g = dot(vAccumDirectG,1);
    vLightDirect.b = dot(vAccumDirectB,1);

    // Calculate indirect light
    float4 vAccumIndirectR = prtMap * PSC_PRTConstantIndirect[0];
    float4 vAccumIndirectG = prtMap * PSC_PRTConstantIndirect[1];
    float4 vAccumIndirectB = prtMap * PSC_PRTConstantIndirect[2];
    float3 vLightIndirect;
    vLightIndirect.r = dot(vAccumIndirectR,1);
    vLightIndirect.g = dot(vAccumIndirectG,1);
    vLightIndirect.b = dot(vAccumIndirectB,1);

    // Calculate the lights and diffuse attenuation
    fout.light.indirect = vLightIndirect + input.ambientColor;
    fout.light.direct = vLightDirect;
    fout.light.specular = 0;
    fout.light.specularEnv = 0;
    fout.light.diffuseShadowCoef = 1;

    // Calculate the color
    fout.color = GetColorDiffuseDiscreteNoise(
      primTex * half4(macroMap.rgb, 1),
      input.ambientColor.a,
      0);

    // Calculate the fog
    fout.fog = input.tPSFog.w;

    // Remember the nDotL
    fout.nDotL = 1; // TreePRT is a special object - it actually doesn't matter where the sun is shining from

    // We know we don't want blending for trees
    fout.psBlend = false;

    // Calculate the land shadow
    fout.landShadow = input.lightLocal_landShadow.a;
    CallFinalColor(fout,tShadowBuffer,vPos,finalColorType,oColor);
  }
  else
  {
    oColor = GetFinalColorEmpty();
  }
#endif
}

//////////////////////////////////////////////

FOut PFDetailMacroAS(SDetailMacroAS input, bool sbShadows, bool thermal)
{
  FOut fout = InitFOut();

  // Calculate the lights
  fout.light = GetLightAS(
    input.ambientColor,
    half3(0, 0, 0),
    input.x_x_nDotU_nDotL,
    tex2D(sampler3, input.tMacroMap_ShadowMap.wzyx));

  // Calculate the color
  fout.color = GetColorDiffuseDetailMacroAS(
    tex2D0(input.tDiffuseMap_DetailMap.xy),
    tex2D(sampler1, input.tDiffuseMap_DetailMap.wzyx),
    tex2D(sampler2, input.tMacroMap_ShadowMap.xy),
    input.ambientColor.a, 0);

  // Calculate the fog
  fout.fog = input.tPSFog.w;

  // Calculate the land shadow
  fout.landShadow = input.landShadow.a;

  // Remember the nDotL
  fout.nDotL = input.x_x_nDotU_nDotL.w;

  return fout;
}

//////////////////////////////////////////////

FOut PFDetailSpecularAlphaMacroAS(SDetailSpecularAlphaMacroAS input, bool sbShadows, bool thermal)
{
  FOut fout = InitFOut();

  // Calculate the lights
  fout.light = GetLightAS(
    input.ambientColor,
    input.specularColor_landShadow.rgb,
    input.x_x_nDotU_nDotL,
    tex2D(sampler3, input.tMacroMap_ShadowMap.wzyx));

  /*
  Not transcribed the handling of alpha from the original shader:
  mov_x2 r1, v1
  dp3_x2 r1, r1, r1
  */

  // Calculate the color
  fout.color = GetColorDiffuseDetailMacroAS(
    tex2D0(input.tDiffuseMap_DetailMap.xy),
    tex2D(sampler1, input.tDiffuseMap_DetailMap.wzyx),
    tex2D(sampler2, input.tMacroMap_ShadowMap.xy),
    input.ambientColor.a, 0);

  // Calculate the fog
  fout.fog = input.tPSFog.w;

  // Calculate the land shadow
  fout.landShadow = input.specularColor_landShadow.a;

  // Remember the nDotL
  fout.nDotL = input.x_x_nDotU_nDotL.w;

  return fout;
}

//////////////////////////////////////////////

FOut PFNormalMap(SNormalMap input, bool sbShadows, bool thermal)
{
  FOut fout = InitFOut();

  // Get the irradiance data
  half4 normal = tex2D(sampler1, input.tDiffuseMap_NormalMap.wzyx);
  half nDotL;
  half nDotU;
  half4 irradiance = GetIrradianceData(input.tLightLocal.xyz, input.tUpLocal.xyz, input.tHalfway, normal, sampler3, nDotL, nDotU);

  // Calculate the lights
  fout.light = GetLightIrradiance(input.ambientColor, irradiance, normal.a, nDotL, nDotU);

  // Calculate the color
  fout.color = GetColorDiffuse(
    tex2D0(input.tDiffuseMap_NormalMap.xy),
    input.ambientColor.a, 0);

  // Calculate the fog
  fout.fog = input.tPSFog.w;

  // Calculate the land shadow
  fout.landShadow = input.landShadow.a;

  // Remember the nDotL
  fout.nDotL = nDotL;

  return fout;
}

//////////////////////////////////////////////

FOut PFNormalMapDetailMacroASSpecularMap(SNormalMapDetailMacroASSpecularMap input, bool sbShadows, bool thermal)
{
  FOut fout = InitFOut();

  // Calculate the lights
  half coefDiffuse;
  fout.light = GetLightNormalSpecularAS(input.ambientColor,
    tex2D(sampler1, input.tDiffuseMap_NormalMap.wzyx), input.tLightLocal.xyz, input.tHalfway, input.tUpLocal.xyz,
    tex2D(sampler5, input.tShadowMap_SpecularMap.wzyx),
    tex2D(sampler4, input.tShadowMap_SpecularMap.xy),
    coefDiffuse);

  // Calculate the color
  fout.color = GetColorDiffuseDetailMacroAS(
    tex2D0(input.tDiffuseMap_NormalMap.xy),
    tex2D(sampler2, input.tDetailMap_MacroMap.xy),
    tex2D(sampler3, input.tDetailMap_MacroMap.wzyx),
    input.ambientColor.a, 0);

  // Calculate the fog
  fout.fog = input.tPSFog.w;

  // Calculate the land shadow
  fout.landShadow = input.landShadow.a;

  // Remember the nDotL
  fout.nDotL = coefDiffuse;

  return fout;
}

//////////////////////////////////////////////

FOut PFNormalMapDetailMacroASSpecularDIMap(SNormalMapDetailMacroASSpecularDIMap input, bool sbShadows, bool thermal)
{
  FOut fout = InitFOut();

  // Calculate the lights
  half coefDiffuse;
  fout.light = GetLightNormalSpecularDIAS(input.ambientColor,
    tex2D(sampler1, input.tDiffuseMap_NormalMap.wzyx), input.tLightLocal.xyz, input.tHalfway, input.tUpLocal.xyz,
    tex2D(sampler5, input.tShadowMap_SpecularMap.wzyx),
    tex2D(sampler4, input.tShadowMap_SpecularMap.xy),
    coefDiffuse);

  // Calculate the color
  fout.color = GetColorDiffuseDetailMacroAS(
    tex2D0(input.tDiffuseMap_NormalMap.xy),
    tex2D(sampler2, input.tDetailMap_MacroMap.xy),
    tex2D(sampler3, input.tDetailMap_MacroMap.wzyx),
    input.ambientColor.a, 0);

  // Calculate the fog
  fout.fog = input.tPSFog.w;

  // Calculate the land shadow
  fout.landShadow = input.landShadow.a;

  // Remember the nDotL
  fout.nDotL = coefDiffuse;

  return fout;
}

//////////////////////////////////////////////

FOut PFNormalMapDetailSpecularMap(SNormalMapDetailSpecularMap input, bool sbShadows, bool thermal)
{
  FOut fout = InitFOut();

  // Calculate the lights
  half coefDiffuse;
  fout.light = GetLightNormalSpecular(input.ambientColor,
    tex2D(sampler1, input.tDiffuseMap_NormalMap.wzyx), input.tLightLocal.xyz, input.tHalfway, input.tUpLocal.xyz,
    tex2D(sampler3, input.tDetailMap_SpecularMap.wzyx),
    coefDiffuse);

  // Calculate the color
  fout.color = GetColorDiffuseDetail(
    tex2D0(input.tDiffuseMap_NormalMap.xy),
    tex2D(sampler2, input.tDetailMap_SpecularMap.xy),
    input.ambientColor.a, 0);

  // Calculate the fog
  fout.fog = input.tPSFog.w;

  // Calculate the land shadow
  fout.landShadow = input.landShadow.a;

  // Remember the nDotL
  fout.nDotL = coefDiffuse;

  return fout;
}

//////////////////////////////////////////////

FOut PFNormalMapDetailSpecularDIMap(SNormalMapDetailSpecularDIMap input, bool sbShadows, bool thermal)
{
  FOut fout = InitFOut();

  // Calculate the lights
  half coefDiffuse;
  fout.light = GetLightNormalSpecularDI(input.ambientColor,
    tex2D(sampler1, input.tDiffuseMap_NormalMap.wzyx), input.tLightLocal.xyz, input.tHalfway, input.tUpLocal.xyz,
    tex2D(sampler3, input.tDetailMap_SpecularMap.wzyx),
    coefDiffuse);

  // Calculate the color
  fout.color = GetColorDiffuseDetail(
    tex2D0(input.tDiffuseMap_NormalMap.xy),
    tex2D(sampler2, input.tDetailMap_SpecularMap.xy),
    input.ambientColor.a, 0);

  // Calculate the fog
  fout.fog = input.tPSFog.w;

  // Calculate the land shadow
  fout.landShadow = input.landShadow.a;

  // Remember the nDotL
  fout.nDotL = coefDiffuse;

  return fout;
}

//////////////////////////////////////////////

FOut PFNormalMapMacroAS(SNormalMapMacroAS input, bool sbShadows, bool thermal)
{
  FOut fout = InitFOut();

  // Get the irradiance data
  half4 normal = tex2D(sampler1, input.tDiffuseMap_NormalMap.wzyx);
  half nDotL;
  half nDotU;
  half4 irradiance = GetIrradianceData(input.tLightLocal.xyz, input.tUpLocal.xyz, input.tHalfway, normal, sampler3, nDotL, nDotU);

  // Calculate the lights
  fout.light = GetLightIrradiance(input.ambientColor, irradiance, normal.a, nDotL, nDotU);

  // Calculate the color
  fout.color = GetColorDiffuseMacroAS(
    tex2D0(input.tDiffuseMap_NormalMap.xy),
    tex2D(sampler2, input.tMacroMap.xy),
    input.ambientColor.a, 0);

  // Calculate the fog
  fout.fog = input.tPSFog.w;

  // Calculate the land shadow
  fout.landShadow = input.landShadow.a;

  // Remember the nDotL
  fout.nDotL = nDotL;

  return fout;
}

//////////////////////////////////////////////

FOut PFNormalMapMacroASSpecularMap(SNormalMapMacroASSpecularMap input, bool sbShadows, bool thermal)
{
  FOut fout = InitFOut();

  // Calculate the lights
  half coefDiffuse;
  fout.light = GetLightNormalSpecularAS(input.ambientColor,
    tex2D(sampler1, input.tDiffuseMap_NormalMap.wzyx), input.tLightLocal.xyz, input.tHalfway, input.tUpLocal.xyz,
    tex2D(sampler4, input.tSpecularMap.xy),
    tex2D(sampler3, input.tMacroMap_ShadowMap.wzyx),
    coefDiffuse);

  // Calculate the color
  fout.color = GetColorDiffuseMacroAS(
    tex2D0(input.tDiffuseMap_NormalMap.xy),
    tex2D(sampler2, input.tMacroMap_ShadowMap.xy),
    input.ambientColor.a, 0);

  // Calculate the fog
  fout.fog = input.tPSFog.w;

  // Calculate the land shadow
  fout.landShadow = input.landShadow.a;

  // Remember the nDotL
  fout.nDotL = coefDiffuse;

  return fout;
}

//////////////////////////////////////////////

FOut PFNormalMapMacroASSpecularDIMap(SNormalMapMacroASSpecularDIMap input, bool sbShadows, bool thermal)
{
  FOut fout = InitFOut();

  // Calculate the lights
  half coefDiffuse;
  fout.light = GetLightNormalSpecularDIAS(input.ambientColor,
    tex2D(sampler1, input.tDiffuseMap_NormalMap.wzyx), input.tLightLocal.xyz, input.tHalfway, input.tUpLocal.xyz,
    tex2D(sampler4, input.tSpecularMap.xy),
    tex2D(sampler3, input.tMacroMap_ShadowMap.wzyx),
    coefDiffuse);

  // Calculate the color
  fout.color = GetColorDiffuseMacroAS(
    tex2D0(input.tDiffuseMap_NormalMap.xy),
    tex2D(sampler2, input.tMacroMap_ShadowMap.xy),
    input.ambientColor.a, 0);

  // Calculate the fog
  fout.fog = input.tPSFog.w;

  // Calculate the land shadow
  fout.landShadow = input.landShadow.a;

  // Remember the nDotL
  fout.nDotL = coefDiffuse;

  return fout;
}

//////////////////////////////////////////////

FOut PFNormalMapSpecularMap(SNormalMapSpecularMap input, bool sbShadows, bool thermal)
{
  FOut fout = InitFOut();

  // Calculate the lights
  half coefDiffuse;
  fout.light = GetLightNormalSpecular(input.ambientColor,
    tex2D(sampler1, input.tDiffuseMap_NormalMap.wzyx), input.tLightLocal.xyz, input.tHalfway, input.tUpLocal.xyz,
    tex2D(sampler2, input.tSpecularMap.xy),
    coefDiffuse);

  // Calculate the color
  fout.color = GetColorDiffuse(
    tex2D0(input.tDiffuseMap_NormalMap.xy),
    input.ambientColor.a, 0);

  // Calculate the fog
  fout.fog = input.tPSFog.w;

  // Calculate the land shadow
  fout.landShadow = input.landShadow.a;

  // Remember the nDotL
  fout.nDotL = coefDiffuse;

  return fout;
}

//////////////////////////////////////////////

FOut PFNormalMapSpecularDIMap(SNormalMapSpecularDIMap input, bool sbShadows, bool thermal)
{
  FOut fout = InitFOut();

  // Calculate the lights
  half coefDiffuse;
  fout.light = GetLightNormalSpecularDI(input.ambientColor,
    tex2D(sampler1, input.tDiffuseMap_NormalMap.wzyx), input.tLightLocal.xyz, input.tHalfway, input.tUpLocal.xyz,
    tex2D(sampler2, input.tSpecularMap.xy),
    coefDiffuse);

  // Calculate the color
  fout.color = GetColorDiffuse(
    tex2D0(input.tDiffuseMap_NormalMap.xy),
    input.ambientColor.a, 0);

  // Calculate the fog
  fout.fog = input.tPSFog.w;

  // Calculate the land shadow
  fout.landShadow = input.landShadow.a;

  // Remember the nDotL
  fout.nDotL = coefDiffuse;

  return fout;
}

//////////////////////////////////////////////

FOut PFSpecularNormalMapDiffuse(SSpecularNormalMapDiffuse input, bool sbShadows, bool thermal)
{
  FOut fout = InitFOut();

  // Calculate the lights
  half coefDiffuse;
  fout.light = GetLightNormal(input.ambientColor, input.specularColor_landShadow.rgb,
    tex2D(sampler1, input.tColorMap_NormalMap.wzyx), input.tLightLocal.xyz, input.tUpLocal.xyz, coefDiffuse);

  // Calculate the color
  fout.color = GetColorDiffuseDetail(
    tex2D0(input.tColorMap_NormalMap.xy),
    tex2D(sampler2, input.tDetailMap.xy),
    input.ambientColor.a, 0);

  // Calculate the fog
  fout.fog = input.tPSFog.w;

  // Calculate the land shadow
  fout.landShadow = input.specularColor_landShadow.a;

  // Remember the nDotL
  fout.nDotL = coefDiffuse;

  return fout;
}

//////////////////////////////////////////////

FOut PFSpecularNormalMapDiffuseMacroAS(SSpecularNormalMapDiffuseMacroAS input, bool sbShadows, bool thermal)
{
  FOut fout = InitFOut();

  // Calculate the lights
  half coefDiffuse;
  fout.light = GetLightNormalAS(input.ambientColor, input.specularColor_landShadow.rgb,
    tex2D(sampler1, input.tColorMap_NormalMap.wzyx), input.tLightLocal.xyz, input.tUpLocal.xyz,
    tex2D(sampler4, input.tShadowMap.xy),
    coefDiffuse);

  // Calculate the color
  fout.color = GetColorDiffuseDetailMacroAS(
    tex2D0(input.tColorMap_NormalMap.xy),
    tex2D(sampler2, input.tDetailMap_MacroMap.xy),
    tex2D(sampler3, input.tDetailMap_MacroMap.wzyx),
    input.ambientColor.a, 0);

  // Calculate the fog
  fout.fog = input.tPSFog.w;

  // Calculate the land shadow
  fout.landShadow = input.specularColor_landShadow.a;

  // Remember the nDotL
  fout.nDotL = coefDiffuse;

  return fout;
}

//////////////////////////////////////////////

#define HeightFieldScale +0.03

/// see also landImpl.hpp
static const int LandMatStageSat = 0;
static const int LandMatStageMask = 1;
static const int LandMatStageMidDet = 2;

static const int LandMatStageLayers = 3;
static const int LandMatStagePerLayer = 2;
static const int LandMatNoInStage = 0;
static const int LandMatDtInStage = 1;
static const int LandMatLayerCount = 6;

#ifndef _XBOX

/**
When using switch inside of the inner loop, we get much shorter shaders (much less total instructions), however there needs to be a dynamic branch
inside of each loop, which might be slower.

Note: Experiments on X1950 did not confirm any measurable performance difference
*/
#define INNER_SWITCH 1

half SampleHeightField(const int stage, float2 dtMap, float mipLevel)
{
  // same differential in both x and y, different for u,v
  //return tex2Dgrad(samplers[stage],dtMap,rayDStep,rayDStep).r;
  //return tex2Dgrad(samplers[stage],dtMap,dx,dy).r;
  return tex2Dlod(samplers[stage],float4(dtMap,0,mipLevel)).r;
  //return 0;
}

half3 HeightFieldCastRay(float3 ray, const int layerIndex, float2 dtMap, float2 dx, float2 dy, half heightAlpha, half heightFarAlpha, half noMapSizeLog2)
{
  int nSteps = 8;
  
  const float stepScale = 0.1;
  float3 step = normalize(ray)/nSteps*stepScale;
  
  float stepSize = stepScale/nSteps;
  
  float3 offset = 0;
  half lastDepth = 0;
  
  // compute which LOD (or dx/dy) is needed
  // step.x is the du/dx, step.y is the dv/dx
  // to prevent aliasing reliable we pretend a little bit higher step
  // we do not use dx/dy, as it has no relation to how we are sampling the height map at all
  float2 rayDStep = step.xy*1.5;
  
  //float rayMip = 0;
  float rayMip = noMapSizeLog2 + log2( dot(rayDStep,rayDStep) )*0.5;
  //float rayMip = 0 + log2( dot(rayDStep,rayDStep) )*0.5;
  //float rayMip = 10 + log2( dot(rayDStep,rayDStep) )*0.5;
  
  [loop] for (int s=0; s<nSteps; s++)
  {
    half heightOffset;
    #if INNER_SWITCH
      [branch] switch (layerIndex)
      {
        default: heightOffset = SampleHeightField(LandMatStageLayers+LandMatStagePerLayer*0+LandMatNoInStage,dtMap-offset.xy,rayMip); break;
        case 1:  heightOffset = SampleHeightField(LandMatStageLayers+LandMatStagePerLayer*1+LandMatNoInStage,dtMap-offset.xy,rayMip); break;
        case 2:  heightOffset = SampleHeightField(LandMatStageLayers+LandMatStagePerLayer*2+LandMatNoInStage,dtMap-offset.xy,rayMip); break;
        case 3:  heightOffset = SampleHeightField(LandMatStageLayers+LandMatStagePerLayer*3+LandMatNoInStage,dtMap-offset.xy,rayMip); break;
        case 4:  heightOffset = SampleHeightField(LandMatStageLayers+LandMatStagePerLayer*4+LandMatNoInStage,dtMap-offset.xy,rayMip); break;
        case 5:  heightOffset = SampleHeightField(LandMatStageLayers+LandMatStagePerLayer*5+LandMatNoInStage,dtMap-offset.xy,rayMip); break;
      }
    #else
      heightOffset = SampleHeightField(LandMatStageLayers+LandMatStagePerLayer*layerIndex+LandMatNoInStage,dtMap-offset.xy,dx,dy,rayDStep);
    #endif

    // we want heightOffset 1 to mean on surface (black is deep, white is shallow)
    half depth = lerp(0.5*HeightFieldScale,(1-heightOffset)*HeightFieldScale,heightAlpha)*heightFarAlpha;
    
    if (depth<=offset.z)
    {
      // intersection with interpolation from previous sample
      //return offset;
      float3 lastOffset = offset-step;
      half deltaC = offset.z-depth;
      float deltaL = lastOffset.z-lastDepth;
      half denom = deltaC-deltaL;
      half3 nom = lastOffset*deltaC-offset*deltaL;
      if (denom!=0) offset = nom/denom; // avoid division by zero
      return offset;
    }
    
    lastDepth = depth;
    
    offset += step;
  }
  // last offset was never sampled - we cannot return it as it might be under the surface
  // we need to return a previous sample which was verified to be OK.
  // as an alternative we might extrapolate from the last two samples
  return offset-step;
}

half HeightFieldCastRayShadow(half3 ray, float3 pos, const int layerIndex, float2 dtMap, float2 dx, float2 dy, half heightAlpha, half heightFarAlpha, half noMapSizeLog2)
{
  int nSteps = 6;
  
  const half stepScale = 0.10;
  half3 step = normalize(ray)/nSteps*stepScale;
  // first sample is not necessary - depth in it should always be zero, we can safely skip it
  half3 offset = pos + step;
  half lightThrough = 1;
  
  half distCoef = 1;
  
  half2 rayDStep = step.xy*1.5;
  float rayMip = noMapSizeLog2 + log2( dot(rayDStep,rayDStep) )*0.5;
  [loop] for (int s=0; s<nSteps-1; s++)
  {
    half heightOffset;
    #if INNER_SWITCH
      [branch] switch (layerIndex)
      {
        /*
        default: heightOffset = tex2Dgrad(samplers[LandMatStageLayers+LandMatStagePerLayer*0+LandMatNoInStage],dtMap-offset.xy,rayDStep,rayDStep).r; break;
        case 1: heightOffset = tex2Dgrad(samplers[LandMatStageLayers+LandMatStagePerLayer*1+LandMatNoInStage],dtMap-offset.xy,rayDStep,rayDStep).r; break;
        case 2: heightOffset = tex2Dgrad(samplers[LandMatStageLayers+LandMatStagePerLayer*2+LandMatNoInStage],dtMap-offset.xy,rayDStep,rayDStep).r; break;
        case 3: heightOffset = tex2Dgrad(samplers[LandMatStageLayers+LandMatStagePerLayer*3+LandMatNoInStage],dtMap-offset.xy,rayDStep,rayDStep).r; break;
        case 4: heightOffset = tex2Dgrad(samplers[LandMatStageLayers+LandMatStagePerLayer*4+LandMatNoInStage],dtMap-offset.xy,rayDStep,rayDStep).r; break;
        case 5: heightOffset = tex2Dgrad(samplers[LandMatStageLayers+LandMatStagePerLayer*5+LandMatNoInStage],dtMap-offset.xy,rayDStep,rayDStep).r; break;
        */
        default: heightOffset = tex2Dlod(samplers[LandMatStageLayers+LandMatStagePerLayer*0+LandMatNoInStage],float4(dtMap-offset.xy,0,rayMip)).r; break;
        case 1: heightOffset = tex2Dlod(samplers[LandMatStageLayers+LandMatStagePerLayer*1+LandMatNoInStage],float4(dtMap-offset.xy,0,rayMip)).r; break;
        case 2: heightOffset = tex2Dlod(samplers[LandMatStageLayers+LandMatStagePerLayer*2+LandMatNoInStage],float4(dtMap-offset.xy,0,rayMip)).r; break;
        case 3: heightOffset = tex2Dlod(samplers[LandMatStageLayers+LandMatStagePerLayer*3+LandMatNoInStage],float4(dtMap-offset.xy,0,rayMip)).r; break;
        case 4: heightOffset = tex2Dlod(samplers[LandMatStageLayers+LandMatStagePerLayer*4+LandMatNoInStage],float4(dtMap-offset.xy,0,rayMip)).r; break;
        case 5: heightOffset = tex2Dlod(samplers[LandMatStageLayers+LandMatStagePerLayer*5+LandMatNoInStage],float4(dtMap-offset.xy,0,rayMip)).r; break;
      }
    #else
      heightOffset = tex2Dgrad(samplers[LandMatStageLayers+LandMatStagePerLayer*layerIndex+LandMatNoInStage],dtMap-offset.xy,dx,dy).r;
    #endif

    
    // we want heightOffset 1 to mean on surface (black is deep, white is shallow)
    half depth = lerp(0.5*HeightFieldScale,(1-heightOffset)*HeightFieldScale,heightAlpha)*heightFarAlpha;
    
    // how deep is the ray under the surface
    half under = saturate((offset.z-depth)*300);
    
    // the further away we are from the shadow caster, the weaker we want the shadow
    lightThrough = min(lightThrough,1-under*distCoef);
    
    distCoef -= 1.0/nSteps;
    
    offset += step;
  }
  //return 1;
  return lightThrough;
}

#endif

#if 1
/// represent 8 values by one object
struct Half8
{
  half4 lo;
  half4 hi;
};
#define Half8Zeros {0,0,0,0,0,0,0,0}

void MulHalf8(inout Half8 x, half f)
{
  x.lo *= f;
  x.hi *= f;
}

void SetHalf8(inout Half8 x, int index, half value)
{
  if (index>=4) x.hi[index-4] = value;
  else x.lo[index] = value;
}
half GetHalf8(Half8 x, int index)
{
  if (index>=4) return x.hi[index-4];
  return x.lo[index];
}

#else
/// keep the same interface as Half8, but only for 4 layers
struct Half8
{
  half4 lo;
};
#define Half8Zeros {0,0,0,0}

void MulHalf8(inout Half8 x, half f)
{
  x.lo *= f;
}

void SetHalf8(inout Half8 x, int index, half value)
{
  x.lo[index] = value;
}
half GetHalf8(Half8 x, int index)
{
  return x.lo[index];
}
#endif

FOut PFTerrainBodyX(STerrain input, const bool layers[6], bool simple, bool grass, bool sbShadows, bool alphaOnly, bool thermal)
{
  FOut fout = InitFOut();

#if _SM2
  simple = true;
#endif

  // Sample satellite and mask maps
  float2 satMask = input.tSatAndMask_GrassMap.xy;
  half3 satTexture   = tex2D0(satMask);
  half4 maskTexture  = tex2D(sampler1,satMask);

  Half8 alphas = Half8Zeros;
  {
    int passIndex = 0;
    half first = 1;
    [unroll] for (int i=0; i<LandMatLayerCount; i++)
    {
      if (layers[i])
      {
        // read constant telling us if the layer is on or off
        half layerOn = PSC_Layers[i].x;
        half alpha;
        // read the corresponding mask channel
        if (passIndex>0)
        {
          if (i<3)
          {
            // ABC style weights
            alpha = maskTexture[i-1];
          }
          else
          {
            // overall weight for the whole XYZ layer
            half cAlpha = maskTexture[3-1];
            // XYZ style weights
            // when texture alpha is 1, we want layer 0 to be used
            half a = 1-maskTexture[3];
            // weight for layer 1 grows between a = 0 .. 0.5 
            // weight for layer 2 grows between a = 0.5 .. 1
            half weightsXYZ[3] = {1, saturate(a*2), saturate((a-0.5)*2) };
            // compute overall weight
            alpha = weightsXYZ[i-3]*cAlpha;
          }
        }
        else alpha = 1;
        // we want the first layer which is really used to be always opaque
        // on the other hand, if the channel is off, we want the alpha to be fully transparent
        alpha = max(alpha,first)*layerOn;
        // if the layer was used, we want to reset the "first"
        first = min(first,1-layerOn);
        
        // all alphas blended so far need to be reduced by 1-alpha
        MulHalf8(alphas,1-alpha);
        SetHalf8(alphas,passIndex,alpha);
        // proceed to the next stage
        passIndex++;
      }
    }
  }
  
  // self shadowing computed by occlusion mapping
  half selfshadow = 1;

  float3 offset = 0;
  float2 coMap = input.tColorMap_NormalMap.xy;
  float2 dtMap = input.tDetailMap_SpecularMap.xy;
  // noMap most often shares mapping with a detail map
  //float2 noMap = input.tColorMap_NormalMap.wz;

  //dtMap = frac(dtMap);

  #if 1
    // first of all select dominant layer
    // this is used for ray-casting, as ray-casting with sampling all layers and mask is way too expensive
    
    int heightLayer = 0; // sampler containing the height map
    half noMapSizeLog2 = 0;
    
    //
    half heightAlpha = 0; // height map influence - used for transitions
    half heightFarAlpha =0; // used for transition into normal maps used far away
    {
      int passIndex = 0;
      // find last stage with effective alpha>0.5
      // how much of alpha is covered by the layers which were already processed
      half maxAlpha = 0;
        
      [unroll] for (int i=0; i<LandMatLayerCount; i++)
      {
        if (layers[i])
        {
          half effectiveAlpha = GetHalf8(alphas,i);
          if (effectiveAlpha>maxAlpha)
          {
            maxAlpha = effectiveAlpha;
            heightLayer = i;
            if (i<4) noMapSizeLog2 = PSC_NoTexSizeLog2[0][i];
            else noMapSizeLog2 = PSC_NoTexSizeLog2[1][i-4];
          }
          passIndex++;
        }
      }
      // map maxAlpha = 1 to transitionFactor = 1
      // map maxAlpha = 0.5 to transitionFactor = 0
      // this is too much conservative with more than two surfaces blending together
      // however is guarantees smooth transitions everywhere
      heightAlpha = max(maxAlpha*2-1,0);
      //heightAlpha = 1;
    }
  #endif
  
  // Get the light in local vertex space
  half3 lightDir = input.tLightLocal.xyz;
  
  // avoid tEyeLocal being under the surface, which might happen as a result of ST space interpolation
  input.tEyeLocal.z = max(input.tEyeLocal.z,0.2);
  
#ifndef _XBOX
  // we want parallax mapping only with Shading Detail Normal or better
  if (!simple && !grass)
  {
    //[branch]
    if (PSC_ShadingDetail[1])
    //if (false)
    {
      float2 dx = ddx(dtMap), dy = ddy(dtMap);
      
      // read (and average) all height maps for parallax mapping
      // perform multiple samples to find a first intersection

      //float2 dx = 0, dy = 0;
    
      // Find min of change in u and v across quad: compute du and dv magnitude across quad
      half2 dTexCoords = dx * dx + dy * dy;

      // Standard mip-mapping uses max here
      half minTexCoordDelta = max( dTexCoords.x, dTexCoords.y );

      // Compute the current mip level  (* 0.5 is effectively computing a square root before )
      half mipLevel = 0.5 * log2( minTexCoordDelta );
      
      // once mip level reaches a threshold, start transition to normal mapping
      // what we get is a "relative mip", where 0 = whole u/v range mapped to 1 texel
      // mipLevel = -10 means resolution 1024x1024 would be desired
      // endFade = -10 means transition starts very close to the camera
      // endFade = -6 means transition starts quite far from the camera
      
      const half startFade = -6;
      const half endFade = -8;
      
      if (mipLevel<startFade)
      {
        half lodFactor = 1;
        if (mipLevel>endFade)
        {
          lodFactor = 1-(mipLevel-endFade)/(startFade-endFade);
        }
        heightFarAlpha = lodFactor;
        #if INNER_SWITCH
          offset = HeightFieldCastRay(input.tEyeLocal,heightLayer,dtMap,dx,dy,heightAlpha,heightFarAlpha,noMapSizeLog2);
          selfshadow = HeightFieldCastRayShadow(-lightDir,offset,heightLayer,dtMap,dx,dy,heightAlpha,heightFarAlpha,noMapSizeLog2);
        #else
          [branch] switch (heightLayer)
          {
            default:
              offset = HeightFieldCastRay(input.tEyeLocal,0,dtMap,dx,dy,heightAlpha,heightFarAlpha);
              selfshadow = HeightFieldCastRayShadow(-lightDir,offset,0,dtMap,dx,dy,heightAlpha,heightFarAlpha);
              break;
            case 1:
              offset = HeightFieldCastRay(input.tEyeLocal,1,dtMap,dx,dy,heightAlpha,heightFarAlpha);
              selfshadow = HeightFieldCastRayShadow(-lightDir,offset,1,dtMap,dx,dy,heightAlpha,heightFarAlpha);
              break;
            case 2:
              offset = HeightFieldCastRay(input.tEyeLocal,2,dtMap,dx,dy,heightAlpha,heightFarAlpha);
              selfshadow = HeightFieldCastRayShadow(-lightDir,offset,2,dtMap,dx,dy,heightAlpha,heightFarAlpha);
              break;
            case 3:
              offset = HeightFieldCastRay(input.tEyeLocal,3,dtMap,dx,dy,heightAlpha,heightFarAlpha);
              selfshadow = HeightFieldCastRayShadow(-lightDir,offset,3,dtMap,dx,dy,heightAlpha,heightFarAlpha);
              break;
            case 4:
              offset = HeightFieldCastRay(input.tEyeLocal,4,dtMap,dx,dy,heightAlpha,heightFarAlpha);
              selfshadow = HeightFieldCastRayShadow(-lightDir,offset,4,dtMap,dx,dy,heightAlpha,heightFarAlpha);
              break;
            case 5:
              offset = HeightFieldCastRay(input.tEyeLocal,5,dtMap,dx,dy,heightAlpha,heightFarAlpha);
              selfshadow = HeightFieldCastRayShadow(-lightDir,offset,5,dtMap,dx,dy,heightAlpha,heightFarAlpha);
              break;
          }
        #endif
        
        //selfshadow *= lodFactor;
        
        dtMap -= offset.xy;
      }
    }
    
    //selfshadow = saturate(startFade-mipLevel);

  }
#else
  if (!simple && !grass)
  {
    // read dominant height map for parallax mapping
    half heightOffset;
    
    float2 dx = ddx(dtMap), dy = ddy(dtMap);
    
    [branch] switch (heightLayer)
    {
      default: heightOffset = tex2Dgrad(samplers[LandMatStageLayers+LandMatStagePerLayer*0+LandMatNoInStage],dtMap,dx,dy).r; break;
      case 1: heightOffset = tex2Dgrad(samplers[LandMatStageLayers+LandMatStagePerLayer*1+LandMatNoInStage],dtMap,dx,dy).r; break;
      case 2: heightOffset = tex2Dgrad(samplers[LandMatStageLayers+LandMatStagePerLayer*2+LandMatNoInStage],dtMap,dx,dy).r; break;
      case 3: heightOffset = tex2Dgrad(samplers[LandMatStageLayers+LandMatStagePerLayer*3+LandMatNoInStage],dtMap,dx,dy).r; break;
      case 4: heightOffset = tex2Dgrad(samplers[LandMatStageLayers+LandMatStagePerLayer*4+LandMatNoInStage],dtMap,dx,dy).r; break;
      case 5: heightOffset = tex2Dgrad(samplers[LandMatStageLayers+LandMatStagePerLayer*5+LandMatNoInStage],dtMap,dx,dy).r; break;
    }
    // in the map: 0 = deep, 1 = shallow
    // convert to 1 = deep, 0 = shallow. At the same time apply transition.
    offset = normalize(input.tEyeLocal.xyz)*(1-heightOffset)*heightFarAlpha*HeightFieldScale;
    dtMap -= offset.xy;
  }
#endif

  float2 dx = ddx(dtMap), dy = ddy(dtMap);
  float2 dxCo = ddx(coMap), dyCo = ddy(coMap);
  
  float2 noMap;
  float2 dxNo,dyNo;
  if (!grass)
  {
    noMap = dtMap;
    dxNo = dx, dyNo = dy;
  }
  else
  {
    // grass layer needs a specific "normal map stage" mapping
    noMap = input.tSatAndMask_GrassMap.wz;
    //satMask*SegGridScale.xx+SegGridScale.yy;
    dxNo = ddx(noMap), dyNo = ddy(noMap);
  }

  // Get first layer
  half3 detTextureAvg = 0;
  half3 detTexture = 0;
  half4 normalMap=0;

  half3 primTexture = tex2D(samplers[LandMatStageMidDet],coMap);

  {
    int passIndex=0;  
    // coordinates for sampling smallest mipmap of the detail texture
    float4 dtLod = float4(dtMap,0,20);
    [unroll] for (int i=0; i<LandMatLayerCount; i++)
    {
      if (layers[i])
      {
        half alpha = GetHalf8(alphas,passIndex);
        // while dynamic branching should be able to reduce texture loads significantly
        // in practice it seems the difference is marginal
        // Avoid tex2Dgrad as it is very slow on nVidia HW. Explanation from Kirill Dmitriev mailto:KDmitriev@nvidia.com
        
        /*
        Quad has four pixels. DDX between the upper two may return different value than DDX between the lower two. And
        the same for DDY between left pair and right pair. As I understand this is the reason why different pixels in
        the quad may fetch from different mipmap levels when using tex2Dgrad. tex2D uses something like an average between
        two DDXs and between two DDYs, thus it ends up with 4 pixels landing on the same mip level - which is faster.
        */
        
        #define BRANCH_HERE 0
        #if 0 // BRANCH_HERE
        [branch] if (alpha>0)
        #endif
        {
          int base = LandMatStageLayers+LandMatStagePerLayer*passIndex;
        #if 0 //BRANCH_HERE
          normalMap   += tex2Dgrad(samplers[base+LandMatNoInStage],noMap,dxNo,dyNo)*alpha;
          detTexture  += tex2Dgrad(samplers[base+LandMatDtInStage],dtMap,dx,dy)*alpha;
          detTextureAvg += tex2Dlod(samplers[base+LandMatDtInStage],dtLod)*alpha;
        #else
          normalMap   += tex2D(samplers[base+LandMatNoInStage],noMap)*alpha;
          detTexture  += tex2D(samplers[base+LandMatDtInStage],dtMap)*alpha;
#if _SM2
          detTextureAvg += tex2Dbias(samplers[base+LandMatDtInStage],dtLod)*alpha;
#else
          detTextureAvg += tex2Dlod(samplers[base+LandMatDtInStage],dtLod)*alpha;
#endif
        #endif
        }
        passIndex++;
      }
    }
  }
  

  // now adjust the detTexture color to match satellite map
  //detTexture = min(detTexture*(satTexture/detTextureAvg),1);
  // See COLOR_MODIFICATION
#if _SM2
  if (!thermal)
#endif
  {
    half3 adjust = (satTexture/detTextureAvg);
    adjust = min(2,max(adjust,0.5));
    detTexture = detTexture*adjust;
    //detTexture = detTextureAvg;

    //detTexture = alphas;
  }
  
  /**/
  // lerp - detail map / satellite map
  // distance is given in ambientColor.a
  // Calculate the texel color
  half3 colorTexel;
  half diffuse;
  half diffuseBack;
  
  
  //primTexture = 0.5;
  if (grass)
  {
    half perlin = tex2D(samplers[LandMatStageLayers+LandMatDtInStage],noMap).a;
    // grass is rendered for middle distance
    // no detail textures needed for the grass
    //colorTexel = lerp(satTexture*primTexture*2,normalMap.rgb,input.grassAlpha.XXX);
    colorTexel = satTexture*primTexture*2;
    diffuse = max(lightDir.z, 0);
    diffuseBack = max(-lightDir.z, 0);
    // normal map has different meaning - alpha used only for alpha testing
    // clip only needed during z-priming (i.e. when rendering alpha only)
    if (alphaOnly)
    {
      clip(normalMap.a*input.grassAlpha.w+perlin-1);
    }
    //clip(-1);

    // This code will disable grass layer for thermal imaging mode (grass layer is not very important in thermal vision anyway)
    if (thermal)
    {
      colorTexel = half3(0, 0, 0);
      clip(-1);
    }
  }
  else if (simple)
  {
#if _SM2
    if (thermal)
    {
      colorTexel = satTexture;
    }
    else
#endif
    {
      half detFactor = input.ambientColor.a;
      colorTexel = lerp(satTexture,detTexture,detFactor);
    }
    diffuse = max(lightDir.z, 0);
    diffuseBack = max(-lightDir.z, 0);
  }
  else
  {
    colorTexel = lerp(satTexture*primTexture*2,detTexture,input.ambientColor.a);

    //colorTexel = lerp(half3(1,0,0),half3(0,1,0),noMapSizeLog2+0.5);
    //colorTexel = half3(0,0,nopxMap.r)+half3(length(offset)*10,0,0);
    //colorTexel = half3(selfshadow,1-selfshadow,0);
    //colorTexel = half3(0,normalMap.r,0)+half3(offset.z*10,0,length(offset.xz)*10);
    //colorTexel = half3(0,normalMap.r,0)+half3(0,0,length(offset.xz)*10);
    //colorTexel = half3(1-length(offset.xy)*10,0,length(offset.xy)*10);

    //colorTexel = normalize(input.tEyeLocal)*0.5+0.5;
    //colorTexel = normalize(input.tEyeLocal).bbb*0.5+0.5;
    
    // Get the normal from normal map
    // we know the normal map is DXT5 compressed (hq, vhq or nopx)
    half2 normalXY = half2(1 - normalMap.a,normalMap.g)*2-1;
    half3 normal = half3(normalXY,sqrt(1-dot(normalXY,normalXY)));

    // Calculate the diffuse coefficient  
    half nDotL = dot(normal, lightDir);
    diffuse = max(nDotL, 0) * selfshadow;
    diffuseBack = max(-nDotL, 0) * selfshadow;
    
  }


  // Fill out the Light structure
  if (grass)
  {
    fout.light.indirect = GetIndirectLight(1) + input.ambientColor.rgb + PSC_LDirectionGround * diffuseBack; // In grass case the "normalMap" is apparently not a normal map
  }
  else
  {
    half3 upLocal = half3(0, 0, 1); // We can consider the normal map on ground is mapped always the similar way - down the ground. That's why this is roughly correct
    fout.light.indirect = GetIndirectLight(dot(normalMap.rgb, upLocal)) + input.ambientColor.rgb
#if _VBS3_PERPIXELPSLIGHTS
      + PointSpotLighting(input.tPSFog.xyz, input.grassAlpha.xyz)
#endif
      + PSC_LDirectionGround * diffuseBack;
  }
  fout.light.direct = /*PSC_DForced + NotRequired*/diffuse * PSC_Diffuse;
  fout.light.specular = input.specularColor.rgb;
  fout.light.specularEnv = 0;
  fout.light.diffuseShadowCoef = 1;

  // Calculate the color
  fout.color = half4(colorTexel, 1);

  // Calculate the fog
  fout.fog = input.tPSFog.w;

  // Calculate the land shadow
  // specularColor alpha contains depth under the shadow horizon
  // depth=0 -> shadow = 1
  // depth=1 -> shadow = 0
  fout.landShadow = saturate(0.5-input.specularColor.a*0.5)*0.8+0.2;

  // Initialize the halfCooling time
  // The closer is the base color to grass color, the smaller the halfCooling time is
  half3 grassColor = half3(0.0123, 0.0118, 0.0185);
  half3 grassDiff = (fout.color.rgb - grassColor);
  half3 grassDiff2 = grassDiff * grassDiff;
  half grassCoef = sqrt(grassDiff2.r + grassDiff2.g + grassDiff2.b);
  fout.airCoolingFactor = max(1.0f - grassCoef, 0); // _VBS3_TI

  // Remember the nDotL
  fout.nDotL = diffuse;

  /* // zeroing not needed - the caller will not use the results anyway
  if (alphaOnly)
  {
    // with alpha only the only thing we need from the shader is the clip (texkill)
    // we do not need any color to be returned
    fout.light.indirect = 0;
    fout.light.direct = 0;
    fout.light.specular = 0;
    fout.light.specularEnv = 0;
    fout.light.diffuseShadowCoef = 1;
    fout.color = 0;
    fout.fog = 0;
    fout.landShadow = 1;
    fout.halfCooling = 0;
    fout.htCoef = 0;
  }
  */
  return fout;
}

#define TERRAINLAYER_N(x,N) \
  FOut PFTerrain##N(STerrain input, bool sbShadows, bool thermal) \
  { \
    const bool layers[6]={(x%2)!=0, ((x/2)%2)!=0, ((x/4)%2)!=0, ((x/8)%2)!=0, ((x/16)%2)!=0, ((x/32)%2)!=0}; \
    return PFTerrainBodyX(input, layers, false, false, sbShadows, false, thermal); \
  } \
  FOut PFTerrainSimple##N(STerrain input, bool sbShadows, bool thermal) \
  { \
    const bool layers[6]={(x%2)!=0, ((x/2)%2)!=0, ((x/4)%2)!=0, ((x/8)%2)!=0, ((x/16)%2)!=0, ((x/32)%2)!=0}; \
    return PFTerrainBodyX(input, layers, true, false, sbShadows, false, thermal); \
  } \
  FOut PFTerrainGrass##N(STerrain input, bool sbShadows, bool thermal) \
  { \
    const bool layers[6]={(x%2)!=0, ((x/2)%2)!=0, ((x/4)%2)!=0, ((x/8)%2)!=0, ((x/16)%2)!=0, ((x/32)%2)!=0}; \
    return PFTerrainBodyX(input, layers, true, true, sbShadows, false, thermal); \
  } \

TERRAINLAYER_N(15,15)
#if _SM2
TERRAINLAYER_N(15,X)
#else
TERRAINLAYER_N(63,X)
#endif

//////////////////////////////////////////////
// very similar to PFTerrain and PFNormalMapDetailSpecularMap

FOut PFRoad(STerrain input, bool sbShadows, bool thermal)
{
  FOut fout = InitFOut();

  // Calculate the lights
  half3 upLocal = half3(0, 0, 1); // We can consider the normal map on roads is mapped always the similar way - down the ground. That's why this is roughly correct
  half coefDiffuse;
  half3 halfway = normalize(normalize(input.tEyeLocal)+input.tLightLocal);
  fout.light = GetLightNormalSpecular(input.ambientColor
#if _VBS3_PERPIXELPSLIGHTS
    + PointSpotLighting(input.tPSFog.xyz, input.grassAlpha.xyz)
#endif
    ,
    tex2D(sampler1, input.tColorMap_NormalMap.wzyx), input.tLightLocal.xyz, halfway, upLocal,
    tex2D(sampler3, input.tDetailMap_SpecularMap.wzyx),
    coefDiffuse);

  // Calculate the color
  fout.color = GetColorDiffuseDetail(
    tex2D0(input.tColorMap_NormalMap.xy),
    tex2D(sampler2, input.tDetailMap_SpecularMap.xy),
    input.ambientColor.a, 0);

  // Calculate the fog
  fout.fog = input.tPSFog.w;

  // Calculate the land shadow
  // specularColor alpha contains depth under the shadow horizon
  // depth=0 -> shadow = 1
  // depth=1 -> shadow = 0
  fout.landShadow = saturate(0.5-input.specularColor.a*0.5)*0.8+0.2;

  // Remember the nDotL
  fout.nDotL = coefDiffuse;

  return fout;
}

//////////////////////////////////////////////

FOut PFRoad2Pass(STerrain input, bool sbShadows, bool thermal)
{
  FOut fout = InitFOut();

  // Kill output in case of alfa
  clip(tex2D0(input.tColorMap_NormalMap.xy).a - 0.5f);

  // Calculate the lights
  half coefDiffuse = max(dot(half3(0, 0, 1), input.tLightLocal.xyz), 0.0f);
  fout.light = GetLightSimple(half3(0, 0, 0), half3(0, 0, 0), half3(0, 0, 0));

  // Calculate the color
  fout.color = half4(0, 0, 0, 1);

  // Calculate the fog
  fout.fog = input.tPSFog.w;

  // Remember the nDotL
  fout.nDotL = coefDiffuse;

  // Calculate the land shadow
  // specularColor alpha contains depth under the shadow horizon
  // depth=0 -> shadow = 1
  // depth=1 -> shadow = 0
  fout.landShadow = saturate(0.5-input.specularColor.a*0.5)*0.8+0.2;

  return fout;
}

//////////////////////////////////////////////

#if _VBS3

float4 PSShore( float4 tNormalMap1_2          : TEXCOORD1,
                float2 tNormalMap3            : TEXCOORD3,
                half4 ambientColor            : TEXCOORDAMBIENT,
                float4 tFresnelView           : TEXCOORD2,
                half3 specularHalfway         : COLOR0,
                half4 tPSFog                  : COLOR1,
                float4 tHeight                : TEXCOORD0,
                float4 stn2lws0               : TEXCOORD4,
                float4 stn2lws1               : TEXCOORD5,
                float4 stn2lws2               : TEXCOORDSPECULAR) : COLOR0
{
  // Get the normal
  half3 normal;
  {
    // Sample 3 normals from the source
    half4 normal1 = tex2D(sampler1, tNormalMap1_2.xy);
    half4 normal2 = tex2D(sampler1, tNormalMap1_2.zw);
    half4 normal3 = tex2D(sampler1, tNormalMap3.xy);

    // perform DXT5 swizzle and bx2 conversion
    half4 normalSrc = (normal1+normal2+normal3)*0.333f;
    normalSrc.x += (1-normalSrc.a);
    normalSrc.xy -= 0.5;
    // recalculate z based on xy
    normalSrc.z = 0.333;
    //normal = normalize(normalSrc.xyz);
    //normal = half3(0, 0, 1); // FLY

    // See label NORMAL_DISAPPEAR
    const half maxDisappear = 0.5f;
    normal = normalize(lerp(normalSrc.xyz, half3(0, 0, 1), maxDisappear));
  }

  // Get value from the reflection map
  half3 envColor;
  {
    // Get the LWS position
    float3 lwsPosition = float3(stn2lws0.w, stn2lws1.w, stn2lws2.w);

    // Get the LWS normal
    float3 lwsNormal;
    {
      lwsNormal.x = dot(stn2lws0, normal);
      lwsNormal.y = dot(stn2lws1, normal);
      lwsNormal.z = dot(stn2lws2, normal);
    }

    // Calculate the O vector (the reflection vector)
    float3 o;
    {
      float3 v = normalize(-lwsPosition.xyz);
      o = lwsNormal.xyz * dot(lwsNormal.xyz, v) * 2.0f - v;
    }

    // Modify O vector to favour bounding texels, multiply by constant to omit degenerated texels
    float oSize2 = dot(o.xz,o.xz);
    o.xz = o.xz * oSize2 * 0.95f;

    // Get color from the environmental map
    half3 envColorA = tex2D(sampler4, o.zx * float2(-0.5,+0.5) + 0.5);
    half3 envColorB = tex2D(sampler5, o.zx * float2(-0.5,+0.5) + 0.5);
    envColor = lerp(envColorB, envColorA, tFresnelView.w);
  }

  // Get the fresnel specular coefficient
  half4 fresnelSpecular;
  {
    // Get the specular halfway vector
    half3 specularHalfway = normalize(specularHalfway.rgb);

    // Get the sample coordinates and sample the reflect map
    half u = dot(normal, normalize(tFresnelView));
    half v = dot(normal, specularHalfway);
    fresnelSpecular = tex2D(sampler3, half2(u,v));
  }

  // Sum environmental color and sun specular - result is total reflected color
  half3 reflectedColor = envColor * PSC_GlassEnvColor + fresnelSpecular.rgb * PSC_Specular;

  // Calculate the water factor (1 - on wave, 0 - biggest distance from wave)
  half waveFactorA = saturate(tHeight.w + tHeight.x);
  half waveFactorB = saturate(tHeight.w + tHeight.y);

  // Calculate the water factor (almost discrete output depending on current wave)
  half waterFactorA = 1.0f - saturate((waveFactorA - 0.80f) * 10.0f);
  half waterFactorB = 1.0f - saturate((waveFactorB - 0.80f) * 10.0f);

  // Modify waterFactor - disappear water with height
  half waterFactor = saturate(saturate(1.2f - /*abs*/(tHeight.w * tHeight.w)) * max(waterFactorA, waterFactorB));


  half3 color = lerp(ambientColor.rgb, reflectedColor, fresnelSpecular.a) * waterFactor;
  half alpha = waterFactor * ambientColor.a;
  half4 base = half4(color,alpha);
  float4 result = base;
  return ApplySrcAlpha(half4(ToRTColorSpace(ApplyFog(result.rgb, tPSFog.w)),result.a));
}

#else

FOut PFShore(SShore input, bool sbShadows)
{
  // Get the normal
  half3 normal;
  {
    // Sample 3 normals from the source
    half4 normal1 = tex2D(sampler1, input.tNormalMap1_2.xy);
    half4 normal2 = tex2D(sampler1, input.tNormalMap1_2.zw);
    half4 normal3 = tex2D(sampler1, input.tNormalMap3.xy);

    // perform DXT5 swizzle and bx2 conversion
    half4 normalSrc = (normal1+normal2+normal3)*0.333f;
    normalSrc.x += (1-normalSrc.a);
    normalSrc.xy -= 0.5;
    // recalculate z based on xy
    normalSrc.z = 0.333;
    //normal = normalize(normalSrc.xyz);
    //normal = half3(0, 0, 1); // FLY

    // See label NORMAL_DISAPPEAR
    const half maxDisappear = 0.5f;
    normal = normalize(lerp(normalSrc.xyz, half3(0, 0, 1), maxDisappear));
  }

  // Get value from the reflection map
  half3 envColor;
  {
    // Get the LWS position
    float3 lwsPosition = float3(input.stn2lws0.w, input.stn2lws1.w, input.stn2lws2.w);

    // Get the LWS normal
    float3 lwsNormal;
    {
      lwsNormal.x = dot(input.stn2lws0, normal);
      lwsNormal.y = dot(input.stn2lws1, normal);
      lwsNormal.z = dot(input.stn2lws2, normal);
    }

    // Calculate the O vector (the reflection vector)
    float3 o;
    {
      float3 v = normalize(-lwsPosition.xyz);
      o = lwsNormal.xyz * dot(lwsNormal.xyz, v) * 2.0f - v;
    }

    // Modify O vector to favour bounding texels, multiply by constant to omit degenerated texels
    float oSize2 = dot(o.xz,o.xz);
    o.xz = o.xz * oSize2 * 0.95f;

    // Get color from the environmental map
    half3 envColorA = tex2D(sampler4, o.zx * float2(-0.5,+0.5) + 0.5);
    half3 envColorB = tex2D(sampler5, o.zx * float2(-0.5,+0.5) + 0.5);
    envColor = lerp(envColorB, envColorA, input.tFresnelView.w);
  }

  // Get the fresnel specular coefficient
  half4 fresnelSpecular;
  {
    // Get the specular halfway vector
    half3 specularHalfway = normalize(input.specularHalfway.rgb);

    // Get the sample coordinates and sample the reflect map
    half u = dot(normal, normalize(input.tFresnelView));
    half v = dot(normal, specularHalfway);
    fresnelSpecular = tex2D(sampler3, half2(u,v));
  }

  // Sum environmental color and sun specular - result is total reflected color
  half3 reflectedColor = envColor * PSC_GlassEnvColor + fresnelSpecular.rgb * PSC_Specular;

  // Calculate the water factor (1 - on wave, 0 - biggest distance from wave)
  half waveFactorA = saturate(input.tHeight.w + input.tHeight.x);
  half waveFactorB = saturate(input.tHeight.w + input.tHeight.y);

  // Calculate the water factor (almost discrete output depending on current wave)
  half waterFactorA = 1.0f - saturate((waveFactorA - 0.80f) * 10.0f);
  half waterFactorB = 1.0f - saturate((waveFactorB - 0.80f) * 10.0f);

  // Modify waterFactor - disappear water with height
  half waterFactor = saturate(saturate(1.2f - /*abs*/(input.tHeight.w * input.tHeight.w)) * max(waterFactorA, waterFactorB));

  FOut fout = InitFOut();

  // Calculate the lights
  fout.light.indirect = float3(1, 1, 1);
  fout.light.direct = float3(0, 0, 0);
  fout.light.specular = lerp(input.ambientColor.rgb, reflectedColor, fresnelSpecular.a) * waterFactor;
  fout.light.specularEnv = 0;
  fout.light.diffuseShadowCoef = 1;

  // Calculate the color
  fout.color = half4(0, 0, 0, waterFactor * input.ambientColor.a);

  // Calculate the fog
  fout.fog = input.tPSFog.w;

  // Calculate the land shadow
  fout.landShadow = 1;

  return fout;
}

#endif

//////////////////////////////////////////////

FOut PFShoreFoam(SShore input, bool sbShadows)
{
  // Get the foam
  half foam;
  {
    half foam1 = tex2D(sampler2, input.tNormalMap1_2.xy * 8).x;
    half foam2 = tex2D(sampler2, input.tNormalMap1_2.zw * 8).x;
    half foam3 = tex2D(sampler2, input.tNormalMap3.xy * 8).x;
    foam = saturate(foam1 + foam2 + foam3 - 2.1875);
  }

  // Calculate the wave factor (1 - on wave, 0 - biggest distance from wave)
  half waveFactorA = saturate(input.tHeight.w + input.tHeight.x);
  half waveFactorB = saturate(input.tHeight.w + input.tHeight.y);

  // Calculate the water factor (almost discrete output depending on current wave)
  half waterFactorA = 1.0f - saturate((waveFactorA - 0.80f) * 10.0f);
  half waterFactorB = 1.0f - saturate((waveFactorB - 0.80f) * 10.0f);

  // Calculate the foam factor (sample the foam texture with distance to the top of the wave),
  half foamFactor;
  {
    // Calculate foam factors according to waveFactors
    // /*restrict it only to where water is drawn (*waterFactor)*/
    half foamFactorA = (pow(waveFactorA, 10) * 11 + input.tHeight.w * 0.7) * waterFactorA;
    half foamFactorB = (pow(waveFactorB, 10) * 11 + input.tHeight.w * 0.7) * waterFactorB;

    // Get the foam factor
    foamFactor = foam * max(foamFactorA, foamFactorB);

    // Modify foam factor - disappear foam that lies low
    half h = input.tHeight.w * 2.0f - 0.3f;
    foamFactor = saturate(pow(h, 4)) * saturate(foamFactor);
  }

  FOut fout = InitFOut();

  // Calculate the lights
  fout.light.indirect = float3(1, 1, 1);
  fout.light.direct = float3(0, 0, 0);
  fout.light.specular = float3(0, 0, 0);
  fout.light.specularEnv = 0;
  fout.light.diffuseShadowCoef = 1;

  // Calculate the color
  fout.color = half4(PSC_WaveColor.rgb, foamFactor);

  // Calculate the fog
  fout.fog = input.tPSFog.w;

  // Calculate the land shadow
  fout.landShadow = 1;

  return fout;
}

//////////////////////////////////////////////

FOut PFShoreWet(SShoreWet input, bool sbShadows)
{
  // Calculate the wet factor (darkenning of shore disappearing with distance above water)
  half wetFactor = saturate(2.0f - abs(input.tHeight.w)) * 0.4f;

  // Fill out the FOut structure
  FOut fout = InitFOut();

  // Calculate the lights
  fout.light = GetLightSimple(float3(0, 0, 0), float3(0, 0, 0), float3(0, 0, 0));

  // Calculate the color
  fout.color = half4(0, 0, 0, wetFactor);

  // Calculate the fog
  fout.fog = input.tPSFog.w;

  // Calculate the land shadow
  fout.landShadow = 1;

  return fout;
}

//////////////////////////////////////////////

FOut PFGlass(SGlass input, bool sbShadows, bool thermal)
{
  // Calculate the O vector (the reflection vector)
  half3 o;
  {
    half3 v = normalize(-input.lwsPosition.xyz);
    o = input.lwsNormal.xyz * dot(input.lwsNormal.xyz, v) * 2.0f - v;
  }

  // Get the color from the environmental map
  //half3 envColor = tex2D(sampler2, o.xy * 0.5 + 0.5);
  half3 envColor = tex2D(sampler2, o.xy * float2(1, -1) * 0.5 + 0.5) * PSC_GlassEnvColor.rgb;

  // Get the fresel reflection coefficient
  half fresnelCoef = tex2D(sampler1, float2(dot(input.lwsNormal.xyz, o), 0.5)).a;

  FOut fout = InitFOut();

  // Sample the diffuse color
  half4 diffuseColor = tex2D0(input.tDiffuseMap.xy);
  clip(diffuseColor.a - 1.0f/255.0f); // Clip output in case of 100% alpha

  // Get the diffuse coefficient
  half coefDiffuse = max(input.x_x_nDotU_nDotL.w, 0);
  half coefDiffuseBack = max(-input.x_x_nDotU_nDotL.w, 0);

  // Calculate the lights
  fout.light.indirect = input.ambientColor + GetIndirectLight(input.x_x_nDotU_nDotL.z) + PSC_LDirectionGround * coefDiffuseBack;
  fout.light.direct = /*PSC_DForced + Not required*/ PSC_Diffuse * coefDiffuse;
  half3 baseSpecular = (input.specularColor_landShadow.rgb + envColor * PSC_GlassMatSpecular * 2.0f) * fresnelCoef; // 2 is here because specular color is halved
  fout.light.specular = baseSpecular;
  fout.light.specularEnv = 0;
  fout.light.diffuseShadowCoef = 1;

  // Calculate the color
  fout.color = diffuseColor;
  //fout.color = half4(1,0,0,1);

  // In thermal vision the glass is almost opaque
  // _VBS3_TI
  if (thermal) fout.color.a = lerp(fout.color.a, 1.0f, 0.98f);

  // Calculate the fog
  fout.fog = input.tPSFog.w;

  // Calculate the land shadow
  fout.landShadow = input.specularColor_landShadow.a;

  // Remember the nDotL
  fout.nDotL = input.x_x_nDotU_nDotL.w;

  return fout;
}

//////////////////////////////////////////////

FOut PFCraterX(SCrater input, int nCraters, bool sbShadows, bool thermal)
{
  // Sample the diffuse map
  half4 diffuseMap = tex2D0(input.tDiffuseMap.xy);

  //   // More sofistikated footprint - oval shape
  //   float3 closestVector = input.originalPosition.xyz - PSC_Crater[0].xyz;
  //   for (int i = 1; i < nCraters; i++)
  //   {
  //     float3 v = input.originalPosition.xyz - PSC_Crater[i].xyz;
  //     if (dot(v*v, 1) < dot(closestVector*closestVector, 1))
  //     {
  //       closestVector = v;
  //     }
  //   }
  //   float radius = 0.02f;
  //   float3 direction = float3(0, 1, 0);
  //   float stretch = dot(normalize(closestVector), direction);
  //   float coef = radius * (1 + abs(stretch*stretch*stretch)) * rsqrt(dot(closestVector, closestVector)) - 1;
  //   float icoef = (1 - saturate(coef));
  //   half wetFactor = (1 - icoef * icoef * icoef * icoef) * diffuseMap.a;



  // Find the closest crater distance weighted by the crater size
  float distance2 = 100.0f;
#if !_SM2
  [unroll] for (int i = 0; i < nCraters; i++)
  {
    float3 v = PSC_Crater[i].xyz - input.originalPosition.xyz;
    float d2 = dot(v*v, PSC_Crater[i].www);
    if (d2 < distance2)
    {
      distance2 = d2;
    }
  }
#endif

  // Calculate darkening factor
  float craterCoef = 0.9f;
  half wetFactor = (1.0f - min(distance2 * distance2, 1)) * diffuseMap.a * craterCoef;

  // Fill out the FOut structure
  FOut fout = InitFOut();

  // Calculate the lights (very simplified lighting for craters - input.originalPosition.w stores the diffuse coefficient)
  fout.light = GetLightSimple(PSC_GlassEnvColor.rgb, PSC_GlassEnvColor.rgb * input.originalPosition.w, half3(0, 0, 0));

  // Calculate the color
  fout.color.rgb =  half3(1, 1, 1);
  fout.color.w = wetFactor * PSC_GlassEnvColor.a;

  // Calculate the fog
  fout.fog = input.tPSFog.w;

  // Calculate the land shadow
  fout.landShadow = 1;

  return fout;
}

#define CRATER(x) \
FOut PFCrater##x(SCrater input, bool sbShadows, bool thermal) \
{ \
  return PFCraterX(input, x, sbShadows, thermal); \
} \

CRATER(1)
CRATER(2)
CRATER(3)
CRATER(4)
CRATER(5)
CRATER(6)
CRATER(7)
CRATER(8)
CRATER(9)
CRATER(10)
CRATER(11)
CRATER(12)
CRATER(13)
CRATER(14)

//////////////////////////////////////////////

#define MAX_HALF 65504.0

FOut PFSpriteSimple(SSprite input, bool sbShadows, bool thermal)
{
  FOut fout = InitFOut();

  // Calculate the lights
  fout.light = GetLight(
    input.ambientColor,
    half3(0, 0, 0),
    0);
  fout.light.indirect *= input.modulateColor;
  fout.light.direct *= input.modulateColor;

  // Calculate the z offset to perform the non-linear disappearing
  half4 tex0 = tex2D0(input.tDiffuseMap.xy);

  // Calculate the color
  fout.color = GetColorDiffuse(tex0,input.ambientColor.a * input.modulateColor.a, 0);
  fout.psBlend = true;
  if (fout.color.a<0)
  {
    fout.addBlend = true;
    fout.color.a = -fout.color.a;
  }
  
  // make sure the "color" can be represented as half with no overflow
  fout.color = min(fout.color,MAX_HALF);

  // Calculate the fog
  fout.fog = input.tPSFog.w;

  // Calculate the land shadow
  fout.landShadow = input.specularColor_landShadow.a;

  // Remember the nDotL
  fout.nDotL = 1; // Sprite is a special object - it actually doesn't matter where the sun is shining from

  return fout;
}

//////////////////////////////////////////////

FOut PFSprite(SSprite input, bool sbShadows, bool thermal)
{
  // If we use SM2 and thermal vision, use PFSpriteSimple instead, as we run out of available instructions in that case
#if _SM2
  if (thermal) return PFSpriteSimple(input, sbShadows, thermal);
#endif

  FOut fout = InitFOut();

  // Calculate the lights
  fout.light = GetLight(
    input.ambientColor,
    half3(0, 0, 0),
    0);
  fout.light.indirect *= input.modulateColor;
  fout.light.direct *= input.modulateColor;

  // Calculate the z offset to perform the non-linear disappearing
  half4 tex0 = tex2D0(input.tDiffuseMap.xy);

  // Get the depth buffer value in meters
  half z = 1.0f / tex2D(sampler1, input.tPosition.xy).r;

  // Calculate the alpha coefficient (soft particles)
  //half zOffset = input.tPosition.w * (1.0f - tex0.a);
  //half alphaCoef = saturate((z - (input.tPosition.z + zOffset)) / input.tPosition.w);
  //half alphaCoef = saturate((z - input.tPosition.z - input.tPosition.w * (1.0f - tex0.a)) / input.tPosition.w);
  //half alphaCoef = saturate((z - input.tPosition.z - input.tPosition.w + input.tPosition.w*tex0.a)) / input.tPosition.w);
  //half alphaCoef = saturate((z - input.tPosition.z - input.tPosition.w)) / input.tPosition.w + tex0.a);
  half alphaCoef = saturate((z - input.tPosition.z)/input.tPosition.w + tex0.a);

  // Calculate the color
  fout.color = GetColorDiffuse(tex0,input.ambientColor.a * input.modulateColor.a * alphaCoef, 0);
  fout.psBlend = true;
  if (fout.color.a<0)
  {
    fout.addBlend = true;
    fout.color.a = -fout.color.a;
  }

  // Calculate the fog
  fout.fog = input.tPSFog.w;

  // Calculate the land shadow
  fout.landShadow = input.specularColor_landShadow.a;

  // Remember the nDotL
  fout.nDotL = 1; // Sprite is a special object - it actually doesn't matter where the sun is shining from

  fout.htCoef = 1;

  // The closer is the base color to fire color, the higher the particle color is
  half3 fireColor = half3(1, 0.5, 0);
  half3 fireDiff = (fout.color.rgb - fireColor);
  half3 fireDiff2 = fireDiff * fireDiff;
  half fireCoef = 1.0f - min(sqrt(fireDiff2.r + fireDiff2.g + fireDiff2.b) * 2.0f, 1.0f); // 2 is here, because if only one component of the RGB is away half it's scale, we consider the color as cold
  fout.addTemp = fireCoef * 500.0f; // _VBS3_TI

  // The colder the particle is, the more transparent it appears
  // _VBS3_TI
  if (thermal) fout.color.a = lerp(fout.color.a * 0.1f, fout.color.a, fireCoef);

  return fout;
}

//////////////////////////////////////////////

FOut PFSuper(SSuper input, bool sbShadows, bool thermal)
{
  // Get the LWS position
  float3 lwsPosition = float3(input.stn2lws0.w, input.stn2lws1.w, input.stn2lws2.w);

  // Get the normal in LWS space
  half3 lwsNormal;
  {
    // Get the normal from texture in (STN space)
#if _SM2
    half3 normal = half3(0, 0, 1);
#else
    half4 normal4 = tex2D(sampler1, input.tDiffuseMap_NormalMap.wzyx);
    half3 dxt5FixedNormal = half3(1 - normal4.a, 0, 0) + normal4.rgb;
    half3 normal = /*normalize*/(dxt5FixedNormal * 2 - 1); // No normalization needed, it will be done after transformations
#endif

    // Transform normal to LWS
    lwsNormal.x = dot(input.stn2lws0.xyz, normal);
    lwsNormal.y = dot(input.stn2lws1.xyz, normal);
    lwsNormal.z = dot(input.stn2lws2.xyz, normal);

    // Normalize the result, because the stn2lws may be inaccurate due to per vertex artefacts
    lwsNormal = normalize(lwsNormal);
  }

  // Calculate vector to eye
  float3 lwsEye = normalize(-lwsPosition.xyz);

  // Calculate the halfway vector in LWS space
  float3 lwsHalfway = normalize(lwsEye + PSC_WLight);

  // Calculate the O vector (the reflection vector)
  float3 o = lwsNormal.xyz * dot(lwsNormal.xyz, lwsEye) * 2.0f - lwsEye;

  // Calculate the specular power and environmental map power coef
  // Inner Max is here to avoid zero division
  // Outer Max is here to not to cause aliasing in DDX and DDY (to not to make them smaller than 1)
  half specularPower = PSC_Specular.a * tex2D(sampler5, input.tShadowMap_SpecularMap.wzyx).b;
  half specularPowerEnvCoef = max(500/max(specularPower, 1), 1); // Heuristic conversion of specularPower to env. map. bluring coefficient

  // Get the color from the environmental map
  float2 envT = o.xy * float2(1, -1) * 0.5 + 0.5;
#if _SM2
  half3 envColor = /*tex2D(sampler7, envT) **/ PSC_GlassEnvColor.rgb;
#else
  half3 envColor = tex2Dgrad(sampler7, envT, ddx(envT)*specularPowerEnvCoef, ddy(envT)*specularPowerEnvCoef) * PSC_GlassEnvColor.rgb;
#endif

  // Get the Fresnel reflection coefficient
#if _SM2
  half fresnelCoef = 0.5;
#else
  half fresnelCoef = tex2D(sampler6, float2(dot(lwsNormal, o), 0.5)).a;
#endif

  // Get the specular map content
  half4 specular = tex2D(sampler5, input.tShadowMap_SpecularMap.wzyx);

  // Get the specular coefficient
  half specularCoef = fresnelCoef * specular.g;

  FOut fout = InitFOut();

  // Calculate the lights
  half coefDiffuse;
  fout.light = GetLightSuper(input.ambientColor.rgb,
    lwsNormal, lwsHalfway, envColor, fresnelCoef, specular,
    tex2D(sampler4, input.tShadowMap_SpecularMap.xy),
    coefDiffuse);

//   fout.light.indirect = half3(1, 1, 1);
//   fout.light.direct = half3(0, 0, 0);
//   fout.light.specular = half3(0, 0, 0);
//   fout.light.diffuseShadowCoef = 1;
//   coefDiffuse = 1;


  // Calculate the color
  fout.color = GetColorSuper(
    tex2D0(input.tDiffuseMap_NormalMap.xy),
    tex2D(sampler2, input.tDetailMap_MacroMap.xy),
    tex2D(sampler3, input.tDetailMap_MacroMap.wzyx),
    input.ambientColor.a, specularCoef);
//  fout.color = float4(lwsNormal.rgb, 1);

  // Calculate the fog
  fout.fog = input.tPSFog.w;

  // Calculate the land shadow
  fout.landShadow = input.landShadow.a;

  // Remember the nDotL
  fout.nDotL = coefDiffuse;

  return fout;
}

//////////////////////////////////////////////

FOut PFMulti(SMulti input, bool sbShadows, bool thermal)
{
  FOut fout = InitFOut();

  // Get the mask content
  half3 mask = tex2D(sampler4, input.tMSK_DT0.xy);

//   if (mask.b > 0.5)
//   {
//     mask = float3(0, 0, 1);
//   }
//   else
//   {
//     if (mask.g > 0.5)
//     {
//       mask = float3(0, 1, 0);
//     }
//     else
//     {
//       if (mask.r > 0.5)
//       {
//         mask = float3(1, 0, 0);
//       }
//       else
//       {
//         mask = float3(0, 0, 0);
//       }
//     }
//   }

  // Remember the texture coordinates for each stage (CO and NO textures) - it is a float4 structure, because we use it to select the last mipmap (20) as well
  float4 co0 = float4(input.tCO0_CO1.xy, 0, 20);
  float4 co1 = float4(input.tCO0_CO1.wz, 0, 20);
  float4 co2 = float4(input.tCO2_CO3.xy, 0, 20);
  float4 co3 = float4(input.tCO2_CO3.wz, 0, 20);

  // Remember the texture coordinates for each stage (DT (SMDI) textures)
  float2 dt0 = input.tMSK_DT0.wz;
  float2 dt1 = input.tDT1_DT2.xy;
  float2 dt2 = input.tDT1_DT2.wz;
  float2 dt3 = co3; // We don't have enough room for the last DT coordinate, so we use the same coordinates as in corresponding CO

  // Get the masked CO map
#if _SM2
  half3 mCO = half3(1, 1, 1);
#else
  half3 mCO = tex2D(sampler0, co0.xy).rgb;
  mCO = lerp(mCO, tex2D(sampler1, co1.xy).rgb, mask.r);
  mCO = lerp(mCO, tex2D(sampler2, co2.xy).rgb, mask.g);
  mCO = lerp(mCO, tex2D(sampler3, co3.xy).rgb, mask.b);
#endif

  // Get the masked avg CO map
#if _SM2
  half3 mCOavg = tex2Dbias(sampler0, co0).rgb;
  mCOavg = lerp(mCOavg, tex2Dbias(sampler1, co1).rgb, mask.r);
  mCOavg = lerp(mCOavg, tex2Dbias(sampler2, co2).rgb, mask.g);
  //mCOavg = lerp(mCOavg, tex2Dbias(sampler3, co3).rgb, mask.b);
#else
  half3 mCOavg = tex2Dlod(sampler0, co0).rgb;
  mCOavg = lerp(mCOavg, tex2Dlod(sampler1, co1).rgb, mask.r);
  mCOavg = lerp(mCOavg, tex2Dlod(sampler2, co2).rgb, mask.g);
  mCOavg = lerp(mCOavg, tex2Dlod(sampler3, co3).rgb, mask.b);
#endif

  // Get the masked NO map
#if _SM2
  half4 mNO = half4(0.5, 0.5, 1, 1);
#else
  half4 mNO = tex2D(sampler11, co0);
  mNO = lerp(mNO, tex2D(sampler12, co1), mask.r);
  mNO = lerp(mNO, tex2D(sampler13, co2), mask.g);
  mNO = lerp(mNO, tex2D(sampler14, co3), mask.b);
#endif

  // Get the masked SMDI map
  half4 mSMDI;
#if _SM2
  if (thermal)
  {
    mSMDI = half4(0, 0, 0, 1);
  }
  else
#endif
  {
    mSMDI = tex2D(sampler5, dt0);
    mSMDI = lerp(mSMDI, tex2D(sampler6, dt1), mask.r);
    mSMDI = lerp(mSMDI, tex2D(sampler7, dt2), mask.g);
    mSMDI = lerp(mSMDI, tex2D(sampler8, dt3), mask.b);
  }

  // Sample the macro map
  half4 macro = tex2D(sampler9, input.tMSK_DT0.xy);

  // Modify the base color by macro map (the same turn is used in Terrain shader)
  // See COLOR_MODIFICATION
  half3 adjust = (macro.rgb/mCOavg);
  adjust = min(2,max(adjust,0));
  mCO = lerp(mCO, mCO * adjust, macro.a);

  // Calculate the lights
  half coefDiffuse;
  fout.light = GetLightNormalSpecularDIAS(input.ambientColor,
    mNO, input.tLightLocal.xyz, input.halfway_landShadow.xyz, input.tUpLocal.xyz,
    mSMDI, tex2D(sampler10, input.tMSK_DT0.xy), coefDiffuse);

  // Calculate the color
  fout.color = GetColorDiffuseDetail(half4(mCO, 1), mSMDI.rrr, input.ambientColor.a, 0);

  // Calculate the fog
  fout.fog = input.tPSFog.w;

  // Calculate the land shadow
  fout.landShadow = input.halfway_landShadow.a;

  // Remember the nDotL
  fout.nDotL = coefDiffuse;

  return fout;
}

/////////////////////////////////////////////////////////////////////////////////////////////
// Special pixel shaders - no shadow versions required

half4 PSInterpolation(half4 ambientColor:TEXCOORDAMBIENT,
                      half4 diffuseColor:TEXCOORDDIFFUSE,
                      float2 tDiffuseMap:TEXCOORD0) : COLOR
{
  half4 diffuseMapA = tex2D(sampler0, tDiffuseMap.xy);
  half4 diffuseMapB = tex2D(sampler1, tDiffuseMap.xy);

  // Interpolate between A and B
  half4 diffuse = lerp(diffuseMapB, diffuseMapA, ambientColor.a);

  // reconstruct colors from DXT5 compression
  // do it in such a way that even DXT1 textures work
  half3 diffuseFinal = half3(0, 1 - diffuse.a, 0) + diffuse;

  return half4(ToRTColorSpace((ambientColor.rgb /*+ diffuseColor*/) * diffuseFinal), 1);
}

// Get coefficient teling us how much are to colors similar: 0 - not similar, 1 - equal
// maxDistance - distance that is already considered to be 0 - not similar
half GetColorSimilarityCoef(half3 colorA, half3 colorB, half maxDistance)
{
  half3 colorDiff = (colorA - colorB);
  half3 colorDiff2 = colorDiff * colorDiff;
  return 1.0f - min(sqrt(colorDiff2.r + colorDiff2.g + colorDiff2.b) * 1.0f / maxDistance, 1.0f);
}

half4 PSThermalInterpolation(half4 ambientColor:TEXCOORDAMBIENT,
                             float2 tDiffuseMap:TEXCOORD0) : COLOR
{
  half4 diffuseMapA = tex2D(sampler0, tDiffuseMap.xy);
  half4 diffuseMapB = tex2D(sampler1, tDiffuseMap.xy);

  // Interpolate between A and B
  half4 diffuse = lerp(diffuseMapB, diffuseMapA, ambientColor.a);

  // reconstruct colors from DXT5 compression
  // do it in such a way that even DXT1 textures work
  half3 diffuseFinal = half3(0, 1 - diffuse.a, 0) + diffuse;

  // Get temperature of the air (TC table, WhiteUnlit place, first place in the table)
  float offset = 0.5f / 16.0f;
  half airTemperature = tex2D(samplerTc, float2(0 + offset, 2.0f/16.0f + offset)).r;

  // The closer is the base color to the clear sky color, the lower the temperature is (the minimum in real is 0K, however we use some higher value here (TempMin),
  // because the scale is controlled linearily here and using 0K we would see black almost all the time)
  // The far is the base color to the clear sky color (the more cloudy it is), the more is the temperature similar to the air temperature
  float temperature = lerp(airTemperature, airTemperature - 10, GetColorSimilarityCoef(half3(0, 0.27734375, 0.6328125), diffuseFinal, 0.7));

  // Encode the temperature into RGB
  float3 rgbEncodedTemperature;
  EncodeTemperatureToRGB(temperature, rgbEncodedTemperature.r, rgbEncodedTemperature.g, rgbEncodedTemperature.b);
  return half4(ToRTColorSpace(rgbEncodedTemperature), 1);
}

//////////////////////////////////////////////

float4 PSCloud(half4 ambientColor : TEXCOORDAMBIENT, float4 tDiffuseMap : TEXCOORD0) : COLOR
{
  return ApplySrcAlpha(ToRTColorSpace(tex2D0(tDiffuseMap.xy) * (half4(PSC_AE.rgb + PSC_DForced.rgb, 0) + ambientColor)));
}

//////////////////////////////////////////////

float4 PSHorizon(half4 ambientColor : TEXCOORDAMBIENT, float4 tDiffuseMap : TEXCOORD0) : COLOR
{
  half alpha = tex2D0(tDiffuseMap.xy).a;
  return ApplySrcAlpha(ToRTColorSpace(half4(PSC_FogColor.xyz, alpha)));
  //return half4(PSC_FogColor.rgb,1);
  //return 0;
  //return half4(PSC_FogColor.xyz, tex2D0(tDiffuseMap.xy).a);
  //return 0;
}

//////////////////////////////////////////////

float4 PSWhite() : COLOR
{
  return float4(1, 1, 1, 1);
}

//////////////////////////////////////////////

/// simple pixel shader intended for UI and other 2D stuff
/*
float4 PSNonTL(float4 tDiffuseMap : TEXCOORD0, float4 ambientColor : TEXCOORD6) : COLOR
{
  return tex2DLOD0(tDiffuseMap.xy) * ambientColor;
}
*/

float4 PSNonTL(SNormal input) : COLOR
{
  return ApplySrcAlpha(tex2DLOD0(input.tDiffuseMap.xy) * input.ambientColor);
}

//////////////////////////////////////////////

float4 PSWhiteAlpha(float4 tDiffuseMap:TEXCOORD0) : COLOR
{
  half4 diffuseMap = tex2D(sampler0, tDiffuseMap.xy);
  return ApplySrcAlpha(half4(1, 1, 1, diffuseMap.a));
}

//////////////////////////////////////////////

half4 PSReflect(half4 color : TEXCOORDDIFFUSE, float4 tDiffuseMap : TEXCOORD0) : COLOR
{
  // TEXCOORDDIFFUSE.a should contain shadow-encoded amount of diffuse lighting
  half4 diffuseMap = tex2D(sampler0, tDiffuseMap.xy);
  return ApplySrcAlpha(ToRTColorSpace(half4(diffuseMap.rgb, color.a)));
}

//////////////////////////////////////////////

half4 PSReflectNoShadow(float4 tDiffuseMap : TEXCOORD0) : COLOR
{
  half4 diffuseMap = tex2D(sampler0, tDiffuseMap.xy);
  return half4(diffuseMap.rgb, 1);
}

//////////////////////////////////////////////

float4 PSWater(
half3 specularHalfway  : COLOR0,
half4 tPSFog           : COLOR1,
half4 basicColor:TEXCOORDAMBIENT,
float4 stn2lws2 : TEXCOORDSPECULAR,
float4 stn2lws0 : TEXCOORD4, // TEXCOORD_DIFFUSE_SI,
float4 tWaveMap:TEXCOORD0,
float4 tNormalMap1_2:TEXCOORD1,
half4 tFresnelView:TEXCOORD2,
float2 tNormalMap3:TEXCOORD3,
float4 stn2lws1 : TEXCOORD5
) : COLOR0
{
  //return float4(frac(tNormalMap1_2.z), frac(tNormalMap1_2.w), 0, 1);

  // z is vertical in local space (normal map)
#if _SM2
  half4 normal1 = half4(0.5, 0.5, 1, 1);
  half4 normal2 = half4(0.5, 0.5, 1, 1);
  half4 normal3 = half4(0.5, 0.5, 1, 1);
#else
  half4 normal1 = tex2D(sampler1,tNormalMap1_2.xy);
  half4 normal2 = tex2D(sampler1,tNormalMap1_2.zw);
  half4 normal3 = tex2D(sampler1,tNormalMap3.xy);
#endif

  // Foam
  half foam1 = tex2D(sampler2,tNormalMap1_2.xy).x;
  half foam2 = tex2D(sampler2,tNormalMap1_2.zw*4).x;
  half foam3 = tex2D(sampler2,tNormalMap3.xy*2).x;

  //  // 1- is here so that if foam texture is missing, we get white foam
  //  half foam = saturate(1-(foam1+foam2+foam3-1.5));
  half foam = saturate(foam1 + foam2 + foam3 - 2.1875);

  // x is sum of r + (1-a)
  /*
  // perform DXT5 nomap format 1-a->r swizzle and normalize 
  float4 sumNormals = normal1+normal2+normal3;
  sumNormals.x += (3-sumNormals.a);
  float3 normal = normalize(sumNormals.rgb-1.5);
  */

  // perform DXT5 swizzle and bx2 conversion
  half4 normalSrc = (normal1+normal2+normal3)*0.333f;
  normalSrc.x += (1-normalSrc.a);
  normalSrc.xy -= 0.5;
  // recalculate z based on xy
  //normalSrc.z = sqrt(1-dot(normalSrc.xy,normalSrc.xy));
  normalSrc.z = 0.333;
  //sqrt(1-dot(normalSrc.xy,normalSrc.xy));

  // Calculate normal disappearing factor (normal map disappears close to shore).
  // The same calculation is done on VS to disappear waves normal modification - see label NORMAL_DISAPPEAR
  const half maxDisappear = 0.5f;
  half normalDisappear = min(pow(tWaveMap.z, 10), maxDisappear);
  //normalDisappear = 0;

  half3 normal = normalize(lerp(normalSrc.xyz, half3(0, 0, 1), normalDisappear));

  // Get value from the reflection map
  half3 envColor;
  {
    // Get the LWS position
    float3 lwsPosition = float3(stn2lws0.w, stn2lws1.w, stn2lws2.w);

    // Get the LWS normal
    float3 lwsNormal;
    {
      lwsNormal.x = dot(stn2lws0, normal);
      lwsNormal.y = dot(stn2lws1, normal);
      lwsNormal.z = dot(stn2lws2, normal);
    }

    // Calculate the O vector (the reflection vector)
    float3 o;
    {
      float3 v = normalize(-lwsPosition.xyz);
      o = lwsNormal.xyz * dot(lwsNormal.xyz, v) * 2.0f - v;
    }

    // Modify O vector to favour bounding texels, multiply by constant to omit degenerated texels
    float oSize2 = dot(o.xz,o.xz);
    o.xz = o.xz * oSize2 * 0.95f;

    // Get color from the environmental map
    half3 envColorA = tex2D(sampler4, o.zx * float2(-0.5,+0.5) + 0.5);
    half3 envColorB = tex2D(sampler5, o.zx * float2(-0.5,+0.5) + 0.5);
    envColor = lerp(envColorB, envColorA, tFresnelView.w);
  }

  // Get the fresnel specular coefficient
  half4 fresnelSpecular;
  {
    // Get the specular halfway vector
    specularHalfway.rgb = normalize(specularHalfway.rgb);

    // Get the sample coordinates and sample the reflect map
    half u = dot(normal, normalize(tFresnelView.xyz));
    half v = dot(normal, specularHalfway);
    fresnelSpecular = tex2D(sampler3, half2(u,v)); // Reflect map
  }

  // Sum environmental color and sun specular - result is total reflected color
  half3 reflectedColor = envColor * PSC_GlassEnvColor + fresnelSpecular.rgb * PSC_Specular.rgb;

  half wave = tex2D(sampler0,tWaveMap.xy).x; // Wave map

#if _VBS3
  half3 bodyColor = basicColor;
  half3 color = lerp(bodyColor,reflectedColor,fresnelSpecular.a);
  half alpha = max(basicColor.a, fresnelSpecular.a);
  half4 base = half4(color,alpha);
  float4 result = base+half4(PSC_WaveColor*wave*foam,0.9*wave*foam);
  return ApplySrcAlpha(half4(ToRTColorSpace(ApplyFog(result.rgb, tPSFog.w)),result.a));
#else
  half4 colorBlend = basicColor+half4(PSC_WaveColor*wave*foam,0.9*wave*foam);
  //colorBlend.a *= 1-fresnelSpecular.a;
  half3 colorAdd = reflectedColor*fresnelSpecular.a;

  return ApplySrcAlpha(half4(ToRTColorSpace(ApplyFog(colorBlend.rgb*colorBlend.a +colorAdd.rgb, tPSFog.w)),colorBlend.a));
  //return ApplySrcAlpha(half4(ToRTColorSpace(ApplyFog(result.rgb, tPSFog.w)),result.a));
#endif
}

float4 PSThermalWater(half4 tPSFog : COLOR1) : COLOR0
{
  float3 rgbEncodedTemperature;
  EncodeTemperatureToRGB(15.0f, rgbEncodedTemperature.r, rgbEncodedTemperature.g, rgbEncodedTemperature.b);
  return ApplySrcAlpha(half4(ToRTColorSpace(ApplyFog(half3(rgbEncodedTemperature), tPSFog.w)), 1));
}

//////////////////////////////////////////////

float4 PSWaterSimple( float4 basicColor:TEXCOORDAMBIENT, float3 reflectionHalfway:TEXCOORDSPECULAR,
                     float2 tWaveMap:TEXCOORD0, float2 tNormalMap1:TEXCOORD1,
                     float3 tFresnelView:TEXCOORD2,
                     float3 specularHalfway : COLOR0,
                     half4 tPSFog           : COLOR1,
                     float2 tNormalMap2:TEXCOORD4, float2 tNormalMap3:TEXCOORD3) : COLOR0
{
  // z is vertical in local space (normal map)
  float4 normal1 = tex2D(sampler1,tNormalMap1);
  float4 normal2 = tex2D(sampler1,tNormalMap2);
  float4 normal3 = tex2D(sampler1,tNormalMap3);

  // x is sum of r + (1-a)
  /*
  // perform DXT5 nomap format 1-a->r swizzle and normalize 
  float4 sumNormals = normal1+normal2+normal3;
  sumNormals.x += (3-sumNormals.a);
  float3 normal = normalize(sumNormals.rgb-1.5);
  */

  // perform DXT5 swizzle and bx2 conversion
  float4 normalSrc = (normal1+normal2+normal3)*0.333f;
  normalSrc.x += (1-normalSrc.a);
  normalSrc.xy -= 0.5;
  // recalculate z based on xy
  normalSrc.z = sqrt(1-dot(normalSrc.xy,normalSrc.xy));

  float3 normal = normalSrc.xyz;

  reflectionHalfway = reflectionHalfway*2-1;

  specularHalfway = normalize(specularHalfway);

  float u = dot(normal,tFresnelView);
  float v = dot(normal,specularHalfway);

  float4 fresnelSpecular = tex2D(sampler3,float2(u,v)); // Reflect map

  float cosReflectHalf = max(dot(normal,reflectionHalfway), 0);

  // full ground reflection at value cos(45deg) = 0.70
  // we would like to know cos (2*alfa)
  // cos 2x = 2 cos^2(x) - 1
  float reflected = saturate(2*cosReflectHalf*cosReflectHalf-1);
  float3 reflectedColor = lerp(PSC_GroundReflColor,PSC_SkyReflColor,reflected) ;

  reflectedColor += fresnelSpecular.rgb*PSC_Specular.rgb;

  float wave = tex2D(sampler0,tWaveMap).x; // Wave map
  float3 bodyColor = basicColor;

  float3 color = lerp(bodyColor,reflectedColor,fresnelSpecular.a);


  float alpha = max(basicColor.a, fresnelSpecular.a);

  return ApplySrcAlpha(half4(ToRTColorSpace(ApplyFog(color, tPSFog.w)),alpha));
}

float4 PSThermalWaterSimple(half4 tPSFog : COLOR1) : COLOR0
{
  float3 rgbEncodedTemperature;
  EncodeTemperatureToRGB(15.0f, rgbEncodedTemperature.r, rgbEncodedTemperature.g, rgbEncodedTemperature.b);
  return ApplySrcAlpha(half4(ToRTColorSpace(ApplyFog(half3(rgbEncodedTemperature), tPSFog.w)), 1));
}

//////////////////////////////////////////////

float4 queryDot:register(c7);

/// shader to query any copoment or its combination
float4 PSPostProcessQuery(VSPP4T_OUTPUT input) : COLOR
{
  float value = DecodeApertureChange(tex2D(sampler0, input.TexCoord0).x);
  clip(value-queryDot.w);
  return float4(value.xxx,1);
}

float4 PSPostProcessQuerySimple(VSPP4T_OUTPUT input) : COLOR
{
  float value = tex2D(sampler0, input.TexCoord0).x;
  clip(value-queryDot.w);
  return float4(value.xxx,1);
}

/// RGB eye sensitivity, alpha not used
float4 rgbEyeCoef : register(c26);

/// x: multiplier - absolute level adjustment for rod/cell B/W effect simulation
/// y: addition - offset to allow disabling rods completely
float4 nightControl : register(c28);

half3 ToneMapping(half3 c, half intensity, half3 maxAvgMin)
{
  // apply tone-mapping
  // tone mapping should never extend dynamic range
  // we also do not want to extend it too much, we want to get some overbright
  half oMax = min(max(maxAvgMin.r,1),2);
  // never needed to compress more than necessary
  // we might precalculate some of the following expression in some texture
  return c / (1+c*(oMax-1)/oMax);
  //return c / (1+c);
  //return c;
  //return maxAvgMin;
  //return c/max(maxAvgMin.r,0.01);
}

/// aperture (aperture value is computed on GPU)
float ApertureControl(float texValue)
{
  // sample aperture modification texture
  // value is stored as 0.5x, so that it can be higher than 1
  float apertureMod = DecodeApertureChange(texValue);
  return PSC_HDRMultiply.x*apertureMod;
}

//! Function to return brightness of the color
half Brightness(half3 color)
{
  return dot(color, half3(0.299, 0.587, 0.114));
}

//! Simple pixel shader designed for flare drawing
float4 PSNonTLFlare(SNormal input) : COLOR
{
  // Get the brightness coefficient upon measured visibility of the light source
  half brightness = Brightness(tex2D(samplerLightIntensity, half2(0.5, 0.5)).rgb);
  half bCoef = saturate((brightness - 0.5) * 10.0);
  //half bCoef = (brightness > 0.1) ? 1 : 0;

  // Get the aperture value
  float aperture = ApertureControl(tex2D(sampler14,float2(0,0)).x);

  // Calculate original color and alpha
  half4 cOriginal = tex2D0(input.tDiffuseMap.xy);
  cOriginal.a = cOriginal.a*input.ambientColor.a*bCoef;
  cOriginal.rgb = cOriginal.rgb*input.ambientColor;

  // Include aperture
  half3 cAfterAperture = cOriginal.rgb * aperture;

  // read min/max/avg, transform it into the new color space
  half3 maxAvgMin = tex2D(sampler13,float2(0,0)) * aperture;
  return half4(ToneMapping(cAfterAperture, dot(cAfterAperture, rgbEyeCoef.rgb),maxAvgMin)*cOriginal.a,0);
}
