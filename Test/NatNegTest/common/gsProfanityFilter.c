#include <string.h>
#include "gsPlatform.h"
#include "gsPlatformUtil.h"
#include "gsMemory.h"
#include "gsAssert.h"
#include "gsDebug.h"


static const char *ProfanityList[] =
{
	"",
	"ass",
	"asses",	
	"asshole",
	"assholes",
	"bastard", 
	"beastial",
	"beastiality",
	"beastility",
	"bestial",
	"bestiality",
	"bitch",
	"bitcher",
	"bitchers",
	"bitches",
	"bitchin",
	"bitching",
	"blowjob",
	"blowjobs",
	"clit",
	"cock",
	"cocks",
	"cocksuck", 
	"cocksucked", 
	"cocksucker",
	"cocksucking",
	"cocksucks", 
	"cum",
	"cummer",
	"cumming",
	"cums",
	"cumshot",
	"cunilingus",
	"cunillingus",
	"cunnilingus",
	"cunt",
	"cuntlick", 
	"cuntlicker", 
	"cuntlicking", 
	"cunts",
	"cyberfuck",
	"cyberfucked",
	"cyberfucker",
	"cyberfuckers",
	"cyberfucking", 
	"damn", 
	"dildo",
	"dildos",
	"dink",
	"dinks",
	"ejaculate",
	"ejaculated",
	"ejaculates",
	"ejaculating", 
	"ejaculatings",
	"ejaculation",
	"fag",
	"fagging",
	"faggot",
	"faggs",
	"fagot",
	"fagots",
	"fags",
	"fart",
	"farted", 
	"farting", 
	"fartings", 
	"farts",
	"farty", 
	"felatio", 
	"fellatio",
	"fingerfuck", 
	"fingerfucked", 
	"fingerfucker", 
	"fingerfuckers",
	"fingerfucking", 
	"fingerfucks", 
	"fistfuck",
	"fistfucked", 
	"fistfucker", 
	"fistfuckers", 
	"fistfucking", 
	"fistfuckings", 
	"fistfucks", 
	"fuck",
	"fucked",
	"fucker",
	"fuckers",
	"fuckin",
	"fucking",
	"fuckings",
	"fuckme", 
	"fucks",
	"fuk",
	"fuks",
	"gangbang",
	"gangbanged", 
	"gangbangs", 
	"gaysex", 
	"goddamn",
	"hardcoresex", 
	"hell", 
	"horniest",
	"horny",
	"hotsex",
	"jack-off", 
	"jerk-off", 
	"jism",
	"jiz", 
	"jizm", 
	"kock",
	"kondum",
	"kondums",
	"kum",
	"kummer",
	"kumming",
	"kums",
	"kunilingus",
	"lust",
	"lusting",
	"mothafuck",
	"mothafucka",
	"mothafuckas",
	"mothafuckaz",
	"mothafucked", 
	"mothafucker",
	"mothafuckers",
	"mothafuckin",
	"mothafucking", 
	"mothafuckings",
	"mothafucks",
	"motherfuck",
	"motherfucked",
	"motherfucker",
	"motherfuckers",
	"motherfuckin",
	"motherfucking",
	"motherfuckings",
	"motherfucks",
	"nigger",
	"niggers", 
	"orgasim", 
	"orgasims", 
	"orgasm",
	"orgasms", 
	"phonesex",
	"phuk",
	"phuked",
	"phuking",
	"phukked",
	"phukking",
	"phuks",
	"phuq",
	"piss",
	"pissed",
	"pisser",
	"pissers",
	"pisses", 
	"pissin", 
	"pissing",
	"pissoff", 
	"porn",
	"porno",
	"pornography",
	"pornos",
	"prick",
	"pricks", 
	"pussies",
	"pussy",
	"pussys", 
	"shit",
	"shited",
	"shitfull",
	"shiting",
	"shitings",
	"shits",
	"shitted",
	"shitter",
	"shitters", 
	"shitting",
	"shittings",
	"shitty", 
	"slut",
	"sluts",
	"smut",
	"spunk",
	"twat",
	"",
};
static const ProfanityListLength = DIM(ProfanityList);

int gsProfanityFilterCompareWord(const char *a, const char *b, int *length)
{
	*length = 0;
	while (*a) 
	{
		if (tolower(*a) != tolower(*b))
		{
			if (tolower(*a) < tolower(*b))
				return -1;
			return 1;
		}
		if (*a ==' ')
		{
			if (*b ==' ')
				return 0;
			return -1;
		}
		else
		if (*b ==' ')
			return 1;

		a++;
		b++;
		(*length)++;
	}
	return 0;
}

// returns length of matching word, or 0 if none
size_t gsProfanityFilterFindWord(const char *in)
{
	int L, H;

	GS_ASSERT(in);

	L = 0;
	H = ProfanityListLength - 1;

	while (L <= H)
	{
		int i = (L + H) >> 1;
		int length;
		int c = gsProfanityFilterCompareWord(ProfanityList[i],in, &length);
		if (c == 0)
		{
			return length;
		}
		if (c < 0) 
		{
			L = i + 1;
		}
		else
		{
			H = i - 1;
		}
	}
	return 0;
}


void gsProfanityFilter(const char *in, char *out)
{

	GS_ASSERT(in);
	GS_ASSERT(out);

	strcpy(out,in);
	while(*out)
	{
		size_t len = gsProfanityFilterFindWord(out);
		if (len)
		{
			while (len)
			{
				*out = '*';
				out++;
				len--;
			}

		}
		else
			out++;
	}

}

