
#ifndef _PROFANITY_FILTER_H_
#define _PROFANITY_FILTER_H_

/************************************************************************************************/
/*
		This is a sample utility.

		Recommended usage:
		After obtaining user strings from your GUI code, send it through the profanity filter.
		Pass the resulting string onto any chat or  ??? functions.

		The profanity list used is in no way all inclusive.  We recommend you maintain your own list.

*/
/************************************************************************************************/

#ifdef __cplusplus
extern "C" {
#endif

void gsProfanityFilter(const char *in, char *out);

#ifdef __cplusplus
}
#endif

#endif


