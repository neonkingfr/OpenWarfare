

#include "../natneg.h"
#include "../../common/gsAvailable.h"

#ifdef UNDER_CE
void RetailOutputA(CHAR *tszErr, ...);
#define printf RetailOutputA
#elif defined(_NITRO)
#include "../../common/nitro/screen.h"
#define printf Printf
#define vprintf VPrintf
#endif

struct ClientInfo
{
  struct sockaddr_in otheraddr;
  SOCKET socket;
  int cookie;
};

struct CookieParams
{
  int cookie;    // cookieNr, from -c or -cs cookieNr command line parameter
  int newSocket; // use new socket to this cookie and other "-cs cookieNr" parameter
  int reuseSocket;  // use currently binded socket with this index
  int port;         // local port used for binding
  SOCKET socket;    // final socket used for networking for peers using this cookie to negotiate
};

#define MAX_COOKIES 20
int connected = 0;
struct sockaddr_in otheraddr;
struct ClientInfo otheraddrArray[MAX_COOKIES];
int otheraddrCount = 0;

SOCKET sock = INVALID_SOCKET;
unsigned int ESTRING_SIZE = 128;
char *aString = NULL;
#define USE_OWN_SOCK

#ifdef GSI_COMMON_DEBUG
static void DebugCallback(GSIDebugCategory theCat, GSIDebugType theType,
                          GSIDebugLevel theLevel, const char * theTokenStr,
                          va_list theParamList)
{
  GSI_UNUSED(theLevel);

  printf("[%s][%s] ", 
    gGSIDebugCatStrings[theCat], 
    gGSIDebugTypeStrings[theType]);

  vprintf(theTokenStr, theParamList);
}
#endif

static void pc(NegotiateState state, void *userdata)
{
  if (aString != NULL)
  {
    // Print any status updates to the console
    int charsWritten;

    memset(aString, 0, ESTRING_SIZE);
#pragma warning( push )
#pragma warning( disable: 4996 )
    charsWritten = sprintf(aString, "Got State Update: ");
    switch(state)
    {
    case ns_initack:
      sprintf(aString + charsWritten,  "ns_initack, Init Packets Acknowledged.\n");
      break;
    case ns_connectping:
      sprintf(aString + charsWritten, "ns_connectping, Starting connection and pinging other machine.\n");
      break;
    default:
      break;
    }
#pragma warning( pop )

    gsDebugFormat(GSIDebugCat_App, GSIDebugType_Misc, GSIDebugLevel_Notice,
      aString);
  }

  GSI_UNUSED(userdata);
}

int NewConnectionOccured = 0; //global ... true when new connection was established (NAT traversed)

static void tryread(SOCKET s)
{
  char buf[256];
  int len;
  struct sockaddr_in saddr;
  int saddrlen = sizeof(saddr);
  while (CanReceiveOnSocket(s))
  {
#if defined(_PS3)
    len = recvfrom(s, buf, sizeof(buf) - 1, 0, (struct sockaddr *)&saddr, (socklen_t *)&saddrlen);
#else
    len = recvfrom(s, buf, sizeof(buf) - 1, 0, (struct sockaddr *)&saddr, &saddrlen);
#endif

    if (len < 0)
    {
      len = GOAGetLastError(s);
      printf("|Got recv error: %d\n", len);
      break;
    }
    buf[len] = 0;
    if (memcmp(buf, NNMagicData, NATNEG_MAGIC_LEN) == 0)
    {
      printf("*** NAT Message arrived from %s:%d, length %d bytes\n", inet_ntoa(saddr.sin_addr), ntohs(saddr.sin_port), len);
      NNProcessData(buf, len, &saddr);
    } 
    else printf("|Got data (%s:%d): %s\n", inet_ntoa(saddr.sin_addr),ntohs(saddr.sin_port), buf);
  }
}

static void cc(NegotiateResult result, SOCKET gamesocket, struct sockaddr_in *remoteaddr, void *userdata)
{
  struct sockaddr_in saddr;
  int namelen = sizeof(saddr);
  if (gamesocket != INVALID_SOCKET)
  {

#if defined(_PS3)
    getsockname(gamesocket, (struct sockaddr *)&saddr, (socklen_t *)&namelen);
#else
    getsockname(gamesocket, (struct sockaddr *)&saddr, &namelen);
#endif

    printf("|Local game socket: %d\n", ntohs(saddr.sin_port));
  }

  if (result != nr_success)
  {
    if (aString == NULL)
      return;
    memset(aString, 0, ESTRING_SIZE);
#pragma warning( push )
#pragma warning( disable: 4996 )
    switch(result)
    {
    case nr_deadbeatpartner:
      sprintf(aString,  "Result: nr_deadbeatpartner, The other machine isn't responding.\n");
      break;
    case nr_inittimeout:
      sprintf(aString, "Result: nr_inittimeout, Either the NAT server or other machine could not be contacted.\n");
      break;
    default:
      break;
    }
#pragma warning ( pop )
    gsDebugFormat(GSIDebugCat_App, GSIDebugType_Misc, GSIDebugLevel_Notice,	aString);
    return;
  }

  printf("|Got connected, remoteaddr: %s, remoteport: %d, cookie: %d\n", (remoteaddr == NULL) ? "" : inet_ntoa(remoteaddr->sin_addr), (remoteaddr == NULL) ? 0 : ntohs(remoteaddr->sin_port), ((struct CookieParams*)userdata)->cookie);
  NewConnectionOccured = 1;
  if (result == nr_success)
  {
    connected = 1;
    memcpy(&otheraddr, remoteaddr, sizeof(otheraddr));
    sock = gamesocket;
    memcpy(&(otheraddrArray[otheraddrCount].otheraddr), remoteaddr, sizeof(otheraddr));
    otheraddrArray[otheraddrCount].cookie = ((struct CookieParams *)userdata)->cookie;
    otheraddrArray[otheraddrCount].socket = ((struct CookieParams *)userdata)->socket;
    otheraddrCount++;
  }

  //GSI_UNUSED(userdata);
}

void NNDetectionResultsCallback(gsi_bool success, NAT nat)
{
  char noString[] = "";
  char * natType = noString;
  char *natPromiscuity = noString;
  char *natMappingScheme = noString;
  if (success) 
  {
    printf("NAT detection was successful, details follows...\n");
    if (*nat.brand) printf("   Brand: %s", nat.brand);
    if (*nat.model) printf("   Model: %s", nat.model);
    if (*nat.firmware) printf("   Firmware: %s", nat.firmware);
    switch (nat.natType)
    {
    case no_nat: natType = "no nat"; break;
    case firewall_only: natType = "firewall only"; break;
    case full_cone: natType = "full cone"; break;
    case restricted_cone: natType = "restricted cone"; break;
    case port_restricted_cone: natType = "port restricted cone"; break;
    case symmetric: natType = "symmetric"; break;
    case unknown: 
    default: natType = "unknown";
    }
    printf("   NAT type: %s\n", natType); 
    switch (nat.promiscuity)
    {
    case promiscuous: natPromiscuity = "promiscuous"; break;
    case not_promiscuous: natPromiscuity = "not promiscuous"; break;
    case port_promiscuous: natPromiscuity = "port promiscuous"; break;
    case ip_promiscuous: natPromiscuity = "ip promiscuous"; break;
    case promiscuity_not_applicable:
    default: natPromiscuity = "promiscuity not applicable"; break;
    }
    printf("   NAT promiscuity: %s\n", natPromiscuity);
    switch (nat.mappingScheme)
    {
    case private_as_public: natMappingScheme = "private as public"; break;
    case consistent_port: natMappingScheme = "consistent port"; break;
    case incremental: natMappingScheme = "incremental"; break;
    case mixed: natMappingScheme = "mixed"; break;
    case unrecognized:
    default: natMappingScheme = "unrecognized"; break;
    }
    printf("   NAT mapping scheme: %s\n", natMappingScheme);
    if (nat.ipRestricted) printf("   NAT is IP restricted\n");
    if (nat.portRestricted) printf("   NAT is PORT restricted\n");
    if (nat.qr2Compatible) printf("   NAT is qr2 compatible\n");
    printf("END of NAT detection result.\n");
  }
  else printf("NAT detection was not successful.\n");
}

#ifdef __MWERKS__ // CodeWarrior will warn if not prototyped
int test_main(int argc, char **argp);
#endif
int test_main(int argc, char **argp)
{
  unsigned long lastsendtime = 0;
  GSIACResult result;
  gsi_time startTime;
  NegotiateError error;
  int cookieCount = 0;
  struct CookieParams cookieArray[MAX_COOKIES];
  char woohooMessage[1024];
  int clientNr = 0;
  int i, ix;
  int NatDetectionStarted = 0;
  int processNATDetection = 0;
  int port = 2304;

#ifdef GSI_COMMON_DEBUG
  // Define GSI_COMMON_DEBUG if you want to view the SDK debug output
  // Set the SDK debug log file, or set your own handler using gsSetDebugCallback
  //gsSetDebugFile(stdout); // output to console
  gsSetDebugCallback(DebugCallback);

  // Set debug levels
  //gsSetDebugLevel(GSIDebugCat_All, GSIDebugType_All, GSIDebugLevel_Verbose);
  gsSetDebugLevel(GSIDebugCat_NatNeg, GSIDebugType_All, GSIDebugLevel_Verbose);
#endif

#ifdef GSI_MEM_MANAGED	// use gsi mem managed
  {
#define MEMPOOL_SIZE (8* 1024*1024)
    PRE_ALIGN(16);
    static char _mempool[MEMPOOL_SIZE]	POST_ALIGN(16);
    gsMemMgrCreate	(gsMemMgrContext_Default, "default",
      _mempool, MEMPOOL_SIZE);
  }
#endif

  aString = (char *)gsimalloc(ESTRING_SIZE);
  // check that the game's backend is available
  GSIStartAvailableCheck("gmtest");
  while((result = GSIAvailableCheckThink()) == GSIACWaiting)
    msleep(5);
  if(result != GSIACAvailable)
  {
    printf("The backend is not available\n");
    return 1;
  }

  if (argc>1)
  {
    int _reuseSock = -1;
    for (i=1; i<argc; i++)
    {
#pragma warning( push )
#pragma warning( disable: 4996 )
      int isNewSocket = (stricmp(argp[i],"-cs")==0); // -cs cookieNr ... use new socket
      if (stricmp(argp[i],"-c")==0 || isNewSocket) // -c cookieNr ... reuse last socket
      {
        if (argc>++i)
        {
          cookieArray[cookieCount].cookie = atoi(argp[i]);
          cookieArray[cookieCount].newSocket = isNewSocket;
          cookieArray[cookieCount++].reuseSocket = _reuseSock;
          _reuseSock = -1;
        }
      }
      else if (stricmp(argp[i],"-r")==0) // -r reuseSocketIx
      {
        if (argc>++i)
        {
          _reuseSock = atoi(argp[i]);
        }
      }
      else if (stricmp(argp[i],"-port")==0) // start port definition
      {
        if (argc>++i)
        {
          port = atoi(argp[i]);
        }
      }
      else if (stricmp(argp[i],"-d")==0)
      {
        processNATDetection = 1;
      }
      else clientNr = 1;
#pragma warning ( pop )
    }
  }
  if (!cookieCount) 
  {
    cookieArray[cookieCount].newSocket= 0;
    cookieArray[cookieCount++].cookie = 463141;
  }

#ifdef USE_OWN_SOCK
  {
    // we want to bind it to port 2304 or some other with greater number near this value
    int i, cookieIx;
    int result = SOCKET_ERROR;
    struct sockaddr_in directAddr;
    int _sockets[MAX_COOKIES];
    int _ports[MAX_COOKIES];
    int _socketsIx = 0;
    for (cookieIx=0; cookieIx<cookieCount; cookieIx++)
    {
      if (!cookieIx || cookieArray[cookieIx].newSocket)
      {
        directAddr.sin_family      = AF_INET;
        directAddr.sin_addr.s_addr = INADDR_ANY;
        memset(&(directAddr.sin_zero), '\0', 8); // zero the rest of the struct
        sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
        _sockets[_socketsIx++] = sock;
        for (i=0; i<5; i++, port+= 1) //try at most five port numbers
        {
          int result;
          directAddr.sin_port = htons(port);
          result = bind( sock, (const struct sockaddr *)&directAddr, sizeof( struct sockaddr_in ) );
          if (result != SOCKET_ERROR) 
          { 
            _ports[_socketsIx-1] = port;
            port++; //use next port number for possible next socket
            break; 
          } 
        }
      }
      if (cookieArray[cookieIx].reuseSocket != -1)
      {
        cookieArray[cookieIx].port = _ports[cookieArray[cookieIx].reuseSocket];
        cookieArray[cookieIx].socket = _sockets[cookieArray[cookieIx].reuseSocket]; // reuse socket for this cookie
      }
      else
      {
        cookieArray[cookieIx].port = port-1;
        cookieArray[cookieIx].socket = sock; // socket for this cookie is established
      }
      printf("Cookie: %d will use socket: %d and port:%d\n", cookieArray[cookieIx].cookie, cookieArray[cookieIx].socket, cookieArray[cookieIx].port);
    }
  }
#endif

  for (ix=0; ix<cookieCount; ix++)
  {
    printf("Simple test started for cookie %d, with clientNr %d\n", cookieArray[ix].cookie, clientNr);
    printf("********** Waiting for COOKIE: %d **********\n", cookieArray[ix].cookie);
#ifdef USE_OWN_SOCK
    error = NNBeginNegotiationWithSocket(cookieArray[ix].socket, cookieArray[ix].cookie, clientNr, pc, cc, cookieArray+ix);
#else
    error = NNBeginNegotiation(cookieArray[ix].cookie, clientNr, pc, cc, cookieArray+ix);
#endif

    if(error != ne_noerror)
    {
      int charsWritten;
      memset(aString, 0, ESTRING_SIZE);
#pragma warning( push )
#pragma warning( disable: 4996 )
      charsWritten = sprintf(aString, "Error beginning negotiation: ");
      switch(error)
      {
      case ne_allocerror:
        sprintf(aString + charsWritten, "memory allocation failed.\n");
        break;
      case ne_dnserror:
        sprintf(aString + charsWritten, "DNS lookup failed.\n");
        break;
      case ne_socketerror:
        sprintf(aString + charsWritten, "socket failed to be created.\n");
        break;
      default:
        break;
      }
#pragma warning( pop )

      gsDebugFormat(GSIDebugCat_App, GSIDebugType_Misc, GSIDebugLevel_Notice,	aString);
      gsifree(aString);	
      return 1;
    }
    if (!NatDetectionStarted && processNATDetection)
    {
      (void)NNStartNatDetection(NNDetectionResultsCallback);
      NatDetectionStarted = 1;
    }

    startTime = current_time();
    while ((current_time() - startTime) < 60000) // 1 minute test
    {
      NNThink();
      if (connected)
      {
        if (current_time() - lastsendtime > 2000)
        {
          for (i=0; i<otheraddrCount; i++)
          {
            int ret, error;
#pragma warning( push )
#pragma warning( disable: 4996 )
            sprintf(woohooMessage, "woohoo!! (cookie %d, sock %d)", otheraddrArray[i].cookie, otheraddrArray[i].socket);
#pragma warning( pop )
            ret = sendto(otheraddrArray[i].socket, woohooMessage, strlen(woohooMessage), 0, (struct sockaddr *)&(otheraddrArray[i].otheraddr), sizeof(struct sockaddr_in));
            error = GOAGetLastError(otheraddrArray[i].socket);
            printf("|Sending (%d:%d), remoteaddr: %s, remoteport: %d\n", ret, error, inet_ntoa(otheraddrArray[i].otheraddr.sin_addr), ntohs(otheraddrArray[i].otheraddr.sin_port));
          }
          lastsendtime = current_time();
        }
      }
      for (sock=0, i=0; i<cookieCount; i++)
      {
        if (cookieArray[i].socket != INVALID_SOCKET && cookieArray[i].socket != sock)
        {
          sock = cookieArray[i].socket;
          tryread(sock);
        }
      }
      if (NewConnectionOccured) 
      {
        NewConnectionOccured = 0;
        if (ix<cookieCount-1) break; //try to connect using next cookie in the list (or finish)
      }
      msleep(10);
    }
  }
  for (sock=0, i=0; i<otheraddrCount; i++)
  {
    if (otheraddrArray[i].socket != INVALID_SOCKET && otheraddrArray[i].socket != sock) 
      closesocket(otheraddrArray[i].socket);
  }

  sock = INVALID_SOCKET;
  SocketShutDown();
  NNFreeNegotiateList();
  gsifree(aString);

  GSI_UNUSED(argp);
  return 0;
}
