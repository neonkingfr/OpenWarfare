/**
    @file   test3m.hpp
    @brief  Test program for GNU c++ (abstract interface).
    
    Copyright &copy; 2002 by Josef Pelikan (Josef.Pelikan@mff.cuni.cz)
    @author PE
    @since  9.10.2002
    @date   9.10.2002
*/

#ifndef _TEST3M_HPP
#define _TEST3M_HPP

//-------------------------------------------------------------------------------------
//  abstract interface

class AbstractAPI {

public:

    virtual int f () =0;

    };

//-------------------------------------------------------------------------------------
//  global implementation (selector):

extern AbstractAPI * const globalAPI;

#endif
