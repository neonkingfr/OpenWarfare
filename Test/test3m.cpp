/**
    @file   test3m.cpp
    @brief  Test program for GNU c++ (implementations).
    
    Copyright &copy; 2002 by Josef Pelikan (Josef.Pelikan@mff.cuni.cz)
    @author PE
    @since  9.10.2002
    @date   9.10.2002
*/

#include <time.h>
#include "test3m.hpp"

//-------------------------------------------------------------------------------------
//  implementation:

class SpecialAPI : public AbstractAPI {

public:

    SpecialAPI () {}
    
    virtual int f ()
    { return (int)time(NULL); }
    
    };

//-------------------------------------------------------------------------------------
//  global implementation (selector):

SpecialAPI gAPI;

AbstractAPI * const globalAPI = &gAPI;
