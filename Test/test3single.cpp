/**
    @file   test3.cpp
    @brief  Test program for GNU c++.
    
    Copyright &copy; 2002 by Josef Pelikan (Josef.Pelikan@mff.cuni.cz)
    @author PE
    @since  9.10.2002
    @date   9.10.2002
*/

#include <stdio.h>
#include <time.h>

//-------------------------------------------------------------------------------------
//  abstract interface

class AbstractAPI {

public:

    virtual int f () =0;

    };

//-------------------------------------------------------------------------------------
//  implementation:

class SpecialAPI : public AbstractAPI {

public:

    SpecialAPI () {}
    
    virtual int f ()
    { return (int)time(NULL); }
    
    };

//-------------------------------------------------------------------------------------
//  global implementation (selector):

SpecialAPI gAPI;

AbstractAPI * const globalAPI = &gAPI;

//-------------------------------------------------------------------------------------
//  main routine:

int main ()
{
    int a = globalAPI->f();
    int b = gAPI.f();
    printf("a = %d, b = %d\n",a,b);
    return 0;
}
