#include <windows.h>

#ifdef SINGLE_THREAD

int main ( void )
{
    HANDLE out = CreateFile("test4.txt",GENERIC_WRITE,0,NULL,CREATE_ALWAYS,0,NULL);
    char test[] = "test";
    DWORD written;
    WriteFile(out,test,sizeof(test)-1,&written,NULL);
    CloseHandle(out);
    return 0;
}

#else

static DWORD WINAPI DownloadToFileThread ( void *context )
{
    HANDLE out = CreateFile("test4.txt",GENERIC_WRITE,0,NULL,CREATE_ALWAYS,0,NULL);
    char test[] = "test";
    DWORD written;
    WriteFile(out,test,sizeof(test)-1,&written,NULL);
    CloseHandle(out);
    return 0;
}

int main ( void )
{
    HANDLE handle = CreateThread(NULL,64*1024,DownloadToFileThread,NULL,0,NULL);
    CloseHandle(handle);
    Sleep(100);
    return 0;
}

#endif
