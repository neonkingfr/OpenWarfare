/*
    @file   test.cpp
    @brief  Test program for GNU c++.
    
    Copyright &copy; 2002 by Josef Pelikan (Josef.Pelikan@mff.cuni.cz)
    @author PE
    @since  20.6.2002
    @date   23.7.2002
*/

#include <stdio.h>

//---------------------------------------------------------------------------
//  Support:

class A {

protected:

    int a;
    
public:

    A ();

    virtual int attr () const;

    virtual int pure ( int x ) =0;
            
    virtual ~A ();
    
    };


A::A ()
{
    a = 12;
    printf("A::A()\n");
}

int A::attr () const
{
    printf("A::attr() = %d\n",a);
    return a;
}

A::~A ()
{
    printf("A::~A()\n");
}

class B : public A {

public:

    B ( int x );

    virtual int pure ( int x );

    virtual int more ( int y );
            
    virtual ~B ();
    
    };

B::B ( int x )
{
    a = x;
    printf("B::B(%d)\n",x);
}

int B::pure ( int x )
{
    int old = a;
    a = x;
    printf("B::pure(%d) = %d\n",x,old);
    return old;
}

int B::more ( int y )
{
    int old = a;
    a = y;
    printf("B::more(%d) = %d\n",y,old);
    return old;
}

B::~B ()
{
    printf("B::~B()\n");
}

template <class Type>
class X {

    Type a;
    
public:

    inline X ( Type init )
    {
        a = init;
    }
    
    float error ( const X &x ) const;
    
    };

float X<double>::error ( const X<double> &y ) const
{
    return (float)(a-y.a);
}

//---------------------------------------------------------------------------
//  Main routine:

int main ( int argc, char **argv )
{
    B b(2);
    int result = b.attr();
    printf("b.attr() = %d\n",result);
    result = b.pure(5);
    printf("b.pure(5) = %d\n",result);
    result = b.more(13);
    printf("b.more(13) = %d\n",result);
    A *a = &b;
    result = a->attr();
    printf("a->attr() = %d\n",result);
    result = a->pure(1);
    printf("a->pure(1) = %d\n",result);
      // X test:
    X<double> x(1.0);
    X<double> y(2.0);
    printf("error = %f\n",(double)x.error(y));
    return 0;
}
