#ifdef _MSC_VER
#pragma once
#endif

#ifndef _MULTISYNC_HPP
#define _MULTISYNC_HPP

#ifdef _WIN32

#include <Es/Common/win.h>
#include <Es/Framework/appFrame.hpp>
#include <Es/Threads/pothread.hpp>

enum _noInitHandle {NoInitHandle};

/// Encapsulation of any HANDLE based synchronization object (usable as argument of WaitForXXX)
class SignaledObject : private NoCopy
{
protected:
  mutable HANDLE _handle;

public:
  /// create uninitialized object
  SignaledObject(enum _noInitHandle) : _handle(NULL) {}
  /// create initialized object
  SignaledObject(HANDLE handle) : _handle(handle)
  {
    if (!_handle)
    {
      ErrorMessage("Win32 Error: Cannot create MT object.");
    }
  }
  ~SignaledObject() {Done();}

  bool Init(HANDLE handle)
  {
    DoAssert(!_handle);
    _handle = handle;
    if (!_handle)
    {
      ErrorMessage("Win32 Error: Cannot create MT object.");
      return false;
    }
    return true;
  }
  void Done()
  {
    if (_handle)
    {
      CloseHandle(_handle);
      _handle = NULL;
    }
  }
  
  /// get the handle
  /** sometimes we use SignaledObject to store handle like threads, which we may need to access */
  HANDLE GetHandle() const {return _handle;}


  /// check the initialization state
  operator bool() const {return _handle != NULL;}

  // wait/lock are synonymous
  // do not wait for uninitialized objects
  bool Wait() const {return _handle ? WaitForSingleObject(_handle, INFINITE) == WAIT_OBJECT_0 : false;}
  bool TryWait(DWORD time = 0) const {return _handle ? WaitForSingleObject(_handle, time) == WAIT_OBJECT_0 : false;}
  bool Lock() const {return _handle ? WaitForSingleObject(_handle, INFINITE) == WAIT_OBJECT_0 : false;}
  bool TryLock() const {return _handle ? WaitForSingleObject(_handle, 0) == WAIT_OBJECT_0 : false;}

  static int WaitForMultiple(SignaledObject *events[], int n, int timeout = -1);
};

/// Encapsulation of Event
class Event : public SignaledObject
{
public:
  Event() : SignaledObject(CreateEvent(NULL, FALSE, FALSE, NULL)) {}
  explicit Event(bool manualReset) : SignaledObject(CreateEvent(NULL, manualReset, FALSE, NULL)) {}
  Event(enum _noInitHandle) : SignaledObject(NoInitHandle) {}

  bool Init(bool manualReset = false)
  {
    return SignaledObject::Init(CreateEvent(NULL, manualReset, FALSE, NULL));
  }

  void Set() {if (_handle) SetEvent(_handle);}
  void Reset() {if (_handle) ResetEvent(_handle);}
  void Pulse() {if (_handle) PulseEvent(_handle);}
};

/// Encapsulation of Mutex
class Mutex: public SignaledObject
{
public:
  Mutex() : SignaledObject(CreateMutex(NULL, FALSE, NULL)) {}
  Mutex(enum _noInitHandle) : SignaledObject(NoInitHandle) {}

  bool Init()
  {
    return SignaledObject::Init(CreateMutex(NULL, FALSE, NULL));
  }

  void Unlock() const {if (_handle) ReleaseMutex(_handle);}
  bool IsLocked() const
  {
    bool ret = TryLock();
    if (ret) Unlock();
    return ret;
  }
};

/// Encapsulation of Semaphore
class Semaphore: public SignaledObject
{
  // example: serialize disk access between audio and texture
  // semaphore semantics would be: signaled when disk is busy
  // when texture load receives a request for loading, is Unlocks semaphore
  // (semaphore becomes signaled)
  // after processing request (load or delete), semaphore is Locked
  // and eventually becomes non-signaled
  // audio can check semaphore to see if texture loader uses disk
  // if semaphore is signaled, disk is busy
  
public:
  Semaphore(int init = 0, int max = INT_MAX)
  :SignaledObject(CreateSemaphore(NULL,init,max,NULL))
  {
  }
  Semaphore(enum _noInitHandle) : SignaledObject(NoInitHandle) {}

  bool Init(int init = 0, int max = INT_MAX)
  {
    return SignaledObject::Init(CreateSemaphore(NULL, init, max, NULL));
  }

  bool Unlock(int count = 1) const {return _handle ? ReleaseSemaphore(_handle, count, NULL) != FALSE : true;}
  bool IsLocked() const
  {
    bool ret = TryLock();
    if (ret) Unlock();
    return ret;
  }
};

/// multi threaded synchronization functionality
class CriticalSection
{
private:
  bool _initialized;
  mutable CRITICAL_SECTION _handle;
  
public:
  /// create uninitialized object
  CriticalSection(enum _noInitHandle) {_initialized = false;}
  /// create initialized object
  CriticalSection() {InitializeCriticalSection(&_handle); _initialized = true;}
  /// create initialized object
  CriticalSection(DWORD spinCount) {InitializeCriticalSectionAndSpinCount(&_handle, spinCount); _initialized = true;}
  ~CriticalSection() {Done();}

  void Init()
  {
    DoAssert(!_initialized);
    InitializeCriticalSection(&_handle);
    _initialized = true;
  }
  void Done()
  {
    if (_initialized)
    {
      DeleteCriticalSection(&_handle);
      _initialized = false;
    }
  }

  bool Lock() const
  {
    if (_initialized) 
    { 
      ENTER_DEAD_LOCK_DETECTOR  
      EnterCriticalSection(&_handle);
    }
    return true;
  }
  void Unlock() const
  {
    if (_initialized) 
    {
      LEAVE_DEAD_LOCK_DETECTOR
      LeaveCriticalSection(&_handle);
    }
  }
#ifdef _XBOX
  bool TryLock() const
  {
    return _initialized ? TryEnterCriticalSection(&_handle) != FALSE : true;
  }
#endif
};

#include <Es/Types/scopeLock.hpp>

class ScopeLockMutex: public ScopeLock<Mutex>
{
  public:
  ScopeLockMutex( Mutex &lock )
  :ScopeLock<Mutex>(lock)
  {}
  bool TryLock() const {return _lock.TryLock();}
};

typedef ScopeLock<CriticalSection> ScopeLockSection;

#else

//----------------- POSIX implementation ---------------------

#include "Es/essencepch.hpp"
#include "Es/Common/global.hpp"
#include "Es/Threads/pocritical.hpp"

class SignaledObject
{
  private:
  // no copy
  SignaledObject ( const SignaledObject &src );
  void operator = ( const SignaledObject &src );

  public:
  SignaledObject ()
  {}
  ~SignaledObject()
  {}

  // wait/lock are synonymous
  bool Wait () const
  { return TRUE; }
  
  bool TryWait ( DWORD time=0 ) const
  { return TRUE; }
  
  bool Lock () const
  { return TRUE; }
  
  bool TryLock () const
  { return TRUE; }

  static int WaitForMultiple ( SignaledObject *events[], int n, int timeout=-1 );
};

class Event: public SignaledObject
{
  public:
  Event ()
  {}
  explicit Event ( bool manualReset )
  {}
  void Set ()
  {}
  void Reset ()
  {}
};

class Mutex: public SignaledObject
{
        protected:
  mutable pthread_mutex_t mutex;
  
  public:
  Mutex ()
  {
      mutex = mutexInit;
  }
  void Unlock () const
  {
      pthread_mutex_unlock(&mutex);
  }
  bool IsLocked () const
  {
      bool ret = (pthread_mutex_trylock(&mutex) == 0);
      if ( ret ) Unlock();
      return ret;
  }
};

class Semaphore: public SignaledObject
{
protected:
  mutable sem_t sem;
  // example: serialize disk access between audio and texture
  // semaphore semantics would be: signaled when disk is busy
  // when texture load receives a request for loading, is Unlocks semaphore
  // (semaphore becomes signaled)
  // after processing request (load or delete), semaphore is Locked
  // and eventually becomes non-signaled
  // audio can check semaphore to see if texture loader uses disk
  // if semaphore is signaled, disk is busy

public:
  Semaphore ( int init=0, int max=INT_MAX )
  {
    sem_init(&sem,0,(unsigned)init);
  }
  bool Unlock ( int count=1 ) const
  {
    while ( count-- > 0 )
      sem_post(&sem);
    return TRUE;
  }
  bool IsLocked () const
  {
    int val = 0;
    if ( sem_getvalue(&sem,&val) != 0 ) val = 0;
    return( val != 0 );
  }
};

// multi threaded synchronization functionality
class CriticalSection
{
  private:
  mutable pthread_mutex_t mutex;
  
  public:
  CriticalSection ()
  {
    mutex = mutexInit;
  }
  ~CriticalSection ()
  {
    pthread_mutex_destroy(&mutex);
  }
  bool Lock () const
  {
    pthread_mutex_lock(&mutex);
    return true;
  }
  void Unlock () const
  {
    pthread_mutex_unlock(&mutex);
  }
};

#include <Es/Types/scopeLock.hpp>

class ScopeLockMutex: public ScopeLock<Mutex>
{
  public:
  ScopeLockMutex ( Mutex &lock ) : ScopeLock<Mutex>(lock)
  {}
  bool TryLock() const
  {
      return _lock.TryLock();
  }
};

typedef ScopeLock<CriticalSection> ScopeLockSection;

#endif

#endif
