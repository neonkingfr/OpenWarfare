// SoftLink.cpp : Defines the entry point for the console application.
//

#include <Es/essencepch.hpp>
#include <Es/Types/softLinks.hpp>

// implement linked system

//template TrackSoftLinksWithId<int>;

const int Ids = 100;

class LinkedClass;


class LinkedClassId
{
  int _id;
  public:
  LinkedClassId(){_id=-1;}
  explicit LinkedClassId(int id)
  {
    _id = id;
  }
  int GetIndex() const {return _id;}

  //bool IsEqual(const LinkedClassId &with) const {return with._id == _id;}

  bool operator == (const LinkedClassId &with) const {return with._id == _id;}
  //bool operator != (const LinkedClassId &with) const {return with._id != _id;}
};

class LinkedClass: public RemoveSoftLinks< SoftLinkIdTraits<LinkedClassId> >
{
  LinkedClassId _id;
  int _value;
  int _lock;

  public:
  LinkedClass();

  void SetId(LinkedClassId id) {_id = id;}
  void SetValue(int val) {_value = val;}
  int GetValue() const
  {
    Assert(_lock>=0);
    return _value;
  }

  // soft link support
  typedef LinkedClassId LinkId;

  static LinkedClass *RestoreLink(LinkId id);
  static LinkId GetNullId() {return LinkedClassId();}

  LinkId GetLinkId() const {return _id;}

  void LockLink() {_lock++;}
  void UnlockLink() {_lock--;}
  bool IsLocked() const {return _lock>0;}

  bool NeedsRestoring() {return true;}
};

template <>
FastCAlloc TrackSoftLinks< SoftLinkIdTraits<LinkedClassId> >::_allocatorF = sizeof(TrackSoftLinks);

template <>
Ref< TrackSoftLinks< SoftLinkIdTraits<LinkedClassId> > > TrackSoftLinks<SoftLinkIdTraits<LinkedClassId> >::_nil
  = new TrackSoftLinks(NULL,SoftLinkIdTraits<LinkedClassId>::GetNullId());


Ref<LinkedClass> Loaded[Ids];
bool Created[Ids];

/**/

LinkedClass::LinkedClass()
{
  _id = GetNullId();
  _value = -1;
  _lock = 0;
}


LinkedClass *LoadLinkedClass(LinkedClassId id)
{
  int index = id.GetIndex();
  if (index<0 || index>=Ids) return NULL;

  LinkedClass *cls = new LinkedClass;
  cls->SetId(id);
  cls->SetValue(0x10001*index);
  // lock should be done here
  cls->LockLink();

  return cls;
}

void CreateLinkedClass(int id)
{
  int index = id;
  Loaded[index] = LoadLinkedClass(LinkedClassId(id));
  Created[index] = true;
}

void DeleteLinkedClass(LinkedClass *cls)
{
  int id = cls->GetLinkId().GetIndex();
  cls->SoftLinksDestroy();
  Loaded[id].Free();
  Created[id] = false;
}

void UnloadLinkedClass(LinkedClass *cls)
{
  Assert(!cls->IsLocked());
  int id = cls->GetLinkId().GetIndex();
  Loaded[id].Free();
}

LinkedClass *LinkedClass::RestoreLink(LinkId id)
{
  int index = id.GetIndex();
  if (!Created[index]) return NULL;
  if (Loaded[index]) return Loaded[index];
  return LoadLinkedClass(id);
}

#if 1
  #define LOCK(a) (a).Lock()
  #define UNLOCK(a) (a).Unlock()
#else
  #define LOCK(a)
  #define UNLOCK(a)
#endif


SoftLink<LinkedClass> GL;
SoftLink<LinkedClass> GL3(SoftLinkById,LinkedClassId(4));

int main(int argc, char* argv[])
{
  CreateLinkedClass(1);
  CreateLinkedClass(2);
  CreateLinkedClass(3);

  SoftLink<LinkedClass> l1(SoftLinkById,LinkedClassId(1));
  SoftLink<LinkedClass> l2 = Loaded[2];
  #define L3_ALWAYSLOCKED 1
  #if L3_ALWAYSLOCKED
    SoftLink<LinkedClass, SoftLinkTraitsLocked<LinkedClass> > l3 = Loaded[3];
  #else
    SoftLink<LinkedClass, SoftLinkTraits<LinkedClass> > l3 = Loaded[3];
  #endif

  SoftLink<LinkedClass> l1a;

  SoftLink<LinkedClass> l2a = l2;

  l1a = l1;

  LOCK(l1);
  printf("value %x\n",l1.NotNull() ? l1->GetValue() : 0xffff);
  UNLOCK(l1);

  LOCK(l2);
  printf("value %x\n",l2.NotNull() ? l2->GetValue() : 0xffff);
  UNLOCK(l2);

  LOCK(l3);
  printf("value %x\n",l3.NotNull() ? l3->GetValue() : 0xffff);
  UNLOCK(l3);

  // distinguish: unloading and deleting
  printf("Delete o1\n");
  LOCK(l1);
  DeleteLinkedClass(l1);
  UNLOCK(l1);

  LOCK(GL3);
  UNLOCK(GL3);

  // delete o2
  printf("Delete o2\n");
  DeleteLinkedClass(Loaded[2]);
  // invalidate o3
  printf("Unload o3\n");
  #if !L3_ALWAYSLOCKED
    // note: if link is locked, class cannot be unloaded
    UnloadLinkedClass(Loaded[3]);
    // l3 is now invalid
  #endif

  //LOCK(l1);
  printf("value %x\n",l1.NotNull() ? l1->GetValue() : 0xffff);
  //UNLOCK(l1);

  // o3 should autorestore now, o2 not
  LOCK(l2);
  printf("value %x\n",l2.NotNull() ? l2->GetValue() : 0xffff);
  UNLOCK(l2);

  #if !L3_ALWAYSLOCKED
    LOCK(l3);
  #endif
  printf("value %x\n",l3.NotNull() ? l3->GetValue() : 0xffff);
  #if !L3_ALWAYSLOCKED
    UNLOCK(l3);
  #endif

	return 0;
}
