#include <Es/essencepch.hpp>
#include <Es/ErrorProp/errorProp.hpp>
#include <stdio.h>

void ErrorHere()
{
  ERROR_THROW(ErrorInfoText,"This is an error")
  ERROR_THROW_NOVAL(ErrorInfo)
}

void NestLevel3()
{
  ErrorHere();
}

void NestLevel2()
{
  ERROR_TRY()
  NestLevel3();
  ERROR_END()
}

void NestLevel1()
{
  NestLevel2();
}


int main()
{
  ERROR_TRY()
    ERROR_TRY()
      NestLevel1();
    ERROR_CATCH(ErrorInfoText,text)
    {
      printf("Known error encountered: %s\n",text->GetText());
    }
    ERROR_CATCH_ANY(exc)
    {
      printf("Unknown error encountered\n");
      ERROR_RETHROW(exc)
    }
    ERROR_END()
  ERROR_CATCH_ANY(exc)
  {
    printf("Unhandled error\n");
  }
  ERROR_END()
  printf("Press return to continue...\n");
  getchar();
  return 0;
}
