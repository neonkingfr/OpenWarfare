#ifdef _MSC_VER
#pragma once
#endif

#ifndef __SUMA_CLASS_SORTED_ARRAY_HPP
#define __SUMA_CLASS_SORTED_ARRAY_HPP

#include <Es/Containers/array.hpp>
#include <Es/Algorithms/qsort.hpp>
#include <Es/Algorithms/bSearch.hpp>

template <class Type>
struct SortedArrayTraits
{
	typedef const Type &KeyType;

	//! get key
	static KeyType GetKey(const Type &type) {return type;}
	//! compare two key values
	static int Compare(KeyType a, KeyType b)
	{
		if (a>b) return +1;
		if (a<b) return -1;
		return 0;
	}
};

/// array in which items are stored sorted
/**
This makes searching for item possible in O(log n) instead of O(n)
*/
template <class Type,class Allocator=MemAllocD,class Traits=SortedArrayTraits<Type> >
class SortedArray: private AutoArray<Type,Allocator>
{
	typedef typename Traits::KeyType KeyType;
	typedef AutoArray<Type,Allocator> base;

	public:
	SortedArray();

  //! Add item without sorting.
	//! Sort is required before any function that require searching may be used.
	//! (Add, AddUnique, Find, FindKey, Delete, DeleteKey)
	void AddDirty(const Type &src) {base::Add(src);}

  //! Adding of an item with resizing the field.
	int Add(const Type &src);
	//! add item if item with the same key does not exist
	void AddUnique(const Type &src)
	{
		if( Find(src)>=0 ) return false;
		Add(src);
		return true;
	}
	//! replace value with another value with the same key
	void Replace(int index, const Type &src)
	{
		Assert(Traits::Compare(Traits::GetKey(Get(index)),Traits::GetKey(src)));
		base::_data[index] = src;
	}
	//! find if item already exists
	int Find( const Type &src ) const
	{
		return FindKey(Traits::GetKey(src));
	}
	//! find if key already exists
	int FindKey(KeyType key) const;

	//! Delete given element
	bool Delete(const Type &src)
	{
		return DeleteKey(Traits::GetKey(src));
	}
	//! Delete given element
	bool DeleteKey(KeyType src);

  //! Delete item at given position
  void DeleteAt(int indx) {return base::Delete(indx);};

  //! Get item from given position
  const Type& Get(int indx) const {return base::Get(indx);};

  /// Get item from given position
  /** Using this function is potentially dangerous.
  Make sure you do not change the key value while changing the item.
  */
  Type& GetMutable(int indx) {return base::Set(indx);};

	//! allow access to selected functions from base class interface
	__forceinline const Type *Data() const {return base::Data();}
	__forceinline int Size() const {return base::Size();}
	__forceinline const Type &operator [] (int i) const {return base::Get(i);}
	__forceinline void Compact() {base::Compact();}
	__forceinline void Clear() {base::Clear();}
	__forceinline void Realloc(int sizeNeed, int sizeWant)
	{
		base::Realloc(sizeNeed,sizeWant);
	}
	__forceinline void Realloc(int size){base::Realloc(size);}
	__forceinline void Reserve(int sizeNeed, int sizeWant)
	{
		base::Reserve(sizeNeed,sizeWant);
	}

  __forceinline double GetMemoryUsed() const {return base::GetMemoryUsed();};  

	//! sort array after adding elements with AddDirty
	void Sort();

  

	ClassIsMovable(SortedArray);
};

template<class Type,class Allocator, class Traits>
SortedArray<Type,Allocator,Traits>::SortedArray()
{
}

template<class Type,class Allocator, class Traits>
bool SortedArray<Type,Allocator,Traits>::DeleteKey(KeyType key)
{
	int index=FindKey(key);
	if( index<0 ) return false;
	base::Delete(index);
	return true;
}

//! functor - to be used as QSort parameter
template <class Type,class Traits>
struct SortedArrayCompare
{
	int operator () (const Type *a, const Type *b)
	{
		// use Traits compare
		return Traits::Compare(Traits::GetKey(*a),Traits::GetKey(*b));
	}
};

template <class Type, class Traits=SortedArrayTraits<Type> >
struct CompareWithKey
{
	int operator () (typename Traits::KeyType b, const Type &a)
	{
		return Traits::Compare(b,Traits::GetKey(a));
	}
};

template<class Type,class Allocator, class Traits>
int SortedArray<Type,Allocator,Traits>::Add( const Type &src )
{
	KeyType key = Traits::GetKey(src);
	int index = BSearchPos
	(
		Data(),Size(),CompareWithKey<Type,Traits>(),key
	);
	// verify found position is correct
	#if _DEBUG
		Assert(index<=Size());
		Assert(index>=0);
		if (index==Size())
		{
			Assert(Size()==0 || Traits::Compare(key,Traits::GetKey(Get(Size()-1)))>0);
		}
		else
		{
			Assert(Traits::Compare(key,Traits::GetKey(Get(index)))<=0);
			Assert(index<=0 || Traits::Compare(key,Traits::GetKey(Get(index-1)))>=0);
		}
	#endif
	base::Insert(index,src);
  return index;
}

template<class Type,class Allocator, class Traits>
void SortedArray<Type,Allocator,Traits>::Sort()
{
	// perform quick sort on whole array
	SortedArrayCompare<Type,Traits> compare;
	QSort(base::Data(),Size(),compare);
}

template<class Type,class Allocator, class Traits>
int SortedArray<Type,Allocator,Traits>::FindKey(KeyType key) const
{
	return BSearch
	(
		Data(),Size(),CompareWithKey<Type,Traits>(),key
	);
}

/// traits for key comparison and searching
template <class Type>
struct BinFindArrayKeyTraits
{
  typedef const Type &KeyType;
  /// check if two keys are equal
  static bool IsEqual(KeyType a, KeyType b) {return a==b;}
  /// check if one key is lower than the other
  static bool IsLess(KeyType a, KeyType b) {return a<b;}
  /// get a key from an item
  static KeyType GetKey(const Type &a) {return a;}
};


//! array with searching based on key - binary search used
template <class Type, class Traits=BinFindArrayKeyTraits<Type>, class Allocator=MemAllocD>
class BinFindArrayKey: public AutoArray<Type,Allocator>
{
  typedef typename Traits::KeyType KeyType;
  typedef AutoArray<Type,Allocator> base;

  public:
  /// if it already exists, return existing entry. If not, create a new one
  int FindOrAdd( const Type &src );
  //! add unless there already is an item with the same key
  int AddUnique( const Type &src );
  //! find first item with the same key
  int Find( const Type &src ) const {return FindKey(Traits::GetKey(src));}
  //! find item by key
  int FindKey(KeyType key) const;
  //! find suitable position for a key, report if exact match was found
  int FindKeyPos(KeyType key, bool &found) const;
  //! delete first item with the same key
  bool Delete( const Type &src ){return DeleteKey(Traits::GetKey(src));}
  //! delete first item with given key
  bool DeleteKey(KeyType src);
  void DeleteAt( int index ) {AutoArray<Type,Allocator>::Delete(index,1);}
  void DeleteAt( int index, int count ) {AutoArray<Type,Allocator>::Delete(index,count);}

  #if _MSC_VER>=1300
  /// AddUnique all elements from another array
  template <class ArrayType>
  void MergeUnique(const ArrayType &src)
  {
    for (int i=0; i<src.Size(); i++)
    {
      AddUnique(src[i]);
    }
  }
  #endif
  
  ClassIsMovable(BinFindArrayKey);
};

template<class Type,class Traits,class Allocator>
int BinFindArrayKey<Type,Traits,Allocator>::FindKeyPos(KeyType key, bool &found) const
{
  // based on http://www.dcc.uchile.cl/~rbaeza/handbook/algs/3/321.srch.c
  // see also http://www.nist.gov/dads/HTML/binarySearch.html

  // if the new value is outside of the existing range, return Size()
  if(base::Size()==0 || Traits::IsLess(Traits::GetKey(base::_data[base::Size()-1]),key))
  {
    #if _DEBUG
      for (int i=0; i<base::Size(); i++)
      {
        Assert(!Traits::IsEqual(Traits::GetKey(base::Get(i)),key));
      }
    #endif
    found = false;
    return base::Size();
  }

  if(Traits::IsLess(key,Traits::GetKey(base::_data[0])))
  {
    #if _DEBUG
      for (int i=0; i<Size(); i++)
      {
        Assert(!Traits::IsEqual(Traits::GetKey(Get(i)),key));
      }
    #endif
    found = false;
    return 0;
  }

  int high, low; // high / low not inclusive
  for ( low = -1, high = base::Size();  high-low > 1;  )
  {
    int i = (high+low) / 2;
    if (  !Traits::IsLess(Traits::GetKey(base::_data[i]),key) )  high = i;
    else low  = i;
  }
  found = Traits::IsEqual(key,Traits::GetKey(base::_data[high]) );
  #if _DEBUG
    if (!found)
    {
      for (int i=0; i<Size(); i++)
      {
        Assert(!Traits::IsEqual(Traits::GetKey(Get(i)),key));
      }
    }
  #endif
  return high;
}

template<class Type,class Traits,class Allocator>
int BinFindArrayKey<Type,Traits,Allocator>::FindKey(KeyType key) const
{
  bool found;
  int index = FindKeyPos(key,found);
  if (found) return index;
  return -1;
}

template<class Type,class Traits,class Allocator>
int BinFindArrayKey<Type,Traits,Allocator>::AddUnique( const Type &src )
{
  bool found;
  int pos = FindKeyPos(src,Traits::GetKey(src));
  if (found) return -1;
  return Insert(pos,src);
}

template<class Type,class Traits,class Allocator>
int BinFindArrayKey<Type,Traits,Allocator>::FindOrAdd( const Type &src )
{
  bool found;
  int pos = FindKeyPos(src,Traits::GetKey(src));
  if (found) return pos;
  return Insert(pos,src);
}

template<class Type,class Traits,class Allocator>
bool BinFindArrayKey<Type,Traits,Allocator>::DeleteKey(KeyType key)
{
  int index=FindKey(key);
  if( index<0 ) return false;
  DeleteAt(index);
  return true;
}


#endif
