#ifdef _MSC_VER
#pragma once
#endif

#ifndef __ES_QUAD_TREE_HPP
#define __ES_QUAD_TREE_HPP

/**
@file
quadtree with overlapping borders
*/

#include <Es/essencepch.hpp>
#include <Es/Memory/memAlloc.hpp>
#include <Es/Types/pointers.hpp>
#include <Es/Containers/array.hpp>
#include <Es/Strings/bString.hpp>

struct OffTreeRegion
{
  float _begX,_endX;
  float _begZ,_endZ;
  
  OffTreeRegion(){}
  OffTreeRegion(float begX, float begZ, float endX, float endZ)
  :_begX(begX),_begZ(begZ),_endX(endX),_endZ(endZ)
  {
  }
  
  bool IsIntersection(const OffTreeRegion &reg) const
  {
    return(
      floatMax(reg._begX,_begX)<floatMin(reg._endX,_endX) &&
      floatMax(reg._begZ,_begZ)<floatMin(reg._endZ,_endZ)
    );
  }
};
TypeIsSimple(OffTreeRegion)

/// interface between OffTree items and OffTreeRegion
template <class Type, class Allocator=MemAllocD>
struct OffTreeRegionTraits
{
  /// get corresponding region  
  static const OffTreeRegion &GetRegion(const Type &item)
  {
    return item.GetRegion();
  }

  static bool IsIntersection(const Type &item, const OffTreeRegion &q)
  {
    return item.IsIntersection(q);
  }
  
  static bool IsEqual(const Type &item1, const Type &item2)
  {
    return item1==item2;
  }
};

/// implement query statistics
template <class Type>
struct OffTreeStatTraits: public OffTreeRegionTraits<Type>
{
  typedef AutoArray<Type> Container;
  
  static float GetMaxBorder() {return FLT_MAX;}
  static float GetRelBorder() {return 0.10f;}

  struct QueryResult
  {
    int regionTests;
    int nodeTests;
    
    QueryResult(){regionTests = 0;nodeTests = 0;}
    /// stats are easy to merge
    void operator += (const QueryResult &b)
    {
      regionTests += b.regionTests;
      nodeTests += b.nodeTests;
    }
  };


  /// perform a check against all regions
  static bool PerformQuery(QueryResult &res, const OffTreeRegion &q, const Container &regions)
  {
    res.regionTests += regions.Size();
    res.nodeTests += 1;
    return false;
  }
};

/// implement real query
template <class Type, class Allocator=MemAllocD>
struct OffTreeDefTraits: public OffTreeRegionTraits<Type, Allocator>
{
  typedef AutoArray<Type, Allocator> Container;
  
  static float GetMaxBorder() {return FLT_MAX;}
  static float GetRelBorder() {return 0.10f;}

  struct QueryResult
  {
    Container regions;
    
    QueryResult(){}
    /// merging implemented, but it can be quite slow
    void operator += (const QueryResult &b)
    {
      regions.Merge(b.regions);
    }
  };

  /// perform a check against all regions
  static bool PerformQuery(QueryResult &res, const OffTreeRegion &q, const Container &items)
  {
    for (int i=0; i<items.Size(); i++)
    {
      const Type &item = items[i];
      if (IsIntersection(item,q))
      {
        res.regions.Add(item);
      }
    }
    // by default search for all elements matching the query
    return false;
  }
  static void SetStorage(const Container &container) {}
};

/// traits for OffTree property implementation
template <class Type, class Allocator=MemAllocD>
struct OffTreeTraits: public OffTreeDefTraits<Type, Allocator>
{
};


/// one level of offset tree

template <class Type, class Traits=OffTreeTraits<Type>, class Allocator=MemAllocD >
class OffTree;

template <class Type, class Traits, class Allocator>
struct SRefAllocTraits
{
  static void Destruct(Allocator &alloc, Type *type)
  {
    Delete(alloc,type);
  }
};

template <
  class Type, class Traits, class Allocator,
  class AllocTraits=SRefAllocTraits<Type,Traits,Allocator>
>
class SRefAlloc: private NoCopy
{
  Type *_ref;
  
  public:
  SRefAlloc() {_ref = NULL;}
  explicit SRefAlloc(Allocator &alloc)
  :_ref(AllocatorNew<Type>(alloc))
  {}
  void New(Allocator &alloc)
  {
    Assert(!_ref);
    _ref = AllocatorNew<Type>(alloc);
  }
  void Free(Allocator &alloc)
  {
    if (_ref)
    {
      AllocTraits::Destruct(alloc,_ref);
      _ref = NULL;
    }
  }
  ~SRefAlloc()
  {
    Assert(!_ref);
  }
  Type *operator ->() const {return _ref;}
  bool IsNull() const {return _ref==NULL;}
  bool NotNull() const {return _ref!=NULL;}
};

template <class Type, class Traits, class Allocator>
class OffTreeLevel;

//template <class Type, class Traits, class Allocator>
//struct SRefAllocTraits< TypeOffTreeLevel<Type,Traits,Allocator> >

template <class Type, class Traits, class Allocator>
struct SRefAllocTraits< OffTreeLevel<Type,Traits,Allocator>, Traits, Allocator >
{
  static void Destruct(Allocator &alloc, OffTreeLevel<Type,Traits,Allocator> *type)
  {
    type->Free(alloc);
    AllocatorDelete(alloc,type);
  }
};

/// quad-tree with overlapping borders
template <class Type, class Traits, class Allocator>
class OffTreeLevel
{
  //@{ simple access to traits
  typedef typename Traits::QueryResult QueryResult;
  typedef typename Traits::Container Container;
  static float GetMaxBorder() {return Traits::GetMaxBorder();}
  static float GetRelBorder() {return Traits::GetRelBorder();}
  //@}
  
  friend class OffTree<Type,Traits,Allocator>;
  /// children
  SRefAlloc<OffTreeLevel,Traits,Allocator> _child[2][2];
  /// list of items contained here
  Container _regions;
  
  #if _DEBUG
  /// count items in this branch
  int _regionsInBranch;
  #endif
  
  /// check if branch is empty - such branch should be removed
  bool IsEmpty() const 
  {
    return (
      _regions.Size()==0 &&
      _child[0][0].IsNull() && _child[0][1].IsNull() &&
      _child[1][0].IsNull() && _child[1][1].IsNull()
    );
  }
  public:
  OffTreeLevel()
  {
//    _child[0][0] = NULL;
//    _child[0][1] = NULL;
//    _child[1][0] = NULL;
//    _child[1][1] = NULL;
    #if _DEBUG
      _regionsInBranch = 0;
    #endif
  }
  /// add a new item into a corresponding location
  void Add(
    const Type &item, float startX, float startZ, float size, OffTree<Type,Traits,Allocator> &alloc
  );
  /// find and remove the item
  int Remove(
    const Type &item, float startX, float startZ, float size, Allocator &alloc
  );
  /// find all items in given region
  bool Query(
    QueryResult &res, const OffTreeRegion &q,
    float startX, float startZ, float size
  ) const;

  void Dump(int level, float startX, float startZ, float size) const;
  
  void Free(Allocator &alloc)
  {
    _child[0][0].Free(alloc);
    _child[0][1].Free(alloc);
    _child[1][0].Free(alloc);
    _child[1][1].Free(alloc);
  }
};



template <class Type,class Traits,class Allocator>
void OffTreeLevel<Type,Traits,Allocator>::Add(
  const Type &item, float startX, float startZ, float size,
  OffTree<Type,Traits,Allocator> &alloc
)
{
  #if _DEBUG
  _regionsInBranch++;
  #endif
  Assert(size>0);
  // check if we can subdivide even more
  float halfSize = size*0.5f;
  float border = floatMin(GetRelBorder()*halfSize,GetMaxBorder());
  float begX0 = startX-border;
  float endX0 = startX+halfSize+border;
  float begX1 = startX+halfSize-border;
  float endX1 = startX+size+border;
  float begZ0 = startZ-border;
  float endZ0 = startZ+halfSize+border;
  float begZ1 = startZ+halfSize-border;
  float endZ1 = startZ+size+border;
  // left child: start...mid
  // right child: mid...start+size
  // we can be part of child if: child.beg<=beg && child.end>=end
  // if both contain us, prefer the one containing us "more"
  float edgeDistX0 = -1;
  float edgeDistX1 = -1;
  float edgeDistZ0 = -1;
  float edgeDistZ1 = -1;
  const OffTreeRegion &reg = Traits::GetRegion(item);
  if (reg._begX>=begX0 && reg._endX<=endX0)
  {
    edgeDistX0 = floatMin(reg._begX-begX0,endX0-reg._endX);
  }
  if (reg._begX>=begX1 && reg._endX<=endX1)
  {
    edgeDistX1 = floatMin(reg._begX-begX1,endX1-reg._endX);
  }
  if (reg._begZ>=begZ0 && reg._endZ<=endZ0)
  {
    edgeDistZ0 = floatMin(reg._begZ-begZ0,endZ0-reg._endZ);
  }
  if (reg._begZ>=begZ1 && reg._endZ<=endZ1)
  {
    edgeDistZ1 = floatMin(reg._begZ-begZ1,endZ1-reg._endZ);
  }
  if (edgeDistZ0>edgeDistZ1)
  {
    if (edgeDistX0>edgeDistX1)
    {
      // right child is better
      // if it does not exist, create it
      if (_child[0][0].IsNull()) _child[0][0].New(alloc);
      _child[0][0]->Add(item,startX,startZ,halfSize,alloc);
      return;
    }
    else if (edgeDistX1>=0)
    {
      if (_child[1][0].IsNull()) _child[1][0].New(alloc);
      _child[1][0]->Add(item,startX+halfSize,startZ,halfSize,alloc);
      return;
    }
  }
  else if (edgeDistZ1>=0)
  {
    if (edgeDistX0>edgeDistX1)
    {
      // right child is better
      // if it does not exist, create it
      if (_child[0][1].IsNull()) _child[0][1].New(alloc);
      _child[0][1]->Add(item,startX,startZ+halfSize,halfSize,alloc);
      return;
    }
    else if (edgeDistX1>=0)
    {
      if (_child[1][1].IsNull()) _child[1][1].New(alloc);
      _child[1][1]->Add(item,startX+halfSize,startZ+halfSize,halfSize,alloc);
      return;
    }
  }
  
  // we need to be placed here
  if (_regions.Size()==0) alloc.SetStorage(_regions);
  _regions.Add(item);
}

template <class Type,class Traits,class Allocator>
int OffTreeLevel<Type,Traits,Allocator>::Remove(
  const Type &item, float startX, float startZ, float size,
  Allocator &alloc
)
{
  Assert(size>0);
  // check if we can subdivide even more
  float halfSize = size*0.5f;
  float border = floatMin(GetRelBorder()*halfSize,GetMaxBorder());
  float begX0 = startX-border;
  float endX0 = startX+halfSize+border;
  float begX1 = startX+halfSize-border;
  float endX1 = startX+size+border;
  float begZ0 = startZ-border;
  float endZ0 = startZ+halfSize+border;
  float begZ1 = startZ+halfSize-border;
  float endZ1 = startZ+size+border;
  // left child: start...mid
  // right child: mid...start+size
  // we can be part of child if: child.beg<=beg && child.end>=end
  // if both contain us, prefer the one containing us "more"
  float edgeDistX0 = -1;
  float edgeDistX1 = -1;
  float edgeDistZ0 = -1;
  float edgeDistZ1 = -1;
  const OffTreeRegion &reg = Traits::GetRegion(item);
  if (reg._begX>=begX0 && reg._endX<=endX0)
  {
    edgeDistX0 = floatMin(reg._begX-begX0,endX0-reg._endX);
  }
  if (reg._begX>=begX1 && reg._endX<=endX1)
  {
    edgeDistX1 = floatMin(reg._begX-begX1,endX1-reg._endX);
  }
  if (reg._begZ>=begZ0 && reg._endZ<=endZ0)
  {
    edgeDistZ0 = floatMin(reg._begZ-begZ0,endZ0-reg._endZ);
  }
  if (reg._begZ>=begZ1 && reg._endZ<=endZ1)
  {
    edgeDistZ1 = floatMin(reg._begZ-begZ1,endZ1-reg._endZ);
  }
  if (edgeDistZ0>edgeDistZ1)
  {
    if (edgeDistX0>edgeDistX1)
    {
      // right child is better
      if (_child[0][0].IsNull()) return 0;
      int ret = _child[0][0]->Remove(item,startX,startZ,halfSize,alloc);
      #if _DEBUG
        _regionsInBranch -= ret;
      #endif
      if (_child[0][0]->IsEmpty())
      {
        _child[0][0].Free(alloc);
      }
      return ret;
    }
    else if (edgeDistX1>=0)
    {
      if (_child[1][0].IsNull()) return 0;
      int ret = _child[1][0]->Remove(item,startX+halfSize,startZ,halfSize,alloc);
      #if _DEBUG
        _regionsInBranch -= ret;
      #endif
      if (_child[1][0]->IsEmpty())
      {
        _child[1][0].Free(alloc);
      }
      return ret;
    }
  }
  else if (edgeDistZ1>=0)
  {
    if (edgeDistX0>edgeDistX1)
    {
      // right child is better
      if (_child[0][1].IsNull()) return 0;
      int ret = _child[0][1]->Remove(item,startX,startZ+halfSize,halfSize,alloc);
      #if _DEBUG
        _regionsInBranch -= ret;
      #endif
      if (_child[0][1]->IsEmpty())
      {
        _child[0][1].Free(alloc);
      }
      return ret;
    }
    else if (edgeDistX1>=0)
    {
      if (_child[1][1].IsNull()) return 0;
      int ret = _child[1][1]->Remove(item,startX+halfSize,startZ+halfSize,halfSize,alloc);
      #if _DEBUG
        _regionsInBranch -= ret;
      #endif
      if (_child[1][1]->IsEmpty())
      {
        _child[1][1].Free(alloc);
      }
      return ret;
    }
  }
  
  // we need to be placed here
  int src, dst;
  for (src=0,dst=0; src<_regions.Size(); src++)
  {
    if (!Traits::IsEqual(_regions[src],item))
    {
      _regions[dst++] = _regions[src];
    }
  }
  _regions.Resize(dst);
  #if _DEBUG
    _regionsInBranch -= src-dst;
  #endif
  return src-dst;
}


template <class Type,class Traits, class Allocator>
bool OffTreeLevel<Type,Traits,Allocator>::Query(
  QueryResult &res, const OffTreeRegion &q,
  float startX, float startZ, float size
) const
{
  //Log("Query %g,%g - %d: %g,%g,%g",begX,begZ,level,startX,startZ,size);
  // check intersection with children
  if (Traits::PerformQuery(res,q,_regions))
  {
    return true;
  }
  // check if we can subdivide even more
  float halfSize = size*0.5f;
  float border = floatMin(GetRelBorder()*halfSize,GetMaxBorder());
  float begX0 = startX-border;
  float endX0 = startX+halfSize+border;
  float begX1 = startX+halfSize-border;
  float endX1 = startX+size+border;
  float begZ0 = startZ-border;
  float endZ0 = startZ+halfSize+border;
  float begZ1 = startZ+halfSize-border;
  float endZ1 = startZ+size+border;
  // left child: start...mid
  // right child: mid...start+size
  // we can be part of child if: child.beg<=beg && child.end>=end
  // if both contain us, prefer the one containing us "more"

  if (floatMax(q._begZ,begZ0)<floatMin(q._endZ,endZ0))
  {
    if (_child[0][0].NotNull() && floatMax(q._begX,begX0)<floatMin(q._endX,endX0))
    {
      bool done = _child[0][0]->Query(res,q,startX,startZ,halfSize);
      if (done) return done;
    }
    if (_child[1][0].NotNull() && floatMax(q._begX,begX1)<floatMin(q._endX,endX1))
    {
      bool done = _child[1][0]->Query(res,q,startX+halfSize,startZ,halfSize);
      if (done) return done;
    }
  }
  if (floatMax(q._begZ,begZ1)<floatMin(q._endZ,endZ1))
  {
    if (_child[0][1].NotNull() && floatMax(q._begX,begX0)<floatMin(q._endX,endX0))
    {
      bool done = _child[0][1]->Query(res,q,startX,startZ+halfSize,halfSize);
      if (done) return done;
    }
    if (_child[1][1].NotNull() && floatMax(q._begX,begX1)<floatMin(q._endX,endX1))
    {
      bool done = _child[1][1]->Query(res,q,startX+halfSize,startZ+halfSize,halfSize);
      if (done) return done;
    }
  }
  return false;
}

template <class Type,class Traits, class Allocator>
void OffTreeLevel<Type,Traits,Allocator>::Dump(int level, float startX, float startZ, float size) const
{
  BString<256> indent;
  for(int i=0; i<level; i++) strcat(indent,"  ");
  #if _DEBUG
  LogF(
    "%sNode %d: %g,%g..%g,%g (%g) - %d regions",
    cc_cast(indent),
    level,startX,startZ,startX+size,startZ+size,size,_regionsInBranch
  );
  #endif
  for (int i=0; i<_regions.Size(); i++)
  {
    const OffTreeRegion &r = _regions[i];
    Log(
      "%s  region %g,%g..%g,%g (%g,%g)",
      cc_cast(indent),
      r._begX,r._begZ,r._endX,r._endZ,r._endX-r._begX,r._endZ-r._begZ
    );
  }
  float halfSize = size*0.5f;
  if (_child[0][0])
  {
    _child[0][0]->Dump(level+1,startX,startZ,halfSize);
  }
  if (_child[1][0])
  {
    _child[1][0]->Dump(level+1,startX+halfSize,startZ,halfSize);
  }
  if (_child[0][1])
  {
    _child[0][1]->Dump(level+1,startX,startZ+halfSize,halfSize);
  }
  if (_child[1][1])
  {
    _child[1][1]->Dump(level+1,startX+halfSize,startZ+halfSize,halfSize);
  }
}

/// offset tree root
template <class Type, class Traits, class Allocator>
class OffTree: public Allocator, public Traits
{
  public:
  //@{ simple access to traits
  typedef typename Traits::QueryResult QueryResult;
  typedef typename Traits::Container Container;
  static float GetMaxBorder() {return Traits::GetMaxBorder();}
  static float GetRelBorder() {return Traits::GetRelBorder();}
  //@}
  
  private:
  
  SRefAlloc< OffTreeLevel<Type,Traits,Allocator>, Traits, Allocator > _root;
  //@{ span of the whole tree
  float _startX,_startZ,_size;
  //@}

  public:
  OffTree(float startX=0, float startZ=0, float size=1);
  /// add item to the tree
  void Add(const Type &item);
  /// remove all occurences of the item from the tree
  int Remove(const Type &item);
  /// find items in given area
  bool Query(QueryResult &res, float begX, float begZ, float endX, float endZ) const;
  void Dump() const;
  /// erase data, keep dimensions intact
  void Clear()
  {
    _root.Free(*this);
    _root.New(*this);
  }
  /// erase data, change dimensions
  void Dim(float startX, float startZ, float size)
  {
    _startX = startX;
    _startZ = startZ;
    _size = size;
    _root.Free(*this);
    _root.New(*this);
  }
  ~OffTree()
  {
    _root.Free(*this);
  }
};

#pragma warning(disable:4355)
template <class Type, class Traits, class Allocator>
OffTree<Type,Traits,Allocator>::OffTree(float startX, float startZ, float size)
:_startX(startX),_startZ(startZ),_size(size),
_root(*this)
{
}

template <class Type, class Traits, class Allocator>
void OffTree<Type,Traits,Allocator>::Add(const Type &item)
{
  // find where should we add
  Assert(Traits::GetRegion(item)._begX>=_startX);
  Assert(Traits::GetRegion(item)._endX<=_startX+_size);
  Assert(Traits::GetRegion(item)._begX<Traits::GetRegion(item)._endX);
  Assert(Traits::GetRegion(item)._begZ<Traits::GetRegion(item)._endZ);
  _root->Add(item,_startX,_startZ,_size,*this);
}

/**
@return how many times item was found
*/
template <class Type, class Traits, class Allocator>
int OffTree<Type,Traits,Allocator>::Remove(const Type &item)
{
  return _root->Remove(item,_startX,_startZ,_size,*this);
}

template <class Type, class Traits, class Allocator>
bool OffTree<Type,Traits,Allocator>::Query(QueryResult &res, float begX, float begZ, float endX, float endZ) const
{
  if (begX<=_startX) begX = _startX;
  if (endX>=_startX+_size) endX = _startX+_size;
  if (begZ<=_startZ) begZ = _startZ;
  if (endZ>=_startZ+_size) endZ = _startZ+_size;
  OffTreeRegion q(begX,begZ,endX,endZ);
  return _root->Query(res,q,_startX,_startZ,_size);
}

template <class Type, class Traits, class Allocator>
void OffTree<Type,Traits,Allocator>::Dump() const
{
  LogF("**** %g,%g,%g",_startX,_startZ,_size);
  _root->Dump(0,_startX,_startZ,_size);
}

#endif
