#if _MSC_VER>=1100
#pragma once
#endif

#ifndef __BIG_ARRAY_HPP
#define __BIG_ARRAY_HPP

// Big array no longer separate implementation - use traits for Array2D
#include <Es/Containers/array2D.hpp>

#endif

