#ifdef _MSC_VER
#pragma once
#endif

#ifndef _LLINKS_HPP
#define _LLINKS_HPP

#include <Es/Types/pointers.hpp>
#include <Es/Memory/fastAlloc.hpp>
#include <Es/Containers/array.hpp>

class TrackLLinks;
class RemoveLLinks;

class RemoveLLinks: public RefCount
{
	friend class TrackLLinks;
	mutable TrackLLinks *_track;

	public:
	RemoveLLinks() {_track=NULL;}
	RemoveLLinks( const RemoveLLinks &src ){_track=NULL;}
	void operator =( const RemoveLLinks &src ){_track=NULL;}

	/// make sure all existing links pointing to this object are set to NULL
  inline void DestroyLinkChain() const;
  /// check no links exist - useful for asserting in caching scenarios
  inline bool CheckNoLinks() const;

	~RemoveLLinks()
	{
	  DestroyLinkChain();
	}

	__forceinline TrackLLinks *Track() const {return _track;}
};

#include <Es/Memory/normalNew.hpp>

class TrackLLinks: public RefCount
{
	friend class RemoveLLinks;

	mutable RemoveLLinks *_remove;

	public:
	TrackLLinks()
	{
	}
	explicit TrackLLinks( RemoveLLinks *obj )
	{
		_remove=obj;
		if( obj )
		{
			Assert( !obj->_track );
			obj->_track=this;
		}
	}

	~TrackLLinks()
	{
		if( _remove ) _remove->_track=NULL;
		_remove=NULL;
	}

	__forceinline RemoveLLinks *GetObject() const {return _remove;}

	USE_FAST_ALLOCATOR_ID(TrackLLinks)
};

#include <Es/Memory/debugNew.hpp>

inline void RemoveLLinks::DestroyLinkChain() const
{
	if( _track ) _track->_remove=NULL;
	_track=NULL;
}
inline bool RemoveLLinks::CheckNoLinks() const
{
  return _track==NULL;
}

#if _DEBUG
	#define COUNT_LLINKS 1
#else
	#define COUNT_LLINKS 0
#endif

extern Ref<TrackLLinks> LLinkNil;

template <class Type>
struct DefLLinkTraits
{
  static __forceinline TrackLLinks *Track(Type *ref) {return ref->Track();}
  static __forceinline RemoveLLinks *GetBase(Type *ref) {return ref;}
  static __forceinline Type *GetObject(TrackLLinks *tracker) {return static_cast<Type *>(tracker->GetObject());}
};

template <class Type, class Traits = DefLLinkTraits<Type> >
class LLink
{
	mutable Ref<TrackLLinks> _tracker;
	#if COUNT_LLINKS
		static int _notNullInstanceCount;
		static int _instanceCount;
	#endif

	public:
  //! make data type accessible
  typedef Type ItemType;

	LLink() : _tracker(LLinkNil)
	{
		#if COUNT_LLINKS
		_instanceCount++;
		#endif
	}
	LLink(const Ref<Type> &ref)
  :_tracker(ref.NotNull() ? (Traits::Track(ref) ? Traits::Track(ref) : new TrackLLinks(Traits::GetBase(ref.GetRef()))) : (TrackLLinks*)LLinkNil)
	{
		#if COUNT_LLINKS
		if (ref.NotNull())
		{
			_notNullInstanceCount++;
		}
		_instanceCount++;
		#endif
	}
	LLink( Type *ref )
	:_tracker(ref ? (Traits::Track(ref) ? Traits::Track(ref) : new TrackLLinks(Traits::GetBase(ref))) : (TrackLLinks*)LLinkNil)
	{
		#if COUNT_LLINKS
		if (ref)
		{
			_notNullInstanceCount++;
		}
		_instanceCount++;
		#endif
	}
	#if COUNT_LLINKS
	LLink(const LLink &src)
	{
		_tracker = src._tracker;
		if (_tracker != LLinkNil)
		{
			_notNullInstanceCount++;
		}
		_instanceCount++;
	}
	const LLink &operator =(const LLink &src)
	{
		if (_tracker != LLinkNil)
		{
			_notNullInstanceCount--;
		}
		_tracker = src._tracker;
		if (_tracker != LLinkNil)
		{
			_notNullInstanceCount++;
		}
		return *this;
	}
	~LLink()
	{
		if (_tracker != LLinkNil)
		{
			_notNullInstanceCount--;
		}
		_instanceCount--;
	}
	#endif

	__forceinline Type *GetLink() const
	{
    return Traits::GetObject(_tracker);
	}
	__forceinline operator Type *() const {return GetLink();}
	__forceinline Type * operator ->() const {return GetLink();}

	__forceinline bool NotNull() const {return Traits::GetObject(_tracker) != NULL;}
	__forceinline bool IsNull() const {return Traits::GetObject(_tracker) == NULL;}

	ClassIsMovable(LLink);
};

#define TypeContainsLLink TypeIsMovable
#define ClassContainsLLink ClassIsMovable

#if COUNT_LLINKS

template <class Type, class Traits>
int LLink<Type,Traits>::_notNullInstanceCount=0;

template <class Type, class Traits>
int LLink<Type,Traits>::_instanceCount=0;

#endif

template <class Type>
struct LLinkArrayKeyTraits
{
	typedef const Type *KeyType;
	static bool IsEqual(KeyType a, KeyType b)
	{
		return a==b;
	}
	static KeyType GetKey(const LLink<Type> &a) {return a.GetLink();}
};

//! array of LLink-s
template <class Type,class Allocator=MemAllocD>
class LLinkArray: public FindArrayKey< LLink<Type>, LLinkArrayKeyTraits<Type>, Allocator >
{
	typedef FindArrayKey< LLink<Type>, LLinkArrayKeyTraits<Type>, Allocator > base;
	typedef LLinkArrayKeyTraits<Type> traits;

	public:
	int Count() const;
	
	public:
	/// delete any NULL entries
	void RemoveNulls();

	//! delete at given location
	__forceinline void Delete( int index ) {base::DeleteAt(index);}
	//! delete at given location, given number of items
  __forceinline void Delete( int index, int count ) {base::DeleteAt(index,count);}
	//! delete, given pointer
	__forceinline bool Delete(const Type *src) {return DeleteKey(src);}
	//! delete, given Link
	__forceinline bool Delete(const LLink<Type> &src) const {return DeleteKey(traits::GetKey(src));}

	//! if link is provided, get a key from it
	__forceinline int Find(const LLink<Type> &src) const {return FindKey(traits::GetKey(src));}
	//! if pointer is provided to search, avoid temporary Link creation
	__forceinline int Find(const Type *src) const {return FindKey(src);}

  /// add, reuse any NULL slots if possible
  int AddReusingNull(Type *item)
  {
    int free = base::FindKey(NULL);
    if (free>=0)
    {
      base::Set(free) = item;
      return free;
    }
    else
    {
      return Add(item);
    }
  }
  
	ClassIsMovable(LLinkArray)
};

template <class Type,class Allocator>
int LLinkArray<Type,Allocator>::Count() const
{
	int i, n = 0;
	for (i=0; i<base::Size(); i++)
		if (base::Get(i)!=NULL) n++;
	return n;
}

template <class Type, class Allocator>
void LLinkArray<Type,Allocator>::RemoveNulls()
{
	int d=0;
	for( int s=0; s<base::Size(); s++ )
	{
		if( base::Get(s)!=NULL )
		{
			if( s!=d ) base::Set(d)=base::Get(s);
			d++;
		}
	}
	base::Resize(d);
}

#endif


