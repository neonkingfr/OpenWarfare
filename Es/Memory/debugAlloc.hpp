#ifdef _MSC_VER
#pragma once
#endif

#ifndef _ES__DEB_ALLOC_HPP
#define _ES__DEB_ALLOC_HPP

/*!
\file
Allocator for arrays used for debugging.
*/

/// similiar to MemAllocD, but always using system level allocation

class DebugAlloc
{
  public:
	#if ALLOC_DEBUGGER
	static void *Alloc( int &size, const char *file, int line, const char *postfix );
	#else
	static void *Alloc( int &size );
	#endif
	static void Free( void *mem, int size );
	static void *Realloc(void *mem, int oldSize, int size);
	static void Unlink( void *mem );
  static inline int MinGrow() {return 32;}

};

#endif
