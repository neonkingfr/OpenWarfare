#ifdef _MSC_VER
#  pragma once
#endif

/**
@file   C++ Languauge extenstions
*/

#ifndef _ES_LANG_EXT_HPP
#define _ES_LANG_EXT_HPP

/// cast using implicit conversions only
template <class To,class From>
inline To safe_cast( const From &from ) {return from;}

/// specialization to avoid C4800 warning
template <>
inline bool safe_cast( const int &from ) {return from!=0;}

/// cast to const char * using implicit conversions only
#define cc_cast(from) ( (const char *)(from) )

/// remove const-ness from a pointer
template <class Type>
__forceinline Type *unconst_cast(const Type *ptr)
{
  return const_cast<Type *>(ptr);
}

#if !defined _MSC_VER || _MSC_VER>=1300
/// remove const-ness from a reference
template <class Type>
__forceinline Type &unconst_cast(const Type &ptr)
{
  return const_cast<Type &>(ptr);
}
#endif

/// get length of an array
#define lenof(x) (sizeof(x)/sizeof(*(x)))

/// compile time comparing of 2 constants
template <int A, int B>
class CompileTimeCompare
{
  /// If you get an error message on the following line then some constants doesn't match
  enum {test=1/int(A==B)};
};
#define COMPILETIME_COMPARE(a,b) static CompileTimeCompare< (a), (b)> test;

template <int A, int B>
class CompileTimeCompareIsLessOrEqual
{
  /// If you get an error message on the following line then some constants doesn't match
  enum {test=1/int(A<=B)};
};
#define COMPILETIME_COMPARE_ISLESSOREQUAL(a,b) static CompileTimeCompareIsLessOrEqual< (a), (b)> test;

/// allow initialization using a different type
template <class Type, class InitType=Type, InitType defaultValue=0>
class InitValT
{
public:
  typedef Type ValueType;
private:
  ValueType _value;
public:
  // This is the point of the exercise: provide a default constructor
  __forceinline InitValT():_value(Type(defaultValue)) {}

  // These make it close to interchangeable with T
  __forceinline InitValT(const InitValT &other) :_value(other._value){}
  __forceinline InitValT(const ValueType &initialValue):_value(initialValue){}
  __forceinline const InitValT & operator = (const InitValT &other) { _value = other._value;return *this;}
  __forceinline const InitValT & operator = (const ValueType &newValue) {_value = newValue;return *this;}
  
  __forceinline operator const ValueType &() const { return _value; }
  __forceinline operator ValueType &() { return _value; }

  // And these are useful sometimes
  __forceinline const ValueType &get() const {return _value;}
  __forceinline void set(const ValueType &newValue) {_value = newValue;}
};

/// variable with given initialization value
template <class Type, Type defaultValue=0>
class InitVal
{
public:
  typedef Type ValueType;
private:
  ValueType _value;
public:
  // This is the point of the exercise: provide a default constructor
  __forceinline InitVal():_value(defaultValue) {}

  // These make it close to interchangeable with T
  __forceinline InitVal(const InitVal &other) :_value(other._value){}
  __forceinline InitVal(const ValueType &initialValue):_value(initialValue){}
  __forceinline const InitVal & operator = (const InitVal &other) { _value = other._value;return *this;}
  __forceinline const InitVal & operator = (const ValueType &newValue) {_value = newValue;return *this;}
  
  __forceinline operator const ValueType &() const { return _value; }
  __forceinline operator ValueType &() { return _value; }

  // And these are useful sometimes
  __forceinline const ValueType &get() const {return _value;}
  __forceinline void set(const ValueType &newValue) {_value = newValue;}
};

/// check in the compile time (Debug configuration) scalars with different units (for example indices to different structures)
#if _DEBUG
template <typename Base>
class Derived
{
private:
  Base _val;

public:
  operator Base() const {return _val;}
  operator Base &() {return _val;}
  explicit Derived(Base val) {_val = val;}
  /// having default constructor as uninitialized is a little bit unsafe, but performant
  Derived(){}
};

/// convenience wrapper to Derived template
#define UNIQUE_TYPE(name, base) class name : public Derived<base> \
{ \
public: \
  explicit name(base val) : Derived<base>(val){} \
  name(){} \
}
#else
#define UNIQUE_TYPE(name, base) typedef base name
#endif

/// by inheriting from this class copy constructor and assignment is disabled
class NoCopy
{
public:
  NoCopy(){} // we do not want to disable default constructor

private:
  NoCopy(const NoCopy &src);
  NoCopy &operator =( const NoCopy &src);
};

#endif

