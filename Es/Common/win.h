#ifndef _EL_WIN_H
#define _EL_WIN_H

#define WIN32_LEAN_AND_MEAN // no OLE or other complicated stuff

#ifdef _XBOX

  // use SSE optimized matrix math
  #define _USE_XGMATH 1
	// note: Verify is function name in XBox Direct3D, but we use it as macro
  #undef Verify
  #include <Es/Memory/normalNew.hpp> // some X360 libraries override new/delete
  #include <xtl.h>
  #include <xonline.h>
  #include <Es/Memory/debugNew.hpp>
  #ifdef NDEBUG
    #define Verify( expr ) (expr)
  #else
    #define Verify( expr ) DoAssert(expr)
  #endif

#elif defined(_WIN32)

	#if defined _WINDOWS_ && !defined _MFC_VER
		#error Windows.h used not through Es/Common/win.h in non-MFC application.
	#endif

  #ifndef _WINDOWS_ // in MFC applications we cannot prevent windows.h to be included before
    #define _WIN32_WINNT 0x0501 // some WinXP features are used
    #define STRICT 1
    #include <windows.h>
    #if _GAMES_FOR_WINDOWS
      #include <Es/Memory/normalNew.hpp> // some GfW libraries override new/delete
      #include <winlive.h>
      #include <Es/Memory/debugNew.hpp>
    #endif
  #endif

  // various CPU intrinsics - intelocked operations, barriers, ...
  #include <intrin.h>

#else

  #undef Verify
  #define Verify( expr ) (expr)

#endif

// #undef SearchPath
// #undef LoadString
#undef DrawText
#undef GetObject
#undef GetMessage

#endif
