#include "RefCount.hpp"
#include "LexicalAnalyser.hpp"
#include "Config.hpp"


#ifndef _SYNTAX_ANYALISER
#define _SYNTAX_ANYALISER

class SyntaxAnyaliser : public RefCount
{
  SmartRef<LexicalAnalyser> _lexier;
   
  bool Expect(GTokenEnum expected);

  SmartRef<ConfigEntry>    ParseFields();
  SmartRef<ConfigEntry>    ParseClass();
  SmartRef<ConfigEntry>    ParseEntry();
  SmartRef<IConfigType>    ParseArray();
  SmartRef<ConfigBaseType> ParseExpression();
  SmartRef<ConfigBaseType> ParseTimesDivide();
  SmartRef<ConfigBaseType> ParseFactor();
public:

  // To add more config settings, just add more LexicalAnalyised files.
  bool Parse(LexicalAnalyser *lexier,ParamaFileConfig *configFile);
};

#endif