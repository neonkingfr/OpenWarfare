#ifndef __STDAFX_H__
#define __STDAFX_H__

#include <stdlib.h>
#include <stdio.h>
#include <memory.h>
#include <assert.h>
#include <math.h>

#ifdef _DEBUG
#define ASSERT(a) assert(a);
#else
#define ASSERT(a) 
#endif

#include "MyArray.h"

#define FLOATTOINT(a) (int) ((a)+0.5f)
#define min(a,b) (((a) < (b)) ? (a) : (b))
#define max(a,b) (((a) > (b)) ? (a) : (b))

#define for(a) if (1) for(a)

#include <El/elementpch.hpp>

#endif //__STDAFX_H__