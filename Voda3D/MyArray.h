/* Auxiliary array, later it will be replaced by some shared one. */
#ifndef __MYARRAY_H__
#define __MYARRAY_H__

template <class Type> // BE WARE, It assumes that Type is simple (memcpy can be used).
class MyArray
{
protected:
	unsigned int _nSize;
	unsigned int _nAllocatedSize;
	Type * _pField;
public:

	MyArray(): _nSize(0), _pField(NULL), _nAllocatedSize(0) {};
	MyArray(MyArray& cArray);
	~MyArray() { delete [] _pField;};

	int Add(const Type &cItem);
	int Add(const MyArray<Type>& cArray);
	int GetItem(Type * &pcItem, unsigned int iIndex) const;
	unsigned int GetSize() const {return _nSize;};
	void Release();
	int PrepareSpace(unsigned int nAllocatedSize);
	void MoveDown(unsigned int iFromIndex, unsigned int iToIndex);
	void MoveUp(unsigned int iFromIndex, unsigned int iToIndex);

	const MyArray<Type>& operator=(const MyArray<Type>& cArray);
	Type& operator[](unsigned int iIndex) const {ASSERT(_nSize > iIndex); return _pField[iIndex];};
};

template <class Type> 
MyArray<Type>::MyArray(MyArray& cArray)
{ 
	_nSize = cArray._nSize;
    _nAllocatedSize = _nSize;
	if (_nSize > 0)
	{
		_pField = new Type[_nSize]; // TODO: how to get error out.		
		memcpy(_pField, cArray._pField, _nSize * sizeof(Type));		
	}
}

template <class Type> 
const MyArray<Type>& MyArray<Type>::operator=(const MyArray<Type>& cArray)
{
	Release();
	PrepareSpace(cArray._nSize);

	_nSize = cArray._nSize;
	
	if (_nSize > 0)
	{
		memcpy(_pField, cArray._pField, _nSize * sizeof(Type));
	}

	return *this;
}

template <class Type> 
int MyArray<Type>::Add(const Type &cItem)
{
	_nSize++;
	if (_nSize > _nAllocatedSize)
	{
		_nAllocatedSize = _nSize;
		Type *pNewField = new Type[_nSize];

		if (pNewField == NULL)
			return -1;

		if (_pField != NULL)
		{
			memcpy(pNewField, _pField, (_nSize - 1) * sizeof(Type));
			
			delete [] _pField;
		}
		_pField = pNewField;
	}

	_pField[_nSize - 1] = cItem;
	
	return 1;
}

template <class Type> 
int MyArray<Type>::Add(const MyArray<Type>& cArray)
{
	PrepareSpace(_nSize + cArray._nSize);

	memcpy(_pField + _nSize, cArray._pField, cArray._nSize * sizeof(Type));
	_nSize += cArray._nSize;

	return 1;
}

template <class Type> 
int MyArray<Type>::GetItem(Type * &pcItem, unsigned int iIndex) const
{
	if (iIndex >= _nSize)
		return -1;

	pcItem = _pField + iIndex;
	return 1;
}

template <class Type> 
void MyArray<Type>::Release()
{
	_nSize = 0;
}

template <class Type> 
int MyArray<Type>::PrepareSpace(unsigned int nAllocatedSize)
{
	if (_nAllocatedSize>= nAllocatedSize)
		return 1;

	_nAllocatedSize = nAllocatedSize;
	Type *pNewField = new Type[nAllocatedSize];

	if (pNewField == NULL)
		return -1;

	if (_pField != NULL)
	{
		memcpy(pNewField, _pField, _nSize * sizeof(Type));

		delete [] _pField;
	}

	_pField = pNewField;

	return 1;
}

template <class Type>
void MyArray<Type>::MoveDown(unsigned int iFromIndex, unsigned int iToIndex)
{
	ASSERT(iFromIndex > iToIndex);
	ASSERT(iFromIndex < _nSize);

	Type Temp = _pField[iFromIndex];
	memmove(_pField + iToIndex + 1, _pField + iToIndex, (iFromIndex - iToIndex) * sizeof(Type));
	_pField[iToIndex] = Temp;
}

template <class Type>
void MyArray<Type>::MoveUp(unsigned int iFromIndex, unsigned int iToIndex)
{
	ASSERT(iFromIndex < iToIndex);
	ASSERT(iToIndex < _nSize);

	Type Temp = _pField[iFromIndex];
	memmove(_pField + iFromIndex, _pField + iFromIndex + 1, (iToIndex - iFromIndex) * sizeof(Type));
	_pField[iToIndex] = Temp;
}

#endif // __MYARRAY_H__