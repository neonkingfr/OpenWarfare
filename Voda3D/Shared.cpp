#include "stdafx.h"
#include "shared.h"

// for loadpbl
#include <El/ParamFile/paramFile.hpp>
#include <extern/libpng/include/png.h>

void CCountry::Init(unsigned int nSizeX, unsigned int nSizeY, float fStepX, float fStepY, int origX, int origY)
{
  _fStepX = fStepX;
  _fStepY = fStepY;

  _originX = origX;
  _originY = origY;

  g_cCoordConverter.SetSimStepX(fStepX);
  g_cCoordConverter.SetSimStepY(fStepY);

  MyArray2D<float>::Init( nSizeX, nSizeY);
}

void CCountry::Load(const char * pszFileName)
{
    
    // Brehy
  
 /*      for (int j = 0; j < _nSizeY ; j++) {
            for (int i = 0; i < _nSizeX; i++) {
                _pItems[j * _nSizeX + i] = ((j - 10) * (j - 10) /16 + (_nSizeY - (j - 10)) * (_nSizeY - (j - 10)) / 16);
                //_pItems[j * _nSizeX + i] = ((j) * (j) /16 + (_nSizeY - (j)) * (_nSizeY - (j)) / 16);
                _pItems[j * _nSizeX + i] *= -1.0f;

            }
        }

        
             /*  for (int j =  _nSizeX / 2; j < _nSizeY; j++) {
                    for (int i = 0; i < _nSizeX ; i++) {
                        _pItems[j * _nSizeX + i] = (_nSizeX - j) * 3;
                    }
                }*/
        

    /*
    
      for ( j = 0; j < _nSizeY; j++)
      _pItems[j * _nSizeX ] = _pItems[j * _nSizeX + _nSizeX - 1] = 1000.0f;
      
        
          for ( j = 0; j < _nSizeY; j++)
    _pItems[j] = _pItems[(_nSizeY - 1) * _nSizeX + j] = 1000.0f;*/
    
				
    // Ryha
    
    
    /*
    for (int i = _nSizeX/2 - 5; i <= _nSizeX/2 + 5; i++) 
			 {
             _pItems[(i) * _nSizeX + _nSizeX/2] = 5.0f;
             }
    */
    
    
    
    /*
    
      {
      
        for (int i = _nSizeX/2 - 5; i <= _nSizeX/2 + 5; i++) {
        for (int j = _nSizeX/2 - 5; j <= _nSizeX/2 + 5; j++)
        _pItems[i * _nSizeX + j] = 0.0f;
        }
}*/
    
    
 //   _pItems[(_nSizeY/2) * _nSizeX + 1] = 0.0f;
  
 /*
        
               for (i = 0; i < _nSizeX; i++) {
                    _pItems[(_nSizeY/2) * _nSizeX + i] = 0.0f;
                }*/
        
    
    
  /*
      //  // Prvni sikma
        for (i = 0; i < _nSizeX / 4; i++) {
            _pItems[(_nSizeY/2) * _nSizeX + i] = 100.0f - i;
        }
        
        
        // Druha sikma
        for (; i < _nSizeX / 2 ; i++) {
            _pItems[(_nSizeY/2) * _nSizeX + i] = 68.0f - 2 * (i - 32);
        }
        
        for (; i < 3 * _nSizeX / 4 ; i++) {
            _pItems[(_nSizeY/2) * _nSizeX + i] = 2 + 2 * (i - 64);
        }
        
        for (; i < _nSizeX ; i++) {
            _pItems[(_nSizeY/2) * _nSizeX + i] = 68 - (i - 96);
        }
        */
    /*
    for (int j = 0; j < _nSizeY ; j++) 
    {
        int i = 0;
        for (; i < _nSizeX/4; i++) 
        {
            _pItems[i * _nSizeX + j] =  5 * i;            
        }

        for (; i < _nSizeX/2; i++) 
        {
            _pItems[i * _nSizeX + j] = 5 * (_nSizeX / 2 - i);            
        }

        for (; i < 3*_nSizeX/4; i++) 
        {
            _pItems[i * _nSizeX + j] = 5 * (i - _nSizeX / 2);            
        }

        for (; i < _nSizeX; i++) 
        {
            _pItems[i * _nSizeX + j] = 5 * (_nSizeX - i);           
        }

    }
    */
/*
  
  for (int j = 0; j < _nSizeY ; j++) 
    {
        int i = 0;
        for (; i < _nSizeX/4; i++) 
        {
            _pItems[i * _nSizeX + j] =  5 * i;            
        }

        for (; i < _nSizeX/2; i++) 
        {
            _pItems[i * _nSizeX + j] = 5 * (_nSizeX / 2 - i) + 30;            
        }

        for (; i < 3*_nSizeX/4; i++) 
        {
            _pItems[i * _nSizeX + j] = 5 * (i - _nSizeX / 2);            
        }

        for (; i < _nSizeX; i++) 
        {
            _pItems[i * _nSizeX + j] = 5 * (_nSizeX - i);           
        }
  }


 */  
    for(unsigned int i = 0; i < _nSizeX; i++)
    {
        for(unsigned int j = 0; j < _nSizeY; j++)
        {
            _pItems[i * _nSizeY + j] = 300.0f;
        }
    } 

    FILE * f = fopen(pszFileName,"r");
    if (f == NULL)
        return;
    
    for(unsigned int i = 0; i < _nSizeX * _nSizeY; i ++)
    {
        float fTempX, fTempY, fTempZ;
        int nTemp;
        
        fscanf(f,"*MESH_VERTEX %d %f %f %f\n",&nTemp, &fTempX, &fTempY, &fTempZ);
        
        _pItems[FLOATTOINT(fTempX/_fStepX) * _nSizeY + FLOATTOINT(fTempY/_fStepY) ] = fTempZ / 5.0f;
    }
    fclose(f);

    /*for (unsigned int j = 0; j < _nSizeY/2 ; j++) {
        for (unsigned int i = 0; i < _nSizeX; i++) {
            _pItems[j * _nSizeX + i] += 100.0f;
        }
    }*/

  
   /*    
    
    
    
    
    
    /*
				{
                
                  for (int j = 0; j < _nSizeY/2 ; j++) {
                  for (int i = 0; i < _nSizeX; i++) {
                  _pItems[j * _nSizeX + i] = 200.0f;
                  }
                  }
                  
                    
                      for ( j =  _nSizeY/2 + 1; j < _nSizeY; j++) {
                      for (int i = 0; i < _nSizeX; i++) {
                      _pItems[j * _nSizeX + i] = 200.0f;
                      }
                      }
                      
                        
                                }*/
    
                                /*
                                for (int  i = _nSizeX/2 - 5; i <= _nSizeX/2 + 5; i++) {
                                for (int j = _nSizeX/2 - 5; j <= _nSizeX/2 + 5; j++)
                                _pItems[i * _nSizeX + j] -= 200.0f;
                                                }*/
    
				
    
    //
    FindMaxHeight();
    
    
    
    
}

// error and warning handling for png
void user_error_fn(png_structp png_ptr, png_const_charp error_msg)
{
  //AfxMessageBox(error_msg);
  LogF(error_msg);
}

void user_warning_fn(png_structp png_ptr,
                     png_const_charp warning_msg)
{
  LogF(warning_msg);
}

// used in LoadPbl function originaly from MNTools lib in Visitor
LPSTR SeparateDirFromFileName(LPCSTR source)
{
  int length = strlen(source);
  TCHAR * dest = new TCHAR[length + 1];
  strcpy(dest, source);
  _strrev(dest);

  int enddir = strcspn(dest, "/\\");
  strncpy(dest, source, length - enddir);
  dest[length - enddir] = 0;
  return dest;
}

// load from Pbl
void CCountry::LoadPbl(const char * cfgFileName)
{
  ParamFile cfgFile;
  if (LSOK != cfgFile.Parse(cfgFileName))
  {
    //CString text;
    //text.Format(IDS_FILE_OPEN_FAILED, cfgFileName);
    //AfxMessageBox(text);
    return;
  }

  ParamClassPtr cfgClass = cfgFile.GetClass("cfg");
  if (cfgClass.IsNull())
  {
    LogF("Entry %s, not found in paramfile %s.","cfg",cfgFileName );
    //AfxMessageBox(IDS_OPER_FAILED);
    return ;
  }
  ParamEntryPtr entry = cfgClass->FindEntry("PNGfilename");
  if (entry.IsNull())
  {
    LogF("Entry %s, not found in paramfile %s.","PNGfilename",cfgFileName );
    //AfxMessageBox(IDS_OPER_FAILED);
    return ;
  }

  LPCSTR fileName = (LPCSTR)((RStringB)(*entry));
  
  LPSTR dir = SeparateDirFromFileName( cfgFileName);

  char * pngFileName = new char[strlen(dir) + strlen(fileName) + 1];
  strcpy(pngFileName,dir);
  strcat(pngFileName, fileName);


  FILE *fp = fopen(pngFileName, "rb");
  if (!fp) 
  {
    //CString text;
    //text.Format(IDS_FILE_OPEN_FAILED, dir + fileName);
    //AfxMessageBox(text);
    return ;
  }

  png_byte header[8];
  fread(header, sizeof(png_byte), 8, fp); 
  int ret = png_sig_cmp((png_bytep) header, 0, 8);
  if (ret != 0) 
  {
    //AfxMessageBox(IDS_NOT_PNG);
    fclose(fp);
    return ; 
  }

  png_structp png_ptr = png_create_read_struct
    (PNG_LIBPNG_VER_STRING, NULL,user_error_fn,user_warning_fn);

  if (!png_ptr)
  {
    //AfxMessageBox(IDS_OPER_FAILED);
    LogF("png_create_read_struct failed");
    fclose(fp);
    return ;
  }

  png_infop info_ptr = png_create_info_struct(png_ptr);
  if (!info_ptr)
  {
    png_destroy_read_struct(&png_ptr,
      (png_infopp)NULL, (png_infopp)NULL);
    ///AfxMessageBox(IDS_OPER_FAILED);
    LogF("png_create_info_struct failed");
    fclose(fp);
    return ;
  }

  png_infop end_info = png_create_info_struct(png_ptr);
  if (!end_info)
  {
    png_destroy_read_struct(&png_ptr, &info_ptr,
      (png_infopp)NULL);
    //AfxMessageBox(IDS_OPER_FAILED);
    LogF("png_create_info_struct failed");
    fclose(fp);
    return ;
  }

  png_init_io(png_ptr, fp);
  png_set_sig_bytes(png_ptr, 8);

  png_read_info(png_ptr, info_ptr);

  png_uint_32 width;
  png_uint_32 height;
  int bit_depth;
  int color_type;
  png_get_IHDR(png_ptr, info_ptr, &width, &height, &bit_depth,
    &color_type, NULL, NULL, NULL);

  if (color_type != PNG_COLOR_TYPE_GRAY && bit_depth != 16)
  {
    //AfxMessageBox(IDS_BAD_PNG);
    png_destroy_read_struct(&png_ptr, &info_ptr,
      &end_info);

    LogF("png_create_info_struct failed");
    fclose(fp);
    return ;
  }

  if (bit_depth == 16)
    png_set_swap(png_ptr);

  png_read_update_info(png_ptr, info_ptr);

  png_uint_16pp row_pointers = new png_uint_16p[height];
  png_uint_16p data = new png_uint_16[height * width];

  for(unsigned int i = 0; i < height; i++)
    row_pointers[i] = data + i * width;

  png_read_image(png_ptr, (png_bytepp)row_pointers);
  png_read_end(png_ptr, end_info);
  png_destroy_read_struct(&png_ptr, &info_ptr, &end_info);
  fclose(fp);

  entry = cfgClass->FindEntry("maxHeight");
  if (entry.IsNull())
  {
    LogF("Entry %s, not found in paramfile %s.","maxHeight",cfgFileName );
    //AfxMessageBox(IDS_OPER_FAILED);
    return ;
  }
  float maxHeight = *entry;

  entry = cfgClass->FindEntry("minHeight");
  if (entry.IsNull())
  {
    LogF("Entry %s, not found in paramfile %s.","minHeight",cfgFileName );
    //AfxMessageBox(IDS_OPER_FAILED);
    return ;
  }
  float minHeight = *entry;

  float step = (maxHeight - minHeight) / 0xffff;

  // Create new document
  entry = cfgClass->FindEntry("squareSize");
  float realSizeUnit = *entry;

  int originX = 0;
  entry = cfgClass->FindEntry("originX");
  if (!entry.IsNull()) 
    originX = (int) *entry;

  int originY = 0;
  entry = cfgClass->FindEntry("originY");
  if (!entry.IsNull()) 
    originY = (int) *entry;

  // size must be power of 2
  /*int log = 0;
  int orig = max(width, height);
  int size = orig;
  for(; size > 0; log++)
    size = size >> 1;

  if (orig == 1 << (log - 1))
    size = 1 << (log - 1);
  else
    size = 1 << log;*/

  Init(width, height, realSizeUnit, realSizeUnit, originX, originY);

  int heightm1 = height - 1;
  for(int z = 0; z < (int) height; z++) for(int x = 0; x < (int) width; x++)
    SetItem(x,z, (0xffff - data[(heightm1 - z) * width + x]) * step + minHeight);

    //m_PoseidonMap.SetLandscapeHeight(pos,  (0xffff - data[(heightm1 - pos.z) * width + pos.x]) * step + minHeight, FALSE);

  //UpdateAllViews(NULL);*/
  FindMaxHeight();

  return ;
}

void CCountry::CreateDamCountry(unsigned int nSize)
{
    Init( nSize, nSize, 1, 1);

    for(unsigned int i = 0; i < nSize; i++)
    {
        for(unsigned int j = 0; j < nSize; j++)
        {
            _pItems[i * nSize + j] = 300.0f;
        }
    }

    // Dam
    for(unsigned int i = 0; i < nSize / 4 - 1; i++)
    {
        for(unsigned int j = 0; j < nSize / 4; j++)
        {
            _pItems[i * nSize + j] = 0.0f;
        }
    }


    // Channel
    for(unsigned int i = nSize / 4 + 1; i < nSize ; i++)
    {
        for(unsigned int j = 0; j < nSize / 8; j++)
        {
            _pItems[i * nSize + j] = 0.0f;
        }
    }

    FindMaxHeight();
}

void CCountry::CreateFlatLand(unsigned int nSize)
{
    Init( nSize, nSize, 1, 1);

    for(unsigned int i = 0; i < nSize; i++)
    {
        for(unsigned int j = 0; j < nSize; j++)
        {
            _pItems[i * nSize + j] = 0.0f;
        }
    }

    FindMaxHeight();
}

void CCountry::CreateCustomLand(unsigned int nSize)
{
    Init( nSize, nSize, 1, 1);

    for(unsigned int i = 0; i < nSize/4; i++)
    {
        for(unsigned int j = 0; j < nSize; j++)
        {
            _pItems[i  + j * nSize] = -230.0f;
        }
    }

    for(unsigned int i =  nSize / 4; i < 3 * nSize / 4; i++)
    {
        for(unsigned int j = 0; j < nSize; j++)
        {
            _pItems[i + j * nSize] = -230.0f + 150.0f * (i - nSize / 4) / (nSize / 2);
        }
    }

    for(unsigned int i = 3 *  nSize / 4; i < nSize; i++)
    {
        for(unsigned int j = 0; j < nSize; j++)
        {
            _pItems[i + j * nSize] = 0.0f;
        }
    }


    FindMaxHeight();
    
}

void CCountry::BlowUpDam()
{
    for(unsigned int i = _nSizeX / 4 - 1; i < _nSizeX / 4 + 1 ; i++)
    {
        for(unsigned int j = 0; j < _nSizeY / 8; j++)
        {
            _pItems[i * _nSizeX + j] = 0.0f;
        }
    }

}

void CCountry::Save(const char * pszFileName)
{
	FILE * f = fopen(pszFileName,"w");
	if (f == NULL)
		return;

	for(unsigned int y = 0; y < _nSizeY; y ++)
		for(unsigned int x = 0; x < _nSizeX; x ++)
		{
			fprintf(f,"*MESH_VERTEX %d %f %f %f\n", x  + y * _nSizeX, _fStepX * x, _fStepY * y, _pItems[x  * _nSizeY + y ]);
		}

		fclose(f);
}

const CCountry& CCountry::operator=(const CCountry& cCountry)
{
	_fStepX = cCountry._fStepX;
	_fStepY = cCountry._fStepY;
	_fMaxHeight = cCountry._fMaxHeight;
		
	MyArray2D<float>::operator=(cCountry);

	return *this;
}

int CCountry::FindMaxHeight()
{
  _fMaxHeight = 0;
  float fMinHeight = _pItems[0,0];

  FindMaxMin(_fMaxHeight,fMinHeight);

  // do not lower land to the lowest position...
  return 1; 
  /*
  float * pTemp = _pItems;
  float * pTempEnd = _pItems + _nSizeX * _nSizeY;
  // move to 0

  for(; pTemp < pTempEnd; pTemp++)
  {

    *pTemp -= fMinHeight;
  }

  _fMaxHeight -= fMinHeight;

  return 1;*/
}

float CCountry::GetAverageHeight(const unsigned int iFromX, const unsigned int iFromY, 
                                 const unsigned int nSizeX, const unsigned int nSizeY) const
{
    float fAverageHeigth = 0;
    ASSERT(iFromX < _nSizeX && (iFromX + nSizeX) <= _nSizeX);
    ASSERT(iFromY < _nSizeY && (iFromY + nSizeY) <= _nSizeY);

    for(unsigned int i = iFromX; i < iFromX + nSizeX; i++)
    {
        for(unsigned int j = iFromY; j < iFromY + nSizeY; j++)
        {
            fAverageHeigth += _pItems[i * _nSizeX + j];
        }
    }

    fAverageHeigth /= (nSizeX * nSizeY);

    return fAverageHeigth;
}



float CCountry::GetMinimum(const unsigned int iFromX, const unsigned int iFromY, 
                                 const unsigned int nSizeX, const unsigned int nSizeY) const
{
    float fMinHeigth = 100000;
    ASSERT(iFromX < _nSizeX && (iFromX + nSizeX) <= _nSizeX);
    ASSERT(iFromY < _nSizeY && (iFromY + nSizeY) <= _nSizeY);

    for(unsigned int i = iFromX; i < iFromX + nSizeX; i++)
    {
        for(unsigned int j = iFromY; j < iFromY + nSizeY; j++)
        {
            if (fMinHeigth > _pItems[i * _nSizeX + j])
            {
                fMinHeigth = _pItems[i * _nSizeX + j];
            }
        }
    }

    return fMinHeigth;
}

void CCountry::GetMassOnHeight(const unsigned int iFromX, const unsigned int iFromY, 
                               const unsigned int nSizeX, const unsigned int nSizeY,
                               const unsigned int nTilles, float * pfMassOnHeight, 
                               float& fMinHeight, float& fMassOnHeightStep) const
{
  memset(pfMassOnHeight, 0x0, nTilles * sizeof(*pfMassOnHeight));
  // Find Max and Min
  fMinHeight = 1000000; 
  float fMaxHeight = 0;

  ASSERT(iFromX < _nSizeX && (iFromX + nSizeX) <= _nSizeX);
  ASSERT(iFromY < _nSizeY && (iFromY + nSizeY) <= _nSizeY);

  for(unsigned int i = iFromX; i < iFromX + nSizeX && i < _nSizeX; i++)
  {
    for(unsigned int j = iFromY; j < iFromY + nSizeY && j < _nSizeY; j++)
    {
      if (i >= _nSizeX || j >= _nSizeY)
      {      
        LogF("i %d,j %d, iFromX %d, iFromY %d, nSizeX %d, nSizeX %d",i ,j , iFromX , iFromY , nSizeX , nSizeX  );
      }

      if (fMinHeight > _pItems[i * _nSizeX + j])
      {
        fMinHeight = _pItems[i * _nSizeX + j];
      }

      if (fMaxHeight < _pItems[i * _nSizeX + j])
      {
        fMaxHeight = _pItems[i * _nSizeX + j];
      }
    }
  }

  // Calculate  MassOnHeight
  if (fMaxHeight == fMinHeight)
  {        
    fMassOnHeightStep = 0.0f;
    return;        
  }

  fMassOnHeightStep = (fMaxHeight + 0.00001f - fMinHeight) / nTilles; 
  // 0.0001 is to be sure that (fMaxHeight  - fMinHeight) / fMassOnHeightStep = nTilles - 1;

  float fInvMassOnHeightStep = 1.0f / fMassOnHeightStep;

  for(unsigned int i = iFromX; i < iFromX + nSizeX && i < _nSizeX; i++)
  {
    for(unsigned int j = iFromY; j < iFromY + nSizeY && j < _nSizeX; j++)
    {
      //ASSERT((unsigned int) ((_pItems[i * _nSizeX + j] - fMinHeight) * fInvMassOnHeightStep) < nTilles);
      pfMassOnHeight[(unsigned int) ((_pItems[i * _nSizeX + j] - fMinHeight) * fInvMassOnHeightStep)] += 
        _pItems[i * _nSizeX + j] - fMinHeight;                             
    }
  }

  float fSurfaceOfOneParcel = _fStepX * _fStepY;
  for(unsigned int i = 1; i < nTilles; i++)
  {    
    pfMassOnHeight[i] += pfMassOnHeight[i - 1];

    pfMassOnHeight[i - 1] *= fSurfaceOfOneParcel; 
  }

  pfMassOnHeight[nTilles - 1] *= fSurfaceOfOneParcel; 

  return;    
}



float CDrawToFromSimCoordConverter::_fDrawStepX = 2.0f;
float CDrawToFromSimCoordConverter::_fDrawStepY = 2.0f;
float CDrawToFromSimCoordConverter::_fDrawStepZ = 0.02f;

CDrawToFromSimCoordConverter g_cCoordConverter;

void CFlyShapeSimCoord::AddVertex(float fX, float fY, float fZ, RGBQUAD& rgbColor)
{
    SFlyVertex tVertex;

    tVertex._position = g_cCoordConverter.FromSimCoordToDrawCoord( fX, fY, fZ);
    tVertex._diffuse = D3DCOLOR_XRGB(rgbColor.rgbRed, rgbColor.rgbGreen, rgbColor.rgbBlue);

    CFlyShape::AddVertex(tVertex);    
}
    
