#ifndef __AMUNDSEN_H__
#define __AMUNDSEN_H__

#include "Arctic.h"



class CAmundsen : public CArctic
{
protected:
  CCountry * _pcCountry;
  MyArray2DFloat _cWaterMass;
  // MyArray2DFloat _cWaterMassNew;

  // MyArray2DFloat _cWaterHeight;
  MyArray2DFloat _cWaterHeightOld;

  // MyArray2DFloat _cWaterMassPlusCountry;
  MyArray2DFloat _cFlowX;
  MyArray2DFloat _cFlowY;
  MyArray2DFloat _cFlowXOld;
  MyArray2DFloat _cFlowYOld;
  MyArray2DInt _cType;
  MyArray2DInt _cTypeOld;

  // for drawing
  MyArray2DFloat _cWaterHeightZoomed;
  MyArray2DFloat _cWaterMassZoomed;

  Sources * _ptSources;
public:
  CAmundsen(): _pcCountry(NULL), _ptSources(NULL) {};
  virtual void Init(CCountry * _pcCountry);

  virtual void Simulate();

  virtual void SetSources(Sources * ptSources) {_ptSources = ptSources;};
  virtual const MyArray2DFloat& GetWaterHeight() const  {return _cWaterHeightZoomed;};
  virtual const MyArray2DFloat& GetMass() const {return _cWaterMassZoomed;};
  virtual const MyArray2DInt& GetType() const {return _cTypeOld;};

  virtual void MoveTeselationToPoint(float x, float y, unsigned int iLowestLevel) {};
  void EquidistantTeselation(unsigned int iLevel) {};
  void PrepareZoomedAndOffseted(unsigned int nSizeX, 
    unsigned int nSizeY, 
    unsigned int iZoomLevel, 
    unsigned int iOffsetX, 
    unsigned int iOffsetY);

  // save water height into pbl (png file defined by visitor)
  virtual void SaveWaterInPbl(LPCSTR dir, LPCSTR fileName, bool waterMass = false);
};

#endif