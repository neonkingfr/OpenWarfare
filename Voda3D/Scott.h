#ifndef _SCOTT_H_
#define _SCOTT_H_

#include "Shared.h"

#define LIQUIDITY 0.5f

class CScott {
private:
  CCountry *_pcCountry;
	MyArray2DFloat _cWaterHeight;
	Sources *_ptSources;
  int _SimCounter;
public:
  CScott();
	void Init(CCountry *pcCountry);
  void Simulate();
  void SetSources(Sources *ptSources) {_ptSources = ptSources;};
  const MyArray2DFloat& GetWaterHeight() const {return _cWaterHeight;};


};

#endif