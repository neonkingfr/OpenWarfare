#ifndef __MYARRAY2D_H__
#define __MYARRAY2D_H__



template<class TYPE> 
class MyArray2D
{
protected:
	unsigned int _nSizeX;
	unsigned int _nSizeY;

	TYPE *_pItems;

public:
	MyArray2D(): _nSizeX(0), _nSizeY(0), _pItems(NULL) {};
	~MyArray2D();
	MyArray2D(const MyArray2D& cArray);

	void Init(unsigned int nSizeX, unsigned int nSizeY);

	unsigned int GetSizeX() const {return _nSizeX;};
	unsigned int GetSizeY() const {return _nSizeY;};
	const TYPE * GetItems() const {return _pItems;};

	TYPE GetItem(unsigned int x, unsigned int y) const {return _pItems[x * _nSizeY + y ];}
	void SetItem(unsigned int x, unsigned int y, const TYPE& cItem) {_pItems[x * _nSizeY + y] = cItem;};	
	void AddItem(unsigned int x, unsigned int y, const TYPE& cItem) {_pItems[x * _nSizeY + y] += cItem;};
	
	void ZeroItems() {memset(_pItems, 0x0, _nSizeX * _nSizeY * sizeof(*_pItems));};
    
    void FindMaxMin(float &fMax, float &fMin) const;
    
    void GetZoomedAndOffseted(MyArray2D<TYPE>& cArray, 
                              unsigned int iZoomFactor, // 0 - no zoomed, 1 - 2x , 2 - 4x atd ...
                              unsigned int iOffsetX,
                              unsigned int iOffsetY) const;

	virtual const MyArray2D& operator=(const MyArray2D& cArray);
	const MyArray2D& operator+=(const MyArray2D& cArray);
    const MyArray2D& operator-=(const MyArray2D& cArray);

	MyArray2D operator+(const MyArray2D& cArray) const;
};

template<class TYPE> 
MyArray2D<TYPE>::MyArray2D(const MyArray2D& cArray) 
{
  _pItems = NULL;
	Init( cArray._nSizeX, cArray._nSizeY);
	memcpy(_pItems, cArray._pItems,_nSizeX * _nSizeY * sizeof(*_pItems));
}

template<class TYPE> 
MyArray2D<TYPE>::~MyArray2D()
{
	delete [] _pItems;
}

template<class TYPE> 
void MyArray2D<TYPE>::Init(unsigned int nSizeX, unsigned int nSizeY)
{
    if (_pItems != NULL)
        delete [] _pItems;

	_nSizeX = nSizeX;
	_nSizeY = nSizeY;

	_pItems = new TYPE[_nSizeX * _nSizeY];
	memset(_pItems, 0x0, _nSizeX * _nSizeY * sizeof(*_pItems));
}

template<class TYPE> 
 const MyArray2D<TYPE>& MyArray2D<TYPE>::operator=(const MyArray2D<TYPE>& cArray)
{
	if (_nSizeX != cArray._nSizeX || _nSizeY != cArray._nSizeY)
	{
		delete [] _pItems;
		Init( cArray._nSizeX, cArray._nSizeY);
	}

	memcpy(_pItems, cArray._pItems,_nSizeX * _nSizeY * sizeof(*_pItems));

	return *this;
}

template<class TYPE> 
const MyArray2D<TYPE>& MyArray2D<TYPE>::operator+=(const MyArray2D<TYPE>& cArray)
{
	if (_nSizeX != cArray._nSizeX || _nSizeY != cArray._nSizeY)
	{
		return *this;
	}

	float * pTempTo = _pItems;
	float * pTempFrom = cArray._pItems;
	float * pTempToEnd = _pItems + _nSizeX + _nSizeY;
	for(; pTempTo < pTempToEnd; )
	{
		*pTempTo += *pTempFrom;
		pTempTo++; pTempFrom++;
		
	}
	
	return *this;
}

template<class TYPE> 
const MyArray2D<TYPE>& MyArray2D<TYPE>::operator-=(const MyArray2D<TYPE>& cArray)
{
	if (_nSizeX != cArray._nSizeX || _nSizeY != cArray._nSizeY)
	{
		return *this;
	}

	float * pTempTo = _pItems;
	float * pTempFrom = cArray._pItems;
	float * pTempToEnd = _pItems + _nSizeX + _nSizeY;
	for(; pTempTo < pTempToEnd; )
	{
		*pTempTo -= *pTempFrom;
		pTempTo++; pTempFrom++;
		
	}
	
	return *this;
}

template<class TYPE> 
MyArray2D<TYPE> MyArray2D<TYPE>::operator+(const MyArray2D<TYPE>& cArray) const
{
	if (_nSizeX != cArray._nSizeX || _nSizeY != cArray._nSizeY)
	{
		return *this;
	}

	MyArray2D<TYPE> cResult(*this);

	cResult += cArray;
	
	return cResult;
}

template<class TYPE> 
void MyArray2D<TYPE>::FindMaxMin(float &fMax, float &fMin) const
{
    if (_nSizeX > 0 && _nSizeY > 0)
	{
		fMax = _pItems[0,0];
		fMin = _pItems[0,0];

		float * pTemp = _pItems;
		float * pTempEnd = _pItems + _nSizeX * _nSizeY;
		for(; pTemp < pTempEnd; pTemp++)
		{
			if (*pTemp > fMax)
				fMax = *pTemp;
			if (*pTemp < fMin)
				fMin = *pTemp;
		}
    }
}


template<class TYPE> 
void MyArray2D<TYPE>::GetZoomedAndOffseted(MyArray2D<TYPE>& cArray, 
                              unsigned int iZoomPower, // 0 - no zoomed, 1 - 2x , 2 - 4x atd ...
                              unsigned int iOffsetX,
                              unsigned int iOffsetY) const
{
  ASSERT(_nSizeX >= cArray._nSizeX && _nSizeY >= cArray._nSizeY);
  ASSERT(cArray._nSizeX > 0 && cArray._nSizeY > 0);

  unsigned int iZoomFactor = min(((unsigned int) 1 << iZoomPower), min(_nSizeX / cArray._nSizeX, _nSizeY / cArray._nSizeY));

  unsigned int nFromSizeX = cArray._nSizeX * iZoomFactor;
  unsigned int nFromSizeY = cArray._nSizeY * iZoomFactor;

  unsigned int iFromOffsetX = (iOffsetX + nFromSizeX > _nSizeX) ? (_nSizeX - nFromSizeX) : iOffsetX;
  unsigned int iFromOffsetY = (iOffsetY + nFromSizeY > _nSizeY) ? (_nSizeY - nFromSizeY) : iOffsetY;

  cArray.ZeroItems();

  /*
  // Replace goemetry by average value.

  for(unsigned int iToX = 0; iToX < cArray._nSizeX; iToX ++)
  {
  unsigned int iFromX = iFromOffsetX + iToX * iZoomFactor;
  for(unsigned int iDeltaFromX = 0;  iDeltaFromX < iZoomFactor; iDeltaFromX ++)
  {
  for(unsigned int iToY = 0; iToY < cArray._nSizeY; iToY ++)
  {
  unsigned int iFromY = iFromOffsetY + iToY * iZoomFactor;
  for(unsigned int iDeltaFromY = 0;  iDeltaFromY < iZoomFactor; iDeltaFromY++)
  {
  cArray.AddItem(iToX, iToY, GetItem(iFromX, iFromY));
  iFromY++;
  }
  }
  iFromX++;
  }
  }

  // Make Average.

  if (iZoomFactor == 1)
  return;

  iZoomFactor *= iZoomFactor;
  for(unsigned int iToX = 0; iToX < cArray._nSizeX; iToX ++)
  {
  for(unsigned int iToY = 0; iToY < cArray._nSizeY; iToY ++)
  {                
  cArray._pItems[iToX * cArray._nSizeY + iToY] /= iZoomFactor;                        
  }
  }
  */

  for(unsigned int iToX = 0; iToX < cArray._nSizeX; iToX ++)
  {
    unsigned int iFromX = iFromOffsetX + iToX * iZoomFactor;
    for(unsigned int iDeltaFromX = 0;  iDeltaFromX < iZoomFactor; iDeltaFromX ++)
    {
      for(unsigned int iToY = 0; iToY < cArray._nSizeY; iToY ++)
      {
        unsigned int iFromY = iFromOffsetY + iToY * iZoomFactor;
        cArray.SetItem(iToX, iToY,GetItem(iFromX, iFromY));
        iFromY++;
        for(unsigned int iDeltaFromY = 1;  iDeltaFromY < iZoomFactor; iDeltaFromY++)
        {
          if (GetItem(iFromX, iFromY) < cArray.GetItem(iToX, iToY))
            cArray.SetItem(iToX, iToY, GetItem(iFromX, iFromY));

          iFromY++;
        }
      }
      iFromX++;
    }
  }
}

typedef MyArray2D<float> MyArray2DFloat;
typedef MyArray2D<int> MyArray2DInt;

#endif //__MYARRAY2D_H__