#include "stdafx.h"
#include "Nansen.h"

extern const unsigned int  g_ppBorders[4][4];// = {{0,1,0,2},{3,0,2,0},{0,0,0,3},{0,0,1,0}};

// Squares tilling: 0 1
//                  3 2
// Borders:    0
//            3 1
//             2  

const float MIN_WATER_SURFACE_COEF = 0.01f;

const float NUMERICAL_ZERO = 0.00001f;

__forceinline int IsNumericalZero(float fValue)
{
    return (fValue > -NUMERICAL_ZERO && fValue < NUMERICAL_ZERO);
}

extern int g_iStep;
extern const int g_iStop;

extern void DebugPoint();

#define USE_MASS_CRITERIUM

void CSquare::SetMinMaxHeightFromCountry(CCountry * pcCountry)
{  
    _fAverageWaterHeight = _fMinCountryHeight = pcCountry->GetMinimum(
            _iTopLeftX, _iTopLeftY, _nSizeX, _nSizeY);
        
    _fMaxCountryHeight = _fMinCountryHeight + 2 * (pcCountry->GetAverageHeight(
            _iTopLeftX, _iTopLeftY, _nSizeX, _nSizeY) - 
            _fMinCountryHeight);
}
     
void CSquare::CalculateWaterHeightAndSurfaceFromMass()
{    
    if (_fMass > 0.0f)
    {
        float fTriangle = _fSizeX * _fSizeY * 
            (_fMaxCountryHeight - _fMinCountryHeight) / 2;

        if (_fMass > fTriangle)
        {
            _fAverageWaterHeight = _fMaxCountryHeight + 
                (_fMass - fTriangle) / (_fSizeX * _fSizeY);

            _fWaterSurface = (_fSizeX * _fSizeY);
        }
        else
        {
            _fAverageWaterHeight = sqrtf(_fMass / (_fSizeX * _fSizeY) * 2.0f * 
                (_fMaxCountryHeight - _fMinCountryHeight));

            _fWaterSurface = (_fSizeX * _fSizeY) * _fAverageWaterHeight/(_fMaxCountryHeight - _fMinCountryHeight);

            _fAverageWaterHeight += _fMinCountryHeight;
        }
    }
    else
    {
        _fMass = 0.0f;
        _fAverageWaterHeight = _fMinCountryHeight;
        if (!IsNumericalZero(_fMaxCountryHeight - _fMinCountryHeight))
            _fWaterSurface = MIN_WATER_SURFACE_COEF * (_fSizeX * _fSizeY);
        else
            _fWaterSurface = (_fSizeX * _fSizeY);
    }

    _fWaterSurface = max( _fWaterSurface, MIN_WATER_SURFACE_COEF * (_fSizeX * _fSizeY));   
}

void CSquare::CalculateMassFromWaterHeight()
{
    if (_fAverageWaterHeight < _fMinCountryHeight)
    {
        _fAverageWaterHeight = _fMinCountryHeight;
        _fMass = 0.0f;

        return;
    }

    if (_fAverageWaterHeight < _fMaxCountryHeight)
    {
        _fMass = _fSizeX * _fSizeY *  
            (_fAverageWaterHeight - _fMinCountryHeight) * (_fAverageWaterHeight  - _fMinCountryHeight)/ (_fMaxCountryHeight - _fMinCountryHeight) / 2.0f;

        return;
    }

    _fMass = _fSizeX * _fSizeY * ((_fAverageWaterHeight - _fMaxCountryHeight)  + (_fMaxCountryHeight - _fMinCountryHeight) / 2.0f);
    return;
}

void CSquare::CreateContact(const int iBorder, 
                            const unsigned int iFrom, 
                            const unsigned int nSize, 
                            CSquare * pSquare, 
                            const int bSimulate,
                            CCountry * pcCountry)
{
    ASSERT(iFrom + nSize <= _nSizeX);
    TCONTACT tContact;

    tContact._tData._iBorder = iBorder;
    tContact._tData._iFrom = iFrom;
    tContact._tData._nSize = nSize;
    tContact._bSimulate = bSimulate;
    tContact._ptSquare = pSquare;

    if (iBorder == 0 || iBorder == 2)
        tContact._tData._fSize = _fSizeX * nSize / _nSizeX;
    else
        tContact._tData._fSize = _fSizeY * nSize / _nSizeY;

    if (_iTopLeftX < 0 || _iTopLeftY < 0 ||
        _iTopLeftX >= pcCountry->GetSizeX() || _iTopLeftY >= pcCountry->GetSizeY())
    {
        // Fictive borders 
        tContact._tData._fMinBorderHeight = _fMinCountryHeight;
        tContact._tData._fMaxBorderHeight = _fMaxCountryHeight;
    }
    else
    {    
        switch (iBorder)
        {
        case 0:
            tContact._tData._fMinBorderHeight = pcCountry->GetMinimum(
                _iTopLeftX + iFrom, _iTopLeftY, nSize, 1);
            
            tContact._tData._fMaxBorderHeight = tContact._tData._fMinBorderHeight + 2 * (pcCountry->GetAverageHeight(
                _iTopLeftX + iFrom, _iTopLeftY, nSize, 1) - 
                tContact._tData._fMinBorderHeight);
            break;
        case 1:
            tContact._tData._fMinBorderHeight = pcCountry->GetMinimum(
                _iTopLeftX + _nSizeX - 1, _iTopLeftY + iFrom, 1, nSize);
            
            tContact._tData._fMaxBorderHeight = tContact._tData._fMinBorderHeight + 2 * (pcCountry->GetAverageHeight(
                _iTopLeftX + _nSizeX - 1, _iTopLeftY + iFrom, 1, nSize) - 
                tContact._tData._fMinBorderHeight);
            break;
            
        case 2:
            tContact._tData._fMinBorderHeight = pcCountry->GetMinimum(
                _iTopLeftX + iFrom, _iTopLeftY + _nSizeY - 1, nSize, 1);
            
            tContact._tData._fMaxBorderHeight = tContact._tData._fMinBorderHeight + 2 * (pcCountry->GetAverageHeight(
                _iTopLeftX + iFrom, _iTopLeftY + _nSizeY - 1, nSize, 1) - 
                tContact._tData._fMinBorderHeight);
            break;
        case 3:
            tContact._tData._fMinBorderHeight = pcCountry->GetMinimum(
                _iTopLeftX, _iTopLeftY + iFrom, 1, nSize);
            
            tContact._tData._fMaxBorderHeight = tContact._tData._fMinBorderHeight + 2 * (pcCountry->GetAverageHeight(
                _iTopLeftX, _iTopLeftY + iFrom, 1, nSize) - 
                tContact._tData._fMinBorderHeight);
            break;
        default:
            ASSERT(0);
        }
    }

    _cContacts.Add(tContact);
    return;
}

int CSquare::FindContact(CSquare * pSquare, unsigned int iFrom)
{
    for(unsigned int i = 0; i < _cContacts.GetSize(); i++)
    {
        if (_cContacts[i]._ptSquare == pSquare)
        {
            if (_cContacts[i]._tData._iBorder % 2 == 0)
            {
                if (_cContacts[i]._tData._iFrom + _iTopLeftX == iFrom)                
                    return i;                
            }
            else
            {
                if (_cContacts[i]._tData._iFrom + _iTopLeftY == iFrom)                
                    return i;
                
            }
        }
    }

    return -1;
}

TCONTACT& CSquare::FindContact2(CSquare * pSquare, unsigned int iFrom)
{
    for(unsigned int i = 0; i < _cContacts.GetSize(); i++)
    {
        if (_cContacts[i]._ptSquare == pSquare)
        {
            if (_cContacts[i]._tData._iBorder % 2 == 0)
            {
                if (_cContacts[i]._tData._iFrom + _iTopLeftX == iFrom)                
                    return _cContacts[i];                
            }
            else
            {
                if (_cContacts[i]._tData._iFrom + _iTopLeftY == iFrom)                
                    return _cContacts[i];
                
            }
        }
    }

    ASSERT(0); // The function must not end here
    return _cContacts[0];
}

int CSquare::RetrieveContacts(MyContactArray& cFoundContacts, unsigned int iBorder)
{
    int nFound = 0;
    for(unsigned int i = 0; i < _cContacts.GetSize(); i++)
    {
        if (_cContacts[i]._tData._iBorder == iBorder)
        {
            nFound++;
            cFoundContacts.Add(_cContacts[i]);
        }
    }

    return nFound;
}

void CSquare::RefreshContactsCountryDep(CCountry * pcCountry)
{
    for(unsigned int i = 0; i < _cContacts.GetSize(); i++)
    {
        TCONTACT& tContact = _cContacts[i];

        if (_iTopLeftX < 0 || _iTopLeftY < 0 ||
            _iTopLeftX >= pcCountry->GetSizeX() || _iTopLeftY >= pcCountry->GetSizeY())
        {
            // Fictive borders 
            tContact._tData._fMinBorderHeight = _fMinCountryHeight;
            tContact._tData._fMaxBorderHeight = _fMaxCountryHeight;
        }
        else
        {    
            switch (tContact._tData._iBorder)
            {
            case 0:
                tContact._tData._fMinBorderHeight = pcCountry->GetMinimum(
                    _iTopLeftX + tContact._tData._iFrom, _iTopLeftY, tContact._tData._nSize, 1);
                
                tContact._tData._fMaxBorderHeight = tContact._tData._fMinBorderHeight + 2 * (pcCountry->GetAverageHeight(
                    _iTopLeftX + tContact._tData._iFrom, _iTopLeftY, tContact._tData._nSize, 1) - 
                    tContact._tData._fMinBorderHeight);
                break;
            case 1:
                tContact._tData._fMinBorderHeight = pcCountry->GetMinimum(
                    _iTopLeftX + _nSizeX - 1, _iTopLeftY + tContact._tData._iFrom, 1, tContact._tData._nSize);
                
                tContact._tData._fMaxBorderHeight = tContact._tData._fMinBorderHeight + 2 * (pcCountry->GetAverageHeight(
                    _iTopLeftX + _nSizeX - 1, _iTopLeftY + tContact._tData._iFrom, 1, tContact._tData._nSize) - 
                    tContact._tData._fMinBorderHeight);
                break;
                
            case 2:
                tContact._tData._fMinBorderHeight = pcCountry->GetMinimum(
                    _iTopLeftX + tContact._tData._iFrom, _iTopLeftY + _nSizeY - 1, tContact._tData._nSize, 1);
                
                tContact._tData._fMaxBorderHeight = tContact._tData._fMinBorderHeight + 2 * (pcCountry->GetAverageHeight(
                    _iTopLeftX + tContact._tData._iFrom, _iTopLeftY + _nSizeY - 1, tContact._tData._nSize, 1) - 
                    tContact._tData._fMinBorderHeight);
                break;
            case 3:
                tContact._tData._fMinBorderHeight = pcCountry->GetMinimum(
                    _iTopLeftX, _iTopLeftY + tContact._tData._iFrom, 1, tContact._tData._nSize);
                
                tContact._tData._fMaxBorderHeight = tContact._tData._fMinBorderHeight + 2 * (pcCountry->GetAverageHeight(
                    _iTopLeftX, _iTopLeftY + tContact._tData._iFrom, 1, tContact._tData._nSize) - 
                    tContact._tData._fMinBorderHeight);
                break;
            default:
                ASSERT(0);
            }
        }
        
    }
    
}

void CNansen::Init(CCountry * pcCountry)
{
    _pcCountry = pcCountry;
    _cWaterHeight = *_pcCountry;
    _cType.Init(pcCountry->GetSizeX(), pcCountry->GetSizeY());
    _cWaterMass.Init(pcCountry->GetSizeX(), pcCountry->GetSizeY());

    CWater2QuadTreeIterator cIterator(_cQuadTree);

    for(int i = 0; i < 4; i++)
    {    
        cIterator[i].Mass() = 0.0f;
        cIterator[i].nSizeX() = pcCountry->GetSizeX() / 2;
        cIterator[i].nSizeY() = pcCountry->GetSizeY() / 2;
        cIterator[i].fSizeX() = pcCountry->GetStepX() * pcCountry->GetSizeX() / 2;
        cIterator[i].fSizeY() = pcCountry->GetStepY() * pcCountry->GetSizeY() / 2;
        cIterator[i].DiffMass() = 0.0f;
        cIterator[i].Velocity()[0] = 0.0f;
        cIterator[i].Velocity()[1] = 0.0f;

    }

    cIterator[0].TopLeftX() = 0;
    cIterator[0].TopLeftY() = 0;    

    cIterator[1].TopLeftX() = cIterator[0].nSizeX();
    cIterator[1].TopLeftY() = 0;
    
    cIterator[2].TopLeftX() = cIterator[0].nSizeX();
    cIterator[2].TopLeftY() = cIterator[0].nSizeY();

    cIterator[3].TopLeftX() = 0;
    cIterator[3].TopLeftY() = cIterator[0].nSizeY();

    for(int j = 0; j < 4; j++)
    {
        cIterator[j].SetMinMaxHeightFromCountry(_pcCountry);
        cIterator[j].CalculateWaterHeightAndSurfaceFromMass();
    }

    //Create Borders    
    cIterator[0].CreateContact(0, 0, cIterator[0].nSizeX(), _pFictiveBorders, 1,_pcCountry);
    cIterator[0].CreateContact(1, 0, cIterator[0].nSizeY(), &cIterator[1], 1,_pcCountry);
    cIterator[0].CreateContact(2, 0, cIterator[0].nSizeX(), &cIterator[3], 0,_pcCountry);
    cIterator[0].CreateContact(3, 0, cIterator[0].nSizeY(), _pFictiveBorders + 3, 1,_pcCountry);

    cIterator[1].CreateContact(0, 0, cIterator[1].nSizeX(), _pFictiveBorders, 1,_pcCountry);
    cIterator[1].CreateContact(1, 0, cIterator[1].nSizeY(), _pFictiveBorders + 1, 1,_pcCountry);
    cIterator[1].CreateContact(2, 0, cIterator[1].nSizeX(), &cIterator[2], 0,_pcCountry);
    cIterator[1].CreateContact(3, 0, cIterator[1].nSizeY(), &cIterator[0], 0,_pcCountry);

    cIterator[2].CreateContact(0, 0, cIterator[2].nSizeX(), &cIterator[1], 1,_pcCountry);
    cIterator[2].CreateContact(1, 0, cIterator[2].nSizeY(), _pFictiveBorders + 1, 1,_pcCountry);
    cIterator[2].CreateContact(2, 0, cIterator[2].nSizeX(), _pFictiveBorders + 2, 1,_pcCountry);
    cIterator[2].CreateContact(3, 0, cIterator[2].nSizeY(), &cIterator[3], 0,_pcCountry);

    cIterator[3].CreateContact(0, 0, cIterator[3].nSizeX(), &cIterator[0], 1,_pcCountry);
    cIterator[3].CreateContact(1, 0, cIterator[3].nSizeY(), &cIterator[2], 1,_pcCountry);
    cIterator[3].CreateContact(2, 0, cIterator[3].nSizeX(), _pFictiveBorders + 2, 1,_pcCountry);
    cIterator[3].CreateContact(3, 0, cIterator[3].nSizeY(), _pFictiveBorders + 3, 1,_pcCountry);

    //Create Fictive Borders

    for(int i = 0; i < 4; i++)
    {    
        _pFictiveBorders[i].Mass() = 0.0f;
        _pFictiveBorders[i].nSizeX() = pcCountry->GetSizeX();
        _pFictiveBorders[i].nSizeY() = pcCountry->GetSizeY();
        _pFictiveBorders[i].fSizeX() = pcCountry->GetStepX() * pcCountry->GetSizeX();
        _pFictiveBorders[i].fSizeY() = pcCountry->GetStepY() * pcCountry->GetSizeY(); 
        _pFictiveBorders[i].MinCountryHeight() = 0.0f;
        _pFictiveBorders[i].MaxCountryHeight() = 0.0f;
        _pFictiveBorders[i].DiffMass() = 0.0f;
        _pFictiveBorders[i].CalculateWaterHeightAndSurfaceFromMass();
        _pFictiveBorders[i].Velocity()[0] = 0.0f;
        _pFictiveBorders[i].Velocity()[1] = 0.0f;
    }

    _pFictiveBorders[0].TopLeftX() = 0;
    _pFictiveBorders[0].TopLeftY() = - (int) pcCountry->GetSizeY();
       
    _pFictiveBorders[1].TopLeftX() = pcCountry->GetSizeX();
    _pFictiveBorders[1].TopLeftY() = 0;   
   
    _pFictiveBorders[2].TopLeftX() = 0;
    _pFictiveBorders[2].TopLeftY() = pcCountry->GetSizeY();
 
    _pFictiveBorders[3].TopLeftX() = - (int) pcCountry->GetSizeX();
    _pFictiveBorders[3].TopLeftY() = 0;

    _pFictiveBorders[0].CreateContact(2, 0, cIterator[0].nSizeX(), &(cIterator[0]), 0,_pcCountry);
    _pFictiveBorders[0].CreateContact(2, cIterator[0].nSizeX() , cIterator[1].nSizeX(), &cIterator[1], 0,_pcCountry);

    _pFictiveBorders[1].CreateContact(3, 0, cIterator[1].nSizeY(), &cIterator[1], 0,_pcCountry);
    _pFictiveBorders[1].CreateContact(3, cIterator[1].nSizeY(), cIterator[2].nSizeY(), &cIterator[2], 0,_pcCountry);

    _pFictiveBorders[2].CreateContact(0, 0, cIterator[3].nSizeX(), &cIterator[3], 0,_pcCountry);
    _pFictiveBorders[2].CreateContact(0, cIterator[2].nSizeX(), cIterator[2].nSizeX(), &cIterator[2], 0,_pcCountry);

    _pFictiveBorders[3].CreateContact(1, 0, cIterator[0].nSizeX(), &cIterator[0], 0,_pcCountry);
    _pFictiveBorders[3].CreateContact(1, cIterator[0].nSizeX(), cIterator[3].nSizeX(), &cIterator[3], 0,_pcCountry);

    // Update _cType a _cWaterMass
    cIterator.ToTop();
    _cWaterMass.ZeroItems();    
    CalculateWaterHeightFromQuadTree(cIterator);
    

    cIterator.ToTop();
    _cType.ZeroItems();
    CalculateNet(cIterator);

    _nSimulations = 0;
    
    return;
}

void CNansen::CalculateNet(CWater2QuadTreeIterator& cIterator)
{
    for(int i = 0; i < 4; i++)
    {
        if (cIterator.IsChild(i))
        {
            cIterator.ToChild(i);
            CalculateNet(cIterator);
            cIterator.ToParent();            
        }
        else
        {
            for(unsigned int j = 0; j < cIterator[i].nSizeX(); j++ )
                _cType.SetItem(cIterator[i].TopLeftX() + j,cIterator[i].TopLeftY(), -1);

            for(unsigned int j = 0; j < cIterator[i].nSizeY(); j++ )
                _cType.SetItem(cIterator[i].TopLeftX(), cIterator[i].TopLeftY() + j, -1);

            for(unsigned int j = 0; j < cIterator[i].nSizeX(); j++ )
                _cType.SetItem(cIterator[i].TopLeftX() + j,cIterator[i].TopLeftY() + cIterator[i].nSizeY() - 1, -1);

            for(unsigned int j = 0; j < cIterator[i].nSizeY(); j++ )
                _cType.SetItem(cIterator[i].TopLeftX() + cIterator[i].nSizeX() - 1,cIterator[i].TopLeftY() + j, -1);

            // Visualization of contacts
           
           /* MyContactArray& cContacts = cIterator[i].Contacts();
            for(unsigned int k = 0; k < cContacts.GetSize(); k++)
            {
                const TCONTACT& tContact = cContacts[k];
                
               if (!tContact._bSimulate)
                    continue;
                
                switch(tContact._tData._iBorder)
                {
                case 0:
                    {
                        unsigned int iFrom =  cIterator[i].TopLeftY() + cIterator[i].nSizeY() / 4;
                        
                        int iTo = iFrom - cIterator[i].nSizeY() / 4 - 
                            tContact._ptSquare->nSizeY() / 4;   
                        
                        iTo = max(0, iTo);                                                
                        
                        for(int j = iFrom; j > iTo; j--)
                            _cType.SetItem(cIterator[i].TopLeftX() + tContact._tData._iFrom + tContact._tData._nSize / 2, j, -1);
                        
                        break;
                        
                    }
                case 1:
                    {
                        unsigned int iFrom =  cIterator[i].TopLeftX() + 3 * cIterator[i].nSizeX() / 4;
                        
                        int iTo = iFrom + cIterator[i].nSizeX() / 4 +
                            tContact._ptSquare->nSizeX() / 4;   
                        
                        iTo = min(_pcCountry->GetSizeX(), iTo);                                                
                        
                        for(int j = iFrom; j < iTo; j++)
                            _cType.SetItem( j, cIterator[i].TopLeftY() + tContact._tData._iFrom + tContact._tData._nSize / 2, -1);
                        
                        break;
                        
                    }
                case 2:
                    {
                        unsigned int iFrom =  cIterator[i].TopLeftY() + 3 * cIterator[i].nSizeY() / 4;
                        
                        int iTo = iFrom + cIterator[i].nSizeY() / 4 +
                            tContact._ptSquare->nSizeY() / 4;   
                        
                        iTo = min(_pcCountry->GetSizeY(), iTo);                                                
                        
                        for(int j = iFrom; j < iTo; j++)
                            _cType.SetItem(cIterator[i].TopLeftX() + tContact._tData._iFrom + tContact._tData._nSize / 2, j, -1);
                        
                        break;
                    } 
                case 3:
                    {
                        unsigned int iFrom = cIterator[i].TopLeftX() + cIterator[i].nSizeX() / 4;
                        
                        int iTo = iFrom - cIterator[i].nSizeX() / 4 -
                            tContact._ptSquare->nSizeX() / 4;   
                        
                        iTo = max(0, iTo);                                                
                        
                        for(int j = iFrom; j > iTo; j--)
                            _cType.SetItem( j, cIterator[i].TopLeftY() + tContact._tData._iFrom + tContact._tData._nSize / 2, -1);
                        
                        break;
                    }  
                default:
                    ASSERT(0);                    
                }
                
            }*/
            
            


        }
    }
}

void CNansen::CalculateWaterHeightFromQuadTree(CWater2QuadTreeIterator& cIterator)
{
    for(int i = 0; i < 4; i++)
    {
        if (cIterator.IsChild(i))
        {
            cIterator.ToChild(i);
            CalculateWaterHeightFromQuadTree(cIterator);
            cIterator.ToParent();
        }
        else
        {
            const CSquare& tSquare = cIterator[i];
            for(unsigned int ix = tSquare.TopLeftX(); ix < tSquare.TopLeftX() + tSquare.nSizeX(); ix++)
            {
                for(unsigned int iy = tSquare.TopLeftY(); iy < tSquare.TopLeftY() + tSquare.nSizeY(); iy++)
                {
                    _cWaterHeight.SetItem(ix,iy,tSquare.AverageWaterHeight());
                    if (tSquare.AverageWaterHeight() > _pcCountry->GetItem(ix,iy))
                        _cWaterMass.SetItem( ix, iy, tSquare.AverageWaterHeight() - _pcCountry->GetItem(ix,iy));
                }
            }
        }
    }    
}

void AddMass(CWater2QuadTreeIterator& cIterator, float fMass)
{
    // TODO: new algorythm, this one is very stupid
    for(int i = 0; i < 100; i++)
    {
        unsigned int iMinHeightSquare = 0;
        float fMinHeight = cIterator[0].AverageWaterHeight();
        for(int j = 1; j < 4; j++)
        {
            if (fMinHeight > cIterator[j].AverageWaterHeight())
            {
                iMinHeightSquare = j;
                fMinHeight = cIterator[j].AverageWaterHeight();
            }
        }

        cIterator[iMinHeightSquare].Mass() += fMass / 100;
        cIterator[iMinHeightSquare].CalculateWaterHeightAndSurfaceFromMass();
    }

    for(int j = 0; j < 4; j++)
        cIterator[j].CalculateWaterHeightAndSurfaceFromMass();
}

void CNansen::Teselate(MyArray<unsigned int>& cZone)
{
    CWater2QuadTreeIterator cIterator(_cQuadTree);

    Teselate( cIterator, cZone);

    // Update _cType a _cWaterMass
    cIterator.ToTop();
    _cWaterMass.ZeroItems();    
    CalculateWaterHeightFromQuadTree(cIterator);

    cIterator.ToTop();
    _cType.ZeroItems();
    CalculateNet(cIterator);
    
}


void CNansen::Teselate(CWater2QuadTreeIterator& cIterator, MyArray<unsigned int>& cZone)
{    
    for(unsigned int i = 0 ; i < cZone.GetSize(); i++)
    {
        ASSERT(cZone[i] < 4);
        if (cIterator.IsChild(cZone[i]))
            cIterator.ToChild(cZone[i]);
        else
        {
           Teselate(cIterator, cZone[i]);
        }
    }
}


void CNansen::EquidistantTeselation(CWater2QuadTreeIterator& cIterator, unsigned int iActualLevel, unsigned int iStopLevel)
{
    
    if (iActualLevel == iStopLevel)
        return;
       
    for(unsigned int i = 0; i < 4; i++)
    {        
        if (cIterator.IsChild(i))
        {        
            cIterator.ToChild(i);
            EquidistantTeselation(cIterator, iActualLevel + 1, iStopLevel);
            cIterator.ToParent();
        }            
        else
        {
            Teselate(cIterator, i);
            EquidistantTeselation(cIterator, iActualLevel + 1, iStopLevel);
            cIterator.ToParent();
        }
    }    
}

void CNansen::EquidistantTeselation(unsigned int iLevel)
{
    CWater2QuadTreeIterator cIterator(_cQuadTree);

    EquidistantTeselation(cIterator, 0, iLevel);
    
    cIterator.ToTop();
    _cType.ZeroItems();
    CalculateNet(cIterator);
    
}

void CNansen::Teselate(CWater2QuadTreeIterator& cIterator, unsigned int iZone)
{ 
    cIterator.CreateChild(iZone);
    CSquare& tParentSquare = cIterator[iZone];
    cIterator.ToChild(iZone);
    
    // Create childs
    for(int j = 0; j < 4; j++)
    {
        cIterator[j].nSizeX() =  tParentSquare.nSizeX() / 2;
        cIterator[j].nSizeY() =  tParentSquare.nSizeY() / 2;
        cIterator[j].fSizeX() =  tParentSquare.fSizeX() / 2;
        cIterator[j].fSizeY() =  tParentSquare.fSizeY() / 2;
        cIterator[j].Mass() =  0;
        cIterator[j].DiffMass() = 0.0f;
        cIterator[j].Velocity()[0] = tParentSquare.Velocity()[0];
        cIterator[j].Velocity()[1] = tParentSquare.Velocity()[1];
        
    }                
    
    cIterator[0].TopLeftX() = tParentSquare.TopLeftX();
    cIterator[0].TopLeftY() = tParentSquare.TopLeftY();
    
    cIterator[1].TopLeftX() = tParentSquare.TopLeftX() + cIterator[0].nSizeX();
    cIterator[1].TopLeftY() = tParentSquare.TopLeftY();
    
    cIterator[2].TopLeftX() = tParentSquare.TopLeftX() + cIterator[0].nSizeX();
    cIterator[2].TopLeftY() = tParentSquare.TopLeftY() + cIterator[0].nSizeY();
    
    cIterator[3].TopLeftX() = tParentSquare.TopLeftX();                        
    cIterator[3].TopLeftY() = tParentSquare.TopLeftY() + cIterator[0].nSizeY();
    
    for(int j = 0; j < 4; j++)
    {
        cIterator[j].SetMinMaxHeightFromCountry(_pcCountry);
        
        //cIterator[j].CalculateWaterHeightAndSurfaceFromMass();
#ifndef USE_MASS_CRITERIUM
        cIterator[j].AverageWaterHeight() = tParentSquare.AverageWaterHeight();
        cIterator[j].CalculateMassFromWaterHeight();
#endif
    }
#ifdef USE_MASS_CRITERIUM
    AddMass(cIterator, tParentSquare.Mass());
#endif
    
    /// Set up new  contacts            
    cIterator[0].CreateContact(1, 0, cIterator[0].nSizeY(), &cIterator[1], 1,_pcCountry);
    cIterator[0].CreateContact(2, 0, cIterator[0].nSizeX(), &cIterator[3], 0,_pcCountry);
    
    cIterator[1].CreateContact(2, 0, cIterator[1].nSizeX(), &cIterator[2], 0,_pcCountry);
    cIterator[1].CreateContact(3, 0, cIterator[1].nSizeY(), &cIterator[0], 0,_pcCountry);
    
    cIterator[2].CreateContact(0, 0, cIterator[2].nSizeX(), &cIterator[1], 1,_pcCountry);
    cIterator[2].CreateContact(3, 0, cIterator[2].nSizeY(), &cIterator[3], 0,_pcCountry);
    
    cIterator[3].CreateContact(0, 0, cIterator[3].nSizeX(), &cIterator[0], 1,_pcCountry);
    cIterator[3].CreateContact(1, 0, cIterator[3].nSizeY(), &cIterator[2], 1,_pcCountry);
    
    /// Resolve old contacts 
    for(unsigned int j = 0; j < tParentSquare.Contacts().GetSize(); j++)
    {
        ResolveOldContact(cIterator, tParentSquare.Contacts()[j], &tParentSquare);          
    }  
}


void CNansen::ResolveOldContact(CWater2QuadTreeIterator& cIterator, 
                                TCONTACT& tContact, 
                                CSquare * pcParentSquare)
{       
    int iBrotherContact;
    CSquare& cBrotherSquare = *tContact._ptSquare;
    
    
    switch (tContact._tData._iBorder)
    {
    case 0: 
        {
            
            iBrotherContact = cBrotherSquare.FindContact(pcParentSquare, tContact._tData._iFrom + pcParentSquare->TopLeftX());
            ASSERT(iBrotherContact >= 0);
            TCONTACT& tBrotherContact = cBrotherSquare.Contacts()[iBrotherContact];
            
            if (tContact._tData._iFrom < cIterator[0].nSizeX())
            {
                // Contact is not divided
                if (tContact._tData._iFrom + tContact._tData._nSize <= cIterator[0].nSizeX())                            
                {            
                    cIterator[0].CreateContact(0, tContact._tData._iFrom, tContact._tData._nSize,
                        tContact._ptSquare, tContact._bSimulate, _pcCountry);
                    
                    tBrotherContact._ptSquare = &cIterator[0];
                }
                else
                {     
                    // Contact is devided into two.                
                    cIterator[0].CreateContact(0, tContact._tData._iFrom, (cIterator[0].nSizeX() - tContact._tData._iFrom),
                        tContact._ptSquare, tContact._bSimulate, _pcCountry);
                    
                    cBrotherSquare.CreateContact(2, tBrotherContact._tData._iFrom, (cIterator[0].nSizeX() - tContact._tData._iFrom),
                        &cIterator[0], tBrotherContact._bSimulate, _pcCountry);
                    
                    ASSERT(tContact._tData._iFrom + tContact._tData._nSize <= cIterator[0].nSizeX() + cIterator[1].nSizeX());

                    TCONTACT& tBrotherContact2 = cBrotherSquare.Contacts()[iBrotherContact];
                    
                    cIterator[1].CreateContact(0, 0, ( tContact._tData._iFrom + tContact._tData._nSize - cIterator[0].nSizeX() ),
                        tContact._ptSquare, tContact._bSimulate, _pcCountry);
                    
                    cBrotherSquare.CreateContact(2, tBrotherContact2._tData._iFrom + (cIterator[0].nSizeX() - tContact._tData._iFrom), 
                        (tContact._tData._iFrom + tContact._tData._nSize - cIterator[0].nSizeX()),
                        &cIterator[1], tBrotherContact2._bSimulate, _pcCountry);
                    
                    cBrotherSquare.Contacts().Delete(iBrotherContact);
                }
            }
            else
            {
                // Contact is not devided
                ASSERT(tContact._tData._iFrom + tContact._tData._nSize <= cIterator[0].nSizeX() + cIterator[1].nSizeX());
                
                cIterator[1].CreateContact(0, tContact._tData._iFrom -  cIterator[0].nSizeX(), tContact._tData._nSize,
                    tContact._ptSquare, tContact._bSimulate, _pcCountry);
                
                tBrotherContact._ptSquare = &cIterator[1];
                
            }
            return;
        }
    case 1:
        {
            
            iBrotherContact = cBrotherSquare.FindContact(pcParentSquare, tContact._tData._iFrom + pcParentSquare->TopLeftY());
            ASSERT(iBrotherContact >= 0);
            TCONTACT& tBrotherContact = cBrotherSquare.Contacts()[iBrotherContact];
            
            if (tContact._tData._iFrom < cIterator[1].nSizeY())
            {
                
                if (tContact._tData._iFrom + tContact._tData._nSize <= cIterator[1].nSizeY())                            
                {            
                    cIterator[1].CreateContact(1, tContact._tData._iFrom, tContact._tData._nSize,
                        tContact._ptSquare, tContact._bSimulate, _pcCountry);
                    
                    tBrotherContact._ptSquare = &cIterator[1];
                }
                else
                {            
                    cIterator[1].CreateContact(1, tContact._tData._iFrom, (cIterator[1].nSizeY() - tContact._tData._iFrom),
                        tContact._ptSquare, tContact._bSimulate, _pcCountry);
                    
                    cBrotherSquare.CreateContact(3, tBrotherContact._tData._iFrom, (cIterator[1].nSizeY() - tContact._tData._iFrom),
                        &cIterator[1], tBrotherContact._bSimulate, _pcCountry);
                     
                    // Previes functions changed array refernec is not more valid
                    TCONTACT& tBrotherContact2 = cBrotherSquare.Contacts()[iBrotherContact];

                    ASSERT(tContact._tData._iFrom + tContact._tData._nSize <= cIterator[1].nSizeY() + cIterator[2].nSizeY());
                    
                    cIterator[2].CreateContact(1, 0, ( tContact._tData._iFrom + tContact._tData._nSize - cIterator[1].nSizeY() ),
                        tContact._ptSquare, tContact._bSimulate, _pcCountry);
                    
                    cBrotherSquare.CreateContact(3, tBrotherContact2._tData._iFrom + (cIterator[1].nSizeY() - tContact._tData._iFrom), 
                        (tContact._tData._iFrom + tContact._tData._nSize - cIterator[1].nSizeY()),
                        &cIterator[2], tBrotherContact2._bSimulate, _pcCountry);
                    
                    cBrotherSquare.Contacts().Delete(iBrotherContact);
                }
            }
            else
            {
                
                ASSERT(tContact._tData._iFrom + tContact._tData._nSize <= cIterator[1].nSizeX() + cIterator[2].nSizeX());
                
                cIterator[2].CreateContact(1, tContact._tData._iFrom -  cIterator[1].nSizeX(), tContact._tData._nSize,
                    tContact._ptSquare, tContact._bSimulate, _pcCountry);
                
                tBrotherContact._ptSquare = &cIterator[2];
            }
            return;
        }
    case 2:  
        {
            
            iBrotherContact = cBrotherSquare.FindContact(pcParentSquare, tContact._tData._iFrom + pcParentSquare->TopLeftX());
            ASSERT(iBrotherContact >= 0);
            TCONTACT& tBrotherContact = cBrotherSquare.Contacts()[iBrotherContact];
            
            if (tContact._tData._iFrom < cIterator[3].nSizeX())
            {
                
                if (tContact._tData._iFrom + tContact._tData._nSize <= cIterator[3].nSizeX())                            
                {            
                    cIterator[3].CreateContact(2, tContact._tData._iFrom, tContact._tData._nSize,
                        tContact._ptSquare, tContact._bSimulate, _pcCountry);
                    
                    tBrotherContact._ptSquare = &cIterator[3];
                }
                else
                {            
                    cIterator[3].CreateContact(2, tContact._tData._iFrom, (cIterator[3].nSizeX() - tContact._tData._iFrom),
                        tContact._ptSquare, tContact._bSimulate, _pcCountry);
                    
                    cBrotherSquare.CreateContact(0, tBrotherContact._tData._iFrom, (cIterator[3].nSizeX() - tContact._tData._iFrom),
                        &cIterator[3], tBrotherContact._bSimulate, _pcCountry);
                                        
                    ASSERT(tContact._tData._iFrom + tContact._tData._nSize <= cIterator[3].nSizeX() + cIterator[2].nSizeX());

                    TCONTACT& tBrotherContact2 = cBrotherSquare.Contacts()[iBrotherContact];
                    
                    cIterator[2].CreateContact(2, 0, ( tContact._tData._iFrom + tContact._tData._nSize - cIterator[3].nSizeX() ),
                        tContact._ptSquare, tContact._bSimulate, _pcCountry);
                    
                    cBrotherSquare.CreateContact(0, tBrotherContact2._tData._iFrom + (cIterator[3].nSizeX() - tContact._tData._iFrom), 
                        (tContact._tData._iFrom + tContact._tData._nSize - cIterator[3].nSizeX()),
                        &cIterator[2], tBrotherContact2._bSimulate, _pcCountry);
                    
                    cBrotherSquare.Contacts().Delete(iBrotherContact);
                }
            }
            else
            {
                
                ASSERT(tContact._tData._iFrom + tContact._tData._nSize <= cIterator[3].nSizeX() + cIterator[2].nSizeX());
                
                cIterator[2].CreateContact(2, tContact._tData._iFrom -  cIterator[3].nSizeX(), tContact._tData._nSize,
                    tContact._ptSquare, tContact._bSimulate, _pcCountry);
                
                tBrotherContact._ptSquare = &cIterator[2];
            }
            return;
        }
    case 3:
        {
            
            iBrotherContact = cBrotherSquare.FindContact(pcParentSquare, tContact._tData._iFrom + pcParentSquare->TopLeftY());
            ASSERT(iBrotherContact >= 0);
            TCONTACT& tBrotherContact = cBrotherSquare.Contacts()[iBrotherContact];
            
            if (tContact._tData._iFrom < cIterator[0].nSizeY())
            {
                
                if (tContact._tData._iFrom + tContact._tData._nSize <= cIterator[0].nSizeY())                            
                {            
                    cIterator[0].CreateContact(3, tContact._tData._iFrom, tContact._tData._nSize,
                        tContact._ptSquare, tContact._bSimulate, _pcCountry);
                    
                    tBrotherContact._ptSquare = &cIterator[0];
                }
                else
                {            
                    cIterator[0].CreateContact(3, tContact._tData._iFrom, (cIterator[0].nSizeY() - tContact._tData._iFrom),
                        tContact._ptSquare, tContact._bSimulate, _pcCountry);
                    
                    cBrotherSquare.CreateContact(1, tBrotherContact._tData._iFrom, (cIterator[0].nSizeY() - tContact._tData._iFrom),
                        &cIterator[0], tBrotherContact._bSimulate, _pcCountry);
                    
                    ASSERT(tContact._tData._iFrom + tContact._tData._nSize <= cIterator[0].nSizeY() + cIterator[3].nSizeY());

                    TCONTACT& tBrotherContact2 = cBrotherSquare.Contacts()[iBrotherContact];
                    
                    cIterator[3].CreateContact(3, 0, ( tContact._tData._iFrom + tContact._tData._nSize - cIterator[0].nSizeY() ),
                        tContact._ptSquare, tContact._bSimulate, _pcCountry);
                    
                    cBrotherSquare.CreateContact(1, tBrotherContact2._tData._iFrom + (cIterator[0].nSizeY() - tContact._tData._iFrom), 
                        (tContact._tData._iFrom + tContact._tData._nSize - cIterator[0].nSizeY()),
                        &cIterator[3], tBrotherContact2._bSimulate, _pcCountry);
                    
                    cBrotherSquare.Contacts().Delete(iBrotherContact);
                }
            }
            else
            {
                
                ASSERT(tContact._tData._iFrom + tContact._tData._nSize <= cIterator[0].nSizeX() + cIterator[3].nSizeX());
                
                cIterator[3].CreateContact(3, tContact._tData._iFrom -  cIterator[0].nSizeX(), tContact._tData._nSize,
                    tContact._ptSquare, tContact._bSimulate, _pcCountry);
                
                tBrotherContact._ptSquare = &cIterator[3];
            }
            return;
        }
    default:
        ASSERT(0);
    }
}

void CNansen::Simulate()
{
    _nSimulations ++;
    //if (_nSimulations < 1000)
        _pFictiveBorders[3].Mass() = 300.0f;
   // else
   //     _pFictiveBorders[3].Mass() =    0.0f;
        
    _pFictiveBorders[3].AverageWaterHeight() = 350.0f;

    _pFictiveBorders[2].Mass() = 0.0f;
    _pFictiveBorders[2].AverageWaterHeight() = 30000.0f;

    _pFictiveBorders[0].Mass() = 0.0f;
    _pFictiveBorders[0].AverageWaterHeight() = 30000.0f;


    CWater2QuadTreeIterator cIterator(_cQuadTree);
    float fSimTime = 2.00f;
    float fSpentTime = 0.0;
    do 
    {
        float fTime = min(fSimTime - fSpentTime, fSimTime/5);

        SimulateLeaf(cIterator);

        cIterator.ToTop();

        SimulateLeaf2(fTime, cIterator);

        cIterator.ToTop();

        SimulateLeaf3(fTime, cIterator);

        fSpentTime += fTime;
    } while (fSpentTime + 0.01 < fSimTime);

    
    cIterator.ToTop();
    _cWaterMass.ZeroItems();    
    CalculateWaterHeightFromQuadTree(cIterator);
   
}

__forceinline float CalculateDiffSpace(const TCONTACTDATA& tContactData, const float fMaxHeight, 
                         float fMinHeight)
{

    fMinHeight = max(tContactData._fMinBorderHeight, fMinHeight);

    if (fMinHeight >= tContactData._fMaxBorderHeight)
        return tContactData._fSize * (fMaxHeight - fMinHeight);

    float fMinHeightSpace = (fMinHeight - tContactData._fMinBorderHeight) * tContactData._fSize * (fMinHeight - tContactData._fMinBorderHeight) / 
        (tContactData._fMaxBorderHeight- tContactData._fMinBorderHeight) / 2.0f;

    float fMaxHeightSpace;
    if (fMaxHeight >= tContactData._fMaxBorderHeight)
    {
        fMaxHeightSpace = tContactData._fSize * ((fMaxHeight - tContactData._fMaxBorderHeight) + 
            (tContactData._fMaxBorderHeight- tContactData._fMinBorderHeight) / 2.0f);
    }
    else
    {
        fMaxHeightSpace = (fMaxHeight - tContactData._fMinBorderHeight) * tContactData._fSize * (fMaxHeight - tContactData._fMinBorderHeight) / 
            (tContactData._fMaxBorderHeight- tContactData._fMinBorderHeight) / 2.0f;
    }

    return fMaxHeightSpace - fMinHeightSpace;
        

}

void CNansen::SimulateLeaf(CWater2QuadTreeIterator& cIterator)
{
    DebugPoint();
    
    for(int i = 0; i < 4; i++)
    {
        if (cIterator.IsChild(i))
        {                                                
            cIterator.ToChild(i);                                   
            SimulateLeaf(cIterator);
            cIterator.ToParent();
        }
        else
        {
            // Simulate
            
            MyContactArray& cContacts = cIterator[i].Contacts();
                        
            for(unsigned int j = 0; j < cContacts.GetSize(); j++)
            {
                TCONTACT& tContact = cContacts[j];
                if (!tContact._bSimulate)
                    continue;

                CSquare& cBrotherSquare = * tContact._ptSquare;
                                    
                if (cIterator[i].AverageWaterHeight() > cBrotherSquare.AverageWaterHeight() &&   
                        !IsNumericalZero(cIterator[i].Mass()) &&
                        !IsNumericalZero(cIterator[i].AverageWaterHeight() - cBrotherSquare.AverageWaterHeight()) && 
                        cIterator[i].AverageWaterHeight() > tContact._tData._fMinBorderHeight)
                {
                        
                    float fFlow =  0.3f * 
                          CalculateDiffSpace(tContact._tData, cIterator[i].AverageWaterHeight(), 
                          cBrotherSquare.AverageWaterHeight()) /  
                          //tContact._tData._fSize * (cIterator[i].AverageWaterHeight() - cBrotherSquare.AverageWaterHeight()) /
                          (cIterator[i].fSizeY() + cBrotherSquare.fSizeY());

                    cIterator[i].DiffMass() -= fFlow;
                    cBrotherSquare.DiffMass() += fFlow;

                    continue;
                }

                if (cIterator[i].AverageWaterHeight() < cBrotherSquare.AverageWaterHeight() &&   
                        !IsNumericalZero(cBrotherSquare.Mass()) &&
                        !IsNumericalZero(cIterator[i].AverageWaterHeight() - cBrotherSquare.AverageWaterHeight()) && 
                        cBrotherSquare.AverageWaterHeight() > tContact._tData._fMinBorderHeight)
                {
                        
                    float fFlow =  0.3f * 
                          CalculateDiffSpace(tContact._tData, cBrotherSquare.AverageWaterHeight(), 
                          cIterator[i].AverageWaterHeight()) /  
                          //  tContact._tData._fSize * (cBrotherSquare.AverageWaterHeight() - cIterator[i].AverageWaterHeight()) /
                          (cIterator[i].fSizeY() + cBrotherSquare.fSizeY());

                    cIterator[i].DiffMass() += fFlow;
                    cBrotherSquare.DiffMass() -= fFlow;

                    continue;
                }
            }            
        }
    }
}

void CNansen::SimulateLeaf2(float & fMinTime, CWater2QuadTreeIterator& cIterator)
{
    for(int i = 0; i < 4; i++)
    {
        if (cIterator.IsChild(i))
        {                                                
            cIterator.ToChild(i);                                   
            SimulateLeaf2(fMinTime, cIterator);
            cIterator.ToParent();
        }
        else
        {
            if (cIterator[i].DiffMass() < 0.0f && fMinTime > - cIterator[i].Mass() / cIterator[i].DiffMass())
                fMinTime = - cIterator[i].Mass() / cIterator[i].DiffMass();
        }
    }

}

void CNansen::SimulateLeaf3(const float fMinTime, CWater2QuadTreeIterator& cIterator)
{
    for(int i = 0; i < 4; i++)
    {
        if (cIterator.IsChild(i))
        {                                                
            cIterator.ToChild(i);                                   
            SimulateLeaf3(fMinTime, cIterator);
            cIterator.ToParent();
        }
        else
        {
            cIterator[i].Mass() += cIterator[i].DiffMass() * fMinTime;
            cIterator[i].DiffMass() = 0.0f;

           // if (IsNumericalZero(cIterator[i].Mass()))
           //     cIterator[i].Mass() = 0.0f;

            cIterator[i].CalculateWaterHeightAndSurfaceFromMass();



        }
    }

    
}

void CNansen::DesolveOldContact(unsigned int iBorder, 
                                unsigned int iSquare0, 
                                unsigned int iSquare1, 
                                CWater2QuadTreeIterator& cIterator, 
                                CSquare& cParentSquare)
{
    MyContactArray cArray;

    const int nContacts0 = cIterator[iSquare0].RetrieveContacts( cArray, iBorder);
    const int nContacts1 = cIterator[iSquare1].RetrieveContacts( cArray, iBorder);
    unsigned int nParentContacts = cParentSquare.Contacts().GetSize();

    for(int i = 0; i <  nContacts0; i ++)
    { 
        const TCONTACT& cContact0 = cArray[i];

        int iContact1ID = -1;
        for(unsigned int j = nContacts0; j < cArray.GetSize(); j++)
        {
            if (cContact0._ptSquare == cArray[j]._ptSquare)
            {
                iContact1ID = j;
                break;
            }
        }

        
        if (iContact1ID == -1)
        {
            TCONTACT tParentContact;
            memcpy( &tParentContact, &cContact0, sizeof(tParentContact));

            if (tParentContact._tData._iBorder % 2 == 0)
            {            
                tParentContact._tData._iFrom = cIterator[iSquare0].TopLeftX() + cContact0._tData._iFrom - cParentSquare.TopLeftX();
                tParentContact._ptSquare->FindContact2(&cIterator[iSquare0], tParentContact._tData._iFrom + cParentSquare.TopLeftX())._ptSquare = &cParentSquare;
            }
            else
            {            
                tParentContact._tData._iFrom = cIterator[iSquare0].TopLeftY() + cContact0._tData._iFrom - cParentSquare.TopLeftY();
                tParentContact._ptSquare->FindContact2(&cIterator[iSquare0], tParentContact._tData._iFrom + cParentSquare.TopLeftY())._ptSquare = &cParentSquare;
            }                                        

            cParentSquare.Contacts().Add(tParentContact);
        }
        else
        {
            const TCONTACT& cContact1 = cArray[iContact1ID];

            ASSERT(cContact0._bSimulate == cContact1._bSimulate);
            ASSERT(cContact0._ptSquare == cContact1._ptSquare);
            ASSERT(cContact0._tData._iBorder == cContact1._tData._iBorder);            
            
            CSquare& cBrotherSquare = *cContact0._ptSquare;
            int iBrother0, iBrother1;

            if (cContact0._tData._iBorder % 2 == 0)
            {            
                cParentSquare.CreateContact(cContact0._tData._iBorder, cIterator[iSquare0].TopLeftX() + cContact0._tData._iFrom - cParentSquare.TopLeftX(),
                    cContact0._tData._nSize + cContact1._tData._nSize,
                    cContact0._ptSquare, cContact0._bSimulate, _pcCountry);

                iBrother0 = cBrotherSquare.FindContact(&cIterator[iSquare0], cIterator[iSquare0].TopLeftX() + cContact0._tData._iFrom);
                iBrother1 = cBrotherSquare.FindContact(&cIterator[iSquare1], cIterator[iSquare1].TopLeftX() + cContact1._tData._iFrom);
            }
            else
            {
                cParentSquare.CreateContact(cContact0._tData._iBorder, cIterator[iSquare0].TopLeftY() + cContact0._tData._iFrom - cParentSquare.TopLeftY(),
                    cContact0._tData._nSize + cContact1._tData._nSize,
                    cContact0._ptSquare, cContact0._bSimulate, _pcCountry);

                iBrother0 = cBrotherSquare.FindContact(&cIterator[iSquare0], cIterator[iSquare0].TopLeftY() + cContact0._tData._iFrom);
                iBrother1 = cBrotherSquare.FindContact(&cIterator[iSquare1], cIterator[iSquare1].TopLeftY() + cContact1._tData._iFrom);
                    
            }

            ASSERT(iBrother0 >= 0);
            ASSERT(iBrother1 >= 0);
            
            cBrotherSquare.CreateContact(cBrotherSquare.Contacts()[iBrother0]._tData._iBorder, cBrotherSquare.Contacts()[iBrother0]._tData._iFrom,
                cContact0._tData._nSize + cContact1._tData._nSize,
                &cParentSquare, !cContact0._bSimulate, _pcCountry);

            cBrotherSquare.Contacts().Delete(iBrother0);
            if (iBrother1 > iBrother0)
                cBrotherSquare.Contacts().Delete(iBrother1 - 1);
            else
                cBrotherSquare.Contacts().Delete(iBrother1);
            
            cArray.Delete(iContact1ID);
        }
    }

    for(unsigned int i = nContacts0; i < cArray.GetSize(); i ++)
    {
        TCONTACT tParentContact;
        memcpy( &tParentContact, &cArray[i], sizeof(tParentContact));
        
        if (tParentContact._tData._iBorder % 2 == 0)
        {
        
            tParentContact._tData._iFrom = cIterator[iSquare1].TopLeftX() + cArray[i]._tData._iFrom - cParentSquare.TopLeftX();
            tParentContact._ptSquare->FindContact2(&cIterator[iSquare1], tParentContact._tData._iFrom + cParentSquare.TopLeftX())._ptSquare = &cParentSquare;
        }
        else
        {        
            tParentContact._tData._iFrom = cIterator[iSquare1].TopLeftY() + cArray[i]._tData._iFrom - cParentSquare.TopLeftY();
            tParentContact._ptSquare->FindContact2(&cIterator[iSquare1], tParentContact._tData._iFrom + cParentSquare.TopLeftY())._ptSquare = &cParentSquare;
        }
                  
        cParentSquare.Contacts().Add(tParentContact);
    }
}

void CNansen::DeTeselate(CWater2QuadTreeIterator& cIterator, unsigned int iChild)
{

    CSquare& cParentSquare = cIterator[iChild];
    cParentSquare.Contacts().Release();

    ASSERT(cIterator.IsChild(iChild));

    cIterator.ToChild(iChild);

    for(int i = 0; i < 4; i++)
    {
        if (cIterator.IsChild(i))
        {            
            DeTeselate(cIterator,i);
        }
    }

    DesolveOldContact(0,0,1, cIterator, cParentSquare);
    DesolveOldContact(1,1,2, cIterator, cParentSquare);
    DesolveOldContact(2,3,2, cIterator, cParentSquare);
    DesolveOldContact(3,0,3, cIterator, cParentSquare);

    
    cParentSquare.DiffMass() = 0;
#ifdef USE_MASS_CRITERIUM
    cParentSquare.Mass() = cIterator[0].Mass() + cIterator[1].Mass() + cIterator[2].Mass() + cIterator[3].Mass();
    cParentSquare.CalculateWaterHeightAndSurfaceFromMass();
#else
    float fAverageHeight = 0.0f;
    unsigned int nN = 0;

    for(int i = 0; i < 4; i++)
    {
        if (cIterator[i].Mass() != 0.0f && fAverageHeight < cIterator[i].AverageWaterHeight())
        {
            fAverageHeight = cIterator[i].AverageWaterHeight();
            nN++;
        }
    }
    
    cParentSquare.AverageWaterHeight() = fAverageHeight ;
    cParentSquare.CalculateMassFromWaterHeight();
#endif

    cParentSquare.Velocity()[0] = (cIterator[0].Velocity()[0] +  cIterator[1].Velocity()[0] + 
         cIterator[2].Velocity()[0] +  cIterator[3].Velocity()[0]) / 4.0f;
    cParentSquare.Velocity()[1] = (cIterator[0].Velocity()[1] +  cIterator[1].Velocity()[1] + 
         cIterator[2].Velocity()[1] +  cIterator[3].Velocity()[1]) / 4.0f;

    cIterator.ToParent();

    cIterator.DeleteChild(iChild);    
}

void CNansen::DeTeselate(CWater2QuadTreeIterator& cIterator, MyArray<unsigned int>& cZone)
{
    for(unsigned int i = 0; i < cZone.GetSize() - 1; i++)
    {
        if (cIterator.IsChild(cZone[i]))                                                        
            cIterator.ToChild(cZone[i]);         
        else
            return; // No deteselation needed.         
    }
    
    unsigned int iLastZoneId = cZone[cZone.GetSize() - 1];
    if (!cIterator.IsChild(iLastZoneId))
        return; // No deteselation needed.         

    DeTeselate(cIterator,iLastZoneId);
}

void CNansen::DeTeselate(MyArray<unsigned int>& cZone)
{
    CWater2QuadTreeIterator cIterator(_cQuadTree);

    DeTeselate(cIterator, cZone);

    cIterator.ToTop();
    _cWaterMass.ZeroItems();    
    CalculateWaterHeightFromQuadTree(cIterator);

    cIterator.ToTop();
    _cType.ZeroItems();
    CalculateNet(cIterator);
    
}

float CalculateMinDistanceToSquare(const float x,
                                   const float y,
                                   const float fSquareOriginX, 
                                   const float fSquareOriginY, 
                                   const float fSizeX, 
                                   const float fSizeY)
{
    int iVoronyX;
    int iVoronyY;

    if (x < fSquareOriginX)
        iVoronyX = -1;
    else
    {
        if (x <= fSquareOriginX + fSizeX)
            iVoronyX = 0;
        else
            iVoronyX = 1;
    }

    if (y < fSquareOriginY)
        iVoronyY = -1;
    else
    {
        if (y <= fSquareOriginY + fSizeY)
            iVoronyY = 0;
        else
            iVoronyY = 1;
    }

    if (iVoronyX == 0 && iVoronyY == 0)
        return 0.0f;

    if (iVoronyX == 0)
    {
        return fabsf(y - (fSquareOriginY + (iVoronyY == 1 ? fSizeY : 0.0f)));
    }

    if (iVoronyY == 0)
    {
        return fabsf(x - (fSquareOriginX + (iVoronyX == 1 ? fSizeX : 0.0f)));
    }

    float fDiffX = x - (fSquareOriginX + (iVoronyX == 1 ? fSizeX : 0.0f));
    float fDiffY = y - (fSquareOriginY + (iVoronyY == 1 ? fSizeY : 0.0f));

    return sqrtf(fDiffX * fDiffX + fDiffY * fDiffY);
}

void CNansen::CreatePatternTree(CPatternTreeIterator cIterator,
                                const float x, 
                                const float y, 
                                const float fSquareOriginX, 
                                const float fSquareOriginY, 
                                const float fSizeX, 
                                const float fSizeY,
                                const float fStopSize)
{
    for(int i = 0; i < 4; i++)
    {
        float fActualSquareOriginX = fSquareOriginX;
        float fActualSquareOriginY = fSquareOriginY;

        switch (i)
        {
        case 0:
            break;
        case 1:
            fActualSquareOriginX += fSizeX/2;
            break;
        case 2:
            fActualSquareOriginX += fSizeX/2;
            fActualSquareOriginY += fSizeY/2;
            break;
        case 3:
            fActualSquareOriginY += fSizeY/2;
            break;            
        default:;
        }
        
        float fMinDistance = CalculateMinDistanceToSquare(x,y,fActualSquareOriginX, fActualSquareOriginY, fSizeX/2, fSizeY/2);
        if (fMinDistance < (fSizeX + fSizeY) / 8 && (fSizeX + fSizeY) / 4 > fStopSize)
        {
            cIterator.CreateChild(i);
            cIterator.ToChild(i);

            CreatePatternTree(cIterator, x, y, 
                fActualSquareOriginX, 
                fActualSquareOriginY, 
                fSizeX / 2, 
                fSizeY / 2, 
                fStopSize);
            cIterator.ToParent();
        }
    }
}

void CNansen::SynchronizeWithPatternTree(CPatternTreeIterator& cPatternIterator, CWater2QuadTreeIterator& cIterator)
{
    for(int i = 0; i < 4; i++)
    {
        if (cPatternIterator.IsChild(i) && cIterator.IsChild(i))
        {
            cPatternIterator.ToChild(i);
            cIterator.ToChild(i);

            SynchronizeWithPatternTree(cPatternIterator, cIterator);

            cPatternIterator.ToParent();
            cIterator.ToParent();
        }
        else
        {
            if (cPatternIterator.IsChild(i))
            {
                // Teselate
                MyArray<unsigned int> cZone;
                cZone.Add(i);
                Teselate(cIterator, cZone);

                cPatternIterator.ToChild(i);
                //cIterator.ToChild(i); // Is allready in child because of teselation.

                SynchronizeWithPatternTree(cPatternIterator, cIterator);

                cPatternIterator.ToParent();
                cIterator.ToParent();
            }
            else
                if (cIterator.IsChild(i))
                {
                    // deteselate                    
                    DeTeselate(cIterator,i);                    
                }
        }
    }
}

void CNansen::MoveTeselationToPoint(float x, float y, unsigned int iLowestLevel)
{
    CPatternTree cPatternTree;
    CPatternTreeIterator cPatternIterator(cPatternTree);

    CreatePatternTree(cPatternIterator,
                                x, 
                                y, 
                                0.0f, 
                                0.0f, 
                                _pcCountry->GetSizeX() * _pcCountry->GetStepX(), 
                                _pcCountry->GetSizeY() * _pcCountry->GetStepY(),
                                _pcCountry->GetSizeX() * _pcCountry->GetStepX() / (1 << iLowestLevel));
    cPatternIterator.ToTop();

    CWater2QuadTreeIterator cIterator(_cQuadTree);

    SynchronizeWithPatternTree(cPatternIterator, cIterator);

    // Update _cType a _cWaterMass
    cIterator.ToTop();
    _cWaterMass.ZeroItems();    
    CalculateWaterHeightFromQuadTree(cIterator);

    cIterator.ToTop();
    _cType.ZeroItems();
    CalculateNet(cIterator);
    
}

void CNansen::CountryChanged()
{
    CWater2QuadTreeIterator cIterator(_cQuadTree);

    RefreshCountryDep(cIterator);

    cIterator.ToTop();
    _cWaterMass.ZeroItems();    
    CalculateWaterHeightFromQuadTree(cIterator);
    

    
}

void CNansen::RefreshCountryDep(CWater2QuadTreeIterator& cIterator)
{
    for(int i = 0; i < 4; i++)
    {
        cIterator[i].SetMinMaxHeightFromCountry(_pcCountry);        

        if (cIterator.IsChild(i))
        {
            cIterator.ToChild(i);
            RefreshCountryDep(cIterator);            
            cIterator.ToParent();
        }
        else
        {
            cIterator[i].RefreshContactsCountryDep(_pcCountry);
            cIterator[i].CalculateWaterHeightAndSurfaceFromMass();
        }
    }   
}