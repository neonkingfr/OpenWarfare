#include "stdafx.h"
#include "Peary.h"

#define BETTER_BORDERS
//#define USE_MASS_CRITERIUM


extern int g_iStep;
extern const int g_iStop;

extern void DebugPoint();

const float K1 = 10.0f;
const float K2 = 0.1f;


const float MIN_WATER_SURFACE_COEF = 0.01f;

const float NUMERICAL_ZERO = 0.00001f;

__forceinline int IsNumericalZero(float fValue)
{
    return (fValue > -NUMERICAL_ZERO && fValue < NUMERICAL_ZERO);
}

//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//              C O U N T R Y  P A R C E L
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

void CCountryParcel::Init(const int iTopLeftX, 
         const int iTopLeftY,
         const unsigned int nSizeX,
         const unsigned int nSizeY,
         const CCountry * pcCountry)
{
  ::LogF("iTopLeftX %d, iTopLeftY %d", iTopLeftX, iTopLeftY );
    _fSizeX = pcCountry->GetStepX() * nSizeX;
    _fSizeY = pcCountry->GetStepY() * nSizeY;
   
    pcCountry->GetMassOnHeight(iTopLeftX, iTopLeftY, nSizeX, nSizeY,
        _nTilles, _pfMassOnHeight, _fMinCountryHeight, _fMassOnHeightStep);

    _fMaxCountryHeight = _fMinCountryHeight + _nTilles * _fMassOnHeightStep;

    _fHeightOnMassStep = _pfMassOnHeight[_nTilles - 1] / _nTilles;

    if (_fHeightOnMassStep == 0.0f)
    {
        for(unsigned int i = 0; i < _nTilles; i++)
        {
            _pfHeightOnMass[i] = _fMinCountryHeight;
        }

        return;        
    }
    
    // field _pfHeightOnMass is created according to field _pfMassOnHeight. Error is of course bigger, but it is fast

    for(unsigned int i = 0; i < _nTilles; i++)
    {
        float fMass = (i + 1) * _fHeightOnMassStep;
        
        unsigned int j = 0;
        for(;fMass > _pfMassOnHeight[j]; j++)
        {
            ASSERT(j < _nTilles - 1);            
        }

        // fMass <= _pfMassOnHeight[j]

        _pfHeightOnMass[i] = _fMinCountryHeight + _fMassOnHeightStep * (j + 1 -  (_pfMassOnHeight[j] - fMass) / (_pfMassOnHeight[j] - ((j == 0) ? 0.0f :_pfMassOnHeight[j - 1])));
    }
}

void CCountryParcel::InitOnConstantHeight(
         const float fHeight,
         const unsigned int nSizeX,
         const unsigned int nSizeY,
         const CCountry * pcCountry)
{
    _fSizeX = pcCountry->GetStepX() * nSizeX;
    _fSizeY = pcCountry->GetStepY() * nSizeY;

    _fHeightOnMassStep = 0.0f;
    _fMassOnHeightStep = 0.0f;

    _fMinCountryHeight = fHeight;
    _fMaxCountryHeight = fHeight;

    memset( _pfMassOnHeight, 0x0, _nTilles * sizeof(*_pfMassOnHeight));
    _pfHeightOnMass[_nTilles -1] = fHeight;
}

void CCountryParcel::CalculateMassAndSurfaceFromHeight(float& fMass, float& fSurface, float fHeight)
{
    // Find position in table 
    fHeight -= _fMinCountryHeight;

    if (fHeight <= 0)
    {
        fMass = 0.0f;

        fSurface = (_fMassOnHeightStep == 0.0f) ? _fSizeX * _fSizeY : _pfMassOnHeight[0] / _fMassOnHeightStep;
        return;
    }

    unsigned int iPos = (_fMassOnHeightStep == 0.0f) ? _nTilles : fHeight / _fMassOnHeightStep;

    if (iPos < _nTilles)
    {
        // Height is in the table
        fMass = (iPos == 0)? 0 : _pfMassOnHeight[iPos - 1];
        fSurface = (_pfMassOnHeight[iPos] - fMass) / _fMassOnHeightStep;
        fMass += (fHeight - iPos * _fMassOnHeightStep) * fSurface;
    }
    else
    {
        fMass = _pfMassOnHeight[_nTilles - 1];
        fSurface = _fSizeX * _fSizeY;
        fMass += (fHeight - _nTilles * _fMassOnHeightStep) * fSurface;
    }
}

void CCountryParcel::CalculateHeightAndSurfaceFromMass(float& fHeight, float& fSurface, const float fMass)
{
    // Find position in table
    unsigned int iPos = (_fHeightOnMassStep == 0.0f) ? _nTilles : fMass / _fHeightOnMassStep;

    if (iPos < _nTilles)
    {
        // Mass is in table
        fHeight = (iPos == 0) ? _fMinCountryHeight : _pfHeightOnMass[iPos - 1];
        fSurface = (_pfHeightOnMass[iPos] - fHeight) / _fHeightOnMassStep;
        fHeight += (fMass - iPos * _fHeightOnMassStep) * fSurface;
        fSurface = 1.0f / fSurface;
    }
    else
    {
        fHeight = _pfHeightOnMass[_nTilles - 1];
        fSurface = _fSizeX * _fSizeY;
        fHeight += (fMass - _nTilles * _fHeightOnMassStep) / fSurface;
    }
}

float CCountryParcel::CalculateFaceInXFromHeight(const float fHeight)
{
    float fMass;
    float fSurface;

    CalculateMassAndSurfaceFromHeight(fMass, fSurface, fHeight);

    return fMass / _fSizeY;
}

float CCountryParcel::CalculateFaceInYFromHeight(const float fHeight)
{
    float fMass;
    float fSurface;

    CalculateMassAndSurfaceFromHeight(fMass, fSurface, fHeight);

    return fMass / _fSizeX;
}

const CCountryParcel& CCountryParcel::operator=(const CCountryParcel& cParcel)
{
    _fMinCountryHeight = cParcel._fMinCountryHeight;
    _fMaxCountryHeight = cParcel._fMaxCountryHeight;
    _fSizeX = cParcel._fSizeX;
    _fSizeY = cParcel._fSizeY;
    _fHeightOnMassStep = cParcel._fHeightOnMassStep;
    _fMassOnHeightStep = cParcel._fMassOnHeightStep;

    memmove(_pfMassOnHeight, cParcel._pfMassOnHeight, sizeof(*_pfMassOnHeight) * _nTilles);
    memmove(_pfHeightOnMass, cParcel._pfMassOnHeight, sizeof(*_pfHeightOnMass) * _nTilles);  
    
    return *this;
}

//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//              P E A R Y S Q U A R E
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

void CPearySquare::Init(int iTopLeftX, int iTopLeftY, 
        unsigned int SizeX, unsigned int SizeY, const CCountry * pcCountry)
{
    _iTopLeftX = iTopLeftX;
    _iTopLeftY = iTopLeftY;

    _nSizeX = SizeX;
    _nSizeY = SizeY;

    _fSizeX = SizeX * pcCountry->GetStepX();
    _fSizeY = SizeY * pcCountry->GetStepY();

    if (iTopLeftX < 0 || iTopLeftX >= pcCountry->GetSizeX() ||
        iTopLeftY < 0 || iTopLeftY >= pcCountry->GetSizeY())
    {
        // fictive border
        _cParcel._fSizeX = _fSizeX;
        _cParcel._fSizeX = _fSizeY;

        _cParcel._fHeightOnMassStep = 0.0f;
        _cParcel._fMassOnHeightStep = 0.0f;

        _cParcel._fMinCountryHeight = 0.0f;
        _cParcel._fMaxCountryHeight = 0.0f;

        _fWaterSurface = _fSizeX * _fSizeY;
    }
    else
        _cParcel.Init(_iTopLeftX, _iTopLeftY, _nSizeX, _nSizeY, pcCountry);

   
        
    _fMass = 0.0f;
    _fDiffMass = 0.0f;

    _cParcel.CalculateHeightAndSurfaceFromMass(_fAverageWaterHeight, _fWaterSurface, _fMass);

}


/*void CPearySquare::SetMinMaxHeightFromCountry(CCountry * pcCountry)
{      
    _cParcel.Init(_iTopLeftX, _iTopLeftY, _nSizeX, _nSizeY, pcCountry);
    _fAverageWaterHeight = _cParcel._fMinCountryHeight;
}*/
     
void CPearySquare::CalculateWaterHeightAndSurfaceFromMass()
{    
    _cParcel.CalculateHeightAndSurfaceFromMass(_fAverageWaterHeight, _fWaterSurface, _fMass);
    /*if (_fMass > 0.0f)
    {
        float fTriangle = _fSizeX * _fSizeY * 
            (_fMaxCountryHeight - _fMinCountryHeight) / 2;

        if (_fMass > fTriangle)
        {
            _fAverageWaterHeight = _fMaxCountryHeight + 
                (_fMass - fTriangle) / (_fSizeX * _fSizeY);

            _fWaterSurface = (_fSizeX * _fSizeY);
        }
        else
        {
            _fAverageWaterHeight = sqrtf(_fMass / (_fSizeX * _fSizeY) * 2.0f * 
                (_fMaxCountryHeight - _fMinCountryHeight));

            _fWaterSurface = (_fSizeX * _fSizeY) * _fAverageWaterHeight/(_fMaxCountryHeight - _fMinCountryHeight);

            _fAverageWaterHeight += _fMinCountryHeight;
        }
    }
    else
    {
        _fMass = 0.0f;
        _fAverageWaterHeight = _fMinCountryHeight;
        if (!IsNumericalZero(_fMaxCountryHeight - _fMinCountryHeight))
            _fWaterSurface = MIN_WATER_SURFACE_COEF * (_fSizeX * _fSizeY);
        else
            _fWaterSurface = (_fSizeX * _fSizeY);
    }

    _fWaterSurface = max( _fWaterSurface, MIN_WATER_SURFACE_COEF * (_fSizeX * _fSizeY));   
    */
}

void CPearySquare::CalculateMassFromWaterHeight()
{
    _cParcel.CalculateMassAndSurfaceFromHeight(_fMass, _fWaterSurface, _fAverageWaterHeight); 
    
    if (_fAverageWaterHeight < _cParcel.GetMinCoutryHeight())
        _fAverageWaterHeight = _cParcel.GetMinCoutryHeight();
}

void CPearySquare::CreateContact(const int iBorder, 
                            const unsigned int iFrom, 
                            const unsigned int nSize, 
                            CPearySquare * pSquare, 
                            const int bSimulate,
                            const CCountry * pcCountry,
                            float fSpeed)
{
    ASSERT(iFrom + nSize <= _nSizeX);
    TPEARYCONTACT tContact;
    memset(&tContact, 0x0, sizeof(tContact));

    tContact._iBorder = iBorder;
    tContact._iFrom = iFrom;
    tContact._nSize = nSize;
    tContact._bSimulate = bSimulate;
    tContact._ptSquare = pSquare;
    tContact._fSpeed = fSpeed;
    tContact._fNewSpeed = 0.0f;

    if (iBorder == 0 || iBorder == 2)
        tContact._fSize = _fSizeX * nSize / _nSizeX;
    else
        tContact._fSize = _fSizeY * nSize / _nSizeY;

    ::LogF("_iTopLeftX %d, _iTopLeftY %d", _iTopLeftX, _iTopLeftY );
    // Set up country info
    if (!(_iTopLeftX < 0 || _iTopLeftY < 0 ||
        _iTopLeftX >= pcCountry->GetSizeX() || _iTopLeftY >= pcCountry->GetSizeY()))
    /*{
        // Fictive square 
        if (iBorder == 0 || iBorder == 2)
            tContact._cBorderParcel.InitOnConstantHeight(_cParcel.GetMinCoutryHeight(), nSize, 1, pcCountry);
        else
            tContact._cBorderParcel.InitOnConstantHeight(_cParcel.GetMinCoutryHeight(), 1, nSize, pcCountry);             
    }
    else*/
    {    
        // Normal square
        switch (iBorder)
        {
        case 0:
            tContact._cBorderParcel.Init(_iTopLeftX + iFrom, _iTopLeftY, nSize, 1, pcCountry);
            break;
        case 1:            
            tContact._cBorderParcel.Init( _iTopLeftX + _nSizeX - 1, _iTopLeftY + iFrom, 1, nSize, pcCountry);
            break;
        case 2:
            tContact._cBorderParcel.Init(_iTopLeftX + iFrom, _iTopLeftY + _nSizeY - 1, nSize, 1, pcCountry);
            break;
        case 3:
            ::LogF("_iTopLeftX %d, _iTopLeftY %d", _iTopLeftX, _iTopLeftY );
            tContact._cBorderParcel.Init(_iTopLeftX, _iTopLeftY + iFrom, 1, nSize, pcCountry);
            break;           
        default:
            ASSERT(0);
        }
    }

    if (!(pSquare->TopLeftX() < 0 || pSquare->TopLeftY() < 0 ||
        pSquare->TopLeftX() >= pcCountry->GetSizeX() || pSquare->TopLeftY() >= pcCountry->GetSizeY()))
    /*{
        // Fictive square
        if (iBorder == 0 || iBorder == 2)
            tContact._cBrotherBorderParcel.InitOnConstantHeight(pSquare->MinCountryHeight(), nSize, 1, pcCountry);
        else
            tContact._cBrotherBorderParcel.InitOnConstantHeight(pSquare->MinCountryHeight(), 1, nSize, pcCountry);   
    }
    else*/
    {   
        // Normal square
        ::LogF("_iTopLeftX %d, _iTopLeftY %d", _iTopLeftX, _iTopLeftY );
        switch (iBorder)
        {
        case 0:
            tContact._cBrotherBorderParcel.Init(_iTopLeftX + iFrom, _iTopLeftY - 1, nSize, 1, pcCountry);
            break;
        case 1:            
            tContact._cBrotherBorderParcel.Init( _iTopLeftX + _nSizeX , _iTopLeftY + iFrom, 1, nSize, pcCountry);
            break;
        case 2:
            tContact._cBrotherBorderParcel.Init(_iTopLeftX + iFrom, _iTopLeftY + _nSizeY, nSize, 1, pcCountry);
            break;
        case 3:          
            tContact._cBrotherBorderParcel.Init(_iTopLeftX - 1, _iTopLeftY + iFrom, 1, nSize, pcCountry);
            break;           
        default:
            ASSERT(0);
        }

    }

    // Fictive borders are equal to normal borders
     if (_iTopLeftX < 0 || _iTopLeftY < 0 ||
        _iTopLeftX >= pcCountry->GetSizeX() || _iTopLeftY >= pcCountry->GetSizeY())
     {
         tContact._cBorderParcel = tContact._cBrotherBorderParcel;
     }

     if (pSquare->TopLeftX() < 0 || pSquare->TopLeftY() < 0 ||
        pSquare->TopLeftX() >= pcCountry->GetSizeX() || pSquare->TopLeftY() >= pcCountry->GetSizeY())
     {
         tContact._cBrotherBorderParcel = tContact._cBorderParcel;
     }


       

    
    _cContacts.Add(tContact);
    return;
}

int CPearySquare::FindContact(CPearySquare * pSquare, unsigned int iFrom)
{
    for(unsigned int i = 0; i < _cContacts.GetSize(); i++)
    {
        if (_cContacts[i]._ptSquare == pSquare)
        {
            if (_cContacts[i]._iBorder % 2 == 0)
            {
                if (_cContacts[i]._iFrom + _iTopLeftX == iFrom)                
                    return i;                
            }
            else
            {
                if (_cContacts[i]._iFrom + _iTopLeftY == iFrom)                
                    return i;
                
            }
        }
    }

    return -1;
}

TPEARYCONTACT& CPearySquare::FindContact2(CPearySquare * pSquare, unsigned int iFrom)
{
    for(unsigned int i = 0; i < _cContacts.GetSize(); i++)
    {
        if (_cContacts[i]._ptSquare == pSquare)
        {
            if (_cContacts[i]._iBorder % 2 == 0)
            {
                if (_cContacts[i]._iFrom + _iTopLeftX == iFrom)                
                    return _cContacts[i];                
            }
            else
            {
                if (_cContacts[i]._iFrom + _iTopLeftY == iFrom)                
                    return _cContacts[i];
                
            }
        }
    }

    ASSERT(0); // The function must not end here
    return _cContacts[0];
}

int CPearySquare::RetrieveContacts(MyPearyContactArray& cFoundContacts, unsigned int iBorder)
{
    int nFound = 0;
    for(unsigned int i = 0; i < _cContacts.GetSize(); i++)
    {
        if (_cContacts[i]._iBorder == iBorder)
        {
            nFound++;
            cFoundContacts.Add(_cContacts[i]);
        }
    }

    return nFound;
}

void CPearySquare::RefreshContactsCountryDep(CCountry * pcCountry)
{
    for(unsigned int i = 0; i < _cContacts.GetSize(); i++)
    {
        TPEARYCONTACT& tContact = _cContacts[i];

        // Set up country info
        if (_iTopLeftX < 0 || _iTopLeftY < 0 ||
            _iTopLeftX >= pcCountry->GetSizeX() || _iTopLeftY >= pcCountry->GetSizeY())
        {
            // Fictive square 
            if (tContact._iBorder == 0 || tContact._iBorder == 2)
                tContact._cBorderParcel.InitOnConstantHeight(_cParcel.GetMinCoutryHeight(), tContact._nSize, 1, pcCountry);
            else
                tContact._cBorderParcel.InitOnConstantHeight(_cParcel.GetMinCoutryHeight(), 1, tContact._nSize, pcCountry);             
        }
        else
        {    
            // Normal square
            switch (tContact._iBorder)
            {
            case 0:
                tContact._cBorderParcel.Init(_iTopLeftX + tContact._iFrom, _iTopLeftY, tContact._nSize, 1, pcCountry);
                break;
            case 1:            
                tContact._cBorderParcel.Init( _iTopLeftX + _nSizeX - 1, _iTopLeftY + tContact._iFrom, 1, tContact._nSize, pcCountry);
                break;
            case 2:
                tContact._cBorderParcel.Init(_iTopLeftX + tContact._iFrom, _iTopLeftY + _nSizeY - 1, tContact._nSize, 1, pcCountry);
                break;
            case 3:
                tContact._cBorderParcel.Init(_iTopLeftX, _iTopLeftY + tContact._iFrom, 1, tContact._nSize, pcCountry);
                break;           
            default:
                ASSERT(0);
            }
        }
        
        if (tContact._ptSquare->TopLeftX() < 0 || tContact._ptSquare->TopLeftY() < 0 ||
            tContact._ptSquare->TopLeftX() >= pcCountry->GetSizeX() || tContact._ptSquare->TopLeftY() >= pcCountry->GetSizeY())
        {
            // Fictive square
            if (tContact._iBorder == 0 || tContact._iBorder == 2)
                tContact._cBrotherBorderParcel.InitOnConstantHeight(tContact._ptSquare->MinCountryHeight(), tContact._nSize, 1, pcCountry);
            else
                tContact._cBrotherBorderParcel.InitOnConstantHeight(tContact._ptSquare->MinCountryHeight(), 1, tContact._nSize, pcCountry);   
        }
        else
        {   
            // Normal square
            switch (tContact._iBorder)
            {
            case 0:
                tContact._cBrotherBorderParcel.Init(_iTopLeftX + tContact._iFrom, _iTopLeftY - 1, tContact._nSize, 1, pcCountry);
                break;
            case 1:            
                tContact._cBrotherBorderParcel.Init( _iTopLeftX + _nSizeX, _iTopLeftY + tContact._iFrom, 1, tContact._nSize, pcCountry);
                break;
            case 2:
                tContact._cBrotherBorderParcel.Init(_iTopLeftX + tContact._iFrom, _iTopLeftY + _nSizeY, tContact._nSize, 1, pcCountry);
                break;
            case 3:
                tContact._cBrotherBorderParcel.Init(_iTopLeftX - 1, _iTopLeftY + tContact._iFrom, 1, tContact._nSize, pcCountry);
                break;           
            default:
                ASSERT(0);
            }
            
        }
        
    }
    
}

void CPearySquare::Log() const
{
  //LogF("PearySquare: mass %lf, water height %lf, _fWaterSurface %lf", _fMass, _fAverageWaterHeight,_fWaterSurface);
}
//-----------------------------------------------------------
//              P E A R Y S Q U A R E
//-----------------------------------------------------------

//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//                   P E A R Y
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

void CPeary::Init(CCountry * pcCountry)
{
    _pcCountry = pcCountry;
    _cWaterHeight = *_pcCountry;
    _cType.Init(pcCountry->GetSizeX(), pcCountry->GetSizeY());
    _cWaterMass.Init(pcCountry->GetSizeX(), pcCountry->GetSizeY());

    CPearyWaterQuadTreeIterator cIterator(_cQuadTree);

    cIterator[0].Init(0,0,
        pcCountry->GetSizeX() / 2, pcCountry->GetSizeY() / 2, pcCountry);
    cIterator[1].Init(pcCountry->GetSizeX() / 2,0,
        pcCountry->GetSizeX() / 2, pcCountry->GetSizeY() / 2, pcCountry);
    cIterator[2].Init(pcCountry->GetSizeX() / 2,pcCountry->GetSizeY() / 2,
        pcCountry->GetSizeX() / 2, pcCountry->GetSizeY() / 2, pcCountry);
    cIterator[3].Init(0,pcCountry->GetSizeY() / 2,
        pcCountry->GetSizeX() / 2, pcCountry->GetSizeY() / 2, pcCountry);  

    //Create Borders    
    cIterator[0].CreateContact(0, 0, cIterator[0].nSizeX(), _pFictiveBorders, 1,_pcCountry);
    cIterator[0].CreateContact(1, 0, cIterator[0].nSizeY(), &cIterator[1], 1,_pcCountry);
    cIterator[0].CreateContact(2, 0, cIterator[0].nSizeX(), &cIterator[3], 0,_pcCountry);
    cIterator[0].CreateContact(3, 0, cIterator[0].nSizeY(), _pFictiveBorders + 3, 1,_pcCountry);

    cIterator[1].CreateContact(0, 0, cIterator[1].nSizeX(), _pFictiveBorders, 1,_pcCountry);
    cIterator[1].CreateContact(1, 0, cIterator[1].nSizeY(), _pFictiveBorders + 1, 1,_pcCountry);
    cIterator[1].CreateContact(2, 0, cIterator[1].nSizeX(), &cIterator[2], 0,_pcCountry);
    cIterator[1].CreateContact(3, 0, cIterator[1].nSizeY(), &cIterator[0], 0,_pcCountry);

    cIterator[2].CreateContact(0, 0, cIterator[2].nSizeX(), &cIterator[1], 1,_pcCountry);
    cIterator[2].CreateContact(1, 0, cIterator[2].nSizeY(), _pFictiveBorders + 1, 1,_pcCountry);
    cIterator[2].CreateContact(2, 0, cIterator[2].nSizeX(), _pFictiveBorders + 2, 1,_pcCountry);
    cIterator[2].CreateContact(3, 0, cIterator[2].nSizeY(), &cIterator[3], 0,_pcCountry);

    cIterator[3].CreateContact(0, 0, cIterator[3].nSizeX(), &cIterator[0], 1,_pcCountry);
    cIterator[3].CreateContact(1, 0, cIterator[3].nSizeY(), &cIterator[2], 1,_pcCountry);
    cIterator[3].CreateContact(2, 0, cIterator[3].nSizeX(), _pFictiveBorders + 2, 1,_pcCountry);
    cIterator[3].CreateContact(3, 0, cIterator[3].nSizeY(), _pFictiveBorders + 3, 1,_pcCountry);

    //Create Fictive Borders
    _pFictiveBorders[0].Init(0, - (int) pcCountry->GetSizeY(), 
        pcCountry->GetSizeX(), pcCountry->GetSizeY(), pcCountry);
    _pFictiveBorders[1].Init(pcCountry->GetSizeX(), 0, 
        pcCountry->GetSizeX(), pcCountry->GetSizeY(), pcCountry);
    _pFictiveBorders[2].Init(0, pcCountry->GetSizeY(), 
        pcCountry->GetSizeX(), pcCountry->GetSizeY(), pcCountry);
    _pFictiveBorders[3].Init(- (int) pcCountry->GetSizeX(), 0, 
        pcCountry->GetSizeX(), pcCountry->GetSizeY(), pcCountry);
    
    _pFictiveBorders[0].CreateContact(2, 0, cIterator[0].nSizeX(), &(cIterator[0]), 0,_pcCountry);
    _pFictiveBorders[0].CreateContact(2, cIterator[0].nSizeX() , cIterator[1].nSizeX(), &cIterator[1], 0,_pcCountry);

    _pFictiveBorders[1].CreateContact(3, 0, cIterator[1].nSizeY(), &cIterator[1], 0,_pcCountry);
    _pFictiveBorders[1].CreateContact(3, cIterator[1].nSizeY(), cIterator[2].nSizeY(), &cIterator[2], 0,_pcCountry);

    _pFictiveBorders[2].CreateContact(0, 0, cIterator[3].nSizeX(), &cIterator[3], 0,_pcCountry);
    _pFictiveBorders[2].CreateContact(0, cIterator[2].nSizeX(), cIterator[2].nSizeX(), &cIterator[2], 0,_pcCountry);

    _pFictiveBorders[3].CreateContact(1, 0, cIterator[0].nSizeX(), &cIterator[0], 0,_pcCountry);
    _pFictiveBorders[3].CreateContact(1, cIterator[0].nSizeX(), cIterator[3].nSizeX(), &cIterator[3], 0,_pcCountry);

    cIterator.ToTop();
    UpdateContactsLength(cIterator);
    
    // Update _cType a _cWaterMass
    /*cIterator.ToTop();
    _cWaterMass.ZeroItems();    
    CalculateWaterHeightFromQuadTree(cIterator);

    cIterator.ToTop();
    _cType.ZeroItems();
    CalculateNet(cIterator);
    */

    _nSimulations = 0;
    
    return;
}

void CPeary::CalculateNet(CPearyWaterQuadTreeIterator& cIterator)
{

    return; 
    
    for(int i = 0; i < 4; i++)
    {
        if (cIterator.IsChild(i))
        {
            cIterator.ToChild(i);
            CalculateNet(cIterator);
            cIterator.ToParent();            
        }
        else
        {
            for(unsigned int j = 0; j < cIterator[i].nSizeX(); j++ )
                _cType.SetItem(cIterator[i].TopLeftX() + j,cIterator[i].TopLeftY(), -1);

            for(unsigned int j = 0; j < cIterator[i].nSizeY(); j++ )
                _cType.SetItem(cIterator[i].TopLeftX(), cIterator[i].TopLeftY() + j, -1);

            for(unsigned int j = 0; j < cIterator[i].nSizeX(); j++ )
                _cType.SetItem(cIterator[i].TopLeftX() + j,cIterator[i].TopLeftY() + cIterator[i].nSizeY() - 1, -1);

            for(unsigned int j = 0; j < cIterator[i].nSizeY(); j++ )
                _cType.SetItem(cIterator[i].TopLeftX() + cIterator[i].nSizeX() - 1,cIterator[i].TopLeftY() + j, -1);

            // Visualization of contacts
           
           /* MyContactArray& cContacts = cIterator[i].Contacts();
            for(unsigned int k = 0; k < cContacts.GetSize(); k++)
            {
                const TPEARYCONTACT& tContact = cContacts[k];
                
               if (!tContact._bSimulate)
                    continue;
                
                switch(tContact._iBorder)
                {
                case 0:
                    {
                        unsigned int iFrom =  cIterator[i].TopLeftY() + cIterator[i].nSizeY() / 4;
                        
                        int iTo = iFrom - cIterator[i].nSizeY() / 4 - 
                            tContact._ptSquare->nSizeY() / 4;   
                        
                        iTo = max(0, iTo);                                                
                        
                        for(int j = iFrom; j > iTo; j--)
                            _cType.SetItem(cIterator[i].TopLeftX() + tContact._iFrom + tContact._nSize / 2, j, -1);
                        
                        break;
                        
                    }
                case 1:
                    {
                        unsigned int iFrom =  cIterator[i].TopLeftX() + 3 * cIterator[i].nSizeX() / 4;
                        
                        int iTo = iFrom + cIterator[i].nSizeX() / 4 +
                            tContact._ptSquare->nSizeX() / 4;   
                        
                        iTo = min(_pcCountry->GetSizeX(), iTo);                                                
                        
                        for(int j = iFrom; j < iTo; j++)
                            _cType.SetItem( j, cIterator[i].TopLeftY() + tContact._iFrom + tContact._nSize / 2, -1);
                        
                        break;
                        
                    }
                case 2:
                    {
                        unsigned int iFrom =  cIterator[i].TopLeftY() + 3 * cIterator[i].nSizeY() / 4;
                        
                        int iTo = iFrom + cIterator[i].nSizeY() / 4 +
                            tContact._ptSquare->nSizeY() / 4;   
                        
                        iTo = min(_pcCountry->GetSizeY(), iTo);                                                
                        
                        for(int j = iFrom; j < iTo; j++)
                            _cType.SetItem(cIterator[i].TopLeftX() + tContact._iFrom + tContact._nSize / 2, j, -1);
                        
                        break;
                    } 
                case 3:
                    {
                        unsigned int iFrom = cIterator[i].TopLeftX() + cIterator[i].nSizeX() / 4;
                        
                        int iTo = iFrom - cIterator[i].nSizeX() / 4 -
                            tContact._ptSquare->nSizeX() / 4;   
                        
                        iTo = max(0, iTo);                                                
                        
                        for(int j = iFrom; j > iTo; j--)
                            _cType.SetItem( j, cIterator[i].TopLeftY() + tContact._iFrom + tContact._nSize / 2, -1);
                        
                        break;
                    }  
                default:
                    ASSERT(0);                    
                }
                
            }*/
            
            


        }
    }
}


typedef struct
{
    float _fX;
    float _fY;
    float _fHeight;
} THEIGHTITEM;

void CPeary::CalculateWaterHeightFromQuadTree(CPearyWaterQuadTreeIterator& cIterator)
{
    for(int i = 0; i < 4; i++)
    {
        if (cIterator.IsChild(i))
        {
            cIterator.ToChild(i);
            CalculateWaterHeightFromQuadTree(cIterator);
            cIterator.ToParent();
        }
        else
        {

            const CPearySquare& tSquare = cIterator[i];
            for(unsigned int ix = tSquare.TopLeftX(); ix < tSquare.TopLeftX() + tSquare.nSizeX(); ix++)
            {
                for(unsigned int iy = tSquare.TopLeftY(); iy < tSquare.TopLeftY() + tSquare.nSizeY(); iy++)
                {
                    if (tSquare.AverageWaterHeight() > _pcCountry->GetItem(ix,iy))
                    {                    
                        _cWaterHeight.SetItem(ix,iy,tSquare.AverageWaterHeight());
                    //if (tSquare.AverageWaterHeight() > _pcCountry->GetItem(ix,iy))
                        _cWaterMass.SetItem( ix, iy, tSquare.AverageWaterHeight() - _pcCountry->GetItem(ix,iy));
                    }
                    else
                    {                    
                        _cWaterHeight.SetItem(ix,iy,_pcCountry->GetItem(ix,iy));
                        _cWaterMass.SetItem( ix, iy, 0.0f);
                    }

                        
                }
            }

        }
    }    
}

void CPeary::Simulate()
{
    _nSimulations ++; 
   //if (_nSimulations < 5000)
   {
    
          
        _pFictiveBorders[0].AverageWaterHeight() = 470.0f;
        _pFictiveBorders[0].Mass() = _pFictiveBorders[0].fSizeX() * _pFictiveBorders[0].fSizeX() * _pFictiveBorders[0].AverageWaterHeight(); 
        _pFictiveBorders[0].WaterSurface() = _pFictiveBorders[0].fSizeX() * _pFictiveBorders[0].fSizeX();
   }
  /* else
   {   
       _pFictiveBorders[0].AverageWaterHeight() = 300.0f;
       _pFictiveBorders[0].Mass() = _pFictiveBorders[0].fSizeX() * _pFictiveBorders[0].fSizeX() * _pFictiveBorders[0].AverageWaterHeight();
   }*/
        
    //_pFictiveBorders[3].AverageWaterHeight() = 250.0f;

    _pFictiveBorders[2].AverageWaterHeight() = 400.0f;
    _pFictiveBorders[2].Mass() = _pFictiveBorders[2].fSizeX() * _pFictiveBorders[2].fSizeX() * _pFictiveBorders[2].AverageWaterHeight(); 
    _pFictiveBorders[2].WaterSurface() = _pFictiveBorders[2].fSizeX() * _pFictiveBorders[2].fSizeX();

    _pFictiveBorders[3].Mass() = 0.0f;
    _pFictiveBorders[3].AverageWaterHeight() = 400.0f;
    _pFictiveBorders[3].WaterSurface() = _pFictiveBorders[3].fSizeX() * _pFictiveBorders[3].fSizeX();

    _pFictiveBorders[1].Mass() = 0.0f;
    _pFictiveBorders[1].AverageWaterHeight() = 400.0f;
    _pFictiveBorders[1].WaterSurface() = _pFictiveBorders[1].fSizeX() * _pFictiveBorders[1].fSizeX();
 

    //_pFictiveBorders[0].Log();
    //_pFictiveBorders[1].Log();
    //_pFictiveBorders[2].Log();
    //_pFictiveBorders[3].Log();



    CPearyWaterQuadTreeIterator cIterator(_cQuadTree);
    float fSimTime = .04f;
    float fSpentTime = 0.0;
    do 
    {
        float fTime = min(fSimTime - fSpentTime, fSimTime/5);

        // Update velocity field.
        /*float fDiffMass = _pFictiveBorders[0].DiffMass() +
            _pFictiveBorders[1].DiffMass() +
            _pFictiveBorders[2].DiffMass() +
            _pFictiveBorders[3].DiffMass();

        cIterator.ToTop();
        CalculateDiffMass(fDiffMass, cIterator);

        
*/
        _pFictiveBorders[0].DiffMass() = 0.0f;
        _pFictiveBorders[1].DiffMass() = 0.0f;
        _pFictiveBorders[2].DiffMass() = 0.0f;
        _pFictiveBorders[3].DiffMass() = 0.0f;

        cIterator.ToTop();
        UpdateVelocities(fTime,cIterator);

        cIterator.ToTop();
        ZeroDiffMass(cIterator);
               

        cIterator.ToTop();
        PrepareUpdateWaterHeight(cIterator);

        

        cIterator.ToTop();
        FindUpdateTime(fTime, cIterator);

        cIterator.ToTop();
        UpdateWaterHeight(fTime,cIterator);

        fSpentTime += fTime;

        

    } while (fSimTime - fSpentTime > fSimTime/100);


    /*cIterator.ToTop();
    _cWaterMass.ZeroItems();    
    CalculateWaterHeightFromQuadTree(cIterator);
    */
}

const float G = 10.f;
const float H = 0.01f;
const float K = 0.1f;

//const float H = 0.1f;
//const float K = 0.01f;

//const float H = 0.0f;
//const float K = 0.00f;

void CPeary::UpdateVelocities(float fTime, CPearyWaterQuadTreeIterator& cIterator)
{
    for(int i = 3; i >= 0; i--)
    {
        if (cIterator.IsChild(i))
        {
            cIterator.ToChild(i);
            UpdateVelocities(fTime, cIterator);
            cIterator.ToParent();
        }
        else
        {
            unsigned int nContacts = cIterator[i].Contacts().GetSize();
            for(unsigned int j = 0; j < nContacts; j++)
            {
                const TPEARYCONTACT& tContact = cIterator[i].Contacts()[j];
                if (tContact._bSimulate == 1)
                {
                    
                    TPEARYCONTACT& tContactOut = cIterator[i].Contacts()[j];
                    CPearySquare& cActualSquare = cIterator[i];
                    CPearySquare& cContactSquare = *tContact._ptSquare;

                    // if both squares are without water 
                    if (cActualSquare.Mass() == 0.0f && cContactSquare.Mass() == 0.0f) 
                    {
                        tContactOut._fNewSpeed = 0.0f;                        
                    }
                    else
                    {
                        
                      /*  float fLength;
                        
                        switch (tContact._iBorder)
                        {
                        case 0:
                            fLength = (cActualSquare.fSizeY() + cContactSquare.fSizeY())/2;                        
                            break;
                        case 1:
                            fLength = -(cActualSquare.fSizeX() + cContactSquare.fSizeX())/2;
                            break;
                        case 2:
                            fLength = -(cActualSquare.fSizeY() + cContactSquare.fSizeY())/2;
                            break;
                        case 3:
                            fLength = (cActualSquare.fSizeX() + cContactSquare.fSizeX())/2;
                            break;
                        default:
                            ASSERT(0);
                        }                                       

                        float fInvLength = 1.0f / fLength;*/
                        
                        //cActualSquare.Log();
                        //cContactSquare.Log();

                        float fDiff =  K * ((cActualSquare.DiffMass() / cActualSquare.WaterSurface() - cContactSquare.DiffMass() / cContactSquare.WaterSurface()) );
                        //float fDiff =  K * ((cActualSquare.DiffMass()  - cContactSquare.DiffMass() ) / fLength);
                        //FILE *f = fopen("slon.txt","a");
                        //fprintf(f,"%f %f %f \n",fDiff, cActualSquare.WaterSurface(),cContactSquare.WaterSurface() );
                        //fclose(f);
                        
                        float fHeightDiff;
                        
#ifdef BETTER_BORDERS
                     if (max(tContact._cBorderParcel.GetMinCoutryHeight(), tContact._cBrotherBorderParcel.GetMinCoutryHeight()) < min(cActualSquare.AverageWaterHeight(), cContactSquare.AverageWaterHeight()))
                            fHeightDiff = (cActualSquare.AverageWaterHeight() - cContactSquare.AverageWaterHeight());
                        else
                        {
                            if (cActualSquare.AverageWaterHeight() > cContactSquare.AverageWaterHeight())
                            {
                                if (tContact._fSpeed * tContact._fLength < 0.0f) 
                                    fHeightDiff = cActualSquare.AverageWaterHeight() - tContact._cBorderParcel.GetMinCoutryHeight(); // water flows from higher to lower
                                else
                                    fHeightDiff = max(cActualSquare.AverageWaterHeight(),tContact._cBorderParcel.GetMinCoutryHeight()) - cContactSquare.AverageWaterHeight();
                            }
                            else
                            {
                                if (tContact._fSpeed * tContact._fLength > 0.0f) 
                                    fHeightDiff = tContact._cBrotherBorderParcel.GetMinCoutryHeight() - cContactSquare.AverageWaterHeight(); // water flows from higher to lower
                                else
                                    fHeightDiff = cActualSquare.AverageWaterHeight() - max(cContactSquare.AverageWaterHeight(),tContact._cBrotherBorderParcel.GetMinCoutryHeight());
                            }
                        }
#else
                        fHeightDiff = (cActualSquare.AverageWaterHeight() - cContactSquare.AverageWaterHeight());
                        float fHeightDiffOld = fHeightDiff;
                        if (fHeightDiff > 0 && cActualSquare.MinCountryHeight() > cContactSquare.AverageWaterHeight() && tContact._fSpeed * tContact._fLength < 0 )
                        {
                            fHeightDiff = (cActualSquare.AverageWaterHeight() - cActualSquare.MinCountryHeight());
                        }
                        
                        if (fHeightDiff < 0 && cActualSquare.AverageWaterHeight() < cContactSquare.MinCountryHeight() && tContact._fSpeed * tContact._fLength > 0 )
                        {
                            fHeightDiff = (cContactSquare.MinCountryHeight() - cContactSquare.AverageWaterHeight());
                        }
                        
                        ASSERT(fabsf(fHeightDiff) <= fabsf(fHeightDiffOld));
                        
                        //fHeightDiff = fHeightDiffOld;
#endif                        
                        
                         
                        tContactOut._fNewSpeed = tContact._fSpeed - fTime * ((G * fHeightDiff + fDiff) * tContact._fInvLength + 
                            H * tContact._fSpeed * fabsf(tContact._fSpeed) /* fabsf(tContact._fSpeed)*/);
                        
                         tContactOut._fSpeed = tContactOut._fNewSpeed;
                         //LogF("fHeightDiff %lf, fDiff %lf, tContact._fSpeed %lf", fHeightDiff, fDiff, tContact._fSpeed);
                         //LogF("tContactOut._fSpeed %lf, tContactOut._fNewSpeed %lf",tContactOut._fSpeed,  tContactOut._fNewSpeed);

                        
                    }
                }                   
            }            
        }
    }    
}

void CPeary::ZeroDiffMass(CPearyWaterQuadTreeIterator& cIterator)
{
    for(int i = 0; i < 4; i++)
    {
        if (cIterator.IsChild(i))
        {
            cIterator.ToChild(i);
            ZeroDiffMass(cIterator);
            cIterator.ToParent();
        }
        else
        {
            cIterator[i].DiffMass() = 0.0f;
        }
    }
}

void CPeary::CalculateDiffMass(float& fDiffMass, CPearyWaterQuadTreeIterator& cIterator)
{
    for(int i = 0; i < 4; i++)
    {
        if (cIterator.IsChild(i))
        {
            cIterator.ToChild(i);
            CalculateDiffMass( fDiffMass, cIterator);
            cIterator.ToParent();
        }
        else
        {
            fDiffMass += cIterator[i].DiffMass();
        }
    }
}

void CPeary::PrepareUpdateWaterHeight(CPearyWaterQuadTreeIterator& cIterator)
{
    for(int i = 0; i < 4; i++)
    {
        if (cIterator.IsChild(i))
        {
            cIterator.ToChild(i);
            PrepareUpdateWaterHeight(cIterator);
            cIterator.ToParent();
        }
        else
        {
            CPearySquare& cActualSquare = cIterator[i];
            unsigned int nContacts = cActualSquare.Contacts().GetSize();
            for(unsigned int j = 0; j < nContacts; j++)
            {
                TPEARYCONTACT& tContact = cActualSquare.Contacts()[j];

                CPearySquare& cContactSquare = *tContact._ptSquare;

                if (tContact._bSimulate && tContact._fSpeed != 0.0f)
                {
#ifdef BETTER_BORDERS_2
                    float fLenght = fabsf(tContact._fLength);                  

                    float fRealHeightAtBorder = (cActualSquare.AverageWaterHeight() * (fLenght - cActualSquare.fSizeX() / 2.0f) + 
                        cContactSquare.AverageWaterHeight() * (fLenght - cContactSquare.fSizeX() / 2.0f)) * fabsf(tContact._fInvLength);
#endif
                    float fDiff = 0.0f;
                    if (tContact._iBorder == 0 || tContact._iBorder == 3)
                    {   
                        if (tContact._fSpeed > 0.0f)
                        {
                            if (cContactSquare.Mass() == 0.0f)
                            {
                                tContact._fSpeed = 0.0f;
                            }
                            else
                            {
#ifdef BETTER_BORDERS
                                //float fDiff = tContact._fSpeed * (cContactSquare.Mass() / cContactSquare.fSizeX()/cContactSquare.fSizeY()) * tContact._fSize;
                                
                                 fDiff = tContact._fSpeed * 
                                    ((tContact._iBorder == 0) ? tContact._cBrotherBorderParcel.CalculateFaceInXFromHeight(cContactSquare.AverageWaterHeight()) : 
                                    tContact._cBrotherBorderParcel.CalculateFaceInYFromHeight(cContactSquare.AverageWaterHeight()));
#else
#ifdef BETTER_BORDERS_2
                                 fDiff = tContact._fSpeed * (fRealHeightAtBorder - cContactSquare.MinCountryHeight()) * tContact._fSize;
#else
                                 fDiff = tContact._fSpeed * (cContactSquare.AverageWaterHeight() - cContactSquare.MinCountryHeight()) * tContact._fSize;
#endif
#endif
                                cActualSquare.DiffMass() += fDiff;
                                cContactSquare.DiffMass() -= fDiff;

                            }
                        }
                        else
                        {
                            if (cActualSquare.Mass() == 0.0f)
                            {
                                tContact._fSpeed = 0.0f;
                            }
                            else
                            {
#ifdef BETTER_BORDERS
                                //float fDiff = tContact._fSpeed * (cActualSquare.Mass() / cActualSquare.fSizeX()/cActualSquare.fSizeY()) * tContact._fSize;

                                 fDiff = tContact._fSpeed * 
                                    ((tContact._iBorder == 0) ? tContact._cBorderParcel.CalculateFaceInXFromHeight(cActualSquare.AverageWaterHeight()) : 
                                    tContact._cBorderParcel.CalculateFaceInYFromHeight(cActualSquare.AverageWaterHeight()));
#else
#ifdef BETTER_BORDERS_2
                                 fDiff = tContact._fSpeed * (fRealHeightAtBorder - cActualSquare.MinCountryHeight()) * tContact._fSize;
#else
                                 fDiff = tContact._fSpeed * (cActualSquare.AverageWaterHeight() - cActualSquare.MinCountryHeight()) * tContact._fSize;
#endif
#endif                                   
                                cActualSquare.DiffMass() += fDiff;
                                cContactSquare.DiffMass() -= fDiff;

                            }
                        }
                    }
                    else
                    {   
                        if (tContact._fSpeed > 0.0f)
                        {
                            if (cActualSquare.Mass() == 0.0f)
                            {
                                tContact._fSpeed = 0.0f;
                            }
                            else
                            {
#ifdef BETTER_BORDERS
                                //float fDiff = tContact._fSpeed * (cActualSquare.Mass() / cActualSquare.fSizeX()/cActualSquare.fSizeY()) * tContact._fSize;

                                 fDiff = tContact._fSpeed * 
                                    ((tContact._iBorder == 2) ? tContact._cBorderParcel.CalculateFaceInXFromHeight(cActualSquare.AverageWaterHeight()) : 
                                    tContact._cBorderParcel.CalculateFaceInYFromHeight(cActualSquare.AverageWaterHeight()));
#else
#ifdef BETTER_BORDERS_2
                                 fDiff = tContact._fSpeed * (fRealHeightAtBorder - cActualSquare.MinCountryHeight()) * tContact._fSize;
#else
                                 fDiff = tContact._fSpeed * (cActualSquare.AverageWaterHeight() - cActualSquare.MinCountryHeight()) * tContact._fSize;
#endif
#endif                                
                                cActualSquare.DiffMass() -= fDiff;
                                cContactSquare.DiffMass() += fDiff;

                            }
                        }
                        else
                        {
                            if (cContactSquare.Mass() == 0.0f)
                            {
                                tContact._fSpeed = 0.0f;
                            }
                            else
                            {
#ifdef BETTER_BORDERS
                                //float fDiff = tContact._fSpeed * (cContactSquare.Mass() / cContactSquare.fSizeX()/cContactSquare.fSizeY()) * tContact._fSize;

                                 fDiff = tContact._fSpeed * 
                                    ((tContact._iBorder == 2) ? tContact._cBrotherBorderParcel.CalculateFaceInXFromHeight(cContactSquare.AverageWaterHeight()) : 
                                    tContact._cBrotherBorderParcel.CalculateFaceInYFromHeight(cContactSquare.AverageWaterHeight()));
#else
#ifdef BETTER_BORDERS_2
                                 fDiff = tContact._fSpeed * (fRealHeightAtBorder - cContactSquare.MinCountryHeight()) * tContact._fSize;
#else
                                 fDiff = tContact._fSpeed * (cContactSquare.AverageWaterHeight() - cContactSquare.MinCountryHeight()) * tContact._fSize;
#endif
#endif

                                cActualSquare.DiffMass() -= fDiff;
                                cContactSquare.DiffMass() += fDiff;
                            }
                        }
                    }
                    
                    if (fDiff == 0)
                    {
                        tContact._fSpeed = 0.0f;
                    }
                }
            }            
        }
    }
}

void CPeary::FindUpdateTime(float& fTime, CPearyWaterQuadTreeIterator& cIterator)
{
    for(int i = 0; i < 4; i++)
    {
        if (cIterator.IsChild(i))
        {
            cIterator.ToChild(i);
            FindUpdateTime(fTime,   cIterator);
            cIterator.ToParent();
        }
        else
        {
            if (cIterator[i].DiffMass() < 0.0f && -cIterator[i].DiffMass() * fTime > cIterator[i].Mass())                
            {
                fTime = - cIterator[i].Mass() / cIterator[i].DiffMass();
            }
            
        }
    }
}


void CPeary::UpdateWaterHeight(float fTime, CPearyWaterQuadTreeIterator& cIterator)
{
    for(int i = 0; i < 4; i++)
    {
        if (cIterator.IsChild(i))
        {
            cIterator.ToChild(i);
            UpdateWaterHeight( fTime, cIterator);
            cIterator.ToParent();
        }
        else
        {
            if (cIterator[i].DiffMass() != 0)
            {
                
                if (cIterator[i].DiffMass() < 0.0f && -cIterator[i].DiffMass() * fTime > cIterator[i].Mass())                
                {
                    cIterator[i].Mass() = 0.0f;  
                    cIterator[i].DiffMass() = - cIterator[i].Mass() / fTime;
                }
                else
                {
                    cIterator[i].Mass() += cIterator[i].DiffMass() * fTime;
                    if (IsNumericalZero(cIterator[i].Mass()))
                    {                    
                        cIterator[i].Mass() = 0.0f;
                    }
                    
                    ASSERT(cIterator[i].Mass() >= 0.0f);
                }
                
                
                //cIterator[i].DiffMass() = 0.0f;
                cIterator[i].CalculateWaterHeightAndSurfaceFromMass();
                if (IsNumericalZero(cIterator[i].AverageWaterHeight() - cIterator[i].MinCountryHeight()))
                {
                    cIterator[i].Mass() = 0.0f;
                    cIterator[i].CalculateWaterHeightAndSurfaceFromMass();
                }
            }
        }
    }

}

void CPeary::Teselate(MyArray<unsigned int>& cZone)
{
    CPearyWaterQuadTreeIterator cIterator(_cQuadTree);

    Teselate( cIterator, cZone);

    cIterator.ToTop();
    UpdateContactsLength(cIterator);

    // Update _cType a _cWaterMass
    /*cIterator.ToTop();
    _cWaterMass.ZeroItems();    
    CalculateWaterHeightFromQuadTree(cIterator);

    cIterator.ToTop();
    _cType.ZeroItems();
    CalculateNet(cIterator);
    */
}

void AddMass(CPearyWaterQuadTreeIterator& cIterator, float fMass)
{
    // TODO: new algorythm, this one is very stupid
    for(int i = 0; i < 100; i++)
    {
        unsigned int iMinHeightSquare = 0;
        float fMinHeight = cIterator[0].AverageWaterHeight();
        for(int j = 1; j < 4; j++)
        {
            if (fMinHeight > cIterator[j].AverageWaterHeight())
            {
                iMinHeightSquare = j;
                fMinHeight = cIterator[j].AverageWaterHeight();
            }
        }

        cIterator[iMinHeightSquare].Mass() += fMass / 100;
        cIterator[iMinHeightSquare].CalculateWaterHeightAndSurfaceFromMass();
    }

    for(int j = 0; j < 4; j++)
        cIterator[j].CalculateWaterHeightAndSurfaceFromMass();

}

#define USE_MASS_CRITERIUM

void CPeary::Teselate(CPearyWaterQuadTreeIterator& cIterator, MyArray<unsigned int>& cZone)
{    
    for(unsigned int i = 0 ; i < cZone.GetSize(); i++)
    {
        ASSERT(cZone[i] < 4);
        if (cIterator.IsChild(cZone[i]))
            cIterator.ToChild(cZone[i]);
        else
        {
            Teselate(cIterator, cZone[i]);            
        }
    }
}

void CPeary::EquidistantTeselation(CPearyWaterQuadTreeIterator& cIterator, unsigned int iActualLevel, unsigned int iStopLevel)
{
    
    if (iActualLevel == iStopLevel)
        return;
       
    for(unsigned int i = 0; i < 4; i++)
    {        
        if (cIterator.IsChild(i))
        {        
            cIterator.ToChild(i);
            EquidistantTeselation(cIterator, iActualLevel + 1, iStopLevel);
            cIterator.ToParent();
        }            
        else
        {
            Teselate(cIterator, i);
            EquidistantTeselation(cIterator, iActualLevel + 1, iStopLevel);
            cIterator.ToParent();
        }
    }    
}

void CPeary::EquidistantTeselation(unsigned int iLevel)
{
    CPearyWaterQuadTreeIterator cIterator(_cQuadTree);

    EquidistantTeselation(cIterator, 0, iLevel);

    //cIterator.ToChild(0);
    //EquidistantTeselation(cIterator, 1, iLevel);
    //cIterator.ToTop();

    //cIterator.ToChild(1);
    //EquidistantTeselation(cIterator, 1, iLevel);
    //cIterator.ToTop();

    //cIterator.ToChild(2);
    //EquidistantTeselation(cIterator, 2, iLevel);
    //cIterator.ToTop();

    //cIterator.ToChild(3);
    //EquidistantTeselation(cIterator, 1, iLevel);
    //cIterator.ToTop();

    cIterator.ToTop();
    UpdateContactsLength(cIterator);
    
    cIterator.ToTop();
    _cType.ZeroItems();
    CalculateNet(cIterator);
    
}

void CPeary::EquidistantTeselation2(unsigned int iLevel)
{
    CPearyWaterQuadTreeIterator cIterator(_cQuadTree);

    EquidistantTeselation(cIterator, 6, iLevel);

    cIterator.ToChild(1);
    EquidistantTeselation(cIterator, 1, iLevel);
    cIterator.ToTop();

   // cIterator.ToChild(2);
   // EquidistantTeselation(cIterator, 1, iLevel);
   // cIterator.ToTop();

    //cIterator.ToChild(2);
    //EquidistantTeselation(cIterator, 2, iLevel);
    //cIterator.ToTop();

    //cIterator.ToChild(3);
    //EquidistantTeselation(cIterator, 1, iLevel);
    //cIterator.ToTop();

    cIterator.ToTop();
    UpdateContactsLength(cIterator);
    
    cIterator.ToTop();
    _cType.ZeroItems();
    CalculateNet(cIterator);
    
}

void CPeary::Teselate(CPearyWaterQuadTreeIterator& cIterator, unsigned int iZone)
{
    ASSERT(iZone < 4);

    if (cIterator.IsChild(iZone))
        return;

    cIterator.CreateChild(iZone);
    CPearySquare& tParentSquare = cIterator[iZone];
    cIterator.ToChild(iZone);
    
    // Create childs
    
    unsigned int nSizeX = tParentSquare.nSizeX() / 2;
    unsigned int nSizeY = tParentSquare.nSizeY() / 2;

    cIterator[0].Init( tParentSquare.TopLeftX(), tParentSquare.TopLeftY(), 
        nSizeX, nSizeY, _pcCountry);
    cIterator[1].Init( tParentSquare.TopLeftX() + nSizeX, tParentSquare.TopLeftY(), 
        nSizeX, nSizeY, _pcCountry);
    cIterator[2].Init( tParentSquare.TopLeftX() + nSizeX, tParentSquare.TopLeftY() + nSizeY, 
        nSizeX, nSizeY, _pcCountry);
    cIterator[3].Init( tParentSquare.TopLeftX(), tParentSquare.TopLeftY() + nSizeY, 
        nSizeX, nSizeY, _pcCountry);
    
    
    for(int j = 0; j < 4; j++)
    {              
#ifndef USE_MASS_CRITERIUM
        cIterator[j].AverageWaterHeight() = tParentSquare.AverageWaterHeight();
        cIterator[j].CalculateMassFromWaterHeight();
#endif
    }
#ifdef USE_MASS_CRITERIUM
    AddMass(cIterator, tParentSquare.Mass());
#endif
    
    /// Set up new  contacts, They have average speeds.
    MyPearyContactArray cContatcs;
    tParentSquare.RetrieveContacts(cContatcs, 1); 

    cIterator[0].CreateContact(1, 0, cIterator[0].nSizeY(), &cIterator[1], 1,_pcCountry, cContatcs[0]._fSpeed);
    cIterator[3].CreateContact(1, 0, cIterator[3].nSizeY(), &cIterator[2], 1,_pcCountry, cContatcs[0]._fSpeed);

    cContatcs.Release();
    tParentSquare.RetrieveContacts(cContatcs, 0); 

    cIterator[2].CreateContact(0, 0, cIterator[2].nSizeX(), &cIterator[1], 1,_pcCountry, cContatcs[0]._fSpeed);
    cIterator[3].CreateContact(0, 0, cIterator[3].nSizeX(), &cIterator[0], 1,_pcCountry, cContatcs[0]._fSpeed);
    
    cIterator[0].CreateContact(2, 0, cIterator[0].nSizeX(), &cIterator[3], 0,_pcCountry);    
    cIterator[1].CreateContact(2, 0, cIterator[1].nSizeX(), &cIterator[2], 0,_pcCountry);

    cIterator[1].CreateContact(3, 0, cIterator[1].nSizeY(), &cIterator[0], 0,_pcCountry);        
    cIterator[2].CreateContact(3, 0, cIterator[2].nSizeY(), &cIterator[3], 0,_pcCountry);
    
    
   
    
    /// Resolve old contacts 
    for(unsigned int j = 0; j < tParentSquare.Contacts().GetSize(); j++)
    {
        ResolveOldContact(cIterator, tParentSquare.Contacts()[j], &tParentSquare);          
    }
}



void CPeary::ResolveOldContact(CPearyWaterQuadTreeIterator& cIterator, 
                                TPEARYCONTACT& tContact, 
                                CPearySquare * pcParentSquare)
{       
    int iBrotherContact;
    CPearySquare& cBrotherSquare = *tContact._ptSquare;
    
    
    switch (tContact._iBorder)
    {
    case 0: 
        {
            
            iBrotherContact = cBrotherSquare.FindContact(pcParentSquare, tContact._iFrom + pcParentSquare->TopLeftX());
            ASSERT(iBrotherContact >= 0);
            TPEARYCONTACT& tBrotherContact = cBrotherSquare.Contacts()[iBrotherContact];
            
            if (tContact._iFrom < cIterator[0].nSizeX())
            {
                // Contact is not divided
                if (tContact._iFrom + tContact._nSize <= cIterator[0].nSizeX())                            
                {            
                    cIterator[0].CreateContact(0, tContact._iFrom, tContact._nSize,
                        tContact._ptSquare, tContact._bSimulate, _pcCountry, tContact._fSpeed);
                    
                    tBrotherContact._ptSquare = &cIterator[0];
                }
                else
                {     
                    // Contact is devided into two.                
                    cIterator[0].CreateContact(0, tContact._iFrom, (cIterator[0].nSizeX() - tContact._iFrom),
                        tContact._ptSquare, tContact._bSimulate, _pcCountry, tContact._fSpeed);
                    
                    cBrotherSquare.CreateContact(2, tBrotherContact._iFrom, (cIterator[0].nSizeX() - tContact._iFrom),
                        &cIterator[0], tBrotherContact._bSimulate, _pcCountry, tBrotherContact._fSpeed);
                    
                    ASSERT(tContact._iFrom + tContact._nSize <= cIterator[0].nSizeX() + cIterator[1].nSizeX());

                    TPEARYCONTACT& tBrotherContact2 = cBrotherSquare.Contacts()[iBrotherContact];
                    
                    cIterator[1].CreateContact(0, 0, ( tContact._iFrom + tContact._nSize - cIterator[0].nSizeX() ),
                        tContact._ptSquare, tContact._bSimulate, _pcCountry, tContact._fSpeed);
                    
                    cBrotherSquare.CreateContact(2, tBrotherContact2._iFrom + (cIterator[0].nSizeX() - tContact._iFrom), 
                        (tContact._iFrom + tContact._nSize - cIterator[0].nSizeX()),
                        &cIterator[1], tBrotherContact2._bSimulate, _pcCountry, tBrotherContact2._fSpeed);
                    
                    cBrotherSquare.Contacts().Delete(iBrotherContact);
                }
            }
            else
            {
                // Contact is not devided
                ASSERT(tContact._iFrom + tContact._nSize <= cIterator[0].nSizeX() + cIterator[1].nSizeX());
                
                cIterator[1].CreateContact(0, tContact._iFrom -  cIterator[0].nSizeX(), tContact._nSize,
                    tContact._ptSquare, tContact._bSimulate, _pcCountry, tContact._fSpeed);
                
                tBrotherContact._ptSquare = &cIterator[1];
                
            }
            return;
        }
    case 1:
        {
            
            iBrotherContact = cBrotherSquare.FindContact(pcParentSquare, tContact._iFrom + pcParentSquare->TopLeftY());
            ASSERT(iBrotherContact >= 0);
            TPEARYCONTACT& tBrotherContact = cBrotherSquare.Contacts()[iBrotherContact];
            
            if (tContact._iFrom < cIterator[1].nSizeY())
            {
                
                if (tContact._iFrom + tContact._nSize <= cIterator[1].nSizeY())                            
                {            
                    cIterator[1].CreateContact(1, tContact._iFrom, tContact._nSize,
                        tContact._ptSquare, tContact._bSimulate, _pcCountry, tContact._fSpeed);
                    
                    tBrotherContact._ptSquare = &cIterator[1];
                }
                else
                {            
                    cIterator[1].CreateContact(1, tContact._iFrom, (cIterator[1].nSizeY() - tContact._iFrom),
                        tContact._ptSquare, tContact._bSimulate, _pcCountry, tContact._fSpeed);
                    
                    cBrotherSquare.CreateContact(3, tBrotherContact._iFrom, (cIterator[1].nSizeY() - tContact._iFrom),
                        &cIterator[1], tBrotherContact._bSimulate, _pcCountry, tBrotherContact._fSpeed);
                     
                    // Previes functions changed array refernec is not more valid
                    TPEARYCONTACT& tBrotherContact2 = cBrotherSquare.Contacts()[iBrotherContact];

                    ASSERT(tContact._iFrom + tContact._nSize <= cIterator[1].nSizeY() + cIterator[2].nSizeY());
                    
                    cIterator[2].CreateContact(1, 0, ( tContact._iFrom + tContact._nSize - cIterator[1].nSizeY() ),
                        tContact._ptSquare, tContact._bSimulate, _pcCountry, tContact._fSpeed);
                    
                    cBrotherSquare.CreateContact(3, tBrotherContact2._iFrom + (cIterator[1].nSizeY() - tContact._iFrom), 
                        (tContact._iFrom + tContact._nSize - cIterator[1].nSizeY()),
                        &cIterator[2], tBrotherContact2._bSimulate, _pcCountry, tBrotherContact2._fSpeed);
                    
                    cBrotherSquare.Contacts().Delete(iBrotherContact);
                }
            }
            else
            {
                
                ASSERT(tContact._iFrom + tContact._nSize <= cIterator[1].nSizeX() + cIterator[2].nSizeX());
                
                cIterator[2].CreateContact(1, tContact._iFrom -  cIterator[1].nSizeX(), tContact._nSize,
                    tContact._ptSquare, tContact._bSimulate, _pcCountry, tContact._fSpeed);
                
                tBrotherContact._ptSquare = &cIterator[2];
            }
            return;
        }
    case 2:  
        {
            
            iBrotherContact = cBrotherSquare.FindContact(pcParentSquare, tContact._iFrom + pcParentSquare->TopLeftX());
            ASSERT(iBrotherContact >= 0);
            TPEARYCONTACT& tBrotherContact = cBrotherSquare.Contacts()[iBrotherContact];
            
            if (tContact._iFrom < cIterator[3].nSizeX())
            {
                
                if (tContact._iFrom + tContact._nSize <= cIterator[3].nSizeX())                            
                {            
                    cIterator[3].CreateContact(2, tContact._iFrom, tContact._nSize,
                        tContact._ptSquare, tContact._bSimulate, _pcCountry, tBrotherContact._fSpeed);
                    
                    tBrotherContact._ptSquare = &cIterator[3];
                }
                else
                {            
                    cIterator[3].CreateContact(2, tContact._iFrom, (cIterator[3].nSizeX() - tContact._iFrom),
                        tContact._ptSquare, tContact._bSimulate, _pcCountry, tContact._fSpeed);
                    
                    cBrotherSquare.CreateContact(0, tBrotherContact._iFrom, (cIterator[3].nSizeX() - tContact._iFrom),
                        &cIterator[3], tBrotherContact._bSimulate, _pcCountry, tBrotherContact._fSpeed);
                                        
                    ASSERT(tContact._iFrom + tContact._nSize <= cIterator[3].nSizeX() + cIterator[2].nSizeX());

                    TPEARYCONTACT& tBrotherContact2 = cBrotherSquare.Contacts()[iBrotherContact];
                    
                    cIterator[2].CreateContact(2, 0, ( tContact._iFrom + tContact._nSize - cIterator[3].nSizeX() ),
                        tContact._ptSquare, tContact._bSimulate, _pcCountry, tContact._fSpeed);
                    
                    cBrotherSquare.CreateContact(0, tBrotherContact2._iFrom + (cIterator[3].nSizeX() - tContact._iFrom), 
                        (tContact._iFrom + tContact._nSize - cIterator[3].nSizeX()),
                        &cIterator[2], tBrotherContact2._bSimulate, _pcCountry, tBrotherContact2._fSpeed);
                    
                    cBrotherSquare.Contacts().Delete(iBrotherContact);
                }
            }
            else
            {
                
                ASSERT(tContact._iFrom + tContact._nSize <= cIterator[3].nSizeX() + cIterator[2].nSizeX());
                
                cIterator[2].CreateContact(2, tContact._iFrom -  cIterator[3].nSizeX(), tContact._nSize,
                    tContact._ptSquare, tContact._bSimulate, _pcCountry, tContact._fSpeed);
                
                tBrotherContact._ptSquare = &cIterator[2];
            }
            return;
        }
    case 3:
        {
            
            iBrotherContact = cBrotherSquare.FindContact(pcParentSquare, tContact._iFrom + pcParentSquare->TopLeftY());
            ASSERT(iBrotherContact >= 0);
            TPEARYCONTACT& tBrotherContact = cBrotherSquare.Contacts()[iBrotherContact];
            
            if (tContact._iFrom < cIterator[0].nSizeY())
            {
                
                if (tContact._iFrom + tContact._nSize <= cIterator[0].nSizeY())                            
                {            
                    cIterator[0].CreateContact(3, tContact._iFrom, tContact._nSize,
                        tContact._ptSquare, tContact._bSimulate, _pcCountry, tContact._fSpeed);
                    
                    tBrotherContact._ptSquare = &cIterator[0];
                }
                else
                {            
                    cIterator[0].CreateContact(3, tContact._iFrom, (cIterator[0].nSizeY() - tContact._iFrom),
                        tContact._ptSquare, tContact._bSimulate, _pcCountry, tContact._fSpeed);
                    
                    cBrotherSquare.CreateContact(1, tBrotherContact._iFrom, (cIterator[0].nSizeY() - tContact._iFrom),
                        &cIterator[0], tBrotherContact._bSimulate, _pcCountry, tBrotherContact._fSpeed);
                    
                    ASSERT(tContact._iFrom + tContact._nSize <= cIterator[0].nSizeY() + cIterator[3].nSizeY());

                    TPEARYCONTACT& tBrotherContact2 = cBrotherSquare.Contacts()[iBrotherContact];
                    
                    cIterator[3].CreateContact(3, 0, ( tContact._iFrom + tContact._nSize - cIterator[0].nSizeY() ),
                        tContact._ptSquare, tContact._bSimulate, _pcCountry, tContact._fSpeed);
                    
                    cBrotherSquare.CreateContact(1, tBrotherContact2._iFrom + (cIterator[0].nSizeY() - tContact._iFrom), 
                        (tContact._iFrom + tContact._nSize - cIterator[0].nSizeY()),
                        &cIterator[3], tBrotherContact2._bSimulate, _pcCountry, tBrotherContact2._fSpeed);
                    
                    cBrotherSquare.Contacts().Delete(iBrotherContact);
                }
            }
            else
            {
                
                ASSERT(tContact._iFrom + tContact._nSize <= cIterator[0].nSizeX() + cIterator[3].nSizeX());
                
                cIterator[3].CreateContact(3, tContact._iFrom -  cIterator[0].nSizeX(), tContact._nSize,
                    tContact._ptSquare, tContact._bSimulate, _pcCountry, tContact._fSpeed);
                
                tBrotherContact._ptSquare = &cIterator[3];
            }
            return;
        }
    default:
        ASSERT(0);
    }
}

void CPeary::SetUpArrows(ADDLINEFCN fcnAddArrow, void * pFcnData)
{
    CPearyWaterQuadTreeIterator cIterator(_cQuadTree);
    SetUpArrows(fcnAddArrow, pFcnData, cIterator);
}

void CPeary::SetUpArrows(ADDLINEFCN fcnAddArrow, void * pFcnData, CPearyWaterQuadTreeIterator& cIterator)
{
    float fArrowHeight = 340.0f;
    float fVelocityCoef = .2f;

    for(int i = 0; i < 4; i++)
    {
        if (cIterator.IsChild(i))
        {
            cIterator.ToChild(i);
            SetUpArrows( fcnAddArrow, pFcnData, cIterator);
            cIterator.ToParent();
        }
        else
        {
            for(unsigned int j = 0; j < cIterator[i].Contacts().GetSize(); j++)
            {
                TPEARYCONTACT& tContact = cIterator[i].Contacts()[j];

                if (tContact._bSimulate && tContact._fSpeed != 0.0f)
                {
                    switch(tContact._iBorder)
                    {
                    case 0:
                        {
                            float fX = (cIterator[i].TopLeftX() + tContact._iFrom + tContact._nSize / 2.0f) * _pcCountry->GetStepX();
                            float fY = cIterator[i].TopLeftY() * _pcCountry->GetStepY();

                            fcnAddArrow(pFcnData, fX, fY, fArrowHeight, fX, fY +fVelocityCoef * tContact._fSpeed, fArrowHeight);
                            break;
                        }
                    case 1:
                        {
                            float fX = (cIterator[i].TopLeftX() + cIterator[i].nSizeX()) * _pcCountry->GetStepX();
                            float fY = (cIterator[i].TopLeftY() + tContact._iFrom + tContact._nSize / 2.0f) * _pcCountry->GetStepY();

                            fcnAddArrow(pFcnData, fX, fY, fArrowHeight, fX + fVelocityCoef * tContact._fSpeed, fY, fArrowHeight);
                            break;
                        }
                    case 2:
                        {
                            float fX = (cIterator[i].TopLeftX() + tContact._iFrom + tContact._nSize / 2.0f) * _pcCountry->GetStepX();
                            float fY = (cIterator[i].TopLeftY() + cIterator[i].nSizeY()) * _pcCountry->GetStepY();

                            fcnAddArrow(pFcnData, fX, fY, fArrowHeight, fX, fY + fVelocityCoef * tContact._fSpeed, fArrowHeight);
                            break;
                        }
                    case 3:
                        {
                            float fX = cIterator[i].TopLeftX() * _pcCountry->GetStepX();
                            float fY = (cIterator[i].TopLeftY() + tContact._iFrom + tContact._nSize / 2.0f) * _pcCountry->GetStepY();

                            fcnAddArrow(pFcnData, fX, fY, fArrowHeight, fX + fVelocityCoef * tContact._fSpeed, fY, fArrowHeight);
                            break;
                        }
                    default:
                        ASSERT(0);                        
                    }
                }
            }
        }
    }
}

void CPeary::SetUpBorderLines(ADDLINEFCN fcnAddArrow, void * pFcnData, BOOL bVerticalLines)
{
    CPearyWaterQuadTreeIterator cIterator(_cQuadTree);
    SetUpBorderLines(fcnAddArrow, pFcnData, cIterator, bVerticalLines);
}

void CPeary::SetUpBorderLines(ADDLINEFCN fcnAddLine, void * pFcnData, CPearyWaterQuadTreeIterator& cIterator, BOOL bVerticalLines)
{
    float fLineHeight = 340.0f;
    

    for(int i = 0; i < 4; i++)
    {
        if (cIterator.IsChild(i))
        {
            cIterator.ToChild(i);
            SetUpBorderLines( fcnAddLine, pFcnData, cIterator, bVerticalLines);
            cIterator.ToParent();
        }
        else
        {
            float fTopLeftX = cIterator[i].TopLeftX() * _pcCountry->GetStepX();
            float fTopLeftY = cIterator[i].TopLeftY() * _pcCountry->GetStepY();

            fcnAddLine( pFcnData, fTopLeftX, fTopLeftY, fLineHeight, fTopLeftX + cIterator[i].fSizeX(), fTopLeftY, fLineHeight);
            fcnAddLine( pFcnData, fTopLeftX, fTopLeftY, fLineHeight, fTopLeftX, fTopLeftY + cIterator[i].fSizeY(), fLineHeight);
            fcnAddLine( pFcnData, fTopLeftX + cIterator[i].fSizeX(), fTopLeftY, fLineHeight, fTopLeftX + cIterator[i].fSizeX(), fTopLeftY + cIterator[i].fSizeY(), fLineHeight);
            fcnAddLine( pFcnData, fTopLeftX, fTopLeftY + cIterator[i].fSizeY(), fLineHeight, fTopLeftX + cIterator[i].fSizeX(), fTopLeftY + cIterator[i].fSizeY(), fLineHeight);            

            if (bVerticalLines)
            {
                fcnAddLine( pFcnData, fTopLeftX, fTopLeftY, fLineHeight, fTopLeftX , fTopLeftY, 0);

                fcnAddLine( pFcnData, fTopLeftX + cIterator[i].fSizeX(), fTopLeftY, fLineHeight, fTopLeftX + cIterator[i].fSizeX(), fTopLeftY, 0);

                fcnAddLine( pFcnData, fTopLeftX + cIterator[i].fSizeX(), fTopLeftY + cIterator[i].fSizeY(),
                    fLineHeight, fTopLeftX + cIterator[i].fSizeX(), fTopLeftY + + cIterator[i].fSizeY(), 0);

                fcnAddLine( pFcnData, fTopLeftX, fTopLeftY + cIterator[i].fSizeY(), fLineHeight, 
                    fTopLeftX , fTopLeftY + cIterator[i].fSizeY(), 0);
            }
        }
    }
}


float CalculateMinDistanceToSquare(const float x,
                                   const float y,
                                   const float fSquareOriginX, 
                                   const float fSquareOriginY, 
                                   const float fSizeX, 
                                   const float fSizeY); // already defined in Nansen.cpp 
/*{
    int iVoronyX;
    int iVoronyY;

    if (x < fSquareOriginX)
        iVoronyX = -1;
    else
    {
        if (x <= fSquareOriginX + fSizeX)
            iVoronyX = 0;
        else
            iVoronyX = 1;
    }

    if (y < fSquareOriginY)
        iVoronyY = -1;
    else
    {
        if (y <= fSquareOriginY + fSizeY)
            iVoronyY = 0;
        else
            iVoronyY = 1;
    }

    if (iVoronyX == 0 && iVoronyY == 0)
        return 0.0f;

    if (iVoronyX == 0)
    {
        return fabsf(y - (fSquareOriginY + (iVoronyY == 1 ? fSizeY : 0.0f)));
    }

    if (iVoronyY == 0)
    {
        return fabsf(x - (fSquareOriginX + (iVoronyX == 1 ? fSizeX : 0.0f)));
    }

    float fDiffX = x - (fSquareOriginX + (iVoronyX == 1 ? fSizeX : 0.0f));
    float fDiffY = y - (fSquareOriginY + (iVoronyY == 1 ? fSizeY : 0.0f));

    return sqrtf(fDiffX * fDiffX + fDiffY * fDiffY);
}
*/
void CPeary::CreatePatternTree(CPatternTreeIterator cIterator,
                                const float x, 
                                const float y, 
                                const float fSquareOriginX, 
                                const float fSquareOriginY, 
                                const float fSizeX, 
                                const float fSizeY,
                                const float fStopSize)
{
    for(int i = 0; i < 4; i++)
    {
        float fActualSquareOriginX = fSquareOriginX;
        float fActualSquareOriginY = fSquareOriginY;

        switch (i)
        {
        case 0:
            break;
        case 1:
            fActualSquareOriginX += fSizeX/2;
            break;
        case 2:
            fActualSquareOriginX += fSizeX/2;
            fActualSquareOriginY += fSizeY/2;
            break;
        case 3:
            fActualSquareOriginY += fSizeY/2;
            break;            
        default:;
        }
        
        float fMinDistance = CalculateMinDistanceToSquare(x,y,fActualSquareOriginX, fActualSquareOriginY, fSizeX/2, fSizeY/2);
        if ((fMinDistance/10 * fMinDistance/10 * fMinDistance / 10) / (fSizeX + fSizeY) < 1.0f && (fSizeX + fSizeY) / 4 > fStopSize)
        {
            cIterator.CreateChild(i);
            cIterator.ToChild(i);

            CreatePatternTree(cIterator, x, y, 
                fActualSquareOriginX, 
                fActualSquareOriginY, 
                fSizeX / 2, 
                fSizeY / 2, 
                fStopSize);
            cIterator.ToParent();
        }
    }
}

void CPeary::DesolveOldContact(unsigned int iBorder, 
                                unsigned int iSquare0, 
                                unsigned int iSquare1, 
                                CPearyWaterQuadTreeIterator& cIterator, 
                                CPearySquare& cParentSquare)
{
    MyPearyContactArray cArray;

    const int nContacts0 = cIterator[iSquare0].RetrieveContacts( cArray, iBorder);
    const int nContacts1 = cIterator[iSquare1].RetrieveContacts( cArray, iBorder);
    unsigned int nParentContacts = cParentSquare.Contacts().GetSize();

    for(int i = 0; i <  nContacts0; i ++)
    { 
        const TPEARYCONTACT& cContact0 = cArray[i];

        int iContact1ID = -1;
        for(unsigned int j = nContacts0; j < cArray.GetSize(); j++)
        {
            if (cContact0._ptSquare == cArray[j]._ptSquare)
            {
                iContact1ID = j;
                break;
            }
        }

        
        if (iContact1ID == -1)
        {
            // contact is not divided
            TPEARYCONTACT tParentContact;
            memcpy( &tParentContact, &cContact0, sizeof(tParentContact));

            if (tParentContact._iBorder % 2 == 0)
            {            
                tParentContact._iFrom = cIterator[iSquare0].TopLeftX() + cContact0._iFrom - cParentSquare.TopLeftX();
                tParentContact._ptSquare->FindContact2(&cIterator[iSquare0], tParentContact._iFrom + cParentSquare.TopLeftX())._ptSquare = &cParentSquare;
            }
            else
            {            
                tParentContact._iFrom = cIterator[iSquare0].TopLeftY() + cContact0._iFrom - cParentSquare.TopLeftY();
                tParentContact._ptSquare->FindContact2(&cIterator[iSquare0], tParentContact._iFrom + cParentSquare.TopLeftY())._ptSquare = &cParentSquare;
            }            

            cParentSquare.Contacts().Add(tParentContact);
        }
        else
        {
            // contact is divided
            const TPEARYCONTACT& cContact1 = cArray[iContact1ID];

            ASSERT(cContact0._bSimulate == cContact1._bSimulate);
            ASSERT(cContact0._ptSquare == cContact1._ptSquare);
            ASSERT(cContact0._iBorder == cContact1._iBorder);            
            
            CPearySquare& cBrotherSquare = *cContact0._ptSquare;
            int iBrother0, iBrother1;

            if (cContact0._iBorder % 2 == 0)
            {                                            
                cParentSquare.CreateContact(cContact0._iBorder, cIterator[iSquare0].TopLeftX() + cContact0._iFrom - cParentSquare.TopLeftX(),
                    cContact0._nSize + cContact1._nSize,
                    cContact0._ptSquare, cContact0._bSimulate, _pcCountry, (cContact0._fSpeed + cContact1._fSpeed) / 2.0f);

                iBrother0 = cBrotherSquare.FindContact(&cIterator[iSquare0], cIterator[iSquare0].TopLeftX() + cContact0._iFrom);
                iBrother1 = cBrotherSquare.FindContact(&cIterator[iSquare1], cIterator[iSquare1].TopLeftX() + cContact1._iFrom);
            }
            else
            {
                cParentSquare.CreateContact(cContact0._iBorder, cIterator[iSquare0].TopLeftY() + cContact0._iFrom - cParentSquare.TopLeftY(),
                    cContact0._nSize + cContact1._nSize,
                    cContact0._ptSquare, cContact0._bSimulate, _pcCountry, (cContact0._fSpeed + cContact1._fSpeed) / 2.0f);

                iBrother0 = cBrotherSquare.FindContact(&cIterator[iSquare0], cIterator[iSquare0].TopLeftY() + cContact0._iFrom);
                iBrother1 = cBrotherSquare.FindContact(&cIterator[iSquare1], cIterator[iSquare1].TopLeftY() + cContact1._iFrom);
                    
            }

            ASSERT(iBrother0 >= 0);
            ASSERT(iBrother1 >= 0);
            
            cBrotherSquare.CreateContact(cBrotherSquare.Contacts()[iBrother0]._iBorder, cBrotherSquare.Contacts()[iBrother0]._iFrom,
                cContact0._nSize + cContact1._nSize,
                &cParentSquare, !cContact0._bSimulate, _pcCountry, 
                (cBrotherSquare.Contacts()[iBrother0]._fSpeed + cBrotherSquare.Contacts()[iBrother1]._fSpeed) / 2.0f);

            cBrotherSquare.Contacts().Delete(iBrother0);
            if (iBrother1 > iBrother0)
                cBrotherSquare.Contacts().Delete(iBrother1 - 1);
            else
                cBrotherSquare.Contacts().Delete(iBrother1);
            
            cArray.Delete(iContact1ID);
        }
    }

    for(unsigned int i = nContacts0; i < cArray.GetSize(); i ++)
    {
        TPEARYCONTACT tParentContact;
        memcpy( &tParentContact, &cArray[i], sizeof(tParentContact));
        
        if (tParentContact._iBorder % 2 == 0)
        {
        
            tParentContact._iFrom = cIterator[iSquare1].TopLeftX() + cArray[i]._iFrom - cParentSquare.TopLeftX();
            tParentContact._ptSquare->FindContact2(&cIterator[iSquare1], tParentContact._iFrom + cParentSquare.TopLeftX())._ptSquare = &cParentSquare;
        }
        else
        {        
            tParentContact._iFrom = cIterator[iSquare1].TopLeftY() + cArray[i]._iFrom - cParentSquare.TopLeftY();
            tParentContact._ptSquare->FindContact2(&cIterator[iSquare1], tParentContact._iFrom + cParentSquare.TopLeftY())._ptSquare = &cParentSquare;
        }
                  
        cParentSquare.Contacts().Add(tParentContact);
    }
}

void CPeary::DeTeselate(CPearyWaterQuadTreeIterator& cIterator, unsigned int iChild)
{

    CPearySquare& cParentSquare = cIterator[iChild];
    cParentSquare.Contacts().Release();

    ASSERT(cIterator.IsChild(iChild));

    cIterator.ToChild(iChild);

    for(int i = 0; i < 4; i++)
    {
        if (cIterator.IsChild(i))
        {            
            DeTeselate(cIterator,i);
        }
    }

    DesolveOldContact(0,0,1, cIterator, cParentSquare);
    DesolveOldContact(1,1,2, cIterator, cParentSquare);
    DesolveOldContact(2,3,2, cIterator, cParentSquare);
    DesolveOldContact(3,0,3, cIterator, cParentSquare);

    
    cParentSquare.DiffMass() = 0;
#ifdef USE_MASS_CRITERIUM
    cParentSquare.Mass() = cIterator[0].Mass() + cIterator[1].Mass() + cIterator[2].Mass() + cIterator[3].Mass();
    cParentSquare.CalculateWaterHeightAndSurfaceFromMass();
#else
    float fAverageHeight = 0.0f;
    unsigned int nN = 0;

    for(int i = 0; i < 4; i++)
    {
        if (cIterator[i].Mass() != 0.0f && fAverageHeight < cIterator[i].AverageWaterHeight())
        {
            fAverageHeight = cIterator[i].AverageWaterHeight();
            nN++;
        }
    }
    
    cParentSquare.AverageWaterHeight() = fAverageHeight ;
    cParentSquare.CalculateMassFromWaterHeight();
#endif

//    cParentSquare.Velocity()[0] = (cIterator[0].Velocity()[0] +  cIterator[1].Velocity()[0] + 
//         cIterator[2].Velocity()[0] +  cIterator[3].Velocity()[0]) / 4.0f;
//    cParentSquare.Velocity()[1] = (cIterator[0].Velocity()[1] +  cIterator[1].Velocity()[1] + 
//         cIterator[2].Velocity()[1] +  cIterator[3].Velocity()[1]) / 4.0f;

    cIterator.ToParent();

    cIterator.DeleteChild(iChild);    
}

void CPeary::DeTeselate(CPearyWaterQuadTreeIterator& cIterator, MyArray<unsigned int>& cZone)
{
    for(unsigned int i = 0; i < cZone.GetSize() - 1; i++)
    {
        if (cIterator.IsChild(cZone[i]))                                                        
            cIterator.ToChild(cZone[i]);         
        else
            return; // No deteselation needed.         
    }
    
    unsigned int iLastZoneId = cZone[cZone.GetSize() - 1];
    if (!cIterator.IsChild(iLastZoneId))
        return; // No deteselation needed.         

    DeTeselate(cIterator,iLastZoneId);
}

void CPeary::DeTeselate(MyArray<unsigned int>& cZone)
{
    CPearyWaterQuadTreeIterator cIterator(_cQuadTree);

    DeTeselate(cIterator, cZone);

    cIterator.ToTop();
    UpdateContactsLength(cIterator);
    
   /* cIterator.ToTop();
    _cWaterMass.ZeroItems();    
    CalculateWaterHeightFromQuadTree(cIterator);

    cIterator.ToTop();
    _cType.ZeroItems();
    CalculateNet(cIterator);
    */
}

void CPeary::SynchronizeWithPatternTree(CPatternTreeIterator& cPatternIterator, CPearyWaterQuadTreeIterator& cIterator)
{
    for(int i = 0; i < 4; i++)
    {
        if (cPatternIterator.IsChild(i) && cIterator.IsChild(i))
        {
            cPatternIterator.ToChild(i);
            cIterator.ToChild(i);

            SynchronizeWithPatternTree(cPatternIterator, cIterator);

            cPatternIterator.ToParent();
            cIterator.ToParent();
        }
        else
        {
            if (cPatternIterator.IsChild(i))
            {
                // Teselate
                MyArray<unsigned int> cZone;
                cZone.Add(i);
                Teselate(cIterator, cZone);

                cPatternIterator.ToChild(i);
                //cIterator.ToChild(i); // Is allready in child because of teselation.

                SynchronizeWithPatternTree(cPatternIterator, cIterator);

                cPatternIterator.ToParent();
                cIterator.ToParent();
            }
            else
                if (cIterator.IsChild(i))
                {
                    // deteselate                    
                    DeTeselate(cIterator,i);                    
                }
        }
    }
}

void CPeary::MoveTeselationToPoint(float x, float y, unsigned int iLowestLevel)
{
    CPatternTree cPatternTree;
    CPatternTreeIterator cPatternIterator(cPatternTree);

    CreatePatternTree(cPatternIterator,
                                x, 
                                y, 
                                0.0f, 
                                0.0f, 
                                _pcCountry->GetSizeX() * _pcCountry->GetStepX(), 
                                _pcCountry->GetSizeY() * _pcCountry->GetStepY(),
                               // _pcCountry->GetSizeX() * _pcCountry->GetStepX() / (1 << iLowestLevel));
                                _pcCountry->GetStepX());
    cPatternIterator.ToTop();

    CPearyWaterQuadTreeIterator cIterator(_cQuadTree);

    SynchronizeWithPatternTree(cPatternIterator, cIterator);

    cIterator.ToTop();
    UpdateContactsLength(cIterator);

    // Update _cType a _cWaterMass
   /* cIterator.ToTop();
    _cWaterMass.ZeroItems();    
    CalculateWaterHeightFromQuadTree(cIterator);

    cIterator.ToTop();
    _cType.ZeroItems();
    CalculateNet(cIterator);
    */
}

// Calculates contacts length.
void CPeary::UpdateContactsLength(CPearyWaterQuadTreeIterator& cIterator)
{
    for(int i = 3; i >= 0; i--)
    {
        if (cIterator.IsChild(i))
        {
            cIterator.ToChild(i);
            UpdateContactsLength(cIterator);
            cIterator.ToParent();
        }
        else
        {
            unsigned int nContacts = cIterator[i].Contacts().GetSize();
            for(unsigned int j = 0; j < nContacts; j++)
            {
                
                TPEARYCONTACT& tContact = cIterator[i].Contacts()[j];
                                                
                switch (tContact._iBorder)
                {
                case 0:
                    tContact._fLength = (cIterator[i].fSizeY() + tContact._ptSquare->fSizeY())/2;                        
                    break;
                case 1:
                    tContact._fLength = -(cIterator[i].fSizeX() + tContact._ptSquare->fSizeX())/2;
                    break;
                case 2:
                    tContact._fLength = -(cIterator[i].fSizeY() + tContact._ptSquare->fSizeY())/2;
                    break;
                case 3:
                    tContact._fLength = (cIterator[i].fSizeX() + tContact._ptSquare->fSizeX())/2;
                    break;
                default:
                    ASSERT(0);
                }                                       
                
                tContact._fInvLength = 1.0f / tContact._fLength;
            }
        }
    }                        
}

void CPeary::PrepareZoomedAndOffseted(unsigned int nSizeX, 
        unsigned int nSizeY, 
        unsigned int iZoomLevel, 
        unsigned int iOffsetX, 
        unsigned int iOffsetY)
{
    _cWaterHeightZoomed.Init(nSizeX, nSizeY);
    _cWaterMassZoomed.Init(nSizeX, nSizeY);

    // Update Mass and Water field.
    CPearyWaterQuadTreeIterator cIterator(_cQuadTree);
    
    cIterator.ToTop();
    _cWaterMass.ZeroItems();    
    CalculateWaterHeightFromQuadTree(cIterator);
    
    

    _cWaterHeight.GetZoomedAndOffseted(_cWaterHeightZoomed, iZoomLevel, iOffsetX, iOffsetY);
    _cWaterMass.GetZoomedAndOffseted(_cWaterMassZoomed, iZoomLevel, iOffsetX, iOffsetY);
}
   
#define INFTY 999999999
 
void CPeary::CreateWaterVertexes(
      CFlyShapeSimCoord& cShape,
      unsigned int nSizeX,
      unsigned int nSizeY,
      unsigned int iZoomLevel,
      unsigned int iOffsetX,
      unsigned int iOffsetY,
      const RGBQUAD& tColor)
{   
    unsigned int iWindowDownRightX = iOffsetX + (1 << iZoomLevel) * nSizeX;
    unsigned int iWindowDownRightY = iOffsetY
        + (1 << iZoomLevel) * nSizeY;
    
    CPearyWaterQuadTreeIterator cIterator(_cQuadTree);
    float fMaxPureHeight = 0;
    float fMinPureHeight = INFTY;

    FindMaxMinPureWaterHeight(cIterator, fMaxPureHeight, fMinPureHeight);
    //LogF("max height %lf, min height %lf", fMaxPureHeight, fMinPureHeight);


    cIterator.ToTop();
    CreateWaterVertexes( cIterator, cShape, iOffsetX, iOffsetY, iWindowDownRightX, iWindowDownRightY, fMaxPureHeight, fMinPureHeight, tColor);
}


typedef struct
{
    int _nX;
    int _nY;
    float _fHeight;
    float _fPureHeight;
    BOOL _bDraw;
} TMYVERTEX;



int CompareVertexes(const void * elem1, const void * elem2)
{
    const TMYVERTEX * pVertex1 = (const TMYVERTEX *) elem1;
    const TMYVERTEX * pVertex2 = (const TMYVERTEX *) elem2;

    if (pVertex1->_nX > 0) 
    {
        if (pVertex2->_nX < 0)
            return -1;
        
        float tang1 = pVertex1->_nY / (float)pVertex1->_nX;

        float tang2;
        if (pVertex2->_nX == 0)
            return (pVertex2->_nY > 0) ?  -1 : 1;
        else
            tang2 = pVertex2->_nY / (float)pVertex2->_nX;

        
        return  (tang1 < tang2) ? -1 : 1;
    }

    if (pVertex1->_nX == 0)
    {
        if (pVertex2->_nX < 0)
            return -1;

        if (pVertex2->_nX > 0)
            return (pVertex1->_nY < 0) ? -1 : 1;
        else        
        {
            ASSERT(pVertex2->_nX == 0);
            if (pVertex2->_nY * pVertex1->_nY > 0)
            {
                return -1;
            }
            else
                return (pVertex1->_nY < 0) ? -1 : 1;           
        }       
    }
    else
    {
        ASSERT(pVertex1->_nX < 0);
        
        if (pVertex2->_nX >= 0)            
            return 1;
            
        float tang1 = pVertex1->_nY / (float)pVertex1->_nX;
        
        float tang2 = pVertex2->_nY / (float)pVertex2->_nX;

        return  (tang1 < tang2) ? -1 : 1;
    }
}


void CPeary::CreateWaterVertexes(CPearyWaterQuadTreeIterator& cIterator, 
                                 CFlyShapeSimCoord& cShape, 
                                 unsigned int iWindowTopLeftX,
                                 unsigned int iWindowTopLeftY,
                                 unsigned int iWindowDownRightX,
                                 unsigned int iWindowDownRightY,
                                 float fMaxPureHeight,
                                 float fMinPureHeight,
                                 const RGBQUAD& tColor)
{
    const float fRangePureHeight = fMaxPureHeight - fMinPureHeight;
    
    for(int i = 3; i >= 0; i--)
    {

        CPearySquare& cActualSquare = cIterator[i];
                
        //if (min(iWindowDownRightX, cActualSquare.TopLeftX() + cActualSquare.nSizeX()) > max(iWindowTopLeftX, cActualSquare.TopLeftX()) &&
         //   min(iWindowDownRightY, cActualSquare.TopLeftY() + cActualSquare.nSizeY()) > max(iWindowTopLeftY, cActualSquare.TopLeftY()) )
        {
            // they have intersection
            
            if (cIterator.IsChild(i))
            {                   
                cIterator.ToChild(i);
                CreateWaterVertexes(cIterator, cShape, iWindowTopLeftX, iWindowTopLeftY, iWindowDownRightX, iWindowDownRightY,fMaxPureHeight,fMinPureHeight,tColor);
                cIterator.ToParent();
            }
            else
            {
                if (cActualSquare.Mass() != 0.0f)
                {
                    int iCenterX = cActualSquare.TopLeftX() + cActualSquare.nSizeX() / 2;
                    int iCenterY = cActualSquare.TopLeftY() + cActualSquare.nSizeY() / 2;
                    
                    if (iCenterX <= iWindowDownRightX && iCenterY <= iWindowDownRightY)
                    {
                        // do vertexes    
                        MyPearyContactArray& cContactArray = cActualSquare.Contacts();
                        TMYVERTEX * pVertexes = new TMYVERTEX[cContactArray.GetSize()];
                        
                        unsigned int nInVertexes = 0;
                        
                        for(; nInVertexes < cContactArray.GetSize(); nInVertexes++)
                        {
                            CPearySquare& cBrotherSquare = * cContactArray[nInVertexes]._ptSquare;                            
                            
                            pVertexes[nInVertexes]._fHeight = cBrotherSquare.AverageWaterHeight();
                            pVertexes[nInVertexes]._fPureHeight = cBrotherSquare.AverageWaterHeight() - cBrotherSquare.MinCountryHeight();
                            pVertexes[nInVertexes]._nX = cBrotherSquare.TopLeftX() + (int) cBrotherSquare.nSizeX() / 2 - iCenterX;
                            pVertexes[nInVertexes]._nY = cBrotherSquare.TopLeftY() + (int) cBrotherSquare.nSizeY() / 2 - iCenterY;

                            // Do not connect with fictive borders
                            if (&cBrotherSquare != &(_pFictiveBorders[cContactArray[nInVertexes]._iBorder]))
                                pVertexes[nInVertexes]._bDraw = TRUE;
                            else
                                pVertexes[nInVertexes]._bDraw = FALSE;
                                
                        }
                        
                        // Order vertexes anticlockwisely
                        qsort( (void *) pVertexes, nInVertexes, sizeof(*pVertexes), CompareVertexes);
                        
                        // Put vertexes into shape                            
                        float fCenterX = iCenterX * _pcCountry->GetStepX();
                        float fCenterY = iCenterY * _pcCountry->GetStepY();
                        float fHeight = cActualSquare.AverageWaterHeight();
                        float fPureHeight = cActualSquare.AverageWaterHeight() - cActualSquare.MinCountryHeight();

                        RGBQUAD tResultColor;
                        float fColorCoef;
                        
                        
                        for(unsigned int i = 0; i < nInVertexes - 1; i++)
                        {
                            if  (pVertexes[i]._bDraw && pVertexes[i + 1]._bDraw)
                            {
                                fColorCoef = (fPureHeight - fMinPureHeight) / fRangePureHeight * 0.6f + 0.4f;
                                    tResultColor.rgbBlue = tColor.rgbBlue * fColorCoef;
                                tResultColor.rgbGreen = tColor.rgbGreen * fColorCoef;
                                tResultColor.rgbRed = tColor.rgbRed * fColorCoef;
                                
                                cShape.AddVertex(fCenterX, fCenterY, fHeight, tResultColor);
                                
                                if (pVertexes[i + 1]._fPureHeight != 0.0f) // no water in it
                                {
                                    fColorCoef = (pVertexes[i + 1]._fPureHeight - fMinPureHeight) / fRangePureHeight * 0.6f + 0.4f;
                                    tResultColor.rgbBlue = tColor.rgbBlue * fColorCoef;
                                    tResultColor.rgbGreen = tColor.rgbGreen * fColorCoef;
                                    tResultColor.rgbRed = tColor.rgbRed * fColorCoef;
                                    
                                    cShape.AddVertex((pVertexes[i + 1]._nX + iCenterX) * _pcCountry->GetStepX(),
                                        (pVertexes[i + 1]._nY + iCenterY) * _pcCountry->GetStepY(),
                                        pVertexes[i + 1]._fHeight, 
                                        tResultColor);
                                }
                                else
                                {
                                    fColorCoef = (fPureHeight - fMinPureHeight) / fRangePureHeight * 0.6f + 0.4f;
                                        tResultColor.rgbBlue = tColor.rgbBlue * fColorCoef;
                                    tResultColor.rgbGreen = tColor.rgbGreen * fColorCoef;
                                    tResultColor.rgbRed = tColor.rgbRed * fColorCoef;
                                    
                                    cShape.AddVertex((pVertexes[i + 1]._nX + iCenterX) * _pcCountry->GetStepX(),
                                        (pVertexes[i + 1]._nY + iCenterY) * _pcCountry->GetStepY(),
                                        fHeight, 
                                        tResultColor);
                                }
                                
                                if (pVertexes[i]._fPureHeight != 0.0f) // no water in it
                                {
                                    
                                    fColorCoef = (pVertexes[i]._fPureHeight - fMinPureHeight) / fRangePureHeight * 0.6f + 0.4f;
                                    tResultColor.rgbBlue = tColor.rgbBlue * fColorCoef;
                                    tResultColor.rgbGreen = tColor.rgbGreen * fColorCoef;
                                    tResultColor.rgbRed = tColor.rgbRed * fColorCoef;
                                    
                                    cShape.AddVertex((pVertexes[i]._nX + iCenterX) * _pcCountry->GetStepX(),
                                        (pVertexes[i]._nY + iCenterY) * _pcCountry->GetStepY(),
                                        pVertexes[i]._fHeight, 
                                        tResultColor);
                                }
                                else
                                {
                                    fColorCoef = (fPureHeight - fMinPureHeight) / fRangePureHeight * 0.6f + 0.4f;
                                        tResultColor.rgbBlue = tColor.rgbBlue * fColorCoef;
                                    tResultColor.rgbGreen = tColor.rgbGreen * fColorCoef;
                                    tResultColor.rgbRed = tColor.rgbRed * fColorCoef;
                                    
                                    cShape.AddVertex((pVertexes[i]._nX + iCenterX) * _pcCountry->GetStepX(),
                                        (pVertexes[i]._nY + iCenterY) * _pcCountry->GetStepY(),
                                        fHeight, 
                                        tResultColor);
                                }
                            }
                        }
                        
                        if  (pVertexes[0]._bDraw && pVertexes[nInVertexes - 1]._bDraw)
                        {
                            fColorCoef = (fPureHeight - fMinPureHeight) / fRangePureHeight * 0.6f + 0.4f;
                            tResultColor.rgbBlue = tColor.rgbBlue * fColorCoef;
                            tResultColor.rgbGreen = tColor.rgbGreen * fColorCoef;
                            tResultColor.rgbRed = tColor.rgbRed * fColorCoef;
                            
                            cShape.AddVertex(fCenterX, fCenterY, fHeight, tResultColor);
                            
                            if (pVertexes[0]._fPureHeight != 0.0f) // no water in it
                            {
                                
                                fColorCoef = (pVertexes[0]._fPureHeight - fMinPureHeight) / fRangePureHeight * 0.6f + 0.4f;
                                tResultColor.rgbBlue = tColor.rgbBlue * fColorCoef;
                                tResultColor.rgbGreen = tColor.rgbGreen * fColorCoef;
                                tResultColor.rgbRed = tColor.rgbRed * fColorCoef;
                                
                                cShape.AddVertex((pVertexes[0]._nX + iCenterX) * _pcCountry->GetStepX(),
                                    (pVertexes[0]._nY + iCenterY) * _pcCountry->GetStepY(),
                                    pVertexes[0]._fHeight, 
                                    tResultColor);
                            }
                            else
                            {
                                
                                fColorCoef = (fPureHeight - fMinPureHeight) / fRangePureHeight * 0.6f + 0.4f;
                                tResultColor.rgbBlue = tColor.rgbBlue * fColorCoef;
                                tResultColor.rgbGreen = tColor.rgbGreen * fColorCoef;
                                tResultColor.rgbRed = tColor.rgbRed * fColorCoef;
                                
                                cShape.AddVertex((pVertexes[0]._nX + iCenterX) * _pcCountry->GetStepX(),
                                    (pVertexes[0]._nY + iCenterY) * _pcCountry->GetStepY(),
                                    fHeight, 
                                    tResultColor);                                
                            }
                            
                            if (pVertexes[nInVertexes - 1]._fPureHeight != 0.0f) // no water in it
                            {
                                fColorCoef = (pVertexes[nInVertexes - 1]._fPureHeight - fMinPureHeight) / fRangePureHeight * 0.6f + 0.4f;                                
                                tResultColor.rgbBlue = tColor.rgbBlue * fColorCoef;
                                tResultColor.rgbGreen = tColor.rgbGreen * fColorCoef;
                                tResultColor.rgbRed = tColor.rgbRed * fColorCoef;
                                
                                cShape.AddVertex((pVertexes[nInVertexes - 1]._nX + iCenterX) * _pcCountry->GetStepX(),
                                    (pVertexes[nInVertexes - 1]._nY + iCenterY) * _pcCountry->GetStepY(),
                                    pVertexes[nInVertexes - 1]._fHeight, 
                                    tResultColor);
                            }
                            else
                            {
                                fColorCoef = (fPureHeight - fMinPureHeight) / fRangePureHeight * 0.6f + 0.4f;
                                tResultColor.rgbBlue = tColor.rgbBlue * fColorCoef;
                                tResultColor.rgbGreen = tColor.rgbGreen * fColorCoef;
                                tResultColor.rgbRed = tColor.rgbRed * fColorCoef;
                                
                                cShape.AddVertex((pVertexes[nInVertexes - 1]._nX + iCenterX) * _pcCountry->GetStepX(),                                    
                                    (pVertexes[nInVertexes - 1]._nY + iCenterY) * _pcCountry->GetStepY(),
                                    fHeight, 
                                    tResultColor);
                            }
                        }  
                        
                        delete []pVertexes;
                    }               
                }
            }
        }
    }
}


void CPeary::FindMaxMinPureWaterHeight(CPearyWaterQuadTreeIterator& cIterator, float& fMaxHeight, float& fMinHeight)
{
    for(int i = 3; i >= 0; i--)
    {

        if (cIterator.IsChild(i))
        {                   
            cIterator.ToChild(i);
            FindMaxMinPureWaterHeight(cIterator, fMaxHeight, fMinHeight);
            cIterator.ToParent();
        }
        else
        {
            float fPureWaterHeight = cIterator[i].AverageWaterHeight() -  cIterator[i].MinCountryHeight();
            if (fPureWaterHeight > fMaxHeight)
                fMaxHeight = fPureWaterHeight;
            
            if (fPureWaterHeight < fMinHeight)
                fMinHeight = fPureWaterHeight;                
        }
    }
}