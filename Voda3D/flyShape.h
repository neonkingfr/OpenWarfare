#ifndef _flyShape_h_
#define _flyShape_h_

#define BUFFER_VERTEX_COUNT 32768
#define SHAPE_VERTEX_COUNT 1131072



struct SFlyVertex {
  D3DXVECTOR3 _position;  // vertex position
  DWORD       _diffuse;   // diffuse color
};

#define D3DFVF_FLYVERTEX (D3DFVF_XYZ|D3DFVF_DIFFUSE)

//! Shape with huge number of vertices
class CFlyShape {
private:
  //! Vertex buffer
  LPDIRECT3DVERTEXBUFFER8 _vb;
  //! Array of vertices
  SFlyVertex _vertices[SHAPE_VERTEX_COUNT];
  //! Number of vertices in the array
  int _vertexCount;
public:
  //! Constructor
  CFlyShape();
  //! Destructor
  ~CFlyShape();
  //! Initialization
  void Init(IDirect3DDevice8 *pD3DDevice);
  //! Prepares shape for new stream of primitives
  void Reset();
  //! Add one vertex
  void AddVertex(const SFlyVertex &vertex);
  //! Drawing of triangles
  void DrawTriangles(IDirect3DDevice8 *pD3DDevice);
  //! Drawing of lines
  void DrawLines(IDirect3DDevice8 *pD3DDevice);
};

#endif