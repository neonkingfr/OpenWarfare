//-----------------------------------------------------------------------------
// File: Voda3D.cpp
//
// Desc: DirectX window application created by the DirectX AppWizard
//-----------------------------------------------------------------------------
#define STRICT
#define DIRECTINPUT_VERSION 0x0800
#include <windows.h>
#include <basetsd.h>
#include <math.h>
#include <stdio.h>
#include <D3DX8.h>
#include <DXErr8.h>
#include <tchar.h>
#include <dinput.h>
#include "D3DApp.h"
#include "D3DUtil.h"
#include "DXUtil.h"
#include "resource.h"
#include "stdafx.h"
#include "Voda3D.h"


#define MIN_WATER_HEIGHT 0.0f

//#define RUN_SECOND_SIMUL


//-----------------------------------------------------------------------------
// Global access to the app (needed for the global WndProc())
//-----------------------------------------------------------------------------
CMyD3DApplication* g_pApp  = NULL;
HINSTANCE          g_hInst = NULL;
CommandLineParam GCLParam;

// Parse command line params.
bool ParseCommandLine(LPCSTR cmdLine) // valid command line
{
  const char * cfgStart = strstr(cmdLine, "-cfg");
  if (!cfgStart)
    goto error;

  cfgStart += 4;

  while (*cfgStart != '\0' && (*cfgStart == ' ' || *cfgStart == '\t' || *cfgStart == '=')) {cfgStart++;};

  if (*cfgStart == '\0')
    goto error;

  const char * cfgEnd = cfgStart;
  while (*cfgEnd != '\0' && (*cfgEnd != ' ' && *cfgEnd != '\t')) {cfgEnd++;};

  char * cfgFileName = new char[cfgEnd - cfgStart + 1];
  strncpy(cfgFileName, cfgStart, cfgEnd - cfgStart + 1);


  if (LSOK !=  GCLParam._cfgFile.Parse(cfgFileName))
  {
    printf("Not valid cfgFile.\n");
    goto error;
  }

  return true;
  
error:
  printf("Wrong commad line paramaters.\n\n Syntax:\n\n   Voda3D.exe -cfg cfgFilePath\n");
  return false;
}



//-----------------------------------------------------------------------------
// Name: WinMain()
// Desc: Entry point to the program. Initializes everything, and goes into a
//       message-processing loop. Idle time is used to render the scene.
//-----------------------------------------------------------------------------
INT WINAPI WinMain( HINSTANCE hInst, HINSTANCE, LPSTR cmdLine, INT )
{
  if (!ParseCommandLine(cmdLine))
    return 0;

  CMyD3DApplication d3dApp;

  g_pApp  = &d3dApp;
  g_hInst = hInst;

  if( FAILED( d3dApp.Create( hInst ) ) )
    return 0;

  INT ret =  d3dApp.Run();
  GCLParam._cfgFile.Clear();
  return ret;
}




//-----------------------------------------------------------------------------
// Name: CMyD3DApplication()
// Desc: Application constructor. Sets attributes for the app.
//-----------------------------------------------------------------------------
CMyD3DApplication::CMyD3DApplication()
{
    m_dwCreationWidth           = 500;
    m_dwCreationHeight          = 375;
    m_strWindowTitle            = TEXT( "Voda3D" );
    m_bUseDepthBuffer           = TRUE;

    m_pD3DXFont                 = NULL;
    m_bLoadingApp               = TRUE;
//    m_pCIB                      = NULL;

    m_pWVB1 = new CFlyShape();
    m_pWVB2 = new CFlyShape();
    m_pCVB  = new CFlyShape();
    
    m_pDI                       = NULL;
    m_pKeyboard                 = NULL;
    
    m_pLineVB1 = new CFlyShape();
    m_pLineVB2 = new CFlyShape();

    m_pHierarchyVB1 = new CFlyShape();
    m_pHierarchyVB2 = new CFlyShape();

    m_pWVB2_1 = new CFlyShapeSimCoord();
    m_pWVB2_2 = new CFlyShapeSimCoord();

    ZeroMemory( &m_UserInput, sizeof(m_UserInput) );
    m_CameraPosition             = D3DXVECTOR3(0, 65, -20);
    m_fCameraRotX                = 30.0f/180.0f * 3.1415;
    m_fCameraRotY                = 0.0f;

    // Init country
    //m_Country.LoadPbl("S:\\markus\\Maps\\Istan\\istan.pbl");   
    ParamClassPtr cfgClass = GCLParam._cfgFile.GetClass("InOut");
    if (cfgClass.IsNull())
    {
      LogF("Entry %s, not found in cfgFile.","InOut");
      LogF("Cannot load country.");      
      return;
    }

    ParamEntryPtr entry = cfgClass->FindEntry("island");
    if (entry.IsNull())
    {
      LogF("Entry %s, not found in cfgFile.","island");
      LogF("Cannot load country.");           
      return ;
    }

    m_Country.LoadPbl((LPCSTR)((RStringB)(*entry)));
    m_DrawCountry.Init(DRAW_SIZE, DRAW_SIZE, 1.0f, 1.0f);

    //m_Country.Init(MAP_SIZE,MAP_SIZE, 0.000832f, 0.000835f);
    //m_Country.Load("F1024.ase");
    //m_Country.Load("Feh64x64.ase");
    //m_Country.Load("Fehy03.ase");
    //m_Country.Load("reka2.ase");
    //m_Country.SetStepX(1.0f);
    //m_Country.SetStepY(1.0f);

    //m_Country.CreateDamCountry(MAP_SIZE);  
    //m_Country.CreateFlatLand(MAP_SIZE); 
    //m_Country.CreateCustomLand(MAP_SIZE); 

    m_Country.GetZoomedAndOffseted(m_DrawCountry, 
        g_cCoordConverter.GetZoomLevel(), 
        g_cCoordConverter.GetOffsetX(), 
        g_cCoordConverter.GetOffsetY());
        
    //m_Country.CreateCustomLand(MAP_SIZE);  

    m_pPlayer1Pos[0] = m_Country.GetSizeX() * m_Country.GetStepX() / 2;
    m_pPlayer1Pos[1] = m_Country.GetSizeY() * m_Country.GetStepY() / 2;

    m_pPlayer2Pos[0] = m_Country.GetSizeX() * m_Country.GetStepX() / 2;
    m_pPlayer2Pos[1] = m_Country.GetSizeY() * m_Country.GetStepY() / 2;

    m_Simulation1.Init(&m_Country);

    // Read water sources from cfg
    ParamClassPtr sourcesClass = GCLParam._cfgFile.GetClass("Sources");
    if (sourcesClass.IsNull())
    {
      LogF("Entry %s, not found in cfgFile.","Sources");
      LogF("Cannot load country.");      
      return;
    }

    ParamEntryPtr sourcesData = sourcesClass->FindEntry("data");
    if (sourcesData.IsNull())
    {
      LogF("Entry %s, not found in cfgFile.","data");
      LogF("Cannot load country.");           
      return ;
    }

    if (!sourcesData->IsArray())
    {
      LogF("Wrong entry %s in cfgFile.","data");
      LogF("Cannot load country.");     
      return ; // must be array
    }

    int n = sourcesData->GetSize() ;   
    for(int i = 0; i < n; i++)
    {
      if (!(*sourcesData)[i].IsArrayValue())
      {
        LogF("Wrong entry %s in cfgFile.","data");
        continue;
      }

      const IParamArrayValue& cur = (*sourcesData)[i];

      if (cur.GetItemCount() != 3)
      {
        LogF("Wrong entry %s in cfgFile.","data");
        continue;
      }

      SOURCEPOINT SP;
      SP.x = toInt((float) *cur.GetItem(0) / m_Country.GetStepX()) - m_Country.GetOriginX();
      SP.y = toInt((float) *cur.GetItem(1) / m_Country.GetStepY()) - m_Country.GetOriginY();
      SP.fFlow = (float) *cur.GetItem(2);
      m_Sources.Add(SP);      
    }

    /*SOURCEPOINT SP;
    SP.x = 20;
    SP.y = 73;
    SP.fFlow = 1.0f;
    m_Sources.Add(SP);

    SP.x = 231;
    SP.y = 117;
    SP.fFlow = 0.5f;
    m_Sources.Add(SP);*/
    m_Simulation1.SetSources(&m_Sources);


    

    //MyArray<unsigned int> cZone;        
    //m_Simulation1.EquidistantTeselation(7);    
    //m_Simulation1.MoveTeselationToPoint(m_Country.GetStepX() * m_Country.GetSizeX(), 0, MAP_SIZE_POWER - 1);
            
    /*cZone.Release();
    
    cZone.Add(0);
    cZone.Add(0);
    
    m_Simulation1.Teselate(cZone);

    cZone.Release();
    
    cZone.Add(0);
    cZone.Add(1);
    
    m_Simulation1.Teselate(cZone);

    cZone.Release();
    
    cZone.Add(0);
    cZone.Add(2);
    
    m_Simulation1.Teselate(cZone);

    cZone.Release();
    
    cZone.Add(0);
    cZone.Add(3);
    
    m_Simulation1.Teselate(cZone);
    
    cZone.Release();
    
    cZone.Add(1);
    cZone.Add(0);
    
    m_Simulation1.Teselate(cZone);

    cZone.Release();
    
    cZone.Add(1);
    cZone.Add(1);
    
    m_Simulation1.Teselate(cZone);

    cZone.Release();
    
    cZone.Add(1);
    cZone.Add(2);
    
    m_Simulation1.Teselate(cZone);

    cZone.Release();
    
    cZone.Add(1);
    cZone.Add(3);
    
    m_Simulation1.Teselate(cZone);

    cZone.Release();
    
    cZone.Add(2);
    cZone.Add(0);
    
    m_Simulation1.Teselate(cZone);

    cZone.Release();
    
    cZone.Add(2);
    cZone.Add(1);
    
    m_Simulation1.Teselate(cZone);

    cZone.Release();
    
    cZone.Add(2);
    cZone.Add(2);
    
    m_Simulation1.Teselate(cZone);

    cZone.Release();
    
    cZone.Add(2);
    cZone.Add(3);
    
    m_Simulation1.Teselate(cZone);

    cZone.Release();
    
    cZone.Add(3);
    cZone.Add(0);
    
    m_Simulation1.Teselate(cZone);

    cZone.Release();
    
    cZone.Add(3);
    cZone.Add(1);
    
    m_Simulation1.Teselate(cZone);

    cZone.Release();
    
    cZone.Add(3);
    cZone.Add(2);
    
    m_Simulation1.Teselate(cZone);

    cZone.Release();
    
    cZone.Add(3);
    cZone.Add(3);
    
    m_Simulation1.Teselate(cZone);
 */   

    // Fehy03
 
 /*   cZone.Add(2);
    cZone.Add(0);
    cZone.Add(1);

    m_Simulation1.Teselate(cZone);

    cZone.Release();

    cZone.Add(2);
    cZone.Add(3);
    cZone.Add(2);

    m_Simulation1.Teselate(cZone);

    cZone.Release();

    cZone.Add(2);
    cZone.Add(3);
    cZone.Add(1);

    m_Simulation1.Teselate(cZone);

    cZone.Release();

    cZone.Add(2);
    cZone.Add(0);
    cZone.Add(2);

    m_Simulation1.Teselate(cZone);

    cZone.Release();
    
    cZone.Add(1);
    cZone.Add(0);
    cZone.Add(1);

    m_Simulation1.Teselate(cZone);

    cZone.Release();
    
    cZone.Add(1);
    cZone.Add(0);
    cZone.Add(2);

    m_Simulation1.Teselate(cZone);

    cZone.Release();
    
    cZone.Add(1);
    cZone.Add(3);
    cZone.Add(1);

    m_Simulation1.Teselate(cZone);

    cZone.Release();
    
    cZone.Add(1);
    cZone.Add(3);
    cZone.Add(2);

    m_Simulation1.Teselate(cZone);
*/
    // Feh64x64
    
  /*
    cZone.Add(0); 
    cZone.Add(2);
    cZone.Add(2);
    //cZone.Add(2);
    //cZone.Add(2);
    m_Simulation1.Teselate(cZone);
      
    cZone.Release();

    cZone.Add(1);
    cZone.Add(3);
    cZone.Add(3);
    //cZone.Add(3);
    //cZone.Add(3);
    m_Simulation1.Teselate(cZone);
    cZone.Release();
    

    cZone.Add(3); 
    cZone.Add(1);
    cZone.Add(1);
    //cZone.Add(1);
    //cZone.Add(1);
    m_Simulation1.Teselate(cZone);
      
    cZone.Release();

    cZone.Add(2);
    cZone.Add(0);
    cZone.Add(0);
    //cZone.Add(0);
    //cZone.Add(0);    
    m_Simulation1.Teselate(cZone);

    cZone.Release();
*/
   /* cZone.Add(0); 
    //cZone.Add(2);
    //cZone.Add(2);
    m_Simulation1.DeTeselate(cZone);

    cZone.Release();

    cZone.Add(1); 
    cZone.Add(3);
    //cZone.Add(3);
    m_Simulation1.DeTeselate(cZone);

    cZone.Release();

    cZone.Add(1); 
    cZone.Add(3);
    //cZone.Add(3);
    m_Simulation1.DeTeselate(cZone);

    cZone.Release();

    cZone.Add(1); 
    //cZone.Add(3);
    //cZone.Add(3);
    m_Simulation1.DeTeselate(cZone);

    cZone.Release();

    cZone.Add(2); 
    cZone.Add(0);
    cZone.Add(0);
    m_Simulation1.DeTeselate(cZone);

    cZone.Release();

    cZone.Add(2); 
    cZone.Add(0);
    //cZone.Add(0);
    m_Simulation1.DeTeselate(cZone);

    cZone.Release();

    cZone.Add(2); 
    //cZone.Add(0);
    //cZone.Add(0);
    m_Simulation1.DeTeselate(cZone);
*/
   // m_Simulation1.MoveTeselationToPoint(m_pPlayer1Pos[0],m_pPlayer1Pos[1]);
  //  m_Simulation1.MoveTeselationToPoint(-m_pPlayer1Pos[0],m_pPlayer1Pos[1]);
  //  m_Simulation1.MoveTeselationToPoint(m_pPlayer1Pos[0],m_pPlayer1Pos[1]);


 /*
   cZone.Release();

   cZone.Add(3); 
    //cZone.Add(2);
    //cZone.Add(2);
    m_Simulation1.DeTeselate(cZone);
*/
    // Desynchronizace
 /*   cZone.Add(0); 
    cZone.Add(0);
    cZone.Add(0);
    m_Simulation1.Teselate(cZone);
    */
    //cZone.Add(0);
  /*  cZone.Add(2);
    m_Simulation1.Teselate(cZone);
    cZone.Release();

    cZone.Add(1);
    m_Simulation1.Teselate(cZone);
*/
    

  //  m_Simulation1.SetSources(&m_Sources);
#ifdef RUN_SECOND_SIMUL
    m_Simulation2.Init(&m_Country);

    m_Simulation2.EquidistantTeselation(2);

  /*  cZone.Release();
       
    cZone.Add(0); 
    cZone.Add(2);
    cZone.Add(2);
    cZone.Add(2);
    cZone.Add(2);
    m_Simulation2.Teselate(cZone);
      
    cZone.Release();

    cZone.Add(1);
    cZone.Add(3);
    cZone.Add(3);
    cZone.Add(3);
    cZone.Add(3);
    m_Simulation2.Teselate(cZone);
    cZone.Release();
    

    cZone.Add(3); 
    cZone.Add(1);
    cZone.Add(1);
    cZone.Add(1);
    cZone.Add(1);
    m_Simulation2.Teselate(cZone);
      
    cZone.Release();

    cZone.Add(2);
    cZone.Add(0);
    cZone.Add(0);
    cZone.Add(0);
    cZone.Add(0);
    
    m_Simulation2.Teselate(cZone);
*/


/* // fehy03
    cZone.Release();

     cZone.Add(2);
    cZone.Add(0);
    cZone.Add(1);

    m_Simulation2.Teselate(cZone);

    cZone.Release();

    cZone.Add(2);
    cZone.Add(3);
    cZone.Add(2);

    m_Simulation2.Teselate(cZone);

    cZone.Release();

    cZone.Add(2);
    cZone.Add(3);
    cZone.Add(1);

    m_Simulation2.Teselate(cZone);

    cZone.Release();

    cZone.Add(2);
    cZone.Add(0);
    cZone.Add(2);

    m_Simulation2.Teselate(cZone);

    cZone.Release();
    
    cZone.Add(1);
    cZone.Add(0);
    cZone.Add(1);

    m_Simulation2.Teselate(cZone);

    cZone.Release();
    
    cZone.Add(1);
    cZone.Add(0);
    cZone.Add(2);

    m_Simulation2.Teselate(cZone);

    cZone.Release();
    
    cZone.Add(1);
    cZone.Add(3);
    cZone.Add(1);

    m_Simulation2.Teselate(cZone);

    cZone.Release();
    
    cZone.Add(1);
    cZone.Add(3);
    cZone.Add(2);

    m_Simulation2.Teselate(cZone);
    */
    //m_Simulation2.MoveTeselationToPoint(m_pPlayer2Pos[0],m_pPlayer2Pos[1]);
    
#endif
    // Read settings from registry
    ReadSettings();
}




//-----------------------------------------------------------------------------
// Name: OneTimeSceneInit()
// Desc: Called during initial app startup, this function performs all the
//       permanent initialization.
//-----------------------------------------------------------------------------
HRESULT CMyD3DApplication::OneTimeSceneInit()
{
    // TODO: perform one time initialization

    // Drawing loading status message until app finishes loading
    SendMessage( m_hWnd, WM_PAINT, 0, 0 );

    // Initialize DirectInput
    InitInput( m_hWnd );

    m_bLoadingApp = FALSE;

    return S_OK;
}




//-----------------------------------------------------------------------------
// Name: ReadSettings()
// Desc: Read the app settings from the registry
//-----------------------------------------------------------------------------
VOID CMyD3DApplication::ReadSettings()
{
    HKEY hkey;
    if( ERROR_SUCCESS == RegCreateKeyEx( HKEY_CURRENT_USER, DXAPP_KEY, 
        0, NULL, REG_OPTION_NON_VOLATILE, KEY_ALL_ACCESS, NULL, &hkey, NULL ) )
    {
        // TODO: change as needed

        // Read the stored window width/height.  This is just an example,
        // of how to use DXUtil_Read*() functions.
        DXUtil_ReadIntRegKey( hkey, TEXT("Width"), &m_dwCreationWidth, m_dwCreationWidth );
        DXUtil_ReadIntRegKey( hkey, TEXT("Height"), &m_dwCreationHeight, m_dwCreationHeight );

        RegCloseKey( hkey );
    }
}




//-----------------------------------------------------------------------------
// Name: WriteSettings()
// Desc: Write the app settings to the registry
//-----------------------------------------------------------------------------
VOID CMyD3DApplication::WriteSettings()
{
    HKEY hkey;
    DWORD dwType = REG_DWORD;
    DWORD dwLength = sizeof(DWORD);

    if( ERROR_SUCCESS == RegCreateKeyEx( HKEY_CURRENT_USER, DXAPP_KEY, 
        0, NULL, REG_OPTION_NON_VOLATILE, KEY_ALL_ACCESS, NULL, &hkey, NULL ) )
    {
        // TODO: change as needed

        // Write the window width/height.  This is just an example,
        // of how to use DXUtil_Write*() functions.
        DXUtil_WriteIntRegKey( hkey, TEXT("Width"), m_rcWindowClient.right );
        DXUtil_WriteIntRegKey( hkey, TEXT("Height"), m_rcWindowClient.bottom );

        RegCloseKey( hkey );
    }
}





//-----------------------------------------------------------------------------
// Name: InitInput()
// Desc: Initialize DirectInput objects
//-----------------------------------------------------------------------------
HRESULT CMyD3DApplication::InitInput( HWND hWnd )
{
    HRESULT hr;

    // Create a IDirectInput8*
    if( FAILED( hr = DirectInput8Create( GetModuleHandle(NULL), DIRECTINPUT_VERSION, 
                                         IID_IDirectInput8, (VOID**)&m_pDI, NULL ) ) )
        return DXTRACE_ERR_NOMSGBOX( "DirectInput8Create", hr );
    
    // Create a IDirectInputDevice8* for the keyboard
    if( FAILED( hr = m_pDI->CreateDevice( GUID_SysKeyboard, &m_pKeyboard, NULL ) ) )
        return DXTRACE_ERR_NOMSGBOX( "CreateDevice", hr );
    
    // Set the keyboard data format
    if( FAILED( hr = m_pKeyboard->SetDataFormat( &c_dfDIKeyboard ) ) )
        return DXTRACE_ERR_NOMSGBOX( "SetDataFormat", hr );
    
    // Set the cooperative level on the keyboard
    if( FAILED( hr = m_pKeyboard->SetCooperativeLevel( hWnd, 
                                            DISCL_NONEXCLUSIVE | 
                                            DISCL_FOREGROUND | 
                                            DISCL_NOWINKEY ) ) )
        return DXTRACE_ERR_NOMSGBOX( "SetCooperativeLevel", hr );

    // Acquire the keyboard
    m_pKeyboard->Acquire();

    return S_OK;
}




//-----------------------------------------------------------------------------
// Name: ConfirmDevice()
// Desc: Called during device initialization, this code checks the display device
//       for some minimum set of capabilities
//-----------------------------------------------------------------------------
HRESULT CMyD3DApplication::ConfirmDevice( D3DCAPS8* pCaps, DWORD dwBehavior,
                                          D3DFORMAT Format )
{
    BOOL bCapsAcceptable;

    // TODO: Perform checks to see if these display caps are acceptable.
    bCapsAcceptable = TRUE;

    if( bCapsAcceptable )         
        return S_OK;
    else
        return E_FAIL;
}




//-----------------------------------------------------------------------------
// Name: InitDeviceObjects()
// Desc: Initialize scene objects.
//-----------------------------------------------------------------------------
HRESULT CMyD3DApplication::InitDeviceObjects()
{
    HRESULT hr;
    int a = 2;

    // Create the line vertex buffer
    m_pLineVB1->Init(m_pd3dDevice);
    /*
    if(FAILED(hr = m_pd3dDevice->CreateVertexBuffer(
      6 * MAP_SIZE * MAP_SIZE * sizeof(CUSTOMVERTEX),
      0,
      D3DFVF_CUSTOMVERTEX,
      D3DPOOL_MANAGED,
      &m_pLineVB1))) return DXTRACE_ERR_NOMSGBOX("CreateVertexBuffer", hr);
      */
#ifdef RUN_SECOND_SIMUL
    m_pLineVB2->Init(m_pd3dDevice);
    /*
    if(FAILED(hr = m_pd3dDevice->CreateVertexBuffer(
      6 * MAP_SIZE * MAP_SIZE  * sizeof(CUSTOMVERTEX),
      0,
      D3DFVF_CUSTOMVERTEX,
      D3DPOOL_MANAGED,
      &m_pLineVB2))) return DXTRACE_ERR_NOMSGBOX("CreateVertexBuffer", hr);
      */
#endif

    // Create hierarchy visualization line buffer
    m_pHierarchyVB1->Init(m_pd3dDevice);

#ifdef RUN_SECOND_SIMUL
    m_pHierarchyVB2->Init(m_pd3dDevice);
#endif

    // Create the country index buffer
  /*  if(FAILED(hr = m_pd3dDevice->CreateIndexBuffer(
      MAP_SIZE * MAP_SIZE * sizeof(WORD) * 6,
      D3DUSAGE_WRITEONLY,
      D3DFMT_INDEX16,
      D3DPOOL_MANAGED,
      &m_pCIB))) return DXTRACE_ERR_NOMSGBOX("CreateIndexBuffer", hr);

    // Fill index buffer with proper values
    WORD *pData;
    if(FAILED(hr = m_pCIB->Lock(0, 0, (BYTE**)&pData, 0))) return DXTRACE_ERR_NOMSGBOX("Lock", hr);
    int Curr = 0;
    for (int j = 0; j < MAP_SIZE - 1; j++) {
      for (int i = 0; i < MAP_SIZE - 1; i++) {
        // First triangle in square
        pData[Curr++] = (j * MAP_SIZE + i) ;
        pData[Curr++] = ((j + 1) * MAP_SIZE + i);
        pData[Curr++] = (j * MAP_SIZE + i + 1);
        // Second triangle in square
        pData[Curr++] = (j * MAP_SIZE + i + 1);
        pData[Curr++] = ((j + 1) * MAP_SIZE + i);
        pData[Curr++] = ((j + 1) * MAP_SIZE + i + 1);
      }
    }
    m_pCIB->Unlock();
    */

    // Create the country vertex buffer
    m_pCVB->Init(m_pd3dDevice);
    /*if(FAILED(hr = m_pd3dDevice->CreateVertexBuffer(
      MAP_SIZE * MAP_SIZE * sizeof(CUSTOMVERTEX),
      0,
      D3DFVF_CUSTOMVERTEX,
      D3DPOOL_MANAGED,
      &m_pCVB))) return DXTRACE_ERR_NOMSGBOX("CreateVertexBuffer", hr);
*/
    // Fill the vertex buffer with height data
    CreateCountryVertexes();
    

    // Create the water vertex buffer
    m_pWVB1->Init(m_pd3dDevice);
    m_pWVB2_1->Init(m_pd3dDevice);

    /*if(FAILED(hr = m_pd3dDevice->CreateVertexBuffer(
      (MAP_SIZE - 1) * (MAP_SIZE - 1) * sizeof(CUSTOMVERTEX) * 6,
      0,
      D3DFVF_CUSTOMVERTEX,
      D3DPOOL_MANAGED,
      &m_pWVB1))) return DXTRACE_ERR_NOMSGBOX("CreateVertexBuffer", hr);
      */
#ifdef RUN_SECOND_SIMUL
    // Create the water vertex buffer
    m_pWVB2->Init(m_pd3dDevice);
    m_pWVB2_2->Init(m_pd3dDevice);

    /*if(FAILED(hr = m_pd3dDevice->CreateVertexBuffer(
      (MAP_SIZE - 1) * (MAP_SIZE - 1) * sizeof(CUSTOMVERTEX) * 6,
      0,
      D3DFVF_CUSTOMVERTEX,
      D3DPOOL_MANAGED,
      &m_pWVB2))) return DXTRACE_ERR_NOMSGBOX("CreateVertexBuffer", hr);
      */
#endif 

    return S_OK;
}

HRESULT CMyD3DApplication::CreateCountryVertexes()
{
    HRESULT hr;
    
    //CUSTOMVERTEX* pVertices;
    //if(FAILED(hr = m_pCVB->Lock(0, 0, (BYTE**)&pVertices, 0))) return DXTRACE_ERR_NOMSGBOX("Lock", hr);

    RGBQUAD tType1, tType2, tGround;
    tType1.rgbRed = 255;
    tType1.rgbGreen = 0;
    tType1.rgbBlue = 0;

    tType2.rgbRed = 255;
    tType2.rgbGreen = 255;
    tType2.rgbBlue = 0;

    tGround.rgbRed = 0;
    tGround.rgbGreen = 255;
    tGround.rgbBlue = 0;

    m_pCVB->Reset();

    SFlyVertex tVertex1;
    SFlyVertex tVertex2;
    SFlyVertex tVertex3;

    const MyArray2DInt& cType1 = m_Simulation1.GetType();

#ifdef RUN_SECOND_SIMUL
    const MyArray2DInt& cType2 = m_Simulation2.GetType();
#endif



    for (int j = 0; j < DRAW_SIZE - 1; j++) {
      for (int i = 0; i < DRAW_SIZE - 1; i++) {
        //float Height = (float)rand() / (float)RAND_MAX;

        float Height = m_DrawCountry.GetItem(i, j);
       //tVertex1._position = g_cCoordConverter.FromSimCoordOZToDrawCoord((i + 0.5f) * m_Country.GetStepX(),(j + 0.5f) * m_Country.GetStepY(),Height);       
        tVertex1._position = g_cCoordConverter.FromSimCoordOZToDrawCoord(i,j,Height);       

        float fColorCoef = (Height / m_Country.GetMaxHeight() * 0.8f + 0.2f);
        
#ifndef RUN_SECOND_SIMUL
        if (cType1.GetItem(i,j) == -1 )
            tVertex1._diffuse = D3DCOLOR_XRGB((BYTE)(fColorCoef * tType1.rgbRed), (BYTE)(fColorCoef * tType1.rgbGreen), (BYTE)(fColorCoef * tType1.rgbBlue));
        else
            tVertex1._diffuse = D3DCOLOR_XRGB((BYTE)(fColorCoef * tGround.rgbRed), (BYTE)(fColorCoef * tGround.rgbGreen), (BYTE)(fColorCoef * tGround.rgbBlue));
            
#else
        if (cType1.GetItem(i,j) == -1 )
        {
            if (m_Simulation2.GetType().GetItem(i,j) == -1 )            
                tVertex1._diffuse = D3DCOLOR_XRGB((BYTE)(fColorCoef * (tType1.rgbRed + tType2.rgbRed) / 2.0f), (BYTE)(fColorCoef * (tType1.rgbGreen + tType2.rgbGreen) / 2.0f), (BYTE)(fColorCoef * (tType1.rgbBlue + tType2.rgbBlue) / 2.0f));            
            else
                tVertex1._diffuse = D3DCOLOR_XRGB((BYTE)(fColorCoef * tType1.rgbRed), (BYTE)(fColorCoef * tType1.rgbGreen), (BYTE)(fColorCoef * tType1.rgbBlue));
        }
        else
        {
            if (m_Simulation2.GetType().GetItem(i,j) == -1 )            
                tVertex1._diffuse = D3DCOLOR_XRGB((BYTE)(fColorCoef * tType2.rgbRed), (BYTE)(fColorCoef * tType2.rgbGreen), (BYTE)(fColorCoef * tType2.rgbBlue));            
            else
                tVertex1._diffuse = D3DCOLOR_XRGB((BYTE)(fColorCoef * tGround.rgbRed), (BYTE)(fColorCoef * tGround.rgbGreen), (BYTE)(fColorCoef * tGround.rgbBlue));
        }
#endif
        m_pCVB->AddVertex(tVertex1);

        Height = m_DrawCountry.GetItem(i ,j + 1);
        //tVertex2._position = g_cCoordConverter.FromSimCoordOZToDrawCoord((i + 0.5f) * m_Country.GetStepX(), (j + 1.5f) * m_Country.GetStepY(),Height);        
        tVertex2._position = g_cCoordConverter.FromSimCoordOZToDrawCoord(i,j+1,Height);

        fColorCoef = (Height / m_Country.GetMaxHeight() * 0.8f + 0.2f);
        
#ifndef RUN_SECOND_SIMUL
        if (cType1.GetItem(i,j) == -1 )
            tVertex2._diffuse = D3DCOLOR_XRGB((BYTE)(fColorCoef * tType1.rgbRed), (BYTE)(fColorCoef * tType1.rgbGreen), (BYTE)(fColorCoef * tType1.rgbBlue));
        else
            tVertex2._diffuse = D3DCOLOR_XRGB((BYTE)(fColorCoef * tGround.rgbRed), (BYTE)(fColorCoef * tGround.rgbGreen), (BYTE)(fColorCoef * tGround.rgbBlue));
            
#else
        if (cType1.GetItem(i,j) == -1 )
        {
            if (m_Simulation2.GetType().GetItem(i,j) == -1 )            
                tVertex2._diffuse = D3DCOLOR_XRGB((BYTE)(fColorCoef * (tType1.rgbRed + tType2.rgbRed) / 2.0f), (BYTE)(fColorCoef * (tType1.rgbGreen + tType2.rgbGreen) / 2.0f), (BYTE)(fColorCoef * (tType1.rgbBlue + tType2.rgbBlue) / 2.0f));            
            else
                tVertex2._diffuse = D3DCOLOR_XRGB((BYTE)(fColorCoef * tType1.rgbRed), (BYTE)(fColorCoef * tType1.rgbGreen), (BYTE)(fColorCoef * tType1.rgbBlue));
        }
        else
        {
            if (m_Simulation2.GetType().GetItem(i,j) == -1 )            
                tVertex2._diffuse = D3DCOLOR_XRGB((BYTE)(fColorCoef * tType2.rgbRed), (BYTE)(fColorCoef * tType2.rgbGreen), (BYTE)(fColorCoef * tType2.rgbBlue));            
            else
                tVertex2._diffuse = D3DCOLOR_XRGB((BYTE)(fColorCoef * tGround.rgbRed), (BYTE)(fColorCoef * tGround.rgbGreen), (BYTE)(fColorCoef * tGround.rgbBlue));
        }
#endif
        m_pCVB->AddVertex(tVertex2);

        Height = m_DrawCountry.GetItem(i + 1,j);
        //tVertex3._position = g_cCoordConverter.FromSimCoordOZToDrawCoord((i + 1.5f) * m_Country.GetStepX(), (j + 0.5f) * m_Country.GetStepY(),Height);       
        tVertex3._position = g_cCoordConverter.FromSimCoordOZToDrawCoord(i + 1,j,Height);       

        fColorCoef = (Height / m_Country.GetMaxHeight() * 0.8f + 0.2f);
        
#ifndef RUN_SECOND_SIMUL
        if (cType1.GetItem(i,j) == -1 )
            tVertex3._diffuse = D3DCOLOR_XRGB((BYTE)(fColorCoef * tType1.rgbRed), (BYTE)(fColorCoef * tType1.rgbGreen), (BYTE)(fColorCoef * tType1.rgbBlue));
        else
            tVertex3._diffuse = D3DCOLOR_XRGB((BYTE)(fColorCoef * tGround.rgbRed), (BYTE)(fColorCoef * tGround.rgbGreen), (BYTE)(fColorCoef * tGround.rgbBlue));
            
#else
        if (cType1.GetItem(i,j) == -1 )
        {
            if (m_Simulation2.GetType().GetItem(i,j) == -1 )            
                tVertex3._diffuse = D3DCOLOR_XRGB((BYTE)(fColorCoef * (tType1.rgbRed + tType2.rgbRed) / 2.0f), (BYTE)(fColorCoef * (tType1.rgbGreen + tType2.rgbGreen) / 2.0f), (BYTE)(fColorCoef * (tType1.rgbBlue + tType2.rgbBlue) / 2.0f));            
            else
                tVertex3._diffuse = D3DCOLOR_XRGB((BYTE)(fColorCoef * tType1.rgbRed), (BYTE)(fColorCoef * tType1.rgbGreen), (BYTE)(fColorCoef * tType1.rgbBlue));
        }
        else
        {
            if (m_Simulation2.GetType().GetItem(i,j) == -1 )            
                tVertex3._diffuse = D3DCOLOR_XRGB((BYTE)(fColorCoef * tType2.rgbRed), (BYTE)(fColorCoef * tType2.rgbGreen), (BYTE)(fColorCoef * tType2.rgbBlue));            
            else
                tVertex3._diffuse = D3DCOLOR_XRGB((BYTE)(fColorCoef * tGround.rgbRed), (BYTE)(fColorCoef * tGround.rgbGreen), (BYTE)(fColorCoef * tGround.rgbBlue));
        }
#endif
        m_pCVB->AddVertex(tVertex3);

        // Second triangle
        m_pCVB->AddVertex(tVertex3);
        m_pCVB->AddVertex(tVertex2);


        Height = m_DrawCountry.GetItem(i + 1,j + 1);
        //tVertex1._position = g_cCoordConverter.FromSimCoordOZToDrawCoord((i + 1.5f) * m_Country.GetStepX(), (j + 1.5f) * m_Country.GetStepY(),Height);        
        tVertex1._position = g_cCoordConverter.FromSimCoordOZToDrawCoord(i + 1, j + 1,Height);        

        fColorCoef = (Height / m_Country.GetMaxHeight() * 0.8f + 0.2f);
        
#ifndef RUN_SECOND_SIMUL
        if (cType1.GetItem(i,j) == -1 )
            tVertex1._diffuse = D3DCOLOR_XRGB((BYTE)(fColorCoef * tType1.rgbRed), (BYTE)(fColorCoef * tType1.rgbGreen), (BYTE)(fColorCoef * tType1.rgbBlue));
        else
            tVertex1._diffuse = D3DCOLOR_XRGB((BYTE)(fColorCoef * tGround.rgbRed), (BYTE)(fColorCoef * tGround.rgbGreen), (BYTE)(fColorCoef * tGround.rgbBlue));
            
#else
        if (cType1.GetItem(i,j) == -1 )
        {
            if (m_Simulation2.GetType().GetItem(i,j) == -1 )            
                tVertex1._diffuse = D3DCOLOR_XRGB((BYTE)(fColorCoef * (tType1.rgbRed + tType2.rgbRed) / 2.0f), (BYTE)(fColorCoef * (tType1.rgbGreen + tType2.rgbGreen) / 2.0f), (BYTE)(fColorCoef * (tType1.rgbBlue + tType2.rgbBlue) / 2.0f));            
            else
                tVertex1._diffuse = D3DCOLOR_XRGB((BYTE)(fColorCoef * tType1.rgbRed), (BYTE)(fColorCoef * tType1.rgbGreen), (BYTE)(fColorCoef * tType1.rgbBlue));
        }
        else
        {
            if (m_Simulation2.GetType().GetItem(i,j) == -1 )            
                tVertex1._diffuse = D3DCOLOR_XRGB((BYTE)(fColorCoef * tType2.rgbRed), (BYTE)(fColorCoef * tType2.rgbGreen), (BYTE)(fColorCoef * tType2.rgbBlue));            
            else
                tVertex1._diffuse = D3DCOLOR_XRGB((BYTE)(fColorCoef * tGround.rgbRed), (BYTE)(fColorCoef * tGround.rgbGreen), (BYTE)(fColorCoef * tGround.rgbBlue));
        }
#endif
        m_pCVB->AddVertex(tVertex1);
        

        //pVertices[j * MAP_SIZE + i].normal = D3DXVECTOR3(0.0f, 0.0f, -1.0f);
      }
    }
    
    return S_OK;
}



//-----------------------------------------------------------------------------
// Name: RestoreDeviceObjects()
// Desc: Restores scene objects.
//-----------------------------------------------------------------------------
HRESULT CMyD3DApplication::RestoreDeviceObjects()
{
    // TODO: setup render states
    HRESULT hr;

    // Setup a material
    D3DMATERIAL8 mtrl;
    D3DUtil_InitMaterial( mtrl, 1.0f, 0.0f, 0.0f );
    m_pd3dDevice->SetMaterial( &mtrl );

    // Set up the textures
    m_pd3dDevice->SetTextureStageState( 0, D3DTSS_COLOROP,   D3DTOP_MODULATE );
    m_pd3dDevice->SetTextureStageState( 0, D3DTSS_COLORARG1, D3DTA_TEXTURE );
    m_pd3dDevice->SetTextureStageState( 0, D3DTSS_COLORARG2, D3DTA_DIFFUSE );
    m_pd3dDevice->SetTextureStageState( 0, D3DTSS_MINFILTER, D3DTEXF_LINEAR );
    m_pd3dDevice->SetTextureStageState( 0, D3DTSS_MAGFILTER, D3DTEXF_LINEAR );

    // Set miscellaneous render states
    m_pd3dDevice->SetRenderState( D3DRS_DITHERENABLE,   FALSE );
    m_pd3dDevice->SetRenderState( D3DRS_SPECULARENABLE, FALSE );
    m_pd3dDevice->SetRenderState( D3DRS_ZENABLE,        TRUE );
    m_pd3dDevice->SetRenderState( D3DRS_AMBIENT,        0x000F0F0F );

    // Switching to wireframe
    //m_pd3dDevice->SetRenderState( D3DRS_FILLMODE, D3DFILL_WIREFRAME );

    // Set the world matrix
    D3DXMATRIX matIdentity;
    D3DXMatrixIdentity( &matIdentity );
    m_pd3dDevice->SetTransform( D3DTS_WORLD,  &matIdentity );

    // Set up our view matrix. A view matrix can be defined given an eye point,
    // a point to lookat, and a direction for which way is up. Here, we set the
    // eye five units back along the z-axis and up three units, look at the
    // origin, and define "up" to be in the y-direction.
    D3DXMATRIX matView;
    D3DXVECTOR3 vFromPt   = D3DXVECTOR3( 0.0f, 0.0f, +1500.0f );
    D3DXVECTOR3 vLookatPt = D3DXVECTOR3( 0.0f, 0.0f, 0.0f );
    D3DXVECTOR3 vUpVec    = D3DXVECTOR3( 0.0f, 1.0f, 0.0f );
    D3DXMatrixLookAtLH( &matView, &vFromPt, &vLookatPt, &vUpVec );
    m_pd3dDevice->SetTransform( D3DTS_VIEW, &matView );

    // Set the projection matrix
    D3DXMATRIX matProj;
    FLOAT fAspect = ((FLOAT)m_d3dsdBackBuffer.Width) / m_d3dsdBackBuffer.Height;
    D3DXMatrixPerspectiveFovLH( &matProj, D3DX_PI/4, fAspect, 1.0f, 100000.0f );
    m_pd3dDevice->SetTransform( D3DTS_PROJECTION, &matProj );

    // Set up lighting states
    D3DLIGHT8 light;
    D3DUtil_InitLight( light, D3DLIGHT_DIRECTIONAL, -1.0f, -1.0f, 2.0f );
    m_pd3dDevice->SetLight( 0, &light );
    m_pd3dDevice->LightEnable( 0, TRUE );
    //m_pd3dDevice->SetRenderState( D3DRS_LIGHTING, TRUE );
    m_pd3dDevice->SetRenderState( D3DRS_LIGHTING, FALSE );

    // Create a D3D font using D3DX
    HFONT hFont = CreateFont( 20, 0, 0, 0, FW_BOLD, FALSE, FALSE, FALSE,
                              ANSI_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS,
                              ANTIALIASED_QUALITY, FF_DONTCARE, "Arial" );      
    if( FAILED( hr = D3DXCreateFont( m_pd3dDevice, hFont, &m_pD3DXFont ) ) )
        return DXTRACE_ERR_NOMSGBOX( "D3DXCreateFont", hr );

    return S_OK;
}




//-----------------------------------------------------------------------------
// Name: FrameMove()
// Desc: Called once per frame, the call is the entry point for animating
//       the scene.
//-----------------------------------------------------------------------------
int nTimes = 0;
int nTimesAll = 0;

#define WATER_COLOR
const float MASS_RANGE = 350;

HRESULT CMyD3DApplication::FrameMove()
{

  HRESULT hr;

  // TODO: update world

  // Update user input state
  UpdateInput( &m_UserInput );

  BOOL bSimulate = FALSE;
  BOOL bReDraw = FALSE;

  CreateCountryVertexes();

  if (m_UserInput.bSaveWaterHeight || m_UserInput.bSaveWaterMass)
  {
    // save actual water status into png file.
    ParamClassPtr cfgClass = GCLParam._cfgFile.GetClass("InOut");
    if (cfgClass.IsNull())
    {
      LogF("Entry %s, not found in cfgFile.","InOut");
      LogF("Cannot save water.");      
    }
    else
    {
      ParamEntryPtr entry = cfgClass->FindEntry("water");
      if (entry.IsNull())
      {
        LogF("Entry %s, not found in cfgFile.","water");
        LogF("Cannot save water.");                 
      }
      else
      {      
        LPCSTR waterName = (LPCSTR)((RStringB)(*entry));
        LPSTR dir = SeparateDirFromFileName(waterName);
        waterName += strlen(dir);

        m_Simulation1.SaveWaterInPbl(dir,waterName, m_UserInput.bSaveWaterMass);
      }
    }
  }

  if (m_UserInput.bPlayer1XUp)
  {
    m_pPlayer1Pos[0] -= m_Country.GetStepX() * 5;
    m_Simulation1.MoveTeselationToPoint(m_pPlayer1Pos[0], m_pPlayer1Pos[1], MAP_SIZE_POWER);
    bReDraw = TRUE;        
  }

  if (m_UserInput.bPlayer1XDown)
  {
    m_pPlayer1Pos[0] += m_Country.GetStepX() * 5;;
    m_Simulation1.MoveTeselationToPoint(m_pPlayer1Pos[0], m_pPlayer1Pos[1], MAP_SIZE_POWER);
    bReDraw = TRUE;  
  }

  if (m_UserInput.bPlayer1YUp)
  {
    m_pPlayer1Pos[1] += m_Country.GetStepY() * 5;
    m_Simulation1.MoveTeselationToPoint(m_pPlayer1Pos[0], m_pPlayer1Pos[1], MAP_SIZE_POWER );
    bReDraw = TRUE;
  }

  if (m_UserInput.bPlayer1YDown)
  {
    m_pPlayer1Pos[1] -= m_Country.GetStepY() * 5;;
    m_Simulation1.MoveTeselationToPoint(m_pPlayer1Pos[0], m_pPlayer1Pos[1], MAP_SIZE_POWER);
    bReDraw = TRUE;
  }

  if (m_UserInput.bOffsetXUp)
  {
    if (g_cCoordConverter.GetOffsetX() < MAP_SIZE - 5)
    {
      g_cCoordConverter.SetOffsetX(g_cCoordConverter.GetOffsetX() + 5);
      bReDraw = TRUE;        
    }        
  }

  if (m_UserInput.bOffsetXDown)
  {
    if (g_cCoordConverter.GetOffsetX() > 5)
    {
      g_cCoordConverter.SetOffsetX(g_cCoordConverter.GetOffsetX() - 5);
      bReDraw = TRUE;        
    }        
  }

  if (m_UserInput.bOffsetYUp)
  {
    if (g_cCoordConverter.GetOffsetY() < MAP_SIZE - 5)
    {
      g_cCoordConverter.SetOffsetY(g_cCoordConverter.GetOffsetY() + 5);
      bReDraw = TRUE;        
    }
  }

  if (m_UserInput.bOffsetYDown)
  {
    if (g_cCoordConverter.GetOffsetY() > 5)
    {
      g_cCoordConverter.SetOffsetY(g_cCoordConverter.GetOffsetY() - 5);
      bReDraw = TRUE;        
    }      
  }

  if (m_UserInput.bBuldozer)
  { 
    m_Country.BlowUpDam();
    m_Simulation1.CountryChanged();
    bReDraw = TRUE;
  }

  if (m_UserInput.bZoomIn)
  {
    if (g_cCoordConverter.GetZoomLevel() < MAP_SIZE_POWER - DRAW_SIZE_POWER)
    {
      g_cCoordConverter.SetZoomLevel(g_cCoordConverter.GetZoomLevel() + 1);
    }

    bReDraw = TRUE;
  }

  if (m_UserInput.bZoomOut)
  {        
    if (g_cCoordConverter.GetZoomLevel() > 0)
    {
      g_cCoordConverter.SetZoomLevel(g_cCoordConverter.GetZoomLevel() - 1);
    }  

    bReDraw = TRUE;
  }


  nTimes++;
  nTimesAll++;

  // if (nTimes < 10000 || (nTimes % 100 == 0))
  {
    /*m_Sources.Release();

    if ( nTimesAll < 100000)
    {

    SOURCEPOINT SP;
    SP.x = 20;
    SP.y = 20;
    SP.fFlow = 20.0f + (1.0f * rand()) / RAND_MAX ;
    m_Sources.Add(SP);

    m_Simulation2.SetSources(&m_Sources);

    }
    */

    if (!m_UserInput.bSimulate)		
    { 
      bSimulate = TRUE;
    }
  }

  if (bSimulate)
  {                    
    m_Simulation1.Simulate();            
#ifdef RUN_SECOND_SIMUL

    m_Simulation2.Simulate();
#endif            
  }

  if (bSimulate || bReDraw)
  {     
    RGBQUAD tWater, tType;
    tWater.rgbRed = 0;
    tWater.rgbGreen = 0;
    tWater.rgbBlue = 255;

    tType.rgbRed = 255;
    tType.rgbGreen = 0;
    tType.rgbBlue = 0;

    if (!m_UserInput.bDrawByRect)
    {
      m_pWVB2_1->Reset();

      m_Simulation1.PrepareZoomedAndOffseted( DRAW_SIZE, DRAW_SIZE, 
        g_cCoordConverter.GetZoomLevel(), 
        g_cCoordConverter.GetOffsetX(), 
        g_cCoordConverter.GetOffsetY());

      CreateWaterVertexes(&m_Simulation1, *m_pWVB1, tWater, tType);
    }
    else  
    {
      m_pWVB1->Reset();

      CreateWaterVertexes2(&m_Simulation1, *m_pWVB2_1,tWater);
    }


    RGBQUAD tArrow;
    tArrow.rgbRed = 0;
    tArrow.rgbGreen = 0;
    tArrow.rgbBlue = 0;

    CreateArrowVertexes(&m_Simulation1, m_pLineVB1, tArrow);

#ifdef RUN_SECOND_SIMUL

    tWater.rgbRed = 0;
    tWater.rgbGreen = 255;
    tWater.rgbBlue = 255;

    tType.rgbRed = 255;
    tType.rgbGreen = 255;
    tType.rgbBlue = 0;

    /*m_Simulation2.PrepareZoomedAndOffseted( DRAW_SIZE, DRAW_SIZE, 
    g_cCoordConverter.GetZoomLevel(), 
    g_cCoordConverter.GetOffsetX(), 
    g_cCoordConverter.GetOffsetY());

    CreateWaterVertexes(&m_Simulation2, *m_pWVB2, tWater, tType);
    */

    CreateWaterVertexes2(&m_Simulation2, *m_pWVB2_2,tWater);

    tArrow.rgbRed = 255;
    tArrow.rgbGreen = 0;
    tArrow.rgbBlue = 0;
    CreateArrowVertexes(&m_Simulation2, m_pLineVB2, tArrow);
#endif
  }

  if (bReDraw)
  {
    m_Country.GetZoomedAndOffseted(m_DrawCountry, 
      g_cCoordConverter.GetZoomLevel(), 
      g_cCoordConverter.GetOffsetX(), 
      g_cCoordConverter.GetOffsetY());

    CreateCountryVertexes();

    RGBQUAD tLine;
    tLine.rgbRed = 150;
    tLine.rgbGreen = 150;
    tLine.rgbBlue = 0;
    CreateHierarchyVertexes(&m_Simulation1, m_pHierarchyVB1, tLine);

#ifdef RUN_SECOND_SIMUL

    tLine.rgbRed = 0;
    tLine.rgbGreen = 0;
    tLine.rgbBlue = 150;
    //CreateHierarchyVertexes(&m_Simulation2, m_pHierarchyVB2, tLine);

#endif

  }

  // Update the world state according to user input
  if (m_UserInput.bMoveForward && !m_UserInput.bMoveBackward) {
    m_CameraPosition.x += (float)sin(m_fCameraRotY) * m_fElapsedTime * 10;
    m_CameraPosition.z += (float)cos(m_fCameraRotY) * m_fElapsedTime * 10;
  }
  else if (m_UserInput.bMoveBackward && !m_UserInput.bMoveForward) {
    m_CameraPosition.x -= (float)sin(m_fCameraRotY) * m_fElapsedTime * 10;
    m_CameraPosition.z -= (float)cos(m_fCameraRotY) * m_fElapsedTime * 10;
  }

  if( m_UserInput.bRotateLeft && !m_UserInput.bRotateRight )
    m_fCameraRotY -= m_fElapsedTime;
  else if( m_UserInput.bRotateRight && !m_UserInput.bRotateLeft )
    m_fCameraRotY += m_fElapsedTime;

  if( m_UserInput.bRotateUp && !m_UserInput.bRotateDown )
    m_fCameraRotX -= m_fElapsedTime;
  else if( m_UserInput.bRotateDown && !m_UserInput.bRotateUp )
    m_fCameraRotX += m_fElapsedTime;

  if( m_UserInput.bMoveUp && !m_UserInput.bMoveDown )
    m_CameraPosition.y += m_fElapsedTime * 10;
  else if( m_UserInput.bMoveDown && !m_UserInput.bMoveUp )
    m_CameraPosition.y -= m_fElapsedTime * 10;


  D3DXMATRIX MatView;
  D3DXMATRIX MatCamera;
  D3DXMATRIX MatTransl;
  D3DXMATRIX MatRotY;
  D3DXMATRIX MatRotX;
  D3DXMatrixTranslation(&MatTransl, m_CameraPosition.x, m_CameraPosition.y + 1.5f, m_CameraPosition.z);
  D3DXMatrixRotationX(&MatRotX, m_fCameraRotX);
  D3DXMatrixRotationY(&MatRotY, m_fCameraRotY);
  MatCamera = MatRotX * MatRotY * MatTransl;
  D3DXMatrixInverse(&MatView, NULL, &MatCamera);


  m_pd3dDevice->SetTransform(D3DTS_VIEW, &MatView);

  return S_OK;
}

HRESULT CMyD3DApplication::CreateWaterVertexes(CArctic * pSimulation, CFlyShape& cWVB, RGBQUAD& tWater, RGBQUAD& tType)
{

  HRESULT hr;
  float fMassMax, fMassMin;
  pSimulation->GetMass().FindMaxMin(fMassMax,fMassMin);
  float fMassRange = max(1,fMassMax - fMassMin);


  // Fill the water vertex buffer with water patches
  /*    CUSTOMVERTEX* pVertices;
  if(FAILED(hr = pWVB->Lock(0, 0, (BYTE**)&pVertices, 0))) return DXTRACE_ERR_NOMSGBOX("Lock", hr);
  nWVCount = 0;
  */

  cWVB.Reset();
  SFlyVertex tVertex;

  const MyArray2DFloat& cMass = pSimulation->GetMass();
  const MyArray2DFloat& cWaterHeight = pSimulation->GetWaterHeight(); 
  const MyArray2DInt& cType = pSimulation->GetType(); 

  for (int j = 0; j < DRAW_SIZE - 1; j++) {
    for (int i = 0; i < DRAW_SIZE - 1; i++) {

      float fReplaceHeight = -1000;
      if (m_DrawCountry.GetItem(i, j)  + MIN_WATER_HEIGHT < cWaterHeight.GetItem(i, j))
        fReplaceHeight = cWaterHeight.GetItem(i, j);

      if (m_DrawCountry.GetItem(i, j + 1)  + MIN_WATER_HEIGHT < cWaterHeight.GetItem(i, j + 1))
        fReplaceHeight = cWaterHeight.GetItem(i, j + 1);

      if (m_DrawCountry.GetItem(i + 1, j)  + MIN_WATER_HEIGHT < cWaterHeight.GetItem(i + 1, j))
        fReplaceHeight = cWaterHeight.GetItem(i + 1, j);




      // First triangle of the square
      if (fReplaceHeight > -1000)
      {

        if ((m_DrawCountry.GetItem(i, j)  + MIN_WATER_HEIGHT< cWaterHeight.GetItem(i, j)))
          tVertex._position = g_cCoordConverter.FromSimCoordOZToDrawCoord(i, j, cWaterHeight.GetItem(i, j));
        //D3DXVECTOR3((float)(i - MAP_SIZE/2) * 0.2f, cWaterHeight.GetItem(i, j) / 100.0f, (float)j * 0.2f - 2.0f);
        else
          tVertex._position = g_cCoordConverter.FromSimCoordOZToDrawCoord(i, j, fReplaceHeight);
        //D3DXVECTOR3((float)(i - MAP_SIZE/2) * 0.2f, fReplaceHeight / 100.0f, (float)j * 0.2f - 2.0f);

        float fColorCoef = ((cMass.GetItem(i, j) - fMassMin) / fMassRange * 0.6f + 0.4f);
        if (cType.GetItem(i, j) == -1)
          tVertex._diffuse = D3DCOLOR_XRGB((BYTE)(fColorCoef * tType.rgbRed), (BYTE)(fColorCoef * tType.rgbGreen), (BYTE)(fColorCoef * tType.rgbBlue));
        else
          tVertex._diffuse = D3DCOLOR_XRGB((BYTE)(fColorCoef * tWater.rgbRed), (BYTE)(fColorCoef * tWater.rgbGreen), (BYTE)(fColorCoef * tWater.rgbBlue));

        cWVB.AddVertex(tVertex);


        if ((m_DrawCountry.GetItem(i, j + 1)  + MIN_WATER_HEIGHT< cWaterHeight.GetItem(i, j + 1)))
          tVertex._position = g_cCoordConverter.FromSimCoordOZToDrawCoord(i, j + 1, cWaterHeight.GetItem(i, j + 1));
        //D3DXVECTOR3((float)(i - MAP_SIZE/2) * 0.2f, cWaterHeight.GetItem(i, j + 1) / 100.0f, (float)(j + 1) * 0.2f - 2.0f);
        else
          tVertex._position = g_cCoordConverter.FromSimCoordOZToDrawCoord(i, j + 1, fReplaceHeight);
        //D3DXVECTOR3((float)(i - MAP_SIZE/2) * 0.2f, fReplaceHeight / 100.0f, (float)(j + 1) * 0.2f - 2.0f);

        fColorCoef = ((cMass.GetItem(i, j + 1) - fMassMin) / fMassRange * 0.6f + 0.4f);
        if (cType.GetItem(i, j + 1) == -1)
          tVertex._diffuse = D3DCOLOR_XRGB((BYTE)(fColorCoef * tType.rgbRed), (BYTE)(fColorCoef * tType.rgbGreen), (BYTE)(fColorCoef * tType.rgbBlue));
        else
          tVertex._diffuse = D3DCOLOR_XRGB((BYTE)(fColorCoef * tWater.rgbRed), (BYTE)(fColorCoef * tWater.rgbGreen), (BYTE)(fColorCoef * tWater.rgbBlue));


        cWVB.AddVertex(tVertex);

        if ((m_DrawCountry.GetItem(i + 1, j)  + MIN_WATER_HEIGHT< cWaterHeight.GetItem(i + 1, j)))
          tVertex._position = g_cCoordConverter.FromSimCoordOZToDrawCoord(i + 1, j, cWaterHeight.GetItem(i + 1, j));
        //D3DXVECTOR3((float)((i + 1) - MAP_SIZE/2) * 0.2f, cWaterHeight.GetItem(i + 1, j) / 100.0f, (float)j * 0.2f - 2.0f);
        else
          tVertex._position = g_cCoordConverter.FromSimCoordOZToDrawCoord(i + 1, j, fReplaceHeight); 
        //D3DXVECTOR3((float)((i + 1) - MAP_SIZE/2) * 0.2f, fReplaceHeight / 100.0f, (float)j * 0.2f - 2.0f);

        fColorCoef = ((cMass.GetItem(i + 1, j) - fMassMin) / fMassRange * 0.6f + 0.4f);
        if (cType.GetItem(i+1, j) == -1)
          tVertex._diffuse = D3DCOLOR_XRGB((BYTE)(fColorCoef * tType.rgbRed), (BYTE)(fColorCoef * tType.rgbGreen), (BYTE)(fColorCoef * tType.rgbBlue));
        else
          tVertex._diffuse = D3DCOLOR_XRGB((BYTE)(fColorCoef * tWater.rgbRed), (BYTE)(fColorCoef * tWater.rgbGreen), (BYTE)(fColorCoef * tWater.rgbBlue));

        cWVB.AddVertex(tVertex);
      }
      // Second triangle of the square

      fReplaceHeight = -1000;
      if (m_DrawCountry.GetItem(i + 1, j)  + MIN_WATER_HEIGHT < cWaterHeight.GetItem(i + 1, j))
        fReplaceHeight = cWaterHeight.GetItem(i + 1, j);

      if (m_DrawCountry.GetItem(i, j + 1)  + MIN_WATER_HEIGHT < cWaterHeight.GetItem(i, j + 1))
        fReplaceHeight = cWaterHeight.GetItem(i, j + 1);

      if (m_DrawCountry.GetItem(i + 1, j + 1)  + MIN_WATER_HEIGHT < cWaterHeight.GetItem(i + 1, j + 1))
        fReplaceHeight = cWaterHeight.GetItem(i + 1, j + 1);


      if (fReplaceHeight > -1000)
      {

        if ((m_DrawCountry.GetItem(i + 1, j)  + MIN_WATER_HEIGHT< cWaterHeight.GetItem(i + 1, j)))
          tVertex._position = g_cCoordConverter.FromSimCoordOZToDrawCoord(i + 1, j, cWaterHeight.GetItem(i + 1, j));
        //D3DXVECTOR3((float)((i + 1) - MAP_SIZE/2) * 0.2f, cWaterHeight.GetItem(i + 1, j) / 100.0f, (float)j * 0.2f - 2.0f);
        else
          tVertex._position = g_cCoordConverter.FromSimCoordOZToDrawCoord(i + 1, j, fReplaceHeight);
        //D3DXVECTOR3((float)((i + 1) - MAP_SIZE/2) * 0.2f, fReplaceHeight / 100.0f, (float)j * 0.2f - 2.0f);

        float fColorCoef = ((cMass.GetItem(i + 1, j) - fMassMin) / fMassRange * 0.6f + 0.4f);
        if (cType.GetItem(i+1, j) == -1)
          tVertex._diffuse = D3DCOLOR_XRGB((BYTE)(fColorCoef * tType.rgbRed), (BYTE)(fColorCoef * tType.rgbGreen), (BYTE)(fColorCoef * tType.rgbBlue));
        else
          tVertex._diffuse = D3DCOLOR_XRGB((BYTE)(fColorCoef * tWater.rgbRed), (BYTE)(fColorCoef * tWater.rgbGreen), (BYTE)(fColorCoef * tWater.rgbBlue));

        cWVB.AddVertex(tVertex);


        if ((m_DrawCountry.GetItem(i, j + 1)  + MIN_WATER_HEIGHT< cWaterHeight.GetItem(i, j + 1)))
          tVertex._position = g_cCoordConverter.FromSimCoordOZToDrawCoord(i, j + 1, cWaterHeight.GetItem(i, j + 1));
        //D3DXVECTOR3((float)(i - MAP_SIZE/2) * 0.2f, cWaterHeight.GetItem(i, j + 1) / 100.0f, (float)(j + 1) * 0.2f - 2.0f);
        else
          tVertex._position = g_cCoordConverter.FromSimCoordOZToDrawCoord(i, j + 1, fReplaceHeight);
        //D3DXVECTOR3((float)(i - MAP_SIZE/2) * 0.2f, fReplaceHeight / 100.0f, (float)(j + 1) * 0.2f - 2.0f);

        fColorCoef = ((cMass.GetItem(i, j + 1) - fMassMin) / fMassRange * 0.6f + 0.4f);
        if (cType.GetItem(i, j+1) == -1)
          tVertex._diffuse = D3DCOLOR_XRGB((BYTE)(fColorCoef * tType.rgbRed), (BYTE)(fColorCoef * tType.rgbGreen), (BYTE)(fColorCoef * tType.rgbBlue));
        else
          tVertex._diffuse = D3DCOLOR_XRGB((BYTE)(fColorCoef * tWater.rgbRed), (BYTE)(fColorCoef * tWater.rgbGreen), (BYTE)(fColorCoef * tWater.rgbBlue));

        cWVB.AddVertex(tVertex);


        if ((m_DrawCountry.GetItem(i + 1, j + 1)  + MIN_WATER_HEIGHT< cWaterHeight.GetItem(i + 1, j + 1)))
          tVertex._position = g_cCoordConverter.FromSimCoordOZToDrawCoord(i + 1, j + 1, cWaterHeight.GetItem(i + 1, j + 1));
        //D3DXVECTOR3((float)((i + 1) - MAP_SIZE/2) * 0.2f, cWaterHeight.GetItem(i + 1, j + 1) / 100.0f, (float)(j + 1) * 0.2f - 2.0f);
        else
          tVertex._position = g_cCoordConverter.FromSimCoordOZToDrawCoord(i + 1, j + 1, fReplaceHeight);
        //D3DXVECTOR3((float)((i + 1) - MAP_SIZE/2) * 0.2f, fReplaceHeight / 100.0f, (float)(j + 1) * 0.2f - 2.0f);

        fColorCoef = ((cMass.GetItem(i + 1, j + 1) - fMassMin) / fMassRange * 0.6f + 0.4f);
        if (cType.GetItem(i+1, j+1) == -1)
          tVertex._diffuse = D3DCOLOR_XRGB((BYTE)(fColorCoef * tType.rgbRed), (BYTE)(fColorCoef * tType.rgbGreen), (BYTE)(fColorCoef * tType.rgbBlue));
        else
          tVertex._diffuse = D3DCOLOR_XRGB((BYTE)(fColorCoef * tWater.rgbRed), (BYTE)(fColorCoef * tWater.rgbGreen), (BYTE)(fColorCoef * tWater.rgbBlue));

        cWVB.AddVertex(tVertex);
      }
    }
  }


  return S_OK;
}

HRESULT CMyD3DApplication::CreateWaterVertexes2(CArctic * pSimulation, CFlyShapeSimCoord& cWVB, const RGBQUAD& tColor)
{
    cWVB.Reset();

    pSimulation->CreateWaterVertexes(cWVB, DRAW_SIZE, DRAW_SIZE, 
        g_cCoordConverter.GetZoomLevel(),
        g_cCoordConverter.GetOffsetX(),
        g_cCoordConverter.GetOffsetY(),
        tColor);

    return S_OK;
}

void AddArrow(void * pInternalData, float fXFrom, float fYFrom, float fZFrom, 
                  float fXTo, float fYTo, float fZTo)
{
    TADDLINEDATA * ptData = (TADDLINEDATA *) pInternalData;

    SFlyVertex tVertex;

    ASSERT(fZFrom == fZTo);

    tVertex._diffuse = ptData->_nColor;

    tVertex._position = g_cCoordConverter.FromSimCoordToDrawCoord(fXFrom, fYFrom, fZFrom);
    ptData->_pVB->AddVertex(tVertex);

    tVertex._position = g_cCoordConverter.FromSimCoordToDrawCoord(fXTo, fYTo, fZTo);
    ptData->_pVB->AddVertex(tVertex);

    float fVecX = fXTo - fXFrom;
    float fVecY = fYTo - fYFrom;
    float fVecZ = fZTo - fZFrom;

    tVertex._position = g_cCoordConverter.FromSimCoordToDrawCoord(fXTo, fYTo, fZTo); 
    ptData->_pVB->AddVertex(tVertex);

    tVertex._position = g_cCoordConverter.FromSimCoordToDrawCoord(fXTo - fVecX * 0.1f + fVecY * 0.1f, fYTo - fVecX * 0.1f - fVecY * 0.1f, fZTo);
    ptData->_pVB->AddVertex(tVertex);

    tVertex._position = g_cCoordConverter.FromSimCoordToDrawCoord(fXTo, fYTo, fZTo); 
    ptData->_pVB->AddVertex(tVertex);

    tVertex._position = g_cCoordConverter.FromSimCoordToDrawCoord(fXTo - fVecX * 0.1f - fVecY * 0.1f, fYTo + fVecX * 0.1f - fVecY * 0.1f, fZTo);
    ptData->_pVB->AddVertex(tVertex);

}

void AddLine(void * pInternalData, float fXFrom, float fYFrom, float fZFrom, 
                  float fXTo, float fYTo, float fZTo)
{
    TADDLINEDATA * ptData = (TADDLINEDATA *) pInternalData;

    SFlyVertex tVertex;

    tVertex._diffuse = ptData->_nColor;

    tVertex._position = g_cCoordConverter.FromSimCoordToDrawCoord(fXFrom, fYFrom, fZFrom);
    ptData->_pVB->AddVertex(tVertex);

    tVertex._position = g_cCoordConverter.FromSimCoordToDrawCoord(fXTo, fYTo, fZTo);
    ptData->_pVB->AddVertex(tVertex);
}

HRESULT CMyD3DApplication::CreateArrowVertexes(CArctic * pSimulation, CFlyShape *pVB, RGBQUAD& tColor)
{
    TADDLINEDATA tData;
    
    HRESULT hr;

    pVB->Reset();  
    
    tData._pVB = pVB;
    tData._nColor = D3DCOLOR_XRGB(tColor.rgbRed, tColor.rgbGreen, tColor.rgbBlue);

    pSimulation->SetUpArrows(AddArrow, (void *) &tData);

    return S_OK;
}

HRESULT CMyD3DApplication::CreateHierarchyVertexes(CArctic * pSimulation, CFlyShape *pVB, RGBQUAD& tColor, BOOL bVerticalLines)
{
    TADDLINEDATA tData;
    
    HRESULT hr;

    pVB->Reset();  
    
    tData._pVB = pVB;
    tData._nColor = D3DCOLOR_ARGB(100, tColor.rgbRed, tColor.rgbGreen, tColor.rgbBlue);//D3DCOLOR_XRGB(tColor.rgbRed, tColor.rgbGreen, tColor.rgbBlue);

    pSimulation->SetUpBorderLines(AddLine, (void *) &tData, bVerticalLines);

    return S_OK;
}


//-----------------------------------------------------------------------------
// Name: UpdateInput()
// Desc: Update the user input.  Called once per frame 
//-----------------------------------------------------------------------------
void CMyD3DApplication::UpdateInput( UserInput* pUserInput )
{
  HRESULT hr;

  // Get the input's device state, and put the state in dims
  ZeroMemory( &pUserInput->diks, sizeof(pUserInput->diks) );
  hr = m_pKeyboard->GetDeviceState( sizeof(pUserInput->diks), &pUserInput->diks );
  if( FAILED(hr) ) 
  {
    m_pKeyboard->Acquire();
    return; 
  }

  // TODO: Process user input as needed
  pUserInput->bRotateLeft  = ( (pUserInput->diks[DIK_LEFT] & 0x80)  == 0x80 );
  pUserInput->bRotateRight = ( (pUserInput->diks[DIK_RIGHT] & 0x80) == 0x80 );
  pUserInput->bRotateUp    = ( (pUserInput->diks[DIK_Q] & 0x80)    == 0x80 );
  pUserInput->bRotateDown  = ( (pUserInput->diks[DIK_A] & 0x80)  == 0x80 );
  pUserInput->bMoveForward = ( (pUserInput->diks[DIK_UP] & 0x80)    == 0x80 );
  pUserInput->bMoveBackward= ( (pUserInput->diks[DIK_DOWN] & 0x80)  == 0x80 );
  pUserInput->bMoveUp =      ( (pUserInput->diks[DIK_PGUP] & 0x80)    == 0x80 );
  pUserInput->bMoveDown =    ( (pUserInput->diks[DIK_PGDN] & 0x80)    == 0x80 );
  pUserInput->bSimulate =    ( (pUserInput->diks[DIK_SPACE] & 0x80)    == 0x80 );
  pUserInput->bPlayer1XUp =  ( (pUserInput->diks[DIK_J] & 0x80)  == 0x80 );
  pUserInput->bPlayer1XDown =( (pUserInput->diks[DIK_L] & 0x80)    == 0x80 );
  pUserInput->bPlayer1YUp =  ( (pUserInput->diks[DIK_I] & 0x80)    == 0x80 );
  pUserInput->bPlayer1YDown =( (pUserInput->diks[DIK_K] & 0x80)    == 0x80 );
  pUserInput->bBuldozer =    ( (pUserInput->diks[DIK_B] & 0x80)    == 0x80 );
  pUserInput->bZoomOut =     ( (pUserInput->diks[DIK_Z] & 0x80)    == 0x80 ) && ( (pUserInput->diks[DIK_LSHIFT] & 0x80)    == 0x80 );
  pUserInput->bZoomIn =      ( (pUserInput->diks[DIK_Z] & 0x80)    == 0x80 ) && ( (pUserInput->diks[DIK_LSHIFT] & 0x80)    != 0x80 );
  pUserInput->bOffsetXUp =   ( (pUserInput->diks[DIK_6] & 0x80)  == 0x80 );
  pUserInput->bOffsetXDown = ( (pUserInput->diks[DIK_4] & 0x80)    == 0x80 );
  pUserInput->bOffsetYUp =   ( (pUserInput->diks[DIK_8] & 0x80)    == 0x80 );
  pUserInput->bOffsetYDown = ( (pUserInput->diks[DIK_2] & 0x80)    == 0x80 );
  pUserInput->bDrawByRect  = ( (pUserInput->diks[DIK_RSHIFT] & 0x80)    == 0x80 );
  pUserInput->bSaveWaterHeight =        ( (pUserInput->diks[DIK_S] & 0x80)  == 0x80 );
  pUserInput->bSaveWaterMass =        ( (pUserInput->diks[DIK_M] & 0x80)  == 0x80 );
};




//-----------------------------------------------------------------------------
// Name: Render()
// Desc: Called once per frame, the call is the entry point for 3d
//       rendering. This function sets up render states, clears the
//       viewport, and renders the scene.
//-----------------------------------------------------------------------------
HRESULT CMyD3DApplication::Render()
{
    // Clear the viewport
    m_pd3dDevice->Clear( 0L, NULL, D3DCLEAR_TARGET|D3DCLEAR_ZBUFFER,
                         0x00ffffff, 1.0f, 0L );

    // Begin the scene
    if( SUCCEEDED( m_pd3dDevice->BeginScene() ) )
    {
        // TODO: render world

        // Render the line
        m_pLineVB1->DrawLines(m_pd3dDevice);
        //m_pd3dDevice->SetStreamSource(0, m_pLineVB1, sizeof(CUSTOMVERTEX));
        //m_pd3dDevice->SetVertexShader(D3DFVF_CUSTOMVERTEX);
        //if (m_LineVCount1 > 0)
        //    m_pd3dDevice->DrawPrimitive(D3DPT_LINELIST, 0, m_LineVCount1);

#ifdef RUN_SECOND_SIMUL
        m_pLineVB2->DrawLines(m_pd3dDevice);
        //m_pd3dDevice->SetStreamSource(0, m_pLineVB2, sizeof(CUSTOMVERTEX));
        //m_pd3dDevice->SetVertexShader(D3DFVF_CUSTOMVERTEX);
        //if (m_LineVCount2 > 0)
        //    m_pd3dDevice->DrawPrimitive(D3DPT_LINELIST, 0, m_LineVCount2);        
#endif
      
        m_pHierarchyVB1->DrawLines(m_pd3dDevice);

#ifdef RUN_SECOND_SIMUL
        m_pHierarchyVB2->DrawLines(m_pd3dDevice);
#endif

        // Render the vertex buffer contents
        m_pCVB->DrawTriangles(m_pd3dDevice);
        /*m_pd3dDevice->SetStreamSource(0, m_pCVB, sizeof(CUSTOMVERTEX));
        m_pd3dDevice->SetIndices(m_pCIB, 0);
        m_pd3dDevice->SetVertexShader(D3DFVF_CUSTOMVERTEX);
        //m_pd3dDevice->DrawPrimitive(D3DPT_TRIANGLELIST, 0, 2);
        
        m_pd3dDevice->DrawIndexedPrimitive(
          D3DPT_TRIANGLELIST,
          0,
          MAP_SIZE * MAP_SIZE,
          0,
          (MAP_SIZE - 1) * (MAP_SIZE - 1) * 2);
        */
        m_pWVB1->DrawTriangles(m_pd3dDevice);
        m_pWVB2_1->DrawTriangles(m_pd3dDevice);
        /*
        if (m_WVCount1 != 0)
        {        
            m_pd3dDevice->SetStreamSource(0, m_pWVB1, sizeof(CUSTOMVERTEX));
            m_pd3dDevice->DrawPrimitive(D3DPT_TRIANGLELIST, 0, m_WVCount1 / 3);
        }
        */
#ifdef RUN_SECOND_SIMUL

        m_pWVB2->DrawTriangles(m_pd3dDevice);
        m_pWVB2_2->DrawTriangles(m_pd3dDevice);
        /*
        if (m_WVCount2 != 0)
        { 
            m_pd3dDevice->SetStreamSource(0, m_pWVB2, sizeof(CUSTOMVERTEX));
            m_pd3dDevice->DrawPrimitive(D3DPT_TRIANGLELIST, 0, m_WVCount2 / 3);
        }*/
#endif



        // Render stats and help text  
        RenderText();

        // End the scene.
        m_pd3dDevice->EndScene();
    }

    return S_OK;
}




//-----------------------------------------------------------------------------
// Name: RenderText()
// Desc: Renders stats and help text to the scene.
//-----------------------------------------------------------------------------
HRESULT CMyD3DApplication::RenderText()
{
    D3DCOLOR fontColor        = D3DCOLOR_ARGB(255,255,0,0);
    D3DCOLOR fontWarningColor = D3DCOLOR_ARGB(255,0,0,255);
    TCHAR szMsg[MAX_PATH] = TEXT("");
    RECT rct;
    ZeroMemory( &rct, sizeof(rct) );       

    m_pD3DXFont->Begin();
    rct.left   = 2;
    rct.right  = m_d3dsdBackBuffer.Width - 20;

    // Output display stats
    INT nNextLine = 40; 

    lstrcpy( szMsg, m_strDeviceStats );
    nNextLine -= 20; rct.top = nNextLine; rct.bottom = rct.top + 20;    
    m_pD3DXFont->DrawText( szMsg, -1, &rct, 0, fontColor );

    lstrcpy( szMsg, m_strFrameStats );
    nNextLine -= 20; rct.top = nNextLine; rct.bottom = rct.top + 20;    
    m_pD3DXFont->DrawText( szMsg, -1, &rct, 0, fontColor );

    // Output statistics & help
   /* nNextLine = m_d3dsdBackBuffer.Height; 

    wsprintf( szMsg, TEXT("Arrow keys: Up=%d Down=%d Left=%d Right=%d"), 
              m_UserInput.bRotateUp, m_UserInput.bRotateDown, m_UserInput.bRotateLeft, m_UserInput.bRotateRight );
    nNextLine -= 20; rct.top = nNextLine; rct.bottom = rct.top + 20;    
    m_pD3DXFont->DrawText( szMsg, -1, &rct, 0, fontColor );

    lstrcpy( szMsg, TEXT("Use arrow keys to rotate object") );
    nNextLine -= 20; rct.top = nNextLine; rct.bottom = rct.top + 20;    
    m_pD3DXFont->DrawText( szMsg, -1, &rct, 0, fontColor );

    lstrcpy( szMsg, TEXT("Press 'F2' to configure display") );
    nNextLine -= 20; rct.top = nNextLine; rct.bottom = rct.top + 20;    
    m_pD3DXFont->DrawText( szMsg, -1, &rct, 0, fontColor );
    */

    m_pD3DXFont->End();

    return S_OK;
}




//-----------------------------------------------------------------------------
// Name: MsgProc()
// Desc: Overrrides the main WndProc, so the sample can do custom message
//       handling (e.g. processing mouse, keyboard, or menu commands).
//-----------------------------------------------------------------------------
LRESULT CMyD3DApplication::MsgProc( HWND hWnd, UINT msg, WPARAM wParam,
                                    LPARAM lParam )
{
    switch( msg )
    {
        case WM_PAINT:
        {
            if( m_bLoadingApp )
            {
                // Draw on the window tell the user that the app is loading
                // TODO: change as needed
                HDC hDC = GetDC( hWnd );
                TCHAR strMsg[MAX_PATH];
                wsprintf( strMsg, TEXT("Loading... Please wait") );
                RECT rct;
                GetClientRect( hWnd, &rct );
                DrawText( hDC, strMsg, -1, &rct, DT_CENTER|DT_VCENTER|DT_SINGLELINE );
                ReleaseDC( hWnd, hDC );
            }
            break;
        }

    }

    return CD3DApplication::MsgProc( hWnd, msg, wParam, lParam );
}




//-----------------------------------------------------------------------------
// Name: InvalidateDeviceObjects()
// Desc: Invalidates device objects.  
//-----------------------------------------------------------------------------
HRESULT CMyD3DApplication::InvalidateDeviceObjects()
{
    // TODO: Cleanup any objects created in RestoreDeviceObjects()
    SAFE_RELEASE( m_pD3DXFont );

    return S_OK;
}




//-----------------------------------------------------------------------------
// Name: DeleteDeviceObjects()
// Desc: Called when the app is exiting, or the device is being changed,
//       this function deletes any device dependent objects.  
//-----------------------------------------------------------------------------
HRESULT CMyD3DApplication::DeleteDeviceObjects()
{
    // TODO: Cleanup any objects created in InitDeviceObjects()
//    SAFE_RELEASE(m_pCVB);
//    SAFE_RELEASE(m_pCIB);
/*
    SAFE_RELEASE(m_pWVB1);
    SAFE_RELEASE(m_pWVB2);
*/
    
//    SAFE_RELEASE(m_pLineVB1);
#ifdef RUN_SECOND_SIMUL
//    SAFE_RELEASE(m_pLineVB2);
#endif

    return S_OK;
}




//-----------------------------------------------------------------------------
// Name: FinalCleanup()
// Desc: Called before the app exits, this function gives the app the chance
//       to cleanup after itself.
//-----------------------------------------------------------------------------
HRESULT CMyD3DApplication::FinalCleanup()
{
    // TODO: Perform any final cleanup needed
    // Cleanup DirectInput
    CleanupDirectInput();

    // Write the settings to the registry
    WriteSettings();

    return S_OK;
}




//-----------------------------------------------------------------------------
// Name: CleanupDirectInput()
// Desc: Cleanup DirectInput 
//-----------------------------------------------------------------------------
VOID CMyD3DApplication::CleanupDirectInput()
{
    // Cleanup DirectX input objects
    SAFE_RELEASE( m_pKeyboard );
    SAFE_RELEASE( m_pDI );

}




/*
void LogF(const char *format, ... )
{
  va_list argptr;
  va_start( argptr, format );


  char buf[512];
  vsprintf(buf,format,argptr);
  strcat(buf,"\n");
#ifdef _WIN32
  OutputDebugString(buf);
#else
  fputs(buf,stderr);
#endif
  va_end(argptr);

  // log file
  FILE * f = fopen("voda3d.log","a");
  if (f)  
    fprintf(f,buf);

  fclose(f);
}*/