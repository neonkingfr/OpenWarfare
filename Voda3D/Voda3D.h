//-----------------------------------------------------------------------------
// File: Voda3D.h
//
// Desc: Header file Voda3D sample app
//-----------------------------------------------------------------------------
#ifndef AFX_VODA3D_H__C00E51FC_93BB_40B8_AAAF_623EA5F7CF5C__INCLUDED_
#define AFX_VODA3D_H__C00E51FC_93BB_40B8_AAAF_623EA5F7CF5C__INCLUDED_

#include "Shared.h"
#include "amundsen.h"
#include "Byrd.h"
#include "Nansen.h"
#include "Peary.h"
#include "flyShape.h"
#include <El/ParamFile/paramFile.hpp>

//-----------------------------------------------------------------------------
// Defines, and constants
//-----------------------------------------------------------------------------

typedef struct 
{  
  ParamFile _cfgFile;
} CommandLineParam;

extern CommandLineParam GCLParam;

// TODO: change "DirectX AppWizard Apps" to your name or the company name
#define DXAPP_KEY        TEXT("Software\\DirectX AppWizard Apps\\Voda3D")

// Custom D3D vertex format used by the vertex buffer
struct CUSTOMVERTEX
{
    D3DXVECTOR3 position;       // vertex position
    DWORD       diffuse;        // diffuse color
};

#define D3DFVF_CUSTOMVERTEX (D3DFVF_XYZ|D3DFVF_DIFFUSE)


// Struct to store the current input state
struct UserInput
{
    BYTE diks[256];   // DirectInput keyboard state buffer 

    // TODO: change as needed
    BOOL bRotateUp;
    BOOL bRotateDown;
    BOOL bRotateLeft;
    BOOL bRotateRight;
    BOOL bMoveForward;
    BOOL bMoveBackward;
    BOOL bMoveUp;
    BOOL bMoveDown;
    BOOL bSimulate;
    BOOL bPlayer1XUp;
    BOOL bPlayer1XDown;
    BOOL bPlayer1YUp;
    BOOL bPlayer1YDown;
    BOOL bBuldozer;
    BOOL bZoomIn;
    BOOL bZoomOut;
    BOOL bOffsetXUp;
    BOOL bOffsetXDown;
    BOOL bOffsetYUp;
    BOOL bOffsetYDown;
    BOOL bDrawByRect;
    BOOL bSaveWaterMass;
    BOOL bSaveWaterHeight;
};




//-----------------------------------------------------------------------------
// Name: class CMyD3DApplication
// Desc: Application class. The base class (CD3DApplication) provides the 
//       generic functionality needed in all Direct3D samples. CMyD3DApplication 
//       adds functionality specific to this sample program.
//-----------------------------------------------------------------------------
class CMyD3DApplication : public CD3DApplication
{
    BOOL                    m_bLoadingApp;          // TRUE, if the app is loading
    CFlyShape               * m_pCVB;               // Country Vextex buffer 
    //LPDIRECT3DINDEXBUFFER8  m_pCIB;               // Country Index buffer 
    CFlyShape               * m_pWVB1;              // Water Vextex buffer
    CFlyShape               * m_pWVB2;              // Water Vextex buffer
    CFlyShape               * m_pLineVB1;           // Water Vextex buffer
    CFlyShape               * m_pLineVB2;           // Water Vextex buffer
    CFlyShape               * m_pHierarchyVB1;      // Water Vextex buffer
    CFlyShape               * m_pHierarchyVB2;      // Water Vextex buffer

    CFlyShapeSimCoord       * m_pWVB2_1;
    CFlyShapeSimCoord       * m_pWVB2_2;
    //int                     m_WVCount1;              // Water vertices count
    //int                     m_WVCount2;              // Water vertices count
    //int                     m_LineVCount1;
    //int                     m_LineVCount2;
    ID3DXFont*              m_pD3DXFont;            // D3DX font    

    LPDIRECTINPUT8          m_pDI;                  // DirectInput object
    LPDIRECTINPUTDEVICE8    m_pKeyboard;            // DirectInput keyboard device
    UserInput               m_UserInput;            // Struct for storing user input 

    D3DXVECTOR3             m_CameraPosition;
    FLOAT                   m_fCameraRotX;           // World rotation state X-axis
    FLOAT                   m_fCameraRotY;           // World rotation state Y-axis


    //float                   m_HeightMap[MAP_SIZE * MAP_SIZE];
    //float                   m_WaterMap[MAP_SIZE * MAP_SIZE];
    CCountry                m_Country;
    CCountry                m_DrawCountry;
    Sources                 m_Sources;
    
    CAmundsen                 m_Simulation1;    
    CPeary                  m_Simulation2;
    
    float                   m_pPlayer1Pos[2];
    float                   m_pPlayer2Pos[2];

    //unsigned int            m_iZoomLevel;  // 0 - no zoom, 
    //unsigned int                   m_pOffsets[2]; // Top Left coordinates of the window



protected:
    HRESULT OneTimeSceneInit();
    HRESULT InitDeviceObjects();
    HRESULT RestoreDeviceObjects();
    HRESULT InvalidateDeviceObjects();
    HRESULT DeleteDeviceObjects();
    HRESULT Render();
    HRESULT FrameMove();
    HRESULT CreateWaterVertexes(CArctic * pSimulation, CFlyShape& cWVB, RGBQUAD& tWater, RGBQUAD& tType);
    HRESULT CreateWaterVertexes2(CArctic * pSimulation, CFlyShapeSimCoord& cWVB,const RGBQUAD& tColor);
    HRESULT CreateCountryVertexes();
    HRESULT CreateArrowVertexes(CArctic * pSimulation, CFlyShape* pVB, RGBQUAD& tColor);
    HRESULT CreateHierarchyVertexes(CArctic * pSimulation, CFlyShape *pVB, RGBQUAD& tColor, BOOL bVerticalLines = FALSE);
    HRESULT FinalCleanup();
    HRESULT ConfirmDevice( D3DCAPS8*, DWORD, D3DFORMAT );

    HRESULT RenderText();

    HRESULT InitInput( HWND hWnd );
    void    UpdateInput( UserInput* pUserInput );
    void    CleanupDirectInput();

    VOID    ReadSettings();
    VOID    WriteSettings();
    
public:
     

    LRESULT MsgProc( HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam );
    CMyD3DApplication();
};


// Structure of AddArrow internal data
typedef struct 
{
    CFlyShape* _pVB;
    DWORD _nColor;
} TADDLINEDATA;

#endif // !defined(AFX_VODA3D_H__C00E51FC_93BB_40B8_AAAF_623EA5F7CF5C__INCLUDED_)
