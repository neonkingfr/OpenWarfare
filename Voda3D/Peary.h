#ifndef __PEARY_H__
#define __PEARY_H__

#include "Arctic.h"
#include "QuadTree.h"
#include "MyArrayWithDelete.h"


class CPearySquare;
class CCountryParcel;




//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//              C O U N T R Y P A R C E L
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


class CCountryParcel
{
protected:
    enum { _nTilles = 4, };

    float _fMinCountryHeight;
    float _fMaxCountryHeight;

    float _pfMassOnHeight[_nTilles];
    float _pfHeightOnMass[_nTilles];

    float _fHeightOnMassStep;
    float _fMassOnHeightStep;

    float _fSizeX;
    float _fSizeY;
public:
    CCountryParcel() {};//: _fMinCountryHeight(0), _fMaxCountryHeight(0), _fHeightOnMassStep(0), _fMassOnHeightStep(0),
    //_fSizeX(0), _fSizeY(0) {memset(_pfMassOnHeight,0x0, _nTilles * sizeof(*_pfMassOnHeight));memset(_pfHeightOnMass,0x0, _nTilles * sizeof(*_pfHeightOnMass));};

    CCountryParcel(const CCountryParcel& cParcel) {*this = cParcel;};

    float GetMinCoutryHeight() const {return _fMinCountryHeight;};
    float GetMaxCoutryHeight() const {return _fMaxCountryHeight;};

    void Init(const int iTopLeftX, 
         const int iTopLeftY,
         const unsigned int nSizeX,
         const unsigned int nSizeY,
         const CCountry * pcCountry);

    void InitOnConstantHeight(
         const float fHeight,
         const unsigned int nSizeX,
         const unsigned int nSizeY,
         const CCountry * pcCountry);

    void CalculateMassAndSurfaceFromHeight(float& fMass, float & fSurface, float fHeight);
    void CalculateHeightAndSurfaceFromMass(float& fHeight, float & fSurface, const float fMass);
    float CalculateFaceInXFromHeight(const float fHeight);
    float CalculateFaceInYFromHeight(const float fHeight);

    const CCountryParcel& operator=(const CCountryParcel& cParcel);

    friend class CPearySquare;
};
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//             P E A R Y C O N T A C T
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

/**
  * Contact. Data and pointer to the contact square.
  */
typedef struct _TPEARYCONTACT
{
    
    int _bSimulate;

    // Position data
    int _iBorder;
    unsigned int _iFrom;
    unsigned int _nSize;
    
    // Country data
    CCountryParcel _cBorderParcel;
    CCountryParcel _cBrotherBorderParcel;

    //float _fMinBorderHeight;
    //float _fMaxBorderHeight; 
    
    // Simulation data
    float _fSize;
    float _fLength;
    float _fInvLength;

    float _fSpeed;
    float _fNewSpeed;

    // Brother Square
    CPearySquare * _ptSquare;
} TPEARYCONTACT;


typedef MyArrayWithDelete<TPEARYCONTACT> MyPearyContactArray;
//-----------------------------------------------------------
//             P E A R Y C O N T A C T
//-----------------------------------------------------------

//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//              P E A R Y S Q U A R E
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

class CPearySquare
{    
protected:
    int _iTopLeftX; 
    int _iTopLeftY;

    unsigned int _nSizeX;
    unsigned int _nSizeY;

    float _fSizeX;
    float _fSizeY;

    CCountryParcel _cParcel;

    float _fWaterSurface; // Surface of water currently ready in Square
    float _fMass;
    float _fAverageWaterHeight;    
    float _fDiffMass; 
    
    //float _pfVelocity[2];
    //float _pfNewVelocity[2];

    MyPearyContactArray _cContacts;
public:
    CPearySquare() : _nSizeX(0), _nSizeY(0), _fSizeX(0.0f), _fSizeY(0.0f), _fWaterSurface(0.0f), _fMass(0.0f) 
      ,_fAverageWaterHeight(0.0f),  _fDiffMass(0.0f)
    {};

    void Init(int iTopLeftX, int iTopLeftY, 
        unsigned int SizeX, unsigned int SizeY, const CCountry * pcCountry);

    //int& TopLeftX() {return _iTopLeftX;};
    int TopLeftX() const {return _iTopLeftX;};

    //int& TopLeftY() {return _iTopLeftY;};
    int TopLeftY() const {return _iTopLeftY;};

    //unsigned int& nSizeX() {return _nSizeX;};
    unsigned int nSizeX() const {return _nSizeX;};

    //unsigned int& nSizeY() {return _nSizeY;};
    unsigned int nSizeY() const {return _nSizeY;};

    //float& fSizeX() {return _fSizeX;};
    float fSizeX() const {return _fSizeX;};

    //float& fSizeY() {return _fSizeY;};
    float fSizeY() const {return _fSizeY;};

    //float& MaxCountryHeight() {return _cParcel._fMaxCountryHeight;};
    float MaxCountryHeight() const {return _cParcel._fMaxCountryHeight;};

    //float& MinCountryHeight() {return _cParcel._fMinCountryHeight;};
    float MinCountryHeight() const {return _cParcel._fMinCountryHeight;};

    float& WaterSurface() {return _fWaterSurface;};
    float WaterSurface() const {return _fWaterSurface;};

    float& Mass() {return _fMass;};
    float Mass() const {return _fMass;};

    float& AverageWaterHeight() {return _fAverageWaterHeight;};
    float AverageWaterHeight() const {return _fAverageWaterHeight;};

    float& DiffMass() {return _fDiffMass;};
    float DiffMass() const {return _fDiffMass;};

    //float * Velocity() {return _pfVelocity;};
    //const float * Velocity() const {return _pfVelocity;};

    //float * NewVelocity() {return _pfNewVelocity;};
    //const float * NewVelocity() const {return _pfNewVelocity;};
    

    MyPearyContactArray& Contacts() {return _cContacts;};
    //MyContactArray Contacts() const {return _cContacts;};
    
    void AddContact(const TPEARYCONTACT& tContact) {_cContacts.Add(tContact);}  
    //void SetMinMaxHeightFromCountry(CCountry * pcCountry);
    void CalculateWaterHeightAndSurfaceFromMass();
    void CalculateMassFromWaterHeight();

    void RefreshContactsCountryDep(CCountry * pcCountry);

    void CreateContact(const int iBorder, 
        const unsigned int iFrom, 
        const unsigned int nSize, 
        CPearySquare * pSquare, 
        const int bSimulate,
        const CCountry * pcCountry, 
        float fSpeed = 0.0f);

    int FindContact(CPearySquare * pSquare, unsigned int iFrom); 
    TPEARYCONTACT& FindContact2(CPearySquare * pSquare, unsigned int iFrom); 
    int RetrieveContacts(MyPearyContactArray& cContatcs, unsigned int iBorder); 

    void Log() const; // Log contents of the square

};

//-----------------------------------------------------------
//              P E A R Y S Q U A R E
//-----------------------------------------------------------


typedef CQuadTree<CPearySquare> CPearyWaterQuadTree;
typedef CQuadTreeIterator<CPearySquare> CPearyWaterQuadTreeIterator;

typedef CQuadTree<int> CPatternTree;
typedef CQuadTreeIterator<int> CPatternTreeIterator;

//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//                   P E A R Y
//+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++


struct CPeary : public CArctic
{
protected:
    const CCountry * _pcCountry;
    MyArray2DFloat _cWaterHeight;
    MyArray2DFloat _cWaterMass;
    MyArray2DFloat _cWaterHeightZoomed;
    MyArray2DFloat _cWaterMassZoomed;
    MyArray2DInt _cType;
    //Sources * _ptSources;
    CPearyWaterQuadTree _cQuadTree;
    CPearySquare _pFictiveBorders[4];
    unsigned int _nSimulations;

protected:
    void CalculateWaterHeightFromQuadTree(CPearyWaterQuadTreeIterator& cIterator);    
    void FindMaxMinPureWaterHeight(CPearyWaterQuadTreeIterator& cIterator, float& fMaxHeight, float& fMinHeight);    
    void CalculateNet(CPearyWaterQuadTreeIterator& cIterator);

    void UpdateVelocities(float fTime, CPearyWaterQuadTreeIterator& cIterator);

    void PrepareUpdateWaterHeight(CPearyWaterQuadTreeIterator& cIterator);
    void UpdateWaterHeight(float fTime, CPearyWaterQuadTreeIterator& cIterator);
    void FindUpdateTime(float& fTime, CPearyWaterQuadTreeIterator& cIterator);
    void ZeroDiffMass(CPearyWaterQuadTreeIterator& cIterator);
    void CalculateDiffMass(float& fDiffMass, CPearyWaterQuadTreeIterator& cIterator);


    void ResolveOldContact(CPearyWaterQuadTreeIterator& cIterator, 
        TPEARYCONTACT& tContact,
        CPearySquare * pcParentSquare);  

    void DesolveOldContact(unsigned int iBorder, 
                                unsigned int iSquare0, 
                                unsigned int iSquare1, 
                                CPearyWaterQuadTreeIterator& cIterator, 
                                CPearySquare& cParentSquare);
    
    void CreatePatternTree(CPatternTreeIterator cIterator,
                           const float x, 
                           const float y, 
                           const float fSquareOriginX, 
                           const float fSquareOriginY, 
                           const float fSizeX, 
                           const float fSizeY,
                           const float fStopSize);

    void DeTeselate(CPearyWaterQuadTreeIterator& cIterator, unsigned int iChild);
    void DeTeselate(CPearyWaterQuadTreeIterator& cIterator, MyArray<unsigned int>& cZone);
    void Teselate(CPearyWaterQuadTreeIterator& cIterator, MyArray<unsigned int>& cZone);
    void Teselate(CPearyWaterQuadTreeIterator& cIterator, unsigned int iChild);

    void UpdateContactsLength(CPearyWaterQuadTreeIterator& cIterator);

    void EquidistantTeselation(CPearyWaterQuadTreeIterator& cIterator, unsigned int iActualLevel, unsigned int iStopLevel);

    void SynchronizeWithPatternTree(CPatternTreeIterator& cPatternIterator, CPearyWaterQuadTreeIterator& cIterator);

    void SetUpArrows(ADDLINEFCN fcnAddArrow, void * pFcnData, CPearyWaterQuadTreeIterator& cIterator);
    void SetUpBorderLines(ADDLINEFCN fcnAddLine, void * pFcnData, CPearyWaterQuadTreeIterator& cIterator, BOOL bVerticalLines);

    void CreateWaterVertexes(CPearyWaterQuadTreeIterator& cIterator, 
                                 CFlyShapeSimCoord& cShape, 
                                 unsigned int iWindowTopLeftX,
                                 unsigned int iWindowTopLeftY,
                                 unsigned int iWindowDownRightX,
                                 unsigned int iWindowDownRightY,
                                 float fMaxPureHeight,
                                 float fMinPureHeight,
                                 const RGBQUAD& tColor);

    
   
public:
    // Working
    void Teselate(MyArray<unsigned int>& cZone);
    void EquidistantTeselation(unsigned int iLevel);
    void EquidistantTeselation2(unsigned int iLevel);
    
    virtual void Init(CCountry *pcCountry);    
    virtual void Simulate();    
    virtual const MyArray2DFloat& GetWaterHeight() const {return _cWaterHeightZoomed;};
    virtual const MyArray2DInt& GetType() const {return _cType;};
    virtual const MyArray2DFloat& GetMass() const {return _cWaterMassZoomed;};

    virtual void SetUpArrows(ADDLINEFCN fcnAddArrow, void * pFcnData);
    virtual void SetUpBorderLines(ADDLINEFCN fcnAddArrow, void * pFcnData, BOOL bVerticalLines = FALSE);

    virtual void MoveTeselationToPoint(float x, float y, unsigned int iLowestLevel) ; 
    void DeTeselate(MyArray<unsigned int>& cZone);

    virtual void PrepareZoomedAndOffseted(unsigned int nSizeX, 
        unsigned int nSizeY, 
        unsigned int iZoomFactor, 
        unsigned int iOffsetX, 
        unsigned int iOffsetY);

    virtual void CreateWaterVertexes(
      CFlyShapeSimCoord& cShape,
      unsigned int nSizeX,
      unsigned int nSizeY,
      unsigned int iZoomLevel,
      unsigned int iOffsetX,
      unsigned int iOffsetY,
      const RGBQUAD& tColor);

    
    // Not ready yet
    virtual void SetSources(Sources *ptSources) {};
    virtual void CountryChanged() {};
        
};



#endif //__PEARY_H__