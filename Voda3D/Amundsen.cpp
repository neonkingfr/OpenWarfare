// Amundsen.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "Amundsen.h"
#include "math.h"

// for savepbl
#include <El/ParamFile/paramFile.hpp>
#include <extern/libpng/include/png.h>

//#define MARKERS
//#define MEASURE_MASS


//#define min(a,b) (a)

const float R_K = 0.7f;
const float H = 0.0f;

void CAmundsen::Init(CCountry * pcCountry)
{
	_pcCountry = pcCountry;
	_cWaterMass.Init(_pcCountry->GetSizeX(), _pcCountry->GetSizeY());

	_cWaterHeightOld = *pcCountry;
	_cType.Init(_pcCountry->GetSizeX(), _pcCountry->GetSizeY());
	_cTypeOld.Init(_pcCountry->GetSizeX(), _pcCountry->GetSizeY());
	_cFlowX.Init(_pcCountry->GetSizeX(), _pcCountry->GetSizeY());
	_cFlowY.Init(_pcCountry->GetSizeX(), _pcCountry->GetSizeY());
	_cFlowXOld.Init(_pcCountry->GetSizeX(), _pcCountry->GetSizeY());
	_cFlowYOld.Init(_pcCountry->GetSizeX(), _pcCountry->GetSizeY());
	return ;
}

void CAmundsen::Simulate()
{
	

	// Part 1. Add water to sources
	if (_ptSources != NULL)
	{ 
		for(unsigned int i = 0; i < _ptSources->GetSize(); i++)
		{
			const SOURCEPOINT& tSource = (*_ptSources)[i];
			_cWaterHeightOld.AddItem(tSource.x,tSource.y,tSource.fFlow *10);			
#ifdef MARKERS
			_cTypeOld.AddItem(tSource.x,tSource.y,-1);
#endif
		}
	}
	
	// Part 2. Water flow.
	// Within this part is calculated with old water heights (results from the last step) 
	for(unsigned int x = 1; x < _cWaterMass.GetSizeX() - 1; x ++)
	{
		for(unsigned int y = 1; y < _cWaterMass.GetSizeY() - 1; y ++)
		{
			
			float fHeight = _pcCountry->GetItem(x,y);	
			float fWaterHeight = _cWaterHeightOld.GetItem(x,y);
			float fMass = fWaterHeight - fHeight;

			if (fMass != 0)
			{
				
				int nChannels = 0;
				float fTotalDiff = 0;
				float fTotalIn = 0;
				float pfDiff[4];
				memset(pfDiff, 0x0, sizeof(*pfDiff) * 4);
				float pfNeighborHeight[4];

				// Part 2.1 Calculate gradients

				unsigned int xx = x + 1, yy = y;
				float fFlow = _cFlowXOld.GetItem(x,y);
				pfNeighborHeight[0] = max(_cWaterHeightOld.GetItem(xx,yy) - fFlow, _pcCountry->GetItem(xx,yy));
				if (fFlow >= 0)
				{
					if (fWaterHeight > pfNeighborHeight[0]) 
					{
						fTotalDiff += pfDiff[0] = fWaterHeight - pfNeighborHeight[0];
						nChannels++;
					}
				}
				else
						fTotalIn -= fFlow;
				
				xx = x - 1; yy = y;
				fFlow = - _cFlowXOld.GetItem(x - 1 ,y);
				pfNeighborHeight[1] = max(_cWaterHeightOld.GetItem(xx,yy) - fFlow, _pcCountry->GetItem(xx,yy));
				if (fFlow >= 0)
				{
					if (fWaterHeight > pfNeighborHeight[1]) 
					{
						fTotalDiff += pfDiff[1] = fWaterHeight - pfNeighborHeight[1];
						nChannels++;
					}
				}
				else
					fTotalIn -= fFlow;
			
				
				xx = x; yy = y + 1;
				fFlow = _cFlowYOld.GetItem(x ,y);
				pfNeighborHeight[2] = max(_cWaterHeightOld.GetItem(xx,yy) - fFlow, _pcCountry->GetItem(xx,yy));
				if (fFlow >= 0)
				{
					if( fWaterHeight > pfNeighborHeight[2]) 
					{
						fTotalDiff += pfDiff[2] = fWaterHeight - pfNeighborHeight[2];
						nChannels++;
					}
				}
				else
					fTotalIn -= fFlow;
				
				
				xx = x; yy = y - 1;
				fFlow = -_cFlowYOld.GetItem(x ,y - 1);
				pfNeighborHeight[3] = max(_cWaterHeightOld.GetItem(xx,yy) - fFlow, _pcCountry->GetItem(xx,yy));
				if (fFlow >= 0)
				{
					if (fWaterHeight > pfNeighborHeight[3]) 
					{
						fTotalDiff += pfDiff[3] = fWaterHeight - pfNeighborHeight[3];
						nChannels++;
					}
				}
				else
					fTotalIn -= fFlow;
								
								
				// Part 2.2 Determinate flows in direction of gradients

				fTotalIn = min(fMass, fTotalIn); 
				// fTotalIn can be grater than fMass. (some amount of water can just transwers through point

				if (fTotalDiff > 0.0f)					
				{
					// It is asummned, that the same amount of water will arrive to the point as in the last time step.
					// That's why, we can redistribute fTotalIn, without change of the water height. 
					float fGiveTotal; 
					float pfGive[4];
					memset(pfGive,0x0, sizeof(*pfGive) * 4);					
					
					fGiveTotal = (fTotalDiff <= fTotalIn)? fTotalDiff : fTotalIn;						
					
					for(unsigned int i = 0; i < 4; i++)
					{
						pfGive[i] = fGiveTotal * pfDiff[i] / fTotalDiff;
						pfDiff[i] -= pfGive[i];					
					}
					
					if (fTotalDiff > fTotalIn + 0.01 && fTotalIn + 0.01 < fMass ) // 0.01 are numerical barrieries.
					{
						fTotalDiff -= fGiveTotal;			

						float fAverage;
												
						// Find new water height. (fAverage -- means difference between current and new water height).
						do
						{
							fAverage = fTotalDiff / (nChannels + 1);
							fAverage = min(fAverage, fMass - fTotalIn);
							
							{								
								nChannels = 0;
								fTotalDiff = 0.0f;
								
								for(unsigned int i = 0; i < 4; i++)
								{
									if (pfDiff[i] > fAverage)
									{
										nChannels++;
										fTotalDiff += pfDiff[i];
									}							
								}
							}
						} while ( min(fTotalDiff / (nChannels + 1), fMass - fTotalIn) != fAverage);
						
						float fGiveTotal =  fAverage;
						float fGivenTotal = 0;
						fTotalDiff -= fAverage * nChannels;
						ASSERT(fTotalDiff > 0.0f);
						
						for(unsigned int i = 0; i < 4; i++)
						{
							if (pfDiff[i] > fAverage)
							{
								pfGive[i] += (pfDiff[i] - fAverage)  * fGiveTotal/ fTotalDiff;
								fGivenTotal += (pfDiff[i] - fAverage)  * fGiveTotal/ fTotalDiff;
								ASSERT( pfGive[i] > 0.0);
							}							
						}
						ASSERT(fabs(fGiveTotal - fGivenTotal) < 0.01);
					}
					
					
					// Set up flows.
					xx = x + 1, yy = y;				
					if (pfGive[0] > 0.0f)
					{																
						_cFlowX.SetItem( x, y, pfGive[0]); 																
					}
					
					
					xx = x - 1; yy = y;
					if (pfGive[1] > 0.0f )
					{					
						_cFlowX.SetItem( x - 1, y, -pfGive[1]); 
					}
					
					xx = x; yy = y + 1;				
					if (pfGive[2] > 0.0f)
					{																
						_cFlowY.SetItem( x, y, pfGive[2]); 																
					}
					
					
					xx = x; yy = y -1;
					if (pfGive[3] > 0.0f )
					{					
						_cFlowY.SetItem( x,y - 1, -pfGive[3]); 
					}
				}
			}
		}
	}



#ifdef MEASURE_MASS	
	float fGlobalMass = 0;
	{
	
		for(unsigned int x = 0; x < _cWaterHeightOld.GetSizeX(); x ++)
			for( unsigned int y = 0; y < _cWaterHeightOld.GetSizeY(); y ++)
				fGlobalMass += _cWaterHeightOld.GetItem(x,y) - _pcCountry->GetItem(x,y);
	}
#endif

	// Step 2.3 Calculate new water height according to flows.
	// NewWaterHeight = OldWaterHeight + FlowIn - flowOut

	for(unsigned int x = 0; x < _cWaterHeightOld.GetSizeX(); x ++)
	{		
		for( unsigned int y = 0; y < _cWaterHeightOld.GetSizeY(); y ++)
		{
			float fFlow = _cFlowX.GetItem(x,y);
			fFlow +=  x == 0 ? 0 : - _cFlowX.GetItem(x - 1 ,y);
			fFlow += _cFlowY.GetItem(x ,y);
			fFlow +=  y == 0 ? 0 : - _cFlowY.GetItem(x ,y - 1);

			_cWaterHeightOld.AddItem(x,y,-fFlow);
			if (_cWaterHeightOld.GetItem(x,y) < _pcCountry->GetItem(x,y))
			{
				ASSERT(_pcCountry->GetItem(x,y) - _cWaterHeightOld.GetItem(x,y) < 0.01);
				_cWaterHeightOld.SetItem(x,y,_pcCountry->GetItem(x,y)) ;
			}

		}
	}

#ifdef MEASURE_MASS			
	float fGlobalMass3 = 0;
	{
	
		for(unsigned int x = 0; x < _cWaterHeightOld.GetSizeX(); x ++)
			for( unsigned int y = 0; y < _cWaterHeightOld.GetSizeY(); y ++)
				fGlobalMass3 += _cWaterHeightOld.GetItem(x,y) - _pcCountry->GetItem(x,y);
	}
#endif		
	 
	
	// Step 3. Water effusion
	// Within this part we work and change current heights. The result is dependent on the 
	// "path" we walk through the field. (In future it is possible to vary path for each time step. 
	for(unsigned int x = 1; x < _cWaterHeightOld.GetSizeX() - 1; x ++)
		for( unsigned int y = 1; y < _cWaterHeightOld.GetSizeY() - 1; y ++)
		{			
			
			float fHeight = _pcCountry->GetItem(x,y);
			float fWaterHeight = _cWaterHeightOld.GetItem(x,y);
			float fMass = fWaterHeight - fHeight;
			if (fMass > 0)
			{
				// Step 3.1. Calculate gradients
				float fTotalDiff = 0;
				unsigned int nChannels = 0;
				float pfDiff[4];
				memset(pfDiff, 0x0, sizeof(*pfDiff) * 4);			
				
				unsigned int xx = x + 1, yy = y;
				
				if (fWaterHeight > _cWaterHeightOld.GetItem(xx,yy)) 
				{
					fTotalDiff += pfDiff[0] = fWaterHeight - _cWaterHeightOld.GetItem(xx,yy);
					nChannels++;
					
				}				
				
				xx = x - 1; yy = y;
				if (fWaterHeight > _cWaterHeightOld.GetItem(xx,yy)) 
				{
					fTotalDiff += pfDiff[1] = fWaterHeight - _cWaterHeightOld.GetItem(xx,yy);
					nChannels++;
					
				}		
				
				
				xx = x; yy = y + 1;
				if (fWaterHeight > _cWaterHeightOld.GetItem(xx,yy)) 
				{
					fTotalDiff += pfDiff[2] = fWaterHeight - _cWaterHeightOld.GetItem(xx,yy);
					nChannels++;
				}
				
				xx = x; yy = y - 1;
				if (fWaterHeight > _cWaterHeightOld.GetItem(xx,yy)) 
				{
					fTotalDiff += pfDiff[3] = fWaterHeight - _cWaterHeightOld.GetItem(xx,yy);
					nChannels++;
				}		
				
				if (fTotalDiff > 0.01f)
				{
										
					float pfGive[4];
					memset(pfGive,0x0, sizeof(*pfGive) * 4);					
					
					float fAverage;
																				
					// Find new water height. (fAverage -- means difference between current and new water height)
					do
					{
						fAverage = fTotalDiff / (nChannels + 1);
						fAverage = min(fAverage, fMass);
						//if (fAverage < fWaterHeight - fHeight)
						{
							
							nChannels = 0;
							fTotalDiff = 0.0f;
							
							for(unsigned int i = 0; i < 4; i++)
							{
								if (pfDiff[i] > fAverage)
								{
									nChannels++;
									fTotalDiff += pfDiff[i];
								}							
							}
						}
					} while ( min(fTotalDiff / (nChannels + 1), fMass) != fAverage);
					
					float fGiveTotal =  R_K * fAverage;
					float fGivenTotal = 0;
					fTotalDiff -= fAverage * nChannels;
					ASSERT(fTotalDiff > 0.0f);
					
					for(unsigned int i = 0; i < 4; i++)
					{
						if (pfDiff[i] > fAverage)
						{
							fGivenTotal += pfGive[i] += (pfDiff[i] - fAverage)  * fGiveTotal/ fTotalDiff;
							ASSERT( pfGive[i] > 0.0);
						}							
					}
					
					ASSERT(fabs(fGivenTotal - fGiveTotal) < 0.01 );
					
																				
					_cWaterHeightOld.AddItem(x,y, -fGiveTotal);

					// Step 3.2 Set up flows.
					xx = x + 1, yy = y;				
					if (pfGive[0] > 0.0f)
					{																
						_cFlowX.AddItem( x, y, pfGive[0]);
						_cWaterHeightOld.AddItem(xx,yy, pfGive[0]);
						
					}
					
					
					xx = x - 1; yy = y;
					if (pfGive[1] > 0.0f )
					{					
						_cFlowX.AddItem( x - 1, y, -pfGive[1]); 
						_cWaterHeightOld.AddItem(xx,yy, pfGive[1]);
					}
					
					xx = x; yy = y + 1;				
					if (pfGive[2] > 0.0f)
					{																
						_cFlowY.AddItem( x, y, pfGive[2]); 
						_cWaterHeightOld.AddItem(xx,yy, pfGive[2]);
					}
					
					
					xx = x; yy = y -1;
					if (pfGive[3] > 0.0f )
					{					
						_cFlowY.AddItem( x,y - 1, -pfGive[3]); 
						_cWaterHeightOld.AddItem(xx,yy, pfGive[3]);
					}
					
				}
			}
		}
				
#ifdef MEASURE_MASS					
	fGlobalMass = 0;	
	{
	
		for(unsigned int x = 0; x < _cWaterHeightOld.GetSizeX(); x ++)
			for( unsigned int y = 0; y < _cWaterHeightOld.GetSizeY(); y ++)
				fGlobalMass += _cWaterHeightOld.GetItem(x,y) - _pcCountry->GetItem(x,y);
	}
#endif 

	// Step 4. Suction. Remove water at the borders.
	float fWaterOut = 0;
	for(unsigned int  x = 0; x < _cWaterHeightOld.GetSizeX(); x ++)
	{
		float fFlow = -_cFlowY.GetItem(x,0);
		fFlow = fFlow > 0 ? fFlow : 0;

		fWaterOut += _cWaterHeightOld.GetItem(x,0) - _pcCountry->GetItem(x,0) - fFlow;		
		_cWaterHeightOld.SetItem(x, 0, _pcCountry->GetItem(x,0) + fFlow);
        
		unsigned int yy =  _cWaterHeightOld.GetSizeY() - 1;
		fFlow = -_cFlowY.GetItem(x,yy - 1);
		fFlow = fFlow > 0 ? fFlow : 0;
		fWaterOut += _cWaterHeightOld.GetItem(x,yy) - _pcCountry->GetItem(x,yy) - fFlow;
		_cWaterHeightOld.SetItem(x,yy,_pcCountry->GetItem(x,yy) + fFlow);
	}
	
	for(unsigned int  x = 0; x < _cWaterHeightOld.GetSizeY(); x ++)
	{
		float fFlow = -_cFlowX.GetItem(0,x);
		fFlow = fFlow > 0 ? fFlow : 0;
		fWaterOut += _cWaterHeightOld.GetItem(0,x) - _pcCountry->GetItem(0,x) - fFlow;
		_cWaterHeightOld.SetItem(0, x, _pcCountry->GetItem(0,x) + fFlow);

		unsigned int yy =  _cWaterHeightOld.GetSizeX() - 1;
		fFlow = _cFlowX.GetItem(yy - 1,x);
		fFlow = fFlow > 0 ? fFlow : 0;
		fWaterOut += _cWaterHeightOld.GetItem(yy,x) - _pcCountry->GetItem(yy,x)- fFlow;
		_cWaterHeightOld.SetItem(yy,x,_pcCountry->GetItem(yy,x) + fFlow);
	}

#ifdef MEASURE_MASS	
	float fGlobalMass2 = 0;	
	{
	
		for(unsigned int x =0; x < _cWaterHeightOld.GetSizeX(); x ++)
			for( unsigned int y = 0; y < _cWaterHeightOld.GetSizeY(); y ++)
				fGlobalMass2 += _cWaterHeightOld.GetItem(x,y) - _pcCountry->GetItem(x,y);
	}
	float fF = fGlobalMass2 + fWaterOut;
#endif
		 
	 _cFlowXOld = _cFlowX;
	 _cFlowYOld = _cFlowY;
	 _cFlowX.ZeroItems();
	 _cFlowY.ZeroItems();

	 // Calulate mass in each point (just for debugging purpouses
	 for(unsigned int  x = 0; x < _cWaterHeightOld.GetSizeX(); x ++)
		 for(unsigned int  y = 0; y < _cWaterHeightOld.GetSizeY(); y ++)
			 _cWaterMass.SetItem(x,y,_cWaterHeightOld.GetItem(x,y) - _pcCountry->GetItem(x,y));

#ifdef MARKERS	 

#define MIN_FLOW 0
	 // Move markers according to flow
	for(unsigned int  x = 1; x < _cWaterHeightOld.GetSizeX(); x ++)
		for( unsigned int y = 1; y < _cWaterHeightOld.GetSizeY(); y ++)
		{
			if (_cTypeOld.GetItem(x,y) < 0)
			{
				
				float fTotalFlow = 0.0f;

				unsigned int xx = x + 1, yy = y;
				float fFlow = _cFlowXOld.GetItem(x,y);
				if (fFlow >=MIN_FLOW) 
				{
					fTotalFlow += fFlow;
				}
				
				xx = x - 1; yy = y;
				fFlow = - _cFlowXOld.GetItem(x - 1 ,y);
				if (fFlow >= MIN_FLOW) 
				{
					fTotalFlow += fFlow;
				}
			
				
				xx = x; yy = y + 1;
				fFlow = _cFlowYOld.GetItem(x ,y);
				if (fFlow >= MIN_FLOW) 
				{
					fTotalFlow += fFlow;
				}
				
				
				xx = x; yy = y - 1;
				fFlow = -_cFlowYOld.GetItem(x,y - 1);
				if (fFlow >= MIN_FLOW) 
				{
					fTotalFlow += fFlow;
				}	
				
				if (fTotalFlow > 0.0f)
				{
					for(unsigned k = 0; k < -_cTypeOld.GetItem(x,y); k++)
					{
						float fRandValue = (1.0f * rand()) / RAND_MAX * fTotalFlow;
						float fValue = 0;

						unsigned int xx = x + 1, yy = y;
						float fFlow = _cFlowXOld.GetItem(x,y);
						if (fFlow >= MIN_FLOW) 
						{
							fValue += fFlow;
							if (fRandValue < fValue)
							{
								_cType.AddItem(xx,yy,-1);
								continue;
							}
						}
						

						xx = x - 1; yy = y;
						fFlow = - _cFlowXOld.GetItem(x - 1 ,y);
						if (fFlow >= MIN_FLOW) 
						{
							fValue += fFlow;
							if (fRandValue < fValue)
							{
								_cType.AddItem(xx,yy,-1);
								continue;
							}
						}
						
						
						xx = x; yy = y + 1;
						fFlow = _cFlowYOld.GetItem(x ,y);
						if (fFlow >= MIN_FLOW) 
						{
							fValue += fFlow;
							if (fRandValue < fValue)
							{
								_cType.AddItem(xx,yy,-1);
								continue;
							}
						}
						
						
						xx = x; yy = y - 1;
						fFlow = -_cFlowYOld.GetItem(x,y - 1);
						if (fFlow >= MIN_FLOW) 
						{
							fValue += fFlow;
							if (fRandValue < fValue)
							{
								_cType.AddItem(xx,yy,-1);
								continue;
							}
						}
						
					}
					
				}
				else
					_cType.AddItem(x,y, _cTypeOld.GetItem(x,y));
			}

			
		}
	
	_cTypeOld = _cType;
	_cType.ZeroItems();
#endif

	return;
}

void CAmundsen::PrepareZoomedAndOffseted(unsigned int nSizeX, 
                                       unsigned int nSizeY, 
                                       unsigned int iZoomLevel, 
                                       unsigned int iOffsetX, 
                                       unsigned int iOffsetY)
{
  _cWaterHeightZoomed.Init(nSizeX, nSizeY);
  _cWaterMassZoomed.Init(nSizeX, nSizeY);

  // Update Mass and Water field.
  _cWaterHeightOld.GetZoomedAndOffseted(_cWaterHeightZoomed, iZoomLevel, iOffsetX, iOffsetY);
  _cWaterMass.GetZoomedAndOffseted(_cWaterMassZoomed, iZoomLevel, iOffsetX, iOffsetY);
}

// error and warning handling for png


void user_error_fn(png_structp png_ptr, png_const_charp error_msg);
void user_warning_fn(png_structp png_ptr,
                     png_const_charp warning_msg);

void WipeWaterHeight(MyArray2DFloat& out, const MyArray2DFloat& in, const MyArray2DFloat& mass);
void CAmundsen::SaveWaterInPbl(LPCSTR dir, LPCSTR fileName, bool waterMass)
{

  MyArray2DFloat waterHeight;
  if (waterMass)
    waterHeight = _cWaterMass;
  else
  {
    WipeWaterHeight(waterHeight, *_pcCountry, _cWaterMass);
  }   

  // TODO: check file access
  //if (!CheckFileAccess(fileName, TRUE, FALSE))
  //  return;
  float minmaxImageHeight[2];  
  waterHeight.FindMaxMin(minmaxImageHeight[1], minmaxImageHeight[0]);

  float diff = minmaxImageHeight[1] - minmaxImageHeight[0];
  minmaxImageHeight[1] += diff / 10;
  minmaxImageHeight[0] -= diff / 10;

  char * cfgFileName = new char[strlen(dir) + strlen(fileName) + 5];
  strcpy(cfgFileName, dir);
  strcat(cfgFileName, fileName);
  strcat(cfgFileName, ".pbl");


  ParamFile cfgFile;
  if (LSOK != cfgFile.Parse(cfgFileName))
  {
    //AfxMessageBox(IDS_OPER_FAILED);
    return;
  }

  ParamClassPtr cfgClass = cfgFile.AddClass("cfg");  

  char * pngFileName = new char[strlen(dir) + strlen(fileName) + 5];
  strcpy(pngFileName, dir);
  strcat(pngFileName, fileName);
  strcat(pngFileName, ".png");

  //if (!CheckFileAccess(pngFileName, TRUE, FALSE))
  //  return;

  FILE *fp = fopen(pngFileName, "wb");
  if (!fp) 
  {
    //AfxMessageBox(IDS_OPER_FAILED);
    return;
  };

  char * pngFileName2 = new char[strlen(fileName) + 5];  
  strcpy(pngFileName2, fileName);
  strcat(pngFileName2, ".png");
  
  cfgClass->Add("PNGfilename", RStringB((LPCTSTR) pngFileName2));
  cfgClass->Add("squareSize", _pcCountry->GetStepX());
  cfgClass->Add("originX", _pcCountry->GetOriginX());
  cfgClass->Add("originY", _pcCountry->GetOriginY());

  int width = waterHeight.GetSizeX();
  int height = waterHeight.GetSizeY();

  png_structp png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL,user_error_fn,user_warning_fn); 

  png_infop info_ptr = png_create_info_struct(png_ptr);
  if (!info_ptr)
  {
    png_destroy_write_struct(&png_ptr,
      (png_infopp)NULL);
    fclose(fp);
    return;
  }

  png_init_io(png_ptr, fp);

  png_set_IHDR(png_ptr, info_ptr, width, height,
    16, PNG_COLOR_TYPE_GRAY, PNG_INTERLACE_NONE,
    PNG_COMPRESSION_TYPE_DEFAULT, PNG_FILTER_TYPE_DEFAULT);

  png_uint_16pp row_pointers = new png_uint_16p[height];
  png_uint_16p data = new png_uint_16[height* width];  

  for(int i = 0; i < height; i++)
    row_pointers[i] = data + i * width;  

  cfgClass->Add("minHeight", minmaxImageHeight[0]);
  cfgClass->Add("maxHeight", minmaxImageHeight[1]);

  cfgFile.Save();

  float step = (minmaxImageHeight[1] - minmaxImageHeight[0]) / 0xffff;  
  int heightm1 = height - 1;
  
  for(int z = 0; z < height; z++) for( int x = 0; x < width; x++)
  {
    float val = waterHeight.GetItem(x,z);
    data[(heightm1 - z) * width + x ] = (png_uint_16) ((minmaxImageHeight[1] - val)/step);
  }

  png_set_rows(png_ptr, info_ptr,(png_bytepp) row_pointers);
  png_write_png(png_ptr, info_ptr, PNG_TRANSFORM_SWAP_ENDIAN, NULL);
  png_destroy_write_struct(&png_ptr, &info_ptr);

  delete [] row_pointers;
  delete [] data;

  fclose(fp);
}


void WipeWaterHeight(MyArray2DFloat& out, const MyArray2DFloat& in, const MyArray2DFloat& mass)
{
  out = in;

  // place without water should be several meters under. place near water should be average value.
  float under = -10.0f;

  int sizex = in.GetSizeX() - 1;
  int sizez = in.GetSizeY() - 1;
  for(int z = 1; z < sizez; z++) 
    for(int x = 1; x < sizex; x++) 
    {
      if (mass.GetItem(x,z) > 0)
        out.AddItem(x,z,mass.GetItem(x,z));
      else
      {
        int n = 0;
        float val = 0; 

        for(int zz = -1; zz < 2; zz++) 
          for(int xx = -1; xx < 2; xx++)
          {
            float tmp;
            if ((tmp = mass.GetItem(x+xx, z+zz)) > 0)
            {
              val += in.GetItem(x+xx, z+zz) + mass.GetItem(x+xx, z+zz);
              n++;
            }
          }
        if (n > 0)
          out.SetItem(x,z,val/n);
        else
          out.AddItem(x,z,under);

      }
    }
}
