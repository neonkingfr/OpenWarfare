#ifndef __NANSEN_H__
#define __NANSEN_H__

#include "Arctic.h"
#include "QuadTree.h"
#include "MyArrayWithDelete.h"

class CSquare;

/**
  * Data about contact between two squares
  */
typedef struct _TCONTACTDATA 
{
    int _iBorder;
    unsigned int _iFrom;
    unsigned int _nSize;
    float _fMinBorderHeight;
    float _fMaxBorderHeight;  
    float _fSize;
} TCONTACTDATA;

/**
  * Contact. Data and pointer to the contact square.
  */
typedef struct _TCONTACT
{
    int _bSimulate;
    TCONTACTDATA _tData;
    CSquare * _ptSquare;
} TCONTACT;


typedef MyArrayWithDelete<TCONTACT> MyContactArray;

class CSquare
{    
protected:
    int _iTopLeftX; 
    int _iTopLeftY;
    unsigned int _nSizeX;
    unsigned int _nSizeY;
    float _fSizeX;
    float _fSizeY;

    float _fMaxCountryHeight;
    float _fMinCountryHeight;
    float _fWaterSurface;
    float _fMass;
    float _fAverageWaterHeight;    
    float _fDiffMass; 
    
    float _pfVelocity[2];
    float _pfNewVelocity[2];

    MyContactArray _cContacts;
public:
    CSquare() : _nSizeX(0), _nSizeY(0), _fSizeX(0.0f), _fSizeY(0.0f) {};

    int& TopLeftX() {return _iTopLeftX;};
    int TopLeftX() const {return _iTopLeftX;};

    int& TopLeftY() {return _iTopLeftY;};
    int TopLeftY() const {return _iTopLeftY;};

    unsigned int& nSizeX() {return _nSizeX;};
    unsigned int nSizeX() const {return _nSizeX;};

    unsigned int& nSizeY() {return _nSizeY;};
    unsigned int nSizeY() const {return _nSizeY;};

    float& fSizeX() {return _fSizeX;};
    float fSizeX() const {return _fSizeX;};

    float& fSizeY() {return _fSizeY;};
    float fSizeY() const {return _fSizeY;};

    float& MaxCountryHeight() {return _fMaxCountryHeight;};
    float MaxCountryHeight() const {return _fMaxCountryHeight;};

    float& MinCountryHeight() {return _fMinCountryHeight;};
    float MinCountryHeight() const {return _fMinCountryHeight;};

    float& WaterSurface() {return _fWaterSurface;};
    float WaterSurface() const {return _fWaterSurface;};

    float& Mass() {return _fMass;};
    float Mass() const {return _fMass;};

    float& AverageWaterHeight() {return _fAverageWaterHeight;};
    float AverageWaterHeight() const {return _fAverageWaterHeight;};

    float& DiffMass() {return _fDiffMass;};
    float DiffMass() const {return _fDiffMass;};

    float * Velocity() {return _pfVelocity;};
    const float * Velocity() const {return _pfVelocity;};

    float * NewVelocity() {return _pfNewVelocity;};
    const float * NewVelocity() const {return _pfNewVelocity;};
    

    MyContactArray& Contacts() {return _cContacts;};
    //MyContactArray Contacts() const {return _cContacts;};
    
    void AddContact(const TCONTACT& tContact) {_cContacts.Add(tContact);}  
    void SetMinMaxHeightFromCountry(CCountry * pcCountry);
    void CalculateWaterHeightAndSurfaceFromMass();
    void CalculateMassFromWaterHeight();

    void RefreshContactsCountryDep(CCountry * pcCountry);

    void CreateContact(const int iBorder, 
        const unsigned int iFrom, 
        const unsigned int nSize, 
        CSquare * pSquare, 
        const int bSimulate,
        CCountry * pcCountry);

    int FindContact(CSquare * pSquare, unsigned int iFrom); 
    TCONTACT& FindContact2(CSquare * pSquare, unsigned int iFrom); 
    int RetrieveContacts(MyContactArray& cContatcs, unsigned int iBorder); 

} ;

typedef CQuadTree<CSquare> CWater2QuadTree;
typedef CQuadTreeIterator<CSquare> CWater2QuadTreeIterator;

typedef CQuadTree<int> CPatternTree;
typedef CQuadTreeIterator<int> CPatternTreeIterator;

class CNansen : public CArctic
{
protected:
   CCountry * _pcCountry;
   MyArray2DFloat _cWaterHeight;
   MyArray2DFloat _cWaterMass;  
   MyArray2DInt _cType;
   Sources * _ptSources;
   CWater2QuadTree _cQuadTree;
   CSquare _pFictiveBorders[4];
   unsigned int _nSimulations;

private:
    /*
    void Congregate(CWaterQuadTreeIterator& cIterator, TSQUARE& tParentSquare);
    void CongregateFlows(CWaterQuadTreeIterator& cIterator, float * pFlows);
    
    void SimulateLeaf(CWaterQuadTreeIterator& cIterator, float * pFlows);
    void SimulateLeaf2(CWaterQuadTreeIterator& cIterator);
    void SimulateLeaf3(float & fMinTime, CWaterQuadTreeIterator& cIterator, 
                          float * pDiffFlows);

    void SimulateLeaf4(CWaterQuadTreeIterator& cIterator, 
                          const float fSimTime);
*/
    void SimulateLeaf2(float & fMinTime, CWater2QuadTreeIterator& cIterator);

    void SimulateLeaf3(const float fMinTime, CWater2QuadTreeIterator& cIterator);


    void SimulateLeaf(CWater2QuadTreeIterator& cIterator);

protected:

    void ResolveOldContact(CWater2QuadTreeIterator& cIterator, 
        TCONTACT& tContact,
        CSquare * pcParentSquare);  

    void CalculateWaterHeightFromQuadTree(CWater2QuadTreeIterator& cIterator);
    
    void CalculateNet(CWater2QuadTreeIterator& cIterator);

    void DesolveOldContact(unsigned int iBorder, 
                                unsigned int iSquare0, 
                                unsigned int iSquare1, 
                                CWater2QuadTreeIterator& cIterator, 
                                CSquare& cParentSquare);
    
    void CreatePatternTree(CPatternTreeIterator cIterator,
                           const float x, 
                           const float y, 
                           const float fSquareOriginX, 
                           const float fSquareOriginY, 
                           const float fSizeX, 
                           const float fSizeY,
                           const float fStopSize);

    void DeTeselate(CWater2QuadTreeIterator& cIterator, unsigned int iChild);
    void DeTeselate(CWater2QuadTreeIterator& cIterator, MyArray<unsigned int>& cZone);
    void Teselate(CWater2QuadTreeIterator& cIterator, MyArray<unsigned int>& cZone);
    void Teselate(CWater2QuadTreeIterator& cIterator, unsigned int iZone);

    void EquidistantTeselation(CWater2QuadTreeIterator& cIterator, unsigned int iActualLevel, unsigned int iStopLevel);

    void SynchronizeWithPatternTree(CPatternTreeIterator& cPatternIterator, CWater2QuadTreeIterator& cIterator);

    void RefreshCountryDep(CWater2QuadTreeIterator& cIterator);


public:
   void Teselate(MyArray<unsigned int>& cZone);
   void DeTeselate(MyArray<unsigned int>& cZone);
   void EquidistantTeselation(unsigned int iLevel);

   virtual void Init(CCountry * pcCountry);
   virtual void CountryChanged();
   virtual void Simulate();
   virtual void SetSources(Sources *ptSources) { _ptSources = ptSources;};
   virtual const MyArray2DFloat& GetWaterHeight() const {return _cWaterHeight;};
   virtual const MyArray2DInt& GetType() const {return _cType;};
   virtual const MyArray2DFloat& GetMass() const {return _cWaterMass;};
   virtual void MoveTeselationToPoint(float x, float y, unsigned int iLowestLevel);

   virtual void CreateWaterVertexes(
      CFlyShapeSimCoord& cShape,
      unsigned int nSizeX,
      unsigned int nSizeY,
      unsigned int iZoomLevel,
      unsigned int iOffsetX,
      unsigned int iOffsetY) {};

};

#endif //__NANSEN_H__