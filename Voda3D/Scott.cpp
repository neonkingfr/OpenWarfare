#include "stdafx.h"
#include "Scott.h"

CScott::CScott() {
  _pcCountry = NULL;
  _ptSources = NULL;
  _SimCounter = 0;
}

void CScott::Simulate() {
	
  // add water from sources
	if (_ptSources != NULL)
	{ 
		for(unsigned int i = 0; i < _ptSources->GetSize(); i++)
		{
			const SOURCEPOINT& tSource = (*_ptSources)[i];
			_cWaterHeight.AddItem(tSource.x,tSource.y,tSource.fFlow);
		}
	}
  // distribute water
  for (int j = 1; j < _cWaterHeight.GetSizeY() - 1; j++) {
    for (int i = 1; i < _cWaterHeight.GetSizeX() - 1; i++) {
      float Country = _pcCountry->GetItem(i, j);
      float Water = _cWaterHeight.GetItem(i, j);
      float NewWater = Water;
      for (int jj = j - 1; jj <= j + 1; jj++) {
        for (int ii = i - 1; ii <= i + 1; ii++) {
          if ((ii != i) || (jj != j)) {
            float NeiWater = _cWaterHeight.GetItem(ii, jj);
            if (NeiWater < Water) {
              float Delta;
              if (NeiWater > Country) {
                Delta = ((Water - NeiWater) / 8) * LIQUIDITY;
              }
              else {
                Delta = ((Water - Country) / 8) * LIQUIDITY;
              }
              if ((ii > 0) && (ii < _cWaterHeight.GetSizeX() - 1) && (jj > 0) && (jj < _cWaterHeight.GetSizeY() - 1)) {
                _cWaterHeight.AddItem(ii, jj, Delta);
              }
              NewWater -= Delta;
            }
          }
        }
      }
      _cWaterHeight.SetItem(i, j, NewWater);

      
      
/*      
      // Get sum of heights
      float SumHeight =
        (_cWaterHeight.GetItem(i - 1, j - 1) +
        _cWaterHeight.GetItem(i, j - 1) +
        _cWaterHeight.GetItem(i + 1, j - 1) +
        _cWaterHeight.GetItem(i - 1, j) +
        _cWaterHeight.GetItem(i, j) +
        _cWaterHeight.GetItem(i + 1, j) +
        _cWaterHeight.GetItem(i - 1, j + 1) +
        _cWaterHeight.GetItem(i, j + 1) +
        _cWaterHeight.GetItem(i + 1, j + 1));
      // Get average height
      float AvgHeight = SumHeight / 9;
      float Delta;
      if (_pcCountry->GetItem(i, j) > AvgHeight) {
        Delta = _cWaterHeight.GetItem(i, j) - _pcCountry->GetItem(i, j);
      }
      else {
        if (_cWaterHeight.GetItem(i, j) > AvgHeight)
          Delta = _cWaterHeight.GetItem(i, j) - AvgHeight;
        else
          Delta = 0.0f;
      }
      // Current minus delta
      _cWaterHeight.AddItem(i, j, -Delta);
      // Get sum of heights related to AvgHeight
      float SumAH = 0;
      for (int jj = j - 1; jj < j + 1; jj++) {
        for (int ii = i - 1; ii < i + 1; ii++) {
          if (_cWaterHeight.GetItem(ii, jj) <= AvgHeight) {
            SumAH += AvgHeight - _cWaterHeight.GetItem(ii, jj);
          }
        }
      }
      // Proportionally add delta
      for (jj = j - 1; jj < j + 1; jj++) {
        for (int ii = i - 1; ii < i + 1; ii++) {
          if (_cWaterHeight.GetItem(ii, jj) <= AvgHeight) {
            _cWaterHeight.AddItem(ii, jj, ((AvgHeight - _cWaterHeight.GetItem(ii, jj)) / SumAH) * Delta);
          }
        }
      }
*/

    }
  }
  _SimCounter++;
}

void CScott::Init(CCountry *pcCountry)
{
	_pcCountry = pcCountry;
	_cWaterHeight.Init(_pcCountry->GetSizeX(), _pcCountry->GetSizeY());
  _cWaterHeight = *_pcCountry;
}
