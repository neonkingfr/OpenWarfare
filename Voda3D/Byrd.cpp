#include "stdafx.h"
#include "Byrd.h"
#include "math.h"

const unsigned int  g_ppBorders[4][4] = {{0,1,0,2},{3,0,2,0},{0,0,0,3},{0,0,1,0}};

// Squares tilling: 0 1
//                  3 2
// Borders:    0
//            3 1
//             2  

const float MIN_WATER_SURFACE_COEF = 0.01f;
const float NUMERICAL_ZERO = 0.0001f;

__forceinline int IsNumericalZero(float fValue)
{
    return (fValue > -NUMERICAL_ZERO && fValue < NUMERICAL_ZERO);
}

int g_iStep = 1;
const int g_iStop = 0;

void DebugPoint()
{
    if (g_iStop == g_iStep)
    {
        int i = 3;
    }

    g_iStep++;
}


void CalculateWaterHeightAndSurfaceFromMass(TSQUARE& cSquare)
{
    

    if (cSquare._fMass > 0.0f)
    {
        float fTriangle = cSquare._fSizeX * cSquare._fSizeY * 
            (cSquare._fMaxCountryHeight - cSquare._fMinCountryHeight) / 2;

        if (cSquare._fMass > fTriangle)
        {
            cSquare._fAverageWaterHeight = cSquare._fMaxCountryHeight + 
                (cSquare._fMass - fTriangle) / (cSquare._fSizeX * cSquare._fSizeY);

            cSquare._fWaterSurface = (cSquare._fSizeX * cSquare._fSizeY);
        }
        else
        {
            cSquare._fAverageWaterHeight = sqrtf(cSquare._fMass / (cSquare._fSizeX * cSquare._fSizeY) * 2.0f * 
                (cSquare._fMaxCountryHeight - cSquare._fMinCountryHeight));

            cSquare._fWaterSurface = (cSquare._fSizeX * cSquare._fSizeY) * cSquare._fAverageWaterHeight/(cSquare._fMaxCountryHeight - cSquare._fMinCountryHeight);

            cSquare._fAverageWaterHeight += cSquare._fMinCountryHeight;
        }
    }
    else
    {
        cSquare._fMass = 0.0f;
        cSquare._fAverageWaterHeight = cSquare._fMinCountryHeight;
        if (!IsNumericalZero(cSquare._fMaxCountryHeight - cSquare._fMinCountryHeight))
            cSquare._fWaterSurface = MIN_WATER_SURFACE_COEF * (cSquare._fSizeX * cSquare._fSizeY);
        else
            cSquare._fWaterSurface = (cSquare._fSizeX * cSquare._fSizeY);
    }

    cSquare._fWaterSurface = max( cSquare._fWaterSurface, MIN_WATER_SURFACE_COEF * (cSquare._fSizeX * cSquare._fSizeY));
}

void CalculateMassFromWaterHeight(TSQUARE& cSquare)
{
    if (cSquare._fAverageWaterHeight < cSquare._fMaxCountryHeight)
    {
        cSquare._fMass = cSquare._fSizeX * cSquare._fSizeY *  
            (cSquare._fAverageWaterHeight - cSquare._fMinCountryHeight) * (cSquare._fAverageWaterHeight  - cSquare._fMinCountryHeight)/ (cSquare._fMaxCountryHeight - cSquare._fMinCountryHeight) / 2.0f;

        return;
    }

    cSquare._fMass = cSquare._fSizeX * cSquare._fSizeY * ((cSquare._fAverageWaterHeight - cSquare._fMaxCountryHeight)  + (cSquare._fMaxCountryHeight - cSquare._fMinCountryHeight) / 2.0f);
    return;
}

void UpdateWaterHeightAndSurfaceFromMass(TSQUARE& cSquare, float fOldMass)
{
    if (cSquare._fMass == 0.0f)
    {
        cSquare._fMass = 0.0f;
        cSquare._fAverageWaterHeight = cSquare._fMinCountryHeight;
        if (!IsNumericalZero(cSquare._fMaxCountryHeight - cSquare._fMinCountryHeight))
            cSquare._fWaterSurface = MIN_WATER_SURFACE_COEF * (cSquare._fSizeX * cSquare._fSizeY);
        else
            cSquare._fWaterSurface = (cSquare._fSizeX * cSquare._fSizeY);

        return;
    }

    float fNewMass = cSquare._fMass;    
    CalculateMassFromWaterHeight(cSquare);

    cSquare._fMass += fNewMass - fOldMass;
    cSquare._fMass = max(0.0f, cSquare._fMass);

    CalculateWaterHeightAndSurfaceFromMass(cSquare);
    cSquare._fMass = fNewMass;
}

void CByrd::Init(CCountry * pcCountry)
{
    _pcCountry = pcCountry;
    _cWaterHeight = *_pcCountry;
    _cType.Init(pcCountry->GetSizeX(), pcCountry->GetSizeY());
    _cWaterMass.Init(pcCountry->GetSizeX(), pcCountry->GetSizeY());

    // Initialize
    CWaterQuadTreeIterator cIterator(_cQuadTree);

    cIterator[0]._fMass = 0.0f;
    cIterator[0]._nSizeX = pcCountry->GetSizeX() / 2;
    cIterator[0]._nSizeY = pcCountry->GetSizeY() / 2;
    cIterator[0]._fSizeX = pcCountry->GetStepX() * pcCountry->GetSizeX() / 2;
    cIterator[0]._fSizeY = pcCountry->GetStepY() * pcCountry->GetSizeY() / 2;
    cIterator[0]._iTopLeftX = 0;
    cIterator[0]._iTopLeftY = 0;
    cIterator[0]._fWaterSurface = MIN_WATER_SURFACE_COEF * cIterator[0]._fSizeX * cIterator[0]._fSizeY;


    cIterator[1] =  cIterator[0];    
    cIterator[1]._iTopLeftX = cIterator[0]._nSizeX;
    cIterator[1]._iTopLeftY = 0;

    cIterator[2] =  cIterator[0];    
    cIterator[2]._iTopLeftX = cIterator[0]._nSizeX;;
    cIterator[2]._iTopLeftY = cIterator[0]._nSizeY;

    cIterator[3] =  cIterator[0];    
    cIterator[3]._iTopLeftX = 0;
    cIterator[3]._iTopLeftY = cIterator[0]._nSizeY;

    for(int j = 0; j < 4; j++)
    {
        cIterator[j]._fAverageWaterHeight = cIterator[j]._fMinCountryHeight = _pcCountry->GetMinimum(
            cIterator[j]._iTopLeftX, cIterator[j]._iTopLeftY, cIterator[j]._nSizeX, cIterator[j]._nSizeY);
        
        cIterator[j]._fMaxCountryHeight = cIterator[j]._fMinCountryHeight + 2 * (_pcCountry->GetAverageHeight(
            cIterator[j]._iTopLeftX, cIterator[j]._iTopLeftY, cIterator[j]._nSizeX, cIterator[j]._nSizeY) - 
            cIterator[j]._fMinCountryHeight);      

        cIterator[j]._pfMinBorderHeight[0] = _pcCountry->GetMinimum( cIterator[j]._iTopLeftX, cIterator[j]._iTopLeftY, cIterator[j]._nSizeX, 1);
        cIterator[j]._pfMinBorderHeight[1] = _pcCountry->GetMinimum( cIterator[j]._iTopLeftX + cIterator[j]._nSizeX - 1, cIterator[j]._iTopLeftY, 1, cIterator[j]._nSizeY);
        cIterator[j]._pfMinBorderHeight[2] = _pcCountry->GetMinimum( cIterator[j]._iTopLeftX, cIterator[j]._iTopLeftY + cIterator[j]._nSizeY - 1, cIterator[j]._nSizeX, 1);
        cIterator[j]._pfMinBorderHeight[3] = _pcCountry->GetMinimum( cIterator[j]._iTopLeftX, cIterator[j]._iTopLeftY, 1, cIterator[j]._nSizeY);                
     
        CalculateWaterHeightAndSurfaceFromMass(cIterator[j]);
    }

    //Create Borders
    memset(_pFictiveBorders, 0x0, 4 * sizeof(*_pFictiveBorders));
    _pFictiveBorders[0] = cIterator[0];
    _pFictiveBorders[1] = cIterator[1];
    _pFictiveBorders[2] = cIterator[2];
    _pFictiveBorders[3] = cIterator[3];
    
    cIterator[0]._ppBorders[0] = _pFictiveBorders;
    cIterator[0]._ppBorders[1] = &cIterator[1];
    cIterator[0]._ppBorders[2] = &cIterator[3];
    cIterator[0]._ppBorders[3] = _pFictiveBorders + 3;

    cIterator[1]._ppBorders[0] = _pFictiveBorders;
    cIterator[1]._ppBorders[1] = _pFictiveBorders + 1;
    cIterator[1]._ppBorders[2] = &cIterator[2];
    cIterator[1]._ppBorders[3] = &cIterator[0];

    cIterator[2]._ppBorders[0] = &cIterator[1];
    cIterator[2]._ppBorders[1] = _pFictiveBorders + 1;
    cIterator[2]._ppBorders[2] = _pFictiveBorders + 2;
    cIterator[2]._ppBorders[3] = &cIterator[3];

    cIterator[3]._ppBorders[0] = &cIterator[0];
    cIterator[3]._ppBorders[1] = &cIterator[2];
    cIterator[3]._ppBorders[2] = _pFictiveBorders + 2;
    cIterator[3]._ppBorders[3] = _pFictiveBorders + 3;


    MyArray<unsigned int> cZone;

    // Fehy03
/* 
    cZone.Add(2);
    cZone.Add(0);
    cZone.Add(1);

    Teselate(cZone);

    cZone.Release();

    cZone.Add(2);
    cZone.Add(3);
    cZone.Add(2);

    Teselate(cZone);

    cZone.Release();

    cZone.Add(2);
    cZone.Add(3);
    cZone.Add(1);

    Teselate(cZone);

    cZone.Release();

    cZone.Add(2);
    cZone.Add(0);
    cZone.Add(2);

    Teselate(cZone);

    cZone.Release();
    
    cZone.Add(1);
    cZone.Add(0);
    cZone.Add(1);

    Teselate(cZone);

    cZone.Release();
    
    cZone.Add(1);
    cZone.Add(0);
    cZone.Add(2);

    Teselate(cZone);

    cZone.Release();
    
    cZone.Add(1);
    cZone.Add(3);
    cZone.Add(1);

    Teselate(cZone);

    cZone.Release();
    
    cZone.Add(1);
    cZone.Add(3);
    cZone.Add(2);

    Teselate(cZone);
*/
    // Feh64x64
    
    
 /*   cZone.Add(0); 
    cZone.Add(2);
    cZone.Add(2);
    Teselate(cZone);
      
    cZone.Release();

    cZone.Add(1);
    cZone.Add(3);
    cZone.Add(3);
    Teselate(cZone);
    cZone.Release();
    

    cZone.Add(3); 
    cZone.Add(1);
    cZone.Add(1);
    Teselate(cZone);
      
    cZone.Release();

    cZone.Add(2);
    cZone.Add(0);
    cZone.Add(0);
    
    Teselate(cZone);
   */ 
/*   
    cZone.Release();      

    cZone.Add(1);          
    Teselate(cZone);

    cZone.Release();

    cZone.Add(2);          
    Teselate(cZone);
                
    
   cZone.Release();
    
    cZone.Add(1);
    cZone.Add(3);
    
    Teselate(cZone);
    
    cZone.Release();
    
    cZone.Add(2);
    cZone.Add(0);
    
    Teselate(cZone);
    cZone.Release();
    
    cZone.Add(2);
    cZone.Add(3);
    
    Teselate(cZone);
    cZone.Release();
    
    cZone.Add(0);
    cZone.Add(1);
    
    Teselate(cZone);
    cZone.Release();
    
    cZone.Add(0);
    cZone.Add(2);
    
    Teselate(cZone);
    cZone.Release();
    
    cZone.Add(3);
    cZone.Add(1);
    
    Teselate(cZone);
    cZone.Release();
    
    cZone.Add(3);
    cZone.Add(2);
    
    Teselate(cZone);
    cZone.Release();
    
    cZone.Add(1);
    cZone.Add(2);
    cZone.Add(0);
    
    Teselate(cZone);
    cZone.Release();
    
    cZone.Add(1);
    cZone.Add(2);
    cZone.Add(1);
    
    Teselate(cZone);
    cZone.Release();
    
    cZone.Add(1);
    cZone.Add(2);
    cZone.Add(2);
    
    Teselate(cZone);
    cZone.Release();
    
    cZone.Add(1);
    cZone.Add(2);
    cZone.Add(3);

    Teselate(cZone);
    cZone.Release();
    
    cZone.Add(2);
    cZone.Add(1);
    cZone.Add(0);
    
    Teselate(cZone);
    cZone.Release();
    
    cZone.Add(2);
    cZone.Add(1);
    cZone.Add(1);
    
    Teselate(cZone);
    cZone.Release();
    
    cZone.Add(2);
    cZone.Add(1);
    cZone.Add(2);
    
    Teselate(cZone);
    cZone.Release();
    
    cZone.Add(2);
    cZone.Add(1);
    cZone.Add(3);
    
    Teselate(cZone);
 */
    cIterator.ToTop();
    _cWaterMass.ZeroItems();    
    CalculateWaterHeightFromQuadTree(cIterator);

    cIterator.ToTop();
    CalculateNet(cIterator);

    _nSimulations = 0;    
}



void CByrd::Teselate(MyArray<unsigned int>& cZone)
{
    CWaterQuadTreeIterator cIterator(_cQuadTree);
   
    for(unsigned int i = 0; i < cZone.GetSize(); i++)
    {
        ASSERT(cZone[i] < 4);
        if (!cIterator.IsChild(cZone[i]))
        {
            cIterator.CreateChild(cZone[i]);
            TSQUARE& tParentSquare = cIterator[cZone[i]];
            cIterator.ToChild(cZone[i]);

            cIterator[0] = tParentSquare;
            cIterator[0]._nSizeX =  tParentSquare._nSizeX / 2;
            cIterator[0]._nSizeY =  tParentSquare._nSizeY / 2;
            cIterator[0]._fSizeX =  tParentSquare._fSizeX / 2;
            cIterator[0]._fSizeY =  tParentSquare._fSizeY / 2;
            cIterator[0]._fMass =  tParentSquare._fMass / 4; 

            cIterator[1] = cIterator[0];
            cIterator[1]._iTopLeftX = cIterator[0]._iTopLeftX + cIterator[0]._nSizeX;

            cIterator[2] = cIterator[0];
            cIterator[2]._iTopLeftX = cIterator[0]._iTopLeftX + cIterator[0]._nSizeX;
            cIterator[2]._iTopLeftY = cIterator[0]._iTopLeftY + cIterator[0]._nSizeY;

            cIterator[3] = cIterator[0];            
            cIterator[3]._iTopLeftY = cIterator[0]._iTopLeftY + cIterator[0]._nSizeY;
            
            for(int j = 0; j < 4; j++)
            {
                cIterator[j]._fMinCountryHeight = _pcCountry->GetMinimum(
                    cIterator[j]._iTopLeftX, cIterator[j]._iTopLeftY, cIterator[j]._nSizeX, cIterator[j]._nSizeY);

                cIterator[j]._fMaxCountryHeight = cIterator[j]._fMinCountryHeight + 2 * (_pcCountry->GetAverageHeight(
                    cIterator[j]._iTopLeftX, cIterator[j]._iTopLeftY, cIterator[j]._nSizeX, cIterator[j]._nSizeY) - 
                    cIterator[j]._fMinCountryHeight);

                cIterator[j]._pfMinBorderHeight[0] = _pcCountry->GetMinimum( cIterator[j]._iTopLeftX, cIterator[j]._iTopLeftY, cIterator[j]._nSizeX, 1);
                cIterator[j]._pfMinBorderHeight[1] = _pcCountry->GetMinimum( cIterator[j]._iTopLeftX + cIterator[j]._nSizeX - 1, cIterator[j]._iTopLeftY, 1, cIterator[j]._nSizeY);
                cIterator[j]._pfMinBorderHeight[2] = _pcCountry->GetMinimum( cIterator[j]._iTopLeftX, cIterator[j]._iTopLeftY + cIterator[j]._nSizeY - 1, cIterator[j]._nSizeX, 1);
                cIterator[j]._pfMinBorderHeight[3] = _pcCountry->GetMinimum( cIterator[j]._iTopLeftX, cIterator[j]._iTopLeftY, 1, cIterator[j]._nSizeY);                

                CalculateWaterHeightAndSurfaceFromMass(cIterator[j]);
            }

            cIterator[0]._ppBorders[0] = tParentSquare._ppBorders[0];
            cIterator[0]._ppBorders[1] = &cIterator[1];
            cIterator[0]._ppBorders[2] = &cIterator[3];
            cIterator[0]._ppBorders[3] = tParentSquare._ppBorders[3];
            
            cIterator[1]._ppBorders[0] = tParentSquare._ppBorders[0];
            cIterator[1]._ppBorders[1] = tParentSquare._ppBorders[1];
            cIterator[1]._ppBorders[2] = &cIterator[2];
            cIterator[1]._ppBorders[3] = &cIterator[0];
            
            cIterator[2]._ppBorders[0] = &cIterator[1];
            cIterator[2]._ppBorders[1] = tParentSquare._ppBorders[1];
            cIterator[2]._ppBorders[2] = tParentSquare._ppBorders[2];
            cIterator[2]._ppBorders[3] = &cIterator[3];
            
            cIterator[3]._ppBorders[0] = &cIterator[0];
            cIterator[3]._ppBorders[1] = &cIterator[2];
            cIterator[3]._ppBorders[2] = tParentSquare._ppBorders[2];
            cIterator[3]._ppBorders[3] = tParentSquare._ppBorders[3];

        }
        else
            cIterator.ToChild(cZone[i]);            
    }    

    cIterator.ToTop();
    unsigned int iFirstChild = cZone[0];

    TSQUARE& tSquare = cIterator[iFirstChild];
    cIterator.ToChild(iFirstChild);
    
    Congregate(cIterator,tSquare);

    cIterator.ToTop();
    _cWaterMass.ZeroItems();    
    CalculateWaterHeightFromQuadTree(cIterator);

    cIterator.ToTop();
    _cType.ZeroItems();
    CalculateNet(cIterator);
}
 
void CByrd::Congregate(CWaterQuadTreeIterator& cIterator, TSQUARE& tParentSquare)
{
    if (!cIterator.IsParent())
        return;

    tParentSquare._fMass = 0.0f;
    tParentSquare._fWaterSurface = 0.0f;
    tParentSquare._fAverageWaterHeight = 0.0f;
    float fWaterSurfaceTemp = 0.0f;

    float fMinWithoutWater = 10000000.0f;
    for(int i = 0; i < 4; i++)
    {

        TSQUARE& tSquare = cIterator[i];
        if (cIterator.IsChild(i))
        {            
            cIterator.ToChild(i);
            Congregate(cIterator, tSquare);         
            cIterator.ToParent();
        }
        else
        {
            if (IsNumericalZero(cIterator[i]._fMass))
                cIterator[i]._fMass = 0.0f;
                
        
            CalculateWaterHeightAndSurfaceFromMass(cIterator[i]);
        }


        tParentSquare._fMass +=  tSquare._fMass;
        tParentSquare._fWaterSurface += tSquare._fWaterSurface;

        

        if (!IsNumericalZero(tSquare._fMass ))
        {
            tParentSquare._fAverageWaterHeight += tSquare._fAverageWaterHeight * tSquare._fWaterSurface;
            fWaterSurfaceTemp += tSquare._fWaterSurface;            
        }
        else
        {
            if (fMinWithoutWater > tSquare._fMinCountryHeight)
                fMinWithoutWater = tSquare._fMinCountryHeight;
        }

    }

    if (fWaterSurfaceTemp == 0.0f)
    {
        tParentSquare._fAverageWaterHeight = tParentSquare._fMinCountryHeight;
    }
    else
    {
        tParentSquare._fAverageWaterHeight /= fWaterSurfaceTemp;
        if (fMinWithoutWater < tParentSquare._fAverageWaterHeight)
            tParentSquare._fAverageWaterHeight = fMinWithoutWater; // There is a place without water.

        
    }
}

void CByrd::CongregateFlows(CWaterQuadTreeIterator& cIterator, float * pFlows)
{
    for(int i = 0; i < 4; i++)
    {
        if (cIterator.IsChild(i))
        {                        
            float * pNewFlows = cIterator[i]._pFlows;
            memset(pNewFlows, 0x0, 4 * sizeof(*pNewFlows)); // Remove them and calculate own
            
            cIterator.ToChild(i);                                   
            CongregateFlows(cIterator,pNewFlows);
            cIterator.ToParent();
        }
    }

    pFlows[0] = cIterator[0]._pFlows[0];
    pFlows[0] += cIterator[1]._pFlows[0];

    pFlows[1] = cIterator[1]._pFlows[1];
    pFlows[1] += cIterator[2]._pFlows[1];

    pFlows[2] = cIterator[2]._pFlows[2];
    pFlows[2] += cIterator[3]._pFlows[2];

    pFlows[3] = cIterator[3]._pFlows[3];
    pFlows[3] += cIterator[0]._pFlows[3];

    


}

void CByrd::Simulate()
{
    // Simulate
    float pFlows[4];
    memset( pFlows, 0x0, sizeof(*pFlows) * 4);    

    CWaterQuadTreeIterator cIterator(_cQuadTree);

  //  if (cIterator[0]._fAverageWaterHeight < 300.0f)
  //  {
  //      //pFlows[2] = 0.001f;

  //      pFlows[3] = 50.0f;
  //  }
     //_pFictiveBorders[3]._fAverageWaterHeight = cIterator[0]._fAverageWaterHeight + 100.0f;
    _nSimulations++;

    // Fehy03
    /*
    _pFictiveBorders[0]._fAverageWaterHeight = 1000000.0f;
    _pFictiveBorders[0]._fMass = 0.0f;
    _pFictiveBorders[1]._fAverageWaterHeight = 0.0f;
    _pFictiveBorders[1]._fMass = 0.0f;
    _pFictiveBorders[2]._fAverageWaterHeight = 1000000.0f;
    _pFictiveBorders[2]._fMass = 0.0f;
    _pFictiveBorders[3]._fAverageWaterHeight = 150.0f;
*/
    //Feh64x64
    _pFictiveBorders[0]._fAverageWaterHeight = 1000000.0f;
    _pFictiveBorders[0]._fMass = 0.0f;
    _pFictiveBorders[1]._fAverageWaterHeight = 0.0f;
    _pFictiveBorders[1]._fMass = 0.0f;
    _pFictiveBorders[2]._fAverageWaterHeight = 1000000.0f;
    _pFictiveBorders[2]._fMass = 0.0f;
    _pFictiveBorders[3]._fAverageWaterHeight = 150.0f;

    if (_nSimulations < 1000)
        _pFictiveBorders[3]._fMass = 300.0f;
    else
        _pFictiveBorders[3]._fMass = 0.0f;

    float fSimTime = 2.00f;
    float fSpentTime = 0.0;
    do 
    {
        float fTime = min(fSimTime - fSpentTime, fSimTime/5);
        
        
        memset(_pFictiveBorders[0]._pFlows, 0x0, 4 * sizeof(*_pFictiveBorders[0]._pFlows));
        memset(_pFictiveBorders[1]._pFlows, 0x0, 4 * sizeof(*_pFictiveBorders[1]._pFlows));
        memset(_pFictiveBorders[2]._pFlows, 0x0, 4 * sizeof(*_pFictiveBorders[2]._pFlows));
        memset(_pFictiveBorders[3]._pFlows, 0x0, 4 * sizeof(*_pFictiveBorders[3]._pFlows));
        
        // if (cIterator[0]._fAverageWaterHeight < 200.0f)
        SimulateLeaf2( cIterator);
        
        // Congregate from bottom to top
        cIterator.ToTop();
        
        CongregateFlows(cIterator, pFlows);
        cIterator.ToTop();
        
        float fTotalFlow = pFlows[0] + pFlows[1] +pFlows[2] + pFlows[3];
        
        
        memset( pFlows, 0x0, sizeof(*pFlows) * 4);  
        SimulateLeaf3(fTime, cIterator, pFlows);
        
        cIterator.ToTop();
        SimulateLeaf4(cIterator, fTime);
        
        cIterator.ToTop();
        for(int i = 0; i < 4; i++)
        {
            if (cIterator.IsChild(i))
            {
                TSQUARE& tSquare = cIterator[i];
                cIterator.ToChild(i);
                Congregate(cIterator,tSquare);
                cIterator.ToParent();            
            }
            else
            {
                if (IsNumericalZero(cIterator[i]._fMass))
                    cIterator[i]._fMass = 0.0f;
            
                CalculateWaterHeightAndSurfaceFromMass(cIterator[i]);
            }
            
        }
        
        
        // Create water mass
        cIterator.ToTop();
        
        float fTotalMass = cIterator[0]._fMass + cIterator[1]._fMass + cIterator[2]._fMass + cIterator[3]._fMass;
        fTotalFlow *= fSimTime;

        fSpentTime += fTime;
    } while (fSpentTime + 0.01 < fSimTime);

    _cWaterMass.ZeroItems();    
    CalculateWaterHeightFromQuadTree(cIterator);

    return;   
}

__forceinline void AddFlow( float * pSquareFlows, const unsigned int iFromSquare, const unsigned int iToSquare, const float fFlow)
{
    if (iFromSquare < iToSquare)
    {
        if (iFromSquare == 0 && iToSquare == 3)
        {
            pSquareFlows[3] -= fFlow;
        }
        else
        {
            ASSERT(iFromSquare + 1 == iToSquare);
            pSquareFlows[iFromSquare] += fFlow;
        }

        return;
    }
    else
    {
        
        if (iToSquare == 0 && iFromSquare == 3)
        {
            pSquareFlows[3] += fFlow;
        }
        else
        {
            ASSERT(iToSquare + 1 == iFromSquare);
            pSquareFlows[iToSquare] -= fFlow;
        }

        return;
    }
}

__forceinline void DenegativeMass(CWaterQuadTreeIterator& cIterator,float * pSquareDiffs, float * pSquareFlows, const unsigned int iSquareTo, const unsigned int iSquareFrom1, const unsigned int iSquareFrom2)
{
    //ASSERT(iSquare1 < iSquare2 );

    unsigned int iSquare2 = iSquareFrom1;
    if (IsNumericalZero(cIterator[iSquareTo]._fMass) && pSquareDiffs[iSquareTo] < 0.0f)
    {
        // First take water from the higher neighbour.
        if ( cIterator[iSquareFrom1]._fAverageWaterHeight < cIterator[iSquareFrom2]._fAverageWaterHeight)
        {
            iSquare2 = iSquareFrom2;
        }

        if (IsNumericalZero(cIterator[iSquare2]._fMass))
        {
            if (pSquareDiffs[iSquare2] > 0.0f)
            {
                float fGive = min(pSquareDiffs[iSquare2], -pSquareDiffs[iSquareTo]);

                pSquareDiffs[iSquare2] -= fGive;
            
                AddFlow(pSquareFlows, iSquare2, iSquareTo, fGive);                

                pSquareDiffs[iSquareTo] += fGive;   
            }
        }
        else        
        {
            pSquareDiffs[iSquare2] += pSquareDiffs[iSquareTo];
            
            AddFlow(pSquareFlows, iSquare2, iSquareTo, -pSquareDiffs[iSquareTo]);
            pSquareDiffs[iSquareTo] = 0;            
        }
    }

    // Is more water needed?
    iSquare2 = iSquareFrom2;
    if (IsNumericalZero(cIterator[iSquareTo]._fMass) && pSquareDiffs[iSquareTo] < 0.0f)
    {
        if ( cIterator[iSquareFrom1]._fAverageWaterHeight < cIterator[iSquareFrom2]._fAverageWaterHeight)
        {
            iSquare2 = iSquareFrom1;
        }

        if (IsNumericalZero(cIterator[iSquare2]._fMass))
        {
            if (pSquareDiffs[iSquare2] > 0.0f)
            {
                float fGive = min(pSquareDiffs[iSquare2], -pSquareDiffs[iSquareTo]);

                pSquareDiffs[iSquare2] -= fGive;
            
                AddFlow(pSquareFlows, iSquare2, iSquareTo, fGive);                

                pSquareDiffs[iSquareTo] += fGive;   
            }
        }
        else        
        {
            pSquareDiffs[iSquare2] += pSquareDiffs[iSquareTo];
            
            AddFlow(pSquareFlows, iSquare2, iSquareTo, -pSquareDiffs[iSquareTo]);

            pSquareDiffs[iSquareTo] = 0;            
        }
    }


}

__forceinline void DenegativeMass(CWaterQuadTreeIterator& cIterator,float * pSquareDiffs, float * pSquareFlows, const unsigned int iSquareTo, const unsigned int iSquareFrom)
{
    //ASSERT(iSquare1 < iSquare2 );
  
    if (!IsNumericalZero(cIterator[iSquareTo]._fMass) || pSquareDiffs[iSquareTo] >= 0.0f)
        return;
    
    if (abs((int) (iSquareTo - iSquareFrom)) != 2)
    {
        
        if (IsNumericalZero(cIterator[iSquareFrom]._fMass))
        {
            if (pSquareDiffs[iSquareFrom] > 0.0f)
            {
                float fGive = min(pSquareDiffs[iSquareFrom], -pSquareDiffs[iSquareTo]);
                
                pSquareDiffs[iSquareFrom] -= fGive;
                
                AddFlow(pSquareFlows, iSquareFrom, iSquareTo, fGive);                
                
                pSquareDiffs[iSquareTo] += fGive;   
            }
        }
        else        
        {
            pSquareDiffs[iSquareFrom] += pSquareDiffs[iSquareTo];
            
            AddFlow(pSquareFlows, iSquareFrom, iSquareTo, -pSquareDiffs[iSquareTo]);
            pSquareDiffs[iSquareTo] = 0;            
        }

        return;
    }

    
    // transversal
    unsigned int iThroughtSquare = min(iSquareTo,iSquareFrom) + 1;
    
    if (IsNumericalZero(cIterator[iSquareFrom]._fMass))
    {
        if (pSquareDiffs[iSquareFrom] > 0.0f)
        {
            float fGive = min(pSquareDiffs[iSquareFrom], -pSquareDiffs[iSquareTo]);
            
            pSquareDiffs[iSquareFrom] -= fGive;
            
            AddFlow(pSquareFlows, iSquareFrom, iThroughtSquare, fGive);
            AddFlow(pSquareFlows, iThroughtSquare, iSquareTo, fGive);
            
            pSquareDiffs[iSquareTo] += fGive;   
        }
    }
    else        
    {
        pSquareDiffs[iSquareFrom] += pSquareDiffs[iSquareTo];
        
        AddFlow(pSquareFlows, iSquareFrom, iThroughtSquare, -pSquareDiffs[iSquareTo]);
        AddFlow(pSquareFlows, iThroughtSquare, iSquareTo, -pSquareDiffs[iSquareTo]);
        
        pSquareDiffs[iSquareTo] = 0.0f;            
    }
    
    return;
    
}



__forceinline void DenegativeMassTransversal(CWaterQuadTreeIterator& cIterator, float * pSquareDiffs, float * pSquareFlows, const unsigned int iSquare1, const unsigned int iSquare2, const unsigned int iThroughtSquare)
{
    
    if (IsNumericalZero(cIterator[iSquare1]._fMass) && pSquareDiffs[iSquare1] < 0.0f)
    {
        if (IsNumericalZero(cIterator[iSquare2]._fMass))
        {
            if (pSquareDiffs[iSquare2] > 0.0f)
            {
                float fGive = min(pSquareDiffs[iSquare2], -pSquareDiffs[iSquare1]);

                pSquareDiffs[iSquare2] -= fGive;
            
                AddFlow(pSquareFlows, iSquare2, iThroughtSquare, fGive);
                AddFlow(pSquareFlows, iThroughtSquare, iSquare1, fGive);

                pSquareDiffs[iSquare1] += fGive;   
            }
        }
        else        
        {
            pSquareDiffs[iSquare2] += pSquareDiffs[iSquare1];
            
            AddFlow(pSquareFlows, iSquare2, iThroughtSquare, -pSquareDiffs[iSquare1]);
            AddFlow(pSquareFlows, iThroughtSquare, iSquare1, -pSquareDiffs[iSquare1]);

            pSquareDiffs[iSquare1] = 0;            
        }
    }


    // Reverse order
    if (IsNumericalZero(cIterator[iSquare2]._fMass) && pSquareDiffs[iSquare2] < 0.0f)
    {
        if (IsNumericalZero(cIterator[iSquare1]._fMass))
        {
            if (pSquareDiffs[iSquare1] > 0.0f)
            {
                float fGive = min(pSquareDiffs[iSquare1], -pSquareDiffs[iSquare2]);

                pSquareDiffs[iSquare1] -= fGive;
            
                AddFlow(pSquareFlows, iSquare1, iThroughtSquare, fGive);
                AddFlow(pSquareFlows, iThroughtSquare, iSquare2, fGive);

                pSquareDiffs[iSquare2] += fGive;   
            }
        }
        else        
        {
            pSquareDiffs[iSquare1] += pSquareDiffs[iSquare2];
            
            AddFlow(pSquareFlows, iSquare1, iThroughtSquare, -pSquareDiffs[iSquare2]);
            AddFlow(pSquareFlows, iThroughtSquare, iSquare2, -pSquareDiffs[iSquare2]);

            pSquareDiffs[iSquare2] = 0;            
        }
    }
}

__forceinline void DenegativeMass2(CWaterQuadTreeIterator& cIterator,
                                   float * pSquareDiffs, 
                                   float * pSquareFlows, 
                                   const unsigned int iSquareTo)
{
    if (!IsNumericalZero(cIterator[iSquareTo]._fMass) || pSquareDiffs[iSquareTo] >= 0.0f)
        return;

    // The first try the heighest
    unsigned int iSquareFrom1;
    float fMaxHeigth = -10000;
    for(unsigned int i = 0; i < 4; i++)
    {
        if (i != iSquareTo && cIterator[i]._fAverageWaterHeight > fMaxHeigth)
        {
            fMaxHeigth = cIterator[i]._fAverageWaterHeight;
            iSquareFrom1 = i;
        }
            
    }

    DenegativeMass( cIterator, pSquareDiffs, pSquareFlows, iSquareTo, iSquareFrom1);

    if (!IsNumericalZero(cIterator[iSquareTo]._fMass) || pSquareDiffs[iSquareTo] >= 0.0f)
        return;

    unsigned int iSquareFrom2;
    fMaxHeigth = -10000;

    for(unsigned int i = 0; i < 4; i++)
    {
        if (i != iSquareTo && i != iSquareFrom1 && cIterator[i]._fAverageWaterHeight > fMaxHeigth)
        {        
            iSquareFrom2 = i;
            fMaxHeigth = cIterator[i]._fAverageWaterHeight;
        }

    }

    DenegativeMass( cIterator, pSquareDiffs, pSquareFlows, iSquareTo, iSquareFrom2);

    if (!IsNumericalZero(cIterator[iSquareTo]._fMass) || pSquareDiffs[iSquareTo] >= 0.0f)
        return;

    unsigned int iSquareFrom3;    

    for(unsigned int i = 0; i < 4; i++)
    {
        if (i != iSquareTo && i != iSquareFrom1 && i != iSquareFrom2)
            iSquareFrom3 = i;
    }

    DenegativeMass( cIterator, pSquareDiffs, pSquareFlows, iSquareTo, iSquareFrom3);    
}


__forceinline void PumpWaterBetweenBorders(float * pNewFlows, const unsigned int iBorderFrom, const unsigned int iBorderTo, const float fGive)
{
    if (abs((int)iBorderFrom - (int)iBorderTo) == 2)
    {
        if (iBorderFrom == 0)
        {
            pNewFlows[0 * 4 + 0] += fGive/2.0f;
            pNewFlows[1 * 4 + 0] += fGive/2.0f;

            pNewFlows[0 * 4 + 2] -= fGive/2.0f;
            pNewFlows[1 * 4 + 2] -= fGive/2.0f;

            pNewFlows[3 * 4 + 0] += fGive/2.0f;
            pNewFlows[2 * 4 + 0] += fGive/2.0f;

            pNewFlows[3 * 4 + 2] -= fGive/2.0f;
            pNewFlows[2 * 4 + 2] -= fGive/2.0f;

            return;
        }

        if (iBorderFrom == 2)
        {
            pNewFlows[0 * 4 + 0] -= fGive/2.0f;
            pNewFlows[1 * 4 + 0] -= fGive/2.0f;

            pNewFlows[0 * 4 + 2] += fGive/2.0f;
            pNewFlows[1 * 4 + 2] += fGive/2.0f;

            pNewFlows[3 * 4 + 0] -= fGive/2.0f;
            pNewFlows[2 * 4 + 0] -= fGive/2.0f;

            pNewFlows[3 * 4 + 2] += fGive/2.0f;
            pNewFlows[2 * 4 + 2] += fGive/2.0f;

            return;
        }

        if (iBorderFrom == 1)
        {
            pNewFlows[1 * 4 + 1] += fGive/2.0f;
            pNewFlows[2 * 4 + 1] += fGive/2.0f;

            pNewFlows[1 * 4 + 3] -= fGive/2.0f;
            pNewFlows[2 * 4 + 3] -= fGive/2.0f;

            pNewFlows[0 * 4 + 1] += fGive/2.0f;
            pNewFlows[3 * 4 + 1] += fGive/2.0f;

            pNewFlows[0 * 4 + 3] -= fGive/2.0f;
            pNewFlows[3 * 4 + 3] -= fGive/2.0f;

            return;
        }

        if (iBorderFrom == 3)
        {
            pNewFlows[1 * 4 + 1] -= fGive/2.0f;
            pNewFlows[2 * 4 + 1] -= fGive/2.0f;

            pNewFlows[1 * 4 + 3] += fGive/2.0f;
            pNewFlows[2 * 4 + 3] += fGive/2.0f;

            pNewFlows[0 * 4 + 1] -= fGive/2.0f;
            pNewFlows[3 * 4 + 1] -= fGive/2.0f;

            pNewFlows[0 * 4 + 3] += fGive/2.0f;
            pNewFlows[3 * 4 + 3] += fGive/2.0f;

            return;
        }
    }

    unsigned int iSquare = max(iBorderFrom, iBorderTo);
    if (abs((int) (iBorderFrom - iBorderTo)) == 3)
        iSquare = 0;

    pNewFlows[iSquare * 4 + iBorderFrom] += fGive;
    pNewFlows[iSquare * 4 + iBorderTo] -= fGive;
}


void CByrd::SimulateLeaf(CWaterQuadTreeIterator& cIterator, float * pFlows)
{
    DebugPoint();

    float pNewFlows[16]; // 4x4
    memset(pNewFlows,0x0,16 * sizeof(*pNewFlows));
       
    float fSimTime = 2.0f;
    float fSpentTime = 0.0f;
    unsigned int iCycles = 0;
    int bSimulateInnerFlows = 1;
    do 
    {
        iCycles++;
        float pSquareFlows[4];  //0->1, 1->2, 2->3, 3->0
        memset(pSquareFlows, 0x0, 4 * sizeof(*pSquareFlows));
        
        if (bSimulateInnerFlows)
        {
            
            for(int i = 0; i < 3; i++)
            {
                if (cIterator[i]._fAverageWaterHeight > cIterator[i + 1]._fAverageWaterHeight &&                 
                    (!IsNumericalZero(cIterator[i]._fMass))
                    && !IsNumericalZero(cIterator[i]._fAverageWaterHeight - cIterator[i + 1]._fAverageWaterHeight) && 
                    cIterator[i]._fAverageWaterHeight > cIterator[i]._pfMinBorderHeight[i+1])
                {
                /*if (cIterator[i]._fMinBorderHeight > cIterator[i + 1]._fAverageWaterHeight)
                {                
                pSquareFlows[i] = 1.0f * cIterator[i]._fSizeX * 
                (cIterator[i]._fAverageWaterHeight - cIterator[i]._fMinBorderHeight) / cIterator[i]._fSizeY;
                }
                    else */
                    {
                        pSquareFlows[i] = 1.0f * cIterator[i]._fSizeX * 
                            (cIterator[i]._fAverageWaterHeight - cIterator[i + 1]._fAverageWaterHeight) / cIterator[i]._fSizeY;
                    }
                }
                
                if (cIterator[i + 1]._fAverageWaterHeight > cIterator[i]._fAverageWaterHeight && 
                    (!IsNumericalZero(cIterator[i + 1]._fMass))
                    && !IsNumericalZero(cIterator[i]._fAverageWaterHeight - cIterator[i + 1]._fAverageWaterHeight) && 
                    cIterator[i + 1]._fAverageWaterHeight > cIterator[i]._pfMinBorderHeight[i + 1])
                {
                /*if (cIterator[i + 1]._fMinBorderHeight > cIterator[i]._fAverageWaterHeight)
                { 
                pSquareFlows[i] = -1.0f * cIterator[i]._fSizeX *
                (cIterator[i + 1]._fAverageWaterHeight - cIterator[i + 1]._fMinBorderHeight) / cIterator[i]._fSizeY;
                }
                    else*/
                    {
                        pSquareFlows[i] = -1.0f * cIterator[i]._fSizeX *
                            (cIterator[i + 1]._fAverageWaterHeight - cIterator[i]._fAverageWaterHeight) / cIterator[i]._fSizeY;
                    }
                }
            }
            
            if (cIterator[3]._fAverageWaterHeight > cIterator[0]._fAverageWaterHeight && 
                (!IsNumericalZero(cIterator[3]._fMass))
                && !IsNumericalZero(cIterator[0]._fAverageWaterHeight - cIterator[3]._fAverageWaterHeight)&& 
                cIterator[3]._fAverageWaterHeight > cIterator[3]._pfMinBorderHeight[0])
            {
            /*if (cIterator[3]._fMinBorderHeight > cIterator[0]._fAverageWaterHeight)
            {            
            pSquareFlows[3] = 1.0f * cIterator[3]._fSizeX * 
            (cIterator[3]._fAverageWaterHeight - cIterator[3]._fMinBorderHeight) / cIterator[3]._fSizeY;
            }
                else*/
                {
                    pSquareFlows[3] = 1.0f * cIterator[3]._fSizeX * 
                        (cIterator[3]._fAverageWaterHeight - cIterator[0]._fAverageWaterHeight) / cIterator[3]._fSizeY;
                }
                
            }
            
            if (cIterator[0]._fAverageWaterHeight > cIterator[3]._fAverageWaterHeight && 
                (!IsNumericalZero(cIterator[0]._fMass))
                && !IsNumericalZero(cIterator[3]._fAverageWaterHeight - cIterator[0]._fAverageWaterHeight)&& 
                cIterator[0]._fAverageWaterHeight > cIterator[3]._pfMinBorderHeight[0])
            {
            /*if (cIterator[0]._fMinBorderHeight > cIterator[3]._fAverageWaterHeight)
            {
            pSquareFlows[3] = -1.0f * cIterator[3]._fSizeX *
            (cIterator[0]._fAverageWaterHeight - cIterator[0]._fMinCountryHeight) / cIterator[3]._fSizeY;
            }
                else*/
                {
                    pSquareFlows[3] = -1.0f * cIterator[3]._fSizeX *
                        (cIterator[0]._fAverageWaterHeight - cIterator[3]._fAverageWaterHeight) / cIterator[3]._fSizeY;
                }
            }
        }

        bSimulateInnerFlows = 1;
        
        float pSquareDiffs[4];
        memset(pSquareDiffs, 0x0, 4 * sizeof(*pSquareDiffs));
        
        pSquareDiffs[0] = pSquareFlows[3] - pSquareFlows[0];
        pSquareDiffs[1] = pSquareFlows[0] - pSquareFlows[1];
        pSquareDiffs[2] = pSquareFlows[1] - pSquareFlows[2];
        pSquareDiffs[3] = pSquareFlows[2] - pSquareFlows[3];

        // Add border condition
        pSquareDiffs[0] += pFlows[0] / 2.0f / fSimTime + pFlows[3] / 2.0f / fSimTime;
        pSquareDiffs[1] += pFlows[1] / 2.0f / fSimTime + pFlows[0] / 2.0f / fSimTime;
        pSquareDiffs[2] += pFlows[2] / 2.0f / fSimTime + pFlows[1] / 2.0f / fSimTime;
        pSquareDiffs[3] += pFlows[3] / 2.0f / fSimTime + pFlows[2] / 2.0f / fSimTime;

        // If mass is 0 and pSquareDiffs is zero pump the water from neighbour squares.
        DenegativeMass( cIterator, pSquareDiffs, pSquareFlows, 0, 1, 3);    
        DenegativeMass( cIterator, pSquareDiffs, pSquareFlows, 1, 2, 0);
        DenegativeMass( cIterator, pSquareDiffs, pSquareFlows, 2, 3, 1);
        DenegativeMass( cIterator, pSquareDiffs, pSquareFlows, 3, 0, 2);

        DenegativeMassTransversal(cIterator, pSquareDiffs, pSquareFlows, 0, 2, 1);
        DenegativeMassTransversal(cIterator, pSquareDiffs, pSquareFlows, 1, 3, 2);

        // Calculate minimal time till mass will be lost
        float fMinTime = fSimTime / 10.0f;
        for(int i = 0; i < 4; i++)
        {
            if (pSquareDiffs[i] < 0.0f && (-cIterator[i]._fMass / pSquareDiffs[i]) < fMinTime && (-cIterator[i]._fMass / pSquareDiffs[i]) > 0.0f)
                fMinTime = (-cIterator[i]._fMass / pSquareDiffs[i]);
        }
        
        // Calculate time till any two values will be the same.
        float fMinTimeZal = fMinTime;
        for(int i = 0; i < 4; i++)
        {
            pSquareDiffs[i] /= cIterator[i]._fWaterSurface; // Diff in mass -> diff in height
        }
        
        for(int i = 0; i < 3; i++)
        {
            if (!IsNumericalZero(cIterator[i]._fAverageWaterHeight - cIterator[i + 1]._fAverageWaterHeight) &&
                !IsNumericalZero(pSquareDiffs[i] - pSquareDiffs[i + 1]))
                
            {
                float fMinTime2 = (cIterator[i]._fAverageWaterHeight - cIterator[i + 1]._fAverageWaterHeight) /
                    (pSquareDiffs[i + 1] - pSquareDiffs[i]);
                if (fMinTime2 > 0.0 && fMinTime2 < fMinTime)
                    fMinTime = fMinTime2;
            }
        }
        
        if (!IsNumericalZero(cIterator[0]._fAverageWaterHeight - cIterator[0]._fAverageWaterHeight) &&
            !IsNumericalZero(pSquareDiffs[3] - pSquareDiffs[0]))                  
        {
            float fMinTime2 = (cIterator[0]._fAverageWaterHeight - cIterator[3]._fAverageWaterHeight) /
                (pSquareDiffs[3] - pSquareDiffs[0]);
            
            if (fMinTime2 > 0.0 && fMinTime2 < fMinTime)
                fMinTime = fMinTime2;
        }
        
        // speed over correctness
        if (IsNumericalZero(fMinTime/100.0f))
            fMinTime = fMinTimeZal;

        fMinTime = min(fSimTime - fSpentTime, fMinTime);
        fSpentTime += fMinTime;
        //
        // Do simulation for time t
        float pfOldMass[4];
        for(int i = 0; i < 4; i++)
        {
            pfOldMass[i] =  cIterator[i]._fMass;
            cIterator[i]._fMass += pSquareDiffs[i] * fMinTime * cIterator[i]._fWaterSurface;
        }
        
        for(int i = 0; i < 3; i++)
        {
            pNewFlows[4 * (i + 1) + g_ppBorders[i + 1][i]] += pSquareFlows[i] * fMinTime;
            pNewFlows[4 * i + g_ppBorders[i][i + 1]] -= pSquareFlows[i] * fMinTime;

            pNewFlows[4 * i + i] += pFlows[i] / 2.0f * fMinTime / fSimTime;
            pNewFlows[4 * (i + 1) + i] += pFlows[i] / 2.0f * fMinTime / fSimTime;
        }
        
        pNewFlows[ g_ppBorders[0][3]] += pSquareFlows[3] * fMinTime;
        pNewFlows[4 * 3 + g_ppBorders[3][0]] -= pSquareFlows[3] * fMinTime;

        pNewFlows[4 * 3 + 3] += pFlows[3] / 2.0f * fMinTime / fSimTime;
        pNewFlows[3] += pFlows[3] / 2.0f * fMinTime / fSimTime;        
        
        for(int i = 0; i < 4; i++)
            UpdateWaterHeightAndSurfaceFromMass(cIterator[i], pfOldMass[i]);

        if (iCycles == 20 && fSpentTime < fSimTime)
        {
            // Bloody trick. Maybe is problem with water just flowing throught rectangle. Remove this water.                  
            for(int i = 0; i < 4; i++)
            {                            
                // redistribute water
                for(int j = 0; pFlows[i] > 0.0f && j < 4; j++)
                {
                    if (pFlows[j] < 0.0f)
                    {
                        float fGive = min(pFlows[i], -pFlows[j]);
                        PumpWaterBetweenBorders(pNewFlows,i,j,fGive);
                        pFlows[i] -= fGive;
                        pFlows[j] += fGive;

                    }
                }                           
            }
        }

        if (iCycles > 20 && IsNumericalZero(fMinTime/100))
            bSimulateInnerFlows = 0;
        
        
    } while (fSpentTime < fSimTime && !IsNumericalZero(fSimTime - fSpentTime));

    // Go to childs

    for(int i = 0; i < 4; i++)
    {
        if (cIterator.IsChild(i))
        {
            cIterator.ToChild(i);
            SimulateLeaf(cIterator, pNewFlows + (4 * i));
            cIterator.ToParent();            
        }
    }
}


void CByrd::CalculateWaterHeightFromQuadTree(CWaterQuadTreeIterator& cIterator)
{
    for(int i = 0; i < 4; i++)
    {
        if (cIterator.IsChild(i))
        {
            cIterator.ToChild(i);
            CalculateWaterHeightFromQuadTree(cIterator);
            cIterator.ToParent();
        }
        else
        {
            const TSQUARE& tSquare = cIterator[i];
            for(unsigned int ix = tSquare._iTopLeftX; ix < tSquare._iTopLeftX + tSquare._nSizeX; ix++)
            {
                for(unsigned int iy = tSquare._iTopLeftY; iy < tSquare._iTopLeftY + tSquare._nSizeY; iy++)
                {
                    _cWaterHeight.SetItem(ix,iy,tSquare._fAverageWaterHeight);
                    if (tSquare._fAverageWaterHeight > _pcCountry->GetItem(ix,iy))
                        _cWaterMass.SetItem( ix, iy, tSquare._fAverageWaterHeight - _pcCountry->GetItem(ix,iy));
                }
            }
        }
    }    
}

void CByrd::SimulateLeaf2(CWaterQuadTreeIterator& cIterator)
{
    for(int i = 0; i < 4; i++)
    {
        memset(cIterator[i]._pFlows, 0x0, 4 * sizeof(*cIterator[i]._pFlows));
    }

    for(int i = 0; i < 4; i++)
    {
        if (cIterator.IsChild(i))
        {                        
            float * pNewFlows = cIterator[i]._pFlows;
            memset(pNewFlows, 0x0, 4 * sizeof(*pNewFlows)); // Remove them and calculate own
            
            cIterator.ToChild(i);                                   
            SimulateLeaf2(cIterator);
            cIterator.ToParent();
        }
        else
        {
            // Simulate
            float * pNewFlows = cIterator[i]._pFlows;            

            TSQUARE ** ppBorders = cIterator[i]._ppBorders;
            TSQUARE & tActualSqr = cIterator[i];
            
            for(int j = 0; j < 4; j++)
            {
                if (pNewFlows[j] == 0.0f)
                {
                    
                    if (tActualSqr._fAverageWaterHeight > ppBorders[j]->_fAverageWaterHeight &&   
                        !IsNumericalZero(tActualSqr._fMass) &&
                        !IsNumericalZero(tActualSqr._fAverageWaterHeight - ppBorders[j]->_fAverageWaterHeight) && 
                        tActualSqr._fAverageWaterHeight > tActualSqr._pfMinBorderHeight[j])
                    {
                        
                        if (tActualSqr._fMinCountryHeight > ppBorders[j]->_fAverageWaterHeight)
                        {
                        
                            pNewFlows[j] = 0.3f * tActualSqr._fSizeX * 
                                (tActualSqr._fMinCountryHeight - tActualSqr._fAverageWaterHeight) / 
                                (ppBorders[j]->_fSizeY + tActualSqr._fSizeY);
                        }
                        else
                        {
                            pNewFlows[j] = 0.3f * tActualSqr._fSizeX * 
                                (ppBorders[j]->_fAverageWaterHeight - tActualSqr._fAverageWaterHeight) / 
                                (ppBorders[j]->_fSizeY + tActualSqr._fSizeY);

                        }

                        ppBorders[j]->_pFlows[(j + 2) > 3 ? (j + 2) - 4 : (j + 2)] -= pNewFlows[j];
                        
                    }
                    
                    if (ppBorders[j]->_fAverageWaterHeight > tActualSqr._fAverageWaterHeight && 
                        !IsNumericalZero(ppBorders[j]->_fMass) &&
                        !IsNumericalZero(ppBorders[j]->_fAverageWaterHeight - tActualSqr._fAverageWaterHeight) && 
                        ppBorders[j]->_fAverageWaterHeight > tActualSqr._pfMinBorderHeight[j])
                    {                
                        if (ppBorders[j]->_fMinCountryHeight > tActualSqr._fAverageWaterHeight)
                        {
                        
                            pNewFlows[j] = 0.3f * tActualSqr._fSizeX * 
                                (ppBorders[j]->_fAverageWaterHeight - ppBorders[j]->_fMinCountryHeight) / 
                                (ppBorders[j]->_fSizeY + tActualSqr._fSizeY);
                        }
                        else
                        {
                            pNewFlows[j] = 0.3f * tActualSqr._fSizeX * 
                                (ppBorders[j]->_fAverageWaterHeight - tActualSqr._fAverageWaterHeight) / 
                                (ppBorders[j]->_fSizeY + tActualSqr._fSizeY);

                        }

                        ppBorders[j]->_pFlows[(j + 2) > 3 ? (j + 2) - 4 : (j + 2)] -= pNewFlows[j];                        
                    }
                }
            }            
        }
    }

    
    
}

void CByrd::SimulateLeaf3(float & fMinTime, CWaterQuadTreeIterator& cIterator, 
                          float * pDiffFlows)
{
    float ppNewDiffFlows[4 * 4];
    memset(ppNewDiffFlows,0x0, 16 * sizeof(*ppNewDiffFlows));
 
    // Fix from border
    cIterator[0]._pFlows[0] += pDiffFlows[0] / 2.0f;
    cIterator[1]._pFlows[0] += pDiffFlows[0] / 2.0f;
    ppNewDiffFlows[0 * 4 + 0] = pDiffFlows[0] / 2.0f;
    ppNewDiffFlows[1 * 4 + 0] = pDiffFlows[0] / 2.0f;

    cIterator[1]._pFlows[1] += pDiffFlows[1] / 2.0f;
    cIterator[2]._pFlows[1] += pDiffFlows[1] / 2.0f;
    ppNewDiffFlows[1 * 4 + 1] = pDiffFlows[1] / 2.0f;
    ppNewDiffFlows[2 * 4 + 1] = pDiffFlows[1] / 2.0f;

    cIterator[2]._pFlows[2] += pDiffFlows[2] / 2.0f;
    cIterator[3]._pFlows[2] += pDiffFlows[2] / 2.0f;
    ppNewDiffFlows[2 * 4 + 2] = pDiffFlows[2] / 2.0f;
    ppNewDiffFlows[3 * 4 + 2] = pDiffFlows[2] / 2.0f;

    cIterator[3]._pFlows[3] += pDiffFlows[3] / 2.0f;
    cIterator[0]._pFlows[3] += pDiffFlows[3] / 2.0f;
    ppNewDiffFlows[3 * 4 + 3] = pDiffFlows[3] / 2.0f;
    ppNewDiffFlows[0 * 4 + 3] = pDiffFlows[3] / 2.0f;


    // Fix between
    float fAverage; 
    fAverage = (cIterator[0]._pFlows[1] - cIterator[1]._pFlows[3]) / 2.0f;
    ppNewDiffFlows[0 * 4 + 1] = -(cIterator[0]._pFlows[1] - fAverage);
    ppNewDiffFlows[1 * 4 + 3] = ppNewDiffFlows[0 * 4 + 1];
    cIterator[0]._pFlows[1] = fAverage;
    cIterator[1]._pFlows[3] = -fAverage;

    fAverage = (cIterator[1]._pFlows[2] - cIterator[2]._pFlows[0]) / 2.0f;
    ppNewDiffFlows[1 * 4 + 2] = -(cIterator[1]._pFlows[2] - fAverage);
    ppNewDiffFlows[2 * 4 + 0] = ppNewDiffFlows[1 * 4 + 2];
    cIterator[1]._pFlows[2] = fAverage;
    cIterator[2]._pFlows[0] = -fAverage;

    fAverage = (cIterator[2]._pFlows[3] - cIterator[3]._pFlows[1]) / 2.0f;
    ppNewDiffFlows[2 * 4 + 3] = -(cIterator[2]._pFlows[3] - fAverage);
    ppNewDiffFlows[3 * 4 + 1] = ppNewDiffFlows[2 * 4 + 3];
    cIterator[2]._pFlows[3] = fAverage;
    cIterator[3]._pFlows[1] = -fAverage;

    fAverage = (cIterator[3]._pFlows[0] - cIterator[0]._pFlows[2]) / 2.0f;
    ppNewDiffFlows[3 * 4 + 0] = -(cIterator[3]._pFlows[0] - fAverage);
    ppNewDiffFlows[0 * 4 + 2] = ppNewDiffFlows[3 * 4 + 0];
    cIterator[3]._pFlows[0] = fAverage;
    cIterator[0]._pFlows[2] = -fAverage;

    float pMassDiffs[4];
    float fWholeMassDiff = 0.0f;

    for(int i = 0; i < 4; i++)
    {
        pMassDiffs[i] = 0.0f;
        for(int j = 0; j < 4; j++)
        {
            pMassDiffs[i] += cIterator[i]._pFlows[j];
        } 
        
        fWholeMassDiff += pMassDiffs[i];
    }


    

     // If mass is 0 and pSquareDiffs is zero pump the water from neighbour squares.
    float pSquareFlows[4];
    memset(pSquareFlows, 0x0, 4 * sizeof(*pSquareFlows));

    //DenegativeMass( cIterator, pMassDiffs, pSquareFlows, 0, 1, 3);    
    //DenegativeMass( cIterator, pMassDiffs, pSquareFlows, 1, 2, 0);
    //DenegativeMass( cIterator, pMassDiffs, pSquareFlows, 2, 3, 1);
    //DenegativeMass( cIterator, pMassDiffs, pSquareFlows, 3, 0, 2);

    //DenegativeMassTransversal(cIterator, pMassDiffs, pSquareFlows, 0, 2, 1);
    //DenegativeMassTransversal(cIterator, pMassDiffs, pSquareFlows, 1, 3, 2);
    DenegativeMass2( cIterator, pMassDiffs, pSquareFlows, 0);
    DenegativeMass2( cIterator, pMassDiffs, pSquareFlows, 1);
    DenegativeMass2( cIterator, pMassDiffs, pSquareFlows, 2);
    DenegativeMass2( cIterator, pMassDiffs, pSquareFlows, 3);    

    for(int i = 0; i < 3; i++)
    {
        cIterator[i + 1]._pFlows[g_ppBorders[i + 1][i]] += pSquareFlows[i];
        cIterator[i]._pFlows[g_ppBorders[i][i + 1]] -= pSquareFlows[i];
                
        ppNewDiffFlows[4 * (i + 1) + g_ppBorders[i + 1][i]] += pSquareFlows[i];
        ppNewDiffFlows[4 * i + g_ppBorders[i][i + 1]] -= pSquareFlows[i];
            
    }
    
    cIterator[0]._pFlows[2] += pSquareFlows[3];
    cIterator[3]._pFlows[0] -= pSquareFlows[3]; 

    ppNewDiffFlows[4 * 0 + g_ppBorders[0][3]] += pSquareFlows[3];
    ppNewDiffFlows[4 * 3 + g_ppBorders[3][0]] -= pSquareFlows[3];
    

     // Bloody trick. Maybe is problem with water just flowing throught rectangle. Remove this water.  
    int bPumpBetweenBorders = 1;
    if (bPumpBetweenBorders)
    {        
        float pFlows[4];
        pFlows[0] = cIterator[0]._pFlows[0];
        pFlows[0] += cIterator[1]._pFlows[0];
        
        pFlows[1] = cIterator[1]._pFlows[1];
        pFlows[1] += cIterator[2]._pFlows[1];
        
        pFlows[2] = cIterator[2]._pFlows[2];
        pFlows[2] += cIterator[3]._pFlows[2];
        
        pFlows[3] = cIterator[3]._pFlows[3];
        pFlows[3] += cIterator[0]._pFlows[3];
        
        float pNewFlows[16];
        memset(pNewFlows, 0x0, 16 * sizeof(*pNewFlows));
        for(int i = 0; i < 4; i++)
        {                            
            // redistribute water
            for(int j = 0; pFlows[i] > 0.0f && j < 4; j++)
            {
                if (pFlows[j] < 0.0f)
                {
                    float fGive = min(pFlows[i], -pFlows[j]);
                    PumpWaterBetweenBorders(pNewFlows,i,j,fGive);
                    pFlows[i] -= fGive;
                    pFlows[j] += fGive;
                }
            }                           
        }

        for(int i = 0; i < 4; i++)
        {
            for(int j = 0; j < 4; j++)
            {
                ppNewDiffFlows[i * 4 + j] += pNewFlows[i * 4 + j];
                cIterator[i]._pFlows[j] += pNewFlows[i * 4 + j];

            }
        }

         for(int i = 0; i < 4; i++)
         {
            pMassDiffs[i] = 0.0f;
            for(int j = 0; j < 4; j++)
            {
                pMassDiffs[i] += cIterator[i]._pFlows[j];
            }        
        }
    }

    // Go to childs
    for(int i = 0; i < 4; i++)
    {
        if (cIterator.IsChild(i))
        {
            cIterator.ToChild(i);
            SimulateLeaf3(fMinTime, cIterator, ppNewDiffFlows + 4 * i);
            cIterator.ToParent();
        }
        else
            if (pMassDiffs[i] < 0.0f && fMinTime > -cIterator[i]._fMass / pMassDiffs[i] && !IsNumericalZero(pMassDiffs[i]))
            {
                fMinTime = -cIterator[i]._fMass / pMassDiffs[i];
            }
    }
}

void CByrd::SimulateLeaf4(CWaterQuadTreeIterator& cIterator, 
                          const float fSimTime)
                          
{
    for(int i = 0; i < 4; i++)
    {
        if (cIterator.IsChild(i))
        {
            cIterator.ToChild(i);
            SimulateLeaf4(cIterator, fSimTime);
            cIterator.ToParent();
        }
        else
        {
            float fMassDiff = 0;
            for(int j = 0; j < 4; j++)
            {
                fMassDiff += cIterator[i]._pFlows[j];
            }

            cIterator[i]._fMass += fMassDiff * fSimTime;


        }
    }
}

void CByrd::CalculateNet(CWaterQuadTreeIterator& cIterator)
{
    for(int i = 0; i < 4; i++)
    {
        if (cIterator.IsChild(i))
        {
            cIterator.ToChild(i);
            CalculateNet(cIterator);
            cIterator.ToParent();            
        }
        else
        {
            for(unsigned int j = 0; j < cIterator[i]._nSizeX; j++ )
                _cType.SetItem(cIterator[i]._iTopLeftX + j,cIterator[i]._iTopLeftY, -1);

            for(unsigned int j = 0; j < cIterator[i]._nSizeY; j++ )
                _cType.SetItem(cIterator[i]._iTopLeftX,cIterator[i]._iTopLeftY + j, -1);

            for(unsigned int j = 0; j < cIterator[i]._nSizeX; j++ )
                _cType.SetItem(cIterator[i]._iTopLeftX + j,cIterator[i]._iTopLeftY + cIterator[i]._nSizeY - 1, -1);

            for(unsigned int j = 0; j < cIterator[i]._nSizeY; j++ )
                _cType.SetItem(cIterator[i]._iTopLeftX +  + cIterator[i]._nSizeY - 1,cIterator[i]._iTopLeftY + j, -1);
        }
    }
}