#include <windows.h>
#include <basetsd.h>
#include <math.h>
#include <stdio.h>
#include <D3DX8.h>
#include <DXErr8.h>
#include <tchar.h>
#include <dinput.h>
#include "D3DApp.h"
#include "D3DUtil.h"
#include "DXUtil.h"
#include "resource.h"
#include "stdafx.h"
#include "flyShape.h"

CFlyShape::CFlyShape() {
  _vertexCount = 0;
  _vb = NULL;
}

CFlyShape::~CFlyShape() {
  SAFE_RELEASE(_vb);
}

void CFlyShape::Init(IDirect3DDevice8 *pD3DDevice) {
  pD3DDevice->CreateVertexBuffer(
    BUFFER_VERTEX_COUNT * sizeof(SFlyVertex),
    0,
    D3DFVF_FLYVERTEX,
    D3DPOOL_MANAGED,
    &_vb);
}

void CFlyShape::Reset() {
  _vertexCount = 0;
}

void CFlyShape::AddVertex(const SFlyVertex &vertex) {
    ASSERT(_vertexCount < SHAPE_VERTEX_COUNT);
  _vertices[_vertexCount++] = vertex;
}

void CFlyShape::DrawTriangles(IDirect3DDevice8 *pD3DDevice) {
  SFlyVertex* pVertices;
  int roundedBufferVertexCount = (BUFFER_VERTEX_COUNT / 3) * 3;
  int index = 0;
  while (index < _vertexCount) {
    // Fill the vertex buffer
    int vcount = min(_vertexCount - index, roundedBufferVertexCount);
    _vb->Lock(0, vcount * sizeof(SFlyVertex), (BYTE**)&pVertices, 0);
    memcpy(pVertices, &_vertices[index], vcount * sizeof(SFlyVertex));
    _vb->Unlock();
    // Draw the stream
    pD3DDevice->SetStreamSource(0, _vb, sizeof(SFlyVertex));
    pD3DDevice->SetVertexShader(D3DFVF_FLYVERTEX);
    pD3DDevice->DrawPrimitive(D3DPT_TRIANGLELIST, 0, vcount / 3);
    // Increment the current index
    index += roundedBufferVertexCount;
  }
}

void CFlyShape::DrawLines(IDirect3DDevice8 *pD3DDevice) {
  SFlyVertex* pVertices;
  int roundedBufferVertexCount = (BUFFER_VERTEX_COUNT / 2) * 2;
  int index = 0;
  while (index < _vertexCount) {
    // Fill the vertex buffer
    int vcount = min(_vertexCount - index, roundedBufferVertexCount);
    _vb->Lock(0, vcount * sizeof(SFlyVertex), (BYTE**)&pVertices, 0);
    memcpy(pVertices, &_vertices[index], vcount * sizeof(SFlyVertex));
    _vb->Unlock();
    // Draw the stream
    pD3DDevice->SetStreamSource(0, _vb, sizeof(SFlyVertex));
    pD3DDevice->SetVertexShader(D3DFVF_FLYVERTEX);
    pD3DDevice->DrawPrimitive(D3DPT_LINELIST, 0, vcount / 2);
    // Increment the current index
    index += roundedBufferVertexCount;
  }
}
