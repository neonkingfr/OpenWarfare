#ifndef __TYPES_H__
#define __TYPES_H__

typedef struct 
{
	unsigned int x,y;
	float h;
	void *pData;
} COUNTRYPOINT;

typedef struct
{
	unsigned int x,y;
	float fFlow;
} SOURCEPOINT;

typedef  MyArray<SOURCEPOINT> Sources;

template<class TYPE> 
class MyArray2D
{
protected:
	unsigned int _nSizeX;
	unsigned int _nSizeY;

	TYPE *_pItems;

public:
	MyArray2D(): _nSizeX(0), _nSizeY(0), _pItems(NULL) {};
	~MyArray2D();
	MyArray2D(const MyArray2D& cArray);

	void Init(unsigned int nSizeX, unsigned int nSizeY);

	unsigned int GetSizeX() const {return _nSizeX;};
	unsigned int GetSizeY() const {return _nSizeY;};
	const TYPE * GetItems() const {return _pItems;};

	TYPE GetItem(unsigned int x, unsigned int y) const {return _pItems[x * _nSizeY + y ];}
	void SetItem(unsigned int x, unsigned int y, const TYPE& cItem) {_pItems[x * _nSizeY + y] = cItem;};	
	void AddItem(unsigned int x, unsigned int y, const TYPE& cItem) {_pItems[x * _nSizeY + y] += cItem;};
	
	void ZeroItems() {memset(_pItems, 0x0, _nSizeX * _nSizeY * sizeof(*_pItems));};
    
    void FindMaxMin(float &fMax, float &fMin) const;

	virtual const MyArray2D& operator=(const MyArray2D& cArray);
	const MyArray2D& operator+=(const MyArray2D& cArray);
    const MyArray2D& operator-=(const MyArray2D& cArray);

	MyArray2D operator+(const MyArray2D& cArray) const;
};

template<class TYPE> 
MyArray2D<TYPE>::MyArray2D(const MyArray2D& cArray) 
{
	Init( cArray._nSizeX, cArray._nSizeY);
	memcpy(_pItems, cArray._pItems,_nSizeX * _nSizeY * sizeof(*_pItems));
}

template<class TYPE> 
MyArray2D<TYPE>::~MyArray2D()
{
	delete [] _pItems;
}

template<class TYPE> 
void MyArray2D<TYPE>::Init(unsigned int nSizeX, unsigned int nSizeY)
{
	_nSizeX = nSizeX;
	_nSizeY = nSizeY;

	_pItems = new TYPE[_nSizeX * _nSizeY];
	memset(_pItems, 0x0, _nSizeX * _nSizeY * sizeof(*_pItems));
}

template<class TYPE> 
 const MyArray2D<TYPE>& MyArray2D<TYPE>::operator=(const MyArray2D<TYPE>& cArray)
{
	if (_nSizeX != cArray._nSizeX || _nSizeY != cArray._nSizeY)
	{
		delete [] _pItems;
		Init( cArray._nSizeX, cArray._nSizeY);
	}

	memcpy(_pItems, cArray._pItems,_nSizeX * _nSizeY * sizeof(*_pItems));

	return *this;
}

template<class TYPE> 
const MyArray2D<TYPE>& MyArray2D<TYPE>::operator+=(const MyArray2D<TYPE>& cArray)
{
	if (_nSizeX != cArray._nSizeX || _nSizeY != cArray._nSizeY)
	{
		return *this;
	}

	float * pTempTo = _pItems;
	float * pTempFrom = cArray._pItems;
	float * pTempToEnd = _pItems + _nSizeX + _nSizeY;
	for(; pTempTo < pTempToEnd; )
	{
		*pTempTo += *pTempFrom;
		pTempTo++; pTempFrom++;
		
	}
	
	return *this;
}

template<class TYPE> 
const MyArray2D<TYPE>& MyArray2D<TYPE>::operator-=(const MyArray2D<TYPE>& cArray)
{
	if (_nSizeX != cArray._nSizeX || _nSizeY != cArray._nSizeY)
	{
		return *this;
	}

	float * pTempTo = _pItems;
	float * pTempFrom = cArray._pItems;
	float * pTempToEnd = _pItems + _nSizeX + _nSizeY;
	for(; pTempTo < pTempToEnd; )
	{
		*pTempTo -= *pTempFrom;
		pTempTo++; pTempFrom++;
		
	}
	
	return *this;
}

template<class TYPE> 
MyArray2D<TYPE> MyArray2D<TYPE>::operator+(const MyArray2D<TYPE>& cArray) const
{
	if (_nSizeX != cArray._nSizeX || _nSizeY != cArray._nSizeY)
	{
		return *this;
	}

	MyArray2D<TYPE> cResult(*this);

	cResult += cArray;
	
	return cResult;
}

template<class TYPE> 
void MyArray2D<TYPE>::FindMaxMin(float &fMax, float &fMin) const
{
    if (_nSizeX > 0 && _nSizeY > 0)
	{
		fMax = _pItems[0,0];
		fMin = _pItems[0,0];

		float * pTemp = _pItems;
		float * pTempEnd = _pItems + _nSizeX * _nSizeY;
		for(; pTemp < pTempEnd; pTemp++)
		{
			if (*pTemp > fMax)
				fMax = *pTemp;
			if (*pTemp < fMin)
				fMin = *pTemp;
		}
    }
}

typedef MyArray2D<float> MyArray2DFloat;
typedef MyArray2D<int> MyArray2DInt;

class CCountry : public MyArray2D<float>
{
protected:
	float _fStepX;
	float _fStepY;
	float _fMaxHeight;

	int FindMaxHeight();
public:
	void Init(unsigned int nSizeX, unsigned int nSizeY, float fStepX, float fStepY);
	void Load(const char * pszFileName);
	void Save(const char * pszFileName);
    void CreateDamCountry(unsigned int nSize);
    void BlowUpDam();
    void CreateFlatLand(unsigned int nSize);

    void CreateCustomLand(unsigned int nSize);

	float GetStepX() const {return _fStepX;};
	float GetStepY() const {return _fStepY;};
    void SetStepX(float fStepX) {_fStepX  = fStepX;};
    void SetStepY(float fStepY) {_fStepY  = fStepY;};
	float GetMaxHeight() const {return _fMaxHeight;};
    float GetAverageHeight(const unsigned int iFromX, const unsigned int iFromY, 
                                 const unsigned int nSizeX, const unsigned int nSizeY) const;

    float GetMinimum(const unsigned int iFromX, const unsigned int iFromY, 
                                 const unsigned int nSizeX, const unsigned int nSizeY) const;

	virtual const CCountry& operator=(const CCountry& cCountry);
	
};



#endif

