#ifndef __BYRD_H__
#define __BYRD_H__

#include "arctic.h"
#include "QuadTree.h"

typedef struct _TSQUARE
{    
    unsigned int _iTopLeftX;
    unsigned int _iTopLeftY;
    unsigned int _nSizeX;
    unsigned int _nSizeY;
    float _fSizeX;
    float _fSizeY;
    float _fMaxCountryHeight;
    float _fMinCountryHeight;
    float _fWaterSurface;
    float _fMass;
    float _fAverageWaterHeight;
    float _pfMinBorderHeight[4];
    _TSQUARE * _ppBorders[4];
    float _pFlows[4];
} TSQUARE;

typedef CQuadTree<TSQUARE> CWaterQuadTree;
typedef CQuadTreeIterator<TSQUARE> CWaterQuadTreeIterator;

class CByrd : public CArctic
{
protected:
   CCountry * _pcCountry;
   MyArray2DFloat _cWaterHeight;
   MyArray2DFloat _cWaterMass;
   MyArray2DInt _cType;
   Sources * _ptSources;
   CWaterQuadTree _cQuadTree;
   TSQUARE _pFictiveBorders[4];
   unsigned int _nSimulations;

private:
    void Congregate(CWaterQuadTreeIterator& cIterator, TSQUARE& tParentSquare);
    void CongregateFlows(CWaterQuadTreeIterator& cIterator, float * pFlows);
    void CalculateWaterHeightFromQuadTree(CWaterQuadTreeIterator& cIterator);
    void SimulateLeaf(CWaterQuadTreeIterator& cIterator, float * pFlows);
    void SimulateLeaf2(CWaterQuadTreeIterator& cIterator);
    void SimulateLeaf3(float & fMinTime, CWaterQuadTreeIterator& cIterator, 
                          float * pDiffFlows);

    void SimulateLeaf4(CWaterQuadTreeIterator& cIterator, 
                          const float fSimTime);

    void CalculateNet(CWaterQuadTreeIterator& cIterator);
    
public:
   virtual void Init(CCountry * pcCountry);
   void Teselate(MyArray<unsigned int>& cZone);
   virtual void Simulate();
   virtual void SetSources(Sources *ptSources) { _ptSources = ptSources;};
   virtual const MyArray2DFloat& GetWaterHeight() const {return _cWaterHeight;};
   virtual const MyArray2DInt& GetType() const {return _cType;};
   virtual const MyArray2DFloat& GetMass() const {return _cWaterMass;};
   virtual void MoveTeselationToPoint(float x, float y) {};
};

#endif //__BYRD_H__