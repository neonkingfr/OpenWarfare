#ifndef __QUADTREE_H__
#define __QUADTREE_H__

template<class TYPE>
class CQuadTree;

template<class TYPE>
class CQuadTreeLeaf
{
private:
    unsigned int _iLevel;
    TYPE _pValues[4];
    CQuadTreeLeaf * _pChilds[4];
    CQuadTreeLeaf * _pParent;
    unsigned int _iParentIndex;

    CQuadTreeLeaf() : _iLevel(0), _iParentIndex(0), _pParent(NULL) {memset(_pChilds,0x0, 4 * sizeof(*_pChilds));};

public:  
    unsigned int GetLevel() const {return _iLevel;};
    
    TYPE * GetValues() {return _pValues;};

    CQuadTreeLeaf * GetChild(unsigned int iIndex) const {return _pChilds[iIndex];};
    CQuadTreeLeaf * GetParent() const {return _pParent;};
    unsigned int GetParentIndex() const {return _iParentIndex;};

    TYPE& operator[](unsigned int iIndex) {return _pValues[iIndex];};

    friend class CQuadTree<TYPE>;
};


template<class TYPE>
class CQuadTree 
{
public:
   CQuadTreeLeaf<TYPE> _tTopLeaf;
       
    ~CQuadTree() {DeleteChild(_tTopLeaf);};

    void DeleteChild(CQuadTreeLeaf<TYPE> &tLeaf);
    void CreateChild(CQuadTreeLeaf<TYPE> &tLeaf, unsigned int iChildIndex);
};


template<class TYPE>
class CQuadTreeIterator
{
private:
    CQuadTree<TYPE>& _cQuadTree;
    CQuadTreeLeaf<TYPE> * _ptActualLeaf;

public:
    CQuadTreeIterator(CQuadTree<TYPE>& cQuadTree) : _cQuadTree(cQuadTree) {_ptActualLeaf = &(cQuadTree._tTopLeaf);};


    void ToTop() {_ptActualLeaf =  &(_cQuadTree._tTopLeaf);};
    int ToParent();
    int ToChild(unsigned int iChildIndex);

    TYPE * GetValues() {return _ptActualLeaf->GetValues();}
    unsigned int GetParentIndex() {return _ptActualLeaf->GetParentIndex();};
    
    TYPE& operator[](unsigned int iIndex) {return (*_ptActualLeaf)[iIndex];};

    int IsChild(unsigned int iChildIndex) {return _ptActualLeaf->GetChild(iChildIndex) != NULL;};
    int IsParent();
    void DeleteChild(unsigned int iChildIndex);
    void CreateChild(unsigned int iChildIndex);
};
 
   
template<class TYPE>
void CQuadTree<TYPE>::DeleteChild(CQuadTreeLeaf<TYPE> &tLeaf)
{
    for(int i = 0; i < 4; i++)
    {
        if (tLeaf._pChilds[i] != NULL)
            DeleteChild(*(tLeaf._pChilds[i]));
    }

    if (tLeaf._pParent != NULL)
    {
        tLeaf._pParent->_pChilds[tLeaf._iParentIndex] = NULL;
        delete &tLeaf;
    }    
}

template<class TYPE>
void CQuadTree<TYPE>::CreateChild(CQuadTreeLeaf<TYPE> &tLeaf, unsigned int iChildIndex)
{
    if (tLeaf._pChilds[iChildIndex] == NULL)
    {
        CQuadTreeLeaf<TYPE> * pLeaf = new CQuadTreeLeaf<TYPE>;

        pLeaf->_pParent = &tLeaf;
        pLeaf->_iParentIndex = iChildIndex;

        tLeaf._pChilds[iChildIndex] = pLeaf;
    }
}

template<class TYPE>
int CQuadTreeIterator<TYPE>::ToParent()
{
    if (_ptActualLeaf->GetParent() != NULL)
    {
        _ptActualLeaf = _ptActualLeaf->GetParent();
        return 1;
    }
    return 0;
}
   
template<class TYPE>
int CQuadTreeIterator<TYPE>::ToChild(unsigned int iChildIndex)
{
    if (_ptActualLeaf->GetChild(iChildIndex)!= NULL)
    {
        _ptActualLeaf = _ptActualLeaf->GetChild(iChildIndex);
        return 1;
    }
    return 0;
}

template<class TYPE>
void CQuadTreeIterator<TYPE>::DeleteChild(unsigned int iChildIndex)
{
    if (_ptActualLeaf->GetChild(iChildIndex) != NULL)
        _cQuadTree.DeleteChild(*_ptActualLeaf->GetChild(iChildIndex));
}

template<class TYPE>
void CQuadTreeIterator<TYPE>::CreateChild(unsigned int iChildIndex)
{
    if (_ptActualLeaf->GetChild(iChildIndex) == NULL)
        _cQuadTree.CreateChild(*_ptActualLeaf,iChildIndex);
}

template<class TYPE>
int CQuadTreeIterator<TYPE>::IsParent()
{
    return _ptActualLeaf->GetParent() != NULL;
}


#endif //__QUADTREE_H__