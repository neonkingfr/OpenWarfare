#ifndef __MYARRAYWITHDELETE_H__
#define __MYARRAYWITHDELETE_H__

#include "MyArray.h"

template <class Type>
class MyArrayWithDelete : public MyArray<Type>
{
public:
    MyArrayWithDelete() : MyArray<Type>() {};
    void Delete(unsigned int iFromIndex, unsigned int nItems = 1);
};

template<class Type> 
void MyArrayWithDelete<Type>::Delete(unsigned int iFromIndex, unsigned int nItems /*= 1*/)
{
    ASSERT(iFromIndex + nItems <= _nSize);

    if (iFromIndex + nItems == _nSize)
    {
        _nSize = iFromIndex;
        return;
    }

    memcpy(_pField + iFromIndex, _pField + iFromIndex + nItems, sizeof(Type) * (_nSize - iFromIndex - nItems));

    _nSize -= nItems;
}

#endif //__MYARRAYWITHDELETE_H__