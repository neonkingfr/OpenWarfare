#ifndef __ARCTIC_H__
#define __ARCTIC_H__

#include "Shared.h"

typedef void (*ADDLINEFCN)(void * pInternalData, float fXFrom, float fYFrom, float fZFrom, 
                  float fXTo, float fYTo, float fZTo);


class CArctic 
{
  
public:  
  // Implemented in all childs
  virtual void Init(CCountry *pcCountry) = 0;
  virtual void Simulate() = 0;
  virtual void SetSources(Sources *ptSources) = 0;
  virtual const MyArray2DFloat& GetWaterHeight() const = 0;
  virtual const MyArray2DInt& GetType() const = 0;
  virtual const MyArray2DFloat& GetMass() const = 0;
  virtual void MoveTeselationToPoint(float x, float y, unsigned int iLowestLevel) = 0; 
  
  // Not implemented in all childs
  virtual void SetUpArrows(ADDLINEFCN fcnAddArrow, void * pFcnData) {};
  virtual void SetUpBorderLines(ADDLINEFCN fcnAddArrow, void * pFcnData,BOOL bVerticalLines = FALSE) {};
  virtual void CountryChanged() {};
  virtual void PrepareZoomedAndOffseted(unsigned int nSizeX, 
      unsigned int nSizeY, 
      unsigned int iZoomFactor, 
      unsigned int iOffsetX, 
      unsigned int iOffsetY) {};

  virtual void CreateWaterVertexes(
      CFlyShapeSimCoord& cShape,
      unsigned int nSizeX,
      unsigned int nSizeY,
      unsigned int iZoomLevel,
      unsigned int iOffsetX,
      unsigned int iOffsetY,
      const RGBQUAD& tColor) {};

  virtual void SaveWaterInPbl(LPCSTR dir, LPCSTR fileName, bool waterMass = false) {LogF("SaveWaterInPbl not supported");};

};

#endif //__ARCTIC_H__