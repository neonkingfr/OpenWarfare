#ifndef __BI_MATRIX_DP__
#define __BI_MATRIX_DP__

#include "BIMatrix.h"

/** \brief 4D matrix with double-precision position */
/** This matrix is similar to 3D matrix, but contains also another vector, which defined position */

template<class Real>
class __declspec( dllexport ) BI_TMatrix4DP : public BI_TMatrix3<Real>
{
  typedef BI_TMatrix3<Real> Base;

public:
  /** position of the matrix */
  BI_TVector3<double> _position;

public:
  /** \brief Contructor */
  BI_TMatrix4DP<Real>(const BI_TMatrix3<Real>& matrix, const BI_TVector3<double>& pos = BI_TVector3<double>(0,0,0) ) : Base(matrix), _position(pos) {}

  // constructing double precision matrix from float matrix
  BI_TMatrix4DP<Real>(const BI_TMatrix3<float>& matrix, const BI_TVector3<float>& pos = BI_TVector3<float>(0,0,0) ) : Base(matrix) 
  {
    _position[0] = pos[0];
    _position[1] = pos[1];
    _position[2] = pos[2];
  }

  // constructing double precision matrix from float matrix
  BI_TMatrix4DP<Real>(const BI_TMatrix4<float>& matrix ) : Base(matrix.Orientation()) 
  {
    _position[0] = matrix.GetPos(0);
    _position[1] = matrix.GetPos(1);
    _position[2] = matrix.GetPos(2);
  }


  /** \brief Contructor */
  BI_TMatrix4DP<Real>(Real x1 = 0, Real x2 = 0, Real x3 = 0,
    Real y1 = 0, Real y2 =0, Real y3 =0,
    Real z1 =0 , Real z2 =0, Real z3 =0,
    double p1 =0, double p2 =0, double p3 =0) : Base(x1,x2,x3,y1,y2,y3,z1,z2,z3), _position(p1,p2,p3) {}

	/** \brief Gets position vector */
	/** \param i i<sup>th</sup> item if the position vector */
	Real GetPos(int i) const { return _position[i]; }

	/** \brief Gets position coordinate vector */
	/** \param i i<sup>th</sup> item if the position vector */
	Real & GetPos(int i) { return _position[i]; }

  void Set(int axis, BI_TVector3<Real> vector)
  {
    Base::Set(axis,vector);
  }
	/** \brief Set method */
	Real& Set(int row, int column) 
	{  
		if (row == 3)
			return SetPos(column);
		else
			return Base::Set(row,column);
	}

	/** \brief Set cell */
	void Set(int row, int column, Real value) 
	{
		Set(row,column) = value;
	}

	/** \brief Sets position coordinate vector */
	Real& SetPos(int i) { return _position[i]; }

  void SetPosition(BI_TVector3<Real> pos) 
  { 
    _position[0] = pos[0];
    _position[1] = pos[1];
    _position[2] = pos[2];
  }

  void SetPosition(Real posX, Real posY, Real posZ) { _position[0] = posX; _position[1] = posY; _position[2] = posZ; }

  BI_TVector3<Real>  GetPosition() const
  {
    BI_TVector3<Real> v;
    v[0] = _position[0];
    v[1] = _position[1];
    v[2] = _position[2];
    return v;
  }
	
	/** \brief Gets cell */
	Real Get(int row, int column)const
	{
		return row == 3? GetPos(row):Base::Get(row,column);
	}

  BI_TMatrix3<Real>& Orientation()
  {
    return *this; //automatic conversion
  }
	BI_TMatrix3<Real> Orientation()const
	{
		return *this; //automatic conversion
	}

  bool operator==( const BI_TMatrix4<Real> other )const
  {
    return Base::operator==(other) && _position == other.GetPos();
  }
  bool operator!=( const BI_TMatrix4<Real> other )const
  {
    return Base::operator!=(other) && _position != other.GetPos();
  }
};

#endif