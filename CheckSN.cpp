
#include <tchar.h>
#include <windows.h>

#include "serial.hpp"

#if _VBS1
LPCTSTR regPath = _T ("Software\\BIS\\VBS1");
#else
LPCTSTR regPath = _T ("Software\\Codemasters\\Operation Flashpoint");
#endif

int CheckSN (int &res)
{
	HKEY regKey;
	if (RegOpenKey (HKEY_LOCAL_MACHINE, regPath, &regKey) != ERROR_SUCCESS)
	{
		return 1;
	}

	DWORD length = KEY_BYTES;
	unsigned char	cdKeyFromRegistry[KEY_BYTES];
	if (RegQueryValueEx (regKey, _T ("KEY"), 0, NULL, static_cast<BYTE*> (&cdKeyFromRegistry[0]), &length) != ERROR_SUCCESS)
	{
		RegCloseKey (regKey);
		return 1;
	}
	
	RegCloseKey (regKey);

	CDKey cdKey;
#if _VBS1
	static const unsigned char publicKey[] =
	{
		0x11, 0x00, 0x00, 0x00,
		0xF9, 0x54, 0x9E, 0x33, 0x73, 0xCD, 0x0F, 0x5A, 0x15, 0x85, 0xE2, 0xAC, 0x32, 0x80, 0xA1
	};
	const char *check = "VBSystem 1";
#else
	static const unsigned char publicKey[] =
	{
		0x13, 0x00, 0x00, 0x00,
		0xF7, 0xB0, 0x64, 0xD7, 0xDB, 0x87, 0x52, 0x27, 0x57, 0x91, 0xB1, 0x9E, 0x0E, 0x81, 0x88
	};
	const char *check = "Flashpoint";
#endif

	cdKey.Init(cdKeyFromRegistry, publicKey);
	if (!cdKey.Check(5, check))
	{
		res = 0;
		return 0;
	}

	__int64 value = cdKey.GetValue (0, 5);
	if (value > 110000000 || value > 10000000 && value < 100000000)
	{
		res = 0;
		return 0;
	}

	__int64 warezIds[] = 
	{
		6787,
		16344,
		33774,
		140290,
		155635,
		175466,
		221923,
		518635,
		2805325,
		3165452,
		4781374471,
		8535059025,
	};

	int n = sizeof (warezIds) / sizeof (warezIds[0]);
	for (int i = 0; i < n; ++i)
	{
		if (value == warezIds[i])
		{
			res = 0;
			return 0;
		}
	}

	res = 1;
	return 0;
}

int WINAPI WinMain(
  HINSTANCE hInstance,      // handle to current instance
  HINSTANCE hPrevInstance,  // handle to previous instance
  LPSTR lpCmdLine,          // command line
  int nCmdShow              // show state
)
{
	int res;
	int errCode = CheckSN (res);
	if (errCode)
	{
		return errCode;
	}

	HKEY regKey;
	if (RegOpenKey (HKEY_LOCAL_MACHINE, regPath, &regKey) != ERROR_SUCCESS)
	{
		return 1;
	}

	if (RegSetValueEx (regKey, _T ("KEY_NOTE"), 0, REG_DWORD, reinterpret_cast<CONST BYTE*> (&res), sizeof (res)) != ERROR_SUCCESS)
	{
		RegCloseKey (regKey);
		return 1;
	}

	RegCloseKey (regKey);

	return 0;
}
