#include <Es/Framework/consoleBase.h>
#include <Es/Files/fileContainer.hpp>
#include <Es/Strings/bString.hpp>
#include <El/ParamArchive/paramArchive.hpp>
#include <El/ParamArchive/paramArchiveDb.hpp>
#include <El/ParamFile/paramFile.hpp>
#include <El/Enum/enumNames.hpp>
#include <El/Math/math3d.hpp>
#include <El/Color/colors.hpp>
#include <stdio.h>
#include "multiPassMat.hpp"


////////////////

class IsTextureFile
{
  public:
  typedef FileItem Argument;
  
  bool operator () (const FileItem &name) const
  {
    if (name.directory) return true;
    const char *ext = GetFileExt(name.filename);
    return !strcmpi(ext,".paa") || !strcmpi(ext,".pac");
  }
};

static inline float Bilint(
  float y00, float y01, float y10, float y11,
  float xf, float zf
)
{
  // y00 top-left
  // y01 bottom-left
  // y10 top-right
  // y11 bottom-right
  float y0z = y00*(1-zf) + y01*zf;
  float y1z = y10*(1-zf) + y11*zf;
  return y0z*(1-xf) + y1z*xf;
}

static bool ConvertTextureToMultipass(const FileItem &file, int context)
{
  AutoArray<RString> passes;
  // based on a texture name we create a multipass material
  // we assume name has form AABBCCDD
  const char *name = file.filename;
  const char *ext = GetFileExt(name);
  // create a material file based on passes
  Ref<MultipassMaterial> mat = new MultipassMaterial;
  if (ext<name+8)
  {
    // simple material, create one pass, based on full name
    char shortName[256];
    GetFilename(shortName,name);

    SinglePassInfo info;
    info.texture = file.path+file.filename;
    info.material = file.path+RString(shortName)+RString(".rvmat");
    info.tlAlpha = 1;
    info.trAlpha = 1;
    info.blAlpha = 1;
    info.brAlpha = 1;
    mat->_passes.Add(info);
  }
  else
  {
    int len = ext-name;
    Assert(len>=8);
    // TL TR BL BR
    static const float factorsAll[4][4]=
    {
      {1,0,0,0},
      {0,1,0,0},
      {0,0,1,0},
      {0,0,0,1}
    };
    for (int i=0; i<4; i++)
    {
      const float *factors = factorsAll[i];
      char simpleName[3];
      strncpy(simpleName,name+i*2,2);
      simpleName[2]=0;
      SinglePassInfo info;
      info.tlAlpha = Bilint(factors[0],factors[2],factors[1],factors[3],0,0);
      info.trAlpha = Bilint(factors[0],factors[2],factors[1],factors[3],1,0);
      info.blAlpha = Bilint(factors[0],factors[2],factors[1],factors[3],0,1);
      info.brAlpha = Bilint(factors[0],factors[2],factors[1],factors[3],1,1);
      info.texture = file.path+RString(simpleName)+RString(".paa");
      info.material = file.path+RString(simpleName)+RString(".rvmat");
      mat->AddPass(info);
    }
  }
  mat->Close();
  ParamArchiveSave ar(0);
  mat->Serialize(ar);
  char tgtName[256];
  strcpy(tgtName,file.path);
  strcat(tgtName,file.filename);
  strcpy(GetFileExt(GetFilenameExt(tgtName)),".bimpas");
  ar.Save(tgtName);
  return false; // traverse all, never stop
}
int consoleMain( int argc, const char *argv[] )
{
  // scan directory - for each .paa or .pac file create a corresponding .bimpas file
  argv++,argc--;
  if (argc!=1)
  {
    printf(
      "Usage:\n"
      "\n"
      "multiPassTex sourceFolder\n"
      "\n"
      "Description:\n"
      "\n"
      "For each texture (.pac or .paa) file in the source folder corresponding\n"
      "multipass material (.bimpas) is created. File name is assumed to take a form \n"
      "TlTrBlBr, where Tl, Tr, Bl, Br are basic non-blended texture names placed\n"
      "top-left, top-rigth, bottom-left, bottom-right."
    );
    return 1;
  }
  const char *folder = argv[0];
  int context;
  ForSomeFileR(folder,IsTextureFile(),ConvertTextureToMultipass,context);
  
  return 0;
}
