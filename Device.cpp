#include "Device.h"

namespace Framework
{
    Device::Device(bool windowed)
        : _p3d(0), _device(0), _pParams(0), _pDisMode(0), _adapter(0), _deviceLost(false)
    {
        _p3d = Direct3DCreate9( D3D_SDK_VERSION );

        if (_p3d == NULL)
        {
            MessageBox(NULL, "Direct3 9 Creation Fail !!!", "Fatal error", MB_OK);

            throw "Direct3 9 Creation Fail";
        }

        // get current display mode
        _pDisMode = new D3DDISPLAYMODE();
        _p3d->GetAdapterDisplayMode(_adapter, _pDisMode);

        // set all attributes to zero
        _pParams = new D3DPRESENT_PARAMETERS();
        memset(_pParams, 0, sizeof(D3DPRESENT_PARAMETERS));

        // early fill of present parameters
        _pParams->Windowed = windowed;
        _pParams->BackBufferFormat = D3DFMT_X8R8G8B8;
    }

    Device::Device(UINT16 width, UINT16 height, bool windowed, D3DFORMAT backBufferFormat)
        : _p3d(0), _device(0), _pParams(0), _pDisMode(0), _adapter(0), _deviceLost(false)
    {
        _p3d = Direct3DCreate9( D3D_SDK_VERSION );

        if (_p3d == NULL)
        {
            MessageBox(NULL, "Direct3 9 Creation Fail !!!", "Fatal error", MB_OK);

            throw "Direct3DX 9 creation fail";
        }

        // current display mode
        _pDisMode = new D3DDISPLAYMODE();
        _p3d->GetAdapterDisplayMode(_adapter, _pDisMode);

        // early fill of present parameters
        _pParams = new D3DPRESENT_PARAMETERS();
        memset(_pParams, 0, sizeof(D3DPRESENT_PARAMETERS));

        _pParams->Windowed = windowed;
        _pParams->BackBufferWidth = width;
        _pParams->BackBufferHeight = height;
        _pParams->BackBufferFormat = backBufferFormat;

        if (!_pParams->Windowed)
            _pParams->FullScreen_RefreshRateInHz = _pDisMode->RefreshRate;
    }

    Device::~Device(void)
    {
        if (_device)
            _device->Release();

        _device = 0;

        if (_p3d)
            _p3d->Release();

        _p3d = 0;

        if (_pParams)
            delete _pParams;

        _pParams = 0;

        if (_pDisMode)
            delete _pDisMode;

        _pDisMode = 0;
    }

    bool Device::TryReset(void)
    {
        HRESULT hr = _device->TestCooperativeLevel();

        if (hr == D3DERR_DEVICENOTRESET)
        {
            if (_device->Reset(_pParams) == D3D_OK)
            {
                if (_device->TestCooperativeLevel() == D3D_OK)
                {
                    _deviceLost = false;

                    return true;
                }
            }
        }

        return false;
    }

    bool Device::CreateDevice(HWND hWnd)
    {
        // device type selection
        D3DDEVTYPE devType = SelectDeviceType();

        // create flags selection
        DWORD cFlags = SelectBehaviorFlags(devType);

        _pParams->BackBufferCount = 1;
        _pParams->MultiSampleQuality = 0;
        _pParams->hDeviceWindow = hWnd;
        _pParams->SwapEffect = D3DSWAPEFFECT_DISCARD;
        _pParams->MultiSampleType = D3DMULTISAMPLE_NONE;
        _pParams->PresentationInterval = D3DPRESENT_INTERVAL_IMMEDIATE;

#ifdef _DEBUG
        _pParams->PresentationInterval = D3DPRESENT_INTERVAL_ONE;
		cFlags = D3DCREATE_SOFTWARE_VERTEXPROCESSING;
#endif

        _pParams->EnableAutoDepthStencil = true;
        _pParams->AutoDepthStencilFormat = SelectDepthFormat(devType, _pDisMode->Format, _pParams->BackBufferFormat);

        // device sreation
        if ( SUCCEEDED(_p3d->CreateDevice(_adapter, devType, hWnd, cFlags, _pParams, &_device)) )
            return true;
        else
            return false;
    }

    D3DFORMAT Device::SelectDepthFormat(D3DDEVTYPE devType, D3DFORMAT adapterFmt, D3DFORMAT renderFmt)
    {
        const D3DFORMAT depthFormat[] = 
        { 
            D3DFMT_D32, 
            D3DFMT_D32_LOCKABLE, 
            D3DFMT_D24S8, 
            D3DFMT_D24X8, 
            D3DFMT_D24X4S4, 
            D3DFMT_D16_LOCKABLE
        };

        for (int i = 0; i < 6; i++)
        {
            if ( SUCCEEDED(_p3d->CheckDepthStencilMatch(_adapter, devType, adapterFmt, renderFmt, depthFormat[i])) )
                return depthFormat[i];
        }

        return D3DFMT_D16;
    }

    D3DDEVTYPE Device::SelectDeviceType(void)
    {
        const D3DDEVTYPE dType[] = 
        { 
            D3DDEVTYPE_HAL, 
            D3DDEVTYPE_SW, 
            D3DDEVTYPE_REF 
        };

        HRESULT hr;

        for (int i = 0; i < 3; i ++)
        {
            hr = _p3d->CheckDeviceType(_adapter, dType[i], _pDisMode->Format, D3DFMT_X8R8G8B8, _pParams->Windowed);

            if (SUCCEEDED ( hr) )
                return dType[i];
        }

        return D3DDEVTYPE_NULLREF;
    }

    DWORD Device::SelectBehaviorFlags(D3DDEVTYPE devType)
    {
        D3DCAPS9 caps;
        _p3d->GetDeviceCaps(_adapter, devType, &caps);

        // minimum supported
        DWORD result = D3DCREATE_SOFTWARE_VERTEXPROCESSING;

        if ( ( (D3DDEVCAPS_HWTRANSFORMANDLIGHT & caps.DevCaps ) != 0) && ( caps.VertexShaderVersion >= D3DVS_VERSION(2, 0) ) )
            result = D3DCREATE_HARDWARE_VERTEXPROCESSING;

        if (D3DDEVCAPS_PUREDEVICE & caps.DevCaps)
            result |= D3DCREATE_PUREDEVICE;

        return result;
    }
}