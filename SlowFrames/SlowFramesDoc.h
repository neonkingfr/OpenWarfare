// SlowFramesDoc.h : interface of the CSlowFramesDoc class
//


#pragma once
#include "SlowFramesSet.h"
#include "afxwin.h"

class CSlowFramesDoc : public CDocument
{
protected: // create from serialization only
	CSlowFramesDoc();
	DECLARE_DYNCREATE(CSlowFramesDoc)

// Attributes
public:
	CSlowFramesSet m_SlowFramesSet;

// Operations
public:

// Overrides
	public:
	virtual BOOL OnNewDocument();

// Implementation
public:
	virtual ~CSlowFramesDoc();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

protected:

// Generated message map functions
protected:
	DECLARE_MESSAGE_MAP()
public:
  afx_msg void OnLogImport();
  afx_msg void OnLogRemove();
};


#pragma once


// CLogFileDialog dialog

class CLogFileDialog : public CDialog
{
	DECLARE_DYNCREATE(CLogFileDialog)

public:
	CLogFileDialog(CWnd* pParent = NULL);   // standard constructor
	virtual ~CLogFileDialog();

// Dialog Data
	enum { IDD = IDD_LOG_FILE, IDH = IDR_HTML_LOGFILEDIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();

	DECLARE_MESSAGE_MAP()
public:
  CListBox _list;

  CString GetLogFile() const;
  CString _logFile;
};
