// SlowFramesSet.h: interface of the CSlowFramesSet class
//


#pragma once

// code generated on 1. ��jna 2004, 8:11

class CSlowFramesSet : public CRecordset
{
public:
	CSlowFramesSet(CDatabase* pDatabase = NULL);
	DECLARE_DYNAMIC(CSlowFramesSet)

// Field/Param Data

// The string types below (if present) reflect the actual data type of the
// database field - CStringA for ANSI datatypes and CStringW for Unicode
// datatypes. This is to prevent the ODBC driver from performing potentially
// unnecessary conversions.  If you wish, you may change these members to
// CString types and the ODBC driver will perform all necessary conversions.
// (Note: You must use an ODBC driver version that is version 3.5 or greater
// to support both Unicode and these conversions).

  long m_Id;
  CString	m_Campaign;
	CString	m_Mission;
	CString	m_Build;
	CString	m_Situation;
	CString	m_Cause;
	CString	m_AssignedTo;
	CString	m_Status;
	CString	m_LogFile;
	long	m_LogLine;
	CString	m_FrameMinTitles;
	CString	m_FrameMinValues;
	CString	m_FrameAvgTitles;
	CString	m_FrameAvgValues;
  long	m_MaxTotal;
  long	m_MaxDraw;
  long	m_MaxSimul;
  long	m_MaxAI;
  long	m_AvgTotal;
  long	m_AvgDraw;
  long	m_AvgSimul;
  long	m_AvgAI;

// Overrides
	// Wizard generated virtual function overrides
	public:
	virtual CString GetDefaultConnect();	// Default connection string

	virtual CString GetDefaultSQL(); 	// default SQL for Recordset
	virtual void DoFieldExchange(CFieldExchange* pFX);	// RFX support

// Implementation
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

};

