#include "stdafx.h"
#include "El/elementpch.hpp"
#include "Es/Strings/rString.hpp"

struct SlowFrameItem
{
  RString name;
  int value;
};
TypeIsMovableZeroed(SlowFrameItem)

class SlowFrame : public AutoArray<SlowFrameItem>
{
public:
  int GetValue(RString name);
};

int SlowFrame::GetValue(RString name)
{
  for (int i=0; i<Size(); i++)
  {
    if (stricmp(Get(i).name, name) == 0) return Get(i).value;
  }
  return 0;
}

static void AnalyzeFrame(AutoArray<SlowFrameItem> &frame, const char *titles, const char *values)
{
  Assert(strlen(titles) == strlen(values));
  int n = strlen(titles) / 6;
  const char *ptrt = titles;
  const char *ptrv = values;
  frame.Realloc(n);
  frame.Resize(n);
  for (int i=0; i<n; i++)
  {
    RString value(ptrv, 6);
    int val = 0;
    for (const char *ptr=ptrv; ptr < ptrv + 6 && *ptr; ptr++)
    {
      int c = *ptr;
      if (isdigit(c))
      {
        val *= 10;
        val += c - '0';
      }
      else if (c == 'k' || c == 'K')
      {
        val *= 1000;
      }
    }
    const char *p = ptrt;
    while (p < ptrt + 6 && *p && isspace(*p)) p++;
    const char *pb = p;
    while (p < ptrt + 6 && *p && !isspace(*p)) p++;

    frame[i].name = RString(pb, p-pb);
    frame[i].value = val;
    ptrt += 6;
    ptrv += 6;
  }
}

struct SlowFrameCoef
{
  RString name;
  RString displayName;
  float coef;
};

const float invWantedFrame = 1.0f / 5000.0f;

SlowFrameCoef rootWanted[] =
{
  // base scopes
  {"wDraw", "Drawing", 2500 * invWantedFrame},
  {"wSimA", "Simulation", 1500 * invWantedFrame},
  {"aiAll", "AI", 800 * invWantedFrame},

  // additional scopes
  {"siScr", "Scripts", 200 * invWantedFrame},
  {"hudDr", "HUD drawing", 200 * invWantedFrame},
  {"sound", "Sounds", 200 * invWantedFrame},
  {"wsSet", "Landscape and UI simulation", 100 * invWantedFrame},
  {"drwFn", "FinishDraw and diagnostics", 100 * invWantedFrame},

  // time manager scopes
  {"visUA", "Visibility update", 300 * invWantedFrame},
  {"aiMap", "AI Map update", 300 * invWantedFrame}
};

SlowFrameCoef aiWanted[] =
{
  {"pthSt", "AI - Strategic path", 0.3f},
  {"pthOp", "AI - Operative path", 0.3f},
  {"aiGCT", "AI - Target list update", 0.3f},
  {"aiGAT", "AI - Assign targets", 0.1f}
};

static int SelectWorseScope(SlowFrame &frame, SlowFrameCoef *wanted, int count, float invFrame)
{
  int index = -1;
  float maxDiff = 0;
  for (int i=0; i<count; i++)
  {
    RString name = wanted[i].name;
    int value = frame.GetValue(name);
    float coef = value * invFrame;
    float diff = coef - wanted[i].coef;
    if (diff > maxDiff)
    {
      maxDiff = diff;
      index = i;
    }
  }
  return index;
}

CString IdentifyCause(const char *titles, const char *values)
{
  SlowFrame frame;
  AnalyzeFrame(frame, titles, values);
  
  if (frame.Size() == 0) return "Ignore";

  int timeMax = frame[0].value;
  if (timeMax <= 5000) return "Ignore";

  float invFrame = 1.0f / timeMax;

  int rootScope = SelectWorseScope(frame, rootWanted, lenof(rootWanted), invFrame);
  if (rootScope < 0) return "";

  // next levels
  if (stricmp(rootWanted[rootScope].name, "aiAll") == 0)
  {
    // AI
    float invFrame = 1.0f / frame.GetValue("aiAll");
    int aiScope = SelectWorseScope(frame, aiWanted, lenof(aiWanted), invFrame);
    if (aiScope >= 0) return (const char *)aiWanted[aiScope].displayName;
    // else return root scope
  }
  else if (stricmp(rootWanted[rootScope].name, "wDraw") == 0)
  {
    // Drawing
  }
  
  return (const char *)rootWanted[rootScope].displayName;
}

void GetValues(const char *titles, const char *values, int &total, int &draw, int &simul, int &ai)
{
  SlowFrame frame;
  AnalyzeFrame(frame, titles, values);

  total = 0; draw = 0; simul = 0; ai = 0;
  if (frame.Size() == 0) return;
  
  total = frame[0].value;
  draw = frame.GetValue("wDraw");
  simul = frame.GetValue("wSimA");
  ai = frame.GetValue("aiAll");
}
