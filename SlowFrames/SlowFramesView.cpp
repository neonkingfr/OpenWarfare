// SlowFramesView.cpp : implementation of the CSlowFramesView class
//

#include "stdafx.h"
#include "SlowFrames.h"

#include "SlowFramesSet.h"
#include "SlowFramesDoc.h"
#include "SlowFramesView.h"
#include ".\slowframesview.h"

#include "El/elementpch.hpp"
#include "Es/Strings/rString.hpp"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

class CStatusSet : public CRecordset
{
public:
  CStatusSet(CDatabase* pDatabase = NULL);
  DECLARE_DYNAMIC(CStatusSet)

  // Field/Param Data
  CString	m_Status;

  // Overrides
  // Wizard generated virtual function overrides
public:
  virtual CString GetDefaultSQL() {return _T("[States]");}
  virtual void DoFieldExchange(CFieldExchange* pFX);	// RFX support
};

IMPLEMENT_DYNAMIC(CStatusSet, CRecordset)

CStatusSet::CStatusSet(CDatabase* pdb)
: CRecordset(pdb)
{
  m_Status = L"";
  m_nFields = 1;
  m_nDefaultType = snapshot;
}

void CStatusSet::DoFieldExchange(CFieldExchange* pFX)
{
  pFX->SetFieldType(CFieldExchange::outputColumn);
  RFX_Text(pFX, _T("[Status]"), m_Status);
}

// CSlowFramesView

IMPLEMENT_DYNCREATE(CSlowFramesView, CRecordView)

BEGIN_MESSAGE_MAP(CSlowFramesView, CRecordView)
	// Standard printing commands
	ON_COMMAND(ID_FILE_PRINT, CRecordView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_DIRECT, CRecordView::OnFilePrint)
	ON_COMMAND(ID_FILE_PRINT_PREVIEW, CRecordView::OnFilePrintPreview)
  ON_WM_SIZE()
  ON_NOTIFY(LVN_ITEMCHANGED, IDC_LIST, OnLvnItemchangedList)
  ON_EN_KILLFOCUS(IDC_ASSIGNED_TO, OnEnKillfocusAssignedTo)
  ON_COMMAND(ID_IDENTIFY_CAUSE, OnIdentifyCause)
  ON_COMMAND(ID_IDENTIFY_CAUSES_ALL, OnIdentifyCausesAll)
  ON_COMMAND(ID_FILTER, OnFilter)
  ON_CBN_SELENDOK(IDC_STATUS, OnCbnSelendokStatus)
  ON_NOTIFY(LVN_COLUMNCLICK, IDC_LIST, OnLvnColumnclickList)
END_MESSAGE_MAP()

// CSlowFramesView construction/destruction

CSlowFramesView::CSlowFramesView()
	: CRecordView(CSlowFramesView::IDD)
{
	m_pSet = NULL;

  _sortColumn = -1;
  _sortDescending = false;
}

CSlowFramesView::~CSlowFramesView()
{
}

void CSlowFramesView::DoDataExchange(CDataExchange* pDX)
{
  CRecordView::DoDataExchange(pDX);
  // you can insert DDX_Field* functions here to 'connect' your controls to the database fields, ex.
  // DDX_FieldText(pDX, IDC_MYEDITBOX, m_pSet->m_szColumn1, m_pSet);
  // DDX_FieldCheck(pDX, IDC_MYCHECKBOX, m_pSet->m_bColumn2, m_pSet);
  // See MSDN and ODBC samples for more information
  DDX_Control(pDX, IDC_LIST, _list);

  DDX_FieldText(pDX, IDC_ASSIGNED_TO, m_pSet->m_AssignedTo, m_pSet);
  DDX_FieldCBStringExact(pDX, IDC_STATUS, m_pSet->m_Status, m_pSet);
  DDX_FieldText(pDX, IDC_LOG_FILE, m_pSet->m_LogFile, m_pSet);
  DDX_FieldText(pDX, IDC_LOG_LINE, m_pSet->m_LogLine, m_pSet);

  DDX_FieldText(pDX, IDC_LINE1, m_pSet->m_FrameMinTitles, m_pSet);
  DDX_FieldText(pDX, IDC_LINE2, m_pSet->m_FrameMinValues, m_pSet);
  DDX_FieldText(pDX, IDC_LINE3, m_pSet->m_FrameAvgTitles, m_pSet);
  DDX_FieldText(pDX, IDC_LINE4, m_pSet->m_FrameAvgValues, m_pSet);
  DDX_Control(pDX, IDC_LINE1, _line1);
  DDX_Control(pDX, IDC_LINE2, _line2);
  DDX_Control(pDX, IDC_LINE3, _line3);
  DDX_Control(pDX, IDC_LINE4, _line4);
  DDX_Control(pDX, IDC_ASSIGNED_TO_TITLE, _assignedToTitle);
  DDX_Control(pDX, IDC_STATUS_TITLE, _statusTitle);
  DDX_Control(pDX, IDC_LOG_FILE_TITLE, _logFileTitle);
  DDX_Control(pDX, IDC_LOG_LINE_TITLE, _logLineTitle);
  DDX_Control(pDX, IDC_ASSIGNED_TO, _assignedTo);
  DDX_Control(pDX, IDC_STATUS, _status);
  DDX_Control(pDX, IDC_LOG_FILE, _logFile);
  DDX_Control(pDX, IDC_LOG_LINE, _logLine);
  DDX_Control(pDX, IDC_MIN_FRAME, _minFrame);
  DDX_Control(pDX, IDC_AVG_FRAME, _avgFrame);
}

BOOL CSlowFramesView::PreCreateWindow(CREATESTRUCT& cs)
{
	// TODO: Modify the Window class or styles here by modifying
	//  the CREATESTRUCT cs

	return CRecordView::PreCreateWindow(cs);
}

void CSlowFramesView::SelectRecord(long id)
{
  Assert(m_pSet->IsOpen());

  char buffer[256];
  sprintf(buffer, "Id = %d", id);
  m_pSet->m_strFilter = buffer;
  
  m_pSet->Requery();
  UpdateData(FALSE);
}

void CSlowFramesView::OnInitialUpdate()
{
  m_pSet = &GetDocument()->m_SlowFramesSet;
  CRecordView::OnInitialUpdate();

  CStatusSet set(m_pSet->m_pDatabase);
  set.Open();
  while (!set.IsEOF())
  {
    _status.AddString(set.m_Status);
    set.MoveNext();
  }

  // Controls initialization
  _font.CreateFont(-12, 0, 0, 0, FW_NORMAL, FALSE, FALSE, 0, ANSI_CHARSET,
    OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_MODERN, "Courier New");
  _line1.SetFont(&_font);
  _line2.SetFont(&_font);
  _line3.SetFont(&_font);
  _line4.SetFont(&_font);

  _list.InsertColumn(0, "Campaign", LVCFMT_LEFT, 80);
  _list.InsertColumn(1, "Mission", LVCFMT_LEFT, 160);
  _list.InsertColumn(2, "Build", LVCFMT_LEFT, 120);
  _list.InsertColumn(3, "Situation", LVCFMT_LEFT, 80);
  _list.InsertColumn(4, "Cause", LVCFMT_LEFT, 160);
  _list.InsertColumn(5, "Assigned to", LVCFMT_LEFT, 80);
  _list.InsertColumn(6, "Status", LVCFMT_LEFT, 80);
  _list.InsertColumn(7, "Max Total", LVCFMT_LEFT, 60);
  _list.InsertColumn(8, "Max Draw", LVCFMT_LEFT, 60);
  _list.InsertColumn(9, "Max Simul", LVCFMT_LEFT, 60);
  _list.InsertColumn(10, "Max AI", LVCFMT_LEFT, 60);
  _list.InsertColumn(11, "Avg Total", LVCFMT_LEFT, 60);
  _list.InsertColumn(12, "Avg Draw", LVCFMT_LEFT, 60);
  _list.InsertColumn(13, "Avg Simul", LVCFMT_LEFT, 60);
  _list.InsertColumn(14, "Avg AI", LVCFMT_LEFT, 60);

  UpdateList();
}

void CSlowFramesView::OnUpdate(CView* pSender, LPARAM lHint, CObject* pHint)
{
  CRecordView::OnUpdate(pSender, lHint, pHint);
  UpdateList();
}

void CSlowFramesView::UpdateList()
{
  m_pSet->m_strFilter = _filter;
  m_pSet->Requery();

  long id = 0;
  POSITION pos = _list.GetFirstSelectedItemPosition();
  if (pos != NULL)
  {
    int item = _list.GetNextSelectedItem(pos);
    id = (long)_list.GetItemData(item);
  }

  _list.DeleteAllItems();

  if (m_pSet->IsEOF())
  {
    _assignedTo.EnableWindow(FALSE);
    _status.EnableWindow(FALSE);
    return;
  }

  m_pSet->MoveFirst();
  int row = 0;
  int selected = 0;
  while (!m_pSet->IsEOF())
  {
    _list.InsertItem(row, "");
    _list.SetItemData(row, (DWORD_PTR)m_pSet->m_Id);
    _list.SetItem(row, 0, LVIF_TEXT, m_pSet->m_Campaign, -1, 0, 0, 0);
    _list.SetItem(row, 1, LVIF_TEXT, m_pSet->m_Mission, -1, 0, 0, 0);
    _list.SetItem(row, 2, LVIF_TEXT, m_pSet->m_Build, -1, 0, 0, 0);
    _list.SetItem(row, 3, LVIF_TEXT, m_pSet->m_Situation, -1, 0, 0, 0);
    _list.SetItem(row, 4, LVIF_TEXT, m_pSet->m_Cause, -1, 0, 0, 0);
    _list.SetItem(row, 5, LVIF_TEXT, m_pSet->m_AssignedTo, -1, 0, 0, 0);
    _list.SetItem(row, 6, LVIF_TEXT, m_pSet->m_Status, -1, 0, 0, 0);
    _list.SetItem(row, 7, LVIF_TEXT, Format("%d", m_pSet->m_MaxTotal), -1, 0, 0, 0);
    _list.SetItem(row, 8, LVIF_TEXT, Format("%d", m_pSet->m_MaxDraw), -1, 0, 0, 0);
    _list.SetItem(row, 9, LVIF_TEXT, Format("%d", m_pSet->m_MaxSimul), -1, 0, 0, 0);
    _list.SetItem(row, 10, LVIF_TEXT, Format("%d", m_pSet->m_MaxAI), -1, 0, 0, 0);
    _list.SetItem(row, 11, LVIF_TEXT, Format("%d", m_pSet->m_AvgTotal), -1, 0, 0, 0);
    _list.SetItem(row, 12, LVIF_TEXT, Format("%d", m_pSet->m_AvgDraw), -1, 0, 0, 0);
    _list.SetItem(row, 13, LVIF_TEXT, Format("%d", m_pSet->m_AvgSimul), -1, 0, 0, 0);
    _list.SetItem(row, 14, LVIF_TEXT, Format("%d", m_pSet->m_AvgAI), -1, 0, 0, 0);
    if (m_pSet->m_Id == id) selected = row;
    m_pSet->MoveNext();
    row++;
  }
  _list.EnsureVisible(selected, FALSE);
  _list.SetItemState(selected, LVIS_SELECTED, LVIS_SELECTED);
  
  id = (long)_list.GetItemData(selected);
  SelectRecord(id);

  _assignedTo.EnableWindow(TRUE);
  _status.EnableWindow(TRUE);
}

BOOL CSlowFramesView::OnMove(UINT nIDMoveCommand)
{
  // not supported
  return FALSE;
}

void CSlowFramesView::OnSize(UINT nType, int cx, int cy)
{
  CRecordView::OnSize(nType, cx, cy);

  if (nType == SIZE_MINIMIZED) return;

  HDWP handle = BeginDeferWindowPos(15);
  const int h2 = 146;

  int top = 7;
  int bottom = cy - 7 - h2 - 6;

  DeferWindowPos(handle, _list.GetSafeHwnd(), NULL, 7, top, cx - 14, bottom - top, SWP_NOZORDER);
  
  top = cy - 7 - h2;

  DeferWindowPos(handle, _assignedToTitle.GetSafeHwnd(), NULL, 7, top, 100, 14, SWP_NOZORDER);
  DeferWindowPos(handle, _statusTitle.GetSafeHwnd(), NULL, 107, top, 100, 14, SWP_NOZORDER);
  DeferWindowPos(handle, _logFileTitle.GetSafeHwnd(), NULL, cx - 7 - 280, top, 240, 14, SWP_NOZORDER);
  DeferWindowPos(handle, _logLineTitle.GetSafeHwnd(), NULL, cx - 7 - 40, top, 40, 14, SWP_NOZORDER);
  top += 14 + 2;
  DeferWindowPos(handle, _assignedTo.GetSafeHwnd(), NULL, 7, top, 100, 20, SWP_NOZORDER);
  DeferWindowPos(handle, _status.GetSafeHwnd(), NULL, 107, top, 100, 20, SWP_NOZORDER);
  DeferWindowPos(handle, _logFile.GetSafeHwnd(), NULL, cx - 7 - 280, top, 240, 20, SWP_NOZORDER);
  DeferWindowPos(handle, _logLine.GetSafeHwnd(), NULL, cx - 7 - 40, top, 40, 20, SWP_NOZORDER);
  top += 20 + 8;

  DeferWindowPos(handle, _minFrame.GetSafeHwnd(), NULL, 7, top, cx - 14, 14, SWP_NOZORDER);
  top += 14 + 2;
  DeferWindowPos(handle, _line1.GetSafeHwnd(), NULL, 7, top, cx - 14, 14, SWP_NOZORDER);
  top += 14 + 2;
  DeferWindowPos(handle, _line2.GetSafeHwnd(), NULL, 7, top, cx - 14, 14, SWP_NOZORDER);
  top += 14 + 8;

  DeferWindowPos(handle, _avgFrame.GetSafeHwnd(), NULL, 7, top, cx - 14, 14, SWP_NOZORDER);
  top += 14 + 2;
  DeferWindowPos(handle, _line3.GetSafeHwnd(), NULL, 7, top, cx - 14, 14, SWP_NOZORDER);
  top += 14 + 2;
  DeferWindowPos(handle, _line4.GetSafeHwnd(), NULL, 7, top, cx - 14, 14, SWP_NOZORDER);
  top += 14 + 2;

  EndDeferWindowPos(handle);
}

// CSlowFramesView printing

BOOL CSlowFramesView::OnPreparePrinting(CPrintInfo* pInfo)
{
	// default preparation
	return DoPreparePrinting(pInfo);
}

void CSlowFramesView::OnBeginPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add extra initialization before printing
}

void CSlowFramesView::OnEndPrinting(CDC* /*pDC*/, CPrintInfo* /*pInfo*/)
{
	// TODO: add cleanup after printing
}


// CSlowFramesView diagnostics

#ifdef _DEBUG
void CSlowFramesView::AssertValid() const
{
	CRecordView::AssertValid();
}

void CSlowFramesView::Dump(CDumpContext& dc) const
{
	CRecordView::Dump(dc);
}

CSlowFramesDoc* CSlowFramesView::GetDocument() const // non-debug version is inline
{
	ASSERT(m_pDocument->IsKindOf(RUNTIME_CLASS(CSlowFramesDoc)));
	return (CSlowFramesDoc*)m_pDocument;
}
#endif //_DEBUG


// CSlowFramesView database support
CRecordset* CSlowFramesView::OnGetRecordset()
{
	return m_pSet;
}



// CSlowFramesView message handlers

void CSlowFramesView::OnLvnItemchangedList(NMHDR *pNMHDR, LRESULT *pResult)
{
  LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
  if ((pNMLV->uOldState & LVIS_SELECTED) == 0 && (pNMLV->uNewState & LVIS_SELECTED) != 0)
  {
    int item = pNMLV->iItem;
    long id = (long)_list.GetItemData(item);

    SelectRecord(id);
  }

  *pResult = 0;
}

void CSlowFramesView::SaveAndUpdate()
{
  // Update old record
  if (m_pSet->CanUpdate() && !m_pSet->IsDeleted())
  {
    m_pSet->Edit();
    UpdateData();
    m_pSet->Update();
  }

  // Update list
  UpdateList();
}

void CSlowFramesView::OnEnKillfocusAssignedTo()
{
  CString text;
  _assignedTo.GetWindowText(text);
  if (m_pSet->m_AssignedTo == text) return;

  SaveAndUpdate();
}

void CSlowFramesView::OnCbnSelendokStatus()
{
  SaveAndUpdate();
}

CString IdentifyCause(const char *titles, const char *values);

void CSlowFramesView::OnIdentifyCause()
{
  if (!m_pSet->IsEOF())
  {
    CString cause = IdentifyCause(m_pSet->m_FrameAvgTitles, m_pSet->m_FrameAvgValues);
    if (m_pSet->m_Cause != cause && m_pSet->CanUpdate() && !m_pSet->IsDeleted())
    {
      m_pSet->Edit();
      m_pSet->m_Cause = cause;
      m_pSet->Update();

      UpdateList();
    }
  }
}

void CSlowFramesView::OnIdentifyCausesAll()
{
  if (m_pSet->IsEOF()) return;

  int changed = 0;
  {
    CWaitCursor wait;

    m_pSet->m_strFilter = "";
    m_pSet->Requery();
    m_pSet->MoveFirst();
    while (!m_pSet->IsEOF())
    {
      // if (set.m_Cause.IsEmpty())
      {
        CString cause = IdentifyCause(m_pSet->m_FrameAvgTitles, m_pSet->m_FrameAvgValues);
        if (m_pSet->m_Cause != cause && m_pSet->CanUpdate() && !m_pSet->IsDeleted())
        {
          m_pSet->Edit();
          m_pSet->m_Cause = cause;
          m_pSet->Update();
          changed++;
        }
      }
      m_pSet->MoveNext();
    }
  }

  if (changed > 0)
  {
    UpdateList();
    MessageBox(Format("Cause updated for %d records.", changed), "Causes changed", MB_OK);
  }
  else
  {
    MessageBox("No cause updated.", "Causes not changed", MB_OK);
  }
}

void CSlowFramesView::OnFilter()
{
  CFilterDialog dlg;
  dlg._filter = _filter;
  if (dlg.DoModal() == IDOK && dlg._filter != _filter)
  {
    _filter = dlg._filter;
    UpdateList();
  }
}

void CSlowFramesView::OnLvnColumnclickList(NMHDR *pNMHDR, LRESULT *pResult)
{
  LPNMLISTVIEW pNMLV = reinterpret_cast<LPNMLISTVIEW>(pNMHDR);
  Assert(pNMLV->iItem == -1);
  int column = pNMLV->iSubItem;
  switch (column)
  {
  case 0:
    m_pSet->m_strSort = _T("[Campaign]");
    break;
  case 1:
    m_pSet->m_strSort = _T("[Mission]");
    break;
  case 2:
    m_pSet->m_strSort = _T("[Build]");
    break;
  case 3:
    m_pSet->m_strSort = _T("[Situation]");
    break;
  case 4:
    m_pSet->m_strSort = _T("[Cause]");
    break;
  case 5:
    m_pSet->m_strSort = _T("[AssignedTo]");
    break;
  case 6:
    m_pSet->m_strSort = _T("[Status]");
    break;
  case 7:
    m_pSet->m_strSort = _T("[MaxTotal]");
    break;
  case 8:
    m_pSet->m_strSort = _T("[MaxDraw]");
    break;
  case 9:
    m_pSet->m_strSort = _T("[MaxSimul]");
    break;
  case 10:
    m_pSet->m_strSort = _T("[MaxAI]");
    break;
  case 11:
    m_pSet->m_strSort = _T("[AvgTotal]");
    break;
  case 12:
    m_pSet->m_strSort = _T("[AvgDraw]");
    break;
  case 13:
    m_pSet->m_strSort = _T("[AvgSimul]");
    break;
  case 14:
    m_pSet->m_strSort = _T("[AvgAI]");
    break;
  default:
    *pResult = 0;
    return; // wrong column
  }
  if (_sortColumn == column)
  {
    if (_sortDescending)
    {
      _sortDescending = false;
    }
    else
    {
      m_pSet->m_strSort += _T(" DESC");
      _sortDescending = true;
    }
  }
  else
  {
    _sortColumn = column;
    _sortDescending = false;
  }
  UpdateList();
  *pResult = 0;
}

// CFilterDialog dialog

IMPLEMENT_DYNAMIC(CFilterDialog, CDialog)
CFilterDialog::CFilterDialog(CWnd* pParent /*=NULL*/)
	: CDialog(CFilterDialog::IDD, pParent)
  , _filter(_T(""))
{
}

CFilterDialog::~CFilterDialog()
{
}

void CFilterDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
  DDX_Text(pDX, IDC_FILTER, _filter);
}


BEGIN_MESSAGE_MAP(CFilterDialog, CDialog)
END_MESSAGE_MAP()

// CFilterDialog message handlers
