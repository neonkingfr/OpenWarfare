// SlowFramesView.h : interface of the CSlowFramesView class
//


#pragma once
#include "afxcmn.h"
#include "afxwin.h"

class CSlowFramesSet;

class CSlowFramesView : public CRecordView
{
protected: // create from serialization only
	CSlowFramesView();
	DECLARE_DYNCREATE(CSlowFramesView)

  CFont _font;
  CString _filter;

  int _sortColumn;
  bool _sortDescending;

public:
	enum{ IDD = IDD_SLOWFRAMES_FORM };
	CSlowFramesSet* m_pSet;

// Attributes
public:
	CSlowFramesDoc* GetDocument() const;

// Operations
public:

// Overrides
public:
	virtual CRecordset* OnGetRecordset();
  virtual BOOL PreCreateWindow(CREATESTRUCT& cs);
protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void OnInitialUpdate(); // called first time after construct
  virtual void OnUpdate(CView* pSender, LPARAM lHint, CObject* pHint);
	virtual BOOL OnPreparePrinting(CPrintInfo* pInfo);
	virtual void OnBeginPrinting(CDC* pDC, CPrintInfo* pInfo);
	virtual void OnEndPrinting(CDC* pDC, CPrintInfo* pInfo);
  virtual BOOL OnMove(UINT nIDMoveCommand);

// Implementation
public:
	virtual ~CSlowFramesView();
#ifdef _DEBUG
	virtual void AssertValid() const;
	virtual void Dump(CDumpContext& dc) const;
#endif

  void UpdateList();
  void SaveAndUpdate();
  void SelectRecord(long id);
protected:

// Generated message map functions
protected:
	DECLARE_MESSAGE_MAP()
public:
  CListCtrl _list;
  CStatic _line1;
  CStatic _line2;
  CStatic _line3;
  CStatic _line4;
  afx_msg void OnSize(UINT nType, int cx, int cy);
  afx_msg void OnLvnItemchangedList(NMHDR *pNMHDR, LRESULT *pResult);
  CStatic _assignedToTitle;
  CStatic _statusTitle;
  CStatic _logFileTitle;
  CStatic _logLineTitle;
  CEdit _assignedTo;
  CComboBox _status;
  CEdit _logFile;
  CEdit _logLine;
  CStatic _minFrame;
  CStatic _avgFrame;
  afx_msg void OnEnKillfocusAssignedTo();
  afx_msg void OnIdentifyCause();
  afx_msg void OnIdentifyCausesAll();
  afx_msg void OnFilter();
  afx_msg void OnCbnSelendokStatus();
  afx_msg void OnLvnColumnclickList(NMHDR *pNMHDR, LRESULT *pResult);
};

#ifndef _DEBUG  // debug version in SlowFramesView.cpp
inline CSlowFramesDoc* CSlowFramesView::GetDocument() const
   { return reinterpret_cast<CSlowFramesDoc*>(m_pDocument); }
#endif

#pragma once


// CFilterDialog dialog

class CFilterDialog : public CDialog
{
	DECLARE_DYNAMIC(CFilterDialog)

public:
	CFilterDialog(CWnd* pParent = NULL);   // standard constructor
	virtual ~CFilterDialog();

// Dialog Data
	enum { IDD = IDD_FILTER };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
  CString _filter;
};
