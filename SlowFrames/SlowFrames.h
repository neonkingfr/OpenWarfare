// SlowFrames.h : main header file for the SlowFrames application
//
#pragma once

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"       // main symbols


// CSlowFramesApp:
// See SlowFrames.cpp for the implementation of this class
//

class CSlowFramesApp : public CWinApp
{
public:
	CSlowFramesApp();


// Overrides
public:
	virtual BOOL InitInstance();

// Implementation
	afx_msg void OnAppAbout();
	DECLARE_MESSAGE_MAP()
};

extern CSlowFramesApp theApp;