// SlowFramesDoc.cpp : implementation of the CSlowFramesDoc class
//

#include "stdafx.h"
#include "SlowFrames.h"

#include "SlowFramesSet.h"
#include "SlowFramesDoc.h"
#include ".\slowframesdoc.h"

#include <El/QStream/qstream.hpp>
#include <Es/Files/fileContainer.hpp>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CSlowFramesDoc

IMPLEMENT_DYNCREATE(CSlowFramesDoc, CDocument)

BEGIN_MESSAGE_MAP(CSlowFramesDoc, CDocument)
  ON_COMMAND(ID_LOG_IMPORT, OnLogImport)
  ON_COMMAND(ID_LOG_REMOVE, OnLogRemove)
END_MESSAGE_MAP()


// CSlowFramesDoc construction/destruction

CSlowFramesDoc::CSlowFramesDoc()
{
	// TODO: add one-time construction code here

}

CSlowFramesDoc::~CSlowFramesDoc()
{
}

BOOL CSlowFramesDoc::OnNewDocument()
{
	if (!CDocument::OnNewDocument())
		return FALSE;

	// TODO: add reinitialization code here
	// (SDI documents will reuse this document)

	return TRUE;
}

// CSlowFramesDoc diagnostics

#ifdef _DEBUG
void CSlowFramesDoc::AssertValid() const
{
	CDocument::AssertValid();
}

void CSlowFramesDoc::Dump(CDumpContext& dc) const
{
	CDocument::Dump(dc);
}
#endif //_DEBUG

inline bool Compare(const char *buffer, const char *with)
{
  return strnicmp(buffer, with, strlen(with)) == 0;
}

static RString NormalizeName(const char *name)
{
  char buffer[256];
  char *dst = buffer;
  while (*name)
  {
    if (*name == '\\') *dst = '-';
    else *dst = *name;
    dst++;
    name++;
  }
  *dst = 0;
  return buffer;
}

static RString FindLogName(RString dir, RString campaign, RString mission)
{
  RString baseName;
  if (campaign.GetLength() > 0) baseName = baseName + NormalizeName(campaign) + RString(" ");
  baseName = baseName + NormalizeName(mission) + RString(" ");
  for (int i=1;;i++)
  {
    RString name = baseName + Format("%d.txt", i);
    if (!QIFileFunctions::FileExists(dir + name)) return name;
  }
  return RString();
}

static RString ExtractName(const char *prefix, const char *name)
{
  char buffer[256];
  if (Compare(name, prefix)) strcpy(buffer, name + strlen(prefix));
  else strcpy(buffer, name);
  int n = strlen(buffer);
  if (buffer[n-1] == '\\')
  {
    n--; buffer[n] = 0;
  }
  return buffer;
}

RString LogDirectory = "U:\\SlowFrames\\Logs\\";

// CSlowFramesDoc commands

void CSlowFramesDoc::OnLogImport()
{
  // Get file name
  CString fileName;
  CFileDialog dlg(TRUE, NULL, NULL, OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT, NULL, NULL, 0);
  // dlg.m_ofn.Flags |= lFlags;
  dlg.m_ofn.nMaxCustFilter = 2;
  dlg.m_ofn.lpstrFilter = "Text files (*.txt)\0*.txt\0All files (*.*)\0*.*\0";
  dlg.m_ofn.lpstrTitle = "Select log file";
  dlg.m_ofn.lpstrFile = fileName.GetBuffer(_MAX_PATH);
  INT_PTR nResult = dlg.DoModal();
  fileName.ReleaseBuffer();
  if (nResult != IDOK) return;

  int records = 0;
  bool some = false;
  {
    CWaitCursor wait;

    m_SlowFramesSet.m_strFilter = "";
    m_SlowFramesSet.Requery();

    QIFStream in;
    in.open(fileName);

    QOFStream out;

    bool mission = false;
    bool map = false;
    bool dialog = false;
    bool ignore = false;

    RString campaignName;
    RString missionName;
    RString build;

    int line;
    RString logName;

    while (!in.eof())
    {
      char buffer[256];
      if (in.readLine(buffer, 256))
      {
        static const char *elementMission = "<mission";
        static const char *attrCampaign = "campaign=";
        static const char *attrMission = "mission=";
        static const char *attrBuild = "build=";
        if (Compare(buffer, elementMission))
        {
          Assert(!mission);
          mission = true;

          const char *ptr = buffer + strlen(elementMission);
          while (*ptr && isspace(*ptr)) ptr++;
          while (*ptr && *ptr != '>')
          {
            if (Compare(ptr, attrCampaign))
            {
              ptr += strlen(attrCampaign);
              if (*ptr != '"') break;
              ptr++;
              const char *end = strchr(ptr, '"');
              if (!end) break;
              campaignName = ExtractName("campaigns\\", RString(ptr, end - ptr));
              ptr = end + 1;
            }
            else if (Compare(ptr, attrMission))
            {
              ptr += strlen(attrMission);
              if (*ptr != '"') break;
              ptr++;
              const char *end = strchr(ptr, '"');
              if (!end) break;
              missionName = ExtractName("missions\\", RString(ptr, end - ptr));
              ptr = end + 1;
            }
            else if (Compare(ptr, attrBuild))
            {
              ptr += strlen(attrBuild);
              if (*ptr != '"') break;
              ptr++;
              const char *end = strchr(ptr, '"');
              if (!end) break;
              build = RString(ptr, end - ptr);
              ptr = end + 1;
            }
            while (*ptr && isspace(*ptr)) ptr++;
          }

          logName = FindLogName(LogDirectory, campaignName, missionName);
          line = 1;
          out.open(LogDirectory + logName);
          some = true;
        }
        else if (Compare(buffer, "</mission>"))
        {
          Assert(mission);
          mission = false;

          campaignName = RString();
          missionName = RString();
          build = RString();

          out.write(buffer, strlen(buffer));
          out.write("\r\n", 2);
          out.close();
        }
        else if (Compare(buffer, "<map>")) map = true;
        else if (Compare(buffer, "</map>")) map = false;
        else if (Compare(buffer, "<dialog>")) dialog = true;
        else if (Compare(buffer, "</dialog>")) dialog = false;
        else if (Compare(buffer, "<ignore>")) ignore = true;
        else if (Compare(buffer, "</ignore>")) ignore = false;
        else if (Compare(buffer, "WARN: Low fps detected:"))
        {
          if (mission && !ignore)
          {
            int index = line;
            RString ln1, ln2, ln3, ln4;
            if (in.readLine(buffer, 256))
            {
              ln1 = buffer;
              out.write(buffer, strlen(buffer));
              out.write("\r\n", 2);
              line++;
            }
            if (in.readLine(buffer, 256))
            {
              ln2 = buffer;
              out.write(buffer, strlen(buffer));
              out.write("\r\n", 2);
              line++;
            }
            if (in.readLine(buffer, 256))
            {
              ln3 = buffer;
              out.write(buffer, strlen(buffer));
              out.write("\r\n", 2);
              line++;
            }
            if (in.readLine(buffer, 256))
            {
              ln4 = buffer;
              out.write(buffer, strlen(buffer));
              out.write("\r\n", 2);
              line++;
            }
            // add to database
            m_SlowFramesSet.AddNew();
            m_SlowFramesSet.m_Campaign = campaignName;
            m_SlowFramesSet.m_Mission = missionName;
            m_SlowFramesSet.m_Build = build;
            if (dialog)
              m_SlowFramesSet.m_Situation = "dialog";
            else if (map)
              m_SlowFramesSet.m_Situation = "map";
            else 
              m_SlowFramesSet.m_Situation = "mission";
            CString IdentifyCause(const char *titles, const char *values);
            m_SlowFramesSet.m_Cause = IdentifyCause(ln3, ln4);
            m_SlowFramesSet.m_AssignedTo = "";
            m_SlowFramesSet.m_Status = "Added";
            m_SlowFramesSet.m_LogFile = logName;
            m_SlowFramesSet.m_LogLine = index;
            m_SlowFramesSet.m_FrameMinTitles = ln1;
            m_SlowFramesSet.m_FrameMinValues = ln2;
            m_SlowFramesSet.m_FrameAvgTitles = ln3;
            m_SlowFramesSet.m_FrameAvgValues = ln4;
            void GetValues(const char *titles, const char *values, int &total, int &draw, int &simul, int &ai);
            int total, draw, simul, ai;
            GetValues(ln1, ln2, total, draw, simul, ai);
            m_SlowFramesSet.m_MaxTotal = total;
            m_SlowFramesSet.m_MaxDraw = draw;
            m_SlowFramesSet.m_MaxSimul = simul;
            m_SlowFramesSet.m_MaxAI = ai;
            GetValues(ln3, ln4, total, draw, simul, ai);
            m_SlowFramesSet.m_AvgTotal = total;
            m_SlowFramesSet.m_AvgDraw = draw;
            m_SlowFramesSet.m_AvgSimul = simul;
            m_SlowFramesSet.m_AvgAI = ai;
            m_SlowFramesSet.Update();
            records++;
            continue; // do not write last line once more
          }
        }
        if (mission)
        {
          out.write(buffer, strlen(buffer));
          out.write("\r\n", 2);
          line++;
        }
      }
    }

    in.close();

    if (mission) out.close();
  }

  if (some)
  {
    UpdateAllViews(NULL);
    MessageBox(theApp.GetMainWnd()->GetSafeHwnd(), Format("%d records added to database.", records), "Import succeed", MB_OK);
  }
  else
    MessageBox(theApp.GetMainWnd()->GetSafeHwnd(), "No mission header found!", "Import failed", MB_OK);
}

void CSlowFramesDoc::OnLogRemove()
{
  CLogFileDialog dlg;

  if (dlg.DoModal() == IDOK)
  {
    RString log = (const char *)dlg.GetLogFile();
    if (log.GetLength() > 0)
    {
      // remove log
      QIFileFunctions::Unlink(LogDirectory + log);

      // remove database items
      // do not use SQL command for removing items to keep recordset synchronized
      m_SlowFramesSet.m_strFilter = "";
      m_SlowFramesSet.Requery();

      while (!m_SlowFramesSet.IsEOF())
      {
        if (stricmp(m_SlowFramesSet.m_LogFile, log) == 0) m_SlowFramesSet.Delete();
        m_SlowFramesSet.MoveNext();
      }

      UpdateAllViews(NULL);
    }
  }
}

// CLogFileDialog dialog

IMPLEMENT_DYNCREATE(CLogFileDialog, CDialog)

CLogFileDialog::CLogFileDialog(CWnd* pParent /*=NULL*/)
	: CDialog(CLogFileDialog::IDD, pParent)
  , _logFile(_T(""))
{
}

CLogFileDialog::~CLogFileDialog()
{
}

void CLogFileDialog::DoDataExchange(CDataExchange* pDX)
{
  CDialog::DoDataExchange(pDX);
  DDX_Control(pDX, IDC_LOG_FILES, _list);
  DDX_LBString(pDX, IDC_LOG_FILES, _logFile);
}

static bool AddLog(const FileItem &arg, CListBox &list)
{
  if (!arg.directory) list.AddString(arg.filename);
  return false; // continue with enumeration
}

BOOL CLogFileDialog::OnInitDialog()
{
	CDialog::OnInitDialog();

  ForEachFile(LogDirectory, AddLog, _list);
  if (_list.GetCount() > 0) _list.SetCurSel(0);

	return TRUE;  // return TRUE  unless you set the focus to a control
}

CString CLogFileDialog::GetLogFile() const
{
  return _logFile;
}

BEGIN_MESSAGE_MAP(CLogFileDialog, CDialog)
END_MESSAGE_MAP()

// CLogFileDialog message handlers
