//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by SlowFrames.rc
//
#define IDD_ABOUTBOX                    100
#define IDP_OLE_INIT_FAILED             100
#define IDD_SLOWFRAMES_FORM             101
#define IDP_FAILED_OPEN_DATABASE        103
#define IDR_HTML_LOGFILEDIALOG          104
#define IDR_MAINFRAME                   128
#define IDR_SlowFramesTYPE              129
#define IDD_FILTER                      130
#define IDD_LOG_FILE                    131
#define IDC_LIST                        1000
#define IDC_ASSIGNED_TO                 1001
#define IDC_LOG_FILE                    1003
#define IDC_LOG_LINE                    1004
#define IDC_LINE1                       1005
#define IDC_LINE2                       1006
#define IDC_LINE3                       1007
#define IDC_LINE4                       1008
#define IDC_ASSIGNED_TO_TITLE           1010
#define IDC_STATUS_TITLE                1011
#define IDC_LOG_FILE_TITLE              1012
#define IDC_LOG_LINE_TITLE              1013
#define IDC_MIN_FRAME                   1014
#define IDC_AVG_FRAME                   1015
#define IDC_FILTER                      1016
#define IDC_LOG_FILES                   1017
#define IDC_STATE                       1018
#define IDC_STATUS                      1019
#define ID_LOG_IMPORT                   32772
#define ID_IDENTIFY_CAUSE               32774
#define ID_IDENTIFY_CAUSES_ALL          32775
#define ID_FILTER                       32777
#define ID_LOG_REMOVE                   32779

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        132
#define _APS_NEXT_COMMAND_VALUE         32780
#define _APS_NEXT_CONTROL_VALUE         1020
#define _APS_NEXT_SYMED_VALUE           105
#endif
#endif
