// SlowFramesSet.cpp : implementation of the CSlowFramesSet class
//

#include "stdafx.h"
#include "SlowFrames.h"
#include "SlowFramesSet.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CSlowFramesSet implementation

// code generated on 1. ��jna 2004, 8:11

IMPLEMENT_DYNAMIC(CSlowFramesSet, CRecordset)

CSlowFramesSet::CSlowFramesSet(CDatabase* pdb)
	: CRecordset(pdb)
{
  m_Id = 0;
	m_Campaign = L"";
	m_Mission = L"";
	m_Build = L"";
	m_Situation = L"";
	m_Cause = L"";
	m_AssignedTo = L"";
	m_Status = L"";
	m_LogFile = L"";
	m_LogLine = 0;
	m_FrameMinTitles = L"";
	m_FrameMinValues = L"";
	m_FrameAvgTitles = L"";
	m_FrameAvgValues = L"";
  m_MaxTotal = 0;
  m_MaxDraw = 0;
  m_MaxSimul = 0;
  m_MaxAI = 0;
  m_AvgTotal = 0;
  m_AvgDraw = 0;
  m_AvgSimul = 0;
  m_AvgAI = 0;
	m_nFields = 22;
	m_nDefaultType = dynaset;
}
// #error Security Issue: The connection string may contain a password
// The connection string below may contain plain text passwords and/or
// other sensitive information. Please remove the #error after reviewing
// the connection string for any security related issues. You may want to
// store the password in some other form or use a different user authentication.
CString CSlowFramesSet::GetDefaultConnect()
{
  return _T("FILEDSN=U:\\SlowFrames\\Slow frames.dsn;DBQ=U:\\Tools\\SlowFrames\\SlowFrames.mdb;DefaultDir=U:\\Tools\\SlowFrames;Driver={Microsoft Access Driver (*.mdb)};DriverId=25;FIL=MS Access;MaxBufferSize=2048;MaxScanRows=8;PageTimeout=5;SafeTransactions=0;Threads=3;UID=admin;UserCommitSync=Yes;");
}

CString CSlowFramesSet::GetDefaultSQL()
{
	return _T("[SlowFrames]");
}

void CSlowFramesSet::DoFieldExchange(CFieldExchange* pFX)
{
	pFX->SetFieldType(CFieldExchange::outputColumn);
// Macros such as RFX_Text() and RFX_Int() are dependent on the
// type of the member variable, not the type of the field in the database.
// ODBC will try to automatically convert the column value to the requested type
  RFX_Long(pFX, _T("[Id]"), m_Id);
	RFX_Text(pFX, _T("[Campaign]"), m_Campaign);
	RFX_Text(pFX, _T("[Mission]"), m_Mission);
	RFX_Text(pFX, _T("[Build]"), m_Build);
	RFX_Text(pFX, _T("[Situation]"), m_Situation);
	RFX_Text(pFX, _T("[Cause]"), m_Cause);
	RFX_Text(pFX, _T("[AssignedTo]"), m_AssignedTo);
	RFX_Text(pFX, _T("[Status]"), m_Status);
	RFX_Text(pFX, _T("[LogFile]"), m_LogFile);
	RFX_Long(pFX, _T("[LogLine]"), m_LogLine);
	RFX_Text(pFX, _T("[FrameMinTitles]"), m_FrameMinTitles);
	RFX_Text(pFX, _T("[FrameMinValues]"), m_FrameMinValues);
	RFX_Text(pFX, _T("[FrameAvgTitles]"), m_FrameAvgTitles);
	RFX_Text(pFX, _T("[FrameAvgValues]"), m_FrameAvgValues);
  RFX_Long(pFX, _T("[MaxTotal]"), m_MaxTotal);
  RFX_Long(pFX, _T("[MaxDraw]"), m_MaxDraw);
  RFX_Long(pFX, _T("[MaxSimul]"), m_MaxSimul);
  RFX_Long(pFX, _T("[MaxAI]"), m_MaxAI);
  RFX_Long(pFX, _T("[AvgTotal]"), m_AvgTotal);
  RFX_Long(pFX, _T("[AvgDraw]"), m_AvgDraw);
  RFX_Long(pFX, _T("[AvgSimul]"), m_AvgSimul);
  RFX_Long(pFX, _T("[AvgAI]"), m_AvgAI);
}
/////////////////////////////////////////////////////////////////////////////
// CSlowFramesSet diagnostics

#ifdef _DEBUG
void CSlowFramesSet::AssertValid() const
{
	CRecordset::AssertValid();
}

void CSlowFramesSet::Dump(CDumpContext& dc) const
{
	CRecordset::Dump(dc);
}
#endif //_DEBUG

