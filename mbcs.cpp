// mbcs.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include <stdio.h>
#include <stdlib.h>
#include <windows.h>

static int VowelsIndices[] =
{
	0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x0a, 0x0b, 0x0c, 0x0d, 0x0e, 0x0f,
	0x12, 0x13, 0x14, 0x15, 0x16, 0x17, 0x1a, 0x1b, 0x1c, 0x1d
};

#define PSEUDOCODE(lc, vo, tc) \
	0x8000 |\
	((lc + 1) << 10) |\
	(VowelsIndices[vo] << 5) |\
	tc + 1

static int SingleCodes[] =
{
	0x8841, 0x8c41, 0x8444, 0x9041, 0x8446, 0x8447, 0x9441, 0x9841,
	0x9c41, 0x844a, 0x844b, 0x844c, 0x844d, 0x844e, 0x844f, 0x8450,
	0xa041, 0xa441, 0xa841, 0x8454, 0xac41, 0xb041, 0xb441, 0xb841,
	0xbc41, 0xc041, 0xc441, 0xc841, 0xcc41, 0xd041,
	0x8461, 0x8481, 0x84a1, 0x84c1, 0x84e1, 0x8541, 0x8561, 0x8581,
	0x85a1, 0x85c1, 0x85e1, 0x8641, 0x8661, 0x8681, 0x86a1, 0x86c1,
	0x86e1, 0x8741, 0x8761, 0x8781, 0x87a1
};

int MultiByteToPseudoCode(const char *in, char *out)
{
	if (IsDBCSLeadByteEx(949, *in))
	{
		wchar_t unicode;
		int len = MultiByteToWideChar
		(
			949,					     // code page
			0,				         // character-type options
			in,								 // address of string to map
			2,						     // number of bytes in string
			&unicode,					 // address of wide-character buffer
			1									 // size of buffer
		);
		if (len == 1)
		{
			if (unicode >= 0x1100 && unicode < 0x1113)
			{
				int pseudocode = PSEUDOCODE(unicode - 0x1110 + 1, 0, 0);
				*out = *((char *)&pseudocode + 1);
				*(out + 1) = *(char *)&pseudocode;
				return 2;
			}
			else if (unicode >= 0x1161 && unicode < 0x1176)
			{
				int pseudocode = PSEUDOCODE(0, unicode - 0x1161 + 1, 0);
				*out = *((char *)&pseudocode + 1);
				*(out + 1) = *(char *)&pseudocode;
				return 2;
			}
			else if (unicode >= 0x11a8 && unicode < 0x11ba)
			{
				int pseudocode = PSEUDOCODE(0, 0, unicode - 0x11a8);
				*out = *((char *)&pseudocode + 1);
				*(out + 1) = *(char *)&pseudocode;
				return 2;
			}
			else if (unicode >= 0x3131 && unicode < 0x3164)
			{
				int pseudocode = SingleCodes[unicode - 0x3131];
				*out = *((char *)&pseudocode + 1);
				*(out + 1) = *(char *)&pseudocode;
				return 2;
			}
			else if (unicode >= 0xac00 && unicode < 0xd7a4)
			{
				unicode -= 0xac00;
				int r1 = unicode / 28;
				int tc = unicode - r1 * 28;
				int lc = r1 / 21;
				int vo = r1 - lc * 21;
				int pseudocode = PSEUDOCODE(lc + 1, vo + 1, tc);
				*out = *((char *)&pseudocode + 1);
				*(out + 1) = *(char *)&pseudocode;
				return 2;
			}
		}
	}
	*out = *in;
	return 1;
}

static unsigned char LCSymbols[] =
{
	0x80, 0x81, 0x82, 0x84, 0x87, 0x88, 0x89, 0x91, 0x92, 0x93,
	0x95, 0x96, 0x97, 0x98, 0x99, 0x9a, 0x9b, 0x9c, 0x9d, 0x9e
};

static unsigned char TCSymbols[] =
{
	0x80, 0x81, 0x82, 0x83, 0x84, 0x85, 0x86, 0x87, 0x89, 0x8a,
	0x8b, 0x8c, 0x8d, 0x8e, 0x8f, 0x90, 0x91, 0x92, 0x94, 0x95,
	0x96, 0x97, 0x98, 0x9a, 0x9b, 0x9c, 0x9d, 0x9e
};

static unsigned char VowelSymbols[] =
{
	0x80, 0x9f, 0xa0, 0xa1, 0xa2, 0xa3, 0x00, 0x00, 0xa4, 0xa5,
	0xa6, 0xa7, 0xa8, 0xa9, 0x00, 0x00,	0xaa, 0xab, 0xac, 0xad,
	0xae, 0xaf,	0x00, 0x00,	0xb0, 0xb1, 0xb2, 0xb3
};

static char *VowelPrefix2[] =
{
	"", "\x0f0", "\x0f0", "\x0f0", "\x0f0", "\x0f0", "", "",
	"\x0f0", "\x0f0", "\x0f0", "\x0f1", "\x0f2", "\x0f2", "", "",
	"\x0f2", "\x0f1", "\x0f1", "\x0f2", "\x0f2", "\x0f2",	"", "",
	"\x0f1", "\x0f1", "\x0f2", "\x0f0"
};

static char *VowelPrefix3[] =
{
	"", "\x0f1\x0f0", "\x0f1\x0f0", "\x0f1\x0f0", "\x0f1\x0f0", "\x0f1\x0f0", "", "",
	"\x0f1\x0f0", "\x0f1\x0f0", "\x0f1\x0f0", "\x0f8", "\x0f1\x0f2", "\x0f1\x0f2", "", "",
	"\x0f1\x0f2", "\x0f8", "\x0f8", "\x0f1\x0f2", "\x0f1\x0f2", "\x0f1\x0f2",	"", "",
	"\x0f8", "\x0f8", "\x0f1\x0f2", "\x0f1\x0f0"
};

bool PseudoCodeToPictureCode(const char *in, char *out)
{
	// DoAssert(*in & 0x80);
	*out = 0;
	int pseudocode = (*(unsigned char *)in << 8) | *((unsigned char *)in + 1);
	int lc = ((pseudocode & 0x7c00) >> 10) - 1;
	int vo = ((pseudocode & 0x03e0) >> 5) - 2;
	int tc = (pseudocode & 0x001f) - 1;
	if (vo == 0)
	{
		if (tc == 0)
		{
			// single leading consonant
			*out = LCSymbols[lc];
			out++;
			*out = 0;
			return true;
		}
		else if (lc == 0)
		{
			// single trailing consonant
			if (tc > 16) tc--;
			*out = TCSymbols[tc];
			out++;
			*out = 0;
			return true;
		}
		else return false;
	}
	else if (lc == 0)
	{
		if (tc == 0)
		{
			// single vowel
			*out = VowelSymbols[vo];
			out++;
			*out = 0;
			return true;
		}
		else return false;
	}
	else
	{
		// composition
		if (tc == 0)
		{
			// leading consonant + vowel
			strcpy(out, VowelPrefix2[vo]);
			out += strlen(out);
			*out = LCSymbols[lc];
			out++;
			*out = VowelSymbols[vo];
			out++;
			*out = 0;
			return true;
		}
		else
		{
			// leading consonant + vowel + trailing consonant
			strcpy(out, VowelPrefix3[vo]);
			out += strlen(out);
			*out = LCSymbols[lc];
			out++;
			*out = VowelSymbols[vo];
			out++;
			*out = TCSymbols[tc];
			out++;
			*out = 0;
			return true;
		}
	}
}

int main(int argc, char* argv[])
{
	for (wchar_t i=0xac00; i<0xd7a4; i++)
//	for (wchar_t i=0x1100; i<0x11bf; i++)
	{
		char mbcs[4];

		int len = WideCharToMultiByte
		(
			949,			         // code page
		  0,				         // performance and mapping flags
			&i,								 // address of wide-character string
			1,					       // number of characters in string
			mbcs,							 // address of buffer for new string
			4,						     // size of buffer
			NULL,							 // address of default for unmappable 
														 // characters
			NULL							 // address of flag set when default 
                             // char. used
		);
		if (len == 1 && i != 0x3f && mbcs[0] == 0x3f) continue;

		mbcs[len] = 0;
		printf("%04x = ", i);
		for (int j=0; j<len; j++)
		{
			printf("%02x ", (unsigned char)mbcs[j]);
		}
		char pseudo[4];
		if (MultiByteToPseudoCode(mbcs, pseudo) == len)
		{
			printf("(");
			for (j=0; j<len; j++)
				printf("%02x ", (unsigned char)pseudo[j]);
			printf(")");

			char picture[8];
			if (PseudoCodeToPictureCode(pseudo, picture))
			{
				printf("= (");
				for (j=0; picture[j]!=0; j++)
					printf("%02x ", (unsigned char)picture[j]);
				printf(")");
			}
		}
		for (j=0; j<len; j++)
		{
			printf("%c", mbcs[j]);
		}
		printf("<BR>\n");
	}
	return 0;
}
