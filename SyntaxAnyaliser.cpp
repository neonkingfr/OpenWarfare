#include "SyntaxAnyaliser.hpp"
#include "AppFrameWork.hpp"

bool SyntaxAnyaliser::Expect(GTokenEnum expected)
{
  const Token &found = _lexier->GetCurrentToken();
  if (expected != found._type)
  {
    const char *startPos = _lexier->GetStartPos();
    const char *currentPos = _lexier->GetCurrentPos();
    std::string content = std::string(startPos,currentPos-startPos);

    std::string msg;
    msg.append("Config file parse error\r\n\r\n");
    //msg.append(::Format("Content:\r\n%s\r\n",content.data()).data());
    msg.append
   (
      ::Format
      (
        "Line Number:%d \r\nExpected: %s \r\n\r\nFound Type: %s\r\nFound String:%s\r\n",
         _lexier->GetLineNumber(),
         GTokenString[expected],
         GTokenString[found._type],
         found._identifier.data()
      ).data()
    );

    // Warning message when trying to parse config file.
    WarningMessage(msg.data());

    return false;
  }

  _lexier->NextToken();
  return true;
}

bool SyntaxAnyaliser::Parse(LexicalAnalyser *lexier,ParamaFileConfig *configFile)
{
  _lexier = lexier;
  _lexier->NextToken();
  do 
  {
    // Start it off!
    SmartRef<ConfigEntry> entry = ParseFields();
    if (!entry) return false;
    else configFile->Add(entry);
  }
  while (_lexier->GetCurrentToken()._type != T_EOF);

  // Successfully parsed the file.
  return true;
}

SmartRef<ConfigEntry> SyntaxAnyaliser::ParseFields()
{
  Token token = _lexier->GetCurrentToken();
  
  SmartRef<ConfigEntry> entry;
  switch (token._type)
  {
  case T_IDENTIFIER:
    entry = ParseEntry();
    Expect(T_SEMICOLON);
    break;
  case T_CLASS:
    entry = ParseClass();
    if (entry) Expect(T_SEMICOLON);
    break;
  }

  return entry;
}


SmartRef<ConfigEntry> SyntaxAnyaliser::ParseClass()
{
  if (!Expect(T_CLASS)) return NULL;
  std::string className = _lexier->GetCurrentToken()._identifier;
  if (!Expect(T_IDENTIFIER)) return NULL;

  SmartRef<ConfigEntry> entry = new ConfigEntry(className);
  SmartRef<ConfigClass> configClass = new ConfigClass(entry);

  // Just a quick hack here. Just for inheritance that does not work right now.
  if (_lexier->GetCurrentToken()._type == T_COLON) Expect(T_COLON);
  if (_lexier->GetCurrentToken()._type == T_IDENTIFIER) Expect(T_IDENTIFIER);

  // Keep doing the loop for entry s.
  if (!Expect(T_LEFTBRACE)) return NULL;
  do
  {
    SmartRef<ConfigEntry> entry = ParseFields();
    // perfectly valid to have a empty class.
    if (entry) configClass->Add(entry);
  } 
  while (_lexier->GetCurrentToken()._type != T_RIGHTBRACE && _lexier->GetCurrentToken()._type != T_EOF );

  if (!Expect(T_RIGHTBRACE)) return NULL;

  // Everything is okay, return the config class as the entry.
  entry->SetEntry(configClass);
  return entry;
}

SmartRef<ConfigEntry> SyntaxAnyaliser::ParseEntry()
{
  std::string name = _lexier->GetCurrentToken()._identifier;
  SmartRef<ConfigEntry> entry = new ConfigEntry(name);

  if (!Expect(T_IDENTIFIER)) return NULL;
  Token token = _lexier->GetCurrentToken();

  SmartRef<IConfigType> any;
  switch (token._type)
  {
  case T_LSBRACKET:
    if (!Expect(T_LSBRACKET)) return NULL;
    if (!Expect(T_RSBRACKET)) return NULL;
    if (!Expect(T_ASSIGN)) return NULL;
    any = ParseArray();
    break;
  case T_ASSIGN:
    if (!Expect(T_ASSIGN)) return NULL;
    // Expect a base type back here.
    SmartRef<ConfigBaseType> baseType = ParseExpression();
    any = baseType.GetRef();
    break;
  }

  // need to check for null here.
  entry->SetEntry(any);

  return entry;
}

// Parse array call 
SmartRef<IConfigType> SyntaxAnyaliser::ParseArray()
{
  if (!Expect(T_LEFTBRACE)) return NULL;

  SmartRef<ConfigArray> configArray = new ConfigArray;
  while (_lexier->GetCurrentToken()._type != T_RIGHTBRACE)
  {
    SmartRef<IConfigType> any;
    if (_lexier->GetCurrentToken()._type == T_LEFTBRACE)
    {
      any = ParseArray();
    }
    else
    {
      SmartRef<ConfigBaseType> baseType = ParseExpression();
      any = baseType.GetRef();
    }
    
    // Check to make sure we have a valid item.
    if (!any) return NULL;
    // Add the item to the array.
    configArray->push_back(any);
    if (_lexier->GetCurrentToken()._type != T_RIGHTBRACE) Expect(T_COMMA);
  }  

  // End of the array expect the right brace.
  Expect(T_RIGHTBRACE);

  // Little bit safer return type.
  SmartRef<IConfigType> ret = configArray.GetRef();
  return ret;
}

SmartRef<ConfigBaseType> SyntaxAnyaliser::ParseExpression()
{
  // dealing with negative numbers now
  bool isMinus = false;
  if (_lexier->GetCurrentToken()._type == T_MINUS) 
  {
    isMinus = true;
    Expect(T_MINUS);
  }

  SmartRef<ConfigBaseType> before = ParseTimesDivide();
  if (!before) return NULL;

  if (isMinus) 
  {
    if (before->IsInteger())
    {
      int beforeValue;
      before->GetVal(beforeValue);
      before->SetVal(-beforeValue);
    }
    else if (before->IsScalar())
    {
      float beforeValue;
      before->GetVal(beforeValue);
      before->SetVal(-beforeValue);
    }
    else
    {
      WarningMessage("Expected number");
    }
  }

  SmartRef<ConfigBaseType> after;

  Token token = _lexier->GetCurrentToken();
  while ((token._type == T_PLUS) || (token._type == T_MINUS))
  {
    switch (token._type)
    {
    case T_PLUS:
      if (!Expect(T_PLUS)) return NULL;
      after = ParseTimesDivide();
      break;
    case T_MINUS:
      if (!Expect(T_MINUS)) return NULL;
      after = ParseTimesDivide();
      break;
    default:
      return NULL;
    }

    if (!after) return NULL;
    float beforeValue;
    float afterValue;
    after->GetVal(afterValue);
    before->GetVal(beforeValue);

    switch (token._type)
    {
    case T_PLUS:
      beforeValue += afterValue;
      break;
    case T_MINUS:
      beforeValue -= afterValue;
      break;
    }      
    before->SetVal(beforeValue);
  }

  return before;
}

SmartRef<ConfigBaseType> SyntaxAnyaliser::ParseTimesDivide()
{
  SmartRef<ConfigBaseType> before = ParseFactor();
  if (!before) return NULL;

  SmartRef<ConfigBaseType> after;
  Token token = _lexier->GetCurrentToken();

  while ((token._type == T_TIMES) || (token._type == T_DIVIDE))
  {
    if (before->GetType()!=DTFloat) 
    {
      WarningMessage("Expected number");      
      return NULL;
    }

    switch (token._type)
    {
    case T_TIMES:
      if (!Expect(T_TIMES)) return NULL;
      after = ParseFactor();
      break;
    case T_DIVIDE:
      if (!Expect(T_DIVIDE)) return NULL;
      after = ParseFactor();

      if (!after) return NULL;
      break;
    default: return NULL;
    }

    if (!after) return NULL;
    if (after->GetType()!=DTFloat)
    {
      WarningMessage("Expected number");
      return NULL;
    }
    // calculation time!
    float afterValue;
    float beforeValue;
    before->GetVal(beforeValue);
    after->GetVal(afterValue);

    switch (token._type)
    {
    case T_TIMES:
      beforeValue *= afterValue;
    case T_DIVIDE:
      if (afterValue==0.0f)
      {
        WarningMessage("Cannot divide by zero");
        return NULL;
      }
      beforeValue /= afterValue;
    default:
      break;
    }

    // Set the new value for before.
    before->SetVal(beforeValue);
  }

  return before;
}

SmartRef<ConfigBaseType> SyntaxAnyaliser::ParseFactor()
{
  // Because of a bit of simplicity, we only deal with floats. 
  // This can be changed to more higher precision later on if require.
  Token token = _lexier->GetCurrentToken();

  SmartRef<ConfigBaseType> configValue;
  switch (token._type)
  {
  case T_INTCONST:
    {        
      Expect(T_INTCONST);
      configValue = new ConfigInt;
      int conversion = (int) token._intConst;
      configValue->SetVal(conversion);
    }
    break;
  case T_FLOATCONST:
    {
      Expect(T_FLOATCONST);
      configValue = new ConfigScalar;
      configValue->SetVal(token._floatConst);
    }
    break;
  case T_BOOLEANCONST:
    {
      Expect(T_BOOLEANCONST);
      configValue = new ConfigBool;
      configValue->SetVal(token._boolConst);
    }
    break;
  case T_STRINGLITERAL:
    {
      // We may have a lot of string literals on one line, keep reading until we have everthing.
      configValue = new ConfigString;
      std::string value;
      do
      {      
        Expect(T_STRINGLITERAL);
        value.append(token._stringLiteralVal);
      }
      while (_lexier->GetCurrentToken()._type == T_STRINGLITERAL && _lexier->GetCurrentToken()._type != T_EOF);
      
      // Finlay set the value;
      configValue->SetVal(value);
    }
    break;
  case T_LEFTBRACKET:
    {        
      Expect(T_LEFTBRACKET);
      configValue = ParseExpression();
      Expect(T_RIGHTBRACKET);
    }
  default:
    return NULL;
  }

  return configValue;
}
