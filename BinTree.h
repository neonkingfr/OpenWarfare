///////////////////////////////////////////////////////////////////////////////
//
//                             Binary Tree
//
///////////////////////////////////////////////////////////////////////////////

#ifndef BINTREE_H
#define BINTREE_H

#define BINTREE_FATHER 1

#include <Es/Containers/array.hpp>

///StackArray implementation of basic Stack data structure using AutoArray
template <class Type>
class StackArray : public AutoArray<Type>
{
public:
  ///standard push operation
  void Push(Type item) {Add(item);}
  ///standard pop operation
  Type Pop()
  {
    int n = Size() - 1;
    Type item = Set(n);
    Delete(n);
    return item;
  }
  ///get the top stack value for possible update
  Type &Top() 
  {
    return Set(Size() - 1);
  }
  ///true when Stack is empty
  bool IsEmpty()
  {
    return Size()==0;
  }
};

/// traits for key comparison and searching
/// default traits: Type is actual value
template <class Type> 
struct BinTreeKeyTraits
{
  typedef Type TraitType;
  typedef typename Type::KeyType KeyType;
  static bool IsLess(const TraitType &a, const TraitType &b)
  {
    return a < b;
  }
  static bool IsEqual(const TraitType &a, const TraitType &b)
  {
    return a == b;
  }
  static bool KeyIsLess(const KeyType a, const Type &b)
  {
    return a < b.GetKey();
  }
  static bool KeyIsEqual(const KeyType a, const Type &b)
  {
    return a == b.GetKey();
  }
  static KeyType GetKey(TraitType a) {return a.GetKey(); }
};

//partial specialization for Ref
template <class Type> 
struct BinTreeKeyTraits<Ref<Type> >
{
  typedef Ref<Type> TraitType;
  typedef typename Type::KeyType KeyType;
  static bool IsLess(const TraitType a, const TraitType b)
  {
    return *a < *b;
  }
  static bool IsEqual(const TraitType a, const TraitType b)
  {
    return *a == *b;
  }
  static bool KeyIsLess(KeyType a, const TraitType b)
  {
    return a < b->GetKey();
  }
  static bool KeyIsEqual(KeyType a, const TraitType b)
  {
    return a == b->GetKey();
  }
  static KeyType GetKey(TraitType a) {return a->GetKey(); }
};

//partial specialization for pointer
template <class Type> 
struct BinTreeKeyTraits<Type *>
{
  typedef Type * TraitType;
  typedef typename Type::KeyType KeyType;
  static bool IsLess(const TraitType a, const TraitType b)
  {
    return *a < *b;
  }
  static bool IsEqual(const TraitType a, const TraitType b)
  {
    return *a == *b;
  }
  static bool KeyIsLess(const KeyType a, const TraitType b)
  {
    return a < b->GetKey();
  }
  static bool KeyIsEqual(const KeyType a, const TraitType b)
  {
    return a == b->GetKey();
  }
  static KeyType GetKey(TraitType a) {return a->GetKey(); }
};

template <class Type, class Traits> class BinTreeIterator;

/// binary searching tree based on key
/**
  \param Type type of item stored in binary tree
         When used with BinTreeKeyTraits<Type>, it must provide:
         1. typedef KeyType ... needed to know type of keys for comparision
         2. operator < and ==
         3. method KeyType GetKey()  
  \param Traits traits for key comparison and searching
*/
template <class Type, class Traits=BinTreeKeyTraits<Type> >
class BinTree 
{
public:
  /// BinTree node
  class BinTreeNode 
  {
  public:
    Type data;
    BinTreeNode *greaterSon, *lessSon;
#if BINTREE_FATHER
    BinTreeNode *father;
#endif

    BinTreeNode(const Type &newItem) : greaterSon(NULL), lessSon(NULL) 
#if BINTREE_FATHER
      , father(NULL)
#endif
    { data=newItem; };
  };
  BinTreeNode *FindNode(const Type &);
private:
  int count;  //number of list nodes inside tree
  BinTreeNode *root;
public:
  /// provide public access to the keyTraits type
  typedef Traits KeyTraits;
  /// default constructor
  BinTree()
  {
    root=NULL;
    count=0;
  };
  ~BinTree();
  
  ///clear all items in tree
  void Clear();
  ///get number of items in tree
  const int GetCount() { return count; }
  ///insert item
  void *Insert(const Type &item); //NULL if some error occurred
  ///delete item
  bool Delete(const Type &item); //true if successful
#if BINTREE_FATHER
  bool Delete(BinTreeNode *&node); //delete node without need to find it (more quick and safe)
#endif
  ///find the item
#pragma warning( push )
#pragma warning( disable : 4181 )
  const Type *Find(const Type &) const;
  const Type *FindKey(const typename Traits::KeyType val) const;
#pragma warning( pop )
  ///get Minimal item
  const Type *GetMin();
  ///get Maximal item
  const Type *GetMax();
  ///get first item greater or equal than specified key value
  const Type *GetFirstGreaterOrEqual(typename Traits::KeyType val);
  ///get first item greater or equal than specified item
  const Type *GetFirstGreaterOrEqual(Type &item) { return GetFirstGreaterOrEqual(KeyTraits::GetKey(item)); }
  ///get first item less or equal than specified key value
  const Type *GetFirstLessOrEqual(typename Traits::KeyType val);
  ///get first item less or equal than specified item
  const Type *GetFirstLessOrEqual(Type &item) { return GetFirstLessOrEqual(KeyTraits::GetKey(item)); }
  ///get first item greater than specified key value
  const Type *GetFirstGreater(typename Traits::KeyType val);
  ///get first item greater than specified item
  const Type *GetFirstGreater(Type &item) { return GetFirstGreater(KeyTraits::GetKey(item)); }
  ///get first item less than specified value
  const Type *GetFirstLess(typename Traits::KeyType val);
  ///get first item less than specified item
  const Type *GetFirstLess(Type &item) { return GetFirstLess(KeyTraits::GetKey(item)); }
  ///provide an easy way to get the proper BinTreeIterator template type
  typedef BinTreeIterator<Type, Traits> Iterator;
  friend class BinTreeIterator<Type,Traits>;
};

typedef enum
{
  BTMNotDefined,BTMPreOrder,BTMInOrder,BTMPostOrder
} BinTreeIteratorMethod;

///BinTreeIterator
/** iterates all items of tree using specified order (PreOrder, PostOrder, InOrder)
  \param Type type must be the same as in BinTree
  \param Traits traits for key comparison and searching. Should be the same as in BinTree.
*/
template <class Type, class Traits=BinTreeKeyTraits<Type> >
class BinTreeIterator
{
private: //helper types
  typedef typename BinTree<Type, Traits>::BinTreeNode BinTreeNode;
  typedef enum
  {
    NothingDone,FirstSonDone,NodeDone,SecondSonDone
  } BinTreeIteratorState;
  struct BinTreeStackItem
  {
    BinTreeNode *member;
    BinTreeIteratorState state;
    ClassIsSimple(BinTreeStackItem);
  };
private: //data members
  StackArray<BinTreeStackItem> stack;
  BinTreeIteratorMethod method;
private:
  BinTreeNode *NextPreOrder(Type &target);
  BinTreeNode *NextInOrder(Type &target);
  BinTreeNode *NextPostOrder(Type &target);
public:
  ~BinTreeIterator() {}
  BinTreeIterator(const BinTree<Type, Traits> & tree,BinTreeIteratorMethod m_=BTMInOrder )
  {
    Reset(tree,m_);
  }
  void Reset(const BinTree<Type, Traits> & tree,BinTreeIteratorMethod m_=BTMInOrder)
  {
    if(tree.root!=NULL)
    {
      BinTreeStackItem temp;
      temp.member=tree.root;
      temp.state=NothingDone;
      stack.Clear();
      stack.Push(temp);
    }
    method=m_;
  }
  BinTreeNode *Next(Type &target);  //returns false at the end of iteration
};

/////////////////////////////////////////////////////////////////////
//
//                       BinTree implementation
//
/////////////////////////////////////////////////////////////////////

//
//BinTree implementation
//

//destructor
template <class Type, class Traits>
BinTree<Type, Traits>::~BinTree()
{
  Clear();
}

//insert item
template <class Type, class Traits>
void *BinTree<Type, Traits>::Insert(const Type &newItem)
{
  BinTreeNode *retval=NULL;
  if (root)
  {
    BinTreeNode *a,*b;
    a=root;
    while(a!=NULL)
    {
      if(KeyTraits::IsEqual(newItem,a->data))
        a=b=NULL;
      else
      {
        b=a;
        if(KeyTraits::IsLess(newItem,a->data))
          a=a->lessSon;
        else
          a=a->greaterSon;
      }
    }
    if(b!=NULL)
    {
      if(KeyTraits::IsLess(newItem,b->data))
      {
        if((b->lessSon=new BinTreeNode(newItem))==NULL) return NULL;
        retval = b->lessSon;
#if BINTREE_FATHER
        b->lessSon->father=b;
#endif
      }
      else
      {
        if((b->greaterSon=new BinTreeNode(newItem))==NULL) return NULL;
        retval = b->greaterSon;
#if BINTREE_FATHER
        b->greaterSon->father=b;
#endif
      }
      count++;
    }
  }
  else
  {
    if((root = new BinTreeNode(newItem))==NULL) return NULL;
    retval = root;
    count++;
  }
  return retval;
}

//delete item
template <class Type, class Traits>
bool BinTree<Type, Traits>::Delete(const Type &item)
{
  BinTreeNode *father, *node;

  node = root; father = NULL;
  while (node)
  {
    if ( KeyTraits::IsEqual(node->data,item ) )
    {
      if ( !node->lessSon )
      {
        if ( !node->greaterSon )
        {
          if (father)
            if (father->lessSon==node)
              father->lessSon=NULL;
            else
              father->greaterSon=NULL;
          else if (node==root) root=NULL;
          delete node;
        }
        else
        {
/*
          BinTreeNode *help = node->greaterSon;
          node->data = help->data;
          node->lessSon = help->lessSon;
          node->greaterSon = help->greaterSon;
          delete help;
*/
          if (father)
            if (father->lessSon==node)
            {
              father->lessSon=node->greaterSon;
#if BINTREE_FATHER
              if (father->lessSon) father->lessSon->father=father;
#endif
            }
            else
            {
              father->greaterSon=node->greaterSon;
#if BINTREE_FATHER
              if (father->greaterSon) father->greaterSon->father=father;
#endif
            }
          else //it is new root
          {
            root = node->greaterSon;
#if BINTREE_FATHER
            root->father = NULL;
#endif
          }
          delete node;
        }
      }
      else
      {
        if ( !node->greaterSon )
        {
/*
          BinTreeNode *help = node->lessSon;
          node->data = help->data;
          node->lessSon = help->lessSon;
          node->greaterSon = help->greaterSon;
          delete help;
*/
          if (father)
            if (father->lessSon==node)
            {
              father->lessSon=node->lessSon;
#if BINTREE_FATHER
              if (father->lessSon) father->lessSon->father=father;
#endif
            }
            else
            {
              father->greaterSon=node->lessSon;
#if BINTREE_FATHER
              if (father->greaterSon) father->greaterSon->father=father;
#endif
            }
          else //it is new root
          {
            root = node->lessSon;
#if BINTREE_FATHER
            root->father = NULL;
#endif
          }
          delete node;
        }
        else
        {
          father = node;
          BinTreeNode *help;
          help = node->greaterSon;
          do
          if (help->lessSon) { father = help; help = help->lessSon; }
          else
          {
            node->data = help->data;
            if (father->lessSon == help)
            {
              father->lessSon = help->greaterSon;
#if BINTREE_FATHER
              if (father->lessSon) father->lessSon->father=father;
#endif
            }
            else
            {
              father->greaterSon = help->greaterSon;
#if BINTREE_FATHER
              if (father->greaterSon) father->greaterSon->father=father;
#endif
            }

            delete help;
            help = NULL;
          }
          while (help);
        }
      }
      node=NULL;    //the work is over
      count--;
    }
    else
    {
      father = node;
      if ( KeyTraits::IsLess(item,node->data) )
        node = node->lessSon;
      else
        node = node->greaterSon;
    };
  }
  return true;
}

#if BINTREE_FATHER
//delete node
template <class Type, class Traits>
bool BinTree<Type, Traits>::Delete(BinTreeNode *&delnode)
{
  BinTreeNode *father = delnode->father;
  BinTreeNode *node = delnode;
  delnode = NULL;
  if ( !node->lessSon )
  {
    if ( !node->greaterSon )
    {
      if (father)
        if (father->lessSon==node)
          father->lessSon=NULL;
        else
          father->greaterSon=NULL;
      else if (node==root) root=NULL;
      delete node;
    }
    else
    {
      if (father)
        if (father->lessSon==node)
        {
          father->lessSon=node->greaterSon;
          if (father->lessSon) father->lessSon->father=father;
        }
        else
        {
          father->greaterSon=node->greaterSon;
          if (father->greaterSon) father->greaterSon->father=father;
        }
      else //it is new root
      {
        root = node->greaterSon;
        root->father = NULL;
      }
      delete node;
    }
  }
  else
  {
    if ( !node->greaterSon )
    {
      if (father)
        if (father->lessSon==node)
        {
          father->lessSon=node->lessSon;
          if (father->lessSon) father->lessSon->father=father;
        }
        else
        {
          father->greaterSon=node->lessSon;
          if (father->greaterSon) father->greaterSon->father=father;
        }
      else //it is new root
      {
        root = node->lessSon;
        root->father = NULL;
      }
      delete node;
    }
    else
    {
      father = node;
      BinTreeNode *help;
      help = node->greaterSon;
      do
      if (help->lessSon) { father = help; help = help->lessSon; }
      else
      {
        node->data = help->data;
        if (father->lessSon == help)
        {
          father->lessSon = help->greaterSon;
          if (father->lessSon) father->lessSon->father=father;
        }
        else
        {
          father->greaterSon = help->greaterSon;
          if (father->greaterSon) father->greaterSon->father=father;
        }
        delnode=node;
        delete help;
        help = NULL;
      }
      while (help);
    }
  }
  node=NULL;    //the work is over
  count--;
  return true;
}
#endif

//clear all items of tree
template <class Type, class Traits>
void BinTree<Type, Traits>::Clear()
{
  {
    BinTreeIterator<Type, Traits> iterator(*this,BTMPostOrder);
    BinTreeNode *member;
    Type item;
    while((member=iterator.Next(item))!=NULL)
      delete member;
  }
  count=0;
  root=NULL;
}

//find the item
template <class Type, class Traits>
const Type *BinTree<Type, Traits>::Find(const Type &item) const
{
  BinTreeNode *node = root;
  while (node)
  {
    if ( KeyTraits::IsEqual(node->data,item) ) return &node->data;
    else
      if ( KeyTraits::IsLess(item,node->data) )
        node = node->lessSon;
      else
        node = node->greaterSon;
  };
  return NULL;
}

#pragma warning( push )
#pragma warning( disable : 4181 )
template <class Type, class Traits>
const Type *BinTree<Type, Traits>::FindKey(const typename Traits::KeyType val) const
{
  BinTreeNode *node = root;
  while (node)
  {
    if ( KeyTraits::KeyIsEqual(val,node->data) ) return &node->data;
    else
      if ( KeyTraits::KeyIsLess(val,node->data) )
        node = node->lessSon;
      else
        node = node->greaterSon;
  };
  return NULL;
}
#pragma warning( pop )

template <class Type, class Traits>
typename BinTree<Type, Traits>::BinTreeNode *BinTree<Type, Traits>::FindNode(const Type &item)
{
  BinTreeNode *node = root;
  while (node)
  {
    if ( KeyTraits::IsEqual(node->data, item) ) return node;
    else
      if ( KeyTraits::IsLess(item, node->data) )
        node = node->lessSon;
      else
        node = node->greaterSon;
  };
  return NULL;
}

template <class Type, class Traits>
const Type * BinTree<Type, Traits>::GetMin()
{
  BinTreeNode *node=root;
  if(node==NULL)
    return NULL;
  else
  {
    while(node->lessSon!=NULL)
      node=node->lessSon;
    return &node->data;
  }
}

template <class Type, class Traits>
const Type * BinTree<Type, Traits>::GetMax()
{
  BinTreeNode *node=root;
  if(node==NULL)
    return NULL;
  else
  {
    while(node->greaterSon!=NULL)
      node=node->greaterSon;
    return &node->data;
  }
}

template <class Type, class Traits>
const Type * BinTree<Type, Traits>::GetFirstGreaterOrEqual(typename Traits::KeyType val)
{
  BinTreeNode *node=root,*result=NULL;
  while(node!=NULL)
  {
    if ( KeyTraits::KeyIsEqual(val,node->data) ) return &node->data;
    else
      if ( KeyTraits::KeyIsLess(val,node->data) )
      {
        result=node;
        node = node->lessSon;
      }
      else
        node = node->greaterSon;
  }
  return (result==NULL) ? NULL : &result->data;
}

template <class Type, class Traits>
const Type * BinTree<Type, Traits>::GetFirstLessOrEqual(typename Traits::KeyType val)
{
  BinTreeNode *node=root,*result=NULL;
  while(node!=NULL)
  {
    if ( KeyTraits::KeyIsEqual(val, node->data) ) return &node->data;
    else
      if ( KeyTraits::KeyIsLess(val,node->data) )
        node = node->lessSon;
      else
      {
        result=node;
        node = node->greaterSon;
      }
  }
  return (result==NULL) ? NULL : &result->data;
}

template <class Type, class Traits>
const Type *  BinTree<Type, Traits>::GetFirstGreater(typename Traits::KeyType val)
{
  BinTreeNode *node=root,*result=NULL;
  while(node!=NULL)
  {
    if ( KeyTraits::KeyIsEqual(val, node->data) )
    {
      if(node->greaterSon==NULL)
        return (result==NULL) ? NULL : &result->data;
      result=node->greaterSon;
      while(result->lessSon!=NULL)
        result=result->lessSon;
      return &result->data;
    }
    else
      if ( KeyTraits::KeyIsLess(val,node->data) )
      {
        result=node;
        node = node->lessSon;
      }
      else
        node = node->greaterSon;
  }
  return (result==NULL) ? NULL : &result->data;
}

template <class Type, class Traits>
const Type * BinTree<Type, Traits>::GetFirstLess(typename Traits::KeyType val)
{
  BinTreeNode *node=root,*result=NULL;
  while(node!=NULL)
  {
    if ( KeyTraits::KeyIsEqual(val, node->data))
    {
      if(node->lessSon==NULL)
        return (result==NULL) ? NULL : &result->data;
      result=node->lessSon;
      while(result->greaterSon!=NULL)
        result=result->greaterSon;
      return &result->data;
    }
    else
      if ( KeyTraits::KeyIsLess(val, node->data) )
        node = node->lessSon;
      else
      {
        result=node;
        node = node->greaterSon;
      }
  }
  return (result==NULL) ? NULL : &result->data;
}

//
//BinTreeIterator implementation
//
template <class Type, class Traits>
typename BinTree<Type, Traits>::BinTreeNode * BinTreeIterator<Type, Traits>::Next( Type &target )
{
  switch (method)
  {
  case BTMPreOrder:
    return NextPreOrder(target);;
  case BTMInOrder :
    return NextInOrder(target);
  default:   //case BTMPostOrder:
    return NextPostOrder(target);
  }
}

template <class Type, class Traits>
typename BinTree<Type, Traits>::BinTreeNode *BinTreeIterator<Type, Traits>::NextInOrder(Type &target)
{
  BinTreeStackItem tmp;
  while(!stack.IsEmpty())
  {
    BinTreeStackItem &temp=stack.Top();
    switch (temp.state)
    {
    case NothingDone:
      temp.state=FirstSonDone;
      if((temp.member->lessSon)!=NULL)
      {
        tmp.state=NothingDone;
        tmp.member=temp.member->lessSon;
        stack.Push(tmp);
      }
      break;
    case FirstSonDone:
      temp.state=NodeDone;
      target = temp.member->data;
      return temp.member;
    case NodeDone:
      temp.state=SecondSonDone;
      if((temp.member->greaterSon)!=NULL)
      {
        tmp.state=NothingDone;
        tmp.member=temp.member->greaterSon;
        stack.Push(tmp);
      }
      break;
    case SecondSonDone:
      stack.Pop();
      break;
    }
  }
  return NULL;
}

template <class Type, class Traits>
typename BinTree<Type, Traits>::BinTreeNode *BinTreeIterator<Type, Traits>::NextPreOrder(Type &target)
{
  BinTreeStackItem tmp;
  while(!stack.IsEmpty())
  {
    BinTreeStackItem &temp=stack.Top();
    switch (temp.state)
    {
    case NothingDone:
      temp.state=NodeDone;
      target = temp.member->data;
      return temp.member;
    case FirstSonDone:
      temp.state=SecondSonDone;
      if (temp.member->greaterSon!=NULL)
      {
        tmp.state=NothingDone;
        tmp.member=temp.member->greaterSon;
        stack.Push(tmp);
      }
      break;
    case NodeDone:
      temp.state=FirstSonDone;
      if(temp.member->lessSon!=NULL)
      {
        tmp.state=NothingDone;
        tmp.member=temp.member->lessSon;
        stack.Push(tmp);
      }
      break;
    case SecondSonDone:
      stack.Pop();
      break;
    }
  }
  return NULL;
}

template <class Type, class Traits>
typename BinTree<Type, Traits>::BinTreeNode *BinTreeIterator<Type, Traits>::NextPostOrder(Type &target)
{
  BinTreeStackItem tmp;
  while(!stack.IsEmpty())
  {
    BinTreeStackItem &temp=stack.Top();
    switch (temp.state)
    {
    case NothingDone:
      temp.state=FirstSonDone;
      if(temp.member->lessSon!=NULL)
      {
        tmp.state=NothingDone;
        tmp.member=temp.member->lessSon;
        stack.Push(tmp);
      }
      break;
    case FirstSonDone:
      temp.state=SecondSonDone;
      if(temp.member->greaterSon!=NULL)
      {
        tmp.state=NothingDone;
        tmp.member=temp.member->greaterSon;
        stack.Push(tmp);
      }
      break;
    case NodeDone:
      stack.Pop();
      break;
    case SecondSonDone:
      temp.state=NodeDone;
      target = temp.member->data;
      return temp.member;
    }
  }
  return NULL;
}

/* 
//Example 1:
  typedef BinTree<TrialType> ValueTree;
  ValueTree tree;
  TrialType d; d.a = 10; d.b = 10.0f;
  tree.Insert(d);
  TrialType c; c.a = 12; c.b = 12.0f;
  tree.Insert(c);
  TrialType b; b.a = 13; b.b = 13.0f;
  tree.Insert(b);
  TrialType a; a.a = 11; a.b = 11.0f;
  tree.Insert(a);
  a.a = 5; a.b = 5.0f;
  tree.Insert(a);
  //ok
  tree.Delete(d);
  //ok
  ValueTree::Iterator iterator(tree, BTMInOrder);
  TrialType item;
  printf("Ordered list:\n");
  while (iterator.Next(item)) printf("item = %d, %f\n", item.a, item.b);

//Example 2:
  typedef BinTree<Ref<TrialTypeRef> > MyTree; //partial specialization for Ref used
  MyTree refTree;
  Ref<TrialTypeRef> x = new TrialTypeRef; x->a=10; x->b=10; refTree.Insert(x);
  Ref<TrialTypeRef> y = new TrialTypeRef; y->a=12; y->b=12; refTree.Insert(y);
  Ref<TrialTypeRef> z = new TrialTypeRef; z->a=13; z->b=13; refTree.Insert(z);
  Ref<TrialTypeRef> q = new TrialTypeRef; q->a=11; q->b=11; refTree.Insert(q);
  Ref<TrialTypeRef> w = new TrialTypeRef; w->a=5; w->b=5; refTree.Insert(w);
  MyTree::Iterator refIterator(refTree, BTMInOrder);
  Ref<TrialTypeRef> refItem;
  printf("Ordered list:\n");
  while (refIterator.Next(refItem)) printf("item = %d, %f\n", refItem->a, refItem->b);

  int max = (*refTree.GetMax())->a;
  int min = (*refTree.GetMin())->a;
  int less = (*refTree.GetFirstLess(10))->a;
  int greater = (*refTree.GetFirstGreater(10))->a;
  int ge = (*refTree.GetFirstGreaterOrEqual(10))->a;
  int le = (*refTree.GetFirstLessOrEqual(10))->a;
  printf("10 is: >%d, <%d, <=%d, >=%d\nmax=%d, min=%d\n", less, greater, ge, le, max, min);

  int k=5;
  const Ref<TrialTypeRef> *item = refTree.FindKey(k);
  if (item)
    printf("item with key=%d has been found\n", k);
  else 
    printf("item with key=%d has NOT been found\n", k);
*/
#endif
