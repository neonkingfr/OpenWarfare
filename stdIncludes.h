#ifdef _MSC_VER
#pragma once
#endif

#ifndef _STD_INCLUDES_H
#define _STD_INCLUDES_H
// precompiled header files - many files use something of Windows API

// C libraries
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <math.h>
#include <stdio.h>
#include <float.h>
#include <stddef.h>

#endif
