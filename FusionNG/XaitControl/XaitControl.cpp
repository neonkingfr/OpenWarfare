#include "XaitControl.h"

#define _DIAG 1 // Enables/disables diagnostic messages

#pragma region Servicecode

#pragma region GlobalDefinitions
#include <xait/common/callback/FileIOCallbackWin32.h>

#include <data/Unit.h>
#include <data/ControllableObject.h>
#include <util/ControllableObjectUtilities.h>

#define FSM_SET_NAME "xaitControlVBS2.xml"
#define DLL_DIRECTORY "pluginsFusion\\dll"

#if _X64
#if _DEBUG
#define XAITCOMMON_DLL "xaitcommon-win64D-vc80shared.dll"
#define XAITCONTROL_DLL "xaitcontrol-win64D-vc80shared.dll"
#else
#define XAITCOMMON_DLL "xaitcommon-win64-vc80shared.dll"
#define XAITCONTROL_DLL "xaitcontrol-win64-vc80shared.dll"
#endif

#else

#if _DEBUG
#define XAITCOMMON_DLL "xaitcommon-win32D-vc80shared.dll"
#define XAITCONTROL_DLL "xaitcontrol-win32D-vc80shared.dll"
#else
#define XAITCOMMON_DLL "xaitcommon-win32-vc80shared.dll"
#define XAITCONTROL_DLL "xaitcontrol-win32-vc80shared.dll"
#endif
#endif //_X64

#define DELAYED_DLLS 0

HMODULE XaitControlHModule = NULL;
HMODULE XaitCommonHModule = NULL;

FSMSet* gFsmSet = NULL;
float gGameTime = 0.0;
bool gIsControlInitialized = false;
FILETIME gLastWrite;
UnitFSMLookup *gUnitFSMLookup;

#pragma warning (disable: 4244)
VBS2Fusion::NetworkID NetID(int64 id[3]) { return VBS2Fusion::NetworkID((int)id[0], (int)id[1], id[2]); }
#pragma endregion GlobalDefinitions

#pragma region Diagnostics
// Logs text message into debug output
void LogDiagMessage(const char *format, ...)
{
#if _DIAG
  va_list arglist;
  va_start(arglist, format);
  char buffer[512] = { 0 };
  vsprintf_s(buffer,512,format, arglist);
  va_end(arglist);
  OutputDebugString(buffer);
#endif
}

void LogMessage(Common::String text)
{
  LogDiagMessage(text.getConstCharPtr());
}

void ShowFSMDiagnostics(float deltaT)
{
#if _DIAG
  // Diagnostics messages about FSMs currently used by unit and about states those FSMs are in.
  // Shows in debug output and also above unit's head in VBS2.
  for (UnitFSMLookupIterator unitIterator = gUnitFSMLookup->begin(); unitIterator != gUnitFSMLookup->end(); unitIterator++)
  {
    Container::Vector<FSMInstance *> *instances = (*unitIterator).second;
    if (!instances) continue;

    Common::String output = "XaitControl";
    for (FSMInstancesIterator fsmIterator = instances->begin(); fsmIterator != instances->end(); fsmIterator++)
    {
      (*fsmIterator)->update(deltaT * 1000.0f);

      Common::String fsmName =  (*fsmIterator)->getCurrentFSM()->getName();
      VBS2Fusion::Unit unit((VBS2Fusion::NetworkID)(*unitIterator).first);
      if(VBS2Fusion::ControllableObjectUtilities::ControllableObjectExists(unit))
      {
        output += "\\n[" + fsmName + "]:" + (*fsmIterator)->getCurrentStateName();
        VBS2Fusion::ControllableObjectUtilities::applyDisplayText(unit, output.getConstCharPtr());
      }
#endif
    }
  }
}
#pragma endregion Diagnostics

#pragma region FSMMaintenance
void RestartAllFSM()
{
  if(!gIsControlInitialized)
    return;

  //check for file changed
  WIN32_FILE_ATTRIBUTE_DATA fileInfo;
  GetFileAttributesEx(FSM_SET_NAME, GetFileExInfoStandard, &fileInfo);
  FILETIME lw = fileInfo.ftLastWriteTime;
  if (lw.dwLowDateTime == gLastWrite.dwLowDateTime && lw.dwHighDateTime == gLastWrite.dwHighDateTime)
  {
    LogDiagMessage(LOG_HEADER" [Restart] not needed, file not changed.\n"); 
    return;
  }
  gLastWrite = lw;    

  // delete old FSM pointers (but store the FSM names)
  Container::LookUp<VBS2Fusion::NetworkID, Container::Vector<Common::String>* > dump(Common::Interface::getGlobalAllocator());

  UnitFSMLookup::Iterator iter = gUnitFSMLookup->begin();
  UnitFSMLookup::Iterator iterEnd = gUnitFSMLookup->end();

  for(; iter!= iterEnd; ++iter)
  {
    Container::Vector<Common::String>* fsmNames = new Container::Vector<Common::String>(iter->second->size(),Common::Interface::getGlobalAllocator());
    dump.add(iter->first, fsmNames);

    Container::Vector<FSMInstance *>::Iterator innerIter = (*iter).second->begin();
    Container::Vector<FSMInstance *>::Iterator innerIterEnd = (*iter).second->end();

    for(; innerIter!= innerIterEnd; ++innerIter)
    {
      fsmNames->pushBack((*innerIter)->getRootFSM()->getName());
      delete (*innerIter);
    }

    (*iter).second->clear();
  }

  //unload and reload the FSMSet
  FSMManager* fsmManager= Control::Interface::getFSMManager();
  fsmManager->unloadFSMSet(gFsmSet);

  fsmManager->loadFSMSet(FSM_SET_NAME);

  //restore FSMInstance from names
  Container::LookUp<VBS2Fusion::NetworkID, Container::Vector<Common::String>* >::Iterator nameIter = dump.begin();
  Container::LookUp<VBS2Fusion::NetworkID, Container::Vector<Common::String>* >::Iterator nameIterEnd = dump.end();

  for(; nameIter != nameIterEnd; ++ nameIter)
  {
    Container::Vector<FSMInstance *>* insertVec = gUnitFSMLookup->find(nameIter->first)->second;

    X_ASSERT_DBG(insertVec->empty());


    Container::Vector<Common::String>::Iterator innerIter = nameIter->second->begin();
    Container::Vector<Common::String>::Iterator innerIterEnd = nameIter->second->end();

    for(; innerIter!= innerIterEnd; ++innerIter)
    {
      FSM* fsm = gFsmSet->getFSM(*innerIter);
      FSMInstance* fsmInstance = fsm->createFSMInstance();
      //TODO: register default functions and custom functions
      insertVec->pushBack(fsmInstance);
    }

    nameIter->second->clear();
    delete nameIter->second;
  }
}
// Searches for FSM instance of given FSM in vector of instances
// Returns iterator of the instance in the vector if found; returns NULL otherwise
static FSMInstancesIterator FindFSMInstanceByFSM(Container::Vector<FSMInstance *> &instances, FSM *fsm)
{
  if (fsm == NULL)
    return instances.end();

  int i=0;
  for (FSMInstancesIterator iterator = instances.begin(); iterator != instances.end(); iterator++)
  {
    if ((*iterator)->getCurrentFSM() == fsm)
      return iterator;
    i++;
  }

  return instances.end();
}

static FSMInstance *CreateFSMInstance(FSM *fsm, const char *instanceName)
{
  FSMInstance *instance = fsm->createFSMInstance
    (
    instanceName
#if _DIAG
    ,true
#endif
    );
  if (!instance)
  {
    LogDiagMessage(LOG_HEADER"Unable to create instance of FSM '%s'.\n", instanceName);
  }

  return instance;
}

VBSPLUGIN_EXPORT void WINAPI SetFSM(const char * name, int64 id[3], const char* fsmName)
{
  if (!gIsControlInitialized)
      return;

  // calling of SetFSM(NULL,"*") causes restart of all FSMs
  if (fsmName && fsmName[0]=='*' && fsmName[1]=='\0') // 
    return RestartAllFSM();

  FSM *fsm = gFsmSet->getFSM(fsmName);
  if (!fsm)
  {
    LogDiagMessage(LOG_HEADER"Unable to find FSM of name '%s'.\n", fsmName);
    return;
  }

  VBS2Fusion::NetworkID nid = NetID(id);

  UnitFSMLookupIterator unitFSMIterator = gUnitFSMLookup->find(nid);

  // No FSM yet for the given unit
  if (unitFSMIterator == gUnitFSMLookup->end())
  {
    FSMInstance *instance = CreateFSMInstance(fsm, name);
    if (instance)
    {
      UnitFSMLookupIterator newItem = gUnitFSMLookup->add(nid, new Container::Vector<FSMInstance *>(Common::Interface::getGlobalAllocator()));
      (*newItem).second->pushBack(instance);
      instance->registerStaticFunction("LogDiagMessage", &LogMessage);
      instance->start();
      // New FSM added
    }
    return;
  }

  // Do not allow two instances of the same FSM for one unit
  Container::Vector<FSMInstance *> *instances  = unitFSMIterator != gUnitFSMLookup->end() ? (*unitFSMIterator).second : NULL;
  if (instances && FindFSMInstanceByFSM(*instances, fsm) != instances->end())
  {
    LogDiagMessage(LOG_HEADER" [SetFSM] %s already running.\n",fsmName);
    return;
  }

  // Unit already has some FSMs assigned but not this one yet, let's assign it then
  FSMInstance *instance = CreateFSMInstance(fsm, name);
  if (instance)
  {
    (*unitFSMIterator).second->pushBack(instance);
    instance->start();
  }

  return;
};

// Functions removing FSM of given name from unit identified by given id
VBSPLUGIN_EXPORT void WINAPI RemoveFSM(int64 id[3], const char* fsmName)
{
  //if (!XaitControlLoaded)
  //  return;

  FSM *fsm = gFsmSet->getFSM(fsmName);
  if (!fsm)
  {
    LogDiagMessage(LOG_HEADER"Unable to find FSM of name '%s'.\n", fsmName);
    return;
  }

  VBS2Fusion::NetworkID nid = NetID(id);

  UnitFSMLookupIterator unitFSMIterator = gUnitFSMLookup->find(nid);
  if (unitFSMIterator != gUnitFSMLookup->end())
    return;

  Container::Vector<FSMInstance *> *instances  = unitFSMIterator != gUnitFSMLookup->end() ? (*unitFSMIterator).second : NULL;
  if (instances)
  {
    FSMInstancesIterator instance = FindFSMInstanceByFSM(*instances, fsm);
    if (instance != instances->end())
      (*unitFSMIterator).second->erase(instance);
  }

  return;
}
#pragma endregion FSMMaintenance

#pragma region Deinit
//! Function delay loading xaitConrtrol dlls
bool LoadXaitmentDlls()
{
	//Load xaitCommon dll
	{
		XaitCommonHModule = LoadLibrary(DLL_DIRECTORY"\\"XAITCOMMON_DLL);
		if (XaitCommonHModule == NULL)
		{
			LogDiagMessage(LOG_HEADER"Library '%s' not found.\n", XAITCOMMON_DLL);
			return false;
		}
	}

	//Load xaitControl dll
	{
		XaitControlHModule = LoadLibrary(DLL_DIRECTORY"\\"XAITCONTROL_DLL);
		if (XaitControlHModule == NULL)
		{
			LogDiagMessage(LOG_HEADER"Library '%s' not found.\n", XAITCONTROL_DLL);
			FreeLibrary(XaitCommonHModule);
			return false;
		}
	}

  gIsControlInitialized = true;
	return true;
}

void Init() 
{
#if DELAYED_DLLS
  if (!LoadXaitmentDlls())
    return;
#endif

  if(gIsControlInitialized)
    return;

  //create callbacks and initialize xaitControl
  Common::Callback::FileIOCallback::Ptr	ioCallback(new Common::Callback::FileIOCallbackWin32());
  Common::Callback::MathCallback::Ptr		mathCallback(new Common::Callback::MathCallback());

  Common::Interface::initLibrary();
  Common::Network::InitializeNetwork();
  Control::Interface::initLibrary(ioCallback, mathCallback);  

  gUnitFSMLookup = new UnitFSMLookup(Common::Interface::getGlobalAllocator());

  //load the FSMSet
  FSMManager* fsmManager= Control::Interface::getFSMManager();
  gFsmSet  = fsmManager->loadFSMSet(FSM_SET_NAME);  
  if (gFsmSet)
    LogDiagMessage(LOG_HEADER" %s loaded\n",FSM_SET_NAME);
  else
    LogDiagMessage(LOG_HEADER" %s load failed\n",FSM_SET_NAME);

  WIN32_FILE_ATTRIBUTE_DATA fileInfo;
  GetFileAttributesEx(FSM_SET_NAME, GetFileExInfoStandard, &fileInfo);
  gLastWrite = fileInfo.ftLastWriteTime;

  gIsControlInitialized = true;
}
#pragma endregion Init

#pragma region Deinit
void FreeXaitmentDlls()
{
  if (XaitControlHModule != NULL)
    FreeLibrary(XaitControlHModule);
  if (XaitCommonHModule != NULL)
    FreeLibrary(XaitCommonHModule);

  XaitControlHModule = NULL;
  XaitCommonHModule = NULL;
}

void Close()
{
  if(!gIsControlInitialized)
    return;

  gIsControlInitialized = false;

  //TODO: delete ptr to vectors too
  delete gUnitFSMLookup;

  // release all FSMs and all Units
  Control::Interface::closeLibrary();
  Common::Network::CloseNetwork();
  Common::Interface::closeLibrary();

#if !DELAYED_DLLS
  FreeXaitmentDlls();
#endif
};
#pragma endregion Deinit

#pragma endregion ServiceCode

VBSPLUGIN_EXPORT void WINAPI OnSimulationStep(float deltaT)
{
  // Do nothing when the libraries were not loaded
  if (!gIsControlInitialized)
    return;

  // Call the time update for FSMs
  FSMManager* fsmManager= Control::Interface::getFSMManager();
  fsmManager->update(gGameTime * 1000.0f);
  gGameTime += deltaT;
  
#if _DIAG
  // Show diagnostics about FSM and state for each unit in debug console and in VBS2 above unit head.
  ShowFSMDiagnostics(deltaT);
#endif
}

// Function called each time danger state of a unit is changed in VBS2
VBSPLUGIN_EXPORT void WINAPI UpdateDangerFSM(int64 id[3], int cause, const float position[3], float until)
{
}

VBSPLUGIN_EXPORT int WINAPI OnLoad(VBSParameters *params)
{
  Init();
  return 0;
}

VBSPLUGIN_EXPORT void WINAPI OnUnload()
{
  Close();
}

VBSPLUGIN_EXPORT void WINAPI OnMissionStart()
{
  gGameTime = 0;
  FSMManager* fsmManager= Control::Interface::getFSMManager(); 
  fsmManager->setGametime( 0 );
  RestartAllFSM();
}

VBSPLUGIN_EXPORT void WINAPI OnMissionEnd()
{
  gUnitFSMLookup->clear();
}

BOOL APIENTRY DllMain(HMODULE hModule, DWORD ul_reason_for_call, LPVOID lpReserved)
{
  switch(ul_reason_for_call)
  {
  case DLL_PROCESS_ATTACH:
    Init();
    break;
  case DLL_PROCESS_DETACH:
    break;
  case DLL_THREAD_ATTACH:
    break;
  case DLL_THREAD_DETACH:
    break;
  }
  return TRUE;
}