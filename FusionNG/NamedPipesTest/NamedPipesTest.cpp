#include <Windows.h>
#include <string>
#include "NamedPipesTest.h"
#include "resource.h"

///////////////////////////////////////////////////////////////////////////////
// Procedure of new callback addition                                        //
//---------------------------------------------------------------------------//
// - add new callback to CALLBACKS factory                                   //
// - if number of callbacks exceeds MAX_CALLBACKS_SELECTED it has to be      //
//   extended                                                                //    
///////////////////////////////////////////////////////////////////////////////

// create dependency on Fusion
#include "VBS2FusionAppContext.h"
VBS2Fusion::VBS2FusionAppContext appContext;

HWND hWnd; ///< dialog window handle
HWND hOut; ///< output control handle
HWND hList; ///< list control handle

std::string output; ///< ouput string

bool pause = false; ///< pause output flag

class Matrix4P;
class GameValue;
struct VBSParameters;
struct VBSParametersEx;

typedef float (WINAPI *VIRTUALINPUTDEVICE_DEVICE_GETVALUE)(UINT device_id);
typedef UINT  (WINAPI *VIRTUALINPUTDEVICE_DEVICE_REGISTERCALLBACK)(const char *plugin_name, UINT device_id, UINT device_type, const char *device_description, VIRTUALINPUTDEVICE_DEVICE_GETVALUE fptr_get_device_value);
typedef UINT (WINAPI *VIRTUALINPUTDEVICE_DEVICE_UNREGISTERCALLBACK)(const char *plugin_name, UINT device_id);


//! Dialog's procedure function.
INT_PTR CALLBACK DlgProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
  return FALSE;
}

//! Create window.
void TestNamedPipes(LPVOID hInst)
{
  //Small test of NamedPipeServer, we're expecting that server is simple repeater which will retransmit everything that it receives.
  NamedPipeClient npc;
  npc.Connect("testPipe");
  NamedPipeClient::Error err = npc.GetLastError();
  DWORD err2 = npc.GetLastIOError();
  npc.SendString("Hello world");
  std::string mess = npc.ReceiveString();
  npc.SendInt(25);
  npc.SendBool(true);
  npc.SendFloat(455e25f);
  npc.SendDouble(800.808080);
  int in1 = npc.ReceiveInt();
  bool in2 = npc.ReceiveBool();
  float in3 = npc.ReceiveFloat();
  double in4 = npc.ReceiveDouble();
  npc.Disconnect();
}

//! Main DLL function
BOOL WINAPI DllMain(HINSTANCE hDll, DWORD fdwReason, LPVOID lpvReserved)
{
  switch(fdwReason)
  {
  case DLL_PROCESS_ATTACH:
    TestNamedPipes(hDll);
    break;
  case DLL_PROCESS_DETACH:
    break;
  case DLL_THREAD_ATTACH:
    break;
  case DLL_THREAD_DETACH:
    break;
  }
  return TRUE;
}