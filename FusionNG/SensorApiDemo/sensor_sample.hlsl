// The SENSOR_SHADER macro contains helper for accessing sensor peel data & helpers

// Accessing sensor data:
// Use:
//   sensorPeelData pd = getSensorPeelData(In.uv);
// sensorPeelData struct contains:
//   float4 rgba;    /* 'Out the window' RGB color + alpha information*/
//   int matID;      /* Material index (0-255)*/
//   float tiHT;     /* Engine TI half cooling time */
//   float tiHeat;   /* Engine TI heat emit maps */
//   float sunLastH; /*How much of the last hour this pixel has been on the sun (0.0-1.0)*/
//   float nDotL;    /*Dot product of surface normal & main light ('how much is this pixel surface towards the sun', 0.0 - 1.0)*/
//   float3 lightIndirect; /*Indirect light color*/
//   float3 lightDirect;   /*Direct light color*/
//   float3 lightSpecular; /* Specular light color*/

// Globals:
//  lightDirSunMoon.xyz = Main light (sun or moon) direction in world space 
//  lightDirSunMoon.w   = Main light sun or moon factor (0 = moon, 1 = sun) 
//  clipPlane.x  = Near clip plane distance (0.0 - 1.0)
//  clipPlane.y  = Far clip plane distance (0.0 - 1.0)
//  camPos.xyz   = Camera position X Y Z in world space
//  camDir.xyz   = Camera direction vector X Y Z 
//  camUp.xyz    = Camera up vector X Y Z 

SENSOR_SHADER;

// Material temperatures
#define TEMP_MIN 6
#define TEMP_MAX 19

#define TEMP_BASELINE 60
#define TEMP_HEAT_MAX 100

static float tEnv[] =  {65, 67, 69, 71, 74, 75, 76, 77, 78, 77, 75, 73, 70, 67};
static float tWood[] = {67, 69, 71, 71, 71, 71, 71, 71, 71, 71, 71, 71, 70, 67};
static float tConstructionMaterial[] = {64, 65, 67, 71, 75, 77, 78, 79, 80, 78, 75, 72, 69, 66};
static float tThickGlass[] = {64, 65, 67, 70, 74, 78, 80, 80, 82, 80, 76, 73, 69, 65};
static float tPlastic[] = {64, 66, 69, 72, 75, 77, 78, 78, 80, 78, 75, 72, 69, 65};
static float tMetal[] = {62, 64, 65, 66, 70, 75, 80, 82, 83, 82, 75, 71, 67, 65};
static float tCeramic[] = {63, 65, 67, 69, 74, 77, 78, 80, 82, 80, 77, 73, 67, 65};

float4 daytime_x_x_x : register(c0); // daytime in hours

// In sensorSample::createResources we created define macros for all Material IDs in materialMap with mat prefix.
// Value of the define is the Material ID for that material.
// For example value of 'matMetalSteel' is 7

float4 psmain(in VS_OUTPUT In) : COLOR
{   
    sensorPeelData pd = getSensorPeelData(In.uv);
    
    // Check for sky material (we set this in sensorSample::getDefaultMaterial)
    if(pd.matID == matSky)
    {
        // Sky, draw opaque static color
        return float4(0.4,0.4,0.4,1);
    }
    
    // Draw everything as medium gray
    float lum = 0.33;
    
    // Mix in a bit of red color for detail
    lum += pd.rgba.r * 0.05;
    
    // Add ti heat, this contains vehicle engines, tires, weapons and metabolism
    lum += pd.tiHeat;
    
    // In sensorSample::getDefaultMaterial we set the default material for buildings as "WoodUntreated". Make that material semi-transparent here.
    float alpha;
    if(pd.matID == matWoodUntreated)
    {   
        alpha = 0.85;
    }
    else
    {
        alpha = 1.0;
    }
        
    // Determine index for temperature arrays above based on daytime
    float dt = daytime_x_x_x.x;
    int a = floor(min(dt - TEMP_MIN, TEMP_MAX-TEMP_MIN)); // Current hour
    int b = min(a+1, TEMP_MAX-TEMP_MIN); // Next hour
    float tc, tn;
    
    // Pick temperature from temperature arrays above
    if(pd.matID == matCeramic) { tc = tCeramic[a]; tn = tCeramic[b];}
    else if(pd.matID == matMetalAluminium || pd.matID == matMetalComposite || 
            pd.matID == matMetalSteel || pd.matID == matMetalSteelGalvanized ||
            pd.matID == matMetalTitanium) {tc = tMetal[a]; tn = tMetal[b];}
    else if(pd.matID == matPlastic) {tc = tPlastic[a]; tn = tPlastic[b];}
    else if(pd.matID == matThickGlass) {tc = tThickGlass[a]; tn = tThickGlass[b];}
    else if(pd.matID == matWoodUntreated) {tc = tWood[a]; tn = tWood[b];}
    else if(pd.matID == matConstructionAsphalt || pd.matID == matConstructionBrickRed ||
            pd.matID == matConstructionCardboard || pd.matID == matConstructionGraniteMarble ||
            pd.matID == matConstructionTreatedWood || pd.matID == matConstructionVinylSliding)
            {tc = tConstructionMaterial[a]; tn = tConstructionMaterial[b];}
    else tc = tEnv[a]; tn = tEnv[b];
    
    // Interpolate final temperature from previous and current hour
    float temp = lerp(tc, tn, dt-floor(dt));
  
    // Influence luminance by temperature
    lum = saturate(lum+(temp-TEMP_BASELINE)/(TEMP_HEAT_MAX-TEMP_BASELINE));

    // Set color to luminance value
    // Multiply alpha with pixel alpha. this is so transparent objects are drawn transparent (ie. windows)
    return float4(lum,lum,lum, alpha*pd.rgba.a);
}



