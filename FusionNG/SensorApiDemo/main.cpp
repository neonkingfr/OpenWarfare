
// Fusion or no fusion
//#define FUSION

#include <windows.h>
#include "sensorplugin.h"

#include <d3d9.h>
#include <d3dx9.h>

#include "main.h"

#include "sensorInterfaceBase.h"
#include "sensorSample.h"

#if FUSION
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// VBS2Fusion interface library forced linking
// Needed for plugins that don't use any of VBS2Fusion symbols
// This will insure that VBS2Fusion_2005 library will be linked to the plugin
//  and the plugin will require to have VBS2Fusion_2005.dll
// It is required by Simcentric that each Fusion plugin links with VBS2Fusion_2005.lib or VBS2_Fusion_2008.lib.
#include "VBS2FusionAppContext.h"
VBS2Fusion::VBS2FusionAppContext appContext;
#if _X64
#pragma comment(lib, "../_depends/Fusion/lib64/VBS2Fusion_2005.lib")
#else
#pragma comment(lib, "../_depends/Fusion/lib/VBS2Fusion_2005.lib")
#endif
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#endif

sensorInterfaceBase *sample = NULL;

// Global notes for NG drawing & sensor interface:
// - Do not attempt to access the Direct3D device directly in functions where it is not allowed, like in OnSensorFinal.
// - Do not use D3DPOOL_MANAGED pool for any resource, it is not usable in Windows Vista/7 due to Direct3D 9Ex.
// - When using Windows Vista/7, device resets behave different to Windows XP and previous VBS2 versions. To test plugin
//   reset compatibility use "-winxp" commandline parameter with VBS2NG.


// Called when a peel containing rendered data by engine is ready
// param->peelSurface are four RGBA buffers from which rendered data is found (see shader code for the structure description)
// param->peelDepth is the depth buffer for this peel
// Size and format of these buffers are indicated in parameters of OnSensorInit
// Note no Direct3D device access is available here so buffers for storing the peel data must be created earlier
VBSPLUGIN_EXPORT void WINAPI OnSensorPeel(paramSensorPeel *param, DWORD paramSize)
{
  if(sample)
  {
    // Copy peel data to buffers prepared in OnSensorInit    
    sample->doCopyBuffers(param->cbi, param->peelIndex, param->peelSurface, param->peelDepth);
  }  
}

// Called at end of frame for sensor to render to VBS backbuffer
// Replaces postprocess stage of engine rendering
VBSPLUGIN_EXPORT void WINAPI OnSensorFinal(paramSensorFinal *p, DWORD paramSize)
{
  if(sample)
  {
    sample->doPostprocess(p->cbi, p->renderTarget, p->shadowMap, &p->mainLight, p->cameraFrustum);
  }
}

VBSPLUGIN_EXPORT void WINAPI OnSensorRequest(paramSensorRequest *param, DWORD paramSize, outSensorRequest *out, DWORD outSize)
{
  // Create our sample regardless of what param was requested
  sample = new sensorSample(param->param, param->numDefaultMaterials);

  if(sample)
  {
    // Ask sample to update parameters such as width and height
    sample->updateSensorParameters();

    // Return parameters
    out->width = sample->getWidth();
    out->height = sample->getHeight();
    out->peels = sample->numPeels();
    for(int i=0;i<param->numDefaultMaterials;i++)
    {
      out->defaultMaterials[i] = sample->getDefaultMaterial(i);
    }
    for(int i=0;i<param->numTerrainMaterials;i++)
    {
      out->terrainMaterials[i] = sample->getTerrainMaterial(i);
    }
  }
}


// Called when sensor is initalized with EnableCustomSensor script command
// parameters contain real direct 3D device, so resource management can be done here
// use param->width,height,peelSurfaceFormats & peelDepthFormat to prepare buffers for data received in OnSensorPeel
// Set contents of param->materialPriority array to indicate priorized materials in last peel (>0 = priority material)
VBSPLUGIN_EXPORT void WINAPI OnSensorInit(paramSensorInit *param, DWORD paramSize)
{
  if(sample)
  {
    sample->initialize(param->dev);
    sample->prepareBuffers(param->dev, param->width, param->height, param->peels, param->peelSurfaceFormats, param->peelDepthFormat);
    sample->createResources(param->dev);
    sample->getMaterialPriority(param->materialPriority);
  }
}

// This function is called once per frame before any drawing is done by engine
// parameters contain real direct 3D device, so resource management can be done here
// Essentially the same as OnNgPreDraw from draw interface, but dedicated for sensor plugins
VBSPLUGIN_EXPORT void WINAPI OnSensorPreDraw(paramSensorPreDraw *param, DWORD paramSize)
{
  if(sample)
  {
    sample->manageResources(param->dev);
  }  
}


// This function is called when D3D device is destroyed, reset by engine or when custom sensor is turned off
// parameters contain real direct 3D device, so resource management can be done here
// All D3D resources created by the plugin must be released here, otherwise device resets will fail
VBSPLUGIN_EXPORT void WINAPI OnSensorDeviceInvalidate(paramSensorDeviceInvalidate *param, DWORD paramSize)
{
  if(sample)
  {
    sample->freeResources();
    if(!param->isReset)
    {
      // Engine is shutting down
      delete sample;
      sample = NULL;
    }
  }
}

// This function is called when D3D device is re-created after reset by engine
// parameters contain real direct 3D device, so resource management can be done here
// Resources released in OnSensorDeviceInvalidate should be re-created here
VBSPLUGIN_EXPORT void WINAPI OnSensorDeviceRestore(paramSensorDeviceRestore *param, DWORD paramSize)
{
  if(sample)
  {
    sample->createResources(param->dev);
  }
}


void LogF(const char* format, ...)
{
	va_list     argptr;
	char        str[1024];
	va_start (argptr,format);
	vsnprintf (str,1024,format,argptr);
	va_end   (argptr);
  OutputDebugString(str);
}

ExecuteCommandType ExecuteCommand = NULL;

VBSPLUGIN_EXPORT void WINAPI OnUnload() {
  // Whole plugin is unloaded, release the sample
  delete sample;
  sample = NULL;
}

VBSPLUGIN_EXPORT void WINAPI OnSimulationStep(float deltaT) {}

VBSPLUGIN_EXPORT const char* WINAPI PluginFunction(const char *inp)
{
  return "[]";
}

BOOL WINAPI DllMain(HINSTANCE hDll, DWORD fdwReason, LPVOID lpvReserved)
{
   switch(fdwReason)
   {
      case DLL_PROCESS_ATTACH:
        break;
      case DLL_PROCESS_DETACH:
        break;
   }
   return TRUE;
}

VBSPLUGIN_EXPORT void WINAPI RegisterHWND(void *hwnd, void *hins) {}

VBSPLUGIN_EXPORT void WINAPI RegisterCommandFnc(void *executeCommandFnc)
{
  ExecuteCommand = (ExecuteCommandType)executeCommandFnc;
}