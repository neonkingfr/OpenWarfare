#include "materialMap.h"
#include "main.h"

using namespace std;

void materialMap::loadMaterials()
{
  // Create materials
  // Material name(string) = material ID(int)

  _vehicleMaterials["Ceramic"] = 2;
  _vehicleMaterials["CompositeMaterial"] = 3;
  _vehicleMaterials["FabricCloth"] = 4;
  _vehicleMaterials["MetalAluminium"] = 5;
  _vehicleMaterials["MetalComposite"] = 6;
  _vehicleMaterials["MetalSteel"] = 7;
  _vehicleMaterials["MetalSteelGalvanized"] = 8;
  _vehicleMaterials["MetalTitanium"] = 9;
  _vehicleMaterials["Plastic"] = 10;
  _vehicleMaterials["ThickGlass"] = 11;
  
  _constructionMaterials["ConstructionAsphalt"] = 23;
  _constructionMaterials["ConstructionBrickRed"] = 24;
  _constructionMaterials["ConstructionCardboard"] = 25;
  _constructionMaterials["ConstructionGraniteMarble"] = 26;
  _constructionMaterials["ConstructionTreatedWood"] = 27;
  _constructionMaterials["ConstructionVinylSliding"] = 28;
  _constructionMaterials["Foam"] = 29;
  _constructionMaterials["RockBasalt"] = 30;
  _constructionMaterials["RockSandstoneVarnishedDesertFerruginuous"] = 31;
  _constructionMaterials["RoofingTile"] = 32;
  _constructionMaterials["Rubber"] = 33;
  _constructionMaterials["ThinGlass"] = 34;

  _environmentMaterials["Snow"] = 45;
  _environmentMaterials["SoilClayLoamBrownRedOrange"] = 46;
  _environmentMaterials["SoilDesertWashBasin"] = 47;
  _environmentMaterials["SoilGravel"] = 48;
  _environmentMaterials["SoilSiltDirtTopsoil"] = 49;
  _environmentMaterials["VegetationGrass"] = 50;
  _environmentMaterials["VegetationTreeCanopy"] = 51;
  _environmentMaterials["Water"] = 52;
  _environmentMaterials["WoodUntreated"] = 53;

  // Added for this sample:
  _vehicleMaterials["AliveGeneric"] = 12;
  _environmentMaterials["Sky"] = 54; 
  _environmentMaterials["SoilSand"] = 55;
  _environmentMaterials["SoilRock"] = 56;
  _environmentMaterials["SoilGrass"] = 57;

};

materialMap::MatID materialMap::getMaterial(string name)
{
  // Search material from all material classes
  if (_vehicleMaterials.count(name)) return _vehicleMaterials[name];
  if (_constructionMaterials.count(name)) return _constructionMaterials[name];
  if (_environmentMaterials.count(name)) return _environmentMaterials[name];

  LogF("Warning: Material <%s> not found!", name.c_str());
  return MatInvalid;
}