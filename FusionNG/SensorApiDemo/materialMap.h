#include <string>
#include <map>

using namespace std;

// This class hold material IDs for us and provides easy material name to material ID mapping
// Materials are stored in classes: vehicle materials, construction materials and environment materials.
// Note the classes are only for convinience ie. for setting all vehicle materials as priorized
// for the Material IDs themselves returned by this class there is no separation by class.

class materialMap
{
public:  
  typedef int MatID;   // Material ID type, valid range 0-255
  const static int MatInvalid = -1;  // Invalid material

  // std::map for (material name, material ID) pairs
  typedef map<string, MatID> MatMapNameID;

public:
  void loadMaterials();

  // Get material ID for material by name
  MatID getMaterial(string name);

  // Get material lists by type
  MatMapNameID getVehicleMaterials() {return _vehicleMaterials;};
  MatMapNameID getConstructionMaterials() {return _constructionMaterials;};
  MatMapNameID getEnvironmentMaterials() {return _environmentMaterials;};

private:
  MatMapNameID _vehicleMaterials;
  MatMapNameID _constructionMaterials;
  MatMapNameID _environmentMaterials;
};