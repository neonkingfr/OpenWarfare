// Copy "sensor_sample.hlsl" to VBS3 directory

#include "sensorSample.h"

sensorSample::sensorSample(const char *param, int numDefaultMaterials) : sensorInterfaceBase(param, numDefaultMaterials),_ps(NULL)
{
  _matMap.loadMaterials();
};

// This is called during sensor request, set wanted sensor parameters here
void sensorSample::updateSensorParameters()
{
  // Wanted sensor resolution. Increased resolution will significantly increase required video ram
  _width = 1024;
  _height = 1024;

  // Number of depth peels wanted, more peels increases video ram usage and decreases render performance
  _peels = 4;
}

// Helper: creates D3DXMACRO from material map material
void materialToD3DXMACRO(D3DXMACRO *m, std::string str, materialMap::MatID matid)
{
  m->Name = new char[256];
  m->Definition = new char[5];
  _snprintf((char*)m->Name, 255, "mat%s", str.c_str());
  _snprintf((char*)m->Definition, 4, "%d", matid);
}

void sensorSample::createResources(IDirect3DDevice9 *d3d)
{
  // Create D3DXMACROs for shader compiler defines, out of all materials in materialMap
  // This will allow us to use material names in HLSL directly (with "mat" prefix).
  // Creates for example for "Plastic", which is defined in materialMap as Material ID 10, matPlastic define with value 10 in HLSL
  // so we can do:
  //   if(pd.matID == matPlastic) ...
  // in HLSL code
  std::vector<D3DXMACRO> macros;

  // Iterate vehicle materials and create macros for their IDs
  materialMap::MatMapNameID v = _matMap.getVehicleMaterials();
  for(materialMap::MatMapNameID::iterator i = v.begin(); i != v.end(); ++i)
  {
    // i->first is the name of material as std::string from materialMap::MatMapNameID.
    // i->second is MatID of materialMap::MatMapNameID, which is the Material ID of this material.
    D3DXMACRO m;
    materialToD3DXMACRO(&m, i->first, i->second);
    macros.push_back(m);
  }

  // Iterate construction materials and create macros for their IDs
  materialMap::MatMapNameID c = _matMap.getConstructionMaterials();
  for(materialMap::MatMapNameID::iterator i = c.begin(); i != c.end(); ++i)
  {
    D3DXMACRO m;
    materialToD3DXMACRO(&m, i->first, i->second);
    macros.push_back(m);
  }

  // Iterate environment materials and create macros for their IDs
  materialMap::MatMapNameID e = _matMap.getEnvironmentMaterials();
  for(materialMap::MatMapNameID::iterator i = e.begin(); i != e.end(); ++i)
  {
    D3DXMACRO m;
    materialToD3DXMACRO(&m, i->first, i->second);
    macros.push_back(m);
  }

  // Compile our sensor pixel shader
  _ps = compilePixelShader(d3d, "sensor_sample.hlsl", "psmain", macros);

  // Free strings allocated for D3DXMACROs
  for(std::vector<D3DXMACRO>::iterator i = macros.begin(); i != macros.end(); ++i)
  {
    delete[] i->Definition;
    delete[] i->Name;
  }
}

void sensorSample::freeResources()
{
  // Release all direct3D handles
  SAFE_RELEASE(_ps); 
}

unsigned char sensorSample::getDefaultMaterial(int defMatId)
{
  if(defMatId < NSensorDefaultMaterial)
  {
    // Set Material IDs to default materials.
    // These are used for vehicles, units and objects that do not have StageMAT material texture defined, ie. any content that is not prepared for Sensor API
    // All default materials classified by VBS are in enum sensorDefaultMaterial.
    // For each of those we get a call here with defMatId as value of sensorDefaultMaterial.
    // We return the wanted Material ID for that default material.

    if(defMatId == MatVehicle)
    {
      // Set material for vehicles that do not have StageMAT as MetalSteel
      // MetalSteel is defined in MaterialMAp, as Material ID 7
      return _matMap.getMaterial("MetalSteel");
    }
    else if(defMatId == MatPerson)
    {
      // Set material for persons that do not have StageMAT as AliveGeneric
      return _matMap.getMaterial("AliveGeneric");
    }
    else if(defMatId == MatTree || defMatId == MatBush)
    {
      // For trees and bushes as VegetationTreeCanopy
      return _matMap.getMaterial("VegetationTreeCanopy");
    }
    else if(defMatId == MatBuilding || defMatId == MatObject)
    {
      // Set buildings and objects as material WoodUntreated
      // In the HLSL shader for this sample we check for this material and make buildings & objects (that do not have StageMAT) transparent.
      return _matMap.getMaterial("WoodUntreated");
    }
    else if(defMatId == MatSky)
    {
      // Skybox
      return _matMap.getMaterial("Sky");
    }
    else if(defMatId == MatWater)
    {
      // Water in sea and rivers
      return _matMap.getMaterial("Water");
    }
  }

  // Unknown materials to us
  // This could happen if VBS engine has added new default material classifications (sensorDefaultMaterial has grown)
  return 0;
}

unsigned char sensorSample::getTerrainMaterial(int terMatId)
{
  if(terMatId < NSensorTerrainMaterial)
  {
    // Set Material IDs for terrain types.
    // VBS classifies materials from terrain mask automatically as one of those listed in enum sensorTerrainMaterial.
    // Here we set Material IDs for those terrain types.
    // For each terrain material we get a call here with terMatId as value of sensorTerrainMaterial.
    // We return the wanted Material ID for that terrain material.

    if(terMatId == TerrainDefault)
    {
      return _matMap.getMaterial("SoilGravel");
    }
    else if(terMatId == TerrainDirt)
    {
      return _matMap.getMaterial("SoilGravel");
    }
    else if(terMatId == TerrainForest || terMatId == TerrainDryGrass || terMatId == TerrainGrass)
    {
      return _matMap.getMaterial("SoilGrass");
    }
    else if(terMatId == TerrainGravel)
    {
      return _matMap.getMaterial("SoilGravel");
    }
    else if(terMatId == TerrainMud)
    {
      return _matMap.getMaterial("SoilGravel");
    }
    else if(terMatId == TerrainRoad)
    {
      return _matMap.getMaterial("SoilSand");
    }
    else if(terMatId == TerrainRock)
    {
      return _matMap.getMaterial("SoilRock");
    }
    else if(terMatId == TerrainSand)
    {
      return _matMap.getMaterial("SoilSand");
    }
    else if(terMatId == TerrainWater)
    {
      // This is not actual water like sea but terrain that has been classified as water-like (likely not used often).
      // See MatWater in getDefaultMaterial for actual water material ID.
      return _matMap.getMaterial("Water");
    }
    else if(terMatId == TerrainHall || terMatId == TerrainMetal || terMatId == TerrainWood)
    {
      // These materials are rarely found in terrain
      return _matMap.getMaterial("SoilGravel");
    }
    else if(terMatId == TerrainSnow)
    {
      return _matMap.getMaterial("Snow");
    }
  }

  // Unknown terrain material to us
  // This could happen if VBS engine has added new terrain material classifications (sensorTerrainMaterial has grown)
  return 0; 
}

void sensorSample::getMaterialPriority(char materialPriority[256])
{
  // Priorized material IDs are set here. Only they are drawn into the last peel.
  // This helps with not having unimportant transparent materials mask important materials completely by using up all the peels.
  // Normally you would always set terrain materials (set in getTerrainMaterial) as priorized here.

  // The materialPriority array parameter indices refer to Material IDs.
  // Setting value of materialPriority entry to zero means the material with that Material ID is unpriorized, one means priorized.
  // So materialPriority[100] refers to Material ID 100

  // First, set all materials as non-priorized, they default to priorized
  for(int i=0;i<256;i++)
  {
    materialPriority[i] = 0;
  }

  // Priorize some of our materials by name.
  // Get the Material ID for material by name from materialMap (_matMap).
  // Set the corresponding entry in materialPriority as 1 to make it priorized.

  // Set priorization for all materials we returned in getTerrainMaterial as terrain materials.
  materialPriority[_matMap.getMaterial("SoilGravel")] = 1;
  materialPriority[_matMap.getMaterial("SoilGrass")] = 1;
  materialPriority[_matMap.getMaterial("SoilSand")] = 1;
  materialPriority[_matMap.getMaterial("SoilRock")] = 1;
  materialPriority[_matMap.getMaterial("Water")] = 1;
  materialPriority[_matMap.getMaterial("Snow")] = 1;

  // Priorize also some specific construction materials from materialMap
  materialPriority[_matMap.getMaterial("ConstructionAsphalt")] = 1;
  materialPriority[_matMap.getMaterial("ConstructionGraniteMarble")] = 1;

  // Priorize all vehicle materials:
  // Iterate those materials listed in materialMap::_vehicleMaterials and set them as priorized.
  materialMap::MatMapNameID v = _matMap.getVehicleMaterials();
  for(materialMap::MatMapNameID::iterator i = v.begin(); i != v.end(); ++i)
  {
    // i->second is MatID of MatMapNameID, which is the Material ID of this material
    materialPriority[i->second] = 1;
  }
}

// Sensor postprocess pass - data has been copied for us here by sensorInterfaceBase.
// We render direct to VBS backbuffer.
void sensorSample::doPostprocess(VBS2CBInterface *cbi, IDirect3DSurface9 *renderTarget, IDirect3DTexture9 *shadowmap, pluginLightDir *mainLight, const FrustumSpecNG &frustum)
{
  if(!ready()) return; // Init has failed?

  // Retrieve daytime from engine
  char res[256];
  ExecuteCommand("daytime", res, 256);
  float daytime;
  sscanf(res, "%f", &daytime);

  // This prepares the device for drawing (set default state, vertex shader, etc)
  prepareDraw(cbi, mainLight, frustum);

  // Draw data from each peel, going backwards (furthest peel first)
  for(int peel=numPeels()-1;peel>=0;peel--)
  {
    // Prepares peel rendering, sets peel data to texture smaplers
    preparePeel(cbi, peel);

    // Disable SRGB write. This will make writes to be linear, which is a bit easier for this type of sensor
    // For correct lighting SRGB must be used, but as in this sensor we use no visible light and thus no lighting it is not necessary
    cbi->SetRenderState(D3DRS_SRGBWRITEENABLE, 0);

    // Set our sensor PS
    cbi->SetPixelShader(_ps);

    // Set daytime to shader constant 0
    float psc0[4] = {daytime, 0, 0, 0};
    cbi->SetPixelShaderConstantF(0, psc0, 1);

    // Draw helper - it draws a quad that covers whole renderTarget
    draw(cbi, renderTarget);
  }
}