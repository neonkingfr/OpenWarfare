#ifndef __DRAWSAMPLEPEELS_H__
#define __DRAWSAMPLEPEELS_H__

#include "sensorInterfaceBase.h"
#include "materialMap.h"

extern ExecuteCommandType ExecuteCommand;

class sensorSample : public sensorInterfaceBase
{
public:
  sensorSample(const char *param, int numDefaultMaterials);
  ~sensorSample(){};

  void createResources(IDirect3DDevice9 *d3d);
  void manageResources(IDirect3DDevice9 *dev) {};
  void freeResources();

  virtual void getMaterialPriority(char materialPriority[256]);
  virtual unsigned char getDefaultMaterial(int defMatId);
  virtual unsigned char getTerrainMaterial(int terMatId);

  void doPostprocess(VBS2CBInterface *cbi, IDirect3DSurface9 *renderTarget, IDirect3DTexture9 *shadowmap, pluginLightDir *mainLight, const FrustumSpecNG &frustum);
  void updateSensorParameters();

private:

  IDirect3DPixelShader9 *_ps; // Sensor pixel shader
  IDirect3DTexture9 *_cgrad; // Color gradient texture
  IDirect3DTexture9 *_noise; // Noise texture

  materialMap _matMap;
};

#endif