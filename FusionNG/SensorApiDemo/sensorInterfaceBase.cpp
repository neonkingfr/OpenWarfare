
#include "sensorInterfaceBase.h"

extern ExecuteCommandType ExecuteCommand;

#define FOURCC_INTZ ((D3DFORMAT)MAKEFOURCC('I','N','T','Z'))

#pragma comment( lib, "dxguid" )

struct UVVERTEX {
    float x, y, z;
    float u, v;
};

// Simple input->output vertex shader
const static char *simpleVS = "\
    struct VS_INPUT {\
        float4 Position   : POSITION;\
	    float2 Tex0       : TEXCOORD0;\
    };\
    struct VS_OUTPUT {\
        float4 Position   : POSITION;\
	    float2 Tex0       : TEXCOORD0;\
    };\
    VS_OUTPUT vsui(in VS_INPUT In) {\
	    VS_OUTPUT Out;\
	    Out.Position = float4(In.Position.xyz, 1.0f);\
	    Out.Tex0 = In.Tex0;\
	    return Out;\
    }";

// PS helper for sensor shader (use SENSOR_SHADER macro)
const static char *sensor_shader = "\
	struct VS_OUTPUT\
	{\
		float4 pos      : POSITION;\
		float2 uv       : TEXCOORD0;\
	};\
	sampler2D sMrt0            : register(s11);\
	sampler2D sMrt1            : register(s12);\
	sampler2D sMrt2            : register(s13);\
	sampler2D sMrt3            : register(s14);\
	sampler2D sDepth           : register(s15);\
	float4 directLightColor   : register(c100);\
	float4 specularLightColor : register(c101);\
  float4 lightDirSunMoon    : register(c102);\
  float4 clipPlane          : register(c103);\
	struct sensorPeelData {\
		float4 rgba;    /* 'Out the window' RGB color + alpha information*/\
		int matID;      /* Material index (0-255)*/\
		float tiHT;     \
		float tiHeat;   \
		float sunLastH; /*How much of the last hour this pixel has been on the sun (0.0-1.0)*/\
		float nDotL;    /*Dot product of surface normal & main light ('how much is this pixel surface towards the sun', 0.0 - 1.0)*/\
		float3 lightIndirect; /*Indirect light color*/\
		float3 lightDirect;   /*Direct light color*/\
		float3 lightSpecular; /* Specular light color*/\
	};\
  float3 camPos     : register(c104);\
  float3 camDir     : register(c105);\
  float3 camUp      : register(c106);\
	sensorPeelData getSensorPeelData(float2 uv) {\
		sensorPeelData data;\
		float4 a = tex2D(sMrt0, uv); /*RGBA color*/\
		float4 b = tex2D(sMrt1, uv); /*tiHalfTime, direct light luminance, specular luminance, 0*/\
		float4 c = tex2D(sMrt2, uv); /*indirect light RGB, sunLastH*/\
		float4 d = tex2D(sMrt3, uv); /*material ID, TI, nDotL, 0*/\
		data.rgba = a;\
    data.matID = floor(d.r*255);\
		data.tiHeat = d.g;\
		data.tiHT = b.r;\
		data.sunLastH = c.a*lightDirSunMoon.w;\
		data.nDotL = d.b;\
		data.lightIndirect = c.rgb;\
		data.lightDirect = b.g * normalize(directLightColor);\
		data.lightSpecular = b.b * normalize(specularLightColor);\
		return data;\
	}\
    ";

sensorInterfaceBase::sensorInterfaceBase(const char *param, int numDefaultMaterials)
: _width(0), _height(0), _peels(0), _vertexDecl(NULL), _vs(NULL)
{
  updateSensorParameters();
}

void sensorInterfaceBase::updateSensorParameters()
{
  _width = _height = 512;
  _peels = 1;
}

// Base initialization: Create shaders, vertex declarations, etc.
void sensorInterfaceBase::initialize(IDirect3DDevice9 *d3d)
{ 
  _vs = NULL;
  _vertexDecl = NULL;

  _ready = false;

  LogF("Initializing sensorInterfaceBase...");

  // Compile a simple base vertex shader, since we do postprocess style drawing this is enough for most sensor api uses
  LPD3DXBUFFER codevs;
  LPD3DXBUFFER err = NULL;

  if(D3DXCompileShader(simpleVS, (UINT)strlen(simpleVS), NULL, NULL, "vsui", "vs_3_0", NULL, &codevs, &err, NULL) != D3D_OK)  
  {
    if(err) LogF("Error compiling vertex shader: %s", (char*)err->GetBufferPointer());
    else    LogF("Error compiling vertex shader (file not found?)");
    return;
  }

  d3d->CreateVertexShader((DWORD*)codevs->GetBufferPointer(), &_vs);
  codevs->Release();

  // Create vertex declaration
  // Similar to the FVF in fixed function pipeline, this sets the structure of our vertex data,
  // it must match the vertex data we use (SIMPLEVERTEX) and the vertex shader input (VS_INPUT in hlsl)
  D3DVERTEXELEMENT9 decl[] = {{0,
                               0,
                               D3DDECLTYPE_FLOAT3,
                               D3DDECLMETHOD_DEFAULT,
                               D3DDECLUSAGE_POSITION,
                               0},
                              {0,
                               12,
                               D3DDECLTYPE_FLOAT2,
                               D3DDECLMETHOD_DEFAULT,
                               D3DDECLUSAGE_TEXCOORD,
                               0},
                              D3DDECL_END()};
  if(d3d->CreateVertexDeclaration(decl, &_vertexDecl) != D3D_OK)
  {
    LogF("CreateVertexDeclaration failed!");
    return;
  }

  _ready = true;
}

// Helper for compiling a pixel shader
IDirect3DPixelShader9* sensorInterfaceBase::compilePixelShader(IDirect3DDevice9 *dev, const char *hlsl, const char *main, const std::vector<D3DXMACRO> macros)
{
  IDirect3DPixelShader9* ps = NULL;
  LPD3DXBUFFER codeps;
  LPD3DXBUFFER err = NULL;

  const int numMacros = (const int )macros.size()+2;

  // Build D3DXMACRO array for D3DXCompileShaderFromFile
  D3DXMACRO* ma = new D3DXMACRO[numMacros];
  for(int i=0;i<(int)macros.size();i++)
  {
    ma[i].Definition = macros[i].Definition;
    ma[i].Name = macros[i].Name;
  }

  // Append our shader macro
  ma[numMacros-2].Name  = "SENSOR_SHADER";
  ma[numMacros-2].Definition = sensor_shader;

  // Null terminator
  ma[numMacros-1].Name = NULL;
  ma[numMacros-1].Definition = NULL;


  if(D3DXCompileShaderFromFile(hlsl, ma, NULL, main, "ps_3_0", NULL, &codeps, &err, NULL) != D3D_OK)
  {
    if(err) LogF("Error compiling pixel shader: %s", (char*)err->GetBufferPointer());
    else    LogF("Error compiling pixel shader (file <%s> not found?)", hlsl);
#if 0 // Dump preprocessed source to txt file for debugging preprocessor related issues
    ID3DXBuffer *shaderCode = NULL;
    D3DXPreprocessShaderFromFile(hlsl, macros, NULL, &shaderCode, NULL);
    if(shaderCode)
    {
      FILE *f = fopen("shader_dump.txt", "wb");
      fwrite(shaderCode->GetBufferPointer(), shaderCode->GetBufferSize(), 1, f);
      fclose(f);
    }
#endif
    return ps;
  }
  
  dev->CreatePixelShader((DWORD*)codeps->GetBufferPointer(), &ps);
  codeps->Release();
  return ps;
}

sensorInterfaceBase::~sensorInterfaceBase()
{
  LogF("Deinitializing sensorInterfaceBase...");
  _ready = false;

  SAFE_RELEASE(_vertexDecl);
  SAFE_RELEASE(_vs);

  _peelData.resize(0);
}

void sensorInterfaceBase::prepareBuffers(IDirect3DDevice9 *dev, int width, int height, int peels, D3DFORMAT *fmtMrt, D3DFORMAT fmtDepth)
{
  if(_width != width || _height != height || _peels != peels)
  {
    // Buffer data from OnSensorInit does not match what we requested
    // This can happen when multiple sensor plugins are trying to handle the same sensor
    LogF("Error: sensorInterfaceBase::prepareBuffers sensor parameters do not match %dx%d %dx%d %d-%d", width, height, _width, _height, peels, _peels);
    return;
  }

  _peelData.resize(peels);

  for(int p=0;p<peels;p++)
  {
    // Create storage for MRT buffers, data is copied to these in doCopyBuffers.
    // In future we need to create separate buffers for each peel.
    // fmtMrt contains surface formats chosen by engine. 
    // Currently buffers use at least 16bit per channel (64bit per pixel), apart from the last buffer which is A8R8G8B8 format.
    // Create textures so we can sample from them in sensor shader.
    for(int i=0;i<NUM_MRT;i++)
    {
      if(dev->CreateTexture(width, height, 1, D3DUSAGE_RENDERTARGET, fmtMrt[i], D3DPOOL_DEFAULT, &_peelData[p]._mrt[i], NULL) != D3D_OK)
      {
        LogF("Error creating texture %d", i);
        return;
      }
    }

    // Create storage for depth buffer. The buffer uses special INTZ depth format which can be bound as texture.
    // It is a GPU manufacturer extension so some old hardware might not support it.
    // In such case fmtDepth is NULL and no depth is available
    if(fmtDepth != NULL)
    {
      if(dev->CreateTexture(width, height, 1, D3DUSAGE_DEPTHSTENCIL, FOURCC_INTZ, D3DPOOL_DEFAULT, &_peelData[p]._depth, NULL) != D3D_OK)
      {
        LogF("Error creating depth texture");
        return;
      }
    }
    else
    {
      _peelData[p]._depth = NULL;
      LogF("Warning: No depth info");
    }
  }
}

void sensorInterfaceBase::doCopyBuffers(VBS2CBInterface *cbi, int peelIndex, IDirect3DSurface9 **peelSurfaces, IDirect3DSurface9 *depth)
{
  if(peelIndex >= _peelData.size())
  {
    // Using debug commands engine can be made to render more peels than requested by us
    return;
  }

  // Copy MRT data from engine rendered temporary buffers to our permanents storage to be used in doPostprocess
  for(int i=0;i<NUM_MRT;i++)
  {
    IDirect3DSurface9 *src = peelSurfaces[i];
    IDirect3DSurface9 *dst = NULL;
    _peelData[peelIndex]._mrt[i]->GetSurfaceLevel(0, &dst);

    if(src && dst) cbi->StretchRect(src, NULL, dst, NULL, D3DTEXF_POINT);
    else LogF("Error: doCopyBuffers: no surface");

    if(dst) dst->Release();
  }

  // Copy depth as well
  if(_peelData[peelIndex]._depth && depth)
  {
    IDirect3DSurface9 *dst = NULL;
    _peelData[peelIndex]._depth->GetSurfaceLevel(0, &dst);

    if(depth && dst) cbi->StretchRect(depth, NULL, dst, NULL, D3DTEXF_POINT);
    else LogF("Error: doCopyBuffers: no depth surface");

    if(dst) dst->Release();   
  }
}

void sensorInterfaceBase::getMaterialPriority(char materialPriority[256])
{
  // If actual sample doesn't want material priorities, set all to priority
  for(int i=0;i<256;i++)
  {
    materialPriority[i] = 1;
  }
}

unsigned char sensorInterfaceBase::getDefaultMaterial(int defMatId)
{
  if(defMatId < NSensorDefaultMaterial)
    return defMatId;
  else
    return 0; // Unknown materials to us
}

unsigned char sensorInterfaceBase::getTerrainMaterial(int terMatId)
{
  if(terMatId < NSensorTerrainMaterial)
    return 100+terMatId;
  else
    return 0; // Unknown materials to us
}

// Helper for doing drawing preparation
void sensorInterfaceBase::prepareDraw(VBS2CBInterface *cbi, pluginLightDir *mainLight, const FrustumSpecNG &frustum)
{
  // Set device to default state
  setDefaultState(cbi);

  // Set simple vertex shader & vertex declaration
  cbi->SetVertexShader(_vs);
  cbi->SetVertexDeclaration(_vertexDecl);

  cbi->SetRenderState(D3DRS_CULLMODE, D3DCULL_NONE);
  cbi->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
  cbi->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_SRCALPHA);
  cbi->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA);
  cbi->SetRenderState(D3DRS_BLENDOP, D3DBLENDOP_ADD);

  cbi->SetRenderState(D3DRS_ZENABLE, D3DZB_FALSE);
  cbi->SetRenderState(D3DRS_ZWRITEENABLE, FALSE);

  // Pass light color in PS constants 100 & 101
  float directLightColor[4] = {
      mainLight->ambient.x,
      mainLight->ambient.y,
      mainLight->ambient.z,
      1.0f};

  float specularLightColor[4] = {
      mainLight->diffuse.x,
      mainLight->diffuse.y,
      mainLight->diffuse.z,
      1.0f};

  cbi->SetPixelShaderConstantF(100, directLightColor, 1);
  cbi->SetPixelShaderConstantF(101, specularLightColor, 1);

  // Light direction & sun-moon value
  float lightDirectionSunMoon[4] = {
      mainLight->direction.x,
      mainLight->direction.y,
      mainLight->direction.z,
      mainLight->sunMoon};
  cbi->SetPixelShaderConstantF(102, lightDirectionSunMoon, 1);

  float clipPlanes[4] = {
      frustum.clipDistNear,
      frustum.clipDistFar,
      0,
      0};
  cbi->SetPixelShaderConstantF(103, clipPlanes, 1);

  // Set camera position, direction, up
  float camPos[4] = {frustum.pointPosX, frustum.pointPosY, frustum.pointPosZ, 1};
  float camDir[4] = {frustum.viewDirX, frustum.viewDirY, frustum.viewDirZ, 0};
  float camUp[4] = {frustum.viewUpX, frustum.viewUpY, frustum.viewUpZ, 0};
  cbi->SetPixelShaderConstantF(104, camPos, 1);
  cbi->SetPixelShaderConstantF(105, camDir, 1);
  cbi->SetPixelShaderConstantF(106, camUp, 1);
}
// Helper for doing drawing preparation
void sensorInterfaceBase::preparePeel(VBS2CBInterface *cbi, int peelIndex)
{
  if(peelIndex >= numPeels()) return;

  // Set MRT data buffers to texture stages (11-15)
  cbi->SetTexture(STAGE_MRT0, _peelData[peelIndex]._mrt[0]);
  cbi->SetTexture(STAGE_MRT1, _peelData[peelIndex]._mrt[1]);
  cbi->SetTexture(STAGE_MRT2, _peelData[peelIndex]._mrt[2]);
  cbi->SetTexture(STAGE_MRT3, _peelData[peelIndex]._mrt[3]);
  cbi->SetTexture(STAGE_DEPTH, _peelData[peelIndex]._depth);
  // cbi->SetTexture(4, shadowmap);

  cbi->SetRenderState(D3DRS_SRGBWRITEENABLE, 1);
  cbi->SetSamplerState(STAGE_MRT0, D3DSAMP_SRGBTEXTURE, 0);
  cbi->SetSamplerState(STAGE_MRT0, D3DSAMP_MINFILTER, D3DTEXF_LINEAR);
  cbi->SetSamplerState(STAGE_MRT0, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR);
  cbi->SetSamplerState(STAGE_MRT0, D3DSAMP_MIPFILTER, D3DTEXF_NONE);

  cbi->SetSamplerState(STAGE_MRT1, D3DSAMP_SRGBTEXTURE, 0);
  cbi->SetSamplerState(STAGE_MRT1, D3DSAMP_MINFILTER, D3DTEXF_LINEAR);
  cbi->SetSamplerState(STAGE_MRT1, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR);
  cbi->SetSamplerState(STAGE_MRT1, D3DSAMP_MIPFILTER, D3DTEXF_NONE);

  cbi->SetSamplerState(STAGE_MRT2, D3DSAMP_SRGBTEXTURE, 0);
  cbi->SetSamplerState(STAGE_MRT2, D3DSAMP_MINFILTER, D3DTEXF_LINEAR);
  cbi->SetSamplerState(STAGE_MRT2, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR);
  cbi->SetSamplerState(STAGE_MRT2, D3DSAMP_MIPFILTER, D3DTEXF_NONE);

  cbi->SetSamplerState(STAGE_MRT3, D3DSAMP_SRGBTEXTURE, 0);  
  cbi->SetSamplerState(STAGE_MRT3, D3DSAMP_MINFILTER, D3DTEXF_POINT);
  cbi->SetSamplerState(STAGE_MRT3, D3DSAMP_MAGFILTER, D3DTEXF_POINT);
  cbi->SetSamplerState(STAGE_MRT3, D3DSAMP_MIPFILTER, D3DTEXF_NONE);

}

// Helper for simple drawing
void sensorInterfaceBase::draw(VBS2CBInterface *cbi, IDirect3DSurface9 *renderTarget)
{
  const float x = -1.0f;
  const float y = -1.0f;
  const float w = 2.0f;
  const float h = 2.0f;

  // Get half-texel offset
  D3DSURFACE_DESC desc;
  renderTarget->GetDesc(&desc);
  float htx = 0.5f/(float)desc.Width;
  float hty = 0.5f/(float)desc.Height;

  UVVERTEX vdata[] = {
  // X     Y     Z         U         V
    {x,    y, 0.1f, 0.0f+htx, 1.0f+hty},
    {x+w,  y, 0.1f, 1.0f+htx, 1.0f+hty},
    {x,  y+h, 0.1f, 0.0f+htx, 0.0f+hty},
    {x+w,y+h, 0.1f, 1.0f+htx, 0.0f+hty},
  };

  cbi->DrawPrimitiveUP(D3DPT_TRIANGLESTRIP, 2, vdata, sizeof(UVVERTEX));

  // Un-set textures for all render stages
  for(int i=0;i<16;i++) cbi->SetTexture(i, NULL);
}

void sensorInterfaceBase::setDefaultState(VBS2CBInterface *d3dd)
{
  // Set D3D device properties to their default state
  // This is to ensure things are in a predictable state when coming from engine rendering code
  const float zerof = 0.0f;
  const float onef = 1.0f;
  #define ZEROf	*((DWORD*) (&zerof))
  #define ONEf	*((DWORD*) (&onef))

  d3dd->SetRenderState(D3DRS_ZENABLE, D3DZB_FALSE);
  d3dd->SetRenderState(D3DRS_FILLMODE, D3DFILL_SOLID);
  d3dd->SetRenderState(D3DRS_SHADEMODE, D3DSHADE_GOURAUD);
  d3dd->SetRenderState(D3DRS_ZWRITEENABLE, TRUE);
  d3dd->SetRenderState(D3DRS_ALPHATESTENABLE, FALSE);
  d3dd->SetRenderState(D3DRS_LASTPIXEL, TRUE);
  d3dd->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_ONE);
  d3dd->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_ZERO);
  d3dd->SetRenderState(D3DRS_CULLMODE, D3DCULL_CCW);
  d3dd->SetRenderState(D3DRS_ZFUNC, D3DCMP_LESSEQUAL);
  d3dd->SetRenderState(D3DRS_ALPHAREF, 0);
  d3dd->SetRenderState(D3DRS_ALPHAFUNC, D3DCMP_ALWAYS);
  d3dd->SetRenderState(D3DRS_ALPHABLENDENABLE, FALSE);
  d3dd->SetRenderState(D3DRS_FOGENABLE, FALSE);
  d3dd->SetRenderState(D3DRS_SPECULARENABLE, FALSE);
  d3dd->SetRenderState(D3DRS_FOGCOLOR, 0);
  d3dd->SetRenderState(D3DRS_FOGTABLEMODE, D3DFOG_NONE);
  d3dd->SetRenderState(D3DRS_FOGSTART, ZEROf);
  d3dd->SetRenderState(D3DRS_FOGEND, ONEf);
  d3dd->SetRenderState(D3DRS_FOGDENSITY, ONEf);
  d3dd->SetRenderState(D3DRS_RANGEFOGENABLE, FALSE);
  d3dd->SetRenderState(D3DRS_STENCILENABLE, FALSE);
  d3dd->SetRenderState(D3DRS_STENCILFAIL, D3DSTENCILOP_KEEP);
  d3dd->SetRenderState(D3DRS_STENCILZFAIL, D3DSTENCILOP_KEEP);
  d3dd->SetRenderState(D3DRS_STENCILPASS, D3DSTENCILOP_KEEP);
  d3dd->SetRenderState(D3DRS_STENCILFUNC, D3DCMP_ALWAYS);
  d3dd->SetRenderState(D3DRS_STENCILREF, 0);
  d3dd->SetRenderState(D3DRS_STENCILMASK, 0xffffffff);
  d3dd->SetRenderState(D3DRS_STENCILWRITEMASK, 0xffffffff);
  d3dd->SetRenderState(D3DRS_TEXTUREFACTOR, 0xffffffff);
  d3dd->SetRenderState(D3DRS_WRAP0, 0);
  d3dd->SetRenderState(D3DRS_WRAP1, 0);
  d3dd->SetRenderState(D3DRS_WRAP2, 0);
  d3dd->SetRenderState(D3DRS_WRAP3, 0);
  d3dd->SetRenderState(D3DRS_WRAP4, 0);
  d3dd->SetRenderState(D3DRS_WRAP5, 0);
  d3dd->SetRenderState(D3DRS_WRAP6, 0);
  d3dd->SetRenderState(D3DRS_WRAP7, 0);
  d3dd->SetRenderState(D3DRS_CLIPPING, TRUE);
  d3dd->SetRenderState(D3DRS_AMBIENT, 0);
  d3dd->SetRenderState(D3DRS_LIGHTING, TRUE);
  d3dd->SetRenderState(D3DRS_FOGVERTEXMODE, D3DFOG_NONE);
  d3dd->SetRenderState(D3DRS_COLORVERTEX, TRUE);
  d3dd->SetRenderState(D3DRS_LOCALVIEWER, TRUE);
  d3dd->SetRenderState(D3DRS_NORMALIZENORMALS, FALSE);
  d3dd->SetRenderState(D3DRS_DIFFUSEMATERIALSOURCE, D3DMCS_COLOR1);
  d3dd->SetRenderState(D3DRS_SPECULARMATERIALSOURCE, D3DMCS_COLOR2);
  d3dd->SetRenderState(D3DRS_AMBIENTMATERIALSOURCE, D3DMCS_MATERIAL);
  d3dd->SetRenderState(D3DRS_EMISSIVEMATERIALSOURCE, D3DMCS_MATERIAL);
  d3dd->SetRenderState(D3DRS_CLIPPLANEENABLE, 0);
  d3dd->SetRenderState(D3DRS_POINTSIZE_MIN, ONEf);
  d3dd->SetRenderState(D3DRS_POINTSPRITEENABLE, FALSE);
  d3dd->SetRenderState(D3DRS_MULTISAMPLEANTIALIAS, TRUE);
  d3dd->SetRenderState(D3DRS_MULTISAMPLEMASK, 0xffffffff);
  d3dd->SetRenderState(D3DRS_PATCHEDGESTYLE, D3DPATCHEDGE_DISCRETE);
  d3dd->SetRenderState(D3DRS_POINTSIZE_MAX, ONEf);
  d3dd->SetRenderState(D3DRS_COLORWRITEENABLE, 0x0000000f);
  d3dd->SetRenderState(D3DRS_BLENDOP, D3DBLENDOP_ADD);
  d3dd->SetRenderState(D3DRS_POSITIONDEGREE, D3DDEGREE_CUBIC);
  d3dd->SetRenderState(D3DRS_NORMALDEGREE, D3DDEGREE_LINEAR);
  d3dd->SetRenderState(D3DRS_SCISSORTESTENABLE, FALSE);
  d3dd->SetRenderState(D3DRS_SLOPESCALEDEPTHBIAS, 0);
  d3dd->SetRenderState(D3DRS_MINTESSELLATIONLEVEL, ONEf);
  d3dd->SetRenderState(D3DRS_MAXTESSELLATIONLEVEL, ONEf);
  d3dd->SetRenderState(D3DRS_ADAPTIVETESS_X, ZEROf);
  d3dd->SetRenderState(D3DRS_ADAPTIVETESS_Y, ZEROf);
  d3dd->SetRenderState(D3DRS_ADAPTIVETESS_Z, ONEf);
  d3dd->SetRenderState(D3DRS_ADAPTIVETESS_W, ZEROf);
  d3dd->SetRenderState(D3DRS_ENABLEADAPTIVETESSELLATION, FALSE);
  d3dd->SetRenderState(D3DRS_TWOSIDEDSTENCILMODE, FALSE);
  d3dd->SetRenderState(D3DRS_CCW_STENCILFAIL, D3DSTENCILOP_KEEP);
  d3dd->SetRenderState(D3DRS_CCW_STENCILZFAIL, D3DSTENCILOP_KEEP);
  d3dd->SetRenderState(D3DRS_CCW_STENCILPASS, D3DSTENCILOP_KEEP);
  d3dd->SetRenderState(D3DRS_CCW_STENCILFUNC, D3DCMP_ALWAYS);
  d3dd->SetRenderState(D3DRS_COLORWRITEENABLE1, 0x0000000f);
  d3dd->SetRenderState(D3DRS_COLORWRITEENABLE2, 0x0000000f);
  d3dd->SetRenderState(D3DRS_COLORWRITEENABLE3, 0x0000000f);
  d3dd->SetRenderState(D3DRS_BLENDFACTOR, 0xffffffff);
  d3dd->SetRenderState(D3DRS_SRGBWRITEENABLE, 0);
  d3dd->SetRenderState(D3DRS_DEPTHBIAS, 0);
  d3dd->SetRenderState(D3DRS_WRAP8, 0);
  d3dd->SetRenderState(D3DRS_WRAP9, 0);
  d3dd->SetRenderState(D3DRS_WRAP10, 0);
  d3dd->SetRenderState(D3DRS_WRAP11, 0);
  d3dd->SetRenderState(D3DRS_WRAP12, 0);
  d3dd->SetRenderState(D3DRS_WRAP13, 0);
  d3dd->SetRenderState(D3DRS_WRAP14, 0);
  d3dd->SetRenderState(D3DRS_WRAP15, 0);
  d3dd->SetRenderState(D3DRS_SEPARATEALPHABLENDENABLE, FALSE);
  d3dd->SetRenderState(D3DRS_SRCBLENDALPHA, D3DBLEND_ONE);
  d3dd->SetRenderState(D3DRS_DESTBLENDALPHA, D3DBLEND_ZERO);
  d3dd->SetRenderState(D3DRS_BLENDOPALPHA, D3DBLENDOP_ADD);
  d3dd->SetRenderState(D3DRS_DITHERENABLE, FALSE);
  d3dd->SetRenderState(D3DRS_VERTEXBLEND, D3DVBF_DISABLE);
  d3dd->SetRenderState(D3DRS_POINTSIZE, ONEf);
  d3dd->SetRenderState(D3DRS_POINTSCALEENABLE, FALSE);
  d3dd->SetRenderState(D3DRS_POINTSCALE_A, ONEf);
  d3dd->SetRenderState(D3DRS_POINTSCALE_B, ZEROf);
  d3dd->SetRenderState(D3DRS_POINTSCALE_C, ZEROf);
  d3dd->SetRenderState(D3DRS_INDEXEDVERTEXBLENDENABLE, FALSE);
  d3dd->SetRenderState(D3DRS_TWEENFACTOR, ZEROf);
  d3dd->SetRenderState(D3DRS_ANTIALIASEDLINEENABLE, FALSE);

  for(int i=0;i<16;i++) {
	  d3dd->SetSamplerState(i, D3DSAMP_MINFILTER, D3DTEXF_LINEAR);
	  d3dd->SetSamplerState(i, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR);
	  d3dd->SetSamplerState(i, D3DSAMP_MIPFILTER, D3DTEXF_NONE);
	  d3dd->SetSamplerState(i, D3DSAMP_MIPMAPLODBIAS, 0);
	  d3dd->SetSamplerState(i, D3DSAMP_MAXMIPLEVEL, 0);			
	  d3dd->SetSamplerState(i, D3DSAMP_MAXANISOTROPY, 1);
	  d3dd->SetSamplerState(i, D3DSAMP_SRGBTEXTURE, 0);			
	  d3dd->SetSamplerState(i, D3DSAMP_ELEMENTINDEX, 0);
	  d3dd->SetSamplerState(i, D3DSAMP_DMAPOFFSET, 0);			
	  d3dd->SetSamplerState(i, D3DSAMP_ADDRESSU, D3DTADDRESS_WRAP);
	  d3dd->SetSamplerState(i, D3DSAMP_ADDRESSV, D3DTADDRESS_WRAP);
	  d3dd->SetSamplerState(i, D3DSAMP_ADDRESSW, D3DTADDRESS_WRAP);

    d3dd->SetTexture(i, NULL);
  }

  #undef ZEROf
  #undef ONEf
}