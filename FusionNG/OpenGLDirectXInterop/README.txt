This plugin demonstrates the NVidia interop between openGL and DirectX.

Plugin was designed to work with VBS2 2.12 32bit

For it to work the following needs to be done. 

1. Copy glew32.dll from deps/bin to the root VBS2 directory (since this binary is needed so that the OpenGL extension function procs can be queried)

2. Copy the OpenGLShader.hlsl file to the root VBS2 directory

3. Copy the compiled OpenGLDirectXInterop.dll to the VBS2/pluginsFusion directory

Tested and working on VBS 2.12 32bit on Windows7 with NVidia GT570 graphics card. NOT yet tested on AMD graphics hardware.