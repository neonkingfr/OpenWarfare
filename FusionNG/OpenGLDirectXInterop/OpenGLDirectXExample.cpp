#include "OpenGLDraw.h"
#include "OpenGLDirectXExample.h"
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// VBS2Fusion interface library forced linking
// Needed for plugins that don't use any of VBS2Fusion symbols
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
#include "VBS2FusionAppContext.h"
#include "util/ErrorHandleUtilities.h"
VBS2Fusion::VBS2FusionAppContext appContext;
#pragma comment(lib, "../_depends/Fusion/lib/VBS2Fusion_2005.lib")
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
OpenGLDraw *sample = NULL;
HWND		vbsWindow = NULL;
HINSTANCE	vbsInstance = NULL;

VBSPLUGIN_EXPORT void WINAPI RegisterHWND(void *hwnd, void *hins)
{
	vbsWindow = (HWND)hwnd;
	vbsInstance = (HINSTANCE)hins;
}

// This function is called once per frame before any drawing is done by engine
// parameters contain real direct 3D device, so resource management can be done here
VBSPLUGIN_EXPORT void WINAPI OnNgPreDraw(paramPreDraw *param, DWORD paramSize)
{
	if(paramSize<sizeof(paramPreDraw))
		return; // Engine is older than our plugin structs

}

// This function is called once per frame, after depth priming is done but before normal rendering is done
// parameters contain real direct 3D device, so resource management can be done here
// At this point some extra information is available like eye accomodation value
VBSPLUGIN_EXPORT void WINAPI OnNgInitDraw(paramInitDraw *param, DWORD paramSize)
{  
	if(sample)
	{
		// OpenGL drawing	
 		sample->BeginGLDraw();
		sample->DrawGLScene();
		sample->EndGLDraw();
	}
}

// This function is called at end of frame rendering, after post processing is done and UI is rendered
// parameters contain CB interface which must be used to do only drawing here,
// do not attempt to access the D3D device directly and do not release any resources!
VBSPLUGIN_EXPORT void WINAPI OnNgFilledBackbuffer(paramFilledBackbuffer *param, DWORD paramSize)
{
	if(sample)
	{
		RECT rect;
		GetClientRect(vbsWindow, &rect);
		sample->SetDefaultState(param->cbi);

		param->cbi->SetPixelShader(sample->GetPixelShader());
		param->cbi->SetVertexShader(sample->GetVertexShader());
		param->cbi->SetVertexDeclaration(sample->GetVertexDeclaration());

		param->cbi->SetRenderState(D3DRS_CULLMODE, D3DCULL_NONE);
		param->cbi->StretchRect(param->renderTarget, NULL, sample->GetRenderSurface(), NULL, D3DTEXF_LINEAR);
		param->cbi->SetTexture(0, sample->GetRenderTexture());
		param->cbi->SetTexture(1, sample->GetClourTexture());

		float x = -1.0f;
		float y = -1.0f;
		float w = 2.0f;
		float h = 2.0f;

		// create half texel offset
		D3DSURFACE_DESC desc;
		sample->GetRenderSurface()->GetDesc(&desc);
		float htx = 0.5f/(float)desc.Width;
		float hty = 0.5f/(float)desc.Height;

		UVVERTEX vData[] = {
		 	{x,		y, 0.1f, 0.0f +htx, 1.0f +hty},
		 	{x+w,   y, 0.1f, 1.0f +htx, 1.0f +hty},
		 	{x,	  y+h, 0.1f, 0.0f +htx, 0.0f +hty},
		 	{x+w, y+h, 0.1f, 1.0f +htx, 0.0f +hty}
		};

		param->cbi->DrawPrimitiveUP(D3DPT_TRIANGLESTRIP, 2, vData, sizeof(UVVERTEX));
	}
}

// This function is called when D3D device is created by engine
// parameters contain real direct 3D device, so resource management can be done here
VBSPLUGIN_EXPORT void WINAPI OnNgDeviceInit(paramDeviceInit *param, DWORD paramSize)
{  
	// Create the sample
	if(!sample)
	{
		sample = new OpenGLDraw();
		sample->SetHWND(vbsWindow);
		sample->SetHINST(vbsInstance);

		RECT rect;
		GetClientRect(vbsWindow, &rect);
		int width = rect.right - rect.left;
		int height = rect.bottom - rect.top;
		sample->SetWindowWidth(width);
		sample->SetWindowHeight(height);

		sample->SetDirectDevice(param->dev);
		sample->InitDX();
		sample->CreateGLContext(width, height, 24);
		sample->InitializeInterop(param->dev);
		sample->CreateResources();
		sample->ReleaseDevice();
	}
}

// This function is called when D3D device is destroyed or reset by engine
// parameters contain real direct 3D device, so resource management can be done here
// All D3D resources created by the plugin must be released here, otherwise device resets will fail
VBSPLUGIN_EXPORT void WINAPI OnNgDeviceInvalidate(paramDeviceInvalidate *param, DWORD paramSize)
{
	// Delete the sample object, this will release everything it has created
	// Alternatively, we could check for param->isReset and only release Direct3D handles without destroying anything else
	delete sample;
	sample = NULL;
}

// This function is called when D3D device is re-created after reset by engine
// parameters contain real direct 3D device, so resource management can be done here
// Resources released in OnNgDeviceInvalidate should be re-created here
VBSPLUGIN_EXPORT void WINAPI OnNgDeviceRestore(paramDeviceRestore *param, DWORD paramSize)
{
	// Re-create the sample
	if(!sample)
	{
		sample = new OpenGLDraw();
		sample->SetHWND(vbsWindow);
		sample->SetHINST(vbsInstance);

		RECT rect;
		GetClientRect(vbsWindow, &rect);
		int width = rect.right - rect.left;
		int height = rect.bottom - rect.top;
		sample->SetWindowWidth(width);
		sample->SetWindowHeight(height);

		sample->SetDirectDevice(param->dev);
		sample->InitDX();
		sample->CreateGLContext(width, height, 24);
		sample->InitializeInterop(param->dev);
		sample->CreateResources();
		sample->ReleaseDevice();
	}
}


// Previous plugin functionality, same as before:

void LogF(const char* format, ...)
{
	VBS2Fusion::ErrorHandleUtilities::setVBS2FusionLogging(true);
	va_list     argptr;
	char        str[1024];
	va_start (argptr,format);
	vsnprintf (str,1024,format,argptr);
	va_end   (argptr);
	VBS2Fusion::ErrorHandleUtilities::logMessage(str);
	VBS2Fusion::ErrorHandleUtilities::setVBS2FusionLogging(false);
}


VBSPLUGIN_EXPORT void WINAPI OnUnload() {
	// Whole plugin is unloaded, release the sample
	delete sample;
	sample = NULL;
}

VBSPLUGIN_EXPORT void WINAPI OnSimulationStep(float deltaT) {}

VBSPLUGIN_EXPORT const char* WINAPI PluginFunction(const char *inp)
{
	return "[]";
}

BOOL WINAPI DllMain(HINSTANCE hDll, DWORD fdwReason, LPVOID lpvReserved)
{
	switch(fdwReason)
	{
	case DLL_PROCESS_ATTACH:
		break;
	case DLL_PROCESS_DETACH:
		break;
	}
	return TRUE;
}