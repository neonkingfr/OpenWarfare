Mirror plugin documentation:

Introduction:
  Allows the user to specify command line arguments as well as call script functions
  that mirrors VBS2's rendered screen. 

Vocab:
  root = Where your VBS2(_64).exe resides.
  Example:
    "C:/Bohemia Interactive/VBS2/"

Needed files:
  xMirror.fusion (32bit or 64bit)
  mirror_shader.hlsl
  
  Note:
    Place the .fusion into the respective "root/pluginsFusion" folder.
    "root/pluginsFusion" for 32bit and "root/pluginsFusion64" for 64bit.
    
    Place the shader into the "root/pluginsFusion/xmirror" folder. If this
    folder does not exist, create the folder and place the shader inside.

Values:
  vertical		= Flips the screen on the X axis (Top becomes bottom and vice versa)
  horizontal		= Flips the screen on the Y axis (Left becomes right and vice versa)
  both			= Flips the screen on both axis (equivalent to a 180� rotation)
  
CommandLine:
  Format:
    -mirror=<value>
    
  Purpose:
    To flip the screen on load without having to know complicated scripting. The screen
    will remain flipped until VBS2 is restarted and the command line argument is removed.
  
  Example:
    -mirror=vertical (flips the screen on the x axis)
  
Script Calls:
  Format:
    ["",true:false]"] call fn_setMirrorDisplay;

  Purpose:
    To flip the screen during runtime to fit the current needs of the user.

  Examples:
    ["vertical",true] call fn_setMirrorDisplay; (turns on mirroring for the x axis)
    ["vertical",false] call fn_setMirrorDisplay; (turns off mirroring for the x axis)
    ["vertical"] call fn_setMirrorDisplay; (get current state of y axis mirroring [true|false] )

    ["horizontal",true] call fn_setMirrorDisplay; (turns on mirroring for the y axis)
    ["horizontal",false] call fn_setMirrorDisplay; (turns off mirroring for the y axis)
    ["horizontal"] call fn_setMirrorDisplay; (get current state of y axis mirroring [true|false] )

    ["both ",true] call fn_setMirrorDisplay; (turns on mirroring for the both axis)
    ["both ",false] call fn_setMirrorDisplay; (turns off mirroring for the both axis)
    ["both"] call fn_setMirrorDisplay; (get current state of both axis mirroring [true|false] )
