/*
Description: 
	This example is intended to demonstrate VBS2Fusion pose control utility.
	A unit is created and its neck is expanded and retracted based on mission duration.

Plugin Function Parameters:
	's' - Retrieves mission player, create pose control unit in front of player unit
		  and start the display.
	'a' - Activates pose controlling over the added unit.
	'b' - Deactivates pose controlling over the added unit.
	'k' - Deletes the pose control unit and stop the display.
*/

#include <string>

#include "pluginHeader.h"
#include "poseControlUnit.h"

#include "util/UnitUtilities.h"
#include "util/MissionUtilities.h"
#include "DisplayFunctions.h"

using namespace VBS2Fusion;

volatile bool started = false;
string displayString;

/*! Defines player unit. */
Unit playerUnit;

/*! Defines new unit of type "PoseControlUnit". */
PoseControlUnit pcUnit;

/*!
Following function is called by the VBS2 engine in each an every frame
\param deltatT is an floating point argument which returns times
since the last call.
*/
VBSPLUGIN_EXPORT void WINAPI OnSimulationStep(float deltaT)
{
	try
	{
		if (started)
		{
			/*! Set a changing value with time to make skeleton changes of the player. */
			pcUnit.setHeadMovementParam(deltaT);
				
			/*! Display user instructions. */
			DisplayFunctions::DisplayString(displayString);
		}

	}
	catch (exception& e)
	{
		string displayString;
		ostringstream eo;
		eo << e.what();
		displayString = "vbs2Fusion Error: " + eo.str() ;
		DisplayFunctions::DisplayString(displayString);
	}
	
};

/*!
The plugin function is called at the initialization of the plugin.
\param input is  an character which controls the execution of the
plugin
*/
VBSPLUGIN_EXPORT const char* WINAPI PluginFunction(const char *input)
{
	static const char result[]="[1.0, 3.75]";

	if (input[0] == 's')
	{		
		/*! Get the player unit. */
		playerUnit = MissionUtilities::getPlayer();

		/*! Update player static and dynamic properties. */
		UnitUtilities::updateStaticProperties(playerUnit);
		UnitUtilities::updateDynamicProperties(playerUnit);	

		/*! Declare a position 3 meters in front of the player unit.*/
		position3D pos(playerUnit.getPosition().getX(),
			playerUnit.getPosition().getZ() + 3,
			playerUnit.getPosition().getY());

		/*! Create pose control unit 3 meters in front of the player and update. */
		UnitUtilities::CreateUnit(pcUnit, pos);
		UnitUtilities::updateStaticProperties(pcUnit);
		UnitUtilities::updateDynamicProperties(pcUnit);

		displayString = "Pose Control Unit created...";
		displayString += "\\nCall plugin function with 'a' to demonstrate pose control.";

		started = true;

	}

	else if (input[0] == 'a')
	{
		if (started)
		{
			/*! Activate pose controlling over the pcUnit. */
			PoseControlUtilities::setExternalPose(pcUnit, true);
			PoseControlUtilities::setExternalPoseUpBody(pcUnit, true);
			PoseControlUtilities::setExternalPoseSkeleton(pcUnit, true);

			displayString = "Call plugin function with 'd' to revoke pose control.";
		}
	}

	else if (input[0] == 'd')
	{
		if (started)
		{
			/*! Deactivate pose controlling over the pcUnit. */
			PoseControlUtilities::setExternalPose(pcUnit, false);

			displayString = "Pose control revoked.";
		}
	}

	else if (input[0] == 'k')
	{
		if (started)
		{
			/*! Delete the pose control unit. */
			UnitUtilities::deleteUnit(pcUnit);

			started = false;
		}
	}

	return result;
};

VBSPLUGIN_EXPORT void WINAPI OnUnload()
{
	/*! Deactivate pose controlling over the pcUnit. */
	PoseControlUtilities::setExternalPose(pcUnit, false);
};

BOOL WINAPI DllMain(HINSTANCE hDll, DWORD fdwReason, LPVOID lpvReserved)
{
	switch(fdwReason)
	{
	case DLL_PROCESS_ATTACH:
		break;
	case DLL_PROCESS_DETACH:
		break;
	case DLL_THREAD_ATTACH:
		break;
	case DLL_THREAD_DETACH:
		break;
	}

	return TRUE;
};