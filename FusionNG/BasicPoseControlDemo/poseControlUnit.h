#include "VBS2Fusion.h"
#include "data/Unit.h"
#include "Math/Matrix4f.h"
#include "util/PoseControlUtilities.h"
#include "VBS2FusionDefinitions.h"

using namespace VBS2Fusion;

class PoseControlUnit : public Unit
{

public:
	/*! Set head movement variable value. */
	void setHeadMovementParam(float param);

	/*! Implementation for the "onModifyBone" virtual function. */
	bool onModifyBone(Matrix4f& mat, SKELETON_TYPE index);

private:
	float moveVal;

};