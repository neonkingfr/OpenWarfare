#ifndef __VBSPLUGIN_H__
#define __VBSPLUGIN_H__

#include <windows.h>
#include <d3d9.h>

#define VBSPLUGIN_EXPORT __declspec(dllexport)

#define RTT_WATER_REFLECTION "_int_refl" // Internal name for water reflections RTT

// Command function declaration
typedef int (WINAPI * ExecuteCommandType)(const char *command, char *result, int resultLength);
// camera frustum parameters
struct FrustumSpecNG
{
  // camera position
  double pointPosX;
  double pointPosY;
  double pointPosZ;

  // camera speed, used for OpenAL sounds
  float pointSpeedX;
  float pointSpeedY;
  float pointSpeedZ;

  // camera view direction
  float viewDirX;
  float viewDirY;
  float viewDirZ;

  // camera up-view
  float viewUpX;
  float viewUpY;
  float viewUpZ;

  // camera projection angle tangents
  // Beware: for now, top/bottom and left/right pairs are used as single (symmetric) values
  float projTanTop;
  float projTanBottom;
  float projTanLeft;
  float projTanRight;

  // near, far distance clipping
  float clipDistNear;
  float clipDistFar;
  float clipDistFarShadow;
  float clipDistFarSecShadow;
  float clipDistFarFog;

  // standard constructor
  FrustumSpecNG():
    pointPosX(0), pointPosY(0), pointPosZ(0), pointSpeedX(0), pointSpeedY(0), pointSpeedZ(0),
    viewDirX(0), viewDirY(0), viewDirZ(0), viewUpX(0), viewUpY(0), viewUpZ(0),
    projTanTop(0), projTanBottom(0), projTanLeft(0), projTanRight(0),
    clipDistNear(0),clipDistFar(0), clipDistFarShadow(0), clipDistFarSecShadow(0), clipDistFarFog(0)
  {}

  // auxiliary setting functions
  void SetPos(float posX, float posY, float posZ) {pointPosX = posX; pointPosY = posY; pointPosZ = posZ;}
  void SetSpeed(float speedX, float speedY, float speedZ) {pointSpeedX = speedX; pointSpeedY = speedY; pointSpeedZ = speedZ;}
  void SetDir(float dirX, float dirY, float dirZ) {viewDirX = dirX; viewDirY = dirY; viewDirZ = dirZ;}
  void SetUp(float upX, float upY, float upZ) {viewUpX = upX; viewUpY = upY; viewUpZ = upZ;}
  void SetProj(float top, float bottom, float left, float right) {projTanTop = top; projTanBottom = bottom; projTanLeft = left; projTanRight = right;}
  void SetClip(float nearStd, float farStd, float farShadow, float farSecShadow, float farFog)
  {
    clipDistNear = nearStd; clipDistFar = farStd; clipDistFarShadow = farShadow; clipDistFarSecShadow = farSecShadow; clipDistFarFog = farFog;
  }
};

// CB interface, used to do drawing ie. in paramDrawnPass
// Functions are the same or very similar as D3D device, but no value is returned.
// Use D3D9 debug runtime to identify problems
interface VBS2CBInterface
{
  virtual void SetTexture(DWORD Sampler, IDirect3DBaseTexture9 * pTexture)=0;
  virtual void SetRenderTarget(DWORD RenderTargetIndex, IDirect3DSurface9* pRenderTarget)=0;
  virtual void DrawPrimitive(D3DPRIMITIVETYPE PrimitiveType, UINT StartVertex, UINT PrimitiveCount)=0;
  virtual void SetPixelShader(IDirect3DPixelShader9* pShader)=0;
  virtual void SetVertexShader(IDirect3DVertexShader9* pShader)=0;
  virtual void SetVertexDeclaration(IDirect3DVertexDeclaration9* pDecl)=0;
  virtual void BeginScene()=0;
  virtual void EndScene()=0;
  virtual void SetStreamSource(UINT StreamNumber, IDirect3DVertexBuffer9 * pStreamData, UINT OffsetInBytes, UINT Stride)=0;
  virtual void DrawPrimitiveUP(D3DPRIMITIVETYPE PrimitiveType, UINT PrimitiveCount, CONST void* pVertexStreamZeroData, UINT VertexStreamZeroStride)=0;
  virtual void SetRenderState(D3DRENDERSTATETYPE State, DWORD Value)=0;
  virtual void SetSamplerState(DWORD Sampler, D3DSAMPLERSTATETYPE Type, DWORD Value)=0;
  virtual void SetIndices(IDirect3DIndexBuffer9 * pIndexData)=0;
  virtual void Clear(DWORD Count, CONST D3DRECT * pRects, DWORD Flags, D3DCOLOR Color, float Z, DWORD Stencil)=0;
  virtual void SetVertexShaderConstantB(UINT StartRegister, CONST BOOL * pConstantData, UINT BoolCount)=0;
  virtual void SetVertexShaderConstantF(UINT StartRegister, CONST float * pConstantData, UINT Vector4fCount)=0;
  virtual void SetVertexShaderConstantI(UINT StartRegister, CONST int * pConstantData, UINT Vector4iCount)=0;
  virtual void SetPixelShaderConstantB(UINT StartRegister, CONST BOOL * pConstantData, UINT BoolCount)=0;
  virtual void SetPixelShaderConstantF(UINT StartRegister, CONST float * pConstantData, UINT Vector4fCount)=0;
  virtual void SetPixelShaderConstantI(UINT StartRegister, CONST int * pConstantData, UINT Vector4iCount)=0;
  virtual void DrawIndexedPrimitive(D3DPRIMITIVETYPE Type, INT BaseVertexIndex, UINT MinIndex, UINT NumVertices, UINT StartIndex, UINT PrimitiveCount)=0; // Available in 91072
  virtual void StretchRect(IDirect3DSurface9 * pSourceSurface, CONST RECT * pSourceRect, IDirect3DSurface9 * pDestSurface, CONST RECT * pDestRect, D3DTEXTUREFILTERTYPE Filter)=0; // Available in 98134
};

// Possible draw passes used by the engine (used with onDrawnPass)
enum drawPassIdentifier
{
  drawPass1ON = 0, // Near by opaque objects
  drawPass1OF = 1, // Distant opaque objects
  drawPass1AF = 2, // Distant objects with some alpha
  drawPass1AN = 3, // Near by objects with some alpha
  drawPass2 = 4,  // Significantly alpha transparent objects like smoke particles
  drawPass3 = 5,  // Player body + vehicle pass, has its own depth range
  drawPass4 = 6,  // Compass, watch, etc. 3D objects. Has its own camera + depth range
  drawPassMapBg = 7, // Map background, before grid lines and other map elements are drawn
  drawPassMap   = 8, // End of map rendering  
  drawPassPpOverlayBegPP = 9,  // Pre-postprocess overlay buffer (since engine rev. 113629)
  drawPassPpOverlayEndPP = 10, // Post-postprocess overlay buffer (since engine rev. 113629)
  drawPassPpOverlayUI = 11,    // Post-UI overlay buffer (since engine rev. 113629)
};

// Draw mode identifier
enum drawModeIdentifier
{
  drawModeCommon = 0,         // Common rendering
  drawModeShadowBuffer = 1,   // Shadow buffer pass
  drawModeZPrime = 2,         // Z-priming
  drawModeDPrime = 3,         // Depth priming
  drawModeCrater = 4,         // Craters rendering (bullet holes)
  drawModeCaustics = 5,       // Underwater caustics pass
};

struct pvector3f
{
  float x,y,z;
};

enum rttType
{
  rttTypeCamera = 0,   // CameraEffect type RTT, controlled with camCreate
  rttTypeMirror = 1,   // Mirror effect type
  rttTypeMFD = 2,      // MFD display
  rttTypePrism = 3,    // Prism effect type (like mirror, but without mirroring)
  rttTypeReflection = 4, // Reflection special type (engine internal use only)
};

enum rttSensor
{
  rttSensorNormal = 0,
  rttSensorNightvision = 1,
  rttSensorThermal = 2,
  rttSensorNvgti = 3,
};

// Directional light, sun or moon light
struct pluginLightDir
{
  pvector3f direction; // Light direction
  pvector3f diffuse; // Diffuse color RGB
  pvector3f ambient; // Ambient color RGB
  float sunMoon; // 1.0 = Sun 0.0 = Moon
};

struct paramPreDraw
{
  IDirect3DDevice9 *dev;  // D3D device, can be used only during this function call
  FrustumSpecNG cameraFrustum;
  float clipNear_feedback;  // Plugins can set this to limit floating near clip plane
  char wantOverlayBegPP_feedback; // Plugin can set this to 1 to request pre-postprocess overlay buffer (since engine rev. 113629)
  char wantOverlayEndPP_feedback; // Plugin can set this to 1 to request post-postprocess overlay buffer (since engine rev. 113629)
  char wantOverlayUI_feedback;    // Plugin can set this to 1 to request post-UI overlay buffer (since engine rev. 113629)
  int mainThreadIdlePrev; // How many ms main thread idled in previous frame waiting for render thread or fps limit (since engine rev. 114745)
};

struct paramInitDraw
{
  IDirect3DDevice9 *dev;  // D3D device, can be used only during this function call
  __int32 renderWidth;    // Width of render area
  __int32 renderHeight;   // Height of render area
  float eyeAccom;         // Eye accomodation value
  float deltaT;           // Time in seconds since last frame
  pluginLightDir mainLight; // Main scene light (sun or moon). Available in 92080
};

struct paramDeviceInit
{
  IDirect3DDevice9 *dev;  // D3D device, can be used only during this function call
};

struct paramDeviceInvalidate
{
  IDirect3DDevice9 *dev;  // D3D device, can be used only during this function call
  bool isReset;           // true = device is reseted and will be restored later. false = device is being destroyed
};

struct paramDeviceRestore
{
  IDirect3DDevice9 *dev;  // D3D device, can be used only during this function call
};

struct paramDrawnPass
{
  VBS2CBInterface *cbi;
  drawPassIdentifier passId; // pass id, ie. pass 1, 2, 3
  drawModeIdentifier modeId; // ZPrime...
  char rttName[64];          // Rendered RTT name, or null string in case of main scene
  IDirect3DSurface9 *renderTarget;
  IDirect3DSurface9 *depthBuffer;
  FrustumSpecNG cameraFrustum;
};

struct paramDrawnSky
{
  VBS2CBInterface *cbi;
  char rttName[64];          // Rendered RTT name, or null string in case of main scene
  IDirect3DSurface9 *renderTarget;
  IDirect3DSurface9 *depthBuffer;
  FrustumSpecNG cameraFrustum;
};

struct paramFilledBackbuffer
{
  VBS2CBInterface *cbi;
  char rttName[64];          // Rendered RTT name, or null string in case of main scene
  IDirect3DSurface9 *renderTarget;
  IDirect3DSurface9 *depthBuffer;
};

struct paramPostProcessPre
{
  VBS2CBInterface *cbi;
  IDirect3DTexture9 *sourceTexture;
  IDirect3DSurface9 *renderTarget;
  int postFxQuality;
};

struct paramPostProcessFinal
{
  VBS2CBInterface *cbi;
  IDirect3DTexture9 *sourceTexture;
  IDirect3DSurface9 *renderTarget;
  int postFxQuality;
};

struct paramDrawnRTT
{
  VBS2CBInterface *cbi;
  char rttName[64];          // Rendered RTT name, or null string in case of main scene
  int width;                 // RTT width
  int height;                // RTT height
  float aspect;              // RTT aspect ratio
  rttType type;              // RTT type
  rttSensor sensor;          // RTT sensor type 
};

struct paramRttAccess
{
  IDirect3DDevice9 *dev;  // D3D device, can be used only during this function call
  char rttName[64];          // Rendered RTT name, or null string in case of main scene
  int width;                 // RTT width
  int height;                // RTT height
  float aspect;              // RTT aspect ratio
  rttType type;              // RTT type
  rttSensor sensor;          // RTT sensor type  
  IDirect3DTexture9 *rttTexture; // RTT texture
  int lastFrame;             // Last frame number where this RTT was active
  int currentFrame;          // Current frame number  
};


#endif