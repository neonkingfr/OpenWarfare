#include "PCH.h"

#include "pluginHeader.h"
#include "ControlledUnit.hpp"
#include "ControlledVehicle.hpp"
#include "VRPNClient.hpp"

//_________________________________________________________________________
VRPNClient::VRPNClient()
{
  Initialize();
}

//_________________________________________________________________________
void VRPNClient::OnSimStep()
{
  // update all trackers (from server)
  if (_pConnection)
  {
    TrackerList::iterator itTracker;
    for (itTracker = _trackers.begin(); itTracker != _trackers.end(); ++itTracker)
    {
      (*itTracker).second._remote->mainloop();
    }
    _pConnection->mainloop();
  }

  // update vehicles (characters are updated automatically via onModifyBone)
  VehiclesList::iterator itVehicle;
  for (itVehicle = _vehicles.begin(); itVehicle != _vehicles.end(); ++itVehicle)
  {
    (*itVehicle).second.OnUpdate();
  }

}

//_________________________________________________________________________
bool VRPNClient::Initialize()
{
  char msgbuf[1024];

  // get path to the config file
  LPTSTR strDLLPath = new TCHAR[2048];
  GetModuleFileName(NULL, strDLLPath, 2048);  //vbs2.exe path
  std::string pluginPath = strDLLPath;
  pluginPath = pluginPath.substr(0, (pluginPath.rfind("\\") + 1));
  std::string configFilePath = pluginPath.append("pluginsFusion\\VRPNClient.conf");
  sprintf(msgbuf, "[VRPNClient] Config file path:%s\n", configFilePath.c_str());
  OutputDebugString(msgbuf);    

  // try to open config file
  std::string line;
  std::ifstream inConfigFile;

  bool serverConfigFound = false;
  bool trackerMapFound = false;
  
  inConfigFile.open(configFilePath.c_str(), std::ifstream::in);  

  // parse the config file
  if (inConfigFile.is_open())
  {
    while (inConfigFile.good())
    {
      GetUncommentedLine(inConfigFile,line);

      // get server info from config and connect      
      if(line.find("[Server]", 0) != (size_t)-1)
      { 
        if (!InitConnection(line, inConfigFile))
          return false;
      } 
      // create controlled unit
      else if(line.find("[SkeletonTracker:", 0) != (size_t)-1)
      {
        InitUnit(line, inConfigFile);
      }
      // create controlled vehicle
      else if(line.find("[VehicleTracker:", 0) != (size_t)-1)
      {
        InitVehicle(line, inConfigFile);
      }
    }
    inConfigFile.close();
  }
  else
  {
    OutputDebugString("[VRPNClient]Cannot open VRPNClient.conf file!");
    return false;
  }

  return true;
}

//_________________________________________________________________________
void VRPNClient::LoadServerConfig(std::ifstream &inConfigFile, std::string &line)
{
  std::string addrval = "localhost";
  int portval = 3883;

  GetUncommentedLine(inConfigFile,line);
  if (line.find("address=") != (size_t)-1) 
  {
    OutputDebugString(line.c_str());
    addrval = GetConfValue(line);
  }

  GetUncommentedLine(inConfigFile,line);
  if (line.find("port=") != (size_t)-1) 
  {
    OutputDebugString(line.c_str());
    portval = atoi(GetConfValue(line).c_str());
  }   
  assert(portval >= 0);

  sprintf(_connectionName,"%s:%d", addrval.c_str(), portval);  
}
//_________________________________________________________________________
bool VRPNClient::IsNewConfigSection(const string &line)
{
  size_t left_br_index = line.find("[");
  if (left_br_index == (size_t)-1) {return false;}
  size_t right_br_index = line.find("]");
  if (right_br_index == (size_t)-1) {return false;}

  return true;
}

//_________________________________________________________________________
std::string VRPNClient::GetConfValue(std::string confString)
{
  int valIndex = (confString.find("=") + 1);
  if (valIndex == (size_t)-1) {return std::string("");}
  else {return confString.substr(valIndex, confString.size() - valIndex);}
}

//_________________________________________________________________________
bool VRPNClient::GetConfigValues(const string &confString, string &confName_out, string &confValue_out)
{
  int valIndex = (confString.find("=") + 1);
  if (valIndex == (size_t)-1)
  {
    confName_out = "";
    confValue_out = "";
    return false;
  }
  else
  {
    confName_out = confString.substr(0, valIndex - 1);
    confValue_out = confString.substr(valIndex, confString.size() - valIndex); 
  }
  return true;
}

//_________________________________________________________________________
void VRPNClient::GetUncommentedLine(std::ifstream& confFile, std::string& line)
{
  do 
  {
    if (confFile.eof()) {return;}
    getline (confFile,line); 
  } while (line.find_first_of("#") == 0);
}

//_________________________________________________________________________
VRPNClient::~VRPNClient()
{
}

//_________________________________________________________________________
bool VRPNClient::InitConnection()
{
  return true;
}

//_________________________________________________________________________
bool VRPNClient::InitConnection( std::string &line, std::ifstream& inConfigFile )
{
  OutputDebugString(line.c_str());
  LoadServerConfig(inConfigFile, line);

  // try to connect to the server
  _pConnection = vrpn_get_connection_by_name(_connectionName);
  if (!_pConnection)
  {
    OutputDebugString("[VRPNClient]Cannot connect to VRPN server!\n");
    return false;
  }
  OutputDebugString("[VRPNClient]Connected to the server\n");
  return true;
}

//_________________________________________________________________________
void VRPNClient::InitUnit( std::string &line, std::ifstream &inConfigFile )
{
  OutputDebugString(line.c_str());

  // get unit name
  int delimIndex = line.find_first_of(':');
  std::string unitName = line.substr(delimIndex + 1, line.find_first_of(']') - delimIndex - 1);
  OutputDebugString(unitName.c_str());

  // create new unit
  InitControlledUnit(unitName);

  // connect new unit to the trackers
  GetUncommentedLine(inConfigFile,line);
  OutputDebugString(line.c_str());
  std::streampos sp = inConfigFile.tellg();
  while (!IsNewConfigSection(line))
  {
    if (inConfigFile.eof()) { break; }
    OutputDebugString(line.c_str());

    string boneName = "";
    string trackerName = "";
    if (GetConfigValues(line, boneName, trackerName))
    {
      // check if the tracker already exists
      //if (_trackers.find(trackerName) == _trackers.end())
      {
        _trackers[trackerName]._remote = new vrpn_Tracker_Remote(trackerName.c_str(), _pConnection);
        _trackers[trackerName]._remote->register_change_handler(&_trackers[trackerName], HandleVRPNTracker);
      }

      // connect tracker to the unit's bone
      BonesNameIndexMap::const_iterator it = SkeletonDefinition.BonesMap.find(boneName);
      if (it != SkeletonDefinition.BonesMap.end())
      {
        int boneIndex = (*it).second;
        assert((boneIndex >= 0) && (boneIndex < MAX_BONES));

        _units[unitName]._boneTrackerMap[boneIndex] = trackerName;
      }
    }
    sp = inConfigFile.tellg();
    GetUncommentedLine(inConfigFile,line);
  }
  inConfigFile.seekg( sp ); // return stream to the prev entry
}

//_________________________________________________________________________
void VRPNClient::InitVehicle( std::string &line, std::ifstream &inConfigFile )
{
  OutputDebugString(line.c_str());

  // get unit name
  int delimIndex = line.find_first_of(':');
  std::string vehicleName = line.substr(delimIndex + 1, line.find_first_of(']') - delimIndex - 1);
  OutputDebugString(vehicleName.c_str());

  // try to get control of the vehicle specified
  InitControlledVehicle(vehicleName);

  // connect vehicle to the trackers
  GetUncommentedLine(inConfigFile,line);
  OutputDebugString(line.c_str());
  std::streampos sp = inConfigFile.tellg();
  while (!IsNewConfigSection(line))
  {
    if (inConfigFile.eof()) { break; }
    OutputDebugString(line.c_str());

    string attrName = "";
    string trackerName = "";
    if (GetConfigValues(line, attrName, trackerName))
    {
      if (!trackerName.empty())
      {
        _trackers[trackerName]._remote = new vrpn_Tracker_Remote(trackerName.c_str(), _pConnection);
        _trackers[trackerName]._remote->register_change_handler(&_trackers[trackerName], HandleVRPNTracker);
        _vehicles[vehicleName]._trackerName = trackerName;
      }
    }

    sp = inConfigFile.tellg();
    GetUncommentedLine(inConfigFile,line);
  }
  inConfigFile.seekg( sp ); // return stream to the prev entry
}


//_________________________________________________________________________
void VRPNClient::InitControlledUnit( std::string &unitName )
{
  VBS2Fusion::Unit& controlledUnit = _units[unitName];
  _units[unitName]._unitName = unitName;
  if (!stricmp(unitName.c_str(), "player"))
  {
    controlledUnit = VBS2Fusion::MissionUtilities::getPlayer();
  }
  else
  {
    int id1, id2, id3;
    GetObjectNetID(unitName.c_str(), &id1, &id2, &id3);
    VBS2Fusion::NetworkID unitNetID(id1,id2,id3);
    controlledUnit.setNetworkID(unitNetID);
    VBS2Fusion::UnitUtilities::updateStaticProperties(controlledUnit);
    VBS2Fusion::UnitUtilities::updateDynamicProperties(controlledUnit);
  }
}

//_________________________________________________________________________
void VRPNClient::EnablePoseControl(bool enable)
{
  PoseControlUnitList::iterator it;
  for (it = _units.begin(); it != _units.end(); ++it)
  {
     std::string unitName = (*it).first;
     PoseControlUnit& unit = (*it).second;
     // set pose control value to the current unit
     VBS2Fusion::PoseControlUtilities::setExternalPose(unit, enable);
     VBS2Fusion::PoseControlUtilities::setExternalPoseUpBody(unit, enable);
     VBS2Fusion::PoseControlUtilities::setExternalPoseSkeleton(unit, enable);

     char command[256];
     sprintf(command, "%s allowDamage %s", unitName.c_str(), enable?"false":"true");
     ExecuteCmd(command); 
     sprintf(command, "%s setDisableAnimationMove %s", unitName.c_str(), enable?"true":"false");
     ExecuteCmd(command);
/*
     sprintf(command, "%s setDir 0", unitName.c_str());
     ExecuteCmd(command); //reset unit orientation
*/
  }
}

//_________________________________________________________________________
void VRPNClient::InitControlledVehicle(std::string &unitName)
{
  int id1, id2, id3;
  GetObjectNetID(unitName.c_str(), &id1, &id2, &id3);
  VBS2Fusion::NetworkID unitNetID(id1,id2,id3);
  _vehicles[unitName].setNetworkID(unitNetID);
  VBS2Fusion::VehicleUtilities::updateStaticProperties(_vehicles[unitName]);
  VBS2Fusion::VehicleUtilities::updateDynamicProperties(_vehicles[unitName]);
  VBS2Fusion::VehicleUtilities::applyManualControl(_vehicles[unitName], true);
}

//_________________________________________________________________________
void VRPNClient::EnableVehicleControl(bool enable)
{
  VehiclesList::iterator it;
  for (it = _vehicles.begin(); it != _vehicles.end(); ++it)
  {
    std::string unitName = (*it).first;
    VBS2Fusion::VehicleUtilities::applyManualControl(_vehicles[unitName], enable);
  }
}

//_________________________________________________________________________
void	VRPN_CALLBACK HandleVRPNTracker (void *userdata, const vrpn_TRACKERCB t)
{
  if (!userdata) {return;}
  Tracker* pTracker = (Tracker*)userdata;
  if (pTracker) 
  {
    pTracker->_data = t;
    pTracker->_data.sensor = eTRACKER_INITIALIZED;  // used as initilization flag
  }
}

//_________________________________________________________________________
void	VRPN_CALLBACK HandleVRPNButtons (void *userdata, const vrpn_BUTTONCB t)
{
  if (!userdata) {return;}
  Buttons* pButtons = (Buttons*)userdata;
  if (pButtons) 
  {
    pButtons->_data = t;
  }
}


