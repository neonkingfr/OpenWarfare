#pragma once

#ifndef ControlledVehicle_h__
#define ControlledVehicle_h__

class ControlledVehicle : public VBS2Fusion::Vehicle
{
public:
  std::string _trackerName;

  ControlledVehicle();
  void OnUpdate();
};

#endif // ControlledVehicle_h__
