#ifndef __POSE_CONTROL_UNIT__
#define __POSE_CONTROL_UNIT__

#include <map>
#include "vrpn_Tracker.h"
#include "pluginHeader.h"
#include "data/Unit.h"
#include "..\Common\MathHelper.hpp"

typedef std::map<int,std::string> BoneTrackerMap;

class PoseControlUnit : public VBS2Fusion::Unit
{
public:
  BoneTrackerMap _boneTrackerMap;
  std::string _unitName;

private:
  // position updates
  float _pelvisXOffset, _pelvisPrevXOffset, _pelvisZOffset, _pelvisPrevZOffset;

public:
  PoseControlUnit();

  virtual bool onModifyBone(VBS2Fusion::Matrix4f& mat, VBS2Fusion::SKELETON_TYPE index);
};

#endif