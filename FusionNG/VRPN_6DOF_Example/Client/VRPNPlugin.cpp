#include "PCH.h"

#include "ControlledUnit.hpp" 
#include "VRPNClient.hpp"

VRPNClient* g_pClient = NULL;

VBSPLUGIN_EXPORT void WINAPI OnSimulationStep(float deltaT)
{
  if (g_pClient)
  {
    g_pClient->OnSimStep();
  }
};


VBSPLUGIN_EXPORT const char* WINAPI PluginFunction(const char *input)
{
	static const char result[]="[1.0]";
  VBS2Fusion::MissionUtilities::getPlayer();

	if (input[0] == 'i')
	{	
    if (!g_pClient)
    {
      g_pClient = new VRPNClient();
      g_pClient->EnablePoseControl(true);
      g_pClient->EnableVehicleControl(true);
    }
	}

	else if (input[0] == 'd')
	{
    g_pClient->EnablePoseControl(false);
    g_pClient->EnableVehicleControl(false);
	}

	else if (input[0] == 'e')
	{
    g_pClient->EnablePoseControl(true);
    g_pClient->EnableVehicleControl(true);
	}

	return result;
};

VBSPLUGIN_EXPORT void WINAPI OnUnload()
{
  if (g_pClient)
  {
    g_pClient->EnablePoseControl(false);
    g_pClient->EnableVehicleControl(false);
    delete g_pClient;
    g_pClient = NULL;
  }
};

BOOL WINAPI DllMain(HINSTANCE hDll, DWORD fdwReason, LPVOID lpvReserved)
{
	switch(fdwReason)
	{
	case DLL_PROCESS_ATTACH:
		break;
	case DLL_PROCESS_DETACH:
		break;
	case DLL_THREAD_ATTACH:
		break;
	case DLL_THREAD_DETACH:
		break;
	}

	return TRUE;
};

ExecuteCommandType ExecuteCommand = NULL;
int ExecuteCmd(const char* command, char *result, int resultLength)
{ 
  ZeroMemory(result,resultLength);
  if (!ExecuteCommand) return 0;
  return ExecuteCommand(command, result, resultLength);
}

VBSPLUGIN_EXPORT void WINAPI RegisterCommandFnc(void *executeCommandFnc)
{
  ExecuteCommand = (ExecuteCommandType)executeCommandFnc;
}

// get unit position
Vector3 GetUnitPosASL2(const char* name)
{
  float x, y, z;
  Vector3 res(0.0f,0.0f,0.0f);

  // get original position
  // Set orig pos
  char command[256];
  sprintf(command, "getPosASL2 %s", name);

  // send command
  char resultBuff[256];
  if(!ExecuteCommand(command, resultBuff, 256))
  {
    char debugText[256];
    sprintf(debugText, "GetPos failed: %s\n", resultBuff);
    OutputDebugString(debugText);
    return res;
  }

  // parse results
  int parse = sscanf(resultBuff, "[%f,%f,%f]", &x, &z, &y);
  if (parse != 3)
  {
    OutputDebugString("GetPos failed: can't parse command result\n");
    return res;
  }

  res = Vector3(x, y, z);
  return res;
}

// set unit position
void SetUnitPosASL2(const char* name, float x, float y, float z)
{
  char buf[512];
  sprintf(buf, "%s setPosASL2 [ %f, %f, %f]", name, x, z, y);
  ExecuteCmd(buf);
}

void SetUnitPosASL2(const char* name, const Vector3& pos)
{
  SetUnitPosASL2(name, pos.X(), pos.Z(), pos.Y());
} 

//get object network ID
void GetObjectNetID(const char* name, int* val1, int* val2, int* val3)
{
  char command[256];
  sprintf(command, "ObjToIdEx %s", name);

  char resultBuff[256];
  if(!ExecuteCommand(command, resultBuff, 256))
  {
    char debugText[256];
    sprintf(debugText, "ObjToIdEx failed: %s\n", resultBuff);
    OutputDebugString(debugText);
    return;
  }

  int parse = sscanf(resultBuff, "\"%i:%i:%i\"", val1, val2, val3);
  if (parse != 3)
  {
    OutputDebugString("GetPos failed: can't parse command result\n");
    return;
  }
}
