#pragma once

#ifndef VRPNClient_h__
#define VRPNClient_h__

#include <map>
#include "vrpn_Tracker.h"
#include "vrpn_Button.h"

class PoseControlUnit;
class ControlledVehicle;

typedef enum
{
  AXIS_DEFAULT = 0,
  UP_TO_NEGATIVE_UP,
  UP_TO_DIR,
  UP_TO_NEGATIVE_DIR,
  UP_TO_ASIDE,
  UP_TO_NEGATIVE_ASIDE,
  DIR_TO_ASIDE,
  DIR_TO_NEGATIVE_ASIDE
} eAXIS_ADJUSTMENT;

typedef enum
{
  ROTATION_DEFAULT = 0,
  ROTATEX_HALF_PI,
  ROTATEX_NEGATIVE_HALF_PI,
  ROTATEX_PI,
  ROTATEY_HALF_PI,
  ROTATEY_NEGATIVE_HALF_PI,
  ROTATEY_PI,
  ROTATEZ_HALF_PI,
  ROTATEZ_NEGATIVE_HALF_PI,
  ROTATEZ_PI
} eROTATION_ADJUSTMENT;

struct Tracker 
{
  vrpn_Tracker_Remote* _remote;
  vrpn_TRACKERCB _data;
};

struct Buttons 
{
  vrpn_Button_Remote* _remote;
  vrpn_BUTTONCB _data;
};


typedef std::map<std::string, PoseControlUnit> PoseControlUnitList;
typedef std::map<std::string, ControlledVehicle> VehiclesList;
typedef std::map<std::string, Tracker> TrackerList;
typedef std::map<std::string, Buttons> ButtonsList;

struct AdjustmentPair
{
  eAXIS_ADJUSTMENT _axis;
  eROTATION_ADJUSTMENT _rotation;
};

typedef std::map<std::string, AdjustmentPair> AdjustmentList;

// tracker-initialization flag
typedef enum
{
  eTRACKER_UNINITIALIZED = 0,
  eTRACKER_INITIALIZED
}ETrackerInitFlag;



class VRPNClient
{
  friend class PoseControlUnit;
  friend class ControlledVehicle;

private:
  //! connection string (server:port)
  char _connectionName[1024];

  //! vrpn connection
  vrpn_Connection* _pConnection;

  //! list of controlled units
  PoseControlUnitList _units;
  VehiclesList _vehicles;

  //! list of VRPN trackers
  TrackerList _trackers;
  
public:
  VRPNClient();
  ~VRPNClient();

  bool InitConnection();
  void OnSimStep();
  void EnablePoseControl(bool enable = true);
  void EnableVehicleControl(bool enable = true);

private:
  bool Initialize();
  bool InitConnection( std::string &line, std::ifstream& inConfigFile );
  void InitUnit( std::string &line, std::ifstream &inConfigFile );
  void InitVehicle( std::string &line, std::ifstream &inConfigFile );
  void InitControlledUnit( std::string &unitName );
  void InitControlledVehicle(std::string &unitName);
  void LoadServerConfig(std::ifstream &inConfigFile, std::string &line);
  std::string GetConfValue(std::string confString);
  void GetUncommentedLine(std::ifstream& confFile, std::string& line);
  bool IsNewConfigSection(const string &line);
  bool GetConfigValues(const string &confString, string &confName_out, string &confValue_out);
};

//! tracker handler (updates tracker data from remote tracker)
void	VRPN_CALLBACK HandleVRPNTracker (void *userdata, const vrpn_TRACKERCB t);
void	VRPN_CALLBACK HandleVRPNButtons (void *userdata, const vrpn_BUTTONCB t);

#endif // VRPNClient_h__
