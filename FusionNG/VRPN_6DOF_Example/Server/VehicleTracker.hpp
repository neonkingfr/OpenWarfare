#pragma once

#ifndef VehicleTracker_h__
#define VehicleTracker_h__

class VehicleTracker : public vrpn_Tracker
{
  struct timeval _timestamp;

public:
  VehicleTracker(const char* trackerName, vrpn_Connection *c = 0);
  void mainloop();
};

#endif // VehicleTracker_h__
