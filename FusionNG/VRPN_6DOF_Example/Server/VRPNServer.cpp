// VRPNServer.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include <stdio.h>
#include <tchar.h>

#include "..\Common\VBS2_2xx_Skeleton.hpp"
#include "BoneTracker.h"
#include "VehicleTracker.hpp"

#include <iostream>
using namespace std;

//typedef std::map<std::string, >

////////////// MAIN ///////////////////

int _tmain(int argc, _TCHAR* argv[])
{
  // Creating the network server
  vrpn_Connection_IP* m_Connection = new vrpn_Connection_IP();
  std::vector<BoneTracker*> boneTrackers;
  std::vector<VehicleTracker*> btnTrackers;

  // Creating the tracker
  // - just one for demonstration purposes (tracker for the head bone)
  // - the name is important (a bone is connected to the corresponding tracker
  //   by the name specified in the configuration file - F.I. "HEAD=headtracker" in this case)
  //BoneTracker* bt = new BoneTracker("Head", m_Connection); 
  // 

  // create skeleton trackers
  BonesNameIndexMap::const_iterator itBone;
  for (itBone = SkeletonDefinition.BonesMap.begin(); itBone != SkeletonDefinition.BonesMap.end(); ++itBone)
  { 
    std::string boneName = (*itBone).first;
    int boneIndex = (*itBone).second;
    Matrix4 bp = SkeletonDefinition.Bones[boneIndex];
    BoneTracker* bt = new BoneTracker(boneName.c_str(), bp, m_Connection);
    boneTrackers.push_back(bt);
  } 

  VehicleTracker* vt = new VehicleTracker("vehicle1", m_Connection);

  cout << "VRPN Server initialized." << endl;

  while(true)
  {
    BoneTracker::ANGLE += 0.0006f;

    // update tracker
    vt->mainloop();

    // update all bone-trackers
    for (int i = 0; i < boneTrackers.size(); i++)
    {
      boneTrackers[i]->mainloop();
    }

    //update connection
    m_Connection->mainloop();

    // Calling Sleep to let the CPU breathe.
    SleepEx(1,FALSE);
  }
}
