#include "stdafx.h"
#include "BoneTracker.h"
#include "VehicleTracker.hpp"

VehicleTracker::VehicleTracker(const char* trackerName, vrpn_Connection *c)
: vrpn_Tracker(trackerName, c)
{
}

void VehicleTracker::mainloop()
{
  // Update all tracker data in this method
  // - we will use some fake data for demonstration   
  vrpn_gettimeofday(&_timestamp, NULL);
  vrpn_Tracker::timestamp = _timestamp;

  float thrustWanted = sinf(BoneTracker::ANGLE/2.0f);
  float turnWanted = -sinf(BoneTracker::ANGLE);

  // encode thrust/turn wanted to the tracker's position
  // (creating a new custom vrpn class would be a proper solution)
  pos[0] = thrustWanted;
  pos[1] = turnWanted;

  // pack network message
  char msgbuf[1000];
  int  len = vrpn_Tracker::encode_to(msgbuf);
  if (d_connection->pack_message(len, _timestamp, position_m_id, d_sender_id, msgbuf, vrpn_CONNECTION_LOW_LATENCY))
  {
    fprintf(stderr,"can't write message: tossing\n");
  }

  server_mainloop();
}