#ifndef _MATH_HELPER_H
#define _MATH_HELPER_H

#include "../../_depends/BISDK/Include/BIMatrix.h"
#include "Math/Matrix4f.h"

using namespace VBS2Fusion;

#ifndef NULL
#define NULL
#endif

#define DOUBLE_PRECISION 0 // GetInvertGeneral not working in DP for now

#define H_PI  ( 3.14159265358979323846 )
const double	PI = 3.14159265358979323846;
const double	HALF_PI = 1.57079632679489661923;
const double	R_TO_D = 180.0/PI;
const double	D_TO_R = PI/180.0;

#if DOUBLE_PRECISION

#define Matrix4 BI_Matrix4d
#define Matrix3 BI_Matrix3d
#define Vector3 BI_Vector3D

const BI_Vector3D VUpPC(0.0, 1.0, 0.0);
const BI_Vector3D VForwardPC(0.0, 0.0, 1.0);
const BI_Vector3D VAsidePC(1.0, 0.0, 0.0);

#define VZero BI_ZeroVector3D
#define VUp VUpPC
#define VForward VForwardPC
#define VAside VAsidePC
#define MIdentity BI_IdentityMatrix4d
#define MZero BI_ZeroMatrix3d

#else

#define Matrix4 BI_Matrix4
#define Matrix3 BI_Matrix3
#define Vector3 BI_Vector3

const BI_Vector3 VUpPC(0.0, 1.0, 0.0);
const BI_Vector3 VForwardPC(0.0, 0.0, 1.0);
const BI_Vector3 VAsidePC(1.0, 0.0, 0.0);

#define VZero BI_ZeroVector3
#define VUp VUpPC
#define VForward VForwardPC
#define VAside VAsidePC
#define MIdentity BI_IdentityMatrix4
#define MZero BI_ZeroMatrix3

#endif

static void Vector3ToVector3f(Vector3 &vec3, Vector3f &vec3f)
{
  vec3f.setX(vec3.X());
  vec3f.setY(vec3.Y());
  vec3f.setZ(vec3.Z());
}

static void Vector3fToVector3(Vector3f &vec3f, Vector3 &vec3)
{
  vec3.Set(vec3f.X(), vec3f.Y(), vec3f.Z());
}

static void Matrix4ToMatrix4f(Matrix4 &mat4, Matrix4f &mat4f)
{
  Vector3 direction      =  mat4.GetDirection();
  Vector3 directionUp    =  mat4.GetUp();
  Vector3 directionAside = mat4.GetAside();
  Vector3 position       = mat4.GetPosition();

  mat4f.setElement(0,0, directionAside.X()); 
  mat4f.setElement(0,1, directionAside.Y());
  mat4f.setElement(0,2, directionAside.Z());

  mat4f.setElement(1,0, directionUp.X());
  mat4f.setElement(1,1, directionUp.Y());
  mat4f.setElement(1,2, directionUp.Z());

  mat4f.setElement(2,0, direction.X());
  mat4f.setElement(2,1, direction.Y());
  mat4f.setElement(2,2, direction.Z());

  mat4f.setElement(0,3, position.X());
  mat4f.setElement(1,3, position.Y());
  mat4f.setElement(2,3, position.Z());

};

static void Matrix4fToMatrix4(Matrix4f &mat4f, Matrix4 &mat4)
{
  Vector3f direction =  mat4f.getDirection();
  Vector3 dirVec(direction.X(),direction.Y(),direction.Z());
  mat4.SetDirection(dirVec);

  Vector3f directionUp =  mat4f.getDirectionUp();
  Vector3 dirupVec(directionUp.X(),directionUp.Y(),directionUp.Z());
  mat4.SetUp(dirupVec);

  Vector3f directionAside = mat4f.getDirectionSide();
  Vector3 dirsideVec(directionAside.X(),directionAside.Y(),directionAside.Z());
  mat4.SetAside(dirsideVec);

  Vector3f position = mat4f.getPosition();
  Vector3 dirposVec(position.X(),position.Y(),position.Z());
  mat4.SetPosition(dirposVec);
};

#endif