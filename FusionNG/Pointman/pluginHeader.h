#ifndef PLUGIN_HEADER_H
#define PLUGIN_HEADER_H

#define _WIN32_WINNT 0x0502
#include <windows.h>

#pragma warning(disable:4244 4305) // double to float conversion warnings, disables in64 to int as well
#pragma warning(disable:4065) // switch statement contains 'default' but no 'case' labels
#pragma warning(disable:4018) // signed/unsigned mismatch

#define _BISIM_DEV_EXTERNAL_POSE 1
#define _MOVE_TO_SURFACE 0
#define _RESET_TORSO_TRANS_ON_LADDER  0
#define _USE_SWITCH_ACTION 0

//#define _CRT_SECURE_NO_WARNINGS // Defined in preprocessor
//#define _CRT_SECURE_CPP_OVERLOAD_STANDARD_NAMES // Consider this instead

#define VBSPLUGIN_EXPORT __declspec(dllexport)

VBSPLUGIN_EXPORT void WINAPI OnSimulationStep(float deltaT);
VBSPLUGIN_EXPORT const char* WINAPI PluginFunction(const char *input);


#endif
