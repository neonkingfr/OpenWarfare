///////////////////////////////////////////////////////////////////////////////
// VbsPointman2.h
//	VbsPointman2 API exported function declarations.
//	For use by VBS client applications which interface with Pointman via the
//		dynamic link library "VbsPointman_Interface.dll".
//	Contants, type definitions and utility functions are contained in the
//		header file "V2_Interface.h".
//	For background and additional information, reference the document 
//		"VBS-Pointman API Version 2.0".
//	Author: Patricia Denbrook
//	Creation Date: 20091002
//	Last modified: 20091005
//	Copyright 2009 Denbrook Computing Services, Inc.
///////////////////////////////////////////////////////////////////////////////
#pragma once
#include "V2_Interface.h"

#ifdef BUILD_VBS_POINTMAN2_INTERFACE
#define VBS_POINTMAN2_INTERFACE_EXPORT __declspec(dllexport)
#else
#define VBS_POINTMAN2_INTERFACE_EXPORT __declspec(dllimport)
#endif

/////////////////////////////////////////////////////////////////////////////////////////
// Data Transmission Functions
/////////////////////////////////////////////////////////////////////////////////////////

VBS_POINTMAN2_INTERFACE_EXPORT bool V2_GetServerMotionXmit(
								   V2_Interface::_V2_ServerMotionDat& serverMotionDat,
								   BYTE& bError, WORD& wSeqnum);

VBS_POINTMAN2_INTERFACE_EXPORT bool V2_GetServerJoyXmit(
								   V2_Interface::_V2_ServerJoyDat& serverJoyDat,
								   BYTE& bError, WORD& wSeqnum);

VBS_POINTMAN2_INTERFACE_EXPORT bool V2_SetClientStateXmit(
							       const V2_Interface::_V2_ClientStateDat& clientStateDat);

VBS_POINTMAN2_INTERFACE_EXPORT bool V2_GetClientStateXmitInfo(
								   BYTE& bError, WORD& wSeqnum);

VBS_POINTMAN2_INTERFACE_EXPORT DWORD V2_GetClientStateXmitFreq();

/////////////////////////////////////////////////////////////////////////////////////////
// Simulation Control Functions
/////////////////////////////////////////////////////////////////////////////////////////

VBS_POINTMAN2_INTERFACE_EXPORT bool V2_CmdSetSkeletalDef(
								   const V2_Interface::_V2_SkeletalDef& skeletalDef, 
								   BYTE* pbError = NULL, WORD* pwSeqnum = NULL);

VBS_POINTMAN2_INTERFACE_EXPORT bool V2_CmdSetUserParams(
								   const V2_Interface::_V2_UserParams& userParams, 
								   BYTE* pbError = NULL, WORD* pwSeqnum = NULL);

VBS_POINTMAN2_INTERFACE_EXPORT bool V2_CmdSetDisplayConfig(
								   const V2_Interface::_V2_DisplayConfig& displayConfig, 
								   BYTE* pbError = NULL, WORD* pwSeqnum = NULL);

VBS_POINTMAN2_INTERFACE_EXPORT bool V2_CmdSetUserView(BYTE bUserView, 
								   BYTE* pbError = NULL, WORD* pwSeqnum = NULL);

VBS_POINTMAN2_INTERFACE_EXPORT bool V2_CmdSetMotionState(
								   V2_Interface::_V2_MotionState& motionState, 
								   BYTE* pbError = NULL, WORD* pwSeqnum = NULL);

VBS_POINTMAN2_INTERFACE_EXPORT bool V2_CmdSetPrimaryWeapon(
								   const V2_Interface::_V2_AimObject& object, 
								   BYTE* pbError = NULL, WORD* pwSeqnum = NULL);

VBS_POINTMAN2_INTERFACE_EXPORT bool V2_CmdSetSecondaryWeapon(
								   const V2_Interface::_V2_AimObject& object, 
								   BYTE* pbError = NULL, WORD* pwSeqnum = NULL);

VBS_POINTMAN2_INTERFACE_EXPORT bool V2_CmdSetPistolWeapon(
								   const V2_Interface::_V2_AimObject& object, 
								   BYTE* pbError = NULL, WORD* pwSeqnum = NULL);

VBS_POINTMAN2_INTERFACE_EXPORT bool V2_CmdSetMountedWeapon(
								   const V2_Interface::_V2_MountedAimObject& object, 
								   BYTE* pbError = NULL, WORD* pwSeqnum = NULL);

VBS_POINTMAN2_INTERFACE_EXPORT bool V2_CmdSetBinoculars(
								   const V2_Interface::_V2_AimObject& object, 
								   BYTE* pbError = NULL, WORD* pwSeqnum = NULL);

VBS_POINTMAN2_INTERFACE_EXPORT bool V2_CmdSetCarriedObject(
								   const V2_Interface::_V2_HeldObject& object, 
								   BYTE* pbError = NULL, WORD* pwSeqnum = NULL);

VBS_POINTMAN2_INTERFACE_EXPORT bool V2_CmdSetDraggedObject(
								   const V2_Interface::_V2_HeldObject& object, 
								   BYTE* pbError = NULL, WORD* pwSeqnum = NULL);

VBS_POINTMAN2_INTERFACE_EXPORT bool V2_CmdSetFireState(
								   const V2_Interface::_V2_FireState& fireState, 
								   BYTE* pbError = NULL, WORD* pwSeqnum = NULL);

VBS_POINTMAN2_INTERFACE_EXPORT bool V2_CmdReload(
								   const V2_Interface::_V2_AmmoObject& ammo, 
								   BYTE* pbError = NULL, WORD* pwSeqnum = NULL);

/////////////////////////////////////////////////////////////////////////////////////////
// Simulation Query Functions
/////////////////////////////////////////////////////////////////////////////////////////

VBS_POINTMAN2_INTERFACE_EXPORT bool V2_GetSkeletalDef(
								    V2_Interface::_V2_SkeletalDef& skeletalDef);

VBS_POINTMAN2_INTERFACE_EXPORT bool V2_GetUserParams(
									V2_Interface::_V2_UserParams& userParams);

VBS_POINTMAN2_INTERFACE_EXPORT bool V2_GetDisplayConfig(
									V2_Interface::_V2_DisplayConfig& displayConfig);

VBS_POINTMAN2_INTERFACE_EXPORT bool V2_GetUserView(BYTE& bUserView);

VBS_POINTMAN2_INTERFACE_EXPORT bool V2_GetMotionState(
									V2_Interface::_V2_MotionState& motionState);

VBS_POINTMAN2_INTERFACE_EXPORT bool V2_GetPrimaryWeapon(
									V2_Interface::_V2_AimObject& object);

VBS_POINTMAN2_INTERFACE_EXPORT bool V2_GetSecondaryWeapon(
									V2_Interface::_V2_AimObject& object);

VBS_POINTMAN2_INTERFACE_EXPORT bool V2_GetPistolWeapon(
									V2_Interface::_V2_AimObject& object);

VBS_POINTMAN2_INTERFACE_EXPORT bool V2_GetMountedWeapon(
									V2_Interface::_V2_MountedAimObject& object);

VBS_POINTMAN2_INTERFACE_EXPORT bool V2_GetBinoculars(
									V2_Interface::_V2_AimObject& object);

VBS_POINTMAN2_INTERFACE_EXPORT bool V2_GetCarriedObject(
									V2_Interface::_V2_HeldObject& object);

VBS_POINTMAN2_INTERFACE_EXPORT bool V2_GetDraggedObject(
									V2_Interface::_V2_HeldObject& object);

VBS_POINTMAN2_INTERFACE_EXPORT bool V2_GetFireState(
									V2_Interface::_V2_FireState& fireState);

VBS_POINTMAN2_INTERFACE_EXPORT bool V2_CmdGetPreSitPelvisRotations(
									V2_Interface::_V2_PreSitPelvisRotations& preSitPelvisRotations, 
									BYTE* pbError, WORD* pwSeqnum);

VBS_POINTMAN2_INTERFACE_EXPORT bool V2_CmdGetSimControlParams(
									V2_Interface::_V2_SimControlParams& params, 
									BYTE* pbError, WORD* pwSeqnum);

/////////////////////////////////////////////////////////////////////////////////////////
// Interface Control Functions
/////////////////////////////////////////////////////////////////////////////////////////

VBS_POINTMAN2_INTERFACE_EXPORT bool V2_LoadLib(
								   WORD wInterfaceType,
								   LPCTSTR lpszLibDirPath);

VBS_POINTMAN2_INTERFACE_EXPORT bool V2_FreeLib();

VBS_POINTMAN2_INTERFACE_EXPORT bool V2_Connect(
								   LPCTSTR lpszServerIPAddr = 0);

VBS_POINTMAN2_INTERFACE_EXPORT bool V2_Disconnect();

VBS_POINTMAN2_INTERFACE_EXPORT bool V2_CmdSetXmitFreqRanges(
									const V2_Interface::_V2_XmitFreqRanges& xmitFreqRanges, 
									BYTE* pbError = NULL, WORD* pwSeqnum = NULL);

VBS_POINTMAN2_INTERFACE_EXPORT bool V2_CmdStartServerInputGen(
								   BYTE* pbError = NULL, WORD* pwSeqnum = NULL);

VBS_POINTMAN2_INTERFACE_EXPORT bool V2_CmdStopServerInputGen(
								   BYTE* pbError = NULL, WORD* pwSeqnum = NULL);

VBS_POINTMAN2_INTERFACE_EXPORT bool V2_CmdStartXmit(
								   BYTE* pbError = NULL, WORD* pwSeqnum = NULL);

VBS_POINTMAN2_INTERFACE_EXPORT bool V2_CmdStopXmit(
								   BYTE* pbError = NULL, WORD* pwSeqnum = NULL);

/////////////////////////////////////////////////////////////////////////////////////////
// Interface Query Functions
/////////////////////////////////////////////////////////////////////////////////////////

VBS_POINTMAN2_INTERFACE_EXPORT WORD V2_GetInterfaceType();

VBS_POINTMAN2_INTERFACE_EXPORT bool V2_IsConnected();

VBS_POINTMAN2_INTERFACE_EXPORT bool V2_GetXmitFreqRanges(
								    V2_Interface::_V2_XmitFreqRanges& xmitFreqRanges);

VBS_POINTMAN2_INTERFACE_EXPORT BYTE V2_GetClientStatus();

VBS_POINTMAN2_INTERFACE_EXPORT bool V2_CmdGetServerStatus(
								    BYTE& bStatus, 
								    BYTE* pbError = NULL, WORD* pwSeqnum = NULL);

VBS_POINTMAN2_INTERFACE_EXPORT bool V2_CmdGetServerJoycaps(
								    V2_Interface::_V2_ServerJoyCaps& joyCaps, 
								    BYTE* pbError = NULL, WORD* pwSeqnum = NULL);

VBS_POINTMAN2_INTERFACE_EXPORT BYTE V2_GetServerInputGenStartError();

/////////////////////////////////////////////////////////////////////////////////////////
// Micellaneous functions
/////////////////////////////////////////////////////////////////////////////////////////

VBS_POINTMAN2_INTERFACE_EXPORT bool V2_GetNrlSkeletalDef(
								    V2_Interface::_V2_SkeletalDef& skeletalDef);

VBS_POINTMAN2_INTERFACE_EXPORT bool V2_GetVbsSkeletalDef(
								    V2_Interface::_V2_SkeletalDef& skeletalDef);

VBS_POINTMAN2_INTERFACE_EXPORT bool V2_GetPointmanDirectedRotLimits(
										BYTE bBodyPosture, BYTE bHeldObject, BYTE bAiming,
										V2_Interface::_V2_RotLimits& rotLimitsDirTorso,
										V2_Interface::_V2_RotLimits& rotLimitsDirView);

VBS_POINTMAN2_INTERFACE_EXPORT bool V2_CmdGetPointmanPrimaryWeapon(
								    V2_Interface::_V2_AimObject& object, 
								    BYTE* pbError = NULL, WORD* pwSeqnum = NULL);

VBS_POINTMAN2_INTERFACE_EXPORT bool V2_CmdGetPointmanSecondaryWeapon(
								    V2_Interface::_V2_AimObject& object, 
								    BYTE* pbError = NULL, WORD* pwSeqnum = NULL);

VBS_POINTMAN2_INTERFACE_EXPORT bool V2_CmdGetPointmanPistolWeapon(
								    V2_Interface::_V2_AimObject& object, 
								    BYTE* pbError = NULL, WORD* pwSeqnum = NULL);

VBS_POINTMAN2_INTERFACE_EXPORT bool V2_CmdGetPointmanBinoculars(
								    V2_Interface::_V2_AimObject& object, 
								    BYTE* pbError = NULL, WORD* pwSeqnum = NULL);

VBS_POINTMAN2_INTERFACE_EXPORT bool V2_CmdGetPointmanMountedWeapon(
								    V2_Interface::_V2_MountedAimObject& object, 
								    BYTE* pbError = NULL, WORD* pwSeqnum = NULL);

VBS_POINTMAN2_INTERFACE_EXPORT bool V2_CmdGetPointmanCarriedObject(
								    V2_Interface::_V2_HeldObject& object, 
								    BYTE* pbError = NULL, WORD* pwSeqnum = NULL);

VBS_POINTMAN2_INTERFACE_EXPORT bool V2_CmdGetPointmanDraggedObject(
								    V2_Interface::_V2_HeldObject& object, 
								    BYTE* pbError = NULL, WORD* pwSeqnum = NULL);

VBS_POINTMAN2_INTERFACE_EXPORT bool V2_ResetClientStateXmit();

VBS_POINTMAN2_INTERFACE_EXPORT bool V2_GetClientStateXmit(
								    V2_Interface::_V2_ClientStateDat& clientStateDat);

//#ifdef _WINDOWS
//VBS_POINTMAN2_INTERFACE_EXPORT bool V2_CreateServerStatusWnd(void* pParentWnd = NULL);
class CWnd
{
};
VBS_POINTMAN2_INTERFACE_EXPORT bool V2_CreateServerStatusWnd(CWnd* pParentWnd = NULL);
//#endif

VBS_POINTMAN2_INTERFACE_EXPORT bool V2_LoadServerConfig(LPCTSTR lpszFilename);



