#include "pluginHeader.h"

#include <delayimp.h>

#include "Pointman.h"
#include "VbsPointman2.h"
#include "PointmanPose.h"

using namespace VBS2Fusion;

static bool PointmanEnabled = false;
static bool MissionStarted = false;
std::string PointmanIP = "10.0.10.103";
std::string PointmanPath = "c:\\Program Files (x86)\\DCS\\Gaiter_30125";

bool PoseControlUnit::onModifyUpperView(Matrix4f& mat4f)
{
  // ?? this callback appears to be replaced by onModifyHeadTrans below
  if (GExplicitPose)
  {
    Matrix4 mat;
    Matrix4fToMatrix4(mat4f, mat);
    mat = GExplicitPose->GetView();
    Matrix4ToMatrix4f(mat, mat4f);
  }
  return true;
}

bool PoseControlUnit::onModifyUpperTroso(Matrix4f& mat4f)
{
  if (GExplicitPose)
  {
    Matrix4 mat;
    Matrix4fToMatrix4(mat4f, mat);
    mat = GExplicitPose->GetTorso();
    Matrix4ToMatrix4f(mat, mat4f);
  }
  return true;
}

bool PoseControlUnit::onModifyUpperAim(Matrix4f& mat4f)
{
  // ?? this callback appears to be replaced by onModifyGunTrans below
  if (GExplicitPose)
  {
    Matrix4 mat;
    Matrix4fToMatrix4(mat4f, mat);
    mat = GExplicitPose->GetAim();
    Matrix4ToMatrix4f(mat, mat4f);
  }
  return true;
}

bool PoseControlUnit::onModifyBone(Matrix4f& mat4f, SKELETON_TYPE index)
{
  if (GExplicitPose)
  {
    Matrix4 mat;
    Matrix4fToMatrix4(mat4f, mat);
    GExplicitPose->ModifyBone(mat, index);
    Matrix4ToMatrix4f(mat, mat4f);
  }
  return true;
}

bool PoseControlUnit::onModifyGunTrans(Matrix4f& mat4f)
{
  if (GExplicitPose)
  {
    Matrix4 mat;
    Matrix4fToMatrix4(mat4f, mat);
    GExplicitPose->ModifyGunTrans(mat);
    Matrix4ToMatrix4f(mat, mat4f);
  }
  return true;
}

bool PoseControlUnit::onModifyHeadTrans(Matrix4f& mat4f)
{
  if (GExplicitPose)
  {
    Matrix4 mat;
    Matrix4fToMatrix4(mat4f, mat);
    GExplicitPose->ModifyHeadTrans(mat);
    Matrix4ToMatrix4f(mat, mat4f);
  }
  return true;
}

bool PoseControlUnit::onModifyLookTrans(Matrix4f& mat4f)
{
  if (GExplicitPose)
  {
    Matrix4 mat;
    Matrix4fToMatrix4(mat4f, mat);
    GExplicitPose->ModifyLookTrans(mat);
    Matrix4ToMatrix4f(mat, mat4f);
  }
  return true;
}

bool PoseControlUnit::onModifyRelativeChange(Matrix4f& mat4f)
{
  if (GExplicitPose)
  {
    Matrix4 mat;
    Matrix4fToMatrix4(mat4f, mat);
    mat = GExplicitPose->GetRelativeChange();
    Matrix4ToMatrix4f(mat, mat4f);
  }
  return true;
}

bool PoseControlUnit::onReload()
{
  if (GExplicitPose)
  {
    GExplicitPose->SetReload();
  }
  return true;
}


void LogF(const char* format, ...)
{
  va_list     argptr;
  char        str[1024];
  va_start (argptr,format);
  vsnprintf (str,1024,format,argptr);
  va_end   (argptr);
  OutputDebugString(str);
  OutputDebugString("\n");
}

int Pointman2Load()
{
	LogF("{info} Pointman: Pointman2Load()");

	// First time only, add the VBS2 lib directory to the dll search path so that
	// the VbsPointman interface dll (VbsPointman2_Interface.dll) can be loaded via
	// calls to V2_LoadLib.
	static bool bLibPathSet = false;
	if (!bLibPathSet)
	{
		char oldDirectory[1024];
		GetCurrentDirectory(1024, oldDirectory);
		string curDir(oldDirectory);
		string path = curDir + "\\lib";
		SetDllDirectory(path.c_str());
		bLibPathSet = true;
	}

	// Update the user profile directory in case it has changed
	ValidateProfile();

	// Load Pointman using the directory path and IP address from Pointman.cfg in the
	//	Config folder of the user profile directory; if these are not found (e.g., Pointman.cfg
	//	does not exist for this user profile, Pointman is not loaded and the standard VBS2
	//	controls are used.
	if (LoadPathFromConfig(PointmanPath) && LoadIPFromConfig(PointmanIP))
	{
		// Enable external joystick and disable TrackIR prior to initializing Pointman
		// (TrackIR must be disabled when loading Pointman in direct mode in order for Pointman
		// to get control over it)
		EnableExternalJoystick(true);
		EnableTrackIR(false);

		bool bPointmanLoaded = false;

		if (PointmanIP.size() > 0)
		{
			// Load Pointman using the UDP interface
			if (V2_LoadLib(V2_Interface::V2_INTERFACE_UDP, PointmanPath.data()))
			{
				LogF("{info} Pointman: Library loaded, UDP protocol used, executables at location: %s", PointmanPath.data());
				if (V2_Connect(PointmanIP.data()))
				{
					LogF("{info} Pointman: Connection to server established, server IP: %s", PointmanIP.data());
					bPointmanLoaded = true;
				}
				else
				{
					LogF("{error} Pointman: Unable to connect to server %s", PointmanIP.data());
					V2_FreeLib();
				}
			}
			else
			{
				LogF("{error} Pointman: Initialization of Pointman using UDP protocol failed, executables at location: %s", PointmanPath.data());
			}
		}
		else
		{
			// Load Pointman using the Direct interface
			if (V2_LoadLib(V2_Interface::V2_INTERFACE_DIRECT, PointmanPath.data()))
			{
				LogF("{info} Pointman: Library loaded, direct access used, executables at location: %s", PointmanPath.data());
				bPointmanLoaded = true;
			}
			else
			{
				LogF("{error} Pointman: Initialization of Pointman using direct access failed, executables at location: %s", PointmanPath.data());
			}
		}

		// If load succeeded, set the frequency ranges
		if (bPointmanLoaded)
		{
			V2_Interface::_V2_XmitFreqRanges xmitFreqRanges;
			xmitFreqRanges.nMinFreqServerMotion = 1;
			xmitFreqRanges.nMaxFreqServerMotion = 60;
			xmitFreqRanges.nMinFreqServerJoy = 1;
			xmitFreqRanges.nMaxFreqServerJoy = 60;
			xmitFreqRanges.nMinFreqClientState = 1;
			xmitFreqRanges.nMaxFreqClientState = 60;
			BYTE bRspError;
			WORD wRspSeqnum;
			if (V2_CmdSetXmitFreqRanges(xmitFreqRanges, &bRspError, &wRspSeqnum))
			{
				LogF("{info} Pointman: Frequency ranges set");
				PointmanEnabled = true;
			}
			else
			{
				LogF("{error} Pointman: Setting of frequency ranges failed, error %d", bRspError);
				V2_Disconnect();
				V2_FreeLib();
			}
		}
	}
	else
	{
		// No config file for this profile
		LogF("{info} Pointman: no config file, Pointman not loaded");
	}

	if (PointmanEnabled)
	{
		LogF("{info} Pointman: Pointman enabled, Pointman controls joystick and TrackIR");
		EnableExternalJoystick(true);
		EnableTrackIR(false);
		return 0;
	}
	else
	{
		// Disable external joystick and enable TrackIR for standard VBS2 control
		LogF("{info} Pointman: Pointman disabled, VBS2 controls joystick and TrackIR");
		EnableExternalJoystick(false);
		EnableTrackIR(true);
		return -1;
	}
}

void Pointman2Unload()
{
  LogF("{info} Pointman: Pointman2Unload()");
  if (PointmanEnabled)
  {
	  V2_Disconnect();
	  V2_FreeLib();
	  PointmanEnabled = false;
  }
}

void Pointman2End()
{
  LogF("{info} Pointman: Pointman2End()");
 if (GExplicitPose)
 {
   DisableEPC();

   GExplicitPose->SetActive(false);

   delete GExplicitPose;
   GExplicitPose = NULL;
 }
}

void Pointman2Start()
{
  LogF("{info} Pointman: Pointman2Start()");

  // Reload Pointman before starting to handle change of profile, also to make sure
  // sure that Pointman has control of TrackIR and external joystick when it is enabled,
  // and that VBS2 has control of TrackIR and its joystick when Pointman is not enabled. 
  Pointman2Unload();
  Pointman2Load();

  if (!PointmanEnabled) return;

  if (GExplicitPose)
  {
    delete GExplicitPose;
    GExplicitPose = NULL;
  }

  GExplicitPose = new Pointman2Pose();

  InitPlayer();

  EnableEPC();

  GExplicitPose->SetActive(true);

  if (GExplicitPose) 
  {
    GExplicitPose->GetSceleton();
  }
}

VBSPLUGIN_EXPORT void WINAPI RegisterCommandFnc(void *executeCommandFnc)
{
  extern ExecuteCommandType ExecuteCommand;
  ExecuteCommand = (ExecuteCommandType)executeCommandFnc;
}

VBSPLUGIN_EXPORT void OnExtJoyInputBtn(BYTE &value, int btn)
{
  if (GExplicitPose)
  {
   value = GExplicitPose->JoystickArbButtons(btn);
  }
}

VBSPLUGIN_EXPORT void OnExtJoyInputPov(LONG &value, int pov)
{
  if (GExplicitPose)
  {
    value = GExplicitPose->JoystickArlPovs(pov);
  }
}

VBSPLUGIN_EXPORT void OnExtJoyInputAxe(float &value, int axe)
{
  if (GExplicitPose)
  {
    value = GExplicitPose->JoystickArfAxes(axe);
  }
}

VBSPLUGIN_EXPORT void OnReload()
{
  if (GExplicitPose)
  {
    GExplicitPose->SetReload();
  }
}

VBSPLUGIN_EXPORT void OnUserControlDisplayed(int controlsDisplayed)
{
  LogF("{info} Pointman: OnUserControlDisplayed(%d)", controlsDisplayed);
  if (controlsDisplayed == 1 && !MissionStarted) 
  {
	Pointman2Start();
  }
  if (controlsDisplayed == 0) 
  {
    if (MissionStarted)
    {
      // Restart is necessary for both UDP and direct modes. 
	  // (Also because TrackIR is initialized in Options / Controls dialog and pointman need to gain controll again)
	  Pointman2End();
      Pointman2Start();
    }
    else
    {
      Pointman2End();
    }
  }
  if (GExplicitPose)
  {
    GExplicitPose->SetUserControlDisplayed(controlsDisplayed);
  }
}

VBSPLUGIN_EXPORT void WINAPI OnSimulationStep(float deltaT)
{
  if (GExplicitPose)
  {

    // Validate if we control correct player unit (needed when focus is chanaged)
    ValidatePlayer();

    // Set suspended mobility state (lazy setting, only in case the control options menu is visible)
    if (GExplicitPose->GetUserControlDisplayed() > 0)
    {
      // Set suspended motion state, to receive full input from the composite joystick
      GExplicitPose->SetMotionStateSuspended();
    }

    // Run simulation on the explicit pose
    GExplicitPose->Simulate();

    // Update the transformation
    GExplicitPose->ModifyRelativeChange();

    // Get joystick data from the Pointman
    GExplicitPose->SimulateJoystick();
  }
};


VBSPLUGIN_EXPORT void WINAPI OnMissionStart()
{
  LogF("{info} Pointman: OnMissionStart()");
  MissionStarted = true;
  Pointman2Start();
}

VBSPLUGIN_EXPORT void WINAPI OnMissionEnd()
{
  LogF("{info} Pointman: OnMissionEnd()");
  MissionStarted = false;
  Pointman2End();
}

VBSPLUGIN_EXPORT int WINAPI OnLoadEx(VBSParametersEx* params) 
{
  LogF("{info} Pointman: OnLoadEx()");
  GExplicitPose = NULL;
  if (PointmanEnabled)
  {
	  Pointman2Unload();
  }
  return Pointman2Load();
};

VBSPLUGIN_EXPORT void WINAPI OnUnload()
{
  LogF("{info} Pointman: OnUnload()");
  Pointman2Unload();
};

VBSPLUGIN_EXPORT const char* WINAPI PluginFunction(const char *input)
{
  LogF("{info} Pointman: PluginFunction()");
  if (input[0] == 's')
  {
    Pointman2Start();
  }

  if (input[0] == 'a') // fusionFunction["Pointman.dll","a=10.0.10.103"]
  {
    char res[1024];
    sscanf(input,"a=%s",res);
    PointmanIP = res;
    LogF("PointmanIP: %s",PointmanIP.data());
  }

  if (input[0] == 'd') // fusionFunction["Pointman.dll","d=c:\\Program Files (x86)\\DCS\\Gaiter_30125"]
  {
    char res[1024];
    sscanf(input,"d=%[^\0]s",res);
    PointmanPath = res;
    LogF("PointmanPath: %s",PointmanPath.data());
  } 

  if (input[0] == 'l')
  {
    Pointman2Load();
  }

  if (input[0] == 'u')
  {
    Pointman2Unload();
  }

	return "";
};

VBSPLUGIN_EXPORT const char* WINAPI GetInfo(const char* name)
{
  if (PointmanEnabled)
  {
    return "enabled";
  }
  else
  {
    return "disabled";
  }
}

BOOL WINAPI DllMain(HINSTANCE hDll, DWORD fdwReason, LPVOID lpvReserved)
{
	switch(fdwReason)
	{
	case DLL_PROCESS_ATTACH:
		break;
	case DLL_PROCESS_DETACH:
		break;
	case DLL_THREAD_ATTACH:
		break;
	case DLL_THREAD_DETACH:
		break;
	}

	return TRUE;
};