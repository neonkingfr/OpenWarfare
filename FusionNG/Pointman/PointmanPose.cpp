#include "pluginHeader.h"

#include "PointmanPose.h"
#include "Pointman.h"


void MATRIX_TO_V2_6D_PAT(V2_Interface::_V2_6D &d, const Matrix4 &mo) \
{
  Matrix4 m = mo;
  m.SwitchYZ();
#if DOUBLE_PRECISION
  double aX, aY, aZ;
#else
  float aX, aY, aZ;
#endif
  m.Orientation().GetEulerAngles(aX, aY, aZ);
  d.rx = aX * 180.0f / H_PI;
  d.ry = aY * 180.0f / H_PI;
  d.rz = aZ * 180.0f / H_PI;
  d.tx = m.GetPosition().X();
  d.ty = m.GetPosition().Y();
  d.tz = m.GetPosition().Z();
}

void ExplicitPose::SetMotionControllLower(BYTE &state, BYTE value)
{
  state = value;

  switch (value)
  {
  case V2_Interface::MOTION_CTRL_FULL:
  case V2_Interface::MOTION_CTRL_PARTIAL:
    SetExternalMovementControlled(true);
    break;
  case V2_Interface::MOTION_CTRL_NONE:
    SetExternalMovementControlled(false);
    break;
  }
}

void ExplicitPose::SetMotionControllUpper(BYTE &state, BYTE value)
{
  state = value;

  switch (value)
  {
  case V2_Interface::MOTION_CTRL_FULL:
    SetExternalCameraControlled(true);
    SetExternalPoseLowerBody(false);
    SetExternalPoseUpBody(false);
    break;
  case V2_Interface::MOTION_CTRL_PARTIAL:
    SetExternalCameraControlled(false);
    SetExternalPoseLowerBody(true);
    SetExternalPoseUpBody(true);
    break;
  case V2_Interface::MOTION_CTRL_NONE:
    SetExternalCameraControlled(false);
    SetExternalPoseLowerBody(true);
    SetExternalPoseUpBody(false);
    break;
  }
}

void ExplicitPose::ApplyMotionControllLower(BYTE value)
{
  switch (value)
  {
  case V2_Interface::MOTION_CTRL_FULL:
  case V2_Interface::MOTION_CTRL_PARTIAL:
    SetExternalMovementControlled(true);
    break;
  case V2_Interface::MOTION_CTRL_NONE:
    SetExternalMovementControlled(false);
    break;
  }
}

void ExplicitPose::ApplyMotionControllUpper(BYTE value)
{
  switch (value)
  {
  case V2_Interface::MOTION_CTRL_FULL:
    SetExternalCameraControlled(true);
    SetExternalPoseLowerBody(false);
    SetExternalPoseUpBody(false);
    break;
  case V2_Interface::MOTION_CTRL_PARTIAL:
    SetExternalCameraControlled(false);
    SetExternalPoseLowerBody(true);
    SetExternalPoseUpBody(true);
    break;
  case V2_Interface::MOTION_CTRL_NONE:
    SetExternalCameraControlled(false);
    SetExternalPoseLowerBody(true);
    SetExternalPoseUpBody(false);
    break;
  }
}

void ExplicitPose::DefineSkeleton(float height)
{
  //VBS skeleton bones IDs
  int  _vid_pelvis = -1;	
  int  _vid_camera = -1;	
  int  _vid_spine = -1;	
  int  _vid_spine1 = -1;	
  int  _vid_weapon = -1;	
  int  _vid_launcher = -1;	
  int  _vid_spine2 = -1;	
  int  _vid_spine3 = -1;	
  int  _vid_neck = -1;	
  int  _vid_neck1 = -1;	
  int  _vid_head = -1;	
  int  _vid_lbrow = -1;	
  int  _vid_mbrow = -1;	
  int  _vid_rbrow = -1;	
  int  _vid_lmouth = -1;	
  int  _vid_mmouth = -1;	
  int  _vid_rmouth = -1;	
  int  _vid_eyelids = -1;	
  int  _vid_llip = -1;	
  int  _vid_l_eye = -1;	
  int  _vid_r_eye = -1;	
  int  _vid_l_pupila = -1;	
  int  _vid_r_pupila = -1;	
  int  _vid_cheek_lf = -1;	
  int  _vid_nose_tip = -1;	
  int  _vid_lip_uplb = -1;	
  int  _vid_jaw_ls = -1;	
  int  _vid_lip_uplf = -1;	
  int  _vid_lip_lc = -1;	
  int  _vid_lip_lwlb = -1;	
  int  _vid_lip_lwlf = -1;	
  int  _vid_jaw_lm = -1;	
  int  _vid_zig_lb = -1;	
  int  _vid_lip_lwm = -1;	
  int  _vid_lip_upm = -1;	
  int  _vid_ear_l = -1;	
  int  _vid_corr = -1;	
  int  _vid_tongue_m = -1;	
  int  _vid_tongue_f = -1;	
  int  _vid_eyebrow_lb = -1;	
  int  _vid_eyebrow_lf = -1;	
  int  _vid_eyebrow_lm = -1;	
  int  _vid_zig_lm = -1;	
  int  _vid_eye_upl = -1;	
  int  _vid_eye_lwl = -1;	
  int  _vid_cheek_l = -1;	
  int  _vid_cheek_lb = -1;	
  int  _vid_zig_lt = -1;	
  int  _vid_nose_l = -1;	
  int  _vid_cheek_lm = -1;	
  int  _vid_nose_r = -1;	
  int  _vid_forehead_r = -1;	
  int  _vid_forehead_m = -1;	
  int  _vid_forehead_l = -1;	
  int  _vid_cheek_rb = -1;	
  int  _vid_eye_lwr = -1;	
  int  _vid_cheek_r = -1;	
  int  _vid_zig_rt = -1;	
  int  _vid_zig_rm = -1;	
  int  _vid_cheek_rf = -1;	
  int  _vid_cheek_rm = -1;	
  int  _vid_eyebrow_rm = -1;	
  int  _vid_eyebrow_rf = -1;	
  int  _vid_eye_upr = -1;	
  int  _vid_eyebrow_rb = -1;	
  int  _vid_tongue_b = -1;	
  int  _vid_ear_r = -1;	
  int  _vid_neck_l = -1;	
  int  _vid_lip_uprf = -1;	
  int  _vid_neck_r = -1;	
  int  _vid_lip_uprb = -1;	
  int  _vid_lip_rc = -1;	
  int  _vid_lip_lwrb = -1;	
  int  _vid_lip_lwrf = -1;	
  int  _vid_neck_b = -1;	
  int  _vid_zig_rb = -1;	
  int  _vid_neck_t = -1;	
  int  _vid_jaw_rf = -1;	
  int  _vid_jaw_lf = -1;	
  int  _vid_chin = -1;	
  int  _vid_jaw_rm = -1;	
  int  _vid_jaw_rs = -1;	
  int  _vid_jaw = -1;	
  int  _vid_headcutscene = -1;	
  int  _vid_leftshoulder = -1;	
  int  _vid_leftarm = -1;	
  int  _vid_leftarmroll = -1;	
  int  _vid_leftforearm = -1;	
  int  _vid_leftforearmroll = -1;	
  int  _vid_lefthand = -1;	
  int  _vid_lefthandring = -1;	
  int  _vid_lefthandring1 = -1;	
  int  _vid_lefthandring2 = -1;	
  int  _vid_lefthandring3 = -1;	
  int  _vid_lefthandpinky1 = -1;	
  int  _vid_lefthandpinky2 = -1;	
  int  _vid_lefthandpinky3 = -1;	
  int  _vid_lefthandmiddle1 = -1;	
  int  _vid_lefthandmiddle2 = -1;	
  int  _vid_lefthandmiddle3 = -1;	
  int  _vid_lefthandindex1 = -1;	
  int  _vid_lefthandindex2 = -1;	
  int  _vid_lefthandindex3 = -1;	
  int  _vid_lefthandthumb1 = -1;	
  int  _vid_lefthandthumb2 = -1;	
  int  _vid_lefthandthumb3 = -1;	
  int  _vid_rightshoulder = -1;	
  int  _vid_rightarm = -1;	
  int  _vid_rightarmroll = -1;	
  int  _vid_rightforearm = -1;	
  int  _vid_rightforearmroll = -1;	
  int  _vid_righthand = -1;	
  int  _vid_righthandring = -1;	
  int  _vid_righthandring1 = -1;	
  int  _vid_righthandring2 = -1;	
  int  _vid_righthandring3 = -1;	
  int  _vid_righthandpinky1 = -1;	
  int  _vid_righthandpinky2 = -1;	
  int  _vid_righthandpinky3 = -1;	
  int  _vid_righthandmiddle1 = -1;	
  int  _vid_righthandmiddle2 = -1;	
  int  _vid_righthandmiddle3 = -1;	
  int  _vid_righthandindex1 = -1;	
  int  _vid_righthandindex2 = -1;	
  int  _vid_righthandindex3 = -1;	
  int  _vid_righthandthumb1 = -1;	
  int  _vid_righthandthumb2 = -1;	
  int  _vid_righthandthumb3 = -1;	
  int  _vid_leftupleg = -1;	
  int  _vid_leftuplegroll = -1;	
  int  _vid_leftleg = -1;	
  int  _vid_leftlegroll = -1;	
  int  _vid_leftfoot = -1;	
  int  _vid_lefttoebase = -1;	
  int  _vid_rightupleg = -1;	
  int  _vid_rightuplegroll = -1;	
  int  _vid_rightleg = -1;	
  int  _vid_rightlegroll = -1;	
  int  _vid_rightfoot = -1;	
  int  _vid_righttoebase = -1;	
  int  _vid_slot_backpack = -1;	
  int  _vid_slot_backwpnl = -1;	
  int  _vid_slot_backwpnr = -1;	
  int  _vid_slot_buttpack = -1;	
  int  _vid_slot_patrolwpn = -1;	

  // Pelvis
  _b_pelvis = MIdentity;
  _b_pelvis.SetPosition(Vector3(0.0f, 1.014f, 0.0f));

  // Offsets (parent relative)
  Vector3 offsetLUpperLeg(-0.114f, -0.060f, +0.000f); // Lower body
  Vector3 offsetLLowerLeg(-0.026f, -0.407f, +0.019f);
  Vector3 offsetLFoot    (-0.030f, -0.422f, -0.102f);
  Vector3 offsetLToes    (-0.004f, -0.108f, +0.113f);
  Vector3 offsetLToesEnd (-0.000f, -0.017f, +0.125f);
  Vector3 offsetRUpperLeg(+0.114f, -0.060f, +0.000f);
  Vector3 offsetRLowerLeg(+0.026f, -0.407f, +0.019f);
  Vector3 offsetRFoot    (+0.030f, -0.422f, -0.102f);
  Vector3 offsetRToes    (+0.004f, -0.108f, +0.113f);
  Vector3 offsetRToesEnd (+0.000f, -0.017f, +0.125f);
  Vector3 offsetSpine1   (+0.000f, +0.000f, +0.000f); // Upper body - spin
  Vector3 offsetSpine2   (+0.002f, +0.156f, -0.012f);
  Vector3 offsetSpine3   (-0.002f, +0.130f, -0.015f);
  Vector3 offsetSpine4   (+0.000f, +0.152f, -0.006f);
  Vector3 offsetNeck1    (+0.000f, +0.117f, +0.008f);
  Vector3 offsetNeck2    (+0.000f, +0.071f, +0.017f);
  Vector3 offsetSkull    (+0.000f, +0.041f, +0.002f);
  Vector3 offsetSkullEnd (+0.000f, +0.147f, +0.001f);
  Vector3 offsetLeftEye  (-0.040f, +0.020f, +0.050f);
  Vector3 offsetRightEye (+0.040f, +0.020f, +0.050f);
  Vector3 offsetLClavicle  (-0.064f, +0.062f, +0.028f); // Upper body - left arm
  Vector3 offsetLUpperArm  (-0.139f, -0.023f, -0.045f);
  Vector3 offsetLLowerArm  (-0.175f, -0.194f, -0.019f);
  Vector3 offsetLHand      (-0.209f, -0.217f, -0.005f);
  Vector3 offsetLThumb1    (-0.003f, -0.004f, +0.027f);
  Vector3 offsetLThumb2    (-0.023f, -0.025f, +0.039f);
  Vector3 offsetLThumb3    (-0.020f, -0.023f, +0.017f);
  Vector3 offsetLThumbEnd  (-0.013f, -0.015f, +0.016f);
  Vector3 offsetLIndex1    (-0.064f, -0.061f, +0.036f);
  Vector3 offsetLIndex2    (-0.028f, -0.027f, +0.010f);
  Vector3 offsetLIndex3    (-0.016f, -0.016f, +0.006f);
  Vector3 offsetLIndexEnd  (-0.015f, -0.015f, +0.005f);
  Vector3 offsetLMiddle1   (-0.068f, -0.066f, +0.013f);
  Vector3 offsetLMiddle2   (-0.030f, -0.031f, +0.002f);
  Vector3 offsetLMiddle3   (-0.018f, -0.019f, +0.001f);
  Vector3 offsetLMiddleEnd (-0.016f, -0.016f, +0.001f);
  Vector3 offsetLRing1     (-0.064f, -0.065f, -0.008f);
  Vector3 offsetLRing2     (-0.025f, -0.024f, -0.008f);
  Vector3 offsetLRing3     (-0.017f, -0.017f, -0.005f);
  Vector3 offsetLRingEnd   (-0.015f, -0.014f, -0.004f);
  Vector3 offsetLPinky1    (-0.054f, -0.062f, -0.032f);
  Vector3 offsetLPinky2    (-0.019f, -0.018f, -0.006f);
  Vector3 offsetLPinky3    (-0.015f, -0.013f, -0.005f);
  Vector3 offsetLPinkyEnd  (-0.013f, -0.012f, -0.004f);
  Vector3 offsetRClavicle  (+0.064f, +0.062f, +0.028f); // Upper body - right arm
  Vector3 offsetRUpperArm  (+0.139f, -0.023f, -0.045f);
  Vector3 offsetRLowerArm  (+0.175f, -0.194f, -0.019f);
  Vector3 offsetRHand      (+0.209f, -0.217f, -0.005f);
  Vector3 offsetRThumb1    (+0.003f, -0.004f, +0.027f);
  Vector3 offsetRThumb2    (+0.023f, -0.025f, +0.039f);
  Vector3 offsetRThumb3    (+0.020f, -0.023f, +0.017f);
  Vector3 offsetRThumbEnd  (+0.013f, -0.015f, +0.016f);
  Vector3 offsetRIndex1    (+0.064f, -0.061f, +0.036f);
  Vector3 offsetRIndex2    (+0.028f, -0.027f, +0.010f);
  Vector3 offsetRIndex3    (+0.016f, -0.016f, +0.006f);
  Vector3 offsetRIndexEnd  (+0.015f, -0.015f, +0.005f);
  Vector3 offsetRMiddle1   (+0.068f, -0.066f, +0.013f);
  Vector3 offsetRMiddle2   (+0.030f, -0.031f, +0.002f);
  Vector3 offsetRMiddle3   (+0.018f, -0.019f, +0.001f);
  Vector3 offsetRMiddleEnd (+0.016f, -0.016f, +0.001f);
  Vector3 offsetRRing1     (+0.064f, -0.065f, -0.008f);
  Vector3 offsetRRing2     (+0.025f, -0.024f, -0.008f);
  Vector3 offsetRRing3     (+0.017f, -0.017f, -0.005f);
  Vector3 offsetRRingEnd   (+0.015f, -0.014f, -0.004f);
  Vector3 offsetRPinky1    (+0.054f, -0.062f, -0.032f);
  Vector3 offsetRPinky2    (+0.019f, -0.018f, -0.006f);
  Vector3 offsetRPinky3    (+0.015f, -0.013f, -0.005f);
  Vector3 offsetRPinkyEnd  (+0.013f, -0.012f, -0.004f);

  // Absolute positions
  Vector3 poseLUpperLeg     = _b_pelvis.GetPosition()  + offsetLUpperLeg; // Lower body
  Vector3 poseLLowerLeg     = poseLUpperLeg         + offsetLLowerLeg;
  Vector3 poseLFoot         = poseLLowerLeg         + offsetLFoot;
  Vector3 poseLToes         = poseLFoot             + offsetLToes;
  Vector3 poseLToesEnd      = poseLToes             + offsetLToesEnd;
  Vector3 poseRUpperLeg     = _b_pelvis.GetPosition()  + offsetRUpperLeg;
  Vector3 poseRLowerLeg     = poseRUpperLeg         + offsetRLowerLeg;
  Vector3 poseRFoot         = poseRLowerLeg         + offsetRFoot;
  Vector3 poseRToes         = poseRFoot             + offsetRToes;
  Vector3 poseRToesEnd      = poseRToes             + offsetRToesEnd;
  Vector3 poseSpine1        = _b_pelvis.GetPosition()  + offsetSpine1; // Upper body - spin
  Vector3 poseSpine2        = poseSpine1            + offsetSpine2;
  Vector3 poseSpine3        = poseSpine2            + offsetSpine3;
  Vector3 poseSpine4        = poseSpine3            + offsetSpine4;
  Vector3 poseNeck1         = poseSpine4            + offsetNeck1;
  Vector3 poseNeck2         = poseNeck1             + offsetNeck2;
  Vector3 poseSkull         = poseNeck2             + offsetSkull;
  Vector3 poseSkullEnd      = poseSkull             + offsetSkullEnd;
  Vector3 poseLeftEye       = poseSkull             + offsetLeftEye;
  Vector3 poseRightEye      = poseSkull             + offsetRightEye;
  Vector3 poseLClavicle     = poseSpine4            + offsetLClavicle; // Upper body - left arm
  Vector3 poseLUpperArm     = poseLClavicle         + offsetLUpperArm;
  Vector3 poseLLowerArm     = poseLUpperArm         + offsetLLowerArm;
  Vector3 poseLHand         = poseLLowerArm         + offsetLHand;
  Vector3 poseLThumb1       = poseLHand             + offsetLThumb1;
  Vector3 poseLThumb2       = poseLThumb1           + offsetLThumb2;
  Vector3 poseLThumb3       = poseLThumb2           + offsetLThumb3;
  Vector3 poseLThumbEnd     = poseLThumb3           + offsetLThumbEnd;
  Vector3 poseLIndex1       = poseLHand             + offsetLIndex1;
  Vector3 poseLIndex2       = poseLIndex1           + offsetLIndex2;
  Vector3 poseLIndex3       = poseLIndex2           + offsetLIndex3;
  Vector3 poseLIndexEnd     = poseLIndex3           + offsetLIndexEnd;
  Vector3 poseLMiddle1      = poseLHand             + offsetLMiddle1;
  Vector3 poseLMiddle2      = poseLMiddle1          + offsetLMiddle2;
  Vector3 poseLMiddle3      = poseLMiddle2          + offsetLMiddle3;
  Vector3 poseLMiddleEnd    = poseLMiddle3          + offsetLMiddleEnd;
  Vector3 poseLRing1        = poseLHand             + offsetLRing1;
  Vector3 poseLRing2        = poseLRing1            + offsetLRing2;
  Vector3 poseLRing3        = poseLRing2            + offsetLRing3;
  Vector3 poseLRingEnd      = poseLRing3            + offsetLRingEnd;
  Vector3 poseLPinky1       = poseLHand             + offsetLPinky1;
  Vector3 poseLPinky2       = poseLPinky1           + offsetLPinky2;
  Vector3 poseLPinky3       = poseLPinky2           + offsetLPinky3;
  Vector3 poseLPinkyEnd     = poseLPinky3           + offsetLPinkyEnd;
  Vector3 poseRClavicle     = poseSpine4            + offsetRClavicle; // Upper body - right arm
  Vector3 poseRUpperArm     = poseRClavicle         + offsetRUpperArm;
  Vector3 poseRLowerArm     = poseRUpperArm         + offsetRLowerArm;
  Vector3 poseRHand         = poseRLowerArm         + offsetRHand;
  Vector3 poseRThumb1       = poseRHand             + offsetRThumb1;
  Vector3 poseRThumb2       = poseRThumb1           + offsetRThumb2;
  Vector3 poseRThumb3       = poseRThumb2           + offsetRThumb3;
  Vector3 poseRThumbEnd     = poseRThumb3           + offsetRThumbEnd;
  Vector3 poseRIndex1       = poseRHand             + offsetRIndex1;
  Vector3 poseRIndex2       = poseRIndex1           + offsetRIndex2;
  Vector3 poseRIndex3       = poseRIndex2           + offsetRIndex3;
  Vector3 poseRIndexEnd     = poseRIndex3           + offsetRIndexEnd;
  Vector3 poseRMiddle1      = poseRHand             + offsetRMiddle1;
  Vector3 poseRMiddle2      = poseRMiddle1          + offsetRMiddle2;
  Vector3 poseRMiddle3      = poseRMiddle2          + offsetRMiddle3;
  Vector3 poseRMiddleEnd    = poseRMiddle3          + offsetRMiddleEnd;
  Vector3 poseRRing1        = poseRHand             + offsetRRing1;
  Vector3 poseRRing2        = poseRRing1            + offsetRRing2;
  Vector3 poseRRing3        = poseRRing2            + offsetRRing3;
  Vector3 poseRRingEnd      = poseRRing3            + offsetRRingEnd;
  Vector3 poseRPinky1       = poseRHand             + offsetRPinky1;
  Vector3 poseRPinky2       = poseRPinky1           + offsetRPinky2;
  Vector3 poseRPinky3       = poseRPinky2           + offsetRPinky3;
  Vector3 poseRPinkyEnd     = poseRPinky3           + offsetRPinkyEnd;

  // Skeleton bones
  _b_lHip.SetPosition(        poseLUpperLeg); _b_lHip.SetDirectionAndUp(        (poseLLowerLeg - poseLUpperLeg),  -VForward); // Lower body
  _b_lKnee.SetPosition(       poseLLowerLeg); _b_lKnee.SetDirectionAndUp(       (poseLFoot -     poseLLowerLeg),  -VForward);
  _b_lAnkle.SetPosition(      poseLFoot);     _b_lAnkle.SetDirectionAndUp(      (poseLToes -     poseLFoot),      -VUp);
  _b_lMetatarsal.SetPosition( poseLToes);     _b_lMetatarsal.SetDirectionAndUp( (poseLToesEnd -  poseLToes),      -VUp);
  _b_rHip.SetPosition(        poseRUpperLeg); _b_rHip.SetDirectionAndUp(        (poseRLowerLeg - poseRUpperLeg),  -VForward);
  _b_rKnee.SetPosition(       poseRLowerLeg); _b_rKnee.SetDirectionAndUp(       (poseRFoot -     poseRLowerLeg),  -VForward);
  _b_rAnkle.SetPosition(      poseRFoot);     _b_rAnkle.SetDirectionAndUp(      (poseRToes -     poseRFoot),      -VUp);
  _b_rMetatarsal.SetPosition( poseRToes);     _b_rMetatarsal.SetDirectionAndUp( (poseRToesEnd -  poseRToes),      -VUp);
  _b_spine1.SetPosition(      poseSpine1);    _b_spine1.SetDirectionAndUp(      (poseSpine2    - poseSpine1),     VForward); // Upper body - spin
  _b_spine2.SetPosition(      poseSpine2);    _b_spine2.SetDirectionAndUp(      (poseSpine3    - poseSpine2),     VForward);
  _b_spine3.SetPosition(      poseSpine3);    _b_spine3.SetDirectionAndUp(      (poseSpine4    - poseSpine3),     VForward);
  _b_spine4.SetPosition(      poseSpine4);    _b_spine4.SetDirectionAndUp(      (poseNeck1     - poseSpine4),     VForward);
  _b_neck1.SetPosition(       poseNeck1);     _b_neck1.SetDirectionAndUp(       (poseNeck2     - poseNeck1),      VForward);
  _b_neck2.SetPosition(       poseNeck2);     _b_neck2.SetDirectionAndUp(       (poseSkull     - poseNeck2),      VForward);
  _b_skull.SetPosition(       poseSkull);     _b_skull.SetDirectionAndUp(       (poseSkullEnd  - poseSkull),      VForward);
  _b_lEye.SetPosition(        poseLeftEye);   _b_lEye.SetDirectionAndUp(        (poseSkullEnd  - poseSkull),      VForward);
  _b_rEye.SetPosition(        poseRightEye);  _b_rEye.SetDirectionAndUp(        (poseSkullEnd  - poseSkull),      VForward);
  _b_lClavicle.SetPosition(   poseLClavicle); _b_lClavicle.SetDirectionAndUp(   (poseLUpperArm  - poseLClavicle),  -VUp); // Upper body - left arm
  _b_lUpperArm.SetPosition(   poseLUpperArm); _b_lUpperArm.SetDirectionAndUp(   (poseLLowerArm  - poseLUpperArm),  -VUp);
  _b_lLowerArm.SetPosition(   poseLLowerArm); _b_lLowerArm.SetDirectionAndUp(   (poseLHand      - poseLLowerArm),  -VUp);
  _b_lHand.SetPosition(       poseLHand);     _b_lHand.SetDirectionAndUp(       (poseLMiddle1   - poseLHand),      -VUp);
  _b_lThumbSeg1.SetPosition(  poseLThumb1);   _b_lThumbSeg1.SetDirectionAndUp(  (poseLThumb2    - poseLThumb1),    -VUp);
  _b_lThumbSeg2.SetPosition(  poseLThumb2);   _b_lThumbSeg2.SetDirectionAndUp(  (poseLThumb3    - poseLThumb2),    -VUp);
  _b_lThumbSeg3.SetPosition(  poseLThumb3);   _b_lThumbSeg3.SetDirectionAndUp(  (poseLThumbEnd  - poseLThumb3),    -VUp);
  _b_lIndexSeg1.SetPosition(  poseLIndex1);   _b_lIndexSeg1.SetDirectionAndUp(  (poseLIndex2    - poseLIndex1),    -VUp);
  _b_lIndexSeg2.SetPosition(  poseLIndex2);   _b_lIndexSeg2.SetDirectionAndUp(  (poseLIndex3    - poseLIndex2),    -VUp);
  _b_lIndexSeg3.SetPosition(  poseLIndex3);   _b_lIndexSeg3.SetDirectionAndUp(  (poseLIndexEnd  - poseLIndex3),    -VUp);
  _b_lMiddleSeg1.SetPosition( poseLMiddle1);  _b_lMiddleSeg1.SetDirectionAndUp( (poseLMiddle2   - poseLMiddle1),   -VUp);
  _b_lMiddleSeg2.SetPosition( poseLMiddle2);  _b_lMiddleSeg2.SetDirectionAndUp( (poseLMiddle3   - poseLMiddle2),   -VUp);
  _b_lMiddleSeg3.SetPosition( poseLMiddle3);  _b_lMiddleSeg3.SetDirectionAndUp( (poseLMiddleEnd - poseLMiddle3),   -VUp);
  _b_lRingSeg1.SetPosition(   poseLRing1);    _b_lRingSeg1.SetDirectionAndUp(   (poseLRing2     - poseLRing1),     -VUp);
  _b_lRingSeg2.SetPosition(   poseLRing2);    _b_lRingSeg2.SetDirectionAndUp(   (poseLRing3     - poseLRing2),     -VUp);
  _b_lRingSeg3.SetPosition(   poseLRing3);    _b_lRingSeg3.SetDirectionAndUp(   (poseLRingEnd   - poseLRing3),     -VUp);
  _b_lPinkySeg1.SetPosition(  poseLPinky1);   _b_lPinkySeg1.SetDirectionAndUp(  (poseLPinky2    - poseLPinky1),    -VUp);
  _b_lPinkySeg2.SetPosition(  poseLPinky2);   _b_lPinkySeg2.SetDirectionAndUp(  (poseLPinky3    - poseLPinky2),    -VUp);
  _b_lPinkySeg3.SetPosition(  poseLPinky3);   _b_lPinkySeg3.SetDirectionAndUp(  (poseLPinkyEnd  - poseLPinky3),    -VUp);
  _b_rClavicle.SetPosition(   poseRClavicle); _b_rClavicle.SetDirectionAndUp(   (poseRUpperArm  - poseRClavicle),  -VUp); // Upper body - right arm
  _b_rUpperArm.SetPosition(   poseRUpperArm); _b_rUpperArm.SetDirectionAndUp(   (poseRLowerArm  - poseRUpperArm),  -VUp);
  _b_rLowerArm.SetPosition(   poseRLowerArm); _b_rLowerArm.SetDirectionAndUp(   (poseRHand      - poseRLowerArm),  -VUp);
  _b_rHand.SetPosition(       poseRHand);     _b_rHand.SetDirectionAndUp(       (poseRMiddle1   - poseRHand),      -VUp);
  _b_rThumbSeg1.SetPosition(  poseRThumb1);   _b_rThumbSeg1.SetDirectionAndUp(  (poseRThumb2    - poseRThumb1),    -VUp);
  _b_rThumbSeg2.SetPosition(  poseRThumb2);   _b_rThumbSeg2.SetDirectionAndUp(  (poseRThumb3    - poseRThumb2),    -VUp);
  _b_rThumbSeg3.SetPosition(  poseRThumb3);   _b_rThumbSeg3.SetDirectionAndUp(  (poseRThumbEnd  - poseRThumb3),    -VUp);
  _b_rIndexSeg1.SetPosition(  poseRIndex1);   _b_rIndexSeg1.SetDirectionAndUp(  (poseRIndex2    - poseRIndex1),    -VUp);
  _b_rIndexSeg2.SetPosition(  poseRIndex2);   _b_rIndexSeg2.SetDirectionAndUp(  (poseRIndex3    - poseRIndex2),    -VUp);
  _b_rIndexSeg3.SetPosition(  poseRIndex3);   _b_rIndexSeg3.SetDirectionAndUp(  (poseRIndexEnd  - poseRIndex3),    -VUp);
  _b_rMiddleSeg1.SetPosition( poseRMiddle1);  _b_rMiddleSeg1.SetDirectionAndUp( (poseRMiddle2   - poseRMiddle1),   -VUp);
  _b_rMiddleSeg2.SetPosition( poseRMiddle2);  _b_rMiddleSeg2.SetDirectionAndUp( (poseRMiddle3   - poseRMiddle2),   -VUp);
  _b_rMiddleSeg3.SetPosition( poseRMiddle3);  _b_rMiddleSeg3.SetDirectionAndUp( (poseRMiddleEnd - poseRMiddle3),   -VUp);
  _b_rRingSeg1.SetPosition(   poseRRing1);    _b_rRingSeg1.SetDirectionAndUp(   (poseRRing2     - poseRRing1),     -VUp);
  _b_rRingSeg2.SetPosition(   poseRRing2);    _b_rRingSeg2.SetDirectionAndUp(   (poseRRing3     - poseRRing2),     -VUp);
  _b_rRingSeg3.SetPosition(   poseRRing3);    _b_rRingSeg3.SetDirectionAndUp(   (poseRRingEnd   - poseRRing3),     -VUp);
  _b_rPinkySeg1.SetPosition(  poseRPinky1);   _b_rPinkySeg1.SetDirectionAndUp(  (poseRPinky2    - poseRPinky1),    -VUp);
  _b_rPinkySeg2.SetPosition(  poseRPinky2);   _b_rPinkySeg2.SetDirectionAndUp(  (poseRPinky3    - poseRPinky2),    -VUp);
  _b_rPinkySeg3.SetPosition(  poseRPinky3);   _b_rPinkySeg3.SetDirectionAndUp(  (poseRPinkyEnd  - poseRPinky3),    -VUp);

  // Size of end bones
  offsetLToesEndSize    = offsetLToesEnd.Size();
  offsetRToesEndSize    = offsetRToesEnd.Size();
  offsetSkullEndSize    = offsetSkullEnd.Size();
  offsetLThumbEndSize   = offsetLThumbEnd.Size();
  offsetLIndexEndSize   = offsetLIndexEnd.Size();
  offsetLMiddleEndSize  = offsetLMiddleEnd.Size();
  offsetLRingEndSize    = offsetLRingEnd.Size();
  offsetLPinkyEndSize   = offsetLPinkyEnd.Size();
  offsetRThumbEndSize   = offsetRThumbEnd.Size();
  offsetRIndexEndSize   = offsetRIndexEnd.Size();
  offsetRMiddleEndSize  = offsetRMiddleEnd.Size();
  offsetRRingEndSize    = offsetRRingEnd.Size();
  offsetRPinkyEndSize   = offsetRPinkyEnd.Size();
}

float PointmanPose::GetPelvisHeight() const
{
  return _pelvisPosition.Y();
}

Pointman2Pose::Pointman2Pose() : PointmanPose()
{
  // Set identity to the last pose
  _lastPose = MIdentity;

  _impulseAngX = 0;
  _impulseAngY = 0;
  _impulseZ = 0;
  _lastWeaponName[0] = '\0';

  // Define the skeleton
  {
    // Define the _b_* matrices
    DefineSkeleton(1.014f);

    // Define hand grips (hand position and pose) associated with a held object
    DefineHandGrips();

    // Define stow positions (for primary and secondary weapons)
    DefineStowPos();

    // Pelvis bone
    MATRIX_TO_V2_6D_PAT(_sd.posePelvis, _b_pelvis);

    // Convert to skeleton definition
    MATRIX_TO_V2_6D_PAT(_sd.poseLowerBody.poseLeftLeg.poseUpperLeg,                 _b_lHip); // Lower body
    MATRIX_TO_V2_6D_PAT(_sd.poseLowerBody.poseLeftLeg.poseLowerLeg,                 _b_lKnee);
    MATRIX_TO_V2_6D_PAT(_sd.poseLowerBody.poseLeftLeg.poseFoot,                     _b_lAnkle);
    MATRIX_TO_V2_6D_PAT(_sd.poseLowerBody.poseLeftLeg.poseToes,                     _b_lMetatarsal);
    MATRIX_TO_V2_6D_PAT(_sd.poseLowerBody.poseRightLeg.poseUpperLeg,                _b_rHip);
    MATRIX_TO_V2_6D_PAT(_sd.poseLowerBody.poseRightLeg.poseLowerLeg,                _b_rKnee);
    MATRIX_TO_V2_6D_PAT(_sd.poseLowerBody.poseRightLeg.poseFoot,                    _b_rAnkle);
    MATRIX_TO_V2_6D_PAT(_sd.poseLowerBody.poseRightLeg.poseToes,                    _b_rMetatarsal);
    MATRIX_TO_V2_6D_PAT(_sd.poseUpperBody.poseTorso.poseSpine1,                     _b_spine1); // Upper body - spin
    MATRIX_TO_V2_6D_PAT(_sd.poseUpperBody.poseTorso.poseSpine2,                     _b_spine2);
    MATRIX_TO_V2_6D_PAT(_sd.poseUpperBody.poseTorso.poseSpine3,                     _b_spine3);
    MATRIX_TO_V2_6D_PAT(_sd.poseUpperBody.poseTorso.poseSpine4,                     _b_spine4);
    MATRIX_TO_V2_6D_PAT(_sd.poseUpperBody.poseHead.poseNeck1,                       _b_neck1);
    MATRIX_TO_V2_6D_PAT(_sd.poseUpperBody.poseHead.poseNeck2,                       _b_neck2);
    MATRIX_TO_V2_6D_PAT(_sd.poseUpperBody.poseHead.poseSkull,                       _b_skull);
    MATRIX_TO_V2_6D_PAT(_sd.poseUpperBody.poseHead.poseLeftEye,                     _b_lEye);
    MATRIX_TO_V2_6D_PAT(_sd.poseUpperBody.poseHead.poseRightEye,                    _b_rEye);



    MATRIX_TO_V2_6D_PAT(_sd.poseUpperBody.poseLeftArm.poseClavicle,                 _b_lClavicle); // Upper body - left arm
    MATRIX_TO_V2_6D_PAT(_sd.poseUpperBody.poseLeftArm.poseUpperArm,                 _b_lUpperArm);
    MATRIX_TO_V2_6D_PAT(_sd.poseUpperBody.poseLeftArm.poseLowerArm,                 _b_lLowerArm);
    MATRIX_TO_V2_6D_PAT(_sd.poseUpperBody.poseLeftArm.poseHand,                     _b_lHand);
    MATRIX_TO_V2_6D_PAT(_sd.poseUpperBody.poseLeftArm.poseThumb.poseBone1,          _b_lThumbSeg1);
    MATRIX_TO_V2_6D_PAT(_sd.poseUpperBody.poseLeftArm.poseThumb.poseBone2,          _b_lThumbSeg2);
    MATRIX_TO_V2_6D_PAT(_sd.poseUpperBody.poseLeftArm.poseThumb.poseBone3,          _b_lThumbSeg3);
    MATRIX_TO_V2_6D_PAT(_sd.poseUpperBody.poseLeftArm.poseIndexFinger.poseBone1,    _b_lIndexSeg1);
    MATRIX_TO_V2_6D_PAT(_sd.poseUpperBody.poseLeftArm.poseIndexFinger.poseBone2,    _b_lIndexSeg2);
    MATRIX_TO_V2_6D_PAT(_sd.poseUpperBody.poseLeftArm.poseIndexFinger.poseBone3,    _b_lIndexSeg3);
    MATRIX_TO_V2_6D_PAT(_sd.poseUpperBody.poseLeftArm.poseMiddleFinger.poseBone1,   _b_lMiddleSeg1);
    MATRIX_TO_V2_6D_PAT(_sd.poseUpperBody.poseLeftArm.poseMiddleFinger.poseBone2,   _b_lMiddleSeg2);
    MATRIX_TO_V2_6D_PAT(_sd.poseUpperBody.poseLeftArm.poseMiddleFinger.poseBone3,   _b_lMiddleSeg3);
    MATRIX_TO_V2_6D_PAT(_sd.poseUpperBody.poseLeftArm.poseRingFinger.poseBone1,     _b_lRingSeg1);
    MATRIX_TO_V2_6D_PAT(_sd.poseUpperBody.poseLeftArm.poseRingFinger.poseBone2,     _b_lRingSeg2);
    MATRIX_TO_V2_6D_PAT(_sd.poseUpperBody.poseLeftArm.poseRingFinger.poseBone3,     _b_lRingSeg3);
    MATRIX_TO_V2_6D_PAT(_sd.poseUpperBody.poseLeftArm.posePinkyFinger.poseBone1,    _b_lPinkySeg1);
    MATRIX_TO_V2_6D_PAT(_sd.poseUpperBody.poseLeftArm.posePinkyFinger.poseBone2,    _b_lPinkySeg2);
    MATRIX_TO_V2_6D_PAT(_sd.poseUpperBody.poseLeftArm.posePinkyFinger.poseBone3,    _b_lPinkySeg3);
    MATRIX_TO_V2_6D_PAT(_sd.poseUpperBody.poseRightArm.poseClavicle,                _b_rClavicle); // Upper body - right arm
    MATRIX_TO_V2_6D_PAT(_sd.poseUpperBody.poseRightArm.poseUpperArm,                _b_rUpperArm);
    MATRIX_TO_V2_6D_PAT(_sd.poseUpperBody.poseRightArm.poseLowerArm,                _b_rLowerArm);
    MATRIX_TO_V2_6D_PAT(_sd.poseUpperBody.poseRightArm.poseHand,                    _b_rHand);
    MATRIX_TO_V2_6D_PAT(_sd.poseUpperBody.poseRightArm.poseThumb.poseBone1,         _b_rThumbSeg1);
    MATRIX_TO_V2_6D_PAT(_sd.poseUpperBody.poseRightArm.poseThumb.poseBone2,         _b_rThumbSeg2);
    MATRIX_TO_V2_6D_PAT(_sd.poseUpperBody.poseRightArm.poseThumb.poseBone3,         _b_rThumbSeg3);
    MATRIX_TO_V2_6D_PAT(_sd.poseUpperBody.poseRightArm.poseIndexFinger.poseBone1,   _b_rIndexSeg1);
    MATRIX_TO_V2_6D_PAT(_sd.poseUpperBody.poseRightArm.poseIndexFinger.poseBone2,   _b_rIndexSeg2);
    MATRIX_TO_V2_6D_PAT(_sd.poseUpperBody.poseRightArm.poseIndexFinger.poseBone3,   _b_rIndexSeg3);
    MATRIX_TO_V2_6D_PAT(_sd.poseUpperBody.poseRightArm.poseMiddleFinger.poseBone1,  _b_rMiddleSeg1);
    MATRIX_TO_V2_6D_PAT(_sd.poseUpperBody.poseRightArm.poseMiddleFinger.poseBone2,  _b_rMiddleSeg2);
    MATRIX_TO_V2_6D_PAT(_sd.poseUpperBody.poseRightArm.poseMiddleFinger.poseBone3,  _b_rMiddleSeg3);
    MATRIX_TO_V2_6D_PAT(_sd.poseUpperBody.poseRightArm.poseRingFinger.poseBone1,    _b_rRingSeg1);
    MATRIX_TO_V2_6D_PAT(_sd.poseUpperBody.poseRightArm.poseRingFinger.poseBone2,    _b_rRingSeg2);
    MATRIX_TO_V2_6D_PAT(_sd.poseUpperBody.poseRightArm.poseRingFinger.poseBone3,    _b_rRingSeg3);
    MATRIX_TO_V2_6D_PAT(_sd.poseUpperBody.poseRightArm.posePinkyFinger.poseBone1,   _b_rPinkySeg1);
    MATRIX_TO_V2_6D_PAT(_sd.poseUpperBody.poseRightArm.posePinkyFinger.poseBone2,   _b_rPinkySeg2);
    MATRIX_TO_V2_6D_PAT(_sd.poseUpperBody.poseRightArm.posePinkyFinger.poseBone3,   _b_rPinkySeg3);

    // End values in skeleton definition
    _sd.lenLeftToesEnd          = offsetLToesEndSize;
    _sd.lenRightToesEnd         = offsetRToesEndSize;
    _sd.lenSkullEnd             = offsetSkullEndSize;
    _sd.lenLeftThumbEnd         = offsetLThumbEndSize;
    _sd.lenLeftIndexFingerEnd   = offsetLIndexEndSize;
    _sd.lenLeftMiddleFingerEnd  = offsetLMiddleEndSize;
    _sd.lenLeftRingFingerEnd    = offsetLRingEndSize;
    _sd.lenLeftPinkyFingerEnd   = offsetLPinkyEndSize;
    _sd.lenRightThumbEnd         = offsetRThumbEndSize;
    _sd.lenRightIndexFingerEnd   = offsetRIndexEndSize;
    _sd.lenRightMiddleFingerEnd  = offsetRMiddleEndSize;
    _sd.lenRightRingFingerEnd    = offsetRRingEndSize;
    _sd.lenRightPinkyFingerEnd   = offsetRPinkyEndSize;
  }

  // Pelvis initialization
  _pelvisPosition = VZero;
  _lastPelvisPosition = Vector3(0, 1, 0);

  // Skeleton bones initialization
  _lHip         = MIdentity;
  _lKnee        = MIdentity;
  _lAnkle       = MIdentity;
  _lMetatarsal  = MIdentity;
  _rHip         = MIdentity;
  _rKnee        = MIdentity;
  _rAnkle       = MIdentity;
  _rMetatarsal  = MIdentity;
  _pelvis       = MIdentity;
  _spine1       = MIdentity;
  _spine2       = MIdentity;
  _spine3       = MIdentity;
  _spine4       = MIdentity;
  _neck1        = MIdentity;
  _neck2        = MIdentity;
  _skull        = MIdentity;
  _lEye         = MIdentity;
  _rEye         = MIdentity;
  _lClavicle    = MIdentity;
  _lUpperArm    = MIdentity;
  _lLowerArm    = MIdentity;
  _lHand        = MIdentity;
  _lThumbSeg1   = MIdentity;
  _lThumbSeg2   = MIdentity;
  _lThumbSeg3   = MIdentity;
  _lIndexSeg1   = MIdentity;
  _lIndexSeg2   = MIdentity;
  _lIndexSeg3   = MIdentity;
  _lMiddleSeg1  = MIdentity;
  _lMiddleSeg2  = MIdentity;
  _lMiddleSeg3  = MIdentity;
  _lRingSeg1    = MIdentity;
  _lRingSeg2    = MIdentity;
  _lRingSeg3    = MIdentity;
  _lPinkySeg1   = MIdentity;
  _lPinkySeg2   = MIdentity;
  _lPinkySeg3   = MIdentity;
  _rClavicle    = MIdentity;
  _rUpperArm    = MIdentity;
  _rLowerArm    = MIdentity;
  _rHand        = MIdentity;
  _rThumbSeg1   = MIdentity;
  _rThumbSeg2   = MIdentity;
  _rThumbSeg3   = MIdentity;
  _rIndexSeg1   = MIdentity;
  _rIndexSeg2   = MIdentity;
  _rIndexSeg3   = MIdentity;
  _rMiddleSeg1  = MIdentity;
  _rMiddleSeg2  = MIdentity;
  _rMiddleSeg3  = MIdentity;
  _rRingSeg1    = MIdentity;
  _rRingSeg2    = MIdentity;
  _rRingSeg3    = MIdentity;
  _rPinkySeg1   = MIdentity;
  _rPinkySeg2   = MIdentity;
  _rPinkySeg3   = MIdentity;

  _weapon       = MIdentity;
  _launcher     = MIdentity;
  _righthand    = MIdentity;
  _lefthand     = MIdentity;

  _isFired = false;
  _isReloaded = false;

  // Zoom, health, fatigue
  _lastFS.Reset();
  _lastAO.Reset(); // Reset aim object to default values
  _lastAOS.Reset();
  _lastAOP.Reset();
  _lastAOB.Reset();
  _lastUV = V2_Interface::USER_VIEW_FIRST_PERSON;
  _lastMS.Reset(); // Reset mobility state to default values
  _lastCSD.Reset(); // Reset client state to default values

  // Initialize communication with Pointman server
  // Set definition of the skeleton
  {
    if (!_sd.CheckValues())
    {
      RptF("{warning} Pointman: Skeleton definition values are not properly set, checking failed");
    }
    BYTE bRspError;
    WORD wRspSeqnum;
    if (!V2_CmdSetSkeletalDef(_sd, &bRspError, &wRspSeqnum))
    {
      RptF("{error} Pointman: Setting of skeleton definition failed, error %d", bRspError);
      return;
    }
    RptF("{info} Pointman: Skeleton defined");
  }

  // Set user parameters
  {
    V2_Interface::_V2_UserParams up;
    BYTE bRspError;
    WORD wRspSeqnum;
    up.bDominantEye = V2_Interface::RIGHT;
    up.bDominantHand = V2_Interface::RIGHT;
    if (!V2_CmdSetUserParams(up, &bRspError, &wRspSeqnum))
    {
      RptF("{error} Pointman: Cannot set user parameters, error %d", bRspError);
    }
    RptF("{info} Pointman: User parameters set");
  }

  // Set display config
  {
    V2_Interface::_V2_DisplayConfig dc;
    dc.bHeadMounted = IsViewAlignedWithHead();
    BYTE bRspError;
    WORD wRspSeqnum;
    if (!V2_CmdSetDisplayConfig(dc, &bRspError, &wRspSeqnum))
    {
      RptF("{error} Pointman: Cannot set display config, error %d", bRspError);
    }
    RptF("{info} Pointman: Display config set");
  }

  //     V2_CmdSetSecondaryWeapon
  //     V2_CmdSetPistolWeapon
  //     V2_CmdSetMountedWeapon
  //     V2_CmdSetBinoculars

  //     XV2_CmdGetPreSitPelvisRotations


  // Start server input generation
  {
    BYTE bRspError;
    WORD wRspSeqnum;
    if (!V2_CmdStartServerInputGen(&bRspError, &wRspSeqnum))
    {
      RptF("{error} Pointman: Cannot start server input generation, error %d", bRspError);
    }
    RptF("{info} Pointman: Server input generation started");
  }

  // Start data transmission
  {
    BYTE bRspError;
    WORD wRspSeqnum;
    if (!V2_CmdStartXmit(&bRspError, &wRspSeqnum))
    {
      RptF("{error} Pointman: Cannot start data transmission, error %d", bRspError);
    }
    RptF("{info} Pointman: Data transmission started");
  }

  // Check server status
  {
    BYTE bStatus;
    BYTE bRspError;
    WORD wRspSeqnum;
    if (!V2_CmdGetServerStatus(bStatus, &bRspError, &wRspSeqnum))
    {
      RptF("{error} Pointman: Cannot get server status, error %d", bRspError);
    }

    if ((bStatus&V2_Interface::STATUS_INPUT_GEN) == 0)
    {
      RptF("{error} Pointman: Input generation not started");
    }

    if ((bStatus&V2_Interface::STATUS_DATA_XMIT) == 0)
    {
      RptF("{error} Pointman: Data transmission not started");
    }
  }
  
}

Pointman2Pose::~Pointman2Pose()
{
  // Finish communication with Pointman server
  // Stop data transmission
  if (!V2_CmdStopXmit())
  {
    RptF("{error} Pointman: Failed to stop data transmission");
  }
  RptF("{info} Pointman: Data transmission stopped");

  // Stop server input generation
  if (!V2_CmdStopServerInputGen())
  {
    RptF("{error} Pointman: Failed to stop server input generation");
  }
  RptF("{info} Pointman: Server input generation stopped");

  // Dirty hack - Pointman server has a problem in case we stop data transmission and start it again immediately (it won't start)
  // Putting some pause in between helps to solve the problem
  Sleep(1000);
}

bool Pointman2Pose::IsLowerBodySkeletonFullyControlled() const
{
  return _serverMotionDat.bMotionCtrlLower == V2_Interface::MOTION_CTRL_FULL;
} 

bool Pointman2Pose::IsLowerBodySkeletonControlled() const
{
  return _serverMotionDat.bMotionCtrlLower != V2_Interface::MOTION_CTRL_NONE;
} 

void Pointman2Pose::Simulate()
{
  if (!IsActive()) return;

  // User control is being displayed, don't do any updates as we use own SetMotionState in that case
  if (_userControlDisplayed > 0) return;

  // Update Pointman's primary weapon
  UpdateAimObject(V2_Interface::HELD_OBJECT_PRIMARY_WEAPON);
  // Update Pointman's secondary weapon
  UpdateAimObject(V2_Interface::HELD_OBJECT_SECONDARY_WEAPON);
  // Update Pointman's handgun
  UpdateAimObject(V2_Interface::HELD_OBJECT_PISTOL_WEAPON);
  // Update Pointman's binoculars
  UpdateAimObject(V2_Interface::HELD_OBJECT_BINOCULARS);

  // Update Pointman's user view
  int camType;
  {
    BYTE userView = V2_Interface::USER_VIEW_FIRST_PERSON;
    camType = GetCameraType();
    switch (camType)
    {
    case CamExternal:
    case CamGroup:
      userView = V2_Interface::USER_VIEW_THIRD_PERSON;
      break;
    }
    SetUserView(userView);
  }

  // Update Pointman's motion state
  {
    // Get the upDegree
    int actUpDegree = GetActUpDegree();

    // Motion state
    V2_Interface::_V2_MotionState ms;

    // Initialize mobility state

    bool isAlive = IsAlive();

    bool isInVehicle = IsInVehicle();

    bool isDriver = false;
    bool isGunner = false;
    bool isPassanger = false;
    if (isInVehicle)
    {
      isDriver = IsDriver();  

      isGunner = IsGunner();
    }

    bool isOnSomeLadder = IsOnSomeLadder();

    if ((actUpDegree == ManPosSwimming) || (actUpDegree == ManPosDiving) || (actUpDegree == ManPosBottomDiving) || 
        (actUpDegree == ManPosSurfaceDiving) || (actUpDegree == ManPosBottomSwimming) || (actUpDegree == ManPosSurfaceSwimming) ||(actUpDegree == ManPosGoingFromWater))
    {
      ms.bMobilityState = V2_Interface::MOBILITY_SWIMMING;
#if _MOVE_TO_SURFACE
      if (_lastMS.bMobilityState != V2_Interface::MOBILITY_SWIMMING)
      {
        // We want soldier to start swimming on surface
        MoveToSurface();
      }
#endif
    }
    else
    {
      if (!isAlive)
      {
        ms.bMobilityState = V2_Interface::MOBILITY_SUSPENDED;
      }
      else
      {
        if (isInVehicle)
        {
          if (isDriver)
          {
            ms.bMobilityState = V2_Interface::MOBILITY_DRIVER;
          }
          else if (isGunner)
          {
			  ms.bMobilityState = V2_Interface::MOBILITY_GUNNER;
          }
          else
          {
            isPassanger = true;
			ms.bMobilityState = V2_Interface::MOBILITY_PASSENGER;
          }
        }
        else
        {
          if (isOnSomeLadder || (actUpDegree == -1)) // It was detected that actUpDegree is -1 when player is on ladder in first few frames. The condition is not very nice because of that.
          {
            ms.bMobilityState = V2_Interface::MOBILITY_CLIMBING;
          }
          else
          {
            ms.bMobilityState = V2_Interface::MOBILITY_GROUNDED;
          }
        }
      }
    }
    

    // Initialize body posture
      switch (actUpDegree)
      {
      case ManPosDead:
      case ManPosBinocLying:
      case ManPosLyingNoWeapon:
      case ManPosSwimming:
      case ManPosBottomDiving:
      case ManPosSurfaceDiving:
      case ManPosBottomSwimming:
      case ManPosSurfaceSwimming:
      case ManPosLying:
      case ManPosHandGunLying:
      case ManPosLauncherLying:
      case ManPosDiving:
        ms.bBodyPosture = V2_Interface::BODY_POSTURE_PRONE;
        break;
      case ManPosLowCrouch:
      case ManPosHandGunLowCrouch:
      case ManPosBinoc: 
      case ManPosWeapon:
      case ManPosCrouch:
      case ManPosHandGunCrouch:
       case ManPosLauncherCrouch:
        ms.bBodyPosture = V2_Interface::BODY_POSTURE_CROUCH;
        break;
      case ManPosStand:
      case ManPosHandGunLowStand:
      case ManPosNoWeapon:
      case ManPosBinocStand:
      case ManPosCombat:
      case ManPosHandGunStand:
      case ManPosLauncherStand:
      case ManPosGoingFromWater:
        ms.bBodyPosture = V2_Interface::BODY_POSTURE_STAND;
        break;
	  case -1:
		  switch (ms.bMobilityState)
		  {
		  case V2_Interface::MOBILITY_DRIVER:
		  case V2_Interface::MOBILITY_PASSENGER:
			  ms.bBodyPosture = V2_Interface::BODY_POSTURE_SEATED;
			  break;
		  case V2_Interface::MOBILITY_GUNNER:
			  ms.bBodyPosture = V2_Interface::BODY_POSTURE_STAND;
			  break;
		  case V2_Interface::MOBILITY_SWIMMING:
			  ms.bBodyPosture = V2_Interface::BODY_POSTURE_PRONE;
			  break;
		  case V2_Interface::MOBILITY_CLIMBING:
			  ms.bBodyPosture = V2_Interface::BODY_POSTURE_STAND;
			  break;
		  default:
			  RptF("{error} Pointman: UpDegree=-1, MobilityState=%d", ms.bMobilityState);
			  ms.bBodyPosture = V2_Interface::DEF_BODY_POSTURE;
			  break;
		  }
		  break;
	  default:
		  RptF("{error} BP: Unhandled degree state %d", actUpDegree);
		  ms.bBodyPosture = V2_Interface::DEF_BODY_POSTURE;
		  break;
      }

      // Detect if avatar is seated in which case overwrite the body posture
      bool isSeated = IsSeated();
      if (isSeated || isPassanger)
      {
        ms.bBodyPosture = V2_Interface::BODY_POSTURE_SEATED;
      }
 
	 // Initialize engagement stance (heldObject, aiming, optics)
     ms.bAiming = 0;
     ms.bOptics = (camType == CamGunner) ? 1 : 0;
	 bool isWeaponOnBack = IsWeaponOnBack();

     switch ( actUpDegree )
      {
      case ManPosDead:
      case ManPosSwimming:
      case ManPosLyingNoWeapon:
      case ManPosNoWeapon:
      case ManPosDiving:
      case ManPosBottomDiving:
      case ManPosSurfaceDiving:
      case ManPosBottomSwimming:
      case ManPosSurfaceSwimming:
        ms.bHeldObject = V2_Interface::HELD_OBJECT_HANDS_FREE;
        break;

      case ManPosStand:
      case ManPosCrouch:
      case ManPosLying:
      case ManPosCombat:
      case ManPosGoingFromWater:
      case ManPosLowCrouch:
        {
          if ( isWeaponOnBack ) 
		  {
				ms.bHeldObject = V2_Interface::HELD_OBJECT_HANDS_FREE;
          }
          else
          {
            // updegree could be incorrect in case of turn out animation, so we need to check if we really have primary weapon
            if (IsCurrentWeaponHandGun())
            {
				ms.bHeldObject = V2_Interface::HELD_OBJECT_PISTOL_WEAPON;
            }
			else
			{
				ms.bHeldObject = V2_Interface::HELD_OBJECT_PRIMARY_WEAPON;
			}
			ms.bAiming = 1;
          }
          break;
        }
      case ManPosWeapon:
      case ManPosLauncherStand:
      case ManPosLauncherCrouch:
      case ManPosLauncherLying:
        ms.bHeldObject = V2_Interface::HELD_OBJECT_SECONDARY_WEAPON;
        ms.bAiming = 1;
        break;

      case ManPosHandGunStand: 
      case ManPosHandGunCrouch:
      case ManPosHandGunLying:
      case ManPosHandGunLowCrouch:
      case ManPosHandGunLowStand:
        ms.bHeldObject = V2_Interface::HELD_OBJECT_PISTOL_WEAPON;
        ms.bAiming = 1;
        break;

      case ManPosBinoc:
      case ManPosBinocStand:
      case ManPosBinocLying:
        ms.bHeldObject = V2_Interface::HELD_OBJECT_BINOCULARS;
        ms.bAiming = 1;
        break;

	  case -1:
		 if (ms.bMobilityState == V2_Interface::MOBILITY_GUNNER)
		 {
			 ms.bHeldObject = V2_Interface::HELD_OBJECT_MOUNTED_WEAPON;
			 ms.bAiming = 1;
		 }
		 else
		 {
			 ms.bHeldObject = V2_Interface::HELD_OBJECT_HANDS_FREE;
		 }
		 break;

      default:
        RptF("{error} ES: Unhandled degree state %d", actUpDegree);
        ms.bHeldObject = V2_Interface::DEF_HELD_OBJECT;
      }
	  
	// Initialize pointman control of lower and upper body
	switch (ms.bMobilityState)
	{
	case V2_Interface::MOBILITY_GROUNDED:
		SetMotionControllLower(ms.bMotionCtrlLower, V2_Interface::MOTION_CTRL_FULL);
		if (ms.bHeldObject == V2_Interface::HELD_OBJECT_HANDS_FREE || ms.bHeldObject == V2_Interface::HELD_OBJECT_PRIMARY_WEAPON)
		{
			//SetMotionControllUpper(ms.bMotionCtrlUpper,V2_Interface::MOTION_CTRL_FULL);
			ms.bMotionCtrlUpper = V2_Interface::MOTION_CTRL_FULL;
		}
		else
		{
			// temp solution for secondary weapon, pistol and binoculars
			//SetMotionControllUpper(ms.bMotionCtrlUpper,V2_Interface::MOTION_CTRL_PARTIAL);
			ms.bMotionCtrlUpper = V2_Interface::MOTION_CTRL_PARTIAL;
		}
		break;
	case V2_Interface::MOBILITY_CLIMBING:
		//SetMotionControllLower(ms.bMotionCtrlLower, V2_Interface::MOTION_CTRL_NONE);
		//SetMotionControllUpper(ms.bMotionCtrlUpper, V2_Interface::MOTION_CTRL_PARTIAL);
		ms.bMotionCtrlLower = V2_Interface::MOTION_CTRL_NONE;
		ms.bMotionCtrlUpper = V2_Interface::MOTION_CTRL_PARTIAL;
		break;
	case V2_Interface::MOBILITY_SWIMMING:
		//SetMotionControllLower(ms.bMotionCtrlLower, V2_Interface::MOTION_CTRL_NONE);
		//SetMotionControllUpper(ms.bMotionCtrlUpper, V2_Interface::MOTION_CTRL_PARTIAL);
		ms.bMotionCtrlLower = V2_Interface::MOTION_CTRL_NONE;
		ms.bMotionCtrlUpper = V2_Interface::MOTION_CTRL_PARTIAL;
		break;
	case V2_Interface::MOBILITY_DRIVER:
		//SetMotionControllLower(ms.bMotionCtrlLower, V2_Interface::MOTION_CTRL_NONE);
		//SetMotionControllUpper(ms.bMotionCtrlUpper, V2_Interface::MOTION_CTRL_PARTIAL);
		ms.bMotionCtrlLower = V2_Interface::MOTION_CTRL_NONE;
		ms.bMotionCtrlUpper = V2_Interface::MOTION_CTRL_PARTIAL;
		break;
	case V2_Interface::MOBILITY_PASSENGER:
		//SetMotionControllLower(ms.bMotionCtrlLower, V2_Interface::MOTION_CTRL_NONE);
		ms.bMotionCtrlLower = V2_Interface::MOTION_CTRL_NONE;
		if (ms.bHeldObject == V2_Interface::HELD_OBJECT_HANDS_FREE || ms.bHeldObject == V2_Interface::HELD_OBJECT_PRIMARY_WEAPON)
		{
			//SetMotionControllUpper(ms.bMotionCtrlUpper, V2_Interface::MOTION_CTRL_FULL);
			ms.bMotionCtrlUpper = V2_Interface::MOTION_CTRL_FULL;
		}
		else
		{
			// temp solution for secondary weapon, pistol and binoculars
			//SetMotionControllUpper(ms.bMotionCtrlUpper, V2_Interface::MOTION_CTRL_PARTIAL);
			ms.bMotionCtrlUpper = V2_Interface::MOTION_CTRL_PARTIAL;
		}
		break;
	case V2_Interface::MOBILITY_GUNNER:
		//SetMotionControllLower(ms.bMotionCtrlLower, V2_Interface::MOTION_CTRL_NONE);
		ms.bMotionCtrlLower = V2_Interface::MOTION_CTRL_NONE;
		if (ms.bHeldObject == V2_Interface::HELD_OBJECT_HANDS_FREE || ms.bHeldObject == V2_Interface::HELD_OBJECT_PRIMARY_WEAPON)
		{
			//SetMotionControllUpper(ms.bMotionCtrlUpper, V2_Interface::MOTION_CTRL_FULL);
			ms.bMotionCtrlUpper = V2_Interface::MOTION_CTRL_FULL;
		}
		else if (ms.bHeldObject == V2_Interface::HELD_OBJECT_PISTOL_WEAPON || ms.bHeldObject == V2_Interface::HELD_OBJECT_BINOCULARS)
		{
			// temp solution for pistol and binoculars
			//SetMotionControllUpper(ms.bMotionCtrlUpper, V2_Interface::MOTION_CTRL_PARTIAL);
			ms.bMotionCtrlUpper = V2_Interface::MOTION_CTRL_PARTIAL;
		}
		else
		{
			//SetMotionControllUpper(ms.bMotionCtrlUpper, V2_Interface::MOTION_CTRL_NONE);
			ms.bMotionCtrlUpper = V2_Interface::MOTION_CTRL_NONE;
		}
		break;
	case V2_Interface::MOBILITY_SUSPENDED:
		//SetMotionControllLower(ms.bMotionCtrlLower, V2_Interface::MOTION_CTRL_NONE);
		//SetMotionControllUpper(ms.bMotionCtrlUpper, V2_Interface::MOTION_CTRL_NONE);
		ms.bMotionCtrlLower = V2_Interface::MOTION_CTRL_NONE;
		ms.bMotionCtrlUpper = V2_Interface::MOTION_CTRL_NONE;
		break;
	}

	// PDenbrook: Removed the following to prevent VBS posture setting and collision geometry 
	//	getting out of sync with Pointman-controlled body posture.
	//// When Pointman has full lower body control, we want bBodyPosture to corresond with _serverMotionDat
	//if (IsLowerBodySkeletonFullyControlled())
	//{
	//	ms.bBodyPosture = _serverMotionDat.bBodyPosture;
	//}
	
	// When Pointman have full upper body control we want bAiming and bOptics to correspond with _serverMotionDat
    if (IsUpperBodyFullyControlled())
    {
      ms.bAiming = _serverMotionDat.bAiming;
	  ms.bOptics = _serverMotionDat.bOptics;
    }

    // Set the rotation limits
    {
      // Torso limits (hardcoded)
      ms.rotLimitsDirTorso.rotNeg.rx = -85.0f;
      ms.rotLimitsDirTorso.rotNeg.ry = -45.0f;
      ms.rotLimitsDirTorso.rotNeg.rz = -45.0f;
      ms.rotLimitsDirTorso.rotPos.rx = 20.0f;
      ms.rotLimitsDirTorso.rotPos.ry = 45.0f;
      ms.rotLimitsDirTorso.rotPos.rz = 45.0f;

      // View limits (hardcoded)
      ms.rotLimitsDirView.rotNeg.rx =  -80.0f;
      ms.rotLimitsDirView.rotNeg.ry =  -80.0f;
      ms.rotLimitsDirView.rotNeg.rz =  -100.0f;
      ms.rotLimitsDirView.rotPos.rx =  80.0f;
      ms.rotLimitsDirView.rotPos.ry =  80.0f;
      ms.rotLimitsDirView.rotPos.rz =  100.0f;

      // Lying -> Gun&Torso X <-8, 40>
      if (ms.bBodyPosture == V2_Interface::BODY_POSTURE_PRONE)
      {
        ms.rotLimitsDirTorso.rotNeg.rx = -8.0f;
        ms.rotLimitsDirTorso.rotPos.rx = 40.0f;
      }
    }

    // In case soldier have weapon on back, we need to raise weapon when reload action is performed, because this action no longer raise weapon by itself
    if (_isReloaded && ms.bHeldObject == V2_Interface::HELD_OBJECT_HANDS_FREE && isWeaponOnBack)
    {
      RaiseWeapon();
    }

    // Send motion state to the Pointman
    SetMotionState(ms);

	// Apply the lower body and upper body motion control levels here (after sending the
	// motion state to Pointman), since Pointman may have downgraded a level to one it can
	// support, e.g., if Pointman is configured to run without foot pedals, the lower 
	// body control level is downgraded from full to partial in the grounded mobility state
	ApplyMotionControllLower(ms.bMotionCtrlLower);
	ApplyMotionControllUpper(ms.bMotionCtrlUpper);
  }

  // Update Pointman's fire state
  {
    V2_Interface::_V2_FireState fs;

    char weaponName[256];
    int weaponType;
    int ammoCount;
    float weaponWeight;
    char magazineName[256];
    int magazineType;
    float magazineWeight;
    int magazineAmmoSimulation;
    GetCurrentWeaponInfo(weaponName, magazineName, ammoCount, weaponType, weaponWeight, magazineType, magazineWeight, magazineAmmoSimulation);

    char mode[512];
    GetCurrentWeaponMode(mode);

    if (strlen(mode) > 0)
    {
      if (strcmp(mode, "FullAuto") == 0) 
      {
        fs.bFireMode = V2_Interface::FIRE_FULL_AUTO;
      } 
      else if (strcmp(mode, "Burst") == 0)
      {
        fs.bFireMode = V2_Interface::FIRE_BURST;
      }
      else 
      {
        fs.bFireMode = V2_Interface::FIRE_SINGLE;    
      }
    } 
    else
    {
      fs.bFireMode = V2_Interface::FIRE_NONE; 
    }

    // Safety Switch
    bool weaponSafety = WeaponSafety();
    if (weaponSafety)
    {
      fs.bFireMode = V2_Interface::FIRE_NONE; 
    }

    if (!IsUpperBodyFullyControlled()) 
    {
      GetRecoilImpulse(_impulseAngX, _impulseAngY, _impulseZ);
    }

    // Update recoil only when weapon or mode changes
    if (fs.bFireMode != _lastFS.bFireMode || strcmp(weaponName, _lastWeaponName) != 0)
    {
      _impulseAngX = _impulseAngY = _impulseZ = 0;
      strcpy (_lastWeaponName, weaponName);
    }
    fs.ammo.recoilEnergy = fabs(_impulseAngX) + fabs(_impulseAngY);  


    fs.bSubsystem = V2_Interface::SUBSYS_THROW;

    if ( magazineAmmoSimulation >= 0)
    {
      fs.ammo.weight    = magazineWeight;
      fs.ammo.bMagazine = FALSE;

      if (_lastMS.bHeldObject != V2_Interface::HELD_OBJECT_HANDS_FREE)
      {
        switch (magazineAmmoSimulation)
        {
        case AmmoShotBullet:
          fs.bSubsystem = V2_Interface::SUBSYS_MAIN;
          fs.ammo.bMagazine = TRUE;
          break;

        case AmmoShotMissile:
          fs.bSubsystem = V2_Interface::SUBSYS_MAIN;
          break;

        case AmmoShotShell:
          if ((magazineType & MaskSlotItem) == 0)
          {
            fs.bSubsystem = V2_Interface::SUBSYS_ATTACHED;
            fs.ammo.bMagazine = TRUE;
          }
          break;
        }
      }
    }

    if (strlen(magazineName) > 0)
    {
      if ( fs.bSubsystem != V2_Interface::SUBSYS_THROW )
      {
        fs.ammo.numRounds = ammoCount;
      }
      else if ( weaponType == 0 )
      {
        // Granades 
        int magazines;

        magazines = GetMagazinesCountOfType(magazineName);

        fs.ammo.numRounds = magazines;
      }
     
    }
    
    strncpy( fs.ammo.strDescription, magazineName, 30);    // char strDescription[30]; 

    fs.ammo.weight = magazineWeight; 

    fs.ammo.bObjectType = V2_Interface::HELD_OBJECT_AMMO;
    fs.ammo.bStowable   = TRUE;
    fs.ammo.bGripType   = (fs.bSubsystem != V2_Interface::SUBSYS_THROW) ? V2_Interface::LEFT : V2_Interface::RIGHT;

    fs.ammo.gripLeftHand.bHand  = V2_Interface::LEFT;
    fs.ammo.gripRightHand.bHand = V2_Interface::RIGHT;

    // When the change in fire state is due to a Pointman-generated shot fired we don't want to send V2CmdSetFireState
    if (_isFired)
    {
      // Reset the fire state
      if (strcmp(_lastFS.ammo.strDescription, fs.ammo.strDescription) != 0) 
      {
        _isFired = false;
        _isReloaded = false;
      }
      else
      {
        if (_lastFS.ammo.numRounds > fs.ammo.numRounds) 
        {
          _isFired = false;
        }

        // In case of changed recoil energy we need to send it (first shot from weapon)
        if (_lastFS.ammo.recoilEnergy == fs.ammo.recoilEnergy)
        {
          _lastFS = fs;
        }
      }
    }

    // Send fire state to the Pointman
    SetFireState(fs);

    // Reset the fire state
    _isFired = false;
  }



  // Update Pointman's reload state
  if ( _isReloaded )
  {
    if ( _lastFS.bSubsystem == V2_Interface::SUBSYS_MAIN )
    {
      V2_Interface::_V2_AmmoObject mo;

      mo.recoilEnergy = _lastFS.ammo.recoilEnergy;

      strcpy( mo.strDescription, _lastFS.ammo.strDescription );

      mo.weight    = _lastFS.ammo.weight;
      mo.bMagazine = _lastFS.ammo.bMagazine;
      mo.numRounds = _lastFS.ammo.numRounds;

      mo.bObjectType = V2_Interface::HELD_OBJECT_AMMO;
      mo.bStowable   = TRUE;
      mo.bGripType   = _lastFS.ammo.bGripType;

      mo.gripLeftHand.bHand  = V2_Interface::LEFT;
      mo.gripRightHand.bHand = V2_Interface::RIGHT;

      // Send reload to the Pointman
      if ( !V2_CmdReload( mo ) )
      {
        RptF("{error} Pointman: Failed to reload");
      }
    }
    else if ( _lastFS.bSubsystem == V2_Interface::SUBSYS_ATTACHED )
    {
      if ( !V2_CmdSetFireState( _lastFS ) )
      {
        RptF("{error} Pointman: Failed to send fire state");
      }
    }

    // Reset the reloaded state
    _isReloaded = false;
  }

  // Update Pointman's client state
  {
    V2_Interface::_V2_ClientStateDat clientStateDat;

    // Zoom factor as described in Pointman documentation (the bigger zoom in, the bigger number, default is 1)
    // aiming is scaled based on current FOV
    // default FOV is assumed to be 0.95
    float fov = 0.95f / GetZoom();

    if (!IsUpperBodyFullyControlled())
    {
      fov = GetCameraFOV();
    }

    float zoomFactor = 0.95f / fov;

    SetCameraFOV(fov);

    clientStateDat.fZoom = zoomFactor;

    // Health as described in Pointman documentation (1 is perfect health, 0 is dead)
    float damage = GetDamage();

    float health = 1.0f - damage;

    clientStateDat.fHealth = health;

    float fatigue = GetFatigue();

    clientStateDat.fFatigue = fatigue;

    // Update the Pointman
    if (!(_lastCSD == clientStateDat))
    {
      _lastCSD = clientStateDat;
      if (!V2_SetClientStateXmit(clientStateDat))
      {
        RptF("{error} Pointman: Failed to send update from the client");
      }
    }
  }


  // Receive motion update
  {
    BYTE bError;
    WORD wSeqnum;
    if (!V2_GetServerMotionXmit(_serverMotionDat, bError, wSeqnum))
    {
      RptF("{error} Pointman: Failed to send motion update to the client");
      return;
    }
  }

}

void V2_6D_TO_MATRIX_PAT(Matrix4 &m, const V2_Interface::_V2_6D &d) \
{
  m.SetEulerAngles(d.rx * H_PI / 180.0f, d.ry * H_PI / 180.0f, d.rz * H_PI / 180.0f);
  m.SetPosition(Vector3(d.tx, d.ty, d.tz));
  m.SwitchYZ();
}

#define V2_3DT_TO_VECTOR(v,d) \
{ \
  v = Vector3(d.tx, d.tz, d.ty); \
}

#define VECTOR_TO_V2_3DT(d,v) \
{ \
  d.tx = (v).X(); \
  d.ty = (v).Z(); \
  d.tz = (v).Y(); \
}

#define SET_BONE(boneName,boneSource) \
  V2_6D_TO_MATRIX_PAT(##boneName, boneSource); \
  /*Matrix4 abs##boneName = ##boneName;*/ \
##boneName.SetPosition(##boneName.GetPosition() + _b_pelvis.GetPosition() - ptPelvisPos); \
##boneName = _b_pelvis.GetInvertGeneral() * ##boneName *_b##boneName.GetInvertGeneral() * _b_pelvis;

Matrix4 Pointman2Pose::UpdatePose()
{
  // Initialize position of the pelvis
  V2_3DT_TO_VECTOR(_pelvisPosition, _serverMotionDat.trnPelvis);
  Vector3 ptPelvisPos = Vector3(0.0f, GetPelvisHeight(), 0.0f);
  if (_pelvisPosition.Y() > 0)
  {
    _lastPelvisPosition = _pelvisPosition;
  }

  V2_Interface::_V2_6D pelvisBone( _serverMotionDat.trnPelvis.tx, _serverMotionDat.trnPelvis.ty, _serverMotionDat.trnPelvis.tz,
    _serverMotionDat.rotPelvis.rx, _serverMotionDat.rotPelvis.ry, _serverMotionDat.rotPelvis.rz );
  SET_BONE(_pelvis, pelvisBone);

  //   _serverMotionDat.poseLowerBody = _sd.poseLowerBody;
  //   _serverMotionDat.poseUpperBody = _sd.poseUpperBody;

  // Set the skeleton bones
  SET_BONE(_lHip,         _serverMotionDat.poseLowerBody.poseLeftLeg.poseUpperLeg); // Lower body
  SET_BONE(_lKnee,        _serverMotionDat.poseLowerBody.poseLeftLeg.poseLowerLeg);
  SET_BONE(_lAnkle,       _serverMotionDat.poseLowerBody.poseLeftLeg.poseFoot);
  SET_BONE(_lMetatarsal,  _serverMotionDat.poseLowerBody.poseLeftLeg.poseToes);
  SET_BONE(_rHip,         _serverMotionDat.poseLowerBody.poseRightLeg.poseUpperLeg);
  SET_BONE(_rKnee,        _serverMotionDat.poseLowerBody.poseRightLeg.poseLowerLeg);
  SET_BONE(_rAnkle,       _serverMotionDat.poseLowerBody.poseRightLeg.poseFoot);
  SET_BONE(_rMetatarsal,  _serverMotionDat.poseLowerBody.poseRightLeg.poseToes);
  SET_BONE(_spine1,       _serverMotionDat.poseUpperBody.poseTorso.poseSpine1); // Upper body - spin
  SET_BONE(_spine2,       _serverMotionDat.poseUpperBody.poseTorso.poseSpine2);
  SET_BONE(_spine3,       _serverMotionDat.poseUpperBody.poseTorso.poseSpine3);
  SET_BONE(_spine4,       _serverMotionDat.poseUpperBody.poseTorso.poseSpine4);
  SET_BONE(_neck1,        _serverMotionDat.poseUpperBody.poseHead.poseNeck1);
  SET_BONE(_neck2,        _serverMotionDat.poseUpperBody.poseHead.poseNeck2);
  SET_BONE(_skull,        _serverMotionDat.poseUpperBody.poseHead.poseSkull);
  SET_BONE(_lEye,         _serverMotionDat.poseUpperBody.poseHead.poseLeftEye);
  SET_BONE(_rEye,         _serverMotionDat.poseUpperBody.poseHead.poseRightEye);
  SET_BONE(_lClavicle,    _serverMotionDat.poseUpperBody.poseLeftArm.poseClavicle); // Upper body - left arm
  SET_BONE(_lUpperArm,    _serverMotionDat.poseUpperBody.poseLeftArm.poseUpperArm);
  SET_BONE(_lLowerArm,    _serverMotionDat.poseUpperBody.poseLeftArm.poseLowerArm);
  SET_BONE(_lHand,        _serverMotionDat.poseUpperBody.poseLeftArm.poseHand);
  SET_BONE(_lThumbSeg1,   _serverMotionDat.poseUpperBody.poseLeftArm.poseThumb.poseBone1);
  SET_BONE(_lThumbSeg2,   _serverMotionDat.poseUpperBody.poseLeftArm.poseThumb.poseBone2);
  SET_BONE(_lThumbSeg3,   _serverMotionDat.poseUpperBody.poseLeftArm.poseThumb.poseBone3);
  SET_BONE(_lIndexSeg1,   _serverMotionDat.poseUpperBody.poseLeftArm.poseIndexFinger.poseBone1);
  SET_BONE(_lIndexSeg2,   _serverMotionDat.poseUpperBody.poseLeftArm.poseIndexFinger.poseBone2);
  SET_BONE(_lIndexSeg3,   _serverMotionDat.poseUpperBody.poseLeftArm.poseIndexFinger.poseBone3);
  SET_BONE(_lMiddleSeg1,  _serverMotionDat.poseUpperBody.poseLeftArm.poseMiddleFinger.poseBone1);
  SET_BONE(_lMiddleSeg2,  _serverMotionDat.poseUpperBody.poseLeftArm.poseMiddleFinger.poseBone2);
  SET_BONE(_lMiddleSeg3,  _serverMotionDat.poseUpperBody.poseLeftArm.poseMiddleFinger.poseBone3);
  SET_BONE(_lRingSeg1,    _serverMotionDat.poseUpperBody.poseLeftArm.poseRingFinger.poseBone1);
  SET_BONE(_lRingSeg2,    _serverMotionDat.poseUpperBody.poseLeftArm.poseRingFinger.poseBone2);
  SET_BONE(_lRingSeg3,    _serverMotionDat.poseUpperBody.poseLeftArm.poseRingFinger.poseBone3);
  SET_BONE(_lPinkySeg1,   _serverMotionDat.poseUpperBody.poseLeftArm.posePinkyFinger.poseBone1);
  SET_BONE(_lPinkySeg2,   _serverMotionDat.poseUpperBody.poseLeftArm.posePinkyFinger.poseBone2);
  SET_BONE(_lPinkySeg3,   _serverMotionDat.poseUpperBody.poseLeftArm.posePinkyFinger.poseBone3);
  SET_BONE(_rClavicle,    _serverMotionDat.poseUpperBody.poseRightArm.poseClavicle); // Upper body - right arm
  SET_BONE(_rUpperArm,    _serverMotionDat.poseUpperBody.poseRightArm.poseUpperArm);
  SET_BONE(_rLowerArm,    _serverMotionDat.poseUpperBody.poseRightArm.poseLowerArm);
  SET_BONE(_rHand,        _serverMotionDat.poseUpperBody.poseRightArm.poseHand);
  SET_BONE(_rThumbSeg1,   _serverMotionDat.poseUpperBody.poseRightArm.poseThumb.poseBone1);
  SET_BONE(_rThumbSeg2,   _serverMotionDat.poseUpperBody.poseRightArm.poseThumb.poseBone2);
  SET_BONE(_rThumbSeg3,   _serverMotionDat.poseUpperBody.poseRightArm.poseThumb.poseBone3);
  SET_BONE(_rIndexSeg1,   _serverMotionDat.poseUpperBody.poseRightArm.poseIndexFinger.poseBone1);
  SET_BONE(_rIndexSeg2,   _serverMotionDat.poseUpperBody.poseRightArm.poseIndexFinger.poseBone2);
  SET_BONE(_rIndexSeg3,   _serverMotionDat.poseUpperBody.poseRightArm.poseIndexFinger.poseBone3);
  SET_BONE(_rMiddleSeg1,  _serverMotionDat.poseUpperBody.poseRightArm.poseMiddleFinger.poseBone1);
  SET_BONE(_rMiddleSeg2,  _serverMotionDat.poseUpperBody.poseRightArm.poseMiddleFinger.poseBone2);
  SET_BONE(_rMiddleSeg3,  _serverMotionDat.poseUpperBody.poseRightArm.poseMiddleFinger.poseBone3);
  SET_BONE(_rRingSeg1,    _serverMotionDat.poseUpperBody.poseRightArm.poseRingFinger.poseBone1);
  SET_BONE(_rRingSeg2,    _serverMotionDat.poseUpperBody.poseRightArm.poseRingFinger.poseBone2);
  SET_BONE(_rRingSeg3,    _serverMotionDat.poseUpperBody.poseRightArm.poseRingFinger.poseBone3);
  SET_BONE(_rPinkySeg1,   _serverMotionDat.poseUpperBody.poseRightArm.posePinkyFinger.poseBone1);
  SET_BONE(_rPinkySeg2,   _serverMotionDat.poseUpperBody.poseRightArm.posePinkyFinger.poseBone2);
  SET_BONE(_rPinkySeg3,   _serverMotionDat.poseUpperBody.poseRightArm.posePinkyFinger.poseBone3);

  // Create matrix from received pose
  Matrix4 nextPose;
  nextPose.SetDirectionAndUp(Vector3(-sin(_serverMotionDat.sitPos.heading * 2.0f *H_PI / 360.0f), 0.0f, cos(_serverMotionDat.sitPos.heading * 2.0f *H_PI / 360.0f)), VUp);
  nextPose.SetPosition(Vector3(_serverMotionDat.sitPos.displacement.tx, _serverMotionDat.sitPos.displacement.tz, _serverMotionDat.sitPos.displacement.ty));

  // Create relative matrix to the previous one, in case the next pose equals to the last pose, we don't need to calculate anything (we even would receive inaccuracies)
  Matrix4 relChange;
  if (nextPose == _lastPose)
  {
    relChange = MIdentity;
  }
  else
  {
    Matrix4 invLastPose = _lastPose.GetInvertRotation();
    relChange = invLastPose * nextPose;
  }

  // In case the mobility state has been changed or the motion (nextPose) was reset to (0,0,0). We have to reflect that in the relative change (reset it as well)
  if (_oldServerMotionDat.bMobilityState != _serverMotionDat.bMobilityState || nextPose.GetPosition() == VZero)
  {
    relChange = MIdentity;
  }

  // Update body posture in VBS, mainly because of collision bounding box

  // We need also check _lastMS because when body posture is changed quickly, new action trigged by SetAction is not started until previous action is done
  // in this case we need to send it repeatedly until value in _lastMS is correct.
  if (IsLowerBodySkeletonControlled() && (_oldServerMotionDat.bBodyPosture != _serverMotionDat.bBodyPosture || _lastMS.bBodyPosture != _serverMotionDat.bBodyPosture))
  {
    switch (_serverMotionDat.bBodyPosture)
    {
    case V2_Interface::BODY_POSTURE_CROUCH:
      SetAction("Crouch");
      break;
    case V2_Interface::BODY_POSTURE_PRONE:
      SetAction("Prone");
      break;
    case V2_Interface::BODY_POSTURE_STAND:
      SetAction("Stand");
      break;
    case V2_Interface::BODY_POSTURE_SEATED:
      // Looks like that crouch position have better collision bonding box then "sitdown" stance
      SetAction("Crouch");
      break;
    }
  }

  // Update the last pose
  _lastPose = nextPose;

  // Remember the upper body view, aim and lean angles
  {
    // Remember torso, view and aim rotation angles
    Vector3 torso = Vector3(_serverMotionDat.rotDirTorso.rx * H_PI / 180.0f, _serverMotionDat.rotDirTorso.rz * H_PI / 180.0f, _serverMotionDat.rotDirTorso.ry * H_PI / 180.0f);
    Vector3 view = Vector3(_serverMotionDat.rotDirView.rx * H_PI / 180.0f, _serverMotionDat.rotDirView.rz * H_PI / 180.0f, _serverMotionDat.rotDirView.ry * H_PI / 180.0f);
    Vector3 aim = Vector3(_serverMotionDat.rotDirAim.rx * H_PI / 180.0f, _serverMotionDat.rotDirAim.rz * H_PI / 180.0f, _serverMotionDat.rotDirAim.ry * H_PI / 180.0f);

    // Setup torso, view and aim transformations
    _torso = Matrix4().GetRotatedY(torso.Y()) * Matrix4().GetRotatedZ(-torso.Z()) * Matrix4().GetRotatedX(-torso.X());
    _view = Matrix4().GetRotatedY(view.Y()) * Matrix4().GetRotatedZ(-view.Z()) * Matrix4().GetRotatedX(-view.X());
    _aim = Matrix4().GetRotatedY(aim.Y()) * Matrix4().GetRotatedZ(-aim.Z()) * Matrix4().GetRotatedX(-aim.X());
  }

#if _RESET_TORSO_TRANS_ON_LADDER
  if (IsOnSomeLadder())
  {
    _torso = MIdentity;
  }
#endif

  // Get the gun transformation
  V2_6D_TO_MATRIX_PAT(_gun, _serverMotionDat.poseHeldObject);
  Vector3 gunUp = _gun.GetUp();
  Vector3 gunDir = _gun.GetDirection();
  _gun.SetDirectionAndUp(gunUp, gunDir); // Muzzle in Pointman space points to the Y direction, whereas in VBS space to the Z direction -> convert it

  // Get the camera transformation
  V2_6D_TO_MATRIX_PAT(_camera, _serverMotionDat.poseRightCamera);
  Vector3 cameraUp = _camera.GetUp();
  Vector3 cameraDir = _camera.GetDirection();
  _camera.SetDirectionAndUp(cameraUp, cameraDir); // Muzzle in Pointman space points to the Y direction, whereas in VBS space to the Z direction -> convert it

  // !! This condition is here to solve problem when soldier is sitting in vehicle and perform action "turn out"
  // !! In this case his pelvis is not at the correct position and we need to move weapon and camera to correct one.
  // !! When soldier is turned out while standing this is not an issue
  if (IsLowerBodySkeletonControlled() && !IsLowerBodySkeletonFullyControlled())
  {
    if (_pelvisPosition.Y() == 0 && GetPelvisPosition().Y() <= 0.0001)
    {
      _gun.SetPosition(_gun.GetPosition() - _lastPelvisPosition);
      _camera.SetPosition(_camera.GetPosition() - _lastPelvisPosition);
    }
  }

  // Get indicator whether optics should be displayed
  _enableOptics = (_serverMotionDat.bOptics != 0);

  // Get the camera zoom factor
  _zoom = _serverMotionDat.fZoom;

  // Update _oldServerMotionDat to reflect new state
  _oldServerMotionDat = _serverMotionDat;

  // Return relative pose
  return relChange;
}

#define FIND_BONE_ID(boneName, sourceName) \
  boneId = FindBone(#sourceName); \
  _vid_##sourceName = boneId;

void Pointman2Pose::GetSceleton()
{
  int boneId = -1; 

  // First parameter is not used
  FIND_BONE_ID(lHip,        leftupleg);
  FIND_BONE_ID(lHip,        leftuplegroll);
  FIND_BONE_ID(lKnee,       leftleg);
  FIND_BONE_ID(lKnee,       leftlegroll);
  FIND_BONE_ID(lAnkle,      leftfoot);
  FIND_BONE_ID(lMetatarsal, lefttoebase);
  FIND_BONE_ID(rHip,        rightupleg);
  FIND_BONE_ID(rHip,        rightuplegroll);
  FIND_BONE_ID(rKnee,       rightleg);
  FIND_BONE_ID(rKnee,       rightlegroll);
  FIND_BONE_ID(rAnkle,      rightfoot);
  FIND_BONE_ID(rMetatarsal, righttoebase);

  FIND_BONE_ID(pelvis,      pelvis);
  FIND_BONE_ID(spine1,      spine);
  FIND_BONE_ID(spine2,      spine1);
  FIND_BONE_ID(spine3,      spine2);
  FIND_BONE_ID(spine4,      spine3);
  FIND_BONE_ID(neck1,       neck);
  FIND_BONE_ID(neck2,       neck1);
  FIND_BONE_ID(skull,       head);
  FIND_BONE_ID(lClavicle,   leftshoulder);
  FIND_BONE_ID(lUpperArm,   leftarm);
  FIND_BONE_ID(lUpperArm,   leftarmroll);
  FIND_BONE_ID(lLowerArm,   leftforearm);
  FIND_BONE_ID(lLowerArm,   leftforearmroll);
  FIND_BONE_ID(lHand,       lefthand);
  FIND_BONE_ID(lThumbSeg1,  lefthandthumb1);
  FIND_BONE_ID(lThumbSeg2,  lefthandthumb2);
  FIND_BONE_ID(lThumbSeg3,  lefthandthumb3);
  FIND_BONE_ID(lIndexSeg1,  lefthandindex1);
  FIND_BONE_ID(lIndexSeg2,  lefthandindex2);
  FIND_BONE_ID(lIndexSeg3,  lefthandindex3);
  FIND_BONE_ID(lMiddleSeg1, lefthandmiddle1);
  FIND_BONE_ID(lMiddleSeg2, lefthandmiddle2);
  FIND_BONE_ID(lMiddleSeg3, lefthandmiddle3);
  FIND_BONE_ID(lRingSeg1,   lefthandring);
  FIND_BONE_ID(lRingSeg1,   lefthandring1);
  FIND_BONE_ID(lRingSeg2,   lefthandring2);
  FIND_BONE_ID(lRingSeg3,   lefthandring3);
  FIND_BONE_ID(lPinkySeg1,  lefthandpinky1);
  FIND_BONE_ID(lPinkySeg2,  lefthandpinky2);
  FIND_BONE_ID(lPinkySeg3,  lefthandpinky3);
  FIND_BONE_ID(rClavicle,   rightshoulder);
  FIND_BONE_ID(rUpperArm,   rightarm);
  FIND_BONE_ID(rUpperArm,   rightarmroll);
  FIND_BONE_ID(rLowerArm,   rightforearm);
  FIND_BONE_ID(rLowerArm,   rightforearmroll);
  FIND_BONE_ID(rHand,       righthand);
  FIND_BONE_ID(rThumbSeg1,  righthandthumb1);
  FIND_BONE_ID(rThumbSeg2,  righthandthumb2);
  FIND_BONE_ID(rThumbSeg3,  righthandthumb3);
  FIND_BONE_ID(rIndexSeg1,  righthandindex1);
  FIND_BONE_ID(rIndexSeg2,  righthandindex2);
  FIND_BONE_ID(rIndexSeg3,  righthandindex3);
  FIND_BONE_ID(rMiddleSeg1, righthandmiddle1);
  FIND_BONE_ID(rMiddleSeg2, righthandmiddle2);
  FIND_BONE_ID(rMiddleSeg3, righthandmiddle3);
  FIND_BONE_ID(rRingSeg1,   righthandring);
  FIND_BONE_ID(rRingSeg1,   righthandring1);
  FIND_BONE_ID(rRingSeg2,   righthandring2);
  FIND_BONE_ID(rRingSeg3,   righthandring3);
  FIND_BONE_ID(rPinkySeg1,  righthandpinky1);
  FIND_BONE_ID(rPinkySeg2,  righthandpinky2);
  FIND_BONE_ID(rPinkySeg3,  righthandpinky3);

  FIND_BONE_ID(weapon,      weapon);
  FIND_BONE_ID(launcher,    launcher);
}

#define CHECK_BONE(boneName,sourceName) \
  if (_vid_##sourceName == si) \
  { \
    mat = _##boneName; \
    mat.SetPosition(mat.GetPosition() + ptPelvisPos); \
    return true; \
  }

bool Pointman2Pose::GetExplicitBone(Matrix4 &mat, int si) const
{
  // Explicit pelvis position
  Vector3 ptPelvisPos = Vector3(0.0f, GetPelvisHeight(), 0.0f);

  if (IsLowerBodySkeletonFullyControlled())
  {
    CHECK_BONE(lHip,        leftupleg);
    CHECK_BONE(lHip,        leftuplegroll);
    CHECK_BONE(lKnee,       leftleg);
    CHECK_BONE(lKnee,       leftlegroll);
    CHECK_BONE(lAnkle,      leftfoot);
    CHECK_BONE(lMetatarsal, lefttoebase);
    CHECK_BONE(rHip,        rightupleg);
    CHECK_BONE(rHip,        rightuplegroll);
    CHECK_BONE(rKnee,       rightleg);
    CHECK_BONE(rKnee,       rightlegroll);
    CHECK_BONE(rAnkle,      rightfoot);
    CHECK_BONE(rMetatarsal, righttoebase);
  }
  else
  {
    ptPelvisPos = GetPelvisPosition();
  }

  // Upper body controlled, don't use upper body bones
  if (IsUpperBodyControlled()) return false;

  CHECK_BONE(pelvis,      pelvis);
  CHECK_BONE(spine1,      spine);
  CHECK_BONE(spine2,      spine1);
  CHECK_BONE(spine3,      spine2);
  CHECK_BONE(spine4,      spine3);
  CHECK_BONE(neck1,       neck);
  CHECK_BONE(neck2,       neck1);
  CHECK_BONE(skull,       head);
  CHECK_BONE(lClavicle,   leftshoulder);
  CHECK_BONE(lUpperArm,   leftarm);
  CHECK_BONE(lUpperArm,   leftarmroll);
  CHECK_BONE(lLowerArm,   leftforearm);
  CHECK_BONE(lLowerArm,   leftforearmroll);
  CHECK_BONE(lHand,       lefthand);
  CHECK_BONE(lThumbSeg1,  lefthandthumb1);
  CHECK_BONE(lThumbSeg2,  lefthandthumb2);
  CHECK_BONE(lThumbSeg3,  lefthandthumb3);
  CHECK_BONE(lIndexSeg1,  lefthandindex1);
  CHECK_BONE(lIndexSeg2,  lefthandindex2);
  CHECK_BONE(lIndexSeg3,  lefthandindex3);
  CHECK_BONE(lMiddleSeg1, lefthandmiddle1);
  CHECK_BONE(lMiddleSeg2, lefthandmiddle2);
  CHECK_BONE(lMiddleSeg3, lefthandmiddle3);
  CHECK_BONE(lRingSeg1,   lefthandring);
  CHECK_BONE(lRingSeg1,   lefthandring1);
  CHECK_BONE(lRingSeg2,   lefthandring2);
  CHECK_BONE(lRingSeg3,   lefthandring3);
  CHECK_BONE(lPinkySeg1,  lefthandpinky1);
  CHECK_BONE(lPinkySeg2,  lefthandpinky2);
  CHECK_BONE(lPinkySeg3,  lefthandpinky3);
  CHECK_BONE(rClavicle,   rightshoulder);
  CHECK_BONE(rUpperArm,   rightarm);
  CHECK_BONE(rUpperArm,   rightarmroll);
  CHECK_BONE(rLowerArm,   rightforearm);
  CHECK_BONE(rLowerArm,   rightforearmroll);
  CHECK_BONE(rHand,       righthand);
  CHECK_BONE(rThumbSeg1,  righthandthumb1);
  CHECK_BONE(rThumbSeg2,  righthandthumb2);
  CHECK_BONE(rThumbSeg3,  righthandthumb3);
  CHECK_BONE(rIndexSeg1,  righthandindex1);
  CHECK_BONE(rIndexSeg2,  righthandindex2);
  CHECK_BONE(rIndexSeg3,  righthandindex3);
  CHECK_BONE(rMiddleSeg1, righthandmiddle1);
  CHECK_BONE(rMiddleSeg2, righthandmiddle2);
  CHECK_BONE(rMiddleSeg3, righthandmiddle3);
  CHECK_BONE(rRingSeg1,   righthandring);
  CHECK_BONE(rRingSeg1,   righthandring1);
  CHECK_BONE(rRingSeg2,   righthandring2);
  CHECK_BONE(rRingSeg3,   righthandring3);
  CHECK_BONE(rPinkySeg1,  righthandpinky1);
  CHECK_BONE(rPinkySeg2,  righthandpinky2);
  CHECK_BONE(rPinkySeg3,  righthandpinky3);

  // Bone is not explicit
  return false;
}

bool Pointman2Pose::IsUpperBodyControlled() const
{
  return _serverMotionDat.bMotionCtrlUpper == V2_Interface::MOTION_CTRL_PARTIAL;
}

void Pointman2Pose::UpdateAimObject(BYTE aimObjectType)
{
  int weaponMask = 0;
  V2_Interface::_V2_HandGrip *leftHandGrip  = NULL;
  V2_Interface::_V2_HandGrip *rightHandGrip = NULL;
  V2_Interface::_V2_6D *stowPos = NULL;

  switch ( aimObjectType )
  {
  case V2_Interface::HELD_OBJECT_PRIMARY_WEAPON:
    weaponMask = MaskSlotPrimary;
    leftHandGrip  = &_primaryGripLeft;
    rightHandGrip = &_primaryGripRight;
    stowPos = &_primaryStowPos;
    break;

  case V2_Interface::HELD_OBJECT_SECONDARY_WEAPON:
    weaponMask = MaskSlotSecondary;
    leftHandGrip  = &_secondaryGripLeft;
    rightHandGrip = &_secondaryGripRight;
    stowPos = &_secondaryStowPos;
    break;

  case V2_Interface::HELD_OBJECT_PISTOL_WEAPON:
    weaponMask = MaskSlotHandGun;
    leftHandGrip  = &_pistolGripLeft;
    rightHandGrip = &_pistolGripRight;
    break;

  case V2_Interface::HELD_OBJECT_BINOCULARS:
    weaponMask = MaskSlotBinocular;
    leftHandGrip  = &_binocularsGripLeft;
    rightHandGrip = &_binocularsGripRight;
    break;
  }

  // Aim object
  V2_Interface::_V2_AimObject ao;

  ao.bObjectType = aimObjectType;
  ao.bStowable   = (aimObjectType != V2_Interface::HELD_OBJECT_BINOCULARS) && (aimObjectType != V2_Interface::HELD_OBJECT_PISTOL_WEAPON);
  ao.bGripType   = V2_Interface::LEFT | V2_Interface::RIGHT;

  ao.gripLeftHand.bHand  = V2_Interface::LEFT;
  ao.gripRightHand.bHand = V2_Interface::RIGHT;

  char* weaponType = GetWeaponType(weaponMask);

  char weaponName[256];
  GetWeaponName(weaponName, weaponType);

  // We are using weaponName to detect if desired weapon exists
  if (strlen(weaponName) > 0)
  {
    // Setting gun properties
    {
      strncpy (ao.strDescription, weaponName, 30); // char strDescription[30];

      Vector3 bbMax;
      Vector3 bbMin;
      GetWeaponBBox(bbMin, bbMax, weaponType);

      Vector3 cameraPos = GetMuzzleCameraPos(weaponType);

      Vector3 muzzlePos = GetMuzzlePos(weaponType);

      float weaponWeight = GetWeaponWeight(weaponType);

      // Bounding box
      VECTOR_TO_V2_3DT(ao.boundBox.posMin, Vector3(bbMin.Z(), bbMin.Y(), bbMin.X()));
      VECTOR_TO_V2_3DT(ao.boundBox.posMax, Vector3(bbMax.Z(), bbMax.Y(), bbMax.X()));

      // Stow position
      if ( stowPos != NULL ) 
      {
        ao.stowPos.poseObject  = *stowPos;
        ao.stowPos.wHostBoneId = V2_Interface::AVATAR_SPINE_2;
        // ao.stowPos.dwTransitionTime - cannot be set

        ao.quickStowPos.poseObject  = *stowPos;
        ao.quickStowPos.wHostBoneId = V2_Interface::AVATAR_SPINE_2;
        // ao.quickStowPos.dwTransitionTime - cannot be set
      }

      // Mass
      ao.weight = weaponWeight;

      // Important points on the gun
      VECTOR_TO_V2_3DT(ao.offsetSight,     Vector3(cameraPos.Z(), cameraPos.Y(), -cameraPos.X()));
      VECTOR_TO_V2_3DT(ao.offsetBarrelTip, Vector3(muzzlePos.Z(), muzzlePos.Y(), -muzzlePos.X()));
      VECTOR_TO_V2_3DT(ao.offsetButtStock, Vector3(muzzlePos.Z(), 0, -bbMax.X()));
      
      float reloadTime = GetWeaponReloadTime(weaponType);
      ao.opMain.cyclingRate = (reloadTime > 0) ? 60 / reloadTime : 0.f;

      ao.opMain.bBurstCount = 1;
      ao.opMain.bFullAuto   = V2_Interface::FULL_AUTO_NONE;

      int maxBurst = 1;
      bool fullAuto = false;
      GetWeaponMaxBurst(weaponType, maxBurst, fullAuto);
      ao.opMain.bBurstCount = maxBurst;
      if (fullAuto) ao.opMain.bFullAuto = V2_Interface::FULL_AUTO_HOLD;

      float opticsZoomMin;
      float opticsZoomMax;
      GetMuzzleOpticsZoom(opticsZoomMin, opticsZoomMax, weaponType);

      // Get "Actual Optic Zoom" (Pointman term.)
      float actualZoomMin = (opticsZoomMax != 0) ? 0.25 / opticsZoomMax : 1;
      float actualZoomMax = (opticsZoomMin != 0) ? 0.25 / opticsZoomMin : 1;

      // Hardcoded zoom scaling factors 
      float zoomFactorIron  = 2.714;
      float zoomFactorScope = 3.8;

      // As requested by Pointman
      ao.opMain.dMinOpticZoom = (actualZoomMin <= 1) ? zoomFactorIron : actualZoomMin * zoomFactorScope;
      ao.opMain.dMaxOpticZoom =  actualZoomMax * zoomFactorScope;

      if ( !V2_Interface::CheckZoomRange( ao.opMain.dMinOpticZoom, ao.opMain.dMaxOpticZoom ) ) {
        ao.opMain.dMinOpticZoom = ao.opMain.dMaxOpticZoom = 1.f;
      }

      ao.opMain.bOpticsType = (actualZoomMin <= 1) ? V2_Interface::OPTICS_IRON_SIGHT : V2_Interface::OPTICS_SCOPE;
      ao.opMain.bTriggered  = (aimObjectType != V2_Interface::HELD_OBJECT_BINOCULARS);
      ao.opMain.bReloadable = (aimObjectType == V2_Interface::HELD_OBJECT_PRIMARY_WEAPON) || 
        (aimObjectType != V2_Interface::HELD_OBJECT_PISTOL_WEAPON);

      // Ammo load position
      // ao.opMain.ammoLoadPos.poseObject - cannot be set, nothing similar in VBS
      ao.opMain.ammoLoadPos.wHostId = aimObjectType;
      
      int dummyBurst;
      float transitionTime;
      GetMuzzleInfo(weaponType, 0, 0, dummyBurst, transitionTime);
      ao.opMain.ammoLoadPos.dwTransitionTime = transitionTime * 1000;    // in miliseconds

      // Attached subsystem present
      if ( GetMuzzleCount() > 1 )
      {
        ao.bAttachedSubsys = TRUE;

        int burst;
        float reloadTime;

        if (GetMuzzleInfo(weaponType, 1, 0, burst, reloadTime))
        {
          ao.opAttached.bBurstCount =  burst;
          ao.opAttached.cyclingRate = (reloadTime > 0) ? 60 / reloadTime : 0.f;

          ao.opAttached.bFullAuto   = V2_Interface::FULL_AUTO_NONE;
          ao.opAttached.bOpticsType = V2_Interface::OPTICS_NONE;
          ao.opAttached.bTriggered  = TRUE;
          ao.opAttached.bReloadable = FALSE;
        }
      }

      // Hardcoded properties
      ao.bBarrel = (aimObjectType != V2_Interface::HELD_OBJECT_BINOCULARS);
      ao.bStock  = (aimObjectType != V2_Interface::HELD_OBJECT_BINOCULARS) && (aimObjectType != V2_Interface::HELD_OBJECT_PISTOL_WEAPON);
    }

    // Setting hand pose - predefinied hand grips - set once, at the beginning (first calls
    // of V2_CmdSet(PrimaryWeapon|SecondaryWeapon|PistolWeapon|Binoculars))
    if ( leftHandGrip != NULL ) 
    {
      ao.gripLeftHand = *leftHandGrip;
    }

    if ( rightHandGrip != NULL ) 
    {
      ao.gripRightHand = *rightHandGrip;
    }
  }

  if (!ao.CheckValues()) RptF("{error} ao.CheckValues()");

  switch ( aimObjectType )
  {
  case V2_Interface::HELD_OBJECT_PRIMARY_WEAPON:
    // Send primary weapon to the Pointman
    SetPrimaryWeapon( ao );
    break;

  case V2_Interface::HELD_OBJECT_SECONDARY_WEAPON:
    // Send secondary weapon to the Pointman
    SetSecondaryWeapon( ao );
    break;

  case V2_Interface::HELD_OBJECT_PISTOL_WEAPON:
    // Send pistol weapon to the Pointman
    SetPistolWeapon( ao );
    break;

  case V2_Interface::HELD_OBJECT_BINOCULARS:
    // Send binoculars to the Pointman
    SetBinoculars( ao );
    break;
  }
}

void Pointman2Pose::SetPrimaryWeapon(V2_Interface::_V2_AimObject &ao)
{
  if (!(_lastAO == ao))
  {
    // Setting Throw subsystem, as advised by Patricia, to solve error 20 in following SetMotionState, but there is still the error
    V2_Interface::_V2_FireState fs = _lastFS;
    fs.bSubsystem = V2_Interface::SUBSYS_THROW;
    SetFireState(fs);

    // Set mobility state to suspended, to allow setting the primary weapon (as specified in the Pointman interface documentation), otherwise we get an error inside setPrimaryWeapon call
    V2_Interface::_V2_MotionState ms = _lastMS;
    ms.bMobilityState = V2_Interface::MOBILITY_SUSPENDED;
    SetMotionState(ms);

    _lastAO = ao;
    if (!V2_CmdSetPrimaryWeapon(ao))
    {
      RptF("{error} Pointman: Failed to send primary object from the client");
    }
  }
}

void Pointman2Pose::SetSecondaryWeapon( V2_Interface::_V2_AimObject &ao )
{
  if ( !(_lastAOS == ao) )
  {
    _lastAOS = ao;

    if ( !V2_CmdSetSecondaryWeapon( ao ) ) 
    {
      RptF("{error} Pointman: Failed to send secondary weapon from the client");
    }
  }
}

void Pointman2Pose::SetPistolWeapon( V2_Interface::_V2_AimObject &ao )
{
  if ( !(_lastAOP == ao) )
  {
    _lastAOP = ao;

    if ( !V2_CmdSetPistolWeapon( ao ) ) 
    {
      RptF("{error} Pointman: Failed to send pistol weapon from the client");
    }
  }
}

void Pointman2Pose::SetBinoculars( V2_Interface::_V2_AimObject &ao )
{
  if ( !(_lastAOB == ao) )
  {
    _lastAOB = ao;

    if ( _lastMS.bHeldObject == V2_Interface::HELD_OBJECT_BINOCULARS )
    {
      return;
    }

    if ( !V2_CmdSetBinoculars( ao ) ) 
    {
      RptF("{error} Pointman: Failed to send binoculars from the client");
    }
  }
}

void Pointman2Pose::SetFireState(V2_Interface::_V2_FireState &fs)
{
  if (!(_lastFS == fs) && _lastMS.bHeldObject != V2_Interface::HELD_OBJECT_BINOCULARS)
  {
    if (!fs.CheckValues())
    {
      RptF("{error} fs.CheckValues()");
    }
    _lastFS = fs;
    if (!V2_CmdSetFireState(fs))
    {
      RptF("{error} Pointman: Failed to send fire state");
    }
  }
}

void Pointman2Pose::SetMotionState(V2_Interface::_V2_MotionState &ms)
{
  if (IsLowerBodySkeletonFullyControlled())
  {
	 _lastMS.bBodyPosture = ms.bBodyPosture;
  }
  if (IsUpperBodyFullyControlled())
  {
	 _lastMS.bAiming = ms.bAiming;
	 _lastMS.bOptics = ms.bOptics;
  }
  if (!(_lastMS == ms))
  {
    if ( ms.bHeldObject == V2_Interface::HELD_OBJECT_HANDS_FREE &&
      _lastFS.bSubsystem != V2_Interface::SUBSYS_THROW )
    {
      V2_Interface::_V2_FireState fs = _lastFS;
      fs.bSubsystem = V2_Interface::SUBSYS_THROW;

      fs.ammo.numRounds = 0;
      fs.ammo.bMagazine = 0;
      fs.ammo.bGripType = V2_Interface::RIGHT;

      SetFireState( fs );
    }

    _lastMS = ms;
    if (!V2_CmdSetMotionState(ms))
    {
      RptF("{error} Pointman: Failed to send motion state from the client");
    }
  }
}

void Pointman2Pose::SetUserView(BYTE &uv)
{
  if (!(_lastUV == uv))
  {
    _lastUV = uv;
    if (!V2_CmdSetUserView(uv))
    {
      RptF("{error} Pointman: Failed to send user view");
    }
  }
}

bool Pointman2Pose::IsGunControlled() const
{
  return (_serverMotionDat.bMotionCtrlUpper == V2_Interface::MOTION_CTRL_FULL) && (_serverMotionDat.bHeldObject == V2_Interface::HELD_OBJECT_PRIMARY_WEAPON);
}

bool Pointman2Pose::IsLauncherControlled() const
{
  return (_serverMotionDat.bMotionCtrlUpper == V2_Interface::MOTION_CTRL_FULL) && 
    (_serverMotionDat.bHeldObject == V2_Interface::HELD_OBJECT_SECONDARY_WEAPON);
}

bool Pointman2Pose::IsPistolControlled() const
{
  return (_serverMotionDat.bMotionCtrlUpper == V2_Interface::MOTION_CTRL_FULL) && 
    (_serverMotionDat.bHeldObject == V2_Interface::HELD_OBJECT_PISTOL_WEAPON);
}

bool Pointman2Pose::IsBinocularsControlled() const
{
  return (_serverMotionDat.bMotionCtrlUpper == V2_Interface::MOTION_CTRL_FULL) && 
    (_serverMotionDat.bHeldObject == V2_Interface::HELD_OBJECT_BINOCULARS);
}

#define CHECK_PROXY_BONE(boneName) \
  (_vid_##boneName == boneIndex) 
 
void Pointman2Pose::AnimateProxyMatrix(Matrix4 &mat, int boneIndex)
{
  // primary gun (weapon) || launcher (launcher) || handgun (righthand) || binoculars (lefthand)
  if ((IsGunControlled()        && CHECK_PROXY_BONE(weapon))    || 
      (IsLauncherControlled()   && CHECK_PROXY_BONE(launcher))  || 
      (IsPistolControlled()     && CHECK_PROXY_BONE(righthand)) ||
      (IsBinocularsControlled() && CHECK_PROXY_BONE(lefthand)))
  {
    // use custom transformation
    mat = GetGun();

    // just weapons (no binocular)
    if (!CHECK_PROXY_BONE(lefthand))
    {
      // weapon models must be rotated 90 degrees along Y axis to point forward
      mat = mat * Matrix4().GetRotatedY(-H_PI * 0.5f);
    }
  }
  else if (IsUpperBodyFullyControlled() && (CHECK_PROXY_BONE(weapon) || CHECK_PROXY_BONE(launcher)))
  {
    // primary || secondary weapon on back - needs to be attached to the spine
    GetStowPosition(mat, CHECK_PROXY_BONE(weapon));
  } 
 
}

void Pointman2Pose::ModifyBone(Matrix4 &mat, int skeletonIndex)
{
  if (IsLowerBodySkeletonControlled())
  {
    if (IsLowerBodySkeletonFullyControlled())
    {
      // Consider pelvis from Pointman and move the matrix to the new position
      Vector3 ptPelvisPos = Vector3(0.0f, GetPelvisHeight(), 0.0f);
      mat.SetPosition((mat.GetPosition() + ptPelvisPos - _rtmPelvisOffset) * _explicitSkeletonFactor + mat.GetPosition() * (1.0f - _explicitSkeletonFactor));
    }

    // Replace the bone, if it is explicitly set
    Matrix4 m;
    if (GetExplicitBone(m, skeletonIndex))
    {
      mat = m * _explicitSkeletonFactor + mat * (1.0 - _explicitSkeletonFactor);
    }  
    AnimateProxyMatrix(mat, skeletonIndex);
  }
}

void Pointman2Pose::GetStowPosition( Matrix4 &mat, bool isPrimary ) const
{
  Matrix4 stowOffset, spine2; 
  V2_6D_TO_MATRIX_PAT( stowOffset, isPrimary ? _primaryStowPos : _secondaryStowPos );
  V2_6D_TO_MATRIX_PAT( spine2, _serverMotionDat.poseUpperBody.poseTorso.poseSpine2 );

  mat = spine2 * stowOffset;
}

bool Pointman2Pose::IsUpperBodyFullyControlled() const
{
  return _serverMotionDat.bMotionCtrlUpper == V2_Interface::MOTION_CTRL_FULL;
}

#define SET_HAND_BONE(bone, transX, transY, transZ, rotX, rotY, rotZ) \
{ \
  bone.tx = transX; bone.ty = transY; bone.tz = transZ; \
  bone.rx = rotX; bone.ry = rotY; bone.rz = rotZ; \
}

void Pointman2Pose::DefineHandGrips()
{
  _primaryGripLeft.bHand = V2_Interface::LEFT;

  SET_HAND_BONE(_primaryGripLeft.poseHand,                   -0.037f, 0.066f, 0.046f, 101.880f, -24.885f,  152.533f);
  SET_HAND_BONE(_primaryGripLeft.poseThumb.poseBone1,        -0.056f, 0.085f, 0.053f,  58.329f, -55.923f, -153.980f);
  SET_HAND_BONE(_primaryGripLeft.poseThumb.poseBone2,        -0.055f, 0.134f, 0.068f,  69.312f, -51.047f, -179.622f);
  SET_HAND_BONE(_primaryGripLeft.poseThumb.poseBone3,        -0.045f, 0.167f, 0.076f,  67.238f, -57.878f, -179.562f);
  SET_HAND_BONE(_primaryGripLeft.poseIndexFinger.poseBone1,  -0.023f, 0.160f, 0.035f,  91.848f, -33.962f,  129.301f);
  SET_HAND_BONE(_primaryGripLeft.poseIndexFinger.poseBone2,   0.008f, 0.186f, 0.034f,  29.006f, -26.983f,  143.784f);
  SET_HAND_BONE(_primaryGripLeft.poseIndexFinger.poseBone3,   0.023f, 0.190f, 0.053f,  -9.458f, -18.861f,  143.895f);
  SET_HAND_BONE(_primaryGripLeft.poseMiddleFinger.poseBone1, -0.001f, 0.152f, 0.028f,  44.158f, -28.170f,  140.715f);
  SET_HAND_BONE(_primaryGripLeft.poseMiddleFinger.poseBone2,  0.029f, 0.166f, 0.055f, -24.766f, -26.208f,  140.679f);
  SET_HAND_BONE(_primaryGripLeft.poseMiddleFinger.poseBone3,  0.030f, 0.151f, 0.076f, -67.266f, -24.769f,  139.540f);
  SET_HAND_BONE(_primaryGripLeft.poseRingFinger.poseBone1,    0.015f, 0.139f, 0.025f,  33.702f, -14.743f,  121.814f);
  SET_HAND_BONE(_primaryGripLeft.poseRingFinger.poseBone2,    0.035f, 0.142f, 0.054f, -19.724f, -24.436f,  121.535f);
  SET_HAND_BONE(_primaryGripLeft.poseRingFinger.poseBone3,    0.033f, 0.130f, 0.076f, -61.874f, -27.536f,  129.143f);
  SET_HAND_BONE(_primaryGripLeft.posePinkyFinger.poseBone1,   0.031f, 0.118f, 0.025f,  19.200f, -10.096f,  118.972f);
  SET_HAND_BONE(_primaryGripLeft.posePinkyFinger.poseBone2,   0.041f, 0.119f, 0.050f, -12.751f, -18.157f,  117.722f);
  SET_HAND_BONE(_primaryGripLeft.posePinkyFinger.poseBone3,   0.039f, 0.110f, 0.069f, -76.140f, -28.356f,  125.227f);

  _primaryGripRight.bHand = V2_Interface::RIGHT;

  SET_HAND_BONE(_primaryGripRight.poseHand,                    0.032f, -0.210f, -0.020f,  -44.288f, 85.501f,   46.714f);
  SET_HAND_BONE(_primaryGripRight.poseThumb.poseBone1,         0.033f, -0.203f,  0.006f,  -67.629f, 33.298f,   52.319f);
  SET_HAND_BONE(_primaryGripRight.poseThumb.poseBone2,         0.001f, -0.166f,  0.023f,  -82.452f, 48.400f,   39.318f);
  SET_HAND_BONE(_primaryGripRight.poseThumb.poseBone3,        -0.017f, -0.137f,  0.026f, -112.612f, 38.316f,    9.115f);
  SET_HAND_BONE(_primaryGripRight.poseIndexFinger.poseBone1,   0.033f, -0.119f,  0.009f,  -62.012f, 77.592f,   38.796f);
  SET_HAND_BONE(_primaryGripRight.poseIndexFinger.poseBone2,   0.025f, -0.080f,  0.013f,  -61.588f, 76.739f,   40.126f);
  SET_HAND_BONE(_primaryGripRight.poseIndexFinger.poseBone3,   0.020f, -0.057f,  0.016f,  -65.340f, 78.731f,   36.649f);
  SET_HAND_BONE(_primaryGripRight.poseMiddleFinger.poseBone1,  0.030f, -0.115f, -0.014f,    4.807f, 83.152f,  175.025f);
  SET_HAND_BONE(_primaryGripRight.poseMiddleFinger.poseBone2, -0.012f, -0.108f, -0.009f,  -64.829f, 81.277f, -174.237f);
  SET_HAND_BONE(_primaryGripRight.poseMiddleFinger.poseBone3, -0.021f, -0.133f, -0.008f, -102.755f, 80.732f, -161.693f);
  SET_HAND_BONE(_primaryGripRight.poseRingFinger.poseBone1,    0.026f, -0.121f, -0.035f,   71.365f, 63.550f, -109.878f);
  SET_HAND_BONE(_primaryGripRight.poseRingFinger.poseBone2,   -0.009f, -0.119f, -0.029f,   20.504f, 72.832f,  -97.612f);
  SET_HAND_BONE(_primaryGripRight.poseRingFinger.poseBone3,   -0.020f, -0.140f, -0.023f,  -44.437f, 81.108f, -114.851f);
  SET_HAND_BONE(_primaryGripRight.posePinkyFinger.poseBone1,   0.021f, -0.131f, -0.058f,   56.647f, 56.703f, -121.228f);
  SET_HAND_BONE(_primaryGripRight.posePinkyFinger.poseBone2,  -0.004f, -0.131f, -0.049f,   20.392f, 64.280f, -114.112f);
  SET_HAND_BONE(_primaryGripRight.posePinkyFinger.poseBone3,  -0.018f, -0.144f, -0.041f,  -24.365f, 70.762f, -116.079f);

  _secondaryGripLeft.bHand = V2_Interface::LEFT;

  SET_HAND_BONE(_secondaryGripLeft.poseHand,                   -0.510f, -0.239f,  0.013f,  146.379f,  -7.402f,   81.727f);
  SET_HAND_BONE(_secondaryGripLeft.poseThumb.poseBone1,        -0.504f, -0.213f,  0.008f,  -59.009f, -58.061f, -120.753f);
  SET_HAND_BONE(_secondaryGripLeft.poseThumb.poseBone2,        -0.454f, -0.216f,  0.022f,  -95.430f, -69.294f,  -93.348f);
  SET_HAND_BONE(_secondaryGripLeft.poseThumb.poseBone3,        -0.420f, -0.221f,  0.021f, -104.082f, -62.002f,  -88.221f);
  SET_HAND_BONE(_secondaryGripLeft.poseIndexFinger.poseBone1,  -0.458f, -0.213f, -0.063f,  116.243f, -16.503f,   75.904f);
  SET_HAND_BONE(_secondaryGripLeft.poseIndexFinger.poseBone2,  -0.421f, -0.217f, -0.079f,   73.945f, -17.711f,   86.746f);
  SET_HAND_BONE(_secondaryGripLeft.poseIndexFinger.poseBone3,  -0.399f, -0.220f, -0.073f,   48.328f, -14.291f,   90.262f);
  SET_HAND_BONE(_secondaryGripLeft.poseMiddleFinger.poseBone1, -0.457f, -0.236f, -0.066f,  105.838f,  -0.194f,   80.440f);
  SET_HAND_BONE(_secondaryGripLeft.poseMiddleFinger.poseBone2, -0.416f, -0.243f, -0.077f,   63.343f,   0.323f,   81.445f);
  SET_HAND_BONE(_secondaryGripLeft.poseMiddleFinger.poseBone3, -0.392f, -0.247f, -0.066f,   38.233f,   0.823f,   82.551f);
  SET_HAND_BONE(_secondaryGripLeft.poseRingFinger.poseBone1,   -0.459f, -0.257f, -0.061f,  120.411f,  16.338f,   90.731f);
  SET_HAND_BONE(_secondaryGripLeft.poseRingFinger.poseBone2,   -0.428f, -0.262f, -0.077f,   81.865f,  13.667f,   84.442f);
  SET_HAND_BONE(_secondaryGripLeft.poseRingFinger.poseBone3,   -0.404f, -0.263f, -0.073f,   55.583f,   8.918f,   81.676f);
  SET_HAND_BONE(_secondaryGripLeft.posePinkyFinger.poseBone1,  -0.463f, -0.280f, -0.049f,  138.178f,   6.352f,   96.949f);
  SET_HAND_BONE(_secondaryGripLeft.posePinkyFinger.poseBone2,  -0.445f, -0.280f, -0.068f,  106.735f,   8.232f,   89.988f);
  SET_HAND_BONE(_secondaryGripLeft.posePinkyFinger.poseBone3,  -0.425f, -0.281f, -0.074f,   74.697f,   5.696f,   85.498f);

  _secondaryGripRight.bHand = V2_Interface::RIGHT;

  SET_HAND_BONE(_secondaryGripRight.poseHand,                   -0.413f, -0.074f,  0.066f, -133.631f, -0.000f,  -79.177f);
  SET_HAND_BONE(_secondaryGripRight.poseThumb.poseBone1,        -0.410f, -0.047f,  0.062f, -165.052f, 28.329f,   20.747f);
  SET_HAND_BONE(_secondaryGripRight.poseThumb.poseBone2,        -0.437f, -0.043f,  0.018f,  177.690f,  8.854f,    9.924f);
  SET_HAND_BONE(_secondaryGripRight.poseThumb.poseBone3,        -0.442f, -0.045f, -0.016f,  175.773f,  0.049f,   18.877f);
  SET_HAND_BONE(_secondaryGripRight.poseIndexFinger.poseBone1,  -0.349f, -0.038f,  0.005f, -171.098f, -9.280f,  -69.167f);
  SET_HAND_BONE(_secondaryGripRight.poseIndexFinger.poseBone2,  -0.341f, -0.042f, -0.035f,  149.062f,  1.249f,  -70.946f);
  SET_HAND_BONE(_secondaryGripRight.poseIndexFinger.poseBone3,  -0.353f, -0.045f, -0.055f,  123.368f,  3.816f,  -75.281f);
  SET_HAND_BONE(_secondaryGripRight.poseMiddleFinger.poseBone1, -0.346f, -0.061f,  0.001f,  176.535f, -3.399f,  -85.344f);
  SET_HAND_BONE(_secondaryGripRight.poseMiddleFinger.poseBone2, -0.348f, -0.064f, -0.043f,  136.123f, -2.636f,  -86.140f);
  SET_HAND_BONE(_secondaryGripRight.poseMiddleFinger.poseBone3, -0.366f, -0.066f, -0.061f,  112.339f, -1.725f,  -86.915f);
  SET_HAND_BONE(_secondaryGripRight.poseRingFinger.poseBone1,   -0.349f, -0.082f,  0.001f, -171.799f, -3.588f, -107.782f);
  SET_HAND_BONE(_secondaryGripRight.poseRingFinger.poseBone2,   -0.346f, -0.086f, -0.034f,  152.339f, -8.017f, -103.140f);
  SET_HAND_BONE(_secondaryGripRight.poseRingFinger.poseBone3,   -0.358f, -0.086f, -0.055f,  127.518f, -9.307f,  -98.248f);
  SET_HAND_BONE(_secondaryGripRight.posePinkyFinger.poseBone1,  -0.359f, -0.106f,  0.004f, -161.789f,  2.534f, -106.950f);
  SET_HAND_BONE(_secondaryGripRight.posePinkyFinger.poseBone2,  -0.351f, -0.107f, -0.021f,  172.243f, -3.116f, -105.284f);
  SET_HAND_BONE(_secondaryGripRight.posePinkyFinger.poseBone3,  -0.354f, -0.108f, -0.042f,  144.951f, -6.274f, -103.332f);

  _pistolGripLeft.bHand = V2_Interface::LEFT;

  SET_HAND_BONE(_pistolGripLeft.poseHand,                   -0.448f, -0.097f,  0.018f, 135.281f,  -1.349f,  94.665f);
  SET_HAND_BONE(_pistolGripLeft.poseThumb.poseBone1,        -0.446f, -0.071f,  0.011f, 135.245f, -64.518f,  99.169f);
  SET_HAND_BONE(_pistolGripLeft.poseThumb.poseBone2,        -0.415f, -0.032f, -0.005f, 131.366f, -32.621f,  72.549f);
  SET_HAND_BONE(_pistolGripLeft.poseThumb.poseBone3,        -0.387f, -0.029f, -0.024f, 130.076f, -25.517f,  53.945f);
  SET_HAND_BONE(_pistolGripLeft.poseIndexFinger.poseBone1,  -0.387f, -0.067f, -0.050f,  73.866f, -16.969f,  99.871f);
  SET_HAND_BONE(_pistolGripLeft.poseIndexFinger.poseBone2,  -0.348f, -0.064f, -0.039f,  29.716f, -10.270f, 109.021f);
  SET_HAND_BONE(_pistolGripLeft.poseIndexFinger.poseBone3,  -0.336f, -0.064f, -0.019f,   3.553f,  -5.104f, 109.160f);
  SET_HAND_BONE(_pistolGripLeft.poseMiddleFinger.poseBone1, -0.381f, -0.090f, -0.050f,  59.000f,   3.933f,  94.515f);
  SET_HAND_BONE(_pistolGripLeft.poseMiddleFinger.poseBone2, -0.345f, -0.086f, -0.028f,  12.646f,   5.131f,  94.874f);
  SET_HAND_BONE(_pistolGripLeft.poseMiddleFinger.poseBone3, -0.339f, -0.083f, -0.002f, -15.171f,   6.396f,  95.167f);
  SET_HAND_BONE(_pistolGripLeft.poseRingFinger.poseBone1,   -0.381f, -0.110f, -0.042f,  54.944f,  24.449f,  79.537f);
  SET_HAND_BONE(_pistolGripLeft.poseRingFinger.poseBone2,   -0.350f, -0.107f, -0.023f,  19.273f,  18.092f,  79.181f);
  SET_HAND_BONE(_pistolGripLeft.poseRingFinger.poseBone3,   -0.341f, -0.104f, -0.001f,  -4.296f,  13.493f,  80.898f);
  SET_HAND_BONE(_pistolGripLeft.posePinkyFinger.poseBone1,  -0.382f, -0.133f, -0.029f,  60.466f,  35.723f,  82.237f);
  SET_HAND_BONE(_pistolGripLeft.posePinkyFinger.poseBone2,  -0.358f, -0.128f, -0.018f,  21.549f,  28.580f,  77.489f);
  SET_HAND_BONE(_pistolGripLeft.posePinkyFinger.poseBone3,  -0.349f, -0.121f, -0.001f, -14.254f,  23.064f,  80.042f);

  _pistolGripRight.bHand = V2_Interface::RIGHT;

  SET_HAND_BONE(_pistolGripRight.poseHand,                   -0.413f, -0.074f,  0.066f, -133.631f,   0.000f,  -79.177f);
  SET_HAND_BONE(_pistolGripRight.poseThumb.poseBone1,        -0.410f, -0.047f,  0.062f, -169.523f,  -4.015f,   -2.401f);
  SET_HAND_BONE(_pistolGripRight.poseThumb.poseBone2,        -0.406f, -0.038f,  0.011f,  178.598f, -29.355f,  -12.923f);
  SET_HAND_BONE(_pistolGripRight.poseThumb.poseBone3,        -0.390f, -0.043f, -0.019f,  169.705f, -41.479f,   -1.818f);
  SET_HAND_BONE(_pistolGripRight.poseIndexFinger.poseBone1,  -0.349f, -0.038f,  0.005f, -149.693f,  -9.903f,  -75.947f);
  SET_HAND_BONE(_pistolGripRight.poseIndexFinger.poseBone2,  -0.328f, -0.038f, -0.029f, -150.827f,  -9.604f,  -75.056f);
  SET_HAND_BONE(_pistolGripRight.poseIndexFinger.poseBone3,  -0.316f, -0.039f, -0.049f, -150.906f, -10.236f,  -77.111f);
  SET_HAND_BONE(_pistolGripRight.poseMiddleFinger.poseBone1, -0.345f, -0.061f,  0.001f,  140.154f, -12.056f,  -73.892f);
  SET_HAND_BONE(_pistolGripRight.poseMiddleFinger.poseBone2, -0.370f, -0.075f, -0.032f,   65.552f, -11.911f,  -76.145f);
  SET_HAND_BONE(_pistolGripRight.poseMiddleFinger.poseBone3, -0.393f, -0.079f, -0.021f,   18.356f, -13.118f,  -77.711f);
  SET_HAND_BONE(_pistolGripRight.poseRingFinger.poseBone1,   -0.350f, -0.082f,  0.001f,  141.359f, -29.462f,  -99.366f);
  SET_HAND_BONE(_pistolGripRight.poseRingFinger.poseBone2,   -0.374f, -0.091f, -0.023f,   77.636f, -29.143f,  -88.242f);
  SET_HAND_BONE(_pistolGripRight.poseRingFinger.poseBone3,   -0.398f, -0.090f, -0.017f,   32.455f, -22.373f,  -83.324f);
  SET_HAND_BONE(_pistolGripRight.posePinkyFinger.poseBone1,  -0.359f, -0.106f,  0.004f,  141.952f, -23.681f, -107.745f);
  SET_HAND_BONE(_pistolGripRight.posePinkyFinger.poseBone2,  -0.378f, -0.109f, -0.015f,   88.286f, -29.188f,  -97.496f);
  SET_HAND_BONE(_pistolGripRight.posePinkyFinger.poseBone3,  -0.399f, -0.105f, -0.013f,   35.453f, -25.174f,  -90.163f);

  _binocularsGripLeft.bHand = V2_Interface::LEFT;

  SET_HAND_BONE(_binocularsGripLeft.poseHand,                   -1.587f, -0.074f,  0.066f, -133.631f,  -0.000f,  79.177f);
  SET_HAND_BONE(_binocularsGripLeft.poseThumb.poseBone1,        -1.590f, -0.047f,  0.062f, -178.763f,  -1.181f, -12.857f);
  SET_HAND_BONE(_binocularsGripLeft.poseThumb.poseBone2,        -1.589f, -0.046f,  0.010f,  178.363f,  -0.020f,  -1.009f);
  SET_HAND_BONE(_binocularsGripLeft.poseThumb.poseBone3,        -1.588f, -0.047f, -0.025f,  179.252f,  -4.799f,  -6.558f);
  SET_HAND_BONE(_binocularsGripLeft.poseIndexFinger.poseBone1,  -1.650f, -0.038f,  0.005f, -151.421f,   5.720f,  76.746f);
  SET_HAND_BONE(_binocularsGripLeft.poseIndexFinger.poseBone2,  -1.670f, -0.037f, -0.030f, -167.556f,   1.810f,  74.760f);
  SET_HAND_BONE(_binocularsGripLeft.poseIndexFinger.poseBone3,  -1.675f, -0.036f, -0.053f, -179.880f,   0.360f,  76.027f);
  SET_HAND_BONE(_binocularsGripLeft.poseMiddleFinger.poseBone1, -1.655f, -0.061f,  0.000f, -153.740f,  -0.821f,  85.703f);
  SET_HAND_BONE(_binocularsGripLeft.poseMiddleFinger.poseBone2, -1.674f, -0.059f, -0.039f, -173.118f,  -1.314f,  86.018f);
  SET_HAND_BONE(_binocularsGripLeft.poseMiddleFinger.poseBone3, -1.677f, -0.058f, -0.065f,  176.715f,  -2.070f,  85.695f);
  SET_HAND_BONE(_binocularsGripLeft.poseRingFinger.poseBone1,   -1.651f, -0.082f,  0.001f, -141.578f, -10.871f, 104.600f);
  SET_HAND_BONE(_binocularsGripLeft.poseRingFinger.poseBone2,   -1.673f, -0.082f, -0.026f, -167.696f,  -6.706f, 105.536f);
  SET_HAND_BONE(_binocularsGripLeft.poseRingFinger.poseBone3,   -1.679f, -0.081f, -0.051f,  175.451f,  -4.164f, 105.023f);
  SET_HAND_BONE(_binocularsGripLeft.posePinkyFinger.poseBone1,  -1.641f, -0.106f,  0.004f, -136.167f, -12.461f, 100.630f);
  SET_HAND_BONE(_binocularsGripLeft.posePinkyFinger.poseBone2,  -1.660f, -0.105f, -0.015f, -152.819f,  -9.034f, 103.963f);
  SET_HAND_BONE(_binocularsGripLeft.posePinkyFinger.poseBone3,  -1.669f, -0.105f, -0.033f, -171.135f,  -6.462f, 104.006f);

  _binocularsGripRight.bHand = V2_Interface::RIGHT;

  SET_HAND_BONE(_binocularsGripRight.poseHand,                   -1.539f, -0.074f, -0.250f,  62.556f, 11.473f, -106.621f);
  SET_HAND_BONE(_binocularsGripRight.poseThumb.poseBone1,        -1.541f, -0.048f, -0.243f,  -9.613f, 13.229f,  158.504f);
  SET_HAND_BONE(_binocularsGripRight.poseThumb.poseBone2,        -1.555f, -0.051f, -0.194f, -11.212f, 19.490f,  169.672f);
  SET_HAND_BONE(_binocularsGripRight.poseThumb.poseBone3,        -1.567f, -0.056f, -0.162f, -13.994f, 16.672f,  164.287f);
  SET_HAND_BONE(_binocularsGripRight.poseIndexFinger.poseBone1,  -1.615f, -0.036f, -0.207f,  44.683f, 17.627f, -107.407f);
  SET_HAND_BONE(_binocularsGripRight.poseIndexFinger.poseBone2,  -1.645f, -0.035f, -0.180f,  27.843f, 14.189f, -110.571f);
  SET_HAND_BONE(_binocularsGripRight.poseIndexFinger.poseBone3,  -1.657f, -0.036f, -0.160f,  15.700f, 12.441f, -109.715f);
  SET_HAND_BONE(_binocularsGripRight.poseMiddleFinger.poseBone1, -1.623f, -0.058f, -0.207f,  43.563f,  8.791f, -100.532f);
  SET_HAND_BONE(_binocularsGripRight.poseMiddleFinger.poseBone2, -1.653f, -0.057f, -0.176f,  24.201f,  8.217f, -100.385f);
  SET_HAND_BONE(_binocularsGripRight.poseMiddleFinger.poseBone3, -1.664f, -0.059f, -0.152f,  13.945f,  7.577f, -100.931f);
  SET_HAND_BONE(_binocularsGripRight.poseRingFinger.poseBone1,   -1.621f, -0.079f, -0.210f,  57.941f, -6.682f,  -85.857f);
  SET_HAND_BONE(_binocularsGripRight.poseRingFinger.poseBone2,   -1.651f, -0.079f, -0.191f,  31.792f, -3.081f,  -83.597f);
  SET_HAND_BONE(_binocularsGripRight.poseRingFinger.poseBone3,   -1.664f, -0.080f, -0.170f,  15.030f, -0.513f,  -83.246f);
  SET_HAND_BONE(_binocularsGripRight.posePinkyFinger.poseBone1,  -1.614f, -0.104f, -0.213f,  62.384f, -6.689f,  -89.794f);
  SET_HAND_BONE(_binocularsGripRight.posePinkyFinger.poseBone2,  -1.637f, -0.103f, -0.200f,  44.073f, -4.204f,  -85.310f);
  SET_HAND_BONE(_binocularsGripRight.posePinkyFinger.poseBone3,  -1.651f, -0.103f, -0.186f,  24.053f, -1.473f,  -84.348f);
}

void Pointman2Pose::DefineStowPos()
{
  SET_HAND_BONE(_primaryStowPos,  -0.155f, 0.235f, -0.131f,  20.716f, -7.718f, 92.742f);
  SET_HAND_BONE(_secondaryStowPos, 0.231f, 0.219f, -0.054f, 122.770f, -6.197f, 87.068f);
}

void Pointman2Pose::ModifyGunTrans(Matrix4& mat)
{
  //if (IsUpperBodyControlled()) // Always compute in case Upper Body control is enabled in engine
  {
    // Prepare invTorso.
    Matrix4 invTorso = GetTorso().GetInvertGeneral();

    // Gun transformation corresponds to Pointman's aim
    // Note that head Axis is used here instead of gun Axis. That's because it is closer to what Pointman expects and it also looks more natural
    Matrix4 headOrient(MIdentity);
    Vector3 headAxis = GetHeadCenterMoves();

    if (IsLowerBodySkeletonControlled())
    {
      Vector3 ptPelvisPos = Vector3(0.0f, GetPelvisHeight(), 0.0f);
      headAxis = (headAxis + ptPelvisPos - _rtmPelvisOffset) * _explicitSkeletonFactor + headAxis * (1.0f - _explicitSkeletonFactor);
    }

    headOrient.SetPosition(headAxis);
    Matrix4 headOrientInv = headOrient.GetInvertScaled();
    mat =
      (
      headOrient*
      invTorso*
      GetAim()*
      headOrientInv
     );
  }
}

void Pointman2Pose::ModifyHeadTrans(Matrix4& mat)
{
  //if (IsUpperBodyControlled()) // Always compute in case Upper Body control is enabled in engine
  {
    // Prepare invTorso.
    Matrix4 invTorso = GetTorso().GetInvertGeneral();

    // Gun transformation corresponds to Pointman's aim
    // Note that head Axis is used here instead of gun Axis. That's because it is closer to what Pointman expects and it also looks more natural
    Matrix4 headOrient(MIdentity);
    Vector3 headAxis = GetHeadCenterMoves();

    if (IsLowerBodySkeletonControlled())
    {
      Vector3 ptPelvisPos = Vector3(0.0f, GetPelvisHeight(), 0.0f);
      headAxis = (headAxis + ptPelvisPos - _rtmPelvisOffset) * _explicitSkeletonFactor + headAxis * (1.0f - _explicitSkeletonFactor);
    }

    headOrient.SetPosition(headAxis);
    Matrix4 headOrientInv = headOrient.GetInvertScaled();

    // Head transformation corresponds to Pointman's view
    mat =
      (
      headOrient*
      invTorso*
      GetView()*
      headOrientInv
      );
  }
}

void Pointman2Pose::ModifyLookTrans(Matrix4& mat)
{
  mat = GetCamera();
}

void Pointman2Pose::ModifyRelativeChange()
{
  // Call it before UpdatePose, as it overrides motion state from previous frame - it's needed for comparison
  int firedCount = Fired();
  if ( firedCount != 0 )
  {
    FireWeapon(firedCount);

    // Recoil impulse is very short so it must be read in same frame whet it is created
    GetRecoilImpulse(_impulseAngX, _impulseAngY, _impulseZ);

    // Pointman-generated shot fired (no need to send V2_CmdSetFireState)
    SetFired();
  }

  // If explicit skeleton is not used, don't update the bones (keep them as they were in previous steps)
  // Note: this is actually not wanted, because we want to read the relative change even in case the skeleton is not defined (like during swimming)
  //if (_explicitPose->UseExplicitSkeleton())
  {
    // Update the transformation
    _relativeChange = UpdatePose();
  }

  if (IsUpperBodyFullyControlled()) 
  {
    if (GetCameraTypeWanted() != CamGunner) _oldCameraType = GetCameraTypeWanted();

    if (EnableOptics())
    {
      SetCameraType(CamGunner);
    }
    else
    {
      SetCameraType(_oldCameraType);
    }
  }

  // Update _explicitSkeletonFactor & _rtmPelvisOffset
  _rtmPelvisOffset = GetPelvisPosition();
  //_explicitSkeletonFactor += TODO
}

int Pointman2Pose::Fired() const 
{ 
  if ( _serverMotionDat.bMotionCtrlUpper == V2_Interface::MOTION_CTRL_FULL )
  {
    return _serverMotionDat.dwFireCount - _oldServerMotionDat.dwFireCount;
  }

  return 0;
}

void Pointman2Pose::SimulateJoystick()
{
  BYTE bError;
  WORD wSeqnum;
  if (!V2_GetServerJoyXmit(_serverJoyDat, bError, wSeqnum))
  {
    RptF("{error} Pointman: Failed to send joystick update to the client");
    return;
  }
}

BYTE Pointman2Pose::JoystickArbButtons(int i) const
{
  return (i < V2_Interface::NUM_JOY_BUTTONS) ? _serverJoyDat.arbButtons[i] : 0;
}

LONG Pointman2Pose::JoystickArlPovs(int i) const
{
  return (i < V2_Interface::NUM_JOY_POVS) ? _serverJoyDat.arlPovs[i] : 0;
}

float Pointman2Pose::JoystickArfAxes(int i) const
{
  float value = 0.0f;
  if (i < V2_Interface::NUM_JOY_AXES && abs(_serverJoyDat.arfAxes[i]) > 0.1) // We don't transmit smaller values then 0.1 to avoid sliding
  {
    value = _serverJoyDat.arfAxes[i];
  }
  return value;
}

void Pointman2Pose::SetMotionStateSuspended()
{
  V2_Interface::_V2_MotionState ms;
  ms.bMobilityState = V2_Interface::MOBILITY_SUSPENDED;
  SetMotionState(ms);
}

Pointman2Pose *GExplicitPose;