#ifndef _POINTMAN_H
#define _POINTMAN_H

#include "mathHelper.h"

#include "pluginHeader.h"

#include "VBS2Fusion.h"
#include "VBS2FusionDefinitions.h"

#include "data/Unit.h"
#include "util/UnitUtilities.h"
#include "util/PlayerUtilities.h"
#include "util/MissionUtilities.h"
#include "util/PoseControlUtilities.h"
#include "util/InputUtilities.h"
#include "util/WeaponUtilities.h"
#include "util/CameraUtilities.h"
#include "util/GeneralUtilities.h"
#include "DisplayFunctions.h"
#include "VBS2FusionCallBackDefinitions.h"

typedef int (WINAPI * ExecuteCommandType)(const char *command, char *result, int resultLength);

void InitPlayer();
void ValidatePlayer();
bool ValidateProfile();
void EnableEPC();
void DisableEPC();

int ExecuteCmd(const char* command, char *result = NULL, int resultLength = 0);
bool LoadPathFromConfig(string &path);
bool LoadIPFromConfig(string &ip);

void SetExternalPose(bool value);
void SetExternalMovementControlled(bool value);
void SetExternalCameraControlled(bool value);
void SetExternalPoseLowerBody(bool value);
void SetExternalPoseUpBody(bool value);
void SetExternalPoseSkeleton(bool value);
void EnableExternalJoystick(bool value);
void EnableTrackIR(bool value);

int GetActUpDegree();

bool IsAlive();
bool IsInVehicle();
bool IsDriver();
bool IsGunner();
bool IsOnSomeLadder();
bool IsSeated();
bool IsWeaponOnBack();
bool WeaponSafety();
void RaiseWeapon();

void GetRecoilImpulse(float &impulseAngX, float &impulseAngY, float &impulseZ);

float GetCameraFOV();
void SetCameraFOV(float fov);

float GetDamage();
float GetFatigue();

char* GetWeaponType (int weaponMask);
void GetWeaponName(char *name, char* type);
void GetWeaponBBox(Vector3 &bbMin, Vector3 &bbMax, char* type);
Vector3 GetMuzzleCameraPos(char* type);
Vector3 GetMuzzlePos(char* type);
int GetMuzzleCount();
bool GetMuzzleInfo(char* type, int muzzle, int mode, int& burst, float& reloadTime);
float GetWeaponWeight(char* type);
float GetWeaponReloadTime(char* type);
void GetWeaponMaxBurst(char* type,int& maxBurst,bool& fullAuto);
void GetMuzzleOpticsZoom(float &opticsZoomMin, float &opticsZoomMax, char* type);
int GetMagazinesCountOfType(char * type);

Vector3 GetHeadCenterMoves();

void FireWeapon(int firedCount);

int GetCameraType();
int GetCameraTypeWanted();
void SetCameraType(int camTypeToSet);

Vector3 GetPelvisPosition();

bool IsViewAlignedWithHead();

int FindBone(const char* boneName);

void GetCurrentWeaponInfo(char* weaponName, char* magazineName, int& ammoCount, int& weaponType, float& weaponWeight, int& magazineType, float& magazineWeight, int& magazineAmmoSimulation);
void GetCurrentWeaponMode(char* mode);
bool IsCurrentWeaponHandGun();

void SetAction(char * action);

void MoveToSurface();

void LogF(const char* format, ...);

#define RptF LogF

class PoseControlUnit : public Unit
{
public:
  bool onModifyBone(Matrix4f& mat4f, SKELETON_TYPE index);

  bool onModifyGunTrans(Matrix4f& mat4f);

  bool onModifyHeadTrans(Matrix4f& mat4f);

  bool onModifyLookTrans(Matrix4f& mat4f);

  bool onModifyRelativeChange(Matrix4f& mat4f);

  bool onModifyUpperView(Matrix4f& mat4f);

  bool onModifyUpperTroso(Matrix4f& mat4f);

  bool onModifyUpperAim(Matrix4f& mat4f);

  bool onReload();
};

#endif