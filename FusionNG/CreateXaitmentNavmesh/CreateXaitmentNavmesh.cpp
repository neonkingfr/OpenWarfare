#pragma warning(disable: 4996)

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// VBS2Fusion interface library forced linking
// Needed for plugins that don't use any of VBS2Fusion symbols
// This will insure that VBS2Fusion_2005 library will be linked to the plugin
//  and the plugin will require to have VBS2Fusion_2005.dll
// It is required by Simcentric that each Fusion plugin links with VBS2Fusion_2005.lib or VBS2_Fusion_2008.lib.
#include "VBS2FusionAppContext.h"
VBS2Fusion::VBS2FusionAppContext appContext;
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#include "CreateXaitmentNavmesh.h"

#include <xait/common/callback/FileIOCallbackWin32.h>
#include <xait/common/memory/MemoryStream.h>
#include "xait/common/io/FileStream.h" 
#include <xait/map/build/TriangleClassifier.h> 
#include <xait/map/xaitMap.h>

#include "Model/BIModel.h"

#define DLL_DIRECTORY "pluginsFusion\\dll"
#define BIMODELSDK_DLL "BISDKModel.dll"
#define XAITMAP_DLL "xaitmap-win32-vc80shared.dll"
#define XAITCOMMON_DLL "xaitcommon-win32-vc80shared.dll"

#define LOG_HEADER "[Xaitment Navmesh Build] "
#define NAVMESH_PROPERTY_NAME "navmesh"
#define NAVMESH_PROPERTY_VALUE "xait"

#define _DIAG 0
#define ADD_BASE_POLYGON 1

HMODULE BIModelSDKHModule = NULL;
HMODULE XaitMapHModule = NULL;
HMODULE XaitCommonHModule = NULL;

//! Function returning file name without path and extension
//   \param filePath Path to file
BI_String ParseFileName(BI_String filePath)
{
  char *fileNameWithExt = strrchr(filePath.data(), '\\');
  if (!fileNameWithExt)
    fileNameWithExt = strrchr(filePath.data(), '/');
  if (!fileNameWithExt)
    fileNameWithExt = filePath.data();

  char *extension = strrchr(fileNameWithExt, '.');
  if (extension)
    *extension = 0;

  return fileNameWithExt;
}

void LogDiagMessage(const char *format, ...)
{
#if _DIAG
  va_list arglist;
  va_start(arglist, format);
  vprintf(format, arglist);
  va_end(arglist);
#endif
}

//! Function delay loading xaitment dlls 
bool LoadXaitmentDlls()
{
  //Load xaitCommon dll
  {
    XaitCommonHModule = LoadLibrary(DLL_DIRECTORY"\\"XAITCOMMON_DLL);
    if (XaitCommonHModule == NULL)
    {
      printf(LOG_HEADER"Library '%s' not found.\n", XAITCOMMON_DLL);
      return false;
    }
  }

  //Load xaitMap dll
  {
    XaitMapHModule = LoadLibrary(DLL_DIRECTORY"\\"XAITMAP_DLL);
    if (XaitMapHModule == NULL)
    {
      printf(LOG_HEADER"Library '%s' not found.\n", XAITMAP_DLL);
      FreeLibrary(XaitCommonHModule);
      return false;
    }
  }

  return true;
}

//! Function freeing xaitment dlls
void FreeXaitmentDlls()
{
  if (XaitMapHModule != NULL)
    FreeLibrary(XaitMapHModule);
  if (XaitCommonHModule != NULL)
    FreeLibrary(XaitCommonHModule);
  XaitMapHModule = NULL;
  XaitCommonHModule = NULL;
}

//! Function creating navigation mesh
//   \param p3dFile Full path to p3d file from which to create the navigation mesh
//   \param exportDirectory Full path to directory where the resulting navogation mesh should be stored
bool WINAPI CreateNavmesh(const char *p3dFile, const char *exportDirectory)
{
  //TODO: make the constants configurable
  const float NavMeshCubeSize = 0.15f;
  const float NavMeshUnitHeight = 1.6f;
  const float NavMeshUnitRadius = 0.20f;
  const float NavMeshStepHeight = 0.6f;
  const float NavMeshSectorSize = 16.0f;
  const float maxSlope= 60.0f * X_DEG2RAD_F;
  const XAIT::Common::Math::Vec3f NavMeshGravityDir = XAIT::Common::Math::Vec3f(0.0f, -1.0f, 0.0f);

  BI_Model model;
  BI_ErrCode err = model.Load(p3dFile);
  if (err != BIEC_OK)
  {
    LogDiagMessage(LOG_HEADER"Error: Unable to load p3d file '%s'.\n", p3dFile);
    return false;
  }

  LogDiagMessage(LOG_HEADER"P3D file '%s' loaded.\n", p3dFile);
  //the navigation mesh is built out of geometry LOD
  int geometryLOD = model.FindLevelExact(BI_LOD_GEOMETRY);

  if (geometryLOD == -1)
  {
    LogDiagMessage(LOG_HEADER"No GEOMETRY LOD in '%s'.\n", p3dFile);
    return false;
  }

  //mesh represents all the geometry we need
  BI_Mesh mesh = model.GetMeshAtLevel(geometryLOD);
  //we want to create navmesh only for meshes set as navmesh=xait
  const char *xProp = mesh.GetNamedProp(NAVMESH_PROPERTY_NAME);
  if (!xProp || stricmp(xProp, NAVMESH_PROPERTY_VALUE))
  {
    LogDiagMessage(LOG_HEADER"Model ignored: Property '%s' not set to '%s'.\n", NAVMESH_PROPERTY_NAME, NAVMESH_PROPERTY_VALUE);
    return false;
  }

  
  bool result = LoadXaitmentDlls();
  if (!result) return false;

  XAIT::Common::Callback::MemoryCallback::Ptr memoryCallback(new XAIT::Common::Callback::MemoryCallback()); 
  XAIT::Common::Callback::FileIOCallback::Ptr fileIOCallback( new XAIT::Common::Callback::FileIOCallbackWin32() ); 
  XAIT::Common::Interface::initLibrary(memoryCallback); 
  XAIT::Map::Interface::initLibrary(fileIOCallback);
  {
#if !_DIAG
    XAIT::Common::Interface::getLogReporter()->setLogTypes(XAIT::Common::Debug::LT_LOG_NONE);
#endif

    XAIT::Map::Build::NavMeshBuilder* navBuilder= XAIT::Map::Interface::createNavMeshBuilder();
    if (!navBuilder) return false;
    if (!navBuilder->getBuildConfig()) 
    {
      delete navBuilder;
      result = false;
    }
    if (result)
    {
      //Insert spawn points if they are defined in model
      int memoryLOD = model.FindLevelExact((BI_LOD_MEMORY));
      if (memoryLOD != -1)
      {
        BI_Mesh memMesh = model.GetMeshAtLevel(memoryLOD);
        BI_Selection spSel;
        if (memMesh.GetNamedSel("spawn_points", spSel) == BIEC_OK)
        {
          LogDiagMessage(LOG_HEADER"Spawn points found.\n");
          int index = -1;
          while ((index = spSel.NextPoint(true, index)) != -1)
          {
            XAIT::Map::SpawnpointPathObject* spawnPoint = new XAIT::Map::SpawnpointPathObject("");
            BI_Vertex v = memMesh.Point(index);
            spawnPoint->setPosition(XAIT::Common::Math::Vec3f(-v.X(), v.Y(), -v.Z()));
            navBuilder->addPathObject(spawnPoint);
            LogDiagMessage(LOG_HEADER"Spawn point(%i) [%f, %f, %f] used.\n", index, v.X(), v.Y(), v.Z());
          }
        }
      }

      // determine xaitment cache and navmesh path file
      BI_String fileName = ParseFileName(p3dFile);

      BI_String meshFileName(exportDirectory);
      meshFileName += "\\";
      BI_String cacheFileName(meshFileName);

      meshFileName += fileName + ".xmap";;
      cacheFileName += fileName + ".xch";

      XAIT::Common::SharedPtr<XAIT::Common::IO::Stream> mSectorCache; 
      mSectorCache= new(XAIT::Map::MapMemoryPool::getLowAllocThreadSafeAllocator()) XAIT::Common::IO::FileStream(cacheFileName.c_str(), XAIT::Common::IO::FileStream::FACCESS_READWRITE, XAIT::Common::IO::FileStream::FMODE_CREATE);
      navBuilder->setSectorCache(mSectorCache); 

      XAIT::Common::Memory::Allocator::Ptr allocator = XAIT::Common::Interface::getGlobalAllocator();

      XAIT::Common::Container::Vector<XAIT::Common::Math::Vec3f> vertices(XAIT::Common::Interface::getGlobalAllocator());
      XAIT::Common::Container::Vector<XAIT::Common::Geom::IndexedTriangle> triangles(XAIT::Common::Interface::getGlobalAllocator());

      int nVertices = mesh.NumberOfPoints();
      LogDiagMessage(LOG_HEADER"Number of vertices: %i\n", nVertices);
      for (int i = 0; i < nVertices; i++)
      {
        const BI_Vertex& v = mesh.Point(i);
        vertices.pushBack(XAIT::Common::Math::Vec3f(-v.X(), v.Y(), -v.Z()));
      }
      
#if ADD_BASE_POLYGON
      vertices.pushBack(XAIT::Common::Math::Vec3f(- mesh.BoundingBox().GetLower().X(), 0.0f, - mesh.BoundingBox().GetLower().Z()));
      vertices.pushBack(XAIT::Common::Math::Vec3f(- mesh.BoundingBox().GetUpper().X(), 0.0f, - mesh.BoundingBox().GetLower().Z()));
      vertices.pushBack(XAIT::Common::Math::Vec3f(- mesh.BoundingBox().GetUpper().X(), 0.0f, - mesh.BoundingBox().GetUpper().Z()));
      vertices.pushBack(XAIT::Common::Math::Vec3f(- mesh.BoundingBox().GetLower().X(), 0.0f, - mesh.BoundingBox().GetUpper().Z()));
#endif

      int nFaces = mesh.NumberOfFaces();
      LogDiagMessage(LOG_HEADER"Number of faces: %i\n", nFaces);
      for (int i = 0; i < nFaces; i++)
      {
        const BI_Face& f = mesh.GetFace(i);
        //we can handle only faces with exactly 3 vertices now
        if (f.VertexCount() == 3) 
        {
          triangles.pushBack(XAIT::Common::Geom::IndexedTriangle(f.GetPoint(2), f.GetPoint(1), f.GetPoint(0)));
        }
        else if (f.VertexCount() == 4)
        {
          triangles.pushBack(XAIT::Common::Geom::IndexedTriangle(f.GetPoint(0), f.GetPoint(2), f.GetPoint(1)));
          triangles.pushBack(XAIT::Common::Geom::IndexedTriangle(f.GetPoint(0), f.GetPoint(3), f.GetPoint(2)));
        }
        else
          LogDiagMessage(LOG_HEADER"Warning: Face %i vertex count: %i. Faces with vertex count other than 3 or 4 are not supported.\n", i, f.VertexCount());
      }

#if ADD_BASE_POLYGON
      triangles.pushBack(XAIT::Common::Geom::IndexedTriangle(vertices.size() - 2, vertices.size() - 3, vertices.size() - 4));
      triangles.pushBack(XAIT::Common::Geom::IndexedTriangle(vertices.size() - 4, vertices.size() - 1, vertices.size() - 2));
#endif

      XAIT::Map::Build::TriangleClassifier* mTriClass= new XAIT::Map::Build::SlopeTriangleClassifier(maxSlope,NavMeshGravityDir,0); 

      XAIT::Map::Build::GeometryInfo geomInfo; 
      geomInfo.mNumVertices = vertices.size();
      geomInfo.mVertices = (XAIT::uint08*)&vertices[0];
      geomInfo.mVertexSize = sizeof(XAIT::Common::Math::Vec3f); //Bytes --- float 
      geomInfo.mNumTriangles = triangles.size();
      geomInfo.mIndices = (XAIT::uint08*)&triangles[0]; 
      geomInfo.mTriangleSize = sizeof(XAIT::Common::Geom::IndexedTriangle); 
      geomInfo.mTriangleClassifier= mTriClass; 
      navBuilder->addInputGeometry(geomInfo);

      // initialize the character
      XAIT:: Map::CharacterInfo charInfo;
      charInfo.mName = "character";
      charInfo.mPhysicalHeight = NavMeshUnitHeight;
      charInfo.mRadius = NavMeshUnitRadius;

      //float sectorSize = navBuilder->getBuildConfig()->getSectorSize(true);
      navBuilder->getBuildConfig()->addCharacter(charInfo);
      // set the build parameter for the navmesh
      navBuilder->getBuildConfig()->setUpDirection(NavMeshGravityDir * -1.f);
      navBuilder->getBuildConfig()->setCubeSize(NavMeshCubeSize);
      navBuilder->getBuildConfig()->setSectorSize(NavMeshSectorSize);
      navBuilder->getBuildConfig()->setUnitRadius(NavMeshUnitRadius);
      navBuilder->getBuildConfig()->setStepHeight(NavMeshStepHeight);


      unsigned long long startTime = GetTickCount();
      navBuilder->startNavMeshGeneration(true, -1);
      unsigned long long endTime = GetTickCount();
      printf(LOG_HEADER"Navmesh built in %d ms. \n", endTime - startTime);

      XAIT::Map::NavMeshContainer* navMesh= navBuilder->returnBuildResult();

      result = false;
      if (navMesh)
      {
        XAIT::Map::NavMeshSerializer::writeNavMesh(meshFileName.c_str(), navMesh, XAIT::Common::Platform::Endian::ENDIAN_LITTLE);
        result = true;
        delete navMesh;
      }

      delete mTriClass;
      delete navBuilder;
    }
  }
  XAIT::Map::Interface::closeLibrary(); 
  XAIT::Common::Interface::closeLibrary();  
  FreeXaitmentDlls();

  return result;
}

struct IDirect3DDevice9;


// parameters that may be passed to the plugin
struct VBSParameters
{
  const char *_dirInstall;  // VBS installation directory
  const char *_dirUser;     // user's VBS directory
  IDirect3DDevice9 *_device; // rendering D3D device; is NULL if !_VBS3_PLUGIN_DEVICE
};

// Onload - get the d3d device
VBSPLUGIN_EXPORT int WINAPI OnLoad(VBSParameters *params) 
{
  LogDiagMessage("[Xait Navmesh Build] Plugin loaded.\n");
  return 0;
}

VBSPLUGIN_EXPORT void WINAPI OnModelBinarized(const char *p3dFile, const char *exportDirectory)
{
  bool ret = CreateNavmesh(p3dFile, exportDirectory);
}

// DllMain
BOOL WINAPI DllMain(HINSTANCE hDll, DWORD fdwReason, LPVOID lpvReserved)
{
  switch(fdwReason)
  {
  case DLL_PROCESS_ATTACH:
    {
      //Load BISDKModel dll
      {
        BIModelSDKHModule = LoadLibrary(DLL_DIRECTORY"\\"BIMODELSDK_DLL);
        if (BIModelSDKHModule == NULL)
        {
          printf(LOG_HEADER"Library '%s' not found.\n", BIMODELSDK_DLL);
          return FALSE;
        }
      }
    }
    break;
  case DLL_PROCESS_DETACH:
    {
      if (BIModelSDKHModule != NULL)
        FreeLibrary(BIModelSDKHModule);
    }
    break;
  case DLL_THREAD_ATTACH:
    OutputDebugString("Called DllMain with DLL_THREAD_ATTACH\n");
    break;
  case DLL_THREAD_DETACH:
    OutputDebugString("Called DllMain with DLL_THREAD_DETACH\n");
    break;
  default:
    break;
  }
  return TRUE;
}
