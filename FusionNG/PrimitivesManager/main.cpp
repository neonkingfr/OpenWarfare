#pragma warning(disable: 4996)

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// VBS2Fusion interface library forced linking
// Needed for plugins that don't use any of VBS2Fusion symbols
// This will insure that VBS2Fusion_2005 library will be linked to the plugin
//  and the plugin will require to have VBS2Fusion_2005.dll
// It is required by Simcentric that each Fusion plugin links with VBS2Fusion_2005.lib or VBS2_Fusion_2008.lib.
#include "VBS2FusionAppContext.h"
VBS2Fusion::VBS2FusionAppContext appContext;
#if _X64
#pragma comment(lib, "../_depends/Fusion/lib64/VBS2Fusion_2005.lib")
#else
#pragma comment(lib, "../_depends/Fusion/lib/VBS2Fusion_2005.lib")
#endif
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

#include <windows.h>
#include "vbsplugin.h"

#include "primitivesmanager.h"
#include "../../Common/Essential/RefCount.hpp"
#include <vector>

PrimitivesManager primitivesManager;

std::vector<SmartRef<Primitive3D>> _pluginTriangles;

// This function is called once per frame, after depth priming is done but before normal rendering is done
// parameters contain real direct 3D device, so resource management can be done here
// At this point some extra information is available like eye accomodation value
VBSPLUGIN_EXPORT void WINAPI OnNgInitDraw(paramInitDraw *param, DWORD paramSize)
{  
  //send all the triangles into the drawing manager
  std::vector<SmartRef<Primitive3D>>::iterator it;
  for (it = _pluginTriangles.begin(); it != _pluginTriangles.end(); it++)
  {
    primitivesManager.Add(*it);
  }

  primitivesManager.PrepareDraw(param->dev);
}

// This function is called when engine completes a render pass
// Render passes are done for different types of objects (see drawPassIdentifier) and different render modes (see drawModeIdentifier)
// so this function is called many times per framed, you can pick the required slot to do drawing here
// parameters contain CB interface which must be used to do only drawing here,
// do not attempt to access the D3D device directly and do not release any resources!
VBSPLUGIN_EXPORT void WINAPI OnNgDrawnPass(paramDrawnPass *param, DWORD paramSize)
{
  // This is where we want to do our 3D scene drawing. 
  // We want to only draw in drawModeCommon here, since we do not do depth priming in the plugin
  if(param->modeId != drawModeCommon) return;

  // Only draw at end of drawPass2 - Most of the scene has been drawn at this point excluding the
  // player's own character and vehicle he/she is in.
  // Alternatively we could draw at end of drawPass3, if we were drawing something very close to the player
  // as it is when the player's character and vehicle are drawn with a special depth range
  if(param->passId == drawPass2)
  {
    primitivesManager.SetFrustum(param->cameraFrustum);
    primitivesManager.Draw(param->cbi);
  }
}

// This function is called when D3D device is destroyed or reset by engine
// parameters contain real direct 3D device, so resource management can be done here
// All D3D resources created by the plugin must be released here, otherwise device resets will fail
VBSPLUGIN_EXPORT void WINAPI OnNgDeviceInvalidate(paramDeviceInvalidate *param, DWORD paramSize)
{
#if MANAGER_DEBUG
  OutputDebugString("PrimitivesManager: OnNgDeviceInvalidate\n");
#endif
  // Delete the sample object, this will release everything it has created
  // Alternatively, we could check for param->isReset and only release Direct3D handles without destroying anything else

  primitivesManager.ReleaseAll();
}

VBSPLUGIN_EXPORT const char* WINAPI PluginFunction(const char *input)
{
  //example input: 
  //  pluginFunction("PrimitivesManager","add [0,0,0],[1,1,0],[-1,1,0],[1,1,1,1]");
  //    creates 1 triangle
  //  pluginFunction("PrimitivesManager","delete all");
  //    deletes all triangles 

  static const char resultDone[]="[true]";
  static const char resultError[]="[error]";

  if (stricmp(input, "delete all") == 0)
  {
    _pluginTriangles.clear();
  }
  else
  {
    float x1 = 0,x2 = 0,x3 = 0,y1 = 0,y2 = 0,y3 = 0,z1 = 0,z2 = 0,z3 = 0;
    float r = 0, g = 0, b = 0, a = 0;
    int parsed = sscanf(input, "add [%f,%f,%f],[%f,%f,%f],[%f,%f,%f],[%f,%f,%f,%f]", &x1, &z1, &y1, &x2, &z2, &y2, &x3, &z3, &y3, &r, &g, &b, &a);
    if (parsed != 13) return resultError;

    BI_Vector3 v1(x1, y1, z1);
    BI_Vector3 v2(x2, y2, z2);
    BI_Vector3 v3(x3, y3, z3);

    SmartRef<Primitive3D> triangle = new Triangle3D(v1, v2, v3);
    triangle->SetColor(BI_Color(r,g,b,a));

    _pluginTriangles.push_back(triangle);
  }

  return resultDone;
}


BOOL WINAPI DllMain(HINSTANCE hDll, DWORD fdwReason, LPVOID lpvReserved)
{
  switch(fdwReason)
  {
  case DLL_PROCESS_ATTACH:
    OutputDebugString("DLL_PROCESS_ATTACH");
    break;
  case DLL_PROCESS_DETACH:
    OutputDebugString("DLL_PROCESS_DETACH");
    break;
  }
  return TRUE;
}
