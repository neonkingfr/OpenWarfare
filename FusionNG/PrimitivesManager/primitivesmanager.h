#include "../../Common/BIVector.h"
#include "../../Common/BIMatrix.h"
#include "../../Common/BIColor.h"
#include "../../Common/Essential/RefCount.hpp"

//#include "../../Common/Essential/AppFrameWork.hpp"
#include "vbsplugin.h"  // frustum specification
#include <vector>

#define MANAGER_DEBUG 0

class PrimitivesManager;

class Primitive3D : public RefCount
{
  friend class PrimitivesManager;

protected:
  BI_Vector3* _vertices;    // vertices of the primitive (does not have to be triangulated)
  int _nVertices;           // number of primitive vertices
  BI_Color _color;          // color of the primitive

public:
  Primitive3D(int size);                  
  virtual ~Primitive3D();
  void SetColor(const BI_Color& color);                       // set color of the whole primitive (does not support individual vertex colors yet)

  virtual int NTriangles() const = 0;                         // return number of the triangles needed for drawing this primitive
  virtual void AddTrianglesToBuffer(char* buffer) const = 0;  // adds triangles of this primitive to the draw buffer (pointer is expected to already point at the corresponding location in the buffer)
};


class Triangle3D: public Primitive3D
{
public:
  Triangle3D(const BI_Vector3& v1, const BI_Vector3& v2, const BI_Vector3& v3);
  
  virtual int NTriangles() const {return 1;}
  virtual void AddTrianglesToBuffer(char* buffer) const;
};


class PrimitivesManager
{
  FrustumSpecNG _frustum;
  BI_Matrix4 _transform;
  
  LPDIRECT3DVERTEXBUFFER9 _vertexBuffer;       // vertex buffer containing all primitives
  int _vbTriangles;
  IDirect3DPixelShader9* _ps;
  IDirect3DVertexShader9* _vs;
  IDirect3DVertexDeclaration9 *_vertexDecl;

  std::vector<SmartRef<Primitive3D>> _primitives;
  int NTriangles();

  void ReleaseVB();
  void CreateShaders(IDirect3DDevice9* device);
  void ReleaseShaders();

  void SetDefaultState(VBS2CBInterface* device);

public:
  PrimitivesManager();
  ~PrimitivesManager();
  void Draw(VBS2CBInterface* device);
  void PrepareDraw(IDirect3DDevice9* device);
  void SetFrustum(const FrustumSpecNG& frustum) {_frustum = frustum;}

  // all these functions have to be called before every draw cycle (new cycle resets everything to default values)
  void Add(SmartRef<Primitive3D> primitive);                  // adds one single primitive
  void Add(std::vector<SmartRef<Primitive3D>>& primitives);   // adds list of primitives
  void SetCustomTransform(const BI_Matrix4& transform);       // sets custom transformation (it will be applied on all primitives)
  void ReleaseAll();                                          // releases all resources (shaders, vertex buffers)
};
