#ifndef __UNITINFO_H__
#define __UNITINFO_H__

#include <stdio.h>
#include <string>
#include <map>
#include <Util\MissionUtilities.h>

struct UnitInfo
{
  std::string _ip;
  std::string _name;
  UnitInfo(std::string ip,std::string name):_ip(ip),_name(name){};
};

typedef std::map<VBS2Fusion::NetworkID, UnitInfo> UnitMap;
typedef std::pair<VBS2Fusion::NetworkID, UnitInfo> UnimInfoPair;

#endif
