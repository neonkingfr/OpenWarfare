#ifndef _FILEHANDLER_H
#define _FILEHANDLER_H

#include <xait/common/callback/FileIOCallback.h>
#include <xait/common/Interface.h>
#include <xait/common/String.h>
#include <xait/common/memory/MemoryStream.h>
#include <xait/map/xaitMap.h>

#define VBSPLUGIN_EXPORT __declspec(dllexport)

VBSPLUGIN_EXPORT void WINAPI RegisterOpenFileFnc(void *fileFnc);
VBSPLUGIN_EXPORT void WINAPI RegisterCloseFileFnc(void *fileFnc);
VBSPLUGIN_EXPORT void WINAPI RegisterSeekgFileFnc(void *fileFnc);
VBSPLUGIN_EXPORT void WINAPI RegisterTellgFileFnc(void *fileFnc);
VBSPLUGIN_EXPORT void WINAPI RegisterReadFileFnc(void *fileFnc);

int FileSize(const char* name);

XAIT::Common::IO::Stream* GetStreamFromFile(const char* filename); // creates xaitment memory stream from file

class XaitmentVBSFileHandler: public XAIT::Common::Callback::FileIOCallback
{
public:
  //! \brief check if the file handle is valid
  //! \param fHandle		file handle to check
  //! \returns true if the handle is valid, false otherwise
  bool isFileHandleValid(XAIT::Common::Callback::FileIOCallback::FileHandle* fHandle);

  //! \brief create an invalid filehandle
  //! \returns an invalid file handle
  XAIT::Common::Callback::FileIOCallback::FileHandle* getInvalidFileHandle() const;

  //! \brief open a file 
  //! \param fHandle		returns the filehandle (returnValue!)
  //! \param fileName		name of the file
  //! \param fMode		the specific filemode
  //! \returns the error value. see enum Error
  XAIT::int32 openFile(XAIT::Common::Callback::FileIOCallback::FileHandle*& fHandle, const XAIT::Common::String& fileName, const XAIT::Common::Callback::FileIOCallback::FileMode fMode);

  //! \brief	closes the file
  //! \param fHandle		a valid filehandle
  void closeFile(XAIT::Common::Callback::FileIOCallback::FileHandle* fHandle);

  //! \brief determines the size of the file
  //! \param	fHandle		a valid filehandle
  //! \returns the size of the file, if no error happens, the errorcode otherwise (<0)see enum Error
  XAIT::int64 getFileSize(XAIT::Common::Callback::FileIOCallback::FileHandle* fHandle);

  //! \brief reads a specified number of bytes into a buffer
  //! \param dstBuffer	the buffer that contains the data after the functioncall
  //! \param numBytes		Maximum number of bytes.
  //! \param fHandle		a valid filehandle
  //! \returns number of bytes read, which might be lower than numBytes
  XAIT::int32 readFromFile(void* dstBuffer, const XAIT::int32 numBytes, XAIT::Common::Callback::FileIOCallback::FileHandle* fHandle);

  //! \brief write data from a buffer to a file
  //! \param srcBuffer	the buffer containing the data
  //! \param numBytes		the size of the data in Bytes
  //! \param fHandle		a valid filehandle
  //! \returns returns numBytes if successfull, error code otherwise
  XAIT::int32 writeToFile(const void* srcBuffer, const XAIT::int32 numBytes, XAIT::Common::Callback::FileIOCallback::FileHandle* fHandle);

  //! \brief	get the current position in file
  //! \param fHandle		a valid handle
  //! \returns the current fileposition if the request was succesful; an errorcode otherwise
  XAIT::int64 getFilePosition(XAIT::Common::Callback::FileIOCallback::FileHandle* fHandle);

  //! \brief	moves the position in the file
  //! \param offset	position offset to the origin
  //! \param origin	origin of the file seek (see enum SeekOrigin)
  //! \param fHandle	valid handle of the file
  //! \return the new position in file if successful; an errorcode otherwise
  XAIT::int64 seekFile(const XAIT::int64 offset, const XAIT::Common::Callback::FileIOCallback::SeekOrigin origin, XAIT::Common::Callback::FileIOCallback::FileHandle* fHandle);

  //! \brief get the full path to a filename (only used for debugging purpose, can be the same fileName)
  //! \param path		path to the filename
  //! \returns full path
  XAIT::Common::String getFullFileName(const XAIT::Common::String& path);
};


#endif