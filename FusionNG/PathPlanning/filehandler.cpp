#include "FileHandler.h"

typedef void* (WINAPI* OpenFileType)(const char* name);
typedef void (WINAPI* CloseFileType)(void* handle);
typedef void (WINAPI* SeekgFileType)(void* handle, int pos, int dir);
typedef int (WINAPI* TellgFileType)(void* handle);
typedef void (WINAPI* ReadFileType)(void* handle, void* buffer, int n);

OpenFileType open = NULL;
CloseFileType close = NULL;
SeekgFileType seekg = NULL;
TellgFileType tellg = NULL;
ReadFileType read = NULL;

VBSPLUGIN_EXPORT void WINAPI RegisterOpenFileFnc(void *fileFnc)
{
  open = (OpenFileType)fileFnc;
}

VBSPLUGIN_EXPORT void WINAPI RegisterCloseFileFnc(void *fileFnc)
{
  close = (CloseFileType)fileFnc;
}

VBSPLUGIN_EXPORT void WINAPI RegisterSeekgFileFnc(void *fileFnc)
{
  seekg = (SeekgFileType)fileFnc;
}

VBSPLUGIN_EXPORT void WINAPI RegisterTellgFileFnc(void *fileFnc)
{
  tellg = (TellgFileType)fileFnc;
}

VBSPLUGIN_EXPORT void WINAPI RegisterReadFileFnc(void *fileFnc)
{
  read = (ReadFileType)fileFnc;
}


bool XaitmentVBSFileHandler::isFileHandleValid(XAIT::Common::Callback::FileIOCallback::FileHandle* fHandle)
{
  return fHandle != NULL;
}


XAIT::Common::Callback::FileIOCallback::FileHandle* XaitmentVBSFileHandler::getInvalidFileHandle() const
{
  return NULL;
}


XAIT::int32 XaitmentVBSFileHandler::openFile(XAIT::Common::Callback::FileIOCallback::FileHandle*& fHandle, const XAIT::Common::String& fileName, const XAIT::Common::Callback::FileIOCallback::FileMode fMode)
{
  void* fileHandle = NULL;

  if (fMode == FMODE_OPEN_READ)
  {
    fileHandle = open(fileName.getConstCharPtr());
  }
  fHandle = fileHandle;
  return 0;
}


void XaitmentVBSFileHandler::closeFile(XAIT::Common::Callback::FileIOCallback::FileHandle* fHandle)
{
  if (isFileHandleValid(fHandle))
    close(fHandle);
}


XAIT::int64 XaitmentVBSFileHandler::getFileSize(XAIT::Common::Callback::FileIOCallback::FileHandle* fHandle)
{
  XAIT::int64 currentPos = tellg(fHandle);

  XAIT::int64 begin,end;
  seekg(fHandle, 0, 0);
  begin = tellg(fHandle);

  seekg(fHandle, 0, 2);
  end = tellg(fHandle);

  seekg(fHandle, currentPos, 0);

  return (XAIT::int64) (end-begin);
}


XAIT::int32 XaitmentVBSFileHandler::readFromFile(void* dstBuffer, const XAIT::int32 numBytes, XAIT::Common::Callback::FileIOCallback::FileHandle* fHandle)
{
  XAIT::int32 startPos = tellg(fHandle);

  XAIT::int32 numBytesToRead = numBytes;
  XAIT::int32 maxBytesToRead = getFileSize(fHandle) - startPos;

  if (numBytesToRead > maxBytesToRead)
    numBytesToRead = maxBytesToRead;

  read(fHandle, dstBuffer, numBytesToRead);
  XAIT::int32 endPos = tellg(fHandle);

  return endPos - startPos;
}


XAIT::int32 XaitmentVBSFileHandler::writeToFile(const void* srcBuffer, const XAIT::int32 numBytes, XAIT::Common::Callback::FileIOCallback::FileHandle* fHandle)
{
  return 0;
}


XAIT::int64 XaitmentVBSFileHandler::getFilePosition(XAIT::Common::Callback::FileIOCallback::FileHandle* fHandle)
{
  XAIT::int64 pos = tellg(fHandle);
  return pos;
}


XAIT::int64 XaitmentVBSFileHandler::seekFile(const XAIT::int64 offset, const XAIT::Common::Callback::FileIOCallback::SeekOrigin origin, XAIT::Common::Callback::FileIOCallback::FileHandle* fHandle)
{
  switch (origin)
  {
  case SEEKORIG_CUR:
    seekg(fHandle, offset, 1);
    break;
  case SEEKORIG_END:
    seekg(fHandle, offset, 2);
    break;
  case SEEKORIG_START:
    seekg(fHandle, offset, 0);
    break;
  }
  return getFilePosition(fHandle);
}

XAIT::Common::String XaitmentVBSFileHandler::getFullFileName(const XAIT::Common::String& path)
{
  return path;
}


int FileSize(const char* name)
{
  void* fHandle = open(name);
  if (!fHandle) return -1;

  int begin,end;
  seekg(fHandle, 0, 0);
  begin = tellg(fHandle);

  seekg(fHandle, 0, 2);
  end = tellg(fHandle);
  
  close(fHandle);
  return end-begin;
}

// creates xaitment memory stream from file
XAIT::Common::IO::Stream* GetStreamFromFile(const char* filename)
{
  int size = FileSize(filename);
  XAIT::Common::IO::Stream* result;
  result = new(XAIT::Map::MapMemoryPool::getLowAllocThreadSafeAllocator()) XAIT::Common::Memory::MemoryStream(filename, size); 
  if (size <= 0) return result;

  void* fHandle = open(filename);
  if (fHandle)
  {
    char* buffer = new char[size];
    seekg(fHandle, 0, 0);
    read(fHandle, buffer, size);
    close(fHandle);
    
    result->seek(0, XAIT::Common::IO::Stream::SEEKORIG_START);
    result->write(buffer, size);
    result->seek(0, XAIT::Common::IO::Stream::SEEKORIG_START);
    delete [] buffer;
  }

  return result;
}

