#include "navmeshcache.h"


NavmeshCache::NavmeshCache(CacheFunctions* functions)
{
  _globalNavmesh = NULL; 
  _totalFileSize = 0;
  _cacheFunctions = functions;
}

NavmeshCache::~NavmeshCache()
{
  Clear();
}

bool NavmeshCache::RemoveOldest()
{
  if (_map.empty()) return false; // nothing can be removed

  NavmeshMap::iterator iter;

  unsigned long long oldestTime = GetTickCount() + 1; // set time to future
  NavmeshMap::iterator oldestItem = _map.end();

  for (iter = _map.begin(); iter != _map.end(); iter++)
  {
    if (iter->second.lastUsed < oldestTime)
    {
      if (iter->second.fileSize == 0) continue;
      oldestTime = iter->second.lastUsed;
      oldestItem = iter;
    }
  }

  if (oldestItem == _map.end()) return false; 

  _cacheFunctions->DeleteNavmesh(oldestItem->second.navmesh);
  _cacheFunctions->DeleteBuildCache(oldestItem->second.buildCache);
  _totalFileSize -= oldestItem->second.fileSize;
  if (_totalFileSize < 0) _totalFileSize = 0; // should never happen!
  _map.erase(oldestItem);
  return true;
}

void* NavmeshCache::GetNavmesh(const char* filename)
{
  if (!_cacheFunctions) return NULL;
  std::string name(filename);

  // check if navmesh already exists
  if (_globalNavmesh && _globalNavmeshName == name)
  {
    return _globalNavmesh;
  }

  NavmeshMap::iterator iter;
  iter = _map.find(name);
  if (iter != _map.end())
  {
    iter->second.lastUsed = GetTickCount();
    return iter->second.navmesh;
  }

  // navmesh doesn't exist -> try to load it
  return Load(filename);
}

// rebuilds navmesh using given geometry (creates new one if not in cache) - should be called when object geometry has been changed (usually because of animations) or when user wants to dynamically create navmesh that is not stored in file.
void NavmeshCache::Build(const char* filename, void* geomObj)
{
  if (!filename) return;
  if (!geomObj) return;
  std::string name(filename);

  void* navmesh = NULL;
  void* buildCache = NULL;

  NavmeshMap::iterator iter = _map.find(name);
  if (iter == _map.end())
  {
    // tries to load navmesh from disk to speed up build calculations
    if (Load(filename))
      iter = _map.find(name);
  }
  
  if (iter != _map.end())
  {
    navmesh = iter->second.navmesh;
    buildCache = iter->second.buildCache;

    _totalFileSize -= iter->second.fileSize;
    if (_totalFileSize < 0) _totalFileSize = 0;
    _map.erase(iter);
  }
  
  bool result = _cacheFunctions->Build(geomObj, navmesh, buildCache);
  if (result)
  {
    CacheItem item;
    item.buildCache = buildCache;
    item.navmesh = navmesh;
    item.lastUsed = GetTickCount();
    item.fileSize = 0;
    _map[name] = item;
  }
  else
  {
    _cacheFunctions->DeleteNavmesh(navmesh);
    _cacheFunctions->DeleteBuildCache(buildCache);
  }
}

// updates navmesh (restores from disk if no input geometry) - should be called when user added some obstacles to existing navmesh
void NavmeshCache::Update(const char* filename, void* geom)
{
  if (!filename) return;
  
  if (geom)
  {
    Build(filename, geom);
  }
  else
  {
    Load(filename);
  }
}

// removes all items from cache (including modified navmeshes) - should be called always after mission end
void NavmeshCache::Clear() 
{
  if (_cacheFunctions)
  {
    NavmeshMap::iterator iter;
    for (iter = _map.begin(); iter != _map.end(); iter++)
    {
      _cacheFunctions->DeleteNavmesh(iter->second.navmesh);
      _cacheFunctions->DeleteBuildCache(iter->second.buildCache);
    }

    _cacheFunctions->DeleteNavmesh(_globalNavmesh);
  }
  _map.clear();
  _globalNavmesh = NULL;
  _globalNavmeshName.clear();
  _totalFileSize = 0;
}


void NavmeshCache::Delete(const char* filename)
{
  if (!_cacheFunctions) return;
  if (!filename) return;
  std::string name(filename);
  NavmeshMap::iterator iter;
  iter = _map.find(name);
  if (iter != _map.end())
  {
    _cacheFunctions->DeleteNavmesh(iter->second.navmesh);
    _cacheFunctions->DeleteBuildCache(iter->second.buildCache);
    _totalFileSize -= iter->second.fileSize;
    if (_totalFileSize < 0) _totalFileSize = 0;
    _map.erase(iter);
  }
}

void* NavmeshCache::Load(const char* filename)
{
  if (!_cacheFunctions) return NULL;
  if (!filename) return NULL;
  std::string name(filename);
  
  // deletes existing item
  Delete(filename);

  int fileSize = _cacheFunctions->GetFileSize(filename);
  if (fileSize <= 0) return NULL;  // can't create navmesh - file not found

  if (fileSize > MAX_CACHE_SIZE)  // temporary hack - everything bigger than 2MB is global navigation mesh
  {
    _cacheFunctions->DeleteNavmesh(_globalNavmesh);
    _globalNavmesh = _cacheFunctions->LoadNavmesh(filename);
    _globalNavmeshName = name;
    return _globalNavmesh;
  }

  while (_totalFileSize + fileSize > MAX_CACHE_SIZE) // delete oldest items until there is enought memory to create new navmesh
  {
    bool removed = RemoveOldest();
    if (!removed) return NULL;    // cache is empty but we still can't create new navmesh - this should never happen
  }

  CacheItem item;
  item.fileSize = fileSize;
  item.lastUsed = GetTickCount();
  item.navmesh = _cacheFunctions->LoadNavmesh(filename);

  if (!item.navmesh) 
  {
    OutputDebugString("Cache Error - Cant open navmesh file!\n");
    return NULL;       // can't create navmesh
  }

  item.buildCache = _cacheFunctions->LoadBuildCache(filename);

  _map.insert(std::pair<std::string, CacheItem>(name, item));
  _totalFileSize += fileSize;
  return item.navmesh;
}
