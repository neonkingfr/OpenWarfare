#ifndef _BUILDNAVMESH_HPP
#define _BUILDNAVMESH_HPP

#include <Common/GeometryObject.h>

const float NavMeshCubeSize = 0.15f;
const float NavMeshUnitHeight = 1.6f;
const float NavMeshUnitRadius = 0.20f;
const float NavMeshStepHeight = 0.6f;
const float NavMeshSectorSize = 16.0f;
const float NavMeshMaxSlope= 60.0f;

bool BuildNavmesh(const GeometryObject* gObj, void*& navmesh, void*& buildCache);
#endif