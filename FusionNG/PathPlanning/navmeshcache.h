#ifndef _NAVMESHCACHE_H
#define _NAVMESHCACHE_H

#include <windows.h>
#include <string>
#include <map>

#define MAX_CACHE_SIZE 2097152 // 2MB ought to be enough for anybody.

class CacheFunctions
{
public:
  virtual void* LoadNavmesh(const char* filename) = 0;
  virtual void* LoadBuildCache(const char* filename) = 0;
  virtual void DeleteNavmesh(void* navmesh) = 0;
  virtual void DeleteBuildCache(void* buildCache) = 0;
  virtual int GetFileSize(const char* name) = 0;
  virtual bool Build(const void* geometry, void*& navmesh, void*& buildCache) = 0;
};

struct CacheItem
{
  void* navmesh;        // pointer to the navigation mesh
  void* buildCache;     // build cache - speeds up next geometry updates (optional)
  unsigned long long lastUsed; // time when this item was used (older items will be deleted first)
  unsigned int fileSize;       // size of this item. If 0, item was dynamically updated/created and can't be deleted until current mission ends.
  CacheItem(){navmesh = NULL; buildCache = NULL; lastUsed = 0; fileSize = 0;}
};

typedef std::map<std::string, CacheItem> NavmeshMap;


class NavmeshCache
{
  void* _globalNavmesh;
  std::string _globalNavmeshName;
  NavmeshMap _map;
  unsigned int _totalFileSize;
  CacheFunctions* _cacheFunctions;

  bool RemoveOldest();    // removes the oldest item from cache (but ignores items with zero size). Returns false if nothing can be removed.
  
  void* Load(const char* filename);  // reloads navmesh from disk (deletes all changes made by Update/Build). 
  void Delete(const char* filename); // deletes item from cache
public:
  NavmeshCache(CacheFunctions* functions);
  ~NavmeshCache();
  
  void* GetNavmesh(const char* filename);      // returns requested navmesh (if not in cache, tries to load it from disk)
  void Build(const char* filename, void* geomObj);   // rebuilds navmesh using given geometry (creates new one if not in cache) - should be called when object geometry has been changed (usually because of animations) or when user wants to dynamically create navmesh that is not stored in file.
  void Update(const char* filename, void* geomObj = NULL);   // updates navmesh (restores from disk if no input geometry) - should be called when user added some obstacles to existing navmesh
  void Clear(); // removes all items from cache (including modified navmeshes) - should be called always after mission end
};


#endif
