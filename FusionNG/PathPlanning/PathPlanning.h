#include <windows.h>
#include <Common/GeometryObject.h>

#define VBSPLUGIN_EXPORT __declspec(dllexport)

//VBSPLUGIN_EXPORT void WINAPI OnSimulationStep(float deltaT);
VBSPLUGIN_EXPORT const char* WINAPI PluginFunction(const char *input);
VBSPLUGIN_EXPORT void WINAPI RegisterReturnPathFnc(void *returnPathFnc);
VBSPLUGIN_EXPORT void WINAPI UpdateNavmesh(const char* navmeshName, GeometryObject* geom);
VBSPLUGIN_EXPORT void WINAPI SetNavmeshBlockers(const char* navmeshName, GeometryObject* geom);
VBSPLUGIN_EXPORT void WINAPI DrawNavmesh(const char* objectName);
VBSPLUGIN_EXPORT void WINAPI DrawNavmeshExt(void* data, const char* navmesh, bool draw);
VBSPLUGIN_EXPORT bool WINAPI IsOnNavmesh(const char* navmeshName, float* pos);
VBSPLUGIN_EXPORT void WINAPI RegisterGetObjectTransformFnc(void *getObjectTransform);

typedef int (WINAPI * ExecuteCommandType)(const char *command, char *result, int resultLength);
typedef void (WINAPI * ReturnPathType)(int requestID, float* positions, int n);
typedef void (WINAPI * GetObjectTransformType)(void* data, float* matrix);

#define USE_PRIMITIVES_MANAGER 1
#if !USE_PRIMITIVES_MANAGER
#include "../../Common/Essential/RefCount.hpp"
#include "../../Common/BIColor.h"
#include "../../Common/BIVector.h"
#include "../../Common/BIMatrix.h"

struct IDirect3DDevice9;

// parameters that may be passed to the plugin
struct VBSParameters
{
  const char *_dirInstall;  // VBS installation directory
  const char *_dirUser;     // user's VBS directory
  IDirect3DDevice9 *_device; // rendering D3D device; is NULL if !_VBS3_PLUGIN_DEVICE
};

// camera frustum parameters
struct FrustumSpec
{
  // camera position
  float _pointPosX;
  float _pointPosY;
  float _pointPosZ;

  // camera speed, used for OpenAL sounds
  float _pointSpeedX;
  float _pointSpeedY;
  float _pointSpeedZ;

  // camera view direction
  float _viewDirX;
  float _viewDirY;
  float _viewDirZ;

  // camera up-view
  float _viewUpX;
  float _viewUpY;
  float _viewUpZ;

  // camera projection angle tangents
  // Beware: for now, top/bottom and left/right pairs are used as single (symmetric) values
  float _projTanTop;
  float _projTanBottom;
  float _projTanLeft;
  float _projTanRight;

  // near, far distance clipping
  float _clipDistNear;
  float _clipDistFar;
  float _clipDistFarShadow;
  float _clipDistFarSecShadow;
  float _clipDistFarFog;

  // standard constructor
  FrustumSpec():
  _pointPosX(0), _pointPosY(0), _pointPosZ(0), _pointSpeedX(0), _pointSpeedY(0), _pointSpeedZ(0),
    _viewDirX(0), _viewDirY(0), _viewDirZ(0), _viewUpX(0), _viewUpY(0), _viewUpZ(0),
    _projTanTop(0), _projTanBottom(0), _projTanLeft(0), _projTanRight(0),
    _clipDistNear(0), _clipDistFar(0), _clipDistFarShadow(0), _clipDistFarSecShadow(0), _clipDistFarFog(0)
  {}

  // auxiliary setting functions
  void SetPos(float posX, float posY, float posZ) {_pointPosX = posX; _pointPosY = posY; _pointPosZ = posZ;}
  void SetSpeed(float speedX, float speedY, float speedZ) {_pointSpeedX = speedX; _pointSpeedY = speedY; _pointSpeedZ = speedZ;}
  void SetDir(float dirX, float dirY, float dirZ) {_viewDirX = dirX; _viewDirY = dirY; _viewDirZ = dirZ;}
  void SetUp(float upX, float upY, float upZ) {_viewUpX = upX; _viewUpY = upY; _viewUpZ = upZ;}
  void SetProj(float top, float bottom, float left, float right) {_projTanTop = top; _projTanBottom = bottom; _projTanLeft = left; _projTanRight = right;}
  void SetClip(float nearStd, float farStd, float farShadow, float farSecShadow, float farFog)
  {
    _clipDistNear = nearStd; _clipDistFar = farStd; _clipDistFarShadow = farShadow; _clipDistFarSecShadow = farSecShadow; _clipDistFarFog = farFog;
  }
};

class Primitive3D : public RefCount
{
  friend class PrimitivesManager;

protected:
  BI_Vector3* _vertices;    // vertices of the primitive (does not have to be triangulated)
  int _nVertices;           // number of primitive vertices
  BI_Color _color;          // color of the primitive

public:
  Primitive3D(int size);                  
  virtual ~Primitive3D();
  void SetColor(const BI_Color& color);                       // set color of the whole primitive (does not support individual vertex colors yet)

  virtual int NTriangles() const = 0;                         // return number of the triangles needed for drawing this primitive
  virtual void AddTrianglesToBuffer(char* buffer) const = 0;  // adds triangles of this primitive to the draw buffer (pointer is expected to already point at the corresponding location in the buffer)
};

class Triangle3D: public Primitive3D
{
public:
  Triangle3D(const BI_Vector3& v1, const BI_Vector3& v2, const BI_Vector3& v3);

  virtual int NTriangles() const {return 1;}
  virtual void AddTrianglesToBuffer(char* buffer) const;
};
#endif