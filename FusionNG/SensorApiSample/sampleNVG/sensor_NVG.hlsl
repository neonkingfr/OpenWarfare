// The SENSOR_SHADER macro contains helper for accessing sensor peel data & helpers

// Accessing sensor data:
// Use:
//   sensorPeelData pd = getSensorPeelData(In.uv);
// sensorPeelData struct contains:
//   float4 rgba;    /* 'Out the window' RGB color + alpha information*/
//   int matID;      /* Material index (0-255)*/
//   float tiHT;     /* Engine TI half cooling time */
//   float tiHeat;   /* Engine TI heat emit maps */
//   float sunLastH; /*How much of the last hour this pixel has been on the sun (0.0-1.0)*/
//   float nDotL;    /*Dot product of surface normal & main light ('how much is this pixel surface towards the sun', 0.0 - 1.0)*/
//   float3 lightIndirect; /*Indirect light color*/
//   float3 lightDirect;   /*Direct light color*/
//   float3 lightSpecular; /* Specular light color*/

// Globals:
//  lightDirSunMoon.xyz = Main light (sun or moon) direction in world space 
//  lightDirSunMoon.w   = Main light sun or moon factor (0 = moon, 1 = sun) 
//  clipPlane.x  = Near clip plane distance (0.0 - 1.0)
//  clipPlane.y  = Far clip plane distance (0.0 - 1.0)
//  camPos.xyz   = Camera position X Y Z in world space
//  camDir.xyz   = Camera direction vector X Y Z 
//  camUp.xyz    = Camera up vector X Y Z 

SENSOR_SHADER;

sampler1D nvgColorGradient       : register(s0);
sampler2D nvgNoiseMap            : register(s1);
float4 rtSize                    : register(c0); // rendertarget size in X & Y + noise map offsets

float4 psmain(in VS_OUTPUT In) : COLOR
{	
	sensorPeelData pd = getSensorPeelData(In.uv);
		
	// Get luminance from RGB color, heavy emphasis on red color (near IR)
	float lum = (pd.rgba.r * 0.75) + (pd.rgba.g * 0.2) + (pd.rgba.b * 0.05);

	float nvgBoost = 0.5; // How much this nvg boosts light (simple effect for demo)

	// Apply lighting (multiply by indirect+direct light, then add specular light)
	lum *= (pd.lightIndirect+pd.lightDirect+nvgBoost);
	lum += pd.lightSpecular;
	
	// Mix in a bit of thermal data
	lum += dot(saturate(pd.tiHeat), float3(0.33, 0.33, 0.33)) * 0.5;

	// Add some noise
	float2 noiseUV = In.uv / (512.0f/rtSize.x, 512.0f/rtSize.y);
	float4 noise = tex2D(nvgNoiseMap, noiseUV + rtSize.zw);
	lum += noise*1.5;
	
	// Do color gradient
	float3 color = tex1D(nvgColorGradient, saturate(lum));
	
	return float4(color, 1.0f);	
}