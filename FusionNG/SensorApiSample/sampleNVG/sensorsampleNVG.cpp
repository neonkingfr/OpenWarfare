// Very simple sample - NVG like color gradient effect + noise
// Copy "sensor_NVG.hlsl", "nvg_sample_colormap.tga" and "nvg_sample_noise.tga" to VBS3 directory

#include "sensorsampleNVG.h"

// This is called during sensor request, set wanted sensor parameters here
void sensorsampleNVG::updateSensorParameters()
{
  // Wanted sensor resolution. Note increased resolution will significantly increase required video ram
  _width = 1024;
  _height = 1024;

  // Number of depth peels wanted, more peels increases video ram usage and decreases render performance
  _peels = 3;
}

void sensorsampleNVG::createResources(IDirect3DDevice9 *d3d)
{
  // Compile our sensor pixel shader
  _ps = compilePixelShader(d3d, "sensor_NVG.hlsl", "psmain");

  // Create textures
  D3DXCreateTextureFromFile(d3d, "nvg_sample_colormap.tga", &_cgrad);
  D3DXCreateTextureFromFile(d3d, "nvg_sample_noise.tga", &_noise);
}

void sensorsampleNVG::freeResources()
{
  // Release all direct3D handles
  SAFE_RELEASE(_ps);
  SAFE_RELEASE(_cgrad);  
  SAFE_RELEASE(_noise);  
}

// Resource management opportunity called for each frame
void sensorsampleNVG::manageResources(IDirect3DDevice9 *dev)
{
  // Nothing to do here for us
}

// Sensor postprocess pass - data has been copied for us here by sensorInterfaceBase.
// We render direct to VBS backbuffer.
void sensorsampleNVG::doPostprocess(VBS2CBInterface *cbi, IDirect3DSurface9 *renderTarget, IDirect3DTexture9 *shadowmap, pluginLightDir *mainLight, const FrustumSpecNG &frustum)
{
  if(!ready()) return; // Init has failed?

  // This prepares the device for drawing (set default state, vertex shader, etc)
  prepareDraw(cbi, mainLight, frustum);

  // Draw data from each peel, going backwards (furthest peel first)
  for(int peel=numPeels()-1;peel>=0;peel--)
  {
    // Prepares peel rendering, sets peel data to texture smaplers
    preparePeel(cbi, peel);

    // Set our sensor PS
    cbi->SetPixelShader(_ps);

    // Set color gradient (nvgColorGradient in hlsl)
    cbi->SetTexture(0, _cgrad);
    cbi->SetSamplerState(0, D3DSAMP_ADDRESSU, D3DTADDRESS_CLAMP);
    cbi->SetSamplerState(0, D3DSAMP_ADDRESSV, D3DTADDRESS_CLAMP);

    // Noise texture (nvgNoiseMap in hlsl)
    cbi->SetTexture(1, _noise);
    cbi->SetSamplerState(1, D3DSAMP_ADDRESSU, D3DTADDRESS_WRAP);
    cbi->SetSamplerState(1, D3DSAMP_ADDRESSV, D3DTADDRESS_WRAP);

    // Set some shader constants (rtSize register C0)
    float noiseOffX = (float)(rand()%100)/100.0f;
    float noiseOffY = (float)(rand()%100)/100.0f;
    float rtSize[4] = {getWidth(), getHeight(), noiseOffX, noiseOffY};
    cbi->SetPixelShaderConstantF(0, rtSize, 1);

    // Draw helper - it draws a quad that covers whole renderTarget
    draw(cbi, renderTarget);
  }
}