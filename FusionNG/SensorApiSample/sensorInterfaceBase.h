#ifndef __DRAWSAMPLE_H__
#define __DRAWSAMPLE_H__

#include <windows.h>
#include <d3d9.h>
#include <d3dx9.h>
#include <vector>

#include "sensorplugin.h"
#include "../../Common/Essential/AppFrameWork.hpp"

#define NUM_MRT 4 // We always get 4 MRT buffers from engine (D3D9 limitation)

// Data is set to these texture stages in prepareDraw helper
#define STAGE_MRT0  11
#define STAGE_MRT1  12
#define STAGE_MRT2  13
#define STAGE_MRT3  14
#define STAGE_DEPTH 15

#define SAFE_RELEASE(p) {if((p)){(p)->Release();(p)=NULL;}}

// Simple structure to hold data for a sensor peel
struct sensorPeel
{
  IDirect3DTexture9 *_mrt[NUM_MRT]; // MRT buffers data (note: one peel only)
  IDirect3DTexture9 *_depth;  // Depth buffer data (one peel only)

  sensorPeel() {
    for(int i=0;i<NUM_MRT;i++) _mrt[i] = NULL;
    _depth = NULL;
  }
  ~sensorPeel() {
    for(int i=0;i<NUM_MRT;i++) SAFE_RELEASE(_mrt[i]);
    SAFE_RELEASE(_depth);
  }
};

// Base class for sensor, helpers for managing surface data, etc.
class sensorInterfaceBase
{
public:
  sensorInterfaceBase(const char *parameter, int numDefaultMaterials);
  ~sensorInterfaceBase();

  void initialize(IDirect3DDevice9 *d3d);

  // Virtual functions for actual sample
  virtual void createResources(IDirect3DDevice9 *d3d) =0;
  virtual void freeResources() =0;
  virtual void manageResources(IDirect3DDevice9 *dev) =0;
  virtual void doPostprocess(VBS2CBInterface *cbi, IDirect3DSurface9 *renderTarget, IDirect3DTexture9 *shadowmap, pluginLightDir *mainLight, const FrustumSpecNG &frustum) =0;
  virtual void getMaterialPriority(char materialPriority[256]);
  virtual unsigned char getDefaultMaterial(int defMatId);
  virtual unsigned char getTerrainMaterial(int terMatId);

  // Buffers management
  void doCopyBuffers(VBS2CBInterface *cbi, int peelIndex, IDirect3DSurface9 **peelSurfaces, IDirect3DSurface9 *depth);
  void prepareBuffers(IDirect3DDevice9 *dev, int width, int height, int peels, D3DFORMAT *fmtMrt, D3DFORMAT fmtDepth);

  // This sets _width, _height and _numPeels
  virtual void updateSensorParameters();

  int numPeels() const {return _peels;};
  bool ready() const {return _ready && numPeels()>0;};
  int getWidth() const {return _width;};
  int getHeight() const {return _height;};

protected:

  // Some helper functions for actual sample
  IDirect3DPixelShader9* compilePixelShader(IDirect3DDevice9 *d3d, const char *hlsl, const char *main);
  void setDefaultState(VBS2CBInterface *cbi);
  void prepareDraw(VBS2CBInterface *cbi, pluginLightDir *mainLight, const FrustumSpecNG &frustum);
  void preparePeel(VBS2CBInterface *cbi, int peelIndex);
  void draw(VBS2CBInterface *cbi, IDirect3DSurface9 *renderTarget);

  int _width, _height;
  int _peels;

private:
  bool _ready;
  std::vector<sensorPeel> _peelData;
  IDirect3DVertexShader9 *_vs; // Simple VS for 2D drawing
  IDirect3DVertexDeclaration9 *_vertexDecl;
};

#endif