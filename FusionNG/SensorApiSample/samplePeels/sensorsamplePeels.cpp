// Copy "sensor_peels.hlsl" to VBS3 directory

#include "sensorsamplePeels.h"

// This is called during sensor request, set wanted sensor parameters here
void sensorsamplePeels::updateSensorParameters()
{
  // Wanted sensor resolution. Increased resolution will significantly increase required video ram
  _width = 512;
  _height = 512;

  // Number of depth peels wanted, more peels increases video ram usage and decreases render performance
  _peels = 5;
}

void sensorsamplePeels::createResources(IDirect3DDevice9 *d3d)
{
  // Compile our sensor pixel shader
  _ps = compilePixelShader(d3d, "sensor_peels.hlsl", "psmain");
}

void sensorsamplePeels::freeResources()
{
  // Release all direct3D handles
  SAFE_RELEASE(_ps); 
}

unsigned char sensorsamplePeels::getDefaultMaterial(int defMatId)
{
  if(defMatId < NSensorDefaultMaterial)
  {
    // Set default materials that we use in sensor shader to detect materials
    // These are used when stageMAT is not set

    if(defMatId == MatVehicle || defMatId == MatPerson)
    {
      // Vehicles & persons to material 1, we priorize these in getMaterialPriority
      return 1;
    }
    else if(defMatId == MatTree || defMatId == MatBush || defMatId == MatBuilding || defMatId == MatObject)
    {
      // Tress, bushes, buildings, objects to material 2
      // These are drawn transparent in shader so persons & vehicles are seen through them
      return 2;
    }
    else if(defMatId == MatSky)
    {
      // Sky to material 3, this is drawn black in shader
      return 3;
    }
    else
    {
      // Everything else mat 0, these are not transparent
      return 0;
    }
  }
  else
    return 0; // Unknown materials to us
}

unsigned char sensorsamplePeels::getTerrainMaterial(int terMatId)
{
  if(terMatId < NSensorTerrainMaterial)
  {
    // Set terrain materials from ID 100 onwards
    // We dont actually need different IDs in this sample, but anything above 100 is priorized below
    return 100+terMatId;
  }
  else
    return 0; // Unknown materials to us
}

void sensorsamplePeels::getMaterialPriority(char materialPriority[256])
{
  // Priorized material IDs are set here. Only they are drawn into the last peel.
  // This helps with not having unimportant transparent materials mask important materials completely by using up all the peels.
  // Normally you would always set terrain materials (set in getTerrainMaterial) as priorized here.

  // First, set all materials as non-priorized
  for(int i=0;i<256;i++)
  {
    // Setting value to zero is unpriorized, one is priorized
    materialPriority[i] = 0;
  }

  // Priorize default material we set in getDefaultMaterial for persons & vehicles (1)
  // And material for sky (3)
  materialPriority[1] = 1;
  materialPriority[3] = 1;

  // Priorize all terrain materials (set in getTerrainMaterial) 
  for(int i=100;i<256;i++)
  {
    materialPriority[i] = 1;
  }
}

// Sensor postprocess pass - data has been copied for us here by sensorInterfaceBase.
// We render direct to VBS backbuffer.
void sensorsamplePeels::doPostprocess(VBS2CBInterface *cbi, IDirect3DSurface9 *renderTarget, IDirect3DTexture9 *shadowmap, pluginLightDir *mainLight, const FrustumSpecNG &frustum)
{
  if(!ready()) return; // Init has failed?

  // This prepares the device for drawing (set default state, vertex shader, etc)
  prepareDraw(cbi, mainLight, frustum);

  // Draw data from each peel, going backwards (furthest peel first)
  for(int peel=numPeels()-1;peel>=0;peel--)
  {
    // Prepares peel rendering, sets peel data to texture smaplers
    preparePeel(cbi, peel);

    // Disable SRGB write. This will make writes to be linear, which is a bit easier for this type of sensor
    // For correct lighting SRGB must be used, but as in this sensor we use no visible light and thus no lighting it is not necessary
    cbi->SetRenderState(D3DRS_SRGBWRITEENABLE, 0);

    // Set our sensor PS
    cbi->SetPixelShader(_ps);

    // Draw helper - it draws a quad that covers whole renderTarget
    draw(cbi, renderTarget);
  }
}