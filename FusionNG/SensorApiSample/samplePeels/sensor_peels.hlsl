// The SENSOR_SHADER macro contains helper for accessing sensor peel data & helpers

// Accessing sensor data:
// Use:
//   sensorPeelData pd = getSensorPeelData(In.uv);
// sensorPeelData struct contains:
//   float4 rgba;    /* 'Out the window' RGB color + alpha information*/
//   int matID;      /* Material index (0-255)*/
//   float tiHT;     /* Engine TI half cooling time */
//   float tiHeat;   /* Engine TI heat emit maps */
//   float sunLastH; /*How much of the last hour this pixel has been on the sun (0.0-1.0)*/
//   float nDotL;    /*Dot product of surface normal & main light ('how much is this pixel surface towards the sun', 0.0 - 1.0)*/
//   float3 lightIndirect; /*Indirect light color*/
//   float3 lightDirect;   /*Direct light color*/
//   float3 lightSpecular; /* Specular light color*/

// Globals:
//  lightDirSunMoon.xyz = Main light (sun or moon) direction in world space 
//  lightDirSunMoon.w   = Main light sun or moon factor (0 = moon, 1 = sun) 
//  clipPlane.x  = Near clip plane distance (0.0 - 1.0)
//  clipPlane.y  = Far clip plane distance (0.0 - 1.0)
//  camPos.xyz   = Camera position X Y Z in world space
//  camDir.xyz   = Camera direction vector X Y Z 
//  camUp.xyz    = Camera up vector X Y Z 

SENSOR_SHADER;

// Some default materials set in sensorsamplePeels::getDefaultMaterial
#define  matPerson 1  // Person or vehicle
#define  matWall   2  // buildings, objects. Make these transparent
#define  matSky    3

float4 psmain(in VS_OUTPUT In) : COLOR
{	
	sensorPeelData pd = getSensorPeelData(In.uv);
	
	if(pd.matID == matSky)
	{
		// Sky, draw opaque static color
		return float4(0.4,0.4,0.4,1);
	}
	
	// Draw everything as medium gray
	float lum = 0.33;
	
	// Mix in a bit of red color for detail
	lum += pd.rgba.r * 0.05;
	
	// Add ti heat, this contains vehicle engines, tires, weapons and metabolism
	lum += pd.tiHeat;
	
	// Make objects with matWall or matPerson transparent, everything else opaque
	float alpha;
	if(pd.matID == matWall || pd.matID == matPerson)
	{	
		alpha = 0.85;
	}
	else
	{
		alpha = 1.0;
	}
		
	// Set color to luminance value
	// Multiply alpha with pixel alpha. this is so transparent objects are drawn transparent (ie. windows)
	return float4(lum,lum,lum, alpha*pd.rgba.a);
}



