#ifndef __SENSORPLUGIN_H__
#define __SENSORPLUGIN_H__

#include <windows.h>
#include <d3d9.h>

#define VBSPLUGIN_EXPORT __declspec(dllexport)

// Command function declaration
typedef int (WINAPI * ExecuteCommandType)(const char *command, char *result, int resultLength);
// camera frustum parameters
struct FrustumSpecNG
{
  // camera position
  double pointPosX;
  double pointPosY;
  double pointPosZ;

  // camera speed, used for OpenAL sounds
  float pointSpeedX;
  float pointSpeedY;
  float pointSpeedZ;

  // camera view direction
  float viewDirX;
  float viewDirY;
  float viewDirZ;

  // camera up-view
  float viewUpX;
  float viewUpY;
  float viewUpZ;

  // camera projection angle tangents
  // Beware: for now, top/bottom and left/right pairs are used as single (symmetric) values
  float projTanTop;
  float projTanBottom;
  float projTanLeft;
  float projTanRight;

  // near, far distance clipping
  float clipDistNear;
  float clipDistFar;
  float clipDistFarShadow;
  float clipDistFarSecShadow;
  float clipDistFarFog;

  // standard constructor
  FrustumSpecNG():
    pointPosX(0), pointPosY(0), pointPosZ(0), pointSpeedX(0), pointSpeedY(0), pointSpeedZ(0),
    viewDirX(0), viewDirY(0), viewDirZ(0), viewUpX(0), viewUpY(0), viewUpZ(0),
    projTanTop(0), projTanBottom(0), projTanLeft(0), projTanRight(0),
    clipDistNear(0),clipDistFar(0), clipDistFarShadow(0), clipDistFarSecShadow(0), clipDistFarFog(0)
  {}
};

// CB interface, used to do drawing ie. in paramDrawnPass
// Functions are the same or very similar as D3D device, but no value is returned.
// Use D3D9 debug runtime to identify problems
interface VBS2CBInterface
{
  virtual void SetTexture(DWORD Sampler, IDirect3DBaseTexture9 * pTexture)=0;
  virtual void SetRenderTarget(DWORD RenderTargetIndex, IDirect3DSurface9* pRenderTarget)=0;
  virtual void DrawPrimitive(D3DPRIMITIVETYPE PrimitiveType, UINT StartVertex, UINT PrimitiveCount)=0;
  virtual void SetPixelShader(IDirect3DPixelShader9* pShader)=0;
  virtual void SetVertexShader(IDirect3DVertexShader9* pShader)=0;
  virtual void SetVertexDeclaration(IDirect3DVertexDeclaration9* pDecl)=0;
  virtual void BeginScene()=0;
  virtual void EndScene()=0;
  virtual void SetStreamSource(UINT StreamNumber, IDirect3DVertexBuffer9 * pStreamData, UINT OffsetInBytes, UINT Stride)=0;
  virtual void DrawPrimitiveUP(D3DPRIMITIVETYPE PrimitiveType, UINT PrimitiveCount, CONST void* pVertexStreamZeroData, UINT VertexStreamZeroStride)=0;
  virtual void SetRenderState(D3DRENDERSTATETYPE State, DWORD Value)=0;
  virtual void SetSamplerState(DWORD Sampler, D3DSAMPLERSTATETYPE Type, DWORD Value)=0;
  virtual void SetIndices(IDirect3DIndexBuffer9 * pIndexData)=0;
  virtual void Clear(DWORD Count, CONST D3DRECT * pRects, DWORD Flags, D3DCOLOR Color, float Z, DWORD Stencil)=0;
  virtual void SetVertexShaderConstantB(UINT StartRegister, CONST BOOL * pConstantData, UINT BoolCount)=0;
  virtual void SetVertexShaderConstantF(UINT StartRegister, CONST float * pConstantData, UINT Vector4fCount)=0;
  virtual void SetVertexShaderConstantI(UINT StartRegister, CONST int * pConstantData, UINT Vector4iCount)=0;
  virtual void SetPixelShaderConstantB(UINT StartRegister, CONST BOOL * pConstantData, UINT BoolCount)=0;
  virtual void SetPixelShaderConstantF(UINT StartRegister, CONST float * pConstantData, UINT Vector4fCount)=0;
  virtual void SetPixelShaderConstantI(UINT StartRegister, CONST int * pConstantData, UINT Vector4iCount)=0;
  virtual void DrawIndexedPrimitive(D3DPRIMITIVETYPE Type, INT BaseVertexIndex, UINT MinIndex, UINT NumVertices, UINT StartIndex, UINT PrimitiveCount)=0; // Available in 91072
  virtual void StretchRect(IDirect3DSurface9 * pSourceSurface, CONST RECT * pSourceRect, IDirect3DSurface9 * pDestSurface, CONST RECT * pDestRect, D3DTEXTUREFILTERTYPE Filter)=0; // Available in 98134
};

struct pvector3f
{
  float x,y,z;
};

// Directional light for plugins
// Sun or moon light
struct pluginLightDir
{
  pvector3f direction; // Light direction
  pvector3f diffuse; // Diffuse color RGB
  pvector3f ambient; // Ambient color RGB
  float sunMoon; // 1.0 = Sun 0.0 = Moon
};

enum sensorDefaultMaterial
{
  MatDefault,
  MatSky,
  MatGround,
  MatWater,
  MatPerson,
  MatVehicle,
  MatTree,
  MatBush,
  MatRock,
  MatBuilding,
  MatObject,
  NSensorDefaultMaterial,
};

enum sensorTerrainMaterial
{
  TerrainDefault,
  TerrainDirt,
  TerrainDryGrass,
  TerrainForest,
  TerrainGrass,
  TerrainGravel,
  TerrainHall,
  TerrainMetal,
  TerrainMud,
  TerrainRoad,
  TerrainRock,
  TerrainSand,
  TerrainWater,
  TerrainWood,
  TerrainSnow,
  NSensorTerrainMaterial,
};

struct paramSensorRequest
{
  char param[256];
  int numDefaultMaterials;
  int numTerrainMaterials;
};

struct outSensorRequest
{
  int width;
  int height;
  int peels;
  unsigned char *defaultMaterials;
  unsigned char *terrainMaterials;
};

struct paramSensorInit
{
  IDirect3DDevice9 *dev;  // D3D device, can be used only during this function call
  int width;  // Sensor resolution width
  int height; // Sensor resolution height
  int peels;  // Number of peels to expect
  D3DFORMAT peelSurfaceFormats[4]; // Surface formats of paramSensorPeel peelSurface surfaces
  D3DFORMAT peelDepthFormat; // Surface format of peelDepth (Usually INTZ)
  const char *sensorParameter; // Parameter from enableCustomSensor
  char materialPriority[256]; // Plugin sets contents of this for material priorization
};

struct paramSensorDeviceInvalidate
{
  IDirect3DDevice9 *dev;  // D3D device, can be used only during this function call
  bool isReset;           // true = device is reseted and will be restored later. false = device is being destroyed
};

struct paramSensorDeviceRestore
{
  IDirect3DDevice9 *dev;  // D3D device, can be used only during this function call
};

struct paramSensorPreDraw
{
  IDirect3DDevice9 *dev;  // D3D device, can be used only during this function call
};

struct paramSensorPeel
{
  VBS2CBInterface *cbi;
  int peelIndex; // Zero based index of this peel
  IDirect3DSurface9 *peelSurface[4]; // Surfaces containing rendered data for this peel
  IDirect3DSurface9 *peelDepth; // Depth surface for this peel
};

struct paramSensorFinal
{
  VBS2CBInterface *cbi;
  IDirect3DSurface9 *renderTarget;
  IDirect3DTexture9 *shadowMap;
  pluginLightDir mainLight; // Main scene light (sun or moon)
  FrustumSpecNG cameraFrustum;
};

#endif