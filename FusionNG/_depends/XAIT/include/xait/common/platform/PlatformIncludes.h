// (C) xaitment GmbH 2006-2012

#pragma once 


//////////////////////////////////////////////////////////////////////////
// WIN32
//////////////////////////////////////////////////////////////////////////
#if XAIT_OS == XAIT_OS_WIN

#ifdef _M_IX86 // 32-bit

#ifdef _WIN32_WINNT
#if _WIN32_WINNT < 0x0500
#error "_WIN32_WINNT must be at least 0x0500 (Win2000) for xaitCommon"
#endif
#else
#define _WIN32_WINNT	0x0500	
#endif

#else // 64-bit

#ifdef _WIN32_WINNT
#if _WIN32_WINNT < 0x0501
#error "_WIN32_WINNT must be at least 0x0501 (WinXP) for xaitCommon 64-bit"
#endif
#else
#define _WIN32_WINNT	0x0501
#endif

#endif

#ifndef WIN32_LEAN_AND_MEAN
	#define WIN32_LEAN_AND_MEAN				// Exclude rarely-used stuff from Windows headers
#endif

#include <windows.h>

//////////////////////////////////////////////////////////////////////////
// XBOX360
//////////////////////////////////////////////////////////////////////////
#elif XAIT_OS == XAIT_OS_XBOX360

#include <xtl.h>

//////////////////////////////////////////////////////////////////////////
// XBOX360EXT
//////////////////////////////////////////////////////////////////////////
#elif XAIT_OS == XAIT_OS_XBOX360EXT

#include <xdk.h>

#endif


