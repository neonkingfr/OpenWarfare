// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/DLLDefines.h>
#include <xait/common/serialize/SerializeStorage.h>
#include <xait/common/reflection/function/FunctionArgumentList.h>

namespace XAIT
{
	namespace Common
	{
		namespace Reflection
		{
			namespace Datatype
			{
				template<typename T_DATATYPE>
				struct DatatypeSerializer
				{
					static void readValue(Serialize::SerializeStorage* stream, T_DATATYPE& value)
					{
						XAIT_SERIALIZE_READ_VALUE_NAME_CHECK("value",value,stream,"GenericValue");
					}

					static void writeValue(Serialize::SerializeStorage* stream, const T_DATATYPE& value)
					{
						stream->writeValue("value",value);
					}
				};

				template<typename T_INNER_DATATYPE>
				struct DatatypeSerializer<Math::Vec2<T_INNER_DATATYPE> >
				{
					static void readValue(Serialize::SerializeStorage* stream, Math::Vec2<T_INNER_DATATYPE>& value)
					{
						XAIT_SERIALIZE_READ_VALUE_NAME_CHECK("x",value.mX,stream,"Vec2fValue");
						XAIT_SERIALIZE_READ_VALUE_NAME_CHECK("y",value.mY,stream,"Vec2fValue");

					}

					static void writeValue(Serialize::SerializeStorage* stream, const Math::Vec2<T_INNER_DATATYPE>& value)
					{
						stream->writeValue("x",value.mX);
						stream->writeValue("y",value.mY);
					}
				};

				template<typename T_INNER_DATATYPE>
				struct DatatypeSerializer<Math::Vec3<T_INNER_DATATYPE> >
				{
					static void readValue(Serialize::SerializeStorage* stream, Math::Vec3<T_INNER_DATATYPE>& value)
					{
						XAIT_SERIALIZE_READ_VALUE_NAME_CHECK("x",value.mX,stream,"Vec2fValue");
						XAIT_SERIALIZE_READ_VALUE_NAME_CHECK("y",value.mY,stream,"Vec2fValue");
						XAIT_SERIALIZE_READ_VALUE_NAME_CHECK("z",value.mZ,stream,"Vec2fValue");
					}

					static void writeValue(Serialize::SerializeStorage* stream, const Math::Vec3<T_INNER_DATATYPE>& value)
					{
						stream->writeValue("x",value.mX);
						stream->writeValue("y",value.mY);
						stream->writeValue("z",value.mZ);
					}
				};

				template<typename ID_T, typename NAMESPACE_T, int UNIQUE_ID>
				struct DatatypeSerializer<IDWrapper<ID_T,NAMESPACE_T,UNIQUE_ID> >
				{
					static void readValue(Serialize::SerializeStorage* stream, IDWrapper<ID_T,NAMESPACE_T,UNIQUE_ID>& value)
					{
						ID_T innerValue;
						XAIT_SERIALIZE_READ_VALUE_NAME_CHECK("innerValue",innerValue,stream,"ID");
						value = *((IDWrapper<ID_T,NAMESPACE_T,UNIQUE_ID>*)&innerValue);
					}

					static void writeValue(Serialize::SerializeStorage* stream, const IDWrapper<ID_T,NAMESPACE_T,UNIQUE_ID>& value)
					{
						stream->writeValue("innerValue",GetIDWrapperValue(value));
					}
				};

				template<typename T_INNER_DATATYPE>
				struct XAIT_COMMON_DLLAPI DatatypeSerializer<Container::Vector<T_INNER_DATATYPE> >
				{
					static void readValue(Serialize::SerializeStorage* stream, Container::Vector<T_INNER_DATATYPE>& value)
					{
						uint32 elementCount;
						XAIT_SERIALIZE_READ_VALUE_NAME_CHECK("count",elementCount,stream,"Vector");
						value.resize(elementCount);
						for (uint32 i=0; i<elementCount; ++i)
						{
							DatatypeSerializer<T_INNER_DATATYPE>::readValue(stream,value[i]);
						}
					}

					static void writeValue(Serialize::SerializeStorage* stream, const Container::Vector<T_INNER_DATATYPE>& value)
					{
						stream->writeValue("count",value.size());
						for (uint32 i=0; i<value.size(); ++i)
						{
							DatatypeSerializer<T_INNER_DATATYPE>::writeValue(stream,value[i]);
						}
					}
				};

				template<>
				struct XAIT_COMMON_DLLAPI DatatypeSerializer<Container::Vector<bool> >
				{
					static void readValue(Serialize::SerializeStorage* stream, Container::Vector<bool>& value)
					{
						uint32 elementCount;
						XAIT_SERIALIZE_READ_VALUE_NAME_CHECK("count",elementCount,stream,"Vector");
						value.resize(elementCount);
						for (uint32 i=0; i<elementCount; ++i)
						{
							bool tempValue;
							DatatypeSerializer<bool>::readValue(stream,tempValue);
							
							value[i] = tempValue;
						}
					}

					static void writeValue(Serialize::SerializeStorage* stream, const Container::Vector<bool>& value)
					{
						stream->writeValue("count",value.size());
						for (uint32 i=0; i<value.size(); ++i)
						{
							DatatypeSerializer<bool>::writeValue(stream,value[i]);
						}
					}
				};

				template<>
				struct XAIT_COMMON_DLLAPI DatatypeSerializer<Function::FunctionArgumentList >
				{
					static void readValue(Serialize::SerializeStorage* stream, Function::FunctionArgumentList& value)
					{
						X_ASSERT_MSG(false, "Not implemented yet");
					}

					static void writeValue(Serialize::SerializeStorage* stream, const Function::FunctionArgumentList& value)
					{
						X_ASSERT_MSG(false, "Not implemented yet");
					}
				};
			}
		}
	}
}
