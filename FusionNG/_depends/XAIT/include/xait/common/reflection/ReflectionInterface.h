// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/memory/Allocator.h>
#include <xait/common/reflection/datatype/DatatypeManager.h>
#include <xait/common/reflection/datatype/Datatypes.h>
#include <xait/common/reflection/datatype/StringConversionFunctions.h>
#include <xait/common/math/Vec2.h>
#include <xait/common/math/Vec3.h>
#include <xait/common/container/Vector.h>
#include <xait/common/reflection/function/FunctionArgumentList.h>


namespace XAIT
{
	namespace Common
	{
		namespace Reflection
		{
			class  ReflectionInterface
			{
			public:
				//! \brief gets the memory allocator used by the reflection 
				//! \returns the reflection memory allocator
				XAIT_COMMON_DLLAPI static const Memory::AllocatorPtr&			getMemoryAllocator();
				//! \brief gets the datatypemanager
				//! \returns the datatypemanager
				XAIT_COMMON_DLLAPI static const Datatype::DatatypeManager::Ptr&	getDatatypeManager();

				//! \brief initializes the reflection
				XAIT_COMMON_DLLAPI static void init(const Datatype::DatatypeManager::Ptr& datatypeManager);

				//! \brief deinitialze the reflection
				XAIT_COMMON_DLLAPI static void close();

				

			private:

				XAIT_COMMON_DLLAPI static Datatype::DatatypeManager::Ptr	mDatatypeManager;
			};
		}
	}
}
