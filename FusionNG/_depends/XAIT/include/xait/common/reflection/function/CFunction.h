// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/reflection/function/FunctionArgumentList.h>
#include <xait/common/reflection/function/FunctionDeclarationManager.h>



namespace XAIT
{
	namespace Common
	{
		namespace Reflection
		{
			namespace Function
			{
				class FunctionArgumentList;
				struct CFunction;
		
				// create an alias (Callerfunction) for a function pointer with return type bool, and signature with the 4 defined arguments
				typedef bool (*CallerFunction) (void* returnValue,CFunction* cFunction,void* owner,const FunctionArgumentList& argList);


				struct CFunction
				{
					XAIT_COMMON_DLLAPI CFunction(void* callerFunction, void* callerFunctionWR, void* internalFunctionPointer, uint32 functionSize, 
						FunctionID id,void* functionOwner,uint32 ownerSize,const FunctionDeclarationManager::Ptr& declarations);

					XAIT_COMMON_DLLAPI CFunction(void* callerFunction, void* callerFunctionWR, void* internalFunctionPointer, uint32 functionSize, FunctionID id,const FunctionDeclarationManager::Ptr& declarations);

					XAIT_COMMON_DLLAPI CFunction(const CFunction& other);

					XAIT_COMMON_DLLAPI ~CFunction();

					XAIT_COMMON_DLLAPI CFunction& operator=(const CFunction& other);

					XAIT_COMMON_DLLAPI Datatype::DatatypeID getReturnValueType() const;

					FunctionDeclarationManager::Ptr			mFunctionDeclarations;
					void*									mFunctionOwner;
					void*									mInternalFunctionPointer;
					void*									mCallerFunction;
					void*									mCallerFunctionWR;
					uint32									mFunctionSize;
					uint32									mOwnerSize;
					FunctionID								mFunctionID;
				};
			}
		}
	}
}

