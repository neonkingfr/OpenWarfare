// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/IDWrapper.h>

namespace XAIT
{
	namespace Common
	{
		namespace Reflection
		{
			namespace Function
			{
				// forward declaration
				class FunctionDeclarationManager;
			}
		}

		XAIT_IDWRAPPER_TEMPLATE_GENERATION(uint32,Reflection::Function::FunctionDeclarationManager,0);

		namespace Reflection
		{
			namespace Function
			{
				// id of a function
				typedef Common::IDWrapper<uint32,FunctionDeclarationManager,0>	FunctionID;

			}
		}
	}
}
