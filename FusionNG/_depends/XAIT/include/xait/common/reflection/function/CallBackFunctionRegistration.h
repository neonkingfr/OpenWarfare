// (C) xaitment GmbH 2006-2012

#pragma once


namespace XAIT
{
	namespace Common
	{
		namespace Reflection
		{
			namespace Function
			{

				typedef void (*CallbackWrapper) (CFunction* cFunction,void*,uint32,uint32);

				template<typename T_VALUETYPE,typename T_FUNCTIONOWNER_TYPE>
				class CallbackFunctionWrapper
				{
				public:
					static void callCallback(CFunction* cFunction,void* value, uint32 typeID, uint32 accessID)
					{
						typedef void (T_FUNCTIONOWNER_TYPE::*Functor)(T_VALUETYPE& value,uint32 typeID, uint32 accessID);
						Functor& func = *((Functor*)cFunction->mInternalFunctionPointer);
						T_FUNCTIONOWNER_TYPE& owner = *((T_FUNCTIONOWNER_TYPE*) cFunction->mFunctionOwner);
						(owner.*func)(*((T_VALUETYPE*)value),typeID,accessID);
					}
				};
			}
		}
	}
}
