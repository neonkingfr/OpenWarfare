// (C) xaitment GmbH 2006-2012

#pragma  once



#include <xait/common/Platform.h>
#include <xait/common/reflection/function/FunctionArgumentList.h>
#include <xait/common/reflection/function/CFunction.h>
#include <xait/common/reflection/function/ModifierHelpFunction.h>



namespace XAIT
{
	namespace Common
	{
		namespace Reflection
		{
			namespace Function
			{

				template<typename T_RETURNVALUE_TYPE>
				class CallerClassWithoutReturnValues
				{
				public:
					// global functions

					// ms linker bug-> no inline
					XAIT_NOINLINE static bool callFunction(T_RETURNVALUE_TYPE (*func)(), const FunctionArgumentList& argList)
					{
						if (argList.size() != 0)
							return false;
						func();
						return true;
					}


					template <typename T_AT1>
					static bool callFunction(T_RETURNVALUE_TYPE (*func)(T_AT1), const FunctionArgumentList& argList)
					{
						if (argList.size() != 1)
							return false;
						func((argList.getValue<typename ModifierHelpFunction<T_AT1>::Type>(0)));
						return true;
					}

					template <typename T_AT1,typename T_AT2>
					static bool callFunction(T_RETURNVALUE_TYPE (*func)(T_AT1,T_AT2), const FunctionArgumentList& argList)
					{
						if (argList.size() != 2)
							return false;
						func((argList.getValue<typename ModifierHelpFunction<T_AT1>::Type>(0)),
							(argList.getValue<typename ModifierHelpFunction<T_AT2>::Type>(1)));
						return true;
					}


					template <typename T_AT1,typename T_AT2,typename T_AT3>
					static bool callFunction(T_RETURNVALUE_TYPE (*func)(T_AT1,T_AT2,T_AT3), const FunctionArgumentList& argList)
					{
						if (argList.size() != 3)
							return false;
						func((argList.getValue<typename ModifierHelpFunction<T_AT1>::Type>(0)),
							(argList.getValue<typename ModifierHelpFunction<T_AT2>::Type>(1)),
							(argList.getValue<typename ModifierHelpFunction<T_AT3>::Type>(2)));
						return true;
					}


					template <typename T_AT1,typename T_AT2,typename T_AT3,typename T_AT4>
					static bool callFunction(T_RETURNVALUE_TYPE (*func)(T_AT1,T_AT2,T_AT3,T_AT4), const FunctionArgumentList& argList)
					{
						if (argList.size() != 4)
							return false;
						func((argList.getValue<typename ModifierHelpFunction<T_AT1>::Type>(0)),
							(argList.getValue<typename ModifierHelpFunction<T_AT2>::Type>(1)),
							(argList.getValue<typename ModifierHelpFunction<T_AT3>::Type>(2)),
							(argList.getValue<typename ModifierHelpFunction<T_AT4>::Type>(3)));
						return true;
					}

					template <typename T_AT1,typename T_AT2,typename T_AT3,typename T_AT4,typename T_AT5>
					static bool callFunction(T_RETURNVALUE_TYPE (*func)(T_AT1,T_AT2,T_AT3,T_AT4,T_AT5), const FunctionArgumentList& argList)
					{
						if (argList.size() != 5)
							return false;
						func((argList.getValue<typename ModifierHelpFunction<T_AT1>::Type>(0)),
							(argList.getValue<typename ModifierHelpFunction<T_AT2>::Type>(1)),
							(argList.getValue<typename ModifierHelpFunction<T_AT3>::Type>(2)),
							(argList.getValue<typename ModifierHelpFunction<T_AT4>::Type>(3)),
							(argList.getValue<typename ModifierHelpFunction<T_AT5>::Type>(4)));
						return true;
					} 

					template <typename T_AT1,typename T_AT2,typename T_AT3,typename T_AT4,typename T_AT5,typename T_AT6>
					static bool callFunction(T_RETURNVALUE_TYPE (*func)(T_AT1,T_AT2,T_AT3,T_AT4,T_AT5,T_AT6), const FunctionArgumentList& argList)
					{
						if (argList.size() != 6)
							return false;
						func((argList.getValue<typename ModifierHelpFunction<T_AT1>::Type>(0)),
							(argList.getValue<typename ModifierHelpFunction<T_AT2>::Type>(1)),
							(argList.getValue<typename ModifierHelpFunction<T_AT3>::Type>(2)),
							(argList.getValue<typename ModifierHelpFunction<T_AT4>::Type>(3)),
							(argList.getValue<typename ModifierHelpFunction<T_AT5>::Type>(4)),
							(argList.getValue<typename ModifierHelpFunction<T_AT6>::Type>(5)));
						return true;
					} 

					template <typename T_AT1,typename T_AT2,typename T_AT3,typename T_AT4,typename T_AT5,
						typename T_AT6,typename T_AT7>
						static bool callFunction(T_RETURNVALUE_TYPE (*func)(T_AT1,T_AT2,T_AT3,T_AT4,T_AT5,T_AT6,T_AT7), const FunctionArgumentList& argList)
					{
						if (argList.size() != 7)
							return false;
						func((argList.getValue<typename ModifierHelpFunction<T_AT1>::Type>(0)),
							(argList.getValue<typename ModifierHelpFunction<T_AT2>::Type>(1)),
							(argList.getValue<typename ModifierHelpFunction<T_AT3>::Type>(2)),
							(argList.getValue<typename ModifierHelpFunction<T_AT4>::Type>(3)),
							(argList.getValue<typename ModifierHelpFunction<T_AT5>::Type>(4)),
							(argList.getValue<typename ModifierHelpFunction<T_AT6>::Type>(5)),
							(argList.getValue<typename ModifierHelpFunction<T_AT7>::Type>(6)));
						return true;
					} 

					template <typename T_AT1,typename T_AT2,typename T_AT3,typename T_AT4,typename T_AT5,
						typename T_AT6,typename T_AT7,typename T_AT8>
						static bool callFunction(T_RETURNVALUE_TYPE (*func)(T_AT1,T_AT2,T_AT3,T_AT4,T_AT5,T_AT6,T_AT7,T_AT8), const FunctionArgumentList& argList)
					{
						if (argList.size() != 8)
							return false;
						func((argList.getValue<typename ModifierHelpFunction<T_AT1>::Type>(0)),
							(argList.getValue<typename ModifierHelpFunction<T_AT2>::Type>(1)),
							(argList.getValue<typename ModifierHelpFunction<T_AT3>::Type>(2)),
							(argList.getValue<typename ModifierHelpFunction<T_AT4>::Type>(3)),
							(argList.getValue<typename ModifierHelpFunction<T_AT5>::Type>(4)),
							(argList.getValue<typename ModifierHelpFunction<T_AT6>::Type>(5)),
							(argList.getValue<typename ModifierHelpFunction<T_AT7>::Type>(6)),
							(argList.getValue<typename ModifierHelpFunction<T_AT8>::Type>(7)));
						return true;
					} 

					template <typename T_AT1,typename T_AT2,typename T_AT3,typename T_AT4,typename T_AT5,
						typename T_AT6,typename T_AT7,typename T_AT8,typename T_AT9>
						static bool callFunction(T_RETURNVALUE_TYPE (*func)(T_AT1,T_AT2,T_AT3,T_AT4,T_AT5,T_AT6,T_AT7,T_AT8,T_AT9), const FunctionArgumentList& argList)
					{
						if (argList.size() != 9)
							return false;
						func((argList.getValue<typename ModifierHelpFunction<T_AT1>::Type>(0)),
							(argList.getValue<typename ModifierHelpFunction<T_AT2>::Type>(1)),
							(argList.getValue<typename ModifierHelpFunction<T_AT3>::Type>(2)),
							(argList.getValue<typename ModifierHelpFunction<T_AT4>::Type>(3)),
							(argList.getValue<typename ModifierHelpFunction<T_AT5>::Type>(4)),
							(argList.getValue<typename ModifierHelpFunction<T_AT6>::Type>(5)),
							(argList.getValue<typename ModifierHelpFunction<T_AT7>::Type>(6)),
							(argList.getValue<typename ModifierHelpFunction<T_AT8>::Type>(7)),
							(argList.getValue<typename ModifierHelpFunction<T_AT9>::Type>(8)));
						return true;
					} 

					template <typename T_AT1,typename T_AT2,typename T_AT3,typename T_AT4,typename T_AT5,
						typename T_AT6,typename T_AT7,typename T_AT8,typename T_AT9,typename T_AT10>
						static bool callFunction(T_RETURNVALUE_TYPE (*func)(T_AT1,T_AT2,T_AT3,T_AT4,T_AT5,T_AT6,T_AT7,T_AT8,T_AT9,T_AT10), const FunctionArgumentList& argList)
					{
						if (argList.size() != 10)
							return false;
						func((argList.getValue<typename ModifierHelpFunction<T_AT1>::Type>(0)),
							(argList.getValue<typename ModifierHelpFunction<T_AT2>::Type>(1)),
							(argList.getValue<typename ModifierHelpFunction<T_AT3>::Type>(2)),
							(argList.getValue<typename ModifierHelpFunction<T_AT4>::Type>(3)),
							(argList.getValue<typename ModifierHelpFunction<T_AT5>::Type>(4)),
							(argList.getValue<typename ModifierHelpFunction<T_AT6>::Type>(5)),
							(argList.getValue<typename ModifierHelpFunction<T_AT7>::Type>(6)),
							(argList.getValue<typename ModifierHelpFunction<T_AT8>::Type>(7)),
							(argList.getValue<typename ModifierHelpFunction<T_AT9>::Type>(8)),
							(argList.getValue<typename ModifierHelpFunction<T_AT10>::Type>(9)));
						return true;
					} 



					// methods
					template <typename T_FUNCTION_OWNER>
					XAIT_NOINLINE static bool callMember(T_FUNCTION_OWNER& owner, T_RETURNVALUE_TYPE (T_FUNCTION_OWNER::*func)(), const FunctionArgumentList& argList)
					{
						if (argList.size() != 0)
							return false;
						(owner.*func)();
						return true;
					}

					template <typename T_FUNCTION_OWNER>
					XAIT_NOINLINE static bool callMember(T_FUNCTION_OWNER& owner, T_RETURNVALUE_TYPE (T_FUNCTION_OWNER::*func)() const, const FunctionArgumentList& argList)
					{
						if (argList.size() != 0)
							return false;
						(owner.*func)();
						return true;
					}

					template <typename T_FUNCTION_OWNER,typename T_AT1>
					static bool callMember(T_FUNCTION_OWNER& owner, T_RETURNVALUE_TYPE (T_FUNCTION_OWNER::*func)(T_AT1), const FunctionArgumentList& argList)
					{
						if (argList.size() != 1)
							return false;
						(owner.*func)((argList.getValue<typename ModifierHelpFunction<T_AT1>::Type>(0)));
						return true;
					}

					template <typename T_FUNCTION_OWNER,typename T_AT1>
					static bool callMember(T_FUNCTION_OWNER& owner, T_RETURNVALUE_TYPE (T_FUNCTION_OWNER::*func)(T_AT1) const, const FunctionArgumentList& argList)
					{
						if (argList.size() != 1)
							return false;
						(owner.*func)((argList.getValue<typename ModifierHelpFunction<T_AT1>::Type>(0)));
						return true;
					}


					template <typename T_FUNCTION_OWNER,typename T_AT1,typename T_AT2>
					static bool callMember(T_FUNCTION_OWNER& owner, T_RETURNVALUE_TYPE (T_FUNCTION_OWNER::*func)(T_AT1,T_AT2), const FunctionArgumentList& argList)
					{
						if (argList.size() != 2)
							return false;
						(owner.*func)((argList.getValue<typename ModifierHelpFunction<T_AT1>::Type>(0)),
							(argList.getValue<typename ModifierHelpFunction<T_AT2>::Type>(1)));
						return true;
					}

					template <typename T_FUNCTION_OWNER,typename T_AT1,typename T_AT2>
					static bool callMember(T_FUNCTION_OWNER& owner, T_RETURNVALUE_TYPE (T_FUNCTION_OWNER::*func)(T_AT1,T_AT2) const, const FunctionArgumentList& argList)
					{
						if (argList.size() != 2)
							return false;
						(owner.*func)((argList.getValue<typename ModifierHelpFunction<T_AT1>::Type>(0)),
							(argList.getValue<typename ModifierHelpFunction<T_AT2>::Type>(1)));
						return true;
					}

					template <typename T_FUNCTION_OWNER,typename T_AT1,typename T_AT2,typename T_AT3>
					static bool callMember(T_FUNCTION_OWNER& owner, T_RETURNVALUE_TYPE (T_FUNCTION_OWNER::*func)(T_AT1,T_AT2,T_AT3), const FunctionArgumentList& argList)
					{
						if (argList.size() != 3)
							return false;
						(owner.*func)((argList.getValue<typename ModifierHelpFunction<T_AT1>::Type>(0)),
							(argList.getValue<typename ModifierHelpFunction<T_AT2>::Type>(1)),
							(argList.getValue<typename ModifierHelpFunction<T_AT3>::Type>(2)));
						return true;
					}

					template <typename T_FUNCTION_OWNER,typename T_AT1,typename T_AT2,typename T_AT3>
					static bool callMember(T_FUNCTION_OWNER& owner, T_RETURNVALUE_TYPE (T_FUNCTION_OWNER::*func)(T_AT1,T_AT2,T_AT3) const, const FunctionArgumentList& argList)
					{
						if (argList.size() != 3)
							return false;
						(owner.*func)((argList.getValue<typename ModifierHelpFunction<T_AT1>::Type>(0)),
							(argList.getValue<typename ModifierHelpFunction<T_AT2>::Type>(1)),
							(argList.getValue<typename ModifierHelpFunction<T_AT3>::Type>(2)));
						return true;
					}

					template <typename T_FUNCTION_OWNER,typename T_AT1,typename T_AT2,typename T_AT3,typename T_AT4>
					static bool callMember(T_FUNCTION_OWNER& owner, T_RETURNVALUE_TYPE (T_FUNCTION_OWNER::*func)(T_AT1,T_AT2,T_AT3,T_AT4), const FunctionArgumentList& argList)
					{
						if (argList.size() != 4)
							return false;
						(owner.*func)((argList.getValue<typename ModifierHelpFunction<T_AT1>::Type>(0)),
							(argList.getValue<typename ModifierHelpFunction<T_AT2>::Type>(1)),
							(argList.getValue<typename ModifierHelpFunction<T_AT3>::Type>(2)),
							(argList.getValue<typename ModifierHelpFunction<T_AT4>::Type>(3)));
						return true;
					}

					template <typename T_FUNCTION_OWNER,typename T_AT1,typename T_AT2,typename T_AT3,typename T_AT4>
					static bool callMember(T_FUNCTION_OWNER& owner, T_RETURNVALUE_TYPE (T_FUNCTION_OWNER::*func)(T_AT1,T_AT2,T_AT3,T_AT4) const, const FunctionArgumentList& argList)
					{
						if (argList.size() != 4)
							return false;
						(owner.*func)((argList.getValue<typename ModifierHelpFunction<T_AT1>::Type>(0)),
							(argList.getValue<typename ModifierHelpFunction<T_AT2>::Type>(1)),
							(argList.getValue<typename ModifierHelpFunction<T_AT3>::Type>(2)),
							(argList.getValue<typename ModifierHelpFunction<T_AT4>::Type>(3)));
						return true;
					}

					template <typename T_FUNCTION_OWNER,typename T_AT1,typename T_AT2,typename T_AT3,typename T_AT4,typename T_AT5>
					static bool callMember(T_FUNCTION_OWNER& owner, T_RETURNVALUE_TYPE (T_FUNCTION_OWNER::*func)(T_AT1,T_AT2,T_AT3,T_AT4,T_AT5), const FunctionArgumentList& argList)
					{
						if (argList.size() != 5)
							return false;
						(owner.*func)((argList.getValue<typename ModifierHelpFunction<T_AT1>::Type>(0)),
							(argList.getValue<typename ModifierHelpFunction<T_AT2>::Type>(1)),
							(argList.getValue<typename ModifierHelpFunction<T_AT3>::Type>(2)),
							(argList.getValue<typename ModifierHelpFunction<T_AT4>::Type>(3)),
							(argList.getValue<typename ModifierHelpFunction<T_AT5>::Type>(4)));
						return true;
					}

					template <typename T_FUNCTION_OWNER,typename T_AT1,typename T_AT2,typename T_AT3,typename T_AT4,typename T_AT5>
					static bool callMember(T_FUNCTION_OWNER& owner, T_RETURNVALUE_TYPE (T_FUNCTION_OWNER::*func)(T_AT1,T_AT2,T_AT3,T_AT4,T_AT5) const, const FunctionArgumentList& argList)
					{
						if (argList.size() != 5)
							return false;
						(owner.*func)((argList.getValue<typename ModifierHelpFunction<T_AT1>::Type>(0)),
							(argList.getValue<typename ModifierHelpFunction<T_AT2>::Type>(1)),
							(argList.getValue<typename ModifierHelpFunction<T_AT3>::Type>(2)),
							(argList.getValue<typename ModifierHelpFunction<T_AT4>::Type>(3)),
							(argList.getValue<typename ModifierHelpFunction<T_AT5>::Type>(4)));
						return true;
					}

					template <typename T_FUNCTION_OWNER,typename T_AT1,typename T_AT2,typename T_AT3,typename T_AT4,typename T_AT5,
						typename T_AT6>
						static bool callMember(T_FUNCTION_OWNER& owner, T_RETURNVALUE_TYPE (T_FUNCTION_OWNER::*func)(T_AT1,T_AT2,T_AT3,T_AT4,T_AT5,T_AT6), const FunctionArgumentList& argList)
					{
						if (argList.size() != 6)
							return false;
						(owner.*func)((argList.getValue<typename ModifierHelpFunction<T_AT1>::Type>(0)),
							(argList.getValue<typename ModifierHelpFunction<T_AT2>::Type>(1)),
							(argList.getValue<typename ModifierHelpFunction<T_AT3>::Type>(2)),
							(argList.getValue<typename ModifierHelpFunction<T_AT4>::Type>(3)),
							(argList.getValue<typename ModifierHelpFunction<T_AT5>::Type>(4)),
							(argList.getValue<typename ModifierHelpFunction<T_AT6>::Type>(5)));
						return true;
					}

					template <typename T_FUNCTION_OWNER,typename T_AT1,typename T_AT2,typename T_AT3,typename T_AT4,typename T_AT5,
						typename T_AT6>
						static bool callMember(T_FUNCTION_OWNER& owner, T_RETURNVALUE_TYPE (T_FUNCTION_OWNER::*func)(T_AT1,T_AT2,T_AT3,T_AT4,T_AT5,T_AT6) const, const FunctionArgumentList& argList)
					{
						if (argList.size() != 6)
							return false;
						(owner.*func)((argList.getValue<typename ModifierHelpFunction<T_AT1>::Type>(0)),
							(argList.getValue<typename ModifierHelpFunction<T_AT2>::Type>(1)),
							(argList.getValue<typename ModifierHelpFunction<T_AT3>::Type>(2)),
							(argList.getValue<typename ModifierHelpFunction<T_AT4>::Type>(3)),
							(argList.getValue<typename ModifierHelpFunction<T_AT5>::Type>(4)),
							(argList.getValue<typename ModifierHelpFunction<T_AT6>::Type>(5)));
						return true;
					}

					template <typename T_FUNCTION_OWNER,typename T_AT1,typename T_AT2,typename T_AT3,typename T_AT4,typename T_AT5,
						typename T_AT6, typename T_AT7>
						static bool callMember(T_FUNCTION_OWNER& owner, T_RETURNVALUE_TYPE (T_FUNCTION_OWNER::*func)(T_AT1,T_AT2,T_AT3,T_AT4,T_AT5,T_AT6,T_AT7), const FunctionArgumentList& argList)
					{
						if (argList.size() != 7)
							return false;
						(owner.*func)((argList.getValue<typename ModifierHelpFunction<T_AT1>::Type>(0)),
							(argList.getValue<typename ModifierHelpFunction<T_AT2>::Type>(1)),
							(argList.getValue<typename ModifierHelpFunction<T_AT3>::Type>(2)),
							(argList.getValue<typename ModifierHelpFunction<T_AT4>::Type>(3)),
							(argList.getValue<typename ModifierHelpFunction<T_AT5>::Type>(4)),
							(argList.getValue<typename ModifierHelpFunction<T_AT6>::Type>(5)),
							(argList.getValue<typename ModifierHelpFunction<T_AT7>::Type>(6)));
						return true;
					}

					template <typename T_FUNCTION_OWNER,typename T_AT1,typename T_AT2,typename T_AT3,typename T_AT4,typename T_AT5,
						typename T_AT6, typename T_AT7>
						static bool callMember(T_FUNCTION_OWNER& owner, T_RETURNVALUE_TYPE (T_FUNCTION_OWNER::*func)(T_AT1,T_AT2,T_AT3,T_AT4,T_AT5,T_AT6,T_AT7) const, const FunctionArgumentList& argList)
					{
						if (argList.size() != 7)
							return false;
						(owner.*func)((argList.getValue<typename ModifierHelpFunction<T_AT1>::Type>(0)),
							(argList.getValue<typename ModifierHelpFunction<T_AT2>::Type>(1)),
							(argList.getValue<typename ModifierHelpFunction<T_AT3>::Type>(2)),
							(argList.getValue<typename ModifierHelpFunction<T_AT4>::Type>(3)),
							(argList.getValue<typename ModifierHelpFunction<T_AT5>::Type>(4)),
							(argList.getValue<typename ModifierHelpFunction<T_AT6>::Type>(5)),
							(argList.getValue<typename ModifierHelpFunction<T_AT7>::Type>(6)));
						return true;
					}

					template <typename T_FUNCTION_OWNER,typename T_AT1,typename T_AT2,typename T_AT3,typename T_AT4,typename T_AT5,
						typename T_AT6,typename T_AT7,typename T_AT8>
						static bool callMember(T_FUNCTION_OWNER& owner, T_RETURNVALUE_TYPE (T_FUNCTION_OWNER::*func)(T_AT1,T_AT2,T_AT3,T_AT4,T_AT5,T_AT6,T_AT7,T_AT8), const FunctionArgumentList& argList)
					{
						if (argList.size() != 8)
							return false;
						(owner.*func)((argList.getValue<typename ModifierHelpFunction<T_AT1>::Type>(0)),
							(argList.getValue<typename ModifierHelpFunction<T_AT2>::Type>(1)),
							(argList.getValue<typename ModifierHelpFunction<T_AT3>::Type>(2)),
							(argList.getValue<typename ModifierHelpFunction<T_AT4>::Type>(3)),
							(argList.getValue<typename ModifierHelpFunction<T_AT5>::Type>(4)),
							(argList.getValue<typename ModifierHelpFunction<T_AT6>::Type>(5)),
							(argList.getValue<typename ModifierHelpFunction<T_AT7>::Type>(6)),
							(argList.getValue<typename ModifierHelpFunction<T_AT8>::Type>(7)));
						return true;
					}

					template <typename T_FUNCTION_OWNER,typename T_AT1,typename T_AT2,typename T_AT3,typename T_AT4,typename T_AT5,
						typename T_AT6,typename T_AT7,typename T_AT8>
						static bool callMember(T_FUNCTION_OWNER& owner, T_RETURNVALUE_TYPE (T_FUNCTION_OWNER::*func)(T_AT1,T_AT2,T_AT3,T_AT4,T_AT5,T_AT6,T_AT7,T_AT8) const, const FunctionArgumentList& argList)
					{
						if (argList.size() != 8)
							return false;
						(owner.*func)((argList.getValue<typename ModifierHelpFunction<T_AT1>::Type>(0)),
							(argList.getValue<typename ModifierHelpFunction<T_AT2>::Type>(1)),
							(argList.getValue<typename ModifierHelpFunction<T_AT3>::Type>(2)),
							(argList.getValue<typename ModifierHelpFunction<T_AT4>::Type>(3)),
							(argList.getValue<typename ModifierHelpFunction<T_AT5>::Type>(4)),
							(argList.getValue<typename ModifierHelpFunction<T_AT6>::Type>(5)),
							(argList.getValue<typename ModifierHelpFunction<T_AT7>::Type>(6)),
							(argList.getValue<typename ModifierHelpFunction<T_AT8>::Type>(7)));
						return true;
					}

					template <typename T_FUNCTION_OWNER,typename T_AT1,typename T_AT2,typename T_AT3,typename T_AT4,typename T_AT5,
						typename T_AT6,typename T_AT7,typename T_AT8,typename T_AT9>
						static bool callMember(T_FUNCTION_OWNER& owner, T_RETURNVALUE_TYPE (T_FUNCTION_OWNER::*func)(T_AT1,T_AT2,T_AT3,T_AT4,T_AT5,T_AT6,T_AT7,T_AT8,T_AT9), const FunctionArgumentList& argList)
					{
						if (argList.size() != 9)
							return false;
						(owner.*func)((argList.getValue<typename ModifierHelpFunction<T_AT1>::Type>(0)),
							(argList.getValue<typename ModifierHelpFunction<T_AT2>::Type>(1)),
							(argList.getValue<typename ModifierHelpFunction<T_AT3>::Type>(2)),
							(argList.getValue<typename ModifierHelpFunction<T_AT4>::Type>(3)),
							(argList.getValue<typename ModifierHelpFunction<T_AT5>::Type>(4)),
							(argList.getValue<typename ModifierHelpFunction<T_AT6>::Type>(5)),
							(argList.getValue<typename ModifierHelpFunction<T_AT7>::Type>(6)),
							(argList.getValue<typename ModifierHelpFunction<T_AT8>::Type>(7)),
							(argList.getValue<typename ModifierHelpFunction<T_AT9>::Type>(8)));
						return true;
					}

					template <typename T_FUNCTION_OWNER,typename T_AT1,typename T_AT2,typename T_AT3,typename T_AT4,typename T_AT5,
						typename T_AT6,typename T_AT7,typename T_AT8,typename T_AT9>
						static bool callMember(T_FUNCTION_OWNER& owner, T_RETURNVALUE_TYPE (T_FUNCTION_OWNER::*func)(T_AT1,T_AT2,T_AT3,T_AT4,T_AT5,T_AT6,T_AT7,T_AT8,T_AT9) const, const FunctionArgumentList& argList)
					{
						if (argList.size() != 9)
							return false;
						(owner.*func)((argList.getValue<typename ModifierHelpFunction<T_AT1>::Type>(0)),
							(argList.getValue<typename ModifierHelpFunction<T_AT2>::Type>(1)),
							(argList.getValue<typename ModifierHelpFunction<T_AT3>::Type>(2)),
							(argList.getValue<typename ModifierHelpFunction<T_AT4>::Type>(3)),
							(argList.getValue<typename ModifierHelpFunction<T_AT5>::Type>(4)),
							(argList.getValue<typename ModifierHelpFunction<T_AT6>::Type>(5)),
							(argList.getValue<typename ModifierHelpFunction<T_AT7>::Type>(6)),
							(argList.getValue<typename ModifierHelpFunction<T_AT8>::Type>(7)),
							(argList.getValue<typename ModifierHelpFunction<T_AT9>::Type>(8)));
						return true;
					}


					template <typename T_FUNCTION_OWNER,typename T_AT1,typename T_AT2,typename T_AT3,typename T_AT4,typename T_AT5,
						typename T_AT6,typename T_AT7,typename T_AT8,typename T_AT9,typename T_AT10>
						static bool callMember(T_FUNCTION_OWNER& owner, T_RETURNVALUE_TYPE (T_FUNCTION_OWNER::*func)(T_AT1,T_AT2,T_AT3,T_AT4,T_AT5,T_AT6,T_AT7,T_AT8,T_AT9,T_AT10), const FunctionArgumentList& argList)
					{
						if (argList.size() != 10)
							return false;
						(owner.*func)((argList.getValue<typename ModifierHelpFunction<T_AT1>::Type>(0)),
							(argList.getValue<typename ModifierHelpFunction<T_AT2>::Type>(1)),
							(argList.getValue<typename ModifierHelpFunction<T_AT3>::Type>(2)),
							(argList.getValue<typename ModifierHelpFunction<T_AT4>::Type>(3)),
							(argList.getValue<typename ModifierHelpFunction<T_AT5>::Type>(4)),
							(argList.getValue<typename ModifierHelpFunction<T_AT6>::Type>(5)),
							(argList.getValue<typename ModifierHelpFunction<T_AT7>::Type>(6)),
							(argList.getValue<typename ModifierHelpFunction<T_AT8>::Type>(7)),
							(argList.getValue<typename ModifierHelpFunction<T_AT9>::Type>(8)),
							(argList.getValue<typename ModifierHelpFunction<T_AT10>::Type>(9)));
						return true;
					}

					template <typename T_FUNCTION_OWNER,typename T_AT1,typename T_AT2,typename T_AT3,typename T_AT4,typename T_AT5,
						typename T_AT6,typename T_AT7,typename T_AT8,typename T_AT9,typename T_AT10>
						static bool callMember(T_FUNCTION_OWNER& owner, T_RETURNVALUE_TYPE (T_FUNCTION_OWNER::*func)(T_AT1,T_AT2,T_AT3,T_AT4,T_AT5,T_AT6,T_AT7,T_AT8,T_AT9,T_AT10) const, const FunctionArgumentList& argList)
					{
						if (argList.size() != 10)
							return false;
						(owner.*func)((argList.getValue<typename ModifierHelpFunction<T_AT1>::Type>(0)),
							(argList.getValue<typename ModifierHelpFunction<T_AT2>::Type>(1)),
							(argList.getValue<typename ModifierHelpFunction<T_AT3>::Type>(2)),
							(argList.getValue<typename ModifierHelpFunction<T_AT4>::Type>(3)),
							(argList.getValue<typename ModifierHelpFunction<T_AT5>::Type>(4)),
							(argList.getValue<typename ModifierHelpFunction<T_AT6>::Type>(5)),
							(argList.getValue<typename ModifierHelpFunction<T_AT7>::Type>(6)),
							(argList.getValue<typename ModifierHelpFunction<T_AT8>::Type>(7)),
							(argList.getValue<typename ModifierHelpFunction<T_AT9>::Type>(8)),
							(argList.getValue<typename ModifierHelpFunction<T_AT10>::Type>(9)));
						return true;
					}
				};

				template<typename T_RETURNVALUE_TYPE>
				class CallerClassWithReturnValues
				{
				public:
					// with returnvalues
					XAIT_NOINLINE static bool callFunction(typename ModifierHelpFunction<T_RETURNVALUE_TYPE>::Type* returnValue,T_RETURNVALUE_TYPE (*func)(), const FunctionArgumentList& argList)
					{
						if (argList.size() != 0)
							return false;

						*returnValue = func();

						return true;
					}


					template <typename T_AT1>
					XAIT_NOINLINE static bool callFunction(typename ModifierHelpFunction<T_RETURNVALUE_TYPE>::Type* returnValue,T_RETURNVALUE_TYPE (*func)(T_AT1), const FunctionArgumentList& argList)
					{
						if (argList.size() != 1)
							return false;

						*returnValue = func((argList.getValue<typename ModifierHelpFunction<T_AT1>::Type>(0)));

						return true;
					}


					template <typename T_AT1,typename T_AT2>
					static bool callFunction(typename ModifierHelpFunction<T_RETURNVALUE_TYPE>::Type* returnValue,T_RETURNVALUE_TYPE (*func)(T_AT1,T_AT2), const FunctionArgumentList& argList)
					{
						if (argList.size() != 2)
							return false;

						*returnValue = func((argList.getValue<typename ModifierHelpFunction<T_AT1>::Type>(0)),
							(argList.getValue<typename ModifierHelpFunction<T_AT2>::Type>(1)));

						return true;
					}

					template <typename T_AT1,typename T_AT2,typename T_AT3>
					static bool callFunction(typename ModifierHelpFunction<T_RETURNVALUE_TYPE>::Type* returnValue,T_RETURNVALUE_TYPE (*func)(T_AT1,T_AT2,T_AT3), const FunctionArgumentList& argList)
					{
						if (argList.size() != 3)
							return false;

						*returnValue = func((argList.getValue<typename ModifierHelpFunction<T_AT1>::Type>(0)),
							(argList.getValue<typename ModifierHelpFunction<T_AT2>::Type>(1)),
							(argList.getValue<typename ModifierHelpFunction<T_AT3>::Type>(2)));

						return true;
					}


					template <typename T_AT1,typename T_AT2,typename T_AT3,typename T_AT4>
					static bool callFunction(typename ModifierHelpFunction<T_RETURNVALUE_TYPE>::Type* returnValue,T_RETURNVALUE_TYPE (*func)(T_AT1,T_AT2,T_AT3,T_AT4), const FunctionArgumentList& argList)
					{
						if (argList.size() != 4)
							return false;

						*returnValue = func((argList.getValue<typename ModifierHelpFunction<T_AT1>::Type>(0)),
							(argList.getValue<typename ModifierHelpFunction<T_AT2>::Type>(1)),
							(argList.getValue<typename ModifierHelpFunction<T_AT3>::Type>(2)),
							(argList.getValue<typename ModifierHelpFunction<T_AT4>::Type>(3)));

						return true;
					}

					template <typename T_AT1,typename T_AT2,typename T_AT3,typename T_AT4,typename T_AT5>
					static bool callFunction(typename ModifierHelpFunction<T_RETURNVALUE_TYPE>::Type* returnValue,T_RETURNVALUE_TYPE (*func)(T_AT1,T_AT2,T_AT3,T_AT4,T_AT5), const FunctionArgumentList& argList)
					{
						if (argList.size() != 5)
							return false;

						*returnValue = func((argList.getValue<typename ModifierHelpFunction<T_AT1>::Type>(0)),
							(argList.getValue<typename ModifierHelpFunction<T_AT2>::Type>(1)),
							(argList.getValue<typename ModifierHelpFunction<T_AT3>::Type>(2)),
							(argList.getValue<typename ModifierHelpFunction<T_AT4>::Type>(3)),
							(argList.getValue<typename ModifierHelpFunction<T_AT5>::Type>(4)));

						return true;
					} 

					template <typename T_AT1,typename T_AT2,typename T_AT3,typename T_AT4,typename T_AT5,typename T_AT6>
					static bool callFunction(typename ModifierHelpFunction<T_RETURNVALUE_TYPE>::Type* returnValue,T_RETURNVALUE_TYPE (*func)(T_AT1,T_AT2,T_AT3,T_AT4,T_AT5,T_AT6), const FunctionArgumentList& argList)
					{
						if (argList.size() != 6)
							return false;

						*returnValue = func((argList.getValue<typename ModifierHelpFunction<T_AT1>::Type>(0)),
							(argList.getValue<typename ModifierHelpFunction<T_AT2>::Type>(1)),
							(argList.getValue<typename ModifierHelpFunction<T_AT3>::Type>(2)),
							(argList.getValue<typename ModifierHelpFunction<T_AT4>::Type>(3)),
							(argList.getValue<typename ModifierHelpFunction<T_AT5>::Type>(4)),
							(argList.getValue<typename ModifierHelpFunction<T_AT6>::Type>(5)));

						return true;
					} 

					template <typename T_AT1,typename T_AT2,typename T_AT3,typename T_AT4,typename T_AT5,
						typename T_AT6,typename T_AT7>
						static bool callFunction(typename ModifierHelpFunction<T_RETURNVALUE_TYPE>::Type* returnValue,T_RETURNVALUE_TYPE (*func)(T_AT1,T_AT2,T_AT3,T_AT4,T_AT5,T_AT6,T_AT7), const FunctionArgumentList& argList)
					{
						if (argList.size() != 7)
							return false;

						*returnValue = func((argList.getValue<typename ModifierHelpFunction<T_AT1>::Type>(0)),
							(argList.getValue<typename ModifierHelpFunction<T_AT2>::Type>(1)),
							(argList.getValue<typename ModifierHelpFunction<T_AT3>::Type>(2)),
							(argList.getValue<typename ModifierHelpFunction<T_AT4>::Type>(3)),
							(argList.getValue<typename ModifierHelpFunction<T_AT5>::Type>(4)),
							(argList.getValue<typename ModifierHelpFunction<T_AT6>::Type>(5)),
							(argList.getValue<typename ModifierHelpFunction<T_AT7>::Type>(6)));

						return true;
					} 

					template <typename T_AT1,typename T_AT2,typename T_AT3,typename T_AT4,typename T_AT5,
						typename T_AT6,typename T_AT7,typename T_AT8>
						static bool callFunction(typename ModifierHelpFunction<T_RETURNVALUE_TYPE>::Type* returnValue,T_RETURNVALUE_TYPE (*func)(T_AT1,T_AT2,T_AT3,T_AT4,T_AT5,T_AT6,T_AT7,T_AT8), const FunctionArgumentList& argList)
					{
						if (argList.size() != 8)
							return false;

						*returnValue = func((argList.getValue<typename ModifierHelpFunction<T_AT1>::Type>(0)),
							(argList.getValue<typename ModifierHelpFunction<T_AT2>::Type>(1)),
							(argList.getValue<typename ModifierHelpFunction<T_AT3>::Type>(2)),
							(argList.getValue<typename ModifierHelpFunction<T_AT4>::Type>(3)),
							(argList.getValue<typename ModifierHelpFunction<T_AT5>::Type>(4)),
							(argList.getValue<typename ModifierHelpFunction<T_AT6>::Type>(5)),
							(argList.getValue<typename ModifierHelpFunction<T_AT7>::Type>(6)),
							(argList.getValue<typename ModifierHelpFunction<T_AT8>::Type>(7)));

						return true;
					} 

					template <typename T_AT1,typename T_AT2,typename T_AT3,typename T_AT4,typename T_AT5,
						typename T_AT6,typename T_AT7,typename T_AT8,typename T_AT9>
						static bool callFunction(typename ModifierHelpFunction<T_RETURNVALUE_TYPE>::Type* returnValue,T_RETURNVALUE_TYPE (*func)(T_AT1,T_AT2,T_AT3,T_AT4,T_AT5,T_AT6,T_AT7,T_AT8,T_AT9), const FunctionArgumentList& argList)
					{
						if (argList.size() != 9)
							return false;

						*returnValue = func((argList.getValue<typename ModifierHelpFunction<T_AT1>::Type>(0)),
							(argList.getValue<typename ModifierHelpFunction<T_AT2>::Type>(1)),
							(argList.getValue<typename ModifierHelpFunction<T_AT3>::Type>(2)),
							(argList.getValue<typename ModifierHelpFunction<T_AT4>::Type>(3)),
							(argList.getValue<typename ModifierHelpFunction<T_AT5>::Type>(4)),
							(argList.getValue<typename ModifierHelpFunction<T_AT6>::Type>(5)),
							(argList.getValue<typename ModifierHelpFunction<T_AT7>::Type>(6)),
							(argList.getValue<typename ModifierHelpFunction<T_AT8>::Type>(7)),
							(argList.getValue<typename ModifierHelpFunction<T_AT9>::Type>(8)));

						return true;
					} 

					template <typename T_AT1,typename T_AT2,typename T_AT3,typename T_AT4,typename T_AT5,
						typename T_AT6,typename T_AT7,typename T_AT8,typename T_AT9,typename T_AT10>
						static bool callFunction(typename ModifierHelpFunction<T_RETURNVALUE_TYPE>::Type* returnValue,T_RETURNVALUE_TYPE (*func)(T_AT1,T_AT2,T_AT3,T_AT4,T_AT5,T_AT6,T_AT7,T_AT8,T_AT9,T_AT10), const FunctionArgumentList& argList)
					{
						if (argList.size() != 10)
							return false;

						*returnValue = func((argList.getValue<typename ModifierHelpFunction<T_AT1>::Type>(0)),
							(argList.getValue<typename ModifierHelpFunction<T_AT2>::Type>(1)),
							(argList.getValue<typename ModifierHelpFunction<T_AT3>::Type>(2)),
							(argList.getValue<typename ModifierHelpFunction<T_AT4>::Type>(3)),
							(argList.getValue<typename ModifierHelpFunction<T_AT5>::Type>(4)),
							(argList.getValue<typename ModifierHelpFunction<T_AT6>::Type>(5)),
							(argList.getValue<typename ModifierHelpFunction<T_AT7>::Type>(6)),
							(argList.getValue<typename ModifierHelpFunction<T_AT8>::Type>(7)),
							(argList.getValue<typename ModifierHelpFunction<T_AT9>::Type>(8)),
							(argList.getValue<typename ModifierHelpFunction<T_AT10>::Type>(9)));

						return true;
					} 



					// methods
					template <typename T_FUNCTION_OWNER>
					static bool callMember(typename ModifierHelpFunction<T_RETURNVALUE_TYPE>::Type* returnValue,T_FUNCTION_OWNER& owner, T_RETURNVALUE_TYPE (T_FUNCTION_OWNER::*func)(), const FunctionArgumentList& argList)
					{
						if (argList.size() != 0)
							return false;

						*returnValue = (owner.*func)();

						return true;
					}

					template <typename T_FUNCTION_OWNER>
					static bool callMember(typename ModifierHelpFunction<T_RETURNVALUE_TYPE>::Type* returnValue,T_FUNCTION_OWNER& owner, T_RETURNVALUE_TYPE (T_FUNCTION_OWNER::*func)() const, const FunctionArgumentList& argList)
					{
						if (argList.size() != 0)
							return false;

						*returnValue = (owner.*func)();

						return true;
					}

					template <typename T_FUNCTION_OWNER,typename T_AT1>
					static bool callMember(typename ModifierHelpFunction<T_RETURNVALUE_TYPE>::Type* returnValue,T_FUNCTION_OWNER& owner, T_RETURNVALUE_TYPE (T_FUNCTION_OWNER::*func)(T_AT1), const FunctionArgumentList& argList)
					{
						if (argList.size() != 1)
							return false;

						*returnValue = (owner.*func)((argList.getValue<typename ModifierHelpFunction<T_AT1>::Type>(0)));

						return true;
					}

					template <typename T_FUNCTION_OWNER,typename T_AT1>
					static bool callMember(typename ModifierHelpFunction<T_RETURNVALUE_TYPE>::Type* returnValue,T_FUNCTION_OWNER& owner, T_RETURNVALUE_TYPE (T_FUNCTION_OWNER::*func)(T_AT1) const, const FunctionArgumentList& argList)
					{
						if (argList.size() != 1)
							return false;

						*returnValue = (owner.*func)((argList.getValue<typename ModifierHelpFunction<T_AT1>::Type>(0)));

						return true;
					}


					template <typename T_FUNCTION_OWNER,typename T_AT1,typename T_AT2>
					static bool callMember(typename ModifierHelpFunction<T_RETURNVALUE_TYPE>::Type* returnValue,T_FUNCTION_OWNER& owner, T_RETURNVALUE_TYPE (T_FUNCTION_OWNER::*func)(T_AT1,T_AT2), const FunctionArgumentList& argList)
					{
						if (argList.size() != 2)
							return false;

						*returnValue = (owner.*func)((argList.getValue<typename ModifierHelpFunction<T_AT1>::Type>(0)),
							(argList.getValue<typename ModifierHelpFunction<T_AT2>::Type>(1)));

						return true;
					}

					template <typename T_FUNCTION_OWNER,typename T_AT1,typename T_AT2>
					static bool callMember(typename ModifierHelpFunction<T_RETURNVALUE_TYPE>::Type* returnValue,T_FUNCTION_OWNER& owner, T_RETURNVALUE_TYPE (T_FUNCTION_OWNER::*func)(T_AT1,T_AT2) const, const FunctionArgumentList& argList)
					{
						if (argList.size() != 2)
							return false;

						*returnValue = (owner.*func)((argList.getValue<typename ModifierHelpFunction<T_AT1>::Type>(0)),
							(argList.getValue<typename ModifierHelpFunction<T_AT2>::Type>(1)));

						return true;
					}

					template <typename T_FUNCTION_OWNER,typename T_AT1,typename T_AT2,typename T_AT3>
					static bool callMember(typename ModifierHelpFunction<T_RETURNVALUE_TYPE>::Type* returnValue,T_FUNCTION_OWNER& owner, T_RETURNVALUE_TYPE (T_FUNCTION_OWNER::*func)(T_AT1,T_AT2,T_AT3), const FunctionArgumentList& argList)
					{
						if (argList.size() != 3)
							return false;

						*returnValue = (owner.*func)((argList.getValue<typename ModifierHelpFunction<T_AT1>::Type>(0)),
							(argList.getValue<typename ModifierHelpFunction<T_AT2>::Type>(1)),
							(argList.getValue<typename ModifierHelpFunction<T_AT3>::Type>(2)));

						return true;
					}

					template <typename T_FUNCTION_OWNER,typename T_AT1,typename T_AT2,typename T_AT3>
					static bool callMember(typename ModifierHelpFunction<T_RETURNVALUE_TYPE>::Type* returnValue,T_FUNCTION_OWNER& owner, T_RETURNVALUE_TYPE (T_FUNCTION_OWNER::*func)(T_AT1,T_AT2,T_AT3) const, const FunctionArgumentList& argList)
					{
						if (argList.size() != 3)
							return false;

						*returnValue = (owner.*func)((argList.getValue<typename ModifierHelpFunction<T_AT1>::Type>(0)),
							(argList.getValue<typename ModifierHelpFunction<T_AT2>::Type>(1)),
							(argList.getValue<typename ModifierHelpFunction<T_AT3>::Type>(2)));

						return true;
					}

					template <typename T_FUNCTION_OWNER,typename T_AT1,typename T_AT2,typename T_AT3,typename T_AT4>
					static bool callMember(typename ModifierHelpFunction<T_RETURNVALUE_TYPE>::Type* returnValue,T_FUNCTION_OWNER& owner, T_RETURNVALUE_TYPE (T_FUNCTION_OWNER::*func)(T_AT1,T_AT2,T_AT3,T_AT4), const FunctionArgumentList& argList)
					{
						if (argList.size() != 4)
							return false;

						*returnValue = (owner.*func)((argList.getValue<typename ModifierHelpFunction<T_AT1>::Type>(0)),
							(argList.getValue<typename ModifierHelpFunction<T_AT2>::Type>(1)),
							(argList.getValue<typename ModifierHelpFunction<T_AT3>::Type>(2)),
							(argList.getValue<typename ModifierHelpFunction<T_AT4>::Type>(3)));


						return true;
					}

					template <typename T_FUNCTION_OWNER,typename T_AT1,typename T_AT2,typename T_AT3,typename T_AT4>
					static bool callMember(typename ModifierHelpFunction<T_RETURNVALUE_TYPE>::Type* returnValue,T_FUNCTION_OWNER& owner, T_RETURNVALUE_TYPE (T_FUNCTION_OWNER::*func)(T_AT1,T_AT2,T_AT3,T_AT4) const, const FunctionArgumentList& argList)
					{
						if (argList.size() != 4)
							return false;

						*returnValue = (owner.*func)((argList.getValue<typename ModifierHelpFunction<T_AT1>::Type>(0)),
							(argList.getValue<typename ModifierHelpFunction<T_AT2>::Type>(1)),
							(argList.getValue<typename ModifierHelpFunction<T_AT3>::Type>(2)),
							(argList.getValue<typename ModifierHelpFunction<T_AT4>::Type>(3)));


						return true;
					}

					template <typename T_FUNCTION_OWNER,typename T_AT1,typename T_AT2,typename T_AT3,typename T_AT4,typename T_AT5>
					static bool callMember(typename ModifierHelpFunction<T_RETURNVALUE_TYPE>::Type* returnValue,T_FUNCTION_OWNER& owner, T_RETURNVALUE_TYPE (T_FUNCTION_OWNER::*func)(T_AT1,T_AT2,T_AT3,T_AT4,T_AT5), const FunctionArgumentList& argList)
					{
						if (argList.size() != 5)
							return false;

						*returnValue = (owner.*func)((argList.getValue<typename ModifierHelpFunction<T_AT1>::Type>(0)),
							(argList.getValue<typename ModifierHelpFunction<T_AT2>::Type>(1)),
							(argList.getValue<typename ModifierHelpFunction<T_AT3>::Type>(2)),
							(argList.getValue<typename ModifierHelpFunction<T_AT4>::Type>(3)),
							(argList.getValue<typename ModifierHelpFunction<T_AT5>::Type>(4)));


						return true;
					}

					template <typename T_FUNCTION_OWNER,typename T_AT1,typename T_AT2,typename T_AT3,typename T_AT4,typename T_AT5>
					static bool callMember(typename ModifierHelpFunction<T_RETURNVALUE_TYPE>::Type* returnValue,T_FUNCTION_OWNER& owner, T_RETURNVALUE_TYPE (T_FUNCTION_OWNER::*func)(T_AT1,T_AT2,T_AT3,T_AT4,T_AT5) const, const FunctionArgumentList& argList)
					{
						if (argList.size() != 5)
							return false;

						*returnValue = (owner.*func)((argList.getValue<typename ModifierHelpFunction<T_AT1>::Type>(0)),
							(argList.getValue<typename ModifierHelpFunction<T_AT2>::Type>(1)),
							(argList.getValue<typename ModifierHelpFunction<T_AT3>::Type>(2)),
							(argList.getValue<typename ModifierHelpFunction<T_AT4>::Type>(3)),
							(argList.getValue<typename ModifierHelpFunction<T_AT5>::Type>(4)));


						return true;
					}

					template <typename T_FUNCTION_OWNER,typename T_AT1,typename T_AT2,typename T_AT3,typename T_AT4,typename T_AT5,
						typename T_AT6>
						static bool callMember(typename ModifierHelpFunction<T_RETURNVALUE_TYPE>::Type* returnValue,T_FUNCTION_OWNER& owner, T_RETURNVALUE_TYPE (T_FUNCTION_OWNER::*func)(T_AT1,T_AT2,T_AT3,T_AT4,T_AT5,T_AT6), const FunctionArgumentList& argList)
					{
						if (argList.size() != 6)
							return false;

						*returnValue = (owner.*func)((argList.getValue<typename ModifierHelpFunction<T_AT1>::Type>(0)),
							(argList.getValue<typename ModifierHelpFunction<T_AT2>::Type>(1)),
							(argList.getValue<typename ModifierHelpFunction<T_AT3>::Type>(2)),
							(argList.getValue<typename ModifierHelpFunction<T_AT4>::Type>(3)),
							(argList.getValue<typename ModifierHelpFunction<T_AT5>::Type>(4)),
							(argList.getValue<typename ModifierHelpFunction<T_AT6>::Type>(5)));


						return true;
					}

					template <typename T_FUNCTION_OWNER,typename T_AT1,typename T_AT2,typename T_AT3,typename T_AT4,typename T_AT5,
						typename T_AT6>
						static bool callMember(typename ModifierHelpFunction<T_RETURNVALUE_TYPE>::Type* returnValue,T_FUNCTION_OWNER& owner, T_RETURNVALUE_TYPE (T_FUNCTION_OWNER::*func)(T_AT1,T_AT2,T_AT3,T_AT4,T_AT5,T_AT6) const, const FunctionArgumentList& argList)
					{
						if (argList.size() != 6)
							return false;

						*returnValue = (owner.*func)((argList.getValue<typename ModifierHelpFunction<T_AT1>::Type>(0)),
							(argList.getValue<typename ModifierHelpFunction<T_AT2>::Type>(1)),
							(argList.getValue<typename ModifierHelpFunction<T_AT3>::Type>(2)),
							(argList.getValue<typename ModifierHelpFunction<T_AT4>::Type>(3)),
							(argList.getValue<typename ModifierHelpFunction<T_AT5>::Type>(4)),
							(argList.getValue<typename ModifierHelpFunction<T_AT6>::Type>(5)));


						return true;
					}

					template <typename T_FUNCTION_OWNER,typename T_AT1,typename T_AT2,typename T_AT3,typename T_AT4,typename T_AT5,
						typename T_AT6, typename T_AT7>
						static bool callMember(typename ModifierHelpFunction<T_RETURNVALUE_TYPE>::Type* returnValue,T_FUNCTION_OWNER& owner, T_RETURNVALUE_TYPE (T_FUNCTION_OWNER::*func)(T_AT1,T_AT2,T_AT3,T_AT4,T_AT5,T_AT6,T_AT7), const FunctionArgumentList& argList)
					{
						if (argList.size() != 7)
							return false;

						*returnValue = (owner.*func)((argList.getValue<typename ModifierHelpFunction<T_AT1>::Type>(0)),
							(argList.getValue<typename ModifierHelpFunction<T_AT2>::Type>(1)),
							(argList.getValue<typename ModifierHelpFunction<T_AT3>::Type>(2)),
							(argList.getValue<typename ModifierHelpFunction<T_AT4>::Type>(3)),
							(argList.getValue<typename ModifierHelpFunction<T_AT5>::Type>(4)),
							(argList.getValue<typename ModifierHelpFunction<T_AT6>::Type>(5)),
							(argList.getValue<typename ModifierHelpFunction<T_AT7>::Type>(6)));


						return true;
					}

					template <typename T_FUNCTION_OWNER,typename T_AT1,typename T_AT2,typename T_AT3,typename T_AT4,typename T_AT5,
						typename T_AT6, typename T_AT7>
						static bool callMember(typename ModifierHelpFunction<T_RETURNVALUE_TYPE>::Type* returnValue,T_FUNCTION_OWNER& owner, T_RETURNVALUE_TYPE (T_FUNCTION_OWNER::*func)(T_AT1,T_AT2,T_AT3,T_AT4,T_AT5,T_AT6,T_AT7) const, const FunctionArgumentList& argList)
					{
						if (argList.size() != 7)
							return false;

						*returnValue = (owner.*func)((argList.getValue<typename ModifierHelpFunction<T_AT1>::Type>(0)),
							(argList.getValue<typename ModifierHelpFunction<T_AT2>::Type>(1)),
							(argList.getValue<typename ModifierHelpFunction<T_AT3>::Type>(2)),
							(argList.getValue<typename ModifierHelpFunction<T_AT4>::Type>(3)),
							(argList.getValue<typename ModifierHelpFunction<T_AT5>::Type>(4)),
							(argList.getValue<typename ModifierHelpFunction<T_AT6>::Type>(5)),
							(argList.getValue<typename ModifierHelpFunction<T_AT7>::Type>(6)));


						return true;
					}

					template <typename T_FUNCTION_OWNER,typename T_AT1,typename T_AT2,typename T_AT3,typename T_AT4,typename T_AT5,
						typename T_AT6,typename T_AT7,typename T_AT8>
						static bool callMember(typename ModifierHelpFunction<T_RETURNVALUE_TYPE>::Type* returnValue,T_FUNCTION_OWNER& owner, T_RETURNVALUE_TYPE (T_FUNCTION_OWNER::*func)(T_AT1,T_AT2,T_AT3,T_AT4,T_AT5,T_AT6,T_AT7,T_AT8), const FunctionArgumentList& argList)
					{
						if (argList.size() != 8)
							return false;

						*returnValue = (owner.*func)((argList.getValue<typename ModifierHelpFunction<T_AT1>::Type>(0)),
							(argList.getValue<typename ModifierHelpFunction<T_AT2>::Type>(1)),
							(argList.getValue<typename ModifierHelpFunction<T_AT3>::Type>(2)),
							(argList.getValue<typename ModifierHelpFunction<T_AT4>::Type>(3)),
							(argList.getValue<typename ModifierHelpFunction<T_AT5>::Type>(4)),
							(argList.getValue<typename ModifierHelpFunction<T_AT6>::Type>(5)),
							(argList.getValue<typename ModifierHelpFunction<T_AT7>::Type>(6)),
							(argList.getValue<typename ModifierHelpFunction<T_AT8>::Type>(7)));

						return true;
					}

					template <typename T_FUNCTION_OWNER,typename T_AT1,typename T_AT2,typename T_AT3,typename T_AT4,typename T_AT5,
						typename T_AT6,typename T_AT7,typename T_AT8>
						static bool callMember(typename ModifierHelpFunction<T_RETURNVALUE_TYPE>::Type* returnValue,T_FUNCTION_OWNER& owner, T_RETURNVALUE_TYPE (T_FUNCTION_OWNER::*func)(T_AT1,T_AT2,T_AT3,T_AT4,T_AT5,T_AT6,T_AT7,T_AT8) const, const FunctionArgumentList& argList)
					{
						if (argList.size() != 8)
							return false;

						*returnValue = (owner.*func)((argList.getValue<typename ModifierHelpFunction<T_AT1>::Type>(0)),
							(argList.getValue<typename ModifierHelpFunction<T_AT2>::Type>(1)),
							(argList.getValue<typename ModifierHelpFunction<T_AT3>::Type>(2)),
							(argList.getValue<typename ModifierHelpFunction<T_AT4>::Type>(3)),
							(argList.getValue<typename ModifierHelpFunction<T_AT5>::Type>(4)),
							(argList.getValue<typename ModifierHelpFunction<T_AT6>::Type>(5)),
							(argList.getValue<typename ModifierHelpFunction<T_AT7>::Type>(6)),
							(argList.getValue<typename ModifierHelpFunction<T_AT8>::Type>(7)));


						return true;
					}

					template <typename T_FUNCTION_OWNER,typename T_AT1,typename T_AT2,typename T_AT3,typename T_AT4,typename T_AT5,
						typename T_AT6,typename T_AT7,typename T_AT8,typename T_AT9>
						static bool callMember(typename ModifierHelpFunction<T_RETURNVALUE_TYPE>::Type* returnValue,T_FUNCTION_OWNER& owner, T_RETURNVALUE_TYPE (T_FUNCTION_OWNER::*func)(T_AT1,T_AT2,T_AT3,T_AT4,T_AT5,T_AT6,T_AT7,T_AT8,T_AT9), const FunctionArgumentList& argList)
					{
						if (argList.size() != 9)
							return false;

						*returnValue = (owner.*func)((argList.getValue<typename ModifierHelpFunction<T_AT1>::Type>(0)),
							(argList.getValue<typename ModifierHelpFunction<T_AT2>::Type>(1)),
							(argList.getValue<typename ModifierHelpFunction<T_AT3>::Type>(2)),
							(argList.getValue<typename ModifierHelpFunction<T_AT4>::Type>(3)),
							(argList.getValue<typename ModifierHelpFunction<T_AT5>::Type>(4)),
							(argList.getValue<typename ModifierHelpFunction<T_AT6>::Type>(5)),
							(argList.getValue<typename ModifierHelpFunction<T_AT7>::Type>(6)),
							(argList.getValue<typename ModifierHelpFunction<T_AT8>::Type>(7)),
							(argList.getValue<typename ModifierHelpFunction<T_AT9>::Type>(8)));


						return true;
					}


					template <typename T_FUNCTION_OWNER,typename T_AT1,typename T_AT2,typename T_AT3,typename T_AT4,typename T_AT5,
						typename T_AT6,typename T_AT7,typename T_AT8,typename T_AT9>
						static bool callMember(typename ModifierHelpFunction<T_RETURNVALUE_TYPE>::Type* returnValue,T_FUNCTION_OWNER& owner, T_RETURNVALUE_TYPE (T_FUNCTION_OWNER::*func)(T_AT1,T_AT2,T_AT3,T_AT4,T_AT5,T_AT6,T_AT7,T_AT8,T_AT9) const, const FunctionArgumentList& argList)
					{
						if (argList.size() != 9)
							return false;

						*returnValue = (owner.*func)((argList.getValue<typename ModifierHelpFunction<T_AT1>::Type>(0)),
							(argList.getValue<typename ModifierHelpFunction<T_AT2>::Type>(1)),
							(argList.getValue<typename ModifierHelpFunction<T_AT3>::Type>(2)),
							(argList.getValue<typename ModifierHelpFunction<T_AT4>::Type>(3)),
							(argList.getValue<typename ModifierHelpFunction<T_AT5>::Type>(4)),
							(argList.getValue<typename ModifierHelpFunction<T_AT6>::Type>(5)),
							(argList.getValue<typename ModifierHelpFunction<T_AT7>::Type>(6)),
							(argList.getValue<typename ModifierHelpFunction<T_AT8>::Type>(7)),
							(argList.getValue<typename ModifierHelpFunction<T_AT9>::Type>(8)));


						return true;
					}


					template <typename T_FUNCTION_OWNER,typename T_AT1,typename T_AT2,typename T_AT3,typename T_AT4,typename T_AT5,
						typename T_AT6,typename T_AT7,typename T_AT8,typename T_AT9,typename T_AT10>
						static bool callMember(typename ModifierHelpFunction<T_RETURNVALUE_TYPE>::Type* returnValue,T_FUNCTION_OWNER& owner, T_RETURNVALUE_TYPE (T_FUNCTION_OWNER::*func)(T_AT1,T_AT2,T_AT3,T_AT4,T_AT5,T_AT6,T_AT7,T_AT8,T_AT9,T_AT10), const FunctionArgumentList& argList)
					{
						if (argList.size() != 10)
							return false;

						*returnValue = (owner.*func)((argList.getValue<typename ModifierHelpFunction<T_AT1>::Type>(0)),
							(argList.getValue<typename ModifierHelpFunction<T_AT2>::Type>(1)),
							(argList.getValue<typename ModifierHelpFunction<T_AT3>::Type>(2)),
							(argList.getValue<typename ModifierHelpFunction<T_AT4>::Type>(3)),
							(argList.getValue<typename ModifierHelpFunction<T_AT5>::Type>(4)),
							(argList.getValue<typename ModifierHelpFunction<T_AT6>::Type>(5)),
							(argList.getValue<typename ModifierHelpFunction<T_AT7>::Type>(6)),
							(argList.getValue<typename ModifierHelpFunction<T_AT8>::Type>(7)),
							(argList.getValue<typename ModifierHelpFunction<T_AT9>::Type>(8)),
							(argList.getValue<typename ModifierHelpFunction<T_AT10>::Type>(9)));


						return true;
					}

					template <typename T_FUNCTION_OWNER,typename T_AT1,typename T_AT2,typename T_AT3,typename T_AT4,typename T_AT5,
						typename T_AT6,typename T_AT7,typename T_AT8,typename T_AT9,typename T_AT10>
						static bool callMember(typename ModifierHelpFunction<T_RETURNVALUE_TYPE>::Type* returnValue,T_FUNCTION_OWNER& owner, T_RETURNVALUE_TYPE (T_FUNCTION_OWNER::*func)(T_AT1,T_AT2,T_AT3,T_AT4,T_AT5,T_AT6,T_AT7,T_AT8,T_AT9,T_AT10) const, const FunctionArgumentList& argList)
					{
						if (argList.size() != 10)
							return false;

						*returnValue = 
							(owner.*func)((argList.getValue<typename ModifierHelpFunction<T_AT1>::Type>(0)),
							(argList.getValue<typename ModifierHelpFunction<T_AT2>::Type>(1)),
							(argList.getValue<typename ModifierHelpFunction<T_AT3>::Type>(2)),
							(argList.getValue<typename ModifierHelpFunction<T_AT4>::Type>(3)),
							(argList.getValue<typename ModifierHelpFunction<T_AT5>::Type>(4)),
							(argList.getValue<typename ModifierHelpFunction<T_AT6>::Type>(5)),
							(argList.getValue<typename ModifierHelpFunction<T_AT7>::Type>(6)),
							(argList.getValue<typename ModifierHelpFunction<T_AT8>::Type>(7)),
							(argList.getValue<typename ModifierHelpFunction<T_AT9>::Type>(8)),
							(argList.getValue<typename ModifierHelpFunction<T_AT10>::Type>(9)));


						return true;
					}

				};

				// specialized for void
				// bypass to caller functions without return value
				template<>
				class CallerClassWithReturnValues<void>
				{
				public:
					// with returnvalues

					template<typename T_FUNC>
					static bool callFunction(void* returnValue,T_FUNC func, const FunctionArgumentList& argList)
					{
						return CallerClassWithoutReturnValues<void>::callFunction(func,argList);
					}



					// methods
					template <typename T_FUNCTION_OWNER,typename T_FUNC>
					static bool callMember(void* returnValue,T_FUNCTION_OWNER& owner, T_FUNC func, const FunctionArgumentList& argList)
					{
						return CallerClassWithoutReturnValues<void>::callMember(owner,func,argList);
					}
				};


				// caller classes
				// need static function pointer, each compilers handles member function pointer somehow different...
				// with returnvalue
#define CALL_FUNCTION_WR_BODY \
	return CallerClassWithReturnValues<T_RETURNVALUE_TYPE>::callFunction((typename ModifierHelpFunction<T_RETURNVALUE_TYPE>::Type*)returnValue,func,argList);


				// without returnvalue
#define CALL_FUNCTION_BODY \
	return CallerClassWithoutReturnValues<T_RETURNVALUE_TYPE>::callFunction(func,argList);

#define CALL_METHOD_WR_BODY \
	return CallerClassWithReturnValues<T_RETURNVALUE_TYPE>::callMember((typename ModifierHelpFunction<T_RETURNVALUE_TYPE>::Type*)returnValue,owner,func,argList);

#if XAIT_OS == XAIT_OS_WIN
#define CALL_METHOD_BODY \
	return CallerClassWithoutReturnValues<T_RETURNVALUE_TYPE>::callMember<T_OWNER>(owner,func,argList);
#else
#define CALL_METHOD_BODY \
	return CallerClassWithoutReturnValues<T_RETURNVALUE_TYPE>::callMember(owner,func,argList);
#endif



				class Callerclass
				{
				public:
					template<typename T_RETURNVALUE_TYPE> 
					static bool callGFunc(void* returnValue,T_RETURNVALUE_TYPE (*func)(), const FunctionArgumentList& argList)
					{
						CALL_FUNCTION_WR_BODY;
					}

					template<typename T_RETURNVALUE_TYPE, typename T_AT1> 
					static bool callGFunc(void* returnValue,T_RETURNVALUE_TYPE (*func)(T_AT1), const FunctionArgumentList& argList)
					{
						CALL_FUNCTION_WR_BODY;
					}

					template<typename T_RETURNVALUE_TYPE, typename T_AT1, typename T_AT2> 
					static bool callGFunc(void* returnValue,T_RETURNVALUE_TYPE (*func)(T_AT1,T_AT2), const FunctionArgumentList& argList)
					{
						CALL_FUNCTION_WR_BODY;
					}

					template<typename T_RETURNVALUE_TYPE, typename T_AT1, typename T_AT2, typename T_AT3> 
					static bool callGFunc(void* returnValue,T_RETURNVALUE_TYPE (*func)(T_AT1,T_AT2,T_AT3), const FunctionArgumentList& argList)
					{
						CALL_FUNCTION_WR_BODY;
					}

					template<typename T_RETURNVALUE_TYPE, typename T_AT1, typename T_AT2, typename T_AT3, typename T_AT4> 
					static bool callGFunc(void* returnValue,T_RETURNVALUE_TYPE (*func)(T_AT1,T_AT2,T_AT3,T_AT4), const FunctionArgumentList& argList)
					{
						CALL_FUNCTION_WR_BODY;
					}

					template<typename T_RETURNVALUE_TYPE, typename T_AT1, typename T_AT2, typename T_AT3, typename T_AT4, typename T_AT5> 
					static bool callGFunc(void* returnValue,T_RETURNVALUE_TYPE (*func)(T_AT1,T_AT2,T_AT3,T_AT4,T_AT5), const FunctionArgumentList& argList)
					{
						CALL_FUNCTION_WR_BODY;
					}

					template<typename T_RETURNVALUE_TYPE, typename T_AT1, typename T_AT2, typename T_AT3, typename T_AT4, typename T_AT5,
						typename T_AT6> 
						static bool callGFunc(void* returnValue,T_RETURNVALUE_TYPE (*func)(T_AT1,T_AT2,T_AT3,T_AT4,T_AT5,T_AT6), const FunctionArgumentList& argList)
					{
						CALL_FUNCTION_WR_BODY;
					}

					template<typename T_RETURNVALUE_TYPE, typename T_AT1, typename T_AT2, typename T_AT3, typename T_AT4, typename T_AT5,
						typename T_AT6, typename T_AT7> 
						static bool callGFunc(void* returnValue,T_RETURNVALUE_TYPE (*func)(T_AT1,T_AT2,T_AT3,T_AT4,T_AT5,T_AT6,T_AT7), const FunctionArgumentList& argList)
					{
						CALL_FUNCTION_WR_BODY;
					}

					template<typename T_RETURNVALUE_TYPE, typename T_AT1, typename T_AT2, typename T_AT3, typename T_AT4, typename T_AT5,
						typename T_AT6, typename T_AT7, typename T_AT8> 
						static bool callGFunc(void* returnValue,T_RETURNVALUE_TYPE (*func)(T_AT1,T_AT2,T_AT3,T_AT4,T_AT5,T_AT6,T_AT7,T_AT8), const FunctionArgumentList& argList)
					{
						CALL_FUNCTION_WR_BODY;
					}

					template<typename T_RETURNVALUE_TYPE, typename T_AT1, typename T_AT2, typename T_AT3, typename T_AT4, typename T_AT5,
						typename T_AT6, typename T_AT7, typename T_AT8, typename T_AT9> 
						static bool callGFunc(void* returnValue,T_RETURNVALUE_TYPE (*func)(T_AT1,T_AT2,T_AT3,T_AT4,T_AT5,T_AT6,T_AT7,T_AT8,T_AT9), const FunctionArgumentList& argList)
					{
						CALL_FUNCTION_WR_BODY;
					}

					template<typename T_RETURNVALUE_TYPE, typename T_AT1, typename T_AT2, typename T_AT3, typename T_AT4, typename T_AT5,
						typename T_AT6, typename T_AT7, typename T_AT8, typename T_AT9, typename T_AT10> 
						static bool callGFunc(void* returnValue,T_RETURNVALUE_TYPE (*func)(T_AT1,T_AT2,T_AT3,T_AT4,T_AT5,T_AT6,T_AT7,T_AT8,T_AT9,T_AT10), const FunctionArgumentList& argList)
					{
						CALL_FUNCTION_WR_BODY;
					}



					template<typename T_RETURNVALUE_TYPE> 
					static bool callGFunc(T_RETURNVALUE_TYPE (*func)(), const FunctionArgumentList& argList)
					{
						CALL_FUNCTION_BODY;
					}

					template<typename T_RETURNVALUE_TYPE, typename T_AT1> 
					static bool callGFunc(T_RETURNVALUE_TYPE (*func)(T_AT1), const FunctionArgumentList& argList)
					{
						CALL_FUNCTION_BODY;
					}

					template<typename T_RETURNVALUE_TYPE, typename T_AT1, typename T_AT2> 
					static bool callGFunc(T_RETURNVALUE_TYPE (*func)(T_AT1,T_AT2), const FunctionArgumentList& argList)
					{
						CALL_FUNCTION_BODY;
					}

					template<typename T_RETURNVALUE_TYPE, typename T_AT1, typename T_AT2, typename T_AT3> 
					static bool callGFunc(T_RETURNVALUE_TYPE (*func)(T_AT1,T_AT2,T_AT3), const FunctionArgumentList& argList)
					{
						CALL_FUNCTION_BODY;
					}

					template<typename T_RETURNVALUE_TYPE, typename T_AT1, typename T_AT2, typename T_AT3, typename T_AT4> 
					static bool callGFunc(T_RETURNVALUE_TYPE (*func)(T_AT1,T_AT2,T_AT3,T_AT4), const FunctionArgumentList& argList)
					{
						CALL_FUNCTION_BODY;
					}

					template<typename T_RETURNVALUE_TYPE, typename T_AT1, typename T_AT2, typename T_AT3, typename T_AT4, typename T_AT5> 
					static bool callGFunc(T_RETURNVALUE_TYPE (*func)(T_AT1,T_AT2,T_AT3,T_AT4,T_AT5), const FunctionArgumentList& argList)
					{
						CALL_FUNCTION_BODY;
					}

					template<typename T_RETURNVALUE_TYPE, typename T_AT1, typename T_AT2, typename T_AT3, typename T_AT4, typename T_AT5,
						typename T_AT6> 
						static bool callGFunc(T_RETURNVALUE_TYPE (*func)(T_AT1,T_AT2,T_AT3,T_AT4,T_AT5,T_AT6), const FunctionArgumentList& argList)
					{
						CALL_FUNCTION_BODY;
					}

					template<typename T_RETURNVALUE_TYPE, typename T_AT1, typename T_AT2, typename T_AT3, typename T_AT4, typename T_AT5,
						typename T_AT6, typename T_AT7> 
						static bool callGFunc(T_RETURNVALUE_TYPE (*func)(T_AT1,T_AT2,T_AT3,T_AT4,T_AT5,T_AT6,T_AT7), const FunctionArgumentList& argList)
					{
						CALL_FUNCTION_BODY;
					}

					template<typename T_RETURNVALUE_TYPE, typename T_AT1, typename T_AT2, typename T_AT3, typename T_AT4, typename T_AT5,
						typename T_AT6, typename T_AT7, typename T_AT8> 
						static bool callGFunc(T_RETURNVALUE_TYPE (*func)(T_AT1,T_AT2,T_AT3,T_AT4,T_AT5,T_AT6,T_AT7,T_AT8), const FunctionArgumentList& argList)
					{
						CALL_FUNCTION_BODY;
					}

					template<typename T_RETURNVALUE_TYPE, typename T_AT1, typename T_AT2, typename T_AT3, typename T_AT4, typename T_AT5,
						typename T_AT6, typename T_AT7, typename T_AT8, typename T_AT9> 
						static bool callGFunc(T_RETURNVALUE_TYPE (*func)(T_AT1,T_AT2,T_AT3,T_AT4,T_AT5,T_AT6,T_AT7,T_AT8,T_AT9), const FunctionArgumentList& argList)
					{
						CALL_FUNCTION_BODY;
					}

					template<typename T_RETURNVALUE_TYPE, typename T_AT1, typename T_AT2, typename T_AT3, typename T_AT4, typename T_AT5,
						typename T_AT6, typename T_AT7, typename T_AT8, typename T_AT9, typename T_AT10> 
						static bool callGFunc(T_RETURNVALUE_TYPE (*func)(T_AT1,T_AT2,T_AT3,T_AT4,T_AT5,T_AT6,T_AT7,T_AT8,T_AT9,T_AT10), const FunctionArgumentList& argList)
					{
						CALL_FUNCTION_BODY;
					}



					// methods
					// with returnvalues 

					template<typename T_OWNER,typename T_RETURNVALUE_TYPE> 
					static bool callMethod(void* returnValue,T_OWNER& owner, T_RETURNVALUE_TYPE (T_OWNER::*func)(), const FunctionArgumentList& argList)
					{
						CALL_METHOD_WR_BODY;
					}

					template<typename T_OWNER,typename T_RETURNVALUE_TYPE> 
					static  bool callMethod(void* returnValue,T_OWNER& owner, T_RETURNVALUE_TYPE (T_OWNER::*func)() const, const FunctionArgumentList& argList)
					{
						CALL_METHOD_WR_BODY;
					}

					template<typename T_OWNER,typename T_RETURNVALUE_TYPE, typename T_AT1> 
					static bool callMethod(void* returnValue,T_OWNER& owner, T_RETURNVALUE_TYPE (T_OWNER::*func)(T_AT1), const FunctionArgumentList& argList)
					{
						CALL_METHOD_WR_BODY;
					}

					template<typename T_OWNER,typename T_RETURNVALUE_TYPE, typename T_AT1> 
					static bool callMethod(void* returnValue,T_OWNER& owner, T_RETURNVALUE_TYPE (T_OWNER::*func)(T_AT1) const, const FunctionArgumentList& argList)
					{
						CALL_METHOD_WR_BODY;
					}

					template<typename T_OWNER,typename T_RETURNVALUE_TYPE, typename T_AT1, typename T_AT2> 
					static bool callMethod(void* returnValue,T_OWNER& owner, T_RETURNVALUE_TYPE (T_OWNER::*func)(T_AT1,T_AT2), const FunctionArgumentList& argList)
					{
						CALL_METHOD_WR_BODY;
					}

					template<typename T_OWNER,typename T_RETURNVALUE_TYPE, typename T_AT1, typename T_AT2> 
					static bool callMethod(void* returnValue,T_OWNER& owner, T_RETURNVALUE_TYPE (T_OWNER::*func)(T_AT1,T_AT2) const, const FunctionArgumentList& argList)
					{
						CALL_METHOD_WR_BODY;
					}

					template<typename T_OWNER,typename T_RETURNVALUE_TYPE, typename T_AT1, typename T_AT2, typename T_AT3> 
					static bool callMethod(void* returnValue,T_OWNER& owner, T_RETURNVALUE_TYPE (T_OWNER::*func)(T_AT1,T_AT2,T_AT3), const FunctionArgumentList& argList)
					{
						CALL_METHOD_WR_BODY;
					}

					template<typename T_OWNER,typename T_RETURNVALUE_TYPE, typename T_AT1, typename T_AT2, typename T_AT3> 
					static bool callMethod(void* returnValue,T_OWNER& owner, T_RETURNVALUE_TYPE (T_OWNER::*func)(T_AT1,T_AT2,T_AT3) const, const FunctionArgumentList& argList)
					{
						CALL_METHOD_WR_BODY;
					}

					template<typename T_OWNER,typename T_RETURNVALUE_TYPE, typename T_AT1, typename T_AT2, typename T_AT3, typename T_AT4> 
					static bool callMethod(void* returnValue,T_OWNER& owner, T_RETURNVALUE_TYPE (T_OWNER::*func)(T_AT1,T_AT2,T_AT3,T_AT4), const FunctionArgumentList& argList)
					{
						CALL_METHOD_WR_BODY;
					}

					template<typename T_OWNER,typename T_RETURNVALUE_TYPE, typename T_AT1, typename T_AT2, typename T_AT3, typename T_AT4> 
					static bool callMethod(void* returnValue,T_OWNER& owner, T_RETURNVALUE_TYPE (T_OWNER::*func)(T_AT1,T_AT2,T_AT3,T_AT4) const, const FunctionArgumentList& argList)
					{
						CALL_METHOD_WR_BODY;
					}

					template<typename T_OWNER,typename T_RETURNVALUE_TYPE, typename T_AT1, typename T_AT2, typename T_AT3, typename T_AT4, typename T_AT5> 
					static bool callMethod(void* returnValue,T_OWNER& owner, T_RETURNVALUE_TYPE (T_OWNER::*func)(T_AT1,T_AT2,T_AT3,T_AT4,T_AT5), const FunctionArgumentList& argList)
					{
						CALL_METHOD_WR_BODY;
					}

					template<typename T_OWNER,typename T_RETURNVALUE_TYPE, typename T_AT1, typename T_AT2, typename T_AT3, typename T_AT4, typename T_AT5> 
					static bool callMethod(void* returnValue,T_OWNER& owner, T_RETURNVALUE_TYPE (T_OWNER::*func)(T_AT1,T_AT2,T_AT3,T_AT4,T_AT5) const, const FunctionArgumentList& argList)
					{
						CALL_METHOD_WR_BODY;
					}


					template<typename T_OWNER,typename T_RETURNVALUE_TYPE, typename T_AT1, typename T_AT2, typename T_AT3, typename T_AT4, typename T_AT5,
						typename T_AT6> 
						static bool callMethod(void* returnValue,T_OWNER& owner, T_RETURNVALUE_TYPE (T_OWNER::*func)(T_AT1,T_AT2,T_AT3,T_AT4,T_AT5,T_AT6), const FunctionArgumentList& argList)
					{
						CALL_METHOD_WR_BODY;
					}

					template<typename T_OWNER,typename T_RETURNVALUE_TYPE, typename T_AT1, typename T_AT2, typename T_AT3, typename T_AT4, typename T_AT5,
						typename T_AT6> 
						static bool callMethod(void* returnValue,T_OWNER& owner, T_RETURNVALUE_TYPE (T_OWNER::*func)(T_AT1,T_AT2,T_AT3,T_AT4,T_AT5,T_AT6) const, const FunctionArgumentList& argList)
					{
						CALL_METHOD_WR_BODY;
					}

					template<typename T_OWNER,typename T_RETURNVALUE_TYPE, typename T_AT1, typename T_AT2, typename T_AT3, typename T_AT4, typename T_AT5,
						typename T_AT6, typename T_AT7> 
						static bool callMethod(void* returnValue,T_OWNER& owner, T_RETURNVALUE_TYPE (T_OWNER::*func)(T_AT1,T_AT2,T_AT3,T_AT4,T_AT5,T_AT6,T_AT7), const FunctionArgumentList& argList)
					{
						CALL_METHOD_WR_BODY;
					}

					template<typename T_OWNER,typename T_RETURNVALUE_TYPE, typename T_AT1, typename T_AT2, typename T_AT3, typename T_AT4, typename T_AT5,
						typename T_AT6, typename T_AT7> 
						static bool callMethod(void* returnValue,T_OWNER& owner, T_RETURNVALUE_TYPE (T_OWNER::*func)(T_AT1,T_AT2,T_AT3,T_AT4,T_AT5,T_AT6,T_AT7) const, const FunctionArgumentList& argList)
					{
						CALL_METHOD_WR_BODY;
					}

					template<typename T_OWNER,typename T_RETURNVALUE_TYPE, typename T_AT1, typename T_AT2, typename T_AT3, typename T_AT4, typename T_AT5,
						typename T_AT6, typename T_AT7, typename T_AT8> 
						static bool callMethod(void* returnValue,T_OWNER& owner, T_RETURNVALUE_TYPE (T_OWNER::*func)(T_AT1,T_AT2,T_AT3,T_AT4,T_AT5,T_AT6,T_AT7,T_AT8), const FunctionArgumentList& argList)
					{
						CALL_METHOD_WR_BODY;
					}

					template<typename T_OWNER,typename T_RETURNVALUE_TYPE, typename T_AT1, typename T_AT2, typename T_AT3, typename T_AT4, typename T_AT5,
						typename T_AT6, typename T_AT7, typename T_AT8> 
						static bool callMethod(void* returnValue,T_OWNER& owner, T_RETURNVALUE_TYPE (T_OWNER::*func)(T_AT1,T_AT2,T_AT3,T_AT4,T_AT5,T_AT6,T_AT7,T_AT8) const, const FunctionArgumentList& argList)
					{
						CALL_METHOD_WR_BODY;
					}

					template<typename T_OWNER,typename T_RETURNVALUE_TYPE, typename T_AT1, typename T_AT2, typename T_AT3, typename T_AT4, typename T_AT5,
						typename T_AT6, typename T_AT7, typename T_AT8, typename T_AT9> 
						static bool callMethod(void* returnValue,T_OWNER& owner, T_RETURNVALUE_TYPE (T_OWNER::*func)(T_AT1,T_AT2,T_AT3,T_AT4,T_AT5,T_AT6,T_AT7,T_AT8,T_AT9), const FunctionArgumentList& argList)
					{
						CALL_METHOD_WR_BODY;
					}

					template<typename T_OWNER,typename T_RETURNVALUE_TYPE, typename T_AT1, typename T_AT2, typename T_AT3, typename T_AT4, typename T_AT5,
						typename T_AT6, typename T_AT7, typename T_AT8, typename T_AT9> 
						static bool callMethod(void* returnValue,T_OWNER& owner, T_RETURNVALUE_TYPE (T_OWNER::*func)(T_AT1,T_AT2,T_AT3,T_AT4,T_AT5,T_AT6,T_AT7,T_AT8,T_AT9) const, const FunctionArgumentList& argList)
					{
						CALL_METHOD_WR_BODY;
					}

					template<typename T_OWNER,typename T_RETURNVALUE_TYPE, typename T_AT1, typename T_AT2, typename T_AT3, typename T_AT4, typename T_AT5,
						typename T_AT6, typename T_AT7, typename T_AT8, typename T_AT9, typename T_AT10> 
						static bool callMethod(void* returnValue,T_OWNER& owner, T_RETURNVALUE_TYPE (T_OWNER::*func)(T_AT1,T_AT2,T_AT3,T_AT4,T_AT5,T_AT6,T_AT7,T_AT8,T_AT9,T_AT10), const FunctionArgumentList& argList)
					{
						CALL_METHOD_WR_BODY;
					}

					template<typename T_OWNER,typename T_RETURNVALUE_TYPE, typename T_AT1, typename T_AT2, typename T_AT3, typename T_AT4, typename T_AT5,
						typename T_AT6, typename T_AT7, typename T_AT8, typename T_AT9, typename T_AT10> 
						static bool callMethod(void* returnValue,T_OWNER& owner, T_RETURNVALUE_TYPE (T_OWNER::*func)(T_AT1,T_AT2,T_AT3,T_AT4,T_AT5,T_AT6,T_AT7,T_AT8,T_AT9,T_AT10) const, const FunctionArgumentList& argList)
					{
						CALL_METHOD_WR_BODY;
					}


					// without returnvalues		

					template<typename T_OWNER,typename T_RETURNVALUE_TYPE> 
					static bool callMethod(T_OWNER& owner, T_RETURNVALUE_TYPE (T_OWNER::*func)(), const FunctionArgumentList& argList)
					{
						CALL_METHOD_BODY;
					}
					template<typename T_OWNER,typename T_RETURNVALUE_TYPE> 
					static bool callMethod(T_OWNER& owner, T_RETURNVALUE_TYPE (T_OWNER::*func)() const, const FunctionArgumentList& argList)
					{
						CALL_METHOD_BODY;
					}

					template<typename T_OWNER,typename T_RETURNVALUE_TYPE, typename T_AT1> 
					static bool callMethod(T_OWNER& owner, T_RETURNVALUE_TYPE (T_OWNER::*func)(T_AT1), const FunctionArgumentList& argList)
					{
						CALL_METHOD_BODY;
					}
					template<typename T_OWNER,typename T_RETURNVALUE_TYPE,typename T_AT1> 
					static bool callMethod(T_OWNER& owner, T_RETURNVALUE_TYPE (T_OWNER::*func)(T_AT1) const, const FunctionArgumentList& argList)
					{
						CALL_METHOD_BODY;
					}

					template<typename T_OWNER,typename T_RETURNVALUE_TYPE, typename T_AT1, typename T_AT2> 
					static bool callMethod(T_OWNER& owner, T_RETURNVALUE_TYPE (T_OWNER::*func)(T_AT1,T_AT2), const FunctionArgumentList& argList)
					{
						CALL_METHOD_BODY;
					}
					template<typename T_OWNER,typename T_RETURNVALUE_TYPE, typename T_AT1, typename T_AT2> 
					static bool callMethod(T_OWNER& owner, T_RETURNVALUE_TYPE (T_OWNER::*func)(T_AT1,T_AT2) const, const FunctionArgumentList& argList)
					{
						CALL_METHOD_BODY;
					}

					template<typename T_OWNER,typename T_RETURNVALUE_TYPE, typename T_AT1, typename T_AT2, typename T_AT3> 
					static bool callMethod(T_OWNER& owner, T_RETURNVALUE_TYPE (T_OWNER::*func)(T_AT1,T_AT2,T_AT3), const FunctionArgumentList& argList)
					{
						CALL_METHOD_BODY;
					}
					template<typename T_OWNER,typename T_RETURNVALUE_TYPE, typename T_AT1, typename T_AT2, typename T_AT3> 
					static bool callMethod(T_OWNER& owner, T_RETURNVALUE_TYPE (T_OWNER::*func)(T_AT1,T_AT2,T_AT3) const, const FunctionArgumentList& argList)
					{
						CALL_METHOD_BODY;
					}

					template<typename T_OWNER,typename T_RETURNVALUE_TYPE, typename T_AT1, typename T_AT2, typename T_AT3, typename T_AT4> 
					static bool callMethod(T_OWNER& owner, T_RETURNVALUE_TYPE (T_OWNER::*func)(T_AT1,T_AT2,T_AT3,T_AT4), const FunctionArgumentList& argList)
					{
						CALL_METHOD_BODY;
					}
					template<typename T_OWNER,typename T_RETURNVALUE_TYPE, typename T_AT1, typename T_AT2, typename T_AT3, typename T_AT4> 
					static bool callMethod(T_OWNER& owner, T_RETURNVALUE_TYPE (T_OWNER::*func)(T_AT1,T_AT2,T_AT3,T_AT4) const, const FunctionArgumentList& argList)
					{
						CALL_METHOD_BODY;
					}

					template<typename T_OWNER,typename T_RETURNVALUE_TYPE, typename T_AT1, typename T_AT2, typename T_AT3, typename T_AT4, typename T_AT5> 
					static bool callMethod(T_OWNER& owner, T_RETURNVALUE_TYPE (T_OWNER::*func)(T_AT1,T_AT2,T_AT3,T_AT4,T_AT5), const FunctionArgumentList& argList)
					{
						CALL_METHOD_BODY;
					}
					template<typename T_OWNER,typename T_RETURNVALUE_TYPE, typename T_AT1, typename T_AT2, typename T_AT3, typename T_AT4, typename T_AT5> 
					static bool callMethod(T_OWNER& owner, T_RETURNVALUE_TYPE (T_OWNER::*func)(T_AT1,T_AT2,T_AT3,T_AT4,T_AT5) const, const FunctionArgumentList& argList)
					{
						CALL_METHOD_BODY;
					}

					template<typename T_OWNER,typename T_RETURNVALUE_TYPE, typename T_AT1, typename T_AT2, typename T_AT3, typename T_AT4, typename T_AT5,
						typename T_AT6> 
						static bool callMethod(T_OWNER& owner, T_RETURNVALUE_TYPE (T_OWNER::*func)(T_AT1,T_AT2,T_AT3,T_AT4,T_AT5,T_AT6), const FunctionArgumentList& argList)
					{
						CALL_METHOD_BODY;
					}
					template<typename T_OWNER,typename T_RETURNVALUE_TYPE, typename T_AT1, typename T_AT2, typename T_AT3, typename T_AT4, typename T_AT5,
						typename T_AT6> 
						static bool callMethod(T_OWNER& owner, T_RETURNVALUE_TYPE (T_OWNER::*func)(T_AT1,T_AT2,T_AT3,T_AT4,T_AT5,T_AT6) const, const FunctionArgumentList& argList)
					{
						CALL_METHOD_BODY;
					}

					template<typename T_OWNER,typename T_RETURNVALUE_TYPE, typename T_AT1, typename T_AT2, typename T_AT3, typename T_AT4, typename T_AT5,
						typename T_AT6, typename T_AT7> 
						static bool callMethod(T_OWNER& owner, T_RETURNVALUE_TYPE (T_OWNER::*func)(T_AT1,T_AT2,T_AT3,T_AT4,T_AT5,T_AT6,T_AT7), const FunctionArgumentList& argList)
					{
						CALL_METHOD_BODY;
					}
					template<typename T_OWNER,typename T_RETURNVALUE_TYPE, typename T_AT1, typename T_AT2, typename T_AT3, typename T_AT4, typename T_AT5,
						typename T_AT6, typename T_AT7> 
						static bool callMethod(T_OWNER& owner, T_RETURNVALUE_TYPE (T_OWNER::*func)(T_AT1,T_AT2,T_AT3,T_AT4,T_AT5,T_AT6,T_AT7) const, const FunctionArgumentList& argList)
					{
						CALL_METHOD_BODY;
					}

					template<typename T_OWNER,typename T_RETURNVALUE_TYPE, typename T_AT1, typename T_AT2, typename T_AT3, typename T_AT4, typename T_AT5,
						typename T_AT6, typename T_AT7, typename T_AT8> 
						static bool callMethod(T_OWNER& owner, T_RETURNVALUE_TYPE (T_OWNER::*func)(T_AT1,T_AT2,T_AT3,T_AT4,T_AT5,T_AT6,T_AT7,T_AT8), const FunctionArgumentList& argList)
					{
						CALL_METHOD_BODY;
					}
					template<typename T_OWNER,typename T_RETURNVALUE_TYPE, typename T_AT1, typename T_AT2, typename T_AT3, typename T_AT4, typename T_AT5,
						typename T_AT6, typename T_AT7, typename T_AT8> 
						static bool callMethod(T_OWNER& owner, T_RETURNVALUE_TYPE (T_OWNER::*func)(T_AT1,T_AT2,T_AT3,T_AT4,T_AT5,T_AT6,T_AT7,T_AT8) const, const FunctionArgumentList& argList)
					{
						CALL_METHOD_BODY;
					}

					template<typename T_OWNER,typename T_RETURNVALUE_TYPE, typename T_AT1, typename T_AT2, typename T_AT3, typename T_AT4, typename T_AT5,
						typename T_AT6, typename T_AT7, typename T_AT8, typename T_AT9> 
						static bool callMethod(T_OWNER& owner, T_RETURNVALUE_TYPE (T_OWNER::*func)(T_AT1,T_AT2,T_AT3,T_AT4,T_AT5,T_AT6,T_AT7,T_AT8,T_AT9), const FunctionArgumentList& argList)
					{
						CALL_METHOD_BODY;
					}
					template<typename T_OWNER,typename T_RETURNVALUE_TYPE, typename T_AT1, typename T_AT2, typename T_AT3, typename T_AT4, typename T_AT5,
						typename T_AT6, typename T_AT7, typename T_AT8, typename T_AT9> 
						static bool callMethod(T_OWNER& owner, T_RETURNVALUE_TYPE (T_OWNER::*func)(T_AT1,T_AT2,T_AT3,T_AT4,T_AT5,T_AT6,T_AT7,T_AT8,T_AT9) const, const FunctionArgumentList& argList)
					{
						CALL_METHOD_BODY;
					}

					template<typename T_OWNER,typename T_RETURNVALUE_TYPE, typename T_AT1, typename T_AT2, typename T_AT3, typename T_AT4, typename T_AT5,
						typename T_AT6, typename T_AT7, typename T_AT8, typename T_AT9, typename T_AT10> 
						static bool callMethod(T_OWNER& owner, T_RETURNVALUE_TYPE (T_OWNER::*func)(T_AT1,T_AT2,T_AT3,T_AT4,T_AT5,T_AT6,T_AT7,T_AT8,T_AT9,T_AT10), const FunctionArgumentList& argList)
					{
						CALL_METHOD_BODY;
					}
					template<typename T_OWNER,typename T_RETURNVALUE_TYPE, typename T_AT1, typename T_AT2, typename T_AT3, typename T_AT4, typename T_AT5,
						typename T_AT6, typename T_AT7, typename T_AT8, typename T_AT9, typename T_AT10> 
						static bool callMethod(T_OWNER& owner, T_RETURNVALUE_TYPE (T_OWNER::*func)(T_AT1,T_AT2,T_AT3,T_AT4,T_AT5,T_AT6,T_AT7,T_AT8,T_AT9,T_AT10) const, const FunctionArgumentList& argList)
					{
						CALL_METHOD_BODY;
					}
				};

				template<typename T_FUNCTION_TYPE>
				class ArgFunctionCaller
				{
				public:
					static bool callFunc(void* returnValue,CFunction* cFunction,void* owner,const FunctionArgumentList& argList)
					{
						return Callerclass::callGFunc(*((T_FUNCTION_TYPE*)cFunction->mInternalFunctionPointer),argList);
					}		

					static bool callFuncWR(void* returnValue,CFunction* cFunction,void* owner,const FunctionArgumentList& argList)
					{
						return Callerclass::callGFunc(returnValue,*((T_FUNCTION_TYPE*)cFunction->mInternalFunctionPointer),argList);
					}
				};

				template<typename T_FUNCTION_OWNER_TYPE,typename T_FUNCTION_TYPE>
				class ArgMethodCaller
				{
				public:
					static bool callFunc(void* returnValue,CFunction* cFunction,void* owner,const FunctionArgumentList& argList)
					{
						return Callerclass::callMethod<T_FUNCTION_OWNER_TYPE>(**((T_FUNCTION_OWNER_TYPE**)cFunction->mFunctionOwner),*((T_FUNCTION_TYPE*)cFunction->mInternalFunctionPointer),argList);	
					}	

					static bool callFuncWR(void* returnValue,CFunction* cFunction,void* owner,const FunctionArgumentList& argList)
					{
						return Callerclass::callMethod<T_FUNCTION_OWNER_TYPE>(returnValue,**((T_FUNCTION_OWNER_TYPE**)cFunction->mFunctionOwner),*((T_FUNCTION_TYPE*)cFunction->mInternalFunctionPointer),argList);	
					}	
				};

				template<typename T_FUNCTION_OWNER_TYPE,typename T_FUNCTION_TYPE>
				class ArgOwnMethodCaller
				{
				public:
					static bool callFunc(void* returnValue,CFunction* cFunction,void* owner,const FunctionArgumentList& argList)
					{
						return Callerclass::callMethod<T_FUNCTION_OWNER_TYPE>(*((T_FUNCTION_OWNER_TYPE*)owner),*((T_FUNCTION_TYPE*)cFunction->mInternalFunctionPointer),argList);	
					}		

					static bool callFuncWR(void* returnValue,CFunction* cFunction,void* owner,const FunctionArgumentList& argList)
					{
						return Callerclass::callMethod<T_FUNCTION_OWNER_TYPE>(returnValue,*((T_FUNCTION_OWNER_TYPE*)owner),*((T_FUNCTION_TYPE*)cFunction->mInternalFunctionPointer),argList);	
					}		
				};

				template<typename T_DUMMY_RETURNVALUE_TYPE>
				class FunctionCaller
				{
					typedef T_DUMMY_RETURNVALUE_TYPE (*Functor)(void*,Datatype::DatatypeID,const FunctionArgumentList&);
				public:
					static bool callFunc(void* returnValue,CFunction* cFunction,void* owner,const FunctionArgumentList& argList)
					{
						Functor& func = *((Functor*)cFunction->mInternalFunctionPointer);
						func(returnValue,Datatype::DatatypeID(),argList);
						return true;
					}
					static bool callFuncWR(void* returnValue,CFunction* cFunction,void* owner,const FunctionArgumentList& argList)
					{
						Functor& func = *((Functor*)cFunction->mInternalFunctionPointer);
						func(returnValue,cFunction->getReturnValueType(),argList);
						return true;
					}
				};

				template<typename T_DUMMY_RETURNVALUE_TYPE,typename T_FUNCTION_OWNER_TYPE>
				class MethodCaller
				{
					typedef T_DUMMY_RETURNVALUE_TYPE (T_FUNCTION_OWNER_TYPE::*Functor)(void*,Datatype::DatatypeID,const FunctionArgumentList&);
				public:
					static bool callFunc(void* returnValue,CFunction* cFunction,void* owner,const FunctionArgumentList& argList)
					{
						Functor& func = *((Functor*)cFunction->mInternalFunctionPointer);
						T_FUNCTION_OWNER_TYPE& newOwner = **((T_FUNCTION_OWNER_TYPE**) cFunction->mFunctionOwner);
						(newOwner.*func)(returnValue,Datatype::DatatypeID(),argList);
						return true;
					}
					static bool callFuncWR(void* returnValue,CFunction* cFunction,void* owner,const FunctionArgumentList& argList)
					{
						Functor& func = *((Functor*)cFunction->mInternalFunctionPointer);
						T_FUNCTION_OWNER_TYPE& newOwner = **((T_FUNCTION_OWNER_TYPE**) cFunction->mFunctionOwner);
						(newOwner.*func)(returnValue,cFunction->getReturnValueType(),argList);
						return true;
					}
				};

				template<typename T_DUMMY_RETURNVALUE_TYPE,typename T_FUNCTION_OWNER_TYPE>
				class OwnMethodCaller
				{
					typedef T_DUMMY_RETURNVALUE_TYPE (T_FUNCTION_OWNER_TYPE::*Functor)(void*,Datatype::DatatypeID,const FunctionArgumentList&);
				public:
					static bool callFunc(void* returnValue,CFunction* cFunction,void* owner,const FunctionArgumentList& argList)
					{
						Functor& func = *((Functor*)cFunction->mInternalFunctionPointer);
						T_FUNCTION_OWNER_TYPE& newOwner = **((T_FUNCTION_OWNER_TYPE**) owner);
						(newOwner.*func)(returnValue,Datatype::DatatypeID(),argList);
						return true;
					}
					static bool callFuncWR(void* returnValue,CFunction* cFunction,void* owner,const FunctionArgumentList& argList)
					{
						Functor& func = *((Functor*)cFunction->mInternalFunctionPointer);
						T_FUNCTION_OWNER_TYPE& newOwner = **((T_FUNCTION_OWNER_TYPE**) owner);
						(newOwner.*func)(returnValue,cFunction->getReturnValueType(),argList);
						return true;
					}
				};
			}
		}
	}
}
