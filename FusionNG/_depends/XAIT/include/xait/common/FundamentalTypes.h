// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/Platform.h>

// defines for fundamental datatypes
namespace XAIT
{
	typedef unsigned char       uint08;		//!< unsigned integer with 8-bit
	typedef signed char			int08;		//!< integer with 8-bit
	typedef unsigned short      uint16;		//!< unsigned integer with 16-bit
	typedef short               int16;		//!< integer with 16-bit
	typedef unsigned int        uint32;		//!< unsigned integer with 32-bit
	typedef int					int32;		//!< integer with 32-bit
	typedef unsigned long long	uint64;		//!< unsigned integer with 64-bit
	typedef long long			int64;		//!< integer with 64-bit
	typedef float               float32;	//!< float with single precession - 32 bit wide
	typedef double              float64;	//!< float with double precession - 64 bit wide

#if XAIT_OS == XAIT_OS_WIN
	#if _MSC_VER < 1400 
		typedef unsigned long		sysptr;
	#else
		typedef intptr_t			sysptr;		//!< system pointer
	#endif
#else 
	#if XAIT_ARCH_POINTERSIZE == 4
		typedef unsigned long		sysptr;
	#else
		typedef unsigned long long	sysptr;
	#endif
#endif


	typedef char				BChar;		//!< byte character (multi byte)
	typedef wchar_t				WChar;		//!< word character (unicode)

	typedef BChar				Char;		//!< short cut to BChar
}

#define X_MIN_UINT08		0
#define X_MAX_UINT08		0xFF
#define X_MIN_UINT16		0
#define X_MAX_UINT16		0xFFFF
#define X_MIN_UINT32		0
#define X_MAX_UINT32		0xFFFFFFFF
#define X_MIN_UINT64		0
#define X_MAX_UINT64		0xFFFFFFFFFFFFFFFFULL
#define X_MIN_INT08			((int08)0x80)
#define X_MAX_INT08			((int08)0x7F)
#define X_MIN_INT16			((int16)0x8000)
#define X_MAX_INT16			((int16)0x7FFF)
#define X_MIN_INT32			((int32)0x80000000)
#define X_MAX_INT32			((int32)0x7FFFFFFF)
#define X_MIN_INT64			0x8000000000000000LL
#define X_MAX_INT64			0x7FFFFFFFFFFFFFFFLL
#define X_MIN_FLOAT32		-3.402823466e38f
#define X_MAX_FLOAT32		3.402823466e38f
#define X_MIN_FLOAT64		-1.7976931348623157e308
#define X_MAX_FLOAT64		1.7976931348623157e308

#define X_UNDEF_FLOAT32		X_MAX_FLOAT32

#ifndef NULL
	#define	NULL		0
#endif

#ifndef MAX_PATH
	#if !(XAIT_OS == XAIT_OS_XBOX360EXT)
		#define MAX_PATH	256
	#endif
#endif

