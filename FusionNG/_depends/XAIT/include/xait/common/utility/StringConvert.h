// (C) xaitment GmbH 2006-2012

#pragma once 

#include <xait/common/FundamentalTypes.h>
#include <xait/common/String.h>

namespace XAIT
{
	namespace Common
	{
		namespace Utility
		{
			template<typename T>
			class StringConvert
			{
			public:
				static Common::String toString(const T& value)
				{
					return Common::String(value);
				}
			};

			// specialization for fundamental types
			template<>
			class StringConvert<bool>
			{
			public:
				inline static Common::String toString(const bool value)
				{
					return value ? "True" : "False";
				}
			};

			template<>
			class StringConvert<uint08>
			{
			public:
				inline static Common::String toString(const uint08 value)
				{
					return Common::String((uint32)value);
				}
			};

			//template<>
			//class StringConvert<int08>
			//{
			//public:
			//	inline static Common::String toString(const int08 value)
			//	{
			//		return Common::String((int32)value);
			//	}
			//};

			template<>
			class StringConvert<uint16>
			{
			public:
				inline static Common::String toString(const uint16 value)
				{
					return Common::String((uint32)value);
				}
			};

			template<>
			class StringConvert<int16>
			{
			public:
				inline static Common::String toString(const int16 value)
				{
					return Common::String((int32)value);
				}
			};

			template<>
			class StringConvert<uint32>
			{
			public:
				inline static Common::String toString(const uint32 value)
				{
					return Common::String((uint32)value);
				}
			};

			template<>
			class StringConvert<int32>
			{
			public:
				inline static Common::String toString(const int32 value)
				{
					return Common::String((int32)value);
				}
			};

			template<>
			class StringConvert<uint64>
			{
			public:
				inline static Common::String toString(const uint64 value)
				{
					return Common::String(value);
				}
			};

			template<>
			class StringConvert<int64>
			{
			public:
				inline static Common::String toString(const int64 value)
				{
					return Common::String(value);
				}
			};

			template<>
			class StringConvert<float32>
			{
			public:
				inline static Common::String toString(const float32 value)
				{
					return Common::String(value);
				}
			};

			template<>
			class StringConvert<float64>
			{
			public:
				inline static Common::String toString(const float64 value)
				{
					return Common::String((int32)value);
				}
			};

			template<typename CHAR_T>
			class StringConvert<Common::BasicString<CHAR_T> >
			{
			public:
				inline static Common::String toString(const Common::BasicString<CHAR_T>& value)
				{
					return Common::String(value);
				}
			};

			template<>
			class StringConvert<Common::String>
			{
			public:
				inline static Common::String toString(const Common::String& value)
				{
					return value;
				}
			};

			template<>
			class StringConvert<BChar>
			{
			public:
				inline static Common::String toString(const BChar value)
				{
					return Common::String(value);
				}
			};

			template<>
			class StringConvert<WChar>
			{
			public:
				inline static Common::String toString(const WChar value)
				{
					return Common::String(value);
				}
			};

			template<typename T_COMP>
			class StringConvert<Math::Vec2<T_COMP> >
			{
			public:
				inline static Common::String toString(const Math::Vec2<T_COMP>& value)
				{
					return Common::String("(" + String(value.mX) + "/" + String(value.mY) + ")");
				}
			};

			template<typename T_COMP>
			class StringConvert<Math::Vec3<T_COMP> >
			{
			public:
				inline static Common::String toString(const Math::Vec3<T_COMP>& value)
				{
					return Common::String("(" + String(value.mX) + "/" + String(value.mY) + "/" + String(value.mZ) + ")");
				}
			};

			template<typename T>
			inline Common::String ToString(const T& value)
			{
				return StringConvert<T>::toString(value);
			}

		}
	}
}

