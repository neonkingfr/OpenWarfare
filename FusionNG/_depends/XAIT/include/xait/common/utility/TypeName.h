// (C) xaitment GmbH 2006-2012

#pragma once

#define TYPENAME_ILLEGAL_CHARS	"<>,"
#define TYPENAME_REPLACE_CHAR	'_'

#include <xait/common/String.h>

namespace XAIT
{
	namespace Common
	{
		namespace Utility
		{
			template<typename T>
			class TypeName
			{
			public:
				static String getName()
				{
					return typeid(T).name();
				}

				static String getCleanName()
				{
					const char* typeName= typeid(T).name();

					// remove datastruct definition in front of the typename
					uint32 pos= 0;
					while(typeName[pos] && typeName[pos] != ' ') ++pos;

					if (!typeName[pos]) return typeName;

					return &typeName[pos+1];
				}

				static String getLowerCleanName()
				{
					String cleanName= getCleanName();
					cleanName= cleanName.toLower();
				
					uint32 pos= 0;
					while(cleanName[pos])
					{
						// replace illegal chars
						checkCharAndReplace(cleanName[pos],TYPENAME_ILLEGAL_CHARS,TYPENAME_REPLACE_CHAR);
						++pos;
					}
					return cleanName;
				}

			private:
				static void checkCharAndReplace(char& c, const char* illegalChars, const char replaceChar)
				{
					uint32 pos= 0;

					while(illegalChars[pos] && c != illegalChars[pos]) ++pos;
					if (illegalChars[pos])
					{
						c= replaceChar;
					}
				}
			};
		}
	}
	
}
