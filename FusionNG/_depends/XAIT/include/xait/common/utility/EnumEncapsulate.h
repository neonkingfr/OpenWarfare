// (C) xaitment GmbH 2006-2012

#pragma once 


#define STRUCT_TO_ENUM_MEMBERS(structname,enumname,defaultvalue)	\
	public:	\
	inline structname()	\
	: mEnumVal(defaultvalue)	\
	{}	\
	inline structname(const int val)	\
	: mEnumVal((enumname)val)	\
	{}	\
	inline structname(const enumname val)	\
	: mEnumVal(val)	\
	{}	\
	inline operator int() const	\
	{ return (int)mEnumVal; }	\
	inline structname& operator=(const enumname val)	\
	{ mEnumVal= val; return *this; }	\
	inline bool operator==(const structname& other)	const	\
	{ return mEnumVal == other.mEnumVal; }	\
	inline bool operator!=(const structname& other)	const	\
	{ return mEnumVal != other.mEnumVal; }	\
	inline bool operator==(const enumname other) const	\
	{ return mEnumVal == other; }	\
	inline bool operator!=(const enumname other) const	\
	{ return mEnumVal != other; }	\
	private:	\
	enumname	mEnumVal;	\
	public:

