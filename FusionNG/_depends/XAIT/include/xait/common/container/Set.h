// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/memory/STLAllocator.h>
#include <xait/common/container/Pair.h>
#include <set>


namespace XAIT
{
	namespace Common
	{
		namespace Container
		{
			//! \brief Set class. A set holds a number of values, where each value can only 
			//! exist once in the set.
			template<typename T>
			class Set
			{
			public:
				typedef std::set<T,std::less<T>,Memory::STLAllocator<T> >	SetType;
				typedef typename SetType::iterator							Iterator;
				typedef typename SetType::const_iterator					ConstIterator;

			public:
				//! \brief constructor
				//! \param allocator	memory allocator for new elements in the set
				Set(const Memory::Allocator::Ptr& allocator)
					: mSet(std::less<T>(),allocator)
				{}

				//! \brief get the memory allocator for this vector
				inline Memory::Allocator::Ptr getAllocator() const
				{
					return mSet.get_allocator().getMemoryAlloc();
				}


				//! \brief add an element to the set
				//! \param value	value to add to the set
				//! \returns true if the value could be added or false, if there was already an element
				//! \remark Remember that a set can hold each value just once.
				inline bool add(const T& value)
				{
					return mSet.insert(value).second;
				}

				//! \brief remove an element from the set
				//! \param value	value to remove
				//! \returns true if the element could be removed
				inline bool remove(const T& value)
				{
					return mSet.erase(value) > 0;
				}

				//! \brief remove an element from the set by its iterator position
				//! \param position		position in the set where we want to delete an item
				inline bool remove(Iterator position)
				{
					return mSet.erase(position) != mSet.end(); 
				}

				//! \brief insert an element into the set
				//! \param value		value to insert
				//! \returns a pair where the first element is an iterator to the inserted element and the second value
				//!	is a bool which defines if an element has been new inserted (true) or if an element already exists (false).
				inline Pair<Iterator,bool> insert(const T& value)
				{
					std::pair<Iterator,bool> retVal= mSet.insert(value);
					return MakePair(retVal.first,retVal.second);
				};

				//! \brief get the beginning of the set
				inline Iterator begin() 
				{
					return mSet.begin();
				}

				//! \brief get the end of the set
				inline Iterator end() 
				{
					return mSet.end();
				}

				//! \brief get the beginning of the set
				inline ConstIterator begin() const
				{
					return mSet.begin();
				}

				//! \brief get the end of the set
				inline ConstIterator end() const
				{
					return mSet.end();
				}

				//! \brief test if the set is empty
				//! \returns true if the set is empty
				inline bool empty() const
				{
					return mSet.empty();
				}

				//! \brief find an element in the set
				//! \returns the iterator of to the found element, or the end iterator if the element to the key has not been found
				inline Iterator find(const T& key) 
				{
					return mSet.find(key);
				}

				//! \brief find an element in the set
				//! \returns the iterator of to the found element, or the end iterator if the element to the key has not been found
				inline ConstIterator find(const T& key) const
				{
					return mSet.find(key);
				}

				//! \brief get the number of elements in the set
				//! \returns the number of elements in the set
				inline uint32 size() const
				{
					return (uint32)mSet.size();
				}

				//! \brief remove all elements from the set
				inline void clear() 
				{
					mSet.clear();
				}

				//! \brief test if an element is part of the set
				//! \returns true if the element named by the key exists, or false if not.
				bool contains(const T& key) const
				{
					return (mSet.find(key) != mSet.end());
				}

				//! \brief Returns an iterator to the first element in a set with a key that is equal to or greater than a specified key.
				//! \param key	The argument key to be compared with the sort key of an element from the set being searched.
				ConstIterator lowerBound(const T& key) const
				{
					return mSet.lower_bound(key);
				}

				//! \brief Returns an iterator to the first element in a set with a key that is equal to or greater than a specified key.
				//! \param key	The argument key to be compared with the sort key of an element from the set being searched.
				Iterator lowerBound(const T& key)
				{
					return mSet.lower_bound(key);
				}


				//! \brief Returns an iterator to the first element in a set that with a key that is greater than a specified key.
				//! \param key	The argument key to be compared with the sort key of an element from the set being searched.
				ConstIterator upperBound(const T& key) const
				{
					return mSet.upper_bound(key);
				}

				//! \brief Returns an iterator to the first element in a set that with a key that is greater than a specified key.
				//! \param key	The argument key to be compared with the sort key of an element from the set being searched.
				Iterator upperBound(const T& key)
				{
					return mSet.upper_bound(key);
				}


			private:
				SetType		mSet;

			};

		}
	}

}
