// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/Platform.h>
#include <xait/common/FundamentalTypes.h>
#include <xait/common/memory/Allocator.h>
#include <xait/common/utility/TemplatesUtility.h>
#include <xait/common/math/ValueFunc.h>

#define IDVECTOR_RESIZE_SCALE		1.5f		// scale factor of vector each time the vector will be enlarged (must be greater than 1.0f)
#define IDVECTOR_GET_ELEMENT(id)	(*((T*)(data + id * elemSize + 1)))				// get dereferenced element
#define IDVECTOR_GET_PELEMENT(id)	((T*)(data + id * elemSize + 1))				// get pointer to element
#define IDVECTOR_GET_IDTYPE(id)		(*((IDType*)(data + id * elemSize + 1)))		// get freelist id
#define IDVECTOR_GET_VALID(id)		(*((unsigned char*)(data + id * elemSize)))		// get valid bit

namespace XAIT
{
	namespace Common
	{
		namespace Container
		{
			//! Here we describe, how the idvector should manage its ids and objects
			//! \brief Storage policy suitable for every datatype
			template<typename T_OBJECT, typename T_ID>
			class IDVectorStoragePolicy
			{
				typedef typename Utility::MaxSizeType<T_OBJECT,T_ID>::Type MaxSizeType;

				inline T_OBJECT& CastToObject(MaxSizeType& data)
				{
					return (T_OBJECT)data;
				}

				inline T_ID& CastToID(MaxSizeType& data)
				{
					return (T_ID)data;
				}

			public:
				struct SElement
				{
					uint32		mIsID;		// 4 byte alignment

					// we cannot use union, since union does not allow types with user defined constructors
					MaxSizeType	mObject;
				};


				typedef T_ID		IDType;
				typedef SElement	StorageElement;


				StorageElement*		mStorage;		//!< all elements in the idvector
				uint32				mReserved;		//!< number of storage elements, used or not

				IDVectorStoragePolicy()
					: mStorage(NULL),mReserved(0)
				{}

				IDVectorStoragePolicy(const IDVectorStoragePolicy& storage)
					: mStorage(NULL),mReserved(0)
				{}

				inline const T_OBJECT& getAsObject(const StorageElement& sElem) const
				{
					return *((const T_OBJECT*)&sElem.mObject);
				}

				inline T_OBJECT& getAsObject(StorageElement& sElem) const
				{
					return *((T_OBJECT*)&sElem.mObject);
				}

				inline void initAsObject(StorageElement& sElem, const T_OBJECT& object) const
				{
					sElem.mIsID= 0;
					new (&sElem.mObject) T_OBJECT(object);
				}

				inline T_ID getAsID(const StorageElement& sElem) const
				{
					return *((T_ID*)&sElem.mObject);
				}

				inline void initAsID(StorageElement& sElem, const T_ID& id) const
				{
					sElem.mIsID= 1;
					new (&sElem.mObject) T_ID(id);
				}

				inline bool isID(const StorageElement& sElem) const
				{
					return sElem.mIsID == 1;
				}
			};


			//! \brief Storage policy suitable only for pointer types
			template<typename T_OBJECT, typename T_ID>
			class IDVectorStoragePolicy<T_OBJECT*,T_ID>
			{
			public:
				union SElement
				{
					T_OBJECT*	mObject;
					uint08*		mIDPtr;		//!< id as pointer to the element of the id
				};

				typedef T_ID		IDType;
				typedef SElement	StorageElement;
				typedef T_OBJECT*	ObjectType;


				StorageElement*		mStorage;		//!< all elements in the idvector
				uint32				mReserved;		//!< number of storage elements, used or not

				IDVectorStoragePolicy()
					: mStorage(NULL),mReserved(0)
				{}

				IDVectorStoragePolicy(const IDVectorStoragePolicy& storage)
					: mStorage(NULL),mReserved(0)
				{}

				inline const ObjectType& getAsObject(const StorageElement& sElem) const
				{
					return sElem.mObject;
				}

				inline ObjectType& getAsObject(StorageElement& sElem) const
				{
					return sElem.mObject;
				}

				inline void initAsObject(StorageElement& sElem, const ObjectType& object) const
				{
					sElem.mObject= object;
				}

				inline T_ID getAsID(const StorageElement& sElem) const
				{
					return (T_ID)((sElem.mIDPtr - (uint08*)mStorage) / sizeof(ObjectType));
				}

				inline void initAsID(StorageElement& sElem, const T_ID& id) const
				{
					sElem.mIDPtr= (uint08*)(&mStorage[id]);
				}

				inline bool isID(const StorageElement& sElem) const
				{
					// check if storage element is a pointer in the range of the array
					// if so this is an freelist id
					const uint08* arrayStart= reinterpret_cast<const uint08*>(mStorage);
					const uint08* arrayEnd= reinterpret_cast<const uint08*>(&mStorage[mReserved]);
					const uint08* arrayPointer= reinterpret_cast<const uint08*>(sElem.mIDPtr);

					return (arrayStart <= arrayPointer && arrayPointer <= arrayEnd);
				}

			};


			//! This class is a normal vector which is intend for id
			//! distribution for objects. You can add new objects and
			//! receive an id for this object. You cannot specify the
			//! assigned id. It also tries to maintain a compact range
			//! for the ids. If you remove an object, the next added object
			//! will get the last freed position.
			//! \brief vector for assigning ids
			//! \remark This class uses a dynamic vector. If the vector must be resized
			//!			the complete array will be copied with memcpy. so use no data types
			//!			which are no longer initialized if you just copy the content. If you
			//!			want to prevent this error, use pointers to your data types.

			template<typename T_OBJECT, typename T_ID = uint32, int N_DEBUG_HELPER = Utility::CheckForPointer<T_OBJECT>::HAS_POINTER>
			class IDVector : private IDVectorStoragePolicy<T_OBJECT,T_ID>
			{
			public:
				class ConstIterator;
				friend class Iterator;
				friend class ConstIterator;

				typedef typename IDVectorStoragePolicy<T_OBJECT,T_ID>::IDType IDType;
				typedef typename IDVectorStoragePolicy<T_OBJECT,T_ID>::StorageElement StorageElement;

				//! \brief iterator
				class Iterator
				{
					IDType			mPos;		//!< position of the iterator in the datafield
					IDVector*		mOwner;		//!< idvector this iterator belongs to

					friend class IDVector;
					friend class ConstIterator;

				public:
					Iterator()
						: mPos(0),mOwner(NULL)
					{}

					Iterator(const IDType pos, IDVector* owner)
						: mPos(pos),mOwner(owner)
					{}

					Iterator(const ConstIterator& other)
						: mPos(other.mPos),mOwner(other.mOwner)
					{}

					//! \brief increment to the next valid position or gets invalid if end of data reached (check against end-iterator)
					Iterator& operator++()
					{
						X_ASSERT_MSG_DBG(mOwner,"Iterator is not bound to an IDVector");

						// get the next valid id
						mOwner->_getNextID(mPos,mPos);

						return *this;
					}

					//! \brief get element iterator currently points to
					T_OBJECT& operator*() const
					{
						X_ASSERT_MSG_DBG(mOwner,"Iterator is not bound to an IDVector");

						return (*mOwner)[mPos];
					}

					//! \brief get pointer to element iterator points
					T_OBJECT* operator->() const
					{
						X_ASSERT_MSG_DBG(mOwner,"Iterator is not bound to an IDVector");

						return &(*mOwner)[mPos];
					}

					//! \brief compare to iterators
					bool operator==(const Iterator& iter) const
					{
						if (mOwner != iter.mOwner) return false;
						return iter.mPos == mPos;
					}

					//! \brief not equal operator
					bool operator!=(const Iterator& iter) const
					{
						if (mOwner != iter.mOwner) return true;
						return iter.mPos != mPos;
					}

					//! \brief get id this iterator points to
					IDType getID() const
					{
						X_ASSERT_MSG_DBG(mOwner,"Iterator is not bound to an IDVector");
						return mPos;
					}
				};

				//! \brief iterator
				class ConstIterator
				{
					IDType				mPos;		//!< position of the iterator in the datafield
					const IDVector*		mOwner;		//!< idvector this iterator belongs to

					friend class IDVector;
					friend class Iterator;

				public:
					ConstIterator()
						: mPos(0),mOwner(NULL)
					{}

					ConstIterator(const IDType pos, const IDVector* owner)
						: mPos(pos),mOwner(owner)
					{}

					ConstIterator(const Iterator& other)
						: mPos(other.mPos),mOwner(other.mOwner)
					{}

					//! \brief increment to the next valid position or gets invalid if end of data reached (check against end-iterator)
					ConstIterator& operator++()
					{
						X_ASSERT_MSG_DBG(mOwner,"Iterator is not bound to an IDVector");

						// get the next valid id
						mOwner->_getNextID(mPos,mPos);

						return *this;
					}

					//! \brief get element iterator currently points to
					const T_OBJECT& operator*() const
					{
						X_ASSERT_MSG_DBG(mOwner,"Iterator is not bound to an IDVector");

						return (*mOwner)[mPos];
					}

					//! \brief get pointer to element iterator points
					const T_OBJECT* operator->() const
					{
						X_ASSERT_MSG_DBG(mOwner,"Iterator is not bound to an IDVector");

						return &(*mOwner)[mPos];
					}

					//! \brief compare to iterators
					bool operator==(const ConstIterator& iter) const
					{
						if (mOwner != iter.mOwner) return false;
						return iter.mPos == mPos;
					}

					//! \brief not equal operator
					bool operator!=(const ConstIterator& iter) const
					{
						if (mOwner != iter.mOwner) return true;
						return iter.mPos != mPos;
					}

					//! \brief get id this iterator points to
					IDType getID() const
					{
						X_ASSERT_MSG_DBG(mOwner,"Iterator is not bound to an IDVector");
						return mPos;
					}
				};


				IDVector(const Memory::Allocator::Ptr& allocator)
					: mAllocator(allocator),mSize(0),mNextFreePos(0)
				{
					// init data with one element 
					// this element marks the end of the freelist and is always needed
					// so if you create a bigger array it must always be size + 1
					this->mStorage= Memory::Allocate(this->mAllocator,(StorageElement*)0);
					this->initAsID(*(this->mStorage),(IDType)0);

					// init begin / end iterator
					mBeginIter.mPos= 0;
					mBeginIter.mOwner= this;

					mEndIter.mPos= this->mReserved;
					mEndIter.mOwner= this;
				}

				IDVector(const IDVector& other) 
					: IDVectorStoragePolicy<T_OBJECT,T_ID>(other),mAllocator(other.mAllocator),mSize(0),mNextFreePos(0)
				{
					// init data with one element 
					// this element marks the end of the freelist and is always needed
					// so if you create a bigger array it must always be size + 1
					this->mStorage= Memory::Allocate(this->mAllocator,(StorageElement*)0);
					this->initAsID(*(this->mStorage),(IDType)0);

					// init begin / end iterator
					mBeginIter.mPos= this->mReserved;
					mBeginIter.mOwner= this;

					mEndIter.mPos= this->mReserved;
					mEndIter.mOwner= this;

					(*this)= other;
				}

				~IDVector()
				{
					_destroy();
				}


				//! \brief add a new element
				//! \param elem		new element
				//! \returns the id assigned to the new element
				IDType add(const T_OBJECT& elem)
				{
					IDType id= this->_reserveID();

					StorageElement& sElem= this->mStorage[id];
					this->initAsObject(sElem,elem);

					// increment number of elements
					++mSize;

					// check if begin iterator stays valid
					if (id < mBeginIter.mPos) 
					{
						mBeginIter.mPos= id;
					}

					return id;
				}

				//! \brief remove an element
				//! \param id	index of the element to remove
				void remove(const IDType id)
				{
					X_ASSERT_MSG(isValidID(id),"Element with id '" << id << "' cannot be removed since it is invalid");

					// call destructor of stored element
					StorageElement& sElem= this->mStorage[id];
					Memory::CallDestructor(&this->getAsObject(sElem));

					// move id back into the freelist pool
					this->_freeID(id);
					--mSize;

					// make begin iter valid
					if (id == mBeginIter.mPos) 
					{
						this->_getNextID(mBeginIter.mPos,id);
					}
				}

				//! \brief get element for an id (with id check)
				//! \param elem		returned element
				//! \param id		index of the element to get
				//! \returns true if element found or false if id invalid
				bool getElement(T_OBJECT& elem, const IDType id) const
				{
					if (isValidID(id)) 
					{
						elem= this->getAsObject(this->mStorage[id]);
						return true;
					} 
					else 
					{
						return false;
					}
				}

				// \brief find an element 
				// \remark slow since the IDVector has no element ordering O(n)
				Iterator find(const T_OBJECT& t) const
				{
					Iterator iter = mBeginIter;

					while (iter != mEndIter)
					{
						if ((*iter) == t)
							return iter;

						++iter;
					}
					return mEndIter;
				}


				//! \brief get element for an id (without idcheck)
				//! \return reference to the object
				inline T_OBJECT& operator[](const IDType id) 
				{
					X_ASSERT_MSG_DBG(isValidID(id),"ID '" << id << "' points to an invalid item");
					return this->getAsObject(this->mStorage[id]);
				}

				//! \brief get element for an id (without idcheck)
				//! \return reference to the object
				inline const T_OBJECT& operator[](const IDType id)  const
				{
					X_ASSERT_MSG_DBG(isValidID(id),"ID '" << id << "' points to an invalid item");
					return this->getAsObject(this->mStorage[id]);
				}

				//! \brief assign operator
				IDVector& operator=(const IDVector& other)
				{
					this->_destroy();

					this->mReserved= other.mReserved;
					this->mSize= other.mSize;
					this->mNextFreePos= other.mNextFreePos;
					this->mBeginIter.mPos= other.mBeginIter.mPos;
					this->mEndIter.mPos= other.mEndIter.mPos;
					
					// copy data array
					// remember that we have to use the copy constructor for the objects
					this->mStorage= Memory::AllocateArray(mAllocator,this->mReserved + 1,(StorageElement*)0);
						
					for(IDType i= 0; i < this->mReserved; ++i)
					{
						if (other.isID(other.mStorage[i]))
						{
							this->initAsID(this->mStorage[i],other.getAsID(other.mStorage[i]));
						}
						else
						{
							this->initAsObject(this->mStorage[i],other.getAsObject(other.mStorage[i]));
						}
					}

					return (*this);
				}

				//! \brief swap the elements with the other idvector
				void swap(IDVector& other)
				{
					SwapValues(this->mStorage,other.mStorage);
					SwapValues(mSize,other.mSize);
					SwapValues(this->mReserved,other.mReserved);
					SwapValues(mNextFreePos,other.mNextFreePos);
					SwapValues(mBeginIter.pos,other.mBeginIter.mPos);
					SwapValues(mEndIter.pos,other.mEndIter.mPos);
				}

				//! \brief get number of elements in the vector
				//! \returns number of elements
				inline IDType size() const
				{
					return mSize;
				}

				//! \brief get maximum id in O(n)
				//! \returns maximum id used in vector
				IDType getMaxID() const
				{
					if (this->mReserved == 0) return 0;

					IDType maxID= this->mReserved - 1;
					while (maxID > 0 && this->isID(this->mStorage[maxID]))
					{
						--maxID;
					}
					return maxID;
				}

				//! \brief checks if id is assigned in vector
				//! \param id	index to test
				//! \returns true if index is used and false if not
				bool isValidID(const IDType id) const
				{
					if (id >= this->mReserved) return false;
					return !this->isID(this->mStorage[id]);
				}

				//! \brief get begin iterator
				inline Iterator begin() const
				{
					return mBeginIter;
				}



				//! \brief get end iterator
				inline Iterator end() const
				{
					return mEndIter;
				}

				//! \brief get iterator for an element
				Iterator getIterator(const IDType id)
				{
					Iterator iter;
					iter.mPos= id;
					iter.mOwner= this;
					return iter;
				}

				//! Extend the internal array. This is useful if you now in advance how many items you add and you want
				//! to avoid often calls to resize. If the new size is smaller than the current, this will be ignored.
				//! \brief extend internal array
				//! \param size		new size of internal array, must be bigger than current internal size
				void extend(const uint32 size)
				{
					if (size <= this->mReserved) return;

					_resize(size);
				}

				//! clear the internal array (resets to size zero)
				void clear(bool deleteStorage= true)
				{
					// iterate over all elements and call destructor
					Iterator iter= begin();

					for(;iter != end(); ++iter)
					{
						Memory::CallDestructor(&(*iter));
					}

					if (deleteStorage)
					{
						Memory::Free(this->mStorage,mAllocator);
						this->mReserved= 0;
					}

					mSize= 0;
					mNextFreePos= 0;

					// init data with one element 
					// this element marks the end of the freelist and is always needed
					// so if you create a bigger array it must always be size + 1
					if (deleteStorage)
					{
						this->mStorage= Memory::Allocate(this->mAllocator,(StorageElement*)0);
					}
					// init freelist in the complete array
					for(IDType pos= 0; pos < this->mReserved; ++pos)
					{
						this->initAsID(this->mStorage[pos],pos + 1);
					}

					// init begin / end iterator
					mBeginIter.mPos= this->mReserved;
					mBeginIter.mOwner= this;

					mEndIter.mPos= this->mReserved;
					mEndIter.mOwner= this;
				}

				//! assign explicit a value to an id position. this could overwrite freelist elements and thus destroy the 
				//! internal freelist. But you can repair the freelist afterwards with a call to IDVector::repairFreeList()
				//! \brief assign a value to an id position
				//! \param id		id to assign (no bordertest)
				//! \param value	value to assign to this pos
				void assign(const IDType id, const T_OBJECT& value)
				{
					StorageElement& sElem= this->mStorage[id];
					if (this->isID(sElem))
					{
						this->initAsObject(sElem,value);
					}
					else
					{
						this->getAsObject(sElem)= value;
					}
				}

				//! \brief repair the freelist
				void repairFreeList()
				{
					// find first free item
					mSize= 0;
					uint32 i= 0;
					for(; i < this->mReserved; ++i) 
					{
						if (this->isID(this->mStorage[i])) 
						{
							break;
						}
						else
						{
							++mSize;
						}
					}
					mNextFreePos= i;

					// set begin iterator
					uint32 k= 0;
					for (; k < this->mReserved; ++k)
					{
						if (!this->isID(this->mStorage[k])) break;
					}
					mBeginIter.mPos= k;


					// fix the rest of the freelist
					uint32 lastFreeItem= i;
					++i;
					for(; i < this->mReserved; ++i) 
					{
						if (this->isID(this->mStorage[i])) 
						{
							this->initAsID(this->mStorage[lastFreeItem],i);
							lastFreeItem= i;
						}
						else
						{
							++mSize;
						}
					}

					// set list end (which points to the element after the last element (mReserverd))
					this->initAsID(this->mStorage[lastFreeItem],this->mReserved);
				}

				//! \brief Moves all elements to the front of the idvector
				//! \remark This invalidates existing id values
				//! \remark The order of the elements when iterating will not be stable
				void compact()
				{
					IDType readPos= this->mReserved;
					for(IDType writePos= 0; writePos < mSize; ++writePos)
					{
						StorageElement& writeElem= this->mStorage[writePos];
						if (this->isID(writeElem))
						{
							// find element from the back to insert in this id slot
							do 
							{
								--readPos;
							} while (this->isID(this->mStorage[readPos]));

							StorageElement& readElem= this->mStorage[readPos];
							this->initAsObject(writeElem,this->getAsObject(readElem));

							// destruct copied element
							Memory::CallDestructor(&this->getAsObject(readElem));

							// we repair the freelist later on
						}
					}

					// init freelist from mSize to mReserved
					for(IDType i= mSize; i < this->mReserved; ++i)
					{
						this->initAsID(this->mStorage[i],i + 1);
					}
					mNextFreePos= mSize;

					if (mSize > 0)
						mBeginIter.mPos= 0;
					else
						mBeginIter.mPos= this->mReserved;
				}

			private:
				Memory::Allocator::Ptr	mAllocator;		//!< memory allocator for additional memory
				IDType					mSize;			//!< number of data elements
				IDType					mNextFreePos;	//!< first free position

				Iterator				mBeginIter;		//!< begin iterator
				Iterator				mEndIter;		//!< end iterator, always points to the last possible dataelement (mStorage[mReserved])

				//! \brief reserve a free id 
				//! \returns the reserved id
				//! \remark Does not increment element count, does also not init the reserved
				//!			object.
				IDType _reserveID()
				{
					if (mNextFreePos == this->mReserved) {
						// no free elements in the list, resize the list
						// +1 is needed if you have an initial dataSize of 0
						_resize((IDType)(this->mReserved * IDVECTOR_RESIZE_SCALE) + 1);
					}
					// now we have new items and valid freePos

					// the freepos points at an element that is free in the data array.
					IDType id= mNextFreePos;

					// read the next free position from the dataobject, freePos points to
					mNextFreePos= this->getAsID(this->mStorage[mNextFreePos]);

					return id;
				}

				//! \brief free id
				//! \param id	id to free
				//! \returns true if this was a valid id
				bool _freeID(const IDType id)
				{
					if (!isValidID(id)) return false;

					this->initAsID(this->mStorage[id],mNextFreePos);
					mNextFreePos= id;

					return true;
				}


				//! \brief resize data array
				//! \param size		new size for the array
				//! \remark If the size is bigger than the current size, the freelist will be extended by all the new elements.
				//!			If the size is smaller, elements in that part of the array will be thrown away and freelist elements
				//!			will be removed from the freelist
				void _resize(const IDType size)
				{
					if (size == this->mReserved) return;	// nothing to resize

					const IDType copyUntil = Math::Min(size,this->mReserved);

					// create a new dataarray which is one bigger than size, since we
					// need the last element for endcondition
					StorageElement* newArray= Memory::AllocateArray<StorageElement>(mAllocator,size + 1);

					// copy content from old array to new array with memcopy. We can do this, since
					// old objects memory will be deleted without a call to the destructor
					memcpy(newArray,this->mStorage,sizeof(StorageElement) * copyUntil);

					// repair id system in the old array part
					for(IDType i= 0; i < copyUntil; ++i)
					{
						StorageElement& srcElem= this->mStorage[i];
						StorageElement& dstElem= newArray[i];
						if (this->isID(srcElem))
						{
							this->initAsID(dstElem,this->getAsID(srcElem));
						}
					}

					// delete old array
					Memory::Free(this->mStorage,mAllocator);
					this->mStorage= newArray;

					if (size > this->mReserved) 
					{
						// init freelist in new array part
						for(IDType pos= this->mReserved; pos < size; ++pos)
						{
							this->initAsID(this->mStorage[pos],pos + 1);
						}
					} 
					else 
					{
						// the array is smaller as before,
						// just repair the freelist
						this->repairFreeList();
					}

					// fix begin iterator (if begin iterator == end iterator)
					if (mBeginIter.mPos == this->mReserved)
					{
						mBeginIter.mPos= size;
					}
					// set end iterator to the datasize end
					mEndIter.mPos= size;

					// set new reserved end
					this->mReserved= size;

				}

				//! \brief get the next valid id
				//! \param id		next valid id (invalid if returns false)
				//! \param start	id to start from
				//! \returns false if there is no next id
				bool _getNextID(IDType& id, const IDType start) const
				{
					// increment id until a valid id or end of datablock reached
					id= start + 1;
					while (!isValidID(id) && id < this->mReserved) ++id;

					return isValidID(id);
				}

				void _destroy()
				{
					this->clear();

					// clear has build a new storage pointer with one element
					Memory::Free(this->mStorage,this->mAllocator);

					this->mStorage= NULL;
					this->mReserved= 0;
				}


			};	// class IDVector
		}	// namespace Container
	}	// namespace Common
}	// namespace XAIT
