// (C) xaitment GmbH 2006-2012

#pragma once



namespace XAIT
{
	namespace Common
	{
		namespace Container
		{
			template<class TYPE1_T, class TYPE2_T>
			struct Pair 
			{	
				TYPE1_T mFirst; //< first value
				TYPE2_T mSecond; //< second value

				//! Constructor
				Pair()
				{}

				//! Constructor
				//! \param Val1  first value
				//! \param Val2  second value
				Pair(const TYPE1_T& val1, const TYPE2_T& val2) : mFirst(val1), mSecond(val2)
				{}

				//! Constructor
				//! \param otherPair   another pair
				Pair(const Pair<TYPE1_T,TYPE2_T>& otherPair) : mFirst(otherPair.mFirst), mSecond(otherPair.mSecond)
				{}

				//! comparison operator

				inline bool operator==(const Pair<TYPE1_T,TYPE2_T>& otherPair) const
				{
					return (mFirst == otherPair.mFirst && mSecond == otherPair.mSecond);
				}

				//! \brief assignment operator
				//! \param otherPair   another pair
				void operator=(const Pair<TYPE1_T,TYPE2_T>& otherPair)
				{
					mFirst = otherPair.mFirst;
					mSecond = otherPair.mSecond;
				}

			};

			template<class FIRST_T, class SECOND_T>
			inline Pair<FIRST_T,SECOND_T> MakePair(const FIRST_T& first, const SECOND_T& second)
			{
				return Pair<FIRST_T,SECOND_T>(first,second);
			}
		}
	}
}
