// (C) xaitment GmbH 2006-2012

#pragma once 

#include <xait/common/FundamentalTypes.h>
#include <xait/common/debug/Assert.h>
#include <xait/common/SharedPtr.h>

namespace XAIT
{
	namespace Common
	{
		namespace Container
		{
			//! A class that emulates an C style array. The advantage of this array is, that you can also build static arrays
			//! from classes which do not have a default constructor by using the copy constructor.
			//! \brief a wrapper class for arrays of constant size
			template<typename T_OBJECT, int N_NUM_ELEM>
			class Array
			{
			public:
				//! \brief default constructor, initializing every element with its default constructor
				Array()
				{
					// call the default constructor on every element
					uint08* pos= mData;
					for(uint32 i= 0; i < N_NUM_ELEM; ++i)
					{
						new(pos) T_OBJECT();
						pos+= sizeof(T_OBJECT);
					}
				}

				//! \brief a constructor which does not initialize the elements
				//! \remark use init to initialise the elements
				Array(const Common::Helper::IgnoreInit&)
				{}

				//! \brief single copy constructor, initializing every element with a copy from one element
				Array(const T_OBJECT& copyFrom)
				{
					// call the copy constructor on every element with the same source
					uint08* pos= mData;
					for(uint32 i= 0; i < N_NUM_ELEM; ++i)
					{
						new(pos) T_OBJECT(copyFrom);
						pos+= sizeof(T_OBJECT);
					}
				}

				//! \brief copy constructor, copy the complete array
				Array(const Array& copyFrom)
				{
					// call the copy constructor on every element
					uint08* pos= mData;
					for(uint32 i= 0; i < N_NUM_ELEM; ++i)
					{
						new(pos) T_OBJECT(copyFrom[i]);
						pos+= sizeof(T_OBJECT);
					}
				}

				~Array()
				{
					// call destructor for every object
					uint08* pos= mData;
					for(uint32 i= 0; i < N_NUM_ELEM; ++i)
					{
						((T_OBJECT*)pos)->~T_OBJECT();
						pos+= sizeof(T_OBJECT);
					}
				}

				//! \brief element access
				inline T_OBJECT& operator[](const uint32 index)
				{
					X_ASSERT_MSG_DBG(index < N_NUM_ELEM,"index out of range");
					return *((T_OBJECT*)&mData[sizeof(T_OBJECT) * index]);
				}

				//! \brief element access
				inline const T_OBJECT& operator[](const uint32 index) const
				{
					X_ASSERT_MSG_DBG(index < N_NUM_ELEM,"index out of range");
					return *((const T_OBJECT*)&mData[sizeof(T_OBJECT) * index]);
				}

				//! \brief get the size of the array
				inline uint32 size() const
				{
					return N_NUM_ELEM;
				}

				//! \brief initialize an element (this is not an assign !!!)
				inline void init(const uint32 index, const T_OBJECT& copyFrom)
				{
					X_ASSERT_MSG_DBG(index < N_NUM_ELEM,"index out of range");
					new(mData + (sizeof(T_OBJECT) * index)) T_OBJECT(copyFrom);
				}

			private:
				uint08		mData[sizeof(T_OBJECT) * N_NUM_ELEM];		//!< array containing the data
			};
		}
	}
}
