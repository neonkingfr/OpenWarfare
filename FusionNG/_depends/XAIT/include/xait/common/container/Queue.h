// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/container/List.h>
#include <list>


namespace XAIT
{
	namespace Common
	{
		namespace Container
		{
			//! \brief class implementing an FIFO queue
			template<typename T>
			class Queue
			{
			public:
				typedef List<T>								ListType;
				typedef typename ListType::Iterator			Iterator; 
				typedef typename ListType::ConstIterator	ConstIterator; 

			private:
				ListType	mQueue;		//!< list container, used for this queue

			public:
				//! \brief constructor
				//! \param allocator	allocator used for new elements
				Queue(const Memory::Allocator::Ptr& allocator)
					: mQueue(allocator)
				{}

				//! \brief get the memory allocator for this vector
				inline Memory::Allocator::Ptr getAllocator() const
				{
					return mQueue.get_allocator().getMemoryAlloc();
				}

				//! \brief add an element at the end of the queue
				//! \param value	value to add
				inline void add(const T& value)
				{
					mQueue.pushBack(value);
				}

				//! \brief get the element at the front of the FIFO queue
				inline T& front()
				{
					return mQueue.front();
				}

				//! \brief get the element at the front of the FIFO queue
				inline const T& front() const
				{
					return mQueue.front();
				}

				//! \brief remove the element at the front of the FIFO queue
				inline void popFront()
				{
					mQueue.popFront();
				}

				//! \brief erase the element at the iterator position
				//! \returns an iterator to the next element in the queue (could be the end iterator if no further elements)
				inline Iterator erase(const Iterator& iter)
				{
					return mQueue.erase(iter);
				}

				//! \brief find an element in the queue
				inline Iterator find(const T& value)
				{
					return mQueue.find(value);
				}

				//! \brief find an element in the queue
				inline ConstIterator find(const T& value) const
				{
					return mQueue.find(value);
				}

				//! \brief get the begin iterator of the queue (starting at the front)
				inline Iterator begin()
				{

					return mQueue.begin();
				}

				//! \brief get the end iterator of the queue
				inline Iterator end()
				{
					return mQueue.end();
				}

				//! \brief get the begin iterator of the queue (starting at the front)
				inline ConstIterator begin() const
				{

					return mQueue.begin();
				}

				//! \brief get the end iterator of the queue
				inline ConstIterator end() const
				{
					return mQueue.end();
				}

				//! \brief test if the queue is empty
				//! \returns true if the queue is empty
				inline bool empty() const
				{
					return mQueue.empty();
				}

				//! \brief get the number of elements in the queue
				//! \returns the number of elements in the queue
				inline uint32 size() const 
				{
					return mQueue.size();
				}

				//! \brief removes all elements from the queue
				inline void clear()
				{
					mQueue.clear();
				}

		};

		}
	}

}

