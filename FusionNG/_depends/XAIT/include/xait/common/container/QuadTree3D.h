// (C) 2011 xaitment GmbH

#ifndef _QUAD_TREE_H_
#define _QUAD_TREE_H_

#include <xait/common/FundamentalTypes.h>

#include <xait/common/memory/Allocator.h>

#include <xait/common/math/MathDefines.h>
#include <xait/common/math/Vec2.h>
#include <xait/common/math/Vec3.h>

#include <xait/common/container/IDVector.h>
#include <xait/common/container/List.h>
#include <xait/common/container/Pair.h>
#include <xait/common/container/QuadTreeFilter3D.h>

#include <xait/common/debug/Debug.h>

#include <xait/common/exceptions/ArgumentException.h>
#include <xait/common/exceptions/InvalidStateException.h>
#include <xait/common/Platform.h>

namespace XAIT
{
	namespace Common
	{
		namespace Container
		{
			//! \brief				class implements a quadtree
			//! \param T_USERDATA	type of userdata in every node
			//! \param T_POINT		type of the components in every point (e.g. float32, float64, int32)
			template<typename T_USERDATA, typename T_POINT = float32>
			class QuadTree3d
			{
			public:
				//! \brief enum defines an invalid node id
				enum 
				{
					QuadNodeIDUndef = X_MAX_UINT32
				};

				typedef uint32 QuadNodeID;

			private: 
				//! \brief relation of two bounding boxes (needed by find methods)
				enum Relation
				{
					UNDEFINED = 0,
					NONE,
					OVERLAPS,
					CONTAINS,
				};

				//! \brief	a node of the quadtree
				struct QuadNode
				{
					//!\ brief constructor
					QuadNode(Memory::Allocator::Ptr mAllocator)
						: mNorthWest(QuadNodeIDUndef)
						, mNorthEast(QuadNodeIDUndef)
						, mSouthWest(QuadNodeIDUndef)
						, mSouthEast(QuadNodeIDUndef)
						, mParent(QuadNodeIDUndef)
						, mData(mAllocator)
					{};

					//! \brief				constructor
					//! \param parent		id of the parent node
					//! \param mAllocator	memory allocator
					QuadNode(QuadNodeID parent, Memory::Allocator::Ptr mAllocator)
						: mNorthWest(QuadNodeIDUndef)
						, mNorthEast(QuadNodeIDUndef)
						, mSouthWest(QuadNodeIDUndef)
						, mSouthEast(QuadNodeIDUndef)
						, mParent(parent)
						, mData(mAllocator)
					{};

					QuadNodeID mNorthWest; //!< id of the upper left child
					QuadNodeID mNorthEast; //!< id of the upper right child
					QuadNodeID mSouthWest; //!< id of the lower left child
					QuadNodeID mSouthEast; //!< id of the lower right child

					QuadNodeID mParent; //!< id of the parent node

					Math::Vec2<T_POINT> mMin; //!< bounding box upper left corner of the node
					Math::Vec2<T_POINT> mMax; //!< bounding box lower right corner of the node

					List<Pair<T_USERDATA,Math::Vec3<T_POINT> > > mData; //!< user data for this node
				};

				//! \brief helper class to sort data by distance to a given point
				struct compareByDistance
				{
					Math::Vec3<T_POINT> mPivot; //!< point to compute the distance to
					uint08				mUpComponent;

				public:
					//! \brief		constructor
					//! \param  a	point to compute the distance to
					compareByDistance(const Math::Vec3<T_POINT> &a,const uint08 upComponent)
						:mPivot(a),mUpComponent(upComponent)
					{};

					//! \brief operator()
					//! \param a	first point
					//! \param b	second point
					//! \returns	returns true if the distance from a to mPivot is smaller than the distance from b to mPivot; false otherwise
					bool operator () (const Pair<T_USERDATA,Math::Vec3<T_POINT> > &a,const Pair<T_USERDATA,Math::Vec3<T_POINT> > &b) const
					{
						return ( mPivot.reduceDimensions(mUpComponent).getDistance2(a.mSecond.reduceDimensions(mUpComponent)) 
							< mPivot.reduceDimensions(mUpComponent).getDistance2(b.mSecond.reduceDimensions(mUpComponent)));
					};

				};

				//! \brief		helper function to compare points by distance to given one
				//! \param a	point to compare others to
				//! \returns	\see compareByDistance
				compareByDistance makeCompareByDistance(const Math::Vec3<T_POINT> &a)
				{
					return compareByDistance(a,mUpComponent);
				} ;


			public:

				//! \brief				default constructor
				//! \param allocator	memory allocator
				//! \param upComponent	which component of Vec3 is up?
				//! \remarks			member variables must be set via int method
				QuadTree3d(const Memory::Allocator::Ptr& allocator,const uint32 upComponent)
					: mAllocator(allocator)
					, mSizeX( (T_POINT)0)
					, mSizeY( (T_POINT)0)
					, mMaxEntries(0)
					, mQuads(allocator)
					, mUpComponent(upComponent)
				{};

				//! \brief						constructor
				//!\ param minX the				minimal	coordinate of the quad in x direction
				//!\ param minY the				minimal	coordinate of the quad in y direction
				//!\ param maxX the				maximal	coordinate of the quad in x direction
				//!\ param maxY the				maximal	coordinate of the quad in y direction
				//!\ param maxEntriesPerQuad	if this number of entries for a node is exceeded, the node will be split
				//!\ param allocator			allocator pointer
				QuadTree3d(const T_POINT minX,const T_POINT minY, const T_POINT maxX,const T_POINT maxY, uint32 maxEntriesPerQuad, Memory::Allocator::Ptr allocator)
					: mMaxEntries(maxEntriesPerQuad)
					, mAllocator(allocator)
				{
					QuadNode* root = new(mAllocator->alloc(sizeof(QuadNode))) QuadNode();

					root->mMin = Math::Vec2f(minX,minY);
					root->mMax = Math::Vec2f(maxX,maxY);
					mRoot = mQuads.add(root);

					mSizeX = maxX-minX;
					mSizeY = maxY-minY;
				}

				//! \brief destructor
				~QuadTree3d()
				{
					clear();
					mQuads[mRoot]->~QuadNode();
					mAllocator->free(mQuads[mRoot]);
				}

				//! \brief						initializes the quad tree using 2D bounding box
				//!\ param min					the minimal coordinate of the quad
				//!\ param max					the maximal coordinate of the quad
				//!\ param maxEntriesPerQuad	if this number of entries for a node is exceeded, the node will be split
				//!\ param allocator			allocator pointer
				//! \remarks					must only be called if default constructor has been used to instance the class
				void init(const Math::Vec2<T_POINT> min,const Math::Vec2<T_POINT> max, uint32 maxEntriesPerQuad, Memory::Allocator::Ptr allocator)
				{
					mMaxEntries = maxEntriesPerQuad;
					mAllocator = allocator;

					QuadNode* root = new(mAllocator->alloc(sizeof(QuadNode))) QuadNode(mAllocator);

					root->mMin = min;
					root->mMax = max;
					mRoot = mQuads.add(root);

					mSizeX = max.mX - min.mX;
					mSizeY = max.mY - min.mY;
				}

				//! \brief						initializes the quad tree using 3D bounding box
				//!\ param min					the minimal coordinate of the quad
				//!\ param max					the maximal coordinate of the quad
				//!\ param maxEntriesPerQuad	if this number of entries for a node is exceeded, the node will be split
				//!\ param allocator			allocator pointer
				//! \remarks					must only be called if default constructor has been used to instance the class
				void init(const Math::Vec3<T_POINT> min,const Math::Vec3<T_POINT> max, uint32 maxEntriesPerQuad, Memory::Allocator::Ptr allocator)
				{
					init(min.reduceDimensions(mUpComponent),max.reduceDimensions(mUpComponent),maxEntriesPerQuad,allocator);
				}

				//! \brief			adds an entry to the quadtree
				//! \param point	location of the data
				//! \param data		user data linked to the point
				//! \returns		id of the node, the data has been entered
				inline QuadNodeID addPoint(const Math::Vec3<T_POINT>& point,const T_USERDATA data)
				{
					return (addPoint(mRoot,point,data));
				}

				//! \brief			removes the point with data from the quadtree
				//! \param point	point to remove
				//! \param data		data linked with this point
				//! \returns		false if the point/data pair couldn't be found, true otherwise
				bool remove(const Math::Vec3<T_POINT>&  point, const T_USERDATA data)
				{
					if(hasChildren(mRoot))
						return (removeData(mRoot,point,data));
					//else
					QuadNode* root = mQuads[mRoot];

					typename List<Pair<T_USERDATA,Math::Vec3<T_POINT> > >::Iterator iter 
						= root->mData.find(Pair<T_USERDATA,Math::Vec3<T_POINT> >(data,point));

					//tree empty?
					if(iter == root->mData.end())
					{
						XAIT_THROW_EXCEPTION(XAIT::Common::Exceptions::ArgumentException("data", "can't delete " + Common::BString(data) + " in root"));
						return false;
					}
					//else
					root->mData.erase(iter);
					return true;
				}

				//! \brief removes all entries (except the root node) from the quadtree
				void clear()
				{
					QuadNode* rootNode = NULL;
					mQuads.getElement(rootNode,mRoot);
					Math::Vec2f min = (rootNode)->mMin;
					Math::Vec2f max = (rootNode)->mMax;


					typename IDVector<QuadNode*>::Iterator iter = mQuads.begin();
					typename IDVector<QuadNode*>::Iterator end = mQuads.end();

					for(;iter != end; ++iter)
					{
						(*iter)->~QuadNode();
						mAllocator->free(*iter);
					}

					mQuads.clear();

					//add root again
					QuadNode* root = new(mAllocator->alloc(sizeof(QuadNode))) QuadNode(mAllocator);
					root->mMin = min;
					root->mMax = max;
					mRoot = mQuads.add(root);
				}

				//! \brief			move a point/data pair from orgPoint to newPoint
				//! \param orgPoint	original point 
				//! \param data		data linked to orgPoint
				//! \param newPoint	new position for the data
				//! \returns		true if the point/data pair couldbe moved, false otherwise
				bool movePoint(const Math::Vec3<T_POINT>& orgPoint, const T_USERDATA data, const Math::Vec3<T_POINT>& newPoint)
				{
					//new point outside the quadtree
					if(!isInside(mRoot,newPoint))
					{
						return false;
					}

					//find node corresponding to orgPoint
					QuadNodeID node=mRoot;

					while(hasChildren(node))
						node = findChild(node,orgPoint);

					if (node == X_MAX_UINT32)
 						return false;

					QuadNode* root = mQuads[node];

					//get iter to data
					typename List<Pair<T_USERDATA,Math::Vec3<T_POINT> > >::Iterator iter 
						= root->mData.find(Pair<T_USERDATA,Math::Vec3<T_POINT> >(data,orgPoint));

					//point not found
					if(iter == root->mData.end())
					{
						XAIT_FAIL( "point (" + Common::BString(orgPoint.mX) + ", " + Common::BString(orgPoint.mY) + ") not found");
						return false;
					}

					//is newPoint inside the quad of node?
					if(isInside(node,newPoint))
					{
						iter->mSecond = newPoint;
						return true;
					}
					//else 
					//remove data from orgPoint node
					root->mData.erase(iter);

					//try to delete the quad
					removeChilds(root->mParent);

					//TODO: make use of neighbourhood information and continousity
					//TODO (cont): i.e. it is very likely, that newPoint is in a node, that is a neighbour of orgPoint 
					//find node corresponding to newPoint
					QuadNodeID newNode = mRoot;

					while(hasChildren(newNode))
						newNode = findChild(newNode,newPoint);

					QuadNodeID addID = addPoint(newNode,newPoint,data);

					return (addID != QuadNodeIDUndef);

				}

				//! \brief			tests if there's data stored at point
				//! \param point	point to query
				//! \returns		returns true if there's a data element at point, false otherwise
				bool containsData(const Math::Vec3<T_POINT>& point)
				{
					QuadNodeID node = mRoot;

					while(hasChildren(node))
						node = findChild(node,point);

					if(node == QuadNodeIDUndef)
					{
						XAIT_THROW_EXCEPTION(Common::Exceptions::ArgumentException("point", "Can't find point (" + Common::BString(point.mX) + ", " + Common::BString(point.mY) + ")"));
						return false;
					}

					typename List<Pair<T_USERDATA,Math::Vec3<T_POINT> > >::Iterator iter = mQuads[node]->mData.begin();
					typename List<Pair<T_USERDATA,Math::Vec3<T_POINT> > >::Iterator end  = mQuads[node]->mData.end();

					for(;iter!= end; ++iter)
					{
						if(iter->mSecond == point)
							return true;
					}
					return false;
				}

				//! \brief			searches for data that is exactly located at point
				//! \param point	point to look for data
				//! \param data		data found at the point (if found) (output paramter)
				//! \returns		returns true if there's data at point, false otherwise
				bool findData(const Math::Vec3<T_POINT>& point, T_USERDATA& data)
				{
					QuadNodeID node = mRoot;

					while(hasChildren(node))
						node = findChild(node,point);

					if(node == QuadNodeIDUndef)
					{
						throw new Common::Exceptions::ArgumentException("point", "Can't find point (" + BString(point.mX) + ", " + BString(point.mY) + ")");
						return false;
					}

					typename List<Pair<T_USERDATA,Math::Vec3<T_POINT> > >::Iterator iter = mQuads[node]->mData.begin();
					typename List<Pair<T_USERDATA,Math::Vec3<T_POINT> > >::Iterator end  = mQuads[node]->mData.end();

					for(;iter!= end; ++iter)
					{
						if(iter->second == point)
						{
							data = iter->first;
							return true;
						}
					}
					return false;
				}

				//! \brief			finds all point/data pairs in a circle with radius around center
				//! \param points	point/data pairs found (output parameter)
				//! \param center	center of the circle to search in
				//! \param radius	radius of the circle to search in
				void doRangeSearch( List<Pair<T_USERDATA,Math::Vec3<T_POINT> > >& points, const Math::Vec3<T_POINT>& center,const T_POINT radius)
				{
					QuadNodeID node = mRoot;

					//find quad containing full BB of circle
					Math::Vec2<T_POINT> minBB = center.reduceDimensions(mUpComponent)-Math::Vec2<T_POINT>(radius,radius);
					Math::Vec2<T_POINT> maxBB = center.reduceDimensions(mUpComponent)+Math::Vec2<T_POINT>(radius,radius);

					while( hasChildren(node)&&isInside(node,minBB,maxBB)==CONTAINS)
						node = findChild(node,center);

					T_POINT sqRadius = radius*radius;

					if(node != mRoot)
					{
						QuadNode* root = mQuads[mQuads[node]->mParent];

						doRangeSearch(root->mNorthWest,minBB,maxBB, points,center,sqRadius);
						doRangeSearch(root->mNorthEast,minBB,maxBB, points,center,sqRadius);
						doRangeSearch(root->mSouthWest,minBB,maxBB, points,center,sqRadius);
						doRangeSearch(root->mSouthEast,minBB,maxBB, points,center,sqRadius);
					}
					else
					{
						doRangeSearch(node,minBB,maxBB, points,center, sqRadius);
					}


				}

				//! \brief				find up to maxNumber point/data pairs inside the circle with radius around center
				//! \param points		point/data pairs found (output parameter)
				//! \param center		center of the circle
				//! \param maxNumber	maximal number of point/data pairs that are returned
				//! \param radius		radius of the circle
				//! \returns			number of point/data pairs found
				uint32 doNearestNeighbourSearch(List<Pair<T_USERDATA,Math::Vec3<T_POINT> > >& points,const Math::Vec2<T_POINT>& center,const uint32 maxNumber,const T_POINT radius)
				{
					//TODO: this might not be the most efficent way, so if you want...you now what to do ;)

					List<Pair<T_USERDATA,Math::Vec3<T_POINT> > > hits;

					doRangeSearch(hits,center,radius);

					hits.sort(makeCompareByDistance(center));

					uint32 numHits = hits.size();

					numHits = (numHits<maxNumber)?(numHits):(maxNumber);

					numHits += points.size();

					points.insert(points.end(),hits.begin(),hits.end());

					points.resize(numHits);

					return numHits;
				}

				//! \brief			\see doNearestNeighbourSearch but applies a filter function to decide if the point/data pair will be returned
				//! \param filter	filter function \see QuadFilter
				uint32 doFilteredNearestNeighbourSearch(List<Pair<T_USERDATA,Math::Vec3<T_POINT> > >& points, const Math::Vec3<T_POINT>& center, const uint32 maxNumber, const T_POINT radius, QuadTreeFilter3d<T_USERDATA,T_POINT>* filter)
				{
					List<Pair<T_USERDATA,Math::Vec3<T_POINT> > > hits(mAllocator);

					doRangeSearch(hits,center,radius);

					hits.sort(makeCompareByDistance(center));

					typename List<Pair<T_USERDATA,Math::Vec3<T_POINT> > >::Iterator iter = hits.begin();
					typename List<Pair<T_USERDATA,Math::Vec3<T_POINT> > >::Iterator end = hits.end();

					uint32 num = 0;
					for(; iter != end && num<maxNumber;++iter)
					{
						if((*filter)(iter->mSecond,iter->mFirst))
						{
							++num;
							points.pushBack(*iter);
						}
					}

					return num;
				}

				//! \brief			returns all point/data pairs stored in node
				//! \param points	point/user data pairs stored in node (output paramter)
				//! \param node		id of the node
				inline void getUserData(List<Pair<T_USERDATA,Math::Vec3<T_POINT> > >& points,const QuadNodeID node) const
				{
					return (mQuads[node]->mData.mFirst);
				}

				//! \brief				returns the upper left and lower right corners of the node with id node
				//! \param upperLeft	upper left corner of the node (output paramter)
				//! \param lowerRight	lower right corner of the node (output paramter)
				//! \param node			id of the node
				void getQuad(Math::Vec2<T_POINT>& upperLeft, Math::Vec2<T_POINT>& lowerRight, const QuadNodeID node) const
				{
					upperLeft = mQuads[node]->mMin;
					lowerRight = mQuads[node]->mMax;
				}

				//! \brief		returns the center of the node
				//! \param node id of the node
				//! \returns	center of the node
				const Math::Vec2<T_POINT>& getPoint(const QuadNodeID node)
				{
					QuadNode* root = mQuads[node];
					return ( (root->mMax - root->mMin)/( (T_POINT)2) );
				}


				//! \brief			returns the id of the node containing point
				//! \param point	point to get the node id from
				//! \returns		id of the node 
				QuadNodeID getNodeID(const Math::Vec3<T_POINT>& point) const
				{
					QuadNodeID node = mRoot;

					while(hasChildren(node))
						node = findChild(node,point);

					return node;
				}


#ifdef XAIT_DEBUG
				//! \brief		draws the quadtree and its data
				//! \remarks	available only in debug mode
				inline void drawQuadTree()
				{
					drawQuadTree(mRoot);
				}

				//! \brief				recursive draw method for the quadtree
				//! \param node	node to start drawing from
				//! \remarks			available only in debug mode. usually you call \see drawQuadTree() to draw the complete tree
				void drawQuadTree(QuadNodeID node)
				{
					QuadNode* parent = mQuads[node];
					DLINE2D_SLD(parent->mMin, Vec2f(parent->mMax.mX,parent->mMin.mY));
					DLINE2D_SLD(Vec2f(parent->mMax.mX,parent->mMin.mY),parent->mMax);
					DLINE2D_SLD(parent->mMax,Vec2f(parent->mMin.mX,parent->mMax.mY));
					DLINE2D_SLD(Vec2f(parent->mMin.mX,parent->mMax.mY),parent->mMin);

					if(hasChildren(node))
					{
						X_ASSERT(parent->mData.size() == 0);

						drawQuadTree(parent->mNorthWest);
						drawQuadTree(parent->mNorthEast);
						drawQuadTree(parent->mSouthWest);
						drawQuadTree(parent->mSouthEast);
						return;
					}
					//else (must be a leaf -> visualize data)
					typename List<Pair<T_USERDATA,Math::Vec3<T_POINT> > >::Iterator iter = parent->mData.begin();
					typename List<Pair<T_USERDATA,Math::Vec3<T_POINT> > >::Iterator end  = parent->mData.end();

					for(;iter!=end; ++iter)
					{
						SETDCOLOR(1,0,0);
						DPOINT2D(iter->second);
						SETDCOLOR(0,0,0)
					}
				}
#endif

			private:

				Memory::Allocator::Ptr	mAllocator;		//!< allocator pointer
				T_POINT					mSizeX;			//!< size of the quadtree in x direction
				T_POINT					mSizeY;			//!< size of the quadtree in y direction
				uint32					mMaxEntries;	//!< if this number is exceeded in one node, the node will be split
				QuadNodeID				mRoot;			//!< id of the root node
				IDVector<QuadNode*>		mQuads;			//!< vector to store the nodes in
				uint08					mUpComponent;


				//! \brief		tests if the node as children
				//! \param node	id of the node to test
				//! \returns	bool true if the node exists and has children, false otherwise
				//! \remarks	returns also false if the node does not exists
				inline bool hasChildren(const QuadNodeID node) const
				{
					//all nodes or no node has children ->test one
					return (node != QuadNodeIDUndef && mQuads[node]->mNorthWest != QuadNodeIDUndef);
				}

				//! \brief		tests if node could store more data, or if it has to be split up
				//! \param node	id of the node
				//! \returns	returns true if the node has space to store at last one more point/data pair; false otherwise
				inline bool hasSpace(const QuadNodeID node) const
				{
					return (node != QuadNodeIDUndef && mQuads[node]->mData.size()<mMaxEntries);
				}

				//! \brief			tests if the point is inside the node
				//! \param node		id of the node
				//! \param point	point to test
				//! \returns		returns true if the point is inside to node
				inline bool isInside(const QuadNodeID node,const Math::Vec3<T_POINT>& point) const 
				{
					Math::Vec2<T_POINT> point2d = point.reduceDimensions(mUpComponent);
					QuadNode* root = mQuads[node];
					return (  (root->mMin.mX <= point2d.mX && point2d.mX < root->mMax.mX)
						&&( root->mMin.mY < point2d.mY && point2d.mY <= root->mMax.mY));
				}

				//! \brief		determines the relative position of the nodes bounding box and the bounding box specified by min and max
				//! \param node	id of the node
				//! \param min	upper left corner of the bounding box
				//! \param max	lower right corner of the bounding box
				//! \returns	Relation of the two boxes ( \see Relation)
				inline Relation isInside(const QuadNodeID node,const Math::Vec3<T_POINT>& min,const Math::Vec3<T_POINT>& max) const 
				{
					Math::Vec2<T_POINT> min2d = min.reduceDimensions(mUpComponent);
					Math::Vec2<T_POINT> max2d = max.reduceDimensions(mUpComponent);
					QuadNode* root = mQuads[node];


					if( min2d.mX >= root->mMin.mX && min2d.mY > root->mMin.mY && max2d.mX < root->mMax.mX && max2d.mY <= root->mMax.mY)
						return CONTAINS;
					//else
					if (root->mMax.mY <= min2d.mY || root->mMin.mY > max2d.mY || root->mMax.mX <= min2d.mX || root->mMin.mX > max2d.mX)
						return NONE;
					//else
					return OVERLAPS;
				}

				//! \brief		determines the relative position of the nodes bounding box and the bounding box specified by min and max
				//! \param node	id of the node
				//! \param min	upper left corner of the bounding box
				//! \param max	lower right corner of the bounding box
				//! \returns	Relation of the two boxes ( \see Relation)
				inline Relation isInside(const QuadNodeID node,const Math::Vec2<T_POINT>& min2d,const Math::Vec2<T_POINT>& max2d) const 
				{

					QuadNode* root = mQuads[node];


					if( min2d.mX >= root->mMin.mX && min2d.mY > root->mMin.mY && max2d.mX < root->mMax.mX && max2d.mY <= root->mMax.mY)
						return CONTAINS;
					//else
					if (root->mMax.mY <= min2d.mY || root->mMin.mY > max2d.mY || root->mMax.mX <= min2d.mX || root->mMin.mX > max2d.mX)
						return NONE;
					//else
					return OVERLAPS;
				}

				//! \brief			finds the child of node parent that contains the position point
				//! \param parent	id of the node
				//! \param point	point to find the child for
				//! \returns		id of the child that contains the point or QuadNodeIDUndef if no child could be found
				//! \remarks		finds only the direct child (no recursion)
				QuadNodeID findChild(QuadNodeID parent,const Math::Vec3<T_POINT> point) const
				{
					Math::Vec2<T_POINT> point2d = point.reduceDimensions(mUpComponent);
					QuadNodeID child = QuadNodeIDUndef;
					QuadNode* root = mQuads[parent];

					//point outside root?
					if(!isInside(parent,point))
						return child;

					//find child to recurse in
					Math::Vec2<T_POINT> size = (root->mMax + root->mMin)/( (T_POINT)2);

					//northern quads?
					if(point2d.mY > size.mY)
					{
						// western quad
						if(point2d.mX < size.mX)
							child = root->mNorthWest;
						else //eastern quad
							child = root->mNorthEast;
					}
					else //southern quads
					{
						// western quad
						if(point2d.mX < size.mX)
							child = root->mSouthWest;
						else //eastern quad
							child = root->mSouthEast;
					}

					return child;
				}

				//! \brief			adds children to node parent
				//! \param parent	id of the node to add children to
				//! \remarks		do not call this method if parent has allready children (old children will be overwritten and cause a memorey leak)
				void addChildren(QuadNodeID parent)
				{
					QuadNode* root = mQuads[parent];
					Math::Vec2<T_POINT> halfSize = (root->mMax - root->mMin)/( (T_POINT)2);

					//sw child
					QuadNode* node = new QuadNode(parent, mAllocator);
					node->mMin = root->mMin;
					node->mMax = root->mMin+ halfSize;
					root->mSouthWest = mQuads.add(node);

					//se child
					node = new QuadNode(parent, mAllocator);
					node->mMin = Math::Vec2<T_POINT>(root->mMin.mX+halfSize.mX,root->mMin.mY);
					node->mMax = Math::Vec2<T_POINT>(root->mMax.mX,root->mMin.mY+halfSize.mY);
					root->mSouthEast = mQuads.add(node);

					//nw child
					node = new QuadNode(parent, mAllocator);
					node->mMin = Math::Vec2<T_POINT>(root->mMin.mX,root->mMin.mY+halfSize.mY);
					node->mMax = Math::Vec2<T_POINT>(root->mMin.mX+halfSize.mX,root->mMax.mY);
					root->mNorthWest = mQuads.add(node);

					//ne child
					node = new QuadNode(parent, mAllocator);
					node->mMin = Math::Vec2<T_POINT>(root->mMin.mX+halfSize.mX,root->mMin.mY+halfSize.mY);
					node->mMax = root->mMax;
					root->mNorthEast = mQuads.add(node);

					typename List<Pair<T_USERDATA,Math::Vec3<T_POINT> > >::Iterator iter = root->mData.begin();
					typename List<Pair<T_USERDATA,Math::Vec3<T_POINT> > >::Iterator end = root->mData.end();

					for(;iter != end; ++iter)
					{

						QuadNodeID nodeID = findChild(parent,iter->mSecond);
						X_ASSERT_MSG(nodeID != QuadNodeIDUndef, "internal error. No child found to add data to");
						mQuads[nodeID]->mData.pushBack(*iter);
					}

					root->mData.clear();
				}

				//! \brief			traverses recursively the tree and adds the point/data pair
				//! \param node		id of the current node
				//! \param point	point to add
				//! \param data		data to add
				//! \returns		returns the id of the node, the point/data pair was added to
				inline QuadNodeID addPoint(QuadNodeID node,const Math::Vec3<T_POINT>& point,const T_USERDATA data)
				{
					if (!isInside(node,point))
						return QuadNodeIDUndef;

					if(!hasChildren(node))
					{
						//is there space left or allready data at this point?
						if(hasSpace(node)||containsData(point))
						{
							mQuads[node]->mData.pushBack(Pair<T_USERDATA,Math::Vec3<T_POINT> >(data,point));
							return  node;
						}
						//else
						addChildren(node);
					}
					//else
					const QuadNodeID linkerBug = addPoint(findChild(node,point),point,data); //internal compiler error when placed in return
					return (linkerBug);
				}

				//! \brief			traverses recursively the tree and deletes the point/data pair
				//! \param node		id of the current node
				//! \param point	point to add
				//! \param data		data to add
				//! \returns		returns true if the point/data pair could be removed, false otherwise
				bool removeData(QuadNodeID node, const Math::Vec3<T_POINT>&  point, const T_USERDATA data)
				{
					QuadNodeID child = findChild(node,point);

					//recurse if no leaf node
					if( hasChildren(child))
					{
						return(removeData(child,point,data));
					}
					//else -> remove data

					QuadNode* root = mQuads[child];

					//find data to remove (must be in curent quad)
					typename List<Pair<T_USERDATA,Math::Vec3<T_POINT> > >::Iterator iter 
						= root->mData.find(Pair<T_USERDATA,Math::Vec3<T_POINT> >(data,point));

					if(iter == root->mData.end())
					{
						XAIT_THROW_EXCEPTION(Common::Exceptions::InvalidStateException("error on deleting data in node " + Common::BString(child)));
						return false;
					}
					//else
					root->mData.erase(iter);

					//try to delete the quad
					removeChilds(root->mParent);

					return true;
				}

				//! \brief		removes recursively nodes that contain too few data
				//! \param node	id of the node
				//! \remarks	secure to call for non empty nodes
				void removeChilds(QuadNodeID node)
				{
					QuadNode* parent =  mQuads[node];

					//remove only if childs are leafs and number of data stored is smaler than maxEntries
					if(    !hasChildren(parent->mNorthWest)
						&& !hasChildren(parent->mNorthEast)
						&& !hasChildren(parent->mSouthWest)
						&& !hasChildren(parent->mSouthEast)
						&& (  mQuads[parent->mNorthWest]->mData.size()
						+ mQuads[parent->mNorthEast]->mData.size()
						+ mQuads[parent->mSouthWest]->mData.size()
						+ mQuads[parent->mSouthEast]->mData.size()) <= mMaxEntries)
					{
						//move data from childs to parent (childs will be deleted)
						QuadNode* child = mQuads[parent->mNorthWest];
						typename List<Pair<T_USERDATA,Math::Vec3<T_POINT> > >::Iterator iter = child->mData.begin();
						typename List<Pair<T_USERDATA,Math::Vec3<T_POINT> > >::Iterator end = child->mData.end();
						for(;iter != end; ++iter)
						{
							parent->mData.pushBack(*iter);
						}
						child->~QuadNode();
						mAllocator->free(child);

						child = mQuads[parent->mNorthEast];
						iter = child->mData.begin();
						end = child->mData.end();
						for(;iter != end; ++iter)
						{
							parent->mData.pushBack(*iter);
						}
						child->~QuadNode();
						mAllocator->free(child);

						child = mQuads[parent->mSouthWest];
						iter = child->mData.begin();
						end = child->mData.end();
						for(;iter != end; ++iter)
						{
							parent->mData.pushBack(*iter);
						}
						child->~QuadNode();
						mAllocator->free(child);

						child = mQuads[parent->mSouthEast];
						iter = child->mData.begin();
						end = child->mData.end();
						for(;iter != end; ++iter)
						{
							parent->mData.pushBack(*iter);
						}
						child->~QuadNode();
						mAllocator->free(child);

						//remove childs
						mQuads.remove(parent->mNorthWest);
						mQuads.remove(parent->mNorthEast);
						mQuads.remove(parent->mSouthWest);
						mQuads.remove(parent->mSouthEast);

						parent->mNorthWest = QuadNodeIDUndef;
						parent->mNorthEast = QuadNodeIDUndef;
						parent->mSouthWest = QuadNodeIDUndef;
						parent->mSouthEast = QuadNodeIDUndef;

						//try to recurse
						if(parent->mParent != QuadNodeIDUndef)
							removeChilds(parent->mParent);
					}
				}

				//! \brief			recursively range search method
				//! \param node		id of the node
				//! \param minBB	upper left corner of the bounding box
				//! \param maxBB	lower right corner of the bounding box
				//! \param points	found points (in-/output paramter)
				//! \param center	center of the circle
				//! \param sqRadius	squared radius of the circle
				void doRangeSearch(QuadNodeID node,Math::Vec2<T_POINT>& minBB,Math::Vec2<T_POINT>& maxBB, List<Pair<T_USERDATA,Math::Vec3<T_POINT> > >& points, const Math::Vec3<T_POINT>& center,const T_POINT sqRadius)
				{
					QuadNode* root=mQuads[node];

					if(hasChildren(node))
					{
						if(isInside(root->mNorthWest,minBB,maxBB) != NONE)
							doRangeSearch(root->mNorthWest,minBB,maxBB,points,center,sqRadius);
						if(isInside(root->mNorthEast,minBB,maxBB) != NONE)
							doRangeSearch(root->mNorthEast,minBB,maxBB,points,center,sqRadius);
						if(isInside(root->mSouthWest,minBB,maxBB) != NONE)
							doRangeSearch(root->mSouthWest,minBB,maxBB,points,center,sqRadius);
						if(isInside(root->mSouthEast,minBB,maxBB) != NONE)
							doRangeSearch(root->mSouthEast,minBB,maxBB,points,center,sqRadius);
					}
					//we are at a leaf -> test if data is in circle
					typename List<Pair<T_USERDATA,Math::Vec3<T_POINT> > >::Iterator iter = root->mData.begin();
					typename List<Pair<T_USERDATA,Math::Vec3<T_POINT> > >::Iterator end = root->mData.end();

					for(;iter!=end;++iter)
					{
						if( center.getDistance2(iter->mSecond) <= sqRadius)
							points.pushBack(*iter);
					}
				}

			};
		}
	}
}

#endif

