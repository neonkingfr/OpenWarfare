// (C) 2011 xaitment GmbH

#pragma once

#include <xait/common/FundamentalTypes.h>
#include <xait/common/math/Vec2.h>

namespace XAIT
{

	namespace Common
	{
		namespace Container
		{
			//! \brief QuadTreeFilter base class ( \see QuadTree)
			//! \param T_USERDATA type of userdata in every node
			//! \param T_POINT type of the components in every point (e.g. float32, float64, int32)
			template<typename T_USERDATA, typename T_POINT = float32>
			class QuadTreeFilter
			{
			public:

				QuadTreeFilter()
				{
				}

				virtual ~QuadTreeFilter()
				{
				}

				virtual bool operator()(const Common::Math::Vec2<T_POINT>, const T_USERDATA) = 0;

			}; 
		}
	}
}