// (C) xaitment GmbH 2006-2012

#pragma once
#ifndef _XAIT_COMMON_CONTAINER_LOOKUP_H_
#define _XAIT_COMMON_CONTAINER_LOOKUP_H_

#include<map>
#include<string>
#include<string.h>
#include <xait/common/FundamentalTypes.h>
#include <xait/common/memory/STLAllocator.h>
#include <xait/common/container/Vector.h>


namespace XAIT
{
	namespace Common
	{
		namespace Container
		{
			struct StringComp
			{
				bool operator()(const char* c1, const char* c2) const
				{
					return strcmp(c1,c2) < 0;
				}
			};

			template<typename KEY_T,typename VALUE_T, typename KEY_COMP>
			class LookUp;

			template<typename T>
			class List;

			template<typename T>
			class Queue;

			template<typename T, typename T_ALLOC>
			class Vector;

			namespace Helper
			{
				template<typename T>
				class ConstructorEval
				{
				public:
					enum { NEED_ALLOCATOR_INIT = 0};
				};

				template<typename T_INNER>
				class ConstructorEval<Vector<T_INNER> >
				{
				public:
					enum { NEED_ALLOCATOR_INIT = 1};
				};


				template<typename T_INNER>
				class ConstructorEval<List<T_INNER> >
				{
				public:
					enum { NEED_ALLOCATOR_INIT = 1};
				};

				template<typename T_INNER>
				class ConstructorEval<Queue<T_INNER> >
				{
				public:
					enum { NEED_ALLOCATOR_INIT = 1};
				};

				template<typename T_KEY, typename T_VALUE, typename T_COMP>
				class ConstructorEval<LookUp<T_KEY,T_VALUE,T_COMP> >
				{
				public:
					enum { NEED_ALLOCATOR_INIT = 1};
				};

				template<int N_COND>
				class ConditionalLookUpCompile
				{
				public:
					template<typename KEY_T,typename VALUE_T, typename KEY_COMP>
					static inline VALUE_T& getValue(LookUp<KEY_T,VALUE_T,KEY_COMP>& inst, const KEY_T& key)
					{
						return inst._elementOperatorFixedAlloc(key);
					}
				};

				template<>
				class ConditionalLookUpCompile<0>
				{
				public:
					template<typename KEY_T,typename VALUE_T, typename KEY_COMP>
					static inline VALUE_T& getValue(LookUp<KEY_T,VALUE_T,KEY_COMP>& inst, const KEY_T& key)
					{
						return inst._elementOperatorNormal(key);
					}
				};


			}


			template<typename KEY_T,typename VALUE_T, typename KEY_COMP = ::std::less<KEY_T> >
			class LookUp
			{
			public:
				typedef ::std::map<KEY_T,VALUE_T,KEY_COMP,Memory::STLAllocator<std::pair<KEY_T,VALUE_T> > >	MapType;
				typedef typename MapType::iterator														Iterator;
				typedef typename MapType::const_iterator												ConstIterator;

			private:
				MapType		mTable;

			public:
				//! \brief constructor for lookup
				//! \param allocator	allocator used for new elements in the lookup table
				LookUp(const Memory::Allocator::Ptr& allocator)
					: mTable(KEY_COMP(),(Memory::STLAllocator<std::pair<KEY_T,VALUE_T> >(allocator)))
				{}

				//! \brief get the memory allocator for this vector
				inline Memory::Allocator::Ptr getAllocator() const
				{
					// do not return reference since get_allocator() returns a temporary object
					return mTable.get_allocator().getMemoryAlloc();
				}

				//! \brief find an element to a key
				//! \param key	key to search for
				//! \returns Iterator pointing to the element with the search key, or end iterator if nothing found
				inline Iterator find(const KEY_T& key)
				{
					return mTable.find(key);
				}

				//! \brief find an element to a key
				//! \param key	key to search for
				//! \returns Iterator pointing to the element with the search key, or end iterator if nothing found
				inline ConstIterator find(const KEY_T& key) const
				{
					return mTable.find(key);
				}

				//! \brief test if lookup is empty
				//! \returns true if empty, false if not.
				inline bool empty() const 
				{
					return mTable.empty();
				}

				//! \brief get a value for a key
				//! \param key			key to search for
				//! \param value[out]	value belonging to the key 
				//! \returns true if the key has been found or false if not.
				bool get(const KEY_T& key, VALUE_T& value) const
				{
					ConstIterator i = mTable.find(key);
					if(i != mTable.end())
					{
						value = i->second;
						return true;
					}
					return false;
				}

				//! \brief tests if a key exists
				//! \param key	key to test for
				//! \returns true if the key exists
				bool exists(const KEY_T& key) const
				{
					ConstIterator i = mTable.find(key);
					if(i == mTable.end())
						return false;
					return true;
				}

				//! \brief get the begin iterator
				inline Iterator begin()
				{
					return mTable.begin();
				}

				//! \brief get the end iterator
				inline Iterator end()
				{
					return mTable.end();
				}

				//! \brief get the begin iterator
				inline ConstIterator begin() const
				{
					return mTable.begin();
				}

				//! \brief get the end iterator
				inline ConstIterator end() const
				{
					return mTable.end();
				}

				//! \brief get the number of elements in the lookup
				//! \returns the number of elements in the lookup
				inline uint32 size() const
				{
					return (uint32)mTable.size();
				}

				//! \brief add an element for a specific key
				//! \param key		key where to add the element
				//! \param value	value to insert at the key position
				//! \remark if there is already a value with the specified key, this value will NOT be
				//!			overwritten.
				inline Iterator add(const KEY_T& key, const VALUE_T& value)
				{

					return mTable.insert(std::make_pair(key,value)).first;
				}

				//! \brief remove an element with a specific key
				//! \param key	key to the element which should be removed
				//! \remark Does not produce an error if the element specified by key does not exist.
				inline void remove(const KEY_T& key)
				{
					mTable.erase(key);
				}

				
				//! \brief remove an element at the position of an iterator
				//! \param where	position where we should remove an element
				//! \returns iterator to the next element in the iterator listing
				inline void remove(Iterator where)
				{
					mTable.erase(where);
				}

				//! \brief remove all elements from the lookup
				inline void clear()
				{
					mTable.clear();
				}

				//! \brief swap the content of two lookups
				inline void swap(LookUp& right)
				{
					mTable.swap(right.mTable);
				}


				//! \brief access an element at the position of a key
				//! \remark This function cannot be const, since it could introduce a new element
				//!			which depends on the runtime state of the LookUp.
				inline VALUE_T& operator[](const KEY_T& key)
				{
					return Container::Helper::ConditionalLookUpCompile<Container::Helper::ConstructorEval<VALUE_T>::NEED_ALLOCATOR_INIT>::getValue(*this,key);
				}

			private:
				template<int N_COND>
				friend class Container::Helper::ConditionalLookUpCompile;

				inline VALUE_T& _elementOperatorNormal(const KEY_T& key)
				{
					return mTable[key];
				}

				inline VALUE_T& _elementOperatorFixedAlloc(const KEY_T& key)
				{
					Iterator iter= mTable.find(key);
					if (iter == mTable.end())
					{
						// insert a new element and use the memory allocator of the embedded map type
						iter= mTable.insert(iter,std::make_pair(key,VALUE_T(this->getAllocator())));
					}
					return iter->second;
				}
			};
		}
	}
}
#endif

