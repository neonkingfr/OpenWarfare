// (C) 2011 xaitment GmbH

#pragma once

#include <xait/common/FundamentalTypes.h>
#include <xait/common/math/Vec2.h>

namespace XAIT
{

	namespace Common
	{
		namespace Container
		{
			//! \brief QuadTreeFilter base class ( \see QuadTree)
			//! \param T_USERDATA type of userdata in every node
			//! \param T_POINT type of the components in every point (e.g. float32, float64, int32)
			template<typename T_USERDATA, typename T_POINT = float32>
			class QuadTreeFilter3d
			{
			public:

				QuadTreeFilter3d()
				{
				}

				virtual ~QuadTreeFilter3d()
				{
				}

				virtual bool operator()(const Common::Math::Vec3<T_POINT>, const T_USERDATA) = 0;

			}; 
		}
	}
}


