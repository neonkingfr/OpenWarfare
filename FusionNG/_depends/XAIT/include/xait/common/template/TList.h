// (C) xaitment GmbH 2006-2012

#pragma once 


namespace XAIT
{
	namespace Common
	{
		namespace Template
		{
			// This namespace contains a construct of classes that can build a list
			// of types. This list can be processed with a foreach construct.
			namespace TList
			{
				template<typename T_ELEMENT, typename T_PREV>
				class Element
				{
				public:
					enum { ELEMENT_NUMBER = T_PREV::ELEMENT_NUMBER + 1};

					typedef T_ELEMENT	ElementType;
					typedef T_PREV		Prev;
				};

				template<typename T_ELEMENT>
				class ElementStart
				{
				public:
					enum { ELEMENT_NUMBER = 0 };

					typedef T_ELEMENT	ElementType;
				};

				//! \brief foreach runs an operator on every element of the TList
				//! \param T_LIST		The list "instance"
				//! \param T_OPERATOR	The class which will be called with every list element
				//!						There must be a template operator() with the following parameters
				//!						int ELEMENT_NUMBER		// gives the number of the element
				//!						T_ELEMENT* dummy		// dummy parameter to give the type to the methode
				template<typename T_LIST, typename T_OPERATOR>
				class Foreach;

				template<typename T_ELEMENT, typename T_NEXT,typename T_OPERATOR>
				class Foreach<Element<T_ELEMENT,T_NEXT>,T_OPERATOR>
				{
					typedef Element<T_ELEMENT,T_NEXT>	ListElement;

				public:
					static inline void iterateForward(T_OPERATOR& op)
					{
						// goto prev element, till start, since we want to iterate forward
						Foreach<typename ListElement::Prev,T_OPERATOR>::iterateForward(op);

						// body do
						op(ListElement::ELEMENT_NUMBER,(ListElement::ElementType*)NULL);

					}
					static inline void iterateBackward(T_OPERATOR& op)
					{
						op(ListElement::ELEMENT_NUMBER,(ListElement::ElementType*)NULL);

						// goto prev element, we started at the end of the list
						Foreach<typename ListElement::Prev,T_OPERATOR>::iterateBackward(op);
					}
				};

				template<typename T_ELEMENT, typename T_OPERATOR>
				class Foreach<ElementStart<T_ELEMENT>,T_OPERATOR>
				{
					typedef ElementStart<T_ELEMENT>	ListElement;

				public:
					static inline void iterateForward(T_OPERATOR& op)
					{
						op(ListElement::ELEMENT_NUMBER,(typename ListElement::ElementType*)NULL);
					}

					static inline void iterateBackward(T_OPERATOR& op)
					{
						op(ListElement::ELEMENT_NUMBER,(typename ListElement::ElementType*)NULL);
					}
				};

			}
		}
	}
}