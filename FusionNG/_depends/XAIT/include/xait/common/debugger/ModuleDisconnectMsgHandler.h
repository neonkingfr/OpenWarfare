// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/MinimalPrerequisites.h>
#include <xait/common/debugger/Server.h>
#include <xait/common/network/message/MessageHandler.h>

namespace XAIT
{
	namespace Common
	{
		namespace Debugger
		{
			//! \brief Message handler handling MESSAGE_MODULE_DISCONNECT.
			//!
			//! Called when the client tells the server that it wants to disconnect from a module.
			//! The message handler has to be registered with the server.
			//! \see Server
			class ModuleDisconnectMsgHandler : public Network::Message::MessageHandler
			{
			public:
				//! \brief Constructor.
				//! \param server	the server allows disconnection between client and module
				ModuleDisconnectMsgHandler( Server* server );

				//! \brief Handles the message by reading the data through the MessageReader.
				//! \param msgReader	the reader containing the message
				//! \param connectionID		the identifier of the client the message is from
				//! \return true, if the message was handled correctly, and false otherwise
				bool handleMessage(Network::Message::MessageReader* msgReader, const uint32 connectionID );

			protected:

			private:
				Server*		mServer; //!< the server
			};

		}
	}
}
