// (C) xaitment GmbH 2006-2012

#pragma once 

#include <xait/common/io/Stream.h>
#include <xait/common/debugger/Server.h>

namespace XAIT
{
	namespace Common
	{
		namespace Debugger
		{
			//! \brief Message handler handling MESSAGE_NETWORKSTREAM_REGISTRATION.
			//!
			//! The message will be sent if a new NetworkStream has been registered from client to server.
			//! Has to be registered with the server.
			//! \see Server
			class NetworkStreamRegisterMsgHandler : public Network::Message::MessageHandler
			{
#define MSGHANDLER_NETWORKSTREAM_REGISTER_PARAMETER	"registerNetworkStreamMessage: { String:networkStreamName, uint32:networkStreamID, uint32:networkStreamLength }"

			public:
				//! \brief Constructor
				//! \param server	the server allows access to the registered modules
				NetworkStreamRegisterMsgHandler( Server* server );

				//! \brief Destructor
				virtual ~NetworkStreamRegisterMsgHandler();

				//! \brief handles the message by reading the data through the MessageReader
				//! \param msgReader	the reader containing the message
				//! \param connectionID		the identifier of the client the message is from
				//! \returns true, if message was handled correctly, else false
				bool handleMessage(Network::Message::MessageReader* msgReader, const uint32 connectionID );

			protected:

			private:
				//! \brief Reads the message parameters from the stream.
				//! \param msgReader the message reader
				bool getParameters(Network::Message::MessageReader* msgReader);

				//! \brief Resets the internal variables.
				void reinitVariables();

				//! \brief Sends the reply to the given connection ID.
				//! \param connectionID the connection ID
				//! \return true, if sending the reply was successful, and false otherwise
				bool reportReply( const uint32 connectionID );


				Server*		mServer;				//!< the server
				uint32		mNetworkStreamID;		//!< the network stream ID
				String		mNetworkStreamName;		//!< the network stream name
				uint32		mNetworkStreamLength;	//!< the network stream length

				uint08		mResultCode;			// TODO: Remove (see base class)!
				String		mErrorText;				// TODO: Remove!
			};
		}
	}
}
