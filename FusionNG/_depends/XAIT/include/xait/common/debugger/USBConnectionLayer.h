// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/MinimalPrerequisites.h>
#include <xait/common/debugger/ConnectionLayer.h>
#include <xait/common/container/Queue.h>
#include <xait/common/threading/Semaphore.h>
#include <xait/common/platform/Time.h>

#include "Hio2If.h"

#define XAIT_USBCONNLAYER_BUFFERSIZE		4096
#define XAIT_USBCONNLAYER_PC2NNGC_ADDR		0x1000
#define XAIT_USBCONNLAYER_NNGC2PC_ADDR		0x0000
#define XAIT_USBCONNLAYER_PINGTIMEOUT_MS	2000



namespace XAIT
{
	namespace Common
	{
		namespace Debugger
		{
			//! \brief This class describes the USB connection layer.
			//!
			//! The USB connection layer handles low level USB connection operations. It is used by the Server.
			//! \see Network::Server
			class USBConnectionLayer : public ConnectionLayer
			{
			public:
				//! \brief Constructor.
				//! \param server the server
				USBConnectionLayer(Server* server);

				//! \brief Destructor.
				~USBConnectionLayer();

				//! \brief Establishes a TCP connection to the given address.
				//! \param address	the address
				bool connect( CONNECTION_LAYER_ADDRESS address );

				//! \brief Disconnects all connections.
				void disconnect();

				//! \brief Adds a message stream that should be send over the given socket.
				//! \param connectionID		connection ID of the receiver
				//! \param messageStream	the data to send
				//! \returns true, if the socket exists and a OutputHandler was found, and false otherwise
				bool sendMessage( const uint32 connectionID, IO::Stream* messageStream );

				//! \brief Removes the connection with the given connection ID.
				//! \param connectionID		identifier to match the client
				//! \returns true, if removing the connection was successful, and false otherwise
				bool removeConnection( const uint32 connectionID );

				//! \brief Updates the internal listeners and senders.
				//! \returns 0, if updating was successful, and -1 if updating was not successful
				int32 onTick();

				void releaseOnTick();

			private:
				enum PacketType
				{
					PT_PING= 1,
					PT_DATA= 2,
				};

				HIO2IF_ID						mHioID;
				Threading::Semaphore			mProcessMainSignal;
				bool							mClientConnected;
				volatile bool					mDataReceived;
				volatile bool					mSendPossible;
				volatile bool					mConnectRequest;
				volatile bool					mDisconnectRequest;

				uint08							mTransmitBuffer[XAIT_USBCONNLAYER_BUFFERSIZE] ATTR_ALIGN(32);
				
				bool							mSendPing;
				Platform::MilliSeconds			mLastPingReceived;

				Container::Queue<IO::Stream*>	mSendQueue;
				Threading::SpinLock				mSendQueueLock;
				IO::Stream*						mCurrSendPacket;
				IO::Stream*						mCurrRecvPacket;


				static void	processHioIfEvent(HIO2IF_ID id, HIO2IF_EVENT event);

				inline void triggerMainExecution()
				{
					mProcessMainSignal.signal();
				}

				void onClientDisconnected();
				void onClientConnected();
				void clearSendQueue();
				void createNewRecvPacket();

				void waitMailboxCleared();
				bool readFromPC();
				bool writeToPC();
				void processInternalPacket();
				void processPingPacket();
				void processDataPacket();
				void checkClientsAlive();

				void sendInternalPacket();
				void sendPingPacket();
				void sendDataPacket();


			};
		}
	}
}
