// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/MinimalPrerequisites.h>
#include <xait/common/debugger/Server.h>
#include <xait/common/debugger/ObjectType.h>
#include <xait/common/network/message/MessageHandler.h>

namespace XAIT
{
	namespace Common
	{
		namespace Debugger
		{
			//! \brief Message handler handling MESSAGE_VARIABLE_DATA_SET.
			//!
			//! This message handler handles the request of the client to set the value of the specified variable to the specified value.
			//! Has to be registered with the server.
			//! \see Server
			class SetVariableMsgHandler : public Network::Message::MessageHandler
			{
#define MSGHANDLER_SETVARIABLE_PARAMETER	"setVariableMessage: { uint32:moduleID; uint32:objInstanceID; uint32:variableID; uint08[]:Data}"

			public:
				//! \brief Constructor.
				//! \param server	the server allows access to the modules
				SetVariableMsgHandler( Server* server );

				//! \brief Handles the message by reading the data with the MessageReader.
				//! \param msgReader	the reader containing the message
				//! \param connectionID		the identifier of the client the message is from
				//! \returns true, if message was handled correctly, else false
				bool handleMessage(Network::Message::MessageReader* msgReader, const uint32 connectionID );

			protected:

			private:
				//! \brief Reads the message parameters from the stream.
				//! \param msgReader the message reader
				bool getParameters(Network::Message::MessageReader* msgReader);
				bool getObjectType(ObjectType*& objType);

				bool setVariable(ObjectType* objType, Network::Message::MessageReader* msgReader);

				//! \brief Sends the reply to the given connection ID.
				//! \param connectionID the connection ID
				//! \return true, if sending the reply was successful, and false otherwise
				bool reportReply( const uint32 connectionID );

				Server*		mServer;				//!< the server
				
				uint32		mModuleID;				//!< the ID of the respective module
				uint32		mObjectInstanceID;		//!< the ID of the object instance
				uint32		mVariableID;			//!< the variable ID
				ObjectInstance*		mObjectInstance; //!< the object instance
			};

		}
	}
}
