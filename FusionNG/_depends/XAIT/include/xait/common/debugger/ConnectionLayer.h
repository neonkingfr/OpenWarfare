// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/MinimalPrerequisites.h>
#include <xait/common/io/Stream.h>
#include <xait/common/memory/MemoryManaged.h>


#if XAIT_OS == XAIT_OS_WII
//ToDo: implement Wii USB address
#define CONNECTION_LAYER_ADDRESS		void*
#else
#include <xait/common/network/sockets/HostAddress.h>
#define CONNECTION_LAYER_ADDRESS		XAIT::Common::Network::Sockets::HostAddress
#endif


namespace XAIT
{
	namespace Common
	{
		namespace Debugger
		{
			class Server;
		}
	}
}


namespace XAIT
{
	namespace Common
	{
		namespace Debugger
		{
			//! \brief Abstract class describing a connection layer that can be used by the server.
			//! 
			//! The server communicates with his clients using a connection layer. This class describes
			//! the generic interface of such a connection layer.
			//! \see Server
			class ConnectionLayer : public Memory::MemoryManaged
			{
			public:
				//! \brief Destructor.
				virtual ~ConnectionLayer();

				//! \brief Connects to the specified address.
				//! \param address	the address to connect to
				//! \return true, if the connecting was successful, and false otherwise
				virtual bool connect(CONNECTION_LAYER_ADDRESS address ) = 0;

				//! \brief Disconnects all connections.
				virtual void disconnect() = 0;

				//! \brief Adds a message stream that should be send over the given socket.
				//! \param connectionID		identifier to match the receiver
				//! \param messageStream	the data to send
				//! \returns true, if the socket exists and a OutputHandler was found, and false otherwise 
				virtual bool sendMessage( const uint32 connectionID, IO::Stream* messageStream ) = 0;

				//! \brief Removes the connection with the given connection ID.
				//! \param connectionID		identifier to match the connection
				//! \returns true, if the connection was removed, and false otherwise
				virtual bool removeConnection( const uint32 connectionID ) = 0;

				//! \brief Updates the internal listeners and senders.
				//! \returns 0, if updating was successful, and a value not equal to 0 if updating was not successful
				virtual int32 onTick() = 0;

				//! \brief Informs the connection layer to release any blocking in onTick.
				virtual void releaseOnTick();

			protected:
				//! \brief Constructor.
				//! \param server	pointer to the server to communicate with
				ConnectionLayer(Server* server);

				//! \brief Further dispatches the received message.
				//! \param messageStream	the received data to dispatch
				//! \param connectionID		the connection identifier
				void dispatchMessage( IO::Stream* messageStream, const uint32 connectionID );

				//! \brief Connects a new client.
				//! \param connectionID		identifier of the new client
				bool connectClient( const uint32 connectionID );

				//! \brief Disconnects a client.
				//! \param connectionID		identifier of the client
				bool disconnectClient( const uint32 connectionID );

				// member variable
				Server*		mServer;	//!< the corresponding server

			private:
			};
		}
	}
}
