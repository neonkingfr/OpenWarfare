// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/MinimalPrerequisites.h>
#include <xait/common/reflection/function/FunctionRegistrar.h>
#include <xait/common/reflection/function/FunctionTypedefs.h>
#include <xait/common/network/message/MessageReader.h>
#include <xait/common/network/message/MessageBuilder.h>
#include <xait/common/debugger/MutexGate.h>
#include <xait/common/threading/Threaded.h>
#include <xait/common/threading/Thread.h>
#include <xait/common/container/List.h>

namespace XAIT
{
	namespace Common
	{
		namespace Debugger
		{
			class Server;
			class Module;
		}
	}
}


namespace XAIT
{
	namespace Common
	{
		namespace Debugger
		{
			//! \brief Describes an RPC request.
			class RPCRequest
			{
			public:
				//! \brief Constructor.
				//! \param connectionID		the ID of the client
				//! \param rpcID			the ID of the RPC function
				//! \param funcRegistrar	the function registrar
				//! \param module			the module
				//! \param parameterStream	the stream containing the parameters
				//! \param nameRPC			the name of the RPC function
				RPCRequest(const uint32 connectionID, Reflection::Function::FunctionID rpcID, Reflection::Function::FunctionRegistrar* funcRegistrar, Module* module, IO::Stream* parameterStream, const String& nameRPC)
					: mModule(module), mClientID(connectionID), mRPCID(rpcID), mFuncRegistrar(funcRegistrar), mParameterStream(parameterStream), mNameRPC(nameRPC)
				{}

				//! \brief Destructor.
				~RPCRequest()
				{}

				Module*		mModule;						//!< the module
				uint32		mClientID;						//!< the ID of the client
				Reflection::Function::FunctionID	mRPCID;	//!< the ID of the RPC function
				Reflection::Function::FunctionRegistrar*	mFuncRegistrar; //!< the function registrar
				IO::Stream*		mParameterStream;			//!< the stream containing the parameters
				String		mNameRPC;						//!< the name of the RPC function

			private:
			};

			//! \brief Describes an RPC helper class.
			class RPCHelper : public Threading::Threaded
			{
			public:
				//! \brief Creates an RPCHelper instance.
				//! \param allocator a memory allocator
				//! \param server the server
				static void createInstance( const Memory::Allocator::Ptr& allocator, Server* server );

				//! \brief Destroys this RPCHelper instance.
				static void destroyInstance();

				//! \brief Gets the instance of the RPCHelper.
				//! \return the instance
				static inline RPCHelper* getInstance() {return mInstance;};

				//! \brief Starts the RPCHelper in an additional thread.
				//! 
				//! Use the exitAsync method to clean up.
				//! \return true, if it was successful, and false otherwise
				//! \see exitAsync
				static bool runAsync();

				//! \brief Stops the server.
				//!
				//! Use this method only when you also called runAsync.
				//! \return true, if exiting was successful, and false otherwise
				//! \see runAsync
				static bool exitAsync();
				
				//! \brief Adds an RPCRequest to the request list.
				//! \param rpcRequest the request
				void addRPCRequest( RPCRequest rpcRequest );

			private:
				//! \brief Constructor.
				//! \param server the server
				RPCHelper( Server* server );

				//! \brief Destructor
				~RPCHelper();

				//! \brief The main function that runs in the thread.
				int32 main();

				//! \brief Exits the main function loop.
				void exit();

				//! \brief Cleans everything up.
				void clean();

				//! \brief Processes the RPC request list.
				void checkRPCRequest();

				//! \brief Handles a RPCRequest.
				//! \param connectionID		the identifier of the client to answer to
				//! \param rpcID			the id of the function
				//! \param funcRegistrar	pointer to the list of registered Functions
				//! \param parameterStream	stream containing the parameters to call the function with
				//! \param nameRPC	the name of the RPC
				void handleRPCRequest(const uint32 connectionID, Reflection::Function::FunctionID rpcID, Reflection::Function::FunctionRegistrar* funcRegistrar, IO::Stream* parameterStream, const String& nameRPC);

				//! \brief Sends a message including the return value back to client after the RPC call.
				//! \param connectionID			the identifier of the client to answer to
				//! \param nameRPC				the name of the RPC
				//! \param value				pointer to the return value of the rpc
				//! \param typeID				id of the type of the return value
				//! \param useReturnVariable	flag defining whether the return value have to be written to the message
				//! \return true, if the message could be put correctly to be send back to client, and false otherwise
				bool sendMessageRPCAnswer(const uint32 connectionID, const String& nameRPC, void* value, const Reflection::Datatype::DatatypeID typeID, const bool useReturnVariable);

				//! \brief Sends a message including its return value back to client after the RPC call.
				//! \param connectionID		the identifier of the client to answer to
				//! \param nameRPC			the name of the RPC
				//! \param value			pointer to the return value of the rpc
				//! \param typeID			id of the type of the return value
				//! \returns true, if the message could be put correctly to be send back to client, and false otherwise
				bool sendMessageRPCAnswerWithVariable(const uint32 connectionID, const String& nameRPC, void* value, const Reflection::Datatype::DatatypeID typeID);

				//! \brief Sends a message without return value back to client after the RPC call.
				//!
				//! This method is useful for functions without return value, as it prevents deadlock by answering the client even for void functions.
				//! \param connectionID		the identifier of the client to answer to
				//! \param nameRPC			the name of the RPC
				//! \returns true, if the message could be put correctly to be send back to client, and false otherwise
				bool sendMessageRPCAnswerVoid(const uint32 connectionID, const String& nameRPC);

				//! \brief Reads and sets the value from the stream through MessageReader after determining the correct type via the given DatatypeID.
				//! \param msgReader	the MessageReader allowing to read from the stream
				//! \param typeID		the id of the data type
				//! \param argList		list of arguments to set the value in
				//! \param index		number of argument to set value
				//! \returns true, if the value could be read and set correctly, and false otherwise
				bool setValueFromStream(Network::Message::MessageReader* msgReader, Reflection::Datatype::DatatypeID typeID, Reflection::Function::FunctionArgumentList& argList, const uint32 index) const;

				bool buildSignatureFromStream(Network::Message::MessageReader* msgReader, Network::Message::StreamIDs streamID, Common::Container::Vector<Reflection::Datatype::DatatypeID>& signature);


				//! \brief Writes a value and its typeID into the message using the given MessageBuilder.
				//! \param msgBuilder	the MessageBuilder
				//! \param value		the value to be written to stream
				//! \param typeID		the id of the type to be written to stream
				bool setValueToStream(Network::Message::MessageBuilder* msgBuilder, void* value, const Reflection::Datatype::DatatypeID typeID) const;


			// member variables
				Server*		mServer;								//!< the server
				Container::List<RPCRequest>		mRPCRequestList;	//!< the RPC request list

				bool				mIsNeeded;						//!< flag indicating whether the server is still needed
				Threading::Mutex	mMutex;							//!< mutex to synchronize with the main thread
				MutexGate			mMutexGate;						//!< prevents RPCHelper from going over main loop without work to do

			//singleton variables
				static Threading::Thread*	mRPCHelperThread;	//!< instance of the RPCHelper thread
				static RPCHelper*			mInstance;			//!< instance of the RPCHelper
			};
		}
	}
}
