// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/MinimalPrerequisites.h>
#include <xait/common/debugger/Server.h>
#include <xait/common/network/message/MessageHandler.h>

namespace XAIT
{
	namespace Common
	{
		namespace Debugger
		{
			//! \brief A message handler to handle RPC request messages.
			//!
			//! Has to be registered with the server.
			//! \see Server
			class RPCMsgHandler : public Network::Message::MessageHandler
			{
			public:
				//! \brief Constructor.
				//! \param server	the server allows RPC access
				RPCMsgHandler( Server* server );

				//! \brief Handles the message by reading the data through the MessageReader
				//! \param msgReader		the reader containing the message
				//! \param connectionID		the identifier of the client the message is from
				//! \returns true, if the message was handled correctly, and false otherwise
				bool handleMessage(Network::Message::MessageReader* msgReader, const uint32 connectionID );

			private:
				////! \brief sends message including return value back to client after rpc call
				////! \param socket	the socket to answer to
				////! \param nameRPC	the name of the RPC
				////! \param value	pointer to the return value of the rpc
				////! \param typeID	id of the type of the return value
				////! \param useReturnVariable	flag defining whether the return value have to be written to the message
				////! \returns true, if the message could be put correctly to be send back to client, else false
				//bool sendMessageRPCAnswer(Network::Sockets::Socket* socket, const String& nameRPC, void* value, const Reflection::Datatype::DatatypeID typeID, const bool useReturnVariable);

				////! \brief sends message including return value back to client after rpc call
				////! \param socket	the socket to answer to
				////! \param nameRPC	the name of the RPC
				////! \param value	pointer to the return value of the rpc
				////! \param typeID	id of the type of the return value
				////! \returns true, if the message could be put correctly to be send back to client, else false
				//bool sendMessageRPCAnswerWithVariable(Network::Sockets::Socket* socket, const String& nameRPC, void* value, const Reflection::Datatype::DatatypeID typeID);

				////! \brief sends message without return value back to client after rpc call
				////! \param socket	the socket to answer to
				////! \param nameRPC	the name of the RPC
				////! \returns true, if the message could be put correctly to be send back to client, else false
				////! \remark useful for functions without return value, prevents deadlook by answering client even for void functions
				//bool sendMessageRPCAnswerVoid(Network::Sockets::Socket* socket, const String& nameRPC);

				////! \brief reads and sets the value from the stream through MessageReader after determined the correct type via given DatatypeID
				////! \param msgReader	the MessageReader allowing to read from the stream
				////! \param typeID	the id of the data type
				////! \param argList	list of arguments to set the value in
				////! \param index	number of argument to set value
				////! \returns true, if the value could be read and set correctly
				//bool setValueFromStream(Network::Message::MessageReader* msgReader, Reflection::Datatype::DatatypeID typeID, Reflection::Function::FunctionArgumentList& argList, const uint32 index) const;

				////! \brief writes a value specified via given typeID into message using given MessageBuilder
				////! \param msgBuilder	MessageBuilder allows writing of data to included stream
				////! \param value		the value to be written to stream
				////! \param typeID		the id of the type to be written to stream
				//bool setValueToStream(Network::Message::MessageBuilder* msgBuilder, void* value, const Reflection::Datatype::DatatypeID typeID) const;


				Server*		mServer; //!< the server
			};

		}
	}
}
