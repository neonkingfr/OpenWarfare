// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/MinimalPrerequisites.h>
#include <xait/common/debugger/Server.h>
#include <xait/common/debugger/ObjectType.h>
#include <xait/common/network/message/MessageHandler.h>

namespace XAIT
{
	namespace Common
	{
		namespace Debugger
		{
			//! \brief Message handler handling MESSAGE_OBJECTINSTANCE_REQUEST.
			//!
			//! This message handler is called when the client requests to create an ObjectInstance
			//! within a specified module from the server.
			//! The message handler has to be registered with the server.
			//! \see Server
			class ObjectInstanceRequestMsgHandler : public Network::Message::MessageHandler
			{
#define MSGHANDLER_OBJECTINSTANCE_REQUEST_PARAMETER	"objectInstanceRequestMessage: { uint32:moduleID, uint32:objectTypeID, BString:objectInstanceName }"

			public:
				//! \brief Constructor.
				//! \param server the server
				ObjectInstanceRequestMsgHandler( Server* server );

				//! \brief Handles the message by reading the data through the MessageReader.
				//! \param msgReader	the reader containing the message
				//! \param connectionID		the identifier of the client the message is from
				//! \returns true, if message was handled correctly, and false otherwise
				bool handleMessage(Network::Message::MessageReader* msgReader, const uint32 connectionID );

			private:
				//! \brief Reads the message parameters from the stream.
				//! \param msgReader the message reader
				bool getParameters(Network::Message::MessageReader* msgReader);

				Server*		mServer;

				uint32 mModuleID;
				uint32 mObjectTypeID;
				BString mObjectInstanceName;

				Module* mModule;
				ObjectType* mObjectType;
			};

		}
	}
}
