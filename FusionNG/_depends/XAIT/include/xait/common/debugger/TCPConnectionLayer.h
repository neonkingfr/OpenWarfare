// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/MinimalPrerequisites.h>
#include <xait/common/network/sockets/TCPSocket.h>
#include <xait/common/debugger/ConnectionLayer.h>
#include <xait/common/network/Reactor.h>
#include <xait/common/network/TCPPacketSendHandler.h>
#include <xait/common/container/IDVector.h>
#include <xait/common/debugger/MutexGate.h>



namespace XAIT
{
	namespace Common
	{
		namespace Debugger
		{
			class TCPPacketReceiveHandler;
		}
	}
}


namespace XAIT
{
	namespace Common
	{
		namespace Debugger
		{
			//! \brief Describes a single TCP connection.
			//!
			//! Used by the TCP connection layer.
			//! \see TCPConnectionLayer
			//! \see TCPPacketSendHandler
			//! \see TCPPacketReceiveHandler
			struct TCPConnection
			{
				//! \brief Constructor.
				//! \param socket the socket of the connection
				TCPConnection(Network::Sockets::TCPSocket* socket)
					: mSocket(socket), mSendHandler(NULL), mReceiveHandler(NULL)
				{}

				//! \brief Constructor.
				//! \param socket the socket of the connection
				//! \param sendHandler handles send requests
				//! \param receiveHandler handles receive requests
				TCPConnection(Network::Sockets::TCPSocket* socket, Network::TCPPacketSendHandler* sendHandler, TCPPacketReceiveHandler* receiveHandler)
					: mSocket(socket), mSendHandler(sendHandler), mReceiveHandler(receiveHandler)
				{}

				Network::Sockets::TCPSocket* mSocket;
				Network::TCPPacketSendHandler* mSendHandler;
				TCPPacketReceiveHandler* mReceiveHandler;
			};

		
			//! \brief This class describes the TCP connection layer.
			//!
			//! The TCP connection layer handles low level TCP network operations. It is used by the Server.
			//! \see Network::Server
			class TCPConnectionLayer : public ConnectionLayer
			{
			public:
				//! \brief Constructor.
				//! \param server the server to which this connection layer belongs to
				//! \param waitTimeMicroSeconds the number of micro seconds that should 
				TCPConnectionLayer(Server* server, Platform::MicroSeconds waitTimeMicroSeconds);

				//! \brief Destructor.
				~TCPConnectionLayer();

				//! \brief Establishes a TCP connection to the given address.
				//! \param address	the address
				bool connect( Network::Sockets::HostAddress address );

				//! \brief Disconnects all connections.
				void disconnect();

				//! \brief Adds a message stream that should be send over the given socket.
				//! \param connectionID		connection ID of the receiver
				//! \param messageStream	the data to send
				//! \returns true, if the socket exists and a OutputHandler was found, and false otherwise
				bool sendMessage( const uint32 connectionID, IO::Stream* messageStream );

				//! \brief Adds and registers a new connection.
				//! \param newSocket the socket
				//! \return true, if adding the connection was successful, and false otherwise
				bool addConnection( Network::Sockets::TCPSocket* newSocket );

				//! \brief Removes the connection with the given connection ID.
				//! \param connectionID		identifier to match the client
				//! \returns true, if removing the connection was successful, and false otherwise
				bool removeConnection( const uint32 connectionID );

				//! \brief Updates the internal listeners and senders.
				//! \returns 0, if updating was successful, and -1 if updating was not successful
				int32 onTick();

			private:
				friend class TCPPacketReceiveHandler;


			// member variables
				Network::Reactor*	mReactor;								//!< the reactor managing the communication
				Container::IDVector<TCPConnection*>		mConnectionList;	//!< the list of connections
			};
		}
	}
}
