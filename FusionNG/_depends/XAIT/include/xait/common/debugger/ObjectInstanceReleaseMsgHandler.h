// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/MinimalPrerequisites.h>
#include <xait/common/debugger/Server.h>
#include <xait/common/network/message/MessageHandler.h>

namespace XAIT
{
	namespace Common
	{
		namespace Debugger
		{
			//! \brief Message handler handling MESSAGE_OBJECTINSTANCE_RELEASE.
			//!
			//! This message handler is called when the client requests to release an ObjectInstance.
			//! This is only possible if the ObjectInstance is a "client only ObjectInstance"
			//! waiting to be connected to a "real ObjectInstance" from the game.
			//! The message handler has to be registered with the server.
			//! \see Server
			class ObjectInstanceReleaseMsgHandler : public Network::Message::MessageHandler
			{
#define MSGHANDLER_OBJECTINSTANCE_RELEASE_PARAMETER	"objectInstanceReleaseMessage: { uint32:moduleID, uint32:objectInstanceID }"

			public:
				//! \brief Constructor.
				//! \param server the server
				ObjectInstanceReleaseMsgHandler( Server* server );

				//! \brief Handles the message by reading the data through the MessageReader.
				//! \param msgReader	the reader containing the message
				//! \param connectionID		the identifier of the client the message is from
				//! \returns true, if message was handled correctly, and false otherwise
				bool handleMessage(Network::Message::MessageReader* msgReader, const uint32 connectionID );

			protected:

			private:
				//! \brief Reads the message parameters from the stream.
				//! \param msgReader the message reader
				bool getParameters(Network::Message::MessageReader* msgReader);

				Server*		mServer;		//!< the server

				uint32 mModuleID;			//!< the module ID
				uint32 mObjectInstanceID;	//!< the object instance ID

				Module* mModule;			//!< the module
			};

		}
	}
}
