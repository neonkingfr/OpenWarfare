// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/MinimalPrerequisites.h>
#include <xait/common/container/List.h>
#include <xait/common/container/LookUp.h>
#include <xait/common/debugger/ObjectType.h>
#include <xait/common/network/message/MessageBuilder.h>

namespace XAIT
{
	namespace Common
	{
		namespace Debugger
		{
#define INVALID_OBJECT_GROUP_ID		(uint32)-1


			//! \brief This class defines an ObjectGroup.
			//!
			//! An object group groups other ObjectGroups and ObjectTypes. This is useful when you want to
			//! have several ObjectTypes with the same name. By establishing an ObjectGroup hierarchy, you
			//! can add different ObjectTypes with the same name to different ObjectGroups.
			//! ObjectGroups can be created by calling Module::createObjectGroup.
			//! \see Module
			//! \see ObjectType
			class ObjectGroup : public Memory::MemoryManaged
			{
			public:
				//! \brief Registers a new sub ObjectGroup to this ObjectGroup.
				//! \param name		the name of the subgroup
				//! \return a pointer to the new ObjectGroup if it wasn't registered yet, or NULL otherwise
				XAIT_COMMON_DLLAPI ObjectGroup* registerSubObjectGroup(const String& name);

				//! \brief Adds an ObjectType to this group.
				//! \param name		the name of the object type
				//! \return the ObjectType if the name wasn't known, NULL else
				XAIT_COMMON_DLLAPI ObjectType* createObjectType(const String& name);

				//! \brief Removes an ObjectType from this group.
				//! \param objTypeID		the id of the object type
				//! \return true, if the ObjectType was registered, and false otherwise
				XAIT_COMMON_DLLAPI bool destroyObjectType(const uint32 objTypeID);

				//! \brief Returns the ID of this ObjectGroup.
				//! \return the ID of this ObjectGroup
				XAIT_COMMON_DLLAPI uint32 getID() const { return mID; }

				//! \brief Gets the name of the ObjectGroup.
				//! \return the name of the ObjectGroup
				inline const String& getName() const { return mName; }

				//! \brief Gets the parent ObjectGroup.
				//! \return the parent ObjectGroup
				inline ObjectGroup* getParent() const {return mParentGroup;}

				//! \brief Returns the sub ObjectGroup with the given ID.
				//! \param subGroupID	the id of the sub ObjectGroup
				//! \return a pointer to the sub ObjectGroup if exists, and NULL otherwise
				XAIT_COMMON_DLLAPI ObjectGroup* getSubGroup(const uint32 subGroupID);

				//! \brief Returns the sub ObjectGroup with the given name.
				//! \param subGroupName	the name of the sub ObjectGroup
				//! \return a pointer to the sub ObjectGroup if exists, and NULL otherwise
				XAIT_COMMON_DLLAPI ObjectGroup* getSubGroup(const String& subGroupName);

				//! \brief Gets the ObjectType with the given ObjectType name.
				//! \param objType		the pointer which is set to the ObjectType
				//! \param objName		the name of the ObjectType
				//! \return true if the ObjectType was found, and false otherwise
				XAIT_COMMON_DLLAPI bool getObjectType(ObjectType*& objType, const String& objName);

				//! \brief Gets the Module to which this ObjectGroup belongs to.
				//! \return the Module this ObjectGroup belongs to
				inline Module* getModule() const {return mModule;}

				//! \brief Checks whether the breakpoint is activated.
				//! \return true, if it is activated, and false otherwise
				XAIT_COMMON_DLLAPI bool isBreakpointActivated() const;

				//! \brief Checks whether the breakpoint is holding.
				//! \return true, if it is holding, and false otherwise
				XAIT_COMMON_DLLAPI bool isBreakpointHolding() const;


			private:
				friend class Module;
				friend class ObjectType;

				//! \brief Constructor.
				//! \param module		the module this ObjectGroup belongs to
				//! \param name			the name of this ObjectGroup
				//! \param parentGroup	the parent group of this ObjectGroup (NULL by default)
				ObjectGroup(Module* module, const String& name, ObjectGroup* parentGroup=NULL);

				//! \brief Destructor.
				virtual ~ObjectGroup();

				//! \brief Initializes the ID.
				//!
				//! This function should only be called once. The id must not be INVALID_OBJECT_GROUP_ID.
				//! \param id	the id to be set
				void initID( const uint32 id );

				//! \brief Creates a new ObjectInstance ID to an object instance.
				//! \param objInst the object instance
				//! \return the new ObjectInstance ID
				uint32 createObjectInstanceID( ObjectInstance* objInst );

				//! \brief Deregisters the given ObjectInstance ID.
				//! \param id the ID
				void deregisterObjectInstanceID( const uint32 id );

				//! \brief Creates a new BreakpointType ID to a breakpoint type.
				//! \param bType the breakpoint type
				//! \return the new BreakpointType ID
				uint32 createBreakpointTypeID( BreakpointType* bType );

				//! \brief Deregisters the given BreakpointType ID.
				//! \param id the ID
				void deregisterBreakpointTypeID( const uint32 id );

				//! \brief Creates a new BreakpointInstance ID to a breakpoint instance.
				//! \param bInstance the breakpoint instance
				//! \return the new BreakpointInstance ID
				uint32 createBreakpointInstanceID( BreakpointInstance* bInstance );

				//! \brief Deregisters the given BreakpointInstance ID.
				//! \param id the ID
				void deregisterBreakpointInstanceID( const uint32 id );

				//uint32 createWatchpointID( Watchpoint& w );
				//void deregisterWatchpointID( const uint32 id );

				//! \brief Creates a new VariableType ID to a variable type.
				//! \param v the variable type
				//! \return the new VariableType ID
				uint32 createVariableTypeID( VariableType* v );

				//! \brief Deregisters the given VariableType ID.
				//! \param id the ID
				void deregisterVariableTypeID( const uint32 id );

				//! \brief Is called when a breakpoint holds.
				//! \param triggerSource	the ObjectGroup containing the breakpoint
				//! \param breakInstance	the breakpoint that inflicted this call
				//! \param triggerObjInst	the ObjectInstance that entered the Breakpoint
				//! \return true, when it worked, and false otherwise
				bool enterBreakpoint(const ObjectGroup* triggerSource, BreakpointInstance* breakInstance, ObjectInstance* triggerObjInst);

				//! \brief Is called when a breakpoint holds.
				//! \param triggerSource	the ObjectType containing the breakpoint
				//! \param breakInstance	the breakpoint that inflicted this call
				//! \param triggerObjInst	the ObjectInstance that entered the Breakpoint
				//! \return true, when it worked, and false otherwise
				bool enterBreakpoint(const ObjectType* triggerSource, BreakpointInstance* breakInstance, ObjectInstance* triggerObjInst);

				//! \brief is called if a breakpoint that was holding released its thread
				//! \param triggerSource	the breakpoint that inflicted this call
				//! \returns true further reporting was correct, false else
				//bool onBreakpointRelease(const ObjectGroup* triggerSource);
				//bool onBreakpointRelease(const ObjectType* triggerSource);

				//! \brief Informs the module that the connected client closed the connection.
				//!
				//! No breakpoints and watchpoints points should report anymore.
				void onClientDisconnect();

				//! \brief collects the values of all variables within all ObjectTypes under this ObgjectGroup within given HistoryEntry
				//! \param hEntry	the HistoryEntry to write to
				//void getWatchesSnapshot(HistoryEntry* hEntry) const;

				//! \brief Sends all the information about this ObjectGroup to the Client.
				//! \param newClientInfo	the client info of the new client
				void onClientConnect(ClientInfo* newClientInfo);

				//! \brief Collects the ID list of the groups.
				//! \param objGroupIDList	the list to fill with the IDs
				//! \returns true if groups are registered and list has at least one entry, false else
				//! \remark only the first layer of groupIDs are reported NOT the subGroupIDs
				//bool getObjectGroupIDList(Container::List<uint32>& objGroupIDList) const;

				//! \brief Activates or deactivates the breakpoint.
				//! \param val set it to true, if the breakpoint should be activated, and to false if it should be deactivated
				void setBreakpointActiveValue( const bool val );

				//! \brief updates active state based on disjunction of parent value and own value
				//! \param val		the new value of the parent
				//void updateOnParentActiveState(const bool val);

				//! \brief Calls Module::sendMessage.
				//! \param stream the message that should be send
				//! \return true, if sending the message was successful, and false otherwise.
				//! \see Module::sendMessage
				bool sendMessage(IO::Stream* stream);

				//! \brief Checks whether a client is connected.
				//! \return true, if a client is connected, and false otherwise
				bool isClientConnected() const;

				//! \brief Attaches the ObjectType to the debugger.
				//! \param obj		the ObjectType to attach
				void attachObjectType(ObjectType* obj);

				//! \brief releases the mutex and marks the current holding ObjectInstance as an additional reason for a Break
				//! \returns true if a Breakpoint was holding a thread, false else
				//! \remarks any enterBreakpoint that is entered with this ObjectInstance will break
				//bool makeStep();

				//! \brief releases the mutex for one moment
				//! \returns true if a Breakpoint was holding a thread, false else
				//bool makeStepFiltered(const uint32 breakpointTypeID);

				//bool report(Network::Message::MessageBuilder& msgBuilder) const;

				////! \brief returns whether the ID of this ObjectGroup is still default value
				////! \returns whether the ID of this ObjectGroup is still default value
				//bool isDefaultID() const {return (mID == INVALID_OBJECT_GROUP_ID); }

			//member variables
				String			mName;						//!< the name of the ObjectGroup
				uint32			mID;						//!< the ID of the ObjectGroup
				ObjectGroup*	mParentGroup;				//!< the parent ObjectGroup
				Module*			mModule;					//!< the corresponding module
				bool			mIsBreakpointActivated;		//!< flag that indicates whether a breakpoint is activated

				Container::LookUp<uint32, ObjectGroup*>		mChildGroupList; //!< the list of child ObjectGroups
				Container::LookUp<uint32, ObjectType*>		mObjectTypeList; //!< the list of ObjectTypes

				Container::LookUp<String, uint32>	mObjTypeNameToID;	//!< a mapping from ObjectType names to ObjectType IDs
				Container::LookUp<String, uint32>	mObjGroupNameToID;	//!< a mapping from ObjectGroup names to ObjectGroup IDs

				uint32 mHoldingObjectTypeID;				//!< the ID of the ObjectType that is currently holding the breakpoint
				uint32 mHoldingObjectGroupID;				//!< the ID of the ObjectGroup that is currently holding the breakpoint

				ClientInfo* mClientConnected;				//!< the client info of the connected client
			};
		}
	}
}
