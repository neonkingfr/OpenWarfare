// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/MinimalPrerequisites.h>
#include <xait/common/debugger/Server.h>
#include <xait/common/debugger/ObjectInstance.h>
#include <xait/common/network/message/MessageHandler.h>

namespace XAIT
{
	namespace Common
	{
		namespace Debugger
		{
			//! \brief Message handler handling MESSAGE_VARIABLE_DATA_GET.
			//!
			//! The client requests the value of a variable of the given object instance and the module
			//! from the server.
			//! The message handler has to be registered with the server.
			//! \see Server
			class GetVariableMsgHandler : public Network::Message::MessageHandler
			{
#define MSGHANDLER_GETVARIABLE_PARAMETER	"getVariableMessage: { uint32:moduleID; uint32:objInstanceID; uint32:variableID;}"

			public:
				//! \brief Constructor.
				//! \param server	the server allows access to the registered modules
				GetVariableMsgHandler( Server* server );

				//! \brief Handles the message by reading the data through the MessageReader.
				//! \param msgReader	the reader containing the message
				//! \param connectionID		the identifier of the client the message is from
				//! \returns true, if message was handled correctly, and false otherwise
				bool handleMessage(Network::Message::MessageReader* msgReader, const uint32 connectionID );

			private:
				//! \brief Reads the message parameters from the stream.
				//! \param msgReader the message reader
				bool getParameters(Network::Message::MessageReader* msgReader);

				//! \brief Sends the reply to the given connection ID.
				//! \param connectionID the connection ID
				//! \return true, if sending the reply was successful, and false otherwise
				bool reportReply( const uint32 connectionID );


				Server*		mServer;					//!< the server
				uint32		mModuleID;					//!< the module ID
				uint32		mObjInstanceID;				//!< the object instance ID
				uint32		mVariableID;				//!< the variable ID
				ObjectInstance*		mObjectInstance;	//!< the object instance
			};

		}
	}
}
