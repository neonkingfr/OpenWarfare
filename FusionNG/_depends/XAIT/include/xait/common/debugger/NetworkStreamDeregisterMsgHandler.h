// (C) xaitment GmbH 2006-2012

#pragma once 

#include <xait/common/io/Stream.h>
#include <xait/common/debugger/Server.h>

namespace XAIT
{
	namespace Common
	{
		namespace Debugger
		{
			//! \brief Message handler handling MESSAGE_NETWORKSTREAM_DEREGISTRATION.
			//!
			//! The message will be send if a NetworkStream is deregistered from client to server.
			//! Has to be registered with the server.
			//! \see Server
			class NetworkStreamDeregisterMsgHandler : public Network::Message::MessageHandler
			{
#define MSGHANDLER_NETWORKSTREAM_DEREGISTER_PARAMETER	"deregisterNetworkStreamMessage: { uint32:networkStreamID }"

			public:
				//! \brief Constructor.
				//! \param server	the server allows access to the registered modules
				NetworkStreamDeregisterMsgHandler( Server* server );

				//! \brief Destructor.
				virtual ~NetworkStreamDeregisterMsgHandler();

				//! \brief Handles the message by reading the data from the MessageReader.
				//! \param msgReader	the reader containing the message
				//! \param connectionID		the identifier of the client the message is from
				//! \returns true, if the message was handled correctly, and false otherwise
				bool handleMessage(Network::Message::MessageReader* msgReader, const uint32 connectionID );

			protected:

			private:
				//! \brief Reads the message parameters from the stream.
				//! \param msgReader the message reader
				bool getParameters(Network::Message::MessageReader* msgReader);

				//! \brief Sends the reply to the given connection ID.
				//! \param connectionID the connection ID
				//! \return true, if sending the reply was successful, and false otherwise
				bool reportReply( const uint32 connectionID );


				Server*		mServer;			//!< the server
				uint32		mNetworkStreamID;	//!< the network stream ID

				uint08		mResultCode;		// TODO: Remove, already defined in the parent class
				String		mErrorText;			// TODO: Remove
			};
		}
	}
}
