// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/MinimalPrerequisites.h>
#include <xait/common/network/message/MessageBuilder.h>
#include <xait/common/network/message/MessageReader.h>
#include <xait/common/container/List.h>
#include <xait/common/container/LookUp.h>
#include <xait/common/container/Vector.h>
#include <xait/common/container/IDGenerator.h>
#include <xait/common/debugger/BreakpointInstance.h>
#include <xait/common/debugger/BreakpointType.h>
#include <xait/common/debugger/ObjectType.h>
#include <xait/common/debugger/HistoryEntry.h>
#include <xait/common/threading/Mutex.h>
#include <xait/common/reflection/function/FunctionRegistrar.h>
#include <xait/common/reflection/datatype/DatatypeBase.h>
#include <xait/common/reflection/datatype/DatatypeTypedefs.h>
#include <xait/common/debugger/Server.h>


namespace XAIT
{
	namespace Common
	{
		namespace Debugger
		{
			class GetHistoryMsgHandler;
			class GetVariableMsgHandler;
			class SetBreakpointConditionMsgHandler;
			class SetBreakpointGlobalMsgHandler;
			class SetBreakpointMsgHandler;
			class SetBreakpointObjectInstanceMsgHandler;
			class SetVariableMsgHandler;
			class MakeStepMsgHandler;
			class SetWatchesMsgHandler;
			class ObjectInstance;
			class ObjectTypeAckMsgHandler;
			class ObjectInstanceRequestMsgHandler;
			class ObjectInstanceReleaseMsgHandler;
		}
	}
}


namespace XAIT
{
	namespace Common
	{
		namespace Debugger
		{
			//! \brief Describes a debugger module.
			//!
			//! A module allows to connect several different debug-able libraries to the same debugger server.
			//! The server is then able to connect the different clients to the matching library.
			//! In order to use the module, it has to be attached to the debugger.
			//! A module may create and contain several ObjectGroups.
			//! \see ObjectGroup
			//! \see Server
			class Module : public Memory::MemoryManaged
			{
			public:
				//! \brief Attaches the Module to the debugger
				//!
				//! All BreakpointTypes of this module have to be registered.
				inline void attach() { mIsAttached = true; }

				//! \brief Checks whether this Module is attached to the debugger.
				//! \return true, if it is attached, and false otherwise
				inline bool isAttached() const { return mIsAttached; }

				//! \brief Registers a new ObjectGroup.
				//! \param group	the name of the ObjectGroup
				//! \return the ObjectGroup if the name wasn't known before, and NULL otherwise
				XAIT_COMMON_DLLAPI ObjectGroup* createObjectGroup(const String& name);

				//! \brief Finds and returns an ObjectGroup within this module.
				//! \param objGroup		stores the ObjectGroup if it was found
				//! \param objGroupName	the name of the wanted ObjectGroup
				//! \returns true, if the ObjectGroup was found, and false otherwise
				XAIT_COMMON_DLLAPI bool getObjectGroup(ObjectGroup*& objGroup, const String& objGroupName) const;

				//! \brief Finds the wanted ObjectGroup within this module.
				//! \param objGroup		stores the ObjectGroup if it was found
				//! \param id			the ID of the wanted ObjectGroup
				//! \returns true, if the ObjectGroup was found, and false otherwise
				inline bool getObjectGroup( ObjectGroup*& objGroup, const uint32 id ) const { return mObjectGroupList.getElement(objGroup, id); }

				//! \brief Finds the wanted ObjectType within this module.
				//! \param objType		stores the ObjectType if it was found
				//! \param id			the ID of the wanted ObjectType
				//! \returns true, if the ObjectType was found, and false otherwise
				inline bool getObjectType( ObjectType*& objType, const uint32 id ) const { return mObjectTypeList.getElement(objType, id); }

				//! \brief Finds the wanted ObjectInstance within this module.
				//! \param objI			stores the ObjectInstance if it was found
				//! \param id			the ID of the wanted ObjectInstance
				//! \returns true, if the ObjectInstance was found, and false otherwise
				inline bool getObjectInstance( ObjectInstance*& objI, const uint32 id ) const { return mObjectInstanceList.getElement(objI, id); }

				//! \brief Finds the wanted BreakpointType within this module.
				//! \param bType		stores the BreakpointType if it was found
				//! \param id			the ID of the wanted BreakpointType
				//! \returns true, if the BreakpointType was found, and false otherwise
				inline bool getBreakpointType( BreakpointType*& bType, const uint32 id ) { return mBreakpointTypeList.getElement(bType, id); }

				//! \brief Finds the wanted BreakpointInstance within this module.
				//! \param b			stores the BreakpointInstance if it was found
				//! \param id			the ID of the wanted BreakpointInstance
				//! \returns true, if the BreakpointInstance was found, and false otherwise
				inline bool getBreakpointInstance( BreakpointInstance*& b, const uint32 id ) const { return mBreakpointInstanceList.getElement(b, id); }

				//! \brief Finds the wanted VariableType within this module.
				//! \param v			stores the VariableType if it was found
				//! \param id			the ID of the wanted VariableType
				//! \returns true, if the VariableType was found, and false otherwise
				inline bool getVariableType( VariableType*& v, const uint32 id ) const { return mVariableTypeList.getElement(v, id); }

				//! \brief Registers the breakpoint type with the given name.
				//! \param breakpointTypeName the BreakpointType name
				//! \return the BreakpointType, if the name was unique, and NULL if there already exists a BreakpointType with this name
				XAIT_COMMON_DLLAPI BreakpointType* registerBreakpointType(const String& breakpointTypeName);

				//! \brief Finds the wanted BreakpointType within this module.
				//! \param breakpointType	stores the BreakpointType if it was found
				//! \param name				the ID of the wanted BreakpointType
				//! \returns true, if the BreakpointType was found, and false otherwise
				XAIT_COMMON_DLLAPI bool getBreakpointType(BreakpointType*& breakpointType, const String& name);

				//! \brief Checks whether a breakpoint is holding in either this module or any other module.
				//! \return true, if a breakpoint is holding in any module, and false otherwise
				XAIT_COMMON_DLLAPI bool isBreakpointActivated() const;

				//! \brief Checks whether a breakpoint is holding in this module.
				//! \return true, if a breakpoint is holding in this module, and false otherwise
				XAIT_COMMON_DLLAPI bool isBreakpointHolding() const;

				//! \brief Gets the name of the module.
				//! \return the name
				inline const String& getName() const { return mName; }

				//! \brief Gets the version of the module.
				//! \return the version
				inline uint32 getVersion() const { return mVersion; }

				//! \brief Gets the ID of the module.
				//! \return the ID
				inline uint32 getModuleID() const { return mID; }

				//! \brief Enable the next Breakpoint that is entered in this module.
				XAIT_COMMON_DLLAPI void stopInNextBreakpoint();

				//! \brief adds a entry to the history that is used to remove the static flag from another entry
				XAIT_COMMON_DLLAPI void addReleaseStaticHistoryEntry(ObjectInstance* objectInstance, HistoryEntry* entry);

				//! \brief sets the maximal length of an history
				//! \param maximalHistoryLength the new maximal length of an history
				XAIT_COMMON_DLLAPI void setMaximalHistoryLength(uint32 maxLength);

				//! \brief Defines whether the library tracks the variables during run time for debugger
				//! \param trackVariables	flag to define whether the variables should be tracked
				XAIT_COMMON_DLLAPI void setVariableTracking(bool trackVariables);

				//! \brief Registers an external function with up to 10 arguments with compiletime/runtime check.
				//! \param name		access name of the function
				//! \param owner	owner object of the method
				//! \param func		a pointer to the function 
				template<typename T_FUNCTIONOWNER_TYPE, typename T_FUNC>
				Reflection::Function::FunctionID registerRPCModule(const String& name, const T_FUNCTIONOWNER_TYPE& owner, T_FUNC func)
				{
					mMutex.lock();
					Reflection::Function::FunctionID fID = mFunctionRegistrar.registerFunctionByFullName(name,owner,func);
					mMutex.unlock();
					return fID;
				}

				//! \brief Registers an external function with up to 10 arguments with compiletime/runtime check.
				//! \param name		access name of the function
				//! \param owner	owner object of the method
				//! \param func		a pointer to the function 
				template<typename T_RETURNVALUE_TYPE,typename T_FUNCTIONOWNER_TYPE>
				void registerRPCModuleWithVariableArguments(
					const String& name,
					const T_FUNCTIONOWNER_TYPE& owner,
					T_RETURNVALUE_TYPE (T_FUNCTIONOWNER_TYPE::*func)(void*,Common::Reflection::Datatype::DatatypeID,const Common::Reflection::Function::FunctionArgumentList&),
					Common::Reflection::Datatype::DatatypeID returnType,
					bool overrideExistingBindings)
				{
					mMutex.lock();
					mFunctionRegistrar.registerVariadicFunction(name, "Sys",owner,func, returnType, overrideExistingBindings);
					mMutex.unlock();
				}

			private:
				friend class Server;
				friend class ObjectGroup;
				friend class ObjectInstance;
				friend class BreakpointInstance;

				friend class GetHistoryMsgHandler;
				friend class GetVariableMsgHandler;
				friend class SetBreakpointConditionMsgHandler;
				friend class SetBreakpointGlobalMsgHandler;
				friend class SetBreakpointMsgHandler;
				friend class SetBreakpointObjectInstanceMsgHandler;
				friend class SetVariableMsgHandler;
				friend class MakeStepMsgHandler;
				friend class SetWatchesMsgHandler;
				friend class ObjectTypeAckMsgHandler;
				friend class ObjectInstanceRequestMsgHandler;
				friend class ObjectInstanceReleaseMsgHandler;
				


				//! \brief Constructor.
				//! \param name				the name of the module
				//! \param version			the version of the module
				//! \param id				the ID of the module
				//! \param server			the server to communicate to
				//! \param maxHistoryLength the maximum number of the history entries
				Module(const String& name, const uint32& version, const uint32& id, Server* server, const uint32 maxHistoryLength=50);

				//! \brief Destructor.
				virtual ~Module();

				//! \brief Checks whether the ObjectGroup with given name exists within this Module.
				//! \return true if the ObjectGroup with given name exists within this Module, and false otherwise
				bool existsObjectGroup(const String& objGroupName);

				//! \brief Sends a message to the connected client with the current breakpoint state.
				//! \return true if the message could be given correctly to the OutputHandler, and false otherwise
				bool sendMessageModuleBreakpointState(ObjectInstance* triggerObjInst);

				//! \brief Is called when a breakpoint holds.
				//! \param triggerSource	the ObjectGroup containing the breakpoint
				//! \param breakInstance	the breakpoint that inflicted this call
				//! \param triggerObjInst	the ObjectInstance that entered the breakpoint
				//! \return true, when everything worked fine, and false otherwise
				bool enterBreakpoint(const ObjectGroup* triggerSource, BreakpointInstance* breakInstance, ObjectInstance* triggerObjInst);

				//! \brief Is called when the module passes a breakpoint without holding.
				//!
				//! Writes a new entry to the history list. It may delete an older one if the list is too long.
				//! \param breakpointInstance	the breakpoint that inflicted this call
				//! \param objectInstance	the ObjectInstance that entered the Breakpoint
				//! \return true, if the history could be updated correctly, and false otherwise
				bool onBreakpointPassing(BreakpointInstance* breakpointInstance, const ObjectInstance* objectInstance);

				//! \biref removes the list of history entries for given ObjectInstanceID
				//! \param	objectInstanceID		the ID of the ObjectInstance
				void removeHistoryList(const uint32 objectInstanceID);

				//! \brief Checks whether the other module has the same specifications as this one.
				//! \return true, if the other module has the same specification, and false otherwise
				bool isEqual(Module& other);

				//! \brief Creates a new ObjectGroup ID to an object group.
				//! \param objGroup the object group
				//! \return the new ObjectGroup ID
				uint32 createObjectGroupID( ObjectGroup* objGroup );

				//! \brief Deregisters the given ObjectGroup ID.
				//! \param id the ID
				void deregisterObjectGroupID( const uint32 id );

				//! \brief Creates a new ObjectType ID to an object type.
				//! \param objType the object type
				//! \return the new ObjectType ID
				uint32 createObjectTypeID( ObjectType* objType );

				//! \brief Deregisters the given ObjectType ID.
				//! \param id the ID
				void deregisterObjectTypeID( const uint32 id );

				//! \brief Creates a new ObjectInstance ID to an object instance.
				//! \param objInst the object instance
				//! \return the new ObjectInstance ID
				uint32 createObjectInstanceID( ObjectInstance* objInst );

				//! \brief Deregisters the given ObjectInstance ID.
				//! \param id the ID
				//! \remark History for this ObjectInstance is removed to
				void deregisterObjectInstanceID( const uint32 id );

				//! \brief Creates a new BreakpointType ID to a breakpoint type.
				//! \param bType the breakpoint type
				//! \return the new BreakpointType ID
				uint32 createBreakpointTypeID( BreakpointType* bType );

				//! \brief Deregisters the given BreakpointType ID.
				//! \param id the ID
				void deregisterBreakpointTypeID( const uint32 id );

				//! \brief Creates a new BreakpointInstance ID to a breakpoint instance.
				//! \param bInstance the breakpoint instance
				//! \return the new BreakpointInstance ID
				uint32 createBreakpointInstanceID( BreakpointInstance* bInstance );

				//! \brief Deregisters the given BreakpointInstance ID.
				//! \param id the ID
				void deregisterBreakpointInstanceID( const uint32 id );

				//uint32 createWatchpointID( Watchpoint& w );
				//void deregisterWatchpointID( const uint32 id );

				//! \brief Creates a new VariableType ID to a variable type.
				//! \param v the variable type
				//! \return the new VariableType ID
				uint32 createVariableTypeID( VariableType* v );

				//! \brief Deregisters the given VariableType ID.
				//! \param id the ID
				void deregisterVariableTypeID( const uint32 id );

				//! \brief Sends a message via the server to the connected client.
				//!
				//! The stream will be delete automatically. Don't delete it on your own.
				//! \param stream	the stream containing the message
				//! \returns true if a client is connected and the message could be given correctly to the OutputHandler, and false otherwise
				bool sendMessage(IO::Stream* stream);

				//! \brief Informs the module that the connected client closed the connection.
				//!
				//! No break- or watchpoints points should report anymore.
				void onClientDisconnect();

				//! \brief Informs the module that a new client connected.
				//! \param newClientInfo	the info about the new connected client
				void onClientConnect(ClientInfo* newClientInfo);

				//! \brief Checks whether there a client is connected.
				//! \return true, if a client is connected, and false otherwise
				bool isClientConnected() const { return (mConnectedClient != NULL); };

				//! \brief Gets the clientInfo of the connected client.
				//! \return the clientInfo
				ClientInfo* getClientInfo() const { return mConnectedClient; };

				//! \brief Performs a single step.
				//! \return true, if a breakpoint was holding a thread, and false otherwise
				bool makeStep();

				//! \brief Releases the current breakpoint and continues.
				//! \return true, if a breakpoint was holding a thread, and false otherwise
				bool makeContinue();

				//! \brief Gets the function ID for a given RPC name, and gets the own FunctionRegistrar.
				//! \param rpcID	stores the functionID of the RPC
				//! \param funcRegistrar	stores the own FunctionRegistrar
				//! \param nameRPC	the name of the function that is searched
				//! \returns true, if the function is known, and false otherwise
				bool getRPC(Reflection::Function::FunctionID& rpcID, Reflection::Function::FunctionRegistrar*& funcRegistrar, const String& nameRPC);

				//! \brief Gets the connected socket.
				//! \param connectionID		the identifier of the client connected to the module
				//! \returns true, if the connected client was found, and false otherwise
				bool getConnection( uint32& connectionID );

				//! \brief Gets the library break flag.
				//! \return the library break flag
				bool getLibraryFlag() const { return mLibraryBreakFlag; };

				//! \brief Sets the library break flag to false.
				void disableLibraryFlag() { mLibraryBreakFlag = false; };


			//member
				String	mName;							//!< the name of the module
				uint32	mVersion;						//!< the version of the module
				uint32	mID;							//!< the ID of the module

				Server*		mServer;					//!< the server
				ClientInfo*		mConnectedClient;		//!< the client that is connected to the module

				HistoryList*		mHistoryList;		//!< the history

				Container::IDVector<ObjectGroup*>		mObjectGroupList;			//!< list of object groups
				Container::IDVector<ObjectType*>		mObjectTypeList;			//!< list of object types
				Container::IDVector<ObjectInstance*>	mObjectInstanceList;		//!< list of object instances

				Container::IDVector<BreakpointType*>		mBreakpointTypeList;	//!< list of breakpoint types
				Container::IDVector<BreakpointInstance*>	mBreakpointInstanceList;//!< list of breakpoint instances
				Container::IDVector<VariableType*>		mVariableTypeList;			//!< list of variable types

				Container::LookUp<String, uint32>	mObjGroupNameToID;				//!< mapping of object group names to object group IDs
				Container::LookUp<String, uint32>	mBreakpointTypeNameToID;		//!< mapping of breakpoint type names to breakpoint type IDs

				Threading::Mutex	mMutex;				//!< mutex synchronizing the calls to the module

				uint32		mHoldingObjectGroupID;		//!< the ID of the object group that is holding a breakpoint

				Reflection::Function::FunctionRegistrar		mFunctionRegistrar;		//!< helper class for rpc system

				bool		mIsAttached;				//!< flag indicating whether the module is attached

				bool		mLibraryBreakFlag;			//!< flag indicating whether the library is holding
			};

		}
	}
}
