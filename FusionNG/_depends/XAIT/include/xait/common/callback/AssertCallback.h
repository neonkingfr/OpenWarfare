// (C) xaitment GmbH 2006-2012

#pragma once 
#include <xait/common/memory/AllocatorCaptured.h>
#include <xait/common/SharedPtr.h>

namespace XAIT
{
	namespace Common
	{
		namespace Callback
		{
			class AssertCallback : public Memory::AllocatorCaptured, public ReferenceCounted
			{
			public:
				//! \brief constrtuctor
				AssertCallback()
				{}

				//! \brief constrtuctor
				//! \param dealloc		custom deallocator
				AssertCallback(Memory::AllocatorCaptured::MemoryDeallocator dealloc)
					: AllocatorCaptured(dealloc)
				{}
				
				typedef SharedPtr<AssertCallback,MemberReferenceCount< > >	Ptr;

				XAIT_COMMON_DLLAPI virtual void showAssert(const char* message , const char *fileName, unsigned lineNumber);
			};
		}
	}
}
