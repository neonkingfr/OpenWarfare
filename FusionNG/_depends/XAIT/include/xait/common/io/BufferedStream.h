// (C) xaitment GmbH 2006-2012

#pragma once 

#include <xait/common/io/Stream.h>

#define XAIT_BUFFEREDSTREAM_DEFAULT_BUFFSIZE		4096

namespace XAIT
{
	namespace Common
	{
		namespace IO
		{
			class BufferedStream : public Stream
			{
			public:
				XAIT_COMMON_DLLAPI BufferedStream(Stream* unbufferedStream, const uint32 bufferSize= XAIT_BUFFEREDSTREAM_DEFAULT_BUFFSIZE);
				XAIT_COMMON_DLLAPI BufferedStream(const Common::SharedPtr<Stream>& unbufferedStream, const uint32 bufferSize= XAIT_BUFFEREDSTREAM_DEFAULT_BUFFSIZE);
				XAIT_COMMON_DLLAPI virtual ~BufferedStream();

				virtual bool canRead() const;
				virtual bool canWrite() const;
				virtual bool canSeek() const;
				virtual bool isClosed() const;
				virtual uint64 getLength() const;
				virtual uint64 getPosition() const;
				virtual const Common::String& getName() const;
				virtual uint64 read(void* dstBuffer, const uint64 numBytes);
				virtual void write(const void* srcBuffer, const uint64 numBytes);
				virtual uint64 seek(const int64 offset, const SeekOrigin origin);
				virtual void flush();
				virtual void close();

				//! \brief clears the buffers to the underlying stream
				XAIT_COMMON_DLLAPI void clearBuffer();

				//! \brief release unbuffered stream and close this stream
				XAIT_COMMON_DLLAPI void releaseAndClose();

			private:
				Stream*				mUnbufferedStream;
				SharedPtr<Stream>	mStreamHolder;
				uint08*				mBuffer;
				uint32				mBufferSize;
				uint32				mBufferPos;
				uint32				mBufferEnd;
				bool				mReadMode;

				inline uint64 getBufferStartPos() const
				{
					if (mReadMode)
						return mUnbufferedStream->getPosition() - mBufferEnd;
					else
						return mUnbufferedStream->getPosition();
				}
			};
		}
	}
}

