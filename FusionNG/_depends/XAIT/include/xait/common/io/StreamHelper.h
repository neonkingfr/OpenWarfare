// (C) xaitment GmbH 2006-2012

#pragma once 

#include <xait/common/utility/StringConvert.h>

namespace XAIT
{
	namespace Common
	{
		namespace IO
		{

			template<typename T>
			inline void StreamWriteAsString(Stream* stream, const T& value)
			{
				Common::String tmp= Utility::StringConvert<T>::toString(value);
				stream->write(tmp.getConstCharPtr(),tmp.getLength());
			}

			template<typename T>
			inline void StreamWriteAsBinary(Stream* stream, const T& value)
			{
				stream->write(&value,sizeof(T));
			}

			template<typename CHAR_T>
			inline void StreamWriteAsBinary(Stream* stream, const Common::BasicString<CHAR_T>& value)
			{
				stream->write(value.getConstCharPtr(),value.getLength());
			}

		}
	}
}