// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/io/Stream.h>

namespace XAIT
{
	namespace Common
	{
		namespace IO
		{
			class SectionedStream : public Stream
			{
			public:
				//! \param fullStreamExclusive		if set to true, there is the assumption that the fullstream is not used by another sectionedstream in parallel.
				SectionedStream(const Common::String& name, Stream* fullStream, const uint64 start, const uint64 end, bool fullStreamExclusive);

				bool canRead() const { return mFullStream != NULL && mFullStream->canRead();}
				bool canWrite() const { return false; }
				bool canSeek() const { return mFullStream != NULL && mFullStream->canSeek();}
				bool isClosed() const { return mFullStream == NULL || mFullStream->isClosed(); }

				uint64 getLength() const { return mEnd - mStart; }
				uint64 getPosition() const { return mCurrPos - mStart; }
				const Common::String& getName() const { return mName; }

				uint64 read(void* dstBuffer, const uint64 numBytes);

				void write(const void* srcBuffer, const uint64 numBytes);

				uint64 seek(const int64 offset, const SeekOrigin origin);

				virtual void flush() { }; // read only stream
				
				void close()
				{
					mFullStream= NULL;
				}
				


			private:
				Common::String	mName;
				Stream*			mFullStream;
				uint64			mStart;
				uint64			mEnd;
				uint64			mCurrPos;		// current position in full stream
				bool			mFullStreamExclusive;
			};
		}
	}
}

