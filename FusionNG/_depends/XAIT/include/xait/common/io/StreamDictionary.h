// (C) xaitment GmbH 2006-2012

#pragma once 

#include <xait/common/memory/MemoryManaged.h>
#include <xait/common/io/Stream.h>


namespace XAIT
{
	namespace Common
	{
		namespace IO
		{
			class StreamDictionary : public Common::Memory::MemoryManaged
			{
			public:
				virtual ~StreamDictionary() {}
				virtual bool canAdd() const= 0;
				virtual bool canRemove() const= 0;
				virtual bool isClosed() const= 0;
				virtual bool isEmpty() const= 0;
				virtual const Common::String& getName() const= 0;

				virtual bool addStream(Stream* stream, const Common::String& name)= 0;
				virtual bool removeStream(const Common::String& name)= 0;
				virtual Stream* getStream(const Common::String& name)= 0;
				virtual bool containsStream(const Common::String& name) const= 0;
				virtual void getContent(Container::Vector<String>& content) const= 0;
				virtual void clear()= 0;
				virtual void close()= 0;

				inline bool addStream(Stream* stream)
				{
					return addStream(stream,stream->getName());
				}
			};
		}
	}
}
