// (C) xaitment GmbH 2006-2012

#pragma once 

#include <xait/common/exceptions/Exception.h>

namespace XAIT
{
	namespace Common
	{
		namespace IO
		{
			//! \brief base class for all exceptions in the namespace IO
			class IOException : public Exceptions::Exception
			{
			public:
				IOException(const Common::String& errorMsg)
					: Exception(errorMsg)
				{}

				virtual Common::String getExceptionName() const
				{
					return "IOException";
				}
			};

		}
	}
}



