// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/crypto/MD5Sum.h>
#include <xait/common/io/Stream.h>
#include <xait/common/memory/Allocator.h>
#include <xait/common/crypto/MD5Implementation.h>

namespace XAIT
{
	namespace Common
	{
		namespace Crypto
		{
			class MD5Builder
			{
			public:
				XAIT_COMMON_DLLAPI MD5Builder(const Memory::Allocator::Ptr& alloc);
				XAIT_COMMON_DLLAPI MD5Builder(const MD5Builder& builder);

				XAIT_COMMON_DLLAPI ~MD5Builder();

				//! \brief resets internal state to compute another checksum
				XAIT_COMMON_DLLAPI void reset();

				//! \brief adds a buffer to the md5 sum
				//! \param buffer		buffer with data to build into the md5 sum
				//! \param length		length of the buffer
				XAIT_COMMON_DLLAPI void add(const void* buffer, const uint32 length);

				//! \brief add a certain type of data to the md5 sum
				template<typename T>
				inline void add(const T& val)
				{
					add(&val,sizeof(T));
				}

				inline void add(const Common::String& val)
				{
					add(val.getConstCharPtr(),val.getLength());
				}

				//! \brief adds a stream to the md5 sum
				//! \param stream		stream with data to build into the md5 sum
				//! \param length		number of bytes to read from the stream, or -1 if the stream should be completly read
				XAIT_COMMON_DLLAPI bool addStream(IO::Stream* stream, const int64 length= -1);

				//! \brief add an array of elements from the same type to the md5 sum
				template<typename T>
				inline void addArray(const T* startPtr, const uint32 numElements)
				{
					add(startPtr,sizeof(T) * numElements);
				}

				//! \brief finally compute the MD5Sum
				XAIT_COMMON_DLLAPI MD5Sum computeSum();

			private:
				Memory::Allocator::Ptr	mAllocator;
				void*					mInternalBuffer;
				MD5Implementation		mMD5Implementation;				
			};
		}
	}
}


