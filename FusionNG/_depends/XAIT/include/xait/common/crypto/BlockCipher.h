// (C) xaitment GmbH 2006-2012

#pragma once 

#include <xait/common/FundamentalTypes.h>
#include <xait/common/memory/MemoryManaged.h>

namespace XAIT
{
	namespace Common
	{
		namespace Crypto
		{
			//! \brief base class for cipher algorithms which implement an block cipher
			class BlockCipher : public Memory::MemoryManaged
			{
			public:
				//! \brief base class constructor
				//! \param blockSize	number of bytes the cipher encryts at once
				BlockCipher(const uint32 blockSize)
					: mBlockSize(blockSize)
				{}

				//! \brief virtual destructor for base class
				virtual ~BlockCipher()
				{}
				
				//! \brief encrypt a block of data
				//! \param encData[out]		encrypted data (output)
				//! \param decData			clear text data
				//! \remark The method assumes that the two arrays have at least blockSize bytes allocated
				virtual void encryptBlock(uint08* encData, const uint08* decData)= 0;

				//! \brief decrypt a block of encrypted data
				//! \param decData[out]		decrypted data (output)
				//! \param encData			encrypted data
				//! \remark The method assumes that the two arrays have at least blockSize bytes allocated
				virtual void decryptBlock(uint08* decData, const uint08* encData)= 0;

				inline uint32 getBlockSize() const
				{
					return mBlockSize;
				}

			protected:
				uint32		mBlockSize;		//!< number of bytes this blockcipher can handle at once
			};
		}
	}
}


