// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/String.h>
#include <xait/common/FundamentalTypes.h>
#include <xait/common/memory/MemoryManaged.h>

#define MD5_CBLOCK	64
#define MD5_LBLOCK	(MD5_CBLOCK/4)
#define MD5_DIGEST_LENGTH 16

namespace XAIT
{
	namespace Common
	{
		namespace Crypto
		{
			class MD5Implementation
			{
			private:
				Memory::Allocator::Ptr mAllocator;

				void* mData;
				static const unsigned int mK[64];
				static const int mR[64];

				void addPadding(const void* data, size_t len);

				unsigned int leftRotate(unsigned int x, int c);

			public:
				struct MD5_CTX
				{
					unsigned int A,B,C,D;
					unsigned int Nl,Nh;
					unsigned int data[MD5_LBLOCK];
					unsigned int num;
				};

				MD5Implementation(Memory::Allocator::Ptr allocator)
					: mAllocator(allocator), mData(NULL)
				{
				}

				~MD5Implementation()
				{
					if (mData != NULL)
						XAIT_DELETE(mData, mAllocator);
				}

				unsigned int Init(MD5_CTX *c);

				int Update(MD5_CTX *ctx, const void *data, size_t len);

				int Final(unsigned char *md, MD5_CTX *ctx);
			};
		}
	}
}