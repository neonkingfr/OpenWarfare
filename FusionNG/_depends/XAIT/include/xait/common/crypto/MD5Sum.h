// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/FundamentalTypes.h>
#include <xait/common/String.h>

#define XAIT_MD5SUM_WRITE_STREAM(name,outStream,md5sum)		outStream->writeArray(name,(const int08*)md5sum.mComp,16)
#define XAIT_MD5SUM_READ_STREAM(namevar,inStream,md5sum)	inStream->readArray(namevar,(int08*)md5sum.mComp,16)


namespace XAIT
{
	namespace Common
	{
		namespace Crypto
		{
			struct MD5Sum
			{
				uint08	mComp[16];


				MD5Sum()
				{
					memset(mComp,0,16);
				}

				MD5Sum(const Common::String& str)
				{
					char parse[3];
					parse[2]= 0;
					for(uint32 i= 0; i < 16 && ((i * 2 + 1) < str.getLength()); ++i)
					{
						parse[0]= str[i * 2];
						parse[1]= str[i * 2 + 1];

						uint32 scanVal;
						sscanf_s(parse,"%x",&scanVal);
						mComp[i]= (uint08)scanVal;
					}
				}

				Common::String toString() const
				{
					char hex[64];
					sprintf_s(hex,64,"%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X"
						,(uint32)mComp[ 0],(uint32)mComp[ 1],(uint32)mComp[ 2],(uint32)mComp[ 3]
						,(uint32)mComp[ 4],(uint32)mComp[ 5],(uint32)mComp[ 6],(uint32)mComp[ 7]
						,(uint32)mComp[ 8],(uint32)mComp[ 9],(uint32)mComp[10],(uint32)mComp[11]
						,(uint32)mComp[12],(uint32)mComp[13],(uint32)mComp[14],(uint32)mComp[15]);
					return hex;
				}

				inline uint08 operator[](const uint32 byteNum) const
				{
					X_ASSERT_MSG_DBG(byteNum < 16,"byte number of out range");
					return mComp[byteNum];
				}

				inline bool operator==(const MD5Sum& sum) const
				{
					return memcmp(this,&sum,sizeof(MD5Sum)) == 0;
				}

				inline bool operator!=(const MD5Sum& sum) const
				{
					return memcmp(this,&sum,sizeof(MD5Sum)) != 0;
				}

				inline bool operator<(const MD5Sum& sum) const
				{
					return memcmp(this,&sum,sizeof(MD5Sum)) < 0;
				}

				inline bool operator>(const MD5Sum& sum) const
				{
					return memcmp(this,&sum,sizeof(MD5Sum)) > 0;
				}


			};
		}
	}

	inline std::ostream& operator<<(std::ostream& os,const Common::Crypto::MD5Sum &sum)
	{
		os << sum.toString();
		return os;
	}

}

