// (C) xaitment GmbH 2006-2012

#pragma once 
#include <xait/common/math/MathDefines.h>


namespace XAIT
{
	namespace Common
	{
		namespace Math
		{
			//! \brief Math class for representation of quaternions
			template<typename T_COMP>
			struct Quaternion
			{
				T_COMP		mW;
				T_COMP		mX;
				T_COMP		mY;
				T_COMP		mZ;

				//! \brief Create a quaternion by its components
				Quaternion(const T_COMP w, const T_COMP x, const T_COMP y, const T_COMP z)
					: mW(w),mX(x),mY(y),mZ(z)
				{}

				//! \brief Create a quaternion representing no rotation
				Quaternion()
					: mW(1),mX(0),mY(0),mZ(0)
				{}

				//! \brief normalize the quaternion
				void normalize()
				{
					T_COMP len= (mW * mW + mX * mX + mY * mY + mZ * mZ);
					len= Math::Sqrt(len);
					mW/= len;
					mX/= len;
					mY/= len;
					mZ/= len;
				}

				//! \brief check if the quaternion is normalized
				//! \remark This method is intend tobe used inside debug assertions
				bool isNormalized() const
				{
					T_COMP len= (mW * mW + mX * mX + mY * mY + mZ * mZ);
					return (len > 1 - X_EPSILON) && (len < 1 + X_EPSILON);
				}
			};

			typedef Quaternion<float32>		Quaternionf32;
			typedef Quaternion<float64>		Quaternionf64;
		}
	}
}
