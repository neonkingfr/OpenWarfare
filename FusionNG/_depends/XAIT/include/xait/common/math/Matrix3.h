// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/math/Vec3.h>
#include <xait/common/FundamentalTypes.h>


namespace XAIT
{
	namespace Common
	{
		namespace Math
		{
			template<typename T_COMP>
			struct Matrix3;

			typedef Matrix3<float32>		Matrix3f;		//!< always 32-bit
			typedef Matrix3<float64>		Matrix3d;		//!< always 64-bit

			//! Template for 3x3 row matrices. The matrix is organized in rows from left to right,
			//! so 0..2 -> row1, 3..5 -> row2 , 6..8 -> row3  (index starting at 0).
			//! \brief template for 3x3 matrices
			template<typename T_COMP>
			struct Matrix3
			{
				T_COMP		mMatrix[9];		//!< matrix values, 0..2 = one row and so on .. 3*3=9

				//! \brief default constructor
				//! \remark Sets identity matrix
				Matrix3()
				{
					setIdentity();
				}

				//! \brief init from another matrix
				//! \param m	copy from this matrix
				Matrix3(const Matrix3& m)
				{
					memcpy(mMatrix,m.mMatrix,sizeof(T_COMP) * 9);
				}

				//! \brief init from another matrix with another component type
				//! \param m	other matrix 
				template<typename OTHER_T_COMP>
				explicit Matrix3(const Matrix3<OTHER_T_COMP>& m)
				{
					for(uint32 i= 0; i < 9; ++i)
					{
						mMatrix[i]= (T_COMP)m.mMatrix[i];
					}
				}

				//! \brief get row y
				inline T_COMP* operator[](const uint32 y)
				{
					return &mMatrix[3*y];
				}

				//! \brief get row y
				inline const T_COMP* operator[](const uint32 y) const
				{
					return &mMatrix[3*y];
				}

				//! \brief multiply matrix with a column vector and return vector result
				inline Vec3<T_COMP> multiplyVector(const Vec3<T_COMP>& v) const
				{
					Vec3<T_COMP> erg;
					const T_COMP* mrow= &mMatrix[0];
					erg.mX= mrow[0] * v.mX + mrow[1] * v.mY + mrow[2] * v.mZ;
					mrow= &mMatrix[3];
					erg.mY= mrow[0] * v.mX + mrow[1] * v.mY + mrow[2] * v.mZ;
					mrow= &mMatrix[6];
					erg.mZ= mrow[0] * v.mX + mrow[1] * v.mY + mrow[2] * v.mZ;

					return erg;
				}

				//! \brief set this matrix to the identity matrix
				inline void setIdentity()
				{
					for(uint08 y= 0; y < 3; ++y) {
						for(uint08 x= 0; x < 3; ++x) {
							if (x == y) {
								(*this)[y][x]= 1;
							} else {
								(*this)[y][x]= 0;
							}
						}
					}
				}

				//! \brief compute the determinant of this matrix
				T_COMP computeDeterminant() const
				{
					T_COMP det= (*this)[0][0]*((*this)[1][1] * (*this)[2][2] - (*this)[1][2] * (*this)[2][1]) -
						(*this)[0][1]*((*this)[1][0] * (*this)[2][2] - (*this)[1][2] * (*this)[2][0]) +
						(*this)[0][2]*((*this)[1][0] * (*this)[2][1] - (*this)[1][1] * (*this)[2][0]);

					return det;
				}

				//! \brief compute the inverse matrix from this one
				//! \param inverse[out]		inverted matrix
				//! \returns true if the matrix could be inverted, or false if not (can only be inverted if determinat different from zero)
				bool computeInverse(Matrix3& invers) const
				{
					T_COMP det= computeDeterminant();
					if (det == 0.0f) return false;   // not invertible
					det= 1 / det;

					T_COMP *mrow= invers[0];
					mrow[0]= det * ((*this)[1][1] * (*this)[2][2] - (*this)[1][2] * (*this)[2][1]);
					mrow[1]= det * ((*this)[0][2] * (*this)[2][1] - (*this)[0][1] * (*this)[2][2]);
					mrow[2]= det * ((*this)[0][1] * (*this)[1][2] - (*this)[0][2] * (*this)[1][1]);

					mrow= invers[1];
					mrow[0]= det * ((*this)[1][2] * (*this)[2][0] - (*this)[1][0] * (*this)[2][2]);
					mrow[1]= det * ((*this)[0][0] * (*this)[2][2] - (*this)[0][2] * (*this)[2][0]);
					mrow[2]= det * ((*this)[0][2] * (*this)[1][0] - (*this)[0][0] * (*this)[1][2]);

					mrow= invers[2];
					mrow[0]= det * ((*this)[1][0] * (*this)[2][1] - (*this)[1][1] * (*this)[2][0]);
					mrow[1]= det * ((*this)[0][1] * (*this)[2][0] - (*this)[0][0] * (*this)[2][1]);
					mrow[2]= det * ((*this)[0][0] * (*this)[1][1] - (*this)[0][1] * (*this)[1][0]);

					return true;
				}

				//! \brief					computes the transposed matrix
				//! \param	transposed[out]	transposed matrix	
				void getTransposed(Matrix3& transposed)
				{
					for(uint08 y= 0; y < 3; ++y)
					{
						for(uint08 x= 0; x < 3; ++x)
						{
							transposed[x][y]= (*this)[y][x];
						}
					}
				}

				//! \brief print the matrix to trace tool
				//void showMatrix() const
				//{
				//	for(int32 y= 0; y < 3; ++y) {
				//		TRACE_MESSAGE("( " << (*this)[y][0] << " , " << (*this)[y][1] << " , " << (*this)[y][2] << " )")
				//	}
				//}
			};
		}	// namespace Math
	}	// namespace Common
}	// namespace XAIT
