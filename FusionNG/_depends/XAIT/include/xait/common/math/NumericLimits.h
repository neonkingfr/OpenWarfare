// (C) xaitment GmbH 2006-2012

#pragma once 

#include <limits>

#if XAIT_OS == XAIT_OS_WIN || XAIT_OS == XAIT_OS_XBOX360 || XAIT_OS == XAIT_OS_XBOX360EXT
#ifdef min
#pragma push_macro("min")
#undef min
#define XAIT_HAD_MIN
#endif

#ifdef max
#pragma push_macro("max")
#undef max
#define XAIT_HAD_MAX
#endif

#endif

namespace XAIT
{
	namespace Common
	{
		namespace Math
		{
			template<typename T>
			class NumericLimits
			{
			public:
				static T maxPosValue()
				{
					return std::numeric_limits<T>::max();
				}

				static T maxNegValue()
				{
					return (T)-1 * (std::numeric_limits<T>::max() - (T)1);
				}
			};
		}
	}
}


#ifdef XAIT_HAD_MIN
#pragma pop_macro("min")
#undef XAIT_HAD_MIN
#endif

#ifdef XAIT_HAD_MAX
#pragma pop_macro("max")
#undef XAIT_HAD_MAX
#endif

