// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/math/Vec2.h>
#include <xait/common/FundamentalTypes.h>


namespace XAIT
{
	namespace Common
	{
		namespace Math
		{
			template<typename T_COMP>
			struct Matrix2;

			typedef Matrix2<float32>		Matrix2f;		//!< always 32-bit
			typedef Matrix2<float64>		Matrix2d;		//!< always 64-bit

			//! Template for 3x3 row matrices. The matrix is organized in rows from left to right,
			//! so 0..2 -> row1, 3..5 -> row2 , 6..8 -> row3  (index starting at 0).
			//! \brief template for 3x3 matrices
			template<typename T_COMP>
			struct Matrix2
			{
				T_COMP		mMatrix[4];		//!< matrix values, 0..1 = one row and so on .. 2*2=4

				//! \brief default constructor
				//! \remark Sets identity matrix
				Matrix2()
				{
					setIdentity();
				}

				//! \brief init from another matrix
				//! \param m	copy from this matrix
				Matrix2(const Matrix2& m)
				{
					memcpy(mMatrix,m.mMatrix,sizeof(T_COMP) * 4);
				}

				//! \brief init from another matrix with another component type
				//! \param m	other matrix 
				template<typename OTHER_T_COMP>
				explicit Matrix2(const Matrix2<OTHER_T_COMP>& m)
				{
					for(uint32 i= 0; i < 4; ++i)
					{
						mMatrix[i]= (T_COMP)m.mMatrix[i];
					}
				}

				//! \brief get row y
				inline T_COMP* operator[](const uint32 y)
				{
					return &mMatrix[2*y];
				}

				//! \brief get row y
				inline const T_COMP* operator[](const uint32 y) const
				{
					return &mMatrix[2*y];
				}

				//! \brief multiply matrix with a column vector and return vector result
				inline Vec2<T_COMP> multiplyVector(const Vec2<T_COMP>& v) const
				{
					Vec2<T_COMP> erg;
					const T_COMP* mrow= &mMatrix[0];
					erg.mX= mrow[0] * v.mX + mrow[1] * v.mY;
					mrow= &mMatrix[2];
					erg.mY= mrow[0] * v.mX + mrow[1] * v.mY;
				
					return erg;
				}

				//! \brief set this matrix to the identity matrix
				inline void setIdentity()
				{
					(*this)[0][0]= 1;
					(*this)[1][1]= 1;
					(*this)[0][1]= 0;
					(*this)[1][0]= 0;
					
				}

				//! \brief compute the determinant of this matrix
				T_COMP computeDeterminant() const
				{
					T_COMP det= (*this)[0][0] * (*this)[1][1] - (*this)[0][1] * (*this)[1][0];

					return det;
				}

				//! \brief compute the inverse matrix from this one
				//! \param inverse[out]		inverted matrix
				//! \returns true if the matrix could be inverted, or false if not (can only be inverted if determinat different from zero)
				bool computeInverse(Matrix2& invers) const
				{
					T_COMP det= computeDeterminant();
					if (det == 0.0f) return false;   // not invertible
					det= 1 / det;

					T_COMP *mrow= invers[0];
					mrow[0]= det * ((*this)[1][1]);
					mrow[1]= det * (-(*this)[0][1]);
				
					mrow= invers[1];
					mrow[0]= det * (-(*this)[1][0]);
					mrow[1]= det * ((*this)[0][0]);
					
					return true;
				}

				//! \brief					computes the transposed matrix
				//! \param	transposed[out]	transposed matrix	
				void getTransposed(Matrix2& transposed)
				{
					for(uint08 y= 0; y < 2; ++y)
					{
						for(uint08 x= 0; x < 2; ++x)
						{
							transposed[x][y]= (*this)[y][x];
						}
					}
				}

				//! \brief print the matrix to trace tool
				//void showMatrix() const
				//{
				//	for(int32 y= 0; y < 3; ++y) {
				//		TRACE_MESSAGE("( " << (*this)[y][0] << " , " << (*this)[y][1] << " , " << (*this)[y][2] << " )")
				//	}
				//}
			};
		}	// namespace Math
	}	// namespace Common
}	// namespace XAIT
