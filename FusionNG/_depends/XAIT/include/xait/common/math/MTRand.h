// (C) xaitment GmbH 2006-2012

#ifndef MERSENNETWISTER_H
#define MERSENNETWISTER_H

//MersenneTwister.h
// Mersenne Twister random number generator -- a C++ class MTRand
// Based on code by Makoto Matsumoto, Takuji Nishimura, and Shawn Cokus
// Richard J. Wagner  v1.0  15 May 2003  rjwagner@writeme.com

// The Mersenne Twister is an algorithm for generating random numbers.  It
// was designed with consideration of the flaws in various other generators.
// The period, 2^19937-1, and the order of equidistribution, 623 dimensions,
// are far greater.  The generator is also fast; it avoids multiplication and
// division, and it benefits from caches and pipelines.  For more information
// see the inventors' web page at http://www.math.keio.ac.jp/~matumoto/emt.html

// Reference
// M. Matsumoto and T. Nishimura, "Mersenne Twister: A 623-Dimensionally
// Equidistributed Uniform Pseudo-Random Number Generator", ACM Transactions on
// Modeling and Computer Simulation, Vol. 8, No. 1, January 1998, pp 3-30.

// Copyright (C) 1997 - 2002, Makoto Matsumoto and Takuji Nishimura,
// Copyright (C) 2000 - 2003, Richard J. Wagner
// All rights reserved.                          
//
// Redistribution and use in source and binary forms, with or without
// modification, are permitted provided that the following conditions
// are met:
//
//   1. Redistributions of source code must retain the above copyright
//      notice, this list of conditions and the following disclaimer.
//
//   2. Redistributions in binary form must reproduce the above copyright
//      notice, this list of conditions and the following disclaimer in the
//      documentation and/or other materials provided with the distribution.
//
//   3. The names of its contributors may not be used to endorse or promote 
//      products derived from this software without specific prior written 
//      permission.
//
// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
// LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
// A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
// PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
// LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
// NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

// The original code included the following notice:
//
//     When you use this, send an email to: matumoto@math.keio.ac.jp
//     with an appropriate reference to your work.
//
// It would be nice to CC: rjwagner@writeme.com and Cokus@math.washington.edu
// when you write.

// Not thread safe (unless auto-initialization is avoided and each thread has
// its own MTRand object)

#include <iostream>
#include <limits.h>
#include <stdio.h>
#include <time.h>
#include <math.h>
#include <xait/common/FundamentalTypes.h>


namespace XAIT
{
	namespace Common
	{
		namespace Math
		{
			class MTRand
			{
				// Data
			public:
				//typedef unsigned long uint64;  // unsigned integer type, at least 32 bits

				enum { N = 624 };       // length of state vector
				enum { SAVE = N + 1 };  // length of array for save()

			protected:
				enum { M = 397 };  // period parameter

				uint64 state[N];   // internal state
				uint64 *pNext;     // next value to get from state
				int64 left;          // number of values left before reload needed


				//Methods
			public:
				MTRand( const uint64& oneSeed );  // initialize with a simple uint64
				MTRand( uint64 *const bigSeed, uint64 const seedLength = N );  // or an array
				MTRand();  // auto-initialize with /dev/urandom or time() and clock()

				// Do NOT use for CRYPTOGRAPHY without securely hashing several returned
				// values together, otherwise the generator state can be learned after
				// reading 624 consecutive values.

				// Access to 32-bit random numbers
				float64 rand();                          // real number in [0,1]
				float64 rand( const float64& n );         // real number in [0,n]
				float64 randExc();                       // real number in [0,1)
				float64 randExc( const float64& n );      // real number in [0,n)
				float64 randDblExc();                    // real number in (0,1)
				float64 randDblExc( const float64& n );   // real number in (0,n)
				uint64 randInt();                       // integer in [0,2^32-1]
				uint64 randInt( const uint64& n );      // integer in [0,n] for n < 2^32
				float64 operator()() { return rand(); }  // same as rand()

				// a random bool generator
				bool randBool();
				float32  randClamped();      // real number in [-1,1];
				uint64 randIntIntv(uint64 x, uint64 y);

				// Access to 53-bit random numbers (capacity of IEEE float64 precision)
				float64 rand53();  // real number in [0,1)

				// Access to nonuniform random number distributions
				float64 randNorm( const float64& mean = 0.0, const float64& variance = 0.0 );

				// Re-seeding functions with same behavior as initializers
				void seed( const uint64 oneSeed );
				void seed( uint64 *const bigSeed, const uint64 seedLength = N );
				void seed();

				// Saving and loading generator state
				void save( uint64* saveArray ) const;  // to array of size SAVE
				void load( uint64 *const loadArray );  // from such array
				friend std::ostream& operator<<( std::ostream& os, const MTRand& mtrand );
				friend std::istream& operator>>( std::istream& is, MTRand& mtrand );

			protected:
				void initialize( const uint64 oneSeed );
				void reload();
				inline uint64 hiBit( const uint64& u ) const { return u & 0x80000000UL; }
				inline uint64 loBit( const uint64& u ) const { return u & 0x00000001UL; }
				inline uint64 loBits( const uint64& u ) const { return u & 0x7fffffffUL; }
				inline uint64 mixBits( const uint64& u, const uint64& v ) const
				{ return hiBit(u) | loBits(v); }
				/*#pragma warning(disable : 4146)*/
				
				inline uint64 twist( const uint64& m, const uint64& s0, const uint64& s1 ) const
				{ return m ^ (mixBits(s0,s1)>>1) ^ (-(int64)(loBit(s1)) & 0x9908b0dfUL); }
				static uint64 hash( time_t t, clock_t c );
			};


			inline MTRand::MTRand( const uint64& oneSeed )
			{ seed(oneSeed); }

			inline MTRand::MTRand( uint64 *const bigSeed, const uint64 seedLength )
			{ seed(bigSeed,seedLength); }

			inline MTRand::MTRand()
			{ seed(); }

			inline float64 MTRand::rand()
			{ return float64(randInt()) * (1.0/4294967295.0); }

			inline float64 MTRand::rand( const float64& n )
			{ return rand() * n; }

			inline float64 MTRand::randExc()
			{ return float64(randInt()) * (1.0/4294967296.0); }

			inline float64 MTRand::randExc( const float64& n )
			{ return randExc() * n; }

			inline float64 MTRand::randDblExc()
			{ return ( float64(randInt()) + 0.5 ) * (1.0/4294967296.0); }

			inline float64 MTRand::randDblExc( const float64& n )
			{ return randDblExc() * n; }

			inline float64 MTRand::rand53()
			{
				uint64 a = randInt() >> 5, b = randInt() >> 6;
				return ( a * 67108864.0 + b ) * (1.0/9007199254740992.0);  // by Isaku Wada
			}

			inline float64 MTRand::randNorm( const float64& mean, const float64& variance )
			{
				// Return a real number from a normal (Gaussian) distribution with given
				// mean and variance by Box-Muller method
				float64 r = sqrt( -2.0 * log( 1.0-randDblExc()) ) * variance;
				float64 phi = 2.0 * 3.14159265358979323846264338328 * randExc();
				return mean + r * cos(phi);
			}

			inline uint64 MTRand::randInt()
			{
				// Pull a 32-bit integer from the generator state
				// Every other access function simply transforms the numbers extracted here

				if( left == 0 ) reload();
				--left;

				register uint64 s1;
				s1 = *pNext++;
				s1 ^= (s1 >> 11);
				s1 ^= (s1 <<  7) & 0x9d2c5680UL;
				s1 ^= (s1 << 15) & 0xefc60000UL;
				return ( s1 ^ (s1 >> 18) );
			}

			inline uint64 MTRand::randIntIntv(uint64 x, uint64 y)
			{
				return randInt()%(y-x+1)+x;
			}

			inline uint64 MTRand::randInt( const uint64& n )
			{
				// Find which bits are used in n
				// Optimized by Magnus Jonsson (magnus@smartelectronix.com)
				uint64 used = n;
				used |= used >> 1;
				used |= used >> 2;
				used |= used >> 4;
				used |= used >> 8;
				used |= used >> 16;

				// Draw numbers until one is found in [0,n]
				uint64 i;
				do
				i = randInt() & used;  // toss unused bits to shorten search
				while( i > n );
				return i;
			}

			inline bool MTRand::randBool()
			{
				if (randIntIntv(0,1)) 
					return true;
				else
					return false;
			}

			inline float32 MTRand::randClamped()
			{
				bool sign = randBool();

				if (sign)
					return (float32) rand();
				else 
					return (float32)(rand() * -1.0f);
			}



			inline void MTRand::seed( const uint64 oneSeed )
			{
				// Seed the generator with a simple uint64
				initialize(oneSeed);
				reload();
			}


			inline void MTRand::seed( uint64 *const bigSeed, const uint64 seedLength )
			{
				// Seed the generator with an array of uint64's
				// There are 2^19937-1 possible initial states.  This function allows
				// all of those to be accessed by providing at least 19937 bits (with a
				// default seed length of N = 624 uint64's).  Any bits above the lower 32
				// in each element are discarded.
				// Just call seed() if you want to get array from /dev/urandom
				initialize(19650218UL);
				register int i = 1;
				register uint64 j = 0;
				register uint64 k = ( (uint64)N > seedLength ? (uint64)N : seedLength );
				for( ; k; --k )
				{
					state[i] =
						state[i] ^ ( (state[i-1] ^ (state[i-1] >> 30)) * 1664525UL );
					state[i] += ( bigSeed[j] & 0xffffffffUL ) + j;
					state[i] &= 0xffffffffUL;
					++i;  ++j;
					if( i >= N ) { state[0] = state[N-1];  i = 1; }
					if( j >= seedLength ) j = 0;
				}
				for( k = N - 1; k; --k )
				{
					state[i] =
						state[i] ^ ( (state[i-1] ^ (state[i-1] >> 30)) * 1566083941UL );
					state[i] -= i;
					state[i] &= 0xffffffffUL;
					++i;
					if( i >= N ) { state[0] = state[N-1];  i = 1; }
				}
				state[0] = 0x80000000UL;  // MSB is 1, assuring non-zero initial array
				reload();
			}


			inline void MTRand::initialize( const uint64 seed )
			{
				// Initialize generator state with seed
				// See Knuth TAOCP Vol 2, 3rd Ed, p.106 for multiplier.
				// In previous versions, most significant bits (MSBs) of the seed affect
				// only MSBs of the state array.  Modified 9 Jan 2002 by Makoto Matsumoto.
				register uint64 *s = state;
				register uint64 *r = state;
				register int i = 1;
				*s++ = seed & 0xffffffffUL;
				for( ; i < N; ++i )
				{
					*s++ = ( 1812433253UL * ( *r ^ (*r >> 30) ) + i ) & 0xffffffffUL;
					r++;
				}
			}


			inline void MTRand::reload()
			{
				// Generate N new values in state
				// Made clearer and faster by Matthew Bellew (matthew.bellew@home.com)
				register uint64 *p = state;
				register int i;
				for( i = N - M; i--; ++p )
					*p = twist( p[M], p[0], p[1] );
				for( i = M; --i; ++p )
					*p = twist( p[M-N], p[0], p[1] );
				*p = twist( p[M-N], p[0], state[0] );

				left = N, pNext = state;
			}


			inline uint64 MTRand::hash( time_t t, clock_t c )
			{
				// Get a uint64 from t and c
				// Better than uint64(x) in case x is floating point in [0,1]
				// Based on code by Lawrence Kirby (fred@genesis.demon.co.uk)

				static uint64 differ = 0;  // guarantee time-based seeds will change

				uint64 h1 = 0;
				unsigned char *p = (unsigned char *) &t;
				for( size_t i = 0; i < sizeof(t); ++i )
				{
					h1 *= UCHAR_MAX + 2U;
					h1 += p[i];
				}
				uint64 h2 = 0;
				p = (unsigned char *) &c;
				for( size_t j = 0; j < sizeof(c); ++j )
				{
					h2 *= UCHAR_MAX + 2U;
					h2 += p[j];
				}
				return ( h1 + differ++ ) ^ h2;
			}


			inline void MTRand::save( uint64* saveArray ) const
			{
				register uint64 *sa = saveArray;
				register const uint64 *s = state;
				register int i = N;
				for( ; i--; *sa++ = *s++ ) {}
				*sa = left;
			}


			inline void MTRand::load( uint64 *const loadArray )
			{
				register uint64 *s = state;
				register uint64 *la = loadArray;
				register int i = N;
				for( ; i--; *s++ = *la++ ) {}
				left = *la;
				pNext = &state[N-left];
			}


			inline std::ostream& operator<<( std::ostream& os, const MTRand& mtrand )
			{
				register const uint64 *s = mtrand.state;
				register int i = mtrand.N;
				for( ; i--; os << *s++ << "\t" ) {}
				return os << mtrand.left;
			}


			inline std::istream& operator>>( std::istream& is, MTRand& mtrand )
			{
				register uint64 *s = mtrand.state;
				register int i = mtrand.N;
				for( ; i--; is >> *s++ ) {}
				is >> mtrand.left;
				mtrand.pNext = &mtrand.state[mtrand.N-mtrand.left];
				return is;
			}
		}
	}
}

#endif  // MERSENNETWISTER_H

