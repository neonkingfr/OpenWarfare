// (C) xaitment GmbH 2006-2012

#pragma once
#ifndef _XAIT_COMMON_MATH_VEC3F_H_
#define _XAIT_COMMON_MATH_VEC3F_H_

#include <xait/common/FundamentalTypes.h>
#include <xait/common/math/Vec2.h>
#include <xait/common/debug/Assert.h>
#include <xait/common/math/MathDefines.h>

#include <iostream>

// ---------------------------------------------------------------------------------------------------------
//							General Template Class for Vector with 3 components
//
//	Author:		Markus Wilhelm
//
// ---------------------------------------------------------------------------------------------------------


namespace XAIT
{
	namespace Common
	{
		namespace Math
		{
			template<typename T_COMP>
			struct Vec3;

			typedef Vec3<float32>		Vec3f;
			typedef Vec3<float64>		Vec3d;

			typedef Vec3<int32>			Vec3i32;
			typedef Vec3<int64>			Vec3i64;
			typedef Vec3<uint32>		Vec3u32;
			typedef Vec3<uint64>		Vec3u64;


			//! \brief template class for vectors with 3 components
			//! \param T_COMP	type of the components (must be a number type)
			template<typename T_COMP>
			struct Vec3
			{
				typedef T_COMP	ComponentType;

				T_COMP mX;		//!< x component
				T_COMP mY;		//!< y component
				T_COMP mZ;		//!< z component
				T_COMP mW;		//!< w component for memory alignment

				//! \brief default constructor
				//! \remark Does not init the vector (performance issue)
				Vec3()
				{}

				//! \brief Copy constructor. Init vector from a vector of equal component type.
				//! \param vec	copy from this vector
				Vec3(const Vec3& vec)
					: mX(vec.mX), mY(vec.mY), mZ(vec.mZ), mW(vec.mW)
				{}

				//! \brief creates a Vec3 from a Vec2 and a T_COMP value by inserting at comp
				template<typename T_OTHER_COMP>
				Vec3(const Vec2<T_OTHER_COMP>& vec,const uint32 comp,const T_OTHER_COMP value)
				{
					X_ASSERT_MSG_DBG(comp < 3,"component id out of range [0..2]");
					mW= (const T_COMP)0;

					switch (comp)
					{
					case 0:
						mX = (const T_COMP)value;
						mY = (const T_COMP)vec.mX;
						mZ = (const T_COMP)vec.mY;
						break;
					case 1:
						mX = (const T_COMP)vec.mX;
						mY = (const T_COMP)value;
						mZ = (const T_COMP)vec.mY;
						break;
					case 2:
						mX = (const T_COMP)vec.mX;
						mY = (const T_COMP)vec.mY;
						mZ = (const T_COMP)value;
						break;
					default:
						mX= (const T_COMP)0;
						mY= (const T_COMP)0;
						mZ= (const T_COMP)0;
						break;
					}
				}

				//! \brief Copy constructor. Init vector from a vector of another component type
				//! \param vec	copy from this vector
				template<typename T_OTHER_COMP>
				explicit Vec3(const Vec3<T_OTHER_COMP>& vec) 
					: mX((const T_COMP)vec.mX), mY((const T_COMP)vec.mY), mZ((const T_COMP)vec.mZ), mW((const T_COMP)vec.mW)
				{}

				//! \brief Init vector from single components
				//! \param x	x component
				//! \param y	y component
				//! \param z	z component
				Vec3(const T_COMP x, const T_COMP y, const T_COMP z)
					: mX(x),mY(y),mZ(z),mW((T_COMP)0)
				{}

				//! \brief get a component by number (0-x,1-y,2-z)
				//! \remark Not so fast as direct member access
				inline T_COMP& operator[](const uint32 comp)
				{
					return *(&mX + comp);
				}

				//! \brief get a component by number (0-x,1-y,2-z)
				//! \remark Not so fast as direct member access
				inline const T_COMP& operator[](const uint32 comp) const
				{
					return *(&mX + comp);
				}

				//! \brief set a component by number by number (0-x,1-y,2-z)
				inline void setComponent(const uint32 comp,const T_COMP& value)
				{
					operator[](comp) = value;
				}

				//! \brief get the length of the vector
				//! \remark Uses square root function. If only compares are need, use the faster getLength2.
				inline T_COMP getLength() const
				{
					return Sqrt(mX * mX + mY * mY + mZ * mZ);
				}

				//! \brief get the squared distance (faster if you need only comparison)
				inline T_COMP getLength2() const
				{
					return (mX * mX + mY * mY + mZ * mZ);
				}

				//! \brief Test if two vectors are equal
				//! \remark Vectors are equal if components are equal
				inline bool operator==(const Vec3& p) const
				{ 
					return (mX == p.mX && mY == p.mY && mZ == p.mZ); 
				}

				//! \brief Test if two vectors are unequal
				//! \remark Vectors are unequal if components are unequal
				inline bool operator!=(const Vec3& p) const
				{ 
					return (mX != p.mX || mY != p.mY || mZ != p.mZ); 
				}

				//! \brief Add this vector to another vector and return the result
				inline Vec3 operator+(const Vec3 &p) const
				{ 
					return Vec3(mX + p.mX,mY + p.mY,mZ + p.mZ);
				}

				//! \brief Substract a vector from this one and return the result
				inline Vec3 operator-(const Vec3& p) const
				{
					return Vec3(mX - p.mX,mY - p.mY,mZ - p.mZ);
				}

				//! \brief multiplies vector by *-operator with scalar 
				inline Vec3 operator*(const T_COMP v) const
				{
					return Vec3(mX * v,mY * v,mZ * v);
				}

				//! \brief multiplies component wise
				inline Vec3 operator*(const Vec3& v) const
				{
					return Vec3(mX * v.mX,mY * v.mY,mZ * v.mZ);
				}

				//! \brief Divide this vector by a scalar and return the result
				inline Vec3 operator/(const T_COMP v) const
				{
					return Vec3(mX / v,mY / v,mZ / v);
				}

				//! \brief Multiply this vector with a scalar
				inline Vec3& operator*=(const T_COMP s)
				{
					mX*= s;
					mY*= s;
					mZ*= s;
					return *this;
				}

				//! \brief Divide this vector by a scalar
				inline Vec3& operator/=(const T_COMP s)
				{
					mX/= s;
					mY/= s;
					mZ/= s;
					return *this;
				}

				//! \brief Add another vector to this vector
				inline Vec3& operator+=(const Vec3& v)
				{
					mX+= v.mX;
					mY+= v.mY;
					mZ+= v.mZ;
					return *this;
				}

				//! \brief Substract another vector from this vector
				inline Vec3& operator-=(const Vec3& v)
				{
					mX-= v.mX;
					mY-= v.mY;
					mZ-= v.mZ;
					return *this;
				}

				inline Vec3& operator=(const Vec3& v)
				{
					mX= v.mX;
					mY= v.mY;
					mZ= v.mZ;
					mW= v.mW;

					return *this;
				}

				//! \brief returns straight-line distance between two vectors
				inline T_COMP getDistance(const Vec3& p) const
				{
					const T_COMP x= p.mX - mX;
					const T_COMP y= p.mY - mY;
					const T_COMP z= p.mZ - mZ;
					
					return Sqrt(x * x + y * y + z * z);
				}

				//! \brief Get squared distance to another position
				//! \param position		position to which we want the distance
				//! \return The squared distance
				inline T_COMP getDistance2(const Vec3& p) const
				{
					const T_COMP x= p.mX - mX;
					const T_COMP y= p.mY - mY;
					const T_COMP z= p.mZ - mZ;

					return x * x + y * y + z * z;
				}

				//! \brief computes dotProduct of two vectors
				inline T_COMP dotProduct(const Vec3& p) const
				{
					return  mX*p.mX + mY*p.mY + mZ*p.mZ;
				}

				//! \brief computes crossProduct of two vectors
				inline Vec3 crossProduct(const Vec3& p) const 
				{
					return Vec3(mY * p.mZ - mZ * p.mY, mZ * p.mX - mX * p.mZ, mX * p.mY - mY * p.mX);
				}

				//! \brief normalizes a vector
				//! \returns the length of the vector
				//! \remark If the length is zero, vector will not be changed.
				inline T_COMP normalize()
				{
					T_COMP length = getLength();

					if (length == 0.0f) return 0.0f;

					T_COMP invLength= (T_COMP)1.0/length;
					mX *= invLength;
					mY *= invLength;
					mZ *= invLength;

					return length;
				}

				//! \brief fast normalize using inverse squareroot
				inline void fastNormalize()
				{
					float32 length2= getLength2();

					float32 invLength= InvSqrt(length2);
					mX*= invLength;
					mY*= invLength;
					mZ*= invLength;
				}

				//! \brief Test if a vector is normalized
				//! \returns True if the vector is normalized or false otherwise
				//! \remark The test encounters a small epsilon, so the vector could be of length 1.0001 or 0.9999 and still being handled as normalized.
				inline bool isNormalized() const
				{
					// we can use getLength2 for this test, since sqrt(1) =  1
					const T_COMP length= getLength2();

					return (length > (T_COMP)1.0 - (T_COMP)X_EPSILON) && (length < (T_COMP)1.0 + (T_COMP)X_EPSILON);
				}

				//! \brief test if this is a null vector
				inline bool isZero() const
				{
					const T_COMP length= getLength2();
					return length < (T_COMP)X_EPSILON;
				}

				//! \brief set new length for the vector
				inline void setLength(const T_COMP newlength)
				{
					normalize();
					mX *= newlength;
					mY *= newlength;
					mZ *= newlength;
				}

				//! \brief inverts the vector
				//! \remark this method makes only sense for signed number types
				inline void invert()
				{
					mX *= (T_COMP)-1;
					mY *= (T_COMP)-1;
					mZ *= (T_COMP)-1;
				}

				//! \brief get the absolute minimum component
				//! \returns the index of the minimum component
				inline int32 getMinComp() const
				{
					if (mX < mY)
					{
						if (mX < mZ) return 0; else return 2;
					}
					else
					{
						if (mY < mZ) return 1; else return 2;
					}
				}

				//! \brief get the absolute maximum component
				//! \returns the index of the maximum component
				inline int32 getMaxComp() const
				{
					if (mX > mY)
					{
						if (mX > mZ) return 0; else return 2;
					}
					else
					{
						if (mY > mZ) return 1; else return 2;
					}
				}

				//! \brief get the absolute maximum component
				//! \returns the index of the maximum component
				inline int32 getAbsMaxComp() const
				{
					if (Common::Math::Abs(mX) > Common::Math::Abs(mY))
					{
						if (Common::Math::Abs(mX) > Common::Math::Abs(mZ)) return 0; else return 2;
					}
					else
					{
						if (Common::Math::Abs(mY) > Common::Math::Abs(mZ)) return 1; else return 2;
					}
				}

				//! \brief Reduces the dimensions of the vector by eliminating one
				//! \param killCompID	component id which should be eliminated 
				//! \remark The components will stay in the order of Vec3.
				inline Vec2<T_COMP> reduceDimensions(const uint32 killCompID) const
				{
					X_ASSERT_MSG_DBG(killCompID < 3,"component id out of range [0..2]");

					switch (killCompID)
					{
					case 0:
						return Vec2<T_COMP>(mY,mZ);
					case 1:
						return Vec2<T_COMP>(mX,mZ);
					case 2:
						return Vec2<T_COMP>(mX,mY);
					};
						
					return Vec2<T_COMP>::ZERO;
				}

				// predefine some common vectors
				static const Vec3<T_COMP> ZERO;
				static const Vec3<T_COMP> UNIT_X;
				static const Vec3<T_COMP> UNIT_Y;
				static const Vec3<T_COMP> UNIT_Z;
				static const Vec3<T_COMP> NEGATIVE_UNIT_X;
				static const Vec3<T_COMP> NEGATIVE_UNIT_Y;
				static const Vec3<T_COMP> NEGATIVE_UNIT_Z;
				static const Vec3<T_COMP> UNIT_SCALE;
			};

			template <typename T_COMP> const Vec3<T_COMP> Vec3<T_COMP>::ZERO = Vec3<T_COMP>( 0, 0, 0);
			template <typename T_COMP> const Vec3<T_COMP> Vec3<T_COMP>::UNIT_X = Vec3<T_COMP>( 1, 0, 0);
			template <typename T_COMP> const Vec3<T_COMP> Vec3<T_COMP>::UNIT_Y = Vec3<T_COMP>( 0, 1, 0);
			template <typename T_COMP> const Vec3<T_COMP> Vec3<T_COMP>::UNIT_Z = Vec3<T_COMP>( 0, 0, 1);
			template <typename T_COMP> const Vec3<T_COMP> Vec3<T_COMP>::NEGATIVE_UNIT_X = Vec3<T_COMP>( -1,  0,  0 );
			template <typename T_COMP> const Vec3<T_COMP> Vec3<T_COMP>::NEGATIVE_UNIT_Y = Vec3<T_COMP>(  0, -1,  0 );
			template <typename T_COMP> const Vec3<T_COMP> Vec3<T_COMP>::NEGATIVE_UNIT_Z = Vec3<T_COMP>(  0,  0, -1 );
			template <typename T_COMP> const Vec3<T_COMP> Vec3<T_COMP>::UNIT_SCALE = Vec3<T_COMP>(1, 1, 1);
		}	// namespace Math
	}	// namespace Common


	template<typename COMP_T>
	inline std::ostream& operator<<(std::ostream& os,const Common::Math::Vec3<COMP_T> &v)
	{
		os << "(" << v.mX << "," << v.mY << "," << v.mZ << ")";
		return os;
	}

}

#endif

