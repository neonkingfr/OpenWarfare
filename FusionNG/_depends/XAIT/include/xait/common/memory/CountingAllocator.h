// (C) xaitment GmbH 2006-2012

#pragma once 

#include <xait/common/memory/Allocator.h>
#include <xait/common/exceptions/ArgumentNullException.h>
#include <xait/common/memory/OutOfMemoryException.h>
#include <xait/common/memory/ThreadSafeAllocWrapper.h>

//////////////////////////////////////////////////////////////////////////
///					Default Allocator
/// 
/// This allocator uses the new and delete functions for memory allocation.
//////////////////////////////////////////////////////////////////////////


namespace XAIT
{
	namespace Common
	{
		namespace Memory
		{
			class CountingAllocator : public Allocator
			{
			public:
				typedef Ptr::Rebind<CountingAllocator>::Other		Ptr;
				typedef ThreadSafeAllocWrapper<CountingAllocator>	ThreadSafe;

				CountingAllocator( const Allocator::Ptr& allocator)
					: Allocator("CountingAllocator")
					, mAllocatedMemory(0)
					, mMaxAllocMemory(0)
					, mNumAllocs(0)
					, mEnabled(true)
					, mAllocator(allocator)
				{}


				inline uint64 getAllocatedMemory() const
				{
					return mAllocatedMemory;
				}

				inline uint64 getMaxAllocatedMemory() const
				{
					return mMaxAllocMemory;
				}

				inline uint64 getNumAllocs() const
				{
					return mNumAllocs;
				}
				
				inline void reset()
				{
					mMaxAllocMemory= mAllocatedMemory;
					mNumAllocs= 0;
				}

				void setEnable(bool val)
				{
					mEnabled = val;
				}

			protected:
				void* allocInternal(const uint32 numBytes)
				{
					// increase size of block to hold the size of the block
					uint32* block= (uint32*)mAllocator->alloc(numBytes + 4);

					if (!block) 
						return NULL;

					*block= numBytes;
					mAllocatedMemory+= numBytes;
					if (mAllocatedMemory > mMaxAllocMemory)
						mMaxAllocMemory= mAllocatedMemory;
					
					if(mEnabled)
						++mNumAllocs;

					// move pointer to user block
					++block;
					return block;
				}

				void freeInternal(void* ptr)
				{
					uint32* block= (uint32*)ptr;

					// move to allocator area
					--block;

					mAllocatedMemory-= *block;

					mAllocator->free(block);
				}

			private:
				uint64			mAllocatedMemory;
				uint64			mMaxAllocMemory;
				uint64			mNumAllocs;
				bool			mEnabled;
				Allocator::Ptr	mAllocator;
			};			
		}
	}
}

