// (C) xaitment GmbH 2006-2012

#pragma once 

#include <xait/common/io/Stream.h>
#include <xait/common/container/Vector.h>

#define MEMORY_STREAM_DEFAULT_BLOCKSIZE		65535

namespace XAIT
{
	namespace Common	
	{
		namespace Memory
		{
			//! \brief a stream which writes/reads only in/from the memory
			//! \remark The allocator from the stream will be used for memory allocation.
			class MemoryStream : public IO::Stream
			{
			public:
				//! \brief construct a memory stream
				//! \param name			name of this memory stream, for later identification
				//! \param blockSize	size of new allocated blocks
				XAIT_COMMON_DLLAPI MemoryStream(const Common::String& name, const uint32 blockSize= MEMORY_STREAM_DEFAULT_BLOCKSIZE);

				//! \brief closes the stream on delete
				XAIT_COMMON_DLLAPI virtual ~MemoryStream();

				//! \brief open a memory stream, always R/W and new
				XAIT_COMMON_DLLAPI void open();

				// derived methods
				XAIT_COMMON_DLLAPI virtual const Common::String& getName() const;
				XAIT_COMMON_DLLAPI virtual bool canRead() const { return true; };
				XAIT_COMMON_DLLAPI virtual bool canWrite() const { return true; };
				XAIT_COMMON_DLLAPI virtual bool canSeek() const { return true; };
				XAIT_COMMON_DLLAPI virtual bool isClosed() const;
				XAIT_COMMON_DLLAPI virtual uint64 getLength() const;
				XAIT_COMMON_DLLAPI virtual uint64 getPosition() const;
				XAIT_COMMON_DLLAPI virtual uint64 read(void* dstBuffer, const uint64 numBytes);
				XAIT_COMMON_DLLAPI virtual void write(const void* srcBuffer, const uint64 numBytes);
				XAIT_COMMON_DLLAPI virtual uint64 seek(const int64 offset, const SeekOrigin origin);
				XAIT_COMMON_DLLAPI virtual void flush() {};
				XAIT_COMMON_DLLAPI virtual void close();
				
				//! \brief instead of close() and open() reduce the MemoryStream to the first allocated Block
				XAIT_COMMON_DLLAPI void reOpen();


			private:
				Container::Vector<uint08*>		mBuffer;
				uint32							mBlockSize;
				uint32							mCurrBlock;
				uint32							mCurrBlockPos;
				uint32							mEndBlockPos;	//!< last byte in the stream

				Common::String					mName;

			};
		}
	}
}
