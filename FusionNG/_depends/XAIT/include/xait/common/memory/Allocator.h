// (C) xaitment GmbH 2006-2012

#pragma once 
#ifndef _XAIT_COMMON_MEMORY_ALLOCATOR_H_	// this seems to be required for Radix 1.4.x on Wii - don't why pragma does not work here...
#define _XAIT_COMMON_MEMORY_ALLOCATOR_H_

#include <cstddef>
#include <new>
#include <xait/common/SharedPtr.h>

#define XAIT_CALL_DESTRUCTOR(ptr,type)	(ptr)->~type();
#define XAIT_NEW(allocator,classname)	new(allocator->alloc(sizeof(classname))) classname
#define XAIT_DELETE(ptr,allocator)		XAIT::Common::Memory::Delete(ptr,allocator)


#ifdef XAIT_ALLOCATOR_DEBUG
#include <xait/common/math/Rand.h>
#include <xait/common/threading/SpinLock.h>
#endif

namespace XAIT
{
	namespace Common
	{
		namespace Memory
		{
			//! \brief function calls the destructor of an object
			template<typename T>
			inline void CallDestructor(T* instance)
			{
				instance->~T();
			}

			//! \brief CallDestructor specialization for void pointer
			inline void CallDestructor(void*)
			{}


			//! \brief allocator basis class
			class Allocator : public LockedReferenceCounted
			{
			public:
				typedef SharedPtr<Allocator,LockedMemberReferenceCount<> >	Ptr;

				//! \brief constructor
				//! \param name		name of this allocator object, so that it can be identified later on (max. 16 chars including null termination)
				Allocator(const char* name)
					: mExtAlloc(mExtAlloc)
				{
					if (name)
					{
						const char* sPos= name;
						char* dPos= mName;
						uint32 cCount= 0;
						while(*sPos && cCount < 14)
						{
							*dPos= *sPos;
							++sPos;
							++dPos;
							++cCount;
						}
						*dPos= 0;
					}
					else
					{
						mName[0]= 0;
					}

#ifdef XAIT_ALLOCATOR_DEBUG
					mUniqueID= Math::Rand::getRandSecureUIntMax();
					mAllocCount= 0;
#endif
				}

				//! \brief copy constructor
				Allocator(const Allocator& copyFrom)
					: LockedReferenceCounted((const LockedReferenceCounted&)copyFrom),mExtAlloc(mExtAlloc)
				{
#ifdef XAIT_ALLOCATOR_DEBUG
					mUniqueID= Math::Rand::getRandSecureUIntMax();
					mAllocCount= 0;
#endif
				}

				virtual ~Allocator() 
				{
#ifdef XAIT_ALLOCATOR_DEBUG
#ifndef XAIT_USE_VLD
					X_ASSERT_MSG(mAllocCount == 0,"Allocator has '" << mAllocCount << "' blocks allocated, memory leak or wrong order of Allocator object -> allocated object deletion");
#endif
#endif
				};

				//! \brief allocate a memoryblock of a defined size of bytes
				//! \returns a pointer to the memory block
				//! \exception OutOfMemoryException		throws if no memory available
				inline void* alloc(const uint32 numBytes)
				{
#ifdef XAIT_ALLOCATOR_DEBUG
					void* block= allocInternal(numBytes + 4);

					if (block)
					{
						// increment alloc count
						mAllocLock.lock();
						++mAllocCount;
						mAllocLock.unlock();

						// sign block
						uint32* signedBlock= (uint32*)block;
						*signedBlock= mUniqueID;

						return ++signedBlock;
					}
					else
						return NULL;
#else
					return allocInternal(numBytes);
#endif
				}

				//! \brief free previously allocated memory
				//! \exception ArgumentNullException	throws if ptr is a null pointer
				inline void free(void* ptr)
				{
					if (!ptr)
						return;

#ifdef XAIT_ALLOCATOR_DEBUG
					// check allocation
					uint32* signedBlock= (uint32*)ptr;
					--signedBlock;
					const uint32 blockID= *signedBlock;
					X_ASSERT_MSG(blockID == mUniqueID,"Memory block has been allocated with a different allocator");

					// decrement alloccount
					mAllocLock.lock();
					--mAllocCount;
					mAllocLock.unlock();

					freeInternal(signedBlock);
#else
					freeInternal(ptr);
#endif
				}

				//! \brief check if allocator has freed all objects
				//! \returns true if all resources are freed
				virtual bool isClean() const
				{
					return true;
				}

				//! \brief check if the allocator is thread safe
				virtual bool isThreadSafe() const
				{
					return false;
				}

				//! \brief some allocators can release unused memory
				//! \brief returns amount of released memory
				inline virtual uint64 release()
				{
					return 0;
				}

				inline const char* getName()
				{
					return mName;
				}

				//! \brief overloaded new operator which can be used to allocate memory for this allocator from another one
				inline void* operator new(std::size_t numBytes, const Allocator::Ptr& allocator)
				{
					Allocator* obj= (Allocator*)allocator->alloc((uint32)numBytes);
					// use new operator for constructor init of the allocator object
					new ((void*)&(obj->mExtAlloc)) Allocator::Ptr(allocator);

					return obj;
				}

				//! \brief overloaded new operator if no other allocator is used to init this allocator
				inline void* operator new(std::size_t numBytes)
				{
					Allocator* obj= (Allocator*)::operator new(numBytes);

					// initialize shared pointer to the external allocator with NULL
					new (&(obj->mExtAlloc)) Allocator::Ptr();

					return obj;
				}

				inline void operator delete(void* ptr,const Allocator::Ptr& allocator)
				{
					allocator->free(ptr);
				}

				inline void operator delete(void* ptr)
				{
					Allocator* obj= (Allocator*)ptr;
					Memory::Allocator::Ptr alloc= obj->mExtAlloc;

					if (alloc != NULL)
					{
						// we used another allocator to init this allocator object
						alloc->free(ptr);

						// one extra call to the destructor, since we initialized the allocator shared pointer twice.
						// one time in the new operator, the second time in the constructor.
						// the first time it will be destructed inside the destructor
						Memory::CallDestructor(&alloc);
					}
					else
					{
						// use the normal delete operator
						::operator delete(ptr);
					}
				}

			protected:
				Memory::Allocator::Ptr		mExtAlloc;

				//! \brief allocate a memoryblock of a defined size of bytes
				//! \returns a pointer to the memory block
				//! \exception OutOfMemoryException		throws if no memory available
				virtual void* allocInternal(const uint32 numBytes)= 0;


				//! \brief free previously allocated memory
				//! \exception ArgumentNullException	throws if ptr is a null pointer
				virtual void freeInternal(void* ptr)= 0;

			private:
				char						mName[16];
#ifdef XAIT_ALLOCATOR_DEBUG
				uint32						mUniqueID;
				Threading::SpinLock			mAllocLock;
				volatile uint32				mAllocCount;
#endif
			};

			typedef Allocator::Ptr	AllocatorPtr;		//!< depreceated, do not USE !!

			//! \brief allocate a single element of type T and init it with val (copy construct)
			//! \param val			copy source
			//! \param allocator	allocator which should be used for new memory
			//! \returns a pointer to the new object
			//! \exception OutOfMemoryException		thrown if we are not able to allocate any memory
			template<typename T>
			inline T* New(const T& val, const Allocator::Ptr& allocator)
			{
				return new(allocator->alloc(sizeof(T))) T(val);
			}

			//! \brief allocate a single element of type T and use the default constructor for init
			//! \param allocator	allocator which should be used for new memory
			//! \returns a pointer to the new object
			//! \exception OutOfMemoryException		thrown if we are not able to allocate any memory
			template<typename T>
			inline T* New(const Allocator::Ptr& allocator, T* /*typeToAlloc*/)
			{
				return new(allocator->alloc(sizeof(T))) T;
			}

			template<typename T>
			inline T* New(const Allocator::Ptr& allocator)
			{
				return new(allocator->alloc(sizeof(T))) T;
			}

			//! \brief allocate a single element of type T
			//! \param allocator	allocator which should be used for new memory
			//! \returns a pointer to the new object
			//! \exception OutOfMemoryException		thrown if we are not able to allocate any memory
			template<typename T>
			inline T* Allocate(const Allocator::Ptr& allocator, T* /*typeToAlloc*/)
			{
				return (T*)allocator->alloc(sizeof(T));
			}

			template<typename T>
			inline T* Allocate(const Allocator::Ptr& allocator)
			{
				return (T*)allocator->alloc(sizeof(T));
			}

			template<typename T>
			inline T* AllocateArray(const Allocator::Ptr& allocator, const uint32 size, T*)
			{
				return (T*)allocator->alloc(sizeof(T) * size);
			}

			template<typename T>
			inline T* AllocateArray(const Allocator::Ptr& allocator, const uint32 size)
			{
				return (T*)allocator->alloc(sizeof(T) * size);
			}


			//! \brief delete an object and free the memory to the passed allocator
			//! \param ptr			pointer to the object we want to delete
			//! \param allocator	allocator object which should be used for freeing the memory
			//! \exception ArgumentNullException	thrown if the ptr is a NULL pointer
			template<typename T>
			inline void Delete(T* ptr, const Allocator::Ptr& allocator)
			{
				if (ptr)
				{
					CallDestructor(ptr);
					allocator->free(ptr);
				}
			}

			//! \brief allocate an array of elements, initialized with a the copy constructor
			//! \param val			copy source
			//! \param numElements	number of elements to allocate for the array
			//! \param allocator	allocator which should be used for new memory
			//! \returns a pointer to the new array object
			//! \exception OutOfMemoryException		thrown if we are not able to allocate any memory
			template<typename T>
			inline T* NewArray(const Allocator::Ptr& allocator, const uint32 numElements, const T& val= T())
			{
				T* memBlock= (T*)allocator->alloc(sizeof(T) * numElements);
				
				// initialize all elements
				T* iter= memBlock;
				for(uint32 i= 0; i < numElements; ++i)
				{
					new(iter) T(val);
					++iter;
				}

				return memBlock;
			}

			//! \brief delete an array of elements
			//! \param ptr			pointer to the array we should delete
			//! \param numElements	number of elements the array has
			//! \param allocator	allocator object which should be used for freeing the memory
			template<typename T>
			inline void DeleteArray(T* ptr, const uint32 numElements, const Allocator::Ptr& allocator)
			{
				if (ptr)
				{
					T* iter= ptr;
					for(uint32 i= 0; i < numElements; ++i)
					{
						CallDestructor(iter);
						++iter;
					}
					allocator->free(ptr);
				}
			}

			inline void Free(void* ptr, const Allocator::Ptr& allocator)
			{
				allocator->free(ptr);
			}

			template<typename T>
			inline void CallCopyConstructor(T& dst, const T& src)
			{
				new(&dst) T(src);
			}

			template<typename T>
			inline void CallDefaultConstructor(T& dst)
			{
				new(&dst) T;
			}
		}
	}
}


#define X_NEW(allocator,instance) new(allocator->alloc( sizeof instance )) instance
#define X_DELETE(allocator,instance) { XAIT::Common::Memory::CallDestructor(instance); allocator->free(instance); }

// we include the simple reference count this late, since the reference count is an default argument for the sharedptr
// and the simplereferencecount uses the allocator which has been just defined
#include <xait/common/SimpleReferenceCount.h>

#endif


