// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/CommonLog.h>

namespace XAIT
{
	namespace Common
	{
		namespace Memory
		{
			//! This base class is for objects which should travel above interfaces. The deallocator
			//! will be saved on create and will be used on delete.
			//! \brief captures the delete allocator during new which should be used for deallocation
			class AllocatorCaptured
			{
			public:
				typedef  void (XAIT_CDECL *MemoryDeallocator) (void* ptr);

				AllocatorCaptured()
					: mMemDealloc(::operator delete)
				{
					// we capture the delete operator so that we have later on a match between delete and new
				}

				AllocatorCaptured(const AllocatorCaptured&)
					: mMemDealloc(::operator delete)
				{
					// we capture the delete operator so that we have later on a match between delete and new
					// we do not copy it from the other object, since it could have used another new/delete pair
				}

				AllocatorCaptured(MemoryDeallocator dealloc)
					: mMemDealloc(dealloc)
				{

				}

				virtual ~AllocatorCaptured()
				{
				}

				void* operator new(std::size_t numBytes)
				{
					return (AllocatorCaptured*)::operator new(numBytes);
				}

				void operator delete(void* ptr)
				{
					AllocatorCaptured* obj= (AllocatorCaptured*)ptr;

					obj->mMemDealloc(ptr);
				}

			private:
				MemoryDeallocator	mMemDealloc;
			};
		}
	}
}
