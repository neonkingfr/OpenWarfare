// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/exceptions/Exception.h>

namespace XAIT
{
	namespace Common
	{
		namespace Memory
		{
			//! \brief exception for all memory related stuff
			class MemoryException : public Exceptions::Exception
			{
			public:
				MemoryException(const Common::String& errorMsg)
					: Exception(errorMsg)
				{}

				//! \brief get the name of the exception
				virtual Common::String getExceptionName() const
				{
					return "MemoryException";
				}

			};
		}
	}
}
