// (C) xaitment GmbH 2006-2012

#pragma once 

#include <xait/common/threading/SpinLock.h>

namespace XAIT
{
	namespace Common
	{
		namespace Memory
		{
			//! \brief Wrapper wraps parts of the allocator base class, so that the allocator gets thread safe
			//! \param T_ALLOC_WRAPPED	type of wrapped allocator
			//! \param N_SPINCOUNT		spincount used for lock
			template<typename T_ALLOC_WRAPPED, int N_SPINCOUNT= 4000>
			class ThreadSafeAllocWrapper : public T_ALLOC_WRAPPED
			{
			public:
				inline ThreadSafeAllocWrapper()
					: mAllocLock(N_SPINCOUNT)
				{

				}

				template<typename T_ARGUMENT>
				inline ThreadSafeAllocWrapper(T_ARGUMENT param1)
					: T_ALLOC_WRAPPED(param1)
					, mAllocLock(N_SPINCOUNT)
				{}
				
				
				inline virtual bool isThreadSafe() const
				{
					return true;
				}

			protected:
				void* allocInternal(const uint32 numBytes)
				{
					mAllocLock.lock();
					void* ret= T_ALLOC_WRAPPED::allocInternal(numBytes);
					mAllocLock.unlock();
					return ret;
				}


				void freeInternal(void* ptr)
				{
					mAllocLock.lock();
					T_ALLOC_WRAPPED::freeInternal(ptr);
					mAllocLock.unlock();
				}

			private:
				Threading::SpinLock		mAllocLock;
			};
		}
	}
}
