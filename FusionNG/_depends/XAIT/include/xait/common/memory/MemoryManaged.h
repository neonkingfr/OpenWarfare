// (C) xaitment GmbH 2006-2012

#pragma once 

#include <xait/common/Platform.h>
#include <xait/common/memory/Allocator.h>
#include <xait/common/debug/Assert.h>
#include <xait/common/DLLDefines.h>


namespace XAIT
{
	namespace Common	
	{
		namespace Memory
		{
			//! Base class for objects, being allocated with an external allocator.
			//! Use this base class only for objects, where each object will be created
			//! with new.
			//! \remark Objects derived from this class can only be created on the heap !!
			class MemoryManaged
			{
			public:
				MemoryManaged()
					: mAllocator(mAllocator)
				{
					// do nothing in the default constructor, since we do not want to mix up the 
					// allocator object instance on this object (which is already set through
					// the new operator)
				}

				//! \brief dummy constructor, which avoids initialization of the memory managed object
				//! \remarks can be used to initialize a memory managed object on the stack.
				MemoryManaged(const Common::Helper::IgnoreInit&)
					: mAllocator(NULL)
				{

				}

				MemoryManaged(const MemoryManaged&)
					: mAllocator(mAllocator)
				{
					// do nothing in the copy constructor, since we do not want to mix up the 
					// allocator object instance on this object (which is already set through
					// the new operator)
				}

				virtual ~MemoryManaged()
				{
					// Virtual destructor needed, since we want to init member variables in the new
					// operator. If we have no virtual destructor, we have no virtual function table
					// for the class MemoryManaged and thus we won't have the right member alignment
					// to assign the attribute mAllocator.
				}

				inline void* operator new(std::size_t numBytes, const Allocator::Ptr& allocator)
				{
					MemoryManaged* obj= (MemoryManaged*)allocator->alloc((uint32)numBytes);
					// use new operator for constructor init of the allocator object
					Allocator::Ptr* allocInitPtr= const_cast<Allocator::Ptr*>(&(obj->mAllocator));
					new (allocInitPtr) Allocator::Ptr(allocator);

					return obj;
				}

				inline void operator delete(void* ptr,const Allocator::Ptr& allocator)
				{
					allocator->free(ptr);
				}

				inline void operator delete(void* ptr)
				{
					MemoryManaged* obj= (MemoryManaged*)ptr;
					Memory::Allocator::Ptr alloc= obj->mAllocator;
					alloc->free(ptr);
					
					// one extra call to the destructor, since we initialized the allocator shared pointer twice
					Memory::CallDestructor(&alloc);
				}

				inline const Allocator::Ptr& getAllocator() const
				{
					return mAllocator;
				}

			protected:
				const Allocator::Ptr		mAllocator;		//!< allocator used to allocate this object

			private:
				// make normal new operator private
				inline void* operator new(std::size_t numBytes);
			};	// class MemoryManaged
		}	// namespace Memory
	}	// namespace Common
}	// namespace XAIT


