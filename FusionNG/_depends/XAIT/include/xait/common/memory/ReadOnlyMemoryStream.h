// (C) xaitment GmbH 2006-2012

#pragma once 

#include <xait/common/io/Stream.h>/*
#include <memory.h>*/

namespace XAIT
{
	namespace Common
	{
		namespace Memory
		{
			class ReadOnlyMemoryStream : public IO::Stream
			{
			public:
				ReadOnlyMemoryStream(const void* buffer, const uint32 bufferSize, const Common::String& name= Common::String())
					: mBuffer((uint08*)buffer)
					, mBufferSize(bufferSize)
					, mCurrPos(0)
					, mName(name)
				{}

				// derived methods
				virtual const Common::String& getName() const { return mName; }
				virtual bool canRead() const { return true; };
				virtual bool canWrite() const { return false; };
				virtual bool canSeek() const { return true; };
				virtual bool isClosed() const { return mBuffer == NULL; };
				virtual uint64 getLength() const { return mBufferSize; };
				virtual uint64 getPosition() const { return mCurrPos; };

				virtual uint64 read(void* dstBuffer, const uint64 numBytes)
				{
					const uint32 bytesToRead= Math::Min(mBufferSize - mCurrPos,(uint32)numBytes);
					memcpy(dstBuffer,mBuffer + mCurrPos,bytesToRead);
					mCurrPos+= bytesToRead;

					return bytesToRead;
				}

				virtual void write(const void* srcBuffer, const uint64 numBytes)
				{
					XAIT_FAIL("Read only stream");
				}

				virtual uint64 seek(const int64 offset, const SeekOrigin origin)
				{
					int64 newPos;
					switch (origin)
					{
					case SEEKORIG_CUR:
						newPos= mCurrPos + offset;
						break;
					case SEEKORIG_START:
						newPos= offset;
						break;
					case SEEKORIG_END:
						newPos= mBufferSize + offset;
						break;
					default:
						newPos= mCurrPos;
					}

					if (newPos < 0)
						newPos= 0;
					if (newPos > mBufferSize)
						newPos= mBufferSize;

					mCurrPos= (uint32)newPos;
					return mCurrPos;
				}

				virtual void flush() {};
				virtual void close()
				{
					mBuffer= NULL;
					mBufferSize= 0;
					mCurrPos= 0;
				}

			private:
				const uint08*	mBuffer;
				uint32			mBufferSize;
				uint32			mCurrPos;
				Common::String	mName;
			};
		}
	}
}

