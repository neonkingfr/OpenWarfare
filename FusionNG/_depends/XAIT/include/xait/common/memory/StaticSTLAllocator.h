// (C) xaitment GmbH 2006-2012

#pragma once 

#include <memory>
#include <xait/common/platform/RuntimeLib.h>
#include <xait/common/FundamentalTypes.h>
#include <xait/common/memory/Allocator.h>
#include <xait/common/memory/StaticMemoryManaged.h>

// we need these defines to get the stl port running with our StaticSTLAllocator
#ifndef _FARQ	/* specify standard memory model */
#define _FARQ
#define _PDFT	ptrdiff_t
#define _SIZT	size_t
#endif /* _FARQ */

namespace XAIT
{
	namespace Common
	{
		namespace Memory
		{
			// TEMPLATE CLASS allocator
			// generic allocator for objects of class _Ty
			template<class _Ty, GetAllocatorFuncType F_GETALLOCATOR>
			class StaticSTLAllocator
			{	
			public:
				typedef _Ty								value_type;
				typedef value_type _FARQ*				pointer;
				typedef value_type _FARQ&				reference;
				typedef const value_type _FARQ*			const_pointer;
				typedef const value_type _FARQ&			const_reference;

				typedef std::_SIZT size_type;
				typedef std::_PDFT difference_type;

				template<class _Other>
				struct rebind
				{	// convert an allocator<_Ty> to an allocator <_Other>
					typedef StaticSTLAllocator<_Other,F_GETALLOCATOR> other;
				};

				pointer address(reference _Val) const
				{	// return address of mutable _Val
					return (&_Val);
				}

				const_pointer address(const_reference _Val) const
				{	// return address of nonmutable _Val
					return (&_Val);
				}

				StaticSTLAllocator()
				{}

				StaticSTLAllocator(const Memory::Allocator::Ptr&) _THROW0()
				{	
				}

				StaticSTLAllocator(const StaticSTLAllocator&) _THROW0()
				{	
				}

				~StaticSTLAllocator()
				{

				}

				template<class _Other>
				StaticSTLAllocator(const StaticSTLAllocator<_Other,F_GETALLOCATOR>& other) _THROW0()
				{	
				}

				template<class _Other>
				StaticSTLAllocator<_Ty,F_GETALLOCATOR>& operator=(const StaticSTLAllocator<_Other,F_GETALLOCATOR>& other)
				{	
					return (*this);
				}

				void deallocate(pointer _Ptr, size_type)
				{	
					F_GETALLOCATOR()->free(_Ptr);
				}

				pointer allocate(size_type _Count)
				{	// allocate array of _Count elements
					return (pointer)F_GETALLOCATOR()->alloc((uint32)(_Count * sizeof(value_type)));
				}

				pointer allocate(size_type _Count, const void _FARQ *)
				{	// allocate array of _Count elements, ignore hint
					return (allocate(_Count));
				}

				void construct(pointer _Ptr, const _Ty& _Val)
				{	// construct object at _Ptr with value _Val
					//std::_Construct(_Ptr, _Val);
					new (_Ptr) _Ty(_Val);
				}

				void destroy(pointer _Ptr)
				{	// destroy object at _Ptr
					CallDestructor(_Ptr);
				}

				size_type max_size() const _THROW0()
				{	// estimate maximum array size
					size_type _Count = (size_type)(-1) / sizeof (_Ty);
					return (0 < _Count ? _Count : 1);
				}

				template<typename T_OTHER>
				bool operator==(const StaticSTLAllocator<T_OTHER,F_GETALLOCATOR>& other) const
				{
					return true;
				}

				template<typename T_OTHER>
				bool operator!=(const StaticSTLAllocator<T_OTHER,F_GETALLOCATOR>& other) const
				{
					return false;
				}

				const Allocator::Ptr& getMemoryAlloc() const
				{
					return F_GETALLOCATOR();
				}

			protected:
				template<typename T_OTHER,GetAllocatorFuncType F_GETALLOCATOR_OTHER>
				friend class StaticSTLAllocator;
			};	// class StaticSTLAllocator
		}	// namespace Memory
	}	// namespace Common
}	// namespace XAIT




