// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/Library.h>
#include <xait/common/event/Delegate.h>

namespace XAIT
{
	namespace Common
	{
		namespace Event
		{
			template<typename T_DELEGATE>
			class EventSource;

			template<typename T_DELEGATE>
			class EventBinder
			{
				template<typename T>
				friend class EventSource;

				EventBinder*	mPrev;
				EventBinder*	mNext; 
				T_DELEGATE		mDelegate;

				EventBinder(const T_DELEGATE& d)
					: mPrev(NULL)
					, mNext(NULL)
					, mDelegate(d)
				{}
			};

			//! This class is used to emit an event across serveral registered event handling delegates. The type of the
			//! delegate is specified in the template argument of the EventSource class. After creation of an event source
			//! you can register delegates at the event by adding them with the add method or += operator.
			//! i.e.
			//!
			//! typedef Delegate<void (int,int)>  SomeDelegate;
			//!
			//! EventSource<SomeDelegate> event;
			//! 
			//! event+= SomeDelegate::fromMethod<Class,&Class::Method>(classInstance);
			//!
			//! Now you can send events by calling the emit function:
			//!
			//! event.emit(SomeDelegate::InvokerType(0,5));
			//!
			//! The InvokerType is a compressed form of the parameters used for the delegate.
			//! In order to remove the event handler from the event, you can use the remove method or -= operator.
			//! This looks then like:
			//!
			//! event-= SomeDelegate::fromMethod<Class,&Class::Method>(classInstance);
			//!
			//! All add methods assume, that you register only one method of a class instance to a certain event. This is normally
			//! no restriction, since you don't know in which order event handlers are executed and therefore you should never
			//! register to methods of the same class instance to the same event.
			//! If there is not defined XAIT_EVENT_FUNCPTR_COMPARE, there is some extra handling for static/global functions required. 
			//! You can register static/global function like member functions with the add method or += operator, but you have to store the 
			//! return value of these methods. The return value uniquely identifies the listener in the event list since this cannot be done 
			//! automatically for static/global functions. This identifier can then be used to remove the listener from the event list.
			//! i.e.
			//! // add 
			//! EventSource<SomeDelegate>::ListenerID listenerID= event+= SomeDelegate::fromFunction<GlobalFunction>();
			//!
			//! // remove
			//! event-= listenerID;
			//!
			//! This will fail if XAIT_EVENT_FUNCPTR_COMPARE is not defined:
			//! event-= SomeDelegate::fromFunction<GlobalFunction>();
			//!
			//! \brief Source for emitting events. Maintains a list of delegates which will be called to emit an event
			template<typename T_DELEGATE>
			class EventSource
			{
			public:
				typedef T_DELEGATE					DelegateType;
				typedef EventBinder<T_DELEGATE>		EventBinderType;
				typedef EventBinderType*			ListenerID;

				EventSource()
					: mHead(NULL)
				{}

				~EventSource()
				{
					removeAllListener();
				}

				//! \brief add a new listener to the event
				//! \returns An ListenerID which can be used to remove the listener from the event queue. See notes on Event class.
				ListenerID add(const T_DELEGATE& listener)
				{
					EventBinderType* eventBinder= XAIT_NEW(Common::Library::getTempAlloc(),EventBinderType)(listener);

					// add new eventbinder to list
					eventBinder->mNext= mHead;
					if (mHead)
						mHead->mPrev= eventBinder;

					mHead= eventBinder;

					return eventBinder;
				}

				//! \brief remove a listener from the event
				//! \remarks Fails if the listener specified was not registered
				void remove(const T_DELEGATE& listener)
				{
#ifdef XAIT_EVENT_FUNCPTR_COMPARE
					for(EventBinderType* iter= mHead; iter != NULL; iter= iter->mNext)
					{
						if (iter->mDelegate == listener)
						{
							unlinkEventBinder(iter);
							return;
						}
					}
#else
					const void* objectPtr= listener.getObjectPtr();

					// this function only works for non-function delegates
					X_ASSERT_MSG(objectPtr,"You cannot use operator-=(const T_DELEGATE&) for function delegates, use the operator-=(EventBinderType*) !");

					// find binder element by searching an event binder that has the same object
					// as the provided delegate. This assumes that no two different methods from
					// one object register for the same event
					for(EventBinderType* iter= mHead; iter != NULL; iter= iter->mNext)
					{
						if (iter->mDelegate.getObjectPtr() == objectPtr)
						{
							unlinkEventBinder(iter);
							return;
						}
					}
#endif

					XAIT_FAIL("No such delegate registered for this event !");
				}

				//! \brief remove a listener from the event
				//! \remarks Fails if the listener specified was not registered
				inline void remove(ListenerID listenerID)
				{
					unlinkEventBinder(listenerID);
				}

				//! \brief add a new listener to the event
				//! \returns An ListenerID which can be used to remove the listener from the event queue. See notes on Event class.
				inline ListenerID operator+=(const T_DELEGATE& listener)
				{
					return add(listener);
				}

				//! \brief remove a listener from the event
				//! \remarks Fails if the listener specified was not registered				
				inline void operator-=(const T_DELEGATE& listener)
				{
					remove(listener);
				}

				//! \brief remove a listener from the event
				//! \remarks Fails if the listener specified was not registered
				inline void operator-=(ListenerID listenerID)
				{
					remove(listenerID);
				}

				//! \brief Emit an event to all registered listeners.
				void emit(const typename T_DELEGATE::InvokerType& invoker) const
				{
					EventBinderType* next;
					for(EventBinderType* iter= mHead; iter != NULL;)
					{
						next= iter->mNext;

						// event may deregister itself, therefore we store already the next element
						invoker(iter->mDelegate);

						iter= next;
					}
				}

				//! \brief remove all listeners from the event queue
				void removeAllListener()
				{
					EventBinderType* current;
					for(EventBinderType* iter= mHead; iter != NULL;)
					{
						current= iter;
						iter= iter->mNext;

						deleteEventBinder(current);
					}
					mHead= NULL;
				}

				//! \brief checks if a listener exists
				inline bool hasListener() const
				{
					return mHead != NULL;
				}

			private:
				EventBinderType* mHead;

				void unlinkEventBinder(EventBinderType* binder)
				{
					// remove binder from double linked list
					if (binder->mPrev)
						binder->mPrev->mNext= binder->mNext;
					if (binder->mNext)
						binder->mNext->mPrev= binder->mPrev;
					if (binder == mHead)
						mHead= binder->mNext;

					deleteEventBinder(binder);

				}

				void deleteEventBinder(EventBinderType* binder)
				{
					XAIT_DELETE(binder,Common::Library::getTempAlloc());
				}
			};

		}
	}
}
