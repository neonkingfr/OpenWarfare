// (C) xaitment GmbH 2006-2012

/*
(c) Sergey Ryazanov (http://home.onego.ru/~ryazanov)

Template file. May be included many times with different predefined macros.

Xaitment: Changed namespace and classnames to fit coding conventions
*/
#if SRUTIL_DELEGATE_PARAM_COUNT > 0
#define SRUTIL_DELEGATE_SEPARATOR ,
#else
#define SRUTIL_DELEGATE_SEPARATOR
#endif

// see BOOST_JOIN for explanation
#define SRUTIL_DELEGATE_JOIN_MACRO( X, Y) SRUTIL_DELEGATE_DO_JOIN( X, Y )
#define SRUTIL_DELEGATE_DO_JOIN( X, Y ) SRUTIL_DELEGATE_DO_JOIN2(X,Y)
#define SRUTIL_DELEGATE_DO_JOIN2( X, Y ) X##Y

namespace XAIT
{
	namespace Common
	{
		namespace Event
		{
#define SRUTIL_DELEGATE_CLASS_NAME SRUTIL_DELEGATE_JOIN_MACRO(_Delegate,SRUTIL_DELEGATE_PARAM_COUNT)
#define SRUTIL_DELEGATE_INVOKER_CLASS_NAME SRUTIL_DELEGATE_JOIN_MACRO(_Invoker,SRUTIL_DELEGATE_PARAM_COUNT)
			//template <typename R SRUTIL_DELEGATE_SEPARATOR SRUTIL_DELEGATE_TEMPLATE_PARAMS>
			//class SRUTIL_DELEGATE_INVOKER_CLASS_NAME;

			template <typename R SRUTIL_DELEGATE_SEPARATOR SRUTIL_DELEGATE_TEMPLATE_PARAMS, typename T_DELEGATE>
			class SRUTIL_DELEGATE_CLASS_NAME
			{
			public:
				typedef R return_type;
				typedef Invoker<R (SRUTIL_DELEGATE_TEMPLATE_ARGS)> InvokerType;

				SRUTIL_DELEGATE_CLASS_NAME()
					: object_ptr(0)
					, stub_ptr(0)
				{}

				template <return_type (*TMethod)(SRUTIL_DELEGATE_TEMPLATE_ARGS)>
				static T_DELEGATE fromFunction()
				{
					return fromStub(0, &functionStub<TMethod>);
				}

				template <class T, return_type (T::*TMethod)(SRUTIL_DELEGATE_TEMPLATE_ARGS)>
				static T_DELEGATE fromMethod(T* object_ptr)
				{
					X_ASSERT_MSG(object_ptr,"You cannot create a delegate to a null pointer object");
					return fromStub(object_ptr, &methodStub<T, TMethod>);
				}

				template <class T, return_type (T::*TMethod)(SRUTIL_DELEGATE_TEMPLATE_ARGS) const>
				static T_DELEGATE fromConstMethod(T const* object_ptr)
				{
					X_ASSERT_MSG(object_ptr,"You cannot create a delegate to a null pointer object");
					return fromStub(const_cast<T*>(object_ptr), &constMethodStub<T, TMethod>);
				}

				return_type operator()(SRUTIL_DELEGATE_PARAMS) const
				{
					return (*stub_ptr)(object_ptr SRUTIL_DELEGATE_SEPARATOR SRUTIL_DELEGATE_ARGS);
				}

				operator bool () const
				{
					return stub_ptr != 0;
				}

				bool operator!() const
				{
					return !(operator bool());
				}

				inline const void* getObjectPtr() const
				{
					return object_ptr;
				}

			private:

				typedef return_type (SRUTIL_DELEGATE_CALLTYPE *StubType)(void* object_ptr SRUTIL_DELEGATE_SEPARATOR SRUTIL_DELEGATE_PARAMS);

				void* object_ptr;
				StubType stub_ptr;

				static T_DELEGATE fromStub(void* object_ptr, StubType stub_ptr)
				{
					T_DELEGATE d;
					d.object_ptr = object_ptr;
					d.stub_ptr = stub_ptr;
					return d;
				}

				inline void assignStub(void* _object_ptr, StubType _stub_ptr)
				{
					object_ptr= _object_ptr;
					stub_ptr= _stub_ptr;
				}

				template <return_type (*TMethod)(SRUTIL_DELEGATE_TEMPLATE_ARGS)>
				static return_type SRUTIL_DELEGATE_CALLTYPE functionStub(void* SRUTIL_DELEGATE_SEPARATOR SRUTIL_DELEGATE_PARAMS)
				{
					return (TMethod)(SRUTIL_DELEGATE_ARGS);
				}

				template <class T, return_type (T::*TMethod)(SRUTIL_DELEGATE_TEMPLATE_ARGS)>
				static return_type SRUTIL_DELEGATE_CALLTYPE methodStub(void* object_ptr SRUTIL_DELEGATE_SEPARATOR SRUTIL_DELEGATE_PARAMS)
				{
					T* p = static_cast<T*>(object_ptr);
					return (p->*TMethod)(SRUTIL_DELEGATE_ARGS);
				}

				template <class T, return_type (T::*TMethod)(SRUTIL_DELEGATE_TEMPLATE_ARGS) const>
				static return_type SRUTIL_DELEGATE_CALLTYPE constMethodStub(void* object_ptr SRUTIL_DELEGATE_SEPARATOR SRUTIL_DELEGATE_PARAMS)
				{
					T const* p = static_cast<T*>(object_ptr);
					return (p->*TMethod)(SRUTIL_DELEGATE_ARGS);
				}
			};

			// prefered syntax, modeled with compiler hint
			template<typename R SRUTIL_DELEGATE_SEPARATOR SRUTIL_DELEGATE_TEMPLATE_PARAMS>
			class Delegate<R (SRUTIL_DELEGATE_TEMPLATE_ARGS)> : public SRUTIL_DELEGATE_CLASS_NAME<R SRUTIL_DELEGATE_SEPARATOR SRUTIL_DELEGATE_TEMPLATE_ARGS, Delegate<R (SRUTIL_DELEGATE_TEMPLATE_ARGS)> >
			{
			};

			template <typename R SRUTIL_DELEGATE_SEPARATOR SRUTIL_DELEGATE_TEMPLATE_PARAMS>
			class SRUTIL_DELEGATE_INVOKER_CLASS_NAME
			{
				SRUTIL_DELEGATE_INVOKER_DATA

			public:
				SRUTIL_DELEGATE_INVOKER_CLASS_NAME(SRUTIL_DELEGATE_PARAMS)
#if SRUTIL_DELEGATE_PARAM_COUNT > 0
					:
#endif
				SRUTIL_DELEGATE_INVOKER_INITIALIZATION_LIST
				{
				}

				template <class TDelegate>
				R operator()(TDelegate d) const
				{
					return d(SRUTIL_DELEGATE_ARGS);
				}
			};

			// prefered syntax, modeled with compiler hint
			template<typename R SRUTIL_DELEGATE_SEPARATOR SRUTIL_DELEGATE_TEMPLATE_PARAMS>
			class Invoker<R (SRUTIL_DELEGATE_TEMPLATE_ARGS)> : public SRUTIL_DELEGATE_INVOKER_CLASS_NAME<R SRUTIL_DELEGATE_SEPARATOR SRUTIL_DELEGATE_TEMPLATE_ARGS>
			{
			public:
				Invoker(SRUTIL_DELEGATE_PARAMS)
					: SRUTIL_DELEGATE_INVOKER_CLASS_NAME<R SRUTIL_DELEGATE_SEPARATOR SRUTIL_DELEGATE_TEMPLATE_ARGS>(SRUTIL_DELEGATE_ARGS)
				{
				}
			};


		}
	}
}


#undef SRUTIL_DELEGATE_CLASS_NAME
#undef SRUTIL_DELEGATE_SEPARATOR
#undef SRUTIL_DELEGATE_JOIN_MACRO
#undef SRUTIL_DELEGATE_DO_JOIN
#undef SRUTIL_DELEGATE_DO_JOIN2
