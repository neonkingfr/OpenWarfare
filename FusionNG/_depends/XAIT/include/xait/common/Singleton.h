// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/debug/Assert.h>
#include <xait/common/memory/MemoryManaged.h>


namespace XAIT
{
	namespace Common
	{
		//! \brief a singleton base class template
		//! \remark declare this class as a friend in derived classes!!
		template<typename T>
		class Singleton : public Memory::MemoryManaged
		{
		public:
			//! \brief get the instance of the singleton
			static inline T* getInstance()
			{
				return mInstance;
			}

			//! \brief create the instance of the singleton with a already created instance
			static void createInstance(T* instance)
			{
				if (mInstance)
				{
					XAIT_FAIL("there is already an instance of this singleton object");
					return;
				}
				mInstance= instance;
			}

			//! \brief create instance with default constructor
			//! \remark Works only if the class has a default constructor without parameters
			static void createInstance(const Memory::Allocator::Ptr& allocator)
			{
				if (mInstance)
				{
					XAIT_FAIL("there is already an instance of this singleton object");
					return;
				}
				mInstance= new(allocator) T();
			}

			//! \brief destroy the instance of the singleton
			static void destroyInstance()
			{
				if (mInstance) delete mInstance;
				mInstance= NULL;
			}

		private:

			static T*		mInstance;		//!< instance of the singleton

		protected:

			//! \brief constructor made protected, use getInstance instead
			Singleton()
			{}

			//! \brief destructor made protected, use destroyInstance() instead
			~Singleton()
			{}
		};

		//! \brief static class member initialization.
		template <typename T> T* Singleton<T>::mInstance = NULL;

	}

}
