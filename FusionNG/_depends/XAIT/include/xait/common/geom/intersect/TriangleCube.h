// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/FundamentalTypes.h>
#include <xait/common/DLLDefines.h>
#include <xait/common/math/Vec3.h>

namespace XAIT
{
	namespace Common
	{
		namespace Geom
		{
			namespace Intersect
			{
				//! test if a triangle and a cube intersect
				//! algorithm from Graphic Gems III
				//! \brief test if a triangle and a cube intersect
				//! \param a		triangle node
				//! \param b		triangle node
				//! \param c		triangle node
				//! \param center	center of the cube
				//! \param edgeLen	length of the edges of the cube (since it is a cube, they are all of equal length)
				//! \returns true if the cube and the triangle intersect, or false else
				XAIT_COMMON_DLLAPI bool TriangleCube(const Math::Vec3f& a, const Math::Vec3f& b, const Math::Vec3f& c, const Math::Vec3f& center, const float32 edgeLen);

				//! \brief 64-bit version of triangle cube intersection
				XAIT_COMMON_DLLAPI bool TriangleCube(const Math::Vec3d& a, const Math::Vec3d& b, const Math::Vec3d& c, const Math::Vec3d& center, const float64 edgeLen);
			}	// namespace Intersect
		}	// namespace Geom
	}	// namespace Common
}	// namespace XAIT
