// (C) xaitment GmbH 2006-2012

#pragma once 


#include <xait/common/FundamentalTypes.h>
#include <xait/common/DLLDefines.h>
#include <xait/common/geom/AABB2D.h>


namespace XAIT
{
	namespace Common
	{
		namespace Geom
		{
			namespace Intersect
			{
				//! \brief AABB circle intersection
				XAIT_COMMON_DLLAPI bool AABBCircleIntersect(const AABB2Df& aabb, const Math::Vec2f& center, const float32 radius);
			}
		}
	}
}

