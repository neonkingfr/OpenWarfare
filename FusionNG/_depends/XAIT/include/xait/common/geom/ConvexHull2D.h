// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/container/Vector.h>
#include <xait/common/math/Vec2.h>

namespace XAIT
{
	namespace Common
	{
		namespace Geom
		{
			//! \brief Class used to build a 2D convex hull with graham scan
			//! \param T_VEC	element type of the 2D positions
			template<typename T_VEC>
			class ConvexHull2D
			{
			public:
				//! \brief constructor
				ConvexHull2D()
				{};

				//! \brief build the convex hull
				//! \param convexHull[out]	computed convex hull (return value), is of size 0 
				//! \param points			points around which a convex hull should be build (not empty)
				void getConvexHull(Container::Vector<Math::Vec2<T_VEC> >& convexHull, Container::Vector<Math::Vec2<T_VEC> >& points)
				{
					convexHull.clear();

					X_ASSERT_MSG(!points.empty(),"cannot build convex hull for empty list of points");

					//find a pivot to start from
					findPivot(points);

					//sort the points by increasing angle (between observed point, pivot and x-axis)
					//std::sort(points.begin(),points.end(),CompareByAngle<T_VEC>(points.front());

					X_ASSERT_MSG(mPivot == points.begin(),"pivot is not at the begining of points vector after sorting");

					typename Container::Vector<Math::Vec2<T_VEC> >::ConstIterator iter = mPivot;

					//push first line segment
					convexHull.pushBack((*iter));
					++iter;
					convexHull.pushBack((*iter));
					++iter;

					typename Container::Vector<Math::Vec2<T_VEC> >::ConstIterator last;

					for(;iter != points.end();++iter)
					{
						last = convexHull.end();
						--last;
						float32 cross = crossProduct( *(last-1),*last,*iter );

						if(cross > .0f)
						{
							convexHull.pushBack(*iter);
						}
						else
						{
							while (cross <= .0f && convexHull.size()>2)
							{
								convexHull.popBack();
								last = convexHull.end();
								--last;
								cross = crossProduct(*(last-1),*last,*iter);
							}

							convexHull.pushBack(*iter);
						}
					}
				};

			private:
				// sorting helpers
				class CompareByAngle
				{
					Math::Vec2<T_VEC> mPivot;

				public:
					CompareByAngle(const Math::Vec2<T_VEC> &a)
						:mPivot(a)
					{};

					bool operator () (const Math::Vec2<T_VEC> &x,const Math::Vec2<T_VEC> &y) const
					{
						if(mPivot == x)
							return true;
						if(y == mPivot)
							return false;

						Math::Vec2<T_VEC> unitX = Math::Vec2<T_VEC>( (T_VEC)(1),(T_VEC)(0) );
						//normalize the vectors and compute the (cosine) of the angle
						Math::Vec2<T_VEC> unit1 = x-mPivot;
						T_VEC length1 = unit1.normalize();
						T_VEC dot1 = unitX.dotProduct(unit1);

						Math::Vec2<T_VEC> unit2 = y-mPivot;
						T_VEC length2 = unit2.normalize();
						T_VEC dot2 = unitX.dotProduct(unit2);

						//sort by angle, if draw use the shortest distance
						return( ( dot1 >  dot2)
							?(true)
							:((dot1<dot2)?(false):(length1<length2)));
					};
				};

				void findPivot(Container::Vector<Math::Vec2<T_VEC> >& points)
				{
					mPivot = points.begin();

					typename Container::Vector<Math::Vec2<T_VEC> >::Iterator iter = mPivot;
					typename Container::Vector<Math::Vec2<T_VEC> >::Iterator iterEnd = points.end();

					for (;iter != iterEnd;++iter)
					{
						if((*mPivot).mY == (*iter).mY)
							mPivot = ((*mPivot).mX<(*iter).mX)?(mPivot):(iter);
						else
							mPivot = ((*mPivot).mY<(*iter).mY)?(mPivot):(iter);
					}

					//assure that the pivot is at the front of the vector
					//this is needed to sort the vector properly
					std::iter_swap(mPivot,points.begin());
					mPivot = points.begin();
				};

				inline float32 crossProduct(const Math::Vec2<T_VEC> x, const Math::Vec2<T_VEC> y, const Math::Vec2<T_VEC> z) const
				{
					return (  (y.mX - x.mX)
						*(z.mY - x.mY)
						-(z.mX - x.mX)
						*(y.mY - x.mY) );
				};


				typename Container::Vector<Math::Vec2<T_VEC> >::Iterator	mPivot; //! < the point to start graham scan from
			};
		}	// namespace Geom
	}	// namespace Common
}	// namespace XAIT
