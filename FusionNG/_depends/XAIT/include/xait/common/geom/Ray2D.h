// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/math/Vec3.h>
#include <xait/common/debug/Assert.h>
#include <xait/common/geom/AABB2D.h>
#include <xait/common/math/NumericLimits.h>

namespace XAIT
{
	namespace Common
	{
		namespace Geom
		{
			//! \brief template struct for ray (use with float32 or float64)
			template<typename T_OBJECTID = int32, typename T_COMP = float32>
			struct Ray2D 
			{
				Math::Vec2<T_COMP>	mStart;		//!< start point of ray
				Math::Vec2<T_COMP>	mEnd;		//!< end point of ray
				T_COMP				mT;			//!< value between 0 .. 1 which specifies the part of ray until it hits something
				T_OBJECTID			mObjectID;	//!< object that has been hit. only valid if t < 1

				//! \brief constructor
				Ray2D()
					: mT((T_COMP)1)
				{}

				//! \brief constructor
				//! \param start	ray origin
				//! \param dir		ray direction (normalized vector)
				//! \param length	ray length
				//! \remark mT will be initialized to 1
				Ray2D(const Math::Vec2<T_COMP>& start, const Math::Vec2<T_COMP>& dir, const float32& length)
					: mStart(start)
					, mEnd(start + dir * length)
					, mT((T_COMP)1)
				{
					X_ASSERT_MSG_DBG(dir.isNormalized(),"ray direction vector must be normalized");
				}


				//! brief constructor takes start point and endpoint
				//! \param start	start of ray
				//! \param end		end of ray
				//! \remark mT will be initialized to 1
				Ray2D(const Math::Vec2<T_COMP>& start, const Math::Vec2<T_COMP>& end)
					: mStart(start)
					, mEnd(end)
					, mT((T_COMP)1)
				{
				}

				//! \brief get hitpoint
				//! \remark Use this function only if there has been something hit, 
				//!			otherwise this method will return nonsense.
				Math::Vec2<T_COMP> getHitPoint() const
				{
					return mStart + (mEnd - mStart) * mT;
				}

				//! \brief Set the distance of an hit from start point
				//! \param hitDistance	distance from start point in world units
				//! \remark This modifies only mT
				void setHitDistance(const T_COMP hitDistance)
				{
					const T_COMP fullLength= (mEnd - mStart).getLength();
					mT= hitDistance / fullLength;
				}

				//! \brief get length of the ray as 2D vector
				Math::Vec2<T_COMP> getLength2D() const
				{
					return (mEnd - mStart) * mT;
				}

				//! \brief get length of the ray till the hit point
				//! \remark Use this function only if there has been something hit, 
				//!			otherwise this method will return nonsense.
				T_COMP getLength() const
				{
					return (mEnd - mStart).getLength() * mT;
				}

				//! \brief get the direction of the ray
				//! \remarks The direction is normalized
				Math::Vec2<T_COMP> getDirection() const
				{
					Math::Vec2<T_COMP> dir= mEnd - mStart;
					dir.normalize();
					return dir;
				}

				//! \brief get a point on this ray
				//! \param	distance	the distance of the point from start coordinate
				//!\remark this point can be behind the end point
				Math::Vec2<T_COMP> getPointOnRay(const float32& distance) const
				{
					Math::Vec2<T_COMP> dir = mEnd - mStart;
					dir.normalize();

					return mStart + dir * distance;
				}

				//! \brief clamp this ray to an axis aligned bounding box
				//! \returns True if the ray intersected with the bounding box or false if not
				//! \remark If no intersection occurs, the ray will stay unmodified.
				//! \remark This method does not recalculate mT, it will stay untouched.
				bool clampToAABB( const AABB2D<T_COMP>& aabb) 
				{
					const Math::Vec2<T_COMP> dir = mEnd - mStart;
					T_COMP t0= (T_COMP)0;
					T_COMP t1= (T_COMP)0;
					T_COMP mint = (T_COMP)0;
					T_COMP maxt = Math::NumericLimits<T_COMP>::maxPosValue();

					for (int axis = 0; axis < 2; ++axis) 
					{
						if (dir[axis] > (T_COMP)0) 
						{
							t0 = ( aabb.mMin[axis] - mStart[axis] ) / dir[axis];
							t1 = ( aabb.mMax[axis] - mStart[axis] ) / dir[axis];
						} 
						else 
						{
							t1 = ( aabb.mMin[axis] - mStart[axis] ) / dir[axis];
							t0 = ( aabb.mMax[axis] - mStart[axis] ) / dir[axis];
						}

						if (t1 < maxt) 
						{
							maxt = t1;
						}
						if (t0 > mint) 
						{
							mint = t0;
						}
					}

					// no intersection
					if (t1 < t0)
						return false;

					mEnd= mStart + dir * maxt;
					mStart= mStart + dir * mint;

					return true;
				}
			};	// struct Ray
		}	// namespace Geom
	}	// namespace Common
}	// namespace XAIT
