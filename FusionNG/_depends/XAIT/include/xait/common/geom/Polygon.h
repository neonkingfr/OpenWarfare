// (C) xaitment GmbH 2006-2012

#pragma once 

#include <xait/common/FundamentalTypes.h>
#include <xait/common/container/Vector.h>
#include <xait/common/math/Epsilon.h>
#include <xait/common/math/Vec3.h>
#include <xait/common/math/Vec2.h>
#include <xait/common/geom/GeomUtility.h>

namespace XAIT
{
	namespace Common
	{
		namespace Geom
		{
			namespace Polygon
			{

				template<typename T_POLYGON>
				class PolygonForwardAccess
				{
				public:
					PolygonForwardAccess(const T_POLYGON& polygon)
					{}

					inline uint32 operator()(const uint32 id) const
					{
						return id;
					}
				};

				template<typename T_POLYGON>
				class PolygonBackwardAccess
				{
					uint32		mOffset;

				public:
					PolygonBackwardAccess(const T_POLYGON& polygon)
						: mOffset(polygon.size() - 1)
					{}

					inline uint32 operator()(const uint32 id) const
					{
						return mOffset - id;
					}
				};

				template<typename T_POLYGON, bool N_CCW>
				class PolygonWindingAccess
				{};

				template<typename T_POLYGON>
				class PolygonWindingAccess<T_POLYGON,true>
				{
				public:
					typedef PolygonForwardAccess<T_POLYGON>		CCW;
					typedef PolygonBackwardAccess<T_POLYGON>	CW;
				};

				template<typename T_POLYGON>
				class PolygonWindingAccess<T_POLYGON,false>
				{
				public:
					typedef PolygonBackwardAccess<T_POLYGON>	CCW;
					typedef PolygonForwardAccess<T_POLYGON>		CW;
				};

				template<typename T_POLYGON, typename T_COMP, bool N_CCW>
				class PolygonDef
				{
					T_POLYGON&	mPolygon;
				public:

					typedef T_POLYGON			Polygon;
					typedef T_COMP				Component;

					typedef typename PolygonWindingAccess<Polygon,N_CCW>::CCW	AccessCCW;
					typedef typename PolygonWindingAccess<Polygon,N_CCW>::CW	AccessCW;


					PolygonDef(T_POLYGON& polygon)
						: mPolygon(polygon)
					{}

					inline T_POLYGON& getPolygon()
					{
						return mPolygon;
					}

					inline const T_POLYGON& getPolygon() const
					{
						return mPolygon;
					}

					inline AccessCCW getCCWAccess() const
					{
						return AccessCCW(mPolygon);
					}

					inline AccessCW getCWAccess() const
					{
						return AccessCW(mPolygon);
					}
				};

				template<typename T_POLYGON3D, typename T_COMP, typename F_FUNC2DMAP>
				class Polygon2DMapping
				{
					T_POLYGON3D&		mPolygon;
					const F_FUNC2DMAP&	m2DMapping;

				public:
					Polygon2DMapping(T_POLYGON3D& polygon3D, const F_FUNC2DMAP& func2DMap)
						: mPolygon(polygon3D)
						, m2DMapping(func2DMap)
					{}

					inline const Math::Vec2<T_COMP> operator[](const uint32 id) const
					{
						return m2DMapping(mPolygon[id]);
					}

					inline uint32 size() const
					{
						return mPolygon.size();
					}

					inline const F_FUNC2DMAP& get2DMapping() const
					{
						return m2DMapping;
					}
				};

				template<typename T_POLYGON3D, typename T_COMP, typename F_FUNC2DMAP, bool N_CCW>
				class PolygonDef3D
				{
				public:
					typedef Polygon2DMapping<T_POLYGON3D,T_COMP,F_FUNC2DMAP>	Polygon2D;
					typedef T_COMP												Component;

					PolygonDef3D(T_POLYGON3D& polygon3D, const F_FUNC2DMAP& func2DMap)
						: mPolygon(polygon3D,func2DMap)
					{}

					inline PolygonDef<Polygon2D,T_COMP,N_CCW> getPolygonDef()
					{
						return PolygonDef<Polygon2D,T_COMP,N_CCW>(mPolygon);
					}

					inline PolygonDef<const Polygon2D,T_COMP,N_CCW> getPolygonDef() const
					{
						return PolygonDef<const Polygon2D,T_COMP,N_CCW>(mPolygon);
					}

					inline const F_FUNC2DMAP& get2DMapping() const
					{
						return mPolygon.get2DMapping();
					}

				private:
					Polygon2D	mPolygon;

				};


				template<typename T_COMP, bool N_CCW>
				class CreatePolygonDef
				{
				public:
					template<typename T_POLYGON>
					inline static PolygonDef<T_POLYGON,T_COMP,N_CCW> from2D(T_POLYGON& polygon)
					{
						return PolygonDef<T_POLYGON,T_COMP,N_CCW>(polygon);
					}

					template<typename T_POLYGON3D,typename F_FUNC2DMAP>
					inline static PolygonDef3D<T_POLYGON3D,T_COMP,F_FUNC2DMAP,N_CCW> from3D(T_POLYGON3D& polygon, const F_FUNC2DMAP& map2D)
					{
						return PolygonDef3D<T_POLYGON3D,T_COMP,F_FUNC2DMAP,N_CCW>(polygon,map2D);
					}
				};

				template<typename T_COMP>
				inline bool IsPointLeftFromLine(const Math::Vec2<T_COMP>& a, const Math::Vec2<T_COMP>& b, const Math::Vec2<T_COMP>& p, const T_COMP epsilon)
				{
					Math::Vec2<T_COMP> ab= b - a;
					Math::Vec2<T_COMP> pa= a - p;

					T_COMP det= pa.getDeterminant(ab);

					// we handle points on the line as points inside the polygon
					// therefore we check for valid det <= 0 
					if (det > 0)
					{
						// we accept some error of points witch are on the right side of the line
						// but inorder to test, we have to compute the distance to the point
						// thus we have to normalize the determinate
						const T_COMP d2= (det * det) / ab.getLength2();
						if (d2 > epsilon)
							return true;
					}
					return false;
				}

				//! \brief Test if a point is in a convex polygon
				//! \param point			point to test
				//! \param polydef			convex polygon definition, polygon winding stored in definition
				template<typename T_POLYGONDEF2D>
				bool IsPointInsideConvexPoly(const Math::Vec2<typename T_POLYGONDEF2D::Component>& point, const T_POLYGONDEF2D& polydef, const typename T_POLYGONDEF2D::Component epsilon= Common::Math::Epsilon<typename T_POLYGONDEF2D::Component>::value())
				{
					const typename T_POLYGONDEF2D::AccessCW access= polydef.getCWAccess();
					const typename T_POLYGONDEF2D::Polygon& polygon= polydef.getPolygon();

					// This algorithm checks if the specified point is on the same side 
					// of all polygon line segments. If this is the case, the point is
					// inside. Further we accept points that are on polygon line segments
					// as points inside the polygon.
					const uint32 n= polygon.size();


					// initial compute a and b, so that we can reuse a as b in the next iteration
					// for the initial segment we use the last segment n-1 -> 0 . This way
					// we can prevent the modulo operation in the for loop.
					Math::Vec2<typename T_POLYGONDEF2D::Component> a= polygon[access(n - 1)];	// start of line segment
					Math::Vec2<typename T_POLYGONDEF2D::Component> b= polygon[access(0)];		// end of line segment

					if (IsPointLeftFromLine(a,b,point,epsilon))
						return false;

					for(uint32 i= 1; i < n; ++i)
					{
						a= b;
						b= polygon[access(i)];

						if (IsPointLeftFromLine(a,b,point,epsilon))
							return false;
					}

					return true;
				}


				template<typename T_POLYGONDEF3D>
				inline bool IsPointInsideConvexPoly(const Math::Vec3<typename T_POLYGONDEF3D::Component>& point, const T_POLYGONDEF3D& polydef, const typename T_POLYGONDEF3D::Component epsilon= Common::Math::Epsilon<typename T_POLYGONDEF3D::Component>::value())
				{
					return IsPointInsideConvexPoly(polydef.get2DMapping()(point),polydef.getPolygonDef(),epsilon);
				}


			}
		}
	}
}

