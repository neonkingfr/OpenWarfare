// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/math/Vec3.h>
#include <xait/common/math/MathDefines.h>

namespace XAIT
{
	namespace Common
	{
		namespace Geom
		{
			template<typename T_COMP>
			struct Triangle3D;

			typedef Triangle3D<float32>		Triangle3Df;	//!< always 32-bit
			typedef Triangle3D<float32>		Triangle3Dd;	//!< always 64-bit

			//! \brief template for a triangle in 3D space
			template<typename T_COMP>
			struct Triangle3D
			{
				Math::Vec3<T_COMP>	mA;
				Math::Vec3<T_COMP>	mB;
				Math::Vec3<T_COMP>	mC;

				Triangle3D()
				{}

				Triangle3D(const Math::Vec3<T_COMP>& a, const Math::Vec3<T_COMP>& b, const Math::Vec3<T_COMP>& c)
					: mA(a),mB(b),mC(c)
				{}


				//! \brief compute area of this triangle
				inline T_COMP getArea() const
				{
					return getArea(mA,mB,mC);
				}

				//! \brief test if this triangle is degenerated
				inline bool isDegenerated(const T_COMP minArea= X_EPSILON) const
				{
					return isDegenerated(mA,mB,mC,minArea);
				}

				//! \brief compute the normal of this triangle
				//! \param normal[out]	normal for the triangle (return value)
				//! \returns true if the triangle normal could be computed, or false, if the triangle was degenerated
				//! \remark the vertices must be in counter clockwise order
				inline bool getNormal(Math::Vec3<T_COMP>& normal) const
				{
					return getNormal(normal,mA,mB,mC);
				}

				inline Math::Vec3<T_COMP>& operator[](const uint32 id)
				{
					return *(&mA + id);
				}

				inline const Math::Vec3<T_COMP>& operator[](const uint32 id) const
				{
					return *(&mA + id);
				}

				//! \brief compute the area of a 3d triangle
				//! \param a	node a of triangle
				//! \param b	node b of triangle
				//! \param c	node c of triangle
				//! \returns the size of the triangle area
				//! \remark nodes can be in cw or ccw direction
				static T_COMP getArea(const Math::Vec3<T_COMP>& a, const Math::Vec3<T_COMP>& b, const Math::Vec3<T_COMP>& c);

				//! \brief check if a triangle is degenerated
				//! \param a			node a of triangle
				//! \param b			node b of triangle
				//! \param c			node c of triangle
				//! \returns true if the triangle is degenerated
				static bool isDegenerated(const Math::Vec3<T_COMP>& a, const Math::Vec3<T_COMP>& b, const Math::Vec3<T_COMP>& c, const T_COMP minArea= X_EPSILON);

				//! \brief compute normal for a triangle
				//! \param normal	normal for the triangle (return value)
				//! \param a		vertex 0
				//! \param b		vertex 1
				//! \param c		vertex 2
				//! \remark the vertices must be in counter clockwise order
				//! \remark the triangle must not be degenerated
				static void getNormal(Math::Vec3<T_COMP>& normal, const Math::Vec3<T_COMP>& a, const Math::Vec3<T_COMP>& b, const Math::Vec3<T_COMP>& c);

				//! \brief compute normal for a triangle and test if triangle is degenerated
				//! \param normal	normal for the triangle (return value)
				//! \param a		vertex 0
				//! \param b		vertex 1
				//! \param c		vertex 2
				//! \remark the vertices must be in counter clockwise order
				static bool getStableNormal(Math::Vec3<T_COMP>& normal, const Math::Vec3<T_COMP>& a, const Math::Vec3<T_COMP>& b, const Math::Vec3<T_COMP>& c);

			};


			template<typename T_COMP>
			inline void XAIT::Common::Geom::Triangle3D<T_COMP>::getNormal( Math::Vec3<T_COMP>& normal, const Math::Vec3<T_COMP>& a, const Math::Vec3<T_COMP>& b, const Math::Vec3<T_COMP>& c )
			{
				Math::Vec3<T_COMP> x,y;

				x= b - a;
				y= c - a; 

				normal= x.crossProduct(y);
				normal.fastNormalize();
			}

			template<typename T_COMP>
			inline bool XAIT::Common::Geom::Triangle3D<T_COMP>::getStableNormal( Math::Vec3<T_COMP>& normal, const Math::Vec3<T_COMP>& a, const Math::Vec3<T_COMP>& b, const Math::Vec3<T_COMP>& c )
			{
				Math::Vec3<T_COMP> x,y;

				x= b - a;
				y= c - a; 

				normal= x.crossProduct(y);
				if (normal.getLength2() < (T_COMP)(X_EPSILON * X_EPSILON))	// test for degenerated
					return false;

				normal.fastNormalize();
				return true;
			}


			template<typename T_COMP>
			inline bool XAIT::Common::Geom::Triangle3D<T_COMP>::isDegenerated( const Math::Vec3<T_COMP>& a, const Math::Vec3<T_COMP>& b, const Math::Vec3<T_COMP>& c, const T_COMP minArea)
			{
				// test for collinear, algo from http://mathworld.wolfram.com/Collinear.html

				Math::Vec3<T_COMP> tmp1;
				Math::Vec3<T_COMP> tmp2;
				Math::Vec3<T_COMP> cross;

				tmp1= b - a;
				tmp2= a - c;

				cross= tmp1.crossProduct(tmp2);

				return cross.getLength2() < ((T_COMP)4.0 * minArea * minArea);
			}


			template<typename T_COMP>
			inline T_COMP XAIT::Common::Geom::Triangle3D<T_COMP>::getArea( const Math::Vec3<T_COMP>& a, const Math::Vec3<T_COMP>& b, const Math::Vec3<T_COMP>& c )
			{
				Math::Vec3<T_COMP> u;
				Math::Vec3<T_COMP> v;
				Math::Vec3<T_COMP> cross;

				u= b - a;
				v= c - a;
				cross= u.crossProduct(v);

				return (T_COMP)0.5 * cross.getLength();
			}
		}	// namespace Geom
	}	// namespace Common
}	// namespace XAIT
