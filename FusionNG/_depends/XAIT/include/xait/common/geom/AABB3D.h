// (C) xaitment GmbH 2006-2012

#pragma once

// ---------------------------------------------------------------------------------------------------
//										Axis Aligned Bounding Box for 3D
//
// Author: Markus Wilhelm
//
// An Axis Aligned Bounding Box for 3D Space
// ---------------------------------------------------------------------------------------------------


#include <xait/common/math/Vec3.h>
#include <xait/common/math/Matrix4.h>
#include <xait/common/geom/Ray3D.h>
#include <xait/common/math/NumericLimits.h>


namespace XAIT
{
	namespace Common
	{
		namespace Geom
		{
			template<typename T_COMP>
			struct AABB3D;

			typedef AABB3D<float32>		AABB3Df;
			typedef AABB3D<float64>		AABB3Dd;

			typedef AABB3D<uint32>		AABB3Du32;
			typedef AABB3D<int32>		AABB3Di32;


			template<typename T_COMP>
			struct AABB3D
			{
				Math::Vec3<T_COMP>	mMin;		//!< minimum value
				Math::Vec3<T_COMP>	mMax;		//!< maximum value

				//! \brief Default constructor
				//! \remark No initialization
				AABB3D()
				{};

				//! \brief init the bounding box with a first value
				//! \param value	first value in the box
				inline void init(const Math::Vec3<T_COMP>& value)
				{
					mMin= value;
					mMax= value;
				}

				//! \brief init the bounding box with default value
				//! \remark The resulting bounding box is not valid until one point has extended the bounding box
				inline void init()
				{
					const T_COMP posInfinity= Math::NumericLimits<T_COMP>::maxPosValue();

					// we build a negative infinity, so we take the positive maximum and decrement it
					// by one for integer values, which have a by one smaller value range in negative
					// area.
					const T_COMP negInfinity= Math::NumericLimits<T_COMP>::maxNegValue();

					mMin.mX= posInfinity;
					mMin.mY= posInfinity;
					mMin.mZ= posInfinity;
					mMin.mW= posInfinity;
					mMax.mX= negInfinity;
					mMax.mY= negInfinity;
					mMax.mZ= negInfinity;
					mMax.mW= negInfinity;
				}

				//! \brief Sets the extends to +/- infinity
				inline void setInfinity()
				{
					const T_COMP posInfinity= Math::NumericLimits<T_COMP>::maxPosValue();
					const T_COMP negInfinity= Math::NumericLimits<T_COMP>::maxNegValue();

					mMax.mX= posInfinity;
					mMax.mY= posInfinity;
					mMax.mZ= posInfinity;
					mMax.mW= posInfinity;
					mMin.mX= negInfinity;
					mMin.mY= negInfinity;
					mMin.mZ= negInfinity;
					mMin.mW= negInfinity;
				}

				//! \brief extend the bounding box by a position
				//! \param pos		position by which the bounding box should be extended
				inline void extend(const Math::Vec3<T_COMP>& pos)
				{
					if (pos.mX < mMin.mX) mMin.mX= pos.mX;
					if (pos.mY < mMin.mY) mMin.mY= pos.mY;
					if (pos.mZ < mMin.mZ) mMin.mZ= pos.mZ;
					if (pos.mX > mMax.mX) mMax.mX= pos.mX;
					if (pos.mY > mMax.mY) mMax.mY= pos.mY;
					if (pos.mZ > mMax.mZ) mMax.mZ= pos.mZ;
				}

				//! \brief extend the bounding box by another bounding box
				//! \param bbox		bounding box by which this bounding box should be extended
				inline void extend(const AABB3D& bbox)
				{
					extend(bbox.mMin);
					extend(bbox.mMax);
				}

				//! \brief get the size of the bounding box
				inline Math::Vec3<T_COMP> getSize() const
				{
					return mMax - mMin;
				}

				//! \brief get the center of the bounding box
				inline Math::Vec3<T_COMP> getCenter() const
				{
					return mMin + (mMax - mMin) * (T_COMP)0.5;
				}

				//! \brief check if the bounding box is valid
				//! \returns true if the bounding box is valid
				inline bool isValid() const
				{
					return (mMin.mX <= mMax.mX && mMin.mY <= mMax.mY && mMin.mZ <= mMax.mZ);
				}

				//! \brief tests if two bounding boxes overlap
				inline bool overlap(const AABB3D& bbox) const
				{
					if (mMax.mZ < bbox.mMin.mZ) return false;
					if (mMin.mZ > bbox.mMax.mZ) return false;
					if (mMax.mY < bbox.mMin.mY) return false;
					if (mMin.mY > bbox.mMax.mY) return false;
					if (mMax.mX < bbox.mMin.mX) return false;
					if (mMin.mX > bbox.mMax.mX) return false;

					return true;
				}

				//! \brief test if a point is in the bounding box
				//! \param pos	point to test
				//! \returns true if the point is inside
				inline bool isPointInside(const Math::Vec3<T_COMP>& pos) const
				{
					if (pos.mX < mMin.mX) return false;
					if (pos.mY < mMin.mY) return false;
					if (pos.mZ < mMin.mZ) return false;
					if (pos.mX > mMax.mX) return false;
					if (pos.mY > mMax.mY) return false;
					if (pos.mZ > mMax.mZ) return false;

					return true;
				}

				//! \brief transform this aabb by a matrix
				//! \remarks The transformed aabb will be bigger than the original one
				AABB3D<T_COMP> transform(const Math::Matrix4<T_COMP>& transMatrix) const
				{
					const Math::Vec3<T_COMP> size(getSize());
					const Math::Vec3<T_COMP> corners[8]=
					{
						mMin,
						Math::Vec3<T_COMP>(mMin.mX + size.mX	,mMin.mY			,mMin.mZ),
						Math::Vec3<T_COMP>(mMin.mX + size.mX	,mMin.mY + size.mY	,mMin.mZ),
						Math::Vec3<T_COMP>(mMin.mX				,mMin.mY + size.mY	,mMin.mZ),
						mMax,
						Math::Vec3<T_COMP>(mMax.mX - size.mX	,mMax.mY			,mMax.mZ),
						Math::Vec3<T_COMP>(mMax.mX - size.mX	,mMax.mY - size.mY	,mMax.mZ),
						Math::Vec3<T_COMP>(mMax.mX				,mMax.mY - size.mY	,mMax.mZ),
					};

					AABB3D transformed;
					transformed.init();
					for(uint32 i= 0; i < 8; ++i)
						transformed.extend(transMatrix.transformPoint(corners[i]));

					return transformed;
				}

				//! \brief intersect a ray with the bounding box
				template<typename T_OBJECT>
				bool intersect(Ray3D<T_OBJECT,T_COMP>& ray)
				{
					T_COMP tmin, tmax, tymin, tymax, tzmin, tzmax;
					Math::Vec3<T_COMP> parameters[2];
					parameters[0]= mMin;
					parameters[1]= mMax;
					int sign[3];
					Math::Vec3<T_COMP> inv_dir= ray.mStart - ray.mEnd;
					T_COMP t0,t1;
					t0= 0;
					t1= inv_dir.normalize();

					sign[0] = (inv_dir.mX < 0);
					sign[1] = (inv_dir.mY < 0);
					sign[2] = (inv_dir.mZ < 0);

					// intersection test start from http://www.cs.utah.edu/~awilliam/box/
					tmin = (parameters[sign[0]].mX - ray.mStart.mX) * inv_dir.mX;
					tmax = (parameters[1-sign[0]].mX - ray.mStart.mX) * inv_dir.mX;
					tymin = (parameters[sign[1]].mY - ray.mStart.mY) * inv_dir.mY;
					tymax = (parameters[1-sign[1]].mY - ray.mStart.mY) * inv_dir.mY;

					if ( (tmin > tymax) || (tymin > tmax) ) 
						return false;

					if (tymin > tmin)
						tmin = tymin;

					if (tymax < tmax)
						tmax = tymax;

					tzmin = (parameters[sign[2]].mZ - ray.mStart.mZ) * inv_dir.mZ;
					tzmax = (parameters[1-sign[2]].mZ - ray.mStart.mZ) * inv_dir.mZ;

					if ( (tmin > tzmax) || (tzmin > tmax) ) 
						return false;

					if (tzmin > tmin)
						tmin = tzmin;

					if (tzmax < tmax)
						tmax = tzmax;

					return ( (tmin < t1) && (tmax > t0) );
				}

				//! \brief extend bounding box to a cube
				//! \remark box must be valid
				//! \remark The minimum will stay valid, only the maximum will be recomputed.
				inline void extendToCube()
				{
					// get max edge
					const Math::Vec3<T_COMP> diff(mMax - mMin);
					const int32 maxComp= diff.getMaxComp();
					const float32 maxEdge= diff[maxComp];

					// extend the other two edges (recompute all, that is faster)
					mMax.mX= mMin.mX + maxEdge;
					mMax.mY= mMin.mY + maxEdge;
					mMax.mZ= mMin.mZ + maxEdge;
				}
			}; // struct AABB3D
		}	// namespace Geom
	}	// namespace Common
}	// namespace XAIT
