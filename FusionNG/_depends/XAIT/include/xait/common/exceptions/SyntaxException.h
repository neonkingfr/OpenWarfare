// (C) xaitment GmbH 2006-2012

#pragma once 

#include <xait/common/exceptions/Exception.h>

namespace XAIT
{
	namespace Common
	{
		namespace Exceptions
		{
			//! \brief Exception for syntax errors while initializing from xmltree
			class SyntaxException : public Exception
			{
			public:
				SyntaxException(const Common::String& is, const Common::String& shouldBe, const Common::String& errorMsg)
					: Exception(errorMsg),mIs(is),mShouldBe(shouldBe)
				{}

				virtual Common::String getExceptionName() const
				{
					return "SyntaxException";
				}

				inline const Common::String& getIs() const
				{
					return mIs;
				}

				inline const Common::String& getShouldBe() const
				{
					return mShouldBe;
				}

			protected:
				Common::String		mIs;
				Common::String		mShouldBe;

				virtual void getExceptionParameter(Container::List<Container::Pair<Common::String,Common::String> >& parameters)
				{
					parameters.pushBack(Container::MakePair(Common::String("Is"),mIs));
					parameters.pushBack(Container::MakePair(Common::String("ShouldBe"),mShouldBe));
				}

			};
		}	// namespace Exceptions
	}	// namespace Common
}	// namespace XAIT
