// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/String.h>
#include <xait/common/debug/CallStack.h>
#include <xait/common/container/List.h>
#include <xait/common/container/Pair.h>
#include <xait/common/Library.h>

// Defines
// XAIT_EXCEPTION_DISABLE_CALLSTACK		if defined, the callstack capture will be disabled

namespace XAIT
{
	namespace Common
	{
		namespace Exceptions
		{
			//! base class for all exceptions which can show the call stack from the startpoint
			//! of the exception being thrown.
			//! \brief base class for all exceptions in the namespace XAIT
			class Exception
			{
			public:
				//! \brief Information which should be shown in the listing (ored options possible)
				enum CallStackListingOption
				{
					CSOPT_NONE		= Debug::CallStack::CSOPT_NONE,		//!< show nothing (listing disabled)
					CSOPT_FUNC		= Debug::CallStack::CSOPT_FUNC,		//!< show the name of the function
					CSOPT_FILE		= Debug::CallStack::CSOPT_FILE,		//!< show file and line number from which the function was called
					CSOPT_MODULE	= Debug::CallStack::CSOPT_MODULE,	//!< show the module from the function was called
					CSOPT_ALL		= Debug::CallStack::CSOPT_ALL,		//!< show all possible information
				};

				//! \brief construct a new exception
				//! \param errorMsg		message which is related to the error
				//! \remark The callstack capture will be created during the construct.
				XAIT_COMMON_DLLAPI Exception(const Common::String& errorMsg);

				XAIT_COMMON_DLLAPI virtual ~Exception();

				//! \brief get the callstack associated with this exception
				inline const Debug::CallStack& getCallStack()
				{
					return mCallStack;
				}

				//! \brief Clear the callstack listing. Removes the callstack information from the exception
				inline void clearCallStack()
				{
					mCallStack.clearCallStack();
				}

				//! \brief get the error message associated with this exception
				inline const Common::String& getErrorMsg()
				{
					return mErrorMsg;
				}

				//! \brief get the name of the exception
				//! \remark The name is usually the exception class name
				virtual Common::String getExceptionName() const
				{
					return "Exception";
				}

				//! \brief sets the inner exception of this exception
				//! \param exception	pointer to the inner exception, will be owned by this exception
				//! \remark The pointer to the inner exception will be owner by this exception, thus
				//!			also be destroy. If there is already an inner exception set, this exception 
				//!			will be deleted.
				XAIT_COMMON_DLLAPI void setInnerException(Exception* exception);

				//! \brief get the inner exception of this exception
				inline const Exception* getInnerException() const
				{
					return mInnerException;
				}

				//! \brief show a message box with the exception error text
				XAIT_COMMON_DLLAPI virtual void showException(const CallStackListingOption showCallStack= CSOPT_ALL);

				//! \brief overload operator new , using the Debug::StaticAllocator for memory
				inline void* operator new(size_t size)
				{
					Exception* block= (Exception*)Library::getExceptionAlloc()->alloc((uint32)size);
				
					// use new operator for constructor init of the allocator object
					new (&(block->mAllocator)) Memory::Allocator::Ptr(Library::getExceptionAlloc());

					return block;
				}

				//! \brief overload operator delete and use the assigned allocator to deallocate the object
				inline void operator delete(void* ptr)
				{
					Exception* e= (Exception*)ptr;
					e->mAllocator->free(ptr);
				}

			protected:
				Memory::Allocator::Ptr	mAllocator;
				Debug::CallStack		mCallStack;			//!< the complete call stack at the time of the exception creation
				Common::String			mErrorMsg;			//!< message related to the error
				Exception*				mInnerException;	//!< exception which has caused this exception

				typedef Container::Pair<Common::String,Common::String>			Parameter;		//!< parameter: first = name, second = value
				typedef Container::List<Parameter>								ParameterList;

				//! \brief get per exception defined error parameter formated for showException
				//! \param parameters	list of parameter, where each paramater is a parameter name (starting with capital letter) and the parameter value
				//! \remark This method is used by showException for the parameter block
				XAIT_COMMON_DLLAPI virtual void getExceptionParameter(ParameterList& parameters);

			};	// class Exception
		}	// namespace Exception
	}	// namespace Common
}	// namespace XAIT

