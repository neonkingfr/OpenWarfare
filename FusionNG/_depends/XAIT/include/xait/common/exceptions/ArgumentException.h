// (C) xaitment GmbH 2006-2012

#pragma once 

#include <xait/common/exceptions/Exception.h>

namespace XAIT
{
	namespace Common
	{
		namespace Exceptions
		{
			//! \brief Exception for wrong arguments passed to a function
			class ArgumentException : public Exception
			{
			public:
				ArgumentException(const Common::String& argumentName, const Common::String& errorMsg)
					: Exception(errorMsg),mArgumentName(argumentName)
				{}

				virtual Common::String getExceptionName() const
				{
					return "ArgumentException";
				}

				inline const Common::String& getArgumentName() const
				{
					return mArgumentName;
				}

			protected:
				Common::String		mArgumentName;

				virtual void getExceptionParameter(Container::List<Container::Pair<Common::String,Common::String> >& parameters)
				{
					parameters.pushBack(Container::MakePair(Common::String("ArgumentName"),mArgumentName));
				}

			};
		}	// namespace Exceptions
	}	// namespace Common
}	// namespace XAIT
