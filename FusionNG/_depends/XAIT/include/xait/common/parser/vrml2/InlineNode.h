// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/parser/vrml2/SceneNode.h>
#include <xait/common/parser/vrml2/FieldTypes.h>
namespace XAIT
{
	namespace Common
	{
		namespace Parser
		{

			namespace VRML2
			{

				class InlineNode : public SceneNode
				{
				public:
					MFSceneNode		mChildren;
					SFVec3f			mBBoxCenter;
					SFVec3f			mBBoxSize;

					InlineNode(SceneNode* parent);
					InlineNode(const InlineNode& other);
					~InlineNode();

					void parseNode(ParseInterface* iParser);
					SceneNode* getCopy(const Memory::Allocator::Ptr& allocator);
					const char* getNodeName() const;
				};

			}
		}
	}
}
