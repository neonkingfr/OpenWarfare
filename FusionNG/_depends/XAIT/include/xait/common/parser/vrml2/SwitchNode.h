// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/parser/vrml2/SceneNode.h>
#include <xait/common/parser/vrml2/FieldTypes.h>

namespace XAIT
{
	namespace Common
	{
		namespace Parser
		{
			namespace VRML2
			{

				class SwitchNode : public SceneNode
				{
				public:
					MFSceneNode			mChoice;
					SFInt32				mWhichChoice;

					SwitchNode(SceneNode* parent);
					SwitchNode(const SwitchNode& other);
					~SwitchNode();

					void parseNode(ParseInterface* iParser);
					SceneNode* getCopy(const Memory::Allocator::Ptr& allocator);
					const char* getNodeName() const;

				};
			}
		}
	}
}
