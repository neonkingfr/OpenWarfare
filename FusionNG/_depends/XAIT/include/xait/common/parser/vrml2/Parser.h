// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/FundamentalTypes.h>
#include <xait/common/parser/vrml2/ParseInterface.h>
#include <xait/common/parser/vrml2/SceneNode.h>
#include <xait/common/parser/vrml2/SceneNodeParser.h>
#include <xait/common/parser/vrml2/GeometryNodeParser.h>
#include <xait/common/parser/vrml2/GeometricPropertyParser.h>
#include <xait/common/memory/MemoryManaged.h>
#include <xait/common/SharedPtr.h>
#include <xait/common/String.h>

#define VRML2_PARSER_READAHEADBUFFER	65536
#define VRML2_WHITESPACE				", \n\r\t"
#define VRML2_COMMENT					'#'
#define VRML2_HEADER					"#VRML V2.0 utf8"
#define VRML2_DIGIT_FLOAT				"0123456789.-+eE"
#define VRML2_DIGIT_INT					"0123456789-+"


//////////////////////////////////////////////////////////////////////////
///								 VRML 2 Parser
//////////////////////////////////////////////////////////////////////////
/// using definition from: http://www.graphcomp.com/info/specs/sgi/vrml/spec/
//////////////////////////////////////////////////////////////////////////
namespace XAIT
{
	namespace Common
	{
		namespace IO
		{
			class FileSystem;
			class Stream;
		}

		namespace Parser
		{
			namespace VRML2
			{
				class Parser : public Memory::MemoryManaged
				{
				public:
					XAIT_COMMON_DLLAPI Parser();
					XAIT_COMMON_DLLAPI ~Parser();

					//! \brief parses a file
					//! \param fileName		file to parse (name is relative to the file system passed)
					//! \param fSystem		file system to use for parsing
					//! \returns a pointer that does not belong to the parser, must be deleted outside
					//! \remark Since VRML2 files can include other files, we need a complete file system
					//!			and open the files ourself.
					XAIT_COMMON_DLLAPI SceneNode* parse(const Common::String& fileName, IO::FileSystem* fSystem);

					//! \register user defined scene node types at the parser
					XAIT_COMMON_DLLAPI void registerSceneNode(SceneNode* node);

					//! \brief set if we should skip not registered nodes
					inline void setSkipNotRegisteredNodes(const bool enable)
					{
						mSkipNotRegisteredNodes= enable;
					}

					//! \brief set if skipped nodes in the vrml parser should be reported
					inline void setReportSkippedNodes(const bool enable)
					{
						mReportSkippedNodes= enable;
					}

				protected:
					friend class ParseInterface;

					SceneNodeParser				mSceneNodeParser;
					GeometryNodeParser			mGeometryNodeParser;
					GeometricPropertyParser		mGeometricPropParser;

					char readNextChar(const bool ignoreComments= false);
					void gotoPrevChar();
					bool isCharInSet(const char testChar, const char* charSet);

					//! \brief add a name from a DEF clause to the global names list
					//! \param name		name that should be added (pointer will not be owned)
					void addDefineName(const char* name);

					//! \brief test if a name exists in the global names list
					//! \param name		name to test
					//! \returns true if the name exists
					bool existsDefineName(const char* name) const;

					bool isEOF();


				private:
					ParseInterface		mParseInterface;
					bool				mSkipNotRegisteredNodes;
					bool				mReportSkippedNodes;

					Common::String		mParseDir;
					Common::String		mParseFile;

					IO::FileSystem*			mFileSystem;
					SharedPtr<IO::Stream>	mCurrStream;
					uint32					mCurrLine;
					uint32					mCurrRow;
					uint32					mLastLineRow;

					char				mReadBuffer[VRML2_PARSER_READAHEADBUFFER];
					uint32				mReadBufferPos;
					uint32				mReadBufferSize;

					void processChar(char& currChar, const bool ignoreComments);
					void readHeader();

					Container::LookUp<const char*,bool,Container::StringComp>	mDefineNames;	//!< all defined names, keys are not owned by this lookup !
				};
			}
		}
	}
}
