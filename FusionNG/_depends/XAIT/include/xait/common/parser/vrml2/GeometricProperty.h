// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/FundamentalTypes.h>
#include <xait/common/parser/vrml2/NodeParsable.h>
#include <xait/common/parser/vrml2/FieldTypes.h>
namespace XAIT
{
	namespace Common
	{
		namespace Parser
		{

			namespace VRML2
			{


				class GeometricProperty : public NodeParsable<GeometricProperty>
				{
				public:
					enum PropType
					{
						PROP_Color,
						PROP_Coordinate,
						PROP_Normal,
						PROP_TextureCoordinate,
					};

					GeometricProperty(const PropType typeID)
						: mTypeID((uint08)typeID)
					{}

					virtual ~GeometricProperty()
					{}

					inline PropType getTypeID() const
					{
						return (PropType)mTypeID;
					}

					static void validate(ParseInterface* iParser, const GeometricProperty* prop, const char* propName, const PropType requestedType);

				private:
					uint08		mTypeID;

				};
			}
		}
	}
}
