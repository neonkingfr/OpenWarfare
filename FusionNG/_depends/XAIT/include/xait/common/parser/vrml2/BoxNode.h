// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/parser/vrml2/GeometryNode.h>


namespace XAIT
{
	namespace Common
	{
		namespace Parser
		{

			namespace VRML2
			{
				class BoxNode : public GeometryNode
				{
				public:
					SFVec3f		mSize;

					BoxNode();

					void parseNode(ParseInterface* iParser);
					GeometryNode* getCopy(const Memory::Allocator::Ptr& allocator);
					const char* getNodeName() const;
				};

			}
		}
	}
}
