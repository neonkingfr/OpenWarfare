// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/FundamentalTypes.h>
#include <xait/common/parser/vrml2/NodeParsable.h>
#include <xait/common/parser/vrml2/FieldTypes.h>

namespace XAIT
{
	namespace Common
	{
		namespace Parser
		{

			namespace VRML2
			{

				class SceneNode : public NodeParsable<SceneNode>
				{
				public:
					enum NodeType
					{
						NT_Anchor = 0,
						NT_Appearance,
						NT_Background,
						NT_Billboard,
						NT_Collision,
						NT_ColorInterpolator,
						NT_CoordinateInterpolator,
						NT_CylinderSensor,
						NT_DirectionalLight,
						NT_Fog,
						NT_Group,
						NT_ImageTexture,
						NT_Inline,
						NT_LOD,
						NT_Material,
						NT_NavigationInfo,
						NT_NormalInterpolator,
						NT_OrientationInterpolator,
						NT_PixelTexture,
						NT_PlaneSensor,
						NT_PointLight,
						NT_PositionInterpolator,
						NT_ProximitySensor,
						NT_ScalarInterpolator,
						NT_Script,
						NT_Shape,
						NT_Sound,
						NT_SpotLight,
						NT_SphereSensor,
						NT_Switch,
						NT_TextureTransform,
						NT_TimeSensor,
						NT_TouchSensor,
						NT_Transform,
						NT_Viewpoint,
						NT_VisibilitySensor,
						NT_WorldInfo,
						NT_UserDefNodes,
					};

					SceneNode(const NodeType typeID, SceneNode* parent)
						: mTypeID((uint16)typeID),mParent(parent)
					{}

					virtual ~SceneNode()
					{}

					inline SceneNode* getParent() const
					{
						return mParent;
					}

					inline void setParent(SceneNode* parent)
					{
						mParent= parent;
					}

					inline NodeType getTypeID() const
					{
						return (NodeType)mTypeID;
					}

				private:

					uint16			mTypeID;
					SceneNode*		mParent;
				};

			}
		}
	}
}
