// (C) xaitment GmbH 2006-2012

#pragma once
#include <xait/common/parser/vrml2/GeometryNode.h>

namespace XAIT
{
	namespace Common
	{
		namespace Parser
		{
			namespace VRML2
			{
				class CoordinateProp;

				class ElevationGridNode : public GeometryNode
				{
				public:
					SFGeometricProperty		mColor;
					SFGeometricProperty		mNormal;
					SFGeometricProperty		mTexCoord;
					MFFloat					mHeight;
					SFBool					mCCW;
					SFBool					mColorPerVertex;
					SFFloat					mCreaseAngle;
					SFBool					mNormalPerVertex;
					SFBool					mSolid;
					SFInt32					mXDimension;
					SFFloat					mXSpacing;
					SFInt32					mZDimension;
					SFFloat					mZSpacing;


					ElevationGridNode();
					~ElevationGridNode();

					ElevationGridNode(const ElevationGridNode& other);

					void parseNode(ParseInterface* iParser);
					GeometryNode* getCopy(const Memory::Allocator::Ptr& allocator);
					const char* getNodeName() const
					{
						return "ElevationGrid";
					}
				};

			}
		}
	}
}
