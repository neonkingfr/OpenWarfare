// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/parser/vrml2/FieldTypes.h>

namespace XAIT
{
	namespace Common
	{
		namespace Parser
		{

			namespace VRML2
			{

				struct Rotation
				{
					SFVec3f		mAxis;
					SFFloat		mAngle;

					Rotation()
						: mAxis(0,0,1),mAngle(0)
					{}

					Rotation(const float32 axisX,const float32 axisY,const float32 axisZ,const float32 angle)
						: mAxis(axisX,axisY,axisZ),mAngle(angle)
					{}
				};
			}
		}
	}
}
