// (C) xaitment GmbH 2006-2012

#pragma once 

#include <xait/common/parser/ParserException.h>

namespace XAIT
{
	namespace Common
	{
		namespace Parser
		{
			namespace XML
			{
				class XMLParserException : public ParserException
				{
				public:
					//! \brief constructor
					//! \param errorMsg		parse error message
					//! \param lineNumber	line in which the error occured
					//! \param rowNumber	row in which the error occured
					XMLParserException(const Common::String& errorMsg, const uint32 lineNumber, const uint32 rowNumber)
						: ParserException(errorMsg,lineNumber,rowNumber)
					{}

					//! \brief get the name of the exception
					virtual Common::String getExceptionName() const
					{
						return "XMLParserException";
					}

				};
			}
		}
	}
}
