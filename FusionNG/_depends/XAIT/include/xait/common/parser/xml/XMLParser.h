// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/container/Set.h>
#include <xait/common/parser/xml/XMLParserException.h>
#include <xait/common/memory/MemoryManaged.h>
#include <xait/common/io/FileSystem.h>

#include <string.h>

#define XMLPARSER_VC_NAME_FIRSTCHAR		"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ_"
#define XMLPARSER_VC_NAME				"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_.,-:"
#define XMLPARSER_VC_FREESPACE			" \t\n\r"
#define XMLPARSER_VC_NEWLINE			'\n'

#ifdef XAIT_EXCEPTION_HAS_EXCEPTIONS
#define XAIT_XMLPARSER_THROW_EXCEPTION_WITH_LINE(errorMsg)			\
	{																\
	uint32 row,line;												\
	getError(line,row);											 	\
	XAIT_THROW_EXCEPTION( XMLParserException(errorMsg,line,row) );  \
	}
#else
#define XAIT_XMLPARSER_THROW_EXCEPTION_WITH_LINE(errorMsg)
#endif


//! \brief builds an xmltree from a xml file
namespace XAIT
{
	namespace Common
	{
		namespace IO
		{
			class Stream;
		}
		namespace Parser
		{
			namespace XML
			{
				class XMLTreeBuilder;
				class XMLNodeElements;

				class XMLParser : public Memory::MemoryManaged
				{
				public:
					//! \brief constructor
					//! \param allocator	Allocator used for memory allocation/deallocation
					XAIT_COMMON_DLLAPI XMLParser();

					//! \brief destructor
					XAIT_COMMON_DLLAPI ~XMLParser();

					//! \brief parse a xml file and generate tree from a stream
					//! \param stream	stream with the xmlfile
					//! \returns the root node of a xmltree or throws exception if an error occured
					//! \remark The returned pointer belongs to the xmlparser. 
					//! If a new file will be parsed, the old tree gets invalid. If the stream 
					//! contains any xi:include statements, the parse will fail, since we have
					//! no filesystem.
					XAIT_COMMON_DLLAPI XMLNodeElements* parseStream(IO::Stream* stream);

					//! \brief parse a xml file from a file
					//! \param fileName		file to parse
					//! \param fSystem		filesystem which should be used to open files
					//! \remark The returned pointer belongs to the xmlparser.
					//! If a new file will be parsed, the old tree gets invalid.
					XAIT_COMMON_DLLAPI XMLNodeElements* parseFile(const Common::String& fileName, IO::FileSystem* fSystem);

					//! \brief parse a buffer with xml data and generate tree
					//! \param xmlBuffer	buffer with xmldata
					//! \param size			size of the buffer
					//! \param copyString	specifies if strings should be copied
					//! \return the root node of a xmltree or NULL if an error occured
					//! \remark if copyStrings is false, the xmlbuffer must remain valid for the time the xmltree is being used.
					//! if a new buffer will be parsed, the old tree gets invalid. The buffer will be modified.
					//! If the buffer contains any xi:include statements, the parse will fail, 
					//! since we have no filesystem.
					XAIT_COMMON_DLLAPI XMLNodeElements* parseBuffer(char* xmlBuffer, const uint32 size, const bool copyStrings);

					//! \brief get the parse error
					//! \param line		line where error occured
					//! \param row		row in the line, where error occured
					//! \returns the error text
					XAIT_COMMON_DLLAPI char* getError(uint32& line, uint32& row);

					//! \brief converts special characters used in mathfunctions from xml standard to a normal string
					XAIT_COMMON_DLLAPI static void convertSpecialCharacters(String& string);

				private:
					char*					mXMLFile;			//!< one big buffer for the complete xmlfile
					uint32					mBufferSize;		//!< size of the filebuffer
					bool					mInternalBuffer;	//!< if true, the xmlFile is internally allocated
					XMLTreeBuilder*			mXMLBuilder;		//!< builder to build xmltree
					Container::Set<uint64>	mNewLinePos;		//!< positions of the newlines
					uint64					mErrorPos;			//!< position in buffer where error occured
					char					mErrorText[128];
					IO::FileSystem*			mFileSystem;		//!< filesystem for opening files
					Container::List<SharedPtr<XMLParser> >	mOpenParser;		//!< parser used for includes

					//! \brief identify next tag
					//! \param startTagStart	position in the buffer at the position of the "<" symbol
					//! \param startTagEnd		position in the buffer after the ">" symbol
					//! \param startPos			position in the buffer where to start, should be infront of or at the position of "<". 
					//!							freespace will be skipped but non-freespace characters result in an error
					//! \param buffer			buffer with xml content
					//! \returns type of found node or -1 for comment, -2 for external definition, -3 for endtag, -4 for an error
					int32 identifyTag(uint64& startTagStart, uint64& startTagEnd, const uint64 startPos);

					//! \brief verify that character is one in a specified characterset
					//! \param c		character to test for
					//! \param charSet	set of characters
					//! \returns true if character found, or false otherwise
					inline bool verifyChar(const char c, const char* charSet)
					{
						return strchr(charSet,(int)c) != NULL;
					}

					//! \brief skip free space in buffer
					//! \param pos		startposition for freespaceskipping, returns at first non-freespace position
					inline void skipFreeSpace(uint64& pos)
					{
						while (pos < mBufferSize && verifyChar(mXMLFile[pos],XMLPARSER_VC_FREESPACE)) 
						{
							if (mXMLFile[pos] == XMLPARSER_VC_NEWLINE) mNewLinePos.add(pos);
							++pos;
						}

						if (pos == mBufferSize) 
						{
							XAIT_XMLPARSER_THROW_EXCEPTION_WITH_LINE("XMLParser: End of buffer reached.");
						}
					}

					//! \brief read check next character
					//! \param pos		position of the character to read, will be incremented by one if success
					//! \param c		check character		
					//! \returns false if next character is not equal to c, true otherwise
					inline bool isChar(uint64& pos, const char c)
					{
						if (pos >= mBufferSize) 
						{
							XAIT_XMLPARSER_THROW_EXCEPTION_WITH_LINE("XMLParser: End of buffer reached.");
						}
						if (mXMLFile[pos] != c) 
						{
							return false;
						}
						++pos;
						return true;
					}

					//! \brief read check next character againts a characterset
					//! \param pos		position of the character to read, will be incremented by one if success
					//! \param c		check characterset		
					//! \returns false if next character is not equal to c, true otherwise
					inline bool isChars(uint64& pos, const char* set)
					{
						if (pos >= mBufferSize) 
						{
							XAIT_XMLPARSER_THROW_EXCEPTION_WITH_LINE("XMLParser: End of buffer reached.");
						}
						if (!verifyChar(mXMLFile[pos],set)) 
						{
							return false;
						}
						++pos;
						return true;
					}

					//! \brief read complete starttag
					//! \param pos	position in front of starttag. leading freespace will be skipped
					//!				returns the position after the closing ">" symbol
					//! \returns the name of the element
					//! \remark This function sets the name of the last node in the tree and adds the attributes.
					char* readStartTag(uint64& pos);

					//! \brief read complete endttag and compare it to the specified startagname
					//! \param pos	postion in front of the starttag. leading freespace will be skipped
					//!				returns the position after the closing ">" symbol
					//! \param name	name of the starttag
					void readEndTag(uint64& pos, const char* name);

					//! \brief read a name, this can be element or attribute name
					//! \param pos		position in the buffer where to start reading, leading freespace will be ignored.
					//!					it returns the position after the name, if success
					//! \returns pointer to the start of the name, or throws an exception if an error occured
					//! \remark The returned string is not nullterminated, must be done with pos
					char* readName(uint64& pos);

					//! \brief read attributes and add it to the last item in the xmlBuilder
					//! \param pos		position in the buffer where to start reading. leading freespace will be ignored.
					//!					it returns the position after the last parsed position, if success
					void readAttributes(uint64& pos);

					//! \brief read the next tag as empty node
					//! \param pos		position in the buffer where to start. must be at the "<" position
					//!					it returns the position after the last parsed position, if success
					//! \remark Parses complete node, including endtag.
					void readNodeEmpty(uint64& pos);

					//! \brief read the next tag as textnode
					//! \param pos		position of the tag "<". it returns the position after the last parsed position, if success
					//! \param textPos	position in the buffer, where the text starts
					//! \remark Parses complete node, including endtag.
					void readNodeText(uint64& pos, uint64 textPos);

					//! \brief read the next tag as element node
					//! \param pos		position of the tag "<". it returns the position after the last parsed position, if success
					//! \remark Parses complete node, including endtag.
					//!			This function includes recursive calls
					void readNodeElement(uint64& pos);

					//! \brief read the next tag as include node
					//! \param pos		position of the tag "<". it returns the position after the last parsed position, if success
					//! \remark Opens the file in the include statement and parses it.			
					void readNodeInclude(uint64& pos);
				};


			}
		}
	}

}
