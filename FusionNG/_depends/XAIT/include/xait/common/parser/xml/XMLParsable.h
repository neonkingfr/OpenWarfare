// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/DLLDefines.h>

namespace XAIT
{
	namespace Common
	{
		namespace Parser
		{
			namespace XML
			{
				class XMLNodeElements;
				class XMLTreeBuilder;


				class XAIT_COMMON_DLLAPI XMLParsable
				{
				public:
					XMLParsable()
					{};

					virtual ~XMLParsable() {};

					//! \brief initialise an object from a xml subtree
					//! \param node		xmlsubtree
					//! \returns true if everything needed could be read from the xmltree
					virtual bool initFromXML(XMLNodeElements* node)= 0;

					//! \brief save the state of an object to a xml subtree
					//! \param tree		treebuilder for xmltrees
					virtual void writeToXML(XMLTreeBuilder* /*tree*/)
					{

					}
				};


			}
		}
	}


}
