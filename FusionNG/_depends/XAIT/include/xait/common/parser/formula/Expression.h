// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/String.h>
#include <xait/common/memory/Allocator.h>
#include <xait/common/parser/formula/FunctionType.h>

#include <xait/common/io/Stream.h>

#include <xait/common/parser/xml/XMLNode.h>

namespace XAIT
{
	namespace Common
	{
		namespace Parser
		{
			namespace Formula
			{

				class MathValue;
				class VariableResolver;
				class FunctionEnvironment;
				template<typename T_VALUETYPE>
				class TypedMathValue;


				class Expression
				{
					enum SplitHelp
					{
						SH_GENERIC,
						SH_ATOMIC,
						SH_DELETE,			
					};

				public:

					// the functiontype which can be used to extract a part of a disjunction
					typedef bool (*SplitFunction) (MathValue* leftValue);

					//! \brief struct used to handle error message while parsing expressions
					struct ErrorMsg
					{
						//! \brief contains the error message
						Common::String mErrorMsg;

						//! \brief checks if an error exists
						//! \returns true if an error exists, false otherwise
						bool existsError()
						{
							return mErrorMsg.getLength() != 0;
						};

						//! \brief raises an assertion if an error exists
						void raiseAssertion();
					};


					//! \brief operator classifier 
					enum Operation
					{
						OP_INVALID = 0,
						// first binding order
						OP_MEMBER_FUNCTION = 1, // todo remove?
						// second binding order
						OP_DIV = 5,
						OP_MUL = 6,
						// third binding order 
						OP_SUB = 10,
						OP_ADD = 11,					
						// fourth binding order - comp
						OP_SMALLER = 20,
						OP_SMALLEREQUAL = 21,
						OP_EQUAL = 22,
						OP_GREATEREQUAL = 23,
						OP_GREATER = 24,
						OP_UNEQUAL = 25,
						// fifth binding order - logical
						OP_AND = 30,
						OP_OR = 31,
						// error
						OP_NOOP = 100,
					};



					//! \brief  parse an expression store in an xmltree
					//! \param expressionNode			the xmlnode that contains the expression
					//! \param varResolver				the variableResolver that is used (converting non core expressions to mathvalues
					//! \param functionEnvironment		the functionEnvironment that is used (knows all functions and resolves the function nodes to mathvalues)
					//! \param allocator				the used allocator
					//! \returns the resulting mathvalue
					//! \remark if an error occurs an assertion is raised
					XAIT_COMMON_DLLAPI static MathValue* parse(Common::Parser::XML::XMLNodeElements* expressionNode,
						VariableResolver* varResolver, 
						FunctionEnvironment* functionEnvironment, 
						const Common::Memory::AllocatorPtr& allocator);


					//! \brief parse an expression store in an xmltree
					//! \param expressionNode			the xmlnode that contains the expression
					//! \param varResolver				the variableResolver that is used (converting non core expressions to mathvalues
					//! \param functionEnvironment		the functionEnvironment that is used (knows all functions and resolves the function nodes to mathvalues)
					//! \param allocator				the used allocator
					//! \param errorMsg					an error message container which stores occuring errors
					//! \param root						defines if an expression is subexpression (false) or the root of the expression(true)
					//! \returns the resulting mathvalue or NULL if an error occurred
					//! \remark normally you want to use the simplified overloaded function above
					XAIT_COMMON_DLLAPI static MathValue* parse(Common::Parser::XML::XMLNodeElements* expressionNode,
						VariableResolver* varResolver, 
						FunctionEnvironment* functionEnvironment, 
						const Common::Memory::AllocatorPtr& allocator, 
						ErrorMsg& errorMsg,
						bool root = true);


					//! \brief this function takes an mathvalue and call for each outer disjunction a specified function
					//! \param extractedTrees	the mathvalues which has been removed from the mathvalue
					//! \param mathvalue		the mathvalue to be checked
					//! \param splitfunction	the specified split function (returns bool if part has to be extracted)
					//! \returns the new mathvalue without the extracted subtrees
					//! \remark	the subtrees that result in a true returnvalue of the splitfunction will be removed from the entire root tree
					XAIT_COMMON_DLLAPI static MathValue* extractFromDisjunction(Common::Container::Vector<MathValue*>& extractedTrees, MathValue* mathValue,SplitFunction splitFunction);

					//! \brief checks if a certain function occurs in a direct child node of the expression
					//! \param functionType		the searched function type
					//! \param mathValue		the mathvalue
					//! \returns true if the functiontype was found, false otherwise
					XAIT_COMMON_DLLAPI static bool containsFunctionInDisjunction(FunctionType functionType, MathValue* mathValue);

					//! \brief splits a disjunction in its components
					//! \param mathValues		the resulting mathvalues (returnValue)
					//! \param originalValue	the original mathvalue
					//! \remark the original value will be deleted
					XAIT_COMMON_DLLAPI static void splitDisjunction(Common::Container::Vector<TypedMathValue<bool>*>& mathValues,TypedMathValue<bool>* originalValue);

					//! \brief creates a conjunction of the given boolean mathvalues
					//! \param mathValues		the given boolean mathvalues
					//! \param allocator		the allocator that is used to create the new mathvalues
					//! \returns the resulting mathvalue
					XAIT_COMMON_DLLAPI static TypedMathValue<bool>* joinTests(Common::Container::Vector<TypedMathValue<bool>*>& mathValues, 
						const Common::Memory::AllocatorPtr& allocator);




				private:

					static inline uint32 getExpressionRank(const MathValue* mathValue, uint32 (*getRank)(uint32),const Common::Memory::AllocatorPtr& allocator);

					static inline MathValue* createDatatypeFunctionMathValue(Common::Parser::XML::XMLNodeElements* expression,
						VariableResolver* varResolver, FunctionEnvironment* functionEnvironment,
						const Common::Memory::AllocatorPtr& allocator, ErrorMsg& errorMsg);


					static inline MathValue* createArgumentListMathValue(Common::Parser::XML::XMLNodeElements* expression,
						VariableResolver* varResolver, FunctionEnvironment* functionEnvironment,
						const Common::Memory::AllocatorPtr& allocator, ErrorMsg& errorMsg);

				};
			}
		}
	}
}
