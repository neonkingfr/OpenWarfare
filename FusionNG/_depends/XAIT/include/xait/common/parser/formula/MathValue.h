// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/memory/MemoryManaged.h>
#include <xait/common/math/Vec2.h>
#include <xait/common/math/Vec3.h>
#include <xait/common/container/Array.h>
#include <xait/common/parser/formula/Expression.h>
#include <xait/common/parser/formula/FunctionType.h>
#include <xait/common/parser/formula/VariableAccessor.h>
#include <xait/common/parser/formula/FunctionEnvironment.h>
#include <xait/common/parser/formula/VariableAccessor.h>
#include <xait/common/reflection/datatype/DatatypeManager.h>
#include <xait/common/reflection/datatype/DatatypeBase.h>
#include <xait/common/reflection/ReflectionInterface.h>
#include <xait/common/parser/formula/MathValueIdentifier.h>

#define XAIT_MATHVALUE_TYPE_FUNCTIONS(className) \
	uint32 getMathValueTypeID() const \
	{ \
	return XAIT::Common::Parser::Formula::MathValueTypeID<className>::getMathValueTypeID(); \
	}

namespace XAIT
{
	namespace Common
	{
		namespace Parser
		{
			namespace Formula
			{

				//! \brief the untyped base class of all mathvalues
				class MathValue : public Common::Memory::MemoryManaged
				{
				public:
					//! \brief constructor
					XAIT_COMMON_DLLAPI MathValue()
					{}

					//! \brief destructor
					XAIT_COMMON_DLLAPI virtual ~MathValue()
					{}

					//! \brief	gets the type of a mathvalue
					//! \returns the type of the value
					XAIT_COMMON_DLLAPI virtual Reflection::Datatype::DatatypeID getDatatypeID() const = 0;

					//! \brief check if a mathvalue is constant
					//! \returns true if value is constant; false otherwise
					XAIT_COMMON_DLLAPI virtual bool isConstant()
					{
						return true;
					}

					//! \brief gets the operationtype of the mathvalue
					//! \returns the operationtype of the function
					XAIT_COMMON_DLLAPI virtual Expression::Operation getOperationType() 
					{ 
						return Expression::OP_NOOP;
					}

					//! \brief gets the functiontype of the mathvalue
					//! \returns the functiontype of the mahtvalue
					XAIT_COMMON_DLLAPI virtual FunctionType getFunctionType()
					{
						return FT_NOFUNC;
					};

					//! \brief gets the contained variables of the mathvalue
					//! \param variables		the contained variables		(return value)
					XAIT_COMMON_DLLAPI virtual void getContainingVariables(Common::Container::Vector<uint32>& /*variables*/) const
					{}

					//! \brief check if a value can be merged to an atomic mathvalue
					//! \returns pointer to new mathvalue if merge was possible; NULL otherwise
					XAIT_COMMON_DLLAPI virtual MathValue* merge()
					{
						return NULL;
					}

					//! \brief checks if the value is a referenced value
					//! \returns true if the mathvalue is a referenced value; false otherwise
					XAIT_COMMON_DLLAPI virtual bool isReferenceValue() const
					{
						return false;
					}

					//! \brief replaces variable ids
					//! \param oldAccessID		the old variableID
					//! \param newAccessID		the new variableID
					XAIT_COMMON_DLLAPI virtual void replaceVariableAccessID(uint32 /*oldAccessID*/, uint32 /*newAccessID*/)
					{
						return;
					}

					//! \brief gets the datatype of the left operand
					//! \returns the datatype of the left operand
					//! \remark if the mathvalue is not an operator mathvalue an invalid datatypeid is returned
					XAIT_COMMON_DLLAPI virtual Reflection::Datatype::DatatypeID getLeftDatatypeID()
					{
						return Reflection::Datatype::DatatypeID(); 
					}


					//! \brief gets the datatype of the right operand
					//! \returns the datatype of the right operand
					//! \remark if the mathvalue is not an operator mathvalue an invalid datatypeid is returned
					XAIT_COMMON_DLLAPI virtual Reflection::Datatype::DatatypeID getRightDatatypeID()
					{
						return Reflection::Datatype::DatatypeID();
					}

					//! \brief evalutes the mathvalue and write the result in the given memory
					//! \param value			pointer the memory that is used to store the result
					//! \param varAccessor		the used variable accessor (used by variable mathvalues to get the value)
					//! \remark the memory used by the result has to be allocated
					XAIT_COMMON_DLLAPI virtual void evaluateToPtr(void* value, VariableAccessor* varAccessor) = 0;

					//! \brief returns a pointer to the referenced value (only usable with reference values)
					//! \param varAccessor	the used variable accessor (used by variable mathvalues to get the value)
					//! \returns a pointer to the referenced value, NULL if the mathvalue is not a reference value
					XAIT_COMMON_DLLAPI virtual void* getValueReference(VariableAccessor* varAccessor)
					{
						return NULL;
					}
					
					//! \brief sets leftsided mathvalues by the given pointer
					//! \param value the value to set to the mathvalue
					//! \param varAccessor	the used variable accessor (used by variable mathvalues to get the value)
					//! \remark this method is only available in mathvalues that can be placed on the left side of an assignment
					XAIT_COMMON_DLLAPI virtual void setByPtr(void* value, VariableAccessor* varAccessor)
					{
						X_ASSERT_MSG(false, "Not supported for general expressions");
					}


					//! \brief creates a copy of the mathvalue
					//! \returns a copy of the mathvalue
					XAIT_COMMON_DLLAPI virtual MathValue* clone() const = 0;

					//! \brief compares two mathvalue
					//! \returns true if the mathvalues are equal, false otherwise
					//! \remark the binary tree is compared the expression itself
					XAIT_COMMON_DLLAPI virtual bool compare(MathValue* mathValue) const = 0;

					//! \brief gets the id of the mathvaluetype (replaces rtti)
					//! \returns the mathvaluetypeid
					XAIT_COMMON_DLLAPI virtual uint32 getMathValueTypeID() const = 0;

					XAIT_COMMON_DLLAPI virtual Common::String convertToString() const = 0;


				protected:

				};

				//! \brief a typed mathvalue baseclass
				template<typename T_VALUE_TYPE>
				class TypedMathValue : public MathValue
				{
				public:
					//! \brief constructor
					TypedMathValue()
					{}

					//! \brief destructor
					virtual ~TypedMathValue()
					{}

					//! \brief	gets the type of a mathvalue
					//! \returns the type of the value
					virtual Reflection::Datatype::DatatypeID getDatatypeID() const
					{
						return GET_DATATYPE_ID(T_VALUE_TYPE);
					}

					//! \brief evalutes the mathvalue
					//! \param value			returnvalue
					//! \param varAccessor		pointer to a class needed to get variable values
					virtual void evaluate(T_VALUE_TYPE& value,VariableAccessor* varAccessor) = 0;
				};


				template<typename T_VALUE_TYPE>
				class AtomicMathValue : public TypedMathValue<T_VALUE_TYPE>
				{
				public:

					//! \brief constructor
					//! \param value	value
					AtomicMathValue(T_VALUE_TYPE value) 
						: TypedMathValue<T_VALUE_TYPE>(),
						mValue(value)
					{}

					//! \brief destructor
					virtual ~AtomicMathValue()
					{}

					//! \brief constructor , converts a constant mathvalue to an atomic mathvalue
					//! \param mathValue		an constant mathvalue
					AtomicMathValue(TypedMathValue<T_VALUE_TYPE>* mathValue)
					{
						if (mathValue->isConstant())
							mathValue->evaluate(mValue,0);
						else
							XAIT_THROW_EXCEPTION( BString("cannot init atomic value with non static value"));
					}

					//! \see TypedMathValue
					virtual void evaluate(T_VALUE_TYPE& value,VariableAccessor* /*varAccessor*/)
					{
						value = mValue;
					}

					//! \see MathValue
					virtual void evaluateToPtr(void* value, VariableAccessor* /*varAccessor*/)
					{
						*(T_VALUE_TYPE*)value = mValue;
					}

					//! \see MathValue
					virtual MathValue* clone() const 
					{
						return new(TypedMathValue<T_VALUE_TYPE>::mAllocator) AtomicMathValue<T_VALUE_TYPE>(mValue);
					}

					//! \see MathValue
					virtual bool compare(MathValue* mathValue) const
					{
						if (getMathValueTypeID() != mathValue->getMathValueTypeID())
							return false;

						AtomicMathValue* other = (AtomicMathValue<T_VALUE_TYPE>*)(mathValue);

						return (mValue == other->mValue);
					}

					virtual Common::String convertToString() const
					{
						return "constantValue";			
					}

				protected:
					T_VALUE_TYPE	mValue;		

					XAIT_MATHVALUE_TYPE_FUNCTIONS(AtomicMathValue<T_VALUE_TYPE>);
				};

				//! \brief represents an untyped atom mathvalue
				class UntypedAtomicMathValue : public MathValue
				{
				public:
					//! \brief constructor
					//! \param value		the value that is stored
					//! \param datatypeID	the Reflection::Datatype::DatatypeID of the value
					UntypedAtomicMathValue(void* value, const Reflection::Datatype::DatatypeID datatypeID);

					//! \destructor
					virtual ~UntypedAtomicMathValue();

					//! \see MathValue
					virtual void evaluateToPtr(void* value, VariableAccessor* varAccessor);

					//! \see MathValue
					virtual Reflection::Datatype::DatatypeID getDatatypeID() const;

					//! \see MathValue
					virtual MathValue* clone() const;

					//! \see MathValue
					virtual bool compare(MathValue* mathValue) const;

					virtual Common::String convertToString() const
					{
						return "constantValue";			
					}

				protected:
					Reflection::Datatype::DatatypeBase* mDatatype;
					Reflection::Datatype::DatatypeID	mDatatypeID;
					void*								mValue;

					XAIT_MATHVALUE_TYPE_FUNCTIONS(UntypedAtomicMathValue);
				};

				//! \brief base class of variable mathvalue (which means not constant)
				template<typename T_VALUE_TYPE>
				class VariableMathValue : public TypedMathValue<T_VALUE_TYPE>
				{
				public:
					//! \brief constructor
					VariableMathValue()
						: TypedMathValue<T_VALUE_TYPE>()
					{}

					//! \brief destructor
					virtual ~VariableMathValue()
					{}

					//! \see MathValue
					virtual bool isConstant()
					{
						return false;
					}

					//! \see MathValue
					virtual void replaceVariableAccessID(uint32 oldAccessID, uint32 newAccessID) = 0;
				};


				//! \brief mathvalue to negate other boolean mathvalues
				class NegatedMathValue : public TypedMathValue<bool>
				{
				public:
					//! \brief constructor
					//! \param value	mathvalue to be negated
					NegatedMathValue(TypedMathValue<bool>* value);

					//! destructor
					virtual ~NegatedMathValue();

					//! \see TypedMathValue
					virtual void evaluate(bool& value, VariableAccessor* varAccessor);

					//! \see MathValue
					virtual bool isConstant();

					//! \see MathValue
					virtual void getContainingVariables(Common::Container::Vector<uint32>& variables) const;

					//! \see MathValue
					virtual void evaluateToPtr(void* value,VariableAccessor* varAccessor);

					//! \see MathValue
					virtual MathValue* clone() const;

					//! \see MathValue
					virtual bool compare(MathValue* mathValue) const;

					virtual Common::String convertToString() const
					{
						return String("!") + mMathValue->convertToString();			
					}

				protected:
					TypedMathValue<bool>* mMathValue;

					XAIT_MATHVALUE_TYPE_FUNCTIONS(NegatedMathValue);
				};

				//! \brief mathvalue to represent typed function calls
				//! \remark not threading safe
				template<typename T_VALUETYPE>
				class FunctionMathValue : public VariableMathValue<T_VALUETYPE>
				{
				public:
					//! \brief constructor
					//! \param functionID			the id of the function
					//! 'param functionEnvironment	the use functionEn
					FunctionMathValue(const Reflection::Function::FunctionID functionID, 
						FunctionEnvironment* functionEnvironment, 
						const Common::Container::Vector<MathValue*>& argumentValues,
						const Container::Vector<Common::Reflection::Datatype::DatatypeID>& signature)
						: VariableMathValue<T_VALUETYPE>(),
						mFunctionID(functionID), 
						mArgumentList(signature,(TypedMathValue<T_VALUETYPE>::mAllocator))
					{
						uint32 i=0;
						for (; i<argumentValues.size(); ++i)
						{
							mArgumentMathValues[i] = argumentValues[i];
						}
						for (; i<10; ++i)
							mArgumentMathValues[i] = NULL;
					}

					//! \brief copy constructor
					//! \param other		the other functionmathvalue
					FunctionMathValue(const FunctionMathValue<T_VALUETYPE>& other)
						: VariableMathValue<T_VALUETYPE>(),
						mFunctionID(other.mFunctionID), 
						mArgumentList(other.mArgumentList)
					{
						uint32 size = mArgumentList.size();
						uint32 i=0;
						for(;i < size; ++i)
						{
							mArgumentMathValues[i] = other.mArgumentMathValues[i]->clone();
						}
						for (; i<10; ++i)
						{
							mArgumentMathValues[i] = NULL;
						}
					}

					//! \param destructor
					virtual ~FunctionMathValue()
					{
						for (uint32 i=0; i<10; ++i)
							delete mArgumentMathValues[i];
					}


					//! \see MathValue
					virtual void getContainingVariables(Common::Container::Vector<uint32>& variables) const
					{
						for (uint32 i=0; i<10; ++i)
						{
							if (mArgumentMathValues[i])
								mArgumentMathValues[i]->getContainingVariables(variables);
						}
					}

					//! \see MathValue
					virtual void replaceVariableAccessID(uint32 oldAccessID, uint32 newAccessID)
					{
						for (uint32 i=0; i<10; ++i)
						{
							if (mArgumentMathValues[i])
								mArgumentMathValues[i]->replaceVariableAccessID(oldAccessID,newAccessID);
						}
					}

					//! \see MathValue
					virtual MathValue* merge()
					{
						T_VALUETYPE value;
						evaluate(value,0);
						return new(TypedMathValue<T_VALUETYPE>::mAllocator) AtomicMathValue<T_VALUETYPE>(value);
					}

					//! \see MathValue
					virtual void evaluateToPtr(void* value,VariableAccessor* varAccessor)
					{

						for (uint32 i=0; i<10; ++i)
						{
							if (mArgumentMathValues[i])
								mArgumentMathValues[i]->evaluateToPtr(mArgumentList.getValuePtr(i),varAccessor);
						}
						varAccessor->getFunctionRegistrar()->callFunction(value,mFunctionID,NULL,mArgumentList);
					}

					//! \see TypedMathValue

					virtual void evaluate(T_VALUETYPE& value,VariableAccessor* varAccessor)
					{

						for (uint32 i=0; i<10; ++i)
						{
							if (mArgumentMathValues[i])
								mArgumentMathValues[i]->evaluateToPtr(mArgumentList.getValuePtr(i),varAccessor);
						}
						varAccessor->getFunctionRegistrar()->callFunction(&value,mFunctionID,NULL,mArgumentList);
					}

					//! \see MathValue
					virtual Reflection::Datatype::DatatypeID getDatatypeID() const
					{
						return GET_DATATYPE_ID(T_VALUETYPE);
					}

					//! \see MathValue
					virtual MathValue* clone() const
					{
						return new(TypedMathValue<T_VALUETYPE>::mAllocator)  FunctionMathValue<T_VALUETYPE>(*this);
					}

					//! \see MathValue
					virtual bool compare(MathValue* mathValue) const
					{
						if (getMathValueTypeID() != mathValue->getMathValueTypeID())
							return false;

						FunctionMathValue<T_VALUETYPE> *other = (FunctionMathValue<T_VALUETYPE>*)(mathValue);


						if (other->mFunctionID != mFunctionID)
							return false;


						if (other->mArgumentList.size() != mArgumentList.size())
							return false;

						for (uint32 i=0; i<mArgumentList.size(); ++i)
						{
							if (!mArgumentMathValues[i]->compare(other->mArgumentMathValues[i]))
								return false;
						}

						return true;
					}

					virtual Common::String convertToString() const
					{
						return "functioncall";			
					}

				protected:
					Reflection::Function::FunctionID			mFunctionID;
					Reflection::Function::FunctionArgumentList	mArgumentList;
					MathValue*									mArgumentMathValues[10];

					XAIT_MATHVALUE_TYPE_FUNCTIONS(FunctionMathValue<T_VALUETYPE>);
				};

				//! \brief mathvalue to represent untyped function calls
				//! \remark not threading safe
				class UntypedFunctionMathValue : public MathValue
				{
				public:
					//! \brief constructor
					//! \param functionID				functionID
					//! \param functionEnvironment		the used functionEnvironment
					//! \param
					UntypedFunctionMathValue(const Reflection::Function::FunctionID functionID, 
						FunctionEnvironment* functionEnvironment, 
						const Common::Container::Vector<MathValue*>& argumentValues, 
						const Container::Vector<Reflection::Datatype::DatatypeID>& signature,
						const Reflection::Datatype::DatatypeID datatypeID);

					//! \brief copy constructor
					//! \param other		the other UntypedFunctionMathValue
					UntypedFunctionMathValue(const UntypedFunctionMathValue& other);

					//! \brief destructor
					virtual ~UntypedFunctionMathValue();

					//! \see MathValue
					virtual bool isConstant();


					//! \see MathValue
					virtual void getContainingVariables(Common::Container::Vector<uint32>& variables) const;

					//! \see MathValue
					virtual void replaceVariableAccessID(uint32 oldAccessID, uint32 newAccessID);

					//! \see MathValue
					virtual void evaluateToPtr(void* value,VariableAccessor* varAccessor);

					//! \see MathValue
					virtual Reflection::Datatype::DatatypeID getDatatypeID() const;

					//! \see MathValue
					virtual MathValue* clone() const;

					//! \see MathValue
					virtual bool compare(MathValue* mathValue) const;

					virtual Common::String convertToString() const
					{
						return "functioncall";			
					}

				protected:
					Reflection::Datatype::DatatypeID			mDatatypeID;
					Reflection::Function::FunctionID			mFunctionID;
					Reflection::Function::FunctionArgumentList	mArgumentList;
					MathValue*									mArgumentMathValues[10];

					XAIT_MATHVALUE_TYPE_FUNCTIONS(UntypedFunctionMathValue);
				};


				//! \brief mathvalue that call a datatype member function 
				template<typename T_VALUETYPE>
				class DatatypeFunctionMathValue : public VariableMathValue<T_VALUETYPE>
				{
				public:
					//! \brief constructor
					//! \param functionID				the id of the function
					//! \param ownerMathValue			the mathvalue that is used to evalute the owner of the function
					//! \param functionEnvironment		the used function environment
					//! \param argumentValues			the mathvalues that are used to evaluate the argumentlist of the function
					//! \param signature				the signature of the function
					DatatypeFunctionMathValue(const Reflection::Function::FunctionID functionID, 
						MathValue* ownerMathValue, 
						const Common::Container::Vector<MathValue*>& argumentValues, 
						const Container::Vector<Reflection::Datatype::DatatypeID>& signature)
						: VariableMathValue<T_VALUETYPE>(),
						mFunctionID(functionID),
						mOwnerMathValue(ownerMathValue),
						mArgumentList(signature,(TypedMathValue<T_VALUETYPE>::mAllocator)), 
						mArgumentCount(argumentValues.size()),
						mOwnerValue(NULL)
					{
						mOwnerDatatype = Reflection::ReflectionInterface::getDatatypeManager()->getDatatype(mOwnerMathValue->getDatatypeID());

						if (!mOwnerMathValue->isReferenceValue())
						{
							mOwnerValue = mOwnerDatatype->createHeapObject();
						}

						uint32 i=0;
						for (; i<argumentValues.size(); ++i)
						{
							mArgumentMathValues[i] = argumentValues[i];
						}
						for (; i<10; ++i)
							mArgumentMathValues[i] = NULL;
					}
			

					//! \brief	copy constructor
					//! \param other	the other DatatypeFunctionMathValue
					DatatypeFunctionMathValue(const DatatypeFunctionMathValue& other)
						: VariableMathValue<T_VALUETYPE>(), 
						mFunctionID(other.mFunctionID), 
						mOwnerMathValue(other.mOwnerMathValue->clone()),
						mOwnerDatatype(other.mOwnerDatatype),
						mArgumentList(other.mArgumentList),
						mArgumentCount(other.mArgumentCount),
						mOwnerValue(NULL)
					{
						if (!mOwnerMathValue->isReferenceValue())
							mOwnerValue = mOwnerDatatype->createHeapObject(other.mOwnerValue);

						uint32 i=0;
						for (;i<mArgumentCount; ++i)
						{
							mArgumentMathValues[i] = other.mArgumentMathValues[i]->clone();
						}
						for (; i<10; ++i)
							mArgumentMathValues[i] = NULL;
					}


					//! \brief destructor
					virtual ~DatatypeFunctionMathValue()
					{
						for (uint32 i=0; i<mArgumentCount; ++i)
							delete mArgumentMathValues[i];

						if (!mOwnerMathValue->isReferenceValue())
							Reflection::ReflectionInterface::getDatatypeManager()->getDatatype(mOwnerMathValue->getDatatypeID())->destroyHeapObject(mOwnerValue);
						
						delete mOwnerMathValue;
					}


					//! \see MathValue
					virtual void getContainingVariables(Common::Container::Vector<uint32>& variables) const
					{
						for (uint32 i=0; i<mArgumentCount; ++i)
						{
							mArgumentMathValues[i]->getContainingVariables(variables);
						}
					}

					//! \see MathValue
					virtual void replaceVariableAccessID(uint32 oldAccessID, uint32 newAccessID)
					{
						for (uint32 i=0; i<mArgumentCount; ++i)
						{
							mArgumentMathValues[i]->replaceVariableAccessID(oldAccessID,newAccessID);
						}
					}

					//! \see TypedMathValue
					virtual void evaluate(T_VALUETYPE& value,VariableAccessor* varAccessor)
					{
						for (uint32 i=0; i<mArgumentCount; ++i)
						{
							mArgumentMathValues[i]->evaluateToPtr(mArgumentList.getValuePtr(i),varAccessor);
						}
						
						if (mOwnerMathValue->isReferenceValue())
							mOwnerValue = mOwnerMathValue->getValueReference(varAccessor);
						else
							mOwnerMathValue->evaluateToPtr(mOwnerValue,varAccessor);

						mOwnerDatatype->callFunction((void*)&value,mFunctionID,mOwnerValue,mArgumentList);
					}

					//! \see MathValue
					virtual void evaluateToPtr(void* value,VariableAccessor* varAccessor)
					{
						for (uint32 i=0; i<mArgumentCount; ++i)
						{
							mArgumentMathValues[i]->evaluateToPtr(mArgumentList.getValuePtr(i),varAccessor);
						}
						if (mOwnerMathValue->isReferenceValue())
							mOwnerValue = mOwnerMathValue->getValueReference(varAccessor);
						else
							mOwnerMathValue->evaluateToPtr(mOwnerValue,varAccessor);

						if (value != NULL)
							mOwnerDatatype->callFunction(value,mFunctionID,mOwnerValue,mArgumentList);
						else
							mOwnerDatatype->callFunction(mFunctionID,mOwnerValue,mArgumentList);
					}

					//! \see MathValue
					MathValue* clone() const
					{
						return new(TypedMathValue<T_VALUETYPE>::mAllocator) DatatypeFunctionMathValue<T_VALUETYPE>(*this);
					}

					//! \see MathValue
					virtual bool compare(MathValue* mathValue) const
					{
						if (getMathValueTypeID() != mathValue->getMathValueTypeID())
							return false;
						
						DatatypeFunctionMathValue<T_VALUETYPE>* other = (DatatypeFunctionMathValue<T_VALUETYPE>*)(mathValue);

						if (other->mFunctionID != mFunctionID)
							return false;

						if (other->mArgumentList.size() != mArgumentList.size())
							return false;

						for (uint32 i=0; i<mArgumentList.size(); ++i)
						{
							if (!mArgumentMathValues[i]->compare(other->mArgumentMathValues[i]))
								return false;
						}

						return true;
					}

					virtual Common::String convertToString() const
					{
						return "memberfunction";			
					}

				protected:
					Reflection::Function::FunctionID			mFunctionID;
					MathValue*									mOwnerMathValue;
					Reflection::Datatype::DatatypeBase*			mOwnerDatatype;

					Reflection::Function::FunctionArgumentList	mArgumentList;
					Common::Container::Array<MathValue*,10>		mArgumentMathValues;
					uint32										mArgumentCount;
					void*										mOwnerValue;

					XAIT_MATHVALUE_TYPE_FUNCTIONS(DatatypeFunctionMathValue<T_VALUETYPE>);
				};


				//! \brief untyped mathvalue that call a datatype member function 
				class UntypedDatatypeFunctionMathValue : public MathValue
				{
				public:
					//! \brief constructor
					//! \param functionID				the id of the function
					//! \param ownerMathValue			the mathvalue that is used to evalute the owner of the function
					//! \param datatype					the returntype of the function
					//! \param functionEnvironment		the used function environment
					//! \param argumentValues			the mathvalues that are used to evaluate the argumentlist of the function
					//! \param signature				the signature of the function
					UntypedDatatypeFunctionMathValue(const Reflection::Function::FunctionID functionID, 
													MathValue* ownerMathValue, 
													Reflection::Datatype::DatatypeID datatypeID,
													const Common::Container::Vector<MathValue*>& argumentValues, 
													const Container::Vector<Reflection::Datatype::DatatypeID>& signature);

					//! \brief copy constructor
					//! \param other	the other UntypedDatatypeFunctionMathValue
					UntypedDatatypeFunctionMathValue(const UntypedDatatypeFunctionMathValue& other);


					//! \brief destructor
					virtual ~UntypedDatatypeFunctionMathValue();

					//! \see MathValue
					virtual void getContainingVariables(Common::Container::Vector<uint32>& variables) const;

					//! \see MathValue
					virtual bool isConstant();

					//! \see MathValue
					virtual Reflection::Datatype::DatatypeID getDatatypeID() const;

					//! \see MathValue
					virtual void replaceVariableAccessID(uint32 oldAccessID, uint32 newAccessID);

					//! \see MathValue
					virtual void evaluateToPtr(void* value,VariableAccessor* varAccessor);

					//! \see MathValue
					virtual MathValue* clone() const;

					//! \see MathValue
					virtual bool compare(MathValue* mathValue) const;

					XAIT_COMMON_DLLAPI virtual Common::String convertToString() const
					{
						return "memberfunction";			
					}


				protected:
					Reflection::Datatype::DatatypeID			mDatatypeID;
					Reflection::Datatype::DatatypeBase*			mOwnerDatatype;
					Reflection::Function::FunctionID			mFunctionID;
					MathValue*									mOwnerMathValue;

					Reflection::Function::FunctionArgumentList	mArgumentList;
					Common::Container::Vector<MathValue*>		mArgumentMathValues;
					void*										mOwnerValue;

					XAIT_MATHVALUE_TYPE_FUNCTIONS(UntypedDatatypeFunctionMathValue);
				};
			}			
		}
	}
}

