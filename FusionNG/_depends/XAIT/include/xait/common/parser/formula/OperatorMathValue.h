// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/parser/formula/MathValue.h>

// makros to create the operator math values
#define OPERATIONAL_MATH_VALUE(name,operation,enumName) \
	template<typename T_VALUETYPE,typename T_ARG_TYPE2> \
class name##MathValue : public OperatorMathValue<T_VALUETYPE,T_VALUETYPE,T_ARG_TYPE2> \
	{ \
	typedef name##MathValue<T_VALUETYPE,T_ARG_TYPE2> MyType; \
	public: \
	name##MathValue(TypedMathValue<T_VALUETYPE>* leftHandSide,TypedMathValue<T_ARG_TYPE2>* rightHandSide) \
	: OperatorMathValue<T_VALUETYPE, T_VALUETYPE, T_ARG_TYPE2>(leftHandSide,rightHandSide) \
		{} \
		\
		virtual ~name##MathValue() \
		{} \
		\
		virtual void evaluate(T_VALUETYPE& value,VariableAccessor* varAccessor) \
		{ \
		T_VALUETYPE value1; \
		T_ARG_TYPE2 value2; \
		\
		(MyType::mLeftHandSide)->evaluate(value1,varAccessor); \
		(MyType::mRightHandSide)->evaluate(value2,varAccessor); \
		\
		value = (value1 operation value2); \
		} \
		\
		virtual Expression::Operation getOperationType() \
		{ \
		return Expression::enumName; \
		} \
		virtual MathValue* clone() const \
		{ \
		return new(TypedMathValue<T_VALUETYPE>::mAllocator) name##MathValue((TypedMathValue<T_VALUETYPE>*)(MyType::mLeftHandSide)->clone(),(TypedMathValue<T_ARG_TYPE2>*)(MyType::mRightHandSide)->clone()); \
		} \
		\
		virtual bool compare(MathValue* mathValue) const \
		{ \
		if (getMathValueTypeID() != mathValue->getMathValueTypeID()) \
			return false; \
		name##MathValue<T_VALUETYPE,T_ARG_TYPE2>* other = (name##MathValue<T_VALUETYPE,T_ARG_TYPE2>*)(mathValue); \
		\
		return ((MyType::mLeftHandSide)->compare(other->mLeftHandSide) &&  \
			    (MyType::mRightHandSide)->compare(other->mRightHandSide)); \
		} \
		\
		virtual Common::String convertToString() const \
		{ \
		return (MyType::mLeftHandSide)->convertToString() + Common::String(#operation) + (MyType::mRightHandSide)->convertToString(); \
		} \
	protected: \
		  XAIT_MATHVALUE_TYPE_FUNCTIONS(MyType); \
	};  \


namespace XAIT
{
	namespace Common
	{
		namespace Parser
		{
			namespace Formula
			{
				//! \brief base class of the typed operator mathvalues
				template<typename T_VALUETYPE,typename T_ARG_TYPE1, typename T_ARG_TYPE2>
				class OperatorMathValue : public TypedMathValue<T_VALUETYPE>
				{
					friend class Expression; 
				public:
					//! \brief constructor
					//! \param leftHandSide		pointer to a mathvalue representing the left side of an operation
					//! \param rightHandSide	pointer to a mathvalue representing the right side of an operation
					OperatorMathValue(TypedMathValue<T_ARG_TYPE1>* leftHandSide,TypedMathValue<T_ARG_TYPE2>* rightHandSide )
						: TypedMathValue<T_VALUETYPE>(),
						  mLeftHandSide(leftHandSide), 
						  mRightHandSide(rightHandSide)
					{}

					//! \brief destructor
					virtual ~OperatorMathValue()
					{
						delete mLeftHandSide;
						delete mRightHandSide;
					}

					//! \see MathValue
					virtual bool isConstant()
					{
						return (mLeftHandSide->isConstant() && mRightHandSide->isConstant());
					}

					//! \see MathValue
					virtual Expression::Operation getOperationType() 
					{ 
						return Expression::OP_INVALID;
					}

					//! \see MathValue
					virtual Reflection::Datatype::DatatypeID getLeftDatatypeID()
					{
						return GET_DATATYPE_ID(T_ARG_TYPE1); 
					}

					//! \see MathValue
					virtual Reflection::Datatype::DatatypeID getRightDatatypeID()
					{
						return GET_DATATYPE_ID(T_ARG_TYPE2);
					}

					//! \see MathValue
					virtual MathValue* merge()
					{
						if (mLeftHandSide->isConstant() && mRightHandSide->isConstant())
						{
							return new(TypedMathValue<T_VALUETYPE>::mAllocator) AtomicMathValue<T_VALUETYPE>(this);
						}
						else
						{
							TypedMathValue<T_ARG_TYPE1>* newLeftHandSide =  (TypedMathValue<T_ARG_TYPE1>*)mLeftHandSide->merge();
							TypedMathValue<T_ARG_TYPE2>* newRightHandSide = (TypedMathValue<T_ARG_TYPE2>*)mRightHandSide->merge();

							if(newLeftHandSide)
							{
								delete mLeftHandSide;
								mLeftHandSide = newLeftHandSide;
							}
							if(newRightHandSide)
							{
								delete mRightHandSide;
								mRightHandSide = newRightHandSide;
							}
							return 0;
						}

					}

					//! \see MathValue
					virtual void getContainingVariables(Common::Container::Vector<uint32>& variables) const
					{
						mLeftHandSide->getContainingVariables(variables);
						mRightHandSide->getContainingVariables(variables);
					}

					//! \see MathValue
					virtual void replaceVariableAccessID(uint32 oldAccessID, uint32 newAccessID)
					{
						mLeftHandSide->replaceVariableAccessID(oldAccessID,newAccessID);
						mRightHandSide->replaceVariableAccessID(oldAccessID,newAccessID);
					}

					//! \see TypedMathValue
					virtual void evaluate(T_VALUETYPE& value,VariableAccessor* varAccessor) = 0;

					//! \see MathValue
					virtual void evaluateToPtr(void* value,VariableAccessor* varAccessor) 
					{ 
						evaluate(*(T_VALUETYPE*)value,varAccessor); 
					} 

				protected:
					TypedMathValue<T_ARG_TYPE1>* mLeftHandSide;
					TypedMathValue<T_ARG_TYPE2>* mRightHandSide;


				};

				//! \brief base class of all untyped operator mathvalues
				class UntypedOperatorMathValue : public MathValue
				{
				public:
					//! \brief constructor
					//! \param datatypeID		the datatype of the mathvalue
					//! \param leftHandSide		the left operand of the operation
					//! \param rightHandSide		the right operand of the operation
					UntypedOperatorMathValue(Reflection::Datatype::DatatypeID datatypeID,MathValue* leftHandSide,MathValue* rightHandSide);

					//! \brief destructor
					virtual ~UntypedOperatorMathValue();


					virtual bool isConstant();

					virtual Reflection::Datatype::DatatypeID getDatatypeID() const;

					virtual void getContainingVariables(Common::Container::Vector<uint32>& variables) const
					{
						mLeftHandSide->getContainingVariables(variables);
						mRightHandSide->getContainingVariables(variables);
					}

					virtual Reflection::Datatype::DatatypeID getLeftDatatypeID()
					{
						return mLeftHandSide->getDatatypeID(); 
					}

					virtual Reflection::Datatype::DatatypeID getRightDatatypeID()
					{
						return mRightHandSide->getDatatypeID();
					}

					//! \see MathValue
					virtual MathValue* merge();

					virtual void replaceVariableAccessID(uint32 oldID, uint32 newID)
					{
						mLeftHandSide->replaceVariableAccessID(oldID,newID);
						mRightHandSide->replaceVariableAccessID(oldID,newID);
					}


					Reflection::Datatype::DatatypeBase*		mDatatype;
					MathValue*								mLeftHandSide;
					MathValue*								mRightHandSide;
					void*									mOwnerValue;
					void*									mArgumentValue;


				protected:
					XAIT_MATHVALUE_TYPE_FUNCTIONS(UntypedOperatorMathValue); 
				};


#if XAIT_OS == XAIT_OS_WIN
#pragma warning(push)
#pragma warning(disable: 4244)
#pragma warning(disable: 4018)
#endif
				OPERATIONAL_MATH_VALUE(Addition,+,OP_ADD);
				OPERATIONAL_MATH_VALUE(Substraction,-,OP_SUB);
				OPERATIONAL_MATH_VALUE(Multiplication,*,OP_MUL);
				OPERATIONAL_MATH_VALUE(Division,/,OP_DIV);
#if XAIT_OS == XAIT_OS_WIN
#pragma warning(pop)
#endif



				class UntypedAdditionMathValue : public UntypedOperatorMathValue
				{
				public:
					UntypedAdditionMathValue(Reflection::Datatype::DatatypeID datatypeID,MathValue* leftHandSide,MathValue* rightHandSide);
					virtual ~UntypedAdditionMathValue();

					virtual void evaluateToPtr(void* returnValue,VariableAccessor* varAccessor);

					virtual MathValue* clone() const;

					virtual bool compare(MathValue* mathValue) const
					{
						if (getMathValueTypeID() != mathValue->getMathValueTypeID())
							return false;

						UntypedAdditionMathValue* other = (UntypedAdditionMathValue*)(mathValue);
						
						return ((mLeftHandSide->compare(other->mLeftHandSide) && mRightHandSide->compare(other->mRightHandSide)) 
							||  (mLeftHandSide->compare(other->mRightHandSide) && mRightHandSide->compare(other->mLeftHandSide))); 
					}


					virtual Common::String convertToString() const 
					{ 
						return mLeftHandSide->convertToString() + String("+") + mRightHandSide->convertToString();
					} 

				protected:
					XAIT_MATHVALUE_TYPE_FUNCTIONS(UntypedAdditionMathValue); 
				};

				class UntypedSubstractionMathValue : public UntypedOperatorMathValue
				{
				public:
					UntypedSubstractionMathValue(Reflection::Datatype::DatatypeID datatypeID,MathValue* leftHandSide,MathValue* rightHandSide);
					virtual ~UntypedSubstractionMathValue();

					virtual void evaluateToPtr(void* returnValue,VariableAccessor* varAccessor);

					virtual MathValue* clone() const;

					virtual bool compare(MathValue* mathValue) const
					{
						if (getMathValueTypeID() != mathValue->getMathValueTypeID())
							return false;

						UntypedSubstractionMathValue* other = (UntypedSubstractionMathValue*)(mathValue);
						
						return ((mLeftHandSide->compare(other->mLeftHandSide) && mRightHandSide->compare(other->mRightHandSide))); 
					}

					virtual Common::String convertToString() const 
					{ 
						return mLeftHandSide->convertToString() + String("-") + mRightHandSide->convertToString();
					} 

				protected:
					XAIT_MATHVALUE_TYPE_FUNCTIONS(UntypedSubstractionMathValue); 
				};

				class UntypedMultiplicationMathValue : public UntypedOperatorMathValue
				{
				public:
					UntypedMultiplicationMathValue(Reflection::Datatype::DatatypeID datatypeID,MathValue* leftHandSide,MathValue* rightHandSide);
					virtual ~UntypedMultiplicationMathValue();

					virtual void evaluateToPtr(void* returnValue,VariableAccessor* varAccessor);

					virtual MathValue* clone() const;

					virtual bool compare(MathValue* mathValue) const
					{
						if (getMathValueTypeID() == mathValue->getMathValueTypeID())
							return false;
						
						UntypedMultiplicationMathValue* other = (UntypedMultiplicationMathValue*)(mathValue);
						
						return ((mLeftHandSide->compare(other->mLeftHandSide) && mRightHandSide->compare(other->mRightHandSide)) 
							||  (mLeftHandSide->compare(other->mRightHandSide) && mRightHandSide->compare(other->mLeftHandSide))); 
					}

					virtual Common::String convertToString() const 
					{ 
						return mLeftHandSide->convertToString() + String("*") + mRightHandSide->convertToString();
					} 

				protected:
					XAIT_MATHVALUE_TYPE_FUNCTIONS(UntypedMultiplicationMathValue); 
				};

				class UntypedDivisionMathValue : public UntypedOperatorMathValue
				{
				public:
					UntypedDivisionMathValue(Reflection::Datatype::DatatypeID datatypeID,MathValue* leftHandSide,MathValue* rightHandSide);
					virtual ~UntypedDivisionMathValue();

					virtual void evaluateToPtr(void* returnValue,VariableAccessor* varAccessor);

					virtual MathValue* clone() const;

					virtual bool compare(MathValue* mathValue) const
					{
						if (getMathValueTypeID() != mathValue->getMathValueTypeID())
							return false;
						
						UntypedDivisionMathValue* other = (UntypedDivisionMathValue*)(mathValue);
					
						return ((mLeftHandSide->compare(other->mLeftHandSide) && mRightHandSide->compare(other->mRightHandSide))); 
					}

					virtual Common::String convertToString() const 
					{ 
						return mLeftHandSide->convertToString() + String("/") + mRightHandSide->convertToString();
					} 

				protected:
					XAIT_MATHVALUE_TYPE_FUNCTIONS(UntypedDivisionMathValue); 
				};

				class AndMathValue : public OperatorMathValue<bool,bool,bool>
				{
				public:
					//! \brief constructor
					//! \param leftHandSide		pointer to a mathvalue representing the left side of the and operation
					//! \param rightHandSide	pointer to a mathvalue representing the right side of the and operation
					AndMathValue(TypedMathValue<bool>* leftHandSide,TypedMathValue<bool>* rightHandSide)
						: OperatorMathValue<bool,bool,bool>(leftHandSide,rightHandSide)
					{}

					virtual ~AndMathValue()
					{}

					//! \see TypedMathValue
					virtual void evaluate(bool& value,VariableAccessor* varAccessor)
					{
						mLeftHandSide->evaluate(value,varAccessor);
						if(value)
							mRightHandSide->evaluate(value,varAccessor);
					}

					virtual Expression::Operation getOperationType() 
					{ 
						return Expression::OP_AND;
					}

					virtual void evaluateToPtr(void* value,VariableAccessor* varAccessor) 
					{ 
						evaluate(*(bool*)value,varAccessor); 
					} 

					virtual MathValue* clone() const
					{
						return new(mAllocator) AndMathValue((TypedMathValue<bool>*) mLeftHandSide->clone(), (TypedMathValue<bool>*) mRightHandSide->clone());
					}

					virtual bool compare(MathValue* mathValue) const
					{
						if (getMathValueTypeID() != mathValue->getMathValueTypeID())
							return false;
						
						AndMathValue* other = (AndMathValue*)(mathValue);
						
						return ((mLeftHandSide->compare(other->mLeftHandSide) && mRightHandSide->compare(other->mRightHandSide)) 
							||  (mLeftHandSide->compare(other->mRightHandSide) && mRightHandSide->compare(other->mLeftHandSide))); 
					}

					virtual Common::String convertToString() const 
					{ 
						return mLeftHandSide->convertToString() + String("&&") + mRightHandSide->convertToString();
					} 
				protected:
					XAIT_MATHVALUE_TYPE_FUNCTIONS(AndMathValue); 
				};

				class OrMathValue : public OperatorMathValue<bool,bool,bool>
				{
				public:
					//! \brief constructor
					//! \param leftHandSide		pointer to a mathvalue representing the left side of the or operation
					//! \param rightHandSide	pointer to a mathvalue representing the right side of the or operation
					OrMathValue(TypedMathValue<bool>* leftHandSide,TypedMathValue<bool>* rightHandSide)
						: OperatorMathValue<bool,bool,bool>(leftHandSide,rightHandSide)
					{}

					virtual ~OrMathValue()
					{}

					//! \see TypedMathValue
					virtual void evaluate(bool& value,VariableAccessor* varAccessor)
					{
						mLeftHandSide->evaluate(value,varAccessor);
						if(!value)
							mRightHandSide->evaluate(value,varAccessor);
					}

					virtual Expression::Operation getOperationType() 
					{ 
						return Expression::OP_OR;
					}

					virtual void evaluateToPtr(void* value,VariableAccessor* varAccessor) 
					{ 
						evaluate(*(bool*)value,varAccessor); 
					} 
					virtual MathValue* clone() const
					{
						return new(mAllocator) OrMathValue((TypedMathValue<bool>*) mLeftHandSide->clone(), (TypedMathValue<bool>*) mRightHandSide->clone());
					}

					virtual bool compare(MathValue* mathValue) const
					{
						if (getMathValueTypeID() != mathValue->getMathValueTypeID())
							return false;
						
						OrMathValue* other = (OrMathValue*)(mathValue);
						
						return ((mLeftHandSide->compare(other->mLeftHandSide) && mRightHandSide->compare(other->mRightHandSide)) 
							||  (mLeftHandSide->compare(other->mRightHandSide) && mRightHandSide->compare(other->mLeftHandSide))); 
					}

					virtual Common::String convertToString() const 
					{ 
						return mLeftHandSide->convertToString() + String("||") + mRightHandSide->convertToString();
					} 
				protected:
					XAIT_MATHVALUE_TYPE_FUNCTIONS(OrMathValue); 
				};
			}
		}
	}
}

