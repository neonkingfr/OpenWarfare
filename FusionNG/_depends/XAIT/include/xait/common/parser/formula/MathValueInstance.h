// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/threading/SpinLock.h>
#include <xait/common/parser/formula/MathValue.h>

namespace XAIT
{
	namespace Common
	{
		namespace Parser
		{
			namespace Formula
			{

				// this class encapsulates a mathvalue that should be executed
				// the evaluation of the mathvalue is thread safe
				// the stored mathvalue is NOT deleted automatically! (since the mathvalue could be shared between multiple instances


				template<typename T_VALUE_TYPE>
				class TypedMathValueInstance
				{
				public:
					//! \brief constructor
					//! \param mathValue	the encapsulated mathvalue
					TypedMathValueInstance(TypedMathValue<T_VALUE_TYPE>* mathValue)
						: mMathValue(mathValue)
					{}

					virtual ~TypedMathValueInstance()
					{}

					//! \brief gets the encapsulated mathvalue
					//! \returns the encapsulated mathvalue
					TypedMathValue<T_VALUE_TYPE>* getTypedMathValue() const
					{
						return mMathValue;
					}

					//! \brief gets the encapsulated mathvalue
					//! \returns the encapsulated mathvalue
					MathValue*	getMathValue() const
					{
						return mMathValue;
					}

					//! \brief evalutes the mathvalue and write the result in the given memory
					//! \param value			pointer the memory that is used to store the result
					//! \param varAccessor		the used variable accessor (used by variable mathvalues to get the value)
					//! \remark the memory used by the result has to be allocated
					void evaluateToPtr(void* value, VariableAccessor* varAccessor)
					{
						mMutex.lock();
						mMathValue->evaluateToPtr(value,varAccessor);
						mMutex.unlock();
					}

					//! \brief returns a pointer to the referenced value (only usable with reference values)
					//! \param varAccessor	the used variable accessor (used by variable mathvalues to get the value)
					//! \returns a pointer to the referenced value, NULL if the mathvalue is not a reference value
					void* getValueReference(VariableAccessor* varAccessor)
					{
						return mMathValue->getValueReference(varAccessor);
					}

					//! \brief evalutes the mathvalue
					//! \param value			returnvalue
					//! \param varAccessor		pointer to a class needed to get variable values
					virtual void evaluate(T_VALUE_TYPE& value,VariableAccessor* varAccessor)
					{
						mMutex.lock();
						mMathValue->evaluate(value,varAccessor);
						mMutex.unlock();
					}

				private:
					TypedMathValue<T_VALUE_TYPE>*	mMathValue;
					Threading::SpinLock				mMutex;
				};

				class MathValueInstance
				{
				public:
					//! \brief constructor
					//! \param mathValue	the encapsulated mathvalue
					XAIT_COMMON_DLLAPI MathValueInstance(MathValue*	mathValue);

					//! \brief destrcutor
					XAIT_COMMON_DLLAPI virtual ~MathValueInstance();

					//! \brief gets the encapsulated mathvalue
					//! \returns the encapsulated mathvalue
					XAIT_COMMON_DLLAPI MathValue*	getMathValue() const;

					//! \brief evalutes the mathvalue and write the result in the given memory
					//! \param value			pointer the memory that is used to store the result
					//! \param varAccessor		the used variable accessor (used by variable mathvalues to get the value)
					//! \remark the memory used by the result has to be allocated
					XAIT_COMMON_DLLAPI virtual void evaluateToPtr(void* value, VariableAccessor* varAccessor);

					//! \brief returns a pointer to the referenced value (only usable with reference values)
					//! \param varAccessor	the used variable accessor (used by variable mathvalues to get the value)
					//! \returns a pointer to the referenced value, NULL if the mathvalue is not a reference value
					XAIT_COMMON_DLLAPI virtual void* getValueReference(VariableAccessor* varAccessor);

				private:
					MathValue*						mMathValue;
					Threading::SpinLock				mMutex;
				};
			}
		}

	}
}
