// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/Interface.h>

#include <xait/common/parser/formula/MathValue.h>

namespace XAIT
{
	namespace Common
	{
		namespace Reflection
		{
			namespace Function
			{
				class FunctionArgumentList;
			}
		}


		namespace Parser
		{
			namespace Formula
			{
				class ArgumentListMathValue : public MathValue
				{
				public:

					ArgumentListMathValue(Container::Vector<MathValue*>* arguments);

					~ArgumentListMathValue()
					{
						for (uint32 i = 0; i < mArguments->size(); i++)
						{
							delete mArguments->getElement(i);
						}
						XAIT_DELETE(mArguments,Common::Interface::getGlobalAllocator());
					}

					Reflection::Datatype::DatatypeID getDatatypeID() const
					{
						return GET_DATATYPE_ID(Reflection::Function::FunctionArgumentList);
					}

					XAIT_COMMON_DLLAPI void evaluateToPtr(void* value, VariableAccessor* varAccessor);
					
					//! \brief creates a copy of the mathvalue
					//! \returns a copy of the mathvalue
					virtual MathValue* clone() const
					{
						X_ASSERT_MSG(false, "Not used so far");
						return NULL;
					}

					//! \brief compares two mathvalue
					//! \returns true if the mathvalues are equal, false otherwise
					//! \remark the binary tree is compared the expression itself
					virtual bool compare(MathValue* mathValue) const
					{
						X_ASSERT_MSG(false, "Not used so far");
						return false;
					}

					virtual Common::String convertToString() const
					{
						return "not implemented";
					}



				protected:

					void buildSignature();

					Container::Vector<MathValue*>* mArguments;
					Container::Vector<Reflection::Datatype::DatatypeID> mSignature;
					//FunctionEnvironment* mFunctionEnvironment;

					XAIT_MATHVALUE_TYPE_FUNCTIONS(ArgumentListMathValue);
				};
			}

		}
	}
}