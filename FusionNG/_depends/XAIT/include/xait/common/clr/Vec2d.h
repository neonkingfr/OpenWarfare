// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/math/Vec2.h>
#include <xait/common/clr/Marshalling.h>

// ---------------------------------------------------------------------------------------------------------
//							Vector class for two doubles
//
//	Vector class must stay identical to XAIT::Common::Math::Vec2 in means of attributes and virtual methods
//	such that the cast works properly.
//
//	Author:		Daniel G�rgen
//
// ---------------------------------------------------------------------------------------------------------


namespace XAIT
{
	namespace Common
	{
		namespace CLR
		{
			//! \brief template class for vectors with 2 components
			//! \param T_COMP	type of the components (must be a number type)
			public value class Vec2d
			{
			private:
				float64	mX;		//!< component in x
				float64	mY;		//!< component in y

			public:
				property float64 X
				{
					float64 get() { return mX; }
					void set(const float64 val) { mX= val; }
				}

				property float64 Y
				{
					float64 get() { return mY; }
					void set(const float64 val) { mY= val; }
				}

				//! \brief get a component by number (0-x,1-y)
				//! \remark Not so fast as direct member access
				property float64 default[unsigned int]
				{
					inline float64 get(unsigned int i)
					{
						X_ASSERT_MSG_DBG(i < 2,"component index out of range");

						return *(&mX + i);
					}

					inline void set(unsigned int i, float64 value)
					{
						X_ASSERT_MSG_DBG(i < 2,"component index out of range");

						*(&mX + i) = value;
					}
				}

		
				//! \brief Element constructor
				//! \param x	x component
				//! \param y	y component
				Vec2d(const float64 x, const float64 y)
					:mX(x),mY(y)
				{}


				//! \brief Test if two vectors are equal
				//! \remark Vectors are equal if components are equal
				inline bool operator== (const Vec2d a)
				{
					return (a.mX == mX && a.mY == mY);
				}

				//! \brief Test if two vectors are unequal
				//! \remark Vectors are unequal if components are unequal
				inline bool operator!= (const Vec2d a)
				{
					return (a.mX != mX || a.mY != mY);
				}


			};	// class Vec2d
		}	// namespace Math
	}	// namespace CommonCLR
}	// namespace XAIT

XAIT_NETCONVERT_DEFINE_VALUETYPE_CAST(XAIT::Common::Math::Vec2d,XAIT::Common::CLR::Vec2d);
