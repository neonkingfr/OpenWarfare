// (C) xaitment GmbH 2006-2012

#pragma once 

#include <xait/common/SharedPtr.h>
#include <xait/common/clr/Marshalling.h>

namespace XAIT
{
	namespace Common
	{
		namespace CLR
		{

			template<typename T_POINTER,
				typename T_CP,
				typename T_OP>
			ref class SharedPtrCLR
			{
			public:
				T_POINTER* operator->()
				{
					return mNative->operator->();
				}

				SharedPtrCLR(const SharedPtr<T_POINTER,T_CP,T_OP>& native)
				{
					mNative= new SharedPtr<T_POINTER,T_CP,T_OP>(native);
				}

				SharedPtrCLR(SharedPtrCLR^ other)
				{
					mNative= new SharedPtr<T_POINTER,T_CP,T_OP>(other.Native());
				}

				~SharedPtrCLR()
				{
					delete mNative;
				}

				SharedPtr<T_POINTER,T_CP,T_OP>& Native()
				{
					return *mNative;
				}

			private:
				SharedPtr<T_POINTER,T_CP,T_OP>* mNative;
			};



			template<typename T_POINTER, 
				typename T_CP, 
				typename T_OP>
			class WrapperType<SharedPtr<T_POINTER,T_CP,T_OP>>
			{
			public:
				typedef SharedPtr<T_POINTER,T_CP,T_OP>		Native;
				typedef SharedPtrCLR<T_POINTER,T_CP,T_OP>	Managed;
			};

		}
	}
}