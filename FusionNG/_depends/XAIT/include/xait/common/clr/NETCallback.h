// (C) xaitment GmbH 2006-2012

#pragma once 

#include <xait/common/FundamentalTypes.h>
#include <xait/common/String.h>
#include <xait/common/Interface.h>
#include <xait/common/clr/StringConvert.h>
#include <xait/common/clr/VectorConvert.h>
#include <xait/common/clr/Vec2f.h>
#include <xait/common/clr/Vec3f.h>
#include <xait/common/clr/VectorCLR.h>
#include <xait/common/reflection/function/FunctionArgumentList.h>
#include <xait/common/reflection/datatype/DatatypeTypedefs.h>
#include <xait/common/reflection/ReflectionInterface.h>


using namespace System::Reflection;
using namespace XAIT::Common::Reflection::Function;

namespace XAIT
{
	namespace Common
	{
		namespace CLR
		{
			class NETCallback
			{
				gcroot<System::Object^>		mInstance;
				gcroot<MethodInfo^>	mMethod;

			public:
				NETCallback(System::Object^ instance, MethodInfo^ method)
					: mInstance(instance),mMethod(method)
				{}

				NETCallback(MethodInfo^ method)
					: mInstance(nullptr),mMethod(method)
				{}

				NETCallback(System::Object^ instance, System::String^ name)
					: mInstance(instance)
				{
					mMethod= instance->GetType()->GetMethod(name,BindingFlags::NonPublic | BindingFlags::Public | BindingFlags::Static | BindingFlags::Instance);
				}

				NETCallback(System::Type^ staticType, System::String^ name)
					: mInstance(nullptr)
				{
					mMethod= staticType->GetMethod(name,BindingFlags::NonPublic | BindingFlags::Public | BindingFlags::Static);
				}


				void CallFunction(void* returnValue, Common::Reflection::Datatype::DatatypeID datatypeID, const FunctionArgumentList& arguments)
				{
					const uint32 numArgs= arguments.size();
					array<System::Object^>^ netArgs= gcnew array<System::Object^>(numArgs);

					for(uint32 i= 0; i < numArgs; ++i)
					{
						const Common::Reflection::Datatype::DatatypeID variableType = arguments.getDatatypeID(i);
						if (variableType == GET_DATATYPE_ID(bool))
							netArgs[i] = arguments.getValue<bool>(i);
						else if (variableType == GET_DATATYPE_ID(int32))
							netArgs[i]= arguments.getValue<int32>(i);
						else if (variableType == GET_DATATYPE_ID(float32))
							netArgs[i]= arguments.getValue<float>(i);
						else if (variableType == GET_DATATYPE_ID(Common::String))
							netArgs[i]= NETConvert(arguments.getValue<XAIT::Common::String>(i));
						else if (variableType == GET_DATATYPE_ID(Common::Math::Vec2f))
							netArgs[i]= NETConvert(arguments.getValue<XAIT::Common::Math::Vec2f>(i));
						else if (variableType == GET_DATATYPE_ID(Common::Math::Vec3f))
							netArgs[i]= NETConvert(arguments.getValue<XAIT::Common::Math::Vec3f>(i));
						else if (variableType == GET_DATATYPE_ID(Common::Container::Vector<bool>))
							netArgs[i]= NETConvert(arguments.getValue<XAIT::Common::Container::Vector<bool> >(i));
						else if (variableType == GET_DATATYPE_ID(Common::Container::Vector<int32>))
							netArgs[i]= NETConvert(arguments.getValue<XAIT::Common::Container::Vector<int32> >(i));
						else if (variableType == GET_DATATYPE_ID(Common::Container::Vector<float32>))
							netArgs[i]= NETConvert(arguments.getValue<XAIT::Common::Container::Vector<float32> >(i));
//  todo				else if (variableType == GET_DATATYPE_ID(Common::Container::Vector<Common::String>))
//  markus fragen			netArgs[i]= NETConvert(arguments.getValue<XAIT::Common::Container::Vector<Common::String> >(i));
						else if (variableType == GET_DATATYPE_ID(Common::Container::Vector<Common::Math::Vec2f>))
							netArgs[i]= NETConvert(arguments.getValue<XAIT::Common::Container::Vector<Common::Math::Vec2f> >(i));
						else if (variableType == GET_DATATYPE_ID(Common::Container::Vector<Common::Math::Vec3f>))
							netArgs[i]= NETConvert(arguments.getValue<XAIT::Common::Container::Vector<Common::Math::Vec3f> >(i));

						else
							throw gcnew System::ArgumentException("Argument type: '" + NETConvert(Common::Reflection::ReflectionInterface::getDatatypeManager()->getDatatypeName(arguments.getDatatypeID(i)) + "' not yet supported"));
						
					}

					System::Object^ methodReturnValue = mMethod->Invoke(mInstance,netArgs);

#define RETURN_VALUE_FUNDAMENTAL_TYPE_ASSIGNMENT(type) \
	if (datatypeID == GET_DATATYPE_ID(type)) \
		*((type*)returnValue) = *(type^) methodReturnValue;

#define RETURN_VALUE_ASSIGNMENT(type) \
	if (datatypeID == GET_DATATYPE_ID(type)) \
	*((type*)returnValue) = NETConvert((XAIT::Common::CLR::WrapperType<type>::Managed) methodReturnValue);


					if (returnValue != NULL)
					{
						RETURN_VALUE_FUNDAMENTAL_TYPE_ASSIGNMENT(bool)
						else RETURN_VALUE_FUNDAMENTAL_TYPE_ASSIGNMENT(int32)
						else RETURN_VALUE_FUNDAMENTAL_TYPE_ASSIGNMENT(float32)
						else RETURN_VALUE_FUNDAMENTAL_TYPE_ASSIGNMENT(int32)
						else if (datatypeID == GET_DATATYPE_ID(Common::String)) 
						*((Common::String*)returnValue) = Common::BString(NETConvert((System::String^) methodReturnValue));
 						else RETURN_VALUE_ASSIGNMENT(Common::Math::Vec2f)
 						else RETURN_VALUE_ASSIGNMENT(Common::Math::Vec3f)
 						else RETURN_VALUE_ASSIGNMENT(XAIT::Common::Container::Vector<bool>)
 						else RETURN_VALUE_ASSIGNMENT(XAIT::Common::Container::Vector<int32>)
 						else RETURN_VALUE_ASSIGNMENT(XAIT::Common::Container::Vector<float32>)
 						else RETURN_VALUE_ASSIGNMENT(XAIT::Common::Container::Vector<Common::Math::Vec2f>)
 						else RETURN_VALUE_ASSIGNMENT(XAIT::Common::Container::Vector<Common::Math::Vec3f>)
					}
				}

				Reflection::Datatype::DatatypeID ConvertNetTypeToDatatypeID(System::Type^ type)
				{
					if (type == bool::typeid)
						return GET_DATATYPE_ID(bool);
					else if (type == int::typeid)
						return GET_DATATYPE_ID(int32);
					else if (type == float::typeid)
						return GET_DATATYPE_ID(float32);
					else if (type == System::String::typeid)
						return GET_DATATYPE_ID(Common::String);
					else if (type == Common::CLR::Vec2f::typeid)
						return GET_DATATYPE_ID(Common::Math::Vec2f);
					else if (type == Common::CLR::Vec2f::typeid)
						return GET_DATATYPE_ID(Common::Math::Vec3f);
					else if (type  == Common::CLR::VectorCLR<bool>::typeid)
						return GET_DATATYPE_ID(Common::Container::Vector<bool>);
					else if (type == Common::CLR::VectorCLR<int32>::typeid)
						return GET_DATATYPE_ID(Common::Container::Vector<int32>);
					else if (type == Common::CLR::VectorCLR<float32>::typeid)
						return GET_DATATYPE_ID(Common::Container::Vector<float32>);
				//  todo				else if (variableType == GET_DATATYPE_ID(Common::Container::Vector<Common::String>))
				//  markus fragen			netArgs[i]= NETConvert(arguments.getValue<XAIT::Common::Container::Vector<Common::String> >(i));
					else if (type == Common::CLR::VectorCLR<Common::CLR::Vec2f>::typeid) 
						return GET_DATATYPE_ID(Common::Container::Vector<Common::Math::Vec2f>);
					else if (type == Common::CLR::VectorCLR<Common::CLR::Vec3f>::typeid) 
						return GET_DATATYPE_ID(Common::Container::Vector<Common::Math::Vec3f>);

					return Common::Reflection::Datatype::DatatypeID();
				}

				Container::Vector<Reflection::Datatype::DatatypeID> GetSignature()
				{
					Container::Vector<Reflection::Datatype::DatatypeID>  signature(Common::Interface::getGlobalAllocator());

					signature.pushBack(ConvertNetTypeToDatatypeID(mMethod->ReturnType));

					for each ( ParameterInfo^ parameterInfo in (mMethod->GetParameters()) )
					{
						signature.pushBack(ConvertNetTypeToDatatypeID(parameterInfo->ParameterType));
					}

					return signature;
				}

			};
		}
	}
}