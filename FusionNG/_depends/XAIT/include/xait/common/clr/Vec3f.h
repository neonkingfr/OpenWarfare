// (C) xaitment GmbH 2006-2012

#pragma once 

#include <assert.h>
#include <xait/common/math/MathDefines.h>
#include <xait/common/math/Vec3.h>
#include <xait/common/clr/Marshalling.h>
#include <xait/common/clr/Vec2f.h>

// ---------------------------------------------------------------------------------------------------------
//							Vector class for three floats
//
//	Vector class must stay identical to XAIT::Common::Math::Vec3 in means of attributes and virtual methods
//	such that the cast works properly.
//
//	Author:		Markus Wilhelm
//
// ---------------------------------------------------------------------------------------------------------


namespace XAIT
{
	namespace Common
	{
		namespace CLR
		{
			public value class Vec3f
			{
			private:
				float	mX;
				float	mY;
				float	mZ;
			public:

				property float X
				{
					float get() { return mX; }
					void set(const float val) { mX= val; }
				}

				property float Y
				{
					float get() { return mY; }
					void set(const float val) { mY= val; }
				}

				property float Z
				{
					float get() { return mZ; }
					void set(const float val) { mZ= val; }
				}


				//! \brief get a component by number (0-x,1-y,2-z)
				//! \remark Not so fast as direct member access
				property float default[unsigned int]
				{
					inline float get(unsigned int i)
					{
						assert( i < 3 );

						return *(&mX + i);
					}

					inline void set(unsigned int i, float value)
					{
						assert( i < 3 );

						*(&mX + i) = value;
					}
				}

				//! \brief get vector as float array
				inline array<float>^ GetArrayCopy()
				{
					array<float>^ tmp= gcnew array<float>(3);
					tmp[0]= mX;
					tmp[1]= mY;
					tmp[2]= mZ;

					return tmp;
				}


				//! \brief Init vector from single components
				//! \param x	x component
				//! \param y	y component
				//! \param z	z component
				inline Vec3f(float x, float y, float z)
					: mX(x),mY(y),mZ(z)
				{}

				//! \brief Init vector from array
				inline Vec3f(array<float>^ vec)
					: mX(vec[0]),mY(vec[1]),mZ(vec[2])
				{}


				//! \brief Test if two vectors are equal
				//! \remark Vectors are equal if components are equal
				inline bool operator==(const Vec3f p) 
				{ 
					return (mX == p.mX && mY == p.mY && mZ == p.mZ); 
				}

				//! \brief Test if two vectors are unequal
				//! \remark Vectors are unequal if components are unequal
				inline bool operator!=(const Vec3f p) 
				{ 
					return (mX != p.mX || mY != p.mY || mZ != p.mZ); 
				}

				//! \brief Add this vector to another vector and return the result
				inline static Vec3f operator+(Vec3f a, Vec3f b) 
				{ 
					return Vec3f(a.mX + b.mX,a.mY + b.mY,a.mZ + b.mZ);
				}

				//! \brief Substract a vector from this one and return the result
				inline static Vec3f operator-(Vec3f a, Vec3f b)
				{
					return Vec3f(a.mX - b.mX,a.mY - b.mY,a.mZ - b.mZ);
				}

				//! \brief multiplies vector by *-operator with scalar 
				inline static Vec3f operator*(Vec3f a, float v) 
				{
					return Vec3f(a.mX * v,a.mY * v,a.mZ * v);
				}

				//! \brief Divide this vector by a scalar and return the result
				inline Vec3f operator/(const float v) 
				{
					return Vec3f(mX / v,mY / v,mZ / v);
				}


				//! \brief returns straight-line distance between two vectors
				inline float GetDistance(const Vec3f p)
				{
					return (float)System::Math::Sqrt((p.mX-mX) * (p.mX-mX) + (p.mY-mY) * (p.mY-mY) + (p.mZ-mZ) * (p.mZ-mZ));
				}

				//! \brief Get squared distance to another position
				//! \param position		position to which we want the distance
				//! \return The squared distance
				inline float GetSquaredDistance(const Vec3f p)
				{
					return ((p.mX-mX) * (p.mX-mX) + (p.mY-mY) * (p.mY-mY) + (p.mZ-mZ) * (p.mZ-mZ));
				}

				//! \brief computes dotProduct of two vectors
				inline float DotProduct(const Vec3f p)
				{
					return  mX*p.mX + mY*p.mY + mZ*p.mZ;
				}

				//! \brief computes crossProduct of two vectors
				inline Vec3f CrossProduct(const Vec3f p)
				{
					return Vec3f(mY * p.mZ - mZ * p.mY, mZ * p.mX - mX * p.mZ, mX * p.mY - mY * p.mX);
				}

				//! \brief normalizes a vector
				//! \returns the length of the vector
				//! \remark If the length is zero, vector will not be changed.
				inline float Normalize()
				{
					float length = Length;

					if (length == 0.0f) return 0.0f;

					mX /= length;
					mY /= length;
					mZ /= length;


					return length;
				}

				//! \brief Test if a vector is normalized
				//! \returns True if the vector is normalized or false otherwise
				//! \remark The test encounters a small epsilon, so the vector could be of length 1.0001 or 0.9999 and still being handled as normalized.
				inline bool IsNormalized()
				{
					// we can use getLength2 for this test, since sqrt(1) =  1
					const float length= SquaredLength;

					return (length > (float)1.0 - (float)X_EPSILON) && (length < (float)1.0 + (float)X_EPSILON);
				}

				//! \brief inverts the vector
				//! \remark this method makes only sense for signed number types
				inline void Invert()
				{
					mX *= (float)-1;
					mY *= (float)-1;
					mZ *= (float)-1;
				}


				//! \brief Reduces the dimensions of the vector by eliminating one
				//! \param killCompID	component id which should be eliminated 
				//! \remark The components will stay in the order of Vec3.
				inline Vec2f ReduceDimensions(const uint32 killCompID)
				{
					X_ASSERT_MSG_DBG(killCompID < 3,"component id out of range [0..2]");

					switch (killCompID)
					{
					case 0:
						return Vec2f(mY,mZ);
					case 1:
						return Vec2f(mX,mZ);
					case 2:
						return Vec2f(mX,mY);
					};

					return Vec2f::ZERO;
				}


				//! \brief get the length of the vector
				//! \remark Uses square root function. If only compares are need, use the faster getLength2.
				property float Length
				{
					inline float get()
					{
						return (float)System::Math::Sqrt(mX * mX + mY * mY + mZ * mZ);
					}
					inline void set(const float length)
					{
						Normalize();
						mX *= length;
						mY *= length;
						mZ *= length;
					}
				}

				//! \brief get the squared distance (faster if you need only comparison)
				property float SquaredLength
				{
					inline float get()
					{
						return (mX * mX + mY * mY + mZ * mZ);
					}
				}

				//! \brief get the absolute minimum component
				//! \returns the index of the minimum component
				property int MinComp
				{
					inline int get()
					{
						if (mX < mY)
						{
							if (mX < mZ) return 0; else return 2;
						}
						else
						{
							if (mY < mZ) return 1; else return 2;
						}
					}
				}

				//! \brief get the absolute maximum component
				//! \returns the index of the maximum component
				property int MaxComp
				{
					inline int get()
					{
						if (mX > mY)
						{
							if (mX > mZ) return 0; else return 2;
						}
						else
						{
							if (mY > mZ) return 1; else return 2;
						}
					}
				}


				// predefine some common vectors
				static const Vec3f ZERO = Vec3f( 0, 0, 0);
				static const Vec3f UNIT_X = Vec3f( 1, 0, 0);
				static const Vec3f UNIT_Y = Vec3f( 0, 1, 0);
				static const Vec3f UNIT_Z = Vec3f( 0, 0, 1);
				static const Vec3f NEGATIVE_UNIT_X = Vec3f( -1,  0,  0 );
				static const Vec3f NEGATIVE_UNIT_Y = Vec3f(  0, -1,  0 );
				static const Vec3f NEGATIVE_UNIT_Z = Vec3f(  0,  0, -1 );
				static const Vec3f UNIT_SCALE = Vec3f(1, 1, 1);

			};
		}
	}
}

XAIT_NETCONVERT_DEFINE_VALUETYPE_CAST(XAIT::Common::Math::Vec3f,XAIT::Common::CLR::Vec3f);
