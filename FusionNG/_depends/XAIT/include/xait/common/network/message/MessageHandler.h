// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/MinimalPrerequisites.h>
#include <xait/common/memory/MemoryStream.h>

#include <xait/common/network/message/MessageReader.h>

namespace XAIT
{
	namespace Common
	{
		namespace Network
		{
			namespace Message
			{
				class MessageHandler : public Common::Memory::MemoryManaged
				{
				public:
					//! \brief Destructor
					virtual ~MessageHandler() {}

					//! \brief handles the message by reading the data through the MessageReader
					//! \param msgReader	the reader containing the message
					//! \param connectionID		the identifier of the client the message is from
					//! \returns true, if message was handled correctly, else false
					virtual bool handleMessage(Message::MessageReader* msgReader, const uint32 connectionID ) = 0;

					//! \brief returns the MessageTypeID this MessageHandler should handle
					//! \returns the MessageTypeID this MessageHandler should handle
					uint32 getID() const {return mID;}

				protected:
					//! \brief Constructor
					//! \param id	the id of the message this MessageHandler should react on
					MessageHandler(const uint32 id)
						: mResultCode(0), mErrorText(""), mID(id)
					{}

					virtual void reinitVariables()
					{
						mResultCode = 0;
						mErrorText = "";
					}

					uint08		mResultCode;
					String		mErrorText;

				private:
					uint32	mID;		//!<	the id of the message this MessageHandler should react on


				};
			}
		}
	}
}
