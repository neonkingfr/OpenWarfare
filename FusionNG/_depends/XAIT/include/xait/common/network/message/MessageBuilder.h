// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/MinimalPrerequisites.h>
#include <xait/common/io/Stream.h>
#include <xait/common/math/Vec2.h>
#include <xait/common/math/Vec3.h>
#include <xait/common/network/message/StreamIDs.h>
#include <xait/common/platform/Endian.h>


namespace XAIT
{
	namespace Common
	{
		namespace Network
		{
			namespace Message
			{
				class MessageBuilder
				{
				public:
					//! \brief Constructor
					//! \param msgStream	stream to write the data to
					//! \param useType	defines whether the types should be added in front of each data
					//! \remark the current position of the stream isn't changed s.t. appending to the stream is done with next write
					XAIT_COMMON_DLLAPI MessageBuilder(IO::Stream* msgStream, const bool useType=true);

					//! \brief Destructor
					XAIT_COMMON_DLLAPI ~MessageBuilder();

					//! \brief converts the void* to the specified variable and writes its data to stream
					//! \param source	pointer to the variable
					//! \param type		the type the source should be converted to
					XAIT_COMMON_DLLAPI void write( void* source, const StreamIDs type );

					//! \brief converts the void* to the specified variable and writes its data to stream
					//! \param source	pointer to the variable
					//! \param type		the type the source should be converted to
					//! \param useType		determines whether the type have to be written
					//! \remark must be read with same useType, will be written independently of own useType
					XAIT_COMMON_DLLAPI void writeForceUseType( void* source, const StreamIDs type, const bool useType );

					//! \brief converts the void* to the specified variable and writes its data as a stream to the stream
					//! \param source	pointer to the variable
					//! \param type		the type the source should be converted to
					//! \param useType		determines whether the type have to be written
					//! \remark different to writeForceUseType this function hides the data within a stream in stream
					//! it is faster than creating a dummy stream only for this data and writting the stream
					XAIT_COMMON_DLLAPI void writeAsStreamForceUseType( void* source, const StreamIDs type, const bool useType );

					//! \brief writes from another stream into this stream
					//! \param stream	the source to take data from
					//! \param numBytes		the amount of bytes to write
					//! \param writeDataOnly	flag determining whether only the data or previously the type(ARRAY | UINT08) should be written
					//! \remark see IO::Stream::writeFromStream for exceptions
					XAIT_COMMON_DLLAPI void writeStream( IO::Stream* stream, const uint64 numBytes, bool writeDataOnly = false );

					//! \brief writes the data to the stream
					//! \param object	the data that should be inserted to the stream
					XAIT_COMMON_DLLAPI void write( const bool& object);
					XAIT_COMMON_DLLAPI void write( const uint08& object);
					XAIT_COMMON_DLLAPI void write( const uint16& object);
					XAIT_COMMON_DLLAPI void write( const uint32& object);
					XAIT_COMMON_DLLAPI void write( const uint64& object);
					XAIT_COMMON_DLLAPI void write( const int08& object);
					XAIT_COMMON_DLLAPI void write( const int16& object);
					XAIT_COMMON_DLLAPI void write( const int32& object);
					XAIT_COMMON_DLLAPI void write( const int64& object);
					XAIT_COMMON_DLLAPI void write( const float32& object);
					XAIT_COMMON_DLLAPI void write( const float64& object);
					XAIT_COMMON_DLLAPI void write( const BString& object);
					XAIT_COMMON_DLLAPI void write( const WString& object);
					XAIT_COMMON_DLLAPI void write( const Math::Vec2f& object);
					XAIT_COMMON_DLLAPI void write( const Math::Vec2d& object);
					XAIT_COMMON_DLLAPI void write( const Math::Vec2i32& object);
					XAIT_COMMON_DLLAPI void write( const Math::Vec2u32& object);
					XAIT_COMMON_DLLAPI void write( const Math::Vec3f& object);
					XAIT_COMMON_DLLAPI void write( const Math::Vec3d& object);
					XAIT_COMMON_DLLAPI void write( const Math::Vec3i32& object);
					XAIT_COMMON_DLLAPI void write( const Math::Vec3u32& object);
					XAIT_COMMON_DLLAPI void write( const StreamIDs& object);

					template <typename T> 
					void write(const Container::Vector<T>& object)
					{
						StreamIDs childID = getStreamID<T>();

						//write that this is an array of T
						writeType((StreamIDs)(STREAM_TYPE_ARRAY | childID));

						//write the number of children
						uint32 numChildren = object.size();
						writeWithEndian(numChildren);

						//write each child
						for (uint32 i=0; i<numChildren; i++)
						{
							bool oldValue = mUseType;
							mUseType = false;	//!< don't use types for children
							write(object[i]);
							mUseType = oldValue;
						}
					}

					//! \brief returns the stream that was written to
					//! \returns the stream that was written to
					XAIT_COMMON_DLLAPI IO::Stream* getStream() const {return mStream;}

				private:
					//! \brief the writing to the stream have to care about the endianness
					template <typename T> 
					void writeWithEndian(const T& object)
					{
						T objectBigEndian;
						Platform::Endian::convertSystemToBig(objectBigEndian,object);
						mStream->write(&objectBigEndian, sizeof(T));
					}

					//! \brief writes the type of the data to the stream
					//! \param type		the typeID to check for
					//! \returns true, if the type could be verified else returns false
					void writeType(const StreamIDs& type);

					IO::Stream*	mStream;	//!< stream to write to
					bool		mUseType;	//!< flag to use types for each data
				};
			}
		}
	}
}
