// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/MinimalPrerequisites.h>
#include <xait/common/reflection/datatype/Datatypes.h>

namespace XAIT
{
	namespace Common
	{
		namespace Network
		{
			namespace Message
			{
				enum StreamIDs// : uint08
				{
					STREAM_TYPE_BOOL			= 0x00,
					STREAM_TYPE_INT08			= 0x01,
					STREAM_TYPE_INT16			= 0x02,
					STREAM_TYPE_INT32			= 0x03,
					STREAM_TYPE_INT64			= 0x04,
					STREAM_TYPE_UINT08			= 0x05,
					STREAM_TYPE_UINT16			= 0x06,
					STREAM_TYPE_UINT32			= 0x07,
					STREAM_TYPE_UINT64			= 0x08,
					STREAM_TYPE_FLOAT32			= 0x09,
					STREAM_TYPE_FLOAT64			= 0x0A,
					STREAM_TYPE_BSTRING			= 0x0B,
					STREAM_TYPE_WSTRING			= 0x0C,
					STREAM_TYPE_VEC_2_FLOAT32	= 0x10,
					STREAM_TYPE_VEC_2_FLOAT64	= 0x11,
					STREAM_TYPE_VEC_2_INT32		= 0x12,
					STREAM_TYPE_VEC_2_UINT32	= 0x13,
					STREAM_TYPE_VEC_3_FLOAT32	= 0x14,
					STREAM_TYPE_VEC_3_FLOAT64	= 0x15,
					STREAM_TYPE_VEC_3_INT32		= 0x16,
					STREAM_TYPE_VEC_3_UINT32	= 0x17,
					STREAM_TYPE_ARRAY					= 0x80, // additional array flag
					STREAM_TYPE_ARRAY_BOOL				= 0x80,
					STREAM_TYPE_ARRAY_INT08				= 0x81,
					STREAM_TYPE_ARRAY_INT16				= 0x82,
					STREAM_TYPE_ARRAY_INT32				= 0x83,
					STREAM_TYPE_ARRAY_INT64				= 0x84,
					STREAM_TYPE_ARRAY_UINT08			= 0x85,
					STREAM_TYPE_ARRAY_UINT16			= 0x86,
					STREAM_TYPE_ARRAY_UINT32			= 0x87,
					STREAM_TYPE_ARRAY_UINT64			= 0x88,
					STREAM_TYPE_ARRAY_FLOAT32			= 0x89,
					STREAM_TYPE_ARRAY_FLOAT64			= 0x8A,
					STREAM_TYPE_ARRAY_BSTRING			= 0x8B,
					STREAM_TYPE_ARRAY_WSTRING			= 0x8C,
					STREAM_TYPE_ARRAY_VEC_2_FLOAT32		= 0x90,
					STREAM_TYPE_ARRAY_VEC_2_FLOAT64		= 0x91,
					STREAM_TYPE_ARRAY_VEC_2_INT32		= 0x92,
					STREAM_TYPE_ARRAY_VEC_2_UINT32		= 0x93,
					STREAM_TYPE_ARRAY_VEC_3_FLOAT32		= 0x94,
					STREAM_TYPE_ARRAY_VEC_3_FLOAT64		= 0x95,
					STREAM_TYPE_ARRAY_VEC_3_INT32		= 0x96,
					STREAM_TYPE_ARRAY_VEC_3_UINT32		= 0x97,
				};

				String getStreamIDName(const StreamIDs& id);

				XAIT_COMMON_DLLAPI bool getStreamIDFromDatatypeID(StreamIDs& streamID, Reflection::Datatype::DatatypeID datatypeID);

				template <typename T> 
				StreamIDs getStreamID()
				{
					XAIT_FAIL("TYPE not supported for StreamIDs");
					return STREAM_TYPE_BOOL;
				}


				template <> XAIT_COMMON_DLLAPI StreamIDs getStreamID<bool>();
				template <> XAIT_COMMON_DLLAPI StreamIDs getStreamID<uint08>();
				template <> XAIT_COMMON_DLLAPI StreamIDs getStreamID<uint16>();
				template <> XAIT_COMMON_DLLAPI StreamIDs getStreamID<uint32>();
				template <> XAIT_COMMON_DLLAPI StreamIDs getStreamID<uint64>();
				template <> XAIT_COMMON_DLLAPI StreamIDs getStreamID<int08>();
				template <> XAIT_COMMON_DLLAPI StreamIDs getStreamID<int16>();
				template <> XAIT_COMMON_DLLAPI StreamIDs getStreamID<int32>();
				template <> XAIT_COMMON_DLLAPI StreamIDs getStreamID<int64>();
				template <> XAIT_COMMON_DLLAPI StreamIDs getStreamID<float32>();
				template <> XAIT_COMMON_DLLAPI StreamIDs getStreamID<float64>();
				template <> XAIT_COMMON_DLLAPI StreamIDs getStreamID<BString>();
				template <> XAIT_COMMON_DLLAPI StreamIDs getStreamID<WString>();
				template <> XAIT_COMMON_DLLAPI StreamIDs getStreamID<Math::Vec2f>();
				template <> XAIT_COMMON_DLLAPI StreamIDs getStreamID<Math::Vec2d>();
				template <> XAIT_COMMON_DLLAPI StreamIDs getStreamID<Math::Vec2i32>();
				template <> XAIT_COMMON_DLLAPI StreamIDs getStreamID<Math::Vec2u32>();
				template <> XAIT_COMMON_DLLAPI StreamIDs getStreamID<Math::Vec3f>();
				template <> XAIT_COMMON_DLLAPI StreamIDs getStreamID<Math::Vec3d>();
				template <> XAIT_COMMON_DLLAPI StreamIDs getStreamID<Math::Vec3i32>();
				template <> XAIT_COMMON_DLLAPI StreamIDs getStreamID<Math::Vec3u32>();
				
				template <> XAIT_COMMON_DLLAPI StreamIDs getStreamID<Container::Vector<bool> >();
				template <> XAIT_COMMON_DLLAPI StreamIDs getStreamID<Container::Vector<uint08> >();
				template <> XAIT_COMMON_DLLAPI StreamIDs getStreamID<Container::Vector<uint16> >();
				template <> XAIT_COMMON_DLLAPI StreamIDs getStreamID<Container::Vector<uint32> >();
				template <> XAIT_COMMON_DLLAPI StreamIDs getStreamID<Container::Vector<uint64> >();
				template <> XAIT_COMMON_DLLAPI StreamIDs getStreamID<Container::Vector<int08> >();
				template <> XAIT_COMMON_DLLAPI StreamIDs getStreamID<Container::Vector<int16> >();
				template <> XAIT_COMMON_DLLAPI StreamIDs getStreamID<Container::Vector<int32> >();
				template <> XAIT_COMMON_DLLAPI StreamIDs getStreamID<Container::Vector<int64> >();
				template <> XAIT_COMMON_DLLAPI StreamIDs getStreamID<Container::Vector<float32> >();
				template <> XAIT_COMMON_DLLAPI StreamIDs getStreamID<Container::Vector<float64> >();
				template <> XAIT_COMMON_DLLAPI StreamIDs getStreamID<Container::Vector<BString> >();
				template <> XAIT_COMMON_DLLAPI StreamIDs getStreamID<Container::Vector<WString> >();
				template <> XAIT_COMMON_DLLAPI StreamIDs getStreamID<Container::Vector<Math::Vec2f> >();
				template <> XAIT_COMMON_DLLAPI StreamIDs getStreamID<Container::Vector<Math::Vec2d> >();
				template <> XAIT_COMMON_DLLAPI StreamIDs getStreamID<Container::Vector<Math::Vec2i32> >();
				template <> XAIT_COMMON_DLLAPI StreamIDs getStreamID<Container::Vector<Math::Vec2u32> >();
				template <> XAIT_COMMON_DLLAPI StreamIDs getStreamID<Container::Vector<Math::Vec3f> >();
				template <> XAIT_COMMON_DLLAPI StreamIDs getStreamID<Container::Vector<Math::Vec3d> >();
				template <> XAIT_COMMON_DLLAPI StreamIDs getStreamID<Container::Vector<Math::Vec3i32> >();
				template <> XAIT_COMMON_DLLAPI StreamIDs getStreamID<Container::Vector<Math::Vec3u32> >();
			}
		}
	}
}
