// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/memory/MemoryManaged.h>
#include <xait/common/container/List.h>
#include <xait/common/platform/Time.h>
#include <xait/common/network/NetworkPlatform.h>
#include <xait/common/network/sockets/AddressFamily.h>
#include <xait/common/network/sockets/HostAddress.h>
#include <xait/common/network/sockets/SocketError.h>

namespace XAIT
{
	namespace Common
	{
		namespace Network
		{
			namespace Sockets
			{
				//! Base class for all sockets (TCP + UDP)
				class Socket : public Common::Memory::MemoryManaged
				{
				public:
					enum SelectError
					{
						SELECT_ERR_NOERROR		= 0,	//!< no error during select
						SELECT_ERR_TIMEOUT		= 1,	//!< timeout reached
						SELECT_ERR_SOCKET_ERR	= 2,	//!< one of the sockets had an error
					};


					//! \brief Destructor
					//! \remark Closes the socket
					XAIT_COMMON_DLLAPI virtual ~Socket();

					//! \brief close the socket
					bool close();
				

					//! \brief test if the socket is valid
					bool isValid() const
					{
						return mHandle != SOCKET_HANDLE_INVALID;
					}

					//! \brief get the handle created by the OS
					inline const SocketOSHandle& getOSHandle() const
					{
						return mHandle;
					}

					//! \brief get the last error that occurred on this socket
					static inline SocketError getLastError()
					{
						return (SocketError)SOCKET_ERR_GETERROR();
					}


					//! \brief Check lists of sockets for avaiable data or errors
					//! \param checkRead	list of sockets that should be checked for read. The list will be modified and only contain sockets that have data to read.
					//! \param checkWrite	list of sockets that should be checked for write. The list will be modified and only contain sockets that have data to write.
					//! \param checkError	list of sockets that should be checked for errors. The list will be modified and only contain sockets that have errors.
					//! \param timeOut		time out value in microseconds (0 is non blocking, -1 is timeout disabled -> blocking)
					//! \returns SOCK_ERR_NOERROR if successfull
					//! \remark This call blocks until timeOut is reached or on of the provided sockets has read/write data or an error. 
					static SelectError select(Container::List<Socket*>* checkRead, Container::List<Socket*>* checkWrite, Container::List<Socket*>* checkError, const int32 timeOut);

					//! \brief Check if a certain addressfamily is supported by the os
					inline static bool hasOSAddressFamilySupport(const AddressFamily addressFamily)
					{
						return addressFamily != ADDRFAMILY_UNSUPPORTED;
					}

				protected:
					//! \brief Initialize an invalid socket
					Socket()
						: mHandle(SOCKET_HANDLE_INVALID)
					{}

					//! \brief Initialize an socket with specified os handle
					//! \param osHandle		handle from the OS
					Socket(const SocketOSHandle& osHandle)
						: mHandle(osHandle)
					{}

					//! \brief bind the socket to a local address
					//! \returns True if the socket is bind to local address, false otherwise. Socket error can be retrieved with Socket::getLastError.
					bool bind(const HostAddress& address);

					SocketOSHandle	mHandle;
				private:
					//! \brief made copy constructor private as it is not allowed to copy sockets
					Socket(const Socket& src)
						: MemoryManaged((const MemoryManaged&)src)
					{
						XAIT_FAIL("Copy constructor on socket not allowed");
					}
				};
			}
		}
	}
}

