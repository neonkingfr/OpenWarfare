// (C) xaitment GmbH 2006-2012

#pragma once 

#include <xait/common/FundamentalTypes.h>
#include <xait/common/String.h>
#include <xait/common/network/NetworkPlatform.h>


namespace XAIT
{
	namespace Common
	{
		namespace Network
		{
			namespace Sockets
			{
				//! \brief class represents a network ip address for IPV4
				struct IPV4
				{
					union 
					{
						struct { uint08 mB0,mB1,mB2,mB3; };
						struct { uint16 mW0,mW1; };
						uint32 mU0;
						uint08 mBArray[4];
						uint16 mWArray[2];
					};

					IPV4()
					{
						mU0 = INADDR_ANY;
					}

					IPV4(const uint08 b0, const uint08 b1, const uint08 b2, const uint08 b3)
					{
						mBArray[0]= b0;
						mBArray[1]= b1;
						mBArray[2]= b2;
						mBArray[3]= b3;
					}

					//! \brief initialize the class from an os address struct
					XAIT_COMMON_DLLAPI IPV4(const SocketOSIPV4* address);

					//! \brief copy the address to an os address struct
					XAIT_COMMON_DLLAPI void copyToOSAddress(SocketOSIPV4* address) const;

					XAIT_COMMON_DLLAPI Common::String toString() const;

					//! \brief any in address
					XAIT_COMMON_DLLAPI static const IPV4 ANY;
				};

#ifdef XAIT_NETWORK_HAS_IPV6

				//! \brief class represents a network ip address for IPV6
				struct IPV6
				{
					union
					{
						struct { uint08 mB0,mB1,mB2,mB3,mB4,mB5,mB6,mB7,mB8,mB9,mB10,mB11,mB12,mB13,mB14,mB15; };
						struct { uint16 mW0,mW1,mW2,mW3,mW4,mW5,mW6,mW7; };
						struct { uint32 mU0,mU1,mU2,mU3; };
						uint08 mBArray[16];
						uint16 mWArray[8];
						uint32 mUArray[4];
					};

					IPV6()
					{
						memset(mBArray,0,16);
					}

					IPV6(const uint08 ip[16])
					{
						memcpy(mBArray,ip,16);
					}

					//! \brief initialize the class from an os address struct
					IPV6(const SocketOSIPV6* address);

					//! \brief copy the address to an os address struct
					void copyToOSAddress(SocketOSIPV6* address) const;


					Common::String toString() const;
				};
#endif

			}
		}
	}
}



