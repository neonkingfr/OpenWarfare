// (C) xaitment GmbH 2006-2012

#pragma once 

#include <xait/common/network/sockets/Socket.h>


namespace XAIT
{
	namespace Common
	{
		namespace Network
		{
			namespace Sockets
			{
				class TCPSocket : public Socket
				{
				public:
					//! \brief Create a tcpsocket
					//! \param addressFamily	Specifies the address family which should be used for the socket
					XAIT_COMMON_DLLAPI TCPSocket(const AddressFamily addressFamily= ADDRFAMILY_INET4);

					//! \brief Opens the socket for listening 
					//! \returns True if the socket is set to listen, false otherwise. Socket error can be retrieved with Socket::getLastError.
					XAIT_COMMON_DLLAPI bool listen(const HostAddress& listenAdapter, const int32 maxPendingConnections= SOCKET_MAX_CONNECTIONS);

					//! \brief Connect to a remote host
					//! \returns True if the socket connected, false otherwise. Socket error can be retrieved with Socket::getLastError.
					XAIT_COMMON_DLLAPI bool connect(const HostAddress& remoteAddress);

					//! \brief disconnects a connection
					XAIT_COMMON_DLLAPI bool disconnect();

					//! \brief accept a connection
					//! \param remoteAddress	if non-NULL the host address will be filled with the informations of the connected remote host. Only valid if a connection is established.
					//! \returns a new socket or NULL if an error occured, use Socket::getLastError for the full error. 
					//! \remarks Works only for sockets in listen mode. This call is blocking, so if there is no
					//!			 pending connection it will wait. Use select if you do not want to block.
					XAIT_COMMON_DLLAPI TCPSocket* accept(HostAddress* remoteAddress);

					//! \brief receive number of bytes from the socket in the buffer
					//! \param buffer		buffer to fill
					//! \param maxLength	maximum bytes the buffer can hold
					//! \param bufferOffset	byte offset in the buffer where data should be written
					//! \returns number of bytes received, or -1 if an error occured
					//! \remarks Receives as much data as the buffer can hold, or until the socket has no more data.
					XAIT_COMMON_DLLAPI int32 receive(void* buffer, const uint32 maxLength, const uint32 bufferOffset= 0);

					//! \brief send number of bytes to the socket
					//! \param buffer		buffer to send content from
					//! \param length		number of bytes to send from the buffer to the socket
					//! \param bufferOffset	byte offset in the buffer where data should be read
					//! \returns the number of bytes which have been send
					//! \remarks Sends as much data as the output buffer of the socket can accept
					XAIT_COMMON_DLLAPI int32 send(const void* buffer, const uint32 length, const uint32 bufferOffset= 0);

				private:
					TCPSocket(const SocketOSHandle& osHandle);
				};
			}
		}
	}
}

