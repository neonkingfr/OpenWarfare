// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/debug/LogDispatcher.h>
#include <xait/common/memory/MemoryManaged.h>
#include <iostream>

namespace XAIT
{
	namespace Common
	{
		namespace Debug
		{
			class BasicLogDispatcher : public LogDispatcher
			{
			public:
				virtual void handleLogReport(const LogReport& report)
				{
					switch (report.mType)
					{
					case LT_DEBUG_MESSAGE:
						std::cout << "DbgMsg: " << report.mMessage << std::endl;
						break;
					case LT_DEBUG_ERROR:
						std::cout << "DbgErr: " << report.mMessage << std::endl;
						break;
					case LT_MESSAGE:
						std::cout << "Msg:    " << report.mMessage << std::endl;
						break;
					case LT_WARNING:
						std::cout << "Warning:" << report.mMessage << std::endl;
						break;
					case LT_ERROR:
						std::cerr << "Error: " << report.mMessage << std::endl;
						break;
					default:
						std::cerr << "UNKNOWN REPORT TYPE: " << report.mMessage << std::endl;
					}
				}

			};
		}
	}
}

