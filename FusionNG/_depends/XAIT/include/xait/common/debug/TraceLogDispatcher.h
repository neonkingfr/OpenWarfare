// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/debug/LogDispatcher.h>
#include <xait/common/debug/Trace.h>

namespace XAIT
{
	namespace Common
	{
		namespace Debug
		{
			class TraceLogDispatcher : public LogDispatcher
			{
			public:
				void sendTrace(const Trace::XAITraceType traceType, const LogReport& report)
				{
					std::cout << "Log: " << report.mMessage << std::endl;

					_TRACE_DEF_STREAM;
					_TRACE_STREAM << "(" << report.mInstanceName << ") " << report.mFunctionName << " : " << report.mMessage;
					Trace::TraceMessage(traceType,_TRACE_STREAM);
				}

				virtual void handleLogReport(const LogReport& report)
				{
					switch (report.mType)
					{
					case LT_DEBUG_MESSAGE:
						sendTrace(Trace::TRACE_MESSAGE,report);
						break;
					case LT_DEBUG_ERROR:
						sendTrace(Trace::TRACE_ERROR,report);
						break;
					case LT_MESSAGE:
						sendTrace(Trace::TRACE_MESSAGE,report);
						break;
					case LT_WARNING:
						sendTrace(Trace::TRACE_ERROR,report);
						break;
					case LT_ERROR:
						sendTrace(Trace::TRACE_ERROR,report);
						break;
					default:
						sendTrace(Trace::TRACE_ERROR,report);
						break;
					}
				}

			};
		}
	}
}

