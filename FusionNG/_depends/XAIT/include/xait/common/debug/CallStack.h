// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/String.h>
#include <xait/common/container/List.h>

namespace XAIT
{
	namespace Common
	{
		namespace Debug
		{
			class CallStack
			{
			public:
				struct CallStackEntry
				{
					Common::String		mFunctionName;		//!< name of the called function in the current frame
					Common::String		mLineFileName;		//!< file from which the function was called
					uint32				mLineNumber;		//!< line number in the CallFile from which the function was called
					Common::String		mModuleName;		//!< name of the module from which the function was called
				};

				enum CallStackListingOption
				{
					CSOPT_NONE		= 0x00,		//!< show nothing (listing disabled)
					CSOPT_FUNC		= 0x01,		//!< show the name of the function
					CSOPT_FILE		= 0x02,		//!< show file and linenumber from which the function was called
					CSOPT_MODULE	= 0x08,		//!< show the module from the function was called
					CSOPT_ALL		= 0x0F,		//!< show all possible information
				};

				typedef Container::List<CallStackEntry>		CallStackList;

				//! \brief default constructor
				//! \remark using Debug::StaticAllocator for callstack capture
				CallStack();

				//! \brief make a capture of the current callstack
				void capture();

				//! \brief get the captured callstack
				inline const CallStackList& getCallStack() const
				{
					return mCapture;
				}

				//! \brief clear the call stack
				inline void clearCallStack()
				{
					mCapture.clear();
				}

				//! \brief remove a entry at the top of the stack
				//! \remark does not throw an error if the stack is empty
				inline void popTop()
				{
					if (mCapture.empty()) return;
					mCapture.popFront();
				}

				//! \brief create a listing of the callstack elements, seperated by newline and in top down order
				//! \param options	flag list, which specifies what we should write in the listing
				Common::String createCallStackListing(const CallStackListingOption options= CSOPT_ALL) const;


			private:
				CallStackList		mCapture;
			};
		}
	}
}
