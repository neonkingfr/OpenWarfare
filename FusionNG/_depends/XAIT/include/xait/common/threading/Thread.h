// (C) xaitment GmbH 2006-2012

#pragma once 

#include <xait/common/FundamentalTypes.h>
#include <xait/common/memory/MemoryManaged.h>
#include <xait/common/threading/Threaded.h>
#include <xait/common/String.h>

// Win32 --------------------------------------------------------
#if XAIT_OS == XAIT_OS_WIN

#define THREAD_HANDLE_DEFINE()		typedef HANDLE	ThreadHandle;
#define THREAD_INVALID_HANDLE		NULL
#define THREAD_MAX_PRIORITY			THREAD_PRIORITY_TIME_CRITICAL
#define THREAD_DEFAULT_STACKSIZE	65536

// XBox360 --------------------------------------------------------
#elif XAIT_OS == XAIT_OS_XBOX360

#include <xtl.h>

#define THREAD_HANDLE_DEFINE()		typedef HANDLE	ThreadHandle;
#define THREAD_INVALID_HANDLE		NULL
#define THREAD_MAX_PRIORITY			THREAD_PRIORITY_TIME_CRITICAL
#define THREAD_DEFAULT_STACKSIZE	65536

// XBOX360EXT --------------------------------------------------------
#elif XAIT_OS == XAIT_OS_XBOX360EXT

#include <xdk.h>

#define THREAD_HANDLE_DEFINE()		typedef HANDLE	ThreadHandle;
#define THREAD_INVALID_HANDLE		NULL
#define THREAD_MAX_PRIORITY			THREAD_PRIORITY_TIME_CRITICAL
#define THREAD_DEFAULT_STACKSIZE	65536

// PS3 --------------------------------------------------------------------
#elif XAIT_OS == XAIT_OS_PS3

#include <sys/ppu_thread.h>

#define THREAD_HANDLE_DEFINE()		typedef sys_ppu_thread_t		ThreadHandle;
#define THREAD_INVALID_HANDLE		((uint64)-1)
#define THREAD_MAX_PRIORITY			3071
#define THREAD_DEFAULT_STACKSIZE	65536

// here we define the priority levels for our enum
#define THREAD_PRIORITY_IDLE			0
#define THREAD_PRIORITY_LOWEST			1
#define THREAD_PRIORITY_BELOW_NORMAL	99
#define THREAD_PRIORITY_NORMAL			100
#define THREAD_PRIORITY_ABOVE_NORMAL	101
#define THREAD_PRIORITY_HIGHEST			200
#define THREAD_PRIORITY_TIME_CRITICAL	THREAD_MAX_PRIORITY

// WII --------------------------------------------------------------------
#elif XAIT_OS == XAIT_OS_WII

#include <revolution/os.h>

#define THREAD_HANDLE_DEFINE()			typedef OSThread*	ThreadHandle;
#define THREAD_INVALID_HANDLE			NULL
#define THREAD_DEFAULT_STACKSIZE		32768

#define THREAD_MAX_PRIORITY				0
#define THREAD_PRIORITY_IDLE			31
#define THREAD_PRIORITY_LOWEST			30
#define THREAD_PRIORITY_BELOW_NORMAL	17
#define THREAD_PRIORITY_NORMAL			16
#define THREAD_PRIORITY_ABOVE_NORMAL	15
#define THREAD_PRIORITY_HIGHEST			4
#define THREAD_PRIORITY_TIME_CRITICAL	THREAD_MAX_PRIORITY

// Linux / Mac ------------------------------------------------------------------

#elif XAIT_OS == XAIT_OS_LINUX || XAIT_OS == XAIT_OS_MAC

#include <pthread.h>
#include <xait/common/utility/PointerHandle.h>


#define THREAD_HANDLE_DEFINE()		typedef Common::Utility::PointerHandle<pthread_t>	ThreadHandle;
#define THREAD_INVALID_HANDLE		ThreadHandle()
#define THREAD_MAX_PRIORITY			-6
#define THREAD_DEFAULT_STACKSIZE	65536

// here we define the priority levels for our enum
#define THREAD_PRIORITY_IDLE			0
#define THREAD_PRIORITY_LOWEST			-1
#define THREAD_PRIORITY_BELOW_NORMAL	-2
#define THREAD_PRIORITY_NORMAL			-3
#define THREAD_PRIORITY_ABOVE_NORMAL	-4
#define THREAD_PRIORITY_HIGHEST			-5
#define THREAD_PRIORITY_TIME_CRITICAL	THREAD_MAX_PRIORITY


// Unknown OS --------------------------------------------------------------------
#else	// unkown os

#define THREAD_HANDLE_DEFINE()	typedef void*	ThreadHandle;
#define THREAD_INVALID_HANDLE	NULL
#define THREAD_MAX_PRIORITY		1

#endif


namespace XAIT
{
	namespace Common
	{
		namespace Threading
		{
			THREAD_HANDLE_DEFINE();

			//! \brief A prototype for a main function of a thread
			typedef int32	(*ThreadMain) (void* param);


			//! \brief a class specifying the start parameter of a thread
			//! \remark This class supports the creation of threads on certain platforms
			//!			so that we can get a certain compatibility.
			struct ThreadParam
			{
				ThreadMain		mMainFunc;
				void*			mParam;
				uint08*			mStack;			//!< needed by wii

				ThreadParam()
					: mMainFunc(NULL),mParam(NULL),mStack(NULL)
				{}
			};


			//! \brief a wrapper class for thread handling on the different platforms
			//! \remark The class is memory managed, as for the wii we have to create
			//!			our own stack.
			class Thread : public Common::Memory::MemoryManaged
			{
			public:
				//! \brief priority of the thread. 
				//! \remark This is aligned to the windows priority system as this
				//! is the least flexible. (See SetThreadPriority in MSDN)
				enum ThreadPriority
				{
					TPRIO_IDLE			= THREAD_PRIORITY_IDLE,
					TPRIO_LOWEST		= THREAD_PRIORITY_LOWEST,
					TPRIO_BELOW_NORMAL	= THREAD_PRIORITY_BELOW_NORMAL,
					TPRIO_NORMAL		= THREAD_PRIORITY_NORMAL,
					TPRIO_ABOVE_NORMAL	= THREAD_PRIORITY_ABOVE_NORMAL,
					TPRIO_HIGHEST		= THREAD_PRIORITY_HIGHEST,
					TPRIO_TIME_CRITICAL	= THREAD_PRIORITY_TIME_CRITICAL,
				};

				//! \brief create a new thread
				//! \param startFunc		function that will be entered inside the thread after the thread is created
				//! \param param			parameter that will be passed to the start function
				//! \param name				name of the thread. This will be used in the debugger. 
				//!							Restriction: 
				//!								- currently no reflection to the code
				//!								- limited amount of characters for certain platforms (will be truncated)
				//! \param stackSize		size of the stack in bytes, if 0 the same size as for the main thread will be used
				XAIT_COMMON_DLLAPI Thread(ThreadMain startFunc, void* param, const Common::String& name, const int32 stackSize= THREAD_DEFAULT_STACKSIZE);

				//! \brief create a new thread on an object
				//! \param threadObject		main object for the thread
				//! \param name				name of the thread. This will be used in the debugger. 
				//!							Restriction: 
				//!								- currently no reflection to the code
				//!								- limited amount of characters for certain platforms (will be truncated)
				//! \param stackSize		size of the stack in bytes, if 0 the same size as for the main thread will be used
				XAIT_COMMON_DLLAPI Thread(Threaded* threadObject, const Common::String& name, const int32 stackSize= THREAD_DEFAULT_STACKSIZE);

				//! \brief create a thread from an already initialized handle 
				//! \remark Use this with care, the main purpose is for transferring threads from game to library
				XAIT_COMMON_DLLAPI Thread(ThreadHandle handle);

				//! \brief destructor
				//! \remark Closes only the handle, you should have joined the thread before
				XAIT_COMMON_DLLAPI ~Thread();

				//! \brief gets a thread class to the current running thread
				//! \remark This class instance is not the same as the one used to start the thread.
				XAIT_COMMON_DLLAPI static Thread getCurrentThread();

				//! \brief get the id of the thread (mostly needed for debugging)
				XAIT_COMMON_DLLAPI static int32 getCurrentID();

				//! \brief joins the thread back to the creation thread
				//! \returns The exit code from the main loop of the thread
				//! \remark This blocks the calling thread until the joinable thread has terminated (pay attention to deadlocks).
				XAIT_COMMON_DLLAPI int32 join();

				//! \brief set the priority of the thread
				//! \param priority		priority which should be assigned, higher is better
				//! \remark See THREAD_MAX_PRIORITY for maximum thread priority
				XAIT_COMMON_DLLAPI void setPriority(const ThreadPriority priority);

				//! \brief get the priority of the thread
				XAIT_COMMON_DLLAPI ThreadPriority getPriority();

				//! \brief exit the calling thread
				//! \param errorCode	error code which should be reported on join()
				XAIT_COMMON_DLLAPI static void exit(const int32 errorCode);

				//! \brief let the calling thread sleep for a certain amount of time
				//! \param milliSeconds		number of milli seconds the thread should sleep
				XAIT_COMMON_DLLAPI static void sleep(const uint32 milliSeconds);

				//! \brief releases the handle of the thread
				//! \remark This will release the handle from the thread class, leaving it unchanged. The handle of the thread class
				//! will get set to invalid.
				XAIT_COMMON_DLLAPI ThreadHandle releaseHandle();

				//! \brief close thread handle
				XAIT_COMMON_DLLAPI void closeHandle();

			protected:
				ThreadHandle	mHandle;
				bool			mPsydoInstance;			//!< if true, this class has been create by get current thread and has thus another destructor

				//! \param startFunc		function that will be entered inside the thread after the thread is created
				//! \param param			parameter that will be passed to the start function
				//! \param name				name of the thread. This will be used in the debugger. 
				//!							Restriction: 
				//!								- currently no reflection to the code
				//!								- limited amount of characters for certain platforms (will be truncated)
				//! \param stackSize		size of the stack in bytes, if 0 the same size as for the main thread will be used
				//! \returns If successful returns a valid thread handle, if not returns THREAD_INVALID_HANDLE
				ThreadHandle createThread(ThreadMain startFunc, void* param, const Common::String& name, const int32 stackSize);


			private:
				ThreadParam		mThreadParam;	//!< additional thread start parameter, not used by all platforms
			};
		}
	}
}
