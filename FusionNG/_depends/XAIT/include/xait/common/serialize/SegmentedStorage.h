// (C) 2011 xaitment GmbH

#pragma once 

#include <xait/common/serialize/SerializeStorage.h>
#include <xait/common/container/List.h>

namespace XAIT
{
	namespace Common
	{
		namespace Serialize
		{
			//! \brief a segmented storage is built up from other storage classes
			//! \remark The segmented storage is always an input storage, it can be filled
			//!	with extra methods.
			class SegmentedStorage : public SerializeStorage
			{
				struct StorageSegment
				{
					SharedPtr<SerializeStorage>		mStorage;	//!< current storage
					SharedPtr<Position>				mFrom;		//!< start pos in this storage (inclusive)
					SharedPtr<Position>				mTo;		//!< end pos in this storage (exclusive)
				};
			public:
				class SegmentedPosition : public Position
				{
				public:
					SegmentedPosition()
						: Position(STORAGE_SEGMENTED)
					{}

					bool equals(const SharedPtr<Position>& otherPos) const
					{
						SegmentedPosition* pos= (SegmentedPosition*)(otherPos.get());
						return pos->mSegmentIter == mSegmentIter && pos->mCurrStoragePos == mCurrStoragePos;
					}

				private:
					friend class SegmentedStorage;

					Container::List<StorageSegment>::Iterator	mSegmentIter;
					SharedPtr<Position>							mCurrStoragePos;
				};

				//! \brief constructor
				//! \remark builds always an input storage
				SegmentedStorage();

				//! \brief virtual destructor, needed
				~SegmentedStorage() {};


				//! \brief insert an segment into the segmented storage
				//! \param storage		storage to insert as an segment
				//! \param from			position in the storage where the segment starts (from position is already used)
				//! \param to			position in the storage until which the segment reaches (not included)
				//! \remark Neighbouring segments will be combined if possible
				void insertSegment(const SharedPtr<SerializeStorage>& storage,
								   const SharedPtr<Position>& from,
								   const SharedPtr<Position>& to);


				//! \brief reset segment storage to the beginning
				void resetToBegin();

				// derived methods
				//! \brief opens the segmented storage
				//!	\remark This has a dummy implementation and the stream will not be used
				void open(const SharedPtr<IO::Stream>& stream);

				//! \brief closes the segmented storage and loses its content
				void close();

				SharedPtr<Position> getPosition();
				void setPosition(const SharedPtr<Position>& storagePos);

				void writeEnterClassTree(const Common::String& name, const Common::String& typeName, const Common::String& subTypeName, const int32 version);
				void writeExitClassTree();
				void writeValue(const Common::String& name, const bool value);
				void writeValue(const Common::String& name, const uint08 value);
				void writeValue(const Common::String& name, const int08 value);
				void writeValue(const Common::String& name, const uint16 value);
				void writeValue(const Common::String& name, const int16 value);
				void writeValue(const Common::String& name, const uint32 value);
				void writeValue(const Common::String& name, const int32 value);
				void writeValue(const Common::String& name, const uint64 value);
				void writeValue(const Common::String& name, const int64 value);
				void writeValue(const Common::String& name, const float32 value);
				void writeValue(const Common::String& name, const float64 value);
				void writeValue(const Common::String& name, const Common::BString& value);
				void writeValue(const Common::String& name, const Common::WString& value);

				void readSkipValue(Common::String& name, StreamID& streamID);
				StreamID readNextStreamID();
				void readEnterClassTree(Common::String& name, Common::String& typeName, Common::String& subTypeName, int32& version);
				void readExitClassTree();
				void readValue(Common::String& name, bool& value);
				void readValue(Common::String& name, uint08& value);
				void readValue(Common::String& name, int08& value);
				void readValue(Common::String& name, uint16& value);
				void readValue(Common::String& name, int16& value);
				void readValue(Common::String& name, uint32& value);
				void readValue(Common::String& name, int32& value);
				void readValue(Common::String& name, uint64& value);
				void readValue(Common::String& name, int64& value);
				void readValue(Common::String& name, float32& value);
				void readValue(Common::String& name, float64& value);
				void readValue(Common::String& name, Common::BString& value);
				void readValue(Common::String& name, Common::WString& value);

			private:
				Container::List<StorageSegment>				mSegments;
				Container::List<StorageSegment>::Iterator	mSegmentIter;
				StorageSegment								mCurrSegment;
				SharedPtr<Position>							mCurrStoragePos;

				//! \brief goto the next item in the storage
				void gotoNextPosition();

			};
		}
	}
}
