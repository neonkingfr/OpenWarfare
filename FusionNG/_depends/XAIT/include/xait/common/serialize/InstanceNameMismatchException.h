// (C) 2011 xaitment GmbH

#pragma once 

#include <xait/common/serialize/SerializeException.h>

namespace XAIT
{
	namespace Common
	{
		namespace Serialize
		{
			class InstanceNameMismatchException : public SerializeException
			{
			public:
				InstanceNameMismatchException(const Common::String& errorMsg, const Common::String& foundName, const Common::String& expectedName)
					: SerializeException(errorMsg),mFoundName(foundName),mExpectedName(expectedName)
				{}

				virtual Common::String getExceptionName() const
				{
					return "InstanceNameMismatchException";
				}

				//! \brief get the name which has been found
				inline Common::String getFoundName() const
				{
					return mFoundName;
				}

				//! \brief get the name which has been excepted
				inline Common::String getExpectedName() const
				{
					return mExpectedName;
				}

			protected:
				virtual void getExceptionParameter(Container::List<Container::Pair<Common::String,Common::String> >& parameters)
				{
					SerializeException::getExceptionParameter(parameters);

					parameters.pushBack(Container::MakePair(Common::String("FoundName"),Common::String(mFoundName)));
					parameters.pushBack(Container::MakePair(Common::String("ExpectedName"),Common::String(mExpectedName)));
				}

			private:
				Common::String		mFoundName;
				Common::String		mExpectedName;
			};
		}
	}
}

