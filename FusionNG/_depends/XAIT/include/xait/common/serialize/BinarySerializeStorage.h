// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/serialize/SerializeStorage.h>
#include <xait/common/platform/Endian.h>
#include <xait/common/container/LookUp.h>
#include <xait/common/container/SegmentVector.h>

#define XAIT_BINARY_SERIALIZE_STORAGE_VERSION	4


#define HAS_FEAT_INSTANCENAMES()		(mFeatures & SFEAT_INSTANCENAMES)
#define HAS_FEAT_STREAMIDS()			(mFeatures & SFEAT_STREAMIDS)


namespace XAIT
{
	namespace Common
	{
		namespace Serialize
		{
			class BinarySerializeStorage : public SerializeStorage
			{
			public:
				class BinaryPosition : public Position
				{
				public:
					BinaryPosition(const uint64 streamPosition)
						: Position(SerializeStorage::STORAGE_BINARY),mStreamPos(streamPosition)
					{}

					inline uint64 getStreamPos() const
					{
						return mStreamPos;
					}

					bool equals(const SharedPtr<Position>& otherPos) const
					{
						return ((BinaryPosition*)(otherPos.get()))->mStreamPos == mStreamPos;
					}

				private:
					uint64		mStreamPos;
				};

				enum SerializerFeature
				{
					SFEAT_NONE			= 0x00,
					SFEAT_INSTANCENAMES	= 0x01,
					SFEAT_STREAMIDS		= 0x02,
					SFEAT_ALL			= SFEAT_INSTANCENAMES | SFEAT_STREAMIDS,
				};

				//! \brief create a binary serializer
				//! \param serialMode		mode in which the serializer runs
				//! \param endiness			multi byte ordering which should be used when writing (ignored on reading!)
				XAIT_COMMON_DLLAPI BinarySerializeStorage(const SerializeStorage::SerializeMode serialMode, const Platform::Endian::Endiness endiness= (Platform::Endian::Endiness)XAIT_ARCH_ENDIAN);

				XAIT_COMMON_DLLAPI ~BinarySerializeStorage();

				//! \brief set serializer features on write
				//! \remarks Features can only be set on an unopened output stream
				inline void setSerializerFeatures(const SerializerFeature features)
				{
					X_ASSERT_MSG(this->getSerializeMode() == SMODE_OUTPUT,"Serializer features can only be set on an output stream");
					X_ASSERT_MSG(this->mStream == NULL,"Serializer features can only be set on an unopened stream");
					mFeatures= features;
				}

				//! \brief get serializer features
				inline SerializerFeature getSerializerFeatures() const
				{
					return mFeatures;
				}

				// derived methods

				//! \brief opens the binary serialize storage on an io stream
				//! \remark The io stream must support read or write and seek in both cases, otherwise there will be an exception
				XAIT_COMMON_DLLAPI bool open(IO::Stream* stream);
				void close();

				SharedPtr<Position> getPosition();
				void setPosition(const SharedPtr<Position>& storagePos);

				void writeEnterClassTree(const char* name, const char* typeName, const char* subTypeName, const int32 version);
				void writeExitClassTree();
				void writeValue(const char* name, const bool value);
				void writeValue(const char* name, const uint08 value);
				void writeValue(const char* name, const int08 value);
				void writeValue(const char* name, const uint16 value);
				void writeValue(const char* name, const int16 value);
				void writeValue(const char* name, const uint32 value);
				void writeValue(const char* name, const int32 value);
				void writeValue(const char* name, const uint64 value);
				void writeValue(const char* name, const int64 value);
				void writeValue(const char* name, const float32 value);
				void writeValue(const char* name, const float64 value);
				void writeValue(const char* name, const Common::Math::Vec2f& value);
				void writeValue(const char* name, const Common::Math::Vec3f& value);
				void writeValue(const char* name, const Common::BString& value);
				void writeValue(const char* name, const Common::WString& value);

				void writeArray(const char* name, const int08* buffer, const uint32 bufferLen);
				void writeArray(const char* name, const int32* buffer, const uint32 bufferLen);
				void writeArray(const char* name, const float32* buffer, const uint32 bufferLen);
				void writeArray(const char* name, const Common::Math::Vec3f* buffer, const uint32 bufferLen);

				void readSkipValue(const char*& name, StreamID& streamID);
				StreamID readNextStreamID();
				void readEnterClassTree(const char*& name, const char*& typeName, const char*& subTypeName, int32& version);
				void readExitClassTree();
				void readValue(const char*& name, bool& value);
				void readValue(const char*& name, uint08& value);
				void readValue(const char*& name, int08& value);
				void readValue(const char*& name, uint16& value);
				void readValue(const char*& name, int16& value);
				void readValue(const char*& name, uint32& value);
				void readValue(const char*& name, int32& value);
				void readValue(const char*& name, uint64& value);
				void readValue(const char*& name, int64& value);
				void readValue(const char*& name, float32& value);
				void readValue(const char*& name, float64& value);
				void readValue(const char*& name, Common::Math::Vec2f& value);
				void readValue(const char*& name, Common::Math::Vec3f& value);
				void readValue(const char*& name, Common::BString& value);
				void readValue(const char*& name, Common::WString& value);

				void readArray(const char*& name, int08* buffer, const uint32 bufferLen);
				void readArray(const char*& name, int32* buffer, const uint32 bufferLen);
				void readArray(const char*& name, float32* buffer, const uint32 bufferLen);
				void readArray(const char*& name, Common::Math::Vec3f* buffer, const uint32 bufferLen);


			protected:
				IO::Stream*													mStream;

				Platform::Endian::Endiness									mEndiness;			//!< endiness which should be used during streaming
				Container::LookUp<const char*,uint32,Container::StringComp>	mInstanceNameToID;	//!< global lookup with all instance names streamed
				Container::LookUp<const char*,uint32,Container::StringComp>	mTypeNameToID;		//!< global lookup with all type names that are streamed
				Container::SegmentVector<Common::String>					mInstanceNames;		//!< all instance names
				Container::SegmentVector<Common::String>					mTypeNames;			//!< all type names
				SerializerFeature											mFeatures;


				//! \brief read an instance name from the stream
				//! \param name		read instance name
				//! \exception SerializeException	thrown if stream corrupted (name id not present in the name id lookup)
				void readInstanceName(const char*& name);

				//! \brief read a type name from the stream
				//! \param typeName		read type name
				//! \exception SerializeException	thrown if stream corrupted (name id not present in the name id lookup)
				void readTypeName(const char*& typeName);

				//! \brief write an instance name to the stream
				//! \param name		instance name to write
				void writeInstanceName(const char* name);

				//! \brief write a type name to the stream
				//! \param typeName		type name to write
				void writeTypeName(const char* typeName);

				//! \brief write some element unit to the stream
				//! \param val	value to write to the stream, should be of an element type
				template<typename T>
				inline void writeToStream(const T val)
				{
					T valConv;
					Platform::Endian::convertSystemTo(valConv,val,mEndiness);
					mStream->write(&valConv,sizeof(T));
				}

				//! \brief read some element unit from the stream
				//! \param val	value which should be read
				template<typename T>
				inline void readFromStream(T& val)
				{
					mStream->read(&val,sizeof(T));
					Platform::Endian::convertToSystem(val,val,mEndiness);
				}

				template<typename T>
				inline void writeToStream(const T* val, const uint32 numElements)
				{
					if (needEndianConversion())
					{
						T valConv;
						for(uint32 i= 0; i < numElements; ++i)
						{
							Platform::Endian::swapBytes(valConv,val[i]);
							mStream->write(valConv);
						}
					}
					else
					{
						mStream->write(val,numElements * sizeof(T));
					}
				}

				template<typename T>
				inline void readFromStream(T* val, const uint32 numElements)
				{
					if (needEndianConversion())
					{
						for(uint32 i= 0; i < numElements; ++i)
						{
							mStream->read(val[i]);
							Platform::Endian::swapBytes(val[i],val[i]);
						}
					}
					else
					{
						mStream->read(val,numElements * sizeof(T));
					}
				}


				//! \brief read the stream identifier
				//! \param expectedStreamID		stream id which should be read next
				//! \exception TypeMismatchException	thrown if the streamid does not match the requested one
				void readStreamID(const StreamID expectedTypeID);

				//! \brief write the stream identifier packet
				inline void writeStreamID(const StreamID streamID)
				{
					if (HAS_FEAT_STREAMIDS())
					{
						const uint08 sID= (uint08)streamID;
						mStream->write(&sID,sizeof(uint08));
					}
				}

				//! \brief read the instance/type name lookup table from the stream
				//! \remark This method expects that the stream position is at the
				//!			position byte of the lookup tables
				void readNamesLookupTables();

				//! \brief write the instance/type name lookup table to the stream at the current position
				//! \param markerPos	position in the stream where to write the start of the lookup table in the stream
				void writeNameLookupTable(const uint64 markerPos);

				inline bool needEndianConversion() const { return mEndiness != Platform::Endian::getSystemEncoding(); }

			};
		}
	}
}
