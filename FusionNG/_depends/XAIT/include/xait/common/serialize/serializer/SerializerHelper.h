// (C) 2011 xaitment GmbH

#pragma once 

// binds a serializer to a type (not suitable for template types)
// THIS MACRO MUST BE CALLED OUTSIDE OF A NAMESPACE, otherwise an error will rise.
#define SERIALIZER_BIND_TYPE(serializer,type)\
	namespace XAIT { namespace Common { namespace Serialize { namespace Serializer {\
	template<typename INST_T>\
	class GetSerializer<type,INST_T>\
	{ public: typedef serializer	Serializer; };\
	}	}	}	}


namespace XAIT
{
	namespace Common
	{
		namespace Serialize
		{
			namespace Serializer
			{
				//! \brief template that gets me the serializer for a specified object type
				//! \param OBJECT_T		object for which we want the serializer
				//! \param INST_T		object which should be used to create an instance of this object if needed (depends on the place where it is used)
				//! \remark This is just a base implementation, use template specification for
				//!			further types, or set the Serializer typedef in your class.
				template<typename OBJECT_T, typename INST_T>
				class GetSerializer
				{
				public:
					typename OBJECT_T::Serializer	Serializer;
				};

				//! \brief build a serializer
				//! \param helper1		needed for object type
				//! \param helper2		needed for instance type, where certain specialisations 
				//!						need the pointer. The instance type must have a
				//!						function createInstance(OBJECT_T*&, Common::String typeName) 
				//!						if OBJECT_T is streamed as pointer.
				//! \remark For special purpose classes (PointerSerializer,ContainerSerializer) 
				//!			we need the instance of the class which can create new instances of
				//!			the serialized object.
				template<typename OBJECT_T,typename INST_T>
				inline static typename GetSerializer<OBJECT_T,INST_T>::Serializer BuildSerializer(const OBJECT_T& helper1, INST_T* helper2)
				{
					return GetSerializer<OBJECT_T,INST_T>::Serializer();
				}
			}
		}
	}
}
