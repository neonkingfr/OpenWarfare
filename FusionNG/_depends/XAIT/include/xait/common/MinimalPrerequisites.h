// (C) xaitment GmbH 2006-2012

#pragma once 

#include <xait/common/CommonCompileSettings.h>
#include <xait/common/FundamentalTypes.h>
#include <xait/common/debug/Assert.h>
#include <xait/common/SharedPtr.h>
#include <xait/common/String.h>
