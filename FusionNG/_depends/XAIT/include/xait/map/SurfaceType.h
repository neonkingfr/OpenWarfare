// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/FundamentalTypes.h>
#include <xait/common/String.h>

namespace XAIT
{
	namespace Map
	{
		//! \brief This struct describes a surface type.
		struct SurfaceType
		{
			Common::String		mName;		//!< the name of the surface type
			uint16				mID;		//!< a user provided surface type ID. This ID will be stored in the NavMeshContainer::NavNode.

			//! \brief Constructor.
			//! \param name the name of the surface type
			//! \param id the surface type ID
			SurfaceType(const Common::String& name, const uint16 id)
				: mName(name),mID(id)
			{}

			//! \brief Default constructor.
			//!
			//! Initializes the surface type with the name "Default" and the ID 0xFFFF
			SurfaceType()
				: mName("Default"),mID(X_MAX_UINT16)
			{}
		};
	}
}
