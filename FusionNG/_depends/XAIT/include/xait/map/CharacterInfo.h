// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/FundamentalTypes.h>
#include <xait/common/String.h>
#include <xait/map/NavMeshNode.h>

namespace XAIT
{
	namespace Map
	{
		class NavMeshContainer;

		//! \brief Defines the properties of a character used for building the navigation mesh.
		//!
		//! This struct defines the properties of a character which can move on a generated navigation mesh. Each Character
		//! has some physical properties that are relevant for the creation of the navigation mesh. The character has a unique
		//! name so that it can be identified by it. The final NavMeshContainer contains the character definitions used
		//! to build the NavMesh. Each character has a MaxHeightClassID value which defines where the character can and where he can not move in the
		//! navigation mesh. If the HeightClassID of a navigation node is greater than or equal to the MaxHeightClassID, the character can walk on a certain navigation node.
		struct CharacterInfo
		{
			Common::String	mName;				//!< name of the character
			int64			mUserID;			//!< user supplied id
			float32			mPhysicalHeight;	//!< physical height of the character
			float32			mRadius;			//!< character radius

			//////////////////////////////////////////////////////////////////////////////////
			// The following data is being filled by the NavMesh algorithm
			uint08			mMaxHeightClassID;		//!< the maximum valid height class id, so that this character can walk (will be filled by the NavMesh generation algorithm). All navigation nodes which have HeightClassIDs greater than or equal to this value can be used by this character.

			//! \brief checks if a given NavMesh node can be used by a character
			inline bool canWalkNode(const NavMeshNode& node) const
			{
				return node.mHeightClassID < mMaxHeightClassID;
			}


			//! \brief checks if a given polygon edge (given by a nodeID and a neighbour id) can be used by a character
			bool canWalkEdge( const NavMeshContainer*, GlobalNodeID nodeID,  uint32 neighbourID );
		};
	}
}

