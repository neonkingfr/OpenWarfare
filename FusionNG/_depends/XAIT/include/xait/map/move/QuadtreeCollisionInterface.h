// (C) xaitment GmbH 2006-2012

 #pragma once

#include <xait/common/container/IDVector.h>

#include <xait/map/CompileDefines.h>
#include <xait/map/move/CollisionInterface.h>
#include <xait/map/move/QuadTree3D.h>

namespace XAIT 
{ 
	namespace Map
	{
		namespace Move
		{
			/*! \brief our default implementation of the collision interface
			 *	see CollisionInterface.h
			 *	this implementation supports CircleCollisionShapes 
			 */
			class QuadtreeCollisionInterface : public Map::Move::CollisionInterface
			{
				struct EntityData
				{
					EntityData(const Common::Math::Vec3f& velocity, float32 radius)
						: mVelocity(velocity)
						, mRadius(radius)
						, mQuadtreeID(X_MAX_UINT32)
					{}

					Common::Math::Vec3f mVelocity;
					float32				mRadius;
					uint32				mQuadtreeID;
				};
			public: 
				//! \brief constructor
				//! \param minCellSize			the minimal size of an cell 
				//! \param upComponent			the up component of the world space
				XAIT_MAP_DLLAPI QuadtreeCollisionInterface( float32 minCellSize,uint32 upComponent );

				//! \brief destructor
				XAIT_MAP_DLLAPI virtual ~QuadtreeCollisionInterface();

				//! \brief query all registered obstacles in a certain range
				//! \param obstacles		the obstacles in the given range (return value)
				//! \param position			the position of the query
				//! \param radius			the radius of the query
				//! \param ignoreID			a ColliderID that is ignored (e.g. colliderID of the own entity)
				XAIT_MAP_DLLAPI virtual void getDynamicObstaclesInRange(Common::Container::Vector<CollisionQueryEntry>& obstacles, const Common::Math::Vec3f& position, float32 radius, uint32 ignoreID = X_MAX_UINT32);

				//! \brief register an obstacle with a certain collision shape
				//! \param collisionShape		the collision shape of the the obstacle
				//! \returns an unique collider id
				//! \remark MoveEntities that are registered in the simulation world are registered automatically
				XAIT_MAP_DLLAPI virtual uint32 registerObstacle(CollisionShape* collisionShape);

				//! \brief deregister an obstacle 
				//! \param colliderID		the colliderID of the obstacle
				//! \return true if the obstacle is deregistered, false if collider is unknown
				//! \remark MoveEntities that are registered in the simulation world are deregistered automatically
				XAIT_MAP_DLLAPI virtual bool deregisterObstacle(uint32 colliderID);

				//! \brief update the data of an obstacle
				//! \param colliderID	the colliderID of the obstacle
				//! \param position		the new position
				//! \param velocity		the new velocity
				XAIT_MAP_DLLAPI virtual void updateObstacle(uint32 colliderID, const Common::Math::Vec3f& position, const Common::Math::Vec3f& velocity);
				
				//! \brief retrieve the biggest radius of all registered obstacles
				//! \returns the biggest radius of all registered obstacles
				XAIT_MAP_DLLAPI virtual float getBiggestObstacleRadius();
				
			protected:
				Common::Container::IDVector<EntityData>	mStoredEntities;
				QuadTree3D								mQuadtree;

				float32									mMaxRadius;
				uint32									mUpComp;

				//! \brief updatecall from the simulation world at the start of a frame
				//! \param frametime	the current gametime
				XAIT_MAP_DLLAPI virtual void update(const Frametime& frametime);
			};
		}
	}
}
