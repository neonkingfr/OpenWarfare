// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/debug/Trace.h>
#include <xait/common/container/List.h>
#include <xait/map/move/MoveEntity.h>
#include <xait/map/move/PathIterator.h>
#include <xait/map/path/CompiledSearchConfig.h>
#include <xait/map/path/SearchConfig.h>
#include <xait/map/path/PathResult.h>
#include <xait/map/path/SearchRequest.h>
#include <xait/map/path/PathSearch.h>
#include <xait/map/path/SearchRequest.h>

namespace XAIT
{
	namespace Map
	{
		namespace Move
		{
			class InverseMotionSlice;
			class PathManager;

			/*! \brief this class represents the default implementation of an entity that is able to follow a path
			 *	with reactive avoidance of dynamic objects. 
			 *	It implements the MoveEntity class and uses CircleShape to register at the collision interface.
			 *  The entity tries to avoid every object that is registered at the collision interface. 
			 */
			class PathFollowMoveEntity : public MoveEntity
			{
				friend class PathManager;
			public:
				//! \brief constructor				
				XAIT_MAP_DLLAPI PathFollowMoveEntity();

				//! \brief destructor
				XAIT_MAP_DLLAPI virtual ~PathFollowMoveEntity();

				//! \brief initialize the entity
				//! \param characterInfoName	the characterinfo specified in the navmesh
				//! \param maxSpeed				the max speed of the entity in worldunits/second
				//! \param navmesh				the navmesh the entity is using
				XAIT_MAP_DLLAPI void init(const Common::String& characterInfoName, float32 maxSpeed, const NavMeshContainer* navmesh);

				//! \brief initialize the entity
				//! \param characterInfo		the characterinfo specified in the navmesh
				//! \param maxSpeed				the max speed of the entity in worldunits/second
				//! \param navmesh				the navmesh the entity is using
				XAIT_MAP_DLLAPI void init(const CharacterInfo* characterInfo, float32 maxSpeed, const NavMeshContainer* navmesh);

				//! \brief initialize the entity
				//! \param collisionRadius	the collision radius (hard radius) of the entity
				//! \param maxSpeed			the max speed of the entity in worldunits/second
				//! \param navmesh			the navmesh the entity is using
				XAIT_MAP_DLLAPI void init(float32 collisionRadius, float32 maxSpeed, const NavMeshContainer* navmesh);

				//! \brief deinitialize the entity and deregister the entity from the simulationworld
				//! \remark this function is called automatically in the destructor if not done before.
				XAIT_MAP_DLLAPI void deinit();

				//! \brief set the used search config
				//! \param searchConfig		the used search config
				//! \remark if no search config is defined, a default searchconfig without restrictions is used 
				//! \remark	the search config is NOT deleted by the entity.
				XAIT_MAP_DLLAPI void setSearchConfig(const Path::SearchConfig* searchConfig);
				
				//! \brief get the used search config
				XAIT_MAP_DLLAPI const Path::SearchConfig* getSearchConfig() const;

				//! \brief suspends the entity - all updates are ignored
				inline void suspend()
				{
					mSuspended = true;
				}

				//! \brief resumes the entity
				inline void resume()
				{
					mSuspended = false;
				}

				//! \brief get the suspended flag
				//! \returns the suspended flag
				inline bool isSuspended()
				{
					return mSuspended;
				}

				//! \brief disables the collision
				//! \param flag		collisionflag
				inline void enableCollision(bool flag)
				{
					if (mCollisionEnabled && !flag && mEntityState == STATE_AVOID_ENTITY)
						mEntityState = STATE_FOLLOW_PATH;

					mCollisionEnabled = flag;
				}

				//! \brief get the collisionflag
				//! \returns the collisionflag
				inline bool getCollisionEnabled() const
				{
					return mCollisionEnabled;
				}


				//! \brief sets the position of the current entity on the navmesh
				//! \param position		the new entity position
				//! \returns false if the given position is not on the navmesh
				//! \remark the call is ignored if the given point is not on the navmesh
				//! \remark	if the entity is following a path, a new path is computed 
				XAIT_MAP_DLLAPI bool setEntityPosition(const Common::Math::Vec3f& position);

				//! \brief get the entity position
				//! \returns the entity position
				inline const Common::Math::Vec3f& getEntityPosition() const
				{
					return mEntityPosition;
				}

				//! \brief get the currently used navmesh
				inline const NavMeshContainer* getNavMesh() const
				{
					return mNavMesh;
				}

				//! \brief set the current path
				//! \param newPath				the new path
				//! \param setEntityOnStartPos	flag if the entity should be set to the start position of the the path
				//! \returns false if the distance between start position of the path and the current move position is to large (if setEntityOnStartPos==false)
				//! \remark if the start position is not the current entity position, the onPositionDivergency Event will be called at the next frame
				XAIT_MAP_DLLAPI bool setCurrentPath(const Path::PathResult& newPath, bool setEntityOnStartPos = false);

				//! \brief get the current path
				//! \return the current path if one exists, NULL otherwise
				XAIT_MAP_DLLAPI const Path::PathResult* getCurrentPath() const;

				//! \brief recomputes the current path
				//! \param continueCurrentPath	flag if the entity should follow the current path while computation
				//! \remark following the current path while computation possibly results in a call of onPositionDivergency to handle the changed position
				XAIT_MAP_DLLAPI void recomputeCurrentPath(bool continueCurrentPath);

				//! \brief checks all computed paths and recomputes the path if the path is now invalid
				//! \remark call this function if a dynamic blocking is activated
				XAIT_MAP_DLLAPI void recomputeInvalidPaths();

				//! \brief set a new target and remove all old targets including the waypoints
				//! \param target	the new target of the entity
				//! \returns the error code of the pathsearch initialization (see SearchRequest.h)
				XAIT_MAP_DLLAPI Path::SearchRequest::ErrorCode setNewTarget(const Common::Math::Vec3f& target);

				//! \brief add a waypoint
				//! \param position		the position of the waypoint
				//! \returns false if the waypoint is not on the navmesh and is not added, true if the point is added
				XAIT_MAP_DLLAPI bool addWaypoint(const Common::Math::Vec3f& position);

				//! \brief get the remaining waypoints of the path
				//! \returns the remaining waypoints of the path
				//! \remark the waypoints do NOT contain the target of the current path
				inline const Common::Container::List<Common::Math::Vec3f>& getWaypoints() const
				{
					return mWayPoints;
				}

				//! \brief clears the waypoint list
				XAIT_MAP_DLLAPI void clearWaypoints();

				//! \brief stops the entites. all waypoints are removed, a new endpoint is computed and the entity will slow down.
				XAIT_MAP_DLLAPI void stopEntity();

				//! \brief aborts everything and removes all waypoints
				XAIT_MAP_DLLAPI void abort();
				
				//! \brief get the collision radius of the entity
				//! \returns the collision radius of the entity
				inline float32 getCollisionRadius() const
				{
					return mCollisionRadius;
				}

				//! \brief get the soft radius factor of the entity
				//! \returns the collision radius of the entity
				inline float32 getSoftRadiusFactor() const 
				{
					return mSoftRadiusFactor; 
				}

				//! \brief set the soft radius collision factor
				//! \param val		the soft radius collision factor
				//! \remark the soft radius defines the distance that a unit tries to keep collision free
				//! \remark the factor is multiplied with the collision radius to compute the soft radius
				inline void setSoftRadiusFactor(float32 val)
				{ 
					X_ASSERT_MSG1(val >= 1.0f, "soft radius factor has to be > 1.0f",val);
					mSoftRadiusFactor = val; 
				}

				//! \brief get the max speed of the entity in worldunits/second
				//! \returns the max speed of the entity in worldunits/second
				inline float32 getMaxSpeed() const 
				{ 
					return mMaxSpeed; 
				}
				//! \brief set the max speed of the entity in worldunits/second
				//! \param maxSpeed		the max speed of the entity in worldunits/second (>= 0.0f)
				inline void setMaxSpeed(float32 maxSpeed) 
				{ 
					X_ASSERT_MSG1(maxSpeed >= 0.0f, "cannot set speed < 0.0f",maxSpeed);
					mMaxSpeed = maxSpeed;
					mMinSpeed = X_MIN(mMinSpeed,mMaxSpeed);
				}

				//! \brief get the min speed of the entity in worldunits/second while pathfollowing
				//! \returns the min speed of the entity in worldunits/second
				inline float32 getMinSpeed() const 
				{ 
					return mMinSpeed; 
				}
				//! \brief set the min speed of the entity in worldunits/second  while pathfollowing
				//! \param minSpeed		the max speed of the entity in worldunits/second (>= 0.0f)
				inline void setMinSpeed(float32 minSpeed) 
				{ 
					X_ASSERT_MSG1(minSpeed >= 0.0f, "cannot set speed < 0.0f",minSpeed);
					mMinSpeed = X_MIN(minSpeed,mMaxSpeed);
				}

				//! \brief get the max acceleration of the the entity in worldunits/s^2
				//! \returns the max acceleration of the entity in worldunits/s^2
				inline float32 getMaxAcceleration() const 
				{ 
					return mMaxAcceleration;
				}
				
				//! \brief set the max acceleration of the entity in worldunits/s^2 (default = 0.5f*maxSpeed)
				//! \param val		the max acceleration value in worldunits/s^2 (>= 0.0f)
				inline void setMaxAcceleration(float32 val)
				{ 
					X_ASSERT_MSG1(val >= 0.0f, "cannot set max acceleration < 0.0f",val)

					mMaxAcceleration = val;
				}
				
				//! \brief get the max deceleration of the the entity in worldunits/s^2
				//! \returns the max deceleration of the entity in worldunits/s^2
				inline float32 getMaxDeceleration() const 
				{ 
					return mMaxDeceleration;
				}

				//! \brief set the max deceleration of the entity (default = 0.5f*maxSpeed)
				//! \param val		the max deceleration value in worldunits/s^2 (>= 0.0f)
				inline void setMaxDeceleration(float32 val) 
				{ 
					X_ASSERT_MSG1(val >= 0.0f, "cannot set max deacceleration < 0.0f",val)
					mMaxDeceleration = val;
				}

				//! \brief get the position of the move entity 
				//! \returns the position of the move entity
				virtual const Common::Math::Vec3f& getMovePosition() const
				{
					return mNextMovePosition;
				}

				//! \brief get the direction of the move entity 
				//! \returns the direction of the move entity
				//! \remark the returned direction vector is normalized
				virtual const Common::Math::Vec3f& getMoveDirection() const
				{
					return mLastMoveDirection;
				}
				
				//! \brief set the max time that a entity stands still, while not idling, until path following aborts
				//! \param milliseconds		the max error time
				//! \remark if the time is reached the onPathFollowFailed event will be raised
				inline void setMaxStuckErrorTime(uint32 milliseconds)
				{
					mMaxStuckErrorTime = milliseconds;
				}

				//! \brief get the max time that a entity stands still, while not idling, until path following aborts
				//! \returns the max error time
				inline uint32 getMaxStuckErrorTime() const
				{
					return mMaxStuckErrorTime;
				}

				//! \brief set the max time that a moves away from the next waypoint before the onPathFollowFailed event is raised
				//! \param milliseconds		the max error time
				//! \remark if the time is reached the onPathFollowFailed event will be raised
				inline void setMaxWrongDirectionErrorTime(uint32 milliseconds)
				{
					mMaxWrongDirectionErrorTime = milliseconds;
				}

				//! \brief get the max time that a moves away from the next waypoint before the onPathFollowFailed event is raised
				//! \returns the max error time
				inline uint32 getMaxWrongDirectionErrorTime() const
				{
					return mMaxWrongDirectionErrorTime;
				}

				/*! \brief set the max angle at a waypoint that entity accept to regard the waypoint as a normal waypoint
				 *		   if the angle is smaller the entity will behave at like getting a new target (including onStartMotionEvent)
				*/
				//! \param radians		the max angle at waypoints in radian
				inline void setMaxWaypointAngle(float32 radians)
				{
					mMaxWaypointAngle = radians;
				}

				/*! \brief set the max angle at a waypoint that entity accept to regard the waypoint as a normal waypoint
				 *		   if the angle is smaller the entity will behave at like getting a new target (including onStartMotion event)
				*/
				//! \returns the max angle in radian
				inline float32 getMaxWaypointAngle() const
				{
					return mMaxWaypointAngle;
				}

			protected:
				

			


				//! \brief get the next movement position (if calculated)
				//! \returns the next movement position
				XAIT_MAP_DLLAPI virtual const Common::Math::Vec3f& getNextPosition() const;

				/*! \brief the main callback function. the given move vector should be used to set the new position of the entity.
				 *			if you are using a character controller (e.g. physics character controller), you can use this vector
				 *			to generate the appropriate input to the character move function.
				 *			if your are not using a character controller, compute the new position with newPosition = oldPosition+moveVector
				 *
				 *			if you use a physic engine the entity position will differ in up direction from the move position.
				 *			in that case ignore the up component. in some cases (e.g. physx character controller) you have to
				 *			set the up component in gravity direction to simulate the gravity (see tutorial)
				 */
				//! \param moveVector	the vector that contains a move vector
				virtual void onMove(const Common::Math::Vec3f& moveVector) = 0;
				
				// called if error handling has to fix something
				// this function is called automatically and should not be called in any error handling by yourself
				// if you want to set the position of the current entity use setCurrentEntityPosition
				//! \brief event to set the position of the game entity
				//! \param position		the new position
				//! \remark this method is used to beam the entity
				virtual void onSetPosition(const Common::Math::Vec3f& position) = 0;

				//! \brief event to set the direction of the game entity
				//! \param direction	the new direction
				virtual void onSetDirection(const Common::Math::Vec3f& direction) = 0;
				
				

				//! \brief get the last move position
				//! \returns the last move position
				inline const Common::Math::Vec3f& getLastMovePosition() const
				{
					return mLastMovePosition;
				}

				//! \brief get the current navmeshnode
				inline GlobalNodeID getCurrentNode() const
				{ 
					return mNodeID;
				}

				//! \brief get a path iterator from the start
				XAIT_MAP_DLLAPI PathIterator getPathIterator();

				//! \brief get the current path iterator 
				XAIT_MAP_DLLAPI const PathIterator& getCurrentPathIterator() const;

				//! \brief let the entity starts idling. 
				//! \param removeWaypoints		if true existing waypoints will be remove, if false the entity starts to idle and will search paths to the next wayypoint
				XAIT_MAP_DLLAPI void startIdle(bool removeWaypoints);

				//! \brief starts a new path search and stores the result
				//! \param end		end point of the path
				//! \param currentPath	if true the result is stored in the current path, if false the result is stored in the next path
				//! \returns the error code of the search request. only initializing errors are returned, because the search is progressed in the background
				XAIT_MAP_DLLAPI Path::SearchRequest::ErrorCode startPathSearch(const Common::Math::Vec3f& end, bool currentPath);

				//! \brief removes the next path that is computed in advance
				XAIT_MAP_DLLAPI void removeNextPath();

				// error handling methods
				//! \brief set the move entity back to a position next to the entity position
				XAIT_MAP_DLLAPI void revertMovePosition();
				
				//! \brief beam the entity to the move position
				XAIT_MAP_DLLAPI void beamToMovePosition();
				
				//! \brief beam the entity to a free position on the path. 
				//! \returns true if a free position was found and the entity was beamed, false otherwise
				XAIT_MAP_DLLAPI bool beamToFreePathPosition();
				
				//! \brief generates a temporary back to the original path if possible
				//! \returns true if a fix was found, false otherwise
				XAIT_MAP_DLLAPI bool fixPathFromEntityPos();
				
				//! \brief skips the current waypoint (the endpoint of the current path)
				//! \return true if there is another waypoint, false otherwise
				XAIT_MAP_DLLAPI bool skipCurrentWaypoint();

				//! \brief skips the next waypoint (the waypoint after the current waypoint)
				//! \return true if there is another waypoint, false otherwise
				XAIT_MAP_DLLAPI bool skipNextWaypoint();


				
				//! \brief called if an error occurred while path search
				//! \param failedRequest		the searchrequest that has failed
				//! \param currentPath			if true the computation of the current path failed, otherwise the computation of the cached next path failed
				//! \remark the search request will be deleted after this call
				virtual void onPathSearchFailed(const Path::SearchRequest* failedRequest,bool currentPath)
				{
					if (currentPath)
					{ 
						if (!skipCurrentWaypoint())
							startIdle(true);
					}
					else if (!skipNextWaypoint())
					{
						removeNextPath();
					}
				}

				//! \brief called if path has has chaned due to a recomputation of the path
				//! \param oldPath		the old path will be deleted after the event
				//! \param newPath		the new path
				virtual void onPathChanged(const Map::Path::PathResult* oldPath, Map::Path::PathResult* newPath)
				{
				}

				//! \brief called if a path search is finished
				//! \param foundPath		the found path
				//! \remark	if waypoints are used, the entity tries to compute paths in advance
				virtual void onPathSearchFinished(Path::PathResult* foundPath)
				{
				}

			
				//! \brief called if the target is blocked by an entity
				//! \param ignoreBlocking		if true the aborting of the the pathfollowing is canceled. otherwise the entity stops and idle. (default: false)
				//! \param colliderID			the collider id of the entity that blocks the targets
				virtual void onTargetBlocked(bool& ignoreBlocking, uint32 colliderID)
				{
					ignoreBlocking = skipCurrentWaypoint();
				}

				
				//! \brief called if the entity starts following a path
				//! \param expectedDirection	the expected move direction
				//! \remark if want to access the path use getCurrentPath
				//! \remark the expected direction can change if the entity is in avoidance mode
				virtual void onStartFollowingPath(const Common::Math::Vec3f& expectedDirection)
				{
				}

				//! \brief called if the entity reaches a target or waypoint
				//! \param waypoint		the reached waypoint
				//! \remark if you want to check if the last waypoint is reached use the onStartIdle event
				//! \remark the pathfollower does not cross the waypoint exactly, if the entity is in avoidance mode
				virtual void onTargetReached(const Common::Math::Vec3f& waypoint)
				{
				}

				//! \brief called if the entity failed to follow a path
				//! \param ignoreError		if true the aborting of the the pathfollowing is canceled. otherwise the entity stops and idle. (default: false)
				//! \remark this usually happens if the way is blocked by other collision entities and the entity is not able to avoid these entities
				virtual void onPathFollowFailed(bool& ignoreError)
				{
					ignoreError = beamToFreePathPosition();
				}

				//! \brief called if the surface type changed
				//! \param oldSurfaceTypeID			the old surface type id
				//! \param newSurfaceTypeID			the new surface type id
				virtual void onSurfaceTypeChanged(uint16 oldSurfaceTypeID,uint16 newSurfaceTypeID)
				{}

				//! \brief called if the heightid changed
				//! \param oldHeightID			the old height id
				//! \param newHeightID			the new height id
				virtual void onHeightIDChanged(uint32 oldHeightID,uint32 newHeightID)
				{}

				//! \brief called if the entity starts to idle (either target reached or waiting for the result of a pathsearch)
				virtual void onStartIdle()
				{}

			

				//! \brief called if the move position and the entity position are diverging
				//! \param position		the current move position of the entity
				//! \param distance		the distance between the entity and the move entity
				virtual void onPositionDivergency(const Common::Math::Vec3f& position, float32 distance)
				{
					beamToMovePosition();
				}

				//! \brief called the first time the entity starts to move after the entity stood still
				//! \param direction	the move direction
				//! \remark this event can be used to trigger for a turning animation. 
				//! \remark suspend the entity if want to wait for something (e.g. turning animation) 
				virtual void onStartMotion(const Common::Math::Vec3f& direction)
				{
				}

				//! \brief called if the entity starts to brake before reaching the target
				virtual void onStartBraking()
				{
				}


				private:
					enum State
					{
						STATE_IDLE,
						STATE_FOLLOW_PATH,
						STATE_AVOID_ENTITY,
					};

					// constant data
					PathManager*								mPathManager;
					const Map::NavMeshContainer*				mNavMesh;
					InverseMotionSlice*							mInverseMotionSlice;

					// current state
					Common::Container::List<Common::Math::Vec3f> mWayPoints;

					Frametime									mFrametime;

					State										mEntityState;
				
					Common::Math::Vec3f							mEntityPosition;
					Common::Math::Vec3f							mNextMovePosition;
					Common::Math::Vec3f							mLastMovePosition;
					Common::Math::Vec3f							mLastMoveDirection;

					Map::GlobalNodeID							mNodeID;
					Map::GlobalNodeID							mLastNodeID;
					uint16										mSurfaceTypeID;
					uint32										mHeightID;
					 

					bool										mWaitForNewPath;
					bool										mSuspended;
					float32										mBrakingDistance;
					
					uint32										mDynamicCollisionErrorTime;
					Common::Math::Vec3f							mPathReferenceDirection;
					uint32										mLastPathUpdateTime;
					uint32										mNextReferencePoint;
					float32										mMinSqDistanceToReferencePoint;
					uint32										mMinDistanceTimeStamp;

					bool										mHasNewResult;
					bool										mTriggerStartMotion;
					bool										mIsInBrakingDistance;
					Common::Math::Vec3f							mErrorReferencePoint;
					

					
					// config variables
					bool		mCollisionEnabled;
					float32		mCollisionRadius;
					float32		mSoftRadiusFactor;

					float32		mMaxSpeed;
					float32		mMinSpeed;
					float32		mMaxAcceleration;
					float32		mMaxDeceleration;

					uint32		mMaxStuckErrorTime;
					uint32		mMaxWrongDirectionErrorTime;
					float32		mMaxWaypointAngle;


					XAIT_MAP_DLLAPI virtual MoveEntity::UpdateState update(const Frametime& time, bool newFrame);

					XAIT_MAP_DLLAPI virtual CollisionShape* getCollisionShape() const;


					XAIT_MAP_DLLAPI virtual void finishFrame();

					XAIT_MAP_DLLAPI virtual void initFrame();

					bool moveOnPath();

					void switchPath();

					void checkCollisions();

					void reevaluteCurrentPolygon();

					bool computeNextPositionOnPath( Common::Math::Vec3f& newPosition,float32& crossedDistance, GlobalNodeID& newNodeID, float32 newSpeed );
					float32 computeBrakingDistance(float32 targetSpeed) const;
					float32 computeNewSpeed(float32 brakingDistance);
					bool isInBrakingDistance() const;

					void avoidEntities();

					void pathFollowErrorHandling();

					Common::Math::Vec3f findFreeDirection(const Common::Math::Vec3f& wantedDirection,float32& distance);

					void onPathUpdate(const Map::Path::PathResult* oldPath, Map::Path::PathResult* newPath);
					void onPathSearchRemoved(bool currentPath);
					void onPathRecomputationFailed(const Path::PathResult* oldPath, bool current);

					float32 getFollowDistance() const;

					void dynamicCollisionErrorHandling();

					bool getSlicesInCollisionArea(uint32& minID, uint32& maxID, const Common::Math::Vec2f& pointOnTangent, float32 radius, const Common::Math::Vec2f& circleMidpoint);

					bool beamToFreePathPosition( const Path::PathResult::Iterator& destinationSegment, const Common::Math::Vec3f& position);

					void validateState(bool forbidIdle) const;

					PathIterator findPathSegment(const Common::Math::Vec3f& position);

					bool isEdgeValid(GlobalNodeID nodeID, uint32 edgeID);

					void startFollowingPath();

					Common::Math::Vec3f intersectSegmentCollisionRadius( PathIterator &pathIter);
					Common::Math::Vec3f getPathDirection() const;

 			};
		}
	}
} 
