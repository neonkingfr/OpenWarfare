// (C) xaitment GmbH 2006-2012

#pragma once 


#ifdef XAIT_LIBRARY_DYNAMIC

#ifdef XAIT_MAP_DLL_EXPORTS
	#define XAIT_MAP_DLLAPI	XAIT_DLLEXPORT
#else
	#define XAIT_MAP_DLLAPI	XAIT_DLLIMPORT
#endif

#else

#define XAIT_MAP_DLLAPI

#endif



#define NAVMESH_DEFAULT_EPSILON		0.001f
