// (C) xaitment GmbH 2006-2012

#pragma once 

#include <xait/map/SolidPathObject.h>
#include <xait/common/math/Vec3.h>
#include <xait/common/math/Matrix4.h>


namespace XAIT
{
	namespace Map
	{
		class BlockingBoxSectorData;

		//! Blocking object with a box as representation.
		//! The local object is a unit box with Y-Axis Up.
		//! Cut planes are the planes around the Y-Axis.
		//! State:
		//! Enabled = Blocked
		//! Disabled = Unblocked
		//! \brief A blocking path object with box representation that can switch its blocking state during runtime
		class BlockingBoxPathObject : public SolidPathObject
		{
		public:
			//! \brief Construct a blocking box path object
			//! \param name		Unique name 
			//! \remark The name must be unique within the navmesh its intend to be used
			XAIT_MAP_DLLAPI BlockingBoxPathObject(const Common::String& name);

			XAIT_MAP_DLLAPI virtual Common::Geom::AABB3Df getBoundingBox() const;

			//! Get the corner vertex of the blocking object with transformation applied. The indices reference to the
			//! vertices as follow
			//!
			//!          7-------6
			//!         /|      /|
			//!        4-+-----5 |
			//!        | 3-----|-2
			//!        |/      |/
			//!        0-------1
			//! 
			//! \brief Get corner vertex
			//! \param cornerID		id of the corner in the range of 0-7
			XAIT_MAP_DLLAPI Common::Math::Vec3f getCorner(const uint32 cornerID) const;

			//! \brief Get the corners of the blocking object without transformation
			XAIT_MAP_DLLAPI const Common::Math::Vec3f& getLocalCorner(const uint32 cornerID) const;

			//! \brief Gets a set of indices that form a cube
			//! \returns A set of 6 * 2 triangles for all faces of the cube. Each triangle consists of 3 indices.
			//!          So the number of indices is 36.
			//! \remark	This is intend for debug purposes
			//! \remarks Indices are CCW.
			XAIT_MAP_DLLAPI const uint32* getIndices() const;

			//! \brief Get Number of indices
			inline uint32 getNumIndices() const { return 36; }

			virtual PathObject* clone() const { return new BlockingBoxPathObject(*this); }

			XAIT_MAP_DLLAPI virtual void refreshSectorData(PathObjectSectorData* data) const;
			XAIT_MAP_DLLAPI virtual void detachSectorData(PathObjectSectorData* data) const;
			XAIT_MAP_DLLAPI	virtual void appendSectorData(PathObjectSectorData* data);
			XAIT_MAP_DLLAPI	virtual void removeSectorData(PathObjectSectorData* data);
			XAIT_MAP_DLLAPI virtual uint32 getMemoryUsage() const;
		private:
			StaticContainer<BlockingBoxSectorData*>::Vector		mSectorDatas;

			void applyConnections(const bool enable) const;
			void applyConnections(PathObjectSectorData* data, const bool enable) const;

			XAIT_MAP_DLLAPI virtual void onEnableChanged();


		};
	}
}