// (C) xaitment GmbH 2006-2012

#pragma once 

#include <xait/map/PathObject.h>
#include <xait/common/geom/AABB3D.h>

namespace XAIT
{
	namespace Map
	{
		//! \brief Base class for solid path object
		//! \remark Solid path objects are object which have a geometry attached and are not build only
		class SolidPathObject : public PathObject
		{
		public:
			XAIT_MAP_DLLAPI SolidPathObject(const PathObjectType typeID, const Common::String& name);

			//! \brief Gets the enabled state of this path object
			inline bool getEnable() const { return mEnable; }

			//! \brief Sets the enabled state of this path object
			//! \remark This can change the navigation mesh owner and there is no check for thread safety on that.
			inline void setEnable(const bool enable) 
			{ 
				if (mEnable != enable)
				{
					mEnable= enable; 
					onEnableChanged();
				}
			}

			//! \brief Gets the transformation of the path object as right hand coordinate system
			inline const Common::Math::Matrix4f& getTransform() const { return mTransform; }

			//! \brief Gets the transformation of the path object as right hand coordinate system
			inline Common::Math::Matrix4f& getTransform() { return mTransform; }

			//! \brief Gets the world bounding box of the path object
			virtual Common::Geom::AABB3Df getBoundingBox() const= 0;

		protected:
			virtual void onEnableChanged() {};

			XAIT_MAP_DLLAPI virtual bool initFromStream(Common::Serialize::SerializeStorage* serializer);
			XAIT_MAP_DLLAPI virtual void writeToStream(Common::Serialize::SerializeStorage* serializer) const;

		private:
			Common::Math::Matrix4f	mTransform;
			bool					mEnable;
		};
	}
}


