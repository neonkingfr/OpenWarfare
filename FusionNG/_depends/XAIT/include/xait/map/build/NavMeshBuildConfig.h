// (C) xaitment GmbH 2006-2012

#pragma once 

#include <xait/map/CompileDefines.h>
#include <xait/map/CharacterInfo.h>
#include <xait/map/SurfaceType.h>
#include <xait/common/math/Vec3.h>
#include <xait/common/container/Vector.h>
#include <xait/common/crypto/MD5Sum.h>
#include <xait/common/memory/MemoryManaged.h>
#include <xait/map/CustomDataDesc.h>


#define X_NAVMESHBLDCFG_DEFAULT_CUBESIZE			0.25f
#define X_NAVMESHBLDCFG_MAXEDGE_CUBESIZE_REL		40.0f
#define X_NAVMESHBLDCFG_SECTORSIZE_CUBESIZE_REL		256.0f
#define X_NAVMESHBLDCFG_STEPSIZE_CUBESIZE_REL		2.0f


namespace XAIT
{
	namespace Map
	{
		namespace Build
		{
			class HeightClassifier;

			//! \brief All static build configurations for a navigation mesh build.
			class NavMeshBuildConfig : public Common::Memory::MemoryManaged
			{
			public:
				//! \brief Default constructor. Creates an empty build configuration.
				NavMeshBuildConfig();

				//! \brief Copy constructor.
				NavMeshBuildConfig(const NavMeshBuildConfig& src);

				//! \brief Destructor.
				~NavMeshBuildConfig();

				//! SectorCubeSizeRelation defines which value of CubeSize or SectorSize is dominant since both values depend on each other
				//! \brief Relation between CubeSize and SectorSize
				enum SectorCubeSizeRelation
				{
					REL_FIXED_CUBESIZE			= 0,	//!< CubeSize will stay fixed, SectorSize will be adopted to nearest possible value
					REL_FIXED_SECTORSIZE		= 1,	//!< SectorSize will stay fixed, CubeSize will be adopted to nearest possible value
					REL_FIXED_TO_LAST_CHANGED	= 2,	//!< the value which has changed last will stay constant
				};

				//! \brief Adds a new character to the configuration.
				//!
				//! Note that the name of the character must be unique.
				//! \param charInfo		the character description
				XAIT_MAP_DLLAPI void addCharacter(const CharacterInfo& charInfo);

				//! \brief Removes all character descriptions from this configuration.
				XAIT_MAP_DLLAPI void clearCharacters();

				//! \brief Adds a new surface type.
				//!
				//! The surface type name and the surface type id must be unique.
				//! \param surfaceType	the surface type description
				//! \see clearSurfaceTypes
				XAIT_MAP_DLLAPI void addSurfaceType(const SurfaceType& surfaceType);

				//! \brief Search a surface type by its name.
				//! \param surfaceTypeName	the name of the surface type to search for
				//! \returns The surface type, if it was found, or NULL if it was not found
				XAIT_MAP_DLLAPI const SurfaceType* findSurfaceType(const Common::String& surfaceTypeName) const;

				//! \brief Search a surface type by its ID.
				//! \param surfaceTypeID	the ID of the surface type to search for
				//! \returns The surface type, if it was found, or NULL if it was not found
				XAIT_MAP_DLLAPI const SurfaceType* findSurfaceType(const uint16 surfaceTypeID) const;

				//! \brief Removes all surface types from this configuration.
				//! \see addSurfaceType
				XAIT_MAP_DLLAPI void clearSurfaceTypes();

				//! \brief Gets all defined characters.
				//! \return a vector containing all CharacterInfos
				inline const Common::Container::Vector<CharacterInfo>& getCharacters() const
				{
					return mCharacters;
				}

				//! \brief Gets all defined surface types.
				//! \return a vector containing all surface types
				inline const Common::Container::Vector<SurfaceType>& getSurfaceTypes() const
				{
					return mSurfaceTypes;
				}

				//! \brief Gets the normalized up direction.
				//! \return the up direction
				inline const Common::Math::Vec3f& getUpDirection() const
				{
					return mUpDirection;
				}

				//! \brief Sets the up direction.
				//!
				//! The up direction must not be normalized but should not be a zero length vector.
				//! \param upDirection	up direction vector
				inline void setUpDirection(const Common::Math::Vec3f& upDirection)
				{
					X_ASSERT_MSG1(upDirection.getLength() > X_EPSILON_F,"up direction must not be zero length",upDirection.getLength());
					mUpDirection= upDirection;
					mUpDirection.normalize();
				}

				//! \brief Gets the cube size.
				//! \return the cube size
				inline float32 getCubeSize() const
				{
					return mCubeSize;
				}

				//! \brief Gets the absolute minimum cube size
				inline float32 getMinCubeSize() const
				{
					return NAVMESH_DEFAULT_EPSILON;
				}

				//! \brief Sets the cube size.
				//!
				//! This might also change the sector size depending on the sector cube size relation.
				//! \param cubeSize		the cube size in units of your world
				XAIT_MAP_DLLAPI void setCubeSize(const float cubeSize);

				//! \brief Gets the sector size.
				//! \param replaceDefault	if set to true returns values smaller zero with the computed default value
				//! \return the sector size
				inline float32 getSectorSize(const bool replaceDefault= false) const
				{
					if (mSectorSize < 0 && replaceDefault)
					{
						return mCubeSize * X_NAVMESHBLDCFG_SECTORSIZE_CUBESIZE_REL;
					}
					return mSectorSize;
				}

				//! \brief Sets the sector size.
				//!
				//! Note that sectors are squares. This might also change the cube size depending on the sector cube size relation.
				//! \param sectorSize	the edge length of a sector in world units
				XAIT_MAP_DLLAPI void setSectorSize(const float32 sectorSize);

				//! \brief Gets the relation between the sector size and the cube size.
				//! \return the SectorCubeSizeRelation
				inline SectorCubeSizeRelation getSectorCubeSizeRelation() const
				{
					return mSectorCubeSizeRelation;
				}

				//! \brief Sets the relation between sector size and cube size.
				//! \param relation the new SectorCubeSizeRelation
				inline void setSectorCubeSizeRelation(const SectorCubeSizeRelation relation)
				{
					mSectorCubeSizeRelation= relation;
				}

				//! \brief Sets the unit radius.
				//!
				//! The unit radius is equal to a wall keeping distance. The navmesh will be moved away from blocking
				//! areas.
				//! \param unitRadius	unified unit radius for the navmesh
				inline void setUnitRadius(const float32 unitRadius)
				{
					mUnitRadius= unitRadius;
				}

				//! \brief Gets the unit radius (\see NavMeshBuildConfig::setUnitRadius)
				//! \param replaceDefault	if set to true returns values smaller zero with the computed default value. See remarks for more information.
				//! \remarks The default value for the unit radius will be computed as maximum of all character radius from all added characters. If no
				//!			 characters are added the unit radius will be 0.
				XAIT_MAP_DLLAPI float32 getUnitRadius(const bool replaceDefault= false) const;

				//! \brief Sets the height one character can go up and down without jumps
				inline void setStepHeight(const float32 stepHeight)
				{
					mStepHeight= stepHeight;
				}

				//! \brief Gets the step height (\see NavMeshBuildConfig::setStepHeight)
				//! \param replaceDefault	if set to true returns values smaller zero with the computed default value
				inline float32 getStepHeight(const bool replaceDefault= false) const
				{
					if (mStepHeight < 0 && replaceDefault)
						return mCubeSize * X_NAVMESHBLDCFG_STEPSIZE_CUBESIZE_REL;

					return mStepHeight;
				}

				//! \brief Enables or disables the usage of the polygon edge reduction.
				//!
				//! This algorithm will try to reduce the amount of polygon edges. It will smooth the shapes of a polygon, thus drastically reducing the amount
				//! of memory used for the final polygon.
				//! \param enable set it to true, if you want to use the polygon edge reduction, and to false otherwise
				inline void setUsePolygonEdgeReduction(const bool enable)
				{
					mUsePolygonEdgeReduction= enable;
				}

				//! \brief Gets the state of the polygon edge reduction algorithm.
				//! \return true, if polygon edge reduction is used, and false otherwise
				inline bool getUsePolygonEdgeReduction() const
				{
					return mUsePolygonEdgeReduction;
				}

				//! Enables or disables the storage of build only path objects inside the generated navmesh container.
				//! Certain path objects are only relevant for the build process and have no influence during runtime of
				//! the navigation mesh usage. These are for instance spawn points or blocked areas. Nevertheless, it is at
				//! some point useful to store these items inside the navmesh container. This can be if you want to update
				//! the navigation mesh during runtime and need the same informations as for generating the initial navmesh
				//! mesh. Or you just want to query the position of spawn points. 
				//! \brief Enables or disables the storage of build only path objects inside the generated navmesh container
				//! \remark Default is turned off to save memory consumption.
				inline void setStoreBuildOnlyPathObjects(const bool enable)
				{
					mStoreBuildOnlyPathObjects= enable;
				}

				//! \brief Gets the build state for storing build only path objects.
				//! \remark See \see NavMeshBuildConfig::setStoreBuildOnlyPathObjects for more details.
				inline bool getStoreBuildOnlyPathObjects() const { return mStoreBuildOnlyPathObjects; }

				//! \brief Sets the maximum edge length for new created polygons
				//!
				//! The navmesh consists of a polygon mesh. This parameter defines how long
				//! those edges are. Longer edges result in less polygons but can effect the
				//! quality of the path search. 
				//! \param maxLength	maximum edge length (values smaller zero use default)
				inline void setMaxPolygonEdgeLength(const float32 maxLength)
				{
					mMaxPolyEdgeLength= maxLength;
				}

				//! \brief Gets the maximum edge length for new created polygons
				//! \param replaceDefault	if set to true returns values smaller zero with the computed default value
				inline float32 getMaxPolygonEdgeLength(const bool replaceDefault= false) const
				{
					if ((mMaxPolyEdgeLength < 0) && replaceDefault)
					{
						return mCubeSize * X_NAVMESHBLDCFG_MAXEDGE_CUBESIZE_REL;
					}
					return mMaxPolyEdgeLength;
				}

				//! \brief Gets the custom data definition that is used to appended custom data space to all navmesh nodes
				//! \returns The complete definition modifiable 
				inline CustomDataDesc& getCustomDataDesc() { return mCustomDataDesc; }

				//! \brief Gets the custom data definition that is used to appended custom data space to all navmesh nodes
				//! \returns The complete definition as const
				inline const CustomDataDesc& getCustomDataDesc() const { return mCustomDataDesc; }

				
				//////////////////////////////////////////////////////////////////////////
				// Internal functions

				//! \brief Internal function. Do not use it.
				bool finalize();

				//! \brief Internal function. Do not use it.
				//!
				//! Builds the MD5 checksum of this configuration.
				//! \return the MD5 checksum
				inline const Common::Crypto::MD5Sum& getCheckSum() const
				{
					return mCheckSum;
				}

				//! \brief Internal function. Do not use it.
				//! \return a pointer to the HeightClassifier
				inline const HeightClassifier* getHeightClassifier() const
				{
					return mHeightClassifier;
				}

				//! \brief Internal function. Do not use it.
				//! \return the ID of the main axis
				inline uint32 getMainAxisID() const
				{
					return mMainAxis;
				}

				//! \brief Internal function. Do not use it.
				//! \return the main axis
				const Common::Math::Vec3f& getMainAxis() const;

				//! \brief Internal function. Do not use it.
				//! \return the up component
				inline uint32 getUpComp() const
				{
					return mUpComp;
				}

				//! \brief Internal function. Do not use it.
				//! \return the x axis
				inline const Common::Math::Vec3f& getXAxis() const
				{
					return mXAxis;
				}

				//! \brief Internal function. Do not use it.
				//! \return the y axis
				inline const Common::Math::Vec3f& getYAxis() const
				{
					return mYAxis;
				}

				//! \brief Internal Function. Do not use it
				//! \returns a value mapped into cube space
				inline uint32 createCubedValue( const float32 value ) const
				{
					if (value > 0)
						return (uint32)(value / getCubeSize() + (1.0f - X_EPSILON_F));

					return 0;
				}

				//! \brief Internal Function. Do nut use it
				//! \returns a rounded up to the next cube multiple, incase of zero, its zero
				inline float32 ceilToCube(const float32 value) const
				{
					return (float32)createCubedValue(value) * getCubeSize();
				}


			private:
				friend class NavMeshBuild;
				friend class DebugInfo;

				Common::Container::Vector<CharacterInfo>	mCharacters;	
				Common::Container::Vector<SurfaceType>		mSurfaceTypes;

				Common::Math::Vec3f							mUpDirection;			//!< major up direction of the world (normally negative gravity direction)
				float32										mCubeSize;				//!< detail size (smaller value, higher detail -> more memory consumption)
				float32										mSectorSize;			//!< the size of the sectors
				SectorCubeSizeRelation						mSectorCubeSizeRelation;	//!< the relation between the cube size and the sector size
				float32										mUnitRadius;
				float32										mStepHeight;
				bool										mUsePolygonEdgeReduction;	//!< flag that indicates whether the polygon edge reduction should be used or not
				float32										mMaxPolyEdgeLength;
				bool										mStoreBuildOnlyPathObjects;
				CustomDataDesc								mCustomDataDesc;

				Common::Crypto::MD5Sum						mCheckSum;				//!< the checksum of this object
				HeightClassifier*							mHeightClassifier;		//!< the height classifier
				uint32										mMainAxis;				//!< the index of the main axis
				uint32										mUpComp;				//!< the component indicating the up direction
				Common::Math::Vec3f							mXAxis;					//!< the x axis
				Common::Math::Vec3f							mYAxis;					//!< the y axis


				//! \brief Calculates the checksum of this object.
				void initCheckSum();

				//! \brief Initializes the height classifier.
				bool initHeightClassifier();

				//! \brief Calculates the main surface axis.
				void initMainAxis();

				//! \brief Calculates the up component.
				void initUpComp();

				bool validateStepHeight();

				//! \brief Computes the nearest cube size to the current cube size and the sector size, given that the sector size is constant.
				float32 computeNearestCubeSize(const float32 cubeSize, const float32 sectorSize);

				//! \brief Computes the nearest sector size to the current cube size and the sector size, given that the cube size is constant.
				float32 computeNearestSectorSize(const float32 cubeSize, const float32 sectorSize);
			};
		}
	}
}

