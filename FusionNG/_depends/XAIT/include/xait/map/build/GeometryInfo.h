// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/FundamentalTypes.h>
#include <xait/common/math/Matrix4.h>
#include <xait/common/math/Vec3.h>
#include <xait/common/container/Vector.h>
#include <xait/common/geom/IndexedTriangle.h>
#include <xait/common/geom/AABB3D.h>

namespace XAIT
{
	namespace Map
	{
		namespace Build
		{
		struct TriangleClassifier;

		//! \brief The GeometryInfo class defines triangles and navigation properties for a part of your geometry.
		//!
		//! In addition to defining the physical appearance, triangles, vertices and transformation matrix,
		//! you can specify a triangle classifier that informs the navigation build algorithm if a certain surface
		//! triangle should contribute to the walkable triangles and the type of the surface. The navigation mesh
		//! will be divided with taking these informations into account. 
		struct GeometryInfo
		{
			enum IndexSize
			{
				INDEX_SIZE_16= 2,
				INDEX_SIZE_32= 4,
			};

			TriangleClassifier*		mTriangleClassifier;	//!< triangle classifier which specifies the properties of each individual triangle in this geometry info

			//////////////////////////////////////////////////////////////////////////////////
			// Geometry description

			//! \brief A pointer to the array containing all vertices of your geometry.
			//! 
			//! The data will only be used read only. The size of each vertex in the array
			//! will be defined by GeometryInfo::mVertexSize in bytes. However, it will be expected
			//! that the position data, which will be used, is available in three consecutive float
			//! values at the start of each vertex.
			const uint08*			mVertices;			

			//! \brief A pointer to the array containing the indices for your triangles.
			//!
			//! The data will only be used read only. The size of each triangle in the array
			//! will be defined by GeometryInfo::mTriangleSize in bytes. However, it will be expected that each triangle
			//! consists of three consecutive integer indices. The size of an individual index is specified
			//! by GeometryInfo::mIndexSize. There is the possibility of 16 and 32 bit indices.
			const uint08*			mIndices;

			uint32					mVertexSize;			//!< size of each individual vertex in mVertices including all data, not only the position
			uint32					mTriangleSize;			//!< size of each individual triangle in mIndices including all data, not only the three indices for the triangle
			IndexSize				mIndexSize;				//!< size of an index in mIndices, can be 16 or 32 bit

			uint32					mNumVertices;			//!< number of vertices available in mVertices
			uint32					mNumTriangles;			//!< number of triangles available in mIndices

			Common::Math::Matrix4f	mTransform;				//!< transformation matrix which should be applied to all positions in mVertices. This can be used for geometry instancing.
			bool					mUseTransform;			//!< specifies if the transformation matrix should be used. If false the transformation won't be used, which speeds up navmesh computation.

			Common::Geom::AABB3Df	mBoundingBox;			//!< WORLD bounding box. used to determine size of this geometry. Settings this, speeds up the computation of the navmesh since the bounding box must not be computed


			//! \brief Default constructor
			GeometryInfo()
				: mTriangleClassifier(NULL)
				, mVertices(NULL)
				, mIndices(NULL)
				, mVertexSize(12)
				, mTriangleSize(12)
				, mIndexSize(INDEX_SIZE_32)
				, mNumVertices(0)
				, mNumTriangles(0)
				, mTransform(Common::Math::Matrix4f::IDENTITY)
				, mUseTransform(false)
			{
				mBoundingBox.init();
			}

			//! \brief Initializes a GeometryInfo with static arrays of vertices and triangles.
			//! \param vertices		pointer to the array of vertices
			//! \param numVertices	number of vertices in the vertex array
			//! \param triangles	pointer to the array of triangles
			//! \param numTriangles	number of triangles in the triangle array
			GeometryInfo(const Common::Math::Vec3f* vertices,  const uint32 numVertices, const Common::Geom::IndexedTriangle* triangles, const uint32 numTriangles)
				: mVertices((uint08*)vertices)
				, mIndices((uint08*)triangles)
				, mVertexSize(sizeof(Common::Math::Vec3f))
				, mTriangleSize(sizeof(Common::Geom::IndexedTriangle))
				, mIndexSize(GeometryInfo::INDEX_SIZE_32)
				, mNumVertices(numVertices)
				, mNumTriangles(numTriangles)
				, mTransform(Common::Math::Matrix4f::IDENTITY)
				, mUseTransform(false)
			{
				mBoundingBox.init();
			}

			//! \brief Initializes a GeometryInfo from dynamic containers.
			//!
			//! The vertices and triangles array must stay constant until the navigation mesh is built. Do not perform pushBack or remove operations on the vertices or triangles containers until the NavMesh is built.
			//! \param vertices		array with vertices
			//! \param triangles	array with triangles
			GeometryInfo(const Common::Container::Vector<Common::Math::Vec3f>& vertices, const Common::Container::Vector<Common::Geom::IndexedTriangle>& triangles)
				: mTriangleClassifier(NULL)
				, mVertices(NULL)
				, mIndices(NULL)
				, mVertexSize(12)
				, mTriangleSize(12)
				, mIndexSize(INDEX_SIZE_32)
				, mNumVertices(0)
				, mNumTriangles(0)
				, mTransform(Common::Math::Matrix4f::IDENTITY)
				, mUseTransform(false)
			{
				mBoundingBox.init();

				if (triangles.size() > 0)
				{
					mVertices= (uint08*)&vertices[0];
					mNumVertices= vertices.size();
					mVertexSize= sizeof(Common::Math::Vec3f);

					mIndices= (uint08*)&triangles[0];
					mNumTriangles= triangles.size();
					mTriangleSize= sizeof(Common::Geom::IndexedTriangle);
					mIndexSize= INDEX_SIZE_32;
				}
			}

			//! \brief Gets the untransformed vertex for a certain vertex ID.
			//! \param vertexID the vertex ID
			//! \return the corresponding vertex
			//! \see getTransformedVertex
			inline const Common::Math::Vec3f& getVertex(uint32 vertexID) const
			{
				X_ASSERT_MSG_DBG2(vertexID < mNumVertices,"Vertex id out of range",vertexID,mNumVertices);
				return *(reinterpret_cast<const Common::Math::Vec3f*>(mVertices + mVertexSize * vertexID));
			}

			//! \brief Gets the transformed vertex for a certain vertex ID.
			//! \param vertexID the vertex ID
			//! \return the vertex transformed by the transformation matrix
			//! \see getVertex
			inline Common::Math::Vec3f getTransformedVertex(uint32 vertexID) const
			{
				return mTransform.transformPoint(getVertex(vertexID));
			}

			//! \brief Gets a pointer to the 32 bit index of a particular triangle ID.
			//! \param triangleID the triangle ID
			//! \return the pointer to the index
			//! \see getIndexOffset16
			inline const uint32* getIndexOffset32(uint32 triangleID) const
			{
				X_ASSERT_MSG_DBG2(triangleID < mNumTriangles,"Triangle id out of range!",triangleID,mNumTriangles);
				return reinterpret_cast<const uint32*>(mIndices + mTriangleSize * triangleID);
			}

			//! \brief Gets a pointer to the 16 bit index of a particular triangle ID.
			//! \param triangleID the triangle ID
			//! \return the pointer to the index
			//! \see getIndexOffset32
			inline const uint16* getIndexOffset16(uint32 triangleID) const
			{
				X_ASSERT_MSG_DBG2(triangleID < mNumTriangles,"Triangle id out of range!",triangleID,mNumTriangles);
				return reinterpret_cast<const uint16*>(mIndices + mTriangleSize * triangleID);
			}

			//! \brief Gets the vertices of a triangle with the specified triangleID
			//!
			//! If you specified a transformation matrix, then the transformed vertices will be written into the parameters.
			//! \param a the first vertex of the triangle is written to this attribute
			//! \param b the second vertex of the triangle is written to this attribute
			//! \param c the third vertex of the triangle is written to this attribute
			//! \param triangleID the ID of the triangle of which you want the vertices
			inline void getTriangleVertices(Common::Math::Vec3f& a, Common::Math::Vec3f& b, Common::Math::Vec3f& c, uint32 triangleID) const
			{
				if (mUseTransform)
				{
					if (mIndexSize == INDEX_SIZE_16)
					{
						const uint16* indexOffset= getIndexOffset16(triangleID);
						a= getTransformedVertex(*(indexOffset++));
						b= getTransformedVertex(*(indexOffset++));
						c= getTransformedVertex(*(indexOffset));
					}
					else
					{
						const uint32* indexOffset= getIndexOffset32(triangleID);
						a= getTransformedVertex(*(indexOffset++));
						b= getTransformedVertex(*(indexOffset++));
						c= getTransformedVertex(*(indexOffset));
					}
				}
				else
				{
					if (mIndexSize == INDEX_SIZE_16)
					{
						const uint16* indexOffset= getIndexOffset16(triangleID);
						a= getVertex(*(indexOffset++));
						b= getVertex(*(indexOffset++));
						c= getVertex(*(indexOffset));
					}
					else
					{
						const uint32* indexOffset= getIndexOffset32(triangleID);
						a= getVertex(*(indexOffset++));
						b= getVertex(*(indexOffset++));
						c= getVertex(*(indexOffset));
					}
				}
			}

			//! \brief Initializes the bounding box.
			void initBoundingBox()
			{
				mBoundingBox.init();

				if (mUseTransform)
				{
					for(uint32 i= 0; i < mNumVertices; ++i)
						mBoundingBox.extend(getTransformedVertex(i));
				}
				else
				{
					for(uint32 i= 0; i < mNumVertices; ++i)
						mBoundingBox.extend(getVertex(i));
				}
			}

		};

		}
	}
}
