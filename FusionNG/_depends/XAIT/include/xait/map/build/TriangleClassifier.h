// (C) xaitment GmbH 2006-2012

#pragma once 

#include <xait/common/geom/Triangle3D.h>
#include <xait/map/build/GeometryInfo.h>

namespace XAIT
{
	namespace Map
	{
		namespace Build
		{
			//! \brief Triangle information for a single triangle.
			struct TriangleInfo
			{
				bool	mWalkable;			//!< indicates whether the triangle is walkable or not
				uint16	mSurfaceTypeID;		//!< indicates the surface type of this triangle

				//! \brief Constructor.
				//! \param walkable flag that indicates whether or not the triangle is walkable
				//! \param surfaceTypeID the ID of the surface type of the triangle
				TriangleInfo(const bool walkable, const uint16 surfaceTypeID)
					: mWalkable(walkable)
					, mSurfaceTypeID(surfaceTypeID)
				{}

				//! \brief Default constructor.
				TriangleInfo()
					: mWalkable(false)
					, mSurfaceTypeID(0)
				{}
			};

			//! \brief Base class for all triangle classifiers.
			struct TriangleClassifier
			{
				//! \brief Destructor.
				virtual ~TriangleClassifier() {};

				//! \brief Gets the triangle information for a single triangle on a certain geometry.
				//! \param triangleInfo		[out] triangle information for the requested triangle
				//! \param geomInfo			GeometryInfo class containing the triangle for which we request information
				//! \param triangleID		id of the triangle in the GeometryInfo
				virtual void getTriangleInfo(TriangleInfo& triangleInfo, const GeometryInfo& geomInfo, const uint32 triangleID) const= 0;
			};

			//! \brief A triangle classifier that returns the same triangle information for every requested triangle.
			struct StaticTriangleClassifier : public TriangleClassifier
			{
				TriangleInfo	mTriangleInfo; //!< the triangle information

				//! \brief Gets the triangle information.
				//! \param triangleInfo		[out] triangle information for the requested triangle
				//! \param geomInfo			GeometryInfo class containing the triangle for which we request information
				//! \param triangleID		id of the triangle in the GeometryInfo
				virtual void getTriangleInfo(TriangleInfo& triangleInfo, const GeometryInfo& geomInfo, const uint32 triangleID) const
				{
					triangleInfo= mTriangleInfo;
				}

				//! Constructor.
				//! \param triangleInfo the triangleInfo for this classifier
				StaticTriangleClassifier(const TriangleInfo& triangleInfo)
					: mTriangleInfo(triangleInfo)
				{}

				//! Constructor.
				//! \param walkable flag that indicates whether or not the triangle is walkable
				//! \param surfaceTypeID the ID of the surface type of the triangle
				StaticTriangleClassifier(const bool walkable, const uint16 surfaceTypeID)
				{
					mTriangleInfo= TriangleInfo(walkable,surfaceTypeID);
				}
			};

			//! \brief Triangle classifier used for blocking geometry.
			struct BlockedTriangleClassifier : public StaticTriangleClassifier
			{
				//! Default constructor.
				BlockedTriangleClassifier()
					: StaticTriangleClassifier(false,X_MAX_UINT16)
				{}
			};

			//! \brief Triangle classifier that specifies triangles as walkable if the angle between their slope and the gravity direction is below a certain threshold.
			struct SlopeTriangleClassifier : public TriangleClassifier
			{
				float32					mCosMaxSlope;
				Common::Math::Vec3f		mNegGravityDir;
				uint16					mSurfaceTypeID;


				//! \brief Constructor.
				//! \param maxSlope			the maximum slope a triangle can have (value is in RADIANS, i.e. in [0; 2*PI])
				//! \param gravityDir		the gravity direction which should be used for slope computation
				//! \param surfaceTypeID	surface type id which should be used for every triangle
				SlopeTriangleClassifier(const float32 maxSlope, const Common::Math::Vec3f& gravityDir, const uint16 surfaceTypeID)
				{
					mNegGravityDir= gravityDir * -1.0f;
					mNegGravityDir.normalize();
					mCosMaxSlope= Common::Math::Cos(maxSlope); 
					mSurfaceTypeID= surfaceTypeID;
				}

				//! \brief Gets the triangle information.
				//! \param triangleInfo		[out] triangle information for the requested triangle
				//! \param geomInfo			GeometryInfo class containing the triangle for which we request information
				//! \param triangleID		id of the triangle in the GeometryInfo
				virtual void getTriangleInfo(TriangleInfo& triangleInfo, const GeometryInfo& geomInfo, const uint32 triangleID) const
				{
					triangleInfo.mSurfaceTypeID= mSurfaceTypeID;

					Common::Math::Vec3f a,b,c;
					geomInfo.getTriangleVertices(a,b,c,triangleID);

					Common::Math::Vec3f normal;
					if (!Common::Geom::Triangle3D<float>::getStableNormal(normal,a,b,c))
					{
						triangleInfo.mWalkable= false;
						return;	// skip degenerated triangles
					}

					// compute the angle (slope) between the normal of the triangle and the negative gravity direction
					const float32 cosSlope= mNegGravityDir.dotProduct(normal);

					triangleInfo.mWalkable= cosSlope >= mCosMaxSlope;
				}
			};

		}
	}
}
