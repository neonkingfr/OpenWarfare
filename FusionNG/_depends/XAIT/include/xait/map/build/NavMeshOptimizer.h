// (C) xaitment GmbH 2006-2012

#pragma once 
#include <xait/map/NavMeshContainer.h>
#include <xait/common/container/Vector.h>
#include <xait/common/memory/MemoryManaged.h>
#include <xait/common/SharedPtr.h>
#include <xait/map/CompileDefines.h>

namespace XAIT
{
	namespace Map
	{
		namespace Build
		{
			//! \brief Class containing methods to optimize a certain NavMesh.
			class NavMeshOptimizer : public Common::Memory::MemoryManaged, public Common::ReferenceCounted
			{
			public:
				typedef Common::SharedPtr<NavMeshOptimizer, Common::MemberReferenceCount< > >	Ptr;

				//! \brief Default constructor. Use XAIT::Map::Interface::createNavMeshOptimizer instead.
				XAIT_MAP_DLLAPI NavMeshOptimizer();

				//! \brief Creates an empty node id array.
				inline Common::Container::Vector<GlobalNodeID> createNodeIDArray() const
				{
					return Common::Container::Vector<GlobalNodeID>(mAllocator);
				}

				//! \brief Removes all nodes from the NavMesh that cannot be reached from an initial set of nodes.
				//! \param seedNodes	the initial set of nodes that can be reached (e.g. nodes with spawn points)
				//! \param navMesh		the navigation mesh which should be optimized
				XAIT_MAP_DLLAPI void removeNotReachable(const Common::Container::Vector<GlobalNodeID>& seedNodes, NavMeshContainer* navMesh);

				//! \brief Removes all nodes from the NavMesh that cannot be reached from an initial set of nodes
				//! \param seedPositions	set of positions that can be reached (e.g. spawn points)
				//! \param navMesh			the navigation mesh which should be optimized
				XAIT_MAP_DLLAPI void removeNotReachable(const Common::Container::Vector<Common::Math::Vec3f>& seedPositions, NavMeshContainer* navMesh);

			private:
				void removeUnusedVertices(NavMeshSector* sector);
			};

		}
	}
}
