// (C) xaitment GmbH 2006-2012

#pragma once 

#include <xait/common/memory/Allocator.h>
#include <xait/common/memory/StaticMemoryManaged.h>
#include <xait/map/CompileDefines.h>

namespace XAIT
{
	namespace Map
	{
		class MapMemoryPool
		{
		public:
			//! \brief Get the free list memory allocator
			XAIT_MAP_DLLAPI static const Common::Memory::Allocator::Ptr& getLowAllocThreadSafeAllocator();
		};
	}
}
