// (C) xaitment GmbH 2006-2012

#pragma once 

#include <xait/map/PathObject.h>

namespace XAIT
{
	namespace Map
	{
		//! A path object that groups several other path objects. This group can be used
		//! to switch several path objects on and off.
		//! \brief A path object that groups several other path objects
		class PathObjectGroup : public PathObject
		{
		public:
			//! \brief Constructs a path object group
			//! \param name		unique name
			//! \remark The name must be unique within owning navmesh.
			XAIT_MAP_DLLAPI PathObjectGroup(const Common::String& name);

			//! \brief Gets all path objects that are within this group
			//! \remark This function is non recursive, delivering only direct path objects within this group.
			const StaticContainer<PathObject*>::Vector& getPathObjects() const { return mPathObjects; }

			//! \brief Add a path object to this group
			XAIT_MAP_DLLAPI void addPathObject(PathObject* object);

			//! \brief Remove a path object from this group
			XAIT_MAP_DLLAPI void removePathObject(PathObject* object);

			//! \brief Set the enable state of all path objects within this group
			//! \remark This function is recursive and handles also path objects within path object groups in this group.
			XAIT_MAP_DLLAPI void setEnable(const bool enable);

			XAIT_MAP_DLLAPI virtual PathObject* clone() const;
			XAIT_MAP_DLLAPI virtual uint32 getMemoryUsage() const;

			XAIT_MAP_DLLAPI virtual void handleNewOwner(NavMeshContainer* newOwner);
			XAIT_MAP_DLLAPI virtual void handleSerializeFinished();

		protected:
			XAIT_MAP_DLLAPI virtual bool initFromStream(Common::Serialize::SerializeStorage* serializer);
			XAIT_MAP_DLLAPI virtual void writeToStream(Common::Serialize::SerializeStorage* serializer) const;


		private:
			StaticContainer<PathObject*>::Vector	mPathObjects;
			bool									mHandled;

			void setEnable(const bool enable, const bool isRoot);
			void clearHandled();
		};
	}
}

