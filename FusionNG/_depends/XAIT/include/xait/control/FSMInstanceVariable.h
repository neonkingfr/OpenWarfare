// (C) xaitment GmbH 2006-2012

#pragma once


namespace XAIT
{
	namespace Control
	{
		class FSMInstance;
		class ExternalVariableCallback;

		class FSMInstanceVariable
		{
			friend class FSM;
			friend class FSMInstance;
			friend class FSMInstanceVariableIterator;
			friend class FSMVariableResolver;
			friend class FSMVariable;

		public:

			XAIT_CONTROL_DLLAPI FSMInstanceVariable();

			XAIT_CONTROL_DLLAPI FSMInstanceVariable& operator=(const FSMInstanceVariable& src);

			XAIT_CONTROL_DLLAPI bool operator==(const FSMInstanceVariable& other) const;

			//! \brief gets the name of the variable 
			//! \returns the name of the variable
			XAIT_CONTROL_DLLAPI const Common::String getName() const;

			//! \brief checks if the fsmVariable is valid
			//! \returns true if the variable is valid
			XAIT_CONTROL_DLLAPI bool isValid() const;
			
			//! \brief gets the namespace of the variable
			//! \returns the namespace of the variable
			XAIT_CONTROL_DLLAPI const Common::String getNameSpace() const;
			
			//! \brief gets the full name of the event (namespace included)
			//! \returns the full name of the event
			XAIT_CONTROL_DLLAPI const Common::String getFullName() const;
			
			//!	\brief gets the datatype id of this variable
			//! \returns the datatype id
			XAIT_CONTROL_DLLAPI Common::Reflection::Datatype::DatatypeID getDatatypeID() const;

			//! \returns true if this variable is a reference variable
			XAIT_CONTROL_DLLAPI bool isReferenceVariable() const;
			
			//! \returns true if this variable is a callback variable
			XAIT_CONTROL_DLLAPI bool isCallbackVariable() const;


			//! \brief the corresponding fsm (if it is global fsm variable)
			//! \returns the corresponding fsm
			XAIT_CONTROL_DLLAPI FSM* getFSM() const;
			
			//! \brief the corresponding fsmInstance 
			//! \returns the corresponding fsmInstance
			XAIT_CONTROL_DLLAPI FSMInstance* getFSMInstance() const;

			//! \brief set a reference to use with this variable
			//! \param varReference the reference to use 
			XAIT_CONTROL_DLLAPI void setVariableReferencePtr(void* varReference);

			//! \brief set a reference to use with this variable
			//! \param varReference the reference to use 
			template<typename T_ENTRY_TYPE>
			void setVariableReference(const T_ENTRY_TYPE& varReference)
			{
				X_ASSERT_MSG(getDatatypeID() == GET_DATATYPE_ID(T_ENTRY_TYPE),"trying to set variable reference with wrong datatype");
				setVariableReferencePtr((void*) &varReference);
			}
			
			//! \brief set an external callback to be used for this variable
			//! \param varCallback the external callback
			XAIT_CONTROL_DLLAPI void setVariableCallback(ExternalVariableCallback* varCallback);

			//! \brief Gets a pointer to the value of the variable.
			//! \returns a pointer to the value of the variable
			//! \see setValueByPtr
			XAIT_CONTROL_DLLAPI void* getValuePtr() const;

			//! \brief Sets the value of the variable from a pointer.
			//! \param value	a pointer to the new value
			//! \see getValuePtr
			XAIT_CONTROL_DLLAPI void setValueByPtr(void* value);

			//! \brief Sets the value of the variable.
			//! \param value	the new value
			//! \see getValue
			template<typename T_ENTRY_TYPE>
			void setValue(const T_ENTRY_TYPE& value)
			{				
				X_ASSERT_MSG(getDatatypeID() == GET_DATATYPE_ID(T_ENTRY_TYPE),"trying to set variable value with wrong datatype");
				setValueByPtr((void*) &value);
			}

			//! \brief Gets the variable value
			//! \returns	the value of the variable
			//! \see setValue
			template<typename T_ENTRY_TYPE>
			const T_ENTRY_TYPE& getValue()
			{
				X_ASSERT_MSG(getDatatypeID() == GET_DATATYPE_ID(T_ENTRY_TYPE),"trying to get variable value with wrong datatype");
				void* value = getValuePtr();
				X_ASSERT_MSG_DBG(value != NULL, "variable is not initalized or already destroyed");
				return *( (T_ENTRY_TYPE*)value );
			}

			//! \brief Gets the variable value
			//! \returns	the value of the variable
			//! \see setValue
			template<typename T_ENTRY_TYPE>
			void getValue(T_ENTRY_TYPE& returnValue)
			{
				X_ASSERT_MSG(getDatatypeID() == GET_DATATYPE_ID(T_ENTRY_TYPE),"trying to get variable value with wrong datatype");
				void* value = getValuePtr();
				X_ASSERT_MSG_DBG(value != NULL, "variable is not initalized or already destroyed");
				returnValue = *( (T_ENTRY_TYPE*)value );
			}
	
		private:

			FSMInstanceVariable(FSMVarID varID, FSMInstanceID instanceID);


			FSMVarID getVariableID() const
			{
				return mVariableID;
			}

			void setFSMInstance(FSMInstanceID instanceID)
			{
				mFSMInstanceID = instanceID;
			}

			FSMMemory& getMemory() const;
			
			FSMInstanceID	mFSMInstanceID;
			FSMVarID		mVariableID;
		};

		
	}
}