// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/memory/Allocator.h>
#include <xait/common/parser/formula/VariableResolver.h>
#include <xait/common/parser/formula/Expression.h>
#include <xait/common/container/LookUp.h>
#include <xait/control/FSMManager.h>
#include <xait/control/FSMVarID.h>
#include <xait/control/FSMTypedefs.h>



namespace XAIT
{
	namespace Control
	{
		class FSMMemory;

		class FSMVariableResolver : public Common::Parser::Formula::VariableResolver
		{
		public:
			//! \brief constructor
			FSMVariableResolver(FSMSetID fsmSetID, const Common::Memory::AllocatorPtr& allocator);

			//! \brief destructor
			virtual ~FSMVariableResolver();

			//! \brief creates an variable mathvalue
			//! \param expression		an xml node that contains the expression
			//! \param errorMsg			an error message container which stores occuring errors
			//! returns the new Mathvalue; NULL if name cannot be resolved
			virtual Common::Parser::Formula::MathValue* getVariableMathValue(Common::Parser::XML::XMLNodeElements* expression, Common::Parser::Formula::Expression::ErrorMsg& errorMsg);
			
			//! \brief Creates an membervariable MathValue.
			//! \param expression		a XML node that contains the expression
			//! \param errorMsg			an error message container which stores errors that might perhaps occur
			//! returns the new Mathvalue, or NULL if name cannot be resolved
			virtual Common::Parser::Formula::MathValue* getMemberVariableMathValue(Common::Parser::XML::XMLNodeElements* expression, Common::Parser::Formula::FunctionEnvironment* functionEnvironment, Common::Parser::Formula::Expression::ErrorMsg& errorMsg);
			
			//! \brief creates an constant mathvalue
			//! \param expression		an xml node that contains the expression
			//! \param errorMsg			an error message container which stores occuring errors
			//! returns the new Mathvalue; NULL if name cannot be resolved
			virtual Common::Parser::Formula::MathValue* getConstantMathValue(Common::Parser::XML::XMLNodeElements* expression, Common::Parser::Formula::Expression::ErrorMsg& errorMsg);


			//! \brief creates an custom mathvalue
			//! \param expression				an xml node that contains the expression
			//! \param functionEnvironment		the used function environment
			//! \param errorMsg					an error message container which stores occuring errors
			//! returns the new Mathvalue; NULL if name cannot be resolved
			virtual Common::Parser::Formula::MathValue* getCustomMathValue(Common::Parser::XML::XMLNodeElements* expression, Common::Parser::Formula::FunctionEnvironment* functionEnvironment, Common::Parser::Formula::Expression::ErrorMsg& errorMsg);

			//! \brief stores a pointer to a fsmMemory
			//! \param fsmMemory	pointer to the memory
			void setFSMMemory(FSMMemory* fsmMemory);



		protected:

			FSMSetID							mFSMSetID;
			FSMMemory*							mFSMMemory;

			const Common::Memory::AllocatorPtr&	mAllocator;



			FSMVarID getFSMMemoryID(const Common::String& name);	
			

#ifdef XAIT_CONTROL_DEBUGGER

			//////////////////////////////////////////////////////////////////////////
			// Debugger Part
			//////////////////////////////////////////////////////////////////////////


		public:
			XAIT_CONTROL_DLLAPI void setFoundVariablesContainer(Common::Container::List<Common::String>* container);	

		protected:

			void storeVariableAppearance(Common::String name);

			Common::Container::List<Common::String>* mFoundVariables;
#endif
		};
	}
}
