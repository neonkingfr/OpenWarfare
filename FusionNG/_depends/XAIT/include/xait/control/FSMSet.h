// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/control/FSMEvent.h>
#include <xait/control/ControlStaticMemoryManaged.h>


namespace XAIT
{
	// forward declarations
	namespace Common
	{
		namespace Parser
		{
			namespace XML
			{
				class XMLNodeElements;
			}
		}

#ifdef XAIT_CONTROL_DEBUGGER
		namespace Debugger
		{
			class Module;
			class ObjectGroup;
		}
#endif
	}

	namespace Control
	{
		class ControlDatatypesInitHelper;
		class FSMInstanceWrapper;
		class FSMVariableResolver;

		class FSMSet : public ControlMemManagedObject
		{
			friend class XAIT::Control::FSMManager;
			friend class XAIT::Control::FSM;
			friend class XAIT::Control::FSMInstance;
			friend class XAIT::Control::FSMEvent;
			friend class XAIT::Control::ControlDatatypesInitHelper;
			friend class XAIT::Control::FSMInstanceWrapper;
			friend class XAIT::Control::FSMVariableResolver;
		public:
			
		
			//! \brief gets the name of the fsmSet
			//! \returns the name of the fsmSet
			XAIT_CONTROL_DLLAPI const Common::String& getName() const { return mName; }

			//! \brief check if the fsmSet is valid
			//! \returns true if the set is valid
			XAIT_CONTROL_DLLAPI bool isValid() const;

			//! \brief gets the fsm with given name from the set
			//! \param name of the fsm to retrieve
			//! \returns the fsm with name
			XAIT_CONTROL_DLLAPI FSM* getFSM(const Common::String& name) const;
			
			//! \brief gets all fsms from this set
			//! \returns an iterator to access all fsms
			XAIT_CONTROL_DLLAPI FSMIterator getFSMs() const;

			//! \brief gets the event with the given name and given namespace
			//! \param name       the Name of the event
			//! \param nameSpace  the namespace of the event
			//! \returns event with the given name from the given namespace
			XAIT_CONTROL_DLLAPI FSMEvent* getEvent(const Common::String& name, const Common::String& nameSpace) const;
			
			//! \brief gets the event with the given name from default namespace (Loc)
			//! \param name  the Name of the event
			//! \returns event with the name in the default namespace
			XAIT_CONTROL_DLLAPI FSMEvent* getEvent(const Common::String& name) const;	
			
			//! \brief get all available events
			//! \returns an iterator to access all events
			XAIT_CONTROL_DLLAPI FSMEventIterator getEvents() const;

			//! \brief Load a single fsm configuration contained in the file given by filename
			//! \param filename path to the configuration file
			//! \returns true when loading was successful, false otherwise
			XAIT_CONTROL_DLLAPI bool loadFSM(const Common::String& filename);

			//! \brief Unload a previously loaded FSM
			//! \param fsm the fsm to unload
			//! \returns true if unload suceeded, false otherwise
			XAIT_CONTROL_DLLAPI bool unloadFSM(FSM* fsm);

			//! \brief Gets a pointer to the function declaration manager 
			//! \returns a pointer to the functions declaration manager
			const Common::Reflection::Function::FunctionDeclarationManager::Ptr& getFunctionDeclarations() const
			{
				return mFunctionDeclarationManager;
			}

			//! \brief Gets an iterator to first function declared in a particular FSM set.
			//! \param fsmSetID		id of the FSM set
			//! \returns the iterator
			XAIT_CONTROL_DLLAPI Common::Reflection::Function::FunctionDeclarationManager::FunctionIterator getFunctions() const;

		private:
			FSMSet(const Common::String& name);
			~FSMSet();

			Common::String getFunctionName(Common::Reflection::Function::FunctionID functionID) const;

			void createDefaultFunctionDeclarations();

			//////////////////////////////////////////////////////////////////////////
			// Init and Deinit
			//////////////////////////////////////////////////////////////////////////

			bool initFromXML(Common::Parser::XML::XMLNodeElements* node, const Common::String& path = "");
			//! \brief read a single file FSM config
			bool initFromXMLMonolithic(Common::Parser::XML::XMLNodeElements* node);
			//! \brief read common config file for FSMSet (minimum contents for FSMSet - required for loading FSMs)
			bool initFromXMLCommonConfig(Common::Parser::XML::XMLNodeElements* node);
			//! \brief read meta file, containing a list of FSMs for a set
			bool initFromXMLMetafile(Common::Parser::XML::XMLNodeElements* node, const Common::String& path);

			bool parseFunctionDeclarations(Common::Parser::XML::XMLNodeElements* xmlNode);
			bool parseEventDeclarations(Common::Parser::XML::XMLNodeElements* xmlNode);			
			bool parseFSMDeclarations(Common::Parser::XML::XMLNodeElements* xmlNode);			
			bool parseTransitionIdentifiers(Common::Parser::XML::XMLNodeElements* xmlNode);	
			void deinit();

			FSMSetID														mOwnID;
			Common::String													mName;
			Common::Reflection::Function::FunctionDeclarationManager::Ptr	mFunctionDeclarationManager;
						
			Common::Container::LookUp<Common::String,FSMID>					mFSMNameIDMapping;	

			Common::Container::IDVector<FSMEvent*>							mEvents;

			Common::Container::HashMap<Common::String, uint32>				mTransitionIdentifierIDMapping;

			FSMSetID getFSMSetID() const { return mOwnID; }
			FSMEvent* getEventByFullName(const Common::String& name) const;
			XAIT_CONTROL_DLLAPI FSMEvent* getEvent(FSMEventID eventID) const;
			
			void closeFSMTransition(Common::String& fsmName, int32 identifier);
			void closeFSMTransitionGroup(Common::String& fsmName, int32 identifier);
			void closeTransition(FSMInstanceWrapper& wrapper, int32 identifier);
			void closeTransitionGroup(FSMInstanceWrapper& wrapper, int32 identifier);
			void openFSMTransition(Common::String& fsmName, int32 identifier);
			void openFSMTransitionGroup(Common::String& fsmName, int32 identifier);
			void openTransition(FSMInstanceWrapper& wrapper, int32 identifier);
			void openTransitionGroup(FSMInstanceWrapper& wrapper, int32 identifier);

#ifdef XAIT_CONTROL_DEBUGGER
			void attachObjectGroup(Common::Debugger::Module* module);
			Common::Debugger::ObjectGroup*									mObjectGroup;
#endif			
		};
	}
}