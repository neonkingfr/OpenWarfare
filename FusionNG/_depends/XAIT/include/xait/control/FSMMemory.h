// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/String.h>
#include <xait/common/container/LookUp.h>
#include <xait/common/memory/Allocator.h>
#include <xait/common/threading/SpinLock.h>
#include <xait/common/reflection/ReflectionInterface.h>
#include <xait/control/Variable.h>
#include <xait/control/FSMManager.h>
#include <xait/control/FSMVarID.h>

#define INVALID_FSM_MEM_ID -1


namespace XAIT
{
	namespace Control
	{
		class FSMVariableResolver;
		class FSMVariableIterator;

		//! \brief This class represent a memory that can be used by FSM instances.
		//!
		//! Don't try to access the memory of a FSM instance directly. Use the corresponding function of the FSM instance instead.
		class FSMMemory
		{
			friend class FSM;
			friend class FSMVariable;
			friend class FSMInstanceVariable;
			friend class FSMManager;
			friend class FSMInstance;
			friend class FSMVariableIterator;
			friend class FSMInstanceVariableIterator;
			friend class FSMVariableResolver;
		public:
			//! \brief Destructor.
			XAIT_CONTROL_DLLAPI ~FSMMemory();

			//! \brief Assignment operator.
			XAIT_CONTROL_DLLAPI FSMMemory& operator=(const FSMMemory& other);


			//! \brief Gets the FSMVarID of a variable.
			//! \param name		the name of the variable
			//! \returns the corresponding variable id
			//! \see existsID
			XAIT_CONTROL_DLLAPI FSMVarID getID(const Common::String& name) const;

			//! \brief Gets the name of a variable by its FSMVarID.
			//! \param variableID		the id of the variable
			//! \returns the name of the variable, or empty string if the variable does not exist
			XAIT_CONTROL_DLLAPI const Common::String& getVariableName(const FSMVarID& variableID) const;

			//! \brief Checks whether a variable with the given name exists.
			//! \param name		name of the variable
			//! \returns true if the variable exists, false otherwise
			//! \see getID
			XAIT_CONTROL_DLLAPI bool existsID(const Common::String& name) const;

			//! \brief Gets the value of a variable.
			//! \param value	reference to the variable in which the value is stored
			//! \param id		id of the variable of which you want the value
			//! \see setValue
			template<typename T_VARIABLE_TYPE>
			void getValue(T_VARIABLE_TYPE& value, FSMVarID id) 
			{
				Common::Reflection::Datatype::DatatypeManager::Ptr datatypeManager = Common::Reflection::ReflectionInterface::getDatatypeManager();
				X_ASSERT_MSG(GET_DATATYPE_ID(T_VARIABLE_TYPE) == getDatatypeID(id), "access variable: \"" << getVariableName(id) << "\" of type: \"" << datatypeManager->getDatatypeName(getDatatypeID(id)) << "\" with wrong type: \"" << datatypeManager->getDatatypeName(GET_DATATYPE_ID(T_VARIABLE_TYPE)) << "\".");
				uint32 internalID = getInternalID(id);

				value = mVariables[internalID]->getValue<T_VARIABLE_TYPE>();
			}

			//! \brief Sets the value of a variable.
			//! \param value	the new value of the variable
			//! \param id		id of the variable
			//! \see getValue
			template<typename T_VARIABLE_TYPE>
			void setValue(const T_VARIABLE_TYPE& value, FSMVarID id)
			{
				Common::Reflection::Datatype::DatatypeManager::Ptr datatypeManager = Common::Reflection::ReflectionInterface::getDatatypeManager();
				X_ASSERT_MSG(GET_DATATYPE_ID(T_VARIABLE_TYPE) == getDatatypeID(id), "access variable: \"" << getVariableName(id) << "\" of type: \"" << datatypeManager->getDatatypeName(getDatatypeID(id)) << "\" with wrong type: \"" << datatypeManager->getDatatypeName(GET_DATATYPE_ID(T_VARIABLE_TYPE)) << "\".");
				uint32 internalID = getInternalID(id);

				mVariables[internalID]->setValue(value);
			}

			//! \brief Gets the pointer to the value of the variable given by the parameter.
			//! \param variableID		the id of the variable
			//! \returns the pointer to the value
			//! \see setValueByPtr
			XAIT_CONTROL_DLLAPI void* getValuePtr(FSMVarID variableID);

			//! \brief Sets a value of a variable from a pointer.
			//! \param value			a pointer to the new value
			//! \param variableID		the id of the variable
			//! \see getValuePtr
			XAIT_CONTROL_DLLAPI void setValueByPtr(void* value, FSMVarID variableID);

			//! \brief Initializes the memory from a stream.
			//! \param stream		the stream containing the memory
			//! \see writeToStream
			XAIT_CONTROL_DLLAPI void initFromStream(Common::Serialize::SerializeStorage* stream);

			//! \brief Serializes the memory to a stream.
			//! \param stream	the stream that is used to store the memory
			//! \see initFromStream
			XAIT_CONTROL_DLLAPI void writeToStream(Common::Serialize::SerializeStorage* stream);


		protected:

			struct FSMVarMapping
			{
				FSMVarMapping(const Common::Memory::AllocatorPtr& allocator);

				FSMID								mFSMID;
				Common::Container::Vector<uint32>	mIDMapping;
			};


			const Common::Memory::AllocatorPtr&						mAllocator;
			FSMID													mOwnFSMID;
			Common::Container::LookUp<Common::String,uint16>		mNameVariableMapping;
			StaticManagedContainer<Variable*>::Vector				mVariables;
			StaticManagedContainer<FSMVarMapping>::Vector			mFSMVariableMapping;

			Common::Threading::SpinLock								mMutex;


			//! \brief Constructor.
			XAIT_CONTROL_DLLAPI FSMMemory(const Common::Memory::AllocatorPtr& allocator);		

			//! \brief Copy constructor.
			XAIT_CONTROL_DLLAPI FSMMemory(const FSMMemory& memory,const Common::Memory::AllocatorPtr& allocator);

			//! \brief Adds a variable to the memory.
			//! \param name		name of the variable
			//! \param value	initial value of the variable
			//! \param datatypeID the ID of the data type of the variable
			//! \returns true when adding succeeded, false otherwise
			XAIT_CONTROL_DLLAPI bool addValue(const Common::String& name, void* value, Common::Reflection::Datatype::DatatypeID datatypeID);

			//! \brief Adds all variables of another memory to this memory.
			//! \param fsmMemory	the other memory
			XAIT_CONTROL_DLLAPI void addFSMMemory(FSMMemory& fsmMemory);


			//! \brief Gets the internal id of a variable.
			//! \param id	FSMVarID of the variable
			//! \returns the internal id, -1 if the variable does not exist
			XAIT_CONTROL_DLLAPI uint32 getInternalID(const FSMVarID& id) const;


			//! \brief Gets the access id of a FSM in the variable mapping.
			//! \param fsmID	id of the FSM
			//! \returns the internal id, -1 if the fsm does not exist in the mapping
			XAIT_CONTROL_DLLAPI uint32 getInternalFSMID(FSMID fsmID);

			//! \brief Sets the owner of this memory to the FSMID given by the parameter.
			//! \param fsmID	id of the owning FSM
			XAIT_CONTROL_DLLAPI void setFSMOwner(FSMID fsmID);

			//! \brief Gets the type of a variable.
			//! \param id	id the variable
			//! \returns the type of the variable
			XAIT_CONTROL_DLLAPI Common::Reflection::Datatype::DatatypeID getDatatypeID(FSMVarID id) const;

			//! \brief Gets a variable.
			//! \param index the index of the variable
			Variable* getVariable(FSMVarID id);

			//! \brief Resets the variable values.
			void reset();

			//! \brief Locks the mutex.
			void lock();

			//! \brief Unlocks the mutex.
			void unlock();

			void overwriteVariable(FSMVarID variableID, Variable* newVariable);
		};
	}
}

