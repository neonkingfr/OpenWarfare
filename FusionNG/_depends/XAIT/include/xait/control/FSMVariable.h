// (C) xaitment GmbH 2006-2012

#pragma once


namespace XAIT
{
	namespace Control
	{
		class FSMInstance;
		class ExternalVariableCallback;

		class FSMVariable
		{
			friend class FSM;
			friend class FSMInstance;
			friend class FSMVariableIterator;
			friend class FSMVariableResolver;

		public:

			XAIT_CONTROL_DLLAPI FSMVariable();

			FSMVariable(FSMVarID varID);

			XAIT_CONTROL_DLLAPI FSMVariable& operator=(const FSMVariable& src);

			XAIT_CONTROL_DLLAPI bool operator==(const FSMVariable& other) const;

			//! \brief gets the name of the variable 
			//! \returns the name of the variable
			XAIT_CONTROL_DLLAPI const Common::String getName() const;

			//! \brief checks if the fsmVariable is valid
			//! \returns true if the variable is valid
			XAIT_CONTROL_DLLAPI bool isValid() const;
			
			//! \brief gets the namespace of the variable
			//! \returns the namespace of the variable
			XAIT_CONTROL_DLLAPI const Common::String getNameSpace() const;
			
			//! \brief gets the full name of the event (namespace included)
			//! \returns the full name of the event
			XAIT_CONTROL_DLLAPI const Common::String getFullName() const;

			//! \breif get an instance variable for this variable type
			//! \returns the fsmInstance Variable for this variable type
			XAIT_CONTROL_DLLAPI FSMInstanceVariable getInstanceVariable(const FSMInstance* instance) const;
			
			//!	\brief gets the datatype id of this variable
			//! \returns the datatype id
			XAIT_CONTROL_DLLAPI Common::Reflection::Datatype::DatatypeID getDatatypeID() const;

			//! \returns true if this variable is a reference variable
			XAIT_CONTROL_DLLAPI bool isReferenceVariable() const;
			
			//! \returns true if this variable is a callback variable
			XAIT_CONTROL_DLLAPI bool isCallbackVariable() const;

			//! \brief the corresponding fsm (if it is global fsm variable)
			//! \returns the corresponding fsm
			XAIT_CONTROL_DLLAPI FSM* getFSM() const;
			
			//! \brief set a reference to use with this variable
			//! \fsmInstance the fsmInstance to set the reference for
			//! \param varReference the reference to use 
			XAIT_CONTROL_DLLAPI void setVariableReferencePtr(FSMInstance* fsmInstance, void* varReference);

			//! \brief set a reference to use with this variable
			//! \fsmInstance the fsmInstance to set the reference for
			//! \param varReference the reference to use
			template<typename T_ENTRY_TYPE>
			void setVariableReference(FSMInstance* fsmInstance, const T_ENTRY_TYPE& varReference)
			{
				X_ASSERT_MSG(getDatatypeID() == GET_DATATYPE_ID(T_ENTRY_TYPE),"trying to set variable reference with wrong datatype");
				setVariableReferencePtr(fsmInstance, (void*) &varReference);
			}
			
			//! \brief set an external callback to be used for this variable
			//! \param varCallback the external callback
			XAIT_CONTROL_DLLAPI void setVariableCallback(FSMInstance* fsmInstance, ExternalVariableCallback* varCallback);

			//! \brief set an fsm-level reference to use on all instances of this variable
			//! \param varReference the reference to use 
			XAIT_CONTROL_DLLAPI void setFSMLevelVariableReferencePtr(void* varReference);

			//! \brief set an fsm-level reference to use on all instances of this variable
			//! \param varReference the reference to use 
			template<typename T_ENTRY_TYPE>
			void setFSMLevelVariableReference(const T_ENTRY_TYPE& varReference)
			{
				X_ASSERT_MSG(getDatatypeID() == GET_DATATYPE_ID(T_ENTRY_TYPE),"trying to set variable reference with wrong datatype");
				setFSMLevelVariableReferencePtr((void*) &varReference);
			}

			//! \brief set an fsm-level external callback to be used for this variable
			//! \param varCallback the external callback
			XAIT_CONTROL_DLLAPI void setFSMLevelVariableCallback(ExternalVariableCallback* varCallback);


			//! \brief Gets a pointer to the value of the variable.
			//! \param fsmInstance to get the value pointer from
			//! \returns a pointer to the value of the variable
			//! \see setValueByPtr
			XAIT_CONTROL_DLLAPI void* getValuePtr(FSMInstance* fsmInstance) const;

			//! \brief Sets the value of the variable from a pointer.
			//! \param fsmInstance to set the value pointer for
			//! \param value	a pointer to the new value
			//! \see getValuePtr
			XAIT_CONTROL_DLLAPI void setValueByPtr(FSMInstance* fsmInstance, void* value);

			//! \brief Sets the value of the variable.
			//! \brief fsmInstance to set the value for
			//! \param value	the new value
			//! \see getValue
			template<typename T_ENTRY_TYPE>
			void setValue(FSMInstance* fsmInstance, const T_ENTRY_TYPE& value)
			{				
				X_ASSERT_MSG(getDatatypeID() == GET_DATATYPE_ID(T_ENTRY_TYPE),"trying to set variable value with wrong datatype");
				setValueByPtr(fsmInstance, (void*) &value);
			}

			//! \brief Gets the variable value
			//! \brief fsmInstance to get the value from
			//! \returns	the value of the variable
			//! \see setValue
			template<typename T_ENTRY_TYPE>
			const T_ENTRY_TYPE& getValue(FSMInstance* fsmInstance)
			{
				X_ASSERT_MSG(getDatatypeID() == GET_DATATYPE_ID(T_ENTRY_TYPE),"trying to get variable value with wrong datatype");
				void* value = getValuePtr(fsmInstance);
				X_ASSERT_MSG_DBG(value != NULL, "variable is not initalized or already destroyed");
				return *( (T_ENTRY_TYPE*)value );
			}

			//! \brief Gets the variable value
			//! \param fsmInstance to set the value for
			//! \returns	the value of the variable
			//! \see setValue
			template<typename T_ENTRY_TYPE>
			void getValue(FSMInstance* fsmInstance, T_ENTRY_TYPE& returnValue)
			{
				X_ASSERT_MSG(getDatatypeID() == GET_DATATYPE_ID(T_ENTRY_TYPE),"trying to get variable value with wrong datatype");
				void* value = getValuePtr(fsmInstance);
				X_ASSERT_MSG_DBG(value != NULL, "variable is not initalized or already destroyed");
				returnValue = *( (T_ENTRY_TYPE*)value );
			}
	
		private:

			FSMVarID getVariableID() const
			{
				return mVariableID;
			}

			FSMMemory& getMemory(FSMInstance* fsmInstance) const;
			

			FSMVarID		mVariableID;
		};

		
	}
}