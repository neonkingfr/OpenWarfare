// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/control/FSMManager.h>
#include <xait/control/FSMMemory.h>
#include <xait/control/FSMVariableResolver.h>
#include <xait/control/FSMTransition.h>
#include <xait/control/FSMEvent.h>
#include <xait/control/FSMVariable.h>
#include <xait/control/FSMInstanceVariable.h>
#include <xait/control/FSMFunction.h>
#include <xait/control/ExternalVariableCallback.h>


namespace XAIT
{
	namespace Common
	{
		namespace Parser
		{
			namespace XML
			{
				class XMLNodeElements;
			}

			namespace Formula
			{
				template<typename T_VALUE_TYPE>class ReferencedFSMMemberVariableMathValue;
				class UntypedReferencedFSMMemberVariableMathValue;
			}
		}
#ifdef XAIT_CONTROL_DEBUGGER
		namespace Debugger
		{
			class ObjectGroup;
			class ObjectType;
			class BreakpointInstance;
		}
#endif
	}

	
	namespace Control
	{
		struct FSMState;
		class FSMManager;

		class FSM : public ControlMemManagedObject
		{
			friend class FSMInstance;
			friend class FSMVariable;
			friend class FSMInstanceVariable;
			friend class FSMFunctionIterator;
			friend class FSMMemory;
			friend class FSMSet;
			friend class FSMManager;
			friend class FSMVariableResolver;
			friend class FSMInstanceWrapper;
#ifndef XAIT_COMPILER_MSVC8
			template<typename T_VALUE_TYPE>friend class Common::Parser::Formula::ReferencedFSMMemberVariableMathValue;
#else
			friend class Common::Parser::Formula::ReferencedFSMMemberVariableMathValue<bool>;
			friend class Common::Parser::Formula::ReferencedFSMMemberVariableMathValue<int32>;
			friend class Common::Parser::Formula::ReferencedFSMMemberVariableMathValue<int64>;
			friend class Common::Parser::Formula::ReferencedFSMMemberVariableMathValue<uint32>;
			friend class Common::Parser::Formula::ReferencedFSMMemberVariableMathValue<float32>;
			friend class Common::Parser::Formula::ReferencedFSMMemberVariableMathValue<Common::Math::Vec2f>;
			friend class Common::Parser::Formula::ReferencedFSMMemberVariableMathValue<Common::Math::Vec3f>;
			friend class Common::Parser::Formula::ReferencedFSMMemberVariableMathValue<Common::String>;
#endif
			friend class Common::Parser::Formula::UntypedReferencedFSMMemberVariableMathValue;
		public:

			//! \brief gets the parent fsmSet
			//! \returns parent fsmSet of this fsm
			XAIT_CONTROL_DLLAPI FSMSet* getFSMSet() const;
			
			//! \brief create an fsmInstance from this fsm
			//! \returns fsmInstance of type fsm
			XAIT_CONTROL_DLLAPI FSMInstance* createFSMInstance();
			
			//! \brief create an fsmInstance from this fsm
			//! \param name a name that can be used for debugging or fsmInstance retrieving
			//! \returns fsmInstance of type fsm
			XAIT_CONTROL_DLLAPI FSMInstance* createFSMInstance(const Common::String& name, bool addIDToName = false);

			//! \brief if the fsmInstance was created with a name, it can be retrieved by that name
			//! \param name name of the instance to retrieve
			//! \returns fsmInstance with name
			XAIT_CONTROL_DLLAPI FSMInstance* getFSMInstance(const Common::String& name) const;
			
			//! \brief get all fsmInstances of this fsmType
			//! \returns iterator to access all the fsmInstances			
			XAIT_CONTROL_DLLAPI FSMInstanceIterator getFSMInstances() const;
					
			//! \brief get the fsm's name
			//! \returns name of this fsm
			XAIT_CONTROL_DLLAPI Common::String getName() const;

			//! \brief gets the event with the given name and given namespace
			//! \param name       the Name of the event
			//! \param nameSpace  the namespace of the event
			//! \returns event with the given name from the given namespace
			XAIT_CONTROL_DLLAPI FSMEvent* getEvent(const Common::String& name, const Common::String& nameSpace) const;
			
			//! \brief gets the event with the given name from default namespace (Loc)
			//! \param name  the Name of the event
			//! \returns event with the name in the default namespace
			XAIT_CONTROL_DLLAPI FSMEvent* getEvent(const Common::String& name) const;
			
			//! \brief get all events that are available in the corresponding fsmSet
			//! \returns an iterator to access all events
			XAIT_CONTROL_DLLAPI FSMEventIterator getEvents() const;

			//! \brief gets the events acutally used by the fsm
			//! \returns the used events of the fsm (including subfsms)
			const StaticManagedContainer<FSMEvent*>::Vector& getUsedEvents() const
			{
				return mUsedEvents;
			}

			//! \brief gets the variable with the given name from given namespace
			//! \param name       the Name of the variable
			//! \param nameSpace  the namespace of the variable
			//! \returns variable with the given name from the given namespace
			XAIT_CONTROL_DLLAPI FSMVariable getVariable(const Common::String& name, const Common::String& nameSpace) const;

			//! \brief gets the variable with the given name from default namespace (Loc)
			//! \param name  the name of the variable
			//! \returns variable with the name in the default namespace
			XAIT_CONTROL_DLLAPI FSMVariable getVariable(const Common::String& name) const;
			
			//! \brief get all variables that are used by the fsm
			//! \returns iterator to access all the variables
			XAIT_CONTROL_DLLAPI FSMVariableIterator getVariables() const;	

			//! \brief closes all transitions with identifier on all fsmInstances of this fsm
			//! \param identifier to specify which transitions should be closed (identifier is defined in control creator)
			XAIT_CONTROL_DLLAPI void closeTransition(const Common::String& identifier);

			//! \brief closes all transitions of same type with identifier on all fsmInstances of this fsm
			//! \param identifier to specify which transitions should be closed (identifier is defined in control creator)
			XAIT_CONTROL_DLLAPI void closeTransitionGroup(const Common::String& identifier);
			
			//! \brief opens all transitions with identifier on all fsmInstances of this fsm
			//! \param identifier to specify which transitions should be opened (identifier is defined in control creator)
			XAIT_CONTROL_DLLAPI void openTransition(const Common::String& identifier);
			
			//! \brief opens all transitions of same type with identifier on all fsmInstances of this fsm
			//! \param identifier to specify which transitions should be opened (identifier is defined in control creator)
			XAIT_CONTROL_DLLAPI void openTransitionGroup(const Common::String& identifier);

			//! \brief gets a wrapper for a function
			//! \param functionName			the name of the function
			//! \param functionNameSpace	the nameSpace of the function
			//! \param signature			the signature of the function
			//! \returns wrapper that can be used to access function data and interface
			FSMFunction getFunction(const Common::String& functionName,const Common::String& nameSpace, const Common::Reflection::Function::Signature& signature) const
			{
				Common::Reflection::Function::FunctionID funcID =  mDefaultFunctions.getFunctionID(functionName, nameSpace,signature);
				return FSMFunction(funcID, this, &mDefaultFunctions);
			}


			//! \brief gets a wrapper for a function in local namespace
			//! \param functionName			the name of the function
			//! \param signature			the signature of the function
			//! \returns wrapper that can be used to access function data and interface
			FSMFunction getFunction(const Common::String& functionName, const Common::Reflection::Function::Signature& signature) const
			{
				Common::Reflection::Function::FunctionID funcID =  mDefaultFunctions.getFunctionID(functionName, X_DEFAULT_NAMESPACE,signature);
				return FSMFunction(funcID, this, &mDefaultFunctions);
			}

			//! \brief gets the required functions of the instance
			//! \returns an iterator to the required functions
			const FSMFunctionIterator getRequiredFunctions() const
			{
				return FSMFunctionIterator(this);
			}
					
			//! \brief register an external static function wrapper for default xaitControl-namespace (Loc) that can be used to create dynamic bindings
			//!
			//! supports functions with the signature void functionName(void* returnValue, DatatypeID returnValueType, FunctionArgumentList argList)
			//! such a callback function can be used to create dynamic bindings of existing data driven systems.  
			//! the function is registered for all fsmInstances of this fsm type.
		    //!
			//! \param name		access name of the function in the xaitControl creator
			//! \param func		a pointer to the function
			//! \param fullSignature	the signature of the function
			//! \remark if the function uses the returnvalue, it has to check if the returnvalue pointer is NULL (discard returnvalue)
			void registerStaticWrapperFunction( const Common::String& name, 
				void (*func)(void*,Common::Reflection::Datatype::DatatypeID,const Common::Reflection::Function::FunctionArgumentList&),
				const Common::Reflection::Function::Signature& fullSignature)
			{
				mDefaultFunctions.registerWrapperFunction(name, X_DEFAULT_NAMESPACE ,func,fullSignature);
			}

			//! \brief register an external static function wrapper that can be used to create dynamic bindings
			//!
			//! supports functions with the signature void functionName(void* returnValue, DatatypeID returnValueType, FunctionArgumentList argList)
			//! such a callback function can be used to create dynamic bindings of existing data driven systems. 
			//! the function is registered for all fsmInstances of this fsm type.
			//!
			//! \param name		   access name of the function in the xaitControl creator
			//! \param nameSpace   nameSpace of the function
			//! \param func		   a pointer to the function
			//! \param fullSignature	the signature of the function
			//! \remark if the function uses the returnvalue, it has to check if the returnvalue pointer is NULL (discard returnvalue)
			void registerStaticWrapperFunction(const Common::String& name,
				const Common::String& nameSpace,
				void (*func)(void*,Common::Reflection::Datatype::DatatypeID,const Common::Reflection::Function::FunctionArgumentList&),
				const Common::Reflection::Function::Signature& fullSignature)
			{
				mDefaultFunctions.registerWrapperFunction(name, nameSpace ,func,fullSignature);
			}


			//! \brief register an external function wrapper for default xaitControl-namespace (Loc) that can be used to create dynamic bindings
			//!
			//! supports functions with the signature void functionName(void* returnValue, DatatypeID returnValueType, FunctionArgumentList argList)
			//! such a callback function can be used to create dynamic bindings of existing data driven systems.  
			//! the function is registered for all fsmInstances of this fsm type.
			//!
			//! \param name		access name of the function in the xaitControl creator
			//! \param owner	owner object of the method
			//! \param func		a pointer to the function 
			//! \param fullSignature	the signature of the function
			//! \remark if the function uses the returnvalue, it has to check if the returnvalue pointer is NULL (discard returnvalue)
			template<typename T_FUNCTIONOWNER_TYPE>
			void registerMemberWrapperFunction(const Common::String& name, 
				const T_FUNCTIONOWNER_TYPE& owner,
				void (T_FUNCTIONOWNER_TYPE::*func)(void*,Common::Reflection::Datatype::DatatypeID,const Common::Reflection::Function::FunctionArgumentList&),
				const Common::Reflection::Function::Signature& fullSignature)
			{
				mDefaultFunctions.registerWrapperFunction(name, X_DEFAULT_NAMESPACE ,owner,func,fullSignature);
			}

			//! \brief register an external function wrapper that can be used to create dynamic bindings
			//!
			//! supports functions with the signature void functionName(void* returnValue, DatatypeID returnValueType, FunctionArgumentList argList)
			//! such a callback function can be used to create dynamic bindings of existing data driven systems. 
			//! the function is registered for all fsmInstances of this fsm type.
			//!
			//! \param name		 access name of the function in the xaitControl creator
			//! \param nameSpace nameSpace of the function
			//! \param owner	 owner object of the method
			//! \param func		 a pointer to the function 
			//! \param fullSignature	the signature of the function
			//! \remark if the function uses the returnvalue, it has to check if the returnvalue pointer is NULL (discard returnvalue)
			template<typename T_FUNCTIONOWNER_TYPE>
			void registerMemberWrapperFunction( const Common::String& name, 
				const Common::String& nameSpace,
				const T_FUNCTIONOWNER_TYPE& owner,
				void (T_FUNCTIONOWNER_TYPE::*func)(void*,Common::Reflection::Datatype::DatatypeID,const Common::Reflection::Function::FunctionArgumentList&),
				const Common::Reflection::Function::Signature& fullSignature)
			{
				mDefaultFunctions.registerWrapperFunction(name, nameSpace,owner,func,fullSignature);
			}

			//! \brief register an external static function for default xaitControl-namespace (Loc) with up to 10 arguments with automatic signature detection
			//!
			//! the signature of the function will be detected automatically, and the correct overload of the function will be registered.
			//! the function is registered for all fsmInstances of this fsm type.
			//!
			//! \param name		access name of the function in the xaitControl creator
			//! \param func		a pointer to the function 
			template <typename T_FUNC>
			void registerStaticFunction(const Common::String& name, T_FUNC func)
			{
				mDefaultFunctions.registerFunction(name, X_DEFAULT_NAMESPACE ,func);
			}

			//! \brief register an external static function with up to 10 arguments with automatic signature detection
			//! 
			//! the signature of the function will be detected automatically, and the correct overload of the function will be registered.
			//! the function is registered for all fsmInstances of this fsm type.
			//!
			//! \param name		access name of the function in the xaitControl creator
			//! \param nameSpace nameSpace of the function
			//! \param func		a pointer to the function 
			template <typename T_FUNC>
			void registerStaticFunction(const Common::String& name, const Common::String& nameSpace,T_FUNC func)
			{
				mDefaultFunctions.registerFunction(name, nameSpace, func);
			}

			//! \brief register an external member function for default xaitControl-namespace (Loc) with up to 10 arguments with automatic signature detection
			//!
			//! the signature of the function will be detected automatically, and the correct overload of the function will be registered.
			//! the function is registered for all fsmInstances of this fsm type.
			//!
			//! \param name		access name of the function in the xaitControl creator
			//! \param owner	owner object of the method
			//! \param func		a pointer to the method
			template<typename T_FUNCTIONOWNER_TYPE, typename T_FUNC>
			void registerMemberFunction(const Common::String& name, const T_FUNCTIONOWNER_TYPE& owner, T_FUNC func)  
			{  
				mDefaultFunctions.registerFunction(name, X_DEFAULT_NAMESPACE ,owner,func);  
			}

			//! \brief register an external member function with up to 10 arguments with automatic signature detection
			//! 
			//! the signature of the function will be detected automatically, and the correct overload of the function will be registered.
			//! the function is registered for all fsmInstances of this fsm type.
			//!
			//! \param name		access name of the function in the xaitControl creator
			//! \param nameSpace nameSpace of the function
			//! \param owner	owner object of the method
			//! \param func		a pointer to the method
			template<typename T_FUNCTIONOWNER_TYPE, typename T_FUNC>
			void registerMemberFunction(const Common::String& name, const Common::String& nameSpace, const T_FUNCTIONOWNER_TYPE& owner, T_FUNC func)  
			{  
				mDefaultFunctions.registerFunction(name,nameSpace,owner,func);  
			}


			//! \brief register external static function for default xaitControl-namespace (Loc) with a dynamic argument list 
			//! 
			//! if you register a callback this way, all overloading of the given function that are not registered separately call this function
			//! tha callbackfunction has to have the signature: void functionName(void* returnValue, DatatypeID returnValueType, FunctionArgumentList argList)
			//! such a callback function can be used to create dynamic bindings of existing data driven systems.
			//! the function is registered for all fsmInstances of this fsm type.
			//!
			//! \param name							access name of the function
			//! \param func							a pointer to the function 
			//! \param overrideExistingBindings		flag if existing bindings should be overridden
			//! \remark if the function uses the returnvalue, it has to check if the returnvalue pointer is NULL (discard returnvalue)
			void registerStaticVariadicFunction(const Common::String& name, void (*func)(void*,Common::Reflection::Datatype::DatatypeID,const Common::Reflection::Function::FunctionArgumentList&), Common::Reflection::Datatype::DatatypeID returnType, bool overrideExistingBindings = false)
			{
				mDefaultFunctions.registerVariadicFunction(name, X_DEFAULT_NAMESPACE,func,returnType,overrideExistingBindings);
			}


			//! \brief register external static function with a dynamic argument list 
			//! 
			//! if you register a callback this way, all overloading of the given function that are not registered separately call this function
			//! tha callbackfunction has to have the signature: void functionName(void* returnValue, DatatypeID returnValueType, FunctionArgumentList argList)
			//! such a callback function can be used to create dynamic bindings of existing data driven systems. 
			//! the function is registered for all fsmInstances of this fsm type.
			//!
			//! \param name							access name of the function
			//! \param nameSpace                    nameSpace of the function
			//! \param func							a pointer to the function 
			//! \param overrideExistingBindings		flag if existing bindings should be overridden
			//! \remark if the function uses the returnvalue, it has to check if the returnvalue pointer is NULL (discard returnvalue)
			void registerStaticVariadicFunction(const Common::String& name, const Common::String& nameSpace, void (*func)(void*,Common::Reflection::Datatype::DatatypeID,const Common::Reflection::Function::FunctionArgumentList&), Common::Reflection::Datatype::DatatypeID returnType, bool overrideExistingBindings = false)
			{
				mDefaultFunctions.registerVariadicFunction(name, nameSpace,func,returnType,overrideExistingBindings);
			}

			//! \brief register external member function for default xaitControl-namespace (Loc) with a dynamic argument list 
			//! 
			//! if you register a callback this way, all overloading of the given function that are not registered separately call this function
			//! tha callbackfunction has to have the signature: void functionName(void* returnValue, DatatypeID returnValueType, FunctionArgumentList argList)
			//! such a callback function can be used to create dynamic bindings of existing data driven systems. 
			//! the function is registered for all fsmInstances of this fsm type.
			//!
			//! \param name							access name of the function
			//! \param owner						ownerclass of the method
			//! \param func							a pointer to the function 
			//! \param overrideExistingBindings		flag if existing bindings should be overridden
			//! \remark if the function uses the returnvalue, it has to check if the returnvalue pointer is NULL (discard returnvalue)
			template<typename T_FUNCTIONOWNER_TYPE>
			void registerMemberVariadicFunction(const Common::String& name, const T_FUNCTIONOWNER_TYPE& owner,void (T_FUNCTIONOWNER_TYPE::*func)(void*,Common::Reflection::Datatype::DatatypeID,const Common::Reflection::Function::FunctionArgumentList&), Common::Reflection::Datatype::DatatypeID returnType, bool overrideExistingBindings = false)
			{
				mDefaultFunctions.registerVariadicFunction(name, X_DEFAULT_NAMESPACE ,owner,func,returnType,overrideExistingBindings);
			}

		
			//! \brief register external member function with a dynamic argument list 
			//! 
			//! if you register a callback this way, all overloading of the given function that are not registered separately call this function
			//! tha callbackfunction has to have the signature: void functionName(void* returnValue, DatatypeID returnValueType, FunctionArgumentList argList)
			//! such a callback function can be used to create dynamic bindings of existing data driven systems. 
			//! the function is registered for all fsmInstances of this fsm type.
			//!
			//! \param name							access name of the function
			//! \param nameSpace                    nameSpace of the function
			//! \param owner						ownerclass of the method
			//! \param func							a pointer to the function 
			//! \param overrideExistingBindings		flag if existing bindings should be overridden
			//! \remark if the function uses the returnvalue, it has to check if the returnvalue pointer is NULL (discard returnvalue)
			template<typename T_FUNCTIONOWNER_TYPE>
			void registerMemberVariadicFunction(const Common::String& name, const Common::String& nameSpace, const T_FUNCTIONOWNER_TYPE& owner,void (T_FUNCTIONOWNER_TYPE::*func)(void*,Common::Reflection::Datatype::DatatypeID,const Common::Reflection::Function::FunctionArgumentList&), Common::Reflection::Datatype::DatatypeID returnType, bool overrideExistingBindings = false)
			{
				mDefaultFunctions.registerVariadicFunction(name,nameSpace,owner,func,returnType,overrideExistingBindings);
			}


		private:
			struct SubFSM
			{
				struct EventTransitionEntry
				{
					EventTransitionEntry(FSMEventID eventID, FSMTransitionCollectionID eventTransition);
					
					~EventTransitionEntry();

					FSMEventID			 mEventID;
					FSMTransitionCollectionID mEventTransitionID;
				};

				SubFSM();
				~SubFSM();

				//! \brief sorts the time based transitions
				void sortTimeBasedTransitions();


				FSMID																mFSMType;
				StaticManagedContainer<FSMTransitionCollectionID>::Vector								mDirectExitTransitions;
				StaticManagedContainer<StaticManagedContainer<FSMTimeBasedTransition>::Vector >::Vector	mTimeBasedExitTransitions;
				StaticManagedContainer<StaticManagedContainer<EventTransitionEntry>::Vector >::Vector	mEventExitTransitions;
			};

			Common::String		mName;

			FSMID				mOwnID;
			FSMSetID			mSetID;
			FSMStateID			mStartState;

			FSMMemory			mMemory;

			Common::Container::LookUp<Common::String,FSMInstanceID>				mFSMInstanceNameIDMapping;			
			
			StaticManagedContainer<FSMTransitionCollection>::Vector				mTransitions;
			Common::Container::LookUp<FSMEventID,FSMTransitionCollectionID>		mGlobalTransitions;
			StaticManagedContainer<SubFSM>::Vector								mSubFSMs;
			Common::Container::LookUp<Common::String,FSMStateID>				mStateMapping;
			StaticManagedContainer<FSMState*>::Vector							mStates;
			StaticManagedContainer<FSMStateID>::Vector							mEndStates;
			Common::Reflection::Function::FunctionRegistrar						mDefaultFunctions;
			StaticManagedContainer<FSMEvent*>::Vector							mUsedEvents;
			Common::Container::LookUp<uint32,StaticManagedContainer<FSMTransition*>::Vector>		mTransitionIdentifierTransitionMapping;

			FSMVarID			mSelfVariableID;
			FSMVarID			mInvalidVariableID;

			uint32 mHashValue0;
			uint32 mHashValue1;
			uint32 mHashValue2;
			uint32 mHashValue3;

			void addSubFSM(uint32 subFSMID,FSMID fsmID);

			FSMTransitionCollection& createTransitionCollection(); 

			void registerDefaultFunctions();

			//! \brief sorts the timebased transitions
			void sortTimebasedTransitions();

			// parse
			bool parseFunctions(Common::Parser::XML::XMLNodeElements* node);
			bool parseVariables(Common::Parser::XML::XMLNodeElements* node);
			bool parseSubFSMs(Common::Parser::XML::XMLNodeElements* node);
			bool parseStates(Common::Parser::XML::XMLNodeElements* node);
			bool parseEvents(Common::Parser::XML::XMLNodeElements* node);
			bool parseTransitions(Common::Parser::XML::XMLNodeElements* node);
			bool parseTransitionCondition(Common::Parser::Formula::TypedMathValueInstance<bool>*& condition, Common::Parser::XML::XMLNodeElements* node);
			bool parseTransitionSource(FSMStateID& sourceState, uint32& sourceSubFSM, Common::Parser::XML::XMLNodeElements* node);
			bool parseTransitionDestination(FSMStateID& destinationState, uint32& destinationSubFSM, Common::Parser::XML::XMLNodeElements* node);
			bool parseTransitionCommands(StaticManagedContainer<Command*>::Vector& commands, Common::Parser::XML::XMLNodeElements* node);
			bool parseHashValue(Common::Parser::XML::XMLNodeElements* node);

			//! \brief constructor
			FSM(Common::String name, FSMSetID setID);

			//! \brief destructor
			virtual ~FSM();

			//! \brief inits the fsm from xml
			bool initFromXML(Common::Parser::XML::XMLNodeElements* node);

			//! \brief inits the fsm from xml
			bool initFromXML2(Common::Parser::XML::XMLNodeElements* node);

			//! \brief set the id of the fsm
			//! \param	id		the new id of the fsm
			void setID(FSMID id);

			//! \brief gets the stateid of a state by name
			//! \param name		name of the state
			//! \returns the id of the state
			FSMStateID getStateID(const Common::String& name) const;

			//! \get the name of a state by the state id
			const Common::String& getStateName(const FSMStateID stateID) const;

			//! \brief gets the endstate id of a state
			//! \param id	id of the state
			//! \returns endstateid of the state, -1 if not endstate
			uint32 getEndStateID(FSMStateID id) const;
			
			//! \brief gets the id of the startState
			//! \returns the id of the startState
			FSMStateID getStartState() const;
			
			//! \brief gets the registered default functions
			//! \returns the functionRegistrar that contains the defaultfunctions
			Common::Reflection::Function::FunctionRegistrar& getDefaultFunctions();

			//! \brief gets the registered default functions
			//! \returns the functionRegistrar that contains the defaultfunctions
			const Common::Reflection::Function::FunctionRegistrar& getDefaultFunctions() const;


			FSMEvent* getEventByFullName(const Common::String& name) const;
			FSMEvent* getEvent(FSMEventID eventID) const;

			FSMVariable getVariable(FSMVarID varID) const;
			FSMVariable getVariableByFullName(const Common::String& fullName) const;

			void saveState( Common::Serialize::SerializeStorage* stream);
			void restoreState( Common::Serialize::SerializeStorage* stream);

			FSMID getFSMID() const
			{
				return mOwnID;
			}

			void closeTransition(uint32 identifier);
			void closeTransitionGroup(uint32 identifier);
			void openTransition(uint32 identifier);
			void openTransitionGroup(uint32 identifier);
				
			////////////////////////////////
			////	Debugger Part		////
			////////////////////////////////

#ifdef XAIT_CONTROL_DEBUGGER
		public:

			void attachToObjectType(Common::Debugger::ObjectGroup* group);
			void detachFromObjectType();
			Common::Debugger::ObjectType* getObjectType();

		private:

			Common::Debugger::ObjectType*			mObjectType;
			Common::Container::Vector<Common::Debugger::BreakpointInstance*> mBreakpoints;
			Common::Container::List<Common::Container::Vector<Common::Debugger::BreakpointInstance*>*> mSubFSMBreakpoints;
			Common::Container::LookUp<FSMEventID, Common::Container::Vector<FSMVarID> > mEventParameterVarIDs;	
			Common::Container::LookUp<uint32, uint32> mSubFSMBreakpointMapping;

			bool parseTransitionConditionForDebugger(Common::Parser::Formula::TypedMathValueInstance<bool>*& condition, Common::Parser::XML::XMLNodeElements* node, Common::Container::List<Common::String>* foundVariables);
			bool parseTransitionCommandsForDebugger(StaticManagedContainer<Command*>::Vector& commands, Common::Parser::XML::XMLNodeElements* node, Common::Container::List<Common::String>* foundVariables);
			
#endif	
		};
	}
}
