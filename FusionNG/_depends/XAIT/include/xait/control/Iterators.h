// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/common/reflection/datatype/DatatypeTypedefs.h>
#include <xait/common/reflection/function/FunctionDeclarationManager.h>
#include <xait/control/FSMTypedefs.h>
#include <xait/control/FSMVarID.h>



namespace XAIT
{
	namespace Control
	{
		class FSMSet;
		class FSM;
		class FSMInstance;
		class FSMEvent;
		class FSMVariable;
		class FSMInstanceVariable;
		class FSMFunction;
		class FSMMemory;
		class FSMFunction;

		template<typename IDVectorType>
		class IDVectorIterator
		{
			friend class FSMManager;
			friend class FSMSet;
		public:
			
			//! \brief Gets the id of the element.
			//! \returns the id of the element
			const IDVectorType& getElement()
			{
				return *mIterator;
			}

			//! \brief get element iterator currently points to
			const IDVectorType& operator*() const
			{
				return *mIterator;
			}

			//! \brief Set the iterator to the next element, if it is not the last one
			//! \returns true if it was NOT the last one, and false otherwise
			bool getNext()
			{
				if (mIterator != mEndIterator)
				{
					++mIterator;
					return (mIterator != mEndIterator);
				}
				return false;
			}

			//! \brief Moves the iterator to the next element.
			IDVectorIterator<IDVectorType>& operator++()
			{
				getNext();
				return *this;
			}

			//! \brief Checks whether the iterator reached the end.
			//! \returns true, if it reached the end, and false otherwise
			bool reachedEnd()
			{
				return (mIterator == mEndIterator);
			}

			IDVectorIterator(const IDVectorIterator& rhs)
			{
				mIterator = rhs.mIterator;
				mEndIterator = rhs.mEndIterator;
			}

		private:
			IDVectorIterator(const typename Common::Container::IDVector<IDVectorType>::ConstIterator& iterator, const typename Common::Container::IDVector<IDVectorType>::ConstIterator& endIterator)
				: mIterator(iterator), mEndIterator(endIterator)
			{}
			typename Common::Container::IDVector<IDVectorType>::ConstIterator mIterator;
			typename Common::Container::IDVector<IDVectorType>::ConstIterator mEndIterator;
		};
		
		//! \brief An iterator type to iterate over FSM sets.
		typedef IDVectorIterator<FSMSet*>		FSMSetIterator;

		//! \brief An iterator type to iterate over all FSMInstances
		typedef IDVectorIterator<FSMInstance*>	GlobalFSMInstanceIterator;

		//! \brief An iterator type to iterate over FSMEvents
		typedef IDVectorIterator<FSMEvent*>		FSMEventIterator;

		//! \brief An iterator type to iterate over Functions.
		typedef Common::Reflection::Function::FunctionDeclarationManager::FunctionIterator	FunctionIterator;
		
		//! \brief Iterator providing access to variable informations.
		class FSMInstanceVariableIterator
		{
			friend class FSM;
			friend class FSMInstance;

		public:
			
			XAIT_CONTROL_DLLAPI FSMInstanceVariable getElement() const; 

			XAIT_CONTROL_DLLAPI FSMInstanceVariable operator*() const;

			//! \brief Sets the iterator to the next element, if it isn't already the last one
			//! \returns true if it was NOT the last one, and false otherwise
			XAIT_CONTROL_DLLAPI bool getNext();

			XAIT_CONTROL_DLLAPI FSMInstanceVariableIterator& operator++();

			//! \brief Checks whether the variable iterator reached the last variable.
			//! \returns true, if it reached the last variable, and false otherwise
			XAIT_CONTROL_DLLAPI bool reachedEnd();

			//! \brief Copy constructor.
			//! \param rhs		the other variable iterator
			FSMInstanceVariableIterator(const FSMInstanceVariableIterator& rhs)
			{
				mIterator = rhs.mIterator;
				mFSMMemory = rhs.mFSMMemory;
				mInstanceID = rhs.mInstanceID;
			}


		private:
			Common::Container::LookUp<Common::String,uint16>::ConstIterator mIterator;
			const FSMMemory*												mFSMMemory;
			FSMInstanceID													mInstanceID;
			
			//! \brief Constructor.
			XAIT_CONTROL_DLLAPI FSMInstanceVariableIterator(const FSMMemory* fsmMemory, FSMInstanceID instanceID);
		
			bool checkVisibleVariable();
			bool isCurrentElementInternal();
		};

		//! \brief Iterator providing access to variable informations.
		class FSMVariableIterator
		{
			friend class FSM;
			friend class FSMInstance;

		public:

			XAIT_CONTROL_DLLAPI FSMVariable getElement() const; 

			XAIT_CONTROL_DLLAPI FSMVariable operator*() const;

			//! \brief Sets the iterator to the next element, if it isn't already the last one
			//! \returns true if it was NOT the last one, and false otherwise
			XAIT_CONTROL_DLLAPI bool getNext();

			XAIT_CONTROL_DLLAPI FSMVariableIterator& operator++();

			//! \brief Checks whether the variable iterator reached the last variable.
			//! \returns true, if it reached the last variable, and false otherwise
			XAIT_CONTROL_DLLAPI bool reachedEnd();

			//! \brief Copy constructor.
			//! \param rhs		the other variable iterator
			FSMVariableIterator(const FSMVariableIterator& rhs)
			{
				mIterator = rhs.mIterator;
				mFSMMemory = rhs.mFSMMemory;
			}


		private:
			Common::Container::LookUp<Common::String,uint16>::ConstIterator mIterator;
			const FSMMemory*												mFSMMemory;

			//! \brief Constructor.
			XAIT_CONTROL_DLLAPI FSMVariableIterator(const FSMMemory* fsmMemory);

			bool checkVisibleVariable();
			bool isCurrentElementInternal();
		};


		class FSMFunctionIterator
		{
			friend class FSM;
			friend class FSMInstance;

		public:

			XAIT_CONTROL_DLLAPI FSMFunction getElement() const; 

			XAIT_CONTROL_DLLAPI FSMFunction operator*() const;

			//! \brief Sets the iterator to the next element, if it isn't already the last one
			//! \returns true if it was NOT the last one, and false otherwise
			XAIT_CONTROL_DLLAPI bool getNext();

			XAIT_CONTROL_DLLAPI FSMFunctionIterator& operator++();

			//! \brief Checks whether the variable iterator reached the last variable.
			//! \returns true, if it reached the last variable, and false otherwise
			XAIT_CONTROL_DLLAPI bool reachedEnd();

			//! \brief Copy constructor.
			//! \param rhs		the other variable iterator
			FSMFunctionIterator(const FSMFunctionIterator& rhs)
			{
				mIterator = rhs.mIterator;
				mFSM = rhs.mFSM;
				mInstance = rhs.mInstance;
				mFunctionRegistrar = rhs.mFunctionRegistrar;
			}

		private:
			Common::Container::Vector<Common::Reflection::Function::FunctionID>::ConstIterator   mIterator;
			const FSM*			mFSM;
			const FSMInstance*	mInstance;
			const Common::Reflection::Function::FunctionRegistrar* mFunctionRegistrar;

			//! \brief Constructor.
			XAIT_CONTROL_DLLAPI FSMFunctionIterator(const FSM* fsm);
			XAIT_CONTROL_DLLAPI FSMFunctionIterator(const FSMInstance* instance);

		};

		class FSMIterator
		{
			friend class FSMSet;
		public:
			XAIT_CONTROL_DLLAPI FSM* getElement() const;

			XAIT_CONTROL_DLLAPI FSM* operator*() const;

			//! \brief sets the iterator to the next element, if it isn't already the last one
			//! \returns true if it was NOT the last one, and false otherwise
			XAIT_CONTROL_DLLAPI bool getNext();

			XAIT_CONTROL_DLLAPI FSMIterator& operator++();

			//! \brief checks whether the variable iterator reached the last variable.
			//! \returns true, if it reached the last variable, and false otherwise
			XAIT_CONTROL_DLLAPI bool reachedEnd() const;


			//! \brief Copy constructor.
			//! \param rhs		the other variable iterator
			FSMIterator(const FSMIterator& rhs)
			{
				mIterator = rhs.mIterator;
				mEndIterator = rhs.mEndIterator;
			}

		private:
			XAIT_CONTROL_DLLAPI FSMIterator(const Common::Container::LookUp<Common::String, FSMID>::ConstIterator& iterator, const Common::Container::LookUp<Common::String, FSMID>::ConstIterator& endIterator);

			Common::Container::LookUp<Common::String,FSMID>::ConstIterator mIterator;
			Common::Container::LookUp<Common::String,FSMID>::ConstIterator mEndIterator;
		};

		class FSMInstanceIterator
		{
			friend class FSM;
		public:

			XAIT_CONTROL_DLLAPI FSMInstance* getElement() const;

			XAIT_CONTROL_DLLAPI FSMInstance* operator*() const;

			//! \brief sets the iterator to the next element, if it isn't already the last one
			//! \returns true if it was NOT the last one, and false otherwise
			XAIT_CONTROL_DLLAPI bool getNext();

			XAIT_CONTROL_DLLAPI FSMInstanceIterator& operator++();

			//! \brief checks whether the variable iterator reached the last variable.
			//! \returns true, if it reached the last variable, and false otherwise
			XAIT_CONTROL_DLLAPI bool reachedEnd() const;

			//! \brief Copy constructor.
			//! \param rhs		the other variable iterator
			FSMInstanceIterator(const FSMInstanceIterator& rhs)
			{
				mIterator = rhs.mIterator;
				mEndIterator = rhs.mEndIterator;
			}

		private:
			XAIT_CONTROL_DLLAPI FSMInstanceIterator(const Common::Container::LookUp<Common::String, FSMInstanceID>::ConstIterator& iterator, const Common::Container::LookUp<Common::String, FSMInstanceID>::ConstIterator& endIterator);

			Common::Container::LookUp<Common::String,FSMInstanceID>::ConstIterator mIterator;
			Common::Container::LookUp<Common::String,FSMInstanceID>::ConstIterator mEndIterator;
		};
	}
}
