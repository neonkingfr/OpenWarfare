// (C) xaitment GmbH 2006-2012

#pragma once

#define CURRENT_FILE_FORMAT 40
#define XAIT_CONTROL_DEBUGGER_VERSION 40
#define XAIT_CONTROL_SERIALIZE_VERSION 40

// this warning can occur by intensive template specialization
// according to MSDN:"It is possible to ship an application that generates C4503"
#pragma warning(disable:4503)