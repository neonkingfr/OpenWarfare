// (C) xaitment GmbH 2006-2012

#pragma once 

#include <xait/control/ControlMemoryPool.h>
#include <xait/common/memory/StaticMemoryManaged.h>
#include <xait/common/memory/StaticSTLAllocator.h>
#include <xait/common/container/Vector.h>

namespace XAIT
{
	namespace Control
	{
		class ControlMemManagedObject : public Common::Memory::StaticMemoryManaged<ControlMemoryPool::getLowAllocThreadSafeAllocator>
		{

		};

		class NonVirtualControlMemManagedObject : public Common::Memory::StaticMemoryManaged<ControlMemoryPool::getLowAllocThreadSafeAllocator,Common::Memory::NonVirtualBaseClass>
		{

		};

		template<typename T_ELEMENT>
		class StaticManagedContainer
		{
		public:
			typedef Common::Container::Vector<T_ELEMENT,Common::Memory::StaticSTLAllocator<T_ELEMENT,ControlMemoryPool::getLowAllocThreadSafeAllocator> >	Vector;
		};

	}
}