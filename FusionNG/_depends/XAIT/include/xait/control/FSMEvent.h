// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/control/ControlStaticMemoryManaged.h>

namespace XAIT
{
	namespace Common
	{
		namespace Parser
		{
			namespace XML
			{
				class XMLNodeElements;
			}
		}
	}

	namespace Control
	{
		class FSMEvent : public ControlMemManagedObject
		{
			friend class FSMManager;
			friend class FSMSet;
			friend class FSM;
			friend class FSMInstance;
			friend class ControlDatatypesInitHelper;

		public:

			//! \brief gets the full name of the event (namespace included)
			//! \returns the full name of the event
			XAIT_CONTROL_DLLAPI const Common::String getFullName() const { return mName; }

			//! \brief gets the name of the event 
			//! \returns the name of the event
			XAIT_CONTROL_DLLAPI const Common::String getName() const;
			
			//! \brief gets the namespace of the event
			//! \returns the namespace of the event
			XAIT_CONTROL_DLLAPI const Common::String getNameSpace() const;
			
			//! \brief gets the id of the event
			//! \returns the id of the event
			//XAIT_CONTROL_DLLAPI const FSMEventID getEventID() const {return mEventID;}
			
			//! \brief gets the signature of the event
			//! \returns the signautre of the event
			XAIT_CONTROL_DLLAPI const Common::Container::Vector<Common::Reflection::Datatype::DatatypeID>& getSignature() const {return mSignature;}
			
			//! \brief gets the names of the event parameters
			//! \returns the names of the event parameters
			XAIT_CONTROL_DLLAPI const Common::Container::Vector<Common::String>& getParameterNames() const {return mParameterNames;}

			//! \brief checks if this event is valid
			//! \returns true if valid
			XAIT_CONTROL_DLLAPI bool isValid() const;

		private:
			Common::String	mName;
			FSMEventID		mEventID;
			Common::Container::Vector<Common::Reflection::Datatype::DatatypeID> mSignature;
			Common::Container::Vector<Common::String> mParameterNames;
			
			FSMEvent();
			bool initFromXML(Common::Parser::XML::XMLNodeElements* node);
			const FSMEventID getEventID() const {return mEventID;}
		};
	}
}