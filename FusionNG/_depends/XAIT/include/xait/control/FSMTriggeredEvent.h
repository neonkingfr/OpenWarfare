// (C) xaitment GmbH 2006-2012

#pragma once

#include <xait/control/FSMTypedefs.h>

namespace XAIT
{
	namespace Common
	{
		namespace Reflection
		{
			namespace Function
			{
				class FunctionArgumentList;
			}
		}
	}

	namespace Control
	{		
		struct FSMTriggeredEvent
		{
			FSMTriggeredEvent(FSMEventID eventID, const Common::Reflection::Function::FunctionArgumentList& parameters);
			FSMTriggeredEvent(const FSMTriggeredEvent& other);

			FSMEventID											mEventID;
			Common::Reflection::Function::FunctionArgumentList	mEventParameters;
		};
	}
}
