
 /**************************************************************************
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 **************************************************************************/

/*****************************************************************************

Name:

Purpose:

	
Version Information:

	Version		Date		Author and Comments
	===========================================
	1.0			00-00/2011	YFP: Original Implementation

/*****************************************************************************/

#ifndef VBS2FUSION_DRAW_PRIMITIVEDRAW_H
#define VBS2FUSION_DRAW_PRIMITIVEDRAW_H


/*****************************************************************************/
/* INCLUDES
/*****************************************************************************/
// STANDARD INCLUDES
#include <vector>
// SIMCENTRIC INCLUDES
#include "VBS2Fusion.h"
#include "VBS2FusionDraw.h"
#include "position3D.h"
#include "math/Vector3f.h"
#include "math/spline.h"
#include "VBS2FusionDefinitions.h"

//#include "data/FD_Polygon.h"
/*****************************************************************************/
/* END INCLUDES
/*****************************************************************************/

namespace VBS2Fusion
{
	namespace Draw
	{
		class VBS2FUSION_DRAW_API PrimitiveDraw
		{
		public:

		 /*!
		 Draw a Filled PolygonBox with given parameters
		 */
		 static void DrawPolygonBoxFill(vector<position3D>& positions, float height, ColorRGBA_FD color,string name);
		
	 
		 /*!
		 Draw a Wired PolygonBox(outline) with given parameters
		 */
		 static void DrawPolygonBoxOutline(vector<position3D>& positions, float height, ColorRGBA_FD color,string name);					
		 
		 /*!
		 Draw a Filled Polygon with given parameters
		 */
		 static void DrawPolygonFill(vector<position3D>& positions,ColorRGBA_FD color,string name);

		 /*!
		 Draw a Filled Triangle with given parameters
		 */
		 static void DrawTriangleFill(position3D pointA, position3D pointB, position3D pointC, ColorRGBA_FD color,string name);

		 /*!
		 Draw a Filled RectAngle with given parameters
		 pointA - starting point
		 normalVec - perpendicular vector to the Rectangle
		 height - height or length of Rectangle
		 width - width of Rectanlge
		 */
		 static void DrawRectangleFill(position3D pointA, vector3D normalVec, float height, float width, ColorRGBA_FD color,string name);

		 /*!
		 Draw a Wired RectAngle with given parameters
		 pointA - starting point
		 normalVec - perpendicular vector to the Rectangle
		 height - height or length of Rectangle
		 width - width of Rectanlge
		 */
		 static void DrawRectangle(position3D pointA, vector3D normalVec, float height, float width, ColorRGBA_FD color,string name);

		 /*!
		 Draw Line Strip. Drawing the straight lines, which connects the given positions.
		 */
		 static void DrawLineStrip(vector<position3D> points, ColorRGBA_FD color, string name);

		 /*!
		 Draw a Solid Sphere with given parameters
		 */
		 static void DrawSphere(position3D center, float radius, ColorRGBA_FD color,string name, int sliceCount, int stackCount );

		 /*!
		 Draw a Solid Cylinder with given parameters
		 */
		 static void DrawCylinderFill(position3D center, float radius1, float radius2, float lenght, ColorRGBA_FD color, string name, int sliceCount, int stackCount);

		 /*!
		 Draw a wired Triangle with given parameters
		 */
		 static void DrawTriangle(position3D pointA, position3D pointB, position3D pointC, ColorRGBA_FD color,string name);

		 /*!
		 Draw a wired Circle with given parameters
		 */
		 static void DrawCircle(position3D centre, float radius, Vector3f normalVector, ColorRGBA_FD color,string name);

		 /*!
		 Draw a line with given parameters
		 */
		 static void DrawLine(position3D start, position3D end,ColorRGBA_FD color, string name);

		 /*!
		 Set the Drawn Object Visibility status
		 name - object name
		 isVisible - visibility Status
		 */
		 static void SetVisible(string name, bool isVisible);

		 /*!
		 Set the Drawn Object Visibility status according to the given APPLICATIONSTATE. Simply this function
		 will hide the drawn object if both given application state and the VBS application state are same.
		 name - object name
		 APPLICATIONSTATE - application state where object needs to be hidden.
		 */
		 static void SetDisableAppState(string name, APPLICATIONSTATE appState);
		 
		 /*!
		 Delete a Drawn Object.
		 */
		 static void DeletePrimitive(string name);
		 
		 /*!
		 Draw a Filled Circle with given parameters
		 */
		 static void DrawCircleFill(position3D centre, float radius, Vector3f normalVector, ColorRGBA_FD color,string name);

#ifdef DEVELOPMENT
		 /*!
		 Rotate a given drawn object around X axis
		 */
		 static bool RotatePrimitiveX(string name , float angle);

		 /*!
		 Rotate a given drawn object around Y axis
		 */
		 static bool RotatePrimitiveY(string name , float angle);
		 
		 /*!
		 Rotate a given drawn object around Z axis
		 */
		 static bool RotatePrimitiveZ(string name , float angle);

		 //static void DrawTexture();

		 /*!
		 The object will be selected and assign to object reference, if the object name is exist
		 object - Object reference
		 name - Name of the existing (created) object
		 */
		 //static bool SelectObject(FDObject* object, string name);
#endif

		 /*!
		 Transform the given object by given position.
		 position = Decided position - current position. 
		 */
		 static bool SetPrimitiveTransform(string name, position3D position);

		 /*!
		 Change the Object color. 
		 */
		 static bool SetPrimitiveColor(string name, ColorRGBA_FD color);
		 /*!
		 Draw a Box with Given parameters.
		 The Box will be drawn as paralleled to x,y,z axis, if the angleRotX=0 and angleRotY=0
		 startPosition - start corner position of the box
		 angleRotX - Rotate the Object related to X axis(in degree).
		 angleRotY - Rotate the Object related to Y axis(in degree).
		 angleRotZ - Rotate the Object related to Z axis(in degree).
		 */
		 static void DrawBoxFill(position3D startPosition, float length, float width, float height, float angleRotX, float angleRotY, float angleRotZ, ColorRGBA_FD color, string name);

		};
	}
}


#endif //VBS2FUSION_DRAW_PRIMITIVEDRAW_H