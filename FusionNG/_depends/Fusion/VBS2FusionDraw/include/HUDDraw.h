 /**************************************************************************
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 **************************************************************************/

/*****************************************************************************

Name:

Purpose:

	
Version Information:

	Version		Date		Author and Comments
	===========================================
	1.0			00-00/2011	YFP: Original Implementation

/*****************************************************************************/

#ifndef VBS2FUSION_DRAW_HUDDRAW_H
#define VBS2FUSION_DRAW_HUDDRAW_H


/*****************************************************************************/
/* INCLUDES
/*****************************************************************************/
// STANDARD INCLUDES
#include <vector>
// SIMCENTRIC INCLUDES
#include "VBS2Fusion.h"
#include "VBS2FusionDraw.h"

/*****************************************************************************/
/* END INCLUDES
/*****************************************************************************/

namespace VBS2Fusion
{
	namespace Draw
	{
		class VBS2FUSION_DRAW_API HUDDraw
		{
		public:

			/*!
			Draw a line of given "color" from "start" point to "end" point. 
			Given "name" should be unique, unless the line will not draw.
			The centre point of VBS2 screen is considered as (0,0),
			left-bottom as (-1,-1) and top-right as (1,1).
			Thus range of X and Y axes varies from (-1) to 1.
			Drawn line will stretch and skew based on the VBS2 window dimensions. 
			*/
			static void DrawLine(Point2D start, Point2D end, ColorRGBA_FD color, std::string name);

			/*!
			Draw a rectangle of given "color" from "leftBottom" point with the given "height" and "width".
			"height" and "width" are considered as a ratios to VBS2 window dimensions.
			Thus drawn rectangle will stretch and skew based on the VBS2 window dimensions.
			Given "name" should be unique, unless the rectangle will not draw.
			The centre point of VBS2 screen is considered as (0,0),
			left-bottom as (-1,-1) and top-right as (1,1).
			Thus range of X and Y axes varies from (-1) to 1.			 
			*/
			static void DrawRectangle(Point2D leftBottom, float height, float width, ColorRGBA_FD color, std::string name);

			/*!
			Draw a circle of given "color" around the "centre" with the given "radius".
			"radius" is the initial radius (in pixels) for the drawn circle. 
			Once the circle has been drawn, it will reshape when the VBS2 window is resized. Use "keepShape" as true otherwise.
			Given "name" should be unique, unless the circle will not draw.
			The centre point of VBS2 screen is considered as (0,0),
			left-bottom as (-1,-1) and top-right as (1,1).
			Thus range of X and Y axes varies from (-1) to 1.
			Once the circle is drawn, it will stretch and skew based on the VBS2 window dimensions. 
			*/
			static void DrawCircle(Point2D centre, float radius, ColorRGBA_FD color, std::string name, bool keepShape = false);
			
			/*!
			Draw an image from "leftBottom" point with the given "height" and "width". 
			"height" and "width" are considered as a ratios to VBS2 window dimensions.
			Thus drawn image will stretch and skew based on the VBS2 window dimensions.
			Given "name" should be unique, unless the circle will not draw.
			The centre point of VBS2 screen is considered as (0,0),
			left-bottom as (-1,-1) and top-right as (1,1).
			Thus range of X and Y axes varies from (-1) to 1. 
			*/
			static void DrawImage(Point2D leftBottom, float height, float width, string filePath, std::string name);

			/*!
			Draw the text of type "fontType" and color "color" on a horizontal line 
			which is placed "fontSize" pixels below the "start" point and towards the right side of the screen.
			Given "name" should be unique, unless the circle will not draw.
			The centre point of VBS2 screen is considered as (0,0),
			left-bottom as (-1,-1) and top-right as (1,1).
			Thus range of X and Y axes varies from (-1) to 1. 
			Do not use the starting point of x coordinate as 1 and y as -1. It wont show any text on the screen.
			*/
			static void DrawTextObject(Point2D start, string strText, string fontType, int fontSize, ColorRGBA_FD color, std::string name);

			/*!
			Delete the HUD object by the given name.
			*/
			static void DeleteObject(std::string name);

			/*!
			Changes the already drawn HUD object's color by giving the id name of the desired object.
			*/
			static bool ChangeObjectColor(string name, ColorRGBA_FD color);

			/*!
			Shifts the already drawn HUD object's by the given offset position. HUD object will be identified 
			through the name.
			*/
			static bool TransformObject(string name, Point2D offset);

			/*!
			Changes the already drawn HUD_Text object's displaying string by the strText. HUD_Text object will be 
			identified through the name.
			*/
			static bool ChangeTextObjectString(string name, string strText);

			/*!
			Changes the already drawn HUD_Text object's displaying string font type by the parameter fontType. 
			HUD_Text object will be identified through the name.
			*/
			static bool ChangeTextObjectFontType(string name, string fontType);

			/*!
			Changes the already drawn HUD_Text object's displaying string font size by the parameter fontSize. 
			HUD_Text object will be identified through the name.
			*/
			static bool ChangeTextObjectFontSize(string name, int fontSize);

		};
	}
}


#endif //VBS2FUSION_DRAW_PRIMITIVEDRAW_H