
 /**************************************************************************
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 **************************************************************************/

/*****************************************************************************

Name:  vbshudplugin.h

Version Information: 

	Version		Date		Author and Comments
	===========================================
	1.0			15-04/2010	YFP: Original Implementation

/*****************************************************************************/

#ifndef VBS2FUSIONHUD_VBSPLUGIN_H
#define VBS2FUSIONHUD_VBSPLUGIN_H

/*****************************************************************************/
/* INCLUDES
/*****************************************************************************/
// WINDOWS INCLUDES
#include <windows.h>
// STANDARD INCLUDES

// SIMCENTRIC INCLUDES

/*****************************************************************************/
/* END INCLUDES
/*****************************************************************************/

#ifdef VBS2FUSION_HUD_EXPORTS
#define VBS2FUSION_HUD_API __declspec(dllexport)
#else
#define VBS2FUSION_HUD_API __declspec(dllimport)
#endif



VBS2FUSION_HUD_API void WINAPI setDirect3DDevice(IDirect3DDevice9* device);

VBS2FUSION_HUD_API void WINAPI onDestroyD3DDevice();

VBS2FUSION_HUD_API void WINAPI onInvalidateD3DDevice();

VBS2FUSION_HUD_API void WINAPI onRestoreD3DDevice();


#endif  //VBS2FUSIONHUD_VBSPLUGIN_H