
 /**************************************************************************
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 **************************************************************************/

/*****************************************************************************

Name:	Line.h 

Purpose:	This file contains the declaration of the Line class.

	
Version Information:

	Version		Date		Author and Comments
	===========================================
	1.0			15-04/2010	YFP: Original Implementation
	1.01		21-04/2010

/*****************************************************************************/

#ifndef VBS2FUSIONHUD_LINE_H
#define VBS2FUSIONHUD_LINE_H

/*****************************************************************************/
/* INCLUDES
/*****************************************************************************/
// STANDARD INCLUDES

// SIMCENTRIC INCLUDES
#include "ShapeObject.h"
/*****************************************************************************/
/* END INCLUDES
/*****************************************************************************/

namespace VBS2Fusion
{
	namespace HUD
	{
		class VBS2FUSION_HUD_API Line : public ShapeObject
		{
		public:
		
			Line();

			/*!
			Main constructor for this class. The following parameters are initialized as follows:
			_color = WHITE ( Red = 255, Green =255, Blue = 255, Alpha = 255 )
			_thickness = 1
			*/
			Line(Point2D startPos , Point2D endPos);

			/*!
			Main destructor for the class.
			*/
			~Line();

			/*!
			create line resources. set default line draw pattern. 
			*/
			void create();

			/*!
			render Line on the screen.
			*/
			void render();

			/*!
			release Line drawing resources.
			*/
			void release();

			/*!
			set Color for line.
			*/
			void setColor(ColorRGBA color);

			/*!
			set thickness of the line.
			*/
			void setThickness(float thickness);

			/*!
			set line start position.
			*/
			void setStartPosition(Point2D position);

			/*!
			set line end position.
			*/
			void setEndPosition(Point2D position);


		private:
			Point2D _startPos;
			Point2D _endPos;
			ColorRGBA _color;
			float _thickness;

			ID3DXLine *_DXline;

			IDirect3DDevice9 *_device;

		};
	}
}


#endif