
 /**************************************************************************
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 **************************************************************************/

/*****************************************************************************

Name:	StatusBar.h

Purpose:	This file contains the declaration of the StatusBar class.

	
Version Information:

	Version		Date		Author and Comments
	===========================================
	1.0			15-04/2010	YFP: Original Implementation

/*****************************************************************************/


#ifndef VBS2FUSIONHUD_STATUSBAR_H
#define VBS2FUSIONHUD_STATUSBAR_H

/*****************************************************************************/
/* INCLUDES
/*****************************************************************************/
// STANDARD INCLUDES
#include <string>

// SIMCENTRIC INCLUDES
#include "UIObject.h"
#include "Rectangle.h"
#include "TextObject.h"
#include "Line.h"
/*****************************************************************************/
/* END INCLUDES
/*****************************************************************************/
using namespace std;
namespace VBS2Fusion
{
	namespace HUD
	{
		class VBS2FUSION_HUD_API StatusBar : public UIObject
		{
		public:
			StatusBar();
			/*!
			Main constructor for this class. The following parameters are initialized as follows:
			_fillPercentage = 0;
			_caption = "";
			_width = 250; 
			_height= 100;
			_MaxBarLength = 200;
			*/
			StatusBar(Point2D pos);

			/*!
			Destructor for this class.
			*/
			~StatusBar();

			/*!
			Create status bar draw resources. 
			*/
			void create();

			/*!
			Render status bar on the screen.
			*/
			void render();

			/*!
			Release status bar draw resources.
			*/
			void release();

			/*!
			set caption of the status bar.
			*/
			void setCaption(string caption);

			/*!
			set fill percentage. This percentage value should be between 0 - 100. 
			*/
			void setPercentage(float percentage);


		private:
			Point2D _screenPos;
			float _width, _height;
			float _fillPercentage;
			string _caption;

			float _MaxBarLength;


		};

	}
}

#endif //VBS2FUSIONHUD_STATUSBAR_H