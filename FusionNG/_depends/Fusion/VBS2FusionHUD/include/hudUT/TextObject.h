
/**************************************************************************
* Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
* Reserved.
*
* COPYRIGHT/OWNERSHIP
* This Software and its source code are proprietary products of SimCentric and
* are protected by copyright and other intellectual property laws. The Software
* is licensed and not sold.
*
* USE OF SOURCE CODE
* This source code may not be used, modified or copied without the expressed,
* written permission of SimCentric.
*
* RESTRICTIONS
* You MAY NOT: (a) copy and distribute the Software or any portion of it;
* (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
* or permit reverse engineering, disassembly, decompilation or alteration of
* this Software; (d) remove any product identification, copyright notices, or
* other notices or proprietary restrictions from this Software; (e) copy the
* documentation accompanying the software.
*
* DISCLAIMER OF WARRANTIES
* The Software is supplied "AS IS". SimCentric disclaims all warranties,
* expressed or implied, including, without limitation, the warranties of
* merchantability and of fitness for any purpose. The user must assume the
* entire risk of using the Software.
*
* DISCLAIMER OF DAMAGES
* SimCentric assumes no liability for damages, direct or consequential, which
* may result from the use of the Software, even if SimCentric has been advised
* of the possibility of such damages. Any liability of the seller will be
* limited to refund the purchase price.
*
**************************************************************************/

/*****************************************************************************

Name:	TextObject.h

Purpose:	This file contains the declaration of the TextObject class.


Version Information:

Version		Date		Author and Comments
===========================================
1.0			15-04/2010	YFP: Original Implementation

/*****************************************************************************/

#ifndef VBS2FUSIONHUD_TEXTOBJECT_H
#define VBS2FUSIONHUD_TEXTOBJECT_H


/*****************************************************************************/
/* INCLUDES
/*****************************************************************************/
// STANDARD INCLUDES
#include <string>
// SIMCENTRIC INCLUDES
#include "DrawObject.h"
/*****************************************************************************/
/* END INCLUDES
/*****************************************************************************/
using namespace std;
namespace VBS2Fusion
{
	namespace HUD
	{
		class VBS2FUSION_HUD_API TextObject : public DrawObject
		{
		public:

			TextObject();
			/*!
			Main constructor for this class. The following parameters are initialized as follows:
			_strFont = "Arial"
			_color   = WHITE  ( Red = 255, Green =255, Blue = 255, Alpha = 255 )

			fontHeightSize is the font height size of the text (normal font size).
			fontWidthSize is the font width size of the text.
			*/
			TextObject(Point2D topLeftPos,Point2D bottomRightPos ,string caption,int fontHeightSize , int fontWidthSize);

			/*!
			Destructor for TextObject class.
			*/
			~TextObject();

			/*!
			Create and Initialize Textobject draw resources.
			*/
			void create();

			/*!
			Render TextObject on the screen.
			*/
			void render();

			/*!
			Release TextObject draw resources. 
			*/
			void release();

			/*!
			set specified font. user need to provide a valid font name.
			*/
			void setFont(string fontName);

			/*!
			set specified color.
			*/
			void setColor(ColorRGBA color);

			/*!
			set specified text to draw on the screen.
			*/
			void setText(string text);

			/*!
			set Top left position of the Text object.
			*/
			void setTopLeftPosition(Point2D position);

			/*!
			set Bottom right position of the rectangle.
			*/
			void setBottomRightPosition(Point2D position);

			/*!
			set Font Height Size. This is the normal font size.
			*/
			void setFontHeightSize(int heightSize);

			/*!
			set Font Width Size. 
			*/
			void setFontWidthSize(int widthSize);

		private:
			Point2D _screenPos;
			Point2D _botomPos;

			ID3DXFont *_dxFont;
			string _strFont;
			ColorRGBA _color;
			string _caption;
			IDirect3DDevice9 *_device;

			int _ifontHeight;
			int _ifontWidth;


		};
	}
}

#endif	//VBS2FUSIONHUD_TEXTOBJECT_H