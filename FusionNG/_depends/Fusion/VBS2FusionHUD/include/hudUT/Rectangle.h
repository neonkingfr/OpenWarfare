
 /**************************************************************************
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 **************************************************************************/

/*****************************************************************************

Name:	Rectangle.h

Purpose:	This file contains the declaration of the Rectangle class.

	
Version Information:

	Version		Date		Author and Comments
	===========================================
	1.0			15-04/2010	YFP: Original Implementation

/*****************************************************************************/

#ifndef VBS2FUSIONHUD_RECTANGLE_H
#define VBS2FUSIONHUD_RECTANGLE_H

/*****************************************************************************/
/* INCLUDES
/*****************************************************************************/
// STANDARD INCLUDES

// SIMCENTRIC INCLUDES
#include "ShapeObject.h"
/*****************************************************************************/
/* END INCLUDES
/*****************************************************************************/
namespace VBS2Fusion
{
	namespace HUD
	{
		class VBS2FUSION_HUD_API Rectangle : public ShapeObject
		{
		public:

			Rectangle();

			/*!
			Main constructor for this class. The following parameters are initialized as follows:
			_thickness  = 1
			_color		= WHITE ( Red = 255, Green =255, Blue = 255, Alpha = 255 )
			*/
			Rectangle(Point2D topLeft, Point2D bottomRight);

			/*!
			Destructor for this class.
			*/
			~Rectangle();

			/*!
			Create rectangle draw resources.
			*/
			void create();

			/*!
			Render rectangle on the screen.
			*/
			void render();

			/*!
			Release draw resources of the rectangle.
			*/
			void release();

			/*!
			set specified color.
			*/
			void setColor(ColorRGBA color);

			/*!
			set specified thickness.
			*/
			void setThickness(float thickness);

			/*!
			set Top left position of the rectangle.
			*/
			void setTopLeftPosition(Point2D position);

			/*!
			set Bottom right position of the rectangle.
			*/
			void setBottomRightPosition(Point2D position);


		private:
			Point2D _topLeft;
			Point2D _bottomRight;
			ColorRGBA _color;
			float _thickness;

			ID3DXLine *_line;

		};
	}
}



#endif //VBS2FUSIONHUD_RECTANGLE_H