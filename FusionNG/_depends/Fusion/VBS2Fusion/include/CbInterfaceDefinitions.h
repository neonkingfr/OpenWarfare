/**************************************************************************
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 **************************************************************************/

/*************************************************************************
Name:		VBS2FusionCallBackDefinitions
Purpose:	This file contains VBS2Fusion Global ENUM definitions which are used in callbacks


Version Information:

Version		Date		Author and Comments
===========================================
1.00		02-05-2012	RDJ: Original Implementation

/************************************************************************/

#ifndef CBINTERFACEDEFINITIONS_H
#define CBINTERFACEDEFINITIONS_H
#include <string>
#include <d3d9.h>
#include <d3dx9.h>

/**********************************************************************/
/* END INCLUDES
/**********************************************************************/
using namespace std;

namespace VBS2Fusion
{	
	interface VBS2CBInterface
	{
		virtual void SetTexture(DWORD Sampler, IDirect3DBaseTexture9 * pTexture)=0;
		virtual void SetRenderTarget(DWORD RenderTargetIndex, IDirect3DSurface9* pRenderTarget)=0;
		virtual void DrawPrimitive(D3DPRIMITIVETYPE PrimitiveType, UINT StartVertex, UINT PrimitiveCount)=0;
		virtual void SetPixelShader(IDirect3DPixelShader9* pShader)=0;
		virtual void SetVertexShader(IDirect3DVertexShader9* pShader)=0;
		virtual void SetVertexDeclaration(IDirect3DVertexDeclaration9* pDecl)=0;
		virtual void BeginScene()=0;
		virtual void EndScene()=0;
		virtual void SetStreamSource(UINT StreamNumber, IDirect3DVertexBuffer9 * pStreamData, UINT OffsetInBytes, UINT Stride)=0;
		virtual void DrawPrimitiveUP(D3DPRIMITIVETYPE PrimitiveType, UINT PrimitiveCount, CONST void* pVertexStreamZeroData, UINT VertexStreamZeroStride)=0;
		virtual void SetRenderState(D3DRENDERSTATETYPE State, DWORD Value)=0;
		virtual void SetSamplerState(DWORD Sampler, D3DSAMPLERSTATETYPE Type, DWORD Value)=0;
		virtual void SetIndices(IDirect3DIndexBuffer9 * pIndexData)=0;
		virtual void Clear(DWORD Count, CONST D3DRECT * pRects, DWORD Flags, D3DCOLOR Color, float Z, DWORD Stencil)=0;
		virtual void SetVertexShaderConstantB(UINT StartRegister, CONST BOOL * pConstantData, UINT BoolCount)=0;
		virtual void SetVertexShaderConstantF(UINT StartRegister, CONST float * pConstantData, UINT Vector4fCount)=0;
		virtual void SetVertexShaderConstantI(UINT StartRegister, CONST int * pConstantData, UINT Vector4iCount)=0;
		virtual void SetPixelShaderConstantB(UINT StartRegister, CONST BOOL * pConstantData, UINT BoolCount)=0;
		virtual void SetPixelShaderConstantF(UINT StartRegister, CONST float * pConstantData, UINT Vector4fCount)=0;
		virtual void SetPixelShaderConstantI(UINT StartRegister, CONST int * pConstantData, UINT Vector4iCount)=0;
		virtual void DrawIndexedPrimitive(D3DPRIMITIVETYPE Type, INT BaseVertexIndex, UINT MinIndex, UINT NumVertices, UINT StartIndex, UINT PrimitiveCount)=0; // Available in 91072
	};

	// Possible draw passes used by the engine (used with onDrawnPass)
	enum drawPassIdentifier
	{
		drawPass1ON = 0, // Near by opaque objects
		drawPass1OF = 1, // Distant opaque objects
		drawPass1AF = 2, // Distant objects with some alpha
		drawPass1AN = 3, // Near by objects with some alpha
		drawPass2 = 4,  // Significantly alpha transparent objects like smoke particles
		drawPass3 = 5,  // Player body + vehicle pass, has its own depth range
		drawPass4 = 6,  // Compass, watch, etc. 3D objects. Has its own camera + depth range
		drawPassMapBg = 7, // Map background, before grid lines and other map elements are drawn
		drawPassMap = 8, // End of map rendering
	};

	// Draw mode identifier
	enum drawModeIdentifier
	{
		drawModeCommon = 0,         // Common rendering
		drawModeShadowBuffer = 1,   // Shadow buffer pass
		drawModeZPrime = 2,         // Z-priming
		drawModeDPrime = 3,         // Depth priming
		drawModeCrater = 4,         // Craters rendering (bullet holes)
		drawModeCaustics = 5,       // Underwater caustics pass
	};

	struct paramPreDraw
	{
		IDirect3DDevice9 *dev;  // D3D device, can be used only during this function call
	};

	struct paramInitDraw
	{
		IDirect3DDevice9 *dev;  // D3D device, can be used only during this function call
		__int32 renderWidth;    // Width of render area
		__int32 renderHeight;   // Height of render area
		float eyeAccom;         // Eye accomodation value
		float deltaT;           // Time in seconds since last frame
	};

	struct paramDeviceInit
	{
		IDirect3DDevice9 *dev;  // D3D device, can be used only during this function call
	};

	struct paramDeviceInvalidate
	{
		IDirect3DDevice9 *dev;  // D3D device, can be used only during this function call
		bool isReset;           // true = device is reseted and will be restored later. false = device is being destroyed
	};

	struct paramDeviceRestore
	{
		IDirect3DDevice9 *dev;  // D3D device, can be used only during this function call
	};

	struct paramDrawnPass
	{
		VBS2CBInterface *cbi;
		drawPassIdentifier passId; // pass id, ie. pass 1, 2, 3
		drawModeIdentifier modeId; // ZPrime...
		char rttName[64];          // Rendered RTT name, or null string in case of main scene
		IDirect3DSurface9 *renderTarget;
		IDirect3DSurface9 *depthBuffer;
		FrustumSpecNG cameraFrustum;
	};

	struct paramDrawnSky
	{
		VBS2CBInterface *cbi;
		char rttName[64];          // Rendered RTT name, or null string in case of main scene
		IDirect3DSurface9 *renderTarget;
		IDirect3DSurface9 *depthBuffer;
		FrustumSpecNG cameraFrustum;
	};

	struct paramFilledBackbuffer
	{
		VBS2CBInterface *cbi;
		char rttName[64];          // Rendered RTT name, or null string in case of main scene
		IDirect3DSurface9 *renderTarget;
		IDirect3DSurface9 *depthBuffer;
	};

	struct paramPostProcessPre
	{
		VBS2CBInterface *cbi;
		IDirect3DTexture9 *sourceTexture;
		IDirect3DSurface9 *renderTarget;
		int postFxQuality;
	};

	struct paramPostProcessFinal
	{
		VBS2CBInterface *cbi;
		IDirect3DTexture9 *sourceTexture;
		IDirect3DSurface9 *renderTarget;
		int postFxQuality;
	};

	enum rttType
	{
		rttTypeCamera = 0,   // CameraEffect type RTT, controlled with camCreate
		rttTypeMirror = 1,   // Mirror effect type
		rttTypeMFD = 2,      // MFD display
		rttTypePrism = 3,    // Prism effect type (like mirror, but without mirroring)
		rttTypeReflection = 4, // Reflection special type (engine internal use only)
	};

	enum rttSensor
	{
		rttSensorNormal = 0,
		rttSensorNightvision = 1,
		rttSensorThermal = 2,
		rttSensorNvgti = 3,
	};

	struct paramRttAccess
	{
		IDirect3DDevice9 *dev;  // D3D device, can be used only during this function call
		char rttName[64];          // Rendered RTT name, or null string in case of main scene
		int width;                 // RTT width
		int height;                // RTT height
		float aspect;              // RTT aspect ratio
		rttType type;              // RTT type
		rttSensor sensor;          // RTT sensor type  
		IDirect3DTexture9 *rttTexture; // RTT texture
		int lastFrame;             // Last frame number where this RTT was active
		int currentFrame;          // Current frame number  
	};

	struct paramDrawnRTT
	{
		VBS2CBInterface *cbi;
		char rttName[64];          // Rendered RTT name, or null string in case of main scene
		int width;                 // RTT width
		int height;                // RTT height
		float aspect;              // RTT aspect ratio
		rttType type;              // RTT type
		rttSensor sensor;          // RTT sensor type 
	};


}
#endif