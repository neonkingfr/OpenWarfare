/*
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 */

/*************************************************************************

Name:

	ObjectSet.h

Purpose:

	This file contains the declaration of the ObjectSet class.

Version Information:

	Version		Date		Author and Comments
	===========================================
	1.0			12-09/2009	UDW: Original Implementation
	2.0			08-02/2010	UDW: Initial Implementation
	2.01		10-02/2010	MHA: Comments Checked

	
/************************************************************************/
#ifndef VBS2FUSION_OBJECT_SET_H
#define VBS2FUSION_OBJCECT_SET_H

/***********************************************************************/
/* INCLUDES
/***********************************************************************/

// Standard C++ Includes
#include <set>

/***********************************************************************/
/* END INCLUDES
/***********************************************************************/

namespace VBS2Fusion	{

	/*!This class is a adaption of the std::set class into VBS2Fusion. The standard
	set uses a binary tree for storing elements. The items added to the set should
	be unique*/
template <typename _Type, typename _Comp = std::less<_Type>> class ObjectSet
	{	
		
		/*Definition of types so shorter names can be used with convience*/
		public:
			typedef typename std::set<_Type, _Comp>::iterator iterator;
			typedef typename std::set<_Type, _Comp>::const_iterator const_iterator;
			typedef typename std::set<_Type, _Comp> set;

		
		private:
			set _set;

		public:
			/*!The default constructor for the class. The less comparator will be used for comparing*/
			ObjectSet() {
			};

			/*!The default constructor for the class with a user specified constructor*/
			template<typename _Comp>  ObjectSet(_Comp& comp): _set(comp)	{
				
			}

			~ObjectSet() {
			};

			/*!adds a item if it is not in the ObjectSet*/
			template<typename _Type> void add(_Type item)	{
				_set.insert(item);
			}

			/*!removes a item if it is in the ObjectSet*/
			template<typename _Type> void remove(_Type item)	{
				_set.erase(item);
			}

			void clear()
			{
				_set.clear();
			}

			/*!returns the iterator that points to the beginning that will allow traverse
			eventually through all add objects*/
			iterator begin()	{
				return _set.begin();
			}

			/*!returns the end of iterator (can be tested to check whether we have traveled through
			the set)*/
			iterator end()	{
				return _set.end();
			}

			/*!returns an iterator that points to the beginning that will allow traverse
			eventually through all add objects*/
			const_iterator begin() const {
				return _set.begin();
			}

		
			/*!returns the end of iterator (can be tested to check whether we have traveled through
			the set)*/
			const_iterator end() const {
				return _set.end();
			}

			/*!returns the number of items in the set*/
			int size()	{
				return _set.size();
			}

			/*returns a iterator pointing to the item in the set if the element is found
			else pointing to iterator returned by end()*/
			template<typename _Type> iterator find(_Type element)	{
				return _set.find(element);
			}
			
		};  //end of ObjectSet class
	};  //end of VBS2Fusion

#endif //End of #ifndef OBJECT_SET_H