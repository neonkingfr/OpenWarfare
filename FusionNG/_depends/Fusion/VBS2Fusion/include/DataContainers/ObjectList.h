/*
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 */

/*************************************************************************

Name:

	ObjectList.h

Purpose:

	This file contains the declaration of the ObjectList class.

Version Information:

	Version		Date		Author and Comments
	===========================================
	1.0			25-09/2009	RMR: Original Implementation
	2.0			08-02/2010	UDW: Initial Implementation
	2.01		10-02/2010	MHA: Comments Checked
	
/************************************************************************/

#ifndef VBS2FUSION_OBJECTLIST_H
#define VBS2FUSION_OBJECTLIST_H

/***********************************************************************/
/* INCLUDES
/***********************************************************************/

// Standard C++ Includes
#include <string>
#include <cmath>
#include <list>

#include "VBS2Fusion.h"



/***********************************************************************/
/* END INCLUDES
/***********************************************************************/

namespace VBS2Fusion
{
	/*!
	ObjectList is a generic templated class which can be used to store lists of homogeneous objects. For example, it can store a list
	of ControllableObjects using ObjectList<ControllableObject>. 
	*/
	/*!
	ObjectList is a generic templated class which can be used to store lists of homogeneous objects. For example, it can store a list
	of ControllableObjects using ObjectList<ControllableObject>. 
	*/
	template <typename _Type> 
	class  ObjectList
	{
	public:
		typedef typename list<_Type>::iterator iterator;
		typedef typename list<_Type>::const_iterator const_iterator;
		typedef typename list<_Type> TypeList;

	private:

		TypeList _list;

	public:
		/*!
		Primary constructor for the ObjectList class. 
		*/
		ObjectList() {} ;

		/*!
		Primary destructor for the ObjectList class. 
		*/
		~ObjectList() {};

		/*!
		Adds a new item of _type to the end of the list. 
		*/
		template<typename _Type> 
		void addToEnd(_Type item)
		{
			_list.push_back(item);
		}

		/*!
		Adds a new item of _type to the beginning of the list. 
		*/
		template<typename _Type> 
		void addToStart(_Type item)
		{
			_list.push_front(item);
		}

		/*!
		Removes the item at the end of the list. 
		*/
		//template<typename _Type> 
		void removeFromEnd()
		{
			_list.pop_back();
		}

		/*!
		Removes the item at the beginning of the list. 
		*/
		template<typename _Type> 
		void removeFromStart()
		{
			_list.pop_front();
		};

		/*!
		erase the given item from the list
		*/
		template<typename _Type> 
		void erase(_Type item)
		{
			_list.erase(item);
		};

		/*!
		Returns the size of (i.e the number of items on) the list
		*/
		int size(){ return _list.size();};

		/*!
		Clears the list and makes its size 0. 
		*/
		//template<typename _Type> 
		void clear() {_list.clear();};

		/*!
		The begin iterator to traverse through the list. 
		*/
		iterator begin() {return _list.begin();};

		/*!
		The const begin iterator to traverse through the list. 
		*/
		const_iterator begin() const {return _list.begin();};

		/*!
		The end iterator to traverse through the list. 
		*/
		iterator end() {return _list.end();};

		/*!
		The const end iterator to traverse through the list. 
		*/
		const_iterator end() const {return _list.end();};
	};
};

#endif