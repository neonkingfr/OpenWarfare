/*
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 */

/*************************************************************************

Name:

	ObjectList.h

Purpose:

	This file contains the declaration of the ObjectList class.

Version Information:

	Version		Date		Author and Comments
	===========================================
	1.0			25-09/2009	RMR: Original Implementation
	2.0			08-02/2010	UDW: Initial Implementation
	2.01		10-02/2010	MHA: Comments Checked
	
/************************************************************************/

#ifndef VBS2FUSION_LENGHTLIMITEDLIST_H
#define VBS2FUSION_LENGHTLIMITEDLIST_H

/***********************************************************************/
/* INCLUDES
/***********************************************************************/

// Standard C++ Includes
#include <cassert>
#include <list>


// Simcentric Includes
#include "VBS2Fusion.h"

/***********************************************************************/
/* END INCLUDES
/***********************************************************************/

using namespace std;

namespace VBS2Fusion
{

	/*!


	The length_limited_list class is a simple adaptation of the std::list
	class. The class maintains a list whose length is not permitted to
	exceed a pre-defined amount. Values added to the list will be sorted
	according to the specified predicate, the value will only actually 
	be added to the list if the following conditions are met:
	
	1) The size of the list is less than the specified maximum, in which 
	   case all values are accepted.
	2) If the size of the list is equal to the specified maximum the
	   new value will only be added if it is better than the current
	   worst value in the list, as determined by the specified predicate.
	   
*/
template < typename _T >
class  LengthLimitedList
	{
	
	//------------------------------------------------------------------//
	// Type definitions:
	
	public:
	
	typedef _T value_type;
	
	typedef typename std::list<value_type> container_type;
	typedef typename container_type::iterator iterator;
	typedef typename container_type::const_iterator const_iterator;
	typedef typename container_type::size_type size_type;
	
	//------------------------------------------------------------------//
	// Data members:
	
	protected:
	
	container_type m_Container;
	size_type m_nMaxLength;
	
	//------------------------------------------------------------------//
	// Construction/destruction:
	
	public:
	
		/*!
		Primary constructor for the Length limited list. Initializes the size of the list to 0; 
		*/
	LengthLimitedList();

		/*!
		Constructor for the Length limited list. Initializes the size of the list to nMaxLenght; 
		*/
	LengthLimitedList( size_type nMaxLength );

		/*!
		Constructor for the Length limited list. Constructs using an already existing LenghtLimtedList and copies
		its values to the new list. 
		*/
	LengthLimitedList( const LengthLimitedList& l );
	virtual ~LengthLimitedList();
	
	const LengthLimitedList& operator=( const LengthLimitedList& l );		
		
	//------------------------------------------------------------------//
	// Member functionality:
	
	public:
	
	/*!
	size(): Retrieves the current size of this length_limited_list.
	*/

	int size() const;
	
	/*!
	insert(): Adds the specified value to this length_limited_list
	according to the insertion rules which can make use of the
	specified predicate. Returns true if the value
	was added.
	*/

	template < typename _P  >
	bool insert( const value_type& value, 
				 _P& predicate = std::less<value_type>() );
				
	

	/*!
	Begin iterator for unit list. 
	*/
	iterator begin() { return m_Container.begin(); };

	/*!
	Const Begin iterator for unit list. 
	*/
	const_iterator begin() const { return m_Container.begin(); };

	/*!
	End iterator for unit list. 
	*/
	iterator end() { return m_Container.end(); };

	/*!
	Const End iterator for unit list. 
	*/
	const_iterator end() const { return m_Container.end(); };
	
	/*!
	clear(): Erase all of the entries.
	*/
	void clear() ;
	
	/*!
	Returns the maximum length of the LenghtLimitedList. 
	*/
	int getMaxLength() const ;

	/*!
	Sets the maximum length of the LenghtLimitedList. 
	*/
	void putMaxLength( int nMaxLength );
		
	/*!
	// getContainer(): Enables access to the underlying container.
	// Mainly present for the CC.
	*/
	container_type& getContainer() { return m_Container; };

	/*!
	// getContainer(): Enables access to the underlying container.
	// Mainly present for the CC.
	*/
	const container_type& getContainer() const { return m_Container; };
	
	};
};
	
// End of class declaration: length_limited_list
/************************************************************************/

#endif // if defined(_LENGHTLIMITEDLIST_H)

// End of file: LengthLimitedList.hpp
/************************************************************************/