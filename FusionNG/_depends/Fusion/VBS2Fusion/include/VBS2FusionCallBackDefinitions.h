 /**************************************************************************
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 **************************************************************************/

/*************************************************************************
Name:		VBS2FusionCallBackDefinitions
Purpose:	This file contains VBS2Fusion Global ENUM definitions which are used in callbacks


Version Information:

Version		Date		Author and Comments
===========================================
1.00		24-04-2012	RDJ: Original Implementation

/************************************************************************/
#ifndef VBS2FUSIONCALLBACKDEFINITIONS_H
#define VBS2FUSIONCALLBACKDEFINITIONS_H

/***********************************************************************/
/* INCLUDES
/***********************************************************************/
// STANDARD INCLUDES
#include <string>


/**********************************************************************/
/* END INCLUDES
/**********************************************************************/
using namespace std;

namespace VBS2Fusion
{	
	struct VBSParametersEx
	{
		const char *_dirInstall;  // VBS installation directory
		const char *_dirUser;     // user's VBS directory
	};

	enum mouseEventType{ MOUSEMOVE, LEFTBUTTONDOWN, LEFTBUTTONUP, RIGHTBUTTONDOWN, RIGHTBUTTONUP, MOUSEWHEEL };
	struct mouseParameters
	{
		mouseEventType _mouseEvent;
		int _xPos;
		int _yPos;
	};

	struct FrustumSpec
	{
		// camera position
		float _pointPosX;
		float _pointPosY;
		float _pointPosZ;

		// camera speed, used for OpenAL sounds
		float _pointSpeedX;
		float _pointSpeedY;
		float _pointSpeedZ;

		// camera view direction
		float _viewDirX;
		float _viewDirY;
		float _viewDirZ;

		// camera up-view
		float _viewUpX;
		float _viewUpY;
		float _viewUpZ;

		// camera projection angle tangents
		// Beware: for now, top/bottom and left/right pairs are used as single (symmetric) values
		float _projTanTop;
		float _projTanBottom;
		float _projTanLeft;
		float _projTanRight;

		// near, far distance clipping
		float _clipDistNear;
		float _clipDistFar;
		float _clipDistFarShadow;
		float _clipDistFarSecShadow;
		float _clipDistFarFog;

		// standard constructor
		FrustumSpec():
		_pointPosX(0), _pointPosY(0), _pointPosZ(0), _pointSpeedX(0), _pointSpeedY(0), _pointSpeedZ(0),
			_viewDirX(0), _viewDirY(0), _viewDirZ(0), _viewUpX(0), _viewUpY(0), _viewUpZ(0),
			_projTanTop(0), _projTanBottom(0), _projTanLeft(0), _projTanRight(0),
			_clipDistNear(0), _clipDistFar(0), _clipDistFarShadow(0), _clipDistFarSecShadow(0), _clipDistFarFog(0)
		{}

		// auxiliary setting functions
		void SetPos(float posX, float posY, float posZ) {_pointPosX = posX; _pointPosY = posY; _pointPosZ = posZ;}
		void SetSpeed(float speedX, float speedY, float speedZ) {_pointSpeedX = speedX; _pointSpeedY = speedY; _pointSpeedZ = speedZ;}
		void SetDir(float dirX, float dirY, float dirZ) {_viewDirX = dirX; _viewDirY = dirY; _viewDirZ = dirZ;}
		void SetUp(float upX, float upY, float upZ) {_viewUpX = upX; _viewUpY = upY; _viewUpZ = upZ;}
		void SetProj(float top, float bottom, float left, float right) {_projTanTop = top; _projTanBottom = bottom; _projTanLeft = left; _projTanRight = right;}
		void SetClip(float nearStd, float farStd, float farShadow, float farSecShadow, float farFog)
		{
			_clipDistNear = nearStd; _clipDistFar = farStd; _clipDistFarShadow = farShadow; _clipDistFarSecShadow = farSecShadow; _clipDistFarFog = farFog;
		}


	};


	struct FrustumSpecNG
	{
		// camera position
		double pointPosX;
		double pointPosY;
		double pointPosZ;

		// camera speed, used for OpenAL sounds
		float pointSpeedX;
		float pointSpeedY;
		float pointSpeedZ;

		// camera view direction
		float viewDirX;
		float viewDirY;
		float viewDirZ;

		// camera up-view
		float viewUpX;
		float viewUpY;
		float viewUpZ;

		// camera projection angle tangents
		// Beware: for now, top/bottom and left/right pairs are used as single (symmetric) values
		float projTanTop;
		float projTanBottom;
		float projTanLeft;
		float projTanRight;

		// near, far distance clipping
		float clipDistNear;
		float clipDistFar;
		float clipDistFarShadow;
		float clipDistFarSecShadow;
		float clipDistFarFog;

		// standard constructor
		FrustumSpecNG():
		pointPosX(0), pointPosY(0), pointPosZ(0), pointSpeedX(0), pointSpeedY(0), pointSpeedZ(0),
			viewDirX(0), viewDirY(0), viewDirZ(0), viewUpX(0), viewUpY(0), viewUpZ(0),
			projTanTop(0), projTanBottom(0), projTanLeft(0), projTanRight(0),
			clipDistNear(0),clipDistFar(0), clipDistFarShadow(0), clipDistFarSecShadow(0), clipDistFarFog(0)
		{}

		// auxiliary setting functions
		void SetPos(float posX, float posY, float posZ) {pointPosX = posX; pointPosY = posY; pointPosZ = posZ;}
		void SetSpeed(float speedX, float speedY, float speedZ) {pointSpeedX = speedX; pointSpeedY = speedY; pointSpeedZ = speedZ;}
		void SetDir(float dirX, float dirY, float dirZ) {viewDirX = dirX; viewDirY = dirY; viewDirZ = dirZ;}
		void SetUp(float upX, float upY, float upZ) {viewUpX = upX; viewUpY = upY; viewUpZ = upZ;}
		void SetProj(float top, float bottom, float left, float right) {projTanTop = top; projTanBottom = bottom; projTanLeft = left; projTanRight = right;}
		void SetClip(float nearStd, float farStd, float farShadow, float farSecShadow, float farFog)
		{
			clipDistNear = nearStd; clipDistFar = farStd; clipDistFarShadow = farShadow; clipDistFarSecShadow = farSecShadow; clipDistFarFog = farFog;
		}
	};


	
	struct GeometryFace
	{
		int nIndices;     // count of indices in 'indices' array (3 or 4)
		int indices[4];   // array of indices to vertex array

		GeometryFace(){nIndices = 0;}
		GeometryFace(const GeometryFace& face){*this = face;}
	};

	struct GeometryVertex
	{
		float x,y,z;
	};

	struct GeometryObjectVerticesList
	{
		// variables:
		int nVertices;
		GeometryVertex* vertices;	
		
	};

#if _BISIM_DEV_EXTERNAL_POSE
	static GeometryObjectVerticesList spawnPoints;
#else
	GeometryObjectVerticesList spawnPoints;
#endif
		
	struct GeometryObject
	{
		enum Type
		{
			House = 1,
			Tree = 2,
			Terrain = 4,
			Road = 8
		};
		Type type;
		long long realObjectId;
		float transform[4][3];
		GeometryVertex* vertices;
		int nVertices;
		GeometryFace* faces;
		int nFaces;

		GeometryObjectVerticesList spawnPoints;

		GeometryObject(){nVertices = 0; nFaces = 0; vertices = NULL; faces = NULL;}
		~GeometryObject(){if (vertices) delete[] vertices; if (faces) delete[] faces;}
		GeometryObject(const GeometryObject& obj);
		const GeometryObject& operator=(const GeometryObject& obj);
		bool IsValid()const{return nFaces > 0 && nVertices > 0 && vertices && faces;}

		GeometryObject& operator+=(const GeometryObject &source); // appends vertices and faces from another object
		
	};

	

	// structure holds additional vertex arrays for GeometryObject
	
	
	}





#endif
