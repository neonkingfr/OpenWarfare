/*
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 */

/*************************************************************************

Name:

	WaypointCompleteEvent.h

Purpose:

	This file contains the declaration of the WaypointCompleteEvent class.

Version Information:

	Version		Date		Author and Comments
	===========================================
	1.0			17-04/2009	RMR: Original Implementation
	2.0			11-01/2009	UDW: Version 2 Implementation
	2.01		10-02/2010	MHA: Comments Checked

/************************************************************************/

#ifndef VBS2FUSION_WAYPOINT_COMPLETE_EVENT_H
#define VBS2FUSION_WAYPOINT_COMPLETE_EVENT_H

#include <sstream>
#include <string>

#include "VBS2Fusion.h"
#include "Events/Event.h"
#include "position3D.h"

namespace VBS2Fusion
{
	/*!
	Defines a WaypointComplete event which is passed onto the object 
	when an 'WAYPOINTCOMPLETE' event is called. Usually passed into the processWaypointCompleteEvent
	method of a Group. 

	Triggered when the group completes a waypoint. 
	*/
	class VBS2FUSION_API WaypointCompleteEvent: public Event
	{
	public:
		/*!
		Primary constructor for the class. Calls setType(WAYPOINTCOMPLETE) to initialize. 
		*/
		WaypointCompleteEvent();

		/*!
		Primary destructor for the class.
		*/
		~WaypointCompleteEvent();

		/*!
		Set the index of the waypoint with relation to its position on the waypoint list of the group.
		*/
		void setIndex(int idx);

		/*!
		Returns the index of the waypoint with relation to its position on the waypoint list of the group.
		*/
		int getIndex() const;

	private:

		int index;
	};
};

#endif //WAYPOINT_COMPLETE_EVENT_H