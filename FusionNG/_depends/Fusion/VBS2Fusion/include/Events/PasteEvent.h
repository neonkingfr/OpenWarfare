/*
* Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
* Reserved.
*
* COPYRIGHT/OWNERSHIP
* This Software and its source code are proprietary products of SimCentric and
* are protected by copyright and other intellectual property laws. The Software
* is licensed and not sold.
*
* USE OF SOURCE CODE
* This source code may not be used, modified or copied without the expressed,
* written permission of SimCentric.
*
* RESTRICTIONS
* You MAY NOT: (a) copy and distribute the Software or any portion of it;
* (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
* or permit reverse engineering, disassembly, decompilation or alteration of
* this Software; (d) remove any product identification, copyright notices, or
* other notices or proprietary restrictions from this Software; (e) copy the
* documentation accompanying the software.
*
* DISCLAIMER OF WARRANTIES
* The Software is supplied "AS IS". SimCentric disclaims all warranties,
* expressed or implied, including, without limitation, the warranties of
* merchantability and of fitness for any purpose. The user must assume the
* entire risk of using the Software.
*
* DISCLAIMER OF DAMAGES
* SimCentric assumes no liability for damages, direct or consequential, which
* may result from the use of the Software, even if SimCentric has been advised
* of the possibility of such damages. Any liability of the seller will be
* limited to refund the purchase price.
*
*/

/*************************************************************************

Name:

	PasteEvent.h

Purpose:

	This file contains the declaration of the PasteEvent class.

Version Information:

	Version		Date		Author and Comments
	===========================================
	1.0			03-08/2011	CGS: Original Implementation

/************************************************************************/

#ifndef VBS2FUSION_PASTE_EVENT_H
#define VBS2FUSION_PASTE_EVENT_H


/***********************************************************************/
/* INCLUDES
/***********************************************************************/
// STANDARD INCLUDES
#include <sstream>
#include <string>

// SIMCENTRIC INCLUDES
#include "VBS2Fusion.h"
#include "Event.h"
/***********************************************************************/
/* END INCLUDES
/***********************************************************************/


namespace VBS2Fusion
{

	/*!
	Defines a Paste event which is passed onto the created object 
	when an 'PASTE' event is called.  
	*/
	class VBS2FUSION_API PasteEvent: public Event
	{
	public:
		/*!
		Primary constructor for the class. Calls setType(PASTE) to initialize. 
		*/
		PasteEvent();

		/*!
		Primary destructor for the class. 
		*/
		~PasteEvent();

		/*!
		Set the VBS2 name of the newly created (i.e. pasted ) Object. 
		*/
		void setNewObjectName(string name);

		/*!
		Return the VBS2 name of the newly created (i.e. pasted ) Object. 
		*/
		string getNewObjectName() const;

		/*!
		Set the newly created Object's NetworkID by using NetworkID type. 
		*/
		void setNewObjectID(NetworkID id);

		/*!
		Returns the newly created Object's NetworkID in NetworkID type.
		*/
		NetworkID getNewObjectID() const;

	private:
		string _newObjName;
		NetworkID _newObjId;
	};

};

#endif //PASTE_EVENT_H