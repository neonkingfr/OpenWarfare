/*
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 */

/*************************************************************************

Name:

	LandedTouchDownEvent.h

Purpose:

	This file contains the declaration of the LandedTouchDownEvent class.

Version Information:

	Version		Date		Author and Comments
	===========================================
	1.0			08-07/2011	CGS: Original Implementation


/************************************************************************/

#ifndef VBS2FUSION_LANDED_TOUCH_DOWN_EVENT_H
#define VBS2FUSION_LANDED_TOUCH_DOWN_EVENT_H

/***********************************************************************/
/* INCLUDES
/***********************************************************************/
// STANDARD INCLUDES
#include <sstream>
#include <string>

// SIMCENTRIC INCLUDES
#include "VBS2Fusion.h"
#include "Event.h"
/***********************************************************************/
/* END INCLUDES
/***********************************************************************/

namespace VBS2Fusion
{

	/*!
	Defines a LandedTouchDown event which is passed onto the object when an 'LANDEDTOUCHDOWN' 
	event is called. 
	*/
	class VBS2FUSION_API LandedTouchDownEvent: public Event
	{
	public:

		/*!
		Primary constructor for the class. Calls setType(LANDEDTOUCHDOWN) to initialize. 
		*/
		LandedTouchDownEvent();

		/*!
		Primary destructor for the class. 
		*/
		~LandedTouchDownEvent();

		/*!
		Sets the ID of the airport .
		*/
		void setAirportID(int airportID);

		/*!
		Gets the ID of the airport .
		*/
		int getAirportID() const;

	private:

		int _airportID;
	};

};

#endif //LANDED_TOUCH_DOWN_EVENT_H