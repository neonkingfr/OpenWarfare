/*
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 */

/*************************************************************************

Name:

VBS2FusionAppContext.h

Purpose:

This file contains the declaration of the VBS2Fusion Global Application Context.

Version Information:

Version		Date		Author and Comments
===========================================
2.0			31-08/2009	SDS: Original Implementation
2.01		03-02/2010	MHA: Checked Comments

/************************************************************************/

#ifndef VBS2FUSION_APP_CONTEXT
#define VBS2FUSION_APP_CONTEXT


/**********************************************/
/* INCLUDES
/**********************************************/

// Standard Includes
#include <sstream>
#include <string>

// Simcentric Includes
#include "conversions.h"
/**********************************************/
/* END INCLUDES
/**********************************************/

using namespace std;

namespace VBS2Fusion
{
	class VBS2FUSION_API VBS2FusionAppContext
	{
	public:

		/*!primary constructor for class*/
		VBS2FusionAppContext(void);

		/*!primary destructor for class*/
		~VBS2FusionAppContext(void);		

		/*!
		Generates and returns a random alias 
		*/
		string generateRandomAlias(string alias);

		/*!
		Clear VBS2Fusion error log
		*/
		void clearErrorLog();
	};

	
};

VBS2Fusion::VBS2FusionAppContext& get_AppContext(); //global application context

#endif
