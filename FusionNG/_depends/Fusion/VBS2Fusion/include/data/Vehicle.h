/*
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 */

/*************************************************************************

Name:

	Vehicle.h

Purpose:

	This file contains the declaration of the Vehicle class.

Version Information:

	Version		Date		Author and Comments
	===========================================
	2.0			01-04/2009	RMR: Original Implementation
	2.01		11-02/2010	MHA: Comments Checked
	2.02		01-06/2010  YFP: Added  		
									int _iGearVal  = gear value 
									double _fRPMVal = RPM of the vehicle 
									double _fEngineStrength  = Engine Strength Coefficient
								 Added Methods,
									void setGear(int);
									int getGear() const;
									void setRPM(double);
									double getRPM() const;
									void setEngineStrength(double);
									double getEngineStrength() const;
									void setMaxSpeedLimit(double);
									double getMaxSpeedLimit() const; 

/************************************************************************/

#ifndef VBS2FUSION_VEHICLE_H
#define VBS2FUSION_VEHICLE_H


/**************************************************************************
  To disable warnings raised at the compile time of a Dynamic Link Library
  (DLL) due to defining objects of classes which has members of types 
  defined in the Standard Template Library (STL). 
  Warning identifier [C:4251]
/**************************************************************************/
#pragma warning (push) 
#pragma warning (disable: 4251)


/***********************************************************************/
/* INCLUDES
/***********************************************************************/
// STANDARD INCLUDES
#include <string>

// SIMCENTRIC INCLUDES
#include "position3D.h"
#include "VBS2Fusion.h"
#include "VBS2FusionDefinitions.h"
#include "data/Waypoint.h"
#include "data/ControllableObject.h"
#include "data/Unit.h"
#include "data/Group.h"
#include "data/VehicleArmedModule.h"

/**********************************************************************/
/* END INCLUDES
/**********************************************************************/


namespace VBS2Fusion
{
	
	class VBS2FUSION_API Vehicle: public ControllableObject
	{
	public:		

		/*!
		The player assignment structure. Used by the Vehicle class 
		to store information on units assigned to a vehicle. Contains 
		the following information. 

		- The units VBS2 name
		- The VBS2Fusion alias
		- Network ID
		- The role of the unit in the vehicle in VEHICLEASSIGNMENTTYPE format. 
		*/
		struct playerAssignment
		{
			/*!
			Stores the VBS2 name of the unit. 
			*/
			string _unitName;

			/*!
			Stores the VBS2Fusion alias of the unit. 
			*/
			string _unitAlias;

			/*!
			Stores the network ID of the unit. 
			*/
			string _unitID;

			/*!
			Stores the type of assignment. 
			*/
			VEHICLEASSIGNMENTTYPE _role;

			/*!
			Internal VBS2Fusion parameter. Do not change. 
			*/
			bool _bCurrentMember;
		};

		/*!
		Typedef for list of units assigned to the vehicle. Is a vector of playerAssignment 
		structures. 
		*/
		typedef vector<playerAssignment> unitList;

		/*!
		Iterator for playerAssignment list. 
		*/
		typedef vector<playerAssignment>::iterator iterator;

		/*!
		Const iterator for playerAssignment list. 
		*/
		typedef vector<playerAssignment>::const_iterator const_iterator;	

		/*!
		Typedef for list of Waypoints assigned to the vehicle. 
		*/
		typedef vector<Waypoint> waypointList;

		/*!
		Iterator for waypoint list. 
		*/
		typedef vector<Waypoint>::iterator wp_iterator;

		/*!
		Const iterator for waypoint list. 
		*/
		typedef vector<Waypoint>::const_iterator const_wp_iterator;

		/*!
		Main constructor for the Vehicle class. Initializes the following:

		- fuel = 0
		- placement radius = 0
		- type = "UralCivil"

		Also calls default constructor for ControllableObject. 
		*/
		Vehicle();

		/*!
		Constructor for the Vehicle class. 
		
		*/
		Vehicle(string& name);

		/*!
		Constructor for the Vehicle class NetworkID. 
		
		*/

		Vehicle(NetworkID& id);

		/*!
		Constructor for the Vehicle class. 

		The following variables are initialized as follows:

		_strAlias = "";
		_strName = name;
		_strID = "";
		_strInitializationStatements = InitializationStatements;
		_bPlayer = false;
		_bPlayable = false;
		*/
		Vehicle(string& name, string& InitializationStatements);


		/*!
		Constructor for the Vehicle class position. 

		
		*/
		Vehicle(string& name, position3D& position);

		/*!
		Constructor for the Vehicle class. 

		The following variables are initialized as follows:

		_strAlias = "";
		_strName = name;
		_strID = "";
		_strInitializationStatements = InitializationStatements;
		_bPlayer = false;
		_bPlayable = false;
		_position = position;
		*/
		Vehicle(string& name, position3D& position, string& InitializationStatements);
		

		/*!
		Main destructor the the Vehicle class. 
		*/
		virtual ~Vehicle();

		/*!
			Copy constructor for Vehicle class. 
		*/
		Vehicle(const Vehicle& vehicle);

		/*!
			Assignment operator for Vehicle class.
			Assign all the attributes of Vehicle class.
			Also call the assignment operator of ControllableObject class.
		*/
		Vehicle& operator=(const Vehicle& vehicle);

		/*!
			Equal operator for Vehicle class.
			Compared by 
				- NetworkID: if both units are global.
				- name and alias: otherwise.
		*/
		bool operator==(const Vehicle& vehicle);

		/*!
			Less than operator for Vehicle class.
			Compared by 
				- NetworkID: if both units are global.
				- otherwise return false.
		*/
		bool operator < (const Vehicle& vehicle) const;

		/*!
			Initialize function that used for initialize the unit object. 
		*/
		void initialize();

		/*!
		Sets the fuel level of the vehicle.
		*/
		void setFuel(double fuel);

		/*!
		Returns the current fuel level of the vehicle.
		*/
		double getFuel() const;

		/*!
		Sets boolean determining whether the vehicle is locked for loading/unloading 
		of units. 
		*/
		void setLock(bool lock);

		/*!
		Returns boolean determining whether the vehicle is locked for loading/unloading 
		of units. 
		*/
		bool getLock() const;

		/*!
		Sets the placement radius. 
		*/
		void setPlacementRadius(double placementRadius);

		/*!
		Returns the placement radius. 
		*/
		double getPlacementRadius() const;

		/*!
		Sets the group name the vehicle belongs to using group.getName(). 
		*/
		void setGroup(Group& group);

		/*!
		Sets the name of the group the vehicle belongs to. 
		*/
		void setGroupName(string group);

		/*!
		Returns the name of the group the vehicle belongs to. 
		*/
		string getGroupName() const;

		/*!
		Sets the alias of the group the vehicle belongs to. 
		*/
		void setGroupAlias(string group);

		/*!
		Returns the alias of the group the vehicle belongs to. 
		*/
		string getGroupAlias() const;

		/*!
		Sets the VBS2 network ID of the object. 
		*/
		void setGroupID(string& strID);

		/*!
		Returns the VBS2 network ID of the object. 
		*/
		string getGroupID() const;

		/*!
		Set vehicle special properties using a VEHICLESPECIALPROPERTIES variable. It should be one
		of: (VNONE, FLY or FORM)
		*/
		void setSpecialProperty(VEHICLESPECIALPROPERTIES sproperty);

		/*!
		Set vehicle special properties using a string variable. It should be one
		of: ("VNONE", "FLY" or "FORM")
		*/
		void setSpecialProperty(string strProperty);

		/*!
		Return vehicle special properties using a VEHICLESPECIALPROPERTIES variable. It should be one
		of: (VNONE, FLY or FORM)
		*/
		VEHICLESPECIALPROPERTIES getSpecialProperty() const;

		/*!
		Return vehicle special properties using a string variable. It should be one
		of: ("VNONE", "FLY" or "FORM")
		*/
		string getSpecialPropertyString();

		/*!
		Adds the unit to the unit list and assigns the assignment type
		defined by role. 
		*/
		void addUnit(Unit& unit, VEHICLEASSIGNMENTTYPE role);

		/*!
		Adds player assignment to the unit list. 
		*/
		void addUnit(playerAssignment playerassignment);

		/*!
		Adds a unit to the list with the name unitName and assignment role. 
		*/
		void addUnitByName(string unitName, VEHICLEASSIGNMENTTYPE role);

		/*!
		Adds a unit to the list with the ID unitID and assignment role. 
		*/
		void addUnitByID(string unitID, VEHICLEASSIGNMENTTYPE role);

		/*!
		Adds a unit to the list with the alias unitAlias and assignment role. 
		*/
		void addUnitByAlias(string unitAlias, VEHICLEASSIGNMENTTYPE role);
		
		/*!
		Removes the unit defined by unit from the list. Returns false if the
		unit is not a member of a vehicle. 
		*/
		bool removeUnit(Unit& unit);

		/*!
		Removes the first unit on the list with the assignment
		defined by role. . Returns false if the
		unit is not a member of a vehicle. 
		*/
		bool removeUnitByRole(VEHICLEASSIGNMENTTYPE role);

		/*!
		Removes the unit with the name unitName. Returns false if the
		unit is not a member of a vehicle. 
		*/
		bool removeUnitByName(string unitName);

		/*!
		Removes the unit with the ID unitID. Returns false if the
		unit is not a member of a vehicle. 
		*/
		bool removeUnitByID(string unitID);

		/*!
		Removes the unit with the alias unitAlias. Returns false if the
		unit is not a member of a vehicle. 
		*/
		bool removeUnitByAlias(string unitAlias);

		/*!
		Returns true if a unit with the alias strUnitAlias is a member of 
		the vehicle. 
		*/
		bool isMemberByAlias(string strUnitAlias);

		/*!
		Returns true if a unit with the name strUnitName is a member of 
		the vehicle. 
		*/
		bool isMemberByName(string strUnitName);

		/*!
		Returns true if a unit with the ID strUnitID is a member of 
		the vehicle. 
		*/
		bool isMemberByID(string strUnitID);

		/*!
		Clears the unit list. 
		*/
		void clearUnits();

		/*!
		Returns the number of units currently assigned to the vehicle. 
		*/
		int getNumberOfUnits()const;

		/*!
		Returns the unit list. 
		*/
		Vehicle::unitList& getUnitList();

		/*!
		Begin iterator for unit list. 
		*/
		iterator begin();

		/*!
		const Begin iterator for unit list. 
		*/
		const_iterator begin() const;

		/*!
		End iterator for unit list. 
		*/
		iterator end();

		/*!
		Const End iterator for unit list. 
		*/
		const_iterator end() const;	

		/*!
		Returns true if the vehicle has crew. 
		*/
		bool hasCrew() const;

		/*!
		Returns true if the vehicle has a driver. 
		*/
		bool hasDriver();

		/*!
		Returns true if the vehicle has a gunner. 
		*/
		bool hasGunner();

		/*!
		Returns true if the vehicle has units assigned as cargo.  
		*/
		bool hasCargo();

		/*!
		Returns true if the vehicle has a commander. 
		*/
		bool hasCommander();

		/*!
		Returns the ID of the driver if one exists. Returns an empty string if
		the vehicle does not have a driver assigned. 
		*/
		string getDriverID();

		/*!
		Returns the name of the driver if one exists. Returns an empty string if
		the vehicle does not have a driver assigned. 
		*/
		string getDriverName();

		/*!
		Returns the alias of the driver if one exists. Returns an empty string if
		the vehicle does not have a driver assigned. 
		*/
		string getDriverAlias();

		//**********************************************************************

		/*!
		Returns the ID of the commander if one exists. Returns an empty string if
		the vehicle does not have a commander assigned. 
		*/
		string getCommanderID();

		/*!
		Returns the name of the commander if one exists. Returns an empty string if
		the vehicle does not have a commander assigned. 
		*/
		string getCommanderName();

		/*!
		Returns the alias of the commander if one exists. Returns an empty string if
		the vehicle does not have a commander assigned. 
		*/
		string getCommanderAlias();

		//**********************************************************************
		
		/*!
		Returns the ID of the first unit with assignment role if one exists. Returns an empty string if
		the vehicle does not have a commander assigned. 
		*/
		string getUnitID(VEHICLEASSIGNMENTTYPE role);

		/*!
		Returns the name of the first unit with assignment role if one exists. Returns an empty string if
		the vehicle does not have a commander assigned. 
		*/
		string getUnitName(VEHICLEASSIGNMENTTYPE role);

		/*!
		Returns the alias of the first unit with assignment role if one exists. Returns an empty string if
		the vehicle does not have a commander assigned. 
		*/
		string getUnitAlias(VEHICLEASSIGNMENTTYPE role);

		/*!
		Returns the waypoint list. 
		*/
		waypointList getWaypoints() const;

		/*!
		Adds the waypoint to the waypoint list. 
		*/
		void addWaypoint(Waypoint& wp);

		/*!
		Inserts the waypoint at the position specified into the waypoint list. 
		*/
		void insertWaypoint(Waypoint& wp, int position);

		/*!
		Removes the waypoint with the name waypoint name if it exists. Returns
		false if the waypoint specified is not a member of the list. 
		*/
		bool removeWaypoint(string waypointName);

		/*!
		Removes the waypoint described by wp if it exists. Returns
		false if the waypoint specified is not a member of the list. 
		*/
		bool removeWaypoint(Waypoint& wp);

		/*!
		Removes the waypoint at the index specified. 
		*/
		bool removeWaypoint(int index);

		/*!
		Returns the number of Waypoints currently assigned. 
		*/
		int getNumberOfWaypoints();

		/*!
		Clears the waypoint list. 
		*/
		void clearAllWaypoints();

		/*!
		Waypoint begin iterator. 
		*/
		wp_iterator wp_begin();

		/*!
		Waypoint begin const iterator. 
		*/
		const_wp_iterator wp_begin() const;

		/*!
		Waypoint end iterator. 
		*/
		wp_iterator wp_end();

		/*!
		Waypoint end const iterator. 
		*/
		const_wp_iterator wp_end() const;	

		/*!
			Sets the direction of the vehicle. 
		*/
		void setDir(double dir);

		/*!
		Returns the direction of the vehicle. 
		DEPRECATED function
		Use "ControllableObject::getDirection()" instead
		*/
		double getDir() const;

		/*!
		Sets the index of the currently active waypoint. 
		*/
		void setCurrentWaypoint(int val);

		/*!
		Returns the index of the currently active waypoint. 
		*/
		int getCurrentWaypoint() const;

		/*!
		Converts a string into a VEHICLEASSIGNMENTTYPE variable. 
		*/
		static VEHICLEASSIGNMENTTYPE convertStringToVBSRole(string vbsOutput);

		/*!
		Converts a VEHICLEASSIGNMENTTYPE variable into a string. 
		*/
		static string convertVBSRoleToString(VEHICLEASSIGNMENTTYPE role);

		/*!
		set currect gear of the vehicle.  0: reverse, 1: neutral, 2: first forward gear, etc.
		*/
		void setGear(int gearVal);

		/*!
		returns the gear value.
		*/
		int getGear() const;


		/*!
		set RPM of the vehicle.
		*/
		void setRPM(float rpmValue);

		/*!
		returns RPM of the vehicle.
		*/
		float getRPM() const;

		/*!
		set Engine Strength coefficient.
		*/
		void setEngineStrength(double strengthCoeff);

		/*!
		returns engine strength coefficient.
		*/
		double getEngineStrength() const;

		/*!
		set Maximum speed limit of the vehicle.
		*/
		void setMaxSpeedLimit(double maxSpeedLimit);

		/*!
		returns maximum speed limit of the vehicle.
		*/
		double getMaxSpeedLimit() const;

		/*!
		Iterator for Turret list.
		*/
		typedef list<Turret>::iterator turret_iterator;

		/*!
		Constant iterator for Turret list.
		*/
		typedef list<Turret>::const_iterator turret_const_iterator;

		/*!
		Iterator for Magazine list.
		*/
		typedef list<Magazine>::iterator magazines_iterator;

		/*!
		Constant iterator for magazine list.
		*/
		typedef list<Magazine>::const_iterator magazines_const_iterator;

		/*!
		Iterator for weapons list.
		*/
		typedef list<Weapon>::iterator weapons_iterator;

		/*!
		Constant Iterator for weapons list.
		*/
		typedef list<Weapon>::const_iterator weapons_const_iterator;

		/*!
		Sets the name of the primary weapon. 
		*/
		void setPrimaryWeapon(string weaponName); 

		/*!
		Returns the name of the primary weapon. 
		*/
		string getPrimaryWeapon() const; 		

		/*!
		Returns the ArmedModule of the Vehicle. 
		*/
		VehicleArmedModule* getArmedModule() const;

		/*!
		Set Armed module to Vehicle. 
		*/
		void setArmedModule(VehicleArmedModule* vamodule);

		/*!
		Adds a new weapon using a weapon object. 
		*/
		void addWeapon(Weapon weapons);

		/*!
		Adds a new weapon using a class name. 
		*/
		void addWeapon(string weaponsName);
		
		/*!
		Clears the weapons list. 
		*/
		void clearWeaponsList();

		/*!
		Returns the number of weapons currently assigned. 
		*/
		int getNumberOfWeapons()const;

		/*!
		Removes weapon with class name weaponName from list. Returns false if
		operation was unsuccessful due to the weapon not being on the list. 
		*/
		bool removeWeapon(string weaponName);

		/*!
		Add magazine to the unit using magazine Object.
		*/
		void addMagazine(Magazine magazine);

		/*!
		Adds a new magazine using a class name. Sets the number of magazines
		to 0 and clears the ammo amount list. 
		*/
		void addMagazine(string magazineName);

		/*!
		Clears the magazine list. 
		*/		
		void clearMagazineList();

		/*!
		Returns the number of magazine types currently assigned. 
		*/
		int getNumberOfMagazines();

		/*!
		Removes magazine with class name magazineName from list. Returns false if
		operation was unsuccessful due to the magazine not being on the list. 
		*/
		bool removeMagazine(string magazineName);

		/*!
		Returns true of a magazine with the class name magazineName
		is on the list. 
		*/
		bool isMagazineAdded(string magazineName);

		/*!
		Clears the turret list. 
		*/		
		void clearTurretList();

		/*!
			Begin iterator for unit list. 
		*/
		turret_iterator turrets_begin();

		/*!
			Const Begin iterator for unit list. 
		*/
		turret_const_iterator turrets_begin() const;

		/*!
			End iterator for unit list. 
		*/
		turret_iterator turrets_end();

		/*!
			Const End iterator for unit list. 
		*/
		turret_const_iterator turrets_end() const;	
		
		/*!
		Begin iterator for the magazine list.
		*/
		magazines_iterator magazines_begin();

		/*!
		Begin const iterator for the magazine list.
		*/
		magazines_const_iterator magazines_begin() const;

		/*!
		End iterator for the magazine list.
		*/
		magazines_iterator magazines_end();

		/*!
		Begin const iterator for the magazine list.
		*/
		magazines_const_iterator magazines_end() const;

		/*!
		Begin iterator for the weapons list.
		*/
		weapons_iterator weapons_begin();

		/*!
		Begin const iterator for the weapons list.
		*/
		weapons_const_iterator weapons_begin() const;

		/*!
		End iterator for the weapons list.
		*/
		weapons_iterator weapons_end();

		/*!
		End const iterator for the weapons list.
		*/
		weapons_const_iterator weapons_end() const;

		/*!
		Add a turret to the Vehicle.
		*/
		void setTurret(Turret turret);

		/*!
		Get the main turret of the Vehicle. 
		*/
		Turret getMainTurret() const;

		/*!
		Get the turret by the specified path string.
		*/
		Turret getTurretByPathString(string turretPath) const;

		/*!
		Get the turret by the specified name. 
		*/
		Turret getTurretByName(string turretName) const;

		/*!
		Check weather the turret is available. 
		*/
		bool isTurretExists(string turretPath) const;



	private:

		bool _bHasCrew;
		double _fuel;
		bool _lock;
		unitList _unitList;
		waypointList _waypointList;
		VEHICLESPECIALPROPERTIES _specialProperty;
		double _fPlacementRadius;
		string _strGroupName;	
		string _strGroupAlias;
		string _strGroupID;
		int _currentWaypoint;

		int	_iGearVal;
		//float _fRPMVal;
		double _fRPMVal;
		double _dEngineStrength;
		double _dMaxSpeedLimit;
		VehicleArmedModule *_varmedModule;
					
	};
};

#pragma warning(pop) // Enable warnings [C:4251]

#endif //VEHICLE_H