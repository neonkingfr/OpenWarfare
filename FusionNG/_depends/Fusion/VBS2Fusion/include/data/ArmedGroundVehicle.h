/*
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 */

/*************************************************************************

Name:

	ArmedGroundVehicle.h

Purpose:

	This file contains the declaration of the NetworkID class.

Version Information:

	Version		Date		Author and Comments
	===========================================
	2.0			11-12/2009	YFP: Original Implementation
	2.01		01-2/2009	MHA: Checked Comments

/************************************************************************/

#ifndef VBS2FUSION_ARMEDGROUNDVEHICLE_H
#define VBS2FUSION_ARMEDGROUNDVEHICLE_H

/**************************************************************************
To disable the warnings arise because of using deprecated ArmedModule and 
GroundVehicle inside the ArmedModuleUtilites class.
Warning identifier [C:4996]
/**************************************************************************/
#pragma warning (push)
#pragma warning (disable: 4996)

/***********************************************************************/
/* INCLUDES
/***********************************************************************/
// STANDARD INCLUDES

// SIMCENTRIC INCLUDES
#include "VBS2Fusion.h"
#include "data/GroundVehicle.h"
#include "data/ArmedModule.h"
/**********************************************************************/
/* END INCLUDES
/**********************************************************************/


namespace VBS2Fusion
{

	class VBS2FUSION_API VBS2FUSION_DPR(DAGV) ArmedGroundVehicle: public GroundVehicle
	{
	public:
		/*!
			Default Constructor for Armed Ground Vehicle. 
		*/
		ArmedGroundVehicle();

		/*!
			Destructor for Armed Ground Vehicle.
		*/
		~ArmedGroundVehicle();

		/*!
			Copy Constructor of Armed Ground Vehicle. 
		*/
		ArmedGroundVehicle(const ArmedGroundVehicle& vehicle);

		/*!
			Assignment operator for ArmedGroundVehicle class.
			Assign all the attributes of ArmedGroundVehicle class.
			Also call the assignment operator of GroundVehicle class.
		*/
		ArmedGroundVehicle& operator = (const ArmedGroundVehicle& vehicle);

		/*!
			Less than operator for ArmedGroundVehicle class.
			Compared by NetworkID.
		*/
		bool operator < (const ArmedGroundVehicle& vehicle) const;

		/*!
			Equal operator for ArmedGroundVehicle class.
			Compared by NetworkID.
		*/
		bool operator == (const ArmedGroundVehicle& vehicle); 
		
		/*!
			Set Armed Module to the Armed Ground Vehicle. 
		*/
		 void setArmedModule(ArmedModule amModule);

		/*!
			Returns the Armed Module of the Armed Ground Vehicle.
		*/
		ArmedModule getArmedModule() const;

	private:
		 void Initialize();

	private:
		ArmedModule _aModule;	

	};

};
#pragma warning (pop) // Enable warnings [C:4996]

#endif //ARMEDGROUNDVEHICLE_H