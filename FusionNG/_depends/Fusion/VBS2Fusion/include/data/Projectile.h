/*
* Copyright 2010 SimCentric Technologies, Pty. Ltd.  All Rights Reserved.
*
* Permission to use, copy, modify, and distribute this software in object
* code form for any purpose and without fee is hereby granted, provided
* that the above copyright notice appears in all copies and that both
* that copyright notice and the limited warranty and restricted rights
* notice below appear in all supporting documentation.
*
* SIMCENTRIC PROVIDES THIS PROGRAM "AS IS" AND WITH ALL FAULTS.
* SIMCENTRIC SPECIFICALLY DISCLAIMS ANY AND ALL WARRANTIES, WHETHER EXPRESS
* OR IMPLIED, INCLUDING WITHOUT LIMITATION THE IMPLIED WARRANTY
* OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR USE OR NON-INFRINGEMENT
* OF THIRD PARTY RIGHTS.  SIMCENTRIC DOES NOT WARRANT THAT THE OPERATION
* OF THE PROGRAM WILL BE UNINTERRUPTED OR ERROR FREE.
*
* In no event shall SimCentric Technologies, Pty. Ltd. be liable for any direct, indirect,
* incidental, special, exemplary, or consequential damages (including,
* but not limited to, procurement of substitute goods or services;
* loss of use, data, or profits; or business interruption) however caused
* and on any theory of liability, whether in contract, strict liability,
* or tort (including negligence or otherwise) arising in any way out
* of such code.
*/

/*************************************************************************

Name:

	Projectile.h

Purpose:

	This file contains the declaration of the Projectile class.

Version Information:

	Version		Date		Author and Comments
	===========================================
	2.0			26-3/2009	RMR: Original Implementation
	2.0			27-12/2009	UDW: Version 2 Implementation
	2.01		08-2/2010	MHA: Comments Checked

/************************************************************************/

#ifndef PROJECTILE_H
#define PROJECTILE_H

/***********************************************************************/
/* INCLUDES
/***********************************************************************/

// STANDARD INCLUDES
#include <sstream>
#include <string>

// SIMCENTRIC INCLUDES
#include "position3D.h"
#include "VBS2Fusion.h"
#include "ControllableObject.h"

/***********************************************************************/
/* END OF INCLUDES
/***********************************************************************/

namespace VBS2Fusion
{
	/*!
	A projectile object is used to store information related to a VBS2 object. The current version is identical to a
	ControllableObject, but is added to the class hierarchy for future feature compatibility. These object can be directly updated using
	the update functions available on ControllableObjectUtilities. 
	*/
	class VBS2FUSION_API Projectile: public ControllableObject
	{
	public:
		/*!
		Constructor for projectile object. 
		*/
		Projectile();

		/*!
		Destructor for projectile. 
		*/
		virtual ~Projectile();

		/*!
		Sets the name of the Projectile .
		*/
		void setName(string name);

	
	};
};

#endif //PROJECTILE_H