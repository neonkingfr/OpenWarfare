
 /**************************************************************************
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 **************************************************************************/

/*************************************************************************

Name:

	Camera.h

Purpose:

	This file contains the declaration of the Camera class.

Version Information:

	Version		Date		Author and Comments
	===========================================
	1.0			11-09/2009	SLA: Original Implementation
	2.0			12-12/2009	UDW: Version 2 Implementation
								 Copy Constructor and = operators added
								 getLogFileName()
	2.01		11-02/2010	MHA: Comments Checked

/************************************************************************/

#ifndef VBS2FUSION_CAMERA_H
#define VBS2FUSION_CAMERA_H

/***********************************************************************/
/* INCLUDES
/***********************************************************************/

// Standard C++ Includes
#include <string>

// Simcentric Includes
#include "position3D.h"
#include "VBS2Fusion.h"
#include "data/ControllableObject.h"
#include "VBS2FusionDefinitions.h"

/***********************************************************************/
/* END INCLUDES
/***********************************************************************/

namespace VBS2Fusion
{
	
	

	class VBS2FUSION_API Camera: public ControllableObject
	{
	public:
		/*!
		Main constructor for this class. The following parameters are initialized as follows:

		- _camEffectType = CAMEFFECTTYPE_INTERNAL;
		- _camEffectPos = CAMEFFECTPOS_BACK;
		- setCamTarget(1,1,1);
		- setCamRelPos(1,1,1);
		- _dCamDiveAngle = 0;
		- _dCamFocusBlur = 1;
		- _dCamFocusDist = -1;
		- _dCamBank = 1;
		- _dCamFov = 0.7;
		- _dCamFovRangeHigh = 0.7;
		- _dCamFovRangeLow = 0.7;
		*/
		Camera();

		/*!
		Main constructor for this class. The following parameters are initialized as follows:

		- _camEffectType = CAMEFFECTTYPE_INTERNAL;
		- _camEffectPos = CAMEFFECTPOS_BACK;
		- _camTarget = target;
		- _camRelPos = camRelPos;
		- _dCamDiveAngle = 0;
		- _dCamFocusBlur = 1;
		- _dCamFocusDist = -1;
		- _dCamBank = 1;
		- _dCamFov = 0.7;
		- _dCamFovRangeHigh = 0.7;
		- _dCamFovRangeLow = 0.7;
		*/
		Camera(position3D target, position3D camRelPos);

		/*!
		Main constructor for this class. The following parameters are initialized as follows:

		- _camEffectType = camEffectType;
		- _camEffectPos = camEffectPos;
		- setCamTarget(1,1,1);
		- setCamRelPos(1,1,1);
		- _dCamDiveAngle = 0;
		- _dCamFocusBlur = 1;
		- _dCamFocusDist = -1;
		- _dCamBank = 1;
		- _dCamFov = 0.7;
		- _dCamFovRangeHigh = 0.7;
		- _dCamFovRangeLow = 0.7;
		*/
		Camera(CAMEFFECTTYPE camEffectType, CAMEFFECTPOS camEffectPos);

		/*!
		Main constructor for this class. The following parameters are initialized as follows:

		- _camEffectType = camEffectType;
		- _camEffectPos = camEffectPos;
		- _camTarget = target;
		- setCamRelPos(1,1,1);
		- _dCamDiveAngle = 0;
		- _dCamFocusBlur = 1;
		- _dCamFocusDist = -1;
		- _dCamBank = 1;
		- _dCamFov = 0.7;
		- _dCamFovRangeHigh = 0.7;
		- _dCamFovRangeLow = 0.7;
		*/
		Camera(CAMEFFECTTYPE camEffectType, CAMEFFECTPOS camEffectPos, position3D target);

		/*!
		 Copy Constructor 
		 */
		Camera(const Camera& _camerar);

		/*!
		Assignment operator for Camera class.
		Assign all the attributes of Camera class.
		Also call the assignment operator of ControllableObject class.
		*/
		Camera& operator=(const Camera& _cam);

		/*!
		Main destructor for this class. 
		*/
		~Camera();

		/*! Sets the camera effect type of the camera using a CAMEFFECTTYPE variable.
		 \param camEffectType is the camera effect type it is an enumerated value defined in the header file.
			values can be type of CAMEFFECTTYPE_INTERNAL, CAMEFFECTTYPE_EXTERNAL, CAMEFFECTTYPE_FIXED, 
			CAMEFFECTTYPE_FIXEDWITHZOOM, CAMEFFECTTYPE_TERMINATE
		*/

		void setCamEffectType(CAMEFFECTTYPE camEffectType);

		/*!
		Returns the effect type of the camera using a CAMEFFECTTYPE variable. will be one of the enum values 
		CAMEFFECTTYPE_INTERNAL, CAMEFFECTTYPE_EXTERNAL, CAMEFFECTTYPE_FIXED, CAMEFFECTTYPE_FIXEDWITHZOOM, CAMEFFECTTYPE_TERMINATE 
		
		*/

		CAMEFFECTTYPE getCamEffectType() const;

		/*!
		Sets the camera effect type of the camera using a string variable.
		  \param strCamEffectType Should be one of("INTERNAL", "EXTERNAL", "FIXED", "FIXEDWITHZOOM", "TERMINATE")		
		*/
		void setCamEffectType(string strCamEffectType);

		/*!
		Returns the effect type of the camera using a string variable. will be be one of the 
		("INTERNAL", "EXTERNAL", "FIXED", "FIXEDWITHZOOM" "TERMINATE")	values 	
		*/
		string getCamEffectTypeString() const;
		
		/*!
		Sets the camera effect position of the camera using a CAMEFFECTPOS variable.
		 \param camEffectPos is the camera effect type it is an enumerated value defined in the header file.
			values can be type of CAMEFFECTPOS_TOP, CAMEFFECTPOS_LEFT, CAMEFFECTPOS_RIGHT, CAMEFFECTPOS_FRONT, CAMEFFECTPOS_BACK
					
		*/		

		void setCamEffectPos(CAMEFFECTPOS camEffectPos);

		
		/*!
		Returns the position of the effect of camera using a CAMEFFECTPOS variable.  Will be one of the enum values 
		type of CAMEFFECTPOS_TOP, CAMEFFECTPOS_LEFT, CAMEFFECTPOS_RIGHT, CAMEFFECTPOS_FRONT, CAMEFFECTPOS_BACK.		
		*/

		CAMEFFECTPOS getCamEffectPos() const;

		/*!
		Sets the camera effect position of the camera using a string variable.
		  \param strCamEffectPos Should be one of ("TOP","LEFT","RIGHT","FRONT","BACK")		
		*/

		void setCamEffectPos(string strCamEffectPos);

		/*!
		Returns the position of the effect of camera using a string variable. 
		Will be one of the enum values type of ("TOP", "LEFT", "RIGHT", "FRONT", "BACK")		
		*/

		string getCamEffectPosString() const;
		
		/*!Sets target of the camera. 
			\param  camTarget is a position3D object. The object should contain values 
			for X - right, Y - Up & Z-front 
		*/
		void setCamTarget(position3D camTarget);

		/*!Sets target location of the camera. 
			\param x - right,
			\param y - Up  
			\param z - front 
		*/

		void setCamTarget(double x, double y, double z);

		/*!
		Returns the camera's target. 
		*/
		position3D getCamTarget() const;

		/*!Sets relative position of the camera. (relative to target)
			\param camRelPos is a position3D object. The object should contain values 
			for X - right, Y - Up & Z-front 
		*/
		void setCamRelPos(position3D camRelPos);

		/*!Sets relative position of the camera using double values of position coordinates. (relative to target)
			\param camX - right,
			\param camY - Up  
			\param camZ - front 
		*/
		void setCamRelPos(double camX, double camY, double camZ);

		/*!
		Returns the camera's relative position. (relative to target) 
		*/
		position3D getCamRelPos() const;

		/*!
		Sets commit time of the camera.
		\param dCamCommit - the commit time of the camera,
		*/
		void setCamCommit(double dCamCommit);
		
		/*!
		Returns the commit time in seconds during which the changes shall be done
		of the camera. 
		*/
		double getCamCommit() const;
		/*!
		Sets focus distance of the camera.
		*/
		void setCamFocusDist(double dCamFocusDist);
		
		/*!
		Returns distance value for Focus range of the camera. 
		*/
		double getCamFocusDist() const;

		/*!
		Sets focus blur value of the camera.
		*/
		void setCamFocusBlur(double dCamFocusBlur);
		
		/*!
		Returns the blur value for Focus range of the camera. 
		*/
		double getCamFocusBlur() const;

		/*!
		Sets the field of view (fov) value of the camera. 
		*/
		void setCamFov(double dCamFov);

		/*!
		Returns the field of view (fov) value of the camera. 
		*/
		double getCamFov() const;

		



	private:
		CAMEFFECTTYPE _camEffectType;
		CAMEFFECTPOS _camEffectPos;
		position3D _camTarget;
		position3D _camRelPos;
		double _dCamFocusDist;
		double _dCamFocusBlur;
		double _dCamFov;
		double _dCamCommit;
		void initCamera();
	};
}; 

#endif //VBS2FUSION_CAMERA_H
