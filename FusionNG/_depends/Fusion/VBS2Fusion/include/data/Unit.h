/*
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 */

/*************************************************************************

Name:

	Unit.h

Purpose:

	This file contains the declaration of the Unit class.

Version Information:

	Version		Date		Author and Comments
	===========================================
	1.0			26-03/2009	RMR: Original Implementation
	2.0			10-02/2010	YFP: Added	operators ==,<
										getLogFileName()							
	2.01		11-02/2010	MHA: Comments Checked
	2.02		06-10/2010  YFP" Added copy constructor

/************************************************************************/

#ifndef VBS2FUSION_UNIT_H
#define VBS2FUSION_UNIT_H

/***********************************************************************/
/* INCLUDES
/***********************************************************************/
// STANDARD INCLUDES
#include <sstream>
#include <string>

// SIMCENTRIC INCLUDES
#include "position3D.h"
#include "VBS2Fusion.h"
#include "VBS2FusionDefinitions.h"
#include "data/ControllableObject.h"
#include "data/UnitArmedModule.h"
#include "Math/Matrix4f.h"

/**********************************************************************/
/* END INCLUDES
/**********************************************************************/

namespace VBS2Fusion
{

	class VBS2FUSION_API Unit: public ControllableObject
	{
	public:

		//****************************************************************
		/*!
		Main constructor for this class. The following parameters are initialized as follows:

		- _dSkill = 0.5;
		- string type("VBS2_AU_Army_Rifleman_W_F88A1");
		- setType(type);
		- setRank(PRIVATE);
		- _strGroup = "grpNull";
		- _strGroupAlias = "grpNull";
		*/
		Unit();

		/*!
		Main constructor for this class. The following parameters are initialized as follows:

		- _dSkill = 0.5;
		- string type("VBS2_AU_Army_Rifleman_W_F88A1");
		- setType(type);
		- setRank(PRIVATE);
		- _strGroup = "grpNull";
		- _strGroupAlias = "grpNull";
		- _strName = strName;
		*/
		Unit(string strName);

		/*!
		Constructor taking the networkId as the parameter
		*/
		Unit(NetworkID& id);

		/*!
		Main constructor for this class. The following parameters are initialized as follows:

		- _dSkill = 0.5;
		- string type("VBS2_AU_Army_Rifleman_W_F88A1");
		- setType(type);
		- setRank(PRIVATE);
		- _strGroup = "grpNull";
		- _strGroupAlias = "grpNull";
		- _strName = strName;
		- _strInitialization = strInitialization;
		*/
		Unit(string strName, string strInitialization);

		/*!
		Main constructor for this class. The following parameters are initialized as follows:

		- _dSkill = 0.5;
		- string type("VBS2_AU_Army_Rifleman_W_F88A1");
		- setType(type);
		- setRank(PRIVATE);
		- _strGroup = "grpNull";
		- _strGroupAlias = "grpNull";
		- _strName = strName;
		- _strInitialization = strInitialization;
		- _position = position;
		*/
		Unit(string strName, string strInitialization, position3D position);

		/*!
		Constructor for the Unit class. 

		The following variables are initialized as follows:

		_strAlias = "";
		_strName = name;
		_strID = "";
		_strInitializationStatements = "";
		_bPlayer = false;
		_bPlayable = false;
		_position = position;
		*/
		Unit(string& name, position3D& position);
		
		/*!
		Main constructor for this class. The following parameters are initialized as follows:

		- _dSkill = 0.5;
		- string type("VBS2_AU_Army_Rifleman_W_F88A1");
		- setType(type);
		- setRank(PRIVATE);
		- _strGroup = "grpNull";
		- _strGroupAlias = "grpNull";
		- _strName = strName;
		- _strInitialization = strInitialization;
		- _position = position;
		- _strGroup = strGroup;
		*/
		Unit(string strName, string strInitialization, position3D position, string strGroup);

		/*!
		Main destructor for this class. 
		*/
		~Unit();

		/*!
		Copy constructor for the unit class. 
		*/
		Unit(const Unit& unit);


		//****************************************************************
		/*!
		Returns a string representing the stance of the unit. Will be one of 
		"UP" , "DOWN" or "AUTO".
		*/
		string getUnitPosString();

		/*!
		Returns a string representing the stance of the unit. Will be one of 
		UP , DOWN or AUTO.
		*/
		UNITPOS getUnitPos() const;

		/*!
		Sets unit position to UP.
		*/
		void UP();

		/*!
		Sets unit position to DOWN.
		*/
		void DOWN();

		/*!
		Sets unit position to AUTO.
		*/
		void AUTO();

		//****************************************************************

		/*!
		Sets skill level of the unit. 
		*/		
		void setSkill(double skill);

		/*!
		Returns skill level of the unit. 
		*/
		double getSkill() const;


		//****************************************************************

		/*!
		Sets the rank of the unit using a string. Should be one of
		("PRIVATE", "CORPORAL", "SERGEANT", "LIEUTENANT", "CAPTAIN", "MAJOR" or
		"COLONEL")
		*/
		void setRank(string strRank);

		/*!
		Sets the rank of the unit using a VBSRANK variable. Should be one of
		(PRIVATE, CORPORAL, SERGEANT, LIEUTENANT, CAPTAIN, MAJOR, COLONEL)		
		*/
		void setRank(VBSRANK rank);

		/*!
		Returns the rank of the unit using a string. Should be one of
		("PRIVATE", "CORPORAL", "SERGEANT", "LIEUTENANT", "CAPTAIN", "MAJOR" or
		"COLONEL")
		*/
		string getRankString() const;

		/*!
		Returns the rank of the unit using a VBSRANK variable. Should be one of
		(PRIVATE, CORPORAL, SERGEANT, LIEUTENANT, CAPTAIN, MAJOR, COLONEL)		
		*/
		VBSRANK Rank() const;

		//****************************************************************

		/*!
		Sets the endurance of the unit. 
		*/
		void setEndurance(double endurance);

		/*!
		Returns the endurance of the unit. 
		*/
		double getEndurance() const;

		//****************************************************************

		/*!
		Sets the experience of the unit. 
		*/
		void setExperience(double experience);

		/*!
		Returns the experience of the unit. 
		*/
		double getExperience() const;

		//****************************************************************

		/*!
		Sets the fatigue level of the unit. 
		*/
		void setFatigue(double fatigue);

		/*!
		Returns the fatigue level of the unit. 
		*/
		double getFatigue() const;

		//****************************************************************

		/*!
		Sets the leadership level of the unit. 
		*/
		void setLeadership(double leadership);

		/*!
		Returns the leadership level of the unit. 
		*/
		double getLeadership() const;

		//****************************************************************

		/*!
		Sets the name of the group the unit belongs to. 
		*/
		void setGroup(string strGroup);

		/*!
		Returns the name of the group the unit belongs to. 
		*/
		string getGroup() const;

		//****************************************************************

		/*!
		Sets the VBS2Fusion alias of the group the unit belongs to. 
		*/
		void setGroupAlias(string strGroupAlias);

		/*!
		Returns the VBS2Fusion alias of the group the unit belongs to. 
		*/
		string getGroupAlias() const;

		//****************************************************************

		/*!
		Assignment operator for Unit class.
		Assign all the attributes of Unit class.
		Also call the assignment operator of ControllableObject class.
		*/
		Unit& operator=(const Unit& u);

		/*!
		Equal operator for Unit class.
		Compared by 
			- NetworkID: if both units are global.
			- name: if both units are local.
			- otherwise return false.
		*/
		bool operator==(const Unit& u);

		/*!
		Less than operator for Unit class.
		Compared by 
			- NetworkID: if both units are global.
			- name: otherwise.
		*/
		bool operator < (const Unit& u) const;

		//****************************************************************

		/*!
		Updates the unit using the information in u. 
		*/
		void update(Unit& u);

		//****************************************************************

		/*!
		Virtual function to handle GetInMan events from the unit. This method is called
		by an EventHandler object in the case of an 'GetInMan' event being called. 

		A 'GetInMan' event handler for the relevant object should first be added
		using addGetInManEvent on a global EventHandler object. 

		Inherit from this class and rewrite the processRespawnEvent function 
		to provide customized functionality. 

		Current functionality does not do anything. Left empty for creating custom 
		behavior. 
		*/
		virtual void processGetInManEvent(GetInManEvent& _event);

		
		/*!
		Virtual function to handle GetOutMan events from the unit. This method is called
		by an EventHandler object in the case of an 'GetOutMan' event being called. 

		A 'GetOutMan' event handler for the relevant object should first be added
		using addGetOutManEvent on a global EventHandler object. 

		Inherit from this class and rewrite the processRespawnEvent function 
		to provide customized functionality. 

		Current functionality does not do anything. Left empty for creating custom 
		behavior. 
		*/
		virtual void processGetOutManEvent(GetOutManEvent& _event);

		/*!
		Virtual function to handle Suppressed events from the unit. This method is called
		by an EventHandler object in the case of an 'Suppressed' event being called. 

		A 'Suppressed' event handler for the relevant object should first be added
		using addSuppressedEvent on a global EventHandler object. 

		Inherit from this class and rewrite the processRespawnEvent function 
		to provide customized functionality. 

		Current functionality does not do anything. Left empty for creating custom 
		behavior. 
		*/
		virtual void processSuppressedEvent(SuppressedEvent& _event);

		/*!
		Iterator for Magazine list.
		*/
		typedef list<Magazine>::iterator magazines_iterator;

		/*!
		Constant iterator for magazine list.
		*/
		typedef list<Magazine>::const_iterator magazines_const_iterator;

		/*!
		Iterator for weapons list.
		*/
		typedef list<Weapon>::iterator weapons_iterator;

		/*!
		Constant iterator for weapons list.
		*/
		typedef list<Weapon>::const_iterator weapons_const_iterator;

		/*!
		Sets the name of the primary weapon. 
		*/
		void setPrimaryWeapon(string weaponName); 

		/*!
		Returns the name of the primary weapon. 
		*/
		string getPrimaryWeapon() const; 

		/*!
		Returns the ArmedModule of the Unit. 
		*/
		UnitArmedModule* getArmedModule() const;

		/*!
		Set Armed module to Unit. 
		*/
		void setArmedModule(UnitArmedModule* uamodule);

		/*!
		Adds a new weapon using a weapon object. 
		*/
		void addWeapon(Weapon weapons);

		/*!
		Adds a new weapon using a class name. 
		*/
		void addWeapon(string weaponsName);
		
		/*!
		Clears the weapons list. 
		*/
		void clearWeaponsList();

		/*!
		Returns the number of weapons currently assigned. 
		*/
		int getNumberOfWeapons()const;

		/*!
		Removes weapon with class name weaponName from list. Returns false if
		operation was unsuccessful due to the weapon not being on the list. 
		*/
		bool removeWeapon(string weaponName);

		/*!
		Add magazine to the unit using magazine Object.
		*/
		void addMagazine(Magazine magazine);

		/*!
		Adds a new magazine using a class name. Sets the number of magazines
		to 0 and clears the ammo amount list. 
		*/
		void addMagazine(string magazineName);

		/*!
		Clears the magazine list. 
		*/		
		void clearMagazinesList();

		/*!
		Returns the number of magazine types currently assigned. 
		*/
		int getNumberOfMagazines();

		/*!
		Removes magazine with class name magazineName from list. Returns false if
		operation was unsuccessful due to the magazine not being on the list. 
		*/
		bool removeMagazine(string magazineName);

		/*!
		Returns true of a magazine with the class name magazineName
		is on the list. 
		*/
		bool isMagazineAdded(string magazineName);

		/*!
		Begin iterator for the magazine list.
		*/
		magazines_iterator magazines_begin();

		/*!
		Begin const iterator for the magazine list.
		*/
		magazines_const_iterator magazines_begin() const;

		/*!
		End iterator for the magazine list.
		*/
		magazines_iterator magazines_end();

		/*!
		Begin const iterator for the magazine list.
		*/
		magazines_const_iterator magazines_end() const;

		/*!
		Begin iterator for the weapons list.
		*/
		weapons_iterator weapons_begin();

		/*!
		Begin const iterator for the weapons list.
		*/
		weapons_const_iterator weapons_begin() const;

		/*!
		End iterator for the weapons list.
		*/
		weapons_iterator weapons_end();

		/*!
		End const iterator for the weapons list.
		*/
		weapons_const_iterator weapons_end() const;


		/*!
		Virtual function that called everytime when Upper view of the unit
		get modify using external pose controlling.
		*/
		virtual bool onModifyUpperView(Matrix4f& view);

		/*!
		Virtual function that called everytime when Upper aim of the unit
		get modify using external pose controlling.
		*/
		virtual bool onModifyUpperAim(Matrix4f& aim);

		/*!
		Virtual function that called everytime when Upper troso of the unit
		get modify using external pose controlling.
		*/
		virtual bool onModifyUpperTroso(Matrix4f& troso);

		/*!
		Virtual function that called everytime when Upper view of the unit
		get modify using external pose controlling.
		*/
		virtual bool onModifyBone(Matrix4f& mat, SKELETON_TYPE skelType);

#if _BISIM_DEV_EXTERNAL_POSE
		/*!
		Virtual function that called every time when gun transformation of the unit
		get modify using external pose controlling.
		*/
    virtual bool onModifyGunTrans(Matrix4f& mat);

    /*!
		Virtual function that called everytime when head transformation view of the unit
		get modify using external pose controlling.
		*/
    virtual bool onModifyHeadTrans(Matrix4f& mat);

		/*!
		Virtual function that called everytime when look transformation view of the unit
		get modify using external pose controlling.
		*/
    virtual bool onModifyLookTrans(Matrix4f& mat);

		/*!
		Virtual function that called everytime when relative position of the unit
		get modify using external pose controlling.
		*/
    virtual bool onModifyRelativeChange(Matrix4f& mat);

		/*!
		Virtual function that called everytime when unit reloads
		*/
    virtual bool onReload();
#endif

	private:
		string _strGroup;
		string _strGroupAlias;
		double _dSkill;
		VBSRANK _rank;
		double _dEndurance;
		double _dExperience;
		double _dFatigue;
		double _dLeadership;
		UNITPOS _unitPosition;	


		/*!
			Init function that is used for initialize the Unit object. This function adds a solider.
			  skill value	= 0.5
			  type			= VBS2_AU_Army_Rifleman_W_F88A1
			  rank			= PRIVATE 
			group and group alias are null. 			
		*/
		void initUnit();
		UnitArmedModule *_uarmedModule;

	};
}; 

#endif //UNIT_H