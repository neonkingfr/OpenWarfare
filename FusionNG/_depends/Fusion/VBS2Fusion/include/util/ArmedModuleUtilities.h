/*
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 */

/*************************************************************************

Name:

ArmedModuleUtilities.h

Purpose:

This file contains the declaration of the ArmedModuleUtilities class.

Version Information:

Version		Date		Author and Comments
===========================================
2.0			29-01/2010	YFP: Original Implementation
2.01		12-02/2010	MHA: Comments Checked

/************************************************************************/


#ifndef VBS2FUSION_ARMEDMODULEUTILITES_H
#define VBS2FUSION_ARMEDMODULEUTILITES_H

/**************************************************************************
To disable the warnings arise because of using deprecated ArmedModule 
inside the ArmedModuleUtilites class.
Warning identifier [C:4996]
/**************************************************************************/
#pragma warning (push)
#pragma warning (disable: 4996)

/***********************************************************************/
/* INCLUDES
/***********************************************************************/
// STANDARD INCLUDES

// SIMCENTRIC INCLUDES
#include "VBS2Fusion.h"
/**********************************************************************/
/* END INCLUDES
/**********************************************************************/

namespace VBS2Fusion
{
	class VBS2FUSION_API VBS2FUSION_DPR(UARM) ArmedModuleUtilites
	{
	public:

		/*!
			Load Turrets available in the vehicle to armed module. 
			Magazines and weapons available inside the turret will be loaded 
			into the turret object as well.
		*/
		static void loadTurrets(Vehicle& vehicle, ArmedModule& module);

		/*!
			Load Magazines available in the vehicle to armed module. 		
		*/
		static void loadMagazines(Vehicle& vehicle, ArmedModule& module);

		/*!
			Update the armed module.
		*/
		static void updateArmedModule(Vehicle& vehicle, ArmedModule& module);
		
	};
};
#pragma warning (pop) // Enable warnings [C:4996]

#endif