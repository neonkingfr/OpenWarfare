
 /**************************************************************************
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 **************************************************************************/

/*****************************************************************************

Name: PoseControlUtilities.h

Purpose: External pose control functions are declared in this file. 

	
Version Information:

	Version		Date		Author and Comments
	===========================================
	1.0			09-11/2010	YFP: Original Implementation

/*****************************************************************************/

#ifndef VBS2FUSION_POSECONTROLUTILITIES_H
#define VBS2FUSION_POSECONTROLUTILITIES_H



/*****************************************************************************/
/* INCLUDES
/*****************************************************************************/
// STANDARD INCLUDES

// SIMCENTRIC INCLUDES
#include "VBS2Fusion.h"
#include "data/Unit.h"
/*****************************************************************************/
/* END INCLUDES
/*****************************************************************************/

namespace VBS2Fusion
{

	class VBS2FUSION_API PoseControlUtilities
	{
	public:
		/************************************************************************/
		/* EXTERNAL POSE CONTROL                                                */
		/************************************************************************/

		/*!
		Enable/Disable the ability to modify pose of specified unit externally.

		Deprecated. Use void applyExternalPose(Unit& unit, bool status)
		*/
		VBS2FUSION_DPR(UPCN001) static void setExternalPose(Unit& unit, bool status);

		/*!
		Returns true if pose of given unit is controlled externally. 
		*/
		static bool getExternalPose(Unit& unit);

		/************************************************************************/
		/* UPPER BODY CONTROL                                                   */
		/************************************************************************/

		/*!
		Enable/Disable upper body pose control of specified unit.
		Upper Body pose control of unit is disabled by default.

		Deprecated. Use void applyExternalPoseUpBody(Unit& unit ,bool status)
		*/
		VBS2FUSION_DPR(UPCN003) static void setExternalPoseUpBody(Unit& unit ,bool status);

		/*!
		Returns true if upper body pose of given unit is Enabled.
		*/
		static bool getExternalPoseUpBody(Unit& unit);


		/************************************************************************/
		/* SKELETON CONTROL                                                    */
		/************************************************************************/

		/*!
		Enable/Disable skeleton control of a given unit.
		Skeleton control is disabled by default.

		Deprecated. Use void applyExternalPoseSkeleton(Unit& unit, bool status)
		*/
		VBS2FUSION_DPR(UPCN002) static void setExternalPoseSkeleton(Unit& unit, bool status);

		/*!
		Return true if skeleton control is enabled.
		*/
		static bool getExternalPoseSkeleton(Unit& unit);

#if _BISIM_DEV_EXTERNAL_POSE
		/*!
		Enable/Disable skeleton movement of a given unit.
		Skeleton movement control is disabled by default.
		*/
    static void setExternalMovementControlled(Unit& unit, bool status);

		/*!
		Return true if skeleton movement control is enabled.
		*/
    static bool getExternalMovementControlled(Unit& unit);

		/*!
		Enable/Disable camera control of a given unit.
		Camera control is disabled by default.
		*/
    static void setExternalCameraControlled(Unit& unit, bool status);

		/*!
		Return true if camera control is enabled.
		*/    
    static bool getExternalCameraControlled(Unit& unit);

		/*!
		Enable/Disable lower body skeleton control of a given unit.
		Lower body skeleton control is disabled by default.
		*/
    static void setExternalPoseLowerBody(Unit& unit, bool status);

		/*!
		Return true if lower body skeleton control is enabled.
		*/    
    static bool getExternalPoseLowerBody(Unit& unit);

    /*!
		Return true if view is aligned with head.
		*/    
    static bool isViewAlignedWithHead();
#endif

		/*!
		Disable Pose controlling of the unit. 
		If user unload the fusion plug in during the mission. 
		This should be called in onUnload callback.
		*/
		static bool DisablePoseControl(Unit& unit);

		/*!
		@description 

		Enable/Disable the ability to modify pose of specified unit externally.

		@locality

		Locally applied, Locally effected

		@version  [VBS2Fusion v3.15]

		@param unit - unit that going to modify the pose.
		@param status - True or false. Enable or desable the ability to modify the pose externally.

		@return None

		@example

		@code

		PoseControlUtilities::applyExternalPose(unit1, true);

		@endcode

		@overloaded None

		@related 

		@remarks This is a replication of void setExternalPose(Unit& unit, bool status)
		*/
		static void applyExternalPose(Unit& unit, bool status);
		
		/*!
		@description 

		Enable/Disable skeleton control of a given unit.
		Skeleton control is disabled by default.

		@locality
		
		Locally applied, Locally effected

		@version  [VBS2Fusion v3.15]

		@param unit - unit that going to modify the pose.
		@param status - True or false. Enable or disable the ability to modify the skeleton externally.

		@return None

		@example

		@code
		
		//Create the pose control unit
		PoseControlUnit pcUnit;
		playerUnit = MissionUtilities::getPlayer();

		position3D pos(playerUnit.getPosition().getX()+3,
		playerUnit.getPosition().getZ(),
		playerUnit.getPosition().getY());

		UnitUtilities::CreateUnit(pcUnit, pos);

		//Activate applyExternalPoseSkeleton
		PoseControlUtilities::applyExternalPose(pcUnit, true);
		PoseControlUtilities::applyExternalPoseSkeleton(pcUnit, true);

		//Deactivate applyExternalPoseSkeleton
		PoseControlUtilities::applyExternalPoseSkeleton(pcUnit, false);

		@endcode

		@overloaded None

		@related 
		
		PoseControlUtilities::applyExternalPose(Unit& unit, bool status)

		@remarks This is a replication of void setExternalPoseSkeleton(Unit& unit, bool status)
		*/
		static void applyExternalPoseSkeleton(Unit& unit, bool status);

		/*!
		@description 

		Enable/Disable upper body pose control of specified unit.
		Upper Body pose control of unit is disabled by default.

		@locality
		
		Locally applied, Locally effected

		@version  [VBS2Fusion v3.15]

		@param unit - unit that need to have torso/view/aim matrices externally controlled.
		@param status - True or false. Enable or disable the ability to modify the torso/view/aim matrices externally controlled.

		@return None

		@example

		@code

		//Create the pose control unit
		PoseControlUnit pcUnit;
		playerUnit = MissionUtilities::getPlayer();

		position3D pos(playerUnit.getPosition().getX()+3,
		playerUnit.getPosition().getZ(),
		playerUnit.getPosition().getY());

		UnitUtilities::CreateUnit(pcUnit, pos);

		//Apply external pose to unit's upper body
		PoseControlUtilities::applyExternalPose(pcUnit, true);
		PoseControlUtilities::applyExternalPoseUpBody(pcUnit, true);

		//Deactivate external pose to unit's upper body
		PoseControlUtilities::applyExternalPoseUpBody(pcUnit, false);

		@endcode

		@overloaded None

		@related 
		
		PoseControlUtilities::applyExternalPose(Unit& unit, bool status)

		@remarks This is a replication of void setExternalPoseUpBody(Unit& unit ,bool status)
		*/
		static void applyExternalPoseUpBody(Unit& unit ,bool status);

#if _BISIM_DEV_EXTERNAL_POSE
    /*!
		Return Id of bone for given unit
		*/   
    static int findBone(Unit& unit,string boneName);
#endif
	};
}


#endif //VBS2FUSION_POSECONTROLUTILITIES_H