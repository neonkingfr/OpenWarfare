
 /**************************************************************************
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 **************************************************************************/


/*************************************************************************

Name:

	CameraUtilities.h

Purpose:

	This file contains the declaration of the CameraUtilities class.

Version Information:

	Version		Date		Author and Comments
	===========================================
	1.0			14-09/2009	SLA: Original Implementation
	2.0			10-01/2009	UDW: Version 2 Implementation
	2.01		10-02/2010	MHA: Comments Checked
	2.02		27-09/2011	NDB: Added Methods
									position3D positionCameraToWorld(position3D camPos)
									void applyCameraPosition(Camera& camera, position3D position)
									void applyCameraRelativePosition(Camera& camera, position3D position)
									void applyCameraFov(Camera& camera, double level)
									void applyCameraFocus(Camera& camera, double distance, double blur)
									void applyCameraCommit(Camera& camera, double time)
									void applyCameraTarget(Camera& camera, ControllableObject& target)
									void applyCameraTarget(Camera& camera, position3D& target)

/************************************************************************/

#ifndef VBS2FUSION_CAMERA_UTILITIES_H
#define VBS2FUSION_CAMERA_UTILITIES_H

#include "position3D.h"
#include "VBS2Fusion.h"
#include "data/Camera.h"
#include "util/ExecutionUtilities.h"
#include "util/ControllableObjectUtilities.h"
#include "VBS2FusionDefinitions.h"

namespace VBS2Fusion
{		
	class VBS2FUSION_API CameraUtilities: public ControllableObjectUtilities
	{
	public:


		//********************************************************
		// Camera Effect utilities
		//********************************************************

		/*!
		Applies the effect specified by c.getCamEffectType() and c.getCamEffectPos() to the camera. 		
		\param c - A reference to a currently implemented camera object ,
 
		*/
		static void applyCamEffect(Camera& c);

		/*!
		Applies the given effect type at a given effect position, specified by user to the camera.
		 If you want to switch the screen directly to the first-person, aiming, third-person or 
		 group view of an object, use switchCamera instead.The effect type "Terminate" is used to 
		 exit the current camera view and switch back to the player's view.Needs the call of camCommit 
		 to be conducted. 
	
		 name is taken by the camEffectType and the position is taken by the camEffectPos

		-Error checking utility validates the following:
		- CameraAlias (outputs an error if the camera alias is invalid)

		\param c - A reference to a currently implemented camera object ,
		\param camEffectType is the camera effect type it which is enumerated in the header file.
			values can be type of CAMEFFECTTYPE_INTERNAL, CAMEFFECTTYPE_EXTERNAL, CAMEFFECTTYPE_FIXED, 
			CAMEFFECTTYPE_FIXEDWITHZOOM, CAMEFFECTTYPE_TERMINATE. 
		\param camEffectPos is the camera effect type which is an enumerated value defined in the header file.
			values can be type of CAMEFFECTPOS_TOP, CAMEFFECTPOS_LEFT, CAMEFFECTPOS_RIGHT, CAMEFFECTPOS_FRONT,
			CAMEFFECTPOS_BACK.
		*/
		static void applyCamEffect(Camera& c, CAMEFFECTTYPE camEffectType, CAMEFFECTPOS camEffectPos);


		//********************************************************
		// Camera Target Position utilities
		//********************************************************
	
		/*!
		Applies the camera target for the position of the camera. 		
		camera is taken by the reference to the camera and position is taken by the c.getCamTarget() 
		\param c - A reference to a currently implemented camera object.
		*/

		static void applyCamTarget(Camera& c);

		/*!
		Applies the target position of the camera for the position specified by the camTarget.		
		camera is taken by the reference to the camera and position is taken by the camTarget 
	
		\param c - A reference to a currently implemented camera object 
		\param camTarget - A postion3D object where the camera will target it self to 
		*/


		static void applyCamTarget(Camera& c, position3D camTarget);

		/*!
		Applies the target position of the camera for the position of the target object.		
		camera is taken by the reference to the camera and 
		position given by the position of the ControllableObject passed as an argument 
	
		\param c - A reference to a currently implemented camera object 
		\param coTarget - the ControllableObject form which the position will be acquired. 
		*/
		static void applyCamTarget(Camera& c, ControllableObject& coTarget);

		//********************************************************
		// Camera Position utilities
		//********************************************************

		/*!
		Returns the position of camera in the VBS2 environment. The position is called
		using the alias of the camera

		Error checking utility validates the following:
		- CameraAlias (outputs an error if the camera alias is invalid)

		\param c - A reference to a currently implemented camera object 
		*/

		static position3D getCamPos(Camera& c);

		/*!
		Acquires the camera position in game environment by using getCamPos(c) 
		and update it to the camera object in VBS2Fusion. 		

		Error checking utility validates the following:

		- CameraAlias (outputs an error if the camera alias is invalid)

		\param c - A reference to a currently implemented camera object 

		*/
		static void updateCamPos(Camera& c);

		/*!
		Applies the camera position specified in c.getPosition() to the camera. 
		Prepares the camera position		
		position is acquired by the c.getPosition()

		Error checking utility validates the following:

		- CameraAlias (outputs an error if the camera alias is invalid)

		\param c - A reference to a currently implemented camera object 

		*/
		static void applyCamPos(Camera& c);

		/*!
		Applies the camera position specified by the camPos object. 
		position is given by the camPos 

		Error checking utility validates the following:

		- CameraAlias (outputs an error if the camera alias is invalid)

		\param c - A reference to a currently implemented camera object 
		\param camPos - A postion3D object where the camera will prepare it self to 

		*/
		static void applyCamPos(Camera& c, position3D camPos);

		//********************************************************
		// Camera Relative Position utilities (Relative to Target)
		//********************************************************

		/*!
		Applies the camera relative position specified in c.getRelPos() to the camera. 
		Prepares the camera position relative to the current position of the current target
		
		position is acquired by the c.getRelPos()

		Error checking utility validates the following:

		- CameraAlias (outputs an error if the camera alias is invalid)

		\param c - A reference to a currently implemented camera object 

		*/

		static void applyCamRelPos(Camera& c);

		/*!
		Applies the camera relative position specified in camRelPos to the camera.  
		Prepares the camera position relative to the current position of the current target
		
		position is acquired by camRelPos

		Error checking utility validates the following:

		- CameraAlias (outputs an error if the camera alias is invalid)

		\param c - A reference to a currently implemented camera object 
		\param camRelPos - A postion3D object where the camera will prepare it self to 
		*/

		static void applyCamRelPos(Camera& c, position3D camRelPos);

		//********************************************************
		// Camera Commit Time utilities 
		//********************************************************

		/*!
		Applies the camera commit time specified in camCommit to the camera. 
		Smoothly conduct the changes that were assigned to a camera within the given time. 
		If the time is set to zero, the changes are done immediately.

		Error checking utility validates the following:

		- CameraAlias (outputs an error if the camera alias is invalid)

		\param c - A reference to a currently implemented camera object 
		\param camCommit - The time the variations should take place 
		*/
		static void applyCamCommit(Camera& c, double camCommit);

		/*!
		Returns the camera committed status.
		If camera is committed, returns true, else false.

		Error checking utility validates the following:

		- CameraAlias (outputs an error if the camera alias is invalid)

		\param c - A reference to a currently implemented camera object 

		*/

		static bool isCamCommitted(Camera& c);

		//********************************************************
		// Camera Focus utilities 
		//********************************************************

		/*!
		Applies the camera focus specified in c.getFocusDist() and c.getFocusBlur() to the camera. 
		Prepares the camera focus blur.[-1,1] will reset default values (auto focusing), [-1,-1] 
		will disable post processing (all is focused).
		
		Error checking utility validates the following:

		- CameraAlias (outputs an error if the camera alias is invalid)

		\param c - A reference to a currently implemented camera object 
		*/
		static void applyCamFocus(Camera& c);

		/*!
		Applies the camera focus specified in camFocusDist and camFocusBlur to the camera.
		Prepares the camera focus blur.[-1,1] will reset default values (auto focusing), [-1,-1] 
		will disable post processing (all is focused).

		Error checking utility validates the following:

		- CameraAlias (outputs an error if the camera alias is invalid)

		\param c - A reference to a currently implemented camera object 
		\param camFocusDist - Camera focus distance  
		\param camFocusBlur - camera focus Blur level  
		*/

		static void applyCamFocus(Camera& c, double camFocusDist, double camFocusBlur);

		//********************************************************
		// Camera Field of View (Fov) utilities 
		//********************************************************


/*!
		Returns the Fov of camera.Get camera FOV in form [minFov,maxFov] 
		
		Error checking utility validates the following:

		- CameraAlias (outputs an error if the camera alias is invalid)

		\param c - A reference to a currently implemented camera object 

		*/

		static vector<double> getCamFov(Camera& c);


		

		/*!	
		Get the camera Fov in game environment by using getCamFov(c) 
		and update it to the camera object in VBS2Fusion. Uses the setCamFov 
		method in the camera class.
	
		Error checking utility validates the following:

		- CameraAlias (outputs an error if the camera alias is invalid)

		\param c - A reference to a currently implemented camera object 

		*/

		static void updateCamFov(Camera& c);

		/*!	
		Applies the camera Fov specified in c.getCamFov() to the camera. 
		Prepares the camera field of view (zoom). 
		Error checking utility validates the following:

		- CameraAlias (outputs an error if the camera alias is invalid)

		\param  c - A reference to a currently implemented camera object 

		*/

		static void applyCamFov(Camera& c);


		/*!	
		Applies the camera Fov specified in camFov to the camera. 
		Prepares the camera field of view (zoom). 
		
		Error checking utility validates the following:

		- CameraAlias (outputs an error if the camera alias is invalid)

		\param  c - A reference to a currently implemented camera object 
		\param  camFov - the fov value specified  

		*/

		static void applyCamFov(Camera& c, double camFov);


		//********************************************************
		// Create Camera
		//********************************************************

		/*!
		Creates a new Camera. 
		Camera object need to be passed to this function and the properties that has camera object will be used during the camera creation.

		-Camera effect type, 
		-Camera effect position,
		-Camera target, 
		-Camera relative position.

		If no alias is specified in camera object, a random alias is generated.

		*/
		static void createCamera(Camera& c);

		/*!
		Creates a new Camera. 
		Camera object need to be passed to this function and the properties that has camera object will be used during the camera creation. 
		Additionally user defined camAlias value will be set.

		\param c - A reference to a currently implemented camera object 
		\param camAlias - The alias the camera should use 
 
		*/
		static void createCamera(Camera& c, string camAlias);

		/*!
		Creates a new Camera. 
		Camera object need to be passed to this function and the properties that has camera object will be used during the camera creation. 
		Additionally user defined camPos, camTarget, camAlias values will be set.

		\param c - A reference to a currently implemented camera object 
		\param camPos - The position the camera should create itself
		\param camTarget - The position the camera should focus itself
		\param camAlias - The alias the camera should use 

		*/

		static void createCamera(Camera& c, position3D camPos, position3D camTarget, string camAlias);

		/*!
		Creates a new Camera. Camera object need to be passed to this function and the properties that has camera object will be used during the camera creation. 
		Additionally user defined camEffectType, camEffectPos, camAlias values will be set.

		\param c - A reference to a currently implemented camera object 
		\param camEffectType is the camera effect type it is an enumerated value defined .
			values can be type of CAMEFFECTTYPE_INTERNAL, CAMEFFECTTYPE_EXTERNAL, CAMEFFECTTYPE_FIXED, 
			CAMEFFECTTYPE_FIXEDWITHZOOM, CAMEFFECTTYPE_TERMINATE.
		
		\param camEffectPos is the camera effect type it is an enumerated value defined in the header file.
			values can be type of CAMEFFECTPOS_TOP, CAMEFFECTPOS_LEFT, CAMEFFECTPOS_RIGHT, CAMEFFECTPOS_FRONT, CAMEFFECTPOS_BACK.		
		
		\param camAlias - The alias the camera should use 

		*/

		static void createCamera(Camera& c, CAMEFFECTTYPE camEffectType, CAMEFFECTPOS camEffectPos, string camAlias);
/*!
		Creates a new Camera. Camera object need to be passed to this function and the properties that has camera object will be used during the camera creation. 
		Additionally user defined camEffectType, camEffectPos, camPos values will be set.

		\param c - A reference to a currently implemented camera object 
		\param camEffectType is the camera effect type it is an enumerated value defined .
			values can be type of CAMEFFECTTYPE_INTERNAL, CAMEFFECTTYPE_EXTERNAL, CAMEFFECTTYPE_FIXED, 
			CAMEFFECTTYPE_FIXEDWITHZOOM, CAMEFFECTTYPE_TERMINATE.
		
		\param camEffectPos is the camera effect type it is an enumerated value defined in the header file.
			values can be type of CAMEFFECTPOS_TOP, CAMEFFECTPOS_LEFT, CAMEFFECTPOS_RIGHT, CAMEFFECTPOS_FRONT, CAMEFFECTPOS_BACK
					
		
		\param camPos - The position the camera should create itself

		*/
		static void createCamera(Camera& c, CAMEFFECTTYPE camEffectType, CAMEFFECTPOS camEffectPos, position3D camPos);

		/*!
		Creates a new Camera. 
		Camera object need to be passed to this function and the properties that has camera object will be used during the camera creation. 
		Additionally user defined camEffectType, camEffectPos, camTarget, camRelPos, camAlias values will be set.

		\param c - A reference to a currently implemented camera object 
		\param camEffectType is the camera effect type it is an enumerated value defined .
			values can be type of CAMEFFECTTYPE_INTERNAL, CAMEFFECTTYPE_EXTERNAL, CAMEFFECTTYPE_FIXED, 
			CAMEFFECTTYPE_FIXEDWITHZOOM, CAMEFFECTTYPE_TERMINATE.
		
		\param camEffectPos is the camera effect type it is an enumerated value defined in the header file.
			values can be type of CAMEFFECTPOS_TOP, CAMEFFECTPOS_LEFT, CAMEFFECTPOS_RIGHT, CAMEFFECTPOS_FRONT, CAMEFFECTPOS_BACK.		
		
		\param camTarget - The position the camera should target itself
		\param camRelPos - The position the camera should create itself
		\param camAlias - The alias the camera should use 

		*/

		static void createCamera(Camera& c, CAMEFFECTTYPE camEffectType, CAMEFFECTPOS camEffectPos, position3D camTarget, position3D camRelPos, string camAlias);

		/*!
		Creates a new Camera. 
		Camera object need to be passed to this function and the properties that has camera object will be used during the camera creation. 
		Additionally user defined camEffectType, camEffectPos, camTarget, camPos values will be set.

		\param c - A reference to a currently implemented camera object 
		\param camEffectType is the camera effect type it is an enumerated value defined .
			values can be type of CAMEFFECTTYPE_INTERNAL, CAMEFFECTTYPE_EXTERNAL, CAMEFFECTTYPE_FIXED, 
			CAMEFFECTTYPE_FIXEDWITHZOOM, CAMEFFECTTYPE_TERMINATE.
		
		\param camEffectPos is the camera effect type it is an enumerated value defined in the header file.
			values can be type of CAMEFFECTPOS_TOP, CAMEFFECTPOS_LEFT, CAMEFFECTPOS_RIGHT, CAMEFFECTPOS_FRONT, CAMEFFECTPOS_BACK.		
		
		\param camTarget - The position the camera should target itself
		\param camPos - The position the camera should create itself

		*/

		static void createCamera(Camera& c, CAMEFFECTTYPE camEffectType, CAMEFFECTPOS camEffectPos, position3D camTarget, position3D camPos);
		
		//********************************************************
		// Destroy utilities
		//********************************************************

		/*!
		Destroy the camera from the scene.
		Destroy an object created with camCreate.camDestroy is conducted immediately, the command doesn't wait for camCommit.

		Error checking utility validates the following:

		- CameraAlias (outputs an error if the camera alias is invalid)

		\param c - A reference to a currently implemented camera object 
		*/
		static void destroyCamera(Camera& c);
	
		/*!
		Executes a command on the given camera / actor object.
		The "manual on" and "manual off" commands are recognized for all types.
		For the "camera" type, the following commands can be used: "inertia on" and "inertia off".
		For the "seagull" type it's one of: "landed" and "airborne" these control if they land or fly. 
		When you execute camCommand "landed" on a flying seagull, it will land and sit on the floor until you call camCommand "airborne". 
		The camCommand changes are conducted immediately, the command doesn't wait for camCommit.

		\param c - A reference to a currently implemented camera object 
		\param camCommand - The command 

		Deprecated function.
		Use void CameraUtilities::applyCamCommand(Camera& c, string camCommand) instead.
		*/
		VBS2FUSION_DPR(UCMR001) static void camCommand(Camera& c, string camCommand);

		/*!
		Returns true if the camera is using radar mode else return false.
		*/
		static bool isCamRadarMode(Camera& camera);

		/*!
		Clears a camera's targets (both object and position).

		Testing code
		CameraUtilities::createCamera(camera);
		camera.setPosition(player.getPosition());
		CameraUtilities::applyPosition(camera);
		CameraUtilities::applyCamEffect(camera,CAMEFFECTTYPE::CAMEFFECTTYPE_INTERNAL,CAMEFFECTPOS::CAMEFFECTPOS_BACK);
		Then
		CameraUtilities::resetTargets(camera);

		Deprecated function.
		Use CameraUtilities::applyTargetReset(Camera& camera) instead.
		*/
		VBS2FUSION_DPR(UCMR004) static void resetTargets(Camera& camera);
		
		/*!
		Set camera orthography parameters.
		onoff - turn camera on or off. 
		left, right, bottom and top are respective edges of the camera.
		*/
		static bool applyCamOrthography(Camera& camera,bool onoff,double left, double right,double bottom,double top);

		/*!
		Explicitly set the direction of the camera if it is attached to an object.
		The camera direction is relative to the object. 
		Changes are committed instantly.

		Testing code
		CameraUtilities::createCamera(camera);
		CameraUtilities::applyCamEffect(camera, CAMEFFECTTYPE::CAMEFFECTTYPE_INTERNAL, CAMEFFECTPOS::CAMEFFECTPOS_BACK);
		ControllableObjectUtilities::attachTo(camera, unit, position3D(5,-2,2));
		Then
		CameraUtilities::applyAttachedLookDir(camera,vector3D(1,0,1));
		*/


		static void applyAttachedLookDir(Camera& camera, vector3D cameraDirection);

#ifdef DEVELOPMENT
		/*!
		Set weather the camera should bank with its attached object.
		If false camera will always be oriented upwards. 
		Changes are committed instantly.
		*/
		static void applyBankIfAttached(Camera& camera, bool cambankStauts);
#endif

		/*!
		Returns current camera aperture. If the return value is -1 then that aperture 
		doesn't exist, and that it wasn't set by user.
		*/
		static double getAperture();

		/*!
		Sets custom camera aperture. set -1 as aperture value to do it automatically.	
		*/
		static void applyAperture(double apertureVal);

		/*!
		Set aperture for RTT(Render to texture) cameras.
		For automatic adjustment aperture should be -1.
		*/
		static void applyAperture(Camera& cameraRTT , double aperture);

		/*!
		Set camera sensor type.
		*/
		static void applySensorType(Camera& camera, CAMEFFECTMODE effectMode);

		/*!
		Returns true if the camera has finished preloading else false returns.
		*/
		static bool isPreLoaded(Camera& camera);

		/*!
		Returns camera interest for given unit. 
		*/
		static double getCameraInterest(ControllableObject& object);

		/*!
		Set camera interest for given entity.
		Note- Camera interest is by default 0. Camera interest is used to focus camera to control
		depth of field in cutscenes.Higher camera interest increases the chance of the unit being 
		focused.
		*/
		static void  applyCameraInterest(ControllableObject& object, double interestVal);

#ifdef DEVELOPMENT
		/*! 
		Set/Clear using of Night vision goggles.
		*/
		// camUseNVG script function is not working
		static void useNightVisionGoggles(bool usage);
#endif

		/*!
		Returns NetworkID of the object to which the camera is attached. 

		Deprecated function.
		Use CameraUtilities::getCameraOnObjectID() instead.
		*/
		VBS2FUSION_DPR(UCMR002) static NetworkID  cameraOnObjectID();


		/*!
		Returns camera FOV (Feild of view) as vector [minFov, maxFov]
		*/
		static vector<float> getFovRange(Camera& camera);

		/*!
		Returns camera FOV (Feild of view) as vector [minFov, maxFov]
		*/
		static vector<double> getFovRangeEx(Camera& camera);

		/*!
		Sets the camera dive angle. It does not automatically commit changes.
		*/
		static void applyDive(Camera& camera , double diveAngle);

		/*!
		Apply the camera heading angle. It does not automatically commit changes.
		*/
		static void applyCamDirection(Camera& camera, double direction);

#ifdef DEVELOPMENT
		/*!
		Prepare the camera field of view range for auto zooming.
		*/
		static void applyPrepareCamFovRange(Camera& camera, double minFov , double maxFov);

		/*!
		Smoothly conduct the changes that were asigned to a camera within the given time.
		If the time is set to zero, the changes are done immediately. 
		*/
		static void applyCamCommitPrepared(Camera& camera, double time);

		/*!
		Prepares the dive angle of the camera.
		*/
		static void applyPrepareDive(Camera& camera, double diveAngle);

		/*!
		Prepares the bank angle of the camera. 
		*/
		static void applyPrepareBank(Camera& camera, double bankAngle);

		/*!
		Prepares the heading angle of the camera.
		*/
		static void applyPrepareDirection(Camera& camera, double direction);

		/*!
		Sets the camera bank angle. It does not automatically commit changes.
		*/
		static void applyBank(Camera& camera, double bankAngle);

		/*!
		Sets the camera field of view range for auto zooming. It does not automatically commit changes.
		*/
		static void applyFovRange(Camera& camera, double minFov , double maxFov );


#endif

		/*!
		Force drawing of cinema borders. This is normally used in cutscenes to indicate player has no control. 

		Deprecated function.
		Use void CameraUtilities::applyCinemaBorder(bool show) instead.
		*/
		VBS2FUSION_DPR(UCMR007) static void showCinemaBorder(bool show);

		/*!
		Sets custom camera view frustum. All parameters are given as tangents of angles from center of camera direction
		use - if set to false, resets camera frustum. 
		tanLeft, tanRight, tanBottom, tanTop are sides of the frustum respectively. 
		*/
		static bool applyFrustum(bool use, float tanLeft, float tanRight, float tanBottom, float tanTop);

		/*!
		Sets custom camera view frustum. All parameters are given as tangents of angles from center of camera direction
		use - if set to false, resets camera frustum. 
		tanLeft, tanRight, tanBottom, tanTop are sides of the frustum respectively. 
		*/
		static bool applyFrustum(bool use, double tanLeft, double tanRight, double tanBottom, double tanTop);

		/*!
		Sets custom camera view frustum. All cameras, including the player view and editor cameras are affected by this.
		All parameters are given as tangents of angles from center of camera direction.

		use - If set to false, resets frustum.
		tanWidth - Changes only the FOV if height isn't set.
		tanHeight - Changes the aspect ratio when set with width. (Optional)
		tanWidth, tanHeight are the tan(FOV angle)
		*/
		static bool applyFrustum(bool use, float tanWidth, float tanHeight);

		/*!
		Sets custom camera view frustum. All cameras, including the player view and editor cameras are affected by this.
		All parameters are given as tangents of angles from center of camera direction.

		use - If set to false, resets frustum.
		tanWidth - Changes only the FOV if height isn't set.
		tanHeight - Changes the aspect ratio when set with width. (Optional)
		tanWidth, tanHeight are the tan(FOV angle)
		*/
		static bool applyFrustum(bool use, double tanWidth, double tanHeight);

		/*!
		Sets custom camera view frustum. All cameras, including the player view and editor cameras are affected by this.
		All parameters are given as tangents of angles from center of camera direction.

		use - If set to false, resets frustum.
		tanWidth - Changes only the FOV
		tanWidth is the tan(FOV angle)
		*/
		static bool applyFrustum(bool use, float tanWidth);

		/*!
		Sets custom camera view frustum. All cameras, including the player view and editor cameras are affected by this.
		All parameters are given as tangents of angles from center of camera direction.

		use - If set to false, resets frustum.
		tanWidth - Changes only the FOV
		tanWidth is the tan(FOV angle)
		*/
		static bool applyFrustum(bool use, double tanWidth);

		/*!
		Sets custom camera view frustum. All cameras, including the player view and editor cameras are affected by this.
		All parameters are given as tangents of angles from center of camera direction.

		use - If set to false, resets frustum.
		Eg: res = setCamFrustum [false] - Resets the camera frustum to default view.
		*/
		static bool applyFrustum(bool use);


#ifdef DEVELOPMENT 
		/*!
		Preload the scene for the prepared camera. Timeout given by time. if time = 0 that 
		means no timeout.
		*/
		static void applyPrepareCamPreload(Camera &cam , double time);

#endif

		/*!
		Transform position from camera coordinate space to world coordinate space.
		
		Deprecated function.
		Use CameraUtilities::getPositionCameraToWorld(position3D camPos) instead.
		*/
		VBS2FUSION_DPR(UCMR003) static position3D positionCameraToWorld(position3D camPos);

		/*!
		Set the position of the given camera or seagull.Needs the call of applyCameraCommit() to be conducted.
		*/
		static void applyCameraPosition(Camera& camera, position3D position);

		/*!
		Smoothly conduct the changes that were assigned to a camera within the given time. If the time is set to zero, the changes are done immediately. 
		*/
		static void applyCameraCommit(Camera& camera, double time);

		/*!
		Set the target object where the given camera should point at. Needs the call of applyCameraCommit() to be conducted. 
		*/
		static void applyCameraTarget(Camera& camera, ControllableObject& target);

		/*!
		Set the target position where the given camera should point at. Needs the call of applyCameraCommit() to be conducted. 
		*/
		static void applyCameraTarget(Camera& camera, position3D& target);

		/*!
		Set the position of the given camera relative to its target set with applyCameraTarget().
		Needs the call of applyCameraCommit() to be conducted. 
		*/
		static void applyCameraRelativePosition(Camera& camera, position3D position);

		/*!
		focusRange is in format [distance,blur]. Sets the camera focus blur. It does not automatically commit changes.
		*/
		static void applyCameraFocus(Camera& camera, double distance, double blur);

		/*!
		Set the zoom level (field of view) of the given camera.

		The default zoom level is 0.7, 0.01 is the nearest and 10 the furthest zoom value (landscape distorts at FOVs > 2).
		The angle of the field of view is atan(FOV)*2 degrees. If the aspectRatio is not 3:4, then the FOV value in the equation has to be multiplied by the x-ratio (e.g. atan(.95*1.2)*2.
		Needs the call of applyCameraCommit() to be conducted. 
		*/
		static void applyCameraFov(Camera& camera, double level);

		/*!
		Sets custom camera view offset. All cameras, including the player view and editor cameras are affected by this.

		All arguments, except for the "custom" switch are optional, and default to 0.

		If the camera is set behind the player, the player head will be invisible (as it normally is, when the player is in first person view).
		
		custom - If set to false, resets frustum.
		*/

		static void applyCameraFrustumOffsets(bool custom, double azimuth, double pitch, double roll);

		/*!
		Sets custom camera view offset. All cameras, including the player view and editor cameras are affected by this.

		All arguments, except for the "custom" switch are optional, and default to 0.

		If the camera is set behind the player, the player head will be invisible (as it normally is, when the player is in first person view).
		Frustrum offsets are not reset between missions, or even editor restarts, and will have to be reset manually.

		custom - If set to false, resets frustum.
		offset - Offset in [x,y,z] 
		*/
		static void applyCameraFrustumOffsets(bool custom, double azimuth, double pitch, double roll, position3D offset);

		/*!
		Defines whether the editor camera is stopped by the ground when moving downwards.
		If true, editor camera is stopped by the ground, if false it can go underground.

		Deprecated function.
		Use CameraUtilities::applyCameraCollision(bool collide) instead.
		*/
		VBS2FUSION_DPR(UCMR005) static void setCameraCollision(bool collide);

		/*!
		Checks whether the editor camera is stopped by the ground when moving downwards.
		*/
		static bool getCameraCollision();
		
		/*!
		Returns camera's target.
		*/
		static ControllableObject getCamTarget(Camera& camera);

		/*!
		Return type of camera.
		*/
		static string getCameraView();

		/*!
		Creates a specific light setting to simulate a radar view (i.e. all natural and artificial lights are off,
		and only one green light source is created at the specified position). Mainly useful for applications 
		over water. (Requires shader model 3 to work.)

		Deprecated function.
		Use CameraUtilities::applyCamRadar(bool mode, ControllableObject& object,position3D offset) instead.
		*/
		VBS2FUSION_DPR(UCMR006) static void setCamRadar(bool mode, ControllableObject& object,position3D offset);

		/*!
		Returns direction of the ViewFrustum.
		*/
		static vector3D getFrustumDirectionVector();

		/*!
		Returns Up vector of the ViewFrustum.
		*/
		static vector3D getFrustumUpVector();

		/*!
		Returns parameters of the current frustum.

		Return value: 
			bool - Custom frustum is used.
			Returned vector contains 
				left	- Left side angle of frustum.
				right	- Right side angle of frustum.
				bottom	- Bottom side angle of frustum.
				top		- Top side angle of frustum.
				nearClip - Near clipping plane.
				farClip	 - Far clipping plane.

		If the VBS2 is not returned proper value set vector will be empty.
		*/
		static bool getFrustum(vector<double>& frustumVector);
		
		/*!
		Returns the vector direction the currently active camera is facing. 
		@param Nothing.
		@return vector3D.
		*/
		static vector3D getCameraDirectionVector();

		/*!
		Returns the azimuth and altitude angles (in degrees) of the facing direction of the currently active camera.
		@param Nothing.
		@return vector<double> - Returned vector contains 2 values. azimuth, altitude in degrees.
		*/
		static vector<double> getCameraDirection();

		/*!
		This function is used to set a basic, fixed, unobstructed camera of a given target (in a cutscene).
		It is useful when you do not know the exact position or nature of the targets in advance, 
		but want to be sure they can clearly be seen during the cutscene.
		The function tries a number of camera positions, attempting to find a position where the camera's view of the target will not be obstructed. 

		Note that if the function fails to find an suitable camera position, a false will be returned.
		The camera can still be committed, but the target will not be able to be seen. 

		It does not automatically commit the changes.
		CameraUtilities::applyCameraCommit(Camera& camera, double time) should be called to commit the changes.

		@param  cam - a reference to a currently implemented camera object.
		@param  distance - modifies the default distances chosen by the function.
					distance = 1 means the default distances are chosen.
					distance = 2 means the distances are roughly twice as far for those given targets.
		@param	target - the object which should be focused on.
		
		@returns bool - true if the shot will have a clear view of the target and false if not.
		*/ 
		static bool applyFixedShot(Camera& cam, double distance, ControllableObject& target);

		/*!
		This function is used to set a basic, fixed, unobstructed camera of a given target (in a cutscene).
		It is useful when you do not know the exact position or nature of the targets in advance, 
		but want to be sure they can clearly be seen during the cutscene.
		The function tries a number of camera positions, attempting to find a position where the camera's view of the target will not be obstructed. 

		The function will attempt to work both targets (primary and secondary) into the shot, although the primary target is given priority. 

		Note that if the function fails to find an suitable camera position, a false will be returned.
		The camera can still be committed, but the primary target will not be able to be seen. 

		It does not automatically commit the changes.
		CameraUtilities::applyCameraCommit(Camera& camera, double time) should be called to commit the changes.

		@param  cam - a reference to a currently implemented camera object.
		@param  distance - modifies the default distances chosen by the function.
					distance = 1 means the default distances are chosen.
					distance = 2 means the distances are roughly twice as far for those given targets.
		@param	priTarget - primary target object.
		@param	secTarget - secondary target object.
		
		@returns bool - true if the shot will have a clear view of the targets and false if not.
		*/
		static bool applyFixedShot(Camera& cam, double distance, ControllableObject& priTarget, ControllableObject& secTarget);
		/*!
		@description 
		
		Executes a command on the given camera / actor object. 
		The "manual on" and "manual off" commands are recognized for all types.
		For the "camera" type, the following commands can be used: "inertia on" and "inertia off".
		For the "seagull" type it's one of: "landed" and "airborne" these control if they land or fly. 
		When you execute camCommand "landed" on a flying seagull, it will land and sit on the floor until you call camCommand "airborne". 
		The camCommand changes are conducted immediately, the command doesn't wait for camCommit.

		@locality

		Globally applied, locally effected

		@version  [VBS2Fusion v3.11]

		@param c - A reference to a currently implemented camera object

		@param camCommand - The command

		@return Nothing.

		@example

		@code

		//apply the command for the camera.
		CameraUtilities::applyCamCommand(cam,string("manual on"));

		@endcode

		@overloaded

		Nothing.

		@related

		@remarks This is a replication of void camCommand(Camera& c, string camCommand) function.
		*/
		static void applyCamCommand(Camera& c, string camCommand);

		/*!
		@description 
		
		Returns NetworkID of the object to which the camera is attached.

		@locality

		Globally applied

		@version  [VBS2Fusion v3.11]

		@param None.

		@return NetworkID.

		@example

		@code

		//Assign the Network ID
		NetworkID netID;

		//Get the network ID of the object to which the camera is attached.
		netID = CameraUtilities::getCameraOnObjectID();

		//Display the network ID
		displayString =  "Network ID - "+netID.getNetworkIDString();

		@endcode

		@overloaded  None.

		@related

		@remarks This is a replication of void NetworkID cameraOnObjectID() function.
		*/
		static NetworkID getCameraOnObjectID();
		
		/*!
		@description Transform position from camera coordinate space to world coordinate space.

		@locality

		Globally applied

		@version  [VBS2Fusion v3.11]

		@param camPos - Position of the camera

		@return Position in world coordinate space.

		@example

		@code

		//Transform the camera position from camera coordinate space to world coordinate space.
		position3D pos = CameraUtilities::getPositionCameraToWorld(cam.getPosition());

		//Display the position
		displayString = "Position in the World Coordinate - "+pos.getVBSPosition();

		@endcode

		@overloaded  None.

		@related

		@remarks This is a replication of position3D positionCameraToWorld(position3D camPos) function.
		*/
		static position3D getPositionCameraToWorld(position3D camPos);

		/*!
		@description Clears a camera's targets (both object and position).

		@locality

		Globally applied.

		@version  [VBS2Fusion v3.12]

		@param camera - A reference to a currently implemented camera object

		@return Nothing.

		@example

		@code

		//Reset camera target.
		CameraUtilities::applyTargetReset(cam);

		@endcode

		@overloaded

		Nothing

		@related

		@remarks This is a replication of void resetTargets(Camera& camera) function.
		*/
		static void applyTargetReset(Camera& camera);

		/*!
		@description Defines whether the editor camera is stopped by the ground when moving downwards.
		If true, editor camera is stopped by the ground, if false it can go underground.

		@locality

		Globally applied

		@version  [VBS2Fusion v3.11]

		@param collide -  If true, editor camera is stopped by the ground, if false it can go underground.

		@return Nothing.

		@example

		@code

		//Display the return value as string
		displayString = "Camera collision - " + conversions::BoolToString(CameraUtilities::getCameraCollision());

		@endcode

		@overloaded  None.

		@related

		@remarks This is a replication of void setCameraCollision(bool collide) function.
		*/
		static void applyCameraCollision(bool collide);

		/*!
		@description Creates a specific light setting to simulate a radar view (i.e. all natural and artificial lights are off,
		and only one green light source is created at the specified position). Mainly useful for applications 
		over water. (Requires shader model 3 to work.)

		@locality

		@version  [VBS2Fusion v3.11]

		@param mode - Turns radar mode on and off.
		@param object - Object to attach radar light source.
		@param offset - Relative offset [x,z,y] of light, in relation to object.

		@return Nothing.

		@example

		@code

		@overloaded  None.

		@related

		@remarks This is a replication of void setCamRadar(bool mode, ControllableObject& object,position3D offset) function.
		*/
		static void applyCamRadar(bool mode, ControllableObject& object,position3D offset);

		/*!
		@description
		Force drawing of cinema borders. This is normally used in cutscenes to indicate player has no control.

		@locality

		Locally effected

		@version [VBS2Fusion v3.11]

		@param show - True or false. When it is true, show the cinema border. If it is false, stop drawing cinema border.

		@return Nothing.

		@example

		@code

		//Apply the cinema border.
		CameraUtilities::applyCinemaBorder(true);

		@endcode

		@overloaded  None.

		@related

		@remarks This is a replication of void showCinemaBorder(bool show) function.
		*/
		static void applyCinemaBorder(bool show);

		/*!
		@description
		
		Creates a shaky 1st person view (is not applied to 3rd person or camera views). 
		Camera will start to shake according to given frequency. This is a similar function to addCamShake script command.

		@locality

		@version [VBS2Fusion v3.15.1]

		@param power - Strength of shake, practical range is 0 to 20
		@param duration -Time duration need to shake the camera
		@param frequency - Frequency of shake, practical range is 0 to 100

		@return Nothing.

		@example

		@code

		@overloaded  None.

		@related

		@remarks 
		*/
		static void applyCamShake(double power, double duration, double frequency);

		/*!
		@description

		Stops any ongoing camera shake effects. Does not prevent new effects from being created. 
		To disallow new effects altogether, enableCamShake should be used.

		@locality

		@version [VBS2Fusion v3.15.1]

		@param 
		
		@return Nothing.

		@example

		@code

		@overloaded  None.

		@related

		@remarks 
		*/
		static void applyCamShakeReset();

		/*!
		@description

		Set camera shake parameters. 
		Will not have any effect unit shake is started via addCamShake.

		@locality

		@version [VBS2Fusion v3.15.1]

		@param posCoef - Strength of positional movement (how far the whole position can change in X,Y&Z), should not be bigger than .1, or the camera might move into, or behind, the player
		@param vertCoef - Strength of vertical camera pan (up/down), practical range: 0-10 
		@param horzCoef - Strength of horizontal camera pan (left/right), practical range: 0-10
		@param bankCoef - Strength of camera tilt/bank, practical range: 0-20
		@param interpolation - If true, then transitions in the different shaking directions will be smoother

		@return Nothing.

		@example

		@code

		@overloaded  None.

		@related

		@remarks 
		*/
		static void applyCamShakeParams(double posCoef, double vertCoef, double horzCoef, double bankCoef, bool interpolation);
		
		/*!
		@description

		Allows camera shake effects via addCamShake.
		If set to false, then a currently active shake effect will stop immediately.
		By default, shake effects are enabled, but once they have been disabled by this command, they will have to be enabled first, in order to be visible.

		@locality

		@version [VBS2Fusion v3.15.1]

		@param enable - Boolean value to deside camShake enable or disable. If true, then shake effects are visible. 
		
		@return Nothing.

		@example

		@code

		@overloaded  None.

		@related

		@remarks 
		*/
		static void applyCamShakeEnable(bool enable);

#if _BISIM_DEV_EXTERNAL_POSE
		/*!
		Switch camera to given unit.
		@param unit - Unit.
    @param mode - Camera mode which will be set.
		*/
    static void switchCamera(Unit& unit, string mode);

		/*!
		Returns wanted type of camera
		@return static - wanted camera mode.
		*/
    static string getCameraViewWanted();
#endif
	};
};

#endif