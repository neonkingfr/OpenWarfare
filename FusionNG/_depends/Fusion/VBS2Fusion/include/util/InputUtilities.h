/*
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 */

/*************************************************************************

Name:

	InputUtilities.h

Purpose:

	This file contains the declaration of the InputUtilities class.

Version Information:

	Version		Date		Author and Comments
	===========================================
	1.0			20-09-2011	SSD: Original Implementation


/************************************************************************/

#ifndef VBS2FUSION_INPUT_UTILITIES_H
#define VBS2FUSION_INPUT_UTILITIES_H

/**********************************************/
/* INCLUDES
/**********************************************/

// Standard Includes
#include <string>


// Simcentric Includes
#include "VBS2Fusion.h"
#include "VBS2FusionDefinitions.h"


/**********************************************/
/* END INCLUDES
/**********************************************/
namespace VBS2Fusion
{
	class VBS2FUSION_API InputUtilities
	{
	public:

		/*!
		Assigns a command to a key action (key board or joystick input).
		If suppress is passed as true, the action assigned to key action does not occur, 
		only the script command executes.
		A key index is returned such that it can be passed into the unBindKey function.
		For an incorrect binding (-1) is returned.

		Deprecated. Use int applyKeyBinding(string keyAction, string command, bool suppress)
		*/
		VBS2FUSION_DPR(UINP001) static int bindKey(string keyAction, string command, bool suppress);
		
		/*!
		Assigns a command to a Direct Input Keyboard (DIK) key code.
		If suppress is passed as true, the action associated with the DIC key does not occur, 
		only the script command executes.
		A key index is returned such that it can be passed into the unBindKey function.
		For an incorrect binding (-1) is returned.

		Deprecated. Use int applyKeyBinding(int keyCode, string command, bool suppress)
		*/
		VBS2FUSION_DPR(UINP002) static int bindKey(int keyCode, string command, bool suppress);

		/*!
		Assigns a command to a key action and returns the key index.

		keyAction -  Key board or joystick input.
		command - Script command to execute on the key action.
		suppress - If suppress is passed as true, the action assigned to key action does not occur, only the script command executes.
		enableFloat - Flag to identify whether the command should return on float inputs (like from joystick ranging from 0 to 1) 
			or boolean inputs (like from key as 0 or 1). When set to false command returns only for discrete inputs (0 or 1).
		rule - Command is executed when the button is pressed, when the value associated with the button has changed or 
			on each simulation step	as specified by the KEY_BIND_RULE enum.
		vehicleType(optional) - Command is executed only when the player is in the passed vehicle type.
		
		For an incorrect binding (-1) is returned.

		Deprecated. Use int applyKeyBinding(string keyAction, string command, bool suppress, bool enableFloat, KEY_BIND_RULE rule, string vehicleType = "")
		*/
		VBS2FUSION_DPR(UINP003) static int bindKey(string keyAction, string command, bool suppress, bool enableFloat, KEY_BIND_RULE rule, string vehicleType = "");

		/*!
		Assigns a command to a Direct Input Keyboard (DIK) key code and returns the key index.

		keyCode - DIK key code.
		command - Script command to execute on button event.
		suppress - If suppress is passed as true, the action assigned to key action does not occur, only the script command executes.
		enableFloat - Flag to identify whether the command should return on float inputs (like from joystick ranging from 0 to 1) 
			or boolean inputs (like from key as 0 or 1). When set to false command returns only for discrete inputs (0 or 1).
		rule - Command is executed when the button is pressed, when the value associated with the button has changed or 
			on each simulation step	as specified by the KEY_BIND_RULE enum.
		vehicleType(optional) - Command is executed only when the player is in the passed vehicle type.

		For an incorrect binding (-1) is returned.

		Deprecated. Use int applyKeyBinding(int keyCode, string command, bool suppress, bool enableFloat, KEY_BIND_RULE rule, string vehicleType = "")
		*/
		VBS2FUSION_DPR(UINP004) static int bindKey(int keyCode, string command, bool suppress, bool enableFloat, KEY_BIND_RULE rule, string vehicleType = "");

		/*!
		Revoke the specified key bound.
		Deprecated. Use void applyKeyUnbinding(int keyIndex)
		*/
		VBS2FUSION_DPR(UINP005) static void unBindKey(int keyIndex);

		/*!
		Records all user input, to be later used with replayUserInput.
		Input will be recorded until either escape button is pressed or stopUserInputStreaming is executed.
		Type of "fileName" has to be [.rec]. 
		Ex: "recording.rec", "d:\\recordings\\recording.rec"
		Note that the specified folder path should exist.

		Deprecated. Use void applyUserInputRecord(string fileName)
		*/
		VBS2FUSION_DPR(UINP006) static void recordUserInput(string fileName);

		/*!
		Replays user input, which was recorded by recordUserInput. 
		Replay is looped until escape button is pressed, or stopUserInputStreaming is executed.
		Type of "fileName" has to be [.rec]. 
		Ex: "recording.rec", "d:\\recordings\\recording.rec"
		
		Deprecated. Use void applyUserInputReplay(string fileName)
		*/
		VBS2FUSION_DPR(UINP010) static void replayUserInput(string fileName);

		/*!
		Stops either the recording or replaying of user input. 
		Alternatively, pressing escape button will also stop either of those commands.

		Deprecated. Use void applyUserInputStreamStop()
		*/
		VBS2FUSION_DPR(UINP007) static void stopUserInputStreaming();

		/*!
		Disable and enable the keyboard and mouse input.
		Be careful with the usage of this command, always remember to enable the user input again 
		once the user input is disabled.

		Deprecated. Use void applyUserInputDisable(bool state)
		*/
		VBS2FUSION_DPR(UINP008) static void disableUserInput(bool state);

		/*!
		Returns true if the key with the specified Direct Input Keyboard (DIK) key code is 
		currently pressed. 
		*/
		static bool isKeyPressed(int keyCode);

#ifdef DEVELOPMENT		
		
		/*!
		Returns true if the specified joystick button is currently pressed 
		(max = 32 buttons). 
		*/
		static bool isJoyBtnPressed(int button);
#endif

		/*!
		Define action performed when user clicks in map by executing command string.

		the string receives 3 (localised in scope) variables:

		_pos: Array - Clicked position
		_units: Array - Units which were selected (via function keys) before opening the map (may be non-functional in Arma)
		_shift, _alt Boolean - Whether <Shift> or <Alt> were pressed when clicking on the map 

		When click is processed, code should ultimately return true back to the engine. If false is returned, default processing by the game engine is done. Return value of any other type (including Nothing) is an error. In such case default processing by the game engine is done, and error message may be displayed.

		The code is executed on every click, until the Command is

		removed via onMapSingleClick "", or
		replaced by onMapSingleClick "SomeOtherCommand(s)"
		*/
		static void onMapSingleClick(string command);

		/*!
		Defines an action performed when the user double clicks on the map.

		The following variables are available in the command section:

		_pos (Position3D) = clicked position
		_units (Array) = units which were selected (via function keys) before opening the map (may be non-functional in V1.23 and lower)
		_alt (Boolean) = whether the <Alt> key was pressed
		_map (Control) = current map control 
		*/
		static void onMapDoubleClick(string command);

		/*!
		Defines an action performed when the user double clicks on the map.
		Map - It is the map (you can use "map" for current map)

		The following variables are available in the command section:

		_pos (Position3D) = clicked position
		_units (Array) = units which were selected (via function keys) before opening the map (may be non-functional in V1.23 and lower)
		_alt (Boolean) = whether the <Alt> key was pressed
		_map (Control) = current map control 
		*/
		static void onDoubleClick(string Map, string command);

		/*!
		Returns the name of a button (on the keyboard, mouse or joystick) with the given code.
		*/
		static string getKeyName(int keyCode);

		/*!
		Returns a list of dikCodes of buttons assigned to the given user action.
		*/
		static vector<int> getActionKeys (string userAction);

		/*!
		Returns the status of the specified input action.
		For key assignments, the returned value is normally 1 (but can, in cases of fast keystrokes, also contain other values, so in general it is safer to test for a value that is != 0).
		For joystick assignments, the returned axis value will be between 0 and 1 (inclusive).

		Deprecated. Use double getInputActionState(string action)
		*/
		VBS2FUSION_DPR(UINP009) static float inputAction(string action);

		/*!
		Returns the status of the specified input action.
		For key assignments, the returned value is normally 1 (but can, in cases of fast keystrokes, also contain other values, so in general it is safer to test for a value that is != 0).
		For joystick assignments, the returned axis value will be between 0 and 1 (inclusive).
		
		Deprecated. Use double getInputActionState(string action)
		*/
		VBS2FUSION_DPR(UINP009) static double inputActionEx(string action);

		/*!
		Returns the key assignments for the specified action.
		*/
		static string getActionKeysNames (string userAction);

		/*!
		Returns the key assignments for the specified action.
		maxKeys - A maximum of maxKeys keys should be listed.
		*/
		static string getActionKeysNames (string userAction, int maxKeys);

		/*!
		Returns the relevant symbolic name for keyCode
		
		@param keyCode - keyCode of the pressed key (keyCode can be received by using the 'OnKeyBoradEvent' callback)

		@return string - Symbolic name of the given keyCode 
		*/
		static string getKeySymbol(int keyCode);
		
		/*!
		@description 

		Returns a vector of currently connected devices.Each element of this vector contain s
		the information of the DEVICE_JOYCAPS_DATA struct.

		@locality

		Locally Effected.

		@version  [VBS2Fusion v3.12]

		@param 
		
		@return vector<VBS2Fusion::DEVICE_JOYCAPS_DATA>

		@example

		@code

		vector<VBS2Fusion::DEVICE_JOYCAPS_DATA> vecJoyCapsData;
		
		vecJoyCapsData = InputUtilities::getConnectedJoystickDevices();

		@endcode

		@overloaded 
		
		None

		@related

		None

		@remarks The Joystick device needs to be connected to the machine before VBS2 starts. If not it will not recognize the Joystick.
		*/
		static std::vector<VBS2Fusion::DEVICE_JOYCAPS_DATA> getConnectedJoystickDevices();

		/*!
		@description 

		Returns the vector of currently connected device information. This contain the information 
		data of STATE_JOYINFO_DATA struct.

		@locality

		Locally Effected.

		@version  [VBS2Fusion v3.12]

		@param 
		
		@return vector<VBS2Fusion::STATE_JOYINFO_DATA>

		@example

		@code

		vector<VBS2Fusion::STATE_JOYINFO_DATA> vecJoyInfoData;
		vecJoyInfoData = InputUtilities::getConnectedJoystickInfo();

		@endcode

		@overloaded 
		
		None

		@related

		None

		@remarks The Joystick device needs to be connected to the machine before VBS2 starts. If not it will not recognize the Joystick.
		*/
		static std::vector<VBS2Fusion::STATE_JOYINFO_DATA> getConnectedJoystickInfo();

		/*!
		@description 

		When we get a Joystick in to our program we should keep track of that device. By using this 
		function  we can store captured joystick IDs. If there is error regarding the pooling, this 
		function return false.

		@locality

		Locally Applied, Locally Effected.

		@version  [VBS2Fusion v3.12]

		@param deviceID - Id of the joystick device
		
		@return bool - If joystick state set properly return true.  

		@example

		@code

		bool joyStickState;

		joyStickState = InputUtilities::applyCaptureJoystickState(0);

		@endcode

		@overloaded  
		
		None

		@related

		None

		@remarks The Joystick device needs to be connected to the machine before VBS2 starts. If not it will not recognize the Joystick.
		*/
		static bool applyCaptureJoystickState(int deviceID);
		

#if _BISIM_DEV_EXTERNAL_POSE

		/*!
		Enable or disable support for external joystick
		@param state - desired state
		*/
    static void enableExternalJoystick(bool state);

    /*!
		Enable or disable support for trackIR
		@param state - desired state
		*/
    static void enableTrackIR(bool state);
#endif

		/*!
		@description 

		Assigns a command to a key action (key board or joystick input).
		If suppress is passed as true, the action assigned to key action does not occur, 
		only the script command executes.
		A key index is returned such that it can be passed into the unBindKey function.
		For an incorrect binding (-1) is returned.

		@locality

		Locally Applied, Globally Effected

		@version  [VBS2Fusion v3.15]

		@param keyAction � The name of a key action.

		@param command - Script code to run.

		@param suppress - Whether to execute the script command action which assigned to key or not.

		@return int - key index

		@example

		@code
		
		//When the player moves forward all his weapons are removed
		int key = InputUtilities::applyKeyBinding("MoveForward","removeAllWeapons player",false);

		@endcode

		@overloaded 
		
		int applyKeyBinding(int keyCode, string command, bool suppress);
		int applyKeyBinding(string keyAction, string command, bool suppress, bool enableFloat, KEY_BIND_RULE rule, string vehicleType = "");
		int applyKeyBinding(int keyCode, string command, bool suppress, bool enableFloat, KEY_BIND_RULE rule, string vehicleType = "");

		@related

		None

		@remarks This is a replication of int bindKey(string keyAction, string command, bool suppress)
		*/
		static int applyKeyBinding(string keyAction, string command, bool suppress);

		/*!
		@description 

		Assigns a command to a Direct Input Keyboard (DIK) key code.
		If suppress is passed as true, the action associated with the DIC key does not occur, 
		only the script command executes.
		A key index is returned such that it can be passed into the unBindKey function.
		For an incorrect binding (-1) is returned.

		@locality

		Locally Applied, Globally Effected

		@version  [VBS2Fusion v3.15]

		@param keyCode � The Direct Input Keyboard key code of a key action.

		@param command - Script code to run.

		@param suppress - Whether to execute the script command action which assigned to key or not.

		@return int - key index

		@example

		@code

		//When the players move forward key I.e 'W' (With key code 17) is pressed all his weapons are removed
		int key = InputUtilities::applyKeyBinding(17,"removeAllWeapons player",false);

		@endcode

		@overloaded 
		
		int applyKeyBinding(string keyAction, string command, bool suppress);
		int applyKeyBinding(string keyAction, string command, bool suppress, bool enableFloat, KEY_BIND_RULE rule, string vehicleType = "");
		int applyKeyBinding(int keyCode, string command, bool suppress, bool enableFloat, KEY_BIND_RULE rule, string vehicleType = "");

		@related

		None

		@remarks This is a replication of int bindKey(int keyCode, string command, bool suppress)

		@remarks Please refer the appendix in the VBS2Fusion Product Manual to get the key codes.
		*/
		static int applyKeyBinding(int keyCode, string command, bool suppress);

		/*!
		@description 

		Assigns a command to a key action and returns the key index.
		For an incorrect binding (-1) is returned.

		@locality

		Locally Applied, Globally Effected

		@version  [VBS2Fusion v3.15]

		@param keyAction � The name of a key action.
		
		@param command - Script code to run.
		
		@param suppress - Whether to execute the script command action which assigned to key or not.
		
		@param enableFloat - Whether to get the float inputs from a joystick (true) or booleans from a key (false).
		
		@param rule - When the newly defined command should be executed:
						KBR_BUTTON_PRESS = 0: (with float true): Constantly, while the button is pressed.
						KBR_BUTTON_PRESS = 0: (with float false): Once, when the button is pressed
						KBR_VALUE_CHANGE = 1: Once per value change (either joystick move, or once per button press & release)
						KBR_SIMULATION_STEP = 2: Constantly (once per simulation step). If used with keyboard input, _value will return 1 while button is pressed, otherwise 0 is returned.
						For joystick input, the current float value is returned.
		
		@param vehicleType - Event is only fired if player is inside a vehicle of the specified type.

		@return int - key index

		@example

		@code

		int key = InputUtilities::applyKeyBinding("MoveForward","player sidechat 'Moving'",false,false,KEY_BIND_RULE::KBR_BUTTON_PRESS,"vbs2_cz_army_bmp2_d_x");

		@endcode

		@overloaded 
		
		int applyKeyBinding(string keyAction, string command, bool suppress);
		int applyKeyBinding(int keyCode, string command, bool suppress);
		int applyKeyBinding(int keyCode, string command, bool suppress, bool enableFloat, KEY_BIND_RULE rule, string vehicleType = "");

		@related

		None

		@remarks This is a replication of int bindKey(string keyAction, string command, bool suppress, bool enableFloat, KEY_BIND_RULE rule, string vehicleType = "")
		*/
		static int applyKeyBinding(string keyAction, string command, bool suppress, bool enableFloat, KEY_BIND_RULE rule, string vehicleType = "");
		
		/*!
		@description 
		
		Assigns a command to a Direct Input Keyboard (DIK) key code and returns the key index.
		For an incorrect binding (-1) is returned.
		
		@locality

		Locally Applied, Globally Effected
		
		@version  [VBS2Fusion v3.15]
		
		@param keyCode � The Direct Input Keyboard key code of a key action.
		
		@param command - Script code to run.
		
		@param suppress - Whether to execute the script command action which assigned to key or not.
		
		@param enableFloat - Whether to get the float inputs from a joystick (true) or booleans from a key (false).
		
		@param rule - When the newly defined command should be executed:
						KBR_BUTTON_PRESS = 0: (with float true): Constantly, while the button is pressed.
						KBR_BUTTON_PRESS = 0: (with float false): Once, when the button is pressed
						KBR_VALUE_CHANGE = 1: Once per value change (either joystick move, or once per button press & release)
						KBR_SIMULATION_STEP = 2: Constantly (once per simulation step). If used with keyboard input, _value will return 1 while button is pressed, otherwise 0 is returned.
						For joystick input, the current float value is returned.
		
		@param vehicleType - Event is only fired if player is inside a vehicle of the specified type (optional).
		
		@return int - key index
		
		@example
		
		@code
		
		int key = InputUtilities::applyKeyBinding(17,"player sidechat 'Forward'",false,true,KEY_BIND_RULE::KBR_BUTTON_PRESS,"vbs2_cz_army_bmp2_d_x");

		@endcode
		
		@overloaded 
		
		int applyKeyBinding(string keyAction, string command, bool suppress);
		int applyKeyBinding(int keyCode, string command, bool suppress);
		int applyKeyBinding(string keyAction, string command, bool suppress, bool enableFloat, KEY_BIND_RULE rule, string vehicleType = "");
		
		@related

		None
		
		@remarks This is a replication of int bindKey(int keyCode, string command, bool suppress, bool enableFloat, KEY_BIND_RULE rule, string vehicleType = "")
		@remarks Please refer the appendix in the VBS2Fusion Product Manual to get the key codes.
		*/
		static int applyKeyBinding(int keyCode, string command, bool suppress, bool enableFloat, KEY_BIND_RULE rule, string vehicleType = "");

		/*!
		@description 
		
		Clears the key bindings.
		
		@locality

		Locally Applied, Globally Effected
		
		@version  [VBS2Fusion v3.15]
		
		@param keyIndex - Index generated by applyKeyBinding function.
		
		@return None
		
		@example
		
		@code
		
		//the value for 'key' can be generated via the applyKeyBinding functions.
		InputUtilities::applyKeyUnbinding(key);

		@endcode
		
		@overloaded None
		
		@related 

		applyKeyBinding(string keyAction, string command, bool suppress);
		applyKeyBinding(int keyCode, string command, bool suppress);
		applyKeyBinding(string keyAction, string command, bool suppress, bool enableFloat, KEY_BIND_RULE rule, string vehicleType = "");
		applyKeyBinding(int keyCode, string command, bool suppress, bool enableFloat, KEY_BIND_RULE rule, string vehicleType = "");
		
		@remarks This is a replication of void unBindKey(int keyIndex)
		*/
		static void applyKeyUnbinding(int keyIndex);

		/*!
		@description 
		
		Records all user input, to be later used with InputUtilities::applyUserInputReplay(string fileName).
		Input will be recorded until either escape button is pressed or InputUtilities::applyUserInputStreamStop() is executed.
		Type of "fileName" has to be [.rec]. 
		Ex: "recording.rec", "d:\\recordings\\recording.rec"
		Note that the specified folder path should exist.
		
		@locality

		Locally Applied, Locally Effected
		
		@version  [VBS2Fusion v3.15]
		
		@param fileName - File name of file to be recorded. 
		
		@return None
		
		@example
		
		@code

		InputUtilities::applyUserInputRecord("d:\\recordings\\recording.rec");
		
		@endcode
		
		@overloaded
		
		None
		
		@related 

		InputUtilities::applyUserInputReplay(string fileName);
		
		@remarks This is a replication of void recordUserInput(string fileName)
		*/
		static void applyUserInputRecord(string fileName);

		/*!
		@description 

		Stops either the recording or replaying of user input. 
		Alternatively, pressing escape button will also stop either of those commands.
		
		@locality

		Locally Applied, Locally Effected.
		
		@version  [VBS2Fusion v3.15]
		
		@param None
		
		@return None
		
		@example
		
		@code
		
		InputUtilities::applyUserInputStreamStop();

		@endcode
		
		@overloaded 
		
		None
		
		@related 

		InputUtilities::applyUserInputRecord(string fileName);
		
		@remarks This is a replication of void stopUserInputStreaming()
		*/
		static void applyUserInputStreamStop();

		/*!
		@description 

		Disable and enable the keyboard and mouse input.
		Be careful with the usage of this command, always remember to enable the user input again 
		once the user input is disabled.
		
		@locality

		Locally Applied, Locally Effected.

		@version  [VBS2Fusion v3.15]
		
		@param state - User input mode enable/disable state
		
		@return None
		
		@example
		
		@code
		
		InputUtilities::applyUserInputDisable(true);

		@endcode
		
		@overloaded
		
		None
		
		@related 

		None
		
		@remarks This is a replication of void disableUserInput(bool state)
		*/
		static void applyUserInputDisable(bool state);

		/*!
		@description 
		
		Returns the status of the specified input action.
		For key assignments, the returned value is normally 1 (but can, in cases of fast keystrokes, also contain other values, so in general it is safer to test for a value that is != 0).
		For joystick assignments, the returned axis value will be between 0 and 1 (inclusive).
		
		@locality

		Locally Applied, Locally Effected.
		
		@version  [VBS2Fusion v3.15]
		
		@param action - Action name
		
		@return None
		
		@example
		
		@code
		
		//run this in the OnSimulationStep 
		double moveForwardInputVal = InputUtilities::getInputActionState(string("MoveForward"));
		displayString = "Move Forward : " +conversions::DoubleToString(moveForwardInputVal);

		@endcode
		
		@overloaded 
		
		None
		
		@related 

		None
		
		@remarks This is a replication of float inputAction(string action) and double inputActionEx(string action).
		@remarks To make full use of this function it would need to be used in the OnSimulationStep callback as it would give real time results.
		*/
		static double getInputActionState(string& action);
		
		/*!
		@description 
		
		Replays user input, which was recorded by InputUtilities::applyUserInputRecord(string fileName). 
		Replay is looped until escape button is pressed, or InputUtilities::applyUserInputStreamStop() is executed.
		Type of "fileName" has to be [.rec]. 
		Ex: "recording.rec", "d:\\recordings\\recording.rec"
		
		@locality
		
		Locally Applied, Globally Effected.
		
		@version  [VBS2Fusion v3.15]
		
		@param fileName - Name of the recorded file which need to be replay.
		
		@return None
		
		@example
		
		@code
		
		InputUtilities::applyUserInputReplay("d:\\recordings\\recording.rec");

		@endcode
		
		@overloaded 
		
		None
		
		@related 

		InputUtilities::applyUserInputRecord(string fileName);
		InputUtilities::applyUserInputStreamStop();
		
		@remarks This is a replication of void replayUserInput(string fileName)
		*/
		static void applyUserInputReplay(string fileName);
	};
};

#endif //VBS2FUSION_INPUT_UTILITIES_H