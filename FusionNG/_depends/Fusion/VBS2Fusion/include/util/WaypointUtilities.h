/*
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 */

/*************************************************************************

Name:

WaypointUtilities.h

Purpose:

This file contains the declaration of the WaypointUtilities class.

Version Information:

Version		Date		Author and Comments
===========================================
1.0			27-03/2009	RMR: Original Implementation
1.01		27-08/2009  SDS: Added updateIsLocal(Waypoint& wp);			
							 updated updateWaypoint(Waypoint& wp);
2.0			11-01/2009	UDW: Version 2 Implementation
2.01		05-02/2010	MHA: Comments Checked
2.02		10-01/2011  CGS: Added updateNumber(Waypoint& wp)
								deleteWaypoint(Waypoint& wp )
2.03		08-09/2011	CGS: Added getAttachedObject(Waypoint& wp)
								setHousePosition(Waypoint& wp, int pos)
								getHousePosition(Waypoint& wp)
								WaypointUtilities::setWaypointVariable(Waypoint& wp, string name, VBS2Variable& val, bool publ)
								WaypointUtilities::getWaypointVariable(Waypoint& wp, string name)
2.04		04-11/2011	CGS: Added void applyRandomPosition(Waypoint& wp, position3D center, double radius)

/************************************************************************/

#ifndef VBS2FUSION_WAYPOINT_UTILITIES_H
#define VBS2FUSION_WAYPOINT_UTILITIES_H

#include <string>
#include <list>

#include "position3D.h"
#include "data/Waypoint.h"

namespace VBS2Fusion
{

	class VBS2FUSION_API WaypointUtilities
	{
	public:

		/*!
		Return the position of the waypoint.

		Error checking utility validates the following:

		- Alias (outputs an error if the alias is invalid)		
		*/
		static position3D getPosition(Waypoint& wp);

		/*!
		Set the waypoint to the position given by wp.getPosition(). 		
		*/
		static void applyPosition(Waypoint& wp);

		/*!
		Set the waypoint to the position given by position. The position is not updated
		on the wo object. Use update to check if changes have been applied.  

		Error checking utility validates the following:

		- Alias (outputs an error if the alias is invalid)		
		*/
		static void applyPosition(Waypoint& wp, position3D position);	

		/*!
		Update the position of the waypoint and save using wp.setPosition(). 

		Error checking utility validates the following:

		- Alias (outputs an error if the alias is invalid)		
		*/

		static void updatePosition(Waypoint& wp);

		//-------------------------------------------------------------------------

		/*!
		Get the direction of the waypoint.
		*/
		static double getDirection(Waypoint& wp);

		/*!
		Set the waypoint pointing in the direction given by wp.getDirection().		
		*/
		static void applyDirection(Waypoint& wp);		

		/*!
		Set the waypoint pointing in the direction given by direction. 

		Error checking utility validates the following:

		- Alias (outputs an error if the alias is invalid)		
		*/
		static void applyDirection(Waypoint& wp, double direction);

		/*!
		Update the direction of the waypoint and save using wp.setDirection(). 
		*/
		static void updateDirection(Waypoint& wp);

	//	//-------------------------------------------------------------------------

		/*!
		Get the description of the waypoint. 

		Error checking utility validates the following:

		- Alias (outputs an error if the alias is invalid)		
		*/
		static string getDescription(Waypoint& wp);

		/*!
		Apply the description given by wp.getDescription() to the waypoint. 
		*/
		static void applyDescription(Waypoint& wp);		

		/*!
		Apply the description given by description to the waypoint.

		Error checking utility validates the following:

		- Alias (outputs an error if the alias is invalid)		
		*/
		static void applyDescription(Waypoint& wp, string description);		

		/*!
		Update the description of the waypoint and save using wp.setDescription(). 
		*/
		static void updateDescription(Waypoint& wp);

		//-------------------------------------------------------------------------

		/*!
		Get the ShowMode of the waypoint in string format. 
		*/
		static string getShowMode(Waypoint& wp);

		/*!
		Apply the ShowMode given by wp.getWaypointShowMode() to the waypoint.	
		*/
		static void applyShowMode(Waypoint& wp);		

		/*!
		Apply the ShowMode given by showMode to the waypoint. 

		Error checking utility validates the following:

		- Alias (outputs an error if the alias is invalid)		
		*/
		static void applyShowMode(Waypoint& wp, string showMode);		

		/*!
		Update the showmode of the waypoint. 
		*/
		static void updateShowMode(Waypoint& wp);

		//-------------------------------------------------------------------------

		/*!
		Return the type of the waypoint in string format. 

		Error checking utility validates the following:

		- Alias (outputs an error if the alias is invalid)		
		*/
		static string getType(Waypoint& wp);

		/*!
		Set the type of the waypoint using wp.getWaypointType(); 
		*/
		static void applyType(Waypoint& wp);		

		/*!
		Set the type of the waypoint using type; 

		Error checking utility validates the following:

		- Alias (outputs an error if the alias is invalid)		
		*/
		static void applyType(Waypoint& wp, string type);		

		/*!
		Update the type of the waypoint and save using wp.setWaypointType(). 
		*/
		static void updateType(Waypoint& wp);

		//	//-------------------------------------------------------------------------

		/*!
		Return the combat mode of the waypoint. 

		Error checking utility validates the following:

		- Alias (outputs an error if the alias is invalid)	
		*/
		static string getCombatMode(Waypoint& wp);

		/*!
		Apply the combat mode obtained from wp.getWaypointCombatMode to the waypoint. 
		*/
		static void applyCombatMode(Waypoint& wp);		


		/*!
		Apply the combat mode obtained from combatMode to the waypoint. 

		Error checking utility validates the following:

		- Alias (outputs an error if the alias is invalid)	
		*/
		static void applyCombatMode(Waypoint& wp, string combatMode);	

		/*!
		Update the combat mode of the waypoint and save using wp.setWaypointCombatMode. 
		*/
		static void updateCombatMode(Waypoint& wp);

		//	//-------------------------------------------------------------------------

		/*!
		Return the behavior of the waypoint in string format. 

		Error checking utility validates the following:

		- Alias (outputs an error if the alias is invalid)	
		*/
		static string getBehaviour(Waypoint& wp);

		/*!
		Apply the behavior of the waypoint using wp.getWaypointBehaviour(). 
		*/
		static void applyBehaviour(Waypoint& wp);

		/*!
		Apply the behavior of the waypoint using behavior. 

		Error checking utility validates the following:

		- Alias (outputs an error if the alias is invalid)	
		*/
		static void applyBehaviour(Waypoint& wp, string behaviour);

		/*!
		Update the behavior of the waypoint and save using wp.setWAypointBehaviour. 
		*/
		static void updateBehaviour(Waypoint& wp);

		//-------------------------------------------------------------------------

		/*!
		Return the formation of the waypoint in string format.

		Error checking utility validates the following:

		- Alias (outputs an error if the alias is invalid)	
		*/
		static string getFormation(Waypoint& wp);

		/*!
		Apply the formation of the waypoint using wp.getWaypointFormation(). 
		*/
		static void applyFormation(Waypoint& wp);

		/*!
		Apply the formation of the waypoint using formation. 

		Error checking utility validates the following:

		- Alias (outputs an error if the alias is invalid)	
		*/
		static void applyFormation(Waypoint& wp, string formation);

		/*!
		Update the formation of the waypoint and save using wp.setWaypointFormation(). 
		*/
		static void updateFormation(Waypoint& wp);

		//-------------------------------------------------------------------------

		/*!
		Return the speed mode of the waypoint in string format.

		Error checking utility validates the following:

		- Alias (outputs an error if the alias is invalid)	
		*/
		static string getSpeedMode(Waypoint& wp);

		/*!
		Apply the speed mode of the waypoint using wp.getWaypointSpeedMode(). 
		*/
		static void applySpeedMode(Waypoint& wp);

		/*!
		Apply the speed mode of the waypoint using speedMode. 

		Error checking utility validates the following:

		- Alias (outputs an error if the alias is invalid)	
		*/
		static void applySpeedMode(Waypoint& wp, string speedMode);

		/*!
		Update the speed mode of the waypoint and save using wp.setSpeedMode(). 
		*/
		static void updateSpeedMode(Waypoint& wp);


		//-------------------------------------------------------------------------

		/*!
		Return the timeout values of the waypoint using a vector<double>.

		return parameter 1 : Min
		return parameter 1 : Mid
		return parameter 1 : Max

		Error checking utility validates the following:

		- Alias (outputs an error if the alias is invalid)	
		*/
		static vector<double> getTimeout(Waypoint& wp);

		/*!
		Apply the timeout values obtained from wp. 
		*/		
		static void applyTimeout(Waypoint& wp);

		/*!
		Apply the timeout values obtained from timeOut.

		Error checking utility validates the following:

		- Alias (outputs an error if the alias is invalid)	
		*/
		static void applyTimeout(Waypoint& wp, vector<double> timeOut);

		/*!
		Update the timeout values and save in wp. 
		*/
		static void updateTimeout(Waypoint& wp);


		//-------------------------------------------------------------------------

		/*!
		Return the waypoint statements

		Error checking utility validates the following:

		- Alias (outputs an error if the alias is invalid)	
		*/
		static string getWaypointStatements(Waypoint& wp);

		/*!
		Apply the statements obtained from wp. 
		*/
		static void applyStatements(Waypoint& wp);

		/*!
		Apply the statements specified in statements to the waypoint. 

		Error checking utility validates the following:

		- Alias (outputs an error if the alias is invalid)	
		*/
		static void applyStatements(Waypoint& wp, string statements);

		
		static void applyStatements(Waypoint& wp, string condition, string statements);

		/*!
		Update the waypoint statements. 
		*/
		static void updateStatements(Waypoint& wp);

		//-------------------------------------------------------------------------

		/*!
		Return the waypoint script.
		
		Error checking utility validates the following:

		- Alias (outputs an error if the alias is invalid)	
		*/
		static string getScript(Waypoint& wp);

		/*!
		Apply the script obtained from wp. 
		*/
		static void applyScript(Waypoint& wp);

		/*!
		Apply the script obtained from script.
		
		Error checking utility validates the following:

		- Alias (outputs an error if the alias is invalid)	
		*/
		static void applyScript(Waypoint& wp, string script);

		/*!
		Update the script for wp. 
		*/
		static void updateScript(Waypoint& wp);

		/*!
		Update the GroupWaypointNo for wp. 
		*/
		static void updateNumber(Waypoint& wp);

		/*!
		Compound update for the following characteristics:

		- behavior
		- combat mode
		- description
		- direction
		- formation
		- position
		- script
		- show mode
		- speed mode
		- statements
		- timeout
		- type
		*/
		static void updateWaypoint(Waypoint& wp);

		/*!
		Applies changes for the following characteristics:

		- behavior
		- combat mode
		- description
		- direction
		- formation
		- position
		- script
		- show mode
		- speed mode
		- statements
		- timeout
		- type
		*/
		static void applyChanges(Waypoint& wp);

		//-------------------------------------------------------
		// Network id Utility
		//-------------------------------------------------------

		/*!
		Update "isLocal" property in co to know whether co has valid
		network id or not		
		*/
		static void updateIsLocal(Waypoint& wp);

		/*!
		Create a Waypoint in VBS2		
		*/
		static void createWaypoint(Waypoint& wp);

		/*!
		Delete a Waypoint in VBS2		
		*/
		static void deleteWaypoint(Waypoint& wp);

		/*!
		Attaches a Object to the given Waypoint.
		Deprecated. Use void applyObjectAttach(Waypoint& wp, ControllableObject& co)
		*/
		VBS2FUSION_DPR(UWPT001) static void WaypointAttachObject(Waypoint& wp, ControllableObject& co);

		/*!
		Synchronizes a waypoint with other waypoint.		
		*/
		static void synchronizeWaypoint(Waypoint& wp, Waypoint& synwp);

		/*!
		Synchronizes a waypoint with other list of waypoints.		
		*/
		static void synchronizeWaypoints(Waypoint& wp, list<Waypoint> wplist);

		/*!
		Gets the list of waypoints the specified waypoint is synchronized with.	
		*/
		static list<Waypoint> getSynchronizedWaypoints(Waypoint& wp);

		/*!
		Sets the radius for a loiter waypoint. Waypoint should be in Loiter type.
		Deprecated. Use void applyLoiterRadius(Waypoint& wp, double radius)
		*/
		VBS2FUSION_DPR(UWPT002) static void setLoiterRadius(Waypoint& wp, double radius);

		/*!
		Returns the radius of a loiter waypoint. For other waypoint types it will give -1.		
		*/
		static double getLoiterRadius(Waypoint& wp);

		/*!
		Sets the radius for a loiter waypoint. Waypoint should be in Loiter type.
		Deprecated. Use void applyLoiterType(Waypoint& wp, string type)
		*/
		VBS2FUSION_DPR(UWPT003) static void setLoiterType(Waypoint& wp, string type);

		/*!
		Gets the radius for a loiter waypoint. Waypoint should be in Loiter type.	
		*/
		static string getLoiterType(Waypoint& wp);

		/*!
		Gets the Controllable object that attached to the waypoint.	
		*/
		static ControllableObject getAttachedObject(Waypoint& wp);

		/*!
		Sets the target house position for waypoints attached to a house,.	
		Deprecated. Use void applyHousePosition(Waypoint& wp, int pos)
		*/
		VBS2FUSION_DPR(UWPT004) static void setHousePosition(Waypoint& wp, int pos);

		/*!
		Gets the house position assigned to the waypoint.	
		*/
		static int getHousePosition(Waypoint& wp);

		/*!
		Sets variable to given value in the variable space of given Waypoint.
		Deprecated. Use void applyVariable(Waypoint& wp, string& name, VBS2Variable& val, bool isPublic)
		*/
		VBS2FUSION_DPR(UWPT005) static void setWaypointVariable(Waypoint& wp, string name, VBS2Variable& val, bool pub);

		/*!
		Gets the value of variable in the variable space of given Waypoint.	
		*/
		static VBS2Variable getWaypointVariable(Waypoint& wp, string name);

		/*!
		Set the waypoint to the random position in a circle with the given center and radius. The position 
		is not updated	on the wp object. Use update to check if changes have been applied.  	
		*/
		static void applyRandomPosition(Waypoint& wp, position3D center, double radius);

		/*!
		Set the radius around the waypoint where is the waypoint completed.
		Deprecated. Use void applyCompletionRadius(Waypoint& wp, double radius)
		*/
		VBS2FUSION_DPR(UWPT010) static void setWaypointCompletionRadius(Waypoint& wp, double radius);

		/*!
		Gets the radius around the waypoint where is the waypoint completed.
		*/
		static double getWaypointCompletionRadius(Waypoint& wp);

		/*!
		Gets waypoint visibility.
		*/
		static bool isWaypointVisible(Waypoint& wp);

		/*!
		Set waypoint's visibility.
		Deprecated. Use void applyVisibility(Waypoint& wp, bool visible)
		*/
		VBS2FUSION_DPR(UWPT011) static void setWaypointVisible(Waypoint& wp, bool visible);

		/*!
		Returns the statement that is executed when a trigger or way point is reached. 
		*/
		static string getEffectCondition(Waypoint& wp);
	
		/*!
		The statement is executed when the waypoint is activated and the effects are launched
		depending on the result.
			- If the result is a boolean and true, the effect is launched.
			- If the result is an object, the effect is launched if the result is the player or
			  the player vehicle.
			- If the result is an array, the effect is launched if the result contains the player
			  or the player vehicle. 
		
		Deprecated. Use void applyEffectCondition(Waypoint& wp, string& statement)
		*/
		VBS2FUSION_DPR(UWPT009) static void setEffectCondition(Waypoint& wp, string& statement);

		/*!
		Defines the music track played on activation. Track is a subclass name of CfgMusic. 
		In addition, "$NONE$" (no change) or "$STOP$" (stops the current music track). 
		Deprecated. Use void applyMusicEffect(Waypoint& wp, string& trackName)
		*/
		VBS2FUSION_DPR(UWPT006) static void setMusicEffect(Waypoint& wp, string& trackName);

		/*!
		Returns the defined music track played on activation on  waypoint.
		Track is a subclass name of CfgMusic or "$NONE$" or "$STOP$".  		
		*/
		static string getMusicEffect(Waypoint& wp);

		/*!
		Defines the different sound effects.
		Sound / voice plays a 2D / 3D sound from CfgSounds.
		SoundEnv plays an enviromental sound from CfgEnvSounds.
		SoundDet (only for triggers) creates a dynamic sound object attached to a trigger defined in CfgSFX. 

		Deprecated. Use void applySoundEffect(Waypoint& wp, string& sound, string& voice, string& soundEnv, string& soundDet)
		*/
		VBS2FUSION_DPR(UWPT007) static void setSoundEffect(Waypoint& wp, string& sound, string& voice, string& soundEnv, string& soundDet);
		
		/*!
		Returns the sound, voice, soundEnv, and soundDet for a waypoint. 
		Return value : vector of string which contains sound, voice, soundEnv and soundDet
		*/
		static vector<string> getSoundEffect(Waypoint& wp);

		/*!
		Defines the title effect via [Type, Effect, Text] where

		'Type' can be,
			- "NONE",
			- "OBJECT",
				'Text' defines the shown object , a subclass of CfgTitles. 
			- "RES"
				'Text' defines a resource class, a subclass of RscTitles. 
			- "TEXT"
				The 'Text' is shown as text itself.
				
		'Effect' defines a subtype: 
			"PLAIN", "PLAIN DOWN", "BLACK", "BLACK FADED", "BLACK OUT", 
			"BLACK IN", "WHITE OUT" or "WHITE IN". 
		
		Deprecated. Use void applyTitleEffect(Waypoint& wp, TITLE_EFFECT_CLASS classType, TITLE_EFFECT_TYPE effect, string& text)
		*/
		VBS2FUSION_DPR(UWPT008) static void setTitleEffect(Waypoint& wp, string& type, string& effect, string& text);

		/*!
		Returns the defined title effect for a waypoint.
		Return :  vector of string which contains type, effect and text

		'type' can be "NONE", "OBJECT", "RES" or "TEXT".
		'effect' returns the selected Title Effect Type for type "TEXT".
		'text' returns the shown object (a subclass of CfgTitles) for type "OBJECT", the resource
		class (a subclass of RscTitles) for type "RES", and the plain text string for type "TEXT"
		*/
		static vector<string> getTitleEffect(Waypoint& wp);

		/*!
		Attaches a Object to the given Waypoint.

		@param wp - Waypoint that object is attached.
		@param co - ControllableObject to be attached.

		@return Nothing.

		@remarks This is a replication of void WaypointAttachObject(Waypoint& wp, ControllableObject& co) function. 
		*/
		static void applyObjectAttach(Waypoint& wp, ControllableObject& co);

		/*!
		Applies the radius for a loiter waypoint. Waypoint should be in Loiter type.

		@param wp - Waypoint that radius is applied to.
		@param radius - Loiter radius to be applied. If no loiter radius is defined, a default radius is used and minimal executable radius 
			depends on the speed and maneuverability of the aircraft.

		@return Nothing.

		@remarks This is a replication of void setLoiterRadius(Waypoint& wp, double radius) function. 
		*/
		static void applyLoiterRadius(Waypoint& wp, double radius);

		/*!
		Defines the type of loitering mode for a loiter waypoint. Waypoint should be in Loiter type.

		@param wp - Waypoint that type of loitering mode is applied to.
		@param radius - Loitering type. It can be,
			"CIRCLE" - Move in a circle around loiter waypoint, clockwise
			"CIRCLE_L" - Move in a circle around loiter waypoint, anticlockwise

		@return Nothing.

		@remarks This is a replication of void setLoiterType(Waypoint& wp, string type) function. 
			Waypoint can be set to "LOITER" mode by using void applyType(Waypoint& wp, string type) function.
		*/
		static void applyLoiterType(Waypoint& wp, string type);

		/*!
		Defines the target house position for waypoints attached to a house.

		@param wp - Waypoint that is attached to a house.
		@param pos -  The target house position.

		@return Nothing.

		@remarks This is a replication of void setHousePosition(Waypoint& wp, int pos) function. 
		*/
		static void applyHousePosition(Waypoint& wp, int pos);

		/*!
		Applies variable to given value in the variable space of given Waypoint.	
		
		@param wp - Waypoint to assign variable to.
		@param name -  Name of variable. Variable name is case-sensitive.
		@param val - Value to assign to variable.
		@param isPublic -  If true, then the value is broadcast to all computers.
		
		@return Nothing.

		@remarks This is a replication of void setWaypointVariable(Waypoint& wp, string name, VBS2Variable& val, bool pub) function. 
		*/
		static void applyVariable(Waypoint& wp, string& name, VBS2Variable& val, bool isPublic);

		/*!
		Defines the music track played on activation.  

		@param wp - Waypoint that music track is defined to.
		@param trackName - Name of the music track that will be played on waypoint activation. Track is a subclass name of CfgMusic 
			(http://resources.bisimulations.com/content/classes/CfgMusic.html) defined by VBS2.
			In addition, "$NONE$" (no change) or "$STOP$" (stops the current music track).  

		@return Nothing.

		@remarks This is a replication of void setMusicEffect(Waypoint& wp, string& trackName) function. 
		*/
		static void applyMusicEffect(Waypoint& wp, string& trackName);

		/*!
		Defines the different sound effects.

		@param wp - Waypoint that sound effects are defined to.
		@param sound - Name of the 2D / 3D sound from CfgSounds.
		@param voice - Name of the 2D / 3D sound from CfgSounds.
		@param soundEnv-  Name of the environmental sound from CfgEnvSounds.
		@param soundDet - Creates a dynamic sound object attached to a waypoint defined in CfgSFX. 

		@return Nothing.

		@remarks This is a replication of void setSoundEffect(Waypoint& wp, string& sound, string& voice, string& soundEnv, string& soundDet function.
		*/
		static void applySoundEffect(Waypoint& wp, string& sound, string& voice, string& soundEnv, string& soundDet);

		/*!
		Defines the title effect.

		@param wp - Waypoint that title effect is defined to.
		@param classType - Type defines in TITLE_EFFECT_CLASS enum and it can be,
		- TEC_NONE - "NONE",
		- TEC_OBJECT - "OBJECT" : 'Text' defines the shown object , a subclass of CfgTitles. 
		- TEC_RES - "RES" : 'Text' defines a resource class, a subclass of RscTitles. 
		- TEC_TEXT - "TEXT" : The 'Text' is shown as text itself.		
		@param effect - Effect defines in TITLE_EFFECT_TYPE enum.
		@param text - Text to be shown. 

		@return Nothing.

		@remarks This is a replication of void setTitleEffect(Waypoint& wp, string& type, string& effect, string& text) function.
		*/
		static void applyTitleEffect(Waypoint& wp, TITLE_EFFECT_CLASS classType, TITLE_EFFECT_TYPE effect, string& text);

		/*!
		The vbs2 script statement is executed when the waypoint is activated and the effects are launched depending on the result of the statement.
			- If the result is a boolean and true, the effect is launched.
			- If the result is an object, the effect is launched if the result is the player or
			  the player vehicle.
			- If the result is an array, the effect is launched if the result contains the player
			  or the player vehicle. 

	    @param wp - Waypoint object.
	    @param statement - vbs2 script statement that is executed when the waypoint is activated.

	    @return Nothing.

	    @remarks This is a replication of void setEffectCondition(Waypoint& wp, string& statement) function.
		*/
		static void applyEffectCondition(Waypoint& wp, string& statement);

		/*!
		Applies the radius around the waypoint. The completion radius allows units to call the waypoint completed once they are
		inside of the given circle. Only has an effect on units in behavior mode "COMBAT", and if the completion radius is larger
		than 25m. Even then, the distance at which a waypoint is considered completed may vary by about 10m from the specified value. 

		@param wp - Waypoint that completion radius is applied to.
		@param radius - Radius value in meters.

		@return Nothing.

		@remarks This is a replication of void setWaypointCompletionRadius(Waypoint& wp, double radius) function.
		*/
		static void applyCompletionRadius(Waypoint& wp, double radius);

		/*!
		Sets the visibility of the waypoint. 

		@param wp - Waypoint that visibility is applied to.
		@param visible - If 'true', waypoint will be visible.

		@return Nothing.

		@remarks This is a replication of void setWaypointVisible(Waypoint& wp, bool visible) function.
		*/
		static void applyVisibility(Waypoint& wp, bool visible);
	};	
};

#endif //WAYPOINT_UTILITIES_H