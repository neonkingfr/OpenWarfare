/*
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 */

/*************************************************************************

Name:

	EnvironmentStateUtilities.h

Purpose:

	This file contains the declaration of the EnvironmentStateUtilities class.

Version Information:

	Version		Date		Author and Comments
	===========================================
	1.0			12-11/2009	RMR: Original Implementation

	2.0			10-02/2010	RMR: Added	vector<float> loadWind()
										void applyWind(vector<float> _value)
										void applyWind(EnvironmentState& state)
										applyWind(EnvironmentState& state, vector<float> _value)
	2.01		10-02/2010	MHA: Comments Checked
	2.02		03-05/2011	SSD: Added	void applySurfaceMoisture(EnvironmentState& state);
										void applySurfaceMoisture(double value);
										void updateSurfaceMoisture(EnvironmentState& state);
										void applyMaxRainDensity(EnvironmentState& state);
										void applyMaxRainDensity(double value);
										void updateMaxRainDensity(EnvironmentState& state);
										void applyMaxEvaporation(EnvironmentState& state);
										void applyMaxEvaporation(double value);
										void updateMaxEvaporation(EnvironmentState& state);
										void applySurfaceDrainageSpeed(EnvironmentState& state);
										void applySurfaceDrainageSpeed(double value);
										void updateSurfaceDrainageSpeed(EnvironmentState& state);
										void applyWaterLevel(EnvironmentState& state);
										void applyWaterLevel(double value);
										vector<int> getSunRise(int year, int month, int date, int hours = 0, int minutes = 0);
										vector<int> getSunsSt(int year, int month, int date, int hours = 0, int minutes = 0);
										void setRandomWeather(bool mode);
										bool getRandomWeather();

/************************************************************************/

#ifndef VBS2FUSION_ENVIRONMENT_STATE_UTILITIES_H
#define VBS2FUSION_ENVIRONMENT_STATE_UTILITIES_H

/***********************************************************************/
/* INCLUDES
/***********************************************************************/
// STANDARD INCLUDES
#include <sstream>
#include <string>

// SIMCENTRIC INCLUDES
#include "data/EnvironmentState.h"
#include "conversions.h"
/**********************************************************************/
/* END INCLUDES
/**********************************************************************/


namespace VBS2Fusion
{
	class VBS2FUSION_API EnvironmentStateUtilities
	{
	public:

		/*!
		This function applies the parameters specified in state to the 
		game environment. The value specified by state.getFog(), state.getRain(), 
		state.getOvercast(), state.getTimeYear(), state.getTimeMonth(), 
		state.getTimeDay(), state.getTimeHours() and state.getTimeMinutes
		are applied to the game environment. No delay is used when applying these
		changes. Changes are therefore registered immediately. 
		*/
		static void applyEnvironmentState(EnvironmentState& state);
		

		/*!
		This function applies the parameters specified in state to the 
		game environment. The value specified by state.getFog(), state.getRain(), 
		state.getOvercast(), state.getTimeYear(), state.getTimeMonth(), 
		state.getTimeDay(), state.getTimeHours() and state.getTimeMinutes
		are applied to the game environment. The delay specified by 
		delay is used when applying these changes. The value of delay is given in seconds. 
		Changes are therefore registered not registered immediately. 
		*/
		static void applyEnvironmentState(EnvironmentState& state, int delay);

		/*!
		The current environment state is loaded onto the state variable. The variables
		overcast, rain, fog, TimeYear, TimeMonth, TimeDay, TimeHours and TimeMinutes are
		updated from the game environment. Also updates the amount of time elapsed
		in the game. 
		*/

		static void loadEnvironmentState(EnvironmentState& state);
		
		//----------------------------------------------------------

		/*!
		The value specified by state.getFog() is used to apply the fog
		value in the game with no delay .
		*/		
		
		static void applyFog(EnvironmentState& state);	

		/*!
		The value specified by state.getFog() is used to apply the fog
		value in the game using the delay specified. 
		*/

		static void applyFog(EnvironmentState& state, int Delay);	

		/*!
		The value specified by value is used to apply the fog
		value in the game with no delay .
		*/
		static void applyFog(float& value);	

		/*!
		The value specified by value is used to apply the fog
		value in the game with no delay .
		*/
		static void applyFog(double& value);	

		/*!
		The value specified by state.getFog() is used to apply the fog
		value in the game using the delay specified.  
		*/
		static void applyFog(float& value, int Delay);	

		/*!
		The value specified by state.getFog() is used to apply the fog
		value in the game using the delay specified.  
		*/
		static void applyFog(double& value, int Delay);

		/*! 
		Returns the current fog setting value.
		*/

		static double loadFog();

		//----------------------------------------------------------

		/*!
		The value specified by state.getOvercast() is used to apply the overcast
		value in the game with no delay. 
		*/	

		static void applyOvercast(EnvironmentState& state);

		/*!
		The value specified by state.getOvercast() is used to apply the overcast
		value in the game in the delay specified. 
		*/	

		static void applyOvercast(EnvironmentState& state, int Delay);
		
		/*!
		The value specified by value is used to apply the overcast
		value in the game with no delay. 
		*/	
		static void applyOvercast(float& value);	

		/*!
		The value specified by value is used to apply the overcast
		value in the game using the delay specified. 
		*/	
		static void applyOvercast(double& value, int Delay);

		/*!
		The value specified by value is used to apply the overcast
		value in the game with no delay. 
		*/	
		static void applyOvercast(double& value);	

		/*!
		The value specified by value is used to apply the overcast
		value in the game using the delay specified. 
		*/	
		static void applyOvercast(float& value, int Delay);
		
		/*!
		Returns the current overcast setting in the game. 
		*/	

		static double loadOvercast();
		

		//----------------------------------------------------------

		/*!
		The value specified by state.getRain() is used to apply the rain
		value in the game with no delay.
		*/	
		static void applyRain(EnvironmentState& state);	

		/*!
		The value specified by state.getRain() is used to apply the rain
		value in the game using the specified delay. 
		*/	
		static void applyRain(EnvironmentState& state, int Delay);	

		/*!
		The value specified by value is used to apply the rain
		value in the game with no delay. 
		*/	
		static void applyRain(double& value);	

		/*!
		The value specified by state.getRain() is used to apply the rain
		value in the game using the specified delay. 
		*/	
		static void applyRain(double& value, int Delay);	

		/*!
		The value specified by value is used to apply the rain
		value in the game with no delay. 
		*/	
		static void applyRain(float& value);	

		/*!
		The value specified by state.getRain() is used to apply the rain
		value in the game using the specified delay. 
		*/	

		static void applyRain(float& value, int Delay);	

		/*!
		Returns the current Rain setting in the game. 
		*/	
		static double loadRain();
		

		//----------------------------------------------------------

		/*!
		Applies the time variables (i.e. TimeYear, TimeMonth, 
		TimeDay, TimeHours and TimeMinutes to the game environment
		with no delay. 
		*/	
		static void applyTime(EnvironmentState& state);	

		/*!
		Applies the time variables (i.e. TimeYear, TimeMonth, 
		TimeDay, TimeHours and TimeMinutes to the game environment
		using the specified delay. 
		*/
		static void applyTime(EnvironmentState& state, int Delay);
		
		//--------------------------------------------------------------------

		/*! 
		Returns the current game environment year.
		*/
		static int loadYear();	

		/*! 
		Returns the current game environment month. 
		*/
		static int loadMonth();	

		/*! 
		Returns the current game environment day.
		*/
		static int loadDay();	

		/*! 
		Returns the current game environment hour.
		*/
		static int loadHour();	

		/*! 
		Returns the current game environment number of minutes.
		*/
		static int loadMinutes();

		//--------------------------------------------------------------------

		/*! 
		Returns the amount of time elapsed (in seconds) in the current
		mission.
		*/
		static double loadTimeElapsed();

		//--------------------------------------------------------------------

		/*!
		Returns the current Rain setting in the game. 
		*/
		static vector<float> loadWind();

		/*!
		Returns the current Rain setting in the game. 
		*/
		static vector<double> loadWindEx();

		/*!
		The value specified by vector is used to apply the wind
		value in the game. 
		*/	
		static void applyWind(vector<double> _value);

		/*!
		The value specified by vector is used to apply the wind
		value in the game. 
		*/	
		static void applyWind(vector<float> _value);

		/*!
		The value specified by state.getWind() is used to apply the wind
		value in the game.
		*/	
		static void applyWind(EnvironmentState& state);

		/*!
		The value specified by vector is used to apply the wind
		value in the game. 
		*/	
		static void applyWind(EnvironmentState& state, vector<float> _value);

		/*!
		The value specified by state.getSurfaceMoisture() is used to apply 
		the current surface moisture value in the game. 
		*/
		static void applySurfaceMoisture(EnvironmentState& state);
		/*!
		The value specified by value is used to apply the current surface 
		moisture value in the game.
		*/
		static void applySurfaceMoisture(double value);
		/*!
		Update the current surface moisture of the terrain.
		*/
		static void updateSurfaceMoisture(EnvironmentState& state);


		/*!
		The value specified by state.getMaxRainDensity() is used to apply 
		the maximal rain density value in the game. 
		*/
		static void applyMaxRainDensity(EnvironmentState& state);
		/*!
		The value specified by value is used to apply the maximal rain 
		density value in the game.
		*/
		static void applyMaxRainDensity(double value);
		/*!
		Update the maximal rain density of the terrain.
		*/
		static void updateMaxRainDensity(EnvironmentState& state);


		/*!
		The value specified by state.getMaxEvaporation() is used to apply 
		the maximal evaporation value for best weather conditions in the game. 
		*/
		static void applyMaxEvaporation(EnvironmentState& state);
		/*!
		The value specified by value is used to apply the maximal evaporation 
		value for best weather conditions in the game.
		*/
		static void applyMaxEvaporation(double value);
		/*!
		Update the maximal evaporation for best weather conditions.
		*/
		static void updateMaxEvaporation(EnvironmentState& state);


		/*!
		The value specified by state.getSurfaceDrainageSpeed() is used to apply 
		the surface drainage speed value in the game. 
		*/
		static void applySurfaceDrainageSpeed(EnvironmentState& state);
		/*!
		The value specified by value is used to apply the surface drainage speed 
		value in the game.
		*/
		static void applySurfaceDrainageSpeed(double value);
		/*!
		Update the surface drainage speed value.
		*/
		static void updateSurfaceDrainageSpeed(EnvironmentState& state);

		/*!
		The value specified by state.getWaterLevel() is used to apply 
		the sea level value in the game. 
		*/
		static void applyWaterLevel(EnvironmentState& state);
		/*!
		The value specified by value is used to apply the sea level 
		value in the game.
		*/
		static void applyWaterLevel(double value);


		/*!
		Returns the time the sun will rise on the specified date, at the current world location
		The return vector contains the sun rise in the order [year,month,date,hour,minute].
		*/
		static vector<int> getSunRise(int year, int month, int date, int hours = 0, int minutes = 0);

		/*!
		Returns the time the sun will set on the specified date, at the current world location
		The return vector contains the sun set in the order [year,month,date,hour,minute].
		*/
		static vector<int> getSunSet(int year, int month, int date, int hours = 0, int minutes = 0);

		/*!
		Switches random weather on and off.
		Deprecated. Use static void applyRandomWeather(bool mode).
		*/
		VBS2FUSION_DPR(UENV001) static void setRandomWeather(bool mode);

		/*!
		Returns current state of random weather flag.
		*/
		static bool getRandomWeather();

		/*!
		Enables/disables environmental effects (ambient life + sound). 
		*/
		static void enableEnvironment(bool enabled);
		
		/*!
		@description  

		Switches random weather on and off.  

		@locality

		Locally applied, locally effected

		@version [VBS2Fusion v3.12] 

		@param mode - If true, random weather is created.

		@return nothing.

		@example

		@code

		EnvironmentStateUtilities::applyRandomWeather(true);
		
		@endcode

		@overloaded 

		@related  

		@remarks This is a replication of void setRandomWeather(bool mode)
		*/
		static void applyRandomWeather(bool mode);

	};
};

#endif