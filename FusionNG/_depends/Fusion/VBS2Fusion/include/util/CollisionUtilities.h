/*
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 */

/*************************************************************************

Name:

	CollisionUtilities.h

Purpose:

	This file contains the declaration of the CollisionUtilities class.

Version Information:

	Version		Date		Author and Comments
	===========================================
	1.0			28-09/2009	RMR: Original Implementation
	2.0			10-01/2009	UDW: Version 2 Implementation
	2.01		10-02/2010	MHA: Comments Checked
	
/************************************************************************/

#ifndef VBS2FUSION_COLLISION_UTILITIES_H
#define VBS2FUSION_COLLISION_UTILITIES_H

#include <sstream>
#include <string>
#include <math.h>

#include <list>

#include "position3D.h"
#include "VBS2Fusion.h"
#include "conversions.h"
#include "data/ControllableObject.h"
#include "util/ExecutionUtilities.h"
#include "DataContainers/ObjectList.h"
#include "data/Group.h"
#include "util/TerrainUtilities.h"

#include "VBS2FusionAppContext.h"

namespace VBS2Fusion
{		

	struct VBS2FUSION_API CollisionDetectionReturnValue
	{
		string ObjectName;
		string ObjectAlias;
		string ObjectNetworkID;
		position3D HitPosition;
		position3D Direction;
	};


	class VBS2FUSION_API CollisionUtilities
	{
	public:

		/*!
		Returns true if a fired projectile would impact an object (i.e. object2) if fired from the specified position (i.e. object1.getPositionASL()).
		Considers only the origin (the center) of the object, so if only a part of the object is visible the command will still return false.
		*/
		static bool isCollision(ControllableObject& object1, ControllableObject& object2);

		/*!
		Returns true if a fired projectile would impact an object (i.e. object2) if fired from the specified position (expects ASL values).
		Considers only the origin (the center) of the object, so if only a part of the object is visible the command will still return false.
		*/
		static bool isCollision(position3D position, ControllableObject& object);	

		/*!
		Returns true if a fired projectile would impact the objects (i.e. given in _List) if fired from the specified position (i.e. object1.getPositionASL()).
		Considers only the origin (the center) of the object, so if only a part of the object is visible the command will still return false.
		*/
		static list<bool> isCollision(ControllableObject& object, ObjectList<ControllableObject>& _List);

		/*!
		Returns true if a fired projectile would impact the object (i.e. given in _List) if fired from the specified position (expects ASL values).
		Considers only the origin (the center) of the object, so if only a part of the object is visible the command will still return false.
		*/
		static list<bool> isCollision(position3D position, ObjectList<ControllableObject>& _List);

		/*!
		Returns true if a fired projectile would impact any of the objects (belonging to group) if fired from the specified position (i.e. object1.getPositionASL()).
		Considers only the origin (the center) of the object, so if only a part of the object is visible the command will still return false.
		*/
		static list<bool> isCollision(ControllableObject& object, Group& group);

		/*!
		Returns true if a fired projectile would impact any of the object (belonging to group) if fired from the specified position (expects ASL values).
		Considers only the origin (the center) of the object, so if only a part of the object is visible the command will still return false.
		*/
		static list<bool> isCollision(position3D position, Group& group);

		/*!
		Returns an array of all objects that are within the path from the start to the end position. It does not detect collisions with the ground.
		The return value is sorted in order of distance from the startpos (from nearest to farthest).

		The return array is a list of CollisionDetectionReturnValue. A CollisionDetectionReturnValue contains

		- The name of the object
		- A random alias assigned to the object. (This random alias can be used to access the object locally)
		- The network ID of the object
		- The hit position (in position3D format)
		- The direction of the collision (in position3D format)
		  Deprecated Use list<CollisionDetectionReturnValue> getCollision(position3D startPos, position3D endPos, double radius, ControllableObject& ignoreObject, COLLISIONTESTTYPE collisionType=FIRE)
		*/
		VBS2FUSION_DPR(UCOL001) static list<CollisionDetectionReturnValue> CollisionDetection(position3D startPos, position3D endPos, double radius, ControllableObject& ignoreObject, COLLISIONTESTTYPE collisionType=FIRE);

		/*!
		@description

		Find named selections in object which is in specified 'lod' intersected by given section of a line.
		Return value is in the form of [selection, number]. Multiple returned arrays are nested within a single array, and no intersection will return an empty array.
		"LodName" is one of: "FIRE", "VIEW", "GEOM", "IFIRE" 

		@locality

		Globally Applied, Globally Effected

		@version [VBS2Fusion v3.12] 

		@param object - object

		@param enum	  - COLLISIONTESTTYPE FIRE, VIEW, GEOM, IFIRED

		@param position3D - starting position.

		@param position3D - end position.

		@return vector<INTERSECT_INFO> - string name,double distance,position3D normal;

		@example

		@code

		vector<INTERSECT_INFO> intersectInfo = CollisionUtilities::getIntersect(vehicle,COLLISIONTESTTYPE::FIRE,vehicle.getPosition(),position3D(vehicle.getPosition().getX(),vehicle.getPosition().getZ(),5));

		@endcode

		@overloaded 

		None 

		@related  

		None

		@remarks To obtain the intersect of an object created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function CollisionUtilities::getIntersect(ControllableObject& object, COLLISIONTESTTYPE colType, position3D startPos, position3D endPos).  
		*/
		static vector<INTERSECT_INFO> getIntersect(ControllableObject& object, COLLISIONTESTTYPE colType, position3D startPos, position3D endPos);
		
		/*!
		@description

		Returns an array of all objects that are within the path from the start to the end position. It does not detect collisions with the ground.
		The return value is sorted in order of distance from the startpos (from nearest to farthest).

		@locality

		Globally Applied

		@version [VBS2Fusion v3.12] 
		
		@param	startpos: PositionASL ,Position:[x,y,z]

		@param	endpos: PositionASL ,Position:[x,y,z]

		@param	radius:- minimum radius of objects that should be checked for. A real number,

				Smaller items than specified could still be found, but it is not guaranteed. (optional, default: 1)

		@param	ignore:  A controllable Object that is not considered in the collision path.
				(Vehicle crews will still cause collisions, even when defined here.) (optional)

		@param	lod:  - LOD to consider in collision detection. Can be "FIRE", "VIEW", "GEOM" or "IFIRE". (optional, default: FIRE)	

		@return Array - Each object is returned as a 3-dimensional array, containing the Object, the Hit Position and the Direction.

		@example

		@code

		list<CollisionDetectionReturnValue> colList = CollisionUtilities::getCollision(position3D(2580.06,2415.49,14.86),position3D(2580.06,2715.49,14.86),5,playerUnit,COLLISIONTESTTYPE::FIRE);

		@endcode

		@overloaded 

		None

		@related  

		None

		@remarks This is a replication of list<CollisionDetectionReturnValue> CollisionDetection(position3D startPos, position3D endPos, double radius, ControllableObject& ignoreObject, COLLISIONTESTTYPE collisionType=FIRE)
		*/		
		
		static list<CollisionDetectionReturnValue> getCollision(position3D startPos, position3D endPos, double radius, ControllableObject& ignoreObject, COLLISIONTESTTYPE collisionType=FIRE);

	};
};

#endif //COLLISION_UTILITIES_H