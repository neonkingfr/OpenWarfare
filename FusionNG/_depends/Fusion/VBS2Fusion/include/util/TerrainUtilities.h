
 /**************************************************************************
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 **************************************************************************/

/*************************************************************************

Name:

	TerrainUtilities.h

Purpose:

	This file contains the declaration of the TerrainUtilities class.

Version Information:

	Version		Date		Author and Comments
	===========================================
	1.0			28-04/2009	RMR: Original Implementation
	2.0			10-02/2010  YFP: Version 2 Implementation
	2.01		10-02/2010	MHA: Comments Checked
	2.02        26-05/2010  YFP: Added Methods,
									void exportTerrainMapSHP(string,string);
	2.03		18-01-2011	CGS:  Modified Method GetNearestCollisionInfo
	2.04		25-07-2011	SSD: Added	bool cylinderCollision(...)
	2.05		21-11-2011	DMB: Added functions
									bool surfaceIsWater(position3D pos)
									string getSurfaceType(position3D pos)
									double getSurfRoughness(position3D pos)
	

/************************************************************************/

#ifndef VBS2FUSION_TERRAIN_UTILITIES_H
#define VBS2FUSION_TERRAIN_UTILITIES_H

/***********************************************************************/
/* INCLUDES
/***********************************************************************/
// STANDARD INCLUDES
#include <sstream>
#include <string>
#include <list>
// SIMCENTRIC INCLUDES
#include "VBS2Fusion.h"
#include "util/ExecutionUtilities.h"
#include "VBS2FusionDefinitions.h"
#include "position2D.h"
#include "data/ControllableObject.h"
/**********************************************************************/
/* END INCLUDES
/**********************************************************************/

namespace VBS2Fusion
{
	
	class VBS2FUSION_API TerrainUtilities
	{
	public:
		
		/*!
		Returns the terrain cell height for coordinate position
		[xCoordinate, zCoordinate]. 
		Deprecated. Use double getTerrainHeight(double xCoordinate, double zCoordinate)
		*/
		VBS2FUSION_DPR(UTER001) static double TerrainHeight(double xCoordinate, double zCoordinate);

		/*!
		Returns the height above water of the road surface at the given position 
		[xCoordinate, zCoordinate]. 
		Deprecated. Use double getRoadSurfaceHeight(double xCoordinate, double zCoordinate)
		*/
		VBS2FUSION_DPR(UTER002) static double RoadSurfaceHeight(double xCoordinate, double zCoordinate);

		/*!
		Returns true if there is a collision detected between the two points. 

		startPos - a position3D parameter giving the position ASL of the start point. The position should be certain height from land
		endPos - a position3D parameter giving the position ASL of the end point. The position should be certain height from land
		type - The type of collision test to perform. Should be one of {FIRE, VIEW, GEOM or IFIRED}. 
		radius - The radius of the cylinder used to perform the collision test. 
		density - The spacing between casted rays. 
		ignoreObjectAlias = [Optional] parameter to specify the alias of an object to ignore during the collision test. 

		NOTE: This function will cast at least a single ray even in circumstances where the radius is much smaller
		than the density specified. Use relevant density and radius values to perform custom collision detection tasks. 
		*/
		static bool IsColliding(position3D startPos, 
			position3D endPos, 
			COLLISIONTESTTYPE type, 
			double radius, 
			double density, 
			string ignoreObjectAlias = "");


		/*!
		Returns true if there is a collision detected between the two points. 

		startPos - a position3D parameter giving the position ASL of the start point. The position should be certain height from land
		endPos - a position3D parameter giving the position ASL of the end point. The position should be certain height from land
		type - The type of collision test to perform. Should be one of {FIRE, VIEW, GEOM or IFIRED}. 
		radius - The radius of the cylinder used to perform the collision test. 
		density - The spacing between casted rays. 
		ignoreObjectAlias = [Optional] parameter to specify the alias of an object to ignore during the collision test. 

		In the case of a detected collision (i.e. return value is true), this function will also store
		the coordinates of the first collision point in the variable firstCollision and the normal of that collision
		in the parameter normal. 
		If a collision is not detected, 'endPos' will be returned as the first collision point.

		NOTE: This function will cast at least a single ray even in circumstances where the radius is much smaller
		than the density specified. Use relevant density and radius values to perform custom collision detection tasks. 
		*/
		static bool GetNearestCollisionInfo(position3D startPos, 
			position3D endPos,
			position3D& firstCollision, 
			position3D& normal,
			COLLISIONTESTTYPE type,
			double radius,
			double density,
			string ignoreObjectAlias = "");
	
		/*!
		This function returns true if a collision is detected within a cylinder of 
		height = (radius * 2) and radius = radius is placed upright 
		with its center at pos. 

		Useful function for determining if a unit or a similar object can be placed
		at a certain position. 

		Note: The considered cylinder should not collide with the land.
		So select the (position3D)center height larger than radius.
		Deprecated. Use bool isUprightCylinderCollision(position3D& center, double radius)
		*/
		VBS2FUSION_DPR(UTER003) static bool uprightCylinderCollision(position3D& center, double radius);

		/*!
		Checks the specified position is on a Road.
		*/
		static bool isOnRoad(position3D& position);

		/*!
		Check the specified object is on the road. 
		*/
		static bool isOnRoad(ControllableObject& co);

		/*! 
		Export terrain map to a shape file. 
		objectFileName - File name for export objects. 
		roadFileName   - File name for export roads.
		Deprecated. Use bool applyMapExportToSHP(string objectFileName, string roadFileName)
		*/
		VBS2FUSION_DPR(UTER004) static bool exportTerrainMapSHP(string objectFileName , string roadFileName);	

		/*!
		Converts the an in-game position to a grid string. Returns an 8-digit string. 
		Deprecated. Use string getPositionToGrid(position3D pos)
		*/
		VBS2FUSION_DPR(UTER005) static string positionToGrid(position3D pos);

		/*!
		Converts the grid coordinates to in-game position. Returns a position3D coordinate. 
		Deprecated. Use position3D getGridToPosition(string gridPosition)
		*/
		VBS2FUSION_DPR(UTER006) static position3D gridtoPosition(string gridPosition);

		/*!
		Converts the game position to the MGRS coordinates.
		Deprecated. Use string getPositionToMGRS(position3D pos)
		*/
		VBS2FUSION_DPR(UTER008) static string positionToMGRS(position3D pos);

		/*!
		Converts the game position to the MGRS co ordinates with precision specified(Maximum is 5).   
		Deprecated. Use string getPositionToMGRS(position3D pos, int precision)
		*/
		VBS2FUSION_DPR(UTER007) static string positionToMGRS(position3D pos, int precision);

		/*!
		Converts the game position to Latitude.
		Deprecated. Use string getPositionToLat(position3D pos)
		*/
		VBS2FUSION_DPR(UTER010) static string positionToLat(position3D pos);

		/*!
		Converts the game position to Latitude with precision specified.
		Deprecated. Use string getPositionToLat(position3D pos , int precision)
		*/
		VBS2FUSION_DPR(UTER009) static string positionToLat(position3D pos , int precision);

		/*!
		Converts the game position to Longitude. 
		Deprecated. Use string getPositionToLon(position3D pos)
		*/										
		VBS2FUSION_DPR(UTER012) static string positionToLon(position3D pos);

		/*!
		Converts the game position to Longitude with precision specified.
		Deprecated. Use string getPositionToLon(position3D pos, int precision)
		*/										
		VBS2FUSION_DPR(UTER011) static string positionToLon(position3D pos, int precision);

		/*!
		Returns the top left grid position string of the map of current mission. 
		*/
		static string getTopLeftGridPos();

		/*!
		Returns the bottom right grid position string of the map of current mission. 
		*/
		static string getBottomRightGridPos();

		/*!
		Returns the top left grid position of the map of current mission. 
		*/
		static position3D getTopLeftPos();

		/*!
		Returns the bottom right grid position of the map of current mission. 
		*/
		static position3D getBottomRightPos();

		/*!
		Returns the ground intercept point (position) along the specified direction. 
		position - origin position, in other words a position in the vector line
		direction - direction to find intercept the ground
		*/
		static position3D getGroundIntercept(position3D position, vector3D direction);

		/*!
		Returns the ground intercept point (position) along the specified direction.
		position - origin position as ASL position, in other words a position in the vector line
		direction - direction to find intercept the ground
		*/
		static position3D getGroundInterceptASL(position3D position, vector3D direction);

		/*!
		Returns the ground intercept point (position) along the specified direction. 
		co - This object position is origin position, in other words a position in the vector line
		direction - direction to find intercept the ground
		*/
		static position3D getGroundIntercept(ControllableObject& co, vector3D direction);

		/*!
		Returns the ground intercept point (position) along the specified direction.
		co - This object position is origin position as ASL position, in other words a position in the vector line
		direction - direction to find intercept the ground
		*/
		static position3D getGroundInterceptASL(ControllableObject& co, vector3D direction);

		/*!
		Resets all height changes in the current map. 
		Deprecated. Use void applyTerrainHeightsReset
		*/										
		VBS2FUSION_DPR(UTER013) static void resetTerrainHeights();

		/*!
		Returns the grid size of the current Terrain 
		*/
		static float getTerrainGrid();

		/*!
		Returns the grid size of the current Terrain 
		*/
		static double getTerrainGridEx();

		/*!
		Changes a terrain cell height 
		*/
		static void setTerrainHeight(float x, float z, float height);

		/*!
		Changes a terrain cell height 
		Deprecated. Use void applyTerrainHeight(double x, double z, double height)
		*/
		VBS2FUSION_DPR(UTER015) static void setTerrainHeight(double x, double z, double height);

		/*!
		Changes terrain cells height within an area. It can be controlled by setting the optional 
		parameters if the lower or higher cells can be changed. 
		*/
		static void setTerrainHeightArea(position2D pos1, position2D pos2,float height, bool changeLower, bool changeHigher);

		/*!
		Changes terrain cells height within an area. It can be controlled by setting the optional 
		parameters if the lower or higher cells can be changed. 
		Deprecated. Use void applyTerrainHeightArea(position2D pos1, position2D pos2,double height, bool changeLower, bool changeHigher)
		*/
		VBS2FUSION_DPR(UTER017) static void setTerrainHeightArea(position2D pos1, position2D pos2,double height, bool changeLower, bool changeHigher);

		/*!
		Returns array with [min,mid,max] height of the specified area. 
		*/
		static list<float> getTerrainHeightArea(position2D pos1, position2D pos2);

		/*!
		Returns array with [min,mid,max] height of the specified area. 
		*/
		static list<double> getTerrainHeightAreaEx(position2D pos1, position2D pos2);

		/*!
		Flattens a crater created by an explosion.
		It flatten a circle area.
		parameters
		co - This object position is the center of the area.
		radius - It is the radius of the area
		flatten = 0.0 - flatten entirely, 1.0 - keep the same
		*/
		static void setFlattenGround(ControllableObject& co , float radius, float flatten);

		/*!
		Flattens a crater created by an explosion.
		It flatten a circle area.
		parameters
		position - This position is the center of the area.
		radius - It is the radius of the area
		flatten = 0.0 - flatten entirely, 1.0 - keep the same
		*/
		static void setFlattenGround(position3D position, float radius, float flatten);

		/*!
		Flattens a crater created by an explosion.
		It flatten a circle area.
		parameters
		co - This object position is the center of the area.
		radius - It is the radius of the area
		flatten = 0.0 - flatten entirely, 1.0 - keep the same
		Deprecated. Use void applyFlattenGround(ControllableObject& co , double radius, double flatten)
		*/
		VBS2FUSION_DPR(UTER021) static void setFlattenGround(ControllableObject& co , double radius, double flatten);

		/*!
		Flattens a crater created by an explosion.
		It flatten a circle area.
		parameters
		position - This position is the center of the area.
		radius - It is the radius of the area
		flatten = 0.0 - flatten entirely, 1.0 - keep the same
		Deprecated. Use void applyFlattenGround(position3D position, double radius, double flatten)
		*/
		VBS2FUSION_DPR(UTER020) static void setFlattenGround(position3D position, double radius, double flatten);

		/*!
		Returns true if there is a collision detected between the two points. 

		startPos - a position3D parameter giving the position ASL of the start point.
		endPos - a position3D parameter giving the position ASL of the end point.
		type - The type of collision test to perform. Should be one of {FIRE, VIEW, GEOM or IFIRED}. 
		radius - The radius of the cylinder used to perform the collision test. 
		density - The spacing between casted rays. 
		ignoreObject = This object is not considered for collisions. 

		In the case of a detected collision (i.e. return value is true), this function will also store
		the coordinates of the first collision point in the variable firstCollision and the normal of that collision
		in the parameter normal. 

		If a collision is not detected, 'endPos' will be returned as the first collision point.
		Deprecated. Use COLLISION_INFO getCylinderCollision(position3D startPos, position3D endPos, 
							COLLISIONTESTTYPE type, double radius, double density, ControllableObject ignoreObject)
		*/

		VBS2FUSION_DPR(UTER037) static bool cylinderCollision(position3D startPos, 
			position3D endPos,
			position3D& firstCollision, 
			position3D& normal,
			COLLISIONTESTTYPE type,
			double radius,
			double density,
			ControllableObject ignoreObject);

		/*!
		Converts the grid coordinates to in-game position. 

		position - Grid position (6 or more digits)
		Deprecated. Use position3D getGridCenterToPosition(string position)
		*/
		VBS2FUSION_DPR(UTER023) static position3D gridCenterToPosition(string position);

		/*!
		Export map data in into a ESRI ASCII file.
		The files are stored in the user profile's "export" folder (e.g. \My Documents\VBS2\export).
		If the UTM zone for the map is defined (not -1) a .prj file is created as well
		Deprecated. Use bool applyASCIIGridExport (string fileName)
		*/
		VBS2FUSION_DPR(UTER024) static bool exportASCIIGrid(string fileName);

		/*!
		Detects whether an object was part of the terrain (e.g. plants, buildings), 
		or whether it was placed via the editor or via script command. 
		*/
		static bool isMapPlaced(ControllableObject& obj);

#ifdef DEVELOPMENT
		/*!
		Returns accuracy of the current height map representation
		*/
		static double getHeightMapAccuracy();

#endif
		/*!
		Construct height map representation with respect to the given accuracy
		*/
		static double applyHeightMapAccuracy(double accuracy);

		/*!
		Returns whether water is at given position.
		Deprecated. Use bool isWaterOnSurface(position3D pos)
		*/
		VBS2FUSION_DPR(UTER025) static bool surfaceIsWater(position3D pos);

		/*!
		Returns what surface is at the given position.
		*/
		static string getSurfaceType(position3D pos);

		/*!
		Returns roughness on given surface position (Function does not work on paved roads).
		*/
		static double getSurfRoughness(position3D pos);

		/*!
		Returns the current coordinate type and grid resolution as GEOMETRY_COORDINATE_SYSTEM
		*/
		static GEOMETRY_COORDINATE_SYSTEM getGeoCoordSystem();

		/*!
		Returns terrain height above sea level.
		*/
		static double getTerrainHeightASL(position2D pos);

		/*!
		Returns terrain height above sea level.
		*/
		static double getTerrainHeightASL(position3D pos);

		/*!
		Returns surface normal on given position as a vector3D.
		*/
		static vector3D getSurfaceNormal(position2D pos);

		/*!
		Returns surface normal on given position as a vector3D.
		*/
		static vector3D getSurfaceNormal(position3D pos);

		/*!
		Uses a custom bitmap (chart) for the "satellite" terrain texture.
		Bitmap data needs to be stored in the same way satellite data is stored, 
		i.e. there must be some folder containing s_XXX_YYY_lco.paa textures of the user 
		defined map (no other files need to be in that folder). If textures are not specified 
		for some squares (in case textures are only defines for certain sections of the map), 
		a white texture is going to be shown instead. When a user chart is activated, all 
		static map information will be removed, as those should be part of the user texture. 

		Deprecated. Use bool applyUserChart(string chart)
		*/
		VBS2FUSION_DPR(UTER026) static void setUserChart(string chart);

		/*!
		Toggles visibility of map objects (buildings, roads, vegetation, etc.) on user-defined terrain bitmap.
		Deprecated. Use void applyUserChartDrawObjects(bool boolVal)
		*/
		VBS2FUSION_DPR(UTER027) static void setUserChartDrawObjects(bool boolVal);

		/*!
		Sets the desired terrain resolution (in meters). 
		For default landscapes, supported resolutions are:
			50	  - smoothest, less lag
			25	  - default in multiplayer
			12.5  - default in singleplayer
			6.25
			3.125 - bumpiest, higher lag 
		If you select unsupported resolutions, nearest supported value is used instead. 
		Deprecated. Use void applyTerrainGrid(double grid)
		*/
		VBS2FUSION_DPR(UTER028) static void setTerrainGrid(double grid);

		/*!
		Returns the "cone index" of a muddy area. Map must have a specific mud area defined.
		Returns a value between 0 and 255, where 0 is softest. 
		If it is not a mud area, returns -1 
		*/
		static int getConeIndex(position2D pos);

		/*!
		Exports map data in into a ESRI ASCII file.	The files will be stored in the user profile's "export" folder.
		If the UTM zone for the map is defined (not -1) a .prj file will also be created. Here file name needs to 
		be given with the extension (like .asc)
		Deprecated. Use bool applyASCIIGridFileExport(string fileName)
		*/
		VBS2FUSION_DPR(UTER029) static bool exportASCIIGridFile(string fileName);

		/*!
		Returns name of the current user-defined terrain bitmap
		*/
		static string getUserChart();

		/*!
		Returns whether objects are being drawn on user defined chart
		*/
		static bool getUserChartDrawObjects();

		/*!
		Sets the coordinate type and grid resolution for the loaded map (setting are valid until map is reloaded.

		Settings mirror those defined in the CfgWorld's Grid zoom classes.
		Grid setting are applied to the zoom levels in the same order as they are defined in the config.
		If fewer zoom levels are passed in this command than exist in the config, only the ones from this
		command will be used.
		If more are passed, then those will be ignored.

		Grid setting are defined as a GEOMETRY_COORDINATE_SYSTEM with one GRID_FORMAT_ELEMENT per zoom level
		and enum GEOMETRY_COORDINATE_TYPE for different coordinate types. 
		Each GRID_FORMAT_ELEMENT contains 5 elements, whose usage depends on the coordinate system applied.

		MGRS (Military grid reference system) 
			� format    :	Always "XY".
			� formatX/Y :	Number of digits to use for labels. String is a mask, with one "0" per digit.
			� stepX/Y	:	Distance between grid lines in meters. 

		LL (Latitude/Longitude in degrees)
			� format	:	Always empty ("").
			� formatX/Y	:	Digit formatting, following C++ notation: "%.4f" would show 4 decimal digits.
			� stepX/Y	:	Distance between grid lines in degrees.

		LLMS (Latitude/Longitude in minute/seconds)
			� format	:	Always empty ("").
			� formatX/Y	:	Always empty ("").
			� stepX/Y	:	Distance between grid lines in minutes/seconds. 
		*/
		static void applyGeoCoordSystem(GEOMETRY_COORDINATE_SYSTEM gridformat);

		/*!
		Resets all terrain height changes to the map's defaults.
		This command will only temporarily revert changes made by the editor's terrain modification tool, 
		as those edits are saved with the mission itself.
		Deprecated. Use void applyTerrainHeightsWrpReset()
		*/
		VBS2FUSION_DPR(UTER031) static void resetTerrainHeightsWrp();
	
		/*!
		Returns the terrain cell height for coordinate position
		[xCoordinate, zCoordinate]. 
		@param xCoordinate - x coordinate is West-East.
		@param zCoordinate - z coordinate is South-North.
		@return double.
		@remarks This is a replication of TerrainHeight(double xCoordinate, double zCoordinate)
		*/
		static double getTerrainHeight(double xCoordinate, double zCoordinate);

		/*!
		Returns the height above water of the road surface at the given position 
		[xCoordinate, zCoordinate]. 
		@param xCoordinate - x coordinate is West-East.
		@param zCoordinate - z coordinate is South-North.
		@return double.
		@remarks This is a replication of RoadSurfaceHeight(double xCoordinate, double zCoordinate)
		*/
		static double getRoadSurfaceHeight(double xCoordinate, double zCoordinate);

		/*!
		Useful function for determining if a unit or a similar object can be placed at a certain position. 
		Since the geometry collisions are accounted, considered cylinder should not collide with the land. 
		Thus pass a center (position3D) for the cylinder which has a larger height value (Y value of position3D) than the radius.
		@param center - center position.
		@param radius - radius value of cylinder.
		@return bool - This function returns true if a collision is detected within a cylinder of 
					   height = (radius * 2) and radius = radius which is placed upright.
		@remarks This is a replication of uprightCylinderCollision(position3D& center, double radius)
		*/
		static bool isUprightCylinderCollision(position3D& center, double radius);

		/*! 
		Export terrain map to a shape file. 
		@param objectFileName - File name for export objects. 
		@param roadFileName   - File name for export roads.
		@return bool - If export is success then function returns true.
		@remarks This is a replication of exportTerrainMapSHP(string objectFileName, string roadFileName) 
		*/
		static bool applyMapExportToSHP(string objectFileName, string roadFileName);	
		
		/*!
		Converts the an in-game position to a grid string. Returns an 8-digit string. 
		@param pos - The position.
		@return string - converted position.
		@remarks This is a replication of positionToGrid(position3D pos)
		*/
		static string getPositionToGrid(position3D pos);
		
		/*!
		Converts the grid coordinates to in-game position. Returns a position3D coordinate. 
		@param gridPosition - Grid position (6-digits in 1.19 and lower VBS versions, 6 or more digits in 1.20 and higher VBS versions).
				Position must be within map bounds.
		@return position3D.
		@remarks This is a replication of gridtoPosition(string gridPosition)
		*/
		static position3D getGridToPosition(string gridPosition);

		/*!
		Converts the game position to the MGRS coordinates.   
		@param pos -  The position.
		@param precision - Precision specified(Maximum is 5).
		@return string - MGRS formatted position.  
		@remarks This is a replication of positionToMGRS(position3D pos, int precision)
		*/
		static string getPositionToMGRS(position3D pos, int precision);

		/*!
		Converts the game position to the MGRS coordinates with precision 5.   
		@param pos -  The position.
		@return string - MGRS formatted position.  
		@remarks This is a replication of positionToMGRS(position3D pos)
		*/
		static string getPositionToMGRS(position3D pos);

		/*!
		Converts the game position to Latitude with precision specified.
		@param pos - The position.
		@param precision - Precision specified(Maximum is 5).
		@return string - LL formatted position.  
		@remarks This is a replication of positionToLat(position3D pos , int precision)
		*/
		static string getPositionToLat(position3D pos , int precision);

		/*!
		Converts the game position to Latitude with precision 5.
		@param pos - The position.
		@return string - LL formatted position.
		@remarks This is a replication of positionToLat(position3D pos)
		*/
		static string getPositionToLat(position3D pos);

		/*!
		Converts the game position to Longitude with precision specified.
		@param pos - The position.
		@param precision - Precision specified(Maximum is 5).
		@return string - LL formatted position (longitude).  
		@remarks This is a replication of positionToLon(position3D pos, int precision)
		*/										
		static string getPositionToLon(position3D pos, int precision);

		/*!
		Converts the game position to Longitude with precision 5.
		@param pos - The position.
		@return string - LL formatted position (longitude).  
		@remarks This is a replication of positionToLon(position3D pos)
		*/										
		static string getPositionToLon(position3D pos);

		/*!
		Resets all height changes in the current map. 
		@return Nothing.
		@remarks This is a replication of resetTerrainHeights()
		*/										
		static void applyTerrainHeightsReset();

		/*!
		Changes terrain cell's height on position. The map's terrain is collection of cells (tiles).
		@param x - x coordinate is West-East. 
		@param z - z coordinate is South-North.
		@param height - terrain cell height.
		@return Nothing.
		@remarks This is a replication of setTerrainHeight(double x, double z, double height)
		*/
		static void applyTerrainHeight(double x, double z, double height);

		/*!
		Changes terrain cells height within an area. It can be controlled by setting the optional 
		parameters if the lower or higher cells can be changed. 
		@param pos1 - Start position of the terrain cell diagonal.
		@param pos2 - End position of the terrain cell diagonal.
		@param height - Terrain cell height.
		@param changeLower - If only changeLower true terrain cell's height can only increase.				
		@param changeHigher - If only changeHigher true terrain cell's height can only decrease.
				If both changeLower and changeHigher are true terrain cell's height can increase or decrease.
		@return Nothing.
		@remarks This is a replication of setTerrainHeightArea(position2D pos1, position2D pos2,double height, bool changeLower, bool changeHigher)
		*/
		static void applyTerrainHeightArea(position2D pos1, position2D pos2,double height, bool changeLower, bool changeHigher);

		/*!
		Flattens a crater created by an explosion. It flatten a circle area.
		@param position - This position is the center of the area.
		@param radius - It is the radius of the area.
		@param flatten = 0.0 - flatten entirely, 1.0 - keep the same.
		@return Nothing.
		@remarks This is a replication of setFlattenGround(position3D position, double radius, double flatten)
		*/
		static void applyFlattenGround(position3D position, double radius, double flatten);

		/*!
		Flattens a crater created by an explosion.It flatten a circle area.
		@param co - This object position is the center of the area.
		@param radius - It is the radius of the area
		@param flatten = 0.0 - flatten entirely, 1.0 - keep the same
		@return Nothing.
		@remarks This is a replication of setFlattenGround(ControllableObject& co , double radius, double flatten)
		*/
		static void applyFlattenGround(ControllableObject& co , double radius, double flatten);

		/*!
		Converts the grid coordinates to in-game position. 
		@param position - Grid position (6 or more digits).
		@return position3D.
		@remarks This is a replication of gridCenterToPosition(string position)
		*/
		static position3D getGridCenterToPosition(string position);
	
		/*!
		Export map data in into a ESRI ASCII file. If the UTM zone for the map is defined (not -1) a .prj file is created as well.
		The files are stored in the user profile's "export" folder.
		@param fileName - File name (including extension).
		@return bool.
		@remarks This is a replication of exportASCIIGrid(string fileName)
		*/
		static bool applyASCIIGridExport(string fileName);

		/*!
		Returns whether water is at given position.
		@param pos - The position which need to check whether water on surface.
		@return bool.
		@remarks This is a replication of surfaceIsWater(position3D pos)
		*/
		static bool isWaterOnSurface(position3D pos);

		/*!
		@description

		Uses a custom bitmap (chart) for the "satellite" terrain texture.
		Bitmap data needs to be stored in the same way satellite data is stored, 
		i.e. there must be some folder containing s_XXX_YYY_lco.paa textures of the user 
		defined map (no other files need to be in that folder). If textures are not specified 
		for some squares (in case textures are only defines for certain sections of the map), 
		a white texture is going to be shown instead. When a user chart is activated, all 
		static map information will be removed, as those should be part of the user texture. 
		
		@locality

		Locally Applied, Locally Effected

		@version [VBS2Fusion v3.15]

		@param chart - Included path of s_XXX_YYY_lco.paa file.
		
		@return Nothing.

		@example

		@code

		TerrainUtilities::applyUserChart(string("ca\\sara\\data"));

		@endcode

		@overloaded 

		None

		@related

		None

		@remarks This is a replication of setUserChart(string chart)
		*/
		static void applyUserChart(string chart);
		
		/*!
		Toggles visibility of map objects (buildings, roads, vegetation, etc.) on user-defined terrain bitmap.
		@param boolVal - If true then map objects will be drawn.
		@return Nothing.
		@remarks This is a replication of setUserChartDrawObjects(bool boolVal)
		*/
		static void applyUserChartDrawObjects(bool boolVal);

		/*!
		Sets the desired terrain resolution (in meters). 
		@param grid - The grid value.
			For default landscapes, supported resolutions are:
			50	  - smoothest, less lag
			25	  - default in multiplayer
			12.5  - default in singleplayer
			6.25
			3.125 - bumpiest, higher lag 
			If you select unsupported resolutions, nearest supported value is used instead. 
		@return Nothing.
		@remarks This is a replication of setTerrainGrid(double grid)
		*/
		static void applyTerrainGrid(double grid);

		/*!
		Exports map data in into a ESRI ASCII file.	The files will be stored in the user profile's "export" folder.
		If the UTM zone for the map is defined (not -1) a .prj file will also be created. 
		@param fileName - File name needs to be given with the extension (like .asc).
		@return bool.
		@remarks This is a replication of exportASCIIGridFile(string fileName)
		*/
		static bool applyASCIIGridFileExport(string fileName);

		/*!
		Resets all terrain height changes to the map's defaults.
		This command will only temporarily revert changes made by the editor's terrain modification tool, 
		as those edits are saved with the mission itself.
		@param Nothing.
		@return Nothing.
		@remarks This is a replication of resetTerrainHeightsWrp()
		*/
		static void applyTerrainHeightsWrpReset();

		/*!
		Translates a Latitude/ Longitude coordinate into a VBS position.
		@param latitude - LL formatted latitude value in degrees, as decimal number.
		@param longitude - LL formatted longitude value in degrees, as decimal number and followed by a space 
				and the northern or western designator in uppercase	"39.58686 N","40.13539 W".
		@return position3D.
		*/
		static position3D getLLToPosition(string latitude,string longitude);

		/*!
		Translates a Latitude/ Longitude coordinate into a VBS position.LLMS: [latitude,longitude] in degrees`, minutes', seconds", 
		@param latDegree - latitude degrees
		@param latMinutes - latitude minutes
		@param latSecond - latitude seconds
		@param longDegree - longitude degrees
		@param longMinutes - longitude minutes
		@param longSeconds - longitude seconds
		@return position3D.
		*/
		static position3D getLLMSToPosition (double latDegree, double latMinutes, double latSecond, double longDegree, double longMinutes,double longSeconds);

		/*!
		Translates a Latitude/ Longitude coordinate into a VBS position. UTM: [zone,easting,northing]
		@param zone - The zone number, followed by the hemisphere letter (N or S).
		@param easting - X position followed by the axis designator (mE).
		@param northing - Y position followed by the axis designator (mN)(e.g. ["24N","402500mE","4382520mN"])
		@return position3D.
		*/
		static position3D getUTMToPosition(string zone, string easting, string northing);

		/*!
		Translates a Latitude/ Longitude coordinate into a VBS position.UTMB: [zone,easting,northing]
		@param zone - The zone number, followed by the latitide band letter.
		@param easting - X position followed by the axis designator (mE).
		@param northing - Y position followed by the axis designator (mN)(e.g. ["24S","402500mE","4382520mN"])
		@return position3D.
		*/
		static position3D getUTMBToPosition(string zone, string easting, string northing);

		/*!
		Translates a  MGRS coordinate into a VBS position.
		@param MGRSgrid - MGRS formatted position (e.g. "24SVJ0250082520").
		@return position3D.
		*/
		static position3D getMGRSToPosition3D(string MGRSgrid);	

		/*!
		Regenerate lightmaps
		@param Nothing.
		@return Nothing.
		*/
		static void applyLightMapRegenerate();

		/*!
		@description
		Returns COLLISION_INFO struct with collision information between given two positions.

		@locality

		Globally applied
		
		@version [VBS2Fusion v3.10.1]

		@param startPos - A position3D parameter giving the position ASL of the start point.
		@param endPos - A position3D parameter giving the position ASL of the end point.
		@param type - The type of collision test to perform. Should be one of {FIRE, VIEW, GEOM or IFIRED}. 
		@param radius - The radius(meters) of the cylinder used to perform the collision test. (If set to 0, command will only use a single ray)
		@param density - The spacing between casted rays. 
		@param ignoreObject - This object is not considered for collisions. 

		@return COLLISION_INFO - If "colldData" is instance of COLLISION_INFO struct,
				colldData.isCollied bool - In the case of a detected collision this is to be true.
				colldData.normal position3D - The normal of that collision.
				colldData.firstCollision position3D - Coordinates of the first collision point.
				If a collision is not detected, 'endPos' will be returned as the first collision point.
		
		@example

		@code
		
		//Define COLLISION_INFO as a structure
		COLLISION_INFO c;

		//Get Cylinder collision and assign to "c"
		c = TerrainUtilities::getCylinderCollision(player.getPositionASL(),co1.getPositionASL(),FIRE,1,0.5,player);

		//Display the return values
		displayString = "Collied - "+conversions::BoolToString(c.isCollied);
		displayString += "\n Normal - "+c.normal.getVBSPosition();
		displayString +="\nFirst Collision - "+c.firstCollision.getVBSPosition();

		@endcode

		@overloaded 

		@related
		bool isUprightCylinderCollision(position3D& center, double radius)

		@remarks This is a replication of bool cylinderCollision(position3D startPos, position3D endPos, etc..)
		*/
		static COLLISION_INFO getCylinderCollision(position3D startPos, 
			position3D endPos, 
			COLLISIONTESTTYPE type, 
			double radius, 
			double density, 
			ControllableObject ignoreObject);

		/*!
		@description 
		Finds the highest surface area for a specified position.
		Function takes into consideration any buildings, objects, vehicles, etc. that are located at that location.

		@locality

		Globally applied

		@version  [VBS2Fusion v3.15]

		@param pos - position to check.
		@param ignoreObj - object to be ignored when calculating surfaces.
		@param maxHeight - from which maximum height in meters to check (default 20m).
		@param radius - size of radius in meters to check (default 1m).

		@return double - height above ground.

		@example

		@code

		//Assign the unit's position to a position3D variable.
		position3D pos = unit1.getPosition();

		//Assign the return value to a double variable height.
		double height = TerrainUtilities::getMaxHeightAGL(pos,co,20,1);

		//Display the height as a string.
		displayString = "Height : " + conversions::DoubleToString(height);

		@endcode

		@overloaded  

		@related

		@remarks 
		*/
		static double getMaxHeightAGL(position3D pos, ControllableObject& ignoreObj, double maxHeight = 20, double radius = 1);

		/*!
		@description 

		Overlays a shapefile on top of the terrain.
		Shapefiles are visible in first person player mode, as well as in the RTE.
		The file can be located in any subfolder beneath the VBS2 profile directory (but is normally called "Shapes"), e.g. My Documents\VBS2\Shapes.
		Both, the name of the subfolder, and the shape file (including extension) will have to be provided, with a double backslash between the path and the filename (e.g. "Shapes\\shape1.shp").
		Be aware that is is possible to load the same shape twice, and unloading it will only remove the one added last!

		@locality
		
		Locally Applied, Locally Effected

		@version  [VBS2Fusion v3.15]

		@param path - Path to shape file   

		@return int - -1 load when the shape could not be loaded. If the shape file corectly load, it will return the coresponding index currently loaded shape files.
		As a example if there is only one shape file load, index 0 is return.(for the second file index 1 is return).

		@example

		@code
		
		string shape = "Shapes\\Portoobjects_list.shp";
		TerrainUtilities::loadShape(shape);

		@endcode

		@overloaded None

		@related 

		@remarks 
		*/
		static int loadShape(string path);

		/*!
		@description 

		Removes an overlaid shape from the terrain.
		Both, the name of the subfolder, and the shape file (including extension) will have to be provided,
		with a double backslash between the path and the filename (e.g. "Shapes\\shape1.shp").

		@locality
		
		Locally Applied, Locally Effected

		@version  [VBS2Fusion v3.15]

		@param path - Path to shape file   

		@return None

		@example

		@code
		
		string shape = "Shapes\\Saraobjects_list.shp";
		TerrainUtilities::loadShape(shape);
		
		//unload the shape
		TerrainUtilities::loadShape(shape);

		@endcode

		@overloaded None

		@related 

		@remarks 
		*/
		static void unloadShape(string path);

		/*!
		@description 

		Returns the value of a shape property. Currently return the color of the shape.

		@locality
		
		Locally Applied, Locally Effected

		@version  [VBS2Fusion v3.15]

		@param path - Path to shape file   

		@return Color_RGBA - struct of current color value.[r, g, b, a]

		@example

		@code
		
		string shape = "Shapes\\Saraobjects_list.shp";
		TerrainUtilities::loadShape(shape);
		
		
		//define the struct
		Color_RGBA _rgba;
		_rgba=TerrainUtilities::getShapeColor(shapes1);
		
		double r = _rgba.r;
		double g = _rgba.g;
		double b = _rgba.b;
		double a = _rgba.a;
		
		@endcode

		@overloaded None

		@related 

		@remarks 
		*/
		static  Color_RGBA getShapeColor(string path);

		/*!
		@description 

		Defines the property of a shapefile. Set the color of the shapefile.

		@locality
		
		Locally Applied, Locally Effected

		@version  [VBS2Fusion v3.15]

		@param path - Path to shape file 
		@param rgbColor - color property of the shape. ( color formate is like this [r, g, b, a], a is regarding the alpha value)

		@return bool - If true, color property was applied to the shape.

		@example

		@code 
		
		string shape = "Shapes\\Saraobjects_list.shp";
		TerrainUtilities::loadShape(shape);
				
		//define the Color
		Color_RGBA _rgba;
		_rgba.a = 0.5;
		_rgba.b = 0;
		_rgba.g = 1;
		_rgba.r = 0;
		TerrainUtilities::applyShapeColor(shape,_rgba);
		
		@endcode

		@overloaded None

		@related 

		@remarks 
		*/
		static bool applyShapeColor(string path, Color_RGBA rgbColor);

	};
};

#endif //VBS2FUSION_TERRAIN_UTILITIES_HD