
 /**************************************************************************
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 **************************************************************************/


/*************************************************************************

Name:

	TIUtilities.h

Purpose:

	This file contains the declaration of the TIUtilities class.


/************************************************************************/

#ifndef TI_UTILITIES_H
#define TI_UTILITIES_H

//#include <vector>

//#include "position3D.h"
#include "VBS2Fusion.h"
#include "data\ControllableObject.h"


namespace VBS2Fusion
{	
	
	class VBS2FUSION_API TIUtilities
	{
	public:	
		/*!
		Returns the level of blur of the TI device. 
		*/
		static double getBlurLevel();

		/*!
		Returns the brightness of the TI device, in Celsius. 
		*/
		static double getBrightness();

		/*!
		Returns the contrast of the TI device, in Celsius. 
		*/
		static double getContrast();

		/*!
		Returns the mode of the TI device, as index to the TIConversion texture. 
		*/
		static int getMode();

		/*!
		Returns whether the engine uses automatic setting of brightness and contrast in thermal vision. 
		*/
		static bool getAutoBC();

		/*!
		Returns the coefficient the variance in the thermal image histogram is multiplied by. 
		*/
		static double getAutoBCContrastCoefficient();

		/*!
		Sets the level of blur of the TI device.

		Value is from 0 to 1.
		0 is a sharp picture, 1 picture is not clear (grainy/blury)
		*/
		static void applyBlurLevel(double blurLevel);

		/*!
		Sets the brightness of the TI device, in Celsius. 

		Value is from 0 to 80.
		*/
		static void applyBrightness(double brightness);

		/*!
		Sets the contrast of the TI device, in Celsius. 

		Value is from 1 to 40
		*/
		static void applyContrast(double contrast);

		/*!
		Sets the mode of the TI device, as index to the TIConversion texture. 

		Value is from 0 to 63
		*/
		static void applyMode(int mode);

		/*!
		Controls the automatic setting of brightness and contrast in thermal vision.
		*/
		static void applyAutoBC(bool enable);

		/*!
		Sets a coefficient the variance in the thermal image histogram is multiplied by. 

		Value is from 0.1 to 5
		*/
		static void applyAutoBCContrastCoefficient(double coefficient);

		/*!
		Forces TI to be shown instead of night vision, when night vision key is pressed. 
		*/
		static void applyTIOverride(bool enable);

		/*!
		Returns the texture used for face and glasses. 
		*/
		static vector<string> getFaceTextures(ControllableObject& object);

	};
}

#endif//TI_UTILITIES_H