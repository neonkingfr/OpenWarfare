/*
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 */

/*************************************************************************

Name:

TriggerUtilities.h

Purpose:

This file contains the declaration of the TriggerUtilities class.

Version Information:

Version		Date		Author and Comments
===========================================
1.0			30-03/2009	RMR: Original Implementation
1.01		27-08/2009   SDS: Added updateIsLocal(Trigger& trig);	
							 Updated updateTrigger(Trigger& trig)		
1.02		02-08/2009		 Added getIDUsingAlias(Trigger& trig)
							 Updated getPosition(Trigger& trig)
2.0			10-02/2010	MHA: Comments Checked
/************************************************************************/

#ifndef VBS2FUSION_TRIGGER_UTILITIES_H
#define VBS2FUSION_TRIGGER_UTILITIES_H

#include <sstream>
#include <string>
#include <list>

#include "position3D.h"
#include "VBS2Fusion.h"
#include "data/Trigger.h"
#include "util/ExecutionUtilities.h"

namespace VBS2Fusion
{

	class VBS2FUSION_API TriggerUtilities
	{
	public:

		//-------------------------------------------------------------------

		/*!
		Returns the position of the trigger in position3D format.

		This function uses the network ID of the object to access it within 
		the game environment, so please ensure that either a registered alias 
		or a string accessible name is assigned to trigger. 

		Error checking utility validates the following:

		- NetworkID (outputs an error if the network ID is invalid)
		- Position (outputs an error if the position returned is invalid)
		*/

		static position3D getPosition(Trigger& trig);		

		/*!
		Applies the position obtained position to the trigger. 

		This function uses the network ID of the object to access it within 
		the game environment, so please ensure that either a registered alias 
		or a string accessible name is assigned to trigger. 

		Error checking utility validates the following:

		- NetworkID (outputs an error if the network ID is invalid)

		This method get parameter from Fusion then apply it in VBS2 side
		*/

		static void applyPosition(Trigger& trig, position3D position);

		/*!
		Applies the position obtained trig.getPosition() to the trigger. 
		*/

		//static void applyPosition(Trigger& trig);

		/*!
		Update the trigger position. 
		This method get the parameter from VBS2 side and update the Fusion parameter
		*/

		static void updatePosition(Trigger& trig);

		//-------------------------------------------------------------------

		/*!
		Returns the trigger statements. 

		This function uses the network ID of the object to access it within 
		the game environment, so please	ensure that either a registered alias
		or a string accessible name is assigned to trigger. 

		Error checking utility validates the following:

		- NetworkID (outputs an error if the network ID is invalid)			
		*/
		static string getStatements(Trigger& trig);		

		/*!
		Applies the statements obtained through 
		trig.getStatementsInVBSFormat() to the trigger.

		This function uses the network ID of the object to access it within 
		the game environment, so please	ensure that either a registered alias
		or a string accessible name is assigned to trigger. 

		Error checking utility validates the following:

		- NetworkID (outputs an error if the network ID is invalid)

		This method get parameter from Fusion then apply it in VBS2 side

		Need to use following Trigger fuction before use this function
		//trig is a Trigger variable
		trig.setStatementsCond("this");
		trig.setStatementsActiv("hint 'trigger on'");
		trig.setStatementsDesactiv("hint 'trigger off'");
		*/

		static void applyStatements(Trigger& trig);	

		/*!
		Updates all trigger statements for trig. 

		This function uses the network ID of the object to access it within 
		the game environment, so please	ensure that either a registered alias
		or a string accessible name is assigned to trigger. 

		Error checking utility validates the following:

		- NetworkID (outputs an error if the network ID is invalid)	

		This method get the parameter from VBS2 side and update the Fusion parameter
		*/
		static void updateStatements(Trigger& trig);	

		//-------------------------------------------------------------------

		/*!
		Returns the activation statements for the trigger. 

		This function uses the network ID of the object to access it within 
		the game environment, so please	ensure that either a registered alias
		or a string accessible name is assigned to trigger. 

		Error checking utility validates the following:

		- NetworkID (outputs an error if the network ID is invalid)		
		*/
		static string getActivation(Trigger& trig);

		/*!
		Applies the activation statements obtained through 
		trig.getTriggerActivationInVBSFormat(); to the trigger. 

		This function uses the network ID of the object to access it within 
		the game environment, so please	ensure that either a registered alias
		or a string accessible name is assigned to trigger. 

		Error checking utility validates the following:

		- NetworkID (outputs an error if the network ID is invalid)	

		This is apply changes the following parameters in VBS2 from Fusion 
		by: String - Who activates trigger. Can be "NONE" or

		Side': "EAST", "WEST", "GUER", "CIV", "LOGIC", "ANY"
		Radio: "ALPHA", "BRAVO", "CHARLIE", "DELTA", "ECHO", "FOXTROT", "GOLF", "HOTEL", "INDIA", "JULIET"
		Object: "STATIC", "VEHICLE", "GROUP", "LEADER", "MEMBER"
		Status: "WEST SEIZED", "EAST SEIZED" or "GUER SEIZED"

		type: String - How trigger is it activated. Can be:

		Presence: "PRESENT", "NOT PRESENT"
		Detection: "WEST D", "EAST D", "GUER D" or "CIV D"

		repeating: Boolean - Activation can happen repeatedly

		The following functions are used to edit trigger parameters
		trig.setTriggerActivation();
		trig.setTriggerPresence(string presence);
		trig.setRepeating(bool repeating);

		This method get parameter from Fusion then apply it in VBS2 side
		*/		
		static void applyActivation(Trigger& trig);

		/*!
		updates the triggers activation statements. 

		This function uses the network ID of the object to access it within 
		the game environment, so please	ensure that either a registered alias
		or a string accessible name is assigned to trigger. 

		Error checking utility validates the following:

		- NetworkID (outputs an error if the network ID is invalid)		

		This method get the parameter from VBS2 side and update the Fusion parameter
		*/
		static void updateActivation(Trigger& trig);

		//-------------------------------------------------------------------

		/*!
		Returns the triggers area statements. 

		This function uses the network ID of the object to access it within 
		the game environment, so please	ensure that either a registered alias
		or a string accessible name is assigned to trigger. 

		Error checking utility validates the following:

		- NetworkID (outputs an error if the network ID is invalid)		
		*/
		static string getArea(Trigger& trig);

		/*!
		Applies the area statements obtained through 
		trig.getTriggerAreaInVBSFormat() to the trigger. 

		This function uses the network ID of the object to access it within 
		the game environment, so please	ensure that either a registered alias
		or a string accessible name is assigned to trigger. 

		Error checking utility validates the following:

		- NetworkID (outputs an error if the network ID is invalid)	
		*/		
		static void applyArea(Trigger& trig);

		/*!
		Updates the triggers area values. 

		This function uses the network ID of the object to access it within 
		the game environment, so please	ensure that either a registered alias
		or a string accessible name is assigned to trigger. 

		Error checking utility validates the following:

		- NetworkID (outputs an error if the network ID is invalid)	

		This method get the parameter from VBS2 side and update the Fusion parameter
		*/
		static void updateArea(Trigger& trig);
		//-------------------------------------------------------------------

		/*!
		Returns the triggers timeout statements. 

		This function uses the network ID of the object to access it within 
		the game environment, so please	ensure that either a registered alias
		or a string accessible name is assigned to trigger. 

		Error checking utility validates the following:

		- NetworkID (outputs an error if the network ID is invalid)
		trig.setTimeoutMax(double val);
		trig.setTimeoutMid(double val);
		trig.setTimeoutMin(double val);
		*/		
		static string getTimeout(Trigger& trig);

		/*!
		Applies the timeout statements obtained through 
		trig.getTriggerTimeoutInVBSFormat() to the trigger. 

		This function uses the network ID of the object to access it within 
		the game environment, so please	ensure that either a registered alias
		or a string accessible name is assigned to trigger. 

		Error checking utility validates the following:

		- NetworkID (outputs an error if the network ID is invalid)	

		trig.setTimeoutMax(double val);
		trig.setTimeoutMid(double val);
		trig.setTimeoutMin(double val);

		This method get parameter from Fusion then apply it in VBS2 side
		*/
		static void applyTimeout(Trigger& trig);

		/*!
		This function uses the network ID of the object to access it within 
		the game environment, so please	ensure that either a registered alias
		or a string accessible name is assigned to trigger. 

		Error checking utility validates the following:

		- NetworkID (outputs an error if the network ID is invalid)	

		This method get the parameter from VBS2 side and update the Fusion parameter
		*/
		static void updateTimeout(Trigger& trig);

		//-------------------------------------------------------------------

		/*!
		Returns the triggers text. 

		This function uses the network ID of the object to access it within 
		the game environment, so please	ensure that either a registered alias
		or a string accessible name is assigned to trigger. 

		Error checking utility validates the following:

		- NetworkID (outputs an error if the network ID is invalid)	
		*/
		static string getText(Trigger& trig);

		/*!
		Applies the text obtained through trig.getText() to the trigger. 
		
		trig.setText(string text);

		This method get parameter from Fusion then apply it in VBS2 side
		*/		
		static void applyText(Trigger& trig);

		/*!
		Applies the text obtained through text to the trigger. 

		This function uses the network ID of the object to access it within 
		the game environment, so please	ensure that either a registered alias
		or a string accessible name is assigned to trigger. 

		Error checking utility validates the following:

		- NetworkID (outputs an error if the network ID is invalid)	

		This method get parameter from Fusion then apply it in VBS2 side
		*/
		static void applyText(Trigger& trig, string text);

		/*!
		Updates the triggers text. 
		This method get the parameter from VBS2 side and update the Fusion parameter
		*/
		static void updateText(Trigger& trig);

		/*!
		Applies the alias of the trigger object if the applied alias has not 
		already been assigned to another object in the mission.
		*/
		static void applyAlias(Trigger& trig);

		/*!
		Returns the name of the variable which contains a primary editor reference to this trigger object.
		*/
		static string getVarName(Trigger& trig);

		/*!
		Returns the triggers type. 

		This function uses the network ID of the object to access it within 
		the game environment, so please	ensure that either a registered alias
		or a string accessible name is assigned to trigger. 

		Error checking utility validates the following:

		*/
		static string getType(Trigger& trig);

		/*!
		Applies the type obtained from type to the trigger. 

		This function uses the network ID of the object to access it within 
		the game environment, so please	ensure that either a registered alias
		or a string accessible name is assigned to trigger. 

		Error checking utility validates the following:

		This method get parameter from Fusion then apply it in VBS2 side

		*/
		static void applyType(Trigger& trig, string type);

		/*!
		Applies the type obtained from trig.getTriggerType() to the trigger. 

		trig.setTriggerType(TRIGGERTYPE	type);

		This method get parameter from Fusion then apply it in VBS2 side
		*/		
		static void applyType(Trigger& trig);

		/*!
		Updates the triggers type.
		This method get the parameter from VBS2 side and update the Fusion parameter
		*/
		static void updateType(Trigger& trig);

		//-------------------------------------------------------------------

		/*!
		Updates the activation status boolean of the trigger. 

		This function uses the network ID of the object to access it within 
		the game environment, so please	ensure that either a registered alias
		or a string accessible name is assigned to trigger. 

		Error checking utility validates the following:

		- NetworkID (outputs an error if the network ID is invalid)		

		This method get the parameter from VBS2 side and update the Fusion parameter
		*/
		static void updateActivated(Trigger& trig);

		//-------------------------------------------------------------------

		/*!
		Compound activation function for the triggers. Updates the following:

		- Activation Statements
		- Area
		- Position
		- Statements
		- Text
		- Timeout values
		- Activation

		This method get the parameter from VBS2 side and update the Fusion parameter
		*/
		static void updateTrigger(Trigger& trig);

		/*!
		Applies all values within the following to the trigger:

		- Activation Statements
		- Area
		- Position
		- Statements
		- Text
		- Timeout values
		- Activation

		This method get parameter from Fusion then apply it in VBS2 side
		*/

		static void applyChanges(Trigger& trig);		

		/*!
		Returns the network ID of an object using its alias. 

		Error checking utility validates the following:

		- Alias (outputs an error if the alias returned is invalid)
		*/
		//static string getIDUsingAlias(Trigger& trig);

		//-------------------------------------------------------
		// NetworkId utility
		//-------------------------------------------------------

		/*!
		Update "isLocal" property in co to know whether co has valid
		network id or not	

		*/
		static void updateIsLocal(Trigger& trig);	
		
		/*!
		update the network ID of an object using its alias. 

		Error checking utility validates the following:

		- NetworkID (outputs an error if the id returned is invalid)

		This method get the parameter from VBS2 side and update the Fusion parameter
		*/

		static void updateTriggerID(Trigger& trig);

		//-------------------------------------------------------------------	

		/*!
		Remove the trigger.
		*/
		static void deleteTrigger(Trigger& trig);

		//-------------------------------------------------------------------	

		/*!
		Creates a trigger as specified by the input trigger object.
		*/
		static void createTrigger(Trigger& trig);
	
		/*!
		Sets optional script that is executed when the trigger activates. 
		Deprecated. Use double applyOnActivationStatement(Trigger& trigger, string command)
		*/
		VBS2FUSION_DPR(UTGR001) static double addTriggerOnActivated(Trigger& trigger, string command);

		/*!
		The trigger is created locally.
		*/
		static Trigger createTriggerLocal(string type, position3D position);

		//static void removeTriggerOnActivated(Trigger trig, double index);

		/*!
		Defines the trigger activation type. See Mission Editor - Triggers for a thorough overview 
		of triggers and its fields for activation, effects, etc.

		by: String - Who activates trigger. Can be "NONE" or

		Side': "EAST", "WEST", "GUER", "CIV", "LOGIC", "ANY"
		Radio: "ALPHA", "BRAVO", "CHARLIE", "DELTA", "ECHO", "FOXTROT", "GOLF", "HOTEL", "INDIA", "JULIET"
		Object: "STATIC", "VEHICLE", "GROUP", "LEADER", "MEMBER"
		Status: "WEST SEIZED", "EAST SEIZED" or "GUER SEIZED"

		type: String - How trigger is it activated. Can be:

		Presence: "PRESENT", "NOT PRESENT"
		Detection: "WEST D", "EAST D", "GUER D" or "CIV D"

		repeating: Boolean - Activation can happen repeatedly

		Deprecated. Use void applyActivationType(Trigger& trig, string& by, string& type, bool repeat) 
		*/
		VBS2FUSION_DPR(UTGR003) static void setTriggerActivation(Trigger& trig, string by, string type, bool repeat);

		/*!
		Returns all objects attached to the trigger
		Deprecated. Use list<ControllableObject> getAttachedObjects(Trigger& trig)
		*/
		VBS2FUSION_DPR(UTGR004) static list<ControllableObject> triggerAttachedObj(Trigger& trig);

		/*!
		Detaches a game object from a trigger.
		Deprecated. Use applyObjectDetach(Trigger& trig, ControllableObject& co)
		*/
		VBS2FUSION_DPR(UTGR006) static void triggerDetachObj(Trigger& trig, ControllableObject& obj);

		/*!
		Assigns any game object to a trigger. 
		Deprecated. Use void applyObjectAttach(Trigger& trig, ControllableObject& co)
		*/
		VBS2FUSION_DPR(UTGR005) static void triggerAttachObj(Trigger& trig, ControllableObject& co);

#ifdef DEVELOPMENT
		
		/*!
		Assigns a static object to the trigger. The activation source is changed to "STATIC". 
		*/
		static void triggerAttachObject(Trigger& trig, float objectId);
#endif

		/*!
		Synchronizes a trigger with other list of waypoints.
		Deprecated. Use void applySynchronization(Trigger& trig, list<Waypoint> wplist)
		*/
		VBS2FUSION_DPR(UTGR007) static void synchronizeTrigger(Trigger& trig, list<Waypoint> wplist);

		/*!
		Returns the statement that is executed when a trigger or way point is reached. 
		*/
		static string getEffectCondition(Trigger& trig);

		/*!
		Assign the trigger that is defined by the passed network id.
			trig - Trigger reference
			id - Network ID

		Note : Can assign the network id to trigger data object by setID(NetworkID nid) then
			use the update function in TriggerUtilities.
		*/
		VBS2FUSION_DPR(UTGR012) static void networkIdToTrigger(Trigger& trig, NetworkID id);

		/*!
		Assign the trigger that is defined by the passed network id.
			trig - Trigger reference
			id - Network ID in string format ("2:1:3")

		Note : Can assign the network id to trigger data object by setID(string triggerID) then
			use the update function in TriggerUtilities.
		*/
		VBS2FUSION_DPR(UTGR012) static void stringIdToTrigger(Trigger& trig, string& id);

		/*!
		The statement is executed when the trigger is activated and the effects are launched
		depending on the result.
			- If the result is a boolean and true, the effect is launched.
			- If the result is an object, the effect is launched if the result is the player or
			  the player vehicle.
			- If the result is an array, the effect is launched if the result contains the player
			  or the player vehicle. 

	    Deprecated. Use void applyEffectCondition(Trigger& trig, string& statement)
		*/
		VBS2FUSION_DPR(UTGR011) static void setEffectCondition(Trigger& trig, string& statement);

		/*!
		Defines the music track played on activation. Track is a subclass name of CfgMusic. 
		In addition, "$NONE$" (no change) or "$STOP$" (stops the current music track). 
		Deprecated. Use void applyMusicEffect(Trigger& trig, string& trackName)
		*/
		VBS2FUSION_DPR(UTGR008) static void setMusicEffect(Trigger& trig, string& trackName);

		/*!
		Returns the defined music track played on activation on a trigger.
		Track is a subclass name of CfgMusic or "$NONE$" or "$STOP$".  		
		*/
		static string getMusicEffect(Trigger& trig);

		/*!
		Defines the different sound effects.
		Sound / voice plays a 2D / 3D sound from CfgSounds.
		SoundEnv plays an enviromental sound from CfgEnvSounds.
		SoundDet (only for triggers) creates a dynamic sound object attached to a trigger defined in CfgSFX. 
		Deprecated. Use void applySoundEffect(Trigger& trig, string& sound, string& voice, string& soundEnv, string& soundDet)
		*/
		VBS2FUSION_DPR(UTGR009) static void setSoundEffect(Trigger& trig, string& sound, string& voice, string& soundEnv, string& soundDet);
	
		/*!
		Returns the sound, voice, soundEnv, and soundDet for a trigger. 
		Return value : vector of string which contains sound, voice, soundEnv and soundDet
		*/
		static vector<string> getSoundEffect(Trigger& trig);

		/*!
		Defines the title effect via [Type, Effect, Text] where

		'Type' can be,
			- "NONE",
			- "OBJECT",
				'Text' defines the shown object , a subclass of CfgTitles. 
			- "RES"
				'Text' defines a resource class, a subclass of RscTitles. 
			- "TEXT"
				The 'Text' is shown as text itself.
				
		'Effect' defines a subtype: 
			"PLAIN", "PLAIN DOWN", "BLACK", "BLACK FADED", "BLACK OUT", 
			"BLACK IN", "WHITE OUT" or "WHITE IN". 

		Deprecated. Use void applyTitleEffect(Trigger& trig, TITLE_EFFECT_CLASS classType, TITLE_EFFECT_TYPE effect, string& text)
		*/
		VBS2FUSION_DPR(UTGR010) static void setTitleEffect(Trigger& trig, string& type, string& effect, string& text);

		/*!
		Returns the defined title effect for a trigger.
		Return :  vector of string which contains type, effect and text

		'type' can be "NONE", "OBJECT", "RES" or "TEXT".
		'effect' returns the selected Title Effect Type for type "TEXT".
		'text' returns the shown object (a subclass of CfgTitles) for type "OBJECT", the resource
		class (a subclass of RscTitles) for type "RES", and the plain text string for type "TEXT"
		*/
		static vector<string> getTitleEffect(Trigger& trig);

		/*!
		List of units that would activate given Trigger.
		For trigger of type "Not present" the list is the same as that returned for "present".
		*/
		static vector<Unit> getListOfUnits(Trigger& trig);

		/*!
		Clears optional script that is executed when the trigger activates.
		Deprecated. Use void applyOnActivationStatementRemoval(Trigger& trigger, double index)
		*/
		VBS2FUSION_DPR(UTGR002) static void removeTriggerOnActivated(Trigger& trigger, double index);

		/*!
		Specifies the entity which will activate the selected trigger.
		Deprecated. Use void applyVehicleAttach(Trigger& trig, ControllableObject& co)
		*/
		VBS2FUSION_DPR(UTGR014) static void triggerAttachVehicle(Trigger& trig, ControllableObject& co);

		/*!
		Apply vbs2 script command that is executed when the trigger activates.
		
		@param trigger - Trigger that vbs2 script command is applied to.
		@param command - vbs2 script that is executed when the trigger activates. 

		@return double - EventHandler ID which can be used on applyOnActivationStatementRemoval(Trigger& trigger, double index)
			function.

		@remarks This is a replication of double addTriggerOnActivated(Trigger& trigger, string command) function.		 
		*/
		static double applyOnActivationStatement(Trigger& trigger, string command);

		/*!
		Clears vbs2 script, executed when the trigger activates which is added by applyOnActivationStatement(Trigger& trigger, string& command) function.
		
		@param trigger - Trigger that vbs2 script is removed from.
		@param index - Index (EventHandler ID) of the vbs2 script. This is the return value of applyOnActivationStatement function.

		@return Nothing.

		@remarks This is a replication of void removeTriggerOnActivated(Trigger& trigger, double index) function.	
		*/
		static void applyOnActivationStatementRemoval(Trigger& trigger, double index);

		/*!
		Defines the trigger activation type. 

		@param trigger - Trigger that activation type is applied to.
		@param by - Who activates trigger.
			It can be "NONE" or
			Side': "EAST", "WEST", "GUER", "CIV", "LOGIC", "ANY"
			Radio: "ALPHA", "BRAVO", "CHARLIE", "DELTA", "ECHO", "FOXTROT", "GOLF", "HOTEL", "INDIA", "JULIET"
			Object: "STATIC", "VEHICLE", "GROUP", "LEADER", "MEMBER"
			Status: "WEST SEIZED", "EAST SEIZED" or "GUER SEIZED"
		@param type - How trigger is it activated. It can be;
			Presence: "PRESENT", "NOT PRESENT"
			Detection: "WEST D", "EAST D", "GUER D" or "CIV D"
		@param repeating - Activation can happen repeatedly.

		@return Nothing.

		@remarks This is a replication of void setTriggerActivation(Trigger& trig, string by, string type, bool repeat) function. 
		*/
		static void applyActivationType(Trigger& trig, string& by, string& type, bool repeat);

		/*!
		Returns all objects attached to the trigger.

		@param trig - Trigger object.

		@return list<ControllableObject> - List of ControllableObjects which are attached to the trigger.

		@remarks This is a replication of list<ControllableObject> triggerAttachedObj(Trigger& trig) function. 
		*/
		static list<ControllableObject> getAttachedObjects(Trigger& trig);

		/*!
		Assigns an object to a trigger.

		@param trig - Trigger that object is assigned.
		@param co - ControllableObject to be attached.

		@return Nothing.

		@remarks This is a replication of void triggerAttachObj(Trigger& trig, ControllableObject& co) function. 
		*/
		static void applyObjectAttach(Trigger& trig, ControllableObject& co);

		/*!
		Detaches an object from a trigger.

		@param trig - Trigger that object is detached from.
		@param co - ControllableObject to be detached.

		@return Nothing.

		@remarks This is a replication of void triggerDetachObj(Trigger& trig, ControllableObject& co) function. 
		*/
		static void applyObjectDetach(Trigger& trig, ControllableObject& co);

		/*!
		Synchronizes a trigger with other list of waypoints. To remove the synchronization empty waypoints list can be passed 
		into the function. 

		@param trig - Trigger object to be synchronized with waypoins.
		@param wplist - List of waypoints.

		@return Nothing.

		@remarks This is a replication of void synchronizeTrigger(Trigger& trig, list<Waypoint> wplist) function.
		*/
		static void applySynchronization(Trigger& trig, list<Waypoint> wplist);

		/*!
		Defines the music track played on activation.  
		
		@param trig - Trigger that music track is defined to.
		@param trackName - Name of the music track played on activation. Track is a subclass name of CfgMusic.
			In addition, "$NONE$" (no change) or "$STOP$" (stops the current music track). 

		@return Nothing.

		@remarks This is a replication of void setMusicEffect(Trigger& trig, string& trackName) function.
		*/
		static void applyMusicEffect(Trigger& trig, string& trackName);

		/*!
		Defines the different sound effects.
		
		@param trig - Trigger that sound effects are defined to.
		@param sound - Name of the 2D / 3D sound from CfgSounds.
		@param voice - Name of the 2D / 3D sound from CfgSounds.
		@param soundEnv-  Name of the environmental sound from CfgEnvSounds.
		@param soundDet - Creates a dynamic sound object attached to a trigger defined in CfgSFX. 

		@return Nothing.

		@remarks This is a replication of void setSoundEffect(Trigger& trig, string& sound, string& voice, string& soundEnv, string& soundDet) function.
		*/
		static void applySoundEffect(Trigger& trig, string& sound, string& voice, string& soundEnv, string& soundDet);

		/*!
		Defines the title effect.

		@param trig - Trigger that title effect is defined to.
		@param classType - Type defines in TITLE_EFFECT_CLASS enum and it can be,
		- TEC_NONE - "NONE",
		- TEC_OBJECT - "OBJECT" : 'Text' defines the shown object , a subclass of CfgTitles. 
		- TEC_RES - "RES" : 'Text' defines a resource class, a subclass of RscTitles. 
		- TEC_TEXT - "TEXT" : The 'Text' is shown as text itself.		
		@param effect - Effect defines in TITLE_EFFECT_TYPE enum.
		@param text - Text to be shown. 

		@return Nothing.

		@remarks This is a replication of void setTitleEffect(Trigger& trig, string& type, string& effect, string& text) function.
		*/
		static void applyTitleEffect(Trigger& trig, TITLE_EFFECT_CLASS classType, TITLE_EFFECT_TYPE effect, string& text);

		/*!
		The vbs2 script statement is executed when the trigger is activated and the effects are launched depending on the result of the statement.
			- If the result is a boolean and true, the effect is launched.
			- If the result is an object, the effect is launched if the result is the player or
			  the player vehicle.
			- If the result is an array, the effect is launched if the result contains the player
			  or the player vehicle.

		@param trig - Trigger object.
		@param statement - vbs2 script statement that is executed when the trigger is activated.
		
		@return Nothing.

		@remarks This is a replication of void setEffectCondition(Trigger& trig, string& statement) function.
		*/
		static void applyEffectCondition(Trigger& trig, string& statement);

		/*!
		Specifies the entity which will activate the selected trigger.

		@param trig - Trigger that is activated by given object.
		@param co - The ControllableObject which will activate the trigger.
			The trigger is coupled to the given object or its group. When the source is "GROUP", "LEADER" or "MEMBER",
			it's coupled to the group, otherwise it's coupled to the object and the source is changed to "VEHICLE". 
		
		@return Nothing.

		@remarks This is a replication of void triggerAttachVehicle(Trigger& trig, ControllableObject& co) function.
			Trigger activation type can be get by using string getActivation(Trigger& trig) function.
		*/
		static void applyVehicleAttach(Trigger& trig, ControllableObject& co);

		/*!
		@description 
		Gets the list of waypoints or triggers the specified trigger is synchronized with.
		
		@locality

		Locally applied

		@version  [VBS2Fusion v3.11]

		@param trig - Trigger object.
		
		@return list<Waypoint> - list of synchronized waypoints with passed trigger.

		@example

		@code

		//Apply synchronization
		TriggerUtilities::applySynchronization(trig1,listWp);

		//Update the trigger
		TriggerUtilities::updateTrigger(trig1);

		//Returns the Waypoint typed list which the particular trigger has synchronized with
		list<Waypoint> synWpList = TriggerUtilities::getSynchronizedWaypoints(trig1);

		//Prints the element names of the returned list
		for (list<Waypoint>::iterator it = synWpList.begin(); it != synWpList.end(); it++){
		displayString += it->getName()+"\\n";
		}

		@endcode

		@overloaded None.

		@related
		void TriggerUtilities::applySynchronization(Trigger& trig, list<Waypoint> wplist)

		@remarks 	
		*/
		static list<Waypoint> getSynchronizedWaypoints(Trigger& trig);
	};

};

#endif //TRIGGER_UTILITIES_H