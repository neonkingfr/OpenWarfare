 /**************************************************************************
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 **************************************************************************/

/*****************************************************************************

Name:	RTEUtilities.h

Purpose: This file contains VBS2 Real Time Editor (RTE) related Utility function definitions. 

/*****************************************************************************/
#ifndef VBS2FUSION_RTEUTILITIES_H
#define VBS2FUSION_RTEUTILITIES_H

/*****************************************************************************/
/* INCLUDES
/*****************************************************************************/
// STANDARD INCLUDES
#include <vector>

// SIMCENTRIC INCLUDES
#include "VBS2Fusion.h"
#include "position3D.h"
#include "position2D.h"
#include "VBS2FusionDefinitions.h"

/*****************************************************************************/
/* END INCLUDES
/*****************************************************************************/

namespace VBS2Fusion
{
	class VBS2FUSION_API RTEUtilities
	{
	public:

		/*!
		While in Real Time Editor (RTE) mode, converts the given screen position
		to world (3D) position.
		Screen position in in form of [x,y] where 'x' is from top-left to top-right 
		and 'y' is from top-left to bottom-left of VBS2 screen. Range of 'x' and 'y'
		on VBS2 screen is 0 to 1.
		If not in RTE mode, a null position3D value will be returned.

		Deprecated. Use position3D getPositionScreenToWorld(position2D screenPos) instead.
		*/
		VBS2FUSION_DPR(URTE001) static position3D positionScreenToWorld(position2D screenPos);
		
		/*!
		While in Real Time Editor (RTE) mode, converts the given world (3D) position
		to screen position.
		Screen position in in form of [x,y] where 'x' is from top-left to top-right 
		and 'y' is from top-left to bottom-left of VBS2 screen. Range of 'x' and 'y'
		on VBS2 screen is 0 to 1.
		If not in RTE mode, a null position2D value will be returned.

		Deprecated. Use position2D getPositionWorldToScreen(position3D worldPos) instead.
		*/
		VBS2FUSION_DPR(URTE002) static position2D positionWorldToScreen(position3D worldPos);

		/*!
		Move (UI) mouse pointer to specified position of the screen. Values of x and y can be in the range from 0 to 1. 
		*/
		static void applyMousePosition(float x, float y);

		/*!
		Sets view options for editor map. 

		type: Can be:
		"contour" - Toggles elevation lines. (Applied to editor map and user maps.)
		"grid" - Toggles grid. (Applied to editor map and user maps.)
		"hit" - Toggles bullet paths. (Only applies to AAR).
		"trail" - Toggles movement trails. If used with 3rd argument, only applies setting to that object. 
		*/
		static void applyViewParameter(VIEW_TYPE type, bool val);

		/*!
		Sets view options for editor map. 
		*/
		static void applyViewParameterTrail(bool val, ControllableObject& co);

		/*!
		Get map view parameter, as set by applyViewParameter()
		*/
		static bool getViewParameter(VIEW_TYPE type);

		/*!
		Get map view parameter for trail, as set by applyViewParameterTrail()
		*/
		static bool getViewParameterTrail(ControllableObject& co);

		/*!
		Sets view options for editor map for texture.
		control - use "map" for 'Control' map

		Toggles texture image. true hides the texture, false displays it. (Is applied to the map control passed as 3rd argument.)
		*/
		static void applyViewParameterTexture(bool val, string control);

		/*!
		Get map view parameter texture, , as set by applyViewParameterTexture()
		control - use "map" for 'Control' map
		*/
		static bool getViewParameterTexture(string control);
		
		/*!
		@description

		While in Real Time Editor (RTE) mode, converts the given screen position
		to world (3D) position.
		Screen position in in form of [x,y] where 'x' is from top-left to top-right 
		and 'y' is from top-left to bottom-left of VBS2 screen. Range of 'x' and 'y'
		on VBS2 screen is 0 to 1.
		If not in RTE mode, a null position3D value will be returned.

		@locality

		Locally applied, locally effected.

		@version [VBS2Fusion v3.15]

		@param screenPos - Position of the screen in the position2D format

		@return position3D - World position according to the given screen position

		@example

		@code

		//convert the given screen position to world (3D) position.
		position3D pos = RTEUtilities::getPositionScreenToWorld(position2D(2700,2200));

		//Display the position as a string.
		displayString = "PositionScreenToWorld : " + pos.getVBSPosition();

		@endcode

		@overloaded 

		@related

		@remarks 

		*/
		static position3D getPositionScreenToWorld(position2D screenPos);

		/*!
		@description

		While in Real Time Editor (RTE) mode, converts the given world (3D) position
		to screen position.
		Screen position in in form of [x,y] where 'x' is from top-left to top-right 
		and 'y' is from top-left to bottom-left of VBS2 screen. Range of 'x' and 'y'
		on VBS2 screen is 0 to 1.
		If not in RTE mode, a null position2D value will be returned.

		@locality

		Locally applied, locally effected.

		@version [VBS2Fusion v3.15]

		@param worldPos - World position in the Position3D format

		@return position2D - Screen position according to the given world position

		@example

		@code

		//converts the given world (3D) position to screen position.
		position2D pos = RTEUtilities::getPositionWorldToScreen(position3D(600,400,50));

		//Display the position as a string.
		displayString = "PositionWorldToScreen : " + pos.getVBSPosition2D();

		@endcode

		@overloaded 

		@related

		@remarks 

		*/
		static position2D getPositionWorldToScreen(position3D worldPos);


	};

};

#endif	//VBS2FUSION_RTEUTILITIES_H