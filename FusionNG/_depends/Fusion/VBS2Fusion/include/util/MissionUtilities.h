/*
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 */

/*************************************************************************

Name:

	MissionUtilities.h

Purpose:

	This file contains the declaration of the MissionUtilities class.

Version Information:

	Version		Date		Author and Comments
	===========================================
	2.0			01-04/2009	RMR: Original Implementation
	2.01		10-02/2010	MHA: Comments Checked
	2.02		10-01/2011  CGS: Added pauseSimulation() 
									isPaused()
									resumeSimulation()
									hostMission(string)
									endMission(string)
									playMission(string, string, bool)
    2.03	    04-03/2011  YFP: Added Methods,
									captureStart(string)
									captureStart(string,double, double,int,list<string>,bool,bool,double);
									captureStop()
									captureTest()
	2.04		21-06/2011	SSD: Added Methods;
									bool IsMapShown()
									bool IsWatchShown()
									bool IsCompassShown()
									bool IsWalkieTalkieShown()
									bool IsNotepadShown()
									bool IsGPSShown()
									void loadMissionName(Mission& mission)
									string getMissionName()
									vector<int> MissionStartTime()
	2.05		09-09/2011	NDB: Added Methods,
									bool isViewClient()
									float getWeaponSwayFactor()
									void applyWeaponSwayFactor(float WSFactor)
									vector<GeometryObject> getAllStaticObjects(string evaluation)
									vector<ControllableObject> getAllShots(string evaluation)
									vector<Vehicle> getAllVehicleExclusion(string evaluation)
									SIDE getPlayerSide()
									float getDayTime()
									bool isPlayerCadetMode()
									void SaveGame()
									void ForceEnd()
	2.06		30-09/2011	SSD: Added Methods;
									void ActivateKey(string keyName)
									bool IsKeyActive(string keyName)
	2.07		01-11/2011	CGS: Added Methods:
									string createCenter(SIDE side)
									void deleteCenter(SIDE side)
									int countEnemy(Unit unit, list<Unit> unitList)
									int countFriendly(Unit unit, list<Unit> unitList)
									int countUnknown(Unit unit, list<Unit> unitList)
									int countSide(SIDE side, list<Unit> unitList)


/************************************************************************/

#ifndef VBS2FUSION_MISSION_UTILITIES_H
#define VBS2FUSION_MISSION_UTILITIES_H

/***********************************************************************/
/* INCLUDES
/***********************************************************************/
// STANDARD INCLUDES
#include <sstream>
#include <string>
#include <list>

// SIMCENTRIC INCLUDES
#include "position3D.h"
#include "VBS2Fusion.h"
#include "data/Mission.h"
#if GEOM_OBJECT
#include "data/GeomObject.h"
#endif
#include "data/ControllableObject.h"
#include "util/GroupListUtilities.h"
#include "util/TriggerListUtilities.h"
#include "util/VehicleListUtilities.h"
#include "util/EnvironmentStateUtilities.h"
/*****************************************************************************/
/* END INCLUDES
/*****************************************************************************/

namespace VBS2Fusion
{	
	class VBS2FUSION_API MissionUtilities
	{
	public:

		/*!
		Loads the mission onto the mission variable. Loads and updates (once) the following:

		- the vehicle list
		- the group list
		- the trigger list
		- the EnvironmentState variable.
		- the mission name
		*/
		static void loadMission(Mission& mission);

		/*!
		Updates the mission. Performs the following updates

		- the vehicle list
		- the group list
		- the trigger list
		- the EnvironmentState variable.
		*/
		static void updateMission(Mission& mission);

		/*!
		Returns the player unit of the mission.
		*/
		static Unit getPlayer();

		/*!
		Returns Network ID of Object which unit is mounted.
		*/
		static NetworkID getMountedObject(Unit& unit);

		/*!
		Set drive road side of the mission. Either right or left.
		Deprecated. Use void applyRoadSideDrive(ROADSIDE side)
		*/
		VBS2FUSION_DPR(UMIS001) static void setDriveRoadSide(ROADSIDE side);

		/*!
		get list of playable units of the mission. 
		*/
		static list<Unit> getPlaybleUnits();

		/*!
		check if the simulation is running 
		*/
		static bool isSimulationEnabled();

		/*!
		check if the simulation is paused
		*/
		static bool isPaused();

		/*!
		pause the game simulation.
		Deprecated. Use void applySimulationPause()
		*/
		VBS2FUSION_DPR(UMIS002) static void pauseSimulation();

		/*!
		Resume the game simulation / opposite to pauseSimulation.
		Deprecated. Use void resumeSimulation()
		*/
		VBS2FUSION_DPR(UMIS003) static void resumeSimulation();

		/*!
		Ends the mission with specific ending.
		Deprecated. Use void applyMissionEnd(string& endtype)
		*/
		VBS2FUSION_DPR(UMIS004) static void endMission(string endtype);

		/*!
		A server is hosted and the mission is launched.
		Deprecated, Use void applyMissionHost(string& missionName)
		*/
		VBS2FUSION_DPR(UMIS011) static void hostMission(string missname);

		/*!
		Mission is launched.
		Deprecated. Use void applyMissionPlay(string& campaign, string& missname, bool skipBriefing)
		*/
		VBS2FUSION_DPR(UMIS005) static void playMission(string campaign, string missname, bool skipBriefing);

		/*!
		Record in-game video. Recording automatically ends when Mission quit.
		If filename doesn't contain a backslash, is taken as a relative path with respect to
		the "$HOME\Documents\VBS2\video" 
		Deprecated. Use bool applyCaptureStart(string& filename)	
		*/
		VBS2FUSION_DPR(UMIS007) static bool captureStart(string fileName);


		/*!
		Record in-game video. Recording automatically ends when Mission quit.

		filename		- path to save video.   (backslashes needs to be escaped.)
		captureWidth	- capture video width.  (should be multiple of 4 : default 640)
		captureHeight   - capture video height. (should be multiple of 4 : default 640)
		frameRate		- Frames/Sec			(default :20)
		codercList		- List of codecs set by standard four letter names.
						  (codecs are tried one by one. If none works, Raw data is written. )
		recordSound     - Record sound on/off.
		recordWithUI	- Record video with UI.
		bufferSize		- capturing buffer size. 

		Deprecated. Use bool applyCaptureStart(string& filename, double captureWidth, double captureHeight, int frameRate, list<string> codecList, bool recordSound, bool recordWithUI, double bufferSize)				
		*/
		VBS2FUSION_DPR(UMIS006) static bool captureStart(	string filename,          
									double captureWidth, 
									double captureHeight,
									int frameRate,
									list<string> codecList,
									bool recordSound,
									bool recordWithUI,
									double bufferSize
									);

		/*!
		Stop in-game video recording. 
		Deprecated. Use void applyCaptureStop()	
		*/
		VBS2FUSION_DPR(UMIS009) static void captureStop();

		/*!
		Check if video recording is running.
		Deprecated. Use bool isCaptureTest()	
		*/
		VBS2FUSION_DPR(UMIS008) static bool captureTest();

		/*!
		Returns true if player has map enabled, else false.
		*/
		static bool IsMapShown();

		/*!
		Returns true if player has watch enabled, else false.
		*/
		static bool IsWatchShown();

		/*!
		Returns true if player has compass enabled, else false.
		*/
		static bool IsCompassShown();

		/*!
		Returns true if player has radio enabled, else false.
		*/
		static bool IsWalkieTalkieShown();

		/*!
		Returns true if player has notebook enabled, else false.
		*/
		static bool IsNotepadShown();

		/*!
		Returns true if player has GPS receiver enabled, else false.
		*/
		static bool IsGPSShown();

		/*!
		Loads the mission name to the mission object.
		*/
		static void loadMissionName(Mission& mission);

		/*!
		Returns the current mission name.
		*/
		static string getMissionName();

		/*!
		Return when mission started in format [year, month, day, hour, minute, second]. 
		Works only in multi-player, in single-player all values are equal to zero [0,0,0,0,0,0]
		Deprecated. Use vector<int> getMissionStartTime()	
		*/
		VBS2FUSION_DPR(UMIS010) static vector<int> MissionStartTime();

		/*!
		Returns true if the current player is logged in as a view client
		*/
		static bool isViewClient();

		/*!
		Return the current weaponSwayFactor
		*/
		static float getWeaponSwayFactor();

		/*!
		Return the current weaponSwayFactor
		*/
		static double getWeaponSwayFactorEx();

		/*!
		Change default sway which is depending on fatigue. If set to 0 the weapon should be completely steady. 
		*/
		static void applyWeaponSwayFactor(float WSFactor);

		/*!
		Change default sway which is depending on fatigue. If set to 0 the weapon should be completely steady. 
		*/
		static void applyWeaponSwayFactor(double WSFactor);

#ifdef DEVELOPMENT
#if 0
		/*!
		Return a list of all editor-placed, static objects in the current mission that match the given evaluation. _x is substituted for the actual object in the evaluation.

		Includes the following simulation types:
		house
		church
		vasi 

		May also return some visitor-placed objects (which are part of the map) in versions before 1.51.

		evaluation - condition that an object has to fulfill, to be returned
		*/
		static vector<GeomObject> getAllStaticObjects(string evaluation);
#endif
#endif

		/*!
		Return a list of all vehicles in the current mission, excluding the ones covered by the evaluation.
		_x is substituted for the actual object in the evaluation.

		evaluation - Object which fulfills the specified condition will be excluded
		*/
		static vector<Vehicle> getAllVehicleExclusion(string evaluation);

		/*!
		Returns the player's side. This is valid even when the player controlled person is dead 
		*/
		static SIDE getPlayerSide();

		/*!
		Returns the current ingame time in hours. 
		Time using a 24 hour clock
		*/
		static float getDayTime();

		/*!
		Returns if the player is currently playing in cadet or veteran mode.
		*/
		static bool isPlayerCadetMode();

#ifdef DEVELOPMENT
		/*!
		Autosave game (used for Retry). 
		*/
		static void SaveGame();

		/*!
		Enforces mission termination. Can be used in an "END" trigger to force end conditions in the editor.
		*/
		static void ForceEnd();

		/*!
		Forces the mission to end with the specified ending. 
		endType - The enum has ET_CONTINUE, ET_KILLED, ET_LOST, ET_END1, ET_END2, ET_END3, ET_END4, ET_END5, ET_END6
		*/
		static void ForceEnding(ENDTYPE endType);
#endif

		/*!
		Activates the given Key Name for the current user profile.
		Keys can be used to indicate completed missions or to check whether 
		required missions have been completed. 
		Deprecated. Use void applyActivateKey(string& keyName)	
		*/
		VBS2FUSION_DPR(UMIS012) static void ActivateKey(string keyName);

		/*!
		Checks whether the given Key is active in the current user profile.
		*/
		static bool IsKeyActive(string keyName);


		/*!
		Creates a new AI HQ for the given side.
		*/
		static string createCenter(SIDE side);

		/*!
		Destroys the AI center of the given side.
		*/
		static void deleteCenter(SIDE side);



		/*!
		Count how many units in the list are considered enemy to the given unit.
		Deprecated, Use int getEnemyCount(Unit unit, list<Unit>& unitList)
		*/
		VBS2FUSION_DPR(UMIS020) static int countEnemy(Unit unit, list<Unit> unitList);

		/*!
		Count how many units in the list are considered friendly to the given unit.
		Deprecated, Use int getFriendlyCount(Unit unit, list<Unit>& unitList)
		*/
		VBS2FUSION_DPR(UMIS021) static int countFriendly(Unit unit, list<Unit> unitList);

		/*!
		Count how many units in the list are unknown to the given unit.
		Deprecated, Use int getUnknownCount(Unit unit, list<Unit>& unitList)
		*/
		VBS2FUSION_DPR(UMIS022) static int countUnknown(Unit unit, list<Unit> unitList);

		/*!
		Count how many units in the list belong to given side.
		Deprecated, Use int getSideCount(SIDE side, list<Unit>& unitList)
		*/
		VBS2FUSION_DPR(UMIS023) static int countSide(SIDE side, list<Unit> unitList);


#ifdef DEVELOPMENT

		/*!
		This statement is launched whenever a player is connected to a MP session.
		*/
		static void onPlayerConnected(string statement);
#endif

		/*!
		This statement is launched whenever a player is disconnected from a MP session.
		Deprecated, Use void applyOnPlayerDisconnected(string statement)
		*/
		VBS2FUSION_DPR(UMIS026) static void onPlayerDisconnected(string statement);

		/*!
		Defines an action performed just after a vehicle or object is created.
		Variable command should be a valid VBS2 script command.
		Deprecated, Use void applyOnVehicleCreated(string command)
		*/
		VBS2FUSION_DPR(UMIS028) static void onVehicleCreated(string command);

#ifdef DEVELOPMENT
		/*!
		Return count of players playing on given side. Works only in multiplayer,
		in singleplayer always returns 0. SIDE variable should be one of 
		(WEST, EAST, CIVILIAN, RESISTANCE)
		*/
		static int getPlayersNumber(SIDE side);

#endif

#ifdef DEVELOPMENT
		/*!
		Forces the mission to end with the specified ending.
		endtype can be;
			0: Continue
			1: Killed
			2: Lost
			3: End1
			4: End2
			5: End3
			6: End4
			7: End5
			8: End6
		*/
		static void ForceEnding(int endtype);
#endif // DEVELOPMENT


		/*!
		Process statements stored using setVehicleInit. The statements will 
		only be executed once even if processInitCommands is called multiple 
		times
		Deprecated, Use void applyInitCommandsProcessing()
		*/
		VBS2FUSION_DPR(UMIS029) static void processInitCommands();

		/*!
		Launch init.sqs or init.sqf scripts.
		Deprecated, Use void applyInitScriptRunning()
		*/
		VBS2FUSION_DPR(UMIS033) static void runInitScript();


		/*!
		Enable Map (default true) 
		void GeneralUtilities::forceMap(bool show) should be call to see the effect of the function.
		Deprecated. Use void applyMapShowing(bool show)
		*/
		VBS2FUSION_DPR(UMIS013) static void showMap(bool show);

		/*!
		Shows or hides the watch on the map screen, if enabled for the mission and you possess the item. (default true) 
		(Only works for animated maps in V1.40.) 
		void GeneralUtilities::forceMap(bool show) should be call to see the effect of the function.
		Deprecated. Use void applyWatchShowing(bool show)
		*/
		VBS2FUSION_DPR(UMIS034) static void showWatch(bool show);

		/*!
		Shows or hides the compass on the map screen, if enabled for the mission and you possess the item. (default true). (Only works for animated maps in V1.40.) 
		void GeneralUtilities::forceMap(bool show) should be call to see the effect of the function.
		Deprecated. Use void applyCompassShowing(bool show)
		*/
		VBS2FUSION_DPR(UMIS035) static void showCompass(bool show);

		/*!
		Shows or hides the radio on the map screen, if enabled for the mission and you possess the item. (default true). (Only works for animated maps in V1.40.) 
		void GeneralUtilities::forceMap(bool show) should be call to see the effect of the function.
		Deprecated. Use void applyRadioShowing(bool show)
		*/
		VBS2FUSION_DPR(UMIS014) static void showRadio(bool show);

		/*!
		Shows or hides the notebook on the map screen, if enabled for the mission. (default true). (Only works for animated maps in V1.40.) 
		void GeneralUtilities::forceMap(bool show) should be call to see the effect of the function.
		Deprecated. Use void applyPadShowing(bool show)
		*/
		VBS2FUSION_DPR(UMIS015) static void showPad(bool show);

#ifdef BANU_DEV_LOCAL
		/*!
		Enable ID card (default false). Obsolete command. 
		void GeneralUtilities::forceMap(bool show) should be call to see the effect of the function.
		*/
		static void showWarrant(bool show);

		/*!
		Shows or hides the GPS receiver on the map screen, if enabled for the mission and you possess the item. (default false). (Doesn't work in V1.40.)
		void GeneralUtilities::forceMap(bool show) should be call to see the effect of the function.
		*/
		static void showGPS(bool show);
#endif
		/*
			Returns version of currently loaded mission
		*/
		static int getMissionVersion();

		/*!
		Sets version of currently loaded mission
		Deprecated. Use void applyMissionVersion(double versionNum)
		*/
		VBS2FUSION_DPR(UMIS016) static void setMissionVersion(double versionNum);

		/*!
		Switch player to no unit
		Deprecated. Use void applyPlayerSwitchOff()
		*/
		VBS2FUSION_DPR(UMIS036) static void switchOffFromPlayer();

		/*!
		Get the names of playable units
		*/
		static vector<string> getPlaybleUnitNames();

		/*!
		Return a list of all units (all persons except agents)
		*/
		static vector<Unit> getAllUnits();
		/*!
		Return a list of dead units and destroyed vehicles. Dead unit might be inside vehicle.
		*/
		static vector<ControllableObject> getAllDead();

		/*!
		Returns all mission objects with given type. 
		Invalid type will return null vector.
		*/
		static vector<ControllableObject> getAllMissionObjects(string& type);

		/*!
		Clean up the content of radio protocol history.
		Deprecated. Use void applyRadioClear()
		*/
		VBS2FUSION_DPR(UMIS017) static void clearRadio();

		/*!
		Sets the status of an objective that was defined in briefing.html.
		Status may be one of:
			- "ACTIVE"
			- "FAILED"
			- "DONE"
			- "HIDDEN" 

		To refer to an objective that is named "OBJ_1", use the index number "1". 
		Deprecated. Use void applyObjectiveStatus(string& objectiveNumber, string& status)
		*/
		VBS2FUSION_DPR(UMIS018) static void setObjectiveStatus(string& objectiveNumber, string& status);

		/*!
		Impulse creation, for a wave simulation, is done via this command. It sets an oscillating point. The oscillations behaves like waves, 
		they look so, and they influence objects therein. Meaining of the parameters mentioned below:
			wPos - Impulse 2D coordinates (m)
			wAmp - Amplitude of impulse (m)
			wOscTime - Oscillation time of impulse (s)
			halfDumpTime - Amplitude decreasing time to half value (s)
			lifeTime - Impulse life time (s)
			radius - Radius of impulse influence (m)
			index - Wave index (modulo 100)
		Deprecated. Use bool applyWaterImpulse(position2D& wPos,double wAmp, double wOscTime, double halfDumpTime, double lifeTime, double radius, int index)
		*/
		VBS2FUSION_DPR(UMIS037) static bool setWaterImpulse(position2D& wPos, double wAmp, double wOscTime, double halfDumpTime, double lifeTime, double radius, int index);

		/*!
		Enable and disable radio messages to be heard and shown in the left lower corner of the screen.  
		True to enable the radio, false to disable it
		Deprecated. Use void applyRadioEnable(bool state)
		*/
		VBS2FUSION_DPR(UMIS019) static void enableRadio(bool state);

		/*!
		Gives the current Music volume.
		*/
		static double getMusicVolume();

		/*!
		Changes the sound volume smoothly within the given time. Here time in seconds.  Sound volume range 0 to 1.
		Deprecated. Use void applySoundVolume(double time, double volume)
		*/
		VBS2FUSION_DPR(UMIS024) static void setSoundVolume(double time, double volume);

		/*!
		Gives the current Sound volume.
		*/
		static double getSoundVolume();

		/*!
		Gives the current Radio volume.
		*/
		static double getRadioVolume();

		/*!
		Adds text into the events section of the mpreport.txt file (in installation folder). 
		Only works in MP sessions, and is only visible after session has been finished. 
		Depricated. Use void applyEventTextAdd(string text)
		*/
		VBS2FUSION_DPR(UMIS025) static void addEventTextInMPRecord(string text);

		/*!
		Adds text into the header section of the mpreport.txt file
		Depricated. Use void applyHeaderTextAdd(string header)
		*/
		VBS2FUSION_DPR(UMIS027) static void addHeaderInMPRecord(string header);

		/*!
		@description

		Adds text into the footer section of the mpreport.txt file.

		@locality

		Locally Applied, Locally Effected

		@version [VBS2Fusion v3.12]

		@param footer - Text to be added into mpreport.txt as a footer.

		@return Nothing.

		@example

		@code

		//Adds "FOOTERINMPRECORD" to the footer section of the mpreport.txt
		MissionUtilities::applyFooterInMPRecord(string("FOOTERINMPRECORD"));

		@endcode

		@overloaded 

		@related

		@remarks
		*/
		static void applyFooterInMPRecord(string& footer);

		/*!
		Sets how friendly side1 is with side2. For a value smaller than 0.6 it results in being enemy,
		otherwise it's friendly.

		Note : This friendly behavior will not be applied for units which were created after called
		this function. If want to apply it to those units, have to call the function again.
		Depricated. Use void applyFriend(SIDE side1, SIDE side2 , double value)
		*/
		VBS2FUSION_DPR(UMIS030) static void setFriend(SIDE side1, SIDE side2 , double value);

		/*!
		Changes the music volume smoothly within the given time. Here time in seconds.  Music volume range 0 to 1.
		Depricated. Use void applyMusicVolume(double time, double volume)
		*/
		VBS2FUSION_DPR(UMIS031) static void setMusicVolume(double time, double volume);

		/*!
		Set HDR eye accommodation.
		Deprecated. Use void applyEyeAccom(double eyeAccValue)
		*/
		VBS2FUSION_DPR(UMIS032) static void setEyeAccom(double eyeAccValue);

		/*!
		returns side score.
		*/
		static double getSideScore(SIDE side);

		/*!
		Return a list of all shots in the current mission that match the given evaluation. 
		_x is substituted for the actual object in the evaluation.
		*/
		static vector<ControllableObject> getAllShots(string evaluation);

		/*!
		@description
		Creates a new engagement between the specified units.
		An engagement is defined as units (within a certain radius) detecting enemy units, even if no fire is exchanged, 
		due to the units being unarmed, out of ammo, or having their behavior set to "careless". 
		They are also created whenever a unit fires his weapon (even if it's just a shot in the air) 
		or is damaged by an explosion (but not by falls or vehicle hits).
		This function will not cause any actual action between the specified units, that is,
		if they aren't aware or firing at each other yet, this function will not cause them to do any of these, either.

		@locality

		Globally Applied

		@version [VBS2Fusion v3.11]

		@param unitList - a list of units to be involved in a new engagement.

		@return int - ID of a newly created engagement (for later access of particular engagement).

		@example

		@code

		//Assign some units to the list called "myList"
		int id = MissionUtilities::createEngagement(myList);

		//To display the engagement as a String, use the Function conversions::IntToString as shown below.
		displayString = "Engagement: " + conversions::IntToString(id);

		@endcode

		@overloaded

		Nothing

		@related

		MissionUtilities::getEngagements()
		MissionUtilities::getEngagementUnits( int engagementID )
		MissionUtilities::getEngagementIDs( Unit& unit )
		MissionUtilities::getEngagementIDs( Group& grp )
		MissionUtilities::getEngagementIDs( SIDE side )
		MissionUtilities::applyEngagementTimeouts( double interaction, double defeat )
		MissionUtilities::getEngagementTimeouts()

		@remarks This function is meant to be used ONLY on the server configuration. It has no effects on clients.
		@remarks To create the engagement between units in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function MissionUtilities::createEngagement( list<Unit> unitList). (Refer (MissionUtilities::loadMission(Mission& mission)for further information)
		 */
		static int  createEngagement( list<Unit> unitList);

		/*!

		@description - Returns a list of all currently active engagements.
		An engagement is defined as units (within a certain radius) detecting enemy units � even if no fire is exchanged, 
		due to the units being unarmed, out of ammo, or having their behavior set to "careless". 
		They are also created whenever a unit fires his weapon (even if it's just a shot in the air) 
		or is damaged by an explosion (but not by falls or vehicle hits).

		@locality:

		Globally Applied

		@version: [VBS2Fusion v3.11] 

		@param Nothing

		@return vector of engagement IDs.

		@example

		@code

		//Assign the getEngagements return value to a vector as shown below
		vector<int> v = MissionUtilities::getEngagements();

		displayString = "Engagement: ";

		//To display the engagement as a String, use the Function conversions::IntToString as shown below.
		for (vector<int>::iterator itr = v.begin(), itr_end = v.end(); itr != itr_end; ++itr)
		{
		displayString += conversions::IntToString(*itr);
		}

		@endcode

		@overloaded Nothing.

		@related

		MissionUtilities::createEngagement( list<Unit> unitList)
		MissionUtilities::getEngagementUnits( int engagementID )
		MissionUtilities::getEngagementIDs( Unit& unit )
		MissionUtilities::getEngagementIDs( Group& grp )
		MissionUtilities::getEngagementIDs( SIDE side )
		MissionUtilities::applyEngagementTimeouts( double interaction, double defeat )
		MissionUtilities::getEngagementTimeouts()	

		@remarks To get the active engagements in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function MissionUtilities::getEngagements(). (Refer (MissionUtilities::loadMission(Mission& mission)for further information)

		 */
		static vector<int> getEngagements();

		/*!

		@description

		Returns units that are part of a specific engagement. It will only return the units for a currently active engagement. 
		For engagements that have times out, nothing is returned.

		@locality

		Globally Applied
		
		@version [VBS2Fusion v3.11]

		@param engagementID -  ID of the engagement that you are going to refer.
		
		@return list of units participating in particular engagement.

		@example

		@code
		
		// should be initialized with createEngagement function or relevant in order to use here.
		int value = -1;

		//Assign the getEngagement Units to a vector as shown below.
		vector<Unit> v = MissionUtilities::getEngagementUnits(conversions::stringToInt(value));

		displayString = "Engagement: ";

		//To display the Engagement Units.
		for (vector<Unit>::iterator itr = v.begin(), itr_end = v.end(); itr != itr_end; ++itr)
		{
			displayString += itr->getName();
			displayString += " : ";
		}

		@endcode
		
		@overloaded: None.

		@related

		MissionUtilities::createEngagement( list<Unit> unitList)
		MissionUtilities::getEngagements()
		MissionUtilities::getEngagementIDs( Unit& unit )
		MissionUtilities::getEngagementIDs( Group& grp )
		MissionUtilities::getEngagementIDs( SIDE side )
		MissionUtilities::applyEngagementTimeouts( double interaction, double defeat )
		MissionUtilities::getEngagementTimeouts()

		@remarks To obtain the units that are part of a specific engagement in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function MissionUtilities::getEngagementUnits(int engagementID). (Refer (MissionUtilities::loadMission(Mission& mission)for further information)

		*/
		static vector<Unit> getEngagementUnits(int engagementID);

		/*!

		@description
		Returns list of engagements the given unit is currently part of.

		@locality

		Globally Applied
		
		@version [VBS2Fusion v3.11]
		
		@param unit - a unit that is engaging with other units.
		
		@return list of engagement IDs of current engagements.
		
		@example

		@code

		//Assign the Engagement ID to a vector as shown below.
		vector<int> v = MissionUtilities::getEngagementIDs(unit1);

		displayString = "Engagement ID: ";
		
		//To display the Engagement ID.
		for (vector<int>::iterator itr = v.begin(), itr_end = v.end(); itr != itr_end; ++itr)
		{
			displayString += conversions::IntToString(*itr);
		}

		@endcode
		
		@overloaded		
		
		MissionUtilities::getEngagementIDs( Group& grp )
		MissionUtilities::getEngagementIDs( SIDE side )

		@related

		MissionUtilities::createEngagement( list<Unit> unitList)
		MissionUtilities::getEngagements()
		MissionUtilities::getEngagementUnits(int engagementID);
		MissionUtilities::applyEngagementTimeouts( double interaction, double defeat )
		MissionUtilities::getEngagementTimeouts()

		@remarks To obtain the list of engagement IDs of current engagements in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function MissionUtilities::getEngagementIDs(Unit& unit). (Refer (MissionUtilities::loadMission(Mission& mission)for further information)

		 */
		static vector<int> getEngagementIDs (Unit& unit);

		/*!
		@description
		Returns list of engagements the given group is currently part of.

		@locality

		Globally applied
		
		@version [VBS2Fusion v3.11]
		
		@param grp - a group that is involved in an engagement.
		
		@return list of engagement IDs of current engagements of the group.
		
		@example

		@code

		//Assign the Engagement ID to a vector as shown below.
		vector<int> v = MissionUtilities::getEngagementIDs(group1);

		displayString = "Engagement ID: ";

		//To display the Engagement ID.
		for (vector<int>::iterator itr = v.begin(), itr_end = v.end(); itr != itr_end; ++itr)
		{
		displayString += conversions::IntToString(*itr);
		}

		@endcode
			
		@overloaded		
		
		MissionUtilities::getEngagementIDs( Unit& unit )
		MissionUtilities::getEngagementIDs( SIDE side )

		@related	
		
		MissionUtilities::createEngagement( list<Unit> unitList)
		MissionUtilities::getEngagements()
		MissionUtilities::getEngagementUnits(int engagementID);
		MissionUtilities::applyEngagementTimeouts( double interaction, double defeat )
		MissionUtilities::getEngagementTimeouts()

		@remarks To obtain the list of engagement IDs of current engagements of the group in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function MissionUtilities::getEngagementIDs(Group& grp). (Refer (MissionUtilities::loadMission(Mission& mission)for further information)
		 */
		static vector<int> getEngagementIDs (Group& grp);

		/*!
		@description
		Returns list of engagements the given side is currently part of.

		@locality

		Globally applied
		
		@version [VBS2Fusion v3.11]
		
		@param side - side parameter. It can be WEST, EAST, CIVILIAN or RESISTANCE
		
		@return list of engagement IDs of current engagements of all entities of particular side.
		
		@example

		@code

		//Assign the Engagement ID to a vector as shown below.
		vector<int> v = MissionUtilities::getEngagementIDs(WEST);

		displayString = "Engagement ID: ";

		//To display the Engagement ID.
		for (vector<int>::iterator itr = v.begin(), itr_end = v.end(); itr != itr_end; ++itr)
		{
		displayString += conversions::IntToString(*itr);
		}

		@endcode
		
		@overloaded

		MissionUtilities::getEngagementIDs( Unit& unit )
		MissionUtilities::getEngagementIDs( Group& grp )

		@related	

		MissionUtilities::createEngagement( list<Unit> unitList)
		MissionUtilities::getEngagements()
		MissionUtilities::getEngagementUnits(int engagementID);
		MissionUtilities::applyEngagementTimeouts( double interaction, double defeat )
		MissionUtilities::getEngagementTimeouts()

		@remarks To obtain the list of engagement IDs of current engagements of all entities of particular side in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function MissionUtilities::getEngagementIDs(SIDE side). (Refer (MissionUtilities::loadMission(Mission& mission)for further information)
		*/
		static vector<int> getEngagementIDs (SIDE side);

		/*!
		@description
		Applies the timeouts after which an engagement is considered finished.
		Any new interaction would be considered a new engagement, even if it involves the same participants.

		@locality

		@version [VBS2Fusion v3.11]

		@param interaction - Seconds of non-interaction (default: 900) 

		@param defeat - Seconds after the defeat of one side (default: 300)

		@return Nothing.

		@example

		@code

		//Apply  Engagement timeouts
		MissionUtilities::applyEngagementTimeouts(60,5);

		@endcode

		@overloaded

		Nothing

		@related

		MissionUtilities::createEngagement( list<Unit> unitList)
		MissionUtilities::getEngagements()
		MissionUtilities::getEngagementUnits(int engagementID);
		MissionUtilities::applyEngagementTimeouts( double interaction, double defeat )
		MissionUtilities::getEngagementTimeouts()
		MissionUtilities::getEngagementIDs( Unit& unit )
		MissionUtilities::getEngagementIDs( Group& grp )
		MissionUtilities::getEngagementIDs( SIDE side )

		@remarks Function is meant to ONLY be used on a server. It does not have any effect on clients.
		*/
		static void applyEngagementTimeouts (double interaction, double defeat);

		/*!
		@description
		Returns the engagement timeouts.

		@locality

		Globally applied
		
		@version [VBS2Fusion v3.11]
		
		@param None.
		
		@return a pair of double values in which first and second would be interaction and defeat respectively.
		
		@example

		@code

		//Define timeOut as pair.
		pair<double,double> timeOut;

		//Assign the Engagement Timeouts as shown below.
		timeOut = MissionUtilities::getEngagementTimeouts();

		//To display the Engagement Timeouts.
		displayString = "Interaction : "+conversions::DoubleToString(timeOut.first);
		displayString += "\\nDefeat : "+conversions::DoubleToString(timeOut.second);

		@endcode

		@overloaded 

		Nothing
		
		@related	
		
		MissionUtilities::createEngagement( list<Unit> unitList)
		MissionUtilities::getEngagements()
		MissionUtilities::getEngagementUnits(int engagementID);
		MissionUtilities::applyEngagementTimeouts( double interaction, double defeat )
		MissionUtilities::getEngagementTimeouts()
		MissionUtilities::getEngagementIDs( Unit& unit )
		MissionUtilities::getEngagementIDs( Group& grp )
		MissionUtilities::getEngagementIDs( SIDE side )
		
		@remarks Function will not return anything on clients for a dedicated server setup.
		 */
		static pair<double,double> getEngagementTimeouts();

		/*!
		@description
		Return a list of connected network clients.

		@locality

		Globally applied
		
		@version [VBS2Fusion v3.11]
		
		@param None.
		
		@return list of IDs of clients that are connected with.
		
		@example

		@code

		//Assign the Engagement ID to a vector as shown below.
		vector<int> v = MissionUtilities::getAllClients();

		displayString = "Clients: ";

		//To display the Clients.
		for (vector<int>::iterator itr = v.begin(), itr_end = v.end(); itr != itr_end; ++itr)
		{
		displayString += conversions::IntToString(*itr);
		}

		@endcode

		@overloaded

		Nothing.

		@related	
		
		MissionUtilities::getClientName( int clientID )
		MissionUtilities::getClientIDofPlayer( Unit& unit )
		MissionUtilities::getPlayerOfClient( int clientID )

		@remarks To obtain the list of IDs of clients that are connected in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function MissionUtilities::getAllClients(). (Refer (MissionUtilities::loadMission(Mission& mission)for further information)
		 */
		static vector<int> getAllClients();

		/*!
		@description
		Return the profile name of a network client.

		@locality

		Globally applied
		
		@version [VBS2Fusion v3.11]
		
		@param clientID - ID of a client as returned by getAllClients or other related functions
		
		@return Profile name of particular client
		
		@example

		@code
		
		//Display the client name as below.
		displayString = "Client Name is: " + MissionUtilities::getClientName(1005188573);

		@endcode

		@overloaded

		Nothing.
		
		@related	
		
		MissionUtilities::getAllClients()
		MissionUtilities::getClientIDofPlayer( Unit& unit )
		MissionUtilities::getPlayerOfClient( int clientID )
		
		@remarks To obtain the profile name of particular client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function MissionUtilities::getClientName(int clientID). (Refer (MissionUtilities::loadMission(Mission& mission)for further information)
		*/
		static string getClientName( int clientID);

		/*!
		@description
		Returns the network ID for a specific unit (object).
		
		@locality

		Globally applied
		
		@version [VBS2Fusion v3.11]
		
		@param unit - Unit Object that is used by the client
		
		@return  client ID of specified unit
		
		@example

		@code

		//Assign the units to a vector as shown below.
		vector<Unit> allUnits;

		//Get all the players in the mission
		allUnits = PlayerUtilities::getAllPlayers();

		//To display the Network ID.
		for (vector<Unit>::iterator itr = allUnits.begin(); itr != allUnits.end();itr++)
		{

			displayString += "\\nPlayer is: " + conversions::IntToString(MissionUtilities::getClientIDofPlayer(*itr));

		}

		@endcode

		@overloaded

		Nothing
		
		@related	
		
		MissionUtilities::getAllClients()
		MissionUtilities::getClientName( int clientID )
		MissionUtilities::getPlayerOfClient( int clientID )

		@remarks To obtain the client ID of specified unit in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function MissionUtilities::getClientIDofPlayer(Unit& unit). (Refer (MissionUtilities::loadMission(Mission& mission)for further information)
		 */
		static int getClientIDofPlayer( Unit& unit);

		/*!
		@description
		Returns the unit object that is being used by a network client.
		
		@locality

		Globally applied
		
		@version [VBS2Fusion v3.11]
		
		@param clientID - client ID of a particular client
		
		@return  Unit that is used by the client
		
		@example

		@code

		//get the player object that is given by the below network ID
		player = MissionUtilities::getPlayerOfClient(1398855324);

		//To display the unit name
		displayString = "Player is: " + player.getName();

		@endcode
			
		@overloaded None

		@related	
		
		MissionUtilities::getAllClients()
		MissionUtilities::getClientName( int clientID )
		MissionUtilities::getClientIDofPlayer( Unit& unit )

		@remarks To obtain the unit that is used by the client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network before calling the function MissionUtilities::getPlayerOfClient(int clientID ). (Refer (MissionUtilities::loadMission(Mission& mission)for further information)
		 */
		static Unit getPlayerOfClient( int clientID );

		/*!
		@description
		
		Applies drive road side of the mission.

		@locality

		Locally applied, Globally affected

		@version [VBS2Fusion v3.15]

		@param side - Drive road side. It can be right, left or middle.

		@return Nothing.

		@example

		@code

		MissionUtilities::applyRoadSideDrive(ROAD_RIGHT);

		@overloaded 

		@related

		@remarks This is a replication of void setDriveRoadSide(ROADSIDE side) function.
		*/
		static void applyRoadSideDrive(ROADSIDE side);

		/*!
		@description

		Pause the game simulation.

		@locality

		Locally applied, Globally effected

		@version [VBS2Fusion v3.15]

		@param None.

		@return Nothing.

		@example

		@code

		MissionUtilities::applySimulationPause();

		@overloaded 

		@related

		MissionUtilities::applySimulatinResume()

		@remarks This is a replication of void pauseSimulation() function.
		*/
		static void applySimulationPause();

		/*!
		@description

		Resume the game simulation / opposite to pauseSimulation.

		@locality

		Locally applied, Globally effected

		@version [VBS2Fusion v3.15]

		@param None.

		@return Nothing.

		@example

		@code

		MissionUtilities::applySimulatinResume();

		@overloaded 

		@related

		MissionUtilities::applySimulationPause()

		@remarks This is a replication of void resumeSimulation() function.
		*/
		static void applySimulatinResume();

		/*!
		@description

		Ends the mission with specific ending.

		@locality

		Locally applied, Locally effected

		@version [VBS2Fusion v3.15]

		@param None.

		@return endtype - End type can be "Continue" (mission continues normally), "Killed" (no debriefing is shown, only the "Retry" or "End" buttons)  
		"End1" - "End6", or "Loser" (appropriate debriefing section is shown)

		@example

		@code

		MissionUtilities::applyMissionEnd(string ("END1"));

		@overloaded 

		@related

		@remarks This is a replication of void endMission(string endtype) function.
		*/
		static void applyMissionEnd(string& endtype);

		/*!
		@description

		Mission is launched.

		@locality

		Locally applied, Locally effected

		@version [VBS2Fusion v3.15]

		@param campaign - Name of the campaign and it is always empty.

		@param missname - Mission name, including map extension.

		@param skipBriefing - Whether to skip the briefing and intro.

		@return Nothing.

		@example

		@code

		MissionUtilities::applyMissionPlay(string(""), string("T01_Basic_Movement.Intro"),true);

		@overloaded 

		@related

		@remarks The mission must exist, as a PBO file, in the mission folder.

		@remarks This is a replication of void playMission(string campaign, string missname, bool skipBriefing) function.
		*/
		static void applyMissionPlay(string& campaign, string& missname, bool skipBriefing);

		/*!
		@description

		Record in-game video. Recording automatically ends when Mission quit.

		@locality

		Locally applied, Locally effected

		@version [VBS2Fusion v3.15]

		@param filename - Path to save video in (backslashes need to be escaped: e.g. "O:\\fp\\vbs-video.avi"). If filename doesn't contain a backslash, is taken as a relative path with respect to the "$HOME\Documents\VBS2\video". If no extension is given, AVI is used. 

		@param captureWidth	- Capture video width. (has to be a multiple of 4) 
		
		@param captureHeight - Captured video height. (has to be a multiple of 4)

		@param frameRate - Number of frames per second.

		@param codercList - List of codecs names by their standard four letter names. codecs are tried one by one. If none works, raw data is written. 

		@param recordSound - Record sound on/off.

		@param recordWithUI	- Record video with UI shown/hidden.

		@param bufferSize - Buffer size in MB. Can be up-to 100MB. It is not necessary to use such a large amount unless raw data is saved.

		@return bool - True if recoding starts successfully otherwise returns false if it already is recording 

		@example

		@code

		//Setup the parameters for the function

		std::list<std::string> codecList;
		codecList.push_back("MPG4");
		codecList.push_back("DIVX");
		codecList.push_back("XVID");
		codecList.push_back("MPG2");
		codecList.push_back("MPG1");
		codecList.push_back("MJPG");

		bool b=MissionUtilities::applyCaptureStart(string("E:\\vbs-video.avi"), 640, 480, 20, codecList, true, true, 10);

		@overloaded 

		@related

		@remarks This is a replication of bool captureStart(string filename, double captureWidth, double captureHeight, int frameRate, list<string> codecList, bool recordSound, bool recordWithUI, double bufferSize) function.
		*/
		static bool applyCaptureStart(string& filename,
						double captureWidth,
						double captureHeight,
						int frameRate,
						list<string> codecList,
						bool recordSound, 
						bool recordWithUI,
						double bufferSize);		

		/*!
		@description

		Record in-game video. Recording automatically ends when Mission quit.

		@locality

		Locally applied, Locally effected

		@version [VBS2Fusion v3.15]

		@param filename - Path to save video in (backslashes need to be escaped: e.g. "O:\\fp\\vbs-video.avi"). If filename doesn't contain a backslash, is taken as a relative path with respect to the "$HOME\Documents\VBS2\video". If no extension is given, AVI is used. 

		@return bool - True if recoding starts successfully otherwise returns false if it already is recording 

		@example

		@code

		bool b=MissionUtilities::applyCaptureStart(string("E:\\vbs-video4.avi"));

		@overloaded 

		MissionUtilities::applyCaptureStart(string& filename, double captureWidth, double captureHeight, int frameRate, list<string> codecList, bool recordSound, bool recordWithUI, double bufferSize)

		@related

		@remarks This is a replication of bool captureStart(string filename) function.
		*/
		static bool applyCaptureStart(string& filename);

		/*!
		@description

		Check if video recording is running.

		@locality

		Locally applied, Locally effected

		@version [VBS2Fusion v3.15]

		@param None.

		@return bool - Return true if video recording is running otherwise returns false.

		@example

		@code

		bool testStatus=MissionUtilities::isCaptureTest();

		@overloaded 

		@related

		@remarks This is a replication of bool captureTest() function.
		*/
		static bool isCaptureTest();

		/*!
		@description

		Stop in-game video recording.

		@locality

		Locally applied, Locally effected

		@version [VBS2Fusion v3.15]

		@param None.

		@return Nothing.

		@example

		@code

		MissionUtilities::applyCaptureStop();

		@overloaded 

		@related

		@remarks This is a replication of void captureStop() function.
		*/
		static void applyCaptureStop();

		/*!
		@description

		Return when mission started.

		@locality

		@version [VBS2Fusion v3.15]

		@param None.

		@return vector<int> - Vector contains year, month, day, hour, minute, second respectively.

		@example

		@overloaded 

		@related

		@remarks Works only in multiplayer, in singleplayer all values are equal to zero.

		@remarks This is a replication of vector<int> MissionStartTime() function.
		*/
		static vector<int> getMissionStartTime();

		/*!
		@description

		A server is hosted and the mission is launched.

		@locality

		Locally applied, Locally effected

		@version [VBS2Fusion v3.15]

		@param missionName - Name of the mission that need to be run.

		@return None.

		@example

		@code

		MissionUtilities::applyMissionHost(string("MultiplayerMission.Intro"));

		@overloaded 

		@related.

		@remarks This is a replication of void hostMission(string missname).
		*/
		static void applyMissionHost(string& missionName);

		/*!
		@description

		Activates the given keyname for the current user profile.
		The keys can be used to indicate completed missions or 
		to check whether required missions have been completed.

		@locality

		Locally applied, Locally affected

		@version [VBS2Fusion v3.15]

		@param keyName - Name of the key.

		@return None.

		@example

		MissionUtilities::applyActivateKey(string ("COIN_OPS"));

		@overloaded 

		@related.

		@remarks This is a replication of void ActivateKey(string keyName).
		*/
		static void applyActivateKey(string& keyName);

		/*!
		@description
		Enable Map

		@locality

		Locally applied, Locally effected

		@version [VBS2Fusion v3.15]

		@param show - show/hide map.

		@return None.

		@example

		@code

		MissionUtilities::applyMapShowing(true);

		@overloaded 

		@related.

		@remarks This is a replication of void showMap(bool show).
		*/
		static void applyMapShowing(bool show);

		/*!
		@description
		Shows or hides the radio on the map screen, if enabled for the mission and you possess the item.

		@locality

		Locally applied, Locally effected

		@version [VBS2Fusion v3.15]

		@param show - show/hide radio.

		@return None.

		@example

		@code

		MissionUtilities::applyRadioShowing(true);

		@overloaded 

		@related.

		@remarks This is a replication of void showRadio(bool show).
		*/
		static void applyRadioShowing(bool show);

		/*!
		@description
		Shows or hides the notebook on the map screen, if enabled for the mission.

		@locality

		Locally applied, Locally effected

		@version [VBS2Fusion v3.15]

		@param show - show/hide pad.

		@return None.

		@example

		@code

		MissionUtilities::applyPadShowing(true);

		@overloaded 

		@related.

		@remarks This is a replication of void showPad(bool show).
		*/
		static void applyPadShowing(bool show);

		/*!
		@description
		Sets version of currently loaded mission

		@locality

		Locally applied, Locally effected

		@version [VBS2Fusion v3.15]

		@param versionNum - version number.

		@return None.

		@example

		@code

		MissionUtilities::applyMissionVersion(3.01);

		@overloaded 

		@related.

		@remarks This is a replication of void setMissionVersion(double versionNum).
		*/
		static void applyMissionVersion(double versionNum);

		/*!
		@description
		Clean up the content of radio protocol history.

		@locality

		locally applied, locally effected.

		@version [VBS2Fusion v3.15]

		@param None.

		@return None.

		@example

		@code

		MissionUtilities::applyRadioClear();

		@endcode

		@overloaded 

		None.

		@related.

		@remarks This is a replication of void clearRadio().
		*/
		static void applyRadioClear();

		/*!
		@description
		Sets the status of an objective that was defined in briefing.html.

		@locality

		Locally applied, locally effected.

		@version [VBS2Fusion v3.15]

		@param objectiveNumber - To refer to an objective that is named "OBJ_1", for example, use only the index number in this command (i.e. "1" ).

		@param status - One of: - "ACTIVE", "FAILED", "DONE", "HIDDEN".

		@return None.

		@example

		@code

		//Marks the objective named "OBJ_1" as completed.
		MissionUtilities::applyObjectiveStatus(string("1"),string("DONE"));

		@endcode

		@overloaded 

		@related.

		@remarks This is a replication of void setObjectiveStatus().
		*/
		static void applyObjectiveStatus(string& objectiveNumber, string& status);

		/*!
		@description
		Enable and disable radio messages to be heard and shown in the left lower corner of the screen. This command can be helpful during cutscenes.

		@locality

		Locally applied, locally effected.

		@version [VBS2Fusion v3.15]

		@param state - true to enable the radio, false to disable it.

		@return None.

		@example

		@code

		MissionUtilities::applyRadioEnable(false);

		@endcode

		@overloaded 

		@related.

		@remarks This is a replication of void enableRadio(bool state).
		*/
		static void applyRadioEnable(bool state);

		/*!
		@description

		Count how many units in the list are considered enemy to the given unit.

		@locality

		Globally effected

		@version [VBS2Fusion v3.15]

		@param unit - Unit that need to get the enemy count.
		@param unitList - List of units that compare with the given unit.

		@return int - Return the number of enemies around the given unit.

		@example

		@code

		int count=MissionUtilities::getEnemyCount(player, unitList);

		@overloaded 

		@related.

		@remarks This is a replication of int countEnemy(Unit unit, list<Unit> unitList).
		*/
		static int getEnemyCount(Unit unit, list<Unit>& unitList);

		/*!
		@description

		Count how many units in the list are considered friendly to the given unit.

		@locality

		Globally effected

		@version [VBS2Fusion v3.15]

		@param unit - Unit that need to get the friendly count.
		@param unitList - List of units that compare with the given unit.

		@return int - Return the number of friends around the given unit.

		@example

		@code

		int count=MissionUtilities::getFriendlyCount(player, unitList);

		@overloaded 

		@related.

		@remarks This is a replication of int countFriendly(Unit unit, list<Unit> unitList).
		*/
		static int getFriendlyCount(Unit unit, list<Unit>& unitList);

		/*!
		@description

		Count how many units in the list are unknown to the given unit.

		@locality

		@version [VBS2Fusion v3.15]

		@param unit - Unit that need to get the unknown count.
		@param unitList - List of units that compare with the given unit.

		@return int - Return the number of unknown units in the list.

		@example

		@overloaded 

		@related.

		@remarks This is a replication of int countUnknown(Unit unit, list<Unit> unitList).
		*/ 
		static int getUnknownCount(Unit unit, list<Unit>& unitList);

		/*!
		@description

		Count how many units in the list belong to given side.

		@locality

		Globally effected

		@version [VBS2Fusion v3.15]

		@param side - Side that need to get the unit count.
		@param unitList - List of units.

		@return int - Return the number of units in the list who belongs to given side.

		@example

		@code

		int count=MissionUtilities::getSideCount(WEST, unitList);

		@overloaded 

		@related.

		@remarks This is a replication of int countSide(SIDE side, list<Unit> unitList).
		*/ 
		static int getSideCount(SIDE side, list<Unit>& unitList);

		/*!
		@description
		Changes the sound volume smoothly within the given time.

		@locality

		Locally applied, locally effected.

		@version [VBS2Fusion v3.15]

		@param time - the time in seconds.

		@param volume - sound volume range 0 to 1. Maximum volume is 1.

		@return Nothing.

		@example

		@code

		MissionUtilities::applySoundVolume(5,0.8);

		@endcode

		@overloaded 

		@related.

		@remarks This is a replication of void setSoundVolume(double time, double volume).
		*/ 
		static void applySoundVolume(double time, double volume);

		/*!
		@description
		Adds text into the events section of the mpreport.txt file (in installation folder). Only works in MP sessions, and is only visible after session has been finished.

		@locality

		Locally applied, locally effected.

		@version [VBS2Fusion v3.15]

		@param text - text value.

		@return Nothing.

		@example

		@code

		MissionUtilities::applyEventTextAdd(string("Testing"));

		@endcode

		@overloaded 

		@related.

		@remarks This is a replication of void addEventTextInMPRecord(string text).
		*/ 
		static void applyEventTextAdd(string text);

		/*!
		@description

		This statement is launched whenever a player is disconnected from a MP session. 
		
		@locality

		Globally applied, Globally effected

		@version [VBS2Fusion v3.15]

		@param statement - Statement that need to be executed.
		
		@return None.

		@example

		@code

		//string use for statement

		string sideDisplay = "player sidechat format['Object: Leaved'];";

		MissionUtilities::applyOnPlayerDisconnected(sideDisplay);

		@overloaded 

		@related.

		@remarks This is a replication of void OnPlayerDisconnected(string statement).This function is mainly applicable for server uses.
		*/ 
		static void applyOnPlayerDisconnected(string statement);

		/*!
		@description
		Adds text into the header section of the mpreport.txt file.

		@locality

		Locally applied, locally effected.

		@version [VBS2Fusion v3.15]

		@param header - header text.

		@return Nothing.

		@example

		@code

		MissionUtilities::applyHeaderTextAdd(string("Testing"));

		@endcode

		@overloaded 

		@related.

		@remarks This is a replication of void addHeaderInMPRecord(string header).
		*/ 
		static void applyHeaderTextAdd(string header);

		/*!
		@description

		Defines an action performed just after a vehicle or object is created.
		Variable command should be a valid VBS2 script command.

		@locality

		Locally applied, Globally effected

		@version [VBS2Fusion v3.15]

		@param command - Statement that need to be executed.

		@return None.

		@example

		@code

		//string for hold script command
		string sideDisplay = "player sidechat format['Vehicle: created'];";

		MissionUtilities::applyOnVehicleCreated(sideDisplay);

		@overloaded 

		@related.

		@remarks This is a replication of void applyOnVehicleCreated(string command).
		*/
		static void applyOnVehicleCreated(string command);

		/*!
		@description

		Process statements stored using setVehicleInit. The statements will 
		only be executed once even if processInitCommands is called multiple 
		times

		@locality

		Locally applied,Globally effected

		@version [VBS2Fusion v3.15]

		@param None.

		@return None.

		@example

		@code

		MissionUtilities::applyInitCommandsProcessing();

		@overloaded 

		@related.

		@remarks This is a replication of void processInitCommands().
		*/
		static void applyInitCommandsProcessing();

		/*!
		@description
		Sets how friendly side1 is with side2. For a value smaller than 0.6 it results in being enemy, otherwise it's friendly.
		Intended to be used on mission start. Changing value during mission can cause unexpected errors in AI behavior.
		Soldiers will (currently) not fire on civilians, no matter the "friendliness" setting.

		@locality

		Globally applied, globally effected.

		@version [VBS2Fusion v3.15]

		@param side1 - side to set friendly

		@param side2 - side to which the side1 friendly to

		@param value - friendly value

		@return None.

		@example

		@code

		MissionUtilities::applyFriend(WEST,RESISTANCE,0.5);

		@endcode

		@overloaded 

		@related.

		@remarks This is a replication of void setFriend(SIDE side1, SIDE side2 , double value).
		*/
		static void applyFriend(SIDE side1, SIDE side2, double value);

		/*!
		@description
		Changes the music volume smoothly within the given time.

		@locality

		Locally applied, locally effected.

		@version [VBS2Fusion v3.15]

		@param time - duration of fade in seconds.

		@param volume - music volume range 0 to 1. Maximum volume is 1. Default is 0.5.

		@return None.

		@example

		@code

		MissionUtilities::applyMusicVolume(5,0.3);

		@endcode

		@overloaded 

		@related.

		@remarks This is a replication of void setMusicVolume(double time, double volume).
		*/
		static void applyMusicVolume(double time, double volume);

		/*!
		@description
		Set HDR eye accommodation.

		@locality

		Locally applied, locally effected.

		@version [VBS2Fusion v3.15]

		@param eyeAccValue - accommodate value.

		@return None.

		@example

		@code

		MissionUtilities::applyEyeAccom(0.00001);

		@endcode

		@overloaded 

		@related.

		@remarks This is a replication of void setEyeAccom(double eyeAccValue).
		*/
		static void applyEyeAccom(double eyeAccValue);

		/*!
		@description

		Launch init.sqs or init.sqf scripts.

		@locality

		Locally applied, Locally effected

		@version [VBS2Fusion v3.15]

		@param None.

		@return None.

		@example

		@code

		MissionUtilities::applyInitScriptRunning();

		@overloaded 

		@related.

		@remarks This is a replication of void runInitScript().
		*/
		static void applyInitScriptRunning();

		/*!
		@description
		Shows or hides the watch on the map screen, if enabled for the mission and you possess the item.

		@locality

		@version [VBS2Fusion v3.15]

		@param show - show/hide watch.

		@return None.

		@example

		@overloaded 

		@related.

		@remarks This is a replication of void showWatch(bool show).
		*/
		static void applyWatchShowing(bool show);

		/*!
		@description
		Shows or hides the compass on the map screen, if enabled for the mission and you possess the item.

		@locality

		@version [VBS2Fusion v3.15]

		@param show - show/hide compass.

		@return None.

		@example

		@overloaded 

		@related.

		@remarks This is a replication of void showCompass(bool show).
		*/
		static void applyCompassShowing(bool show);

		/*!
		@description
		Switch player to no unit. This only works in single-player mode.

		@locality

		@version [VBS2Fusion v3.15]

		@param

		@return None.

		@example

		@overloaded 

		@related.

		@remarks This is a replication of void switchOffFromPlayer().
		*/
		static void applyPlayerSwitchOff();

		/*!
		@description
		Impulse creation, for a wave simulation, is done via this command. It sets an oscillating point. The oscillations behaves like waves, they look so, and they influence objects therein.

		@locality

		@version [VBS2Fusion v3.15]

		@param wPos - Impulse 2D coordinates (m)

		@param wAmp - Amplitude of impulse (m)

		@param wOscTime - Oscillation time of impulse (s)

		@param halfDumpTime - Amplitude decreasing time to half value (s)

		@param lifeTime - Impulse life time (s)

		@param radius - Radius of impulse influence (m)

		@param index - Wave index (modulo 100)

		@return

		@example

		@overloaded 

		@related.

		@remarks This is a replication of void setWaterImpulse(position2D& wPos, double wAmp, double wOscTime, double halfDumpTime, double lifeTime, double radius, int index).
		*/
		static bool applyWaterImpulse(position2D& wPos, double wAmp, double wOscTime, double halfDumpTime, double lifeTime, double radius, int index);
	};
};

#endif // MISSION_UTILITIES_H