/**************************************************************************
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 **************************************************************************/

/*****************************************************************************

Name:	TitleTextUtilities.h

Purpose: This file contains Text Related function

/*****************************************************************************/
#ifndef VBS2FUSION_TITLETEXTUTILITIES_H
#define VBS2FUSION_TITLETEXTUTILITIES_H

/*****************************************************************************/
/* INCLUDES
/*****************************************************************************/
// STANDARD INCLUDES
#include <string>
#include <vector>

// SIMCENTRIC INCLUDES
#include "VBS2Fusion.h"
#include "data/ControllableObject.h"
#include "data/Group.h"

/*****************************************************************************/
/* END INCLUDES
/*****************************************************************************/

namespace VBS2Fusion{

	class VBS2FUSION_API TitleTextUtilities
	{
	public:

		/*!
		Prolongs the active title effect.
		*/
		static void prolongTitleEffect(double time);

		/*!
		Displays a text message in the center of the screen.
		*/
		static void cutText(string text,TITLE_EFFECT_TYPE type,double speed);

		/*!
		Displays a text message in the center of the screen with additonal parameter layer
		layer: Number - Defines the layer in which the effect is shown, where 0 is the backmost one. 
		*/
		static void cutText(double layer,string text,TITLE_EFFECT_TYPE type,double speed);

		/*!
		Display a resource defined in the mission's description.ext, the campaign's 
		description.ext or the global resource.cpp. 
		*/
		static void cutRsc(string resource,TITLE_EFFECT_TYPE type,double speed);

		/*!
		Displays text across the screen.
		If used along with cutText two different texts (in different type styles) can be shown at once. 
		*/
		static void titleText(string text,TITLE_EFFECT_TYPE type,double speed);

		/*!
		Terminate the effect in the given layer and set duration of the fade out phase to the given time. 
		*/
		static void cutFadeOut(double layer,double delay);

		/*!
		Resource title - Resource can be defined in Description.ext Also see cutRsc, with these two commands you can show two different resources at once. 
		*/
		static void titleRsc(string resource, TITLE_EFFECT_TYPE type,double speed);

		/*!
		Terminate the title effect and set duration of the fade out phase to the given time. 
		*/
		static void titleFadeOut(double duration);

		/*!
		Resource title - Preload data. The resource can be defined in the Description.ext file. 
		*/
		static bool preloadTitleRsc(string& resource, TITLE_EFFECT_TYPE type);

	};

};
#endif