/*
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 */

/*************************************************************************

Name:

VehicleUtilities.h

Purpose:

This file contains the declaration of the VehicleUtilities class.

Version Information:

Version		Date		Author and Comments
===========================================
1.0			02-04/2009	RMR: Original Implementation

2.0			10-02/2010  YFP: Added	void animateVehicle( Vehicle& vehicle ,string animationName,double phase)
									void createVehicleLocal(Vehicle& vehicle)
									Unit getEffectiveCommander(Vehicle& vehicle)
									void animateVehicle( Vehicle& vehicle ,string animationName,double phase)
2.01		10-02/2010	MHA: Comments Checked
2.02		14-06/2010  YFP: Added void createVehicle(Vehicle& , string, string)

2.03		02-07/2010  YFP: Methods added,
									void setManualControl(Vehicle&,bool)
									void setTurnWanted(Vehicle&,double)
									void setThrustWanted(Vehicle&,double)
2.04		05-07/2010  YFP: Methods added,
									void startEngine(Vehicle&,bool);
2.05		10-01/2011  CGS: Methods added getVehicleGroup(Vehicle& vehicle)
							Modified All createVehicle
								createAndAddWaypoint(Vehicle& vehicle, Waypoint& wp)
2.06		14-09/2011	NDB: Methods Added,
								vector<CrewPos> getCrewPosition(Vehicle& vehicle)
								vector<string> getMuzzles(Vehicle& vehicle, Turret::TurretPath& turretPath)
								bool isEngineDisabled(Vehicle& vehicle)
								void applyEngineDisable(Vehicle& vehicle, bool state)
								void applyTowParent(Vehicle& vehicle1, Vehicle& vehicle2)
								Vehicle getTowParent(Vehicle& vehicle)
								void applyBlockSeat(Vehicle& vehicle, VEHICLEASSIGNMENTTYPE type, int index = 0, bool isblock = true)
								void applyBlockSeat(Vehicle& vehicle, vector<int> index, bool isblock = true)
								vector<BLOCKEDSEAT> getBlockedSeats(Vehicle& vehicle)
								int getCargoIndex(Vehicle& vehicle, Unit& unit)
								Unit getUnitAtCargoIndex(Vehicle& vehicle, int index)
								void applyTurretLockOn(Vehicle& vehicle, Turret::TurretPath turretPath, position3D& position,bool trackHidden = false)
								position3D getTurretLockedOn(Vehicle& vehicle, Turret::TurretPath turretPath)
								double getLaserRange(Vehicle& vehicle, Unit& gunner)
								void applyLaserRange(Vehicle& vehicle, double range)
								void applyLaserRange(Vehicle& vehicle, Unit& gunner, double range)
								int getLasingStatus(Vehicle& vehicle, Unit& gunner)
								void applyOpticsOffset(Vehicle& vehicle, Turret::TurretPath turretPath, double azimuth, double elevation, bool transition = true)
								vector<double> getOpticsOffset(Vehicle& vehicle, Unit& gunner)
								vector<double> getOpticsOffset(Vehicle& vehicle, Turret::TurretPath turretPath)
								void applyIndicators(Vehicle& vehicle, bool left, bool hazard, bool right)
								void applyMaxFordingDepth(Vehicle& vehicle, double depth)
								double getMaxFordingDepth(Vehicle& vehicle)
								Unit getTurretUnit(Vehicle& vehicle, Turret::TurretPath turretPath)
2.07		19-09/2011	NDB: Added Methods
								void applyThrustLeftWanted(Vehicle& vehicle, double factor)
								void applyThrustRightWanted(Vehicle& vehicle, double factor)
								void applyFireArc(Vehicle& vehicle, Turret::TurretPath& turretPath, vector3D& direction, double sideRange, double verticalRange, bool relative)
								void applyCommanderOverride(Vehicle& vehicle, Unit& unit, Turret::TurretPath turretPath)
								void land(Vehicle& vehicle, LANDMODE mode)
								void sendToVehicleRadio(Vehicle& vehicle, string name)
								void vehicleChat(Vehicle& vehicle, string message)
								bool isEngineOn(Vehicle& vehicle)
								void commandGetOut(Unit& unit)
								void doGetOut(Unit& unit)
								void applyVehicleId(Vehicle& vehicle, int id)
								void respawnVehicle(Vehicle& vehicle, double delay = -1, int count = 0)
								bool isTurnedOut(Unit& unit)
								void autoAssignVehicle(Unit& unit)
2.08		26-09/2011	NDB: Added Methods
								void assignAsCommander(Vehicle& vehicle, Unit& unit, double delay)
								void assignAsDriver(Vehicle& vehicle, Unit& unit, double delay)
								void assignAsGunner(Vehicle& vehicle, Unit& unit, double delay)
								void assignAsCargo(Vehicle& vehicle, Unit& unit, double delay)
								void orderGetIn(vector<Unit> unitVec, bool order)
								void orderGetIn(Unit& unit, bool order)
								vector<WEAPONCARGO> getWeaponFromCargo(Vehicle& vehicle)
								vector<MAGAZINECARGO> getMagazineFromCargo(Vehicle& vehicle)
								vector<string> getMuzzles(Vehicle& vehicle)
								position3D getAimingPosition(Vehicle& vehicle, ControllableObject& target)
2.09		26-09/2011	NDB:	Added Method void commandMove(Vehicle& vehicle, position3D position)
2.10		04-11/2011	NDB: Added Mehthods
								bool isKindOf(Vehicle& vehicle, string typeName)
								float knowsAbout(Vehicle& vehicle, ControllableObject& target)
2.11		08-11-2011	SSD		void applyName(Vehicle& vehicle)
2.12        13-01-2012  RDJ     Add Methods
								static void landTo(Vehicle& vehicle, int port)
								static void assignToAirport(Vehicle& vehicle, int port)
2.12		18-01-2012	RDJ		Add the method islocked(Vehicle& vehicle)
2.13		18-01-2012	RDJ		Add the method void moveInCargoWithPosition(Unit& unit,Vehicle& vehicle, int cargoIndex)
2.14		19-01-2012  RDJ		Add the Method void groupLeaveVehicle(Group& group, Vehicle& vehicle);
2.15		19-01-2012	RDJ		Add the Method void groupLeaveVehicle(Unit& unit, Vehicle& vehicle);



/************************************************************************/ 

#ifndef VBS2FUSION_VEHICLE_UTILITIES_H
#define VBS2FUSION_VEHICLE_UTILITIES_H
/***********************************************************************/
/* INCLUDES
/***********************************************************************/
// STANDARD INCLUDES
#include <sstream>
#include <string>

// SIMCENTRIC INCLUDES
#include "position3D.h"
#include "VBS2Fusion.h"
#include "data/Vehicle.h"
#include "data/Unit.h"
#include "util/ExecutionUtilities.h"
#include "VBS2FusionAppContext.h"
#include "WaypointUtilities.h"
#include "ControllableObjectUtilities.h"
#include "data/vbs2Types.h"
#include "data/VehicleArmedModule.h"

/**********************************************************************/
/* END INCLUDES
/**********************************************************************/


namespace VBS2Fusion
{
	struct CrewPos
	{
		string type;
		position3D pos;
		int cargoID;
		Unit unit;
		Turret::TurretPath turret;
	};

	class VBS2FUSION_API VehicleUtilities : public ControllableObjectUtilities
	{
	public:	

		//********************************************************
		// Load utilities
		//********************************************************

		/*!
		Loads all units belonging to vehicle onto the vehicleList. Random
		aliases are assigned to each loaded unit. The network ID and name of 
		each unit is also loaded along with its role.  
		*/
		static void loadUnits(Vehicle& vehicle);		

		/*!
		Loads all Waypoints belonging to vehicle onto the waypointList. Random aliases
		are assigned to each waypoint. 
		*/
		static void loadWaypoints(Vehicle& vehicle);		

		////**************************************************************		

		/*!
		Loads all units and Waypoints belonging to the vehicle. 	
		*/
		static void loadVehicleUnitsAndWaypoints(Vehicle& vehicle);		

		/*!
		Updates the following:
		- The vehicle members list
		- The waypoint list. 
		- position
		- direction
		- isAlive
		- group name
		- damage
		- fuel
		- type
		- positionASL
		- the current waypoint. 

		This function uses the network ID of the object to access it within the game environment, so please
		ensure that either a registered alias is assigned to vehicle. 
		*/
		static void updateVehicle(Vehicle& vehicle);

		
		//********************************************************
		// position utilities
		//********************************************************


		/*!
		Returns the position of the vehicle in position3D format. 
		*/
		static position3D getPosition(Vehicle& object);

		/*!
		Updates the position of the vehicle.  
		*/
		static void updatePosition(Vehicle& vehicle);

		/*!
		Applies the position defined by position to the vehicle. Value is 
		not updated on the vehicle variable. Use update to verify that changes have been made. 
		*/
		static void applyPosition(Vehicle& vehicle, position3D position);

		/*!
		Applies the position defined by vehicle.getPosition() to the vehicle. 
		*/
		static void applyPosition(Vehicle& vehicle);


		//********************************************************
		// Direction utilities
		//********************************************************

		/*!
		Returns the direction of the vehicle. 
		*/
		static double getDirection(Vehicle& vehicle);

		/*!
		Updates the direction of the vehicle. 
		*/
		static void updateDirection(Vehicle& unit);

		/*!
		Applies the direction defined by direction to the vehicle. Value is 
		not updated on the vehicle variable. Use update to verify that changes have been made. 
		*/
		static void applyDirection(Vehicle& vehicle, double direction);

		/*!
		Applies the direction defined by vehicle.getDir() to the vehicle. 
		*/
		static void applyDirection(Vehicle& vehicle);


	
		//********************************************************
		// Group utilities
		//********************************************************	


		static Group getVehicleGroup(Vehicle& vehicle);

		/*!
		Returns the name of the group the vehicle belongs to. 
		*/
		static string getGroup(Vehicle& vehicle);		

		/*!
		Updates the name of the group the vehicle belongs to. 
		*/
		static void updateGroup(Vehicle& unit);


		//********************************************************
		// Vehicle alive utilities
		//********************************************************	
	
		/*!
		Updates the alive status of the vehicle. 
		*/
		static void updateAlive(Vehicle& unit);


		//********************************************************
		// Damage utilities
		//********************************************************	
			
		/*!
		Returns the current damage level of the vehicle. 
		*/
		static double getDamage(Vehicle& vehicle);

		/*!
		Updates the current damage level of the vehicle. 
		*/
		static void updateDamage(Vehicle& unit);

		/*!
		Applies the damage level specified by damage to the vehicle. Value is 
		not updated on the vehicle variable. Use update to verify that changes have been made. 
		*/
		static void applyDamage(Vehicle& vehicle, double damage);

		/*!
		Applies the damage level specified by vehicle.getDamage() to the vehicle. 
		*/
		static void applyDamage(Vehicle& vehicle);

	

		//********************************************************
		// Fuel utilities
		//********************************************************		

		/*!
		Returns the current fuel level of the vehicle. 
		*/
		static double getFuel(Vehicle& vehicle);

		/*!
		Updates the current fuel level of the vehicle. 
		*/
		static void updateFuel(Vehicle& unit);

		/*!
		Applies the fuel level specified by fuel to the vehicle. Value is 
		not updated on the vehicle variable. Use update to verify that changes have been made. 
		*/
		static void applyFuel(Vehicle& vehicle, double fuel);		

		/*!
		Applies the fuel level specified by vehicle.getFuel() to the vehicle. 
		*/
		static void applyFuel(Vehicle& vehicle);

		//********************************************************
		// Type utilities
		//********************************************************		

		/*!
		Returns the type of the vehicle. 
		*/
		//static string getType(Vehicle& vehicle);

		/*!
		Updates the type of the vehicle. 
		*/
		//static void updateType(Vehicle& unit);


		//********************************************************
		// Waypoint utilities
		//********************************************************		

		/*!
		Creates a new waypoint (i.e. creates it in the game environment) and
		adds it to the vehicle. 
		*/
		static void createAndAddWaypoint(Vehicle& vehicle, Waypoint& wp);

		/*!
		Returns the index of the current waypoint. 
		*/
		static int getCurrentWaypoint(Vehicle& vehicle);			

		/*!
		Updates the index of the current waypoint. 
		*/
		static void updateCurrentWaypoint(Vehicle& vehicle);


		//********************************************************
		// Vehicle create utilities
		//********************************************************	

		/*!
		Creates a new vehicle as defined by the parameters present in vehicle. 

		- If no alias is set, a random alias is assigned. 
		- The type of vehicle defined by vehicle.getType() is used.
		- creates the vehicle at the position defined by vehicle.getPosition(). 
		- If no group is specified, a new group is created and the vehicle is added to that group. 
		*/
		static void createVehicle(Vehicle& vehicle);

		/*!
		Create a new vehicle as defined by parameters of the vehicle. 
		If the string of markers array contains several marker names, the position of a random one is used.
		Otherwise, the given position is used. The vehicle is placed inside a circle with this position 
		as center and placement as its radius. 
		Special properties can be:"NONE", "FLY" and "FORM".
		*/
		static void createVehicle(Vehicle& vehicle, string markerNames, string specialProperties);

		/*!
		Creates a new vehicle as defined by the parameters present in the vehicle locally.
		*/
		static void createVehicleLocal(Vehicle& vehicle);
		/*!
		Creates a new air plane as defined by the parameters present in vehicle. 

		- If no alias is set, a random alias is assigned. 		
		- creates the vehicle at the position defined by vehicle.getPosition(). 
		- If no group is specified, a new group is created and the vehicle is added to that group. 
		*/
		//static void createAirPlane(Vehicle& vehicle, vbs2Type::Airplane::AIRPLANE planeIndex);	

		/*!
		Creates a new Car as defined by the parameters present in vehicle. 

		- If no alias is set, a random alias is assigned. 		
		- creates the vehicle at the position defined by vehicle.getPosition(). 
		- If no group is specified, a new group is created and the vehicle is added to that group. 
		*/
		//static void createCar(Vehicle& vehicle, vbs2Type::Car::CAR carIndex);	


		/*!
		Creates a new Helicopter as defined by the parameters present in vehicle. 

		- If no alias is set, a random alias is assigned. 		
		- creates the vehicle at the position defined by vehicle.getPosition(). 
		- If no group is specified, a new group is created and the vehicle is added to that group. 
		*/
		//static void createHelicopter(Vehicle& vehicle, vbs2Type::Helicopter::HELICOPTER helicopterIndex);	


		/*!
		Creates a new Tank as defined by the parameters present in vehicle. 

		- If no alias is set, a random alias is assigned. 		
		- creates the vehicle at the position defined by vehicle.getPosition(). 
		- If no group is specified, a new group is created and the vehicle is added to that group. 
		*/
		//static void createTank(Vehicle& vehicle, vbs2Type::Tank::TANK tankIndex);	

		/*!
		Creates a new Vasi as defined by the parameters present in vehicle. 

		- If no alias is set, a random alias is assigned. 		
		- creates the vehicle at the position defined by vehicle.getPosition(). 
		- If no group is specified, a new group is created and the vehicle is added to that group. 
		*/
		//static void createVasi(Vehicle& vehicle, vbs2Type::Vasi::VASI vasiIndex);	

		//********************************************************
		// Vehicle delete utilities
		//********************************************************	

		/*!
		deletes the vehicle as defined by the parameters of vehicle. 
		*/
		static void deleteVehicle(Vehicle& vehicle);

		//********************************************************
		// Vehicle group utilities
		//********************************************************	

		/*!
		Adds vehicle to the group defined by group. The group name of vehicle is changed
		to vehicle.setGroup(). 
		*/
		static void addToGroup(Vehicle& vehicle, Group& group);		


		/*!
		returns the commander of the group. 
		*/
		static Unit getEffectiveCommander(Vehicle& vehicle);

		//********************************************************
		// Vehicle move in utilities
		//********************************************************

		/*!
		Creates a new unit as defined by unit and moves the unit
		into vehicle as the driver. 		
		*/
		static void createAndMoveInAndAsDriver(Vehicle& vehicle, Unit& unit);

		/*!
		Moves in an already created unit into vehicle as driver. 

		This function uses the network ID of the object to access it within the game environment, so please
		ensure that either a registered alias is assigned to vehicle. 

		Deprecated. Use void applyMoveInAsDriver(Vehicle& vehicle, Unit& unit)
		*/
		VBS2FUSION_DPR(UVHC001) static void moveInAndAsDriver(Vehicle& vehicle, Unit& unit);	

		/*!
		Creates a new unit as defined by unit and moves the unit
		into vehicle as turret.

		It takes the default turret
		[1,0]: second sub-turret of first main turret. 
		*/
		static void createAndMoveInAndAsTurret(Vehicle& vehicle, Unit& unit);

		
		/*!
		Moves in an already created unit into vehicle as turret.

		This function uses the network ID of the object to access it 
		within the game environment, so please ensure that either a 
		registered alias is assigned to vehicle.

		It take default turret
		[0,1]: second sub-turret of first main turret.

		Deprecated. Use void applyMoveInAsTurret(Vehicle& vehicle, Unit& unit)
		*/
		VBS2FUSION_DPR(UVHC004) static void moveInAndAsTurret(Vehicle& vehicle, Unit& unit);

		/*!
		Creates a new unit as defined by unit and moves the unit
		into vehicle as turret.

		Available turrets can be determined by the getCrewPos command, and is defined as an array, containing either one or two positions definitions, 
		e.g.
		[0]: first turret (no sub-turrets exist).
		[0,0]: first sub-turret of first main turret.
		[1,0]: second sub-turret of first main turret. 
		*/
		static void createAndMoveInAndAsTurret(Vehicle& vehicle, Unit& unit, string turrntPath);

		
		/*!
		Creates a new unit as defined by unit and moves the unit
		into vehicle as turret.

		Available turrets can be determined by the getCrewPos command, and is defined as an array, containing either one or two positions definitions, 
		e.g.
		[0]: first turret (no sub-turrets exist).
		[0,0]: first sub-turret of first main turret.
		[1,0]: second sub-turret of first main turret. 
		*/
		static void createAndMoveInAndAsTurret(Vehicle& vehicle, Unit& unit, Turret turretPath);

		/*!
		Moves in an already created unit into vehicle as turret.

		This function uses the network ID of the object to access it 
		within the game environment, so please ensure that either a 
		registered alias is assigned to vehicle.

		Available turrets can be determined by the getCrewPos command, and is defined as an array, containing either one or two positions definitions, 
		e.g.
		[0]: first turret (no sub-turrets exist).
		[0,0]: first sub-turret of first main turret.
		[1,0]: first sub-turret of second main turret.

		Deprecated. Use void applyMoveInAsTurret(Vehicle& vehicle, Unit& unit, string turretPath)
		*/
		VBS2FUSION_DPR(UVHC002) static void moveInAndAsTurret(Vehicle& vehicle, Unit& unit, string turretPath);

		/*!
		Moves in an already created unit into vehicle as turret.

		This function uses the network ID of the object to access it 
		within the game environment, so please ensure that either a 
		registered alias is assigned to vehicle.

		Available turrets can be determined by the getCrewPos command, and is defined as an array, containing either one or two positions definitions, 
		e.g.
		[0]: first turret (no sub-turrets exist).
		[0,0]: first sub-turret of first main turret.
		[1,0]: first sub-turret of second main turret.

		Deprecated. Use void applyMoveInAsTurret(Vehicle& vehicle, Unit& unit, Turret turretPath).
		*/
		VBS2FUSION_DPR(UVHC003) static void moveInAndAsTurret(Vehicle& vehicle, Unit& unit, Turret turretPath);

		/*!
		Creates a new unit as defined by unit and moves the unit
		into vehicle as cargo. 		
		*/
		static void createAndMoveInAndAsCargo(Vehicle& vehicle, Unit& unit);

		/*!
		Moves in an already created unit into vehicle as cargo. 

		This function uses the network ID of the object to access it 
		within the game environment, so please ensure that either a 
		registered alias is assigned to vehicle. 

		Deprecated. Use void applyMoveInAsCargo(Vehicle& vehicle, Unit& unit)
		*/
		VBS2FUSION_DPR(UVHC005) static void moveInAndAsCargo(Vehicle& vehicle, Unit& unit);		

		/*!
		Creates a new unit as defined by unit and moves the unit
		into vehicle as the commander. 
		*/
		static void createAndMoveInAndAsCommander(Vehicle& vehicle, Unit& unit);

		/*!
		Moves in an already created unit into vehicle as commander. 

		This function uses the network ID of the object to access it 
		within the game environment, so please ensure that either a 
		registered alias is assigned to vehicle. 		

		Deprecated. Use void applyMoveInAsCommander(Vehicle& vehicle, Unit& unit)
		*/
		VBS2FUSION_DPR(UVHC007) static void moveInAndAsCommander(Vehicle& vehicle, Unit& unit);

		/*!
		Creates a new unit as defined by unit and moves the unit
		into vehicle as gunner. 
		*/
		static void createAndMoveInAndAsGunner(Vehicle& vehicle, Unit& unit);

		/*!
		Moves in an already created unit into vehicle as gunner. 

		This function uses the network ID of the object to access it 
		within the game environment, so please ensure that either a 
		registered alias is assigned to vehicle. 

		Deprecated. Use void applyMoveInAsGunner(Vehicle& vehicle, Unit& unit)
		*/
		VBS2FUSION_DPR(UVHC006) static void moveInAndAsGunner(Vehicle& vehicle, Unit& unit);


		//********************************************************
		// Vehicle leave utilities
		//********************************************************

		/*!
		Orders unit to leave vehicle. 

		This function uses the network ID of the object to access it 
		within the game environment, so please ensure that either a 
		registered alias is assigned to vehicle. 

		Deprecated. Use void applyUnitLeave(Vehicle& vehicle, Unit& unit)
		*/
		VBS2FUSION_DPR(UVHC009) static void leaveVehicle(Vehicle& vehicle, Unit& unit);


		/*!
		Orders unit with ID defined by unit.getID() to leave vehicle.

		This function uses the network ID of the object to access it 
		within the game environment, so please ensure that either a 
		registered alias is assigned to vehicle. 

		Deprecated. Use void applyUnitLeave(Vehicle& vehicle, Unit& unit)
		*/
		VBS2FUSION_DPR(UVHC009) static void leaveVehicleByID(Vehicle& vehicle, Unit& unit); 

		/*!
		Orders unit with Name defined by unit.getName() to leave vehicle.

		This function uses the network ID of the object to access it 
		within the game environment, so please ensure that either a 
		registered alias is assigned to vehicle. 

		Deprecated. Use void applyUnitLeave(Vehicle& vehicle, Unit& unit)
		*/
		VBS2FUSION_DPR(UVHC009) static void leaveVehicleByName(Vehicle& vehicle, Unit& unit); 


		/*!
		Orders unit with Alias defined by unit.getAlias() to leave vehicle.

		This function uses the network ID of the object to access it 
		within the game environment, so please ensure that either a 
		registered alias is assigned to vehicle. 

		Deprecated. Use void applyUnitLeave(Vehicle& vehicle, Unit& unit)
		*/
		VBS2FUSION_DPR(UVHC009) static void leaveVehicleByAlias(Vehicle& vehicle, Unit& unit);		

		//*******************************************************/

		/*!
		Activate and deactivate the engine of the vehicle. If state is true, 
		engine will be on and it will stop when state is false.

		Deprecated. Use void applyEngineStart(Vehicle& vehicle , bool state)
		*/
		VBS2FUSION_DPR(UVHC008) static void startEngine(Vehicle& vehicle , bool state);

		/*!
		Enable/ disable manual control of the vehicle.
		if control is TRUE  vehicle AI disabled and it can be controlled 
		by setTurnWanted & setThrustWanted functions.  

		Deprecated. Use void applyManualControl(Vehicle& vehicle , bool control)
		*/
		VBS2FUSION_DPR(UVHC010) static void setManualControl(Vehicle& vehicle , bool control);

		/*!
		set the turn wanted for the vehicle specified by the factor value.
		manual control of the vehicle should be enabled first. 

		Deprecated. Use void applyTurnWanted(Vehicle& vehicle, double factor)
		*/
		VBS2FUSION_DPR(UVHC011) static void setTurnWanted(Vehicle& vehicle, double factor);

		/*!
		set the thrust wanted for the vehicle to passed in. 
		manual control of the vehicle should be enabled first.

		Deprecated. Use void applyThrustWanted(Vehicle& vehicle , double factor)
		*/
		VBS2FUSION_DPR(UVHC012) static void setThrustWanted(Vehicle& vehicle , double factor);

		//********************************************************
		// Animate utilities
		//********************************************************

		/*!
		Animate Vehicle. 
		animationName - The name of the animation
		phase - the phase value is between 0 and 1.  

		Deprecated. Use void applyAnimation(Vehicle& vehicle, string animationName, double phase)
		*/
		VBS2FUSION_DPR(UVHC020) static void animateVehicle( Vehicle& vehicle ,string animationName,double phase);

		/*!
		Returns current gear of the vehicle. 
		*/
		static int getCurrentGear(Vehicle& vehicle);


		/*!
		Returns current RPM of the vehicle. 
		*/
		static float getCurrentRPM(Vehicle& vehicle);
		
		/*!
		Returns current RPM of the vehicle. 
		*/
		static double getCurrentRPMEx(Vehicle& vehicle);
		
		/*!
		Returns engine strength coefficient of the vehicle.
		*/
		static double getEngineStrengthCoefficient(Vehicle& vehicle);

		/*!
		Applies vehicle engine strength coefficient.
		*/
		static void applyEngineStrengthCoefficient(Vehicle& vehicle);

		/*!
		Applies specified engine strength coefficient value to the vehicle.    
		*/
		static void applyEngineStrengthCoefficient(Vehicle& vehicle , double strengthCoeff);

		/*!
		Returns maximum speed limit of the vehicle.
		*/
		static double getMaximumSpeedLimit(Vehicle& vehicle);

		/*!
		Set vehicle's maximum speed. Maximum speed limit should be in Km/h . 
		If maximum speed limit is less than 0. Then the vehicle's maximum speed limit 
		sets to its default value.
		*/
		static void applyMaximumSpeedLimit(Vehicle& vehicle , double maxSpeedLimit);
		
		/*!
		Returns the name of the Vehicle's primary weapon (an empty string if there is none).	
		*/
		static string getPrimaryWeapon(Vehicle& vehicle);

		/*!
		Updates the primary weapon of the Vehicle. 
		*/
		static void updatePrimaryWeapon(Vehicle& vehicle);

		/*
		Updates the list of weapon types assigned to the Vehicle. 
		Each weapon is assigned with ammo value of 0. 

		Note: This function does not update the ammo amounts on
		the weapons magazines. Use updateWeaponAmmo to update the weapon types with ammo.
		*/
		static void updateWeaponTypes(Vehicle& vehicle);

		/*!
		Select the Weapon given by the muzzle Name.
		Deprecated. Use void applyWeaponSelection(Vehicle& vehicle, string muzzleName)
		*/
		VBS2FUSION_DPR(UVHC023) static void selectWeapon(Vehicle& vehicle, string muzzleName);

		/*
		Updates the amount of ammo left in the primary magazine of each assigned 
		weapon type. Uses the list defined by the weapons list within VehicleArmedModule, 
		and therefore will only load ammo information for each weapon on that list. 
		Should be used after updateWeaponTypes is called. 	
		*/
		static void updateWeaponAmmo(Vehicle& vehicle);

		/*!
		Add magazine to the Vehicle.
		Deprecated. Use void applyMagazineAddition(Vehicle& vehicle, string magName)
		*/
		VBS2FUSION_DPR(UVHC025) static void addMagazine(Vehicle& vehicle, string name);

		/*!
		Count number of magazines available in given Vehicle object.

		Deprecated. Use int getMagazineCount(Vehicle& vehicle)
		*/
		VBS2FUSION_DPR(UVHC026) static int countMagazines(Vehicle& vehicle);

		/*!
		Updates the list of magazines assigned to a vehicle's Armed Module. This function loads all 
		magazines assigned to the vehicle from VBS2 and adds a new Magazine object to Magazine list

		Note: This function does not update the ammo amount within Magazine list. 
		So within MagazineList ammo count would be zero.Use updateMagazineAmmo to update this list. 
		*/
		static void updateMagazineTypes(Vehicle& vehicle);

		/*!
		Updates the magazine ammo count & magazine name inside the vehicleArmedModule's MagazineList
		therefore update the magazine list with required details. This function is enough to update
		MagazineList correctly. No need to use updateMagazineTypes function before using this function.
		*/
		static void updateMagazineAmmo(Vehicle& vehicle);

		/*!
		Load Turrets available in the Vehicle to armed module. 
		*/
		static void updateTurrets(Vehicle& vehicle);

		//----------------------------------------------------------------------------

		/*!
		Get the elevation of the weapon in specified turret of the Armed Vehicle from VBS2 Environment.
		turret object should have the correct turret path.
		*/
		static double getElevation(Vehicle& vehicle, Turret& turret);

		/*! 
		Get azimuth of the weapon in specified turret of the armed vehicle from VBS2 Environment.  
		turret object should have the correct turret path.
		*/
		static double getAzimuth(Vehicle& vehicle, Turret& turret);
	
		/*!
		Returns the weapons position in model space. Weapon should have correct name.	
		*/
		static position3D getWeaponPosition(Vehicle& vehicle, Weapon& weapon);

		/*!
		Returns the weapons position in model space. turret should have the correct turret path.
		*/
		static position3D getWeaponPosition(Vehicle& vehicle, Turret& turret);

		/*!
		Returns the weapon firing point for a selected muzzle in model space.	
		*/
		static position3D getWeaponPoint(Vehicle& vehicle, Weapon& weapon);

		/*!
		Returns the weapon firing point for a selected muzzle in model space.	
		*/
		static position3D getWeaponPoint(Vehicle& vehicle, Turret& turret, Weapon& weapon);

		/*! 
		Get the aiming direction of the specified Weapon of the turret in armed vehicle.
		This method returns a unit vector of the direction. 
		*/
		static position3D getWeaponDirectionVector(Vehicle& vehicle, Turret& turret);

		/*! 
		set reload state of the weapon.
		Deprecated. Use void applyWeaponState(Vehicle& vehicle, Weapon& weapon, bool reloadStatus, double reloadTime)
		*/
		VBS2FUSION_DPR(UVHC028) static void setWeaponState(Vehicle& vehicle,Weapon& weapon, bool reloadStatus,double reloadTime);

		/*!
		Returns the Weapon Object with Current weapon details.	
		*/
		static Weapon getWeaponState(Vehicle& vehicle, Turret::TurretPath& tPath);

		/*!
		Returns the list of objects the weapon is aiming at.
		*/
		static list<ControllableObject> getWeaponAimingTargets(Vehicle& vehicle, string weaponName);

		/*!
		Returns the list of objects the weapon is aiming at. Weapon object should have correct weapon name.
		*/
		static list<ControllableObject> getWeaponAimingTargets(Vehicle& vehicle, Weapon& weapon);

		/*!
		Remove the specified magazine from the Vehicle.	
		Deprecated. Use void applyMagazineRemove(Vehicle& vehicle, string magName)
		*/
		VBS2FUSION_DPR(UVHC031) static void removeMagazine(Vehicle& vehicle, string name);

		/*!
		Remove all magazine from the vehicle.	
		Deprecated. Use void applyMagazineRemoveAll(Vehicle& vehicle, string magName)
		*/
		VBS2FUSION_DPR(UVHC032) static void removeallMagazines(Vehicle& vehicle, string name);

		/*!
		Add Weapon to the Vehicle. weapon slots are filled, any further addWeapon commands are ignored. 
		To ensure that the weapon is loaded at the start of the mission, at least one magazine should 
		be added, before adding the weapon.

		Deprecated. Use void applyWeaponAddition(Vehicle& vehicle, string name)
		*/
		VBS2FUSION_DPR(UVHC027) static void addWeapon(Vehicle& vehicle, string name);

		/*!
		Remove the specified Weapon from the unit.	

		Deprecated. Use void applyWeaponRemove(Vehicle& vehicle, string name)
		*/
		VBS2FUSION_DPR(UVHC029) static void removeWeapon(Vehicle& vehicle, string name);

		/*!
		Tells if a unit has the given weapon.	

		Deprecated. Use void isWeaponAvailable(Vehicle& vehicle, string name)
		*/
		VBS2FUSION_DPR(UVHC030) static bool hasWeapon(Vehicle& vehicle, string name);

		/*!
		Sets the direction of the Turret Weapon according to azimuth& elevation. 
		Deprecated. Use void applyWeaponDirection(Vehicle& vehicle, Turret& turret, double azimuth, double elevation, bool transition)
		*/
		VBS2FUSION_DPR(UVHC033) static void setWeaponDirection(Vehicle& vehicle, Turret& turret, double azimuth, double elevation, bool transition);

		/*!
		Returns how much ammunition (compared to a full state defined by the unit type) the vehicle has. 
		Value would be between 0 to 1.	
		*/
		static float getAmmoRatio(Vehicle& vehicle);

		/*!
		Sets how much ammunition (compared to a full state defined by the unit type) the vehicle has. 
		The value ranges from 0 to 1.	
		Deprecated. Use void applyVehicleAmmo(Vehicle& vehicle, double ammovalue)
		*/
		VBS2FUSION_DPR(UVHC035) static void setVehicleAmmo(Vehicle& vehicle, float ammovalue);

		/*!
		Sets how much ammunition (compared to a full state defined by the unit type) the vehicle has. 
		The value ranges from 0 to 1.	
		Deprecated. Use void applyVehicleAmmo(Vehicle& vehicle, double ammovalue)
		*/
		VBS2FUSION_DPR(UVHC035) static void setVehicleAmmo(Vehicle& vehicle, double ammovalue);

		/*!
		Disables gunner input at the turret.
		Deprecated. Use void applyGunnerInputDisable(Vehicle& vehicle, Turret& turret, bool status = true)
		*/
		VBS2FUSION_DPR(UVHC034) static void disableGunnerInput(Vehicle& vehicle, Turret& turret, bool status = true);

		/*!
		Add weapons to the cargo space of vehicles, which can be taken out by infantry units. 
		Once the weapon cargo space is filled up, any further addWeaponCargo commands are ignored.

		Deprecated. Use void applyWeaponCargo(Vehicle& vehicle, string weaponName, int count)
		*/
		VBS2FUSION_DPR(UVHC036) static void addWeaponToCargo(Vehicle& vehicle, string weaponName, int count);

		/*!
		Add magazines to the cargo space of vehicles, which can be taken out by infantry units. 
		Once the magazine cargo space is filled up, any further addMagazineCargo commands are ignored.

		Deprecated. Use void applyMagazineCargo(Vehicle& vehicle, string magName, int count)
		*/
		VBS2FUSION_DPR(UVHC037) static void addMagazineToCargo(Vehicle& vehicle, string magName, int count);

		/*!
		Remove all weapons from the given vehicle's weapon cargo space.
		Deprecated. Use void applyWeaponCargoRemoveAll(Vehicle& vehicle)
		*/
		VBS2FUSION_DPR(UVHC038) static void clearAllWeaponFromCargo(Vehicle& vehicle);

		/*!
		Remove all magazines from the given vehicle's magazine cargo space.
		Deprecated. Use void applyMagazineCargoRemoveAll(Vehicle& vehicle)
		*/
		VBS2FUSION_DPR(UVHC039) static void clearAllMagazineFromCargo(Vehicle& vehicle);

		/*!
		Enables or Disables firing on current weapon in the selected turret. If true, the unit's current 
		weapon cannot be fired.
		Deprecated. Use void applyWeaponSafety(Vehicle& vehicle, Turret& turret, bool safe)
		*/
		VBS2FUSION_DPR(UVHC040) static void setWeaponSafety(Vehicle& vehicle, Turret& turret, bool safe);

		/*!
		Return the current status of the weapon safety in the selected turret.
		*/
		static bool getWeaponSafety(Vehicle& vehicle, Turret& turret);

		/*!
		Updates all the Armed properties of the unit:   
		*/
		static void updateVehicleArmedProperties(Vehicle& vehicle);

		/*!
		Order the Armed Vehicle to fire on the given target (via the radio).   
		*/
		static void commandFire(Vehicle& vehicle, ControllableObject& co);

		/*!
		Order the Armed Vehicle to fire on the given target (without radio messages).   
		*/
		static void doFire(Vehicle& vehicle, ControllableObject& co);

		/*!
		Forces the Armed Vehicle to fire from the specifically named weapon. 
		Deprecated. Use void applyFire(Vehicle& vehicle, string weaponName)
		*/
		VBS2FUSION_DPR(UVHC072) static void fire(Vehicle& vehicle, string weaponName);

		/*!
		Forces the Armed Vehicle to fire according to the parameters given.  
		Deprecated. Use void applyFire(Vehicle& vehicle, string muzzleName, string modeName)
		*/
		VBS2FUSION_DPR(UVHC073) static void fire(Vehicle& vehicle, string muzzleName, string modeName);

		/*!
		Forces the Armed Vehicle to fire according to the parameters given.   
		*/
		static void fire(Vehicle& vehicle, string muzzleName, string modeName, string magazineName, position3D& pos, bool aim);

		/*!
		Apply the network shell mode. Normally shells are locally simulated following a fired event.
		When network shell mode is set, the simulation is broadcast to the network. 
		Deprecated. Use void applyNetworkShellMode(Vehicle& vehicle, bool mode)
		*/
		VBS2FUSION_DPR(UVHC041) static void setNetworkShellMode(Vehicle& vehicle,bool mode);

	/*!
	Returns the gunner of a vehicle. 
	*/
	static Unit getGunner(Vehicle& vehicle);

	/*!
	Returns the commander of a vehicle. 
	*/
	static Unit getCommander(Vehicle& vehicle);

	/*!
	Returns the driver of a vehicle. 
	*/
	static Unit getDriver(Vehicle& vehicle);

	/*!
	Returns a list of arrays, containing information about each seat in the vehicle:

	type - Proxy type. Can be "driver", "gunner", "commander" or "cargo".
	position - Seat position in model space (i.e. relative to the vehicle).
	cargoID - Index of cargo position.
	unit - Unit occupying that seat. objNull if empty.
	turret - Turret path. For non-gunner positions this element is not returned. 
	*/
	static vector<CrewPos> getCrewPosition(Vehicle& vehicle);

	/*!
	Return all the muzzles of a turret
	*/
	static vector<string> getMuzzles(Vehicle& vehicle, Turret::TurretPath& turretPath);

	/*!
	Returns whether a vehicle's engine was disabled
	*/
	static bool isEngineDisabled(Vehicle& vehicle);


	/*!
	Disables/enables the engine of a land vehicle.

	A disabled engine will keep the vehicle from moving, but will not affect its damage status.
	Engines that have been destroyed by in-game actions (e.g. by driving through water), can also be re-enabled with this command.
	Disabled engines can be fixed by a repair truck.

	state - true to disable the engine on, false to enable it.
	*/
	static void applyEngineDisable(Vehicle& vehicle, bool state);

#ifdef DEVELOPMENT
	/*!
	Causes a vehicle (even if it's empty) to adjust its steering and braking to follow the tow vehicle.
	vehicle1 - Vehicle that is being towed
	vehicle2 - Vehicle that is towing vehcle1. If set to objNull, automatic steering/braking is disabled.
	*/
	static void applyTowParent(Vehicle& vehicle1, Vehicle& vehicle2);

	/*!
	Returns the vehicle that is defines as the tow vehicle by applyTowParent. 
	*/
	static Vehicle getTowParent(Vehicle& vehicle);
#endif

	/*!
	Marks a vehicle seat as blocked, so that neither AI nor players can use it.

	Blocking already used seats will not remove the occupying unit.
	A third optional boolean parameter can be used to unblock the seat. 

	vehicle: Object
	seat - Type of seat (proxy): Either "Driver", "Cargo" or "Commander" (the latter means a commander seat, not the commanding unit)
	index - Index of cargo position or turret definition. Optional for "Driver" and "Commander".
	blocked - Whether to block the seat or not. Optional (default is true).
	*/
	static void applyBlockSeat(Vehicle& vehicle, VEHICLEASSIGNMENTTYPE type, int index = 0, bool isblock = true);

	/*!
	Marks a vehicle seat as blocked, so that neither AI nor players can use it.

	Blocking already used seats will not remove the occupying unit.
	A third optional boolean parameter can be used to unblock the seat. 
	It is only for type of "Turret"
	vehicle: Object
	index - turret definition. 
	blocked - Whether to block the seat or not. Optional (default is true).
	*/
	static void applyBlockSeat(Vehicle& vehicle, vector<int> index, bool isblock = true);

	/*!
	Returns list of seats that were blocked
	*/
	static vector<BLOCKEDSEAT> getBlockedSeats(Vehicle& vehicle);

	/*!
	Returns the position of the unit in cargo.
	The first cargo position is 0. If unit is not in vehicle, -1 is returned.
	*/
	static int getCargoIndex(Vehicle& vehicle, Unit& unit);

	/*!
	Returns the unit for the specified cargo index.
	If the tested cargo position is empty, objNull is returned.
	*/
	static Unit getUnitAtCargoIndex(Vehicle& vehicle, int index);

#ifdef DEVELOPMENT
	/*!
	Locks a turret onto an object or ASL position

	vehicle - vehicle to lock
	turretpath - turret to be locked
	co - What to lock on to.
	trackhidden - (optional, default: false)Continue tracking unit after entering a vehicle? 
	*/
	static void applyTurretLockOn(Vehicle& vehicle, Turret::TurretPath turretPath, ControllableObject& co,bool trackHidden = false);

#endif

	/*!
	Locks a turret onto an object or ASL position

	vehicle - vehicle to lock
	turretpath - turret to be locked
	position - What to lock on to. Locking can be disabled by passing [0,0,0]
	*/
	static void applyTurretLockOn(Vehicle& vehicle, Turret::TurretPath turretPath, position3D& position);


	/*!
	Returns the target position a turret is locked on.
	Returns the position of the object if the turret is locked on an object.
	[0,0,0] if it is not locked. 
	*/
	static position3D getTurretLockedOn(Vehicle& vehicle, Turret::TurretPath turretPath);

	/*!
	Returns the currently lased range. (Does not work on water.)
	returns -1 if gunner is not operating a range finder weapon
	*/
	static double getLaserRange(Vehicle& vehicle, Unit& gunner);

	/*!
	Overrides the actual laser measurements (which are used to calculate the ballistic calculations) with a value provided by this command.
	This value will be shown in the gunner's optics view, until he does another manual lasing.

	vehicle - 
	gunner - Unit that is manning the affected weapon. 
	range - Distance (in meters) to be used in ballistics calculations.
	*/
	static void applyLaserRange(Vehicle& vehicle, Unit& gunner, double range);

	/*!
	Sets a new offset between weapon orientation and optics. Note: Lasing will change the elevation offset.
	This function works only if the gunner is the player.
	*/
	static void applyOpticsOffset(Vehicle& vehicle, Turret::TurretPath turretPath, double azimuth, double elevation, bool transition = true);

	/*!
	Returns the offset between weapon orientation and optics.
	*/
	static vector<double> getOpticsOffset(Vehicle& vehicle, Unit& gunner);

	/*!
	Returns the current status of laser rangefinder (LRF)
	(corresponds with the in-game status color):

	0 - lasing not available (unit or vehicle position not equipped with LRF)

	1 - [red]: not lased (distance cannot be determined, e.g. water, air)

	2 - [yellow]: lasing in progress (distance is being determined)

	3 - [green]: lased (distance has been determined and displayed) 
	*/
	static int getLasingStatus(Vehicle& vehicle, Unit& gunner);

	/*!
	Controls turn indicators and hazard lights on vehicles (true=on, false=off). 

	left - Turns left indicator lights on and off. (If true, hazard light will be turned off.)
	hazard - Turns hazard lights on and off. (If true, indicator light will be turned off.)
	right - Turns right indicator lights on and off. (If true, hazard light will be turned off.)
	*/
	static void applyIndicators(Vehicle& vehicle, bool left, bool hazard, bool right);

	/*!
	Sets the new fording depth in meters for objects of the classes motorcycle, helicopter, tank and car. 
	Setting the max fording depth to -1 will cause the engine to revert to the default configuration value.
	*/
	static void applyMaxFordingDepth(Vehicle& vehicle, double depth);

	/*!
	Returns the fording depth of a vehicle in meters. 
	*/
	static double getMaxFordingDepth(Vehicle& vehicle);

	/*!
	Returns the gunner in the turret path specified. 
	*/
	static Unit getTurretUnit(Vehicle& vehicle, Turret::TurretPath turretPath);

	/*!
	Brings an aircraft to a flying altitude. Either planes or helicopters will take off realistically to reach this height
	Deprecated. Use double applyAirborne(Vehicle& vehicle)
	*/
    VBS2FUSION_DPR(UVHC042) static double makeAirborne(Vehicle& vehicle);
 
	 /*!
	 Returns whether or not the aircraft is currently airborne. 
	 */
	 static bool isAirborne(Vehicle& vehicle);


	 /*!
	 Checks whether the light of a vehicle is turned on.
	 (If used with other objects than vehicles may always return true or false.) 
	 */
	 static bool isLightOn(Vehicle& vehicle);

	 /*!
	 Returns either the default fly-in height of aircraft (independently of its current altitude), or the value that was set via flyInHeight.
	 */
	 static double getFlyInHeight(Vehicle& vehicle);


	 /*!
	 Sets the flying altitude for aircraft.
	 Avoid too low altitudes, as helicopters and planes won't evade trees and obstacles on the ground.
	 The default flying altitude depends on the "flyInHeight" definition in the vehicle's configuration. 

	 Deprecated. Use void applyHeightFlyIn(Vehicle& vehicle, double height)
	 */
	 VBS2FUSION_DPR(UVHC043) static void flyInHeight(Vehicle& vehicle, double height);

	 /*!
	 Set object's orientation (given as direction and up vector).
	 Since version +1.30 accepts additional parameter which, when object is attached, 
	 specifies if vectors applied are relative to parent object's orientation.  

	 Deprecated. Use void applyVectorDirAndUp(Vehicle veh, vector3D vectorDir, vector3D vectorUp, bool relative)
	 */
	 VBS2FUSION_DPR(UVHC044) static void setVectorDirAndUp(Vehicle veh,vector3D vectorDir, vector3D vectorUp, bool relative);

	 /*!
	 Set the passed thrust wanted for the left track of entity. Only for tracked vehicles. 
	 Vehicle needs to have the controller enabled. So use the following function
	 VehicleUtilities::setManualControl(vehicle, true)
	 */
	 static void applyThrustLeftWanted(Vehicle& vehicle, double factor);

	 /*!
	 Set the passed thrust wanted for the right track of entity. Only for tracked vehicles. Vehicle needs to have the controller enabled
	 So use the following function
	 VehicleUtilities::setManualControl(vehicle, true)
	 */
	 static void applyThrustRightWanted(Vehicle& vehicle, double factor);

	 /*!
	 Sets the watch direction and firing arc. In the scenario the AI gunner will only watch and engage targets within the allocated arc. Its set for a turrets gunner specified in parameters.

	 Unlike setWeaponDirection, this command doesn't require disabled gunner input in order to be applied. 

	 direction - Direction vector, acts as center of current arc
	 sideRange - Horizontal width of arc (left/right)
	 verticalRange - Vertical width of arc (up/down)
	 relative - Sets if direction vector is relative to vehicle orientation
	 */
	 static void applyFireArc(Vehicle& vehicle, Turret::TurretPath& turretPath, vector3D& direction, double sideRange, double verticalRange, bool relative);

#ifdef DEVELOPMENT
	 /*!
	 Activates commander override. Pass ObjNull to deactivate. 
	 */
	 static void applyCommanderOverride(Vehicle& vehicle, Unit& unit, Turret::TurretPath turretPath);
#endif

	 /*!
	 Force aircraft landing.

	 Planes will use the airport defined by assignToAirport, or the one closest to them (in this priority).
	 Helicopters will use either the nearest editor-placed helipad, or the nearest flat location (in this priority). assignToAirport has no effect on helicopters, and either of them have to be within 500m, or it will not land.
	 
	 Deprecated. Use void applyCustomizedLanding(Vehicle& vehicle, LANDMODE mode)
	 */
	 VBS2FUSION_DPR(UVHC045) static void land(Vehicle& vehicle, LANDMODE mode);

	 /*!
	 Send message to vehicle radio channel. 
	 Deprecated. Use void applyVehicleRadioBroadcast(Vehicle& vehicle, string message)
	 */
	 VBS2FUSION_DPR(UVHC048) static void sendToVehicleRadio(Vehicle& vehicle, string name);

	 /*!
	 Type text to vehicle radio channel.

	 This function only types text to the list, it does not broadcast the message. If you want the message to show on all computers, you have to execute it on them.

	 Object parameter must be a vehicle, not a player.
	 If you are in a crew seat (i.e. driver, gunner or commander), then it will include that role in the chat name output (Eg: Driver (you_name): "Message"). 
	
	 Deprecated. Use void applyVehicleChat(Vehicle& vehicle, string message)
	 */
	 VBS2FUSION_DPR(UVHC049) static void vehicleChat(Vehicle& vehicle, string message);

	 /*!
	 Returns true if engine is on, false if it is off. 
	 */
	 static bool isEngineOn(Vehicle& vehicle);

 #ifdef DEVELOPMENT

	 /*!
	 Sets id (integer value) to vehicle. By this id vehicle is referenced by triggers and waypoints.
	 */
	 static void applyVehicleId(Vehicle& vehicle, int id);

	 /*!
	 Set vehicle as respawnable in MP games
	 delay - Default -1 (use respawnDelay from Description.ext)
	 count - Default 0 (unlimited respawns)
	 */
	 static void respawnVehicle(Vehicle& vehicle, double delay = -1, int count = 0);
#endif

	 
	 /*!
	 Returns true if the given unit is turned out. Default is true
	 */
	 static bool isTurnedOut(Unit& unit);

#ifdef DEVELOPMENT

	 /*!
	 Causes a unit to find and enter a new available vehicle position, if the position it was assigned to is now taken 
	 (e.g. it was temporarily ejected, to make room for some other unit to take its place). 
	 */
	 static void autoAssignVehicle(Unit& unit);
#endif

	 /*!
	 Assigns a unit as commander of a vehicle. Used together with orderGetIn to order a unit to get in as commander.

	 delay - If unit already has an assigned vehicle role, time in seconds it will stay at the newly assigned position, 
	 before it moves back to the original position, if that is still available. If position has been filled during the delay, 
	 unit will then stay at the new position. If delay is negative, unit will always stay at new position.

	 Deprecated. Use void applyAssignAsCommander(Vehicle& vehicle, Unit& unit, double delay)
	 */
	 VBS2FUSION_DPR(UVHC019) static void assignAsCommander(Vehicle& vehicle, Unit& unit, double delay);

	 /*!
	 Assigns a unit as driver of a vehicle. Used together with orderGetIn to order a unit to get in as commander.

	 delay - If unit already has an assigned vehicle role, time in seconds it will stay at the newly assigned position, 
	 before it moves back to the original position, if that is still available. If position has been filled during the delay, 
	 unit will then stay at the new position. If delay is negative, unit will always stay at new position.

	 Deprecated. Use void applyAssignAsDriver(Vehicle& vehicle, Unit& unit, double delay)
	 */
	 VBS2FUSION_DPR(UVHC021) static void assignAsDriver(Vehicle& vehicle, Unit& unit, double delay);

	 /*!
	 Assigns a unit as gunner of a vehicle. Used together with orderGetIn to order a unit to get in as commander.

	 delay - If unit already has an assigned vehicle role, time in seconds it will stay at the newly assigned position, 
	 before it moves back to the original position, if that is still available. If position has been filled during the delay, 
	 unit will then stay at the new position. If delay is negative, unit will always stay at new position.

	 Deprecated. Use void applyAssignAsGunner(Vehicle& vehicle, Unit& unit, double delay)
	 */
	 VBS2FUSION_DPR(UVHC022) static void assignAsGunner(Vehicle& vehicle, Unit& unit, double delay);

	 /*!
	 Assigns a unit as cargo of a vehicle. Used together with orderGetIn to order a unit to get in as commander.

	 delay - If unit already has an assigned vehicle role, time in seconds it will stay at the newly assigned position, 
	 before it moves back to the original position, if that is still available. If position has been filled during the delay, 
	 unit will then stay at the new position. If delay is negative, unit will always stay at new position.

	 Deprecated. Use void applyAssignAsCargo(Vehicle& vehicle, Unit& unit, double delay)
	 */
	 VBS2FUSION_DPR(UVHC024) static void assignAsCargo(Vehicle& vehicle, Unit& unit, double delay);

	 /*!
	 Assigns a unit as commander of a vehicle. Used together with orderGetIn to order a unit to get in as commander.

	 Deprecated. Use void applyAssignAsCommander(Vehicle& vehicle, Unit& unit)
	 */
	 VBS2FUSION_DPR(UVHC015) static void assignAsCommander(Vehicle& vehicle, Unit& unit);

	 /*!
	 Assigns a unit as driver of a vehicle. Used together with orderGetIn to order a unit to get in as commander.
	
	 Deprecated. Use void applyAssignAsDriver(Vehicle& vehicle, Unit& unit)
	 */
	 VBS2FUSION_DPR(UVHC016) static void assignAsDriver(Vehicle& vehicle, Unit& unit);

	 /*!
	 Assigns a unit as gunner of a vehicle. Used together with orderGetIn to order a unit to get in as commander.

	 Deprecated. Use void applyAssignAsGunner(Vehicle& vehicle, Unit& unit)
	 */
	 VBS2FUSION_DPR(UVHC017) static void assignAsGunner(Vehicle& vehicle, Unit& unit);

	 /*!
	 Assigns a unit as cargo of a vehicle. Used together with orderGetIn to order a unit to get in as commander.

	 Deprecated. Use void applyAssignAsCargo(Vehicle& vehicle, Unit& unit)
	 */
	 VBS2FUSION_DPR(UVHC018) static void assignAsCargo(Vehicle& vehicle, Unit& unit);

	 /*!
	 Force all units in the array to get in or out of their assigned vehicles. Units must be assigned to a vehicle before this command will do anything. 

	 Deprecated. Use void applyGetIn(vector<Unit> unitVec, bool order)
	 */
	 VBS2FUSION_DPR(UVHC013) static void orderGetIn(vector<Unit> unitVec, bool order);

	 /*!
	 Force unit to get in or out of their assigned vehicles. Unit must be assigned to a vehicle before this command will do anything. 

	 Deprecated. Use void applyGetIn(Unit& unit, bool order)
	 */
	 VBS2FUSION_DPR(UVHC014) static void orderGetIn(Unit& unit, bool order);

		/*!
		Returns the weapons in cargo of the object. 
		It is added by addWeaponToCargo() method
		*/
		static vector<WEAPONCARGO> getWeaponFromCargo(Vehicle& vehicle);

		/*!
		Returns the magazines in cargo of the object. 
		It is added via addMagazineToCargo()
		*/
		static vector<MAGAZINECARGO> getMagazineFromCargo(Vehicle& vehicle);

		/*!
		Return all the muzzles of a person, vehicle
		*/
		static vector<string> getMuzzles(Vehicle& vehicle);

		
		/*!
		Returns the position a shooter has to aim at to hit a target with the current selected weapon.
		*/
		static position3D getAimingPosition(Vehicle& vehicle, ControllableObject& target);


		/*!
		It is only for local objects
		Order the given unit(s) to move to the given location (via the radio). 
		Exactly the same as doMove, except this command displays a radio message. 

		Deprecated. Use void applyCommandMove(Vehicle& vehicle, position3D position) 
		*/
		VBS2FUSION_DPR(UVHC071) static void commandMove(Vehicle& vehicle, position3D position);

		/*!
		Returns the forced speed of an aircraft. 
		*/
		static double getForceSpeed(Vehicle& vehicle);

#ifdef DEVELOPMENT
		/*!
		Checks if the coordinates are whitin firing arc set for specified turret. 
		Returns true if they are, otherwise false.

		vehicle - Vehicle on which the turret is located
		turret - Turret for which specified arc is set
		position - Position being checked if within turrets arc
		*/
		static bool isWithinFireArc(Vehicle& vehicle, Turret::TurretPath& turretPath, position3D& position);
#endif

		/*!
		Returns the arc parameters set for specified turret. 
		*/
		static FIREARC getFireArcParameters(Vehicle& vehicle, Turret::TurretPath& turretPath);

		/*!
		If set to true, the player can steer the vehicle even when he is not driver. Note that an AI driver has to be 
		switched off manually with disableAIMOVE(); as this is not working for planes, you currently have to make sure that 
		the pilot is local to the player's client. 
		*/
		static void applyDriverOverride(bool overRide);

		/*!
		Deletes the specified weapon from the cargo of a vehicle. Does nothing if the weapon isn't actually in vehicle's cargo. 
		Deprecated. Use void applyWeaponCargoRemove(Vehicle& vehicle, string weapon)
		*/
		VBS2FUSION_DPR(UVHC050) static void removeWeaponFromCargo(Vehicle& vehicle, string weapon);

		/*!
		Deletes the specified magazine from the cargo of a vehicle. Does nothing if the magazine isn't actually in vehicle's cargo. Magazines are removed in order from fullest to emptiest.
		Deprecated. Use void applyMagazineCargoRemove(Vehicle& vehicle, string magazine)
		*/
		VBS2FUSION_DPR(UVHC051) static void removeMagazineFromCargo(Vehicle& vehicle, string magazine);

		/*!
		Removes the magazine from vehicles turret
		Deprecated. Use void applyMagazineTurretRemove(Vehicle& vehicle, string magazine, Turret::TurretPath& turretPath)
		*/
		VBS2FUSION_DPR(UVHC052) static void removeMagazineFromTurret(Vehicle& vehicle, string magazine, Turret::TurretPath& turretPath);

		/*!
		Add magazines to the cargo space of vehicles, which can be taken out by infantry units.
		Deprecated. Use void applySingleMagazineCargo(Vehicle& vehicle, string magazineName, int bulletCount)
		*/
		VBS2FUSION_DPR(UVHC053) static void addMagazineCargoEx(Vehicle& vehicle, string magazineName, int bulletCount);

		/*!
		Sets the armor (or health for men) state of the vehicle (a value from 0 to 1). 
		*/
		static void applyVehicleArmor(Vehicle& vehicle, double armor);

#ifdef DEVELOPMENT
		/*!
		Sets the vehicle formation direction
		*/
		static void applyFormationDirection(Vehicle& vehicle, double direction);
		//----------------------------------------------------------------------------
#endif


		/*!
		Checks whether the object is (a subtype) of the given type.
		Command works with classes contained in CfgVehicles or CfgAmmo, but not with others (e.g. CfgMagazines or CfgWeapons).
		*/
		static bool isKindOf(Vehicle& vehicle, string typeName);

		/*!
		Check if (and by how much) vehicle commander knows about target.
		And returns a number from 0 to 4.
		Deprecated. Use double getKnowsAbout(Vehicle& vehicle, ControllableObject& target)
		*/		
		VBS2FUSION_DPR(UVHC054) static float knowsAbout(Vehicle& vehicle, ControllableObject& target);


		/*!
		Check if (and by how much) vehicle commander knows about target.
		And returns a number from 0 to 4.
		Deprecated. Use double getKnowsAbout(Vehicle& vehicle, ControllableObject& target)
		*/		
		VBS2FUSION_DPR(UVHC054) static double knowsAboutEx(Vehicle& vehicle, ControllableObject& target);


		/*!
		Applies the name of the vehicle to the VBS2 Environment.
		Prior to applying, function verifies that the name is a valid name
		according to following criteria.
		- Name has to start with letter or underscore.
		- Only special characters allowed is underscore.
		- Name should not contain spaces in between.
		- Name should not already exist in the mission.
		*/
		static void applyName(Vehicle& vehicle);

		/*!
		Returns the setting defined by setNetworkShell of the vehicle.
		*/
		static bool getNetworkShell (Vehicle& vehicle);

		/*!
		Lock or Unlock the vehicle for player. 
		Deprecated. Use void applyVehicleLockEnable(Vehicle& vehicle, bool playerLock)
		*/
		VBS2FUSION_DPR(UVHC055) static void lockVehicle(Vehicle& vehicle, bool playerLock);

		/*!
		Lock or Unlock the vehicle for player. 
		Deprecated. Use void applyVehicleLockEnable(Vehicle& vehicle, bool playerLock, bool allLock)
		*/
		VBS2FUSION_DPR(UVHC056) static void lockVehicle(Vehicle& vehicle, bool playerLock, bool allLock);

		/*!
		Checks whether the soldier is mounted in the vehicle.
		*/
		static bool isInVehicle(Vehicle& vehicle, Unit& unit);

		/*!
		Returns the formation leader of a given Vehicle.
		*/
		static Unit getFormLeader(Vehicle& vehicle);

		/*!
		Return the precision of the given entity.
		*/
		static float getPrecision(Vehicle& vehicle);

		/*!
		Return the precision of the given entity.
		*/
		static double getPrecisionEx(Vehicle& vehicle);

#ifdef DEVELOPMENT
		/*!
		Return how much vehicle wants to reload its weapons.
		*/
		static double getNeedReload(Vehicle& vehicle);
#endif

		/*!
		Return how much vehicle wants to reload its weapons.
		*/
		static ControllableObject getAssignedTarget(Vehicle& vehicle);

#ifdef DEVELOPMENT
		/*!
		Get the speed for the given speed mode.
		*/
		static double getSpeed(Vehicle& vehicle, string mode);
#endif

		/*!
		Returns the current position of a selection in model space.
		If a selection does not exist [0,0,0] is returned.
		*/
		static position3D getSelectionPosition(Vehicle& vehicle, string selectName);

		/*!
		Returns the current position of a selection in model space.
		If a selection does not exist [0,0,0] is returned.
		*/
		static position3D getSelectionPosition(Vehicle& vehicle, string selectName, string method);

#ifdef DEVELOPMENT
		/*!
		Returns name of a vehicle's secondary weapon (empty string if none).
		*/
		static string getSecondaryWeapon(Vehicle& vehicle);
#endif

		/*!
		Check whether magazine is reloaded whenever emptied.
		*/
		static bool isReloadEnabled(Vehicle& vehicle);


		/*!
		Attach a statement to a Vehicle. The statement is propagated over the network
		in MP games, it can be executed by invoking processInitCommands. 
		Deprecated. Use void applyInitStatement(Vehicle& vehicle, string statement)
		*/
		VBS2FUSION_DPR(UVHC065) static void setInitStatement(Vehicle& vehicle, string& statement) ;

		/*!
		Clear the vehicle's init statement.
		Deprecated. Use void applyInitStatementClear(Vehicle& vehicle)
		*/
		VBS2FUSION_DPR(UVHC066) static void clearInitStatement(Vehicle& vehicle) ;




		/*!
		returns Name of first crew member(in order commander, driver, gunner).
		*/
		static string getName(Vehicle& vehicle);

		/*!
		Returns if the given Vehicle is captive. "captive" means that enemies will not shoot at the Vehicle.
		*/
		static bool isCaptive(Vehicle& vehicle);




		/*!
		Orders an AI-controlled aircraft to land at a given airport.
		The available airport codes are listed on the respective map pages.

		Deprecated. Use void applyLanding(Vehicle& vehicle, int port)
		*/
		VBS2FUSION_DPR(UVHC047) static void landTo(Vehicle& vehicle, int port);

		/*!
		Assigns the airport that will be used when issuing either a land command to an AI-controlled aircraft, 
		or selecting the "Landing autopilot" action, if the aircraft is player-controlled.
		The available airport codes are listed on the respective map pages. 

		Deprecated. Use void applyAirportAssignment(Vehicle& vehicle, int port)
		*/
		VBS2FUSION_DPR(UVHC046) static void assignToAirport(Vehicle& vehicle, int port);

		


		/* !
		Check if vehicle is locked for Persons. If it is locked, Persons cannot mount / dismount without order. 
		*/

		static bool islocked(Vehicle& vehicle);

		/*!
		Assigns cargo role to unit, and places him into the vehicle (immediate, no animation). 
		*/
		static void moveInCargoWithPosition(Unit& unit,Vehicle& vehicle, int cargoIndex);

		/*!
		Ceases the using of the vehicle by  a group. It unassigns them from the vehicle. 
		Deprecated. Use void applyGroupLeave(Vehicle& vehicle, Group& group)
		*/
		VBS2FUSION_DPR(UVHC057) static void groupLeaveVehicle(Group& group, Vehicle& vehicle);

		/*!
		Ceases the using of the vehicle by  a unit. It unassigns them from the vehicle.
		Deprecated. Use void applyGroupLeave(Vehicle& vehicle, Unit& unit)
		*/
		VBS2FUSION_DPR(UVHC058) static void groupLeaveVehicle(Unit& unit, Vehicle& vehicle);

		/*!
		Returns the type name of Binocular weapon which is in the WeaponSlotBinocular.
		Here Vehicle's side can't be civilian. 
		*/
		static string getBinocularWeaponName(Vehicle& vehicle);

		/*!
		Returns the expected destination position of vehicle.
		*/
		static position3D getExpectedDestinationPos(Vehicle& vehicle);

		/*!
		Returns the expected destination planning mode of vehicle.
		*/
		static string getExpectedDestinationPlanMode(Vehicle& vehicle);

		/*!
		Tells whether destination's replanning of the path was forced.
		*/
		static bool getExpectedDestinationForceReplan(Vehicle& vehicle);

		/*!
		Returns position of vehicle in the formation.
		*/
		static position3D getFormationPosition(Vehicle& vehicle);

		/*!
		Return the current command type for the commander of given vehicle. 
		*/
		static string getCurrentCommand(Vehicle& vehicle);

		/*!
		Returns the group leader for the given vehicle. For dead units, objNull is returned
		*/
		static Unit getGroupLeader(Vehicle& vehicle);

		/*!
		Returns an array with all the units in the group of the given object. For a destroyed object an empty array is returned.
		*/
		static vector<Unit> getGroupUnits(Vehicle& vehicle);

		/*!
		Check if vehicle is stopped by stop command 
		*/
		static bool isStopped(Vehicle& vehicle);

		/*!
		Check if the vehicle is ready. Unit is busy when it is given some command like move, until the command is finished. 
		*/
		static bool isReady(Vehicle& vehicle);
		
		/*!
		Removes all weapons from the vehicle cargo space. MP synchronized.
		Deprecated. Use void applyWeaponCargoRemoveGlobal(Vehicle& vehicle)
		*/
		VBS2FUSION_DPR(UVHC069) static void clearWeaponCargoGlobal(Vehicle& vehicle);

		/*!
		Removes all magazines from the vehicle cargo space. MP synchronized.
		Deprecated. Use void applyMagazineCargoRemoveGlobal(Vehicle& vehicle)
		*/
		VBS2FUSION_DPR(UVHC070) static void clearMagazineCargoGlobal(Vehicle& vehicle);

		/*!
		Return the result of helicopter landing position searching (performed after land command). 
		The value can be \"Found\" (position found), \"NotFound\" (position not found), \"NotReady\" 
		(position searching is still in progress) or empty string when wrong argument given.
		*/
		static  string getLandResult(Vehicle& vehicle);

		/*!
		Sets vehicle's lights states. (-2 forced off, -1 off (user controlled), 0 user controlled, 1 on (user controlled), 2 forced on)
		Deprecated. Use void applyVehicleLights(Vehicle& vehicle, int main, int side, int convoy, int left, int right)
		*/
		VBS2FUSION_DPR(UVHC061) static void setVehicleLights(Vehicle& vehicle,int main, int side, int convoy, int left, int right);

		/*!Returns the status of different vehicle lights, as set by either the user's input (via action menu or control keys), or the command setVehicleLights.

			Each light element can have one of the following states:

			-2: Forced off (user cannot override it, either via action menu or control buttons)
			-1: off (user can turn it on again)
			 1: on (user can turn it off again)
			 2: forced on (user cannot override it, either via action menu or control buttons) 
		*/
		static vector<int> getVehicleLights(Vehicle& vehicle);

		/*!
		Adds weapons to the weapon cargo space. This is used for infantry weapons.
		MP synchronized
		Deprecated. Use void applyWeaponCargoGlobal(Vehicle& vehicle, Weapon& weapon, int count)
		*/
		VBS2FUSION_DPR(UVHC067) static void addWeaponToCargoGlobal(Vehicle& vehicle, Weapon& weapon, int count);

		/*!
		Adds magazines to the weapon cargo space. This is used for infantry weapons.
		MP synchronized.
		magName - Class name of the magazine.
		Deprecated. Use void applyMagazineCargoGlobal(Vehicle& vehicle, string magName, int count)
		*/
		VBS2FUSION_DPR(UVHC068) static void addMagazineToCargoGlobal(Vehicle& vehicle, string magName, int count);

		/*!
		Set if leader can issue attack commands to the soldiers in his group
		Deprecated. Use void applyAttackEnable(Vehicle& vehicle, bool boolVal)
		*/
		VBS2FUSION_DPR(UVHC059) static void setEnableAttack(Vehicle& vehicle, bool boolVal);

		/*!
		Return whether a group's leader can issue attack commands to soldiers under his command. 
		*/
		static bool isAttackEnabled(Vehicle& vehicle);

		/*!
		Sets the rotor wash effect of helicopters (applies to all helicopters in mission).

		This affects the dust created by the helicopter, as well as the wind effect on objects nearby.
		While it is not possible to turn off the dust effect altogether, it is possible to make it somewhat 
		invisible by using very high values (e.g. [100,100]) in the parameters. This only works though, 
		if there are no objects nearby that might be affected by the generated wind (e.g. trees), as they would then show unrealistic wind strain.
		
		Deprecated. Use bool applyRotorWash(double strength, double diameter)
		*/
		VBS2FUSION_DPR(UVHC060) static bool setRotorWash(double strength, double diameter);

		/*!
		Enables or disables specific sound types for a vehicle:

		"SOUNDENGINE": Engine
		"SOUNDENVIRON": Movement sounds (e.g. tank tracks)
		"SOUNDCRASH": Impact with another object
		"SOUNDDAMMAGE": Alarm sound if vehicle was hit
		"SOUNDLANDCRASH": Impact with land (e.g. aircraft crash)
		"SOUNDWATERCRASH": Impact with water (e.g. aircraft crash)
		"SOUNDGETIN": Entering vehicle
		"SOUNDGETOUT": Exiting vehicle
		"SOUNDSERVO": Vehicle attachments (e.g. tank turrets)

		Be aware that not all of these sounds are defined in every vehicle.

		Deprecated. Use void applySoundEnable(Vehicle& vehicle, VEHICLE_SOUND_TYPES types, bool value)
		*/
		VBS2FUSION_DPR(UVHC074) static void allowSound(Vehicle& vehicle,VEHICLE_SOUND_TYPES types,bool value);

		/*
		Controls screen blanking during certain actions. 
		Action with an associated screen blanking. Supported actions: "GETOUT", "TURNOUT", "TURNIN" 
		Enum :VA_GETOUT,VA_TURNOUT,VA_TURNIN

		Deprecated. Use void applyBlackFadeEnable(Vehicle& vehicle, VEHICLE_ACTION actions, bool value)
		*/
		VBS2FUSION_DPR(UVHC062) static void allowBlackFade(Vehicle& vehicle, VEHICLE_ACTION actions, bool value);

		/*!
		Returns list of sounds that were disabled via allowSound
		*/
		static vector<string> getDisallowedSounds(Vehicle& vehicle);

		/*!
		Search for selection in the object model (Fire Geo only). Returns center in model space.
		*/
		static position3D getSelectionCenter(Vehicle& vehicle, string& selectionName);

		/*!
		Returns the number of given positions in the vehicle.
		Positions can be "Commander", "Driver", "Gunner" or "Cargo"
		*/
		static int getEmptyPositions(Vehicle& vehicle,VEHICLEASSIGNMENTTYPE vehPosition);

		/*!
		Returns whether a turret is locked to something or not.
		*/
		static bool IsTurretLocked(Vehicle& vehi, Turret::TurretPath turretPath);

		/*!
		Forces gunner in vehicle to recalculate the current lasing distance.

		Overrides the actual laser measurements (which are used to calculate the ballistic calculations) with a value provided by this command.
		This value will be shown in the gunner's optics view, until he does another manual lasing.
		*/
		static void applyLaserRange(Vehicle& vehicle, Unit& gunner);

		/*!
		Returns the offset between weapon orientation and optics.
		*/
		static vector<double> getOpticsOffset(Vehicle& vehicle, Turret::TurretPath turretPath);

		/*!
		Returns the combat mode (engagement rules) of the vehicle.

		Modes returned may be:

		COMBAT_BLUE (Never fire)
		COMBAT_GREEN (Hold fire - defend only)
		COMBAT_WHITE (Hold fire, engage at will)
		COMBAT_YELLOW (Fire at will)
		COMBAT_RED (Fire at will, engage at will) 

		or

		COMBAT_ERROR (will be returned if group was passed or combat mode couldn't be retrieved) 
		*/
		static COMBATMODE getVehicleCombatMode(Vehicle& vehicle);

		/*!
		Set how vehicle is locked for player
		*/
		static void applyVehicleLock(Vehicle& vehicle, LOCK_STATE state);

		/*!
		Orders the unit to get out from the vehicle (via the radio). 
		This command is only effective when used on local units. When used on a remote unit, this command will create radio 
		dialog on the machine the unit is local to, but the unit will not leave the vehicle.

		Deprecated. Use void applyCommandGetOut(Unit& unit)
		*/
		VBS2FUSION_DPR(UVHC064) static void commandGetOut(Unit& unit);

		/*!
		Orders a unit or units to get out from the vehicle (silently).
		Deprecated. Use void applyGetOut(Unit& unit)
		*/
		VBS2FUSION_DPR(UVHC063) static void doGetOut(Unit& unit);

		/*!
		Forces the Armed Vehicle to fire on the given position from the specified turret path.   
		*/
		static void applyFire(Vehicle& vehicle, position3D& pos, Turret::TurretPath turretPath);

		/*!
		Sends a simple command to the vehicle's driver or gunner. 
		Player should be a vehicle's crew member to use this function. 
		*/
		static void applyCommand(Vehicle& vehicle, SIMPLE_COMMAND command);

		/*!
		Assigns driver role to an already created unit, and places him into the vehicle immediately with out an animation. 
		
		@param vehicle - Vehicle which is supposed to assign the driver.
		@param unit - An already created unit which is assigned the driver role and placed into the vehicle.

		@return Nothing.

		@remarks This is a replication of void moveInAndAsDriver(Vehicle& vehicle, Unit& unit) function.
		*/
		static void applyMoveInAsDriver(Vehicle& vehicle, Unit& unit);

		/*!
		Assigns turret role to an already created unit, and places him into the vehicle immediately with out an animation.

		Available turrets can be determined by the vector<CrewPos> getCrewPosition(Vehicle& vehicle) function, and is defined
		as an array, containing either one or two positions definitions, 
		e.g.
		[0]: first turret (no sub-turrets exist).
		[0,0]: first sub-turret of first main turret.
		[1,0]: first sub-turret of second main turret.

		@param vehicle - Vehicle which is supposed to assign the turret.
		@param unit - An already created unit which is assigned the turret role and placed into the vehicle.
		@param turretPath - TurretPath which the unit is moved into.  As example if wants to move the unit into first sub-turret 
							of first main turret, parameter should be "[0,0]"

		@return Nothing.

		@remarks This is a replication of void moveInAndAsTurret(Vehicle& vehicle, Unit& unit, string turretPath) function.
		*/
		static void applyMoveInAsTurret(Vehicle& vehicle, Unit& unit, string turretPath);

		/*!
		Assigns turret role to an already created unit, and places him into the vehicle immediately with out an animation.

		Available turrets can be determined by the vector<CrewPos> getCrewPosition(Vehicle& vehicle) function, and is defined
		as an array, containing either one or two positions definitions, 
		e.g.
		[0]: first turret (no sub-turrets exist).
		[0,0]: first sub-turret of first main turret.
		[1,0]: first sub-turret of second main turret.

		@param vehicle - Vehicle which is supposed to assign the turret.
		@param unit - An already created unit which is assigned the turret role and placed into the vehicle.
		@param turret - TurretPath which the unit is moved into.  As example if wants to move the unit into first sub-turret 
							of first main turret, parameter should be [0,0]

		@return Nothing.

		@remarks This is a replication of void moveInAndAsTurret(Vehicle& vehicle, Unit& unit, Turret turret) function.
		*/
		static void applyMoveInAsTurret(Vehicle& vehicle, Unit& unit, Turret turret);

		/*!
		Assigns Turret role to an already created unit, and places him into the vehicle immediately with out an animation.

		@param vehicle - Vehicle which is supposed to assign the Turret.
		@param unit - An already created unit which is assigned the Turret role and placed into the vehicle.

		@return Nothing.

		@remarks This is a replication of void moveInAndAsTurret(Vehicle& vehicle, Unit& unit) function.
				It take default turret
		[0,1]: second sub-turret of first main turret.

		*/
		static void applyMoveInAsTurret(Vehicle& vehicle, Unit& unit);

		/*!
		Assigns Cargo role to an already created unit, and places him into the vehicle immediately with out an animation.

		@param vehicle - Vehicle which is supposed to assign the Cargo.
		@param unit - An already created unit which is assigned the Cargo role and placed into the vehicle.

		@return Nothing.

		@remarks This is a replication of void moveInAndAsCargo(Vehicle& vehicle, Unit& unit) function.
		*/
		static void applyMoveInAsCargo(Vehicle& vehicle, Unit& unit);

		/*!
		Assigns Gunner role to an already created unit, and places him into the vehicle immediately with out an animation.

		@param vehicle - Vehicle which is supposed to assign the Gunner.
		@param unit - An already created unit which is assigned the Gunner role and placed into the vehicle.

		@return Nothing.

		@remarks This is a replication of void moveInAndAsGunner(Vehicle& vehicle, Unit& unit) function.
		*/
		static void applyMoveInAsGunner(Vehicle& vehicle, Unit& unit);

		/*!
		Assigns Commander role to an already created unit, and places him into the vehicle immediately with out an animation.

		@param vehicle - Vehicle which is supposed to assign the Commander.
		@param unit - An already created unit which is assigned the Commander role and placed into the vehicle.

		@return Nothing.

		@remarks This is a replication of void moveInAndAsCommander(Vehicle& vehicle, Unit& unit) function.
		*/
		static void applyMoveInAsCommander(Vehicle& vehicle, Unit& unit);


		/*!
		Activate and deactivate the engine of the vehicle. If state is true, 
		engine will be on and it will stop when state is false.
		
		@param vehicle - Vehicle which supposed to start or stop.
		@param state - If state is "true" engine is on and else it is false. 

		@return Nothing.

		@remarks This is a replication of void startEngine(Vehicle& vehicle , bool state) funtion.
		*/
		static void applyEngineStart(Vehicle& vehicle , bool state);

		/*!
		Orders unit to leave vehicle. Vehicle should be properly updated.

		@param vehicle - Vehicle which is leaved by the given unit.
		@param unit - Unit which is going to leave the vehicle. 

		@return Nothing.

		@remarks This is a replication of following functions.
			void leaveVehicle(Vehicle& vehicle, Unit& unit)
			void leaveVehicleByID(Vehicle& vehicle, Unit& unit)
			void leaveVehicleByName(Vehicle& vehicle, Unit& unit)
			void leaveVehicleByAlias(Vehicle& vehicle, Unit& unit)
		*/
		static void applyUnitLeave(Vehicle& vehicle, Unit& unit);

		/*!
		Enable/ disable manual control of the vehicle.
		if control is TRUE  vehicle AI disabled and it can be controlled 
		by applyTurnWanted & applyThrustWanted functions.  
		
		@param vehicle - Vehicle which is supposed to set AI control.
		@param control - If control is "true" AI control able and else it is disable.

		@return Nothing.

		@remarks This is a replication of void setManualControl(Vehicle& vehicle , bool control) funtion.
		*/
		static void applyManualControl(Vehicle& vehicle , bool control);

		/*!
		set the turn wanted for the vehicle specified by the factor value.
		manual control of the vehicle should be enabled first. 

		@param vehicle - Vehicle which is supposed to turn.
		@param factor - value that need to turn between -1.0 to 1.0.

		@return Nothing.

		@remarks This is a replication of void setTurnWanted(Vehicle& vehicle, double factor) funtion.
		*/
		static void applyTurnWanted(Vehicle& vehicle, double factor);

		/*!
		set the thrust wanted for the vehicle to passed in. 
		manual control of the vehicle should be enabled first.
		
		@param vehicle - Vehicle which is supposed to control on thrust.
		@param factor -  Value that need to thrust between -1.0 to 1.0.

		@return Nothing.

		@remarks This is a replication of void setThrustWanted(Vehicle& vehicle , double factor) function
		*/
		static void applyThrustWanted(Vehicle& vehicle , double factor);

		/*!
		Force unit to get in or out of their assigned vehicles. Unit must be assigned to a vehicle before this command will do anything. 
		
		@param unit - Unit who is supposed to get in vehicle.
		@param order - If order is "true" unit get into vehicle else get out from vehicle

		@return Nothing.

		@remarks This is a replication of void orderGetIn(Unit& unit, bool order) function
		*/
		static void applyGetIn(Unit& unit, bool order);

		/*!
		Force all units in the array to get in or out of their assigned vehicles. Units must be assigned to a vehicle before this command will do anything. 

		@param unitVec - The container hold units those who are supposed to get in vehicle.
		@param order - If order is "true" units those who are in unitVec get into vehicle else get out from vehicle

		@return Nothing.

		@remarks This is a replication of void orderGetIn(vector<Unit> unitVec, bool order) function
		*/
		static void applyGetIn(vector<Unit> unitVec, bool order);

		/*!
		Assigns a unit as commander of a vehicle. Used together with orderGetIn to order a unit to get in as commander.

		@param vehicle - Vehicle that suppose to assign as commander.
		@param unit - unit that created as commander.

		@return Nothing.

		@remarks This is a replication of void assignAsCommander(Vehicle& vehicle, Unit& unit) function
		*/
		static void applyAssignAsCommander(Vehicle& vehicle, Unit& unit);

		/*!
		Assigns a unit as cargo of a vehicle. Used together with orderGetIn to order a unit to get in as commander.

		@param vehicle - Vehicle that suppose to assign as cargo.
		@param unit - unit that created as cargo.

		@return Nothing.

		@remarks This is a replication of void assignAsCargo(Vehicle& vehicle, Unit& unit)
		*/
		static void applyAssignAsCargo(Vehicle& vehicle, Unit& unit);

		/*!
		Assigns a unit as driver of a vehicle. Used together with orderGetIn to order a unit to get in as commander.

		@param vehicle - Vehicle that suppose to assign as driver.
		@param unit - unit that created as driver.

		@return Nothing.

		@remarks This is a replication of void assignAsDriver(Vehicle& vehicle, Unit& unit)
		*/
		static void applyAssignAsDriver(Vehicle& vehicle, Unit& unit);

 		/*!
		Assigns a unit as gunner of a vehicle. Used together with orderGetIn to order a unit to get in as commander.

		@param vehicle - Vehicle that suppose to assign as gunner.
		@param unit - unit that created as gunner.

		@return Nothing.

		@remarks This is a replication of void assignAsGunner(Vehicle& vehicle, Unit& unit) function
		*/
		static void applyAssignAsGunner(Vehicle& vehicle, Unit& unit);

		/*!
		Assigns a unit as commander of a vehicle. Used together with orderGetIn to order a unit to get in as commander.

		@param vehicle - Vehicle that suppose to assign as commander.
		@param unit - unit that created as commander.
		@param delay - If unit already has an assigned vehicle role, time in seconds it will stay at the newly assigned position, 
		before it moves back to the original position, if that is still available. If position has been filled during the delay, 
		unit will then stay at the new position. If delay is negative, unit will always stay at new position.

		@return Nothing.

		@remarks This is a replication of void assignAsCommander(Vehicle& vehicle, Unit& unit, double delay)
		*/
		static void applyAssignAsCommander(Vehicle& vehicle, Unit& unit, double delay);

		/*!
		Applies vehicle's maximum speed which is defined by vehicle.getMaximumSpeedLimit() to the vehicle. 

		@param vehicle - Vehicle to be applied the maximum speed limit.

		@return Nothing.
		*/
		static void applyMaximumSpeedLimit(Vehicle& vehicle);

		/*!
		Updates the maximum speed limit of the vehicle. 

		@param vehicle - Vehicle to be updated the maximum speed limit.

		@return Nothing.
		*/
		static void updateMaximumSpeedLimit(Vehicle& vehicle);

		/*!
		Process an animation on a vehicle. 

		@param vehicle - Vehicle to be applied the animation.
		@param animationName - The name of the animation.
		@param phase - Phase value is between 0(start point of the animation) and 1(end point of the animation).  

		@return Nothing.

		@remarks This is a replication of void applyAnimation(Vehicle& vehicle, string animationName, double phase) function. 
		*/
		static void applyAnimation(Vehicle& vehicle, string animationName, double phase);

		/*!
		Assigns a unit as driver of a vehicle. Used together with applyGetIn to order a unit to get in as commander.

		@param vehicle - Vehicle that suppose to assign as driver.
		@param unit - unit that created as driver.
		@param delay - If unit already has an assigned vehicle role, time in seconds it will stay at the newly assigned position, 
		before it moves back to the original position, if that is still available. If position has been filled during the delay, 
		unit will then stay at the new position. If delay is negative, unit will always stay at new position.

		@return Nothing.
		
		@remarks This is a replication of assignAsDriver(Vehicle& vehicle, Unit& unit, double delay) function.
		*/
		static void applyAssignAsDriver(Vehicle& vehicle, Unit& unit, double delay);

		/*!
		Assigns a unit as gunner of a vehicle. Used together with applyGetIn to order a unit to get in as commander.

		@param vehicle - Vehicle that suppose to assign as gunner.
		@param unit - unit that created as gunner.
		@param delay - If unit already has an assigned vehicle role, time in seconds it will stay at the newly assigned position, 
		before it moves back to the original position, if that is still available. If position has been filled during the delay, 
		unit will then stay at the new position. If delay is negative, unit will always stay at new position.

		@return Nothing.

		@remarks This is a replication of assignAsGunner(Vehicle& vehicle, Unit& unit, double delay)
		*/
		static void applyAssignAsGunner(Vehicle& vehicle, Unit& unit, double delay);

 		/*!
		Assigns a unit as cargo of a vehicle. Used together with applyGetIn to order a unit to get in as commander.

		@param vehicle - Vehicle that suppose to assign as cargo.
		@param unit - unit that created as cargo.
		@param delay - If unit already has an assigned vehicle role, time in seconds it will stay at the newly assigned position, 
			before it moves back to the original position, if that is still available. If position has been filled during the delay, 
			unit will then stay at the new position. If delay is negative, unit will always stay at new position.

		@return Nothing.

		@remarks This is a replication of assignAsCargo(Vehicle& vehicle, Unit& unit, double delay)
		*/
		static void applyAssignAsCargo(Vehicle& vehicle, Unit& unit, double delay);

		/*!
		Select the Weapon given by the muzzle Name.

		@param vehicle - Vehicle that hold the weapon.
		@param muzzleName - Muzzle name.  For weapons that have more than one muzzle, you have to input the
				'muzzlename' and not the weaponName. The only weapons that have muzzleNames seem to be rifles
				with grenade launchers, handgrenades, smokeshells and satchels. In all other cases the weaponName
				must be used.

		@return Nothing.

		@remarks This is a replication of void selectWeapon(Vehicle& vehicle, string muzzleName) function. 
		*/
		static void applyWeaponSelection(Vehicle& vehicle, string muzzleName);

		/*!
		Add magazine to the Vehicle. The number of magazines that can be added via this function is not limited
		by the magazine slots allotted to a vehicle (i.e. it is possible to add hundreds of magazines.)

		@param vehicle - Vehicle to be added the magazine.
		@param magName - Name of the magazine which is added to vehicle.

		@return Nothing.

		@remarks This is a replication of void addMagazine(Vehicle& vehicle, string name) function. 
		*/
		static void applyMagazineAddition(Vehicle& vehicle, string magName);

		/*!
		Count number of magazines available in given Vehicle object.

		@param vehicle -  Vehicle that supposed to get magazine count.

		@return - number of magazine. 

		@remarks This is a replication of int countMagazines(Vehicle& vehicle)
		*/
		static int getMagazineCount(Vehicle& vehicle);

		/*!
		Add Weapon to the Vehicle. weapon slots are filled, any further applyWeaponAddition commands are ignored. 
		To ensure that the weapon is loaded at the start of the mission, at least one magazine should 
		be added, before adding the weapon.

		@param vehicle - Vehicle that supposed to add weapon.
		@param name - weapon name.

		@return Nothing.

		@remarks This is a replication of void addWeapon(Vehicle& vehicle, string name)
		*/
		static void applyWeaponAddition(Vehicle& vehicle, string name);

		/*!
		Applies reload state of the weapon. 

		@param vehicle -  Vehicle that hold the weapon.
		@param weapon -  Name of the weapon.
		@param reloadStatus - Reload status of the weapon.
		@param reloadTime - Reload time in seconds.

		@return Nothing.

		@remarks This is a replication of void setWeaponState(Vehicle& vehicle,Weapon& weapon, bool reloadStatus,double reloadTime) function.
		*/
		static void applyWeaponState(Vehicle& vehicle, Weapon& weapon, bool reloadStatus, double reloadTime);

		/*!
		Remove the specified Weapon from the vehicle.
		
		@param vehicle - Vehicle that supposed to remove weapon.
		@param name - Weapon name.

		@return Nothing.

		@remarks This is a replication of void removeWeapon(Vehicle& vehicle, string name)
		*/
		static void applyWeaponRemove(Vehicle& vehicle, string name);

		/*!
		Tells if a vehicle has the given weapon.	
		
		@param vehicle - Vehicle that supposed to check whether named weapon is available or not.
		@param name - Weapon name.

		@return - If it is available return "true" and else it is to be false.

		@remarks This is a replication of bool hasWeapon(Vehicle& vehicle, string name)
		*/
		static bool isWeaponAvailable(Vehicle& vehicle, string name);

		/*!
		Remove the specified magazine from the Vehicle.

		@param vehicle -  Vehicle that hold the magazine.
		@param magName -  Name of the magazine.

		@return Nothing.

		@remarks This is a replication of void removeMagazine(Vehicle& vehicle, string name) function.
		*/
		static void applyMagazineRemove(Vehicle& vehicle, string magName);

		/*!
		Remove all magazines of given type from the vehicle.	

		@param vehicle -  Vehicle without turrets to remove magazines from.
		@param magName -  Name of the magazine.

		@return Nothing.

		@remarks This is a replication of void removeallMagazines(Vehicle& vehicle, string name) function.
		*/
		static void applyMagazineRemoveAll(Vehicle& vehicle, string magName);

		/*!
		Applies the direction of the Turret weapon according to azimuth and elevation.

		@param vehicle -  Vehicle with turret weapon and must has a gunner, whose input has been disabled via 
			void applyGunnerInputDisable(Vehicle& vehicle, Turret& turret, bool status = true) function.
		@param turret -  Turret which weapon belongs to.
		@param azimuth - Azimuth value in degrees.
		@param elevation - Elevation value in degrees.
		@param transition - If the boolean value transition is false, the turret 'jumps' to it's destination.

		@return Nothing.

		@remarks This is a replication of void setWeaponDirection(Vehicle& vehicle, Turret& turret, double azimuth, double elevation, bool transition) function.
		*/
		static void applyWeaponDirection(Vehicle& vehicle, Turret& turret, double azimuth, double elevation, bool transition);

		
		/*!
		Disables gunner input at the turret.

		@param vehicle -  Vehicle with turret. 
		@param turret - Turret to disabled the gunner input. 
		@param	status - Is optional and default is 'true'. 'true' sets as the default value then AI will not control turret anymore. If
			it sets to 'false', AI will control turret. 

		@return Nothing.

		@remarks This is a replication of void disableGunnerInput(Vehicle& vehicle, Turret& turret, bool status = true) function.
		*/
		static void applyGunnerInputDisable(Vehicle& vehicle, Turret& turret, bool status = true);

		/*!
		Applies how much ammunition (compared to a full state defined by the unit type) the vehicle has.

		@param vehicle -  Vehicle to ammunition is applied. 
		@param ammovalue - Ammunition value ranges from 0 to 1.
		
		@return Nothing.

		@remarks This is a replication of void setVehicleAmmo(Vehicle& vehicle, float ammovalue) and void setVehicleAmmo(Vehicle& vehicle, double ammovalue) functions.
		*/
		static void applyVehicleAmmo(Vehicle& vehicle, double ammovalue);

		/*!
		Add weapons to the cargo space of vehicles, which can be taken out by infantry units. Once the weapon cargo space is filled up,
		any further applyWeaponCargo functions are ignored.

		@param vehicle -  Vehicle to add the weapons to it's cargo. 
		@param weaponName - Name of the weapon.
		@param count - Number of weapons to add.

		@return Nothing.

		@remarks This is a replication of void addWeaponToCargo(Vehicle& vehicle, string weaponName, int count) function.
		*/
		static void applyWeaponCargo(Vehicle& vehicle, string weaponName, int count);

		/*!
		Add magazines to the cargo space of vehicles, which can be taken out by infantry units. Once the magazine cargo space is filled up,
		any further applyMagazineCargo functions are ignored.

		@param vehicle -  Vehicle to add the magazines to it's cargo. 
		@param weaponName - Name of the magazine.
		@param count - Number of magazines to add.

		@return Nothing.

		@remarks This is a replication of void addMagazineToCargo(Vehicle& vehicle, string weaponName, int count) function.
		*/
		static void applyMagazineCargo(Vehicle& vehicle, string magName, int count);

		/*!
		Remove all weapons from the given vehicle's weapon cargo space.

		@param vehicle - Vehicle to clear weapon cargo. 

		@return Nothing.

		@remarks This is a replication of void clearAllWeaponFromCargo(Vehicle& vehicle) function.
		*/
		static void applyWeaponCargoRemoveAll(Vehicle& vehicle);

		/*!
		Remove all magazines from the given vehicle's magazine cargo space.

		@param vehicle - Vehicle to clear magazine cargo. 

		@return Nothing.

		@remarks This is a replication of void clearAllMagazineFromCargo(Vehicle& vehicle) function.
		*/
		static void applyMagazineCargoRemoveAll(Vehicle& vehicle);

		/*!
		Enables or Disables firing on current weapon in the selected turret.
		
		@param vehicle - Vehicle with turret. 
		@param turret - Turret where enables or disables firing on current weapon.
		@param safe - If true, none of the turret's weapon can be fired, and a "Safety On" status message is visible in the player HUD.
			If false, weapon can be fired normally.
		
		@return Nothing.

		@remarks This is a replication of void setWeaponSafety(Vehicle& vehicle, Turret& turret, bool safe) function.
		*/
		static void applyWeaponSafety(Vehicle& vehicle, Turret& turret, bool safe);

		/*!
		Apply the network shell mode. This function tells a vehicle when creating shells, to create them as network objects. 	
		Normally shells are treated like bullets and locally simulated following a fired event. When network shell mode is set,
		the simulation is broadcast to the network. 

		@param vehicle - Vehicle which creates shells.  
		@param mode - If true, create shells as network objects and the simulation is broadcast to the network. If false, locally simulates.

		@return Nothing.

		@remarks This is a replication of void setNetworkShellMode(Vehicle& vehicle, bool mode) function.
		*/
		static void applyNetworkShellMode(Vehicle& vehicle,bool mode);

		/*!
		Brings an aircraft to a flying altitude. Either planes or helicopters will take off realistically to reach this height.

		@param vehicle - Vehicle to brings to a flying altitude.  

		@return double - returns flying height that will be reached.

		@remarks This is a replication of double makeAirborne(Vehicle& vehicle) function.
		*/
		static double applyAirborne(Vehicle& vehicle);

		/*!
		Applies the target flying altitude for aircraft. This can not be used to start an aircraft at the given height. 
		For this purpose use setPos combined with flyInHeight in the init line.
		Avoid too low altitudes, as helicopters and planes won't evade trees and obstacles on the ground.
		The default flying altitude depends on the "flyInHeight" definition in the vehicle's configuration. 

		@param vehicle - Vehicle to flying altitude is applied.
		@param height - Flying altitude in meters (Above Ground Level).

		@return Nothing.

		@remarks This is a replication of void flyInHeight(Vehicle& vehicle, double height) function.
		*/
		static void applyHeightFlyIn(Vehicle& vehicle, double height);

		/*!
		Applies object's orientation (given as direction and up vector).

		@param vehicle - Vehicle to orientation is applied.
		@param vectorDir - Direction vector applied to vehicle.
		@param vectorUp - Up vector applied to vehicle.
		@param relative - Parameter specifying if vectors are relative to parent or not.

		@return Nothing.

		@remarks This is a replication of void setVectorDirAndUp(Vehicle veh, vector3D vectorDir, vector3D vectorUp, bool relative) function.
		*/
		static void applyVectorDirAndUp(Vehicle veh, vector3D vectorDir, vector3D vectorUp, bool relative);

		/*!
		Force aircraft landing.

		Planes will use the airport defined by applyAirportAssignment function, or the one closest to them (in this priority).
		Helicopters will use either the nearest editor-placed helipad, or the nearest flat location (in this priority). 
		applyAirportAssignment function has no effect on helicopters, and either of them have to be within 500m, or it will not land.

		@param vehicle - Aircraft to be landed.
		@param mode - Landing Modes. Available modes,
			LAND - A complete stop.
			GET_IN - Hovering very low for another unit to get in.
			GET_OUT - Hovering low for another unit to get out.
			
		@return Nothing.

		@remarks This is a replication of void land(Vehicle& vehicle, LANDMODE mode) function.
		*/
		static void applyCustomizedLanding(Vehicle& vehicle, LANDMODE mode);

		/*!
		Assigns the airport that will be used when applying either a applyCustomizedLanding function to an AI-controlled aircraft, 
		or selecting the "Landing autopilot" action, if the aircraft is player-controlled.

		@param vehicle - Aircraft to be assigned to the airport.
		@param port - Airport code. The available airport codes are listed on the respective map pages.

		@return Nothing.

		@remarks This is a replication of void assignToAirport(Vehicle& vehicle, int port) function.
		*/
		static void applyAirportAssignment(Vehicle& vehicle, int port);

		/*!
		Orders an AI-controlled aircraft to land at a given airport.

		@param vehicle - Aircraft to be landed at the airport.
		@param port - Airport ID. The available airport codes are listed on the respective map pages.

		@return Nothing.

		@remarks This is a replication of void landTo(Vehicle& vehicle, int port) function.
		*/
		static void applyLanding(Vehicle& vehicle, int port);

		/*!
		Send message to vehicle radio channel. 
		
		@param vehicle - Vehicle that suppose to send the message.
		@param message - Message to be sent. Message is defined in description.ext, CfgRadio or VBS2_EnglishLanguageGrammar. 

		@return Nothing.

		@remarks This is a replication of void sendToVehicleRadio(Vehicle& vehicle, string name) function.
		*/
		static void applyVehicleRadioBroadcast(Vehicle& vehicle, string message);

		/*!
		Type text to vehicle radio channel.

		This function only types text to the list, it does not broadcast the message. If you want the message to
		show on all computers, you have to execute it on them.
		If you are in a crew seat (i.e. driver, gunner or commander), then it will include that role in the chat
		name output.
			Eg: Driver (you_name): "Message" 

		@param vehicle - Vehicle that suppose to send the message.
		@param message - Massage to be sent.

		@return Nothing.

		@remarks This is a replication of void vehicleChat(Vehicle& vehicle, string message) function.
		*/
		static void applyVehicleChat(Vehicle& vehicle, string message);

		/*
		Deletes the specified weapon from the cargo of a vehicle. Does nothing if the weapon isn't actually in vehicle's cargo. 

		@param vehicle - Vehicle that suppose to delete the weapon from it's cargo.
		@param weapon - Weapon to be removed.

		@return Nothing.

		@remarks This is a replication of void removeWeaponFromCargo(Vehicle& vehicle, string weapon) function.
		*/
		static void applyWeaponCargoRemove(Vehicle& vehicle, string weapon);

		/*!
		Deletes the specified magazine from the cargo of a vehicle. Does nothing if the magazine isn't actually in vehicle's cargo.
		Magazines are removed in order from fullest to emptiest.

		@param vehicle - Vehicle that suppose to delete the magazine from it's cargo.
		@param magazine - Magazine to be removed.

		@return Nothing.

		@remarks This is a replication of void removeMagazineFromCargo(Vehicle& vehicle, string magazine) function.
		*/
		static void applyMagazineCargoRemove(Vehicle& vehicle, string magazine);

		/*!
		Removes the magazine from specified turret in the vehicle.

		@param vehicle - Vehicle that suppose to delete the magazine from given turret.
		@param magazine - Magazine to be removed.
		@param turretPath - Turret that magazine is removed from.

		@return Nothing.

		@remarks This is a replication of void removeMagazineFromTurret(Vehicle& vehicle, string magazine, Turret::TurretPath& turretPath) function.
		*/
		static void applyMagazineTurretRemove(Vehicle& vehicle, string magazine, Turret::TurretPath& turretPath);

		/*!
		Add magazines to the cargo space of vehicles, which can be taken out by infantry units.

		@param vehicle - Vehicle to add the magazines to.
		@param magazineName - Magazine name.
		@param bulletCount - Number of bullets.

		@return Nothing.

		@remarks This is a replication of void addMagazineCargoEx(Vehicle& vehicle, string magazineName, int bulletCount) function.

		*/
		static void applySingleMagazineCargo(Vehicle& vehicle, string magazineName, int bulletCount);

		/*!
		Check if (and by how much) vehicle commander knows about target.

		@param vehicle - The vehicle to check.
		@param target - The target object.

		@return double - Returns how much unit knows as a number from 0 to 4.

		@remarks This is a replication of float VehicleUtilities::knowsAbout(Vehicle& vehicle, ControllableObject& target) and
		double VehicleUtilities::knowsAboutEx(Vehicle& vehicle, ControllableObject& target) functions.
		*/		
		static double getKnowsAbout(Vehicle& vehicle, ControllableObject& target);

		/*!
		Lock or Unlock the vehicle (disable mounting / dismounting) for player. 

		@param vehicle - Vehicle that should be locked/unlocked.
		@param playerLock - True/false depending on if vehicle will be locked/unlocked

		@return Nothing.

		@remarks This is a replication of void lockVehicle(Vehicle& vehicle, bool playerLock) function.
		*/
		static void applyVehicleLockEnable(Vehicle& vehicle, bool playerLock);

		/*!
		Lock or Unlock the vehicle (disable mounting / dismounting) for player. 

		@param vehicle - Vehicle that should be locked/unlocked.
		@param playerLock - Should vehicle be locked for player.
		@param allLock - Should vehicle be locked for all units including player.

		@return Nothing.

		@remarks This is a replication of void lockVehicle(Vehicle& vehicle, bool playerLock, bool allLock) function.
		*/
		static void applyVehicleLockEnable(Vehicle& vehicle, bool playerLock, bool allLock);

		/*!
		Ceases the using of the vehicle by a group. It unassigns them from the vehicle. 

		@param vehicle - Vehicle that given group leave from.
		@param group - Group to leave.

		@return Nothing.

		@remarks This is a replication of void groupLeaveVehicle(Group& group, Vehicle& vehicle) function.
		*/
		static void applyGroupLeave(Vehicle& vehicle, Group& group);

		/*!
		Ceases the using of the vehicle by a unit. It unassigns the units those who are belonging to given unit's group from the vehicle. 
		
		@param vehicle - Vehicle that given unit's group leave from.
		@param unit - Unit to leave.

		@return Nothing.

		@remarks This is a replication of void groupLeaveVehicle(Unit& unit, Vehicle& vehicle) function.
		*/
		static void applyGroupLeave(Vehicle& vehicle, Unit& unit);

		/*!
		Set if leader can issue attack commands to the soldiers in his group.

		@param vehicle - Vehicle that command is applied to.
		@param boolVal - If 'true' leader can issue attack command otherwise he can't.

		@return Nothing.

		@remarks This is a replication of void setEnableAttack(Vehicle& vehicle, bool boolVal) function.
		*/
		static void applyAttackEnable(Vehicle& vehicle, bool boolVal);

		/*!
		Sets the rotor wash effect of helicopters (applies to all helicopters in mission).
		This affects the dust created by the helicopter, as well as the wind effect on objects nearby.
		While it is not possible to turn off the dust effect altogether, it is possible to make it somewhat 
		invisible by using very high values (e.g. [100,100]) in the parameters. This only works though, 
		if there are no objects nearby that might be affected by the generated wind (e.g. trees), as they
		would then show unrealistic wind strain.

		@param strength - Force of rotor wash
		@param diameter - Maximum range

		@return bool - Returns 'true' if the rotor wash effect sets correctly otherwise 'false'.

		@remarks This is a replication of bool setRotorWash(double strength, double diameter) function.
		*/
		static bool applyRotorWash(double strength, double diameter);

		/*!
		Applies vehicle's lights states.
		(-2 forced off, -1 off (user controlled), 0 user controlled, 1 on (user controlled), 2 forced on)

		@param vehicle - Vehicle that lights states are applied to.
		@param main	- Driving lights (head lights, rear lights and brake lights).
		@param side	- Side lights (small edge indicator lights - only available on some vehicles).
		@param convoy - White rear light for convoy driving (only available on some vehicles). Activating the convoy light will disable driving and indicator lights while on.
		@param left	- Left turn signal.
		@param right - Right turn signal.

		@return Nothing.

		@remarks This is a replication of void setVehicleLights(Vehicle& vehicle,int main, int side, int convoy, int left, int right) function.
		*/
		static void applyVehicleLights(Vehicle& vehicle, int main, int side, int convoy, int left, int right);

		/*!
		Controls screen blanking during certain actions. 

		@param vehicle - Vehicle that fading is applied for.
		@param actions	- Action with an associated screen blanking. Supported actions are,
			VA_GETOUT - GETOUT
			VA_TURNOUT - TURNOUT
			VA_TURNIN - TURNIN
		@param value - If 'true' fading is enabled otherwise disabled.

		@return Nothing.

		@remarks This is a replication of void allowBlackFade(Vehicle& vehicle, VEHICLE_ACTION actions, bool value) function.
		*/
		static void applyBlackFadeEnable(Vehicle& vehicle, VEHICLE_ACTION actions, bool value);

		/*!
		Orders a unit to get out from the vehicle (silently).

		@param unit - Unit that get out from the vehicle.

		@return Nothing.

		@remarks This is a replication of void doGetOut(Unit& unit) function.
		*/
		static void applyGetOut(Unit& unit);

		/*!
		Orders units to get out from the vehicle (silently).

		@param unitVec - Units that get out from the vehicle.

		@return Nothing.
		*/
		static void applyGetOut(vector<Unit> unitVec);

		/*!
		Orders the unit to get out from the vehicle (via the radio). 
		This command is only effective when used on local units. When used on a remote unit, this command will create radio 
		dialog on the machine the unit is local to, but the unit will not leave the vehicle.

		@param unit - Unit to get out from the vehicle.

		@return Nothing.

		@remarks This is a replication of void commandGetOut(Unit& unit) function.
		*/
		static void applyCommandGetOut(Unit& unit);

		/*!
		Attach a statement to a Vehicle. The statement is propagated over the network in MP games, it can be executed by invoking processInitCommands. 
		
		@param vehicle - Vehicle that statement is attached to.
		@param statement - Statement to be attached.

		@return Nothing.

		@remarks This is a replication of void setInitStatement(Vehicle& vehicle, string& statement) function.
		*/
		static void applyInitStatement(Vehicle& vehicle, string statement);

		/*!
		Clear the vehicle's init statements.

		@param vehicle - Vehicle that statements are cleared from.

		@return Nothing.

		@remarks This is a replication of void clearInitStatement(Vehicle& vehicle) function.
		*/
		static void applyInitStatementClear(Vehicle& vehicle);

		/*!
		Adds weapons to the weapon cargo space. This is used for infantry weapons. MP synchronized.
		
		@param vehicle -  Vehicle to add the weapons to 
		@param weapon - Weapon to be added. Class name of the weapon should be stetted. 
		@param count - Number of weapons to add.

		@return Nothing.

		@remarks This is a replication of void addWeaponToCargoGlobal(Vehicle& vehicle, Weapon& weapon, int count) function.
		*/
		static void applyWeaponCargoGlobal(Vehicle& vehicle, Weapon& weapon, int count);

		/*!
		Adds magazines to the cargo space of vehicles, which can be taken out by infantry units. MP synchronized.

		@param vehicle -  Vehicle to add the magazines to.
		@param magName - Class name of the magazine. 
		@param count - Number of magazines to add.

		@return Nothing.

		@remarks This is a replication of void addMagazineToCargoGlobal(Vehicle& vehicle, Weapon& weapon, int count) function.
		*/
		static void applyMagazineCargoGlobal(Vehicle& vehicle, string magName, int count);

		/*!
		Removes all weapons from the vehicle cargo space. MP synchronized.

		@param vehicle - Vehicle to clear weapon cargo. 

		@return Nothing.

		@remarks This is a replication of void clearWeaponCargoGlobal(Vehicle& vehicle) function.
		*/
		static void applyWeaponCargoRemoveGlobal(Vehicle& vehicle);

		/*!
		Removes all magazines from the vehicle cargo space. MP synchronized.

		@param vehicle - Vehicle to clear magazine cargo. 

		@return Nothing.

		@remarks This is a replication of void clearMagazineCargoGlobal(Vehicle& vehicle) function.
		*/
		static void applyMagazineCargoRemoveGlobal(Vehicle& vehicle);

		/*!
		Order the given vehicle to move to the given location (via the radio). It is only for local objects.

		@param vehicle - Vehicle to move. 
		@param position - Position that vehicle is moved to.

		@return Nothing.

		@remarks This is a replication of void commandMove(Vehicle& vehicle, position3D position) function.
		*/
		static void applyCommandMove(Vehicle& vehicle, position3D position);

		/*!
		Forces the vehicle to fire from the specifically named weapon.

		@param vehicle - Vehicle to fire.
		@param weaponName - Name of the weapon which is fired from.
 
		@return Nothing.

		@remarks This is a replication of void fire(Vehicle& vehicle, string weaponName) function.
		*/
		static void applyFire(Vehicle& vehicle, string weaponName);

		/*!
		Forces the vehicle to fire from the specifically named weapon.

		@param vehicle - Vehicle to fire.
		@param muzzleName - Name of the muzzle which is fired from.
		@param modeName - Mode of the weapon.
 
		@return Nothing.

		@remarks This is a replication of void fire(Vehicle& vehicle, string muzzleName, string modeName)function.
		*/
		static void applyFire(Vehicle& vehicle, string muzzleName, string modeName);

		/*!
		Enables or disables specific sound types for a vehicle.

		@param vehicle - vehicle - Vehicle to be enabled or disabled sound type.
		@param types - Sound types. Available sound types are;
			- SOUNDENGINE - Engine
			- SOUNDENVIRON - Movement sounds (e.g. tank tracks)
			- SOUNDCRASH - Impact with another object
			- SOUNDDAMMAGE - Alarm sound if vehicle was hit
			- SOUNDLANDCRASH - Impact with land (e.g. aircraft crash)
			- SOUNDWATERCRASH - Impact with water (e.g. aircraft crash)
			- SOUNDGETIN - Entering vehicle
			- SOUNDGETOUT - Exiting vehicle
			- SOUNDSERVO - Vehicle attachments (e.g. tank turrets)
		Be aware that not all of these sounds are defined in every vehicle.
		@param value - Whether sound type should be played or not. If 'true' sound type will be played.

		@return Nothing. 

		@remarks This is a replication of void allowSound(Vehicle& vehicle,VEHICLE_SOUND_TYPES types,bool value) function.
		*/
		static void applySoundEnable(Vehicle& vehicle, VEHICLE_SOUND_TYPES types, bool value);
		
		/*!
		Return the path to the turret (as a TurretPath object) the unit is in, on the given vehicle.'VehicleUtilities::updateVehicle' and 
		'VehicleUtilities::updateTurrets' must be called on the vehicle object, before calling this function.
		@param vehicle - A Vehicle.
		@param unit - A unit that mounted inside the vehicle.
		@return Turret::TurretPath - The Turret Path of the turret which the unit is mounted on.If the unit is not mounted on 
		the given vehicle OR the unit is mounted on some other crew position other than a turret, an empty Turret Path will be returned.
		The function Returns an empty TurretPath, If the Vehicle is not active or if the Unit is not active or if the Unit is not in the Vehicle 
		or if the Vehicle dosent have any VehicleArmedModule or	if the Unit is not in a GUNNER/COMMANDER position.
		*/
		static Turret::TurretPath getTurretPathOfUnit(Vehicle& vehicle, Unit& unit);

		/*!
		Calculated from the velocity of the vehicle. 
		@param vehicle - A vehicle that suppose to get velocity.
		@return double - Returns the absolute speed of the vehicle in km/h.
		*/
		static double getAbsoluteSpeed(Vehicle& vehicle);

		/*!
		Returns the path to the turret (as a TurretPath object), if a turret exists by the given turretName on the given vehicleType.
		@param vehicleType - A vehicle type as a string ( e.g. "vbs2_cz_army_bmp2_w_x" ).
		@param turretName - A turret on that 'vehicle type' (e.g. "maingun" ).
		@return Turret::TurretPath - If the vehicle type doesn't exist OR no turret found in that vehicle type which has that turretName, an
		empty Turret Path is returned
		*/
		static Turret::TurretPath getTurretPathByName(string vehicleType, string turretName);

		/*!
		@description

		"Respawns" the vehicle passed as a parameter. The initial vehicle is deleted and a new vehicle is created. Units may lose their seat in some vehicles.

		@locality  

		Globally Applied Globally Effected

		@version [VBS2Fusion v3.10.1]

		@param vehicle - Vehicle that is suppose to Respawn.

		@return bool - If false some unit or units might have lost their seats. (ex: If vehicle has two gunner's position one gunner might be not in vehicle after respawns)

		@example

		@code

		//Vehicle to be created
		Vehicle v;

		//Apply the applyVehicleRespawn function to the vehicle v
		VehicleUtilities::applyVehicleRespawn(v);

		@endcode

		@relates to 

		@overloaded

		@remarks To obtain a vehicle created in a client machine within a network, use the function MissionUtilities::loadMission(Mission& mission) to obtain the properties of the objects created by all clients in the network.
		*/
		static bool applyVehicleRespawn(Vehicle& vehicle);
		
		/*!
		@description 

		Checks whether the specified vehicle is currently on top of another vehicle (e.g. a trailer). Only works with physX vehicles.
		
		@locality

		Locally Effected

		@version  [VBS2Fusion v3.12]

		@param vehicle - Specified vehicle that suppose to check whether top of another vehicle or not.

		@return bool - If true then on top of another vehicle.

		@example

		@code

		//The vehicle to be created
		Vehicle vehi;

		VehicleUtilities::isDrivingOn(vehi);

		@endcode

		@overloaded  

		None
		
		@related

		None

		@remarks This checks if a vehicle is on top of a Trailer vehicle type. Would work accordingly for object typ "VBS2_CA_ARMY_HET_TRAILER_D_X"
		*/
		static bool isDrivingOn(Vehicle& vehicle);

		/*!
		@description 
		Returns a list of groups that are in cargo of the passed vehicle. Each group in the return list need to be updated.

		@locality
		
		Locally effected, locally applied

		@version  [VBS2Fusion v3.12.1]

		@param vehicle - A passed vehicle.

		@return list<Group> - list of groups.

		@example

		@code
				
		//Create vehicle and units (groups)
		playerUnit = MissionUtilities::getPlayer();
		UnitUtilities::updateStaticProperties(playerUnit);
		UnitUtilities::updateDynamicProperties(playerUnit);
		position3D playerPos (playerUnit.getPosition().getX(),playerUnit.getPosition().getZ(),0);

		position3D newPosition (playerPos + position3D(20,20,0));
		tank.setPosition(newPosition);
		string type = "vbs2_us_mc_mtvr_bak_d_m2_x";
		tank.setType(type);
		VehicleUtilities::createVehicle(tank);
		VehicleUtilities::updateVehicle(tank);

		position3D newU1Pos (playerPos + position3D(10,0,0));
		UnitUtilities::CreateUnit(u1,newU1Pos);

		position3D newU2Pos (playerPos + position3D(12,0,0));
		UnitUtilities::CreateUnit(u2,newU2Pos);

		//Get-in as cargo
		VehicleUtilities::moveInAndAsCargo(tank,u1);
		VehicleUtilities::moveInAndAsCargo(tank,u2);

		//Returns the cargo groups
		VehicleUtilities::updateVehicle(tank);
		list<Group> myList =VehicleUtilities::getCargoGroups(tank);
		for (list<Group>::iterator itr = myList.begin(), itr_end = myList.end(); itr != itr_end; ++itr)
		{
			displayString+="\\nThe name is "+itr->getName();
		}

		@endcode

		@overloaded

		@related
		void GroupUtilities::updateGroup(Group& group)

		@remarks
		This function works for most of the cargo vehicles. It will not work for non cargo vehicles.

		*/
		static list<Group> getCargoGroups(Vehicle& vehicle);

		/*!
		@description 
		Returns the vehicle position occupied by the passed unit.

		@locality
		
		Globally effected

		@version  [VBS2Fusion v3.15]

		@param vehicle - A passed vehicle.
		@param unit - Unit that suppose to find crew position.

		@return CrewPos - This is the struct that holds crew position description. If instance is crewDesc
					crewDesc.type - Proxy type. Can be "driver", "gunner", "commander" or "cargo".
					crewDesc.position - Seat position in model space (i.e. relative to the vehicle).
					crewDesc.cargoID - Index of cargo position.
					crewDesc.unit - Unit occupying that seat. objNull if empty.
					crewDesc.turret - Turret path. For non-gunner positions this element is not returned. 

		@example

		@code
		
		//Create vehicle and unit
		position3D playerPos (playerUnit.getPosition().getX(),playerUnit.getPosition().getZ(),0);
		position3D newPosition (playerPos + position3D(20,20,0));
		lorry.setPosition(newPosition);
		string type = "vbs2_us_mc_mtvr_d_m240_x";
		lorry.setType(type);
		VehicleUtilities::createVehicle(lorry);
		VehicleUtilities::updateVehicle(lorry);

		position3D driverPos (playerPos + position3D(10,0,0));
		driver.setPosition(driverPos);
		driver.setName(string("driver"));
		UnitUtilities::CreateUnit(driver);

		//Get-in as driver
		VehicleUtilities::moveInAndAsDriver(lorry,driver);
		VehicleUtilities::updateVehicle(lorry);

		//Obtain crew details
		CrewPos driverDetails = VehicleUtilities::getCrewPosition(lorry,driver);
		displayString +="\\nDriver type: "+ driverDetails.type;
		displayString +="\\nDriver position: "+ driverDetails.pos.getVBSPosition();
		displayString +="\\nDriver cargoID: "+ conversions::IntToString(driverDetails.cargoID);
		displayString +="\\nDriver unit: "+ driverDetails.unit.getName();
		Turret::TurretPath TPDriver = driverDetails.turret;
		for (vector<int> :: iterator itrD = TPDriver.begin();itrD != TPDriver.end(); itrD++)
		{
			displayString +="\\nDriver TurretPath: "+ conversions::IntToString(*itrD);
		}
		
		@endcode

		@overloaded
		vector<CrewPos> getCrewPosition(Vehicle& vehicle)

		@related

		@remarks
		*/
		static CrewPos getCrewPosition(Vehicle& vehicle, Unit& unit);

	private:

		/*!
		check for existence of the group when creating a new vehicle 
		if there isn't any group, create random group and add to it.
		*/
		static Group createGroup(Vehicle& vehicle);


	};
};
#endif //VEHICLE_UTILITIES_H