
 /**************************************************************************
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 **************************************************************************/
/*************************************************************************

Name:

WeaponUtilities.h

Purpose:

This file contains the declaration of the WeaponUtilities class.

Version Information:

Version		Date		Author and Comments
===========================================
1.0			29-01/2010	YFP: Original Implementation

/************************************************************************/

#ifndef VBS2FUSION_WEAPONUTILITIES_H
#define VBS2FUSION_WEAPONUTILITIES_H

/**************************************************************************
To disable the warnings arise because of using deprecated ArmedGroundVehicle 
inside the WeaponUtilities class.
Warning identifier [C:4996]
/**************************************************************************/
#pragma warning (push)
#pragma warning (disable: 4996)

/***********************************************************************/
/* INCLUDES
/***********************************************************************/
// STANDARD INCLUDES
#include <string>

// SIMCENTRIC INCLUDES
#include "VBS2Fusion.h"
#include "data/ArmedGroundVehicle.h"
#include "data/Projectile.h"
/**********************************************************************/
/* END INCLUDES
/**********************************************************************/

namespace VBS2Fusion
{

	class VBS2FUSION_API WeaponUtilities
	{
	public:
	
		/*! Get the elevation of the weapon in specified turret of the Armed Vehicle from VBS2 Environment. */
		static double getElevation(Vehicle& vehicle, Turret& turret, Weapon& weapon);

		/*! Get azimuth of the weapon in specified turret of the armed vehicle from VBS2 Environment.  */
		static double getAzimuth(Vehicle& vehicle, Turret& turret, Weapon& weapon);

		/*!
		Gets ammo count of the weapon in the vehicle. If weapon is given with invalid weaponClassName or vehicle
		doesn't have armed features then function will return 0. If function is not executed, return value would 
		be -1. Before call the function, Correct weaponClassName needs to be set to the weapon object.
		*/
		static int getAmmoCount(Vehicle& vehicle, Weapon& weapon);

		/*! Get the aiming direction of the specified Weapon of the turret in armed vehicle.
		This method returns a unit vector of the direction. 
		*/
		static position3D getWeaponDirectionVector(Vehicle& vehicle, Turret& turret, Weapon& weapon);

		/*! set reload state of the weapon.
		Deprecated. Use applyWeaponState(Vehicle& vehicle, Weapon& weapon, bool reloadStatus, double reloadTime)
		*/
		VBS2FUSION_DPR(UWPN006)static void setWeaponState(Vehicle& vehicle,Weapon& weapon, bool reloadStatus,double reloadTime);

		/*! Get the elevation of the weapon in specified turret of the Armed Vehicle from VBS2 Environment. */
		VBS2FUSION_DPR(UWPN003) static double getElevation(ArmedGroundVehicle& vehicle, Turret& turret, Weapon& weapon);

		/*! Get azimuth of the weapon in specified turret of the armed vehicle from VBS2 Environment.  */
		VBS2FUSION_DPR(UWPN002) static double getAzimuth(ArmedGroundVehicle& vehicle, Turret& turret, Weapon& weapon);

		/*! 
		Gets ammo count of the weapon in the armed vehicle from VBS2 Environment. If weapon is given with invalid 
		weaponClassName then function will return 0. If function is not executed, return value would be -1. Before 
		call the function, Correct weaponClassName needs to be set to the weapon object.
		*/
		VBS2FUSION_DPR(UWPN001) static int getAmmoCount(ArmedGroundVehicle& vehicle, Weapon& weapon);

		/*! Get the aiming direction of the specified Weapon of the turret in armed vehicle.
		This method returns a unit vector of the direction. 
		*/
		VBS2FUSION_DPR(UWPN004) static position3D getWeaponDirectionVector(ArmedGroundVehicle& vehicle, Turret& turret, Weapon& weapon);

		/*! set reload state of the weapon.
		Deprecated. Use applyWeaponState(Vehicle& vehicle, Weapon& weapon, bool reloadStatus, double reloadTime)
		*/
		VBS2FUSION_DPR(UWPN005) static void setWeaponState(ArmedGroundVehicle& vehicle,Weapon& weapon, bool reloadStatus,double reloadTime);

		/*! 
		Creates a shot of the specified type, position and velocity
		*/
		static Projectile createShot(string ammoType,position3D pos, vector3D velocity);

		/*! 
		Enables shot creation via mouse clicks. The shot will be created at the player's location, and move
		towards	the position defined by the mouse. The mouse click can either be generated via an in-game 
		dialog, or via a windows message
		Deprecated. Use applyShotCreationEnable(bool enable)
		*/
		VBS2FUSION_DPR(UWPN007) static void allowCreateShot(bool enable);

		/*! 
		Returns the unit or vehicle that fired the given shot or laser target.
		*/
		static ControllableObject getShotOwner(Projectile& proj);

		/*! 
		Makes explosive shell inert. Shell wont create any visual effects or damage and will disappear.
		Deprecated. Use applyDummyRound(Projectile& proj, bool enable = true)
		*/
		VBS2FUSION_DPR(UWPN008) static void setDummyRound(Projectile& proj, bool enable = true);

		/*!
		Returns an array of muzzles used by the input weapon class.
		*/
		static vector<string> GetMuzzles(string weaponClass);

		/*! 
		Creates a shot of the specified type, position and velocity
		sound - Class name of weapon whose sound to play when shot is created.
		tracer - If true, then a tracer shot will be created.
		*/
		static Projectile createShot(string ammoType,position3D pos, vector3D velocity, string sound, bool tracer);

		/*!
		@description 
		Selects the weapon to be used via allowCreateShot created shots.

		@locality
		Locally Applied, Locally Effected

		@version  [VBS2Fusion v3.11]

		@param slot � Slot that can be passed through the GetMessageExtraInfo value in the WM_LBUTTONDOWN windows message (Default: 0).
		@param weapon - Weapon class name to be used by WeaponUtilities::allowCreateShot(bool enable) function.

		@return Nothing.

		@example

		@code

		WeaponUtilities::applyLasershotWeapon(0,"M136")

		@endcode

		@overloaded None.

		@related
		void WeaponUtilities::allowCreateShot(bool enable)

		@remarks 	
		*/
		static void applyLasershotWeapon(int slot, string weapon);

		/*!
		@description  

		Used to set the reload state of a weapon within the VBS2 environment. 

		@locality
		
		Locally applied Globally effected

		@version [VBS2Fusion v3.12] 

		@param vehicle - Type of vehicle.
		@param weapon - Type of weapon.
		@param reloadStatus - Reload status true or false.
		@param reloadTime - Time to reload.

		@return nothing.

		@example

		@code
		
		//The vehicle for which the weapon state is changed
		Vehicle veh;
		Weapon w;
		Turret::TurretPath turretPath;
		turretPath.push_back(0);

		w = VehicleUtilities::getWeaponState(veh,turretPath);

		WeaponUtilities::applyWeaponState(veh,w,true,0);

		@endcode

		@overloaded 

		None.

		@related  

		@remarks This is a replication of void setWeaponState(Vehicle& vehicle, Weapon& weapon, bool reloadStatus, double reloadTime);
		@remarks This function is mainly applicable to vehicles.
		*/
		static void WeaponUtilities::applyWeaponState(Vehicle& vehicle, Weapon& weapon, bool reloadStatus, double reloadTime);

		/*!
		@description 

		Enables shot creation via mouse clicks. The shot will be created at the player's location, and move
		towards	the position defined by the mouse. The mouse click can either be generated via an in-game 
		dialog, or via a windows message

		@locality

		Locally Applied Globally Effected

		@version  [VBS2Fusion v3.12]

		@param enable � Set shot creation via mouse clicks true or false.

		@return Nothing.

		@example

		@code

		//disable the shot creation via mouse click
		WeaponUtilities::applyShotCreationEnable(false);

		@endcode

		@overloaded 
		
		None.

		@related

		None

		@remarks This is a replication of void allowCreateShot(bool enable)
		*/
		static void applyShotCreationEnable(bool enable);

		/*!
		@description 
		Makes explosive shell inert. Shell wont create any visual effects or damage and will disappear.

		@locality

		Globally effected

		@version  [VBS2Fusion v3.12]

		@param enable � Slot that can be passed through the GetMessageExtraInfo value in the WM_LBUTTONDOWN windows message (Default: 0).

		@return Nothing.

		@example

		@code

		WeaponUtilities::applyDummyRound(grenade1,true);

		@endcode

		@overloaded None.

		@related

		@remarks This is a replication of void setDummyRound(Projectile& proj, bool enable = true);
		*/
		static void applyDummyRound(Projectile& proj, bool enable = true);

#if _BISIM_DEV_EXTERNAL_POSE
    /*! 
		Returns bounding box of specified weapon type for given unit.
		*/
    static void getWeaponBBox(Unit& unit, string type, Vector3f &bbMin, Vector3f &bbMax);

    /*! 
		Returns name of specified weapon type for given unit.
		*/
    static string getWeaponName(Unit& unit, string type);

    /*! 
		Returns muzzle camera position of specified weapon type for given unit.
		*/
    static Vector3f getMuzzleCameraPos(Unit& unit, string type);

    /*! 
		Returns muzzle position of specified weapon type for given unit.
		*/
    static Vector3f getMuzzlePos(Unit& unit, string type);

    /*! 
		Returns weight of specified weapon type for given unit.
		*/
    static float getWeaponWeight(Unit& unit, string type);

    /*! 
		Returns reload time of specified weapon type for given unit.
		*/
    static float getWeaponReloadTime(Unit& unit, string type);

    /*! 
		Returns zoom of specified weapon type for given unit.
		*/
    static void getMuzzleOpticsZoom(Unit& unit, string type, float &opticsZoomMin, float &opticsZoomMax);

    /*! 
		Returns information about burst count and reload time for given muzzle in given mode.
		*/
    static bool getMuzzleInfo(Unit& u, string type, int muzzle, int mode,int& burst, float& reloadTime);

    /*! 
		Returns maximal burst for given weapon type. 
    Also returns information if weapon is fully automatic
		*/
    static void getWeaponMaxBurst(Unit& u, string type, int& maxBurst, bool& fullAuto);
#endif

	};

#pragma warning (pop) // Enable warnings [C:4996]

};

#endif

