
 /**************************************************************************
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 **************************************************************************/

/*****************************************************************************

Name:
	PlayerUtilities.h
Purpose:
	This file contains the declaration of the PlayerUtilities class.
	
Version Information:

	Version		Date		Author and Comments
	===========================================
	1.0			17-09/2010	YFP: Original Implementation
	2.0			10-01/2011  CHS: Added getPlayerPosition()
	2.01		08-09/2011	NDB: Added Methods,
									bool isViewPlayer()
									OPTICSTATE getOpticsState()
									vector<double> getOpticsDOF()
									void applySuspendPlayer(bool suspend)
									bool isPlayerSuspended()
									int getNoOfPlayerSuspended()
									bool isJIPed()
	2.02		31-01-2012	RDJ		Add method
									float GetOpticsBrightness
	2.03		31-01-2012	RDJ		Add method
									float GetOpticsBrightnessMin 
									

/*****************************************************************************/

#ifndef VBS2FUSION_PLAYER_UTILITIES_H
#define VBS2FUSION_PLAYER_UTILITIES_H

/*****************************************************************************/
/* INCLUDES
/*****************************************************************************/
// STANDARD INCLUDES
#include <string>

// SIMCENTRIC INCLUDES
#include "VBS2Fusion.h"
#include "VBS2FusionDefinitions.h"
/*****************************************************************************/
/* END INCLUDES
/*****************************************************************************/

namespace VBS2Fusion
{
	class VBS2FUSION_API PlayerUtilities
	{
	public:
		/*!
			get player unit of the mission. 
		*/
		static Unit getPlayer();

		/*!
		set specified unit as player.player should be a playble unit.
		Deprecated. Use bool applyPlayer(Unit& player)
		*/
		VBS2FUSION_DPR(UPLY021) static bool setPlayer(Unit& player);

		/*!
		set unit as playable unit allowing MP role for the unit. 
		Deprecated. Use void applyPlayable(Unit& player, bool playable)
		*/
		VBS2FUSION_DPR(UPLY001) static void setPlayable(Unit& player);

		/*!
			update the player unit of the mission. 
		*/
		static void updatePlayer(Unit& player);

		/*!
			check weather the specified unit is player.
		*/
		static bool isPlayer(Unit& unit);

		/*!
		switch player between defined units.  	
		Deprecated. Use bool isSwitchPlayer(Unit& nplayer , Unit& oplayer)
		*/
		VBS2FUSION_DPR(UPLY002) static bool switchPlayer(Unit& nplayer , Unit& oplayer);

		/*!
			get the player position of the mission. 
		*/
		static position3D getPlayerPosition();

		/*!
		Forces the player to fire using currently selected Weapon.
		Deprecated. Use void applyFiringForce(Unit& player)
		*/
		VBS2FUSION_DPR(UPLY003) static void forceFire(Unit& player);

		/*!
		Returns optics information for the player.

		To convert the returned zoom level into an FOV value, use 0.25/zoom.
		atan(FOV)*2 will then return the FOV in degrees. 
		*/
		static OPTICSTATE getOpticsState();

		/*!
		Returns the player's current focal settings. [focal plane, blur factor]
		*/
		static vector<double> getOpticsDOF();

		/*!
		Disables player from manipulating his avatar. Player can still access the GUI, though.
		Command is cumulative (i.e. if it is applied twice with a true argument, it also has to be applied 
		twice with a false argument, in order to enable player input again.) 
		*/
		static void applySuspendPlayer(bool suspend);

		/*!
		Returns player's suspension status
		*/
		static bool isPlayerSuspended();

		/*!
		Returns the level of player suspensions, as set by multiple, nested suspendPlayer commands.
		(Nested suspends can happen if the player is first suspended while the editor is open, and suspended another time, while the editor is open, due to shell shock effects.) 
		*/
		static int getNoOfPlayerSuspended();

#ifdef DEVELOPMENT

		/*!
		Checks if a player joined a MP game while it was running (Joint-in-Progress). 
		*/
		static bool isJIPed();

#endif
		/*!
		Returns the brightness of current optical enhancement. 
		return value: float
		Deprecated. Use float getOpticsBrightness()
		*/
		VBS2FUSION_DPR(UPLY004) static float GetOpticsBrightness();

		/*!
		Returns the minimum brightness of current optical enhancement. 
		return value:float
		Deprecated. Use float getOpticsBrightnessMin()
		*/
		VBS2FUSION_DPR(UPLY005) static float GetOpticsBrightnessMin();

		/*!
		Returns the maximum brightness of current optical enhancement. 
		return value:float
		Deprecated. Use float getOpticsBrightnessMax()
		*/
		VBS2FUSION_DPR(UPLY006) static float GetOpticsBrightnessMax();

		/*!
		Returns the maximum gain of current optical device. 
		return value:float
		Deprecated. Use float getOpticsMaxGainAmplifier()
		*/
		VBS2FUSION_DPR(UPLY007) static float GetOpticsMaxGainAmplifier();

		/*!
		Returns the optics brightness offset
		return value:float
		Deprecated. Use float getOpticsBrightnessOffset()
		*/
		VBS2FUSION_DPR(UPLY008) static float GetOpticsBrightnessOffset();

		/*!
		Returns the autoBC mode of the active device
		return value:bool
		Deprecated. Use bool getOpticsAutoBC()
		*/
		VBS2FUSION_DPR(UPLY009) static bool GetOpticsAutoBC();

		/*!
		Return the blur level has been set
		return value:float
		Deprecated. Use float getOpticsBlurCoef()
		*/
		VBS2FUSION_DPR(UPLY010) static float GetOpticsBlurCoef();

		/*!
		Returns the brightness of current optical enhancement. 
		return value: double
		Deprecated. Use doble getOpticsBrightnessEx()
		*/
		VBS2FUSION_DPR(UPLY023) static double GetOpticsBrightnessEx();

		/*!
		Returns the minimum brightness of current optical enhancement. 
		return value: double
		Deprecated. Use double getOpticsBrightnessMinEx()
		*/
		VBS2FUSION_DPR(UPLY024) static double GetOpticsBrightnessMinEx();

		/*!
		Returns the maximum brightness of current optical enhancement. 
		return value - double 
		Deprecated. Use double getOpticsBrightnessMaxEx()
		*/
		VBS2FUSION_DPR(UPLY025) static double GetOpticsBrightnessMaxEx();

		/*!
		Returns the maximum gain of current optical device. 
		return value - double 
		Deprecated. Use double getOpticsMaxGainAmplifierEX()
		*/
		VBS2FUSION_DPR(UPLY026) static double GetOpticsMaxGainAmplifierEx();

		/*!
		Returns the optics brightness offset
		return value - double 
		Deprecated. Use double getOpticsBrightnessOffsetEx()
		*/
		VBS2FUSION_DPR(UPLY027) static double GetOpticsBrightnessOffsetEx();

		/*!
		Return the blur level has been set
		return value - double 
		Deprecated. Use double getOpticsBlurCoefEx()
		*/
		VBS2FUSION_DPR(UPLY028) static double GetOpticsBlurCoefEx();

		/*!
		Set the brightness of current optical enhancement. 
		*/
		static void setOpticsBrightness(float brightnessVal);	

		/* 
		Sets the minimum brightness of current optical enhancement. 
		*/
		static void setOpticsBrightnessMin(float minBrightVal);

		/*!
		Sets the maximum brightness of current optical enhancement. 
		*/
		static void setOpticsBrightnessMax(float maxBrightVal);

		/*!
		Sets the maximum gain of current optical device. 
		Only works for units' NV equipment, not for vehicle-mounted ones.
		*/
		static void setOpticsMaxGainAmplifier(float maxGainAmp);

		/*!
		Set the optics brightness offset
		*/
		static void setOpticsBrightnessOffset(float brightOff);

		/*!
		Set the brightness of current optical enhancement. 
		Deprecated. Use void applyOpticsBrightness(double brightnessVal)
		*/
		VBS2FUSION_DPR(UPLY029) static void setOpticsBrightness(double brightnessVal);

		/*! 
		Sets the minimum brightness of current optical enhancement. 
		Deprecated. Use void applyOpticsBrightnessMin(double minBrightVal)
		*/
		VBS2FUSION_DPR(UPLY030) static void setOpticsBrightnessMin(double minBrightVal);
	
		/*!
		Sets the maximum brightness of current optical enhancement. 
		Deprecated. Use void applyOpticsBrightnessMax(double maxBrightVal) 
		*/
		VBS2FUSION_DPR(UPLY031) static void setOpticsBrightnessMax(double maxBrightVal);

		/*!
		Sets the maximum gain of current optical device. 
		Only works for units' NV equipment, not for vehicle-mounted ones.
		Deprecated. Use void applyOpticsMaxGainAmplifier(double maxGainAmp)
		*/
		VBS2FUSION_DPR(UPLY032) static void setOpticsMaxGainAmplifier(double maxGainAmp);

		/*!
		Set the optics brightness offset
		Deprecated. Use void applyOpticsBrightnessOffset(double brightOff)
		*/
		VBS2FUSION_DPR(UPLY033) static void setOpticsBrightnessOffset(double brightOff);
	
		/*!
		set the autoBC mode of the active device
		Deprecated. Use void applyOpticsAutoBC(bool mode)
		*/
		VBS2FUSION_DPR(UPLY016) static void setOpticsAutoBC(bool mode);

		/*!
		Sets the level of blur of the TI device.
		Value is from 0 to 1. 0 creates a sharp picture, 1 creates a grainy/blury picture. 
		*/
		static void setOpticsBlurCoef(float blurAmount);
		
		/*!
		Sets the level of blur of the TI device.
		Value is from 0 to 1. 0 creates a sharp picture, 1 creates a grainy/blury picture. 
		Deprecated. Use void applyOpticsBlurCoef(double blurAmount)
		*/
		VBS2FUSION_DPR(UPLY034) static void setOpticsBlurCoef(double blurAmount);
		
		/*!
		Sets player's focal plane.
		Overrides the difficulty option "Use DOF for optics".
		(If DOF for optics should be used and the current video settings do not 
		support it (it's currently tight with level of post process effects), 
		then a text message appears on the screen.)
		Deprecated. Use void applyOpticsDOF(double focus, double blur)
		*/
		VBS2FUSION_DPR(UPLY018) static void setOpticsDOF(double focus, double blur);

		/*!
		Enables player control functions (fwd, left, back etc) to operate even if a dialog is present. 
		Deprecated. Use void applyAllowControlslnDialogMove(bool boolVal)
		*/
		VBS2FUSION_DPR(UPLY019) static void allowMovControlsInDialog(bool boolVal);

		/*!
		Enables commanding of subordinates even if player is suspended.
		If true, player can give orders independently of his suspension status.
		Deprecated. Use void applyEnableCommanding(bool commanding)
		*/
		VBS2FUSION_DPR(UPLY020) static void enableCommanding(bool commanding);

		/*!
		set unit as playable unit allowing MP role for the unit. 
		@param player - The unit that suppose to set as playable.
		@param playable - If this is true unit set to playable.
		@return Nothing.
		@remarks This is a replication of setPlayable(Unit& player)
		*/
		static void applyPlayable(Unit& player, bool playable);

		/*!
		switch player between defined units.  
		@param nplayer - Player unit that supposed to switched and this should be playable unit.
		@param oplayer - Current player unit.
		@return bool - If switched succeed function return "true"  and else "false".
		@remarks This is a replication of switchPlayer(Unit& nplayer , Unit& oplayer)
		*/
		static bool isSwitchPlayer(Unit& nplayer , Unit& oplayer);

		/*!
		Forces the player to fire using currently selected Weapon.
		@param unit - The unit that suppose to force for firing.
		@return Nothing.
		@remarks This is a replication of forceFire(Unit& player)
		*/
		static void applyFiringForce(Unit& player);

		/*!
		Returns the brightness of current optical enhancement. 
		@return value: float
		@remarks This is a replication of GetOpticsBrightness()
		*/
		static float getOpticsBrightness();

		/*!
		Returns the minimum brightness of current optical enhancement. 
		@return value:float
		@remarks This is a replication of GetOpticsBrightnessMin()
		*/
		static float getOpticsBrightnessMin();

		/*!
		Returns the maximum brightness of current optical enhancement. 
		@return value:float
		@remarks This is a replication of GetOpticsBrightnessMax()
		*/
		static float getOpticsBrightnessMax();

		/*!
		Returns the maximum gain of current optical device. 
		@return value:float
		@remarks This is a replication of GetOpticsMaxGainAmplifier()
		*/
		static float getOpticsMaxGainAmplifier();

		/*!
		Returns the optics brightness offset
		@return value:float
		@remarks This is a replication of GetOpticsBrightnessOffset()
		*/
		static float getOpticsBrightnessOffset();

		/*!
		Returns the autoBC mode of the active device
		@return value:bool
		@remarks This is a replication of GetOpticsAutoBC()
		*/
		static bool getOpticsAutoBC();

		/*!
		Return the blur level has been set
		@return value:float
		@remarks This is a replication of GetOpticsBlurCoef()
		*/
		static float getOpticsBlurCoef();

		/*!
		Controls the automatic setting of brightness and contrast in night vision equipment.
		@param mode - If the mode is "true" ,enable the automatic setting of brightness and contrast in night vision equipment.
		@return nothing.
		@remarks This is a replication of setOpticsAutoBC(bool mode)
		*/
		static void applyOpticsAutoBC(bool mode);

		/*!
		Sets player's focal plane.Overrides the difficulty option "Use DOF for optics".
		(If DOF for optics should be used and the current video settings do not 
		support it (it's currently tight with level of post process effects), 
		then a text message appears on the screen.)
		@param focus - [-1]: no override (default), [0]: use auto focus, [> 0]: distance of the focal plane, in meters
		@param blur - Number: Blur factor, 0: no blur, 1: default blur
		@return Nothing.
		@remarks This is a replication of setOpticsDOF(double focus, double blur)
		*/
		static void applyOpticsDOF(double focus, double blur);

		/*!
		Enables player control functions (fwd, left, back etc) to operate even if a dialog is present. 
		@param boolVal - If value is "true" enable player control function and else disable.
		@return Nothing.
		@remarks This is a replication of allowMovControlsInDialog(bool boolVal)
		*/
		static void applyAllowControlslnDialogMove(bool boolVal);

		/*!
		Enables commanding of subordinates even if player is suspended.	Then player will now not be able to 
		move or select or issue commands to his squad members. Using this function can select his squad members
		(with the FunctionKEYs, F1, F2 etc), and issue commands to them with the squad radio using the number keys
		even though the player still cannot move or shoot himself
		@param commanding - If true, player can give orders independently of his suspension status.
		@return Nothing.
		@remarks This is a replication of enableCommanding(bool commanding)
		*/
		static void applyEnableCommanding(bool commanding);

		/*!
		Returns the brightness of current optical enhancement. 
		@return value: double
		@remarks This is a replication of GetOpticsBrightnessEx()
		*/
		static double getOpticsBrightnessEx();

		/*!
		Returns the minimum brightness of current optical enhancement. 
		@return value: double
		@remarks This is a replication of GetOpticsBrightnessMinEx()
		*/
		static double getOpticsBrightnessMinEx();

		/*!
		Returns the maximum brightness of current optical enhancement. 
		@return value - double 
		@remarks This is a replication of GetOpticsBrightnessMaxEx()
		*/
		static double getOpticsBrightnessMaxEx();

		/*!
		Returns the maximum gain of current optical device. 
		@return value - double 
		@remarks This is a replication of GetOpticsMaxGainAmplifierEX()
		*/
		static double getOpticsMaxGainAmplifierEx();

		/*!
		Returns the optics brightness offset
		@return value - double 
		@remarks This is a replication of GetOpticsBrightnessOffsetEx()
		*/
		static double getOpticsBrightnessOffsetEx();

		/*!
		Return the blur level has been set
		@return value - double 
		@remarks This is a replication of GetOpticsBlurCoefEx()
		*/
		static double getOpticsBlurCoefEx();

		/*!
		Set the brightness of current optical enhancement. 
		@param brightnessVal - The value that suppose to assign.
		@return Nothing.
		@remarks This is a replication of setOpticsBrightness(double brightnessVal)
		*/
		static void applyOpticsBrightness(double brightnessVal);

		/*! 
		Sets the minimum brightness of current optical enhancement. 
		@param minBrightVal - The value that suppose to assign.
		@return Nothing.
		@remarks This is a replication of setOpticsBrightnessMin(double minBrightVal)
		*/
		static void applyOpticsBrightnessMin(double minBrightVal);

		/*!
		Sets the maximum brightness of current optical enhancement. 
		@param maxBrightVal - The value that suppose to assign.
		@return Nothing.
		@remarks This is a replication of setOpticsBrightnessMax(double maxBrightVal) 
		*/	
		static void applyOpticsBrightnessMax(double maxBrightVal);

		/*!
		Sets the maximum gain of current optical device. 
		Only works for units' NV equipment, not for vehicle-mounted ones.
		@param maxGainAmp - The value that suppose to assign.
		@return Nothing.
		@remarks This is a replication of setOpticsMaxGainAmplifier(double maxGainAmp)
		*/
		static void applyOpticsMaxGainAmplifier(double maxGainAmp);

		/*!
		Set the optics brightness offset
		@param brightOff - The value that suppose to assign.
		@return Nothing.
		@remarks This is a replication of setOpticsBrightnessOffset(double brightOff)
		*/
		static void applyOpticsBrightnessOffset(double brightOff);

		/*!
		Sets the level of blur of the TI device.
		Value is from 0 to 1. 0 creates a sharp picture, 1 creates a grainy/blury picture.
		@param blurAmount - The value that suppose to assign.
		@return Nothing.
		@remarks This is a replication of setOpticsBlurCoef(double blurAmount)
		*/
		static void applyOpticsBlurCoef(double blurAmount);

		/*!
		set specified unit as player.player should be a playble unit.
		@param player - The unit that suppose to assign as player.
		@return bool - On successed function return true.
		@remarks This is a replication of setPlayer(Unit& player)
		*/
		static bool applyPlayer(Unit& player);
	
		/*!
		Returns a vector of all units that are player controlled at this time.
		@return vector<Unit>.
		*/
		static vector<Unit> getAllPlayers();

		/*!
		@description 
		Sets optics for the player.

		@locality

		Locally effected

		@version  [VBS2Fusion v3.12]

		@param OPTICSTATE � struct for hold optics information. If instance is opticsState.
				opticsState.isActive : bool - true to select optics.
				opticsState.zoom: double - zoom value.
				opticsState.visionMode: int - 0: day, 1: night, 2: thermal vision (optional for dismounted).
				opticsState.tiMode: int - index of TI Modus (optional for dismounted).
				opticsState.weapon: string - weapon name (optional).
				opticsState.transition: int - Overrides the weapon bore sight to optics transition (blending) value (optional).
				opticsState.device: int - index of optic device on muzzle (optional).
				opticsState.mode:int - index of optic mode of the optic device (optional).

		@return Nothing.

		@example

		@code

		OPTICSTATE opticsState;
		opticsState.isActive = true;
		opticsState.zoom = 1.3;
		opticsState.visionMode = 1;
		opticsState.tiMode = 1.2;
		opticsState.weapon ="weaponName";
		opticsState.transition = 2;
		opticsState.device = 1;
		opticsState.mode = 1;

		PlayerUtilities::applyOpticsState(opticsState);

		@endcode

		@overloaded None.

		@related
		OPTICSTATE PlayerUtilities::getOpticsState()

		@remarks Function is locally effected as long as there are no parameter to pass a player as a parameter.

		@remarks Zooming level can't exceed the maximum value.
		*/
		static void applyOpticsState(OPTICSTATE& opticsState);

	};
}


#endif