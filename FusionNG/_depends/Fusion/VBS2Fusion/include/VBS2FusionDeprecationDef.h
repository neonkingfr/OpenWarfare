/**************************************************************************
 * Copyright 2010 SimCentric Technologies, Pty. Ltd. All Rights
 * Reserved.
 *
 * COPYRIGHT/OWNERSHIP
 * This Software and its source code are proprietary products of SimCentric and
 * are protected by copyright and other intellectual property laws. The Software
 * is licensed and not sold.
 *
 * USE OF SOURCE CODE
 * This source code may not be used, modified or copied without the expressed,
 * written permission of SimCentric.
 *
 * RESTRICTIONS
 * You MAY NOT: (a) copy and distribute the Software or any portion of it;
 * (b) sublicense, lease, rent, or transfer this Software to another; (c) cause
 * or permit reverse engineering, disassembly, decompilation or alteration of
 * this Software; (d) remove any product identification, copyright notices, or
 * other notices or proprietary restrictions from this Software; (e) copy the
 * documentation accompanying the software.
 *
 * DISCLAIMER OF WARRANTIES
 * The Software is supplied "AS IS". SimCentric disclaims all warranties,
 * expressed or implied, including, without limitation, the warranties of
 * merchantability and of fitness for any purpose. The user must assume the
 * entire risk of using the Software.
 *
 * DISCLAIMER OF DAMAGES
 * SimCentric assumes no liability for damages, direct or consequential, which
 * may result from the use of the Software, even if SimCentric has been advised
 * of the possibility of such damages. Any liability of the seller will be
 * limited to refund the purchase price.
 *
 **************************************************************************/

/*************************************************************************

Name:

	VBS2FusionDeprecationDef.h

Purpose:

	This file contains the deprecation warning message definitions of VBS2Fusion.

Version Information:

	Version		Date		Author and Comments
	===========================================
	1.0			07-07/2012	CGS: Original Implementation
	

/************************************************************************/
#ifndef VBS2FUSION_DEPRECATION_H
#define VBS2FUSION_DEPRECATION_H

/*!
Define deprecated declaration into VBS2FUSION_DPR Macro 
*/
#define  VBS2FUSION_DPR(DPR_WarningMessage) __declspec(deprecated(DPR_WarningMessage))


/******************************* DEPRECATED CLASS MACROS ***********************************/

/*! Data Class */
#define DAGV "Deprecated Data class, Use Vehicle Data class instead."/*Warning message for ArmedGroundVehicle */
#define DARM "Deprecated Data class, Use Vehicle Data class instead."	/*Warning message for ArmedModule */
#define DGRV "Deprecated Data class, Use Vehicle Data class instead."	/*Warning message for GroundVehicle */

/*! Utility Class */
#define UAGV "Deprecated Utility class, Use VehicleUtilities class instead." /*Warning message for ArmedGroundVehicleUtilities */
#define UARM "Deprecated Utility class, Use VehicleUtilities class instead."	/*Warning message for ArmedModuleUtilities */
#define UTUR "Deprecated Utility class, Use VehicleUtilities class instead."	/*Warning message for TurretUtilities */

/******************************* DEPRECATED FUNCTION MACROS ***********************************/

/**********Data Class Functions**********/

/*! Turret Data Class */
#define DTUR001 "Deprecated function, Use Vehicle::clearMagazineList() instead" /*Warning message for clearMagazinelist() */
#define DTUR002 "Deprecated function, Use Vehicle::getMagazineCount() instead" /*Warning message for getMagazineCount() */
#define DTUR003 "Deprecated function, Use Vehicle::getCurrentMagazine() instead" /*Warning message for getCurrentMagazine() */
#define DTUR004 "Deprecated function, Use Vehicle::addMagazine(Magazine magazine) instead" /*Warning message for setMagazine(Magazine magazine) */


/**********Utility Class Functions**********/

/*! WeaponUtilities Class */
#define UWPN001 "Deprecated function, Use WeaponUtilities::getAmmoCount(Vehicle& vehicle, Weapon& weapon) instead" /*Warning message for getAmmoCount(ArmedGroundVehicle& vehicle, Weapon& weapon)*/
#define UWPN002 "Deprecated function, Use WeaponUtilities::getAzimuth(Vehicle& vehicle, Turret& turret, Weapon& weapon) instead" /*Warning message for getAzimuth(ArmedGroundVehicle& vehicle, Turret& turret, Weapon& weapon)*/
#define UWPN003 "Deprecated function, Use WeaponUtilities::getElevation(Vehicle& vehicle, Turret& turret, Weapon& weapon) instead" /*Warning message for getElevation(ArmedGroundVehicle& vehicle, Turret& turret, Weapon& weapon)*/
#define UWPN004 "Deprecated function, Use WeaponUtilities::getWeaponDirectionVector(Vehicle& vehicle, Turret& turret, Weapon& weapon) instead" /*Warning message for getWeaponDirectionVector(ArmedGroundVehicle& vehicle, Turret& turret, Weapon& weapon)*/
#define UWPN005 "Deprecated function, Use WeaponUtilities::setWeaponState(Vehicle& vehicle,Weapon& weapon, bool reloadStatus,double reloadTime) instead" /*Warning message for setWeaponState(ArmedGroundVehicle& vehicle,Weapon& weapon, bool reloadStatus,double reloadTime)*/
#define UWPN006 "Deprecated function, Use WeaponUtilities::applyWeaponState(Vehicle& vehicle,Weapon& weapon, bool reloadStatus,double reloadTime) instead" /*Warning message for setWeaponState(Vehicle& vehicle,Weapon& weapon, bool reloadStatus,double reloadTime)*/
#define UWPN007 "Deprecated function, Use WeaponUtilities::applyShotCreationEnable(bool enable) instead" /*Warning message for void allowCreateShot(bool enable)*/
#define UWPN008 "Deprecated function, Use void WeaponUtilities::applyDummyRound(Projectile& proj, bool enable ) instead" /*Warning message for void setDummyRound(Projectile& proj, bool enable )*/

/*! ControllableObjectUtilities Class */
#define UCOB001 "function has been deprecated and will be removed in future, Use bool isAvailable(ControllableObject& co) instead"  /*Warning message for ControllableObjectExists(ControllableObject& co)*/ 
#define UCOB002 "function has been deprecated and will be removed in future, Use void applyExternalControl(ControllableObject& co, bool control) instead"  /*Warning message for setExternalControl(ControllableObject& co, bool control)*/ 
#define UCOB003 "function has been deprecated and will be removed in future, Use void applyKeyAction(string keyStroke, double amount) instead"  /*Warning message for setKeyAction(ControllableObject& co,string keyStroke, float amount)*/ 
#define UCOB004 "function has been deprecated and will be removed in future, Use void applyKeyAction(ControllableObject& co, string keyStroke, double amount) instead"  /*Warning message for setUnitAction(ControllableObject& co,string keyStroke, float amount)*/ 
#define UCOB005 "function has been deprecated and will be removed in future, Use void applySpeedLimit(ControllableObject& co , double speedLimit) instead"  /*Warning message for void limitSpeed(ControllableObject& co , double speedValue)*/ 
#define UCOB006 "function has been deprecated and will be removed in future, Use void applyObjectTexture(ControllableObject& object, int selectionIndex, string& texture) instead"  /*Warning message for void setObjectTexture(ControllableObject& object, int selectionIndex, string& texture)*/ 
#define UCOB007 "function has been deprecated and will be removed in future, Use void applyObjectTexture(ControllableObject& object, string& selection, string& texture) instead"  /*Warning message for void setObjectTexture(ControllableObject& object, string& selection, string& texture)*/ 
#define UCOB008 "function has been deprecated and will be removed in future, Use void applyObjectTextureGlobal(ControllableObject& object, int selectionIndex, string& texture) instead"  /*Warning message for void setObjectTextureGlobal(ControllableObject& object, int selectionIndex, string& texture)*/ 
#define UCOB009 "function has been deprecated and will be removed in future, Use void applyObjectTextureGlobal(ControllableObject& co, string& selection, string& texture) instead"  /*Warning message for void setObjectTextureGlobal(ControllableObject& object, string& selection, string& texture)*/ 
#define UCOB010 "function has been deprecated and will be removed in future, Use void applyRandomPosition(ControllableObject& co, position3D pos, vector<Marker> markers, double placement) instead"  /*Warning message for void setRandomPosition(ControllableObject& co, position3D pos, vector<Marker> markers, double placement)*/ 
#define UCOB011 "function has been deprecated and will be removed in future, Use void applyVectorDirAndUp(ControllableObject& co,vector3D vectorDir, vector3D vectorUp, bool relative) instead"  /*Warning message for void setVectorDirAndUp(ControllableObject& co,vector3D vectorDir, vector3D vectorUp, bool relative)*/ 
#define UCOB012 "function has been deprecated and will be removed in future, Use void applyCommandMove(ControllableObject& co, position3D pos) instead"  /*Warning message for void moveTo(ControllableObject& co, position3D pos)*/ 
#define UCOB013 "function has been deprecated and will be removed in future, Use position3D getModelToWorld(ControllableObject& co, position3D& pos) instead"  /*Warning message for position3D modelToWorld(ControllableObject& co, position3D& pos)*/ 
#define UCOB014 "function has been deprecated and will be removed in future, Use position3D getWorldToModel(ControllableObject& co, position3D& pos) instead"  /*Warning message for position3D worldToModel(ControllableObject& co, position3D& pos)*/ 
#define UCOB015 "function has been deprecated and will be removed in future, Use position3D getModelToWorldASL(ControllableObject& co, position3D& pos) instead"  /*Warning message for position3D modelToWorldASL(ControllableObject& co, position3D& pos)*/ 
#define UCOB016 "function has been deprecated and will be removed in future, Use position3D getWorldToModelASL(ControllableObject& co, position3D& pos) instead"  /*Warning message for position3D worldToModelASL(ControllableObject& co, position3D& pos)*/ 
#define UCOB017 "function has been deprecated and will be removed in future, Use void applyObjectAttach(ControllableObject& co, ControllableObject& tObject, position3D offSet, string memoryPoint) instead"  /*Warning message for void attachTo(ControllableObject& co, ControllableObject& tObject, position3D offSet, string memoryPoint)*/ 
#define UCOB018 "function has been deprecated and will be removed in future, Use void applyObjectAttach(ControllableObject& co, ControllableObject& targetCo, position3D offSet, string memoryPoint, int rope, string bone, bool collisions = false) instead"  /*Warning message for void attachTo(ControllableObject& co, ControllableObject& targetCo, position3D offSet, string memoryPoint, int rope, string bone, bool collisions = false)*/ 
#define UCOB019 "function has been deprecated and will be removed in future, Use void applyObjectAttach(ControllableObject& co, ControllableObject& targetCo, position3D offSet) instead"  /*Warning message for void attachTo(ControllableObject& co, ControllableObject& targetCo, position3D offSet)*/ 
#define UCOB020 "function has been deprecated and will be removed in future, Use void applyObjectDetach(ControllableObject& co) instead"  /*Warning message for void detach(ControllableObject& object)*/ 
#define UCOB021 "function has been deprecated and will be removed in future, Use void applyAIBehaviourDisable(ControllableObject& co, AIBEHAVIOUR behaviour) instead"  /*Warning message for void disableAIbehaviour(ControllableObject& co, AIBEHAVIOUR behaviour)*/ 
#define UCOB022 "function has been deprecated and will be removed in future, Use void applyAIBehaviourEnable(ControllableObject& co, AIBEHAVIOUR behaviour) instead"  /*Warning message for void enableAIbehaviour(ControllableObject& co, AIBEHAVIOUR behaviour)*/ 
#define UCOB023 "function has been deprecated and will be removed in future, Use void applyDamageEnable(ControllableObject& co , bool allow) instead"  /*Warning message for void allowDamage(ControllableObject& co , bool allow)*/ 
#define UCOB024 "function has been deprecated and will be removed in future, Use void applySpeed(ControllableObject& co, double speed) instead"  /*Warning message for void forceSpeed(ControllableObject& co, double speed)*/ 
#define UCOB025 "function has been deprecated and will be removed in future, Use bool isCommandMoveCompleted(ControllableObject& co) instead"  /*Warning message for bool isMoveToCompleted(ControllableObject& co)*/ 
#define UCOB026 "function has been deprecated and will be removed in future, Use the functions from related classes which exactly match to the type of object"  /*Warning message for just deprecated functions*/ 
#define UCOB027 "function has been deprecated and will be removed in future, Use void applyDisplayTextRemoval(ControllableObject& co) instead"  /*Warning message for void removeDisplayText(ControllableObject& co)*/ 
#define UCOB028 "function has been deprecated and will be removed in future, Use bool applyPreloadObject(ControllableObject& co, double distance) instead"  /*Warning message for bool preloadObject(ControllableObject& co, double distance)*/ 
#define UCOB029 "function has been deprecated and will be removed in future, Use bool isAbleToMove(ControllableObject& co) instead"  /*Warning message for bool canMove(ControllableObject& co)*/ 
#define UCOB030 "function has been deprecated and will be removed in future, Use bool isAbleToFire(ControllableObject& co) instead"  /*Warning message for bool canFire(ControllableObject& co)*/ 
#define UCOB031 "function has been deprecated and will be removed in future, Use void applyPlaceOnSurface(ControllableObject& co) instead"  /*Warning message for void placeOnSurface(ControllableObject& co)*/ 
#define UCOB032 "function has been deprecated and will be removed in future, Use void applySuspend(ControllableObject& co , bool collidable = true ) instead"  /*Warning message for void Suspend(ControllableObject& co , bool collidable = true )*/ 
#define UCOB033 "function has been deprecated and will be removed in future, Use void applyReactivate(ControllableObject& co) instead"  /*Warning message for void Reactivate(ControllableObject& co)*/ 
#define UCOB034 "function has been deprecated and will be removed in future, Use void applyTurn(ControllableObject& co, double direction) instead"  /*Warning message for void Turn(ControllableObject& co, double direction)*/ 
#define UCOB035 "function has been deprecated and will be removed in future, Use void applyTurnRelative(ControllableObject& co, double relativeDir) instead"  /*Warning message for void TurnRelative(ControllableObject& co, double direction)*/ 
#define UCOB036 "function has been deprecated and will be removed in future, Use void applyWeaponSelection(ControllableObject& co, string weaponName) instead"  /*Warning message for void selectWeapon(ControllableObject& co, string muzzleName)*/ 
#define UCOB037 "function has been deprecated and will be removed in future, Use bool isFloatable(ControllableObject& co) instead"  /*Warning message for bool canFloat(ControllableObject& co)*/ 
#define UCOB038 "function has been deprecated and will be removed in future, Use void applyActionsDisable(ControllableObject& co, bool disable=true) instead"  /*Warning message for void disableActions(ControllableObject& co, bool disable=true)*/ 
#define UCOB039 "function has been deprecated and will be removed in future, Use void applyGeometrySettingsDisable(ControllableObject& co, bool visible, bool fire, bool view, bool collision, bool roadways, bool shadows) instead"  /*Warning message for void disableGeo(ControllableObject& co, bool visible, bool fire, bool view, bool collision)*/ 
#define UCOB040 "function has been deprecated and will be removed in future, Use void applyAction(ControllableObject& co, string type, ControllableObject& targetCo) instead"  /*Warning message for void performAction(ControllableObject& co, string type, ControllableObject& targetCo)*/ 
#define UCOB041 "function has been deprecated and will be removed in future, Use void applyMoveForward(ControllableObject& co) instead"  /*Warning message for void moveForward(ControllableObject& co)*/ 
#define UCOB042 "function has been deprecated and will be removed in future, Use correct version of oveloaded function with the name of applyAction instead"  /*Warning message for void performAction(ControllableObject& co,list<string> strpara, ControllableObject& targetCo )*/
#define UCOB043 "function has been deprecated and will be removed in future, Use vector3D getBoundingBoxMinExtremePoints(ControllableObject& co) instead"  /*Warning message for vector3D getBoundingBoxMinExtremePoints(ControllableObject& co, ControllableObject& tco)*/
#define UCOB044 "function has been deprecated and will be removed in future, Use vector3D getBoundingBoxMaxExtremePoints(ControllableObject& co) instead"  /*Warning message for vector3D getBoundingBoxMaxExtremePoints(ControllableObject& co, ControllableObject& tco)*/
#define UCOB045 "function has been deprecated and will be removed in future, Use void applyPositionASL(ControllableObject& co, position3D aslPos) instead"  /*Warning message for void applyPositionASL(ControllableObject& co, position3D aslPos, bool considerElev)*/
#define UCOB046 "function has been deprecated and will be removed in future, Use void applyPositionASL(ControllableObject& co, position3D aslPos) instead"  /*Warning message for void applyPositionASLEx(ControllableObject& co, position3D aslPosEx)*/

/*! UnitUtilities Class */
#define UUNT001 "function has been deprecated and will be removed in future, Use void applyAIMovementDisable(Unit& u) instead" /*Warning message for disableAIMOVE(Unit& u)*/
#define UUNT002 "function has been deprecated and will be removed in future, Use void applyAIPathPlanningDisable(Unit& u) instead" /*Warning message for disableAIPATHPLAN(Unit& u)*/
#define UUNT003 "function has been deprecated and will be removed in future, Use void applyAIMovementEnable(Unit& u) instead" /*Warning message for enableAIMOVE(Unit& u)*/
#define UUNT004 "function has been deprecated and will be removed in future, Use void applyAIPathPlanningEnable(Unit& u) instead" /*Warning message for enableAIPATHPLAN(Unit& u)*/
#define UUNT005 "function has been deprecated and will be removed in future, Use void applyWeaponReload(Unit& u) instead" /*Warning message for reloadWeapons(Unit& u)*/
#define UUNT006 "function has been deprecated and will be removed in future, Use void applyGlobalChat(Unit& u , string txtMessage) instead" /*Warning message for globalChat(Unit& u,string txtMessage)*/
#define UUNT007 "function has been deprecated and will be removed in future, Use void applyBehaviour(Unit& unit, BEHAVIOUR behaviour) instead" /*Warning message for setUnitBehaviour(Unit& unit, BEHAVIOUR behaviour)*/
#define UUNT008 "function has been deprecated and will be removed in future, Use bool isGroupExists(string groupName) instead" /*Warning message for groupExists(string groupName)*/
#define UUNT009 "function has been deprecated and will be removed in future, Use void applyCommandStop(Unit& u) instead" /*Warning message for commandStop(Unit& u)*/
#define UUNT010 "function has been deprecated and will be removed in future, Use void applyMove(Unit& u, position3D movePosition) instead" /*Warning message for doMove(Unit& u, position3D movePosition)*/
#define UUNT011 "function has been deprecated and will be removed in future, Use void applyTarget(Unit& u, ControllableObject& co) instead" /*Warning message for doTarget(Unit& unit, ControllableObject& co)*/
#define UUNT012 "function has been deprecated and will be removed in future, Use void applyWatch(Unit& u, position3D targetPosition) instead" /*Warning message for doWatch(Unit& unit, position3D targetPosition)*/
#define UUNT013 "function has been deprecated and will be removed in future, Use void applyUnitPosAUTO(Unit& u) instead" /*Warning message for unitPosAUTO(Unit& u)*/
#define UUNT014 "function has been deprecated and will be removed in future, Use void applyUnitPosDOWN(Unit& u) instead" /*Warning message for unitPosDOWN(Unit& u)*/
#define UUNT015 "function has been deprecated and will be removed in future, Use void applyUnitPosUP(Unit& u) instead" /*Warning message for unitPosUP(Unit& u)*/
#define UUNT016 "function has been deprecated and will be removed in future, Use void applyUnitExactMovementMode(Unit &u, bool precisionMode) instead" /*Warning message for setUnitExactMovementMode(Unit &u, bool precisionMode)*/
#define UUNT017 "function has been deprecated and will be removed in future, Use void applySalutePerform(Unit& unit, Unit& targetUnit) instead" /*Warning message for performSalute(Unit& unit, Unit& targetUnit)*/
#define UUNT018 "function has been deprecated and will be removed in future, Use applyHeight(Unit& u , double height) instead" /*Warning message for setHeight(Unit& u , double height)*/
#define UUNT019 "function has been deprecated and will be removed in future, Use applyBMI(Unit& u, double index) instead" /*Warning message for setBMI(Unit& u, double index)*/
#define UUNT020 "function has been deprecated and will be removed in future, Use applyGunnerInputDisable(Unit& unit, GUNNERINPUT_MODE mode) instead" /*Warning message for disableGunnerInput(Unit& unit,GUNNERINPUT_MODE mode)*/
#define UUNT021 "function has been deprecated and will be removed in future, Use applyWeaponSelect(Unit& u, string muzzleName) instead" /*Warning message for selectWeapon(Unit& u, string muzzleName)*/
#define UUNT022 "function has been deprecated and will be removed in future, Use applyMagazineAdd(Unit& u,string name) instead" /*Warning message for void addMagazine(Unit& u, string name)*/
#define UUNT023 "function has been deprecated and will be removed in future, Use getMagazinesCount(Unit& u) instead" /*Warning message for int countMagazines(Unit& u)*/
#define UUNT024 "function has been deprecated and will be removed in future, Use applyMagazineRemove(Unit& unit, string name) instead" /*Warning message for void removeMagazine(Unit& unit, string name)*/
#define UUNT025 "function has been deprecated and will be removed in future, Use applyMagazineRemoveAll(Unit& unit, string name) instead" /*Warning message for void removeallMagazines(Unit& unit, string name)*/
#define UUNT026 "function has been deprecated and will be removed in future, Use applyWeaponAdd(Unit& unit,string name) instead" /*Warning message for void addWeapon(Unit& u, string name)*/
#define UUNT027 "function has been deprecated and will be removed in future, Use applyWeaponRemove(Unit& unit, string name) instead" /*Warning message for void removeWeapon(Unit& unit, string name)*/
#define UUNT028 "function has been deprecated and will be removed in future, Use isWeaponHaving(Unit& unit, string name) instead" /*Warning message for bool hasWeapon(Unit& unit, string name)*/
#define UUNT029 "function has been deprecated and will be removed in future, Use applyVehicleAmmo(Unit& unit, double ammovalue) instead" /*Warning message for void setVehicleAmmo(Unit& unit, double ammovalue)*/
#define UUNT030 "function has been deprecated and will be removed in future, Use applyWeaponRemoveAll(Unit& unit) instead" /*Warning message for void removeallWeapons(Unit& unit)*/
#define UUNT031 "function has been deprecated and will be removed in future, Use void applyFire(Unit& firingUnit, ControllableObject& target) instead" /*Warning message for doFire(Unit& firingUnit, ControllableObject& target)*/
#define UUNT032 "function has been deprecated and will be removed in future, Use void applyFire(Unit& firingUnit, position3D pos) instead" /*Warning message for doFire(Unit& firingUnit, position3D pos)*/
#define UUNT033 "function has been deprecated and will be removed in future, Use void applyWeaponSafety(Unit& unit, bool safe) instead" /*Warning message for setWeaponSafety(Unit& unit, bool safe)*/
#define UUNT034 "function has been deprecated and will be removed in future, Use vector<Target> getNearTargets(Unit& unit, int range) instead" /*Warning message for nearTargets(Unit& unit, int range)*/
#define UUNT035 "function has been deprecated and will be removed in future, Use void applyPersonalItemsEnable(Unit& unit, bool enable) instead" /*Warning message for EnablePersonalItems(Unit& unit, bool enable)*/
#define UUNT036 "function has been deprecated and will be removed in future, Use void applyMoralAddition(Unit& unit, double moralChange) instead" /*Warning message for addToMorale(Unit& unit, double moralChange)*/
#define UUNT037 "function has been deprecated and will be removed in future, Use void applySpeechEnable(Unit& unit, bool speech) instead" /*Warning message for allowSpeech(Unit& unit, bool speech)*/
#define UUNT038 "function has been deprecated and will be removed in future, Use void applyAnaerobicFatigueAddition(Unit& unit, double AnaerobicFatigueChange) instead" /*Warning message for addToAnaerobicFatigue(Unit& unit, double AnaerobicFatigueChange)*/
#define UUNT039 "function has been deprecated and will be removed in future, Use void applyAerobicFatigueAddition(Unit& unit, double AerobicFatigueChange) instead" /*Warning message for addToAerobicFatigue(Unit& unit, double AerobicFatigueChange)*/
#define UUNT040 "function has been deprecated and will be removed in future, Use void applyStop(Unit& unit) instead" /*Warning message for StopUnit(Unit& unit)*/
#define UUNT041 "function has been deprecated and will be removed in future, Use void applyRevive(Unit& unit) instead" /*Warning message for reviveUnit(Unit& unit)*/
#define UUNT042 "function has been deprecated and will be removed in future, Use void applyWeaponCargo(Unit& unit, string weaponName, int count) instead" /*Warning message for addWeaponToCargo(Unit& unit, string weaponName, int count)*/
#define UUNT043 "function has been deprecated and will be removed in future, Use void applyMagazineCargo(Unit& unit, string magName, int count) instead" /*Warning message for addMagazineToCargo(Unit& co, string magName, int count)*/
#define UUNT044 "function has been deprecated and will be removed in future, Use void applyCommandMove(Unit& unit, position3D position) instead" /*Warning message for commandMove(Unit& unit, position3D position)*/
#define UUNT045 "function has been deprecated and will be removed in future, Use void applyRatingIncrease(Unit& unit, double rating) instead" /*Warning message for addRating(Unit& unit, double rating)*/
#define UUNT046 "function has been deprecated and will be removed in future, Use void applyScoreIncrease(Unit& unit, int score) instead" /*Warning message for addScore(Unit& unit, int score)*/
#define UUNT047 "function has been deprecated and will be removed in future, Use bool isStandable(Unit& unit) instead" /*Warning message for canStand(Unit& unit)*/
#define UUNT048 "function has been deprecated and will be removed in future, Use bool isAmmoAvailable(Unit& unit) instead" /*Warning message for hasSomeAmmo(Unit& unit)*/
#define UUNT049 "function has been deprecated and will be removed in future, Use double getKnowsAbout(Unit& unit, ControllableObject& target) instead" /*Warning message for knowsAbout(Unit& unit, ControllableObject& target)*/
#define UUNT050 "function has been deprecated and will be removed in future, Use void applySideChat(Unit& unit, string chatText) instead" /*Warning message for sideChat(Unit& unit, string chatText)*/
#define UUNT051 "function has been deprecated and will be removed in future, Use void applyGroupChat(Unit& unit, string chatText) instead" /*Warning message for groupChat(Unit& unit, string chatText)*/
#define UUNT052 "function has been deprecated and will be removed in future, Use int createActionMenuItem(Unit &unit, string title, string fileName) instead" /*Warning message for addActionMenuItem(Unit& unit, string title, string fileName)*/
#define UUNT053 "function has been deprecated and will be removed in future, Use void deleteActionMenuItem(Unit& unit, int index) instead" /*Warning message for removeActionMenuItem(Unit& unit, int index)*/
#define UUNT054 "function has been deprecated and will be removed in future, Use void applyPrimaryWeaponReload(Unit &unit) instead" /*Warning message for reloadPrimaryWeapon(Unit& unit)*/
#define UUNT055 "function has been deprecated and will be removed in future, Use void applyStop(Unit& unit, bool stop) instead" /*Warning message for stop(Unit& unit, bool stop)*/
#define UUNT056 "function has been deprecated and will be removed in future, Use void applyMovement(Unit& unit, string moveName) instead" /*Warning message for playMove(Unit& unit, string moveName)*/
#define UUNT057 "function has been deprecated and will be removed in future, Use void applyMovement(Unit &unit, UNITMOVE move) instead" /*Warning message for playMove(Unit& unit, UNITMOVE move)*/
#define UUNT058 "function has been deprecated and will be removed in future, Use void applySwitchMove(Unit &unit, string moveName) instead" /*Warning message for switchMove(Unit& unit, string moveName)*/
#define UUNT059 "function has been deprecated and will be removed in future, Use void applySwitchMove(Unit &unit, UNITMOVE move) instead" /*Warning message for switchMove(Unit& unit, UNITMOVE move)*/
#define UUNT060 "function has been deprecated and will be removed in future, Use void applyAllowGetIn(vector<Unit> unitList, bool val) instead" /*Warning message for allowGetIn(vector<Unit> unitList, bool val)*/
#define UUNT061 "function has been deprecated and will be removed in future, Use void applyVehicleUnassign(Unit& unit) instead" /*Warning message for unAssignVehicle(Unit& unit)*/
#define UUNT062 "function has been deprecated and will be removed in future, Use void applyBodyHiding(Unit& unit) instead" /*Warning message for hideBody(Unit& unit)*/
#define UUNT063 "function has been deprecated and will be removed in future, Use void applyUnselectFromGroup(Unit &unit) instead" /*Warning message for unselectUnitInGroup(Unit& unit)*/
#define UUNT064 "function has been deprecated and will be removed in future, Use void applyAttackEnable(Unit& unit, bool boolVal) instead" /*Warning message for setEnableAttack(Unit& unit, bool boolVal)*/
#define UUNT065 "function has been deprecated and will be removed in future, Use void applySpeech(Unit& unit, string& speechName) instead" /*Warning message for say(Unit& unit, string& speechName)*/
#define UUNT066 "function has been deprecated and will be removed in future, Use void applySpeech(Unit& unit, string& speechName, double maxTitlesDistance) instead" /*Warning message for say(Unit& unit, string& speechName, double maxTitlesDistance)*/
#define UUNT067 "function has been deprecated and will be removed in future, Use void applyAnimationMoveDisable(Unit& unit, bool disable) instead" /*Warning message for setDisableAnimationMove(Unit& unit, bool disable)*/
#define UUNT068 "function has been deprecated and will be removed in future, Use void applyGroupRadioMessage(Unit& unit, string& messageName) instead" /*Warning message for sendGroupRadioMessage(Unit& unit, string& messageName)*/ 
#define UUNT069 "function has been deprecated and will be removed in future, Use void applyGlobalRadioMessage(Unit& unit, string& radioName) instead" /*Warning message for sendGlobalRadioMessage(Unit& unit, string& radioName)*/
#define UUNT070 "function has been deprecated and will be removed in future, Use void applyCommandTarget(vector<Unit>& unitList, Unit& target) instead" /*Warning message for commandTarget(vector<Unit>& unitList, Unit& target)*/
#define UUNT071 "function has been deprecated and will be removed in future, Use void applyAIPathwayCost(Unit& AIunit, double cost) instead" /*Warning message for setAIPathwayCost(Unit& AIunit, double cost)*/ 
#define UUNT072 "function has been deprecated and will be removed in future, Use void applyFleeing(Unit& unit, double cowardice) instead"  /*Warning message for allowFleeing(Unit& unit, double cowardice)*/ 
#define UUNT073 "function has been deprecated and will be removed in future, Use Unit getNearestEnemy(Unit& unit, Unit& positionUnit) instead" /*Warning message for Unit findNearestEnemy(Unit& unit, Unit& positionUnit)*/ 
#define UUNT074 "function has been deprecated and will be removed in future, Use Unit getNearestEnemy(Unit& unit, position3D position) instaed" /*Warning message for Unit findNearestEnemy(Unit& unit, position3D position)*/  
#define UUNT075 "function has been deprecated and will be removed in future, Use void applySideRadioMessage(Unit& unit, string& messageName) instead" /*Warning message for sendSideRadioMessage(Unit& unit, string& messageName)*/  
#define UUNT076 "function has been deprecated and will be removed in future, Use void applyWeaponDirection(Unit& unit, double azimuth, double elevation, bool transition) instead" /*Warning message for setWeaponDirection(Unit& unit, double azimuth, double elevation, bool transition)*/  
#define UUNT077 "function has been deprecated and will be removed in future, Use void applyCommandWatch(Unit& unit, ControllableObject& target) instead" /*Warning message for commandWatch(Unit& unit, ControllableObject& target)*/  
#define UUNT078 "function has been deprecated and will be removed in future, Use void applyCommandWatch(Unit& unit, position3D& target) instead" /*Warning message for commandWatch(Unit& unit, position3D& target)*/  
#define UUNT079 "function has been deprecated and will be removed in future, Use void applyGlanceAt(Unit& unit, position3D& position) instead" /*Warning message for glanceAt(Unit& unit, position3D& position)*/  
#define UUNT080 "function has been deprecated and will be removed in future, Use void applyLookAt(Unit& unit, position3D& position) instead" /*Warning message for LookAt(Unit& unit, position3D& position)*/  

/*! GroupUtilities Class */
#define UGRP001 "function has been deprecated and will be removed in future, Use void applyLeader(Group& group,Unit& u) instead" /*Warning message for setLeader(Group& group,Unit& u)*/
#define UGRP002 "function has been deprecated and will be removed in future, Use void applyWaypointsLock(Group& group) instead" /*Warning message for lockWaypoints(Group& group)*/
#define UGRP003 "function has been deprecated and will be removed in future, Use void applyWaypointsUnlock(Group& group) instead" /*Warning message for unLockWaypoints(Group& group)*/
#define UGRP004 "function has been deprecated and will be removed in future, Use void applyFleeing(Group& grp, double cowardice) instead" /*Warning message for allowFleeing(Group& grp, double cowardice)*/
#define UGRP005 "function has been deprecated and will be removed in future, Use void applyAttackEnable(Group& group, bool boolVal) instead" /*Warning message for setEnableAttack(Group& group, bool boolVal)*/
#define UGRP006 "function has been deprecated and will be removed in future, Use double getKnowsAbout(Group& group, ControllableObject& target) instead" /*Warning message for knowsAbout(Group& grp, ControllableObject& target)*/
#define UGRP007 "function has been deprecated and will be removed in future, Use void applyGroupSwitch(Group& group, Unit& u) instead" /*Warning message for switchGroup(Group& group, Unit& u)*/
#define UGRP008 "function has been deprecated and will be removed in future, Use void applySmartOrderEnable(Group& group, bool enable) instead" /*Warning message for EnableSmartOrder(Group& grp, bool enable)*/
#define UGRP009 "function has been deprecated and will be removed in future, Use void applyMoveDestination(Group& group, position3D pos) instead" /*Warning message for move(Group& grp, position3D pos)*/
#define UGRP010 "function has been deprecated and will be removed in future, Use bool isAvailable(string groupName) instead" /*Warning message for groupExists(string groupName)*/


/*! VehicleUtilities Class */
#define UVHC001 "function has been deprecated and will be removed in future, Use void applyMoveInAsDriver(Vehicle& vehicle, Unit& unit) instead" /*Warning message for moveInAndAsDriver(Vehicle& vehicle, Unit& unit)*/
#define UVHC002 "function has been deprecated and will be removed in future, Use void applyMoveInAsTurret(Vehicle& vehicle, Unit& unit, string turretPath) instead" /*Warning message for moveInAndAsTurret(Vehicle& vehicle, Unit& unit, string turretPath)*/
#define UVHC003 "function has been deprecated and will be removed in future, Use void applyMoveInAsTurret(Vehicle& vehicle, Unit& unit, Turret turret) instead" /*Warning message for moveInAndAsTurret(Vehicle& vehicle, Unit& unit, Turret turret)*/
#define UVHC004 "function has been deprecated and will be removed in future, Use void applyMoveInAsTurret(Vehicle& vehicle, Unit& unit) instead" /*Warning message for moveInAndAsTurret(Vehicle& vehicle, Unit& unit)*/
#define UVHC005 "function has been deprecated and will be removed in future, Use void applyMoveInAsCargo(Vehicle& vehicle, Unit& unit) instead" /*Warning message for moveInAndAsCargo(Vehicle& vehicle, Unit& unit)*/
#define UVHC006 "function has been deprecated and will be removed in future, Use void applyMoveInAsGunner(Vehicle& vehicle, Unit& unit) instead" /*Warning message for moveInAndAsGunner(Vehicle& vehicle, Unit& unit)*/
#define UVHC007 "function has been deprecated and will be removed in future, Use void applyMoveInAsCommander(Vehicle& vehicle, Unit& unit) instead" /*Warning message for moveInAndAsCommander(Vehicle& vehicle, Unit& unit)*/
#define UVHC008 "function has been deprecated and will be removed in future, Use void applyEngineStart(Vehicle& vehicle , bool state) instead" /*Warning message for startEngine(Vehicle& vehicle , bool state)*/
#define UVHC009 "function has been deprecated and will be removed in future, Use void applyUnitLeave(Vehicle& vehicle, Unit& unit) instead" /*Warning message for leaveVehicle(Vehicle& vehicle, Unit& unit), leaveVehicleByID(Vehicle& vehicle, Unit& unit), leaveVehicleByName(Vehicle& vehicle, Unit& unit), applyUnitLeave(Vehicle& vehicle, Unit& unit)*/
#define UVHC010 "function has been deprecated and will be removed in future, Use void applyManualControl(Vehicle& vehicle , bool control) instead" /*Warning message for setManualControl(Vehicle& vehicle , bool control)*/
#define UVHC011 "function has been deprecated and will be removed in future, Use void applyTurnWanted(Vehicle& vehicle, double factor) instead" /*Warning message for setTurnWanted(Vehicle& vehicle, double factor)*/
#define UVHC012 "function has been deprecated and will be removed in future, Use void applyThrustWanted(Vehicle& vehicle , double factor) instead" /*Warning message for setThrustWanted(Vehicle& vehicle , double factor)*/
#define UVHC013 "function has been deprecated and will be removed in future, Use void applyGetIn(vector<Unit> unitVec, bool order) instead" /*Warning message for orderGetIn(vector<Unit> unitVec, bool order)*/
#define UVHC014 "function has been deprecated and will be removed in future, Use void applyGetIn(Unit& unit, bool order) instead" /*Warning message for orderGetIn(Unit& unit, bool order)*/
#define UVHC015 "function has been deprecated and will be removed in future, Use void applyAssignAsCommander(Vehicle& vehicle, Unit& unit) instead" /*Warning message for assignAsCommander(Vehicle& vehicle, Unit& unit)*/
#define UVHC016 "function has been deprecated and will be removed in future, Use void applyAssignAsDriver(Vehicle& vehicle, Unit& unit) instead" /*Warning message for assignAsDriver(Vehicle& vehicle, Unit& unit)*/
#define UVHC017 "function has been deprecated and will be removed in future, Use void applyAssignAsGunner(Vehicle& vehicle, Unit& unit) instead" /*Warning message for assignAsGunner(Vehicle& vehicle, Unit& unit)*/
#define UVHC018 "function has been deprecated and will be removed in future, Use void applyAssignAsCargo(Vehicle& vehicle, Unit& unit) instead" /*Warning message for assignAsCargo(Vehicle& vehicle, Unit& unit)*/
#define UVHC019 "function has been deprecated and will be removed in future, Use void applyAssignAsCommander(Vehicle& vehicle, Unit& unit, double delay) instead" /*Warning message for assignAsCommander(Vehicle& vehicle, Unit& unit, double delay)*/
#define UVHC020 "function has been deprecated and will be removed in future, Use void applyAnimation(Vehicle& vehicle, string animationName, double phase) instead" /*Warning message for animateVehicle( Vehicle& vehicle ,string animationName,double phase)*/
#define UVHC021 "function has been deprecated and will be removed in future, Use void applyAssignAsDriver(Vehicle& vehicle, Unit& unit, double delay) instead" /*Warning message for assignAsDriver(Vehicle& vehicle, Unit& unit, double delay)*/
#define UVHC022 "function has been deprecated and will be removed in future, Use void applyAssignAsGunner(Vehicle& vehicle, Unit& unit, double delay) instead" /*Warning message for assignAsGunner(Vehicle& vehicle, Unit& unit, double delay)*/
#define UVHC023 "function has been deprecated and will be removed in future, Use void applyWeaponSelection(Vehicle& vehicle, string muzzleName) instead" /*Warning message for selectWeapon(Vehicle& vehicle, string muzzleName)*/
#define UVHC024 "function has been deprecated and will be removed in future, Use void applyAssignAsCargo(Vehicle& vehicle, Unit& unit, double delay) instead" /*Warning message for assignAsCargo(Vehicle& vehicle, Unit& unit, double delay)*/
#define UVHC025 "function has been deprecated and will be removed in future, Use void applyMagazineAddition(Vehicle& vehicle, string magName) instead" /*Warning message for addMagazine(Vehicle& vehicle, string name)*/
#define UVHC026 "function has been deprecated and will be removed in future, Use int getMagazineCount(Vehicle& vehicle) instead" /*Warning message for countMagazines(Vehicle& vehicle)*/
#define UVHC027 "function has been deprecated and will be removed in future, Use void applyWeaponAddition(Vehicle& vehicle, string name) instead" /*Warning message for addWeapon(Vehicle& vehicle, string name)*/
#define UVHC028 "function has been deprecated and will be removed in future, Use void applyWeaponState(Vehicle& vehicle, Weapon& weapon, bool reloadStatus, double reloadTime) instead" /*Warning message for setWeaponState(Vehicle& vehicle,Weapon& weapon, bool reloadStatus,double reloadTime)*/
#define UVHC029 "function has been deprecated and will be removed in future, Use void applyWeaponRemove(Vehicle& vehicle, string name) instead" /*Warning message for removeWeapon(Vehicle& vehicle, string name)*/
#define UVHC030 "function has been deprecated and will be removed in future, Use void isWeaponAvailable(Vehicle& vehicle, string name) instead" /*Warning message for hasWeapon(Vehicle& vehicle, string name)*/
#define UVHC031 "function has been deprecated and will be removed in future, Use void applyMagazineRemove(Vehicle& vehicle, string magName) instead" /*Warning message for removeMagazine(Vehicle& vehicle, string name)*/
#define UVHC032 "function has been deprecated and will be removed in future, Use void applyMagazineRemoveAll(Vehicle& vehicle, string magName) instead" /*Warning message for removeallMagazines(Vehicle& vehicle, string name)*/
#define UVHC033 "function has been deprecated and will be removed in future, Use void applyWeaponDirection(Vehicle& vehicle, Turret& turret, double azimuth, double elevation, bool transition) instead" /*Warning message for setWeaponDirection(Vehicle& vehicle, Turret& turret, double azimuth, double elevation, bool transition)*/
#define UVHC034 "function has been deprecated and will be removed in future, Use void applyGunnerInputDisable(Vehicle& vehicle, Turret& turret, bool status = true) instead" /*Warning message for disableGunnerInput(Vehicle& vehicle, Turret& turret, bool status = true)*/
#define UVHC035 "function has been deprecated and will be removed in future, Use void applyVehicleAmmo(Vehicle& vehicle, double ammovalue) instead" /*Warning message for setVehicleAmmo(Vehicle& vehicle, float ammovalue) and setVehicleAmmo(Vehicle& vehicle, double ammovalue)*/
#define UVHC036 "function has been deprecated and will be removed in future, Use void applyWeaponCargo(Vehicle& vehicle, string weaponName, int count) instead" /*Warning message for addWeaponToCargo(Vehicle& vehicle, string weaponName, int count)*/
#define UVHC037 "function has been deprecated and will be removed in future, Use void applyMagazineCargo(Vehicle& vehicle, string magName, int count) instead" /*Warning message for addMagazineToCargo(Vehicle& vehicle, string weaponName, int count)*/
#define UVHC038 "function has been deprecated and will be removed in future, Use void applyWeaponCargoRemoveAll(Vehicle& vehicle) instead" /*Warning message for clearAllWeaponFromCargo(Vehicle& vehicle)*/
#define UVHC039 "function has been deprecated and will be removed in future, Use void applyMagazineCargoRemoveAll(Vehicle& vehicle) instead" /*Warning message for clearAllMagazineFromCargo(Vehicle& vehicle)*/
#define UVHC040 "function has been deprecated and will be removed in future, Use void applyWeaponSafety(Vehicle& vehicle, Turret& turret, bool safe) instead" /*Warning message for setWeaponSafety(Vehicle& vehicle, Turret& turret, bool safe)*/
#define UVHC041 "function has been deprecated and will be removed in future, Use void applyNetworkShellMode(Vehicle& vehicle, bool mode) instead" /*Warning message for setNetworkShellMode(Vehicle& vehicle, bool mode)*/
#define UVHC042 "function has been deprecated and will be removed in future, Use double applyAirborne(Vehicle& vehicle) instead" /*Warning message for makeAirborne(Vehicle& vehicle)*/
#define UVHC043 "function has been deprecated and will be removed in future, Use void applyHeightFlyIn(Vehicle& vehicle, double height) instead" /*Warning message for flyInHeight(Vehicle& vehicle, double height)*/
#define UVHC044 "function has been deprecated and will be removed in future, Use void applyVectorDirAndUp(Vehicle veh, vector3D vectorDir, vector3D vectorUp, bool relative) instead" /*Warning message for setVectorDirAndUp(Vehicle veh, vector3D vectorDir, vector3D vectorUp, bool relative)*/
#define UVHC045 "function has been deprecated and will be removed in future, Use void applyCustomizedLanding(Vehicle& vehicle, LANDMODE mode) instead" /*Warning message for land(Vehicle& vehicle, LANDMODE mode)*/
#define UVHC046 "function has been deprecated and will be removed in future, Use void applyAirportAssignment(Vehicle& vehicle, int port) instead" /*Warning message for assignToAirport(Vehicle& vehicle, int port)*/
#define UVHC047 "function has been deprecated and will be removed in future, Use void applyLanding(Vehicle& vehicle, int port) instead" /*Warning message for landTo(Vehicle& vehicle, int port)*/
#define UVHC048 "function has been deprecated and will be removed in future, Use void applyVehicleRadioBroadcast(Vehicle& vehicle, string name) instead" /*Warning message for sendToVehicleRadio(Vehicle& vehicle, string name)*/
#define UVHC049 "function has been deprecated and will be removed in future, Use void applyVehicleChat(Vehicle& vehicle, string message) instead" /*Warning message for vehicleChat(Vehicle& vehicle, string message)*/
#define UVHC050 "function has been deprecated and will be removed in future, Use void applyWeaponCargoRemove(Vehicle& vehicle, string weapon) instead" /*Warning message for removeWeaponFromCargo(Vehicle& vehicle, string weapon)*/
#define UVHC051 "function has been deprecated and will be removed in future, Use void applyMagazineCargoRemove(Vehicle& vehicle, string magazine) instead" /*Warning message for removeMagazineFromCargo(Vehicle& vehicle, string magazine)*/
#define UVHC052 "function has been deprecated and will be removed in future, Use void applyMagazineTurretRemove(Vehicle& vehicle, string magazine, Turret::TurretPath& turretPath) instead" /*Warning message for removeMagazineFromTurret(Vehicle& vehicle, string magazine, Turret::TurretPath& turretPath)*/
#define UVHC053 "function has been deprecated and will be removed in future, Use void applySingleMagazineCargo(Vehicle& vehicle, string magazineName, int bulletCount) instead" /*Warning message for addMagazineCargoEx(Vehicle& vehicle, string magazineName, int bulletCount)*/
#define UVHC054 "function has been deprecated and will be removed in future, Use double getKnowsAbout(Vehicle& vehicle, ControllableObject& target) instead" /*Warning message for knowsAbout(Vehicle& vehicle, ControllableObject& target) and knowsAboutEx(Vehicle& vehicle, ControllableObject& target)*/
#define UVHC055 "function has been deprecated and will be removed in future, Use void applyVehicleLockEnable(Vehicle& vehicle, bool playerLock) instead" /*Warning message for lockVehicle(Vehicle& vehicle, bool playerLock)*/
#define UVHC056 "function has been deprecated and will be removed in future, Use void applyVehicleLockEnable(Vehicle& vehicle, bool playerLock, bool allLock) instead" /*Warning message for lockVehicle(Vehicle& vehicle, bool playerLock, bool allLock)*/
#define UVHC057 "function has been deprecated and will be removed in future, Use void applyGroupLeave(Vehicle& vehicle, Group& group) instead" /*Warning message for groupLeaveVehicle(Group& group, Vehicle& vehicle)*/
#define UVHC058 "function has been deprecated and will be removed in future, Use void applyGroupLeave(Vehicle& vehicle, Unit& unit) instead" /*Warning message for groupLeaveVehicle(Unit& unit, Vehicle& vehicle*/
#define UVHC059 "function has been deprecated and will be removed in future, Use void applyAttackEnable(Vehicle& vehicle, bool boolVal) instead" /*Warning message for setEnableAttack(Vehicle& vehicle, bool boolVal)*/
#define UVHC060 "function has been deprecated and will be removed in future, Use void applyRotorWash(double strength, double diameter) instead" /*Warning message for setRotorWash(double strength, double diameter)*/
#define UVHC061 "function has been deprecated and will be removed in future, Use void applyVehicleLights(Vehicle& vehicle, int main, int side, int convoy, int left, int right) instead" /*Warning message for setVehicleLights(Vehicle& vehicle,int main, int side, int convoy, int left, int right)*/
#define UVHC062 "function has been deprecated and will be removed in future, Use void applyBlackFadeEnable(Vehicle& vehicle, VEHICLE_ACTION actions, bool value) instead" /*Warning message for allowBlackFade(Vehicle& vehicle, VEHICLE_ACTION actions, bool value)*/
#define UVHC063 "function has been deprecated and will be removed in future, Use void applyGetOut(Unit& unit) instead" /*Warning message for doGetOut(Unit& unit)*/
#define UVHC064 "function has been deprecated and will be removed in future, Use void applyCommandGetOut(Unit& unit) instead" /*Warning message for commandGetOut(Unit& unit)*/
#define UVHC065 "function has been deprecated and will be removed in future, Use void applyInitStatement(Vehicle& vehicle, string statement) instead" /*Warning message for setInitStatement(Vehicle& vehicle, string& statement)*/
#define UVHC066 "function has been deprecated and will be removed in future, Use void applyInitStatementClear(Vehicle& vehicle) instead" /*Warning message for clearInitStatement(Vehicle& vehicle)*/
#define UVHC067 "function has been deprecated and will be removed in future, Use void applyWeaponCargoGlobal(Vehicle& vehicle, Weapon& weapon, int count) instead" /*Warning message for addWeaponToCargoGlobal(Vehicle& vehicle, Weapon& weapon, int count)*/
#define UVHC068 "function has been deprecated and will be removed in future, Use void applyMagazineCargoGlobal(Vehicle& vehicle, string magName, int count) instead" /*Warning message for addWeaponToCargoGlobal(Vehicle& vehicle, Weapon& weapon, int count)*/
#define UVHC069 "function has been deprecated and will be removed in future, Use void applyWeaponCargoRemoveGlobal(Vehicle& vehicle) instead" /*Warning message for clearWeaponCargoGlobal(Vehicle& vehicle)*/
#define UVHC070 "function has been deprecated and will be removed in future, Use void applyMagazineCargoRemoveGlobal(Vehicle& vehicle) instead" /*Warning message for clearMagazineCargoGlobal(Vehicle& vehicle)*/
#define UVHC071 "function has been deprecated and will be removed in future, Use void applyCommandMove(Vehicle& vehicle, position3D position) instead" /*Warning message for commandMove(Vehicle& vehicle, position3D position)*/
#define UVHC072 "function has been deprecated and will be removed in future, Use void applyFire(Vehicle& vehicle, string weaponName) instead" /*Warning message for fire(Vehicle& vehicle, string weaponName)*/
#define UVHC073 "function has been deprecated and will be removed in future, Use void applyFire(Vehicle& vehicle, string muzzleName, string modeName) instead" /*Warning message for fire(Vehicle& vehicle, string muzzleName, string modeName)*/
#define UVHC074 "function has been deprecated and will be removed in future, Use void applySoundEnable(Vehicle& vehicle, VEHICLE_SOUND_TYPES types, bool value) instead" /*Warning message for allowSound(Vehicle& vehicle,VEHICLE_SOUND_TYPES types,bool value)*/

/*! PlayerUtilities Class*/
#define UPLY001 "function has been deprecated and will be removed in future, Use void applyPlayable(Unit& player, bool playable) instead" /*Warning message for setPlayable(Unit& player)*/
#define UPLY002 "function has been deprecated and will be removed in future, Use bool isSwitchPlayer(Unit& nplayer , Unit& oplayer) instead" /*Warning message for switchPlayer(Unit& nplayer , Unit& oplayer)*/
#define UPLY003 "function has been deprecated and will be removed in future, Use void applyFiringForce(Unit& player) instead" /*Warning message for forceFire(Unit& player)*/ 
#define UPLY004 "function has been deprecated and will be removed in future, Use float getOpticsBrightness() instead" /*Warning message for GetOpticsBrightness()*/
#define UPLY005 "function has been deprecated and will be removed in future, Use float getOpticsBrightnessMin() instead" /*Warning message for GetOpticsBrightnessMin()*/
#define UPLY006 "function has been deprecated and will be removed in future, Use float getOpticsBrightnessMax() instead" /*Warning message for GetOpticsBrightnessMax()*/
#define UPLY007 "function has been deprecated and will be removed in future, Use float getOpticsMaxGainAmplifier() instead" /*Warning message for GetOpticsMaxGainAmplifier()*/ 
#define UPLY008 "function has been deprecated and will be removed in future, Use float getOpticsBrightnessOffset() instead" /*Warning message for GetOpticsBrightnessOffset()*/ 
#define UPLY009 "function has been deprecated and will be removed in future, Use bool getOpticsAutoBS() instead" /*Warning message for GetOpticsAutoBS()*/ 
#define UPLY010 "function has been deprecated and will be removed in future, Use float getOpticsBlurCoef() instead" /*Warning message for GetOpticsBlurCoef()*/ 
#define UPLY016 "function has been deprecated and will be removed in future, Use void applyOpticsAutoBC(bool mode) instead" /*Warning message for setOpticsAutoBC(bool mode)*/  
#define UPLY018 "function has been deprecated and will be removed in future, Use void applyOpticsDOF(double focus, double blur) instead" /*Warning message for setOpticsDOF(double focus, double blur)*/
#define UPLY019 "function has been deprecated and will be removed in future, Use void applyAllowControlslnDialogMove(bool boolVal) instead" /*Warning message for allowMovControlsInDialog(bool boolVal)*/ 
#define UPLY020 "function has been deprecated and will be removed in future, Use void applyEnableCommanding(bool commanding) instead" /*Warning message for enableCommanding(bool commanding)*/ 
#define UPLY021 "function has been deprecated and will be removed in future, Use bool applyPlayer(Unit& player) instead" /*Warning message for setPlayer(Unit& player)*/  
#define UPLY023 "function has been deprecated and will be removed in future, Use double getOpticsBrightnessEx() instead" /*Warning message for GetOpticsBrightnessEx()*/
#define UPLY024 "function has been deprecated and will be removed in future, Use double getOpticsBrightnessMinEx() instead" /*Warning message for GetOpticsBrightnessMinEx()*/ 
#define UPLY025 "function has been deprecated and will be removed in future, Use double getOpticsBrightnessMaxEx() instead" /*Warning message for GetOpticsBrightnessMaxEx()*/ 
#define UPLY026 "function has been deprecated and will be removed in future, Use double getOpticsMaxGainAmplifierEX() instead" /*Warning message for GetOpticsMaxGainAmplifierEX()*/   
#define UPLY027 "function has been deprecated and will be removed in future, Use double getOpticsBrightnessOffsetEx() instead" /*Warning message for GetOpticsBrightnessOffsetEx()*/    
#define UPLY028 "function has been deprecated and will be removed in future, Use double getOpticsBlurCoefEx() instead" /*Warning message for GetOpticsBlurCoefEx()*/     
#define UPLY029 "function has been deprecated and will be removed in future, Use void applyOpticsBrightness(double brightnessVal) instead" /*Warning message for setOpticsBrightness(double brightnessVal)*/
#define UPLY030 "function has been deprecated and will be removed in future, Use void applyOpticsBrightnessMin(double minBrightVal) instead" /*Warning message for setOpticsBrightnessMin(double minBrightVal)*/
#define UPLY031 "function has been deprecated and will be removed in future, Use void applyOpticsBrightnessMax(double maxBrightVal) instead" /*Warning message for setOpticsBrightnessMax(double maxBrightVal)*/ 
#define UPLY032 "function has been deprecated and will be removed in future, Use void applyOpticsMaxGainAmplifier(double maxGainAmp) instead" /*Warning message for setOpticsMaxGainAmplifier(double maxGainAmp)*/ 
#define UPLY033 "function has been deprecated and will be removed in future, Use void applyOpticsBrightnessOffset(double brightOff) instead" /*Warning message for setOpticsBrightnessOffset(double brightOff)*/
#define UPLY034 "function has been deprecated and will be removed in future, Use void applyOpticsBlurCoef(double blurAmount) instead" /*Warning message for setOpticsBlurCoef(double blurAmount)*/

/*! TriggerUtilities Class */
#define UTGR001 "function has been deprecated and will be removed in future, Use double applyOnActivationStatement(Trigger& trigger, string command) instead"  /*Warning message for addTriggerOnActivated(Trigger& trigger, string command)*/
#define UTGR002 "function has been deprecated and will be removed in future, Use void applyOnActivationStatementRemoval(Trigger& trigger, double index) instead"  /*Warning message for removeTriggerOnActivated(Trigger& trigger, double index)*/
#define UTGR003 "function has been deprecated and will be removed in future, Use void applyActivationType(Trigger& trig, string& by, string& type, bool repeat) instead"  /*Warning message for setTriggerActivation(Trigger& trig, string by, string type, bool repeat)*/
#define UTGR004 "function has been deprecated and will be removed in future, Use list<ControllableObject> TriggerUtilities::getAttachedObjects(Trigger& trig) instead"  /*Warning message for triggerAttachedObj(Trigger& trig)*/
#define UTGR005 "function has been deprecated and will be removed in future, Use void applyObjectAttach(Trigger& trig, ControllableObject& co) instead"  /*Warning message for triggerAttachObj(Trigger& trig, ControllableObject& co)*/
#define UTGR006 "function has been deprecated and will be removed in future, Use void applyObjectDetach(Trigger& trig, ControllableObject& co) instead"  /*Warning message for triggerDetachObj(Trigger& trig, ControllableObject& co)*/
#define UTGR007 "function has been deprecated and will be removed in future, Use void applySynchronization(Trigger& trig, list<Waypoint> wplist) instead"  /*Warning message for synchronizeTrigger(Trigger& trig, list<Waypoint> wplist)*/
#define UTGR008 "function has been deprecated and will be removed in future, Use void applyMusicEffect(Trigger& trig, string& trackName) instead"  /*Warning message for setMusicEffect(Trigger& trig, string& trackName)*/
#define UTGR009 "function has been deprecated and will be removed in future, Use void applySoundEffect(Trigger& trig, string& sound, string& voice, string& soundEnv, string& soundDet) instead"  /*Warning message for setSoundEffect(Trigger& trig, string& sound, string& voice, string& soundEnv, string& soundDet)*/
#define UTGR010 "function has been deprecated and will be removed in future, Use void applyTitleEffect(Trigger& trig, TITLE_EFFECT_CLASS classType, TITLE_EFFECT_TYPE effect, string& text) instead"  /*Warning message for setTitleEffect(Trigger& trig, string& type, string& effect, string& text)*/
#define UTGR011 "function has been deprecated and will be removed in future, Use void applyEffectCondition(Trigger& trig, string& statement) instead"  /*Warning message for setEffectCondition(Trigger& trig, string& statement)*/
#define UTGR012 "function has been deprecated and will be removed in future, Use the alternative mechanism mentioned in header file comment" /*Warning message for networkIdToTrigger(Trigger& trig, NetworkID id)*/
#define UTGR014 "function has been deprecated and will be removed in future, Use void applyVehicleAttach(Trigger& trig, ControllableObject& co) instead" /*Warning message for triggerAttachVehicle(Trigger& trig, ControllableObject& co)*/

/*! WaypointUtilities Class */
#define UWPT001 "function has been deprecated and will be removed in future, Use void applyObjectAttach(Waypoint& wp, ControllableObject& co) instead"  /*Warning message for WaypointAttachObject(Waypoint& wp, ControllableObject& co)*/
#define UWPT002 "function has been deprecated and will be removed in future, Use void applyLoiterRadius(Waypoint& wp, double radius) instead"  /*Warning message for setLoiterRadius(Waypoint& wp, double radius)*/
#define UWPT003 "function has been deprecated and will be removed in future, Use void applyLoiterType(Waypoint& wp, string type) instead"  /*Warning message for setLoiterType(Waypoint& wp, string type)*/
#define UWPT004 "function has been deprecated and will be removed in future, Use void applyHousePosition(Waypoint& wp, int pos) instead"  /*Warning message for setHousePosition(Waypoint& wp, int pos)*/
#define UWPT005 "function has been deprecated and will be removed in future, Use void applyVariable(Waypoint& wp, string& name, VBS2Variable& val, bool isPublic) instead"  /*Warning message for setWaypointVariable(Waypoint& wp, string name, VBS2Variable& val, bool pub)*/
#define UWPT006 "function has been deprecated and will be removed in future, Use void applyMusicEffect(Waypoint& wp, string& trackName) instead"  /*Warning message for setMusicEffect(Waypoint& wp, string& trackName)*/
#define UWPT007 "function has been deprecated and will be removed in future, Use void applySoundEffect(Waypoint& wp, string& sound, string& voice, string& soundEnv, string& soundDet) instead"  /*Warning message for setSoundEffect(Waypoint& wp, string& sound, string& voice, string& soundEnv, string& soundDet)*/
#define UWPT008 "function has been deprecated and will be removed in future, Use void applyTitleEffect(Waypoint& wp, TITLE_EFFECT_CLASS classType, TITLE_EFFECT_TYPE effect, string& text) instead"  /*Warning message for setTitleEffect(Waypoint& wp, string& type, string& effect, string& text)*/
#define UWPT009 "function has been deprecated and will be removed in future, Use void applyEffectCondition(Waypoint& wp, string& statement) instead"  /*Warning message for setEffectCondition(Waypoint& wp, string& statement)*/
#define UWPT010 "function has been deprecated and will be removed in future, Use void applyCompletionRadius(Waypoint& wp, double radius) instead"  /*Warning message for setWaypointCompletionRadius(Waypoint& wp, double radius)*/
#define UWPT011 "function has been deprecated and will be removed in future, Use void applyVisibility(Waypoint& wp, bool visible) instead"  /*Warning message for setWaypointVisible(Waypoint& wp, bool visible)*/

/*! TerrainUtilities Class */
#define UTER001 "function has been deprecated and will be removed in future, Use double getTerrainHeight(double xCoordinate, double zCoordinate) instead" /*Warning message for TerrainHeight(double xCoordinate, double zCoordinate)*/
#define UTER002 "function has been deprecated and will be removed in future, Use double getRoadSurfaceHeight(double xCoordinate, double zCoordinate) instead" /*Warning message for RoadSurfaceHeight(double xCoordinate, double zCoordinate)*/ 
#define UTER003 "function has been deprecated and will be removed in future, Use bool isUprightCylinderCollision(position3D& center, double radius) instead" /*Warning message for uprightCylinderCollision(position3D& center, double radius)*/ 
#define UTER004 "function has been deprecated and will be removed in future, Use bool applyMapExportToSHP(string objectFileName, string roadFileName) instead" /*Warning message for exportTerrainMapSHP(string objectFileName , string roadFileName)*/ 
#define UTER005 "function has been deprecated and will be removed in future, Use string getPositionToGrid(position3D pos) instead" /*Warning message for positionToGrid(position3D pos)*/
#define UTER006 "function has been deprecated and will be removed in future, Use position3D getGridToPosition(string gridPosition) instead" /*Warning message for gridtoPosition(string gridPosition)*/
#define UTER007 "function has been deprecated and will be removed in future, Use string getPositionToMGRS(position3D pos, int precision) instead" /*Warning message for positionToMGRS(position3D pos, int precision)*/
#define UTER008 "function has been deprecated and will be removed in future, Use string getPositionToMGRS(position3D pos) instead" /*Warning message for positionToMGRS(position3D pos)*/
#define UTER009 "function has been deprecated and will be removed in future, Use string getPositionToLat(position3D pos , int precision) instead"/*Warning message for positionToLat(position3D pos , int precision)*/
#define UTER010 "function has been deprecated and will be removed in future, Use string getPositionToLat(position3D pos) instead"/*Warning message for positionToLat(position3D pos)*/
#define UTER011 "function has been deprecated and will be removed in future, Use string getPositionToLon(position3D pos, int precision) instead" /*Warning message for positionToLon(position3D pos, int precision)*/
#define UTER012 "function has been deprecated and will be removed in future, Use string getPositionToLon(position3D pos) instead" /*Warning message for positionToLon(position3D pos)*/
#define UTER013 "function has been deprecated and will be removed in future, Use void applyTerrainHeightsReset() instead" /*Warning message for resetTerrainHeights()*/
#define UTER015 "function has been deprecated and will be removed in future, Use void applyTerrainHeight(double x, double z, double height) instead" /*Warning message for setTerrainHeight(double x, double z, double height)*/
#define UTER017 "function has been deprecated and will be removed in future, Use void applyTerrainHeightArea(position2D pos1, position2D pos2,double height, bool changeLower, bool changeHigher) instead" /*Warning message for setTerrainHeightArea(position2D pos1, position2D pos2,double height, bool changeLower, bool changeHigher)*/
#define UTER020 "function has been deprecated and will be removed in future, Use void applyFlattenGround(position3D position, double radius, double flatten) instead" /*Warning message for setFlattenGround(position3D position, double radius, double flatten)*/
#define UTER021 "function has been deprecated and will be removed in future, Use void applyFlattenGround(ControllableObject& co , double radius, double flatten) instead" /*Warning message for setFlattenGround(ControllableObject& co , double radius, double flatten)*/
#define UTER023 "function has been deprecated and will be removed in future, Use position3D getGridCenterToPosition(string position) instead" /*Warning message for gridCenterToPosition(string position)*/
#define UTER024 "function has been deprecated and will be removed in future, Use bool applyASCIIGridExport(string fileName) instead" /*Warning message for exportASCIIGrid(string fileName)*/
#define UTER025 "function has been deprecated and will be removed in future, Use bool isWaterOnSurface(position3D pos) instead" /*Warning message for surfaceIsWater(position3D pos)*/
#define UTER026 "function has been deprecated and will be removed in future, Use void applyUserChart(string chart) instead" /*Warning message for setUserChart(string chart)*/
#define UTER027 "function has been deprecated and will be removed in future, Use void applyUserChartDrawObjects(bool boolVal) instead" /*Warning message for setUserChartDrawObjects(bool boolVal)*/
#define UTER028 "function has been deprecated and will be removed in future, Use void applyTerrainGrid(double grid) instead" /*Warning message for setTerrainGrid(double grid)*/
#define UTER029 "function has been deprecated and will be removed in future, Use bool applyASCIIGridFileExport(string fileName) instead" /*Warning message for exportASCIIGridFile(string fileName)*/ 
#define UTER031 "function has been deprecated and will be removed in future, Use void applyTerrainHeightsWrpReset() instead" /*Warning message for resetTerrainHeightsWrp()*/
#define UTER037 "function has been deprecated and will be removed in future, Use COLLISION_INFO getCylinderCollision(position3D startPos, position3D endPos, COLLISIONTESTTYPE type, double radius, double density, ControllableObject ignoreObject) instead" /*Warning message for bool cylinderCollision(param1, param2, ect..)*/

/*! CameraUtilities */
#define UCMR001 "function has been deprecated and will be removed in future, Use void applyCamCommand(Camera& c, string camCommand) instead" /*Warning message for void camCommand(Camera& c, string camCommand)*/
#define UCMR002 "function has been deprecated and will be removed in future, Use NetworkID getCameraOnObjectID() instead" /*Warning message for NetworkID  cameraOnObjectID()*/
#define UCMR003 "function has been deprecated and will be removed in future, Use position3D getPositionCameraToWorld(position3D camPos) instead" /*Warning message for position3D positionCameraToWorld(position3D camPos)*/
#define UCMR004 "function has been deprecated and will be removed in future, Use void applyTargetReset(Camera& camera) instead" /*Warning message for void resetTargets(Camera& camera)*/
#define UCMR005 "function has been deprecated and will be removed in future, Use void applyCameraCollision(bool collide) instead" /*Warning message for void setCameraCollision(bool collide)*/
#define UCMR006 "function has been deprecated and will be removed in future, Use void applyCamRadar(bool mode, ControllableObject& object,position3D offset) instead" /*Warning message for void setCamRadar(bool mode, ControllableObject& object,position3D offset)*/
#define UCMR007 "function has been deprecated and will be removed in future, Use void applyCinemaBorder(bool show) instead" /*Warning message for void showCinemaBorder(bool show)*/

/*! AARUtilities Class */
#define UAAR001 "function has been deprecated and will be removed in future, Use bool applyAARLoad(string& fileName) instead" /*Warning message for load(string fileName)*/
#define UAAR002 "function has been deprecated and will be removed in future, Use void applyAARPlay() instead" /*Warning message for play()*/
#define UAAR003 "function has been deprecated and will be removed in future, Use void applyAARPause() instead" /*Warning message for pause()*/
#define UAAR004 "function has been deprecated and will be removed in future, Use void applyAARStart() instead" /*Warning message for start()*/
#define UAAR005 "function has been deprecated and will be removed in future, Use void applyAARStop() instead" /*Warning message for stop()*/
#define UAAR006 "function has been deprecated and will be removed in future, Use double getCurrentTime() instead" /*Warning message for currentTime()*/
#define UAAR007 "function has been deprecated and will be removed in future, Use double getReplayLength() instead" /*Warning message for replayLength()*/
#define UAAR008 "function has been deprecated and will be removed in future, Use void applyAARRepeat(bool value) instead" /*Warning message for setRepeat(bool value)*/
#define UAAR009 "function has been deprecated and will be removed in future, Use void applyAARPlayBackTime(double time) instead" /*Warning message for  setPlayBackTime(double time)*/
#define UAAR010 "function has been deprecated and will be removed in future, Use void applyAARRecord() instead" /*Warning message for record()*/
#define UAAR011 "function has been deprecated and will be removed in future, Use void applyAARRecordingStop() instead" /*Warning message for stopRecording()*/
#define UAAR012 "function has been deprecated and will be removed in future, Use void applyAARSave(string fileName) instead" /*Warning message for save(string fileName)*/
#define UAAR013 "function has been deprecated and will be removed in future, Use void applyAARUnload() instead" /*Warning message for unload*/
#define UAAR014 "function has been deprecated and will be removed in future, Use int applyAARBookMarkAddition(double time, string& name, string& message) instead" /*Warning message for addBookMark(double time, string name, string message)*/
#define UAAR015 "function has been deprecated and will be removed in future, Use void applyAARBookMarkRemove(int index) instead" /*Warning message for removeBookMark(int index)*/

/*! MarkerUtilities  Class */
#define UMKR001 "function has been deprecated and will be removed in future, Use void applyObjectAttach(Marker& marker, ControllableObject& co, position3D offset) instead" /*Warning message for void attachToObject(Marker& marker, ControllableObject& co, position3D offset)*/
#define UMKR002 "function has been deprecated and will be removed in future, Use void applyLocalObjectAttach(Marker& marker, ControllableObject& co, position3D offset) instead" /*Warning message for void attachToObjectlocal(Marker& marker, ControllableObject& co, position3D offset)*/

/*! EffectsUtilities Class */
#define UEFT001 "function has been deprecated and will be removed in future, Use void applySoundPlaying(string soundName) instead" /*Warning message for void playSound(string soundName) */
#define UEFT002 "function has been deprecated and will be removed in future, Use void applyMusicPlaying(string musicName) instead" /*Warning message for void playMusic(string musicName)*/
#define UEFT003 "function has been deprecated and will be removed in future, Use void applyMusicPlaying(string musicName, double time)instead" /*Warning message for void playMusic(string musicName, double time);*/

/*! EnvironmentStateUtilities */
#define UENV001 "function has been deprecated and will be removed in future, Use void applyRandomWeather(bool mode) instead" /*Warning message for void setRandomWeather(bool mode)*/

/*! CollisionUtilities Class */
#define UCOL001 "function has been deprecated and will be removed in future, Use list<CollisionDetectionReturnValue> getCollision(position3D startPos, position3D endPos, double radius, ControllableObject& ignoreObject, COLLISIONTESTTYPE collisionType=FIRE) instead" /*Warning message for static list<CollisionDetectionReturnValue> CollisionDetection(position3D startPos, position3D endPos, double radius, ControllableObject& ignoreObject, COLLISIONTESTTYPE collisionType=FIRE);void playSound(string soundName)*/

/*! GeneralUtilities Class */
#define UGNR001 "function has been deprecated and will be removed in future, Use applyAccTime(double accTime)" /*Warning message for void setAccTime(double accTime)*/
#define UGNR002 "function has been deprecated and will be removed in future, Use void ApplyShutDown();" /*Warning message for void void ExitVBS()*/
#define UGNR003 "function has been deprecated and will be removed in future, Use  string getApplicationState()" /*Warning message for string applicationState()*/
#define UGNR004 "function has been deprecated and will be removed in future, Use void applyEMFExport(string fileName, bool drawGrid = true, bool drawContours = true, int drawTypes = 256, bool flipVertical = false) " /*Warning message for void exportEMF(string fileName, bool drawGrid = true, bool drawContours = true, int drawTypes = 256, bool flipVertical = false)*/
#define UGNR005 "function has been deprecated and will be removed in future, Use void applyURLOpen(string url)" /*Warning message for void openURL(string url)*/
#define UGNR006 "function has been deprecated and will be removed in future, Use void applyRederingDisable(bool disable)" /*Warning message for void DisableRendering(bool disable)*/
#define UGNR007 "function has been deprecated and will be removed in future, Use double getBenchMark()" /*Warning message for double BenchMark()*/
#define UGNR008 "function has been deprecated and will be removed in future, Use void applytimeSkip(float duration)" /*Warning message for void skipTime(float duration)*/
#define UGNR009 "function has been deprecated and will be removed in future, Use void applytimeSkip(double duration)" /*Warning message for void skipTime(double duration)*/
#define UGNR010 "function has been deprecated and will be removed in future, Use void applyMapForceDisplay(bool show)" /*Warning message for void forceMap(bool show)*/
#define UGNR011 "function has been deprecated and will be removed in future, Use bool isRequiredVersion(string version)" /*Warning message for bool requiredVersion(string version)*/
#define UGNR012 "function has been deprecated and will be removed in future, Use void applyRadioEnable(bool state)" /*Warning message for void EnableRadio(bool state)*/
#define UGNR013 "function has been deprecated and will be removed in future, Use void applyTeamSwitchEnable(bool enable)" /*Warning message for void EnableTeamSwitch(bool enable)*/
#define UGNR014 "function has been deprecated and will be removed in future, Use void applyGPSEnable(bool enable)" /*Warning message for void EnableGPS(bool enable)*/
#define UGNR015 "function has been deprecated and will be removed in future, Use bool applySubtitlesShowing(bool enable)" /*Warning message for bool showSubtitles(bool enable)*/
#define UGNR016 "function has been deprecated and will be removed in future, Use void applyShadowDistance(double settingValue)" /*Warning message for  void setShadowDistance(double settingValue)*/
#define UGNR017 "function has been deprecated and will be removed in future, Use void applyObjectDetail(int settingValue)" /*Warning message for void setObjectDetail(int settingValue)*/
#define UGNR018 "function has been deprecated and will be removed in future, Use void applyDistanceView(double settingValue)" /*Warning message for void setViewDistance(double settingValue)*/
#define UGNR019 "function has been deprecated and will be removed in future, Use void applyShadowsCamHeightCoef(double settingValue)" /*Warning message for  void setShadowsCamHeightCoef(double settingValue)*/
#define UGNR020 "function has been deprecated and will be removed in future, Use void applyScreenResolution(int width, int height);" /*Warning message for void setScreenResolution(int width, int height)*/
#define UGNR021 "function has been deprecated and will be removed in future, Use void applyMultiChannelConfig(list<CONFIG_VALUES_MULTICHANNEL> param)" /*Warning message for void setMultiChannelConfig(list<CONFIG_VALUES_MULTICHANNEL> param)*/

/*! WorldUtilities Class */
#define UWLD001 "function has been deprecated and will be removed in future, Use position2D applyWorldToScreen(position3D pos) instead" /*Warning message for position2D worldToScreen(position3D pos)*/
#define UWLD002 "function has been deprecated and will be removed in future, Use position3D applyScreenToWorld(position2D pos) instead" /*Warning message for position3D ScreenToworld(position2D pos)*/
#define UWLD003 "function has been deprecated and will be removed in future, Use position3D applyGeometricDrawing(int lowLX, int lowLY, int upperRX, int upperRY) instead" /*Warning message for void drawGeometry(int lowLX, int lowLY, int upperRX, int upperRY)*/
#define UWLD004 "function has been deprecated and will be removed in future, Use void applyAmbientLifeInitialization() instead" /*Warning message for void initAmbientLife() */

/*! ExplosiveUtilities Class */
#define UEXP001 "function has been deprecated and will be removed in future, Use void applyDetonation(ControllableObject& co) instead" /*Warning message for void detonate(ControllableObject& co)*/

/*! InputUtilities Class */
#define UINP001 "function has been deprecated and will be removed in future, Use int applyKeyBinding(string keyAction, string command, bool suppress) instead" /*Warning message for int bindKey(string keyAction, string command, bool suppress)*/
#define UINP002 "function has been deprecated and will be removed in future, Use int applyKeyBinding(int keyCode, string command, bool suppress) instead" /*Warning message for int bindKey(int keyCode, string command, bool suppress)*/
#define UINP003 "function has been deprecated and will be removed in future, Use int applyKeyBinding(string keyAction, string command, bool suppress, bool enableFloat, KEY_BIND_RULE rule, string vehicleType = "") instead" /*Warning message for int bindKey(string keyAction, string command, bool suppress, bool enableFloat, KEY_BIND_RULE rule, string vehicleType = "")*/
#define UINP004 "function has been deprecated and will be removed in future, Use int applyKeyBinding(int keyCode, string command, bool suppress, bool enableFloat, KEY_BIND_RULE rule, string vehicleType = "") instead" /*Warning message for int bindKey(int keyCodes, string command, bool suppress, bool enableFloat, KEY_BIND_RULE rule, string vehicleType = "")*/
#define UINP005 "function has been deprecated and will be removed in future, Use void applyKeyUnbinding(int keyIndex) instead" /*Warning message for Use void unBindKey(int keyIndex)*/
#define UINP006 "function has been deprecated and will be removed in future, Use void applyUserInputRecord(string fileName) instead" /*Warning message for Use void recordUserInput(string fileName)*/
#define UINP007 "function has been deprecated and will be removed in future, Use void applyUserInputStreamStop() instead" /*Warning message for Use void stopUserInputStreaming()*/
#define UINP008 "function has been deprecated and will be removed in future, Use void applyUserInputDisable(bool state) instead" /*Warning message for Use void disableUserInput(bool state)*/
#define UINP009 "function has been deprecated and will be removed in future, Use double getInputActionState(string action) instead" /*Warning message for float inputAction(string action) and  double inputActionEx(string action)*/
#define UINP010 "function has been deprecated and will be removed in future, Use void applyUserInputReplay(string fileName) instead" /*Warning message for void replayUserInput(string fileName)*/

/*! ObjectVectorUtilities Class */
#define UOBV001 "function has been deprecated and will be removed in future, Use void applyVectorUp(ControllableObject& co, vector3D vec) instead" /*Warning message for void setVectorUp(ControllableObject& co, vector3D vec)*/
#define UOBV002 "function has been deprecated and will be removed in future, Use void applyVectorDir(ControllableObject& co, vector3D vec) instead" /*Warning message for  void setVectorDir(ControllableObject& co, vector3D vec)*/
#define UOBV003 "function has been deprecated and will be removed in future, Use void applyPitchAndBank(ControllableObject& co, double pitch_angle, double bank_angle) instead" /*Warning message for void setPitchAndBank(ControllableObject& co, double pitch_angle, double bank_angle)*/
#define UOBV004 "function has been deprecated and will be removed in future, Use void applyDirection(ControllableObject& co, double direction) instead" /*Warning message for void void setDirection(ControllableObject& co, double direction)*/

/*! PoseControlUtilities Class */
#define UPCN001 "function has been deprecated and will be removed in future, Use void applyExternalPose(Unit& unit, bool status) instead" /*Warning message for void setExternalPose(Unit& unit, bool status)*/
#define UPCN002 "function has been deprecated and will be removed in future, Use void applyExternalPoseSkeleton(Unit& unit, bool status) instead" /*Warning message for void setExternalPoseSkeleton(Unit& unit, bool status)*/
#define UPCN003 "function has been deprecated and will be removed in future, Use void applyExternalPoseUpBody(Unit& unit ,bool status) instead" /*Warning message for void setExternalPoseUpBody(Unit& unit ,bool status)*/

/*! MissionUtilities Class */
#define UMIS001 "function has been deprecated and will be removed in future, Use void applyRoadSideDrive(ROADSIDE side) instead" /*Warning message for setDriveRoadSide(ROADSIDE side)*/
#define UMIS002 "function has been deprecated and will be removed in future, Use void applySimulationPause() instead" /*Warning message for pauseSimulation()*/
#define UMIS003 "function has been deprecated and will be removed in future, Use void applySimulatinResume() instead" /*Warning message for resumeSimulation()*/
#define UMIS004 "function has been deprecated and will be removed in future, Use void applyMissionEnd(string& endtype) instead" /*Warning message for endMission(string endtype)*/
#define UMIS005 "function has been deprecated and will be removed in future, Use void applyMissionPlay(string& campaign, string& missname, bool skipBriefing)) instead" /*Warning message for playMission(string campaign, string missname, bool skipBriefing)*/
#define UMIS006 "function has been deprecated and will be removed in future, Use bool applyCaptureStart(string& filename, double captureWidth, double captureHeight, int frameRate, list<string> codecList, bool recordSound, bool recordWithUI, double bufferSize) instead" /*Warning message for captureStart(string filename, double captureWidth, double captureHeight, int frameRate, list<string> codecList, bool recordSound, bool recordWithUI, double bufferSize)*/
#define UMIS007 "function has been deprecated and will be removed in future, Use bool applyCaptureStart(string& filename) instead" /*Warning message for captureStart(string filename)*/
#define UMIS008 "function has been deprecated and will be removed in future, Use bool isCaptureTest() instead" /*Warning message for captureTest()*/
#define UMIS009 "function has been deprecated and will be removed in future, Use void applyCaptureStop() instead" /*Warning message for captureStop()*/
#define UMIS010 "function has been deprecated and will be removed in future, Use vector<int> getMissionStartTime() instead" /*Warning message for MissionStartTime()*/
#define UMIS011 "function has been deprecated and will be removed in future, Use void applyMissionHost(string& missname) instead" /*Warning message for void hostMission(string missname)*/
#define UMIS012 "function has been deprecated and will be removed in future, Use void applyActivateKey(string keyName) instead" /*Warning message for void ActivateKey(string keyName)*/
#define UMIS013 "function has been deprecated and will be removed in future, use void applyMapShowing(bool show) instead" /*Warning message for void showMap(bool show)*/
#define UMIS014 "function has been deprecated and will be removed in future, Use void applyRadioShowing(bool show) instead" /*Warning message for void showRadio(bool show)*/
#define UMIS015 "function has been deprecated and will be removed in future, Use void applyPadShowing(bool show) instead" /*Warning message for void showPad(bool show)*/
#define UMIS016 "function has been deprecated and will be removed in future, Use void applyMissionVersion(double versionNum) instead" /*Warning message for void setMissionVersion(double versionNum)*/
#define UMIS017 "function has been deprecated and will be removed in future, Use void applyRadioClear() instead" /*Warning message for void clearRadio()*/
#define UMIS018 "function has been deprecated and will be removed in future, Use void applyObjectiveStatus(string& objectiveNumber, string& status) instead" /*Warning message for void setObjectiveStatus(string& objectiveNumber, string& status)*/
#define UMIS019 "function has been deprecated and will be removed in future, Use void applyRadioEnable(bool state) instead" /*Warning message for void enableRadio(bool state)*/
#define UMIS020 "function has been deprecated and will be removed in future, Use int getEnemyCount(Unit unit, list<Unit>& unitList)" /*Warning message for int countEnemy(Unit unit, list<Unit> unitList)*/
#define UMIS021 "function has been deprecated and will be removed in future, Use int getFriendlyCount(Unit unit, list<Unit>& unitList)" /*Warning message for int countFriendly(Unit unit, list<Unit> unitList)*/
#define UMIS022 "function has been deprecated and will be removed in future, Use int getUnknownCount(Unit unit, list<Unit>& unitList)" /*Warning message for int countUnknown(Unit unit, list<Unit> unitList)*/
#define UMIS023 "function has been deprecated and will be removed in future, Use int getSideCount(SIDE side, list<Unit> unitList)" /*Warning message for int countSide(SIDE side, list<Unit> unitList)*/
#define UMIS024 "function has been deprecated and will be removed in future, Use void applySoundVolume(double time, double volume)" /*Warning message for void setSoundVolume(double time, double volume)*/
#define UMIS025 "function has been deprecated and will be removed in future, Use void applyEventTextAdd(string text)" /*Warning message for void addEventTextInMPRecord(string text)*/
#define UMIS026 "function has been deprecated and will be removed in future, Use void applyOnPlayerDisconnected(string statement)" /*Warning message for void onPlayerDisconnected(string statement)*/
#define UMIS027 "function has been deprecated and will be removed in future, Use void applyHeaderTextAdd(string header)" /*Warning message for void addHeaderInMPRecord(string header)*/
#define UMIS028 "function has been deprecated and will be removed in future, Use void applyOnVehicleCreated(string command)" /*Warning message for void onVehicleCreated(string command)*/
#define UMIS029 "function has been deprecated and will be removed in future, Use void applyInitCommandsProcessing()" /*Warning message for void processInitCommands()*/
#define UMIS030 "function has been deprecated and will be removed in future, Use void applyFriend(SIDE side1, SIDE side2 , double value)" /*Warning message for void setFriend(SIDE side1, SIDE side2 , double value)*/
#define UMIS031 "function has been deprecated and will be removed in future, Use void void applyMusicVolume(double time, double volume)" /*Warning message for void setMusicVolume(double time, double volume)*/
#define UMIS032 "function has been deprecated and will be removed in future, Use void void applyEyeAccom(double eyeAccValue)" /*Warning message for void setEyeAccom(double eyeAccValue)*/
#define UMIS033 "function has been deprecated and will be removed in future, Use void applyInitScriptRunning()" /*Warning message for void runInitScript()*/
#define UMIS034 "function has been deprecated and will be removed in future, Use void applyWatchShowing(bool show)" /*Warning message for void showWatch(bool show)*/
#define UMIS035 "function has been deprecated and will be removed in future, Use void applyCompassShowing(bool show)" /*Warning message for void showCompass(bool show)*/
#define UMIS036 "function has been deprecated and will be removed in future, Use void applyPlayerSwitchOff()" /*Warning message for void switchOffFromPlayer()*/
#define UMIS037 "function has been deprecated and will be removed in future, Use bool applyWaterImpulse(position2D& wPos,double wAmp, double wOscTime, double halfDumpTime, double lifeTime, double radius, int index)" /*Warning message for bool setWaterImpulse(position2D& wPos, double wAmp, double wOscTime, double halfDumpTime, double lifeTime, double radius, int index)*/

/*! MapUtilities Class */
#define UMAP001 "function has been deprecated and will be removed in future, Use MapObject applyMapObjectRestore(string id); instead" /*Warning message for MapObject restoreMapObject(string id)*/
#define UMAP002 "function has been deprecated and will be removed in future, Use void applyStaticToDynamic(MapObject& obj) instead" /*Warning message for void staticToDynamic(MapObject& obj)*/

/*! RTEUtilities Class */
#define URTE001 "function has been deprecated and will be removed in future, Use position3D getPositionScreenToWorld(position2D screenPos) instead" /*Warning message for position3D positionScreenToWorld(position2D screenPos)*/
#define URTE002 "function has been deprecated and will be removed in future, Use position2D getPositionWorldToScreen(position3D worldPos) instead" /*Warning message for position2D positionWorldToScreen(position3D worldPos)*/

/*! ArmedGroundVehicleUtilities Class */
#define UAGV001 "function has been deprecated and will be removed in future, Use applyGunnerInputDisable(ArmedGroundVehicle& vehicle, string strTurretpath, bool status) instead" /*Warning message for void disableGunnerInput(ArmedGroundVehicle& vehicle, string strTurretpath , bool status)*/
#define UAGV002 "function has been deprecated and will be removed in future, Use applyFireFrom(ArmedGroundVehicle& vehicle, string weaponName) instead" /*Warning message for void fire(ArmedGroundVehicle& vehicle, string weaponName)*/

/*! ExecutionUtilities Class */
#define UEXC001 "function has been deprecated and will be removed in future, Use string applyFilePreprocess(string& filename) instead" /*Warning message for string preprocessFile(string& filename)*/
#define UEXC002 "function has been deprecated and will be removed in future, Use string applyFileLineNumbersPreprocess(string& filename) instead" /*Warning message for string preprocessFileLineNumbers(string& filename)*/
#define UEXC003 "function has been deprecated and will be removed in future, Use bool isOutputValid(string output) instead" /*Warning message for bool validOutput(string output)*/

#endif //VBS2FUSION_DEPRECATION_H
