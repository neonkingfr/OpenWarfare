#ifndef __BI_VERTEX__
#define __BI_VERTEX__

#include "BISDKCommon.h"
#include "BIVector.h"
#include "Model\BISpecialVertexFlags.h"

/** \brief Implementiation of vertex */
/** Vertices are notmal 3D cvector s that have special flags. These flags define for exaple type of normal thatis used with them */
class BI_Vertex : public BI_Vector3
{
	typedef BI_Vector3 Base;

	long _flags;
public:
	/** \brief Contructor */
	/** Creates vertex from given coordinates and flags. Default values is set to 0.
	 * For determining, which flag to use, please consult BI_VertexSpecialFlags.h macros
	 * at first, normal is zeroed Vector
	 */
	BI_Vertex(float x =0, float y = 0, float z = 0, int flags = 0): Base(x,y,z), _flags(flags) {}
	
	/** \brief Copy Contructor */
	/** Creates copy of original vertex, coordinates and flags including 
	 */
	BI_Vertex(const Base &vx, int flags=0 ): Base(vx), _flags(flags){}

	/** \brief Copy Contructor */
	/** Creates copy of original vertex, coordinates and flags including 
	 */
	BI_Vertex(const BI_Vertex &vx ): BI_Vector3(vx), _flags(vx._flags){}

	/** \brief  Assign operator */
	/** Copies all from source to destination vector. This is here for conversion sake, as assign operator are not inherited
	 */
	BI_Vertex& operator = ( const BI_Vector3& src) 
	{
		Base::operator=(src);		
		return (*this);
	}

	/** \brief  Assign operator */
	/** Copies all from source to destination vertex. The result is equal 
	 * as performing copy contructor
	 */
	BI_Vertex& operator = ( const BI_Vertex& src) 
	{
		Base::operator=(src);
		_flags = src._flags;
		return (*this);
	}

	bool operator==( const BI_Vertex &v)const
	{
		return Base::operator ==(v) && v.GetFlags() == GetFlags();
	}

	/**\brief Gets flags last set */
	int GetFlags() const { return _flags; }

	/**\brief Sets flags */
	void SetFlags( long flags ){ _flags = flags; }

	/** \brief Checks if the vertex is in halfspace*/
	/**	\returns true if vertex is in direction of normal vector. Normal vector here represent normal vector to some plane
	 * \param normalVector is normal vector of plane in question
	 * \param d is constant defining plane trasition againt centre of the coordinate system
	 */ 
	int VertexInHalfSpace( const BI_Vector3 &normalVector, float d ) const
	{		
		float inside = normalVector.DotProduct(*this) + d;
        if( inside>=0 ) 
			return 1;
        // if the point is too out, fail
        if( inside*inside<normalVector.SquareSize()*1e-6 ) return 1;
		if( inside*inside>normalVector.SquareSize()*1e-6 ) return -1;
        return 0;
	}
};

#endif