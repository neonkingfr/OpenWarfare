#ifndef __BI_MESH__
#define __BI_MESH__

#include "..\BISdkCommon.h"
#include "..\BIVector.h"
#include "..\BIVertex.h"
#include "..\BIError.h"
#include "BISDKReference.h"
#include "BIUVSet.h"
#include "BIModelDef.h"
#include "BIEdges.h"
#include "BISelection.h"
#include "BIFace.h"

/** \addtogroup modelSDK */
/** \{ */

/** \brief Class representing one mesh */
/** Class for manipulating model details.
*/
class BISDK_DLLEXTERN BI_Mesh
{
  friend class BI_SharedMesh;
	friend class BI_Selection;
	friend class BI_Frame;
	friend class BI_Model;

	typedef BI_ManipulationData BI_Internal;

	/// Returns data that this mesh manipulates with
	BI_ManipulationData * GetData()const;
	
	///Constructor
	/** This constructor is used only internally to create presentation class that will handle already loaded/created data */
	BI_Mesh( BI_Internal * data);
	
	/** \brief Creation/Destruction flag */
	/** Flag indicating if data were created by this instance and should be destroyed. Only way that data wil not be _handled by BI_Mesh will be situation, when BI_mesh will be bounded to some model. Model will _handle all its data. Must not be destroyed until this lives ( obvious reasons )
	 */
	bool _handleData; //not used so far, prepared for some _reference, for now always handling data

	/** Actual data to manipulate with */
	BI_Internal * _data;

	friend class BI_Face;

	/** the highest number of points that was assigned. It should lower then deleting from mesh */
	int _lastPoint;

	/** the highest number of normal that was assigned. It should lower then deleting from mesh */
	int _lastNormal;

	/** the highest number of faces that was assigned. It should lower then deleting from mesh */
	int _lastFace;

	//used internally and only at one point.
  int CheckForRepairOrRemove(int i, bool removeInvalid, float dost);

  /** sets data from external source */
  void SetData( BI_ManipulationData*  param1 );
public:

	/** \brief Constructor */
	BI_Mesh();

	/** \brief Construction from BI_Frame */
	/** Create mesh from the frame. This will result in mesh that has some points copie from frame, but no faces nor animations. There is no need to have the frame valid
	 */
	BI_Mesh(const BI_Frame & frame);
	
	/** \brief Constructor */
	/** This constructor will create BI_Mesh from another, but only from point and faces that were selected. \n
	 * Faces are copied, only when they are selected and also all point are selected \n
	 * Copied frames will contain only selected points. \n
	 * All normals will be copied. \n
	 * \param mesh is source mesh to be copied from
	 * \param sel is selection from which mesh should be created. This selection should be synchronized with the source mesh
	 * with mesh to work correctly
	 */
	BI_Mesh(const BI_Mesh& mesh, const BI_Selection &sel );

	/** \brief Copy contructor */
	/** Creates copy of mesh
	 */
	BI_Mesh(const BI_Mesh& mesh);
	
	/** \brief Destructor */
	/** If mesh was bounded to some model, then data allocated by this mesh are not destroyed. So it is safe , for example,
	 * to create pointer to some instance of BI_Mesh, bind it to model and then delete it. 
	 */
	~BI_Mesh();

	/** \name UV Sets */
	/** \{ */

	/** \brief Adds UVSet to BI_Mesh. */
	/** Adds UVSet to BI_Mesh. The number of BI_UVVectors that are in this set should be more or equal to bounded BI_Face located in this Mesh,
	 * If there if less than number of faces, some remaining faces will have acoording UVVector set to zero
	 * it there are more UVVector, they will be ignored 
	 */
	BI_ErrCode DistributeUVSet( const BI_UVSet & set, int index = -1, bool recreate = true );
	
	/** \brief Checks if there was added such UVSet */
	/** This means, that you can safely access UVSets with this id, but it does not assure that on every face is the set defined ( non-zero ) */
	bool IsUVSetEnabled(int uvindex) const;

	/** \brief check nearest uv set enabled */
	/** \retval -1 if no uv set is enabled 
	 *	\retval index of uvset that is enabled and index of greater than index set
	 */
	int GetNextUVSet(int uvindex)const;

	/** \brief Retrieves number ofcurrently enabled  UVSets */
	/** There must be some BI_UVSet defined. Default BI_UVSet, that will be created in contructor, will set all UV coordinates to BI_ZeroVector3
	/*  \returns positive integer 
	 */
	size_t GetUVSetCount() const;

	/** \brief Sets UVset with this id as active */
	/** Active BI_UVSet means that this set will be uset 
	 * \returns BIEC_NoSuchSet if there is no set with given id in mesh. In that case, 
	 * no change is made
	 * \return BIEC_OK if the change to active set was successful
	 */
	BI_ErrCode SetActiveUVSet(int uvsetid);

	/** \brief Retrieves id of set previously declared as active.  */
	/** There has to be always some active set, first with id = 0.
	 */
	int GetActiveUVSet() const;

	/** Deletes BI_UVSet from BI_Mesh */
	/** This operation only disable set. UVSets declared in faces are not destroyed and if not overriden, after enabling this uvset again they will be the same as before.
	 */
	BI_ErrCode DisableUVSet(int id);  

	/** \brief Enables uvset */
	/** UV set with zero id is automatically created. Re-enabling / disabling set with the does not affect 
	 * the uv coordinates set in face, in only indicates that they can be used and saved.\n This operation will always end with success.
	 */
	void EnableUVSet(int id);

	/** \} */

	//--------------------------------------------------------------------------------------------------------------------
	/** \name Vertices */
	/** \{ */
	
	/** \brief Gets number of vertices */
	/** Vertex can be changed only if not selected as 'locked'
	 * \return count of vertices saved in BI_Mesh. 
	 * \note Count of vertices can be also viewed as range of IDs that point can be identified by.
	 */
	int NumberOfPoints() const;

	/** \brief Retrieves or change existing point in mesh */
	BI_Vertex& Point( int i );

	/** \brief Retrieves vertex at the index */
	/** Index must be valid, in range 0 - NumberOfPoints.
	 */
	BI_Vertex Point( int i ) const;

	/** \brief Sets first point that was not set */
	/** If all pointa that were reserved were set, this result in reserving and filling another point. If not, then the point next to the last set point will be set. 
	 * \return index of the point that was set.
	 */
	int SetNextPoint(const BI_Vertex & v);

	/** \brief Sets vertex at index  */
	/** Sets vertex value. This is equal to Point(i) = vx except for the situation when this is first time point is set. SetNextPoint method will skip the next few point that were not set and sets ints index to vx;
	 * \param i index of vertex in internal representation
	 * \param vx vertex data
	 */
	void SetPoint( int i, const BI_Vertex &vx);

	/** \brief Sets vertex at index */
	/** \param index is id of vertex to be set. index must be in valid range
	 * \param x is X-coordinate
	 * \param y is Y-coordinate
	 * \param z is z-coordinate
	 */
	void SetPoint( int index, const float x, const float y, const float z);
	
	/** \brief Reserves a vertex in mesh. */
	/** Vertices have default coordinates at (0,0,0) 
	 * \param count of vertices to reserve
	 * \return index of first vertex. Next vertex has index+1, next index+2, etc.
	 * \note calling this method is not mandatory, but it can avoid time consumption when reallocating array of point to add new one
	 */
	int ReservePoints( int count ); 

	/** \brief Creates a new vertex. */
	/** Creates new point with zero values 
	 * This does not change indexes already set. \n 
	 */
	int AddPoint(const BI_Vertex & v = BI_ZeroVector3);

	/** \brief Removes vertex */
	/** This can result in degenerating some faces
	 * \point id of point that should be deleted
	 * \note Deleting vertex will invalidate all vertex indexes, 
	 * so it can point to another vertices in this mesh. \n
	 */	
	void DeletePoint( int index );

	/** \brief Removes vertex */
	/** This can result in degenerating some faces
	 * \point id of point that should be deleted
	 * \note Deleting vertex will invalidate all vertex indexes, 
	 * so it can point to another vertices in this mesh. \n
	 */	
	void RemovePoint( int index );
	
	/** \brief Replaces point in faces and edges */
	/** Some faces may be invalidated as they become less than triangle.		For degeneration, RepairDegenerated id provided.
	 * the replaced point will not be deleted. No frame changes will be processed.
	 * \param replacing is old index of the point. All occurence will be removed from faces and sharp edges. The point itself will be not removed.
	 * \param replaced id of new index that will be removed and to values of the point will ne possible to acces 
	 */
	void ReplacePoint( int replacing, int replaced );

	/** \} */

	/** \name Normals */
	/** \{ */

	/** \brief Retrieves count of normals located in this mesh */
	int NumberOfNormals() const;

	/** \brief Retrieves normal at position */
	/** \param i ID of normal. This ID is unique in whole BI_Mesh
	 * to get norma, you must be sure that normal is set at the vertex
	 */
	BI_Vector3 & Normal( int vertex ) const;

	/** \brief Recalculates normals */
	/** Recalculates all normals according to the flags set to faces and flags set to vertexes.
	 * if vertex flag is BI_POINT_SPECIAL_LOCKNORMAL, normal at this vertex will not be changed at all
	 * if vertex flag BI_POINT_SPECIAL_ANGLENORMAL is set, then normal will be calculated also according to angle between edges.
	 * \note Since this changes normal located in BI_Mesh, there is no change you could recalculate normals at unbouded faces other than manually.
	 * \note Since this is not changing the vertex, it does not matter if the point is locked 
	 */
	void RecalcNormals();

	/** \brief Creates place for new normal */
	/** \return unique id. Via that id, user can access normal and change it by calling Normal(normalId)
	 */
	int AddNormal(const BI_Vector3 & normal);

	/** \brief Reserves number of normals */
	/** Create space for number of normal specified. It is understood that the number tells how much more 
	 * the space would be needed, if that number is less that number reserved faces, it will be ignored 
	 * \return previous number of normals
	 */
	int ReserveNormals(int need);

	/** \brief Removes normal */
	/** This removes the normal from all faces that is assigned to, decreasing number of normals by one
	*/
	BI_ErrCode RemoveNormal(int index);

	/** \brief Removes normal */
	/** This removes the normal from all faces that is assigned to, decreasing number of normals by one
	*/
	BI_ErrCode DeleteNormal(int index);

	/** \brief Removes unused normals */
	/** Removes normals that are not assigned to any point in any face*/
	void CleanNormals();
	
	/** \} */

	/** \name Faces */
	/** \{ */

  BI_ErrCode ReplaceFace(const BI_Face& f, int index);

	/** \brief Adds BI_Face to BI_Mesh at preferred index */
	/** Adds a copy of face to mesh.
	 * \param face is face to be copied
	 * \retval id of face that was added. 
	 * \retval -1 if there was any problem in adding face
	 */
	int AddFace(const BI_Face & face );
	
 	/** \brief Function test face for degeneration */
	/** 
	* \param points in less distance will be considered as invalid
	* \param removeInvalid \li true faces that cannot be repaired will be ramove
	*					\li false faces that cannot be repaired remain in the state when it is clearly visible that they cannot be repaired
	** \return number of affected/repaired faces 
	*/
	int Repair( float ptDistance, bool removeInvalid);

	/** Remove imvalid entries */
	/** \param ptDistance distance when two points are considered to be the same. Flags will be merged.
	 * \return number of affected rows 
	 */
	int RemoveInvalid( float ptDistance );

	/** \brief Creates a new face in Mesh */
	/** This will create new invalid face in mesh ( similar to BI_Mesh::AddPoint ). This face must be later filled by calling BI_Mesh::AddFace or BI_Mesh::Bind. 
	 */
	int NewFace();

	/** \brief Gets BI_Face located in BI_Mesh */
	/** \param id id of the face in mesh. Must be valid ( face must have been added to mesh before by BI_Mesh::AddFace or BI_Mesh::Bind methods 
	 * \return face located in mesh
	 */
	BI_Face GetFace(int id);

	/** \brief Gets face located in BI_Mesh */
	/** \return face at speficic index at mesh. Index must be valid and face previously added. Since this is constant method, returned face will not be bounded
	 */
	BI_Face GetFace(int i)const;

	/** \brief Removes face from BI_Mesh. */
	/** If there is such a face, it will be removed. All BI_Face that could refer to this facewill be invalidated.
	 * To check validity, BI_Face::IsValid() is provided
	 */
	void RemoveFace( int i );

	/** \brief Gets number of faces in BI_Mesh */
	/** \return Number of faces that are located to this mesh. 
	 This means also the faces that were allocated but not added. To ensure, that number of the faces added is equal to number of added faces, call BI_Mesh::Compact();
	*/
	int NumberOfFaces() const;
	
	/** \brief Reserves faces */
	/** \param count is number of faces that should be reserved in mesh.	\n Must be positive
	 */
	int ReserveFaces( int count ); 

	/** \brief Unlinks face from BI_Mesh */
	/** Removes Face from mesh, but do not destroys it. 
	 */
	BI_Face UnlinkFace(int i);

	/** \} */
//-----------------------------------------------------------------------------------------------------

	/** \name Selections */
	/** \{ */

	/** \brief Saves selection as named selection */
	/** This only map selection to a name 
	 * \param name name of the selection. If selection exists, function will redefine this selection, removing the old.
	   \param sel selection to save. Use NULL to save current selection
	   \retval BIEC_OK if success
	   \retval BIEC_OutOfRange if failed 
	 */
	BI_ErrCode SaveNamedSel( const char *name , const BI_Selection & sel);

	/** Sets selection as current */
	/** \param name name of selection to use
	 * \param mode is behaviour to surrent selection
	 */ 
	void UseNamedSel( const char *name, BI_Selection::BI_SelectionMode mode = BI_Selection::SelectOnly );

	/** \brief Removes named selection */
	/** Removes named selection if exists */
	bool DeleteNamedSel( const char *name ); 

	/** \brief Removes named selection */
	/** Removes named selection if exists */
	bool RemoveNamedSel( const char *name ); 

	/** \brief Renames named selection */
	/** Renames named selection */
	bool RenameNamedSel( const char *name, const char *newName );
	
	/** \brief Uses other named selection */
	/** \param sel selection that will be copied into current selection
	 * \param mode combination mode
	 */
	void UseSelection(const BI_Selection & sel, BI_Selection::BI_SelectionMode mode=BI_Selection::SelectOnly );

	/** \brief get selection name saved in mesh */
	const char * GetSelectionName(int index)const ;

	/** \brief Retrieves named selection by the name 
	 * \retval BIEC_OK if success
	 * \retval BIEC_NoSuchId if failure
	 */
	BI_ErrCode GetNamedSel( int index, BI_Selection & sel ) const;

	/** \brief Retrieves named selection by the name */
	/** \retval BIEC_OK if success
	 * \retval BIEC_NoSuchId if failure
	 */
  BI_ErrCode GetNamedSel( const char *name, BI_Selection & sel ) const;

	/** \brief Retrieves current selection */
	const BI_Selection& Selection() const;

	/** \brief Retrieves current hidden selection */
	const BI_Selection& Hidden() const ;

	/** \brief Retrieves current locked selection */
	const BI_Selection& Locked() const;

	/** \brief Retrieves current selection  */
	BI_Selection& Selection() ;

	/** \brief Makes locked selection current */
	void UseLocked() ;
	
	/** \brief Makes hidden selection current */
	void UseHidden() ;
	
	/** \brief Clears current selection */
	void ClearSelection();
	
	/** \brief Hides current selection */
	void HideSelection();
	
	/** \brief Set selection a current from hide */
	void UnhideSelection();

	/** \brief Locks current selection so that it cant be changed until unlocked */
	/** This will do only from points, faces can change number of vertices etc, but not point location */
	void LockSelection();

	/** \brief Unlocks current selection */
	void UnlockSelection();

	/** \brief Deletes all that selection contains (faces, points ) */
	/** Faces that contain selected point will be removed too */
	bool DeleteSelected(BI_Selection *sel=NULL);

	/** \} */

	/** \name Named properties */
	/** \{ */

	/** \brief Retrieves property text */
	/**	\param name Name of property. Name is note case sensitive . 
	 * \retval NULL, if named property doesn't exists 
	 * \retval char if there is such property
	 */
	const char *GetNamedProp( const char *name ) const; // NULL if not existing

	/** \brief Sets new named property	*/
	/** \param name name of new property. when property exists then it is replaced. Name is not case sensitive
	 * \param value new value of the property
	 * \retval true if success
	 * \retval false if failed
	 */
	bool SetNamedProp( const char *name, const char *value );

	/** Removes named property */
	/** \param name name of the property to remove. Name is not case sensitive
	 * \retval true if success
	 * \retval false if failed
	 */
	bool DeleteNamedProp( const char *name );

	/** Removes named property */
	/** \param name name of the property to remove. Name is not case sensitive
	 * \retval true if success
	 * \retval false if failed
	 */
	bool RemoveNamedProp( const char *name );
	
	/** \brief Renames named property */
	/** \param name name of the property to rename. Name is not case sensitive
	 * \param newname new name of the property.
	 * \retval true success
	 * \retval false failed
	 */
	bool RenameNamedProp( const char *name, const char *newname );

	/** \brief Retrieves name of the property at given index */
	/**	\param i id of the named property
	 *	\return name of property, NULL if index is not used 
	 */
	const char *GetNamedProp( int i ) const;

	/** \brief Retrieves value of the propert at given internal index */
	/**	\retval value of name property 
	 * \retval NULL if index is not used 
	 */
	const char *GetNamedPropValue( int i ) const;

	/** \} */

	/** \name Animation */
	/** \{ */
	/** \brief Retrieves count of animation frames */
	/**	\return count of frames. Value 0 means, that mesh is not animated */
	int NumberOfFrames() const;

	/** \brief Index of frame of closest time */
	/** \param time time that the resulting frame should be nearest to
	 *  \return non-negative id of frame
	 */
	int NearestFrame( float time );

	/** \brief Retrieves frame at given index 
	*/
	BI_Frame GetFrame( int i ) const;

	/** \brief Gets frame at given index */
	BI_Frame& GetFrame( int i );

	/** \brief Converts time to index */
	/** Searches through all animation saved to return the post similat with time
	 * \param time the nearest time we want the animation to approach to
	 * \return first index under which is time as similar as possible to parameter time
	 * return -1 if there is no animation at all
	 */
	int TimeToFrameIndex( float time ) const;

	/** \brief Converts time to nearest index */
	/** \return index whose time is the nearest to the time specified */
	int TimeToFrameIndexNearest( float time ) const;

	/** \brief Adds frame to the animation */
	/** \param src frame to copy to mesh */
	void AddFrame( const BI_Frame &src );

	/** \brief Delete frame by time */
	/** Deletes frame
	 * \retval false if not such time found, 
	 * \retval true otherwise 
	 */
	void DeleteFrame( float time );

	/** \brief Delete frame by time */
	/** Deletes frame
	 * \retval false if not such time found, 
	 * \retval true otherwise 
	 */
	void RemoveFrame( float time );

	/** \brief Delete frame by its index */
	/** Deletes frame. Id of frame must exist,
	 * \param index index to be removed
	*/
	void DeleteFrame( int index );

	/** \brief Delete frame by its index */
	/** Deletes frame. Id of frame must exist,
	 * \param index index to be removed
	*/
	void RemoveFrame( int time );

	/** \brief Removes animation from the mesh */
	/** The mesh will become static */
	void DeleteAllFrames();

	/** \brief Removes animation from the mesh */
	/** The mesh will become static */
	void RemoveAllFrames();

	/** \brief Sorts frames by the time ascending */
	/** Sorts frame. This will not change frame set */
	void SortFrames(); 
	
	/** \brief Retrieves current animation frame */
	/** \retval -1 if mesh is not animated 
	 * \retval id otherwise
	 */
	int CurrentFrameId() const;

	/** \brief Reaload all point location to frame */
	/** This will result in loosing all changes made in this frame and beginning from scratch */
	void ReloadFrame( int i );

	/** \brief Reloads mesh according to frame */
	/** \param frame frame that should be used
	 */
	void UseFrame( const BI_Frame &frame );

  /** \brief Reloads mesh according to Frame selected by id */
  /** Same as the method before, but frame is now selected from internal frames. Frame is determined by it's own id */
  void UseFrame( int id);

  /** \brief Every remove every frame except multiplication of parameter */
  /** This will effectively deacrease reate of animation 
    \param 
  */
  void DecreaseFrameRate(int part);
 
	/** \} */

	/** \name Misc */
	/** \{ */

	/** \brief Merges one mesh to the another */
	/**	\param src source mesh
	 */
	void Merge( const BI_Mesh &src );

	/** \brief Checks if mesh is bounded somewhere */
	/** If it is bounded somewhere, changing the value will immediately affect the model.
	 */
	bool IsBounded()const;

	/** \brief Select faces that is not visible from the point specified */
	/** \param pin is vertex determing the point 
	 * After finishing, all faces that are not in direction of thepoint, will be selected and also points that thay contains
	 */ 
	void BackfaceCull( const BI_Vector3 &pin );

	/** \brief Splits the selection */
	/** Doubles the point that are selected. The other set of point will be unselected */
	void SelectionSplit(BI_Selection &sel);

	/** \brief \brief Triangulate*/
	/**	\param selected means that this will affect all selected faces. If false, all faces will be triangulized */
	void Triangulate( bool selected=false );

	/** \brief Squarize */
	/** \param allFaces makes rectangles from all faces if set to true, otherwise current selection is used */
	void Squarize( bool allFaces=false );
	
	/** \brief Merges points	**/
	/**
	 * \param distlimit how far can be vertices to merge
	 * \retval true if success
	 * \retval false otherwise
	 */
	bool MergePoints( double distlimit );

	/** \brief Merges points **/
	/** \param distlimit vertices in this distance will be merged
	 * \param sel only vertices from this selection will be merged
	 * \retval true if success 
	 * \retval false otherwise 
	 * according to the merged points
	 */
	bool MergePoints( double distlimit, const BI_Selection & sel );
	
	/** \} */

	/** \name Sharp edges */
	/** \{ */

	/** \brief Retrieves sharp edges as Edges class */
	/**	\return edges. By this methos can be changes sharpedges in mesh
	 */
	BI_Edges& SharpEdges();

	/** \brief Sets new sharp edges */
	/** \param edges are edges that should be considered for adding 
	 * \param merge \li true, previous shared edges will be removed \li false, new edged will just be added 
	 */
	void SetEdges( const BI_Edges& edges, bool merge = false );

	/** \name Point mass */
	/** \{ */

	/** \brief Sets point mass */
	void SetPointMass( int i, double mass );

	/** \brief Gets point mass */
	double GetPointMass( int i ) const ;

	/** \brief Set mass according to selection */
	void SetSelectionMass( double mass, bool constTotal=false );

	/** \brief Gets mass according to selection */
	double GetSelectionMass() const;

	/** \brief Gets total mass */
	double GetTotalMass() const;

	/** \brief Gets maximal mass number */
	double GetMaxMass() const;

	/** \brief Gets mass of the centre */
	BI_Vector3 GetMassCentre(bool selection);
	/** \} */

	/** \brief Releases allocated memory that is no longer needed */
	/** The result is not visible, but is releases as much memory as possible to hold all information about BI_Mesh. This is especially useful
	 * when no adding nor deleteting will be further performed
	 * \return ratio of memory that is used against memory used before
	 */
	float Compact();

	/** \brief Computes memory size allocated in whole BI_Mesh */
	/** This do not reflec actual use of memory, that can be more. This method computes just size of memory 
	 * that can be released in runtime
	 * \return number of bytes that this instance occupies 
	 */
	int MemorySize();

	/** \name Operators*/
	/** \{ */
	///Assign operator
	/** Performs deep copy */
	BI_Mesh& operator=(const BI_Mesh & mesh);

  /** \brief Checks if this is same instance */
 bool operator==(const BI_Mesh & mesh)const;

	/** \} */
	/** \name Binding methods */
	/** \{ */
	/** \brief Binds face to Mesh */
	/** Face will be located in this instance of mesh. Any change ( except delete ) will affect also the 
	 * face in BI_Mesh. This method sets flag that face is also bound
	 */
	BI_ErrCode Bind( BI_Face & face);
  /** \brief checks if this mesh contains some points */
  /** this method checks only points, since there is no meaning check faces, all faces will be invalid */
  bool Empty()const;

  void ReplaceFrame( float time, const BI_Frame & frame);
  void ExportAnimation(const char * fileName, BI_Vector3 stepVector);
  void ExportAnimationBinary(const char * fileName, BI_Vector3 stepVector);
  void ExportAnimationText(const char * fileName, BI_Vector3 stepVector);
  int FindCoordSpaces( int * coord, const BI_Frame & firstFrame );
  int NormalizeFrameTimes(bool withLast);
  BI_ErrCode AttachFace( BI_Face & face, int i);
  /** \} */
  /** \name Topology methods */
  /** \{ */

  /**  \brief Checks the topology*/
   /**  Check if the topology is closed. If it is not, point that detects topology leaks will be selected */
    bool CheckClosedTopology();

    /** \brief makes the topology closed */
    /** Method will attempt to close topology. First it finds out which edges make the problem and then creates faces that close it */
    void CloseTopology();
    
    void PromoteComponent( bool *visited, BI_Edges &edges, int point, bool state );
    
    int CalculateInsideFaces( int face, bool onlySelected ) const;
    
    void SelectionToConvexComponents();
    void CheckConvexSelection(int i); // check selection
    
    void RemoveComponents();
    int CreateComponents(const BI_Selection *forbidenFaces=0);
    void CreateConvexComponents();
    void CheckConvexComponents();
    
    bool FaceCanBeInConvexHullPerm(int i, int j, int k );
    void CreateConvexHull( bool allVertices=false );
    void ComponentConvexHull();    

    /** \brief detect sharp edges from the faces */
    /** Sharp edges will be cleared ad new sharp edges added according to faces position. 
     *  Added sharp edge is between two faces that have the edge common and their normals not very same 
     */
    void AutoSharpEdges(float angle=3.14159565/4.0f);

    /** \brief make all selected edges sharp */
    void MakeEdgesSharp(); // make all selected edges sharp.

    /** pointer groups points to array containing smooth group for each face in selection.
     Must be big as number of faces in selection; */
    void ReloadSmoothGroups( bool selfaces );
    
    /** pointer groups points to array allocated for storing groups. 
    Space mush have at least NFaces()*4 bytes.
    */
    void BuildSmoothGroups(unsigned long *groups);

    /** function generates edges from normals
    It compares edge between each two faces and compares its two normals. If 
    it found on one point two or more normals, creates edge. It compares
    direction of normal, not index
    */
    void DetectEdgesFromNormals();

    /** Finds and select nonconvex points and edges
    */
    void CheckConvexity(bool selonly=true);

    void SelectDoubleSided(BI_Selection *inselection);
    int FindFace(const BI_Vector3 * points, int size)const;
    bool IsSelection(const char * name);
    bool GetNextProxy(int& index, const char * name);
    BI_Selection IsolatedPoints()const;
    /** \} */
    /** \brief Builds face graph */
	/** Builds graph, where points are faces, and edges are incidences between faces.
	 * \param result Receives result graph of incidence
	 * \param nocross List of edges in object, that cannot be included into incidence
	 */
    void BuildFaceGraph(BI_Edges &result, const BI_Edges &nocross) const;

    /** \brief checks is all UV coordinated are planar */
    /** this checks for all uvsets at once */
    void CheckMapping();

    /** \brief removes co-linear points */
    /** Removes points that are in one row. Some faces can disappear also */
    void RemoveColinear();

    /** \brief Bounding box of one particula level */
    BI_BoundingBox3 BoundingBox()const ;
};
/** \brief this class is used when there is need to share data between variables */
class BISDK_DLLEXTERN BI_SharedMesh : public BI_Mesh
{
public:
  /** \brief constructor */
  BI_SharedMesh(BI_Mesh & m);

  /** \brief Constructor */
  /** this is an empty constructor. It is similar to the BI_Mesh constructor. */
  BI_SharedMesh();
  
  /** \brief Constructor from another mesh */
  /** this will create shared copy of the mesh */
  BI_SharedMesh(BI_SharedMesh& mesh);
  
  /** creates instance of the BI_Mesh */
  static BI_SharedMesh GetInstance(BI_Mesh& m);
  
  /** \brief operator assigns to the variable shared data */
  BI_SharedMesh operator = (const BI_SharedMesh& mesh);

  /** \brief Destructor */
  virtual ~BI_SharedMesh();
  
  /** \brief releases actual shared data */
  /* released data them may live if they are bounded somewhere or may die if this instance created it */
  void Release();

  /** \brief copies all data */
  void Copy(const BI_Mesh& mesh);
};
/** \} */

#endif