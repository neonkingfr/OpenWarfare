#ifndef ___FLAGS____
#define ___FLAGS____

/** \brief sets "On Surface" face property */
/** This property is used wen model should be stick to the terrain ( like railroad or mud ), but AI should not consider it as road. This must be applied to all vertuces of the model to work correctly */
#define BI_POINT_NORMAL	   0x0
#define BI_POINT_ONLAND    0x1

#define BI_POINT_UNDERLAND 0x2
#define BI_POINT_ABOVELAND 0x4
/** \brief Obsolete. Used for model to have the have slope as the terrain */
/** mostly used for fences and similar object that need to have height according to the map terrain. The are skewed accordingly by the engine. Howeverer, this was replaced by the named property "placement"*/
#define BI_POINT_KEEPLAND  0x8
#define BI_POINT_LAND_MASK 0xf

#define BI_POINT_DECAL      0x100
#define BI_POINT_VDECAL     0x200
#define BI_POINT_DECAL_MASK 0x300

#define BI_POINT_LIGHT_NORMAL 0x0
#define BI_POINT_NOLIGHT    0x10 // active colors
#define BI_POINT_AMBIENT    0x20
#define BI_POINT_FULLLIGHT  0x40
#define BI_POINT_HALFLIGHT  0x80
#define BI_POINT_LIGHT_MASK 0xf0

#define BI_POINT_NORMAL_FOG	  0x0
#define BI_POINT_NO_FOG       0x1000 // active colors
#define BI_POINT_SKY_FOG      0x2000
#define BI_POINT_FOG_MASK     0x3000

#define BI_POINT_USER_MASK  0xff0000
#define BI_POINT_USER_STEP  0x010000

#define BI_POINT_DECAL_NORMAL	0x0
#define BI_POINT_DECAL_DECAL   0x100
#define BI_POINT_VDECAL		0x200
#define BI_POINT_DECAL_MASK	0x300

#define BI_POINT_USER_MASK  0xff0000
#define BI_POINT_USER_STEP  0x010000

#define BI_POINT_SPECIAL_HIDDEN 0x1000000
#define BI_POINT_HIDDEN_TRUE	 0x1000000
#define BI_POINT_HIDDEN_FALSE	 0x0

#define BI_POINT_SPECIAL_DIMNORMAL 0x0
#define BI_POINT_SPECIAL_LOCKNORMAL 0x2000000
#define BI_POINT_SPECIAL_ANGLENORMAL 0x4000000

#define BI_POINT_SPECIAL_MASK   0xf000000
#define BI_POINT_SPECIAL_HIDDEN 0x1000000
#define BI_POINT_SPECIAL_LOCKNORMAL 0x2000000 //Objectiv normal calculation - used only for smoothed vertices
#define BI_POINT_SPECIAL_ANGLENORMAL 0x4000000 //Objectiv normal calculation - normal's at this point are adjusted by angle, not face size

#endif