#ifndef __SDK_REFERENCE__
#define __SDK_REFERENCE__

/* forward declarations */
#include "../BISdkCommon.h"

/// \cond  
class BI_ManipulationData;

class BI_InternalLODObject;
class BI_InternalNamedProperty;
class BI_InternalFace;
/// \endcond

#endif 