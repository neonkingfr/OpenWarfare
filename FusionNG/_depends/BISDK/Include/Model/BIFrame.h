#ifndef __BI_Frame__
#define __BI_Frame__

#include "..\BISDKCommon.h"
#include "BISDKReference.h"
#include "..\BIError.h"
#include "..\BIVector.h"
#include "..\BIFile.h"

class BI_Mesh;

/** \addtogroup modelSDK */
/** \{ */
/** \brief Class representing animation */
/** this class is used to define point povement. Therefore there must be some BI_Mesh that this clas is referring to,
 * Deletion nor insertion of point are not supported, as number of points should always be correspong to number of 
 * points in BI_Mesh. BI_Frame is created in similar way as other object, with reference to mesh. However, it cannot be bound to mesh
 * just added a copy. \n
 */
class BISDK_DLLEXTERN BI_Frame
{
	friend class BI_ManipulationData;

	///Contructor for inner manipulation
	BI_Frame(BI_ManipulationData * ref);

	/** \brief Removes point from frame */
	void RemovePoint(int index);

	/** \brief Removes point from frame */
	void DeletePoint(int index);

	/** \brief Saved data to stream */
	/** Saves data to stream. As complementary function can be used load */
	void Save(BI_File& file);

	/** \brief Loads content to Frame */
	/** This do not affect BI_Mesh _reference */
    void Load( BI_File& file );
	
private:
	/** Manipulation data that object is referring to */
	BI_ManipulationData * _reference; 

	/** Time of animation */
	float _time;

	/** Location of point. For right functioning, size of this should be the size of mesh that this frame is referring to */
	BI_Vector3 * _points;

	/** how much was there allocated.This size is here for cheking if there was any change to location of point in BI_Mesh */
	int _size;

public:
	/** \brief Contructor */
	/** Creates empty frame that is not bounded to any mesh. It is there for sake of containers that need empty contructor. This can be used, but 
	no Synchronization, Reload, Save or Load methos could be called */
	BI_Frame ();
	
	/** \brief Contructor */
	/** This should be contructor that should be most commonly used.
	 * \param parent is BIMesh that BI_Frame should synchronize with. 
	 * However to really add BI_Frame to Mesh, BI_Mesh::AddFrame(frame) must be called
	 */
	BI_Frame (const BI_Mesh& parent);

	/** \brief Copy contructor */
	/** Creates an BI_Frame that has the same points location as source */
	BI_Frame (const BI_Frame & frame);

	/** \brief Sets time */
	/** \param time float used to determine which animation should go first. The lower is the number, the sooner will be animation active. 
	 * Time has no upper nor lower boundary.
	 */
	void SetTime( float time );

	/** \brief Gets time */
	/** \return time set */
	float GetTime() const ;

	/** \brief Checks if frame can be safely used */
	/** For frame to be valid it means that it has to have a valid reference mesh and must be sychronized. To check, if the frame has 
	 * valid reference run synchronize. If it does not change the validness, nothing else will.
	 */
	bool IsValid();

	/** \brief Synchronizes point location according to BI_Mesh that was set */
	/** This is called every time reference mesh should change, to ensure that points locations are valid. 
	 * User should call this everytime when point in reffering mesh are likely to change. There is no need to call this
	 * for frames that are copied in BI_Mesh.
	 * \note Synchronize is called also when saving. That means that any change affecting number of points like delete point manually 
	 * called by user, will be discarded if this frame is bounded in BI_Mesh
	 */
    void Synchronize();

	/** \brief Computes all memory allocated */
	/** Computes just memory allocated dynamically, that can be released. Full memory allocated can 
	 * be achieved by 
	 * \code 
	 * sizeof(BI_Frame) + GetMemorySize();
	 * \endcode
	 */
	int GetMemorySize();

	/** \brief Clear point location */
	/** Sets all point location to zero. Time will remain untouched. 
	 */
    void Clear();

	/** \brief Resets Frame acordin to parent mesh */
	/** Synchronizes with parent and sets all point that mesh has allocated as animation base */
	void Reload();

	/** \brief Clears one point */
	/** Sets specified point location to zero vector. 
	 * \retval BIEC_OutOfRange when no such location
	 * \retval BIEC_OK otherwise
	 */
    BI_ErrCode Clear( int i );

	/** \brief Permutes points location. */
	/** \param permutation is arbitrarily huge array that ends with -1.
	 */
    void PermuteVertices( const int * permutation );

	/** \brief Number of points */
	/** This should be corresponding to number of points in referring mesh. For this to ensure, BI_Frame::Synchronize have to be called.
	 * \note frame in mesh will always be sychronized. However, saved frame previously fetch from mesh is just copy and so does not reflect the changes of mesh */
    int NumberOfPoints() const;    
	
	/** \brief Compares two frames */
	/** Compares all point located here */
	int Compare( const BI_Frame & p );

	/** \name Operators */
	/** \{ */
	/** \brief Assigment operator */
	/** Copies all point location. Since time should not be the same, setTime is expected to be called soon. 
	 * Otherwise order of the two frames with the same time is not defined
	 */
	BI_Frame& operator=(const BI_Frame & src);

	/** \brief Access operator */
	BI_Vector3& operator[](int i);

	/** \brief Access operator for constant object */
	BI_Vector3 operator[](int i)const;

	/* \} */
};
/** \} */
#endif