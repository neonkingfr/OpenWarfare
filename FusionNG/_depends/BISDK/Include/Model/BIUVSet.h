#ifndef __BI_UVSET__
#define __BI_UVSET__

#include "..\BISDKCommon.h"
#include "BISDKReference.h"
#include "..\BIError.h"
#include "..\BIVector.h"
#include "BIModelDef.h"

class BI_Face;

/** \addtogroup modelSDK */
/** \{ */

/** \brief Struct to hold UVSets in face for one ID */
/** It is there for better manipulations with faces. Since it is only container, 
 * there is no other data than operator method. At first the face will bef fille with zerovectors
 * Since we do not hold the size of polygon in this tsructire, you have to remeber it in other variable 
 * or refer to some BI_UVset that should the instance belong to
 */
class BISDK_DLLEXTERN BI_FaceSet 
{
	/** Actual UVVectors in face */
	BI_UVVector _vectors[BI_DATA_MAX_POLY];

public:
	/** \brief Constructor */
	BI_FaceSet();

	/** \brief CopyContructor */
	BI_FaceSet(const BI_FaceSet & set);

	/** \brief Clears mapping */
	/** Sets Uvv cordinates to zero */
	void Clear ();

	/** \brief Checks if there is any mapping */
	/** \return true if face containt all zero vectors. That indicated that face was cleared and not mapped again */
	bool Empty();

	/** \name Operators */
	/** \{ */
	/** \brief Access operator */
	/** Allows to change the vector stored in here 
	 * \param vertex is vertex id in face
	 * \return BI_UVector that is located at this position
	 * \note this will not check check validness of access
	 */
	BI_UVVector& operator[](int vertex );

	/** \brief Access operator for constant object */
	/** Method to be used just for reading 
	 * \param vertex is vertex id in face
	 * \return BI_UVector that is located at this position
	 * \note this will not check check validness of access
	 */
	BI_UVVector operator[](int vertex )const;

	/** \brief Equality operator */
	/** Compares all the uvvectors that can be located in face
	 * \return true if every two corrresponding vertices have same value 
	 * \return false otherwise
	 */
	bool operator==(const BI_FaceSet & face)const;

	/** \brief Inequality operator */
	/** Compares all the uvvectors that can be located in face
	 * \return true if every two corrresponding vertices have same value 
	 * \return false otherwise
	 */
	bool operator!=(const BI_FaceSet & face)const;
	/** \} */
};


/** \brief Class handling BI_UVectors assigned to several vertexes */
/** This is container that manages several uvsets. This describes all uv sets made for one face. 
 */
class BISDK_DLLEXTERN BI_UVSet
{
private:
	/* Pointer to owner. This is used during validation, copying, comparing and size computing */
	BI_InternalFace * _owner; //should depend on context....some macro?like 
	
	/** \brief Size of allocated BI_UVVectors */
	/** This is which is maximum index we've set so far. No space is disallocated until explicitely said. This is said by compact method */
	int _size;

	/* Set of UVVectors that can single vertex access. These can be different for every 
	 * pair <face, vertex_on_face> */
	BI_FaceSet * _uvVectors; 

	//user should not see this as we do not know why to use it. If it would to be somewhe, only mesh should use it
    //int SetIndex(int index);
	
	/** \brief Reallocs space  */
	/** Reallocs number of allocated UVVector according to size */
	BI_ErrCode Realloc(int newSize);
/// \cond
	friend BI_InternalFace;
/// \endcond

	/** \brief Constructor */
	/* This constructor can be used just by owner sets where this set id located. Should not be called no no one else butFace */
	BI_UVSet(BI_InternalFace * owner);

public:
	/** \brief Contructor */
	/** This creates BI_UVSet that has no owner. This set can be used as container to BI_UVVectors for later reference. It is not valid, though.
	 * \param i initializes number of uvsets to be used. Number of UV sets declared is possible via BI_UVSet::Capacity() method. 
	 * \note when initialized with no parent, BI_UVSet::Compact() method has no effect, since every set is considered to be declared.
	 */
	BI_UVSet(int i = 1);

	/**\brief Destructor */
	/** Completely releases all the data that were allocated
	 */
	~BI_UVSet();

	/** \brief Swaps two sets */ //if it specially needed by user. User will not be forced to call this 
	/** Swaps two sets. no set is destroyed. it is only changed coordinted accordin to the ther set
	 */
	void SwapSets(int i, int j);

	/** \brief Swaps vertices in sets */
	/** Swaps UV coordinates in sets */
	void SwapVertices(int i, int j);

	/** \brief Actual size of set */
	/** Number of slots that can be used ( it means that up to this number no further reallocation will be performed )
	 * If called Compact before Capacity, the return value represent also number of defined UV sets. 
	 * \returns range where it is safe to use operator []
	 */
	int Capacity() const;

	/** \brief Destroys all data allocated here */
	/** This will set all uv sets that were declared here to empty ( contating only zero values ). However, it will not destroy the allocated place nad changes co capacity */
	void Clear();

	/** \brief Computes memory usage */
	/** Computes memory allocated by this structure */
	int GetMemorySize()const;

	/** \brief Specifies complete uvCoordinates for one face */
	/** \param setid  id of the set 
	 *  \param set set of uv coordinated that should be used in the parent face when requesting set setid
	 *  \param add if true, faceset will be added nevertheless setid is enabled or not. If false, uv set will 
	 *  changed only is this particulat set is enabled
	 *  \retval BIEC_OutOfMemory it cannot allocate memory for newly added BI_UVVectors
	 *	\retval BIEC_NoSuchID if attempting to set ID ahich does not exists. Add must be false
	 */
	BI_ErrCode SetUVSet(int setid, const BI_FaceSet& set, bool add = false);
	
	/** \brief Fetch uvvector according to uvset and  vertex */
	/**	\param uvsetid uvset to be used
		\param vs id of vertex in question. This is is  the one that mesh handles
		\returns BI_UVVector according to UV in this set
	*/
	BI_UVVector GetUV(int uvsetid, int vs ) const;
	
	/** \brief If possible, decrease size of memory used */
	/** This deallocated so far unused space. This can also invalidate id of sets, 
	 * so this should be called very carefully.
	 * \param necessary is map of uvsets that are active(index set to true) and so they should not be destroyed
	 * \param size is size of the necessary map, since as it is bool we do not have other way to determine end of set
	 * \note this should not be used when BI_UVSet is handled by BI_Mesh. Changing size externally could lead to errors. 
	 */
	BI_ErrCode Compact(const bool * necessary, int size);

	/* \name Operators */
	/** \{ */
	/** \brief Equality operator */
	/** This compares every set that was declared here
	 */
	bool operator==(const BI_UVSet& src)const;

	/** \brief Inequality operator */
	/** implenented as opposite to operator==*/
	bool operator!=(const BI_UVSet& src);

	/** \brief Assign operator */
	/** Creates deep copy of all UV sets already allocated
	 */
	BI_UVSet& operator=(const BI_UVSet& src);

	/** \brief Access operator */
	/** \return FaceSet that represent array of uv coordinates for each vertex in face.
	 * \param uvsetid is ID number of set to be used
	 */
	BI_FaceSet& operator[](int uvsetid);

	/** \brief Access operator for constant object  */
	/** \param uvsetid is ID number of vertex, it must be between 0 and BI_DATA_MAX_POLY
	 * \return FaceSet that represent array of uv coordinates for each vertex in face.
	 */
	BI_FaceSet operator[](int uvsetid)const;
	/** \} */
};
/** \} */
#endif