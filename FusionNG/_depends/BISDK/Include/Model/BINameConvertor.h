#ifndef __CNAMECONVERTOR__
#define __CNAMECONVERTOR__

#include "BISDKCommon.h"

//------------------------------------------CONVERTOR-------------------------------

class BISDK_DLLEXTERN BI_NameConvertor
{
public:
  static const char* GetPointSurfaceStr(unsigned long flag);
  static unsigned long GetPointSurfaceInt(const char* flag);
  static const char * GetLodNameFromResolution(double resolution);
  static float GetLodResolutionFromName(const char *name);
  static const char* GetPointLightingStr(unsigned long flag);
  static int GetPointLightingInt(const char* flag);
  static const char* GetPointFogStr(unsigned long flag);
  static unsigned long GetPointFogInt(const char* flag);
  static const char* GetPointHiddenStr(unsigned long flag);
  static unsigned long GetPointHiddenInt(const char* flag);
  static const char* GetPointNormalStr(unsigned long flag);
  static unsigned long GetPointNormalInt(const char* flag);
  static const char* GetPointDecalStr(unsigned long flag);
  static unsigned long GetPointDecalInt(const char* flag);
  static const char* GetFaceLightStr(int flag);
	static int GetFaceLightInt(const char* flag);
	static const char* GetZBiasStr(int flag);
	static int GetZBiasInt(const char* flag);
	static const char* GetEnShadowStr(int flag);
	static int GetFaceShadowInt(const char* flag);
	static const char* GetTexMergingStr(int flag);
	static int GetTexMergeInt(const char* flag);
};

#endif //_CLODNAMES