#ifndef __BI_FACE__
#define __BI_FACE__

#include "..\BISDKCommon.h"
#include "BISDKReference.h"
#include "..\BIVector.h"
#include "..\BIVertex.h"
#include "BIEdges.h"
#include "BIUVSet.h"
#include "BIFrame.h"
#include "BISpecialLods.h"

class BI_Mesh;

/** \addtogroup modelSDK */
/** \{ */
/** \brief Class referring to some Mesh */
/** Every face is referring to some mesh. When creating face, it is unbound, meaning no changes will take 
 * effect on mesh it refers to, face will be not saves ot other influenced its mesh
 */
class BISDK_DLLEXTERN BI_Face
{
	friend class BI_UVSet;
	friend class BI_Mesh;
  friend class BI_ManipulationData;

	typedef  BI_InternalFace BI_InternalData;
  void SetData( BI_InternalFace * f );

	BI_InternalData * _face; //face that si bound somewhere. If it is not bound, it's unbound face

	/* creation of internal data */
	bool _handleData;
	bool _propagate;
	int _lastPoint;

	BI_Face( BI_InternalData * data );
  const BI_InternalFace * Data()const;

public:

	/** \brief Contructor */
	/** Constructs empty face. This is here for sake of containers like vector. It is invalid and should be used only as template face, lated to be added to mesh, binded or filled */
	BI_Face();

	/** \brief Copy contructor */
	/** Creates copy of a face. Created face will be become unbouded unless created when retrieving from mesh
	 */
	BI_Face(const BI_Face &other);

	/** \brief Contructor */
	/** Contructs unbouded face with reference at data hold by mesh. For face to be bounded to mesh, BI_Mesh::Bind is provided. To save a copy in mesh, BI_Mesh::AddFace is provided */
	BI_Face( const BI_Mesh & mesh);

	/** \brief Destructor */
	~BI_Face();

	/** \brief Resets the face to the state when it was created.*/
	/** It enables posibility to use BI_Face::SetNextPoint from the 0 index
	 * This does not affect anything concerning bounding. This does not affect uvsets. ( no destroying nor deallocating );
	 */
	void Reset();

	/** \brief Checks if the face is bounded */
	/** When face is bounded, every change that is made to the 
	  * instance is propagated to the mesh the face is bounded to.
	  * \retval true if face is located in some mesh 
	  * \retval false otherwise
	  */
	bool IsBounded()const;

	/** \brief Gets face index */
	/** \retval index which is non-negative. Instance of the face can be accessed via this index
		\retval -1 if the face is not bounded to any mesh or was unlinked from the mesh.
	 */
	int GetIndex()const;

	/** \name  State information */
	/** \{ */

	/** \brief Returns char interpretation of texture */ //possible textures are?
	/** Texture name will consist of full path to the texture, extension included */
	const char *GetTexture()const;

	/** \brief Gets material name */ //possible material are?
	/** Material name can be set by absolute or relative path */
	const char *GetMaterial()const;

	/** \brief Returns vertex position defined by face vertex */
	/** \param vs must be between 0 - BI_MAX_DATA_POLY 
	 * \return vertex information
	 */
	BI_Vertex GetVertex(int vs) const;

	/** \brief Returns normal vector defined at vertex in position vs */
	/** Return value of normal vector assigned to vertex 
	 * \param vs index of vertex. Must be between 0 - BI_DATA_MAX_POLY
	 * \note when calling this, be sure that normal was calculated or set manually 
	 */
	BI_Vector3 GetNormalVector(int vs) const;

	/** \} */

	/** \name Direct manipulating with face  */
	/** \{ */

	/** \brief Sets texture */
	/** \param name name of the texture. The texture name should be simple ascii, containing only letters, digits and underscore (no letters with diacritics). Only one dot is allowed leading the extension. There is no checking, if it is so. If not, generated o3d fle will be invalid.
	 */
	void SetTexture(const char *name);

	/** \brief Sets material od the face */
	/** \param name of the material. Material must be in rvmat format.
	 */
	void SetMaterial(const char *name); 

	/** \brief Sets number of vertices in face */
	/** When setting point, number of vertices is automatically 
	 * adjusted to the highest number of vertex set. 
	 * \param n number of vertices creating face
	 */
	void SetVertexCount(int n);

	/** \brief Declared point in face point to face */
	/** Since current engine doues not support more than BI_DATA_MAX_POLY vertices, vs parameter should be in this range. 
	 * If it is not, it will be truncated to legal id. Number of vertices will be automaticaly increased according to the point number given 
	 * ( if four, the face will be quad, if two, for example, it will have two points set and so it will be marked as invalid. If the number of vertices 
	 * was set and vs is less than this number, no vertex count adjustment will happen. If there is need to change number of vertices, there can be called 
	 * BI_Face::Reset() and then fill the point from the beginning, or explicitely call BI_Face::SetVertexCount(int n);
	 * \param pt is valid point id gained from mesh. 
	 * \param vs is id of face point
	 */
	void SetPoint(int vs, int pt);

	/** \brief Increases number of point by this vertex */
	/** Sets the next fac vertex that was not assigned yes. if BI_MAX_DATA points were set, it points to 0 vertex
	 * \param point is index of point that should be face vertex at position  on higher than last 
	 * \param normal is normal that corresponds to this vertex. If the normal won't be set, this is indicated by -1.    * \return if of face point that was set
	 */
	int SetNextPoint(int point, int normal = -1 );

	/** \brief Sets normal at vertex */
	void SetNormal(int vs, int nr);

	/** \brief Change U item in UVVector located in face */
	void SetU(int vs, float tu, int uvid=-1);

	/** \brief Change V item in UVVector located in face */
	void SetV(int vs, float tv, int uvid=-1);

	/** \brief Set UVvector in face */
	void SetUV(int vs, float tu, float tv, int uvid=-1);    

	/** \brief Sets UV coordinated for each vertex set in face */
	/** Sets all coordinated in all vertices that make the face */
	bool AddUVSet(int uvsetid, const BI_FaceSet & set);

	/** \brief gets uvset for all vertices */
	/** \retval true if there was such an uvset
	 *	\retval false otherwise
	 */
	bool GetUVSet(int uvsetid, BI_FaceSet & set);

	/** \brief Sets flags */ 
	/* Possible flags are macros that can be foung in \link BISpecialVertexFlags.h \endlink */
	void SetFlags(unsigned long flags);

	/** \} */

	/** \brief Gets number of vertices */
	int VertexCount() const;

	/** \brief Gets flags of face */
	/** \return integer describing set flags */
	unsigned long GetFlags();
	
	/** \brief Gets flags previously assigned to face */
	unsigned long GetFlags() const ;

	/** \brief Gets index of point in mesh */
	int GetPoint(int vs) const ;

	/** \brief Gets normal located at vertex with index vs */
	int GetNormal(int vs) const ;

	/** \brief Gets U coordinate in BI_UVset with id uvid at vertex vs */
	float GetU(int vs,  int uvid=-1) const;

	/** \brief Gets U coordinate in BI_UVset with id uvid at vertex vs */
	float GetV(int vs, int uvid =-1) const;

	/** \brief Gets UV vector of coordinates in BI_UVset with id uvid at vertex vs */
	/** \param vs vertex in question 
	 * \param uvid index of uvset. If set to -1, uvset used in referring bie_mesh will be used.
	*/
	BI_UVVector GetUV(int vs, int uvid = -1) const;

	/** \brief Checks if point belongs to this face. */
	int ContainsPoint( int vertex ) const;

	/** \brief Checks whether there is a common edge */
	/** \param face a face that is adept to neighborough 
	 *	\return true if face has common edge 
	*/
	bool IsNeighbourgh( const BI_Face &face ) const;

	/** \brief Checks if the face is valid */
	/** if the face is invalid, it is not possible to operate with it safely. This can usually happen when data this face is referring to, 
	 * were unlinked or destroyed. If the data were destroyed, this can result in accesing wrong memory acces.
	 */
	bool IsValid() const ;

	/** \brief Checks if face is contains*/
	/** \param v1 beginning point of edge 
	 * \param v2 ending point of edge
   * \retval 0 if it does not contains the edge
   * \retval 1 if it contains the  edge
   * \retval -1 if it contains the edge but in opposite direction
	 */
	int ContainsEdge( int v1, int v2 ) const;

/** \brief Getts point thet created common edge */
/** If no edge is common ,valued in these v1, v2 will remain unchanged*/
  bool GetCommonEdge(const BI_Face & face, int & v1, int & v2);

	/** \brief Check if there is an edge common to both faces */
	/**  Fills the parameter with indexes of vertices in both faces */
	bool GetEdgeIndices(const BI_Face &other, int &i1a,int &i1b, int &i2a, int& i2b, bool depend) const;

	/** \brief Checks if vertex */
	/** \retval true if there is any vertex that has normal with this id
	 *	\retval false otherwise
	 */
	bool ContainsNormal( int vertex ) const;

	/** \brief Calculates average normal */
	/** Normals will be calculated according actual location set in mesh */
	BI_Vector3 CalculateNormal() const;

	/** \brief Calculates normal in concrete frame */
	/** Normals will be calculated according to location set in frame
	*/
	BI_Vector3 CalculateNormal( const BI_Frame & f ) const;

	/** \brief Calculates normal an situates it at position of point with index x */
	BI_Vector3 CalculateNormalAtPointAdjusted(int vs) const;

	/** \brief Calculates area of face */
	/** \return total area of the face. This are is maximal area that this face can achieve. To check if this really correspond with reality, 
	 * check perimeter or perform BI_Face::AutoUncross. If the perimeter is the highest, face is uncrossed and so the area corresponds 
	 * with calculated value.
	 */
	double CalculateArea() const;

	/** \brief Perimeter of face */
	/** \return perimeter */
	double CalculatePerimeter() const;

	/** \brief Checks if the face is convex */
	/** for face to be convex, it must be also planar. If it is not planar, there exists axis such that projection 
	 * on it would result in unconvex polygon 
	 */
	bool IsConvex() const;
	
	/** \brief Checks if face is planar */
	bool IsPlanar() const;

	/** \brief Checks if face is in space generated by normal vector of this space */
	bool FaceInHalfSpace ( const BI_Face &face ) const; 

	/** \brief Crosses the face */
	/** Swaps two adjanced points */
	void Cross();

	/** \brief Sets face to uncrossed state */
	void Uncross(  );

	/** \brief Reverse face */
	/** Changes orientation of all edged in face */
	void Reverse();

	/** \brief Check the position between faces */
	/** The result is to ballance the number of neighborough faces that have oposite anf the same sign of normal */
	void AutoReverse(  );

	/** \brief Gets location of face relative to second face. 
	/* \retval Above if face is above the other 
		\retval AboveBoth if each face considers the other as above
		\retval Below if face is below the other
		\retval BelowBoth if each face considers the other as below
		\retval Cross if face crossed each other 
	* \see FaceLocations for further information
	*/
	BI_FaceToFaceLocation FaceLocation(const BI_Face &other) const;

	///Function tests edge between the faces for mapping
	/**
	 * \param other Other face.
	 * \retval true, if both faces shares U,V coordinates, so mapping continues from one face to
	 * other. 
	 * \retval false, if face has different mapping on edge, or if faces has no common edge.
	 * \retval true otherwise
	 */
	bool HasSameMappingOnEdge(BI_Face &other);

	/** check if the two faces have common mapping */
	/** The method checks if the mapping is similar enough and if so, removes differences between them */
	bool ContinueMappingOnEdge(BI_Face &other);

	/** \brief Makes a valid face if possible */ 
	/** If face was not valid, this method will swap and remove the point to create a valid face
	 *	\retval true if operation was succesfull ans face is again valid
	 *	\retval false otherwise 
	 */
	bool RepairDegenerated();

	/** \brief Gets used UVMap */ 
	/** information about actual map is held in referred mesh. When changing it there, the change will propagate to all faces 
	 * \return non-negative number denoting the id of the set used 
	 */
	int GetActive() const;

	/** \brief Checks mapping */
	/** \return true, if face is mapped */
	bool HasSomeMapping() const;

	/** \brief Clers any mapping that this face could use */
	/** Clears only mapping that is active in referring mesh. Other mapping will remain untouched */
	void ClearMapping();

	/** \brief Detection, whether one face overlaps another in UV space */
	/** \retval true, there is collision
	 * \retval false if no collision
	 * \note points on border of the face are considered as non-colided
	*/
	bool IsUVCollision(const BI_Face &other) const;

	/** \brief Checks UV coordinates */
	/** \retval true when uv coordinate is inside of face in current uvset 
	 *  \retval false otherwise
	 */
	bool IsUVCoordInside(const BI_UVVector &uv) const;

	/** \brief Calculates angle at the point. */
	/**	Angle is counted around normal axe. It uses right hand rule rotation.
	 * \return angle in radian
	 */
	float CalcAngleAtPoint(int vs) const;

	/** \name Operators */
	/** \{ */
	/* \brief Assign operator */
	/** Copies face data, destructs every value that was in this BI_Face.
	 * As this instance copies only data, it became unbounded, its referencing mesh will be changed to other's face referencing mesh.
	 */
	BI_Face& operator=(const BI_Face &other);
	/** \} */
};
/** \} */
#endif