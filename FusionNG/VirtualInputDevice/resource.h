//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by VirtualInputDevice.rc
//
#define IDD_VID_WINDOW                  101
#define ID_A                            1001
#define ID_B                            1002
#define IDC_X_ASIX                      1003
#define IDC_Y_AXIS                      1004
#define IDC_Y_AXIS2                     1004

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        102
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1005
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
