#pragma once


namespace TalonViewerLib
{
	/* Euler sequences */
	enum eEulerOrder
	{
		EULER_ZYX=1,
		EULER_XYZ=2,
		EULER_YXZ=3,
		EULER_YZX=4,
		EULER_ZXY=5,
		EULER_XZY=6
	};
}