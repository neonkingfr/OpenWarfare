//******************************************************************************************************
//******************************************************************************************************
//
// Filename:	MACVector4.h
// Author:		Ian Elsley
// Created:		26th Feb, 2001
//
//
// Documentation:
//		MACVector4 class and templates for the Solver Interface. This library is the prototye for the
//		maths library in general.
//
//		MACVector4 has allied functions:
//
//			T				dot		(const MACVector4<T>& v1, const MACVector4<T>& v2);
//			MACVector4<T>	cross	(const MACVector4<T>& v1, const MACVector4<T>& v2)
//
//
//
// Log:
//
// Notes:
//
//		There should perhaps be failure with asserts on access to non existant members
//
// Copyright (c) 2001 Motion Analysis Corportation.
// All rights reserved.
//
//
// This copyright needs updating, but this code is internal to MAC. Do not use, copy, look at or breathe.
//
//******************************************************************************************************
//******************************************************************************************************

#ifndef __MAC_VECTOR4_H__
#define __MAC_VECTOR4_H__

#include <iostream>

MAC_MATH_LIB_BEGIN


//***********************************************************************
//***********************************************************************
//
// Instanciated Vectors
//
// Notes:
// Log:
//		Created 28/2/01, Ian Elsley
//
typedef	MACVector4<double>		MACVec4d;
typedef MACVector4<float>		MACVec4f;
typedef MACVector4<int>			MACVec4i;
//
// End of instanciation
//
//***********************************************************************
//***********************************************************************


//***********************************************************************
//***********************************************************************
//
// Templated Class Definitions
//



//
// MACVector4
//
// Notes:
// Log:
//		Created 2/27/01, Ian Elsley
//
template<class T>
class MACVector4 : public MACVector<T>
{

	private:
	
		
		T		x;
		T		y;
		T		z;
		T		w;


	public:		/*** Constructors ***/


		MACVector4<T>	()										: x((T)0), y((T)0), z((T)0), w((T)0) {}
		MACVector4<T>	(T v)									: x(v), y(v), z(v), w(v) {}
		MACVector4<T>	(T vx, T vy, T vz, T vw)				: x(vx), y(vy), z(vz), w(vw) {}
		MACVector4<T>	(T vx, T vy, T vz)						: x(vx), y(vy), z(vz), w((T)0) {}
		MACVector4<T>	(const MACVector<T>& v)					: x(v.X()), y(v.Y()), z(v.Z()), w(v.W()) {}

		virtual ~MACVector4<T>	()								{}

	
	public:		/*** Access - Set and Get ***/

		
		// Standard array access
		virtual T&			operator[]	(const unsigned int i)			{ return (&x)[i]; }
		virtual const T&	operator[]	(const unsigned int i) const	{ return (&x)[i]; }

	
		// Get the values
		virtual T	X()	const									{ return x; }
		virtual T	Y()	const									{ return y; }
		virtual T	Z()	const									{ return z; }	
		virtual T	W()	const									{ return w; }	


		// Set the values
		T			X(T v)										{ return x=v; }
		T			Y(T v)										{ return y=v; }
		T			Z(T v)										{ return z=v; }
		T			W(T v)										{ return w=v; }


		// Set the values in a group
		MACVector4<T>&	set	(const T& v)						{ x=v; y=v; z=v; w=v; return *this; }
		MACVector4<T>&	set	(const T& vx, const T& vy, const T& vz, const T&vw)	
																{ x=vx; y=vy; z=vz; w=vw; return *this; }
		MACVector4<T>&	set	(const MACVector<T>& v)				{ x=v.X(); y=v.Y(); z=v.Z(); w=v.W(); return *this; }


	public:		/*** General Arithmetic operators ***/

	
		// Assignment
		MACVector4<T>&	operator=	(const MACVector<T>& v)		{ return set(v); }
		MACVector4<T>&	operator=	(const T v)					{ return set(v); }
		MACVector4<T>&	operator+=	(const MACVector<T>& v);
		MACVector4<T>&	operator+=	(const T v);
		MACVector4<T>&	operator-=	(const MACVector<T>& v);
		MACVector4<T>&	operator-=	(const T v);
		MACVector4<T>&	operator*=	(const MACVector<T>& v);
		MACVector4<T>&	operator*=	(const T v);
		MACVector4<T>&	operator/=	(const MACVector<T>& v);
		MACVector4<T>&	operator/=	(const T v);

		// Arithmetic
		MACVector4<T>	operator+	(const MACVector<T>& v)	const;
		MACVector4<T>	operator+	(const T v)				const;
		MACVector4<T>	operator-	(const MACVector<T>& v)	const;
		MACVector4<T>	operator-	(const T v)				const;
		MACVector4<T>	operator-	()						const;
		MACVector4<T>	operator*	(const MACVector<T>& v)	const;
		MACVector4<T>	operator*	(const T v)				const;
		MACVector4<T>	operator/	(const MACVector4<T>& v)const;
		MACVector4<T>	operator/	(const T v)				const;
	

		MACVector4<T>&	scale		(const T& v)				{ return ((*this) *= v); }
		MACVector4<T>&	scale		(const MACVector4<T>& v)	{ return ((*this) *= v); }


	public:		/*** Comparitors ***/

	
		bool			equal		(const MACVector4<T>& v, T error = ERROR_VALUE)	const;
		bool			operator==	(const MACVector4<T>& v)						const;
		bool			operator!=	(const MACVector4<T>& v)						const;
		

	public:		/*** Geometric Maths functions ***/

		
		T				lengthSquared	()	const				{ return x*x + y*y + z*z + w*w; }
		T				length			()	const;
		T				mod				()	const				{ return length(); }
		
		MACVector4<T>&	normalize		();

		T				dot				(const MACVector<T>& other)	const;
		
		// Cross product of v2 and v2, result placed in *this. Highly efficient if dirty.
		MACVector4<T>&	cross3			(const MACVector<T>& v1, const MACVector<T>& v2); 
		
		// Cross product of *this and v, result returned as independant value
		MACVector4<T>	cross3			(const MACVector<T>& v)		const; 
		

	public:		/*** Allied functions ***/

		friend T			dot		(const MACVector4<T>& v1, const MACVector4<T>& v2);
		//friend MACVector4<T>	cross3	(const MACVector4<T>& v1, const MACVector4<T>& v2);
		

	protected:	/*** IO functions ***/

		friend std::istream& operator>>(std::istream&, MACVector4<T>&);
		friend std::ostream& operator<<(std::ostream&, MACVector4<T>&);
		// Note instanciation of the base class will have the extra effect of correctly instaciating these

};

//
// End of Class Definitions
//
//***********************************************************************
//***********************************************************************



//***********************************************************************
//***********************************************************************
//
// Templated Method Code
//

//*********************************************
//
// General Arithmetic Assignment Operations
//
// Notes:
//

// Log:
//		Created 2/27/01, Ian Elsley
template<class T> inline MACVector4<T>&
MACVector4<T>::operator+=	(const MACVector<T>& v)
{
	x += v.X(); y += v.Y(); z += v.Z(); w += v.W();
	return *this;
}

// Log:
//		Created 2/27/01, Ian Elsley
template<class T> inline MACVector4<T>&
MACVector4<T>::operator+=	(const T v)
{
	x += v; y += v; z += v; w += v;
	return *this;
}

// Log:
//		Created 2/27/01, Ian Elsley
template<class T> inline MACVector4<T>&
MACVector4<T>::operator-=	(const MACVector<T>& v)
{
	x -= v.X(); y -= v.Y(); z -= v.Z(); w -= v.W();
	return *this;
}

// Log:
//		Created 2/27/01, Ian Elsley
template<class T> inline MACVector4<T>&
MACVector4<T>::operator-=	(const T v)
{
	x -= v; y -= v; z -= v; w -= v;
	return *this;
}

// Log:
//		Created 2/27/01, Ian Elsley
template<class T> inline MACVector4<T>&
MACVector4<T>::operator*=	(const MACVector<T>& v)
{
	x *= v.X(); y *= v.Y(); z *= v.Z(); w *= v.W();
	return *this;
}

// Log:
//		Created 2/27/01, Ian Elsley
template<class T> inline MACVector4<T>&
MACVector4<T>::operator*=	(const T v)
{
	x *= v; y *= v; z *= v; w *= v;
	return *this;
}

// Log:
//		Created 2/27/01, Ian Elsley
template<class T> inline MACVector4<T>&
MACVector4<T>::operator/=	(const MACVector<T>& v)
{
	x /= v.X(); y /= v.Y(); z /= v.Z(); w /= v.W();
	return *this;
}

// Log:
//		Created 2/27/01, Ian Elsley
template<class T> inline MACVector4<T>&
MACVector4<T>::operator/=	(const T v)
{
	x /= v; y /= v; z /= v; w /= v;
	return *this;
}


//************************************
//
// General Arithmetic Operations
//
// Notes:
//

// Log:
//		Created 2/27/01, Ian Elsley
template<class T> inline MACVector4<T>
MACVector4<T>::operator+	(const MACVector<T>& v)	const
{
	return MACVector4<T>(x+v.X(), y+v.Y(), z+v.Z(), w+v.W());
}

// Log:
//		Created 2/27/01, Ian Elsley
template<class T> inline MACVector4<T>
MACVector4<T>::operator+	(const T v)				const
{
	return MACVector4<T>(x+v, y+v, z+v, w+v);
}

// Log:
//		Created 2/27/01, Ian Elsley
template<class T> inline MACVector4<T>
MACVector4<T>::operator-	(const MACVector<T>& v)	const
{
	return MACVector4<T>(x-v.X(), y-v.Y(), z-v.Z(), w-v.W());
}

// Log:
//		Created 2/27/01, Ian Elsley
template<class T> inline MACVector4<T>
MACVector4<T>::operator-	(const T v)				const
{
	return MACVector4<T>(x-v, y-v, z-v, w-v);
}

// Log:
//		Created 7/19/01, Ian Elsley
template<class T> inline MACVector4<T>
MACVector4<T>::operator-	()						const
{
	MACVector4<T> result(-x, -y, -z, -w);
	return result;
}

// Log:
//		Created 2/27/01, Ian Elsley
template<class T> inline MACVector4<T>
MACVector4<T>::operator*	(const MACVector<T>& v)	const
{
	return MACVector4<T>(x*v.X(), y*v.Y(), z*v.Z(), w*=v.W());
}

// Log:
//		Created 2/27/01, Ian Elsley
template<class T> inline MACVector4<T>
MACVector4<T>::operator*	(const T v)				const
{
	return MACVector4<T>(x*v, y*v, z*v, w*v);
}

// Log:
//		Created 2/27/01, Ian Elsley
template<class T> inline MACVector4<T>
MACVector4<T>::operator/	(const MACVector4<T>& v)	const
{
	return MACVector4<T>(x/v.X(), y/v.Y(), z/v.Z(), w/v.w());
}

// Log:
//		Created 2/27/01, Ian Elsley
template<class T> inline MACVector4<T>
MACVector4<T>::operator/	(const T v)				const	
{
	return MACVector4<T>(x/v, y/v, z/v, w/v);
}


//**************************************
//
// Comparitor Functions
//
//

// 
// MACVector4<T>::equal(const MACVector4<T>& v, T error = ERROR_VALUE)	const
// Notes:
//		Allied with this function are the explicit instanciations of the float and double cases
//		These are identical, but are implemented like this for speed.
// Log:
//		Created 2/27/01, Ian Elsley
//
template<class T> inline bool
MACVector4<T>::equal(const MACVector4<T>& v, T error)					const
{
	return (((T)fabs((double)(x-v.X())) < error) && 
			((T)fabs((double)(y-v.Y())) < error) && 
			((T)fabs((double)(z-v.Z())) < error) && 
			((T)fabs((double)(w-v.w)) < error));
}

//
// Equal
// Notes:
//		Double version
// Log:
//		Created 2/27/01, Ian Elsley
//
inline bool
MACVector4<double>::equal(const MACVector4<double>& v, double error)	const
{
	return ((fabs(x-v.X()) < error) && 
			(fabs(y-v.Y()) < error) && 
			(fabs(z-v.Z()) < error) && 
			(fabs(w-v.w) < error));
}

//
// Equal
// Notes:
//		Float version
// Log:
//		Created 2/27/01, Ian Elsley
//
inline bool
MACVector4<float>::equal(const MACVector4<float>& v, float error)		const
{
	return ((fabsf(x-v.X()) < error) && 
			(fabsf(y-v.Y()) < error) && 
			(fabsf(z-v.Z()) < error) && 
			(fabsf(w-v.w) < error));
}


//
// MACVector4<T>::operator==(const MACVector4<T>& v) const
// Notes:
//		This function may not work entirely as expected. 
//		There may be an error factor required.
// Log:
//		Created 2/27/01, Ian Elsley
//
template<class T> inline bool
MACVector4<T>::operator==(const MACVector4<T>& v)						const
{
	return ((x==v.X()) && (y==v.Y()) && (z==v.Z()) && (w==w.W()));
}


//
// MACVector4<T>::operator!=(const MACVector4<T>& v) const
// Notes:
// Log:
//		Created 2/27/01, Ian Elsley
//
template<class T> inline bool
MACVector4<T>::operator!=(const MACVector4<T>& v)						const
{
	return (!((*this) == v));
}


//***********************************************************************
//***********************************************************************
//
// Geometric Math Functions
//
//

// 
// Length
// Notes:
// Log:
//		Created 2/27/01, Ian Elsley
//
template<class T> inline T
MACVector4<T>::length() const
{
	T lenSqr = this->lengthSquared();

	if (lenSqr <= 0) return (T)0;
	return (T)sqrt((double)lenSqr);
}

// 
// Length
// Notes:
// Log:
//		Created 2/27/01, Ian Elsley
//
template<class T>	MACVector4<T>&
MACVector4<T>::normalize		()	
{ 
	T len = this->length();
	if (len > MAC_LENGTH_NEAR_ZERO)
		return ((*this) /= this->length()); 
	else
		return set((T)1, (T)0, (T)0, (T)0);
}

// 
// Dot Product
// Notes:
// Log:
//		Created 2/27/01, Ian Elsley
//
template<class T> inline T
MACVector4<T>::dot(const MACVector<T>& v) const
{
	return (x*v.X() + y*v.Y() + z*v.Z() + x*v.W());
}

//
// Cross Product
// Notes:
//		result in *this for high efficiency
//		NOTA BENE: Only operate as a cross3
// Log:
//		Created 7/18/01, Ian Elsley
//
template<class T> inline MACVector4<T>&
MACVector4<T>::cross3(const MACVector<T>& v1, const MACVector<T>& v2)
{
	// Cross product implemented in this fashion in case v1 or v2 is *this
	T	x1 = v1.X(), 
		y1 = v1.Y(),
		z1 = v1.Z(),
		x2 = v2.X(),
		y2 = v2.Y(),
		z2 = v2.Z();

	X(y1*z2 - z1*y2);
	Y(z1*x2 - x1*z2);
	Z(x1*y2 - y1*x2);
	w = (T)0;

	return (*this);
}



//
// Cross Product
// Notes:
//		result as seperate value. Lower effciency.
//		NOTA BENE: Only operate as a cross3
// Log:
//		Created 7/18/01, Ian Elsley
//
template<class T> inline MACVector4<T>
MACVector4<T>::cross3(const MACVector<T>& v)	const
{
	MACVector4<T> result =  cross3(*this, v);
	return result;
}

//
// End of Method Code
//
//***********************************************************************
//***********************************************************************


//***********************************************************************
//***********************************************************************
//
// Start of Function Code
//


// 
// Dot Product Function
// Notes:
//		dot product function as opposed to method
// Log:
//		Created 2/27/01, Ian Elsley
//
template<class T> T
dot(const MACVector4<T>& v1, const MACVector4<T>& v2)
{
	return (v1.X()*v2.X() + v1.Y()*v2.Y() + v1.Z()*v2.Z() + v1.W()*v2.W());
}
 


//
// Cross Product Function
// Notes:
//		Cross Product Function template. **NOT A METHOD**
//		result as seperate value. Lower effciency.
//		NOTA BENE: Only operate as a cross3
// Log:
//		Created 7/18/01, Ian Elsley
//
/*template<class T> MACVector4<T>
cross3(const MACVector4<T>& v1, const MACVector4<T>& v2)
{
	MACVector4<T> result;
	return (result.cross3(v1, v2));
}
*/

//***********************************************************************
//***********************************************************************
//
// I/O Functions
//
// Notes:
//

//
// operator>>
// Notes:
// Log:
//		Created 2/27/01, Ian Elsley
//
template<class T> std::istream& 
operator>>(std::istream& in, MACVector4<T>& v)
{
	int i;
	for (i=0; i<4; i++)
		in >> v[i];
	return in;
}

//
// operator<<
// Notes:
// Log:
//		Created 2/27/01, Ian Elsley
//
template<class T> std::ostream& 
operator<<(std::ostream& out, MACVector4<T>& v)
{
	out << x << ' ' << y << ' ' << z << ' ' << w;
	return out;
}

//
// End of Function Code
//
//***********************************************************************
//***********************************************************************


MAC_MATH_LIB_END

#endif __MAC_VECTOR4_H__