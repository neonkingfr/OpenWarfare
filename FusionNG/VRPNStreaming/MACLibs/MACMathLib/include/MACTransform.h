//******************************************************************************************************
//******************************************************************************************************
//
// Filename:	MACTransform.h
// Author:		Ian Elsley
// Created:		26th Feb, 2001
//
//
// Documentation:
//		Generalised abstract transform header class.
//		This will be used by all other transformantion types as the generic header class.
//		In the main, all accesses to these classes will be via this.
//
//
//
// Log:
//		Created 2/28/2001, Ian Elsley
//
// Notes:
//
//		Failure is currently through assert though there will be throws. Users should not use local memory
//		pointers but should use the inbuilt ptr<T> forms instead to ensure cleanup on rewind.
//
// Copyright (c) 2001 Motion Analysis Corportation.
// All rights reserved.
//
//
// This copyright needs updating, but this code is internal to MAC. Do not use, copy, look at or breathe.
//
//******************************************************************************************************
//******************************************************************************************************

#ifndef __MAC_TRANSFORM_H__
#define __MAC_TRANSFORM_H__

MAC_MATH_LIB_BEGIN


//***********************************************************************
//***********************************************************************
//
// Templated Class Definitions
//

//
// MACTransform<T>
// Notes: 
//		Generalised Abstract Class for use as a container for all forms of transform.
//		No variables can be defined within this.
//
template<class T>
class MACTransform
{

	public:


		~MACTransform<T>()				{}


	public:		/*** shared scaling routines ***/


	public:		/*** shared get members ***/

};

//
// End ofTemplated Class Definitions
//
//***********************************************************************
//***********************************************************************



//********************************************
//
// Predefinition of classes
//
template <class T> class MACTranslation;
template <class T> class MACScale;
template <class T> class MACRotation;

MAC_MATH_LIB_END

#include "MACTranslation.h"
#include "MACScale.h"
#include "MACRotation.h"


#endif __MAC_TRANSFORM_H__