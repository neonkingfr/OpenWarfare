//******************************************************************************************************
//******************************************************************************************************
//
// Filename:	MACVector3.h
// Author:		Ian Elsley
// Created:		26th Feb, 2001
//
//
// Documentation:
//		Vector3 class and templates for the Solver Interface. This library is the prototye for the
//		maths library in general.
//
//		MACVector3 has allied functions:
//
//			T				dot		(const MACVector3<T>& v1, const MACVector3<T>& v2);
//			MACVector3<T>	cross	(const MACVector3<T>& v1, const MACVector3<T>& v2)
//
//
//
// Log:
//
// Notes:
//
//		There should perhaps be failure with asserts on access to non existant members
//
// Copyright (c) 2001 Motion Analysis Corportation.
// All rights reserved.
//
//
// This copyright needs updating, but this code is internal to MAC. Do not use, copy, look at or breathe.
//
//******************************************************************************************************
//******************************************************************************************************

#ifndef __MAC_VECTOR3_H__
#define __MAC_VECTOR3_H__

#include <iostream>

MAC_MATH_LIB_BEGIN


//***********************************************************************
//***********************************************************************
//
// Instanciated Vectors
//
// Notes:
// Log:
//		Created 28/2/01, Ian Elsley
//

typedef	MACVector3<double>		MACVec3d;
typedef MACVector3<float>		MACVec3f;
typedef MACVector3<int>			MACVec3i;


//
// End of instanciation
//
//***********************************************************************
//***********************************************************************


//***********************************************************************
//***********************************************************************
//
// Templated Class Definitions
//


//
// MACVector3
//
// Notes:
// Log:
//		Created 2/27/01, Ian Elsley
//
template<class T>
class MACVector3 : public MACVector<T>
{

	private:
		
		
		T		x;
		T		y;
		T		z;


	public:		/*** Constructors ***/

		
		MACVector3<T>	()										: x((T)0), y((T)0), z((T)0)	{}
		MACVector3<T>	(T v)									: x(v), y(v), z(v)			{}
		MACVector3<T>	(T vx, T vy, T vz)						: x(vx), y(vy), z(vz)		{}
		//MACVector3<T>	(const MACVector<T>& v)					: x(v.X()/v.W()), y(v.Y()/v.W()), z(v.Z()/v.W()) {}
		MACVector3<T>	(const MACVector<T>& v)					: x(v.X()), y(v.Y()), z(v.Z()) {}

		virtual ~MACVector3<T>	()								{}

	public:		/*** Access - Set and Get ***/

		
		// Standard array access
		virtual T&			operator[]	(const unsigned int i)			{ return (&x)[i]; }
		virtual const T&	operator[]	(const unsigned int i) const	{ return (&x)[i]; }

	
		// Get the values
		virtual T	X()	const									{ return x; }
		virtual T	Y()	const									{ return y; }
		virtual T	Z()	const									{ return z; }	
		virtual T	W()	const									{ return (T)0; }	
																/* This should perhaps fail with an assert */

		// Set the values
		T			X(T v)										{ return x=v; }
		T			Y(T v)										{ return y=v; }
		T			Z(T v)										{ return z=v; }
		T			W(T v)										{ _ASSERT(0); return (T)0; }
																/* This should perhaps fail with an assert */

		// Set the values in a group
		MACVector3<T>&	set	(const T& v)						{ x=v; y=v; z=v; return *this; }
		MACVector3<T>&	set	(const T& vx, const T& vy, const T& vz)	
																{ x=vx; y=vy; z=vz; return *this; }
		//MACVector3<T>&	set	(const MACVector<T>& v)				{ x=v.X()/v.W(); y=v.Y()/v.W(); z=v.Z()/v.W(); return *this; }
		MACVector3<T>&	set	(const MACVector<T>& v)				{ x=v.X(); y=v.Y(); z=v.Z(); return *this; }


	public:		/*** General Arithmetic operators ***/

		
		// Assignment
		MACVector3<T>&	operator=	(const MACVector<T>& v)		{ return set(v); }
		MACVector3<T>&	operator=	(const T v)					{ return set(v); }
		MACVector3<T>&	operator+=	(const MACVector3<T>& v);
		MACVector3<T>&	operator+=	(const T v);
		MACVector3<T>&	operator-=	(const MACVector3<T>& v);
		MACVector3<T>&	operator-=	(const T v);
		MACVector3<T>&	operator*=	(const MACVector3<T>& v);
		MACVector3<T>&	operator*=	(const T v);
		MACVector3<T>&	operator/=	(const MACVector3<T>& v);
		MACVector3<T>&	operator/=	(const T v);

		// Arithmetic
		MACVector3<T>	operator+	(const MACVector3<T>& v)	const;
		MACVector3<T>	operator+	(const T v)					const;
		MACVector3<T>	operator-	(const MACVector3<T>& v)	const;
		MACVector3<T>	operator-	(const T v)					const;
		MACVector3<T>	operator*	(const MACVector3<T>& v)	const;
		MACVector3<T>	operator*	(const T v)					const;
		MACVector3<T>	operator/	(const MACVector3<T>& v)	const;
		MACVector3<T>	operator/	(const T v)					const;

		MACVector3<T>&	scale		(const T& v)				{ return ((*this) *= v); }
		MACVector3<T>&	scale		(const MACVector3<T>& v)	{ return ((*this) *= v); }


	public:		/*** Comparitors ***/

	
		bool			equal		(const MACVector3<T>& v, T error = MAC_VECTOR_ERROR_LIMIT)	const;
		bool			operator==	(const MACVector3<T>& v)							const;
		bool			operator!=	(const MACVector3<T>& v)							const;
		

	public:		/*** Geometric Maths functions ***/

	
		T				lengthSquared	()	const				{ return x*x + y*y + z*z; }
		T				length			()	const;
		T				mod				()	const				{ return length(); }

		MACVector3<T>&	normalize		();

		T				dot				(const MACVector3<T>& other)	const;
		
		// Cross product of v2 and v2, result placed in *this. Highly efficient.
		MACVector3<T>&	cross			(const MACVector3<T>& v1, const MACVector3<T>& v2); 
		
		// Cross product of *this and v, result returned as independant value
		MACVector3<T>	cross			(const MACVector3<T>& v)		const; 


	public:		/*** Allied functions ***/

	
		friend T				dot		(const MACVector3<T>& v1, const MACVector3<T>& v2);
		friend MACVector3<T>	cross	(const MACVector3<T>& v1, const MACVector3<T>& v2);
		// Note instanciation of the base class will have the extra effect of correctly instaciating these


	protected:	/*** IO functions ***/

	
		friend std::istream& operator>>(std::istream&, MACVector3<T>&);
		friend std::ostream& operator<<(std::ostream&, MACVector3<T>&);
		// Note instanciation of the base class will have the extra effect of correctly instaciating these

};

//
// End of Class Definitions
//
//***********************************************************************
//***********************************************************************


//***********************************************************************
//***********************************************************************
//
// Templated Method Code
//

//***********************************************
//
// General Arithmetic Assignment Operations
//
// Notes:
//

// Log:
//		Created 2/27/01, Ian Elsley
template<class T> inline MACVector3<T>&
MACVector3<T>::operator+=	(const MACVector3<T>& v)
{
	x += v.X(); y += v.Y(); z += v.Z();
	return *this;
}

// Log:
//		Created 2/27/01, Ian Elsley
template<class T> inline MACVector3<T>&
MACVector3<T>::operator+=	(const T v)
{
	x += v; y += v; z += v;
	return *this;
}

// Log:
//		Created 2/27/01, Ian Elsley
template<class T> inline MACVector3<T>&
MACVector3<T>::operator-=	(const MACVector3<T>& v)
{
	x -= v.X(); y -= v.Y(); z -= v.Z();
	return *this;
}

// Log:
//		Created 2/27/01, Ian Elsley
template<class T> inline MACVector3<T>&
MACVector3<T>::operator-=	(const T v)
{
	x -= v; y -= v; z -= v;
	return *this;
}

// Log:
//		Created 2/27/01, Ian Elsley
template<class T> inline MACVector3<T>&
MACVector3<T>::operator*=	(const MACVector3<T>& v)
{
	x *= v.X(); y *= v.Y(); z *= v.Z();
	return *this;
}

// Log:
//		Created 2/27/01, Ian Elsley
template<class T> inline MACVector3<T>&
MACVector3<T>::operator*=	(const T v)
{
	x *= v; y *= v; z *= v;
	return *this;
}

// Log:
//		Created 2/27/01, Ian Elsley
template<class T> inline MACVector3<T>&
MACVector3<T>::operator/=	(const MACVector3<T>& v)
{
	x /= v.X(); y /= v.Y(); z /= v.Z();
	return *this;
}

// Log:
//		Created 2/27/01, Ian Elsley
template<class T> inline MACVector3<T>&
MACVector3<T>::operator/=	(const T v)
{
	x /= v; y /= v; z /= v;
	return *this;
}


//********************************************
//
// General Arithmetic Operations
//
// Notes:
//

// Log:
//		Created 2/27/01, Ian Elsley
template<class T> inline MACVector3<T>
MACVector3<T>::operator+	(const MACVector3<T>& v)	const
{
	return MACVector3<T>(x+v.X(), y+v.Y(), z+v.Z());
}

// Log:
//		Created 2/27/01, Ian Elsley
template<class T> inline MACVector3<T>
MACVector3<T>::operator+	(const T v)				const
{
	return MACVector3<T>(x+v, y+v, z+v);
}

// Log:
//		Created 2/27/01, Ian Elsley
template<class T> inline MACVector3<T>
MACVector3<T>::operator-	(const MACVector3<T>& v)	const
{
	return MACVector3<T>(x-v.X(), y-v.Y(), z-v.Z());
}

// Log:
//		Created 2/27/01, Ian Elsley
template<class T> inline MACVector3<T>
MACVector3<T>::operator-	(const T v)				const
{
	return MACVector3<T>(x-v, y-v, z-v);
}

// Log:
//		Created 2/27/01, Ian Elsley
template<class T> inline MACVector3<T>
MACVector3<T>::operator*	(const MACVector3<T>& v)	const
{
	return MACVector3<T>(x*v.X(), y*v.Y(), z*v.Z());
}

// Log:
//		Created 2/27/01, Ian Elsley
template<class T> inline MACVector3<T>
MACVector3<T>::operator*	(const T v)				const
{
	return MACVector3<T>(x*v, y*v, z*v);
}

//
// Log:
//		Created 2/27/01, Ian Elsley
template<class T> inline MACVector3<T>
MACVector3<T>::operator/	(const MACVector3<T>& v)	const
{
	return MACVector3<T>(x/v.X(), y/v.Y(), z/v.Z());
}

//
// Log:
//		Created 2/27/01, Ian Elsley
template<class T> inline MACVector3<T>
MACVector3<T>::operator/	(const T v)				const	
{
	return MACVector3<T>(x/v, y/v, z/v);
}


//*************************************************
//
// Comparitor Functions
//
//

// 
// MACVector3<T>::equal(const MACVector3<T>& v, T error = ERROR_VALUE)	const
// Notes:
//		Allied with this function are the explicit instanciations of the float and double cases
//		These are identical, but are implemented like this for speed.
// Log:
//		Created 2/27/01, Ian Elsley
//
template<class T> inline bool
MACVector3<T>::equal(const MACVector3<T>& v, T error)					const
{
	return (((T)fabs((double)(x-v.X())) < error) && ((T)fabs((double)(y-v.Y())) < error) && ((T)fabs((double)(z-v.Z())) < error));
}

//
// Equal
// Notes:
//		Double version
// Log:
//		Created 2/27/01, Ian Elsley
//
inline bool
MACVector3<double>::equal(const MACVector3<double>& v, double error)	const
{
	return ((fabs(x-v.X()) < error) && (fabs(y-v.Y()) < error) && (fabs(z-v.Z()) < error));
}

//
// Equal
// Notes:
//		Float version
// Log:
//		Created 2/27/01, Ian Elsley
//
inline bool
MACVector3<float>::equal(const MACVector3<float>& v, float error)		const
{
	return ((fabsf(x-v.X()) < error) && (fabsf(y-v.Y()) < error) && (fabsf(z-v.Z()) < error));
}


//
// MACVector3<T>::operator==(const MACVector3<T>& v) const
// Notes:
//		This function may not work entirely as expected. 
//		There may be an error factor required.
// Log:
//		Created 2/27/01, Ian Elsley
//
template<class T> inline bool
MACVector3<T>::operator==(const MACVector3<T>& v)						const
{
	return ((x==v.X()) && (y==v.Y()) && (z==v.Z()));
}


//
// MACVector3<T>::operator!=(const MACVector3<T>& v) const
// Notes:
// Log:
//		Created 2/27/01, Ian Elsley
//
template<class T> inline bool
MACVector3<T>::operator!=(const MACVector3<T>& v)						const
{
	return (!((*this) == v));
}


//***********************************************************************
//***********************************************************************
//
// Geometric Math Functions
//
//

// 
// Length
// Notes:
// Log:
//		Created 2/27/01, Ian Elsley
//
template<class T>	inline T
MACVector3<T>::length() const
{
	T lenSqr = this->lengthSquared();

	if (lenSqr <= 0) return (T)0;
	return (T)sqrt((double)lenSqr);
}


// 
// Normalize
// Notes:
// Log:
//		Created 2/27/01, Ian Elsley
//
template<class T>	MACVector3<T>&
MACVector3<T>::normalize		()	
{ 
	T len = this->length();
	if (len > MAC_LENGTH_NEAR_ZERO)
		return ((*this) /= this->length()); 
	else
		return set((T)1, (T)0, (T)0);
}

// 
// Dot Product
// Notes:
// Log:
//		Created 2/27/01, Ian Elsley
//
template<class T> inline T
MACVector3<T>::dot(const MACVector3<T>& v) const
{
	return this->dot3(v);
}


//
// Cross Product
// Notes:
//		result in *this for high efficiency
// Log:
//		Created 2/27/01, Ian Elsley
//
template<class T> inline MACVector3<T>&
MACVector3<T>::cross(const MACVector3<T>& v1, const MACVector3<T>& v2)
{
	// Cross product implemented in this fashion in case v1 or v2 is *this
	T	x1 = v1.X(), 
		y1 = v1.Y(),
		z1 = v1.Z(),
		x2 = v2.X(),
		y2 = v2.Y(),
		z2 = v2.Z();

	x = y1*z2 - z1*y2;
	y = z1*x2 - x1*z2;
	z = x1*y2 - y1*x2;

	return (*this);
}


//
// Cross Product
// Notes:
//		result as seperate value. Lower effciency.
// Log:
//		Created 2/27/01, Ian Elsley
//
template<class T> inline MACVector3<T>
MACVector3<T>::cross(const MACVector3<T>& v)	const
{
	MACVector3<T> result;
	return (result.cross(*this, v));
}

//
// End of Method Code
//
//***********************************************************************
//***********************************************************************


//***********************************************************************
//***********************************************************************
//
// Start of Function Code
//


// 
// Dot Product Function
// Notes:
//		dot product function as opposed to method
// Log:
//		Created 2/27/01, Ian Elsley
//
template<class T> inline T
dot	(const MACVector3<T>& v1, const MACVector3<T>& v2)
{
	return dot3(v1, v2);
}


//
// Cross Product Function
// Notes:
//		Cross Product Function template. **NOT A METHOD**
//		result as seperate value. Lower effciency.
// Log:
//		Created 2/27/01, Ian Elsley
//
template<class T> MACVector3<T>
cross(const MACVector3<T>& v1, const MACVector3<T>& v2)
{
	MACVector3<T> result;
	return (result.cross(v1, v2));
}


//**************************************
//
// I/O Functions
//
// Notes:
//

//
// operator>>
// Notes:
// Log:
//		Created 2/27/01, Ian Elsley
//
template<class T> std::istream& 
operator>>(std::istream& in, MACVector3<T>& v)
{
	int i;
	for (i=0; i<3; i++)
		in >> v[i];
	return in;
}

//
// operator<<
// Notes:
// Log:
//		Created 2/27/01, Ian Elsley
//
template<class T> std::ostream& operator<<(std::ostream& out, MACVector3<T>& v)
{
	out << x << ' ' << y << ' ' << z;
	return out;
}

//
// End of Function Code
//
//***********************************************************************
//***********************************************************************


MAC_MATH_LIB_END

#endif __MAC_VECTOR3_H__