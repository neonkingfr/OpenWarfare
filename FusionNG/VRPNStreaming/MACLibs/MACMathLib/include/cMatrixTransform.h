#pragma once
#include <iostream>
#include "eEulerOrder.h"
#include "cPoint.h"
#include "cVector.h"


namespace TalonViewerLib
{
	class cSkeletonTransform;


	/* Simple class for handling transformation matrices */
	class cMatrixTransform
	{
	public:
		/* Constructors */
		cMatrixTransform();
		cMatrixTransform(const cMatrixTransform &src);
		

		/* Initializaion */
		void Identity();
		void SetToTranslation(float TX, float TY, float TZ);
		void SetToTranslation(const cVector &T);
		void SetToEulerRotation(eEulerOrder Order, float Angle1, float Angle2, float Angle3);//angles are applied as A1*A2*A3*Point/Vector
		void SetToAngleAxisRotation(const cVector &Axis, float Angle);
		void SetToScale(float SX, float SY, float SZ);
		void SetToScale(float S);//uniform scale

		/* Operators */
		cMatrixTransform &operator=(const cMatrixTransform &src);
		friend cMatrixTransform operator*(const cMatrixTransform &L, const cMatrixTransform &R);
		friend cPoint     operator*(const cMatrixTransform &M, const cPoint     &P);
		friend cVector    operator*(const cMatrixTransform &M, const cVector    &V);
		friend std::ostream &operator<<(std::ostream &out, const cMatrixTransform &trans);
		friend std::istream &operator>>(std::istream & in, cMatrixTransform &trans);
		
		/* Tools */
		bool IsEmpty() const;
		void MakeEmpty();
		void ToOpenGL(float glMatrix[16]) const;
		void ToCG(float cgMatrix[12]) const;
		float Determinant() const;
		bool Invert();//returns false is the matrix is non-invertable
		void Decompose(cSkeletonTransform &Decomposition) const;
		bool Compare(const cMatrixTransform &M, int nDecimalPlaces) const;
		bool IsIdentity(int nDecimalPlaces) const;

		/* IO */
		friend std::ostream &operator<<(std::ostream &out, const cMatrixTransform &M);
		friend std::istream &operator>>(std::istream &in ,       cMatrixTransform &M);


	private:
		float *operator[](int iRow);
		const float *operator[](int iRow) const;

		float m_M[3][4];//row major
	};
};