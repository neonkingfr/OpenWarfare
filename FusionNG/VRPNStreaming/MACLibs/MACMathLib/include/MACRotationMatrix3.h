//******************************************************************************************************
//******************************************************************************************************
//
// Filename:	MACRotationMatrix3.h
// Author:		Ian Elsley
// Created:		16th March, 2001
//
//
// Documentation:
//		 Matrix Template
//		
//
// Log:
//		Created 3/28/2001, Ian Elsley
//
// Notes:
//
//		Failure is currently through assert though there will be throws. Users should not use local memory
//		pointers but should use the inbuilt ptr<T> forms instead to ensure cleanup on rewind.
//
// Copyright (c) 2001 Motion Analysis Corportation.
// All rights reserved.
// 
//
// This copyright needs updating, but this code is internal to MAC. Do not use, copy, look at or breathe.
//
//******************************************************************************************************
//******************************************************************************************************


#ifndef __MAC_ROTATION_MATRIX_3_H__
#define __MAC_ROTATION_MATRIX_3_H__

#include <windows.h>
#include <gl/gl.h>
#include "MACVector3.h"
#include "MACMatrix.h"

MAC_MATH_LIB_BEGIN

//****************************************************************************************
//****************************************************************************************
//
// Instanciated Vectors
//
// Notes:
// Log:
//		Created 28/2/01, Ian Elsley
//

typedef	MACRotationMatrix3<double>		MACRotM3d;
typedef MACRotationMatrix3<float>		MACRotM3f;


//
// End of instanciation
//
//****************************************************************************************
//****************************************************************************************


//****************************************************************************************
//****************************************************************************************
//
// Class Definition
//

template<class T>
class MACRotationMatrix3 : public MACRotation<T>
{

	private:	/*** Variables ***/

	
		MACVector3<T>			x;
		MACVector3<T>			y;
		MACVector3<T>			z;


	public:		/*** Constructors and Destructors ***/


		// Constructors from set values
		MACRotationMatrix3<T>	()									: x((T)1, (T)0, (T)0), y((T)0, (T)1, (T)0), z((T)0, (T)0, (T)1) {}
		MACRotationMatrix3<T>	(const MACVector3<T>& side, const MACVector3<T>& up, const MACVector3<T>& dir)
																	: x(side), y(up), z(dir) {}
		MACRotationMatrix3<T>	(const MACVector3<T>& up, const MACVector3<T>& dir);

		// Constructors from other Classes
		MACRotationMatrix3<T>	(const MACRotationMatrix3<T>& v)	: x(v.x), y(v.y), z(v.z) {}
		MACRotationMatrix3<T>	(const MACHomogeneousMatrix4<T>& v)	: x(v.I()), y(v.J()), z(v.K()) {}
		MACRotationMatrix3<T>	(const MACEulerRotation<T>& v)		{ set(v); }
		MACRotationMatrix3<T>	(const MACAngleAxisRotation<T>& v)	{ set(v); }
		MACRotationMatrix3<T>	(const MACQuaternion<T>& v)			{ set(v); }

		virtual ~MACRotationMatrix3<T>		()						{}

	
	public:		/*** Set and Get ***/


		// Standard array access
		MACVector3<T>&			operator[]	(const unsigned int i)			{ return (&x)[i]; }
		const MACVector3<T>&	operator[]	(const unsigned int i) const	{ return (&x)[i]; }

		// Get
		const MACVector3<T>&	MACde		()	const				{ return x; }
		const MACVector3<T>&	Up			()	const				{ return y; }
		const MACVector3<T>&	Dir			()	const				{ return z; }
		
		const MACVector3<T>&	I			()	const				{ return x; }
		const MACVector3<T>&	J			()	const				{ return y; }
		const MACVector3<T>&	K			()	const				{ return z; }

		// Set
		MACRotationMatrix3<T>&	set			(const MACVector3<T>& side, const MACVector3<T>& up, const MACVector3<T>& dir)
																	{ x.set(side); y.set(up); z.set(dir); return *this; }
		MACRotationMatrix3<T>&	set			(const MACVector3<T>& up, const MACVector3<T>& dir);
		
		// shared set
		MACRotationMatrix3<T>&	set			(const MACHomogeneousMatrix4<T>& v);
		MACRotationMatrix3<T>&	set			(const MACRotationMatrix3<T>& v);
		MACRotationMatrix3<T>&	set			(const MACEulerRotation<T>& v);
		MACRotationMatrix3<T>&	set			(const MACAngleAxisRotation<T>& v);
		MACRotationMatrix3<T>&	set			(const MACQuaternion<T>& v);


	public:		/*** General Arithmetic functions ***/


		T						determinant	();
		MACRotationMatrix3<T>&	invert		();
		MACRotationMatrix3<T>&	transpose	();

		// Note: Returns value as a seperate matrix. Does not affect this one.
		MACRotationMatrix3<T>	theInverse		()	const;
		MACRotationMatrix3<T>	theTranspose	()	const;

	
	
	public:		/*** General Arithmetic operators ***/


		// Assignment
		MACRotationMatrix3<T>&	operator=	(const MACRotationMatrix3<T>& r)	{ return this->set(r); }
		MACRotationMatrix3<T>&	operator*=	(const MACRotationMatrix3<T>& v);
		
		// Arithmetic
		MACRotationMatrix3<T>	operator*	(const MACRotationMatrix3<T>& v) const;
		MACRotationMatrix3<T>	operator-	()	const							{ return theInverse(); }
		

	
	public:		/*** Comparitors ***/


		bool					equal		(const MACRotationMatrix3<T>& v, T error = MAC_MATRIX_ERROR_LIMIT)	const;
		bool					operator==	(const MACRotationMatrix3<T>& v)									const;
		bool					operator!=	(const MACRotationMatrix3<T>& v)									const;

	

	public:		/*** Identity ***/


		MACRotationMatrix3<T>	identity	()	{ return MACRotationMatrix3<T>(); }
		MACRotationMatrix3<T>&	setIdentity	();
		bool					isIdentity	()	{ return equal(identity()); }


	public:		/*** OpenGl code ***/


		MACRotationMatrix3<T>&	setFromGLMatrix	(const GLfloat m[16]);
		void					getGLMatrix		(GLfloat m[16]);


	protected:	/*** Alied classes ***/


		friend class MACHomogeneousMatrix4<T>;


	protected:	/*** Alied functions ***/

#if _MSC_VER >= 1300
		template <class T>	friend MACVector3<T> operator*	(const MACVector3<T>& v, const MACRotationMatrix3<T>& m);
		template <class T>	friend MACVector3<T> operator*	(const MACRotationMatrix3<T>& m, const MACVector3<T>& v);
#else
		/*template <class T>*/	friend MACVector3<T> operator*	(const MACVector3<T>& v, const MACRotationMatrix3<T>& m);
		/*template <class T>*/	friend MACVector3<T> operator*	(const MACRotationMatrix3<T>& m, const MACVector3<T>& v);
#endif
		
	protected:

		// TEMPORARY FIX FOR LARRY - ICE 10th April 2001
		//friend std::istream& operator>>(std::istream&, MACRotationMatrix3<T>&);
		//friend std::ostream& operator<<(std::ostream&, MACRotationMatrix3<T>&);
};

//
// End of Class Definitions
//
//****************************************************************************************
//****************************************************************************************



//****************************************************************************************
//****************************************************************************************
//
// Templated Method Code
//



//********************************************
//
// Constructors
//
// Notes:
//

template<class T>
MACRotationMatrix3<T>::MACRotationMatrix3	(const MACVector3<T>& up, const MACVector3<T>& dir)
{
	this->set(up, dir);
}

//
// End of of Constructors 
//
//********************************************





//********************************************
//
// Set and Get operations
//
// Notes:
//

//
// Notes:
//		up vector and direction
// log:
//		created: 3/28/2001, Ian Elsley
//
template<class T> MACRotationMatrix3<T>&
MACRotationMatrix3<T>::set	(const MACVector3<T>& up, const MACVector3<T>& dir)
{
	z = dir;						// set unit direction
	z.normalize();
	
	y = up - (z * dot(up, z));		// set unit up, less the z component
	y.normalize();

	x = cross(y, z);				// set x vector

	return *this
}


//
// Notes:
//		up vector and direction
// log:
//		created: 3/28/2001, Ian Elsley
//
template<class T> MACRotationMatrix3<T>&
MACRotationMatrix3<T>::set	(const MACRotationMatrix3<T>& v)
{
	x =	v.I();
	y = v.J();
	z =	v.K();

	return *this;
}


//
// Notes:
//		up vector and direction
// log:
//		created: 7/22/2001, Ian Elsley
//
template<class T> MACRotationMatrix3<T>&
MACRotationMatrix3<T>::set	(const MACHomogeneousMatrix4<T>& v)
{
	x =	v.I();
	y = v.J();
	z =	v.K();

	return *this;
}


//
// Notes:
//		Ken Shoemake's recommended algorithm.
// Log:
//		Created: 4/9/01, Ian Elsley
//
template<class T> MACRotationMatrix3<T>&
MACRotationMatrix3<T>::set	(const MACEulerRotation<T>& v)
{
    double					ri, rj, rh;
	T						ci, cj, ch, 
							si, sj, sh; 
	T						cc, cs, sc, ss;
    unsigned int			i, j, k, h, n, s, f;
	MACRotationMatrix3<T>	&m = *this;
    
	v.getOrd(i, j, k, h, n, s, f);
    
	if (f == MACEulFrmR) 
	{
		ri = (double)v.H(); 
		rj = (double)v.J(); 
		rh = (double)v.I();
	}
	else
	{
		ri = (double)v.I(); 
		rj = (double)v.J(); 
		rh = (double)v.H();
	}

	if (n == MACEulParOdd) 
	{
		ri = -ri; 
		rj = -rj; 
		rh = -rh;
	}

    ci = (T)cos(ri);	cj = (T)cos(rj);	ch = (T)cos(rh);
    si = (T)sin(ri);	sj = (T)sin(rj);	sh = (T)sin(rh);
    
	cc = ci*ch;			
	cs = ci*sh; 
	sc = si*ch; 
	ss = si*sh;

    if (s == MACEulRepYes) 
	{
		m[i][i] = cj;	  m[j][i] =  sj*si;    m[k][i] =  sj*ci;
		m[i][j] = sj*sh;  m[j][j] = -cj*ss+cc; m[k][j] = -cj*cs-sc;
		m[i][k] = -sj*ch; m[j][k] =  cj*sc+cs; m[k][k] =  cj*cc-ss;
    } 
	else 
	{
		m[i][i] = cj*ch; m[j][i] = sj*sc-cs; m[k][i] = sj*cc+ss;
		m[i][j] = cj*sh; m[j][j] = sj*ss+cc; m[k][j] = sj*cs-sc;
		m[i][k] = -sj;	 m[j][k] = cj*si;    m[k][k] = cj*ci;
    }

	return *this;
}



//
// Notes:
//		Standard conversion to rotation matrix
// log:
//		created: 3/28/2001, Ian Elsley
//
template<class T> MACRotationMatrix3<T>&
MACRotationMatrix3<T>::set	(const MACAngleAxisRotation<T>& v)
{
	T	v1			= v.X(),					/* vector values */
		v2			= v.Y(),
		v3			= v.Z();
	T	v1s			= v1*v1,					/* squares */
		v2s			= v2*v2,
		v3s			= v3*v3;

	T	cosTheta	= (T)cos((double)v.A()),	/* Casting my cycles away... */
		sinTheta	= (T)sin((double)v.A());

	x[0] = v1s								+ (((T)1.0-v1s) * cosTheta);
	x[1] = ((v1*v2) * ((T)1.0-cosTheta))	+ (v3*sinTheta);
	x[2] = ((v1*v3) * ((T)1.0-cosTheta))	- (v2*sinTheta);

	y[0] = ((v1*v2) * ((T)1.0-cosTheta))	- (v3*sinTheta);
	y[1] = v2s								+ (((T)1.0-v2s) * cosTheta);
	y[2] = ((v2*v3) * ((T)1.0-cosTheta))	+ (v1*sinTheta);

	z[0] = ((v1*v3) * ((T)1.0-cosTheta))	+ (v2*sinTheta);
	z[1] = ((v2*v3) * ((T)1.0-cosTheta))	- (v1*sinTheta);
	z[2] = v3s								+ (((T)1.0-v3s) * cosTheta);

	return *this;
}


//
// Notes:
//		Normalizes the quaternion as a natural effect of collection of terms
// log:
//		created: 3/28/2001, Ian Elsley
//
/*template<class T> MACRotationMatrix3<T>&
MACRotationMatrix3<T>::set	(const MACQuaternion<T>& v)
{
	T xv	=	v.X();
    T yv	=	v.Y();
    T zv	=	v.Z();
    T wv	=	v.W();
    
	T tx	=	xv*xv;
	T ty	=	yv*yv;	  
	T tz	=	zv*zv;	  
	T tq	=	ty+tz;
	
	T tk	=	tq + tx + (wv * wv);
	
	tk		=	(tk > (T)0) ? ((T)2)/tk : (T)0;
	
	x[0]	=	1 - (tk * tq); 
	y[1]	=	1 - (tk * (tx + tz));
	z[2]	=	1 - (tk * (tx + ty));
	
	tx		=	tk * xv; 	 
	ty		=	tk * yv;	  
	tq		=	(tk * zv) * wv;
	tk		=	tx * yv;
	x[1]	=	tk - tq;  
	y[0]	=	tk + tq;
	
	tq		=	ty * wv;
	tk		=	tx * zv;
	x[2]	=	tk + tq; 	
	z[0]	=	tk - tq;
	
	tq		=	tx * wv;	  
	tk		=	ty * zv;
	y[2]	=	tk - tq;	  
	z[2]	=	tk + tq; 
	
	return *this;
}*/


template<class T> MACRotationMatrix3<T>&
MACRotationMatrix3<T>::set	(const MACQuaternion<T>& v)
{
    T xs, ys, zs, wx, wy, wz, xx, xy, xz, yy, yz, zz;

    T xv = v.X();
    T yv = v.Y();
    T zv = v.Z();
    T wv = v.W();
    
    T Nq = (xv * xv) + (yv * yv) + (zv * zv) + (wv * wv);
    T s  = (Nq > 0.0) ? ((T)2.0 / Nq) : (T)0.0;

    xs = xv * s;	ys = yv * s;	zs = zv * s;
    wx = wv * xs;	wy = wv * ys;	wz = wv * zs;
    xx = xv * xs;	xy = xv * ys;	xz = xv * zs;
    yy = yv * ys;	yz = yv * zs;	zz = zv * zs;
    
    x[0] = (T)1.0 - (yy + zz);
    x[1] = xy + wz;
    x[2] = xz - wy;

    y[0] = xy - wz;
    y[1] = (T)1.0 - (xx + zz);
    y[2] = yz + wx;

    z[0] = xz + wy;
    z[1] = yz - wx;
    z[2] = (T)1.0 - (xx + yy);

	return *this;
}
/*
template<class T> MACRotationMatrix3<T>&
MACRotationMatrix3<T>::set	(const MACQuaternion<T>& v)
{
    T xs, ys, zs, wx, wy, wz, xx, xy, xz, yy, yz, zz;

    T xv = v.X();
    T yv = v.Y();
    T zv = v.Z();
    T wv = v.W();
    
    xs = xv * (T)2;	ys = yv * (T)2;	zs = zv * (T)2;
    wx = wv * xs;	wy = wv * ys;	wz = wv * zs;
    xx = xv * xs;	xy = xv * ys;	xz = xv * zs;
    yy = yv * ys;	yz = yv * zs;	zz = zv * zs;
    
    x[0] = 1.0 - yy - zz;
    x[1] = xy + wz;
    x[2] = xz - wy;

    y[0] = xy - wz;
    y[1] = 1.0 - xx - zz;
    y[2] = yz + wx;

    z[0] = xz + wy;
    z[1] = yz - wx;
    z[2] = 1.0 - xx - yy;

	return *this;
}*/


// 
// End of Set and Get
//
//********************************************


//********************************************
//
// General Arithmetic Assignment Operations
//
// Notes:
//

//
// Notes:
//		sets current matrix
// log:
//		created: 3/28/2001, Ian Elsley
//
template<class T> MACRotationMatrix3<T>&	
MACRotationMatrix3<T>::operator*=	(const MACRotationMatrix3<T>& v)
{
	int i, j, k;
	MACRotationMatrix3<T> &result	= *this;
	MACRotationMatrix3<T> v0		= *this;

	for (i=0; i<3; i++) {
		for (j=0; j<3; j++) {
			result[j][i] = (T)0;
			for (k=0; k<3; k++) {
				result[j][i] += v0[k][i]*v[j][k];
			}
		}
	}

	return *this;
}
	
//
// Notes:
//		returns result without altering current matrix
// log:
//		created: 3/28/2001, Ian Elsley
//
template<class T> MACRotationMatrix3<T>	
MACRotationMatrix3<T>::operator*	(const MACRotationMatrix3<T>& v) const
{
	int i, j, k;
	MACRotationMatrix3<T>	result;

	for (i=0; i<3; i++) {
		for (j=0; j<3; j++) {
			result[j][i] = (T)0;
			for (k=0; k<3; k++) {
				result[j][i] += (*this)[k][i]*v[j][k];
			}
		}
	}

	return result;
}



//
// Notes:
// log:
//		Created: 7/17/2001, Ian Elsley
//
template<class T> T	
MACRotationMatrix3<T>::determinant	()
{
	MACRotationMatrix3<T> &m			= *this;
	
	T det1 = ((m[1][1] * m[2][2]) - (m[1][2] * m[2][1]));
	T det2 = ((m[0][2] * m[2][1]) - (m[0][1] * m[2][2]));
	T det3 = ((m[0][1] * m[1][2]) - (m[0][2] * m[1][1]));

	return (m[0][0] * det1) + (m[1][0] * det2) + (m[2][0] * det3);
}


//********************************************
//
// General Arithmetic Operations
//
// Notes:
//

//
// Notes:
//		Changes values in the class
// log:
//		Created: 3/28/2001, Ian Elsley
//
template<class T> MACRotationMatrix3<T>&	
MACRotationMatrix3<T>::invert	()
{
	MACRotationMatrix3<T> m			= *this;
	MACRotationMatrix3<T> &result	= *this;	// for convenience
	
	auto T det1 = ((m[1][1] * m[2][2]) - (m[1][2] * m[2][1]));
	auto T det2 = ((m[0][2] * m[2][1]) - (m[0][1] * m[2][2]));
	auto T det3 = ((m[0][1] * m[1][2]) - (m[0][2] * m[1][1]));

	auto T det = (m[0][0] * det1) + (m[1][0] * det2) + (m[2][0] * det3);

	if (det != (T)0)
	{

		// If this is failing, set an identity matrix
		if (fabs((double)1.0/(double)det) < MAC_MATRIX_INVERSION_PRECISION_LIMIT)
		{
			return this->setIdentity();
		}
		else
		{
			result[0][0] = det1 / det;
			result[1][0] = ((m[2][0] * m[1][2]) - (m[1][0] * m[2][2])) / det;
			result[2][0] = ((m[1][0] * m[2][1]) - (m[2][0] * m[1][1])) / det;
			result[0][1] = det2 / det;
			result[1][1] = ((m[0][0] * m[2][2]) - (m[2][0] * m[0][2])) / det;
			result[2][1] = ((m[2][0] * m[0][1]) - (m[0][0] * m[2][1])) / det;
			result[0][2] = det3 / det;
			result[1][2] = ((m[1][0] * m[0][2]) - (m[0][0] * m[1][2])) / det;
			result[2][2] = ((m[0][0] * m[1][1]) - (m[1][0] * m[0][1])) / det;
		}
	}
	
	return *this;
}

//
// Notes:
//		Changes values in the class
// log:
//		Created: 3/28/2001, Ian Elsley
//
template<class T> MACRotationMatrix3<T>	
MACRotationMatrix3<T>::theInverse	()	const
{
	MACRotationMatrix3<T> result	= *this;

	result.invert();

	return result;
}

//
// Notes:
//		Changes values in the class
// log:
//		Created: 3/28/2001, Ian Elsley
//
template<class T> MACRotationMatrix3<T>&	
MACRotationMatrix3<T>::transpose	()
{
	auto T temp;

	temp = x[1];	x[1] = y[0];	y[0] = temp;
	temp = x[2];	x[2] = z[0];	z[0] = temp;
	temp = y[2];	y[2] = z[1];	z[1] = temp;
	
	return *this;
}

//
// Notes:
//		Changes values in the class
// log:
//		Created: 3/28/2001, Ian Elsley
//
template<class T> MACRotationMatrix3<T>
MACRotationMatrix3<T>::theTranspose	()	const
{
	MACRotationMatrix3<T> result = *this;
	return result.transpose();
}


//********************************************
//
// Comparitor Functions
//
//

// 
// MACRotationMatrix3<T>::equal(const MACRotationMatrix3<T>& v, T error = ERROR_VALUE)	const
// Notes:
// Log:
//		Created 3/28/01, Ian Elsley
//
template<class T> inline bool
MACRotationMatrix3<T>::equal(const MACRotationMatrix3<T>& v, T error)	const
{
	return (x.equal(v.x, error) && y.equal(v.y, error) && z.equal(v.z, error));
}


//
// MACRotationMatrix3<T>::operator==(const MACRotationMatrix3<T>& v) const
// Notes:
//		This function may not work entirely as expected. 
//		There may be an error factor required.
// Log:
//		Created 3/28/01, Ian Elsley
//
template<class T> inline bool
MACRotationMatrix3<T>::operator==(const MACRotationMatrix3<T>& v)	const
{
	return ((x==v.x) && (y==v.y) && (z==v.z));
}


//
// MACRotationMatrix3<T>::operator!=(const MACRotationMatrix3<T>& v) const
// Notes:
// Log:
//		Created 3/28/01, Ian Elsley
//
template<class T> inline bool
MACRotationMatrix3<T>::operator!=(const MACRotationMatrix3<T>& v)	const
{
	return (!((*this) == v));
}


//********************************************
//
// Identity code
//
//		Created 3/28/01, Ian Elsley
//

template<class T> MACRotationMatrix3<T>&
MACRotationMatrix3<T>::setIdentity	()
{
	x.set((T)1, (T)0, (T)0);
	y.set((T)0, (T)1, (T)0);
	z.set((T)0, (T)0, (T)1);

	return *this;
}


//********************************************
//
// OpenGL helper functions
//

//
// Notes:
// Log:
//		Created: 4/10/2001, Ian Elsley
//
template<class T>	MACRotationMatrix3<T>&
MACRotationMatrix3<T>::setFromGLMatrix	(const GLfloat glMatrix[16])
{
	int						i, j;
	MACRotationMatrix3<T>	&localMatrix = *this;

	for (i=0; i<3; i++)
		for (j=0; j<3; j++)
			glMatrix[(i*4)+j] = (GLfloat)localMatrix[i][j];

	return *this;
}

//
// Notes:
// Log:
//		Created: 4/10/2001, Ian Elsley
//
template<class T>	void
MACRotationMatrix3<T>::getGLMatrix	(GLfloat glMatrix[16])
{
	int						i, j;
	MACRotationMatrix3<T>	&localMatrix = *this;

	for (i=0; i<3; i++)
		for (j=0; j<3; j++)
			localMatrix[i][j] = (T)glMatrix[(i*4)+j];
}



//
// End of Method Code
//
//****************************************************************************************
//****************************************************************************************


//****************************************************************************************
//****************************************************************************************
//
// Start of Function Code
//


//***********************************************************************
//***********************************************************************
//
// Arithmetic Functions
//
// Notes:
//

//
// Notes:
// Log:
//		Created: 4/10/2001, Ian Elsley
//
template<class T>	MACVector3<T>
operator* (const MACVector3<T>& v, const MACRotationMatrix3<T>& m)
{
	return MACVector3<T>(v.dot3(m[0]), v.dot3(m[1]), v.dot3(m[2]));
}

//
// Notes:
// Log:
//		Created: 4/10/2001, Ian Elsley
//
template<class T>	MACVector3<T>
operator* (const MACRotationMatrix3<T>& m, const MACVector3<T>& v)
{
	return MACVector3<T>((m[0][0]*v[0]) + (m[1][0]*v[1]) + (m[2][0]*v[2]),
						(m[0][1]*v[0]) + (m[1][1]*v[1]) + (m[2][1]*v[2]),
						(m[0][2]*v[0]) + (m[1][2]*v[1]) + (m[2][2]*v[2]));
}

//
// End of Artithmetic Funtions
//
//***********************************************************************
//***********************************************************************


//***********************************************************************
//***********************************************************************
//
// I/O Functions
//
// Notes:
//

//
// operator>>
// Notes:
// Log:
//		Created 2/27/01, Ian Elsley
//
template<class T> std::istream& 
operator>>(std::istream& in, MACRotationMatrix3<T>& v)
{

	return in >> x >> y >> z;
}

//
// operator<<
// Notes:
// Log:
//		Created 2/27/01, Ian Elsley
//
template<class T> std::ostream& 
operator<<(std::ostream& out, MACRotationMatrix3<T>& v)
{
	return out << x << ' ' << y << ' ' << z;
}

//
// End of Function Code
//
//****************************************************************************************
//****************************************************************************************



MAC_MATH_LIB_END

#endif __MAC_ROTATION_MATRIX_3_H__