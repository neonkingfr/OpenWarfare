#pragma once
#include <iostream>
#include <cmath>
#include "cPoint.h"
#include "cVector.h"
#include "cMatrixTransform.h"
#include "eEulerOrder.h"
#include "Utils.h"



namespace TalonViewerLib
{
	/* Transform */
	class cSkeletonTransform
	{
	public:
		cSkeletonTransform(eEulerOrder Order = EULER_ZYX);//identity
		cSkeletonTransform(const cSkeletonTransform & src);
		cSkeletonTransform(float TX, float TY, float TZ, eEulerOrder Order, float Angle1, float Angle2, float Angle3, float SX, float SY, float SZ);

		/* No-data indication */
		void MakeEmpty();
		bool IsEmpty() const;

		/* Modification */
		void SetRotation(eEulerOrder Order, float Angle1, float Angle2, float Angle3);
		void SetTranslation(float TX, float TY, float TZ);
		void SetScale(float SX, float SY, float SZ);
		void Set(float TX, float TY, float TZ, eEulerOrder Order, float Angle1, float Angle2, float Angle3, float SX, float SY, float SZ);
		void Identity();

		/* Access */
		void GetRotation(eEulerOrder &Order, float &Angle1, float &Angle2, float &Angle3) const;
		void GetTranslation(float &TX, float &TY, float &TZ) const;
		cVector GetTranslation() const;
		void GetScale(float &SX, float &SY, float &SZ) const;
		void Get(float &TX, float &TY, float &TZ, eEulerOrder &Order, float &Angle1, float &Angle2, float &Angle3, float &SX, float &SY, float &SZ) const;
		
		/* Some Matrix Access shortcuts - These are calculated on each call, so cache when appropriate */
		cMatrixTransform GetMatrix() const;
		cMatrixTransform GetTranslationMatrix() const;
		cMatrixTransform GetRotationMatrix() const;
		cMatrixTransform GetScaleMatrix() const;

		/* Operators */
		cSkeletonTransform &operator=(const cSkeletonTransform &src);


	private:
		/* This is the full transform */
		float m_ScaleX, m_ScaleY, m_ScaleZ;
		float m_TransX, m_TransY, m_TransZ;
		eEulerOrder m_Order;
		float m_Angle1, m_Angle2, m_Angle3;
	};
}
