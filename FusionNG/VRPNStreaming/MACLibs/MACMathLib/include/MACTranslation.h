//******************************************************************************************************
//******************************************************************************************************
//
// Filename:	MACTranslation.h
// Author:		Ian Elsley
// Created:		26th Feb, 2001
//
//
// Documentation:
//		Generalised abstract translation header class.
//		This will be used by all other translation types as the generic header class.
//		In the main, all accesses to these classes will be via this.
//
//
//
// Log:
//		Created 3/1/2001, Ian Elsley
//
// Notes:
//
//		Failure is currently through assert though there will be throws. Users should not use local memory
//		pointers but should use the inbuilt ptr<T> forms instead to ensure cleanup on rewind.
//
// Copyright (c) 2001 Motion Analysis Corportation.
// All rights reserved.
//
//
// This copyright needs updating, but this code is internal to MAC. Do not use, copy, look at or breathe.
//
//******************************************************************************************************
//******************************************************************************************************

#ifndef __MAC_TRANSLATION_H__
#define __MAC_TRANSLATION_H__

MAC_MATH_LIB_BEGIN


//*********************************************************************
//*********************************************************************
//
// Instanciated Vectors
//
// Notes:
// Log:
//		Created 3/5/01, Ian Elsley
//
typedef MACTranslation<float>	MACTransf;
typedef MACTranslation<double>	MACTransd;
typedef MACTranslation<int>		MACTransi;
//
// End of Instanciated Translations
//
//*********************************************************************
//*********************************************************************



//*********************************************************************
//*********************************************************************
//
// Start of templated class definitions
//

//
// MACTranslation<T>
// Notes: 
//		Generalised Abstract Class for use as a container for all forms of Translation.
//		No variables can be defined within this.
//
template<class T>
class MACTranslation : public MACTransform<T> 
{

	private:
		
		MACVector3<T>	translation;


	public:

		MACTranslation<T>	()										: translation((T)0) {}
		MACTranslation<T>	(const T& v)							: translation(v) {}
		MACTranslation<T>	(const T& vx, const T& vy, const T& vz)	: translation(vx, vy, vz) {}
		MACTranslation<T>	(const MACVector<T>& v)					: translation(v) {}

		virtual	~MACTranslation<T>	()								{}

	public:		/*** Access - Set and Get ***/

		
		// Standard array access
		T&			operator[]	(const unsigned int i)				{ return translation[i]; }
		const T&	operator[]	(const unsigned int i) const		{ return translation[i]; }

	
		// Get the values	
		T			X()	const										{ return translation.X(); }
		T			Y()	const										{ return translation.Y(); }
		T			Z()	const										{ return translation.Z(); }
																	/* This should perhaps fail with an assert */

		// Set the values
		T			X(T v)											{ return translation.X(v); }
		T			Y(T v)											{ return translation.Y(v); }
		T			Z(T v)											{ return translation.Z(v); }
																	/* This should perhaps fail with an assert */

		// Set the values in a group
		MACTranslation<T>&	set	(const T& v)						{ translation.set(v); return *this }
		MACTranslation<T>&	set	(const T& vx, const T& vy, const T& vz)	
																	{ translation.set(vx, vy, vz); return *this; }
		MACTranslation<T>&	set	(const MACVector<T>& v)				{ translation.set(v); return *this; }

	public:		/*** Comparitors ***/

		bool		equal		(const MACTranslation<T>& v, T error = ERROR_VALUE)	const;
		bool		operator==	(const MACTranslation<T>& v)							const;
		bool		operator!=	(const MACTranslation<T>& v)							const;

	
	public:		/*** Identity Functions ***/

		MACTranslation<T>	identity	()							{ return MACTranslation<T>((T)0); }
		MACTranslation<T>&	setIdentity	()							{ translation.set(identity()); return *this; }
		bool				isIdentity	()							{ return translation.equal(identity()); }


	protected:	/*** IO functions ***/

		friend std::istream& operator>>(std::istream&, MACTranslation<T>&);
		friend std::ostream& operator<<(std::ostream&, MACTranslation<T>&);
		// Note instanciation of the base class will have the extra effect of correctly instaciating these

};

//
// End of templated class definitions
//
//*********************************************************************
//*********************************************************************


//*********************************************************************
//*********************************************************************
//
// Templated Method Code
//

//***************************************
//
// Comparitor Functions
//
//

// 
// MACTranslation<T>::equal(const MACTranslation<T>& v, T error = ERROR_VALUE)	const
// Notes:
//		Allied with this function are the explicit instanciations of the float and double cases
//		These are identical, but are implemented like this for speed.
// Log:
//		Created 2/27/01, Ian Elsley
//
template<class T> inline bool
MACTranslation<T>::equal(const MACTranslation<T>& r, T error)					const
{
	return (((T)fabs((double)(X()-r.X())) < error) && 
			((T)fabs((double)(Y()-r.Y())) < error) && 
			((T)fabs((double)(Z()-r.Z())) < error));
}

//
// Equal
// Notes:
//		Double version
// Log:
//		Created 2/27/01, Ian Elsley
//
inline bool
MACTranslation<double>::equal(const MACTranslation<double>& r, double error)	const
{
	return ((fabs(X()-r.X()) < error) && 
			(fabs(Y()-r.Y()) < error) && 
			(fabs(Z()-r.Z()) < error));
}

//
// Equal
// Notes:
//		Float version
// Log:
//		Created 2/27/01, Ian Elsley
//
inline bool
MACTranslation<float>::equal(const MACTranslation<float>& r, float error)		const
{
	return ((fabsf(X()-r.X()) < error) && 
			(fabsf(Y()-r.Y()) < error) && 
			(fabsf(Z()-r.Z()) < error));
}


//
// MACTranslation<T>::operator==(const MACTranslation<T>& v) const
// Notes:
//		This function may not work entirely as expected. 
//		There may be an error factor required.
// Log:
//		Created 2/27/01, Ian Elsley
//
template<class T> inline bool
MACTranslation<T>::operator==(const MACTranslation<T>& v)						const
{
	return ((X()==v.X()) && (Y()==v.Y()) && (Z()==v.Z()));
}


//
// MACTranslation<T>::operator!=(const MACTranslation<T>& v) const
// Notes:
// Log:
//		Created 2/27/01, Ian Elsley
//
template<class T> inline bool
MACTranslation<T>::operator!=(const MACTranslation<T>& v)						const
{
	return (!((*this) == v));
}


//
// End of Templated Method Code
//
//*********************************************************************
//*********************************************************************


//*********************************************************************
//*********************************************************************
//
// Start of Function Code
//


//***************************************
//
// I/O Functions
//
// Notes:
//

//
// operator>>
// Notes:
// Log:
//		Created 2/27/01, Ian Elsley
//
template<class T> std::istream& 
operator>>(std::istream& in, MACTranslation<T>& v)
{
	return in >> v;
}

//
// operator<<
// Notes:
// Log:
//		Created 2/27/01, Ian Elsley
//
template<class T> std::ostream& operator<<(std::ostream& out, MACTranslation<T>& v)
{
	return out << v;
}

//
// End of Function Code
//
//*********************************************************************
//*********************************************************************


MAC_MATH_LIB_END

#endif __MAC_TRANSLATION_H__