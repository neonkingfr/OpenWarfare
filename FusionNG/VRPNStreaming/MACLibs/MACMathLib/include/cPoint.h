#pragma once
#include "cVector.h"


namespace TalonViewerLib
{
	/* Simple point class */
	class cPoint
	{
	public:
		/* Constructor */
		cPoint(); //0,0,0
		cPoint(float x, float y, float z);

		/* Access */
		float X() const { return m_X; }
		float Y() const { return m_Y; }
		float Z() const { return m_Z; }

		/* Modify */
		void Set(float X, float Y, float Z);
		void X(float val){ m_X = val; }
		void Y(float val){ m_Y = val; }
		void Z(float val){ m_Z = val; }

		/* Operators */
		cVector operator-(const cPoint &RHS) const;


	private:
		/* The vector */
		float m_X, m_Y, m_Z;
	};
}