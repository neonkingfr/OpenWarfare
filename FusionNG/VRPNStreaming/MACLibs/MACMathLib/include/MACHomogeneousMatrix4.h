//******************************************************************************************************
//******************************************************************************************************
//
// Filename:	MACHomogeneousMatrix4.h
// Author:		Ian Elsley
// Created:		17th, July, 2001
//
//
// Documentation:
//		 Homogeneous Matrix Template
//		
//
// Log:
//		Created 7/17/2001, Ian Elsley
//
// Notes:
//
//		Failure is currently through assert though there will be throws. Users should not use local memory
//		pointers but should use the inbuilt ptr<T> forms instead to ensure cleanup on rewind.
//
// Copyright (c) 2001 Motion Analysis Corportation.
// All rights reserved.
// 
//
// This copyright needs updating, but this code is internal to MAC. Do not use, copy, look at or breathe.
//
//******************************************************************************************************
//******************************************************************************************************


#ifndef __MAC_HOMOGENEOUS_MATRIX_4_H__
#define __MAC_HOMOGENEOUS_MATRIX_4_H__

#include <windows.h>
#include <iostream>

#include <gl/gl.h>
#include "MACVector4.h"
#include "MACMatrix.h"

MAC_MATH_LIB_BEGIN


//****************************************************************************************
//****************************************************************************************
//
// Instanciated Vectors
//
// Notes:
// Log:
//		Created 7/17/01, Ian Elsley
//

typedef	MACHomogeneousMatrix4<double>	MACHomM4d;
typedef MACHomogeneousMatrix4<float>	MACHomM4f;


//
// End of instanciation
//
//****************************************************************************************
//****************************************************************************************


//****************************************************************************************
//****************************************************************************************
//
// Class Definition
//

template<class T>
class MACHomogeneousMatrix4 : public MACTransform<T>
{

	private:	/*** Variables ***/

		
		MACVector4<T>			x;
		MACVector4<T>			y;
		MACVector4<T>			z;
		MACVector4<T>			t;


	public:		/*** Constructors and Destructors ***/

		
		// Constructors from set values
		MACHomogeneousMatrix4<T>	()	:	x((T)1, (T)0, (T)0, (T)0), 
											y((T)0, (T)1, (T)0, (T)0), 
											z((T)0, (T)0, (T)1, (T)0), 
											t((T)0, (T)0, (T)0, (T)1) {}

		MACHomogeneousMatrix4<T>	(const MACVector<T>& side,	const MACVector<T>& up,
									 const MACVector<T>& dir,	const MACVector<T>& trans)
										:	x(side), y(up), z(dir),	t(trans) {}
		
		MACHomogeneousMatrix4<T>	(const MACVector<T>& up, const MACVector<T>& dir);
		MACHomogeneousMatrix4<T>	(const MACTranslation<T>& v)			{ set(v); }	
		MACHomogeneousMatrix4<T>	(const MACScale<T>& v)				{ set(v); }	

		// Constructors from other Classes
		MACHomogeneousMatrix4<T>	(const MACHomogeneousMatrix4<T>& v)	
										:	x(v.x), y(v.y), z(v.z), t(v.t)  {}

		MACHomogeneousMatrix4<T>	(const MACRotationMatrix3<T>& v)	
										:	x(v.x), y(v.y), z(v.y), t((T)0, (T)0, (T)0, (T)1)  {}

		MACHomogeneousMatrix4<T>	(const MACEulerRotation<T>& v)		{ set(v); }
		MACHomogeneousMatrix4<T>	(const MACAngleAxisRotation<T>& v)	{ set(v); }
		MACHomogeneousMatrix4<T>	(const MACQuaternion<T>& v)			{ set(v); }

		virtual ~MACHomogeneousMatrix4<T> ()							{}

	
	public:		/*** Set and Get ***/

		
		// Standard array access
		MACVector4<T>&				operator[]	(const unsigned int i)				{ return (&x)[i]; }
		const MACVector4<T>&		operator[]	(const unsigned int i) const		{ return (&x)[i]; }

	
		// Get
		const MACVector4<T>&		MACde		()	const				{ return x; }
		const MACVector4<T>&		Up			()	const				{ return y; }
		const MACVector4<T>&		Dir			()	const				{ return z; }
		const MACVector4<T>&		Trans		()	const				{ return t; }
	
		const MACVector4<T>&		I			()	const				{ return x; }
		const MACVector4<T>&		J			()	const				{ return y; }
		const MACVector4<T>&		K			()	const				{ return z; }

		// Set
		MACHomogeneousMatrix4<T>&	set	(const MACVector<T>& side,	const MACVector<T>& up, 
										 const MACVector<T>& dir,	const MACVector<T>& trans)
											{ x.set(side); y.set(up); z.set(dir); t.set(trans); return *this; }
		MACHomogeneousMatrix4<T>&	set	(const MACVector<T>& up, const MACVector<T>& dir);
		MACHomogeneousMatrix4<T>&	set	(const MACTranslation<T>& v)
											{ setIdentity(); t.set(v.X(), v.Y(), v.Z(), (T)1); return *this; }
		MACHomogeneousMatrix4<T>&	set	(const MACScale<T>& v)
											{ setIdentity(); x[0]=v.X(); y[1]=v.Y(); z[2]=v.Z(); return *this; }
		
		// shared set
		MACHomogeneousMatrix4<T>&	set	(const MACHomogeneousMatrix4<T>& v);
		MACHomogeneousMatrix4<T>&	set	(const MACRotationMatrix3<T>& v);
		MACHomogeneousMatrix4<T>&	set	(const MACEulerRotation<T>& v);
		MACHomogeneousMatrix4<T>&	set	(const MACAngleAxisRotation<T>& v);
		MACHomogeneousMatrix4<T>&	set	(const MACQuaternion<T>& v);


	public:		/*** General Arithmetic functions ***/

	
		MACHomogeneousMatrix4<T>&	invertGeneral	();
		MACHomogeneousMatrix4<T>&	invertAffine	();
		MACHomogeneousMatrix4<T>&	transpose		();
		MACHomogeneousMatrix4<T>&	invert			();
		void						decomposeMatrix (	MACRotationMatrix3<T>&	rotate, 
														MACTranslation<T>&		translate, 
														MACScale<T>&			scale )		const;

		// Note: Returns value as a seperate matrix. Does not affect this one.
		MACHomogeneousMatrix4<T>		theInverse		()	const;
		MACHomogeneousMatrix4<T>		theTranspose	()	const;

	
	public:		/*** General Arithmetic operators ***/


		// Assignment
		MACHomogeneousMatrix4<T>&	operator=	(const MACHomogeneousMatrix4<T>& r)	{ return this->set(r); }
		MACHomogeneousMatrix4<T>&	operator*=	(const MACHomogeneousMatrix4<T>& v);
		MACHomogeneousMatrix4<T>&	operator=	(const MACRotationMatrix3<T>& r)		{ return this->set(r); }
		MACHomogeneousMatrix4<T>&	operator*=	(const MACRotationMatrix3<T>& v);
		MACHomogeneousMatrix4<T>&	operator=	(const MACScale<T>& r)				{ return this->set(r); }
		MACHomogeneousMatrix4<T>&	operator*=	(const MACScale<T>& v);
		MACHomogeneousMatrix4<T>&	operator/=	(const MACScale<T>& v);
		MACHomogeneousMatrix4<T>&	operator=	(const MACTranslation<T>& r)	{ return this->set(r); }
		MACHomogeneousMatrix4<T>&	operator+=	(const MACTranslation<T>& v);
		MACHomogeneousMatrix4<T>&	operator*=	(const MACTranslation<T>& v);
		
		// Arithmetic
		MACHomogeneousMatrix4<T>		operator*	(const MACHomogeneousMatrix4<T>& v)	const;
		MACHomogeneousMatrix4<T>		operator*	(const MACRotationMatrix3<T>& v)		const;
		MACHomogeneousMatrix4<T>		operator*	(const MACScale<T>& v)				const;
		MACHomogeneousMatrix4<T>		operator/	(const MACScale<T>& v)				const;
		MACHomogeneousMatrix4<T>		operator+	(const MACTranslation<T>& v)			const;
		MACHomogeneousMatrix4<T>		operator*	(const MACTranslation<T>& v)			const;
		MACHomogeneousMatrix4<T>		operator-	()									const	{ return theInverse(); }
		
	
	public:		/*** Comparitors ***/


		bool	equal		(const MACHomogeneousMatrix4<T>& v, T error = MAC_MATRIX_ERROR_LIMIT)	const;
		bool	operator==	(const MACHomogeneousMatrix4<T>& v)								const;
		bool	operator!=	(const MACHomogeneousMatrix4<T>& v)								const;


	public:		/*** Identity ***/


		MACHomogeneousMatrix4<T>		identity	()			{ return MACHomogeneousMatrix4<T>(); }
		MACHomogeneousMatrix4<T>&		setIdentity	();
		bool							isIdentity	()			{ return equal(identity()); }


	public:		/*** Mirroring Functions ***/


		MACHomogeneousMatrix4<T>&	mirror		(const MACHomogeneousMatrix4<T>& planeRotation, 
												 const MACHomogeneousMatrix4<T>& inversePlaneRotation);
		MACHomogeneousMatrix4<T>&	mirror		(const MACVector3<T>& planeNormal, 
												 const MACVector3<T>& pointOnPlane);

	public:		/*** OpenGl code ***/

		MACHomogeneousMatrix4<T>&	setFromGLMatrix	(const GLfloat m[16]);
		void						getGLMatrix		(GLfloat m[16]);

	
	protected:	/*** Alied classes ***/

		friend class MACRotationMatrix3<T>;


	protected:	/*** Alied functions ***/

		//friend MACVector4<T> operator*	(const MACVector<T>& v, const MACHomogeneousMatrix4<T>& m);
		friend MACVector3<T> operator*	(const MACHomogeneousMatrix4<T>& m, const MACVector3<T>& v);
		friend MACVector4<T> operator*	(const MACHomogeneousMatrix4<T>& m, const MACVector4<T>& v);
		friend MACHomogeneousMatrix4<T> operator*
										(const MACScale<T>& s, const MACHomogeneousMatrix4<T>& m);
		friend MACHomogeneousMatrix4<T> operator/
										(const MACScale<T>& s, const MACHomogeneousMatrix4<T>& m);
	
	protected:

		friend std::istream& operator>>(std::istream&, MACHomogeneousMatrix4<T>&);
		friend std::ostream& operator<<(std::ostream&, MACHomogeneousMatrix4<T>&);
};

//
// End of Class Definitions
//
//****************************************************************************************
//****************************************************************************************



//****************************************************************************************
//****************************************************************************************
//
// Templated Method Code
//



//********************************************
//
// Constructors
//
// Notes:
//

template<class T>
MACHomogeneousMatrix4<T>::MACHomogeneousMatrix4	(const MACVector<T>& up, const MACVector<T>& dir)
{
	this->set(up, dir);
}

//
// End of of Constructors 
//
//********************************************





//********************************************
//
// Set and Get operations
//
// Notes:
//

//
// Notes:
//		up vector and direction
// log:
//		created: 7/17/2001, Ian Elsley
//
template<class T> MACHomogeneousMatrix4<T>&
MACHomogeneousMatrix4<T>::set	(const MACVector<T>& up, const MACVector<T>& dir)
{
	z.set(dir);							// set unit direction
	z.normalize();
	
	y = -(z * dot3(up, z)) + up ;		// set unit up, less the z component
	y.normalize();

	x = cross3(y, z);					// set x vector

	t.set((T)0, (T)0, (T)0, (T)1);

	return *this;
}


//
// Notes:
//		up vector and direction
// log:
//		created: 7/17/2001, Ian Elsley
//
template<class T> MACHomogeneousMatrix4<T>&
MACHomogeneousMatrix4<T>::set	(const MACHomogeneousMatrix4<T>& v)
{
	x =	v.x;
	y = v.y;
	z =	v.z;
	t = v.t;

	return *this;
}

//
// Notes:
//		up vector and direction
// log:
//		created: 7/17/2001, Ian Elsley
//
template<class T> MACHomogeneousMatrix4<T>&
MACHomogeneousMatrix4<T>::set	(const MACRotationMatrix3<T>& v)
{
	x =	v.x;
	y = v.y;
	z =	v.z;
	t.set((T)0, (T)0, (T)0, (T)1);

	return *this;
}


//
// Log:
//		Created: 4/9/01, Ian Elsley
//
template<class T> MACHomogeneousMatrix4<T>&
MACHomogeneousMatrix4<T>::set	(const MACEulerRotation<T>& v)
{
 	return set(MACRotationMatrix3<T>(v));
}


//
// Notes:
//		Standard conversion to rotation matrix
// log:
//		created: 7/17/2001, Ian Elsley
//
template<class T> MACHomogeneousMatrix4<T>&
MACHomogeneousMatrix4<T>::set	(const MACAngleAxisRotation<T>& v)
{
 	return set(MACRotationMatrix3<T>(v));
}


template<class T> MACHomogeneousMatrix4<T>&
MACHomogeneousMatrix4<T>::set	(const MACQuaternion<T>& v)
{
  	return set(MACRotationMatrix3<T>(v));
}

// 
// End of Set and Get
//
//********************************************


//********************************************
//
// General Arithmetic Assignment Operations
//
// Notes:
//

//
// Notes:
//		sets current matrix
// log:
//		created: 7/17/2001, Ian Elsley
//
template<class T> MACHomogeneousMatrix4<T>&	
MACHomogeneousMatrix4<T>::operator*=	(const MACHomogeneousMatrix4<T>& v)
{
	int i, j, k;
	MACHomogeneousMatrix4<T> &result		= *this;
	MACHomogeneousMatrix4<T> v0(*this);

	for (i=0; i<4; i++) {
		for (j=0; j<4; j++) {
			result[j][i] = (T)0;
			for (k=0; k<4; k++) {
				result[j][i] += v0[k][i]*v[j][k];
			}
		}
	}

	return *this;
}
	

//
// Notes:
//		sets current matrix
// log:
//		created: 7/17/2001, Ian Elsley
//
template<class T> MACHomogeneousMatrix4<T>&	
MACHomogeneousMatrix4<T>::operator*= (const MACRotationMatrix3<T>& v)
{
	MACHomogeneousMatrix4<T> newV(v);
	return (*this) *= newV;
}

	
//
// Notes:
//		sets current matrix
// log:
//		created: 7/17/2001, Ian Elsley
//
template<class T> MACHomogeneousMatrix4<T>&	
MACHomogeneousMatrix4<T>::operator*= (const MACScale<T>& v)
{
	auto int i, j;
	MACHomogeneousMatrix4<T> &result = *this;

	for (i=0; i<4; i++) {
		for (j=0; j<3; j++) {
			result[j][i] *= v[j];
		}
	}

	return *this;
}
	

//
// Notes:
//		sets current matrix
// log:
//		created: 7/17/2001, Ian Elsley
//
template<class T> MACHomogeneousMatrix4<T>&	
MACHomogeneousMatrix4<T>::operator/= (const MACScale<T>& v)
{
	auto int i, j;
	MACHomogeneousMatrix4<T> &result		= *this;

	for (i=0; i<4; i++) {
		for (j=0; j<3; j++) {
			result[j][i] /= v[j];
		}
	}

	return *this;
}
	

//
// Notes:
//		sets current matrix
// log:
//		created: 7/17/2001, Ian Elsley
//
template<class T> MACHomogeneousMatrix4<T>&	
MACHomogeneousMatrix4<T>::operator+= (const MACTranslation<T>& v)
{
	int i;

	for (i=0; i<3; i++)
		t[i] += v[i];

	return *this;
}

	
//
// Notes:
//		sets current matrix
// log:
//		created: 7/28/2001, Ian Elsley
//
template<class T> MACHomogeneousMatrix4<T>&	
MACHomogeneousMatrix4<T>::operator*= (const MACTranslation<T>& v)
{
	auto int i;
	MACHomogeneousMatrix4<T> &result		= *this;
	MACTranslation<T>		origT(Trans());

	for (i=0; i<3; i++)
	{
		// Note t[i] does not need to be added, multiplied by the implied value 1
		// as it is already there. MACm for result[3][3].
		// t[i] = result[3][i] (== t[i])
		//
		for (j=0, j<3; j++)
		{
			t[i] += result[j][i] * origT[j];
			// t[i] == result[3][i]
		}

		result[3][3] += result[i][3]*origT[i];
	}
	return *this;
}

	
//
// Notes:
//		returns result without altering current matrix
// log:
//		created: 7/17/2001, Ian Elsley
//
template<class T> MACHomogeneousMatrix4<T>	
MACHomogeneousMatrix4<T>::operator*	(const MACHomogeneousMatrix4<T>& v) const
{
	int i, j, k;
	MACHomogeneousMatrix4<T>	result;

	for (i=0; i<4; i++) {
		for (j=0; j<4; j++) {
			result[j][i] = (T)0;
			for (k=0; k<4; k++) {
				result[j][i] += (*this)[k][i]*v[j][k];
			}
		}
	}

	return result;
}


//
// Notes:
//		returns result without altering current matrix
// log:
//		created: 7/17/2001, Ian Elsley
//
template<class T> MACHomogeneousMatrix4<T>	
MACHomogeneousMatrix4<T>::operator*	(const MACRotationMatrix3<T>& v) const
{
	MACHomogeneousMatrix4<T>	newV;

	return (*this) * newV;
}



//
// Notes:
//		returns result without altering current matrix
// log:
//		created: 7/17/2001, Ian Elsley
//
template<class T> MACHomogeneousMatrix4<T>	
MACHomogeneousMatrix4<T>::operator*	(const MACScale<T>& v)	const
{
	int i, j;
	MACHomogeneousMatrix4<T> result;
	
	for (i=0; i<4; i++) {
		for (j=0; j<4; j++) {
			result[j][i] = (*this)[j][i] * ((j<3)?v[j]:(T)1);
		}
	}
	
	return result;
}


//
// Notes:
//		returns result without altering current matrix
// log:
//		created: 7/17/2001, Ian Elsley
//
template<class T> MACHomogeneousMatrix4<T>	
MACHomogeneousMatrix4<T>::operator/	(const MACScale<T>& v)	const
{
	int i, j;
	MACHomogeneousMatrix4<T> result;
	
	for (i=0; i<4; i++) {
		for (j=0; j<4; j++) {
			result[j][i] = (*this)[j][i] / ((j<3)?v[j]:(T)1);
		}
	}
	
	return result;
}



//
// Notes:
//		returns result without altering current matrix
// log:
//		created: 7/17/2001, Ian Elsley
//
template<class T> MACHomogeneousMatrix4<T>	
MACHomogeneousMatrix4<T>::operator+	(const MACTranslation<T>& v)	const
{
	int i;
	MACHomogeneousMatrix4<T> result(*this);

	for (i=0; i<3; i++)
		result.t[i] += v[i];

	return result;
}

//
// Notes:
//		returns result without altering current matrix
// log:
//		created: 7/27/2001, Ian Elsley
//
template<class T> MACHomogeneousMatrix4<T>	
MACHomogeneousMatrix4<T>::operator*	(const MACTranslation<T>& v)	const
{
	MACHomogeneousMatrix4<T> result(*this);

	
	return result *= v;
}

	

//********************************************
//
// General Arithmetic Operations
//
// Notes:
//

//
// Notes:
//		Changes values in the class
// log:
//		Created: 7/17/2001, Ian Elsley
//
template<class T> MACHomogeneousMatrix4<T>&	
MACHomogeneousMatrix4<T>::invertAffine	()
{
	MACHomogeneousMatrix4<T> m		= *this;
	MACHomogeneousMatrix4<T> &result	= *this;	// for convenience
	
	T det1 = ((m[1][1] * m[2][2]) - (m[1][2] * m[2][1]));
	T det2 = ((m[0][2] * m[2][1]) - (m[0][1] * m[2][2]));
	T det3 = ((m[0][1] * m[1][2]) - (m[0][2] * m[1][1]));

	T det = (m[0][0] * det1) + (m[1][0] * det2) + (m[2][0] * det3);

	if (det != (T)0)
	{

		// If this is failing, set an identity matrix
		//NOT SURE WHY THIS NEVER WORKS if (fabs((double)1.0/(double)det) < MAC_MATRIX_INVERSION_PRECISCION_LIMIT)
		if (fabs((double)1.0/(double)det) < (0.000005))
		{
			return this->setIdentity();
		}
		else
		{
			result[0][0] = det1 / det;
			result[1][0] = ((m[2][0] * m[1][2]) - (m[1][0] * m[2][2])) / det;
			result[2][0] = ((m[1][0] * m[2][1]) - (m[2][0] * m[1][1])) / det;
			result[0][1] = det2 / det;
			result[1][1] = ((m[0][0] * m[2][2]) - (m[2][0] * m[0][2])) / det;
			result[2][1] = ((m[2][0] * m[0][1]) - (m[0][0] * m[2][1])) / det;
			result[0][2] = det3 / det;
			result[1][2] = ((m[1][0] * m[0][2]) - (m[0][0] * m[1][2])) / det;
			result[2][2] = ((m[0][0] * m[1][1]) - (m[1][0] * m[0][1])) / det;
		}
	}

	// invert the orignal position and negate!
	result[3][0] = -((m[3][0] * result[0][0]) + (m[3][1] * result[1][0]) + (m[3][2] * result[2][0]));
    result[3][1] = -((m[3][0] * result[0][1]) + (m[3][1] * result[1][1]) + (m[3][2] * result[2][1]));
    result[3][2] = -((m[3][0] * result[0][2]) + (m[3][1] * result[1][2]) + (m[3][2] * result[2][2]));

	// Set other values
	result.x[3] = (T)0;
	result.y[3] = (T)0;
	result.z[3] = (T)0;
	result.t[3] = (T)1;
	
	return *this;
}

//
// Notes:
//		Changes values in the class
//		Gauss-Jordan elimination with partial pivoting
// log:
//		Created: 7/17/2001, Ian Elsley
//
template<class T> MACHomogeneousMatrix4<T>&	
MACHomogeneousMatrix4<T>::invertGeneral	()
{
	MACHomogeneousMatrix4<T>	&a	= *this;
	MACHomogeneousMatrix4<T>	b;
    unsigned int				i, j, k;
	MACVector4<T>				temp;

    // Loop over a from left to right, eliminating above and below diagonal
    // Find largest pivot in column j among rows j..3
	//
    for (j=0; j<4; j++)
	{   
        k = j;		   // Row with largest pivot candidate
        
        for (i=j+1; i<4; i++) 
            if (fabs((double)a[i][j]) > fabs((double)a[k][j])) 
                k = i;
        
        // Swap rows k and j in a and b to put pivot on diagonal
        temp = a[k]; a[k] = a[j]; a[j] = temp;
		temp = b[k]; b[k] = b[j]; b[j] = temp;
        
        if (a[j][j] == (T)0)	    // SINGULAR MATRIX !!! Not a good solution, but it will have to do.
			return this->setIdentity();
 
       // Scale row j to have a unit diagonal
        b[j] /= a[j][j];
        a[j] /= a[j][j];
        
        // Eliminate off-diagonal elems in col j of a, doing identical ops to b
        for (i = 0; i < 4; ++i) {
            if (i != j) {
                b[i] = b[i] - (a[i][j] * b[j]);
                a[i] = a[i] - (a[i][j] * a[j]);
            }
        }
    }

    return *this = b;
}



//
// Notes:
//		Changes values in the class
// log:
//		Created: 7/17/2001, Ian Elsley
//
template<class T> MACHomogeneousMatrix4<T>&	
MACHomogeneousMatrix4<T>::invert	()
{
	return invertAffine();
}


//
// Notes:
//		Changes values in the class
// log:
//		Created: 7/17/2001, Ian Elsley
//
template<class T> MACHomogeneousMatrix4<T>	
MACHomogeneousMatrix4<T>::theInverse	()	const
{
	MACHomogeneousMatrix4<T> result(*this);

	result.invert();

	return result;
}

//
// Notes:
//		Changes values in the class
// log:
//		Created: 7/17/2001, Ian Elsley
//
template<class T> MACHomogeneousMatrix4<T>&	
MACHomogeneousMatrix4<T>::transpose	()
{
	T temp;

	temp = x[1];	x[1] = y[0];	y[0] = temp;
	temp = x[2];	x[2] = z[0];	z[0] = temp;
	temp = x[3];	x[3] = t[0];	t[0] = temp;
	temp = y[2];	y[2] = z[1];	z[1] = temp;
	temp = y[3];	y[3] = t[1];	t[1] = temp;
	temp = z[3];	z[3] = t[2];	t[2] = temp;
	
	return *this;
}

//
// Notes:
// log:
//		Created: 7/17/2001, Ian Elsley
//
template<class T> MACHomogeneousMatrix4<T>
MACHomogeneousMatrix4<T>::theTranspose	()	const
{
	MACHomogeneousMatrix4<T> result = *this;
	return result.transpose();
}


//
// Notes:
//		Changes values in the class
// log:
//		Created: 7/17/2001, Ian Elsley
//
template<class T> void
MACHomogeneousMatrix4<T>::decomposeMatrix (	MACRotationMatrix3<T>&	rotate,
											MACTranslation<T>&		translate,
											MACScale<T>&				scale )		const
{
    T	sXY, sXZ;
	const MACHomogeneousMatrix4<T> &matrix = *this;

    translate = t;

    rotate[0][0] = matrix[0][0];
    rotate[0][1] = matrix[0][1];
    rotate[0][2] = matrix[0][2];

    scale.X ( (T)sqrt ((double)(rotate[0][0]*rotate[0][0] + rotate[0][1]*rotate[0][1] + rotate[0][2]*rotate[0][2])) );

    // Normalize first row 
    rotate[0][0] /= scale.X();
    rotate[0][1] /= scale.X();
    rotate[0][2] /= scale.X();

    // Determine xy shear 
    sXY = matrix[0][0]*matrix[1][0] + matrix[0][1]*matrix[1][1] + matrix[0][2]*matrix[1][2];

    rotate[1][0] = matrix[1][0] - sXY*matrix[0][0];
    rotate[1][1] = matrix[1][1] - sXY*matrix[0][1];
    rotate[1][2] = matrix[1][2] - sXY*matrix[0][2];

    scale.Y ( (T)sqrt ((double)(rotate[1][0]*rotate[1][0] + rotate[1][1]*rotate[1][1] + rotate[1][2]*rotate[1][2])) );

    // Normalize second row 
    rotate[1][0] /= scale.Y();
    rotate[1][1] /= scale.Y();
    rotate[1][2] /= scale.Y();
 
    // Determine xz shear 
    sXZ = matrix[0][0]*matrix[2][0] + matrix[0][1]*matrix[2][1] + matrix[0][2]*matrix[2][2];

    rotate[2][0] = matrix[2][0] - sXZ*matrix[0][0];
    rotate[2][1] = matrix[2][1] - sXZ*matrix[0][1];
    rotate[2][2] = matrix[2][2] - sXZ*matrix[0][2];

    scale.Z ( (T)sqrt ((double)(rotate[2][0]*rotate[2][0]+rotate[2][1]*rotate[2][1]+rotate[2][2]*rotate[2][2])) );

    // Normalize third row 
    rotate[2][0] /= scale.Z();
    rotate[2][1] /= scale.Z();
    rotate[2][2] /= scale.Z();
 
    // If the determinant of the rotation matrix is negative, negate the matrix and scale factors.
    if (rotate.determinant() < (T)0)
    {
		rotate[0][0]*=(T)-1;rotate[0][1]*=(T)-1;rotate[0][2]*=(T)-1;
		rotate[1][0]*=(T)-1;rotate[1][1]*=(T)-1;rotate[1][2]*=(T)-1;
		rotate[2][0]*=(T)-1;rotate[2][1]*=(T)-1;rotate[2][2]*=(T)-1;
		
		scale	*=	(T)-1;
	}
}

//********************************************
//
// Comparitor Functions
//
//

// 
// MACHomogeneousMatrix4<T>::equal(const MACHomogeneousMatrix4<T>& v, T error = ERROR_VALUE)	const
// Notes:
// Log:
//		Created 7/17/01, Ian Elsley
//
template<class T> inline bool
MACHomogeneousMatrix4<T>::equal(const MACHomogeneousMatrix4<T>& v, T error)	const
{
	return (x.equal(v.x, error) && y.equal(v.y, error) && z.equal(v.z, error) && t.equal(v.t, error));
}


//
// MACHomogeneousMatrix4<T>::operator==(const MACHomogeneousMatrix4<T>& v) const
// Notes:
//		This function may not work entirely as expected. 
//		There may be an error factor required.
// Log:
//		Created 7/17/01, Ian Elsley
//
template<class T> inline bool
MACHomogeneousMatrix4<T>::operator==(const MACHomogeneousMatrix4<T>& v)	const
{
	return ((x==v.x) && (y==v.y) && (z==v.z) && (t==v.t));
}


//
// MACHomogeneousMatrix4<T>::operator!=(const MACHomogeneousMatrix4<T>& v) const
// Notes:
// Log:
//		Created 7/17/01, Ian Elsley
//
template<class T> inline bool
MACHomogeneousMatrix4<T>::operator!=(const MACHomogeneousMatrix4<T>& v)						const
{
	return (!((*this) == v));
}


//********************************************
//
// Identity code
//
//		Created 7/17/01, Ian Elsley
//

template<class T> MACHomogeneousMatrix4<T>&
MACHomogeneousMatrix4<T>::setIdentity	()
{
	x.set((T)1, (T)0, (T)0, (T)0);
	y.set((T)0, (T)1, (T)0, (T)0);
	z.set((T)0, (T)0, (T)1, (T)0);
	t.set((T)0, (T)0, (T)0, (T)1);

	return *this;
}


//********************************************
//
// Mirror code
//
//		Created 7/18/01, Ian Elsley
//


template<class T> MACHomogeneousMatrix4<T>&	
MACHomogeneousMatrix4<T>::mirror		(const MACHomogeneousMatrix4<T>& planeRotation, 
									 const MACHomogeneousMatrix4<T>& inversePlaneRotation)
{
	MACHomogeneousMatrix4<T> &mirrorTransform = *this;
	MACScale<T>				scaleMirror((T)-1, (T)1, (T)1);
	// rotate 
	mirrorTransform = planeRotation * mirrorTransform;

	// mirror through YZ plane
	mirrorTransform = scaleMirror * mirrorTransform;

	// rotate back
	mirrorTransform = planeRotation.theTranspose() * mirrorTransform;
	//mirrorTransform *= inversePlaneRotation;

	return *this;
}


template<class T> MACHomogeneousMatrix4<T>&	
MACHomogeneousMatrix4<T>::mirror		(const MACVector3<T>& planeNormal, 
									 const MACVector3<T>& pointOnPlane)
{
	static MACVector3<T>			xAxis((T)1, (T)0, (T)0);
	MACHomogeneousMatrix4<T>		planeRotation(MACAngleAxisRotation<T>(xAxis, planeNormal.normalize()));

	planeRotation += MACTranslation(pointOnPlane);

	return mirror(planeRotation, inverse(planeRotation));
}


//********************************************
//
// OpenGL helper functions
//

//
// Notes:
// Log:
//		Created: 4/10/2001, Ian Elsley
//
template<class T>	MACHomogeneousMatrix4<T>&
MACHomogeneousMatrix4<T>::setFromGLMatrix	(const GLfloat glMatrix[16])
{
	int						i, j;
	MACHomogeneousMatrix4<T>	&localMatrix = *this;

	for (i=0; i<4; i++)
		for (j=0; j<4; j++)
			glMatrix[(i*4)+j] = (GLfloat)localMatrix[i][j];

	return *this;
}

//
// Notes:
// Log:
//		Created: 4/10/2001, Ian Elsley
//
template<class T>	void
MACHomogeneousMatrix4<T>::getGLMatrix	(GLfloat glMatrix[16])
{
	int						i, j;
	MACHomogeneousMatrix4<T>	&localMatrix = *this;

	for (i=0; i<4; i++)
		for (j=0; j<4; j++)
			localMatrix[i][j] = (T)glMatrix[(i*4)+j];
}



//
// End of Method Code
//
//****************************************************************************************
//****************************************************************************************


//****************************************************************************************
//****************************************************************************************
//
// Start of Function Code
//


//***********************************************************************
//***********************************************************************
//
// Arithmetic Functions
//
// Notes:
//

//
// Notes:
// Log:
//		Created: 4/10/2001, Ian Elsley
//
//template<class T>	MACVector4<T>
//operator* (const MACVector3<T>& v, const MACHomogeneousMatrix4<T>& m)
//{
//	return MACVector4<T>(v.dot4(m[0]), v.dot4(m[1]), v.dot4(m[2]), v.dot4(m[3]));
//}

//
// Notes:
// Log:
//		Created: 4/10/2001, Ian Elsley
//
template<class T>	MACVector4<T>
operator* (const MACHomogeneousMatrix4<T>& m, const MACVector4<T>& v)
{
	return MACVector4<T>((m[0][0]*v[0]) + (m[1][0]*v[1]) + (m[2][0]*v[2]) + (m[3][0]*v[3]),
						(m[0][1]*v[0]) + (m[1][1]*v[1]) + (m[2][1]*v[2]) + (m[3][1]*v[3]),
						(m[0][2]*v[0]) + (m[1][2]*v[1]) + (m[2][2]*v[2]) + (m[3][2]*v[3]),
						(m[0][3]*v[0]) + (m[1][3]*v[1]) + (m[2][3]*v[2]) + (m[3][3]*v[3]));
}

//
// Notes:
// Log:
//		Created: 4/10/2001, Ian Elsley
//
template<class T>	MACVector3<T>
operator* (const MACHomogeneousMatrix4<T>& m, const MACVector3<T>& v)
{
	return MACVector3<T>((m[0][0]*v[0]) + (m[1][0]*v[1]) + (m[2][0]*v[2]) + (m[3][0]),
						(m[0][1]*v[0]) + (m[1][1]*v[1]) + (m[2][1]*v[2]) + (m[3][1]),
						(m[0][2]*v[0]) + (m[1][2]*v[1]) + (m[2][2]*v[2]) + (m[3][2]));
}


//
// Notes:
// Log:
//		Created: 7/19/2001, Ian Elsley
//
template<class T>	MACHomogeneousMatrix4<T>
operator* (const MACScale<T>& s, const MACHomogeneousMatrix4<T>& m)
{
	int i, j;
	MACHomogeneousMatrix4<T> result;
	
	for (i=0; i<4; i++) {
		for (j=0; j<4; j++) {
			result[i][j] = m[i][j] * ((j<3)?s[j]:(T)1);
		}
	}
	
	return result;
}

//
// Notes:
// Log:
//		Created: 7/19/2001, Ian Elsley
//
template<class T>	MACHomogeneousMatrix4<T>
operator/ (const MACScale<T>& s, const MACHomogeneousMatrix4<T>& m)
{
	int i, j;
	MACHomogeneousMatrix4<T> result;
	
	for (i=0; i<4; i++) {
		for (j=0; j<4; j++) {
			result[i][j] = m[i][j] / ((j<3)?s[j]:(T)1);
		}
	}
	
	return result;
}

//
// End of Artithmetic Funtions
//
//***********************************************************************
//***********************************************************************


//***********************************************************************
//***********************************************************************
//
// I/O Functions
//
// Notes:
//

//
// operator>>
// Notes:
// Log:
//		Created 2/27/01, Ian Elsley
//
template<class T> std::istream& 
operator>>(std::istream& in, MACHomogeneousMatrix4<T>& v)
{

	return in >> x >> y >> z >> t;
}

//
// operator<<
// Notes:
// Log:
//		Created 2/27/01, Ian Elsley
//
template<class T> std::ostream& 
operator<<(std::ostream& out, MACHomogeneousMatrix4<T>& v)
{
	return out << x << '\n' << y << '\n' << z << '\n' << t << '\n';
}

//
// End of Function Code
//
//****************************************************************************************
//****************************************************************************************



MAC_MATH_LIB_END

#endif __MAC_HOMOGENEOUS_MATRIX_4_H__