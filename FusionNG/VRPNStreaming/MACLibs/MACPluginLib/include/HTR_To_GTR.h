//
// HTR_To_GTR.h
// Motion Analysis Corp. 2011
// 
// Utilities to convert HTR segment data to GTR
//


#pragma once

void SegmentData_Copy(tSegmentData &dst, const tSegmentData &src);
void Segment_ConvertHtrToGtr(tSegmentData & segmentGtr, const tSegmentData & parentGtr, const tSegmentData &segmentHtr, int iRotationOrder);
void Body_ConvertHtrToGtr(const sBodyData & BodyData, const sBodyDef & BodyDef, tSegmentData ** gtrs, int iRotationOrder = ZYX_ORDER);
void Frame_ConvertHtrToGtr(const sFrameOfData* FrameOfData, const sBodyDefs *BodyDefs, tSegmentData ** GTR_Segments, int iRotationOrder = ZYX_ORDER);
int Convert_HtrWithoutBaseToWithBase(tSegmentData &WithoutBase, tSegmentData &Base, tSegmentData &WithBase, int iRotationOrder = ZYX_ORDER);
int Convert_HtrWithBaseToWithoutBase(tSegmentData &WithBase, tSegmentData &Base, tSegmentData &WithoutBase, int iRotationOrder = ZYX_ORDER);

