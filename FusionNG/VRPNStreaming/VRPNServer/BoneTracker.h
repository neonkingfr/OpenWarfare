#ifndef __BONETRACKER__
#define __BONETRACKER__

#include <math.h>

#include "vrpn_Text.h"
#include "vrpn_Tracker.h"
#include "vrpn_Connection.h"
#include "quat.h"

#include "VRPN_Config_File.h"


//! Tracker class
/*
*  This class represents a bone transform as
*  position (vector) and orientation (quaternion)
*/
class BoneTracker : public vrpn_Tracker
{
public:
	BoneTracker( const char* name, vrpn_Connection *c = 0 , int iSensorNumber = 0);
	virtual ~BoneTracker() {};

	// update
	virtual void mainloop();

	void SetDataOffset(int iDataOffset){m_iDataOffset = iDataOffset;};
	void SetBodyOffset(int iBodyOffset){m_iBodyOffset = iBodyOffset;};

protected:
	struct timeval _timestamp;
	int m_iDataOffset;
	int m_iBodyOffset;

};

#endif // __BONETRACKER__