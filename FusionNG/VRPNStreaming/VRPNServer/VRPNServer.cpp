// VRPNServer.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include "BoneTracker.h"
#include "VRPN_SDK_Connection.h"
#include "VRPN_Config_File.h"

VRPN_SDK_Connection * g_SDK2_Connection = NULL;

std::list<BoneTracker*> g_BonesList;

MAC_VRPN_Config g_Config;

// Stream data from SDK
sBodyDefs *		g_pBodyDefs = NULL;
tSegmentData * g_pGTR_SegmentData[MAX_N_BODIES];	// GTR segment data for the last frame of data

HANDLE dataMutex = NULL;

char g_szVersion[] = "1.0.0";

////////////// MAIN ///////////////////

int _tmain(int argc, _TCHAR* argv[])
{
	// Creating the network server
	vrpn_Connection_IP* m_Connection = new vrpn_Connection_IP();

	// A mutex to protect data locations
	dataMutex = CreateMutex(NULL, FALSE, "DataMutex");

	g_Config.Msg("*** MAC_VRPN_Server version: ");
	g_Config.Msg(g_szVersion);
	g_Config.Msg(" ***\n\n");

	g_Config.ReadConfig();

	g_SDK2_Connection = new VRPN_SDK_Connection(VL_Debug, ".\\SDKLog.txt");

	char szStatusMessage[MAX_PATH];

	if(g_Config.m_bDebug)
	{
		g_Config.Msg("Debug output is on\n");
	}
	else
	{
		g_Config.Msg("Debug output is off\n");
	}

	sprintf(szStatusMessage, "Delimiter is set to %c\n", g_Config.m_cDelimiter);
	g_Config.Msg(szStatusMessage);

	if(g_Config.m_optionTracker == Tracker_PerBody)
	{
		g_Config.Msg("Tracker per body is set\n");
	}
	else
	{
		g_Config.Msg("Tracker per sensor is set\n");
	}

	if(g_Config.m_bSendInvalidData == true)
	{
		g_Config.Msg("Tracker will send data when invalid (9999999.0)\n");
	}
	else
	{
		g_Config.Msg("Tracker will NOT send data when invalid\n");
	}

	sprintf(szStatusMessage, "Attempting to connect %s to Cortex SDK at %s\n",
		g_Config.ClientIP().c_str(), g_Config.ServerIP().c_str());
	g_Config.Msg(szStatusMessage);

	bool bReturn = g_SDK2_Connection->Initialize(g_Config.ClientIP(), g_Config.ServerIP());
	g_SDK2_Connection->Activate(true);

	if(bReturn)
	{
		std::string verString = g_SDK2_Connection->VersionString();
		g_Config.Msg("SDK driver connection succeeded: ");
		g_Config.Msg("SDK Version: ");
		g_Config.Msg(verString.c_str());
		g_Config.Msg("\n");

		sHostInfo hostInfo;
		Cortex_GetHostInfo(&hostInfo);
		if(hostInfo.bFoundHost)
		{
			g_Config.Msg("Host name: ");
			g_Config.Msg(hostInfo.szHostMachineName);
			g_Config.Msg("   Host program: ");
			g_Config.Msg(hostInfo.szHostProgramName);
			g_Config.Msg("\n\n");
			g_pBodyDefs = g_SDK2_Connection->GetBodyDefinitions();
			if(g_pBodyDefs)
			{
				sFrameOfData * curFrame = g_SDK2_Connection->GetCurrentFrame();
				g_SDK2_Connection->DataHasArrived(curFrame);

				char szOut[MAX_PATH];
				sprintf(szOut, "Number of bodies: %d\n", g_pBodyDefs->nBodyDefs);
				g_Config.Msg(szOut);
				for(int i = 0; i < g_pBodyDefs->nBodyDefs; i++)
				{
					sBodyDef Body = g_pBodyDefs->BodyDefs[i];
					g_Config.DebugMsg("\n** Body: ");
					g_Config.DebugMsg(Body.szName);
					g_Config.DebugMsg(" **\n\n");
					
					// If the Config file contained a body entry, the body config will already exist
					BodyConfig * pBodyConfig = g_Config.LookupBodyConfig(Body.szName);
					if(!pBodyConfig)
					{	// Create a new Body config
						pBodyConfig = new BodyConfig(Body.szName, Body.szName);
						g_Config.m_BodyConfigList.push_back(pBodyConfig);
					}

					for(int j = 0; j < Body.Hierarchy.nSegments; j++)
					{
						char szTrackerName[MAX_PATH];
						int iSegmentIndex = j;
						BoneTracker * bt;

						SegmentConfig * pSegmentConfig = pBodyConfig->FindSegmentConfig(Body.Hierarchy.szSegmentNames[j]);
						g_Config.DebugMsg(Body.Hierarchy.szSegmentNames[j]);
						g_Config.DebugMsg(" - ");
						if(pSegmentConfig)
						{
							iSegmentIndex = pSegmentConfig->m_iSensorIndex;
						}
						
						if(g_Config.m_optionTracker == Tracker_PerBody)
						{
							sprintf(szTrackerName, "%s", pBodyConfig->m_szAliasName);
							bt = new BoneTracker(szTrackerName, m_Connection, iSegmentIndex);
						}
						else
						{
							sprintf(szTrackerName, "%s%c%s", pBodyConfig->m_szAliasName, g_Config.m_cDelimiter, Body.Hierarchy.szSegmentNames[j]);
							bt = new BoneTracker(szTrackerName, m_Connection);
						}
						bt->SetDataOffset(j);
						bt->SetBodyOffset(i);
						g_BonesList.push_back(bt);
					}
				}
			}
		}
		else
		{
			g_Config.Msg("Connection to SDK host failed\n");
		}
	}
	else
	{
		g_Config.Msg("Connection to Motion Analysis SDK2 driver failed\n");
	}
	g_Config.Msg("\nCreated VRPN server.\n");

	while(true)
	{

		std::list<BoneTracker*>::iterator iter;

		// interate through bone trackers and do mainloop:
		for(iter = g_BonesList.begin(); iter != g_BonesList.end(); iter++)
		{
			BoneTracker* bt = (BoneTracker*)*iter;
			// update tracker
			bt->mainloop();
		}

		//update connection
		m_Connection->mainloop();

		// Calling Sleep to let the CPU breathe.
		SleepEx(1,FALSE);
	}

	g_SDK2_Connection->Uninitialize();
	delete g_SDK2_Connection;
	return 0;
}
