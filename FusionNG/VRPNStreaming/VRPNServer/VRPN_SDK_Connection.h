/*
*	VRPN_SDK_Connection.h
*	Connection to SDK specific to VRPN Server
*
*	copyright Motion Analysis Corporation 2011
*
*/

#pragma once

#include "SDK2_Connection.h"

class VRPN_SDK_Connection : public SDK2_Interface //,  public SDK2_DataListener
{
public:
	VRPN_SDK_Connection(int level, const std::string& logFile);
	void DataHasArrived(sFrameOfData* data);
	void SceneUpdate(sFrameOfData * data){;}; // Need to have one of these...

	// ocnvert to mm factor:
	double m_dUnitConversion;
};