/*
*	VRPN_SDK_Connection.cpp
*	SDK Connection for VRPN server implementation
* 
*	copyright Motion Analysis Corporation 2011
*
*/

#include "stdafx.h"
#include "VRPN_SDK_Connection.h"
#include "VRPN_Config_File.h"

extern sBodyDefs * g_pBodyDefs;					// current SDK body defs
extern tSegmentData * g_pGTR_SegmentData[];		// GTR segment data for the last frame of data
extern HANDLE dataMutex;						// Protect segment data from thread conflicts

extern MAC_VRPN_Config g_Config;

static bool bFramesReceived = false;
static int iBroadcastCount = 0;
	
VRPN_SDK_Connection::VRPN_SDK_Connection(int level, const std::string& logFile)
{
	Cortex_SetVerbosityLevel(level);
	// open a log file if specified
	if (logFile.length() > 0)
	{
		// this may fail, in which case logging is disabled
		errno_t errNo = fopen_s(&(this->m_logFile), logFile.c_str(), "w");

		// set this as the message logger
		SetMessageLogger(this);
	}
}

static void LimitRotationDegrees(double & dRotation)
{
	if(dRotation > 180)
		dRotation -= 360;
	if(dRotation < -180)
		dRotation += 360;
}

static void ScaleFrameOfData(sFrameOfData * pData, float dScaleFactor)
{
	int iBodyCount = pData->nBodies;
	for(int i = 0; i < iBodyCount; i++)
	{
		sBodyData BodyData = pData->BodyData[i];
		int iMarkerCount = BodyData.nMarkers;
		int iSegmentCount = BodyData.nSegments;
		for(int j = 0; j < iMarkerCount; j++)
		{
			tMarkerData & MarkerData = BodyData.Markers[j];
			for(int ii = 0; ii < 3; ii++)
			{
				// Don't scale XEMPTY entries so they can be recognized elsewhere
				if(MarkerData[ii] != XEMPTY)
				{
					MarkerData[ii] *= dScaleFactor;
				}
			}
		}
		for(int j = 0; j < iSegmentCount; j++)
		{
			tSegmentData & SegmentData = BodyData.Segments[j];

			// Don't scale XEMPTY entries so they can be recognized elsewhere
			if(SegmentData[0] != XEMPTY)
			{
				SegmentData[0] *= dScaleFactor;
				SegmentData[1] *= dScaleFactor;
				SegmentData[2] *= dScaleFactor;

				LimitRotationDegrees(SegmentData[3]);
				LimitRotationDegrees(SegmentData[4]);
				LimitRotationDegrees(SegmentData[5]);

				SegmentData[6] *= dScaleFactor;
			}
		}
	}
}

void VRPN_SDK_Connection::DataHasArrived(sFrameOfData* data)
{
	if((!bFramesReceived) && (iBroadcastCount > 0)) // Ignore first call from main routine to update data
	{
		bFramesReceived = true;
		g_Config.Msg("SDK stream frame received\n");
	}

	sFrameOfData * pData = NULL;
	pData = CopyFrame(data);
	// Scale data from (assumed mm) to meters
	ScaleFrameOfData(pData, (float)(1.0/1000.0));
	if(g_pBodyDefs)
	{
		if(dataMutex)
		{
			DWORD dWaitResult = WaitForSingleObject(dataMutex, 2000);
			if(dWaitResult == WAIT_OBJECT_0)
			{
				Frame_ConvertHtrToGtr(pData, g_pBodyDefs, (tSegmentData**)g_pGTR_SegmentData, ZYX_ORDER);
				ReleaseMutex(dataMutex);
			}
		}
	}
	// Lose it once you use it...
	FreeFrame(pData);
}
