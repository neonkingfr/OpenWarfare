#ifndef __POSE_CONTROL_UNIT__
#define __POSE_CONTROL_UNIT__

#include <map>
#include "../vrpn/vrpn/vrpn_Tracker.h"

#include "data/Unit.h"
#include "mathHelper.h"

const double	PI = 3.14159265358979323846;
const double	HALF_PI = 1.57079632679489661923;
const double	R_TO_D = 180.0/PI;
const double	D_TO_R = PI/180.0;

typedef std::map<int,std::string> BoneTrackerMap;



class PoseControlUnit : public VBS2Fusion::Unit
{
public:
  BoneTrackerMap boneTrackerMap;

private:
  // position updates
  VBS2Fusion::position3D _prevUnitPos;
  float _pelvisXOffset, _pelvisPrevXOffset, _pelvisZOffset, _pelvisPrevZOffset;

  // Hand bones - uncontrolled 
  VBS2Fusion::Matrix4f m_lastLeftHandBoneTransformation;
  VBS2Fusion::Matrix4f m_lastRightHandBoneTransformation;
  VBS2Fusion::Matrix4f lastHeadBoneTransformation;
  VBS2Fusion::Matrix4f cameraTransformation;

  // Spine bone (to set correct pelvis transformation)
  VBS2Fusion::Matrix4f m_lastSpineBoneTransformation;

  bool _internalCamInitialized;

public:
  PoseControlUnit();

  virtual bool onModifyBone(VBS2Fusion::Matrix4f& mat, VBS2Fusion::SKELETON_TYPE index);

  // new fusion callbacks
  virtual bool onModifyGunTrans(VBS2Fusion::Matrix4f& mat4f);  
  virtual bool onModifyHeadTrans(VBS2Fusion::Matrix4f& mat4f); 
  virtual bool onModifyLookTrans(VBS2Fusion::Matrix4f& mat4f);
  virtual bool onModifyRelativeChange(VBS2Fusion::Matrix4f& mat4f);
  virtual bool onReload();
};

#endif