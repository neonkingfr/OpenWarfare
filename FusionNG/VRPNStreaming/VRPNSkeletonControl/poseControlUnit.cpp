#include <map>
#include "VBS2Fusion.h"
#include "Math/Matrix4f.h"
#include "util/PoseControlUtilities.h"
#include "VBS2FusionDefinitions.h"
#include "util/CameraUtilities.h"
#include "SkeletonDefinition.h"
#include "..\vrpn\quat\quat.h"
#include "pluginHeader.h"
#include "VRPNClient.h"

#include "PoseControlUnit.h"
#include "mathHelper.h"

extern VRPNClient* g_pClient;

//////////////////////////////////////////////////////////////////////////
void TrackerToMatrix(q_type quat, float pos[3], Matrix4& mat)
{
  float x = quat[0];
  float y = quat[1];
  float z = quat[2];
  float w = quat[3];

  float wx = w*x*2;
  float wy = w*y*2;
  float wz = w*z*2;
  float xx = x*x*2;
  float xy = x*y*2;
  float xz = x*z*2;
  float yy = y*y*2;
  float yz = y*z*2;
  float zz = z*z*2;

  mat.Set(0, 0) = 1 - yy - zz;
  mat.Set(0, 1) = xy - wz;
  mat.Set(0, 2) = xz + wy;
  mat.Set(1, 0) = xy + wz;
  mat.Set(1, 1) = 1 - xx - zz;
  mat.Set(1, 2) = yz - wx;
  mat.Set(2, 0) = xz - wy;
  mat.Set(2, 1) = yz + wx;
  mat.Set(2, 2) = 1 - xx - yy;

  mat.SetTranslation(Vector3(pos[0], pos[1], pos[2]));
}

//_________________________________________________________________________
PoseControlUnit::PoseControlUnit() 
:_pelvisXOffset(0.0f), _pelvisZOffset(0.0f), _internalCamInitialized(false)
{
}

bool PoseControlUnit::onModifyRelativeChange(VBS2Fusion::Matrix4f& mat4f)
{
  return false;
}

bool PoseControlUnit::onModifyGunTrans(VBS2Fusion::Matrix4f& mat4f)
{
  return false;
}

bool PoseControlUnit::onModifyHeadTrans(VBS2Fusion::Matrix4f& mat4f)
{
  return false;
}

bool PoseControlUnit::onModifyLookTrans(VBS2Fusion::Matrix4f& mat4f)
{
  if (IsInternalCamera())
  {
    if (!_internalCamInitialized)
    {
      ExecuteCmd("setCamFrustum [true, tan 35]");
      _internalCamInitialized = true;
    }

    mat4f = cameraTransformation;
  }
  else
  {
    if (_internalCamInitialized)
    {
      ExecuteCmd("setCamFrustum [true, tan 30]");
      _internalCamInitialized = false;
    }
  }

  return true;
}

bool PoseControlUnit::onReload()
{
  return false;
}


//////////////////////////////////////////////////////////////////////////
bool PoseControlUnit::onModifyBone(VBS2Fusion::Matrix4f& mat, VBS2Fusion::SKELETON_TYPE index)
{
  //-----------------------------------------------------------------------
  // check for uncontrolled bones  
  switch (index)
  {
  case eLBROW:                { return true; }
  case eMBROW:                { return true; }
  case eRBROW:                { return true; }
  case eLMOUTH:               { return true; }
  case eMMOUTH:               { return true; }
  case eRMOUTH:               { return true; }
  case eEYELIDS:              { return true; }
  case eLLIP:                 { return true; }
  case eCAMERA:               { return true; }

  // FIXED LEFT-HAND BONES
  case eLEFT_HAND_RING:       { mat = m_lastLeftHandBoneTransformation; return true; }
  case eLEFT_HAND_RING1:      { mat = m_lastLeftHandBoneTransformation; return true; }
  case eLEFT_HAND_RING2:      { mat = m_lastLeftHandBoneTransformation; return true; }
  case eLEFT_HAND_RING3:      { mat = m_lastLeftHandBoneTransformation; return true; }
  case eLEFT_HAND_PINKY1:     { mat = m_lastLeftHandBoneTransformation; return true; }
  case eLEFT_HAND_PINKY2:     { mat = m_lastLeftHandBoneTransformation; return true; }
  case eLEFT_HAND_PINKY3:     { mat = m_lastLeftHandBoneTransformation; return true; }
  case eLEFT_HAND_MIDDLE1:    { mat = m_lastLeftHandBoneTransformation; return true; }
  case eLEFT_HAND_MIDDLE2:    { mat = m_lastLeftHandBoneTransformation; return true; }
  case eLEFT_HAND_MIDDLE3:    { mat = m_lastLeftHandBoneTransformation; return true; }
  case eLEFT_HAND_INDEX1:     { mat = m_lastLeftHandBoneTransformation; return true; }
  case eLEFT_HAND_INDEX2:     { mat = m_lastLeftHandBoneTransformation; return true; }
  case eLEFT_HAND_INDEX3:     { mat = m_lastLeftHandBoneTransformation; return true; }
  case eLEFT_HAND_THUMB1:     { mat = m_lastLeftHandBoneTransformation; return true; }
  case eLEFT_HAND_THUMB2:     { mat = m_lastLeftHandBoneTransformation; return true; }
  case eLEFT_HAND_THUMB3:     { mat = m_lastLeftHandBoneTransformation; return true; }

  // FIXED RIGHT-HAND BONES
  case eRIGHT_HAND_RING:       { mat = m_lastRightHandBoneTransformation; return true; }
  case eRIGHT_HAND_RING1:      { mat = m_lastRightHandBoneTransformation; return true; }
  case eRIGHT_HAND_RING2:      { mat = m_lastRightHandBoneTransformation; return true; }
  case eRIGHT_HAND_RING3:      { mat = m_lastRightHandBoneTransformation; return true; }
  case eRIGHT_HAND_PINKY1:     { mat = m_lastRightHandBoneTransformation; return true; }
  case eRIGHT_HAND_PINKY2:     { mat = m_lastRightHandBoneTransformation; return true; }
  case eRIGHT_HAND_PINKY3:     { mat = m_lastRightHandBoneTransformation; return true; }
  case eRIGHT_HAND_MIDDLE1:    { mat = m_lastRightHandBoneTransformation; return true; }
  case eRIGHT_HAND_MIDDLE2:    { mat = m_lastRightHandBoneTransformation; return true; }
  case eRIGHT_HAND_MIDDLE3:    { mat = m_lastRightHandBoneTransformation; return true; }
  case eRIGHT_HAND_INDEX1:     { mat = m_lastRightHandBoneTransformation; return true; }
  case eRIGHT_HAND_INDEX2:     { mat = m_lastRightHandBoneTransformation; return true; }
  case eRIGHT_HAND_INDEX3:     { mat = m_lastRightHandBoneTransformation; return true; }
  case eRIGHT_HAND_THUMB1:     { mat = m_lastRightHandBoneTransformation; return true; }
  case eRIGHT_HAND_THUMB2:     { mat = m_lastRightHandBoneTransformation; return true; }
  case eRIGHT_HAND_THUMB3:     { mat = m_lastRightHandBoneTransformation; return true; }

  // PELVIS
  case ePELVIS: { mat = m_lastSpineBoneTransformation; return true; }

  default:
    break;
  }

  //-----------------------------------------------------------------------
  // get tracker data
  vrpn_TRACKERCB trackerData;
  bool dataInitialized = false;
  eAXIS_ADJUSTMENT axisAdjustment = AXIS_DEFAULT;
  eROTATION_ADJUSTMENT rotationAdjustment = ROTATION_DEFAULT;

  BoneTrackerMap::const_iterator itTrackerData = boneTrackerMap.find(index);
  if (itTrackerData != boneTrackerMap.end())
  {
    TrackerList::iterator itTracker = g_pClient->_trackers.find((*itTrackerData).second);
    if (itTracker != g_pClient->_trackers.end())
    {
      trackerData = (*itTracker).second._data;
      axisAdjustment = (*itTracker).second._axisAdjustment;
      rotationAdjustment = (*itTracker).second._rotationAdjustment;
      dataInitialized = (trackerData.sensor == eTRACKER_INITIALIZED); // sensor used as initialization flag
    }
  }

  //-----------------------------------------------------------------------
  // process tracker data
  // (if the tracker isn't receiving data from server, identity matrix is set)
  if (dataInitialized)
  {
    q_type boneRotation = { trackerData.quat[0], trackerData.quat[1], trackerData.quat[2], trackerData.quat[3] };
    
    // get position and convert to VBS coord system
    float bonePosition[3];
    if (g_pClient->_upAxis == UP_AXIS_Z)
    {
      bonePosition[0] = -trackerData.pos[0];
      bonePosition[1] = trackerData.pos[2];
      bonePosition[2] = -trackerData.pos[1];
    }
    else
    {
      bonePosition[0] = -trackerData.pos[0];
      bonePosition[1] = trackerData.pos[1];
      bonePosition[2] = trackerData.pos[2];
    }

    // apply rotation adjustment from config
    q_type rotation;
    switch (rotationAdjustment)
    {
    case ROTATEX_HALF_PI:
      {
        q_from_axis_angle(rotation, 1.0, 0.0, 0.0, HALF_PI);
        q_mult(boneRotation, boneRotation, rotation);
        break;
      }
    case ROTATEX_NEGATIVE_HALF_PI:
      {
        q_from_axis_angle(rotation, 1.0, 0.0, 0.0, -HALF_PI);
        q_mult(boneRotation, boneRotation, rotation);
        break;
      }
    case ROTATEX_PI:
      {
        q_from_axis_angle(rotation, 1.0, 0.0, 0.0, PI);
        q_mult(boneRotation, boneRotation, rotation);
        break;
      }
    case ROTATEY_HALF_PI:
      {
        q_from_axis_angle(rotation, 0.0, 0.0, 1.0, HALF_PI);
        q_mult(boneRotation, boneRotation, rotation);
        break;
      }
    case ROTATEY_NEGATIVE_HALF_PI:
      {
        q_from_axis_angle(rotation, 0.0, 0.0, 1.0, -HALF_PI);
        q_mult(boneRotation, boneRotation, rotation);
        break;
      }
    case ROTATEY_PI:
      {
        q_from_axis_angle(rotation, 0.0, 0.0, 1.0, PI);
        q_mult(boneRotation, boneRotation, rotation);
        break;
      }
    case ROTATEZ_HALF_PI:
      {
        q_from_axis_angle(rotation, 0.0, 1.0, 0.0, HALF_PI);
        q_mult(boneRotation, boneRotation, rotation);
        break;
      }
    case ROTATEZ_NEGATIVE_HALF_PI:
      {
        q_from_axis_angle(rotation, 0.0, 1.0, 0.0, -HALF_PI);
        q_mult(boneRotation, boneRotation, rotation);
        break;
      }
    case ROTATEZ_PI:
      {
        q_from_axis_angle(rotation, 0.0, 1.0, 0.0, PI);
        q_mult(boneRotation, boneRotation, rotation);
        break;
      }
    }

    // get absolute transform matrix from tracker
    Matrix4 absoluteTransform = BI_IdentityMatrix4;
    TrackerToMatrix(boneRotation, bonePosition, absoluteTransform);

    // convert rotation matrix to VBS coord. system
    if (g_pClient->_upAxis == UP_AXIS_Z)
    {
      // Z-up RH to Y-up LH
      BI_Vector3 dir = absoluteTransform.GetDirection();
      dir.SetX(-absoluteTransform.GetDirection().X());
      dir.SetY(absoluteTransform.GetDirection().Z());
      dir.SetZ(-absoluteTransform.GetDirection().Y());
      BI_Vector3 up = absoluteTransform.GetUp();
      up.SetX(-absoluteTransform.GetUp().X());
      up.SetY(absoluteTransform.GetUp().Z());
      up.SetZ(-absoluteTransform.GetUp().Y());
      absoluteTransform.SetDirectionAndUp(dir, up);
    }
    else
    {
      // Y-up RH to Y-up LH
      BI_Vector3 dir = absoluteTransform.GetDirection();
      dir.SetX(-dir.X());
      BI_Vector3 up = absoluteTransform.GetUp();
      up.SetX(-up.X());
      absoluteTransform.SetDirectionAndUp(dir, up);
    }

    // Apply axis adjustment from config
    {
	    BI_Vector3 dir = absoluteTransform.GetDirection();
	    BI_Vector3 up = absoluteTransform.GetUp();
	    BI_Vector3 aside = absoluteTransform.GetAside();
	    
	    switch (axisAdjustment)
	    {
	    case UP_TO_NEGATIVE_UP: { absoluteTransform.SetDirectionAndUp(dir, -up); break; }
	    case UP_TO_DIR: { absoluteTransform.SetDirectionAndUp(up, dir); break; }      
	    case UP_TO_NEGATIVE_DIR: { absoluteTransform.SetDirectionAndUp(up, -dir); break; }
	    case UP_TO_ASIDE: { absoluteTransform.SetDirectionAndUp(dir, aside); break; }      
	    case UP_TO_NEGATIVE_ASIDE: { absoluteTransform.SetDirectionAndUp(dir, -aside); break; }
	    case DIR_TO_ASIDE: { absoluteTransform.SetDirectionAndAside(aside, dir); break; }
	    case DIR_TO_NEGATIVE_ASIDE: { absoluteTransform.SetDirectionAndAside(aside, -dir); break; }
	    default:
	      break;
	    }
    }

    // adjust player position in the game
    if (index == eSPINE)
    {
      _pelvisPrevXOffset = _pelvisXOffset;
      _pelvisPrevZOffset = _pelvisZOffset; 
      _pelvisXOffset = absoluteTransform.GetPos().X();
      _pelvisZOffset = absoluteTransform.GetPos().Z();
      float deltaX = _pelvisXOffset - _pelvisPrevXOffset;
      float deltaZ = _pelvisZOffset - _pelvisPrevZOffset;

      //const char* unitName = getName().c_str();
      float playerPosX, playerPosY, playerPosZ;
      GetUnitPos(playerPosX, playerPosZ, playerPosY);
      SetUnitPos(playerPosX + deltaX, playerPosZ + deltaZ, playerPosY);
    }
    
    // update transformation relatively to pelvis
    if (index != eWEAPON)
    {
	    BI_TVector3<float> pos = absoluteTransform.GetPos();
	    pos.SetX(pos.X() - _pelvisXOffset);
	    pos.SetZ(pos.Z() - _pelvisZOffset);
	    absoluteTransform.SetTranslation(pos);
    }

    // current bone's binding pose
    Matrix4 bindingPose = SkeletonDefinition.Bones[index];

    // inverse binding pose
    BI_TMatrix4<float> invBindingPose;
    invBindingPose.SetInvertGeneral4x4(bindingPose);

    // compute relative-to-binding-pose transform
    Matrix4 relativeToBP = BI_IdentityMatrix4;
    relativeToBP.SetMultiply(absoluteTransform, invBindingPose);

    Matrix4ToMatrix4f(relativeToBP, mat);

    // save transformations for fixed bones
    switch (index)
    {
    case eLEFT_HAND: { m_lastLeftHandBoneTransformation = mat; break; }
    case eRIGHT_HAND: { m_lastRightHandBoneTransformation = mat; break; }
    case eSPINE: { m_lastSpineBoneTransformation = mat; break; }
    case eHEAD: 
      {
        Vector3 headAbsolutePos = absoluteTransform.GetPos();   
        Matrix4f headRelative;
        Matrix4ToMatrix4f(relativeToBP, headRelative);

        cameraTransformation = headRelative;
        cameraTransformation.setPosition(headAbsolutePos.X(), headAbsolutePos.Y(), headAbsolutePos.Z());
        
        break;
      }
    default:
      break;
    }
  }
  else { mat.setIdentity(); }
 
  return true;
};