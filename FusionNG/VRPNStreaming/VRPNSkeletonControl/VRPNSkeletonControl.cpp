#include <string>

#include "pluginHeader.h"
#include "poseControlUnit.h"

#include "util/UnitUtilities.h"
#include "util/MissionUtilities.h"
#include "DisplayFunctions.h"

#include "../vrpn/vrpn/vrpn_Connection.h" // Missing this file?  Get the latest VRPN distro at
#include "../vrpn/vrpn/vrpn_Tracker.h"    // ftp://ftp.cs.unc.edu/pub/packages/GRIP/vrpn

#include "VRPNClient.h"

VRPNClient* g_pClient = NULL;

VBSPLUGIN_EXPORT void WINAPI OnSimulationStep(float deltaT)
{
  if (g_pClient)
  {
    g_pClient->OnSimStep();
  }
};


VBSPLUGIN_EXPORT const char* WINAPI PluginFunction(const char *input)
{
	static const char result[]="[1.0]";
  VBS2Fusion::MissionUtilities::getPlayer();

	if (input[0] == 'i')
	{	
    if (!g_pClient)
    {
      g_pClient = new VRPNClient();
      g_pClient->SetPoseControlEnabled(true);
    }
	}

	else if (input[0] == 'd')
	{
    g_pClient->SetPoseControlEnabled(false);
	}

	else if (input[0] == 'e')
	{
    g_pClient->SetPoseControlEnabled(true);
	}

	return result;
};

VBSPLUGIN_EXPORT void WINAPI OnUnload()
{
  if (g_pClient)
  {
    g_pClient->SetPoseControlEnabled(false);
    delete g_pClient;
    g_pClient = NULL;
  }
};

BOOL WINAPI DllMain(HINSTANCE hDll, DWORD fdwReason, LPVOID lpvReserved)
{
	switch(fdwReason)
	{
	case DLL_PROCESS_ATTACH:
		break;
	case DLL_PROCESS_DETACH:
		break;
	case DLL_THREAD_ATTACH:
		break;
	case DLL_THREAD_DETACH:
		break;
	}

	return TRUE;
};

ExecuteCommandType ExecuteCommand = NULL;
int ExecuteCmd(const char* command, char *result, int resultLength)
{ 
  ZeroMemory(result,resultLength);
  if (!ExecuteCommand) return 0;
  return ExecuteCommand(command, result, resultLength);
}

VBSPLUGIN_EXPORT void WINAPI RegisterCommandFnc(void *executeCommandFnc)
{
  ExecuteCommand = (ExecuteCommandType)executeCommandFnc;
}

// get current player position
void GetUnitPos(float& x, float& y, float&z)
{
  // get original position
  // Set orig pos
  char command[256];
  sprintf(command, "getPosASL2 player");

  // send command
  char resultBuff[256];
  if(!ExecuteCommand(command, resultBuff, 256))
  {
    char debugText[256];
    sprintf(debugText, "GetPos failed: %s\n", resultBuff);
    OutputDebugString(debugText);
    return;
  }

  // parse results
  int parse = sscanf(resultBuff, "[%f,%f,%f]", &x, &y, &z);
  if (parse != 3)
  {
    OutputDebugString("GetPos failed: can't parse command result\n");
    return;
  }
}

// set player position
void SetUnitPos(float x, float y, float z)
{
  char buf[512];
  sprintf(buf, "player setPosASL2 [ %f, %f, %f]", x, y, z);
  ExecuteCmd(buf);
}

bool IsInternalCamera()
{
  char buf[64];
  ExecuteCmd("cameraView", buf, 64);
  return (_stricmp(buf, "\"INTERNAL\"") == 0);
}
