//
// Sample draw functionality for VBS2NG fusion draw interface
// see main.cpp for more comments
//

// To use this plugin, copy sample_shader.hlsl to your VBS2NG folder
// then when starting a mission you should see colored spinning faces around you

#include "drawsample.h"
#include "../../Common/Essential/AppFrameWork.hpp"

#define SHADER_PATH "sample_shader.hlsl"

#define SAFE_RELEASE(p) {if((p)){(p)->Release();(p)=NULL;}}
extern ExecuteCommandType ExecuteCommand;

struct SIMPLEVERTEX {
    float x, y, z;
};

drawsample::drawsample(IDirect3DDevice9 *d3d)
{  
  _vbBox = NULL;
  _ps = NULL;
  _vs = NULL;
  _vertexDecl = NULL;
  _vbBox = NULL;

  _ready = false;

  LogF("Initializing drawsample...");

  // Compile pixel & vertex shaders
  // The VS & PS are contained in the same HLSL file, using functions "vsmain" and "psmain"
  LPD3DXBUFFER codevs, codeps;
  LPD3DXBUFFER err = NULL;

  if(D3DXCompileShaderFromFile(SHADER_PATH, NULL, NULL, "vsmain", "vs_3_0", NULL, &codevs, &err, NULL) != D3D_OK)
  {
    if(err) LogF("Error compiling vertex shader: %s", (char*)err->GetBufferPointer());
    else    LogF("Error compiling vertex shader (file not found?)");
    return;
  }

  if(D3DXCompileShaderFromFile(SHADER_PATH, NULL, NULL, "psmain", "ps_3_0", NULL, &codeps, &err, NULL) != D3D_OK)
  {
    if(err) LogF("Error compiling pixel shader: %s", (char*)err->GetBufferPointer());
    else    LogF("Error compiling pixel shader (file not found?)");
    return;
  }

  d3d->CreateVertexShader((DWORD*)codevs->GetBufferPointer(), &_vs);
  d3d->CreatePixelShader((DWORD*)codeps->GetBufferPointer(), &_ps);

  codevs->Release();
  codeps->Release();

  // Compile VS for drawing 2D elements "vsui"
  if(D3DXCompileShaderFromFile(SHADER_PATH, NULL, NULL, "vsui", "vs_3_0", NULL, &codevs, &err, NULL) != D3D_OK)
  {
    if(err) LogF("Error compiling vertex shader: %s", (char*)err->GetBufferPointer());
    else    LogF("Error compiling vertex shader (file not found?)");
    return;
  }

  d3d->CreateVertexShader((DWORD*)codevs->GetBufferPointer(), &_vsUI);
  codevs->Release();

  // Create vertex declaration
  // Similar to the FVF in fixed function pipeline, this sets the structure of our vertex data,
  // it must match the vertex data we use (SIMPLEVERTEX) and the vertex shader input (VS_INPUT in hlsl)
  D3DVERTEXELEMENT9 decl[] = {{0,
                               0,
                               D3DDECLTYPE_FLOAT3,
                               D3DDECLMETHOD_DEFAULT,
                               D3DDECLUSAGE_POSITION,
                               0},
                              D3DDECL_END()};
  if(d3d->CreateVertexDeclaration(decl, &_vertexDecl) != D3D_OK)
  {
    LogF("CreateVertexDeclaration failed!");
    return;
  }

  // Create a simple model
  // Note we must not use D3DPOOL_MANAGED
  if(d3d->CreateVertexBuffer(6*sizeof(SIMPLEVERTEX), D3DUSAGE_WRITEONLY, 0, D3DPOOL_DEFAULT, &_vbBox, NULL ) != D3D_OK)
  {
    LogF("CreateVertexBuffer failed!");
    return;
  }

  // Two triangles
  static SIMPLEVERTEX vdata[6] = {
    {-0.5f, 0, 0},
    {-0.5f, 1, 0},
    { 0.5f, 0, 0},

    {-0.5f, 1, 0},
    { 0.5f, 1, 0},
    { 0.5f, 0, 0},
  };

  void *data = NULL;
  _vbBox->Lock(0, 0, &data, NULL);
  memcpy(data, vdata, 6*sizeof(SIMPLEVERTEX));
  _vbBox->Unlock();

  _ready = true;
}

drawsample::~drawsample()
{
  LogF("Deinitializing drawsample...");
  _ready = false;

  SAFE_RELEASE(_vertexDecl);
  SAFE_RELEASE(_ps);
  SAFE_RELEASE(_vs);
  SAFE_RELEASE(_vbBox);
}

void drawsample::createObjects()
{
  // Create a bunch of objects
  const int numObjects = 10;
  for(int i=0;i<numObjects;i++)
  {
    float x,y,z;
    float r,g,b;
    float spin;

    x=y=z=0; // Not used in this sample

    // Random color
    r = (float)rand()/(float)RAND_MAX;
    g = (float)rand()/(float)RAND_MAX;
    b = (float)rand()/(float)RAND_MAX;

    // Random spin
    spin = ((float)rand()/(float)RAND_MAX) + 0.5f;

    simpleObject o(x,y,z,r,g,b,spin);
    _objects.push_back(o);
  }
}


float time = 0;
void drawsample::manageObjects(IDirect3DDevice9 *d3d, float deltaT)
{
  // This is an opportunity to do dynamic resource management like creation and destruction of vertex buffers, textures and shaders
  // Unlike in drawObjects, we have access to the Direct3D9 device here.

  if(deltaT == deltaT)
  {
    time += deltaT;
  }
}

void drawsample::setLights(float eyeAccom, const pluginLightDir &mainLight)
{
  _eyeAccom = eyeAccom;
  _mainLight = mainLight;
}

void drawsample::drawBox2D(VBS2CBInterface *cbi, float x, float y, float w, float h, float r, float g, float b)
{
  if(!_ready) return;

  setDefaultState(cbi);

  cbi->SetPixelShader(_ps);
  cbi->SetVertexShader(_vsUI);
  cbi->SetVertexDeclaration(_vertexDecl);

  cbi->SetRenderState(D3DRS_CULLMODE, D3DCULL_NONE);

  float color[4] = {r, g, b, 1.0f};
  cbi->SetPixelShaderConstantF(0, color, 1);  // Pixel Shader register C0 = float4 matColor in HLSL

  SIMPLEVERTEX vdata[] = {
    {x,    y, 0.1f},
    {x+w,  y, 0.1f},
    {x,  y+h, 0.1f},
    {x+w,y+h, 0.1f},
  };
  cbi->DrawPrimitiveUP(D3DPT_TRIANGLESTRIP, 2, vdata, sizeof(SIMPLEVERTEX));
}

void drawsample::drawObjects(VBS2CBInterface *cbi, const FrustumSpecNG &cameraFrustum)
{
  // Drawing is done here. Note we do not have access to the Direct3D device here and 
  // we also must not even attempt to access it here in any way. Any D3D resources that we
  // need here must have been created previously

  if(!_ready) return;

  // If no objects exist, attempt to create them
  if(_objects.size() == 0)
    createObjects();

  if(_objects.size() == 0)
    return;

  // Set D3D device to default state
  setDefaultState(cbi);

  // Create projection matrix from engine frustum data
  D3DXMATRIX matProj;
  float m = cameraFrustum.clipDistNear;
  float left = cameraFrustum.projTanLeft * m;
  float right = cameraFrustum.projTanRight * m;
  float bottom = cameraFrustum.projTanBottom * m;
  float top = cameraFrustum.projTanTop * m;  
  D3DXMatrixPerspectiveOffCenterLH(&matProj, -left, right, -bottom, top, cameraFrustum.clipDistNear,cameraFrustum.clipDistFar);

  // Create view matrix from engine camera data
  D3DXMATRIX matView;  
  D3DXVECTOR3 cpoint = D3DXVECTOR3((float)cameraFrustum.pointPosX, (float)cameraFrustum.pointPosY, (float)cameraFrustum.pointPosZ);
  D3DXVECTOR3 ctarget = cpoint + D3DXVECTOR3(cameraFrustum.viewDirX, cameraFrustum.viewDirY, cameraFrustum.viewDirZ); // Convert camera direction vector to look-at-target
  D3DXVECTOR3 cup = D3DXVECTOR3(cameraFrustum.viewUpX, cameraFrustum.viewUpY, cameraFrustum.viewUpZ);
  D3DXMatrixLookAtLH(&matView, &cpoint, &ctarget, &cup);
  
  // Create view*projection matrix and transpose
  D3DXMATRIX viewProj = matView * matProj;
  D3DXMatrixTranspose(&viewProj, &viewProj);
  
  // Set the matrices to VS constants so they can be accessed in the vertex shader
  cbi->SetVertexShaderConstantF(4, viewProj, 4); // Vertex Shader register C4 = float4x4 MatViewProj in HLSL

  BOOL foo[]={true,false,true};
  cbi->SetVertexShaderConstantB(0, foo, 3);
  cbi->SetPixelShaderConstantB(0, foo, 3);
  int kala[]={1,2,3,4};
  cbi->SetVertexShaderConstantI(0, kala, 1);
  cbi->SetPixelShaderConstantI(0, kala, 1);

  // Render preparation, set shaders, render states, etc.
  cbi->SetPixelShader(_ps);
  cbi->SetVertexShader(_vs);
  cbi->SetVertexDeclaration(_vertexDecl);
  cbi->SetStreamSource(0, _vbBox, 0, sizeof(SIMPLEVERTEX));
  cbi->SetRenderState(D3DRS_ZENABLE, D3DZB_TRUE);

  // Draw multiple copies of the _vbBox object
  for(int i=0;i<_objects.size();i++)
  {
    const simpleObject &o = _objects[i];

    // Create world matrix & transpose
    D3DXMATRIX world, pos, rot, pos2;
    //D3DXMatrixTranslation(&pos, o._x, o._y, o._z);
    D3DXMatrixTranslation(&pos, (float)cameraFrustum.pointPosX, (float)cameraFrustum.pointPosY, (float)cameraFrustum.pointPosZ);
    D3DXMatrixRotationAxis(&rot, &D3DXVECTOR3(0,1,0), time/5.0f*o._spin );  // Spin the models around
    float t = (float)time+i*10;
    D3DXMatrixTranslation(&pos2, sin(t)*5, -0.5, cos(t)*5);
    world = rot * pos * pos2;
    D3DXMatrixTranspose(&world, &world);

    // Set the matrices to VS constants so they can be accessed in the vertex shader
    cbi->SetVertexShaderConstantF(0, world, 4);    // Vertex Shader register C0 = float4x4 MatWorld in HLSL

    // Set material color to pixel shader constant

    // The object source color (o._r, _g & _b) is multiplied by eye accomodation value and main light ambient color
    // _eyeAccom is the size of the camera (or eye) aperture. At daytime it can be around ~0.0004 and during night ~4.0
    // Main light is either sun or moon, during daytime the brightness can be around ~400 and during night time ~0.001

    // Note the source color is not limited to range of 0..1, we could use ie. higher values to have more intense color objects.
    // Also note the reactive nature of the eye accomodation - for example if we draw significant bright objects in the screen,
    // the _eyeAccom will automatically adjust to have them "exposed" correctly.
    float color[4] = {
      o._r * _eyeAccom * _mainLight.ambient.x,
      o._g * _eyeAccom * _mainLight.ambient.y,
      o._b * _eyeAccom * _mainLight.ambient.z,
      1.0f};
    cbi->SetPixelShaderConstantF(0, color, 1);  // Pixel Shader register C0 = float4 matColor in HLSL

    // Draw front side
    cbi->SetRenderState(D3DRS_CULLMODE, D3DCULL_CCW);
    cbi->DrawPrimitive(D3DPT_TRIANGLELIST, 0, 2);

    // Set dimmer material color to pixel shader constant
    color[0] *= 0.2f;
    color[1] *= 0.2f;
    color[2] *= 0.2f;
    cbi->SetPixelShaderConstantF(0, color, 1); // Pixel Shader register C0 = float4 matColor in HLSL

    // Draw back side
    cbi->SetRenderState(D3DRS_CULLMODE, D3DCULL_CW);
    cbi->DrawPrimitive(D3DPT_TRIANGLELIST, 0, 2);
  }
}

void drawsample::setDefaultState(VBS2CBInterface *d3dd)
{
  // Set D3D device properties to their default state
  // This is to ensure things are in a predictable state when coming from engine rendering code
  const float zerof = 0.0f;
  const float onef = 1.0f;
  #define ZEROf	*((DWORD*) (&zerof))
  #define ONEf	*((DWORD*) (&onef))

  d3dd->SetRenderState(D3DRS_ZENABLE, D3DZB_FALSE);
  d3dd->SetRenderState(D3DRS_FILLMODE, D3DFILL_SOLID);
  d3dd->SetRenderState(D3DRS_SHADEMODE, D3DSHADE_GOURAUD);
  d3dd->SetRenderState(D3DRS_ZWRITEENABLE, TRUE);
  d3dd->SetRenderState(D3DRS_ALPHATESTENABLE, FALSE);
  d3dd->SetRenderState(D3DRS_LASTPIXEL, TRUE);
  d3dd->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_ONE);
  d3dd->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_ZERO);
  d3dd->SetRenderState(D3DRS_CULLMODE, D3DCULL_CCW);
  d3dd->SetRenderState(D3DRS_ZFUNC, D3DCMP_LESSEQUAL);
  d3dd->SetRenderState(D3DRS_ALPHAREF, 0);
  d3dd->SetRenderState(D3DRS_ALPHAFUNC, D3DCMP_ALWAYS);
  d3dd->SetRenderState(D3DRS_ALPHABLENDENABLE, FALSE);
  d3dd->SetRenderState(D3DRS_FOGENABLE, FALSE);
  d3dd->SetRenderState(D3DRS_SPECULARENABLE, FALSE);
  d3dd->SetRenderState(D3DRS_FOGCOLOR, 0);
  d3dd->SetRenderState(D3DRS_FOGTABLEMODE, D3DFOG_NONE);
  d3dd->SetRenderState(D3DRS_FOGSTART, ZEROf);
  d3dd->SetRenderState(D3DRS_FOGEND, ONEf);
  d3dd->SetRenderState(D3DRS_FOGDENSITY, ONEf);
  d3dd->SetRenderState(D3DRS_RANGEFOGENABLE, FALSE);
  d3dd->SetRenderState(D3DRS_STENCILENABLE, FALSE);
  d3dd->SetRenderState(D3DRS_STENCILFAIL, D3DSTENCILOP_KEEP);
  d3dd->SetRenderState(D3DRS_STENCILZFAIL, D3DSTENCILOP_KEEP);
  d3dd->SetRenderState(D3DRS_STENCILPASS, D3DSTENCILOP_KEEP);
  d3dd->SetRenderState(D3DRS_STENCILFUNC, D3DCMP_ALWAYS);
  d3dd->SetRenderState(D3DRS_STENCILREF, 0);
  d3dd->SetRenderState(D3DRS_STENCILMASK, 0xffffffff);
  d3dd->SetRenderState(D3DRS_STENCILWRITEMASK, 0xffffffff);
  d3dd->SetRenderState(D3DRS_TEXTUREFACTOR, 0xffffffff);
  d3dd->SetRenderState(D3DRS_WRAP0, 0);
  d3dd->SetRenderState(D3DRS_WRAP1, 0);
  d3dd->SetRenderState(D3DRS_WRAP2, 0);
  d3dd->SetRenderState(D3DRS_WRAP3, 0);
  d3dd->SetRenderState(D3DRS_WRAP4, 0);
  d3dd->SetRenderState(D3DRS_WRAP5, 0);
  d3dd->SetRenderState(D3DRS_WRAP6, 0);
  d3dd->SetRenderState(D3DRS_WRAP7, 0);
  d3dd->SetRenderState(D3DRS_CLIPPING, TRUE);
  d3dd->SetRenderState(D3DRS_AMBIENT, 0);
  d3dd->SetRenderState(D3DRS_LIGHTING, TRUE);
  d3dd->SetRenderState(D3DRS_FOGVERTEXMODE, D3DFOG_NONE);
  d3dd->SetRenderState(D3DRS_COLORVERTEX, TRUE);
  d3dd->SetRenderState(D3DRS_LOCALVIEWER, TRUE);
  d3dd->SetRenderState(D3DRS_NORMALIZENORMALS, FALSE);
  d3dd->SetRenderState(D3DRS_DIFFUSEMATERIALSOURCE, D3DMCS_COLOR1);
  d3dd->SetRenderState(D3DRS_SPECULARMATERIALSOURCE, D3DMCS_COLOR2);
  d3dd->SetRenderState(D3DRS_AMBIENTMATERIALSOURCE, D3DMCS_MATERIAL);
  d3dd->SetRenderState(D3DRS_EMISSIVEMATERIALSOURCE, D3DMCS_MATERIAL);
  d3dd->SetRenderState(D3DRS_CLIPPLANEENABLE, 0);
  d3dd->SetRenderState(D3DRS_POINTSIZE_MIN, ONEf);
  d3dd->SetRenderState(D3DRS_POINTSPRITEENABLE, FALSE);
  d3dd->SetRenderState(D3DRS_MULTISAMPLEANTIALIAS, TRUE);
  d3dd->SetRenderState(D3DRS_MULTISAMPLEMASK, 0xffffffff);
  d3dd->SetRenderState(D3DRS_PATCHEDGESTYLE, D3DPATCHEDGE_DISCRETE);
  d3dd->SetRenderState(D3DRS_POINTSIZE_MAX, ONEf);
  d3dd->SetRenderState(D3DRS_COLORWRITEENABLE, 0x0000000f);
  d3dd->SetRenderState(D3DRS_BLENDOP, D3DBLENDOP_ADD);
  d3dd->SetRenderState(D3DRS_POSITIONDEGREE, D3DDEGREE_CUBIC);
  d3dd->SetRenderState(D3DRS_NORMALDEGREE, D3DDEGREE_LINEAR);
  d3dd->SetRenderState(D3DRS_SCISSORTESTENABLE, FALSE);
  d3dd->SetRenderState(D3DRS_SLOPESCALEDEPTHBIAS, 0);
  d3dd->SetRenderState(D3DRS_MINTESSELLATIONLEVEL, ONEf);
  d3dd->SetRenderState(D3DRS_MAXTESSELLATIONLEVEL, ONEf);
  d3dd->SetRenderState(D3DRS_ADAPTIVETESS_X, ZEROf);
  d3dd->SetRenderState(D3DRS_ADAPTIVETESS_Y, ZEROf);
  d3dd->SetRenderState(D3DRS_ADAPTIVETESS_Z, ONEf);
  d3dd->SetRenderState(D3DRS_ADAPTIVETESS_W, ZEROf);
  d3dd->SetRenderState(D3DRS_ENABLEADAPTIVETESSELLATION, FALSE);
  d3dd->SetRenderState(D3DRS_TWOSIDEDSTENCILMODE, FALSE);
  d3dd->SetRenderState(D3DRS_CCW_STENCILFAIL, D3DSTENCILOP_KEEP);
  d3dd->SetRenderState(D3DRS_CCW_STENCILZFAIL, D3DSTENCILOP_KEEP);
  d3dd->SetRenderState(D3DRS_CCW_STENCILPASS, D3DSTENCILOP_KEEP);
  d3dd->SetRenderState(D3DRS_CCW_STENCILFUNC, D3DCMP_ALWAYS);
  d3dd->SetRenderState(D3DRS_COLORWRITEENABLE1, 0x0000000f);
  d3dd->SetRenderState(D3DRS_COLORWRITEENABLE2, 0x0000000f);
  d3dd->SetRenderState(D3DRS_COLORWRITEENABLE3, 0x0000000f);
  d3dd->SetRenderState(D3DRS_BLENDFACTOR, 0xffffffff);
  d3dd->SetRenderState(D3DRS_SRGBWRITEENABLE, 0);
  d3dd->SetRenderState(D3DRS_DEPTHBIAS, 0);
  d3dd->SetRenderState(D3DRS_WRAP8, 0);
  d3dd->SetRenderState(D3DRS_WRAP9, 0);
  d3dd->SetRenderState(D3DRS_WRAP10, 0);
  d3dd->SetRenderState(D3DRS_WRAP11, 0);
  d3dd->SetRenderState(D3DRS_WRAP12, 0);
  d3dd->SetRenderState(D3DRS_WRAP13, 0);
  d3dd->SetRenderState(D3DRS_WRAP14, 0);
  d3dd->SetRenderState(D3DRS_WRAP15, 0);
  d3dd->SetRenderState(D3DRS_SEPARATEALPHABLENDENABLE, FALSE);
  d3dd->SetRenderState(D3DRS_SRCBLENDALPHA, D3DBLEND_ONE);
  d3dd->SetRenderState(D3DRS_DESTBLENDALPHA, D3DBLEND_ZERO);
  d3dd->SetRenderState(D3DRS_BLENDOPALPHA, D3DBLENDOP_ADD);
  d3dd->SetRenderState(D3DRS_DITHERENABLE, FALSE);
  d3dd->SetRenderState(D3DRS_VERTEXBLEND, D3DVBF_DISABLE);
  d3dd->SetRenderState(D3DRS_POINTSIZE, ONEf);
  d3dd->SetRenderState(D3DRS_POINTSCALEENABLE, FALSE);
  d3dd->SetRenderState(D3DRS_POINTSCALE_A, ONEf);
  d3dd->SetRenderState(D3DRS_POINTSCALE_B, ZEROf);
  d3dd->SetRenderState(D3DRS_POINTSCALE_C, ZEROf);
  d3dd->SetRenderState(D3DRS_INDEXEDVERTEXBLENDENABLE, FALSE);
  d3dd->SetRenderState(D3DRS_TWEENFACTOR, ZEROf);
  d3dd->SetRenderState(D3DRS_ANTIALIASEDLINEENABLE, FALSE);

  for(int i=0;i<8;i++) {
	  d3dd->SetSamplerState(i, D3DSAMP_MINFILTER, D3DTEXF_LINEAR);
	  d3dd->SetSamplerState(i, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR);
	  d3dd->SetSamplerState(i, D3DSAMP_MIPFILTER, D3DTEXF_NONE);
	  d3dd->SetSamplerState(i, D3DSAMP_MIPMAPLODBIAS, 0);
	  d3dd->SetSamplerState(i, D3DSAMP_MAXMIPLEVEL, 0);			
	  d3dd->SetSamplerState(i, D3DSAMP_MAXANISOTROPY, 1);
	  d3dd->SetSamplerState(i, D3DSAMP_SRGBTEXTURE, 0);			
	  d3dd->SetSamplerState(i, D3DSAMP_ELEMENTINDEX, 0);
	  d3dd->SetSamplerState(i, D3DSAMP_DMAPOFFSET, 0);			
	  d3dd->SetSamplerState(i, D3DSAMP_ADDRESSU, D3DTADDRESS_WRAP);
	  d3dd->SetSamplerState(i, D3DSAMP_ADDRESSV, D3DTADDRESS_WRAP);
	  d3dd->SetSamplerState(i, D3DSAMP_ADDRESSW, D3DTADDRESS_WRAP);

    d3dd->SetTexture(i, NULL);
  }

  #undef ZEROf
  #undef ONEf
}