#include <Windows.h>
#include <string>
#include "FusionTest.h"
#include "resource.h"

///////////////////////////////////////////////////////////////////////////////
// Procedure of new callback addition                                        //
//---------------------------------------------------------------------------//
// - add new callback to CALLBACKS factory                                   //
// - if number of callbacks exceeds MAX_CALLBACKS_SELECTED it has to be      //
//   extended                                                                //    
///////////////////////////////////////////////////////////////////////////////

// create dependency on Fusion
#include "VBS2FusionAppContext.h"
VBS2Fusion::VBS2FusionAppContext appContext;

HWND hWnd; ///< dialog window handle
HWND hOut; ///< output control handle
HWND hList; ///< list control handle

std::string output; ///< ouput string

bool pause = false; ///< pause output flag

class Matrix4P;
class GameValue;
struct VBSParameters;
struct VBSParametersEx;
struct FrustumSpec;
struct paramPreDraw;
struct paramInitDraw;
struct paramDrawnSky;
struct paramDrawnPass;
struct paramFilledBackbuffer;
struct paramDeviceInit;
struct paramDeviceInvalidate;
struct paramDeviceRestore;
struct GeometryObject;
struct Interop_EntityCreateData;
struct Interop_EntityUpdateData;
struct Interop_EntityDeleteData;
struct Interop_DesignatorCreateData;
struct Interop_DesignatorUpdateData;
struct Interop_TransmitterCreateData;
struct Interop_TransmitterUpdateData;
struct Interop_TransmitterDeleteData;
struct Interop_SignalData;
struct Interop_SignalDataFCS_JPEG;
struct Interop_EntityId;
struct Interop_SimStateData;
struct Interop_FireWeaponData;
struct Interop_DetonationData;
struct Interop_DataScriptData;
struct Interop_GameEntityTypeInfo;
struct Interop_CNRLogEvent;

typedef float (WINAPI *VIRTUALINPUTDEVICE_DEVICE_GETVALUE)(UINT device_id);
typedef UINT  (WINAPI *VIRTUALINPUTDEVICE_DEVICE_REGISTERCALLBACK)(const char *plugin_name, UINT device_id, UINT device_type, const char *device_description, VIRTUALINPUTDEVICE_DEVICE_GETVALUE fptr_get_device_value);
typedef UINT (WINAPI *VIRTUALINPUTDEVICE_DEVICE_UNREGISTERCALLBACK)(const char *plugin_name, UINT device_id);

// All callbacks factory
// function name, return type, parameters, return value
#define CALLBACKS(XX) \
  XX(OnLoad, int, (VBSParameters* params), 0) \
  XX(OnLoadEx, int, (VBSParametersEx* params), 0) \
  XX(OnUnload, void, (), ) \
  XX(FusionFunction, const char*, (const char* name, const char* input), "") \
  XX(OnSimulationStep, void, (float deltaT), ) \
  XX(OnAfterSimulation, void, (float deltaT), ) \
  XX(OnSetFrustum, void, (FrustumSpec* frustum), ) \
  XX(OnModelBinarized, void, (const char* p3dFile, const char* exportDirectory), ) \
  XX(OnMissionStart, void, (), ) \
  XX(OnMissionEnd, void, (), ) \
  XX(OnMissionLoad, void, (const char* missionName), ) \
  XX(OnMissionUnload, void, (const char* missionName), ) \
  XX(OnModifyUpperBodyTorso, void, (Matrix4P& torso), ) \
  XX(OnModifyUpperBodyTorsoUnit, bool, (Matrix4P& torso, const GameValue& unit), false) \
  XX(OnModifyUpperBodyView, void, (Matrix4P& view), ) \
  XX(OnModifyUpperBodyViewUnit, bool, (Matrix4P& view, const GameValue& unit), false) \
  XX(OnModifyUpperBodyAim, void, (Matrix4P& aim), ) \
  XX(OnModifyUpperBodyAimUnit, bool, (Matrix4P& aim, const GameValue& unit), false) \
  XX(OnModifyBone, void, (Matrix4P& mat, int skeletonIndex), ) \
  XX(OnModifyBoneUnit, bool, (Matrix4P& mat, int skeletonIndex, const GameValue& unit), false) \
  XX(OnNgPreDraw, void, (paramPreDraw* param, DWORD paramSize), ) \
  XX(OnNgInitDraw, void, (paramInitDraw* param, DWORD paramSize), ) \
  XX(OnNgDrawnSky, void, (paramDrawnSky* param, DWORD paramSize), ) \
  XX(OnNgDrawnPass, void, (paramDrawnPass* param, DWORD paramSize), ) \
  XX(OnNgFilledBackbuffer, void, (paramFilledBackbuffer* param, DWORD paramSize), ) \
  XX(OnNgDeviceInit, void, (paramDeviceInit* param, DWORD paramSize), ) \
  XX(OnNgDeviceInvalidate, void, (paramDeviceInvalidate* param, DWORD paramSize), ) \
  XX(OnNgDeviceRestore, void, (paramDeviceRestore* param, DWORD paramSize), ) \
  XX(AIPathRequest, int, (const char* pluginName, unsigned int id, float* from, float* to, const char* navmeshName), 0) \
  XX(UpdateNavmesh, void, (const char* navmesh, GeometryObject* geom), ) \
  XX(SetNavmeshBlockers, void, (const char* navmesh, GeometryObject* geom), ) \
  XX(DrawNavmesh, void, (const char* objectName), ) \
  XX(IsOnNavmesh, bool, (const char* navmeshName, float* pos), false) \
  XX(DrawNavmeshExt, void, (void* data, const char* navmeshName, bool draw), ) \
  XX(SetFSM, void, (const char* pluginName, void* object, const char* fsmName), ) \
  XX(RemoveFSM, void, (const char* pluginName, void* object, const char* fsmName), ) \
  XX(Interop_Init, bool, (), true) \
  XX(Interop_InitWithPath, bool, (const char* path), true) \
  XX(Interop_IsInitialised, bool, (), true) \
  XX(Interop_Tick, int, (int n), 0) \
  XX(Interop_Shutdown, void, (), ) \
  XX(Interop_IsStopped, bool, (), false) \
  XX(Interop_SetOriginLL, void, (const char* latitude, const char* longitude), ) \
  XX(Interop_SetOriginUTM, void, (double easting, double northing, long zone, char hemisphere), ) \
  XX(Interop_EntityCreated, bool, (const Interop_EntityCreateData* data), false) \
  XX(Interop_EntityUpdated, void, (const Interop_EntityUpdateData* data), ) \
  XX(Interop_EntityDeleted, void, (const Interop_EntityDeleteData* id), ) \
  XX(Interop_DesignatorCreated, void, (const Interop_DesignatorCreateData* data), ) \
  XX(Interop_DesignatorUpdated, void, (const Interop_DesignatorUpdateData* data), ) \
  XX(Interop_TransmitterCreated, void, (const Interop_TransmitterCreateData* data), ) \
  XX(Interop_TransmitterUpdated, void, (const Interop_TransmitterUpdateData* data), ) \
  XX(Interop_TransmitterDeleted, void, (const Interop_TransmitterDeleteData* data), ) \
  XX(Interop_SendSignal, void, (const Interop_SignalData* sigData), ) \
  XX(Interop_SendEncodedFCS_JPEG, void, (const  Interop_TransmitterCreateData* transmitter, const Interop_SignalDataFCS_JPEG* data), ) \
  XX(Interop_SetMainPlayerID, void, (const Interop_EntityId* mainPlayerID), ) \
  XX(Interop_SimulationStateChanged, void, (const Interop_SimStateData* data), ) \
  XX(Interop_WeaponFired, void, (const Interop_FireWeaponData* data), ) \
  XX(Interop_MunitionDetonation, void, (const Interop_DetonationData* data), ) \
  XX(Interop_SendSetDataPDU, void, (const Interop_DataScriptData* scriptData), ) \
  XX(Interop_SendDataPDU, void, (const Interop_DataScriptData* scriptData), ) \
  XX(Interop_RefreshGameTypeInfoCache, void, (const Interop_GameEntityTypeInfo* list, int numElements), ) \
  XX(Interop_EnableCallbacks, void, (), ) \
  XX(Interop_DisableCallbacks, void, (), ) \
  XX(Interop_CNRLogInitRemoteLogger, int, (), 0) \
  XX(Interop_CNRLogReleaseRemoteLogger, int, (), 0) \
  XX(Interop_CNRLogStopRecording, int, (), 0) \
  XX(Interop_CNRLogStartRecording, int, (), 0) \
  XX(Interop_CNRLogIsRecording, int, (bool& isRecording), 0) \
  XX(Interop_CNRLogOpenSession, int, (const char* sessionName), 0) \
  XX(Interop_CNRLogGetEvent, bool, (int index, Interop_CNRLogEvent* newEvent), false) \
  XX(Interop_CNRLogGetEventSize, int, (), 0) \
  XX(Interop_CNRLogStopPlaying, int, (), 0) \
  XX(Interop_CNRLogPlayOneRecord, int, (int index), 0) \
  XX(Interop_CNRLogPlayAllRecords, int, (), 0) \
  XX(Interop_CNRLogGetStartTime, __int64, (), 0) \
  XX(Interop_SetCreateEntityCallback, void, (Interop_EntityId (*fPtr)(Interop_EntityCreateData* data)), ) \
  XX(Interop_SetUpdateEntityCallback, void, (int (*fPtr)(Interop_EntityUpdateData* data)), ) \
  XX(Interop_SetDeleteEntityCallback, void, (void (*fPtr)(Interop_EntityDeleteData* data)), ) \
  XX(Interop_SetCreateDesignatorCallback, void, (void (*fPtr)(Interop_DesignatorCreateData* data)), ) \
  XX(Interop_SetUpdateDesignatorCallback, void, (void (*fPtr)(Interop_DesignatorUpdateData* data)), ) \
  XX(Interop_SetFireWeaponCallback, void, (void (*fPtr)(Interop_FireWeaponData* data)), ) \
  XX(Interop_SetDetonateMunitionCallback, void, (void (*fPtr)(Interop_DetonationData* data)), ) \
  XX(Interop_SetExecuteCommandCallback, void, (int (*fPtr)(const char* command, char* result, int resultLength)), ) \
  XX(Interop_SetRequestGameTypeInfoRefreshCallback, void, (void (*fPtr)()), ) \
  XX(Interop_SetCreateTransmitterCallback, void, (Interop_EntityId (*fPtr)(Interop_TransmitterCreateData* data)), ) \
  XX(Interop_SetUpdateTransmitterCallback, void, (int (*fPtr)(Interop_TransmitterUpdateData* data)), ) \
  XX(Interop_SetDeleteTransmitterCallback, void, (void (*fPtr)(Interop_TransmitterDeleteData* data)), ) \
  XX(Interop_SetSendSignalUnitCallback, void, (void (*fPtr)(Interop_SignalData* data)), ) \
  XX(RegisterVirtualInputDevice, void, (VIRTUALINPUTDEVICE_DEVICE_REGISTERCALLBACK fptr_register_device), ) \
  XX(UnregisterVirtualInputDevice, void, (VIRTUALINPUTDEVICE_DEVICE_UNREGISTERCALLBACK fptr_unregister_device), ) \
  XX(OnKeyInput, bool, (const RAWKEYBOARD* keyData, DWORD keyTime), true) \
  XX(OnMouseInput, bool, (const RAWMOUSE* mouseData, DWORD mouseTime), true) \
  XX(GetInfo, const char*, (const char* name), "") \

#define CALLBACKS_STATE_DEF(name, ret, param, defVal) bool name;
#define CALLBACKS_STATE_INIT(name, ret, param, defVal) name = false;

// warning: once there is more than 300 callbacks this should be extended
#define MAX_CALLBACKS_SELECTED 300
struct Callbacks
{
  int listSelect[MAX_CALLBACKS_SELECTED]; ///< list of selected rows
  int selectCnt; ///< number of selected rows

  CALLBACKS(CALLBACKS_STATE_DEF);

  Callbacks()
  {
    memset(&listSelect, -1, sizeof(listSelect));
    selectCnt = 0;
    CALLBACKS(CALLBACKS_STATE_INIT);
  }

  //! unselect all callbacks
  void UnselectAll()
  {
    CALLBACKS(CALLBACKS_STATE_INIT);
  }
} GCallbacks;

#define TO_STR_(x) #x
#define TO_STR(x) TO_STR_(x)
#define NL(text) text##"\r\n"
#define LOG(callback) \
  if (!pause && GCallbacks.callback) \
  { \
    output.insert(0, NL(TO_STR(callback))); \
    SendMessage(hOut, WM_SETTEXT, 0, (LPARAM)output.c_str()); \
  } \

#define CALLBACKS_CREATE_FUNC(name, ret, param, defVal) \
  ret WINAPI name param \
  { \
    LOG(name); \
    return defVal; \
  } \

CALLBACKS(CALLBACKS_CREATE_FUNC)

#define CALLBACKS_LIST_REG(name, ret, param, defVal) \
 index = (int)SendMessage(hList, LB_ADDSTRING, 0, (LPARAM)TO_STR(name)); \
 SendMessage(hList, LB_SETITEMDATA, (WPARAM)index, (LPARAM)&GCallbacks.name); \

//! Dialog's procedure function.
INT_PTR CALLBACK DlgProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
  switch (message)
  {
    case WM_INITDIALOG:
    {
      // get control handles
      hOut = GetDlgItem(hDlg, IDC_OUTPUT);
      hList = GetDlgItem(hDlg, IDC_CALLBACK_LIST);

      // fill list with callbacks and pointers to their state in Callbacks struct
      int index;
      CALLBACKS(CALLBACKS_LIST_REG);

      break;
    }    

    case WM_COMMAND:
    {
      switch (HIWORD(wParam))
      {
        case BN_CLICKED:
        {
          if (LOWORD(wParam) == IDC_PAUSE)
          {
            // pause/unpause output
            pause = !pause;
          }
          else if (LOWORD(wParam) == IDC_CLEAR)
          {
            // clear output and refresh control
            output.clear();
            SendMessage(hOut, WM_SETTEXT, 0, (LPARAM)output.c_str());
          }

          break;
        }

        case LBN_SELCHANGE:
        {
          if (LOWORD(wParam) == IDC_CALLBACK_LIST)
          {
            // refresh callback selection
            GCallbacks.UnselectAll();
            GCallbacks.selectCnt = (int)SendMessage(hList, LB_GETSELITEMS, (WPARAM)MAX_CALLBACKS_SELECTED, (LPARAM)GCallbacks.listSelect);
            for (int i = 0; i < GCallbacks.selectCnt; ++i)
            {
              bool* select = (bool*)SendMessage(hList, LB_GETITEMDATA, (WPARAM)GCallbacks.listSelect[i], 0);
              *select = true;
            }
          }

          break;
        }
      }

      break;
    }

    case WM_QUIT:
    {
      // called when window is about to be destroyed
      PostQuitMessage(0);
      break;
    }
  }

  return FALSE;
}

//! Create window.
void CreateDlgWindow(LPVOID hInst)
{
  hWnd = CreateDialog((HINSTANCE)hInst, MAKEINTRESOURCE(IDD_WINDOW), NULL, DlgProc);

  ShowWindow(hWnd, SW_SHOW);
  UpdateWindow(hWnd);
}

//! Destroy window.
void DestroyDlgWindow()
{
  DestroyWindow(hWnd);
}

//! Main DLL function
BOOL WINAPI DllMain(HINSTANCE hDll, DWORD fdwReason, LPVOID lpvReserved)
{
  switch(fdwReason)
  {
  case DLL_PROCESS_ATTACH:
    CreateDlgWindow(hDll);
    break;
  case DLL_PROCESS_DETACH:
    DestroyDlgWindow();
    break;
  case DLL_THREAD_ATTACH:
    break;
  case DLL_THREAD_DETACH:
    break;
  }
  return TRUE;
}