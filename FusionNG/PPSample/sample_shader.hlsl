//
// Sample HLSL shader containing vertex- and pixel shader
// 
// Copy this to your VBS2NG folder !
//

// globals defined with shader constants. 
// Values for these are set with SetVertexShaderConstantF/SetPixelShaderConstantF

// Registers for vertex shader
float4x4 MatWorld : register(c0);	   // World matrix, size is 4x4 floats so uses up registers C0 - C4
float4x4 MatViewProj : register(c4); // View-projection matrix, uses registers C4-C8

// Registers for pixel shader
float4 matColor : register(c0);	     // Material color

// input struct for vertex shader
// other values could be added here, like UV coordinates and surface normals
// in that case, also the vertex declaration (_vertexDecl) would need to be changed
// and also the vertex data (SIMPLEVERTEX)
struct VS_INPUT
{
    float4 Position   : POSITION;
};

// output struct for vertex shader
// the VS outputs to this to be used as pixel shader input
// if we were to do for example per-vertex lighting the vertex color would be calculated
// in the vertex shader and then written to this struct to be used in pixel shader to calculate final pixel color
struct VS_OUTPUT
{
    float4 Position   : POSITION;
    //float4 LightColor : COLOR0;
};

// output struct for pixel shader
// this is the final color that is written to the render target
struct PS_OUTPUT
{
    float4 Color   : COLOR0;
};

// The vertex shader. This is executed for each vertex.
// We receive input as model space coordinates, and using world and view-projection matrices
// they are projected into screen coordinates. 
VS_OUTPUT vsmain(in VS_INPUT In)
{
  VS_OUTPUT Out;

  // calculate world position by multiplying model space position by world matrix
	float4 wpos = mul(In.Position, MatWorld);
  // project world position to screen space by multiplying with view-projection matrix
	Out.Position = mul(wpos, MatViewProj);

  // Could do lighting calculation here and write results for pixel shader to use
  //Out.LightColor = float4(1, 0, 0, 1);

	return Out;
};

// The pixel shader. This is executed for each pixel of the rasterized triangles.
PS_OUTPUT psmain(in VS_OUTPUT In)
{
	PS_OUTPUT Out;
	Out.Color = matColor;

  //Multiply output color with light color from vertex shader
  //Out.Color = Out.Color * In.LightColor;

	return Out;
};

// Simple vertex shader for UI elements
// No transforms are necessary, vertices are pretransformed
VS_OUTPUT vsui(in VS_INPUT In)
{
	VS_OUTPUT Out;
	Out.Position = In.Position;
	Out.Position.w = 1.0f;
	return Out;
}