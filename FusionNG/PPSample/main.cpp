#include <windows.h>
#include "vbsngplugin.h"

#include <d3d9.h>
#include <d3dx9.h>

#include "../../Common/Essential/AppFrameWork.hpp"

#include "drawsample.h"

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// VBS2Fusion interface library forced linking
// Needed for plugins that don't use any of VBS2Fusion symbols
// This will insure that VBS2Fusion_2005 library will be linked to the plugin
//  and the plugin will require to have VBS2Fusion_2005.dll
// It is required by Simcentric that each Fusion plugin links with VBS2Fusion_2005.lib or VBS2_Fusion_2008.lib.
#include "VBS2FusionAppContext.h"
VBS2Fusion::VBS2FusionAppContext appContext;
#if _X64
#pragma comment(lib, "../_depends/Fusion/lib64/VBS2Fusion_2005.lib")
#else
#pragma comment(lib, "../_depends/Fusion/lib/VBS2Fusion_2005.lib")
#endif
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

drawsample *sample = NULL;


// Global notes for NG drawing:
// - Do not attempt to access the Direct3D device directly in functions where it is not allowed, like in OnNgDrawnPass.
// - Do not use D3DPOOL_MANAGED pool for any resource, it is not usable in Windows Vista/7 due to Direct3D 9Ex.
// - When using Windows Vista/7, device resets behave different to Windows XP and previous VBS2 versions. To test plugin
//   reset compatibility use "-winxp" commandline parameter with VBS2NG.
// - You no longer need to capture and restore the device state when returning from a plugin call (ie. with a state block)
//   it is done automatically by the engine for draw functions


// This function is called once per frame before any drawing is done by engine
// parameters contain real direct 3D device, so resource management can be done here
VBSPLUGIN_EXPORT void WINAPI OnNgPreDraw(paramPreDraw *param, DWORD paramSize)
{
  if(paramSize<sizeof(paramPreDraw))
    return; // Engine is older than our plugin structs

  // Here we have an opportunity to change the near clip plane by changing the value of param->clipNear_feedback
  // By default the engine will try to move the near clip plane as far as possible, but it cannot consider primitives
  // that are drawn by plugins. Therefore if plugins need to draw primitives very close to the camera, they need to
  // tell here how close they need the near clip plane to be set.

  // It is very important to not set the near clip plane too close, ie. you need to actually determine how close the nearest
  // primitive you are going to draw is and only set the clip to that distance. Simply setting a very low near clip plane
  // at all times will result in severe depth precision issues for the whole scene.
}

// This function is called once per frame, after depth priming is done but before normal rendering is done
// parameters contain real direct 3D device, so resource management can be done here
// At this point some extra information is available like eye accomodation value
VBSPLUGIN_EXPORT void WINAPI OnNgInitDraw(paramInitDraw *param, DWORD paramSize)
{  
  if(sample)
  {
    // drawsample has access to the Direct3D 9 device here
    sample->manageObjects(param->dev, param->deltaT, param->renderWidth, param->renderHeight);
  }
}

// This function is called when engine completes a render pass
// Render passes are done for different types of objects (see drawPassIdentifier) and different render modes (see drawModeIdentifier)
// so this function is called many times per framed, you can pick the required slot to do drawing here
// parameters contain CB interface which must be used to do only drawing here,
// do not attempt to access the D3D device directly and do not release any resources!
VBSPLUGIN_EXPORT void WINAPI OnNgDrawnPass(paramDrawnPass *param, DWORD paramSize)
{
  // This is where we want to do our 3D scene drawing. 
}

// This function is called when the skybox is drawn by the engine
// parameters contain CB interface which must be used to do only drawing here,
// do not attempt to access the D3D device directly and do not release any resources!
VBSPLUGIN_EXPORT void WINAPI OnNgDrawnSky(paramDrawnSky *param, DWORD paramSize)
{
}

// This function is called at end of frame rendering, after postprocessing is done and UI is rendered
// parameters contain CB interface which must be used to do only drawing here,
// do not attempt to access the D3D device directly and do not release any resources!
VBSPLUGIN_EXPORT void WINAPI OnNgFilledBackbuffer(paramFilledBackbuffer *param, DWORD paramSize)
{
  if (sample)
  {
    sample->reverseScreen(param->cbi, param->renderTarget);
  }
}

// This function is called when D3D device is created by engine
// parameters contain real direct 3D device, so resource management can be done here
VBSPLUGIN_EXPORT void WINAPI OnNgDeviceInit(paramDeviceInit *param, DWORD paramSize)
{  
  // Create the sample
  if(!sample) sample = new drawsample(param->dev);
}

// This function is called when D3D device is destroyed or reset by engine
// parameters contain real direct 3D device, so resource management can be done here
// All D3D resources created by the plugin must be released here, otherwise device resets will fail
VBSPLUGIN_EXPORT void WINAPI OnNgDeviceInvalidate(paramDeviceInvalidate *param, DWORD paramSize)
{
  // Delete the sample object, this will release everything it has created
  // Alternatively, we could check for param->isReset and only release Direct3D handles without destroying anything else
  delete sample;
  sample = NULL;
}

// This function is called when D3D device is re-created after reset by engine
// parameters contain real direct 3D device, so resource management can be done here
// Resources released in OnNgDeviceInvalidate should be re-created here
VBSPLUGIN_EXPORT void WINAPI OnNgDeviceRestore(paramDeviceRestore *param, DWORD paramSize)
{
  // Re-create the sample
  if(!sample) sample = new drawsample(param->dev);
}


// Previous plugin functionality, same as before:

void LogF(const char* format, ...)
{
	va_list     argptr;
	char        str[1024];
	va_start (argptr,format);
	vsnprintf (str,1024,format,argptr);
	va_end   (argptr);
  OutputDebugString(str);
}

ExecuteCommandType ExecuteCommand = NULL;

VBSPLUGIN_EXPORT void WINAPI OnUnload() {
  // Whole plugin is unloaded, release the sample
  delete sample;
  sample = NULL;
}

VBSPLUGIN_EXPORT void WINAPI OnSimulationStep(float deltaT) {}

VBSPLUGIN_EXPORT const char* WINAPI PluginFunction(const char *inp)
{
  return "[]";
}

BOOL WINAPI DllMain(HINSTANCE hDll, DWORD fdwReason, LPVOID lpvReserved)
{
   switch(fdwReason)
   {
      case DLL_PROCESS_ATTACH:
        break;
      case DLL_PROCESS_DETACH:
        break;
   }
   return TRUE;
}

VBSPLUGIN_EXPORT void WINAPI RegisterHWND(void *hwnd, void *hins) {}

VBSPLUGIN_EXPORT void WINAPI RegisterCommandFnc(void *executeCommandFnc)
{
  ExecuteCommand = (ExecuteCommandType)executeCommandFnc;
}

