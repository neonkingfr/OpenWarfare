/*
    native.c

    convert text files into a native format
    (with optional URL decoding)

    24.7.2002

    Copyright (C) 1997-2002 by Josef Pelikan, MFF UK Prague
        http://cgg.ms.mff.cuni.cz/~pepca/
*/

#include <stdio.h>
#include <stdlib.h>

#ifdef _WIN32
#  include <sys/types.h>
#  include <sys/stat.h>
#  include <sys/utime.h>
#  define ME 1
#elif defined _MAC
#  define ME 3
#else
#  include <sys/types.h>
#  include <sys/stat.h>
#  include <unistd.h>
#  include <utime.h>
#  define ME 2
#endif

int main ( int argc, char *argv[] )
{
    FILE *input, *output;
    int ch, ch2;
    int URL = 0;
    int verbose = 0;
    int keep = 1;                           // keep mtime?
    int lines = 0;
    int mode = 0;                           // 0 .. native, 1 .. DOS, 2 .. UNIX, 3 .. MAC
    int eoln;                               // actually written EOLN
    int inEoln;                             // EOLN read from input
    int changed = 0;                        // output file was changed (# of changes)
    const char *outFn;
#ifdef _WIN32
    struct _stat inst;
    struct _stat outst;
    struct _utimbuf utim;
#else
    struct stat inst;
    struct stat outst;
    struct utimbuf utim;
#endif

    while ( argc >= 2 && argv[1][0] == '-' ) {
            // "-URL", "-DOS", "-UNIX", "-MAC", "-v", "-m"
        if ( !strcmp(argv[1],"-URL") )
            URL = 1;
        else
        if ( !strcmp(argv[1],"-DOS") )
            mode = 1;
        else
        if ( !strcmp(argv[1],"-UNIX") )
            mode = 2;
        else
        if ( !strcmp(argv[1],"-MAC") )
            mode = 3;
        else
        if ( !strcmp(argv[1],"-v") )
            verbose = 1;
        else
        if ( !strcmp(argv[1],"-m") )
            keep = 0;
        else
            goto usage;
        argc--;
        argv++;
        }
    eoln = mode ? mode : ME;
    
    if ( argc < 2 ) {
      usage:
        printf("Usage: %s [-URL] [-DOS|-UNIX|-MAC] <input-file> [<output-file>]\n",argv[0]);
        return -1;
        }

    if ( !(input = fopen(argv[1],"rb")) ) {
        printf("Cannot open input file %s\n",argv[1]);
        return -1;
        }
    if ( argc > 2 ) {
        if ( !(output = fopen(argv[2],mode?"wb":"wt")) ) {
            printf("Cannot create output file %s\n",argv[2]);
            return -1;
            }
        outFn = argv[2];
        }
    else {
        if ( !(output = tmpfile()) ) {
            printf("Cannot create temporary file!\n");
            return -1;
            }
        outFn = argv[1];
        }

    while ( 1 ) {       // one input character
        ch = getc(input);
        if ( ch == EOF || ch == '\x1A' ) {
            changed += (ch != EOF);
            break;
            }
        if ( URL )
            if ( ch == '%' ) {
                char buf[3];
                buf[0] = getc(input);
                buf[1] = getc(input);
                buf[2] = (char)0;
                ch = strtoul(buf,0,16);
                changed++;
                }
            else
            if ( ch == '+' ) {
                ch = ' ';
                changed++;
                }
        if ( ch == '\x0D' || ch == '\x0A' ) {
                // output EOLN:
            switch ( mode ) {
                case 1:                     // explicit: DOS
                    putc('\x0D',output);
                case 2:                     // explicit: UNIX
                    putc('\x0A',output);
                    break;
                case 3:                     // explicit: MAC
                    putc('\x0D',output);
                    break;
                default:                    // native
                    fprintf(output,"\n");
                }
            if ( ch == '\x0D' ) {           // DOS?
                ch2 = getc(input);
                if ( ch2 == EOF || ch2 == '\x1A' ) {
                    changed += (eoln != 3);
                    break;
                    }
                if ( ch2 != '\x0A' ) ungetc(ch2,input);
                }
            if ( ch == '\x0A' ) inEoln = 2;
            else
                inEoln = (ch2 == '\x0A') ? 1 : 3;
            changed += (inEoln != eoln);
            lines++;
            }
        else
            putc(ch,output);
        }
    fclose(input);

    if ( keep && (changed || argc > 2) ) {  // I'll need file-times
#ifdef _WIN32
        _stat(argv[1],&inst);
#else
        stat(argv[1],&inst);
#endif
        }

    if ( argc <= 2 && changed ) {           // copy back the temporary file
        if ( !(input = fopen(argv[1],mode?"wb":"wt")) ) {
            printf("Cannot write to file %s\n",argv[1]);
            return -1;
            }
        rewind(output);
        while ( (ch = getc(output)) != EOF )
            putc(ch,input);
        fclose(input);
        }
    fclose(output);

    if ( keep && (changed || argc > 2) ) {  // return the original mtime
#ifdef _WIN32
        _stat(outFn,&outst);
        utim.actime  = outst.st_atime;
        utim.modtime = inst.st_mtime;
        _utime(outFn,&utim);
#else
        stat(outFn,&outst);
        utim.actime  = outst.st_atime;
        utim.modtime = inst.st_mtime;
        utime(outFn,&utim);
#endif
        }

    if ( verbose )
        printf("%d lines converted, %d modifications made, mtime %s changed\n",
               lines,changed,keep?"not":"was");

    return changed;
}
