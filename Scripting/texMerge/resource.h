//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by texMerge.rc
//
#define IDS_INVALID_CD_KEY              1
#define IDD_ABOUTBOX                    100
#define IDR_MAINFRAME                   128
#define IDR_TEXMERTYPE                  129
#define IDS_PROPDLGVALUE                129
#define IDS_PROPDLGNAME                 130
#define IDR_SOURCESAFE                  131
#define IDD_CHECKINCOMMENTS             133
#define IDD_SETNAMEBAR                  134
#define IDD_ITEMPROPR                   137
#define IDD_AAMULTI                     138
#define IDD_MERGETOONE                  140
#define IDC_COMMENTS                    1000
#define IDC_KEEPCHIN                    1001
#define IDC_PROGRESS1                   1003
#define IDC_PROPLIST                    1004
#define IDC_DELETE                      1005
#define IDC_ADD                         1006
#define IDC_EDITNAME                    1007
#define IDC_EDITVALUE                   1008
#define IDC_PICKAFILE                   1009
#define IDC_OPTIMIZE                    1009
#define IDC_PICKAFOLDER                 1010
#define IDC_NUMSETS                     1010
#define IDC_LIST                        1011
#define IDC_CHECK1                      1012
#define IDC_DONTMOVESEL                 1012
#define ID_SET_ADD                      32771
#define ID_SET_DELETE                   32772
#define ID_SET_DUPLICATE                32773
#define ID_ITEM_ADD                     32774
#define ID_ITEM_DELETE                  32775
#define ID_SET_NEXT                     32776
#define ID_SET_PREV                     32777
#define ID_ITEM_PREV                    32778
#define ID_ITEM_NEXT                    32779
#define ID_VIEW_ZOOM_IN                 32780
#define ID_VIEW_ZOOM_OUT                32781
#define ID_SET_GROW                     32783
#define ID_SET_SHRINK                   32784
#define ID_ITEM_GROW                    32785
#define ID_ITEM_SHRINK                  32786
#define ID_SET_GROW_ALL                 32788
#define ID_SET_SHRINK_ALL               32789
#define ID_SET_GROW_X                   32791
#define ID_SET_SHRINK_X                 32792
#define ID_SET_GROW_Y                   32793
#define ID_SET_SHRINK_Y                 32794
#define ID_ITEM_ROT_LEFT                32795
#define ID_ITEM_ROT_RIGHT               32796
#define ID_SET_ARRANGE                  32799
#define ID_SET_MODEL                    32800
#define ID_SET_COPY                     32801
#define ID_SET_MERGE                    32802
#define ID_FILE_EXPORT                  32806
#define ID_FILE_PREF                    32807
#define ID_FILE_PROP                    32808
#define ID_SET_APPLYDAMMAGE             32809
#define ID_FILE_MERGED                  32810
#define ID_SET_ARRANGE_USE              32811
#define ID_FILE_TGAOUTPUT               32812
#define ID_Menu                         32820
#define ID_FILE_OPENFROMSOURCESAFE      32821
#define ID_FILE_SAVEANDCHECKIN          32822
#define ID_FILE_SOURCESAFEOTHERCOMMANDS 32823
#define ID_Menu32824                    32824
#define ID_SOURCESAFEOTHERCOMMANDS_CHECKOUT 32825
#define ID_SOURCESAFEOTHERCOMMANDS_HISTORY 32827
#define ID_SOURCESAFEOTHERCOMMANDS_UNDOCHECKOUT 32828
#define ID_SOURCESAFE_DISCONNECT        32829
#define ID_SOURCESAFE_GETLATESTVERSION  32830
#define ID_VIEW_SOURCECONTROLTOOLBAR    32831
#define ID_VIEW_SERVERCONSOLE           32832
#define ID_VIEW_SETNAMEBAR              32833
#define ID_BUTTON32837                  32837
#define IDC_NAME                        32838
#define ID_ITEM_PROPERTIES              32859
#define ID_ITEM_KEEPSIZE                32862
#define ID_SET_PROPERTIES               32863
#define ID_SET_ARRANGE_MULTI            32868
#define ID_SET_MERGETOONE               32869
#define ID_INDICATOR_PAGE               59142
#define IDS_MSG_NOREGISTRY              61206
#define IDS_MSG_NOCONFIG                61207
#define IDS_MSG_BADCONFIG               61208
#define IDS_MSG_CONFIGERR               61209
#define IDS_MSG_CONFIGOLDAPP            61210

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_3D_CONTROLS                     1
#define _APS_NEXT_RESOURCE_VALUE        141
#define _APS_NEXT_COMMAND_VALUE         32870
#define _APS_NEXT_CONTROL_VALUE         1013
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
