#include "d3dXB/shaders/FPShadersDecl.h"

xvs.1.1

// Tell the compiler to link the module as a fragment (without validation on registers to be initialized and without optimalizations)
//#pragma fragment

// Tell the compiler to not to add the screenspace conversion instructions
#pragma screenspace

// Get the index to the first row of the transformed matrix
mad r0.x, INPUT_BLENDINDICES.x, c[CONSTANTS_0_1_765_0d5].z, c[CONSTANTS_0_1_765_0d5].w
mov a0.x, r0.x

// Skinned position and normal
mul r0, INPUT_POSITION, c[POS_MUL]
add SKINNED_POSITION, r0, c[POS_ADD]
mov SKINNED_NORMAL, INPUT_NORMAL

// Transform position (only world matrix is in bones - output is in world space)
dp4 r0.x, SKINNED_POSITION, c[BONES_START + a0.x + 0]
dp4 r0.y, SKINNED_POSITION, c[BONES_START + a0.x + 1]
dp4 r0.z, SKINNED_POSITION, c[BONES_START + a0.x + 2]
mad SKINNED_POSITION, r0, c[CONSTANTS_0_1_765_0d5].yyyx, c[CONSTANTS_0_1_765_0d5].xxxy

// Transform normal (only world matrix is in bones - output is in world space)
dp3 r0.x, SKINNED_NORMAL, c[BONES_START + a0.x + 0]
dp3 r0.y, SKINNED_NORMAL, c[BONES_START + a0.x + 1]
dp3 r0.z, SKINNED_NORMAL, c[BONES_START + a0.x + 2]
mad SKINNED_NORMAL, r0, c[CONSTANTS_0_1_765_0d5].yyyx, c[CONSTANTS_0_1_765_0d5].xxxy

// Shadow volume protraction
dp3 r0, SKINNED_NORMAL, c[SHADOWDIRECTION_TRANSFORMED_DIR]
sge r0.x, r0.x, c[CONSTANTS_0_1_765_0d5].x
//mov r0.x, c[CONSTANTS_0_1_765_0d5].y

// SKINNED_POSITION = SHADOWDIRECTION_TRANSFORMED_DIR * (r0.x * 20 + 0.05) + SKINNED_POSITION
//mad r0.x, r0.x, c[FOGEND__RCP_FOGEND_MINUS_FOGSTART__SHADOWVOLUME_OFFSET__SHADOWVOLUME_SIZE].w, c[FOGEND__RCP_FOGEND_MINUS_FOGSTART__SHADOWVOLUME_OFFSET__SHADOWVOLUME_SIZE].z
mad r0.x, r0.x, c[SHADOW_OFFSET__SHADOW_SIZE].y, c[SHADOW_OFFSET__SHADOW_SIZE].x
mad SKINNED_POSITION.xyz, c[SHADOWDIRECTION_TRANSFORMED_DIR].xyz, r0.x, SKINNED_POSITION.xyz
mov SKINNED_POSITION.w, c[CONSTANTS_0_1_765_0d5].y

// Apply view matrix
m4x3 TRANSFORMED_POSITION.xyz, SKINNED_POSITION, c[WORLD_VIEW_MATRIX_0]
mov TRANSFORMED_POSITION.w, c[CONSTANTS_0_1_765_0d5].y

// Project position
m4x4 r0, TRANSFORMED_POSITION, c[PROJ_MATRIX_0]

// Convert to screenspace
mul r0.xyz, r0, c[SCREENSPACE_SCALE]
+ rcc r1.x, r0.w
mad r0.xyz, r0, r1.x, c[SCREENSPACE_OFFSET]
// apply 4X AA factors and set .w
mad oPos, r0, c[CONSTANTS_0_1_765_0d5].wwyy, -c[CONSTANTS_0_1_765_0d5].wwxx
