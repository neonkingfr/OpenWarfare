#include "wpch.hpp"
#include "gameDirs.hpp"
#include <El/FileServer/fileServer.hpp>
#include <Es/Files/filenames.hpp>

/*!
\patch 5095 Date 12/1/2006 by Jirka
- Fixed: User profile ending with the period character was handled incorrectly
*/

RString EncodeFileName(RString src)
{
  const int len = FILENAME_MAX;
  char buffer[len];
  int dst = 0;
  for (const char *ptr=src; *ptr; ptr++)
  {
    if (dst >= len - 1)
    {
      Assert(dst == len - 1);
      buffer[dst] = 0;
      return buffer;
    }
    if
      (
      *ptr >= '0' && *ptr <= '9' ||
      *ptr >= 'A' && *ptr <= 'Z' ||
      *ptr >= 'a' && *ptr <= 'z'
      )
    {
      buffer[dst++] = *ptr;
    }
    else switch (*ptr)
    {
    case '^':
    case '&':
    case '\'':
    case '@':
    case '{':
    case '}':
    case '[':
    case ']':
    case ',':
    case '$':
    case '=':
    case '!':
    case '-':
    case '#':
    case '(':
    case ')':
#ifdef _XBOX
    // FIX: '.' is ignored when on the end of directory / file
    case '.':
#endif
    case '+':
    case '~':
    case '_':
      buffer[dst++] = *ptr;
      break;
    case '%':
      if (dst >= len - 2)
      {
        buffer[dst] = 0;
        return buffer;
      }
      buffer[dst++] = '%';
      buffer[dst++] = '%';
      break;
    default:
      if (dst >= len - 3)
      {
        buffer[dst] = 0;
        return buffer;
      }
      buffer[dst++] = '%';
      int hi = (int(*ptr) & 0xf0) >> 4;
      int lo = int(*ptr) & 0x0f;
      buffer[dst++] = hi > 9 ? 'a' + hi - 10 : '0' + hi;
      buffer[dst++] = lo > 9 ? 'a' + lo - 10 : '0' + lo;
      break;
    }
  }
  buffer[dst] = 0;
  return buffer;
}

RString DecodeFileName(RString src)
{
  char buffer[FILENAME_MAX];
  char *dst = buffer;
  for (const char *ptr=src; *ptr; ptr++)
  {
    if (*ptr == '%')
    {
      ptr++;
      if (*ptr == '%') *(dst++) = '%';
      else
      {
        char cHi = *(ptr++);
        char cLo = *ptr;
        int hi = cHi > '9' ? cHi - 'a' + 10 : cHi - '0';
        int lo = cLo > '9' ? cLo - 'a' + 10 : cLo - '0';
        *(dst++) = (hi << 4) | lo;
      }
    }
    else *(dst++) = *ptr;
  }
  *dst = 0;
  return buffer;
}

#ifdef _WIN32
/// command line may be used to override user profile storage area 
/* does not terminate with backslash */
static RString ExplicitRootDir;

void SetExplicitRootDir(RString path)
{
  ExplicitRootDir = path;
  if (path.GetLength()>=0 && path[path.GetLength()-1]=='\\')
  {
    ExplicitRootDir = RString(path,path.GetLength()-1);
  }
  else
  {
    ExplicitRootDir = path;
  }
  CreatePath(path);
  CreateDirectory(path,NULL);
}
#endif

#ifdef _XBOX

RString GetUserRootDir()
{
  return RString();
}

bool GetLocalSettingsDir(char *dir)
{
  // For Xbox 360 Remote File Sharing directory can be used for logs
  // - pass command line argument: -profiles=net:\\smb\\<server>\\<shared folder>
  if (ExplicitRootDir.GetLength()>0)
  {
    strcpy(dir, ExplicitRootDir);
    TerminateBy(dir, '\\');
    return true;
  }

  #if _XBOX_VER >= 200
  strcpy(dir,"cache:\\");
  #else
  strcpy(dir,"Z:\\");
  #endif
  return false;
}

RString GetDefaultUserRootDir()
{
  return RString();
}

#elif defined _WIN32

#include <Es/Common/win.h>

RString GetLoginName()
{
  char buf[256];
  DWORD bufSize = sizeof(buf);
  if (::GetUserName(buf, &bufSize) && bufSize > 0) return buf;
  return "Player";
}

#include <shlobj.h>
#include <io.h>
#include <El/QStream/qStream.hpp>

/*!
\patch 5133 Date 2/26/2007 by Jirka
- Fixed: when alternate directory for profiles given by command line options -profiles,
user profiles are no longer stored to this directory, but to its Users subdirectory
*/

RStringB GetProfilePathDefault();
RStringB GetProfilePathCommon();

static RString GetRootDir()
{
  Assert(ExplicitRootDir.GetLength()==0);
  char path[MAX_PATH];
  if (FAILED(SHGetFolderPath(NULL, CSIDL_PERSONAL | CSIDL_FLAG_CREATE, NULL, 0, path)))
  {
    Fail("Cannot access My Documents");
    return RString();
  }
  TerminateBy(path, '\\');
  return path;
}

bool GetLocalSettingsDir(char *dir)
{
  if (ExplicitRootDir.GetLength()>0)
  {
    strcpy(dir,ExplicitRootDir);
    TerminateBy(dir, '\\');
    return true;
  }
  Assert(ExplicitRootDir.GetLength()==0);
  if (FAILED(SHGetFolderPath(NULL, CSIDL_LOCAL_APPDATA | CSIDL_FLAG_CREATE, NULL, 0, dir)))
  {
    Fail("Cannot access Local settings Application data");
    return false;
  }
  TerminateBy(dir, '\\');
  strcat(dir, AppName);
  return true;
}

bool GetLocalSettingsDir(wchar_t *dir)
{
  if (ExplicitRootDir.GetLength()>0)
  {
    MultiByteToWideChar(CP_ACP, 0, ExplicitRootDir, -1, dir, MAX_PATH);
    TerminateBy(dir, '\\');
    return true;
  }
  Assert(ExplicitRootDir.GetLength()==0);
  if (FAILED(SHGetFolderPathW(NULL, CSIDL_LOCAL_APPDATA | CSIDL_FLAG_CREATE, NULL, 0, dir)))
  {
    Fail("Cannot access Local settings Application data");
    return false;
  }
  TerminateBy(dir, '\\');

  
  wchar_t appNameW[256];
  mbstowcs(appNameW, AppName, sizeof(appNameW));

  wcscat(dir, appNameW);
  return true;
}

bool ForEachUser(ForEachUserCallback func, void *context)
{
  RString dir;
  
  if (ExplicitRootDir.GetLength()>0)
  {
    dir = ExplicitRootDir + RString("\\Users");
  }
  else
  {
    RString root = GetRootDir();
    if (root.GetLength() == 0) return false;
    
    // check primary location
    {
      RString dir = GetProfilePathDefault();
      if (QIFileFunctions::DirectoryExists(root + dir))
      {
        if (func(GetLoginName(), context)) return true;
      }
    }

    dir = root + GetProfilePathCommon();
  }

  _finddata_t info;
  long h = _findfirst(dir + RString("\\*.*"), &info);
  if (h != -1)
  {
    do
    {
      if
      (
        (info.attrib & _A_SUBDIR) != 0 &&
        info.name[0] != '.'
      )
      {
        if (func(DecodeFileName(info.name), context)) return true;
      }
    }
    while (_findnext(h, &info) == 0);
    _findclose(h);
  }
  return false;
}

RString GetUserRootDir(RString name)
{
  if (ExplicitRootDir.GetLength()>0)
  {
    return ExplicitRootDir + RString("\\Users\\") + EncodeFileName(name);
  }
  RString root = GetRootDir();
  if (root.GetLength() == 0) return root;

  if (stricmp(name, GetLoginName()) == 0)
  {
    RString dir = GetProfilePathDefault();
    return root + dir;
  }
  else
  {
    RString dir = GetProfilePathCommon();
    return root + dir + "\\" + EncodeFileName(name);
  }
}

RString GetDefaultUserRootDir()
{
  if (ExplicitRootDir.GetLength()>0)
  {
    return ExplicitRootDir + RString("\\Users\\") + EncodeFileName(GetLoginName());
  }
  RString root = GetRootDir();
  if (root.GetLength() == 0) return root;

  RString dir = GetProfilePathDefault();
  return root + dir;
}

#else

RString GetUserRootDir()
{
  return RString(".");
}

bool GetLocalSettingsDir(char *dir)
{
  strcpy(dir,".");
  return false;
}

RString GetDefaultUserRootDir()
{
  return RString(".");
}

void SetExplicitRootDir(RString path)
{
}

RString GetUserRootDir(RString name)
{
  return name;
}

#endif

