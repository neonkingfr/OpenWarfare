#ifdef _MSC_VER
#pragma once
#endif

#ifndef _LASER_TARGET_HPP
#define _LASER_TARGET_HPP

#include "vehicleAI.hpp"

//! virtual laser designated target type

class LaserTargetType: public EntityAIType
{
	friend class LaserTarget;

	typedef EntityAIType base;

	public:
	LaserTargetType( ParamEntryPar param );
	virtual void Load(ParamEntryPar par, const char *shape);
	virtual void InitShape(); // after shape is loaded
	bool AbstractOnly() const {return false;}

  virtual Object* CreateObject(bool unused) const;
};

//! virtual laser designated target

class LaserTarget: public EntityAI
{
	typedef EntityAI base;
protected:
	Time _lastActivation; // time of last activation
	OLink(Object) _designatedTarget;
private:
  float _ownerFOV;

#if _VBS3
  OLink(EntityAI) _owner;
#endif

	void CheckDesignatedTarget(Matrix4Par pos);

	public:
	LaserTarget(const EntityAIType *name, bool fullCreate=true);

	virtual bool IgnoreObstacle(Object *obstacle, ObjIntersect type) const;

  void SetOwnerFov(float fov) {_ownerFOV = fov;}

	virtual void Init(Matrix4Par pos, bool init);
	virtual void Move(Matrix4Par transform);
	virtual void Move(Vector3Par position);

	void Simulate( float deltaT, SimulationImportance prec );

	void Draw(
	  int cb, int level, const SectionMaterialLODs &matLOD, ClipFlags clipFlags, const DrawParameters &dp,
    const InstanceParameters &ip, float dist2, const PositionRender &pos, SortObject *oi
	);
	void DrawDiags();

  //! This object should always use alpha pass (2)
  virtual int PassNum(int lod) {return 2;}
  //! Disable instancing
  virtual bool CanBeInstanced(int level, float distance2, const DrawParameters &dp) const {return false;}
  virtual bool CanObjDrawAsTask(int level) const;
  virtual bool CanObjDrawShadowAsTask(int level) const;
  virtual float EstimateArea() const;
  virtual float GetFeatureSize() const;
  virtual int GetObjSpecial(int shapeSpecial) const {return shapeSpecial|NoShadow;}


#if _VBS3
  // return the destination target the laser pointer is on.
  Object *GetDesinatedTarget(){ return _designatedTarget; }

  void SetOwner(EntityAI *owner){ _owner = owner; }
  EntityAI *GetOwner(){ return _owner; }
#endif

	float VisibleSize(ObjectVisualState const& vs) const;
	float VisibleSizeRequired(ObjectVisualState const& vs) const;
	Vector3 AimingPosition(ObjectVisualState const& vs, const AmmoType *ammo=NULL) const;
	virtual LSError Serialize(ParamArchive &ar);

	DECLARE_NETWORK_OBJECT
	static NetworkMessageFormat &CreateFormat
	(
		NetworkMessageClass cls,
		NetworkMessageFormat &format
	);
	TMError TransferMsg(NetworkMessageContext &ctx);
	float CalculateError(NetworkMessageContextWithError &ctx);

	virtual bool QIsManual() const {return false;}

	USE_CASTING(base)
};

/*-------------------------------------------------------*/

class NVMarkerTargetType: public EntityAIType
{
  friend class NVMarkerTarget;

  typedef EntityAIType base;

public:
  Color _diffuse;
  Color _ambient;
  float _brightness;
  bool _flashing;
  bool _nvVisible;

public:
  NVMarkerTargetType( ParamEntryPar param );
  virtual void Load(ParamEntryPar par, const char *shape);
  virtual void InitShape(); // after shape is loaded
  bool AbstractOnly() const {return false;}

  virtual Object* CreateObject(bool unused) const;
};

class NVMarkerTarget: public EntityAI
{
  typedef EntityAI base;

protected:
  Ref<LightPointOnVehicle> _light;
  Time _lightOn;
  Time _lastActivation; // time of last activation
  bool _on;

public:
  NVMarkerTarget(const EntityAIType *name, bool fullCreate=true);

  virtual void Move(Matrix4Par transform);
  virtual void Move(Vector3Par position);

  void Simulate( float deltaT, SimulationImportance prec );

  void Draw(
    int cb, int level, const SectionMaterialLODs &matLOD, ClipFlags clipFlags, const DrawParameters &dp,
    const InstanceParameters &ip, float dist2, const PositionRender &pos, SortObject *oi
    );

  void DrawDiags();

  //! This object should always use alpha pass (2)
  virtual int PassNum(int lod) {return 2;}
  //! Disable instancing
  virtual bool CanBeInstanced(int level, float distance2, const DrawParameters &dp) const {return false;}
  virtual bool CanObjDrawAsTask(int level) const;
  virtual bool CanObjDrawShadowAsTask(int level) const;
  virtual float EstimateArea() const;
  virtual float GetFeatureSize() const;
  virtual int GetObjSpecial(int shapeSpecial) const { return shapeSpecial|NoShadow; }

  // make sure we can detect it on very long distance
  float VisibleSize(ObjectVisualState const& vs) const;
  float VisibleSizeRequired(ObjectVisualState const& vs) const;

  Vector3 AimingPosition(ObjectVisualState const& vs, const AmmoType *ammo=NULL) const;
  virtual LSError Serialize(ParamArchive &ar);

  DECLARE_NETWORK_OBJECT
  static NetworkMessageFormat &CreateFormat
    (
    NetworkMessageClass cls,
    NetworkMessageFormat &format
    );
  TMError TransferMsg(NetworkMessageContext &ctx);
  float CalculateError(NetworkMessageContextWithError &ctx);

  virtual bool QIsManual() const {return false;}

  USE_CASTING(base)
};

/*-------------------------------------------------------*/

class ArtilleryMarkerTargetType: public EntityAIType
{
  friend class ArtilleryMarkerTarget;

  typedef EntityAIType base;

public:
  ArtilleryMarkerTargetType( ParamEntryPar param );
  virtual void Load(ParamEntryPar par, const char *shape);
  virtual void InitShape(); // after shape is loaded
  bool AbstractOnly() const {return false;}

  virtual Object* CreateObject(bool unused) const;
};

class ArtilleryMarkerTarget: public EntityAI
{
  typedef EntityAI base;

protected:
  Time _lastActivation; // time of last activation
  Time _createTime;

public:
  ArtilleryMarkerTarget(const EntityAIType *name, bool fullCreate=true);

  virtual void Init(Matrix4Par pos, bool init);
  virtual void Move(Matrix4Par transform);
  virtual void Move(Vector3Par position);

  void Simulate( float deltaT, SimulationImportance prec );

  void Draw(
    int cb, int level, const SectionMaterialLODs &matLOD, ClipFlags clipFlags, const DrawParameters &dp,
    const InstanceParameters &ip, float dist2, const PositionRender &pos, SortObject *oi
    );

  void DrawDiags();

  //! This object should always use alpha pass (2)
  virtual int PassNum(int lod) {return 2;}
  //! Disable instancing
  virtual bool CanBeInstanced(int level, float distance2, const DrawParameters &dp) const {return false;}
  virtual bool CanObjDrawAsTask(int level) const;
  virtual bool CanObjDrawShadowAsTask(int level) const;
  virtual float EstimateArea() const;
  virtual float GetFeatureSize() const;
  virtual int GetObjSpecial(int shapeSpecial) const { return shapeSpecial|NoShadow; }

  // make sure we can detect it on very long distance
  float VisibleSize(ObjectVisualState const& vs) const;
  float VisibleSizeRequired(ObjectVisualState const& vs) const;

  virtual Vector3 AimingPosition(ObjectVisualState const& vs, const AmmoType *ammo=NULL) const;

  virtual bool QIsManual() const {return false;}

  USE_CASTING(base)
};

#endif

