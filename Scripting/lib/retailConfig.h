// Configuration parameters for Armed Assault super release

#define _VERIFY_KEY								1
#define _VERIFY_CLIENT_KEY  			1  // server is checking the CD key of clients on GameSpy
#define _VERIFY_BLACKLIST					1
#define _ENABLE_EDITOR						1
#define _ENABLE_WIZARD						1
#define _ENABLE_ALL_MISSIONS			0
#define _ENABLE_MP								1
#define _ENABLE_AI								1
#define _ENABLE_CAMPAIGN					1
#define _ENABLE_SINGLE_MISSION		1
#define _ENABLE_UNSIGNED_MISSIONS	1
#define _ENABLE_BRIEF_GRP					1
#define _ENABLE_PARAMS						1
#define _ENABLE_CHEATS						0
#define _ENABLE_ADDONS						1
#define _ENABLE_DEDICATED_SERVER	1
#define _FORCE_DEMO_ISLAND				0
#define _FORCE_SINGLE_VOICE				0
#define _ENABLE_AIRPLANES					1
#define _ENABLE_HELICOPTERS				1
#define _ENABLE_SHIPS							1
#define _ENABLE_CARS							1
#define _ENABLE_TANKS							1
#define _ENABLE_MOTORCYCLES				1
#define _ENABLE_PARACHUTES				1
#define _ENABLE_DATADISC					1
#define _ENABLE_VBS								0
#define _DISABLE_CRC_PROTECTION   0
#define _FORCE_DS_CONTEXT         0
#define _ENABLE_GAMESPY           1
#define _ENABLE_NEWHEAD           1
#define _ENABLE_SKINNEDINSTANCING 0
#define _ENABLE_BB_TREES          0
#define _ENABLE_CONVERSATION      1
#define _ENABLE_SPEECH_RECOGNITION  0
#define _ENABLE_EDITOR2_MP        0
#define _ENABLE_EDITOR2           1
#define _ENABLE_IDENTITIES        1
#define _ENABLE_INVENTORY         0
#define _ENABLE_MISSION_CONFIG    1
#define _ENABLE_INDEPENDENT_AGENTS  1
#define _ENABLE_DIRECT_MESSAGES   0
#define _ENABLE_FILE_FUNCTIONS    0
#define _ENABLE_HAND_IK           1
#define _ENABLE_WALK_ON_GEOMETRY  0
#define _ENABLE_BULDOZER          1
#define _ENABLE_COMPILED_SHADER_CACHE 1
#define _ENABLE_DISTRIBUTIONS     1
#define _USE_FCPHMANAGER          1
#define _ENABLE_DX10              0
#define _USE_BATTL_EYE_SERVER     1
#define _USE_BATTL_EYE_CLIENT     1
#define _ENABLE_STEAM             0
#define _ENABLE_SEND_UDP_MESSAGE  0
#define _ENABLE_TEXTURE_HEADERS   1 //if reading texture headers from texture headers file is enabled

#define _TIME_ACC_MIN							1.0
#define _TIME_ACC_MAX							4.0
																
#define _MACRO_CDP							  0

#define _ENABLE_AUTOTEST          1

// default mod path 
#define _MOD_PATH_DEFAULT "CA;Expansion"

// CD Key parameters
// new Product Key algorithm
#define NEW_KEYS 1

// number of bits for a distribution id
#define DISTR_BITS 6
// number of bits for a simple hash
#define SIMPLE_HASH_BITS 4
// number of bits for a player id
#define PLAYER_BITS (32 - DISTR_BITS - SIMPLE_HASH_BITS)

const bool CDKeyRegistryUser = false;
const char CDKeyRegistryPath[] = ConfigApp;

// ArmA 2 distributions
enum DistributionId
{
  DistArmA2OAUS,
  DistArmA2OAEnglish,
  DistArmA2OAGerman,
  DistArmA2OAPolish,
  DistArmA2OARussian,
  DistArmA2OACzech,
  DistArmA2OAEuro,
  DistArmA2OAHungarian,
  DistArmA2OABrasilian,
  DistArmA2OAInternal,
  NDistributions
};

// ArmA 2 distributions
enum ProductId
{
  ProductA2,
  ProductOA,
  ProductBAF,
  ProductPMC,
  ProductFree,
  ProductRFT,
  ProductTKOH,
  ProductOADECADE,
  NProducts
};

// maybe the product can have multiple productIDs, use macro factory
#if REINFORCEMENTS
#define PRODUCT_ID(XX) \
  XX(ProductRFT)
#else 
#define PRODUCT_ID(XX) \
  XX(ProductOA) \
  XX(ProductOADECADE)  
#endif

// list of blacklisted cd keys in the form: XX(id1) XX(id2) ...
#define CDKEY_BLACKLIST(XX) \
  XX(883458) \
  XX(537514689) \
  XX(805583236) \
  XX(1879660678) \


// CD/DVD protection
#ifndef _SECUROM
#define _SECUROM 1
#endif
