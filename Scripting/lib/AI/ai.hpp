#ifdef _MSC_VER
#pragma once
#endif

#ifndef _AI_HPP
#define _AI_HPP

#include "../types.hpp"
#include "aiTypes.hpp"
#include "../transport.hpp"
#include <El/ParamArchive/paramArchive.hpp>
#include "../speaker.hpp"

#include <El/Time/time.hpp>
#include <El/ParamArchive/serializeClass.hpp>

#include "../camEffects.hpp"
#include "../titEffects.hpp"

#include "../arcadeWaypoint.hpp"

#include "../fsm.hpp"

#include "../scripts.hpp"

#include "../Network/networkObject.hpp"

#include <time.h>

#include "operMap.hpp"

#include <Es/Threads/multisync.hpp>

#if _ENABLE_CONVERSATION
#include <RV/KBBase/kbCenter.hpp>
#endif

#if _ENABLE_INDEPENDENT_AGENTS || _ENABLE_IDENTITIES
#include "aiTeams.hpp"
#endif

//#define COEF_EXPOSURE                 4e-6F   // coefficient for including exposure into cost
#define COEF_EXPOSURE                   1e-6F   // coefficient for including exposure into cost
#define COEF_EXPOSURE_UNABLE_TO_DEFEND  16e-6F  // coefficient for including exposure into cost

#define DIAG_WANTED_POSITION  0

///////////////////////////////////////////////////////////////////////////////
// class AI - base class for all classes in hierarchy

struct ExperienceDestroyInfo
{
  float maxCost;
  float exp;
};
TypeIsSimple(ExperienceDestroyInfo)

extern AutoArray<float> ExperienceTable;
extern AutoArray<ExperienceDestroyInfo> ExperienceDestroyTable;

extern float ExperienceDestroyEnemy;
extern float ExperienceDestroyFriendly;
extern float ExperienceDestroyCivilian;
extern float ExperienceDestroyStatic;

extern float ExperienceRenegadeLimit;

extern float ExperienceKilled;
  
// for subordinate soldier only
extern float ExperienceCommandCompleted;
extern float ExperienceCommandFailed;
extern float ExperienceFollowMe;

// for leadership only
extern float ExperienceDestroyYourUnit; 
extern float ExperienceMissionCompleted;
extern float ExperienceMissionFailed;

struct SynchronizedGroup
{
  OLinkPermNO(AIGroup) group;
  bool active;

  LSError Serialize(ParamArchive &ar);
};
TypeIsGeneric(SynchronizedGroup);

struct SynchronizedSensor
{
  OLinkPerm<Vehicle> sensor;
  bool active;

  LSError Serialize(ParamArchive &ar);
};
TypeContainsOLink(SynchronizedSensor);

struct SynchronizedItem
{
  AutoArray<SynchronizedGroup> groups;
  AutoArray<SynchronizedSensor> sensors;

#if _VBS3 && _VBS_WP_SYNCH // waypointSynchronizedWith
  int wpIndex;
  void Add(AIGroup *grp, int idx);
#else
  void Add(AIGroup *grp);
#endif
  
  void Add(Vehicle *sensor);

  void SetActive(AIGroup *grp, bool active = true);
  void SetActive(Vehicle *sensor, bool active = true);

  bool IsActive(AIGroup *grp = NULL);

  LSError Serialize(ParamArchive &ar);
};
TypeIsMovable(SynchronizedItem);

void AIGlobalInit();
void AIGlobalCleanUp();

extern OLinkPermArray<EntityAI> vehiclesMap;
extern OLinkPermArray<Vehicle> sensorsMap;
extern AutoArray<ArcadeMarkerInfo> markersMap;
extern AutoArray<SynchronizedItem> synchronized;

///////////////////////////////////////////////////////////////////////////////
// class AbstractAIMachine

template <class Task, class ContextType>
class AbstractAIMachine
{
protected:
  struct Item
  {
    SRef<FSM> _fsm;
    Ref<Task> _task;
    LSError Serialize(ParamArchive &ar)
    {
      CHECK(ar.Serialize("Task", _task, 1))
      if (ar.IsLoading() && ar.GetPass() == ParamArchive::PassFirst)
        _fsm = AbstractAIMachine<Task, ContextType>::CreateFSM(_task);
      // hotfix: avoid crash for unknown tasks
      if (_fsm)
      {
        CHECK(ar.Serialize("FSM", *_fsm, 1))
      }
      return LSOK;
    }
    ClassIsGeneric(Item);
  };
  AutoArray<Item> _stack;
  SRef<FSM> _noTaskFSM;
public:
  AbstractAIMachine(ContextType *context = NULL)
  {
    _noTaskFSM = CreateFSM(NULL);
    if (context)
    {
      context->_task = NULL;
      context->_fsm = _noTaskFSM;
    }
    _noTaskFSM->Enter(context);
    _noTaskFSM->SetState(0, context);
  }
  ~AbstractAIMachine() {}

  LSError Serialize(ParamArchive &ar);
  
  static FSM *CreateFSM(const Task *task);
  void PushTask(Task &task,ContextType *context);                 // add task to top of stack and make it current
  void EnqueueTask(Task &task,ContextType *context);              // add task to bottom of stack
  void PopTask(ContextType *context, bool doRefresh = true);      // remove task from top of stack
  void Delete(int i, ContextType *context, bool doRefresh = true);// remove task from top of stack
  void Clear(ContextType *context = NULL);                        // remove all tasks from stack
  bool Update(ContextType *context);
  void UpdateAndRefresh(ContextType *context);
  Item *GetCurrent()
  {int n = _stack.Size(); return n == 0 ? NULL : &_stack[n - 1];}
  const Item *GetCurrent() const
  {int n = _stack.Size(); return n == 0 ? NULL : &_stack[n - 1];}

  virtual void OnTaskCreated(Task &task, bool enqueued) {};
  virtual void OnTaskDeleted(Task &task) {};
protected:
  void Enter(ContextType *context);
  void Exit(ContextType *context);
};

template <class Task, class ContextType>
LSError AbstractAIMachine<Task, ContextType>::Serialize(ParamArchive &ar)
{
  if (ar.IsLoading() && ar.GetPass() == ParamArchive::PassFirst)
    _noTaskFSM = CreateFSM(NULL);
  CHECK(ar.Serialize("NoTaskFSM", *_noTaskFSM, 1))
  CHECK(ar.Serialize("Stack", _stack, 1))
  return LSOK;
}

template <class Task, class ContextType>
void AbstractAIMachine<Task, ContextType>::PushTask(Task &task, ContextType *context)
{
  Assert(task.GetType() > 0); // Task 0 is used when no task in stack
  if (task.GetType() <= 0) return;
  SRef<FSM> fsm = CreateFSM(&task);
  if (!fsm) return; // FIX: sometimes the fsm is not created (scripted command etc.)

  Exit(context);
  int index = _stack.Add();
  Assert(GetCurrent());
  _stack[index]._task = new Task(task);
  _stack[index]._fsm = fsm;
  OnTaskCreated(*GetCurrent()->_task, false);
  context->_task = GetCurrent()->_task;
  context->_fsm = GetCurrent()->_fsm;
  GetCurrent()->_fsm->Enter(context);
  GetCurrent()->_fsm->SetState(0, context); // State 0 is starting state
}

template <class Task, class ContextType>
void AbstractAIMachine<Task, ContextType>::EnqueueTask(Task &task, ContextType *context)
{
  Assert(task.GetType() > 0); // Task 0 is used when no task in stack
  if (task.GetType() <= 0) return;
  SRef<FSM> fsm = CreateFSM(&task);
  if (!fsm) return; // FIX: sometimes the fsm is not created (scripted command etc.)

  if (_stack.Size() <= 0)
  {
    PushTask(task, context);
    return;
  }
  _stack.Insert(0);
  _stack[0]._task = new Task(task);
  _stack[0]._fsm = fsm;
  OnTaskCreated(*_stack[0]._task, true);
  _stack[0]._fsm->SetState(0); // State 0 is starting state
}

template <class Task, class ContextType>
void AbstractAIMachine<Task, ContextType>::PopTask(ContextType *context, bool doRefresh)
{
  int n = _stack.Size() - 1;
  if (n < 0)
  {
    return;
  }

  Exit(context);
  OnTaskDeleted(*_stack[n]._task);
  _stack.Delete(n);

  Enter(context);
  if (doRefresh) UpdateAndRefresh(context);
}

template <class Task, class ContextType>
void AbstractAIMachine<Task, ContextType>::Delete(int i,ContextType *context, bool doRefresh)
{
  if (GetCurrent())
  {
    context->_task = GetCurrent()->_task;
    context->_fsm = GetCurrent()->_fsm;
  }
  else
  {
    context->_task = NULL;
    context->_fsm = _noTaskFSM;
  }

  if( i==_stack.Size()-1 ) PopTask(context, doRefresh);
  else
  {
    OnTaskDeleted(*_stack[i]._task);
    _stack.Delete(i);
  }
}

template <class Task, class ContextType>
void AbstractAIMachine<Task, ContextType>::Clear(ContextType *context)
{
  if (_stack.Size() == 0)
  {
    return;
  }

  Exit(context);
  for (int i=_stack.Size()-1; i>=0; i--)
    OnTaskDeleted(*_stack[i]._task);
  _stack.Clear();

  if (context)
  {
    Enter(context);
    UpdateAndRefresh(context);
  }
}

template <class Task, class ContextType>
bool AbstractAIMachine<Task, ContextType>::Update(ContextType *context)
{
  if (GetCurrent())
  {
    context->_task = GetCurrent()->_task;
    context->_fsm = GetCurrent()->_fsm;
    if (GetCurrent()->_fsm->Update(context))
    {
      PopTask(context);
      return true;
    }
  }
  else
  {
    context->_task = NULL;
    context->_fsm = _noTaskFSM;
    _noTaskFSM->Update(context);
  }
  return false;
}

template <class Task, class ContextType>
void AbstractAIMachine<Task, ContextType>::UpdateAndRefresh(ContextType *context)
{
  if (GetCurrent())
  {
    context->_task = GetCurrent()->_task;
    context->_fsm = GetCurrent()->_fsm;
  }
  else
  {
    context->_task = NULL;
    context->_fsm = _noTaskFSM;
  }
  context->_fsm->Refresh(context);
  Update(context);
}

template <class Task, class ContextType>
void AbstractAIMachine<Task, ContextType>::Enter(ContextType *context)
{
  if (GetCurrent())
  {
    context->_task = GetCurrent()->_task;
    context->_fsm = GetCurrent()->_fsm;
  }
  else
  {
    context->_task = NULL;
    context->_fsm = _noTaskFSM;
  }
  context->_fsm->Enter(context);
}

template <class Task, class ContextType>
void AbstractAIMachine<Task, ContextType>::Exit(ContextType *context)
{
  if (GetCurrent())
  {
    context->_task = GetCurrent()->_task;
    context->_fsm = GetCurrent()->_fsm;
  }
  else
  {
    context->_task = NULL;
    context->_fsm = _noTaskFSM;
  }
  context->_fsm->Exit(context);
}

///////////////////////////////////////////////////////////////////////////////
// class AILocker

class RoadLink;
//TypeIsSimpleZeroed(InitPtr<RoadLink>);

struct BuildingLockInfo
{
  OLink(Object) house;
  int index;
};
TypeContainsOLink(BuildingLockInfo)

/// used for temporary unlocks of active locks
/**
No memory allocation used - memory is moved by rebinding the pointer
*/

struct AILockerSavedState
{
  AutoArray< InitPtr<RoadLink> > _roadsSoldiers;
  AutoArray< InitPtr<RoadLink> > _roadsVehicles;
  AutoArray<BuildingLockInfo> _buildings;
};

/// for occupied location locking
class AILocker : public TaskEnabledRefCount
{
  /// locked fields
  RefArray<LockField> _fields;  
  /// locked roads (by soldiers)
  AutoArray< InitPtr<RoadLink> > _roadsSoldiers;
  /// locked roads (by vehicles)
  AutoArray< InitPtr<RoadLink> > _roadsVehicles;
  /// locked building positions
  AutoArray<BuildingLockInfo> _buildings;
  
  typedef AILockerSavedState SavedState;

  /// Locks are actually locked in locking map
  bool _tempLocked;
  /// Type of lock (all vehicles or all vehicles but soldiers)
  bool _lockedSoldier;
  /// Type of lock (waiting only, or a long term stop)
  bool _lockedWaiting;
  /// How large is the lock?
  float _lockedRadius;
  /// Size of the locking vehicle (used for the roads and buildings locks)
  float _lockedSize;
  /// Where is the lock?
  Vector3 _lockedBeg;

  /// time of last movement, used to distinguish between waiting/full lock
  Time _lastMovementTime;
  /// Only single thread can access structures at a time
  mutable CriticalSection _criticalSection;

public:
  AILocker();
  ~AILocker();

  /// lock in both layers
  void Lock(Vector3Val pos, float radius, bool soldier, bool canWait, float size);
  /// unlock in both layers
  void Unlock(SavedState *state = NULL);
  /// relock in both layers
  void Relock(SavedState &state);

  /// react to movement - touch the movement time
  void Move();

  /// check if these locker is locking the field
  int IsLocking(OperMapIndex x, OperMapIndex z, bool soldier) const;
  /// check if these locker is locking the road segment
  int IsLocking(const RoadLink *road, bool soldier) const;
  /// check if these locker is locking the building position
  int IsLocking(const Object *house, int index) const;

protected:
  // implementation
  void LockItem(OperMapIndex x, OperMapIndex z, bool soldier);
  void UnlockItem(OperMapIndex x, OperMapIndex z, bool soldier);

  /// lock position with the LockMaskVeh - lock valid only for vehicles
  void LockPosition( Vector3Val pos, float radius, bool soldier, bool waiting, float size);
  /// unlock position with the LockMaskVeh
  void UnlockPosition( Vector3Val pos, float radius, bool soldier, bool waiting, SavedState *state=NULL);
  /// relock position with the LockMaskVeh
  void RelockPosition( Vector3Val pos, float radius, bool soldier, bool waiting, float size, SavedState &state);

  /// lock position with the LockMaskAll - lock valid for both vehicles and men
  void LockPositionAll( Vector3Val pos, float radius, bool soldier, bool waiting, float size);
  /// unlock position with the LockMaskAll
  void UnlockPositionAll( Vector3Val pos, float radius, bool waiting,SavedState *state=NULL);
  /// relock position with the LockMaskAll
  void RelockPositionAll( Vector3Val pos, float radius, bool waiting, SavedState &state);



};

///////////////////////////////////////////////////////////////////////////////
// class AIUnit

extern const Vector3 VUndefined;

#include "../pathSteer.hpp"

#define CREATE_AI_BRAIN_MSG(MessageName, XX) \
  XX(MessageName, OLinkPermO(Person), person, NDTRef, NetworkId,  NCTNone, DEFVALUENULL, DOC_MSG("Attached body"), TRANSF_REF) \
  XX(MessageName, RString, name, NDTString, RString, NCTNone, DEFVALUE(RString, ""), DOC_MSG("Full name"), TRANSF) \
  XX(MessageName, RString, face, NDTString, RString, NCTNone, DEFVALUE(RString, ""), DOC_MSG("Face"), TRANSF) \
  XX(MessageName, RString, glasses, NDTString, RString, NCTNone, DEFVALUE(RString, ""), DOC_MSG("Glasses type"), TRANSF) \
  XX(MessageName, RString, speaker, NDTString, RString, NCTNone, DEFVALUE(RString, ""), DOC_MSG("Speaker"), TRANSF) \
  XX(MessageName, float, pitch, NDTFloat, float, NCTNone, DEFVALUE(float, 1.0f), DOC_MSG("Voice pitch"), TRANSF) \
  XX(MessageName, int, rank, NDTInteger, int, NCTSmallUnsigned, DEFVALUE(int, RankPrivate), DOC_MSG("Current rank"), TRANSF) \
  XX(MessageName, float, experience, NDTFloat, float, NCTNone, DEFVALUE(float, 0), DOC_MSG("Amount of experience"), TRANSF) \
  XX(MessageName, float, initExperience, NDTFloat, float, NCTNone, DEFVALUE(float, 0), DOC_MSG("Initial amount of experience"), TRANSF) \
  XX(MessageName, int, roleIndex, NDTInteger, int, NCTSmallSigned, DEFVALUE(int, -1), DOC_MSG("Multiplayer role index"), TRANSF) \
  XX(MessageName, RString, squadPicture, NDTString, RString, NCTNone, DEFVALUE(RString, ""), DOC_MSG("Squad picture (shown on vehicles)"), TRANSF) \
  XX(MessageName, RString, squadTitle, NDTString, RString, NCTNone, DEFVALUE(RString, ""), DOC_MSG("Squad title (shown on vehicles)"), TRANSF) \
  XX(MessageName, AutoArray<int>, objectIdHistory, NDTIntArray, AutoArray<int>, NCTSmallUnsigned, DEFVALUEINTARRAY, DOC_MSG("respawn history"), TRANSF)

DECLARE_NET_INDICES_EX(CreateAIBrain, NetworkObject, CREATE_AI_BRAIN_MSG)

#define CREATE_AI_UNIT_MSG(MessageName, XX) \
  XX(MessageName, OLinkPermNO(AISubgroup), subgroup, NDTRef, NetworkId,  NCTNone, DEFVALUENULL, DOC_MSG("Superior subgroup"), TRANSF_REF) \
  XX(MessageName, int, id, NDTInteger, int, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG("ID in group"), TRANSF) \

DECLARE_NET_INDICES_EX(CreateAIUnit, CreateAIBrain, CREATE_AI_UNIT_MSG)

#define CREATE_AI_AGENT_MSG(MessageName, XX) \

DECLARE_NET_INDICES_EX(CreateAIAgent, CreateAIBrain, CREATE_AI_AGENT_MSG)

//! network message indices for AIUnit class
/*!
\patch 1.11 Date 07/27/2001 by Jirka
- Improved: MP code
\patch_internal 1.10 Date 07/27/2001 by Jirka
- some information from AIUnit are not sent through network now (operative path etc.)
*/

const float AIUpdateErrorCoef = 0.1f;

#define UPDATE_AI_BRAIN_MSG(MessageName, XX) \
  XX(MessageName, OLinkPermO(Person), person, NDTRef, NetworkId,  NCTNone, DEFVALUENULL, DOC_MSG("Attached body"), TRANSF_REF, ET_NONE, 0) \
  XX(MessageName, float, experience, NDTFloat, float, NCTNone, DEFVALUE(float, 0), DOC_MSG("Amount of experience"), TRANSF, ET_ABS_DIF, AIUpdateErrorCoef * 0.01 * ERR_COEF_VALUE_MINOR) \
  XX(MessageName, float, ability, NDTFloat, float, NCTFloat0To1, DEFVALUE(float, 0.2), DOC_MSG("Ability of (AI) unit"), TRANSF, ET_ABS_DIF, AIUpdateErrorCoef * ERR_COEF_VALUE_MAJOR) \
  XX(MessageName, int, semaphore, NDTInteger, int, NCTSmallUnsigned, DEFVALUE(int, SemaphoreYellow), DOC_MSG("Combat mode"), TRANSF, ET_NOT_EQUAL, AIUpdateErrorCoef * ERR_COEF_MODE) \
  XX(MessageName, int, combatModeMajor, NDTInteger, int, NCTSmallUnsigned, DEFVALUE(int, CMAware), DOC_MSG("Behaviour"), TRANSF, ET_NOT_EQUAL, AIUpdateErrorCoef * ERR_COEF_MODE) \
  XX(MessageName, Time, dangerUntil, NDTTime, Time, NCTNone, DEFVALUE(Time, Time(0)), DOC_MSG("In danger mode until..."), TRANSF, ET_NOT_EQUAL, AIUpdateErrorCoef * ERR_COEF_MODE) \
  XX(MessageName, int, captive, NDTInteger, int, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG("Captive unit"), TRANSF, ET_NONE, 0) \
  XX(MessageName, OLink(Object), house, NDTRef, NetworkId,  NCTNone, DEFVALUENULL, DOC_MSG("House, where destination is"), TRANSF_REF, ET_NOT_EQUAL, AIUpdateErrorCoef * ERR_COEF_VALUE_MAJOR) \
  XX(MessageName, int, housePos, NDTInteger, int, NCTSmallSigned, DEFVALUE(int, -1), DOC_MSG("Destination position in house"), TRANSF, ET_NOT_EQUAL, AIUpdateErrorCoef * ERR_COEF_VALUE_MAJOR) \
  XX(MessageName, Vector3, wantedPosition, NDTVector, Vector3, NCTNone, DEFVALUE(Vector3, VUndefined), DOC_MSG("Destination position"), TRANSF, ET_NOT_EQUAL, AIUpdateErrorCoef * ERR_COEF_VALUE_MINOR) \
  XX(MessageName, int, planningMode, NDTInteger, int, NCTSmallUnsigned, DEFVALUE(int, DoNotPlan), DOC_MSG("How to plan path"), TRANSF, ET_NOT_EQUAL, AIUpdateErrorCoef * ERR_COEF_VALUE_MINOR) \
  XX(MessageName, int, state, NDTInteger, int, NCTSmallUnsigned, DEFVALUE(int, Wait), DOC_MSG("Unit (FSM) state"), TRANSF, ET_NOT_EQUAL, AIUpdateErrorCoef * ERR_COEF_MODE) \
  XX(MessageName, int, mode, NDTInteger, int, NCTSmallUnsigned, DEFVALUE(int, Normal), DOC_MSG("Precision of path planning"), TRANSF, ET_NOT_EQUAL, AIUpdateErrorCoef * ERR_COEF_MODE) \
  XX(MessageName, int, lifeState, NDTInteger, int, NCTSmallUnsigned, DEFVALUE(int, LifeStateAlive), DOC_MSG("Life state of unit (Alive, Death, Asleep etc.)"), TRANSF, ET_NOT_EQUAL, AIUpdateErrorCoef * ERR_COEF_MODE) \
  XX(MessageName, bool, getInAllowed, NDTBool, bool, NCTNone, DEFVALUE(bool, true), DOC_MSG("Can get in vehicles"), TRANSF, ET_NOT_EQUAL, AIUpdateErrorCoef * ERR_COEF_MODE) \
  XX(MessageName, bool, getInOrdered, NDTBool, bool, NCTNone, DEFVALUE(bool, false), DOC_MSG("Must get in vehicles"), TRANSF, ET_NOT_EQUAL, AIUpdateErrorCoef * ERR_COEF_MODE) \
  XX(MessageName, AutoArray<int>, objectIdHistory, NDTIntArray, AutoArray<int>, NCTSmallUnsigned, DEFVALUEINTARRAY, DOC_MSG("respawn history"), TRANSF, ET_NOT_EQUAL,  AIUpdateErrorCoef * ERR_COEF_MODE) \
  XX(MessageName, OLinkPermO(Person), remoteControlled, NDTRef, NetworkId,  NCTNone, DEFVALUENULL, DOC_MSG("The possible person we are controlled by"), TRANSF_REF, ET_NOT_EQUAL, AIUpdateErrorCoef * ERR_COEF_STRUCTURE) \

DECLARE_NET_INDICES_EX_ERR(UpdateAIBrain, NetworkObject, UPDATE_AI_BRAIN_MSG)

#define UPDATE_AI_UNIT_MSG(MessageName, XX) \
  XX(MessageName, float, formationAngle, NDTFloat, float, NCTFloatAngle, DEFVALUE(float, 0), DOC_MSG("Relative orientation in formation"), TRANSF, ET_NONE, 0) \
  XX(MessageName, Vector3, formationPos, NDTVector, Vector3, NCTNone, DEFVALUE(Vector3, VZero), DOC_MSG("Relative position in formation"), TRANSF, ET_NONE, 0) \

DECLARE_NET_INDICES_EX_ERR(UpdateAIUnit, UpdateAIBrain, UPDATE_AI_UNIT_MSG)

#define UPDATE_AI_AGENT_MSG(MessageName, XX) \

DECLARE_NET_INDICES_EX_ERR(UpdateAIAgent, UpdateAIBrain, UPDATE_AI_AGENT_MSG)

//! callback function type - see also FindNearestEmptyCallback
typedef bool FindFreePositionCallback(Vector3Par pos, void *context);

extern bool DefFindFreePositionCallback(Vector3Par pos, void *context);

#ifndef DECL_ENUM_ABILITY_KIND
#define DECL_ENUM_ABILITY_KIND
DECL_ENUM(AbilityKind)
#endif
class MagazineType;

class TargetFunctor
{
public:
  // return true to break enumeration
  virtual bool operator ()(Target *target) const = 0;
};

/// interface to provide information about friendly / enemy
struct SideInfo
{
  virtual bool SideIsFriendly(TargetSide side) const = 0;
  virtual bool SideIsEnemy(TargetSide side) const = 0;
  virtual bool SideIsNeutral(TargetSide side) const = 0;
};

class TargetGhost;

/// Role of long-term FSM
#define BEHAVIOUR_FSM_ENUM(type, prefix, XX) \
  XX(type, prefix, Formation)

#ifndef DECL_ENUM_BEHAVIOUR_FSM_ROLE
#define DECL_ENUM_BEHAVIOUR_FSM_ROLE
DECL_ENUM(BehaviourFSMRole)
#endif
DECLARE_ENUM(BehaviourFSMRole, BFSM, BEHAVIOUR_FSM_ENUM)

/// Role of short-term FSM
#define REACTION_FSM_ENUM(type, prefix, XX) \
  XX(type, prefix, Danger)

#ifndef DECL_ENUM_REACTION_FSM_ROLE
#define DECL_ENUM_REACTION_FSM_ROLE
DECL_ENUM(ReactionFSMRole)
#endif
DECLARE_ENUM(ReactionFSMRole, RFSM, REACTION_FSM_ENUM)




DEFINE_ENUM_BEG(DangerCause)
  DCEnemyDetected, // the first enemy detected
  DCFire, // fire visible
  DCHit, // vehicle hit
  DCEnemyNear, // enemy very close to me
  DCExplosion, // explosion detected
  DCDeadBodyGroup, // dead soldier from my group found
  DCDeadBody, // other dead soldier found
  DCScream, // hit soldier screaming
  DCCanFire // new opportinity to fire on an enemy target
DEFINE_ENUM_END(DangerCause)


struct IdentityInfo
{
  RString _name;
  RString _face;
  RString _glasses;
  RString _speaker;
  float _pitch;
};

//! Abstract brain structure
class AIBrain : public AI
{
public:
  friend class SetGunnerStateFunc;
  friend class ClearGunnerMapFunc;

#if _ENABLE_DIRECT_MESSAGES
  enum SelectedChannel
  {
    SCRadio, // prefer radio channel
    SCDirect, // prefer direct speaking
    SCAuto // select channel automatically
  };
  enum GestureEnabled
  {
    GEEnabled,
    GEDisabled,
    GEAuto
  };
  enum VoiceEnabled
  {
    VEEnabled,
    VEDisabled,
    VEAuto
  };
#endif
#if _VBS3
  enum POW_Status
  {
    POWNone = 0,
    POWCaptive = 1,
    POWCanCapture = 2,
    POWHandcuffed = 4,
    POWCaptured = 8
  };
#endif

  enum State
  {
    /// another plan is needed (current partial plan was done or is not present)
    Wait,
    /// operative planing should start - endpoint is set
    Init,
    /// init failed because data are not ready
    InitFailed,
    /// operative plan ready and valid
    Busy,
    /// completed whole strategic plan
    Completed,
    /// repeat search - usually when failed
    Delay,
    /// unit is in vehicle cargo - no planning needed
    InCargo,
    /// unit is stopping (usually because of Getin/getout request)
    Stopping,
    /// we have a plan, but we would like to search a new one
    Replan,
    /// unit is stopped - no planning needed
    Stopped,
    /// operative planning pending
    Planning
  };
  enum PlanningMode
  {
    /// used when not moving
    DoNotPlan,
    /// used in formation when simple path testing is used
    DoNotPlanFormation,
    /// used for formation leader (full path finding used)
    LeaderPlanned,
    /// used for DirectGo (like getin, supply)
    LeaderDirect,
    /// used in formation when full path finding is used
    FormationPlanned,
    /// used for vehicle driver
    VehiclePlanned,
  };
  enum Mode
  {
    DirectNormal,
    DirectExact,
    Normal,
    Exact
  };
  enum WatchMode
  {
    WMNo,
    WMDir,
    /// look permanently at given position
    WMPos,
    /// glance at given position
    WMPosGlance,
    /// look permanently at given target
    WMTgt,
    /// look around
    WMAround,
    /// glance at given target
    WMTgtGlance,
    NWatchModes
  };
  enum DisabledAI
  { // note: serialized as value - only add values to retain compatilibity
    DATarget=1, // group commander may not assign targets
    DAMove=2, // do not move
    DAAutoTarget=4, // no automatic target selection
    DAAnim=8, // no automatic animation selection
    DATeamSwitch=16, // AI disabled because of Team Switch
#if _VBS2 && _VBS3  // disableAI PATHPLAN
    DAPathPlan=32, // don't path plan, just move
#endif
    DAFSM=64,
  };

protected:
  OLinkPermO(Person) _person;   // who we are
  OLinkPermO(Transport) _inVehicle; // which vehicle are we in
  OLinkPermO(Transport) _vehicleAssigned;
  AutoArray<int> _objectIdHistory; //respawn history

  /// who has a remote control over this unit (used for UAV planes for example)
  OLinkPermO(Person) _remoteControlled;
  bool _remoteControlling;

  /// objects synchronized with this unit using mission editor or scripting command
  OLinkPermOArray(EntityAI) _synchronizedObjects;

  LifeState _lifeState;
  int _disabledAI; // disabled single elements of AI (DisabledAI)
#if !_ENABLE_IDENTITIES
  float _ability;
#endif
  /// "major" is explicitly set via waypoints or player given commands
  CombatMode _combatModeMajor;
  Semaphore _semaphore;
  Time _dangerUntil;

#if DEBUG_FSM
  signed char _fsmAutoDebug;
#else
  static const signed char _fsmAutoDebug = 0;
#endif

  FSM *CreateFSM(const EntityType *type, RString name);

  /// long-term FSM instances
  SRef<FSM> _behaviourFSMs[NBehaviourFSMRole];
  /// short-term FSM instance - react to danger
  SRef<FSM> _reactionFSM;
#if _ENABLE_CONVERSATION
  /// short-term FSM instance - social behaviours
  /** lower priority than _reactionFSM, suspended while _reactionFSM is executed */
  SRef<FSM> _conversationFSM;

  /// talk events to be processed
  /**
  Includes some events other than talk, like when somebody approached us
  */
public:
  struct TalkEvent
  {
    /// who is source of the event
    OLinkPermNO(AIBrain) _from;
    /// content of the event
    Ref<const KBMessageInfo> _event;

    //@{ mimic SerializeClass interface
    LSError Serialize(ParamArchive &ar);
    //@}

    ClassContainsOLink(TalkEvent)
  };
protected:
  /// a queue of talk related events to be processed
  AutoArray<TalkEvent> _talkEvents;

  /// list of other characters we are currently assuming to be in conversation with
  /**
  Any unit which we address or which addresses us is added here.
  It is removed once reasonable, e.g. it goes too far away.
  "Addressing" may be even non-verbal (like saluting, or simply standing in front of someone)

  Currently we are always convesing with one unit only. In future this may be an array of units.
  */
  OLinkPermNO(AIBrain) _conversingWith;
  //OLinkPermNOArray(AIBrain) _conversingWith;

#endif

  /// preferred target for attacking / watching
  LinkTarget _targetAssigned;
  /// leader has enabled firing at the target
  LinkTarget _targetEnableFire;

  /// fictive (ghost) target used for fire sector/suppressive fire control
  RefR<TargetGhost> _ghostTarget;
  
  WatchMode _watchMode;
  /// position to watch (look at)
  Vector3 _watchPos;
  /// position to glance
  Vector3 _glancePos;
  /// target to watch (look at)
  LinkTarget _watchTgt;
  /// target to glance at
  LinkTarget _glanceTgt;
  Time _watchDirSet; // for WMAround
  Vector3 _watchDirHead; //!< direction to watch by head - world space
  Vector3 _watchDir; //!< direction to watch by head and body - world space

  float _nearestEnemyDist2;
  
  /// approximate direction range to enemies we are hiding from - leftmost angle
  Vector3 _enemyDirL;
  /// approximate direction range to enemies we are hiding from - rightmost angle
  Vector3 _enemyDirR;

  /// value of _wantedPosition before transformed by a FSM
  PlanPosition _wantedPositionWanted;
  /// what precision is desired when moving into _wantedPositionWanted
  float _wantedPrecision;
  /// value of _planningMode before transformed by a FSM
  PlanningMode _planningModeWanted;
  /// value of forceReplan flag (passed to SetWantedPosition) before transformed by a FSM
  bool _forceReplanWanted;

  /// forceSpeed < 0 means forceSpeed disabled. Speed forced otherwise.
  float _forceSpeed;

#if _VBS3
  bool _allowSpeech;
#endif
  // captive, changed to int for VBS, can now code different states (see POW)
  int _captive;

  // TODO: check usage of this flag
  bool _getInOrdered;
  bool _getInAllowed;
  bool _completedReceived;

  bool _noPath;
  bool _updatePath;
  bool _lastPlan;

  float _exposureChange;

  /// used for MP - unit is playable if _roleIndex >= 0
  int _roleIndex;

  /// voice used for radio and other communications
  SRef<Speaker> _speaker;

  /// after cover is left, FSM should set the search parameters. Until this is done, we cannot plan
  bool _planParametersSet;
  
  /// desired end point of strategic plan
  PlanPosition _wantedPosition;
  /// end point of current strategic plan
  PlanPosition _plannedPosition;

  /// desired end point of operation path planning
  PlanPosition _expPosition;

  /// current operative plan searching state
  OperMap _map;
  //@{ planning FSM state
  State _state;
  PlanningMode _planningMode;
  Mode _mode;
  /// when failed, we repeat a few more times with updated heuristics
  int _iter;
  /// counter for trying of finding path
  Time _delay;
  
  /// operative path
  Path _path;
  /// index of the path (increased with each operative planning)
  int _pathId;
  
  
  /// target location in the house - which house
  OLink(Object) _house;
  /// target location in the house - which position
  int _housePos;

  /// part of the old path we will reuse
  AutoArray<OperInfoResult> _oldPath;
  
  /// max. time to get into a cover (see also CoverDisposition::_maxCost)
  float _maxCostToCover;
  /// max. delay imposed by a cover (see also CoverDisposition::_maxDelay)
  float _maxDelayByCover;
  //@}

  //@{ current operative plan searching state
  Vector3 _from;
  PlanPosition _to;
  /// when from is not free, path finding started at nearest free location
  bool _fromBusy;
  /// if path is ending on the road, we are happy with it
  bool _toRoad;
  //@}

#if _ENABLE_AI
  //@{ current strategic plan searching state
  SRef<IAIPathPlanner> _planner;
  float _completedTime;
  Time _waitWithPlan;
  int _attemptPlan;
  //@}
#endif

#if _ENABLE_CONVERSATION
  Ref<KBCenter> _kbCenter;
  int _speaking;
  int _listening;

#endif

#if _ENABLE_DIRECT_MESSAGES
  SelectedChannel _selectedChannel;
  GestureEnabled _gestureEnabled;
  VoiceEnabled _voiceEnabled;
#endif
  // when set to true, someone load identity before delayed EH is processed - avoid identity overwrite
  bool _loadIdentityDelayed;

public:
  AIBrain(Person *vehicle);

  Person *GetPerson() const {return _person;}
  void SetPerson(Person *vehicle);
  Transport *GetVehicleIn() const {return _inVehicle;}
  void SetVehicleIn(Transport *vehicle) {_inVehicle = vehicle;}
  EntityAIFull *GetVehicle() const;
  bool IsFreeSoldier() const {return GetVehicleIn() == NULL;}
  bool IsSoldier() const
  {
    return _inVehicle.GetLink() == NULL || IsInCargo();
  }

  //respawn history
  void AddObjectIDHistory(int objectId);
  AutoArray<int> GetObjectIdHistory() { return _objectIdHistory;}

  /// who has a remote control over this unit (used for UAV planes for example)
  Person *GetRemoteControlled() const {return _remoteControlled;}
  /// switch on the remote control of the unit
  bool SetRemoteControlled(Person *by); // return false when controlled unit is not local and networking to ensure it was just started
  void SetRemoteControlling(bool controlling)  {_remoteControlling = controlling;}
  bool IsRemoteControlling() {return _remoteControlling;}

  Transport *VehicleAssigned() const {return _vehicleAssigned;}
  void UnassignVehicle();
  bool AssignAsDriver(Transport *veh);
  bool AssignAsGunner(Transport *veh);
  bool AssignAsCommander(Transport *veh);
  bool AssignAsCargo(Transport *veh, int index = -1, bool reassignWhenConflict = false);
  bool AssignToTurret(Transport *veh, Turret *turret);
  // must be used in pair with vehicle assignment (faster where position is known)
  void ClearAssignedVehicle() {_vehicleAssigned = NULL;}

  bool ProcessGetIn(Transport &veh);
  bool ProcessGetIn2(Transport *veh, UIActionType pos, Turret *turret, int cargoIndex);

  const OLinkPermOArray(EntityAI) &GetSynchronizedObjects() const {return _synchronizedObjects;}
  void AddSynchronizedObject(EntityAI *object) {_synchronizedObjects.AddUnique(object);}
  void RemoveSynchronizedObject(EntityAI *object) {_synchronizedObjects.Delete(object);}

  LifeState GetLifeState() const {return _lifeState;}
  void SetLifeState(LifeState state);


  //@{ Check if unit is able to do some things with the current _lifeState
  bool LSIsAlive() const {return _lifeState == LifeStateAlive || _lifeState == LifeStateAsleep || _lifeState == LifeStateUnconscious;}
  bool LSCanSpeak() const {return _lifeState == LifeStateAlive || _lifeState == LifeStateAsleep || _lifeState == LifeStateUnconscious;}
  bool LSCanLead() const {return _lifeState == LifeStateAlive || _lifeState == LifeStateAsleep || _lifeState == LifeStateUnconscious;}
  // Unconscious person cannot command
  bool LSCanCommand() const {return _lifeState == LifeStateAlive || _lifeState == LifeStateAsleep;}
  bool LSCanTrackTargets() const {return _lifeState == LifeStateAlive || _lifeState == LifeStateAsleep || _lifeState == LifeStateUnconscious;}
  bool LSCanSupport() const {return _lifeState == LifeStateAlive || _lifeState == LifeStateAsleep || _lifeState == LifeStateUnconscious;}
  bool LSCanGetInGetOut() const {return _lifeState == LifeStateAlive || _lifeState == LifeStateAsleep || _lifeState == LifeStateUnconscious;}
  // Unconscious person cannot use actions menu
  bool LSCanProcessActions() const {return _lifeState == LifeStateAlive || _lifeState == LifeStateAsleep;}
  //@}

  int GetAIDisabled() const {return _disabledAI;}
  void SetAIDisabled(int state) {_disabledAI = state;}
  bool IsAIDisabled(DisabledAI state) const {return (_disabledAI&state)!=0;}

  bool IsPlayable() const {return _roleIndex >= 0;}
  int GetRoleIndex() const {return _roleIndex;}
  void SetRoleIndex(int index) {_roleIndex = index;}

  Speaker *GetSpeaker() const {return _speaker;}
  void SetSpeaker(RString speaker, float pitch);
  /// Set the Speaker using _info for AI but PlayerIdentity for Players
  void SetSpeaker();

  //! no difficulty settings correction
  float GetRawAbility() const; // returns from 1 (maximal) to 0.2 (unable)
  void SetRawAbility(float ability);

  //! include correction based on difficulty settings
  float GetAbility(AbilityKind kind) const; // returns from 1 (maximal) to 0.2 (unable)
  //! include correction based on difficulty settings
  float GetInvAbility(AbilityKind kind) const; // returns from 5 (unable) to 1 (maximal)

  bool IsGetInOrdered() const {return _getInOrdered;}
  void OrderGetInLocal(bool flag) {_getInOrdered = flag;}
  bool IsGetInAllowed() const {return _getInAllowed;}
  void AllowGetInLocal(bool flag);

  /// MP safe version of OrderGetInLocal
  void OrderGetIn(bool flag);
  /// MP safe version of AllowGetInLocal
  void AllowGetIn(bool flag);

  /// process target list, creating an aggregate information (general enemy direction)
  void CheckEnemies();

  /// process target list, creating an aggregate information about how endangered is given position
  float CheckDanger(Vector3Par pos) const;

  /// we want to avoid some decision being synchronized - each brain has its own seed
  virtual int RandomSeed() const = 0;

  //! random decision model
  bool RandomDecision(float period, float ratio) const;

  /// check current morale level
  virtual float GetMorale() const = 0;

  State GetState() const {return _state;}
  //! Set state of unit
  /*!
  \param vehChanged if true, enable change of InCargo state
  */
  bool SetState(State state, bool vehChanged = false);
  void ClearCompletedReceived() {_completedReceived = false;}
  Mode GetMode() const {return _mode;}
  void SetMode(Mode mode) {_mode = mode;}
  bool IsPlanning() const;
  PlanningMode GetPlanningMode() const {return _planningMode;}
  int GetIter() const {return _iter;}
  const OperMap &GetOperMap() const {return _map;}

  //@{ behaviour FSM diagnostics
  int NBehaviourDiags() const;
  RString BehaviourDiags(int i) const;

#if _ENABLE_CHEATS
  virtual void DrawDiags(){}
#endif
  //@}
  

  bool IsPlayer() const; // main (local) player
  bool IsAnyPlayer() const; // any player - local or remote
  bool IsRemotePlayer() const;

  bool HasAI() const;

  /// is the unit part of formation?
  bool IsPartOfFormation(const AISubgroup *subgrp) const;
  /// can the unit be given orders?
  bool IsUnit() const;
  bool IsCommander() const;
  bool IsDriver() const;
  bool IsGunner() const;

  UnitPosition GetUnitPosition() const;
  void SetUnitPositionCommanded(UnitPosition status);
  void SetUnitPositionScripted(UnitPosition status);
  void SetUnitPositionFSM(UnitPosition status);

  Transport *FindRespawnVehicle() const;

  /// check if unit wants to fire
  /** used to select appropriate stance or hatch state in which firing is possible */
  bool IsFiringWanted() const;
  
  
  /// proper combat modes, used for behaviours (higher level)
  CombatMode GetCombatMode() const;
  /// low-level combat mode, used for basic behaviours (stance, hatch, reactions)
  CombatMode GetCombatModeLowLevel() const;
  
  CombatMode GetCombatModeMajor() const {return _combatModeMajor;}
  void SetCombatModeMajor(CombatMode mode);
  void OnCombatModeChanged(CombatMode oldMode, CombatMode newMode);
  /// set combat mode, call OnCombatModeChanged if result as seen be GetCombatMode() has changed
  void SetCombatModeMajorReactToChange(CombatMode mode);

  /// notify a new target will be known soon
  virtual void OnNewTarget(TargetNormal *target, Time knownSince);

  bool IsDanger() const;
  void SetDanger(DangerCause cause, Vector3Par position, const Target *causedBy, float until = -1.0);

  Semaphore GetSemaphore() const {return _semaphore;}
  void SetSemaphore(Semaphore status);
  bool IsHoldingFire() const;
  bool IsKeepingFormation() const;

  /// Path type we (or units before us) are currently moving on
  bool IsOnBridge() const {return IsOnRoad()>1;}
  /// Check path type we (or units before us) are currently moving on
  int IsOnRoad() const;

  Target *FindTargetAll(TargetType * id, bool includeBuildings=true) const; // find any (even unknown) target
  Target *FindTarget(TargetType * id, bool includeBuildings=true) const; // find known target 
  Target *AddTarget(
    EntityAI *object, float accuracy, float sideAccuracy, float delay,
    const Vector3 *pos=NULL, AIBrain *sensor=NULL, float sensorDelay=1e10
  );
  
  /// ghost target creation
  Target *CreateGhostTarget(
    const EntityAIType *type, Vector3Par pos,
    float typeAccuracy, float posAccuracy, float sensorDelay
  );
  /// intended for ghost targets, but can be used for any target
  void MoveTarget(Target *tgt, Vector3Par pos, float typeAccuracy, float posAccuracy);
  /// delete ghost target
  void DeleteGhostTarget(Target *tgt);

  Target *GetTargetAssigned() const {return _targetAssigned;}
  void AssignTarget(Target *target) {_targetAssigned = target;}

  Target *GetEnableFireTarget() const {return _targetEnableFire;}
  void EnableFireTarget(Target *target) {_targetEnableFire = target;}

  void SetGhostTarget(TargetGhost *tgt);
  TargetGhost *GetGhostTarget() const;
  /// get current target denoting the fire sector
  virtual Target *GetFireSectorTarget() const {return NULL;}

  /// check if the unit is a formation leader
  virtual bool IsFormationLeaderVehicle() const {return false;}

  float GetNearestEnemyDist2() const {return _nearestEnemyDist2;}
  void SetNearestEnemyDist2(float val) {_nearestEnemyDist2 = val;}

  int GetCaptive() const {return _captive;}
  void SetCaptive(int captive) {_captive=captive;}
#if _VBS3
  bool CanSpeak(){ return _allowSpeech;}
  void SetAllowSpeech(bool speech){_allowSpeech = speech;}
#endif
  void SetNoWatch();
  void SetWatchPosition(Vector3Val pos, bool glanceOnly=false);
  void SetWatchTarget(Target *tgt, bool glanceOnly=false);
  void SetWatchDirection( Vector3Val dir );
  void SetWatchAround();

  /// world space direction watched
  Vector3Val GetWatchHeadDirection() const {return _watchDirHead;}
  /// world space direction watched
  Vector3Val GetWatchDirection() const {return _watchDir;}
  /// watch mode determined what is the unit watching and how
  WatchMode GetWatchMode() const {return _watchMode;}

  ObjectVisualState const* GetFutureVisualState() const { return GetVehicle() ? &(GetVehicle()->FutureVisualState()) : NULL; }
  ObjectVisualState const* GetRenderVisualState() const { return GetVehicle() ? &(GetVehicle()->RenderVisualState()) : NULL; }
  
  Vector3Val Position(ObjectVisualState const* vs) const {return vs ? vs->Position() : VZero;}
  Vector3Val Direction(ObjectVisualState const* vs) const {return vs ? vs->Direction() : VForward;}

  Vector3 GetCurrentPosition() const {return Position(GetFutureVisualState());}

  Vector3 COMPosition(ObjectVisualState const* vs) const {return vs ? GetVehicle()->COMPosition(*vs) : VZero;}
  Vector3 VisiblePosition(ObjectVisualState const* vs) const {return vs ? GetVehicle()->VisiblePosition(*vs) : VZero;}
  Vector3 AimingPosition(ObjectVisualState const* vs, const AmmoType *ammo = NULL) const {return vs ? GetVehicle()->AimingPosition(*vs, ammo) : VZero;}
  float VisibleSize(ObjectVisualState const* vs) const {return vs ? GetVehicle()->VisibleSize(*vs) : 0.0f;}

  void CheckAmmo(CheckAmmoInfo &info);

  void AddExp(float exp);
  void IncreaseExperience(const EntityAIType& type, TargetSide side); // called when you kill/destroy sth.

  /// find agent side from commanding structure
  virtual TargetSide GetSide() const = 0;
  /// check if given target is enemy to me
  virtual bool IsEnemy(TargetSide side) const = 0;
  virtual bool IsNeutral( TargetSide side) const = 0;
  virtual bool IsFriendly(TargetSide side) const = 0;

  virtual bool IsInCargo() const = 0;
  virtual void SetInCargo(bool set) = 0;

  virtual void SetSearchTime(Time time) = 0;

  virtual void OnRespawnFinished() = 0;

  void DoGetOut(Transport *veh, bool eject, bool parachute, Vector3Val diff = VZero);
  bool ProcessGetOut(bool eject, bool parachute, Vector3Val diff = VZero);

  Path& GetPath() {return _path;}
  void SetPathCosts(float costReplan, float costForceReplan);
  const Path& GetPath() const {return _path;}
  int GetPathId() const {return _pathId;}
  
  void SetHouse(Object *house, int pos) {_house = house; _housePos = pos;}

  Vector3Val GetPlanningFrom() const {return _from;}

  Time GetDelay() const {return _delay;}

  bool IsSimplePath(Vector3Val from, Vector3Val pos) const;
  /// check if IsSimplePath has all data ready, preload them if necessary
  bool IsSimplePathReady(Vector3Val from, Vector3Val pos) const;

  Vector3 NearestIntersection(Vector3Val from, Vector3Val pos);

  void RemoveAction(PathAction *action);

  virtual const SideInfo *GetSideInfo() const = 0;
  
#if _ENABLE_INDEPENDENT_AGENTS
  virtual AIAgent *GetAgent() {return NULL;}
#endif

#if _ENABLE_AI
  void CreateStrategicPath(ThinkImportance prec);
  bool VerifyPath(bool waiting=false, float timeAhead=FLT_MAX) const;
  void CopyPath(const IAIPathPlanner &planner); 

  const IAIPathPlanner &GetPlanner() const {return *_planner;}
  float GetTimeCompleted() {return _completedTime;}
  void SetTimeCompleted(float time) {_completedTime = time;}
#endif

  float GetExposureChange() const {return _exposureChange;}
  void ClearExposureChange() {_exposureChange = 0;}

  bool IsFireEnabled(Target *tgt) const; // check if fire enabled at given target

  void Disclose(DangerCause cause, Vector3Par position, EntityAI *causedBy, bool discloseGroup = true); // unit was disclosed - do something

  void Load(const IdentityInfo &info); // init unit info

  bool CanHear(ChatChannel channel, AIBrain *sender) const;

  float GetTimeToLive() const;

  bool CheckEmpty(Vector3Par pos);
  /// before checking for nearest empty you may wish to check if the data are ready
  bool NearestEmptyReady(Vector3Par pos) const;

  static bool FindEmptyInRect(
    Vector3 &emptyPos, bool soldier, EntityAI *veh,
    const FrameBase &pos, const Vector3 *minmax, Vector3Par nearTo,
    int mask = MASK_AVOID_OBJECTS | MASK_PREFER_ROADS | MASK_USE_BUFFER
  );

  static bool FindNearestEmpty(Vector3 &pos, bool soldier, EntityAI *veh, int mask = MASK_AVOID_OBJECTS | MASK_PREFER_ROADS | MASK_USE_BUFFER);
  /// find an empty place nearest to the given location
  bool FindNearestEmpty(Vector3 &pos, int mask = MASK_AVOID_OBJECTS | MASK_PREFER_ROADS | MASK_USE_BUFFER);
  /// find an empty place in given rectangle nearest to the given location
  bool FindEmptyInRect(
    Vector3 &emptyPos, const FrameBase &pos, const Vector3 *minmax, Vector3Par nearTo,
    int mask = MASK_AVOID_OBJECTS | MASK_PREFER_ROADS | MASK_USE_BUFFER
  );

  /// search for the free area nearest to the given position
  static bool FindEmptyArea(Vector3 &center, float radius, float maxDistance, const EntityAIType *vehType);

  void SetWantedPosition(const PlanPosition &pos, float precision, PlanningMode mode, bool forceReplan = false);
  const PlanPosition &GetWantedDestination() const {return _wantedPosition;}
  void ForceReplan(bool clear = true);

  void SetPlanParametersSet(bool val=true){_planParametersSet=val;}
  bool GetPlanParametersSet() const {return _planParametersSet;}
  
  PlanningMode GetPlanningModeWanted() const {return _planningModeWanted;}
  void SetPlanningMode(PlanningMode mode, bool forceReplan);
  float GetForceSpeed() const {return _forceSpeed;}
  void SetForceSpeed(float speed) {_forceSpeed=speed;}

  //@{ interface for formation FSM
  /// check position which higher level requested from the formation FSM
  const PlanPosition &GetWantedPosition() const {return _wantedPositionWanted;}

  float GetWantedPrecision() const {return _wantedPrecision;}
  /// check position and planning modes which higher level requested from the formation FSM
  void GetExpectedDestination(PlanPosition &destination, float &precision, PlanningMode &planningMode, bool &forceReplan);
  /// request formation FSM to move at given position
  void SetWantedDestination(const PlanPosition &destination, PlanningMode planningMode, bool forceReplan);

  bool CheckReplanNeeded(float baseCoef, PlanningMode planningMode, bool forceReplan) const;
  //@}

  // serialization
  virtual RString GetAgentType() const = 0;
  virtual LSError Serialize(ParamArchive &ar);
  static AIBrain *CreateObject(ParamArchive &ar);
  static AIBrain *LoadRef(ParamArchive &ar);
  LSError SaveRef(ParamArchive &ar) const;

  // network transfer
  DECLARE_NETWORK_OBJECT
  static NetworkMessageFormat &CreateFormat
  (
    NetworkMessageClass cls,
    NetworkMessageFormat &format
  );
  TMError TransferMsg(NetworkMessageContext &ctx);
  float CalculateError(NetworkMessageContextWithError &ctx);

#if _ENABLE_CONVERSATION
  /// get primary KB center
  const KBCenter *GetKBCenter() const;
  /// get KB parent (secondary)
  const KBCenter *GetKBParent() const;

  void AddKBTopic(RString name, RString filename, RString taskType, GameValuePar eventHandler);
  void RemoveKBTopic(RString name);
  bool HasKBTopic(RString name);
  KBMessageInfo *CreateKBMessage(RString topic, GameValuePar sentence);
  void SetKBMessageArgument(KBMessageInfo *message, GameValuePar argument);
  void KBTell(AIBrain *to, KBMessageInfo *message, bool forceRadio = false);
  void KBReact(AIBrain *sender, const KBMessageInfo *question);

  bool IsSpeaking() const {return _speaking > 0;}
  bool IsListening() const {return _listening > 0;}
  void ChangeSpeaking(int diff) {_speaking += diff;}
  void ChangeListening(int diff) {_listening += diff;}
#else
  bool IsSpeaking() const {return false;}
  bool IsListening() const {return false;}
  void ChangeSpeaking(int diff) {}
  void ChangeListening(int diff) {}
#endif

#if _ENABLE_DIRECT_MESSAGES
  bool IsGestureEnabled(ChatChannel channel) const;
  bool IsVoiceEnabled(ChatChannel channel) const;
#else
  bool IsGestureEnabled(ChatChannel channel) const {return true;} // by default perform both gestures and voice
  bool IsVoiceEnabled(ChatChannel channel) const {return true;}
#endif

  // simple expression variables for sentence variants
  float GetSERank() const;
  float GetSECaptive() const;
  float GetSESafe() const;
  float GetSECombat() const;
  float GetSEStealth() const;
  float GetSEMorale() const;
  float GetSEDanger() const;

  virtual bool AssertValid() const;

  virtual AIBrain *GetFormationLeader() {return this;}
  virtual CombatModeRange GetCombatModeBasedOnCommand() const {return CombatModeRange(CMSafe,CMStealth);}
  virtual CautionRange GetCautionBasedOnCommand() const {return CautionRange();}
  virtual bool GetCoveringBasedOnCommand() const {return true;}
  virtual OperCover GetCover() const {return OperCover();}
  virtual void GetHideFrom(Vector3 &hideL, Vector3 &hideR) const {hideL = hideR = VZero;}

  /// check if path finding is allowed to find a path into a cover
  bool IsCoveringEnabled() const {return _maxCostToCover>0;}

  float GetPreferredCostToCover() const {return _maxCostToCover*0.7f;}

  void OnStepTimedOut();
  void OnStepCompleted();

  // clear operative plan and start planning in not in progress
  void UpdateOperativePlan();

  virtual TargetList *AccessTargetList() = 0;
  virtual const TargetList *AccessTargetList() const = 0;

  virtual bool ForEachTarget(const TargetFunctor &func) const = 0;

  // TODO: will be removed after full transfer to dynamic AI Structure
  virtual AIUnit* GetUnit() {return NULL;}
  virtual AIGroup* GetGroup() const {return NULL;}

  bool IsGroupLeader() const;

  /// check what is the combat mode based on observed behaviour of given unit
  CombatMode GetCombatModeAutoDetected() const;

  /// compute focus movement needed to scan for the target
  void ScanTarget(Target * tgt, Vector3 &tgtPos);

#if _ENABLE_CHEATS
  /// start FSM debugger for all FSMs currently running
  virtual void DebugFSM();
  /// start FSM debugger for all newly created FSMs
  void DebugFSM(int level);
#endif

  /// Can be identity load from delay processed EH
  bool CanLoadIdentityDelayed() const { return _loadIdentityDelayed; }
  /// Identity loaded before EH process
  void LoadIdentityDelayed() { _loadIdentityDelayed = false; }

protected:
  void ClearOperativePlan();
  void ClearStrategicPlan();
  void RefreshStrategicPlan();
#if _ENABLE_AI
  void OnStrategicPathFound();
  void OnStrategicPathNotFound(bool update);

  bool OperPath(ThinkImportance prec); // returns false if waiting for files
  /// process the path searching (call ProcessFindPath / CreateOperativePath bellow)
  bool CreateOperativePath();
  /// start the path searching (encapsulate BeginOperativePath)
  bool CreateOperativePath(Vector3Par from, const PlanPosition &pos);
  /// path searching initialization
  bool BeginOperativePath(Vector3Par from, const PlanPosition &pos);

  void FinishOperativePath(bool found);
  void OnOperativePathFound();
  void OnOperativePathNotFound();

public:
  bool ObstacleFree(const CoverInfo &object ) const;

protected:
#endif

  /// look at _watchTgt
  void WatchTarget();

  
  /// look as dictated by formation fire sectors
  virtual void WatchFormation();

  /// if no target is given, we may want to look around
  void GlanceRandomDirection();
  /// control head turning (glance behaviour)
  void GlanceAt(Target *tgt);

  // think subtasks
  void SetWatch();

};

#if _ENABLE_CONVERSATION
/// Used to store the context of conversation for example in HUD
struct ConversationContext
{
  /// speaking unit
  OLinkPermNO(AIBrain) _askingUnit;
  /// listening unit
  OLinkPermNO(AIBrain) _askedUnit;
  /// we need to store question for serialization
  Ref<const KBMessageInfo> _question;

  /// KB center defining the conversation topic
  Ref<const KBCenter> _center;
  /// topic reacting to
  RString _topic;

  /// the description of UI based on the event handler result
  GameValue _handlerResult;
  /// the description of the UI based on the class UI in the knowledge base file
  Ref<const KBUINodeInfo> _current;

  ConversationContext(AIBrain *askingUnit, AIBrain *askedUnit, const KBMessageInfo *question);
  ~ConversationContext();

  GameValue ExecuteHandler(GameValuePar handler, RString topic, const KBMessage *message);
};
#endif

#if _ENABLE_IDENTITIES

class FSMScriptedType;

typedef RString AITaskTypeName;

class AITaskType : public RemoveLLinks, public CountInstances<AITaskType>
{
protected:
  /// name of ParamFile definition
  AITaskTypeName _name;

  /// ParamFile definition
  ConstParamClassDeepPtr _parClass;

  /// used for item identification
  RString _typeName;
  /// type of FSM
  RefR<const FSMScriptedType, const RefCount> _fsmType;
  /// source code of condition
  RString _condition;
#if USE_PRECOMPILATION
  /// compiled condition
  CompiledExpression _conditionCompiled;
#endif
  /// expression for task description calculation
  RString _descriptionExp;
  /// expression for short task description calculation
  RString _descriptionShortExp;
  /// expression for task description used in in-game UI
  RString _descriptionHUDExp;
  /// expression for task destination calculation
  RString _destinationExp;
  // list of required resources
  AutoArray<AITaskResource> _resources;

  mutable int _instancesCreated;
  mutable int _instancesDeleted;

public:
  AITaskType(const AITaskTypeName &name);

  const AITaskTypeName &GetName() const {return _name;}
  RString GetTypeName() const {return _typeName;}
  const FSMScriptedType *GetFSMType() const;
  const AutoArray<AITaskResource> &GetResources() const {return _resources;}
  RString GetCondition() const {return _condition;}
#if USE_PRECOMPILATION
  const CompiledExpression &GetConditionCompiled() const {return _conditionCompiled;}
#endif
  RString GetDescriptionExp() const {return _descriptionExp;}
  RString GetDescriptionShortExp() const {return _descriptionShortExp;}
  RString GetDescriptionHUDExp() const {return _descriptionHUDExp;}
  RString GetDestinationExp() const {return _destinationExp;}
  RString GetSentence(RString name) const;

  int GetInstancesCreated() const {return _instancesCreated;}
  int GetInstancesDeleted() const {return _instancesDeleted;}
  void OnInstanceCreated() const {_instancesCreated++;}
  void OnInstanceDeleted() const {_instancesDeleted++;}

  LSError Serialize(ParamArchive &ar);
  static AITaskType *CreateObject(ParamArchive &ar);
};

typedef BankArray<AITaskType, DefLLinkBankTraits<AITaskType> > AITaskTypeBank;

extern AITaskTypeBank AITaskTypes;

/// Implementation of task using the FSM
class AITask : public Task, public CountInstances<AITask>
{
  typedef Task base;

protected:
#if _ENABLE_INDEPENDENT_AGENTS
  /// Who is performing the task
  AITeamMemberLink _owner;
  /// Who send the task
  AITeamMemberLink _sender;
#else
  /// Who is performing the task
  OLinkPerm<AIBrain> _owner;
#endif
  /// Type of the task
  Ref<const AITaskType> _type;
  /// State of the task FSM
  SRef<FSM> _fsm;
  /// Task priority
  float _priority;
  /// Task is running (is not suspended due to resources)
  bool _running;

public:
  AITask(ForSerializationOnly) {_priority = 0; _running = false;} // used for serialization
#if _ENABLE_INDEPENDENT_AGENTS
  AITask(
    AITeamMember *owner, AITeamMember *sender, Task *parent, const AITaskType *type,
    FSM *fsm, float priority);
#else
  AITask(
    AIBrain *owner, Task *parent, const AITaskType *type,
    FSM *fsm, float priority);
#endif

  const AITaskType *GetType() const {return _type;}
  FSM *GetFSM() {return _fsm;}
#if _ENABLE_INDEPENDENT_AGENTS
  AITeamMember *GetOwner() const {return _owner;}
  AITeamMember *GetSender() const {return _sender;}
#else
  AIBrain *GetOwner() const {return _owner;}
#endif
  virtual float GetPriority() const {return _priority;}
  virtual bool GetDescription(RString &result) const;
  virtual bool GetDescriptionShort(RString &result) const;
  virtual bool GetDescriptionHUD(RString &result) const;
  virtual bool GetDestination(Vector3 &result) const;
  RString GetSentence(RString name) const
  {
    return _type ? _type->GetSentence(name) : RString();
  }

  bool IsRunning() const {return _running;}
  void Run(bool run) {_running = run;}

  /// for direct serialization of AITask
  static AITask *CreateObject(ParamArchive &ar);
  LSError Serialize(ParamArchive &ar);

  static AITask *LoadRef(ParamArchive &ar);
  virtual LSError SaveRef(ParamArchive &ar);
  /// for direct serialization of Ref<AITask>
  static LSError LoadRefRef(ParamArchive &ar, Ref<AITask> &link)
  {
    link = AITask::LoadRef(ar);
    return (LSError)0;
  }
  /// for direct serialization of Ref<AITask>
  static LSError SaveRefRef(ParamArchive &ar, Ref<AITask> &link)
  {
    return link->SaveRef(ar);
  }

  DECL_SERIALIZE_TYPE_INFO(AITask, Task);
};

#endif

#if _ENABLE_INDEPENDENT_AGENTS

struct AITeamMemberContext
{
  AITeamMember *_teamMember;
  AITeamMemberContext(AITeamMember *teamMember) {_teamMember = teamMember;}
};

/// Agent - implementation of leaf node in AI hierarchy - brain of the Person
class AIAgent : public AIBrain, public AITeamMember, public SideInfo, public CountInstances<AIAgent>
{
  typedef AIBrain base;

protected:
  /// Target list
  TargetListInAgent _targetList;
#if _ENABLE_CONVERSATION
  /// List of targets no more stored in target list, but usable in conversation
  RefArray<TargetKnowledge> _targetKB;
#endif

  //@{ Expansive think timing
  Time _expensiveThinkTime;
  int _expensiveThinkFrac; // frac in ms
  void ExpensiveThinkDone(); // calculate time of next expensive think
  //@}

public:
  AIAgent(Person *vehicle);

  //@{ Reference counting redirection
  virtual int IAddRef() {return AddRef();}
  virtual int IRelease() {return Release();}
  //@}

  //@{ Soft link support redirection
  virtual TrackSoftLinks<IdTraits> *ITrack() {return Track();}
  virtual RemoveSoftLinks<IdTraits> *GetRemoveLinks() {return this;}
  virtual AITeamMember::LinkId GetLinkId() const {return AIBrain::GetLinkId();}
  //@}

  // AITeamMember implementation
  virtual void SimulateAI();
  virtual void ApplyContext(GameState &state);
  virtual AIAgent *GetAgent() {return this;}

  // AIBrain implementation
  virtual bool IsInCargo() const;
  virtual void SetInCargo(bool set);
  virtual void SetSearchTime(Time time);
  virtual void OnRespawnFinished();
  virtual TargetSide GetSide() const;

  virtual bool IsEnemy(TargetSide side) const;
  virtual bool IsNeutral( TargetSide side) const;
  virtual bool IsFriendly(TargetSide side) const;

  virtual int RandomSeed() const;

  virtual float GetMorale() const;

  //@{ implementation of SideInfo
  virtual bool SideIsFriendly(TargetSide side) const {return IsFriendly(side);}
  virtual bool SideIsEnemy(TargetSide side) const {return IsEnemy(side);}
  virtual bool SideIsNeutral(TargetSide side) const {return IsNeutral(side);}
  //@}

  virtual const SideInfo *GetSideInfo() const {return this;}

  virtual RString GetAgentType() const {return "Agent";}

  // NetworkObject implementation
  virtual RString GetDebugName() const;
  virtual AITeamMember *GetTeamMember() {return this;}
  virtual CombatModeRange GetCombatModeBasedOnCommand() const;
  virtual CautionRange GetCautionBasedOnCommand() const;
  virtual bool GetCoveringBasedOnCommand() const;

  virtual LSError Serialize(ParamArchive &ar);
  virtual LSError SaveTeamMemberRef(ParamArchive &ar);
  static LSError SaveLinkRef(ParamArchive &ar, OLinkPermNO(AIAgent) &link) {return AIBrain::SaveLinkRef(ar, link);}
  static LSError LoadLinkRef(ParamArchive &ar, OLinkPermNO(AIAgent) &link) {return AIBrain::LoadLinkRef(ar, link);}

  DECLARE_NETWORK_OBJECT
  virtual NetworkId GetNetworkId() const {return base::GetNetworkId();}
  static NetworkMessageFormat &CreateFormat
  (
    NetworkMessageClass cls,
    NetworkMessageFormat &format
  );
  static AIAgent *CreateObject(NetworkMessageContext &ctx);
  void DestroyObject();
  TMError TransferMsg(NetworkMessageContext &ctx);
  float CalculateError(NetworkMessageContextWithError &ctx);

  const TargetList &GetTargetList() const {return _targetList;}
  virtual TargetList *AccessTargetList() {return &_targetList;}
  virtual const TargetList *AccessTargetList() const {return &_targetList;}

  virtual bool ForEachTarget(const TargetFunctor &func) const;
  /// add the target to the long-term database
  void OnTargetDeleted(Target *t);

#if _ENABLE_CHEATS
  virtual void DebugFSM();
#endif

protected:
  // implementation
  bool CreateTargetList(bool initialize = false, bool report = true);
  float GetDamagePerMinute(TargetNormal *tar) const;
  float GetSubjectiveCost(TargetNormal *tar) const;
  void UpdateWhenUnassigned(AIGroup *group);
};

#define CREATE_AI_TEAM_MSG(MessageName, XX) \
  XX(MessageName, RString, typeName, NDTString, RString, NCTNone, DEFVALUE(RString, ""), DOC_MSG("Team type name"), TRANSF) \
  XX(MessageName, RString, name, NDTString, RString, NCTNone, DEFVALUE(RString, ""), DOC_MSG("Team name"), TRANSF) \

DECLARE_NET_INDICES_EX(CreateAITeam, NetworkObject, CREATE_AI_TEAM_MSG)

#define UPDATE_AI_TEAM_MSG(MessageName, XX) \
  XX(MessageName, FindArrayKey<AITeamMemberLink>, teamMembers, NDTRefArray, AutoArray<NetworkId>, NCTNone, DEFVALUEREFARRAY, DOC_MSG("Members of the team"), TRANSF_REFS, ET_NOT_CONTAIN_COUNT, ERR_COEF_STRUCTURE) \
  XX(MessageName, AITeamMemberLink, teamLeader, NDTRef, NetworkId, NCTNone, DEFVALUENULL, DOC_MSG("Team leader"), TRANSF_REF, ET_NOT_EQUAL, ERR_COEF_MODE) \
  XX(MessageName, RString, formation, NDTString, RString, NCTNone, DEFVALUE(RString, ""), DOC_MSG("Current formation"), TRANSF, ET_NOT_EQUAL, AIUpdateErrorCoef * ERR_COEF_MODE) \

DECLARE_NET_INDICES_EX_ERR(UpdateAITeam, NetworkObject, UPDATE_AI_TEAM_MSG)

/// Team - implementation of node in AI hierarchy containing other nodes
class AITeam : public NetworkObject, public AITeamMember, public CountInstances<AITeam>
{
protected:
  /// List of contained members
  FindArrayKey<AITeamMemberLink> _teamMembers;
  /// Leader (speaker) of the team
  AITeamMemberLink _teamLeader;
  
  RString _typeName;
  RString _name;
  RString _formation;

  int _id; // unique identifier used for serialization (of references)

  /// Unique identification over network game
  NetworkId _networkId;
  /// True when the object is local on this network client
  bool _local;

public:
  AITeam() {_id = -1; _local = true;} // used for serialization
  AITeam(RString typeName, RString name) {_typeName = typeName; _name = name; _id = -1; _local = true;}

  RString GetTypeName() const {return _typeName;}
  RString GetName() const {return _name;}

  AITeamMember *GetLeader() {return _teamLeader;}
  void SetLeader(AITeamMember *leader) {_teamLeader = leader;}

  RString GetFormation() const {return _formation;}
  void SetFormation(RString formation) {_formation = formation;} 

  void SetId(int id) {_id = id;}

  //@{ Reference counting redirection
  virtual int IAddRef() {return AddRef();}
  virtual int IRelease() {return Release();}
  //@}

  //@{ Soft link support redirection
  virtual TrackSoftLinks<IdTraits> *ITrack() {return Track();}
  virtual RemoveSoftLinks<IdTraits> *GetRemoveLinks() {return this;}
  virtual AITeamMember::LinkId GetLinkId() const {return NetworkObject::GetLinkId();}
  //@}

  // AITeamMember implementation
  virtual AITeam *GetTeam() {return this;}
  virtual void SimulateAI();
  virtual void ApplyContext(GameState &state);
  virtual LSError Serialize(ParamArchive &ar);
  virtual LSError SaveTeamMemberRef(ParamArchive &ar);

  static AITeamMember *LoadRef(ParamArchive &ar);

  // NetworkObject implementation
  DECLARE_NETWORK_OBJECT
  virtual void SetNetworkId(NetworkId &id) {_networkId = id;}
  virtual NetworkId GetNetworkId() const {return _networkId;}
  virtual Vector3 GetCurrentPosition() const {return VZero;}
  virtual bool IsLocal() const {return _local;}
  virtual void SetLocal(bool local = true) {_local = local;}
  static NetworkMessageFormat &CreateFormat(
    NetworkMessageClass cls,
    NetworkMessageFormat &format);
  static AITeam *CreateObject(NetworkMessageContext &ctx);
  virtual void DestroyObject();
  virtual TMError TransferMsg(NetworkMessageContext &ctx);
  virtual float CalculateError(NetworkMessageContextWithError &ctx);
  virtual RString GetDebugName() const {return _name;}
  virtual AITeamMember *GetTeamMember() {return this;}

  /// Add a new member
  int AddTeamMember(AITeamMember *teamMember) {return _teamMembers.AddUnique(teamMember);}
  /// Remove a member
  bool RemoveTeamMember(AITeamMember *teamMember) {return _teamMembers.DeleteKey(teamMember);}

  /// Return number of members
  int NTeamMembers() const {return _teamMembers.Size();}
  /// Return member with given index
  const AITeamMember *GetTeamMember(int i) const {return _teamMembers[i];}
  /// Return member with given index
  AITeamMember *GetTeamMember(int i) {return _teamMembers[i];}
};

#endif

// FSM functions factories

#define AIUNIT_FSM_CONDITIONS(XX) \
  XX(behaviourCombat, Man, FSMBehaviourCombat) \
  XX(vehicleAir, Man, FSMVehicleAir) \
  XX(vehicle, Man, FSMVehicle) \
  XX(formationIsLeader, Man, FSMFormationIsLeader) \
  XX(reloadNeeded, Man, FSMReloadNeeded) \
  XX(coverReached, Man, FSMCoverReached) \
  XX(randomDelay, Man, FSMRandomDelay) \
  XX(formationCanLeaveCover, Man, FSMFormationCanLeaveCover) \

#define AIUNIT_FSM_ACTIONS(XX) \
  XX(formationExcluded, Man, FSMFormationExcluded) \
  XX(formationInit, Man, FSMFormationInit) \
  XX(searchPath, Man, FSMSearchPath) \
  XX(formationProvideCover, Man, FSMFormationProvideCover) \
  XX(formationHideInCover, Man, FSMFormationHideInCover) \
  XX(formationLeader, Man, FSMFormationLeader) \
  XX(formationNextTarget, Man, FSMFormationNextTarget) \
  XX(setUnitPosToDown, Man, FSMSetUnitPosToDown) \
  XX(reload, Man, FSMReload) \
  XX(formationCleanUp, Man, FSMFormationCleanUp) \

/// Redirect FSM function from Man to AIUnit

#define REDIRECT_AIUNIT_FSM_CONDITION(fsmName, type, function) \
  float function(const FSMEntity *fsm, const float *params, int paramsCount) const \
  { \
    AIUnit *unit = _brain ? _brain->GetUnit() : NULL; \
    return unit ? unit->function(fsm,params, paramsCount) : 0; \
  }

#define REDIRECT_AIUNIT_FSM_ACTION(fsmName, type, function) \
  void function##In(FSMEntity *fsm, const float *params, int paramsCount) \
  { \
    AIUnit *unit = _brain ? _brain->GetUnit() : NULL; \
    if (unit) unit->function##In(fsm, params, paramsCount); \
  } \
  void function##Out(FSMEntity *fsm, const float *params, int paramsCount) \
  { \
    AIUnit *unit = _brain ? _brain->GetUnit() : NULL; \
    if (unit) unit->function##Out(fsm, params, paramsCount); \
  }

///////////////////////////////////////////////////////////////////////////////
class AIHCGroup
{
protected:
  //group name shown in command bar
  RString _name;
  //team color
  int _team;
  //true if player has this unit selected
  bool _selected;
  //id - describes page and F-key
  int _id;
  //group with units
  OLinkPermNO(AIGroup) _group;

public:


public:
  AIHCGroup()
  {
    _id = 0;
    _name = "";
    _team = 0;
    _selected = false;
  };

  RString Name(){return _name;};
  int Team(){return _team;};

  bool IsSelected(){return _selected;};
  bool IsInTeam(AIHCGroup hcGroup) 
  {
    int team = hcGroup.Team();
    return (team == _team);
  };
  int ID(){return _id;};

  void SetName(RString name){ _name =name;};
  void SetTeam(int team){_team = team;};
  void SetSelected(bool selected){ _selected = selected;};
  void SetID(int id){_id = id;};
  AIGroup *GetGroup() {return _group;}
  void SetGroup(AIGroup *group) {_group = group;} 

  LSError Serialize(ParamArchive &ar)
  {
   CHECK(ar.Serialize("groupName", _name, 1, ""))
   CHECK(ar.Serialize("team", _team, 1, 0))
   CHECK(ar.Serialize("selected", _selected, 1, false))
   CHECK(ar.Serialize("id", _id, 1, 0))
   CHECK(ar.SerializeRef("group", _group, 1))

    return LSOK;
  }

};
TypeContainsOLink(AIHCGroup)

//! AI structure representing brain of single unit

class AIUnit : public AIBrain
{
friend class AISubgroup;
friend class AIGroup;
friend class AICenter;
friend class AIHCGroup;
  typedef AIBrain base;
public:
  enum ResourceState
  {
    RSNormal,
    RSLow,
    RSCritical
  };

  enum ResourceType
  {
    RTNone,
    RTHealth,
    RTDamage,
    RTAmmo,
    RTFuel
  };

protected:
  // structure
  OLinkPermNO(AISubgroup) _subgroup;

  // contains high command groups
  AutoArray<AIHCGroup> _hcGroups;

  // info
  int _id;          // id in group
  Vector3 _formOffset;
  
  mutable OLinkPermNO(AIUnit) _ledBy;

//   /// formation style: when is dynamic (immediate) used?
//   /** is _formationPos or AISubgroup::GetFormationPosImmediate used? */
//   FormationStyle _formStyle;
  // random from 0 to 1 - when to perform expensive think
  int _expensiveThinkFrac; // frac in ms
  Time _expensiveThinkTime;

  void ExpensiveThinkDone(); // calculate time of next expensive think

  LinkTarget _targetEngage; // enabled engaging this target

  // used when vehicle is static and no target is locked

  float _formationAngle;
  Vector3 _formationPos; // position in formation (relative to leader)

  bool _isAway;
  bool _gearActionProcessed;

  // messages
  ResourceState _lastFuelState;
  ResourceState _lastHealthState;
  ResourceState _lastArmorState;
  ResourceState _lastAmmoState;

  Time _awayTime;
  Time _fuelCriticalTime;
  Time _healthCriticalTime;
  Time _dammageCriticalTime;
  Time _ammoCriticalTime;

#if _ENABLE_CHEATS
  mutable float _fsmDiagSize;
  mutable PackedColor _fsmDiagColor;
#endif

public:
  AIUnit( Person *vehicle );
  ~AIUnit();

  virtual RString GetAgentType() const {return "Unit";}
  virtual LSError Serialize(ParamArchive &ar);
  // used in old saves
  static AIUnit *CreateObject(ParamArchive &ar) {return new AIUnit(NULL);}

  DECLARE_NETWORK_OBJECT
  static NetworkMessageFormat &CreateFormat
  (
    NetworkMessageClass cls,
    NetworkMessageFormat &format
  );
  static AIUnit *CreateObject(NetworkMessageContext &ctx);
  void DestroyObject();
  TMError TransferMsg(NetworkMessageContext &ctx);
  float CalculateError(NetworkMessageContextWithError &ctx);

#if _ENABLE_DIRECT_MESSAGES
  bool UseRadio(AIUnit *receiver) const;
  bool UseRadio(AIGroup *group, const OLinkPermNOArray(AIUnit) &list) const;
#endif

  // access to data members
  virtual TargetSide GetSide() const;
  virtual bool IsEnemy(TargetSide side) const;
  virtual bool IsNeutral( TargetSide side) const;
  virtual bool IsFriendly(TargetSide side) const;

  virtual void OnNewTarget(TargetNormal *target, Time knownSince);

  bool IsSubgroupLeader() const;
  bool IsSubgroupLeaderVehicle() const;

  virtual bool IsFormationLeaderVehicle() const {return IsSubgroupLeaderVehicle();}
  virtual AIBrain *GetFormationLeader();
  virtual void GetHideFrom(Vector3 &hideL, Vector3 &hideR) const {hideL = _fsmHideFromL; hideR = _fsmHideFromR;}
  virtual AIUnit* GetUnit() {return this;}
  AISubgroup* GetSubgroup() const;
  virtual AIGroup* GetGroup() const;

  virtual const SideInfo *GetSideInfo() const;

  virtual CombatModeRange GetCombatModeBasedOnCommand() const;
  virtual CautionRange GetCautionBasedOnCommand() const;
  virtual bool GetCoveringBasedOnCommand() const;

  /// check if covering is enabled by current AI state
  bool CanUseCover() const;
  /// check if covering is required by current AI state
  bool MustUseCover() const;

  bool IsAway() const {return _isAway;}
  void SetAway( bool val=true ) {/*_isAway=val;*/}

  void SetAwayTime(Time awayTime){_awayTime=awayTime;}

  bool IsGearActionProcessed() const {return _gearActionProcessed;}
  void SetGearActionProcessed(bool set ) {_gearActionProcessed = set;}

  bool IsPathValid() const;

  Target *GetEngageTarget() const {return _targetEngage;}
  void EngageTarget(Target *target) {_targetEngage = target;}

  void ExposureChanged(int x, int z, float optimistic);

  bool IsPlayerDriven() const;
  int ID() const {return _id;}

  float GetRandomizedExperience() const;

  /// we want to avoid some decision being synchronized - each brain has its own seed
  virtual int RandomSeed() const;

  virtual float GetMorale() const;

  virtual bool IsInCargo() const;
  virtual void SetInCargo(bool set);

  virtual void SetSearchTime(Time time);
  
  virtual void OnRespawnFinished();

  bool IsEngageEnabled(Target *tgt) const; // check if fire enabled at given target

  void AddFormationPos(FormationPos status);
  Vector3 GetFormationPosition() {return _formationPos;}

//   FormationStyle GetFormationStyle() const {return _formStyle;}
//   void SetFormationStyle(FormationStyle val) {_formStyle = val;}
  
  void ReportStatus(bool position=true);
  float GetInvAverageSpeed() const;
  float GetAverageSpeed() const;

  
  /// check who is ruling our movement in the formation
  AIUnit *LedBy();

  /// absolute, do not adjust Y based on terrain data
  Vector3 GetFormationAbsoluteFreeY(float estT=0, bool init=false) const;
  /// relative to the group leader
  Vector3 GetFormationRelative(bool init=false) const;
  /// absolute formation position
  Vector3 GetFormationAbsolute(float estT=0, bool init=false) const; 
  /// check how much is given unit behind its expected formation position
  float GetBehindFormation() const;

  float GetCautiousInCombat() const;
  
  float GetFormationAngleRelative() const {return _formationAngle;}
  
  ResourceState GetFuelState() const;
  ResourceState GetHealthState() const;
  ResourceState GetArmorState() const;
  ResourceState GetAmmoState() const;

  const AITargetInfo *CheckAmmo(ResourceState state);
  void CheckAmmo();

  static bool FindFreePosition
  (
    Vector3 &pos, Vector3 &normal, bool soldier, const EntityAIType *vehType,
    FindFreePositionCallback *isFree=DefFindFreePositionCallback,
    void *context=NULL
  );
  bool FindFreePosition( Vector3 &pos, Vector3 &normal );
  bool FindFreePosition();
  
  float GetFeeling() const;

  Vector3 GetFormationDirection() const;

  // changes of structure
  void RemoveFromSubgroup();
  void RemoveFromGroup();
  void ForceRemoveFromGroup();
  void CheckIfAliveInTransport();
  void IssueGetOut();
  
  // mind
  bool Think(ThinkImportance prec); // if true, OperPath was called

  void ProcessOneTalkEvent();
  
  // communication with group
  void SendAnswer(Answer answer);
  void RefreshMission();

  RString GetDebugName() const;
  bool AssertValid() const;
  void Dump(int indent = 3) const;

  virtual TargetList *AccessTargetList();
  virtual const TargetList *AccessTargetList() const;

  virtual bool ForEachTarget(const TargetFunctor &func) const;
  
  virtual Target *GetFireSectorTarget() const;

  void ComputeHideFrom(Vector3 &hideFromL, Vector3 &hideFromR);
  
  /// check formation neighbours state and provide suppressive fire to them as needed
  void ProvideSuppressiveFireAsNeeded();

  // FSM functions
  AIUNIT_FSM_CONDITIONS(DECLARE_ENTITY_FSM_CONDITION)
  AIUNIT_FSM_ACTIONS(DECLARE_ENTITY_FSM_ACTION)

  //! add group under unit's command 
  void AddHCGroup(AIHCGroup group);
  //! remove group from unit's command 
  void RemoveHCGroup(AIGroup *group);
  //! remove all groups from unit's command
  void ClearHCGroups();
  //! redefines group parameters
  void EditHCGroup(int index, AIHCGroup *group);
  //! returns selected groups from unit's command 
  void SelectedHCGroups(RefArray<AIGroup> &selection);
  //! select unit from unit's command
  void SelectHCGroup(int index, bool select = true) {if(index<_hcGroups.Size()) _hcGroups[index].SetSelected(select);};
  //! returns HCGroup from unit's command in HC 
  AIHCGroup *GetHCGroup(AIGroup *group);
  //! returns all groups in HC 
  AutoArray<AIHCGroup> &GetHCGroups(){return _hcGroups;};
  //! unselect all groups
  void UnselectAllHCGroups(){for(int i=0; i<_hcGroups.Size(); i++) SelectHCGroup(i,false);};

#if _ENABLE_CHEATS
  //@{ one diagnostic sphere per FSM
  float GetFSMDiagSize() const {return _fsmDiagSize;}
  PackedColor GetFSMDiagColor() const {return _fsmDiagColor;}
  //@}
  /// allow flexible FSM diag rendering
  virtual void DrawDiags();
#endif

  /// previous unit (we are coordinating covering with it), looped - gives tail for the first unit
  AIUnit *FSMGetFormationNext() const;
  /// next unit (we are coordinating covering with it), looped - gives first one for the last unit
  AIUnit *FSMGetFormationPrev() const;


  struct ForEachFormationCallback
  {
    virtual void Do(const AIUnit *me, const AIUnit *next) = 0;
  };
  void ForEachFormationNext(ForEachFormationCallback &callback) const;
  void ForEachFormationPrev(ForEachFormationCallback &callback) const;
  void ForEachFormationNeighbour(ForEachFormationCallback &callback) const;

protected:
  // FSM functions implementation helpers
  AIUnit *FSMGetFormationNeighbour() const;

  //@{ angular interval - the direction we are "hiding from", because we know or expect enemies there  
  Vector3 _fsmHideFromL;
  Vector3 _fsmHideFromR;
  //@}
  
  Time _fsmTimeStart;
  Time _fsmTimeNow;
  float _fsmTimeMin;
  float _fsmTimeMax;
  float _fsmDelay;
  LinkTarget _fsmDummyTarget;
  Vector3 _fsmTargets[5];

public:
  INHERIT_SOFTLINK(AIUnit,AI);

protected:
  // implementation
  virtual void WatchFormation();

  // think subtasks
  void AutoAttack();
  void CheckResources();
  
  /// handle getting out
  void CheckGetOut();
  /// handle getting in/out
  void CheckGetInGetOut();
  // check if calling CheckGetInGetOut has sense
  bool IsCheckGetInGetOutNeeded() const;

private: // disable copy
  void operator = ( const AIUnit &src );


};

///////////////////////////////////////////////////////////////////////////////
// class AISubgroup

struct FormationPositionInfo
{
  int base;
  Vector3 position;
  float angle;
  bool allowCovering;

  FormationPositionInfo() {};
  FormationPositionInfo(int b, float x, float z, float a, bool covering=true)
  {
    base = b;
    position.Init();
    position[0] = x; position[1] = 0; position[2] = z;
    angle = a;
    allowCovering = covering;
  }
};
TypeIsMovable(FormationPositionInfo);

struct FormationInfo
{
  AutoArray<FormationPositionInfo> _fixed;
  AutoArray<FormationPositionInfo> _pattern;

  FormationInfo (){};
  FormationInfo(const AutoArray<FormationPositionInfo> &fixed, const AutoArray<FormationPositionInfo> &pattern) 
  {
    _fixed = fixed;
    _pattern = pattern;
  };
  const FormationPositionInfo &GetInfo(int slot, int &parent) const;
};

//extern const FormationInfo formations[AI::NForms];

#define CREATE_COMMAND_MSG(MessageName, XX) \
  XX(MessageName, OLinkPermNO(AISubgroup), subgroup, NDTRef, NetworkId,  NCTNone, DEFVALUENULL, DOC_MSG("Subgroup, executing this command"), TRANSF_REF) \
  XX(MessageName, int, enqueued, NDTBool, bool, NCTNone, DEFVALUE(bool, false), DOC_MSG("Command is enqueued in stack of commands"), TRANSF) \
  XX(MessageName, int, message, NDTInteger, int, NCTSmallUnsigned, DEFVALUE(int, NoCommand), DOC_MSG("Type of command"), TRANSF) \
  XX(MessageName, TargetId, target, NDTRef, NetworkId,  NCTNone, DEFVALUENULL, DOC_MSG("Target (exact) of command - used for well known targets"), TRANSF_REF) \
  XX(MessageName, LinkTarget, targetE, NDTRef, NetworkId,  NCTNone, DEFVALUENULL, DOC_MSG("Target (inexact) - used for enemies"), TRANSF_REF) \
  XX(MessageName, Vector3, destination, NDTVector, Vector3, NCTNone, DEFVALUE(Vector3, VZero), DOC_MSG("Destination position"), TRANSF) \
  XX(MessageName, Time, time, NDTTime, Time, NCTNone, DEFVALUE(Time, Time(0)), DOC_MSG("Time, when command timeouts"), TRANSF) \
  XX(MessageName, OLinkPermNO(AISubgroup), join, NDTRef, NetworkId,  NCTNone, DEFVALUENULL, DOC_MSG("Join to main subroup after completition"), TRANSF_REF) \
  XX(MessageName, int, intParam, NDTInteger, int, NCTSmallSigned, DEFVALUE(int, -1), DOC_MSG("General parameter"), TRANSF) \
  XX(MessageName, RString, strParam, NDTString, RString, NCTNone, DEFVALUE(RString, RString("")), DOC_MSG("General parameter"), TRANSF) \
  XX(MessageName, int, discretion, NDTInteger, int, NCTSmallUnsigned, DEFVALUE(int, Undefined), DOC_MSG("Subgroup discretion"), TRANSF) \
  XX(MessageName, int, context, NDTInteger, int, NCTSmallUnsigned, DEFVALUE(int, CtxUndefined), DOC_MSG("How command was ordered"), TRANSF) \
  XX(MessageName, float, precision, NDTFloat, float, NCTNone, DEFVALUE(float, 0), DOC_MSG("Command required precision"), TRANSF) \
  XX(MessageName, int, id, NDTInteger, int, NCTSmallSigned, DEFVALUE(int, -1), DOC_MSG("Unique (in group) id of command"), TRANSF)

// TODO:
/*
XX(MessageName, int, action, NDTInteger, int, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG("Type of action for Action command"), TRANSF) \
XX(MessageName, int, param, NDTInteger, int, NCTSmallSigned, DEFVALUE(int, 0), DOC_MSG("Action parameter"), TRANSF) \
XX(MessageName, int, param2, NDTInteger, int, NCTSmallSigned, DEFVALUE(int, 0), DOC_MSG("Action parameter"), TRANSF) \
XX(MessageName, RString, param3, NDTString, RString, NCTNone, DEFVALUE(RString, RString("")), DOC_MSG("Action parameter"), TRANSF) \
*/

DECLARE_NET_INDICES_EX(CreateCommand, NetworkObject, CREATE_COMMAND_MSG)

class Command : public NetworkObject, public SerializeClass
{
protected:
  NetworkId _networkId;
  bool _local;  // local / remote in network game

public:
  enum Message
  {
    NoCommand,
    Wait,
    Attack,
    Hide,
    Move,
    // supplied units
    Heal,
    Repair,
    Refuel,
    Rearm,
    // supplying units
    Support,
    Join,
    GetIn,
    Fire,
    GetOut,
    Stop,
    Expect,
    Action,
    Scripted,
    Dismiss,
    HealSoldier,
    FirstAid,
    AttackAndFire,
    TakeBag,
    Assemble,
    DisAssemble,
    DropBag,
    OpenBag,
    IRLaserOn,
    IRLaserOff,
    GunLightOn,
    GunLightOff,
    FireAtPosition,
    RepairVehicle
  };
  enum Discretion
  {
    /// no change
    Undefined = 0,
    /// enforce safe behaviour
    Major,
    /// strictly enforce normal (aware) behaviour, but disallow any hiding
    NormalNoHide,
    /// enforce normal (aware) behaviour
    Normal,
    /// enforce combat behaviour
    Minor,
    /// enforce combat behaviour, stay hidden whenever possible
    MinorHidden
  };
  enum Movement
  {
    /// no change
    MoveUndefined = 0,
    /// no covering, move fast
    MoveFast,
    /// move fast, cover at the end
    MoveFastIntoCover,
    /// somewhat cautious
    MoveCautios,
    /// very cautions (under heave fire) - note: same as MoveCautios, only some movement coefficients vary
    MoveInDanger,
  };
  enum Context
  {
    CtxUndefined,
    CtxAuto,
    CtxAutoSilent,
    CtxJoin,
    CtxAutoJoin,
    CtxEscape,
    CtxMission,
    CtxUI,
    CtxUIWithJoin
  };
  // exact values
  Message _message;
  // _target is exact vehicle information
  // it should be used for friendly/well known targets (static ets.)
  TargetId _target; // TODO: save both _target and _targetID
  // _targetE should be used for enemy targets (esp. for attack)
  LinkTarget _targetE;
  Vector3 _destination;
  /// formation direction while executing this command
  Vector3 _direction;
  
  Time _time;
  OLinkPermNO(AISubgroup) _joinToSubgroup;
  Ref< ::Action > _action;
  int _intParam; // int parameter - house position, weapon index
  RString _strParam; // RString parameter - weapon name, fsm name
  Discretion _discretion;
  Movement _movement;
  Context _context;
  /// precision with which the command movement is expected to be executed
  /** default 0 means as precisely as the unit is able to move */
  float _precision;

  /// command identification
  int _id;
public: 
  Command()
  {
    _local = true;

    _message = NoCommand;
    _destination=VZero;
    _time = TIME_MIN;
    _intParam = -1;
    _discretion = Undefined;
    _movement = MoveUndefined;
    _context = CtxUndefined;
    _id = -1;
    _precision = 0;
    _direction = VZero;
  }
  //@{ functions expected by AbstractAIMachine
  int GetType() const {return _message;}

  LSError Serialize(ParamArchive &ar);
  static Command *CreateObject(ParamArchive &ar) {return new Command();}

  NetworkId GetNetworkId() const {return _networkId;}
  void SetNetworkId(NetworkId &id) {_networkId = id;}
  bool IsLocal() const {return _local;}
  void SetLocal(bool local = true) {_local = local;}

  DECLARE_NETWORK_OBJECT
  static NetworkMessageFormat &CreateFormat(
    NetworkMessageClass cls,
    NetworkMessageFormat &format
  );
  TMError TransferMsg(NetworkMessageContext &ctx);
  float CalculateError(NetworkMessageContextWithError &ctx);
  static Command *CreateObject(NetworkMessageContext &ctx);
  void DestroyObject();
  Vector3 GetCurrentPosition() const;
  RString GetDebugName() const;
  //@}
};

template<>
const EnumName *GetEnumNames(Command::Message dummy);
template<>
const EnumName *GetEnumNames(Command::Discretion dummy);
template<>
const EnumName *GetEnumNames(Command::Context dummy);

enum CommandState
{
  CSSent,
  CSReceived,
  CSSucceed,
  CSFailed
};

void SetCommandState(int id, CommandState state, AISubgroup *subgrp);

struct AISubgroupContext
{
  Command *_task;   // expected parameter, supply by Abstract Machine
  AISubgroup *_subgroup;
  FSM *_fsm;

  AISubgroupContext()
    {_task = NULL; _subgroup = NULL; _fsm = NULL;}
  AISubgroupContext(AISubgroup *subgroup)
    {_task = NULL; _subgroup = subgroup; _fsm = NULL;}
};

// export global fsm functions

void CommandSucceed(AISubgroupContext *context);
void CommandFailed(AISubgroupContext *context);
void CheckCommandSucceed(AISubgroupContext *context);
void CheckCommandFailed(AISubgroupContext *context);

void CreateUnitsList(OLinkPermNOArray(AIUnit) &list, char *buffer, int bufferSize);

#include "pathPlanner.hpp"

#define CREATE_AI_SUBGROUP_MSG(MessageName, XX) \
  XX(MessageName, OLinkPermNO(AIGroup), group, NDTRef, NetworkId,  NCTNone, DEFVALUENULL, DOC_MSG("Superior group"), TRANSF_REF)

DECLARE_NET_INDICES_EX(CreateAISubgroup, NetworkObject, CREATE_AI_SUBGROUP_MSG)

#define UPDATE_AI_SUBGROUP_MSG(MessageName, XX) \
  XX(MessageName, OLinkPermNO(AIGroup), group, NDTRef, NetworkId,  NCTNone, DEFVALUENULL, DOC_MSG("Superior group"), TRANSF_REF, ET_NONE, 0) \
  XX(MessageName, RefArray<AIUnit>, units, NDTRefArray, AutoArray<NetworkId>, NCTNone, DEFVALUEREFARRAY, DOC_MSG("Member units"), TRANSF_REFS, ET_NOT_CONTAIN_COUNT, ERR_COEF_STRUCTURE) \
  XX(MessageName, OLinkPermNO(AIUnit), leader, NDTRef, NetworkId,  NCTNone, DEFVALUENULL, DOC_MSG("Leader unit"), TRANSF_REF, ET_NOT_EQUAL, ERR_COEF_STRUCTURE) \
  XX(MessageName, int, mode, NDTInteger, int, NCTSmallUnsigned, DEFVALUE(int, PlanAndGo), DOC_MSG("Planning mode"), TRANSF, ET_NOT_EQUAL, ERR_COEF_MODE) \
  XX(MessageName, Vector3, wantedPosition, NDTVector, Vector3, NCTNone, DEFVALUE(Vector3, VUndefined), DOC_MSG("Destination"), TRANSF, ET_NOT_EQUAL, ERR_COEF_MODE) \
  XX(MessageName, int, formation, NDTInteger, int, NCTSmallUnsigned, DEFVALUE(int, FormVee), DOC_MSG("Formation type"), TRANSF, ET_NOT_EQUAL, ERR_COEF_MODE) \
  XX(MessageName, int, speedMode, NDTInteger, int, NCTSmallUnsigned, DEFVALUE(int, SpeedNormal), DOC_MSG("Speed mode"), TRANSF, ET_NOT_EQUAL, ERR_COEF_MODE) \
  XX(MessageName, int, lastPrec, NDTInteger, int, NCTSmallUnsigned, DEFVALUE(int, LevelOperative), DOC_MSG("Planning precision"), TRANSF, ET_NONE, 0) \
  XX(MessageName, float, formationCoef, NDTFloat, float, NCTNone, DEFVALUE(float, 1.0), DOC_MSG("Formation spacing"), TRANSF, ET_NONE, 0) \
  XX(MessageName, Vector3, direction, NDTVector, Vector3, NCTNone, DEFVALUE(Vector3, VForward), DOC_MSG("Formation direction"), TRANSF, ET_NONE, 0)

DECLARE_NET_INDICES_EX_ERR(UpdateAISubgroup, NetworkObject, UPDATE_AI_SUBGROUP_MSG)

class AISubgroup
  : public AI, public AbstractAIMachine<Command, AISubgroupContext>
{
friend class AIGroup;
typedef AbstractAIMachine<Command, AISubgroupContext> base;

public:
  enum Mode
  {
    Wait,
    PlanAndGo,
    DirectGo
  };

protected:
  // list of units
  RefArray<AIUnit> _units;
  /// subgroup leader
  OLinkPermNO(AIUnit) _whoAmI;
  OLinkPermNO(AIGroup) _group;

  Mode _mode;
  Time _refreshTime;
  PlanPosition _wantedPosition;

  // operative control
  Formation _formation;

  SpeedMode _speedMode;

  // flags
  ThinkImportance _lastPrec;
  
  bool _avoidRefresh;
  bool _doRefresh;

  float _formationCoef;
  Time _formationCoefChanged;

  /// direction the formation is facing
  Vector3 _direction;
  // direction the formation is moving to
  /** for AI _directionMove and _direction are the same */
  Vector3 _directionMove;
  
  Time _directionChanged;

public:
  AISubgroup();
  ~AISubgroup();

  LSError Serialize(ParamArchive &ar);
  static AISubgroup *CreateObject(ParamArchive &ar) {return new AISubgroup();}
  static AISubgroup *LoadRef(ParamArchive &ar);
  LSError SaveRef(ParamArchive &ar) const;
  // Commands with NetworkId, created after load from save, should be NetworkClient::CreateCommand "created"
  void CreateCommandsFromLoad();

  DECLARE_NETWORK_OBJECT
  static NetworkMessageFormat &CreateFormat
  (
    NetworkMessageClass cls,
    NetworkMessageFormat &format
  );
  static AISubgroup *CreateObject(NetworkMessageContext &ctx);
  void DestroyObject();
  TMError TransferMsg(NetworkMessageContext &ctx);
  float CalculateError(NetworkMessageContextWithError &ctx);
  Vector3 GetCurrentPosition() const {return Leader() ? Leader()->Position(Leader()->GetFutureVisualState()) : VZero;}

  void OnTaskCreated(Command &cmd, bool enqueued);
  void OnTaskDeleted(Command &cmd);
  void InsertCommand(Command *cmd, bool enqueued);
  void DeleteCommand(NetworkId id);

  // access to data members
  AIUnit* Leader() const {return _whoAmI;} 
  AIUnit *Commander() const;

  AIGroup* GetGroup() const;
  int NUnits() const {return _units.Size();}
  AIUnit *GetUnit( int i ) const {return _units[i];} // caution - can be NULL
  bool HasAI()
    {return Leader() && Leader()->HasAI();}
  bool IsPlayerSubgroup()
    {return Leader() && Leader()->IsPlayer();}
  bool IsAnyPlayerSubgroup()
    {return Leader() && Leader()->IsAnyPlayer();}
  bool IsPlayerDrivenSubgroup()
    {return Leader() && Leader()->IsPlayerDriven();}
  bool HasCommand() {return GetCurrent() != NULL;}
  const Command *GetCommand() const
    {return (GetCurrent() != NULL) ? GetCurrent()->_task : NULL;}
  Command *GetCommand()
    {return (GetCurrent() != NULL) ? GetCurrent()->_task : NULL;}
  Formation GetFormation() const {return _formation;}
  void SetFormation(Formation f);
  float GetCommandPrecision() const
  {
    const Item *item = GetCurrent();
    if (!item) return 0;
    if (!item->_task) return 0;
    return item->_task->_precision;
  }
  
  CombatModeRange GetCombatModeBasedOnCommand() const;
  CautionRange GetCautionBasedOnCommand() const;
  bool GetCoveringBasedOnCommand() const;

  bool IsAvoidingRefresh() const {return _avoidRefresh;}
  void AvoidRefresh(bool set = true) {_avoidRefresh = set;} 

  Mode GetMode() const {return _mode;}
  Vector3Val GetFormationDirection() const {return _direction;}
  void SetFormationDirection(Vector3Par dir) {_direction = dir;}
  Vector3Val GetMoveDirection() const {return _directionMove;}
  void SetMoveDirection(Vector3Par dir) {_directionMove = dir;}
  AIUnit *GetFormationPrevious( AIUnit *unit ) const;
  AIUnit *GetFormationNext( AIUnit *unit ) const;

  SpeedMode GetSpeedMode() const {return _speedMode;}
  void SetSpeedMode(SpeedMode speed) {_speedMode = speed;}

  // changes of structure
  void UnitRemoved(AIUnit *unit);
  void UnitReplaced(AIUnit *unitOld, AIUnit* unitNew);
  void AddUnit(AIUnit *unit);
  void AddUnitWithCargo(AIUnit *unit);
  void SetLeader(AIUnit *unit = NULL);
  void SelectLeader(AIUnit *unit = NULL);
  void RemoveFromGroup();

  void JoinToSubgroup(AISubgroup *subgrp); // remove all units and then remove from group

  int GetFormationParent(const AIUnit *unit) const;
  
  void UpdateFormationPos();
  void UpdateFormationCoef(bool init=false);
  void UpdateFormationDirection();
  
  /// immediate formation position, based relative formation pattern
  Vector3 GetFormationPosImmediate(const AIUnit *formUnit, AIUnit *&ledBy) const;
  /// helper for GetFormationPosImmediate
  Vector3 GetFormationPosImmediate(int slot, AIUnit *&ledBy) const;


  bool GetCoveringEnabled(int slot) const;

  float GetFormationCoef() const {return _formationCoef;}

  void SetDirection(Vector3Val dir);

  // mind
  ThinkImportance CalculateImportance();
  bool Think(ThinkImportance prec); // if true, OperPath was called

  // communication with units
  void ReceiveAnswer(AIUnit* from, Answer answer);

  void SetWaypoints( int animationIndex, int vehicleIndex, bool direct=true );

  // communication with group
  void ClearAllCommands();
  void ClearMissionCommands();
  void ClearEscapeCommands();
  /// clear all attack / hide commands
  void ClearAttackCommands();
  /// clear all hide commands and when allowAttackTarget is given also all attacks to different target
  void ClearAttackCommands(Target *allowAttackTarget);
  void ClearGetInCommands();
  bool CheckHide() const;
  void ReceiveCommand(Command &cmd);
  void SendAnswer(Answer answer);
  void OnEnemyDetected(const EntityAIType *type, Vector3Val pos);

  // void DoRefresh();
  void FailCommand();

  void GetUnitsList(OLinkPermNOArray(AIUnit) &list);
  void GetUnitsListNoCargo(OLinkPermNOArray(AIUnit) &list);
  bool FindNearestSafe(int &x, int &z, float threshold);
  bool FindNearestSafer(int &x, int &z);
  void ExposureChanged(int x, int z, float optimistic);

  // interface for fsm
  void SetDiscretion(Command::Discretion discretion);
  void SetRefreshTime(Time time) {_refreshTime = time;}
  bool AllUnitsCompleted();
  void GoDirect(Vector3Val pos);
  void GoPlanned(Vector3Val pos, float precision=0, bool intoCover=false);
  void DoNotGo();
  void Stop();

  void ClearPlan();
  void RefreshPlan();

  // debug functions
  RString GetDebugName() const;
  bool AssertValid() const;
  void Dump(int indent = 2) const;

  int StackSize() const {return _stack.Size();}
  /// calculate cost per second for the whole group being there
  float CalculateExposure(int x, int z);

public:
  float GetFieldCost(int x, int z, bool road, bool includeGradient = true, float gradient = 0);

  float GetExposure(int x, int z);
  Threat GetThreat(int x, int z);

  void OnRemotePlayerAdded();

  INHERIT_SOFTLINK(AISubgroup,AI);

protected:
  // implementation
  // strategic planning
  bool IsSafe(int x, int z, float threshold);
  bool GetSafety(int x, int z, float &exposure);

private:
  // disable copy
  AISubgroup( const AISubgroup &src );
  void operator = ( const AISubgroup &src );
};

///////////////////////////////////////////////////////////////////////////////
// class Team

DEFINE_ENUM_BEG(Team)
  TeamMain,
  TeamRed,
  TeamGreen,
  TeamBlue,
  TeamYellow,
  NTeams
DEFINE_ENUM_END(Team)


///////////////////////////////////////////////////////////////////////////////
// class AIGroup

enum AIGroupType
{
  GTMilitary = 0,
  GTNone = GTMilitary,
  GTHeal,
  GTRepair,
  GTRefuel,
  GTRearm,
  GTShip,
};


enum ReportSubject
{
  ReportNew,
  ReportDestroy
};

enum MissionStatus
{
  MSNone,
  MSCompleted,
  MSFailed
};

class Mission : public RefCount, public SerializeClass
{
public:
  enum Action
  {
    NoMission,
    Arcade
  };

  Action _action;
  TargetId _target;
  Vector3 _destination;
  /// pass precision from waypoint to command
  float _precision;

public: 
  Mission()
  {
    _action = NoMission;
    _target = NULL;
    _destination=VZero;
    _precision=0;
  }
// functions excepted by AbstractAIMachine
  int GetType() const {return _action;}
  LSError Serialize(ParamArchive &ar);
  static Mission *CreateObject(ParamArchive &ar) {return new Mission();}
};

struct AIGroupContext
{
  Mission *_task;   // expected parameter, supply by Abstract Machine
  AIGroup *_group;
  FSM *_fsm;        // expected parameter, supply by Abstract Machine

  AIGroupContext()
    {_task = NULL; _group = NULL; _fsm = NULL;}
  AIGroupContext(AIGroup *group)
    {_task = NULL; _group = group; _fsm = NULL;}
};

typedef FSMTyped<AIGroupContext> AIGroupFSM;
AIGroupFSM *NewArcadeFSM();

void MissionSucceed(AIGroupContext *context);
void CheckMissionSucceed(AIGroupContext *context);
void CheckMissionFailed(AIGroupContext *context);
void MissionFailed(AIGroupContext *context);

class RadioMessageState;

enum OpenFireState
{
  OFSNeverFire,
  OFSHoldFire,
  OFSOpenFire
};

#define UPDATE_AI_GROUP_MSG(MessageName, XX) \
  XX(MessageName, OLinkPermNO(AISubgroup), mainSubgroup, NDTRef, NetworkId,  NCTNone, DEFVALUENULL, DOC_MSG("Main subgroup"), TRANSF_REF, ET_NOT_EQUAL, ERR_COEF_STRUCTURE) \
  XX(MessageName, OLinkPermNO(AIUnit), leader, NDTRef, NetworkId,  NCTNone, DEFVALUENULL, DOC_MSG("Leader unit"), TRANSF_REF, ET_NOT_EQUAL, ERR_COEF_STRUCTURE) \
  XX(MessageName, int, semaphore, NDTInteger, int, NCTSmallUnsigned, DEFVALUE(int, SemaphoreYellow), DOC_MSG("Default combat mode"), TRANSF, ET_NOT_EQUAL, ERR_COEF_MODE) \
  XX(MessageName, int, combatModeMinor, NDTInteger, int, NCTSmallUnsigned, DEFVALUE(int, CMSafe), DOC_MSG("Default behaviour"), TRANSF, ET_NOT_EQUAL, ERR_COEF_MODE) \
  XX(MessageName, int, enemiesDetected, NDTInteger, int, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG("Number of detected enemies"), TRANSF, ET_ABS_DIF, ERR_COEF_VALUE_MINOR) \
  XX(MessageName, int, unknownsDetected, NDTInteger, int, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG("Number of detected possible enemies"), TRANSF, ET_ABS_DIF, ERR_COEF_VALUE_MINOR) \
  XX(MessageName, float, forceCourage, NDTFloat, float, NCTFloatM1ToP1, DEFVALUE(float, -1), DOC_MSG("Enforced (by designer) courage"), TRANSF, ET_ABS_DIF, ERR_COEF_MODE) \
  XX(MessageName, float, courage, NDTFloat, float, NCTFloat0To1, DEFVALUE(float, 1), DOC_MSG("Calculated courage"), TRANSF, ET_ABS_DIF, ERR_COEF_MODE) \
  XX(MessageName, bool, flee, NDTBool, bool, NCTNone, DEFVALUE(bool, false), DOC_MSG("Units are fleeing"), TRANSF, ET_NOT_EQUAL, ERR_COEF_MODE) \
  XX(MessageName, int, waypointIndex, NDTInteger, int, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG("Index of currently processing waypoint"), TRANSF, ET_NOT_EQUAL, ERR_COEF_MODE) \
  XX(MessageName, bool, attackEnabled, NDTBool, bool, NCTNone, DEFVALUE(bool, true), DOC_MSG("Leader can issue attack commands"), TRANSF, ET_NOT_EQUAL, ERR_COEF_MODE)

DECLARE_NET_INDICES_EX_ERR(UpdateAIGroup, NetworkObject, UPDATE_AI_GROUP_MSG)

struct AIUnitSlot
{
  OLinkPerm<AIUnit> _unit;

  LinkTarget _assignTarget; // pending in radio
  Time _assignValidUntil;
  // remember state in the moment of assignment
  TargetState _assignTargetState;

  // remember status (as units reported it)
  AIUnit::ResourceState _healthState;
  AIUnit::ResourceState _ammoState;
  AIUnit::ResourceState _fuelState;
  AIUnit::ResourceState _dammageState;
  bool _reportedDown;
  Time _reportBeforeTime;

  AIUnitSlot() {Init();}

  void Init();
  void Copy(const AIUnitSlot &src);
};
TypeContainsOLink(AIUnitSlot)

#if _VBS2 // group eventhandlers
//! group event enum defined using enum factory
#define GROUP_EVENT_ENUM(type,prefix,XX) \
  XX(type, prefix, WaypointComplete) /*number:waypointIndex*/

#ifndef DECL_ENUM_GROUP_EVENT
#define DECL_ENUM_GROUP_EVENT
DECL_ENUM(GroupEvent)
#endif
DECLARE_ENUM(GroupEvent,GE,GROUP_EVENT_ENUM)
#endif


struct GIcon
{//describe single icon used to draw group marker in HC 

public: 
  Ref<Texture> _groupMaker; 
  //unique ID in single group icons
  int _ID;
  //position x is x + width * offsetX 
  float _offsetX;
  //position y is y + width * offsetY 
  float _offsetY;
  //icon size
  int _size;   
  //icon shadow
  int _shadow; 
  //texture config name
  RString _name;

  GIcon(){}
  GIcon(Ref<Texture> groupMaker, int ID, RString name, int size, float offsetX, float offsetY, int shadow)
  {
    _groupMaker = groupMaker; _ID = ID;
    _offsetX = offsetX; _offsetY = offsetY;
    _size = size;_shadow = shadow;  _name = name;
  }

  LSError Serialize(ParamArchive &ar);
}; 
TypeIsMovable(GIcon)

class GroupMarkers: public SerializeClass
{//
protected:
  //show group icons 
  bool _showIcon;
  //every icon needs unique ID (index is not enough, because of removing)
  int _IDcounter;
  // scales all icons used to draw final marker - used only in 3D world
  float _scale;
  // show waypoints
  bool _waypoints;

public:
  //list of texture used for final icon 
  AutoArray<GIcon> _groupMakers;
  //final icon color
  Color _markerColor,_waypointColor;
  //group description used in map and 3D 
  RString _text;

  GroupMarkers()
  {
    _markerColor = _waypointColor = Color(255,255,255,255);
    _showIcon = false; _IDcounter = 0;
    _scale = 1; _showIcon = true; _waypoints = false;
  }

  PackedColor IconColor(){return (PackedColor)_markerColor;};
  PackedColor WaypointColor(){return (PackedColor)_waypointColor;};

  bool IsVisible() {return _showIcon;};
  bool ShowWaypoints() {return _waypoints;};
  float Scale() {return _scale;};
  //!add new texture to final icon
  int AddMarker(Ref<Texture> texture,RString name,int shadow,int size, float offsetX, float offsetY);
  //!edit parameters of texture with given ID (offsets remains)
  void SetMarker(Ref<Texture> texture,RString name,int shadow,int size, int ID);
  //!edit parameters of texture with given ID 
  void SetMarker(Ref<Texture> texture,RString name,int shadow,int size, float offsetX, float offsetY, int ID);
  //!set final icon parameters, applies to all textures used for final icon
  void Setparams(PackedColor markerColor, PackedColor waypointColor,RString text,float scale, bool showIcon, bool showWaypoint);
  //!remove single icon with given ID from group markers list
  void RemoveIcon(int ID);
  //!remove all textures from group markers list
  void Clear(){_groupMakers.Clear();};

  LSError Serialize(ParamArchive &ar);
};

class AIGroup
  : public AI, public AbstractAIMachine<Mission, AIGroupContext>
{
  friend class AISubgroup;
  friend class AICenter;

  typedef AbstractAIMachine<Mission, AIGroupContext> base;
protected:
  // structure
  RefArray<AISubgroup> _subgroups;
  OLinkPermNO(AISubgroup) _mainSubgroup;
  OLinkPermNO(AIUnit) _leader;
  OLinkPermNO(AICenter) _center;
  RadioChannel _radio;
  //unit which controls this group in HC
  OLinkPermNO(AIUnit) _hcCommander;
  //unit to be leader after healed
  OLinkPermNO(AIUnit) _unconsciousLeader;

  /// each unit stored at its ID()-1 location in unitSlots
  AutoArray<AIUnitSlot> _unitSlots;
  //group marker used for high command
  GroupMarkers _groupMakers;
  // state
  Time _lastUpdateTime;
  Time _checkTime;
  Semaphore _semaphore;
  /// auto-detected combat mode
  /**
  see also AIBrain::_combatModeMajor 
  */
  CombatMode _combatModeMinor;

  int _nextCmdId;
  int _locksWP;

  int _enemiesDetected;
  int _unknownsDetected;
  Time _lastEnemyDetected;  
  // _nearestEnemyDist2 is used to optimize target tracking
  // there is no need to track targets often when there is no enemy near
  // this variable is reset always during AddNewTargets
  // therefore it is able to track only enemies that are already in tracking range
  // group value is min over all units
  float _nearestEnemyDist2;
  Time _disclosed;

  // info
  int _id;
  RString _name;
  // source format (localization independent) of _name
  RString _nameFormat;
  AutoArray<RString> _nameParts;

  // targets
  Time _expensiveThinkTime;
  int _expensiveThinkFrac; // frac in ms

  Time _enemyRecomputeCostTime;

  Time _checkCenterDBase; // time when center dbase was last checked

  void ExpensiveThinkDone(); // calculate time of next expensive think

  Time _lastSendTargetTime;

  TargetListInGroup _targetList;
#if _ENABLE_CONVERSATION
  /// List of targets no more stored in target list, but usable in conversation
  RefArray<TargetKnowledge> _targetKB;
#endif
  OLinkPermArray<Transport> _vehicles;

  //@{ Helpers for radio target reporting (group compactness)
  bool    _coreUpdated;  //to ensure the _coreRadius and position is updated before first use
  float   _coreRadius;
  float   _groupCompactness;
  Vector3 _corePosition;
  int     _martaTypeIx;
  //@}

  TargetId _overlookTarget;
  Vector3 _guardPosition;

  // waypoint SUPPORT
  OLinkPermNO(AIGroup) _supportedGroup;
  Vector3 _supportPosition;

  // waypoints, first waypoint (index 0) is always original position of the group
  AutoArray<WaypointInfo> _wp;
  Ref<Script> _script;

  // flee mode
  float _maxStrength;
  float _forceCourage;
  float _courage;
  bool _flee;

  /// leader can issue attack commands (set by the scripting commands)
  bool _attackEnabled;

  // threshold for randomization
  float _threshold;
  Time _thresholdValid;

  int _roleIndex;

  /// allow groups to have a variable space
  GameVarSpace _vars;

#if _VBS3 
  //! all event handlers for given event
  AutoArray<RString> _eventHandlers[NGroupEvent];
#endif

public:
  AIGroup(bool createMainSubgroup = true);
  ~AIGroup();
  void Init();  // call after id is assigned
  void SetIdentity(RString nameFormat, RString *nameParts, int namePartsCount);

  /// access to attached space of variables
  GameVarSpace *GetVars() {return &_vars;}

#if _VBS3
  //@{ Event handlers
  //! add event handler (return handle
  int AddEventHandler(GroupEvent event, RString expression);
  //! remove given event handler
  void RemoveEventHandler(GroupEvent event, int handle);
  //! remove all event handlers
  void ClearEventHandlers(GroupEvent event);
  //! get list of event handlers
  const AutoArray<RString> &GetEventHandlers(GroupEvent event) const;
  //! check if there is some event handler
  bool IsEventHandler(GroupEvent event) const;
  //! generic event handler with typical parameter sets
  void OnEvent(GroupEvent event, const GameValue &pars);
  //! generic event handler with typical parameter sets
  void OnEvent(GroupEvent event, float par1);
#endif

  LSError Serialize(ParamArchive &ar);
  static AIGroup *CreateObject(ParamArchive &ar) {return new AIGroup(false);}
  static AIGroup *LoadRef(ParamArchive &ar);
  LSError SaveRef(ParamArchive &ar) const;

  DECLARE_NETWORK_OBJECT
  static NetworkMessageFormat &CreateFormat
  (
    NetworkMessageClass cls,
    NetworkMessageFormat &format
  );
  static AIGroup *CreateObject(NetworkMessageContext &ctx);
  void DestroyObject();
  TMError TransferMsg(NetworkMessageContext &ctx);
  float CalculateError(NetworkMessageContextWithError &ctx);
  Vector3 GetCurrentPosition() const {return Leader() ? Leader()->Position(Leader()->GetFutureVisualState()) : VZero;}
  int GetNextCmdId() {return _nextCmdId++;}

  RadioChannel &GetRadio() {return _radio;}
  const RadioChannel &GetRadio() const {return _radio;}
  // structure
  AISubgroup *MainSubgroup() const {return _mainSubgroup;}
  AICenter* GetCenter() const;
  AICenter* GetCenterS() const; // not-inlined implementation
  int NSubgroups() const {return _subgroups.Size();}
  AISubgroup *GetSubgroup( int i ) const {return _subgroups[i];}
  AIUnit* Leader() const {return _leader;} 
  AIUnit* HCCommander() const {return _hcCommander;} 
  void SetHCCommander(AIUnit* commander)  {_hcCommander = commander;} ;
  bool IsCameraGroup() const;

  AIUnit* UnconsciousLeader() const {return _unconsciousLeader;} 
  void SetUnconsciousLeader(AIUnit *uncleader) {_unconsciousLeader = uncleader;} 
  /// group led by main (local) player
  bool IsPlayerGroup() const;
  /// group led by any (remote or local) player
  bool IsAnyPlayerGroup() const;
  /// group contains any (remote or local) player
  bool ContainsAnyPlayer() const;
  /// group contains some remote player
  bool ContainsRemotePlayer() const;
  int GetRoleIndex() const {return _roleIndex;}
  void SetRoleIndex(int index) {_roleIndex = index;}

  bool IsPlayerDrivenGroup() const
    {return Leader() && Leader()->IsPlayerDriven();}
  const char* GetName() const {return _name;}
/*
  Texture *GetNameSign() const {return _nameSign;}
  Texture *GetColorSign() const {return _colorSign;}
*/
  int ID() const {return _id;}
  int NUnits() const {return _unitSlots.Size();}
  AIUnit *GetUnit(int index) const
  {
    if (index < 0 || index >= _unitSlots.Size()) return NULL;
    // if the unit is there, it must have a good ID
    Assert(!_unitSlots[index]._unit || _unitSlots[index]._unit->ID()-1==index);
    return _unitSlots[index]._unit;
  }


  // low level function used for network transfer
  // !!! avoid another using
  void SetUnit(int index, AIUnit *unit);
  int UnitsCount() const;
  int NSoldiers() const;
  int NFreeVehicles() const;
  int NFreeManCargo() const;
  int NAliveUnits() const;

  AIGroupType GetType() const {return GTMilitary;}
  const TargetList &GetTargetList() const {return _targetList;}
  TargetList &GetTargetList() {return _targetList;}
  /// add the target to the long-term database
  void OnTargetDeleted(Target *t);
#if _ENABLE_CONVERSATION
  /// List of targets no more stored in target list, but usable in conversation
  const RefArray<TargetKnowledge> &GetTargetKB() const {return _targetKB;}
#endif

  int NVehicles() const {return _vehicles.Size();}
  const Transport *GetVehicle(int i) const {return _vehicles[i];}
  Transport *GetVehicle(int i) {return _vehicles[i];}

  Vector3 GetGroupDirection() const;

  TargetType *GetOverlookTarget() const {return _overlookTarget;}
  void SetOverlookTarget(TargetType *target) {_overlookTarget = target;}

  Vector3Val GetGuardPosition() const {return _guardPosition;}
  void SetGuardPosition(Vector3Par pos) {_guardPosition = pos;}

  // waypoint SUPPORT
  void Support(AIGroup *grp, Vector3Par pos) {_supportedGroup = grp; _supportPosition = pos;}
  void CancelSupport() {_supportedGroup = NULL;}
  const AIGroup *GetSupportedGroup() const {return _supportedGroup;}
  Vector3Val GetSupportPos() const {return _supportPosition;}

  float UpdateAndGetThreshold();

  Time GetDisclosed() const {return _disclosed;}
  void Disclose(DangerCause cause, AIUnit *sender, const Array< Ref<AIUnit> > *possibleSender, EntityAI *causedBy);
  Threat GetAttackInfluence(); 
  Threat GetDefendInfluence(); 
  int NWaypoints() const {return _wp.Size();}
  /// search waypoints before this one and selected a nearest of them
  int FindNearestPreviousWaypoint(int index, Vector3Par pos) const;
  /// add starting waypoint for the group
  void AddFirstWaypoint(Vector3Par pos);
  int AddWaypoint() {return _wp.Add();}
  void InsertWaypoint(int index) {if (index < _wp.Size()) _wp.Insert(index);}
  void DeleteWaypoint(int index) {if (index < _wp.Size()) _wp.Delete(index);}
  const WaypointInfo &GetWaypoint(int i) const {return _wp[i];}
  WaypointInfo &GetWaypoint(int i) {return _wp[i];}
  Vector3 GetWaypointPosition(int index) const;
  int GetCurrentWaypoint() const;
  bool GetWaypointVisible(int index);
  void SetCurrentWaypoint(int index, bool applyEffects = true);
  /// Copy the whole chain of waypoints from the source group
  void CopyWaypoints(const AIGroup *source);
  Script *GetScript() const {return _script;}
  void SetScript(Script *script) {_script = script;}
  Semaphore GetSemaphore() const {return _semaphore;}
  void SetSemaphore(Semaphore s) {_semaphore = s;}
  CombatMode GetCombatModeMinor() const {return _combatModeMinor;}
  void SetCombatModeMinor(CombatMode mode) {_combatModeMinor = mode;}
  void SetCombatModeMajor(CombatMode mode);
  // Enable/disable using ir lasers
  void EnableIRLasers(bool enable);
  // Enable/disable switch on gun light for AI
  void EnableLightOnGun(bool enable);
  void SetCombatModeMajorReactToChange(CombatMode mode);

  bool IsLockedWP() const {return _locksWP > 0;}
  void LockWP(bool lock = true);

  // changes of structure
  void SubgroupRemoved(AISubgroup *subgrp);
  void UnitRemoved(AIUnit *unit);
  void AddSubgroup(AISubgroup *subgrp);

  void AddUnit(AIUnit *unit, int id = -1);
  void AddVehicle(Transport *veh);
  AIUnit *LeaderCandidate(AIUnit *dead) const;
  void SelectLeader(AIUnit *unit);
  void RemoveFromCenter();
  void ForceRemoveFromCenter();

  /// maximum of importances over units (vehicles)
  SimulationImportance GetImportance() const;
  /// return how much the group want to call Think (based on _lastUpdateTime and importance)
  float GetThinkError() const;
  // mind
  bool Think(float timeAvail); // if true, OperPath was called

  void RecomputeTargetSubjectiveCost(TargetNormal *target, Time until);

  // communication with subgroups
  void SendCommand(Command &cmd, bool channelCenter = false);
  void SendCommand(Command &cmd, const OLinkPermNOArray(AIUnit) &list, bool channelCenter = false);
  void IssueCommand(Command &cmd, const OLinkPermNOArray(AIUnit) &list);
  void SendFormation(Formation f, AISubgroup *to);
  void SendSemaphore(Semaphore sem, const OLinkPermNOArray(AIUnit) &list);
  void SendBehaviour(CombatMode mode, const OLinkPermNOArray(AIUnit) &list, bool direct);
  void SendLooseFormation(bool loose, const OLinkPermNOArray(AIUnit) &list);
  void SendOpenFire(OpenFireState open, const OLinkPermNOArray(AIUnit) &list);
  void SendState(RadioMessageState *msg, bool silent=false);
  void SendSuppress(Time until, const OLinkPermNOArray(AIUnit) &list, bool direct);
  void SendReportStatus(const OLinkPermNOArray(AIUnit) &list);
  void SendTarget
  (
    Target *target, bool engage, bool fire,
    const OLinkPermNOArray(AIUnit) &list, bool silent=false
  );

  void SendObjectDestroyed(AIUnit *sender, const EntityAIType *type);
  void SendContact(AIUnit *sender);
  void SendUnderFire(AIUnit *sender);

  void SendCoverMe(AIUnit *sender);
  void SendCovering(AIUnit *sender);
  void SendReloading(AIUnit *sender);
  void SendThrowGrenade(AIUnit *sender);
  void SendThrowSmoke(AIUnit *sender);
  
  //! send message unit is down, autoselect sender
  void SendIsDown(AIUnit *down);
  //! send message unit is down - prefer unit from given vehicle as sender
  void SendIsDown(Transport *vehicle, AIUnit *down);
  //! send message unit is down, sender known
  void SendUnitDown(AIUnit *sender, AIUnit *down);
  void SendClear(AIUnit *sender);
  void SendGetOut(const OLinkPermNOArray(AIUnit) &list);
  void SendAutoCommandToUnit(Command &cmd, AIUnit *unit, bool join = false, bool channelCenter = false);
  void NotifyAutoCommand(Command &cmd, const OLinkPermNOArray(AIUnit) &list);
  void IssueAutoCommand(Command &cmd, const OLinkPermNOArray(AIUnit) &list);
  void ReceiveAnswer(AISubgroup* from, Answer answer);
  void ReceiveUnitStatus(AIUnit *unit, Answer answer);
  bool CommandSent();
  bool CommandSent(Command::Message message);
  bool CommandSent(AIUnit *to, Command::Message message);

  void ClearGetInCommands(AIUnit *to);

   // check unit state and pending messages
  Target *EngageSent(AIUnit *unit) const;
  Target *FireSent(AIUnit *unit) const;
  Target *TargetSent(AIUnit *unit) const;

  bool ReportSent(ReportSubject subject, const EntityAIType *type);
  void ReportFire(AIUnit *who, bool state);
  void ReportArtilleryFire(AIUnit *who, int state);

  AIUnit::ResourceState GetHealthStateReported(AIUnit *unit);
  AIUnit::ResourceState GetAmmoStateReported(AIUnit *unit);
  AIUnit::ResourceState GetFuelStateReported(AIUnit *unit);
  AIUnit::ResourceState GetDamageStateReported(AIUnit *unit);
  AIUnit::ResourceState GetWorstStateReported(AIUnit *unit);
  AIUnit::ResourceType GetWorstStateTypeReported(AIUnit *unit);

  void SetHealthStateReported(AIUnit *unit, AIUnit::ResourceState state);
  void SetAmmoStateReported(AIUnit *unit, AIUnit::ResourceState state);
  void SetFuelStateReported(AIUnit *unit, AIUnit::ResourceState state);
  void SetDamageStateReported(AIUnit *unit, AIUnit::ResourceState state);

  bool GetReportedDown(AIUnit *unit);
  void SetReportedDown(AIUnit *unit, bool state);

  Time GetReportBeforeTime(AIUnit *unit);
  void SetReportBeforeTime(AIUnit *unit, Time time);

  // communication with center
  void ReceiveMission(Mission &mis);
  void SendAnswer(Answer answer);
  void SendReport(ReportSubject subject, TargetNormal &target);
  typedef RefArray<TargetNormal, MemAllocLocal<Ref<TargetNormal>,16> > ReportTargetList;
  void SendRadioReport(ReportTargetList &targets, AIUnit *from);

  //@{ Helpers for Radio Report Target Aggregation
  // Sets the MARTA type of the unit with the highest MARTA importance
  void UpdateMartaType();
  // Get the MARTA type index into World::_martaType, set by UpdateMartaType
  int GetMartaTypeIx() { return _martaTypeIx; };
  // Computes also CoreRadius and CorePosition (needed to determine the level of compactness)
  void UpdateCompactness(bool lazy=false);
  float GetCompactness() const { return _groupCompactness; }
  float GetCoreRadius() const { return _coreRadius; }
  Vector3 GetCorePosition() const { return _corePosition; }
  // SendReport can be called when group consist from only single unit or target is ready for report
  bool CanSendReport(const TargetNormal *target) { return ( target->CanSendReport() || (UnitsCount() <= 1) ); }
  //@}

  // access to FSM
  const Mission *GetMission()
    {return (GetCurrent() != NULL) ? GetCurrent()->_task : NULL;}

  void DoUpdate(); 
  void DoRefresh();

  // interface for FSM
  void Move(AISubgroup *who, Vector3Val destination, Command::Discretion discretion, float precision);
  void Wait(AISubgroup *who, Time until, Command::Discretion discretion);
  void Fire(AISubgroup *who, TargetType *target, int weapon, Command::Discretion discretion);
  void GetIn(AISubgroup *who, TargetType *target, Command::Discretion discretion);

  void MoveUnit(AIUnit *who, Vector3Val destination, Command::Discretion discretion, float precision);

  // FSM helpers
  bool GetAllDone() const; // query group status
  void AllGetOut(); // get out and disable get in
  void CargoGetOut(); // get out and disable get in for cargo

  // ...  
  int NEnemiesDetected() const {return _enemiesDetected;}
  int NUnknownsDetected() const {return _unknownsDetected;}
  void SetCheckTime(Time time) {_checkTime = time;}
  Time GetCheckTime() const {return _checkTime;}
  
  bool FindHealPosition(Command &cmd) const;
  bool FindRepairPosition(Command &cmd) const;
  bool FindRefuelPosition(Command &cmd) const;
  bool FindRearmPosition(Command &cmd) const;
  AIUnit *FindEngineer(EntityAIFull *transport) ;
//  bool FindInfantryRearmPosition(Command &cmd) const;

  const AITargetInfo *FindHealPosition(AIUnit::ResourceState state, AIUnit *unit = NULL) const;
  const AITargetInfo *FindRepairPosition(AIUnit::ResourceState state) const;
  const AITargetInfo *FindRefuelPosition(AIUnit::ResourceState state) const;
  const AITargetInfo *FindRearmPosition(AIUnit::ResourceState state) const;
  const AIUnit *FindFirstAid(AIUnit::ResourceState state, AIUnit *unit = NULL) const;
//  const AITargetInfo *FindInfantryRearmPosition(AIUnit::ResourceState state) const;

  float GetCost() const;              // cost of all group
  float EstimateTime(Vector3Val pos) const; // in minutes
  bool GetFlee() const {return _flee;}
  void AllowFleeing(float allow = 1.0f) {_courage = _forceCourage = 1.0f - allow;}

  bool IsAttackEnabled() const {return _attackEnabled;}
  void EnableAttack(bool enable) {_attackEnabled = enable;}

  void CalculateMaximalStrength();

  RString GetDebugName() const;
  bool AssertValid() const;
  void Dump(int indent = 1) const;

  void UnassignVehicle(Transport *veh);
  void UnitAssignCanceled( AIUnit *unit );

  void AssignVehicles();
  void GetInVehicles();
  
  void RessurectUnit(AIUnit *unit);

  /// check if some unit has weapons to attack given target
  bool GroupCanAttack(const Target &tar) const;

  void ReactToEnemyDetected(Target *enemy);
  void SetLastEnemyDetected(const Time &t) {_lastEnemyDetected = t;}

  float GetDamagePerMinute(TargetNormal *tar) const;
  float GetSubjectiveCost(TargetNormal *tar) const;

  /// Initialize group (waypoints, sensors etc. from mission template)
  void PrepareGroup(const ArcadeGroupInfo &gInfo, bool multiplayer, bool patchWpPos);

  /// Remote player becomes member of group
  void OnRemotePlayerAdded();

  /// start fleeing
  void Flee();

  /// stop fleeing
  void Unflee();

  /// check if some target is so dangerous we should rather retreat (flee)
  bool CheckRetreat();
  
  // group icon for high command
  int AddGroupIcon(Ref<Texture> texture, RString name,int shadow,int size, float offsetX =0, float offsetY=0) {return _groupMakers.AddMarker(texture,name,shadow, size, offsetX, offsetY);};
  void SetGroupIcon(int gID,Ref<Texture> texture,RString name,int shadow,int size) {return _groupMakers.SetMarker(texture,name,shadow, size,gID);};
  void SetGroupIcon(int gID, Ref<Texture> texture, RString name,int shadow,int size, float offsetX, float offsetY) {return _groupMakers.SetMarker(texture,name,shadow, size, offsetX, offsetY,gID);};
  void RemoveGroupIcon(int ID) {_groupMakers.RemoveIcon(ID);};
  void ClearGroupIcons() {_groupMakers.Clear();};
  AutoArray<GIcon> GetGroupIcons(){return _groupMakers._groupMakers ;};
  GroupMarkers GetGroupIcon(){return _groupMakers;};
  void SetGroupIconParams(PackedColor markerColor,PackedColor waypointColor ,RString text,float scale, bool showIcon, bool showWaypoint = false){_groupMakers.Setparams(markerColor,waypointColor,text,scale, showIcon,showWaypoint);};
  

  INHERIT_SOFTLINK(AIGroup,AI);

protected:
  friend class TargetNormal;
  // implementation
  bool CreateTargetList(bool initialize = false, bool report = true);

  void InitUnitSlot(int i); // i is ID-1
  void CopyUnitSlot(int from, int to); // i is ID-1

  void AssignTargets();

  bool CheckFuel();
  bool CheckArmor();
  bool CheckHealth();
  bool CheckAmmo();

  /// check if some units in the group require supporting
  void CheckSupport();

  /// check if all units are still alive
  void CheckAlive();


  float ActualStrength() const;
  void CalculateCourage();

  void SortUnits(); // call only from AICenter::Init (BeginArcade, BeginMultiplayer)

  // init identity based on _nameFormat, _nameParts
  void ApplyIdentity();

private:
  // disable copy
  AIGroup( const AIGroup &src );
  void operator = ( const AIGroup &src );
};


#include <Es/Memory/normalNew.hpp>
/// Target info stored in AICenter::_targets database
class AITargetInfo: public RefCount
{
public:
  TargetId _idExact; ///< unique target identification (link to real object)
  TargetSide _side; ///< expected target side
  Ref<const EntityAIType> _type; ///< expected target type

  Vector3 _realPos; ///< last known target position
  Vector3 _pos; ///< position in exposure map
  Vector3 _dir; ///< direction used in exposure map

  int _x, _z;  ///< position in exposure map

  float _accuracySide; ///< side accuracy (see Target::FadingSideAccuracy())
  Time _timeSide; ///< when _side and _accuracySide was set
  float _accuracyType; ///< type accuracy (see Target::FadingAccuracy())
  Time _timeType; ///< when _type and _accuracyType was set
  float _precisionPos; ///< position accuracy (see Target::FadingPosAccuracy())
  Time _time; ///< when target was last seen

  bool _exposure; ///< target shown in exposure map

  bool _vanished; ///< disappered - e.g. GetIn
  bool _destroyed; ///< dead - do not attack

  AITargetInfo();

  LSError Serialize(ParamArchive &ar);
  static AITargetInfo *CreateObject(ParamArchive &ar) {return new AITargetInfo;}
  
  float FadingSideAccuracy() const;
  float FadingTypeAccuracy() const;
  float FadingPositionAccuracy() const;
  
  USE_FAST_ALLOCATOR
};

#include <Es/Memory/debugNew.hpp>

typedef bool (AICenter::*SideFunction)(TargetSide side) const;

enum AIGuardTargetType
{
  GTTNothing,
  GTTVehicle,
  GTTPoint
};

struct AIGuardTarget
{
  AIGuardTargetType type;
  TargetId vehicle;
  Vector3 position;
};

struct AIGurdedVehicle
{
  TargetId vehicle;
  OLinkPerm<AIGroup> by;

  LSError Serialize(ParamArchive &ar);
};
TypeContainsOLink(AIGurdedVehicle)

struct AIGurdedPoint
{
  Vector3 position;
  OLinkPerm<AIGroup> by;

  LSError Serialize(ParamArchive &ar);
};
TypeContainsOLink(AIGurdedPoint)

struct AIGuardingGroup
{
  OLinkPerm<AIGroup> group;
  bool guarding;

  LSError Serialize(ParamArchive &ar);
};
TypeContainsOLink(AIGuardingGroup)

// support target
struct AISupportTarget
{
  OLinkPerm<AIGroup> group;
  Vector3 pos;
  bool medic;
  bool ambulance;
  bool repair;
  bool rearm;
  bool refuel;
  //force medic to start healing
  bool requestedMedic;
  LSError Serialize(ParamArchive &ar);
};
TypeContainsOLink(AISupportTarget)

// group ready for support (on waypoint SUPPORT)
struct AISupportGroup
{
  OLinkPerm<AIGroup> group;
  bool ambulance;
  bool repair;
  bool rearm;
  bool refuel;
  bool medic;
  OLinkPerm<AIGroup> assigned;

  LSError Serialize(ParamArchive &ar);
};
TypeContainsOLink(AISupportGroup)

DEFINE_ENUM_BEG(EndMode)
  EMContinue, EMKilled, EMLoser,
  EMEnd1, EMEnd2, EMEnd3, EMEnd4, EMEnd5, EMEnd6
DEFINE_ENUM_END(EndMode)

DEFINE_ENUM_BEG(AICenterMode)
    AICMDisabled,AICMArcade,AICMIntro,AICMNetwork
DEFINE_ENUM_END(AICenterMode)

struct VehicleInitMessages;

struct AITargetInfoPresentTraits: public DefMapClassTraits< Ref<AITargetInfo> >
{
  /// stored type
  typedef Ref<AITargetInfo> Type;
  /// key type
  typedef EntityAI *KeyType;
  /// calculate hash value
  static unsigned int CalculateHashValue(KeyType key)
  {
    intptr_t pointerInt = (int)key;
    // we can assume pointer is DWORD aligned
    return (unsigned int)(pointerInt>>2);
  }

  /// compare keys, return no-zero when not equal
  static int CmpKey(KeyType k1, KeyType k2)
  {
    return k1!=k2;
  }
  
  static int CoefExpand() {return DefCoefExpand;}
  
  /// get a key for given a item
  static KeyType GetKey(const Type &item) {return item->_idExact;}

  struct DeleteNullTarget
  {
    bool operator () (const Ref<AITargetInfo> &ref) const
    {
      return ref->_idExact.IsNull();
    }
  };
  
  /// cleanup zombies - handle NULL links
  static int CleanUp(RefArray<AITargetInfo> &array)
  {
    int oldCount = array.Size();
    // remove any targets with idExact = NULL
    array.ForEachWithDelete(DeleteNullTarget());
    // return how many items did we remove
    return oldCount- array.Size();
  }
};

/// commanding center for one army

class AICenter : public AI, public SideInfo
{
  friend class CreateGunner;

  public:
  typedef AICenterMode Mode;

  typedef MapStringToClass<
    Ref<AITargetInfo>, RefArray<AITargetInfo>, AITargetInfoPresentTraits
  > AITargetInfoPresence;

private:
  // Structure
  RefArray<AIGroup> _groups;

  /// Side radio channel (audible to all from the given side)
  RadioChannel _sideRadio;
  /// Command radio channel (audible to all group leaders from the given side)
  RadioChannel _commandRadio;

  Mode _mode;

  // State
  TargetSide _side;
  float _friends[TSideUnknown];
  float _resources; // actual money
  
  /// database of all targets known to the center
  RefArray<AITargetInfo> _targets;
  
  /// hash-map for fast AITargetInfo searching
  /**
  TODO: remove _targets, use ForEach instead of NTargets / GetTarget
  */
  
  AITargetInfoPresence _presence;
  

  // Strategic map
  SRef<AIMap> _map;
  /// it is not necessary to perform expensive maintenance very often
  Time _mapExpensiveMaintenanceTime;

  int _nSoldier;
  int _nWoman;
  //@{ skill and precision coeficient for units belonging to this center
  float _skillCoef;
  float _precisionCoef;
  //@}
  
  // guarded targets
  AutoArray<AIGuardingGroup> _guardingGroups;
  AutoArray<AIGurdedVehicle> _guardedVehicles;
  AutoArray<AIGurdedPoint> _guardedPoints;
  Time _guardingValid;

  // supported targets
  AutoArray<AISupportTarget> _supportTargets;
  AutoArray<AISupportGroup> _supportGroups;
  Time _supportValid;

  //side formations
  FormationInfo _formations[AI::NForms];

public:
  AICenter(TargetSide side, Mode mode=AICMArcade);

  LSError Serialize(ParamArchive &ar);
  static AICenter *CreateObject(ParamArchive &ar)
    {return new AICenter(TSideUnknown, AICMDisabled);}
  static AICenter *LoadRef(ParamArchive &ar);
  LSError SaveRef(ParamArchive &ar) const;

   //!< get skill coeficient for units belonging to this center
  __forceinline float GetSkillCoef() const {return _skillCoef; }
  __forceinline float GetPrecisionCoef() const {return _precisionCoef; }
  
  DECLARE_NETWORK_OBJECT
  static NetworkMessageFormat &CreateFormat
  (
    NetworkMessageClass cls,
    NetworkMessageFormat &format
  );
  static AICenter *CreateObject(NetworkMessageContext &ctx);
  void DestroyObject();
  TMError TransferMsg(NetworkMessageContext &ctx);
  float CalculateError(NetworkMessageContextWithError &ctx);
  static float CalculateErrorCoef(Vector3Par position, Vector3Par cameraPosition) {return 1.0;}
  Vector3 GetCurrentPosition() const {return VZero;}

  RadioChannel &GetSideRadio() {return _sideRadio;}
  const RadioChannel &GetSideRadio() const {return _sideRadio;}
  RadioChannel &GetCommandRadio() {return _commandRadio;}
  const RadioChannel &GetCommandRadio() const {return _commandRadio;}
  float Resources() const {return _resources;}
  void SetResources(float res)
  {
    Assert(res >= 0);
    _resources = res;
  }
  void SpendResources(float toSpend)
  {
#if 0
    Assert(_resources >= toSpend);
    _resources -= toSpend;
#endif
  }
  int NGroups() const {return _groups.Size();}
  AIGroup *GetGroup(int i) const {return _groups[i];}

  int NTargets() const {return _targets.Size();}
  const AITargetInfo &GetTarget(int i) const {return *_targets[i];}
  const AITargetInfo *FindTargetInfo(TargetType *id) const;
  AITargetInfo *FindTargetInfo(TargetType *id);

  Mode GetMode() const {return _mode;}
  void SetMode(Mode mode) {_mode = mode;}

  const FormationInfo &GetFormation(int index) {return _formations[index];};

  TargetSide GetSide() const {return _side;}
  float GetFriendship(TargetSide side) const {return _friends[side];}
  void SetFriendship(TargetSide side, float friendship)
  {
    if (side < TSideUnknown) _friends[side] = friendship;
  }

  //@{ implementation of SideInfo
  virtual bool SideIsFriendly(TargetSide side) const;
  virtual bool SideIsEnemy(TargetSide side) const;
  virtual bool SideIsNeutral(TargetSide side) const;
  //@}
  bool IsFriendly( TargetSide side) const; // more complex test
  bool IsNeutral( TargetSide side) const;
  bool IsEnemy( TargetSide side) const;

  bool IsUnknown( TargetSide side) const; // used when selector function is needed
  bool IsCivilian( TargetSide side) const;
  bool IsWest( TargetSide side) const;
  bool IsEast( TargetSide side) const;
  bool IsResistance( TargetSide side) const;

  void UpdateGuarding();
  AIGuardTarget GetGuardTarget(AIGroup *grp);
  int AddGuardedPoint(Vector3Par pos);
  
  // supported targets
  void UpdateSupport();
  void ReadyForSupport(AIGroup *grp);
  void AskSupport(AIGroup *grp, UIActionType type);
  void SupportDone(AIGroup *grp);
  bool IsSupported(AIGroup *grp, UIActionType type);
  // FIX
  bool WaitingForSupport(AIGroup *grp, UIActionType type);
  bool CanSupport(UIActionType type);
  // new medic support (HealSoldier)
  void InitMedicalSupport(AIGroupContext *context);

  void InitPreview(const ArcadeUnitInfo &info);
  void Init(ArcadeTemplate &t, VehicleInitMessages &inits);

  void InitSensors(bool initialize = false);
  void AddGroup(AIGroup *grp, int id = 0);
  void GroupRemoved(AIGroup *grp);
  void SelectLeader(AIGroup *grp); // implementation of leader selection for group

  // mind
  void Think();
  void UpdateMap();

  // communication with group
  void SendMission(AIGroup *to, Mission &mis);
  void ReceiveAnswer(AIGroup *from, Answer answer);
  void ReceiveReport(AIGroup *from, ReportSubject subject, TargetNormal &target);

  void InitTarget(EntityAI *veh, float age);
  void DeleteTarget(TargetType *id);
  void UpdateTarget(TargetNormal &target);

  //@{ LandGrid coordinate system, all kinds
  AIThreat GetThreatPessimistic(int x, int z) const;
  AIThreat GetThreatOptimistic(int x, int z) const;
  //@}
  
  //@{ LandGrid coordinate system, kind agnostic
  float GetExposurePessimistic(int x, int z) const;
  float GetExposureOptimistic(int x, int z) const;
  //@}

  float GetExposurePessimistic(Vector3Par pos) const;
  float GetExposureOptimistic(Vector3Par pos) const;

  RString GetDebugName() const;
  bool AssertValid() const;
  void Dump(int indent = 0) const;
  
  void NextSoldierIdentity(IdentityInfo &result, const Person *person);

  AIBrain *CreateUnit
  (
    const ArcadeUnitInfo &info, ArcadeTemplate &t, bool multiplayer, AIGroup *grp, int groupIndex = -1, int unitIndex = -1
  );

  EntityAI *CreateBackpack(AIUnit *unit, bool multiplayer);

  void InitSkills( bool multiplayer );

  void LoadIdentityInfo(Person *soldier);
// Implementation
private:
  float RecalculateExposure(int x, int z, AIThreat &result);
  void UpdateGroup(float timeAvail);
  
  void BeginArcade(ArcadeTemplate &t, VehicleInitMessages &inits);

  void BeginPreviewUnit(const ArcadeUnitInfo &info);

  AIBrain *CreateSoldier(Transport *transport, int rank, bool multiplayer);

  friend EntityAI *CreateVehicle(const ArcadeUnitInfo &info, ArcadeTemplate &t, TargetSide side, bool multiplayer);
  friend Vehicle *CreateSensor(const ArcadeSensorInfo &info, AIGroup *grp, bool multiplayer);
  friend void CreateMarker(const ArcadeMarkerInfo &info, bool multiplayer);

  void RemoveOldExposures();
  void AddNewExposures();

  void InitFormations();
};

AICenter *CreateCenter(ArcadeTemplate &t, TargetSide side, AICenter::Mode mode);
void InitNoCenters(ArcadeTemplate &t, VehicleInitMessages &inits, AICenterMode mode);

inline AISubgroup* AIUnit::GetSubgroup() const {return _subgroup;}
inline AIGroup* AISubgroup::GetGroup() const {return _group;}
inline AICenter* AIGroup::GetCenter() const {return _center;}
inline bool AIUnit::IsSubgroupLeader() const {return GetSubgroup() && GetSubgroup()->Leader() == this;}
inline bool AIUnit::IsSubgroupLeaderVehicle() const {return GetSubgroup() && GetSubgroup()->Leader()->GetVehicle() == GetVehicle();}

/******************************************/
/*                                        */
/*              Statistics                */
/*                                        */
/******************************************/

enum AIStatsEventType
{
  SETUnitLost,
  SETKillsEnemyInfantry,
  SETKillsEnemySoft,
  SETKillsEnemyArmor,
  SETKillsEnemyAir,
  SETKillsFriendlyInfantry,
  SETKillsFriendlySoft,
  SETKillsFriendlyArmor,
  SETKillsFriendlyAir,
  SETKillsCivilInfantry,
  SETKillsCivilSoft,
  SETKillsCivilArmor,
  SETKillsCivilAir,
  SETUser,
};

enum AIStatsKills
{
  SKEnemyInfantry,
  SKEnemySoft,
  SKEnemyArmor,
  SKEnemyAir,
  SKFriendlyInfantry,
  SKFriendlySoft,
  SKFriendlyArmor,
  SKFriendlyAir,
  SKCivilianInfantry,
  SKCivilianSoft,
  SKCivilianArmor,
  SKCivilianAir,
  SKN
};

#define AI_STATS_ROW_MSG_CREATE(MessageName, XX) \
  XX(MessageName, int, dpnid, NDTInteger, int, NCTNone, DEFVALUE(int, 0), DOC_MSG("Client (player) id"), TRANSF) \
  XX(MessageName, int, role, NDTInteger, int, NCTNone, DEFVALUE(int, -1), DOC_MSG("Index of player role"), TRANSF) \
  XX(MessageName, RString, name, NDTString, RString, NCTNone, DEFVALUE(RString, ""), DOC_MSG("Player name"), TRANSF) \
  XX(MessageName, TargetSide, side, NDTInteger, int, NCTSmallUnsigned, DEFVALUE(int, NCTSmallUnsigned), DOC_MSG("Player side"), TRANSF)

DECLARE_NET_INDICES_EX(AIStatsMPRowCreate, NetworkObject, AI_STATS_ROW_MSG_CREATE)

#define AI_STATS_ROW_MSG_UPDATE(MessageName, XX) \
  XX(MessageName, int, killsInfantry, NDTInteger, int, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG("Infantry units killed by this player"), TRANSF, ET_ABS_DIF, ERR_COEF_MODE) \
  XX(MessageName, int, killsSoft, NDTInteger, int, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG("Soft units killed by this player"), TRANSF, ET_ABS_DIF, 2 * ERR_COEF_MODE) \
  XX(MessageName, int, killsArmor, NDTInteger, int, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG("Armored units killed by this player"), TRANSF, ET_ABS_DIF, 3 * ERR_COEF_MODE) \
  XX(MessageName, int, killsAir, NDTInteger, int, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG("Air units killed by this player"), TRANSF, ET_ABS_DIF, 5 * ERR_COEF_MODE) \
  XX(MessageName, int, killsPlayers, NDTInteger, int, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG("Other players killed by this player"), TRANSF, ET_NONE, 0) \
  XX(MessageName, int, customScore, NDTInteger, int, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG("Score added to this player by mission designer"), TRANSF, ET_ABS_DIF, ERR_COEF_MODE) \
  XX(MessageName, int, liveStats, NDTInteger, int, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG("Score for Xbox Live statistics for team based games"), TRANSF, ET_ABS_DIF, ERR_COEF_MODE) \
  XX(MessageName, int, killed, NDTInteger, int, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG("Number of times this player was killed"), TRANSF, ET_ABS_DIF, ERR_COEF_MODE)

DECLARE_NET_INDICES_EX_ERR(AIStatsMPRowUpdate, NetworkObject, AI_STATS_ROW_MSG_UPDATE)

#define COPY_MEMBERS_FROM(ObjFrom,type,name,datatype,rawType, compres, defval, description, xfer, errType, errCoef ) \
  _##name = ObjFrom->_##name;

class AIStatsMPRow : public NetworkObject, public SerializeClass
{
public:
  AI_STATS_ROW_MSG_CREATE(AIStatsMPRowCreate, MSG_DEF)
  AI_STATS_ROW_MSG_UPDATE(AIStatsMPRowUpdate, MSG_DEF_ERR)
  
  int _killsTotal;
  int _order;

protected:
  NetworkId _id;
  bool _local;

public:
  AIStatsMPRow();
  ~AIStatsMPRow();

  void RecalculateTotal();

  void SetNetworkId(NetworkId &id) {_id = id;}
  NetworkId GetNetworkId() const {return _id;}
  bool IsLocal() const {return _local;}
  void SetLocal(bool local = true) {_local = local;}
  Vector3 GetCurrentPosition() const {return VZero;}

  DECLARE_NETWORK_OBJECT
  static NetworkMessageFormat &CreateFormat
  (
    NetworkMessageClass cls,
    NetworkMessageFormat &format
  );
  TMError TransferMsg(NetworkMessageContext &ctx);
  float CalculateError(NetworkMessageContextWithError &ctx);
  static float CalculateErrorCoef(Vector3Par position, Vector3Par cameraPosition) {return 1.0;}

  static AIStatsMPRow *CreateObject(NetworkMessageContext &ctx);
  static AIStatsMPRow *CreateObject(ParamArchive &ar);
  void DestroyObject();
  RString GetDebugName() const;

  LSError Serialize(ParamArchive &ar);
};

struct AIStatsEvent
{
  AIStatsEventType type;
  float time; // from Clock
  Vector3 position;
  RString message;

  RString killedType;
  RString killerType;
  RString killedName;
  RString killerName;
  bool killedPlayer;
  bool killerPlayer;

  LSError Serialize(ParamArchive &ar);
};
TypeIsMovable(AIStatsEvent)

#if _ENABLE_VBS

enum VBSStatsEventType
{
  VBSETKill,
  VBSETInjury,
  VBSETUser,
};

struct VBSUnitInfo
{
  RString name;
  RString type;
  Vector3 position;
  TargetSide side;
  bool player;
};

struct VBSStatsEvent
{
  Time t;
  VBSStatsEventType type;
  VBSUnitInfo unit1;
  VBSUnitInfo unit2;
  float value;
  RString text;
};
TypeIsMovable(VBSStatsEvent)
#endif

class AIStatsMission : public SerializeClass
{
protected:
  float _size;
  PackedColor _color;
  PackedColor _colorSelected;
  PackedColor _colorTitleBg;
  PackedColor _colorWest;
  PackedColor _colorEast;
  PackedColor _colorCiv;
  PackedColor _colorRes;
  int _shadow;

  static const int columns = 8;
  Ref<Font> _font;
  float _colWidths[columns];
  bool _doubleLines[columns];
  float _totalWidth;
  Ref<Texture> _pictures[columns];
  PackedColor _bgColors[columns];

public:
  AutoArray<AIStatsEvent> _events;
  RefArray<AIStatsMPRow> _tableMP;   //both players and AI statistics
  int _lives;
  Time _start;
  time_t _time;

#if _ENABLE_VBS
  AutoArray<RString> _vbsHeader;
  AutoArray<VBSStatsEvent> _vbsEvents;
  AutoArray<RString> _vbsFooter;
#endif

public:
  AIStatsMission();
  void Init();
  
  void Clear();
  void AddEvent
  (
    AIStatsEventType type, Vector3Par position, RString message,
    const EntityAIType *killedType, RString killedName, bool killedPlayer,
    const EntityAIType *killerType, RString killerName, bool killerPlayer
  );
  void AddMPKill(RString playerName, TargetSide playerSide, int dpnid, int playerRole, const VehicleType *killedType, bool killedPlayer, bool isEnemy);
  void AddMPScore(RString playerName, TargetSide playerSide, int dpnid, int playerRole, int score);
  void AddLiveStats(RString playerName, TargetSide playerSide, int dpnid, int playerRole, int score);
#if _ENABLE_VBS
  void VBSKilled(EntityAI *killed, EntityAI *killer);
  void VBSInjured(EntityAI *injured, EntityAI *killer, float damage, RString ammo);
  void VBSAddHeader(RString text);
  void VBSAddEvent(RString text);
  void VBSAddFooter(RString text);
  void WriteVBSStatistics();
#endif
  void OnMPMissionStart();

  void DrawMPTable(float alpha);

  LSError Serialize(ParamArchive &ar);

  void Sort();

  float ScoreSide(TargetSide side);

  const AIStatsMPRow *GetRow(int dpnid) const;

  /// Try to find existing matching row
  int FindRowNoAdd(RString playerName, TargetSide playerSide, int dpnid, int playerRole);
  /// Add statistic row when either aiKills or forceAdd is set on
  int TryAddRow(RString playerName, TargetSide playerSide, int dpnid, int playerRole, bool forceAdd=false);
  /// Try to find existing matching row or add a new one (if required)
  int FindRow(RString playerName, TargetSide playerSide, int dpnid, int playerRole, bool forceAdd=false);
};

struct AIUnitHeader : public SerializeClass
{
  int index;
  float experience;
  Rank rank;

  OLinkPerm<AIBrain> unit;
  bool used;

  LSError Serialize(ParamArchive &ar);
};
TypeIsGeneric(AIUnitHeader)

/*
class AIUnitPool : public SerializeClass
{
public:
  AutoArray<AIUnitHeader> _units;
  TargetSide _side;
  int _nextSoldier;

public:
  AIUnitPool(TargetSide side) {_side = side; Clear();}

  void Clear();
  void Update();
  LSError Serialize(ParamArchive &ar);
};
*/

class AIStatsCampaign : public SerializeClass
{
public:
  AIUnitHeader _playerInfo;
//  AIUnitPool _westUnits;
//  AIUnitPool _eastUnits;
  AutoArray<RString> _awards;
  AutoArray<RString> _penalties;

  float _casualties;
  float _inCombat;
  int _kills[SKN];

  AutoArray<GameVariable> _variables;

  float _score, _lastScore;
  float _liveScore;

  time_t _date;

public:
  AIStatsCampaign();
  void Clear();
  void Update(AIStatsMission &mission);
  void AddVariable(GameVariable &var);

  LSError CampaignSerialize(ParamArchive &ar);
  LSError Serialize(ParamArchive &ar);
};

class AIStats : public SerializeClass
{
public:
  AIStatsMission _mission;
  AIStatsCampaign _campaign;

public:
  AIStats();
  ~AIStats();

  void Init() {_mission.Init();}
  
  void ClearAll() {_campaign.Clear(); _mission.Clear();}
  void ClearMission() {_mission.Clear();}

  void OnMPMissionStart();
  void OnMPMissionEnd();

  void Update() {_campaign.Update(_mission);}

  void OnVehicleDestroyed(EntityAI *killed, EntityAI *killer);
#if _ENABLE_VBS
  void OnVehicleDamaged(EntityAI *injured, EntityAI *killer, float damage, RString ammo);
  void VBSAddHeader(RString text);
  void VBSAddEvent(RString text);
  void VBSAddFooter(RString text);
#endif
  void AddUserEvent(RString event, Vector3Par pos);

  void DrawMPTable(float alpha) {_mission.DrawMPTable(alpha);}

  LSError Serialize(ParamArchive &ar);
};

extern AIStats GStats;
inline AIUnitHeader &PlayerInfo() {return GStats._campaign._playerInfo;}


extern ArcadeTemplate CurrentTemplate;

extern RString CurrentCampaign;
extern RString CurrentBattle;
extern RString CurrentMission;
extern RString CurrentWorld;
extern RString CurrentFile;

inline bool IsCampaign() {return CurrentBattle.GetLength() > 0;}

LSError AIGlobalSerialize(ParamArchive &ar);

#ifdef _WIN32
template Ref<AIStatsMPRow>;
#endif

#endif
