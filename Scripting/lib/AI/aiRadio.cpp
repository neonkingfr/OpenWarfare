// AI - implementation of AI radio messages

#include "../wpch.hpp"
#include "ai.hpp"
#include "../person.hpp"
#include "aiDefs.hpp"

#include "aiRadio.hpp"

#include "../world.hpp"
#include "../landscape.hpp"
//#include "loadStream.hpp"
#include <El/Common/randomGen.hpp>
#include "../global.hpp"

//#include "strIncl.hpp"
#include "../stringtableExt.hpp"
#include "../paramArchiveExt.hpp"

#include <El/ParamFile/paramFile.hpp>

#include "../sentences.hpp"
#include "../UI/uiActions.hpp"

#include <ctype.h>

#include "../UI/chat.hpp"

#include <El/Enum/enumNames.hpp>
#include <El/Evaluator/express.hpp>

#include "../Network/network.hpp"

#include <Es/Common/win.h>
#include <Es/Strings/bString.hpp>

#include "../gameDirs.hpp"

#include <El/HiResTime/hiResTime.hpp>

/*
#define PAUSE_AFTER_WORD				0.00F
#define PAUSE_IN_NUMBER					0.00F
#define PAUSE_AFTER_NUMBER			PAUSE_AFTER_WORD
#define PAUSE_IN_UNIT_LIST			0.00F
#define PAUSE_AFTER_UNIT_LIST		0.05F
*/

///////////////////////////////////////////////////////////////////////////////
// helper functions

static void Spell(RadioSentence &sentence, const char *buffer, float pauseAfter, const RadioChannelParams &cfg)
{
  int i, n = strlen(buffer);
  for (i=0; i<n; i++)
  {
    float pause = (i == n - 1) ? pauseAfter : cfg.pauseInNumber;
    char c = tolower(buffer[i]);
    switch (c)
    {
#if 0
    case '0':
      sentence.Add("zero", pause);
      break;
    case '1':
      sentence.Add("one", pause);
      break;
    case '2':
      sentence.Add("two", pause);
      break;
    case '3':
      sentence.Add("three", pause);
      break;
    case '4':
      sentence.Add("four", pause);
      break;
    case '5':
      sentence.Add("five", pause);
      break;
    case '6':
      sentence.Add("six", pause);
      break;
    case '7':
      sentence.Add("seven", pause);
      break;
    case '8':
      sentence.Add("eight", pause);
      break;
    case '9':
      sentence.Add("nine", pause);
      break;
#else
    case '0':
      sentence.Add("zero2", pause);
      break;
    case '1':
      sentence.Add("one2", pause);
      break;
    case '2':
      sentence.Add("two2", pause);
      break;
    case '3':
      sentence.Add("three2", pause);
      break;
    case '4':
      sentence.Add("four2", pause);
      break;
    case '5':
      sentence.Add("five2", pause);
      break;
    case '6':
      sentence.Add("six2", pause);
      break;
    case '7':
      sentence.Add("seven2", pause);
      break;
    case '8':
      sentence.Add("eight2", pause);
      break;
    case '9':
      sentence.Add("nine2", pause);
      break;
#endif
    case 'a':
      sentence.Add("alpha", pause);
      break;
    case 'b':
      sentence.Add("bravo", pause);
      break;
    case 'c':
      sentence.Add("charlie", pause);
      break;
    case 'd':
      sentence.Add("delta", pause);
      break;
    case 'e':
      sentence.Add("echo", pause);
      break;
    case 'f':
      sentence.Add("foxtrot", pause);
      break;
    case 'g':
      sentence.Add("golf", pause);
      break;
    case 'h':
      sentence.Add("hotel", pause);
      break;
    case 'i':
      sentence.Add("india", pause);
      break;
    case 'j':
      sentence.Add("juliet", pause);
      break;
    case 'k':
      sentence.Add("kilo", pause);
      break;
    case 'l':
      sentence.Add("lima", pause);
      break;
    case 'm':
      sentence.Add("mike", pause);
      break;
    case 'n':
      sentence.Add("november", pause);
      break;
    case 'o':
      sentence.Add("oscar", pause);
      break;
    case 'p':
      sentence.Add("papa", pause);
      break;
    case 'q':
      sentence.Add("quebec", pause);
      break;
    case 'r':
      sentence.Add("romeo", pause);
      break;
    case 's':
      sentence.Add("sierra", pause);
      break;
    case 't':
      sentence.Add("tango", pause);
      break;
    case 'u':
      sentence.Add("uniform", pause);
      break;
    case 'v':
      sentence.Add("victor", pause);
      break;
    case 'w':
      sentence.Add("whiskey", pause);
      break;
    case 'x':
      sentence.Add("xray", pause);
      break;
    case 'y':
      sentence.Add("yankee", pause);
      break;
    case 'z':
      sentence.Add("zulu", pause);
      break;
    case '.':
    case ',':
    case ':':
    case ';':
    case '?':
    case '!':
    case '-':
    case '(':
    case ')':
      break;
    default:
      Fail("Character");
      break;
    }
  }
}

static void SayUnitNumber(RadioSentence &sentence, int number, float pause, const RadioChannelParams &cfg)
{
  if (number > 100)
  {
    Spell(sentence, Format("%d", number), pause, cfg);
    return;
  }
  if (number == 100)
  {
    sentence.Add("hundred", pause);
    return;
  }
  int tens = 0;
  if (number >= 20)
  {
    tens = number / 10;
    number -= tens * 10;
  }
  float pauseAfterTens = number == 0 ? pause : cfg.pauseInNumber;
  switch (tens)
  {
  case 0:
    break;
  case 2:
    sentence.Add("twenty", pauseAfterTens);
    break;
  case 3:
    sentence.Add("thirty", pauseAfterTens);
    break;
  case 4:
    sentence.Add("forty", pauseAfterTens);
    break;
  case 5:
    sentence.Add("fifty", pauseAfterTens);
    break;
  case 6:
    sentence.Add("sixty", pauseAfterTens);
    break;
  case 7:
    sentence.Add("seventy", pauseAfterTens);
    break;
  case 8:
    sentence.Add("eighty", pauseAfterTens);
    break;
  case 9:
    sentence.Add("ninety", pauseAfterTens);
    break;
  default:
    Fail("Unexpected number");
    break;
  }
  switch (number)
  {
  case 0:
    break;
  case 1:
    sentence.Add("one", pause);
    break;
  case 2:
    sentence.Add("two", pause);
    break;
  case 3:
    sentence.Add("three", pause);
    break;
  case 4:
    sentence.Add("four", pause);
    break;
  case 5:
    sentence.Add("five", pause);
    break;
  case 6:
    sentence.Add("six", pause);
    break;
  case 7:
    sentence.Add("seven", pause);
    break;
  case 8:
    sentence.Add("eight", pause);
    break;
  case 9:
    sentence.Add("nine", pause);
    break;
  case 10:
    sentence.Add("ten", pause);
    break;
  case 11:
    sentence.Add("eleven", pause);
    break;
  case 12:
    sentence.Add("twelve", pause);
    break;
  case 13:
    sentence.Add("thirteen", pause);
    break;
  case 14:
    sentence.Add("fourteen", pause);
    break;
  case 15:
    sentence.Add("fifteen", pause);
    break;
  case 16:
    sentence.Add("sixteen", pause);
    break;
  case 17:
    sentence.Add("seventeen", pause);
    break;
  case 18:
    sentence.Add("eighteen", pause);
    break;
  case 19:
    sentence.Add("nineteen", pause);
    break;
  default:
    Fail("Unexpected number");
    break;
  }
}

static void SayNumber
(
  RadioSentence &sentence, int number, float pauseAfter, const RadioChannelParams &cfg,
  const char *format = "%d", int mode=1
)
{
  switch (mode)
  {
    default:
      char buffer[32];
      sprintf(buffer, format, number);
      Spell(sentence, buffer, pauseAfter, cfg);
    break;
    case 2:
      SayUnitNumber(sentence, number, pauseAfter, cfg);
    break;
  }
}

static void SayUnit(RadioSentence &sentence, AIBrain *agent, int mode, float pause, const RadioChannelParams &cfg)
{
  Assert(agent);
  AIUnit *unit = agent->GetUnit();
  if (!unit) return;
  Assert(unit->ID() > 0);
  if (mode == 1)
  {
    SayUnitNumber(sentence,unit->ID(),pause,cfg);
  }
  else
  {
    Fail("Mode");
  }
}

static void PrepareList(OLinkPermNOArray(AIUnit) &result, const OLinkPermNOArray(AIUnit) &list, bool wholeCrew)
{
  result = list;
  result.RemoveNulls();
  if (!wholeCrew) return;
  
  for (int i=0; i<list.Size(); i++)
  {
    AIUnit *unit = list[i];
    if (!unit) continue;
    Transport *veh = unit->GetVehicleIn();
    if (!veh) continue;
    // one of the vehicle is always CommanderUnit
    AIBrain *commanderBrain = veh->CommanderUnit();
    AIUnit *commander = commanderBrain ? commanderBrain->GetUnit() : NULL;
    if (commander && unit != commander && commander->GetUnit())
    {
      if (commander->GetGroup() != unit->GetGroup())
        // cannot command (vehicle is under commanding of other group)
        result.DeleteKey(unit);
      else if (!commander->IsGroupLeader())
        // pass command to vehicle commander
        result.AddUnique(commander);
    }
  }
}

static bool AllUnits(OLinkPermNOArray(AIUnit) &list, bool wholeCrew)
{
  AIGroup *grp = NULL;
  int count = 0;
  for (int i=0; i<list.Size(); i++)
  {
    AIUnit *unit = list[i];
    if (unit)
    {
      count++;
      if (!grp) grp = unit->GetGroup();
    }
  }
  if (count <= 1) return false;
  if (!grp) return false;
  
  for (int i=0; i<grp->NUnits(); i++)
  {
    AIUnit *unit = grp->GetUnit(i);
    if (!unit) continue;
    if (wholeCrew)
    {
      Transport *veh = unit->GetVehicleIn();
      if (veh)
      {
        AIBrain *commander = veh->CommanderUnit();
        if (commander && commander->GetUnit())
        {
          unit = commander->GetUnit();
        }
      }
    }
    if (unit == grp->Leader()) continue;
    if (list.FindKey(unit) < 0) return false;
  }

  return true;
}

static const EnumName TeamNames[]=
{
  EnumName(TeamMain, "MAIN"),
  EnumName(TeamRed, "RED"),
  EnumName(TeamGreen, "GREEN"),
  EnumName(TeamBlue, "BLUE"),
  EnumName(TeamYellow, "YELLOW"),
  EnumName()
};
template<>
const EnumName *GetEnumNames(Team dummy)
{
  return TeamNames;
}

//Team GetTeam(int i);
//void SetTeam(int i, Team team);

static int WholeTeam(OLinkPermNOArray(AIUnit) &list, bool wholeCrew, bool avoidTeams)
{
  if (avoidTeams) return -1;

  AIGroup *grp = NULL;
  int count = 0;
  for (int i=0; i<list.Size(); i++)
  {
    AIUnit *unit = list[i];
    if (unit)
    {
      count++;
      if (!grp) grp = unit->GetGroup();
    }
  }
  if (count <= 1) return -1;
  if (!grp) return -1;

  int all = 0;
  int main = 0;
  int red = 0;
  int green = 0;
  int blue = 0;
  int yellow = 0;
  int mainOut = 0;
  int redOut = 0;
  int greenOut = 0;
  int blueOut = 0;
  int yellowOut = 0;

  for (int i=0; i<grp->NUnits(); i++)
  {
    AIUnit *unit = grp->GetUnit(i);
    if (!unit) continue;
    if (wholeCrew)
    {
      Transport *veh = unit->GetVehicleIn();
      if (veh && veh->GetType()->HasObserver())
      {
        AIBrain *commanderAgent = veh->ObserverBrain();
        AIUnit *commander = commanderAgent ? commanderAgent->GetUnit() : NULL;;
        AIBrain *driverAgent = veh->DriverBrain();
        AIUnit *driver = driverAgent ? driverAgent->GetUnit() : NULL;
        AIBrain *gunnerAgent = veh->GunnerBrain();
        AIUnit *gunner = gunnerAgent ? gunnerAgent->GetUnit() : NULL;
        if (commander)
        {
          if (driver == unit || gunner == unit)
          {
            unit = commander;
          }
        }
        else if (driver)
        {
          if (gunner == unit)
          {
            unit = driver;
          }
        }
      }
    }
    if (unit == grp->Leader()) continue;

    Team team = GWorld->UI()->GetTeam(unit);
    if (list.FindKey(unit) >= 0)
    {
      all++;
      switch (team)
      {
      case TeamMain:
        main++;
        break;
      case TeamRed:
        red++;
        break;
      case TeamGreen:
        green++;
        break;
      case TeamBlue:
        blue++;
        break;
      case TeamYellow:
        yellow++;
        break;
      }
    }
    else
    {
      switch (team)
      {
      case TeamMain:
        mainOut++;
        break;
      case TeamRed:
        redOut++;
        break;
      case TeamGreen:
        greenOut++;
        break;
      case TeamBlue:
        blueOut++;
        break;
      case TeamYellow:
        yellowOut++;
        break;
      }
    }
  }

  if (all > 0)
  {
    if (main == all && mainOut == 0) return TeamMain;
    if (red == all && redOut == 0) return TeamRed;
    if (green == all && greenOut == 0) return TeamGreen;
    if (blue == all && blueOut == 0) return TeamBlue;
    if (yellow == all && yellowOut == 0) return TeamYellow;
  }
  return -1;
}

static void SayUnits(RadioSentence &sentence, OLinkPermNOArray(AIUnit) &list, int mode, float pauseAfter, const RadioChannelParams &cfg)
{
  for (int i=0; i<list.Size(); i++)
  {
    AIUnit *unit = list[i];
    Assert(unit);
    Assert(unit->ID() > 0);
    float pause;
    if (i == list.Size() - 1)
      pause = pauseAfter;
    else
      pause = cfg.pauseInUnitList;
    SayUnitNumber(sentence, unit->ID(), pause, cfg);
  }
}

/// detect and output continuous parts of the list
/**
List can often look like 2..8, 10..15
*/

class ListOutput
{
  LString _buffer;
  int _from;
  int _to;
  
  public:
  ListOutput(char *buffer, int bufferSize)
  :_buffer(buffer,bufferSize),_from(-1),_to(-2)
  {
    strcpy(buffer,"");
  }
  ~ListOutput(){Flush();}
  
  void Add(int number);
  void Flush();
};

void ListOutput::Add(int number)
{
  if (number!=_to+1)
  {
    Flush();
    _from = _to = number;
  }
  else
  {
    _to = number;
  }
}

void ListOutput::Flush()
{
  // if empty, do nothing
  if (_from>_to) return;
  
  // if not empty, add delimiter
  if (_buffer[0]) strcat(_buffer, ", ");
  int lenBuf = strlen(_buffer);
  if (_from==_to)
  {
    // one item only
    LString tmp = _buffer + lenBuf;
    sprintf(tmp, "%d", _from);
  }
  else if (_from+1==_to)
  {
    // two items, still no need to use ...
    LString tmp = _buffer + lenBuf;
    sprintf(tmp, "%d, %d", _from, _to);
  }
  else
  {
    // more than two items, use ..
    LString tmp = _buffer + lenBuf;
    sprintf(tmp, "%d..%d", _from, _to);
  }
  _from = -1;
  _to = -2;
}

void CreateUnitsList(OLinkPermNOArray(AIUnit) &list, char *buffer, int bufferSize)
{
  ListOutput output(buffer,bufferSize);
  // 32*8 = 256 should be enough for all practical purposes
  PackedBoolAutoArrayT< MemAllocLocal<PackedBoolArray,32> > bits;
  // list ordering may be arbitrary
  // we want ordered list
  // as we know input is a set, using QSort would be overkill - O(N*logN)
  // constructing an parsing a set can be done in O(N)
  for (int i=0; i<list.Size(); i++)
  {
    AIUnit *unit = list[i];
    if (unit)
    {
      bits.Set(unit->ID(),true);
    }
  }
  for (int i=0; i<bits.Size(); i++)
  {
    if (bits[i])
    {
      output.Add(i);
    }
  }
}

void PositionToAA11(Vector3Val pos, char *buffer);

Object *FindNearestMoveObject(Vector3Par pos)
{
  if (!GWorld->CameraOn()) return NULL;
  float dist2 = pos.Distance2(GWorld->CameraOn()->FutureVisualState().Position());
  if (dist2 > Square(400)) return NULL;
  float diff = 0.25 * sqrt(dist2);
  saturate(diff, 5.0, 100.0);
  int xMin,xMax,zMin,zMax;
  ObjRadiusRectangle(xMin,xMax,zMin,zMax,pos,pos,diff);

  Object *best = NULL;
  const float minDist2 = diff * diff;
  float minFunc = FLT_MAX;
  for (int x=xMin; x<=xMax; x++)
    for (int z=zMin; z<=zMax; z++)
    {
      const ObjectListUsed &list = GLandscape->UseObjects(x,z);
      int n = list.Size();
      for (int i=0; i<n; i++)
      {
        Object *obj = list[i];
        if (obj->GetType() == Network) continue;
        if (obj->GetType() == Temporary) continue;
        if (obj->GetType() == TypeTempVehicle) continue;
        if (!obj->IsMoveTarget()) continue;
        float dist2 = pos.Distance2(obj->FutureVisualState().Position());
        if (dist2 >= minDist2) continue;
        float func = dist2 * obj->GetInvMass();
        if (func >= minFunc) continue;
        minFunc = func ;
        best = obj;
      }
    } // (for all near objects)
    return best;
}

int SentenceParams::Add(AISubgroup *subgrp, bool wholeCrew, bool avoidTeams)
{
  return base::Add(new SentenceParamUnitList(subgrp, wholeCrew, avoidTeams));
}

int SentenceParams::Add(const OLinkPermNOArray(AIUnit) &units, bool wholeCrew, bool avoidTeams)
{
  return base::Add(new SentenceParamUnitList(units, wholeCrew, avoidTeams));
}

static const RStringB azimSay[13]=
{
  "at12","at1",
  "at2","at3",
  "at4","at5",
  "at6","at7",
  "at8","at9",
  "at10","at11",
  "at12"
};
static const int *azimWrite[13]=
{
  &IDS_WORD_AT12,&IDS_WORD_AT1,
  &IDS_WORD_AT2,&IDS_WORD_AT3,
  &IDS_WORD_AT4,&IDS_WORD_AT5,
  &IDS_WORD_AT6,&IDS_WORD_AT7,
  &IDS_WORD_AT8,&IDS_WORD_AT9,
  &IDS_WORD_AT10,&IDS_WORD_AT11,
  &IDS_WORD_AT12
};

void SentenceParams::AddAzimutRelDir(Vector3Par dir)
{
  int azimut = toInt(atan2(dir.X(), dir.Z()) * (6 / H_PI));
  if (azimut < 0) azimut += 12;

  //int ret = -1;
  if (azimut>=0 && azimut<=12)
  {
    AddWord(azimSay[azimut],LocalizeString(*azimWrite[azimut]));
  }
}

static const RStringB distSay[]=
{
  "dist50", // 0..50
  "dist100", // 50..150
  "dist200","dist200",
  "dist500",
  "dist500", // 500
  "dist500",
  "dist1000","dist1000","dist1000",
  "dist1000", // 1000
  "dist1000","dist1000","dist1000","dist1000","dist1000",
  "dist2000","dist2000","dist2000","dist2000",
  "dist2000", // 2000
  "dist2000","dist2000","dist2000","dist2000","dist2000",
  "far" // 2500 (and more)
};

static const int *distWrite[]=
{
  &IDS_WORD_DIST50,
  &IDS_WORD_DIST100, // 50..150
  &IDS_WORD_DIST200,&IDS_WORD_DIST200,
  &IDS_WORD_DIST500,
  &IDS_WORD_DIST500, // 500
  &IDS_WORD_DIST500,
  &IDS_WORD_DIST1000,&IDS_WORD_DIST1000,
  &IDS_WORD_DIST1000,
  &IDS_WORD_DIST1000, // 1000
  &IDS_WORD_DIST1000,&IDS_WORD_DIST1000,
  &IDS_WORD_DIST1000,&IDS_WORD_DIST1000,
  &IDS_WORD_DIST1000,
  &IDS_WORD_DIST2000,&IDS_WORD_DIST2000,
  &IDS_WORD_DIST2000,&IDS_WORD_DIST2000,
  &IDS_WORD_DIST2000, // 2000
  &IDS_WORD_DIST2000,&IDS_WORD_DIST2000,
  &IDS_WORD_DIST2000,&IDS_WORD_DIST2000,
  &IDS_WORD_DIST2000,
  &IDS_WORD_DISTFAR // 2500 (and more)
};

void SentenceParams::AddDistance(float dist)
{
  int dist100 = toInt(dist*0.01);
  const int maxDist = sizeof(distWrite)/sizeof(*distWrite)-1;
  saturate(dist100,0,maxDist);

  AddWord(distSay[dist100],LocalizeString(*distWrite[dist100]));
}

void SentenceParams::AddDistance(float dist, int martaTypeIx, const RadioMessage *msg)
{
  static float defaultDistLevels[2] = {50.0f, 150.0f};
  if ( martaTypeIx>=0 )
    base::Add(new SentenceParamDistance(dist, GWorld->_martaType[martaTypeIx]._distanceLevels, msg));
  else
    base::Add(new SentenceParamDistance(dist, defaultDistLevels, msg));
}

void SentenceParams::AddDirection(float relDir, float absDir, bool forceCompass, const RadioMessage *msg)
{
  base::Add(new SentenceParamDirection(msg, relDir, absDir, forceCompass));
}

void SentenceParams::AddLocation(const Location *location)
{
  base::Add(new SentenceParamLocation(location));
}

void SentenceParams::AddAggrTargets(AutoArray<AggrTargetWord> &speechList, RString text, const RadioMessage *msg)
{
  base::Add(new SentenceParamAggrTargets(speechList, text, msg));
}

void SentenceParams::AddRelativePosition(Vector3Par dir)
{
  // convert position to relative position
  int azimut = toInt(0.1 * atan2(dir.X(), dir.Z()) * (180 / H_PI));
  if (azimut < 0) azimut += 36;
  Add("%02d", azimut);
  //int dist = toInt(0.1 * dir.SizeXZ());
  //Add(dist);
}

void SentenceParams::AddAzimut(Vector3Par pos)
{
  AIBrain *unit = GWorld->FocusOn();
  // FIXED for dedicated server
  if (!unit)
  {
    AddAzimutRelDir(VForward);
    return;
  }

  AIGroup *grp = unit ? unit->GetGroup() : NULL;
  AIUnit *leader = grp ? grp->Leader() : NULL;
  if (leader)
  {
    Matrix4 transf;
    transf.SetOriented(grp->GetGroupDirection(), VUp);
    transf.SetPosition(leader->Position(leader->GetFutureVisualState()));
    AddAzimutRelDir(transf.InverseRotation().FastTransform(pos));
  }
  else
    AddAzimutRelDir(GWorld->CameraOn()->FutureVisualState().GetInvTransform().FastTransform(pos));
 //	GWorld->UI()->ShowGroupDir();
}

void SentenceParams::AddAzimutDir(Vector3Par dir)
{
  AIBrain *unit = GWorld->FocusOn();
  // FIXED for dedicated server
  if (!unit)
  {
    AddAzimutRelDir(VForward);
    return;
  }

  AIGroup *grp = unit ? unit->GetGroup() : NULL;
  AIUnit *leader = grp ? grp->Leader() : NULL;
  if (leader)
  {
    Matrix3 orient(MDirection, grp->GetGroupDirection(), VUp);
    AddAzimutRelDir(orient.InverseRotation() * dir);
  }
  else
    AddAzimutRelDir(GWorld->CameraOn()->FutureVisualState().GetInvTransform().Rotate(dir));
//  GWorld->UI()->ShowGroupDir();
}

/*!
\patch 1.29 Date 10/31/2001 by Jirka
- Fixed: Group direction shown in MP game
*/
static void ShowGroupDir(const AIGroup *to)
{
  AIBrain *unit = GWorld->FocusOn();
  for (int i=0; i<to->NUnits(); i++)
  {
    AIUnit *u = to->GetUnit(i);
    if (!u) continue;
    if (u == unit)
      GWorld->UI()->ShowGroupDir();
    else if (u->GetPerson()->IsRemotePlayer())
      GetNetworkManager().ShowGroupDir(u->GetPerson(), u->GetGroup()->GetGroupDirection());
  }
}

static void ShowGroupDir(const OLinkPermNOArray(AIUnit) &to)
{
  AIBrain *unit = GWorld->FocusOn();
  for (int i=0; i<to.Size(); i++)
  {
    AIUnit *u = to[i];
    if (!u) continue;
    if (u == unit)
      GWorld->UI()->ShowGroupDir();
    else if (u->GetPerson()->IsRemotePlayer())
      GetNetworkManager().ShowGroupDir(u->GetPerson(), u->GetGroup()->GetGroupDirection());
  }
}

/// list of sentence simple expression variables
#define SENTENCE_SE_VARIABLES(XXX) \
  XXX(Rank, GetSERank) \
  XXX(Captive, GetSECaptive) \
  XXX(Safe, GetSESafe) \
  XXX(Combat, GetSECombat) \
  XXX(Stealth, GetSEStealth) \
  XXX(Morale, GetSEMorale) \
  XXX(Danger, GetSEDanger) \
  XXX(GroupCompactness, GetSEGroupCompactness) \
  XXX(DistanceToUnit, GetSEDistanceToUnit) \
  XXX(DistanceToSender, GetSEDistanceToSender) \
  XXX(DistanceToGroup, GetSEDistanceToGroup) \
  XXX(DistanceToLocation, GetSEDistanceToLocation) \
  XXX(GroupCoreRadius, GetSEGroupCoreRadius) \
  XXX(UnitDistanceFactor, GetSEUnitDistanceFactor) \
  XXX(InsideLocation, GetSEInsideLocation) \
  XXX(MoveToObject, GetSEMoveToObject) \
  XXX(DistanceToRecipients, GetSEDistanceToRecipients) \
  XXX(RecipientsRadius, GetSERecipientsRadius)

#define SENTENCE_SE_VAR_NAME(name, function) #name,
#define SENTENCE_SE_VAR_FUNC(name, function) &RadioMessage::function,

typedef float (RadioMessage::*SimpleExpressionFunc)() const;

// simple expression variables for sentence variants
float RadioMessage::GetSERank() const {if (GetSender()) return GetSender()->GetSERank(); else return 0; }
float RadioMessage::GetSECaptive() const {if (GetSender()) return GetSender()->GetSECaptive(); else return 0; }
float RadioMessage::GetSESafe() const {if (GetSender()) return GetSender()->GetSESafe(); else return 0; }
float RadioMessage::GetSECombat() const {if (GetSender()) return GetSender()->GetSECombat(); else return 0; }
float RadioMessage::GetSEStealth() const {if (GetSender()) return GetSender()->GetSEStealth(); else return 0; }
float RadioMessage::GetSEMorale() const {if (GetSender()) return GetSender()->GetSEMorale(); else return 0; }
float RadioMessage::GetSEDanger() const {if (GetSender()) return GetSender()->GetSEDanger(); else return 0; }

// helper function for SelectVariant
static ParamEntryPtr SelectVariant(RadioMessage *msg, ParamEntryPar cls, float rnd);
static ParamEntryPtr RecurseSelectVariant(RadioMessage *msg, RString sentenceName)
{
  // select radio protocol by the speaker
  Speaker *speaker = msg->GetSpeaker();
  if (speaker)
  {
    RString protocol = speaker->GetProtocol();
    ConstParamEntryPtr protocolCls = protocol.GetLength() > 0 ? Pars.FindEntry(protocol) : ParamEntryPtr(NULL);
    if (protocolCls) 
    { 
      ParamEntryPtr sentence = protocolCls->FindEntry(sentenceName);
      if (sentence) return SelectVariant(msg, (*sentence), GRandGen.RandomValue());
    }
  }
  return ParamEntryPtr();
}

#define DIAG_RECURSIVE_SENTENCES 0
static ParamEntryPtr SelectVariant(RadioMessage *msg, ParamEntryPar cls, float rnd)
{
  // Check for recursive sentence selection
  ParamEntryPtr versions = cls.FindEntry("versions");
  bool recurse = false;
  if (!versions)
  {
    recurse = true;
    versions = cls.FindEntry("sentences");
  }
  if (versions)
  {
    // TODO: precompile weight expressions (bank/cache of sentences?)
    int n = versions->GetSize() / 2;
    if (n * 2 != versions->GetSize())
    {
      RptF("Sentence %s - size of variants should be even", cc_cast(cls.GetName()));
      return ParamEntryPtr();
    }

    // names of simple expression variables
    static RString varNames[] =
    {
      SENTENCE_SE_VARIABLES(SENTENCE_SE_VAR_NAME)
    };

    // prepare all simple expression variables
    const int nValues = lenof(varNames);
    float varValues[nValues];
    bool  varInitialized[nValues];
    for (int i=0; i<nValues; i++) varInitialized[i] = false;
    SimpleExpressionFunc varFunc[] = 
    {
      SENTENCE_SE_VARIABLES(SENTENCE_SE_VAR_FUNC)
    };

    // collect weights of variants
    float totalWeight = 0;
    AutoArray< float, MemAllocLocal<float, 16> > weights;
    weights.Resize(n);
    for (int i=0; i<n; i++)
    {
      RString expText = (*versions)[2 * i + 1];
      ExpressionCode exp;
      bool ok = exp.Compile(expText, varNames, lenof(varNames));
      if (!ok)
      {
        RptF("Error in expression %s - sentence %s", cc_cast(expText), cc_cast(versions->GetName()));
        weights[i] = 0;
        continue;
      }
      ExpressionCodeVars vars = exp.GetUsedVariables();
      for (int i=0; i<vars.Size(); i++)
      {
        int vi = vars[i];
        if (!varInitialized[vi]) 
        {
          varValues[vi] = msg ? (msg->*varFunc[vi])() : NULL;
          varInitialized[vi] = true;
        }
      }

      float weight;
      ok = exp.Evaluate(weight, varValues, lenof(varValues));  
      if (ok)
      {
        weights[i] = weight;
        totalWeight += weight;
      }
      else weights[i] = 0;
    }
    if (totalWeight <= 0) return ParamEntryPtr();

    // find where weighted random number fit
    float value = rnd * totalWeight;
#if DIAG_RECURSIVE_SENTENCES
    if (recurse)
    {
      LogF("*** RecurseSelectVariant:");
      for (int i=0; i<n; i++)
      {
        LogF("  %s = %f", cc_cast((*versions)[2*i].GetValue()), weights[i]);
      }
    }
#endif
    for (int i=0; i<n-1; i++)
    {
      value -= weights[i];
      if (value <= 0)
      {
        RStringB version = (*versions)[2 * i];
        if (version.IsEmpty()) return ParamEntryPtr();
        if (recurse) return RecurseSelectVariant(msg, version);
        else
        {
          return cls.FindEntry(version);
        }
      }
    }
    RStringB version = (*versions)[2 * (n - 1)];
    if (version.IsEmpty()) return ParamEntryPtr();
    if (recurse) return RecurseSelectVariant(msg, version);
    else
    {
      return cls.FindEntry(version);
    }
  }
  return ParamEntryPtr();
}

void SentenceParamText::Say(RadioSentence &sentence, const RadioChannelParams &cfg, int mode) const
{
  Spell(sentence, _text, cfg.pauseAfterWord, cfg);
}

RString SentenceParamText::Write(int mode) const
{
  return _text;
}

DEFINE_FAST_ALLOCATOR(SentenceParamText)

void SentenceParamNumber::Say(RadioSentence &sentence, const RadioChannelParams &cfg, int mode) const
{
  SayNumber(sentence, _number, cfg.pauseAfterWord, cfg, "%d", mode);
}

RString SentenceParamNumber::Write(int mode) const
{
  return Format("%d", _number);
}

DEFINE_FAST_ALLOCATOR(SentenceParamNumber)

void SentenceParamFormat::Say(RadioSentence &sentence, const RadioChannelParams &cfg, int mode) const
{
  SayNumber(sentence, _number, cfg.pauseAfterWord, cfg, _format, mode);
}

RString SentenceParamFormat::Write(int mode) const
{
  return Format(_format, _number);
}

DEFINE_FAST_ALLOCATOR(SentenceParamFormat)

void SentenceParamUnit::Say(RadioSentence &sentence, const RadioChannelParams &cfg, int mode) const
{
  SayUnit(sentence, _unit, mode, cfg.pauseAfterUnitList, cfg);
}

RString SentenceParamUnit::Write(int mode) const
{
  if (_unit)
  {
    if (mode == 1)
    {
      AIUnit *unit = _unit->GetUnit();
      return Format("%d", unit ? unit->ID() : 0);
    }
    else
    {
      Fail("Mode");
    }
  }
  return RString();
}

DEFINE_FAST_ALLOCATOR(SentenceParamUnit)

SentenceParamUnitList::SentenceParamUnitList(AISubgroup *subgrp, bool wholeCrew, bool avoidTeams)
{
  if (subgrp) subgrp->GetUnitsList(_list);
  _wholeCrew = wholeCrew;
  _avoidTeams = avoidTeams;
}

void SentenceParamUnitList::Say(RadioSentence &sentence, const RadioChannelParams &cfg, int mode) const
{
  OLinkPermNOArray(AIUnit) list;
  PrepareList(list, _list, _wholeCrew);
  if (AllUnits(list, _wholeCrew))
  {
    sentence.Add("all", cfg.pauseAfterUnitList);
  }
  else
  {
    int team = WholeTeam(list, _wholeCrew, _avoidTeams);
    if (team >= 0)
    {
      switch (team)
      {
      case TeamMain:
        sentence.Add("whiteTeam", cfg.pauseAfterUnitList);
        break;
      case TeamRed:
        sentence.Add("redTeam", cfg.pauseAfterUnitList);
        break;
      case TeamGreen:
        sentence.Add("greenTeam", cfg.pauseAfterUnitList);
        break;
      case TeamBlue:
        sentence.Add("blueTeam", cfg.pauseAfterUnitList);
        break;
      case TeamYellow:
        sentence.Add("yellowTeam", cfg.pauseAfterUnitList);
        break;
      }
    }
    else
    {
      SayUnits(sentence, list, mode, cfg.pauseAfterUnitList, cfg);
    }
  }
}

RString SentenceParamUnitList::Write(int mode) const
{
  OLinkPermNOArray(AIUnit) list;
  PrepareList(list, _list, _wholeCrew);
  if (AllUnits(list, _wholeCrew))
  {
    return LocalizeString(IDS_WORD_ALL);
  }
  else
  {
    int team = WholeTeam(list, _wholeCrew, _avoidTeams);
    if (team >= 0)
    {
      switch (team)
      {
      case TeamMain:
        return LocalizeString(IDS_WORD_TEAM_MAIN);
      case TeamRed:
        return LocalizeString(IDS_WORD_TEAM_RED);
      case TeamGreen:
        return LocalizeString(IDS_WORD_TEAM_GREEN);
      case TeamBlue:
        return LocalizeString(IDS_WORD_TEAM_BLUE);
      case TeamYellow:
        return LocalizeString(IDS_WORD_TEAM_YELLOW);
      default:
        Fail("Team");
        return RString();
      }
    }
    else if (mode == 1)
    {
      char buffer[256];
      CreateUnitsList(list, buffer,sizeof(buffer));
      return buffer;
    }
    else
    {
      Fail("Mode");
      return RString();
    }
  }
}

DEFINE_FAST_ALLOCATOR(SentenceParamUnitList)

void SentenceParamWordText::Say(RadioSentence &sentence, const RadioChannelParams &cfg, int mode) const
{
  sentence.Add(_say, cfg.pauseAfterWord);
}

RString SentenceParamWordText::Write(int mode) const
{
  return _write;
}

DEFINE_FAST_ALLOCATOR(SentenceParamWordText)

RString SentenceParamPhrase::Write(int mode) const
{
  return _write;
}

void SentenceParamPhrase::Say(RadioSentence &sentence, const RadioChannelParams &cfg, int mode) const
{
  for (int i=0; i<_say.Size(); i++)
    sentence.Add(_say[i], cfg.pauseAfterWord);
}

DEFINE_FAST_ALLOCATOR(SentenceParamPhrase)

#if _ENABLE_INDEPENDENT_AGENTS

void SentenceParamTeamMember::Say(RadioSentence &sentence, const RadioChannelParams &cfg, int mode) const
{
  if (_member)
  {
    bool ok = false;
    AutoArray<RString, MemAllocLocal<RString, 16> > words;
    switch (mode)
    {
    case 1:
      // short call sign
      ok = _member->GetCallSignShortSpeech(words);
      break;
    case 2:
      // call sign
      ok = _member->GetCallSignSpeech(words);
      break;
    }
    if (ok)
    {
      for (int i=0; i<words.Size(); i++)
        sentence.Add(words[i], cfg.pauseAfterWord);
    }
  }
}

RString SentenceParamTeamMember::Write(int mode) const
{
  if (_member)
  {
    switch (mode)
    {
    case 1:
      // short call sign
      return _member->GetCallSignShort();
    case 2:
      // call sign
      return _member->GetCallSign();
    }
  }
  return RString();
}

DEFINE_FAST_ALLOCATOR(SentenceParamTeamMember)

static bool GetMemberPos(Vector3 &ret, AITeamMember *member)
{
  if (!member) return false;
  AIAgent *agent = member->GetAgent();
  while (!agent)
  {
    AITeam *team = member->GetTeam();
    if (!team) return false;
    member = team->GetLeader();
    if (!member) return false;
    agent = member->GetAgent();
  }
  ret = agent->Position(agent->GetFutureVisualState());
  return true;
}

static RString GetRadioProtocol(AITeamMember *member)
{
  if (!member) return RString();
  AIAgent *agent = member->GetAgent();
  while (!agent)
  {
    AITeam *team = member->GetTeam();
    if (!team) return RString();
    member = team->GetLeader();
    if (!member) return RString();
    agent = member->GetAgent();
  }

  // speaker is known now
  Speaker *speaker = agent->GetSpeaker();
  if (!speaker) return RString();
  
  return speaker->GetProtocol();
}

static void SaySentence
(
  RadioSentence &sentence,
  ParamEntryPar clsVariant, SentenceParams &params,
  float pauseAfterMessage, const RadioChannelParams &cfg
);

void SentenceParamGameValue::Say(RadioSentence &sentence, const RadioChannelParams &cfg, int mode) const
{
  switch (mode)
  {
  case 1: // position
    {
      bool GetPos(const GameState *state, Vector3 &ret, GameValuePar oper);
      Vector3 pos, posS, posR;
      if (GetPos(GWorld->GetGameState(), pos, _value) && GetMemberPos(posS, _sender) && GetMemberPos(posR, _receiver))
      {
        const float limit2 = Square(400);
        if (pos.Distance2(posS) > limit2 || pos.Distance2(posR) > limit2)
        {
          // position in the map
          //VBS supports bigger grids
          char buffer[32];
          PositionToAA11(pos, buffer);
          Spell(sentence, buffer, cfg.pauseAfterWord, cfg);
        }
        else
        {
          // find correct radio protocol
          RString protocol = GetRadioProtocol(_sender);
          ConstParamEntryPtr protocolCls = protocol.GetLength() > 0 ? Pars.FindEntry(protocol) : ParamEntryPtr(NULL);
          if (protocolCls)
          {
            // position in the world
            Object *obj = FindNearestMoveObject(pos);
            if (obj)
            {
              ConstParamEntryPtr cls = protocolCls->FindEntry("ClausePositionNear");
              if (cls)
              {
                SentenceParams params;
                params.AddWord(obj->GetNameSound(), obj->GetShortName());
                params.AddAzimut(obj->FutureVisualState().Position());
                SaySentence(sentence, *cls, params, 0, cfg);
              }
            }
            else
            {
              ConstParamEntryPtr cls = protocolCls->FindEntry("ClausePosition");
              if (cls)
              {
                SentenceParams params;
                params.AddAzimut(pos);
                Vector3 dir = pos - (GWorld->CameraOn() ? GWorld->CameraOn()->FutureVisualState().Position() : posS);
                RString distance = Format("%d", toInt(0.1 * dir.SizeXZ()));
                params.Add(distance);
                SaySentence(sentence, *cls, params, 0, cfg);
              }
            }
          }
        }
      }
    }
    break;
  }
}

static RString WriteSentence(RString format, SentenceParams &params);

RString SentenceParamGameValue::Write(int mode) const
{
  switch (mode)
  {
  case 1: // position
    {
      bool GetPos(const GameState *state, Vector3 &ret, GameValuePar oper);
      Vector3 pos, posS, posR;
      if (GetPos(GWorld->GetGameState(), pos, _value) && GetMemberPos(posS, _sender) && GetMemberPos(posR, _receiver))
      {
        const float limit2 = Square(400);
        if (pos.Distance2(posS) > limit2 || pos.Distance2(posR) > limit2)
        {
          // position in the map
          //VBS supports bigger grids
          char buffer[32];
          PositionToAA11(pos, buffer);
          return buffer;
        }
        else
        {
          // find correct radio protocol
          RString protocol = GetRadioProtocol(_sender);
          ConstParamEntryPtr protocolCls = protocol.GetLength() > 0 ? Pars.FindEntry(protocol) : ParamEntryPtr(NULL);
          if (protocolCls)
          {
            // position in the world
            Object *obj = FindNearestMoveObject(pos);
            if (obj)
            {
              ConstParamEntryPtr cls = protocolCls->FindEntry("ClausePositionNear");
              if (cls)
              {
                SentenceParams params;
                params.AddWord(obj->GetNameSound(), obj->GetShortName());
                params.AddAzimut(obj->FutureVisualState().Position());
                return WriteSentence((*cls) >> "text", params);
              }
            }
            else
            {
              ConstParamEntryPtr cls = protocolCls->FindEntry("ClausePosition");
              if (cls)
              {
                SentenceParams params;
                params.AddAzimut(pos);
                Vector3 dir = pos - (GWorld->CameraOn() ? GWorld->CameraOn()->FutureVisualState().Position() : posS);
                RString distance = Format("%d", toInt(0.1 * dir.SizeXZ()));
                params.Add(distance);
                return WriteSentence((*cls) >> "text", params);
              }
            }
          }
        }
      }
    }
  }
  return RString();
}

DEFINE_FAST_ALLOCATOR(SentenceParamGameValue)

#endif

static void LoadSpeech(AutoArray<RString, MemAllocLocal<RString, 16> > &result, ParamEntryVal entry)
{
  Assert(entry.IsArray());
  int n = entry.GetSize();
  result.Realloc(n);
  result.Resize(n);
  for (int i=0; i<n; i++) result[i] = entry[i];
}

ParamEntryPtr SentenceParamDistance::GetVariantCls(int mode) const
{
  Speaker *speaker = _msg->GetSpeaker();
  if (speaker)
  {
    RString protocol = speaker->GetProtocol();
    ConstParamEntryPtr protocolCls = protocol.GetLength() > 0 ? Pars.FindEntry(protocol) : ParamEntryPtr(NULL);
    if (protocolCls) 
    { 
      ParamEntryPtr cls = protocolCls->FindEntry("Arguments");
      if (!cls) return ParamEntryPtr();
      char buffer[32];
      sprintf(buffer, "Distance%d", mode%100);
      cls = cls->FindEntry(buffer);
      if (!cls) return ParamEntryPtr();
      if (_distance < _distLevels[0]) return cls->FindEntry("Level0");
      else if (_distance < _distLevels[1]) return cls->FindEntry("Level1");
      else return cls->FindEntry("Level2");
    }
  }
  return ParamEntryPtr();
}

RString SentenceParamDistance::Write(int mode) const
{
  if (mode/100==ModeDistMeters)
  { // use meters in destination
    int dist100 = toInt(_distance*0.01);
    const int maxDist = sizeof(distWrite)/sizeof(*distWrite)-1;
    saturate(dist100,0,maxDist);
    return LocalizeString(*distWrite[dist100]);
  }
  else
  {
    ParamEntryPtr cls = GetVariantCls(mode);
    if (cls)
    {
      return (*cls) >> "text";
    }
  }
  return RString();
}

void SentenceParamDistance::Say(RadioSentence &sentence, const RadioChannelParams &cfg, int mode) const
{
  if (mode/100==ModeDistMeters)
  { // use meters in destination
    int dist100 = toInt(_distance*0.01);
    const int maxDist = sizeof(distSay)/sizeof(*distSay)-1;
    saturate(dist100,0,maxDist);
    sentence.Add(distSay[dist100], cfg.pauseAfterWord);
  }
  else
  {
    ParamEntryPtr cls = GetVariantCls(mode);
    if (cls)
    {
      AutoArray<RString, MemAllocLocal<RString, 16> > words;
      LoadSpeech(words, (*cls) >> "speech");
      for (int i=0; i<words.Size(); i++)
        sentence.Add(words[i], cfg.pauseAfterWord);
    }
  }
}

DEFINE_FAST_ALLOCATOR(SentenceParamDistance)

RString SentenceParamLocation::Write(int mode) const
{
  return _location ? _location->_text : RString("");
}

void SentenceParamLocation::Say(RadioSentence &sentence, const RadioChannelParams &cfg, int mode) const
{
#if _ENABLE_CONVERSATION
  if (_location)
  {
    for (int i=0; i<_location->_speech.Size(); i++)
      sentence.Add(_location->_speech[i], cfg.pauseAfterWord);
  }
#endif
}

DEFINE_FAST_ALLOCATOR(SentenceParamLocation)

RString SentenceParamAggrTargets::GetVariantCls(int mode) const
{
  RString varDefault("Default");
  Speaker *speaker = _msg->GetSpeaker();
  if (speaker)
  {
    RString protocol = speaker->GetProtocol();
    ConstParamEntryPtr protocolCls = protocol.GetLength() > 0 ? Pars.FindEntry(protocol) : ParamEntryPtr(NULL);
    if (protocolCls)
    { 
      ParamEntryPtr cls = protocolCls->FindEntry("Arguments");
      if (!cls) return varDefault;
      if (cls->IsClass())
      {
        cls = cls->FindEntry("AggrTargets");
        if (!cls) return varDefault;
        cls = cls->FindEntry("modes");
        if (!cls || !cls->IsArray()) return varDefault;
        if (mode>cls->GetSize()) mode=1;
        mode--; //make the mode an index into the array
        return (*cls)[mode];
      }
    }
  }
  return varDefault;
}

RString SentenceParamAggrTargets::Write(int mode) const
{
  return _text;
}

void SentenceParamAggrTargets::Say(RadioSentence &sentence, const RadioChannelParams &cfg, int mode) const
{
  // TODO: use mode to select specific speech variant
  for (int tn=0; tn<_speechList.Size(); tn++)
  {
    const EntitySpeechVariant *variant = _speechList[tn]._type->GetSpeechVariant(GetVariantCls(mode));
    const AutoArray<RString> &words = _speechList[tn]._singular ?
      (variant ? variant->_speechSingular : _speechList[tn]._type->GetSpeechSingular() ) : 
      (variant ? variant->_speechPlural : _speechList[tn]._type->GetSpeechPlurar() );
    if (tn>0) sentence.Add(RString("and"), cfg.pauseAfterWord); //note: 'and' should be defined in RadioProtocolBase.Words
    for (int i=0; i < words.Size(); i++)
      sentence.Add(words[i], cfg.pauseAfterWord);
  }
#if DIAG_AGGREGATED_TARGETS
  RString log;
  for (int i=0; i < sentence.Size(); i++)
    log = log + sentence[i].id + RString(" ");
  LogF("  AggrTargets::Say: %s", cc_cast(log));
#endif
}

DEFINE_FAST_ALLOCATOR(SentenceParamAggrTargets)

ParamEntryPtr SentenceParamDirection::GetVariantCls(int mode) const
{
  Speaker *speaker = _msg->GetSpeaker();
  if (speaker)
  {
    RString protocol = speaker->GetProtocol();
    ConstParamEntryPtr protocolCls = protocol.GetLength() > 0 ? Pars.FindEntry(protocol) : ParamEntryPtr(NULL);
    if (protocolCls) 
    { 
      ParamEntryPtr cls = protocolCls->FindEntry("Arguments");
      if (!cls) return ParamEntryPtr();
      cls = cls->FindEntry("Direction");
      if (!cls) return ParamEntryPtr();
      if (_forceCompass || (mode/100==ModeDirCompass))
      {
        char buffer[32];
        sprintf(buffer, "Compass%d", mode%100);
        cls = cls->FindEntry(buffer);
        if (!cls) return ParamEntryPtr();
        int azimuth = toInt(_absDir*4/H_PI)*45; //azimuth using 45 degrees step
        if (azimuth<0) azimuth+=360;
        azimuth %= 360;
        sprintf(buffer, "%d", azimuth);
        return cls->FindEntry(buffer);
      }
      else // use left/right/front/rear side directions
      {
        char buffer[32];
        sprintf(buffer, "Relative%d", mode%100);
        cls = cls->FindEntry(buffer);
        if (!cls) return ParamEntryPtr();
        int azimuth = toInt(_relDir*2/H_PI)*90; //azimuth using 90 degrees step
        if (azimuth<0) azimuth+=360;
        azimuth %= 360;
        sprintf(buffer, "%d", azimuth);
        return cls->FindEntry(buffer);
      }
    }
  }
  return ParamEntryPtr();
}

RString SentenceParamDirection::Write(int mode) const
{
  if (!_forceCompass && (mode/100==ModeDirHours))
  { // use hours as dir
    int azimuth = toInt(_relDir*6/H_PI); //azimuth using hours
    if (azimuth<0) azimuth+=12;
    if (azimuth>=0 && azimuth<=12)
    {
      return LocalizeString(*azimWrite[azimuth]);
    }
  }
  else
  {
    ParamEntryPtr cls = GetVariantCls(mode);
    if (cls)
    {
      return (*cls) >> "text";
    }
  }
  return RString();
}

void SentenceParamDirection::Say(RadioSentence &sentence, const RadioChannelParams &cfg, int mode) const
{
  if (!_forceCompass && (mode/100==ModeDirHours) )
  { // use hours as dir
    int azimuth = toInt(_relDir*6/H_PI); //azimuth using hours
    if (azimuth<0) azimuth+=12;
    if (azimuth>=0 && azimuth<=12)
    {
      sentence.Add(azimSay[azimuth], cfg.pauseAfterNumber);
    }
  }
  else
  {
    ParamEntryPtr cls = GetVariantCls(mode);
    if (cls)
    {
      AutoArray<RString, MemAllocLocal<RString, 16> > words;
      LoadSpeech(words, (*cls) >> "speech");
      for (int i=0; i<words.Size(); i++)
        sentence.Add(words[i], cfg.pauseAfterWord);
    }
  }
}

DEFINE_FAST_ALLOCATOR(SentenceParamDirection)

static void SaySentence
(
  RadioSentence &sentence,
  ParamEntryPar clsVariant, SentenceParams &params,
  float pauseAfterMessage, const RadioChannelParams &cfg
)
{
  ParamEntryVal cls = clsVariant >> "speech";

  ConstParamEntryPtr wordsClass = clsVariant.FindEntry("wordsClass");
  if (wordsClass)
  {
    sentence.wordsClass = *wordsClass;
  }

  int n = cls.GetSize();
  int nParams = params.Size();
  for (int i=0; i<n; i++)
  {
    RString word = cls[i];
    if (!word || word.GetLength() == 0) continue;
    if (word[0] == '\'') continue;
    if (word[0] == '%')
    {
      if (word[1] == 'q')
      {
        // do not say MicOut
        // do not wait
        return;
      }
      else
      {
        const char *format = word;
        format++;
        int index = 0;
        int mode = 1;
        while (isdigit(*format))
        {
          index *= 10;
          index += *format - '0';
          format++;
        }
        index--;
        if (*format == '.' && isdigit(*(format + 1)))
        {
          format++;
          mode = 0;
          do
          {
            mode *= 10;
            mode += *format - '0';
            format++;
          } while (isdigit(*format));
        }
        if (index < 0 || index >= nParams) continue;

        params[index]->Say(sentence, cfg, mode);
      }
    }
    else
      sentence.Add(word, cfg.pauseAfterWord);
  }

  sentence.Add("xmit", 0);
}

#if !defined _WIN32 || defined _XBOX
#include <wctype.h>
int CharUpperBuffW(wchar_t *text, int len)
{
  for (int i=0; i<len; i++) 
  {
    *text = towupper(*text);
    text++;
  }
  return len; //There is no return value reserved to indicate an error (from: man towupper)
}
#endif

//! Function to perform uppercase in unicode space
RString StrUprUniversal(const char *text)
{
  // Convert UTF8 to unicode
  TempWString<> unicodeText;
  {
    int bufferSize = MultiByteToWideChar(CP_UTF8, 0, text, -1, NULL, 0);
    unicodeText.Resize(bufferSize);
    MultiByteToWideChar(CP_UTF8, 0, text, -1, unicodeText.Data(), unicodeText.Size());
  }

  // Make the text uppercase
  CharUpperBuffW(unicodeText.Data(), unicodeText.Size());
  // Test - make only the first character uppercase
  // if (unicodeText.Size() > 0) CharUpperBuffW(unicodeText.Data(), 1);

  // Convert unicode back to UTF8
  RString textUTF8;
  {
    int dstLen = WideCharToMultiByte(CP_UTF8, 0, unicodeText.Data(), unicodeText.Size(), NULL, 0, NULL, NULL);
    char *data = textUTF8.CreateBuffer(dstLen);
    WideCharToMultiByte(CP_UTF8, 0, unicodeText.Data(), unicodeText.Size(), data, dstLen, NULL, NULL);
  }

  // Return UTF8 text
  return textUTF8;
}

static RString WriteSentence(RString format, SentenceParams &params)
{
  int nParams = params.Size();
  const int resultSize = 2048;
  char result[2048], c;
  const char *src = format;
  int len = 0;

  while ((c = *src)!=0)
  {
    if (c == '%')
    {
      src++;
      int index = 0;
      int mode = 1;
      while (isdigit(*src))
      {
        index *= 10;
        index += *src - '0';
        src++;
      }
      index--;
      if (*src == '.' && isdigit(*(src + 1)))
      {
        src++;
        mode = 0;
        do
        {
          mode *= 10;
          mode += *src - '0';
          src++;
        } while (isdigit(*src));
      }
      if (index < 0 || index >= nParams) continue;

      RString text = params[index]->Write(mode);
      int n = text.GetLength();
      if (len + n < resultSize)
      {
        strcpy(result + len, text);
        len += n;
      }
    }
    else
    {
      if (len + 1 >= resultSize) break;
      *(result + len) = c;
      src++;
      len++;
    }
  }
  *(result + len) = 0;

  return StrUprUniversal(result);
}

const float coefAa11 = 100.0 / (256*50);
const float invCoefAa11 = 1.0 / coefAa11;

///////////////////////////////////////////////////////////////////////////////
// class RadioMessage

static const EnumName RadioMessageTypeNames[]=
{
  EnumName(RMTCommand, "COMMAND"),
  EnumName(RMTFormation, "FORM"),
  EnumName(RMTBehaviour, "BEHAVIOUR"),
  EnumName(RMTOpenFire, "OPEN FIRE"),
  EnumName(RMTCeaseFire, "CEASE FIRE"),
  EnumName(RMTLooseFormation, "LOOSE FORM"),
  EnumName(RMTReportStatus, "STATUS"),
  EnumName(RMTTarget, "TARGET"),
  EnumName(RMTSubgroupAnswer, "SUBGRP ANS"),
  EnumName(RMTReturnToFormation, "RETURN TO FORM"),
  EnumName(RMTUnitAnswer, "UNIT ANS"),
  EnumName(RMTFireStatus,"FIRE STATUS"),
  EnumName(RMTUnitKilled, "UNIT KILLED"),
  EnumName(RMTText, "TEXT"),
  EnumName(RMTReportTarget, "REPORT TARGET"),
  EnumName(RMTObjectDestroyed, "OBJ DESTROYED"),
  EnumName(RMTContact, "CONTACT"),
  EnumName(RMTUnderFire, "UNDER FIRE"),
  EnumName(RMTClear, "CLEAR"),
  EnumName(RMTRepeatCommand, "REPEAT COMMAND"),
  EnumName(RMTWhereAreYou, "WHERE ARE YOU"),
  EnumName(RMTNotifyCommand, "NOTIFY"),
  EnumName(RMTCommandConfirm, "CONFIRM"),
  EnumName(RMTPosition, "POSITION"),
  EnumName(RMTFormationPos, "FORM POS"),
  EnumName(RMTTeam, "TEAM"),
  EnumName(RMTSupportAsk, "SUPPORT ASK"),
  EnumName(RMTSupportConfirm, "SUPPORT CONFIRM"),
  EnumName(RMTSupportReady, "SUPPORT READY"),
  EnumName(RMTSupportDone, "SUPPORT DONE"),
  EnumName(RMTJoin, "JOIN"),
  EnumName(RMTJoinDone, "JOIN DONE"),
  EnumName(RMTTalk, "TALK"),
  EnumName(RMTSupportNotAvailable, "SUPPORT NOT AVAILABLE"),
  EnumName(RMTTask, "TASK"),
  EnumName(RMTTaskReceived, "TASK RECEIVED"),
  EnumName(RMTTaskResult, "TASK RESULT"),
  EnumName(RMTWatchAround, "WATCH AROUND"),
  EnumName(RMTWatchDir, "WATCH DIR"),
  EnumName(RMTWatchPos, "WATCH POS"),
  EnumName(RMTWatchTgt, "WATCH TGT"),
  EnumName(RMTWatchAuto, "WATCH AUTO"),
  EnumName(RMTVehicleMove, "V MOVE"),
  EnumName(RMTVehicleFire, "V FIRE"),
  EnumName(RMTVehicleFormation, "V FORM"),
  EnumName(RMTVehicleSimpleCommand, "V SIMPLE COMMAND"),
  EnumName(RMTVehicleTarget, "V TARGET"),
  EnumName(RMTVehicleLoad, "V LOAD"),
  EnumName(RMTVehicleAzimut, "V AZIMUT"),
  EnumName(RMTVehicleStopTurning, "V STOP TURNING"),
  EnumName(RMTVehicleFireFailed, "V FIRE FAILED"),
  EnumName(RMTVehicleLoadMagazine, "V LOAD MAGAZINE"),
  EnumName(RMTVehicleWatchTgt, "V WATCH TGT"),
  EnumName(RMTVehicleWatchPos, "V WATCH POS"),
  EnumName(RMTCovering, "COVERING"),
  EnumName(RMTCoverMe, "COVER ME"),
  EnumName(RMTSuppress, "SUPPRESS"),
  EnumName()
};
template<>
const EnumName *GetEnumNames(RadioMessageType dummy)
{
  return RadioMessageTypeNames;
}

static const EnumName AnswerNames[]=
{
  EnumName(AI::NoAnswer, "NO ANSWER"),
  EnumName(AI::UnitDestroyed, "DESTROYED"),
  EnumName(AI::HealthCritical, "HEALTH CRIT"),
  EnumName(AI::DamageCritical, "DAMM CRIT"),
  EnumName(AI::FuelCritical, "FUEL CRIT"),
  EnumName(AI::ReportPosition, "REPORT POS"),
  EnumName(AI::ReportSemaphore, "REPORT SEM"),
  EnumName(AI::AmmoCritical, "AMMO CRIT"),
  EnumName(AI::FuelLow, "FUEL LOW"),
  EnumName(AI::AmmoLow, "AMMO LOW"),
  EnumName(AI::IsLeader, "LEADER"),
  EnumName(AI::CommandCompleted, "COMM OK"),
  EnumName(AI::CommandFailed, "COMM FAIL"),
  EnumName(AI::SubgroupDestinationUnreacheable, "SUBGRP UNREACH"),
  EnumName(AI::MissionCompleted, "MISS OK"),
  EnumName(AI::MissionFailed, "MISS FAIL"),
  EnumName(AI::DestinationUnreacheable, "UNREACH"),
  EnumName(AI::GroupDestroyed, "GRP DESTROYED"),
  EnumName()
};
template<>
const EnumName *GetEnumNames(AI::Answer dummy)
{
  return AnswerNames;
}

static const EnumName ReportSubjectNames[]=
{
  EnumName(ReportNew, "NEW"),
  EnumName(ReportDestroy, "DESTROY"),
  EnumName()
};
template<>
const EnumName *GetEnumNames(ReportSubject dummy)
{
  return ReportSubjectNames;
}

LSError ReportTargetInfo::Serialize(ParamArchive &ar)
{
  CHECK(ar.SerializeRef("Target", tgt, 1))
  return LSOK;
}

RadioMessage::RadioMessage()
{
  _transmitted = false;
  _playerMsg = false;
}

float RadioMessage::GetDuration() const
{
  return 1.0;
}

Speaker *RadioMessage::GetSpeaker() const
{
  AIBrain *sender = GetSender();
  return sender ? sender->GetSpeaker() : NULL;
}

bool RadioMessage::CanSenderSpeak() const
{
  if (GetSenderName().GetLength() > 0) return true; // HQ can always speak

  // real person is speaking
  AIBrain *sender = GetSender();
  return sender && sender->LSCanSpeak();
}

RString RadioMessage::GetDebugName() const
{
  SentenceParams params;
  RString debugName = PrepareSentence(params);
  if (debugName.GetLength() == 0) debugName = unconst_cast(this)->GetWave();
  AIBrain *sender = GetSender();
  return debugName + " -  " + RString(sender ? sender->GetDebugName() : "<null>");
}

static RadioMessage *CreateRadioMessage(RadioMessageType type)
{
  if (type >= (RadioMessageType)RMTFirstVehicle)
    return CreateVehicleMessage(type);
  else switch (type)
  {
  case RMTCommand:
    return new RadioMessageCommand();
  case RMTFormation:
    return new RadioMessageFormation();
  case RMTBehaviour:
    return new RadioMessageBehaviour();
  case RMTOpenFire:
    return new RadioMessageOpenFire();
  case RMTCeaseFire:
    return new RadioMessageCeaseFire();
  case RMTLooseFormation:
    return new RadioMessageLooseFormation();
  case RMTReportStatus:
    return new RadioMessageReportStatus();
  case RMTTarget:
    return new RadioMessageTarget();
  case RMTSubgroupAnswer:
    return new RadioMessageSubgroupAnswer();
  case RMTReturnToFormation:
    return new RadioMessageReturnToFormation();
  case RMTUnitAnswer:
    return new RadioMessageUnitAnswer();
  case RMTFireStatus:
    return new RadioMessageFireStatus();
  case RMTArtilleryFireStatus:
    return new RadioMessageArtilleryFireStatus();
  case RMTUnitKilled:
    return new RadioMessageUnitKilled();
  case RMTText:
    return new RadioMessageText();
  case RMTReportTarget:
    return new RadioMessageReportTarget();
  case RMTObjectDestroyed:
    return new RadioMessageObjectDestroyed();
  case RMTContact: return new RadioMessageContact();
  case RMTUnderFire: return new RadioMessageUnderFire();
  case RMTCoverMe: return new RadioMessageCoverMe();
  case RMTCovering: return new RadioMessageCovering();
  case RMTSuppress: return new RadioMessageSuppress();
  case RMTClear:
    return new RadioMessageClear();
  case RMTRepeatCommand:
    return new RadioMessageRepeatCommand();
  case RMTWhereAreYou:
    return new RadioMessageWhereAreYou();
  case RMTNotifyCommand:
    return new RadioMessageNotifyCommand();
  case RMTCommandConfirm:
    return new RadioMessageCommandConfirm();
  case RMTWatchAround:
    return new RadioMessageWatchAround();
  case RMTWatchDir:
    return new RadioMessageWatchDir();
  case RMTWatchPos:
    return new RadioMessageWatchPos();
  case RMTWatchTgt:
    return new RadioMessageWatchTgt();
  case RMTWatchAuto:
    return new RadioMessageWatchAuto();
  case RMTPosition:
    return new RadioMessageUnitPos();
  case RMTFormationPos:
    return new RadioMessageFormationPos();
  case RMTTeam:
    return new RadioMessageTeam();
  case RMTSupportAsk:
    return new RadioMessageSupportAsk();
  case RMTSupportConfirm:
    return new RadioMessageSupportConfirm();
  case RMTSupportReady:
    return new RadioMessageSupportReady();
  case RMTSupportDone:
    return new RadioMessageSupportDone();
  case RMTJoin:
    return new RadioMessageJoin();
  case RMTJoinDone:
    return new RadioMessageJoinDone();
#if _ENABLE_CONVERSATION
  case RMTTalk:
    return new RadioMessageTalk();
#endif
  case RMTSupportNotAvailable:
    return new RadioMessageSupportNotAvailable();
#if _ENABLE_IDENTITIES
  case RMTTask:
    return new RadioMessageTask();
  case RMTTaskReceived:
    return new RadioMessageTaskReceived();
  case RMTTaskResult:
    return new RadioMessageTaskResult();
#endif
  }
  ErrF("Uknown radio message Type %d",type);
  return NULL;
}

RadioMessage *RadioMessage::CreateObject(ParamArchive &ar)
{
  RadioMessageType type;
  if (ar.SerializeEnum("type", type, 1) != LSOK) return NULL;
  return CreateRadioMessage(type);
}

LSError RadioMessage::Serialize(ParamArchive &ar)
{
  if (ar.IsSaving())
  {
    RadioMessageType type = (RadioMessageType)GetType();
    CHECK(ar.SerializeEnum("type", type, 1))
  }
  CHECK(ar.Serialize("priority", _priority, 1, 0))
  CHECK(::Serialize(ar, "timeout", _timeOut, 1))
  CHECK(ar.Serialize("playerMsg", _playerMsg, 1, false))
  return LSOK;
}

///////////////////////////////////////////////////////////////////////////////
// class RadioChannel

RadioMessage *RadioChannel::CreateMessage(int type)
{
  return CreateRadioMessage((RadioMessageType)type);
}

void RadioSentence::Say(Person *who, RadioChannel *channel, Speaker *speaker)
{
  bool transmit = false;
  for (int i=0; i<Size(); i++)
  {
    RString id = Get(i).id;
    if (id.GetLength()<=0)
    {
      // log all words
      char buf[1024];
      *buf=0;
      for (int s=0; s<Size(); s++)
      {
        strcat(buf,"'");
        strcat(buf,Get(s).id);
        strcat(buf,"' ");
      }
      RptF("Empty word in sentence %s",buf);
      continue;
    }
    else if (stricmp(id, "xmit") == 0)
    {
      transmit = true;
    }
    else
    {
      channel->Say(who, speaker, Get(i).id, Get(i).pauseAfter, transmit, wordsClass);
      transmit = false;
    }
  }
}

ConstParamEntryPtr FindRadio(RString name, SoundPars &pars, bool silent = false);

void RadioChannel::Say(RString waveName, AIBrain *sender, RString senderName, RString player, float duration)
{
  if (!sender && senderName.GetLength() == 0) return;

  SoundPars pars;
  if (waveName[0] == '#')
  {
    char buffer[256];
    strncpy(buffer, (const char *)waveName + 1, 256); buffer[255] = 0;
    char *ext = strrchr(buffer, '.');
    if (ext) *ext = 0;
    GChatList.Add(_chatChannel, sender, buffer, false, false);

    RString dir;
    if (player.GetLength() > 0)
    {
      RString GetClientCustomFilesDir();
      dir = GetClientCustomFilesDir() + RString("\\players\\") + EncodeFileName(player) + RString("\\sound\\");
    }
    else
    {
      RString GetUserDirectory();
      dir = GetUserDirectory() + RString("sound\\");
    }
    pars.name = dir + RString((const char *)waveName + 1);
    pars.freq = 1;
    pars.freqRnd = 1;
    pars.vol = 1;
    pars.volRnd = 1;
  }
  else
  {
    ConstParamEntryPtr cls = FindRadio(waveName, pars);

    if (cls)
    {
      if (sender)
        GChatList.Add(_chatChannel, sender, *cls >> "title", false, false);
      else
        GChatList.Add(_chatChannel, Pars >> "CfgHQIdentities" >> senderName >> "name", *cls >> "title", false, false);
    }
  }

  Person *who = NULL;
  if (_params.sound3D && sender) who = sender->GetPerson();

  if (pars.name.GetLength() > 0  && GWorld->GetAcceleratedTime() <= 1)
  {
    AbstractWave *wave = NULL;
    if (who)
    {
      const float volume3DCoef = 0.01f;
      wave = GSoundScene->OpenAndPlayOnce(pars.name, who, false, who->FutureVisualState().Position(), who->FutureVisualState().Speed(), volume3DCoef, pars.freq, pars.distance);
      if (wave)
      {
        who->SetDirectSpeaking(wave);
        who->AttachWave(wave,pars.freq);
        wave->SetKind(WaveEffect);
      }
    }
    else
    {
      wave = GSoundScene->OpenAndPlayOnce2D(pars.name, 1, pars.freq, false);
      if (wave)
      {
        wave->SetKind(WaveSpeech);
      }
    }
    if (!_saying)
    {
      // start queue
      _saying = wave;
      if (_saying)
      {
        _saying->Play(); // start playback (once)
      }
    }
    else
    {
      if (wave)
      {
        wave->Queue(_saying);
      }
    }
    _pauseAfterMessage = 0;
  }
  else
  {
    _pauseAfterMessage = duration;
  }

  if (sender)
  {
    _speaker = *sender->GetSpeaker();
  }
  else // if (senderName.GetLength() > 0)
  {
    ParamEntryVal cls = Pars >> "CfgHQIdentities" >> senderName;
    RString name = cls >> "speaker";
    float pitch = cls >> "pitch";
    Ref<BasicSpeaker> basicSpeaker = new BasicSpeaker(name);
    _speaker = Speaker(basicSpeaker, pitch);
  }
  // start playing noise
  if (_saying && _speaker.IsSpeakerValid())
  {
    if (_params.noise.GetLength() > 0)
    {
      _noise = _speaker.SayNoPitch(who, _params.noise, true, RString());
    }
  }
}

bool IsSpeakingDirect(AIBrain *sender);

bool RadioChannel::CheckSpeaking(AIBrain *sender) const
{
  const RadioMessage *msg = GetActualMessage();
  if (msg)
  {
    return msg->GetSender()==sender;
  }
  else
  {
    // nothing is being said right now - check first message in the queue
    if (_messageQueue.Size()>0)
    {
      msg = _messageQueue[0];
      return msg->GetSender()==sender;
    }
    return false;
  }
}

/// helper functor - perform a check if given channel is a more important one
class RadioCheck
{
  RadioChannel *_speaking;
  RadioChannel *_ignore;
  AIBrain *_sender;
  
  public:
  RadioCheck(AIBrain *sender, RadioChannel *ignore)
  :_speaking(NULL),_sender(sender),_ignore(ignore)
  {}

  void operator () (RadioChannel &channel)
  {
    if (&channel==_ignore) return;
    if (_speaking && _speaking->GetLevel()>channel.GetLevel()) return;
    if (!channel.CheckSpeaking(_sender)) return;
    _speaking = &channel;
  }
  operator RadioChannel *() const {return _speaking;}
};

/// determine what priority is currently being used
/**
@return highest priority channel currently in use
*/
RadioChannel *SpeakingOnRadioChannel(AIBrain *sender, RadioChannel *ignore)
{
  if (!sender) return NULL;

  // even when not speaking, but the message is about to start, we should consider it speaking
  // global channel
  RadioCheck check(sender,ignore);
  check(GWorld->GetRadio());

  // vehicle channel
  Transport *veh = sender->GetVehicleIn();
  if (veh)
  {
    check(veh->GetRadio());
  }

  // group channel
  AIGroup *grp = sender->GetGroup();
  if (grp)
  {
    check(grp->GetRadio());

    // side channel
    AICenter *center = grp->GetCenter();
    if (center)
    {
      check(center->GetCommandRadio());
      check(center->GetSideRadio());
    }
  }

  // direct speaking
  Person *person = sender->GetPerson();
  RadioChannel *radio = person ? person->GetRadio() : NULL;
  if (radio) check(*radio);

  return check;
}

/*!
\patch 1.55 Date 5/7/2002 by Ondra
- Changed: All radio trafic that is done in mission init is now processed before the mission starts,
expect for messages transmitted via xxxRadio and xxxChat scripting commands.
\patch 1.78 Date 7/16/2002 by Jirka
- Fixed: Silent processing of radio messages sometimes freeze
*/

void RadioChannel::SilentProcess()
{
  if (_actualMsg)
  {
    if (!_actualMsg->IsTransmitted())
      _actualMsg->Transmitted(_chatChannel); // confirm xmit done
      // SetTransmitted is needless
    _actualMsg=NULL;
  }
/*
  for (int i=0; i<_messageQueue.Size(); i++)
  {
    RadioMessage *msg = _messageQueue[i];
    if (msg->GetType()!=RMTText)
    {
      msg->Transmitted(_chatChannel);
    }
  }
  _messageQueue.Clear();
*/
  // FIX
  while (_messageQueue.Size() > 0)
  {
    Ref<RadioMessage> msg = _messageQueue[0];
    _messageQueue.Delete(0);
    if (msg->GetType()!=RMTText)
      msg->Transmitted(_chatChannel);
  }
}

/*!
\patch 5129 Date 2/19/2007 by Jirka
- Fixed: MP - Radio messages echo on remote clients was missing
*/

void RadioChannel::NextMessage(NetworkObject *object)
{
  if (_actualMsg)
  {
    if (!_actualMsg->IsTransmitted())
      _actualMsg->Transmitted(_chatChannel); // confirm xmit done
      // SetTransmitted is needless
    _actualMsg=NULL;
  }

  while
  (
    _messageQueue.Size() > 0 &&
    ((!_messageQueue[0]) || Glob.time > _messageQueue[0]->GetTimeOut())
  )
  {
    if (_messageQueue[0]) _messageQueue[0]->Canceled();
    _messageQueue.Delete(0);
  }

  if (_messageQueue.Size() <= 0 ) return;

  AIBrain *sender = _messageQueue[0]->GetSender();
  if (IsSpeakingDirect(sender))
  {
    // if speaker is speaking on direct channel, we cannot do anything but wait
    _pauseAfterMessage = 0; // check in next simulation step
    return;
  }
  RadioChannel *speaking = SpeakingOnRadioChannel(sender,this);

  if (speaking)
  {
    // AI will always wait until it finished what it is currently saying
    if (speaking->GetLevel()>=GetLevel() || !sender->IsAnyPlayer() && speaking->GetActualMessage())
    {
      _pauseAfterMessage = 0; // check in next simulation step
      return;
    }
    // we are currently speaking on lower level, we can abort on postpone the message
    
    if(sender->IsAnyPlayer() && speaking->GetActualMessage() && speaking->GetActualMessage()->IsConversation())
    {  
      _messageQueue[0]->Transmitted(_chatChannel);  
      SentChatMessage(_messageQueue[0], sender, object);
      _messageQueue.Delete(0);
      return;
    }
    else 
    {     
      speaking->StopSpeaking(sender);
    }
  }

  _actualMsg = _messageQueue[0];
  bool msgAudible = _audible && (GWorld->IsSentencesEnabled() || _actualMsg->IsConversation());
  _messageQueue.Delete(0);

  float pauseAfterMessage = 0;
  if ((!sender || sender != GWorld->FocusOn()))
  {
    if (_messageQueue.Size() <= 5)
    {
      pauseAfterMessage = GRandGen.Gauss(0.1f, 0.4f, 2.5f); 
    }
    else
    {
      pauseAfterMessage = GRandGen.Gauss(0.1f, 0.15f, 0.3f); 
    }
  }
  
  // not all messages have real 3D sender
  Person *who = sender ? sender->GetPerson() : NULL;

  SentenceParams params;
  RString sentenceName = _actualMsg->PrepareSentence(params); // name of sentence
  ConstParamEntryPtr sentence;

  // select radio protocol by the speaker
  Speaker *speaker = _actualMsg->GetSpeaker();
  ConstParamEntryPtr argClass = ParamEntryPtr(NULL);
  if (speaker && !sentenceName.IsEmpty())
  {
    RString protocol = speaker->GetProtocol();
    ConstParamEntryPtr protocolCls = protocol.GetLength() > 0 ? Pars.FindEntry(protocol) : ParamEntryPtr(NULL);
    if (protocolCls) 
    {
      argClass = protocolCls->FindEntry("Arguments");
      sentence = protocolCls->FindEntry(sentenceName);
    }
  }

  if (sentence)
  {
    float rnd = GRandGen.RandomValue();
    ParamEntryPtr clsVariant = SelectVariant(_actualMsg, (*sentence), rnd);
    if (!clsVariant)
    {
      // nothing to tell
      _pauseAfterMessage = 0;
    }
    else
    {
      bool voiceEnabled = true;
  #if _VBS3
      if(!Glob.config.IsEnabled(DTAIVoices))
        voiceEnabled = false;
      else
        if(who && who->Brain())
          voiceEnabled = who->Brain()->CanSpeak();
  #endif

      if (who && sender->IsGestureEnabled(_chatChannel))
      {
        // first try searching in a variant
        // when not found, use a common entry
        ConstParamEntryPtr entry = (*clsVariant).FindEntry("gesture");
        if (!entry) entry = sentence->FindEntry("gesture");
        if (entry)
        {
          RString gesture = *entry;
          if (gesture.GetLength() > 0)
          {
            who->PlayAction(gesture);
            // voice can be disabled
            voiceEnabled = sender->IsVoiceEnabled(_chatChannel);
          }
        }
      }

      if (voiceEnabled)
      {
        RadioSentence sent;
        // fixed - voice communication was not audible
        if (GWorld->IsSentencesEnabled() || _actualMsg->IsConversation()) SaySentence(sent, *clsVariant, params, pauseAfterMessage, _params);
        if (GWorld->GetAcceleratedTime() <= 1)
        {
          sent.Say(who, this, speaker);
          _pauseAfterMessage=0;
        }
        else
        {
          _pauseAfterMessage = _actualMsg->GetDuration();
        }

        if (msgAudible || GWorld->GetMode() == GModeNetware)
        {
          RString text = WriteSentence((*clsVariant) >> "text", params);
#if DIAG_SELECT_ENEMY_DETECTED_SENTENCE
          if (stricmp(sentenceName, "SelectEnemyDetectedSentence")==0)
          {
            LogF("   Write: \"%s\"", cc_cast(text));
          }
#endif
          if (msgAudible) GChatList.Add(_chatChannel, _actualMsg->GetSender(), text, _actualMsg->IsPlayerMsg(), false);
          if (_actualMsg->GetSender() && _actualMsg->SendOverNetwork()) SendRadioChat(_chatChannel, _actualMsg->GetSender(), object, text, sent);
        }
      }
    }
  }
  else
  {
    _pauseAfterMessage = _actualMsg->GetDuration();

    if (msgAudible || GWorld->GetMode() == GModeNetware)
    {
      RString text = _actualMsg->GetText();
      if (msgAudible && text.GetLength() > 0)
      {
#if _VBS3
        // disable text and audio, if AI speech has been disabled
        if(who && who->Brain() && who->Brain()->CanSpeak())
#endif
        GChatList.Add(_chatChannel, _actualMsg->GetSender(), text, _actualMsg->IsPlayerMsg(), false);
      }

      RString wave = _actualMsg->GetWave();
      if (wave.GetLength() > 0)
      {
        _pauseAfterMessage = 0;
        if (msgAudible)
          Say(wave, _actualMsg->GetSender(), _actualMsg->GetSenderName(), "", _actualMsg->GetDuration());
        if (_actualMsg->GetSender() && _actualMsg->SendOverNetwork())
          SendRadioChatWave(_chatChannel, object, wave, _actualMsg->GetSender(), _actualMsg->GetSenderName());
      }
      else
      {
        RadioSentence sent;
        _actualMsg->GetSpeech(sent, _params);
        if (GWorld->GetAcceleratedTime() <= 1 && msgAudible && sent.Size() > 0)
        {
          sent.Say(who, this, speaker);
          _pauseAfterMessage = 0;
        }
        // SendRadioChat only when something relevant is present
        if ( _actualMsg->GetSender() && _actualMsg->SendOverNetwork() && (text.GetLength() > 0 || sent.Size() > 0) )
          SendRadioChat(_chatChannel, _actualMsg->GetSender(), object, text, sent);
      }
    }
  }
}

void RadioChannel::SentChatMessage(RadioMessage *message, AIBrain *sender, NetworkObject *object)
{
  if(!message) return;
  bool msgAudible = _audible && (GWorld->IsSentencesEnabled() || message->IsConversation());
    
  //float pauseAfterMessage = 0;
  // not all messages have real 3D sender
  Person *who = sender ? sender->GetPerson() : NULL;

  SentenceParams params;
  RString sentenceName = message->PrepareSentence(params); // name of sentence
  ConstParamEntryPtr sentence;

  // select radio protocol by the speaker
  Speaker *speaker = message->GetSpeaker();
  ConstParamEntryPtr argClass = ParamEntryPtr(NULL);
  if (speaker && !sentenceName.IsEmpty())
  {
    RString protocol = speaker->GetProtocol();
    ConstParamEntryPtr protocolCls = protocol.GetLength() > 0 ? Pars.FindEntry(protocol) : ParamEntryPtr(NULL);
    if (protocolCls) 
    {
      argClass = protocolCls->FindEntry("Arguments");
      sentence = protocolCls->FindEntry(sentenceName);
    }
  }

  if (sentence)
  {
    float rnd = GRandGen.RandomValue();
    ParamEntryPtr clsVariant = SelectVariant(message, (*sentence), rnd);
    if (!clsVariant)
    {
      // nothing to tell
      //_pauseAfterMessage = 0;
    }
    else
    {
      bool voiceEnabled = true;

      if (who && sender->IsGestureEnabled(_chatChannel))
      {
        // first try searching in a variant
        // when not found, use a common entry
        ConstParamEntryPtr entry = (*clsVariant).FindEntry("gesture");
        if (!entry) entry = sentence->FindEntry("gesture");
        if (entry)
        {
          RString gesture = *entry;
          if (gesture.GetLength() > 0)
          {
            who->PlayAction(gesture);
            // voice can be disabled
            voiceEnabled = sender->IsVoiceEnabled(_chatChannel);
          }
        }
      }

      if (voiceEnabled)
      {
        RadioSentence sent;

        if (msgAudible || GWorld->GetMode() == GModeNetware)
        {
          RString text = WriteSentence((*clsVariant) >> "text", params);
          if (msgAudible) GChatList.Add(_chatChannel, message->GetSender(), text, message->IsPlayerMsg(), false);
          if (message->GetSender() && message->SendOverNetwork()) SendRadioChat(_chatChannel, message->GetSender(), object, text, sent);
        }
      }
    }
  }
  else
  {
    if (msgAudible || GWorld->GetMode() == GModeNetware)
    {
      RString text = message->GetText();
      if (msgAudible && text.GetLength() > 0)
      {
          GChatList.Add(_chatChannel, message->GetSender(), text, message->IsPlayerMsg(), false);
      }

      RString wave = message->GetWave();
      if (wave.GetLength() > 0)
      {
        if (message->GetSender() && message->SendOverNetwork())
          SendRadioChatWave(_chatChannel, object, wave, message->GetSender(), message->GetSenderName());
      }
      else
      {
        RadioSentence sent;
        if ( message->GetSender() && message->SendOverNetwork() && (text.GetLength() > 0 || sent.Size() > 0) )
          SendRadioChat(_chatChannel, message->GetSender(), object, text, sent);
      }
    }
  }
}

static bool UnitAlive(AIUnit *unit)
{
  // check if unit is alive
  // if it is, confirm it to leader
  if (!unit) return false;
  AIGroup *grp = unit->GetGroup();
  if (!grp) return false;
  if (!unit->LSIsAlive()) return false;
  grp->SetReportedDown(unit,false);
  grp->SetReportBeforeTime(unit,TIME_MAX);
  return true;
}


///////////////////////////////////////////////////////////////////////////////
// class RadioMessageUnitKilled

LSError RadioMessageUnitKilled::Serialize(ParamArchive &ar)
{
  CHECK(RadioMessage::Serialize(ar))
  CHECK(ar.SerializeRef("from", _from, 1))
  CHECK(ar.SerializeRef("who", _who, 1))
  return LSOK;
}

const char *RadioMessageUnitKilled::GetPriorityClass()
{
  return "PriorityReport";
}

RString RadioMessageUnitKilled::PrepareSentence(SentenceParams &params) const
{
  if (!_from) return RString();
  if (!_who) return RString();
  if (!_from->LSCanSpeak()) return RString();

  SetPlayerMsg(_from == GWorld->FocusOn());

  params.Add(_who);
  params.Add(_from);

  return "SentUnitKilled";
}

/*!
\patch 1.42 Date 1/4/2002 by Ondra
- Fixed: Repeated ".. is down" message when player was killed.
*/

void RadioMessageUnitKilled::Transmitted(ChatChannel channel)
{
  // nothing to do
  if (!UnitAlive(_from)) return;
  if (!_who) return;
  // remove unit from the subgroup and group
  AIGroup *grp = _who->GetGroup();
  if (!grp) return;
  if (_who->GetLifeState() == LifeStateDead)
  {
    AISubgroup *subgrp = _who->GetSubgroup();
    subgrp->ReceiveAnswer(_who,AI::UnitDestroyed);
  }
  else
  {
    // force unit to report status
    // temporarily hide in group list (MIA status)
    if (!_who->IsAnyPlayer() || !_who->LSIsAlive())
    {
      grp->SetReportedDown(_who, true);
    }
    if (_who->LSCanSpeak())
    {
      _who->ReportStatus();
    }
  }
}


///////////////////////////////////////////////////////////////////////////////
// class RadioMessageSupportAsk

LSError RadioMessageSupportAsk::Serialize(ParamArchive &ar)
{
  CHECK(RadioMessage::Serialize(ar))
  CHECK(ar.SerializeRef("from", _from, 1))
  CHECK(ar.SerializeEnum("type", _type, 1, ENUM_CAST(UIActionType,ATNone)))
  return LSOK;
}

const char *RadioMessageSupportAsk::GetPriorityClass()
{
  return "NormalCommand";
}

void RadioMessageSupportAsk::Transmitted(ChatChannel channel)
{
  AIBrain *sender = GetSender();
  if (!_from || !sender) return;

  AICenter *center = _from->GetCenter();
  Assert(center);
  center->AskSupport(_from, _type);
}

RString RadioMessageSupportAsk::PrepareSentence(SentenceParams &params) const
{
  AIBrain *sender = GetSender();
  if (!_from || !sender) return RString();
  
  Vector3Val pos = sender->Position(sender->GetFutureVisualState());
  //VBS supports bigger grids
  char buffer[32];
  PositionToAA11(pos, buffer);
  params.Add(RString(buffer));
  
  switch (_type)
  {
  case ATHeal:
    return "SentSupportAskHeal";
  case ATHealSoldier:
    return "SentSupportAskHeal";
  case ATRepair:
    return "SentSupportAskRepair";
  case ATRepairVehicle:
    return "SentSupportAskRepair";
  case ATRearm:
    return "SentSupportAskRearm";
  case ATRefuel:
    return "SentSupportAskRefuel";
  default:
    Fail("Action");
    return RString();
  }
}

///////////////////////////////////////////////////////////////////////////////
// class RadioMessageSupportNotAvailable

LSError RadioMessageSupportNotAvailable::Serialize(ParamArchive &ar)
{
  CHECK(RadioMessage::Serialize(ar))
  CHECK(ar.SerializeRef("to", _to, 1))
  return LSOK;
}

const char *RadioMessageSupportNotAvailable::GetPriorityClass()
{
  return "Failure";
}

void RadioMessageSupportNotAvailable::Transmitted(ChatChannel channel)
{
}

RString RadioMessageSupportNotAvailable::PrepareSentence(SentenceParams &params) const
{
  if (!_to) return RString();
  return "SentSupportNotAvailable";
}

void RadioMessageSupportNotAvailable::SetSenderName()
{
  ParamEntryVal cls = Pars >> "CfgHQIdentities";
  for (int i=0; i<cls.GetEntryCount(); i++)
  {
    ParamEntryVal entry = cls.GetEntry(i);
    if (entry.IsClass())
    {
      _senderName = entry.GetName();
      break;
    }
  }
  if (_senderName.GetLength() > 0)
  {
    ParamEntryVal cls = Pars >> "CfgHQIdentities" >> _senderName;
    RString name = cls >> "speaker";
    float pitch = cls >> "pitch";
    Ref<BasicSpeaker> basicSpeaker = new BasicSpeaker(name);
    _speaker = new Speaker(basicSpeaker, pitch);
  }
}

///////////////////////////////////////////////////////////////////////////////
// class RadioMessageSupportConfirm

LSError RadioMessageSupportConfirm::Serialize(ParamArchive &ar)
{
  CHECK(RadioMessage::Serialize(ar))
  CHECK(ar.SerializeRef("from", _from, 1))
  return LSOK;
}

const char *RadioMessageSupportConfirm::GetPriorityClass()
{
  return "NormalCommand";
}

void RadioMessageSupportConfirm::Transmitted(ChatChannel channel)
{
}

RString RadioMessageSupportConfirm::PrepareSentence(SentenceParams &params) const
{
  AIBrain *sender = GetSender();
  if (!_from || !sender) return RString();

  const AIGroup *group = _from->GetSupportedGroup();
  if (!group) return RString();
  params.AddWord("", group->GetName());

  Vector3Val pos = _from->GetSupportPos();
  //VBS supports bigger grids
  char buffer[32];
  PositionToAA11(pos, buffer);
  params.Add(RString(buffer));

  return "SentSupportConfirm";
}

///////////////////////////////////////////////////////////////////////////////
// class RadioMessageSupportReady

LSError RadioMessageSupportReady::Serialize(ParamArchive &ar)
{
  CHECK(RadioMessage::Serialize(ar))
  CHECK(ar.SerializeRef("from", _from, 1))
  return LSOK;
}

const char *RadioMessageSupportReady::GetPriorityClass()
{
  return "NormalCommand";
}

void RadioMessageSupportReady::Transmitted(ChatChannel channel)
{
}

RString RadioMessageSupportReady::PrepareSentence(SentenceParams &params) const
{
  AIBrain *sender = GetSender();
  if (!_from || !sender) return RString();

  const AIGroup *group = _from->GetSupportedGroup();
  if (!group) return RString();
  params.AddWord("", group->GetName());

  Vector3Val pos = _from->GetSupportPos();
  //VBS supports bigger grids
  char buffer[32];
  PositionToAA11(pos, buffer);
  params.Add(RString(buffer));

  return "SentSupportReady";
}

///////////////////////////////////////////////////////////////////////////////
// class RadioMessageSupportDone

LSError RadioMessageSupportDone::Serialize(ParamArchive &ar)
{
  CHECK(RadioMessage::Serialize(ar))
  CHECK(ar.SerializeRef("from", _from, 1))
  return LSOK;
}

const char *RadioMessageSupportDone::GetPriorityClass()
{
  return "NormalCommand";
}

void RadioMessageSupportDone::Transmitted(ChatChannel channel)
{
  AIBrain *sender = GetSender();
  if (!_from || !sender) return;

  AICenter *center = _from->GetCenter();
  Assert(center);
  center->SupportDone(_from);
}

RString RadioMessageSupportDone::PrepareSentence(SentenceParams &params) const
{
  AIBrain *sender = GetSender();
  if (!_from || !sender) return RString();

  return "SentSupportDone";
}

///////////////////////////////////////////////////////////////////////////////
// class RadioMessageJoin

RadioMessageJoin::RadioMessageJoin(AIGroup *from, AIUnit *follow, OLinkPermNOArray(AIUnit) &to)
{
  _from = from;
  _follow = follow;
  _to = to;
}

LSError RadioMessageJoin::Serialize(ParamArchive &ar)
{
  CHECK(RadioMessage::Serialize(ar))
  CHECK(ar.SerializeRef("From", _from, 1))
  CHECK(ar.SerializeRef("Follow", _follow, 1))
  CHECK(ar.SerializeRefs("To", _to, 1))
  return LSOK;
}

const char *RadioMessageJoin::GetPriorityClass()
{
  return "NormalCommand";
}

bool RadioMessageJoin::IsTo(AIBrain *unit) const
{
  for (int i=0; i<_to.Size(); i++)
  {
    AIUnit *u = _to[i];
    if (u == unit)
      return true;
  }
  return false;
}

RString RadioMessageJoin::PrepareSentence(SentenceParams &params) const
{
  AIBrain *sender = GetSender();
  if (!_from || !sender) return RString();
  if (_to.Count() <= 0) return RString();

  SetPlayerMsg(IsTo(GWorld->FocusOn()));

  params.Add(_to, true);
  params.Add(_follow ? (AIUnit *)_follow : _from->Leader());

  return "SentCmdFollow";
}

///////////////////////////////////////////////////////////////////////////////
// class RadioMessageJoinDone

LSError RadioMessageJoinDone::Serialize(ParamArchive &ar)
{
  CHECK(RadioMessage::Serialize(ar))
  CHECK(ar.SerializeRef("From", _from, 1))
  CHECK(ar.SerializeRef("To", _to, 1))
  return LSOK;
}

const char *RadioMessageJoinDone::GetPriorityClass()
{
  return "JoinCompleted";
}

RString RadioMessageJoinDone::PrepareSentence(SentenceParams &params) const
{
  if (!_from) return RString();

  SetPlayerMsg(_from == GWorld->FocusOn());

  params.Add(_from);
  return "SentJoinCompleted";
}

///////////////////////////////////////////////////////////////////////////////
// class RadioMessageUnitAnswer

LSError RadioMessageUnitAnswer::Serialize(ParamArchive &ar)
{
  CHECK(RadioMessage::Serialize(ar))
  CHECK(ar.SerializeRef("From", _from, 1))
  CHECK(ar.SerializeRef("To", _to, 1))
  CHECK(ar.SerializeEnum("answer", _answer, 1))
  return LSOK;
}

const char *RadioMessageUnitAnswer::GetPriorityClass()
{
  switch (_answer)
  {
    case AI::HealthCritical:
    case AI::DamageCritical:
      return "CriticalReport";
    case AI::FuelCritical:
    case AI::AmmoCritical:
    case AI::IsLeader:
      return "PriorityReport";
    case AI::FuelLow:
    case AI::AmmoLow:
    case AI::ReportPosition:
    case AI::ReportSemaphore:
      return "Report";
    case AI::SubgroupDestinationUnreacheable:
      return "Failure";
    default:
      Fail("Answer");
      return "Default";
  }
}

RString RadioMessageUnitAnswer::PrepareSentence(SentenceParams &params) const
{
  if (!_from)
    return RString();
  if (!_to)
    return RString();

  if (!_from->LSCanSpeak()) return RString();

  SetPlayerMsg(_from == GWorld->FocusOn());

  params.Add(_from);
  switch (_answer)
  {
    case AI::HealthCritical:
      return "SentHealthCritical";
    case AI::DamageCritical:
      return "SentDammageCritical";
    case AI::FuelCritical:
      return "SentFuelCritical";
    case AI::FuelLow:
      return "SentFuelLow";
    case AI::AmmoCritical:
      return "SentAmmoCritical";
    case AI::AmmoLow:
      return "SentAmmoLow";
    case AI::ReportPosition:
      {
        Vector3Val pos = _from->Position(_from->GetFutureVisualState());
        //VBS supports bigger grids
        char buffer[32];
        PositionToAA11(pos, buffer);
        params.Add(RString(buffer));
      }
      return "SentReportPosition";
    case AI::ReportSemaphore:
      return RString();
    case AI::IsLeader:
      return "SentIsLeader";
    case AI::SubgroupDestinationUnreacheable:
      return "SentDestinationUnreacheable";
    default:
      Fail("Answer");
      return RString();
  }
}

void RadioMessageUnitAnswer::Transmitted(ChatChannel channel)
{
  if (_to && _from)
  {
    if (UnitAlive(_from))
    {
      AIGroup *grp = _to->GetGroup();
      if (grp)
      {
        grp->SetReportedDown(_from,false);
        grp->SetReportBeforeTime(_from,TIME_MAX);
      }
      if (!_to->IsLocal())
      {
        DoAssert(_from->IsAnyPlayer());
        if (_from->IsAnyPlayer())
        {
          GetNetworkManager().AskForReceiveUnitAnswer(_from,_to,_answer);
        }
        return;
      }
      _to->ReceiveAnswer(_from, _answer);
      if (_answer == AI::ReportPosition)
      {
        if (grp && grp->IsPlayerGroup())
        {
          void AddUnitInfo(AIUnit *subgroup);
          AddUnitInfo(_from);
        }
      }

      if
      (
        _answer == AI::ReportPosition ||
        _answer == AI::IsLeader
      )
      {
        // show leader when report position or taking command takes place
        AIBrain *player = GWorld->FocusOn();
        AIGroup *pgrp = player ? player->GetGroup() : NULL;
        if (pgrp && pgrp->Leader() == _from && GWorld->UI())
          GWorld->UI()->ShowFormPosition();
        // unit reported - clear flag we are waiting for report
      }
    }
  }
}

///////////////////////////////////////////////////////////////////////////////
// class RadioMessageFireStatus

LSError RadioMessageFireStatus::Serialize(ParamArchive &ar)
{
  CHECK(RadioMessage::Serialize(ar))
  CHECK(ar.SerializeRef("From", _from, 1))
  CHECK(ar.Serialize("answer", _answer, 1, false))
  return LSOK;
}

const char *RadioMessageFireStatus::GetPriorityClass()
{
  return "Report";
}

RString RadioMessageFireStatus::PrepareSentence(SentenceParams &params) const
{
  if (!_from) return RString();

  SetPlayerMsg(_from == GWorld->FocusOn());

  if (_answer)
    return "SentFireReady";
  else
    return "SentFireNegative";
}

void RadioMessageFireStatus::Transmitted(ChatChannel channel)
{
}

///////////////////////////////////////////////////////////////////////////////
// class RadioMessageFireStatus

LSError RadioMessageArtilleryFireStatus::Serialize(ParamArchive &ar)
{
  CHECK(RadioMessage::Serialize(ar))
    CHECK(ar.SerializeRef("From", _from, 1))
    CHECK(ar.Serialize("answer", _answer, 1, 0))
    return LSOK;
}

const char *RadioMessageArtilleryFireStatus::GetPriorityClass()
{
  return "Report";
}

RString RadioMessageArtilleryFireStatus::PrepareSentence(SentenceParams &params) const
{
  if (!_from) return RString();

  SetPlayerMsg(_from == GWorld->FocusOn());

  if (_answer == 0) return "SentARTYRoundsComplete";
  else if (_answer == 1)  return "SentAmmoCritical";
  else if (_answer == 2) return "SentARTYCannotExecuteAdjustCoordinates";
  else return "SentARTYCannotExecuteThatsOutsideOurFiringEnvelope";
}

void RadioMessageArtilleryFireStatus::Transmitted(ChatChannel channel)
{
}

///////////////////////////////////////////////////////////////////////////////
// class RadioMessageSubgroupAnswer

LSError RadioMessageSubgroupAnswer::Serialize(ParamArchive &ar)
{
  CHECK(RadioMessage::Serialize(ar))
  CHECK(ar.SerializeRef("From", _from, 1))
  CHECK(ar.SerializeRef("Leader", _leader, 1))
  CHECK(ar.SerializeRef("To", _to, 1))
  CHECK(ar.SerializeEnum("answer", _answer, 1))
  CHECK(ar.Serialize("cmd", _cmd, 2))
  CHECK(ar.Serialize("display", _display, 1, false))
  CHECK(ar.Serialize("forceLeader", _forceLeader, 1, false))
  return LSOK;
}

RadioMessageSubgroupAnswer::RadioMessageSubgroupAnswer() {}

RadioMessageSubgroupAnswer::RadioMessageSubgroupAnswer
(
  AISubgroup *from, AIGroup *to, AI::Answer answer, Command *cmd, bool display,
  AIUnit *leader
)
{
  _from = from;
  _leader = leader ? leader : from->Leader();
  _forceLeader = leader!=NULL;
  _to = to;
  _answer = answer;
  if (cmd) _cmd = *cmd;
  _display = display;
}

const char *RadioMessageSubgroupAnswer::GetPriorityClass()
{
  switch (_answer)
  {
    case AI::CommandCompleted:
      return "Completition";
    case AI::CommandFailed:
    case AI::SubgroupDestinationUnreacheable:
      return "Failure";
    default:
      Fail("Answer");
      return "Default";
  }
}

RString RadioMessageSubgroupAnswer::PrepareSentence(SentenceParams &params) const
{
  if (!_display) return RString();

  AIUnit *leader = _leader;
  if (!_forceLeader || !leader)
  {
    if (_from && _from->Leader()) leader = _from->Leader();
    if (!leader) return RString();
  }

  if (!leader->LSCanSpeak()) return RString();

  params.Add(leader);
  SetPlayerMsg(leader == GWorld->FocusOn());

  switch (_answer)
  {
    case AI::CommandCompleted:
      if (_cmd._message == Command::Attack || _cmd._message == Command::AttackAndFire)
        return RString(); // no message
      else
        return "SentCommandCompleted";
    case AI::CommandFailed:
      return "SentCommandFailed";
    case AI::SubgroupDestinationUnreacheable:
      return "SentDestinationUnreacheable";
    default:
      Fail("Answer");
      return RString();
  }
}

AIBrain *RadioMessageSubgroupAnswer::GetSender() const
{
  if (_forceLeader && _leader) return _leader;
  return _from && _from->Leader() ? _from->Leader() : (AIUnit*)_leader;
}

void RadioMessageSubgroupAnswer::Transmitted(ChatChannel channel)
{
  AIGroup *grp = _to;
  if (!grp) return;
  AIUnit *fromLeader = _leader;
  if (_from && _from->Leader()) fromLeader = _from->Leader();
  if (!fromLeader) return;

  if (!UnitAlive(fromLeader)) return;

  AIUnit *grpLeader = grp->Leader();
  if (grpLeader && !grpLeader->CanHear(channel, fromLeader)) return;

  // if command was some kind of supply, reset supply status
  const Command &cmd = GetCommand();
  Command::Message msg = cmd._message;
  if (msg == Command::Action)
  {
    // get action ID
    if (!cmd._action) return;
    switch (cmd._action->GetType())
    {
      case ATHeal:
        msg = Command::Heal; break;
      case ATHealSoldier:
        msg = Command::HealSoldier; break;
      case ATFirstAid:
        msg = Command::FirstAid; break;
      case ATRepairVehicle:
        msg = Command::RepairVehicle; break;
      case ATRepair:
        msg = Command::Repair; break;
      case ATRefuel:
        msg = Command::Refuel; break;
      case ATRearm:
        msg = Command::Rearm; break;
      case ATTakeWeapon:
      case ATTakeDropWeapon:
        msg = Command::Rearm; break;
      case ATTakeMagazine:
      case ATTakeDropMagazine:
        msg = Command::Rearm; break;
      case ATTakeBag:
        msg = Command::TakeBag; break;
      case ATAssemble:
        msg = Command::Assemble; break;
      case ATDisAssemble:
        msg = Command::DisAssemble; break;
    }
  }
  switch (msg)
  {
    case Command::Heal:
      grp->SetHealthStateReported(fromLeader,AIUnit::RSNormal);
      break;
    case Command::HealSoldier:
      grp->SetHealthStateReported(fromLeader,AIUnit::RSNormal);
      break;
    case Command::RepairVehicle:
      grp->SetAmmoStateReported(fromLeader,AIUnit::RSLow);
      break;
    case Command::Rearm:
      grp->SetAmmoStateReported(fromLeader,AIUnit::RSNormal);
      break;
    case Command::Repair:
      grp->SetDamageStateReported(fromLeader,AIUnit::RSNormal);
      break;
    case Command::Refuel:
      grp->SetFuelStateReported(fromLeader,AIUnit::RSNormal);
      break;
    case Command::Action:
      // if it was action, it might have changed unit state
      break;

  }
  if (_from)
  {
    grp->ReceiveAnswer(_from, _answer);
  }
}

///////////////////////////////////////////////////////////////////////////////
// class RadioMessageReturnToFormation

const char *RadioMessageReturnToFormation::GetPriorityClass()
{
  return "NormalCommand";
}

LSError RadioMessageReturnToFormation::Serialize(ParamArchive &ar)
{
  CHECK(RadioMessage::Serialize(ar))
  CHECK(ar.SerializeRef("From", _from, 1))
  CHECK(ar.SerializeRef("Leader", _leader, 1))
  CHECK(ar.SerializeRef("To", _to, 1))
  return LSOK;
}

void RadioMessageReturnToFormation::Transmitted(ChatChannel channel)
{
  AIBrain *sender = GetSender();
  if (!sender || !_to) return;

  if (_to == GWorld->FocusOn() && GWorld->UI())
    GWorld->UI()->ShowFormPosition();
}

RString RadioMessageReturnToFormation::PrepareSentence(SentenceParams &params) const
{
  AIBrain *sender = GetSender();
  if (!sender || !_to) return RString();

  SetPlayerMsg(_to == GWorld->FocusOn());

  params.Add(_to);
  params.Add(sender);

  return "SentReturnToFormation";
}

///////////////////////////////////////////////////////////////////////////////
// class RadioMessageReply
// abstract class for all reply information

RadioMessageReply::RadioMessageReply(AIUnit *from, AIGroup *to)
{
  _from = from;
  _to = to;
}

LSError RadioMessageReply::Serialize(ParamArchive &ar)
{
  CHECK(RadioMessage::Serialize(ar))
  CHECK(ar.SerializeRef("From", _from, 1))
  CHECK(ar.SerializeRef("To", _to, 1))
  return LSOK;
}

RString RadioMessageReply::PrepareSentenceHelper(SentenceParams &params, const char *sentence) const
{
  if (!_from || !_to) return RString();

  SetPlayerMsg(_from == GWorld->FocusOn());
  params.Add(_from);
  return sentence;
}

///////////////////////////////////////////////////////////////////////////////
// class RadioMessageReportTarget

RadioMessageReportTarget::RadioMessageReportTarget(AIUnit *from, AIGroup *to, ReportSubject subject,  AIGroup::ReportTargetList &targets)
: base(from, to)
{
  _subject = subject;

  _x = toIntFloor(coefAa11 * targets[0]->position.X());
  _z = toIntFloor(coefAa11 * targets[0]->position.Z());
  _targets.Resize(targets.Size());
  for (int i=0; i<targets.Size(); i++)
  {
    _targets[i].tgt = targets[i]; //.idExact;
    //_targets[i].side = target.side;
    //_targets[i].type = target.type;
  }
}

/*
RadioMessageReportTarget::RadioMessageReportTarget(AIUnit *from, AIGroup *to, ReportSubject subject, AITargetInfo &target)
: base(from, to)
{
  _subject = subject;

  _x = toIntFloor(coefAa11 * target._realPos.X());
  _z = toIntFloor(coefAa11 * target._realPos.Z());
  _targets.Resize(1);
  _targets[0].id = target._idExact;
  _targets[0].side = target._side;
  _targets[0].type = target._type;
}
*/

LSError RadioMessageReportTarget::Serialize(ParamArchive &ar)
{
  CHECK(base::Serialize(ar))
  CHECK(ar.SerializeRef("From", _from, 1))
  CHECK(ar.SerializeRef("To", _to, 1))
  CHECK(ar.SerializeEnum("subject", _subject, 1, ReportNew))
  CHECK(ar.Serialize("x", _x, 1))
  CHECK(ar.Serialize("z", _z, 1))
  CHECK(ar.Serialize("Targets", _targets, 1))
  return LSOK;
}

const char *RadioMessageReportTarget::GetPriorityClass()
{
  return "Detected";
}

float RadioMessageReportTarget::GetSEInsideLocation() const
{
  if (_location && _targets.Size() && _location->IsInside(_targets[0].tgt->position)) return 1.0f;
  return 0;
}

// helper to Sort the targets with decreasing importance
struct AggrTargetItem
{
  // detected type not exact! (it uses target->type instead of target->idExact->GetType())
  const EntityAIType *_type;
  int _count;
  float _sortVal;
  const TargetNormal *_target;

  AggrTargetItem() {}
  AggrTargetItem(const TargetNormal *target) : _type(target->type), _sortVal(target->aggrGroupCost), _target(target), _count(1) {}

  RString GetText() const
  {
    if (_count>1) return _type->GetTextPlurar();
    else return _type->GetTextSingular();
  }

  ClassIsSimpleZeroed(AggrTargetItem);
};

struct BetterAggrTargetItem
{
  int operator () (const AggrTargetItem *t0, const AggrTargetItem *t1) const
  {
    return sign(t1->_sortVal - t0->_sortVal);
  }
};

/*!
\patch_internal 1.51 Date 4/19/2002 by Ondra
- Fixed: More consistent target handling in radio message.
Radio message was used from in-group reports only,
but was prepared also for center, which made group reports ineffective.
\patch 1.51 Date 4/19/2002 by Ondra
- Fixed: Target reporting often did not work, especially when using binoculars.
*/

#define USE_TWO_AGGR_TARGETS_IN_TEXT 1
#define USE_TWO_AGGR_TARGETS_IN_SPEECH 1
RString RadioMessageReportTarget::PrepareSentence(SentenceParams &params) const
{
  if (!_from || !_to || !_to->Leader()) return RString();
  AICenter *center = _to->GetCenter();
  if (!center) return RString();
  if (!GWorld->CameraOn()) return RString();

  Assert(_subject == ReportNew);
  // FIX: do not report Friendly targets
  //   When target dies it becomes friendly too. It can happen after the RadioMessageReportTarget containing
  //   this target is created and before its (i.e. this) PrepareSentence is called.
  //   So remove them at last!
  for (int i=0; i<_targets.Size(); i++)
  {
    TargetNormal *vtar = _targets[i].tgt;
    if ( !vtar || center->IsFriendly( vtar->GetSide() ) )
    {
#define DEBUG_AGGR_TGT_REMOVE 0
#if DEBUG_AGGR_TGT_REMOVE
      if (vtar)
      {
        RString name = (vtar->idExact ? vtar->idExact->GetDebugName() : "???") + vtar->GetType()->GetTextSingular();
        if (_to->GetDebugName()=="B 1-1-A" && (vtar->GetType()->GetTextSingular()=="officer" || vtar->GetType()->GetTextSingular()=="man"))
          _asm nop;
        LogF("*** Group %s: Target %s Removed inside RadioMessageReportTarget::PrepareSentence", cc_cast(_to->GetDebugName()), cc_cast(name));
      }
#endif
      _targets.Delete(i);
      i--; //there is different item under this item, process again
      continue;
    }
  }

  // Are there any targets to be reported after Friendly removal?
  if (_targets.Size() < 1) 
    return RString();

  // group the targets with the same vehicle name
  AutoArray<AggrTargetItem, MemAllocLocal<AggrTargetItem, 64> > reportTargets;
  for (int i=0; i<_targets.Size(); i++)
  {
    TargetNormal *target = _targets[i].tgt;
    if (target && target->idExact)
    {
      RString tname = target->type->GetTextSingular();
#if DIAG_AGGREGATED_TARGETS
      // debugging breakpoint possibility
      if (stricmp(cc_cast(tname),"unknown")==0)
        _asm nop;
#endif
      bool found = false;
      for (int j=0, siz=reportTargets.Size(); j<siz; j++)
      {
        if (tname==reportTargets[j]._type->GetTextSingular())
        {
          reportTargets[j]._count++;
          if (reportTargets[j]._sortVal < target->aggrGroupCost)
          { // update the max cost and target
            reportTargets[j]._sortVal = target->aggrGroupCost;
            reportTargets[j]._target = target;
          }
          found = true;
          break;
        }
      }
      if (!found)
      {
        reportTargets.Add(AggrTargetItem(target));
      }
    }
  }
  if (!reportTargets.Size()) return RString();
  // sort the targets subgroups with importance
  for (int j=0; j<reportTargets.Size(); j++) //update sortVal
    reportTargets[j]._sortVal *= reportTargets[j]._count;
  ::QSort(reportTargets, BetterAggrTargetItem());
#if DIAG_AGGREGATED_TARGETS
  LogF("*** ReportTargets-PrepareSentence ***");
  for (int j=0; j<reportTargets.Size(); j++) //update sortVal
  {
    LogF("   %d %s ... %.1f", reportTargets[j]._count, cc_cast(reportTargets[j].GetText()), reportTargets[j]._sortVal);
  }
#endif

  const TargetNormal *tgt = reportTargets[0]._target;
  if (!tgt) return RString();
  if (tgt->side == center->GetSide()) return RString();

  // Prepare data for Simple Expressions (to evaluate the ReportTarget specific functions 
  // like DistanceToXXX, UnitDistanceFactor,...)

  SetPlayerMsg(_from == GWorld->FocusOn());

  // prepare some RadioMessageReportTarget helper members for later use
  _to->UpdateCompactness(true); //lazy update, only to ensure it is initialized
  _groupCoreRadius = _to->GetCoreRadius();
  _groupCompactness = _to->GetCompactness();

  // Add all parameters needed (all variants have the same parameters list)
  // 1 - who reported ID
  params.Add(_from);
  // 2 - aggrTargetList
  // use at most first two AggrTargetItems ("Tanks and truck" but not "Tanks, truck and soldier")
  RString aggrText;
#if USE_TWO_AGGR_TARGETS_IN_TEXT
  if (reportTargets.Size()>1)
  {
    RString wordAnd = LocalizeString(IDS_AC_AND);
    aggrText = Format("%s %s %s", cc_cast(reportTargets[0].GetText()), cc_cast(wordAnd), cc_cast(reportTargets[1].GetText()));
  }
  else 
#endif
    aggrText = reportTargets[0].GetText();
  // use only one AggrTarget in the speech (to make the speech faster)
  AutoArray<AggrTargetWord> speechList;
  speechList.Add(AggrTargetWord(reportTargets[0]._type, reportTargets[0]._count<=1));
#if USE_TWO_AGGR_TARGETS_IN_SPEECH
  if (reportTargets.Size()>1)
    speechList.Add(AggrTargetWord(reportTargets[1]._type, reportTargets[1]._count<=1));
#endif
  params.AddAggrTargets(speechList, aggrText, this);
  // 3 - gridPosition
  // square in map
  // VBS supports bigger grids
  char buffer[32];
  PositionToAA11(tgt->position, buffer);
  params.Add(RString(buffer));
  // 4 - side
  if (tgt->side == TSideUnknown)
    params.AddWord("unknown", LocalizeString(IDS_WORD_UNKNOWN));
  else if (_to->GetCenter()->IsEnemy(tgt->side))
    params.AddWord("enemy", LocalizeString(IDS_WORD_ENEMY));
  else if (_to->GetCenter()->IsFriendly(tgt->side))
    params.AddWord("friendly", LocalizeString(IDS_WORD_FRIENDLY));
  else
    params.AddWord("neutral", LocalizeString(IDS_WORD_NEUTRAL));

  // Prepare groupTgtDirection and groupTgtDistance
  Matrix4 transf;
  transf.SetOriented(_to->GetGroupDirection(), VUp);
  transf.SetPosition(_to->GetCorePosition());
  Vector3 dir =  transf.InverseRotation().FastTransform(tgt->position);
  float relDir = atan2(dir.X(),dir.Z());
  Vector3 absDirVect = tgt->position-_to->GetCorePosition();
  _distanceToGroup = absDirVect.SizeXZ();
  saturateMax(_distanceToGroup, 1); //at least 1 meter
  float absGrpDir = atan2(absDirVect.X(), absDirVect.Z());
  // 5 - groupTgtDistance
  int martaTypeIx = _to->GetMartaTypeIx();
  params.AddDistance(_distanceToGroup, martaTypeIx, this);
  // 6 - groupTgtDirection
  params.AddDirection(relDir, absGrpDir, false, this);
  // 7 - senderTgtDistance
  Vector3 absSenderVect = tgt->position-_from->Position(_from->GetFutureVisualState());
  _distanceToSender = absSenderVect.SizeXZ(); 
  saturateMax(_distanceToSender, 1); //at least 1 meter
  params.AddDistance(_distanceToSender, martaTypeIx, this);
  // 8 - senderTgtDirection
  float absSenderDir = atan2(absSenderVect.X(),absSenderVect.Z());
  params.AddDirection(relDir, absSenderDir, true, this);
  // 9 - unitTgtDistance
  // Find the nearest unit first
  AIGroup *grp = _to;
  float best2 = 1e30;
  float sbest2 = best2;
  AIBrain *nearestUnit = _from;
  for (int i=0; i<grp->NUnits(); i++)
  {
    AIUnit *unit = grp->GetUnit(i);
    if (!unit) continue;
#if 0
    // diagnoze unit selection and its MARTA type
    int martaIx = GWorld->GetMartaType(unit->GetVehicle()->GetType());
    RString martaName = martaIx>=0 ? GWorld->_martaType[martaIx]._className : "UNKNOWN";
    LogF(" Unit: %x Vehicle: %s Marta:%s%s", (int)unit, cc_cast(unit->GetVehicle()->GetDebugName()), cc_cast(martaName), !unit->IsUnit() ? " <-SKIPPED" : ""); 
#endif
    if ( !unit->IsUnit() ) continue; //use only commander from each vehicle or standalone unit

    float dist2 = unit->Position(unit->GetFutureVisualState()).DistanceXZ2(tgt->position);
    if (dist2 < best2)
    {
      nearestUnit = unit;
      sbest2 = best2; //shift
      best2 = dist2;
    }
    else if (dist2 < sbest2) sbest2 = dist2;
  }
  saturateMax(best2, 1); //at least 1 meter
  _distanceToUnit = sqrt(best2);
  saturateMax(sbest2, 1); //at least 1 meter
  _unitDistanceFactor = sqrt(best2 / sbest2);
  params.AddDistance(_distanceToUnit, martaTypeIx, this);
  // 10 - unitTgtDirection
  Vector3 absUnitVect = tgt->position-nearestUnit->Position(nearestUnit->GetFutureVisualState());
  float absUnitDir = atan2(absUnitVect.X(), absUnitVect.Z());
  params.AddDirection(relDir, absUnitDir, true, this);
  // 11 - locationTgtDistance
  float maxDist2 = min(0.25f*Square(_distanceToGroup),500.0f*500.0f); //not more than five hundred meters
  _location = FindNearestLocationWithDubbing(tgt->position, maxDist2);
  float absLocationDir = 0;
  if (_location)
  {
    if (_location->IsInside(_to->GetCorePosition()))
    { // group is inside location, the description using this location has no sense
      _distanceToLocation = 1e10; // do not use this location
    }
    else
    {
      Vector3 absLocationVect = tgt->position - _location->GetPosition();
      _distanceToLocation = absLocationVect.SizeXZ();
      absLocationDir = atan2(absLocationVect.X(),absLocationVect.Z());
    }
  }
  else _distanceToLocation = 1e10; //same as no Location found
  saturateMax(_distanceToLocation, 1); //at least 1 meter
  params.AddDistance(_distanceToLocation, martaTypeIx, this);
  // 12 - locationTgtDirection
  params.AddDirection(relDir, absLocationDir, true, this);
  // 13 - recipient
  params.Add(nearestUnit);
  // 14 - location
  params.AddLocation(_location);
  
  // Prepare Leader/Group mix position and direction
  Matrix4 transfLeader;
  transfLeader.SetOriented(_to->GetGroupDirection(), VUp);
  transfLeader.SetPosition(_to->Leader()->Position(_to->Leader()->GetFutureVisualState()));
  Vector3 leaderAbsDirVect = tgt->position-_to->Leader()->Position(_to->Leader()->GetFutureVisualState());
  float leaderAbsDir = atan2(leaderAbsDirVect.X(), leaderAbsDirVect.Z());
  float distanceToLeader = leaderAbsDirVect.SizeXZ();
  Vector3 leaderDir = transfLeader.InverseRotation().FastTransform(tgt->position);
  float leaderRelDir = atan2(leaderDir.X(),leaderDir.Z());
  // 15 - leaderTgtDistance
  params.AddDistance(distanceToLeader, martaTypeIx, this);
  // 16 - leaderTgtDirection
  params.AddDirection(leaderRelDir, leaderAbsDir, false, this);
  ::ShowGroupDir(_to); //TODO, cannot be shown always

#if DIAG_SELECT_ENEMY_DETECTED_SENTENCE
  LogF("*** RadioMessageReportTarget log:");
  LogF(" leader: (%.1f,%.1f)", _to->Leader()->Position().X(), _to->Leader()->Position().Z());
  LogF(" core:   (%.1f,%.1f) %.1f, radius: %.1f", _to->GetCorePosition().X(), _to->GetCorePosition().Z(), _distanceToGroup, _groupCoreRadius);
  LogF(" unit:   (%.1f,%.1f) %.1f", nearestUnit->Position().X(), nearestUnit->Position().Z(), _distanceToUnit);
  LogF(" sender: (%.1f,%.1f) %.1f", _from->Position().X(), _from->Position().Z(), _distanceToSender);
  LogF(" unitDistFactor: %.2f", _unitDistanceFactor);
#endif

  return "SelectEnemyDetectedSentence";
}

/*!
\patch 5115 Date 1/10/2007 by Jirka
- Fixed: Random crashes during reporting of targets (since 5111)
*/

void RadioMessageReportTarget::Transmitted(ChatChannel channel)
{
  if (_to && UnitAlive(_from))
  {
    // we have heard about the target, therefore we certainly know about it
    // use group target list - not available for agents?
    //TargetList &list = _to->GetTargetList();
    for (int i=0; i<_targets.Size(); i++)
    {
      TargetNormal *tgt = _targets[i].tgt;
      if (!tgt) continue;
      // avoid too quick reaction by providing some small delay
      Time recent = Glob.time+0.3f;
      if (tgt->delay>recent)
      {
        tgt->delay = recent;
      }
      if (tgt->delaySensor>recent)
      {
        tgt->delaySensor = recent;
      }
      //list.FindTarget(id)
    }

    if (_to->IsPlayerGroup())
    {
      void AddEnemyInfo(TargetType *id, const VehicleType *type, int x, int z);
      for (int i=0; i<_targets.Size(); i++)
      {
        TargetNormal *tgt = _targets[i].tgt;
        if (tgt && _to->GetCenter()->IsEnemy(tgt->side))
        {
          AddEnemyInfo(tgt->idExact, tgt->type, _x, _z);
        }
      }
    }
  }
  // nothing to do
}

bool RadioMessageReportTarget::IsTarget(TargetType *id) const
{
  for (int i=0; i<_targets.Size(); i++)
  {
    const ReportTargetInfo &target = _targets[i];
    if (target.tgt && target.tgt->idExact == id) return true;
  }
  return false;
}

bool RadioMessageReportTarget::HasType(const VehicleType *type) const
{
  for (int i=0; i<_targets.Size(); i++)
  {
    const ReportTargetInfo &target = _targets[i];
    if (target.tgt && target.tgt->type == type) return true;
  }
  return false;
}

void RadioMessageReportTarget::DeleteTarget(TargetType *id)
{
  int i=0;
  while (i<_targets.Size())
  {
    ReportTargetInfo &target = _targets[i];
    if (target.tgt && target.tgt->idExact == id)
    {
      _targets.Delete(i);
      continue;
    }
    i++;
  }
}

void RadioMessageReportTarget::AddTarget(TargetNormal &target)
{
  int index = _targets.Add();
  _targets[index].tgt = &target;
}

///////////////////////////////////////////////////////////////////////////////
// class RadioMessageNotifyCommand

RadioMessageNotifyCommand::RadioMessageNotifyCommand(const OLinkPermNOArray(AIUnit) &list, AIGroup *to, Command &command)
: base(list[0], to)
{
  _command = command;
  _list = list;
}

LSError RadioMessageNotifyCommand::Serialize(ParamArchive &ar)
{
  CHECK(base::Serialize(ar))
  CHECK(ar.Serialize("Command", _command, 1))
  CHECK(ar.SerializeRefs("List", _list, 1))
  return LSOK;
}

const char *RadioMessageNotifyCommand::GetPriorityClass()
{
  return "Notify";
}

RString RadioMessageNotifyCommand::PrepareSentence(SentenceParams &params) const
{
  if (!UnitAlive(_from) || !_to) return RString();
  
  params.Add(_list,false);
  // check if player is involved somehow
  bool focus = false;
  for (int i=0; i<_list.Size(); i++)
  {
    if(_list[i] == GWorld->FocusOn()) focus = true;
  
  }
  SetPlayerMsg(focus);

  switch (_command._message)
  {
  case Command::Attack:
  case Command::AttackAndFire:
    {
      Target *tgt=_command._targetE;
      if( !tgt ) tgt=_from->FindTarget(_command._target);
      if (tgt)
      {
        const EntityAIType *type = tgt->GetType();
        params.AddWord(type->GetNameSound(), type->GetShortName());
      }
      else
      {
        params.AddWord("unknown", LocalizeString(IDS_WORD_UNKNOWN));
      }
      // different notification when multiple units are attacking
      AISubgroup *subgrp = _from->GetSubgroup();
      if (subgrp && subgrp->NUnits() > 1)
      {
        params.Add(subgrp, false);
        return "SentNotifyAttackSubgroup";
      }
    }
    return "SentNotifyAttack";
  case Command::FireAtPosition:
    {
      Target *tgt=_command._targetE;
      if( !tgt ) tgt=_from->FindTarget(_command._target);
      if (tgt)
      {
        const EntityAIType *type = tgt->GetType();
        params.AddWord(type->GetNameSound(), type->GetShortName());
      }
      else
      {
        params.AddWord("unknown", LocalizeString(IDS_WORD_UNKNOWN));
      }
      // different notification when multiple units are attacking
      AISubgroup *subgrp = _from->GetSubgroup();
      if (subgrp && subgrp->NUnits() > 1)
      {
        params.Add(subgrp, false);
        return "SentNotifyAttackSubgroup";
      }
    }
    return "SentNotifyAttack";
  case Command::Support:
    {
      EntityAI *target = _command._target;
      if (!target) return RString();
      params.AddWord(target->GetType()->GetNameSound(), target->GetType()->GetShortName());
    }
    return "SentNotifySupport";
  default:
    return RString();
  }
}

void RadioMessageNotifyCommand::Transmitted(ChatChannel channel)
{
}

///////////////////////////////////////////////////////////////////////////////
// class RadioMessageObjectDestroyed

RadioMessageObjectDestroyed::RadioMessageObjectDestroyed(AIUnit *from, AIGroup *to, const VehicleType *type)
: base(from, to)
{
  _vehicleType = type;
}

LSError RadioMessageObjectDestroyed::Serialize(ParamArchive &ar)
{
  CHECK(base::Serialize(ar))
  CHECK(::Serialize(ar, "vehicleType", _vehicleType, 1))
  return LSOK;
}

const char *RadioMessageObjectDestroyed::GetPriorityClass()
{
  return "Completition";
}

void RadioMessageObjectDestroyed::Transmitted(ChatChannel channel)
{
}

RString RadioMessageObjectDestroyed::PrepareSentence(SentenceParams &params) const
{
  if (!_to || !UnitAlive(_from)) return RString();

  SetPlayerMsg(_from == GWorld->FocusOn());

  // who reported
  params.Add(_from);

  // target
  if (_vehicleType)
  {
    params.AddWord(_vehicleType->GetNameSound(), _vehicleType->GetShortName());
    return "SentObjectDestroyed";
  }
  else
  {
    return "SentObjectDestroyedUnknown";
  }
}

///////////////////////////////////////////////////////////////////////////////
// class RadioMessageContact

RadioMessageContact::RadioMessageContact(AIUnit *from, AIGroup *to)
: base(from, to)
{
}

LSError RadioMessageContact::Serialize(ParamArchive &ar)
{
  CHECK(base::Serialize(ar))
  return LSOK;
}

const char *RadioMessageContact::GetPriorityClass()
{
  return "UrgentCommand";
}

void RadioMessageContact::Transmitted(ChatChannel channel)
{
  if (!_to) return;

}

RString RadioMessageContact::PrepareSentence(SentenceParams &params) const
{
  if (!_from || !_to) return RString();

  // who reported
  params.Add(_from);

  return "SentContact";
}

RadioMessageClear::RadioMessageClear(AIUnit *from, AIGroup *to)
: base(from, to)
{
}

LSError RadioMessageClear::Serialize(ParamArchive &ar)
{
  CHECK(base::Serialize(ar))
  return LSOK;
}

const char *RadioMessageClear::GetPriorityClass()
{
  return "NormalCommand";
}

void RadioMessageClear::Transmitted(ChatChannel channel)
{
}

RString RadioMessageClear::PrepareSentence(SentenceParams &params) const
{
  if (!_from || !_to) return RString();

  // who reported
  params.Add(_from);

  return "SentClear";
}

///////////////////////////////////////////////////////////////////////////////
// class RadioMessageRepeatCommand

RadioMessageRepeatCommand::RadioMessageRepeatCommand(AIUnit *from, AIGroup *to)
: base(from, to)
{
}

LSError RadioMessageRepeatCommand::Serialize(ParamArchive &ar)
{
  CHECK(base::Serialize(ar))
  return LSOK;
}

const char *RadioMessageRepeatCommand::GetPriorityClass()
{
  return "NormalCommand";
}

void RadioMessageRepeatCommand::Transmitted(ChatChannel channel)
{
  if (!_from || !_to || !_to->Leader()) return;
  AISubgroup *subgrp = _from->GetSubgroup();
  if (!subgrp) return;
  Command *cmd = subgrp->GetCommand();
  if (!cmd) return;
  OLinkPermNOArray(AIUnit) list;
  subgrp->GetUnitsList(list);
  _to->GetRadio().Transmit(new RadioMessageCommand(_to, list, *cmd, false, false));
}

RString RadioMessageRepeatCommand::PrepareSentence(SentenceParams &params) const
{
  if (!_from || !_to) return RString();

  // who reported
  params.Add(_from);

  return "SentRepeatCommand";
}

///////////////////////////////////////////////////////////////////////////////
// class RadioMessageWhereAreYou

RadioMessageWhereAreYou::RadioMessageWhereAreYou(AIUnit *from, AIGroup *to)
: base(from, to)
{
}

LSError RadioMessageWhereAreYou::Serialize(ParamArchive &ar)
{
  CHECK(base::Serialize(ar))
  return LSOK;
}

const char *RadioMessageWhereAreYou::GetPriorityClass()
{
  return "NormalCommand";
}

void RadioMessageWhereAreYou::Transmitted(ChatChannel channel)
{
  if (!_to) return;
  AIUnit *leader = _to->Leader();
  if (!leader || leader->IsAnyPlayer()) return;
  if (!leader->LSIsAlive())
  {
    _to->SetReportBeforeTime(leader,Glob.time+10);
  }
  else
  {
    leader->SendAnswer(AI::ReportPosition);
  }
}

RString RadioMessageWhereAreYou::PrepareSentence(SentenceParams &params) const
{
  if (!_from || !_to) return RString();

  // who reported
  params.Add(_from);

  return "SentWhereAreYou";
}

///////////////////////////////////////////////////////////////////////////////
// class RadioMessageCommandConfirm

RadioMessageCommandConfirm::RadioMessageCommandConfirm(AIUnit *from, AIGroup *to, Command &command)
: base(from, to)
{
  _command = command;
}

LSError RadioMessageCommandConfirm::Serialize(ParamArchive &ar)
{
  CHECK(base::Serialize(ar))
  CHECK(ar.Serialize("Command", _command, 1))
  return LSOK;
}

const char *RadioMessageCommandConfirm::GetPriorityClass()
{
  return "Confirmation";
}

RString RadioMessageCommandConfirm::PrepareSentence(SentenceParams &params) const
{
  if (!_from || !_from->LSCanSpeak()) return RString();

  SetPlayerMsg(_from == GWorld->FocusOn());

  params.Add(_from);

  if
  (
    _command._message == Command::Attack ||
    _command._message == Command::AttackAndFire ||
    _command._message == Command::FireAtPosition
  )
  {
    return "SentConfirmAttack";
  }
  else if (_command._message == Command::Move)
    return "SentConfirmMove";
  else
    return "SentConfirmOther";
}

void RadioMessageCommandConfirm::Transmitted(ChatChannel channel)
{
  if (!_from) return;
  if (UnitAlive(_from)) return;
  // unit should report, but does not
  AIGroup *grp = _from->GetGroup();
  if (grp)
  {
    AIUnit *leader = grp->Leader();
    if (!leader || leader->CanHear(channel, _from))
      grp->SetReportBeforeTime(_from,Glob.time+5);
  }
}

///////////////////////////////////////////////////////////////////////////////
// class RadioMessageFormation

LSError RadioMessageFormation::Serialize(ParamArchive &ar)
{
  CHECK(RadioMessage::Serialize(ar))
  CHECK(ar.SerializeRef("From", _from, 1))
  CHECK(ar.SerializeRef("To", _to, 1))
  CHECK(ar.SerializeEnum("formation", _formation, 1, AI::FormLine))
  return LSOK;
}

const char *RadioMessageFormation::GetPriorityClass()
{
  return "NormalCommand";
}

RString RadioMessageFormation::PrepareSentence(SentenceParams &params) const
{
  if (!_from || !_from->Leader()) return RString();

  params.Add(_from->Leader());
  switch (_formation)
  {
    case AI::FormColumn:
      return "SentFormColumn";
    case AI::FormStaggeredColumn:
      return "SentFormStaggeredColumn";
    case AI::FormWedge:
      return "SentFormWedge";
    case AI::FormEcholonLeft:
      return "SentFormEcholonLeft";
    case AI::FormEcholonRight:
      return "SentFormEcholonRight";
    case AI::FormVee:
      return "SentFormVee";
    default:
    case AI::FormLine:
      return "SentFormLine";
    case AI::FormDiamond:
      return "SentFormDiamond";
    case AI::FormFile:
      return "SentFormFile";
  }
}

void RadioMessageFormation::Transmitted(ChatChannel channel)
{
  if (_to)
    _to->SetFormation(_formation);
}

///////////////////////////////////////////////////////////////////////////////
// class RadioMessageSemaphoreState

RadioMessageState::RadioMessageState(AIGroup *from, OLinkPermNOArray(AIUnit) &to)
{
  _from = from;
  _to = to;
}

RadioMessageState::RadioMessageState()
{
}

bool RadioMessageState::IsTo(AIBrain *unit) const
{
  for (int i=0; i<_to.Size(); i++)
  {
    AIUnit *u = _to[i];
    if (u == unit)
      return true;
  }
  return false;
}

bool RadioMessageState::IsOnlyTo(AIBrain *unit) const
{
  bool ret = false;
  for (int i=0; i<_to.Size(); i++)
  {
    AIUnit *u = _to[i];
    if (!u) continue;
    if (u != unit) return false;
    else ret = true;
  }
  return ret;
}

void RadioMessageState::DeleteTo(AIUnit *unit)
{
  for (int i=0; i<_to.Size(); i++)
  {
    AIUnit *u = _to[i];
    if (u == unit)
    {
      _to.Delete(i);
      return;
    }
  }
}

void RadioMessageState::ClearTo()
{
  _to.Clear();
}

void RadioMessageState::AddTo(AIUnit *unit)
{
  Assert(!IsTo(unit));
  _to.Add(unit);
}

bool RadioMessageState::IsToSomeone() const
{
  for (int i=0; i<_to.Size(); i++)
  {
    if (_to[i]) return true;
  }
  return false;
}

const char *RadioMessageState::GetPriorityClass()
{
  return "NormalCommand";
}

LSError RadioMessageState::Serialize(ParamArchive &ar)
{
  CHECK(base::Serialize(ar))
  CHECK(ar.SerializeRef("From", _from, 1))
  CHECK(ar.SerializeRefs("To", _to, 1))
  return LSOK;
}


///////////////////////////////////////////////////////////////////////////////
// class RadioMessageBehaviour

RadioMessageBehaviour::RadioMessageBehaviour(AIGroup *from, OLinkPermNOArray(AIUnit) &to, CombatMode behaviour)
:base(from, to)
{
  _behaviour = behaviour;
}

LSError RadioMessageBehaviour::Serialize(ParamArchive &ar)
{
  CHECK(base::Serialize(ar))
  CHECK(ar.SerializeEnum("behaviour", _behaviour, 1, CMAware))
  return LSOK;
}

const char *RadioMessageBehaviour::GetPriorityClass()
{
  return "NormalCommand";
}


RString RadioMessageBehaviour::PrepareSentence(SentenceParams &params) const
{
  if (!_from || !_from->Leader()) return RString();
  if (_to.Count() <= 0) return RString();

  SetPlayerMsg(IsTo(GWorld->FocusOn()));

  params.Add(_to, false);
  params.Add(_from->Leader());

  switch (_behaviour)
  {
  case CMCareless:
    return "SentBehaviourCareless";
  case CMSafe:
    return "SentBehaviourSafe";
  case CMAware:
    return "SentBehaviourAware";
  case CMCombat:
    return "SentBehaviourCombat";
  case CMStealth:
    return "SentBehaviourStealth";
  default:
    Fail("Behaviour");
    return RString();
  }
}

void RadioMessageBehaviour::Transmitted(ChatChannel channel)
{
  AIBrain *sender = GetSender();

  int i;
  for (i=0; i<_to.Size(); i++)
  {
    AIUnit *unit = _to[i];
    if (unit)
    {
      if (sender && !unit->CanHear(channel, sender)) continue;
      unit->SetCombatModeMajorReactToChange(_behaviour);
    }
  }
}

RadioMessageSuppress::RadioMessageSuppress(AIGroup *from, OLinkPermNOArray(AIUnit) &to, Time until)
:base(from, to)
{
  _until = until;
}

LSError RadioMessageSuppress::Serialize(ParamArchive &ar)
{
  CHECK(base::Serialize(ar))
  CHECK(::Serialize(ar,"suppressUntil", _until, 1, Time(0)))
  return LSOK;
}

const char *RadioMessageSuppress::GetPriorityClass()
{
  return "UrgentCommand";
}


RString RadioMessageSuppress::PrepareSentence(SentenceParams &params) const
{
  if (!_from || !_from->Leader()) return RString();
  if (_to.Count() <= 0) return RString();

  SetPlayerMsg(IsTo(GWorld->FocusOn()));

  params.Add(_to, false);
  params.Add(_from->Leader());

  return "SentSuppress";
}

void RadioMessageSuppress::Transmitted(ChatChannel channel)
{
  AIBrain *sender = GetSender();

  int i;
  for (i=0; i<_to.Size(); i++)
  {
    AIUnit *unit = _to[i];
    if (unit)
    {
      if (sender && !unit->CanHear(channel, sender)) continue;
      if (!unit->IsUnit()) continue;
      if (!unit->GetVehicle()) continue;
      unit->GetVehicle()->SuppressUntil(_until);
    }
  }
}

///////////////////////////////////////////////////////////////////////////////
// class RadioMessageOpenFire

static const EnumName OpenFireNames[]=
{
  EnumName(OFSNeverFire, "NEVER FIRE"),
  EnumName(OFSHoldFire, "HOLD FIRE"),
  EnumName(OFSOpenFire, "OPEN FIRE"),
  EnumName(OFSHoldFire, "0"),
  EnumName(OFSOpenFire, "1"),
  EnumName()
};
template<>
const EnumName *GetEnumNames(OpenFireState dummy)
{
  return OpenFireNames;
}

RadioMessageOpenFire::RadioMessageOpenFire(AIGroup *from, OLinkPermNOArray(AIUnit) &to, OpenFireState open)
:base(from, to)
{
  _open = open;
}

LSError RadioMessageOpenFire::Serialize(ParamArchive &ar)
{
  CHECK(base::Serialize(ar))
  CHECK(ar.SerializeEnum("open", _open, 1, OFSOpenFire))
  return LSOK;
}

const char *RadioMessageOpenFire::GetPriorityClass()
{
  switch (_open)
  {
  case OFSNeverFire:
  case OFSHoldFire:
    return "UrgentCommand";
  default:
    Fail("Open fire status");
  case OFSOpenFire:
    return "NormalCommand";
  }
}


RString RadioMessageOpenFire::PrepareSentence(SentenceParams &params) const
{
  if (!_from || !_from->Leader()) return RString();
  if (_to.Count() <= 0) return RString();

  SetPlayerMsg(IsTo(GWorld->FocusOn()));

  // Remove any messages destined for AIUnit without a ID greater than 0
  // this is because dead units are still alive
  for (int i = 0; i < _to.Size(); i++)
  {
    const AIUnit *unit = _to[i];
    if (!unit || unit->ID()==0)
    {
      _to.Delete(i);
      --i;
    }
  }

  params.Add(_to, false);
  params.Add(_from->Leader());

  switch (_open)
  {
  case OFSNeverFire:
  case OFSHoldFire:
    if (_from->GetCombatModeMinor() >= CMCombat)
      return "SentHoldFireInCombat";
    else
      return "SentHoldFire";
  default:
    Fail("Open Fire State");
  case OFSOpenFire:
    if (_from->GetCombatModeMinor() >= CMCombat)
      return "SentOpenFireInCombat";
    else
      return "SentOpenFire";
  }
}

AI::Semaphore ApplyOpenFire(AI::Semaphore s, OpenFireState open)
{
  switch (s)
  {
  case AI::SemaphoreBlue:
    switch (open)
    {
    case OFSHoldFire:
      s = AI::SemaphoreGreen;
      break;
    case OFSOpenFire:
      s = AI::SemaphoreYellow;
      break;
    }
    break;
  case AI::SemaphoreGreen:
    switch (open)
    {
    case OFSNeverFire:
      s = AI::SemaphoreBlue;
      break;
    case OFSOpenFire:
      s = AI::SemaphoreYellow;
      break;
    }
    break;
  case AI::SemaphoreWhite:
    switch (open)
    {
    case OFSNeverFire:
      s = AI::SemaphoreBlue;
      break;
    case OFSOpenFire:
      s = AI::SemaphoreRed;
      break;
    }
    break;
  case AI::SemaphoreYellow:
    switch (open)
    {
    case OFSNeverFire:
      s = AI::SemaphoreBlue;
      break;
    case OFSHoldFire:
      s = AI::SemaphoreGreen;
      break;
    }
    break;
  case AI::SemaphoreRed:
    switch (open)
    {
    case OFSNeverFire:
      s = AI::SemaphoreBlue;
      break;
    case OFSHoldFire:
      s = AI::SemaphoreWhite;
      break;
    }
    break;
  }
  return s;
}

void RadioMessageOpenFire::Transmitted(ChatChannel channel)
{
  AIBrain *sender = GetSender();

  int i;
  for (i=0; i<_to.Size(); i++)
  {
    AIUnit *unit = _to[i];
    if (unit)
    {
      if (sender && !unit->CanHear(channel, sender)) continue;
      AI::Semaphore s = unit->GetSemaphore();
      s = ApplyOpenFire(s, _open);
      unit->SetSemaphore(s);
    }
  }
}

///////////////////////////////////////////////////////////////////////////////
// class RadioMessageCeaseFire

RadioMessageCeaseFire::RadioMessageCeaseFire(AIBrain *from, AIBrain *to, bool insideGroup)
{
  _from = from;
  _to = to;
  _insideGroup = insideGroup;
}

LSError RadioMessageCeaseFire::Serialize(ParamArchive &ar)
{
  CHECK(base::Serialize(ar))
  CHECK(ar.SerializeRef("from", _from, 1))
  CHECK(ar.SerializeRef("to", _to, 1))
  CHECK(ar.Serialize("insideGroup", _insideGroup, 1, true))
  return LSOK;
}

const char *RadioMessageCeaseFire::GetPriorityClass()
{
  return "UrgentCommand";
}


RString RadioMessageCeaseFire::PrepareSentence(SentenceParams &params) const
{
  if (!_from || !_to || !_to->GetGroup()) return RString();
  if (!_from->LSCanSpeak()) return RString();
  SetPlayerMsg(_to == GWorld->FocusOn());

  if (_insideGroup)
  {
    params.Add(_to);
    return "SentCeaseFireInsideGroup";
  }
  else
  {
    params.AddWord("", _to->GetGroup()->GetName());
    params.Add(_to);
    return "SentCeaseFire";
  }
}

void RadioMessageCeaseFire::Transmitted(ChatChannel channel)
{
}

///////////////////////////////////////////////////////////////////////////////
// class RadioMessageLooseFormation

RadioMessageLooseFormation::RadioMessageLooseFormation(AIGroup *from, OLinkPermNOArray(AIUnit) &to, bool loose)
:base(from, to)
{
  _loose = loose;
}

LSError RadioMessageLooseFormation::Serialize(ParamArchive &ar)
{
  CHECK(base::Serialize(ar))
  CHECK(ar.Serialize("loose", _loose, 1, false))
  return LSOK;
}

const char *RadioMessageLooseFormation::GetPriorityClass()
{
  return "NormalCommand";
}


RString RadioMessageLooseFormation::PrepareSentence(SentenceParams &params) const
{
  if (!_from || !_from->Leader()) return RString();
  if (_to.Count() <= 0) return RString();

  SetPlayerMsg(IsTo(GWorld->FocusOn()));

  params.Add(_to, false);
  params.Add(_from->Leader());
  if (_loose)
    return "SentLooseFormation";
  else
    return "SentKeepFormation";
}

AI::Semaphore ApplyLooseFormation(AI::Semaphore s, bool loose)
{
  switch (s)
  {
  case AI::SemaphoreBlue:
    if (loose) s = AI::SemaphoreWhite;
    break;
  case AI::SemaphoreGreen:
    if (loose) s = AI::SemaphoreWhite;
    break;
  case AI::SemaphoreWhite:
    if (!loose) s = AI::SemaphoreGreen;
    break;
  case AI::SemaphoreYellow:
    if (loose) s = AI::SemaphoreRed;
    break;
  case AI::SemaphoreRed:
    if (!loose) s = AI::SemaphoreYellow;
    break;
  }
  return s;
}

void RadioMessageLooseFormation::Transmitted(ChatChannel channel)
{
  AIBrain *sender = GetSender();

  int i;
  for (i=0; i<_to.Size(); i++)
  {
    AIUnit *unit = _to[i];
    if (unit)
    {
      if (sender && !unit->CanHear(channel, sender)) continue;

      AI::Semaphore s = unit->GetSemaphore();
      s = ApplyLooseFormation(s, _loose);
      unit->SetSemaphore(s);
    }
  }
}

///////////////////////////////////////////////////////////////////////////////
// class RadioMessageFormationPos

static const EnumName FormationPosNames[]=
{
  EnumName(AI::PosInFormation, "InFormation"),
  EnumName(AI::PosAdvance, "Advance"),
  EnumName(AI::PosStayBack, "StayBack"),
  EnumName(AI::PosFlankLeft, "FlankLeft"),
  EnumName(AI::PosFlankRight, "FlankRight"),
  EnumName()
};
template<>
const EnumName *GetEnumNames(AI::FormationPos dummy)
{
  return FormationPosNames;
}

RadioMessageFormationPos::RadioMessageFormationPos(AIGroup *from, OLinkPermNOArray(AIUnit) &to, AI::FormationPos state)
:base(from, to)
{
  _state = state;
}

LSError RadioMessageFormationPos::Serialize(ParamArchive &ar)
{
  CHECK(base::Serialize(ar))
  CHECK(ar.SerializeEnum("state", _state, 1, AI::PosInFormation))
  return LSOK;
}

RString RadioMessageFormationPos::PrepareSentence(SentenceParams &params) const
{
  if (!_from || !_from->Leader()) return RString();
  if (_to.Count() <= 0) return RString();

  SetPlayerMsg(IsTo(GWorld->FocusOn()));

  params.Add(_to, true);
  params.Add(_from->Leader());
  switch (_state)
  {
    case AI::PosAdvance:
      return "SentFormPosAdvance";
    case AI::PosStayBack:
      return "SentFormPosStayBack";
    case AI::PosFlankLeft:
      return "SentFormPosFlankLeft";
    case AI::PosFlankRight:
      return "SentFormPosFlankRight";
    default:
      Fail("FormationPos");
      return RString();
  }
}

void RadioMessageFormationPos::Transmitted(ChatChannel channel)
{
  AIBrain *sender = GetSender();

  int i;
  for (i=0; i<_to.Size(); i++)
  {
    AIUnit *unit = _to[i];
    if (unit)
    {
      if (sender && !unit->CanHear(channel, sender)) continue;
      unit->AddFormationPos(_state);
    }
  }
}

///////////////////////////////////////////////////////////////////////////////
// class RadioMessageUnitPos

static const EnumName UnitPosNames[]=
{
  EnumName(UPUp, "Up"),
  EnumName(UPMiddle, "Middle"),
  EnumName(UPDown, "Down"),
  EnumName(UPAuto, "Auto"),
  EnumName()
};
template<>
const EnumName *GetEnumNames(UnitPosition dummy)
{
  return UnitPosNames;
}

RadioMessageUnitPos::RadioMessageUnitPos(AIGroup *from, OLinkPermNOArray(AIUnit) &to, UnitPosition state)
:base(from, to)
{
  _state = state;
}

LSError RadioMessageUnitPos::Serialize(ParamArchive &ar)
{
  CHECK(base::Serialize(ar))
  CHECK(ar.SerializeEnum("state", _state, 1, ENUM_CAST(UnitPosition,UPAuto)))
  return LSOK;
}

RString RadioMessageUnitPos::PrepareSentence(SentenceParams &params) const
{
  if (!_from || !_from->Leader()) return RString();
  if (_to.Count() <= 0) return RString();

  SetPlayerMsg(IsTo(GWorld->FocusOn()));

  params.Add(_to, false);
  params.Add(_from->Leader());
  switch (_state)
  {
    case UPDown:
      return "SentUnitPosDown";
    case UPMiddle:
      return "SentUnitPosMiddle";
    case UPUp:
      return "SentUnitPosUp";
    case UPAuto:
      return "SentUnitPosAuto";
    default:
      Fail("UnitPos");
      return RString();
  }
}

void RadioMessageUnitPos::Transmitted(ChatChannel channel)
{
  AIBrain *sender = GetSender();

  int i;
  for (i=0; i<_to.Size(); i++)
  {
    AIUnit *unit = _to[i];
    if (unit)
    {
      if (sender && !unit->CanHear(channel, sender)) continue;
      unit->SetUnitPositionCommanded(_state);
    }
  }
}

/////////////////////////////////

RadioMessageWatchAround::RadioMessageWatchAround(AIGroup *from, OLinkPermNOArray(AIUnit) &to)
:base(from, to)
{
}

LSError RadioMessageWatchAround::Serialize(ParamArchive &ar)
{
  CHECK(base::Serialize(ar))
  return LSOK;
}

void RadioMessageWatchAround::Transmitted(ChatChannel channel)
{
  AIBrain *sender = GetSender();

  int i;
  for (i=0; i<_to.Size(); i++)
  {
    AIUnit *unit = _to[i];
    if (!unit) continue;
    if (sender && !unit->CanHear(channel, sender)) continue;
    unit->SetWatchAround();
    // watch commands to vehicle commander are transmitted to vehicle gunner as well
    Transport *vehicle = unit->GetVehicleIn();
    if (vehicle && vehicle->CommanderUnit() == unit && !unit->IsAnyPlayer())
    {
      AIBrain *gunner = vehicle->GunnerUnit();
      if (gunner && gunner != unit && !gunner->IsAnyPlayer())
      {
        gunner->SetWatchAround();
      }
    }
  }
}

RString RadioMessageWatchAround::PrepareSentence(SentenceParams &params) const
{
  if (!_from || !_from->Leader()) return RString();
  if (_to.Count() <= 0) return RString();

  SetPlayerMsg(IsTo(GWorld->FocusOn()));

  params.Add(_to, false);
  params.Add(_from->Leader());
  return "SentWatchAround";
}

/////////////////////////////////

RadioMessageWatchAuto::RadioMessageWatchAuto(AIGroup *from, OLinkPermNOArray(AIUnit) &to)
:base(from, to)
{
}

LSError RadioMessageWatchAuto::Serialize(ParamArchive &ar)
{
  CHECK(base::Serialize(ar))
  return LSOK;
}

void RadioMessageWatchAuto::Transmitted(ChatChannel channel)
{
  AIBrain *sender = GetSender();

  int i;
  for (i=0; i<_to.Size(); i++)
  {
    AIUnit *unit = _to[i];
    if (!unit) continue;
    if (sender && !unit->CanHear(channel, sender)) continue;
    // translate command from leader to his vehicle
    Transport *vehicle = unit->GetVehicleIn();
    TurretContext context;
    if (vehicle && vehicle->CommanderUnit() != unit && vehicle->FindTurret(unit->GetPerson(), context) && context._gunner)
    {
      vehicle->ReceivedTarget(context._gunner, NULL);
      continue;
    }
    unit->SetNoWatch();
  }
}

RString RadioMessageWatchAuto::PrepareSentence(SentenceParams &params) const
{
  if (!_from || !_from->Leader()) return RString();
  if (_to.Count() <= 0) return RString();

  SetPlayerMsg(IsTo(GWorld->FocusOn()));

  params.Add(_to, false);
  params.Add(_from->Leader());
  return "SentNoTarget";
}

/////////////////////////////////

RadioMessageWatchDir::RadioMessageWatchDir(AIGroup *from, OLinkPermNOArray(AIUnit) &to, Vector3Val dir)
:base(from, to)
{
  _dir = dir;
}

LSError RadioMessageWatchDir::Serialize(ParamArchive &ar)
{
  CHECK(base::Serialize(ar))
  CHECK(::Serialize(ar, "dir", _dir, 1))
  return LSOK;
}

void RadioMessageWatchDir::Transmitted(ChatChannel channel)
{
  AIBrain *sender = GetSender();

  int i;
  for (i=0; i<_to.Size(); i++)
  {
    AIUnit *unit = _to[i];
    if (!unit) continue;
    if (sender && !unit->CanHear(channel, sender)) continue;
    unit->SetWatchDirection(_dir);
    // watch commands to vehicle commander are transmitted to vehicle gunner as well
    Transport *vehicle = unit->GetVehicleIn();
    if (vehicle && vehicle->CommanderUnit() == unit && !unit->IsAnyPlayer())
    {
      AIBrain *gunner = vehicle->GunnerUnit();
      if (gunner && gunner != unit && !gunner->IsAnyPlayer())
      {
        gunner->SetWatchDirection(_dir);
      }
    }
  }
}

RString RadioMessageWatchDir::PrepareSentence(SentenceParams &params) const
{
  if (!_from || !_from->Leader()) return RString();
  if (_to.Count() <= 0) return RString();

  SetPlayerMsg(IsTo(GWorld->FocusOn()));

  params.Add(_to, false);
  params.AddAzimutDir(_dir);
  ::ShowGroupDir(_to);
  params.Add(_from->Leader());
  return "SentWatchDir";
}

/////////////////////////////////

RadioMessageWatchPos::RadioMessageWatchPos(AIGroup *from, OLinkPermNOArray(AIUnit) &to, Vector3Val pos)
:base(from, to)
{
  _pos = pos;
}

LSError RadioMessageWatchPos::Serialize(ParamArchive &ar)
{
  CHECK(base::Serialize(ar))
    CHECK(::Serialize(ar, "pos", _pos, 1))
  return LSOK;
}

void RadioMessageWatchPos::Transmitted(ChatChannel channel)
{
  AIBrain *sender = GetSender();

  int i;
  for (i=0; i<_to.Size(); i++)
  {
    AIUnit *unit = _to[i];
    if (!unit) continue;
    if (sender && !unit->CanHear(channel, sender)) continue;
    // translate command from leader to his vehicle
    Transport *vehicle = unit->GetVehicleIn();
    TurretContext context;
    if (vehicle && vehicle->CommanderUnit() != unit && vehicle->FindTurret(unit->GetPerson(), context) && context._gunner)
    {
      vehicle->ReceivedWatchPos(context._gunner, _pos);
      continue;
    }
    unit->SetWatchPosition(_pos);
    // watch commands to vehicle commander are transmitted to vehicle gunner as well
    if (vehicle && vehicle->CommanderUnit() == unit && !unit->IsAnyPlayer())
    {
      AIBrain *gunner = vehicle->GunnerUnit();
      if (gunner && gunner != unit && !gunner->IsAnyPlayer())
      {
        gunner->SetWatchPosition(_pos);
      }
    }
  }
}

RString RadioMessageWatchPos::PrepareSentence(SentenceParams &params) const
{
  if (!_from || !_from->Leader()) return RString();
  if (_to.Count() <= 0) return RString();

  SetPlayerMsg(IsTo(GWorld->FocusOn()));

  params.Add(_to, false);
  params.AddAzimut(_pos);
  ::ShowGroupDir(_to);
  params.Add(_from->Leader());
  return "SentWatchPos";
}

/////////////////////////////////

RadioMessageWatchTgt::RadioMessageWatchTgt(AIGroup *from, OLinkPermNOArray(AIUnit) &to, Target *tgt)
:base(from, to)
{
  _tgt = tgt;
}

LSError RadioMessageWatchTgt::Serialize(ParamArchive &ar)
{
  CHECK(base::Serialize(ar))
  CHECK(ar.SerializeRef("tgt", _tgt, 1))
  return LSOK;
}

void RadioMessageWatchTgt::Transmitted(ChatChannel channel)
{
  AIBrain *sender = GetSender();

  int i;
  for (i=0; i<_to.Size(); i++)
  {
    AIUnit *unit = _to[i];
    if (!unit) continue;
    if (sender && !unit->CanHear(channel, sender)) continue;
    // translate command from leader to his vehicle
    Transport *vehicle = unit->GetVehicleIn();
    TurretContext context;
    if (vehicle && vehicle->CommanderUnit() != unit && vehicle->FindTurret(unit->GetPerson(), context) && context._gunner)
    {
      vehicle->ReceivedWatchTgt(context._gunner, _tgt);
      continue;
    }
    unit->SetWatchTarget(_tgt);
    // watch commands to vehicle commander are transmitted to vehicle gunner as well
    if (vehicle && vehicle->CommanderUnit() == unit && !unit->IsAnyPlayer())
    {
      AIBrain *gunner = vehicle->GunnerUnit();
      if (gunner && gunner != unit && !gunner->IsAnyPlayer())
      {
        gunner->SetWatchTarget(_tgt);
      }
    }
  }
}

RString RadioMessageWatchTgt::PrepareSentence(SentenceParams &params) const
{
  if (!_from || !_from->Leader()) return RString();
  if (_to.Count() <= 0) return RString();

  SetPlayerMsg(IsTo(GWorld->FocusOn()));

  params.Add(_to, false);

  Target *tgt=_tgt;
  if (tgt)
  {
    const EntityAIType *type = tgt->GetType();
    params.AddWord(type->GetNameSound(), type->GetShortName());
  }
  else
  {
    params.AddWord("unknown", LocalizeString(IDS_WORD_UNKNOWN));
  }
  params.Add(_from->Leader());
  return "SentWatchTgt";
}

///////////////////////////////////////////////////////////////////////////////
// class RadioMessageReportStatus

RadioMessageReportStatus::RadioMessageReportStatus(AIGroup *from, OLinkPermNOArray(AIUnit) &to)
  : base(from, to)
{
}

LSError RadioMessageReportStatus::Serialize(ParamArchive &ar)
{
  CHECK(base::Serialize(ar))
  return LSOK;
}

const char *RadioMessageReportStatus::GetPriorityClass()
{
  return "NormalCommand";
}

RString RadioMessageReportStatus::PrepareSentence(SentenceParams &params) const
{
  if (!_from || !_from->Leader()) return RString();
  if (_to.Count() <= 0) return RString();

  SetPlayerMsg(IsTo(GWorld->FocusOn()));

  params.Add(_to, false);
  params.Add(_from->Leader());

  return "SentReportStatus";
}

void RadioMessageReportStatus::Transmitted(ChatChannel channel)
{
  AIBrain *sender = GetSender();

  int i;


  // position should be reported always only by one AI unit in each formation
  FindArrayKey<AISubgroup *, FindArrayKeyTraits<AISubgroup *>, MemAllocLocal<AISubgroup *,16> > subgroupsReported;

  for (i=0; i<_to.Size(); i++)
  {
    AIUnit *unit = _to[i];
    if (!unit) continue;
    if (sender && !unit->CanHear(channel, sender)) continue;
    if (unit->LSCanSpeak())
    {
      bool position = subgroupsReported.AddUnique(unit->GetSubgroup())>=0;
      unit->ReportStatus(position);
      AIGroup *grp = unit->GetGroup();
      if (grp)
      {
        // reset status indication here
        // if it should be set, unit will report it soon
        grp->SetHealthStateReported(unit,AIUnit::RSNormal);
        grp->SetDamageStateReported(unit,AIUnit::RSNormal);
        grp->SetFuelStateReported(unit,AIUnit::RSNormal);
        grp->SetAmmoStateReported(unit,AIUnit::RSNormal);
      }
    }
    else
    {
      AIGroup *grp = unit->GetGroup();
      if (grp)
      {
        grp->SetReportBeforeTime(unit,Glob.time+5);
      }			
    }
  }
}

///////////////////////////////////////////////////////////////////////////////
// class RadioMessageTarget

RadioMessageTarget::RadioMessageTarget()
{
  _engage = false;
  _fire = false;
  _cancelTarget = false;
}

RadioMessageTarget::RadioMessageTarget
(
  AIGroup *from, OLinkPermNOArray(AIUnit) &to, Target *target,
  bool engage, bool fire, bool cancelTarget
)
: base(from, to)
{
  _target = target;
  _engage = engage;
  _fire = fire;
  _cancelTarget = cancelTarget;
}

LSError RadioMessageTarget::Serialize(ParamArchive &ar)
{
  CHECK(base::Serialize(ar))
  CHECK(ar.SerializeRef("target", _target, 1))
  CHECK(ar.Serialize("engage", _engage, 1, false))
  CHECK(ar.Serialize("fire", _fire, 1, false))
  CHECK(ar.Serialize("cancelTarget", _cancelTarget, 1, false))
  return LSOK;
}

/*!
\patch 1.29 Date 10/31/2001 by Jirka
- Fixed: Target cursor shown in MP game
*/
void RadioMessageTarget::Transmitted(ChatChannel channel)
{
  if (_target && _target->IsVanishedOrDestroyed())
  {
    return;
  }
  if (_target)
  {
    if (IsTo(GWorld->FocusOn()) && GWorld->UI())
      GWorld->UI()->ShowTarget();
  }

  AIBrain *sender = GetSender();
  for (int i=0; i<_to.Size(); i++)
  {
    AIUnit *to = _to[i];
    if (!to) continue;
    if (sender && !to->CanHear(channel, sender)) continue;
    if (_target)
    {
      EntityAI *tgtAI = _target->idExact;
      if (to->GetPerson()->IsRemotePlayer())
      {
        GetNetworkManager().ShowTarget(to->GetPerson(), tgtAI);
      }
      if (tgtAI == to->GetVehicleIn() || tgtAI==to->GetPerson())
      {
        // unit cannot target itself
        continue;
      }
    }
    // translate command from leader to his vehicle
    Transport *vehicle = to->GetVehicleIn();
    TurretContext context;
    if (vehicle && vehicle->CommanderUnit() != to && vehicle->FindTurret(to->GetPerson(), context) && context._gunner)
    {
      /*if (_target)*/ vehicle->ReceivedTarget(context._gunner, _target);
      continue;
    }
    if (to->IsGroupLeader())
    {
      if (vehicle /*&& _target*/)
      {
        Person *gunner = vehicle->GunnerUnit() ? vehicle->GunnerUnit()->GetPerson() : NULL;
        if (gunner) vehicle->ReceivedTarget(gunner, _target);
        continue;
      }
      // if group leader gets an order and cannot translate it, he receives it
      // otherwise doFire does not work as expected for solitary soldiers
    }
    if (_engage)
    {	
      Target *tgt = _target ? (Target*)_target : to->GetTargetAssigned();
      to->EngageTarget(tgt);
    }
    if (_fire)
    {	
      Target *tgt = _target ? (Target*)_target : to->GetTargetAssigned();
      to->EnableFireTarget(tgt);
    }
    if (_target || _cancelTarget)
      to->AssignTarget(_target);
    if (_target)
    {
      EntityAI *veh = to->GetVehicle();
      veh->BegAttack(_target);
    }
  }
}

void RadioMessageTarget::Canceled()
{
  for (int i=0; i<_to.Size(); i++)
  {
    AIUnit *to = _to[i];
    if (!to) continue;
    to->GetGroup()->UnitAssignCanceled(to);
  }
}

const char *RadioMessageTarget::GetPriorityClass()
{
  return "UrgentCommand";
}

RString RadioMessageTarget::PrepareSentence(SentenceParams &params) const
{
  if (!_from || !_from->Leader()) return RString();
  if (_to.Count() <= 0) return RString();

  SetPlayerMsg(IsTo(GWorld->FocusOn()));

  // Remove any messages destined for AIUnit without a ID greater than 0
  // this is because dead units are still alive
  for (int i = 0; i < _to.Size(); i++)
  {
    const AIUnit *unit = _to[i];
    if (!unit || unit->ID()==0)
    {
      _to.Delete(i);
      --i;
    }
  }

  Target *target = _target;
  params.Add(_to, false);

  if (target)
  {
    if (target->IsVanishedOrDestroyed())
    {
      return RString();
    }
    const EntityAIType *type = target->GetType();
    params.AddWord(type->GetNameSound(), type->GetShortName());
  }
  else
  {
    params.AddWord("unknown", LocalizeString(IDS_WORD_UNKNOWN));
  }
  params.Add(_from->Leader());

  if (target)
  {
    if (_engage && _fire) return "SentCmdAttack";
    else if (_engage) return "SentEngage";
    else if (_fire) return "SentFire";
    else return "SentTarget";
  }
  else
  {
    if (_engage && _fire) return "SentAttackNoTarget";
    else if (_engage) return "SentEngageNoTarget";
    else if (_fire) return "SentFireNoTarget";
    else return "SentNoTarget";
  }
}

///////////////////////////////////////////////////////////////////////////////
// class RadioMessageTeam

RadioMessageTeam::RadioMessageTeam(AIGroup *from, OLinkPermNOArray(AIUnit) &to, Team team)
{
  _from = from;
  _team = team;
  _to = to;
  for (int i=0; i<to.Size(); i++)
  {
    AIUnit *unit = to[i];
    if (unit && unit->GetGroup() == from)
    {
      // assign team immediately
      GWorld->UI()->SetTeam(unit, team);
    }
  }
}

LSError RadioMessageTeam::Serialize(ParamArchive &ar)
{
  CHECK(RadioMessage::Serialize(ar))
  CHECK(ar.SerializeRef("From", _from, 1))
  CHECK(ar.SerializeRefs("To", _to, 1))
  CHECK(ar.SerializeEnum("team", _team, 1, TeamMain))
  return LSOK;
}

const char *RadioMessageTeam::GetPriorityClass()
{
  return "NormalCommand";
}

void RadioMessageTeam::Transmitted(ChatChannel channel)
{
  // assign team immediatelly
/*
  for (int i=0; i<_to.Size(); i++)
  {
    AIUnit *u = _to[i];
    if (!u) continue;
    if (u->GetGroup()!=_from) continue;
    GWorld->UI()->SetTeam(u->ID() - 1, _team);
  }
*/
}

bool RadioMessageTeam::IsTo(AIBrain *unit) const
{
  Assert(unit);

  int i;
  for (i=0; i<_to.Size(); i++)
  {
    AIUnit *u = _to[i];
    if (u == unit)
      return true;
  }
  return false;
}

RString RadioMessageTeam::PrepareSentence(SentenceParams &params) const
{
  if (!_from || !_from->Leader()) return RString();
  if (_to.Count() <= 0) return RString();
  
  SetPlayerMsg(IsTo(GWorld->FocusOn()));
  
  params.Add(_to, false, true);
  switch (_team)
  {
  case TeamMain:
    params.AddWord("whiteTeam", LocalizeString(IDS_WORD_TEAM_MAIN));
    break;
  case TeamRed:
    params.AddWord("redTeam", LocalizeString(IDS_WORD_TEAM_RED));
    break;
  case TeamGreen:
    params.AddWord("greenTeam", LocalizeString(IDS_WORD_TEAM_GREEN));
    break;
  case TeamBlue:
    params.AddWord("blueTeam", LocalizeString(IDS_WORD_TEAM_BLUE));
    break;
  case TeamYellow:
    params.AddWord("yellowTeam", LocalizeString(IDS_WORD_TEAM_YELLOW));
    break;
  }
  params.Add(_from->Leader());

  if (_to.Size() == 1)
    return "SentTeam";
  else
    return "SentTeamPlural";
}

///////////////////////////////////////////////////////////////////////////////
// class RadioMessageCommand

RadioMessageCommand::RadioMessageCommand(AIGroup *from, OLinkPermNOArray(AIUnit) &to, Command &command, bool toMainSubgroup, bool transmit)
{
  _from = from;
  _to = to;
  _toMainSubgroup = toMainSubgroup;
  _transmit = transmit;
  _command = command;
}

LSError RadioMessageCommand::Serialize(ParamArchive &ar)
{
  CHECK(RadioMessage::Serialize(ar))
  CHECK(ar.SerializeRef("From", _from, 1))
  CHECK(ar.SerializeRefs("To", _to, 1))
  CHECK(ar.Serialize("Command", _command, 1))
  CHECK(ar.Serialize("toMainSubgrp", _toMainSubgroup, 1, false))
  CHECK(ar.Serialize("transmit", _transmit, 1, true))
  return LSOK;
}

const char *RadioMessageCommand::GetPriorityClass()
{
  if
  (
    _command._context == Command::CtxUI ||
    _command._context == Command::CtxUIWithJoin
  )
    return "UICommand";
  else switch (_command._message)
  {
  case Command::Move:
  case Command::Heal:
  case Command::HealSoldier:
  case Command::RepairVehicle:
  case Command::Repair:
  case Command::Refuel:
  case Command::Rearm:
  case Command::Support:
  case Command::GetIn:
  case Command::GetOut:
  case Command::Join:
    return "NormalCommand";
  case Command::Attack:
  case Command::AttackAndFire:
  case Command::Fire:
  case Command::FireAtPosition:
    return "UrgentCommand";
  default:
    return "Default";
  }
}

bool RadioMessageCommand::IsTo(AIBrain *unit) const
{
  // unit may be NULL in MP
  //Assert(unit);

  if (_toMainSubgroup)
    return _from && unit->GetUnit() && unit->GetUnit()->GetSubgroup() == _from->MainSubgroup();

  int i;
  for (i=0; i<_to.Size(); i++)
  {
    AIUnit *u = _to[i];
    if (u == unit)
      return true;
  }
  return false;
}

void RadioMessageCommand::DeleteTo(AIUnit *unit)
{
  Assert(unit);

  if (_toMainSubgroup)
    return;

  int i;
  for (i=0; i<_to.Size(); i++)
  {
    AIUnit *u = _to[i];
    if (u == unit)
    {
      _to.Delete(i);
      return;
    }
  }
}

RadioMoveToHelper::RadioMoveToHelper()
{
  // Helper values to answer later Simple Expression questions
  _distanceToGroup = RadioMessage::GetDefaultDistanceToGroup();
  _distanceToLocation = RadioMessage::GetDefaultDistanceToLocation();
  _distanceToUnit = RadioMessage::GetDefaultDistanceToUnit();
  _groupCoreRadius = RadioMessage::GetDefaultGroupCoreRadius();
  _groupCompactness = RadioMessage::GetDefaultGroupCompactness();
  _distanceToRecipients = RadioMessage::GetDefaultDistanceToRecipients();
  _recipientsRadius = RadioMessage::GetDefaultRecipientsRadius();
  _isInsideLocation = RadioMessage::GetDefaultInsideLocation();
  _moveToObject = RadioMessage::GetDefaultMoveToObject();
  _location = NULL;
}

void RadioMoveToHelper::PrepareSEHelperVariables(AIBrain *first, AIGroup *grpFrom, const OLinkPermNOArray(AIUnit) &recipients, 
                                                 Vector3Par destination, Target *tgtE, TargetId tgtF)
{
  // prepare groupCoreDirection and Position for later use
  AIGroup *grp = first->GetGroup();
  // prepare some RadioMessageReportTarget helper members for later use
  grp->UpdateCompactness(true); //lazy update, only to ensure it is initialized
  _groupCoreRadius = grp->GetCoreRadius();
  _groupCompactness = grp->GetCompactness();
  // prepare _distanceToUnit
  float best2 = first->Position(first->GetFutureVisualState()).DistanceXZ2(destination);
  for (int i=0; i<recipients.Size(); i++)
  {
    AIUnit *unit = recipients[i];
    if (!unit) continue;
    if ( !unit->IsUnit() ) continue; //use only commander from each vehicle or standalone unit
    float dist2 = unit->Position(unit->GetFutureVisualState()).DistanceXZ2(destination);
    saturateMin(best2, dist2);
  }
  _distanceToUnit = sqrt(best2);
  saturateMax(_distanceToUnit, 1); //at least 1 meter

  Vector3 corePosition = grp ? grp->GetCorePosition() : first->Position(first->GetFutureVisualState());
  Vector3 coreDirection = grp ? grp->GetGroupDirection() : first->Direction(first->GetFutureVisualState());

  // Target relative moveTo command is processed different way
  Target *tgt = tgtE;
  if (!tgt && grpFrom->Leader()) tgt = grpFrom->Leader()->FindTarget(tgtF);
  if (tgt && tgt->GetType()->GetShortName().GetLength() > 0)
  { // target should be used, other parameters are not important
    // Prepare groupTgtDirection and groupTgtDistance
    Vector3 absDirVect = tgt->GetPosition()-corePosition;
    _distanceToGroup = absDirVect.SizeXZ();
    saturateMax(_distanceToGroup, 1); //at least 1 meter
    return; //done
  }

  // Prepare groupTgtDirection and groupTgtDistance
  Vector3 absDirVect = destination-corePosition;
  _distanceToGroup = absDirVect.SizeXZ();
  saturateMax(_distanceToGroup, 1); //at least 1 meter

  // LOCATIONS related parameters
  //float maxDist2 = min(0.25f*Square(_distanceToGroup),500.0f*500.0f); //not more than five hundred meters
  float maxDist2 = 1000*1000; //not more than 1000 meters
  _location = FindNearestLocationWithDubbing(destination, maxDist2);
#if PROFILE_LOCATION_FINDING
  /* //Testing on Chernaruss shows (time for 100 iterations in miliseconds)
   643.018: Location 400m: 0.469045
   643.020: Location 500m: 0.793977
   643.021: Location 600m: 0.805468
   643.023: Location 700m: 1.036633
   643.024: Location 800m: 1.149938
   643.025: Location 900m: 1.718684
   643.028: Location 1000m: 1.768494
   643.031: Location 1500m: 3.390542
  */
  {
    SectionTimeHandle _start;
    for (int ldist = 400; ldist < 1000; ldist +=100)
    {
      _start = StartSectionTime();
      maxDist2 = ldist*ldist;
      for (int i=0; i<100; i++) _location = FindNearestLocationWithDubbing(destination, maxDist2);
      LogF("Location %dm: %.2f", ldist, GetSectionTime(_start)*1000);
    }
    int ldist=1500;
    _start = StartSectionTime();
    maxDist2 = ldist*ldist;
    for (int i=0; i<100; i++) _location = FindNearestLocationWithDubbing(destination, maxDist2);
    LogF("Location %dm: %.2f", ldist, GetSectionTime(_start)*1000);
  }
#endif
  //_location = FindNearestLocationWithDubbing(_command._destination, maxDist2);
  _absLocationDir = 0;
  if (_location)
  {
#if 0
    // Diagnose to know locations params available
    LogF("Location: %s, a=%.1f, b=%.1f, rectangular=%d, angle=%.1f", cc_cast(_location->_text), _location->_a, _location->_b, _location->_rectangular, _location->_angle);
#endif
    _isInsideLocation = (_location && _location->IsInside(destination)) ?  1.0f : 0;
    if (_location->IsInside(corePosition))
    { // group is inside location, the description using this location has no sense
      _distanceToLocation = 1e10; // do not use this location
    }
    else
    {
      Vector3 absLocationVect = destination - _location->GetPosition();
      _distanceToLocation = absLocationVect.SizeXZ();
      _absLocationDir = atan2(absLocationVect.X(),absLocationVect.Z());
    }
  }
  else _distanceToLocation = 1e10; //same as no Location found
  saturateMax(_distanceToLocation, 1); //at least 1 meter

  // Object related helpers
  if ( grpFrom->Leader() && (destination.Distance2(grpFrom->Leader()->Position(grpFrom->Leader()->GetFutureVisualState())) < Square(400)) )
  {
    Object *obj = FindNearestMoveObject(destination);
    if (obj) _moveToObject = 1.0f; //set object available
  }

  // Recipients related helpers
  if ( recipients.Size() )
  {
    _recipientsCenterPos = VZero;
    int _recipientsCount = 0;
    for (int i=0; i<recipients.Size(); i++)
    {
      AIUnit *unit = recipients[i];
      if (!unit) continue;
      _recipientsCenterPos = _recipientsCenterPos + unit->Position(unit->GetFutureVisualState());
      ++_recipientsCount;
    }
    if (_recipientsCount) //avoid zero division
      _recipientsCenterPos = _recipientsCenterPos / _recipientsCount;
  }
  else _recipientsCenterPos = first->Position(first->GetFutureVisualState());
  float rad2 = RadioMessage::GetDefaultRecipientsRadius(); //avoid zero division
  for (int i=0; i<recipients.Size(); i++)
  {
    AIUnit *unit = recipients[i];
    if (!unit) continue;
    float dist2 = _recipientsCenterPos.DistanceXZ2(unit->Position(unit->GetFutureVisualState()));
    if ( rad2 < dist2 ) rad2=dist2;
  }
  _recipientsRadius = sqrt(rad2);
  _distanceToRecipients = _recipientsCenterPos.DistanceXZ(destination);
}

void RadioMoveToHelper::SetParams(SentenceParams &params, const RadioMessage *msg, AIBrain *first, Vector3Par destination)
{
  AIGroup *grp = first->GetGroup();
  Vector3 corePosition = grp ? grp->GetCorePosition() : first->Position(first->GetFutureVisualState());
  Vector3 coreDirection = grp ? grp->GetGroupDirection() : first->Direction(first->GetFutureVisualState());

  // Parameters for SelectCmdMoveSentence group of sentences
  // 1 - unit list ... already added
  // 2 - grid position
  // square in map
  // VBS supports bigger grids
  char buffer[32];
  PositionToAA11(destination, buffer);
  params.Add(RString(buffer));

  // Prepare groupTgtDirection and groupTgtDistance
  Matrix4 transf;
  transf.SetOriented(coreDirection, VUp);
  transf.SetPosition(corePosition);
  Vector3 dir =  transf.InverseRotation().FastTransform(destination);
  float relDir = atan2(dir.X(),dir.Z());
  Vector3 absDirVect = destination-corePosition;
  float absGrpDir = atan2(absDirVect.X(), absDirVect.Z());
  // Prepare Leader/Group mix position and direction
  Matrix4 transfLeader;
  float leaderAbsDir = 0;
  float distanceToLeader = 0;
  if (grp->Leader())
  {
    transfLeader.SetOriented(coreDirection, VUp);
    transfLeader.SetPosition(grp->Leader()->Position(grp->Leader()->GetFutureVisualState()));
    Vector3 leaderAbsDirVect = destination-grp->Leader()->Position(grp->Leader()->GetFutureVisualState());
    leaderAbsDir = atan2(leaderAbsDirVect.X(), leaderAbsDirVect.Z());
    distanceToLeader = leaderAbsDirVect.SizeXZ();
  }
  else transfLeader = transf;
  Vector3 leaderDir =  transfLeader.InverseRotation().FastTransform(destination);
  float leaderRelDir = atan2(leaderDir.X(),leaderDir.Z());
  // 3 - distance from group
  params.AddDistance(_distanceToGroup, grp->GetMartaTypeIx(), msg); //DISTANCE
  // 4 - direction from group
  params.AddDirection(relDir, absGrpDir, false, msg); //DIRECTION

  // LOCATIONS related parameters
  //LogF(" dist2Location: %.1f, dist2Group: %.1f, dist2Unit: %.1f", _distanceToLocation, _distanceToGroup, _distanceToUnit);
  // 5 - distance from location
  params.AddDistance(_distanceToLocation, grp->GetMartaTypeIx(), msg);
  // 6 - direction from location
  params.AddDirection(relDir, _absLocationDir, true, msg);
  // 7 - location
  params.AddLocation(_location);

  // Object related parameters
  bool objParamsDone = false;
  if ( grp->Leader() && (destination.Distance2(grp->Leader()->Position(grp->Leader()->GetFutureVisualState())) < Square(400)) )
  {
    Object *obj = FindNearestMoveObject(destination);
    if (obj)
    {
      // 8 - object
      params.AddWord(obj->GetNameSound(), obj->GetShortName());
      // 9 - direction to object
      Vector3 dir =  transf.InverseRotation().FastTransform(obj->FutureVisualState().Position());
      float relObjectDir = atan2(dir.X(),dir.Z());
      Vector3 absObjectVect = obj->FutureVisualState().Position()-corePosition;
      float absObjectDir = atan2(absObjectVect.X(),absObjectVect.Z());
      params.AddDirection(relObjectDir, absObjectDir, false, msg);
      objParamsDone = true;
    }
  }
  if (!objParamsDone)
  { //add empty object related parameters
    params.AddWord(RString(""), RString(""));
    // 9 - direction to object
    params.AddDirection(0, 0, true, msg);
  }

  // Recipients related parameters
  Vector3 absRecipientsVect = destination - _recipientsCenterPos;
  float absRecipientsDir = atan2(absRecipientsVect.X(),absRecipientsVect.Z());
  // 10 - distance from recipients center
  params.AddDistance(_distanceToRecipients, grp->GetMartaTypeIx(), msg); //DISTANCE
  // 11 - direction from recipients center
  params.AddDirection(0, absRecipientsDir, true, msg);
  // 12 - distance from Leader
  params.AddDistance(distanceToLeader, grp->GetMartaTypeIx(), msg); //DISTANCE
  // 13 - direction from Leader
  params.AddDirection(leaderRelDir, leaderAbsDir, false, msg);
}

RString RadioMessageCommand::PrepareCmdMoveSentence(SentenceParams &params, AIUnit *first) const
{
  AIGroup *grp = NULL;
  if (first) grp = first->GetGroup(); // NULL is checked in PrepareSEHelperVariables
  else return RString(""); //no recipient?
  // prepare helper variables for Simple Expressions
  // prepare recipients list for PrepareSEHelperVariables call
  OLinkPermNOArray(AIUnit) tempMainSubgroupList;
  const OLinkPermNOArray(AIUnit) &recipients = _toMainSubgroup ? (_from->MainSubgroup()->GetUnitsList(tempMainSubgroupList), tempMainSubgroupList) : _to;
  _moveToHelper.PrepareSEHelperVariables(first, _from, recipients, _command._destination, _command._targetE, _command._target);
  
  Vector3 corePosition = grp ? grp->GetCorePosition() : first->Position(first->GetFutureVisualState());
  Vector3 coreDirection = grp ? grp->GetGroupDirection() : first->Direction(first->GetFutureVisualState());

  // Target relative moveTo command is processed different way
  Target *tgt = _command._targetE;
  if (!tgt) tgt = _from->Leader() ? _from->Leader()->FindTarget(_command._target) : NULL;
  if (tgt && tgt->GetType()->GetShortName().GetLength() > 0)
  {
    // target should be used, other parameters are not important
    // 1 - unit list
    // 2 - target
    // 3 - distance from group
    // 4 - direction from group
    const EntityAIType *type = tgt->GetType();
    params.AddWord(type->GetNameSound(), type->GetShortName()); //TARGET

    // Prepare groupTgtDirection and groupTgtDistance
    Matrix4 transf;
    transf.SetOriented(coreDirection, VUp);
    transf.SetPosition(corePosition);
    Vector3 dir =  transf.InverseRotation().FastTransform(tgt->GetPosition());
    float relDir = atan2(dir.X(),dir.Z());
    Vector3 absDirVect = tgt->GetPosition()-corePosition;
    float absGrpDir = atan2(absDirVect.X(), absDirVect.Z());
    // 5 - groupTgtDistance
    params.AddDistance(_moveToHelper.GetDistanceToGroup(), grp->GetMartaTypeIx(), this); //DISTANCE
    // 6 - groupTgtDirection
    params.AddDirection(relDir, absGrpDir, false, this);
    return RString("SentCmdMoveToTarget"); //DIRECTION
  }

  // prepare Move specific params
  _moveToHelper.SetParams(params, this, first, _command._destination);

  return RString("SelectCmdMoveSentence");
}

/*!
\patch 1.01 Date 6/25/2001 by Ondra.
- Fixed: Get In UNKNOWN.
\patch 5142 Date 3/16/2007 by Bebul
- Fixed: Salute and SitDown should work as toggle actions (also their radio protocol sentence fixed)
*/

RString RadioMessageCommand::PrepareSentence(SentenceParams &params) const
{
  if 
  (
    !_from ||
    !_from->Leader() ||
    !_from->Leader()->LSCanSpeak() ||
    _command._message == Command::NoCommand ||
    _command._message == Command::Wait ||
    _command._context == Command::CtxUndefined ||
    _command._context == Command::CtxJoin
  )
    return RString();

  if
  ( 
    !_command._target && !_command._targetE &&
    (
      _command._message == Command::Fire ||
      _command._message == Command::Attack ||
      _command._message == Command::AttackAndFire ||
      _command._message == Command::FireAtPosition
    )
  )
  {
    return RString();
  }
  if
  (
    !_command._target &&
    (
      _command._message == Command::GetIn ||
      _command._message == Command::Heal ||
      _command._message == Command::HealSoldier ||
      _command._message == Command::RepairVehicle ||
      _command._message == Command::Repair ||
      _command._message == Command::Refuel ||
      _command._message == Command::Rearm ||
      _command._message == Command::TakeBag ||
      _command._message == Command::Assemble ||
      _command._message == Command::DisAssemble ||
      _command._message == Command::Support
    )
  )
    return RString();

  AIUnit *first = NULL;
  if (_toMainSubgroup)
  {
    params.AddWord("allGroup", LocalizeString(IDS_WORD_ALLGROUP));
    first = _from->MainSubgroup()->Leader();
    if (!first) first = _from->Leader();
  }
  else
  {
    if (_to.Count() <= 0) return RString();
    bool wholeCrew =
      _command._message != Command::GetIn &&
      _command._message != Command::GetOut &&
      !(_command._message == Command::Action && _command._action && _command._action->GetType() == ATEject);
    params.Add(_to, wholeCrew);
    first = _to[0];

    SetPlayerMsg(IsTo(GWorld->FocusOn()));
  }
  // Assert(first) is not sufficient as the crash could happen
  Assert(first);
  if (!first) return RString();

  // say command
  Target *tgt = NULL;
  RString sentence;
  switch (_command._message)
  {
  case Command::Join:
    {
      AISubgroup *subgroup = _command._joinToSubgroup;
      if (!subgroup) subgroup = _from->MainSubgroup();
      Assert(subgroup);
      if (!subgroup->Leader())
      {
        sentence = "SentCmdFollow";
        params.AddWord("allGroup", LocalizeString(IDS_WORD_ALLGROUP));
      }
      else if (subgroup->Leader() == _from->Leader())
      {
        sentence = "SentCmdFollowMe";
      }
      else
      {
        sentence = "SentCmdFollow";
        params.Add(subgroup->Leader());
      }
    }
    break;
  case Command::GetOut:
    sentence = "SentCmdGetOut";
    break;
  case Command::Move:
    return PrepareCmdMoveSentence(params, first);
  case Command::Stop:
    sentence = "SentCmdStop";
    break;
  case Command::Expect:
    sentence = "SentCmdExpect";
    break;
  case Command::Hide:
    sentence = "SentCmdHide";
    break;
  case Command::Heal:
Heal:
    tgt = _command._targetE;
    if (!tgt) tgt = _from->Leader()->FindTarget(_command._target);
    if (tgt && tgt->GetType()->GetShortName().GetLength() > 0)
    {
      sentence = "SentCmdHealAt";
      goto CmdSupplyTarget;
    }
    else if (_command._destination.Distance2(_from->Leader()->Position(_from->Leader()->GetFutureVisualState())) > Square(400))
    {
      sentence = "SentCmdHealFar";
      goto CmdPositionFar;
    }
    else
    {
      sentence = "SentCmdHeal";
      goto CmdPosition;
    }
  case Command::HealSoldier:
HealSoldier:
    tgt = _command._targetE;
    if (!tgt) tgt = _from->Leader()->FindTarget(_command._target);
    if (tgt && tgt->GetType()->GetShortName().GetLength() > 0)
    {
      sentence = "SentCmdHealSomeone";
      goto CmdSupplyTarget;
    }
    else if (_command._destination.Distance2(_from->Leader()->Position(_from->Leader()->GetFutureVisualState())) > Square(400))
    {
      sentence = "SentCmdHealSomeone";
      goto CmdPositionFar;
    }
    else
    {
      sentence = "SentCmdHealSomeone";
      goto CmdPosition;
    }
  case Command::FirstAid:
FirstAid:
    tgt = _command._targetE;
    if (!tgt) tgt = _from->Leader()->FindTarget(_command._target);
    if (tgt && tgt->GetType()->GetShortName().GetLength() > 0)
    {
      sentence = "SentCmdHealSomeone";
      goto CmdSupplyTarget;
    }
    else if (_command._destination.Distance2(_from->Leader()->Position(_from->Leader()->GetFutureVisualState())) > Square(400))
    {
      sentence = "SentCmdHealSomeone";
      goto CmdPositionFar;
    }
    else
    {
      sentence = "SentCmdHealSomeone";
      goto CmdPosition;
    }
  case Command::RepairVehicle:
RepairVehicle:
    tgt = _command._targetE;
    if (!tgt) tgt = _from->Leader()->FindTarget(_command._target);
    if (tgt && tgt->GetType()->GetShortName().GetLength() > 0)
    {
      sentence = "SentRepairThat";
      goto CmdSupplyTarget;
    }
    else if (_command._destination.Distance2(_from->Leader()->Position(_from->Leader()->GetFutureVisualState())) > Square(400))
    {
      sentence = "SentRepairThat";
      goto CmdSupplyTarget;
    }
    else
    {
      sentence = "SentRepairThat";
      goto CmdSupplyTarget;
    }
  case Command::TakeBag:
    {
TakeBag:
      sentence = "SentCmdActionTakeBag";
      goto CmdPosition;
    }
  case Command::DropBag:
    {
DropBag:
      sentence = "SentCmdActionDropBag";
      goto CmdPosition;
    }
  case Command::OpenBag:
    {
OpenBag:
      sentence = "SentCmdActionOpenBag";
      goto CmdPosition;
    }
  case Command::Assemble:
    {
Assemble:
      sentence = "SentAssemble";
      goto CmdTarget;
    }
  case Command::DisAssemble:
    {
DisAssemble:
      sentence = "SentDisassemble";
      goto CmdTarget;
    }
  case  Command::IRLaserOn:
    {
IRLaserOn:
      sentence = "SentPointersOn";
      goto CmdPosition;
    }
  case  Command::IRLaserOff:
    {
IRLaserOff:
      sentence = "SentPointersOff";
      goto CmdPosition;
    }
  case  Command::GunLightOn:
    {
GunLightOn:
      sentence = "SentLightsOn";
      goto CmdPosition;
    }
  case  Command::GunLightOff:
    {     
GunLightOff:
      sentence = "SentLightsOff";
      goto CmdPosition;
    }
  case Command::Repair:
Repair:
    tgt = _command._targetE;
    if (!tgt) tgt = _from->Leader()->FindTarget(_command._target);
    if (tgt && tgt->GetType()->GetShortName().GetLength() > 0)
    {
      sentence = "SentCmdRepairAt";
      goto CmdSupplyTarget;
    }
    else if (_command._destination.Distance2(_from->Leader()->Position(_from->Leader()->GetFutureVisualState())) > Square(400))
    {
      sentence = "SentCmdRepairFar";
      goto CmdPositionFar;
    }
    else
    {
      sentence = "SentCmdRepair";
      goto CmdPosition;
    }
  case Command::Refuel:
Refuel:
    tgt = _command._targetE;
    if (!tgt) tgt = _from->Leader()->FindTarget(_command._target);
    if (tgt && tgt->GetType()->GetShortName().GetLength() > 0)
    {
      sentence = "SentCmdRefuelAt";
      goto CmdSupplyTarget;
    }
    else if (_command._destination.Distance2(_from->Leader()->Position(_from->Leader()->GetFutureVisualState())) > Square(400))
    {
      sentence = "SentCmdRefuelFar";
      goto CmdPositionFar;
    }
    else
    {
      sentence = "SentCmdRefuel";
      goto CmdPosition;
    }
  case Command::Rearm:
Rearm:
    tgt = _command._targetE;
    if (!tgt) tgt = _from->Leader()->FindTarget(_command._target);
    if (tgt && tgt->GetType()->GetShortName().GetLength() > 0)
    {
      sentence = "SentCmdRearmAt";
      goto CmdSupplyTarget;
    }
    else if (_command._destination.Distance2(_from->Leader()->Position(_from->Leader()->GetFutureVisualState())) > Square(400))
    {
      sentence = "SentCmdRearmFar";
      goto CmdPositionFar;
    }
    else
    {
      sentence = "SentCmdRearm";
      goto CmdPosition;
    }
  case Command::Support:
    tgt = _command._targetE;
    if (!tgt) tgt = _from->Leader()->FindTarget(_command._target);
    if (tgt && tgt->GetType()->GetShortName().GetLength() > 0)
    {
      sentence = "SentCmdSupportAt";
      goto CmdSupplyTarget;
    }
    else if (_command._destination.Distance2(_from->Leader()->Position(_from->Leader()->GetFutureVisualState())) > Square(400))
    {
      sentence = "SentCmdSupportFar";
      goto CmdPositionFar;
    }
    else
    {
      sentence = "SentCmdSupport";
      goto CmdPosition;
    }
  case Command::Attack:
  case Command::AttackAndFire:
    sentence = "SentCmdAttack";
    goto CmdTarget;
  case Command::FireAtPosition:
    sentence = "SentARTYFireAt";
    goto CmdTargetPosition;
  case Command::Fire:
    sentence = "SentCmdFire";
    goto CmdTarget;
  case Command::GetIn:
    sentence = "SentCmdGetIn";
    if (!_toMainSubgroup)
    {
      if (_to.Size() == 1)
      {
        Transport *veh = first->VehicleAssigned();
        if (veh == _command._target)
        {
          if (first == veh->GetCommanderAssigned())
            sentence = "SentCmdGetInCommander";
          else if (first == veh->GetDriverAssigned())
          {
            if (veh->GetType()->IsKindOf(GWorld->Preloaded(VTypeAir)))
              sentence = "SentCmdGetInPilot";
            else
              sentence = "SentCmdGetInDriver";
          }
          else if (first == veh->GetGunnerAssigned())
            sentence = "SentCmdGetInGunner";
          else
            for (int i=0; i<veh->GetManCargo().Size(); i++)
              if (first == veh->GetCargoAssigned(i))
              {
                sentence = "SentCmdGetInCargo";
                break;
              }
        }
      }
      else
      {
        // check if all units gets in cargo
        bool ok = true;
        Transport *veh = first->VehicleAssigned();
        if (veh)
        {
          for (int j=0; j<_to.Size(); j++)
          {
            AIUnit *unit = _to[j];
            if (!unit || unit->VehicleAssigned() != veh)
            {
              ok = false; break;
            }
            bool found = false;
            for (int i=0; i<veh->GetManCargo().Size(); i++)
              if (unit == veh->GetCargoAssigned(i))
              {
                found = true; break;
              }
            if (!found)
            {
              ok = false; break;
            }
          }
        }
        else ok = false;
        if (ok) sentence = "SentCmdGetInCargo";
      }
    }
    goto CmdTarget;
  case Command::Action:
    {
      DoAssert(_command._action);
      switch (_command._action->GetType())
      {
      case ATHeal:
        goto Heal;
      case ATHealSoldier:
        goto HealSoldier;
      case ATFirstAid:
        goto FirstAid;
      case ATRepairVehicle:
        goto RepairVehicle;
      case ATRepair:
        goto Repair;
      case ATRefuel:
        goto Refuel;
      case ATRearm:
        goto Rearm;
      case ATTakeBag:
        goto TakeBag;
      case ATDropBag:
      case ATPutBag:
        goto DropBag;
      case ATOpenBag:
        goto OpenBag;
      case ATAssemble:
        goto Assemble;
      case ATDisAssemble:
        goto DisAssemble;
      case ATIRLaserOn:
        goto IRLaserOn;
      case ATIRLaserOff:
        goto IRLaserOff;
      case ATGunLightOn:
        goto GunLightOn;
      case ATGunLightOff:
        goto GunLightOff;
      case ATTakeDropWeapon:
        {
          // add weapon name
          ActionWeapon *action = static_cast<ActionWeapon *>(_command._action.GetRef());
          Ref<WeaponType> weapon = WeaponTypes.New(action->GetWeapon());
          Assert(weapon);
          params.AddWord(weapon->GetNameSound(), weapon->GetDisplayName());
        }
        tgt = _command._targetE;
        if (!tgt) tgt = _from->Leader()->FindTarget(_command._target);
        if (tgt && tgt->GetType()->GetShortName().GetLength() > 0)
        {
          sentence = "SentCmdTakeWeaponAt";
          goto CmdSupplyTarget;
        }
        else if (_command._destination.Distance2(_from->Leader()->Position(_from->Leader()->GetFutureVisualState())) > Square(400))
        {
          sentence = "SentCmdTakeWeaponFar";
          goto CmdPositionFar;
        }
        else
        {
          sentence = "SentCmdTakeWeapon";
          goto CmdPosition;
        }
      case ATTakeMagazine:
      case ATTakeDropMagazine:
        tgt = _command._targetE;
        if (!tgt) tgt = _from->Leader()->FindTarget(_command._target);
        if (tgt && tgt->GetType()->GetShortName().GetLength() > 0)
        {
          sentence = "SentCmdTakeMagazineAt";
          goto CmdSupplyTarget;
        }
        else if (_command._destination.Distance2(_from->Leader()->Position(_from->Leader()->GetFutureVisualState())) > Square(400))
        {
          sentence = "SentCmdTakeMagazineFar";
          goto CmdPositionFar;
        }
        else
        {
          sentence = "SentCmdTakeMagazine";
          goto CmdPosition;
        }
      case ATTakeFlag:
        tgt = _command._targetE;
        if (!tgt) tgt = _from->Leader()->FindTarget(_command._target);
        if (tgt && tgt->GetType()->GetShortName().GetLength() > 0)
        {
          sentence = "SentCmdTakeFlagAt";
          goto CmdSupplyTarget;
        }
        else if (_command._destination.Distance2(_from->Leader()->Position(_from->Leader()->GetFutureVisualState())) > Square(400))
        {
          sentence = "SentCmdTakeFlagFar";
          goto CmdPositionFar;
        }
        else
        {
          sentence = "SentCmdTakeFlag";
          goto CmdPosition;
        }
      case ATReturnFlag:
        tgt = _command._targetE;
        if (!tgt) tgt = _from->Leader()->FindTarget(_command._target);
        if (tgt && tgt->GetType()->GetShortName().GetLength() > 0)
        {
          sentence = "SentCmdReturnFlagAt";
          goto CmdSupplyTarget;
        }
        else if (_command._destination.Distance2(_from->Leader()->Position(_from->Leader()->GetFutureVisualState())) > Square(400))
        {
          sentence = "SentCmdReturnFlagFar";
          goto CmdPositionFar;
        }
        else
        {
          sentence = "SentCmdReturnFlag";
          goto CmdPosition;
        }
      case ATDropWeapon:
      case ATPutWeapon:
        {
          // add weapon name
          ActionWeapon *action = static_cast<ActionWeapon *>(_command._action.GetRef());
          Ref<WeaponType> weapon = WeaponTypes.New(action->GetWeapon());
          Assert(weapon);
          params.AddWord(weapon->GetNameSound(), weapon->GetDisplayName());
        }
        sentence = "SentCmdDropWeapon";
        goto CmdPositionNear;
      case ATDropMagazine:
      case ATPutMagazine:
        sentence = "SentCmdDropMagazine";
        goto CmdPositionNear;
      case ATEject:
        sentence = "SentCmdEject";
        goto CmdPositionNear;
      case ATMoveToDriver:
      case ATMoveToPilot:
        sentence = "SentCmdSwitchToDriver";
        goto CmdPositionNear;
      case ATMoveToGunner:
        sentence = "SentCmdSwitchToGunner";
        goto CmdPositionNear;
      case ATMoveToCommander:
        sentence = "SentCmdSwitchToCommander";
        goto CmdPositionNear;
      case ATMoveToCargo:
        sentence = "SentCmdSwitchToCargo";
        goto CmdPositionNear;
      case ATUseMagazine:
        sentence = "SentCmdPlaceCharge";
        goto CmdPositionNear;
      case ATTouchOff:
        sentence = "SentCmdDetonate";
        goto CmdPositionNear;
      case ATStartTimer:
      case ATSetTimer:
        sentence = "SentCmdActivateTimer";
        goto CmdPositionNear;
      case ATDeactivate:
        sentence = "SentCmdDeactivateBomb";
        goto CmdPositionNear;
      case ATDeactivateMine:
        sentence = "SentCmdDeactivateMine";
        goto CmdPositionNear;
      case ATTakeMine:
        sentence = "SentCmdTakeMine";
        goto CmdPositionNear;
      case ATFireInflame:
        tgt = _command._targetE;
        if (!tgt) tgt = _from->Leader()->FindTarget(_command._target);
        if (tgt && tgt->GetType()->GetShortName().GetLength() > 0)
        {
          sentence = "SentCmdLightFireAt";
          goto CmdSupplyTarget;
        }
        else if (_command._destination.Distance2(_from->Leader()->Position(_from->Leader()->GetFutureVisualState())) > Square(400))
        {
          sentence = "SentCmdLightFireFar";
          goto CmdPositionFar;
        }
        else
        {
          sentence = "SentCmdLightFire";
          goto CmdPosition;
        }
      case ATFirePutDown:
        tgt = _command._targetE;
        if (!tgt) tgt = _from->Leader()->FindTarget(_command._target);
        if (tgt && tgt->GetType()->GetShortName().GetLength() > 0)
        {
          sentence = "SentCmdPutOutFireAt";
          goto CmdSupplyTarget;
        }
        else if (_command._destination.Distance2(_from->Leader()->Position(_from->Leader()->GetFutureVisualState())) > Square(400))
        {
          sentence = "SentCmdPutOutFireFar";
          goto CmdPositionFar;
        }
        else
        {
          sentence = "SentCmdPutOutFire";
          goto CmdPosition;
        }
      case ATSalute:
      case ATSitDown:
#if _VBS3_WEAPON_ON_BACK
      case ATPutWeaponOnBack:
#endif
        {
          RString name = _command._action->GetTextSimple(NULL);
          params.AddWord("", name);
          sentence = "SentCmdActionNear";
          goto CmdPositionNear;
        }
      default:
        {
          UIAction action(_command._action);
          action.priority = 0;
          action.showWindow = false;
          action.hideOnUse = false;
          action.action->SetFromGUI(false);
          RString name = action.action->GetTextSimple(NULL);
          params.AddWord("", name);

          tgt = _command._targetE;
          if (!tgt) tgt = _from->Leader()->FindTarget(_command._target);
          if (
            tgt && tgt->GetType()->GetShortName().GetLength() > 0 &&
            _command._action->GetType() != ATMarkEntity && _command._action->GetType() != ATMarkWeapon)
          {
            sentence = "SentCmdActionAt";
            goto CmdSupplyTarget;
          }
          else if (_command._destination.Distance2(_from->Leader()->Position(_from->Leader()->GetFutureVisualState())) > Square(400))
          {
            sentence = "SentCmdActionFar";
            goto CmdPositionFar;
          }
          else if (_command._destination.Distance2(first->Position(first->GetFutureVisualState())) < Square(5.0f))
          {
            // avoid zero distance in sentence
            sentence = "SentCmdActionNear";
            goto CmdPositionNear;
          }
          else
          {
            sentence = "SentCmdAction";
            goto CmdPosition;
          }
        }
      }
    }
  case Command::Scripted:
      {
        params.AddWord("", _command._strParam);

        tgt = _command._targetE;
        if (!tgt) tgt = _from->Leader()->FindTarget(_command._target);
        if (tgt && tgt->GetType()->GetShortName().GetLength() > 0)
        {
          sentence = "SentCmdActionAt";
          goto CmdSupplyTarget;
        }
        else if (_command._destination.Distance2(_from->Leader()->Position(_from->Leader()->GetFutureVisualState())) > Square(400))
        {
          sentence = "SentCmdActionFar";
          goto CmdPositionFar;
        }
        else if (_command._destination.Distance2(first->Position(first->GetFutureVisualState())) < Square(5.0f))
        {
          // avoid zero distance in sentence
          sentence = "SentCmdActionNear";
          goto CmdPositionNear;
        }
        else
        {
          sentence = "SentCmdAction";
          goto CmdPosition;
        }
      }
    break;
  case Command::Dismiss:
    sentence = "SentCmdDismiss";
    break;
  default:
    Fail("Unknown command.");
    return RString();
CmdPositionNear:
    break;
CmdPosition:
    {
      Vector3 dir = _command._destination - first->Position(first->GetFutureVisualState());
      int azimut = toInt(0.1 * atan2(dir.X(), dir.Z()) * (180 / H_PI));
      if (azimut < 0) azimut += 36;
      int dist = toInt(0.1 * dir.SizeXZ());
      params.Add("%02d", azimut);
      params.Add(dist);
    }
    break;
CmdPositionFar:
    {
      //VBS supports bigger grids
      char buffer[32];
      PositionToAA11(_command._destination, buffer);
      params.Add(RString(buffer));
    }
    break;
CmdSupplyTarget:
    {
      const EntityAIType *type = tgt->GetType();
      params.AddWord(type->GetNameSound(), type->GetShortName());
      params.AddAzimut(tgt->GetPosition());
      ::ShowGroupDir(_to);
    }
    break;
CmdTarget:
    {
      tgt=_command._targetE;
      if( !tgt )tgt=_from->Leader()->FindTarget(_command._target);

      // check if we should use center 
      if (tgt)
      {
        const EntityAIType *type = tgt->GetType();
        params.AddWord(type->GetNameSound(), type->GetShortName());
      }
      else
      {
        // FIX tgt not found - use center database
        AICenter *center = _from->GetCenter();
        AITargetInfo *aiTgt = center->FindTargetInfo(_command._target);
        if (aiTgt)
        {
          params.AddWord(aiTgt->_type->GetNameSound(), aiTgt->_type->GetShortName());
        }
        else
        {
          params.AddWord("unknown", LocalizeString(IDS_WORD_UNKNOWN));
        }
      }
    }
    break;
CmdTargetPosition:
    {
      if(_command._target)
      {
        char buffer[32];
        PositionToAA11(_command._target->FutureVisualState().Position(), buffer);
        params.Add(RString(buffer));
      }
    }
    break;
  }
  // params.Add(_from->Leader());

  return sentence;
}

/// This functor will remove all vehicle gunners from the list
class RemoveGunnersFunc : public ITurretFunc
{
protected:
  OLinkPermNOArray(AIUnit) &_list;

public:
  RemoveGunnersFunc(OLinkPermNOArray(AIUnit) &list) : _list(list) {}
  bool operator () (EntityAIFull *entity, TurretContext &context)
  {
    if (!context._turret) return false; // only gunners
    if (!context._gunner) return false;
    AIBrain *gunner = context._gunner->Brain();
    AIUnit *unit = gunner ? gunner->GetUnit() : NULL;
    if (unit) _list.DeleteKey(unit);
    return false; // all turrets
  }
};

/// This functor will translate group Attack command to the vehicle Fire command
class TranslateAttackFunc : public ITurretFunc
{
protected:
  OLinkPermNOArray(AIUnit) &_list;
  AIBrain *_leader;
  Target *_target;

public:
  TranslateAttackFunc(OLinkPermNOArray(AIUnit) &list, AIBrain *leader, Target *target)
    : _list(list), _leader(leader), _target(target) {}
  bool operator () (EntityAIFull *entity, TurretContext &context)
  {
    if (!context._turret) return false; // only gunners
    if (!context._gunner) return false;
    AIBrain *gunner = context._gunner->Brain();
    AIUnit *unit = gunner ? gunner->GetUnit() : NULL;
    if (unit && _list.FindKey(unit) >= 0)
    {
      if (unit != _leader) static_cast<Transport *>(entity)->ReceivedFire(context._gunner, _target);
      // translate the command
      _list.DeleteKey(unit);
    }
    return false; // all turrets
  }
};

static void DeleteSubgroup(OLinkPermNOArray(AIUnit) &list, AIUnit *unit)
{
  AISubgroup *subgrp = unit->GetSubgroup();
  if (!subgrp)
  {
    // delete only the unit
    list.DeleteKey(unit);
    return;
  }

  // delete all units the subgroup contain
  for (int i=list.Size()-1; i>=0; i--)
  {
    if (list[i] && list[i]->GetSubgroup() == subgrp) list.DeleteAt(i);
  }
}

/*!
\patch 1.34 Date 12/06/2001 by Ondra
- Fixed: MP: Player character autoresponded "Ready" when given command.
*/

void RadioMessageCommand::Transmitted(ChatChannel channel)
{
  AIBrain *agent = GWorld->FocusOn();
  AIUnit *unit = agent ? agent->GetUnit() : NULL;
  if (unit && IsTo(unit) && GWorld->UI())
  {
    if (unit->GetGroup() && unit->GetSubgroup() == unit->GetGroup()->MainSubgroup())
      GWorld->UI()->ShowWaypointPosition();
    else
      GWorld->UI()->ShowCommand();
  }

  if (!_transmit) return;

  if (!_from)	return;
  AIUnit *leader = _from->Leader();
  if (!leader)	return;
  if (!leader->LSCanSpeak()) return;

  if
  ( 
    !_command._target && !_command._targetE &&
    (
      _command._message == Command::Attack ||
      _command._message == Command::AttackAndFire ||
      _command._message == Command::Fire ||
      _command._message == Command::FireAtPosition
    )
  )
  {
    return;
  }
  if
  (
    !_command._target &&
    (
      _command._message == Command::GetIn ||
      _command._message == Command::Heal ||
      _command._message == Command::HealSoldier ||
      _command._message == Command::RepairVehicle ||
      _command._message == Command::Repair ||
      _command._message == Command::Refuel ||
      _command._message == Command::Rearm ||
      _command._message == Command::Support
    )
  )
    return;

  bool confirm = 
    _command._message != Command::NoCommand &&
    _command._message != Command::Wait &&
    _command._context != Command::CtxUndefined &&
    _command._context != Command::CtxJoin;

  unit = NULL;
  if (_toMainSubgroup)
  {
    _from->MainSubgroup()->ReceiveCommand(_command);
    if (confirm)
    {
      unit = _from->MainSubgroup()->Leader();
      if (!unit) unit = leader;
    }
  }
  else
  {
    if (_to.Count() > 0)
    {
      bool wholeCrew =
        _command._message != Command::GetIn &&
        _command._message != Command::GetOut &&
        !(_command._message == Command::Action && _command._action && _command._action->GetType() == ATEject);
      
      OLinkPermNOArray(AIUnit) list;
      PrepareList(list, _to, wholeCrew);
      // exclude units that cannot hear message
      AIBrain *sender = GetSender();
      if (sender)
      {
        for (int i=0; i<list.Size();)
        {
          AIUnit *unit = list[i];
          Assert(unit);
          if (unit->CanHear(channel, sender))
            i++;
          else
            list.Delete(i);
        }
      }
      // translate command from leader to his vehicle
      Transport *leaderVehicle = leader->GetVehicleIn();
      if (leaderVehicle && leaderVehicle->CommanderUnit() == leader)
      {
        AIBrain *driver = leaderVehicle->PilotUnit();
        AIUnit *driverUnit = driver ? driver->GetUnit() : NULL;
        switch (_command._message)
        {
        case Command::Move:
          {
            // ignored by gunners
            RemoveGunnersFunc func(list);
            leaderVehicle->ForEachTurret(func);
          }
          if (driverUnit && list.FindKey(driverUnit) >= 0)
          {
            if (driverUnit != leader) leaderVehicle->ReceivedMove(_command._destination);
            // delete not only the driver, but also all his subgroup (the command is ordered to the whole subgroup)
            DeleteSubgroup(list, driverUnit);
          }
          break;
        case Command::Stop:
          {
            // ignored by gunners
            RemoveGunnersFunc func(list);
            leaderVehicle->ForEachTurret(func);
          }
          if (driverUnit && list.FindKey(driverUnit) >= 0)
          {
            if (driverUnit != leader) leaderVehicle->ReceivedSimpleCommand(driver->GetPerson(), SCStop);
            // delete not only the driver, but also all his subgroup (the command is ordered to the whole subgroup)
            DeleteSubgroup(list, driverUnit);
          }
          break;
        case Command::Attack:
        case Command::AttackAndFire:
          {
            // translate for gunners
            TranslateAttackFunc func(list, leader, _command._targetE);
            leaderVehicle->ForEachTurret(func);
          }
          if (driverUnit)
          {
            list.DeleteKey(driverUnit);
          }
          break;
        case Command::Join:
          {
            // ignored by gunners
            RemoveGunnersFunc func(list);
            leaderVehicle->ForEachTurret(func);
          }
          if (driverUnit && list.FindKey(driverUnit) >= 0)
          {
            if (driverUnit != leader) leaderVehicle->ReceivedJoin();
            list.DeleteKey(driverUnit);
          }
          for (int i=0; i<leaderVehicle->GetManCargo().Size(); i++)
          {
            Person *person = leaderVehicle->GetManCargo()[i];
            if (!person) continue;
            AIBrain *agent = person->Brain();
            if (!agent) continue;
            AIUnit *unit = agent->GetUnit();
            if (unit && unit->GetGroup() == _from)
              list.DeleteKey(unit);
          }
          break;
        }
      }

      if (list.Size() > 0)
      {
        _from->IssueCommand(_command, list);
        if (confirm)
        {
          unit = NULL;
          for (int i=0; i<_to.Size(); i++)
          {
            unit = _to[i];
            if (unit) break;
          }
          Assert(unit);
        }
      }
    }
  }

  if (unit)
  {
    if (!unit->IsAnyPlayer())
    {
      // active channel selection
      RadioChannel *activeChannel = &_from->GetRadio();
#if _ENABLE_DIRECT_MESSAGES
      AIUnit *leader = _from->Leader();
      if (leader && !unit->UseRadio(leader)) activeChannel = unit->GetPerson()->GetRadio();
#endif

      Assert(activeChannel);
      activeChannel->Transmit(new RadioMessageCommandConfirm(unit, _from, _command));
    }
  }
}

///////////////////////////////////////////////////////////////////////////////
// class RadioMessageText

LSError RadioMessageText::Serialize(ParamArchive &ar)
{
  CHECK(RadioMessage::Serialize(ar))
  CHECK(ar.Serialize("wave", _wave, 1))
  CHECK(ar.Serialize("ttl", _timeToLive, 1))
  CHECK(ar.SerializeRef("sender", _sender, 1))
  CHECK(ar.Serialize("senderName", _senderName, 1, RString()))

  return LSOK;
}

const char *RadioMessageText::GetPriorityClass()
{
  return "Design";
}

RString RadioMessageText::PrepareSentence(SentenceParams &params) const
{
  SoundPars pars;
  ConstParamEntryPtr cls = FindRadio(_wave, pars, true); // check only, avoid warning message
  if (cls) return RString(); // if possible, interpret _wave as an entry in CfgRadio

  return _wave; // otherwise, interpret it as a radio protocol sentence
}

void RadioMessageText::Transmitted(ChatChannel channel)
{
}

RString RadioMessageText::GetWave()
{
  return _wave;
}

float RadioMessageText::GetDuration() const
{
  return _timeToLive;
}

#if _ENABLE_CONVERSATION

///////////////////////////////////////////////////////////////////////////////
// class RadioMessageTalk

RadioMessageTalk::RadioMessageTalk(const KBMessageInfo *message, AIBrain *sender, AIBrain *receiver, float ttl)
{
  _message = unconst_cast(message); // store as non-const reference because of serialization
  _sender = sender;
  _receiver = receiver;
  _timeToLive = ttl;

  if (_sender) _sender->ChangeSpeaking(+1);
  if (_receiver) _receiver->ChangeListening(+1);
}

RadioMessageTalk::~RadioMessageTalk()
{
  if (_sender) _sender->ChangeSpeaking(-1);
  if (_receiver) _receiver->ChangeListening(-1);
}

RString RadioMessageTalk::GetWave()
{
  return RString();
}

RString RadioMessageTalk::GetText()
{
  if (!_sender || !_sender->LSCanSpeak() || !_sender->GetKBCenter()) return RString();
  if (!_message) return RString();

  const KBCenter *parent = _sender->GetKBParent();
  return _sender->GetKBCenter()->GetText(_message, parent);
}

float RadioMessageTalk::GetDuration() const
{
  if (!_sender || !_sender->LSCanSpeak() || !_sender->GetKBCenter()) return 0;
  if (!_message) return 0;

  const KBCenter *parent = _sender->GetKBParent();
  RString text = _sender->GetKBCenter()->GetText(_message, parent);

  // when no text is shown, we need not wait (empty sentences used as some control messages in SecOp system)
  if (text.GetLength() == 0) return 0;
  return _timeToLive;
}

void RadioMessageTalk::GetSpeech(RadioSentence &sentence, const RadioChannelParams &cfg)
{
  if (!_sender || !_sender->LSCanSpeak() || !_sender->GetKBCenter()) return;
  if (!_message) return;
  
  AutoArray<RString> speech;
  const KBCenter *parent = _sender->GetKBParent();
  _sender->GetKBCenter()->GetSpeech(speech, _message, parent);
  int n = speech.Size();
  if (n == 0) return;

  for (int i=0; i<n; i++)
    sentence.Add(speech[i], cfg.pauseAfterWord);
}

void RadioMessageTalk::Transmitted(ChatChannel channel)
{
  if (!_sender || !_sender->LSCanSpeak() || !_sender->GetKBCenter()) return;
  if (!_receiver || !_receiver->LSIsAlive()) return;
  if (!_message) return;
  if (!_receiver->CanHear(channel, _sender)) return;
  
#if _ENABLE_IDENTITIES
  _sender->GetPerson()->GetIdentity().AddSaid(_receiver, _message);

  const KBCenter *kbParent = _sender->GetKBParent();
  RString message = _sender->GetKBCenter()->GetText(_message, kbParent);
  if (message.GetLength() > 0)
  {
    // as unit use the person we are speaking with
    // as target use the speaker
    _sender->GetPerson()->GetIdentity().AddDiaryRecord("Conversation", RString(), message, NULL, TSNone, _receiver, _sender, true);
    _receiver->GetPerson()->GetIdentity().AddDiaryRecord("Conversation", RString(), message, NULL, TSNone, _sender, _sender, true);
  }
#endif

  if (_receiver->IsLocal())
  {
    _receiver->KBReact(_sender, _message);
  }
  else
  {
    GetNetworkManager().KBReact(_sender, _receiver, _message);
  }
}

const char *RadioMessageTalk::GetPriorityClass()
{
  return "Design";
}

LSError RadioMessageTalk::Serialize(ParamArchive &ar)
{
  CHECK(RadioMessage::Serialize(ar))

  CHECK(ar.SerializeRef("sender", _sender, 1))
  CHECK(ar.SerializeRef("receiver", _receiver, 1))
  CHECK(ar.Serialize("ttl", _timeToLive, 1))

  KBSaveContext context(_sender, &GGameState);
  void *oldParams = ar.SetParams(&context);
  CHECK(ar.Serialize("message", _message, 1))
  ar.SetParams(oldParams);

  if (ar.IsLoading() && ar.GetPass() == ParamArchive::PassSecond)
  {
    if (_sender) _sender->ChangeSpeaking(+1);
    if (_receiver) _receiver->ChangeListening(+1);
  }

  return LSOK;
}

#endif // _ENABLE_CONVERSATION

#if _ENABLE_IDENTITIES

#if _ENABLE_INDEPENDENT_AGENTS

AIBrain *GetSpeaker(AITeamMember *member)
{
  if (!member) return NULL;
  AIAgent *agent = member->GetAgent();
  while (!agent)
  {
    AITeam *team = member->GetTeam();
    if (!team) return NULL;
    member = team->GetLeader();
    if (!member) return NULL;
    agent = member->GetAgent();
  }
  return agent;
}

#endif

LSError RadioMessageTask::Serialize(ParamArchive &ar)
{
  CHECK(RadioMessage::Serialize(ar))
  CHECK(ar.Serialize("Task", _task, 1))

  void *old = ar.GetParams();
  ar.SetParams(&GGameState);
  CHECK(ar.Serialize("Params", _params, 1))
  ar.SetParams(old);

  return LSOK;
}

const char *RadioMessageTask::GetPriorityClass()
{
  return "NormalCommand";
}

void RadioMessageTask::Transmitted(ChatChannel channel)
{
#if _ENABLE_INDEPENDENT_AGENTS
  if (!GetSender()) return;
  
  Assert(_task);
  AITeamMember *to = _task->GetOwner();
  if (!to) return;
  to->AddTask(_task);

  // send confirmation
  AIBrain *speaker = ::GetSpeaker(to);
  if (speaker && !speaker->IsAnyPlayer())
  {
    to->SendMsg(new RadioMessageTaskReceived(_task));
  }
#endif
}

AIBrain *RadioMessageTask::GetSender() const
{
#if _ENABLE_INDEPENDENT_AGENTS
  AITeamMember *from = _task->GetSender();
  return ::GetSpeaker(from);
#else
  return NULL;
#endif
}

RString RadioMessageTask::PrepareSentence(SentenceParams &params) const
{
#if _ENABLE_INDEPENDENT_AGENTS
  if (!GetSender()) return RString();

  Assert(_task);
  AITeamMember *to = _task->GetOwner();
  if (!to) return RString();

  params.Add(to);
  if (_params.GetType() == GameArray)
  {
    const GameArrayType &array = _params;
    for (int i=0; i<array.Size(); i++) params.Add(array[i], _task->GetSender(), to);
  }
  else
  {
    params.Add(_params, _task->GetSender(), to);
  }

  return _task->GetSentence("SendTask");
#else
  return RString();
#endif
}

LSError RadioMessageTaskReceived::Serialize(ParamArchive &ar)
{
  CHECK(RadioMessage::Serialize(ar))
  CHECK(ar.SerializeRef("Task", _task, 1))
  return LSOK;
}

const char *RadioMessageTaskReceived::GetPriorityClass()
{
  return "Confirmation";
}

void RadioMessageTaskReceived::Transmitted(ChatChannel channel)
{
  // nothing to do
}

AIBrain *RadioMessageTaskReceived::GetSender() const
{
#if _ENABLE_INDEPENDENT_AGENTS
  AITeamMember *from = _task->GetOwner();
  return ::GetSpeaker(from);
#else
  return NULL;
#endif
}

RString RadioMessageTaskReceived::PrepareSentence(SentenceParams &params) const
{
#if _ENABLE_INDEPENDENT_AGENTS
  if (!GetSender()) return RString();
  
  Assert(_task);
  AITeamMember *to = _task->GetSender();
  if (!to) return RString();

  params.Add(to);

  return _task->GetSentence("SendTaskReceived");
#else
  return RString();
#endif
}

LSError RadioMessageTaskResult::Serialize(ParamArchive &ar)
{
  CHECK(RadioMessage::Serialize(ar))
  CHECK(ar.SerializeRef("Task", _task, 1))
  CHECK(ar.SerializeEnum("state", _state, 1))

  void *old = ar.GetParams();
  ar.SetParams(&GGameState);
  CHECK(ar.Serialize("Result", _result, 1))
  ar.SetParams(old);

  CHECK(ar.Serialize("sentence", _sentence, 1))
  return LSOK;
}

const char *RadioMessageTaskResult::GetPriorityClass()
{
  return "Completition";
}

void RadioMessageTaskResult::Transmitted(ChatChannel channel)
{
#if _ENABLE_INDEPENDENT_AGENTS
  if (!GetSender()) return;
  Assert(_task);
  AITeamMember *to = _task->GetSender();
  if (!to) return;

  _task->SetState(_state);
  _task->SetResult(_result);
#endif
}

AIBrain *RadioMessageTaskResult::GetSender() const
{
#if _ENABLE_INDEPENDENT_AGENTS
  AITeamMember *from = _task->GetOwner();
  return ::GetSpeaker(from);
#else
  return NULL;
#endif
}

RString RadioMessageTaskResult::PrepareSentence(SentenceParams &params) const
{
#if _ENABLE_INDEPENDENT_AGENTS
  if (!GetSender()) return RString();
  Assert(_task);
  AITeamMember *to = _task->GetSender();
  if (!to) return RString();

  params.Add(to);
  if (_result.GetType() == GameArray)
  {
    const GameArrayType &array = _result;
    for (int i=0; i<array.Size(); i++) params.Add(array[i], _task->GetOwner(), to);
  }
  else
  {
    params.Add(_result, _task->GetOwner(), to);
  }

  return _task->GetSentence(_sentence);
#else
  return RString();
#endif
}

#endif // _ENABLE_IDENTITIES
