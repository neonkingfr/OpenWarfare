// AI - implementation of Groups

#include "../wpch.hpp"
#include "ai.hpp"
#include "../person.hpp"
#include "aiRadio.hpp"
#include "../global.hpp"

#include "../world.hpp"
#include "../landscape.hpp"
#include "../paramArchiveExt.hpp"
#include <El/Common/randomGen.hpp>

#include "../camEffects.hpp"
#include <El/HiResTime/hiResTime.hpp>

#include "../stringtableExt.hpp"

#include <Es/Algorithms/qsort.hpp>

#include "../UI/chat.hpp"
#include "../Network/network.hpp"
#include <El/Common/perfProf.hpp>
#include <El/Common/perfLog.hpp>

#include <El/Enum/enumNames.hpp>
#include "../UI/uiActions.hpp"

#include "../move_actions.hpp"

#include "../arcadeTemplate.hpp"

#include "../diagModes.hpp"

#if _VBS2 // group eventhandlers
#include "../gameStateExt.hpp"
#endif

///////////////////////////////////////////////////////////////////////////////
// Parameters

// limit for think error when AIGroup::Think must be called
static const float maxThinkError = 1.0f;
// multiplication coefficients to think error based on simulation importance
static const float importanceCoefs[SimulateDefault + 1] =
{
  2.0f, // SimulateVisibleNear
  1.0f, // SimulateVisibleFar
  0.5f, // SimulateInvisibleNear
  0.25f, // SimulateInvisibleFar
  0.25f  // SimulateDefault
};

#include "aiDefs.hpp"

#pragma warning( disable: 4355 )

void AIUnitSlot::Init()
{
  _assignTarget = NULL;
  _assignValidUntil = Time(0);
  _assignTargetState = TargetEnemyCombat;

  _healthState = AIUnit::RSNormal;
  _ammoState = AIUnit::RSNormal;
  _fuelState = AIUnit::RSNormal;
  _dammageState = AIUnit::RSNormal;
  _reportedDown = false;
  _reportBeforeTime = TIME_MAX;
}

void AIUnitSlot::Copy(const AIUnitSlot &src)
{
  _assignTarget = src._assignTarget;
  _assignValidUntil = src._assignValidUntil;
  _assignTargetState = src._assignTargetState;

  _healthState = src._healthState;
  _ammoState = src._ammoState;
  _fuelState = src._fuelState;
  _dammageState = src._dammageState;
  _reportedDown = src._reportedDown;
  _reportBeforeTime = src._reportBeforeTime;
}

///////////////////////////////////////////////////////////////////////////////
// class AIGroup

// Construction and destruction
AIGroup::AIGroup(bool createMainSubgroup)
  : _radio(CCGroup, Pars >> "RadioChannels" >> "GroupChannel"), _vars(true)
{
  _id = 0;
  _roleIndex = -1;

  _lastUpdateTime = Glob.time - maxThinkError * GRandGen.RandomValue();
  _semaphore = SemaphoreYellow;
  _combatModeMinor = CMSafe; // default auto behaviour

  _nextCmdId = 0;
  _locksWP = 0;

  _enemiesDetected = 0;
  _unknownsDetected = 0;
  _lastEnemyDetected = Glob.time;
  // avoid assigning all groups at once

  _expensiveThinkFrac = toIntFloor(GRandGen.RandomValue()*1000);
  // perform first expensive think as soon as possible
  _expensiveThinkTime = Glob.time.Floor().AddMs(_expensiveThinkFrac);
  if (_expensiveThinkTime < Glob.time) Glob.time.AddMs(1000);
  _enemyRecomputeCostTime = TIME_MAX;

  _checkCenterDBase = Glob.time-60;
  
  _lastSendTargetTime = Glob.time-60;

  _disclosed = TIME_MIN;

  _maxStrength = 0;
  _forceCourage = -1;
  _courage = 1.0;
  _flee = false;

  _attackEnabled = true;

  _threshold = 0;
  _thresholdValid = Glob.time - 1.0;

  _nearestEnemyDist2 = 100; // unless we know better, assume 100 m

  _guardPosition = VZero;

  _coreUpdated = false;
  _coreRadius = 0;
  _corePosition = VZero;
  _groupCompactness = 1;
  _martaTypeIx = -1;

  if (createMainSubgroup)
  {
    _mainSubgroup = new AISubgroup();
    AddSubgroup(_mainSubgroup);
  }
}

#if _VBS2 // group eventhandlers

#include <El/Evaluator/express.hpp>

DEFINE_ENUM(GroupEvent,GE,GROUP_EVENT_ENUM)

int AIGroup::AddEventHandler(GroupEvent event, RString expression)
{
  return _eventHandlers[event].Add(expression);
}
void AIGroup::RemoveEventHandler(GroupEvent event, int handle)
{
  _eventHandlers[event].Delete(handle);
}
void AIGroup::ClearEventHandlers(GroupEvent event)
{
  _eventHandlers[event].Clear();
}

const AutoArray<RString> &AIGroup::GetEventHandlers(GroupEvent event) const
{
  return _eventHandlers[event];
}

bool AIGroup::IsEventHandler(GroupEvent event) const
{
  const AutoArray<RString> &handlers = GetEventHandlers(event);
  if (handlers.Size()>0) return true;
  return false;
}

void AIGroup::OnEvent(GroupEvent event, const GameValue &pars)
{
  const AutoArray<RString> &handlers = GetEventHandlers(event);
  for (int i=0; i<handlers.Size(); i++)
  {
    GameVarSpace local(false);
    GameState *gstate = GWorld->GetGameState();
    gstate->BeginContext(&local);
    gstate->VarSetLocal("_this",pars,true);
    RString handler = handlers[i];
    gstate->Execute(handler, GameState::EvalContext::_default, GWorld->GetMissionNamespace());
    gstate->EndContext();
  }
}

void AIGroup::OnEvent(GroupEvent event, float par1)
{
  if (!IsEventHandler(event)) return;
  GameValue value = GWorld->GetGameState()->CreateGameValue(GameArray);
  GameArrayType &arguments = value;
  arguments.Add(GameValueExt(this));
  arguments.Add(GameValue(par1));
  OnEvent(event,value);
}
#endif

void AIGroup::ExpensiveThinkDone()
{
  // calculate time of next expensive think
  // find start of this second
  // advance to start of next second, advance to our frac
  Time nextTime = _expensiveThinkTime.AddMs(1000);
  if (nextTime<Glob.time-2)
  {
    nextTime = Glob.time.Floor().AddMs(1000+_expensiveThinkFrac);
  }
  _expensiveThinkTime = nextTime;
  #if 0
  LogF
  (
    "%.3f: Think %s (%d)",
    Glob.time-Time(0),(const char *)GetDebugName(),_expensiveThinkFrac
  );
  #endif
}

AIGroup::~AIGroup()
{
  _targetList.Clear();
  /* Why this?
  if (IsLocal())
  {
    NetworkId ni = GetNetworkId();
    if (!ni.IsNull())
    {
      GetNetworkManager().DeleteObject(ni);
      if (!GetNetworkManager().IsServer())
      {
        ErrF
        (
          "Warning: Client is deleting local AIGroup: %s, %d:%d",
          (const char *)GetDebugName(), ni.creator, ni.id
        );
      }
    }
  }
  */
}

AICenter* AIGroup::GetCenterS() const
{
  // not-inlined implementation
  return _center;
}



AIGroup *AIGroup::LoadRef(ParamArchive &ar)
{
  TargetSide side = TSideUnknown;
  int id;
  if (ar.SerializeEnum("side", side, 1) != LSOK) return NULL;
  if (ar.Serialize("id", id, 1) != LSOK) return NULL;
  AICenter *center = NULL;
  switch (side)
  {
    case TWest:
      center = GWorld->GetWestCenter();
      break;
    case TEast:
      center = GWorld->GetEastCenter();
      break;
    case TGuerrila:
      center = GWorld->GetGuerrilaCenter();
      break;
    case TCivilian:
      center = GWorld->GetCivilianCenter();
      break;
    case TLogic:
      center = GWorld->GetLogicCenter();
      break;
  }
  if (!center) return NULL;
  for (int i=0; i<center->NGroups(); i++)
  {
    AIGroup *grp = center->GetGroup(i);
    if (grp && grp->ID() == id) return grp; 
  }
  return NULL;
}

LSError AIGroup::SaveRef(ParamArchive &ar) const
{
  TargetSide side = GetCenter() ? GetCenter()->GetSide() : TSideUnknown;
  int id = ID();
  CHECK(ar.SerializeEnum("side", side, 1))
  CHECK(ar.Serialize("id", id, 1))
  return LSOK;
}

LSError AIGroup::Serialize(ParamArchive &ar)
{
  CHECK(base::Serialize(ar))

  // structure
  CHECK(ar.Serialize("Subgroups", _subgroups, 1))
  CHECK(ar.SerializeRef("MainSubgroup", _mainSubgroup, 1))
  CHECK(ar.SerializeRef("Leader", _leader, 1))
  CHECK(ar.SerializeRef("Center", _center, 1))
  CHECK(ar.Serialize("Radio", _radio, 1))
  CHECK(ar.SerializeRef("HCCommander", _hcCommander, 1))
  CHECK(ar.SerializeRef("UnconsciousLeader", _unconsciousLeader, 1))

  if (ar.IsLoading())
  {
    ParamArchive::Pass pass;
    if (ar.GetArVersion() < 15)
    {
      // array is used for refs, so create it in the first pass
      pass = ParamArchive::PassFirst;
    }
    else
    {
      // array is loaded in the second pass (only references are in subgroups) 
      pass = ParamArchive::PassSecond;
    }
    if (ar.GetPass() == pass)
    {
      // build units by their IDs
      _unitSlots.Resize(0);
      for (int i=0; i<_subgroups.Size(); i++)
      {
        AISubgroup *subgrp = _subgroups[i];
        Assert(subgrp);
        if (!subgrp) continue;
        for (int j=0; j<subgrp->NUnits(); j++)
        {
          AIUnit *unit = subgrp->GetUnit(j);
          Assert(unit);
          if (!unit) continue;
          int index = unit->ID() - 1;
          Assert(index >= 0);
          _unitSlots.Access(index);
          _unitSlots[index]._unit = unit;
        }
      }
    }
  }

  for (int i=0; i<_unitSlots.Size(); i++)
  {
    if (ar.IsSaving() || ar.GetPass() == ParamArchive::PassSecond)
    {
      // for loading, _unit is not known in the first pass
      AIUnitSlot &slot = _unitSlots[i];
      if (slot._unit)
      {
        char buf[256];
        sprintf(buf,"AssignTarget%d",i);
        CHECK(ar.SerializeRef(buf, slot._assignTarget, 1))
        
        if (ar.IsLoading()) ar.FirstPass();

        sprintf(buf,"AssignTargetState%d",i);
        CHECK(ar.SerializeEnum(buf, slot._assignTargetState, 1, TargetEnemyCombat))
        sprintf(buf,"assignValidUntil%d",i);
        CHECK(::Serialize(ar, buf, slot._assignValidUntil, 1, Time(0)))
        sprintf(buf,"ammo%d",i);
        CHECK(ar.SerializeEnum(buf, slot._ammoState, 1, AIUnit::RSNormal));
        sprintf(buf,"health%d",i);
        CHECK(ar.SerializeEnum(buf, slot._healthState, 1, AIUnit::RSNormal));
        sprintf(buf,"dammage%d",i);
        CHECK(ar.SerializeEnum(buf, slot._dammageState, 1, AIUnit::RSNormal));
        sprintf(buf,"fuel%d",i);
        CHECK(ar.SerializeEnum(buf, slot._fuelState, 1, AIUnit::RSNormal));
        sprintf(buf,"down%d",i);
        CHECK(ar.Serialize(buf, slot._reportedDown, 1, false));
        sprintf(buf,"reporttime%d",i);
        CHECK(::Serialize(ar, buf, slot._reportBeforeTime, 1, TIME_MAX));

        if (ar.IsLoading()) ar.SecondPass();
      }
    }
  }

  CHECK(ar.Serialize("expensiveThinkFrac",_expensiveThinkFrac,1,0));
  CHECK(::Serialize(ar, "expensiveThinkTime",_expensiveThinkTime,1,Time(0)));
  CHECK(::Serialize(ar, "checkCenterDBase",_checkCenterDBase,1,Time(0)));
  // state
  CHECK(::Serialize(ar, "lastUpdateTime", _lastUpdateTime, 1))
  CHECK(::Serialize(ar, "checkTime", _checkTime, 1))	// used in seek and destroy waypoint
  CHECK(ar.SerializeEnum("semaphore", _semaphore, 1, SemaphoreYellow))
  CHECK(ar.SerializeEnum("combatModeMinor", _combatModeMinor, 1, CMSafe))
  CHECK(::Serialize(ar, "lastEnemyDetected", _lastEnemyDetected, 1))
  CHECK(ar.Serialize("nextCmdId", _nextCmdId, 2, 0))
  CHECK(ar.Serialize("locksWP", _locksWP, 2, 0))

  CHECK(ar.Serialize("nearestEnemyDist2", _nearestEnemyDist2, 1, 0));
  CHECK(ar.Serialize("enemiesDetected", _enemiesDetected, 1, 0))
  CHECK(ar.Serialize("unknownsDetected", _unknownsDetected, 1, 0))

  CHECK(::Serialize(ar, "disclosed", _disclosed, 1))

  // info
  CHECK(ar.Serialize("id", _id, 1))

  CHECK(ar.Serialize("nameFormat", _nameFormat, 1, ""))
  CHECK(ar.SerializeArray("nameParts", _nameParts, 1))

  CHECK(::Serialize(ar,"groupIcons", _groupMakers, 1));

  if (ar.IsLoading() && ar.GetPass() == ParamArchive::PassFirst)
  {
    if (_nameFormat.GetLength() == 0)
      Init();
    else
      ApplyIdentity();
  }

  // targets
  CHECK(_targetList.Serialize(ar, "targetList", 1))
#if _ENABLE_CONVERSATION
  CHECK(ar.Serialize("targetKB", _targetKB, 1))
#endif

  // assigned vehicles
  CHECK(ar.SerializeRefs("Vehicles", _vehicles, 1))

  CHECK(ar.SerializeRef("OverlookTarget", _overlookTarget, 1))
  CHECK(::Serialize(ar, "guardPosition", _guardPosition, 1))

  // waypoints
  CHECK(ar.Serialize("Waypoints", _wp, 1))

  // scripts
  CHECK(ar.Serialize("Script", _script, 1))

  // flee mode
  CHECK(ar.Serialize("maxStrength", _maxStrength, 1))
  CHECK(ar.Serialize("forceCourage", _forceCourage, 1))
  CHECK(ar.Serialize("courage", _courage, 1))
  CHECK(ar.Serialize("flee", _flee, 1, false))

  CHECK(ar.Serialize("attackEnabled", _attackEnabled, 1, true))

  // threshold for randomization
  CHECK(ar.Serialize("threshold", _threshold, 1))
  CHECK(::Serialize(ar, "thresholdValid", _thresholdValid, 1))

  // variables
  {
    void *old = ar.GetParams();
    ar.SetParams(&GGameState);
    DoAssert(_vars.IsSerializationEnabled());
    LSError result = ar.Serialize("Variables", _vars._vars, 1);
    ar.SetParams(old);
    if (result != LSOK) return result;
  }

  // MP related serialization
  CHECK(ar.Serialize("roleIndex", _roleIndex, 1, -1));

  //serialize NetworkObject
  NetworkObject::Serialize(ar); //base

  if ( ar.IsLoading() && (ar.GetPass()==ParamArchive::PassSecond) 
    && !GetNetworkId().IsNull() )
  {
    //Calling CreateObject is not sufficient
    //GetNetworkManager().CreateObject(this, false /*not setNetworkId*/);
    // we should call appropriate CreateXXX methods when everything is serialized (e.g. subobjects,...)
    // store the "action" to see, what should be created "at least"
    GetNetworkManager().RememberSerializedObject(this);
  }

  // Aggregated Targets need to be recomputed (position, radius and costs were not serialized)
  if (ar.GetPass() == ar.PassSecond)
  {
    _targetList.UpdateAggrTargets(this);
  }

  return LSOK;
}

#define AI_GROUP_MSG_LIST(XX) \
  XX(Create, CreateAIGroup) \
  XX(UpdateGeneric, UpdateAIGroup)

DEFINE_NETWORK_OBJECT(AIGroup, NetworkObject, AI_GROUP_MSG_LIST)

struct NetworkMessageWaypoint;

#define CREATE_AI_GROUP_MSG(MessageName, XX) \
  XX(MessageName, OLinkPermNO(AICenter), center, NDTRef, NetworkId, NCTNone, DEFVALUENULL, DOC_MSG("Superior center"), TRANSF_REF) \
  XX(MessageName, int, id, NDTInteger, int, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG("Unique identifier in center"), TRANSF) \
  XX(MessageName, AutoArray<WaypointInfo>, waypoints, NDTObjectArray, REF_MSG_ARRAY(NetworkMessageWaypoint), NCTNone, DEFVALUE_MSG(NMTWaypoint), DOC_MSG("List of waypoints"), TRANSF_ARRAY) \
  XX(MessageName, int, roleIndex, NDTInteger, int, NCTSmallSigned, DEFVALUE(int, -1), DOC_MSG("Multiplayer role index"), TRANSF)

DECLARE_NET_INDICES_EX(CreateAIGroup, NetworkObject, CREATE_AI_GROUP_MSG)
DEFINE_NET_INDICES_EX(CreateAIGroup, NetworkObject, CREATE_AI_GROUP_MSG)

DEFINE_NET_INDICES_EX_ERR(UpdateAIGroup, NetworkObject, UPDATE_AI_GROUP_MSG)

NetworkMessageFormat &AIGroup::CreateFormat
(
  NetworkMessageClass cls,
  NetworkMessageFormat &format
)
{
  switch (cls)
  {
  case NMCCreate:
    NetworkObject::CreateFormat(cls, format);
    CREATE_AI_GROUP_MSG(CreateAIGroup, MSG_FORMAT)
    break;
  case NMCUpdateGeneric:
    NetworkObject::CreateFormat(cls, format);
    UPDATE_AI_GROUP_MSG(UpdateAIGroup, MSG_FORMAT_ERR)
    break;
  default:
    NetworkObject::CreateFormat(cls, format);
    break;
  }
  return format;
}

AIGroup *AIGroup::CreateObject(NetworkMessageContext &ctx)
{
  PREPARE_TRANSFER(CreateAIGroup)

  OLinkPerm<AICenter> center;
  if (TRANSF_REF_BASE(center, center) != TMOK) return NULL;
  if (!center) return NULL;
  int id;
  if (TRANSF_BASE(id, id) != TMOK) return NULL;
  AIGroup *grp = new AIGroup(false);
  center->AddGroup(grp, id);

  NetworkId objectId;
  if (TRANSF_BASE(objectCreator, objectId.creator) != TMOK) return NULL;
  if (TRANSF_BASE(objectId, objectId.id) != TMOK) return NULL;
  grp->SetNetworkId(objectId);
  grp->SetLocal(false);

  // create waypoints etc.
  grp->TransferMsg(ctx);
  // create mission
  Mission mis;
  mis._action = Mission::Arcade;
  grp->ReceiveMission(mis);
  return grp;
}

void AIGroup::DestroyObject()
{
  if (UnitsCount() > 0) return;

  AICenter *center = _center;
  if (center)
  {
    _center = NULL;
    center->GroupRemoved(this);
  }
}

void ApplyEffects(AIGroup *group, int index);

/*!
\patch 5099 Date 12/8/2006 by Jirka
- Fixed: MP server crash (sometimes after JIP client disconnected)
*/

TMError AIGroup::TransferMsg(NetworkMessageContext &ctx)
{
  switch (ctx.GetClass())
  {
  case NMCCreate:
    TMCHECK(NetworkObject::TransferMsg(ctx))
    {
      PREPARE_TRANSFER(CreateAIGroup)

      if (ctx.IsSending())
      {

        TRANSF_REF(center)
        TRANSF(id)
      }
      TRANSF_ARRAY_EX(waypoints, _wp)
      if (!ctx.IsSending())
      {
        for (int i=0; i<_wp.Size(); i++)
        {
          WaypointInfo &wInfo = _wp[i];
          for (int j=0; j<wInfo.synchronizations.Size(); j++)
          {
            int sync = wInfo.synchronizations[j];
            Assert(sync >= 0);
            if (sync >= synchronized.Size())
              synchronized.Resize(sync + 1);
#if _VBS3 && _VBS_WP_SYNCH
            synchronized[sync].Add(this,i);
#else
            synchronized[sync].Add(this);
#endif
          }
        }
      }
      TRANSF(roleIndex)
    }
    break;
  case NMCUpdateGeneric:
    TMCHECK(NetworkObject::TransferMsg(ctx))
    {
      PREPARE_TRANSFER(UpdateAIGroup)

      if (ctx.IsSending())
      {
        TRANSF_REF(mainSubgroup)
        TRANSF_REF(leader)
      }
      else
      {
        OLinkPermNO(AISubgroup) subgrp;
        TRANSF_REF_EX(mainSubgroup, subgrp)
        if (subgrp != _mainSubgroup)
        {
          if (_mainSubgroup)
          {
            LogF("Warning: main subgroup changed");
          }
          if (!subgrp)
            LogF("Warning: no main subgroup");
          else if (subgrp->GetGroup() != this)
            LogF("Warning: main subgroup not from this group");
          else
            _mainSubgroup = subgrp;
        }
        OLinkPermNO(AIUnit) leader;
        TRANSF_REF_EX(leader, leader)
        if (leader != _leader)
        {
LogF(
  "Group update arrived - leader changed from %s (%d:%d) to %s (%d:%d)",
  _leader ? (const char *)_leader->GetDebugName() : "<null>",
  _leader ? _leader->GetNetworkId().creator : 0,
  _leader ? _leader->GetNetworkId().id : 0,
  leader ? (const char *)leader->GetDebugName() : "<null>",
  leader ? leader->GetNetworkId().creator : 0,
  leader ? leader->GetNetworkId().id : 0
);
          if (!leader)
            LogF("Warning: no group leader");
          else if (leader->GetGroup() != this)
            LogF("Warning: leader not from this group");
          else
            _leader = leader;
  /*
          if (leader && !leader->GetSubgroup()) AddUnit(leader);
          _leader = leader;
  */
        }
      }
      TRANSF_ENUM(semaphore)
      TRANSF_ENUM(combatModeMinor)
      // ??	_lastEnemyDetected
      // ?? _nextCmdId
      // ?? _locksWP
      TRANSF(enemiesDetected)
      TRANSF(unknownsDetected)
      // ?? format.Add(indices->disclosed, NDTTime, NCTNone, DEFVALUE(Time, Time(0)));
      // TODO: _targetList
      // ?? _vehicles
      // ?? _overlookTarget
      // ?? _guardPosition
      // ?? _maxStrength
      TRANSF(forceCourage)
      TRANSF(courage)
      TRANSF(flee)

      TRANSF(attackEnabled)

      // ?? _threshold
      // ?? _thresholdValid
      DoAssert(GetCurrent());

      if (ctx.IsSending())
        TRANSF_EX(waypointIndex, GetCurrent()->_fsm->Var(0))
      else
      {
        int newIndex;
        TRANSF_EX(waypointIndex, newIndex)
        // do not apply effects when initial (or the first JIP) update arrived
        bool applyEffects = !ctx.GetInitialUpdate() && GetNetworkManager().GetClientState() >= NCSGameLoaded;
        SetCurrentWaypoint(newIndex, applyEffects);
      }
    }
    break;
  default:
    TMCHECK(NetworkObject::TransferMsg(ctx))
    break;
  }
  return TMOK;
}

float AIGroup::CalculateError(NetworkMessageContextWithError &ctx)
{
  float error = NetworkObject::CalculateError(ctx);
  {
    PREPARE_TRANSFER(UpdateAIGroup)

    ICALCERR_NEQREF_PERM(AISubgroup, mainSubgroup, ERR_COEF_STRUCTURE)
    ICALCERR_NEQREF_PERM(AIUnit, leader, ERR_COEF_STRUCTURE)
    ICALCERR_NEQ(int, semaphore, ERR_COEF_MODE)
    ICALCERR_NEQ(int, combatModeMinor, ERR_COEF_MODE)
    ICALCERR_ABSDIF(int, enemiesDetected, ERR_COEF_VALUE_MINOR)
    ICALCERR_ABSDIF(int, unknownsDetected, ERR_COEF_VALUE_MINOR)
    ICALCERR_ABSDIF(float, forceCourage, ERR_COEF_MODE)
    ICALCERR_ABSDIF(float, courage, ERR_COEF_MODE)
    ICALCERR_NEQ(bool, flee, ERR_COEF_MODE)
    ICALCERRE_NEQ(int, waypointIndex, GetCurrent()->_fsm->Var(0), ERR_COEF_MODE)
    ICALCERR_NEQ(bool, attackEnabled, ERR_COEF_MODE)
    // TODO: Not implemented
  }
  return error;
}

int AIGroup::UnitsCount() const
{
  int n = 0;
  for (int i=0; i<_unitSlots.Size(); i++)
  {
    const AIUnitSlot &slot = _unitSlots[i];
    if (slot._unit) n++;
  }
  return n;
}

void AIGroup::SetUnit(int index, AIUnit *unit)
{
// low level function used for network transfer
// !!! avoid another using
  _unitSlots.Access(index);
  AIUnitSlot &slot = _unitSlots[index];
  slot.Init();
  slot._unit = unit;
}

int AIGroup::NSoldiers() const
{
  int n = 0;
  for (int i=0; i<_unitSlots.Size(); i++)
  {
    const AIUnitSlot &slot = _unitSlots[i];
    if (slot._unit && slot._unit->IsFreeSoldier() && !slot._unit->VehicleAssigned()) n++;
  }
  return n;
}

int AIGroup::NAliveUnits() const
{
  int alive = 0;
  for (int i=0; i<NUnits(); i++)
  {
    AIUnit *unit = GetUnit(i);
    if (unit && unit->LSIsAlive()) alive++;
  }
  return alive;
}

int AIGroup::NFreeVehicles() const
{
  int n = 0;
  for (int i=0; i<_vehicles.Size(); i++)
  {
    Transport *veh = _vehicles[i];
    if (veh && !veh->GetDriverAssigned()) n++;
  }
  return n;
}

int AIGroup::NFreeManCargo() const
{
  int n = 0;
  for (int i=0; i<_vehicles.Size(); i++)
  {
    Transport *veh = _vehicles[i];
    if (veh)
    {
      for (int i=0; i<veh->GetManCargo().Size(); i++)
      {
        if (!veh->GetCargoAssigned(i)) n++;
      }
    }
  }
  return n;
}

void AIGroup::OnTargetDeleted(Target *t)
{
#if _ENABLE_CONVERSATION
  _targetKB.Add(new TargetKnowledge(t));
#endif
}

void AIGroup::SetCombatModeMajor(CombatMode mode)
{
  for (int i=0; i<_unitSlots.Size(); i++)
  {
    const AIUnitSlot &slot = _unitSlots[i];
    if (slot._unit) slot._unit->SetCombatModeMajor(mode);
  }
}

void AIGroup::EnableIRLasers(bool enable)
{
  for (int i=0; i<_unitSlots.Size(); i++)
  {
    const AIUnitSlot &slot = _unitSlots[i];
    Person *person = slot._unit ? slot._unit->GetPerson() : NULL;
    if (person)
    {
      person->EnableIRLaser(enable);
    }
  }
}

void AIGroup::EnableLightOnGun(bool enable)
{
  for (int i=0; i<_unitSlots.Size(); i++)
  {
    const AIUnitSlot &slot = _unitSlots[i];
    Person *person = slot._unit ? slot._unit->GetPerson() : NULL;
    if (person)
    {
      person->ForceGunLight(enable);
    }
  }
}

void AIGroup::SetCombatModeMajorReactToChange(CombatMode mode)
{
  for (int i=0; i<_unitSlots.Size(); i++)
  {
    const AIUnitSlot &slot = _unitSlots[i];
    if (slot._unit) slot._unit->SetCombatModeMajorReactToChange(mode);
  }
}

void AIGroup::LockWP(bool lock)
{
  if (lock) _locksWP++; else _locksWP--;
}

void AIGroup::Disclose(DangerCause cause, AIUnit *sender, const Array< Ref<AIUnit> > *possibleSender, EntityAI *causedBy)
{
  _disclosed = Glob.time;
  
  if (IsPlayerGroup())
  {
    if (NUnits()>1 && _combatModeMinor < CMCombat && possibleSender)
    {
      AIUnit *nonPlayerSender = NULL;
      // find some non-player unit which can report the event
      if (!sender || sender->IsAnyPlayer())
      {
        for (int i=0; i<possibleSender->Size(); i++)
        {
          AIUnit *unit = possibleSender->Get(i);
          if (unit && !unit->IsAnyPlayer() && unit->LSCanSpeak())
          {
            nonPlayerSender = unit;
            break;
          }
        }
      }
      else
        nonPlayerSender = sender;
      
      if (nonPlayerSender)
      {
        if (cause == DCFire || cause == DCHit || cause == DCExplosion)
        {
          _combatModeMinor = CMCombat;
          SendUnderFire(nonPlayerSender);
        }
      }
    }
    return;
  }
  if (!sender)
  {
    sender = Leader();
    if (!sender) return;
  }

  // sender is valid now
  if (!sender->IsLocal()) return;
  if (!sender->LSIsAlive()) return;

  //LogF("%s: disclosed %.2f",(const char *)GetDebugName(),Glob.time-Time(0));
  
  if( !_flee )
  {
    // note: combatModeMinor is never CMCareless (it is autodetected)
    // combatModeMajor may be - but this is member of leader
    if (_combatModeMinor < CMCombat)
    {
      AIUnit *unit = Leader();
      
      CombatMode oldCM = unit ? unit->GetCombatMode() : CMSafe;
      
      _combatModeMinor = CMCombat;
      
      CombatMode newCM = unit ? unit->GetCombatMode() : CMSafe;
      if (oldCM!=newCM) unit->OnCombatModeChanged(oldCM,newCM);
      
      if
      (
        unit && unit->LSIsAlive() &&
        unit->GetCombatModeMajor()!=CMCareless
      )
      {
        if (cause == DCFire || cause == DCHit || cause == DCExplosion || cause == DCScream)
          SendUnderFire(sender);

        // unload all units in car cargo position
        for (int i=0; i<_unitSlots.Size(); i++)
        {
          AIUnit *unit = _unitSlots[i]._unit;
          if (!unit) continue;
          // going to combat - get out of some vehicles
          if (unit->IsInCargo())
          {
            Transport *trans = unit->GetVehicleIn();
            if (trans && trans->Type()->GetUnloadInCombat())
            {
              unit->OrderGetIn(false);
            }
          }
        }
      }
    }
    if (_semaphore!=SemaphoreBlue)
    {
      _semaphore = ApplyOpenFire(_semaphore, OFSOpenFire);
      OLinkPermNOArray(AIUnit) list;
      for (int i=0; i<_unitSlots.Size(); i++)
      {
        AIUnit *unit = _unitSlots[i]._unit;
        if (unit) list.Add(unit);
      }
      SendOpenFire(OFSOpenFire, list);
    }
  }
}

// Changes of structure
void AIGroup::SubgroupRemoved(AISubgroup *subgrp)
{
  DoAssert(subgrp->NUnits() == 0);
  DoAssert(subgrp != MainSubgroup()); 

  int index = _subgroups.Find(subgrp);
  Assert(index >= 0);
  if (index < 0)
    return;

  _subgroups.Delete(index);
}

void AIGroup::UnitRemoved(AIUnit *unit)
{
  Assert(unit->GetGroup() == this);
  bool bLeader = (unit == Leader());

  int index = unit->ID() - 1;
  if (index >= 0 && index < _unitSlots.Size())
  {
    if (_unitSlots[index]._unit == unit)
    {
      _unitSlots[index]._unit = NULL;
    }
    else
    {
      RptF("Wrong unit index %d (unit is not in the slot)", index);
    }
  }
  else
  {
    RptF("Unit index %d out of range", index);
  }
  unit->GetSubgroup()->UnitRemoved(unit);

  if (UnitsCount() == 0)
  {
//		GetCenter()->GroupRemoved(this);
    _leader = NULL;
    return;
  }
  if (bLeader)
    GetCenter()->SelectLeader(this);
}

void AIGroup::AddSubgroup(AISubgroup *subgrp)
{
  Assert(!subgrp->GetGroup());

  // add to group
  int index = _subgroups.Find(NULL);
  if (index >= 0) _subgroups[index] = subgrp;
  else _subgroups.Add(subgrp);
  subgrp->_group = this;
  //keep formation pattern and orientation
  if(MainSubgroup())
  {
    subgrp->_formation = MainSubgroup()->_formation;
    subgrp->_direction = MainSubgroup()->_direction;
    subgrp->_directionMove = MainSubgroup()->_directionMove;
  }
}

void AIGroup::CopyUnitSlot(int from, int to)
{
  Assert(from >= 0 && from < _unitSlots.Size());
  Assert(to >= 0 && to < _unitSlots.Size());
  _unitSlots[to].Copy(_unitSlots[from]);
}

void AIGroup::InitUnitSlot(int i)
{
  Assert(i >= 0 && i < _unitSlots.Size());
  _unitSlots[i].Init();
}

void AIGroup::RessurectUnit(AIUnit *unit)
{
  int i = unit->ID() - 1;
  Assert(i >= 0 && i < _unitSlots.Size());
  AIUnitSlot &slot = _unitSlots[i];
  slot._healthState = AIUnit::RSNormal;
  slot._dammageState = AIUnit::RSNormal;
  slot._reportedDown = false;
  slot._reportBeforeTime = TIME_MAX;
}

void AIGroup::AddUnit(AIUnit *unit, int id)
{
  if (!_mainSubgroup)
  {
    _mainSubgroup = new AISubgroup();
    AddSubgroup(_mainSubgroup);
  }
  if (unit->ID() != 0)
  {
    int i = unit->ID() - 1;
    Assert(i >= 0 && i < _unitSlots.Size());
    AIUnitSlot &slot = _unitSlots[i];
    if (slot._unit && slot._unit != unit)
    {
      Fail("Unit ID mismatch");
    }
    else
    {
      slot.Init();
      slot._unit = unit;
      _mainSubgroup->AddUnit(unit);
    }
    return;
  }

  if (id > 0)
  {
    int index = id - 1;
    _unitSlots.Access(index);
    AIUnitSlot &slot = _unitSlots[index];

    AIUnit *removed = slot._unit; // need not to AddRef, only link is destroyed
    slot._unit = unit;

    unit->_id = id;
    _mainSubgroup->AddUnit(unit);
    if (removed)
    {
      // find a new slot for this unit
      int newIndex = -1;
      for (int i=0; i<_unitSlots.Size(); i++)
      {
        if (!_unitSlots[i]._unit)
        {
          newIndex = i;
          break;
        }
      }
      if (newIndex < 0) newIndex = _unitSlots.Add();
      
      AIUnitSlot &newSlot = _unitSlots[newIndex];
      newSlot._unit = removed;
      removed->_id = newIndex + 1;
      // move old information together with the unit
      newSlot.Copy(slot);
      // init information for the new unit
      slot.Init();
    }
    else
    {
      slot.Init();
    }
  }
  else
  {
    // find a new slot for this unit
    int index = -1;
    for (int i=0; i<_unitSlots.Size(); i++)
    {
      if (!_unitSlots[i]._unit)
      {
        index = i;
        break;
      }
    }
    if (index < 0) index = _unitSlots.Add();

    AIUnitSlot &slot = _unitSlots[index];
    slot._unit = unit;
    unit->_id = index + 1;
    slot.Init();
    
    _mainSubgroup->AddUnit(unit);
  }
}

void AIGroup::AddVehicle(Transport *veh)
{
  // delete free positions
  _vehicles.RemoveNulls();
  // try to find
  if (_vehicles.Find(veh) >= 0)
    return;

  // assign
  veh->AssignGroup(this);

  // add to array sorted by cost	
  int i;
  for (i=0; i<_vehicles.Size(); i++)
  {
    Transport *trans = _vehicles[i];
    if (veh->GetType()->GetCost() >= trans->GetType()->GetCost())
    {
      _vehicles.Insert(i, veh);
      return;
    }
  }
  _vehicles.Add(veh);
}

AIUnit *AIGroup::LeaderCandidate(AIUnit *dead) const
{
  // implementation of leader selection for group
  AIUnit *unit = NULL;
  Rank bestRank = RankPrivate;
  float bestExp = -FLT_MAX;
  // find unit with the highest rank
  for (int i=0; i<_unitSlots.Size(); i++)
  {
    AIUnit *u = _unitSlots[i]._unit;
//		group leader can be in cargo now
//		if (u && !u->GetInCargo())
    if (u && u != dead && u->GetPerson())
    {
      if (unit && unit->GetLifeState() != LifeStateUnconscious &&  u->GetLifeState() == LifeStateUnconscious) continue;
      if (unit && !unit->GetPerson()->IsRenegade() && u->GetPerson()->IsRenegade()) continue;
      Rank rank = u->GetPerson()->GetRank();

      if(!unit || unit->GetLifeState() == LifeStateUnconscious)
      {
        bestRank = rank;
        bestExp = u->GetPerson()->GetExperience();
        unit = u;
      }
      else if (rank > bestRank)
      {
        bestRank = rank;
        bestExp = u->GetPerson()->GetExperience();
        unit = u;
      }
      else if (rank == bestRank)
      {
        float exp = u->GetPerson()->GetExperience();
        if (exp > bestExp)
        {
          bestExp = exp;
          unit = u;
        }
      }
    }
  }

  return unit;
}

void AIGroup::SelectLeader(AIUnit *unit)
{
  Assert(unit);
  Assert(unit->GetSubgroup())
  Assert(unit->GetSubgroup()->GetGroup() == this);

  if (_leader != unit)
  {
/*
    // Update change of leader in _playerRoles
    if ( _leader->GetRoleIndex() >= 0 ) GetNetworkManager().PlayerRoleUpdateLeader(_leader->GetRoleIndex(), false);
    if ( _unit->GetRoleIndex() >= 0 ) GetNetworkManager().PlayerRoleUpdateLeader(_unit->GetRoleIndex(), true);
*/
    _leader = unit;
    CalculateCourage();

    AISubgroup *subgrp = unit->GetSubgroup();
    if (subgrp)
    {
      subgrp->SelectLeader();

      // prevent player inheriting commands given to AI units
      if (unit->IsAnyPlayer())
      {
        subgrp->ClearAllCommands();
        if (subgrp != MainSubgroup() && MainSubgroup())
        {
          MainSubgroup()->ClearAllCommands();
        }
        // TODO: clear radio protocol
      }
    }
    else
    {
      if (MainSubgroup()) MainSubgroup()->ClearAllCommands();
    }
  }
}

static int CompUnits(const AIUnitSlot *slot1, const AIUnitSlot *slot2)
{
  // check existence
  AIUnit *u1 = slot1->_unit;
  if (!u1) return 1;
  AIUnit *u2 = slot2->_unit;
  if (!u2) return -1;

  // check rank
  int rank1 = u1->GetPerson()->GetRank();
  int rank2 = u2->GetPerson()->GetRank();
  int diff = rank2 - rank1;
  if (diff != 0) return diff;
  
  // check function
  int pos1 = 1;
  int pos2 = 1;
  Transport *v1 = u1->GetVehicleIn(); 
  Transport *v2 = u2->GetVehicleIn();
  if (v1)
  {
    if (v1->ObserverBrain() == u1) pos1 = 2;
    else if (v1->GunnerBrain() == u1) pos1 = 0;
  }
  if (v2)
  {
    if (v2->ObserverBrain() == u2) pos2 = 2;
    else if (v2->GunnerBrain() == u2) pos2 = 0;
  }
  diff = pos2 - pos1;
  if (diff != 0) return diff;

  diff = u2->IsGroupLeader()-u1->IsGroupLeader();
  if (diff) return diff;
  // other cases
  return u1->ID() - u2->ID();
}

void AIGroup::SortUnits()
{
  QSort(_unitSlots.Data(), _unitSlots.Size(), CompUnits);

  for (int i=0; i<_unitSlots.Size(); i++)
  {
    AIUnit *unit = _unitSlots[i]._unit;
    if (unit) unit->_id = i + 1;
  }
}

bool AIGroup::IsPlayerGroup() const
{
  return Leader() && Leader()->IsPlayer();
}

bool AIGroup::IsAnyPlayerGroup() const
{
  // note: currently group is always local where leader is local
  // therefore IsAnyPlayerGroup called on local group
  // should always be identical to IsPlayerGroup
  return Leader() && Leader()->IsAnyPlayer();
}

bool AIGroup::ContainsAnyPlayer() const
{
  for (int u=0; u<_unitSlots.Size(); u++)
  {
    AIUnit *unit = _unitSlots[u]._unit;
    if (unit && unit->IsAnyPlayer()) return true;
  }
  return false;
}

bool AIGroup::ContainsRemotePlayer() const
{
  for (int u=0; u<_unitSlots.Size(); u++)
  {
    AIUnit *unit = _unitSlots[u]._unit;
    if (unit && unit->IsRemotePlayer()) return true;
  }
  return false;
}

SimulationImportance AIGroup::GetImportance() const
{
  SimulationImportance minImportance = SimulateDefault;
  for (int u=0; u<_unitSlots.Size(); u++)
  {
    AIUnit *unit = _unitSlots[u]._unit;
    if (!unit) continue;
    EntityAIFull *vehicle = unit->GetVehicle();
    if (!vehicle) continue;
    SimulationImportance importance = vehicle->GetLastImportance();
    if (importance < minImportance) minImportance = importance;
  }
  return minImportance;
}

float AIGroup::GetThinkError() const
{
  float age = Glob.time - _lastUpdateTime;
  float error = importanceCoefs[GetImportance()] * age * (1.0f / maxThinkError);
  return floatMin(error, 1.0f);
}

bool AIGroup::IsCameraGroup() const
{
  for (int u=0; u<_unitSlots.Size(); u++)
  {
    AIUnit *unit = _unitSlots[u]._unit;
    if (unit && unit->GetVehicle() == GWorld->CameraOn()) return true;
  }
  return false;
}

inline bool ScanNeeded(AIUnit *unit, AIGroup *grp, EntityAI *vehicle)
{
  return vehicle->GetGroup() != grp && vehicle->GunnerUnit() == unit;
}

/*!
\patch 5161 Date 5/30/2007 by Jirka
- Fixed: Group leader left by team switch no longer orders some commands
*/

// Mind
bool AIGroup::Think(float timeAvail)
{
#if 0
  EntityAI *cameraVehicle = dyn_cast<EntityAI>(GWorld->CameraOn());
  if (cameraVehicle)
  {
    AIBrain *unit = cameraVehicle->CommanderUnit();
    if (unit && unit->GetGroup() == this)
    {
      LogF("%.2f: Group %s simulated - available time %.1f ms", Glob.time.toFloat(), cc_cast(GetDebugName()), timeAvail * 1000);
    }
  }
#endif

  PROFILE_SCOPE_EX(aiGrp, ai);
  ADD_COUNTER_EX(aiGrp, ai, 1);

  _lastUpdateTime = Glob.time;

// LogF("* %.2f: AIGroup(%s)::Think available time %.4f", Glob.time.toFloat(), cc_cast(GetDebugName()), timeAvail);

  SectionTimeHandle sectionTime = StartSectionTime();

#if LOG_THINK
  Log("Group %s think.", (const char *)GetDebugName());
#endif

  AIUnit *leader = Leader();
  if (!leader)
  {
    DoAssert(UnitsCount() == 0);
    return false;
  }

  if (GetCenter()->GetSide() == TLogic)
  {
    if (IsLocal()) DoUpdate();
    return false;
  }

  if (!IsLocal())
  {
    // update target list
    if (Glob.time > _expensiveThinkTime)
    {
      ExpensiveThinkDone();
      CreateTargetList();
    }
    // update Subgroups
    for (int i=0; i<NSubgroups(); i++)
    {
      // do not destroy subgroup during Think
      Ref <AISubgroup> subgrp = GetSubgroup(i);
      ThinkImportance prec = subgrp->CalculateImportance();
      subgrp->Think(prec);
    }
    return false;
  }

  // Check if the leader is not renegade, if so, select other one
  if (leader->GetPerson() && leader->GetPerson()->IsRenegade())
  {
    AIUnit *newLeader = LeaderCandidate(NULL);
    // FIX: newLeader can be NULL!
    if (newLeader && newLeader != leader && newLeader->GetPerson() && !newLeader->GetPerson()->IsRenegade() && newLeader->GetLifeState() != LifeStateUnconscious)
    {
      leader = newLeader;
      SelectLeader(leader);
      leader->SendAnswer(AI::IsLeader);
    }
  }

  // Check if the leader is not unconscious, if so, select other one
  if (leader->GetPerson() && leader->GetLifeState() == LifeStateUnconscious)
  {
    AIUnit *newLeader = LeaderCandidate(NULL);
    if (newLeader && newLeader != leader && newLeader->GetPerson() && !newLeader->GetPerson()->IsRenegade() && newLeader->GetLifeState() != LifeStateUnconscious)
    {
      if(!_unconsciousLeader || 
        !_unconsciousLeader->GetPerson() ||
        _unconsciousLeader->GetLifeState() == LifeStateDead
        ) 
      {
        SetUnconsciousLeader(leader);
        if (GWorld->GetMode() == GModeNetware)
          GetNetworkManager().GroupSetUnconsciousLeader(leader, this);
      }

      leader = newLeader;
      SelectLeader(leader);
      leader->SendAnswer(AI::IsLeader);
    }
  }
  else
  {
    if(UnconsciousLeader() && UnconsciousLeader()->GetLifeState()==LifeStateAlive)
    {
      leader = UnconsciousLeader();
      SelectLeader(leader);
      leader->SendAnswer(AI::IsLeader);
      SetUnconsciousLeader(NULL);
      if (GWorld->GetMode() == GModeNetware)
        GetNetworkManager().GroupSetUnconsciousLeader(NULL, this);
    }
  }

  bool leaderAlive = Leader()->LSIsAlive();

  if (IsAnyPlayerGroup())
  {
    if (_flee) Unflee();
  }
  else
  {
    // flee only when leader is alive
    if (leaderAlive)
    {
      float strength = ActualStrength();
      if (strength <= (1.0 - _courage) * _maxStrength)
      {
        if (!_flee)
        {
          Flee();
        }
      }
      /*
      Unflee will be done once flee group FSM is completed
      else
      {
        if (_flee)
        {
          Unflee();
        }
      }
      */
    }
  }

// check if state in state graph not changed
  {
    Ref<AIGroup> safePtr = this;
    DoUpdate();
    Assert(safePtr->RefCounter() > 0);
    if (safePtr->RefCounter() == 1) return false; // group destroyed

    if (!Leader())
    {
      DoAssert(UnitsCount() == 0);
      return false;
    }
  }

// TODO: find better placement
  AISubgroup *leaderSubgroup = Leader()->GetSubgroup();
  if (leaderSubgroup != MainSubgroup() && !leaderSubgroup->HasCommand())
  {
    leaderSubgroup->JoinToSubgroup(MainSubgroup());
  }

  // when AI disabled because of team switch, do not join subgroups
  if (!Leader()->IsAnyPlayer() && (Leader()->GetAIDisabled() & AIBrain::DATeamSwitch) == 0)
  {
    // do not join while scripted waypoint
    if (!GetScript())
    {
      // if there are subgroups in wait state, join them
      for( int i=0; i<_subgroups.Size(); i++ )
      {
        AISubgroup *sub = _subgroups[i];
        if( !sub ) continue;
        if( sub==MainSubgroup() ) continue;
        if( sub->HasCommand() ) continue;
        sub->JoinToSubgroup(MainSubgroup());
      }
    }
  }

  const float nearTargetLimit = 100.0f;
  // if some enemy is near or some target locked, we have to track it some targets continuously
  // force all units to track if necessary
  for (int i=0; i<_unitSlots.Size(); i++)
  {
    AIUnit *unit = _unitSlots[i]._unit;
    if (!unit) continue;
    if (!unit->IsLocal()) continue;
    
    EntityAIFull *veh = unit->GetVehicle();
    
    if (!veh || !unit->LSCanTrackTargets() || !veh->GetType()->ScanTargets()) continue;

    // cargo test needed - if unit is a unit, it still may be a cargo, as cargo may be a CommanderUnit
    if ((!unit->IsUnit() || unit->IsInCargo()) && !ScanNeeded(unit,this,veh))
    {
      AIBrain *pilot = veh->PilotUnit();
      // when vehicle is driven by nobody or another group, make sure even cargo units do know about it
      if (!pilot || pilot->GetGroup()!=unit->GetGroup())
      {
        TargetNormal *tgtVeh = _targetList.FindTarget(veh);
        if (tgtVeh)
        {
          const SideInfo *info = _targetList.GetSideInfo(unit);
          veh->TrackMyVehicle(tgtVeh,unit,5.0f,*info);
        }
        
      }
      continue;
    }
    // if we are not fighting, there is no need for frequent updates
    if (unit->GetCombatMode()<=CMSafe) continue;
    if (!veh->HasSomeWeapons()) continue;
    if (unit->GetNearestEnemyDist2()<Square(nearTargetLimit))
    {
      veh->TrackNearTargets(_targetList,nearTargetLimit);
    }
    veh->TrackLockedTargets(_targetList);
  }

  if (leaderAlive)
  {
    // auto-detection only for non-player groups
    if (_combatModeMinor >= CMAware)
    {
      bool isPlayerGroup = IsAnyPlayerGroup();
      CombatMode oldCM = Leader()->GetCombatMode();
      
      float timeFromCombat = Glob.time-_lastEnemyDetected; // _lastEnemyDetected is set to random 5-30 seconds after seeing
      if (_disclosed>Glob.time-10*60)
      {
        saturateMin(timeFromCombat,Glob.time -(_disclosed + 15.0f));
      }
      if (timeFromCombat>0)
      {
        if (timeFromCombat>(isPlayerGroup ? 30 : 5*60)) // for players  group we want a lot faster recovery to safe
        {
          _combatModeMinor = CMSafe;
        }
        else if (_combatModeMinor>=CMCombat)
        {
          _combatModeMinor = CMAware;
          if (!isPlayerGroup)
          {
            SendClear(Leader());
          }
          else
          {
            // find some non-player unit which could report Clear
            // only report when danger was not ordered explicitly
            AIUnit *report = NULL;
            for (int i=0; i<NUnits(); i++)
            {
              AIUnit *unit = GetUnit(i);
              if (unit && unit->LSCanSpeak() && unit->GetCombatModeMajor()<CMCombat && !unit->IsAnyPlayer())
              {
                report = unit;
                break;
              }
            }
            if (report)
            {
              SendClear(report);
            }
          }
        }
      }
      
      CombatMode newCM = Leader()->GetCombatMode();
      if (oldCM!=newCM) Leader()->OnCombatModeChanged(oldCM,newCM);
    }
  }

  bool someOperPath=false;
  for (int i=0; i<NSubgroups(); i++)
  {
    // do not destroy subgroup during Think
    Ref <AISubgroup> subgrp = GetSubgroup(i);
    ThinkImportance prec = subgrp->CalculateImportance();
    if (subgrp->Think(prec))
      someOperPath = true;
  }

  float restTime = timeAvail - GetSectionTime(sectionTime);
  float updateNeeded = Glob.time - _expensiveThinkTime;
  
  const float coefTime = 200.0f;
  const float coefNeeded = 2.0f;
  const float threshold = 1.0f;
  float processUpdate = coefTime * restTime + coefNeeded * updateNeeded;

  // visible enemy targets
  // if we have fresh assignment, do not assign anything
  // if (Glob.time>_expensiveThinkTime)
  if (processUpdate > threshold && updateNeeded>0)
  {
    // many functions need not be performed in every think
    // once per second is enough for them
    //PROFILE_SCOPE_EX(aiGEx, ai);

    ExpensiveThinkDone();

    int oldED = _enemiesDetected;
    CreateTargetList();

    if (oldED == 0 && _enemiesDetected > 0 && !IsAnyPlayerGroup())
    { 
      const TargetList &list = GetTargetList();
      Target *enemy1st = NULL;
      for (int i=0; i<list.EnemyCount(); i++)
      {
        Target *tgt = list.GetEnemy(i);
        if (tgt->IsVanishedOrDestroyed()) continue;
        if (!tgt->IsKnownBySome()) continue;
       // if (tgt->idExact &&  tgt->idExact->IsArtilleryTarget()) continue;
        enemy1st = tgt;
        break;

      }
      ReactToEnemyDetected(enemy1st);
    }

    if (leaderAlive)
    {
      //if (enemies)
      // min delay betweeen targets
      float minDelay = Leader()->GetInvAbility(AKCommanding)*5;
      if (Glob.time>_lastSendTargetTime+minDelay)
      {
        PROFILE_SCOPE_EX(aiGAT, ai);
        AssignTargets();
      }
    }

    if (_enemiesDetected > 0)
    {
      _lastEnemyDetected = Glob.time + GRandGen.PlusMinus(30.0f, 5.0f);
    }

    // try to assign unassigned vehicles
    if (!IsAnyPlayerGroup() && leaderAlive)
    {
      AssignVehicles();
      GetInVehicles();


      // check resources
      bool ok = true;
      if (!CheckFuel()) ok = false;
      if (!CheckArmor()) ok = false;
      if (!CheckHealth()) ok = false;
      if (!CheckAmmo()) ok = false;

      if (ok)
      {
        Assert(_center);
        if (_center->GetCommandRadio().Done() && _center->IsSupported(this, ATNone))
        {
          _center->GetCommandRadio().Transmit(new RadioMessageSupportDone(this));
        }
      }
      if (!GetFlee())
      {
        if (CheckRetreat())
        {
          Flee();
        }
      }
    }

    // check if support needed
    CheckSupport();
    CheckAlive(); // check if unseen units are alive

//LogF("  Group %d extensive think: available %.4f, needed %.2f, spent %.4f", ID(), restTime, updateNeeded, GetSectionTime(sectionTime) - (timeAvail - restTime));
  }

  if (Glob.time>=_enemyRecomputeCostTime)
  {
    _enemyRecomputeCostTime = TIME_MAX;
    // it is enough to recompute the slot with the target
    _targetList.RecomputeTargetListSubjectiveCost(TgtCatEnemy,Leader(),this);
    //LogF("+++ %s recomputed at  %.2f",cc_cast(GetDebugName()),Glob.time.toFloat());
  }

  _targetList.ProcessAggrTargets(this);

#if LOG_THINK
  Log("End of group %s think (%d).", (const char *)GetDebugName(), someOperPath);
#endif

//LogF("  Group %d update %.4f", ID(), GetSectionTime(sectionTime));
 
  return someOperPath;
}

struct VehInGroup
{
  EntityAIFull *_veh;
  float   _dist2; //distance to leader
  
  VehInGroup() {}
  VehInGroup(EntityAIFull *veh, float dist2) : _veh(veh), _dist2(dist2) {}
};

TypeIsMovableZeroed(VehInGroup);

struct CompareDist2Leader
{
  int operator () (const VehInGroup *u1, const VehInGroup *u2) const
  {
    return sign(u1->_dist2-u2->_dist2);
  }
};

void AIGroup::UpdateMartaType()
{
  float bestImportance = -1e10;
  int bestIx = -1;
  for (int i=0; i<NUnits(); i++)
  {
    AIUnit *unit = _unitSlots[i]._unit;
    if (!unit || !unit->IsUnit()) continue; //each vehicle only once
    EntityAIFull *veh = unit->GetVehicle();
    if (!veh) continue;
    int ix = GWorld->GetMartaType(veh->GetType());
    if (ix<0) continue;
    float importance = GWorld->_martaType[ix]._importance;
    if (importance > bestImportance)
    {
      bestImportance = importance;
      bestIx = ix;
    }
  }
  _martaTypeIx = bestIx;
}

void AIGroup::UpdateCompactness(bool lazy)
{
#if DIAG_AGGREGATED_TARGETS
  static char DiagGroupDebugName[] = "B 1-1-A";
  if (stricmp(GetDebugName(),DiagGroupDebugName)==0)
  {
    _targetList.DebugAggrTargets(true);
  }
#endif
  AIUnit *leader = _leader;
  if ( (!lazy || !_coreUpdated) && leader ) //FIX: leader was sometimes NULL, causing crash (leader->Position()) 
  {
    _coreUpdated = true;
    // sort units according to the distance from leader
    AutoArray<VehInGroup, MemAllocLocal<VehInGroup, 32> > vehs;
    for (int i=0; i<NUnits(); i++)
    {
      AIUnit *unit = _unitSlots[i]._unit;
      if (!unit || !unit->IsUnit()) continue; //each vehicle only once
      EntityAIFull *veh = unit->GetVehicle();
      if (veh) vehs.Add( VehInGroup(veh, veh->FutureVisualState().Position().DistanceXZ2(leader->Position(leader->GetFutureVisualState())) ) );
    }
    ::QSort(vehs, CompareDist2Leader());

    // use some of the closest units to define the Group Core Position and Radius
    // corePos first
    _corePosition = VZero;
    float costSum = 0;
    int coreCount = 0.7f*vehs.Size();
    if (coreCount < vehs.Size()) coreCount++; //implies nonzero
    for (int i=0; i<coreCount; i++)
    {
      float cost = GWorld->GetMartaImportance(vehs[i]._veh->GetType());
      costSum += cost;
      _corePosition += vehs[i]._veh->FutureVisualState().Position() * cost;
    }
    _corePosition = costSum>FLT_MIN ? _corePosition/costSum : _corePosition;
    // coreRad second
    float coreRad2 = 0;
    for (int i=0; i<coreCount; i++)
    {
      float dist2 = vehs[i]._veh->FutureVisualState().Position().DistanceXZ2(_corePosition);
      saturateMax(coreRad2, dist2);
    }
    _coreRadius = sqrt(coreRad2);

    // determine the level of the groupCompactness
    // TODO: differs on the group type (MARTA)
    _groupCompactness = Interpolativ(_coreRadius, 50, 150, 1, 0);

#if DIAG_AGGREGATED_TARGETS && 0
    LogF("*** AIGroup::UpdateCompactness: %s radius=%.1f, comp=%d%%", cc_cast(GetDebugName()), _coreRadius, toInt(_groupCompactness*100));
    int limitIx = 0.7f*vehs.Size();
    for (int i=0; i<vehs.Size(); i++)
    {
      LogF("  #%d: %.1f%s", i, vehs[i]._dist2, i==limitIx ? " <- last item in the 70% list" : "");
    }
#endif
  }
}

// Communication with subgroups

void AIGroup::SendCommand(Command &cmd, bool channelCenter)
{
  cmd._id = _nextCmdId++;

  Assert(Leader());
#if LOG_COMM
  Log("Send command: Main subgroup of %s: Command %d (context %d)",
  (const char *)GetDebugName(), cmd._message, cmd._context);
#endif
  
  AIUnit *leader = Leader();
  if (!leader) return;
  
  // if leader is not alive, he should not be able to send commands
  if (leader && !leader->LSCanCommand())
  {
    LogF("No command, leader %s is not alive",(const char *)leader->GetDebugName());
    return;
  }

  // do not command yourself only
  if (UnitsCount() == 1)
  {
    MainSubgroup()->ReceiveCommand(cmd);
    return;
  }
 
  bool transmit = true;
  if (leader->GetSubgroup() == MainSubgroup())
  {
    MainSubgroup()->ReceiveCommand(cmd);
    transmit = false;
  }

  // do not say if its near
  if (!transmit)
  {
    AIUnit *subLeader = MainSubgroup()->Leader();
    if (subLeader && subLeader->Position(subLeader->GetFutureVisualState()).Distance2(cmd._destination) < Square(6)) return;
  }
  
  // active channel selection
  RadioChannel *activeChannel;
  if (channelCenter)
  {
    Assert(GetCenter());
    activeChannel = &GetCenter()->GetSideRadio();
  }
  else
  {
    activeChannel = &GetRadio();
#if _ENABLE_DIRECT_MESSAGES
    Person *person = leader->GetPerson();
    if (person->GetRadio())
    {
      bool useRadio = false;
      AISubgroup *subgrp = MainSubgroup();
      for (int i=0; i<subgrp->NUnits(); i++)
      {
        AIUnit *u = subgrp->GetUnit(i);
        if (u && leader->UseRadio(u))
        {
          useRadio = true;
          break;
        }
      }
      if (!useRadio) activeChannel = person->GetRadio();
    }
#endif
  }
  Assert(activeChannel);

  // list is empty because units are identified by the flag toMainSubgroup
  OLinkPermNOArray(AIUnit) list;
  activeChannel->Transmit(new RadioMessageCommand(this, list, cmd, true, transmit));
}

class DeleteGetXXXMessage
{
protected:
  AIGroup *_group;
  AIUnit *_unit;

public:
  DeleteGetXXXMessage(AIGroup *group, AIUnit *unit)
  {
    _group = group; _unit = unit;
  }
  bool operator ()(RadioChannel *channel, RadioMessage *msg, bool actual) const;
};

bool DeleteGetXXXMessage::operator ()(RadioChannel *channel, RadioMessage *msg, bool actual) const
{
  if (msg->GetType() != RMTCommand) return false;

  Assert(dynamic_cast<RadioMessageCommand *>(msg));
  RadioMessageCommand *msgCmd = static_cast<RadioMessageCommand *>(msg);
  Assert(msgCmd->GetFrom() == _group);
  if (msgCmd->GetMessage() == Command::GetIn && msgCmd->IsTo(_unit))
  {
    msgCmd->DeleteTo(_unit);
    return true;
  }
  if (msgCmd->GetMessage() == Command::GetOut && msgCmd->IsTo(_unit))
  {
    msgCmd->DeleteTo(_unit);
    return true;
  }
  return false;
}

void AIGroup::SendCommand(Command &cmd, const OLinkPermNOArray(AIUnit) &listPar, bool channelCenter)
{
  cmd._id = _nextCmdId++;

  Assert(Leader());

#if LOG_COMM
  char buffer[256];
  CreateUnitsList(list, buffer,sizeof(buffer));
  Log("Send command: units %s of %s: Command %d (context %d)",
    buffer, (const char *)GetDebugName(), cmd._message, cmd._context);
#endif

  AIUnit *leader = Leader();
  // if leader is not alive, he should not be able to send commands
  if (leader && !leader->LSCanCommand())
  {
    LogF("SendCommand: No command, leader %s is not alive",(const char *)leader->GetDebugName());
    return;
  }

  // active channel selection
  RadioChannel *radioChannelGroup = &GetRadio();
  Assert(GetCenter());
  RadioChannel *radioChannelCenter = &GetCenter()->GetSideRadio();
#if _ENABLE_DIRECT_MESSAGES
  Person *person = leader->GetPerson();
  RadioChannel *directChannel = person->GetRadio();
#endif
  RadioChannel *activeChannel;
  if (channelCenter)
  {
    activeChannel = radioChannelCenter;
  }
  else
  {
    activeChannel = radioChannelGroup;
#if _ENABLE_DIRECT_MESSAGES
    if (!leader->UseRadio(this, listPar)) activeChannel = directChannel;
#endif
  }
  Assert(activeChannel);

  /// Fix: change the list only locally
  OLinkPermNOArray(AIUnit) list = listPar;

  for (int i=0; i<list.Size();)
  {
    AIUnit *unit = list[i];
    if (!unit)
    {
      list.Delete(i);
      continue;
    }

    if (cmd._message == Command::GetIn || cmd._message == Command::GetOut)
    {
      // check radio channel
      DeleteGetXXXMessage func(this, unit);
      radioChannelGroup->ForEachMessage(func);
      radioChannelCenter->ForEachMessage(func);
#if _ENABLE_DIRECT_MESSAGES
      directChannel->ForEachMessage(func);
#endif
    }

    i++;
  }

  if(cmd._action)
  {
    //assing medic to unit
    if(cmd._action->GetType() == ATHealSoldier)
    {
      if(cmd._target && cmd._target->CommanderUnit() && list.Size()>0)
      {
        AIUnit *unit = cmd._target->CommanderUnit()->GetUnit();
        if(unit && !cmd._target->AssignedAttendant())
        {
          for (int i=0; i<list.Size(); i++)
          {
            AIUnit *medic = list[i]; 
            if(medic && medic->IsFreeSoldier() &&  medic->GetVehicle() &&  medic->GetVehicle()->GetType()->IsAttendant())
            {
              //cmd._message = Command::HealSoldier;
              cmd._target->SetAssignedAttendant(medic->GetVehicle());
              break;
            }
          }
        }
      }
    }
    //assing attendat to unit (can be any soldier)
    if(cmd._action->GetType() == ATFirstAid)
    {
      if(cmd._target && cmd._target->CommanderUnit() && list.Size()>0)
      {
        AIUnit *unit = cmd._target->CommanderUnit()->GetUnit();
        if(unit && !cmd._target->AssignedAttendant())
        {
          for (int i=0; i<list.Size(); i++)
          {
            AIUnit *medic = list[i]; 
            if(medic && medic->IsFreeSoldier() && medic->GetVehicle())
            {
              // cmd._message = Command::FirstAid;
              cmd._target->SetAssignedAttendant(medic->GetVehicle());
              break;
            }
          }
        }
      }
    }
    //assing attendat to unit (can be any soldier)
    if(cmd._action->GetType() == ATRepairVehicle)
    {
      if(cmd._target && list.Size()>0)
      {
        if(!cmd._target->AssignedAttendant())
        {
          for (int i=0; i<list.Size(); i++)
          {
            AIUnit *engineer = list[i]; 
            if(engineer && engineer->IsFreeSoldier() && engineer->GetVehicle())
            {
              // cmd._message = Command::FirstAid;
              cmd._target->SetAssignedAttendant(engineer->GetVehicle());
              break;
            }
          }
        }
      }
    }
  }

  if (list.FindKey(leader) >= 0)
  {
    // try to process command directly
    if (cmd._message == Command::GetIn || cmd._message == Command::GetOut)
    {
      list.DeleteKey(leader);
      OLinkPermNOArray(AIUnit) leaderList;
      leaderList.Realloc(1);
      leaderList.Resize(1);
      leaderList[0] = leader;
      IssueCommand(cmd, leaderList);
    }
    else
    {
      IssueCommand(cmd, list);
      return;
    }
  }
  
  if (list.Size() > 0)
    activeChannel->Transmit(new RadioMessageCommand(this, list, cmd));
}

/*!
\patch 5106 Date 12/20/2006 by Jirka
- Fixed: Mount command (from the commanding menu) now works even for units already in the target vehicle (but on the different position)
*/

void AIGroup::IssueCommand(Command &cmd, const OLinkPermNOArray(AIUnit) &listPar)
{
  if (listPar.Size() == 0) return;

  AIUnit *leader = Leader();
  // if leader is not alive, he should not be able to send commands
  if (leader && !leader->LSCanCommand())
  {
    // auto commands may be issued even without leader alive
    if (cmd._context != Command::CtxAuto && cmd._context != Command::CtxAutoSilent)
    {
      LogF("IssueCommand: No command, leader %s is not alive",(const char *)leader->GetDebugName());
      return;
    }
  }

  /// Fix: change the list only locally
  OLinkPermNOArray(AIUnit) list = listPar;

  // TODO: direct Command::Join processing

  if (cmd._message == Command::GetOut || cmd._message == Command::Action)
  {
    bool fixTarget = cmd._message == Command::Action && !cmd._target;

    // Get Out or Action is processed by units
    int i;
    for (i=0; i<list.Size(); i++)
    {
      AIUnit *unit = list[i];
      if (!unit) continue;
      
      if (cmd._message == Command::GetOut || cmd._action && cmd._action->GetType() == ATEject)
      {
        // GetOut and Eject have sense only for units in vehicles
        if (unit->IsFreeSoldier())
          continue;
      }

      AISubgroup *subgrp = unit->GetSubgroup();
      Assert(subgrp);
      if (!subgrp) continue;
      if
      (
        subgrp != MainSubgroup() &&
        subgrp->NUnits() == 1
      )
      {
        cmd._joinToSubgroup = NULL;
        if (fixTarget)
        {
          // patch action target
          EntityAI *veh = unit->GetVehicle();
          cmd._target = veh;
          cmd._destination = veh->FutureVisualState().Position();
        }
        subgrp->ReceiveCommand(cmd);
      }
      else
      {
        if (cmd._context == Command::CtxAuto || cmd._context == Command::CtxAutoSilent)
          cmd._joinToSubgroup = subgrp;
        subgrp = new AISubgroup();
        AddSubgroup(subgrp);
        subgrp->AddUnit(unit);
        subgrp->SelectLeader(unit);
        if (GWorld->GetMode() == GModeNetware)
          GetNetworkManager().CreateObject(subgrp);
        if (fixTarget)
        {
          // patch action target
          EntityAI *veh = unit->GetVehicle();
          cmd._target = veh;
          cmd._destination = veh->FutureVisualState().Position();
        }
        subgrp->ReceiveCommand(cmd);
      }
    }
  }
  else if
  (
    cmd._message == Command::GetIn ||
    cmd._message == Command::Heal ||
    cmd._message == Command::HealSoldier ||
    cmd._message == Command::FirstAid ||
    cmd._message == Command::RepairVehicle ||
    cmd._message == Command::Repair ||
    cmd._message == Command::Refuel ||
    cmd._message == Command::Rearm ||
    cmd._message == Command::Support ||
    cmd._message == Command::Stop ||
    cmd._message == Command::Expect ||
    cmd._message == Command::Join ||
    cmd._message == Command::TakeBag ||
    cmd._message == Command::Assemble ||
    cmd._message == Command::DisAssemble ||
    cmd._message == Command::FireAtPosition
  )
  {
    // Get In and supply commands processed by vehicles
    int i;
    for (i=0; i<list.Size(); i++)
    {
      AIUnit *unit = list[i];
      if (!unit) continue;

      Transport *veh = unit->GetVehicleIn();
      Person *person = unit->GetPerson();

      if (cmd._message == Command::GetIn && veh && veh == cmd._target)
      {
        // unit already in vehicle - check its position
        int currentCompartments = 0;
        if (person == veh->Driver())
        {
          if (unit == veh->GetDriverAssigned()) continue; // already in place
          currentCompartments = veh->GetDriverCompartments();
        }
        else
        {
          TurretContext context;
          if (veh->FindTurret(person, context) && context._turret)
          {
            if (unit == context._turret->GetGunnerAssigned()) continue; // already in place
            currentCompartments = context._turretType->_gunnerCompartments;
          }
          else
          {
            int n = veh->GetManCargo().Size();
            int index = -1;
            for (int i=0; i<n; i++)
            {
              if (person == veh->GetManCargo()[i])
              {
                index = i; break;
              }
            }
            if (index >= 0)
            {
              bool assigned = false;
              for (int i=0; i<n; i++)
              {
                if (unit == veh->GetCargoAssigned(i))
                {
                  assigned = true;
                  break;
                }
              }
              if (assigned) continue; // already in place
              currentCompartments = veh->GetCargoCompartments(index);
            }
          }
        }

        // unit in vehicle, but in wrong position
        // first try to switch position
        if (veh->CanChangePosition(unit))
        {
          if (unit == veh->GetDriverAssigned())
          {
            // assigned as driver
            if (veh->CanMoveToDriver(unit, currentCompartments))
            {
              UIAction action(new ActionBasic(ATMoveToDriver, veh));
              action.Process(unit);
              continue;
            }
          }
          else
          {
            TurretContextV context;
            if (veh->FindAssignedTurret(unconst_cast(veh->FutureVisualState()), unit, context) && context._turret)
            {
              // assigned to turret
              if (veh->CanMoveToTurret(context, unit, currentCompartments))
              {
                UIAction action(new ActionTurret(ATMoveToTurret, veh, context._turret));
                action.Process(unit);
                continue;
              }
            }
            else
            {
              int n = veh->GetManCargo().Size();
              bool assigned = false;
              for (int i=0; i<n; i++)
              {
                if (unit == veh->GetCargoAssigned(i))
                {
                  assigned = true;
                  break;
                }
              }

              if (assigned)
              {
                // assigned to cargo
                // try to find some we can switch position to

                int index = -1;
                // search for empty position
                for (int i=0; i<veh->GetManCargo().Size(); i++)
                {
                  Person *man = veh->GetManCargo()[i];
                  if (!man && (veh->GetCargoCompartments(i) & currentCompartments) != 0)
                  {
                    index = i;
                    break;
                  }
                }
                // if not found, search for position occupied by a AI
                if (index < 0)
                {
                  for (int i=0; i<veh->GetManCargo().Size(); i++)
                  {
                    Person *man = veh->GetManCargo()[i];
                    if (!man) continue; // checked in the previous cycle
                    if (
                      (!man->Brain() || !man->Brain()->IsAnyPlayer()) &&
                      (veh->GetCargoCompartments(i) & currentCompartments) != 0) 
                    {
                      index = i;
                      break;
                    }
                  }
                }

                if (index >= 0)
                {
                  UIAction action(new ActionIndex(ATMoveToCargo, veh, index));
                  action.Process(unit);
                  continue;
                }
              }
            }
          }
        }
      }

      AIUnit *commander = NULL;
      if (veh) commander = veh->CommanderUnit() ? veh->CommanderUnit()->GetUnit() : NULL;

      if (veh == cmd._target)
      {
        // get out, then get in other position
        unit->IssueGetOut();
        veh = NULL;
      }
      else if (unit->IsInCargo())
      {
        if (!commander || commander->GetGroup() != this || list.FindKey(commander) < 0)
          unit->IssueGetOut();
      }
      else if (commander && commander != unit && commander->GetGroup() == this)
      {
        list.AddUnique(commander);
        continue;
      }

      AISubgroup *subgrp = unit->GetSubgroup();
      Assert(subgrp);
      if (subgrp != MainSubgroup())
      {
        bool allInside = true;
        if (veh)
        {
          for (int i=0; i<subgrp->NUnits(); i++)
          {
            AIUnit *u = subgrp->GetUnit(i);
            if (u && u->GetVehicleIn() != veh)
            {
              allInside = false;
              break;
            }
          }
        }
        else
        {
          allInside = subgrp->NUnits() == 1;
        }
        if (allInside)
        {
          subgrp->ReceiveCommand(cmd);
          continue;
        }
      }
      if (cmd._context == Command::CtxAuto || cmd._context == Command::CtxAutoSilent)
        cmd._joinToSubgroup = subgrp;
      subgrp = new AISubgroup();
      AddSubgroup(subgrp);
      if (veh)
      {
        if (veh->ObserverBrain() && veh->ObserverBrain()->GetUnit() && veh->ObserverBrain()->GetGroup() == this)
        {
          unit = veh->ObserverBrain()->GetUnit();
          subgrp->AddUnit(unit);
        }
        if (veh->DriverBrain() && veh->DriverBrain()->GetUnit() && veh->DriverBrain()->GetGroup() == this)
        {
          unit = veh->DriverBrain()->GetUnit();
          subgrp->AddUnit(unit);
        }
        if (veh->GunnerBrain() && veh->GunnerBrain()->GetUnit() && veh->GunnerBrain()->GetGroup() == this)
          subgrp->AddUnit(veh->GunnerBrain()->GetUnit());
        subgrp->SelectLeader(unit);
      }
      else
      {
        subgrp->AddUnit(unit);
        subgrp->SelectLeader(unit);
      }
      if (GWorld->GetMode() == GModeNetware)
        GetNetworkManager().CreateObject(subgrp);
      subgrp->ReceiveCommand(cmd);
    }
  }
  else 
  {
    // create subgroup
    AISubgroup *subgrp = NULL;
    {
      for (int i=0; i<NSubgroups(); i++)
      {
        AISubgroup *s = GetSubgroup(i);
        if (!s || s == MainSubgroup()) continue;
        OLinkPermNOArray(AIUnit) cargoList;
        s->GetUnitsListNoCargo(cargoList);
        bool contains = true;
        for (int j=0; j<cargoList.Size(); j++)
        {
          AIUnit *unit = cargoList[j];
          if (list.FindKey(unit) < 0)
          {
            contains = false;
            break;
          }
        }
        if (contains)
        {
          subgrp = s;
          break;
        }
      }
      if (!subgrp)
      {
        subgrp = new AISubgroup();
        AddSubgroup(subgrp);
        if (GWorld->GetMode() == GModeNetware)
          GetNetworkManager().CreateObject(subgrp);
      }
    }
    Assert(subgrp);
    Assert(subgrp->GetGroup());
    DoAssert(subgrp->GetGroup() == this);
    subgrp->AvoidRefresh(); // avoid refresh during subgrp creation
    // add units from other subgroups
    for (int i=0; i<list.Size(); i++)
    {
      AIUnit *unit = list[i];
      if (!unit) continue;
      if (unit->GetSubgroup() == subgrp) continue;
      DoAssert(unit->GetGroup() == this);
      if (!unit->IsUnit())
      {
        AIBrain *agent = unit->GetVehicleIn()->CommanderUnit();
        AIUnit *u = agent ? agent->GetUnit() : NULL;
        if (!u || u->GetGroup() != this || u->IsPlayer())
        {
          // unit in cargo with other group or no driver
          unit->IssueGetOut();
        }
        else if (list.FindKey(u) >= 0)
          continue;
        else
          unit = u;
      }
      #if _ENABLE_REPORT
      DoAssert(unit->GetGroup() == this);
      AISubgroup *oldSubgrp = unit->GetSubgroup();
      DoAssert(oldSubgrp != subgrp);
      #endif
      if (!unit->IsUnit())
      {
        subgrp->AddUnit(unit);
        subgrp->SelectLeader();
        DoAssert(subgrp->GetGroup() == this);
      }
      else
      {
        DoAssert(subgrp->GetGroup() == this);
        subgrp->AddUnitWithCargo(unit);
        DoAssert(subgrp->GetGroup() == this);
      }
/*	// TODO: BUG
      if (oldSubgrp && oldSubgrp->NUnits() <= 0 && oldSubgrp != MainSubgroup())
      {
        oldSubgrp->RemoveFromGroup();
      }
*/
    }
    subgrp->AvoidRefresh(false);

    // send command to subgroup	
    subgrp->ReceiveCommand(cmd);
  }
}

class UpdateFormationMessage
{
protected:
  AIGroup *_group;
  AISubgroup *_subgroup;
  AI::Formation _formation;
  bool &_done;

public:
  UpdateFormationMessage(AIGroup *group, AISubgroup *subgroup, AI::Formation formation, bool &done)
    : _done(done)
  {
    _group = group; _subgroup = subgroup; _formation = formation;
  }
  bool operator ()(RadioChannel *channel, RadioMessage *msg, bool actual) const;
};

bool UpdateFormationMessage::operator ()(RadioChannel *channel, RadioMessage *msg, bool actual) const
{
  if (msg->GetType() != RMTFormation) return false;

  Assert(dynamic_cast<RadioMessageFormation *>(msg));
  RadioMessageFormation *msgForm = static_cast<RadioMessageFormation *>(msg);
  Assert(msgForm);
  Assert(msgForm->GetFrom() == _group);

  if (msgForm->GetTo() == _subgroup)
  {
    if (msgForm->GetFormation() == _formation)
    {
      _done = true;
    }
    else if (!actual)
    {
      msgForm->SetFormation(_formation);
      _done = true;
    }
    return true;
  }
  return false;
}

void AIGroup::SendFormation(Formation f, AISubgroup *to)
{
  Assert(to);
  if (!to)
    return;

  Assert(Leader());
  if (!Leader()) return;

  // check radio channel
  bool done = false;
  UpdateFormationMessage func(this, to, f, done);
  bool found = GetRadio().ForEachMessage(func);
  if (done) return;

  if (!found)
  {
    // nothing is already processing
    if (to->GetFormation() == f) return;

    if (UnitsCount() == 1)
    {
      to->SetFormation(f);
      return;
    }
  }

  GetRadio().Transmit(new RadioMessageFormation(this, to, f));
}

void AIGroup::SendSemaphore(Semaphore sem, const OLinkPermNOArray(AIUnit) &list)
{
  // FIX: do not change the list, can be used further
  switch (sem)
  {
  case AI::SemaphoreBlue:
    SendLooseFormation(false, list);
    SendOpenFire(OFSNeverFire, list);
    break;
  case AI::SemaphoreGreen:
    SendLooseFormation(false, list);
    SendOpenFire(OFSHoldFire, list);
    break;
  case AI::SemaphoreWhite:
    SendLooseFormation(true, list);
    SendOpenFire(OFSHoldFire, list);
    break;
  case AI::SemaphoreYellow:
    SendLooseFormation(false, list);
    SendOpenFire(OFSOpenFire, list);
    break;
  case AI::SemaphoreRed:
    SendLooseFormation(true, list);
    SendOpenFire(OFSOpenFire, list);
    break;
  }
}

class UpdateBehaviourMessage
{
protected:
  RadioChannel *_activeChannel;
  AIGroup *_group;
  AIUnit *_unit;
  CombatMode _mode;
  bool &_done;
  bool &_clear;

public:
  UpdateBehaviourMessage(RadioChannel *activeChannel, AIGroup *group, AIUnit *unit, CombatMode mode, bool &done, bool &clear)
    : _done(done), _clear(clear)
  {
    _activeChannel = activeChannel; _group = group; _unit = unit; _mode = mode;
  }
  bool operator ()(RadioChannel *channel, RadioMessage *msg, bool actual) const;
};

bool UpdateBehaviourMessage::operator ()(RadioChannel *channel, RadioMessage *msg, bool actual) const
{
  if (msg->GetType() != RMTBehaviour) return false;

  Assert(dynamic_cast<RadioMessageBehaviour *>(msg));
  RadioMessageBehaviour *msgSem = static_cast<RadioMessageBehaviour *>(msg);
  Assert(msgSem);
  Assert(msgSem->GetFrom() == _group);

  if (msgSem->IsTo(_unit))
  {
    if (msgSem->GetBehaviour() == _mode)
    {
      _clear = true;
      _done = true;
      return true;
    }
    else if (!actual)
    {
      msgSem->DeleteTo(_unit);
      return true;
    }
    else
    {
      _done = true;
      return true;
    }
  }
  else if (!actual)
  {
    if (msgSem->GetBehaviour() == _mode && channel == _activeChannel)
    {
      msgSem->AddTo(_unit);
      _clear = true;
      _done = true;
      return true;
    }
  }
  return false;
}

void AIGroup::SendBehaviour(CombatMode mode, const OLinkPermNOArray(AIUnit) &listPar, bool direct)
{
  AIUnit *leader = Leader();

  // active channel selection
  RadioChannel *radioChannel = &GetRadio();
  RadioChannel *activeChannel = radioChannel;
#if _ENABLE_DIRECT_MESSAGES
  RadioChannel *directChannel = NULL;
  if (leader)
  {
    Person *person = leader->GetPerson();
    directChannel = person->GetRadio();
    if (!leader->UseRadio(this, listPar)) activeChannel = directChannel;
  }
#endif

  /// Fix: change the list only locally
  OLinkPermNOArray(AIUnit) list = listPar;

  for (int i=0; i<list.Size();)
  {
    AIUnit *unit = list[i];
    if (!unit)
    {
      list.Delete(i);
      continue;
    }

    if (!leader || unit == leader)
    {
      unit->SetCombatModeMajorReactToChange(mode);
      list.Delete(i);
      continue;
    }

    // check radio channel
    bool done = false; // skip check of current state
    bool clear = false; // unit processed
    UpdateBehaviourMessage func(activeChannel, this, unit, mode, done, clear);
    radioChannel->ForEachMessage(func);
    if (clear)
    {
      list.Delete(i);
      continue;
    }
    if (done)
    {
      i++;
      continue;
    }

#if _ENABLE_DIRECT_MESSAGES
    if (directChannel)
    {
      directChannel->ForEachMessage(func);
      if (clear)
      {
        list.Delete(i);
        continue;
      }
      if (done)
      {
        i++;
        continue;
      }
    }
#endif

    if (unit->GetCombatModeMajor() == mode)
      list.Delete(i);
    else
      i++;
  }

  if (leader && list.Size() > 0)
  {
    Assert(activeChannel);
    activeChannel->Transmit(new RadioMessageBehaviour(this, list, mode));
  }
}

class UpdateLooseFormationMessage
{
protected:
  RadioChannel *_activeChannel;
  AIGroup *_group;
  AIUnit *_unit;
  bool _loose;
  bool &_done;
  bool &_clear;

public:
  UpdateLooseFormationMessage(RadioChannel *activeChannel, AIGroup *group, AIUnit *unit, bool loose, bool &done, bool &clear)
    : _done(done), _clear(clear)
  {
    _activeChannel = activeChannel; _group = group; _unit = unit; _loose = loose;
  }
  bool operator ()(RadioChannel *channel, RadioMessage *msg, bool actual) const;
};

bool UpdateLooseFormationMessage::operator ()(RadioChannel *channel, RadioMessage *msg, bool actual) const
{
  if (msg->GetType() != RMTLooseFormation) return false;

  Assert(dynamic_cast<RadioMessageLooseFormation *>(msg));
  RadioMessageLooseFormation *msgSem = static_cast<RadioMessageLooseFormation *>(msg);
  Assert(msgSem);
  Assert(msgSem->GetFrom() == _group);

  if (msgSem->IsTo(_unit))
  {
    if (msgSem->IsLooseFormation() == _loose)
    {
      _clear = true;
      _done = true;
      return true;
    }
    else if (!actual)
    {
      msgSem->DeleteTo(_unit);
      return true;
    }
    else
    {
      _done = true;
      return true;
    }
  }
  else if (!actual)
  {
    if (msgSem->IsLooseFormation() == _loose && channel == _activeChannel)
    {
      msgSem->AddTo(_unit);
      _clear = true;
      _done = true;
      return true;
    }
  }
  return false;
}

void AIGroup::SendLooseFormation(bool loose, const OLinkPermNOArray(AIUnit) &listPar)
{
  AIUnit *leader = Leader();

  // active channel selection
  RadioChannel *radioChannel = &GetRadio();
  RadioChannel *activeChannel = radioChannel;
#if _ENABLE_DIRECT_MESSAGES
  RadioChannel *directChannel = NULL;
  if (leader)
  {
    Person *person = leader->GetPerson();
    directChannel = person->GetRadio();
    if (!leader->UseRadio(this, listPar)) activeChannel = directChannel;
  }
#endif

  /// Fix: change the list only locally
  OLinkPermNOArray(AIUnit) list = listPar;

  for (int i=0; i<list.Size();)
  {
    AIUnit *unit = list[i];
    if (!unit)
    {
      list.Delete(i);
      continue;
    }

    if (!leader || unit == leader)
    {
      AI::Semaphore s = unit->GetSemaphore();
      s = ApplyLooseFormation(s, loose);
      unit->SetSemaphore(s);
      list.Delete(i);
      continue;
    }

    // check radio channel
    bool done = false;
    bool clear = false;
    UpdateLooseFormationMessage func(activeChannel, this, unit, loose, done, clear);
    radioChannel->ForEachMessage(func);
    if (clear)
    {
      list.Delete(i);
      continue;
    }
    if (done)
    {
      i++;
      continue;
    }

#if _ENABLE_DIRECT_MESSAGES
    if (directChannel)
    {
      directChannel->ForEachMessage(func);
      if (clear)
      {
        list.Delete(i);
        continue;
      }
      if (done)
      {
        i++;
        continue;
      }
    }
#endif

    AI::Semaphore s = unit->GetSemaphore();
    if (ApplyLooseFormation(s, loose) == s)
      list.Delete(i);
    else
      i++;
  }

  if (leader && list.Size() > 0)
  {
    Assert(activeChannel);
    activeChannel->Transmit(new RadioMessageLooseFormation(this, list, loose));
  }
}

class UpdateOpenFireMessage
{
protected:
  RadioChannel *_activeChannel;
  AIGroup *_group;
  AIUnit *_unit;
  OpenFireState _open;
  bool &_done;
  bool &_clear;

public:
  UpdateOpenFireMessage(RadioChannel *activeChannel, AIGroup *group, AIUnit *unit, OpenFireState open, bool &done, bool &clear)
    : _done(done), _clear(clear)
  {
    _group = group; _unit = unit; _open = open;
  }
  bool operator ()(RadioChannel *channel, RadioMessage *msg, bool actual) const;
};

bool UpdateOpenFireMessage::operator ()(RadioChannel *channel, RadioMessage *msg, bool actual) const
{
  if (msg->GetType() != RMTOpenFire) return false;

  Assert(dynamic_cast<RadioMessageOpenFire *>(msg));
  RadioMessageOpenFire *msgSem = static_cast<RadioMessageOpenFire *>(msg);
  Assert(msgSem);
  Assert(msgSem->GetFrom() == _group);

  if (msgSem->IsTo(_unit))
  {
    if (msgSem->GetOpenFireState() == _open)
    {
      _clear = true;
      _done = true;
      return true;
    }
    else if (!actual)
    {
      msgSem->DeleteTo(_unit);
      return true;
    }
    else
    {
      _done = true;
      return true;
    }
  }
  else if (!actual)
  {
    if (msgSem->GetOpenFireState() == _open && channel == _activeChannel)
    {
      msgSem->AddTo(_unit);
      _clear = true;
      _done = true;
      return true;
    }
  }
  return false;
}

void AIGroup::SendOpenFire(OpenFireState open, const OLinkPermNOArray(AIUnit) &listPar)
{
  AIUnit *leader = Leader();

  // active channel selection
  RadioChannel *radioChannel = &GetRadio();
  RadioChannel *activeChannel = radioChannel;
#if _ENABLE_DIRECT_MESSAGES
  RadioChannel *directChannel = NULL;
  if (leader)
  {
    Person *person = leader->GetPerson();
    directChannel = person->GetRadio();
    if (!leader->UseRadio(this, listPar)) activeChannel = directChannel;
  }
#endif

  /// Fix: change the list only locally
  OLinkPermNOArray(AIUnit) list = listPar;

  for (int i=0; i<list.Size();)
  {
    AIUnit *unit = list[i];
    if (!unit)
    {
      list.Delete(i);
      continue;
    }

    if (!leader || unit == leader)
    {
      AI::Semaphore s = unit->GetSemaphore();
      s = ApplyOpenFire(s, open);
      unit->SetSemaphore(s);
      list.Delete(i);
      continue;
    }

    // check radio channel
    bool done = false;
    bool clear = false;
    UpdateOpenFireMessage func(activeChannel, this, unit, open, done, clear);
    radioChannel->ForEachMessage(func);
    if (clear)
    {
      list.Delete(i);
      continue;
    }
    if (done)
    {
      i++;
      continue;
    }

#if _ENABLE_DIRECT_MESSAGES
    if (directChannel)
    {
      directChannel->ForEachMessage(func);
      if (clear)
      {
        list.Delete(i);
        continue;
      }
      if (done)
      {
        i++;
        continue;
      }
    }
#endif

    AI::Semaphore s = unit->GetSemaphore();
    if (ApplyOpenFire(s, open) == s)
      list.Delete(i);
    else
      i++;
  }

  if (leader && list.Size() > 0)
  {
    Assert(activeChannel);
    activeChannel->Transmit(new RadioMessageOpenFire(this, list, open));
  }
}


void AIGroup::SendSuppress(Time until, const OLinkPermNOArray(AIUnit) &listPar, bool direct)
{
  AIUnit *leader = Leader();

  // active channel selection
  RadioChannel *radioChannel = &GetRadio();
  RadioChannel *activeChannel = radioChannel;
#if _ENABLE_DIRECT_MESSAGES
  RadioChannel *directChannel = NULL;
  if (leader)
  {
    Person *person = leader->GetPerson();
    directChannel = person->GetRadio();
    if (!leader->UseRadio(this, listPar)) activeChannel = directChannel;
  }
#endif

  /// Fix: change the list only locally
  OLinkPermNOArray(AIUnit) list = listPar;

  for (int i=0; i<list.Size();)
  {
    AIUnit *unit = list[i];
    if (!unit)
    {
      list.Delete(i);
      continue;
    }

    if (!leader || unit == leader)
    {
      if (unit->GetVehicle()) unit->GetVehicle()->SuppressUntil(until);
      list.Delete(i);
      continue;
    }
    
    i++;
  }

  if (leader && list.Size() > 0)
  {
    Assert(activeChannel);
    activeChannel->Transmit(new RadioMessageSuppress(this, list, until));
  }
}


class FindTargetMessage
{
protected:
  AIUnit *_unit;
  Target **_target;
  Target **_engageTarget;
  Target **_fireTarget;

public:
  FindTargetMessage(AIUnit *unit, Target **target, Target **engageTarget = NULL, Target **fireTarget = NULL)
  {
    _unit = unit; _target = target; _engageTarget = engageTarget; _fireTarget = fireTarget;
  }
  bool operator ()(const RadioChannel *channel, const RadioMessage *msg, bool actual) const;
};

bool FindTargetMessage::operator ()(const RadioChannel *channel, const RadioMessage *msg, bool actual) const
{
  if (msg->GetType() != RMTTarget) return false;

  Assert(dynamic_cast<const RadioMessageTarget *>(msg));
  const RadioMessageTarget *msgTgt = static_cast<const RadioMessageTarget *>(msg);
  if (!msgTgt->IsTo(_unit)) return false;

  if (msgTgt->GetTarget()) *_target = msgTgt->GetTarget();
  if (_engageTarget && msgTgt->GetEngage()) *_engageTarget = *_target;
  if (_fireTarget && msgTgt->GetFire()) *_fireTarget = *_target;
  return false;
}

Target *AIGroup::EngageSent(AIUnit *unit) const
{
  Target *tgt = unit->GetEngageTarget();
  Target *defTgt = unit->GetTargetAssigned();

  FindTargetMessage func(unit, &defTgt, &tgt);
  GetRadio().ForEachMessage(func);

  return tgt;
}

Target *AIGroup::FireSent(AIUnit *unit) const
{
  Target *tgt = unit->GetEnableFireTarget();
  Target *defTgt = unit->GetTargetAssigned();

  FindTargetMessage func(unit, &defTgt, NULL, &tgt);
  GetRadio().ForEachMessage(func);

  return tgt;
}

Target *AIGroup::TargetSent(AIUnit *unit) const
{
  Target *tgt = unit->GetTargetAssigned();

  FindTargetMessage func(unit, &tgt);
  GetRadio().ForEachMessage(func);

  return tgt;
}

class UpdateTargetMessage
{
protected:
  AIGroup *_group;
  AIUnit *_unit;
  Target *_target;
  bool _engage;
  bool _fire;
  bool &_done;
  bool &_clear;

public:
  UpdateTargetMessage(AIGroup *group, AIUnit *unit, Target *target, bool engage, bool fire, bool &done, bool &clear)
    : _done(done), _clear(clear)
  {
    _group = group; _unit = unit; _target = target; _engage = engage; _fire = fire;
  }
  bool operator ()(RadioChannel *channel, RadioMessage *msg, bool actual) const;
};

bool UpdateTargetMessage::operator ()(RadioChannel *channel, RadioMessage *msg, bool actual) const
{
  if (!actual) return false;

  if (msg->GetType() != RMTTarget) return false;

  Assert(dynamic_cast<RadioMessageTarget *>(msg));
  RadioMessageTarget *msgTgt = static_cast<RadioMessageTarget *>(msg);
  Assert(msgTgt);
  Assert(msgTgt->GetFrom() == _group);

  if (msgTgt->IsTo(_unit))
  {
    // we have found some target command to same unit
    // if parameters wanted are subset of actual parameters
    // we may consider target transmitted
    if (_target && msgTgt->GetTarget() != _target)
    {
      // we may remove the old message - it is being superseded
      msgTgt->DeleteTo(_unit);
      return true;
    }
    // we might want to change target parameters
    // we transmit either no target command (adding par to last target)
    // or same target command
    // this means: !target || msgTgt->GetTarget()==target
    // see if above -
    if (msgTgt->IsOnlyTo(_unit))
    {
      // message only to given unit - we may add new parameters
      if (_fire) msgTgt->SetFire(true);
      if (_engage) msgTgt->SetEngage(true);
      _clear = true;
      _done = true;
      return true;
    }
    // check if some target (even to multiple units)
    // that would be superset of current target is on the way
    if
    (
      msgTgt->GetEngage() >= _engage &&
      msgTgt->GetFire() >= _fire
    )
    {
      _clear = true;
      _done = true;
      return true;
    }
  }
  else
  {
    if
    (
      msgTgt->GetTarget() == _target &&
      msgTgt->GetEngage() == _engage &&
      msgTgt->GetFire() == _fire
    )
    {
      // some message with same arguments to other unit is on the way
      // we add unit to address list
      msgTgt->AddTo(_unit);
      _clear = true;
      _done = true;
      return true;
    }
  }
  return false;
}

void AIGroup::SendTarget
(
  Target *target, bool engage, bool fire,
  const OLinkPermNOArray(AIUnit) &listPar, bool silent
)
{ 
  AIUnit *leader = Leader();
  if (!leader) return;

  /// Fix: change the list only locally
  OLinkPermNOArray(AIUnit) list = listPar;

  for (int i=0; i<list.Size();)
  {
    AIUnit *unit = list[i];
    if (!unit)
    {
      list.Delete(i);
      continue;
    }

    if (target)
    {
      EntityAI *tgtAI = target->idExact;
      if (tgtAI == unit->GetVehicleIn() || tgtAI==unit->GetPerson())
      {
        // unit cannot target itself
        list.Delete(i);
        continue;
      }
    }

    // check if unit is already doing what we tell it
    if
    (
      TargetSent(unit)==target &&
      ( !fire || target==FireSent(unit) ) &&
      ( !engage || target==EngageSent(unit) )
    )
    {
      list.Delete(i);
      continue;
    }
    if (unit == leader || silent)
    {
      // leader - direct processing
      // equal to RadioMessageTarget::Transmitted

      Target *tgt = target ? target : unit->GetTargetAssigned();
      if (tgt)
      {
        if (fire) unit->EnableFireTarget(tgt);
        if (engage) unit->EngageTarget(tgt);
      }
      if (target)
      {
        unit->AssignTarget(target);
        EntityAI *veh = unit->GetVehicle();
        veh->BegAttack(target);
      }

      list.Delete(i);
      continue;
    }

    // check radio channel
    bool done = false;
    bool clear = false;
    UpdateTargetMessage func(this, unit, target, engage, fire, done, clear);
    GetRadio().ForEachMessage(func);
    if (clear)
    {
      list.Delete(i);
      continue;
    }
    if (done)
    {
      i++;
      continue;
    }

    // TODO: check if target is not pending
    /*
    bool pending = false;
    if (tgt)
    {
      if (engage) 
      if (TargetSent()==tgt) pending = true;
    if (!engage
    */
    i++;
  }

  if (list.Size() > 0)
  {
    GetRadio().Transmit(new RadioMessageTarget(this, list, target, engage, fire, true));
  }
}

void AIGroup::SendState(RadioMessageState *msg, bool silent)
{
  AIUnit *leader = Leader();

  // active channel selection
  RadioChannel *radioChannel = &GetRadio();
  RadioChannel *activeChannel = radioChannel;
#if _ENABLE_DIRECT_MESSAGES
  RadioChannel *directChannel = NULL;
  if (leader)
  {
    Person *person = leader->GetPerson();
    directChannel = person->GetRadio();
    if (!leader->UseRadio(this, msg->GetTo())) activeChannel = directChannel;
  }
#endif

  // usually called with new
  // if message is not queued we have the only reference to the message
  Ref<RadioMessageState> ref = msg;
  for (int i=0; i<NUnits(); i++)
  {
    AIUnit *unit = GetUnit(i);
    if (!unit) continue;
    if (!msg->IsTo(unit)) continue;

    if (!leader || unit == leader || silent)
    {
      // leader command is executed directly
      Ref<RadioMessageState> temp = msg->Clone();
      temp->ClearTo();
      temp->AddTo(unit);
      temp->Transmitted(CCGroup);
      msg->DeleteTo(unit);
      continue;
    }

    // TODO: check radio channel & optimize
  }
  if (leader && msg->IsToSomeone())
  {
    Assert(activeChannel);
    activeChannel->Transmit(msg);
  }
}

class FindReportStatusMessage
{
protected:
  AIGroup *_group;
  AIUnit *_unit;

public:
  FindReportStatusMessage(AIGroup *group, AIUnit *unit)
  {
    _group = group; _unit = unit;
  }
  bool operator ()(RadioChannel *channel, RadioMessage *msg, bool actual) const;
};

bool FindReportStatusMessage::operator ()(RadioChannel *channel, RadioMessage *msg, bool actual) const
{
  if (msg->GetType() != RMTReportStatus) return false;

  Assert(dynamic_cast<RadioMessageReportStatus *>(msg));
  RadioMessageReportStatus *msgRep = static_cast<RadioMessageReportStatus *>(msg);
  Assert(msgRep);
  Assert(msgRep->GetFrom() == _group);

  return msgRep->IsTo(_unit);
}

void AIGroup::SendReportStatus(const OLinkPermNOArray(AIUnit) &listPar)
{
  AIUnit *leader = Leader();
  if (!leader) return;

  // active channel selection
  RadioChannel *radioChannel = &GetRadio();
  RadioChannel *activeChannel = radioChannel;
#if _ENABLE_DIRECT_MESSAGES
  Person *person = leader->GetPerson();
  RadioChannel *directChannel = person->GetRadio();
  if (!leader->UseRadio(this, listPar)) activeChannel = directChannel;
#endif

  /// Fix: change the list only locally
  OLinkPermNOArray(AIUnit) list = listPar;

  for (int i=0; i<list.Size();)
  {
    AIUnit *unit = list[i];
    if (!unit)
    {
      list.Delete(i);
      continue;
    }

    if (unit == Leader())
    {
      list.Delete(i);
      continue;
    }

    FindReportStatusMessage func(this, unit);
    if (radioChannel->ForEachMessage(func))
      list.Delete(i);
#if _ENABLE_DIRECT_MESSAGES
    else if (directChannel->ForEachMessage(func))
      list.Delete(i);
#endif
    else
      i++;
  }

  Assert(activeChannel);

  if (list.Size() > 0)
    activeChannel->Transmit(new RadioMessageReportStatus(this, list));
}

void AIGroup::SendObjectDestroyed(AIUnit *sender, const VehicleType *type)
{
  if (!sender) return;

  GetRadio().Transmit(new RadioMessageObjectDestroyed(sender, this, type));
}

void AIGroup::SendContact(AIUnit *sender)
{
  if (!sender) return;

  GetRadio().Transmit(new RadioMessageContact(sender, this));
}

class FindUnitDownMessage
{
protected:
  AIUnit *_down;

public:
  FindUnitDownMessage(AIUnit *down)
  {
    _down = down;
  }
  bool operator ()(RadioChannel *channel, RadioMessage *msg, bool actual) const;
};

bool FindUnitDownMessage::operator ()(RadioChannel *channel, RadioMessage *msg, bool actual) const
{
  if (msg->GetType() != RMTUnitKilled) return false;
  if (!msg->GetSender())  return false;
  if (!msg->GetSender()->LSCanSpeak())  return false;

  Assert(dynamic_cast<RadioMessageUnitKilled *>(msg));
  RadioMessageUnitKilled *msgT = static_cast<RadioMessageUnitKilled *>(msg);
  Assert(msgT);

  return msgT->GetWhoKilled() == _down;
}

void AIGroup::SendUnitDown(AIUnit *sender, AIUnit *down)
{
  // check if report already exists
  FindUnitDownMessage func(down);
  if (GetRadio().ForEachMessage(func)) return;

  if (!GetReportedDown(down))
  {
    GetRadio().Transmit(new RadioMessageUnitKilled(sender,down));
  }
  else
  {
    // it is already suppossed to be dead
    // react immediatelly
    if (down->GetLifeState()==LifeStateDead)
    {
      AISubgroup *subgrp = down->GetSubgroup();
      if (subgrp) subgrp->ReceiveAnswer(down,AI::UnitDestroyed);
    }	
  }
}

void AIGroup::SendUnderFire(AIUnit *sender)
{
  if (!sender) return;
  GetRadio().Transmit(new RadioMessageUnderFire(sender, this));
}
void AIGroup::SendCoverMe(AIUnit *sender)
{
  if (!sender) return;
  GetRadio().Transmit(new RadioMessageCoverMe(sender, this));
}
void AIGroup::SendCovering(AIUnit *sender)
{
  if (!sender) return;
  #if 0 // _PROFILE || _DEBUG
  static AIUnit *lastSender;
  static Time lastTime;
  static bool doSwitch = true;
  if (lastSender==sender && lastTime>Glob.time-0.2f)
  { // something went wrong
    if (doSwitch && GWorld->CameraOn()!=sender->GetVehicle())
    {
      doSwitch = false;
      GWorld->SwitchCameraTo(sender->GetVehicle(), CamExternal, true);
    }
  }
  lastSender = sender;
  lastTime = Glob.time;
  #endif
  GetRadio().Transmit(new RadioMessageCovering(sender, this));
}
void AIGroup::SendThrowGrenade(AIUnit *sender)
{
  if (!sender) return;
  GetRadio().Transmit(new RadioMessageThrowingGrenade(sender, this));
}
void AIGroup::SendThrowSmoke(AIUnit *sender)
{
  if (!sender) return;
  GetRadio().Transmit(new RadioMessageThrowingSmoke(sender, this));
}
void AIGroup::SendReloading(AIUnit *sender)
{
  if (!sender) return;
  GetRadio().Transmit(new RadioMessageReloading(sender, this));
}

class CancelFireStatusMessage
{
protected:
  AIGroup *_group;

public:
  CancelFireStatusMessage(AIGroup *group)
  {
    _group = group;
  }
  bool operator ()(RadioChannel *channel, RadioMessage *msg, bool actual) const;
};

bool CancelFireStatusMessage::operator ()(RadioChannel *channel, RadioMessage *msg, bool actual) const
{
  if (actual) return false;
  if (msg->GetType() != RMTFireStatus) return false;

  channel->Cancel(msg, _group);
  return true;
}


void AIGroup::ReportFire(AIUnit *who, bool status)
{
  // check radio for messages of the same type
  CancelFireStatusMessage func(this);
  GetRadio().ForEachMessage(func);

  GetRadio().Transmit(new RadioMessageFireStatus(who, status));
}

void AIGroup::ReportArtilleryFire(AIUnit *who, int status)
{
  // check radio for messages of the same type
  CancelFireStatusMessage func(this);
  GetRadio().ForEachMessage(func);

  GetRadio().Transmit(new RadioMessageArtilleryFireStatus(who, status));
}

void AIGroup::SendClear(AIUnit *sender)
{
  if (!sender) return;

  GetRadio().Transmit(new RadioMessageClear(sender, this));
}

void AIGroup::SendGetOut(const OLinkPermNOArray(AIUnit) &list)
{
  Command cmd;
  
  cmd._message = Command::GetOut;
  cmd._context = Command::CtxAuto;
  SendCommand(cmd, list);
}

void AIGroup::SendAutoCommandToUnit(Command &cmd, AIUnit *unit, bool join, bool channelCenter)
{
  Assert(unit);
  if (!unit || !unit->IsUnit())
  {
    // enable eject for whole crew
    if (cmd._message != Command::Action || !(cmd._action && cmd._action->GetType() == ATEject))
      return;
  }
  AISubgroup *subgrp = unit->GetSubgroup();
  Assert(subgrp);
  if (!subgrp)
    return;

  OLinkPermNOArray(AIUnit) list;
  list.Realloc(1);
  list.Resize(1);
  list[0] = unit;

  if (join)
  {
    // TODO: check stack and radio for some other command with _joinToSubgroup
    cmd._joinToSubgroup = subgrp;
    cmd._context = Command::CtxAuto;
  }
  else
    cmd._context = Command::CtxAuto;

  Assert(cmd._message != Command::NoCommand);
  SendCommand(cmd, list, channelCenter);
}

void AIGroup::IssueAutoCommand(Command &cmd, const OLinkPermNOArray(AIUnit) &list)
{
  AIUnit *unit = list[0];
  Assert(unit);
  if (!unit || !unit->IsUnit())
    return;
  AISubgroup *subgrp = unit->GetSubgroup();
  Assert(subgrp);
  if (!subgrp)
    return;

  cmd._joinToSubgroup = subgrp;
  cmd._context = Command::CtxAuto;
  cmd._id = _nextCmdId++;

  Assert(cmd._message != Command::NoCommand);

  IssueCommand(cmd, list);
}

void AIGroup::NotifyAutoCommand(Command &cmd, const OLinkPermNOArray(AIUnit) &list)
{
  // unit is not able to issue any commands when it is not alive
  AIUnit *unit = list[0];
  if (!unit->LSIsAlive()) return;
  IssueAutoCommand(cmd, list);
  
  GetRadio().Transmit(new RadioMessageNotifyCommand(list, this, cmd));
  /*
  if (list.Size()>1)
  {
    // note: the units are already in the subgroup as a result of RadioMessageNotifyCommand
    // however we want audible notification for this
    OLinkPermNOArray(AIUnit) listLeft;
    listLeft.Copy(list.Data()+1,list.Size()-1);
    GetRadio().Transmit(new RadioMessageJoin(this, unit, listLeft));
  }
  */
}

void AIGroup::ReceiveUnitStatus(AIUnit *unit, Answer answer)
{
  Assert(unit->GetGroup() == this);
  if (unit->GetGroup()!=this) return;
  int index = unit->ID() - 1;
  Assert(index >= 0 && index < _unitSlots.Size());
  AIUnitSlot &slot = _unitSlots[index];
  switch (answer)
  {
    case HealthCritical:
      slot._healthState = AIUnit::RSCritical;
      break;
    case DamageCritical:
      slot._dammageState = AIUnit::RSCritical;
      break;
    case FuelCritical:
      slot._fuelState = AIUnit::RSCritical;
      break;
    case FuelLow:
      slot._fuelState = AIUnit::RSLow;
      break;
    case AmmoCritical:
      slot._ammoState = AIUnit::RSCritical;
      break;
    case AmmoLow:			
      slot._ammoState = AIUnit::RSLow;
      break;
  }
}

void AIGroup::ReceiveAnswer(AISubgroup* from, Answer answer)
{
  if (!from)
    return;

#if LOG_COMM
  Log("Receive answer: Group %s: From subgroup %s: Answer %d)",
    (const char *)GetDebugName(), (const char *)from->GetDebugName(), answer);
#endif
  
  switch (answer)
  {
    case AI::CommandCompleted:
    {
    }
    break;
    case AI::CommandFailed:
    case AI::SubgroupDestinationUnreacheable:
    {
    }
    break;
  }
}

// Communication with center
void AIGroup::SendAnswer(Answer answer)
{
#if LOG_COMM
  Log("Send answer: Group %s: Answer %d",
  (const char *)GetDebugName(), answer);
#endif

  if (_center)
  {
    _center->ReceiveAnswer(this, answer);

    /*
    switch (answer)
    {
      case AI::MissionCompleted:
      case AI::MissionFailed:
      case AI::DestinationUnreacheable:
      case AI::GroupDestroyed:
        if (GLOB_WORLD->FocusOn() && GLOB_WORLD->FocusOn()->GetGroup() == this)
          GLOB_WORLD->CancelTimeJump();
        break;
    }
    */
  }
}

void AIGroup::SendRadioReport(ReportTargetList &targets, AIUnit *from)
{
  if (UnitsCount() <= 1) return; //nobody is interested in
  if (!targets.Size()) return;   //nothing to report
  if (!from || !from->LSCanSpeak()) return; //from is invalid

  //if (ReportSent(subject, target.type)) return;

  GetRadio().Transmit(new RadioMessageReportTarget(from, this, ReportNew, targets));

  // Send Report - Inform center
  for (int tn=0; tn<targets.Size(); tn++)
  {
    TargetNormal *vtar = targets[tn].GetRef();
    if( vtar->IsKnown() )
    {
      SendReport(ReportNew, *vtar);
      if (GetCenter()->IsEnemy(vtar->side) && IsLocal() && !IsAnyPlayerGroup())
      {
        for (int j=0; j<NSubgroups(); j++)
        {
          AISubgroup *subgrp = GetSubgroup(j);
          if (subgrp) subgrp->OnEnemyDetected(vtar->type, vtar->position);
        }
      }
    }
  }

  // Transfer targets over network
  // TODO: aggregate into one message
  if (GWorld->GetMode() == GModeNetware)
  {
    for (int tn=0; tn<targets.Size(); tn++)
    {
      for (int i=0; i<_unitSlots.Size(); i++)
      {
        AIUnit *unit = _unitSlots[i]._unit;
        if (!unit) continue;
        Person *person = unit->GetPerson();
        if (!person) continue;
        if (person->IsLocal()) continue;
        int dpnid = person->GetRemotePlayer();
        if (dpnid == AI_PLAYER) continue;

        GetNetworkManager().RevealTarget(dpnid, this, targets[tn]->idExact);
      }
    }
  }
}

/*!
\patch_internal 1.45 Date 2/18/2002 by Ondra
- Fixed: send reports about seen units only when leader
is alive and knows about the target.
*/

void AIGroup::SendReport(ReportSubject subject, TargetNormal &target)
{
  if (_center)
  {
    // send report about units only when leader is alive
    AIUnit *leader = Leader();
    if
    (
      leader && leader->LSCanSpeak() &&
      target.IsKnownBy(leader)
    )
    {
      _center->ReceiveReport(this, subject, target);
    }
  }
}

TypeIsSimple(AIUnit *);

void AIGroup::UnassignVehicle(Transport *veh)
{
  if (!veh) return;
  // remove from list
  for (int i=0; i<_vehicles.Size(); i++)
  {
    if (_vehicles[i] == veh)
    {
      _vehicles.Delete(i);
      break;
    }
  }
  veh->AssignGroup(NULL);
  
  AIBrain *unit;
  unit = veh->GetDriverAssigned();
  if (unit && unit->GetGroup() == this)
  {
    // faster than unit->UnassignVehicle
    unit->ClearAssignedVehicle();
    veh->AssignDriver(NULL);
  }
  unit = veh->GetCommanderAssigned();
  if (unit && unit->GetGroup() == this)
  {
    // faster than unit->UnassignVehicle
    unit->ClearAssignedVehicle();
    veh->AssignCommander(NULL);
  }
  unit = veh->GetGunnerAssigned();
  if (unit && unit->GetGroup() == this)
  {
    // faster than unit->UnassignVehicle
    unit->ClearAssignedVehicle();
    veh->AssignGunner(NULL);
  }
  for (int i=0; i<veh->GetManCargo().Size(); i++)
  {
    // faster than unit->UnassignVehicle
    unit = veh->GetCargoAssigned(i);
    if (unit && unit->GetGroup() == this) unit->ClearAssignedVehicle();
  }
  veh->LeaveCargo(this);
}

// Implementation
/*!
\patch 1.17 Date 08/14/2001 by Ondra
- Fixed: Discrepancy between conditions for getting in and automatical eject.
\patch_internal 1.17 Date 08/14/2001 by Ondra
- New: New function IsPossibleToGetIn, has most functionality from IsAbleToMove,
but has no test for driver.
*/

void AIGroup::AssignVehicles()
{
  // if there is nothing to assign, do nothing
  if (_vehicles.Size() <=0) return;
  // TODO: check assignment structure for consistence

  AUTO_STATIC_ARRAY(AIUnit *, soldiers, 32)
  for (int i=0; i<_unitSlots.Size(); i++)
  {
    AIUnit *unit = _unitSlots[i]._unit;
    if (!unit) continue;
    //if (!unit->IsFreeSoldier()) continue;
    Transport *veh = unit->VehicleAssigned();
    if (veh)
    {
      if (veh->IsAbleToMove() && 
        (!veh->Type()->HasDriver() || (veh->GetDriverAssigned() && !veh->GetDriverAssigned()->GetPerson()->IsDamageDestroyed()))) continue;
      // we need to remove this vehicle
    }
    if (!unit->GetPerson()) continue;
    float exp = unit->GetPerson()->GetExperience();
    for (int j=0; j<soldiers.Size(); j++)
    {
      AIUnit *with = soldiers[j];
      if (exp >= with->GetPerson()->GetExperience())
      {
        soldiers.Insert(j, unit);
        goto NextUnit;
      }
    }
    soldiers.Add(unit);
NextUnit:
    continue;
  }

  if (soldiers.Size() <= 0) return;

  for (int i=0; i<_vehicles.Size(); i++)
  {
    Transport *veh = _vehicles[i];
    if (!veh  || !veh->IsPossibleToGetIn()) continue;

    AIUnit *commander = NULL;
    AIUnit *driver = NULL;
    AIUnit *gunner = NULL;
    if (veh->GetType()->HasObserver() && (!veh->GetCommanderAssigned() || veh->GetCommanderAssigned()->GetPerson()->IsDamageDestroyed()))
    {
      for (int j=0; j<soldiers.Size(); j++)
      {
        AIUnit *unit = soldiers[j];
        if (veh->QCanIGetInCommander(unit->GetPerson()))
        {
          commander = unit;
          soldiers.Delete(j);
          break;
        }
      }
      //LogF("Commander %s",commander ? (const char *)commander->GetDebugName() : "no");
    }
    if (veh->GetType()->HasGunner() && (!veh->GetGunnerAssigned() || veh->GetGunnerAssigned()->GetPerson()->IsDamageDestroyed()))
    {
      for (int j=0; j<soldiers.Size(); j++)
      {
        AIUnit *unit = soldiers[j];
        if (veh->QCanIGetInGunner(unit->GetPerson()))
        {
          gunner = unit;
          soldiers.Delete(j);
          break;
        }
      }
      //LogF("Gunner %s",gunner ? (const char *)gunner->GetDebugName() : "no");
      if (!gunner)
      {
        // no suitable gunner found - use commander if possible
        gunner = commander, commander = NULL;
        //LogF("   2nd %s",gunner ? (const char *)gunner->GetDebugName() : "no");
      }
    }

    if (veh->GetType()->HasDriver() && (!veh->GetDriverAssigned() || veh->GetDriverAssigned()->GetPerson()->IsDamageDestroyed()))
    {
      for (int j=0; j<soldiers.Size(); j++)
      {
        AIUnit *unit = soldiers[j];
        if (veh->QCanIGetIn(unit->GetPerson()))
        {
          driver = unit;
          soldiers.Delete(j);
          break;
        }
      }

      //LogF("Driver %s",driver ? (const char *)driver->GetDebugName() : "no");
      if (!driver)
      {
        // no suitable driver found - use gunner or commander if possible
        driver = commander, commander = NULL;
        if (!driver) driver = gunner, gunner = NULL;
        //LogF("   2nd %s",driver ? (const char *)driver->GetDebugName() : "no");
      }
    }

    // assign most needed positions first

    if (driver) driver->AssignAsDriver(veh);
    if (gunner) gunner->AssignAsGunner(veh);
    if (commander) commander->AssignAsCommander(veh);

    if (soldiers.Size() <= 0) return;
  }

  // remove vehicles that can be no longer used
  /*
  for (int i=0; i<_vehicles.Size(); i++)
  {
    Transport *veh = _vehicles[i];
    if (!veh  || !veh->IsAbleToMove()) continue;
  }
  */

  // now try to assign to cargo position
  for (int i=0; i<_vehicles.Size(); i++)
  {
    Transport *veh = _vehicles[i];
    if (!veh  || !veh->IsAbleToMove()) continue;
    for (int j=0; j<veh->GetManCargo().Size(); j++)
    {
      // FIX - add also cargo positions with dead units
      if (!veh->GetCargoAssigned(j) || veh->GetCargoAssigned(j)->GetPerson()->IsDamageDestroyed())
      {
        // try to assign somebody to this position
        for (int k=0; k<soldiers.Size(); k++)
        {
          AIUnit *unit = soldiers[k];
          if (veh->QCanIGetInCargo(unit->GetPerson(), j) == j)
          {
            soldiers.Delete(k);
            unit->AssignAsCargo(veh, j);
            if (soldiers.Size() <= 0) return;
            break;
          }
        }
      }
    }
  }
}

class FindMainSubgroupCommandMessage
{
protected:
  AIGroup *_group;

public:
  FindMainSubgroupCommandMessage(AIGroup *group)
  {
    _group = group;
  }
  bool operator ()(RadioChannel *channel, RadioMessage *msg, bool actual) const;
};

bool FindMainSubgroupCommandMessage::operator ()(RadioChannel *channel, RadioMessage *msg, bool actual) const
{
  if (msg->GetType() != RMTCommand) return false;

  Assert(dynamic_cast<RadioMessageCommand *>(msg));
  RadioMessageCommand *msgCmd = static_cast<RadioMessageCommand *>(msg);
  Assert(msgCmd);
  Assert(msgCmd->GetFrom() == _group);
  
  return msgCmd->IsToMainSubgroup();
}

bool AIGroup::CommandSent()
{
  // check stack
  if (MainSubgroup()->_stack.Size() > 0)
    return true;

  // check all radio channels
  FindMainSubgroupCommandMessage func(this);
  if (GetRadio().ForEachMessage(func)) return true;
  Assert(GetCenter());
  if (GetCenter()->GetSideRadio().ForEachMessage(func)) return true;
#if _ENABLE_DIRECT_MESSAGES
  AIUnit *leader = Leader();
  if (leader)
  {
    RadioChannel *directChannel = leader->GetPerson()->GetRadio();
    if (directChannel && directChannel->ForEachMessage(func)) return true;
  }
#endif

  return false;
}

class FindCommandMessage
{
protected:
  AIGroup *_group;
  AIUnit *_unit;
  Command::Message _message;
  bool _checkUnit;

public:
  FindCommandMessage(AIGroup *group, AIUnit *unit, Command::Message message)
  {
    _group = group; _unit = unit; _message = message; _checkUnit = true;
  }
  FindCommandMessage(AIGroup *group, Command::Message message)
  {
    _group = group; _message = message; _checkUnit = false;
  }
  bool operator ()(RadioChannel *channel, RadioMessage *msg, bool actual) const;
};

bool FindCommandMessage::operator ()(RadioChannel *channel, RadioMessage *msg, bool actual) const
{
  if (msg->GetType() != RMTCommand) return false;

  Assert(dynamic_cast<RadioMessageCommand *>(msg));
  RadioMessageCommand *msgCmd = static_cast<RadioMessageCommand *>(msg);
  Assert(msgCmd);
  Assert(msgCmd->GetFrom() == _group);

  if (msgCmd->GetMessage() != _message) return false;
  if (_checkUnit && !msgCmd->IsTo(_unit)) return false;
  return true;
}

bool AIGroup::CommandSent(Command::Message message)
{
  int m = NSubgroups();
  for (int j=0; j<m; j++)
  {
    AISubgroup *subgrp = GetSubgroup(j);
    if (!subgrp) continue;

    int n = subgrp->_stack.Size();
    for (int i=0; i<n; i++)
    {
      const Command *cmd = subgrp->_stack[i]._task;
      if (cmd->_message == message)
        return true;
    }
  }

  // check all radio channels
  FindCommandMessage func(this, message);
  if (GetRadio().ForEachMessage(func)) return true;
  Assert(GetCenter());
  if (GetCenter()->GetSideRadio().ForEachMessage(func)) return true;
#if _ENABLE_DIRECT_MESSAGES
  AIUnit *leader = Leader();
  if (leader)
  {
    RadioChannel *directChannel = leader->GetPerson()->GetRadio();
    if (directChannel && directChannel->ForEachMessage(func)) return true;
  }
#endif

  return false;
}

bool AIGroup::CommandSent(AIUnit *to, Command::Message message)
{
  AISubgroup *subgrp = to->GetSubgroup();
  if (!subgrp) return false;	// BUG in MP
  int i, n = subgrp->_stack.Size();
  for (i=0; i<n; i++)
  {
    const Command *cmd = subgrp->_stack[i]._task;
    if (cmd->_message == message)
      return true;
  }
  // check all radio channels
  FindCommandMessage func(this, to, message);
  if (GetRadio().ForEachMessage(func)) return true;
  Assert(GetCenter());
  if (GetCenter()->GetSideRadio().ForEachMessage(func)) return true;
#if _ENABLE_DIRECT_MESSAGES
  AIUnit *leader = Leader();
  if (leader)
  {
    RadioChannel *directChannel = leader->GetPerson()->GetRadio();
    if (directChannel && directChannel->ForEachMessage(func)) return true;
  }
#endif

  return false;
}

class CancelGetInCommandMessage
{
protected:
  AIGroup *_group;
  AIUnit *_unit;

public:
  CancelGetInCommandMessage(AIGroup *group, AIUnit *unit)
  {
    _group = group; _unit = unit;
  }
  bool operator ()(RadioChannel *channel, RadioMessage *msg, bool actual) const;
};

bool CancelGetInCommandMessage::operator ()(RadioChannel *channel, RadioMessage *msg, bool actual) const
{
  if (msg->GetType() != RMTCommand) return false;

  Assert(dynamic_cast<RadioMessageCommand *>(msg));
  RadioMessageCommand *msgCmd = static_cast<RadioMessageCommand *>(msg);
  Assert(msgCmd);
  Assert(msgCmd->GetFrom() == _group);

  if
  (
    msgCmd->IsTo(_unit) && msgCmd->GetMessage() == Command::GetIn &&
    (msgCmd->GetContext() == Command::CtxAuto || msgCmd->GetContext() == Command::CtxAutoSilent)
  )
  {
    channel->Cancel(msg, _group);
  }
  return false;
}

void AIGroup::ClearGetInCommands(AIUnit *to)
{
  // cancel in all radio channels
  CancelGetInCommandMessage func(this, to);
  GetRadio().ForEachMessage(func);
  Assert(GetCenter());
  GetCenter()->GetSideRadio().ForEachMessage(func);
#if _ENABLE_DIRECT_MESSAGES
  AIUnit *leader = Leader();
  if (leader)
  {
    RadioChannel *directChannel = leader->GetPerson()->GetRadio();
    if (directChannel) directChannel->ForEachMessage(func);
  }
#endif
}

class FindReportSentMessage
{
protected:
  ReportSubject _subject;
  const VehicleType *_type;

public:
  FindReportSentMessage(ReportSubject subject, const VehicleType *type)
  {
    _subject = subject; _type = type;
  }
  bool operator ()(RadioChannel *channel, RadioMessage *msg, bool actual) const;
};

bool FindReportSentMessage::operator ()(RadioChannel *channel, RadioMessage *msg, bool actual) const
{
  if (msg->GetType() != RMTReportTarget) return false;
  Assert(dynamic_cast<RadioMessageReportTarget *>(msg));
  RadioMessageReportTarget *msgReport = static_cast<RadioMessageReportTarget *>(msg);
  Assert(msgReport);
  return msgReport->GetSubject() == _subject && msgReport->HasType(_type);
}

bool AIGroup::ReportSent(ReportSubject subject, const VehicleType *type)
{
  // check radio channel
  FindReportSentMessage func(subject, type);
  return GetRadio().ForEachMessage(func);
}

struct GetInPair
{
  TargetId vehicle;
  OLinkPermNOArray(AIUnit) list;
};
TypeContainsOLink(GetInPair)

class GetInPairs : public AutoArray<GetInPair>
{
  typedef AutoArray<GetInPair> base;

public:
  void Add(EntityAI *veh, AIUnit *unit);
};

void GetInPairs::Add(EntityAI *veh, AIUnit *unit)
{
  int i, n = Size();
  for (i=0; i<n; i++)
  {
    GetInPair &pair = Set(i);
    if (pair.vehicle == veh)
    {
      pair.list.AddUnique(unit);
      return;
    }
  }
  i = base::Add();
  GetInPair &pair = Set(i);
  pair.vehicle = TargetId(veh);
  pair.list.AddUnique(unit);
}

static const char *GetBrainName(const AIBrain *unit)
{
  if (!unit) return "NULL";
  return unit->GetDebugName();
}

void AIGroup::GetInVehicles()
{
  if (IsAnyPlayerGroup())
    return;

  // get in / get out
  OLinkPermNOArray(AIUnit) getout;
  GetInPairs getin;
  for (int i=0; i<_unitSlots.Size(); i++)
  {
    AIUnit *unit = _unitSlots[i]._unit;
    if (!unit) continue;
    if (unit->GetLifeState() == LifeStateDead) continue;

    if (CommandSent(unit, Command::Heal))
    {
      // must be outside
      if (!unit->IsFreeSoldier())
      {
        getout.Add(unit);
      }
      continue;
    }
    //unit cannot board vehicle while unconscious
    if(unit->GetLifeState() == LifeStateUnconscious) continue;

    if (CommandSent(unit, Command::GetIn))
      continue;
    if (CommandSent(unit, Command::GetOut))
      continue;

    Transport *assigned = unit->VehicleAssigned();
    if (assigned && !assigned->IsAbleToMove())
    {
      unit->UnassignVehicle();
      assigned = NULL;
    }
    if
    (
      unit->IsGetInAllowed() &&
      unit->IsGetInOrdered() &&
      assigned
    )
    {
      // must be inside
      if (unit->IsFreeSoldier())
      {
        if (!unit->GetPerson()->IsActionInProgress(MFGetIn))
        {
          getin.Add(assigned, unit);
        }
      }
      // wrong vehicle or wrong position
      else if
      (
        assigned != unit->GetVehicle() ||
        (assigned->GetDriverAssigned() == unit) != (assigned->DriverBrain() == unit) ||
        (assigned->GetCommanderAssigned() == unit) != (assigned->ObserverBrain() == unit) ||
        (assigned->GetGunnerAssigned() == unit) != (assigned->GunnerBrain() == unit)
      )
      {
        getout.Add(unit);
      }
    }
    else
    {
      // must be outside
      if (!unit->IsFreeSoldier())
      {
        getout.Add(unit);
      }
    }
  }

  Command cmd;
  cmd._joinToSubgroup = MainSubgroup();
  cmd._context = Command::CtxAuto;
  if (getout.Size() > 0)
  {
    cmd._message = Command::GetOut;
    SendCommand(cmd, getout);
  }

  cmd._message = Command::GetIn;
  cmd._discretion =  Command::Undefined;
  cmd._movement = Command::MoveFast;
  for (int i=0; i<getin.Size(); i++)
  {
    GetInPair &pair = getin[i];
    //Target *tgt=FindTarget(pair.vehicle);
    //if( tgt )
    //{
    //  cmd._target = tgt;
    //	SendCommand(cmd, pair.list);
    //}
    //else
    //{
    //	Fail("Getin target unknown");
    //}
    cmd._target = pair.vehicle;
    SendCommand(cmd, pair.list);
  }
}

/*!
\patch 1.28 Date 10/19/2001 by Ondra
- Fixed: Bug in reaction to enemy detection caused passangers not attacking enemy.
*/
void AIGroup::ReactToEnemyDetected(Target *enemy)
{
  AIUnit *leader = Leader();
  for (int i=0; i<_unitSlots.Size(); i++)
  {
    AIUnit *unit = _unitSlots[i]._unit;
    if (!unit) continue;
    unit->SetDanger(DCEnemyDetected, unit->Position(unit->GetFutureVisualState()), enemy);
    if (unit->GetCombatModeMajor() == CMCareless) continue;
    if (_combatModeMinor >= CMCombat) continue;

    // going to combat - get out of some vehicles
    if (unit->IsInCargo())
    {
      Transport *trans = unit->GetVehicleIn();
      if (trans && trans->Type()->GetUnloadInCombat())
      {
        unit->OrderGetIn(false);
      }
    }
  }
  if (_combatModeMinor < CMCombat)
  {
    if (leader && leader->LSCanSpeak() && UnitsCount()>1)
    {
      SendContact(leader);
    }
    CombatMode oldCM = leader ? leader->GetCombatMode() : CMSafe;
    _combatModeMinor = CMCombat;
    CombatMode newCM = leader ? leader->GetCombatMode() : CMSafe;
    if (oldCM!=newCM) leader->OnCombatModeChanged(oldCM,newCM);
  }
}

/*!
\patch 1.28 Date 10/30/2001 by Jirka
- Fixed: MP - gunner does not see any unit on the radar
*/
bool AIGroup::CreateTargetList(bool initialize, bool report)
{
  PROFILE_SCOPE_EX(aiGCT, ai);

#if _VBS2 // disableAI target prevents group from seeing any targets
   AIUnit *leader = Leader();
   if (leader)
    if (leader->GetAIDisabled()&AIUnit::DATarget) return false;
#endif

  float nearestEnemyDist2 = 1e10;
  {
    //PROFILE_SCOPE_EX(aiCT1, ai);
    for (int i=0; i<_unitSlots.Size(); i++)
    {
      AIUnit *unit = _unitSlots[i]._unit;
      if (!unit) continue;
      EntityAIFull *veh = unit->GetVehicle();
      if (!veh) continue;
      if (!unit->IsUnit() && !ScanNeeded(unit,this,veh)) continue;
      if (!unit->LSCanTrackTargets() || !veh->GetType()->ScanTargets()) continue;
      veh->WhatIsVisible(_targetList, initialize);
      // check unit for nearestEnemyDist2
      saturateMin(nearestEnemyDist2,unit->GetNearestEnemyDist2());
    }
  }

  _nearestEnemyDist2 = nearestEnemyDist2;
  //LogF("%s: nearestEnemyDist %g",(const char *)GetDebugName(),sqrt(_nearestEnemyDist2));

  _targetList.Manage(this);

  _enemiesDetected = 0;
  _unknownsDetected = 0;
  
  AICenter *center = GetCenter();

  // consider using Friendly + Enemy instead of Any
  bool someTarget=false;
  bool someAggrTargetAdded=false;
  int n = _targetList.AnyCount(); 
  for (int i=0; i<n; i++)
  {
    TargetNormal *vtar = _targetList.GetAny(i);
    // any target that is vanished or destroyed should be updated
    // and no more processing is required
    if (vtar->vanished || vtar->destroyed)
    {
      SendReport(ReportDestroy, *vtar);
      // we might also send radio report

      if
      (
        vtar->destroyed && vtar->IsKnownNoDelay() &&
        vtar->idKiller && vtar->timeReported<=TIME_MIN &&
        vtar->idExact && vtar->idExact->IsInLandscape()
      )
      {
        AIBrain *killer = vtar->idKiller->CommanderUnit();
        if (killer && killer->IsLocal() && !killer->IsAnyPlayer() && killer->GetUnit())
        {
          if (killer->LSCanSpeak())
          {
            SendObjectDestroyed(killer->GetUnit(),vtar->type);
          }
          vtar->timeReported = Glob.time;
          vtar->posReported = vtar->position;
        }
        else if (killer && killer->IsLocal() && killer->IsAnyPlayer() && killer->GetUnit())
        { // targets player should report should be considered as reported
          // without it the targets are reported after the commanding player leave the vehicle
          // see news:h0b1cp$jrg$1@new-server.localdomain
          vtar->timeReported = Glob.time;
          vtar->posReported = vtar->position;
        }
      }
        
      continue;	
    }
    bool doReport = report ? vtar->IsKnown() : vtar->IsKnownNoDelay();
    // report only recently seen targets
    if (doReport && !(vtar->idExact && vtar->idExact->IsAnimal()))
    {
      // check if we can report it
      if
      (
        vtar->lastSeen>=Glob.time-10 &&
        vtar->idSensor && vtar->idSensor->IsLocal() &&
        (!vtar->idSensor->IsAnyPlayer() || vtar->timeReported<=TIME_MIN)
      )
      {
        // check how large position change is required to report
        float time = 240; // time from reporting 
        if (vtar->timeReported>Glob.time-time)
        {
          time = Glob.time-vtar->timeReported;
        }
        float minDist = 1000 - time*4;
        float minTime = 60;
        saturateMax(minDist,0);

        // calculate how big distance is required to report the change
        AIUnit *unit = vtar->idSensor->GetUnit(); // we can convert it here - old AI structures is used
        if (unit && unit->GetCombatMode()==CMStealth)
        {
          // in stealth mode report targets more often
          minDist *= 0.125;
          minTime = 10;
        }
        
        if
        (
          vtar->timeReported<Glob.time-minTime &&
          vtar->posReported.Distance2(vtar->position)>Square(minDist)
        )
        {
          // timeReported - when was target reported
          if (initialize)
          {
            vtar->timeReported = TIME_MIN;
            vtar->posReported = VZero;
          }
          else
          {
            if
            (
              vtar->type->IsKindOf(GWorld->Preloaded(VTypeStatic)) ||
              center->IsFriendly(vtar->side) ||
              center->IsNeutral(vtar->side)
            )
            {
              // never report friendly or static
              vtar->timeReported = Glob.time;
              vtar->posReported = vtar->position;
              //LogF("  ignore report");
            }
            else if( vtar->side == TSideUnknown || center->IsEnemy(vtar->side) )
            {
              // simple target->GetSubjectiveCost cannot be used as it contains negative values often
              // moreover it is possibly initialized after the AddAggrTarget at the end of AIGroup::CreateTargetList
              // float vtarCost = GetSubjectiveCost(vtar);
              //TODO: possibly add some dist2 related weight to MARTA importance
              float vtarCost = GWorld->GetMartaImportance(vtar->GetType());
              // report is to be delayed (targets aggregation)
              _targetList.AddAggrTarget(vtar, vtarCost);
              someAggrTargetAdded = true;
            }
          }
        } // if (some change to report)
      } // if (can report)
    } // if (report)

    // SendReport only for the following targets which:
    //   1. are known, i.e. doReport holds
    //   2. can be already reported, i.e. CanSendReport holds
    //      or it is called from initialization, i.e. !report holds
    if ( doReport && ( CanSendReport(vtar) || !report ) )
    {
      // Send Report - Inform center
      SendReport(ReportNew, *vtar);

      if (GetCenter()->IsEnemy(vtar->side) && IsLocal() && !IsAnyPlayerGroup())
      {
        for (int j=0; j<NSubgroups(); j++)
        {
          AISubgroup *subgrp = GetSubgroup(j);
          if (subgrp) subgrp->OnEnemyDetected(vtar->type, vtar->position);
        }
      }
    }

    if (vtar->IsKnownBySome())
    {
      if (vtar->side == TSideUnknown)
      {
        someTarget=true;
        _unknownsDetected++;
      }
      else if (center->IsEnemy(vtar->side))
      {
        Threat threat=vtar->type->GetDamagePerMinute(Square(200),1);
        if( (threat[VSoft]+threat[VArmor]+threat[VAir])>0 )
        { // calculate only dangerous enemies
          _enemiesDetected++;
        }
        someTarget=true;
      }
    }
  }

  // when !report, it is called from InitSensors and these targets should be considered as already Reported
  // move all targets from _toReport into _reported
  if (!report && someAggrTargetAdded)
  {
    _targetList.ConsiderAggrTargetsReported();
  }

  // check center database sometimes
  if (Glob.time>_checkCenterDBase+20.0f && !IsAnyPlayerGroup())
  {
    // use AICenter database for target recognition
    _checkCenterDBase = Glob.time;

    // consider: using Unknown instead of Any
    for (int i=0; i<_targetList.AnyCount(); i++)
    {
      TargetNormal *tar = _targetList.GetAny(i);
      // no need to check the center for unknown targets
      if (!tar->IsKnownBySome()) continue;

      float sideAccuracy=tar->FadingSideAccuracy();
      float typeAccuracy=tar->FadingSideAccuracy();
      
      if (sideAccuracy < 1.5 || typeAccuracy < 1.5 || tar->side==TSideUnknown )
      {	// inaccurate info, consult AICenter
        const AITargetInfo *tgt = center->FindTargetInfo(tar->idExact);
        if (tgt)
        {
          if (tgt->FadingSideAccuracy() > sideAccuracy)
          {
            tar->side = tgt->_side;
            tar->sideAccuracy = tgt->_accuracySide;
            if (tar->sideAccuracy>1.5f) tar->sideChecked = true;
          }
          if (tgt->FadingTypeAccuracy() > typeAccuracy)
          {
            tar->type = tgt->_type;
            tar->accuracy = tgt->_accuracyType;
          }
        }
      }
    }
  }

  AIBrain *leader = Leader();
  for (int i=0; i<NTgtCategory; i++)
  {
    _targetList.RecomputeTargetListSubjectiveCost(TgtCategory(i),leader,this);
  }
  if (_enemyRecomputeCostTime<TIME_MAX)
  {
    //LogF("+++ %s recomputed* at %.2f",cc_cast(GetDebugName()),Glob.time.toFloat());
  }
  _targetList.CompactIfNeeded();
  _enemyRecomputeCostTime = TIME_MAX; // more than recompute was done here, recompute no longer needed

#define TEST_TARGET_LIST 0
#if TEST_TARGET_LIST
  RptF("AIGroup Target List Report for %s (time: %d)", cc_cast(GetDebugName()), Glob.time.toInt());
  for (int i=0; i<_targetList.AnyCount(); i++)
  {
    TargetNormal *tar = _targetList.GetAny(i);
    RString tgtListRpt;
    Vector3 pos = tar->GetPosition();
    Vector3 expos = tar->idExact->Position();
    Vector3 aimpos = tar->AimingPosition();
    RptF("   %s ... tgt[%.0f,%.0f,%.0f], aim[%.0f,%.0f,%.0f], idExact[%.0f,%.0f,%.0f]", 
      cc_cast(tar->GetDebugName()), 
      pos.X(),pos.Y(),pos.Z(),
      aimpos.X(),aimpos.Y(),aimpos.Z(),
      expos.X(),expos.Y(),expos.Z()
      );
  }
#endif

  
#if LOG_THINK
    Log("Group %s, %d enemies, %d visible",
    (const char *)GetDebugName(), _targetList.Size(), visibleList.Size() );
#endif

  return someTarget;
}

void TargetList::RecomputeTargetListSubjectiveCost(TgtCategory cat, AIBrain *leader, AIGroup *grp)
{
  RefArray<TargetNormal> &list = _list[cat];
  AICenter *center = grp->GetCenter();

  for (int i=0; i<list.Size(); i++)
  {
    TargetNormal *tar = list.Get(i);

    // tar->State will return at most TargetAlive for anything not IsEnemy
    if (
      !tar->idExact || !tar->IsKnownNoDelay() || // prefer no delay here - the function is executed infrequently anyway and we need subjectiveCost to be ready
      tar->destroyed || tar->vanished ||
      tar->State(leader)<TargetEnemy
      )
    {

      if( tar->side==TSideUnknown )
      {
        tar->damagePerMinute=0;
        // result of HowMuchInteresting is typically small 0..100
        tar->subjectiveCost = tar->HowMuchInteresting()*0.01f;
      }
      else if (tar->side!=TCivilian && center->IsFriendly(tar->side))
      {
        // friendly - no need to test it
        tar->damagePerMinute=0;
        // avoid to large negative to avoid rounding off the value we add
        tar->subjectiveCost = floatMin(-1e7 + tar->HowMuchInteresting(),-1.1e6);
      }
      else
      {
        // we want empty/neutral to be listed before friendly
        // reset subjective cost
        tar->damagePerMinute=0;
        tar->subjectiveCost = floatMin(-1e6 + tar->HowMuchInteresting(),-1);
      }

      continue;
    }
    else
    {
      DoAssert (center->IsEnemy(tar->side));
      // calculate damagePerMinute and subjectiveCost
      tar->damagePerMinute=grp->GetDamagePerMinute(tar);
      tar->subjectiveCost=grp->GetSubjectiveCost(tar);
    }


  }

  // sort
  ::QSort(list,TargetNormal::BetterTarget());
}

void AIGroup::RecomputeTargetSubjectiveCost(TargetNormal *target, Time until)
{
  if (_enemyRecomputeCostTime<until) return; // if recompute is already scheduled soon enough, leave it
  if (target->category!=TgtCatEnemy) return; // quick reaction is needed only for enemy targets
  // we need to do lazy recompute, target is not fully set yet
  _enemyRecomputeCostTime = until;
  //LogF("+++ %s recompute set to %.2f",cc_cast(GetDebugName()),until.toFloat());
}


float AIGroup::GetDamagePerMinute(TargetNormal *tar) const
{
  // calculate how much are assigned unit able to destroy this target
  const VehicleType *type=tar->type;
  float dpm = 0;
  for (int j=0; j<_unitSlots.Size(); j++)
  {
    const AIUnitSlot &slot = _unitSlots[j];
    AIUnit *unit = slot._unit;
    if (!unit || !unit->IsUnit()) continue;
    if (slot._assignTarget == tar)
    {
      VehicleKind kind = type->GetKind();
      float dist2 = tar->snappedPosition.Distance2(unit->Position(unit->GetFutureVisualState()));
      EntityAI *veh = unit->GetVehicle();
      if (!veh) continue;
      Threat threat = veh->GetDamagePerMinute(dist2, 1.0);
      dpm += threat[kind];
    }
  }
  return dpm;
}

float AIGroup::GetSubjectiveCost(TargetNormal *tar) const
{
#if _ENABLE_CHEATS
  bool debugMe = false;
  if (CHECK_DIAG(DECombat))
  {
    extern Ref<const TargetNormal> diagTarget;
    if (tar==diagTarget)
      debugMe = true;
  }
#endif
  const VehicleType *type=tar->type;
  float baseCost = type->GetCost();

  Object *obj = tar->idExact;
  EntityAIFull *enemy = dyn_cast<EntityAIFull>(obj);
  if (!enemy) return baseCost;

  TurretContext context;
  if (!enemy->GetPrimaryGunnerTurret(context) || !enemy->IsAbleToFire(context)) return baseCost;

  float maxCost = 0;
  for (int i=0; i<_unitSlots.Size(); i++)
  {
    AIUnit *unit = _unitSlots[i]._unit;
    if (!unit || !unit->IsUnit()) continue;

    EntityAI *veh = unit->GetVehicle();
    if (!veh) continue;

    float dist2 = veh->SnappedPosition().Distance2(tar->snappedPosition);

    float dangerCost = veh->GetDangerCost(type,dist2,1.0f,unit->GetTimeToLive());

    saturateMax(maxCost, dangerCost);
  }

  return baseCost + maxCost;
}



#define DIAGS 0


void AIGroup::UnitAssignCanceled( AIUnit *unit )
{
  Assert(unit->GetGroup() == this);
  int index = unit->ID() - 1;
  Assert(index >= 0 && index < _unitSlots.Size());
  _unitSlots[index]._assignTarget = NULL;
}

#define DEBUG_ASSIGN_TARGETS 0
void AIGroup::AssignTargets()
{
#if DEBUG_ASSIGN_TARGETS
  bool debugAT = false;
  RString dname = GetDebugName();
  LogF("%s", cc_cast(dname));
  if ( GetDebugName()==RString("B 1-1-A") )
  {
    LogF("AIGroup::AssignTargets - %s", GetName());
    debugAT = true;
  }
#endif
  AICenter *center = GetCenter();
  Assert(center);
  if (!center) return;

  AIUnit *leader = Leader();
  if (!leader) return;
  if (leader->GetPerson()->IsUserStopped()) return;
  if (leader->GetAIDisabled() & AIBrain::DATeamSwitch) return;

#if DIAGS
  LogF("Group %s - targets assignment", (const char *)GetDebugName());
#endif
  // calculate TimeToLive for all units and whole group
  AUTO_STATIC_ARRAY(float, ttl, 32);
  ttl.Resize(_unitSlots.Size());
  float groupTTL = FLT_MAX;
  int nUnits = 0;
  
  for (int i=0; i<_unitSlots.Size(); i++)
  {
    AIUnitSlot &slot = _unitSlots[i];
    AIUnit *unit = slot._unit;
    if (!unit || !unit->IsUnit())
    {
      ttl[i] = FLT_MAX;
      continue;
    }
    // update snapped position
    EntityAI *veh = unit->GetVehicle();
    if (veh) 
    {
      veh->UpdateSnappedPosition();
#if 0
      if (GetDebugName()==RString("B 1-1-A"))
         LogF("%s  %f,%f --> %f,%f", cc_cast(unit->GetDebugName()), veh->Position().X(), veh->Position().Z(), 
                                                                 veh->SnappedPosition().X(), veh->SnappedPosition().Z());
#endif
    }

    Target *at = slot._assignTarget;
    if
    (
      !at ||
      slot._assignValidUntil < Glob.time ||
      at->IsVanishedOrDestroyed() ||
      at->State(unit) < slot._assignTargetState
    )
    {
      slot._assignTarget = NULL;
      // ReCalculate damagePerMinute
      if (at) 
      { // target is unassigned and fast reassign of targets can occur
        at->UpdateWhenUnassigned(this);
      }
      nUnits++;
    }
  #if DIAGS>=2
    else
    {
  LogF
  (
    "  Unit %d - TAS = %.3f",
    unit->ID(), slot._assignValidUntil - Glob.time
  );
    }
#endif
    ttl[i] = unit->GetTimeToLive();
    saturateMin(groupTTL, ttl[i]);
#if DIAGS>=2
  LogF("  Unit %d - TTL = %.3f", unit->ID(), ttl[i]);
#endif
  }
  const float coefTTL = 0.6f;
  groupTTL *= coefTTL;
#if DIAGS>=2
  LogF("  Group's TTL = %.3f", groupTTL);
#endif

#if DEBUG_ASSIGN_TARGETS
  if (debugAT) LogF("  Group's TTL = %.3f\n", groupTTL);
#endif

  if (nUnits <= 0)
  {
#if DIAGS>=2
  LogF("  No unassigned units, exiting");
#endif
    return;	// no unassigned units
  }

  // sort group target list 

  if (IsAnyPlayerGroup())
  {
    // player has to assign targets manualy
    return;
  }

  // try to assign units to 
  const float MinTimeToLive = 1e-10;
  const float MaxTimeToLive = 2419200.0;
  // first targets in the list are the most dangerous

  for (int i=0; i<_targetList.EnemyCount(); i++)
  {
    TargetNormal *tar = _targetList.GetEnemy(i);
    if (!tar->idExact) continue;
    // consider: should we engage targets which are destroyed but we do not know it yet?
    // following condition means AI is somewhat cheating
    if (tar->idExact->IsDamageDestroyed()) continue;
    if (!tar->IsKnown() ) continue;
    // we cannot engage vanished targets
    if (tar->IsVanishedOrDestroyed()) continue;
    if (!center->IsEnemy(tar->side)) break;
    if (tar->State(leader)<TargetFireMin) continue;

    //ignore artillery targets (are assigned directly with command)
    if(tar->type->GetArtilleryTarget())
      continue;

#if DEBUG_ASSIGN_TARGETS
    if (debugAT)
      LogF("   %s damagePerMin=%.2f subjCost=%.2f", cc_cast(tar->GetDebugName()), tar->damagePerMinute, tar->GetSubjectiveCost());
#endif

#if DIAGS>=2
    OLinkPermNOArray(AIUnit) array;
    for (int j=0; j<_unitSlots.Size(); j++)
    {
      AIUnitSlot &slot = _unitSlots[j];
      AIUnit *unit = slot._unit;
      if (!unit || !unit->IsUnit()) continue;
      if (slot._assignTarget == tar) array.Add(unit);
    }
    char buffer[256];
    CreateUnitsList(array, buffer,sizeof(buffer));

    LogF
    (
      "  Target %s (side %d): cost %.0f, dpm %.0f, targeted by %s",
      (const char *)tar->idExact->GetDebugName(),tar->side,
      tar->subjectiveCost, tar->damagePerMinute, buffer
    );
    if (tar->damagePerMinute>0 && array.Size() == 0)
    {
      float dammage = GetDamagePerMinute(tar);
      LogF("Damage with no dammage source: %g",dammage);
    }
    const AITargetInfo *tgt = center->FindTargetInfo(tar->idExact);
    if( tgt )
    {
      LogF
      (
        "    - center info: side %d (%.3f), type %s (%.3f)",
        tgt->_side,tgt->FadingSideAccuracy(),
        (const char *)tgt->_type->GetDisplayName(),tgt->FadingTypeAccuracy()
      );
    }
#endif
    
    float enemyTTL = MaxTimeToLive;
    if (tar->damagePerMinute > 0) 
    {
      enemyTTL = 60 * tar->type->GetArmor() / tar->damagePerMinute;
      saturate(enemyTTL, MinTimeToLive, MaxTimeToLive);
    }
    
    bool laserTarget = tar->type->GetLaserTarget();
    bool nvTarget = tar->type->GetNvTarget();

#if DIAGS
  if (enemyTTL > groupTTL)
    LogF
    (
      "  Try to cover target %s (side %d)",
      (const char *)tar->idExact->GetDebugName(),tar->side
    );
#endif

#if DEBUG_ASSIGN_TARGETS
  if (debugAT) 
    if (enemyTTL > groupTTL)
      LogF
      (
      "  Try to cover target %s (side %d)",
      (const char *)tar->idExact->GetDebugName(),tar->side
      );
#endif

    struct TargetForUnit
    {
      Target *tar;
      OLinkPermNOArray(AIUnit) list;
      bool listAttack;
      AIGroup *grp;
      
      TargetForUnit(Target *tar, AIGroup *grp)
      :tar(tar),grp(grp){}
      
      void Flush()
      {
        grp->SendTarget(tar, listAttack, false, list);
        grp->_lastSendTargetTime = Glob.time;
        list.Resize(0);
      }
      bool Add(AIUnit *unit, bool attack, float dt)
      {
        // if we are not compatible with already open list, terminate the loop - one target only is allowed
        if (list.Size()>0 && attack!=listAttack)
        {
          return false;
        }
        
        // add a new unit into the list
        list.Add(unit);
        listAttack = attack;
        
        int bestI = unit->ID() - 1;
        AIUnitSlot &bestSlot = grp->_unitSlots[bestI];
        bestSlot._assignTarget = tar;
        bestSlot._assignTargetState = tar->State(unit);
        bestSlot._assignValidUntil = Glob.time+dt;

        // no hide nor attack any other targets- "IGNORE" behaviour
        unit->GetSubgroup()->ClearAttackCommands(tar);
        
        return true;
      }
      
      ~TargetForUnit()
      {
        Flush();
      }
    };
    
    TargetForUnit target(tar,this);
    
    bool someAssigned = false;
    int fireteamCountLeft = 0;
    int fireteamCount = 0;
    while (enemyTTL > groupTTL || fireteamCountLeft>0)
    {
      // select best unit
      float maxSurplus = -FLT_MAX;
      AIUnit *bestUnit = NULL;
      FireResult bestResult;
      bool attack = false;

      for (int j=0; j<_unitSlots.Size(); j++)
      {
        AIUnitSlot &slot = _unitSlots[j];
        AIUnit *unit = slot._unit;
        if (!unit || !unit->IsUnit()) continue;
        if (slot._assignTarget) continue; // if the unit has already target assigned, skip it
        if (unit->IsHoldingFire()) continue; // if the unit is not allow to fire, no use assigning targets to it
        if (unit->GetAIDisabled()&AIUnit::DATarget) continue;
        if (fireteamCount>0 && unit->GetVehicleIn()) continue; // include only soldiers in fire teams
        // note: group leader may assign target to himself
        TurretContext context;
        EntityAIFull *veh = unit->GetVehicle();
        // if the unit has no weapons or is not able to use them, skip it
        if (!veh || !veh->GetPrimaryGunnerTurret(context) || !veh->IsAbleToFire(context)) continue;

        bool canFire = true;
        float attackCoef = unit->IsKeepingFormation() ? 1.0f/16 : 1.0f/2;
        float attackCost = unit->IsKeepingFormation() ? 10000 : 1000;

        bool canAttack = (
          _attackEnabled &&
          !CommandSent(unit, Command::Attack) &&
          !CommandSent(unit, Command::AttackAndFire) &&
          !CommandSent(unit, Command::FireAtPosition) &&
          !CommandSent(unit, Command::GetOut) &&
          !CommandSent(unit, Command::GetIn) &&
          !CommandSent(unit, Command::Heal) &&
          !CommandSent(unit, Command::HealSoldier) &&
          !CommandSent(unit, Command::FirstAid) &&
          !CommandSent(unit, Command::RepairVehicle) &&
          !CommandSent(unit, Command::Repair) &&
          !CommandSent(unit, Command::Refuel) &&
          !CommandSent(unit, Command::Rearm) &&
          !CommandSent(unit, Command::TakeBag) &&
          !CommandSent(unit, Command::Assemble) &&
          !CommandSent(unit, Command::DisAssemble) &&
          !CommandSent(unit, Command::Action)
        );
  
        // TODO: delay before attacking

        // before issuing Attack there is delay
        // unit is assigned to target but attack is not issued
        // if unit is not able to fire during this time
        // attack is issued (if there is still some surplus from using it)
        if (unit==leader && unit->IsKeepingFormation())
        {
          canAttack=false;
        }
        if (!canAttack && fireteamCount>0) continue; // who cannot attack, cannot assist in an attack as well
        
        FireResult result;
        // special case handling: laser designated targets assigned to unit able to engage them
        if ((laserTarget && veh->GetType()->GetLaserScanner()) || (nvTarget && veh->GetType()->GetNVScanner()))
        {
          // we should check the vehicle has some weapons suitable for this purpose
          if (veh->WhatAttackResult(result, *tar, context, ttl[j]))
          {
            float surplus = result.Surplus();
            if (surplus>maxSurplus)
            {
              maxSurplus = surplus;
              bestUnit = unit;
              bestResult = result;
              // TODO: attack or no attack? Currently we are only assigning a target
              // unit able to acquire laser targets is heli ar plane anyway, and it will engage as needed
              attack = false;
            }
            // no need to test fire or attack on this one
            continue;
          }
        }
        
        if (canFire)
        {
          if (veh->WhatFireResult(result, *tar, context, ttl[j])==EntityAIFull::FPCan)
          {
            float surplus = result.Surplus();
            if (surplus > maxSurplus)
            {
              maxSurplus = surplus;
              bestUnit = unit;
              bestResult = result;
              attack = false;
            }
          }
          else
          {
#if DIAGS
  LogF("  - Unit %d - fire is not possible", unit->ID());
#endif
          }
        }
        else
        {
#if DIAGS
  LogF("  - Unit %d - cannot fire", unit->ID());
#endif
        }
        if (canAttack)
        {
          if (veh->WhatAttackResult(result, *tar, context, ttl[j]))
          {
            float surplus = result.Surplus()*attackCoef-attackCost;
            if (surplus > maxSurplus)
            {
              maxSurplus = surplus;
              bestUnit = unit;
              bestResult = result;
              attack = true;
            }
          }
          else
          {
#if DIAGS
  LogF("  - Unit %d - attack is not possible", unit->ID());
#endif
          }
        } // if( canAttack )
        else
        {
#if DIAGS
  LogF(
    "  - Unit %d - attack is forbidden%s%s%s%s", unit->ID(),
    unit->IsKeepingFormation() ? " Semaphore" : "",
    CommandSent(unit, Command::Attack) ? " Attack" : "",
    CommandSent(unit, Command::AttackAndFire) ? " Attack" : "",
    CommandSent(unit, Command::GetIn) ? " GetIn" : "",
    CommandSent(unit, Command::GetOut) ? " GetOut" : ""
  );
#endif
        }
      }
      if (!bestUnit) break;

      // update ttl
      VehicleKind kind = tar->type->GetKind();
      float dist2 = tar->position.Distance2(bestUnit->Position(bestUnit->GetFutureVisualState()));
      Threat threat = bestUnit->GetVehicle()->GetDamagePerMinute(dist2, 1.0);
      tar->damagePerMinute += threat[kind];
      if(tar->damagePerMinute > 0)
      {
        enemyTTL = 60 * tar->type->GetArmor() / tar->damagePerMinute;
      }

#if DIAGS
  LogF("    Unit %d assigned", bestUnit->ID());
#endif
      {
        const float coefTimeout = 15.0f;
        float dt = coefTimeout * enemyTTL;
        float minTimeout = bestUnit->GetVehicle()->GetMinFireTime();
        if (attack)
        {
          minTimeout *= 3; // for soldier this should give attack time at least 15 seconds
          // engaging takes a lot of time - assume at least 60 seconds
          saturateMax(minTimeout,60);
        }
        saturate(dt, minTimeout, 120);

//LogF("Unit %s %s - target assigned %s", (const char *)bestUnit->GetDebugName(), (const char *)bestUnit->GetPerson()->GetInfo()._name, attack ? "(attack)" : "");
        // send AssignTarget to radio
        

        if (!target.Add(bestUnit,attack,dt))
        {
          someAssigned = true;
          break;
        }
        else
        {
          if (bestUnit->GetVehicleIn()) fireteamCountLeft = 0; // in vehicle: mark the fire team as full, no assist needed
          else
          {
            // when we are the first assigned, request an assistance
            if (fireteamCount==0) fireteamCountLeft = 2;
            fireteamCountLeft--;
          }
          fireteamCount++; // count units participating in the fire team
        }

#if DIAGS
  LogF("    - fire for %.1f sec",dt);
#endif
      }

      if (--nUnits <= 0) return;
    } // while (ttl)
    if (someAssigned) break; // only one target per function call
    // nobody assigned for the target
    // if the target is aware of us and it can harm us
    // we should better run away

#if DIAGS
    // how can we know if the target is aware of us?
    // easy test - check if he knows about the leader
    // complete test - check if he knows about any unit
    LogF(
      "  target %s not covered",
      (const char *)tar->idExact->GetDebugName()
    );
#endif

  } // for (target)

  // all units left should hide
  // select hide command target

  bool groupEnableHideEnemy = !GetFlee();
  bool groupEnableHideUnknown = groupEnableHideEnemy;

  if (groupEnableHideEnemy)
  {
    // check for enemy / unknown targets
    groupEnableHideEnemy = false;
    groupEnableHideUnknown = false;
    // note: Enemy contains unknown enemy, but not unknown friendly
    for (int i=0; i<_targetList.EnemyCount(); i++)
    {
      TargetNormal *tar = _targetList.GetEnemy(i);
      if (!tar->idExact) continue;
      if (tar->idExact->IsDamageDestroyed()) continue;
      if (!tar->IsKnown() ) continue;
      if (center->IsEnemy(tar->side))
      {
        groupEnableHideEnemy = true;
        groupEnableHideUnknown = true;
        break;
      }
      else if (tar->side==TSideUnknown)
      {
        groupEnableHideUnknown = true;
      }
    }
  }

  for (int j=0; j<_unitSlots.Size(); j++)
  {
    AIUnitSlot &slot = _unitSlots[j];
    AIUnit *unit = slot._unit;
    if (!unit || !unit->IsUnit()) continue;
    if (IsAnyPlayerGroup() && unit->IsGroupLeader()) continue;
    
    // check if there is some hide command issued
    // if there is valid hide, leave it

    AISubgroup *subgrp = unit->GetSubgroup();
    bool oldHideTgt = subgrp->CheckHide();
    bool enableHide=false;

    if (slot._assignTarget == NULL && unit->GetCombatMode() != CMCareless)
    {
      if
      (
        groupEnableHideEnemy ||
        // in stealth hide also from unknown targets
        groupEnableHideUnknown && unit->GetCombatMode() == CMStealth
      )
      {
        if (!unit->IsKeepingFormation())
        {
          // TODO: enable hiding of other kinds of vehicles
          if( unit->IsFreeSoldier() )
          {
            enableHide =
            (
              !CommandSent(unit, Command::Attack) &&
              !CommandSent(unit, Command::AttackAndFire) &&
              !CommandSent(unit, Command::FireAtPosition) &&
              !CommandSent(unit, Command::GetOut) &&
              !CommandSent(unit, Command::GetIn) &&
              !CommandSent(unit, Command::Heal) &&
              !CommandSent(unit, Command::HealSoldier) &&
              !CommandSent(unit, Command::FirstAid) &&
              !CommandSent(unit, Command::RepairVehicle) &&
              !CommandSent(unit, Command::Repair) &&
              !CommandSent(unit, Command::Refuel) &&
              !CommandSent(unit, Command::Rearm) &&
              !CommandSent(unit, Command::Action) &&
              !CommandSent(unit, Command::TakeBag) &&
              !CommandSent(unit, Command::Assemble) &&
              !CommandSent(unit, Command::DisAssemble) &&
              !CommandSent(unit, Command::Hide)
            );
          }
        }
      }
    }

    // select hide target

    if( oldHideTgt == enableHide ) continue;

    // hide target changed - issue command
    subgrp->ClearAttackCommands(NULL);

    /*
    if( enableHide )
    {
      Assert(!GetFlee());
      Command cmd;
      cmd._message = Command::Hide;
      cmd._targetE = NULL;
      cmd._destination = unit->Position();
      cmd._id = GetNextCmdId();

      IssueAutoCommand(cmd, unit);
    }
    */
  }
}

#define	ATTACK_DISTANCE		250.0
Threat AIGroup::GetAttackInfluence()
{
  Threat sum;
  int i;
  for (i=0; i<NUnits(); i++)
  {
    AIUnit *unit = GetUnit(i);
    if (!unit) continue;
    const VehicleType *type = unit->GetPerson()->GetType();
    sum += type->GetDamagePerMinute(ATTACK_DISTANCE, 1.0);
    if (!unit->IsSoldier())
    {	// driver
      type = unit->GetVehicle()->GetType();
      sum += type->GetDamagePerMinute(ATTACK_DISTANCE, 1.0);
    }
  }
  for (int v=0; v<_vehicles.Size(); v++)
  {
    Transport *veh = _vehicles[v];
    if (!veh || veh->IsDamageDestroyed())
      continue;
    sum += veh->GetType()->GetDamagePerMinute(ATTACK_DISTANCE, 1.0);
  }
  return sum;	// return total dammage per minute
}

Threat AIGroup::GetDefendInfluence() 
{
  Threat sum;
  int i;
  for (i=0; i<NUnits(); i++)
  {
    AIUnit *unit = GetUnit(i);
    if (!unit)
      continue;
    const VehicleType *type = unit->GetPerson()->GetType();
    sum[type->GetKind()] += type->GetArmor();
    if (!unit->IsSoldier())
    {	// driver
      type = unit->GetVehicle()->GetType();
      sum[type->GetKind()] += type->GetArmor();
    }
  }
  for (int v=0; v<_vehicles.Size(); v++)
  {
    Transport *veh = _vehicles[v];
    if (!veh || veh->IsDamageDestroyed())
      continue;
    const VehicleType *type = veh->GetType();
    sum[type->GetKind()] += type->GetArmor();
  }
  return sum;	// return armor
}


int AIGroup::FindNearestPreviousWaypoint(int index, Vector3Par pos) const
{
  if (index<0) return -1;
  float minDist2 = FLT_MAX;
  int iBest = -1;
  for (int i=0; i<index; i++)
  {
    float dist2 = GetWaypoint(i).position.DistanceXZ2(pos);
    if (dist2 < minDist2)
    {
      iBest = i;
      minDist2 = dist2;
    }
  }
  return iBest;
}

void AIGroup::AddFirstWaypoint(Vector3Par pos)
{
  _wp.Insert(0);
  WaypointInfo &wInfo = _wp[0];
  wInfo.InitWp();
  wInfo.position = pos;
  wInfo.type = ACMOVE;
}

int AIGroup::GetCurrentWaypoint() const
{
  if (!GetCurrent() || !GetCurrent()->_fsm) return -1;
  return GetCurrent()->_fsm->Var(0);
}

void AIGroup::SetCurrentWaypoint(int index, bool applyEffects)
{
  if (!GetCurrent() || !GetCurrent()->_fsm) return;
  int &oldIndex = GetCurrent()->_fsm->Var(0);
  if (index == oldIndex) return; // need not to be set

  if (applyEffects) ApplyEffects(this, oldIndex);
  oldIndex = index;

  // reset the FSM to the initial state
  AIGroupContext context(this);
  context._task = GetCurrent()->_task;
  context._fsm = GetCurrent()->_fsm;
  void ResetWaypoint(AIGroupContext *context);
  ResetWaypoint(&context);
}

Vector3 AIGroup::GetWaypointPosition(int index) const
{
  DoAssert(index < NWaypoints());
  const WaypointInfo &wInfo = GetWaypoint(index);

  EntityAI *target = NULL;
  if (wInfo.id >= 0 && wInfo.id < vehiclesMap.Size())
    target = vehiclesMap[wInfo.id];
  else if (!wInfo.idObject.IsNull())
  {
    target = dyn_cast<EntityAI>(GLOB_LAND->GetObject(wInfo.idObject).GetLink());
    // FIX: waypoint attached to the wrong (for example deleted) object
    if (target) return target->FutureVisualState().Position();
  }
  else
  {
    int &current = GetCurrent()->_fsm->Var(0);
    if (index == current) target = GetCurrent()->_task->_target;
  }
  
  if (target)
  {
    const AITargetInfo *info = GetCenter()->FindTargetInfo(target);
    if (info) return info->_realPos;
  }

  return wInfo.position;
}

bool AIGroup::GetWaypointVisible(int index)
{
  if(index>=0 && index<NWaypoints()) return GetWaypoint(index).visible;
  return false;

}

void AIGroup::CopyWaypoints(const AIGroup *source)
{
  _wp = source->_wp;

  // force the group to start processing of WP #1
  if (GetCurrent() && GetCurrent()->_fsm)
  {
    int &index = GetCurrent()->_fsm->Var(0);
    index = 1;

    // reset the FSM to the initial state
    AIGroupContext context(this);
    context._task = GetCurrent()->_task;
    context._fsm = GetCurrent()->_fsm;
    void ResetWaypoint(AIGroupContext *context);
    ResetWaypoint(&context);
  }
}

void AIGroup::Move(AISubgroup *who, Vector3Val destination, Command::Discretion discretion, float precision)
{
  if (!who) return;
  if (who->Leader())
  {
    Vector3Val pos = who->Leader()->Position(who->Leader()->GetFutureVisualState());
    float dist = 200;
    if ((pos - destination).SquareSizeXZ() > Square(dist))
    {
      for (int i=0; i<who->NUnits(); i++)
      {
        AIUnit *unit = who->GetUnit(i);
        if (unit) unit->OrderGetIn(true);
      }
    }
    AssignVehicles();
    GetInVehicles();
  }

  #if _ENABLE_CHEATS
    if (who->Leader() && !who->Leader()->CheckEmpty(destination))
    {
      RptF(
        "%s: move for %s to %.2f,%.2f impossible",
        cc_cast(GetDebugName()),cc_cast(who->GetDebugName()),destination.X(),destination.Z()
      );
    }
  #endif

  Command cmd;
  cmd._message = Command::Move;
  cmd._destination = destination;
  cmd._discretion = discretion;
  cmd._precision = precision;
  cmd._context = Command::CtxMission;
  if (who == MainSubgroup())
  {
    SendCommand(cmd);
  }
  else
  {
    OLinkPermNOArray(AIUnit) list;
    who->GetUnitsList(list);
    SendCommand(cmd, list);
  }
}

void AIGroup::Wait(AISubgroup *who, Time until, Command::Discretion discretion)
{
  Command cmd;
  cmd._message = Command::Wait;
  cmd._time = until;
  cmd._discretion = discretion;
  cmd._context = Command::CtxMission;
  if (who == MainSubgroup())
  {
    SendCommand(cmd);
  }
  else
  {
    OLinkPermNOArray(AIUnit) list;
    who->GetUnitsList(list);
    SendCommand(cmd, list);
  }
}

void AIGroup::Fire(AISubgroup *who, TargetType *target, int weapon, Command::Discretion discretion)
{
  Command cmd;
  cmd._message = Command::Fire;
  cmd._target = target;
  cmd._intParam = weapon;
  cmd._discretion = discretion;
  cmd._context = Command::CtxMission;
  if (who == MainSubgroup())
  {
    SendCommand(cmd);
  }
  else
  {
    OLinkPermNOArray(AIUnit) list;
    who->GetUnitsList(list);
    SendCommand(cmd, list);
  }
}

void AIGroup::GetIn(AISubgroup *who, TargetType *target, Command::Discretion discretion)
{
  Command cmd;
  cmd._message = Command::GetIn;
  cmd._target = target;
  cmd._discretion = discretion;
  cmd._movement = Command::MoveFast;
  cmd._context = Command::CtxMission;
  if (who == MainSubgroup())
  {
    SendCommand(cmd);
  }
  else
  {
    OLinkPermNOArray(AIUnit) list;
    who->GetUnitsList(list);
    SendCommand(cmd, list);
  }
}

void AIGroup::MoveUnit(AIUnit *who, Vector3Val destination, Command::Discretion discretion, float precision)
{
  Command cmd;
  cmd._message = Command::Move;
  cmd._destination = destination;
  cmd._discretion = discretion;
  cmd._precision = precision;
  SendAutoCommandToUnit(cmd, who, false);
}

float AIGroup::UpdateAndGetThreshold()
{
  if (Glob.time >= _thresholdValid)
  {
    _threshold = GRandGen.RandomValue();
    _thresholdValid = Glob.time + 60.0f;
  }
  return _threshold;
}

bool AIGroup::GetAllDone() const
{
  if (IsPlayerGroup()) return false;

  // query group status
  if( !_radio.Done() )
  {
    return false;
  }
  // check if main subgroup completed
  if( MainSubgroup()->HasCommand() ) return false;
  AIUnit *leader = Leader();
  // group may not be done if leader is not ready
  if (leader && !leader->LSIsAlive()) return false;
  return true;
}

void AIGroup::AllGetOut()
{
  for (int i=0; i<_unitSlots.Size(); i++)
  {
    AIUnit *unit = _unitSlots[i]._unit;
    if (unit) unit->AllowGetIn(false);
  }
}

void AIGroup::CargoGetOut()
{
  for (int i=0; i<_unitSlots.Size(); i++)
  {
    AIUnit *unit = _unitSlots[i]._unit;
    if (unit && unit->IsInCargo()) unit->AllowGetIn(false);
  }
}

#define COMMAND_TIMEOUT			480.0		// 8 min

#define FUEL_NEAR			100.0f
#define FUEL_FAR			500.0f

const AITargetInfo *AIGroup::FindRefuelPosition(AIUnit::ResourceState state) const
{
  if (state == AIUnit::RSNormal) return NULL;
  Assert(Leader());
  
  // find fuel in close range
  const AITargetInfo *depot = NULL;
  float dist2Depot = FLT_MAX;
  const AITargetInfo *truck = NULL;
  float dist2Truck = FLT_MAX;

  for (int i=0; i<GetCenter()->NTargets(); i++)
  {
    const AITargetInfo &info = GetCenter()->GetTarget(i);
    if (info._destroyed)
      continue;
    if (!(info._side == TCivilian) && !(GetCenter()->IsFriendly(info._side)))
      continue;
    if (info._type->GetMaxFuelCargo() <= 0)
      continue;
    EntityAI *veh = info._idExact;
    if (!veh) continue;
    if (veh->GetFuelCargo() <= 0) continue;
    if( GetCenter()->GetExposurePessimistic(info._realPos)>0 ) continue;
    float dist2 = (Leader()->Position(Leader()->GetFutureVisualState()) - info._realPos).SquareSizeXZ();
    
    if (info._type->IsKindOf(GLOB_WORLD->Preloaded(VTypeStatic)))
    {
      if (dist2 < dist2Depot)
      {
        dist2Depot = dist2;
        depot = &info;
      }
    }
    else
    {
      if (dist2 < dist2Truck)
      {
        dist2Truck = dist2;
        truck = &info;
      }
    }
  }

  if (depot && dist2Depot < Square(FUEL_NEAR))
  {
    // refuel in near depot
    return depot;
  }
  if (truck && dist2Truck < Square(FUEL_NEAR))
  {
    return truck;
  }

  if (state == AIUnit::RSLow) return NULL;

  // find fuel in wider range
  if (depot && dist2Depot < Square(FUEL_FAR))
  {
    // refuel in far depot
    return depot;
  }
  if (truck && dist2Truck < Square(FUEL_FAR))
  {
    // refuel in far truck
    return truck;
  }

  // fuel not found
  return NULL;
}

bool AIGroup::FindRefuelPosition(Command &cmd) const
{
  const AITargetInfo *target = FindRefuelPosition(AIUnit::RSCritical);
  if (!target) return false;

  cmd._destination = target->_realPos;
  cmd._target = target->_idExact;
  cmd._time = Glob.time + COMMAND_TIMEOUT;
  return true;
}

/*!
\patch 1.16 Date 08/14/2001 by Jirka
- Fixed: Unit repeats ask for support
*/
bool AIGroup::CheckFuel()
{
  Assert(Leader());
  if (IsAnyPlayerGroup())
    return true;

  bool ok = true;
  AIUnit::ResourceState stateMax = AIUnit::RSNormal;
  for (int i=0; i<_unitSlots.Size(); i++)
  {
    AIUnit *unit = _unitSlots[i]._unit;
    if (!unit) continue;
    AIUnit::ResourceState state = GetFuelStateReported(unit);
    if (state == AIUnit::RSNormal) continue;

    ok = false;
    if (unit->GetCombatMode() == CMCareless) continue;
    // check both group and center radio channel
    if (CommandSent(unit, Command::Refuel)) continue;
    if (CommandSent(unit, Command::Support)) continue;
    if (state > stateMax) stateMax = state;
  }

  const AITargetInfo *target = FindRefuelPosition(stateMax);

  if (target)
  {
    bool channelCenter = false;
    EntityAI *veh = target->_idExact;
    if (veh)
    {
      AIBrain *unit = veh->CommanderUnit();
      if (unit && unit->LSIsAlive())
        channelCenter = unit->GetGroup() != this;
    }

    Command cmd;
    cmd._message = Command::Refuel;
    cmd._destination = target->_realPos;
    cmd._target = target->_idExact;
    cmd._time = Glob.time + COMMAND_TIMEOUT;

    for (int i=0; i<_unitSlots.Size(); i++)
    {
      AIUnit *unit = _unitSlots[i]._unit;
      if (!unit) continue;
      if (unit->GetCombatMode() == CMCareless) continue;
      // check both group and center radio channel
      if (CommandSent(unit, Command::Refuel)) continue;
      if (CommandSent(unit, Command::Support)) continue;

      if (unit->GetFuelState() >= stateMax)
      {
        SendAutoCommandToUnit(cmd, unit, true, channelCenter);
      }
    }
  }
  else
  {
    if (stateMax == AIUnit::RSCritical && !IsAnyPlayerGroup())
    {
      Assert(_center);
      // FIX
      if (_center->GetCommandRadio().Done() && _center->CanSupport(ATRefuel) && !_center->WaitingForSupport(this, ATRefuel))
      {
        _center->GetCommandRadio().Transmit(new RadioMessageSupportAsk(this, ATRefuel));
      }
    }
  }

  return ok;
}

#define REPAIR_NEAR			100.0f
#define REPAIR_FAR			500.0f

const AITargetInfo *AIGroup::FindRepairPosition(AIUnit::ResourceState state) const
{
  if (state == AIUnit::RSNormal) return NULL;
  Assert(Leader());

  // find repair in close range
  const AITargetInfo *depot = NULL;
  float dist2Depot = FLT_MAX;
  const AITargetInfo *truck = NULL;
  float dist2Truck = FLT_MAX;

  for (int i=0; i<GetCenter()->NTargets(); i++)
  {
    const AITargetInfo &info = GetCenter()->GetTarget(i);
    if (!info._type) continue;
    if (!(info._side == TCivilian) && !(GetCenter()->IsFriendly(info._side)))
      continue;
    if (info._type->GetMaxRepairCargo() <= 0)
      continue;
    EntityAI *veh = info._idExact;
    if (!veh) continue;
    if (veh->GetRepairCargo() <= 0) continue;
    if( GetCenter()->GetExposurePessimistic(info._realPos)>0 ) continue;
    float dist2 = (Leader()->Position(Leader()->GetFutureVisualState()) - info._realPos).SquareSizeXZ();

    if (info._type->IsKindOf(GLOB_WORLD->Preloaded(VTypeStatic)))
    {
      if (dist2 < dist2Depot)
      {
        dist2Depot = dist2;
        depot = &info;
      }
    }
    else
    {
      if (dist2 < dist2Truck)
      {
        dist2Truck = dist2;
        truck = &info;
      }
    }
  }

  if (depot && dist2Depot < Square(REPAIR_NEAR))
  {
    // repair in near depot
    return depot;
  }
  if (truck && dist2Truck < Square(REPAIR_NEAR))
  {
    // repair in near truck
    return truck;
  }

  if (state == AIUnit::RSLow) return NULL;

  // find repair in wider range
  if (depot && dist2Depot < Square(REPAIR_FAR))
  {
    // repair in far depot
    return depot;
  }
  if (truck && dist2Truck < Square(REPAIR_FAR))
  {
    // repair in far truck
    return truck;
  }

  // repair not found
  return NULL;
}

bool AIGroup::FindRepairPosition(Command &cmd) const
{
  const AITargetInfo *target = FindRepairPosition(AIUnit::RSCritical);
  if (!target) return false;

  cmd._destination = target->_realPos;
  cmd._target = target->_idExact;
  cmd._time = Glob.time + COMMAND_TIMEOUT;
  return true;
}

bool AIGroup::CheckArmor()
{
  Assert(Leader());
  if (IsAnyPlayerGroup())
    return true;

  bool ok = true;
  AIUnit::ResourceState stateMax = AIUnit::RSNormal;
  for (int i=0; i<_unitSlots.Size(); i++)
  {
    AIUnit *unit = _unitSlots[i]._unit;
    if (!unit) continue;
    AIUnit::ResourceState state = GetDamageStateReported(unit);
    if (state == AIUnit::RSNormal) continue;

    ok = false;
    if (unit->GetCombatMode() == CMCareless) continue;
    // check both group and center radio channel
    if (CommandSent(unit, Command::Repair)) continue;
    if (CommandSent(unit, Command::Support)) continue;

    if (state > stateMax) stateMax = state;

    //check if assigned enginner is working
    EntityAIFull *attendant = (unit->GetVehicleIn())? unit->GetVehicleIn()->AssignedAttendant() : NULL; 
    if (attendant  && attendant->CommanderUnit() && attendant->CommanderUnit()->GetUnit())
    {
      AIUnit *attendantUnit=attendant->CommanderUnit()->GetUnit();
      if(attendantUnit->GetLifeState() == LifeStateUnconscious) 
        unit->GetVehicle()->SetAssignedAttendant(NULL);
    }
    //assigned vehicles are not neccessarily part of group 
    attendant = (unit->VehicleAssigned())? unit->VehicleAssigned()->AssignedAttendant() : NULL; 
    if (attendant  && attendant->CommanderUnit() && attendant->CommanderUnit()->GetUnit())
    {
      AIUnit *attendantUnit=attendant->CommanderUnit()->GetUnit();
      if(attendantUnit->GetLifeState() == LifeStateUnconscious) 
        unit->VehicleAssigned()->SetAssignedAttendant(NULL);
    }
  }

   
  for (int i=0; i<_unitSlots.Size(); i++)
  {
    Transport *transport = NULL;
    AIUnit *unit = _unitSlots[i]._unit;
    if (!unit) continue;

    if(unit->GetVehicleIn() && !unit->GetVehicleIn()->AssignedAttendant() && unit->GetVehicleIn()->GetMaxHitCont() > 0.7f) 
    {
      transport = unit->GetVehicleIn();
    }
    else if(unit->VehicleAssigned() && !unit->VehicleAssigned()->AssignedAttendant() && unit->VehicleAssigned()->GetMaxHitCont() > 0.7f)  
    {
      transport = unit->VehicleAssigned();
    }
    if(!transport) continue;

    AIUnit *engineer = FindEngineer(transport);
    if(!engineer) continue;

    bool channelCenter = false;
    if (engineer->LSIsAlive())
      channelCenter = engineer->GetGroup() != this;

    Command cmd;
    cmd._message = Command::RepairVehicle;
    cmd._destination = transport->FutureVisualState().Position();
    cmd._target = transport;
    cmd._time = Glob.time + COMMAND_TIMEOUT;
    transport->SetAssignedAttendant(engineer->GetVehicle());
    // transport->SetAssignedMedic(engineer);
    SendAutoCommandToUnit(cmd, engineer, true, channelCenter);
  }
  
  const AITargetInfo *target = FindRepairPosition(stateMax);

  if (target)
  {
    bool channelCenter = false;
    EntityAI *veh = target->_idExact;
    if (veh)
    {
      AIBrain *unit = veh->CommanderUnit();
      if (unit && unit->LSIsAlive())
        channelCenter = unit->GetGroup() != this;
    }

    Command cmd;
    cmd._message = Command::Repair;
    cmd._destination = target->_realPos;
    cmd._target = target->_idExact;
    cmd._time = Glob.time + COMMAND_TIMEOUT;

    for (int i=0; i<_unitSlots.Size(); i++)
    {
      AIUnit *unit = _unitSlots[i]._unit;
      if (!unit) continue;
      if (unit->GetCombatMode() == CMCareless) continue;
      // check both group and center radio channel
      if (CommandSent(unit, Command::Repair)) continue;
      if (CommandSent(unit, Command::Support)) continue;
      if (unit->GetVehicleIn() && unit->GetVehicleIn()->AssignedAttendant()) continue;

      if (unit->GetArmorState() >= stateMax)
      {
        SendAutoCommandToUnit(cmd, unit, true, channelCenter);
      }
    }
  }
  else
  {
    if (stateMax == AIUnit::RSCritical && !IsAnyPlayerGroup())
    {
      Assert(_center);
      if (_center->GetCommandRadio().Done() && _center->CanSupport(ATRepair) && !_center->WaitingForSupport(this, ATRepair))
      {
        _center->GetCommandRadio().Transmit(new RadioMessageSupportAsk(this, ATRepair));
      }
    }
  }

  return ok;
}

#define HOSPITAL_NEAR			100.0f
#define HOSPITAL_FAR			500.0f

const AITargetInfo *AIGroup::FindHealPosition(AIUnit::ResourceState state, AIUnit *unit) const
{
  if (state == AIUnit::RSNormal) return NULL;
  Assert(Leader());

  // find hospital / ambulance in close range
  const AITargetInfo *depot = NULL;
  float dist2Depot = FLT_MAX;
  const AITargetInfo *truck = NULL;
  float dist2Truck = FLT_MAX;

  Vector3 pos = unit ? unit->Position(unit->GetFutureVisualState()) : Leader()->Position(Leader()->GetFutureVisualState());
  AIGroup *group = unit->GetGroup();
  
  for (int i=0; i<GetCenter()->NTargets(); i++)
  {
    const AITargetInfo &info = GetCenter()->GetTarget(i);
    if (info._destroyed)
      continue;
    const VehicleType *type = info._idExact ? safe_cast<const VehicleType*>(info._idExact->GetType()) : safe_cast<const VehicleType*>(info._type);
    if (!type) continue;
    if (!type->IsAttendant())
      continue;
    if (!(info._side == TCivilian) && !(GetCenter()->IsFriendly(info._side)))
      continue;
    if (unit && info._idExact == unit->GetPerson()) continue;
    if (unit && info._idExact == unit->GetVehicleIn()) continue;
    if( GetCenter()->GetExposureOptimistic(info._realPos)>50.0f ) continue;
    float dist2 = (pos - info._realPos).SquareSizeXZ();

    if (type->IsKindOf(GLOB_WORLD->Preloaded(VTypeStatic)))
    {
      if (dist2 < dist2Depot && unit->GetLifeState()!=LifeStateUnconscious)
      {
        dist2Depot = dist2;
        depot = &info;
      }
    }
    else
    {
      if (dist2 < dist2Truck)
      {
        EntityAI *entity = info._idExact;
        AIBrain *brain =  entity->CommanderUnit();
        if(brain)
        {
          //medics from another group can not be commanded 
          if(brain->IsFreeSoldier() && brain->GetGroup() == group)
          {
            if (!(group->CommandSent( brain->GetUnit(), Command::HealSoldier))  && brain->GetLifeState()!= LifeStateUnconscious)
            {
              dist2Truck = dist2;
              truck = &info;
            }
          }
          else if(!brain->IsFreeSoldier() && unit->GetLifeState()!=LifeStateUnconscious)
          {//ambulance
            dist2Truck = dist2;
            truck = &info;
          }
        }
      }
    }
  }

  if (depot && dist2Depot < Square(HOSPITAL_NEAR))
  {
    // heal in near hospital
    return depot;
  }
  if (truck && dist2Truck < Square(HOSPITAL_NEAR))
  {
    // heal in near ambulance
    return truck;
  }

  if (state == AIUnit::RSLow) return NULL;

  // find hospital / ambulance in wider range
  if (depot && dist2Depot < Square(HOSPITAL_FAR))
  {
    // heal in far hospital
    return depot;
  }
  if (truck && dist2Truck < Square(HOSPITAL_FAR))
  {
    // heal in far ambulance
    return truck;
  }

  // hospital / ambulance not found
  return NULL;
}

bool AIGroup::FindHealPosition(Command &cmd) const
{
  const AITargetInfo *target = FindHealPosition(AIUnit::RSCritical);
  if (!target) return false;

  cmd._destination = target->_realPos;
  cmd._target = target->_idExact;
  cmd._time = Glob.time + COMMAND_TIMEOUT;
  return true;
}

const AIUnit *AIGroup::FindFirstAid(AIUnit::ResourceState state, AIUnit *unit) const
{
  if (state == AIUnit::RSNormal) return NULL;
  Assert(Leader());

  // find nearest soldier
  const AIUnit *firstAidUnit = NULL;
  float dist2Aid = FLT_MAX;

  Vector3 pos = unit ? unit->Position(unit->GetFutureVisualState()) : Leader()->Position(Leader()->GetFutureVisualState());
  AIGroup *group = unit->GetGroup();

  for (int i=0; i<group->NUnits(); i++)
  {
    AIUnit *u = group->GetUnit(i);
    if (!u || !u->GetVehicle()) continue;
    if (u->GetLifeState() == LifeStateUnconscious) continue;
    if (u == group->Leader()) continue;

    if (group->CommandSent( u, Command::HealSoldier)) continue;
    if (group->CommandSent( u, Command::FirstAid)) continue;

    if (u == unit) continue;
    float dist2 = (pos - u->Position(u->GetFutureVisualState())).SquareSizeXZ();

    //avoid crawling medic if possible
    if( firstAidUnit && firstAidUnit->GetVehicle()->IsAbleToStand()
      && !u->GetVehicle()->IsAbleToStand()) continue;

    if (u->IsFreeSoldier())
    {
      if (dist2 < dist2Aid || (firstAidUnit && !firstAidUnit->GetVehicle()->IsAbleToStand()))
      {
        dist2Aid = dist2;
        firstAidUnit = u;
      }
    }
  }

  if (firstAidUnit && dist2Aid < 250000)//square(500))
  {
    return firstAidUnit;
  }
  else
  { //leader goes help  last
    AIUnit *u = group->Leader();

    if (group->CommandSent( u, Command::HealSoldier))   return NULL;;
    if (group->CommandSent( u, Command::FirstAid))   return NULL;;
    if (u->GetLifeState() == LifeStateUnconscious)   return NULL;;

    if (u != unit && u->GetLifeState() != LifeStateUnconscious && u->IsFreeSoldier()) 
    {
      float dist2 = (pos - u->Position(u->GetFutureVisualState())).SquareSizeXZ();
      if (u->IsFreeSoldier())
      {
        if (dist2 < Square(500))
          return u;
      }
    }
  }
  // no free unit found
  return NULL;
}

void AIGroup::SendIsDown(Transport *vehicle, AIUnit *down)
{
  // any unit from the same vehicle should report if possible
  // check which units are alive
  // consider: better check here, with Report Status verification
  AIUnit *reportTransport = NULL;
  AIUnit *report = NULL;
  AIUnit *reportPlayer = NULL;
  for (int i=0; i<_unitSlots.Size(); i++)
  {
    AIUnit *unit = _unitSlots[i]._unit;
    if (!unit) continue;
    // check when was the unit last seen (or heard)
    // ignore units that are alive
    if (unit->LSCanSpeak())
    {
      if (!reportTransport && unit->GetVehicle()==vehicle)
      {
        reportTransport = unit;
      }
      if (report) continue;
      if (unit->IsPlayer() || unit->GetPerson()->IsRemotePlayer())
      {
        if (!reportPlayer) reportPlayer = unit;
        continue;
      }
      report = unit;
      continue;
    }
  }
  // lowest ID AI soldier should report casualties
  if (reportTransport) report = reportTransport;
  if (!report) report = reportPlayer;
  for (int i=0; i<_unitSlots.Size(); i++)
  {
    AIUnit *unit = _unitSlots[i]._unit;
    if (!unit) continue;
    if (!unit->LSIsAlive())
    {
      if (report)
      {
        if (report->IsLocal())
        {
          SendUnitDown(report,unit);
        }
      }
      else
      {
        if (unit->GetLifeState() == LifeStateDead)
        {
          AISubgroup *subgrp = unit->GetSubgroup();
          if (subgrp) subgrp->ReceiveAnswer(unit, AI::UnitDestroyed);
        }
      }
    }
  }
}

void AIGroup::SendIsDown(AIUnit *down)
{
  // lowest ID AI soldier should report casualties
  // autoselect sender
  // check which units are alive
  // consider: better check here, with Report Status verification
  AIUnit *report = NULL;
  AIUnit *reportPlayer = NULL;
  for (int i=0; i<_unitSlots.Size(); i++)
  {
    AIUnit *unit = _unitSlots[i]._unit;
    if (!unit) continue;
    // check when was the unit last seen (or heard)
    // ignore units that are alive
    if (unit->LSCanSpeak())
    {
      if (report) continue;
      if (unit->IsPlayer() || unit->GetPerson()->IsRemotePlayer())
      {
        if (!reportPlayer) reportPlayer = unit;
        continue;
      }
      report = unit;
      continue;
    }
  }
  // lowest ID AI soldier should report casualties
  if (!report) report = reportPlayer;
  // for sure: verify unit is down
  if (!down->LSIsAlive())
  {
    if (report)
    {
      if (report->IsLocal())
      {
        SendUnitDown(report,down);
      }
    }
    else
    {
      if (down->GetLifeState() == LifeStateDead)
      {
        AISubgroup *subgrp = down->GetSubgroup();
        if (subgrp) subgrp->ReceiveAnswer(down,AI::UnitDestroyed);
      }
    }
  }
}

void AIGroup::CheckAlive()
{
  // check which units are alive
  // consider: better check here, with Report Status verification
  AIUnit *leader = Leader();
  bool leaderAlive = leader && leader->LSIsAlive();
  if (leader && !leaderAlive)
  {
    // set max. time when to known leader is dead
    SetReportBeforeTime(leader, Glob.time + 30);
  }
  if ((!IsAnyPlayerGroup() || Glob.config.IsEnabled(DTAutoSpot)) && leaderAlive)
  {
    // only AI groups or autospot players will ask for status
    for (int i=0; i<_unitSlots.Size(); i++)
    {
      AIUnit *unit = _unitSlots[i]._unit;
      if (!unit) continue;
      // check when was the unit last seen (or heard)
      // ignore units that are alive
      if (unit->LSIsAlive()) continue;
      // find corresponding target
      Target *tgt = unit->FindTarget(unit->GetVehicle());
      if (!tgt || tgt->GetLastSeen()<Glob.time-50)
      {
        // ask unit to report status
        // check if report was not asked yet?
        if (GetReportBeforeTime(unit)>Glob.time+30)
        {
          // nobody asked about it - it is probably dead?
          OLinkPermNOArray(AIUnit) list;
          list.Realloc(1);
          list.Resize(1);
          list[0] = unit;
          SendReportStatus(list);
        }

      }
    }
  }
  for (int i=0; i<_unitSlots.Size(); i++)
  {
    AIUnit *unit = _unitSlots[i]._unit;
    if (!unit) continue;
    // only local unit can report
    if (GetReportBeforeTime(unit)<Glob.time)
    {
      SetReportBeforeTime(unit,TIME_MAX);
      SendIsDown(unit);
    }
  }
}

void AIGroup::CheckSupport()
{
  bool playerGroup = IsPlayerGroup();

  for (int i=0; i<_unitSlots.Size(); i++)
  {
    AIUnit *unit = _unitSlots[i]._unit;
    if (!unit || !unit->IsUnit()) continue;
    if (unit->IsPlayer() && playerGroup) continue;
    if (unit->GetCombatMode() == CMCareless) continue;
    if (playerGroup && unit->IsKeepingFormation()) continue;

    if (CommandSent(unit, Command::Support)) continue;
    if (CommandSent(unit, Command::Heal)) continue;
    if (CommandSent(unit, Command::HealSoldier)) continue;
    if (CommandSent(unit, Command::RepairVehicle)) continue;
    if (CommandSent(unit, Command::Rearm)) continue;
    if (CommandSent(unit, Command::Repair)) continue;
    if (CommandSent(unit, Command::Refuel)) continue;
    if (CommandSent(unit, Command::TakeBag)) continue;
    if (CommandSent(unit, Command::Assemble)) continue;
    if (CommandSent(unit, Command::DisAssemble)) continue;

    // FIX: avoid support when getting in
    if (CommandSent(unit, Command::GetIn)) continue;

    EntityAI *veh = unit->GetVehicle();
    if (!veh || !veh->HasSupply()) continue;

    EntityAI *target = veh->GetAllocSupply();
    if (!target)
    {
      float minDist2 = FLT_MAX;
      const OLinkPermNOArray(AIUnit) &supplyUnits = veh->GetSupplyUnits();
      for (int j=0; j<supplyUnits.Size(); j++)
      {
        if (!supplyUnits[j]) continue;
        EntityAI *t = supplyUnits[j]->GetVehicle();
        float dist2 = t->FutureVisualState().Position().Distance2(veh->FutureVisualState().Position());
        if (dist2 < minDist2)
        {
          target = t;
          minDist2 = dist2;
        }
      }
    }
    if (!target) continue;

    if (target->FutureVisualState().Position().Distance2(veh->FutureVisualState().Position())>Square(veh->GetPrecision()*4))
    {
      Command cmd;
      cmd._message = Command::Support;
      cmd._destination = target->FutureVisualState().Position();
      cmd._target = target;
      cmd._time = Glob.time + COMMAND_TIMEOUT;
      if (unit->IsPlayer() || unit->IsKeepingFormation())
        SendAutoCommandToUnit(cmd, unit, true);
      else
      {
        OLinkPermNOArray(AIUnit) list;
        list.Realloc(1);
        list.Resize(1);
        list[0] = unit;

        NotifyAutoCommand(cmd, list);
      }
    }
  }
}

bool AIGroup::CheckHealth()
{
  bool ok = true;
  
  for (int i=0; i<_unitSlots.Size(); i++)
  {
    AIUnit *unit = _unitSlots[i]._unit;
    if (!unit) continue;
    if (!unit->GetVehicle()) continue;
    if (unit->GetCombatMode() == CMCareless && unit->GetLifeState() != LifeStateUnconscious) continue;

    EntityAI *vehicle = unit->GetVehicle();
    if (vehicle->GetType()->IsAttendant() && !unit->IsAnyPlayer() && unit->GetLifeState() != LifeStateUnconscious)
    {
      // heal himself
      if (unit->GetHealthState() != AIUnit::RSNormal 
        && !vehicle->CheckActionProcessing(ATHeal, unit)
        && !vehicle->CheckActionProcessing(ATHealSoldier, unit))
      {
        vehicle->StartActionProcessing(new ActionBasic(ATHealSoldier, vehicle), unit);
      }
      continue;
    }

    // units in vehicles does not send Heal command
    if (unit->GetVehicleIn()) continue;

    AIUnit::ResourceState state = GetHealthStateReported(unit);
    if (state == AIUnit::RSNormal) continue;

    ok = false;

    // check both group and center radio channel
    if (CommandSent(unit, Command::Heal)) continue;
    if (CommandSent(unit, Command::Support)) continue;
    
    EntityAIFull *attendant = unit->GetVehicle()->AssignedAttendant(); 

    if (attendant  && attendant->CommanderUnit() && attendant->CommanderUnit()->GetUnit())
    {
      AIUnit *attendantUnit=attendant->CommanderUnit()->GetUnit();
      if(attendantUnit->GetLifeState() == LifeStateUnconscious ) 
        unit->GetVehicle()->SetAssignedAttendant(NULL);
    }


    // FIX: avoid heal when getting in - this fix was removed and replaced by ClearGetInCommands
    //      it has caused never ending Command::Stop spamming
    if (unit->GetLifeState() != LifeStateUnconscious && unit->GetVehicle()->AssignedAttendant() && CommandSent(unit, Command::GetIn))
    {
      AISubgroup *subgrp = unit->GetSubgroup();
      if (subgrp)
      {
        subgrp->ClearGetInCommands();
    }
      ClearGetInCommands(unit);
    }
    
    if (unit->GetVehicle()->AssignedAttendant()) continue;

    const AITargetInfo *target = FindHealPosition(state, unit);

    if(!target && unit->GetLifeState() == LifeStateUnconscious)
    {
      const AIUnit *aidUnit = FindFirstAid(state, unit);
      if(aidUnit)
      {

        bool channelCenter = false;
        EntityAI *veh = aidUnit->GetVehicle();
        if (veh)
        {
          AIBrain *unit = veh->CommanderUnit();
          if (unit && unit->LSIsAlive())
            channelCenter = unit->GetGroup() != this;
        }

        AIBrain *brain =  veh->CommanderUnit();
        if (brain && brain->IsFreeSoldier() && brain->GetVehicle())
        {
          Command cmd;
          cmd._message = Command::FirstAid;
          cmd._destination = unit->Position(unit->GetFutureVisualState());
          cmd._target = unit->GetVehicle();
          cmd._time = Glob.time + COMMAND_TIMEOUT;
          unit->GetVehicle()->SetAssignedAttendant(brain->GetVehicle());
          SendAutoCommandToUnit(cmd, brain->GetUnit(), true, channelCenter);
          return ok;
        }
      }
    }

    if (!target)
    {
      if (state == AIUnit::RSCritical && !IsAnyPlayerGroup())
      {
        Assert(_center);
        if (_center->GetCommandRadio().Done() && _center->CanSupport(ATHeal) && !_center->WaitingForSupport(this, ATHeal))
        {
          _center->GetCommandRadio().Transmit(new RadioMessageSupportAsk(this, ATHeal));
        }
        else if (_center->GetCommandRadio().Done() && _center->CanSupport(ATHealSoldier) && !_center->WaitingForSupport(this, ATHealSoldier))
        {
          _center->GetCommandRadio().Transmit(new RadioMessageSupportAsk(this, ATHealSoldier));
        }
      }
      continue;
    }

    bool channelCenter = false;
    EntityAI *veh = target->_idExact;
    if (veh)
    {
      AIBrain *unit = veh->CommanderUnit();
      if (unit && unit->LSIsAlive())
        channelCenter = unit->GetGroup() != this;
    }

    AIBrain *brain =  veh->CommanderUnit();
    if (brain && brain->IsFreeSoldier() && brain->GetVehicle())
    {
      Command cmd;
      cmd._message = Command::HealSoldier;
      cmd._destination = unit->Position(unit->GetFutureVisualState());
      cmd._target = unit->GetVehicle();
      cmd._time = Glob.time + COMMAND_TIMEOUT;
      unit->GetVehicle()->SetAssignedAttendant(brain->GetVehicle());
      SendAutoCommandToUnit(cmd, brain->GetUnit(), true, channelCenter);
    }
    else
    {
      Command cmd;
      cmd._message = Command::Heal;
      cmd._destination = target->_realPos;
      cmd._target = veh;
      cmd._time = Glob.time + COMMAND_TIMEOUT;
      SendAutoCommandToUnit(cmd, unit, true, channelCenter);
    }
  }

  return ok;
}

#define AMMO_NEAR								100.0f
#define AMMO_FAR								500.0f

const AITargetInfo *AIGroup::FindRearmPosition(AIUnit::ResourceState state) const
{
  if (state == AIUnit::RSNormal) return NULL;
  Assert(Leader());
  
  // find ammo in close range
  const AITargetInfo *depot = NULL;
  float dist2Depot = FLT_MAX;
  const AITargetInfo *truck = NULL;
  float dist2Truck = FLT_MAX;

  for (int i=0; i<GetCenter()->NTargets(); i++)
  {
    const AITargetInfo &info = GetCenter()->GetTarget(i);
    if (info._destroyed)
      continue;
    if (!(info._side == TCivilian) && !(GetCenter()->IsFriendly(info._side)))
      continue;
    if (info._type->GetMaxAmmoCargo() <= 0)
      continue;
    EntityAI *veh = info._idExact;
    if (!veh) continue;
    if (veh->GetAmmoCargo() <= 0) continue;
    if( GetCenter()->GetExposurePessimistic(info._realPos)>0 ) continue;
    float dist2 = (Leader()->Position(Leader()->GetFutureVisualState()) - info._realPos).SquareSizeXZ();

    if (info._type->IsKindOf(GLOB_WORLD->Preloaded(VTypeStatic)))
    {
      if (dist2 < dist2Depot)
      {
        dist2Depot = dist2;
        depot = &info;
      }
    }
    else
    {
      if (dist2 < dist2Truck)
      {
        dist2Truck = dist2;
        truck = &info;
      }
    }
  }
  if (depot && dist2Depot < Square(AMMO_NEAR))
  {
    // rearm in near depot
    return depot;
  }
  if (truck && dist2Truck < Square(AMMO_NEAR))
  {
    // rearm in near truck
    return truck;
  }

  if (state == AIUnit::RSLow) return NULL;

  // find ammo in wider range
  if (depot && dist2Depot < Square(AMMO_FAR))
  {
    // rearm in far depot
    return depot;
  }
  if (truck && dist2Truck < Square(AMMO_FAR))
  {
    // rearm in far truck
    return truck;
  }

  // ammo not found
  return NULL;
}

bool AIGroup::FindRearmPosition(Command &cmd) const
{
  const AITargetInfo *target = FindRearmPosition(AIUnit::RSCritical);
  if (!target) return false;

  cmd._destination = target->_realPos;
  cmd._target = target->_idExact;
  cmd._time = Glob.time + COMMAND_TIMEOUT;
  return true;
}

AIUnit *AIGroup::FindEngineer(EntityAIFull *transport)
{
  if(!transport) return NULL;
  AIUnit *enginner = NULL;
  for (int i=0; i<_unitSlots.Size(); i++)
  {
    AIUnit *unit = _unitSlots[i]._unit;
    if (!unit || !unit->GetVehicle()) continue;

    if (!unit->GetVehicle()->GetType()->IsEngineer())  continue;
    // check both group and center radio channel
    if (CommandSent(unit, Command::RepairVehicle)) continue;
    if (CommandSent(unit, Command::Support)) continue;
    if (!unit->IsFreeSoldier()) continue;
    if (unit->GetLifeState() == LifeStateUnconscious) continue;

    if(!enginner) enginner = unit;
    else if(enginner->GetGroup() && enginner == enginner->GetGroup()->Leader()) enginner = unit;
    else if(transport->FutureVisualState().Position().Distance2(unit->Position(unit->GetFutureVisualState())) < transport->FutureVisualState().Position().Distance2(enginner->Position(enginner->GetFutureVisualState()))) enginner = unit;
  }
  if(enginner && transport->FutureVisualState().Position().Distance2(enginner->Position(enginner->GetFutureVisualState())) > 40000) return NULL;
  return enginner;
}

bool AIGroup::CheckAmmo()
{
  Assert(Leader());
  if (IsAnyPlayerGroup())
    return true;

  bool ok = true;
  AIUnit::ResourceState stateMax = AIUnit::RSNormal;
  for (int i=0; i<_unitSlots.Size(); i++)
  {
    AIUnit *unit = _unitSlots[i]._unit;
    if (!unit) continue;
    AIUnit::ResourceState state = GetAmmoStateReported(unit);
    if (state == AIUnit::RSNormal) continue;
    ok = false;

    if (unit->GetCombatMode() == CMCareless) continue;
    // check both group and center radio channel
    if (CommandSent(unit, Command::Rearm)) continue;
    if (CommandSent(unit, Command::Support)) continue;
    if (unit->IsFreeSoldier())
    {
      // infantry rearm
      unit->CheckAmmo();
    }
    else if (unit->IsUnit())
    {
      // check if some vehicles will need rearm
      if (state > stateMax) stateMax = state;
    }
  }

  const AITargetInfo *target = FindRearmPosition(stateMax);
  if (target)
  {
    bool channelCenter = false;
    EntityAI *veh = target->_idExact;
    if (veh)
    {
      AIBrain *unit = veh->CommanderUnit();
      if (unit && unit->LSIsAlive())
        channelCenter = unit->GetGroup() != this;
    }

    Command cmd;
    cmd._message = Command::Rearm;
    cmd._destination = target->_realPos;
    cmd._target = veh;
    cmd._time = Glob.time + COMMAND_TIMEOUT;

    for (int i=0; i<_unitSlots.Size(); i++)
    {
      AIUnit *unit = _unitSlots[i]._unit;
      if (!unit) continue;
      if (!unit->IsUnit()) continue;
      if (unit->GetCombatMode() == CMCareless) continue;
      // check both group and center radio channel
      if (CommandSent(unit, Command::Rearm)) continue;
      if (CommandSent(unit, Command::Support)) continue;

      if (GetAmmoStateReported(unit) >= stateMax)
      {
        SendAutoCommandToUnit(cmd, unit, true, channelCenter);
      }
    }
  }
  else
  {
    if (stateMax == AIUnit::RSCritical && !IsAnyPlayerGroup())
    {
      Assert(_center);
      if (_center->GetCommandRadio().Done() && _center->CanSupport(ATRearm) && !_center->WaitingForSupport(this, ATRearm))
      {
        _center->GetCommandRadio().Transmit(new RadioMessageSupportAsk(this, ATRearm));
      }
    }
  }

  return ok;
}

float AIGroup::GetCost() const
{
  // note: this function is obsolete and needs some polishing
  // if it should be used again
  float cost = 0;
  for (int u=0; u<NUnits(); u++)
  {
    AIUnit *unit = GetUnit(u);
    if (!unit) continue;
    cost += unit->GetPerson()->GetType()->GetCost();
    if (!unit->IsSoldier())
    {	// driver
      cost += unit->GetVehicle()->GetType()->GetCost();
    }
  }
  for (int v=0; v<_vehicles.Size(); v++)
  {
    Transport *veh = _vehicles[v];
    if (!veh || veh->IsDamageDestroyed())
      continue;
    cost += veh->GetType()->GetCost();
  }
  return cost;
}

float AIGroup::EstimateTime(Vector3Val pos) const
{
  EntityAI *vehicle = NULL;
  for (int v=0; v<_vehicles.Size(); v++)
  {
    Transport *veh = _vehicles[v];
    if (!veh || veh->IsDamageDestroyed())
      continue;
    vehicle = veh;
    break;
  }
  if (!vehicle)
  {
    for (int u=0; u<NUnits(); u++)
    {
      AIUnit *unit = GetUnit(u);
      if (!unit || !unit->IsUnit()) continue;
      vehicle = unit->GetVehicle();
      break;
    }
  }
  Assert(vehicle);
  Assert(Leader());
  float dist = (pos - Leader()->Position(Leader()->GetFutureVisualState())).SizeXZ();
  float time = dist / (0.5 * vehicle->GetType()->GetTypSpeed() * (1000.0 / 60.0)); // speed km/h -> m/min
  
  return time;
}

RString AIGroup::GetDebugName() const
{
  if (GetCenter())
  {
    return Format("%s %s", (const char *)GetCenter()->GetDebugName(), GetName());
  }
  return Format("<No center> %s", GetName());
}

void ReportUnit(const AIBrain *unit);
void ReportSubgroup(const AISubgroup *subgrp);
void ReportGroup(const AIGroup *grp)
{
  RptF
  (
    "Group %s (0x%x) - network ID %d:%d",
    (const char *)grp->GetDebugName(),
    grp,
    grp->GetNetworkId().creator, grp->GetNetworkId().id
  );
}

bool AIGroup::AssertValid() const
{
  bool result = true;
  if (UnitsCount() <= 0)
  {
    return true;	// its valid now
  }
  else if (!Leader())
  {
    ReportGroup(this);
    RptF(" - no leader");
    result = false;
  }
  else if (Leader()->GetGroup() == NULL)
  {
    ReportGroup(this);
    ReportUnit(Leader());
    RptF(" - leader has no group");
    result = false;
  }
  else if (Leader()->GetGroup() != this)
  {
    ReportGroup(this);
    ReportUnit(Leader());
    RptF(" - leader from another group");
    result = false;
  }
  if (!MainSubgroup())
  {
    ReportGroup(this);
    RptF(" - no main subgroup");
    result = false;
  }
  for (int i=0; i<_unitSlots.Size(); i++)
  {
    const AIUnitSlot &slot = _unitSlots[i];
    AIUnit *unit = slot._unit;
    if (!unit) continue;
    if (unit->ID() != i + 1)
    {
      ReportGroup(this);
      ReportUnit(unit);
      RptF(" - wrong unit ID - should be %d", i + 1);
      result = false;
      // error recovery
      unconst_cast(slot)._unit = NULL;
      continue;
    }
    if (unit->GetGroup() == NULL)
    {
      ReportGroup(this);
      ReportUnit(unit);
      RptF(" - unit with no group");
      result = false;
    }
    else if (unit->GetGroup() != this)
    {
      ReportGroup(this);
      ReportUnit(unit);
      RptF(" - unit from another group");
      result = false;
    }
  }
  for (int i=0; i<_subgroups.Size(); i++)
  {
    AISubgroup *subgrp = _subgroups[i];
    if (!subgrp) continue;
    if (subgrp->GetGroup() == NULL)
    {
      ReportGroup(this);
      ReportSubgroup(subgrp);
      RptF(" - subgroup with no group");
      result = false;
    }
    else if (subgrp->GetGroup() != this)
    {
      ReportGroup(this);
      ReportSubgroup(subgrp);
      RptF(" - subgroup from another group");
      result = false;
    }
    // check subgroup
    if (!subgrp->AssertValid()) result = false;
  }
  return result;
}

void AIGroup::Dump(int indent) const
{
}

///////////////////////////////////////////////////////////////////////////////
// class Mission

static const EnumName MissionActionNames[]=
{
  EnumName(Mission::NoMission, "NO MISSION"),
  EnumName(Mission::Arcade, "ARCADE"),
  EnumName(Mission::Arcade, "TRAINING"),
  EnumName()
};
template<>
const EnumName *GetEnumNames(Mission::Action dummy)
{
  return MissionActionNames;
}

LSError Mission::Serialize(ParamArchive &ar)
{
  CHECK(ar.SerializeEnum("action", _action, 1, Mission::Arcade))
  CHECK(ar.SerializeRef("Target", _target, 1))
  CHECK(::Serialize(ar, "destination", _destination, 1, VZero))
  CHECK(ar.Serialize("precision", _precision, 1, 0))
  return LSOK;
}

struct GroupNameParam
{
  RString _name;
  int _count;
};
TypeIsMovableZeroed(GroupNameParam)

int MaxGroups;
static RString GroupNameFormat;
static AutoArray<GroupNameParam> GroupNameParams;

class ReadGroupNameFunc
{
protected:
  ParamEntryPar _cls;

public:
  ReadGroupNameFunc(ParamEntryPar cls) : _cls(cls) {}

  RString operator ()(const char *&ptr)
  {
    // read the parameter name
    const char *begin = ptr;
    while (*ptr && __iscsym(*ptr)) ptr++;
    RString name(begin, ptr - begin);

    // add to the description
    int index = GroupNameParams.Add();
    GroupNameParams[index]._name = name;
    GroupNameParams[index]._count = (_cls >> name).GetEntryCount();
    return RString();
  }
};

void ReadGroupNameFormat(ParamEntryPar cls)
{
  // read the group names description
  GroupNameFormat = cls >> "groupNameFormat";
  GroupNameParams.Resize(0);
  ReadGroupNameFunc func(cls);
  GroupNameFormat.ParseFormat(func);
  GroupNameParams.Compact();

  // calculate the maximal number of groups per side
  MaxGroups = 0;
  if (GroupNameParams.Size() > 0)
  {
    MaxGroups = 1;
    for (int i=0; i<GroupNameParams.Size(); i++)
    {
      MaxGroups *= GroupNameParams[i]._count;
    }
  }
}

class FormatGroupName
{
protected:
  ParamEntryPar _cls;
  int _index;
  const AutoArray<RString> &_params;

public:
  FormatGroupName(ParamEntryPar cls, const AutoArray<RString> &params) : _cls(cls), _params(params) {_index = 0;}

  RString operator ()(const char *&ptr)
  {
    // read the parameter name
    const char *begin = ptr;
    while (*ptr && __iscsym(*ptr)) ptr++;
    RString name(begin, ptr - begin);

    // return the display name
    if (_index >= _params.Size()) return RString();
    ParamEntryVal entry = _cls >> name >> _params[_index];
    _index++;
    return entry >> "name"; // localized name
  }
};

RString GetGroupName(int id)
{
  AutoArray<RString> parts;
  int levels = GroupNameParams.Size();
  parts.Realloc(levels);
  parts.Resize(levels);
  for (int i=levels-1; i>=0; i--)
  {
    int index = id % GroupNameParams[i]._count;
    id /= GroupNameParams[i]._count;

    ParamEntryVal entry = (Pars >> "CfgWorlds" >> GroupNameParams[i]._name).GetEntry(index);
    parts[i] = entry.GetName();
  }

  FormatGroupName func(Pars >> "CfgWorlds", parts);
  return GroupNameFormat.ParseFormat(func);
}

void AIGroup::Init()
{
  Assert(_id > 0 && _id <= MaxGroups);
  if (_id <= 0 || _id > MaxGroups) return;

  int id = _id - 1;

  _nameFormat = GroupNameFormat;

  int levels = GroupNameParams.Size();
  _nameParts.Realloc(levels);
  _nameParts.Resize(levels);
  for (int i=levels-1; i>=0; i--)
  {
    int index = id % GroupNameParams[i]._count;
    id /= GroupNameParams[i]._count;

    ParamEntryVal entry = (Pars >> "CfgWorlds" >> GroupNameParams[i]._name).GetEntry(index);
    _nameParts[i] = entry.GetName();
  }

  // localized full name
  FormatGroupName func(Pars >> "CfgWorlds", _nameParts);
  _name = _nameFormat.ParseFormat(func);
}

void AIGroup::ApplyIdentity()
{
  // localized full name
  FormatGroupName func(Pars >> "CfgWorlds", _nameParts);
  RString fullname = _nameFormat.ParseFormat(func);

  // if name is used, switch the current name with the used one
  AICenter *center = GetCenter();
  if (center)
  {
    for (int i=0; i<center->NGroups(); i++)
    {
      AIGroup *grp = center->GetGroup(i);
      if (!grp) continue;
      if (grp->_name == fullname)
      {
        grp->_name = _name;
        break;
      }
    }
  }
  _name = fullname;
}

void AIGroup::SetIdentity(RString nameFormat, RString *nameParts, int namePartsCount)
{
  _nameFormat = nameFormat;
  _nameParts.Copy(nameParts, namePartsCount);
  ApplyIdentity();
}

// structure
void AIGroup::RemoveFromCenter()
{
  if (!IsLocal()) return;

  AICenter *center = _center;
  if (center)
  {
    _center = NULL;
    center->GroupRemoved(this);
  }
}

void AIGroup::ForceRemoveFromCenter()
{
  AICenter *center = _center;
  if (center)
  {
    _center = NULL;
    center->GroupRemoved(this);
  }
}

#ifndef DECL_ENUM_ARCADE_STATES
#define DECL_ENUM_ARCADE_STATES
DECL_ENUM(ArcadeFSMStates);
#endif

void ReportArcadeFSMState(ArcadeFSMStates state);

// FSM virtuals
void AIGroup::DoUpdate()
{
  AIGroupContext context(this);
  Update(&context);
#if 0
  if (GWorld->FocusOn() && GWorld->FocusOn()->GetGroup() == this)
  {
    if (GetCurrent())
    {
       FSM *fsm = GetCurrent()->_fsm;
       if (fsm)
       {
         DIAG_MESSAGE(100, Format("Arcade FSM: %s", fsm->GetStateName()))
       }
    }
    if (Leader() && Leader()->GetSubgroup()->GetCurrent())
    {
      Command *command = Leader()->GetSubgroup()->GetCurrent()->_task;
      FSM *fsm = Leader()->GetSubgroup()->GetCurrent()->_fsm;
      if (command && fsm)
      {
        DIAG_MESSAGE(100, Format("%s FSM: %s", (const char *)FindEnumName(command->_message), fsm->GetStateName()))
      }
    }
  }
#endif
}

void AIGroup::DoRefresh()
{
  AIGroupContext context(this);
  UpdateAndRefresh(&context);
}

// communication with center
void AIGroup::ReceiveMission(Mission &mis)
{
#if LOG_COMM
  Log("Receive mission: Group %s: Mission %d at %.0f,%.0f", (const char *)GetDebugName(), mis._action, mis._destination.X(), mis._destination.Z());
#endif
  // repeat mission for members of my group
  /*
  Assert(Leader());
  GetRadio().Transmit
  (
    new RadioMessageMission(NULL, this, mis),
    GetCenter()->GetLanguage(),
    Leader()->GetSpeaker()
  );
  */

  AIGroupContext context(this);
  base::Clear();
  PushTask(mis, &context);
//	Update(&context);
}

float AIGroup::ActualStrength() const
{
  float strength = 0;
  for (int i=0; i<_unitSlots.Size(); i++)
  {
    AIUnit *unit = _unitSlots[i]._unit;
    if (!unit) continue;
    Person *person = unit->GetPerson();
    if (!person) continue;
    float armor = person->GetType()->GetArmor();
    float dammage = person->GetTotalDamage();
    armor *= (1.0 - dammage);
    strength += sqrt(armor);
    Transport *veh = unit->GetVehicleIn();
    if (!veh) continue;
    if (veh->GetGroupAssigned() != this) // transporting vehicle
      continue;
    if (unit->IsUnit())
    {
      armor = veh->GetType()->GetArmor();
      dammage = veh->GetFleeDamage();
      armor *= (1.0 - dammage);
      strength += sqrt(armor);
    }
  }
/*
  LogF
  (
    "%s: Actual strength %.3f",
    (const char *)GetDebugName(),
    strength
  );
*/
  return strength;
}

void AIGroup::CalculateMaximalStrength()
{
  _maxStrength = 0;
  for (int i=0; i<_unitSlots.Size(); i++)
  {
    AIUnit *unit = _unitSlots[i]._unit;
    if (!unit) continue;
    float armor = unit->GetPerson()->GetType()->GetArmor();
    _maxStrength += armor * InvSqrt(armor);
    Transport *veh = unit->GetVehicleIn();
    if (!veh) continue;
    if (veh->GetGroupAssigned() != this) // transporting vehicle
      continue;
    if (unit->IsUnit())
    {
      armor = veh->GetType()->GetArmor();
      _maxStrength += sqrt(armor);
    }
  }
/*
  LogF
  (
    "%s: New maximal strength %.3f",
    (const char *)GetDebugName(),
    _maxStrength
  );
*/
}

void AIGroup::CalculateCourage()
{
  if (_forceCourage >= 0)
  {
    _courage = _forceCourage;
    return;
  }
  if (!Leader()) return;
  _courage = Leader()->GetAbility(AKCourage);
  saturate(_courage,0,1);
}

extern void Flee(AIGroupContext *context);
extern void Unflee(AIGroupContext *context);

Vector3 FindFleePoint( AIGroup *group, bool &forced );

void AIGroup::Flee()
{
  bool forced;
  Vector3 pos = FindFleePoint(this,forced);
  if( !forced && pos.Distance2(Leader()->Position(Leader()->GetFutureVisualState()))<50 )
  {
    // if we have got nowhere to flee to, do not flee
    return;
  }
  // pos result is ignored here, we are only interested if some point exists

  _flee = true;
  if (!GetCurrent()) return;
  AIGroupContext context(this);
  context._task = GetCurrent()->_task;
  context._fsm = GetCurrent()->_fsm;
  ::Flee(&context);
}

void AIGroup::Unflee()
{
  _flee = false;
  if (!GetCurrent()) return;
  AIGroupContext context(this);
  context._task = GetCurrent()->_task;
  context._fsm = GetCurrent()->_fsm;
  // we want reset Disclose flag for all targets
  // those that are disclosing us will set it again
  TargetList &list = GetTargetList();
  for (int i=0; i<list.AnyCount(); i++)
  {
    TargetNormal *tgt = list.GetAny(i);
    tgt->hasDisclosedUs = false;
  }

  ::Unflee(&context);
}

/// check if group is able to attack given target using its weapons
bool AIGroup::GroupCanAttack(const Target &tar) const
{
  for (int u=0; u<NUnits(); u++)
  {
    AIUnit *unit = GetUnit(u);
    if (!unit) continue;
    EntityAIFull *veh;
    if (unit->IsInCargo()) veh = unit->GetPerson();
    else if (unit->IsUnit()) veh = unit->GetVehicle();
    else continue;

    FireResult result;
    float bestDist, minRange, maxRange;
    bool potential = veh->BestFireResult(
      result, tar, bestDist, minRange, maxRange, 30, true
    );
    if (potential)
    {
      #if DIAG_ATTACK
      LogF("GroupCanAttack true");
      #endif
      return true;
    }
  }
  #if DIAG_ATTACK
  LogF("GroupCanAttack false");
  #endif
  return false;
}

/*!
\patch 5117 Date 1/16/2007 by Ondra
- Improved: AI now retreats when under threat and having no weapons to engage any enemy targets.

\patch 5132 Date 2/23/2007 by Ondra
- Fixed: When fleeing is disabled, units never flee - even when having no weapons
*/

bool AIGroup::CheckRetreat()
{
#if 1
  AICenter *center = GetCenter();
  if (!center) return false;
  // if there is no leader, we cannot check fleeing
  if (!Leader()) return false;

  // when fleeing is disabled, units never flee - even when having no weapons
  if (_courage>=1) return false;
  // if a target has damagePerMinute = 0, nobody is assigned to damage it
  // this may have different reasons
  //   lack of weapon
  //   combat mode
  //   semaphore (hold fire)

  // we are interested only in units which already know about us

  // if somebody is assigned to any target, do not retreat
  // the assigned unit needs to be able to fill its orders
  const TargetList &list = GetTargetList();
  for (int i=0; i<list.EnemyCount(); i++)
  {
    const TargetNormal *tar = list.GetEnemy(i);
    if (!tar->idExact) continue;
    if (!tar->IsKnown() ) continue;
    // enemies are listed first - when seeing non-enemy, we can break
    if (!center->IsEnemy(tar->side)) break;
    if (tar->damagePerMinute>0) return false;
  }

  // check if there is some target we are unable to cover

  for (int i=0; i<list.EnemyCount(); i++)
  {
    const TargetNormal *tar = list.GetEnemy(i);
    if (!tar->idExact) continue;
    if (tar->IsVanishedOrDestroyed()) continue;
    if (tar->idExact->IsDamageDestroyed()) continue;
    // if we did not see the target for a minute, there is no need to start retreating
    if (!tar->IsKnown() || tar->GetLastSeen()<Glob.time-60) continue;
    // enemies are listed first - when seeing non-enemy, we can break
    if (!center->IsEnemy(tar->side)) break;
    // if somebody is assigned to any target, no need to retreat
    if (tar->damagePerMinute>0) break;
    // if does not know about us, no need to retreat
    if (!tar->hasDisclosedUs) continue;
    
    // now we want to double-check if we can engage it or not
    // if any unit could engage it, we do not retreat
    if (GroupCanAttack(*tar)) return false;

    // we cannot engage the target, we need to flee
    return true;
  }
#endif
  return false;
}


AIUnit::ResourceState AIGroup::GetHealthStateReported(AIUnit *unit)
{
  Assert(unit->GetGroup() == this);
  int index = unit->ID() - 1;
  Assert(index >= 0 && index < _unitSlots.Size());
  return _unitSlots[index]._healthState;
}

AIUnit::ResourceState AIGroup::GetAmmoStateReported(AIUnit *unit)
{
  Assert(unit->GetGroup() == this);
  int index = unit->ID() - 1;
  Assert(index >= 0 && index < _unitSlots.Size());
  return _unitSlots[index]._ammoState;
}

AIUnit::ResourceState AIGroup::GetFuelStateReported(AIUnit *unit)
{
  Assert(unit->GetGroup() == this);
  int index = unit->ID() - 1;
  Assert(index >= 0 && index < _unitSlots.Size());
  return _unitSlots[index]._fuelState;
}
AIUnit::ResourceState AIGroup::GetDamageStateReported(AIUnit *unit)
{
  Assert(unit->GetGroup() == this);
  int index = unit->ID() - 1;
  Assert(index >= 0 && index < _unitSlots.Size());
  return _unitSlots[index]._dammageState;
}
AIUnit::ResourceState AIGroup::GetWorstStateReported(AIUnit *unit)
{
  Assert(unit->GetGroup() == this);
  int index = unit->ID() - 1;
  Assert(index >= 0 && index < _unitSlots.Size());
  AIUnitSlot &slot = _unitSlots[index];
  
  AIUnit::ResourceState state = slot._ammoState;
  if (state < slot._healthState) state = slot._healthState;
  if (state < slot._ammoState) state = slot._ammoState;
  if (state < slot._fuelState) state = slot._fuelState;
  if (state < slot._dammageState) state = slot._dammageState;
  return state;
}

AIUnit::ResourceType AIGroup::GetWorstStateTypeReported(AIUnit *unit)
{
  Assert(unit->GetGroup() == this);
  int index = unit->ID() - 1;
  Assert(index >= 0 && index < _unitSlots.Size());
  AIUnitSlot &slot = _unitSlots[index];


  if (slot._healthState >= AIUnit::RSCritical)  return AIUnit::RTHealth;
  if (slot._dammageState >= AIUnit::RSCritical) return AIUnit::RTDamage;
  if (slot._ammoState >= AIUnit::RSCritical) return AIUnit::RTAmmo;
  if (slot._fuelState >= AIUnit::RSCritical) return AIUnit::RTFuel;
  return AIUnit::RTNone;
}

void AIGroup::SetHealthStateReported(AIUnit *unit, AIUnit::ResourceState state)
{
  Assert(unit->GetGroup() == this);
  int index = unit->ID() - 1;
  Assert(index >= 0 && index < _unitSlots.Size());
  _unitSlots[index]._healthState = state;
}
void AIGroup::SetAmmoStateReported(AIUnit *unit, AIUnit::ResourceState state)
{
  Assert(unit->GetGroup() == this);
  int index = unit->ID() - 1;
  Assert(index >= 0 && index < _unitSlots.Size());
  _unitSlots[index]._ammoState = state;
}
void AIGroup::SetFuelStateReported(AIUnit *unit, AIUnit::ResourceState state)
{
  Assert(unit->GetGroup() == this);
  int index = unit->ID() - 1;
  Assert(index >= 0 && index < _unitSlots.Size());
  _unitSlots[index]._fuelState = state;
}
void AIGroup::SetDamageStateReported(AIUnit *unit, AIUnit::ResourceState state)
{
  Assert(unit->GetGroup() == this);
  int index = unit->ID() - 1;
  Assert(index >= 0 && index < _unitSlots.Size());
  _unitSlots[index]._dammageState = state;
}

bool AIGroup::GetReportedDown(AIUnit *unit)
{
  Assert(unit->GetGroup() == this);
  int index = unit->ID() - 1;
  Assert(index >= 0 && index < _unitSlots.Size());
  return _unitSlots[index]._reportedDown;
}

void AIGroup::SetReportedDown(AIUnit *unit, bool state)
{
  Assert(unit->GetGroup() == this);
  int index = unit->ID() - 1;
  Assert(index >= 0 && index < _unitSlots.Size());
  _unitSlots[index]._reportedDown = state;
}

Time AIGroup::GetReportBeforeTime(AIUnit *unit)
{
  Assert(unit->GetGroup() == this);
  int index = unit->ID() - 1;
  Assert(index >= 0 && index < _unitSlots.Size());
  return _unitSlots[index]._reportBeforeTime;
}

void AIGroup::SetReportBeforeTime(AIUnit *unit, Time time)
{
  Assert(unit->GetGroup() == this);
  int index = unit->ID() - 1;
  Assert(index >= 0 && index < _unitSlots.Size());
  AIUnitSlot &slot = _unitSlots[index];

  if (slot._reportedDown) return;
  Time &tgt = slot._reportBeforeTime;
  if (tgt > time) tgt = time;
}

Vector3 AIGroup::GetGroupDirection() const
{
  if (!Leader() || Leader()->IsAnyPlayer() || !GetCurrent())
    return MainSubgroup()->GetFormationDirection();

  int wp = GetCurrent()->_fsm->Var(0);
  if (wp <= 0 || wp >= NWaypoints())
    return MainSubgroup()->GetFormationDirection();

  return (GetWaypointPosition(wp) - GetWaypointPosition(wp - 1)).Normalized();
}

Vector3 FindWaypointPosition(Vector3Par center, float radius)
{
  // use paper car - it is cheap in terms of textures and memory
  // it is defined so that it gives most constraints on position of all all vehicles
  Vector3 normal = VUp;
  const EntityAIType *dummyVehType = GWorld->Preloaded(VTypePaperCar);
  for (int j=0; j<100; j++)
  {
    float xr = (2.0 * GRandGen.RandomValue() - 1.0) * radius;
    float zr = (2.0 * GRandGen.RandomValue() - 1.0) * radius;
    if (Square(xr) + Square(zr) > radius * radius) continue;
    Vector3 pos;		
    pos.Init();
    pos[0] = center[0] + xr;
    pos[1] = 0;
    pos[2] = center[2] + zr;
    int xtest = toIntFloor(pos.X() * InvLandGrid);
    int ztest = toIntFloor(pos.Z() * InvLandGrid);
    for (int x=-1; x<=1; x++)
      for (int z=-1; z<=1; z++)
      {
        GeographyInfo info = GLOB_LAND->GetGeography(xtest + x, ztest + z);
        if (dummyVehType->GetBaseCost(info, false, true) >= 1e10)
          goto BadPosition;
      }

      if (AIUnit::FindFreePosition(pos, normal, false, dummyVehType))
        return pos;

BadPosition:
      continue;
  }

  Vector3 pos = center;		
  AIUnit::FindFreePosition(pos, normal, false, dummyVehType);
  return pos;
}


void AIGroup::PrepareGroup(const ArcadeGroupInfo &gInfo, bool multiplayer, bool patchWpPos)
{
  AIUnit *leaderUnit = Leader();
  Assert(leaderUnit);
  AICenter *center = GetCenter();
  Assert(center);

  CalculateMaximalStrength();

  // create waypoints
  int m = gInfo.waypoints.Size();
  _wp.Realloc(m + 1);
  _wp.Resize(m + 1);
  WaypointInfo &wInfo = _wp[0];
  wInfo.InitWp();
  wInfo.position = leaderUnit->Position(leaderUnit->GetFutureVisualState());
  wInfo.type = ACMOVE;
  for (int j=0; j<m; j++)
  {
    const WaypointInfo &wInfo = gInfo.waypoints[j];
    _wp[j + 1] = wInfo;
    // Add synchronization into map
    int o = wInfo.synchronizations.Size();
    for (int k=0; k<o; k++)
    {
      int sync = wInfo.synchronizations[k];
      Assert(sync >= 0);
      if (sync >= synchronized.Size())
        synchronized.Resize(sync + 1);
#if _VBS3 && _VBS_WP_SYNCH
      synchronized[sync].Add(this,j);
#else
      synchronized[sync].Add(this);
#endif
    }

    if (wInfo.placement > 0)
      // set random position
      _wp[j + 1].position = FindWaypointPosition(wInfo.position, wInfo.placement);
    else if (patchWpPos)
    {
      _wp[j + 1].position[1] = GLandscape->RoadSurfaceYAboveWater(wInfo.position[0], wInfo.position[2]);
    }
  }

  // create sensors - must be after selecting leader
  m = gInfo.sensors.Size();
  for (int j=0; j<m; j++)
  {
    const ArcadeSensorInfo &sInfo = gInfo.sensors[j];
    extern Vehicle *CreateSensor(const ArcadeSensorInfo &info, AIGroup *grp, bool multiplayer);
    CreateSensor(sInfo, this, multiplayer);
  }

  // set formation and semaphores
  if (NWaypoints() > 1)
  {
    const WaypointInfo &wInfo = GetWaypoint(1);
    if (wInfo.formation >= 0)
    {
      AI::Formation f = (AI::Formation)wInfo.formation;
      MainSubgroup()->SetFormation(f);
    }
    if (wInfo.combatMode >= 0)
    {
      AI::Semaphore s = (AI::Semaphore)wInfo.combatMode;
      SetSemaphore(s);
      for (int i=0; i<NUnits(); i++)
      {
        AIUnit *unit = GetUnit(i);
        if (unit) unit->SetSemaphore(s);
      }
    }
    if (wInfo.speed != SpeedUnchanged)
      MainSubgroup()->SetSpeedMode(wInfo.speed);
    if (wInfo.combat != CMUnchanged)
      SetCombatModeMajorReactToChange(wInfo.combat);
  }

  // send mission
  Mission mis;
  mis._action = Mission::Arcade;
  center->SendMission(this, mis);
}

void AIGroup::OnRemotePlayerAdded()
{
  for (int i=0; i<_subgroups.Size(); i++)
  {
    AISubgroup *subgroup = _subgroups[i];
    if (subgroup) subgroup->OnRemotePlayerAdded();
  }
}

RString GetPictureName( RString baseName );

LSError GIcon::Serialize(ParamArchive &ar)
{
    CHECK(ar.Serialize("offsetX", _offsetX, 1, 0))
    CHECK(ar.Serialize("offsetY", _offsetY, 1, 0))
    CHECK(ar.Serialize("ID", _ID, 1, 0))
    CHECK(ar.Serialize("textureName", _name, 1, ""))
    if (ar.IsLoading())
    {
      ParamEntryVal cfg = Pars >> "cfgGroupIcons";
      _groupMaker = GlobLoadTexture(
        GetPictureName(cfg >> _name >> "icon")
        );
      //read texture params from config
      _shadow = cfg >> _name >> "shadow";
      _size = cfg >> _name >> "size";
    }

    return LSOK;
}

//!add new texture to final icon
int GroupMarkers::AddMarker(Ref<Texture> texture,RString name,int shadow,int size, float offsetX, float offsetY)
{
  _groupMakers.Add(GIcon(texture,++_IDcounter,name, size, offsetX, offsetY, shadow));
  return _IDcounter;
}
//!edit parameters of texture with given ID (offsets remains)
void GroupMarkers::SetMarker(Ref<Texture> texture,RString name,int shadow,int size, int ID)
{
  for(int i=0; i< _groupMakers.Size();i++)
    if(ID == _groupMakers[i]._ID)
    {
      _groupMakers[i]._groupMaker = texture;
      _groupMakers[i]._size = size;
      _groupMakers[i]._shadow = shadow;
      _groupMakers[i]._name = name;
    }
}
//!edit parameters of texture with given ID 
void GroupMarkers::SetMarker(Ref<Texture> texture,RString name,int shadow,int size, float offsetX, float offsetY, int ID)
{
  for(int i=0; i< _groupMakers.Size();i++)
    if(ID == _groupMakers[i]._ID)
    {
      _groupMakers[i]._groupMaker = texture;
      _groupMakers[i]._offsetX = offsetX;
      _groupMakers[i]._offsetY = offsetY;
      _groupMakers[i]._size = size;
      _groupMakers[i]._shadow = shadow;
      _groupMakers[i]._name = name;
    }
}
//!set final icon parameters, applies to all textures used for final icon
void GroupMarkers::Setparams(PackedColor markerColor, PackedColor waypointColor,RString text,float scale, bool showIcon, bool showWaypoint)
{
  _markerColor = markerColor;
  _text = text;
  _showIcon = showIcon;
  _scale = scale;
  _waypoints = showWaypoint;
  _waypointColor = waypointColor;
}
//!remove single icon with given ID from group markers list
void GroupMarkers::RemoveIcon(int ID) 
{
  for(int i=0; i< _groupMakers.Size();i++)
    if(ID == _groupMakers[i]._ID)
    {
      _groupMakers.Delete(i);
    }
}


LSError GroupMarkers::Serialize(ParamArchive &ar)
{
  CHECK(ar.Serialize("showIcon", _showIcon, 1, false))
    CHECK(ar.Serialize("IDcounter", _IDcounter, 1, 0))
    CHECK(ar.Serialize("scale", _scale, 1, 1))
    CHECK(ar.Serialize("waypoints", _waypoints, 1, false))

    CHECK(::Serialize(ar,"markerColor", _markerColor, 1,Color(255,255,255,255)))
    CHECK(::Serialize(ar,"waypointColor", _waypointColor, 1,Color(0,0,0,255)))
    CHECK(ar.Serialize("text", _text, 1, ""))
    CHECK(ar.Serialize("textures", _groupMakers, 1))

    return LSOK;
}
