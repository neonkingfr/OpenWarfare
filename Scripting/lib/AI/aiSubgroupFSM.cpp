///////////////////////////////////////////////////////////////////////////////
// Implementation of AISubgroup as an Abstract AI Machine

#include "../wpch.hpp"

#pragma warning(disable:4100 4511 4512 4201 4127)

#include "ai.hpp"
#include "aiRadio.hpp"
#include "../fsm.hpp"
#include "../global.hpp"
#include "../world.hpp"
#include "../landscape.hpp"
#include "../person.hpp"
#include "../house.hpp"

#include "../shots.hpp"
#include "../weapons.hpp"
#include "../UI/uiActions.hpp"

#include <El/Common/perfProf.hpp>
#include <El/Evaluator/express.hpp>

#include "../Network/network.hpp"

#include <Es/Memory/normalNew.hpp>

class AISubgroupFSM: public FSMTyped<AISubgroupContext>
{
  typedef FSMTyped<AISubgroupContext> base;

public:
  AISubgroupFSM
    (
    const StateInfo *states, int n,
    const pStateFunction *functions = NULL, int nFunc = 0
    );
  ~AISubgroupFSM();

  USE_FAST_ALLOCATOR
};

#include <Es/Memory/debugNew.hpp>

DEFINE_FAST_ALLOCATOR(AISubgroupFSM)

AISubgroupFSM::AISubgroupFSM
(
	const StateInfo *states, int n,
	const pStateFunction *functions, int nFunc
)
:base(states,n,functions,nFunc)
{
}

AISubgroupFSM::~AISubgroupFSM()
{
}

#include "fsmScripted.hpp"
#include "../gameStateExt.hpp"

#include <Es/Memory/normalNew.hpp>

class AISubgroupFSMScripted: public FSMScriptedTyped<AISubgroupContext>
{
  typedef FSMScriptedTyped<AISubgroupContext> base;

public:
  AISubgroupFSMScripted(RString name, GameDataNamespace *globals)
    : base(FSMScriptedTypes.New(FSMScriptedTypeName(name)), globals, NULL) {}

  virtual void ApplyContext(GameState &state, void *context);

  USE_FAST_ALLOCATOR
};

#include <Es/Memory/debugNew.hpp>

DEFINE_FAST_ALLOCATOR(AISubgroupFSMScripted)

void AISubgroupFSMScripted::ApplyContext(GameState &state, void *context)
{
  AISubgroupContext *ctx = reinterpret_cast<AISubgroupContext *>(context);

  AIUnit *leader = ctx->_subgroup ? ctx->_subgroup->Leader() : NULL;
  Person *person = leader ? leader->GetPerson() : NULL;
  state.VarSetLocal("_leader", GameValueExt(person), true);
  {
    GameValue value = state.CreateGameValue(GameArray);
    GameArrayType &array = value;
    if (ctx->_subgroup)
    {
      int n = ctx->_subgroup->NUnits();
      for (int i=0; i<n; i++)
      {
        AIUnit *unit = ctx->_subgroup->GetUnit(i);
        if (!unit) continue;
        Person *person = unit->GetPerson();
        if (person) array.Add(GameValueExt(person));
      }
    }
    state.VarSetLocal("_units", value, true);
  }
  
  state.VarSetLocal("_target", GameValueExt(ctx->_task->_target,GameValExtObject), true);
  
  {
    GameValue value = state.CreateGameValue(GameArray);
    GameArrayType &array = value;
    array.Realloc(3);
    array.Resize(3);
    Vector3Val pos = ctx->_task->_destination;
    array[0] = pos.X();
    array[1] = pos.Z();
    array[2] = pos.Y() - GLandscape->SurfaceYAboveWater(pos.X(), pos.Z());
    state.VarSetLocal("_destination", value, true);
  }
}

#define LOST_UNIT_MIN							45.0F
#define LOST_UNIT_MAX							300.0F

#define SUBGROUP_REFRESH					20.0F

///////////////////////////////////////////////////////////////////////////////
// Generic Command Functions

#if !_RELEASE
	#define LOG_FSM 0
#else
	#define LOG_FSM 0
#endif

#define DIAG_ATTACK 0

#include "../engine.hpp"

inline void CheckDistance(AIUnit *unit, Vector3Val dest)
{
/*
	if (!unit) return;

	EntityAI *veh = unit->GetVehicle();
	float prec = 3.0 * veh->GetPrecision();
	saturateMax(prec, 10.0);
	if (veh->Position().Distance2(dest) > Square(prec))
	{
		GEngine->ShowMessage
		(
			2000,"%.3f, %s (%s): Far destination completed - distance %.0f m",
			Glob.time.toFloat(), (const char *)unit->GetDebugName(),
			(const char *)veh->GetType()->GetName(), veh->Position().Distance(dest)
		);
		RptF
		(
			"%.3f, %s (%s): Far destination completed",
			Glob.time.toFloat(), (const char *)unit->GetDebugName(),
			(const char *)veh->GetType()->GetName()
		);
		RptF
		(
			"  - position %.0f, %.0f, %.0f", veh->Position().X(), veh->Position().Y(), veh->Position().Z()
		);
		RptF
		(
			"  - destination %.0f, %.0f, %.0f", dest.X(), dest.Y(), dest.Z()
		);
		RptF
		(
			"  - FSM state: %d %s",
			unit->GetSubgroup()->GetCommand()->_message,
			unit->GetSubgroup()->GetCurrent()->_fsm->GetStateName()
		);
	}
*/
}

inline void LogFSM(AISubgroup *subgrp, const char *cmd, const char *state)
{
#if LOG_FSM
	if (subgrp && subgrp->GetGroup())
		LogF
		(
			"FSM: %s %s - state %s, %d tasks",
			(const char *)subgrp->GetDebugName(),
			cmd,
			state, 
			subgrp->StackSize()
		);
#endif
}

#if 0 // _ENABLE_CHEATS
static bool TrackUnit(AISubgroup *subgrp)
{
  if (!subgrp->Leader()) return false;
	if (subgrp->Leader()->GetVehicle()!=GWorld->CameraOn()) return false;
	__asm nop;
	return true;
}
#else
static inline bool TrackUnit(AISubgroup *subgrp) {return false;}
#endif

#define INIT_PREFIX \
	AISubgroup *subgrp = context->_subgroup; \
	Assert(subgrp); \
	const bool diag = TrackUnit(subgrp);(void)diag;

#define STATE_PREFIX \
	AISubgroup *subgrp = context->_subgroup; \
	Assert(subgrp); \
	Command *cmd = context->_task; \
	Assert(cmd); \
	(void)cmd; \
	if (subgrp->NUnits() <= 0) return; \
	Assert(subgrp->Leader()); \
	if (!subgrp->Leader()) return; \
	const bool diag = TrackUnit(subgrp);(void)diag;
/*!
\patch 1.34 Date 12/6/2001 by Ondra
- Fixed: MP: Player character auto reported "Ready" when completed command.
*/

void CommandSucceed(AISubgroupContext *context)
{
	AISubgroup *subgroup = context->_subgroup;
	Assert(subgroup);
	Command *cmd = context->_task;
	Assert(cmd);
	LogFSM(subgroup, "Command", "SUCCEED");

	subgroup->DoNotGo();
	subgroup->ClearPlan();

	AIGroup *grp = subgroup->GetGroup();
	if (subgroup->IsAnyPlayerSubgroup())
	{
		grp->ReceiveAnswer(subgroup,AI::CommandCompleted);
	}
	else
	{
		subgroup->SendAnswer(AI::CommandCompleted);
	}
	if (grp->IsPlayerGroup())
		SetCommandState(cmd->_id, CSSucceed, subgroup);

	if (cmd->_joinToSubgroup && cmd->_joinToSubgroup != subgroup && subgroup != grp->MainSubgroup())
	{
		subgroup->JoinToSubgroup(cmd->_joinToSubgroup);
	}
}

void CheckCommandSucceed(AISubgroupContext *context)
{
	context->_fsm->SetState(FSM::FinalState, context);
}

void CommandFailed(AISubgroupContext *context)
{
	AISubgroup *subgroup = context->_subgroup;
	Assert(subgroup);
	Command *cmd = context->_task;
	Assert(cmd);
	LogFSM(subgroup, "Command", "FAILED");

	subgroup->DoNotGo();
	subgroup->ClearPlan();
	AIGroup *grp = subgroup->GetGroup();
	if (subgroup->IsAnyPlayerSubgroup())
	{
		grp->ReceiveAnswer(subgroup,AI::CommandFailed);
	}
	else
	{
		subgroup->SendAnswer(AI::CommandFailed);
	}
	if (cmd->_id >= 0)
		SetCommandState(cmd->_id, CSFailed, subgroup);

	if (cmd->_joinToSubgroup && cmd->_joinToSubgroup != subgroup && subgroup != grp->MainSubgroup())
	{
		subgroup->JoinToSubgroup(cmd->_joinToSubgroup);
	}
}

void CheckCommandFailed(AISubgroupContext *context)
{
	context->_fsm->SetState(FSM::FinalState, context);
}

///////////////////////////////////////////////////////////////////////////////
// No Command FSM

static void NoCommandWait(AISubgroupContext *context)
{
	if (!context) return;

	AISubgroup *subgrp = context->_subgroup;
	if(!subgrp ) return;

	LogFSM(subgrp, "NoCommand", "WAIT");
}

static void CheckNoCommandWait(AISubgroupContext *context)
{
}

/*
static void NoCommandEnter(AISubgroupContext *context)
{
}

static void NoCommandExit(AISubgroupContext *context)
{
}
*/

static AISubgroupFSM::StateInfo noCommandStates[] =
{
	AISubgroupFSM::StateInfo("Wait", NoCommandWait, CheckNoCommandWait)
};

/*
static AISubgroupFSM::pStateFunction noCommandSpecial[] =
{
	NoCommandEnter,NoCommandExit
};
*/

///////////////////////////////////////////////////////////////////////////////
// Command Wait FSM

enum
{
	SWaitInit,
	SWaitWait,
	SWaitDone,SWaitFail
};

static void WaitInit(AISubgroupContext *context)
{
	INIT_PREFIX
	if (!subgrp->HasAI())
		return;

	LogFSM(subgrp, "Wait", "INIT");

	subgrp->SetRefreshTime(TIME_MAX);
	subgrp->DoNotGo();
}

static void CheckWaitInit(AISubgroupContext *context)
{
	Assert(context->_subgroup);

	context->_fsm->SetState(SWaitWait, context);
}

static void WaitWait(AISubgroupContext *context)
{
	STATE_PREFIX
	if (!subgrp->HasAI())
		return;

	LogFSM(subgrp, "Wait", "WAIT");

	subgrp->SetRefreshTime(TIME_MAX);
	subgrp->DoNotGo();
}

static void CheckWaitWait(AISubgroupContext *context)
{
	STATE_PREFIX

	if (cmd->_time >= Time(0) && Glob.time > cmd->_time)
		context->_fsm->SetState(SWaitDone, context);
}

static AISubgroupFSM::StateInfo waitStates[] =
{
	AISubgroupFSM::StateInfo("Init", WaitInit, CheckWaitInit),
	AISubgroupFSM::StateInfo("Wait", WaitWait, CheckWaitWait),
	AISubgroupFSM::StateInfo("Succeed", CommandSucceed, CheckCommandSucceed),
	AISubgroupFSM::StateInfo("Failed", CommandFailed, CheckCommandFailed)
};


///////////////////////////////////////////////////////////////////////////////
// Command Attack FSM

enum
{
	SAttackInit,
	SAttackFarMove,SAttackMove,
	SAttackAttack,
	SAttackWait,
  SAttackRunAway,
  SAttackWaitAfterExplo,
	SAttackDone,SAttackFail
};

// TODO: correct handling of cmd->_target
// use always _targetE

static Target *GetAttackTarget( const Command *cmd, AIGroup *group )
{
	Target *tgt = cmd->_targetE;
	if (!tgt)
  {
    AIUnit *leader = group->Leader();
    if (!leader) return NULL;
    tgt = leader->FindTarget(cmd->_target);
  }
	if (!tgt) return tgt;
	if (tgt->IsDestroyed()) return NULL;
	return tgt;
}

static bool IsAttackTarget(const Command *cmd, AISubgroup *subgrp)
{	
	Target *tgt = cmd->_targetE;
	if (tgt)
	{
    // if target has been destroyed, do not attack any more
		if (tgt->IsDestroyed())
		  return false;
		// if side has changed and the target is no longer enemy, do not attack any more
    if (subgrp->Leader() && !subgrp->Leader()->IsEnemy(tgt->GetSide()))
      return false;
    return true;
	}
	return cmd->_target!=NULL;
}

/*!
\patch_internal 5120 Date 1/18/2007 by Ondra
- Fixed: Dead units no longer planning attacks (avoid Add dead sensor message)
*/
static bool AttackReplan(AISubgroupContext *context)
{
	AISubgroup *subgrp = context->_subgroup;
	Assert(subgrp);
	Command *cmd = context->_task;
	Assert(cmd);

  if (!subgrp->Leader()->LSIsAlive()) return false;
	EntityAI *lVehicle=subgrp->Leader()->GetVehicle();
	Target *tgt = GetAttackTarget(cmd,subgrp->GetGroup());

	// call BeginAttack to be sure vehicle is engaged
	if( tgt ) lVehicle->BegAttack(tgt);

	Vector3 attackPos;
	FireResult result;

	#if DIAG_ATTACK
	LogF("AttackThink");
	#endif
	if( lVehicle->AttackThink(result,attackPos) )
	{
		if( attackPos.Distance2(cmd->_destination)>Square(5) )
		{
			if( attackPos.SquareSize()<1 )
			{
				Fail("Bad attack pos");
			}
			// Refreshing when attack position changed
			subgrp->GoPlanned(attackPos);
		}
		return true;
	}
	return false;
}

static bool CanAttack( AISubgroup *subgrp, const Target &tar )
{
	for (int u=0; u<subgrp->NUnits(); u++)
	{
		AIUnit *unit = subgrp->GetUnit(u);
		if (!unit) continue;
		EntityAIFull *veh;
		if (unit->IsInCargo()) veh = unit->GetPerson();
		else if (unit->IsUnit()) veh = unit->GetVehicle();
		else continue;

		FireResult result;
		float bestDist, minRange, maxRange;
		bool potential = veh->BestFireResult(
			result, tar, bestDist, minRange, maxRange, 30, true
		);
		if (potential)
		{
			#if DIAG_ATTACK
			LogF("CanAttack true");
			#endif
			return true;
		}
	}
	#if DIAG_ATTACK
	LogF("CanAttack false");
	#endif
	return false;
}

static void TryAttack
(
	bool &somePossible, bool &somePotential, AISubgroup *subgrp, Target &tar
)
{
	somePossible=somePotential=false;
	AIUnit *leader = subgrp->Leader();
	EntityAIFull *lVehicle = leader->GetVehicle();
	Shot *shot = dyn_cast<Shot>(lVehicle->GetLastShot());
	if (shot)
	{
		// if we have already placed a bomb, wait until it explodes
		switch (shot->Type()->_simulation)
		{
			case AmmoShotPipeBomb:
			case AmmoShotTimeBomb:
				// no fire possible until bomb exploded
				return;
		}
	}
	//Assert( subgrp->HasAI() );
	for (int u=0; u<subgrp->NUnits(); u++)
	{
		AIUnit *unit = subgrp->GetUnit(u);
		if (!unit) continue;
		FireResult result;
		if (unit->IsUnit())
		{
			EntityAIFull *veh=unit->GetVehicle();
      // TODO: which turret?
      TurretContext context;
      if (veh->GetPrimaryGunnerTurret(context))
      {
			  bool possible=veh->WhatFireResult(result, tar, context, 30.0)==EntityAIFull::FPCan;
			  if( possible )
			  {
				  somePossible = true;
				  somePotential = true;
			  }
			  else
			  {
				  float bestDist,minRange,maxRange;
				  if( veh->BestFireResult(result, tar, bestDist, minRange, maxRange,30, true) )
				  {
					  somePotential = true;
				  }
			  }
      }
		}
		else if( unit->IsInCargo() )
		{
			// unit may be cargo
			float bestDist,minRange,maxRange;
			EntityAIFull *veh=unit->GetPerson();
			if( veh->BestFireResult(result, tar, bestDist, minRange, maxRange,30, true) )
			{
				somePotential = true;
			}
		}
	}
	#if DIAG_ATTACK
	LogF("TryAttack %d %d",somePossible,somePotential);
	#endif
}


static void AttackInit(AISubgroupContext *context)
{
	STATE_PREFIX
	if (!subgrp->HasAI())
		return;

	AIUnit *leader = subgrp->Leader();
	cmd->_destination = leader->Position(leader->GetFutureVisualState());

	LogFSM(subgrp, "Attack", "INIT");

	subgrp->SetRefreshTime(Glob.time + SUBGROUP_REFRESH);
	subgrp->DoNotGo();
	if( subgrp!=subgrp->GetGroup()->MainSubgroup() )
	{
		//subgroup->GetGroup()->SendFormation();
		subgrp->SetFormation(AI::FormLine);
	}
}

static void CheckAttackInit(AISubgroupContext *context)
{
	STATE_PREFIX

	if (!IsAttackTarget(cmd,subgrp))
	{
		subgrp->Leader()->GetVehicle()->EndAttack();
		context->_fsm->SetState(SAttackDone, context);	// Success
		subgrp->ClearPlan();
		return;
	}
	Target *tgt=GetAttackTarget(cmd,subgrp->GetGroup());
	if( tgt && !CanAttack(subgrp,*tgt) )
	{
		subgrp->Leader()->GetVehicle()->EndAttack();
		context->_fsm->SetState(SAttackWait, context);
		return;
	}
	if (!subgrp->HasAI())
	{
		context->_fsm->SetState(SAttackAttack, context);
		subgrp->ClearPlan();
		return;
	}
	else
	{
		if( tgt )
		{
			subgrp->Leader()->GetVehicle()->BegAttack(tgt);
			#if DIAG_ATTACK
				LogF("Engage - attack command.");
			#endif
		}
		else
		{
			const AITargetInfo *info = NULL;
			AIGroup *grp = subgrp->GetGroup();
			AICenter *center = grp ? grp->GetCenter() : NULL;
			if (center)
			{
				info = center->FindTargetInfo(cmd->_target);
			}
			if (info)
			{
				cmd->_destination = info->_realPos;
			}
			else
			{
				subgrp->Leader()->GetVehicle()->EndAttack();
				context->_fsm->SetState(SAttackFail,context);	// target lost
				subgrp->ClearPlan();
				return;
			}
		}

		context->_fsm->SetState(SAttackFarMove, context);
		subgrp->ClearPlan();
		return;
	}
}

static void AttackFarMove(AISubgroupContext *context)
{
	STATE_PREFIX
	if (!subgrp->HasAI())
		return;

	LogFSM(subgrp, "Attack", "FARMOVE");

	subgrp->SetRefreshTime(Glob.time + 180); // no refresh

	subgrp->GoPlanned(cmd->_destination,cmd->_precision);
	context->_fsm->SetTimeOut(30);

	Vector3Val posL = subgrp->Leader()->Position(subgrp->Leader()->GetFutureVisualState());
	Vector3Val posD = cmd->_destination;
	float dist = 300;
	if ((posL - posD).SquareSizeXZ() > Square(dist))
	{
		for (int i=0; i<subgrp->NUnits(); i++)
		{
			AIUnit *unit = subgrp->GetUnit(i);
			if (unit)
				unit->OrderGetIn(true);
		}
	}
}


static void CheckAttackFarMove(AISubgroupContext *context)
{
	STATE_PREFIX

	if (!IsAttackTarget(cmd,subgrp))
	{
		subgrp->Leader()->GetVehicle()->EndAttack();
		context->_fsm->SetState(SAttackDone, context);	// Success
		subgrp->ClearPlan();
		return;
	}
	Target *tgt=GetAttackTarget(cmd,subgrp->GetGroup());
	if( tgt && !CanAttack(subgrp,*tgt) )
	{
		subgrp->Leader()->GetVehicle()->EndAttack();
		context->_fsm->SetState(SAttackWait, context);
		return;
	}
	if (!subgrp->HasAI())
	{
		context->_fsm->SetState(SAttackAttack, context);	// Attack
		return;
	}

	// refreshing when target position changed
	if (!AttackReplan(context))
	{
		// try to find some attack position
		// timeout when no attack possible
		if( context->_fsm->TimedOut() )
		{
			#if DIAG_ATTACK
			LogF("Far Move Replan Failed.");
			#endif
			// even when ran out of time, try finishing the attack, it will fail if not possible
			context->_fsm->SetState(SAttackAttack, context);
		}
		//return;
	}
	else
	{
    // if we have found any position, we cannot timeout easily any more
	  context->_fsm->SetTimeOut(30);
  }
  	
	EntityAI *lai=subgrp->Leader()->GetVehicle();
	Vector3Val posL = lai->FutureVisualState().Position();
	Vector3Val posD = cmd->_destination;
	float dist = 200;
	// Precision is FormationX()*0.25 by default
	// for tank it is 5 m
	saturateMax(dist, lai->GetPrecision()*10);
	saturateMin(dist, 1000);

	#if LOG_FSM
	if (subgrp && subgrp->GetGroup())
	LogF
	(
		"FSM: %s %s - state %s, dist %.1f posL %.1f,%.1f,%.1f posD %.1f,%.1f,%.1f",
		(const char *)subgrp->GetDebugName(),
		"ATTACK","MOVE",
		(posL - posD).SizeXZ(),
		posL[0],posL[1],posL[2], posD[0],posD[1],posD[2]
	);
	#endif

	if ((posL - posD).SquareSizeXZ() <= Square(dist) )
	{
		context->_fsm->SetState(SAttackMove, context);
		return;
	}

	if (subgrp->HasAI() && subgrp->AllUnitsCompleted())
	{
		CheckDistance(subgrp->Leader(), cmd->_destination);

		LogFSM(subgrp,"Attack","far move refresh");
		context->_fsm->Refresh(context);
		return;
	}
}

static void AttackMove(AISubgroupContext *context)
{
	STATE_PREFIX
	if (!subgrp->HasAI())
		return;

	LogFSM(subgrp, "Attack", "MOVE");

	// near attack position - get out from cargo
	AIGroup *grp=subgrp->GetGroup();

	for (int u=0; u<subgrp->NUnits(); u++)
	{
		AIUnit *unit = subgrp->GetUnit(u);
		if (!unit) continue;
		if (!unit->IsInCargo()) continue;
		Transport *veh = unit->GetVehicleIn();
		if (!veh) continue;
		AIBrain *driver = veh->DriverBrain();
		if (!driver || !driver->IsPlayer())
		{
			Target *tgt=GetAttackTarget(cmd,grp);
			if( tgt ) unit->AssignTarget(tgt);
			unit->OrderGetIn(false);
		}
	}

//	subgrp->SetRefreshTime(Glob.time + SUBGROUP_REFRESH);
	subgrp->SetRefreshTime(TIME_MAX);
	subgrp->GoPlanned(cmd->_destination,cmd->_precision);
	context->_fsm->SetTimeOut(30);
}

// how long should we wait when we have no ammo

static void CheckAttackMove(AISubgroupContext *context)
{
	STATE_PREFIX

	AIUnit *leader = subgrp->Leader();
	if (!IsAttackTarget(cmd,subgrp))
	{
		leader->GetVehicle()->EndAttack();
		context->_fsm->SetState(SAttackDone, context);	// Success
		subgrp->ClearPlan();
		return;
	}
	if (!subgrp->HasAI())
	{
		context->_fsm->SetState(SAttackAttack, context);	// Attack
		subgrp->ClearPlan();
		return;
	}

	#if DIAG_ATTACK
	LogF("FindTarget");
	#endif
	Target *tar=GetAttackTarget(cmd,subgrp->GetGroup());
	if( tar )
	{
		#if DIAG_ATTACK
		LogF("FindTarget OK");
		#endif
		bool somePossible,somePotential;
		TryAttack(somePossible,somePotential,subgrp, *tar);
		if( !somePotential )
		{
			#if DIAG_ATTACK
			LogF("Attack: No potential fire.");
			#endif
			context->_fsm->SetState(SAttackWait, context);
			return;
		}
	}

	// update attack position if necessary
	if (!AttackReplan(context))
	{
		// try to find some attack position
		// timeout when no attack possible
		if( context->_fsm->TimedOut() )
		{
			#if DIAG_ATTACK
			LogF("Attack: Move Replan Failed.");
			#endif
			// even when ran out of time, try finishing the attack, it will fail if not possible
			context->_fsm->SetState(SAttackAttack, context);
		}
		return;
	}
	
	

	EntityAI *lai=subgrp->Leader()->GetVehicle();
	Vector3Val posL = lai->FutureVisualState().Position();
	Vector3Val posD = cmd->_destination;
	if ((posL - posD).SquareSizeXZ() <= Square(lai->GetPrecision()*2))
	{
		if( tar && tar->IsKnown() )
		{
			// check if we have something planned
			EntityAI *lVehicle=subgrp->Leader()->GetVehicle();	
			if (lVehicle->AttackReady())
			{
				// target seen recently  - and path is planned
				#if DIAG_ATTACK
				LogF("Target seen - Attack");
				#endif
				context->_fsm->SetState(SAttackAttack, context);
			}
		}
		else
		{
			// target not seen, we are in position - attack, it will fail if needed
			#if DIAG_ATTACK
			LogF("Attack: Target not seen");
			#endif
			context->_fsm->SetState(SAttackAttack, context);
		}
		//subgrp->ClearPlan();
		return;
	}

	#if LOG_FSM
	if (subgrp && subgrp->GetGroup())
	LogF
	(
		"FSM: %s %s - state %s, dist %.1f posL %.1f,%.1f,%.1f posD %.1f,%.1f,%.1f",
		(const char *)subgrp->GetDebugName(),
		"ATTACK","MOVE",
		(posL - posD).SizeXZ(),
		posL[0],posL[1],posL[2], posD[0],posD[1],posD[2]
	);
	#endif
	if (subgrp->HasAI() && subgrp->AllUnitsCompleted())
	{
		CheckDistance(subgrp->Leader(), cmd->_destination);

		LogFSM(subgrp,"Attack","move refresh");
		context->_fsm->Refresh(context);
		return;
	}
}


static void AttackAttack(AISubgroupContext *context)
{
	STATE_PREFIX
	if (!subgrp->HasAI())
		return;

	LogFSM(subgrp, "Attack", "ATTACK");

	subgrp->SetRefreshTime(TIME_MAX);
	subgrp->DoNotGo();

	context->_fsm->SetTimeOut(10);

}

static void CheckAttackAttack(AISubgroupContext *context)
{
	STATE_PREFIX

	if (!IsAttackTarget(cmd,subgrp))
	{
		subgrp->Leader()->GetVehicle()->EndAttack();
		context->_fsm->SetState(SAttackDone, context);	// Success
		subgrp->ClearPlan();
		return;
	}

	#if DIAG_ATTACK
    EntityAI *lVehicle=subgrp->Leader()->GetVehicle();	
		LogF("Check fire %.1f,%.1f",lVehicle->Position().X(),lVehicle->Position().Z());
	#endif

	Target *tar=GetAttackTarget(cmd,subgrp->GetGroup());
	if (tar)
	{
		if( !tar->IsKnown() )
		{
			// target has not been seen for some time by any unit
			if (subgrp->HasAI())
			{
				// target disappeared - move to its last known position
				// check AI Center database

				AIGroup *grp = subgrp->GetGroup();
				AICenter *center = grp ? grp->GetCenter() : NULL;
				if (center)
				{
					const AITargetInfo *info = center->FindTargetInfo(cmd->_target);
					if (info)
					{
						cmd->_destination = info->_realPos;
					}
				}

        // if we are already there, we have no other option than to fail
	      EntityAI *lai=subgrp->Leader()->GetVehicle();
	      Vector3Val posL = lai->FutureVisualState().Position();
	      Vector3Val posD = cmd->_destination;
	      if ((posL - posD).SquareSizeXZ() <= Square(lai->GetPrecision()*2))
	      {
      		context->_fsm->SetState(SAttackWait, context); // Fail
      		return;
	      }
        
				context->_fsm->SetState(SAttackMove, context);
				return;			
			}
			else
			{ // non-AI attack - target not known
				// if target is not seen for long time, fail
				if( tar->GetLastSeen()<Glob.time-60 )
				{
			    #if DIAG_ATTACK
			    LogF("Attack: Target not seen for very long time");
			    #endif
					context->_fsm->SetState(SAttackWait, context);
					return;
				}
			}

		}


    // check if we can see the target - if not, we need to search for it
    // if the target position is not known, or the target was not seen long enough, we need to search for it

    // if the target info is old or inaccurate, search for the target
    if (subgrp->HasAI() && tar->GetLastSeen()<Glob.time-20 || tar->FadingPosAccuracy()>10)
    {
      AIBrain *leader = subgrp->Leader();
      // we have not seen it for quite a long time
      // pick up a "random" position on the circle around the last know target position
      float radius = tar->FadingPosAccuracy();
      Vector3 center = tar->LandAimingPosition(leader);
      // distribute various units around the circle
      // walk around the circle 4 ms/s
      const float speed = 4;
      const float minRadius = 4;
      float angularSpeed = radius>minRadius ? speed/radius : speed/minRadius;
      const float idAngle = (180-180.0f/11)*(H_PI/180);
      int id = leader->RandomSeed();
      float angle = Glob.time.toFloat()*angularSpeed + id*idAngle;
      Vector3 posOnCircle(sin(angle)*radius,0,cos(angle)*radius);
      Vector3 pos = posOnCircle + center;

      if (leader->FindNearestEmpty(pos))
      {
    #if _ENABLE_WALK_ON_GEOMETRY
        pos[1] = GLandscape->RoadSurfaceYAboveWater(pos.X(),pos.Z(), Landscape::FilterPrimary());
    #else
        pos[1] = GLandscape->RoadSurfaceYAboveWater(pos.X(),pos.Z());
    #endif
        pos[1] += leader->GetVehicle()->GetCombatHeight();

			  cmd->_destination = pos;

			  context->_fsm->SetState(SAttackMove, context);
        return;
      }
    }

		
		
		bool somePossible=false,somePotential=false;
		TryAttack(somePossible,somePotential,subgrp,*tar);
		if (!somePossible)
		{
			if( !somePotential )
			{
				// wait until last shot is gone
				subgrp->Leader()->GetVehicle()->EndAttack();
				LogFSM(subgrp,"Attack","Attack fail");
			  #if DIAG_ATTACK
			  LogF("Attack: fail - no potential fire");
			  #endif
				context->_fsm->SetState(SAttackWait, context); // Fail
				subgrp->ClearPlan();
				return;
			}
			if( subgrp->HasAI() )
			{
				// fire not possible
				if( context->_fsm->TimedOut() )
				{
			    #if DIAG_ATTACK
			    LogF("Attack: retry - timed out");
			    #endif
					LogFSM(subgrp, "Attack", "Retry - no fire possible.");
					context->_fsm->SetState(SAttackInit, context); // Retry
					//subgrp->ClearPlan();
					return;
				}
			}
		}
	}
	else
	{
		subgrp->Leader()->GetVehicle()->EndAttack();
    #if DIAG_ATTACK
    LogF("Attack: Failed - target out of range");
    #endif
		LogFSM(subgrp, "Attack", "Failed - target out of range.");
		context->_fsm->SetState(SAttackWait, context); // Fail
		//subgrp->ClearPlan();
		return;
	}
}

static void AttackWait(AISubgroupContext *context)
{
	STATE_PREFIX
	if (!subgrp->HasAI())
		return;
}

static void CheckAttackWait(AISubgroupContext *context)
{
	STATE_PREFIX

	// check if target is destroyed
	if (!IsAttackTarget(cmd,subgrp))
	{
		subgrp->Leader()->GetVehicle()->EndAttack();
		context->_fsm->SetState(SAttackDone, context);	// Success
		subgrp->ClearPlan();
		return;
	}

	EntityAIFull *lVehicle = subgrp->Leader()->GetVehicle();
	
  // if we placed a bomb, we should go away quickly
	if (lVehicle->HasPendingBomb() && subgrp->HasAI())
	{
		context->_fsm->SetState(SAttackRunAway, context);	// start running away
		subgrp->ClearPlan();
		return;
	}
	
	// wait until last shot at target is gone
	if (lVehicle->GetLastShotAtAssignedTarget()<Glob.time-20)
	{
		subgrp->Leader()->GetVehicle()->EndAttack();
		context->_fsm->SetState(SAttackFail, context);	// failed
		subgrp->ClearPlan();
		return;
	}
	
}

static void AttackRunAway(AISubgroupContext *context)
{
	STATE_PREFIX
	if (!subgrp->HasAI())
		return;

	AIUnit *leader = subgrp->Leader();
	EntityAI *lVehicle = leader->GetVehicle();
	// run away from target
	Target *tgt=GetAttackTarget(cmd,subgrp->GetGroup());
	if( tgt )
	{

		Vector3 tgtPos = tgt->LandAimingPosition();
		Vector3 pos = lVehicle->FutureVisualState().Position();
		AIUnit *gLeader = subgrp->GetGroup()->Leader();
		if (gLeader!=leader)
		{
			// prefer running in group leader direction
			pos = gLeader->Position(gLeader->GetFutureVisualState());
		}
		else
		{
			// TODO: if leader is me, select some safe place
			// (perhaps direction to first waypoint
		}
		Vector3 dir = pos-tgtPos;
		dir.Normalize();

		Vector3 posD = tgtPos+dir*100;
		leader->FindNearestEmpty(posD);
	
		subgrp->SetRefreshTime(Glob.time + 60);
		subgrp->GoPlanned(posD);
	}
}

static void CheckAttackRunAway(AISubgroupContext *context)
{
	STATE_PREFIX
	if (!subgrp->HasAI())
	{
		subgrp->Leader()->GetVehicle()->EndAttack();
		context->_fsm->SetState(SAttackFail, context);	// failed
		subgrp->ClearPlan();
		return;
	}

	if (!IsAttackTarget(cmd,subgrp))
	{
		subgrp->Leader()->GetVehicle()->EndAttack();
		context->_fsm->SetState(SAttackDone, context);	// Success
		subgrp->ClearPlan();
		return;
	}

  // we are running away - this must be because someone has planted bombs
  // once the bombs went off, the state is done, no matter if target was destroyed or not
	EntityAIFull *lVehicle = subgrp->Leader()->GetVehicle();
	if (!lVehicle->HasPendingBomb() && subgrp->HasAI())
  {
    // wait for a while - check the explosion results, let secondary explosions expire
    // this also gives opportunity to lay down
    context->_fsm->VarTime(0)= Glob.time + 15;
		context->_fsm->SetState(SAttackWaitAfterExplo, context);
    subgrp->Stop();
		subgrp->ClearPlan();
		return;
  }

	// wait until leader is at given position
	if (subgrp->AllUnitsCompleted())
	{
		CheckDistance(subgrp->Leader(), cmd->_destination);

		// if there is a pipe bomb waiting, touch it off
    lVehicle->TouchOffBombs();

    // wait for a while - check the explosion results, let secondary explosions expire
    // this also gives opportunity to lay down
    context->_fsm->VarTime(0)= Glob.time + 15;
    context->_fsm->SetState(SAttackWaitAfterExplo, context);
    subgrp->Stop();
		subgrp->ClearPlan();
		return;
	}
}

static void AttackWaitAfterExplo(AISubgroupContext *context)
{
	STATE_PREFIX

  // we do not want set time here, as this may be executed again as a result of exposure change
}


static void CheckAttackWaitAfterExplo(AISubgroupContext *context)
{
	STATE_PREFIX

	if (!subgrp->HasAI())
	{
		subgrp->Leader()->GetVehicle()->EndAttack();
		context->_fsm->SetState(SAttackFail, context);	// failed
		subgrp->ClearPlan();
		return;
	}

	if (!IsAttackTarget(cmd,subgrp))
	{
		subgrp->Leader()->GetVehicle()->EndAttack();
		context->_fsm->SetState(SAttackDone, context);	// Success
		subgrp->ClearPlan();
		return;
	}

  if (Glob.time>context->_fsm->VarTime(0))
  {
		subgrp->Leader()->GetVehicle()->EndAttack();
		context->_fsm->SetState(SAttackInit, context);	// start again, if necessary
		subgrp->ClearPlan();
  }


}

static AISubgroupFSM::StateInfo attackStates[] =
{
	AISubgroupFSM::StateInfo("Init", AttackInit, CheckAttackInit),
	AISubgroupFSM::StateInfo("FarMove", AttackFarMove, CheckAttackFarMove),
	AISubgroupFSM::StateInfo("Move", AttackMove, CheckAttackMove),
	AISubgroupFSM::StateInfo("Attack", AttackAttack, CheckAttackAttack),
	AISubgroupFSM::StateInfo("Wait", AttackWait, CheckAttackWait),
	AISubgroupFSM::StateInfo("RunAway", AttackRunAway, CheckAttackRunAway),
	AISubgroupFSM::StateInfo("WaitAfterExplo", AttackWaitAfterExplo, CheckAttackWaitAfterExplo),
	AISubgroupFSM::StateInfo("Succeed", CommandSucceed, CheckCommandSucceed),
	AISubgroupFSM::StateInfo("Failed", CommandFailed, CheckCommandFailed)
};

/*!
\patch 1.53 Date 3/8/2002 by Ondra
- Fixed: AI units when both Engage at will and Hold fire were given.
*/

static void AttackEnterEnableFire(AISubgroupContext *context, bool fire)
{
	STATE_PREFIX
	//if (!subgrp->HasAI()) return;

	Target *tgt=GetAttackTarget(cmd,subgrp->GetGroup());
	if( !tgt ) return;

	for (int u=0; u<subgrp->NUnits(); u++)
	{
		AIUnit *unit = subgrp->GetUnit(u);
		if (unit)
		{
			unit->AssignTarget(tgt);
			if (fire) unit->EnableFireTarget(tgt);
		}
	}
}

static void AttackExitEnableFire(AISubgroupContext *context, bool fire)
{
	STATE_PREFIX
	//if (!subgrp->HasAI()) return;

	Target *tgt=GetAttackTarget(cmd,subgrp->GetGroup());
	if( !tgt ) return;

	for (int u=0; u<subgrp->NUnits(); u++)
	{
		AIUnit *unit = subgrp->GetUnit(u);
		if (unit)
		{
			unit->AssignTarget(NULL);
			if (fire) unit->EnableFireTarget(NULL);
		}
	}
}

static void AttackFireEnter(AISubgroupContext *context)
{
	AttackEnterEnableFire(context,true);
}
static void AttackFireExit(AISubgroupContext *context)
{
	AttackExitEnableFire(context,true);
}

static void AttackEnter(AISubgroupContext *context)
{
	AttackEnterEnableFire(context,false);
}

static void AttackExit(AISubgroupContext *context)
{
	AttackExitEnableFire(context,false);
}

static AISubgroupFSM::pStateFunction attackSpecial[] =
{
	AttackEnter,
	AttackExit
};

static AISubgroupFSM::pStateFunction attackFireSpecial[] =
{
	AttackFireEnter,
	AttackFireExit
};

///////////////////////////////////////////////////////////////////////////////
// Command Hide FSM

enum
{
	SHideInit,
	SHideMove,
	SHideFail
};

static void HideInit(AISubgroupContext *context)
{
	INIT_PREFIX
	if (!subgrp->HasAI())
		return;

	LogFSM(subgrp, "Hide", "INIT");

	subgrp->SetRefreshTime(TIME_MAX);
	subgrp->DoNotGo();
}

static void CheckHideInit(AISubgroupContext *context)
{
	STATE_PREFIX
	
	context->_fsm->SetState(SHideMove, context);
	subgrp->ClearPlan();
}

static void HideMove(AISubgroupContext *context)
{
	STATE_PREFIX
	if (!subgrp->HasAI())
		return;

	LogFSM(subgrp, "Hide", "MOVE");

	subgrp->SetRefreshTime(TIME_MAX);
	
	// each unit should scan its surroundings for cover
	
  AIUnit *leader = subgrp->Leader();
	EntityAI *veh = leader->GetVehicle();
	
	// make sure there is some allowance for hiding
	// to force some planning, tell each unit to go slightly back
	// TODO: find some other wat to force planning
	Vector3Val forward = subgrp->GetFormationDirection();
	subgrp->GoPlanned(veh->FutureVisualState().Position()-forward*5,floatMax(cmd->_precision,50));
}

static void CheckHideMove(AISubgroupContext *context)
{
  // hide never completes - leader must cancel it
}



static AISubgroupFSM::StateInfo hideStates[] =
{
	AISubgroupFSM::StateInfo("Init", HideInit, CheckHideInit),
	AISubgroupFSM::StateInfo("Move", HideMove, CheckHideMove),
	AISubgroupFSM::StateInfo("Failed", CommandFailed, CheckCommandFailed)
};


///////////////////////////////////////////////////////////////////////////////
// Command Fire FSM

enum
{
	SFireInit,
	SFireFire,
	SFireDone,SFireFail
};

static void FireInit(AISubgroupContext *context)
{
	INIT_PREFIX
	Command *cmd = context->_task; \
	Assert(cmd); \
	if (!subgrp->HasAI())
		return;

	LogFSM(subgrp, "Fire", "INIT");

	subgrp->SetRefreshTime(Glob.time + SUBGROUP_REFRESH);
	subgrp->DoNotGo();

	for (int u=0; u<subgrp->NUnits(); u++)
	{
		AIUnit *unit = subgrp->GetUnit(u);
		if (!unit || !unit->IsUnit()) 
			continue;
		Target *tgt=GetAttackTarget(cmd,subgrp->GetGroup());
		if( tgt ) unit->AssignTarget(tgt);
	}
}

static void CheckFireInit(AISubgroupContext *context)
{
	STATE_PREFIX

	if (!IsAttackTarget(cmd,subgrp))
	{
		context->_fsm->SetState(SFireDone, context);	// Success
		subgrp->ClearPlan();
		return;
	}
	context->_fsm->SetState(SFireFire, context);
	subgrp->ClearPlan();
	return;
}

static void FireFire(AISubgroupContext *context)
{
	STATE_PREFIX
	if (!subgrp->HasAI())
		return;

	LogFSM(subgrp, "Fire", "FIRE");

	subgrp->SetRefreshTime(TIME_MAX);
	subgrp->Stop();
}

static void CheckFireFire(AISubgroupContext *context)
{
	STATE_PREFIX

	if (!IsAttackTarget(cmd,subgrp))
	{
		context->_fsm->SetState(SFireDone, context);	// Success
		subgrp->ClearPlan();
		return;
	}

	Target *tar=GetAttackTarget(cmd,subgrp->GetGroup());
	bool somePossible = false;
	if (tar)
	{
		if (!subgrp->HasAI())
		{
			// check out of ammo
			bool outOfAmmo=true;
			for (int u=0; u<subgrp->NUnits(); u++)
			{
				AIUnit *unit = subgrp->GetUnit(u);
				if (!unit || !unit->IsUnit()) 
					continue;
				EntityAIFull *vehicle=unit->GetVehicle();
        // TODO: which turret?
        TurretContext context;
        if (vehicle->GetPrimaryGunnerTurret(context))
        {
          if( cmd->_intParam>=0 )
          {
            const Magazine *magazine = context._weapons->_magazineSlots[cmd->_intParam]._magazine;
            if (magazine && magazine->GetAmmo()>0 ) outOfAmmo=false;
          }
          else
          {
            for (int w=0; w<context._weapons->_magazineSlots.Size(); w++)
            {
              const Magazine *magazine = context._weapons->_magazineSlots[w]._magazine;
              if (magazine && magazine->GetAmmo()>0 ) outOfAmmo=false;
            }
          }
        }
			}
			if( outOfAmmo )
			{
				context->_fsm->SetState(SFireFail, context); // Fail
				subgrp->ClearPlan();
			}
			return;
		}
		for (int u=0; u<subgrp->NUnits(); u++)
		{
			AIUnit *unit = subgrp->GetUnit(u);
			if (!unit || !unit->IsUnit()) 
				continue;
      EntityAIFull *vehicle=unit->GetVehicle();
      // TODO: which turret?
      TurretContext context;
      if (vehicle->GetPrimaryGunnerTurret(context))
      {
        if (cmd->_intParam >= 0)
        {
          const Magazine *magazine = context._weapons->_magazineSlots[cmd->_intParam]._magazine;
          if (magazine && magazine->GetAmmo()>0 )
          {
            //vehicle->FireAt(cmd->_param, *tar);
            somePossible = true;
          }
        }
        else
        {
          FireResult result;
          bool possible = vehicle->WhatFireResult(result, *tar, context, 30.0)==EntityAIFull::FPCan;
          if( possible )
          {
            somePossible = true;
          }
        }
      }
		}
		if (!somePossible)
		{
			// no unit can fire - fail
			LogFSM(subgrp, "Fire", "Failed - no fire possible.");
			context->_fsm->SetState(SFireFail, context); // Fail
			subgrp->ClearPlan();
			return;
		}
	}
	else
	{
		LogFSM(subgrp, "Fire", "Failed - target out of range.");
		context->_fsm->SetState(SFireFail, context); // Fail
		subgrp->ClearPlan();
		return;
	}
}

static AISubgroupFSM::StateInfo fireStates[] =
{
	AISubgroupFSM::StateInfo("Init", FireInit, CheckFireInit),
	AISubgroupFSM::StateInfo("Fire", FireFire, CheckFireFire),
	AISubgroupFSM::StateInfo("Succeed", CommandSucceed, CheckCommandSucceed),
	AISubgroupFSM::StateInfo("Failed", CommandFailed, CheckCommandFailed)
};

///////////////////////////////////////////////////////////////////////////////
// Command Fire At Position FSM

enum
{
  SFireAtPositionInit,
  SFireAtPositionFire,
  SFireAtPositionFireEnd,
  SFireAtPositionDone,SFireAtPositionFail
};

static void FireAtPositionRemoveTarget(AISubgroupContext *context)
{
  STATE_PREFIX
  //Unassign target
  Target *tgt=GetAttackTarget(cmd,subgrp->GetGroup());
  for (int u=0; u<subgrp->NUnits(); u++)
  {
    AIUnit *unit = subgrp->GetUnit(u);
    if (!unit || !unit->IsUnit()) 
      continue;
    if( tgt ) unit->AssignTarget(NULL);
  }

  if(subgrp->Leader() &&  subgrp->Leader()->GetGroup())
  {
    AIGroup *group = subgrp->Leader()->GetGroup();
    bool needed = false;
    for (int u=0; u<group->NUnits(); u++)
    {
      AIUnit *unit = group->GetUnit(u);
      if (!unit || !unit->IsUnit()) 
        continue;
      if(tgt == unit->GetTargetAssigned()) needed = true;
    }
  
    if(!needed && tgt && tgt->idExact && !tgt->idExact->ToDelete())
    {
      NetworkId  tgtId = tgt->idExact->GetNetworkId();
      GetNetworkManager().DeleteObject(tgtId);
      tgt->idExact->SetDelete();
    }
  }
}

static void FireAtPositionInit(AISubgroupContext *context)
{
  INIT_PREFIX
    Command *cmd = context->_task; \
    Assert(cmd); \
    if (!subgrp->HasAI())
      return;

  LogFSM(subgrp, "Fire", "INIT");

  for (int u=0; u<subgrp->NUnits(); u++)
  {
    AIUnit *unit = subgrp->GetUnit(u);
    if (!unit || !unit->IsUnit()) 
      continue;
    Target *tgt=GetAttackTarget(cmd,subgrp->GetGroup());
    if( tgt ) unit->AssignTarget(tgt);
  }

  subgrp->SetRefreshTime(Glob.time + SUBGROUP_REFRESH);
  subgrp->Stop();
}

static void CheckFireAtPositionInit(AISubgroupContext *context)
{
  STATE_PREFIX

    if (!IsAttackTarget(cmd,subgrp))
    {
      context->_fsm->SetState(SFireAtPositionDone, context);	// Success
      subgrp->ClearPlan();

      FireAtPositionRemoveTarget(context);

      return;
    }
    context->_fsm->SetState(SFireAtPositionFire, context);
    subgrp->ClearPlan();

    return;
}

static void FireAtPositionFire(AISubgroupContext *context)
{
  STATE_PREFIX
    if (!subgrp->HasAI())
      return;

  LogFSM(subgrp, "Fire", "FIRE");

  subgrp->SetRefreshTime(TIME_MAX);
  subgrp->DoNotGo();
}

static void CheckFireAtPositionFire(AISubgroupContext *context)
{
  STATE_PREFIX

    if (!IsAttackTarget(cmd,subgrp))
    {
      context->_fsm->SetState(SFireAtPositionDone, context);	// Success
      subgrp->ClearPlan();

      FireAtPositionRemoveTarget(context);
      return;
    }

    Target *tar=GetAttackTarget(cmd,subgrp->GetGroup());
    bool somePossible = false;
    if (tar)
    {
      {
        // check out of ammo
        bool outOfAmmo=true;
        bool isArtillery=true;
        for (int u=0; u<subgrp->NUnits(); u++)
        {
          AIUnit *unit = subgrp->GetUnit(u);
          if (!unit || !unit->IsUnit()) 
            continue;
          EntityAIFull *vehicle=unit->GetVehicle();
          // TODO: which turret?
          TurretContext context;
          if (!vehicle || !vehicle->GetType()->GetArtilleryScanner()) 
          {
            isArtillery = false;
            break;
          }
          if (vehicle->GetPrimaryGunnerTurret(context))
          {
            if( cmd->_intParam>=0 )
            {
              const Magazine *magazine = context._weapons->_magazineSlots[cmd->_intParam]._magazine;
              if (magazine && magazine->GetAmmo()>0 ) outOfAmmo=false;
            }
            else
            {
              for (int w=0; w<context._weapons->_magazineSlots.Size(); w++)
              {
                const Magazine *magazine = context._weapons->_magazineSlots[w]._magazine;
                if (magazine && magazine->GetAmmo()>0 ) outOfAmmo=false;
              }
            }
          }
        }

        if(!isArtillery)
        {
          LogFSM(subgrp, "Fire", "Failed - target out of range.");
          context->_fsm->SetState(SFireAtPositionFail, context); // Fail
          subgrp->ClearPlan();

          //Unassign target
          FireAtPositionRemoveTarget(context);
          return;
        }

        if( outOfAmmo )
        {
          context->_fsm->SetState(FSM::FinalState, context); // Fail
          if(subgrp && subgrp->GetGroup() && subgrp->Leader())
            subgrp->GetGroup()->ReportArtilleryFire(subgrp->Leader(),1);
          subgrp->ClearPlan();

          //Unassign target
          FireAtPositionRemoveTarget(context);
          return;
        }
      }
      for (int u=0; u<subgrp->NUnits(); u++)
      {
        AIUnit *unit = subgrp->GetUnit(u);
        if (!unit || !unit->IsUnit()) 
          continue;
        EntityAIFull *vehicle=unit->GetVehicle();
        // TODO: which turret?
        TurretContextV turretContext;
        if (vehicle && vehicle->GetPrimaryGunnerTurret(unconst_cast(vehicle->FutureVisualState()), turretContext))
        {
          if (cmd->_intParam >= 0)
          {
            const Magazine *magazine = turretContext._weapons->_magazineSlots[cmd->_intParam]._magazine;
            if (magazine && magazine->GetAmmo()>0 )
            {
              //vehicle->FireAt(cmd->_param, *tar);
              somePossible = true;
            }
          }
          else
          {
            FireResult result;
            bool possible = vehicle->WhatFireResult(result, *tar, turretContext, 30.0)==EntityAIFull::FPCan;
            if( possible)
            {
              somePossible = true;
              int selected = turretContext._weapons->_currentWeapon;

              //if(turretContext._weapons->_fire._fireTarget)
              //{
              //  float distance =  turretContext._weapons->_fire._fireTarget->GetPosition().Distance(vehicle->Position());
              //  for(int i=0; i< turretContext._weapons->_magazineSlots.Size(); i++)
              //  {//select proper fire mode
              //    MagazineSlot slot = turretContext._weapons->_magazineSlots[i];
              //    const WeaponModeType *mode = turretContext._weapons->GetWeaponMode(i);
              //    if(mode && (mode->minRange < distance && mode->maxRange > distance))
              //    {
              //      if(slot._magazine && slot._magazine->_type->_ammo && slot._magazine->_type->_ammo->artilleryLock)
              //      {
              //        turretContext._weapons->SelectWeapon(vehicle,i);
              //        selected = i;
              //        break;
              //      }
              //    }
              //  }
              //}

              // check if weapon is aimed
              if
                (
                vehicle->GetWeaponLoaded(*turretContext._weapons, selected) 
                && vehicle->GetWeaponReady(*turretContext._weapons, selected, turretContext._weapons->_fire._fireTarget)
                )
              {
                float aimed = vehicle->GetAimed(turretContext, selected, turretContext._weapons->_fire._fireTarget, true, true);
                if( aimed >= 0.75)
                {
                  //weapon is not locked
                  if(!vehicle->GetWeaponLockReady(turretContext, turretContext._weapons->_fire._fireTarget, selected, aimed)) return;

                  if (!vehicle->GetAIFireEnabled(turretContext._weapons->_fire._fireTarget)) vehicle->ReportFireReady();
                  else
                  {
                    vehicle->FireWeapon(turretContext, selected, turretContext._weapons->_fire._fireTarget->idExact, false);
                  
                    context->_fsm->SetState(SFireAtPositionFireEnd, context);	// Success
                    subgrp->ClearPlan();
                  }
                  //    LogF("Fired: aimed %.3f", GetAimed(selected, weapons._fire._fireTarget));
                  //    _firePrepareOnly = true;
                  turretContext._weapons->_fire._fireCommanded = false;
                }
              }
            }
          }
        }
      }
      if (!somePossible)
      {
        // no unit can fire - fail
        LogFSM(subgrp, "Fire", "Failed - no fire possible.");
        context->_fsm->SetState(FSM::FinalState, context); // Fail
        if(subgrp && subgrp->GetGroup() && subgrp->Leader())
          subgrp->GetGroup()->ReportArtilleryFire(subgrp->Leader(),2);
        subgrp->ClearPlan();

        //Unassign target
        FireAtPositionRemoveTarget(context);

        return;
      }
    }
    else
    {
      LogFSM(subgrp, "Fire", "Failed - target out of range.");
      context->_fsm->SetState(SFireAtPositionFail, context); // Fail
      subgrp->ClearPlan();

      //Unassign target
      FireAtPositionRemoveTarget(context);

      return;
    }
}

static void CheckFireAtPositionFireEnd(AISubgroupContext *context)
{
  STATE_PREFIX

  if (!IsAttackTarget(cmd,subgrp))
  {
    context->_fsm->SetState(SFireDone, context);	// Success
    subgrp->ClearPlan();

    //Unassign target
    FireAtPositionRemoveTarget(context);

    return;
  }

  Target *tar=GetAttackTarget(cmd,subgrp->GetGroup());
  if (tar)
  {
    {
      // check out of ammo
      bool outOfAmmo=true;
      for (int u=0; u<subgrp->NUnits(); u++)
      {
        AIUnit *unit = subgrp->GetUnit(u);
        if (!unit || !unit->IsUnit()) 
          continue;
        EntityAIFull *vehicle=unit->GetVehicle();
        // TODO: which turret?
        TurretContext context;
        if (vehicle->GetPrimaryGunnerTurret(context))
        {
          if( cmd->_intParam>=0 )
          {
            const Magazine *magazine = context._weapons->_magazineSlots[cmd->_intParam]._magazine;
            if (magazine && magazine->GetAmmo()>0  && magazine->_burstLeft>0) outOfAmmo=false;
          }
          else
          {
            for (int w=0; w<context._weapons->_magazineSlots.Size(); w++)
            {
              const Magazine *magazine = context._weapons->_magazineSlots[w]._magazine;
              if (magazine && magazine->GetAmmo()>0 && magazine->_burstLeft>0) outOfAmmo=false;
            }
          }
        }
      }
      if( outOfAmmo )
      {
        context->_fsm->SetState(FSM::FinalState, context); // Fail
        if(subgrp && subgrp->GetGroup() && subgrp->Leader())
          subgrp->GetGroup()->ReportArtilleryFire(subgrp->Leader(),0);
        subgrp->ClearPlan();

        //Unassign target
        FireAtPositionRemoveTarget(context);
      }
      return;
    }
  }
  else
  {
    LogFSM(subgrp, "Fire", "Failed - target out of range.");
    context->_fsm->SetState(SFireAtPositionDone, context); // Fail
    subgrp->ClearPlan();

    //Unassign target
    FireAtPositionRemoveTarget(context);

    return;
  }
}

static void FireAtPositionFireEnd(AISubgroupContext *context)
{
  STATE_PREFIX
    if (!subgrp->HasAI())
      return;

  LogFSM(subgrp, "FireEnd", "FIREEND");

  subgrp->SetRefreshTime(TIME_MAX);
  subgrp->DoNotGo();
}

static void FireAtPositionEnter(AISubgroupContext *context)
{
  STATE_PREFIX
    if (!subgrp->HasAI())
      return;

  // set leader direction
  subgrp->Stop();
}

static void FireAtPositionExit(AISubgroupContext *context)
{
  STATE_PREFIX

  // check out of ammo
  for (int u=0; u<subgrp->NUnits(); u++)
  {
    AIUnit *unit = subgrp->GetUnit(u);
    if (!unit || !unit->IsUnit()) 
      continue;
    EntityAIFull *vehicle=unit->GetVehicle();
    // TODO: which turret?
    TurretContext context;
    if (vehicle->GetPrimaryGunnerTurret(context))
    {
      for (int w=0; w<context._weapons->_magazineSlots.Size(); w++)
      {
        Magazine *magazine = context._weapons->_magazineSlots[w]._magazine;
        if (magazine) magazine->_burstLeft = 0;
      }
    }
  }

  //Unassign target
  FireAtPositionRemoveTarget(context);
}

static AISubgroupFSM::StateInfo fireAtPositionStates[] =
{
  AISubgroupFSM::StateInfo("Init", FireAtPositionInit, CheckFireAtPositionInit),
  AISubgroupFSM::StateInfo("Fire", FireAtPositionFire, CheckFireAtPositionFire),
  AISubgroupFSM::StateInfo("FireEnd", FireAtPositionFireEnd, CheckFireAtPositionFireEnd),
  AISubgroupFSM::StateInfo("Succeed", CommandSucceed, CheckCommandSucceed),
  AISubgroupFSM::StateInfo("Failed", CommandFailed, CheckCommandFailed)
};

static AISubgroupFSM::pStateFunction fireAtPositionSpecial[] =
{
  FireAtPositionEnter,
  FireAtPositionExit
};

///////////////////////////////////////////////////////////////////////////////
// Command Move FSM

enum
{
	SMoveInit,SMoveMove,SMoveDone,SMoveFail
};

static void MoveInit(AISubgroupContext *context)
{
	INIT_PREFIX
	if (!subgrp->HasAI())
		return;

	LogFSM(subgrp, "Move", "INIT");

	subgrp->SetRefreshTime(TIME_MAX);
	subgrp->DoNotGo();
}

static void CheckMoveInit(AISubgroupContext *context)
{
	STATE_PREFIX
	
	context->_fsm->SetState(SMoveMove, context);
	subgrp->ClearPlan();
}

static void MoveMove(AISubgroupContext *context)
{
	STATE_PREFIX
	if (!subgrp->HasAI())
		return;

	LogFSM(subgrp, "Move", "MOVE");

	subgrp->SetRefreshTime(TIME_MAX);
	subgrp->GoPlanned(cmd->_destination,cmd->_precision,cmd->_movement==Command::MoveFastIntoCover);

	Vector3Val posL = subgrp->Leader()->Position(subgrp->Leader()->GetFutureVisualState());
	Vector3Val posD = cmd->_destination;
	float dist = 200;
	if ((posL - posD).SquareSizeXZ() > Square(dist))
	{
		for (int i=0; i<subgrp->NUnits(); i++)
		{
			AIUnit *unit = subgrp->GetUnit(i);
			if (unit)
				unit->OrderGetIn(true);
		}
	}
}

static void CheckMoveMove(AISubgroupContext *context)
{
	STATE_PREFIX

	if (!subgrp->HasAI())
	{
		Vector3Val posL = subgrp->Leader()->Position(subgrp->Leader()->GetFutureVisualState());
		Vector3Val posD = cmd->_destination;
		float maxDistance=10;
		if( subgrp->Leader()->IsFreeSoldier() ) maxDistance=2.5;
		if ((posL - posD).SquareSizeXZ() <= Square(maxDistance))
		{
			context->_fsm->SetState(SMoveDone, context);
			subgrp->ClearPlan();
		}
		return;
	}

	if (subgrp->GetMode() != AISubgroup::PlanAndGo)
  {
    // Multiplayer - Disconnected player's unit becomes AI
    // reinit state
    MoveMove(context);
  }
	if (subgrp->AllUnitsCompleted())
	{
		CheckDistance(subgrp->Leader(), cmd->_destination);

		context->_fsm->SetState(SMoveDone, context);
		subgrp->ClearPlan();
	}
}

static AISubgroupFSM::StateInfo moveStates[] =
{
	AISubgroupFSM::StateInfo("Init", MoveInit, CheckMoveInit),
	AISubgroupFSM::StateInfo("Move", MoveMove, CheckMoveMove),
	AISubgroupFSM::StateInfo("Succeed", CommandSucceed, CheckCommandSucceed),
	AISubgroupFSM::StateInfo("Failed", CommandFailed, CheckCommandFailed)
};

///////////////////////////////////////////////////////////////////////////////
// Command Stop FSM

enum
{
	SStopInit,SStopStop,SStopDone,SStopFail
};

static void StopEnter(AISubgroupContext *context)
{
	STATE_PREFIX
	if (!subgrp->HasAI())
		return;

	// set leader direction

	subgrp->Stop();
}

static void StopExit(AISubgroupContext *context)
{
	STATE_PREFIX
}

static void StopInit(AISubgroupContext *context)
{
	INIT_PREFIX
	if (!subgrp->HasAI())
		return;

	LogFSM(subgrp, "Stop", "INIT");

	subgrp->SetRefreshTime(TIME_MAX);
	subgrp->Stop();
}

static void CheckStopInit(AISubgroupContext *context)
{
	STATE_PREFIX
	
	context->_fsm->SetState(SStopStop, context);
	subgrp->ClearPlan();
}

static void StopStop(AISubgroupContext *context)
{
	// do nothing
}

static void CheckStopStop(AISubgroupContext *context)
{
}


static AISubgroupFSM::StateInfo stopStates[] =
{
	AISubgroupFSM::StateInfo("Init", StopInit, CheckStopInit),
	AISubgroupFSM::StateInfo("Stop", StopStop, CheckStopStop),
	AISubgroupFSM::StateInfo("Succeed", CommandSucceed, CheckCommandSucceed),
	AISubgroupFSM::StateInfo("Failed", CommandFailed, CheckCommandFailed)
};

///////////////////////////////////////////////////////////////////////////////
// Command Expect FSM

enum
{
	SExpectInit,SExpectExpect,SExpectDone,SExpectFail
};

static void ExpectEnter(AISubgroupContext *context)
{
	STATE_PREFIX
	if (!subgrp->HasAI())
		return;

	// set leader direction

	subgrp->DoNotGo();
}

static void ExpectExit(AISubgroupContext *context)
{
	STATE_PREFIX
}

static void ExpectInit(AISubgroupContext *context)
{
	INIT_PREFIX
	if (!subgrp->HasAI())
		return;

	LogFSM(subgrp, "Expect", "INIT");

	subgrp->SetRefreshTime(TIME_MAX);
	subgrp->DoNotGo();
}

static void CheckExpectInit(AISubgroupContext *context)
{
	STATE_PREFIX
	
	context->_fsm->SetState(SExpectExpect, context);
	subgrp->ClearPlan();
}

static void ExpectExpect(AISubgroupContext *context)
{
	// do nothing
}

static void CheckExpectExpect(AISubgroupContext *context)
{
	// check if we are in formation

}

static AISubgroupFSM::StateInfo expectStates[] =
{
	AISubgroupFSM::StateInfo("Init", ExpectInit, CheckExpectInit),
	AISubgroupFSM::StateInfo("Expect", ExpectExpect, CheckExpectExpect),
	AISubgroupFSM::StateInfo("Succeed", CommandSucceed, CheckCommandSucceed),
	AISubgroupFSM::StateInfo("Failed", CommandFailed, CheckCommandFailed)
};

///////////////////////////////////////////////////////////////////////////////
// Command Dismiss FSM

enum
{
  SDismissInit, SDismissSelectTask, SDismissMove, SDismissRelax, SDismissCleanUp, SDismissDone
};

static void DismissEnter(AISubgroupContext *context)
{
  INIT_PREFIX

  if (subgrp->HasAI())
  {
    subgrp->SetSpeedMode(SpeedLimited);
  }
}

static void DismissExit(AISubgroupContext *context)
{
  INIT_PREFIX

  if (subgrp->HasAI())
  {
    subgrp->SetSpeedMode(SpeedNormal);
  }
}

static void DismissInit(AISubgroupContext *context)
{
  STATE_PREFIX

  LogFSM(subgrp, "Dismiss", "INIT");
  if (!subgrp->HasAI()) return;

  subgrp->SetRefreshTime(TIME_MAX);
  subgrp->SetSpeedMode(SpeedLimited);
  subgrp->Stop();
  cmd->_destination = subgrp->Leader()->Position(subgrp->Leader()->GetFutureVisualState());
}

static void CheckDismissInit(AISubgroupContext *context)
{
  STATE_PREFIX

  for (int i=0; i<subgrp->NUnits(); i++)
  {
    AIUnit *unit = subgrp->GetUnit(i);
    if (unit && unit->GetVehicleIn())
    {
      // command not supported for units in vehicles
      context->_fsm->SetState(SDismissDone, context);
      return;
    }
  }

  context->_fsm->SetState(SDismissSelectTask, context);
}

static void DismissSelectTask(AISubgroupContext *context)
{
  INIT_PREFIX

  LogFSM(subgrp, "Dismiss", "SELECT TASK");
}

static void CheckDismissSelectTask(AISubgroupContext *context)
{
  STATE_PREFIX

  // check if some unit did not reach combat mode
  for (int i=0; i<subgrp->NUnits(); i++)
  {
    AIUnit *unit = subgrp->GetUnit(i);
    if (unit && unit->GetCombatMode() > CMAware)
    {
      // command not supported for units in vehicles
      context->_fsm->SetState(SDismissDone, context);
      return;
    }
  }

  // random task selection
  if (!subgrp->HasAI() || GRandGen.RandomValue() > 0.3)
    context->_fsm->SetState(SDismissRelax, context);
  else
    context->_fsm->SetState(SDismissMove, context);
}

static void DismissMove(AISubgroupContext *context)
{
  STATE_PREFIX

  LogFSM(subgrp, "Dismiss", "MOVE");
  if (!subgrp->HasAI()) return;

  const float territory = 300.0f;

  Vector3 pos;
  pos[0] = cmd->_destination[0] + territory * (GRandGen.RandomValue() - 0.5);
  pos[2] = cmd->_destination[2] + territory * (GRandGen.RandomValue() - 0.5);
  subgrp->Leader()->FindNearestEmpty(pos);
  pos[1] = GLandscape->RoadSurfaceYAboveWater(pos[0], pos[2]);

  // no great precision needed here
  subgrp->GoPlanned(pos,territory*0.2f);
}

static void CheckDismissMove(AISubgroupContext *context)
{
  STATE_PREFIX

  // check if some unit did not reach combat mode
  for (int i=0; i<subgrp->NUnits(); i++)
  {
    AIUnit *unit = subgrp->GetUnit(i);
    if (unit && unit->GetCombatMode() > CMAware)
    {
      // command not supported for units in vehicles
      context->_fsm->SetState(SDismissDone, context);
      return;
    }
  }

  if (!subgrp->HasAI())
  {
    context->_fsm->SetState(SDismissSelectTask, context);
    return;
  }

  if (subgrp->AllUnitsCompleted())
  {
    context->_fsm->SetState(SDismissSelectTask, context);
    subgrp->ClearPlan();
  }
}

static void DismissRelax(AISubgroupContext *context)
{
  STATE_PREFIX

  LogFSM(subgrp, "Dismiss", "RELAX");

  context->_fsm->SetTimeOut(20.0f + 30.0f * GRandGen.RandomValue());
  if (!subgrp->HasAI()) return;

  for (int i=0; i<subgrp->NUnits(); i++)
  {
    AIUnit *unit = subgrp->GetUnit(i);
    if (unit)
    {
      // Randomly look at someone in the team
      if (GRandGen.RandomValue() > 0.4)
      {
        int index = toIntFloor((subgrp->NUnits() - 1) * GRandGen.RandomValue());
        for (int j=0; j<subgrp->NUnits(); j++)
        {
          AIUnit *u = subgrp->GetUnit(j);
          if (u != unit) continue;
          if (index-- <= 0)
          {
            if (u)
            {
              Target *tgt = unit->FindTarget(u->GetVehicle());
              if (tgt) unit->SetWatchTarget(tgt);
            }
            break;
          }
        }
      }

      // Randomly sit down
      if (GRandGen.RandomValue() > 0.7)
      {
        unit->GetPerson()->PlayAction(RString("SitDown"));
      }
    }
  }
}

static void CheckDismissRelax(AISubgroupContext *context)
{
  STATE_PREFIX

  // check if some unit did not reach combat mode
  for (int i=0; i<subgrp->NUnits(); i++)
  {
    AIUnit *unit = subgrp->GetUnit(i);
    if (unit && unit->GetCombatMode() > CMAware)
    {
      // command not supported for units in vehicles
      context->_fsm->SetState(SDismissDone, context);
      return;
    }
  }

  if (context->_fsm->TimedOut())
  {
    context->_fsm->SetState(SDismissCleanUp, context);
  }
}

static void DismissCleanUp(AISubgroupContext *context)
{
  INIT_PREFIX

  LogFSM(subgrp, "Dismiss", "CLEAN UP");

  if (!subgrp->HasAI()) return;

  for (int i=0; i<subgrp->NUnits(); i++)
  {
    AIUnit *unit = subgrp->GetUnit(i);
    if (unit)
    {
      unit->SetNoWatch();
    }
  }
}

static void CheckDismissCleanUp(AISubgroupContext *context)
{
  STATE_PREFIX

  context->_fsm->SetState(SDismissSelectTask, context);
}

static AISubgroupFSM::pStateFunction dismissSpecial[] =
{
  DismissEnter, DismissExit
};

static AISubgroupFSM::StateInfo dismissStates[] =
{
  AISubgroupFSM::StateInfo("Init", DismissInit, CheckDismissInit),
  AISubgroupFSM::StateInfo("SelectTask", DismissSelectTask, CheckDismissSelectTask),
  AISubgroupFSM::StateInfo("Move", DismissMove, CheckDismissMove),
  AISubgroupFSM::StateInfo("Relax", DismissRelax, CheckDismissRelax),
  AISubgroupFSM::StateInfo("CleanUp", DismissCleanUp, CheckDismissCleanUp),
  AISubgroupFSM::StateInfo("Succeed", CommandSucceed, CheckCommandSucceed)
};

///////////////////////////////////////////////////////////////////////////////
// Commands Heal, Repair, Refuel, Rearm, TakeWeapon, TakeMagazine FSM

static Vector3 GetSupplyPoint(EntityAI *veh)
{
	const VehicleType *type = veh->GetType();
	return veh->RenderVisualState().PositionModelToWorld
	(
		type->GetSupplyPoint()
	);
}

static Vector3 GetActionPoint(EntityAI *veh, const Command *cmd)
{
  if (cmd->_action)
  {
    switch (cmd->_action->GetType())
    {
    case ATLadderOnUp:
    case ATLadderOnDown:
    case ATLadderUp:
    case ATLadderDown:
      {
        // check corresponding ladder point
        Building *house = dyn_cast<Building>(veh);
        if (!house) break;
        ActionLadderPos *action = static_cast<ActionLadderPos *>(cmd->_action.GetRef());
        Vector3 pos = house->GetLadderPos(action->GetLadder(), action->GetLadderPos() != 0);
        return pos;
      }
      break;
    }
  }
	return GetSupplyPoint(veh);
}

static Vector3 GetSupplyPointFar(EntityAI *veh)
{
	const VehicleType *type = veh->GetType();
	float distance = veh->GetRadius() * 1.4 + 3.5;
	Vector3 offset = type->GetSupplyPoint().Normalized() * distance;
	Vector3 res = veh->RenderVisualState().PositionModelToWorld(offset);
	res[1] = GLandscape->RoadSurfaceYAboveWater(res[0],res[2]);

	return res;
}


static Vector3 GetActionPointFar(EntityAI *veh, const Command *cmd)
{
	Vector3 res = GetActionPoint(veh,cmd);
	Vector3 pos = veh->RenderVisualState().Position();

	float distance = veh->GetRadius() * 1.4 + 3.5;
	Vector3 norm = res-pos;
	res = norm.Normalized() * distance + pos;

	res[1] = GLandscape->RoadSurfaceYAboveWater(res[0],res[2]);
	return res;
}


enum SupplyFSMStates
{
	SSupplyInit,
	SSupplyAlloc,
	SSupplyMove,
	SSupplyDirect,
	SSupplySupply,
	SSupplySucceed,
	SSupplyFail
};

static void ReportStatus(AIUnit *unit,Command::Message msg)
{
	AIGroup *grp = unit->GetGroup();
	if (!grp) return;
	switch (msg)
	{
		case Command::Heal:
			grp->SetHealthStateReported(unit,AIUnit::RSNormal);
			break;
    case Command::HealSoldier:
      grp->SetHealthStateReported(unit,AIUnit::RSNormal);
      break;
    case Command::RepairVehicle:
      grp->SetHealthStateReported(unit,AIUnit::RSLow);
      break;
		case Command::Rearm:
			grp->SetAmmoStateReported(unit,AIUnit::RSNormal);
			break;
		case Command::Repair:
			grp->SetDamageStateReported(unit,AIUnit::RSNormal);
			break;
		case Command::Refuel:
			grp->SetFuelStateReported(unit,AIUnit::RSNormal);
			break;
	}
}

static Command::Message ActionToCommand(UIActionType action)
{
	switch (action)
	{
		case ATHeal: return Command::Heal;
    case ATHealSoldier: return Command::HealSoldier;
    case ATFirstAid: return Command::FirstAid;
    case ATRepairVehicle: return Command::RepairVehicle;
		case ATRepair: return  Command::Repair;
		case ATRefuel: return  Command::Refuel;
		case ATRearm: return  Command::Rearm;
    case ATAssemble: return  Command::Assemble;
    case ATTakeBag: return  Command::TakeBag;
    case ATDisAssemble: return  Command::DisAssemble;
		default: return Command::Action;
	}
}

static bool CheckAbleToRearm(EntityAI *veh,EntityAI *transport)
{
	// adapted from Man::Supply, action == ATRearm
	Person *man = dyn_cast<Person>(veh);

	// check which magazines we need
  CheckAmmoInfo info;
	man->CheckAmmo(info);
	for (int i=0; i<transport->NMagazinesToTake();)
	{
		Ref<const Magazine> magazine = transport->GetMagazineToTake(i);
		if (!magazine || magazine->GetAmmo() == 0)
		{
			i++;
			continue;
		}
    /*
		if (transport->IsMagazineUsed(magazine))
		{
			i++;
			continue;
		}
    */
		// check if magazine can be used
		const MagazineType *type = magazine->_type;
		int slots = GetItemSlotsCount(type->_magazineType);
    int hgslots = GetHandGunItemSlotsCount(type->_magazineType);

		bool add = false;
    if (slots > 0)
    {
		  if (info.muzzle1 && info.muzzle1->CanUse(type))
		  {
			  if (slots <= info.slots1)
			  {
				  info.slots1 -= slots;
				  add = true;
			  }
		  }
		  else if (info.muzzle2 && info.muzzle2->CanUse(type))
		  {
			  if (slots <= info.slots2)
			  {
				  info.slots2 -= slots;
				  add = true;
			  }
		  }
		  else if (man->IsMagazineUsable(type))
		  {
			  if (slots <= info.slots3)
			  {
				  info.slots3 -= slots;
				  add = true;
			  }
		  }
    }
    else if (hgslots > 0)
    {
      if (info.muzzleHG && info.muzzleHG->CanUse(type))
      {
        if (hgslots <= info.slotsHG)
        {
          info.slotsHG -= hgslots;
          add = true;
        }
      }
    }
		if (add)
		{
			// we are able to take magazine
			return true;
		}
		else i++;
	}
	return false;
}

/*!
\patch 1.34 Date 10/22/2001 by Ondra.
- Fixed: When AI soldiers wanted to take riffle ammo
but there was no suitable avaiable, they looped kneeling and standing forever.
*/

static bool CheckSupplyDone(AISubgroupContext *context)
{
	AISubgroup *subgrp = context->_subgroup;
	Command *cmd = context->_task;

  AIUnit *leader = subgrp->Leader();
	EntityAI *veh = leader->GetVehicle();
	Assert(veh);


	#define UNALLOC() if( transport && transport->GetAllocSupply()==veh ) transport->SetAllocSupply(NULL);
	EntityAI *transport = cmd->_target;
	if (Glob.time > cmd->_time)
	{ // timeout
		context->_fsm->SetState(SSupplyFail, context);
		subgrp->ClearPlan();
		UNALLOC(); // unalloc supply
		return true;
	}

	// wait until we are fully filled or source is exhausted
//	if (!transport || transport->GetTotalDamage() >= 1.0)
	if (!transport)
	{
		// unalloc supply
		context->_fsm->SetState(SSupplyFail, context);
		//if( transport ) UNALLOC(); // unalloc supply
		return true;
	}

	Command::Message msg = cmd->_message;
	if (msg == Command::Action)
  {
    // hotfix - cmd->_action can be NULL in MP 
    if (!cmd->_action)
    {
      context->_fsm->SetState(SSupplyFail, context);
      return true;
    }
    msg = ActionToCommand(cmd->_action->GetType());
  }

	switch (msg)
	{
		case Command::Heal:
			if (veh->NeedsAmbulance() <= 0.01)
			{
				context->_fsm->SetState(SSupplySucceed, context);
				UNALLOC(); // unalloc supply
				ReportStatus(subgrp->Leader(), msg);
				return true;
			}
			if (!transport->GetType()->IsAttendant() || transport->IsDamageDestroyed())
			{
				context->_fsm->SetState(SSupplyFail, context);
				UNALLOC(); // unalloc supply
				ReportStatus(subgrp->Leader(), msg);
				return true;
			}
			return false;
    case Command::HealSoldier:
      if (cmd->_target)
      {
        if(cmd->_target->NeedsAmbulance() <= 0.01)
        {
          AIBrain *brain = cmd->_target->CommanderUnit();
          cmd->_target->SetAssignedAttendant(NULL);
          if(brain && brain->GetUnit())
          {
            ReportStatus(brain->GetUnit(), msg);
          }
          context->_fsm->SetState(SSupplySucceed, context);
          UNALLOC(); // unalloc supply

          return true;
        }
        else
        {
          if(cmd->_target && cmd->_target->CommanderUnit())
          {
            AIBrain *brain = cmd->_target->CommanderUnit();
            if(!brain->GetUnit() || !cmd->_target->AssignedAttendant())
            {
              context->_fsm->SetState(SSupplyFail, context);
              UNALLOC(); // unalloc supply
              return true;
            }
          }
        }
        if(veh->IsDeadSet())
        {
          context->_fsm->SetState(SSupplyFail, context);
          UNALLOC(); // unalloc supply
          return true;
      }
      }
      return false;
    case Command::FirstAid:
      if (cmd->_target)
      {
        EntityAI *entity = NULL;
        if(cmd->_target) entity = cmd->_target;
        if(entity)
        {
          AIBrain *brain = entity->CommanderUnit();
          if(brain && brain->GetLifeState() == LifeStateAlive)
          {
            entity->SetAssignedAttendant(NULL);
            ReportStatus(brain->GetUnit(), msg);

            UNALLOC(); // unalloc supply
            context->_fsm->SetState(SSupplySucceed, context);

            return true;
          }
          else
          {
            if(!entity->AssignedAttendant())
            {
              context->_fsm->SetState(SSupplyFail, context);
              UNALLOC(); // unalloc supply
              return true;
            }
          }
        }
        if(veh->IsDeadSet())
        {
          context->_fsm->SetState(SSupplyFail, context);
          UNALLOC(); // unalloc supply
          return true;
      }
      }
      return false;
    case Command::RepairVehicle:
      if (cmd->_target)
      {
        if(cmd->_target->GetMaxHitCont() <= 0.6f)
        {
          cmd->_target->SetAssignedAttendant(NULL);
          AIBrain *brain = cmd->_target->CommanderUnit();
          if(brain && brain->GetUnit())
          {
            ReportStatus(brain->GetUnit(), msg);
          }
          context->_fsm->SetState(SSupplySucceed, context);
          UNALLOC(); // unalloc supply
          return true;
        }
        else if(!cmd->_target->AssignedAttendant())
        {
          if(cmd->_target && cmd->_target->CommanderUnit())
          {
            AIBrain *brain = cmd->_target->CommanderUnit();
            if(!brain->GetUnit())
            {
              context->_fsm->SetState(SSupplyFail, context);
              UNALLOC(); // unalloc supply
              return true;
            }
          }
        }
        if(veh->IsDeadSet())
        {
          context->_fsm->SetState(SSupplyFail, context);
          UNALLOC(); // unalloc supply
          return true;
        }
      }
      return false;
		case Command::Repair:
			if (veh->NeedsRepair() <= 0.05)
			{
				context->_fsm->SetState(SSupplySucceed, context);
				UNALLOC(); // unalloc supply
				ReportStatus(subgrp->Leader(), msg);
				return true;
			}
			if (transport->GetRepairCargo() <= 10 || transport->IsDamageDestroyed())
			{
				context->_fsm->SetState(SSupplyFail, context);
				UNALLOC(); // unalloc supply
				ReportStatus(subgrp->Leader(), msg);
				return true;
			}
			return false;
		case Command::Refuel:
			if (veh->NeedsRefuel() <= 0.01)
			{
				context->_fsm->SetState(SSupplySucceed, context);
				UNALLOC(); // unalloc supply
				ReportStatus(subgrp->Leader(), msg);
				return true;
			}
			if (transport->GetFuelCargo() <= 10 || transport->IsDamageDestroyed())
			{
				context->_fsm->SetState(SSupplyFail, context);
				UNALLOC(); // unalloc supply
				ReportStatus(subgrp->Leader(), msg);
				return true;
			}
			return false;
		case Command::Rearm:
			if (subgrp->Leader()->IsFreeSoldier())
			{
				// check if we are able to take something from the supply
				// TODO: improve conditions
				if 
				(
					veh->NeedsInfantryRearm() <= 0.01 ||
					!CheckAbleToRearm(veh,transport)
				)
				{
					context->_fsm->SetState(SSupplySucceed, context);
					UNALLOC(); // unalloc supply
					ReportStatus(subgrp->Leader(), msg);
					return true;
				}
			}
			else
			{
				if (veh->NeedsRearm() <= 0.01)
				{
					context->_fsm->SetState(SSupplySucceed, context);
					UNALLOC(); // unalloc supply
					ReportStatus(subgrp->Leader(), msg);
					return true;
				}
				if
				(
					transport->GetAmmoCargo() <= 10 || transport->IsDamageDestroyed()
				)
				{
					context->_fsm->SetState(SSupplyFail, context);
					UNALLOC(); // unalloc supply
					ReportStatus(subgrp->Leader(), msg);
					return true;
				}
			}
			return false;
/*
		case Command::TakeWeapon:
			{
				Ref<WeaponType> weapon = WeaponTypes.New(cmd->_param3);
				for (int i=0; i<veh->NWeaponSystems(); i++)
				{
					if (veh->GetWeaponSystem(i) == weapon)
					{
						context->_fsm->SetState(SSupplySucceed, context);
						UNALLOC(); // unalloc supply
						return true;
					}
				}
			}
			return false;
		case Command::TakeMagazine:
			{
				int creator = cmd->_param;
				int id = cmd->_param2;
				for (int i=0; i<veh->NMagazines(); i++)
				{
					const Magazine *magazine = veh->GetMagazine(i);
					if (magazine->_creator == creator && magazine->_id == id)
					{
						context->_fsm->SetState(SSupplySucceed, context);
						UNALLOC(); // unalloc supply
						return true;
					}
				}
			}
			return false;
*/
    case Command::TakeBag:
    case Command::DisAssemble:
      if (!cmd->_target || !cmd->_target->IsInLandscape() || (cmd->_target->GetList()->Find(cmd->_target)<0))
      {
        context->_fsm->SetState(SSupplySucceed, context);
        UNALLOC(); // unalloc supply
        return true;
      }
      return false;
    case Command::Assemble:
			return false;
		case Command::Action:
			return false;
		default:
			Fail("Command");
			context->_fsm->SetState(SSupplyFail, context);
			UNALLOC(); // unalloc supply
			return true;
	}
}

static bool SupplyDoneImmediatelly(AISubgroupContext *context)
{
	Command *cmd = context->_task;
	Command::Message msg = cmd->_message;
	if (msg == Command::Action)
  {
    // hotfix - cmd->_action can be NULL in MP
    if (!cmd->_action) return false;
    msg = ActionToCommand(cmd->_action->GetType());
  }

	switch (msg)
	{
		case Command::Heal:
		case Command::HealSoldier:
    case Command::FirstAid:		
    case Command::RepairVehicle:
		case Command::Refuel:
		case Command::Repair:
			return false;
		case Command::Rearm:
			{
				AISubgroup *subgrp = context->_subgroup;
				Assert(subgrp);
				AIUnit *leader = subgrp->Leader();
				Assert(leader);
				return leader->IsFreeSoldier(); 
			}
    case Command::Action:
      if (cmd->_action->GetType() == ATGear) return false;
      if (cmd->_action->GetType() == ATOpenBag) return false;
      // continue
		default:
			return true;
	}
}

static void SupplyInit(AISubgroupContext *context)
{
	STATE_PREFIX
	LogFSM(subgrp, "Supply", "INIT");

	if (!subgrp->HasAI())
	{
		EntityAI *transport = cmd->_target;
		if (transport) cmd->_destination = GetSupplyPoint(transport);
		return;
	}

	subgrp->SetRefreshTime(TIME_MAX);
	subgrp->DoNotGo();
}

static void CheckSupplyInit(AISubgroupContext *context)
{
	STATE_PREFIX

	if (!subgrp->HasAI())
	{
		context->_fsm->SetState(SSupplySupply, context);
	}
	else
	{
		context->_fsm->SetState(SSupplyMove, context);
		subgrp->ClearPlan();
	}
}

static void SupplyAlloc(AISubgroupContext *context)
{
	STATE_PREFIX
	if (!subgrp->HasAI())
		return;

	LogFSM(subgrp, "Supply", "ALLOC");

	// stop and wait
	subgrp->Stop();
}

const float SupplyWaitDistance=70;

static void CheckSupplyAlloc(AISubgroupContext *context)
{
	STATE_PREFIX

	if (!subgrp->HasAI())
	{
		context->_fsm->SetState(SSupplySupply, context);
		return;
	}
	if (CheckSupplyDone(context)) return;

	EntityAI *vehicle = subgrp->Leader()->GetVehicle();

	EntityAI *transport = cmd->_target;
	Assert(transport);	// checked in CheckSupplyDone

	// if supply is allocated, we cannot go direct
	EntityAI *alloc=transport ? transport->GetAllocSupply() : NULL;
	if( !alloc || alloc==vehicle )
	{
		// start moving - supply is free
		context->_fsm->SetState(SSupplyMove, context);
		return;
	}

	// if supply is far (moved), we can get nearer
	if( transport->FutureVisualState().Position().Distance2(vehicle->FutureVisualState().Position())>Square(SupplyWaitDistance*1.5) )
	{
		context->_fsm->SetState(SSupplyMove, context);
		return;
	}
}

static bool IsDropAction(Command *cmd)
{
	return
		cmd->_message == Command::Action && cmd->_action &&
		(cmd->_action->GetType() == ATDropWeapon || cmd->_action->GetType() == ATDropMagazine);
}

static void SupplyMove(AISubgroupContext *context)
{
	STATE_PREFIX
	if (!subgrp->HasAI())
		return;

	LogFSM(subgrp, "Supply", "MOVE");

	// update command destination
	EntityAI *transport = cmd->_target;
	if (transport)
	{
		cmd->_destination = GetActionPointFar(transport,cmd);
	}

	subgrp->SetRefreshTime(TIME_MAX);
	subgrp->GoPlanned(cmd->_destination);
}

static void CheckSupplyMove(AISubgroupContext *context)
{
	STATE_PREFIX

	if (!subgrp->HasAI())
	{
		context->_fsm->SetState(SSupplySupply, context);
		return;
	}
	if (IsDropAction(context->_task))
	{
		context->_fsm->SetState(SSupplySupply, context);
		return;
	}

	if (CheckSupplyDone(context)) return;

	EntityAIFull *vehicle = subgrp->Leader()->GetVehicle();

  if (vehicle == cmd->_target)
  {
    // action on itself
    context->_fsm->SetState(SSupplySupply, context);
    return;
  }

	Vector3Val posL = vehicle->FutureVisualState().Position();
	Vector3Val posD = cmd->_destination;

	EntityAI *transport = cmd->_target;
	Assert(transport);	// checked in CheckSupplyDone

	if( transport && transport->FutureVisualState().Position().Distance2(vehicle->FutureVisualState().Position())<Square(SupplyWaitDistance) )
	{
		// if supply is allocated, we cannot go direct
		EntityAI *alloc=transport->GetAllocSupply();
		if( alloc && alloc!=vehicle )
		{
			context->_fsm->SetState(SSupplyAlloc, context);
			return;
		}
		// allocate - we are quite near
		transport->SetAllocSupply(vehicle);
		#if LOG_FSM
			LogF
			(
				"%s allocated %s",
				(const char *)vehicle->GetDebugName(),(const char *)transport->GetDebugName()
			);
		#endif
	}


	Point3 posDNew = GetActionPointFar(transport,cmd);
	float radius = transport->GetRadius() * 1.4 + 3.5;
  // FIX: AI vehicle cannot navigate more accurately than its precision
  saturateMax(radius, vehicle->GetPrecision());

	if ((posL - posDNew).SquareSizeXZ() <= Square(radius))
	{
		bool ok = subgrp->AllUnitsCompleted();
		if (!ok)
		{
			Vector3 posDExact = GetActionPoint(transport,cmd);
			if ((posL - posDExact).CosAngle(posDExact - transport->FutureVisualState().Position()) > cos(0.25 * H_PI))
			{
				//CollisionBuffer collision;
				float doneFrac;
				Vector3 normal;	
        ObjectCollisionFirstTouchContext  collContext(vehicle->FutureVisualState());
        collContext.with = vehicle;
        collContext.deltaPos =vehicle->InvTransform().Rotate(posDExact - posL);
        collContext.onlyVehicles = false;

				GLandscape->ObjectCollisionFirstTouch(doneFrac, normal, collContext);

				ok =((1 - doneFrac) * (1 - doneFrac) * collContext.deltaPos.SquareSize() < 1.0f);
				//GLandscape->ObjectCollision(collision, vehicle, NULL, posL, posDExact, 0, ObjIntersectGeom);
				//ok = collision.Size() == 0;
				#if _ENABLE_CHEATS
				/*if (!ok)
				{
					for (int i=0; i<collision.Size(); i++)
					{
						const CollisionInfo &info = collision[i];
						
						LogF
						(
							"%s at %s: collision with %s",
							(const char *)vehicle->GetDebugName(),
							(const char *)transport->GetDebugName(),
							(const char *)info.object->GetDebugName()
						);
					}
				}*/
				#endif
			}
		}
		if (ok)
		{
			context->_fsm->SetState(SSupplyDirect, context);
			subgrp->ClearPlan();
			return;
		}
	}

	float limit = 0.01 * (posL - posD).SquareSizeXZ();
	saturateMax(limit, Square(0.5 * radius));
	if ((posD - posDNew).SquareSizeXZ() > limit)
	{
		// update target position
		subgrp->GoPlanned(posDNew);
	}
}

static void SupplyDirect(AISubgroupContext *context)
{
	STATE_PREFIX
	if (!subgrp->HasAI())
		return;

	LogFSM(subgrp, "Supply", "DIRECT");

	EntityAI *transport = cmd->_target;
	if (transport)
	{
		// direct go
		subgrp->GoDirect(GetSupplyPoint(transport));
	}
}

static void CheckSupplyDirect(AISubgroupContext *context)
{
	STATE_PREFIX

	if (!subgrp->HasAI())
	{
		context->_fsm->SetState(SSupplySupply, context);
		return;
	}
	if (IsDropAction(context->_task))
	{
		context->_fsm->SetState(SSupplySupply, context);
		return;
	}
	if (CheckSupplyDone(context)) return;

	EntityAI *transport = cmd->_target;
	Assert(transport);	// checked in CheckSupplyDone
	// check if supply is allocated
	AIUnit *leader = subgrp->Leader();
	EntityAI *vehicle = leader->GetVehicle();

	Vector3Val posL = vehicle->FutureVisualState().Position();
	Vector3Val posD = cmd->_destination;
	Point3 posDNew = GetActionPoint(transport,cmd);
	float radius = transport->GetType()->GetSupplyRadius() + vehicle->CollisionSize() * 1.4f + 1;
  // FIX: AI vehicle cannot navigate more accurately than its precision
  saturateMax(radius, vehicle->GetPrecision());
	if ((posL - posDNew).SquareSizeXZ() <= Square(radius))
	{
		context->_fsm->SetState(SSupplySupply, context);
		subgrp->ClearPlan();
		leader->SetPlanningMode(AIBrain::DoNotPlan,false);
		return;
	}

	float limit = Square(0.5 * radius);
	if ((posD - posDNew).SquareSizeXZ() > limit)
	{
		// target moved
		context->_fsm->SetState(SSupplyMove, context);
		return;
	}
	#if 1
	// if we are not moving any longer and our supply is not moving as well
	// it is dead-lock
	// we have to recover somehow
	if 
	(
		transport->FutureVisualState().Speed().SquareSize()<1 &&
		leader->HasAI() && leader->GetPath().Size()<=0 
	)
	{
		if (posDNew.Distance2(vehicle->FutureVisualState().Position())<Square(50))
		{
			LogF("%s: supply dead-lock recovery - OK.",(const char *)vehicle->GetDebugName());
			context->_fsm->SetState(SSupplySupply, context);
			subgrp->ClearPlan();
			return;
		}
		else
		{
			Time maxTimeout = Glob.time+30;
			if (cmd->_time>maxTimeout)
			{
				LogF("%s: supply dead-lock recovery - Fail.",(const char *)vehicle->GetDebugName());
				cmd->_time = maxTimeout;
			}
		}
	}
	#endif

}

/*!
\patch 1.28 Date 10/22/2001 by Ondra.
- Fixed: AI soldiers with AI leader were able to rearm
without corresponding animation.
\patch 5115 Date 1/5/2007 by Jirka
- Fixed: DS crash after player disconnected and action command active for him
*/

static void ProcessSupply(EntityAI *transport, AIUnit *unit, Command *cmd)
{
	EntityAI *vehicle = unit->GetVehicle();

	Ref<Action> action;
	// translate message to action
	switch (cmd->_message)
	{
	default:
		Fail("Unknown action");
	case Command::Heal:
		action = new ActionBasic(ATHeal, cmd->_target);
		break;
  case Command::HealSoldier:
    action = new ActionBasic(ATHealSoldier, cmd->_target);
    break;
  case Command::FirstAid:
    action = new ActionBasic(ATFirstAid, cmd->_target);
    break;
  case Command::RepairVehicle:
    action = new ActionBasic(ATRepairVehicle, cmd->_target);
    break;
	case Command::Repair:
    action = new ActionBasic(ATRepair, cmd->_target);
		break;
	case Command::Refuel:
    action = new ActionBasic(ATRefuel, cmd->_target);
		break;
	case Command::Rearm:
    action = new ActionBasic(ATRearm, cmd->_target);
		break;
  case Command::TakeBag:
    action = new ActionBasic(ATTakeBag, cmd->_target);
    break;
  case Command::Assemble:
    action = new ActionBasic(ATAssemble, cmd->_target);
    break;
  case Command::DisAssemble:
    action = new ActionBasic(ATDisAssemble, cmd->_target);
    break;
	case Command::Action:
    // hotfix - cmd->_action can be NULL in MP
    if (!cmd->_action) return;
		action = cmd->_action->Copy();
    if (!action->GetTarget()) action->SetTarget(cmd->_target);
		break;
	}
  DoAssert(action);
	if (!vehicle->CheckActionProcessing(action->GetType(), unit))
	{
/*
		LogF
		(
			"%s ProcessSupply %s",
			(const char *)vehicle->GetDebugName(),transport ? (const char *)transport->GetDebugName() : "NULL"
		);
*/
		vehicle->StartActionProcessing(action, unit);
	}
}

static void SupplySupply(AISubgroupContext *context)
{
	STATE_PREFIX
	LogFSM(subgrp, "Supply", "SUPPLY");

	if (!subgrp->HasAI()) return;

	cmd->_time = Glob.time+60;
	subgrp->Stop();

	if (IsDropAction(context->_task))
	{
		ProcessSupply(NULL, subgrp->Leader(), cmd);
		context->_fsm->SetState(SSupplySucceed, context);
		return;
	}

	EntityAI *transport = cmd->_target;
	if (transport)
	{
    subgrp->Leader()->SetGearActionProcessed(false);
		ProcessSupply(transport, subgrp->Leader(), cmd);
		if (SupplyDoneImmediatelly(context))
		{
			// supply command succeed immediatelly
			EntityAI *veh = subgrp->Leader()->GetVehicle();

			#define UNALLOC() if( transport && transport->GetAllocSupply()==veh ) transport->SetAllocSupply(NULL);
			EntityAI *transport = cmd->_target;
			UNALLOC();

			context->_fsm->SetState(SSupplySucceed, context);
		}
	}
}

static void CheckSupplySupply(AISubgroupContext *context)
{
	STATE_PREFIX
	
  if (cmd->_message == Command::Action &&
    (cmd->_action && (cmd->_action->GetType() == ATGear || cmd->_action->GetType() == ATOpenBag)))
  {
    // special handling
    AIUnit *leader = subgrp->Leader();
    if (leader->IsGearActionProcessed())
    {
      leader->SetGearActionProcessed(false);
      context->_fsm->SetState(SSupplySucceed, context);
    }
    return;
  }

	if (CheckSupplyDone(context)) return;

	if (!subgrp->HasAI())
	{
		EntityAI *transport = cmd->_target;
		if (transport) cmd->_destination = GetSupplyPoint(transport);
		return;
	}

	EntityAI *vehicle = subgrp->Leader()->GetVehicle();
	EntityAI *transport = cmd->_target;
	Assert(transport);	// checked in CheckSupplyDone
	Vector3Val posL = vehicle->FutureVisualState().Position();
	Point3 posD = GetSupplyPoint(transport);
	float radius = transport->GetType()->GetSupplyRadius() + vehicle->CollisionSize() * 1.2 + 1;

	if ((posL - posD).SquareSizeXZ() > Square(radius))
	{
		context->_fsm->SetState(SSupplyMove, context);
		subgrp->ClearPlan();
		return;
	}
	if (transport->GetSupplying() != vehicle)
	{
		// refresh supply
		ProcessSupply(transport, subgrp->Leader(), cmd);
	}
}

static void SupplyEnter(AISubgroupContext *context)
{
	STATE_PREFIX
	//if (!subgrp->HasAI()) return;

	AIUnit *leader = subgrp->Leader();
	EntityAI *vehInto = cmd->_target;
	if (vehInto)
	{
		vehInto->WaitForSupply(leader);
	}
}

static void SupplyExit(AISubgroupContext *context)
{
	STATE_PREFIX
	//if (!subgrp->HasAI()) return;

	AIUnit *leader = subgrp->Leader();
	EntityAI *vehInto = cmd->_target;
  if (vehInto)
  {
    vehInto->SupplyFinished(leader);

    if (cmd->_message == Command::HealSoldier || cmd->_message == Command::FirstAid || cmd->_message == Command::RepairVehicle)
    {
      vehInto->SetAssignedAttendant(NULL);
    }
  }
}


static AISubgroupFSM::StateInfo supplyStates[] =
{
	AISubgroupFSM::StateInfo("Init", SupplyInit, CheckSupplyInit),
	AISubgroupFSM::StateInfo("Alloc", SupplyAlloc, CheckSupplyAlloc),
	AISubgroupFSM::StateInfo("Move", SupplyMove, CheckSupplyMove),
	AISubgroupFSM::StateInfo("Direct", SupplyDirect, CheckSupplyDirect),
	AISubgroupFSM::StateInfo("Supply", SupplySupply, CheckSupplySupply),
	AISubgroupFSM::StateInfo("Succeed", CommandSucceed, CheckCommandSucceed),
	AISubgroupFSM::StateInfo("Failed", CommandFailed, CheckCommandFailed)
};

static AISubgroupFSM::pStateFunction supplySpecial[] =
{
  SupplyEnter,
  SupplyExit
};

///////////////////////////////////////////////////////////////////////////////
// Command Join FSM

/*

enum
{
	SJoinInit,
	SJoinMove,
	SJoinDone,SJoinFail
};

static void JoinInit(AISubgroupContext *context)
{
	INIT_PREFIX
	if (!subgrp->HasAI())
		return;

	LogFSM(subgrp, "Join", "INIT");

	subgrp->SetRefreshTime(Glob.time + SUBGROUP_REFRESH);
	subgrp->DoNotGo();
}

static void CheckJoinInit(AISubgroupContext *context)
{
	STATE_PREFIX

	context->_fsm->SetState(SJoinMove, context);
	// subgrp may not be valid
	// subgrp->ClearPlan(); 
}

static void JoinMove(AISubgroupContext *context)
{
	STATE_PREFIX
	if (!subgrp->HasAI())
	{
		context->_fsm->SetTimeOut(20);
		return;
	}

	LogFSM(subgrp, "Join", "MOVE");

	// update destination
	AIGroup *grp = subgrp->GetGroup();
	Assert(grp);
	if (subgrp == grp->MainSubgroup())
	{
		context->_fsm->SetState(SJoinFail, context);
		subgrp->ClearPlan();
		return;
	}
	else
	{
		AISubgroup *subgroup = cmd->_joinToSubgroup;
		if (!subgroup)
			subgroup = grp->MainSubgroup();
		Assert(subgroup);
		Assert(subgrp != subgroup);
		if (subgroup->NUnits() == 0)
		{
			// do not join now - wait for update phase
			return;	// join now
		}

		if (!subgroup->Leader())
		{
			Fail("Leader");
			subgroup->SelectLeader();
			if (!subgroup->Leader())
			{
				Fail("Leader cannot be selected");
				return;
			}
		}

		AIUnit *tgtLeader = subgroup->Leader();

		AIUnit *leader = subgrp->Leader();

		cmd->_destination = leader->GetFormationAbsolute();
		
		subgrp->SetRefreshTime(Glob.time + SUBGROUP_REFRESH);
		subgrp->GoPlanned(cmd->_destination);
	}
}

static void CheckJoinMove(AISubgroupContext *context)
{
	STATE_PREFIX

	// TODO: BUG
	if (subgrp->HasAI() && subgrp->GetMode() != AISubgroup::PlanAndGo)
	{
		subgrp->GoPlanned(cmd->_destination);
	}

	AIGroup *grp = subgrp->GetGroup();
	Assert(grp);
	// update destination
	AISubgroup *subgroup = cmd->_joinToSubgroup;
	if (!subgroup)
		subgroup = grp->MainSubgroup();
	Assert(subgroup);
	Assert(subgrp != subgroup);
	if (subgroup->NUnits() == 0)
	{
		context->_fsm->SetState(SJoinDone, context);
		subgrp->ClearPlan();
		subgrp->JoinToSubgroup(subgroup);
		return;	// join now
	}

	AIUnit *leader = subgrp->Leader();
	AIUnit *tgtLeader = subgroup->Leader();
	Assert(leader);

	EntityAI *veh = leader->GetVehicle();

	bool away = veh->IsAway(tgtLeader,0.75);
	if (!away)
	{
		context->_fsm->SetState(SJoinDone, context);
		subgrp->ClearPlan();
		// note: subgrp may become MainSubgroup if MainSubgroup is destroyed
		if( grp->MainSubgroup() != subgrp )
		{
			subgrp->JoinToSubgroup(subgroup);
		}
		return;
	}

	if (subgrp->HasAI() )
	{
		if( subgrp->AllUnitsCompleted())
		{
			context->_fsm->Refresh(context);
			return;
		}
	}
	else
	{
		if( context->_fsm->TimedOut() )
		{
			context->_fsm->SetState(SJoinFail, context);
		}
	}
}

static AISubgroupFSM::StateInfo joinStates[] =
{
	AISubgroupFSM::StateInfo("Init", JoinInit, CheckJoinInit),
	AISubgroupFSM::StateInfo("Move", JoinMove, CheckJoinMove),
	AISubgroupFSM::StateInfo("Succeed", CommandSucceed, CheckCommandSucceed),
	AISubgroupFSM::StateInfo("Failed", CommandFailed, CheckCommandFailed)
};

*/

enum
{
	SJoinInit,
	SJoinDone,SJoinFail
};

static void JoinInit(AISubgroupContext *context)
{
	STATE_PREFIX

	AISubgroup *joinTo = cmd->_joinToSubgroup;
	if (!joinTo) joinTo = subgrp->GetGroup()->MainSubgroup();

	if (joinTo && joinTo != subgrp && subgrp != subgrp->GetGroup()->MainSubgroup())
	{
		subgrp->JoinToSubgroup(joinTo);
	}
}

static void CheckJoinInit(AISubgroupContext *context)
{
	context->_fsm->SetState(SJoinDone, context);
}

static AISubgroupFSM::StateInfo joinStates[] =
{
	AISubgroupFSM::StateInfo("Init", JoinInit, CheckJoinInit),
	AISubgroupFSM::StateInfo("Succeed", CommandSucceed, CheckCommandSucceed),
	AISubgroupFSM::StateInfo("Failed", CommandFailed, CheckCommandFailed)
};

///////////////////////////////////////////////////////////////////////////////
// Command GetIn FSM

static Vector3 GetGetInPoint(AIUnit *unit, Transport *veh)
{
	Assert(veh == unit->VehicleAssigned());
	Vector3 pos = veh->GetUnitGetInPos(unit)._pos;
	pos[1] = GLandscape->RoadSurfaceYAboveWater(pos[0], pos[2]);
	return pos;
}


static Vector3 GetGetInPointFar(AIUnit *unit, Transport *veh)
{
	Vector3 res;

	Assert(veh == unit->VehicleAssigned());

	if (veh->IsStopped())
	{
		res = veh->GetUnitGetInPos(unit)._pos;
    Vector3 pos = veh->RenderVisualState().Position();

    float distance = veh->GetRadius() * 1.4 + 3.5;
    Vector3 norm = res - pos;
    res = norm.Normalized() * distance + pos;
	}
	else
	{
		res = veh->GetStopPosition();
		res += Vector3(30,0,0);  // move to a close position (since the land around the vehicle will be locked)
	}

	// TODO: FindNearestEmpty may fail - use alternative GetOut point
	unit->FindNearestEmpty(res);

	res[1] = GLandscape->RoadSurfaceYAboveWater(res[0],res[2]);

	return res;
}

enum
{
	SGetInInit,
	SGetInMove,
	SGetInGetOut,
	SGetInWalk,
	SGetInDirect,
	SGetInGetIn,
	SGetInDone, SGetInFail
};

bool CheckGetInDone(AISubgroupContext *context)
{
	AISubgroup *subgrp = context->_subgroup;
	Assert(subgrp);
	Command *cmd = context->_task;
	Assert(cmd);

	AIUnit *leader = subgrp->Leader();
	Transport *vehInto = dyn_cast<Transport, Object>(cmd->_target);

	if (!vehInto || !vehInto->IsPossibleToGetIn())
	{
		context->_fsm->SetState(SGetInFail, context);
		subgrp->ClearPlan();
		return true;	// state changed
	}

	if (leader->GetVehicle() == vehInto)
	{
		context->_fsm->SetState(SGetInDone, context);
		subgrp->ClearPlan();
		return true;	// state changed
	}

	if (subgrp->HasAI() && leader->VehicleAssigned() != vehInto)
	{
		context->_fsm->SetState(SGetInFail, context);
		subgrp->ClearPlan();
		return true;	// state changed
	}

	return false;	// continue with other tests
}

static void GetInInit(AISubgroupContext *context)
{
	STATE_PREFIX
	LogFSM(subgrp, "GetIn", "INIT");

	AIUnit *leader = subgrp->Leader();
	Transport *vehInto = dyn_cast<Transport,Object>(cmd->_target);
	if (vehInto)
	{
		if (!subgrp->HasAI())
		{
			cmd->_destination = GetGetInPoint(leader, vehInto);
		}
	}

	if (!subgrp->HasAI())
		return;

	subgrp->SetRefreshTime(TIME_MAX);
	subgrp->DoNotGo();
}

static void CheckGetInInit(AISubgroupContext *context)
{
	STATE_PREFIX

	if (!subgrp->HasAI())
	{
		context->_fsm->SetState(SGetInGetIn, context);
	}
	else
	{
		context->_fsm->SetState(SGetInMove, context);
		subgrp->ClearPlan();
	}
}

static void GetInMove(AISubgroupContext *context)
{
	STATE_PREFIX
	if (!subgrp->HasAI())
		return;

	LogFSM(subgrp, "GetIn", "MOVE");

	// update command destination
	AIUnit *leader = subgrp->Leader();
	Transport *vehInto = dyn_cast<Transport, Object>(cmd->_target);
	if (vehInto)
	{
    if (leader->VehicleAssigned() != vehInto)
    {
      context->_fsm->SetState(SGetInFail, context);
      subgrp->ClearPlan();
      return;
    }
		cmd->_destination = GetGetInPointFar(leader, vehInto);
	}

	subgrp->SetRefreshTime(TIME_MAX);
	subgrp->GoPlanned(cmd->_destination,cmd->_precision);

	Vector3Val posL = leader->Position(leader->GetFutureVisualState());
	Vector3Val posD = cmd->_destination;
	float dist = 200;
	if ((posL - posD).SquareSizeXZ() > Square(dist))
	{
		for (int i=0; i<subgrp->NUnits(); i++)
		{
			AIUnit *unit = subgrp->GetUnit(i);
			if (unit)
				unit->OrderGetIn(true);
		}
	}
}

static void CheckGetInMove(AISubgroupContext *context)
{
	STATE_PREFIX

	if (!subgrp->HasAI())
	{
		context->_fsm->SetState(SGetInGetIn, context);
		return;
	}
	if (CheckGetInDone(context)) return;

	AIUnit *leader = subgrp->Leader();
	EntityAI *vehicle = leader->GetVehicle();
	Vector3Val posL = vehicle->FutureVisualState().Position();
	Vector3Val posD = cmd->_destination;

	Transport *vehInto = dyn_cast<Transport, Object>(cmd->_target);
	Assert(vehInto);	// checked in CheckGetInDone
	Point3 posDNew = GetGetInPointFar(leader, vehInto);
	float radius = 2.5 * vehInto->GetRadius() + 15.0;

	bool completed = subgrp->AllUnitsCompleted();
	if (completed) CheckDistance(subgrp->Leader(), cmd->_destination);
	
	if ((posL - posDNew).SquareSizeXZ() <= Square(radius) || completed)
	{
		context->_fsm->SetState(SGetInGetOut, context);
		return;
	}

	float limit = 0.01 * (posL - posD).SquareSizeXZ();
	saturateMax(limit, Square(0.5 * radius));
	if ((posD - posDNew).SquareSizeXZ() > limit)
	{
		// update target position
		subgrp->GoPlanned(posDNew);
	}
}

static void GetInGetOut(AISubgroupContext *context)
{
	STATE_PREFIX
	if (!subgrp->HasAI())
		return;

	LogFSM(subgrp, "GetIn", "GETOUT");

	AIUnit *unit = subgrp->Leader();
	Assert(unit);

	unit->OrderGetIn(true);
	if (!unit->IsFreeSoldier())
	{
    OLinkPermNOArray(AIUnit) list;
    list.Realloc(1);
    list.Resize(1);
    list[0] = unit;
		//subgrp->GetGroup()->SendGetOut(list);
		Command cmd;
		cmd._message = Command::GetOut;
		cmd._context = Command::CtxAutoSilent;
		cmd._id = subgrp->GetGroup()->GetNextCmdId();
		subgrp->GetGroup()->IssueCommand(cmd,list);
	}
}

static void CheckGetInGetOut(AISubgroupContext *context)
{
	STATE_PREFIX

	if (!subgrp->HasAI())
	{
		context->_fsm->SetState(SGetInGetIn, context);
		return;
	}
	if (CheckGetInDone(context)) return;

	AIUnit *leader = subgrp->Leader();
	Transport *vehInto = dyn_cast<Transport, Object>(cmd->_target);

	if (!vehInto || !vehInto->QCanIGetInAny(leader->GetPerson()) )
	{
		// fail - unable to get in
		context->_fsm->SetState(SGetInFail, context);
		subgrp->ClearPlan();
		return;
	}

	if (subgrp->Leader()->IsFreeSoldier())
	{
		context->_fsm->SetState(SGetInWalk, context);
		return;
	}

	if (!subgrp->GetGroup()->CommandSent(subgrp->Leader(), Command::GetOut))
	{
		// get out from current vehicle failed
		context->_fsm->SetState(SGetInFail, context);
		return;
	}
}

static void GetInWalk(AISubgroupContext *context)
{
	STATE_PREFIX
	LogFSM(subgrp, "GetIn", "GETIN");

	if (!subgrp->HasAI())
		return;

	// update command destination
	AIUnit *leader = subgrp->Leader();
	Transport *vehInto = dyn_cast<Transport, Object>(cmd->_target);
	if (vehInto)
	{
		cmd->_destination = GetGetInPointFar(leader, vehInto);
	}

	subgrp->SetRefreshTime(TIME_MAX);
	subgrp->GoPlanned(cmd->_destination);
}

static void CheckGetInWalk(AISubgroupContext *context)
{
	STATE_PREFIX

	if (!subgrp->HasAI())
	{
		context->_fsm->SetState(SGetInGetIn, context);
		return;
	}
	if (CheckGetInDone(context)) return;

	AIUnit *leader = subgrp->Leader();
	EntityAI *vehicle = leader->GetVehicle();
	Vector3Val posL = vehicle->FutureVisualState().Position();
	Vector3Val posD = cmd->_destination;

	Transport *vehInto = dyn_cast<Transport, Object>(cmd->_target);
	Assert(vehInto);	// checked in CheckGetInDone
	Point3 posDNew = GetGetInPointFar(leader, vehInto);
	float radius = vehInto->GetRadius() * 1.4 + 3.5;

  bool completed = subgrp->AllUnitsCompleted();

  if (completed)
	{
		CheckDistance(subgrp->Leader(), cmd->_destination);
		
		if (vehInto->IsStopped())
		{
			if (vehInto->QCanIGetInAny(leader->GetPerson()))
			{
				context->_fsm->SetState(SGetInDirect, context);
			}
			else
			{
				// fail - unable to get in
				context->_fsm->SetState(SGetInFail, context);
			}
			subgrp->ClearPlan();
			return;
		}
	}

	if ((posL - posDNew).SquareSizeXZ() <= Square(radius))
	{
		// check if vehicle is stopped
		if (vehInto->IsStopped())
		{
			//CollisionBuffer collision;
			Vector3 posDExact = GetGetInPoint(leader, vehInto);
			if ((posL - posDExact).CosAngle(posDExact - vehInto->FutureVisualState().Position()) > cos(0.25 * H_PI))
			{
				//GLandscape->ObjectCollision(collision, vehicle, NULL, posL, posDExact, 0, ObjIntersectGeom);
				float doneFrac;
				Vector3 normal;	
        ObjectCollisionFirstTouchContext collContext(vehicle->FutureVisualState());
        collContext.with = vehicle;
        collContext.deltaPos = vehicle->InvTransform().Rotate(posDExact - posL);
        collContext.onlyVehicles = false;

				GLandscape->ObjectCollisionFirstTouch(doneFrac, normal, collContext);

				if ((1 - doneFrac) * (1 - doneFrac) * collContext.deltaPos.SquareSize() < 1.0f)
				//if (false)
				{
					if (vehInto->QCanIGetInAny(leader->GetPerson()))
					{
						context->_fsm->SetState(SGetInDirect, context);
					}
					else
					{
						// fail - unable to get in
						context->_fsm->SetState(SGetInFail, context);
					}
					subgrp->ClearPlan();
					return;
				}
			}
		}
	}

	if (completed) CheckDistance(subgrp->Leader(), cmd->_destination);

	float limit = 0.01 * (posL - posD).SquareSizeXZ();
	saturateMax(limit, Square(0.5 * radius));
	if ((posD - posDNew).SquareSizeXZ() > limit || completed)
	{
		// update target position
		subgrp->GoPlanned(posDNew);
	}
}

static void GetInDirect(AISubgroupContext *context)
{
	STATE_PREFIX
	if (!subgrp->HasAI())
		return;

	LogFSM(subgrp, "GetIn", "DIRECT");

	AIUnit *leader = subgrp->Leader();
	Transport *vehInto = dyn_cast<Transport, Object>(cmd->_target);
	if (vehInto)
	{
		// direct go
    cmd->_destination = GetGetInPoint(leader, vehInto);
		subgrp->GoDirect(cmd->_destination);
	}
}

static void CheckGetInDirect(AISubgroupContext *context)
{
  STATE_PREFIX

	if (!subgrp->HasAI())
	{
		context->_fsm->SetState(SGetInGetIn, context);
		return;
	}
	if (CheckGetInDone(context)) return;

	AIUnit *leader = subgrp->Leader();
	EntityAI *vehicle = leader->GetVehicle();
	Transport *vehInto = dyn_cast<Transport, Object>(cmd->_target);
	Assert(vehInto);	// checked in CheckGetInDone
	Vector3Val posL = vehicle->FutureVisualState().Position();
	Point3 posD = GetGetInPoint(leader, vehInto);

	if( cmd->_destination.Distance2(GetGetInPoint(leader, vehInto))>=Square(3) )
	{
		// vehicle moved - go back to non-direct state
		context->_fsm->SetState(SGetInWalk, context);
		return;
	}

	// if soldier crashes into the vehicle, GetIn
  // change - soldiers now avoids collision, check some space before them
  const float checkDist = 0.1f;
  Ref<ObjectVisualState> transVehicle = vehicle->CloneFutureVisualState();
  transVehicle->SetPosition(transVehicle->Position() + checkDist * transVehicle->Direction());

	CollisionBuffer bump;
	vehicle->Intersect(bump, vehInto, *transVehicle, vehInto->FutureVisualState());

	//float radius = /*vehInto->GetType()->GetGetInRadius()*/2.0 + vehicle->CollisionSize() * 1.2 + 1;
	//float radius = vehicle->GetPrecision() * 0.55;
	float radius = vehicle->GetPrecision();
	//if ((posL - posD).SquareSizeXZ() <= Square(radius))

//	if( bump.Size()>0 || (posL - posD).SquareSizeXZ() <= Square(radius) )
// avoid changes of destination
	if( bump.Size()>0 || (posL - cmd->_destination).SquareSizeXZ() <= Square(radius) )
	{
		context->_fsm->SetState(SGetInGetIn, context);
		subgrp->ClearPlan();
		leader->SetPlanningMode(AIBrain::DoNotPlan,false);
		return;
	}

	if ((posL - cmd->_destination).SquareSizeXZ() >= Square(100))
	{
		// vehicle is now very far - go back to non-direct state
		context->_fsm->SetState(SGetInWalk, context);
		return;
	}
}

static void GetInGetIn(AISubgroupContext *context)
{
	STATE_PREFIX
	if (!subgrp->HasAI())
		return;

	LogFSM(subgrp, "GetIn", "GETIN");

	AIUnit *leader = subgrp->Leader();
	Assert(leader->IsFreeSoldier());
	Transport *vehInto = dyn_cast<Transport, Object>(cmd->_target);
	if (!vehInto)
	{
		context->_fsm->SetState(SGetInFail, context);
		return;
	}
	if (!leader->ProcessGetIn(*vehInto))
		context->_fsm->SetState(SGetInFail, context);
	// else subgroup dies out
}

static void CheckGetInGetIn(AISubgroupContext *context)
{
  STATE_PREFIX
	CheckGetInDone(context);
}

static void GetInEnter(AISubgroupContext *context)
{
	STATE_PREFIX
	//if (!subgrp->HasAI()) return;

	AIUnit *leader = subgrp->Leader();
	Transport *vehInto = dyn_cast<Transport,Object>(cmd->_target);
	if (vehInto) vehInto->WaitForGetIn(leader);
}

static void GetInExit(AISubgroupContext *context)
{
	STATE_PREFIX
	//if (!subgrp->HasAI()) return;

	AIUnit *leader = subgrp->Leader();
	Transport *vehInto = dyn_cast<Transport,Object>(cmd->_target);
	if (vehInto) vehInto->GetInFinished(leader);
}

static AISubgroupFSM::StateInfo getinStates[] =
{
	AISubgroupFSM::StateInfo("Init", GetInInit, CheckGetInInit),
	AISubgroupFSM::StateInfo("Move", GetInMove, CheckGetInMove),
	AISubgroupFSM::StateInfo("GetOut", GetInGetOut, CheckGetInGetOut),
	AISubgroupFSM::StateInfo("Walk", GetInWalk, CheckGetInWalk),
	AISubgroupFSM::StateInfo("Direct", GetInDirect, CheckGetInDirect),
	AISubgroupFSM::StateInfo("GetIn", GetInGetIn, CheckGetInGetIn),
	AISubgroupFSM::StateInfo("Succeed", CommandSucceed, CheckCommandSucceed),
	AISubgroupFSM::StateInfo("Failed", CommandFailed, CheckCommandFailed)
};

static AISubgroupFSM::pStateFunction getinSpecial[] =
{
	GetInEnter,
	GetInExit
};

///////////////////////////////////////////////////////////////////////////////
// Command GetOut FSM

enum
{
	SGetOutInit,
	SGetOutGetOut,
	SGetOutMove,
	SGetOutDone, SGetOutFail
};

static void GetOutInit(AISubgroupContext *context)
{
	INIT_PREFIX
	LogFSM(subgrp, "GetOut", "INIT");
	subgrp->SetRefreshTime(TIME_MAX);
}

static void CheckGetOutInit(AISubgroupContext *context)
{
	INIT_PREFIX
	context->_fsm->SetState(SGetOutGetOut, context);
}

static void GetOutGetOut(AISubgroupContext *context)
{
	STATE_PREFIX
	LogFSM(subgrp, "GetOut", "GETOUT");
}

static void CheckGetOutGetOut(AISubgroupContext *context)
{
	STATE_PREFIX

	AIUnit *leader = subgrp->Leader();

  Transport *veh = leader->GetVehicleIn();
  if (veh)
  {
    if (veh->WhoIsGettingOut().Find(leader) < 0)
      context->_fsm->SetState(SGetOutFail, context);
  }
  else
  {
		if (subgrp->HasAI())
			context->_fsm->SetState(SGetOutMove, context);
		else
			context->_fsm->SetState(SGetOutDone, context);
	}
}

static void GetOutMove(AISubgroupContext *context)
{
	STATE_PREFIX

	AIUnit *leader = subgrp->Leader();

	LogFSM(subgrp, "GetOut", "Move");

	if (!cmd->_target)
	{
		context->_fsm->SetState(SGetOutDone, context);
		return;
	}

	Vector3Val posL = subgrp->Leader()->Position(subgrp->Leader()->GetFutureVisualState());
	Vector3Val posV = cmd->_target->FutureVisualState().Position();
	// float distance = (cmd->_target->GetRadius() * 0.9f + 1.0f);
  float distance = (cmd->_target->GetRadius() * 1.4f + 1.5f);

	Vector3 norm = (posL - posV);
	cmd->_destination = posV + norm.Normalized() * distance;
	leader->FindNearestEmpty(cmd->_destination);
	subgrp->GoPlanned(cmd->_destination,cmd->_precision);
}

static void CheckGetOutMove(AISubgroupContext *context)
{
	STATE_PREFIX

  if (!subgrp->HasAI())
  {
    context->_fsm->SetState(SGetOutDone, context);
    return;
  }

  if (subgrp->AllUnitsCompleted())
  {
    CheckDistance(subgrp->Leader(), cmd->_destination);

    context->_fsm->SetState(SGetOutDone, context);
    subgrp->ClearPlan();
  }
/*
	AIUnit *leader = subgrp->Leader();

	EntityAI *lVehicle=leader->GetVehicle();

	if( !cmd->_target )
	{
		// target destroyed - 
		context->_fsm->SetState(SGetOutDone, context);
		return;
	}
	const float tgtRadius = cmd->_target->GetRadius();
	const float radius = lVehicle->GetPrecision()+tgtRadius*0.5;
	Vector3Val posL = lVehicle->Position();
	if ((posL - cmd->_destination).SquareSizeXZ() <= Square(radius))
	{
		context->_fsm->SetState(SGetOutDone, context);
	}
*/
}

static void GetOutEnter(AISubgroupContext *context)
{
	STATE_PREFIX
	//if (!subgrp->HasAI()) return;

	AIUnit *leader = subgrp->Leader();
	Assert( leader );
	if (leader->IsFreeSoldier()) return;

	Transport *veh = leader->GetVehicleIn();
	Assert(veh);
	cmd->_target = veh;
	if (veh->Driver())
	{
		if (veh->DriverBrain())
		{
      veh->WaitForGetOut(leader);
      if(!veh->IsLocal()) GetNetworkManager().AskWaitForGetOut(veh,leader);
			return;
		}
	}
	if (veh->GetGetOutTime()>Glob.time+2)
	{
		veh->SetGetOutTime(Glob.time+2);
	}
	veh->GetOutStarted(leader);
}

static void GetOutExit(AISubgroupContext *context)
{
	STATE_PREFIX
	//if (!subgrp->HasAI()) return;

	AIUnit *leader = subgrp->Leader();
	Transport *veh = dyn_cast<Transport,Object>(cmd->_target);
	if (veh) veh->GetOutFinished(leader);
}

static AISubgroupFSM::StateInfo getoutStates[] =
{
	AISubgroupFSM::StateInfo("Init", GetOutInit, CheckGetOutInit),
	AISubgroupFSM::StateInfo("GetOut", GetOutGetOut, CheckGetOutGetOut),
	AISubgroupFSM::StateInfo("Move", GetOutMove, CheckGetOutMove),
	AISubgroupFSM::StateInfo("Succeed", CommandSucceed, CheckCommandSucceed),
	AISubgroupFSM::StateInfo("Failed", CommandFailed, CheckCommandFailed)
};

static AISubgroupFSM::pStateFunction getoutSpecial[] =
{
	GetOutEnter,
	GetOutExit
};

///////////////////////////////////////////////////////////////////////////////
// Command Support FSM

enum
{
	SSupportInit,SSupportMove,SSupportDone,SSupportFail
};

static bool CheckSupportDone(AISubgroupContext *context)
{
	AISubgroup *subgrp = context->_subgroup;
	Command *cmd = context->_task;
	EntityAI *veh = subgrp->Leader()->GetVehicle();
	if (!veh || !veh->HasSupply())
	{
		// vehicle changed
		context->_fsm->SetState(SSupportFail, context);
		subgrp->ClearPlan();
		return true;
	}

	EntityAI *target = cmd->_target;
	if (!target)
	{
		// target destroyed
		context->_fsm->SetState(SSupportFail, context);
		subgrp->ClearPlan();
		return true;
	}

	if (Glob.time > cmd->_time)
	{
		// timeout
		context->_fsm->SetState(SSupportFail, context);
		subgrp->ClearPlan();
		return true;
	}

	// target allocated - continue
	if (veh->GetAllocSupply() == target) return false;

	if (veh->GetAllocSupply())
	{
		// allocated by other unit
		context->_fsm->SetState(SSupportFail, context);
		subgrp->ClearPlan();
		return true;
	}

	const OLinkPermNOArray(AIUnit) &supplyUnits = veh->GetSupplyUnits();
	for (int i=0; i<supplyUnits.Size(); i++)
	{
		if (!supplyUnits[i]) continue;
		if (supplyUnits[i]->GetVehicle() == target) return false; // continue
	}

	// done
	context->_fsm->SetState(SSupportDone, context);
	subgrp->ClearPlan();
	return true;
}

static void SupportInit(AISubgroupContext *context)
{
	INIT_PREFIX
	if (!subgrp->HasAI())
		return;

	LogFSM(subgrp, "Support", "INIT");

	subgrp->SetRefreshTime(TIME_MAX);
	subgrp->DoNotGo();
}

static void CheckSupportInit(AISubgroupContext *context)
{
	STATE_PREFIX
	
	context->_fsm->SetState(SSupportMove, context);
	subgrp->ClearPlan();
}

static void SupportMove(AISubgroupContext *context)
{
	STATE_PREFIX
	if (!subgrp->HasAI())
		return;

	LogFSM(subgrp, "Support", "MOVE");

	// update command destination
	EntityAI *veh = cmd->_target;
	if (veh) cmd->_destination = veh->FutureVisualState().Position();

	subgrp->SetRefreshTime(TIME_MAX);
	subgrp->GoPlanned(cmd->_destination,cmd->_precision);
}

/*!
\patch 1.28 Date 10/22/2001 by Ondra.
- Fixed: AI driving and coordination errors when using supply trucks fixed.
*/

static void CheckSupportMove(AISubgroupContext *context)
{
	STATE_PREFIX

	if (CheckSupportDone(context)) return;

	EntityAI *vehicle = subgrp->Leader()->GetVehicle();
	Vector3Val posL = vehicle->FutureVisualState().Position();
	Vector3Val posD = cmd->_destination;

	EntityAI *target = cmd->_target;
	Assert(target);	// checked in CheckSupportDone
	Vector3Val posDNew = target->FutureVisualState().Position();
	float distanceToTarget2 = posL.DistanceXZ2(posD);
	float limit = Square(0.1f) * distanceToTarget2;
	saturateMax(limit, Square(5));
	float precision = vehicle->GetPrecision();
	saturateMax(limit, Square(precision));
	if (posD.DistanceXZ2(posDNew)> limit)
	{
		if (distanceToTarget2>Square(precision*4))
		{
			// update target position
			subgrp->GoPlanned(posDNew);
		}
	}
	else if (distanceToTarget2<Square(precision*2))
	{
		// check if target is in good position for supplying
		subgrp->Stop();
	}
}

static AISubgroupFSM::StateInfo supportStates[] =
{
	AISubgroupFSM::StateInfo("Init", SupportInit, CheckSupportInit),
	AISubgroupFSM::StateInfo("Move", SupportMove, CheckSupportMove),
	AISubgroupFSM::StateInfo("Succeed", CommandSucceed, CheckCommandSucceed),
	AISubgroupFSM::StateInfo("Failed", CommandFailed, CheckCommandFailed)
};

///////////////////////////////////////////////////////////////////////////////
// Registration of FSMs

template <> FSM *AbstractAIMachine<Command, AISubgroupContext>::CreateFSM(const Command *cmd)
{
	int itemSize = sizeof(AISubgroupFSM::StateInfo);
	int funcSize = sizeof(AISubgroupFSM::pStateFunction);

	Assert( sizeof(waitStates) / itemSize == SWaitFail+1 );
	Assert( sizeof(attackStates) / itemSize == SAttackFail+1 );
	Assert( sizeof(hideStates) / itemSize == SHideFail+1 );
	Assert( sizeof(moveStates) / itemSize == SMoveFail+1 );
	Assert( sizeof(getoutStates) / itemSize == SGetOutFail+1 );
	Assert( sizeof(joinStates) / itemSize == SJoinFail+1 );
	Assert( sizeof(getinStates) / itemSize == SGetInFail+1 );
	Assert( sizeof(supplyStates) / itemSize == SSupplyFail+1 );
	Assert( sizeof(supportStates) / itemSize == SSupportFail+1 );
	Assert( sizeof(fireStates) / itemSize == SFireFail+1 );
  Assert( sizeof(fireAtPositionStates) / itemSize == SFireAtPositionFail+1 );
	Assert( sizeof(stopStates) / itemSize == SStopFail+1 );
  Assert( sizeof(expectStates) / itemSize == SExpectFail+1 );
  Assert( sizeof(dismissStates) / itemSize == SDismissDone+1 );

  if (!cmd)
  {
    return new AISubgroupFSM
    (
      noCommandStates,
      sizeof(noCommandStates) / itemSize
    );
  }

	switch (cmd->_message)
	{
		case Command::NoCommand:
			return new AISubgroupFSM
			(
				noCommandStates,
				sizeof(noCommandStates) / itemSize
			);
		case Command::Wait:
			return new AISubgroupFSM
			(
				waitStates,
				sizeof(waitStates) / itemSize
			);
		case Command::Attack:
			return new AISubgroupFSM
			(
				attackStates,
				sizeof(attackStates) / itemSize,
				attackSpecial,
				sizeof(attackSpecial) / funcSize
			);
		case Command::AttackAndFire:
			return new AISubgroupFSM
			(
				attackStates,
				sizeof(attackStates) / itemSize,
				attackFireSpecial,
				sizeof(attackFireSpecial) / funcSize
			);
		case Command::Hide:
			return new AISubgroupFSM
			(
				hideStates,
				sizeof(hideStates) / itemSize
			);
		case Command::Move:
			return new AISubgroupFSM
			(
				moveStates,
				sizeof(moveStates) / itemSize
			);
		case Command::Heal:
    case Command::HealSoldier:
    case Command::RepairVehicle:
		case Command::Repair:
		case Command::Refuel:
		case Command::Rearm:
		case Command::Action:
    case Command::TakeBag:
    case Command::Assemble:
    case Command::DisAssemble:
			return new AISubgroupFSM
			(
				supplyStates,
				sizeof(supplyStates) / itemSize,
				supplySpecial,
				sizeof(supplySpecial) / funcSize
			);
    case Command::FirstAid:
      return new AISubgroupFSM
        (
        supplyStates,
        sizeof(supplyStates) / itemSize,
        supplySpecial,
        sizeof(supplySpecial) / funcSize
        );
		case Command::Support:
			return new AISubgroupFSM
			(
				supportStates,
				sizeof(supportStates) / itemSize
			);
		case Command::Join:
			return new AISubgroupFSM
			(
				joinStates,
				sizeof(joinStates) / itemSize
			);
		case Command::GetIn:
			return new AISubgroupFSM
			(
				getinStates,
				sizeof(getinStates) / itemSize,
				getinSpecial,
				sizeof(getinSpecial) / funcSize
			);
		case Command::GetOut:
			return new AISubgroupFSM
			(
				getoutStates,
				sizeof(getoutStates) / itemSize,
				getoutSpecial,
				sizeof(getoutSpecial) / funcSize
			);
		case Command::Fire:
			return new AISubgroupFSM
			(
				fireStates,
				sizeof(fireStates) / itemSize
			);
    case Command::FireAtPosition:
      return new AISubgroupFSM
        (
        fireAtPositionStates,
        sizeof(fireAtPositionStates) / itemSize,
        fireAtPositionSpecial,
        sizeof(fireAtPositionSpecial) / funcSize
        );
		case Command::Stop:
			return new AISubgroupFSM
			(
				stopStates,
				sizeof(stopStates) / itemSize
			);
		case Command::Expect:
			return new AISubgroupFSM
			(
				expectStates,
				sizeof(expectStates) / itemSize
			);
    case Command::Scripted:
      {
        RString FindScript(RString name);
        RString fullname = FindScript(cmd->_strParam);
        if (fullname.GetLength() > 0)
        {
          return new AISubgroupFSMScripted(fullname, GWorld->GetMissionNamespace()); // mission namespace
        }
        RptF("FSM %s not found", (const char *)cmd->_strParam);
        return NULL;
      }
    case Command::Dismiss:
      return new AISubgroupFSM(
        dismissStates, sizeof(dismissStates) / itemSize,
        dismissSpecial, sizeof(dismissSpecial) / funcSize);
	}
	Fail("Unknown command type");
	// to stay safe always return some FSM
  return new AISubgroupFSM(
    noCommandStates,
    sizeof(noCommandStates) / itemSize
  );
}

void FailCommand(AISubgroupContext *context)
{
	Assert(context->_subgroup);
	Command *cmd = context->_task;
	Assert(cmd);
	FSM *fsm = context->_fsm;
	Assert(fsm);

	if (fsm)
	{
		switch (cmd->_message)
		{
		case Command::NoCommand:
			break;
		case Command::Wait:
			fsm->SetState(SWaitFail, context);
			break;
		case Command::Attack:
		case Command::AttackAndFire:
			fsm->SetState(SAttackFail, context);
			break;
		case Command::Hide:
			fsm->SetState(SHideFail, context);
			break;
		case Command::Move:
			fsm->SetState(SMoveFail, context);
			break;
		case Command::Heal:
    case Command::HealSoldier:
    case Command::RepairVehicle:
		case Command::Repair:
		case Command::Refuel:
		case Command::Rearm:
/*
		case Command::TakeWeapon:
		case Command::TakeMagazine:
*/
		case Command::Action:
			fsm->SetState(SSupplyFail, context);
			break;
		case Command::Support:
			fsm->SetState(SSupportFail, context);
			break;
		case Command::Join:
			fsm->SetState(SJoinFail, context);
			break;
		case Command::GetIn:
			fsm->SetState(SGetInFail, context);
			break;
		case Command::Fire:
			fsm->SetState(SFireFail, context);
			break;
    case Command::FireAtPosition:
      fsm->SetState(SFireAtPositionFail, context);
      break;
		case Command::GetOut:
			fsm->SetState(SGetOutFail, context);
			break;
		case Command::Stop:
			fsm->SetState(SStopFail, context);
			break;
		case Command::Expect:
			fsm->SetState(SExpectFail, context);
			break;
/*
		case Command::Action:
			fsm->SetState(SActionFailed, context);
			break;
*/
    case Command::Dismiss:
      fsm->SetState(SDismissDone, context);
      break;
		}
	}
}
