ASEMasterSDK - functions for reporting a server to the All-Seeing Eye master
server

Only two functions, ASEMaster_init, should be called when the server is
started, and ASEMaster_heartbeat, which should be called periodically when
the server is running, see ASEMasterSDK.c for details and documentation.
