#ifdef _MSC_VER
#pragma once
#endif

#ifndef __OBJECTHIERARCHY_HPP__
#define __OBJECTHIERARCHY_HPP__

#include "visual.hpp"
#include "Shape/plane.hpp"
#include "cast.hpp"
#include <Es/Containers/sortedArray.hpp>

//------------------------------------
// SimulationImportance
//------------------------------------
//! importance determines simulation precision
#ifndef DECL_ENUM_SIMULATION_IMPORTANCE
#define DECL_ENUM_SIMULATION_IMPORTANCE
DECL_ENUM(SimulationImportance)
#endif
DEFINE_ENUM_BEG(SimulationImportance)
SimulateVisibleNear,SimulateVisibleFar, //! it is visible
SimulateInvisibleNear,SimulateInvisibleFar, //! it is inivisible
SimulateDefault //! no specific value
DEFINE_ENUM_END(SimulationImportance)


//------------------------------------
// CollisionContexes
//------------------------------------
class Object;
class IntersectionFilter
{
public:
  /// check if object collision should be tested
  virtual bool operator()(const Object *) const = 0; 
};

/// collision intersection test style
enum ObjIntersect
{
  /// normal fire
  ObjIntersectFire,
  /// view geometry
  ObjIntersectView,
  /// collision geometry 
  ObjIntersectGeom,
  /// indirect fire dammage, uses same geometry as ObjIntersectFire, but different calculation
  ObjIntersectIFire,
  /// no test
  ObjIntersectNone
};

/// context for Landscape::CheckObjectCollision
struct CheckObjectCollisionContext;

class ObjectVisualState;

/// context for Landscape::ObjectCollision objectVsObjects
struct ObjectCollisionContext
{
  const Object * with;
  const ObjectVisualState &withPos; 
  bool person;
  bool onlyVehicles;

  ObjectCollisionContext(const ObjectVisualState &vs) : person(false), onlyVehicles(false), withPos(vs) {};
};

/// context for Landscape::ObjectCollisionFirstTouch
struct ObjectCollisionFirstTouchContext
{
  const Object *with;
  const ObjectVisualState & withPos;
  Vector3 deltaPos; // in with local pos !!! 
  bool onlyVehicles;

  ObjectCollisionFirstTouchContext(const ObjectVisualState &vs) : withPos(vs) {};
};

/// context for Landscape::GroundCollisionWithObject 
struct GroundCollisionWithObjectContext
{
  const Object *with;
  Vector3 centerPos;
  Buffer<Vector3> begEnd;
  float wRad;
  const Shape *withShape;
  Array<int> *solidWaterMap;
  const ObjectVisualState & withTrans; 
  float above; 
  int level; 

  GroundCollisionWithObjectContext(const ObjectVisualState &vs) : withTrans(vs) {};
};

#endif





