#ifdef _MSC_VER
#pragma once
#endif

#ifndef _HELICOPTER_HPP
#define _HELICOPTER_HPP

#include "transport.hpp"
#include "planeOrHeli.hpp"
#include "shots.hpp"

#include "wounds.hpp"
#include "global.hpp"


class HelicopterType;

class HelicopterVisualState : public TransportVisualState
{
  typedef TransportVisualState base;
  friend class Helicopter;
  friend class HelicopterAuto;

protected:
  float _cyclicForward;
  float _cyclicAside;  /// main rotor thrust direction
  float _rotorPosition, _rotorPositionDelta;  // delta used for interpolation
  float _backRotorPosition;
  float _rotorDive;
  float _gearsUp;  /// actual gear position 0 means gears down, 1 gears up, negative gear damaged
  float _rotorSpeed, _mainRotor;  /// for RPM

public:
  HelicopterVisualState(HelicopterType const& type);
  virtual void Interpolate(Object *obj, ObjectVisualState const& t1state, float t, ObjectVisualState& interpolatedResult) const;
  virtual void Copy(ObjectVisualState const& src) {*this=static_cast_checked<const HelicopterVisualState &>(src);}
  virtual HelicopterVisualState *Clone() const {return new HelicopterVisualState(*this);}

  float GetGear(const HelicopterType* type) const;
  float GetRotorH(const HelicopterType* type) const;
  float GetRotorV(const HelicopterType* type) const;
  float GetRotorHDive(const HelicopterType* type) const;
  float GetRotorVDive(const HelicopterType* type) const;
  float GetCyclicFwd() const {return _cyclicForward;}
  float GetCyclicAside() const {return _cyclicAside;}
  float GetRPM() const { return 10 * _rotorSpeed * (1 - _mainRotor * 0.1); }
};


class HelicopterType: public TransportType
{
  typedef TransportType base;
  friend class Helicopter;
  friend class HelicopterVisualState;
  friend class HelicopterAuto;

  protected:
  Vector3 _pilotPos; // neutral neck and head positions

  Vector3 _missileLPos,_missileRPos;
  Vector3 _rocketLPos,_rocketRPos;
  AutoArray<Vector3> _cmPos,_cmDir;
  
  // machine guns memory points
  AutoArray<Vector3> _gunPosArray;  
  AutoArray<Vector3> _gunDirArray;  

  AnimationSection _hRotorStill,_hRotorMove;
  AnimationSection _vRotorStill,_vRotorMove;

  bool _enableSweep;
	bool _gearRetracting;
  float _gearDownSpeed;
  float _gearUpSpeed;

  float _altFullForce;
  float _altNoForce;

  int _hullHit;
  int _engineHit;
  int _rotorHHit,_rotorVHit;
  int _avionicsHit;
  int _missiles;
  int _glassRHit;
  int _glassLHit;

  WoundTextureSelections _glassDamageHalf;
  WoundTextureSelections _glassDamageFull;
 
  float _mainRotorSpeed;
  float _backRotorSpeed;

  // note: dive is used for Chinook.
  // Back rotor is actually second main rotor in this case
  float _minMainRotorDive;
  float _minBackRotorDive;
  float _maxMainRotorDive;
  float _maxBackRotorDive;
  float _neutralMainRotorDive;
  float _neutralBackRotorDive;

  /// lift based on speed
  AutoArray<float> _envelope;

  /// class names describing effect sources
  RString _dustEffect, _waterEffect;
  /// class name describing effect sources
  RString _damageEffect;

  /// Try to get gunPos and dir from memoryPoints
  /** and store them into _gunPosArray and _gunDirArray
      at [index] place
  */
  bool ComputePosAndDirFromBegEnd(const RString &gunBeg,
                                  const RString &gunEnd, 
                                  int index);
      
  /// Try to load Memory points and build all turrets' positions and dirs
  void InitTurretsPosAndDir();
  /// Create at least one record in pos and dir arrays
  /**
    pos will be initialized to VZero, dir to VForward
  */  
  void InitDummyPosAndDir();
  
  public:
  HelicopterType( ParamEntryPar param );
  ~HelicopterType();
  
  virtual void Load(ParamEntryPar par, const char *shape);
  AnimationSource *CreateAnimationSource(const AnimationType *type, RStringB source);
      
  void InitShape();
  void DeinitShape();
  void InitShapeLevel(int level);
  void DeinitShapeLevel(int level);
  bool AbstractOnly() const {return false;}
  /// Get pos from _gunPosArray
  Vector3Val GetGunPos(int i) const;
  /// Get pos from _gunDirArray
  Vector3Val GetGunDir(int i) const;

  virtual Object* CreateObject(bool unused) const;
  virtual EntityVisualState* CreateVisualState() const { return new HelicopterVisualState(*this); }

  float GetFieldCost( const GeographyInfo &info, CombatMode mode ) const;
  float GetBaseCost(const GeographyInfo &info, bool operative, bool includeGradient) const;
  float GetGradientPenalty(float gradient) const;
  float GetCostTurn(int difDir, int &debet, bool forceFullTurn) const;
  bool CanBeAirborne() const {return true;}
  virtual bool StrategicPlanningOnly() const {return true;}
  virtual bool CanUseRoads() const {return false;}
  virtual float GetStopDistance() const;

  Vector3 RotorUpForce( Vector3Val speed, float coef, float alt, bool diag ) const;
};


class Helicopter: public PlaneOrHeli
{
  typedef PlaneOrHeli base;

protected:
  bool _missileLRToggle;
  bool _rocketLRToggle;
  int _cmIndexToggle;
  int _gunPosIndexToogle;
    
	/// true means pilot wants gear down
	bool _pilotGear;
  /// randomize frequency to avoid interference between choppers
  float _rndFrequency;
  float _rotorSpeedWanted; // turning motor on/off

  // pilot can directly coordinate these parameters
  float _backRotor,_backRotorWanted;
  float _mainRotorWanted; // main rotor thrust
  float _cyclicForwardWanted;
  float _cyclicAsideWanted; // main rotor thrust direction

  float _rotorDiveWanted;

  Vector3 _lastAngVelocity; // helper for prediction
  Vector3 _turbulence;
  Time _lastTurbulenceTime;

  /// dust effects
  EffectsSourceRT _dustEffect, _waterEffect;
  /// damage effect
  EffectsSourceRT _damageEffect;

  float GetGearPos() const;
  
public:
  Helicopter( const EntityAIType *name, Person *pilot, bool fullCreate = true);
  ~Helicopter();
  
  const HelicopterType *Type() const
  {
    return static_cast<const HelicopterType *>(GetType());
  }

  /// @{ Visual state
public:
  friend class HelicopterVisualState;
  HelicopterVisualState typedef VisualState;

  VisualState& FutureVisualState() { return static_cast<VisualState &>(base::FutureVisualState()); }
  VisualState const& FutureVisualState() const { return static_cast_checked<VisualState const &>(base::FutureVisualState());}
  VisualState const& CurrentVisualState() const { return static_cast_checked<VisualState const &>(base::CurrentVisualState());}
  const VisualState& RenderVisualState() const{ return static_cast<const VisualState &>(base::RenderVisualState()); }

  ProtectedVisualState<const VisualState> RenderVisualStateScope(bool ignoreParent=false) const {return ProtectedVisualState<const VisualState>(base::RenderVisualStateScope(ignoreParent));}
  ProtectedVisualState<VisualState> FutureVisualStateScope(bool ignoreParent=false) { return ProtectedVisualState<VisualState>(base::FutureVisualStateScope(ignoreParent)); }
  ProtectedVisualState<const VisualState> FutureVisualStateScope(bool ignoreParent=false) const { return ProtectedVisualState<const VisualState>(base::FutureVisualStateScope(ignoreParent)); }
  /// @}

  /// @{ Sound controllers (\sa Transport::Sound())
  virtual float GetRPM() const { return FutureVisualState().GetRPM(); }
  virtual float GetRenderVisualStateRPM() const { return RenderVisualState().GetRPM(); }
  virtual float GetRotorSpeed() const { return FutureVisualState()._rotorSpeed; }
  virtual float GetMainRotorThrust() const { return FutureVisualState()._mainRotor; }
  /// @}
  
  /// @{ Visual controllers (\sa Type()->CreateAnimationSource())
  virtual float GetCtrlRPM(ObjectVisualState const& vs) const { return static_cast_checked<VisualState const&>(vs).GetRPM(); }
  float GetCtrlGear(ObjectVisualState const& vs) const { return static_cast_checked<VisualState const&>(vs).GetGear(Type()); }  /// gear animation

  float GetCtrlRotorH(ObjectVisualState const& vs) const { return static_cast_checked<VisualState const&>(vs).GetRotorH(Type()); }
  float GetCtrlRotorV(ObjectVisualState const& vs) const { return static_cast_checked<VisualState const&>(vs).GetRotorV(Type()); }
  float GetCtrlRotorHDive(ObjectVisualState const& vs) const { return static_cast_checked<VisualState const&>(vs).GetRotorHDive(Type()); }
  float GetCtrlRotorVDive(ObjectVisualState const& vs) const { return static_cast_checked<VisualState const&>(vs).GetRotorVDive(Type()); }
  float GetCtrlCyclicFwd(ObjectVisualState const& vs) const { return static_cast_checked<VisualState const&>(vs).GetCyclicFwd(); }
  float GetCtrlCyclicAside(ObjectVisualState const& vs) const { return static_cast_checked<VisualState const&>(vs).GetCyclicAside(); }
  /// @}

  Vector3 GetFrontOrientation() const { return Vector3(0, -1, 0); }

  protected:
  float GetGlassBroken() const;

  void DamageAnimation(AnimationContext &animContext, int level, float dist2);
  void DamageDeanimation( int level );

  public:
  AnimationStyle IsAnimated( int level ) const; // appearance changed with Animate
  bool IsAnimatedShadow( int level ) const; // shadow changed with Animate
  void Animate(AnimationContext &animContext, int level, bool setEngineStuff, Object *parentObject, float dist2);
  void Deanimate( int level, bool setEngineStuff );

  const char **HUDMode(int &mask) const;

  virtual float GetFleeDamage() const;
  virtual bool IsAbleToMove() const;
  virtual bool IsPossibleToGetIn() const;
  virtual bool IsWorking() const;
  virtual bool EjectNeeded() const;

  /// how much flight time is estimated based on fuel tank damage and current fuel level
  float FuelTimeLeft() const;

  /// how fast is the fuel leaking (l/s)
  float FuelLeakRate() const;

  void StopRotor();
  
  /// amount of back rotor malfunctioning - 0 = OK, 1 = out of service
  float GetBackRotorMalfunction() const;
  void Simulate( float deltaT, SimulationImportance prec );
  void Sound( bool inside, float deltaT );
  void UnloadSound();

  float GetEngineVol( float &freq ) const;
  float GetEnvironVol( float &freq ) const;

  const CursorTextureInfo *GetCursorTexture(Person *person);
  const CursorTextureInfo *GetCursorAimTexture(Person *person);

  float Rigid() const {return 0.3;} // how much energy is transfered in collision

  bool IsVirtual( CameraType camType ) const {return true;}
  //bool IsContinuous( CameraType camType ) const {return true;}
  bool HasFlares( CameraType camType ) const;

  float GetHitForDisplay(int kind) const;

  virtual Vector3 CameraPointRel(CameraType camType) const;
  virtual Vector3 CameraPosition() const;

  virtual bool EmittingWind(Vector3Par pos, float radius) const;
  virtual Vector3 WindEmit(Vector3Par pos, Vector3Par wind) const;

  float TrackingSpeed() const {return 100;}

  float GetCombatHeight() const {return 30;}
  float GetMinCombatHeight() const {return 30;}
  float GetMaxCombatHeight() const {return 100;}

  bool Airborne() const;
  virtual Vector3 GetLockPreferDir() const;
  virtual bool AllowLockDir(Vector3Par dir) const;

  bool HasHUD() const {return true;}
  const char *GetControllerScheme() const {return "Aircraft";}
  void GetAimActions(ValueWithCurve &aimX, ValueWithCurve &aimY) const;

  UserAction GetManualFireAction() const;

  LSError Serialize(ParamArchive &ar);

  USE_CASTING(base)
};

class HelicopterAuto: public Helicopter
{
  typedef Helicopter base;

  protected:
  Vector3 _pilotSpeed;
  float _pilotHeading; // heading set by pilot
  float _pilotDive; // dive set by pilot

  float _forceDive; // dive necessary for aiming
  float _forceBank;
  //float _apDive,_apBank;
  float _pilotHeight;
  float _defPilotHeight; // set by scripting
  float _dirCompensate;  // how much we compensate for estimated change
  bool _avoidBankJitter;  // this should be off when autopilot is flying precisely
  // esp. when landing

  //@{ mid-air collision avoidance - see AvoidCollision
	Time _pilotAvoidHigh;
	float _pilotAvoidHighHeight;
	Time _pilotAvoidLow;
	float _pilotAvoidLowHeight;
	//@}

  // helpers for keyboard pilot
  /// direct cyclic control
  bool _pilotCyclicDirect;
  /// keyboard helper activated
  bool _pilotHeightHelper;
  /// vertical speed autopilot
  bool _pilotVSpeedHelper;
  bool _pilotSpeedHelper; //!< speed autopilot activated
  bool _pilotDirHelper; //!< direction autopilot activated
  /// pilot height may be given as Above Ground or Above Sea.
  bool _pilotHeightAGL;

  bool _hoveringAutopilot;

  Time _analogueThrottleTime; //!< last time when user touched speed autopilot controls
  Time _thrustHelperTime; //!< last time when user touched throttle / brake controls
  
  float _lastForward; // last speed control value, used to track _thrustHelperTime
  float _lastThrotleDirect; //!< last throttle value, used to track _analogueThrottleTime

  //@{ recognize fast speed-up, fast brake
  bool _pressedForward,_pressedBack;
  bool _pressedUp,_pressedDown;
  //@}
  
#if _VBS3_AVRS
  // vector pointing to helicopter.
  // when completed a waypoint
  Vector3 _heliVector;
  // used to tell if the waypoints changed
  Vector3 _wPPos;
#endif

  
  /// keep pilot heading
  bool _pilotHeadingSet; 
  /// keep pilot dive
  bool _pilotDiveSet; 
  /// keep pilot bank
  bool _pilotBankSet;

  // non-helper interface (joystick)
  float _bankWanted,_diveWanted;

  AutopilotState _state;

  // basic helicopter tactics - perform sweep over target

  enum SweepState {SweepDisengage,SweepTurn,SweepEngage,SweepFire};
  SweepState _sweepState;
  Time _sweepDelay;
  LinkTarget _sweepTarget;
  Vector3 _sweepDir;
  
  Matrix3 _worldToRelative;
  
  Vector3 DirectionWorldToRelative(Vector3Par dir) const {return _worldToRelative*dir;}
  
  public:
  HelicopterAuto( const EntityAIType *name, Person *pilot, bool fullCreate = true );

  virtual void Move(Matrix4Par transform);
  virtual void Init(Matrix4Par pos, bool init);
  
  virtual void Simulate( float deltaT, SimulationImportance prec );
  void AvoidGround( float minHeight );
  void AvoidCollision();

  
  void GetActions(UIActions &actions, AIBrain *unit, bool now, bool strict);
  virtual bool GetActionParams(UIActionParams &params, const Action *action, AIBrain *unit) const;
  void PerformAction(const Action *action, AIBrain *unit);

  bool IsStopped() const;

  void DamageCrew( EntityAI *killer, float howMuch,RString ammo );
  void Eject(AIBrain *unit, Vector3Val diff = VZero);

  float MakeAirborne();
  void SetFlyingHeight(float val);

  float GetFlyingHeight();
  float GetSpeedWanted() {return _pilotSpeed[2];} 

  void EngineOn();
  void EngineOff();
  void EngineOffAction();
  bool EngineIsOn() const;
  bool IRSignatureOn() const;


  bool FireWeapon(const TurretContextV &context, int weapon, TargetType *target, bool forceLock);
  virtual bool ProcessFireWeapon(
    const TurretContext &context, int weapon, const Magazine *magazine, EntityAI *target,
    const RemoteFireWeaponInfo *remoteInfo, RemoteFireWeaponInfo *localInfo, bool forceLock
  );
  void FireWeaponEffects(const TurretContext &context, int weapon, const Magazine *magazine,EntityAI *target, const RemoteFireWeaponInfo *remoteInfo);

  float FireValidTime() const {return 45;}

  void SuspendedPilot(AIBrain *unit, float deltaT );
  void KeyboardPilot(AIBrain *unit, float deltaT );

  void AutohoverOn();

  virtual bool GetAutoHover() {return _hoveringAutopilot;}

  void FakePilot( float deltaT );

  Vector3 GetStopPosition() const;

  void Autopilot(float deltaT, Vector3Par targetCur, Vector3Par targetEst, Vector3Par tgtSpeed, Vector3Par direction);
  
  void ResetAutopilot();
  void BrakingManeuver();

  // AI interface
  bool AimWeapon(const TurretContextEx &context, int weapon, Vector3Par direction);
  bool AimWeaponTgt(const TurretContextEx &context, int weapon, Target *target);
  bool CalculateAimWeaponPos(const TurretContext &context, int weapon, Vector3 &pos, float &timeToLead, Target *target, bool exact ) const;
  bool CalculateAimWeapon(const TurretContext &context, int weapon, Vector3 &dir, float &timeToLead, Target *target, bool exact ) const;

  Vector3 GetWeaponPoint(ObjectVisualState const& vs, const TurretContext &context, int weapon) const;
  Vector3 GetWeaponCenter(ObjectVisualState const& vs, const TurretContext &context, int weapon) const;
  Vector3 GetWeaponDirection(ObjectVisualState const& vs, const TurretContext &context, int weapon) const;
  Vector3 GetWeaponDirectionWanted(const TurretContextEx &context, int weapon) const;
  float GetAimed(const TurretContext &context, int weapon, Target *target, bool exact = false, bool checkLockDelay = false) const;

  float FireInRange(const TurretContext &context, int weapon, float &timeToAim, const Target &target) const;
  float FireAngleInRange(const TurretContext &context, int weapon, Vector3Par rel) const;

  void MoveWeapons( float deltaT );

#if _ENABLE_AI
  void AIGunner(TurretContextEx &context, float deltaT );
  void AIPilot(AIBrain *unit, float deltaT );
#endif

  void DrawDiags();
  RString DiagText() const;

  LSError Serialize(ParamArchive &ar);

  DECLARE_NETWORK_OBJECT
  static NetworkMessageFormat &CreateFormat
  (
    NetworkMessageClass cls,
    NetworkMessageFormat &format
  );
  static HelicopterAuto *CreateObject(NetworkMessageContext &ctx);
  TMError TransferMsg(NetworkMessageContext &ctx);
  float CalculateError(NetworkMessageContextWithError &ctx);

protected:
  void SwitchPilotHeightAGL(bool agl);

  USE_CASTING(base)
};

#endif

