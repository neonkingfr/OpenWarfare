// global redefinition of new, delete operators
#include "wpch.hpp"

#if defined _WIN32 && !defined MALLOC_WIN_TEST


#pragma optimize("t",on)
#if !_RELEASE
  // it seems in VS 2005 optimize("t",on) applies /Oy as well (Omit frame pointers)
  // in Testing we need stack frames for memory call-stack and crash-dumps
  #pragma optimize("y",off)
#endif

#include "memHeap.hpp"

#include <Es/Common/win.h>
#include <Es/Files/commandLine.hpp>
#include <El/QStream/qbStream.hpp>
#include <El/Debugging/debugTrap.hpp>
#include <El/Common/perfLog.hpp>
#include <El/Common/perfProf.hpp>
#include <Es/Containers/staticArray.hpp>

#include "memGrow.hpp"

#ifdef _XBOX
  #include <io.h>
  #include <fcntl.h>
  #if _ENABLE_REPORT
  #include <XbDm.h>
  #endif
#endif
//#include "engineDll.hpp"

#include <El/Debugging/imexhnd.h>

#if defined(_WIN32) && _ENABLE_REPORT && !MAIN_HEAP_MT_SAFE


static DWORD UnsafeHeapThread=GetCurrentThreadId();

static void MFSafetyBroken()
{
  Fail("MT safety broken");

#if _ENABLE_REPORT && !defined _XBOX
  CONTEXT context;
  context.ContextFlags = CONTEXT_FULL;
  RtlCaptureContext(&context);
  GDebugExceptionTrap.ReportContext("MT SAFETY BROKEN", &context, true);
  TerminateProcess(GetCurrentProcess(), 1);    
#endif
}
#define AssertThread() \
  if(GetCurrentThreadId()!=UnsafeHeapThread && UnsafeHeapThread) MFSafetyBroken()

#else

#define AssertThread()

#endif

#if 0

#ifndef MFC_NEW

#if MEM_CHECK || DO_MEM_STATS
#include "memCheck.hpp"
#endif







#if DO_MEM_STATS
  // check new call point histogram
  #include <El/Statistics/statistics.hpp>
  #include "keyInput.hpp"
  #include "dikCodes.h"

  //! allocation count statistics
  #if USE_MEM_COUNT
    static StatisticsByName MemCntStats INIT_PRIORITY_URGENT;
  #endif

  #if USE_MEM_FILENAME
    //! file based statistics
    static StatisticsByName MemTotStats INIT_PRIORITY_URGENT;
  #else if USE_MEM_CALLSTACK_CP
    static StatisticsById MemTotStats INIT_PRIORITY_URGENT;
  #endif
  #if USE_MEM_CALLSTACK_CS
    //! call stack based statistics
    static StatisticsByName MemCSStats INIT_PRIORITY_URGENT;
  #endif
  #if USE_MEM_CALLSTACK_CP
    //! call stack based statistics
    static StatisticsById MemCPStats INIT_PRIORITY_URGENT;
  #endif
  #if USE_MEM_CALLSTACK_CL
    //! call stack based statistics
    static StatisticsById MemCLStats INIT_PRIORITY_URGENT;
  #endif

  //! avoid recursion
  static int MemStatsDisabled=0;

  void ReportMemory();
  void ReportMemoryTotals();

  void MemoryFootprintSummary()
  {
    //MemTotStats.Clear();
    // scan all allocated blocks
    for
    (
      MemoryInfo *info=PAllocated->First(); info; info=PAllocated->Next(info)
    )
    {
      #if USE_MEM_FILENAME
        char buf[128];
        _snprintf(buf,sizeof(buf),"%s(%d): S",info->File(),info->Line());
        buf[sizeof(buf)-1]=0;
        MemTotStats.Count(buf,info->Size());
      #else if USE_MEM_CALLSTACK_CP
        if (info->CallstackSize()>0)
        {
          void *callplace = info->Callstack()[0];
          void *caller = 0;
          MemTotStats.Count(
            StatisticsById::Id((int)callplace,(int)caller),
            info->Size()
          );
        }
      #endif
      #if USE_MEM_CALLSTACK_CS
        MemCSStats.Count(info->Callstack(),info->CallstackSize()*sizeof (void *),info->Size());
      #endif
      #if USE_MEM_CALLSTACK_CP
        for (int i=0; i<info->CallstackSize(); i++)
        {
          void *callplace = info->Callstack()[i];
          void *caller = i<info->CallstackSize()-1 ? info->Callstack()[i+1] : 0;
          //void *caller = 0;
          MemCPStats.Count(
            StatisticsById::Id((int)callplace,(int)caller),
            info->Size()
          );
        }
      #endif
      #if USE_MEM_CALLSTACK_CL
        for (int i=0; i<info->CallstackSize(); i++)
        {
          void *callplace = info->Callstack()[i];
          void *caller = 0;
          MemCLStats.Count(
            StatisticsById::Id((int)callplace,(int)caller),
            info->Size()
          );
        }
      #endif
    }
    #if USE_MEM_FILENAME || USE_MEM_CALLSTACK_CP
      MemTotStats.Sample();
    #endif
    #if USE_MEM_CALLSTACK_CS
      MemCSStats.Sample();
    #endif
    #if USE_MEM_CALLSTACK_CP
      MemCPStats.Sample();
    #endif
    #if USE_MEM_CALLSTACK_CL
      MemCLStats.Sample();
    #endif
    ReportMemory();
  }
  void MemoryFootprint()
  {
    MemoryFootprintSummary();
    ReportMemoryTotals();
  }

  void CountNew( const char *file, int line, int size )
  {
    #if USE_MEM_COUNT
      if( MemStatsDisabled>0 ) return;
      MemStatsDisabled++;
      char buf[128];
      _snprintf(buf,sizeof(buf),"%s(%d): C",file,line);
      buf[sizeof(buf)-1]=0;
      MemCntStats.Count(buf);
      // scan all allocated blocks and add them to memory footprint
      MemStatsDisabled--;
    #endif
  }

  
  #if USE_MEM_CALLSTACK_CS
  void MemIdCallbackCS(const void *id, int idSize, int count)
  {
    // convert call stack to text 
    // report only big allocations
    void * const *ids = (void * const *)id;
    int idsSize = idSize/sizeof(void *);
    for (int i=0; i<idsSize; i++)
    {
      void *idcs = ids[i];
      // translate id to text representation
      const char *txt = GMapFile.MapNameFromPhysical((int)idcs);
      LogF("%32s: CS %6d",txt,count);
    }
    //sprintf(buf,"%32s: %6d\r\n",_data[i].name,_data[i].count/_factor);
    //f.write(buf,strlen(buf));
  }
  #endif

  #if USE_MEM_CALLSTACK_CP || USE_MEM_CALLSTACK_CL
  void MemIdCallbackCP(StatisticsById::Id id, int count)
  {
    // convert call stack to text 
    // report only big allocations
    char callPair[512];
    int id1Start;
    const char *name1 = GMapFile.MapNameFromPhysical(id._id1,&id1Start);
    sprintf(callPair,"%X:%s",id._id1,name1);
    if (id._id2)
    {
      int id2Start;
      const char *name2 = GMapFile.MapNameFromPhysical(id._id2,&id2Start);
      strcat(callPair," <- ");
      sprintf(callPair+strlen(callPair),"%X:%s",id._id2,name2);
    }
    LogF("%90s: CP %6d",callPair,count);
    //sprintf(buf,"%32s: %6d\r\n",_data[i].name,_data[i].count/_factor);
    //f.write(buf,strlen(buf));
  }
  #endif

  
  void ReportMemory()
  {
    MemStatsDisabled++;

    #if USE_MEM_FILENAME
      LogF("Allocation footprint report (%d samples)",MemTotStats.NSamples());
      LogF("-----------------------");
      MemTotStats.Report(64*1024);
      MemTotStats.ReportTotal();
    #else if USE_MEM_CALLSTACK_CP
      LogF("Allocation footprint report (%d samples)",MemTotStats.NSamples());
      LogF("-----------------------");
      MemTotStats.Report(MemIdCallbackCP,64*1024);
      MemTotStats.ReportTotal();
    #endif
    #if USE_MEM_CALLSTACK_CS
      MemCSStats.Report(MemIdCallbackCS,128*1024);
    #endif
    #if USE_MEM_CALLSTACK_CP
      LogF("Allocation call-pair report (%d samples)",MemCPStats.NSamples());
      LogF("-----------------------");
      MemCPStats.Report(MemIdCallbackCP,128*1024);
    #endif
    #if USE_MEM_CALLSTACK_CL
      LogF("Allocation call-place report (%d samples)",MemCLStats.NSamples());
      LogF("-----------------------");
      MemCLStats.Report(MemIdCallbackCP,128*1024);
    #endif
    #if USE_MEM_COUNT
      LogF("Allocation count report");
      LogF("-----------------------");
      MemCntStats.Report(100);
    #endif
    
    #if USE_MEM_FILENAME
      MemTotStats.Clear();
    #endif
    #if USE_MEM_CALLSTACK_CS
      MemCSStats.Clear();
    #endif
    #if USE_MEM_CALLSTACK_CP
      MemCPStats.Clear();
    #endif
    #if USE_MEM_CALLSTACK_CL
      MemCLStats.Clear();
    #endif
    #if USE_MEM_COUNT
      MemCntStats.Clear();
    #endif

    MemStatsDisabled--;
  }

#else
  void ReportMemoryTotals();
  void MemoryFootprint()
  {
    #if USE_MEM_CALLSTACK_CP
    ReportAllocated();
    #endif
    ReportMemoryTotals();
  }
  #define ReportMemory() GMemFunctions->ReportMemory(){}

  #define CountNew(file,line,size)
#endif

#if MAIN_HEAP_MT_SAFE
AtomicInt TotalAllocatedByActualAlloc;
#else
size_t TotalAllocatedByActualAlloc;
#endif

static bool MemoryErrorState = false;

void MemoryErrorReported()
{
  MemoryErrorState = true;
}


// manage memory pool
// memory pool is reserved when application is started
// and committed as necessary

// maybe we could maintain order of free blocks
// by address (this will speed-up Free) (est. 2x)
// by size (this will speed-up Alloc) (est. 2x)

int MemoryFreeBlocks();

class MemHeap;
class MemHeapLocked;

#if MAIN_HEAP_MT_SAFE
  #define MemHeapType MemHeapLocked
#else
  #define MemHeapType MemHeap
#endif


//extern MemHeap *SMemPtr;
extern MemHeapType *BMemPtr;



void MemoryErrorPage(size_t size);

#if 0 // _ENABLE_REPORT 

  #define VerifyStructure() DoAssert( CheckIntegrity() )

#else

  #define VerifyStructure()

#endif

#include <Es/Memory/normalNew.hpp>

/*!
\patch 5142 Date 3/20/2007 by Ondra
- Fixed: Reduced virtual address space usage on computers with 1 GB or more of RAM.
*/



//#pragma optimize ("a",off)

// Alloc and Free are used only within operator new/delete
// inlining avoids using 1:1 call

#if _RELEASE && _ENABLE_PERFLOG
  #define MEM_PERF_LOG 1
#endif

#if MEM_PERF_LOG
  #include <El/Common/perfLog.hpp>
  #include <El/Common/perfProf.hpp>
#endif


class RefHeap
{
  Ref<MemHeapType> _memory;

  public:
  operator MemHeapType *() {return _memory;}
  MemHeapType *operator ->() {return _memory;}

  RefHeap( int sizeMB=256 );
  ~RefHeap();
};

#if defined _XBOX && _ENABLE_CHEATS

  /// lock allowing safe access to XYYYMemoryAllocated variables
  CriticalSection XMemoryLock;

  // track memory allocated by XMemAlloc

  static int XPhysMemoryAllocated = 0;
  static int XVirtMemoryAllocated = 0;

  const int firstTrackedXAId = eXALLOCAllocatorId_D3D;
  const int nTrackedXAIds = eXALLOCAllocatorID_XMCORE+1-eXALLOCAllocatorId_D3D;

  // protected by XMemoryLock
  static int XPhysMemoryAllocatedById[nTrackedXAIds];
  static int XVirtMemoryAllocatedById[nTrackedXAIds];

#endif



size_t FreeOnDemandSystemMemoryLowLevel(size_t size);

int HandleOutOfMemory( size_t size )
{
   // Your code
   return FreeOnDemandSystemMemoryLowLevel(size);
}

//! initialization done after all static variables are initialized
void MemoryMainInit()
{
  #if MEM_CHECK
    InitMemoryCheck();
  #endif
}

void MemoryMainDone()
{
  //GMapFile.Clear();
}

void MemoryInit()
{
}


#if _ENABLE_REPORT
void ReportGC(const char *text)
{
  MEMORYSTATUS memstat;
  memstat.dwLength = sizeof(memstat);
  GlobalMemoryStatus(&memstat);
  size_t sysFree = memstat.dwAvailPhys;
  size_t heapFree = MemoryCommited()-MemoryUsed();
  size_t llFree = BMemory->TotalLowLevelMemory();
  LogF("%s: heap %d, system %d, LL %d",text,heapFree+sysFree,sysFree,llFree);
}
#endif


// check how much memory is allocated now
// this is platform dependent operation
#if _ENABLE_CHEATS

static size_t TotalAllocatedCheck()
{
  #ifdef _XBOX
    size_t normalHeap = BMemory->TotalAllocated();
    ScopeLock<CriticalSection> lock(XMemoryLock);
    size_t xPhysMem = XPhysMemoryAllocated;
    size_t xVirtMem = XVirtMemoryAllocated;
    return normalHeap+xPhysMem+xVirtMem;
  #else
    size_t normalHeap = BMemory->TotalAllocated();
    return normalHeap;
  #endif
}

#endif


/**
@param id any slots containing id in the name (ignore case) are processes
@param [out] total ammount of memory which was really released, based on system wide measurements
@return total size of items released
*/
size_t FreeOnDemandReleaseSlot(const char *id, size_t toRelease, size_t *verifyRelease)
{ 
  #if _ENABLE_CHEATS
  if (BMemory)
  {
    return BMemory->FreeOnDemandReleaseSlot(id,toRelease,verifyRelease);
  }
  #endif
  return 0;
}

#if _ENABLE_REPORT
size_t MemHeap::FreeOnDemandReleaseSlot(const char *id, size_t toRelease, size_t *verifyRelease)
{
  size_t released = 0;
  #if _ENABLE_CHEATS
  // check how much memory is free now
  // this is platform dependent operation
  size_t totalBefore = TotalAllocatedCheck();

  // may be called only from the main thread, because it is using the system allocations as well
  AssertThread();
  
  typedef IMemoryFreeOnDemand *(MemHeap::*GetFirstMemoryFreeOnDemandF)() const;
  typedef IMemoryFreeOnDemand *(MemHeap::*GetNextMemoryFreeOnDemandF)(IMemoryFreeOnDemand *cur) const;
  const static struct 
  {
    GetFirstMemoryFreeOnDemandF first;
    GetNextMemoryFreeOnDemandF next;
    const char *name;
  } onDemandFcs[]=
  {
    {&MemHeap::GetFirstFreeOnDemand,&MemHeap::GetNextFreeOnDemand,"Heap"},
    {&MemHeap::GetFirstFreeOnDemandSystem,&MemHeap::GetNextFreeOnDemandSystem,"Sys"},
    {&MemHeap::GetFirstFreeOnDemandLowLevel,&MemHeap::GetNextFreeOnDemandLowLevel,"LL"},
  };

  int slotCount = 0;
  RString lowId = id;
  lowId.Lower();
  int statsDiffTotal = 0;
  for (int i=0; i<sizeof(onDemandFcs)/sizeof(*onDemandFcs); i++)
  {
    for(
      IMemoryFreeOnDemand *mem = (this->*onDemandFcs[i].first)();
      mem;
      mem = (this->*onDemandFcs[i].next)(mem)
    )
    {
      // check if the name matches
      RString lowName = mem->GetDebugName();
      lowName.Lower();
      if (strstr(lowName,lowId))
      {
        size_t totalBeforeItem = TotalAllocatedCheck();
        size_t statBefore = mem->MemoryControlled();
        size_t memReleased = mem->Free(toRelease);
        size_t statAfter = mem->MemoryControlled();
        size_t totalAfterItem = TotalAllocatedCheck();
        // print single slot stats
        if (memReleased>0 || totalAfterItem<totalBeforeItem || statAfter<statBefore)
        {
          LogF(
            "Slot %s: released %d, allocation difference %d, stats difference %d",
            (const char *)mem->GetDebugName(),
            memReleased,totalBeforeItem-totalAfterItem,statBefore-statAfter
          );
          released += memReleased;
          statsDiffTotal += statBefore-statAfter;
          slotCount++;
        }
      }
    }
  }
  size_t totalAfter = TotalAllocatedCheck();
  // if there were mutliple slots affects, print multiple slot stats
  if (slotCount>1)
  {
    LogF(
      "Released %d, allocation difference %d, stats difference %d",
      released,totalBefore-totalAfter,statsDiffTotal
    );
  }
  if (verifyRelease)
  {
    *verifyRelease = totalBefore-totalAfter;
  }
  #endif
  return released;
}
#endif

int FrameId=0;

void FreeOnDemandFrame()
{
  if (BMemory)
  {
    PROFILE_SCOPE_EX(gbFrm,mem); // scope to ensure memory CS locks are inside of a common scope
    BMemory->FreeOnDemandFrame();
  }
}

#include "El/FileServer/fileServerMT.hpp"


void MemHeap::FreeOnDemandFrame()
{
  // FrameId needs to be changed atomically with FileServerST::MemoryControlledFrame
  ScopeLock<FileServerST> fsLock(SFileServer);
//   ScopeLock<MemHeap> memLock(*this);
  
  // called only from main thread - accessing system allocator
  AssertThread();
  // note: memory lock not taken (see MemHeapLocked::FreeOnDemandFrame)
  // note: we do not need memory CS lock just to traverse on demand lists, as it is stable
  typedef IMemoryFreeOnDemand *(MemHeap::*GetFirstMemoryFreeOnDemandF)() const;
  typedef IMemoryFreeOnDemand *(MemHeap::*GetNextMemoryFreeOnDemandF)(IMemoryFreeOnDemand *cur) const;
  const static struct 
  {
    GetFirstMemoryFreeOnDemandF first;
    GetNextMemoryFreeOnDemandF next;
    const char *name;
  } onDemandFcs[]=
  {
    {&MemHeap::GetFirstFreeOnDemand,&MemHeap::GetNextFreeOnDemand,"Heap"},
    {&MemHeap::GetFirstFreeOnDemandSystem,&MemHeap::GetNextFreeOnDemandSystem,"Sys"},
    {&MemHeap::GetFirstFreeOnDemandLowLevel,&MemHeap::GetNextFreeOnDemandLowLevel,"LL"},
  };
  for (int i=0; i<sizeof(onDemandFcs)/sizeof(*onDemandFcs); i++)
  {
    for
    (
      IMemoryFreeOnDemand *mem = (this->*onDemandFcs[i].first)();
      mem;
      mem = (this->*onDemandFcs[i].next)(mem)
    )
    {
      mem->MemoryControlledFrame();
    }
  }  
  
  FrameId++;
}


void RegisterFreeOnDemandMemory(IMemoryFreeOnDemand *object)
{
#ifdef _WIN32
  DoAssert( BMemory );
#endif
  if( BMemory )
  {
    BMemory->RegisterFreeOnDemand(object);
  }
}


void RegisterFreeOnDemandLowLevelMemory(IMemoryFreeOnDemand *object)
{
#ifdef _WIN32
  DoAssert( BMemory );
#endif
  if( BMemory )
  {
    BMemory->RegisterFreeOnDemandLowLevel(object);
  }
}

size_t FreeOnDemandLowLevelMemory(size_t size)
{
  if (BMemory)
  {
    return BMemory->FreeOnDemandLowLevelMemory(size);
  }
  return 0;
}

/**
Perform any possible low-level memory freeing as necessary 

Note: different from FreeOnDemandLowLevel
*/
size_t MemHeap::FreeOnDemandLowLevelMemory(size_t size)
{
  // balanced releasing of all kinds of memory
  // note: allocation here is extremely dangerous
  AUTO_STATIC_ARRAY(OnDemandStats,stats,128);

  for(IMemoryFreeOnDemand *walk=GetFirstFreeOnDemandLowLevel(); walk; walk = GetNextFreeOnDemandLowLevel(walk))
  {
    OnDemandStats &mstat = stats.Append();
    mstat.mem = walk;
    mstat.system = false;
  }
  size_t systemReleased;
  size_t released = MemoryFreeOnDemandList::BalanceList(size,stats.Data(),stats.Size(),NULL,0,&systemReleased);
  #if 0 // _ENABLE_REPORT
  if (released>0)
  {
    LogF("Low level memory release forced - %d B",released);
  }
  #endif
  return released;
}

void FreeOnDemandLock()
{
  if (BMemory) BMemory->Lock();
}
void FreeOnDemandUnlock()
{
  if (BMemory) BMemory->Unlock();
}

IMemoryFreeOnDemand *GetFirstFreeOnDemandMemory()
{
  if (BMemory) return BMemory->GetFirstFreeOnDemand();
  return NULL;
}
IMemoryFreeOnDemand *GetNextFreeOnDemandMemory(IMemoryFreeOnDemand *cur)
{
  if (BMemory) return BMemory->GetNextFreeOnDemand(cur);
  return NULL;
}

IMemoryFreeOnDemand *GetFirstFreeOnDemandLowLevelMemory()
{
  if (BMemory) return BMemory->GetFirstFreeOnDemandLowLevel();
  return NULL;
}
IMemoryFreeOnDemand *GetNextFreeOnDemandLowLevelMemory(IMemoryFreeOnDemand *cur)
{
  if (BMemory) return BMemory->GetNextFreeOnDemandLowLevel(cur);
  return NULL;
}



//////////////////////////////////////////////////////////////////////////


/// coalesce regions with the same flags
class CoalesceRegions
{
  DWORD _state;
  DWORD _type;
  SIZE_T _addr;
  SIZE_T _regionSize;
public:
  CoalesceRegions()
  {
    _state = 0;
    _type = 0;
    _regionSize = 0;
    _addr = 0;
  }
  
  template <class FlushFunc>
  void Flush(const FlushFunc &func)
  {
    if (_regionSize>0)
    {
      func(_state,_type,_addr,_regionSize);
    }
  }

  template <class FlushFunc>
  void Add(DWORD state, DWORD type, SIZE_T addr, SIZE_T regionSize, const FlushFunc &func)
  {
    if (_state!=state || addr!=_addr+_regionSize || type!=_type)
    {
      if (_regionSize>0)
      {
        func(_state,_type,_addr,_regionSize);
      }
      _state = state;
      _type = type;
      _addr = addr;
      _regionSize = regionSize;
    }
    else
    {
      _regionSize += regionSize;
    }
  }
};

enum
{
  PageOwnFree,
  /// page reserved by some heap under our control
  PageOwnReservedHeap=0x10,
  PageOwnReservedCommittedLast=PageOwnReservedHeap+15,
  /// page committed by some heap under our control
  PageOwnCommittedHeap,
  PageOwnCommittedHeapLast=PageOwnCommittedHeap+15,
  /// page reserved by some library (most likely D3D + driver)
  PageOwnReservedOther,
  PageOwnReservedOtherLast=PageOwnReservedOther+15,
  PageOwnCommittedOther,
  PageOwnCommittedOtherLast=PageOwnCommittedOther+15,
};

#ifndef _XBOX
struct WriteRegionToRpt
{
  void operator () (char state, DWORD type, SIZE_T addr, SIZE_T regionSize) const
  {
    const char *stateDesc = "???";
    switch (state)
    {
      #define CASE_ENUM(x) case PageOwn##x: stateDesc = #x; break;
      CASE_ENUM(Free)
      CASE_ENUM(ReservedHeap)
      CASE_ENUM(ReservedHeap+1)
      CASE_ENUM(ReservedHeap+2)
      CASE_ENUM(ReservedHeap+3)
      CASE_ENUM(ReservedHeap+4)
      CASE_ENUM(ReservedHeap+5)
      CASE_ENUM(ReservedHeap+6)
      CASE_ENUM(ReservedHeap+7)
      CASE_ENUM(CommittedHeap)
      CASE_ENUM(CommittedHeap+1)
      CASE_ENUM(CommittedHeap+2)
      CASE_ENUM(CommittedHeap+3)
      CASE_ENUM(CommittedHeap+4)
      CASE_ENUM(CommittedHeap+5)
      CASE_ENUM(CommittedHeap+6)
      CASE_ENUM(CommittedHeap+7)
      CASE_ENUM(ReservedOther)
      CASE_ENUM(CommittedOther)
      #undef CASE_ENUM
    }
    const char *typeDesc = "";
    switch (type)
    {
      #define CASE_ENUM(x) case MEM_##x: stateDesc = #x; break;
      CASE_ENUM(IMAGE)
      CASE_ENUM(MAPPED)
      CASE_ENUM(PRIVATE)
      #undef CASE_ENUM
    }
    RptF("Region: %p,%d KB,%s %s",addr,regionSize/1024,stateDesc,typeDesc);
  }
};
#endif

extern int CheckHeapOwnerShip(size_t addr, size_t size);



#if _ENABLE_REPORT && !defined _XBOX
  #define _VM_OWN_TRACK 0
#endif

#if _VM_OWN_TRACK
/// store last known ownership for each memory page
// possible ownership values:


struct WriteDiffToLog
{
  void operator () (DWORD state, DWORD type, SIZE_T addr, SIZE_T regionSize) const
  {
    const char *stateDesc = "???";
    switch (state)
    {
      #define CASE_ENUM(x) case PageOwn##x: stateDesc = #x; break;
      CASE_ENUM(Free)
      CASE_ENUM(ReservedHeap)
      CASE_ENUM(ReservedHeap+1)
      CASE_ENUM(ReservedHeap+2)
      CASE_ENUM(ReservedHeap+3)
      CASE_ENUM(ReservedHeap+4)
      CASE_ENUM(ReservedHeap+5)
      CASE_ENUM(ReservedHeap+6)
      CASE_ENUM(ReservedHeap+7)
      CASE_ENUM(CommittedHeap)
      CASE_ENUM(CommittedHeap+1)
      CASE_ENUM(CommittedHeap+2)
      CASE_ENUM(CommittedHeap+3)
      CASE_ENUM(CommittedHeap+4)
      CASE_ENUM(CommittedHeap+5)
      CASE_ENUM(CommittedHeap+6)
      CASE_ENUM(CommittedHeap+7)
      CASE_ENUM(ReservedOther)
      CASE_ENUM(CommittedOther)
      #undef CASE_ENUM
    }
    const char *typeDesc = "";
    switch (type)
    {
      #define CASE_ENUM(x) case MEM_##x: stateDesc = #x; break;
      CASE_ENUM(IMAGE)
      CASE_ENUM(MAPPED)
      CASE_ENUM(PRIVATE)
      #undef CASE_ENUM
    }
    LogF("Diff: %p,%6d KB,%s %s",addr,regionSize/1024,stateDesc,typeDesc);
  }
};


char MapOwnership[0x80000000U/0x1000U];

static char CanonicalOwner(char owner)
{
  if (owner==PageOwnFree) return 0;
  // when owner is the same, we do nor care if it is committed or reserved
  if (owner>=PageOwnReservedHeap && owner<=PageOwnCommittedHeapLast) return 1;
  if (owner>=PageOwnReservedOther && owner<=PageOwnCommittedOtherLast) return 2;
  return owner;
}

static bool ChangedOwner(char owner1, char owner2)
{
  char o1Canonical = CanonicalOwner(owner1);
  char o2Canonical = CanonicalOwner(owner2);
  return o1Canonical!=o2Canonical;
}

#endif

static char GetPageState(DWORD bufferState, size_t addr, size_t pageSize)
{
  char state = PageOwnFree;
  if (bufferState&MEM_COMMIT)
  {
    state = PageOwnCommittedOther;
    int own = CheckHeapOwnerShip(addr,pageSize);
    if (own>=0)
    {
      state = PageOwnCommittedHeap+own;
    }
  }
  else if (bufferState&MEM_RESERVE)
  {
    state = PageOwnReservedOther;
    int own = CheckHeapOwnerShip(addr,pageSize);
    if (own>=0)
    {
      state = PageOwnReservedHeap+own;
    }
  }
  return state;
}

// we report any differences against the map
void PrintVMMap(int extended, const char *title=NULL)
{
#ifndef _XBOX
  if (title) LogF("VM map %s",title);
  // print map of virtual memory
  // scan whole application space (2 GB) address range
  // do not try to access kernel space - we would get no information anyway
  size_t start = 0;
  size_t end = 1U<<31;
  SYSTEM_INFO si;
  GetSystemInfo(&si);
  size_t pageSize = si.dwPageSize;
  DoAssert(pageSize>=0x1000); // assumption made by MapOwnership
  size_t longestFreeApp = 0;
  
  /*
  Bug: a lot of small committed mapped pages (8 KB regions) interleaved with 60-64 KB free regions.
  Suspected SoundMax HD driver leak.
  */
  /// compute small mapped regions
  int smallMappedRegions = 0;
  size_t smallMappedRegionsSize = 0;
  CoalesceRegions coalesce;
  CoalesceRegions coalesceDiff;
  int index=0;
  for (size_t addr = start; addr<end; )
  {
    MEMORY_BASIC_INFORMATION buffer;
    SIZE_T retSize = VirtualQuery((void *)addr,&buffer,sizeof(buffer));
    if (retSize==sizeof(buffer) && buffer.RegionSize>0)
    {
      // detect suspected "leak" pattern
      if (buffer.Type==MEM_MAPPED && buffer.RegionSize<=8*1024)
      {
        smallMappedRegions++;
        smallMappedRegionsSize+= buffer.RegionSize;
      }
      if (extended>0)
      {
        char state = GetPageState(buffer.State, addr, pageSize);
        coalesce.Add(state,buffer.Type,addr,buffer.RegionSize,WriteRegionToRpt());
      }
      #if _VM_OWN_TRACK
      if (extended==0)
      {
        for (size_t page=0,pIndex=index; page<buffer.RegionSize; page+=pageSize,pIndex++)
        {
          // mark each page 
          // check what ownership do we have now
          char state = GetPageState(buffer.State, addr+page, pageSize);
          if (ChangedOwner(MapOwnership[pIndex],state))
          {
            coalesceDiff.Add(state,buffer.Type,addr+page,pageSize,WriteDiffToLog());
          }
          MapOwnership[pIndex] = state;
        }
      }
      #endif
      // dump information about this region
      if (buffer.State&MEM_FREE)
      {
        if (buffer.RegionSize>longestFreeApp) longestFreeApp = buffer.RegionSize;
      }
      addr += buffer.RegionSize;
      index+= buffer.RegionSize/pageSize;
    }
    else
    {
      // always proceed
      addr += pageSize;
      index++;
    }
  }
  if (extended)
  {
    coalesce.Flush(WriteRegionToRpt());
  }
  #if _VM_OWN_TRACK
  if (extended==0)
  {
    coalesceDiff.Flush(WriteDiffToLog());
  }
  #endif
  RptF("Longest free VM region: %d",longestFreeApp);
  RptF("Small mapped regions: %d, size %d B",smallMappedRegions,smallMappedRegionsSize);
#endif
}



#if _ENABLE_REPORT
// create and compare virtual memory snapshots
// for each VM page identify its owner

#endif

void ReportMemoryStatus(int level);

void ReportMemoryStatus()
{
  ReportMemoryStatus(1);
}

void MemoryErrorMalloc( int size )
{
  MemoryErrorReported();
  #ifdef _XBOX
    Fail("Out of system memory");
  #endif
  ReportMemoryStatus();
  ErrorMessage("Out of Win32 memory (%d KB).\n",size);
}

#if _USE_DEAD_LOCK_DETECTOR
//Global DeadLockDetector variable
DeadLockDetector GDeadLockDetector;
#endif

#include "Es/essencepch.hpp"
#include "Es/Framework/netlog.hpp"
// no netlog in Xbox retail
#if _ENABLE_REPORT || !defined(_XBOX)
  #if defined(NET_LOG) && defined(EXTERN_NET_LOG)
  #  ifdef NET_LOG_PERIOD
  NetLogger netLogger(NET_LOG_PERIOD);
  #  else
  NetLogger netLogger;
  #  endif
  #endif
#endif

#else

MemFunctions OMemFunctions INIT_PRIORITY_URGENT;
MemFunctions OSafeMemFunctions INIT_PRIORITY_URGENT;
MemFunctions *GMemFunctions = &OMemFunctions;
MemFunctions *GSafeMemFunctions = &OSafeMemFunctions;

// no garbage collect or any on demand releasing with default new

void RegisterFreeOnDemandMemory(IMemoryFreeOnDemand *object)
{
}

size_t MemoryFreeOnDemand(size_t size)
{
  return 0;
}

void FreeOnDemandGarbageCollectMain(size_t freeRequired, size_t freeSysRequired, bool isMain)
{
}

mem_size_t GetMemoryUsageLimit(mem_size_t *virtualLimit)
{
  if (virtualLimit) *virtualLimit = INT_MAX;
  return INT_MAX; // no limit - 2GB allowed
}

int FrameId=0;

void FreeOnDemandFrame()
{
  FrameId++;
}

void RegisterFreeOnDemandSystemMemory(IMemoryFreeOnDemand *object)
{
}

void RegisterFreeOnDemandLowLevelMemory(IMemoryFreeOnDemand *obj)
{
}

void FreeOnDemandGarbageCollectSystemMemoryLowLevel(size_t freeSysRequired)
{
}

size_t FreeOnDemandSystemMemoryLowLevel(size_t size)
{
  return 0;
}

size_t FreeOnDemandSystemMemory(
  size_t size, IMemoryFreeOnDemand **extras, int nExtras
)
{
  return 0;
}

void MemoryInit(){}
void MemoryDone(){}
void MemoryFootprint(){}
void PrintVMMap(int extended, const char *title=NULL){}

#endif

#ifdef _XBOX


LPVOID WINAPI XMemAlloc(SIZE_T dwSize, DWORD dwAllocAttributes)
{
  Retry:
  LPVOID mem = XMemAllocDefault(dwSize,dwAllocAttributes);
  if (!mem)
  {
    // we should try releasing some low-level memory
    // note: FreeOnDemandSystemMemoryLowLevel is thread safe
    size_t released = FreeOnDemandSystemMemoryLowLevel(dwSize);
    if (released>0)
    {
      //LogF("XMemAlloc: low-level released %d, requested %d",released,dwSize);
      goto Retry;
    }
    ErrF("XMemAlloc %d failed",dwSize);
    return mem;
  }
  
  #if _ENABLE_CHEATS
    XALLOC_ATTRIBUTES attr = *(XALLOC_ATTRIBUTES *)&dwAllocAttributes;
    SIZE_T memSize = XMemSizeDefault(mem,dwAllocAttributes);
    // lock access to memory tracking stats
    ScopeLock<CriticalSection> lock(XMemoryLock);
    
    int *totAlloc = NULL;
    #if _ENABLE_CHEATS
    int *idAlloc = NULL;
    const char *name = "";
    #endif
    if (attr.dwMemoryType==XALLOC_MEMTYPE_PHYSICAL)
    {
      totAlloc = &XPhysMemoryAllocated;
      #if _ENABLE_CHEATS
      idAlloc = XPhysMemoryAllocatedById;
      name = "Phys";
      #endif
    }
    else
    {
      totAlloc = &XVirtMemoryAllocated;
      #if _ENABLE_CHEATS
      idAlloc = XVirtMemoryAllocatedById;
      name = "Virt";
      #endif
    }
    *totAlloc += memSize;
    #if _ENABLE_CHEATS
      int i = attr.dwAllocatorId-firstTrackedXAId;
      //LogF("Alloc - Memory %s:%d  - %d",name,i,memSize);
      if (i>=0 && i<nTrackedXAIds)
      {
        idAlloc[i]+= memSize;
      }
    #endif
  #endif
  
  return mem;
}

VOID WINAPI XMemFree(PVOID pAddress, DWORD dwAllocAttributes)
{
  // X360 calls XMemFree with NULL during init phase
  if (pAddress==NULL) return;
  #if _ENABLE_CHEATS
  
  XALLOC_ATTRIBUTES attr = *(XALLOC_ATTRIBUTES *)&dwAllocAttributes;

  SIZE_T memSize = XMemSizeDefault(pAddress,dwAllocAttributes);
  
  int *totAlloc = NULL;
  #if _ENABLE_CHEATS
  int *idAlloc = NULL;
  const char *name = "";
  #endif
  ScopeLock<CriticalSection> lock(XMemoryLock);
  if (attr.dwMemoryType==XALLOC_MEMTYPE_PHYSICAL)
  {
    totAlloc = &XPhysMemoryAllocated;
    #if _ENABLE_CHEATS
    idAlloc = XPhysMemoryAllocatedById;
    name = "Phys";
    #endif
  }
  else
  {
    totAlloc = &XVirtMemoryAllocated;
    #if _ENABLE_CHEATS
    idAlloc = XVirtMemoryAllocatedById;
    name = "Virt";
    #endif
  }

  *totAlloc -= memSize;
  if (*totAlloc<0)
  {    
    #if _ENABLE_CHEATS
      LogF("Memory %s underflow %d",name,memSize);
    #endif
    *totAlloc = 0;
  }
  #if _ENABLE_CHEATS
    int i = attr.dwAllocatorId-firstTrackedXAId;
    //LogF("Free  - Memory %s:%d  - %d",name,i,memSize);
    if (i>=0 && i<=nTrackedXAIds)
    {
      idAlloc[i] -= memSize;
      if (idAlloc[i]<0)
      {
        LogF("Memory %s:%d underflow %d",name,i,memSize);
        idAlloc[i] = 0;
      }
    }
  #endif
  
  #endif
  return XMemFreeDefault(pAddress,dwAllocAttributes);
}

SIZE_T WINAPI XMemSize(PVOID pAddress, DWORD dwAllocAttributes)
{
  return XMemSizeDefault(pAddress,dwAllocAttributes);
}
#endif

#if _ENABLE_REPORT && !defined MFC_NEW
RString GetMemStat(mem_size_t statId, mem_size_t &statVal);

void ReportMemoryTotals()
{
  FastCAlloc::ReportTotals();
  LogF("Fastalloc requested %d",FastCAlloc::TotalRequested());
  LogF("Fastalloc allocated %d",FastCAlloc::TotalAllocated());
  if (BMemory)
  {
    BMemory->ReportTotals();
    BMemory->ReportAnyMemory("");
    // report all basic memory stats
    for( int c=0; c<100; c++ )
    {
      int value;
      RString name = GetMemStat(c,value);
      if (name.GetLength()==0) break;
      if (value==0) continue;
      LogF("%s - %d",(const char *)name,value);
    }
  }
#ifdef _XBOX
  DM_MEMORY_STATISTICS stats;
  stats.cbSize = sizeof(stats);
  DmQueryMemoryStatistics(&stats);
  #define LOG_DMS(x) LogF(#x " %d",stats.x##Pages*4096);
  LOG_DMS(Total);
  LOG_DMS(Available);
  LOG_DMS(Stack);
  LOG_DMS(VirtualPageTable);
  LOG_DMS(SystemPageTable);
  LOG_DMS(Pool);
  LOG_DMS(VirtualMapped);
  LOG_DMS(Image);
  LOG_DMS(FileCache);
  LOG_DMS(Contiguous);
  LOG_DMS(Debugger);
  #undef LOG_DMS
#endif
}
#else
void ReportMemoryTotals()
{
}
#endif

#endif

static MemoryFreeOnDemandList GFreeOnDemandSystem; // memory allocated by GlobalAlloc, malloc, etc..

void RegisterFreeOnDemandSystemMemory(IMemoryFreeOnDemand *object)
{
  GFreeOnDemandSystem.Register(object);
}

IMemoryFreeOnDemand *GetFirstFreeOnDemandSystemMemory()
{
  return GFreeOnDemandSystem.First();
}
IMemoryFreeOnDemand *GetNextFreeOnDemandSystemMemory(IMemoryFreeOnDemand *cur)
{
  return GFreeOnDemandSystem.Next(cur);
}

TypeIsSimple(IMemoryFreeOnDemand *)

//! balance both system and heap memory at the same time
mem_size_t FreeOnDemandAnyMemory(
  mem_size_t size, bool physical, mem_size_t &systemReleased,
  IMemoryFreeOnDemand **extras, int nExtras
)
{
  AssertThread();
  
  // note: allocation here is extremely dangerous
  // we need to avoid it at all costs
  AUTO_STATIC_ARRAY(OnDemandStats,stats,128);
  AUTO_STATIC_ARRAY(IMemoryFreeOnDemand *,lowLevel,16);

  const Memory::LockedFreeOnDemandEnumerator &demand = Memory::GetFreeOnDemandEnumerator();
  
  for(int i=0; i<nExtras; i++)
  {
    if (!extras[i]) continue;
    if (!physical && !extras[i]->UsesVirtualMemory()) continue;
    OnDemandStats &mstat = stats.Append();
    mstat.mem = extras[i];
    mstat.system = false;
  }
  // BalanceList needs to get information about low level memory so that it can keep a space for it
  for(IMemoryFreeOnDemand *walk=demand.GetFirstFreeOnDemandLowLevel(); walk; walk = demand.GetNextFreeOnDemandLowLevel(walk))
  {
    if (!physical && !walk->UsesVirtualMemory()) continue;
    lowLevel.Add(walk);
  }
  for(IMemoryFreeOnDemand *walk=demand.GetFirstFreeOnDemand(); walk; walk = demand.GetNextFreeOnDemand(walk))
  {
    if (!physical && !walk->UsesVirtualMemory()) continue;
    OnDemandStats &mstat = stats.Append();
    mstat.mem = walk;
    mstat.system = false;
  }
  for(IMemoryFreeOnDemand *walk=GetFirstFreeOnDemandSystemMemory(); walk; walk = GetNextFreeOnDemandSystemMemory(walk))
  {
    if (!physical && !walk->UsesVirtualMemory()) continue;
    OnDemandStats &mstat = stats.Append();
    mstat.mem = walk;
    mstat.system = true;
  }
  
  return MemoryFreeOnDemandList::BalanceList(size,stats.Data(),stats.Size(),lowLevel.Data(),lowLevel.Size(),&systemReleased);
}

void PrintVMMap(int extended, const char *title=NULL);
void ReportMemoryStatus();
IMemoryFreeOnDemand *ProgressGetFreeOnDemandInterface();

size_t FreeOnDemandSystemMemoryLowLevel(size_t size)
{
  PROFILE_SCOPE_EX(gbSML,mem);
  size_t released = 0;
  size_t pageSize = GetPageRecommendedSize();
  while (size>0)
  {
    // try decommitting some unused heap memory
    size_t toRelease = size;
    if (toRelease<pageSize) toRelease = pageSize;
    size_t sizeFree = Memory::FreeSystemMemory(toRelease);
    if (sizeFree==0)
    {
      // no system memory released - we need to release some low-level releasable memory
      // note: as we are releasing directly from the memory store, it means we are decomitting it directly
      // even if we would be wrong, the caller would fail again, call us again, and we would fix it by decomitting
      sizeFree = FreeOnDemandLowLevelMemory(toRelease);
      if (sizeFree==0)
      {
        // nothing more to release
        break;
      }
    }
    if (sizeFree<size)
    {
      size -= sizeFree;
      released += sizeFree;
    }
    else
    {
      size = 0;
      released += sizeFree;
    }
  }
  #if _ENABLE_REPORT
    if (released<=0)
    {
      ReportMemoryStatus();
      // report how much memory is allocated in various containers
      //ReportAnyMemory("No low-level sysmem to release");
    }
  #endif
  return released;
}

void FreeOnDemandGarbageCollectSystemMemoryLowLevel(size_t freeSysRequired)
{
  #ifdef _XBOX
    for(;;)
    {
      MEMORYSTATUS memstat;
      memstat.dwLength = sizeof(memstat);
      GlobalMemoryStatus(&memstat);
      size_t sysFree = memstat.dwAvailPhys;
      if (sysFree>=freeSysRequired)
      {
        return;
      }
      size_t toRelease = freeSysRequired-sysFree;
      size_t released = FreeOnDemandSystemMemoryLowLevel(toRelease);
      if (released==0)
      {
        LogF("Caution: not enough system memory was released during GC");
        return;
      }
      if (released>=toRelease)
      {
        break;
      }
    }
    
  #endif
}

size_t FreeOnDemandSystemMemory(
  size_t size, IMemoryFreeOnDemand **extras, int nExtras
)
{
  PROFILE_SCOPE_EX(gbSM,mem);
  mem_size_t released = 0;
  size_t pageSize = GetPageRecommendedSize();
  while (size>0)
  {
    // try decommitting some unused heap memory
    size_t toRelease = size;
    if (toRelease<pageSize) toRelease = pageSize;
    size_t sizeFree = Memory::FreeSystemMemory(toRelease);
    released += sizeFree;
    if (sizeFree<size)
    {
      size -= sizeFree;
    }
    else
    {
      size = 0;
      break;
    }
    
    if (!sizeFree)
    {
      // check how much system memory was released during this operation
      mem_size_t toRelease = size;
      if (toRelease<pageSize) toRelease = pageSize;
      mem_size_t systemReleased;
      mem_size_t totalReleased = FreeOnDemandAnyMemory(toRelease,false,systemReleased,extras,nExtras);
      released += systemReleased;
      if (systemReleased<size)
      {
        size -= systemReleased;
      }
      else
      {
        size = 0;
        break;
      }
      if (!totalReleased)
      {
        // nothing more to release
        break;
      }
    }
  }
  #if _ENABLE_REPORT
    if ((int)size>0 && released<=0)
    {
      // report how much memory is allocated in various containers
      Memory::ReportAnyMemory("No sysmem to release");
    }
  #endif
  return MemSizeTToSizeT(released);
}

mem_size_t ThrottleMemoryUsagePhysical;
mem_size_t ThrottleMemoryUsageVirtual;


template <typename TypeA, typename TypeMin, typename TypeMax>
inline void saturate( TypeA &a, TypeMin min, TypeMax max )
{
  if( a<min ) a=min;
  if( a>max ) a=max;
}

void ChangeMemoryUsageLimit(int memLimit)
{
  const int oneMB = mem_size_t(1024*1024);
  ThrottleMemoryUsagePhysical += memLimit*16*oneMB;
  ThrottleMemoryUsageVirtual += memLimit*16*oneMB;
  saturate(ThrottleMemoryUsagePhysical,512*oneMB,2047*oneMB);
  saturate(ThrottleMemoryUsageVirtual,512*oneMB,2047*oneMB);
  DIAG_MESSAGE(2000,Format("Heap limit %lld MB, Virtual %lld MB",ThrottleMemoryUsagePhysical/oneMB,ThrottleMemoryUsageVirtual/oneMB));
}

mem_size_t GetMemoryUsageLimit(mem_size_t *virtualLimit)
{
  if (virtualLimit) *virtualLimit = ThrottleMemoryUsageVirtual;
  return ThrottleMemoryUsagePhysical;
}

// command line context
struct MemArgumentContext
{
  int maxSize;
};

static int ExThreads = ~0;

bool CheckExThreads(int mask, bool autodetected)
{
  if (ExThreads==~0) return autodetected;
  return (ExThreads&mask)!=0;
}

/// check if an option with arguments matches
static bool IsOptionWArgs(const char *beg, const char *opt)
{
  // when this is used, the last character of opt should be '=', like in -x=800
  Assert(opt[0]!=0 && opt[strlen(opt)-1]=='=');
  return !strnicmp(beg,opt,strlen(opt));
}

static bool IsOption(const char *beg, const char *end, const char *opt)
{
  return end-beg==(int)strlen(opt) && !strnicmp(beg,opt,end-beg);
}

bool MemoryValidator;

static bool MemSingleArgument(const CommandLine::SingleArgument &arg, MemArgumentContext &ctx)
{
  // check if arg is recognized
  const char *beg = arg.beg;
  const char *end = arg.end;

  if( end==beg ) return false;
  if( *beg=='-' || *beg=='/' )
  {
    beg++;
    // option
    static const char maxmem[]="maxmem=";
    static const char memoryvalidator[]="mv";
    static const char exThreads[]="exthreads=";

    if( !strnicmp(beg,maxmem,strlen(maxmem)) )
    {
      ctx.maxSize = atoi(beg+strlen(maxmem));
    }
    if (IsOption(beg,end,memoryvalidator))
    {
      MemoryValidator = true;
    }
    else if (IsOptionWArgs(beg,exThreads))
      ExThreads=atoi(beg + strlen(exThreads));
  }
  return false;
}

/*!
\patch 5123 Date 1/29/2007 by Ondra
- Improved: Increased internal cache size for computers with more than 512 MB of RAM.
*/

int DetectHeapSizeMB()
{
  #if defined _WIN32
  // 768 seems to be the most stable setting available
  int sizeMBPhys = 768;
  int sizeMBVirt = 768;
  #ifndef _XBOX
    MEMORYSTATUSEX memstat;
    memstat.dwLength = sizeof(memstat);
    GlobalMemoryStatusEx(&memstat);
    if (memstat.ullTotalPhys>1024*1024*1024)
    {
      // autodetection changes only physical memory limit, not the virtual one
      int totalMB = int(memstat.ullTotalPhys>>20);
      int sizeMBPage = int(memstat.ullAvailPageFile>>20);
      sizeMBPhys = (totalMB-512)*2/3;
      saturate(sizeMBPhys,768,8*1024);
      // never commit more than allowed by a page file
      if (sizeMBPhys>sizeMBPage)
      {
        RptF("Warning: memory usage limited by a page file. Current limit %d MB, wanted %d MB.",sizeMBPage,sizeMBPhys);
        RptF("Increasing your page file size might improve game performance.");
        sizeMBPhys = sizeMBPage;
      }
    }
    if (memstat.ullAvailVirtual>3ULL*1024*1024*1024)
    {
      // allow 1.5 GB of virtual space on x64 with LARGEADDRESSAWARE
      sizeMBVirt = 1024+512;
    }
  #endif
  // autoselection can be overridden from the command line
  CommandLine cmd(GetCommandLine());
  cmd.SkipArgument(); // skip exe name
  MemArgumentContext ctx;
  ctx.maxSize = 0;
  ForEach<CommandLineForEachTraits>(cmd,MemSingleArgument,ctx);
  if (ctx.maxSize)
  {
    sizeMBPhys = ctx.maxSize;
    sizeMBVirt = ctx.maxSize/4*3;
  }
  // TODO: allow more than 32b including the file cache
  saturate(sizeMBPhys,256,2047);
  saturate(sizeMBVirt,256,2047);
  ThrottleMemoryUsagePhysical = mem_size_t(sizeMBPhys)*1024*1024;
  // do not use more than 1 GB of virtual memory unless running on x64 with LARGEADDRESSAWARE
  size_t maxVirtual = 1024;
  #ifndef _XBOX
    if (memstat.ullTotalVirtual>3U*1024*1024*1024) maxVirtual = 2*1024;
  #endif
  ThrottleMemoryUsageVirtual = intMin(sizeMBVirt,maxVirtual)*mem_size_t(1024*1024);
  return sizeMBVirt;
#else
  return 512;
#endif
}

mem_size_t MemoryStoreUsed();


void FreeOnDemandGarbageCollectMain(size_t freeRequired, size_t freeSysRequired, bool isMain)
{
  #if MEM_PERF_LOG
    PROFILE_SCOPE_EX(gbCol,*);
  #endif
  #if 0
    bool watchGC = freeRequired>=4*1024*1024;
    if (watchGC) ReportGC("Before GC");
  #endif

  #if 0 // _ENABLE_REPORT
  static int lastNewFreeLeft = 0;
  // BMemory->NewFreeLeft is reducing as we are allocating, old value is expected to be larger
  LogF("New Heap allocated %d",lastNewFreeLeft-BMemory->NewFreeLeft());
  // lastNewFreeLeft will be updated after the releasing is finished
  #endif

  #if 0 //_ENABLE_REPORT
    // stress test system allocation - do not leave any system memory free
    freeSysRequired = 0;
  #endif
  #ifdef _XBOX
  // assume at least 2 MB should always be available in the low level heap
  freeRequired += 2*1024*1024;
  // never attempt do discard anything if there is a memory free
  // no need to keep within any other bounds
  #else
  {
    // first of all make sure we stay within desired limits
    mem_size_t overchargePhysical = Memory::TotalAllocated()+MemoryStoreUsed()-ThrottleMemoryUsagePhysical;
    mem_size_t overchargeVirtual = Memory::TotalAllocated()-ThrottleMemoryUsageVirtual;

    if (overchargePhysical>0 || overchargeVirtual>0)
    {
      const Memory::LockedFreeOnDemandEnumerator &demand = Memory::GetFreeOnDemandEnumerator();
      IMemoryFreeOnDemand *extras[32];
      int nExtras = 0;
      for (
        IMemoryFreeOnDemand *extra=demand.GetFirstFreeOnDemandLowLevel();
        extra;
        extra = demand.GetNextFreeOnDemandLowLevel(extra)
      )
      {
        if (nExtras>=32)
        {
          Fail("Too many low-level allocators");
          break;
        }
        extras[nExtras++] = extra;
      }
      
      extras[nExtras++] = ProgressGetFreeOnDemandInterface();
      if (overchargeVirtual>0)
      {
        mem_size_t released = FreeOnDemandMemory(overchargeVirtual,false,extras,nExtras);
        if (released==0)
        {
          LogF("Warning: No overcharged memory can be released");
        }
      }
      else if (overchargePhysical>0)
      {
        mem_size_t released = FreeOnDemandMemory(overchargePhysical,true,extras,nExtras);
        if (released==0)
        {
          LogF("Warning: No overcharged memory can be released");
        }
      }
    }
    mem_size_t overcommit = Memory::TotalCommitted()+MemoryStoreUsed()-ThrottleMemoryUsagePhysical;
    if (overcommit>0)
    {
      // TODO: on PC is has no sense to free memory which is not committed by our manager at all
      size_t released = Memory::FreeSystemMemory(overcommit);
      if (released==0)
      {
        //LogF("Warning: No overcommitted memory can be released");
      }
    }
  }
  #endif
  // we want the balancing to be called at least once to make sure low-level memory is not exhausted
  // however, we want to do this only once per frame, that is in the "isMain=true" call
  bool balanceAtLeastOnce = isMain;
  for(;;)
  {
#ifdef _XBOX
    // check how much memory is free now
    // with Xbox memory architecture free memory is:
    // system memory free
    // memory in the main heap which is committed but not allocated
    
    // if we have enough memory, we can break
    MEMORYSTATUS memstat;
    memstat.dwLength = sizeof(memstat);
    GlobalMemoryStatus(&memstat);
    size_t sysFree = memstat.dwAvailPhys;
    // on Xbox we control total memory usage
    // LL allocation failure handler would handle this, but this way we prevent calling it too often
    // frequent failure handling a retrying could be slow
    size_t heapFree = Memory::TotalCommitted()-Memory::TotalAllocated();
    size_t llFree = Memory::TotalLowLevelMemory();
    // sys memory can be used for both system and heap
    int toFree = freeRequired-(heapFree+sysFree+llFree);
#else
    // no regular balancing on PC, toFree has no sense here
    int toFree = 0;
#endif
    if (toFree>0)
    {
      // we need to free some main heap memory
      IMemoryFreeOnDemand *extras[1]= {ProgressGetFreeOnDemandInterface()};
      balanceAtLeastOnce = false;
      mem_size_t released = FreeOnDemandMemory(toFree,true,extras,1);
      if (released==0)
      {
        LogF("Warning: No more main heap memory can be released");
        break;
      }
    }
    else
    {
      #ifdef _XBOX
        if (sysFree>=freeSysRequired)
        {
          if (balanceAtLeastOnce)
          {
            IMemoryFreeOnDemand *extras[1]= {ProgressGetFreeOnDemandInterface()};
            // not only we do not request any memory to be free, moreover we may indicate some memory is free
            FreeOnDemandMemory(toFree,true,extras,1);
          }
          break;
        }
        size_t toFree = freeSysRequired-sysFree;
      #else
        if (Memory::CheckVirtualFree(16*1024*1024))
          break;

        #if _DEBUG || _PROFILE
          ReportMemoryStatus();
        #endif
        // if VirtualAlloc has failed, we need to free something
        // pretend we have some less memory than needed to guarantee this
        size_t toFree = 4*1024*1024;
        
      #endif

      // assume low-level memory can be seen as free for main heap operations,
      // but not for system heap
      
      IMemoryFreeOnDemand *extras[32];
      int nExtras = 0;
      const Memory::LockedFreeOnDemandEnumerator &demand = Memory::GetFreeOnDemandEnumerator();
      for (
        IMemoryFreeOnDemand *extra=demand.GetFirstFreeOnDemandLowLevel();
        extra;
        extra = demand.GetNextFreeOnDemandLowLevel(extra)
      )
      {
        if (nExtras>=32)
        {
          Fail("Too many low-level allocators");
          break;
        }
        extras[nExtras++] = extra;
      }
      
      extras[nExtras++] = ProgressGetFreeOnDemandInterface();
      balanceAtLeastOnce = false;
      mem_size_t released = FreeOnDemandMemory(toFree,false,extras,nExtras);
      if (released==0)
      {
        LogF("Warning: No more system memory can be released");
        ReportMemoryStatus();
        break;
      }
    }
  }
  #if 0
  if (watchGC) ReportGC("After GC");
  #endif
  #if 0 //_ENABLE_REPORT
  lastNewFreeLeft = BMemory->NewFreeLeft();
  #endif
}


size_t FreeOnDemandSystemMemoryAll()
{
  return GFreeOnDemandSystem.FreeAll();
}


/**
@param physical do we need to release a memory which is physical only (no virtual addresses used?)
*/

mem_size_t FreeOnDemandMemory(mem_size_t size, bool physical, IMemoryFreeOnDemand **extras, int nExtras)
{
  PROFILE_SCOPE_EX(gbMem,mem);
  // balanced releasing of all kinds of memory
  mem_size_t systemReleased;
  mem_size_t released = FreeOnDemandAnyMemory(size,physical,systemReleased,extras,nExtras);

  #if _ENABLE_REPORT
    if ((int)size>0 && released<=0)
    {
      // report how much memory is allocated in various containers
      Memory::ReportAnyMemory("No mem to release");
    }
  #endif
  return released;
}




#else

// Embed the appFrameExt
#if _MSC_VER && !defined INIT_SEG_COMPILER
// we want Memory Heap to deallocate last
#pragma warning(disable:4074)
#pragma init_seg(compiler)
#define INIT_SEG_COMPILER
#endif

//{ useAppFrameExt.cpp is embedded here:
//  - LogF is used in MemHeap construction, so it must be defined in compiler init_seg

#include "appFrameExt.hpp"
static OFPFrameFunctions GOFPFrameFunctions INIT_PRIORITY_URGENT;
AppFrameFunctions *CurrentAppFrameFunctions = &GOFPFrameFunctions;

//} End of embedded useAppFrameExt.cpp

#ifndef MemAllocDataStack
/// define a store for MemAllocDataStack
DataStack GDataStack(1*1024*1024);
#endif

// MT safe heap - equal to GMemFunctions or GSafeMemFunctions depending on configuration
#define MAIN_HEAP_MT_SAFE 1
#if MAIN_HEAP_MT_SAFE
MemFunctions *GMTMemFunctions = GMemFunctions; 
#else
MemFunctions *GMTMemFunctions = GSafeMemFunctions; 
#endif

int FrameId=0;

#endif //_win32

