// global redefinition of new, delete operators
#include "wpch.hpp"

#if defined _WIN32 && !defined MALLOC_WIN_TEST

#pragma optimize("t",on)
#if !_RELEASE
  // it seems in VS 2005 optimize("t",on) applies /Oy as well (Omit frame pointers)
  // in Testing we need stack frames for memory call-stack and crash-dumps
  #pragma optimize("y",off)
#endif

#include "memHeap.hpp"

#ifdef _MEMTABLE_HPP // depending on memHeap.hpp content enable or disable whole implementation
#include <Es/Common/win.h>
#include <Es/Files/commandLine.hpp>
#include <El/QStream/qbStream.hpp>
#include <El/Debugging/debugTrap.hpp>
#include <El/Common/perfLog.hpp>
#include <El/Common/perfProf.hpp>
#include <Es/Containers/staticArray.hpp>

#include "memGrow.hpp"

#ifdef _XBOX
  #include <io.h>
  #include <fcntl.h>
  #if _ENABLE_REPORT
  #include <XbDm.h>
  #endif
#endif
//#include "engineDll.hpp"

#include <Es/Memory/normalNew.hpp>

// note: MEM_CHECK makes sure leaks are detected, but if leak source tracking is needed
// you need to use ALLOC_DEBUGGER

#if _PROFILE
  #define MEM_CHECK 0
  #define DO_MEM_STATS 0
  #define DO_MEM_FILL 0
#elif _DEBUG
  #define MEM_CHECK 1
  #define DO_MEM_STATS 0
  #define DO_MEM_FILL 0
#elif _RELEASE
  #define MEM_CHECK 0
  #define DO_MEM_STATS 0
  #if _ENABLE_REPORT
    #define DO_MEM_FILL 1
  #else
    #define DO_MEM_FILL 0
  #endif
#else
  // testing
  #define DO_MEM_FILL 0
  #define MEM_CHECK 1
  #define DO_MEM_STATS 1
#endif


// when using byte, we have little to choose from
#define MEM_GUARD_VAL 0x6a
#define MEM_FREE_VAL 0x7f
#define MEM_NEW_VAL 0x71

// such value is big positive as int, and at the same time it is float signalling NaN
#define MEM_NEW_VAL_32 0x7f8f7f8f
#define MEM_FREE_VAL_32 0x7f817f81


/// alternative to memset, filling memory with a 32b value aligned to 32b
static __forceinline void MemSet32(void *dst, int val32, int size)
{
  // assume memory is 32b aligned
  // if not, fill less then required
  int *d = (int *)dst;
  size>>=2;
  while (--size>=0) *d++ = val32;
}


#if _MSC_VER && !defined INIT_SEG_COMPILER
  // we want Memory Heap to deallocate last
  #pragma warning(disable:4074)
  #pragma init_seg(compiler)
  #define INIT_SEG_COMPILER
#endif

/// should the primary memory heap should be multi-thread safe?
#define MAIN_HEAP_MT_SAFE 1

//{ useAppFrameExt.cpp is embedded here:
//  - LogF is used in MemHeap construction, so it must be defined in compiler init_seg

#include "appFrameExt.hpp"
static OFPFrameFunctions GOFPFrameFunctions INIT_PRIORITY_URGENT;
AppFrameFunctions *CurrentAppFrameFunctions = &GOFPFrameFunctions;

//} End of embedded useAppFrameExt.cpp

#ifndef MemAllocDataStack
  /// define a store for MemAllocDataStack
  DataStack GDataStack(1*1024*1024);
#endif


#ifndef MFC_NEW

#if MEM_CHECK || DO_MEM_STATS
#include "memCheck.hpp"
#endif


#include <El/Debugging/imexhnd.h>


#if defined(_WIN32) && _ENABLE_REPORT && !MAIN_HEAP_MT_SAFE

static DWORD UnsafeHeapThread=GetCurrentThreadId();

static void MFSafetyBroken()
{
  Fail("MT safety broken");

#if _ENABLE_REPORT && !defined _XBOX
  CONTEXT context;
  context.ContextFlags = CONTEXT_FULL;
  RtlCaptureContext(&context);
  GDebugExceptionTrap.ReportContext("MT SAFETY BROKEN", &context, true);
  TerminateProcess(GetCurrentProcess(), 1);    
#endif
}
#define AssertThread() \
  if(GetCurrentThreadId()!=UnsafeHeapThread && UnsafeHeapThread) MFSafetyBroken()

#else

#define AssertThread()

#endif



#if DO_MEM_STATS
  // check new call point histogram
  #include <El/Statistics/statistics.hpp>
  #include "keyInput.hpp"
  #include "dikCodes.h"

  //! allocation count statistics
  #if USE_MEM_COUNT
    static StatisticsByName MemCntStats INIT_PRIORITY_URGENT;
  #endif

  #if USE_MEM_FILENAME
    //! file based statistics
    static StatisticsByName MemTotStats INIT_PRIORITY_URGENT;
  #else if USE_MEM_CALLSTACK_CP
    static StatisticsById MemTotStats INIT_PRIORITY_URGENT;
  #endif
  #if USE_MEM_CALLSTACK_CS
    //! call stack based statistics
    static StatisticsByName MemCSStats INIT_PRIORITY_URGENT;
  #endif
  #if USE_MEM_CALLSTACK_CP
    //! call stack based statistics
    static StatisticsById MemCPStats INIT_PRIORITY_URGENT;
  #endif
  #if USE_MEM_CALLSTACK_CL
    //! call stack based statistics
    static StatisticsById MemCLStats INIT_PRIORITY_URGENT;
  #endif

  //! avoid recursion
  static int MemStatsDisabled=0;

  void ReportMemory();
  void ReportMemoryTotals();

  void MemoryFootprintSummary()
  {
    //MemTotStats.Clear();
    // scan all allocated blocks
    for
    (
      MemoryInfo *info=PAllocated->First(); info; info=PAllocated->Next(info)
    )
    {
      #if USE_MEM_FILENAME
        char buf[128];
        _snprintf(buf,sizeof(buf),"%s(%d): S",info->File(),info->Line());
        buf[sizeof(buf)-1]=0;
        MemTotStats.Count(buf,info->Size());
      #else if USE_MEM_CALLSTACK_CP
        if (info->CallstackSize()>0)
        {
          void *callplace = info->Callstack()[0];
          void *caller = 0;
          MemTotStats.Count(
            StatisticsById::Id((int)callplace,(int)caller),
            info->Size()
          );
        }
      #endif
      #if USE_MEM_CALLSTACK_CS
        MemCSStats.Count(info->Callstack(),info->CallstackSize()*sizeof (void *),info->Size());
      #endif
      #if USE_MEM_CALLSTACK_CP
        for (int i=0; i<info->CallstackSize(); i++)
        {
          void *callplace = info->Callstack()[i];
          void *caller = i<info->CallstackSize()-1 ? info->Callstack()[i+1] : 0;
          //void *caller = 0;
          MemCPStats.Count(
            StatisticsById::Id((int)callplace,(int)caller),
            info->Size()
          );
        }
      #endif
      #if USE_MEM_CALLSTACK_CL
        for (int i=0; i<info->CallstackSize(); i++)
        {
          void *callplace = info->Callstack()[i];
          void *caller = 0;
          MemCLStats.Count(
            StatisticsById::Id((int)callplace,(int)caller),
            info->Size()
          );
        }
      #endif
    }
    #if USE_MEM_FILENAME || USE_MEM_CALLSTACK_CP
      MemTotStats.Sample();
    #endif
    #if USE_MEM_CALLSTACK_CS
      MemCSStats.Sample();
    #endif
    #if USE_MEM_CALLSTACK_CP
      MemCPStats.Sample();
    #endif
    #if USE_MEM_CALLSTACK_CL
      MemCLStats.Sample();
    #endif
    ReportMemory();
  }
  void MemoryFootprint()
  {
    MemoryFootprintSummary();
    ReportMemoryTotals();
  }

  void CountNew( const char *file, int line, int size )
  {
    #if USE_MEM_COUNT
      if( MemStatsDisabled>0 ) return;
      MemStatsDisabled++;
      char buf[128];
      _snprintf(buf,sizeof(buf),"%s(%d): C",file,line);
      buf[sizeof(buf)-1]=0;
      MemCntStats.Count(buf);
      // scan all allocated blocks and add them to memory footprint
      MemStatsDisabled--;
    #endif
  }

  
  #if USE_MEM_CALLSTACK_CS
  void MemIdCallbackCS(const void *id, int idSize, int count)
  {
    // convert call stack to text 
    // report only big allocations
    void * const *ids = (void * const *)id;
    int idsSize = idSize/sizeof(void *);
    for (int i=0; i<idsSize; i++)
    {
      void *idcs = ids[i];
      // translate id to text representation
      const char *txt = GMapFile.MapNameFromPhysical((int)idcs);
      LogF("%32s: CS %6d",txt,count);
    }
    //sprintf(buf,"%32s: %6d\r\n",_data[i].name,_data[i].count/_factor);
    //f.write(buf,strlen(buf));
  }
  #endif

  #if USE_MEM_CALLSTACK_CP || USE_MEM_CALLSTACK_CL
  void MemIdCallbackCP(StatisticsById::Id id, int count)
  {
    // convert call stack to text 
    // report only big allocations
    char callPair[512];
    int id1Start;
    const char *name1 = GMapFile.MapNameFromPhysical(id._id1,&id1Start);
    sprintf(callPair,"%X:%s",id._id1,name1);
    if (id._id2)
    {
      int id2Start;
      const char *name2 = GMapFile.MapNameFromPhysical(id._id2,&id2Start);
      strcat(callPair," <- ");
      sprintf(callPair+strlen(callPair),"%X:%s",id._id2,name2);
    }
    LogF("%90s: CP %6d",callPair,count);
    //sprintf(buf,"%32s: %6d\r\n",_data[i].name,_data[i].count/_factor);
    //f.write(buf,strlen(buf));
  }
  #endif

  
  void ReportMemory()
  {
    MemStatsDisabled++;

    #if USE_MEM_FILENAME
      LogF("Allocation footprint report (%d samples)",MemTotStats.NSamples());
      LogF("-----------------------");
      MemTotStats.Report(64*1024);
      MemTotStats.ReportTotal();
    #else if USE_MEM_CALLSTACK_CP
      LogF("Allocation footprint report (%d samples)",MemTotStats.NSamples());
      LogF("-----------------------");
      MemTotStats.Report(MemIdCallbackCP,64*1024);
      MemTotStats.ReportTotal();
    #endif
    #if USE_MEM_CALLSTACK_CS
      MemCSStats.Report(MemIdCallbackCS,128*1024);
    #endif
    #if USE_MEM_CALLSTACK_CP
      LogF("Allocation call-pair report (%d samples)",MemCPStats.NSamples());
      LogF("-----------------------");
      MemCPStats.Report(MemIdCallbackCP,128*1024);
    #endif
    #if USE_MEM_CALLSTACK_CL
      LogF("Allocation call-place report (%d samples)",MemCLStats.NSamples());
      LogF("-----------------------");
      MemCLStats.Report(MemIdCallbackCP,128*1024);
    #endif
    #if USE_MEM_COUNT
      LogF("Allocation count report");
      LogF("-----------------------");
      MemCntStats.Report(100);
    #endif
    
    #if USE_MEM_FILENAME
      MemTotStats.Clear();
    #endif
    #if USE_MEM_CALLSTACK_CS
      MemCSStats.Clear();
    #endif
    #if USE_MEM_CALLSTACK_CP
      MemCPStats.Clear();
    #endif
    #if USE_MEM_CALLSTACK_CL
      MemCLStats.Clear();
    #endif
    #if USE_MEM_COUNT
      MemCntStats.Clear();
    #endif

    MemStatsDisabled--;
  }

#else
  void ReportMemoryTotals();
  void MemoryFootprint()
  {
    #if USE_MEM_CALLSTACK_CP
    ReportAllocated();
    #endif
    ReportMemoryTotals();
  }
  #define ReportMemory() GMemFunctions->ReportMemory(){}

  #define CountNew(file,line,size)
#endif

#if _ENABLE_REPORT
  #if MAIN_HEAP_MT_SAFE
  volatile LONG TotalAllocatedByActualAlloc;
  #else
  size_t TotalAllocatedByActualAlloc;
  #endif
#endif

static bool MemoryErrorState = false;

void MemoryErrorReported()
{
  MemoryErrorState = true;
}


// manage memory pool
// memory pool is reserved when application is started
// and committed as necessary

// maybe we could maintain order of free blocks
// by address (this will speed-up Free) (est. 2x)
// by size (this will speed-up Alloc) (est. 2x)

int MemoryFreeBlocks();

class MemHeap;
class MemHeapLocked;

#if MAIN_HEAP_MT_SAFE
  #define MemHeapType MemHeapLocked
#else
  #define MemHeapType MemHeap
#endif


//extern MemHeap *SMemPtr;
extern MemHeapType *BMemPtr;


typedef FreeBlock *HeaderType;

//////////////////////////////////////////////////////////

InfoAlloc::InfoAlloc(size_t n)
:esize( n<sizeof(Link*) ? sizeof(Link*) : n ),
head(0),
chunks(NULL)
{
  //#ifdef _KNI
  //const int alignMem=16;
  //#else
  const int alignMem=esize>=16 ? 16 : 8;
  //#endif
  esize=(esize+alignMem-1)&~(alignMem-1);
}

InfoAlloc::~InfoAlloc()
{
  // check if all chunks may be freed now
}

#if _ENABLE_REPORT
  #define IA_DIAGS 1
#endif

#define NO_ALLOCATION_AFTER_ERROR 0

InfoAlloc::Chunk *InfoAlloc::NewChunk(MemSharedState &shared, MemCommit &commit)
{
  //LogF("Allocated Info blocks: %d",allocated);
  #if NO_ALLOCATION_AFTER_ERROR
  if (MemoryErrorState)
  {
    return NULL;
  }
  #endif
  Retry:
  int pageSize = commit.GetPageSize();
  void *offset = commit.FindFreeRange(pageSize,pageSize);
  if (!offset)
  {
    // even if there is no free range, there may still be opportunity to grow the commit store
    // adapted from MemHeap::AllocRegion

    // first of all check if we can extend the allocated range
    if (commit.CanReserve())
    {
      // we can extend significantly, let us do it
      int allocSize = intMin(shared._reserveGranularity,shared._leftToReserve);
      const MemReservedInfo &info = commit.Reserve(allocSize,pageSize,allocSize);
      if (info._reserved>=(int)pageSize)
      {
        // we have reserved enough, we will use it
        offset = commit.FindFreeRange(pageSize,pageSize);
      }
    }

    if (!offset) 
    {
      if (FreeOnDemandLowLevelMemory(pageSize)) goto Retry;
      return NULL;
    }
  }
  if (!commit.CommitRange(offset,(char *)offset+pageSize))
  {
    if (FreeOnDemandLowLevelMemory(pageSize)) goto Retry;
    // fatal error
    Fail("Fatal error - out of memory");
    return NULL;
  }

  Chunk *mem = (Chunk *)offset;
  ConstructAt(*(Chunk *)mem);
  return mem;
}

void InfoAlloc::DeleteChunk( MemCommit &commit, Chunk *chunk )
{
  chunk->~Chunk();
  int pageSize = commit.GetPageSize();
  commit.UnlockRange((char *)chunk,((char *)chunk)+pageSize);
}

void* InfoAlloc::Alloc( MemSharedState &shared, MemCommit &commit, size_t n )
{

  Assert( n<=esize );
  if( head==0 )
  {
    if (!Grow(shared,commit))
    {
      LogF("Warning: InfoAlloc::Alloc failed");
      return NULL;
    }
  }
  Link *p=head;

  head=p->next;

  // mark chunk to link
  return p;
}

void InfoAlloc::Free( MemCommit &commit, void *pAlloc )
{
  Link *p=static_cast<Link*>(pAlloc);
  //p--; // link is skipped
  p->next=head;
  head=p;

  // check if all chunks may be freed now
}

void MemoryErrorPage(size_t size);

bool InfoAlloc::Grow(MemSharedState &shared, MemCommit &commit)
{
  //Chunk *n=new Chunk; // use global new
  //Chunk *n=(Chunk *)ChunkHeap.Alloc(sizeof(Chunk)); // use chunk new
  Chunk *n=NewChunk(shared,commit); // use chunk new
  if (!n)
  {
    Fail("InfoAlloc::Grow failed");
    MemoryErrorPage(sizeof(Chunk));
    //MemoryErrorMalloc(sizeof(Chunk));
    //MemoryError(sizeof(Chunk));
    return false;
  }

  n->next=chunks;
  chunks=n;

  char *start=n->mem;

  int pageSize = commit.GetPageSize();
  int chSize = pageSize-2*sizeof(void *);
  const int nelem=chSize/esize;

  char *last=&start[(nelem-1)*esize];
  for( char*p = start; p<last; p+=esize )
  {
    Link *link = reinterpret_cast<Link*>(p);

    link->next=reinterpret_cast<Link*>(p+esize);
    link->chunk=n;
  }
  Link *lastLink = reinterpret_cast<Link*>(last);
  lastLink->next=0;
  lastLink->chunk=n;
  head=reinterpret_cast<Link*>(start);

  return true;
}

size_t InfoAlloc::TotalAllocated(const MemCommit &commit) const
{
  size_t size=0;
  // remove all chunks
  Chunk *n=chunks;
  while( n )
  {
    size+=commit.GetPageSize();
    n=n->next;
  }
  return size;
}


void InfoAlloc::FreeChunks(MemCommit &commit)
{

  #if IA_DIAGS
    int size=0;
  #endif
  // remove all chunks
  Chunk *n=chunks;
  while( n )
  {
    Chunk *p=n;
    n=n->next;
    // TODO: perform smart DeleteChunk when chunk is empty
    DeleteChunk(commit,p); // use chunk delete
    #if IA_DIAGS
      size+=commit.GetPageSize();
    #endif
  }
  chunks=NULL;
  head=NULL;

  #if IA_DIAGS
    LogF("InfoAlloc chunks - %d KB (%d)",(size+1023)/1024,esize);
  #endif

}



#if 0 // _ENABLE_REPORT 

  #define VerifyStructure() DoAssert( CheckIntegrity() )

#else

  #define VerifyStructure()

#endif

// Chunk::new is now directed to region memory, and following is no longer true:
// memory allocated by smallAlloc would appear in statistics twice
// because Alloc calls Chunk::new to allocate Chunks
#if MEM_CHECK 
// for small alloc leak debugging use SMALL_ALLOC 0
#define SMALL_ALLOC 1
#else
#define SMALL_ALLOC 1
#endif

// find corresponding free list


void MemHeap::DoConstruct(MemHeap **lateDestructPointer)
{
  InitSmallAllocs();
  _name = "No";
  _lateDestruct = lateDestructPointer;
}

#include <Es/Memory/normalNew.hpp>

/*!
\patch 5142 Date 3/20/2007 by Ondra
- Fixed: Reduced virtual address space usage on computers with 1 GB or more of RAM.
*/


void MemHeap::DoConstruct
(
  const char *name, MemSize size, MemSize align,
  MemHeap **lateDestructPointer
)
{
  const MemSize oneMB = 1024*1024;
  LogF("MemHeap %s constructed (%d)",name,size);
  _name = name;

  _destruct=false;

  _align=align;
  size=AlignSize(size);
  _minAlloc=AlignSize(_minAlloc);
  // pool must be aligned
  MemSize minSize = size;
  if (minSize>256*oneMB) minSize = 256*oneMB;
  // TODO: even _memBlocks can be reserved in smaller blocks
  // if size is very large, it is very likely most of it will be in the paged pool anyway
  MemSize blocksSize = size;
  const MemSize basicSize = 128*oneMB; 
  //const MemSize basicSize = 256*oneMB; 
  if (size>basicSize)
  {
    // TODO: for a case of the primary heap continuous area is needed - enlarge it
    blocksSize = (size-basicSize)/4+basicSize;
    //blocksSize = (size-basicSize)/2+basicSize;
  }
  // experience shows blocks do not contain that much data anyway
  // we want the memory to stay available for regions as well
  if (blocksSize>512*oneMB) blocksSize = 512*oneMB;
  if (minSize>blocksSize) minSize = blocksSize;
  
  _shared._leftToReserve = size;
  const MemReservedInfo &reserved = _memBlocks.ReserveWithMult(minSize,blocksSize,_vspaceMultiplier);
  // TODO: dynamic adding blocks as long as they are needed
  //totalLeft -= reserved._reserved;
  int allocSize = intMin(128*oneMB,_shared._leftToReserve);
  const MemReservedInfo &info = _memRegions.ReserveWithMult(allocSize/2,allocSize,1);
  // report failure
  if (info._reserved<allocSize)
  {
    RptF("Heap init failed, %d wanted, %d allocated",allocSize,allocSize-info._reserved);
  }
  _shared._leftToReserve -= info._reserved;
  _shared._reserveGranularity = 64*oneMB;
  
  _allocatedDirect = 0;
  
  InitSmallAllocs();

  //int sizeFree=_memBlocks.Size();

  LogF(
    "Reserved %d B + %d B (%d MB), Minimal allocation size %d B",
    _memBlocks.GetReserved(),_shared._leftToReserve+_memBlocks.GetReserved(),
    (_memBlocks.GetReserved(),_shared._leftToReserve+_memBlocks.GetReserved()+oneMB-1)/oneMB,

    _minAlloc
  );
  
  // we need to make sure all allocated memory will be aligned as required by _align
  // each memory block has a header of size sizeof(HeaderType)
  // therefore we need the startFree+sizeof(HeaderType) to be aligned
  // reserved._data is most likely aligned, as it is aligned to a page size, however this is not relevant here
  Assert(_align>0);
  // skip any pages which are busy (may be used for headers of MemReservedInfo)
  void *resMem = _memBlocks.FindFreeRange(1,1);
  char *startFree= (char *)(((size_t)resMem + sizeof(HeaderType) + _align-1)&~(_align-1))-sizeof(HeaderType);
  int alignOverhead = startFree-(char *)reserved._data;

  // setup the free list (one large block) and busy list (empty)  
  FreeBlock *allFree = new(_infoAlloc,_shared,_memRegions) FreeBlock;
  allFree->_memory = startFree;
  
  _freeList.Insert(allFree);
  _blockList.Insert(allFree);

  _nItemsFree = 1;
  _freeSize = reserved._reserved-alignOverhead;
  _lateDestruct = lateDestructPointer;
}

void MemHeap::DoDestruct()
{
  DestructSmallAllocs();

  LogF("Destruct memory heap %s",_name);
  // all memory should be deallocated by now

  _blockList.Clear();
  // clear all free lists
  _freeList.Clear();
  //_freeList256.Clear();
  _memBlocks.Clear();
  _infoAlloc.Destruct(_memRegions);
  //_memTable.Clear();
  _nItemsFree=0;
  _freeSize=0;
  if (_lateDestruct)
  {
    *_lateDestruct = NULL; // nothing to diagnose any more - memory is gone
    Log("**** All memory released - memory checker deinitialized *****");
  }
}

bool MemHeap::CheckIntegrity() const
{
  #if 1
    static int seldom = 0;
    if (seldom++<50) return true;
    seldom = 0;
  #endif
  if (!Check())
  {
    return false;
  }
  return true;
//  size_t freeLeft = _freeList.TotalFreeLeft(this);
//  if (freeLeft!=_freeSize)
//  {
//    LogF("freeLeft!=_freeSize : %x!=%x",freeLeft,_freeSize);
//    return false;
//  }
//  bool ret = true;
//  // verify all allocated blocks are PageLocked
//  for( BusyBlock *block=_blockList.First(); block; block=_blockList.Next(block) )
//  {
//    int size = BlockSize(block);
//    int start = block->_memory-(char *)_memBlocks.Data();
//    if (block->IsFree())
//    {
//      bool ok = _memBlocks.CheckUnlockedRange((int)start,(int)start+size);
//      if (!ok) ret = false;
//    }
//    else
//    {
//      bool ok = _memBlocks.CheckLockedRange((int)start,(int)start+size);
//      if (!ok) ret = false;
//    }
//  }
//  return ret;
}

size_t MemHeap::FreeSystemMemory(size_t size)
{
  size_t decommited = 0;
  {
    int pageSize = _memRegions.GetPageSize();
    int nPages = (size+pageSize-1)/pageSize;
    decommited += _memRegions.DecommitPages(nPages)*pageSize;
  }
  if (decommited<size)
  {
    int pageSize = _memBlocks.GetPageSize();
    int nPages = (size+pageSize-1)/pageSize;
    decommited += _memBlocks.DecommitPages(nPages)*pageSize;
  }
  return decommited;
}

size_t MemHeap::TotalLowLevelMemory() const
{
  size_t total = 0;
  for(IMemoryFreeOnDemand *walk=GetFirstFreeOnDemandLowLevel(); walk; walk = GetNextFreeOnDemandLowLevel(walk))
  {
    total += walk->MemoryControlled();
  }
  return total;
}

#if _ENABLE_REPORT

#include <El/Statistics/statistics.hpp>

void MemHeap::LogAllocStats() const
{
  StatisticsByName stats;
  for( BusyBlock *block=_blockList.First(); block; block=_blockList.Next(block) )
  {
    if (block->IsFree()) continue;
    int size= BlockSize(block);

    char name[256];
    sprintf(name,"%08d",size);
    stats.Count(name,1);
  }
  stats.Report();

}

#endif

//#pragma optimize ("a",off)

// Alloc and Free are used only within operator new/delete
// inlining avoids using 1:1 call

#if _RELEASE && _ENABLE_PERFLOG
  #define MEM_PERF_LOG 1
#endif

#if MEM_PERF_LOG
  #include <El/Common/perfLog.hpp>
  #include <El/Common/perfProf.hpp>
#endif

MemHeap::~MemHeap()
{
  DoDestruct();
}

void MemHeap::BeginUse(size_t minUsage)
{
  _memBlocks.SetMinCommit(minUsage);
  // we can GC the other heap
}
void MemHeap::EndUse()
{
  _memBlocks.SetMinCommit(0);
} 

FreeBlock *MemHeap::FindBest( MemSize size, MemSize align )
{
  FreeBlock *best=NULL;
  // note: a nice trick to save one conditional jump
  //if( diff>=0 && diff<bestDiff )
  //if( diff+INT_MIN>=INT_MIN && diff+INT_MIN<bestDiff+INT_MIN )
  // same as above, unroll loop by 4
  //size+=(MemSize)INT_MIN;
  int bestDiff = INT_MAX;
  //for( FreeBlock *free=_freeList.First(); free; free=_freeList.Next(free) )
  for
  (
    FreeBlock *free=_freeList.Start();
    NotEndOf(FreeLink,free,_freeList);
    free=_freeList.Advance(free)
  )
  {
    // check how much memory will be unused from this block when allocating 
    MemSize freeSize=BlockSize(free);
    // if the block is too small. we can quickly reject it
    if (freeSize<size) continue;
    int freeMem = (int)free->_memory;
    int freeMemAligned = (freeMem+align-1)&~(align-1);
    int alignOffset = freeMemAligned-freeMem;
    if (freeSize<size+alignOffset) continue; // block will not match
    int diff=freeSize-size; // check how much will be rest
    if (diff<bestDiff) bestDiff=diff,best=free; //,bestPrev=prev;
  }
  return best;
}

FreeBlock *MemHeap::FindBest( MemSize size )
{
  FreeBlock *best=NULL;
  // note: a nice trick to save one conditional jump
  //if( diff>=0 && diff<bestDiff )
  //if( diff+INT_MIN>=INT_MIN && diff+INT_MIN<bestDiff+INT_MIN )
  // same as above, unroll loop by 4
  size+=(MemSize)INT_MIN;
  int bestDiff=INT_MAX+INT_MIN;
  //for( FreeBlock *free=_freeList.First(); free; free=_freeList.Next(free) )
  for
  (
    FreeBlock *free=_freeList.Start();
    //freeList.NotEnd(free);
    NotEndOf(FreeLink,free,_freeList);
    free=_freeList.Advance(free)
  )
  {
    int freeSize=BlockSize(free);
    int diff=freeSize-size; // GetFreeSize
    if( diff<bestDiff ) bestDiff=diff,best=free; //,bestPrev=prev;
  }
  return best;
}

#if SMALL_ALLOC



class SmallMemAlloc: public FastAlloc
{
  MemHeap *_heap;

  public:
  SmallMemAlloc( MemHeap *heap, size_t n, int pageSize );
  ~SmallMemAlloc();
  void ReleaseAll();

  void *operator new( size_t size ) {return malloc(size);}
  void operator delete( void *mem ) {free(mem);}

  virtual Chunk *NewChunk();
  virtual void DeleteChunk( Chunk *chunk );
};

SmallMemAlloc::SmallMemAlloc( MemHeap *heap, size_t n, int pageSize )
:FastAlloc
(
  #if USE_CHUNK_POINTERS
  n,"mem",12,pageSize
  #else
  n,"mem",0,pageSize
  #endif
),
_heap(heap)
{
}

void SmallMemAlloc::ReleaseAll()
{
#if CHECK_HEAP_THREAD
  AssertThread();
#endif
  // default implementation calls non-virtual implementation of DeleteChunk
  // this implementation looks identical, but appropriate DeleteChunk is called
  if( --allocated==0 )
  {
    FreeChunks();
    Assert(!chunksBusy.First());
    Assert(!chunksPart.First());
    Assert(!chunksFree.First());
  }
  else
  {
    LogF("FastAlloc %s not free when destructed.",Name());
  }
}

#if ALLOC_DEBUGGER

static void ReportBlock(int *block)
{
	int magic=block[1];
	Assert( magic==15879634 );
	if( magic!=15879634 ) return;
	MemoryInfo *info=(MemoryInfo *)block[0];
	if (info)
	{
	  info->Report();
	}
}
#endif

SmallMemAlloc::~SmallMemAlloc()
{
  if (allocated!=0)
  {
    Fail("SmallMemAlloc not free when destructed - SmallMemAlloc::ReleaseAll skipped.");
    RptF("  Elem. size %d",ItemSize());
    // traverse chunks, check pointers to the unreleased memory
    
    //Assert(chunksPart.First()==NULL);
    for (Chunk *n=chunksPart.First(); n; n=chunksPart.Next(n))
    {
      // not all items busy, traverse and check
      char *mem = n->mem+_alignOffset;
      for (int i=0; i<_nElemPerChunk; i++,mem += ItemSize())
      {
        // check if we can find in the free list (started by head)
        bool found = false;
        for (Link *item=n->head; item; item=item->next)
        {
          Assert(item->chunk==n);
          if ((char *)item==mem)
          {
            found = true;
            break;
          }
        }
        if (!found)
        {
          // if there is a memory checker, we may extract the info now
          #if ALLOC_DEBUGGER
            ReportBlock((int *)mem);
          #endif
        }
      }
    }

    for (Chunk *n=chunksBusy.First(); n; n=chunksBusy.Next(n))
    {
      // all items are busy here
      // we may want to traverse them
      char *mem = n->mem+_alignOffset;
      for (int i=0; i<_nElemPerChunk; i++,mem += ItemSize())
      {
        // if there is a memory checker, we may extract the info now
        #if ALLOC_DEBUGGER
          ReportBlock((int *)mem);
        #endif
      }
    }
  }
}

SmallMemAlloc::Chunk *SmallMemAlloc::NewChunk()
{
#if CHECK_HEAP_THREAD
  AssertThread();
#endif
  Chunk *ch = (Chunk *)_heap->AllocRegion(_chunkSize,_chunkSize);
  ConstructAt(*ch);
  return ch;
}

void SmallMemAlloc::DeleteChunk( Chunk *chunk )
{
#if CHECK_HEAP_THREAD
  AssertThread();
#endif
  chunk->~Chunk();
  _heap->FreeRegion(chunk,_chunkSize);
}

void MemHeap::InitSmallAllocs()
{
  for( int i=0; i<nAllocSizes; i++ )
  {
    _smallAllocs[i]=new SmallMemAlloc(this,allocSizes[i],_memRegions.GetPageSize());
  }
  
  for( int i=0; i<lenof(_allocStats); i++ ) _allocStats[i]=0;

  for( int i=0; i<smallAllocMax/smallAllocGranularity; i++ )
  {
    _smallAllocSelect[i] = NULL;
    // find which index corresponds to this size
    int size = i*smallAllocGranularity;
    for (int j=0; j<nAllocSizes; j++)
    {
      if (allocSizes[j]>=size)
      {
        _smallAllocSelect[i] = _smallAllocs[j];
        break;
      }
    }
  }
}

void MemHeap::DestructSmallAllocs()
{
  for( int i=0; i<smallAllocMax/smallAllocGranularity; i++ )
  {
    _smallAllocSelect[i] = NULL;
  }
  for( int i=0; i<nAllocSizes; i++ )
  {
    _smallAllocs[i]->ReleaseAll();
    _smallAllocs[i]->~SmallMemAlloc();
  }
}

#else

void MemHeap::InitSmallAllocs() {}

void MemHeap::DestructSmallAllocs() {}

#endif

#if SMALL_ALLOC
const int DefaultMinAlloc = bigAllocGranularity;
#else
const int DefaultMinAlloc = 32;
#endif

#if defined USE_MALLOC && USE_MALLOC
#error malloc allocation is used - using MemTable makes no sense.
#endif

MemHeap::MemHeap
(
  const char *name, MemSize size, MemSize align,
  MemHeap **lateDestructPointer
)
:_memBlocks(_shared),_memRegions(_shared),_infoAlloc(sizeof(BusyBlock))
{
  _minAlloc=DefaultMinAlloc;
  _vspaceMultiplier = 3;
  DoConstruct(name,size,align,lateDestructPointer);
}
MemHeap::MemHeap(MemHeap **lateDestructPointer)
:_memBlocks(_shared),_memRegions(_shared),_infoAlloc(sizeof(BusyBlock))
{
  _align=DefaultAlign;
  _minAlloc=DefaultMinAlloc;
  _vspaceMultiplier = 3;
  DoConstruct(lateDestructPointer);
}

int MemHeap::SelectSmallAlloc(MemSize size)
{
  if (size>=smallAllocMax)
  {
    return smallAllocMax/smallAllocGranularity;
  }

  return (size+smallAllocGranularity-1)/smallAllocGranularity;
}

BusyBlock *MemHeap::AllocateBlock(FreeBlock *best, MemSize size)
{
  #if MEM_PERF_LOG
    ADD_COUNTER(mCnt,1);
    ADD_COUNTER(mSize,size);
  #endif
  // find best match
  int bestSize=BlockSize(best);

  // block _memory should be valid
  char *startOffset = best->_memory;
  bool ok = false;
  do
  {
    // we should be careful when allocating in the "biggest free" block
    ok = _memBlocks.CommitRange(startOffset,(char *)startOffset+size);
    if (ok) break;
  } while (_memRegions.DecommitOnePage());
  if (!ok)
  {
    // if we cannot commit
    return NULL;
  }
  // range is locked during commit operation
  //_memBlocks.LockRange(startOffset,startOffset+size);

  // there must be at least busy block + _align left
  const MemSize minLeft=_minAlloc;
  //const int minLeft=32; // less efficient, less fragmentation
  if( bestSize-size<=minLeft )
  {
    // exact or nearly exact match, do not split - use whole item
    // delete best from the free list
    _freeList.Delete(best);
    //Log("Allocated %d B (%x)",size,(char *)busy);
    _nItemsFree--;
    
    // we used whole free block
    _freeSize -= bestSize;

    VerifyStructure();
  }
  else
  {
    // split item into two parts
    FreeBlock *free=new(_infoAlloc,_shared,_memRegions) FreeBlock;
    if (!free) return NULL;
    free->_memory=best->_memory+size;

    _freeList.Delete(best);

    _blockList.InsertAfter(best,free);
    _freeList.Insert(free); // replace best block with free block
    //_nItemsFree count not changed
    
    // size of free block was changed
    _freeSize -= size;

    VerifyStructure();
  }

  AddRef(); // increase RefCount - disable deallocation

  //Log("Allocated %x: %06d",ret,size);
  return best;
}

#if 1

static int AllocCounter = 0;
static int FreeCounter = 0;
static int BusyBlockCounter = 0;

void CountAlloc()
{
  AllocCounter++;
  BusyBlockCounter++;
}
void CountFree()
{
  FreeCounter++;
  BusyBlockCounter--;
}

#else

#define CountAlloc()
#define CountFree()

#endif

//const static int DirectThreshold = 8*1024*1024;
/// allocation size limit: for allocations over this limit VirtualAlloc is used directly
/** 
With 8 MB limit result was around 24 MB allocated directly when playing on Chernarus
With 1 MB limit even 2K x 2K texture was allocated this way.
*/
const static int DirectThreshold = 4*1024*1024;

void *MemHeap::Alloc( MemSize size, int aligned )
{
  CountAlloc();
  
  VerifyStructure();

  #if MEM_PERF_LOG
  PROFILE_SCOPE_EX(memAl,mem);
  #endif

  #if 0 //_ENABLE_REPORT
  if( size==0 )
  {
    LogF("Zero-sized allocation");
    //return NULL;
  }
  #endif
  //if( size>=32*1024*1024 )
  if( size>=256*1024*1024 )
  {
    ErrF("Bad memory allocation, %d B",size);
    return NULL;
    //for(;;){}
  }
  if (size>=DirectThreshold)
  {
    // very large allocations are handled separately, using VirtualAlloc
    void *mem = VirtualAlloc(NULL,size,MEM_COMMIT | MEM_RESERVE,PAGE_READWRITE);
    #if DO_MEM_FILL
      MemSet32(mem,MEM_NEW_VAL_32,size);
    #endif
    MEMORY_BASIC_INFORMATION info;
    SIZE_T ret = VirtualQuery(mem,&info,sizeof(info));
    DoAssert(ret==sizeof(info));
    _allocatedDirect += info.RegionSize;
    return mem;
  }
  #if SMALL_ALLOC
  int allocIndex = SelectSmallAlloc(size);

  // count even allocations for which no small allocator exists
  // we want this data to decide which small allocators should be created
  _allocStats[allocIndex]++;
  if( allocIndex<lenof(_smallAllocSelect))
  {
    SmallMemAlloc *alloc=_smallAllocSelect[allocIndex];
    if (alloc)
    {
      DoAssert(alloc->ItemSize()>=size);
      int *mem=(int *)alloc->AllocCounted(size);
      if (!mem) return NULL;
      #if USE_CHUNK_POINTERS
        ++mem;
      #endif
      // verify alignment
      #if DO_MEM_FILL
        MemSet32(mem,MEM_NEW_VAL_32,size);
      #endif
      #if _ENABLE_REPORT
        FastAlloc *allocT = FastAlloc::WhichAllocatorUnsafe(mem,_memRegions.GetPageSize());
        DoAssert(alloc==allocT);
        MemSize test = GetSize(mem);
        DoAssert(test>=size);
      #endif

      return mem; // skip header
    }
  }
  #endif
  
  // item must be able to hold busy item information

  size+=sizeof(HeaderType);
  // align size
  if( aligned<(int)_align ) aligned=(int)_align;
  size=AlignSize(size,aligned);

  #if SMALL_ALLOC
    DoAssert( size>smallAllocMax );
  #endif

  FreeBlock *free = FindBest(size);
  if (!free)
  {
    // if we cannot find any match, try cleaning up small allocators
    LogF("%s: cleaning-up (new)",_name);
    CleanUp();
    free=FindBest(size);
    if (!free) return NULL;
  }
  if (!free) return NULL;

  BusyBlock *best = AllocateBlock(free,size);

  if (!best) return NULL;

  char *ret=(char *)((HeaderType *)best->_memory+1); // use shorter data type
  //LogF("  ret %x",ret);

  // create back reference
  *(HeaderType *)best->_memory=best;

  if( best->FreeLink::next || best->FreeLink::prev )
  {
    Fail("Alloc: Memory block corrupt.");
  }

  /*
  if (((int)ret&15)!=0)
  {
    LogF("Memory %x",ret);
    LogF("  Misaligned Alloc large");
  }
  */

  #if DO_MEM_FILL
    MemSet32(ret,MEM_NEW_VAL_32,size-sizeof(HeaderType));
  #endif

  
  return ret;
}

void *MemHeap::Resize(void *mem, MemSize size)
{
  #if MEM_PERF_LOG
  PROFILE_SCOPE_EX(memAl,mem);
  #endif

  if( size>=256*1024*1024 )
  {
    ErrF("Bad memory allocation, %d B",size);
    return NULL;
    //for(;;){}
  }
  #if SMALL_ALLOC
  FastAlloc *alloc=WhichAllocator(mem);
  if( alloc )
  {
    // if we fit withing the small alloc limits, we can keep current block
    if(alloc->ItemSize()>=size) return mem;
    return NULL;
  }
  #endif

  if (!_memBlocks.OwnsPointer(mem))
  {
    // allocated using VirtualAlloc - would be hard to extend, report failure instead
    return NULL;
  }
  
  // calculate aligned size
  size+=sizeof(HeaderType);
  // align size
  size=AlignSize(size,_align);

  // get information from the busy block
  HeaderType *header=(HeaderType *)mem-1;
  FreeBlock *busy=*header;
  // check if this busy block really point to the memory we are releasing
  if( busy->_memory!=(void *)header )
  {
    Fail("Free: Memory block corrupt.");
  }

  BusyBlock *next=_blockList.Next(busy);
  int busySize=BlockSize(busy,next);
  
  // if aligned size did not change, we can keep current block
  if (busySize==size)
  {
    return mem;
  }

  // TODO: implement resizing into the next free block
  return NULL;
}

extern RefD<MemHeapLocked> safeHeap;

/** Check if given heap owns this memory
*/
int MemHeap::IsFromHeap( void *mem ) const
{
  // we need to check all memory owner by this heap - both normal and regions
  if (_memBlocks.OwnsPointer(mem)) return 0;
  if (_memRegions.OwnsPointer(mem)) return 1;
  //BusyBlock *busy=(HeaderType *)mem-1;
  //if( busy<_memBlocks.Data() || busy->EndBusy()>(char *)_memBlocks.Data()+_memBlocks.Size() ) return false;
  return -1;
}

FastAlloc *MemHeap::WhichAllocator( void *pos )
{
  #if USE_CHUNK_POINTERS
    int *smallMem=(int *)pos-1;
    return alloc=FastAlloc::WhichAllocator(smallMem);
  #else
    // detect SmallAlloc without using stored pointer
    // check if mem is in "page-only" heap
    if (_memRegions.OwnsPointer(pos))
    {
      // small alloc detected
      return FastAlloc::WhichAllocatorUnsafe(pos,_memRegions.GetPageSize());
    }
    return NULL;
  #endif
}

void MemHeap::Free( void *mem )
{
  CountFree();
  
  if( !mem ) return;

  #if MEM_PERF_LOG
  PROFILE_SCOPE_EX(memFr,mem);
  #endif

  #if SMALL_ALLOC
  FastAlloc *alloc=WhichAllocator(mem);
  if( alloc )
  {
    // busyBlock first item is never null
    

    #if USE_CHUNK_POINTERS
      #if DO_MEM_FILL
        MemSet32((char *)mem+sizeof(FastAlloc::Chunk **),MEM_FREE_VAL_32,alloc->ItemSize()-sizeof(FastAlloc::Chunk **));
      #endif
      alloc->FreeCounted((char *)smallMem-sizeof(void *));
    #else
      #if DO_MEM_FILL
        MemSet32(mem,MEM_FREE_VAL_32,alloc->ItemSize());
      #endif
      alloc->FreeCounted(mem);
    #endif
    return;
  }
  #endif
  
  if (!_memBlocks.OwnsPointer(mem))
  {
    MEMORY_BASIC_INFORMATION info;
    SIZE_T ret = VirtualQuery(mem,&info,sizeof(info));
    DoAssert(ret==sizeof(info));
    _allocatedDirect -= info.RegionSize;
    // if not, we must be a memory allocated using VirtualAlloc
    VirtualFree(mem,0,MEM_RELEASE);
    return;
  }

  //BusyBlock *busy=(BusyBlock *)mem-1;
  HeaderType *header=(HeaderType *)mem-1;
  FreeBlock *busy=*header;
  // check if this busy block really point to the memory we are releasing
  if( busy->_memory!=(void *)header )
  {
    Fail("Free: Memory block corrupt.");
  }

  ReleaseBlock(busy);
}

MemSize MemHeap::GetSize(void *mem)
{
  if (!mem) return 0;

  FastAlloc *alloc=WhichAllocator(mem);
  if( alloc )
  {
    // busyBlock first item is never null
    // first dword of the item is allocator pointer
    #if USE_CHUNK_POINTERS
      return alloc->ItemSize()-sizeof(FastAlloc::Chunk **);
    #else
      return alloc->ItemSize();
    #endif
  }

  // check if we are from this heap
  if (!_memBlocks.OwnsPointer(mem))
  {
    // if not, we must be a memory allocated using VirtualAlloc
    MEMORY_BASIC_INFORMATION info;
    SIZE_T ret = VirtualQuery(mem,&info,sizeof(info));
    DoAssert(ret==sizeof(info));
    DoAssert(info.RegionSize>=DirectThreshold);
    return info.RegionSize;
  }
  
  //BusyBlock *busy=(BusyBlock *)mem-1;
  HeaderType *header=(HeaderType *)mem-1;
  FreeBlock *busy=*header;
  // check if this busy block really point to the memory we are releasing
  if( busy->_memory!=(void *)header )
  {
    Fail("Free: Memory block corrupt.");
  }

  MemSize sizeWithHeader = BlockSize(busy);
  // first dword of the item is AllocInfo pointer
  return sizeWithHeader-sizeof(int);
}

void MemHeap::ReleaseBlock(FreeBlock *busy)
{


  VerifyStructure();

  if( busy->FreeLink::next || busy->FreeLink::prev )
  {
    Fail("Free: Memory block corrupt.");
    return;
  }

  // check if block is valid block

  //VerifyBlock(busy);

  // insert busy into the memTable
  // find place where to insert it
  
  //BusyBlock   

  bool doInsert=true;

  // change busy block into free block

  // check if we can concatenate with next block

  BusyBlock *next=_blockList.Next(busy);

  int busySize=BlockSize(busy,next);
  #if MEM_PERF_LOG
    ADD_COUNTER(mCnt,1);
    ADD_COUNTER(mSize,busySize);
  #endif

  // the block spans from busy to next
  #if DO_MEM_FILL
  int size = busySize;
  MemSet32(busy->_memory,MEM_FREE_VAL_32,size);
  #endif

  char *begUnlock = busy->_memory;
  char *endUnlock = begUnlock + busySize;

  // no matter if we concatenate or not, we return all memory used by the busy block
  _freeSize += busySize;
  
  if( next && next->IsFree() )
  {
    FreeBlock *nextCat=(FreeBlock *)next;
    int nextCatSize = BlockSize(nextCat);
    Assert(next->_memory==endUnlock);
    // we may extend endUnlock (up to one page)
    endUnlock = _memBlocks.ExtendEnd(endUnlock,endUnlock+nextCatSize);

    _freeList.Insert(busy);
    _freeList.Delete(nextCat);

    _blockList.Delete(nextCat);

    //delete(_infoAlloc) nextCat;
    _infoAlloc.Free(_memRegions,nextCat); // replacement of delete(_infoAlloc) nextCat

    doInsert=false; // block is already inserted in all lists
  }

  // check if we can concatenate with previous block
  BusyBlock *prev=_blockList.Prev(busy);
  if( prev && prev->IsFree() )
  {
    FreeBlock *prevCat=(FreeBlock *)prev;

    // we may extend begUnlock (up to one page)
    begUnlock = _memBlocks.ExtendBeg(begUnlock,prevCat->_memory);

    _blockList.Delete(busy);
    // if the block should not be inserted into the free list
    // it is already inserted and must be deleted
    if( !doInsert )
    {
      // this is the only place where free list is searched when freeing
      _freeList.Delete(busy);
      _nItemsFree--;
    }
    doInsert=false; // block is already inserted in free list

    //delete(_infoAlloc) busy;
    _infoAlloc.Free(_memRegions,busy); // replacement of delete(_infoAlloc) busy

    busy=prevCat;

  }

  if( doInsert )
  {
    // there is no need to keep freeList sorted
    _freeList.Insert(busy);
    _nItemsFree++;
  }

  //int freeSize = BlockSize(busy);
  //int freeStart = busy->_memory-(char *)_memBlocks.Data();
  _memBlocks.UnlockRange(begUnlock,endUnlock);

  Ref<MemHeap> keepAllocated=this; // keep allocated until return
  Release(); // decrease RefCount - enable deallocation

  VerifyStructure();
}

void *MemHeap::AllocRegion(MemSize size, MemSize align)
{
  #if MEM_PERF_LOG
  PROFILE_SCOPE_EX(memAR,mem);
  #endif
  Assert((size&(_memRegions.GetPageSize()-1))==0);
  // region is always allocated from a separate heap
  void *offset = _memRegions.FindFreeRange(size,align);
  if (!offset)
  {
    // first of all check if we can extend the allocated range
    if (_shared._leftToReserve>(int)size)
    {
      // we can extend significantly, let us do it
      int allocSize = intMin(_shared._reserveGranularity,_shared._leftToReserve);
      const MemReservedInfo &info = _memRegions.Reserve(allocSize,size,allocSize);
      if (info._reserved>=(int)size)
      {
        // we have reserved enough, we will use it
        offset = _memRegions.FindFreeRange(size,align);
      }
    }

    if (!offset)
    {
      LogF("%s: cleaning-up (pages)",_name);
      CleanUp();
      offset = _memRegions.FindFreeRange(size,align);
      if (!offset)
      {
        return NULL;
      }
    }
  }
  do
  {
    if (_memRegions.CommitRange(offset,(char *)offset+size))
    {
      return offset;
    }
    // if the request failed, try to decommit some page
  } while (_memBlocks.DecommitOnePage());
  return NULL;
}
void MemHeap::FreeRegion(void *region, MemSize size)
{
  // release single memory page
  _memRegions.UnlockRange(region,(char *)region+size);
  //ReleaseBlock(region);
}


size_t MemHeap::GetRegionRecommendedSize() const
{
  return _memRegions.GetPageSize();
}

void MemHeap::CleanUp()
{
#if SMALL_ALLOC
  // clean-up all small allocators
  for (int i=0; i<nAllocSizes; i++)
  {
    SmallMemAlloc *ma = _smallAllocs[i];
    Assert(ma);
    if (!ma) continue;
    // check how much we cleaned up
    ma->CleanUp();
  }
#endif
}

MemSize FreeList::MaxFreeLeft( const MemHeap *heap ) const
{
  int max=0;
  for( FreeBlock *free=Start(); NotEnd(free); free=Advance(free) )
  {
    int size=heap->BlockSize(free);
    if( max<size ) max=size;    
  }
  return max;
}

MemSize FreeList::TotalFreeLeft( const MemHeap *heap ) const
{
  MemSize total=0;
  for( FreeBlock *free=Start(); NotEnd(free); free=Advance(free) )
  {
    int size=heap->BlockSize(free);
    total+=size;
  }
  return total;
}

MemSize MemHeap::MaxFreeLeft() const
{
  int max=0;
  int val;
  val=_freeList.MaxFreeLeft(this);
  if( max<val ) max=val;
  return max;
}

MemSize MemHeap::TotalFreeLeft() const
{
  #if _DEBUG
    size_t freeLeft = _freeList.TotalFreeLeft(this);
    DoAssert(freeLeft==_freeSize);
  #endif
  // what we have here is how much virtual memory is free
  // we do not want to report memory which cannot be commited as free
  int virtualOnly = _memBlocks.GetReserved()-_memBlocks.GetMaxCommited();
  return _freeSize - virtualOnly;
}

MemSize MemHeap::TotalAllocated() const
{
  // note: this function is quick, does not walk trough any lists
  return _memBlocks.GetReserved() - _freeSize + _memRegions.GetLocked() + _allocatedDirect;
}
MemSize MemHeap::TotalCommitted() const
{
  return _memBlocks.GetCommitted()+_memRegions.GetCommitted() + _allocatedDirect;
}

MemSize MemHeap::TotalReserved() const
{
  return _memBlocks.GetReserved()+_memRegions.GetReserved() + _allocatedDirect;
}

#if _ENABLE_REPORT

bool MemHeap::Check() const
{
  bool ret = true;
#if SMALL_ALLOC
  SmallMemAlloc *alloc = _smallAllocs[3];
  bool ok = alloc->CheckIntegrity();
  if (!ok)
  {
    LogF("SmallAlloc %d broken",alloc->ItemSize());
    ret = false;
  }
//  for (int i=0; i<nAllocSizes; i++)
//  {
//    SmallMemAlloc *alloc = _smallAllocs[i];
//    bool ok = alloc->CheckIntegrity();
//    if (!ok)
//    {
//      LogF("SmallAlloc %d broken",alloc->ItemSize());
//      ret = false;
//    }
//  }
#endif
  return ret;
}

void MemHeap::ReportTotals() const
{
  LogF("Total via new %d",TotalAllocatedByActualAlloc);
  LogF("Total locked in heap: %d",_memBlocks.GetLocked());
  LogF("Total locked in pages: %d",_memRegions.GetLocked());
  LogF("InfoAlloc: %d",InfoAllocAllocated());

#if SMALL_ALLOC
  size_t smallTotalUse = 0;
  size_t smallTotalReq = 0;
  for( int i=0; i<nAllocSizes; i++ )
  {
    SmallMemAlloc *alloc = _smallAllocs[i];
    size_t smallUse = alloc->Allocated();
    size_t smallReq = alloc->Requested();
    smallTotalUse += smallUse;
    smallTotalReq += smallReq;
    #if DO_MEM_STATS
    if (smallUse>0)
    {
      LogF("Small %d - %d,%d",alloc->ItemSize(),smallUse,smallReq);
    }
    #endif
  };
  LogF("SmallAlloc: %d,%d",smallTotalUse,smallTotalReq);
#endif
  #if 1
    // fragmentation memory report
    // report all free blocks
    for
    (
      FreeBlock *free=_freeList.Start();
      NotEndOf(FreeLink,free,_freeList);
      free=_freeList.Advance(free)
    )
    {
      int freeSize=BlockSize(free);
      LogF("  free %x:%x",free->_memory,freeSize);
    }
  #endif
}

void MemHeap::ReportAnyMemory(const char *reason)
{
  // may be called only from the main thread, because it is using the system allocations as well
  AssertThread();

  // heap is already locked if needed - we are in a member funtion  
  typedef IMemoryFreeOnDemand *(MemHeap::*GetFirstMemoryFreeOnDemandF)() const;
  typedef IMemoryFreeOnDemand *(MemHeap::*GetNextMemoryFreeOnDemandF)(IMemoryFreeOnDemand *cur) const;
  const static struct 
  {
    GetFirstMemoryFreeOnDemandF first;
    GetNextMemoryFreeOnDemandF next;
    const char *name;
  } onDemandFcs[]=
  {
    {&MemHeap::GetFirstFreeOnDemand,&MemHeap::GetNextFreeOnDemand,"Heap"},
    {&MemHeap::GetFirstFreeOnDemandSystem,&MemHeap::GetNextFreeOnDemandSystem,"Sys"},
    {&MemHeap::GetFirstFreeOnDemandLowLevel,&MemHeap::GetNextFreeOnDemandLowLevel,"LL"},
  };

  if (reason) RptF("On demand memory report - %s[[[[ ",reason);
  #ifdef _XBOX
    DM_MEMORY_STATISTICS stats;
    stats.cbSize = sizeof(stats);
    DmQueryMemoryStatistics(&stats);
    
    LogF("Total memory: %d (%d KB)",stats.TotalPages,stats.TotalPages*4);
    LogF("available memory: %d (%d KB)",stats.AvailablePages,stats.AvailablePages*4);
    LogF("stack memory: %d (%d KB)",stats.StackPages,stats.StackPages*4);
    LogF("virtual page table memory: %d (%d KB)",stats.VirtualPageTablePages,stats.VirtualPageTablePages*4);
    LogF("system page table memory: %d (%d KB)",stats.SystemPageTablePages,stats.SystemPageTablePages*4);
    LogF("pool memory: %d (%d KB)",stats.PoolPages,stats.PoolPages*4);
    LogF("virtual mapped memory: %d (%d KB)",stats.VirtualMappedPages,stats.VirtualMappedPages*4);
    LogF("image memory: %d (%d KB)",stats.ImagePages,stats.ImagePages*4);
    LogF("file cache memory: %d (%d KB)",stats.FileCachePages,stats.FileCachePages*4);
    LogF("continuous memory: %d (%d KB)",stats.ContiguousPages,stats.ContiguousPages*4);
    LogF("debugger memory: %d (%d KB)",stats.DebuggerPages,stats.DebuggerPages*4);
  #endif
  for (int i=0; i<sizeof(onDemandFcs)/sizeof(*onDemandFcs); i++)
  {
    size_t total = 0;
    for
    (
      IMemoryFreeOnDemand *mem = (this->*onDemandFcs[i].first)();
      mem;
      mem = (this->*onDemandFcs[i].next)(mem)
    )
    {
      size_t memSize = mem->MemoryControlled();
      RString memUsed = FormatByteSize(memSize);
      total += memSize;
      RptF(
        "%s - %s allocated %s",
        onDemandFcs[i].name,
        (const char *)mem->GetDebugName(),
        (const char *)memUsed
      );
    }
    RString memTotal = FormatByteSize(total);
    RptF("** Total %s: %s",onDemandFcs[i].name,cc_cast(memTotal));
  }
  if (reason) RptF("On demand memory report ]]]] ");
  #if DO_MEM_STATS
    // in Testing build print a memory footprint as well
    MemoryFootprintSummary();
  #endif

}

#endif

int MemHeap::CountFreeLeft() const
{
  return _nItemsFree;
}

#if DO_MEM_STATS
class ReportN
{
  public:
  ~ReportN()
  {
    ReportMemory();
  }
};

static ReportN ReportNOnExit INIT_PRIORITY_URGENT;

#endif

class RefHeap
{
  Ref<MemHeapType> _memory;

  public:
  operator MemHeapType *() {return _memory;}
  MemHeapType *operator ->() {return _memory;}

  RefHeap( int sizeMB=256 );
  ~RefHeap();
};

// command line context
struct MemArgumentContext
{
  int maxSize;
  bool vmEnabled;
};

static bool MemSingleArgument(const CommandLine::SingleArgument &arg, MemArgumentContext &ctx)
{
  // check if arg is recognized
  const char *beg = arg.beg;
  const char *end = arg.end;

  if( end==beg ) return false;
  if( *beg=='-' || *beg=='/' )
  {
    beg++;
    // option
    static const char maxmem[]="maxmem=";
    static const char vmem[]="vmem";

    if( !strnicmp(beg,maxmem,strlen(maxmem)) )
    {
      ctx.maxSize = atoi(beg+strlen(maxmem));
    }
    else if( end-beg==(int)strlen(vmem) && !strnicmp(beg,vmem,end-beg) )
    {
      ctx.vmEnabled=true;
    }
  }
  return false;
}

#if defined _XBOX && _ENABLE_CHEATS

  /// lock allowing safe access to XYYYMemoryAllocated variables
  CriticalSection XMemoryLock;

  // track memory allocated by XMemAlloc

  static int XPhysMemoryAllocated = 0;
  static int XVirtMemoryAllocated = 0;

  const int firstTrackedXAId = eXALLOCAllocatorId_D3D;
  const int nTrackedXAIds = eXALLOCAllocatorID_XMCORE+1-eXALLOCAllocatorId_D3D;

  // protected by XMemoryLock
  static int XPhysMemoryAllocatedById[nTrackedXAIds];
  static int XVirtMemoryAllocatedById[nTrackedXAIds];

#endif

int DetectHeapSizeMB();

RefHeap BMemory(DetectHeapSizeMB()) INIT_PRIORITY_URGENT; // memory low condition testing

//MemHeapType *SMemPtr=SMemory;
MemHeapType *BMemPtr=BMemory;

//size_t MemoryFreeOnDemand(size_t size);

size_t FreeOnDemandSystemMemoryLowLevel(size_t size);

int HandleOutOfMemory( size_t size )
{
   // Your code
   return FreeOnDemandSystemMemoryLowLevel(size);
}

#ifdef _WIN32
#include <new.h>
#endif

RefHeap::RefHeap( int sizeMB )
{
  #ifdef _WIN32
    // initialize malloc failure handler
    _set_new_handler(HandleOutOfMemory);
    _set_new_mode(1);
  #endif
  #if defined _XBOX && !(_XBOX_VER>=2)
    // 64 MB on old Xbox prevents us using large alignment
    const int align=DefaultAlign;
  #else
    // on PC (or X360) we have quite a lot more memory available
    // we prefer to avoid fragmentation
    const int align=DefaultMinAlloc > DefaultAlign ? DefaultMinAlloc : DefaultAlign;
  #endif
#ifdef _WIN32
  _memory=new MemHeapType("Default",sizeMB*1024*1024,align,&BMemPtr);
#endif
}

RefHeap::~RefHeap()
{
  // memory can be allocated - it needs to be deallocated before MemHeap::DoDestruct
//  LogF("Memory static ref deallocated.");
//  if( _memory && _memory->TotalAllocated()>0 )
//  {
//    LogF("  %d KB not deallocated",_memory->TotalAllocated()/1024);
//  }
}

//! initialization done after all static variables are initialized
void MemoryMainInit()
{
  #if MEM_CHECK
    InitMemoryCheck();
  #endif
}

void MemoryMainDone()
{
  //GMapFile.Clear();
}

void MemoryInit()
{
}

void MemoryCleanUp()
{
#ifdef _WIN32
  //GMapFile.Clear();
  if( BMemPtr )
  {
    // force all allocators to clean up
    FastCAlloc::CleanUpAll();
    BMemPtr->CleanUp();
  }
#endif
}

void MemoryDone()
{
  // all FastAlloc should be cleaned up now
  // SmallAlloc probably are not

  if ( BMemPtr )
  {
    BMemPtr->CleanUp();
  }

  if ( BMemPtr )
  {
    RptF("Memory should be free now.");
    RptF("  %d B not deallocated",BMemPtr->TotalAllocated());

    #if MEM_CHECK
      ReportAllocated();
    #endif
  }
  else
  {
    LogF("Memory is free now.");
  }
  #if 0 //SMALL_ALLOC
  LogF("Allocation size report");
  for( int i=0; i<=nAllocSlots; i++ )
  {
    int size=i<nAllocSlots ? allocSizes[i] : 1*1024*1024;
    LogF("slot %2d (%7d): %9d",i,size,AllocStats[i]);
  }
  #endif

  #ifdef _WIN32
    //GMapFile.Clear();
  #endif
}

class MemTableFunctions: public MemFunctions
{
  public:
  virtual void *New(size_t size);
  virtual void *New(size_t size, const char *file, int line);
  virtual void Delete(void *mem);
  virtual void Delete(void *mem, const char *file, int line);
  virtual void *Realloc(void *mem, size_t size);
  virtual void *Realloc(void *mem, size_t size, const char *file, int line);
  virtual void *Resize(void *mem, size_t size);

  void *NewPage(size_t size, size_t align);
  void DeletePage(void *page, size_t size);
  size_t GetPageRecommendedSize();

  //! base of memory allocation space, NULL is unknown/undefined
  virtual void *HeapBase()
  {
    return NULL;
  }

  //! approximate total amount of commited memory
  virtual size_t HeapUsed()
  {
    if( !BMemory ) return 0;
    return BMemory->TotalAllocated();
  }

  virtual size_t HeapUsedByNew();
  //! approximate total amount of commited memory
  virtual size_t HeapCommited()
  {
    if( !BMemory ) return 0;
    return BMemory->TotalCommitted();
  }

  //! approximate total count of free memory blocks (used to determine fragmentation)
  virtual int FreeBlocks()
  {
    if( !BMemory ) return 0;
    return BMemory->CountFreeLeft();
  }

  //! approximate total count of allocated memory blocks (used to determine fragmentation)
  virtual int MemoryAllocatedBlocks()
  {
    if( !BMemory ) return 0;
    return BMemory->RefCounter();
  }

  //! list all allocated blocks to debug log
  virtual void Report()
  {
    #if _ENABLE_REPORT
    if( !BMemory ) return;
    int alloc=BMemory->TotalAllocated();
    int commit=BMemory->TotalCommitted();
    LogF("Total allocated %d, commited %d",alloc,commit);
    #endif
  }

  //! check heap integrity
  virtual bool CheckIntegrity() {return BMemory->Check();}
  //! check if out of memory is signalized
  //! if true, application should limit memory allocation as much as possible
  virtual bool IsOutOfMemory() {return MemoryErrorState;}

  virtual void CleanUp() {BMemory->CleanUp();}

  #if MAIN_HEAP_MT_SAFE
	virtual void Lock() {BMemory->Lock();}
	virtual void Unlock() {BMemory->Unlock();}
	#endif
};

MemTableFunctions OMemTableFunctions INIT_PRIORITY_URGENT;

MemFunctions *GMemFunctions = &OMemTableFunctions;

//////////////////////////////////////////////////////////////////////////

namespace Memory
{
void ReportAnyMemory(const char *reason)
{
    #if _ENABLE_REPORT
  if (BMemory)
  {
    BMemory->ReportAnyMemory(reason);
  }
#endif
  };
  mem_size_t FreeSystemMemory(mem_size_t size)
{
    if (BMemory)
  {
      return BMemory->FreeSystemMemory(size);
      }
    return 0;
    }

  bool CheckVirtualFree(mem_size_t size)
{
    // TODO: implement CheckVirtualFree for MemHeap as well
    return true; // BMemory->CheckVirtualFree(size);
      }
    
  mem_size_t TotalAllocated() {return BMemory ? BMemory->TotalAllocated() : 0;}
  mem_size_t TotalCommitted() {return BMemory ? BMemory->TotalCommitted() : 0;}
  mem_size_t TotalLowLevelMemory() {return BMemory ? BMemory->TotalLowLevelMemory() : 0;}

  LockedFreeOnDemandEnumerator GetFreeOnDemandEnumerator() {return BMemory->GetFreeOnDemandEnumerator();}
    }
    
/*!
\param freeRequired how much main and low level heap memory should be left
\param freeSysRequired  how much system heap memory should be left
*/

IMemoryFreeOnDemand *ProgressGetFreeOnDemandInterface();

#if _ENABLE_REPORT
void ReportGC(const char *text)
{
  MEMORYSTATUS memstat;
  memstat.dwLength = sizeof(memstat);
  GlobalMemoryStatus(&memstat);
  size_t sysFree = memstat.dwAvailPhys;
  size_t heapFree = MemoryCommited()-MemoryUsed();
  size_t llFree = BMemory->TotalLowLevelMemory();
  LogF("%s: heap %d, system %d, LL %d",text,heapFree+sysFree,sysFree,llFree);
}
#endif

/// get information about memory which is committed outside of our heaps (e.g. file cache)
mem_size_t MemoryStoreUsed();



// check how much memory is allocated now
// this is platform dependent operation
#if _ENABLE_CHEATS

static size_t TotalAllocatedCheck()
{
  #ifdef _XBOX
    size_t normalHeap = BMemory->TotalAllocated();
    ScopeLock<CriticalSection> lock(XMemoryLock);
    size_t xPhysMem = XPhysMemoryAllocated;
    size_t xVirtMem = XVirtMemoryAllocated;
    return normalHeap+xPhysMem+xVirtMem;
  #else
    size_t normalHeap = BMemory->TotalAllocated();
    return normalHeap;
  #endif
}

#endif


/**
@param id any slots containing id in the name (ignore case) are processes
@param [out] total ammount of memory which was really released, based on system wide measurements
@return total size of items released
*/
size_t FreeOnDemandReleaseSlot(const char *id, size_t toRelease, size_t *verifyRelease)
{ 
  #if _ENABLE_CHEATS
  if (BMemory)
  {
    return BMemory->FreeOnDemandReleaseSlot(id,toRelease,verifyRelease);
  }
  #endif
  return 0;
}

#if _ENABLE_REPORT
size_t MemHeap::FreeOnDemandReleaseSlot(const char *id, size_t toRelease, size_t *verifyRelease)
{
  size_t released = 0;
  #if _ENABLE_CHEATS
  // check how much memory is free now
  // this is platform dependent operation
  size_t totalBefore = TotalAllocatedCheck();

  // may be called only from the main thread, because it is using the system allocations as well
  AssertThread();
  
  typedef IMemoryFreeOnDemand *(MemHeap::*GetFirstMemoryFreeOnDemandF)() const;
  typedef IMemoryFreeOnDemand *(MemHeap::*GetNextMemoryFreeOnDemandF)(IMemoryFreeOnDemand *cur) const;
  const static struct 
  {
    GetFirstMemoryFreeOnDemandF first;
    GetNextMemoryFreeOnDemandF next;
    const char *name;
  } onDemandFcs[]=
  {
    {&MemHeap::GetFirstFreeOnDemand,&MemHeap::GetNextFreeOnDemand,"Heap"},
    {&MemHeap::GetFirstFreeOnDemandSystem,&MemHeap::GetNextFreeOnDemandSystem,"Sys"},
    {&MemHeap::GetFirstFreeOnDemandLowLevel,&MemHeap::GetNextFreeOnDemandLowLevel,"LL"},
  };

  int slotCount = 0;
  RString lowId = id;
  lowId.Lower();
  int statsDiffTotal = 0;
  for (int i=0; i<sizeof(onDemandFcs)/sizeof(*onDemandFcs); i++)
  {
    for(
      IMemoryFreeOnDemand *mem = (this->*onDemandFcs[i].first)();
      mem;
      mem = (this->*onDemandFcs[i].next)(mem)
    )
    {
      // check if the name matches
      RString lowName = mem->GetDebugName();
      lowName.Lower();
      if (strstr(lowName,lowId))
      {
        size_t totalBeforeItem = TotalAllocatedCheck();
        size_t statBefore = mem->MemoryControlled();
        size_t memReleased = mem->Free(toRelease);
        size_t statAfter = mem->MemoryControlled();
        size_t totalAfterItem = TotalAllocatedCheck();
        // print single slot stats
        if (memReleased>0 || totalAfterItem<totalBeforeItem || statAfter<statBefore)
        {
          LogF(
            "Slot %s: released %d, allocation difference %d, stats difference %d",
            (const char *)mem->GetDebugName(),
            memReleased,totalBeforeItem-totalAfterItem,statBefore-statAfter
          );
          released += memReleased;
          statsDiffTotal += statBefore-statAfter;
          slotCount++;
        }
      }
    }
  }
  size_t totalAfter = TotalAllocatedCheck();
  // if there were mutliple slots affects, print multiple slot stats
  if (slotCount>1)
  {
    LogF(
      "Released %d, allocation difference %d, stats difference %d",
      released,totalBefore-totalAfter,statsDiffTotal
    );
  }
  if (verifyRelease)
  {
    *verifyRelease = totalBefore-totalAfter;
  }
  #endif
  return released;
}
#endif

int FrameId=0;

void FreeOnDemandFrame()
{
  if (BMemory)
  {
    PROFILE_SCOPE(gbFrm); // scope to ensure memory CS locks are inside of a common scope
    BMemory->FreeOnDemandFrame();
  }
}

#include "El/FileServer/fileServerMT.hpp"


void MemHeap::FreeOnDemandFrame()
{
  // called only from main thread - accessing system allocator
  AssertThread();

  // advance global FrameId first, all local ones will follow
  FrameId++;
  
  // note: memory lock not taken (see MemHeapLocked::FreeOnDemandFrame)
  // note: we do not need memory CS lock just to traverse on demand lists, as it is stable
  typedef IMemoryFreeOnDemand *(MemHeap::*GetFirstMemoryFreeOnDemandF)() const;
  typedef IMemoryFreeOnDemand *(MemHeap::*GetNextMemoryFreeOnDemandF)(IMemoryFreeOnDemand *cur) const;
  const static struct 
  {
    GetFirstMemoryFreeOnDemandF first;
    GetNextMemoryFreeOnDemandF next;
    const char *name;
  } onDemandFcs[]=
  {
    {&MemHeap::GetFirstFreeOnDemand,&MemHeap::GetNextFreeOnDemand,"Heap"},
    {&MemHeap::GetFirstFreeOnDemandSystem,&MemHeap::GetNextFreeOnDemandSystem,"Sys"},
    {&MemHeap::GetFirstFreeOnDemandLowLevel,&MemHeap::GetNextFreeOnDemandLowLevel,"LL"},
  };
  for (int i=0; i<sizeof(onDemandFcs)/sizeof(*onDemandFcs); i++)
  {
    for
    (
      IMemoryFreeOnDemand *mem = (this->*onDemandFcs[i].first)();
      mem;
      mem = (this->*onDemandFcs[i].next)(mem)
    )
    {
      mem->MemoryControlledFrame();
    }
  }  
  
}

void RegisterFreeOnDemandMemory(IMemoryFreeOnDemand *object)
{
#ifdef _WIN32
  DoAssert( BMemory );
#endif
  if( BMemory )
  {
    BMemory->RegisterFreeOnDemand(object);
  }
}

void RegisterFreeOnDemandLowLevelMemory(IMemoryFreeOnDemand *object)
{
#ifdef _WIN32
  DoAssert( BMemory );
#endif
  if( BMemory )
  {
    BMemory->RegisterFreeOnDemandLowLevel(object);
  }
}

size_t FreeOnDemandLowLevelMemory(size_t size)
{
  if (BMemory)
  {
    return BMemory->FreeOnDemandLowLevelMemory(size);
  }
  return 0;
}

/**
Perform any possible low-level memory freeing as necessary 

Note: different from FreeOnDemandLowLevel
*/
size_t MemHeap::FreeOnDemandLowLevelMemory(size_t size)
{
  // balanced releasing of all kinds of memory
  // note: allocation here is extremely dangerous
  AUTO_STATIC_ARRAY(OnDemandStats,stats,128);

  for(IMemoryFreeOnDemand *walk=GetFirstFreeOnDemandLowLevel(); walk; walk = GetNextFreeOnDemandLowLevel(walk))
  {
    OnDemandStats &mstat = stats.Append();
    mstat.mem = walk;
    mstat.system = false;
  }
  mem_size_t systemReleased;
  mem_size_t released = MemoryFreeOnDemandList::BalanceList(size,stats.Data(),stats.Size(),NULL,0,&systemReleased);
  #if 0 // _ENABLE_REPORT
  if (released>0)
  {
    LogF("Low level memory release forced - %d B",released);
  }
  #endif
  return released;
}

void FreeOnDemandLock()
{
  if (BMemory) BMemory->Lock();
}
void FreeOnDemandUnlock()
{
  if (BMemory) BMemory->Unlock();
}

IMemoryFreeOnDemand *GetFirstFreeOnDemandMemory()
{
  if (BMemory) return BMemory->GetFirstFreeOnDemand();
  return NULL;
}
IMemoryFreeOnDemand *GetNextFreeOnDemandMemory(IMemoryFreeOnDemand *cur)
{
  if (BMemory) return BMemory->GetNextFreeOnDemand(cur);
  return NULL;
}

IMemoryFreeOnDemand *GetFirstFreeOnDemandLowLevelMemory()
{
  if (BMemory) return BMemory->GetFirstFreeOnDemandLowLevel();
  return NULL;
}
IMemoryFreeOnDemand *GetNextFreeOnDemandLowLevelMemory(IMemoryFreeOnDemand *cur)
{
  if (BMemory) return BMemory->GetNextFreeOnDemandLowLevel(cur);
  return NULL;
}



//////////////////////////////////////////////////////////////////////////


/// coalesce regions with the same flags
class CoalesceRegions
{
  DWORD _state;
  DWORD _type;
  SIZE_T _addr;
  SIZE_T _regionSize;
public:
  CoalesceRegions()
  {
    _state = 0;
    _type = 0;
    _regionSize = 0;
    _addr = 0;
  }
  
  template <class FlushFunc>
  void Flush(const FlushFunc &func)
  {
    if (_regionSize>0)
    {
      func(_state,_type,_addr,_regionSize);
    }
  }

  template <class FlushFunc>
  void Add(DWORD state, DWORD type, SIZE_T addr, SIZE_T regionSize, const FlushFunc &func)
  {
    if (_state!=state || addr!=_addr+_regionSize || type!=_type)
    {
      if (_regionSize>0)
      {
        func(_state,_type,_addr,_regionSize);
      }
      _state = state;
      _type = type;
      _addr = addr;
      _regionSize = regionSize;
    }
    else
    {
      _regionSize += regionSize;
    }
  }
};

enum
{
  PageOwnFree,
  /// page reserved by some heap under our control
  PageOwnReservedHeap=0x10,
  PageOwnReservedCommittedLast=PageOwnReservedHeap+15,
  /// page committed by some heap under our control
  PageOwnCommittedHeap,
  PageOwnCommittedHeapLast=PageOwnCommittedHeap+15,
  /// page reserved by some library (most likely D3D + driver)
  PageOwnReservedOther,
  PageOwnReservedOtherLast=PageOwnReservedOther+15,
  PageOwnCommittedOther,
  PageOwnCommittedOtherLast=PageOwnCommittedOther+15,
};

#ifndef _XBOX
struct WriteRegionToRpt
{
  void operator () (char state, DWORD type, SIZE_T addr, SIZE_T regionSize) const
  {
    const char *stateDesc = "???";
    switch (state)
    {
      #define CASE_ENUM(x) case PageOwn##x: stateDesc = #x; break;
      CASE_ENUM(Free)
      CASE_ENUM(ReservedHeap)
      CASE_ENUM(ReservedHeap+1)
      CASE_ENUM(ReservedHeap+2)
      CASE_ENUM(ReservedHeap+3)
      CASE_ENUM(ReservedHeap+4)
      CASE_ENUM(ReservedHeap+5)
      CASE_ENUM(ReservedHeap+6)
      CASE_ENUM(ReservedHeap+7)
      CASE_ENUM(CommittedHeap)
      CASE_ENUM(CommittedHeap+1)
      CASE_ENUM(CommittedHeap+2)
      CASE_ENUM(CommittedHeap+3)
      CASE_ENUM(CommittedHeap+4)
      CASE_ENUM(CommittedHeap+5)
      CASE_ENUM(CommittedHeap+6)
      CASE_ENUM(CommittedHeap+7)
      CASE_ENUM(ReservedOther)
      CASE_ENUM(CommittedOther)
      #undef CASE_ENUM
    }
    const char *typeDesc = "";
    switch (type)
    {
      #define CASE_ENUM(x) case MEM_##x: stateDesc = #x; break;
      CASE_ENUM(IMAGE)
      CASE_ENUM(MAPPED)
      CASE_ENUM(PRIVATE)
      #undef CASE_ENUM
    }
    RptF("Region: %p,%d KB,%s %s",addr,regionSize/1024,stateDesc,typeDesc);
  }
};
#endif

static int CheckHeapOwnerShip(size_t addr, size_t size)
{
  // check if given page is owner by some heap we know (and which)
  // walk through all known heaps
  
  if (BMemPtr)
  {
    int own = BMemPtr->IsFromHeap((void *)addr);
    if (own>=0) return own;
  }
  if (safeHeap)
  {
    int own = safeHeap->IsFromHeap((void *)addr);
    if (own>=0) return 4+own;
  }
  
  return -1;
}



#if _ENABLE_REPORT && !defined _XBOX
  #define _VM_OWN_TRACK 0
#endif

#if _VM_OWN_TRACK
/// store last known ownership for each memory page
// possible ownership values:


struct WriteDiffToLog
{
  void operator () (DWORD state, DWORD type, SIZE_T addr, SIZE_T regionSize) const
  {
    const char *stateDesc = "???";
    switch (state)
    {
      #define CASE_ENUM(x) case PageOwn##x: stateDesc = #x; break;
      CASE_ENUM(Free)
      CASE_ENUM(ReservedHeap)
      CASE_ENUM(ReservedHeap+1)
      CASE_ENUM(ReservedHeap+2)
      CASE_ENUM(ReservedHeap+3)
      CASE_ENUM(ReservedHeap+4)
      CASE_ENUM(ReservedHeap+5)
      CASE_ENUM(ReservedHeap+6)
      CASE_ENUM(ReservedHeap+7)
      CASE_ENUM(CommittedHeap)
      CASE_ENUM(CommittedHeap+1)
      CASE_ENUM(CommittedHeap+2)
      CASE_ENUM(CommittedHeap+3)
      CASE_ENUM(CommittedHeap+4)
      CASE_ENUM(CommittedHeap+5)
      CASE_ENUM(CommittedHeap+6)
      CASE_ENUM(CommittedHeap+7)
      CASE_ENUM(ReservedOther)
      CASE_ENUM(CommittedOther)
      #undef CASE_ENUM
    }
    const char *typeDesc = "";
    switch (type)
    {
      #define CASE_ENUM(x) case MEM_##x: stateDesc = #x; break;
      CASE_ENUM(IMAGE)
      CASE_ENUM(MAPPED)
      CASE_ENUM(PRIVATE)
      #undef CASE_ENUM
    }
    LogF("Diff: %p,%6d KB,%s %s",addr,regionSize/1024,stateDesc,typeDesc);
  }
};


char MapOwnership[0x80000000U/0x1000U];

static char CanonicalOwner(char owner)
{
  if (owner==PageOwnFree) return 0;
  // when owner is the same, we do nor care if it is committed or reserved
  if (owner>=PageOwnReservedHeap && owner<=PageOwnCommittedHeapLast) return 1;
  if (owner>=PageOwnReservedOther && owner<=PageOwnCommittedOtherLast) return 2;
  return owner;
}

static bool ChangedOwner(char owner1, char owner2)
{
  char o1Canonical = CanonicalOwner(owner1);
  char o2Canonical = CanonicalOwner(owner2);
  return o1Canonical!=o2Canonical;
}

#endif

static char GetPageState(DWORD bufferState, size_t addr, size_t pageSize)
{
  char state = PageOwnFree;
  if (bufferState&MEM_COMMIT)
  {
    state = PageOwnCommittedOther;
    int own = CheckHeapOwnerShip(addr,pageSize);
    if (own>=0)
    {
      state = PageOwnCommittedHeap+own;
    }
  }
  else if (bufferState&MEM_RESERVE)
  {
    state = PageOwnReservedOther;
    int own = CheckHeapOwnerShip(addr,pageSize);
    if (own>=0)
    {
      state = PageOwnReservedHeap+own;
    }
  }
  return state;
}

// we report any differences against the map
void PrintVMMap(int extended, const char *title=NULL)
{
#ifndef _XBOX
  if (title) LogF("VM map %s",title);
  // print map of virtual memory
  // scan whole application space (2 GB) address range
  // do not try to access kernel space - we would get no information anyway
  size_t start = 0;
  size_t end = 1U<<31;
  SYSTEM_INFO si;
  GetSystemInfo(&si);
  size_t pageSize = si.dwPageSize;
  DoAssert(pageSize>=0x1000); // assumption made by MapOwnership
  size_t longestFreeApp = 0;
  
  /*
  Bug: a lot of small committed mapped pages (8 KB regions) interleaved with 60-64 KB free regions.
  Suspected SoundMax HD driver leak.
  */
  /// compute small mapped regions
  int smallMappedRegions = 0;
  size_t smallMappedRegionsSize = 0;
  CoalesceRegions coalesce;
  CoalesceRegions coalesceDiff;
  int index=0;
  for (size_t addr = start; addr<end; )
  {
    MEMORY_BASIC_INFORMATION buffer;
    SIZE_T retSize = VirtualQuery((void *)addr,&buffer,sizeof(buffer));
    if (retSize==sizeof(buffer) && buffer.RegionSize>0)
    {
      // detect suspected "leak" pattern
      if (buffer.Type==MEM_MAPPED && buffer.RegionSize<=8*1024)
      {
        smallMappedRegions++;
        smallMappedRegionsSize+= buffer.RegionSize;
      }
      if (extended>0)
      {
        char state = GetPageState(buffer.State, addr, pageSize);
        coalesce.Add(state,buffer.Type,addr,buffer.RegionSize,WriteRegionToRpt());
      }
      #if _VM_OWN_TRACK
      if (extended==0)
      {
        for (size_t page=0,pIndex=index; page<buffer.RegionSize; page+=pageSize,pIndex++)
        {
          // mark each page 
          // check what ownership do we have now
          char state = GetPageState(buffer.State, addr+page, pageSize);
          if (ChangedOwner(MapOwnership[pIndex],state))
          {
            coalesceDiff.Add(state,buffer.Type,addr+page,pageSize,WriteDiffToLog());
          }
          MapOwnership[pIndex] = state;
        }
      }
      #endif
      // dump information about this region
      if (buffer.State&MEM_FREE)
      {
        if (buffer.RegionSize>longestFreeApp) longestFreeApp = buffer.RegionSize;
      }
      addr += buffer.RegionSize;
      index+= buffer.RegionSize/pageSize;
    }
    else
    {
      // always proceed
      addr += pageSize;
      index++;
    }
  }
  if (extended)
  {
    coalesce.Flush(WriteRegionToRpt());
  }
  #if _VM_OWN_TRACK
  if (extended==0)
  {
    coalesceDiff.Flush(WriteDiffToLog());
  }
  #endif
  RptF("Longest free VM region: %d",longestFreeApp);
  RptF("Small mapped regions: %d, size %d B",smallMappedRegions,smallMappedRegionsSize);
#endif
}



#if _ENABLE_REPORT
// create and compare virtual memory snapshots
// for each VM page identify its owner

#endif

void ReportMemoryStatus(int level)
{
  MEMORYSTATUS memstat;
  memstat.dwLength = sizeof(memstat);
  GlobalMemoryStatus(&memstat);
  size_t used = GetMemoryUsedSize();
  size_t commited = GetMemoryCommitedSize();
  int oneMB = 1024*1024;
  RptF("Virtual memory total %d MB (%u B)",memstat.dwTotalVirtual/oneMB,memstat.dwTotalVirtual);
  RptF("Virtual memory free %d MB (%u B)",memstat.dwAvailVirtual/oneMB,memstat.dwAvailVirtual);
  RptF("Physical memory free %d MB (%u B)",memstat.dwAvailPhys/oneMB,memstat.dwAvailPhys);
  RptF("Page file free %d MB (%u B)",memstat.dwAvailPageFile/oneMB,memstat.dwAvailPageFile);
  RptF("Process working set %d MB (%u B)",used/oneMB,used);
  RptF("Process page file used %d MB (%u B)",commited/oneMB,commited);
  RptF("Runtime reserved %d MB (%d B)",BMemory->TotalReserved()/oneMB,BMemory->TotalReserved());
  RptF("Runtime committed %d MB (%d B)",BMemory->TotalCommitted()/oneMB,BMemory->TotalCommitted());
  //BMemory->Reserved
  //RptF("BM)");
  if (level>0) PrintVMMap(level>1 ? 1 : 0);
}

void ReportMemoryStatus()
{
  ReportMemoryStatus(1);
}

/*!
\patch_internal 1.05 Date 07/10/2001 by Ondra.
- Fix: Bytes were reported instead of KB.
*/
void MemHeap::MemoryError(int size)
{
  MemoryErrorReported();
  #if _ENABLE_REPORT
    ReportAnyMemory("MemoryError");

    // force output of call-stack
    GDebugExceptionTrap.LogFFF("Out of Memory", true);

    ReportMemoryStatus();
  #endif
  ErrorMessage(
    "Out of memory (requested %d KB).\n"
    "Reserved %d KB.\n"
    "Total free %d KB\n"
    "Free blocks %d, Max free size %d KB",
    size/1024,
    _memBlocks.GetReserved()/1024,
    TotalFreeLeft()/1024,
    CountFreeLeft(),
    MaxFreeLeft()/1024
  );
}

void MemHeap::MemoryErrorPage(int size)
{
  MemoryErrorReported();
  #if _ENABLE_REPORT
    ReportAnyMemory("MemoryError");
    // force output of call-stack
    GDebugExceptionTrap.LogFFF("Out of Memory", true);

    ReportMemoryStatus();
  #endif

  ErrorMessage(
    "Out of memory (pages) (requested %d KB).\n"
    "Reserved: %d KB.\n"
    "Total free %d KB\n",
    size/1024,
    _memRegions.GetReserved()/1024,
    (_memRegions.GetReserved()-_memRegions.GetLocked())/1024,
    _memRegions.FindLongestRange()*_memRegions.GetPageSize()/1024
  );
}

void MemoryErrorPage(size_t size)
{
  if (BMemory) BMemory->MemoryErrorPage(size);
}


void MemoryErrorMalloc( int size )
{
  MemoryErrorReported();
  #ifdef _XBOX
    Fail("Out of system memory");
  #endif
  ReportMemoryStatus();
  ErrorMessage("Out of Win32 memory (%d KB).\n",size);
}


#if _MSC_VER
#define MEM_CONV __cdecl
#else
#define MEM_CONV
#endif


/*!
\patch 1.45 Date 2/15/2002 by Ondra
- Fixed: When out of memory, application was crashing.
Message should now be shown instead.
*/

inline void *ActualAlloc( size_t size )
{
#ifdef _WIN32
  AssertThread();
  #if NO_ALLOCATION_AFTER_ERROR
  if (MemoryErrorState)
  {
    // we did not have memory before - we do not have it again
    return NULL;
  }
  #endif
  for (;;)
  {
    void *mem = BMemory->Alloc(size);
    if (mem)
    {
      #if _ENABLE_REPORT
        size_t allocSize = BMemory->GetSize(mem);
        #if MAIN_HEAP_MT_SAFE
        InterlockedExchangeAdd(&TotalAllocatedByActualAlloc,allocSize);
        #else
        TotalAllocatedByActualAlloc += allocSize;
        #endif
      #endif
      return mem;
    }
    size_t freeSize = size>0 ? size : 1;
    size_t freed = FreeOnDemandLowLevelMemory(freeSize);
    if (freed==0)
    {
      // failure: nothing to free - we are unable to satisfy the request
      if (GDebugger.IsDebugger())
      {
        Fail("Memory allocation failed, debugging opportunity");
      }
      else
      {
        BMemory->MemoryError(size);
        return NULL;
      }
    }
    // some memory was freed, but request was not successful
  }
#else
  return malloc(size);
#endif
}

inline void ActualFree( void *mem )
{
#ifdef _WIN32
  AssertThread();

#if _ENABLE_REPORT
  if (mem)
  {
    size_t allocSize = BMemory->GetSize(mem);
    #if MAIN_HEAP_MT_SAFE
    InterlockedExchangeAdd(&TotalAllocatedByActualAlloc,-(int)allocSize);
    #else
    TotalAllocatedByActualAlloc -= allocSize;
    #endif
  }
#endif
  //if( SMemory->IsFromHeap(mem) ) SMemory->Free(mem);
  //else
  BMemory->Free(mem);
#else
        free(mem);
#endif
}

inline void *ActualResize( void *mem, size_t size )
{
#ifdef _WIN32
  AssertThread();
  #if NO_ALLOCATION_AFTER_ERROR
  if (MemoryErrorState)
  {
    // we did not have memory before - we do not have it again
    return NULL;
  }
  #endif
  return BMemory->Resize(mem,size);
#else
  return NULL;
#endif
}

size_t MemTableFunctions::HeapUsedByNew()
{
  #if !_ENABLE_REPORT
    Fail("Do not use in retail version");
    return 0;
  #elif 0 // MEM_CHECK
    // scan all MemoryInfos
    size_t total = 0; // total memory wanted
    size_t guard = 0; // total memory used by guard and similar 
    for
    (
      MemoryInfo *info=Allocated.First(); info; info=Allocated.Next(info)
    )
    {
      total += info->Size();
      guard += PREPARE_ALLOC_SLACK;
    }

    // ask C allocators
    size_t fastAllocated = FastCAlloc::TotalAllocated();
    size_t fastRequested = FastCAlloc::TotalRequested();
    
    total += fastRequested;
    size_t totalAlloc = TotalAllocatedByActualAlloc + fastAllocated;
    size_t infoAlloc = BMemory->InfoAllocAllocated();
    size_t smallOverhead = 0;
    #if SMALL_ALLOC
    for( int i=0; i<nAllocSizes; i++ )
    {
      SmallMemAlloc *alloc = _smallAllocs[i];
      size_t smallUse = alloc->Allocated();
      size_t smallReq = alloc->Requested();
      smallOverhead += smallUse-smallReq;
      
    }
    #endif
    //return total + FastCAlloc::TotalRequested();
    // small alloc statistics are already included in "total"
    // small alloc allocations are included in TotalAllocatedByActualAlloc
    return totalAlloc-total-guard+infoAlloc;
  #else
    size_t fastAllocated = FastCAlloc::TotalAllocated();
    size_t fastRequested = FastCAlloc::TotalRequested();
    size_t infoAlloc = BMemory->InfoAllocAllocated();
    size_t fragm = BMemory->TotalCommitted()-BMemory->TotalAllocated();
    // count partially allocated blocks
    return infoAlloc+fastAllocated-fastRequested+fragm;
  #endif
}


void *MemTableFunctions::New(size_t size)
{
  CountNew("nofile",0,size);
  #if MEM_CHECK
    size=PrepareAlloc(size);
  #endif
  void *ret=ActualAlloc(size);

  #if MEM_CHECK
    ret=FinishAlloc(ret,size,"",0);
  #endif
  return ret;
}

void *MemTableFunctions::New(size_t size, const char *file, int line)
{
  #if DO_MEM_STATS
  if (*file==0)
  {
    LogF("Warning");
  }
  #endif
  CountNew(file,line,size);
  #if MEM_CHECK
    size=PrepareAlloc(size);
  #endif
  void *ret=ActualAlloc(size);

  #if MEM_CHECK
    ret=FinishAlloc(ret,size,file,line);
  #endif
  return ret;
}

void MemTableFunctions::Delete(void *mem)
{
  if( !mem ) return;
  #if MEM_CHECK
    mem=PrepareFree(mem);
  #endif
  // determine which heap it is in
  ActualFree(mem);
}
void MemTableFunctions::Delete(void *mem, const char *file, int line)
{
  if( !mem ) return;
  #if MEM_CHECK
    mem=PrepareFree(mem);
  #endif
  // determine which heap it is in
  ActualFree(mem);
}

void *MemTableFunctions::Realloc(void *mem, size_t size)
{
  // get size of the old memory block?
  size_t oldSize = 0;
  if (mem)
  {
    #if MEM_CHECK
    oldSize = GetRealSize(mem);
    #else
    oldSize = BMemory->GetSize(mem);
    #endif
  }

  if (oldSize==size)
  {
    // if size is the same, there is no need to realloc
    return mem;
  }
  // realloc
  void *newmem = New(size);

  if (mem && newmem)
  {
    // move memory as necessary
    size_t copy = oldSize;
    if (copy>size) copy = size;
    memcpy(newmem,mem,copy);

    Delete(mem);
  }
  return newmem;
}

void *MemTableFunctions::Resize(void *mem, size_t size)
{
  #if MEM_CHECK
    const MemoryInfo *info = GetMemoryInfo(mem);
    char file[MemoryInfo::FileBufferSize];
    int line;
    if (info)
    {
      // secure - info->_file is certainly zero terminated (see MemoryInfo::MemoryInfo)
      strcpy(file,info->File());
      line = info->Line();
    }
    else
    {
      Fail("Error: Unexpected NULL");
      line = -1;
    }
    size_t oldSize = PrepareAlloc(GetRealSize(mem));
    // no memory fill - memory is already used
    mem = PrepareFree(mem,false);
    size=PrepareAlloc(size);
  #endif
  void *ret=ActualResize(mem,size);
  #if MEM_CHECK
    if (ret)
    {
      // no memory fill - memory is already used
      // TODO: fill newly extended memory
      ret=FinishAlloc(ret,size,file,line,false);
    }
    else
    {
      // no memory fill - memory is already used
      FinishAlloc(mem,oldSize,file,line,false);
    }
  #endif
  return ret;
}

void *MemTableFunctions::Realloc(void *mem, size_t size, const char *file, int line)
{
  // get size of the old memory block?
  size_t oldSize = 0;
  if (mem)
  {
    #if MEM_CHECK
    oldSize = GetRealSize(mem);
    #else
    oldSize = BMemory->GetSize(mem);
    #endif
  }

  if (oldSize==size)
  {
    // if size is the same, there is no need to realloc
    return mem;
  }
  // realloc
  void *newmem = New(size,file,line);

  if (mem && newmem)
  {
    // move memory as necessary
    size_t copy = oldSize;
    if (copy>size) copy = size;
    memcpy(newmem,mem,copy);

    Delete(mem);
  }
  return newmem;
}

#if _ENABLE_REPORT
size_t MemHeap::InfoAllocAllocated() const
{
  return _infoAlloc.TotalAllocated(_memRegions);
}
#endif

void *MemTableFunctions::NewPage(size_t size, size_t align)
{
  void * mem = BMemory->AllocRegion(size,align);
  while (!mem)
  {
    // some cleanup may help
    size_t free = BMemory->FreeOnDemandLowLevel(size);
    if (free<=0)
    {
      if (GDebugger.IsDebugger())
      {
        Fail("Memory allocation failed, debugging opportunity");
      }
      else
      {
        BMemory->MemoryErrorPage(size);
        return NULL;
      }
    }
    mem = BMemory->AllocRegion(size,align);;
  }
  return mem;
}
void MemTableFunctions::DeletePage(void *page, size_t size)
{
  BMemory->FreeRegion(page,size);
}

size_t MemTableFunctions::GetPageRecommendedSize()
{
  return BMemory->GetRegionRecommendedSize();
}

#ifdef _WIN32

//---------------------------------------------------------------------------
//  Net-heap support:

#ifdef _WIN32

/// Static instance of mt-safe heap (to be destructed as late as possible).
RefD<MemHeapLocked> safeHeap INIT_PRIORITY_URGENT;

#ifdef NET_LOG_SAFE_HEAP
unsigned safeHeapCounter = 0;
#endif

#ifdef _XBOX
#define NET_HEAP_SIZE   16
#else
#define NET_HEAP_SIZE   32
#endif

class MemFunctionsSafe: public MemFunctions
{
  private:
  bool Create();
  void Destroy();

  public:
  virtual void *New(size_t size);
  virtual void *New(size_t size, const char *file, int line);
  virtual void Delete(void *mem);
  virtual void Delete(void *mem, const char *file, int line);
  virtual void *Realloc(void *mem, size_t size);
  virtual void *Realloc(void *mem, size_t size, const char *file, int line);
  virtual void *Resize(void *mem, size_t size);

  void *NewPage(size_t size, size_t align);
  void DeletePage(void *page, size_t size);
  size_t GetPageRecommendedSize();

  void CleanUp() {if (safeHeap) safeHeap->CleanUp();}
  
	virtual void Lock() {if (safeHeap) safeHeap->Lock();}
	virtual void Unlock() {if (safeHeap) safeHeap->Unlock();}
};

MemFunctionsSafe SafeHeapFunctions INIT_PRIORITY_URGENT;

MemFunctions *GSafeMemFunctions = &SafeHeapFunctions;

MemHeapLocked::MemHeapLocked(
  const char *name, MemSize size, MemSize align,
  MemHeapLocked **lateDestructPointer
)
: base(name,size,align,(MemHeap **)lateDestructPointer)
{
}
MemHeapLocked::MemHeapLocked()
{

}

#if _DEBUG || _PROFILE
static DWORD BPThread=GetCurrentThreadId(); // do only for the main thread
static int BPCount;
int AllowBPMemLock;
void StartBPScope()
{
   BPCount = 0;
}
#endif

void MemHeapLocked::Lock() const
{
  #if _DEBUG || _PROFILE
  if (AllowBPMemLock>0)
  {
    // detected interesting scope
    PROFILE_SCOPE_EX(memLS, mem);
    _lock.Lock();
    if (GetCurrentThreadId()==BPThread)
    {
      BPCount++;
      static int BPOver = 1000;
      if (BPCount>BPOver)
      {
        // next breakpoint at higher limit
        BPOver += 1000;
        __asm nop;
      }
    }
  }
  else
  #endif
  {
    // default handling
    PROFILE_SCOPE_EX(memLo, mem);
    _lock.Lock();
  
  }
}

/** AddRef/Release used only from the main thread, to constuct the global object */

int MemHeapLocked::AddRef() const {AssertThread();return base::AddRef();}
int MemHeapLocked::Release() const {AssertThread();return base::Release();}

bool MemFunctionsSafe::Create()
{
    if ( safeHeap ) return true;
    safeHeap = new MemHeapLocked;
    safeHeap->SetVirtualSpaceMultiplier(1);
    safeHeap->Init("SafeHeap",NET_HEAP_SIZE<<20); // X MB of memory
#ifdef NET_LOG_SAFE_HEAP
    NetLog("Initializing SafeHeap: %d MB of memory",NET_HEAP_SIZE);
#endif
    return true;
}

void MemFunctionsSafe::Destroy ()
{
    if ( !safeHeap ) return;
    safeHeap = NULL;
}

void SafeHeapBeginUse()
{
  if (safeHeap) safeHeap->BeginUse(2*1024*1024);
  FreeOnDemandGarbageCollect(1024*1024,256*1024);
}
void SafeHeapEndUse()
{
  if (safeHeap) safeHeap->EndUse();
}

// enable this once PrepareAlloc/FinishAlloc/PrepareFree is MT safe
#define MEM_CHECK_MT MEM_CHECK

void *MemFunctionsSafe::New(size_t size)
{
  if ( !safeHeap )
  {
    Verify( Create() );
  }

  CountNew("nofile",0,size);
  #if MEM_CHECK_MT
    size=PrepareAlloc(size);
  #endif

  void *ret = safeHeap->Alloc(size);
  while (!ret)
  {
    if (safeHeap->FreeOnDemandLowLevel(size)==0)
    {
      Fail("Out of MP memory");
      return NULL;
    }
    ret = safeHeap->Alloc(size);
  }

  #if MEM_CHECK_MT
    ret=FinishAlloc(ret,size,"",0);
  #endif
  DoAssert( ret );
#ifdef NET_LOG_SAFE_HEAP
  //if ( !(++safeHeapCounter & 0xff) )
  NetLog("safeNew (%s): %6u bytes, %u bytes remains (in %u blocks)",
    mem?"success":"fail",(unsigned)size,(unsigned)safeHeap->TotalFreeLeft(),(unsigned)safeHeap->CountFreeLeft());
#endif
  return ret;
}
void *MemFunctionsSafe::New(size_t size, const char *file, int line)
{
  if ( !safeHeap )
  {
    Verify( Create() );
  }

  CountNew(file,line,size);
  #if MEM_CHECK_MT
    size=PrepareAlloc(size);
  #endif

  void *ret = safeHeap->Alloc(size);
  while (!ret)
  {
    if (safeHeap->FreeOnDemandLowLevel(size)==0)
    {
      Fail("Out of MP memory");
      return NULL;
    }
    ret = safeHeap->Alloc(size);
  }

  #if MEM_CHECK_MT
    ret=FinishAlloc(ret,size,file,line);
  #endif
  DoAssert( ret );
#ifdef NET_LOG_SAFE_HEAP
  //if ( !(++safeHeapCounter & 0xff) )
  NetLog("safeNew (%s): %6u bytes, %u bytes remains (in %u blocks)",
    mem?"success":"fail",(unsigned)size,(unsigned)safeHeap->TotalFreeLeft(),(unsigned)safeHeap->CountFreeLeft());
#endif
  return ret;
}

void MemFunctionsSafe::Delete(void *mem)
{
  if (!mem) return;
  Assert( safeHeap );
  #if MEM_CHECK_MT
    mem=PrepareFree(mem);
  #endif
  safeHeap->Free(mem);
#ifdef NET_LOG_SAFE_HEAP
    //if ( !(++safeHeapCounter & 0xff) )
        NetLog("safeDelete:                      %u bytes remains (in %u blocks)",
               (unsigned)safeHeap->TotalFreeLeft(),(unsigned)safeHeap->CountFreeLeft());
#endif
}
void MemFunctionsSafe::Delete(void *mem, const char *file, int line)
{
  Delete(mem);
}

void *MemFunctionsSafe::Realloc(void *mem, size_t size)
{
  if ( !safeHeap ) { Verify( Create() ); }
  // get size of the old memory block?
  size_t oldSize = 0;
  if (mem)
  {
    #if MEM_CHECK
    oldSize = GetRealSize(mem);
    #else
    oldSize = BMemory->GetSize(mem);
    #endif
  }

  if (oldSize==size)
  {
    // if size is the same, there is no need to realloc
    return mem;
  }
  // realloc
  void *newmem = New(size);

  if (mem && newmem)
  {
    // move memory as necessary
    size_t copy = oldSize;
    if (copy>size) copy = size;
    memcpy(newmem,mem,copy);

    Delete(mem);
  }
  return newmem;
}

void *MemFunctionsSafe::Resize(void *mem, size_t size)
{
  // TODO: adapt from MemTableFunctions::Resize
  return NULL;
}

void *MemFunctionsSafe::Realloc(void *mem, size_t size, const char *file, int line)
{
  if ( !safeHeap ) { Verify( Create() ); }

  // get size of the old memory block?
  size_t oldSize = 0;
  if (mem)
  {
    #if MEM_CHECK
    oldSize = GetRealSize(mem);
    #else
    oldSize = BMemory->GetSize(mem);
    #endif
  }

  if (oldSize==size)
  {
    // if size is the same, there is no need to realloc
    return mem;
  }
  // realloc
  void *newmem = New(size,"_ogg_realloc",0);

  if (mem && newmem)
  {
    // move memory as necessary
    size_t copy = oldSize;
    if (copy>size) copy = size;
    memcpy(newmem,mem,copy);

    Delete(mem);
  }
  return newmem;
}

void *MemFunctionsSafe::NewPage(size_t size, size_t align)
{
  if ( !safeHeap ) { Verify( Create() ); }
  return safeHeap->AllocRegion(size,align);
}
void MemFunctionsSafe::DeletePage(void *page, size_t size)
{
  safeHeap->FreeRegion(page,size);
}

size_t MemFunctionsSafe::GetPageRecommendedSize()
{
  if ( !safeHeap ) {
      Verify( Create() );
      }
  return safeHeap->GetRegionRecommendedSize();
}


#else

void *safeNew ( size_t size )
{
    return malloc(size);
}

void *safeNew ( size_t size, const char *file, int line )
{
    free(mem);
}

void safeDelete ( void *mem )
{
    free(mem);
}

#endif

#endif

#if _USE_DEAD_LOCK_DETECTOR
//Global DeadLockDetector variable
DeadLockDetector GDeadLockDetector;
#endif

#include "Es/essencepch.hpp"
#include "Es/Framework/netlog.hpp"
// no netlog in Xbox retail
#if _ENABLE_REPORT || !defined(_XBOX)
  #if defined(NET_LOG) && defined(EXTERN_NET_LOG)
  #  ifdef NET_LOG_PERIOD
  NetLogger netLogger(NET_LOG_PERIOD);
  #  else
  NetLogger netLogger;
  #  endif
  #endif
#endif

#else

MemFunctions OMemFunctions INIT_PRIORITY_URGENT;
MemFunctions OSafeMemFunctions INIT_PRIORITY_URGENT;
MemFunctions *GMemFunctions = &OMemFunctions;
MemFunctions *GSafeMemFunctions = &OSafeMemFunctions;

// no garbage collect or any on demand releasing with default new

void RegisterFreeOnDemandMemory(IMemoryFreeOnDemand *object)
{
}

size_t MemoryFreeOnDemand(size_t size)
{
  return 0;
}

void FreeOnDemandGarbageCollectMain(size_t freeRequired, size_t freeSysRequired, bool isMain)
{
}

mem_size_t GetMemoryUsageLimit(mem_size_t *virtualLimit)
{
  if (virtualLimit) *virtualLimit = INT_MAX;
  return INT_MAX; // no limit - 2GB allowed
}

int FrameId=0;

void FreeOnDemandFrame()
{
  FrameId++;
}

void RegisterFreeOnDemandSystemMemory(IMemoryFreeOnDemand *object)
{
}

void RegisterFreeOnDemandLowLevelMemory(IMemoryFreeOnDemand *obj)
{
}

void FreeOnDemandGarbageCollectSystemMemoryLowLevel(size_t freeSysRequired)
{
}

size_t FreeOnDemandSystemMemoryLowLevel(size_t size)
{
  return 0;
}

size_t FreeOnDemandSystemMemory(
  size_t size, IMemoryFreeOnDemand **extras, int nExtras
)
{
  return 0;
}

void MemoryInit(){}
void MemoryDone(){}
void MemoryFootprint(){}
void PrintVMMap(int extended, const char *title=NULL){}

#endif

#ifdef _XBOX


LPVOID WINAPI XMemAlloc(SIZE_T dwSize, DWORD dwAllocAttributes)
{
  Retry:
  LPVOID mem = XMemAllocDefault(dwSize,dwAllocAttributes);
  if (!mem)
  {
    // we should try releasing some low-level memory
    // note: FreeOnDemandSystemMemoryLowLevel is thread safe
    size_t released = FreeOnDemandSystemMemoryLowLevel(dwSize);
    if (released>0)
    {
      //LogF("XMemAlloc: low-level released %d, requested %d",released,dwSize);
      goto Retry;
    }
    ErrF("XMemAlloc %d failed",dwSize);
    return mem;
  }
  
  #if _ENABLE_CHEATS
    XALLOC_ATTRIBUTES attr = *(XALLOC_ATTRIBUTES *)&dwAllocAttributes;
    SIZE_T memSize = XMemSizeDefault(mem,dwAllocAttributes);
    // lock access to memory tracking stats
    ScopeLock<CriticalSection> lock(XMemoryLock);
    
    int *totAlloc = NULL;
    #if _ENABLE_CHEATS
    int *idAlloc = NULL;
    const char *name = "";
    #endif
    if (attr.dwMemoryType==XALLOC_MEMTYPE_PHYSICAL)
    {
      totAlloc = &XPhysMemoryAllocated;
      #if _ENABLE_CHEATS
      idAlloc = XPhysMemoryAllocatedById;
      name = "Phys";
      #endif
    }
    else
    {
      totAlloc = &XVirtMemoryAllocated;
      #if _ENABLE_CHEATS
      idAlloc = XVirtMemoryAllocatedById;
      name = "Virt";
      #endif
    }
    *totAlloc += memSize;
    #if _ENABLE_CHEATS
      int i = attr.dwAllocatorId-firstTrackedXAId;
      //LogF("Alloc - Memory %s:%d  - %d",name,i,memSize);
      if (i>=0 && i<nTrackedXAIds)
      {
        idAlloc[i]+= memSize;
      }
    #endif
  #endif
  
  return mem;
}

VOID WINAPI XMemFree(PVOID pAddress, DWORD dwAllocAttributes)
{
  // X360 calls XMemFree with NULL during init phase
  if (pAddress==NULL) return;
  #if _ENABLE_CHEATS
  
  XALLOC_ATTRIBUTES attr = *(XALLOC_ATTRIBUTES *)&dwAllocAttributes;

  SIZE_T memSize = XMemSizeDefault(pAddress,dwAllocAttributes);
  
  int *totAlloc = NULL;
  #if _ENABLE_CHEATS
  int *idAlloc = NULL;
  const char *name = "";
  #endif
  ScopeLock<CriticalSection> lock(XMemoryLock);
  if (attr.dwMemoryType==XALLOC_MEMTYPE_PHYSICAL)
  {
    totAlloc = &XPhysMemoryAllocated;
    #if _ENABLE_CHEATS
    idAlloc = XPhysMemoryAllocatedById;
    name = "Phys";
    #endif
  }
  else
  {
    totAlloc = &XVirtMemoryAllocated;
    #if _ENABLE_CHEATS
    idAlloc = XVirtMemoryAllocatedById;
    name = "Virt";
    #endif
  }

  *totAlloc -= memSize;
  if (*totAlloc<0)
  {    
    #if _ENABLE_CHEATS
      LogF("Memory %s underflow %d",name,memSize);
    #endif
    *totAlloc = 0;
  }
  #if _ENABLE_CHEATS
    int i = attr.dwAllocatorId-firstTrackedXAId;
    //LogF("Free  - Memory %s:%d  - %d",name,i,memSize);
    if (i>=0 && i<=nTrackedXAIds)
    {
      idAlloc[i] -= memSize;
      if (idAlloc[i]<0)
      {
        LogF("Memory %s:%d underflow %d",name,i,memSize);
        idAlloc[i] = 0;
      }
    }
  #endif
  
  #endif
  return XMemFreeDefault(pAddress,dwAllocAttributes);
}

SIZE_T WINAPI XMemSize(PVOID pAddress, DWORD dwAllocAttributes)
{
  return XMemSizeDefault(pAddress,dwAllocAttributes);
}
#endif

#if  _ENABLE_REPORT && !defined MFC_NEW

#if defined _XBOX

#include <El/Enum/enumNames.hpp>

extern const EnumName XALLOCEnumNames[];

template <>
const EnumName *GetEnumNames(XALLOC_ALLOCATOR_IDS dummy)
{
  return XALLOCEnumNames;
}

#endif

RString GetMemStat(mem_size_t statId, mem_size_t &statVal)
{
  int id = 0; // compiler will eliminate the ++ done on this into constants
  #ifdef _XBOX
  //int nXALLOCEnumNames = sizeof(XALLOCEnumNames)/sizeof(*XALLOCEnumNames)-1;
  if (statId<(id+=nTrackedXAIds))
  {
    // no lock here:
    // we are only reading, no need for a proper synchronization - we do not mind if the value is a little bit old
    int i = (statId-(id-nTrackedXAIds)); // id now points at the end of the region, need -nTrackedXAIds
    DoAssert(i>=0 && i<nTrackedXAIds);
    statVal = XPhysMemoryAllocatedById[i];
    return RString("P ")+XALLOCEnumNames[i].GetName();
  }
  #endif
  #if 0
  if (statId==id++)
  {
    // no lock here:
    statVal = XPhysMemoryAllocated;
    return "** XPhys";
  }
  if (statId==id++)
  {
    statVal = 0;
    return " ";
  }
  #endif
  #ifdef _XBOX
  if (statId<(id+=nTrackedXAIds))
  {
    // no lock here:
    int i = statId-(id-nTrackedXAIds); // id now points at the end of the region, need -nTrackedXAIds
    DoAssert(i>=0 && i<nTrackedXAIds);
    statVal = XVirtMemoryAllocatedById[i];
    return RString("V ")+XALLOCEnumNames[i].GetName();
  }
  #endif
  #if 0
  if (statId==id++)
  {
    // no lock here:
    statVal = XVirtMemoryAllocated;
    return "** XVirt";
  }
  #endif
  if (statId==id++)
  {
    statVal = 0;
    return " ";
  }
  if (statId==id++)
  {
    statVal = MemoryUsed();
    return "MemAlloc";
  }
  if (statId==id++)
  {
    statVal = MemoryUsedByNew();
    return "MemOvhead";
  }
  if (statId==id++)
  {
    statVal = MemoryFreeBlocks();
    return "MemFragm";
  }
  if (statId==id++)
  {
    statVal = MemoryCommited();
    return "MemCommit";
  }
  if (statId==id++)
  {
    statVal = BMemory->CommittedByPages();
    return "MemCommit (Page)";
  }
  if (statId==id++)
  {
    statVal = BMemory->CommittedByNew();
    return "MemCommit (New)";
  }
  if (statId==id++)
  {
    statVal = BMemory->CommittedByDirect();
    return "MemCommit (Direct)";
  }
  if (statId==id++)
  {
    statVal = BMemory->LongestFreeForPages();
    return "MemLongFree (Page)";
  }
  if (statId==id++)
  {
    statVal = BMemory->LongestFreeForNew();
    return "MemLongFree (New)";
  }
  if (statId==id++)
  {
    statVal = MemoryStoreUsed();
    return "Memory Store";
  }
  if (statId==id++)
  {
    statVal = GMemStore.GetMappedPages()*GMemStore.GetPageSize();
    return "Mapped Store";
  }
  if (statId==id++)
  {
    statVal = safeHeap->TotalReserved();
    return "MP MemResvd";
  }
  if (statId==id++)
  {
    statVal = safeHeap->TotalCommitted();
    return "MP MemCommit";
  }
  if (statId==id++)
  {
    statVal = safeHeap->TotalAllocated();
    return "MP MemAlloc";
  }
  if (statId==id++)
  {
    statVal = 0;
    return " ";
  }
  if (statId==id++)
  {
    MEMORYSTATUS mem;
    mem.dwLength = sizeof(mem);
    GlobalMemoryStatus(&mem);
    statVal = mem.dwAvailVirtual;
    return "Virtual Free";
  }
  if (statId==id++)
  {
    MEMORYSTATUS mem;
    mem.dwLength = sizeof(mem);
    GlobalMemoryStatus(&mem);
    statVal = mem.dwAvailPhys;
    return "Physical Free";
  }

  
  statVal = 0;
  return RString();
}
#endif

#if _ENABLE_REPORT && !defined MFC_NEW
void ReportMemoryTotals()
{
  FastCAlloc::ReportTotals();
  LogF("Fastalloc requested %d",FastCAlloc::TotalRequested());
  LogF("Fastalloc allocated %d",FastCAlloc::TotalAllocated());
  if (BMemory)
  {
    BMemory->ReportTotals();
    BMemory->ReportAnyMemory("");
    // report all basic memory stats
    for( int c=0; c<100; c++ )
    {
      int value;
      RString name = GetMemStat(c,value);
      if (name.GetLength()==0) break;
      if (value==0) continue;
      LogF("%s - %d",(const char *)name,value);
    }
  }
#ifdef _XBOX
  DM_MEMORY_STATISTICS stats;
  stats.cbSize = sizeof(stats);
  DmQueryMemoryStatistics(&stats);
  #define LOG_DMS(x) LogF(#x " %d",stats.x##Pages*4096);
  LOG_DMS(Total);
  LOG_DMS(Available);
  LOG_DMS(Stack);
  LOG_DMS(VirtualPageTable);
  LOG_DMS(SystemPageTable);
  LOG_DMS(Pool);
  LOG_DMS(VirtualMapped);
  LOG_DMS(Image);
  LOG_DMS(FileCache);
  LOG_DMS(Contiguous);
  LOG_DMS(Debugger);
  #undef LOG_DMS
#endif
}
#else
void ReportMemoryTotals()
{
}
#endif

// MT safe heap - equal to GMemFunctions or GSafeMemFunctions depending on configuration
#if MAIN_HEAP_MT_SAFE
  MemFunctions *GMTMemFunctions = GMemFunctions; 
#else
  MemFunctions *GMTMemFunctions = GSafeMemFunctions; 
#endif

size_t TotalAllocatedWin()
{
#ifdef MFC_NEW
  return 0;
#else
  if( !BMemory ) return 0;
  return BMemory->TotalAllocated();
#endif
}

#endif // enable / disable whole implementation

#endif //_win32

