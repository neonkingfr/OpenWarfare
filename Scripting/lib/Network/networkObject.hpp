#ifdef _MSC_VER
#pragma once
#endif

#ifndef _NETWORK_OBJECT_HPP
#define _NETWORK_OBJECT_HPP


/*!
\file
Interface file for base objects transferred over network in multiplayer game
*/

#include "../paramArchiveExt.hpp"
#include <El/QStream/qStream.hpp>
#include <El/Time/time.hpp>

#include "../objLink.hpp"

#include "messageFactory.hpp"

#include "../cast.hpp"

class NetworkMessageContext;
class NetworkMessageContextWithError;
class NetworkMessageFormatBase;
class NetworkMessageFormat;
DECL_ENUM(TMError)
DECL_ENUM(NetworkMessageType)

//! message update class for objects
enum NetworkMessageClass
{
  NMCCreate = -1,
  NMCUpdateFirst = 0,	// update values are used as index into array
  NMCUpdateGeneric = NMCUpdateFirst,
  NMCUpdatePosition,
  NMCUpdateDamage, // dammage and hit update
  NMCUpdateN
};

#define REF_MSG(type) Ref<NetworkMessage>
#define REF_MSG_ARRAY(type) RefArray<NetworkMessage>

#ifndef DECL_ENUM_NETWORK_MESSAGE_ERROR_TYPE
#define DECL_ENUM_NETWORK_MESSAGE_ERROR_TYPE
DECL_ENUM(NetworkMessageErrorType)
#endif

struct NetworkMessageFormatItem;
struct NetworkUpdateInfo;
struct NetworkObjectInfo;
struct NetworkPlayerObjectInfo;

class AITeamMember;

//! Network message (internal representation)
struct NetworkMessage : public RefCount, public SerializeClass
{
  // network message header
  //! time when message was sent
  Time time;
  //! size of raw (serialized, compressed) message - not transferred, used for statistics
  int size;	

  //! Temporary pointer to update info - used for update object info whenever message is sent
  NetworkUpdateInfo *objectUpdateInfo;

  //! Temporary pointer (OnSendComplete) - used on server only
  NetworkObjectInfo *objectServerInfo;

  //! Temporary pointer (OnSendComplete) - used on server only
  NetworkPlayerObjectInfo *objectPlayerInfo;

  //! Constructor
  NetworkMessage()
  {
    size = 0;
    objectUpdateInfo = NULL;
    objectServerInfo= NULL;
    objectPlayerInfo= NULL;
  }
  //! Destructor
  ~NetworkMessage() {}
  //! Type of message
  virtual NetworkMessageType GetNMType() const;
  //! Error calculation for server
  virtual float CalculateErrorStatic(NetworkMessage *with) = 0;
  //! Error calculation for server
  virtual float CalculateErrorDynamic(NetworkMessage *with, float dt) = 0;
  //! Return the size of the structure (to check structure runtime)
  virtual size_t GetStructureSize() const = 0;
  virtual bool IsSerializable() const
  {
    return false; //not serializable by default
  }
  //! Save in MP game should save NetworkServer::_initMessages
  virtual LSError Serialize(ParamArchive &ar)
  {
    // All possible init messages should overload this method
    Fail("Only selected NetworkMessages can be serialized!");
    return LSOK;
  }
  static NetworkMessage *CreateObject(ParamArchive &ar);
};

/** function is pure, but still derived classes may call base implementation */
inline float NetworkMessage::CalculateErrorStatic(NetworkMessage *with)
{
  return 0;
}
/** function is pure, but still derived classes may call base implementation */
inline float NetworkMessage::CalculateErrorDynamic(NetworkMessage *with, float dt)
{
  return 0;
}

#include <Es/Strings/rString.hpp>

//! simple network object
/*!
- used as base class for guaranteed message structures
*/
class NetworkSimpleObject
{
public:
  //! Destructor
  virtual ~NetworkSimpleObject() {}
  //! Returns message type for this object and message class
  /*!
    \param cls messsage update class
  */
  virtual NetworkMessageType GetNMType(NetworkMessageClass cls = NMCCreate) const = 0;
  //! Create new network message for this object and message class
  /*!
  \param cls messsage update class
  */
  static NetworkMessageFormat &CreateFormat(
    NetworkMessageClass cls,
    NetworkMessageFormat &format
  ) {return format;}
  //! Message update from / to this object
  /*!
    \param ctx message context
  */
  virtual TMError TransferMsg(NetworkMessageContext &ctx) = 0;

  virtual Time GetLastSimulationTime() const;
};

/** function is pure, but still derived classes may call base implementation */
inline TMError NetworkSimpleObject::TransferMsg(NetworkMessageContext &ctx)
{
  return (TMError)0;
}

/// rename NetworkSimpleObject using common naming structure
typedef NetworkSimpleObject SimpleMessage;

/// rename NetworkMessage using common naming structure
typedef NetworkMessage NetworkMessageSimple;


//! unique identifier of network objects
struct NetworkId
{
  //! Player id of client where network object was created
  int creator;
  //! Unique id of network object on given client
  int id;

	//! Constructor
	NetworkId() {creator = 0; id = 0;}
	//! Constructor
	/*!
	\param c player id of client where network object was created
	\param i unique id of network object on given client
	*/
	NetworkId(int c, int i) {creator = c; id = i;}
	//! Assignement operator
	void operator =(const NetworkId &src)
	{
		creator = src.creator;
		id = src.id;
	}
	//! Equality operator
	bool operator ==(const NetworkId &with) const
	{
		return with.creator == creator && with.id == id;
	}
	//! Nonequality operator
	bool operator !=(const NetworkId &with) const
	{
		return with.creator != creator || with.id != id;
	}
	
	//! Null (unassigned) network id
	static NetworkId Null() {return NetworkId();}
	//! Check if id value is null (unassigned)
	bool IsNull() const {return creator == 0;}

  //! Serialize for MP Save/Load
  LSError Serialize(ParamArchive &ar, int minVersion)
  {
    CHECK(ar.Serialize("creator", creator, minVersion, 0))
    CHECK(ar.Serialize("id", id, minVersion, 0))
    return LSOK;
  }
  LSError Serialize(ParamArchive &ar, const char *name, int minVersion);
};
TypeIsMovable(NetworkId);

#define NETWORK_OBJECT_MSG(MessageName, XX) \
  XX(MessageName, int, objectCreator, NDTInteger, int, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG("ID of client, which created this object"), TRANSF) \
  XX(MessageName, int, objectId, NDTInteger, int, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG("Unique (for client) ID of object"), TRANSF) \
  XX(MessageName, Vector3, objectPosition, NDTVector, Vector3, NCTVectorPositionCamera, DEFVALUE(Vector3, VZero), DOC_MSG("Current position of object"), TRANSF) \
  XX(MessageName, bool, guaranteed, NDTBool, bool, NCTNone, DEFVALUE(bool, false), DOC_MSG("Message is guaranteed (must be delivered)"), TRANSF)

DECLARE_NET_INDICES_NO_NMT(NetworkObject, NETWORK_OBJECT_MSG)

#include <El/Time/time.hpp>

//! updateable network object
/*!
  Base class for all objects with automatic update over network in multiplayer game
*/
class NetworkObject : public RemoveOLinks, public NetworkSimpleObject
{
protected:
  //! time when last position update from server arrived
  Time _lastUpdateTime;

  //! time when prediction should stop (calculated by GetMaxPredictionTime() function)
  Time _maxPredictionTime;

#if _EXT_CTRL
  int _externalController; //by HLA or AAR, see enum in hla_base.hpp
#endif

public:
  //! Constructor
  NetworkObject();

#if _EXT_CTRL
  void SetExternalController(int controller = 1) {_externalController = controller;}
  int GetExternalController() const {return _externalController;}
  bool IsExternalControlled() const {return (_externalController > 0);}
  bool AllowExternalControl(int ctrl) const {return _externalController == ctrl;}
#endif

  //! Set unique id of network object
  virtual void SetNetworkId(NetworkId &id) = 0;
  //! Return unique id of network object
  virtual NetworkId GetNetworkId() const = 0;
  //! Returns current position of this object
  virtual Vector3 GetCurrentPosition() const = 0;
  //! Returns speaker position for this object
  virtual Vector3 GetSpeakerPosition() const {return GetCurrentPosition();}
  //! Enable / disable random lip movement (enabled during direct speaking chat)
  virtual void SetRandomLip(bool set = true) {}
  //! Return if object is local on this client
  virtual bool IsLocal() const = 0;
  //! Set if object is local on this client
  virtual void SetLocal(bool local = true) = 0;
  //! Destroy (remote) object
  virtual void DestroyObject() = 0;

  //! Get debugging name (debugging only)
  virtual RString GetDebugName() const {return RString();}

  //! The object is no longer in any usage and can be deleted (used for ClientCameraPositionObject)
  virtual bool IsObsolete() const { return false; }

  //! How long is client side prediction allowed to predict this object
  virtual float GetMaxPredictionTime(NetworkMessageContext &ctx) const;

  float GetLastPositionUpdateAge() const;

  //! Check if prediction should be frozen
  virtual bool CheckPredictionFrozen() const;

  static NetworkMessageFormat &CreateFormat
  (
    NetworkMessageClass cls,
    NetworkMessageFormat &format
  );
  virtual TMError TransferMsg(NetworkMessageContext &ctx) = 0;
  //! Calculate difference between given message and current state
  /*!
    \param ctx context for given network message
    \return calculated error (difference)
  */
  virtual float CalculateError(NetworkMessageContextWithError &ctx);
  //! Calculate coeficient error must be multiplied by
  /*!
    - coeficient is 1 for near objects, for distant objects decrease to zero
    \param position current position of object
    \param cameraPosition camera position on receiving client
  */
#if _VBS3
  static float CalculateErrorCoef(Vector3Par position, Vector3Par cameraPosition, Vector3Par cameraDirection = VForwardP, float cameraFov = 0.75 );
#else
  static float CalculateErrorCoef(Vector3Par position, Vector3Par cameraPosition);
#endif

  // soft link support
  //! id type
  typedef ObjectId LinkId;
  typedef ObjectIdAutoDestroy LinkIdStorage;

  //! load object that was unloaded
  static NetworkObject *RestoreLink(const LinkId &id);
  static bool RequestRestoreLink(const LinkId &id, bool noRequest=false);

  static LinkId GetNullId();

  //! get unique ID (for unloading purposed)
  virtual LinkId GetLinkId() const {return GetNullId();}

  //
  template <class Type,class Traits>
  static LSError LoadPtrRef(ParamArchive &ar, Type * &link)
  {
    link = dynamic_cast<Type *>(Type::LoadRef(ar));
    return LSOK;
  }
  template <class Type,class Traits>
  static LSError SavePtrRef(ParamArchive &ar,  Type * &link)
  {
    return link->SaveRef(ar);
  }

  //
  template <class Type>
  static LSError LoadRefRef(ParamArchive &ar, Ref<Type> &link)
  {
    link = dynamic_cast<Type *>(Type::LoadRef(ar));
    return LSOK;
  }
  template <class Type>
  static LSError SaveRefRef(ParamArchive &ar, Ref<Type> &link)
  {
    return link->SaveRef(ar);
  }

  //
  template <class Type>
  static LSError LoadLLinkRef(ParamArchive &ar, LLink<Type> &link)
  {
    link = dynamic_cast<Type *>(Type::LoadRef(ar));
    return LSOK;
  }
  template <class Type,class Traits>
  static LSError SaveLLinkRef(ParamArchive &ar, LLink<Type> &link)
  {
    return link->SaveRef(ar);
  }

  //
  template <class Type,class Traits>
  static LSError LoadLinkRef(ParamArchive &ar, SoftLinkPerm<Type,Traits> &link)
  {
    link = dynamic_cast<Type *>(Type::LoadRef(ar));
    return LSOK;
  }
  template <class Type,class Traits>
  static LSError SaveLinkRef(ParamArchive &ar,  SoftLinkPerm<Type,Traits> &link)
  {
    return link->SaveRef(ar);
  }

  //
  template <class Type, class Traits>
  static LSError LoadLinkRef(ParamArchive &ar, SoftLink<Type,Traits> &link);
  template <class Type, class Traits>
  static LSError SaveLinkRef(ParamArchive &ar,  SoftLink<Type,Traits> &link);

  //! lock -- disable unloading
  virtual void LockLink() const {}
  //! unlock -- allow unloading
  virtual void UnlockLink() const {}

  //! check if object can be unloaded
  virtual bool NeedsRestoring() const {return false;}

  //! check if object state is good
  virtual bool CheckIntegrity() const;

  //! return team member interface
  virtual AITeamMember *GetTeamMember() {return NULL;}

  //! serialize for MP Save/Load
  LSError Serialize(ParamArchive &ar)
  {
    if (ar.IsLoading())
    {
      NetworkId id;
      LSError retval = id.Serialize(ar, "NetworkId", 1);
      if (!id.IsNull())
        SetNetworkId(id);
      return retval;
    }
    else return GetNetworkId().Serialize(ar, "NetworkId", 1);
  }
  
  USE_CASTING_ROOT;
};

#endif
