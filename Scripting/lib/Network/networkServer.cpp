#include "../wpch.hpp"
#include "networkImpl.hpp"
#include "../global.hpp"
//#include "strIncl.hpp"
#include "../stringtableExt.hpp"

#include "../arcadeTemplate.hpp"
#include "../AI/ai.hpp"
#include "../world.hpp"
#include "../landscape.hpp"
#include "../UI/chat.hpp"
#include "../UI/uiMap.hpp"
#include "../UI/missionDirs.hpp"
#include <El/XML/xml.hpp>

#if defined _WIN32 && !defined _XBOX
  #include <dxerr9.h>
  #pragma comment(lib,"dxerr9")
#endif

#include <Es/Algorithms/qsort.hpp>
#include <Es/Files/filenames.hpp>
#include <Es/Files/fileContainer.hpp>

#include "../allAIVehicles.hpp"
#include "../seaGull.hpp"
#include "../detector.hpp"
#include "../shots.hpp"
#include "../fileLocator.hpp"
#include "../gameDirs.hpp"

#include <El/Debugging/debugTrap.hpp>
#include <El/Common/perfProf.hpp>

#include <El/QStream/qbStream.hpp>
#include <El/QStream/packFiles.hpp>
#include <El/FileServer/fileServer.hpp>

#include <El/Evaluator/express.hpp>

#include <El/Common/randomGen.hpp>
#include "../gameStateExt.hpp"

#include "../progress.hpp"
#include <El/Common/perfLog.hpp>

#include "../keyInput.hpp"
#include "../dikCodes.h"
#include "../UI/uiActions.hpp"

#include <El/CRC/crc.hpp>
#include <Es/Strings/bString.hpp>

#include "../saveGame.hpp"
#include "../saveVersion.hpp"

#include "liveStats.hpp"
#include <El/ParamArchive/paramArchiveDb.hpp>

#ifdef _WIN32
  #include <io.h>
#endif

#if defined _WIN32 && !defined _XBOX
  #if _ENABLE_MP
    #include <winsock2.h>
  #endif
  #if _ENABLE_DEDICATED_SERVER
    #include <process.h>
  #endif
#endif

#include "El/Speech/debugVoN.hpp"

#if _EXT_CTRL
  #include "../hla/HLA_base.hpp"
  #include "../HLA/AAR.hpp"
#endif
/*!
\file
Basic implementation file for network server
*/

//((EXE_CRC_CHECK
// random integrity check
#if _SUPER_RELEASE
  #define _ENABLE_RANDOM_INTEGRITY_CHECK 1
#else
  #define _ENABLE_RANDOM_INTEGRITY_CHECK 0
#endif
//))EXE_CRC_CHECK

#define LOG_SEND_PROCESS 0

#define LOG_PLAYERS 1

extern const char *GameStateNames[];
extern int MaxCustomFileSize;

#define QOS_SERVER_BANDWIDTH  4096

//! enable diagnostic logs of message errors
#define LOG_ERRORS        0
//! limit for diagnostic logs of message errors
static float LogErrorLimit = 1.0f;

//! Server controlled pool of weapons available before start of MP game
static FixedItemsPool GWeaponsPool;

extern GameValue GNetworkPlayerConnected;
extern GameValue GNetworkPlayerDisconnected;

RStringB GetExtensionWizardMission();

/// generate a 32-bit random number (Implementation of rand() in MSVC returns only 15 bits)
int rand32()
{
  return (rand() & 0x7fff) | (rand() & 0x7fff) << 15 | (rand() & 0x0003) << 30;

}

/*!
\patch 5257 Date 7/15/2008 by Bebul
- Fixed: Players connecting JIP with custom files do not cause server lagging.
\patch 5257 Date 7/15/2008 by Bebul
- Fixed: Players with custom files get no longer kicked when server does not run on default port.
*/
static RString GetServerPath(RString src)
{
  if (src.GetLength() == 0) return src;
  if (src[0] == '$')
  { // translate char '$' to Local Settings directory
    char dir[MAX_PATH];
    if (!GetLocalSettingsDir(dir)) return src.Substring(2, INT_MAX); // place in current directory
    TerminateBy(dir, '\\');
    return RString(dir) + src.Substring(2, INT_MAX);
  }
  else if (src[0] == '#') 
  { // translate char '#' to Local Settings directory + ServerTmpDir
    return GetServerTmpDir() + src.Substring(1, INT_MAX); // place in current directory
  }
  else return src;
}

/*!
  \patch 5088 Date 11/06/2006 by Ondra
  - Fixed: Possible crash when exiting multiplayer game.
*/

void ClearNetServer()
{
  GWeaponsPool.Clear();
}

// Moved, declarations needed in BE

//{ QUERY & REPORTING SDK
#if _ENABLE_GAMESPY

#define REPORT_ASE  0

#include "../GameSpy/qr2/qr2.h"
#include "../GameSpy/natneg/natneg.h"
#if _VERIFY_CLIENT_KEY
#include "../GameSpy/gcdkey/gcdkeys.h"
#endif

#if REPORT_ASE
extern "C"
{
#include "../ASE/ASEMasterSDK.h"
}
#endif

char *GetGameName()
{
  // GameSpy game name
#if _LIBERATION
  return "IFpc";
#elif _DEMO
  return "arma2oapcd";
#else
  return "arma2oapc";
#endif
}

char *GetSecretKey()
{
  // GameSpy secret key
  static char secret_key[9];

  //set the secret key, in a semi-obfuscated manner
  secret_key[0] = 's';
  secret_key[1] = 'G';
  secret_key[2] = 'K';
  secret_key[3] = 'W';
  secret_key[4] = 'i';
  secret_key[5] = 'k';
  secret_key[6] = '\0';

  return secret_key;
}

int GetGameID()
{
  // GameSpy game id
#if _LIBERATION
  return 4077;
#elif _DEMO
  return 3047;
#else
  return 3045;
#endif
}

int GetProductID()
{
  // GameSpy product id
#if _LIBERATION
  return 13905;
#elif _DEMO
  return 12576;
#else
  return 12576;
#endif
}

#endif
//}

DEFINE_FAST_ALLOCATOR(NetworkPlayerObjectInfo)

NetworkPlayerObjectInfo *NetworkObjectInfo::GetPlayerObjectInfo(int player)
{
  for (int i=0; i<playerObjects.Size(); i++)
  {
    NetworkPlayerObjectInfo *info = playerObjects[i];
    if (info->player == player) return info;
  }
  return NULL;
}

NetworkPlayerObjectInfo *NetworkObjectInfo::CreatePlayerObjectInfo(int player)
{
  NetworkPlayerObjectInfo *info = GetPlayerObjectInfo(player);
  if (info) return info;

  int index = playerObjects.Add();
  playerObjects[index] = new NetworkPlayerObjectInfo;
  NetworkPlayerObjectInfo &poInfo = *playerObjects[index];
  poInfo.player = player;
  for (int j=NMCUpdateFirst; j<NMCUpdateN; j++)
  {
    NetworkUpdateInfo &info = poInfo.updates[j];
    info.lastCreatedMsgId = 0xFFFFFFFF;
    info.lastCreatedMsgTime = 0;
    info.errorValid = false;
  }
  return &poInfo;
}

void NetworkObjectInfo::DeletePlayerObjectInfo(int player)
{
  for (int i=0; i<playerObjects.Size(); i++)
  {
    NetworkPlayerObjectInfo &info = *playerObjects[i];
    if (info.player == player)
    {
      playerObjects.Delete(i);
      return;
    }
  }
}

#if _ENABLE_DEDICATED_SERVER
int ConsoleF(const char *format, ...);
int VConsoleF(const char *format, va_list argptr);
int ConsoleTitle(const char *format, ...);
#endif
RString GetUserDirectory();

#if _USE_BATTL_EYE_SERVER
/*!
\patch 5254 Date 5/2/2008 by Bebul
- Fixed: BattlEye issues while using server.cfg on Linux dedicated server
\patch 5276 Date 4/16/2009 by Bebul
- Fixed: Changed BattlEye integration (fixing "random" player kicks)
*/

#include <sys/stat.h>
#ifndef _WIN32
#include <dlfcn.h>
#endif

NetworkServer::BattlEye::BattlEye()
{
  CleanUp();
}

NetworkServer::BattlEye::~BattlEye()
{
  if (_initialized && _fnExit) (*_fnExit)();
  if (_library) 
  {
#ifdef _WIN32
    FreeLibrary(_library);
#else
    dlclose(_library);
#endif
    _library = NULL;
  }
}

bool GetBELaunchPath(char *launchPath)
{
#ifndef _XBOX
  RString GetBEPath();
  RString forcedPath = GetBEPath();
#else
  RString forcedPath;
#endif

  // decide the path to the "cache" from where dll is used
  if (IsRelativePath(forcedPath))
  {
    // relative path
#ifdef _WIN32
    if (!GetLocalSettingsDir(launchPath)) return false;
    TerminateBy(launchPath, '\\');
    strcat(launchPath, "BattlEye\\");
    strcat(launchPath, forcedPath);
    TerminateBy(launchPath, '\\');
#else
    strcpy(launchPath, "./expansion/battleye/");
    strcat(launchPath, forcedPath);
    CreateDirectory(launchPath, NULL);
    TerminateBy(launchPath, '/');
#endif
  }
  else
  {
    // absolute path
    strcpy(launchPath, forcedPath);
#ifdef _WIN32
    TerminateBy(launchPath, '\\');
#else
    TerminateBy(launchPath, '/');
#endif
  }

  return true;
}

#ifdef _WIN32

int NetworkServer::BattlEye::GetBEVersion(HMODULE library) const
{
  if (library)
  {
    GetVerF *getVer = (GetVerF *)GetProcAddress(library, "GetVer");
    if (getVer) return (*getVer)();
  }
  return 0;
}

// Win32 implementation
bool NetworkServer::BattlEye::Init()
{
  _initFailedReason = BEReasonOther;
  _initialized = false;
  const char *installPath = "Expansion\\BattlEye\\BEServer.dll";
  char launchPath[MAX_PATH];
  if (!GetBELaunchPath(launchPath)) return false;

  int curBEVersion = 0;
  if (QIFileFunctions::FileExists(installPath))
  {
    GFileServerFunctions->FlushReadHandle(installPath);
    // Get the BEClient.dll version from installPath first
    HMODULE installLibrary = LoadLibrary(installPath);
    int instVersion = GetBEVersion(installLibrary);
    if (installLibrary)
      FreeLibrary(installLibrary);

    CreatePath(launchPath);
    strcat(launchPath, "BEServer.dll");
    // try to load the dll from the cache
    _library = LoadLibrary(launchPath);
    if (_library)
    { // check whether the BE is updated to the last version
      curBEVersion = GetBEVersion(_library);
      if (instVersion>curBEVersion)
      {
        FreeLibrary(_library);
        _library = NULL;  // so it will be repaired from the install path
      }
    }
    if (!_library)
    {
      // install / repair the live copy from the install path
      chmod(launchPath, S_IREAD | S_IWRITE);
      unlink(launchPath);
      QIFileFunctions::Copy(installPath, launchPath);
      _library = LoadLibrary(launchPath);
      if (!_library) return false;
#if _ENABLE_DEDICATED_SERVER
      ::ConsoleF(Format("BattlEye server updated to version: %d", instVersion));
#endif
      curBEVersion = instVersion;
    }
  }
  else
  {
    // BattlEye removed, remove also from the cache
    strcat(launchPath, "BEServer.dll");
    chmod(launchPath, S_IREAD | S_IWRITE);
    unlink(launchPath);
    _initFailedReason = BEReasonNotInstalled;
    return false;
  }

  InitF *init = (InitF *)GetProcAddress(_library, "Init");
  const int minBEVerReq = 69;
  if ((curBEVersion<minBEVerReq) || !init || !(*init)( APP_VERSION_TEXT, &PrintMessage, &KickPlayer, &ExecuteCommand, &_fnExit, &_fnRun, &_fnCommand, &_fnAddPlayer, &_fnRemovePlayer))
  {
    FreeLibrary(_library);
    CleanUp();
    if (curBEVersion<minBEVerReq) RptF("Obsolete/unsupported BattlEye version, please reinstall BE!");
    return false;
  }
  _initialized = true;
  return true;
}

#else 
// Linux implementation using dynamic linking loader (dlopen)
bool NetworkServer::BattlEye::Init()
{
  _initFailedReason = BEReasonOther;
  _initialized = false;
  const char *installPath = "expansion/battleye/beserver.so"; //use lower case - as ./install converts everything!
  char launchPath[MAX_PATH];
  if (!GetBELaunchPath(launchPath)) return false;

  if (QIFileFunctions::FileExists(installPath))
  {
    GFileServerFunctions->FlushReadHandle(installPath);
    CreatePath(launchPath);
    strcat(launchPath, "beserver.so");
    // use the install copy instead
    _library = dlopen(launchPath, RTLD_LAZY);
    if (!_library)
    {
      // install / repair the live copy from the install path
      chmod(launchPath, S_IREAD | S_IWRITE);
      unlink(launchPath);
      QIFileFunctions::Copy(installPath, launchPath);
      _library = dlopen(launchPath, RTLD_LAZY);
      if (!_library) return false;
    }
  }
  else
  {
    // BattlEye removed, remove also from the cache
    strcat(launchPath, "beserver.so");
    chmod(launchPath, S_IREAD | S_IWRITE);
    unlink(launchPath);
    _initFailedReason = BEReasonNotInstalled;
    return false;
  }

  InitF *init = (InitF *)dlsym(_library, "Init");
  if (!init || !(*init)( APP_VERSION_TEXT, &PrintMessage, &KickPlayer, &ExecuteCommand, &_fnExit, &_fnRun, &_fnCommand, &_fnAddPlayer, &_fnRemovePlayer))
  {
    dlclose(_library);
    _library = NULL;
    CleanUp();
    return false;
  }
  _initialized = true;
  return true;
}
#endif 

bool NetworkServer::BattlEye::Run(const NetworkServer *server)
{
  if (!_fnRun) return true;
  if (!(*_fnRun)())
  {
    // auto-update
    if (_initialized && _fnExit) (*_fnExit)(); //call the Exit function before whenever you unload the dll, but only when the previous Init() call succeeded
#ifdef _WIN32
    FreeLibrary(_library);
    CleanUp();

    char path[MAX_PATH];
    if (!GetBELaunchPath(path)) return false;
    strcat(path, "BEServer.dll");

    chmod(path, S_IREAD | S_IWRITE);
    unlink(path);
    rename(RString(path) + RString(".new"), path);
#else
    dlclose(_library);
    CleanUp();

    char path[MAX_PATH];
    if (!GetBELaunchPath(path)) return false;
    strcat(path, "beserver.so");

    chmod(path, S_IREAD | S_IWRITE);
    unlink(path);
    rename(RString(path) + RString(".new"), path);
#endif

    if (!Init())
    {
#if _ENABLE_DEDICATED_SERVER
      ::ConsoleF("BattlEye initialization failed");
#endif
      return false;
    }

    // add all players
#if _ENABLE_GAMESPY && _VERIFY_CLIENT_KEY
    const AutoArray<PlayerIdentity> *identities = server->GetIdentities();
    if (identities)
    {
      for (int i=0; i<identities->Size(); i++)
      {
        const PlayerIdentity &identity = identities->Get(i);
        if (identity.clientState == NCSBriefingRead)
        {
          const char *hash = gcd_getkeyhash(GetGameID(), identity.dpnid);
          sockaddr_in address;
          server->_server->GetClientAddress(identity.dpnid, address);
          AddPlayer(identity.dpnid, address.sin_addr.s_addr, address.sin_port, identity.name, (void *)hash, strlen(hash));
        }
      }
    }
#endif
  }
  return true;
}

void NetworkServer::BattlEye::Command(const char *command)
{
  if (_fnCommand) (*_fnCommand)(command);
}

void NetworkServer::BattlEye::AddPlayer(int player, unsigned long address, unsigned short port, const char *name, void *guid, int guidSize)
{
  if (_fnAddPlayer) (*_fnAddPlayer)(player, address, port, name, guid, guidSize);
}

void NetworkServer::BattlEye::RemovePlayer(int player)
{
  if (_fnRemovePlayer) (*_fnRemovePlayer)(player);
}

void NetworkServer::BattlEye::PrintMessage(const char *message)
{
#if _ENABLE_DEDICATED_SERVER
  bool firstLine = true;
  const char *nl = NULL;
  while (nl = strchr(message, '\n'))
  {
    RString line(message, nl - message);
    if (firstLine)
    {
      ::ConsoleF(RString("BattlEye Server: ") + line);
      firstLine = false;
    }
    else
    {
      ::ConsoleF(RString("  ") + line);
    }
    message = nl + 1;
  }
  if (firstLine)
  {
    ::ConsoleF(RString("BattlEye Server: ") + message);
    firstLine = false;
  }
  else
  {
    ::ConsoleF(RString("  ") + message);
  }
#endif
}

void NetworkServer::BattlEye::KickPlayer(int player, const char *reason)
{
  NetworkServer *server = GNetworkManager.GetServer();
  if (server)
  {
#if _ENABLE_DEDICATED_SERVER
    PlayerIdentity *identity = server->FindIdentity(player);
    ::ConsoleF("Player %s kicked off by BattlEye: %s", identity ? cc_cast(identity->name) : "", reason);

/*
// using DisconnectMessage is not sufficient as we need the kick being immediate
// BattleEye "support" attitude: 
//   "Note that this is of course when the ArmA client does not disconnect itself, 
//    which it does when you click OK in the BE kick message box - but not before;
//    naturally the server itself has to do this to have control and not to allow
//    the player to stay on the server with some exploit"
  DisconnectMessage msg;
  msg._message = Format("Kicked off by BattlEye: %s", reason);
  server->SendMsg(player, &msg, NMFGuaranteed);
*/

  server->KickOff(player,KORBattlEye, reason);
#endif
  }
}

void NetworkServer::BattlEye::ExecuteCommand(const char *command)
{
#if _ENABLE_DEDICATED_SERVER
  NetworkClient *client = GNetworkManager.GetClient();
  if (client)
  {
    client->ProcessCommand(RString(command));
  }
#endif
}

#endif

// open the mpStatistics.log file and write the requiered statistics string
// it open and close the mpStatistics.log file each time
static void LogMPStatistics(RString str)
{
  FILE *file = NULL;
  char logFilename[MAX_PATH];
  if ( GetLocalSettingsDir(logFilename) )
  {
    CreateDirectory(logFilename, NULL);
    TerminateBy(logFilename, '\\');
    strcat(logFilename, "mpStatistics.log");
    file = fopen(logFilename, "a+");

    if (file)
    {
      fputs(cc_cast(str), file);
      fclose(file);
    }
  }
}

//! Load ban list from file
/*!
\param filename name (path) of file
\param list output ban list
*/
static void LoadBanList(RString filename, FindArray<RString> &list)
{
  list.Clear();
  QIFStream f;
  f.open(filename);
  while (!f.eof() && !f.fail())
  {
    int c = f.get();
    if (f.eof() || f.fail()) return;
    while (isspace(c) || c==',' || c==';')
    {
      c = f.get();
      if (f.eof() || f.fail()) return;
    }
    const int MaxBanIdLength = 255;
    char value[MaxBanIdLength+1];
    int ix=0;
    while (!isspace(c) && c!=',' && c!=';' && !f.eof() && ix<MaxBanIdLength)
    {
      value[ix++] = c;
      c = f.get();
    }
    value[ix] = 0;
    list.AddUnique(value);
  }
}

static void SaveBanList(RString filename, FindArray<RString> &list)
{
  QOFStream f(filename);
  for (int i=0; i<list.Size(); i++)
  {
    f.write(list[i], list[i].GetLength());
    f.put('\r');
    f.put('\n');
  }
  f.close();
}

//! Return if given message type is kind of transport update
/*!
When driver of transport changes, ownership of whole transport is changed.
*/
bool IsUpdateTransport(NetworkMessageType type)
{
  switch (type)
  {
  case NMTUpdateTransport:
  case NMTUpdateTankOrCar:
  case NMTUpdateTank:
  case NMTUpdateCar:
  case NMTUpdateMotorcycle:
  case NMTUpdateAirplane:
  case NMTUpdateHelicopter:
  case NMTUpdateParachute:
  case NMTUpdateShip:
  case NMTUpdateDoor:
    return true;
  default:
    return false;
  }
}

bool IsUpdateUnit(NetworkMessageType type)
{
  switch (type)
  {
  case NMTUpdateAIBrain:
  case NMTUpdateAIUnit:
  case NMTUpdateAIAgent:
    return true;
  default:
    return false;
  }
}


//! Find file bank with given prefix
QFBank *FindBank(const char *prefix)
{
  int prefixLen = strlen(prefix);
  for (int i=0; i<GFileBanks.Size(); i++)
  {
    QFBank &bank = GFileBanks[i];
    if (strnicmp(bank.GetPrefix(), prefix, prefixLen) == 0)
      return &bank;
  }
  return NULL;
}

#if _ENABLE_GAMESPY
#include "../GameSpy/common/gsPlatformUtil.h"
#include "../GameSpy/common/gsAvailable.h"
#endif

/*!
\patch 5140 Date 3/14/2007 by Jirka
- Fixed: Multiple dedicated servers on a single PC - wrong port was shown in the title and reported to GameSpy
*/

NetworkServer::NetworkServer(NetworkManager *parent, RString name, RString password, int maxPlayers, int port)
: NetworkComponent(parent)
#if _ENABLE_DEDICATED_SERVER
, _globals(&_evaluator, "server", false)
#endif
#if USE_NAT_NEGOTIATION_MANAGER
, _p2pManager(this)
#endif
#if _ENABLE_STEAM
, _callbackSteamServersConnected(this, &NetworkServer::OnSteamServersConnected)
, _callbackSteamServersDisconnected(this, &NetworkServer::OnSteamServersDisconnected)
, _callbackPolicyResponse(this, &NetworkServer::OnPolicyResponse)
#endif
{
  _serverState = NSSSelectingMission;
  _serverStateTime = GlobalTickCount();
  _serverTimeOutSent = INT_MAX;
  _connectionDesync = CDGood;
  _desyncTotalUpdateNext = GlobalTickCount();
  _desyncTotalAvg = 0;
  _desyncTotalMax = 0;

  _verifySignatures = false;
  _validateCDKey = false;
  _doNotReport = false;
  _qrInitDone = false;
  _persistent = false;

  _maxPlayersLimit = maxPlayers;
  _squadChecksTimeout = 0;

  _serverTime = _serverTimeOfEstimatedEnd = _secondsToEstimatedEnd = 0;
  _serverTimeInitTickCount = ::GetTickCount();
  _nextServerTimeUpdate = _serverTime + ServerTimeUpdateInterval;

#if _ENABLE_DEDICATED_SERVER
  // provide some reasonable default
  _difficulty = Glob.config.diffDefault;
#endif

#if _VBS2 && _VBS2_LITE
  _memoryWrite   = false;
  _nextRanUpdate = 0;
#endif

  _password = password.GetLength() > 0;

  _nextPlayerId = 2; // value 1 is reserved for static objects

  // make sure statistics from previous session will not be present
  GStats.ClearMission();

  Verify(Init(name, password, maxPlayers, port));

  _param1 = 0;
  _param2 = 0;
  _paramsArray.Clear();

  _timeLeftLastValue = 0;
  _timeLeftLastTime = 0;
  _timeLeftLastValueBcast = 0;

//{ DEDICATED SERVER SUPPORT
#if _ENABLE_DEDICATED_SERVER
  _kickDuplicate = false;
  _dedicated = false;
  _missionIndex = -1;

  _gameMaster = AI_PLAYER;
  _admin = false;
  _restart = false;
  _reassign = false;
  
  Monitor(0);
  _debugNext = INT_MAX;
  _debugInterval = 0;

  InitEvaluator();
#endif
//}
  _pingUpdateNext = GlobalTickCount() + 5000;
  
  _sessionLocked = false;

  _equalModRequired = false;

  // ensure no content remain
  if (_server) DeleteDirectoryStructure(GetServerTmpDir(), false);

#ifndef _XBOX
  #if _ENABLE_DEDICATED_SERVER
    if (!IsDedicatedServer())
  #endif
      LoadBanList(GetUserDirectory() + RString("ban.txt"), _banListLocal);
#endif

  _totalSlots = INT_MAX;

#if _ENABLE_GAMESPY 
  // check that the game's back-end is available
  // we need it due to NAT Negotiation
  Ref<ProgressHandle> p = ProgressStartExt(true, LocalizeString(IDS_LOAD_WORLD));
  GSIStartAvailableCheck(GetGameName());
  GSIACResult result;
  while ((result = GSIAvailableCheckThink()) == GSIACWaiting)
  {
    msleep(5);
    ProgressRefresh();
  }
  ProgressFinish(p);
#endif
  _currentSaveType = -1; //no load
  _missionWasLoadedFromSave = false;
}

void NetworkServer::CreateDebugWindow()
{
#if _ENABLE_CHEATS
  _debugWindow = new DebugListWindow("Server debug");
#endif
}

#include "../Protect/selectProtection.h"

extern const int DefaultNetworkPort;

#if _XBOX_SECURE && _ENABLE_MP

#include "..\mbcs.hpp"

DWORD GetGameLanguage()
{
  switch (GetLangID())
  {
    // supported languages
  case English:
    return XC_LANGUAGE_ENGLISH;
  case Japanese:
    return XC_LANGUAGE_JAPANESE;
  case German:
    return XC_LANGUAGE_GERMAN;
  case French:
    return XC_LANGUAGE_FRENCH;
  case Spanish:
    return XC_LANGUAGE_SPANISH;
  case Italian:
    return XC_LANGUAGE_ITALIAN;
  case Korean:
    return XC_LANGUAGE_KOREAN;
  case Chinese:
    return XC_LANGUAGE_TCHINESE;
    // return XC_LANGUAGE_SCHINESE;
  case Portuguese:
    return XC_LANGUAGE_PORTUGUESE;
  case Polish:
    return XC_LANGUAGE_POLISH;
  case Russian:
    return XC_LANGUAGE_RUSSIAN;
#ifndef _XBOX
  case Czech:
    return XC_LANGUAGE_CZECH;
  case Dutch:
    return XC_LANGUAGE_DUTCH;
  case Norwegian:
    return XC_LANGUAGE_NORWEGIAN;
#endif
  default:
    return XC_LANGUAGE_ENGLISH;
  }
}

#if _XBOX_VER >= 200

#include "../X360/ArmA2.spa.h"

void NetworkServer::XOnlineUpdateSession(bool updateAttributes, bool qosOnly)
{
  // On Xbox 360, only QoS need to updated

  if (!_server) return;
  const XNKID *sessionId = _parent->GetSessionKey();
  if (!sessionId) return;

  int maxPlayers = GetMaxPlayers();
  int maxPrivate = _server->GetMaxPrivateSlots();
  maxPlayers -= maxPrivate;

  int numPlayers = 0;
  int numPrivate = 0;

  for (int i=0; i<_players.Size(); i++)
  {
    NetworkPlayerInfo &player = _players[i];
    if (player.privateSlot && numPrivate < maxPrivate) numPrivate++;
    else numPlayers++;
  }

#if _ENABLE_DEDICATED_SERVER
  if (IsDedicatedServer())
  {
    numPlayers--;
  }
  else
#endif
  {
    if (_serverState < NSSAssigningRoles)
    {
      // disable joining to session which is not fully created (mission is not selected)
      maxPrivate = numPrivate;
      maxPlayers = numPlayers;
    }
  }

  int timeleft = GetSessionTimeLeft();

  QosInfo data;
  data.difficulty = -1;
  if (_serverState >= NSSAssigningRoles)
  {
    // TODO: use LocalizedString
    data.gameType = _missionHeader._gameType;
    data.SetMissionName(_missionHeader._name);
    data.island = _missionHeader._island;
    data.difficulty = Glob.config.difficulty;
  }
  data.serverState = _serverState;
  data.playersPublic = numPlayers;
  data.slotsPublic = maxPlayers;
  data.playersPrivate = numPrivate;
  data.slotsPrivate = maxPrivate;
  data.timeleft = timeleft;

  //DWORD flags = (maxPlayers - numPlayers) > 0 ? XNET_QOS_LISTEN_ENABLE : XNET_QOS_LISTEN_DISABLE;
  DWORD flags = XNET_QOS_LISTEN_ENABLE;
  INT err = XNetQosListen(sessionId, NULL, 0, QOS_SERVER_BANDWIDTH, flags);
  DoAssert(err == 0);

  err = XNetQosListen(sessionId, (const BYTE *)&data, sizeof(data), QOS_SERVER_BANDWIDTH, XNET_QOS_LISTEN_SET_DATA);
  DoAssert(err == 0);
}

static int GetEstimateBandwidth()
{
  int GetDSBandwidth();
  int bandwidth = GetDSBandwidth();
  if (bandwidth >= 0) return bandwidth; // value set as command line argument or by DS Settings dialog

  DWORD qosStart = GlobalTickCount();

  // estimate bandwidth by probing Live service
  XNQOS *qos = NULL;
  int qosRet = XNetQosServiceLookup(0, NULL, &qos);
  if (qosRet != ERROR_SUCCESS || !qos) return -1; // Failed

  // wait until results are ready
  ProgressScope progress(false, LocalizeString(IDS_LOAD_WORLD), "%s", true);
  while ((((volatile BYTE &)(qos->axnqosinfo[0].bFlags)) & XNET_XNQOSINFO_COMPLETE) == 0)
  {
    progress.Refresh();
  }
  DWORD qosDuration = GlobalTickCount() - qosStart;
  LogF("QoS duration %d ms", qosDuration);

  // return the minimum from up and down bandwidth
  bandwidth = min(qos->axnqosinfo[0].dwUpBitsPerSec, qos->axnqosinfo[0].dwDnBitsPerSec);
  LogF("Bandwidth detected %d kbps up",qos->axnqosinfo[0].dwUpBitsPerSec/1000);
  LogF("Bandwidth detected %d kbps down",qos->axnqosinfo[0].dwDnBitsPerSec/1000);
  
  // clean up
  XNetQosRelease(qos);
  return bandwidth;
}

#else

#include "match.h"

void NetworkServer::XOnlineInitSession()
{
  *_attrName = 0;
  *_attrMission = 0;
  *_attrIsland = 0;

  // prepare attributes
  _sessionAttributes[0].fChanged = TRUE;
  _sessionAttributes[0].dwAttributeID = XATTRIB_HOST;
  _sessionAttributes[0].info.string.lpValue = _attrName;
  _sessionAttributes[1].fChanged = TRUE;
  _sessionAttributes[1].dwAttributeID = XATTRIB_GAME_TYPE_INT;
  _sessionAttributes[1].info.integer.qwValue = -1;
  _sessionAttributes[2].fChanged = TRUE;
  _sessionAttributes[2].dwAttributeID = XATTRIB_MISSION;
  _sessionAttributes[2].info.string.lpValue = _attrMission;
  _sessionAttributes[3].fChanged = TRUE;
  _sessionAttributes[3].dwAttributeID = XATTRIB_ISLAND;
  _sessionAttributes[3].info.string.lpValue = _attrIsland;
  _sessionAttributes[4].fChanged = TRUE;
  _sessionAttributes[4].dwAttributeID = XATTRIB_SERVER_STATE;
  _sessionAttributes[4].info.integer.qwValue = NSSSelectingMission;
  _sessionAttributes[5].fChanged = TRUE;
  _sessionAttributes[5].dwAttributeID = XATTRIB_ACTUAL_VERSION;
  _sessionAttributes[5].info.integer.qwValue = MP_VERSION_ACTUAL;
  _sessionAttributes[6].fChanged = TRUE;
  _sessionAttributes[6].dwAttributeID = XATTRIB_REQUIRED_VERSION;
  _sessionAttributes[6].info.integer.qwValue = MP_VERSION_REQUIRED;
  _sessionAttributes[7].fChanged = TRUE;
  _sessionAttributes[7].dwAttributeID = XATTRIB_TIME_LEFT;
  _sessionAttributes[7].info.integer.qwValue = 0;
  _sessionAttributes[8].fChanged = TRUE;
  _sessionAttributes[8].dwAttributeID = XATTRIB_AVG_RATING;
  _sessionAttributes[8].info.integer.qwValue = 0;
  _sessionAttributes[9].fChanged = TRUE;
  _sessionAttributes[9].dwAttributeID = XATTRIB_AVG_ROUGH_RATING;
  _sessionAttributes[9].info.integer.qwValue = 0;
  _sessionAttributes[10].fChanged = TRUE;
  _sessionAttributes[10].dwAttributeID = XATTRIB_LANGUAGE;
  _sessionAttributes[10].info.integer.qwValue = GetGameLanguage();
  _sessionAttributes[11].fChanged = TRUE;
  _sessionAttributes[11].dwAttributeID = XATTRIB_DIFFICULTY;
  _sessionAttributes[11].info.integer.qwValue = -1;
}

void ToWideChar(WCHAR *buffer, int bufferSize, RString text);
void EncodeLocalizedString(WCHAR *buffer, int bufferSize, const LocalizedString &name);

static bool XOnlineRegisterServer(
  RString playerName, int maxPlayers, XNKID &kid, XNKEY &key, XONLINETASK_HANDLE &task,
  XONLINE_ATTRIBUTE *sessionAttributes, int &bandwidth
)
{
  if (!GNetworkManager.IsSignedIn()) return false;
  if (GNetworkManager.IsSystemLink()) return false;

  int numPlayers = 1;
#if _ENABLE_DEDICATED_SERVER
  if (IsDedicatedServer())
  {
    numPlayers--;
    maxPlayers--;
  }
  else
#endif
  {
    // disable joining to session which is not fully created (mission is not selected)
    maxPlayers = numPlayers;
  }

  int maxPrivate = 0;
  int numPrivate = 0;

  // prepare attributes

  // XATTRIB_HOST
  ToWideChar(sessionAttributes[0].info.string.lpValue, 16, playerName);
  
  // Register session
  HRESULT result = XOnlineMatchSessionCreate(
    numPlayers, maxPlayers - numPlayers,
    numPrivate, maxPrivate - numPrivate,
    SessionAttributesCount, sessionAttributes,
    NULL,
    &task
  );
  if (!SUCCEEDED(result))
  {
    RptF("XOnlineRegisterServer: XOnlineMatchSessionCreate failed with error 0x%x", result);
    return false;
  }

  int GetDSBandwidth();
  bandwidth = GetDSBandwidth();

  if (bandwidth < 0)
  {
    DWORD qosStart = GlobalTickCount();
    bandwidth = 256000; // by default assume 256 kbps for Live

    // estimate bandwidth by probing Live service
    XNQOS *qos = NULL;
    int qosRet = XNetQosServiceLookup(0,NULL,&qos);

    // force pumping in first loop
    result = XONLINETASK_S_RUNNING;
    do
    {
      ProgressRefresh();
      // pump task if necessary
      if (result==XONLINETASK_S_RUNNING)
      {
        result = XOnlineTaskContinue(task);
      }
      // Qos does not need to be pumped
    }
    while (
      // both QoS and task need to be complete
      result==XONLINETASK_S_RUNNING ||
      qosRet==0 &&(((volatile BYTE &)(qos->axnqosinfo[0].bFlags))&XNET_XNQOSINFO_COMPLETE)==0
      );
    DWORD qosDuration = GlobalTickCount()-qosStart;
    LogF("QoS duration %d ms",qosDuration);

    if (qosRet==0 && qos)
    {
      bandwidth = min(qos->axnqosinfo[0].dwUpBitsPerSec, qos->axnqosinfo[0].dwDnBitsPerSec);
      LogF("Bandwidth detected %d kbps up",qos->axnqosinfo[0].dwUpBitsPerSec/1000);
      LogF("Bandwidth detected %d kbps down",qos->axnqosinfo[0].dwDnBitsPerSec/1000);
    }
    if (qos)
    {
      XNetQosRelease(qos);
    }
    if (!SUCCEEDED(result))
    {
      RptF("XOnlineRegisterServer: XOnlineTaskContinue failed with error 0x%x", result);
      return false;
    }
  }
  else
  {
    do
    {
      ProgressRefresh();
      result = XOnlineTaskContinue(task);
    }
    while (result == XONLINETASK_S_RUNNING);

    if (!SUCCEEDED(result))
    {
      RptF("XOnlineRegisterServer: XOnlineTaskContinue failed with error 0x%x", result);
      return false;
    }
  }
  
  // Obtain session info
  result = XOnlineMatchSessionGetInfo
  (
    task, &kid, &key
  );
  if (!SUCCEEDED(result))
  {
    RptF("XOnlineRegisterServer: XOnlineMatchSessionGetInfo failed with error 0x%x", result);
    return false;
  }


  return true;
}

void NetworkServer::XOnlineUpdateSession(bool updateAttributes, bool qosOnly)
{
  if (!_server) return;
  // TODO: timed update (time left)

  int maxPlayers = GetMaxPlayers();
  int maxPrivate = _server->GetMaxPrivateSlots();
  maxPlayers -= maxPrivate;

  int numPlayers = 0;
  int numPrivate = 0;

  for (int i=0; i<_players.Size(); i++)
  {
    NetworkPlayerInfo &player = _players[i];
    if (player.privateSlot && numPrivate < maxPrivate) numPrivate++;
    else numPlayers++;
  }

#if _ENABLE_DEDICATED_SERVER
  if (IsDedicatedServer())
  {
    numPlayers--;
  }
  else
#endif
  {
    if (_serverState < NSSAssigningRoles)
    {
      // disable joining to session which is not fully created (mission is not selected)
      maxPrivate = numPrivate;
      maxPlayers = numPlayers;
    }
  }

  int timeleft = GetSessionTimeLeft();
  

  if (!qosOnly)
  {
    int rating = 0;
    int roughRating = 0;
    {
      int n = _identities.Size();
      float sum = 0;
      for (int i=0; i<n; i++) sum += _identities[i].rating;
      if (n > 0)
      {
        rating = toLargeInt(sum / n);
        ULONGLONG RoughRating(ULONGLONG rating);
        roughRating = RoughRating(rating);
      }
    }
    
    // prepare attributes
    if (updateAttributes)
    {
      int gameType = -1;
      if (_missionHeader._gameType.GetLength() > 0)
      {
        ConstParamEntryPtr cls = (Pars >> "CfgMPGameTypes").FindEntry(_missionHeader._gameType);
        if (cls) gameType = *cls >> "id";
      }

      *_attrMission = 0;
      *_attrIsland = 0;
      int difficulty = -1;
      if (_serverState >= NSSAssigningRoles)
      {
        EncodeLocalizedString(_attrMission,lenof(_attrMission),_missionHeader._name);
        ToWideChar(_attrIsland, lenof(_attrIsland), _missionHeader._island);
        difficulty = Glob.config.difficulty;
      }

      // XATTRIB_GAME_TYPE_INT
      // _sessionAttributes[1].fChanged = TRUE;
      _sessionAttributes[1].info.integer.qwValue = gameType;
      // XATTRIB_MISSION
      // _sessionAttributes[2].fChanged = TRUE;
      // _attrMission already attached
      // XATTRIB_ISLAND
      // _sessionAttributes[3].fChanged = TRUE;
      // _attrIsland already attached
      // XATTRIB_SERVER_STATE
      // _sessionAttributes[4].fChanged = TRUE;
      _sessionAttributes[4].info.integer.qwValue = _serverState;
      // XATTRIB_DIFFICULTY
      // _sessionAttributes[11].fChanged = TRUE;
      _sessionAttributes[11].info.integer.qwValue = difficulty;
    }
    // XATTRIB_TIME_LEFT
    _sessionAttributes[7].fChanged = TRUE;
    _sessionAttributes[7].info.integer.qwValue = timeleft;
    // XATTRIB_AVG_RATING
    _sessionAttributes[8].fChanged = TRUE;
    _sessionAttributes[8].info.integer.qwValue = rating;
    // XATTRIB_AVG_ROUGH_RATING
    _sessionAttributes[9].fChanged = TRUE;
    _sessionAttributes[9].info.integer.qwValue = roughRating;

    // Fix: Matchmaking reset values called with fChanged == FALSE

    // XATTRIB_GAME_TYPE_INT
    _sessionAttributes[1].fChanged = TRUE;
    // XATTRIB_MISSION
    _sessionAttributes[2].fChanged = TRUE;
    // XATTRIB_ISLAND
    _sessionAttributes[3].fChanged = TRUE;
    // XATTRIB_SERVER_STATE
    _sessionAttributes[4].fChanged = TRUE;
    // XATTRIB_DIFFICULTY
    _sessionAttributes[11].fChanged = TRUE;

    // Register session
    if (_sessionTask)
    {
      XOnlineTaskClose(_sessionTask);
      _sessionTask = NULL;
    }
    HRESULT result = XOnlineMatchSessionUpdate
    (
      _sessionKid,
      numPlayers, maxPlayers - numPlayers,
      numPrivate, maxPrivate - numPrivate,
      SessionAttributesCount, _sessionAttributes,
      NULL,
      &_sessionTask
    );
    if (!SUCCEEDED(result))
    {
      RptF("XOnlineUpdateSession: XOnlineMatchSessionUpdate failed with error 0x%x", result);
    }
  }

  // set data for Qos Lookup service
  QosInfo data;
  data.difficulty = -1;
  if (_serverState >= NSSAssigningRoles)
  {
    // TODO: use LocalizedString
    data.gameType = _missionHeader._gameType;
    data.SetMissionName(_missionHeader._name);
    data.island = _missionHeader._island;
    data.difficulty = Glob.config.difficulty;
  }
  data.serverState = _serverState;
  data.playersPublic = numPlayers;
  data.slotsPublic = maxPlayers;
  data.playersPrivate = numPrivate;
  data.slotsPrivate = maxPrivate;
  data.timeleft = timeleft;

  //DWORD flags = (maxPlayers - numPlayers) > 0 ? XNET_QOS_LISTEN_ENABLE : XNET_QOS_LISTEN_DISABLE;
  DWORD flags = XNET_QOS_LISTEN_ENABLE;
  INT err = XNetQosListen(&_sessionKid, NULL, 0, QOS_SERVER_BANDWIDTH, flags);
  DoAssert(err == 0);

  err = XNetQosListen(&_sessionKid, (const BYTE *)&data, sizeof(data), QOS_SERVER_BANDWIDTH, XNET_QOS_LISTEN_SET_DATA);
  DoAssert(err == 0);

}

#endif // _XBOX_VER >= 200


#else

DWORD GetGameLanguage()
{
  // use internal language ID instead (and mark it)
  int GetLangID();
  return 0x10000 | GetLangID();
}

#endif

void NetworkServer::SendMuteList(const PlayerIdentity &id)
{
#if _ENABLE_MP
  int to = id.dpnid;
  
  RemoteMuteListMessage msg;

  for (int i=0; i<_players.Size(); i++)
  {
    const NetworkPlayerInfo &info = _players[i];
    if (info.dpid == to) continue;

    for (int j=0; j<info.muteList._list.Size(); j++)
    {
      #if _XBOX_SECURE
      if (XOnlineAreUsersIdentical(info.muteList._list[j], id.xuid))
      #else
      if (info.muteList._list[j] == id.xuid)
      #endif
      {
        msg._list.Add(info.dpid);
        break;
      }
    }
  }
  SendMsg(to, &msg, NMFGuaranteed);
#endif
}

int NetworkServer::GetSessionTimeLeft() const
{
  if (_serverState == NSSPlaying)
  {
    int timeleft = 15;

    float et = _missionHeader._estimatedEndTime.toFloat();
    if (et > 0)
    {
      et -= Glob.time.toFloat();
      timeleft = toInt(et / 60.0);
      saturateMax(timeleft, 1);
    }
    return timeleft;
  }
  return 0;
}

RString GetDefaultSessionName(RString playerName)
{
  #ifndef _XBOX
  char hostName[128];
  bool getLocalName(char *name, unsigned len);
  if (getLocalName(hostName, 128))
  {
    if (playerName.GetLength() > 0)
      return Format(LocalizeString(IDS_SESSION_NAME_FORMAT), cc_cast(playerName), cc_cast(hostName));
    else
      return hostName;
  }
  #endif
  return LocalizeString(IDS_SESSION_NAME_INIT);
}

// Macrovision CD Protection
#if _ENABLE_MP

//! Protected version of NetworkServer::Init
/*!
\param port IP port where server communicate
\param password session password
\patch 1.11 Date 07/31/2001 by Jirka
- Improved: Multiplayer session description
\patch 1.11 Date 08/02/2001 by Jirka
- Changed: Temporary directory on server in now Tmp<port> instead of Tmp
\patch 1.24 Date 09/19/2001 by Jirka
- Added: parameter "hostname" added to dedicated server config
- Added: port displayed in session description if not default
- Added: parameter "maxPlayers" added to dedicated server config
\patch 1.30 Date 11/01/2001 by Jirka
- Added: parameter "voteMissionPlayers" added to dedicated server config
  when no Missions section in server config and at least "voteMissionPlayers" (default = 1)
  players are connected, mission voting is started automatically
\patch 1.42 Date 1/9/2002 by Jirka
- Added: parameter "voiceOverNet" added to dedicated server config (default = true)
*/

#if _XBOX_SECURE && _XBOX_VER >= 200
CDP_DECL NetTranspServer * __cdecl CDPInitServer(
  RString name, RString password, int maxPlayers, int port, ParamEntryPar cfg,
  const XSESSION_INFO &sessionInfo, int &bandwidth)
#elif _XBOX_SECURE
CDP_DECL NetTranspServer * __cdecl CDPInitServer(
  RString name, RString password, int maxPlayers, int port, ParamEntryPar cfg,
  XNADDR &addr, XNKID &kid, XONLINETASK_HANDLE &task, XONLINE_ATTRIBUTE *sessionAttributes, int &bandwidth)
#else
CDP_DECL NetTranspServer * __cdecl CDPInitServer(RString name, RString password, int maxPlayers, int port, ParamEntryPar cfg)
#endif
{
  NetTranspServer *server = NULL;
  SECUROM_MARKER_HIGH_SECURITY_ON(10)
  //Direct play has been eliminated from here
  server = CreateNetServer(cfg);
  ProgressRefresh();
  if (!server) return NULL;

  // load some settings from Flashpoint.cfg
  server->SetNetworkParams(cfg);

  MPVersionInfo versionInfo;
  versionInfo.versionActual = MP_VERSION_ACTUAL;
  versionInfo.versionRequired = MP_VERSION_REQUIRED;
  versionInfo.mission[0] = 0;
  versionInfo.serverState = NSSSelectingMission;
  RString GetModListNames();
  strncpy(versionInfo.mod, GetModListNames(), MOD_LENGTH);
  versionInfo.mod[MOD_LENGTH - 1] = 0;

  bool voiceOverNet = true;

#if _XBOX_SECURE

  bandwidth = 10000000; // by default assume 10 Mbps - system link
  RString playerName = Glob.header.GetPlayerName().Substring(0, 15);

# if _XBOX_VER >= 200

  int estBandwidth = GetEstimateBandwidth();
  if (estBandwidth >= 0) bandwidth = estBandwidth;

  // startup the QoS listening
  INT err = XNetQosListen(&sessionInfo.sessionID, NULL, 0, QOS_SERVER_BANDWIDTH, XNET_QOS_LISTEN_ENABLE);
  DoAssert(err == 0);
  err = XNetQosListen(&sessionInfo.sessionID, NULL, 0, QOS_SERVER_BANDWIDTH, XNET_QOS_LISTEN_SET_BITSPERSEC);
  DoAssert(err == 0);

  bool systemLink = GNetworkManager.IsSystemLink();
  int GetDSPrivateSlots();
  int maxPrivate = GetDSPrivateSlots();
  bool ok = server->Init(port, password, sessionInfo.hostAddress, sessionInfo.sessionID, sessionInfo.keyExchangeKey, maxPlayers, maxPrivate,
    playerName, GetGameLanguage(), Glob.config.difficulty,
    versionInfo, true, systemLink, MAGIC_APP);

# else

  DWORD status;
  do
  {
    // Repeat while pending; OK to do other work in this loop
    status = XNetGetTitleXnAddr(&addr);
  } while (status == XNET_GET_XNADDR_PENDING);

  // Error checking
  if (status == XNET_GET_XNADDR_NONE)
  {
    RptF("InitServer: Cannot obtain Xbox address");
    delete server;
    return NULL;
  }

  if (!GNetworkManager.IsSystemLink())
  {
    // get user gamer tag    
    XONLINE_USER *user = XOnlineGetLogonUsers();
    Assert(user);
    if (user) playerName = user->szGamertag;
  }

  XNKEY key;
  if (!XOnlineRegisterServer(playerName, maxPlayers, kid, key, task, sessionAttributes, bandwidth))
  {
    // System link version
    int result = XNetCreateKey(&kid, &key);
    if (result != 0)
    {
      RptF("InitServer: XNetCreateKey failed with error 0x%x", result);
      delete server;
      return NULL;
    }
  }
  int result = XNetRegisterKey(&kid, &key);
  if (result != 0)
  {
    RptF("InitServer: XNetRegisterKey failed with error 0x%x", result);
    delete server;
    return NULL;
  }

  INT err = XNetQosListen(&kid, NULL, 0, QOS_SERVER_BANDWIDTH, XNET_QOS_LISTEN_ENABLE);
  DoAssert(err == 0);

  err = XNetQosListen(&kid, NULL, 0, QOS_SERVER_BANDWIDTH, XNET_QOS_LISTEN_SET_BITSPERSEC);
  DoAssert(err == 0);

  int GetDSPrivateSlots();
  int maxPrivate = GetDSPrivateSlots();

  bool enumResponse = GNetworkManager.IsSystemLink();
  bool ok = server->Init(
    port, password, addr, kid, key, maxPlayers, maxPrivate,
    playerName, GetGameLanguage(), Glob.config.difficulty,
    versionInfo, true, enumResponse, MAGIC_APP);

# endif // _XBOX_VER >= 200

#else

  bool equalModRequired = false;

#if _ENABLE_DEDICATED_SERVER
  if (IsDedicatedServer())
  {
    GameDataNamespace globals(NULL, RString(), false); // TODO: parsing namespace if access to globals needed

    ParamFile cfg;
    RString GetServerConfig();
    cfg.Parse(GetServerConfig(), NULL, NULL, &globals);
    
    ConstParamEntryPtr entry = cfg.FindEntry("hostname");
    if (entry) name = *entry;
    if (name.GetLength() == 0) name = GetDefaultSessionName(RString());

    entry = cfg.FindEntry("voiceOverNet");
    if (entry)
    {
      voiceOverNet = *entry;
    }

    entry = cfg.FindEntry("equalModRequired");
    if (entry) equalModRequired = *entry;

    // on dedicated server, use only server name as session name
    // playerName = LocalizeString(IDS_DEDICATED_SERVER);
  }
  else
#endif
  {
    if (name.GetLength() == 0)
    {
#if _VBS2 // VBS2 nickname
      RString playerName = Glob.header.playerHandle;
#else
      RString playerName = Glob.header.GetPlayerName();
#endif
      name = GetDefaultSessionName(playerName);
    }
  }

  bool ok = server->Init(
    name, password, maxPlayers, port,  
    GetGameLanguage(), Glob.config.difficulty,
    versionInfo, equalModRequired, MAGIC_APP
  );
#endif // def _XBOX

  if (!ok)
  {
    delete server;
    return NULL;
  }

#if _VBS3
  // Force the VON off if requested by the program argument
  extern bool DisableVON;
  if (DisableVON)
  {
    voiceOverNet = false;
  }
#endif

  if (voiceOverNet)
    server->InitVoice();

  char buffer[256];
  sprintf(buffer, "Tmp%d", server->GetSessionPort());
  ServerTmpDir = buffer;

  SECUROM_MARKER_HIGH_SECURITY_OFF(10)
  
  return server;
}
#endif //  _ENABLE_MP

void ParseFlashpointCfg(ParamFile &file);

/*!
\patch 1.89 Date 10/25/2002 by Jirka
- Added: Item "proxy" added to Flashpoint.cfg to enforce proxy server used to download xml squad page etc.
*/

bool NetworkServer::Init(RString name, RString password, int maxPlayers, int port)
{
#if _ENABLE_MP
  // load some settings from Flashpoint.cfg
  ParamFile cfg;
  ParseFlashpointCfg(cfg);

  ConstParamEntryPtr entry = cfg.FindEntry("proxy");
  if (entry) _proxy = *entry;

  // Macrovision CD Protection
#if _XBOX_SECURE

#if _XBOX_VER >= 200
  _server = CDPInitServer(name, password, maxPlayers, port, cfg, _parent->GetSessionInfo(), _serverUpBandwidth);
#else
  _sessionTask = NULL;
  XOnlineInitSession();

  _server = CDPInitServer(
    name, password, maxPlayers, port, cfg, _xnaddr, _sessionKid, _sessionTask, _sessionAttributes,
    _serverUpBandwidth);
#endif

#ifdef PEER2PEER
  if (_server && _sessionTask)
  {
    _p2pServer = CreatePeerToPeerServer(_sessionKid);
  }
#endif    // PEER2PEER
#else
  //_serverUpBandwidth = 1000000000; // assume very fast bandwidth unless proven otherwise - 1Gbps
  _server = CDPInitServer(name, password, maxPlayers, port, cfg);
#endif
  return _server != NULL;
  #else
  return false;
  #endif
}

void NetworkServer::InitVoice()
{
  if (!_parent->IsVoNDisabled())
    if (_server) _server->InitVoice();
}

NetworkServer::~NetworkServer()
{
  RemoveSystemMessages();

  SetServerState(NSSNone);

//{ QUERY & REPORTING SDK
#if _ENABLE_GAMESPY
  DoneQR();
#endif
//}

#if _ENABLE_STEAM
  if (UseSteam) DoneSteam();
#endif

  Done();

#if _ENABLE_CHEATS
  if (_debugWindow) _debugWindow->Close();
#endif
}

void NetworkServer::Done()
{
#if PEER2PEER
  if (_p2pServer) _p2pServer = NULL;
#endif

  // _dp->CancelAsyncOperation(NULL, DPNCANCEL_ALL_OPERATIONS);
  RemoveUserMessages();
  if (_server) DeleteDirectoryStructure(GetServerTmpDir(), true);
  _server = NULL;

#if _XBOX_SECURE && _ENABLE_MP
# if _XBOX_VER >= 200
  const XNKID *sessionId = _parent->GetSessionKey();
  if (sessionId)
  {
    INT err = XNetQosListen(sessionId, NULL, 0, QOS_SERVER_BANDWIDTH, XNET_QOS_LISTEN_RELEASE);
    DoAssert(err == 0);
  }

  // Statistics handled by the NetworkManager
# else
  if (GNetworkManager.IsSignedIn() && !GNetworkManager.IsSystemLink())
  {

    if (_statsUpdate)
    {
      _statsUpdate->Close();
      _statsUpdate = NULL;
    }

    // unregister session from matchmaking service
    if (_sessionTask)
    {
      XOnlineTaskClose(_sessionTask);
      _sessionTask = NULL;
    }
    HRESULT result = XOnlineMatchSessionDelete(_sessionKid, NULL, &_sessionTask);
    if (SUCCEEDED(result))
    {
      do
      {
        result = XOnlineTaskContinue(_sessionTask);
      } 
      while (result == XONLINETASK_S_RUNNING);
      if (!SUCCEEDED(result))
      {
        RptF("Unregister session: XOnlineTaskContinue failed with error 0x%x", result);
      }
      XOnlineTaskClose(_sessionTask);
    }
    else
    {
      RptF("Unregister session: XOnlineMatchSessionDelete failed with error 0x%x", result);
    }
  }

  INT err = XNetQosListen(&_sessionKid, NULL, 0, QOS_SERVER_BANDWIDTH, XNET_QOS_LISTEN_RELEASE);
  DoAssert(err == 0);

  XNetUnregisterKey(&_sessionKid);
# endif // _XBOX_VER >= 200
#endif // _XBOX_SECURE && _ENABLE_MP
}

/*!
\patch 5095 Date 12/5/2006 by Jirka
- Fixed: Dedicated server crash
*/

int NetworkServer::GetMaxPlayers() const
{
  int maxPlayers = GetMaxPlayersLimit();
  if (_serverState >= NSSAssigningRoles)
  {
    saturateMin(maxPlayers, _playerRoles.Size());
  }
#if _ENABLE_DEDICATED_SERVER
  else if (IsDedicatedServer())
  {
#if !_VBS2_LITE // if we're not using lite version, pay attention to DS
    saturateMin(maxPlayers, DEFAULT_PLAYERS_DS);
#endif
  }
#endif

  saturateMin(maxPlayers, _totalSlots);

  return maxPlayers; 
}

int NetworkServer::GetMaxPlayersSafe(int bandwidthPerClient, int round) const
{
  int maxPlayers = GetMaxPlayersLimit();
#if _XBOX_SECURE && _ENABLE_MP
  // consider bandwidth
  // TODO: ask mission about bandwidth requirements
  int maxClients = _serverUpBandwidth/bandwidthPerClient;
  
  int maxPlayersByBandwidth = maxClients;
#if _ENABLE_DEDICATED_SERVER
  // on non-ded. server host is not counted as a player
  if (!IsDedicatedServer()) maxPlayersByBandwidth++;
#endif
  if (maxPlayers>maxPlayersByBandwidth)
  {
    LogF(
      "Max. players limited to %d - bandwidth %d kbps",maxPlayersByBandwidth,_serverUpBandwidth/1000
    );
    maxPlayers = maxPlayersByBandwidth;
  }

#endif
  // always return even number
  maxPlayers = maxPlayers/round*round;
  if (maxPlayers<round) maxPlayers = round;
  return maxPlayers;
  
}

bool NetworkServer::GetURL(char *address, DWORD addressLen)
{
  if (!_server) return false;
  return _server->GetURL(address, addressLen);
}

/*!
\patch 1.21 Date 08/23/2001 by Jirka
- Improved: session description
*/
void NetworkServer::UpdateSessionDescription()
{
  if (_server)
  {
    RString gameType, island;
    LocalizedString mission;
    int timeleft = 0;
    int difficulty = Glob.config.difficulty;
    if (_serverState >= NSSAssigningRoles)
    {
      gameType = _missionHeader._gameType;
      mission = _missionHeader._name;
      island = _missionHeader._island;
      timeleft = GetSessionTimeLeft();
    }
    _server->UpdateSessionDescription(
      _serverState, gameType, mission, island, timeleft, difficulty
    );
  }
}

RString QosInfo::GetLocalizedMissionName() const
{
  int type = missionLocalizedType-' ';
  saturate(type,LocalizedString::PlainText,LocalizedString::Stringtable);
  LocalizedString str(LocalizedString::Type(type),missionId.cstr());
  return str.GetLocalizedValue();
}

void QosInfo::SetMissionName(const LocalizedString &str)
{
  missionLocalizedType = ' '+str._type;
  strcpy(missionId,str._id);
}

/*!
\patch 1.21 Date 08/22/2001 by Jirka
- Added: GameSpy Query & Reporting SDK support for dedicated server
\patch_internal 1.86 Date 10/16/2002 by Jirka
- Added: ASE SDK support for dedicated server
\patch_internal 1.90 Date 11/12/2002 by Jirka
- Fixed: dedicated server was reporting to ASE even for reportingIP="" 
*/

//{ QUERY & REPORTING SDK
#if _ENABLE_GAMESPY

// GameSpy callbacks

/*!
\patch 5090 Date 11/27/2006 by Jirka
- Fixed: Possible crash when hosting MP session
*/

// Called when a server key needs to be reported
void serverkey_callback(int keyid, qr2_buffer_t outbuf, void *userdata)
{
  if (GNetworkManager.GetServer() != userdata) return;
  NetworkServer *server = ((NetworkServer *)userdata);
  if (server->IsValid()) server->OnQueryServer(keyid, outbuf);
}

// Called when a player key needs to be reported
void playerkey_callback(int keyid, int index, qr2_buffer_t outbuf, void *userdata)
{
  if (GNetworkManager.GetServer() != userdata) return;
  NetworkServer *server = ((NetworkServer *)userdata);
  if (server->IsValid()) server->OnQueryPlayer(keyid, index, outbuf);
}

// Called when a team key needs to be reported
void teamkey_callback(int keyid, int index, qr2_buffer_t outbuf, void *userdata)
{
  if (GNetworkManager.GetServer() != userdata) return;
  NetworkServer *server = ((NetworkServer *)userdata);
  if (server->IsValid()) server->OnQueryTeam(keyid, index, outbuf);
}	

// Called when we need to report the list of keys we report values for
void keylist_callback(qr2_key_type keytype, qr2_keybuffer_t keybuffer, void *userdata)
{
  if (GNetworkManager.GetServer() != userdata) return;
  NetworkServer *server = ((NetworkServer *)userdata);
  if (server->IsValid()) server->OnQueryKeys(keytype, keybuffer);
}

// Called when we need to report the number of players and teams
int count_callback(qr2_key_type keytype, void *userdata)
{
  if (GNetworkManager.GetServer() != userdata) return 0;
  NetworkServer *server = ((NetworkServer *)userdata);
  if (server->IsValid()) return server->OnQueryCount(keytype);
  return 0;
}

// Called if our registration with the GameSpy master server failed
void adderror_callback(qr2_error_t error, gsi_char *errmsg, void *userdata)
{
  WarningMessage(errmsg);
  RptF("GameSpy QR2 error: %d, %s", error, errmsg);
  (void *)userdata;
}

// NAT Negotiation callbacks
static void NNProgressCallback(NegotiateState state, void *userdata)
{
}

static void NNCompletedCallback(NegotiateResult result, SOCKET gamesocket, struct sockaddr_in *remoteaddr, void *userdata)
{
  switch (result)
  {
  case nr_success:
    RptF("NAT Negotiation completed");
    break;
  case nr_deadbeatpartner:
    RptF("NAT Negotiation failed - partner not found");
    break;
  case nr_inittimeout:
    RptF("NAT Negotiation failed - unable to communicate with the server");
    break;
  case nr_unknownerror:
    RptF("NAT Negotiation failed - unknown error");
    break;
  }
}

void NNDetectionResultsCallback(gsi_bool success, NAT nat)
{
  QOStrStream natType;
  if (success) 
  {
    natType << "Begin (NAT type):\n";
    static bool doIt = false;
    if (doIt)
    {
      if (*nat.brand) natType << "   Brand: " << nat.brand << "\n";
      if (*nat.model) natType << "   Model: " << nat.model << "\n";
      if (*nat.firmware) natType << "   Firmware: " << nat.firmware << "\n";
    }
    RString natTypeStr;
    switch (nat.natType)
    {
    case no_nat: natTypeStr = "no nat"; break;
    case firewall_only: natTypeStr = "firewall only"; break;
    case full_cone: natTypeStr = "full cone"; break;
    case restricted_cone: natTypeStr = "restricted cone"; break;
    case port_restricted_cone: natTypeStr = "port restricted cone"; break;
    case symmetric: natTypeStr = "symmetric"; break;
    case unknown: 
    default: natTypeStr = "unknown";
    }
    natType << "   NAT type: " << cc_cast(natTypeStr) << "\n"; 
    RString natPromiscuity;
    switch (nat.promiscuity)
    {
    case promiscuous: natPromiscuity = "promiscuous"; break;
    case not_promiscuous: natPromiscuity = "not promiscuous"; break;
    case port_promiscuous: natPromiscuity = "port promiscuous"; break;
    case ip_promiscuous: natPromiscuity = "ip promiscuous"; break;
    case promiscuity_not_applicable:
    default: natPromiscuity = "promiscuity not applicable"; break;
    }
    natType << "   NAT promiscuity: " << cc_cast(natPromiscuity) << "\n";
    RString natMappingScheme;
    switch (nat.mappingScheme)
    {
    case private_as_public: natMappingScheme = "private as public"; break;
    case consistent_port: natMappingScheme = "consistent port"; break;
    case incremental: natMappingScheme = "incremental"; break;
    case mixed: natMappingScheme = "mixed"; break;
    case unrecognized:
    default: natMappingScheme = "unrecognized"; break;
    }
    natType << "   NAT mapping scheme: " << cc_cast(natMappingScheme) << "\n";
    if (nat.ipRestricted) natType << "   NAT is IP restricted\n";
    if (nat.portRestricted) natType << "   NAT is PORT restricted\n";
    if (nat.qr2Compatible) natType << "   NAT is qr2 compatible\n";
    natType << "End (NAT type)";
  }
  else natType << "NAT detection was not successful.";
  RString detectedNatType(natType.str(), natType.pcount());
  GDebugVoNBank._natType = detectedNatType;
#if !_SUPER_RELEASE && ENABLE_REPORT
  if (GInput.cheatVoNDetectNatType)
  {
    RptF(cc_cast(detectedNatType));
  }
#endif
}

// Called when a client wants to connect using nat negotiation
//   (Nat Negotiation must be enabled in qr2_init)
void nn_callback(int cookie, void *userdata)
{
  LogF("NAT Negotiation started for cookie : %d", cookie);
  SOCKET GetServerSocket();
  enterNN();
  NegotiateError error = NNBeginNegotiationWithSocket(GetServerSocket(), cookie, 1, NNProgressCallback, NNCompletedCallback, NULL);
#if !_SUPER_RELEASE && _ENABLE_CHEATS
  if (!GDebugVoNBank.NatTypeDetectionTriggered())
  {
    GDebugVoNBank.TriggerNatTypeDetection();
    NegotiateError errorDet = NNStartNatDetection(NNDetectionResultsCallback);
    (void) errorDet;
  }
#endif
  leaveNN();
  (void)error;
  (void)userdata;
}

// Called when the local clients public address is received
void pa_callback(unsigned int ip, unsigned short port, void *userdata)
{
  NetworkServer *server = reinterpret_cast<NetworkServer *>(userdata);
  if (server && server == GNetworkManager.GetServer() && server->IsDedicatedBotClient(BOT_CLIENT))
  {
    NetworkPlayerInfo *player = server->GetPlayerInfo(BOT_CLIENT);
    DoAssert(player);
    player->_publicAddr = ip;
    (void)port;
  }
}

void SetQRInitialized(bool set);

/*!
\patch 1.34 Date 12/04/2001 by Jirka
- Changed: GameSpy base local port
*/
void NetworkServer::InitQR(bool isPrivate)
{
  _qrInitDone = true;
#if _ENABLE_DEDICATED_SERVER
  if (stricmp(_reportingIP, "noreport") == 0) isPrivate = true;
#endif
  _doNotReport = isPrivate;

  qr2_register_key(PARAM1_KEY, "param1");
  qr2_register_key(PARAM2_KEY, "param2");
  qr2_register_key(CURRENT_VERSION_KEY, "currentVersion");
  qr2_register_key(REQUIRED_VERSION_KEY, "requiredVersion");
  qr2_register_key(MOD_KEY, "mod");
  qr2_register_key(EQUAL_MOD_REQUIRED_KEY, "equalModRequired");
  qr2_register_key(GAME_STATE_KEY, "gameState");
  qr2_register_key(DEDICATED_KEY, "dedicated");
  qr2_register_key(PLATFORM_KEY, "platform");
  qr2_register_key(LANGUAGE_KEY, "language");
  qr2_register_key(DIFFICULTY_KEY, "difficulty");
  qr2_register_key(MISSION_KEY, "mission");
  qr2_register_key(BATTLEYE_KEY, "sv_battleye");
  qr2_register_key(SIGNATURES_KEY, "signatures");
  qr2_register_key(VERIFY_SIGNATURES_KEY, "verifySignatures");
  qr2_register_key(MOD_HASH_KEY, "modhash");
  qr2_register_key(HASH_KEY, "hash");
  qr2_register_key(REQ_BUILD_KEY, "reqBuild");
  
#if _ENABLE_DEDICATED_SERVER
  if (!_doNotReport && _reportingIP.GetLength() > 0)
  {
    int len = sizeof(qr2_hostname) / sizeof(char);
    strncpy(qr2_hostname, _reportingIP, len);
    qr2_hostname[len - 1] = 0;
  }
#endif

  static bool useNatNegotiation = true;

  SOCKET GetServerSocket();
  if (qr2_init_socket(NULL, GetServerSocket(), GetSessionPort(),
    GetGameName(), GetSecretKey(), _doNotReport ? 0 : 1, useNatNegotiation,
    serverkey_callback, playerkey_callback, teamkey_callback,
    keylist_callback, count_callback, adderror_callback, this) == e_qrnoerror)
  {
    SetQRInitialized(true);

    // Set a function to be called when we receive a nat negotiation request
    qr2_register_natneg_callback(NULL, nn_callback);
    qr2_register_publicaddress_callback(NULL, pa_callback);
#if _VERIFY_CLIENT_KEY
    if (gcd_init_qr2(NULL, GetGameID()) == e_qrnoerror)
      _validateCDKey = true; //this enable validation on GameSpy
#endif
  }

#if REPORT_ASE
  ASEMaster_init(1);
#endif
}

void NetworkServer::DoneQR()
{
#if _VERIFY_CLIENT_KEY
  if (_validateCDKey)
  {
    gcd_disconnect_all(GetGameID());
    gcd_shutdown();
    _validateCDKey = false;
  }
#endif

  //let game master know we are shutting down (removes dead server entry from the list)
  SetQRInitialized(false);
  qr2_shutdown(NULL);

#ifdef GSI_UNICODE
  // In Unicode mode we must perform additional cleanup
  qr2_internal_key_list_free();
#endif
}

void NetworkServer::SimulateQR()
{
  if (!_qrInitDone) 
    return; //InitQR was not called (possibly playing LAN game)
  //check for / process incoming queries
  qr2_think(NULL);
#if _VERIFY_CLIENT_KEY
  if (_validateCDKey) gcd_think();
#endif

#if REPORT_ASE
  ASEMaster_heartbeat(0, ::GetServerPort() + 1, GetGameName());
#endif
}

void NetworkServer::ChangeStateQR()
{
  qr2_send_statechanged(NULL);
}

void NetworkServer::OnQueryServer(int keyid, qr2_buffer_t outbuf)
{
  switch (keyid)
  {
  case GAMEVER_KEY:
    qr2_buffer_add(outbuf, Format("%s.%d", APP_VERSION_TEXT, BUILD_NO));
    break;
  case HOSTNAME_KEY:
    qr2_buffer_add(outbuf, _server->GetSessionName());
    break;
  case GAMENAME_KEY:
    qr2_buffer_add(outbuf, GetGameName());
    break;
/*
  case HOSTPORT_KEY:
    qr2_buffer_add_int(outbuf, _server->GetSessionPort());
    break;
*/
  case MAPNAME_KEY:
    {
      RString island;
      if (_serverState >= NSSAssigningRoles) island = _missionHeader._island;
      qr2_buffer_add(outbuf, island);
    }
    break;
  case GAMETYPE_KEY:
    {
      RString gameType;
      if (_serverState >= NSSAssigningRoles) gameType = _missionHeader._gameType;
      qr2_buffer_add(outbuf, gameType);
    }
    break;
  case MISSION_KEY:
    {
      RString mission;
      if (_serverState >= NSSAssigningRoles) mission = _missionHeader.GetLocalizedMissionName();
      qr2_buffer_add(outbuf, mission);
    }
    break;
  case NUMPLAYERS_KEY:
    qr2_buffer_add_int(outbuf, _identities.Size());
    break;
  case NUMTEAMS_KEY:
    qr2_buffer_add_int(outbuf, 0);
    break;
  case MAXPLAYERS_KEY:
    qr2_buffer_add_int(outbuf, GetMaxPlayers());
    break;
  case GAMEMODE_KEY:
    {
      const char *mode = "";
      switch (_serverState)
      {
      case NSSNone:
        mode = "exiting"; 
        break;
      case NSSSelectingMission:
      case NSSEditingMission:
      case NSSAssigningRoles:
        mode = "openwaiting";
        break;
      case NSSSendingMission:
      case NSSLoadingGame:
      case NSSBriefing:
      case NSSPlaying:
        mode = "openplaying"; 
        break;
      case NSSDebriefing:
      case NSSMissionAborted:
        mode = "openwaiting";
        break;
      default:
        Fail("Game state");
        break;
      }
      qr2_buffer_add(outbuf, mode);
    }
    break;
  case TIMELIMIT_KEY:
    {
      int timeleft;
      if (_serverState == NSSPlaying)
      {
        timeleft = 15;

        float et = _missionHeader._estimatedEndTime.toFloat();
        if (et > 0)
        {
          et -= Glob.time.toFloat();
          timeleft = toInt(et / 60.0);
          saturateMax(timeleft, 1);
        }
      }
      else timeleft = 0;
      qr2_buffer_add_int(outbuf, timeleft);
    }
    break;
  case PASSWORD_KEY:
    qr2_buffer_add_int(outbuf, (_password ? 2 : 0) + (_sessionLocked ? 1 : 0));
    break;
  case PARAM1_KEY:
    qr2_buffer_add_int(outbuf, toInt(_param1));
    break;
  case PARAM2_KEY:
    qr2_buffer_add_int(outbuf, toInt(_param2));
    break;
  case CURRENT_VERSION_KEY:
    qr2_buffer_add_int(outbuf, MP_VERSION_ACTUAL);
    break;
  case REQUIRED_VERSION_KEY:
    qr2_buffer_add_int(outbuf, MP_VERSION_REQUIRED);
    break;
  case MOD_KEY:
    {
      RString GetModListNames();
      qr2_buffer_add(outbuf, GetModListNames());
    }
    break;
  case MOD_HASH_KEY:
    {
      RString hashes;
      for (int i=0; i<GModInfos.Size(); i++)
      {
        if (i) hashes = hashes + RString(";") + GModInfos[i].hash;
        else hashes = GModInfos[i].hash;
      }
      qr2_buffer_add(outbuf, hashes);
    }
    break;
  case EQUAL_MOD_REQUIRED_KEY:
    qr2_buffer_add_int(outbuf, _equalModRequired ? 1 : 0);
    break;
  case HASH_KEY:
    qr2_buffer_add(outbuf, GLoadedContentHash);
    break;
  case GAME_STATE_KEY:
    qr2_buffer_add_int(outbuf, _serverState);
    break;
  case DEDICATED_KEY:
#if _ENABLE_DEDICATED_SERVER
    qr2_buffer_add_int(outbuf, _dedicated ? 1 : 0);
#else
    qr2_buffer_add_int(outbuf, 0);
#endif
    break;
  case PLATFORM_KEY:
#if defined(_WIN32) && defined(_XBOX)
    qr2_buffer_add(outbuf, "xbox");
#elif defined(_WIN32)
    qr2_buffer_add(outbuf, "win");
#elif defined(_LINUX)
    qr2_buffer_add(outbuf, "linux");
#else
    qr2_buffer_add(outbuf, "?");
#endif
    break;
  case LANGUAGE_KEY:
    qr2_buffer_add_int(outbuf, GetGameLanguage());
    break;
  case DIFFICULTY_KEY:
    qr2_buffer_add_int(outbuf, Glob.config.difficulty);
    break;
  case BATTLEYE_KEY:
    {
#if _USE_BATTL_EYE_SERVER
      int beEnabled = _beIntegration.IsEnabled() ? 1 : 0;
      qr2_buffer_add_int(outbuf, beEnabled);
#else
      qr2_buffer_add_int(outbuf, 0); //FIX: we must add some value!
#endif
    }
    break;
  case SIGNATURES_KEY:
    //max 200B for signatures list GS key
    //Note: it seems the GS packet splitting does not apply for this. It is not used to split up server keys that cannot fit into a single UDP packet.
    qr2_buffer_add(outbuf, GetSignaturesList(200)); 
    break;
  case VERIFY_SIGNATURES_KEY:
    if (MP_VERSION_REQUIRED<160 && !strcmp(APP_NAME_SHORT,"ArmA2OA")) // TODO: can be removed in 160
    {
      qr2_buffer_add_int(outbuf, _verifySignatures ? 1 : 0);
    }
    else
    {
      #if MP_VERSION_REQUIRED>=160
        #define MP_VER_STRING2(x) #x
        #define MP_VER_STRING(x) MP_VER_STRING2(x)
        #pragma message (__FILE__ "(" MP_VER_STRING(__LINE__) "): warning: Remove obsolete code")
        #undef MP_VER_STRING2
        #undef MP_VER_STRING
      #endif
      qr2_buffer_add_int(outbuf, _verifySignatures);
    }
    break;
  case REQ_BUILD_KEY:
    qr2_buffer_add_int(outbuf, _buildNoRequired);
    break;
  default:
    qr2_buffer_add(outbuf, _T(""));
    break;
  }
}

//Note: it can contain little bit more chars than maxLen as there can be terminating ";..." when space was not sufficient!
RString NetworkServer::GetSignaturesList(int maxLen)
{
  if (!maxLen)  maxLen=INT_MAX;
  RString list;
  for (int i=0, len=0; i < _acceptedKeys.Size(); i++)
  {
    if (i) 
    {
      list = list + RString(";"); //no ";" at the end of list
      len++;
    }
    int sigLen = _acceptedKeys[i]._name.GetLength();
    if ( len + sigLen > maxLen )
    { // truncate and finish signatures list collecting
      list = list + "..."; // mark signatures list as truncated
      break;
    }
    len += sigLen;
    list = list + _acceptedKeys[i]._name;
  }
  return list;
}

void NetworkServer::OnQueryPlayer(int keyid, int index, qr2_buffer_t outbuf)
{
  //check for valid index
  if (index >= _identities.Size())
  {
    qr2_buffer_add(outbuf, _T(""));
    return;
  }

  const PlayerIdentity &identity = _identities[index];
  switch (keyid)
  {
  case PLAYER__KEY:
    qr2_buffer_add(outbuf, identity.name);
    break;
  case TEAM__KEY:
    {
      RString squad;
      if (identity.squad) squad = identity.squad->_nick;
      qr2_buffer_add(outbuf, squad);
    }
    break;
  case SCORE__KEY:
    {
      int roleIndex = -1;
      for (int i=0; i<NPlayerRoles(); i++)
      {
        if (GetPlayerRole(i)->player == identity.dpnid)
        {
          roleIndex = i;
          break;
        }
      }

      int score = 0;
      if (roleIndex >= 0)
      {
        RefArray<AIStatsMPRow> &table = GStats._mission._tableMP;
        for (int i=0; i<table.Size(); i++)
        {
          if (table[i]->_role == roleIndex)
          {
            score = table[i]->_killsTotal;
            break;
          }
        }
      }
      qr2_buffer_add_int(outbuf, score);
    }
    break;
  case DEATHS__KEY:
    {
      int roleIndex = -1;
      for (int i=0; i<NPlayerRoles(); i++)
      {
        if (GetPlayerRole(i)->player == identity.dpnid)
        {
          roleIndex = i;
          break;
        }
      }

      int deaths = 0;
      if (roleIndex >= 0)
      {
        RefArray<AIStatsMPRow> &table = GStats._mission._tableMP;
        for (int i=0; i<table.Size(); i++)
        {
          if (table[i]->_role == roleIndex)
          {
            deaths = table[i]->_killed;
            break;
          }
        }
      }
      qr2_buffer_add_int(outbuf, deaths);
    }
    break;
  default:
    qr2_buffer_add(outbuf, _T(""));
    break;		
  }
}

void NetworkServer::OnQueryTeam(int keyid, int index, qr2_buffer_t outbuf)
{
  qr2_buffer_add(outbuf, _T(""));
}

void NetworkServer::OnQueryKeys(int keytype, qr2_keybuffer_t keybuffer)
{
  //need to add all the keys we support
  switch (keytype)
  {
  case key_server:
    qr2_keybuffer_add(keybuffer, GAMEVER_KEY);
    qr2_keybuffer_add(keybuffer, HOSTNAME_KEY);
//    qr2_keybuffer_add(keybuffer, HOSTPORT_KEY);
    qr2_keybuffer_add(keybuffer, MAPNAME_KEY);
    qr2_keybuffer_add(keybuffer, GAMETYPE_KEY);
    qr2_keybuffer_add(keybuffer, NUMPLAYERS_KEY);
    qr2_keybuffer_add(keybuffer, NUMTEAMS_KEY);
    qr2_keybuffer_add(keybuffer, MAXPLAYERS_KEY);
    qr2_keybuffer_add(keybuffer, GAMEMODE_KEY);
    qr2_keybuffer_add(keybuffer, TIMELIMIT_KEY);
    qr2_keybuffer_add(keybuffer, PASSWORD_KEY);
    qr2_keybuffer_add(keybuffer, PARAM1_KEY);
    qr2_keybuffer_add(keybuffer, PARAM2_KEY);
    qr2_keybuffer_add(keybuffer, CURRENT_VERSION_KEY);
    qr2_keybuffer_add(keybuffer, REQUIRED_VERSION_KEY);
    qr2_keybuffer_add(keybuffer, MOD_KEY);
    qr2_keybuffer_add(keybuffer, EQUAL_MOD_REQUIRED_KEY);
    qr2_keybuffer_add(keybuffer, GAME_STATE_KEY);
    qr2_keybuffer_add(keybuffer, DEDICATED_KEY);
    qr2_keybuffer_add(keybuffer, PLATFORM_KEY);
    qr2_keybuffer_add(keybuffer, LANGUAGE_KEY);
    qr2_keybuffer_add(keybuffer, DIFFICULTY_KEY);
    qr2_keybuffer_add(keybuffer, MISSION_KEY);
    qr2_keybuffer_add(keybuffer, GAMENAME_KEY);
    qr2_keybuffer_add(keybuffer, BATTLEYE_KEY);
    qr2_keybuffer_add(keybuffer, VERIFY_SIGNATURES_KEY);
    qr2_keybuffer_add(keybuffer, SIGNATURES_KEY);
    qr2_keybuffer_add(keybuffer, MOD_HASH_KEY);
    qr2_keybuffer_add(keybuffer, HASH_KEY);
    qr2_keybuffer_add(keybuffer, REQ_BUILD_KEY);
    break;
  case key_player:
    qr2_keybuffer_add(keybuffer, PLAYER__KEY);
    qr2_keybuffer_add(keybuffer, TEAM__KEY);
    qr2_keybuffer_add(keybuffer, SCORE__KEY);
    qr2_keybuffer_add(keybuffer, DEATHS__KEY);
    break;
  case key_team:
    break;
  default:
    break;
  }
}

int NetworkServer::OnQueryCount(int keytype)
{
  switch (keytype)
  {
  case key_player:
    return _identities.Size();
  case key_team:
    return 0;
  default:
    return 0;
  }
}

/*
void NetworkServer::OnQueryRules(char *outbuf, int maxlen)
{
  if (!SafePrint(ptr, maxlen, "\\mod\\%s", (const char *)GetModListShort())) return;
  if (!SafePrint(ptr, maxlen, "\\equalModRequired\\%d", _equalModRequired)) return;
  if (!SafePrint(ptr, maxlen, "\\gstate\\%d", _serverState)) return;
  + dedicated
  
  }
*/

#endif
//}

//{ STEAMWORKS SDK
#if _ENABLE_STEAM

const char *SteamGameDir()
{
  return "arma2arrowpc";
}

void NetworkServer::InitSteam()
{
  if (!UseSteam) return;

  const char *gameDir = SteamGameDir();
  uint32 ip = INADDR_ANY;
  uint16 spectatorPort = 0; // We don't support spectators in our game
  uint16 masterServerUpdaterPort = 27016;
  static const uint16 authenticationPort = 8766;
  EServerMode mode = eServerModeAuthenticationAndSecure;

  // Initialize the SteamGameServer interface, we tell it some info about us, and we request support
  // for both Authentication (making sure users own games) and secure mode, VAC running in our game
  // and kicking users who are VAC banned
  if (!SteamGameServer_Init(
    ip, authenticationPort, 
    GetSessionPort(), 
    spectatorPort, 
    masterServerUpdaterPort, 
    mode, 
    gameDir, 
    APP_VERSION_TEXT))
  {
    return;
  }

  // We want to actively update the master server with our presence so players can
  // find us via the steam matchmaking/server browser interfaces
  if (SteamMasterServerUpdater())
  {
    SteamMasterServerUpdater()->SetActive(true);
  }
}

void NetworkServer::DoneSteam()
{
  if (!UseSteam) return;

  // Notify Steam master server we are going offline
  if (SteamMasterServerUpdater()) SteamMasterServerUpdater()->SetActive(false);

  // Disconnect from the steam servers
  if (SteamGameServer()) SteamGameServer()->LogOff();

  // Release our reference to the steam client library
  SteamGameServer_Shutdown();
}

void NetworkServer::SimulateSteam()
{
  if (!UseSteam) return;

  // Run any Steam Game Server API callbacks
  SteamGameServer_RunCallbacks();

  // Update our server details
  ChangeStateSteam();
}

void NetworkServer::ChangeStateSteam()
{
  if (!UseSteam) return;

  if (SteamMasterServerUpdater())
  {
    // Tell the Steam Master Server about our game
    SteamMasterServerUpdater()->SetBasicServerData( 
      1,		// Protocol version to talk to the master server with
      _dedicated,	// Are we a dedicated server? Lie and say yes for our game since we don't have dedicated servers.
      "255",  // Region name is unused, use the magic value "255" always
      SteamGameDir(), // Our game name
      GetMaxPlayers(),		// Maximum number of players for our server
      _password || _sessionLocked,	// Does our server require a password?
      "Arma 2 Arrowhead" // Description of our game
      );

    // update any rule values we publish
    SteamMasterServerUpdater()->SetKeyValue("requiredVersion", Format("%d", MP_VERSION_REQUIRED));
    SteamMasterServerUpdater()->SetKeyValue("gameType", _missionHeader._gameType);
    SteamMasterServerUpdater()->SetKeyValue("island", _missionHeader._island);
    SteamMasterServerUpdater()->SetKeyValue("serverState", Format("%d", _serverState));
    SteamMasterServerUpdater()->SetKeyValue("mod", GetModListNames());
    SteamMasterServerUpdater()->SetKeyValue("equalModRequired", _equalModRequired ? "1" : "0");
    SteamMasterServerUpdater()->SetKeyValue("dedicated", _dedicated ? "1" : "0");
    SteamMasterServerUpdater()->SetKeyValue("language", Format("%d", GetGameLanguage()));
    SteamMasterServerUpdater()->SetKeyValue("difficulty", Format("%d", Glob.config.difficulty));

#if defined(_WIN32) && defined(_XBOX)
    SteamMasterServerUpdater()->SetKeyValue("platform", "xbox");
#elif defined(_WIN32)
    SteamMasterServerUpdater()->SetKeyValue("platform", "win");
#elif defined(_LINUX)
    SteamMasterServerUpdater()->SetKeyValue("platform", "linux");
#else
    SteamMasterServerUpdater()->SetKeyValue("platform", "?");
#endif

    int timeleft;
    if (_serverState == NSSPlaying)
    {
      timeleft = 15;

      float et = _missionHeader._estimatedEndTime.toFloat();
      if (et > 0)
      {
        et -= Glob.time.toFloat();
        timeleft = toInt(et / 60.0);
        saturateMax(timeleft, 1);
      }
    }
    else timeleft = 0;
    SteamMasterServerUpdater()->SetKeyValue("timeLeft", Format("%d", timeleft));
    if (SteamUtils()) SteamMasterServerUpdater()->SetKeyValue("country", SteamUtils()->GetIPCountry());
  }

  if (SteamGameServer())
  {
    // Tell the Steam authentication servers about our game
    SteamGameServer()->UpdateServerStatus(  
      _identities.Size(),				// Current player count
      GetMaxPlayers(),							// Maximum number of players
      0,							// Number of AI/Bot players
      _server->GetSessionName(),		// Server name
      "",							// Spectator server name, unused for us
      _missionHeader.GetLocalizedMissionName()					// Map name
      );

    // Update all the players names/scores
    for (int i=0; i<_identities.Size(); i++)
    {
      const PlayerIdentity &identity = _identities[i];
      
      // find the score
      int roleIndex = -1;
      for (int j=0; j<NPlayerRoles(); j++)
      {
        if (GetPlayerRole(j)->player == identity.dpnid)
        {
          roleIndex = j;
          break;
        }
      }
      int score = 0;
      if (roleIndex >= 0)
      {
        RefArray<AIStatsMPRow> &table = GStats._mission._tableMP;
        for (int j=0; j<table.Size(); j++)
        {
          if (table[j]->_role == roleIndex)
          {
            score = table[j]->_killsTotal;
            break;
          }
        }
      }

      SteamGameServer()->BUpdateUserData(/*SteamIDUser*/ CSteamID(), identity.GetName(), score);
    }

    // game type is a special string you can use for your game to differentiate different game play types occurring on the same maps
    // When users search for this parameter they do a sub-string search of this string 
    // (i.e if you report "abc" and a client requests "ab" they return your server)
    SteamGameServer()->SetGameType(_missionHeader._gameType);
  }
}

void NetworkServer::OnSteamServersConnected(SteamServersConnected_t *logonSuccess)
{
  LogF("Steam server connected");

  // Steam servers are ready to accept our session info now
  ChangeStateSteam();
}

void NetworkServer::OnSteamServersDisconnected(SteamServersDisconnected_t *loggedOff)
{
  LogF("Steam server disconnected");
}

void NetworkServer::OnPolicyResponse(GSPolicyResponse_t *policyResponse)
{
  LogF("Steam policy response");
}

#endif
//}

void LoadMissions(bool userMissions, AutoArray<MPMissionInfo> &missionList, bool campaignCheat=false);
static void AddMissionList(NetworkCommandMessage &answer, NetworkCommandMessage &answer2)
{
  PROFILE_SCOPE_EX(addML, network)
  AutoArray<MPMissionInfo> missionList;
  LoadMissions(false, missionList);
  for (int i=0; i<missionList.Size(); i++)
  {
    answer._content.WriteString(missionList[i].mission);
    answer._content.WriteString(missionList[i].world);
    answer._content.WriteString(missionList[i].gameType);
    answer._content.WriteLocalizedString(missionList[i].displayName);
    answer._content.Write(&missionList[i].placement, sizeof(int));
    answer._content.Write(&missionList[i].minPlayers, sizeof(int));
    answer._content.Write(&missionList[i].maxPlayers, sizeof(int));
    answer._content.Write(&missionList[i].perClient, sizeof(int));
  }
  // add additional information into the answer2 (not added into the answer1 becaus of backward MP compatibility)
  int count = missionList.Size();
  answer2._content.Write(&count, sizeof(int));
  for (int i=0; i<count; i++)
  {
    answer2._content.WriteLocalizedString(missionList[i].description);
    answer2._content.Write(&missionList[i].respawn, sizeof(int));
  }
}

#if _ENABLE_DEDICATED_SERVER
void NetworkServer::SetGameMaster(int player, bool admin)
{
  Monitor(0); // new game master is not willing to get monitor lines in the previous master rate
  _gameMaster = player;
  _admin = admin;

  RString name;
  for (int i=0; i<_players.Size(); i++)
  {
    if (_players[i].dpid == player)
    {
      name = _players[i].name;
      break;
    }
  }
  ConsoleF(LocalizeString(IDS_DS_ADMIN_LOGGED_IN), (const char *)name);

  _votings.Clear();

  NetworkCommandMessage answer;
  answer._type = NCMTLogged;
  answer._content.Write(&_admin, sizeof(_admin));
  // AddMissionList(answer);
  SendMsg(player, &answer, NMFGuaranteed);

#if !_XBOX_SECURE
  if (_mission[0] == '?' && _mission[1] == 0)
  {
    NetworkCommandMessage answer;
    answer._type = NCMTVoteMission;
    NetworkCommandMessage answer2;
    answer2._type = NCMTVoteMissionExt;
    AddMissionList(answer, answer2);
    SendMsg(player, &answer, NMFGuaranteed);
    SendMsg(player, &answer2, NMFGuaranteed);
  }
#endif

  UpdateAdminState();
}
#endif

void NetworkServer::ApplyVoting(const AutoArray<char> &id, const AutoArray<char> *value)
{
#if _ENABLE_DEDICATED_SERVER
  int *ptr = (int *)id.Data();
  int type = *(ptr++);
  switch (type)
  {
    case NCMTMission:
      if (value)
      {
        // difficulty voting is separate - ask it for a result
        // the voting is performed at the same time, therefore the result should be ready
        _difficulty = Glob.config.diffDefault;
        const Voting *diff = _votings.Find(NCMTDifficulty);
        if (diff)
        {
          const AutoArray<char> *diffValue = diff->GetValue();
          if (diffValue)
          {
            RString diffName(diffValue->Data(),diffValue->Size());
            int difficulty = Glob.config.diffNames.GetValue(diffName);
            if (difficulty >= 0) _difficulty = difficulty;
          }
          _votings.DeleteVoting(NCMTDifficulty);
        }
        // check mission voting result
        _mission = RString(value->Data(),value->Size());
        if (_mission.GetLength() > 0) _restart = true;
        if (_serverState != NSSPlaying)
        {
          GNetworkManager.DestroyAllObjects();
          SetServerState(NSSSelectingMission);
        }
      }
      break;
    case NCMTMissions:
      {
        _mission = "?";
        _restart = true;
        if (_serverState != NSSPlaying)
        {
          GNetworkManager.DestroyAllObjects();
          SetServerState(NSSSelectingMission);
        }
        NetworkCommandMessage answer;
        answer._type = NCMTVoteMission;
        NetworkCommandMessage answer2;
        answer2._type = NCMTVoteMissionExt;
        AddMissionList(answer,answer2);
        for (int i=0; i<_identities.Size(); i++)
        {
          SendMsg(_identities[i].dpnid, &answer, NMFGuaranteed);
          SendMsg(_identities[i].dpnid, &answer2, NMFGuaranteed);
        }
      }
      break;
    case NCMTRestart:
      if (_serverState == NSSPlaying)
      {
        _restart = true;
        _reassign = false;
      }
      break;
    case NCMTReassign:
      _restart = true;
      _reassign = true;
      if (_serverState != NSSPlaying)
      {
        GNetworkManager.DestroyAllObjects();

        // unassign all players
        #if _ENABLE_AI
          int flags = _missionHeader._disabledAI ? PRFNone : PRFEnabledAI;
        #else
          int flags = PRFNone;
        #endif
        for (int i=0; i<_playerRoles.Size(); i++)
        {
          _playerRoles[i].player = AI_PLAYER;
          _playerRoles[i].flags = flags;
        }
        // send to clients
        for (int i=0; i<_players.Size(); i++)
        {
          NetworkPlayerInfo &info = _players[i];
          if (info.clientState < NCSConnected) continue;
          SendMissionInfo(info.dpid, true);
        }

        SetServerState(NSSAssigningRoles);

        _restart = false;
        _reassign = false;
      }
      break;
    case NCMTKick:
      {
        int player = *(ptr++);
        if (player != BOT_CLIENT) KickOff(player,KORKick);
      }
      break;
    case NCMTAdmin:
      if (value)
      {
        const char *p = value->Data();
        int player = *((int *)p);
        if (player != AI_PLAYER && _dedicated && _gameMaster == AI_PLAYER)
          SetGameMaster(player, true);
      }
      break;
  }
#endif
}

void OnServerUserMessage(int from, char *buffer, int bufferSize, void *context)
{
  NetworkServer *server = (NetworkServer *)context;
  NetworkMessageRaw rawMsg(buffer, bufferSize);
  server->DecodeMessage(from, rawMsg);
}

void OnServerSendComplete(DWORD msgID, bool ok, void *context)
{
  NetworkServer *server = (NetworkServer *)context;
  server->OnSendComplete(msgID, ok);
}

void OnServerPlayerCreate(int player, bool botClient, bool privateSlot, const char *name, const char *mod, unsigned long inaddr, void *context)
{
  RString GetModListNames();
  if (!botClient && stricmp(mod, GetModListNames()) != 0)
  {
    RString format = LocalizeString(IDS_MP_VALIDERROR_2);

    RString message = Format(format, cc_cast(name));
    if (mod && *mod) message = message + Format(" - %s", cc_cast(mod));

    RefArray<NetworkObject> dummy;
    GNetworkManager.Chat(CCGlobal, "", dummy, message);
    GChatList.Add(CCGlobal, NULL, message, false, true);
#if _ENABLE_DEDICATED_SERVER
    ConsoleF("%s", cc_cast(message));
#endif
  }
  
  NetworkServer *server = (NetworkServer *)context;
  server->OnCreatePlayer(player, privateSlot, name, inaddr);
}

void OnServerPlayerDelete(int player, void *context)
{
  NetworkServer *server = (NetworkServer *)context;
  server->OnPlayerDestroy(player);
}

bool OnServerVoicePlayerCreate(int player, void *context)
{
  NetworkServer *server = (NetworkServer *)context;
  return server->OnCreateVoicePlayer(player);
}

unsigned NetworkServer::CleanUpMemory()
{
  if (_server) return _server->FreeMemory();
  return 0;
}

void NetworkServer::ReceiveSystemMessages()
{
  if (_server)
  {
    _server->ProcessSendComplete(OnServerSendComplete, this);
    _server->ProcessPlayers(OnServerPlayerCreate, OnServerPlayerDelete, this);
    _server->ProcessVoicePlayers(OnServerVoicePlayerCreate, this);
  }
}

void NetworkServer::ReceiveUserMessages()
{
  if (_server) _server->ProcessUserMessages(OnServerUserMessage, this);
}

void NetworkServer::RemoveSystemMessages()
{
  if (_server)
  {
    _server->RemoveSendComplete();
    _server->RemovePlayers();
    _server->RemoveVoicePlayers();
  }
}

void NetworkServer::RemoveUserMessages()
{
  if (_server) _server->RemoveUserMessages();
}

void CalculateMinMaxAvg::Sample(float value, float weight)
{
  saturateMin(_min,value);
  saturateMax(_max,value);
  _sum += value*weight;
  _lastValue = value;
  _weight += weight;
}

void CalculateMinMaxAvg::Reset()
{
  _min = FLT_MAX;
  _max = FLT_MIN;
  _sum = 0;
  _weight = 0;
  _lastValue = 0;
}

CalculateMinMaxAvg::CalculateMinMaxAvg()
{
  Reset();
}


inline int toLargeIntSat(float x)
{
  if (x>=INT_MAX) return INT_MAX;
  if (x<INT_MIN) return INT_MIN;
  return toLargeInt(x);
}

void NetworkServer::ServerMessage(const char *text)
{
//{ DEDICATED SERVER SUPPORT
#if _ENABLE_DEDICATED_SERVER
  ConsoleF("%s", cc_cast(text));
#endif
//}
  GChatList.Add(CCGlobal, NULL, text, false, true);
  RefArray<NetworkObject> dummy;
  GNetworkManager.Chat(CCGlobal, "", dummy, text);
}

void NetworkServer::ServerMessage(const LocalizedFormatedString &text)
{
  RString localized = text.GetLocalizedValue();
//{ DEDICATED SERVER SUPPORT
#if _ENABLE_DEDICATED_SERVER
  ConsoleF("%s", cc_cast(localized));
#endif
//}
  GChatList.Add(CCGlobal, NULL, localized, false, true);
  RefArray<NetworkObject> dummy;
  GNetworkManager.LocalizedChat(CCGlobal, "", dummy, text);
}


//{ DEDICATED SERVER SUPPORT
#if _ENABLE_DEDICATED_SERVER

#define DEBUG_TYPE_ENUM(type,prefix,XX) \
  XX(type, prefix, TotalSent) \
  XX(type, prefix, UserSent) \
  XX(type, prefix, Console) \
  XX(type, prefix, UserInfo) \
  XX(type, prefix, UserQueue)

#ifndef DECL_ENUM_DEBUG_TYPE
#define DECL_ENUM_DEBUG_TYPE
DECL_ENUM(DebugType)
#endif
DECLARE_DEFINE_ENUM(DebugType,DT,DEBUG_TYPE_ENUM)

/*!
\patch 1.88 Date 10/21/2002 by Ondra
- Added: Dedicated server command #debug (userInfo)
(Requires 1.87 client)
\patch 1.90 Date 10/31/2002 by Ondra
- New: MP: Added #debug checkfile command.
Server admins can use this command to check if any file used by clients matches server version.
\patch 1.90 Date 10/31/2002 by Ondra
- New: MP: Added server.cfg array checkfiles[] -
list of files that should be checked for each player connecting.
Example: checkfiles[]={"HWTL\dta\data3d.pbo","dta\data3d.pbo"}
\patch 1.90 Date 10/31/2002 by Ondra
- New: Servers checks if world file (.wrp) used by client matches server version.
*/

static RString ExtractWord(RString &line)
{
  const char *endWord = strchr(line,' ');
  if (!endWord)
  {
    RString word = line;
    line = RString();
    return word;
  }
  RString word = line.Substring(0,endWord-line.Data());
  line = endWord+1;
  return word;
}

void NetworkServer::DebugAsk(RString str)
{
  // check for some special debug commands
  RString line = str;
  RString word = ExtractWord(line);
  if (!strcmpi(word,"checkfile"))
  {
    // get file name
    RString filename = ExtractWord(line);
    if (filename.GetLength()>0)
    {
      // check file CRC
      if (!IsRelativePath(filename))
      {
        RString message = RString("Only relative path check allowed: ") + filename; 
        GChatList.Add(CCGlobal, NULL, message, false, true);
      }
      else
      {
        // TODO: get player ID (optional)
        // if there is no ID, check all players
        PerformFileIntegrityCheck(filename);
      }
    }
    return;
  }
  if (!strcmpi(word,"limitout"))
  {
    RString limit = ExtractWord(line);
    int limitBW = atoi(limit);
    if (limit>0)
    {
      int limitBWB = (limitBW+7)/8;
      _server->SetConnectionLimits(limitBWB);
      RString message = Format("Outgoing bandwidth limited to %d B/s (%d bps) per client",limitBWB,limitBWB*8);
      DebugAnswer(message);
    }
    return;
  }
  if (!strcmpi(word,"von"))
  {
    // check whether #debug von is not currently running
    if (_server && !GDebugVoNBank.IsRunning()) 
    {
      //P2PManager diagnostics first
      RString p2pDiag = _p2pManager.GetDiagString();
      GDebugVoNBank.SetFeature(DebugVoNBank::VoNFeature_P2PManager, p2pDiag);
      //P2P Topology
      QOStrStream diag;
      AutoArray<ConvPair, MemAllocLocal<ConvPair, 64> > convTable;
      convTable.Realloc(_identities.Size());
      convTable.Resize(_identities.Size());
      for (int i=0; i<_identities.Size(); i++)
      {
        convTable[i]._dpnid = _identities[i].dpnid;
        convTable[i]._id = _identities[i].id;
      }
      _server->GetVoNTopologyDiag(diag, convTable);
      GDebugVoNBank.SetFeature(DebugVoNBank::VoNFeature_NetTopology, RString(diag.str(), diag.pcount()) );
      // Ask each player for her collected VoNFeatures
      for (int i=0; i<_players.Size(); i++)
      {
        const NetworkPlayerInfo &player = _players[i];
        NetworkCommandMessage ask;
        ask._type = NCMTAskVoNDiagnostics;
        SendMsg(player.dpid, &ask, NMFGuaranteed);
      }
      // Set VoNDiagnozer to wait on clients answers and report it after while
      GDebugVoNBank.StartCollecting(GlobalTickCount(), _players.Size());
    }
    return;
  }

  // check if string is only numbers
  char *endNum;
  float interval = strtod(str,&endNum);
  if (*endNum==0)
  {
    _debugInterval = interval;
    saturate(_debugInterval,0.1f,3600);
    BString<256> msg;
    sprintf(msg,"Debug: Interval %g",_debugInterval);
    DebugAnswer(RString(msg));
    return;
  }
  if (!stricmp(str,"off"))
  {
    // clear all debug info
    _debugOn.Clear();
    DebugAnswer(RString("Debug: All off"));
    //_debugInterval = 0;
    _debugNext = INT_MAX;
    return;
  }

  const char *beg = str;
  const char *end = strchr(beg, ' ');
  int len = end ? end - beg : strlen(beg);

  RString strCommand(beg, len);
  DebugType command = GetEnumValue<DebugType>((const char *)strCommand);
  if (command < 0) return;

  beg += len;
  while (*beg == ' ') beg++;

  if (stricmp(beg, "off") == 0)
  {
    _debugOn.DeleteKey(command);
    DebugAnswer(RString("Debug: ") + strCommand + RString(" off"));
  }
  else
  {
    _debugOn.AddUnique(command);
    DebugAnswer(RString("Debug: ") + strCommand);

    if (_debugInterval<=0)
    {
      _debugInterval = 10;
      _debugNext = _debugNext + toInt(_debugInterval*1000);
    }
    DWORD next = GlobalTickCount()+toInt(_debugInterval*1000);
    if (_debugNext>next) _debugNext = next;
  }
}

void NetworkServer::DebugAnswer(RString str)
{
#ifdef _XBOX
  LogF(str);
#else
  if (_gameMaster != AI_PLAYER)
  {
    NetworkCommandMessage msg;
    msg._type = NCMTDebugAnswer;
    msg._content.WriteString(str);
    SendMsg(_gameMaster, &msg, NMFGuaranteed);
  }
#endif
}

/*!
\patch 5126 Date 1/31/2007 by Jirka
- Fixed: DS crash occurred when mission with some special name (containing '%') was selected
*/

int NetworkServer::ConsoleF(const char *format, ...)
{
  va_list arglist;
  va_start(arglist, format);

  if (_debugOn.FindKey(DTConsole) >= 0)
  {
    BString<512> buf;
    vsprintf(buf, format, arglist);
    DebugAnswer((const char *)buf);
  }
  int result = ::VConsoleF(format, arglist);
  
  va_end( arglist );
  return result;
}

#endif
//}

/*
const DWORD VotingTimeOutReady = 30000; // timeout in ms when all players are ready
const DWORD VotingTimeOutNotReady = 120000; // timeout in ms when no players are ready

const DWORD RoleTimeOutReady = 30000; // timeout in ms when all players are ready
const DWORD RoleTimeOutNotReady = 120000; // timeout in ms when no players are ready

const DWORD BriefingTimeOutReady = 15000; // timeout in ms when all players are ready
const DWORD BriefingTimeOutNotReady = 60000; // timeout in ms when no players are ready

const DWORD DebriefingTimeOutReady = 30000; // timeout in ms when all players are ready
const DWORD DebriefingTimeOutNotReady = 60000; // timeout in ms when no players are ready
*/

const DWORD VotingTimeOutReady = 60000; // timeout in ms when all players are ready
const DWORD VotingTimeOutNotReady = 60000; // timeout in ms when no players are ready

const DWORD RoleTimeOutReady = 90000; // timeout in ms when all players are ready
const DWORD RoleTimeOutNotReady = 90000; // timeout in ms when no players are ready

const DWORD BriefingTimeOutReady = 60000; // timeout in ms when all players are ready
const DWORD BriefingTimeOutNotReady = 60000; // timeout in ms when no players are ready

const DWORD DebriefingTimeOutReady = 45000; // timeout in ms when all players are ready
const DWORD DebriefingTimeOutNotReady = 45000; // timeout in ms when no players are ready

bool NetworkServer::CheckTimeOut(int readyCount, int totalCount, DWORD ready, DWORD notReady)
{
  // with low numbers assume there still will be more players connecting
  int minPlayersToVote = 8;
  float readyRatio;
  if (totalCount<minPlayersToVote)
  {
    readyRatio = float(readyCount)/float(minPlayersToVote);
  }
  else
  {
    readyRatio = float(readyCount)/float(totalCount);
  }
  DWORD timeToEnd = _serverStateTime+ready+toLargeInt((1-readyRatio)*(notReady-ready));
  if (GlobalTickCount()>=timeToEnd) return true;
  DWORD timeLeft = timeToEnd-GlobalTickCount();
  int secondsLeft = timeLeft/1000+1;
  
  if (_serverTimeOutSent!=secondsLeft)
  {
    // send to all players
    ServerTimeoutMessage msg;
    msg._timeout = secondsLeft;
    for (int i=0; i<_players.Size(); i++)
    {
      NetworkPlayerInfo &info = _players[i];
      if (info.clientState < NCSConnected) continue;
      SendMsg(info.dpid, &msg, NMFGuaranteed);
    }
    _serverTimeOutSent = secondsLeft;
  }

  #if _ENABLE_REPORT
    GlobalShowMessage(1000,"Time left: %d",secondsLeft);
  #endif
  return false;
}


/*!
\patch 1.52 Date 4/19/2002 by Jirka
- Fixed: When gamemaster disconnect from server during mission selection, server was in bad state.
\patch 1.79 Date 7/31/2002 by Jirka
- Improved: Mission on dedicated server launched if all roles are assigned and confirmed even if some players remain
\patch 1.82 Date 8/23/2002 by Ondra
- Fixed: MP: If server had predefined mission cycle,
mission sometimes started while first user was connecting.
\patch 5110 Date 12/21/2006 by Jirka
- Fixed: Dedicated server can use missions in addons now
\patch 5132 Date 2/23/2007 by Jirka
- Fixed: server config parameter persistent is fully working now
\patch 5133 Date 2/26/2007 by Jirka
- Fixed: MP - admin is deciding when briefing is finished now
*/

struct MissionParameter
{
  RString name;
  float   val;
  MissionParameter() {};
  MissionParameter(RString name1, float val1) : name(name1), val(val1) {};
  ClassIsMovableZeroed(MissionParameter);
};

void NetworkServer::SimulateDS()
{
  PROFILE_SCOPE_EX(nwDS, network)

//{ DEDICATED SERVER SUPPORT
#if _ENABLE_DEDICATED_SERVER
  if (!_dedicated) return;

  DWORD tickCount = GlobalTickCount();
  if (_players.Size() <= 1 && !(_persistent && _serverState == NSSPlaying))
  {
    if (_serverState != NSSSelectingMission)
    {
      ConsoleF(LocalizeString(IDS_DS_NO_USERS));
      SetServerState(NSSSelectingMission);
      CleanUpMemory();
    }
    _missionIndex = -1;
    _mission = "";
    _restart = false;
    _reassign = false;
    // restart timeout
    _serverStateTime = GlobalTickCount();
  }
  else
  {
    static bool loadIsland = false;
    switch (_serverState)
    {
    case NSSSelectingMission:
      {
        RString t;
        // if _param1,2 are not defined in server config, use default
        _param1 = FLT_MAX;
        _param2 = FLT_MAX;
        AutoArray<MissionParameter> parameters; // secondary parameters
        if (_mission.GetLength() == 1 && _mission[0] == '?')
        {
          // voting in progress
          // check timeout, and react if necessary
          // check how many players already voted
          const Voting *miss = _votings.Find(NCMTMission);
          if (!miss) break;
          int total = _parent->GetIdentities()->Size();
          int ready = miss->Size();

          // if server has reached half of its capacity, consider it full
          if (CheckTimeOut(ready,total,VotingTimeOutReady,VotingTimeOutNotReady))
          {
            // try applying vote first
            if (!_votings.Apply(this,NCMTMission))
            {
              // no voting available - select a random mission
              AutoArray<MPMissionInfo> missionList;
              LoadMissions(false, missionList);
              int random = toIntFloor(GRandGen.RandomValue()*missionList.Size());
              saturate(random,0,missionList.Size()-1);
              const MPMissionInfo &info = missionList[random];
              _mission = info.mission+"."+info.world;
              // leave difficulty as it is
            }
          }
          
          break;
        }
        else if (_mission.GetLength() > 0)
        {
          // mission known - start it
          t = _mission;
          Glob.config.difficulty = _difficulty;

          _mission = "";
          _restart = false;
          _reassign = false;
        }
        else
        {
          // check if there is some mission list defined
          ConstParamEntryPtr entry = _serverCfg.FindEntry("Missions");
          int nEntries = entry ? entry->GetEntryCount() : 0;
          if (nEntries == 0)
          {
            // no mission list - check if there are enough players to start voting
            int voteMissionPlayers = 1;
            entry = _serverCfg.FindEntry("voteMissionPlayers");
            if (entry) voteMissionPlayers = *entry;
            if (_identities.Size() >= voteMissionPlayers)
            {
              // start voting
              _mission = "?";
              NetworkCommandMessage answer;
              answer._type = NCMTVoteMission;
              NetworkCommandMessage answer2;
              answer2._type = NCMTVoteMissionExt;
              AddMissionList(answer,answer2);
              for (int i=0; i<_identities.Size(); i++)
              {
                SendMsg(_identities[i].dpnid, &answer, NMFGuaranteed);
                SendMsg(_identities[i].dpnid, &answer2, NMFGuaranteed);
              }
            }
            break;
          }

          // cycle through the mission list
          _missionIndex++;
          if (_missionIndex >= nEntries) _missionIndex = 0;
          ParamEntryVal item = entry->GetEntry(_missionIndex);

          t = item >> "template";

          Glob.config.difficulty = Glob.config.diffDefault;
          entry = item.FindEntry("difficulty");
          if (entry)
          {
            RString diffName = *entry;
            int difficulty = Glob.config.diffNames.GetValue(diffName);
            if (difficulty >= 0) Glob.config.difficulty = difficulty;
          }
          else
          {
            bool cadetMode = item >> "cadetMode";
            if (cadetMode)
            {
              int difficulty = Glob.config.diffNames.GetValue("Cadet");
              if (difficulty >= 0) Glob.config.difficulty = difficulty;
            }
            else
            {
              int difficulty = Glob.config.diffNames.GetValue("Veteran");
              if (difficulty >= 0) Glob.config.difficulty = difficulty;
            }
          }

          if (item.FindEntry("param1")) _param1 = item >> "param1";
          if (item.FindEntry("param2")) _param2 = item >> "param2";

          //read secondary parameters
          ConstParamEntryPtr paramsDesc = item.FindEntry("Params");
          if(paramsDesc && paramsDesc->IsClass()) for(int i=0; i< paramsDesc->GetEntryCount();i++ )
          {
            parameters.Add( MissionParameter( paramsDesc->GetEntry(i).GetName(), paramsDesc->GetEntry(i) ) );
          }
        }
        
        // mission selected, start it now

        CurrentCampaign = "";
        CurrentBattle = "";
        CurrentMission = "";

        SetBaseDirectory(false, "");

        bool noCopy = false;
        RString owner;

        char mission[256]; strcpy(mission, t);
        char *world = strrchr(mission, '.');
        if (!world) return;
        *world = 0; world++;

#if _ENABLE_MISSION_CONFIG
        ConstParamEntryPtr entry = (Pars >> "CfgMissions" >> "MPMissions").FindEntry(mission);
        if (entry)
        {
          ::SelectMission(*entry);
          noCopy = true;
          owner = entry->GetOwnerName();
        }
        else
#endif
        {
          RString FindBriefingName(RString path);
          RString displayName = FindBriefingName(MPMissionsDir + t);

          if (QIFileFunctions::FileExists(MPMissionsDir + t + RString(".pbo")))
          {
            // mission in public bank
            CreateMission(mission, world);
            SetMission(world, "__cur_mp", MPMissionsDir);
            Glob.header.filenameReal = mission;

            ConsoleF(LocalizeString(IDS_DS_MISSION_READ_FROM_BANK), (const char *)displayName);
          }
          else if (QIFileFunctions::FileExists(MPMissionsDir + t + RString("\\mission.sqm")))
          {
            // mission in public directory
            CreateMission("", "");
            SetMission(world, mission, MPMissionsDir);

            ConsoleF(LocalizeString(IDS_DS_MISSION_READ_FROM_DIR), (const char *)displayName);
          }
          else break;
        }

        // load template
        CurrentTemplate.Clear();

        // check Ids after SwitchLandscape
        if (!ParseMission(true, true)) break;

        // if parameters are not set in server config, use defaults
        if (_param1 == FLT_MAX)
        {
          _param1 = 0;
          ConstParamEntryPtr cfg = ExtParsMission.FindEntry("defValueParam1");
          if (cfg) _param1 = *cfg;
        }
        if (_param2 == FLT_MAX)
        {
          _param2 = 0;
          ConstParamEntryPtr cfg = ExtParsMission.FindEntry("defValueParam2");
          if (cfg) _param2 = *cfg;
        }
        _paramsArray.Clear();
        _paramsArray.Add(_param1);
        _paramsArray.Add(_param2);
        // secondary parameters
        ConstParamEntryPtr paramsDesc = ExtParsMission.FindEntry("Params");
        if(paramsDesc) for(int i=0; i< paramsDesc->GetEntryCount();i++ )
        {
          ParamEntryVal parameterDesc = paramsDesc->GetEntry(i);
          ConstParamEntryPtr entry = parameterDesc.FindEntry("default");
          _paramsArray.Add(entry ? *entry : 0);
          RString name = parameterDesc.GetName();
          for (int j=0; j<parameters.Size(); j++)
          {
            if (stricmp(name, parameters[j].name)==0)
            {
              _paramsArray[i+2]=parameters[j].val;
              break;
            }
          }
        }

        InitMission(Glob.config.difficulty, RString(), true, noCopy, owner, false);
        SetServerState(NSSAssigningRoles);
      }
      break;
    case NSSAssigningRoles:
      {
        // check if all commanders are ready
#if _VBS3
        // When using restart command, make sure everyone is ready
        // before starting the mission. Means admin has to click on 
        // the OK button.
#else
        if (!_restart)
#endif
        {
          if (_gameMaster != AI_PLAYER)
          {
            if (GetClientState(_gameMaster) < NCSRoleAssigned) break;
          }
          else
          {
            int total = 0, ready = 0;
            for (int i=0; i<_players.Size(); i++)
            {
              if (_players[i].dpid == BOT_CLIENT) continue;
              // ignore players which are not in role selection yet
              if (_players[i].clientState<NCSMissionSelected) continue;
              
              // CHECK FOR PLAYER WITHOUT IDENTITY
              if (!FindIdentity(_players[i].dpid))
              {
                RptF
                (
                  "Server error: Player without identity %s (id %d)",
                  (const char *)_players[i].name, _players[i].dpid
                );
                continue;
              }

              total++;
              if (_players[i].clientState >= NCSRoleAssigned) ready++;
            }
            // never run a mission without any players
            if (total<=0) break;
            // if all players are ready, the mission should start
            // if all roles are read, the mission should start
            if (ready < total && ready < NPlayerRoles())
            {
              // calculate timeout
              int totalRoles = min(total,NPlayerRoles());
              if (!CheckTimeOut(ready,totalRoles,RoleTimeOutReady,RoleTimeOutNotReady))
              {
                // timeout not expired yet - do not start
                break;
              }
            }
          }
        }
        _restart = false;

#if _VBS3
        // Tell all the clients,except for the bot client to load the map
        // this should give some time for the server to send the mission out
        // while everyone else is loading the map, then for itself to load the map
        // shortly after
        for (int j=0; j<_players.Size(); j++)
        {
          NetworkPlayerInfo &info = _players[j];
          if (info.dpid != BOT_CLIENT)
          {
            LoadIslandMessage msg;
            msg._islandName = Glob.header.worldname;

            if (info.clientState == NCSRoleAssigned)
              SendMsg(info.dpid,&msg,NMFGuaranteed);
          }
        }
#endif

        // create game
        SetServerState(NSSSendingMission);
        SendMissionFile();

        ConsoleF(LocalizeString(IDS_DS_ROLES_ASSIGNED));

        // clear statistics now - ensure they will not be cleared after stats info from clients arrived
        GStats.ClearMission();

        SetServerState(NSSLoadingGame);
        
        loadIsland = true;
      }
      break;
    case NSSSendingMission:
      Fail("Temporary state");
      break;
    case NSSLoadingGame:
      if (loadIsland && GNetworkManager.GetClient()->GetClientState() == NCSMissionReceived)
      {
        loadIsland = false;

        ConsoleF(LocalizeString(IDS_DS_READING_MISSION));

        Ref<ProgressHandle> p = GWorld->BegLoadMissionProgress(Glob.header.worldname, CurrentTemplate, GModeNetware);

        GWorld->SwitchLandscape(Glob.header.worldname);
        if (!CurrentTemplate.CheckObjectIds())
        {
          WarningMessage(LocalizeString(IDS_BAD_OBJECT_ID),(const char *)Localize(CurrentTemplate.intel.briefingName));
        }

        //set parameters - after SwitchLandscape
        float param1 = 0, param2 = 0;
        GetNetworkManager().GetParams(param1, param2);
        
        GameState *gstate = GWorld->GetGameState();
        gstate->VarSet("param1", GameValue(param1), false, false, GWorld->GetMissionNamespace()); // mission namespace
        gstate->VarSet("param2", GameValue(param2), false, false, GWorld->GetMissionNamespace()); // mission namespace
        GNetworkManager.PublicVariable("param1");
        GNetworkManager.PublicVariable("param2");
        
        const MissionHeader *header = GetNetworkManager().GetMissionHeader();
        AutoArray<float> paramsArray = GetNetworkManager().GetParamsArray();
        GameValue valueAR = gstate->CreateGameValue(GameArray);
        GameArrayType &arrayAR = valueAR;
        arrayAR.Resize(0);
        for(int i=0; i< paramsArray.Size(); i++)
        {        
          if(header->_sizesParamArray[i]<=0) continue;
          arrayAR.Add(paramsArray[i]);
        }
        gstate->VarSet("paramsArray", arrayAR, false, false, GWorld->GetMissionNamespace()); // mission namespace
        GetNetworkManager().PublicVariable("paramsArray");

        GWorld->ActivateAddons(CurrentTemplate.addOns,Glob.header.worldname);
        GWorld->InitGeneral(CurrentTemplate.intel);
        bool error = !GWorld->InitVehicles(GModeNetware, CurrentTemplate);

        ProgressFinish(p);

        RString message = GetMaxErrorMessage();
        if (message.GetLength()>0)
        {
          ServerMessage(message);
          SetServerState(NSSSelectingMission);
          break;
        }
        if (error)
        {
          ServerMessage(LocalizeString(IDS_DS_LOAD_MISSION_ERROR));
          SetServerState(NSSSelectingMission);
          break;
        }
        
        GNetworkManager.CreateAllObjects();
        // objects are created on client, so we can change state to briefing after transfer from client to server completes
        // GetNetworkManager().SetClientState(NCSGameLoaded);

#if _VBS2 // hack to enable scripted missions to work in MP
        void RunScriptedMissionVBS();
        RunScriptedMissionVBS();
#endif

        void RunInitScript();
        RunInitScript();

        message = GetMaxErrorMessage();
        if (message.GetLength()>0)
        {
          ServerMessage(message);
          SetServerState(NSSSelectingMission);
          break;
        }
        else
        {
          ConsoleF(LocalizeString(IDS_DS_MISSION_READ));
        }
      }
      break;
    case NSSBriefing:
      {

#if _ENABLE_DEDICATED_SERVER
        if (_gameMaster != AI_PLAYER)
        {
          if (GetClientState(_gameMaster) >= NCSBriefingRead)
          {
            // start game
            SetServerState(NSSPlaying);
            ConsoleF(LocalizeString(IDS_DS_GAME_STARTED));
          }
        }
        else
#endif
        {
          int total = 0;
          int ready = 0;
          int inBriefing = 0;
          for (int i=0; i<_playerRoles.Size(); i++)
          {
            int player = _playerRoles[i].player;
            if (player == AI_PLAYER) continue;

            total++;
            if (GetClientState(player) == NCSBriefingShown)
            {
              inBriefing++;
            }
            if (GetClientState(player) == NCSBriefingRead)
            {
              ready++;
              inBriefing++;
            }
          }

          if (inBriefing<=0)
          {
            // as long as no player has received the mission, reset the timeout
            _serverStateTime = GlobalTickCount();
          }
          else
          {
            // once at least one player is in the briefing, we can start
            // if all are ready, or timeout expired, run the mission
            if (
              ready>=total ||
              CheckTimeOut(ready,total,BriefingTimeOutReady,BriefingTimeOutNotReady)
              )
            {
              // start game
              SetServerState(NSSPlaying);
              ConsoleF(LocalizeString(IDS_DS_GAME_STARTED));
            }
          }
        }
      }
      break;
    case NSSPlaying:
      if (_restart)
      {
        GNetworkManager.DestroyAllObjects();
        ConsoleF("Game restarted");
        SetServerState(NSSMissionAborted);
        //debriefing = Glob.uiTime;
      }
      else if 
      (
        GWorld->GetEndMode() != EMContinue &&
        (
          (!GWorld->GetCameraEffect() && !GWorld->GetTitleEffect()) ||
          GWorld->IsEndForced()
        )
      )
      {
        GNetworkManager.DestroyAllObjects();
        ConsoleF(LocalizeString(IDS_DS_GAME_FINISHED));
        SetServerState(NSSDebriefing); // we need to wait on the end of mission on other clients
        //debriefing = Glob.uiTime;
      }
      else
      {
        if (!_persistent)
        {
          // if the server is not persistent, check if some player is still playing the mission
          // if not, we want the mission to be terminated
          // see also World::CheckMissionEnd to termination on all players dead

          bool all = true;
          for (int i=0; i<_playerRoles.Size(); i++)
          {
            int player = _playerRoles[i].player;
            if (player == AI_PLAYER)
            {
              continue;
            }

            NetworkClientState state = GetClientState(player);
            if (state >= NCSGameLoaded && state <= NCSDebriefingRead)
            {
              all = false;
              break;
            }
          }
          if (all)
          {
            GNetworkManager.DestroyAllObjects();
            ConsoleF(LocalizeString(IDS_DS_GAME_FINISHED));
            SetServerState(NSSMissionAborted);
            //debriefing = Glob.uiTime;
          }
        }
      }
      break;
    case NSSDebriefing:
    case NSSMissionAborted:
      {
        bool all = true;
        if (_gameMaster != AI_PLAYER)
        {
          NetworkClientState state = GetClientState(_gameMaster);
          all = state != NCSBriefingRead && state != NCSGameFinished;
        }
        else
        {
          int total = 0, ready = 0;
          for (int i=0; i<_playerRoles.Size(); i++)
          {
            int player = _playerRoles[i].player;
            if (player == AI_PLAYER) continue;

            NetworkClientState state = GetClientState(player);
            
            if (state>=NCSBriefingRead || state<=NCSMissionSelected)
            {
              total++;
              if (state>=NCSDebriefingRead)
              {
                ready++;
              }
            }
          }
          
          all = (
            ready>=total ||
            CheckTimeOut(ready,total,DebriefingTimeOutReady,DebriefingTimeOutNotReady)
          );
        }
        if (all)
        {
          // update statistics
#if _XBOX_SECURE && _ENABLE_MP
# if _XBOX_VER >= 200
          // Statistics handled by the NetworkManager
# else
          if (_parent->IsSignedIn() && !_parent->IsSystemLink())
          {
            SRef<LiveStatsUpdate> statsUpdate = new LiveStatsUpdate();
            statsUpdate->Close(); // update now
          }
# endif // _XBOX_VER >= 200
#endif // _XBOX_SECURE && _ENABLE_MP
          if (_restart && _reassign)
          {
            // unassign all players
            #if _ENABLE_AI
              int flags = _missionHeader._disabledAI ? PRFNone : PRFEnabledAI;
            #else
              int flags = PRFNone;
            #endif
            for (int i=0; i<_playerRoles.Size(); i++)
            {
              _playerRoles[i].player = AI_PLAYER;
              _playerRoles[i].flags = flags;
            }
            // send to clients
            for (int i=0; i<_players.Size(); i++)
            {
              NetworkPlayerInfo &info = _players[i];
              if (info.clientState < NCSConnected) continue;
              SendMissionInfo(info.dpid, true);
            }

            SetServerState(NSSAssigningRoles);
            _restart = false;
            _reassign = false;
          }
          else if (_restart && _mission.GetLength() == 0)
          {
            SetServerState(NSSAssigningRoles);
            loadIsland = true;
          }
          else
          {
            SetServerState(NSSSelectingMission);
            ConsoleF(LocalizeString(IDS_DS_WAITING_FOR_NEXT_GAME));
          }
        }
      }
      break;
    }
  }

  _monitorFrames++;
  if (tickCount >= _monitorNext)
  {
    if (_gameMaster == AI_PLAYER)
    {
      _monitorInterval = 0;
    }
    else //if (_state == NGSPlay)
    {
      NetworkCommandMessage msg;
      msg._type = NCMTMonitorAnswer;
      float invInterval = 1.0 / (_monitorInterval + 0.001f * (tickCount - _monitorNext));
      float fps = _monitorFrames * invInterval;
      int memory = MemoryUsed();
      float in = _monitorIn * invInterval;
      float out = _monitorOut * invInterval;
      msg._content.Write(&fps, sizeof(fps));
      msg._content.Write(&memory, sizeof(memory));
      msg._content.Write(&in, sizeof(in));
      msg._content.Write(&out, sizeof(out));

      int sizeG = 0, sizeNG = 0;
      for (int i=0; i<_players.Size(); i++)
      {
        NetworkPlayerInfo &player = _players[i];
        for (int j=0; j<player._messageQueue.Size(); j++)
          sizeG += player._messageQueue[j].msg->size;
        for (int j=0; j<player._messageQueueNonGuaranteed.Size(); j++)
          sizeNG += player._messageQueueNonGuaranteed[j].msg->size;
      }
      msg._content.Write(&sizeG, sizeof(sizeG));
      msg._content.Write(&sizeNG, sizeof(sizeNG));

      SendMsg(_gameMaster, &msg, NMFGuaranteed);
    }
    Monitor(_monitorInterval);
  }

  if (GDebugVoNBank.IsReportReady(tickCount))
  { // Report the #debug von collected info at last
    FILE *LogFile = GetNetworkManager().GetDedicatedServerLogFile();
    if ( LogFile )
    {
      AutoArray<ConvPair, MemAllocLocal<ConvPair, 64> > convTable;
      convTable.Realloc(_identities.Size());
      convTable.Resize(_identities.Size());
      for (int i=0; i<_identities.Size(); i++)
      {
        convTable[i]._dpnid = _identities[i].dpnid;
        convTable[i]._id = _identities[i].id;
      }
      fprintf(LogFile, "*** VoN Topology Diagnostics ((***\n");
      RString response = GDebugVoNBank.GetDebugString(convTable);
      GDebugVoNBank.StopCollecting(); // other client responses are too late
      GDebugVoNBank.Reset(); // next #debug von should not contain old data
      fwrite(cc_cast(response), 1, response.GetLength(), LogFile);
      fprintf(LogFile, "*** VoN Topology Diagnostics ))***\n");
      fflush(LogFile);
    }
  }

  for (int i=0; i<_players.Size(); i++)
  {
    NetworkPlayerInfo &info = _players[i];
    if (info.clientState < NCSConnected) continue;
    if (info.motdIndex >= 0 && Glob.uiTime >= info.motdTime)
    {
      ChatMessage chat;
      chat._channel = CCGlobal;
      chat._text = _motd[info.motdIndex];
      chat._name = "";
      SendMsg(info.dpid, &chat, NMFGuaranteed);

      info.motdIndex++;
      if (info.motdIndex >= _motd.Size()) info.motdIndex = -1;
      else info.motdTime = Glob.uiTime + _motdInterval;
    }
  }

#if _XBOX_SECURE && _ENABLE_MP

# if _XBOX_VER >= 200
  // Statistics handled by the NetworkManager
# else
  if (_parent->IsSignedIn() && !_parent->IsSystemLink() && tickCount >= _nextStatsUpdate)
  {
    _nextStatsUpdate += 10 * 60 * 1000; // 10 min
    // Update statistics
    _statsUpdate = new LiveStatsUpdate("DSTime");
  }

  if (_statsUpdate)
  {
    _statsUpdate->Process();
    if (_statsUpdate->IsDone()) _statsUpdate = NULL;
  }
# endif // _XBOX_VER >= 200

#endif // _XBOX_SECURE && _ENABLE_MP

#endif // _ENABLE_DEDICATED_SERVER
//}
}

#if PEER2PEER

void P2PServerProcess(int channel, P2PMsgType type, BYTE *data, int dataSize, void *context)
{
  if (type == P2PRequest)
  {
    if (dataSize < 1) return;
    PeerToPeerServer *p2p = (PeerToPeerServer*)context;
    Assert(p2p);
    switch (data[0]) // message type
    {
    case 0:
      // list of missions
      {
        QOStrStream out;
        out.put(0); // response type

        // TODO: caching for faster response
        AutoArray<SaveHeader> list;
        SaveHeaderAttributes filter;
        filter.Add("type", FindEnumName(STMission));
        filter.Add("player", Glob.header.GetPlayerName());
        GDebugger.PauseCheckingAlive();
        FindSaves(list, filter);
        GDebugger.ResumeCheckingAlive();
        for (int i=0; i<list.Size(); i++)
        {
          SaveHeaderAttributes &attributes = list[i].attributes;
          RString mission;
          if (attributes.Find("mission", mission))
          {
            out.write(mission, mission.GetLength() + 1);
          }
        }
        out.put(0); // end

        p2p->SendAnswer(channel, (BYTE *)out.str(), out.pcount());
      }
      break;
    case 1:
      // mission
      break;
    }
  }
}

#endif    // PEER2PEER

/*!
\patch 1.79 Date 7/26/2002 by Jirka
- Added: Elimination of "disconnect cheat"
\patch 1.90 Date 10/30/2002 by Jirka
- Fixed: Message "Player ... is loosing connection" isn't write for server now
*/


void NetworkServer::OnSimulate()
{
  PROFILE_SCOPE_EX(nwSer, network)

  // raw statistics
#if _ENABLE_CHEATS
  // remove statistics info
  static AutoArray<int> texts;
  for (int i=0; i<texts.Size(); i++)
  {
    if (texts[i] >= 0) GEngine->RemoveText(texts[i]);
  }
  texts.Resize(0);

  if (outputDiags == ODMRawStats+NOutputDiags)
  {
    // int y = 40;
    for (int i=0; i<_rawStatistics.Size(); i++)
    {
      char output[1024];
      NetworkPlayerInfo *info = GetPlayerInfo(_rawStatistics[i].player);
      if (!info) continue;
      sprintf
      (
        output, "To %s: sent %d (%d), received %d (%d)",
        (const char *)info->name,
        _rawStatistics[i].sizeSent, _rawStatistics[i].msgSent,
        _rawStatistics[i].sizeReceived, _rawStatistics[i].msgReceived
      );
      texts.Add(GEngine->ShowTextF(1000, output));
      //y += 25;
    }
  }
#endif

#define DIAG_NETWORK_OBJECTS 0
#if DIAG_NETWORK_OBJECTS
  static int diagnozeObjectId = 0;
  if (diagnozeObjectId)
  {
    for (int i=0; i<_objects.Size(); i++)
    {
      Object *obj = dynamic_cast<Object *>(GNetworkManager.GetObject(_objects[i]->id));
      if (obj && obj->GetObjectId().Encode()==diagnozeObjectId)
      {
        LogF( " DIAGNOZED ENTITY: %s, index in _objects=%d", obj->GetDebugName(), i );
        break;
      }
    }
    diagnozeObjectId = 0;
  }
  // find object diagnoze using her NetworkId
  static bool diagnozeNetworkId = false;
  static int diagnozeCreator = 0;
  static int diagnozeId = 0;
  if (diagnozeNetworkId)
  {
    NetworkId diagNetworkId(diagnozeCreator, diagnozeId);
    int index = -1;
    for (int i=0; i<_objects.Size(); i++)
    {
      if (_objects[i]->id==diagNetworkId) { index=i; break; }
    }
    NetworkObject *nObj = GNetworkManager.GetObject(diagNetworkId);
    if (nObj)
    {
      LogF( " DIAGNOZED ENTITY: %s, index in _objects=%d", nObj->GetDebugName(), index );
    }
    else
    {
      LogF( " DIAGNOZED ENTITY: *NOT FOUND*, index in _objects=%d", index );
    }
    diagnozeNetworkId = false;
  }
#endif

#define DIAG_SLOPE_SELECTION_SERVER_LAGGING 0
#if DIAG_SLOPE_SELECTION_SERVER_LAGGING
  // RptF all players networking information from time to time
  int currentTickCnt = ::GetTickCount();
  static DWORD lastDiagTickCount = currentTickCnt;
  const DWORD nextDiagDelay = 60000; //1 minute
  if ( lastDiagTickCount + nextDiagDelay < currentTickCnt )
  {
    lastDiagTickCount = currentTickCnt;
    RptF("*** Start: Players Networking Diagnostics ***");
    for (int i=0; i<_players.Size(); i++)
    {
      NetworkPlayerInfo &pnet = _players[i];
      //if (pnet.dpid==BOT_CLIENT) continue;
      PlayerIdentity *id = FindIdentity(pnet.dpid);
      if (!id) continue;
      RptF("  %s bandwidth=<%d,%d,%d> ping=<%d,%d,%d> desync=%d", 
        cc_cast(id->name), 
        toLargeIntSat(pnet._bandwidth.GetMin()/(1024/8)), toLargeIntSat(pnet._bandwidth.GetMax()/(1024/8)), toLargeIntSat(pnet._bandwidth.GetAvg()/(1024/8)),
        toInt(pnet._ping.GetMin()), toInt(pnet._ping.GetMax()), toInt(pnet._ping.GetAvg()),
        toLargeIntSat(pnet._desync.GetAvg()*100)
      );
    }
    RptF("*** End: Players Networking Diagnostics ***");
  }
#endif

#if _ENABLE_REPORT && _XBOX_SECURE && _ENABLE_MP
  int VoiceServerGetReceivedDataCount();
  int VoiceServerGetReceivedDataSize();
  int VoiceServerGetReceivedCtrlCount();
  int VoiceServerGetReceivedCtrlSize();
  int VoiceServerGetSentDataCount();
  int VoiceServerGetSentDataSize();
  int VoiceServerGetSentCtrlCount();
  int VoiceServerGetSentCtrlSize();
  void VoiceServerResetStats();

  int VoiceClientGetReceivedDataCount();
  int VoiceClientGetReceivedDataSize();
  int VoiceClientGetSentDataCount();
  int VoiceClientGetSentDataSize();
  void VoiceClientResetStats();

  int rdc = VoiceServerGetReceivedDataCount()+VoiceClientGetReceivedDataCount();
  int rds = VoiceServerGetReceivedDataSize()+VoiceClientGetReceivedDataSize();
  int rcc = VoiceServerGetReceivedCtrlCount();
  int rcs = VoiceServerGetReceivedCtrlSize();
  int sdc = VoiceServerGetSentDataCount()+VoiceClientGetSentDataCount();
  int sds = VoiceServerGetSentDataSize()+VoiceClientGetSentDataSize();
  int scc = VoiceServerGetSentCtrlCount();
  int scs = VoiceServerGetSentCtrlSize();

  int foundData = -1;
  int foundCtrl = -1;
  for (int i=0; i<_statistics.Size(); i++)
  {
    if (_statistics[i].message == NMTN)
    {
      foundData = i;
      if (foundCtrl >= 0) break;
    }
    else if (_statistics[i].message == NMTN + 1)
    {
      foundCtrl = i;
      if (foundData >= 0) break;
    }
  }
  if (rdc > 0 || sdc > 0)
  {
    if (foundData < 0)
    {
      foundData = _statistics.Add();
      _statistics[foundData].message = NMTN;
    }
    _statistics[foundData].msgSent += sdc;
    _statistics[foundData].sizeSent += sds;
    _statistics[foundData].msgReceived += rdc;
    _statistics[foundData].sizeReceived += rds;
  }
  if (rcc > 0 || scc > 0)
  {
    if (foundCtrl < 0)
    {
      foundCtrl = _statistics.Add();
      _statistics[foundCtrl].message = NMTN + 1;
    }
    _statistics[foundCtrl].msgSent += scc;
    _statistics[foundCtrl].sizeSent += scs;
    _statistics[foundCtrl].msgReceived += rcc;
    _statistics[foundCtrl].sizeReceived += rcs;
  }

  VoiceServerResetStats();
  VoiceClientResetStats();
#endif

#if _ENABLE_DEDICATED_SERVER

#ifndef _XBOX
  if (_dedicated)
#endif
  {
    DWORD now = GlobalTickCount();
    if (now >= _debugNext)
    {
      if (_gameMaster == AI_PLAYER || _debugInterval<0.001)
      {
        _debugNext = INT_MAX;
      }
      else
      {
        _debugNext = _debugNext + toInt(_debugInterval*1000);
      }

      float invAge = 1/_debugInterval;

      if (_debugOn.FindKey(DTUserSent) >= 0)
      {
        for (int i=0; i<_rawStatistics.Size(); i++)
        {
          BString<512> output;
          NetworkPlayerInfo *info = GetPlayerInfo(_rawStatistics[i].player);
          if (!info) continue;
          sprintf
          (
            output, "%s: sent %.0f bps (%.2f Msgps), received %.0f bps (%.2f Msgps)",
            (const char *)info->name,
            invAge * _rawStatistics[i].sizeSent *8, invAge * _rawStatistics[i].msgSent,
            invAge * _rawStatistics[i].sizeReceived *8, invAge * _rawStatistics[i].msgReceived
          );
          DebugAnswer((const char *)output);
        }
      }

      if (_debugOn.FindKey(DTTotalSent) >= 0)
      {
        int totalMsgSent = 0;
        int totalMsgReceived = 0;
        int totalSizeSent = 0;
        int totalSizeReceived = 0;
        for (int i=0; i<_rawStatistics.Size(); i++)
        {
          totalMsgSent += _rawStatistics[i].msgSent;
          totalMsgReceived += _rawStatistics[i].msgReceived;
          totalSizeSent += _rawStatistics[i].sizeSent;
          totalSizeReceived += _rawStatistics[i].sizeReceived;
        }
        BString<512> output;
        sprintf
        (
          output, "** Total: sent %.0f bps (%.2f Msgps), received %.0f bps (%.2f Msgps)",
          invAge * totalSizeSent * 8, invAge * totalMsgSent,
          invAge * totalSizeReceived * 8, invAge * totalMsgReceived
        );
        DebugAnswer((const char *)output);
      }

      _rawStatistics.Clear();

      if (_debugOn.FindKey(DTUserQueue) >= 0)
      {
        // send info about all players
        for (int i=0; i<_players.Size(); i++)
        {
          const NetworkPlayerInfo &player = _players[i];
          int sizeG = 0, sizeNG = 0;
          for (int j=0; j<player._messageQueue.Size(); j++)
            sizeG += player._messageQueue[j].msg->size;
          for (int j=0; j<player._messageQueueNonGuaranteed.Size(); j++)
            sizeNG += player._messageQueueNonGuaranteed[j].msg->size;
          // print actual desync as well

          BString<512> output;
          sprintf
          (
            output, "%s: Queue %d B (%d) %d B Guaranteed (%d), Desync %.0f",
            (const char *)player.name,
            sizeNG, player._messageQueueNonGuaranteed.Size(), sizeG, player._messageQueue.Size(),
            player._desync.GetAvg()*100
          );
          DebugAnswer((const char *)output);
        }
      }
      if (_debugOn.FindKey(DTUserInfo) >= 0)
      {
        // send info about all players
        for (int i=0; i<_players.Size(); i++)
        {
          const NetworkPlayerInfo &player = _players[i];

          RString stats = _server->GetStatistics(player.dpid);
          
          BString<512> output;
          sprintf
          (
            output, "%s: Info %s",
            (const char *)player.name, (const char *)stats
          );
          DebugAnswer((const char *)output);
        }
      }
    } // if (time for #debug)

  }
#endif

  // receive all system and user messages
  {
    PROFILE_SCOPE_EX(nwSRv, network)
    ReceiveSystemMessages();
    ReceiveLocalMessages();
    ReceiveUserMessages();
  }

  PerformIntegrityInvestigations();

  #if _ENABLE_MP
  // cancel all update messages
  for (int i=0; i<_players.Size(); i++)
  {
    NetworkPlayerInfo &pInfo = _players[i];
    // statistics
    if (DiagLevel >= 3)
    {
      int nMsg, nBytes, nMsgG, nBytesG;
      _server->GetSendQueueInfo(pInfo.dpid, nMsg, nBytes, nMsgG, nBytesG);
      if (nMsg > 0 || nMsgG > 0)
            {
                if ( nMsgG < 0 )        // old style
            DiagLogF("Server to %s: pending in SendQueue: %d messages, %d bytes", (const char *)pInfo.name, nMsg, nBytes);
                else                    // new style
            DiagLogF("Server to %s: pending in SendQueue: common - %d messages, %d bytes, guaranteed - %d messages, %d bytes",
                             (const char *)pInfo.name, nMsg, nBytes, nMsgG, nBytesG);
            }
    }
    // cancel messages
/*
    int canceled = 0;
    for (int j=0; j<pInfo.objects.Size(); j++)
    {
      NetworkPlayerObjectInfo &poInfo = pInfo.objects[j];
      if (poInfo.lastCreatedMsgId != 0xFFFFFFFF && poInfo.canCancel)
      {
        HRESULT hr = _dp->CancelAsyncOperation(poInfo.lastCreatedMsgId, 0);
        if (DiagLevel > 0 && FAILED(hr))
          LogF("Server: cannot cancel message %x, error %x", poInfo.lastCreatedMsgId, hr);
        canceled++;
      }
    }
    // statistics
    if (DiagLevel >= 2)
    {
      if (canceled > 0) DiagLogF("Server to %s: canceled %d messages", (const char *)pInfo.name, canceled);
    }
*/
  }
  #endif

  DWORD tickCount = GlobalTickCount();
  if (GDebugVoNBank.IsReportReady(tickCount))
  { // Report the #debug von collected info at last
    AutoArray<ConvPair, MemAllocLocal<ConvPair, 64> > convTable;
    convTable.Realloc(_identities.Size());
    convTable.Resize(_identities.Size());
    for (int i=0; i<_identities.Size(); i++)
    {
      convTable[i]._dpnid = _identities[i].dpnid;
      convTable[i]._id = _identities[i].id;
    }
    FILE *LogFile = GetNetworkManager().GetDedicatedServerLogFile();
    if ( LogFile )
    {
      fprintf(LogFile, "*** VoN Topology Diagnostics ((***\n");
      RString response = GDebugVoNBank.GetDebugString(convTable);
      GDebugVoNBank.StopCollecting(); // other client responses are too late
      GDebugVoNBank.Reset(); // next #debug von should not contain old data
      fwrite(cc_cast(response), 1, response.GetLength(), LogFile);
      fprintf(LogFile, "*** VoN Topology Diagnostics ))***\n");
      fflush(LogFile);
    }
    else
    {
      // Set TimeStampFormat of LogF to none in order to make VoN Topology Diagnostics parser VoNDiag2FSM.exe working 
      TimeStampFormat backup = GTimeStampFormat;
      GTimeStampFormat = TSFNone;
      LogF("*** VoN Topology Diagnostics ((***");
      RString response = GDebugVoNBank.GetDebugString(convTable);
      GDebugVoNBank.StopCollecting(); // other client responses are too late
      GDebugVoNBank.Reset(); // next #debug von should not contain old data
      response = response + "*** VoN Topology Diagnostics ))***";
      LogF(response);
      GTimeStampFormat = backup;
    }
  }

#if USE_NAT_NEGOTIATION_MANAGER && _ENABLE_MP
  // Check whether some new NAT negotiations can be started and start them
  _p2pManager.Update();
#endif

  SimulateDS();

  // kick off players faded out
  CheckFadeOut();

  // destroy players kicked out
  int size = _identities.Size();
  for (int i=size-1; i>=0; i--)
  {
    PlayerIdentity &identity = _identities[i];
    if (identity.destroy && Glob.uiTime >= identity.destroyTime)
    {
      OnPlayerDestroy(identity.dpnid);
    }
  }

  // send update messages
  {
    PROFILE_SCOPE_EX(nwSSd, network)
    SendMessages();
  }

  // update _serverTime
  _serverTime = ::GetTickCount()-_serverTimeInitTickCount;
  if ( _serverTime > _nextServerTimeUpdate )
  {
    _nextServerTimeUpdate = _serverTime + ServerTimeUpdateInterval;
    // send fresh serverTime update to all clients
    ServerTimeMessage stmsg;
    _serverTime = ::GetTickCount()-_serverTimeInitTickCount; // update _serverTime
    stmsg._time = _serverTime;
    stmsg._endTime = _serverTimeOfEstimatedEnd;
    for (int i=0; i<_players.Size(); i++)
    {
      NetworkPlayerInfo &info = _players[i];
      if (info.clientState < NCSConnected) continue;
      SendMsg(info.dpid, &stmsg, NMFGuaranteed | NMFHighPriority);
    }
  }

  // calculate aggregate de-sync average
  {  
    float desyncTotal = 0;

    float avgDesync = 0;
    float maxDesync = 0;
    int nDesync = 0;
    for (int i=0; i<_players.Size(); i++)
    {
      const NetworkPlayerInfo &pnet = _players[i];
      // bot client has no de-sync by definition
      if (pnet.dpid==BOT_CLIENT) continue;
      float value = pnet._desync.GetLastValue();
      avgDesync += value, nDesync ++;
      saturateMax(maxDesync,value);
    }
    if (nDesync>0)
    {
      avgDesync /= nDesync;
      // aggregate de-sync - consider both avg and max
      desyncTotal = avgDesync*0.7f+maxDesync*0.3f;
    }
    _desyncTotal.Sample(desyncTotal,1e-3);
  }

  
  const DWORD desyncTotalPeriod = 5000;
  if (GlobalTickCount()>_desyncTotalUpdateNext)
  {
    // maintain aggregate de-sync average
    _desyncTotalAvg = _desyncTotal.GetAvg();
    _desyncTotalMax = _desyncTotal.GetMax();
    _desyncTotal.Reset();
    _desyncTotalUpdateNext = GlobalTickCount()+desyncTotalPeriod;
  }

  // avoid flickering the icon
  // check de-sync information for all players
  _connectionDesync = CDGood;
  // show worst de-sync found for any player
  float aggDesync = _desyncTotalAvg;
  if (aggDesync>PlayerIdentity::DesyncLow/100)
  {
    if (aggDesync>PlayerIdentity::DesyncHigh/100)
    {
      _connectionDesync = CDHigh;
    }
    else
    {
      _connectionDesync = CDLow;
    }
  }
  
  tickCount = GlobalTickCount();
  if (tickCount >= _pingUpdateNext)
  {
    _pingUpdateNext = tickCount + 30000;
    
    // update all players information
    for (int i=0; i<_players.Size(); i++)
    {
      NetworkPlayerInfo &pnet = _players[i];
      //if (pnet.dpid==BOT_CLIENT) continue;
      PlayerIdentity *id = FindIdentity(pnet.dpid);
      if (!id) continue;
      id->_minBandwidth = toLargeIntSat(pnet._bandwidth.GetMin()/(1024/8));
      id->_maxBandwidth = toLargeIntSat(pnet._bandwidth.GetMax()/(1024/8));
      id->_avgBandwidth = toLargeIntSat(pnet._bandwidth.GetAvg()/(1024/8));
      id->_minPing = toInt(pnet._ping.GetMin());
      id->_maxPing = toInt(pnet._ping.GetMax());
      id->_avgPing = toInt(pnet._ping.GetAvg());
      id->_desync = toLargeIntSat(pnet._desync.GetAvg()*100);
      saturate(id->_desync,0,100000);
      pnet._ping.Reset();
      pnet._bandwidth.Reset();
      pnet._desync.Reset();
    }

    // broadcast messages about all players to all players
    for (int i=0; i<_players.Size(); i++)
    {
      NetworkPlayerInfo &info = _players[i];
      if (info.clientState < NCSConnected) continue;
      for (int i=0; i<_identities.Size(); i++)
      {
        PlayerIdentity &pi = _identities[i];

        NetworkMessageType type = pi.GetNMType(NMCUpdatePosition);

        Ref<NetworkMessage> msg = ::CreateNetworkMessage(type);
        msg->time = Glob.time;
        NetworkMessageContext ctx(msg, this, info.dpid, MSG_SEND);
        ctx.SetClass(NMCUpdatePosition);

        PREPARE_TRANSFER(PlayerUpdate)

        TMError err;
        int dpnid = pi.dpnid;
        err = TRANSF_BASE(dpnid, dpnid);
        if (err != TMOK) continue;
        err = pi.TransferMsg(ctx);
        if (err != TMOK) continue;

        // send message
        //NetworkComponent::
        NetworkComponent::SendMsg(info.dpid,msg,type,NMFNone);
      }
    }
  }

  // integrity and CDKey checks
  DWORD time = GlobalTickCount();
  for (int i=0; i<_players.Size(); i++)
  {
    NetworkPlayerInfo &info = _players[i];
    for (int t=0; t<info.integrityQuestions.Size(); t++)
    {
      if (time > info.integrityQuestions[t].timeout)
      {
//((EXE_CRC_CHECK
        OnIntegrityCheckFailed(info.dpid, info.integrityQuestions[t].type,NULL,true);
//))EXE_CRC_CHECK
        info.integrityQuestions.Delete(t);
        t--;
      }
    }
    // CDKey check
    if (info.cdKeyCheck.timeout)
    {
      if (time > info.cdKeyCheck.timeout)
      {
        if (info.dpid == BOT_CLIENT)
        {
          RString errmsg = LocalizeString(IDS_MP_BAD_CDKEY);
          GChatList.Add(CCGlobal, NULL, errmsg, false, true);
          WarningMessage(errmsg);
          Done();
        }
        else
        {
          KickOff(info.dpid, KORBadCDKey);
        }
        info.cdKeyCheck.timeout = 0; //clear
      }
    }
  }

  // squad checks
  bool canSendCustomFiles = true;
  bool timeout = false;
  for (int i=0; i<_squadChecks.Size();)
  {
    CheckSquadObject *obj = _squadChecks[i];
    if (i==0)
    { //first object can be timeouted in order others can connect too
      if (_squadChecksTimeout)
      {
        if (_squadChecksTimeout < ::GetTickCount()) //timeout
          timeout = true;
      }
      else if (obj->GetState()==CheckSquadObject::SendingCustomFiles)
      {
        _squadChecksTimeout = ::GetTickCount() + 30*1000; //30 seconds
      }
    }
    if (obj->IsDone(canSendCustomFiles, this))
    {
      PlayerIdentity &identity = obj->_identity;
      Ref<SquadIdentity> squad = obj->_squad;
      if (squad)
      {
        // only new squad is passed into CreateIdentity and added into _squads
        for (int i=0; i<_squads.Size(); i++)
          if (_squads[i]->_id == identity.squadId)
          {
            squad = NULL;
            break;
          }
      }
      CreateIdentity(identity, squad);

#if _ENABLE_STEAM
      if (!obj->IsAuthenticated())
      {
        // player is not authenticated to play this game - kick off
        KickOff(identity.dpnid, KOROther);
      }
#endif

      _squadChecks.Delete(i);
      if (i==0) //reset timeout
      {
        _squadChecksTimeout = 0;
        timeout = false;
      }
    }
    else i++;
  }
  if (timeout)
  { 
    _squadChecksTimeout = 0;
    // shift the squad objects and place the first at the end
    int schSize = _squadChecks.Size();
    if (schSize>1)
    {
      SRef<CheckSquadObject> first = _squadChecks[0];
      for (int i=1; i<schSize; i++)   
      { //shift
        _squadChecks[i-1] = _squadChecks[i];
      }
      _squadChecks[schSize-1] = first;
    }
  }
  
  int timeleft = GetSessionTimeLeft();
  const DWORD minBetweenUpdates = 120000;
  DWORD now = GlobalTickCount();
  if (_timeLeftLastValue!=timeleft && now>_timeLeftLastTime+minBetweenUpdates)
  {
/*
    // do not update the session description regularly, server will ask us

    // time from time update matchmaking stats
    UpdateMatchmakingDescription(false);
*/

#if _GAMES_FOR_WINDOWS || (defined _XBOX && _XBOX_VER >= 200)
    // set the properties and contexts for matchmaking
    // - TimeLeft
    GSaveSystem.SetProperty(PROPERTY_GAME_TIMELEFT, timeleft);
#endif
  }
  else if (_timeLeftLastValueBcast!=timeleft)
  {
    // if there is any timeleft estimation, update it
    // updating session description for broadcast session is very quick
    // no network traffic required
    #if _XBOX_SECURE && _ENABLE_MP
      //! Session update for Xbox matchmaking service
      if (_parent->IsSignedIn() && !_parent->IsSystemLink()) XOnlineUpdateSession(false,true);
    #endif
    UpdateSessionDescription();
    _timeLeftLastValueBcast = timeleft;
  }

//{ QUERY & REPORTING SDK
#if _ENABLE_GAMESPY
  SimulateQR();
#endif
//}

  //{ STEAMWORKS SDK
#if _ENABLE_STEAM
  if (UseSteam) SimulateSteam();
#endif
  //}

  DoAssert(CheckIntegrityOfPendingMessages());

  //DWORD tickCount = ::GlobalTickCount();
  // check pending messages for timeout
  for (int p=0; p<_pendingMessages.Size(); p++)
  {
    NetPendingMessage &pend = _pendingMessages[p];
    NetworkObjectInfo *oInfo = pend.info;
    NetworkPlayerObjectInfo *poInfo = pend.player;
    NetworkUpdateInfo *uInfo = pend.update;
    //NetworkUpdateInfo &info = poInfo->updates[j];
    #if _ENABLE_REPORT
      // verify update is in player
      DoAssert (uInfo>=poInfo->updates && uInfo<poInfo->updates+NMCUpdateN);
      // verify player exists in given object
      bool found = false;
      for (int i=0; i<oInfo->playerObjects.Size(); i++)
      {
        if (oInfo->playerObjects[i]==poInfo) found = true;
      }
      DoAssert(found);
    #endif
    DoAssert(uInfo->lastCreatedMsgId==pend.msgID);
    if (uInfo->lastCreatedMsgId!=pend.msgID) continue;
    if (uInfo->lastCreatedMsg && tickCount > uInfo->lastCreatedMsgTime + 5000 )
    {
#if DIAG_SLOPE_SELECTION_SERVER_LAGGING
      // trial to add player name to pending messages to know, whether some specific player is causing problems
      RString pName = Format("Unknown:%d", poInfo->player);
      for (int pli=0; pli<_identities.Size(); pli++)
      {
        if (poInfo->player==_identities[pli].dpnid) pName = _identities[pli].name;
      }
      NetworkObject *netwObj = GNetworkManager.GetObject(oInfo->id);
      RptF("Server: Network message %x is pending (player: %s, (%d:%d) - %s)", 
        uInfo->lastCreatedMsgId, cc_cast(pName), 
        oInfo->id.creator, oInfo->id.id,
        netwObj ? cc_cast(netwObj->GetDebugName()) : "UNKNOWN"
      );
#else
      RptF("Server: Network message %x is pending", uInfo->lastCreatedMsgId);
#endif
      // try to find it in pending message list

      uInfo->lastCreatedMsg = NULL;
      uInfo->lastCreatedMsgId = 0xFFFFFFFF;
      uInfo->lastCreatedMsgTime = 0;

      _pendingMessages.Delete(p);
      p--;
      //DoAssert(CheckIntegrityOfPendingMessages());
    }
  }

  // check if sent confirmed for all outgoing messages
  #if _ENABLE_REPORT
  for (int o=0; o<_objects.Size(); o++)
  {
    NetworkObjectInfo *oInfo = _objects[o];
    for (int i=0; i<oInfo->playerObjects.Size(); i++)
    {
      NetworkPlayerObjectInfo &poInfo = *oInfo->playerObjects[i];
      for (int j=NMCUpdateFirst; j<NMCUpdateN; j++)
      {
        NetworkUpdateInfo &info = poInfo.updates[j];
        if (info.lastCreatedMsg && tickCount > info.lastCreatedMsgTime + 5000)
        {
          RptF("Server: Late pending discovery - message %x", info.lastCreatedMsgId);
          // try to find it in pending message list
          int pend = FindPendingMessage(info.lastCreatedMsgId);
          if (pend>=0)
          {
            _pendingMessages.Delete(pend);
            RptF("  Deleted from pending message list");
          }
          else
          {
            Fail("  Message missing in pending message list");
          }
          Fail("Late pending discovery");

          info.lastCreatedMsg = NULL;
          info.lastCreatedMsgId = 0xFFFFFFFF;
          info.lastCreatedMsgTime = 0;
        }
      }
    }
  }
  #endif

  // log all diagnostics
  WriteDiagOutput(true);

  // check for disconnected players
  for (int i=0; i<_players.Size(); i++)
  {
    NetworkPlayerInfo &info = _players[i];
    if (info.clientState != NCSBriefingRead) continue;
    if (info.dpid == BOT_CLIENT) continue;
    float age = _server->GetLastMsgAgeReliable(info.dpid);
    if (info.connectionProblemsReported)
    {
      info.connectionProblemsReported = age > 5.0f;
    }
    else if (age >= 10.0f)
    {
      LocalizedFormatedString message;
      message.SetFormat(LocalizedString::Stringtable,"STR_MP_CONNECTION_LOOSING");
      message.AddArg(LocalizedString::PlainText,info.name);
      ServerMessage(message);
      info.connectionProblemsReported = true;
    }
  }

#if _ENABLE_CHEATS
  if (_debugWindow)
  {
    _debugWindow->ResetContent();
    for (int i=0; i<_objects.Size(); i++)
    {
      NetworkObjectInfo *oInfo = _objects[i];
      if (!oInfo) continue;
      NetworkId id = oInfo->id;
      int owner = oInfo->owner;
      RString name = GetPlayerName(owner);
      RString typeName;
      if (oInfo->current[0].message)
      {
        NetworkMessageType type = oInfo->current[0].type;
        DoAssert(type >=0 && type < NMTN);
        typeName = GetMsgTypeName(type);
      }
      else typeName = "Unknown";
      BString<1024> text;
      sprintf
      (
        text, "%s %d:%d - owner %s (%x)",
        (const char *)typeName, id.creator, id.id,
        (const char *)name, owner
      );
      _debugWindow->AddString(text);
    }
  }
#endif

#if _XBOX_SECURE && _ENABLE_MP
# if _XBOX_VER >= 200
  // Session pumping not needed on Xbox 360
# else
  if (_sessionTask)
  {
    HRESULT result = XOnlineTaskContinue(_sessionTask);
    if (!SUCCEEDED(result))
    {
      RptF("XOnlineTaskContinue of session task failed with error 0x%x", result);
    }
  }
# endif // _XBOX_VER >= 200

  // peer to peer mission sharing
#ifdef PEER2PEER
  if (_p2pServer)
  {
    _p2pServer->ProcessIncomingMessages(P2PServerProcess, _p2pServer);
  }
#endif
#endif


#if _VBS2 && _VBS2_LITE
// Check if there has been a hack attempt on player size limit
// if, there has been, we randomly select offsets and apply them to random players
if(_server)
{
  if(((_players.Size() << 3) > (_VBS2_MAX_PLAYERS << 3))&&(_memoryWrite == false)) 
  {
    _memoryWrite = true;
    _nextRanUpdate = GlobalTickCount() + (DWORD) GRandGen.Gauss((1000*30),(1000*30*2),(1000*30*3));
  }
  
  if(_memoryWrite && (GlobalTickCount()>_nextRanUpdate))
  {
    DWORD nextUpdate = (DWORD) GRandGen.Gauss((1000*30),(1000*30*2),(1000*30*3));
    _nextRanUpdate = GlobalTickCount() + nextUpdate;

    int playerIndex = GRandGen.Gauss(0,0.5,_players.Size());
    if(playerIndex >= 0 && playerIndex < _players.Size())
    {
      NetworkPlayerInfo &info = _players[playerIndex];
      NetworkId &id = info.person;
      if (!id.IsNull())
      {
        NetworkObjectInfo *oInfo = GetObjectInfo(id);
        if(GNetworkManager.GetClient())
        {
          NetworkObject *netObj = GNetworkManager.GetClient()->GetObject(oInfo->id);
          Entity *obj = dyn_cast<Entity>(netObj);
          if(obj)
          {
            // raise player 100 feet above the ground. 
            Matrix4 trans = obj->Transform();
            Vector3 pos = trans.Position();
            pos += Vector3(
              GRandGen.Gauss(-500.0f,0.0f,500.0f),
              GRandGen.Gauss(-1.0f,0.0f,1.0f),
              GRandGen.Gauss(-500.0f,0.0f,500.0f));
           
            trans.SetPosition(pos);
            obj->MoveNetAware(trans);
            obj->OnPositionChanged();
          }
        }
      }
    }
  }
}
#endif

  // TODO: remove (temporary set all to all)
  // Set voice transmission tables on Xbox
//#ifdef _XBOX
  if (_server)
  {
    for (int i=0; i<_players.Size(); i++)
    {
      // if player is not fully created, he might be unable to receive VoN messages
      // this is because VoNClient is created after MAGIC_CREATE_PLAYER message is sent
      // perhaps better solution could be to create VoN client before sending the message
      // Current solution with player state gives players also some time
      // to establish and report peer to peer communication
      if (_players[i].clientState<NCSLoggedIn && _players[i].dpid!=BOT_CLIENT) continue;
      // set all other players as recipients
      int fromID = _players[i].dpid;
      AUTO_STATIC_ARRAY(int, players, 32);
      for (int j=0; j<_players.Size(); j++)
      {
        if (_players[j].clientState<NCSLoggedIn && _players[j].dpid!=BOT_CLIENT) continue;
        int toID = _players[j].dpid;
        if (toID != fromID) players.Add(toID);
#if HEAR_YOURSELF
        else if (hearYourself) players.Add(toID);
#endif
      }
      _server->SetTransmitTargets(fromID, players);
    }
    _server->UpdateTransmitTargets();
  }
//#endif

#if _USE_BATTL_EYE_SERVER
  if (!_beIntegration.Run(this))
  {
    // reinitialization after auto-update failed, need to shut-down the server
    Done();
  }
#if 0
  // simple test of ExecuteCommand functioinality
  static volatile int doIt = -1; //set it to 1000 or something and wait
  if (doIt>0) doIt--;
  else if (!doIt)
  {
    doIt--;
    BattlEye::ExecuteCommand("#restart");
  }
#endif
#endif
}

void NetworkServer::FastUpdate()
{
//{ QUERY & REPORTING SDK
#if _ENABLE_GAMESPY
  SimulateQR();
#endif
//}
}

void NetworkServer::OnDraw()
{
  static bool lagDiags = false;
    
  if (GInput.GetCheatXToDo(CXTLag))
  {
    lagDiags = !lagDiags;
    GlobalShowMessage(500,"Lag diags %s",lagDiags ? "On":"Off");
  }
  if (lagDiags)
  {
    static float minLagToShow = 1.0f;
    static float badLag = 100.0;
    float x = 0.05f;
    float y = 0.08f;
    float width = 0.80f;
    Font *font=GEngine->GetDebugFont();
    #ifdef _XBOX
      float size = 0.03;
    #else
      float size = 0.02;
    #endif
    for (int i=0; i<_players.Size(); i++)
    {
      NetworkPlayerInfo &pnet = _players[i];
      float maxLag = pnet._desync.GetMax();
      if (maxLag<minLagToShow) continue;
      float lag = pnet._desync.GetLastValue();
      // lag 100 is usually considered very bad
      float lagRelative = floatMin(lag/badLag,1);
      PackedColor color(0x40,0x40,0,0x60);
      PackedColor nameColor(0xff,0xff,0xff,0xc0);
      // draw player name and lag indication
      Rect2DFloat rect;
      rect.x = x;
      rect.y = y;
      rect.w = lagRelative*width;
      rect.h = size;
      Rect2DAbs rect2D;
      GEngine->Convert(rect2D,rect);
      Point2DAbs pos(rect2D.x,rect2D.y);
      MipInfo mip = GEngine->TextBank()->UseMipmap(NULL, 0, 0);
      GEngine->Draw2D(mip,color,rect2D);
      
      y+= size; // proceed to a next line
      GEngine->DrawTextF(
        pos,size,font,nameColor,0,"%s - %g, max %g, avg %g",
        cc_cast(pnet.name),lag,pnet._desync.GetMax(),pnet._desync.GetAvg()
      );
    }
  }
}

DEFINE_FAST_ALLOCATOR(NetworkObjectInfo)


int NetworkServer::FindPendingMessage(NetworkUpdateInfo *update) const
{
  // try to find this message in pending message list
  for (int i=0; i<_pendingMessages.Size(); i++)
  {
    if (_pendingMessages[i].update==update) return i;
  }
  return -1;
}

int NetworkServer::FindPendingMessage(DWORD msgID) const
{
  // try to find this message in pending message list
  for (int i=0; i<_pendingMessages.Size(); i++)
  {
    if (_pendingMessages[i].msgID==msgID) return i;
  }
  return -1;
}

void NetworkServer::AddPendingMessage
(
  DWORD msgID, NetworkObjectInfo *info,
  NetworkUpdateInfo *update, NetworkPlayerObjectInfo *player
)
{
  DoAssert (info);
  DoAssert (msgID!=MSGID_REPLACE);
  /*
  for (int i=0; i<_pendingMessages.Size(); i++)
  {
    const NetPendingMessage &pend = _pendingMessages[i];
    if (pend.msgID==msgID && pend.info==info) return;
  }
  */
  NetPendingMessage &pend = _pendingMessages.Append();
  pend.msgID = msgID;
  pend.info = info;
  pend.player = player;
  pend.update = update;
  // note: similiar message (same msgId and info) may be already pending
  // two updates for one object may travel in one message

  //pend.update = update;
  //LogF("Add pending message %x: object (%d:%d)",msgID,info->id.creator,info->id.id);
  #if 0 //_ENABLE_REPORT
    if (_pendingMessages.Size()%100==0)
    {
      LogF("%d pending messages",_pendingMessages.Size());
    }
  #endif
}

bool NetworkUpdateInfo::OnSendComplete(bool ok)
{
#if LOG_SEND_PROCESS1
  DWORD idBak = lastCreatedMsgId;
#endif
  if (!lastCreatedMsg)
  {
    Fail("Message ID without message");
    lastCreatedMsgId = 0xFFFFFFFF;
    lastCreatedMsgTime = 0;
    return false;
  }
  if (ok)
  {
    ADD_COUNTER(netCN, 1);
    ADD_COUNTER(netCS, lastCreatedMsg->size);
    // message sent
    lastSentMsg = lastCreatedMsg;
    errorValid = false;
  }
  else
  {
    ADD_COUNTER(netFN, 1);
    ADD_COUNTER(netFS, lastCreatedMsg->size);
    if (DiagLevel >= 4)
    {
      DiagLogF("Server: sent of message %x failed", lastCreatedMsgId);
    }
  }
  lastCreatedMsg = NULL;
  lastCreatedMsgId = 0xFFFFFFFF;
  lastCreatedMsgTime = 0;
#if LOG_SEND_PROCESS1
  LogF("  - update info %x updated (ok=%d)", idBak, ok);
#endif
  return true;
}

/*!
\patch 1.76 Date 6/13/2002 by Ondra
- Optimized: Dedicated server CPU load improved, especially when many players are connected.
*/

void NetworkServer::OnSendComplete(DWORD msgID, bool ok)
{
#if LOG_SEND_PROCESS
  LogF("Server: Send %x completed: %s", msgID, ok ? "ok" : "failed");
#endif
  // try to find this message in pending message list
  //DoAssert(CheckIntegrityOfPendingMessages());
  int foundCount = 0;
  for (int i=0; i<_pendingMessages.Size(); i++)
  {
    if (_pendingMessages[i].msgID==msgID)
    {
      //LogF("Pending message match %d of %d",i,_pendingMessages.Size());
      // we have object candidate - search in it
      NetworkObjectInfo *oInfo = _pendingMessages[i].info;
      NetworkPlayerObjectInfo *poInfo = _pendingMessages[i].player;
      NetworkUpdateInfo *uInfo = _pendingMessages[i].update;
      #if _ENABLE_REPORT
        // verify update is in player
        DoAssert (uInfo>=poInfo->updates && uInfo<poInfo->updates+NMCUpdateN);
        // verify player exists in given object
        bool found = false;
        for (int j=0; j<oInfo->playerObjects.Size(); j++)
        {
          if (oInfo->playerObjects[j]==poInfo) found = true;
        }
        DoAssert(found);
      #endif
      DoAssert(uInfo->lastCreatedMsgId==msgID);
      if (uInfo->lastCreatedMsgId == msgID)
      {
        #if LOG_SEND_PROCESS1
        LogF
        (
          "Processed pending message %x (%d:%d)",msgID,
          oInfo->id.creator,oInfo->id.id
        );
        #endif
        uInfo->OnSendComplete(ok);
        foundCount++;
      }
      // pending message processed - delete it
      _pendingMessages.Delete(i);
      i--;
      /*
      if(!CheckIntegrityOfPendingMessages())
      {
        RptF("Message %x processed",msgID);
        Fail("!CheckIntegrityOfPendingMessages()");
      }
      */
    }
  }
  if (foundCount>0)
  {
    return;
  }
  #if _ENABLE_REPORT
  //RptF("message %x not found in pending messages",msgID);
  // if it failed, it is probably some bug
  // to be robust, we will perform complete dumb search

  for (int o=0; o<_objects.Size(); o++)
  {
    NetworkObjectInfo &oInfo = *_objects[o];
    for (int i=0; i<oInfo.playerObjects.Size(); i++)
    {
      NetworkPlayerObjectInfo &poInfo = *oInfo.playerObjects[i];
      for (int j=NMCUpdateFirst; j<NMCUpdateN; j++)
      {
        NetworkUpdateInfo &info = poInfo.updates[j];
        if (info.lastCreatedMsgId == msgID)
        {
          ErrF("dumb search found pending message %x",msgID);
          info.OnSendComplete(ok);
          // note: multiple logical messages may be connected with one message id
        }
      }
    }
  }
  #endif
}

/*!
\patch 1.47 Date 3/7/2002 by Ondra
- New: Player ID now displayed in server console when player is connected.
\patch 1.56 Date 5/13/2002 by Ondra
- New: When player is kicked off or banned, specific message is shown.
\patch 5153 Date 4/17/2007 by Jirka
- Fixed: When failed to join to a locked server, the correct cause is shown now.
*/

void NetworkServer::OnCreatePlayer(int player, bool privateSlot, const char *name, unsigned long inaddr)
{
  if (_sessionLocked)
  {
    // session is locked
    _server->KickOff(player, NTRSessionLocked);
    return;
  }

  OnPlayerCreate(player, privateSlot, name, inaddr);
}

bool NetworkServer::OnCreateVoicePlayer(int player)
{
  NetworkPlayerInfo *info = GetPlayerInfo(player);
  if (!info) return false;
  info->dvid = player;
  return true;
}

void NetworkServer::GetPlayers(AutoArray<NetPlayerInfo, MemAllocSA> &players)
{
  players.Resize(0);
  for (int i=0; i<_players.Size(); i++)
  {
    int index = players.Add();
    players[index].dpid = _players[i].dpid;
    players[index].name = _players[i].name;
  }
}

bool NetworkServer::CheckPlayerInLobby(const NetworkPlayerInfo &info) const
{
  if (info.clientState>=NCSBriefingShown && info.clientState<=NCSBriefingRead)
  {
    // in-game - check if alive
    // this may be difficult, as we are server, and should not have any access to a game world?
    // find a network object describing the players unit
    const NetworkObjectInfo *obj = GetObjectInfo(info.unit);
    // if the unit is NULL, session was most likely already terminated on the server
    if (!obj) return true;
    const NetworkCurrentInfo &curInfo = obj->current[NMCUpdateGeneric];
    // HOTFIX: probably possible in some rare conditions (crash occured)
    if (!curInfo.message) return false;
    // unless proven otherwise, assume alive
    int lifeState = LifeStateAlive;
    if (curInfo.type == NMTUpdateAIBrain || curInfo.type == NMTUpdateAIUnit || curInfo.type == NMTUpdateAIAgent)
    {
      // NMTUpdateAIUnit and NMTUpdateAIAgent are derived from NMTUpdateAIBrain
      lifeState = static_cast<NetworkMessageUpdateAIBrain *>(curInfo.message.GetRef())->_lifeState;
    }
    if (lifeState==LifeStateDead)
    {
      return true;
    }
  }
  else
  {
    // not in-game - force lobby
    return true;
  }
  return false;
}

void NetworkServer::GetPlayersOnChannel(AutoArray<int, MemAllocSA> &players, AutoArray<NetworkId> units, DWORD from, bool voice)
{
  players.Resize(0);
  for (int i=0; i<_players.Size(); i++)
  {
    NetworkPlayerInfo &info = _players[i];
    if (info.dpid == from) continue;
    if (info.clientState < NCSConnected) continue;
    for (int j=0; j<units.Size(); j++)
    {
      if (units[j] == info.person)
      {
        if (voice)
        {
          if (info.dvid != 0) players.Add(info.dvid);
        }
        else
          players.Add(info.dpid);
        break;
      }
    }
  }
}

/*!
\patch 5198 Date 12/12/2007 by Ondra
- Fixed: Dead players can no longer talk or write to in-game players unless they are admins.
*/
void NetworkServer::GetPlayersOnChannel(AutoArray<int, MemAllocSA> &players, int channel, DWORD from, bool voice)
{
  players.Resize(0);
  if (channel==CCGroup)
  {
    // CCGroup may mean dead/lobby channel
    for (int i=0; i<_players.Size(); i++)
    {
      const NetworkPlayerInfo &info = _players[i];
      if (info.dpid == from)
      {
        // check if the player is alive and in-game
        // if not, handle CCGlobal / CCGroup as needed
        if (CheckPlayerInLobby(info))
        {
          // now we need to send the message only to other players in the lobby or dead
          for (int p=0; p<_players.Size(); p++)
          {
            const NetworkPlayerInfo &pInfo = _players[p];
            if (pInfo.clientState>=NCSConnected && pInfo.dpid != from && CheckPlayerInLobby(pInfo))
            {
              if (voice)
              {
                if (pInfo.dvid != 0) players.Add(pInfo.dvid);
              }
              else
              {
                players.Add(pInfo.dpid);
              }
            }
          }
          // once we are done with sending the lobby message, we stop any other processing
          return;
        }
        // once the player was found, no need to search for any other
        break;
      }
    }
  }
  switch (channel)
  {
  case CCNone:
  case CCDirect:
    return;
  case CCGlobal:
    for (int i=0; i<_players.Size(); i++)
    {
      const NetworkPlayerInfo &info = _players[i];
      if (info.dpid == from) continue;
      if (info.clientState < NCSConnected) continue;
      if (voice)
      {
        #ifdef _XBOX
          DoAssert(info.dvid==info.dpid);
        #endif
        if (info.dvid != 0) players.Add(info.dvid);
      }
      else
        players.Add(info.dpid);
    }
    break;
  case CCSide:
    {
      TargetSide side = TSideUnknown;
      // find side
      // TODO: use _playerSides instead
      for (int i=0; i<_playerRoles.Size(); i++)
      {
        PlayerRole &role = _playerRoles[i];
        if (role.player == from)
        {
          side = role.side;
          break;
        }
      }
      if (side != TSideUnknown)
      {
        for (int i=0; i<_playerRoles.Size(); i++)
        {
          PlayerRole &role = _playerRoles[i];
          if (role.player == from) continue;
          if (role.side != side) continue;
          NetworkPlayerInfo *info = GetPlayerInfo(role.player);
          if (!info) continue;
          if (info->clientState < NCSConnected) continue;
          if (voice)
          {
            if (info->dvid != 0) players.Add(info->dvid);
          }
          else
            players.Add(info->dpid);
        }
      }
    }
    break;
  case CCCommand:
    {
      TargetSide side = TSideUnknown;
      // find side
      // TODO: use _playerSides instead
      for (int i=0; i<_playerRoles.Size(); i++)
      {
        PlayerRole &role = _playerRoles[i];
        if (role.player == from)
        {
          side = role.side;
          break;
        }
      }
      if (side != TSideUnknown)
      {
        for (int i=0; i<_playerRoles.Size(); i++)
        {
          PlayerRole &role = _playerRoles[i];
          if (role.player == from) continue;
          if (role.side != side) continue;
          if (!role.leader) continue;
          NetworkPlayerInfo *info = GetPlayerInfo(role.player);
          if (!info) continue;
          if (info->clientState < NCSConnected) continue;
          if (voice)
          {
            if (info->dvid != 0) players.Add(info->dvid);
          }
          else
            players.Add(info->dpid);
        }
      }
    }
    break;
  case CCGroup:
    {
      TargetSide side = TSideUnknown;
      int group = -1;
      // find group
      for (int i=0; i<_playerRoles.Size(); i++)
      {
        PlayerRole &role = _playerRoles[i];
        if (role.player == from)
        {
          side = role.side;
          group = role.group;
          break;
        }
      }
      // send message to all players with this group
      if (group >= 0)
      {
        for (int i=0; i<_playerRoles.Size(); i++)
        {
          PlayerRole &role = _playerRoles[i];
          if (role.player == from) continue;
          if (role.side != side || role.group != group) continue;
          NetworkPlayerInfo *info = GetPlayerInfo(role.player);
          if (!info) continue;
          if (info->clientState < NCSConnected) continue;
          if (voice)
          {
            if (info->dvid != 0) players.Add(info->dvid);
          }
          else
            players.Add(info->dpid);
        }
      }
    }
    break;
  case CCVehicle:
    {
      TargetSide side = TSideUnknown;
      int group = -1;
      int unit = -1;
      // find unit
      for (int i=0; i<_playerRoles.Size(); i++)
      {
        PlayerRole &role = _playerRoles[i];
        if (role.player == from)
        {
          side = role.side;
          group = role.group;
          unit = role.unit;
          break;
        }
      }
      // send message to all players with this group
      if (group >= 0)
      {
        for (int i=0; i<_playerRoles.Size(); i++)
        {
          PlayerRole &role = _playerRoles[i];
          if (role.player == from) continue;
          if
          (
            role.side != side || role.group != group ||
            role.unit != unit
          ) continue;
          NetworkPlayerInfo *info = GetPlayerInfo(role.player);
          if (!info) continue;
          if (info->clientState < NCSConnected) continue;
          if (voice)
          {
            if (info->dvid != 0) players.Add(info->dvid);
          }
          else
            players.Add(info->dpid);
        }
      }
    }
    break;
  }
}

void NetworkServer::InitSignatureCollecting(NetworkPlayerInfo *info)
{
  // last DataSignatureAnswer with index CollectEndIndex finishes it
  info->hashes.StartCollecting();
#if 0
  // do not send CollectEndIndex packet, as the collecting is run asynchronously on client
  // the client itself should send the answer after the collecting is over
  DataSignatureAskMessage collectEndMsg;
  collectEndMsg._index = CollectedHashes::CollectEndIndex;
  collectEndMsg._level = 0;
  SendMsg(info->dpid, &collectEndMsg, NMFGuaranteed);
#endif
  // add the question to the list of pending questions
  info->signatureChecks.Add(CollectedHashes::CollectEndIndex);
}

RString CollectedHashes::RandomUsedFile(Vector3Par pos, int level)
{
  const int CandidatesMaxCount = 5;
  AutoArray<RandomUsedFileCandidate, MemAllocLocal<RandomUsedFileCandidate, CandidatesMaxCount> > candidates;
  for (int i=0; i<10; i++) //max 10 tries to get five non-empty candidates
  {
    RString file = GWorld->RandomUsedFile(pos);
    if (file.IsEmpty()) continue;
    QFBank *bank = QFBankQueryFunctions::AutoBank(file);
    if (bank)
    {
      RString prefix = bank->GetPrefix();
      // avoid duplicate banks in candidates
      bool duplicate = false;
      for (int j=0; j<candidates.Size(); j++)
      {
        if (candidates[j].prefix==prefix) { duplicate=true; break; }
      }
      if (duplicate) continue;
      int ix=candidates.Add(RandomUsedFileCandidate(file, prefix));
      if ( ix==(CandidatesMaxCount-1) ) break; // all candidates collected
    }
  }
  if (candidates.Size()>0)
  {
    int bestIx = 0; int bestAsksCount = INT_MAX;
    for (int i=0; i<candidates.Size(); i++)
    {
      RandomUsedFileCandidate &newCandidate = candidates[i];
      RandomUsedFileCandidate &storedCandidate = _candidatesMap[newCandidate.prefix];
      if (_candidatesMap.IsNull(storedCandidate))
      { // add it
        _candidatesMap.Add(newCandidate);
        bestIx = i;
        bestAsksCount = 0;
        break; //further search is pointless
      }
      else
      { // already exists
        if ( storedCandidate.levelAsksCount[level] < bestAsksCount )
        { // update best
          bestIx = i;
          bestAsksCount = storedCandidate.levelAsksCount[level];
        }
      }
    }
    // increase the level used count in stored hash table
    RandomUsedFileCandidate &bestStoredCandidate = _candidatesMap[candidates[bestIx].prefix];
    if (!_candidatesMap.IsNull(bestStoredCandidate))
    {
      bestStoredCandidate.levelAsksCount[level]++;
    }
    return candidates[bestIx].filename;
  }
  return RString(); //no random file found (hopefully rare situation)
}

/*!
\patch 1.12 Date 08/08/2001 by Jirka
- Changed: squad validation on server is now processed asynchronously
\patch 1.22 Date 09/04/2001 by Jirka
- Changed: Messages "Player connected", "Player disconnected" are broadcasts over network
\patch 1.27 Date 10/17/2001 by Jirka
- Fixed: Player identity is not registered if player disconnect meanwhile.
 This caused un-kickable ghost players appearing in player list.
\patch 1.34 Date 12/6/2001 by Jirka
- Fixed: squad logos on dedicated server game
\patch_internal 1.46 Date 3/4/2002 by Jirka
- Changed: random integrity checks disabled (does not work fine) 
*/

void NetworkServer::CreateIdentity(PlayerIdentity &ident, Ref<SquadIdentity> squad)
{
  NetworkPlayerInfo *info = GetPlayerInfo(ident.dpnid);
  // check if player is still connected
  if (!info)
  {
    // player already disconnected
    return;
  }

  int index = _identities.Add(ident);
  PlayerIdentity &identity = _identities[index];
  identity.playerid = NextPlayerID();

  // check for double id
  __int64 id64 = identity.GetIntID();
  if (id64 != 0)
  {
    // check only when id!=0
    // for id==0 we have better handling
    // id==999 is no longer used in Multiplayer Demo (DEV1,DEV2,...,DEV999 are used instead)
    for (int i=0; i<_identities.Size(); i++)
    {
      if (i == index) continue;

      if (_identities[i].id == identity.id)
      {
        OnDoubleIdDetected(identity, _identities[i]);
        // check if player was kicked out
        if (identity.destroy) return;
        break;
      }
    }
  }

#if _ENABLE_GAMESPY && _VERIFY_CLIENT_KEY
  if (_validateCDKey)
  {
    char buffer[9];
    for (int i=0; i<lenof(buffer)-1; i++) buffer[i] = 'a' + rand() % 26;
    buffer[lenof(buffer) - 1] = 0;
    info->challenge = buffer;
    // initial CD Key test
    CDKeyCheckAskMessage ask;
    ask._challenge = info->challenge;
    SendMsg(identity.dpnid, &ask, NMFGuaranteed);
    info->cdKeyCheck.timeout = GlobalTickCount()+30000; //30 seconds
  }
#endif

  if (identity.dpnid == BOT_CLIENT)
    identity._rights |= PRServer;
  else
  {
    if (_verifySignatures)
    {
      if (_sigVerRequired<2)
      {
        // initial test of signatures
        DataSignatureAskMessage ask;
        ask._index = -2;
        ask._level = 0; // shallow check
        SendMsg(identity.dpnid, &ask, NMFGuaranteed);
        InitSignatureCollecting(info);
      }
      else
      {
        // initial test using level 2 check
        DataSignatureAskMessage askLev2;
        askLev2._index = -2;
        askLev2._level = 2; //HashOf(nonDataHash+fileListHash+pboPrefix) is used
        SendMsg(identity.dpnid, &askLev2, NMFGuaranteed);
        InitSignatureCollecting(info);
      }
    }
  }

#if LOG_PLAYERS
LogF
(
  "Server: Identity added - name %s, id %d (total %d identities, %d players)",
  (const char *)ident.name, ident.dpnid, _identities.Size(), _players.Size()
);
#endif

  // send squad file to all players except new (do not send twice)
  if (squad)
  {
    // add squad but do not send it immediately
    _squads.Add(squad);
    RString src, dst;
    for (int i=0; i<_identities.Size(); i++)
      if (i != index)
      { // do not send picture! It was transfered during SquadCheck process.
        SendMsg(_identities[i].dpnid, squad, NMFGuaranteed);
      }
//{ DEDICATED SERVER SUPPORT
#if _ENABLE_DEDICATED_SERVER
    if (_dedicated)
    {
      NetworkClient *client = _parent->GetClient();
      DoAssert(client && client->GetPlayer() != identity.dpnid);
      // send new identity to bot client
      SendMsg(client->GetPlayer(), squad, NMFGuaranteed);
    }
#endif
//}
  }

  identity.name = info->name;
  identity.clientState = NCSConnected;
#if _GAMES_FOR_WINDOWS || defined _XBOX
  identity.privateSlot = info->privateSlot;
#endif
#ifndef _XBOX
  /// send the squads to new player
  for (int i=0; i<_squads.Size(); i++)
  {
    SquadIdentity *squad = _squads[i];
    if (squad->_picture.GetLength() > 0)
    {
      RString src = GetServerTmpDir() + RString("\\squads\\") + squad->_nick + RString("\\") + squad->_picture;
      if (QIFileFunctions::FileExists(src))
      {
        // char '$' is used as an indication of Local Settings directory (different on client than on server)
        RString dst = RString("$\\squads\\") + squad->_nick + RString("\\") + squad->_picture;
        TransferFile(identity.dpnid, dst, src);
      }
    }
    SendMsg(identity.dpnid, squad, NMFGuaranteed);
  }

#endif

  // Send list of current players which custom files should be removed (as new one will be sent)
  DeleteCustomFilesMessage dcfMsg;
  for (int i=0; i<_identities.Size(); i++)
  {
    if (i != index) dcfMsg._list.Add(_identities[i].name);
  }
  if (dcfMsg._list.Size()) SendMsg(identity.dpnid, &dcfMsg, NMFGuaranteed);

  for (int i=0; i<_identities.Size(); i++)
  {
    PlayerIdentity &player = _identities[i];
    if (i != index)
    {
#ifndef _XBOX
      // send all faces and sounds to new player
      {
        if (stricmp(player.face, "custom") == 0)
          TransferFace(identity.dpnid, player.name);
        TransferCustomRadio(identity.dpnid, player.name);
      }
#endif
      // send all identities to new player
      SendMsg(identity.dpnid, &player, NMFGuaranteed);

    }
    // send new identity to all players
    SendMsg(player.dpnid, &identity, NMFGuaranteed);
  }

#if _ENABLE_MP
  // send mute list to a new player
  SendMuteList(identity);
#endif

//{ DEDICATED SERVER SUPPORT
#if _ENABLE_DEDICATED_SERVER
  if (_dedicated)
  {
    NetworkClient *client = _parent->GetClient();
    DoAssert(client && client->GetPlayer() != identity.dpnid);
    // send new identity to bot client
    SendMsg(client->GetPlayer(), &identity, NMFGuaranteed);
  }
#endif
//}

/*
  info->state = NGSLogin;

  // all message formats registered, we may send messages
  // send state dependent info 
  if (_state >= NGSPrepareSide)
  {
    // send mission info
    SendMissionInfo(info->dpid);
    info->state = NGSPrepareSide;
    info->missionFileValid = false;
  }
  // send current state
  ChangeGameState gs(_state);
  SendMsg(info->dpid, &gs, NMFGuaranteed);

  // if game is already in progress, send how long is played already
  if (_state == NGSPlay)
  {
    NetworkCommandMessage msg;
    msg.type = NCMTMissionTimeElapsed;
    int timeElapsed = GlobalTickCount() - _missionHeader.start;
    msg.content.Write(&timeElapsed, sizeof(timeElapsed));
    SendMsg(info->dpid, &msg, NMFGuaranteed);
  }

  identity.state = info->state;

  // send player state to all clients
  SetPlayerState(info->dpid, info->state);
*/

  // Send server state to client
  ServerStateMessage msg;
  msg._serverState = _serverState;
  SendMsg(identity.dpnid, &msg, NMFGuaranteed);

  identity._minPing = 10;
  identity._avgPing = 100;
  identity._maxPing = 1000;

  identity._minBandwidth = 14;
  identity._avgBandwidth = 28;
  identity._maxBandwidth = 33;

  // Send chat message about player is connected
  if (identity.dpnid != BOT_CLIENT)
  {
    LocalizedFormatedString message;
    message.SetFormat(LocalizedString::Stringtable,"STR_MP_CONNECT");
    message.AddArg(LocalizedString::PlainText,identity.name);
    RefArray<NetworkObject> dummy;
    GNetworkManager.LocalizedChat(CCGlobal, "", dummy, message);
    
    GChatList.Add(CCGlobal, NULL, message.GetLocalizedValue(), false, true);

    // make sure ping information will be refreshed soon
    DWORD updatePingTime = GlobalTickCount() + 5000;
    if (_pingUpdateNext>updatePingTime) _pingUpdateNext = updatePingTime;
    #if _ENABLE_DEDICATED_SERVER
      if (_dedicated)
      {
        ConsoleF
        (
          LocalizeString(IDS_DS_PLAYER_CONNECTED),
          (const char *)identity.name, (const char *)identity.id
        );
      }
    #endif
  }

  // send basic integrity questions
  // do not send integrity question to myself (always botclient)
  if (info->dpid != BOT_CLIENT)
  {
    // if player is using different version, it is impossible to check integrity
    int ver = identity.version;
    if (ver!=MP_VERSION_ACTUAL)
    {
      // never check
      info->integrityCheckNext = UINT_MAX;
      char buf[256];
      if (ver)
      {
        sprintf(buf,"%d.%02d",ver/100,ver%100);
      }
      else
      {
        strcpy(buf,"?.??");
      }
      OnIntegrityCheckFailed(info->dpid,IQTConfig,buf,true);
    }
    else
    {
      PerformInitialIntegrityCheck(*info);
      #if _ENABLE_RANDOM_INTEGRITY_CHECK
        info->integrityCheckNext = GlobalTickCount()+17000;
      #else
        info->integrityCheckNext = UINT_MAX;
      #endif
//((EXE_CRC_CHECK
      info->integrityCheckNextIndex = 1; // test 0 was processed during PerformInitialIntegrityCheck
//))EXE_CRC_CHECK
    }
  }

  UpdateMatchmakingDescription(false);

#if _ENABLE_DEDICATED_SERVER
  if (_serverState == NSSSelectingMission && _mission.GetLength() == 1 && _mission[0] == '?')
  {
    NetworkCommandMessage answer;
    answer._type = NCMTVoteMission;
    NetworkCommandMessage answer2;
    answer2._type = NCMTVoteMissionExt;
    AddMissionList(answer,answer2);
    for (int i=0; i<_identities.Size(); i++)
    {
      SendMsg(identity.dpnid, &answer, NMFGuaranteed);
      SendMsg(identity.dpnid, &answer2, NMFGuaranteed);
    }
  }
#endif

  // if you want first player to become admin, it can be done here
#if 0 // _ENABLE_DEDICATED_SERVER && _XBOX_SECURE
  if (_dedicated && _gameMaster == AI_PLAYER) SetGameMaster(identity.dpnid, true);
#endif

#if _ENABLE_DEDICATED_SERVER
  // event handler
  OnEvent(SEOnUserConnected, identity.playerid);
#endif

}

#ifndef _WIN32
extern bool interrupted;
#endif


static bool CheckValidUpload(RString path, RString name)
{
  RString prefixShouldBe = GetServerTmpDir() + RString("\\players\\") + EncodeFileName(name) + RString("\\");
  return !strnicmp(path,prefixShouldBe,prefixShouldBe.GetLength());
}

static RString GetRelUploadPath(RString path, RString name)
{
  RString prefixShouldBe = GetServerTmpDir() + RString("\\players\\") + EncodeFileName(name) + RString("\\");
  if (strnicmp(path,prefixShouldBe,prefixShouldBe.GetLength())) return path;
  return path.Substring(prefixShouldBe.GetLength(),INT_MAX);
}

int NetworkPlayerInfo::FindIntegrityQuestion
(
  IntegrityQuestionType type, const IntegrityQuestion &q
) const
{
  for (int i=0; i<integrityQuestions.Size(); i++)
  {
    const IntegrityQuestionInfo &qi = integrityQuestions[i];
    if (qi.type==type && qi.q.name == q.name && qi.q.offset==q.offset && qi.q.size==q.size) return i;
  }
  return -1;
}

int NetworkPlayerInfo::FindIntegrityQuestion(IntegrityQuestionType type) const
{
  for (int i=0; i<integrityQuestions.Size(); i++)
  {
    const IntegrityQuestionInfo &qi = integrityQuestions[i];
    if (qi.type==type) return i;
  }
  return -1;
}

int NetworkPlayerInfo::FindIntegrityQuestion(int id) const
{
  for (int i=0; i<integrityQuestions.Size(); i++)
  {
    const IntegrityQuestionInfo &qi = integrityQuestions[i];
    if (qi.id==id) return i;
  }
  return -1;
}

void NetworkServer::UpdateWeaponsPool()
{
  // send to all except from (state Mission Received expected)
  for (int i=0; i<_players.Size(); i++)
  {
    NetworkPlayerInfo &info = _players[i];
    //if (info.clientState < NCSMissionReceived) continue;
    SendMsg(info.dpid, &GWeaponsPool._weaponsPool, NMFGuaranteed);
  }
}

void NetworkServer::UpdateMagazinesPool()
{
  // send to all except from (state Mission Received expected)
  for (int i=0; i<_players.Size(); i++)
  {
    NetworkPlayerInfo &info = _players[i];
   // if (info.clientState < NCSMissionReceived) continue;
    SendMsg(info.dpid, &GWeaponsPool._magazinesPool, NMFGuaranteed);
  }
}

void NetworkServer::UpdateBackpacksPool()
{
  // send to all except from (state Mission Received expected)
  for (int i=0; i<_players.Size(); i++)
  {
    NetworkPlayerInfo &info = _players[i];
 //   if (info.clientState < NCSMissionReceived) continue;
    SendMsg(info.dpid, &GWeaponsPool._backpacksPool, NMFGuaranteed);
  }
}


#define EXTRACT_ID(name) objectId = message->_##name;
#define EXTRACT_UNITS units = message->_units;
#define EXTRACT_CHANNEL channel = message->_channel;

DEFINE_FAST_ALLOCATOR(DelayedMessageInfo)

DelayedMessageInfo::DelayedMessageInfo(NetworkMessageType t, NetworkMessage *m)
:msg(m),type(t)
{
  error=1;
  invMaxAge = 1.0f/10;
  timeReceived = Glob.time;
  switch (type)
  {
    case NMTFireWeapon:
    {
      NetworkMessageFireWeapon *msg = static_cast<NetworkMessageFireWeapon *>(m);
      static float visibleErrorCoef = 2.0f;
      error = msg->_visible*visibleErrorCoef;
      pos = msg->_pos;
      invMaxAge = 1.0f/5;
      break;
    }
    case NMTExplosionDamageEffects:
    {
      NetworkMessageExplosionDamageEffects *msg = static_cast<NetworkMessageExplosionDamageEffects *>(m);
      invMaxAge = 1.0f/20;

	    // undefined camera position
	    pos = msg->_pos;
	    // if ammo type is not defined, assume some default importance
	    error = 1;
      if (msg->_type.GetLength() > 0)
      {
        Ref<EntityType> type = VehicleTypes.New(msg->_type);
        AmmoType *ammo = dynamic_cast<AmmoType *>(type.GetRef());
        if (ammo)
        {
          // cf. Landscape::ExplosionDamageEffects
          if (ammo->explosive<0.5f)
          {
            // small explosion - bullet hit only
            static float smallExplosionError = 0.5f;
            error = smallExplosionError;
          }
          else
          {
            static float bigExplosionError = 10.0f;
            error = ammo->explosive*ammo->indirectHitRange*bigExplosionError;
          }
        }
      }
      break; 
    }
  }
}

DEFINE_FAST_ALLOCATOR(WaitingMessageInfo)

/*!
\patch 5164 Date 10/3/2007 by Jirka
- Fixed: Handling of invalid CD keys on dedicated servers improved
*/

#if _ENABLE_GAMESPY && _VERIFY_CLIENT_KEY
void NetworkServer::OnCDKeyAuthorize(int player, int authenticated, const char *errmsg)
{
  if (!authenticated)
  {
    // wrong CD Key - kick off client, shut down server
    if (player == BOT_CLIENT)
    {
      GChatList.Add(CCGlobal, NULL, errmsg, false, true);
      WarningMessage(errmsg);
/*
      WarningMessage(LocalizeString(keyInUse ? IDS_MP_CDKEY_IN_USE : IDS_MP_BAD_CDKEY));
*/
      Done();
    }
    else
    {
/*
      // using DisconnectMessage is not sufficient as we need the kick being immediate
      //   "Note that this is of course when the ArmA client does not disconnect itself, 
      //    naturally the server itself has to do this to have control and not to allow
      //    the player to stay on the server with some exploit"
      DisconnectMessage msg;
      msg._message = errmsg;
      SendMsg(player, &msg, NMFGuaranteed);
*/
      bool keyInUse = strcmp(errmsg, "CD Key in use") == 0;
      KickOff(player, keyInUse ? KORCDKeyInUse : KORBadCDKey, errmsg);
    }
/*
    NetworkPlayerInfo *info = GetPlayerInfo(player);
    LogF("%f: Player %s (%d) was not authenticated (%s)", 1e-3 * GlobalTickCount(), info ? cc_cast(info->name) : "<unknown>", player, errmsg);
*/
  }
  else
  {
/*
    NetworkPlayerInfo *info = GetPlayerInfo(player);
    LogF("%f: Player %s (%d) was successfully authenticated (%s)", 1e-3 * GlobalTickCount(), info ? cc_cast(info->name) : "<unknown>", player, errmsg);
*/
  }
}

/// Callback function to indicate whether a client has been authorized or not.
static void OnAuthorize(int productid, int localid, int authenticated, char *errmsg, void *instance)
{
  NetworkServer *server = reinterpret_cast<NetworkServer *>(instance);
  if (server && server == GNetworkManager.GetServer()) server->OnCDKeyAuthorize(localid, authenticated, errmsg);
}

/// Callback function to reauthorize players.
static void OnRefreshAuthorize(int gameid, int localid, int hint, char *challenge, void *instance)
{
  NetworkServer *server = reinterpret_cast<NetworkServer *>(instance);

  if (server && server == GNetworkManager.GetServer())
  {
    CDKeyRecheckAskMessage ask;
    ask._hint = hint;
    ask._challenge = challenge;

    server->SendMsg(localid, &ask, NMFGuaranteed);
  }
}
#endif

/// check if new message is overriding the old one
template <int msgType>
inline bool InitMessageIsOverride(const NetworkMessage *msgOld, NetworkMessage *msgNew)
{
  // by default assume it is not
  return false;
}

template <>
inline bool InitMessageIsOverride<NMTAskForAirportSetSide>(const NetworkMessage *msgOld, NetworkMessage *msgNew)
{
  // we need to process the messages as they are stored on the server
  
  const NetworkMessageAskForAirportSetSide *mOld = static_cast<const NetworkMessageAskForAirportSetSide *>(msgOld);
  const NetworkMessageAskForAirportSetSide *mNew = static_cast<const NetworkMessageAskForAirportSetSide *>(msgNew);
  return mOld->_airportId==mNew->_airportId;
};

template <>
inline bool InitMessageIsOverride<NMTPublicVariable>(const NetworkMessage *msgOld, NetworkMessage *msgNew)
{
  // we need to process the messages as they are stored on the server
  const NetworkMessagePublicVariable *mOld = static_cast<const NetworkMessagePublicVariable *>(msgOld);
  const NetworkMessagePublicVariable *mNew = static_cast<const NetworkMessagePublicVariable *>(msgNew);
  return mOld->_name==mNew->_name;
};

template <>
inline bool InitMessageIsOverride<NMTObjectSetVariable>(const NetworkMessage *msgOld, NetworkMessage *msgNew)
{
  // we need to process the messages as they are stored on the server
  const NetworkMessageObjectSetVariable *mOld = static_cast<const NetworkMessageObjectSetVariable *>(msgOld);
  const NetworkMessageObjectSetVariable *mNew = static_cast<const NetworkMessageObjectSetVariable *>(msgNew);
  return mOld->_object == mNew->_object && mOld->_name == mNew->_name;
};

template <>
inline bool InitMessageIsOverride<NMTGroupSetVariable>(const NetworkMessage *msgOld, NetworkMessage *msgNew)
{
  // we need to process the messages as they are stored on the server
  const NetworkMessageGroupSetVariable *mOld = static_cast<const NetworkMessageGroupSetVariable *>(msgOld);
  const NetworkMessageGroupSetVariable *mNew = static_cast<const NetworkMessageGroupSetVariable *>(msgNew);
  return mOld->_group == mNew->_group && mOld->_name == mNew->_name;
};

template <>
inline bool InitMessageIsOverride<NMTAskForAnimationPhase>(const NetworkMessage *msgOld, NetworkMessage *msgNew)
{
  // we need to process the messages as they are stored on the server
  const NetworkMessageAskForAnimationPhase *mOld = static_cast<const NetworkMessageAskForAnimationPhase *>(msgOld);
  const NetworkMessageAskForAnimationPhase *mNew = static_cast<const NetworkMessageAskForAnimationPhase *>(msgNew);
  return mOld->_vehicle==mNew->_vehicle && mOld->_animation==mNew->_animation;
};

template <>
inline bool InitMessageIsOverride<NMTStaticObjectDestructed>(const NetworkMessage *msgOld, NetworkMessage *msgNew)
{
  // we need to process the messages as they are stored on the server
  const NetworkMessageStaticObjectDestructed *mOld = static_cast<const NetworkMessageStaticObjectDestructed *>(msgOld);
  const NetworkMessageStaticObjectDestructed *mNew = static_cast<const NetworkMessageStaticObjectDestructed *>(msgNew);
  return mOld->_object==mNew->_object;
};

template <>
inline bool InitMessageIsOverride<NMTWaypointsCopy>(const NetworkMessage *msgOld, NetworkMessage *msgNew)
{
  // we need to process the messages as they are stored on the server
  const NetworkMessageWaypointsCopy *mOld = static_cast<const NetworkMessageWaypointsCopy *>(msgOld);
  const NetworkMessageWaypointsCopy *mNew = static_cast<const NetworkMessageWaypointsCopy *>(msgNew);
  return mOld->_to == mNew->_to;
};

template <>
inline bool InitMessageIsOverride<NMTVehMPEventHandlers>(const NetworkMessage *msgOld, NetworkMessage *msgNew)
{
  // we need to process the messages as they are stored on the server
  const NetworkMessageVehMPEventHandlers *mOld = static_cast<const NetworkMessageVehMPEventHandlers *>(msgOld);
  const NetworkMessageVehMPEventHandlers *mNew = static_cast<const NetworkMessageVehMPEventHandlers *>(msgNew);
  return ( (mOld->_vehicle==mNew->_vehicle) && (mOld->_eventNum==mNew->_eventNum) );
};

    //case NMTSetObjectTexture:
    //case NMTPublicExec:

static bool InitMessageIsOverride(NetworkMessageType type, const NetworkMessage *msgOld, NetworkMessage *msgNew)
{
  switch (type)
  {
    #define NMT_TYPE_CASE_IS_OVERRIDE(macro, class, name, description, group) \
      case NMT##name: return InitMessageIsOverride<NMT##name>(msgOld,msgNew);break;
    // generate cases for all message types
    NETWORK_MESSAGE_TYPES(NMT_TYPE_CASE_IS_OVERRIDE);
  }
  return false;
}

static bool InitMessageIsOverride(
  NetworkMessageType oldType, const NetworkMessage *msgOld, NetworkMessageType newType, NetworkMessage *msgNew
)
{
  if (oldType==newType)
  {
    return InitMessageIsOverride(oldType,msgOld,msgNew);
  }
  return false;
}

/*!
\patch 5161 Date 5/31/2007 by Ondra
- Optimized: Reduced amount of data transferred when JIP into a complex and long running mission.
*/

void NetworkServer::AddInitAndRemoveOverridden(
  NetworkMessageType type, NetworkMessage * msg, int **updateI
)
{
  int dst,src;
  for (src=dst=0; src<_initMessages.Size(); )
  {
    if (InitMessageIsOverride(_initMessages[src].type,_initMessages[src].msg,type,msg))
    {
      if (updateI)
      {
        // if deleting before any of updateI, we need to update the value
        for (int **ii=updateI; *ii; ii++)
        {
          if (src<=**ii) --**ii;
        }
      }
      // skip the message, it is obsolete now
      src++;
    }
    else
    {
      if (src!=dst) _initMessages[dst] = _initMessages[src];
      src++, dst++;
    }
  }
  // if anything was deleted, remove it now
  _initMessages.Resize(dst);

  // add the new message
  //CheckInitMessages(_initMessages, "before _initMessages.Add()");
  int index = _initMessages.Add();
  _initMessages[index].msg = msg;
  _initMessages[index].type = type;
  //CheckInitMessages(_initMessages, "after _initMessages.Add()");
}

#if _ENABLE_CHEATS
extern const char *NetworkMessageTypeNames[];

void NetworkServer::RptFObjectNotFound(int creator, int id, NetworkMessageType type)
{
  if (_serverState==NSSLoadingGame || _serverState==NSSBriefing || _serverState==NSSPlaying)
    RptF("Server: Object %d:%d not found (message %s)", creator, id, NetworkMessageTypeNames[type]);
}
#else
void NetworkServer::RptFObjectNotFound(int creator, int id, NetworkMessageType type)
{
  if (_serverState==NSSLoadingGame || _serverState==NSSBriefing || _serverState==NSSPlaying)
    RptF("Server: Object %d:%d not found (message %d)", creator, id, (int)type);
}
#endif

void NetworkServer::UpdatePlayerBankCount(NetworkPlayerInfo *info, int bankCount, bool knowLevel2)
{
  if (info->hashes.banksCount==-1)
  { // not set yet
    info->hashes.banksCount = bankCount;
    info->hashes.iKnowLevel2 = knowLevel2;
    info->hashes.bankInfoTimeout = 0; //reset
  }
  else if (info->hashes.banksCount != bankCount)
  {
    // player sent different bankCount to her prior info => cheating?
    KickOff(info->dpid,KORKick);
    return;
  }
  int sigCount = info->hashes.SignaturesCount();
  if (sigCount && sigCount!=bankCount)
  {
    // player sent different bankCount than was checked during initial signature test => cheating?
    KickOff(info->dpid,KORKick);
  }
}

void NetworkServer::ReportWrongSignatures(int from, RString filename, const NetworkPlayerInfo *info)
{
#if _ENABLE_DEDICATED_SERVER
  LogF("Hacked Signatures?");
  ServerMessage(Format(
    LocalizeString(IDS_SIGNATURE_WRONG),
    cc_cast(info->name),
    cc_cast(filename)));
#endif
  OnHackedData(HTHackedData, from, filename);
};

/*!
\patch_internal 1.01 Date 6/22/2001 by Ondra.
- Added: ID==0 refusal
\patch 1.02 Date 7/12/2001 by Jirka:
  - Fixed: error in assignment
  - allow player assignment only in appropriate state
  - ignore messages arrived after state has changed
\patch_internal 1.03 Date 7/13/2001 by Jirka.
- Fixed: Allow \ in url of squad (for debugging purposes)
\patch_internal 1.14 Date 8/9/2001 by Ondra.
- Added: ID>110000000 || ID>10000000 && ID<100000000 refusal
\patch 1.21 Date 8/24/2001 by Jirka:
- Fixed: error in assignment
\patch 1.25 Date 9/29/2001 by Ondra:
- New: Ping to all clients displayed on server.
\patch 1.27 Date 10/15/2001 by Jirka
- Fixed: When CAPS LOCK was pressed, Voice Over Net was not disabled (bug since 1.26)
\patch 1.30 Date 11/01/2001 by Jirka
- Fixed: Voting is echoed to clients
\patch 1.33 Date 11/29/2001 by Jirka
- Improved: Global ban list reloads before player is checked
\patch_internal 1.43 Date 1/31/2002 by Jirka
- Fixed: Server does not send create and update messages to nonplaying clients
\patch 1.82 Date 8/19/2002 by Ondra
- Fixed: Voting is echoed even to the player voting.
\patch 1.86 Date 10/16/2002 by Jirka
- Fixed: On dedicated server with mission rotating, after #login and #missions no missions appears
\patch 5136 Date 2/28/2007 by Jirka
- Fixed: MP - command #exec allowed only for logged (not voted) admin
\patch 5137 Date 3/5/2007 by Jirka
- Fixed: Dedicated server - after #reassign two players could control a single avatar
\patch 5154 Date 5/3/2007 by Jirka
- Fixed: Dedicated server - ban.txt was locked for edit during the game
\patch 5164 Date 10/3/2007 by Jirka
- Fixed: Handling of duplicate CD key improved
\patch 5175 Date 10/16/2007 by Jirka
- Added: After 3 wrong attempts to log in as an admin further attempts are ignored for some time
*/

void NetworkServer::OnMessage(int from, NetworkMessage *msg, NetworkMessageType type)
{
#if _ENABLE_PERFLOG
  PROFILE_SCOPE_EX(OnMsg,*);
  if (PROFILE_SCOPE_NAME(OnMsg).IsActive())
  {
    PROFILE_SCOPE_NAME(OnMsg).AddMoreInfo(Format("type=%d", type));
  }
#endif

  NetworkMessageFormatBase *format = GetFormat(/*from, */type);
  if (!format)
  {
    RptF("Server: Bad message %d(%s) format", (int)type, cc_cast(GetMsgTypeName(type)));
    return;
  }
  NetworkMessageContext ctx(msg, this, from, MSG_RECEIVE);

  #if _ENABLE_CHEATS
  int level = GetDiagLevel(type,from!=_parent->GetClient()->GetPlayer());
  if (level >= 2)
  {
    DiagLogF("Server: received message %s from %d", cc_cast(GetMsgTypeName(type)), from);   
    ctx.LogMessage(format, level, "\t");
  }
  #endif

  NetworkId objectId;
  AutoArray<NetworkId> units;
  int channel = -1;

  bool initMessageAlreadyAdded = false;
  switch (type)
  {
/*
    case NMTMsgFormat:
      {
        NetworkPlayerInfo *info = GetPlayerInfo(from);
        if (!info) info = OnPlayerCreate(from, "");
        int index = msg->id.id - NMTFirstVariant;
        if (index >= info->formats.Size())
          info->formats.Resize(index + 1);
        info->formats[index].TransferMsg(ctx);
      }
      break;
*/
    case NMTIntegrityAnswer:
      {
        IntegrityAnswerMessage answer;
        answer.TransferMsg(ctx);
        NetworkPlayerInfo *info = GetPlayerInfo(from);
        if (info)
        {
          int qId = answer._id;
          IntegrityQuestionType qType = (IntegrityQuestionType)answer._type;
          //Assert (info->integrityQuestionTimeout[qType]!=UINT_MAX);
          // it should be an answer to the first question of given type

          int qIndex = -1;
          if (qId > 0) qIndex = info->FindIntegrityQuestion(qId);
          else qIndex = info->FindIntegrityQuestion(qType);

//((EXE_CRC_CHECK
#if !_SUPER_RELEASE
          if (qType == IQTExe)
          {
            // exe modification investigation
            _exeInvestigation.OnAnswered(from, answer._answer);
            if (qIndex >= 0) info->integrityQuestions.Delete(qIndex);
            break;
          }
#endif
//))EXE_CRC_CHECK

          if (qIndex>=0)
          {
            const IntegrityQuestion &q = info->integrityQuestions[qIndex].q;
            LogF
            (
              "%s - %s: Received answer %d",
              (const char *)info->name,(const char *)q.name,answer._answer
            );
//((EXE_CRC_CHECK
            unsigned int myAnswer = IntegrityCheckAnswer(qType, q, true); // server
//))EXE_CRC_CHECK
            // integrity question may be a part of active investigation
            if (myAnswer!=answer._answer)
            {
              LogF
              (
                "%s - %s: Received answer %d, expected %d",
                (const char *)info->name,(const char *)q.name,
                answer._answer,myAnswer
              );
/*
//((TO_BE_REMOVED_IN_FINAL
#if !defined _XBOX
              FILE *LogFile=GetNetworkManager().GetDedicatedServerLogFile();
              if (LogFile)
              {
                fprintf(LogFile, "%s - %s: Received answer %d, expected %d (server buildNo: %d)\n",
                  (const char *)info->name,(const char *)q.name,
                  answer._answer,myAnswer, BUILD_NO);
              }
#endif
//))TO_BE_REMOVED_IN_FINAL
*/
            }
            bool wasIA = IntegrityAnswerReceived(info,qType,q,myAnswer==answer._answer);
            if (!wasIA && myAnswer!=answer._answer)
            {
              // calculate correct answer
              // try to provide more info if possible
              if (qType==IQTConfig)
              {
                OnIntegrityCheckFailed(from,(IntegrityQuestionType)qType,q.name,false);
                // find where integrity failed - ask more detailed questions
                info->integrityInvestigation[qType] = new IntegrityInvestigationConfig(q.name);
              }
//((EXE_CRC_CHECK
              else if (qType==IQTExe)
              {
                // timing is random - kickoff immediately
                for (int i=0; i<_identities.Size(); i++)
                {
                  PlayerIdentity &identity = _identities[i];
                  if (identity.dpnid==from && identity.kickOffTime==UITIME_MAX)
                  {
                    float timeToPlay = GRandGen.Gauss(10,15,30);
                    identity.kickOffTime = Glob.uiTime+timeToPlay;
                    identity.kickOffState = KOExe;
                  }
                }
              }
//))EXE_CRC_CHECK
              else
              {
                OnIntegrityCheckFailed(from,(IntegrityQuestionType)qType,q.name,true);
              }
              // we discovered one modification
              // report it and stop checking
              // otherwise log would be full of "modified xxxxxx" messages
              info->integrityCheckNext = UINT_MAX;
            }
            info->integrityQuestions.Delete(qIndex);
          }
          //info->integrityQuestionTimeout[qType] = UINT_MAX;
        }
      }
      break;
    case NMTSelectPlayer:
      {
LogF("Server: received SelectPlayer");
        SelectPlayerMessage pl;
        pl.TransferMsg(ctx);
        Assert(pl._player != AI_PLAYER);
        NetworkId person(pl._creator, pl._id);
        NetworkId brain = PersonToUnit(person);
        NetworkPlayerInfo *info = GetPlayerInfo(pl._player);
        if (info)
        {
          info->person = person;
          info->cameraPosition = pl._position;
          // HOTFIX: msg->time cannot be used as NMTSelectPlayer is sent by BOT_CLIENT which time can be different
          //         using Time(0) forces first update from client to set it properly according to clients time
          info->cameraPositionTime = Time(0);
#if _VBS3
          info->cameraFov = 0.75;
          info->cameraDirection = VForward;
#endif
          // fulfill info->unit, info->group
          info->unit = brain;
        }

        // first select player, then change owner
        // (when changeOwner arrive to client, player must be already known
        // - for respawn parameters initialization)
        if (from != pl._player)
        {
LogF("  sending to %x", pl._player);
          NetworkComponent::SendMsg(pl._player, msg, type, NMFGuaranteed);
        }

        NetworkObjectInfo *oInfo = GetObjectInfo(person);
        if (oInfo)
        {
          ChangeOwner(person, oInfo->owner, pl._player);
          if (!brain.IsNull())
          {
            NetworkObjectInfo *oInfo = GetObjectInfo(brain);
            if (oInfo) ChangeOwner(brain, oInfo->owner, pl._player);
          }
        }
      }
      break;
    case NMTAttachPerson:
      {
        PREPARE_TRANSFER(AttachPerson)

        NetworkId person;
        if (TRANSF_GET_ID_BASE(person, person) != TMOK) break;
        NetworkId unit;
        if (TRANSF_GET_ID_BASE(unit, unit) != TMOK) break;
        if (unit.IsNull()) break;
        for (int i=0; i<_mapPersonUnit.Size(); i++)
        {
          PersonUnitPair &pair = _mapPersonUnit[i];
          if (pair.unit == unit)
          {
            pair.person = person;
            break;
          }
        }
      }
      break;
    case NMTAskMissionFile:
      {
        AskMissionFileMessage ask;
        ask.TransferMsg(ctx);
        if (ask._valid)
        {
          NetworkPlayerInfo *info = GetPlayerInfo(from);
          if (info) info->missionFileValid = true;
        }
      }
      break;
    case NMTClientState:
      {
        ClientStateMessage cs;
        cs.TransferMsg(ctx);
        OnClientStateChanged(from, (NetworkClientState)cs._clientState);
      }
      break;
    case NMTPlayerClientState:
      {
        PlayerClientStateMessage pcs;
        pcs.TransferMsg(ctx);
        SetClientState(pcs._dpnid, (NetworkClientState)pcs._clientState);
      }
      break;
    case NMTLogin:
      {
        //{ CHANGED
        PlayerIdentity identity;
        identity.TransferMsg(ctx);
/*
        int index = _identities.Add();
        PlayerIdentity &identity = _identities[index];
        identity.TransferMsg(ctx);
*/
        //}

        // check if not in ban lists
        if (from != BOT_CLIENT)
        {
          // reload ban list
          LoadBanList("ban.txt", _banListGlobal);
          GFileServerFunctions->FlushReadHandle("ban.txt");
          
          bool ban = false;
          for (int i=0; i<_banListGlobal.Size(); i++)
            if (identity.id == _banListGlobal[i]) {ban = true; break;}
          if (!ban) for (int i=0; i<_banListLocal.Size(); i++)
            if (identity.id == _banListLocal[i]) {ban = true; break;}
          if (ban)
          {
            //{ CHANGED
            // _identities.Delete(index);
            //}
            KickOff(from,KORBan);
            break;
          }
        }

        //{ CHANGED
        bool isSquad = identity.squadId.GetLength() > 0;
        if (isSquad)
        {
          Ref<SquadIdentity> squad;
          for (int i=0; i<_squads.Size(); i++)
            if (_squads[i]->_id == identity.squadId)
            {
              squad = _squads[i];
              break;
            }

          bool newSquad = false;
          if (!squad)
          {
            squad = new SquadIdentity();
            squad->_id = identity.squadId;
            newSquad = true;
          }

          // create object
          _squadChecks.Add(new CheckSquadObject(identity, squad, newSquad, _proxy, this));

#if LOG_PLAYERS
LogF
(
  "Server: Identity check started - name %s, id %d (total %d identities, %d players)",
  (const char *)identity.name, identity.dpnid, _identities.Size(), _players.Size()
);
#endif
        }
        else
        {
          Ref<SquadIdentity> squad = NULL;
          _squadChecks.Add(new CheckSquadObject(identity, squad, false, _proxy, this));
          //CreateIdentity is called only from squadChecking machinery
          //CreateIdentity(identity, NULL);
        }
        //}

        // send server time to player just connected
        ServerTimeMessage stmsg;
        _serverTime = ::GetTickCount()-_serverTimeInitTickCount; // update _serverTime
        stmsg._time = _serverTime;
        stmsg._endTime = _serverTimeOfEstimatedEnd;
        SendMsg(from, &stmsg, NMFGuaranteed | NMFHighPriority);

#if _ENABLE_DEDICATED_SERVER
        // client need to know whether there is admin on server
        ServerAdminMessage samsg;
        samsg._admin = _admin;
        samsg._gameMaster = _gameMaster;
        SendMsg(from, &samsg, NMFGuaranteed);
#endif
      }
      break;
    case NMTPlayerRole:
      OnMessagePlayerRole(from, type, ctx);
      break;
    case NMTPlayerRoleUpdate:
      OnMessagePlayerRoleUpdate(from, type, ctx);
      break;
    case NMTCustomFilesWanted:
      {
        CustomFilesWantedMessage filesWantedMsg;
        filesWantedMsg.TransferMsg(ctx);
        for (int i=0; i<_squadChecks.Size(); i++)
        {
          if (_squadChecks[i]->IsPlayer(filesWantedMsg._player))
          {
            _squadChecks[i]->SetCustomFilesWanted(from, filesWantedMsg._filesWanted);
            break;
          }
        }
      }
      break;
    case NMTAskForAnimationPhase:
      {
        PREPARE_TRANSFER(AskForAnimationPhase)
        EXTRACT_ID(vehicle)
        if (!objectId.IsNull() && objectId.creator == STATIC_OBJECT)
        {
          // save message for lately connecting players
          AddInitAndRemoveOverridden(type,msg);
        }
      }
      goto LabelAskForDamage;
    case NMTAskForDamage:
      {
        PREPARE_TRANSFER(AskForDamage)
        EXTRACT_ID(who)
      }
      goto LabelAskForDamage;
    case NMTAskForSetDamage:
      {
        PREPARE_TRANSFER(AskForSetDamage)
        EXTRACT_ID(who)
      }
      goto LabelAskForDamage;
    case NMTAskForSetMaxHitZoneDamage:
      {
        PREPARE_TRANSFER(AskForSetMaxHitZoneDamage)
          EXTRACT_ID(who)
      }
      goto LabelAskForDamage;
    case NMTAskForApplyDoDamage:
      {
        PREPARE_TRANSFER(AskForApplyDoDamage)
        EXTRACT_ID(who)
      }
    LabelAskForDamage:
      {
        if (!objectId.IsNull())
        {
          if (objectId.creator == STATIC_OBJECT)
          {
            for (int i=0; i<_players.Size(); i++)
            {
              NetworkPlayerInfo &info = _players[i];
              if (info.dpid == from) continue;
              if (info.clientState != NCSBriefingRead) continue;
              NetworkComponent::SendMsg(info.dpid, msg, type, NMFNone);
            }
          }
          else
          {
            NetworkObjectInfo *info = GetObjectInfo(objectId);
            if (info)
            {
              if (info->pendingOwner != NO_CLIENT)
              {
                WaitingMessageInfo *waitingMsg = new WaitingMessageInfo(type,msg);
                info->waitingMessages.Add(waitingMsg);
              }
              else
              {
                NetworkComponent::SendMsg(info->owner, msg, type, NMFGuaranteed);
              }
            }
            else
            {
              RptFObjectNotFound(objectId.creator, objectId.id, type);
            }
          }
        }
      }
      break;
    case NMTAskForEnableVMode:
      {
        PREPARE_TRANSFER(AskForEnableVMode)
        EXTRACT_ID(vehicle)
      }
      goto LabelAskWithVehicle;
    case NMTAskForForceGunLight:
      {
        PREPARE_TRANSFER(AskForForceGunLight)
        EXTRACT_ID(vehicle)
      }
      goto LabelAskWithVehicle;
    case NMTAskForIRLaser:
      {
        PREPARE_TRANSFER(AskForIRLaser)
        EXTRACT_ID(vehicle)
      }
      goto LabelAskWithVehicle;
    case NMTTakeBackpack:
      {
        PREPARE_TRANSFER(TakeBackpack)
        EXTRACT_ID(soldier)
      }
      goto LabelAskWithVehicle;
    case NMTDisAssemble:
      {
        PREPARE_TRANSFER(DisAssemble)
        EXTRACT_ID(backpack)
      }
      goto LabelAskWithVehicle;
    case NMTAskForGetIn:
      {
        PREPARE_TRANSFER(AskForGetIn)
        if (message->_position == GIPTurret)
        {
          EXTRACT_ID(turret)
        }
        else
        {
          EXTRACT_ID(vehicle)
        }
      }
      goto LabelAskWithVehicle;
    case NMTAskForGetOut:
      {
        PREPARE_TRANSFER(AskForGetOut)
        EXTRACT_ID(turret)
        if (objectId.IsNull())
        {
          EXTRACT_ID(vehicle)
        }
      }
      goto LabelAskWithVehicle;
    case NMTAskRemoteControlled:
      {
        PREPARE_TRANSFER(AskRemoteControlled)
        EXTRACT_ID(whom)
      }
      goto LabelAskWithVehicle;
    case NMTAskWaitForGetOut:
      {
        PREPARE_TRANSFER(AskWaitForGetOut)
        EXTRACT_ID(vehicle)
      }
      goto LabelAskWithVehicle;
    case NMTAskForChangePosition:
      {
        PREPARE_TRANSFER(AskForChangePosition)
        EXTRACT_ID(turret)
        if (objectId.IsNull())
        {
          EXTRACT_ID(vehicle)
        }
      }
      goto LabelAskWithVehicle;
    case NMTAskForSelectWeapon:
      {
        PREPARE_TRANSFER(AskForSelectWeapon)
        EXTRACT_ID(turret)
        if (objectId.IsNull())
        {
          EXTRACT_ID(vehicle)
        }
      }
      goto LabelAskWithVehicle;
    case NMTAskForAmmo:
      {
        PREPARE_TRANSFER(AskForAmmo)
        EXTRACT_ID(turret)
        if (objectId.IsNull())
        {
          EXTRACT_ID(vehicle)
        }
      }
      goto LabelAskWithVehicle;
    case NMTAskForFireWeapon:
      {
        PREPARE_TRANSFER(AskForFireWeapon)
        EXTRACT_ID(turret)
        if (objectId.IsNull())
        {
          EXTRACT_ID(vehicle)
        }
      }
      goto LabelAskWithVehicle;
    case NMTAskForAddImpulse:
      {
        PREPARE_TRANSFER(AskForAddImpulse)
        EXTRACT_ID(vehicle)
      }
      goto LabelAskWithVehicle;
    case NMTAskForMoveVector:
      {
        PREPARE_TRANSFER(AskForMoveVector)
        EXTRACT_ID(vehicle)
      }
      goto LabelAskWithVehicle;
    case NMTAskForMoveMatrix:
      {
        PREPARE_TRANSFER(AskForMoveMatrix)
        EXTRACT_ID(vehicle)
      }
      goto LabelAskWithVehicle;
    case NMTAskForJoinGroup:
      {
        PREPARE_TRANSFER(AskForJoinGroup)
        EXTRACT_ID(join)
      }
      goto LabelAskWithVehicle;
    case NMTAskForJoinUnits:
      {
        PREPARE_TRANSFER(AskForJoinUnits)
        EXTRACT_ID(join)
      }
      goto LabelAskWithVehicle;
    case NMTAskForChangeSide:
      {
        PREPARE_TRANSFER(AskForChangeSide)
        EXTRACT_ID(vehicle)
      }
      goto LabelAskWithVehicle;
    case NMTAskForHideBody:
      {
        PREPARE_TRANSFER(AskForHideBody)
        EXTRACT_ID(vehicle)
      }
      goto LabelAskWithVehicle;
    case NMTUpdateWeapons:
      {
        PREPARE_TRANSFER(UpdateWeapons)
        EXTRACT_ID(vehicle)
      }
      goto LabelAskWithVehicle;
    case NMTShowTarget:
      {
        PREPARE_TRANSFER(ShowTarget)
        EXTRACT_ID(vehicle)
      }
      goto LabelAskWithVehicle;
    case NMTShowGroupDir:
      {
        PREPARE_TRANSFER(ShowGroupDir)
        EXTRACT_ID(vehicle)
      }
      goto LabelAskWithVehicle;
    case NMTAskForCreateUnit:
      {
        PREPARE_TRANSFER(AskForCreateUnit)
        EXTRACT_ID(group)
      }
      goto LabelAskWithVehicle;
    case NMTAskForDeleteVehicle:
      {
        PREPARE_TRANSFER(AskForDeleteVehicle)
        EXTRACT_ID(vehicle)
      }
      goto LabelAskWithVehicle;
    case NMTAskForGroupRespawn:
      {
        PREPARE_TRANSFER(AskForGroupRespawn)
        EXTRACT_ID(group)
      }
      goto LabelAskWithVehicle;
#if _VBS3
		case NMTAskForRespawn:
			{
        PREPARE_TRANSFER(AskForRespawn)
        EXTRACT_ID(person)
        /*
				Assert(dynamic_cast<const IndicesAskForRespawn *>(ctx.GetIndices()))
				const IndicesAskForRespawn *indices = static_cast<const IndicesAskForRespawn *>(ctx.GetIndices());
				index1 = indices->person;
        */
			}
    case NMTAskCommanderOverride:
      {
        PREPARE_TRANSFER(AskCommanderOverride)
          EXTRACT_ID(turret)
      }

			goto LabelAskWithVehicle;
#endif
    case NMTAskForActivateMine:
      {
        PREPARE_TRANSFER(AskForActivateMine)
        EXTRACT_ID(mine)
      }
      goto LabelAskWithVehicle;
    case NMTAskForInflameFire:
      {
        PREPARE_TRANSFER(AskForInflameFire)
        EXTRACT_ID(fireplace)
      }
      goto LabelAskWithVehicle;
    case NMTAskWeapon:
      {
        PREPARE_TRANSFER(AskWeapon)
        EXTRACT_ID(from)
      }
      goto LabelAskWithVehicle;
    case NMTAskMagazine:
      {
        PREPARE_TRANSFER(AskMagazine)
        EXTRACT_ID(from)
      }
      goto LabelAskWithVehicle;
    case NMTAskBackpack:
      {
        PREPARE_TRANSFER(AskBackpack)
        EXTRACT_ID(from)
      }
      goto LabelAskWithVehicle;
    case NMTReplaceWeapon:
      {
        PREPARE_TRANSFER(ReplaceWeapon)
        EXTRACT_ID(to)
      }
      goto LabelAskWithVehicle;
    case NMTReplaceMagazine:
      {
        PREPARE_TRANSFER(ReplaceMagazine)
        EXTRACT_ID(to)
      }
      goto LabelAskWithVehicle;
    case NMTReplaceBackpack:
      {
        PREPARE_TRANSFER(ReplaceBackpack)
        EXTRACT_ID(to)
      }
      goto LabelAskWithVehicle;
    case NMTReturnWeapon:
      {
        PREPARE_TRANSFER(ReturnWeapon)
        EXTRACT_ID(from)
      }
      goto LabelAskWithVehicle;
    case NMTReturnMagazine:
      {
        PREPARE_TRANSFER(ReturnMagazine)
        EXTRACT_ID(from)
      }
      goto LabelAskWithVehicle;
    case NMTReturnBackpack:
      {
        PREPARE_TRANSFER(ReturnBackpack)
        EXTRACT_ID(from)
      }
      goto LabelAskWithVehicle;
    case NMTCancelTakeFlag:
      {
        PREPARE_TRANSFER(CancelTakeFlag)
        EXTRACT_ID(flag)
      }
      goto LabelAskWithVehicle;
    case NMTPoolAskWeapon:
      {
        PoolAskWeaponMessage ask;
        ask.TransferMsg(ctx);
        for (int i=0; i<GWeaponsPool._weaponsPool.Size(); i++)
        {
          if (stricmp(ask._weapon, GWeaponsPool._weaponsPool[i]->GetName()) == 0)
          {
            PoolReplaceWeaponMessage reply;
            reply._creator = ask._creator;
            reply._id = ask._id;
            reply._weapon = ask._weapon;
            reply._slot = ask._slot;
            reply._useBackpack = ask._useBackpack;
            GWeaponsPool._weaponsPool.Delete(i);
            UpdateWeaponsPool();
            SendMsg(from, &reply, NMFGuaranteed);
            break;
          }
        }
      }
      break;
    case NMTPoolAskMagazine:
      {
        PoolAskMagazineMessage ask;
        ask.TransferMsg(ctx);
        MagazineType *type = MagazineTypes.New(ask._type);
        if (type)
        {
          Ref<Magazine> magazine = GWeaponsPool.RemoveMagazine(type);
          if (magazine)
          {
            PoolReplaceMagazineMessage reply;
            reply._creator = ask._creator;
            reply._id = ask._id;
            reply._magazine = magazine;
            reply._slot = ask._slot;
            reply._useBackpack = ask._useBackpack;
            UpdateMagazinesPool();
            SendMsg(from, &reply, NMFGuaranteed);
          }
        }
      }
      break;
    case NMTPoolAskBackpack:
      {
        PoolAskBackpackMessage ask;
        ask.TransferMsg(ctx);
        for (int i=0; i<GWeaponsPool._backpacksPool.Size(); i++)
        {
          if (!GWeaponsPool._backpacksPool[i]._id.IsNull() && strcmpi(ask._name, GWeaponsPool._backpacksPool[i].GetName()) == 0)
          {
            BackpacksPoolItem bag = GWeaponsPool._backpacksPool[i];
            if (GWeaponsPool.RemoveBackpack(bag._id))
            {
              PoolReplaceBackpackMessage reply;
              reply._creator = ask._creator;
              reply._id = ask._id;
              reply._bagCreator = bag._id.creator;
              reply._bagId = bag._id.id;
              reply._typeName = bag._typmeName;
              UpdateBackpacksPool();
              SendMsg(from, &reply, NMFGuaranteed);
              break;
            }
          }
        }
      }
      break;
    case NMTPoolReturnWeapon:
      {
        PoolReturnWeaponMessage ask;
        ask.TransferMsg(ctx);

        WeaponType *weapon = WeaponTypes.New(ask._weapon);
        if (weapon)
        {
          GWeaponsPool._weaponsPool.Add(weapon);
          UpdateWeaponsPool();
        }
      }
      break;
    case NMTPoolReturnMagazine:
      {
        PoolReturnMagazineMessage ask;
        ask.TransferMsg(ctx);

        if (ask._magazine)
        {
          GWeaponsPool._magazinesPool.Add(ask._magazine);
          UpdateMagazinesPool();
        }
      }
      break;
    case NMTPoolReturnBackpack:
      {
        PoolReturnBackpackMessage ask;
        ask.TransferMsg(ctx);
        NetworkId id(ask._creator, ask._id);
        if (!id.IsNull())
        {
          BackpacksPoolItem bagItem = BackpacksPoolItem(NULL, id, ask._typeName);
          GWeaponsPool._backpacksPool.Add(bagItem);
          UpdateBackpacksPool();
        }
      }
      break;
    case NMTMsgVLoadMagazine:
      {
        PREPARE_TRANSFER(MsgVLoadMagazine)
        EXTRACT_ID(receiver)
      }
      goto LabelAskWithVehicle;
    case NMTMsgVTarget:
    case NMTMsgVFire:
    case NMTMsgVMove:
    case NMTMsgVWatchTgt:
    case NMTMsgVWatchPos:
    case NMTMsgVFormation:
    case NMTMsgVSimpleCommand:
    case NMTMsgVLoad:
    case NMTMsgVAzimut:
    case NMTMsgVStopTurning:
    case NMTMsgVFireFailed:
      {
        PREPARE_TRANSFER(VMessage)
        EXTRACT_ID(vehicle)
      }
    LabelAskWithVehicle:
      {
        NetworkObjectInfo *info = GetObjectInfo(objectId);
        if (info)
        {
          if (info->pendingOwner != NO_CLIENT)
          {
            WaitingMessageInfo *waitingMsg = new WaitingMessageInfo(type,msg);
            info->waitingMessages.Add(waitingMsg);
          }
          else
          {
            NetworkComponent::SendMsg(info->owner, msg, type, NMFGuaranteed);
          }
        }
        else
        {
          RptFObjectNotFound(objectId.creator, objectId.id, type);
        }
      }
      break;
    case NMTJoinIntoUnit:
      {
        PREPARE_TRANSFER(JoinIntoUnit)
        NetworkId id;
        if (TRANSF_GET_ID_BASE(unit, id) != TMOK) break;
        if (id.IsNull())
        {
          if (TRANSF_GET_ID_BASE(group, id) != TMOK) break;
        }
        NetworkObjectInfo *info = GetObjectInfo(id);
        if (info)
        {
          if (info->pendingOwner != NO_CLIENT)
          {
            WaitingMessageInfo *waitingMsg = new WaitingMessageInfo(type,msg);
            info->waitingMessages.Add(waitingMsg);
          }
          else
          {
            NetworkComponent::SendMsg(info->owner, msg, type, NMFGuaranteed);
          }
        }
        else
        {
          RptFObjectNotFound(id.creator, id.id, type);
        }
      }
      break;
    case NMTForceDeleteObject:
      {
        // remove from object lists
        PREPARE_TRANSFER(ForceDeleteObject)
        NetworkId id;
        if (TRANSF_GET_ID_BASE(object, id) != TMOK) break;
        PerformObjectDestroy(id);

        // send to other clients
        for (int i=0; i<_players.Size(); i++)
        {
          NetworkPlayerInfo &info = _players[i];
          if (info.dpid == from) continue;
          if (info.clientState < NCSMissionReceived) continue;
          NetworkComponent::SendMsg(info.dpid, msg, type, NMFGuaranteed);
        }
      }
      break;
    case NMTGroupRespawnDone:
      {
        PREPARE_TRANSFER(GroupRespawnDone)
        int to;
        if (TRANSF_BASE(to, to) == TMOK)
          NetworkComponent::SendMsg(to, msg, type, NMFGuaranteed);
      }
      break;
    case NMTAskForReceiveUnitAnswer:
      {
        PREPARE_TRANSFER(AskForReceiveUnitAnswer)
        EXTRACT_ID(to)
        // it is not vehicle index, but it does not matter
        // all we need is to be able to get owner of given network object
      }
      goto LabelAskWithVehicle;
    case NMTAskForOrderGetIn:
      {
        PREPARE_TRANSFER(AskForOrderGetIn)
        EXTRACT_ID(unit)
        // it is not vehicle index, but it does not matter
        // all we need is to be able to get owner of given network object
      }
      goto LabelAskWithVehicle;
    case NMTAskForAllowGetIn:
      {
        PREPARE_TRANSFER(AskForAllowGetIn)
        EXTRACT_ID(unit)
        // it is not vehicle index, but it does not matter
        // all we need is to be able to get owner of given network object
      }
      goto LabelAskWithVehicle;
#if _ENABLE_CONVERSATION
    case NMTKBReact:
      {
        PREPARE_TRANSFER(KBReact)
        EXTRACT_ID(to)
        // it is not vehicle index, but it does not matter
        // all we need is to be able to get owner of given network object
      }
      goto LabelAskWithVehicle;
#endif
    case NMTRevealTarget:
      {
        PREPARE_TRANSFER(RevealTarget)
        int to;
        if (TRANSF_BASE(to, to) == TMOK)
          NetworkComponent::SendMsg(to, msg, type, NMFGuaranteed);
      }
      break;
    case NMTSetRoleIndex:
      // update CreateBrain message
      {
        PREPARE_TRANSFER(SetRoleIndex)
        NetworkId unit;
        int roleIndex;
        if (TRANSF_GET_ID_BASE(unit, unit) == TMOK && TRANSF_BASE(roleIndex, roleIndex) == TMOK)
        {
          NetworkObjectInfo *oInfo = GetObjectInfo(unit);
          if (oInfo)
          {
            NetworkMessageCreateAIBrain *message = (NetworkMessageCreateAIBrain *)oInfo->create.message.GetRef();
            if (message) message->_roleIndex = roleIndex;
          }
        }
      }
      // continue
    case NMTMarkerDelete:
      // send to all except from (state Briefing expected)
      for (int i=0; i<_players.Size(); i++)
      {
        NetworkPlayerInfo &info = _players[i];
        if (info.dpid == from) continue;
        if (info.clientState < NCSGameLoaded) continue;
        NetworkComponent::SendMsg(info.dpid, msg, type, NMFGuaranteed);
      }
#if _VBS3
      if(type == NMTMarkerDelete)
        AddInitAndRemoveOverridden(type, msg); // Saved for JIP players
#endif
      break;
#if _VBS2 //Convoy trainer
  #if _EXT_CTRL
    case NMTAARAskUpdate:// Here we service the ask request for AAR
      {
        AARAskUpdateMessage act;
        act.TransferMsg(ctx);

        switch(act._type)
        {
        case StartRecord:
          GAAR.Record();
          break;
        case StopRecord:
          GAAR.RecordStop();
          break;
        case SaveFile:
          GAAR.RecordSave(act._stringOne);
          break;
        case AddBookMK:
          GAAR.AddBookMark(act._floatType,act._stringOne,act._stringTwo);
          break;
        case RemBookMK:
          GAAR.RemoveBookMark(act._stringOne);
          break;
        case SaveState:
          // Client should never ask to set save state
          Assert(false); 
          break;
        default:
          Assert(false);// message type not handeled, development bug
        }
      }
      break;
  #endif
    case NMTApplyWeather:// request will be sent to the bot client too
    case NMTAddMPReport:
    case NMTAARDoUpdate: // Here we update all clients with their new state.
    case NMTPauseSimulation:
    case NMTEnablePersonalItems:
#endif
#if _ENABLE_ATTACHED_OBJECTS
    case NMTAttachObject:
    case NMTDetachObject:
#endif
#if _VBS2 || _ENABLE_ATTACHED_OBJECTS
      AddInitAndRemoveOverridden(type, msg);
      initMessageAlreadyAdded = true;
#endif
      // save message for lately connecting players
      if (!initMessageAlreadyAdded)
      {
        //CheckInitMessages(_initMessages, "before _initMessages.Add()");
        int index = _initMessages.Add();
        _initMessages[index].msg = msg;
        _initMessages[index].type = type;
        //CheckInitMessages(_initMessages, "after _initMessages.Add()");
      }
    // no break here - continue
    case NMTPlaySound:
    case NMTSoundState:
    case NMTVehicleDestroyed:
    case NMTGroupSynchronization:
    case NMTDetectorActivation:
    case NMTVehicleDamaged:
    case NMTIncomingMissile:
    case NMTLaunchedCounterMeasures:
    case NMTWeaponLocked:
    case NMTDropBackpack:
    case NMTAssemble:
#if _ENABLE_INDEPENDENT_AGENTS
    case NMTTeamMemberSetVariable:
#endif
      // send to all except from (state Play expected)
      for (int i=0; i<_players.Size(); i++)
      {
        NetworkPlayerInfo &info = _players[i];
        if (info.dpid == from) continue;
        if (info.clientState != NCSBriefingRead) continue;
        NetworkComponent::SendMsg(info.dpid, msg, type, NMFGuaranteed);
      }
      break;
    case NMTAddWeaponCargo:
    case NMTRemoveWeaponCargo:
    case NMTAddMagazineCargo:
    case NMTRemoveMagazineCargo:
    case NMTClearWeaponCargo:
    case NMTClearMagazineCargo:
    case NMTClearBackpackCargo:
    case NMTAddBackpackCargo:
    case NMTRemoveBackpackCargo:
      // save message for lately connecting players
      if (!initMessageAlreadyAdded)
      {
        //CheckInitMessages(_initMessages, "before _initMessages.Add()");
        int index = _initMessages.Add();
        _initMessages[index].msg = msg;
        _initMessages[index].type = type;
        //CheckInitMessages(_initMessages, "after _initMessages.Add()");
      }
      // no break here - continue
      // send to all except from (state Play expected)
      for (int i=0; i<_players.Size(); i++)
      {
        NetworkPlayerInfo &info = _players[i];
        if (info.dpid == from) continue;
        NetworkComponent::SendMsg(info.dpid, msg, type, NMFGuaranteed);
      }
      break;

    case NMTFireWeapon:
#if _VBS3
      {
        bool toAll = false;
        NetworkMessageFireWeapon *fireWeaponMsg = static_cast<NetworkMessageFireWeapon *>(msg);
        Assert(fireWeaponMsg);

        // force fire weapon message through network
        // let normal bullets go through delayed messages
        if(fireWeaponMsg->_visible == FLT_MAX)
          toAll = true;

        // is a shot bullet pass into delayed messages
        for (int i=0; i<_players.Size(); i++)
        {
          NetworkPlayerInfo &info = _players[i];
          if (info.dpid == from) continue;
          if (info.clientState != NCSBriefingRead) continue;

          if( toAll
# if _EXT_CTRL //VBS2, send all events directly to the bot client if HLA/ AAR is active
              || _hla.ExternalActive() && info.dpid == BOT_CLIENT
# endif
              )
              NetworkComponent::SendMsg(info.dpid, msg, type, NMFGuaranteed);
           else
           {
              DelayedMessageInfo *delayed = new DelayedMessageInfo(type,msg);
              info._delayedMessages.Add(delayed);
           }
        }
        break;
      }
#endif //_VBS3
    case NMTExplosionDamageEffects:
      {
#if _VBS3
        bool toAll = false;
        NetworkMessageExplosionDamageEffects *ask = static_cast<NetworkMessageExplosionDamageEffects *>(msg);
        Assert(ask);

        Ref<AmmoType> ammo;
        if (ask->_type.GetLength() > 0)
        {
          Ref<EntityType> type = VehicleTypes.New(ask->_type);
          ammo = dynamic_cast<AmmoType *>(type.GetRef());
          if (ammo && (ammo->_simulation != AmmoShotBullet))
            toAll = true;
        }
#endif
        // shotbullet place into delayed messages
        for (int i=0; i<_players.Size(); i++)
        {
          NetworkPlayerInfo &info = _players[i];
          if (info.dpid == from) continue;
          if (info.clientState != NCSBriefingRead) continue;
#if _VBS3
          if( toAll
# if _EXT_CTRL //VBS2, send all events directly to the bot client if HLA/ AAR is active
              || _hla.ExternalActive() && info.dpid == BOT_CLIENT
# endif
            )
              NetworkComponent::SendMsg(info.dpid, msg, type, NMFGuaranteed);
            else
#endif //_VBS3
            {

               DelayedMessageInfo *delayed = new DelayedMessageInfo(type,msg);
               info._delayedMessages.Add(delayed);
             }
          //NetworkComponent::SendMsg(info.dpid, msg, type, NMFGuaranteed);
        }
      }
      break;
    case NMTAskForAirportSetSide:
    case NMTVehicleInit:
    case NMTPublicVariable:
    case NMTObjectSetVariable:
    case NMTGroupSetVariable:
    case NMTWaypointCreate:
    case NMTWaypointUpdate:
    case NMTWaypointDelete:
    case NMTWaypointsCopy:
    case NMTHCClearGroups:
    case NMTHCRemoveGroup:
    case NMTHCSetGroup:
    case NMTGroupSetUnconsciousLeader:
    case NMTGroupSelectLeader:
    case NMTVehMPEventHandlers:
#if _VBS3 // moved up from below
    case NMTSetObjectTexture:
    case NMTPublicExec:
#endif
      // save message for lately connecting players
      AddInitAndRemoveOverridden(type, msg);
      // continue
    case NMTTransferFile:
#if _ENABLE_UNSIGNED_MISSIONS
    case NMTTransferMissionFile:
#endif
    case NMTUpdateWeaponsInfo:
    case NMTMissionStats:
    case NMTVehMPEvent:
      // send to all except from (state Mission Received expected)
      for (int i=0; i<_players.Size(); i++)
      {
        NetworkPlayerInfo &info = _players[i];
        if (info.dpid == from) continue;
        if (info.clientState < NCSMissionReceived) continue;
        NetworkComponent::SendMsg(info.dpid, msg, type, NMFGuaranteed);
      }
//CheckInitMessages(_initMessages, "after NetworkComponent::SendMsg");
      break;
    case NMTTransferFileToServer:
      {
        TransferFileMessage transfer;
        transfer.TransferMsg(ctx);
#ifdef _XBOX
        char buffer[256];
        const char *FullXBoxName(const char *name, char *temp);
        transfer._path = FullXBoxName(transfer._path, buffer);
#else
        transfer._path = GetServerPath(transfer._path);
#endif
        // check if path is valid upload path
        // if not, kick sender
        PlayerIdentity *id = FindIdentity(from);
        bool alreadyKicked = false;
        if (id && id->destroy)
        {
          alreadyKicked = true;
        }

        RString name;
        for (int i=0; i<_players.Size(); i++)
        {
          if (_players[i].dpid == from)
          {
            name = _players[i].name;
            if (_players[i].kickedOff) alreadyKicked = true;
            break;
          }
        }

        if (alreadyKicked)
        {
          LogF
          (
            "Transfer file %s request ignored, sender is being kicked off",
            (const char *)transfer._path
          );
          break;
        }
        // check if file transferred is within allowed range
        // if not, kick off sender
        if (transfer._totSize>MaxCustomFileSize)
        {
          // check name of player (from file name)
          RString reason = Format
          (
              LocalizeString(IDS_DS_PLAYER_KICKED_BIG_CUSTOM_FILE),
              (const char *)name,(const char *)GetRelUploadPath(transfer._path,name),
              transfer._totSize,MaxCustomFileSize
          );
          ServerMessage(reason);
          KickOff(from,KORKick,reason);
          break;
        }

        if (!CheckValidUpload(transfer._path,name))
        {
          RString reason = Format
          (
              LocalizeString(IDS_DS_PLAYER_KICKED_INVALID_CUSTOM_FILE),
              (const char *)name,(const char *)transfer._path
          );
          ServerMessage(reason);
          KickOff(from,KORKick, reason);
          break;
        }

        ReceiveFileSegment(transfer);
      }
      break;
/*
    case NMTRespawn:
      {
        NetworkId group;
        if (ctx.GetId("group", group) == TMOK)
        {
          NetworkObjectInfo *info = GetObjectInfo(group);
          if (info)
          {
            if (info->pendingOwner != NO_CLIENT)
            {
              WaitingMessageInfo *waitingMsg = new WaitingMessageInfo(type,msg);
              info->waitingMessages.Add(waitingMsg);
            }
            else
            {
              NetworkComponent::SendMsg(info->owner, msg, type, NMFGuaranteed);
            }
          }
          else
          {
            RptF("Server: Group %d:%d not found", group.creator, group.id);
          }
        }
      }
      break;
*/
    case NMTSetFlagOwner:
      {
        PREPARE_TRANSFER(SetFlagOwner)
        
        NetworkId id;
        if (TRANSF_GET_ID_BASE(carrier, id) == TMOK)
        {
          NetworkObjectInfo *info = GetObjectInfo(id);
          if (info)
          {
            if (info->pendingOwner != NO_CLIENT)
            {
              WaitingMessageInfo *waitingMsg = new WaitingMessageInfo(type,msg);
              info->waitingMessages.Add(waitingMsg);
            }
            else
            {
              NetworkComponent::SendMsg(info->owner, msg, type, NMFGuaranteed);
            }
          }
          else
          {
            RptFObjectNotFound(id.creator, id.id, type);
          }
        }
      }
      break;
    case NMTSetFlagCarrier:
      {
        PREPARE_TRANSFER(SetFlagOwner)

        NetworkId id;
        if (TRANSF_GET_ID_BASE(owner, id) == TMOK)
        {
          NetworkObjectInfo *info = GetObjectInfo(id);
          if (info)
          {
            if (info->pendingOwner != NO_CLIENT)
            {
              WaitingMessageInfo *waitingMsg = new WaitingMessageInfo(type,msg);
              info->waitingMessages.Add(waitingMsg);
            }
            else
            {
              NetworkComponent::SendMsg(info->owner, msg, type, NMFGuaranteed);
            }
          }
          else
          {
            RptFObjectNotFound(id.creator, id.id, type);
          }
        }
      }
      break;
    case NMTDeleteObject:
      if (_serverState < NSSLoadingGame) break; // updates from the last session
      {
        PREPARE_TRANSFER(DeleteObject)

        NetworkId id;
        if (TRANSF_BASE(creator, id.creator) != TMOK) break;
        if (TRANSF_BASE(id, id.id) != TMOK) break;
        OnObjectDestroy(id);
// RptF("Server: Object %d:%d deleted", id.creator, id.id);
      }
      break;
    case NMTDeleteCommand:
      if (_serverState < NSSLoadingGame) break; // updates from the last session
      {
        PREPARE_TRANSFER(DeleteCommand)

        NetworkId id;
        if (TRANSF_BASE(creator, id.creator) != TMOK) break;
        if (TRANSF_BASE(id, id.id) != TMOK) break;
        // destroy command object
        int owner = PerformObjectDestroy(id);
        // notify all players command was deleted
        for (int i=0; i<_players.Size(); i++)
        {
          NetworkPlayerInfo &pInfo = _players[i];
          if (pInfo.clientState < NCSMissionReceived) continue;
          if (pInfo.dpid == owner) continue;
          NetworkComponent::SendMsg(pInfo.dpid, msg, type, NMFGuaranteed);
        }
      }
      break;
    case NMTCreateObject:
    case NMTCreateVehicle:
    case NMTCreateDetector:
    case NMTCreateShot:
    case NMTCreateCrater:
    case NMTCreateCraterOnVehicle:
    case NMTCreateObjectDestructed:
    case NMTCreateDynSoundSource:
    case NMTCreateAICenter:
    case NMTCreateAIGroup:
    case NMTCreateAISubgroup:
    case NMTCreateAIBrain:
    case NMTCreateAIUnit:
    case NMTCreateAIAgent:
    case NMTCreateAITeam:
    case NMTCreateCommand:
    case NMTCreateHelicopter:
    case NMTCreateSeagull:
    case NMTCreateTurret:
    case NMTAIStatsMPRowCreate:
    case NMTCreateDestructionEffects:
    case NMTCreateClientCameraPosition:
      if (_serverState < NSSLoadingGame) break; // updates from the last session
      {
        PREPARE_TRANSFER(NetworkObject)

        NetworkId id;
        if (TRANSF_BASE(objectCreator, id.creator) != TMOK) break;
        if (TRANSF_BASE(objectId, id.id) != TMOK) break;
        // copy message (avoid changes on message in message queue)
        Ref<NetworkMessage> copy = ::CreateNetworkMessage(type);
        CopyMsg(copy, msg, format);
        OnObjectCreate(id, from, copy, type);
// RptF("Server: Object %d:%d created, type %s", id.creator, id.id, cc_cast(GetMsgTypeName(type)));
        for (int i=0; i<_players.Size(); i++)
        {
          NetworkPlayerInfo &info = _players[i];
          if (info.dpid == from) continue;
          if (info.clientState < NCSMissionReceived) continue;
          NetworkComponent::SendMsg(info.dpid, msg, type, NMFGuaranteed);
        }
        if (type == NMTCreateAIBrain || type == NMTCreateAIUnit || type == NMTCreateAIAgent)
        {
          PREPARE_TRANSFER(CreateAIBrain)

          NetworkId person;
          if (TRANSF_GET_ID_BASE(person, person) == TMOK)
          {
            for (int i=0; i<_players.Size(); i++)
            {
              AddPersonUnitPair(person, id);
              NetworkPlayerInfo &info = _players[i];
              if (info.person == person)
              {
                // change owner
                info.unit = id;
                ChangeOwner(id, from, info.dpid);
                break;
              }
            }
          }
        }
      }
      break;
    case NMTUpdateDamageVehicleAI:
    case NMTUpdateDamageVehicle:
    case NMTUpdateDamageObject:
      ctx.SetClass(NMCUpdateDamage);
      goto Update;
    case NMTUpdatePositionVehicle:
    case NMTUpdatePositionMan:
    case NMTUpdatePositionTank:
    case NMTUpdatePositionCar:
    case NMTUpdatePositionAirplane:
    case NMTUpdatePositionHelicopter:
    case NMTUpdatePositionShip:
    case NMTUpdatePositionDoor:
    case NMTUpdatePositionSeagull:
    case NMTUpdatePositionMotorcycle:
    case NMTUpdatePositionTurret:
      ctx.SetClass(NMCUpdatePosition);
      // continue with update
    case NMTUpdateTransport:
    case NMTUpdateTankOrCar:
    case NMTUpdateTank:
    case NMTUpdateCar:
    case NMTUpdateAirplane:
    case NMTUpdateHelicopter:
    case NMTUpdateParachute:
    case NMTUpdateShip:
    case NMTUpdateDoor:
    case NMTUpdateSeagull:
    case NMTUpdateObject:
    case NMTUpdateVehicle:
    case NMTUpdateDetector:
    case NMTUpdateFlag:
    case NMTUpdateShot:
    case NMTUpdateMine:
    case NMTUpdateVehicleAI:
    case NMTUpdateVehicleAIFull:
    case NMTUpdateVehicleBrain:
    case NMTUpdateMan:
    case NMTUpdateAICenter:
    case NMTUpdateAIGroup:
    case NMTUpdateAISubgroup:
    case NMTUpdateAIBrain:
    case NMTUpdateAIUnit:
    case NMTUpdateAIAgent:
    case NMTUpdateAITeam:
    case NMTUpdateCommand:
    case NMTUpdateMotorcycle:
    case NMTUpdateFireplace:
    case NMTAIStatsMPRowUpdate:
    case NMTUpdateTurret:
    case NMTUpdateClientCameraPosition:
    Update:
      if (_serverState < NSSLoadingGame) break; // updates from the last session
      {
        PREPARE_TRANSFER(NetworkObject)

        NetworkId id;
        if (TRANSF_BASE(objectCreator, id.creator) != TMOK) break;
        if (TRANSF_BASE(objectId, id.id) != TMOK) break;
        NetworkObjectInfo *oInfo = OnObjectUpdate(id, from, msg, type, ctx.GetClass());
        if (!oInfo) break;
        bool guaranteed;
        if (TRANSF_BASE(guaranteed, guaranteed) != TMOK) guaranteed = false;
        // if (_state < NGSPlay)
        if (guaranteed)
        {
          // init state transfer, send immediately
          for (int i=0; i<_players.Size(); i++)
          {
            NetworkPlayerInfo &pInfo = _players[i];
            if (pInfo.dpid == from) continue;
            if (pInfo.clientState < NCSMissionReceived) continue;

            UpdateObject(&pInfo, oInfo, ctx.GetClass(), NMFGuaranteed);
          }
        }

        if (ctx.GetClass() != NMCUpdateGeneric) break;
        if (IsUpdateTransport(type))
        {
          PREPARE_TRANSFER(UpdateTransport)

          // vehicle is always simulated where driver is local
          // if there is no driver, it can be simulated on any computer
          NetworkId driver;
          if (TRANSF_GET_ID_BASE(driver, driver) == TMOK)
          {
            // check for NULL id
            if (!driver.IsNull())
            {
              int newOwner = FindOwner(driver);
              if (newOwner != 0) ChangeOwner(id, oInfo->owner, newOwner);
            }
          }
        }
        else if (type == NMTUpdateTurret)
        {
          PREPARE_TRANSFER(UpdateTurret)

          // turret is always simulated where gunner is local
          // if there is no gunner, it can be simulated on any computer
          NetworkId gunner;
#if _VBS3 //commander override, change locality!
          NetworkId overriddenByTurret;
          bool override = false;
          if (TRANSF_GET_ID_BASE(overriddenByTurret, overriddenByTurret) == TMOK)
          {
            if(!overriddenByTurret.IsNull())
            {
              int newOwner = FindOwner(overriddenByTurret);
              if (newOwner != 0) 
              {
                ChangeOwner(id, oInfo->owner, newOwner);
                override = true;
              }
            }
          }
          if(!override)
#endif
          if (TRANSF_GET_ID_BASE(gunner, gunner) == TMOK)
          {
            // check for NULL id
            if (!gunner.IsNull())
            {
              int newOwner = FindOwner(gunner);
              if (newOwner != 0) ChangeOwner(id, oInfo->owner, newOwner);
            }
          }
        }
        else if (type == NMTUpdateAIGroup)
        {
          PREPARE_TRANSFER(UpdateAIGroup)

          NetworkId leader;
          if (TRANSF_GET_ID_BASE(leader, leader) == TMOK)
          UpdateGroupLeader(id, leader);
        }
        else if (type == NMTUpdateAISubgroup)
        {
          PREPARE_TRANSFER(UpdateAISubgroup)

          // Update AISubgroup::group in oInfo->create message
          NetworkId group;
          if (TRANSF_GET_ID_BASE(group, group) == TMOK)
          {
            NetworkMessageCreateAISubgroup *message = (NetworkMessageCreateAISubgroup *)oInfo->create.message.GetRef();
            message->_group = group;
          }

          // Update AIUnit::subgroup in oInfo->create messages
          AutoArray<NetworkId> units;
          if (TRANSF_GET_IDS_BASE(units, units) == TMOK)
          {
            NetworkId subgroup = oInfo->id;
            for (int i=0; i<units.Size(); i++)
            {
              NetworkObjectInfo *oInfo = GetObjectInfo(units[i]);
              if (!oInfo)
              {
                RptF("Unit %d:%d not found, cannot update", units[i].creator, units[i].id);
                continue;
              }
              NetworkMessageCreateAIUnit *message = (NetworkMessageCreateAIUnit *)oInfo->create.message.GetRef();
              message->_subgroup = subgroup;
            }
          }
        }
        else if (type == NMTUpdateAIBrain || type == NMTUpdateAIUnit || type == NMTUpdateAIAgent)
        {
          PREPARE_TRANSFER(UpdateAIBrain)

          NetworkId person;
          if (TRANSF_GET_ID_BASE(person, person) == TMOK)
          {
            NetworkMessageCreateAIBrain *message = (NetworkMessageCreateAIBrain *)oInfo->create.message.GetRef();
            NetworkId oldPerson = message->_person;
            if (person != oldPerson)
            {
LogF("*** AIUnit::_person updated (%d:%d -> %d:%d) for unit %d:%d", oldPerson.creator, oldPerson.id, person.creator, person.id, id.creator, id.id);
              // Update AIUnit::person in oInfo->create message
              message->_person = person;
            }
          }
          NetworkId remoteControlled;
          if (TRANSF_GET_ID_BASE(remoteControlled, remoteControlled) == TMOK && !remoteControlled.IsNull())
          {
            int from = FindOwner(id);
            int to = FindOwner(remoteControlled);
            if (to!=0)
            {
              ChangeOwner(id, from, to);
              NetworkId personId = UnitToPerson(id);
              ChangeOwner(personId, from, to);
            }
          }
        }
      }
      break;
    case NMTUpdateClientInfo:
      {
        ClientInfoObject clientInfo;
        clientInfo.TransferMsg(ctx);
        NetworkPlayerInfo *pInfo = GetPlayerInfo(from);
        if (pInfo && msg->time > pInfo->cameraPositionTime)
        {
          pInfo->cameraPosition = clientInfo._cameraPosition;
          pInfo->cameraPositionTime = msg->time;
#if _VBS3
          pInfo->cameraFov = clientInfo._cameraFov;
          pInfo->cameraDirection = clientInfo._cameraDirection;
#endif

          if (_serverState < NSSLoadingGame) break; // updates from the last session
          {
            PREPARE_TRANSFER(UpdateClientInfo);

            NetworkId id;
            if (TRANSF_BASE(objectCreator, id.creator) != TMOK) break;
            if (TRANSF_BASE(objectId, id.id) != TMOK) break;
            // Client Info need to be updated on Clients only for dead players (VoN Direct Speech needs position)
            if (CheckPlayerInLobby(*pInfo))
            {
              // create if not created yet
              OnObjectCreate(id, from, NULL, NMTNone);

              clientInfo._dpnid = from;

              Ref<NetworkMessage> msg2 = ::CreateNetworkMessage(NMTUpdateClientInfoDpid);
              msg2->time = Glob.time;
              NetworkMessageContext ctx2(msg2, this, from, MSG_SEND);
              ctx2.SetClass(NMCUpdatePosition);
              
              NetworkMessageUpdateClientInfoDpid *message2 = static_cast<NetworkMessageUpdateClientInfoDpid *>(msg2.GetRef());
              message2->_guaranteed = false;

              TMError err = clientInfo.TransferMsg(ctx2);
              if (err == TMOK)
              {
                OnObjectUpdate(id, from, msg2, NMTUpdateClientInfoDpid, NMCUpdatePosition);
              }
            }
          }
        }
      }
      break;
    case NMTUpdateClientInfoDpid:
      Fail("Unexpected on server");
      break;
    case NMTChat:
      {
        PREPARE_TRANSFER(Chat)

        AutoArray<NetworkId> units;
        if (TRANSF_GET_IDS_BASE(units, units) != TMOK) break;
        AUTO_STATIC_ARRAY(int, players, 32);
        int channel;
        if (TRANSF_BASE(channel, channel) != TMOK) break;
        if (channel & 0x80000000)
        {
          //@{ Ant1HaX0R section
          NetworkPlayerInfo *info = GetPlayerInfo(from);
          if (info)
          {
            // does the client know the level 2 signatures asks?
            RString text;
            if (TRANSF_BASE(text, text) != TMOK) break;
            bool knowLevel2 = (stricmp(text, "2")==0);
            // loaded bank count hidden inside the NMTChat ... channel = ( #loaded banks xor (dpnid & 0x7fffffff) ) | 0x80000000
            int bankCount = (channel & 0x7fffffff) ^ (from  & 0x7fffffff);
            UpdatePlayerBankCount(info, bankCount, knowLevel2);
          }
          break;
          //@}
        }
        if (channel != CCGlobal && units.Size() > 0)
          GetPlayersOnChannel(players, units, from, false);
        else
          GetPlayersOnChannel(players, channel, from, false);
        for (int i=0; i<players.Size(); i++)
        {
          NetMsgFlags priority =
          (
            _serverState == NSSPlaying ? NMFNone : NMFHighPriority
          ); 
          NetworkComponent::SendMsg(players[i], msg, type, NMFGuaranteed|priority);
        }
      }
      break;
    case NMTLocalizedChat:
      {
        PREPARE_TRANSFER(LocalizedChat)

        AutoArray<NetworkId> units;
        if (TRANSF_GET_IDS_BASE(units, units) != TMOK) break;
        AUTO_STATIC_ARRAY(int, players, 32);
        int channel;
        if (TRANSF_BASE(channel, channel) != TMOK) break;
        if (channel != CCGlobal && units.Size() > 0)
          GetPlayersOnChannel(players, units, from, false);
        else
          GetPlayersOnChannel(players, channel, from, false);
        for (int i=0; i<players.Size(); i++)
        {
          NetMsgFlags priority =
          (
            _serverState == NSSPlaying ? NMFNone : NMFHighPriority
          ); 
          NetworkComponent::SendMsg(players[i], msg, type, NMFGuaranteed|priority);
        }
      }
      break;
    case NMTRadioChat:
      {
        PREPARE_TRANSFER(RadioChat)
        EXTRACT_UNITS
        EXTRACT_CHANNEL
      }
      goto LabelChat;
    case NMTRadioChatWave:
      {
        PREPARE_TRANSFER(RadioChatWave)
        EXTRACT_UNITS
        EXTRACT_CHANNEL
      }
      goto LabelChat;
    case NMTMarkerCreate:
      {
        PREPARE_TRANSFER(MarkerCreate)
        EXTRACT_UNITS
        EXTRACT_CHANNEL
#if _VBS3
        AddInitAndRemoveOverridden(type, msg); // saved for JIP players
#endif
      }
    LabelChat:
      {
        AUTO_STATIC_ARRAY(int, players, 32);
        if (channel != CCGlobal)
          GetPlayersOnChannel(players, units, from, false);
        else
          GetPlayersOnChannel(players, channel, from, false);
        for (int i=0; i<players.Size(); i++)
        {
          NetworkComponent::SendMsg(players[i], msg, type, NMFGuaranteed);
        }
      }
      break;
    case NMTNetworkCommand:
      {
        NetworkCommandMessage cmd;
        cmd.TransferMsg(ctx);
        switch (cmd._type)
        {
#if _ENABLE_DEDICATED_SERVER
        case NCMTLogin:
          {
            if (_dedicated && (_gameMaster == AI_PLAYER || _admin))
            {
              static const int MaxLoginAttempts = 3;
              static const DWORD DelayAfterMaxAttempts = 10 * 1000; // delay in ms

              NetworkPlayerInfo *info = GetPlayerInfo(from);
              if (!info || GlobalTickCount() < info->_loginDisabledUntil) break;

              RString password = cmd._content.ReadString();
              ConstParamEntryPtr entry = _serverCfg.FindEntry("passwordAdmin");
              if (entry)
              {
                RString realPassword = *entry;
                if (strcmp(password, realPassword) != 0)
                {
                  info->_loginFailedCount++;
                  if (info->_loginFailedCount >= MaxLoginAttempts)
                  {
                    info->_loginFailedCount = 0;
                    info->_loginDisabledUntil = GlobalTickCount() + DelayAfterMaxAttempts;
                  }
                  break;
                }
              }

              info->_loginFailedCount = 0;

              if (_gameMaster != AI_PLAYER)
              {
                NetworkCommandMessage answer;
                answer._type = NCMTLoggedOut;
                SendMsg(_gameMaster, &answer, NMFGuaranteed);
              }
              
              SetGameMaster(from, false);
            }
          }
          break;
        case NCMTLogout:
          if (_dedicated && from == _gameMaster)
          {
            NetworkCommandMessage answer;
            answer._type = NCMTLoggedOut;
            SendMsg(_gameMaster, &answer, NMFGuaranteed);

            RString name;
            for (int i=0; i<_players.Size(); i++)
              if (_players[i].dpid == _gameMaster)
              {
                name = _players[i].name;
                break;
              }
            ConsoleF(LocalizeString(IDS_DS_ADMIN_LOGGED_OUT), (const char *)name);

            _gameMaster = AI_PLAYER;
            _sessionLocked = false;
            _debugOn.Clear();

            UpdateAdminState();

            if (_mission[0] == '?' && _mission[1] == 0)
            {
              // when admin logged out, perform mission voting instead
              NetworkCommandMessage answer;
              answer._type = NCMTVoteMission;
              NetworkCommandMessage answer2;
              answer2._type = NCMTVoteMissionExt;
              AddMissionList(answer,answer2);
              for (int i=0; i<_identities.Size(); i++)
              {
                SendMsg(_identities[i].dpnid, &answer, NMFGuaranteed);
                SendMsg(_identities[i].dpnid, &answer2, NMFGuaranteed);
              }
            }
          }
          break;
        case NCMTShutdown:
          if (_dedicated && ((from == _gameMaster && !_admin) || from==BOT_CLIENT))
          {
            #ifdef _XBOX
            #elif defined _WIN32
            ::PostMessage((HWND)hwndApp, WM_CLOSE, 0, 0);
            #else
            interrupted = true;
            #endif
          }
          break;
#if _VBS3
        case NCMTExitAll:
          {
            if (_dedicated && from == _gameMaster)
            {
              // send command to all clients to shut down!
              for (int i=0; i<_players.Size(); i++)
              {
                NetworkPlayerInfo &info = _players[i];
                NetworkComponent::SendMsg(info.dpid, msg, type, NMFGuaranteed);
              }
            }
          }
          break;
#endif
        case NCMTMonitorAsk:
          if (_dedicated && ((from == _gameMaster) || from==BOT_CLIENT) )
          {
            float interval;
            cmd._content.Read(&interval, sizeof(interval));
            Monitor(interval);
          }
          break;
        case NCMTDebugAsk:
          if
          (
            (_dedicated && from == _gameMaster && !_admin) ||
             from==BOT_CLIENT
          )
          {
            DebugAsk(cmd._content.ReadString());
          }
          break;
        case NCMTMission:
          if (_dedicated && (from == _gameMaster || from==BOT_CLIENT))
          {
            _mission = cmd._content.ReadString();

            RString diffName = cmd._content.ReadString();
            _difficulty = Glob.config.diffDefault;
            int difficulty = Glob.config.diffNames.GetValue(diffName);
            if (difficulty >= 0) _difficulty = difficulty;
            if (_mission.GetLength() > 0) _restart = true;
            if (_serverState != NSSPlaying)
            {
              GNetworkManager.DestroyAllObjects();
              SetServerState(NSSSelectingMission);
            }
          }
          break;
        case NCMTMissions:
          if (_dedicated && (from == _gameMaster || from==BOT_CLIENT))
          {
            _mission = "?";
            _restart = true;
            if (_serverState != NSSPlaying)
            {
              GNetworkManager.DestroyAllObjects();
              SetServerState(NSSSelectingMission);
            }
            NetworkCommandMessage answer;
            answer._type = NCMTVoteMission;
            NetworkCommandMessage answer2;
            answer2._type = NCMTVoteMissionExt;
            AddMissionList(answer,answer2);
            SendMsg(_gameMaster, &answer, NMFGuaranteed);
            SendMsg(_gameMaster, &answer2, NMFGuaranteed);
          }
          break;
        case NCMTRestart:
          if (_dedicated && _serverState == NSSPlaying && 
              (from == _gameMaster || from==BOT_CLIENT) ) _restart = true;
          break;
        case NCMTReassign:
          if (_dedicated && (from == _gameMaster || from==BOT_CLIENT))
          {
            _restart = true;
            _reassign = true;
            if (_serverState != NSSPlaying)
            {
              GNetworkManager.DestroyAllObjects();

              // unassign all players
              #if _ENABLE_AI
                int flags = _missionHeader._disabledAI ? PRFNone : PRFEnabledAI;
              #else
                int flags = PRFNone;
              #endif
              for (int i=0; i<_playerRoles.Size(); i++)
              {
                _playerRoles[i].player = AI_PLAYER;
                _playerRoles[i].flags = flags;
              }
              // send to clients
              for (int i=0; i<_players.Size(); i++)
              {
                NetworkPlayerInfo &info = _players[i];
                if (info.clientState < NCSConnected) continue;
                SendMissionInfo(info.dpid, true);
              }

              SetServerState(NSSAssigningRoles);

              _restart = false;
              _reassign = false;
            }
          }
          break;
        case NCMTInit:
          if (_dedicated && (from == _gameMaster || from==BOT_CLIENT))
          {
            RString GetServerConfig();
            _serverCfg.Parse(GetServerConfig(), NULL, NULL, &GParsingNamespace); // parsing namespace (persistent namespace needed for persistent ParamFile)
          }
          break;
        case NCMTLockSession:
          if (_dedicated && ( (from == _gameMaster && !_admin) || from==BOT_CLIENT ) )
          {
            bool lock;
            if ( cmd._content.Read(&lock, sizeof(bool)) )
              LockSession(lock);
          }
          break;
#endif
        case NCMTKick:
          if
          (
            from == BOT_CLIENT
#if _ENABLE_DEDICATED_SERVER
            || from == _gameMaster
#endif
          )
          {
            int player;
            if
            (
              cmd._content.Read(&player, sizeof(int)) &&
              player != BOT_CLIENT
            ) KickOff(player,KORKick);
          }
          break;
#if _ENABLE_DEDICATED_SERVER
        case NCMTVote:
#ifdef _XBOX
          if (_dedicated)
#else
          if (_dedicated && _gameMaster == AI_PLAYER)
#endif
          {
            const NetworkPlayerInfo *info = GetPlayerInfo(from);
            if (!info) break;
            bool doEcho = false;
            LocalizedFormatedString echo;
            int subtype;
            cmd._content.Read(&subtype, sizeof(int));
            switch (subtype)
            {
              case NCMTMission:
                Fail("Command-style mission voting no longer supported");
                break;
              case NCMTMissions:
                _votings.Add(this, (char *)&subtype, sizeof(int), _voteThreshold, from);
                echo.SetFormat(LocalizedString::Stringtable,"STR_DS_VOTES_MISSIONS");
                echo.AddArg(LocalizedString::PlainText,info->name);
                doEcho = true;
                break;
              case NCMTRestart:
                _votings.Add(this, (char *)&subtype, sizeof(int), _voteThreshold, from);
                echo.SetFormat(LocalizedString::Stringtable,"STR_DS_VOTES_RESTART");
                echo.AddArg(LocalizedString::PlainText,info->name);
                doEcho = true;
                break;
              case NCMTReassign:
                _votings.Add(this, (char *)&subtype, sizeof(int), _voteThreshold, from);
                echo.SetFormat(LocalizedString::Stringtable,"STR_DS_VOTES_REASSIGN");
                echo.AddArg(LocalizedString::PlainText,info->name);
                doEcho = true;
                break;
              case NCMTKick:
                {
                  int id[2];
                  id[0] = subtype;
                  cmd._content.Read(&id[1], sizeof(int));
                  _votings.Add(this, (char *)id, 2 * sizeof(int), _voteThreshold, from);
                  const NetworkPlayerInfo *kickInfo = GetPlayerInfo(id[1]);
                  if (kickInfo)
                  {
                    echo.SetFormat(LocalizedString::Stringtable,"STR_DS_VOTES_KICK");
                    echo.AddArg(LocalizedString::PlainText,info->name);
                    echo.AddArg(LocalizedString::PlainText,kickInfo->name);
                    doEcho = true;
                  }
                }
                break;
              case NCMTAdmin:
                {
                  char *ptr = cmd._content.Data() + cmd._content.GetPos();
                  int size = cmd._content.Size() - cmd._content.GetPos();
                  _votings.Add(
                    this, (char *)&subtype, sizeof(int), _voteThreshold, from,
                    ptr, size
                  );
                  int player = *(int *)ptr;
                  RString name;
                  for (int i=0; i<_players.Size(); i++)
                    if (_players[i].dpid == player)
                    {
                      name = _players[i].name;
                      break;
                    }
                  echo.SetFormat(LocalizedString::Stringtable,"STR_DS_VOTES_ADMIN");
                  echo.AddArg(LocalizedString::PlainText,info->name);
                  echo.AddArg(LocalizedString::PlainText,name);
                  doEcho = true;
                }
                break;
            }
            // Send voting echo to all other players
            if (doEcho)
            {
              LocalizedChatMessage chat;
              chat._channel = CCGlobal;
              chat._args = echo._args;
              for (int i=0; i<_players.Size(); i++)
              {
                if (_players[i].clientState >= NCSConnected)
                  SendMsg(_players[i].dpid, &chat, NMFGuaranteed);
              }
            }
          }
          break;
        case NCMTExec:
          {
            if (_dedicated && ( (from == _gameMaster && !_admin) || from==BOT_CLIENT ) )
            {
              RString expression = cmd._content.ReadString();

              GameVarSpace local(false);
              _evaluator.BeginContext(&local);
              GameValue result = _evaluator.EvaluateMultiple(expression, GameState::EvalContext::_default, &_globals); // server namespace
              _evaluator.EndContext();

              if (!result.GetNil())
              {
                NetworkCommandMessage msg;
                msg._type = NCMTExecResult;
                msg._content.WriteString(result.GetText());
                SendMsg(from, &msg, NMFGuaranteed);
              }
            }
          }
          break;
        case NCMTAskVoNDiagnostics:
          {
            // convert dpnid of sender to CD based ID
            PlayerIdentity *id = FindIdentity(from);
            RString cdid;
            if (id) cdid = id->id;
            if (cdid.IsEmpty()) cdid = Format("#%d", from);
            RString response = cmd._content.ReadString();
            GDebugVoNBank.AddClientResponse(cdid, response);
          }
          break;
#if _USE_BATTL_EYE_SERVER
          /// BattlEye server command
        case NCMTBattlEye:
          if (_dedicated && ( (from == _gameMaster && !_admin) || from==BOT_CLIENT ) )
          {
            RString command = cmd._content.ReadString();
            _beIntegration.Command(command);
          }
          break;
#endif
#endif
        default:
          Fail("Network command type");
          break;
        }
      }
      break;
    case NMTVoteMission:
#if _ENABLE_DEDICATED_SERVER
      if (_gameMaster == AI_PLAYER) // voting enabled only when no admin
      {
        VoteMissionMessage vote;
        vote.TransferMsg(ctx);
        int diffId = NCMTDifficulty;
        // difficulty voting is never finished
        int minVotePlayers = (GetMaxPlayers() + 1) / 2;
        _votings.Add(
          this, (char *)&diffId, sizeof(diffId), 1e6, from,
          vote._diff, vote._diff.GetLength(), false, minVotePlayers
        );
        int missId = NCMTMission;
        _votings.Add(
          this, (char *)&missId, sizeof(missId), 0.9999, from,
          vote._name, vote._name.GetLength(), true, minVotePlayers
        );
        // note: voting may have been completed by the vote
        MissionVotingEcho();
      }
#endif
      break;
    case NMTMissionParams:
      {
        MissionParamsMessage params;
        params.TransferMsg(ctx);

        _param1 = params._param1;
        _param2 = params._param2;

        for (int i=0; i<_players.Size(); i++)
        {
          NetworkPlayerInfo &info = _players[i];
          if (info.dpid == from) continue;
          if (info.clientState < NCSConnected) continue;
          NetworkComponent::SendMsg(info.dpid, msg, type, NMFGuaranteed);
        }
      }
      break;
#if _ENABLE_MP
    case NMTMuteList:
      {
        NetworkPlayerInfo *info = GetPlayerInfo(from);
        if (info)
        {
          info->muteList.TransferMsg(ctx);
          // send remote mute lists to other players
          for (int i=0; i<_identities.Size(); i++)
          {
            SendMuteList(_identities[i]);
          }
        }
      }
      break;
#endif
    case NMTVoiceOn:
      {
        VoiceOnMessage ask;
        ask.TransferMsg(ctx);
#if _GAMES_FOR_WINDOWS || defined _XBOX
        PlayerIdentity *identity = FindIdentity(ask._dpnid);
        if (identity) identity->voiceOn = ask._voiceOn;
#endif
        for (int i=0; i<_players.Size(); i++)
        {
          NetworkPlayerInfo &info = _players[i];
          if (info.dpid == from) continue;
          if (info.clientState < NCSConnected) continue;
          NetworkComponent::SendMsg(info.dpid, msg, type, NMFGuaranteed);
        }
      }
      break;
    case NMTDataSignatureAnswer:
      {
        DataSignatureAnswerMessage answer;
        answer.TransferMsg(ctx);
#define PROFILE_SIGNATURES 0
#if _ENABLE_PERFLOG && PROFILE_SIGNATURES
        LogF("NMTDataSignatureAnswer START: %s", cc_cast(answer._filename));
        PROFILE_SCOPE_EX(NMTDSA,*);
        int time1;
        int time2;
        int time3;
        int time4;
#endif
        NetworkPlayerInfo *info = GetPlayerInfo(from);
        if (!info) break;

        if (answer._index >= 0)
        {
          // remove from the list of pending checks
          if (!info->signatureChecks.Remove(answer._index))
          { // I have not asked this question!
            if (answer._index != CollectedHashes::CollectEndIndex)
            { // backward compatibility (CollectEndIndex was not added into the pending check before)
              ReportWrongSignatures(from, answer._filename, info);
              break;
            }
          }  
        }

#if _ENABLE_PERFLOG && PROFILE_SIGNATURES
        time1 = PROFILE_SCOPE_NAME(NMTDSA).TimeSpentNorm();
#endif

        if (answer._signature.Version()<_sigVerRequired || !DataSignatures::VerifySignature(answer._hash, answer._signature))
        {
          ReportWrongSignatures(from, answer._filename, info);
          break;
        }
        else 
        {
#if _ENABLE_PERFLOG && PROFILE_SIGNATURES
          time2 = PROFILE_SCOPE_NAME(NMTDSA).TimeSpentNorm();
#endif
          if (info->hashes.AddHash(answer._index, answer._filename, answer._hash._content, answer._signature.Version(), answer._level))
          {
            ReportWrongSignatures(from, answer._filename, info);
            break;
          }
          if (answer._index >= 0)
          {
            if (QIFileFunctions::FileExists(answer._filename))
            {
              // check if the file is the same as my one
              QFBank *bank = NULL;
              for (int i=0; i<GFileBanks.Size(); i++)
              {
                if (stricmp(GFileBanks[i].GetOpenName(), answer._filename) == 0)
                {
                  bank = &GFileBanks[i];
                  break;
                }
              }

              DSHash hash;
              bool ok;
#if _ENABLE_PERFLOG && PROFILE_SIGNATURES
              time3 = PROFILE_SCOPE_NAME(NMTDSA).TimeSpentNorm();
#endif
              if (bank)
                ok = MyHash(hash._content, bank->GetPrefix(), answer._signature.Version(), answer._level);
              else
                ok = DataSignatures::GetHash(hash, answer._filename);

              if (ok &&
                (hash._content.Size() != answer._hash._content.Size() ||
                memcmp(hash._content.Data(), answer._hash._content.Data(), hash._content.Size()) != 0))
              {
                // different data
                OnHackedData(HTDifferentData, from, answer._filename);
              }
            }
          }
        }
#if _ENABLE_PERFLOG && PROFILE_SIGNATURES
        time4 = PROFILE_SCOPE_NAME(NMTDSA).TimeSpentNorm();
        extern unsigned getClockFrequency ();
        LogF("    %.2f ms spent, A NMT=%d", time1/100.0f, NMTDataSignatureAnswer);
        LogF("    %.2f ms spent, B NMT=%d", (time2-time1)/100.0f, NMTDataSignatureAnswer);
        LogF("    %.2f ms spent, C NMT=%d", (time3-time2)/100.0f, NMTDataSignatureAnswer);
        LogF("    %.2f ms spent, D NMT=%d", (time4-time3)/100.0f, NMTDataSignatureAnswer);
        LogF("    TOTAL %.2f ms spent, NMT=%d", time4/100.0f, NMTDataSignatureAnswer);
#endif
      }
      break;
    case NMTFileSignatureAnswer:
      {
        FileSignatureAnswerMessage answer;
        answer.TransferMsg(ctx);
        NetworkPlayerInfo *info = GetPlayerInfo(from);
        if (!info) break;
/*
        if (answer._index >=0)
          RptF("FILE SIG INDEX INTEGRITY: %s -> %s", cc_cast(info->hashes.hashes[answer._index].fileName), cc_cast(answer._fileName));
*/
        // remove from the list of pending checks
        if ( !info->signatureChecks.Remove(answer._fileName) )
        { // I have not asked this question!
          ReportWrongSignatures(from, answer._fileName, info);
          break;
        }

        // compute the answer hash
        DSHash answerHash;
        switch (answer._signature.Version())
        {
        case 1:
          {
            Temp<char> dummy;
            // only data content hash is signed (but pbo prefix is present in answer._dataToHash => extract it)
            answer.GetDataHash(answerHash._content);
            if (answer._index >=0)
            { // new clients (start from rev.78326) contain DataSignatureAnswer matching index
               //AddHash based on index preferably.
              if (info->hashes.AddHash(answer._index, answer.GetPrefix(), answerHash._content, dummy, answer._signature.Version(), answer._level))
              {
                ReportWrongSignatures(from, answer._fileName, info);
                break;
              }
            }
            else if (info->hashes.AddHash(answer._index, answer.GetPrefix(), answerHash._content, dummy, answer._signature.Version(), answer._level))
            {
              ReportWrongSignatures(from, answer._fileName, info);
            }
          }
          break;
        case 2:
          {
            Temp<char> dummy;
            HashCalculator calculator;
            calculator.Add(answer._dataToHash.Data(), answer._dataToHash.Size());
            AutoArray<char> finalHash;
            calculator.GetResult(finalHash);
            answerHash._content.Realloc(finalHash.Data(), finalHash.Size()); //Ver2 hash
            if (answer._level!=2)
            {
              Temp<char> dataContentHash;
              answer.GetDataHash(dataContentHash);
              //AddLevel2Hash based on bank prefix
              if (info->hashes.AddHash(answer._index, answer.GetPrefix(), dataContentHash, answerHash._content, answer._signature.Version(), answer._level)) //AddHash based on bank prefix
              {
                ReportWrongSignatures(from, answer._fileName, info);
              }
            }
            else if (info->hashes.AddHash(answer._index, answer.GetPrefix(), dummy, answerHash._content, answer._signature.Version(), answer._level) ) //AddHash based on bank prefix
            {
              ReportWrongSignatures(from, answer._fileName, info);
            }
          }
          break;
        }

        if (answer._signature.Version()<_sigVerRequired || !DataSignatures::VerifySignature(answerHash, answer._signature))
        {
          ReportWrongSignatures(from, answer._fileName, info);
          break;
        }
        else
        {
          QFBank *bank = QFBankQueryFunctions::AutoBank(answer._fileName);
          if (bank)
          {
            // check the answer has the same prefix
            if ( stricmp(answer.GetPrefix(),bank->GetPrefix())!=0 )
            {
              ReportWrongSignatures(from, answer._fileName, info);
              break;
            }
            
            DSHash myHash;
            bool ok;
            ok = MyHash(myHash._content, bank->GetPrefix(), answer._signature.Version(), answer._level);

            if (ok &&
              (myHash._content.Size() != answerHash._content.Size() ||
              memcmp(myHash._content.Data(), answerHash._content.Data(), myHash._content.Size()) != 0))
            {
              // different data
              OnHackedData(HTDifferentData, from, answer._fileName);
              break;
            }
          }
          else
          { // answer with different filename or filename does not match the prefix in answer
            ReportWrongSignatures(from, answer._fileName, info);
            break;
          }
        }
      }
      break;
    case NMTHackedData:
      {
        HackedDataMessage answer;
        answer.TransferMsg(ctx);
        OnHackedData((HackType)answer._type, from, answer._filename);
      }
      break;
    case NMTStaticObjectDestructed:
      {
        // save message for lately connecting players
        PREPARE_TRANSFER(StaticObjectDestructed)
        {
          Ref<NetworkMessageStaticObjectDestructed> jip = new NetworkMessageStaticObjectDestructed(*message);
          jip->_immediate = true;
          AddInitAndRemoveOverridden(type, jip);
        }
        // if not explicitly told, do not process message now (done through different mechanism)
        if (message->_now)
        {
          EXTRACT_ID(object)
          goto LabelAskForDamage;
        }
      }
      break;
    case NMTCDKeyCheckAnswer:
#if _ENABLE_GAMESPY && _VERIFY_CLIENT_KEY
      {
        CDKeyCheckAnswerMessage answer;
        answer.TransferMsg(ctx);

        NetworkPlayerInfo *info = GetPlayerInfo(from);
        if (!info) break;

        info->cdKeyCheck.timeout = 0; //answer arrived before timeout elapsed

        sockaddr_in address;
        if (!GetClientAddress(from, address)) break;

        gcd_authenticate_user(
          GetGameID(), from, address.sin_addr.s_addr, info->challenge, answer._response,
          OnAuthorize, OnRefreshAuthorize, this);
      }
#endif
      break;
    case NMTCDKeyRecheckAnswer:
#if _ENABLE_GAMESPY && _VERIFY_CLIENT_KEY
      {
        CDKeyRecheckAnswerMessage answer;
        answer.TransferMsg(ctx);

        gcd_process_reauth(GetGameID(), from, answer._hint, answer._response);
      }
#endif
      break;
    case NMTAskConnectVoice:
      {
        AskConnectVoiceMessage ask;
        ask.TransferMsg(ctx);

        NetworkPlayerInfo *info = GetPlayerInfo(from);
        if (!info) break;

        // report verNo to logfile
/*
//((TO_BE_REMOVED_IN_FINAL
#if !defined _XBOX
        FILE *LogFile=GetNetworkManager().GetDedicatedServerLogFile();
        if (LogFile)
        {
          fprintf(LogFile, "%s build no: %d\n", cc_cast(info->name), ask._verNo);
        }
#endif
//))TO_BE_REMOVED_IN_FINAL
*/
        info->_publicAddr = ask._publicAddr;
        info->_privateAddr = ask._privateAddr;
        info->_voicePort = ask._voicePort;
        // do some tests vor GDebugVoNBank collecting system
        sockaddr_in clAdr;
        bool noClientAddr = !_server->GetClientAddress(from, clAdr) && (from!=BOT_CLIENT);
        if (noClientAddr) GDebugVoNBank.SetFeature(DebugVoNBank::VoNFeature_GetClientAddressFailed);
#     if BETA_TESTERS_DEBUG_VON
        sockaddr_in adrPubl; adrPubl.sin_addr.s_addr = ask._publicAddr;
        sockaddr_in adrPriv; adrPriv.sin_addr.s_addr = ask._privateAddr;
        RptF("VoNLog,Server: askConnectVoice \"%s\" public=%d.%d.%d.%d private=%d.%d.%d.%d port=%d", 
          cc_cast(info->name),
          (unsigned)IP4(adrPubl),(unsigned)IP3(adrPubl),(unsigned)IP2(adrPubl),(unsigned)IP1(adrPubl),
          (unsigned)IP4(adrPriv),(unsigned)IP3(adrPriv),(unsigned)IP2(adrPriv),(unsigned)IP1(adrPriv),
          ask._voicePort
          );
        // test whether public addr seems to be valid and give some report
        if (noClientAddr) RptF("VoNLog,Server: GetClientAddress failed!");
        else
        {
          if (adrPubl.sin_addr.s_addr != clAdr.sin_addr.s_addr)
            RptF("VoNLog,Server: publicAddr mess detected: here %d.%d.%d.%d, server has %d.%d.%d.%d",
            (unsigned)IP4(adrPubl),(unsigned)IP3(adrPubl),(unsigned)IP2(adrPubl),(unsigned)IP1(adrPubl),
            (unsigned)IP4(clAdr),(unsigned)IP3(clAdr),(unsigned)IP2(clAdr),(unsigned)IP1(clAdr)
            );
        }
#     endif

        // establish peer to peer connection for each pair
        for (int i=0; i<_players.Size(); i++)
        {
          const NetworkPlayerInfo &player = _players[i];
          if (player.dpid == from) continue; // me
#if _ENABLE_CHEATS
          if (GInput.cheatVoNServer && player.dpid!=BOT_CLIENT && from!=BOT_CLIENT) continue; //force retranslation through server
          if (GInput.cheatVoNRetranslate && (player.dpid==BOT_CLIENT || from==BOT_CLIENT)) continue; //force retranslation through gameSocket
#endif
          if (player._voicePort == 0) continue; // voice not asked yet
          if (
            //(ask._serverAcc && player.dpid==BOT_CLIENT) ||
            player._publicAddr == player._privateAddr && info->_publicAddr == info->_privateAddr || // not behind a NAT
            player._publicAddr == info->_publicAddr) // behind the same NAT
          {
            // can connect directly
            ConnectVoiceDirectMessage connect;
            
            connect._dpnid = player.dpid;
            if (ask._serverAcc && player.dpid==BOT_CLIENT &&
              (player._publicAddr != player._privateAddr || info->_publicAddr != info->_privateAddr) && // at least one is behind a NAT
              player._publicAddr != info->_publicAddr // not behind the same NAT
            )
            {
#             if BETA_TESTERS_DEBUG_VON
              sockaddr_in adrPriv; adrPriv.sin_addr.s_addr = player._privateAddr;
              sockaddr_in adrPubl; adrPubl.sin_addr.s_addr = player._publicAddr;
              RptF("VonLog,Server: The client running on server, publicAddr=%d.%d.%d.%d, privateAddr=%d.%d.%d.%d",
                (unsigned)IP4(adrPubl),(unsigned)IP3(adrPubl),(unsigned)IP2(adrPubl),(unsigned)IP1(adrPubl),
                (unsigned)IP4(adrPriv),(unsigned)IP3(adrPriv),(unsigned)IP2(adrPriv),(unsigned)IP1(adrPriv)
              );
#             endif
              //connect._addr = player._publicAddr;
              connect._addr = player._privateAddr;
            }
            else
            connect._addr = player._privateAddr;
            connect._port = player._voicePort;
            SendMsg(from, &connect, NMFGuaranteed);
#         if BETA_TESTERS_DEBUG_VON
            sockaddr_in adrPriv; adrPriv.sin_addr.s_addr = connect._addr;
            RptF("VoNLog,Server: directMsgTo \"%s\" addr=%d.%d.%d.%d port=%d id=%x", 
              cc_cast(player.name),
              (unsigned)IP4(adrPriv),(unsigned)IP3(adrPriv),(unsigned)IP2(adrPriv),(unsigned)IP1(adrPriv),
              player._voicePort, player.dpid
              );
#         endif

            connect._dpnid = from;
            connect._addr = info->_privateAddr;
            connect._port = info->_voicePort;
            SendMsg(player.dpid, &connect, NMFGuaranteed);
          }
          else
          {
            // ask to start NAT Negotiation
            ConnectVoiceNatNegMessage connect;
            connect._cookie = rand32();
#if USE_NAT_NEGOTIATION_MANAGER && _ENABLE_MP
            // use PeerToPeerManager to schedule NAT negotiation to be taking place one at a time
            _p2pManager.AddEdge(from, player.dpid);
#else
            // start NAT negotiation immediately
            connect._dpnid = player.dpid;
            connect._clientIndex = 0;
            SendMsg(from, &connect, NMFGuaranteed);
#         if BETA_TESTERS_DEBUG_VON
            RptF("VoNLog,Server: NatNegMsg \"%s\" id=%x", cc_cast(player.name), player.dpid);
#         endif

            connect._dpnid = from;
            connect._clientIndex = 1;
            SendMsg(player.dpid, &connect, NMFGuaranteed);
#endif
          }
        }
      }
      break;
    case NMTAskForTeamSwitch:
      {
        PREPARE_TRANSFER(AskForTeamSwitch)
        EXTRACT_ID(to)
      }
      goto LabelAskWithVehicle;
    case NMTTeamSwitchResult:
      {
        PREPARE_TRANSFER(TeamSwitchResult)
        // send answer to the original player
        int player;
        if (TRANSF_BASE(player, player) == TMOK)
          NetworkComponent::SendMsg(player, msg, type, NMFGuaranteed);
      }
      break;
    case NMTFinishTeamSwitch:
      {
        PREPARE_TRANSFER(FinishTeamSwitch)
        // make sure owner will not change during the reaction
        // (so if some change are in progress, finish it now)
        if (!message->_fromGroup.IsNull()) UpdateGroupLeader(message->_fromGroup, message->_fromLeader);
        EXTRACT_ID(from)
      }
      goto LabelAskWithVehicle;

#if defined _XBOX && _XBOX_VER >= 200
    case NMTLocPlayerInfoList:
      {
        // sign in of some user changed - lets send this to all clients
        LocalPlayerInfoList localPlayerInfoList;
        localPlayerInfoList.TransferMsg(ctx);

        for (int i=0; i<_identities.Size(); i++)
        {
          PlayerIdentity &player = _identities[i];
          SendMsg(player.dpnid, &localPlayerInfoList, NMFGuaranteed);
        }
      }
      break;
#endif
#if USE_NAT_NEGOTIATION_MANAGER && _ENABLE_MP
      case NMTNatNegResult:
        {
          NatNegResultMessage result;
          result.TransferMsg(ctx);
#if DEBUG_NAT_NEGOTIATION_MANAGER
          LogF("NNManager: NAT negotiation finished for ids: (%d, %d) - %s", from, result._dpnid, result._result ? "Successful" : "Failed");
#endif
          //Inform PeerToPeerManager that corresponding NAT is ready to negotiate for next pair of players
          _p2pManager.UpdateNAT(from);
        }
        break;
#endif
      case NMTOwnerChanged:
        {
          OwnerChangedMessage ackMsg;
          ackMsg.TransferMsg(ctx);
          LogF("Owner of %d:%d changed", ackMsg._creator, ackMsg._id);
          NetworkId id(ackMsg._creator, ackMsg._id);
          NetworkObjectInfo *oInfo = GetObjectInfo(id);
          if (!oInfo)
          {
            RptF("Server: Object info %d:%d not found during Changing Owner", id.creator, id.id);
            return;
          }

          NetworkPlayerInfo *pInfo = GetPlayerInfo(oInfo->pendingOwner);
          if (pInfo)
          {
            for (int cls=NMCUpdateFirst; cls<NMCUpdateN; cls++)
              UpdateObject(pInfo, oInfo, (NetworkMessageClass)cls, NMFGuaranteed);
          }
          else
          { // new owner has probably disconnected meanwhile
            oInfo->pendingOwner = BOT_CLIENT;
          }
          // reset times to ensure update from new owner will take place
          for (int cls=NMCUpdateFirst; cls<NMCUpdateN; cls++)
          {
            NetworkCurrentInfo &info = oInfo->current[cls];
            if (info.message) info.message->time = Time(0); // do not use TIME_MIN - difference is undefined
          }

          if (oInfo->owner==from)
          {
            // Send message to new owner at last
            ChangeOwnerMessage msg;
            msg._creator = id.creator;
            msg._id = id.id;
            msg._owner = oInfo->pendingOwner;
            msg._disconnected = false;
            SendMsg(oInfo->pendingOwner, &msg, NMFGuaranteed);

            // Send messages waiting to be sent after owner is changed
            for (int i=0; i<oInfo->waitingMessages.Size(); i++)
            {
              WaitingMessageInfo *wMsgInfo = oInfo->waitingMessages[i];
              NetworkComponent::SendMsg(oInfo->pendingOwner, wMsgInfo->msg, wMsgInfo->type, NMFGuaranteed);
            }
            oInfo->waitingMessages.Clear();

            oInfo->DeletePlayerObjectInfo(oInfo->owner);
            oInfo->owner = oInfo->pendingOwner;
            oInfo->pendingOwner = NO_CLIENT;
          }
          else
          {
            RptF("Server: OwnerChanged of %d:%d arrived from non owner %d", id.creator, id.id, from);
          }
        }
        break;
      case NMTLoadedFromSave:
        if (_serverState==NSSLoadingGame)
        {
          //send updates
          GetNetworkManager().CreateAllObjects();
          // PublicVariable calls containing objects with NULL networkId should be processed after the CreateAllObjects call again
          GetNetworkManager().ProcessDelayedPublicVariables();
          // send delayed messages
          for (int j=0; j<_players.Size(); j++)
          {
            NetworkPlayerInfo &player = _players[j];
            if (!player._jipProcessed  && player.clientState<NCSGameLoaded && player.dpid!=BOT_CLIENT)
            {
              //LogF("*** Sending InitMessages to %d, (%d, %d)", player.dpid, player._jipProcessed, player.clientState);
              player._jipProcessed = false; //reset
              for (int i=0; i<_initMessages.Size(); i++)
              {
                if (_initMessages[i].msg)
                  NetworkComponent::SendMsg(player.dpid, _initMessages[i].msg, _initMessages[i].type, NMFGuaranteed);
              }
            }
            //else LogF("*** NOT Sending InitMessages to %d, (%d, %d)", player.dpid, player._jipProcessed, player.clientState);
          }
          // game is loaded, can switch to briefing
          GWeaponsPool.Import(ParamEntryVal(ExtParsMission)); // create pool of available weapons
          SetServerState(NSSBriefing);
          NetworkPlayerInfo *info = GetPlayerInfo(BOT_CLIENT); 
          if (info->_jip)
          {
            // can select player now - game is not running yet
            DoAssert(_parent->GetClient());
            _parent->GetClient()->JIPSelectPlayer(info->dpid); // mission must be loaded on BOT client
            info->_jip = false; // do not process later
          }
        }
        break;
    default:
      RptF("Server: Unhandled user message %s", cc_cast(GetMsgTypeName(type)));
      break;
  }

#if _ENABLE_PERFLOG
  int profileTime = PROFILE_SCOPE_NAME(OnMsg).TimeSpentNorm();
  static float profileTimeLimit = 10.0f; //ms
  if (profileTime/100.0f > profileTimeLimit)
  {
    #if _SUPER_RELEASE
      RptF("Warn: %.2f ms spent, NMT=%d", profileTime/100.0f, type);
      if (type==NMTNetworkCommand)
      {
        NetworkCommandMessage cmd;
        cmd.TransferMsg(ctx);
        RptF("      NetworkCommand: %d", cmd._type);
      }
    #else
      LogF("Warn: %.2f ms spent, NMT=%d", profileTime/100.0f, type);
    #endif
  }
#endif
}

/*!
\patch 5178 Date 11/1/2007 by Bebul
- Fixed: Default handling of different data detected was stricter then intended, resulting in a kick of players.
*/
void NetworkServer::OnHackedData(HackType type, int dpid, RString filename)
{
  const PlayerIdentity *identity = FindIdentity(dpid);
  if (!identity) return;

#if _ENABLE_DEDICATED_SERVER
  switch (type)
  {
  case HTUnsignedData:
    if (OnEvent(SEOnUnsignedData, (float)identity->playerid, filename)) return;
    break;
  case HTDifferentData:
    OnEvent(SEOnDifferentData, (float)identity->playerid, filename);
    return;
  case HTHackedData:
    if (OnEvent(SEOnHackedData, (float)identity->playerid, filename)) return;
    break;
  }
#endif

#if !SIGNATURES_TEST_ONLY
  KickOff(dpid, KORFade);
#endif
}

void NetworkServer::OnDoubleIdDetected(const PlayerIdentity &newIdentity, const PlayerIdentity &oldIdentity)
{
  // report problems on global channel
  RString message = LocalizeString(IDS_FADE_AWAY);
  RString senderName = LocalizeString(IDS_FADE_REMMEMBER);
  RefArray<NetworkObject> dummy;
  GNetworkManager.Chat(CCGlobal, senderName, dummy, message);
  GChatList.Add(CCGlobal, senderName, message, false, true);

#if _ENABLE_DEDICATED_SERVER
  if (OnEvent(SEDoubleIdDetected, (float)newIdentity.playerid, (float)oldIdentity.playerid)) return; // handled
  if (_kickDuplicate) KickOff(newIdentity.dpnid, KORKick);
#endif
}

int NetworkServer::FindOwner(const NetworkId &id)
{
  for (int i=0; i<_objects.Size(); i++)
  {
    NetworkObjectInfo &dInfo = *_objects[i];
    if (dInfo.id == id) return dInfo.owner;
  }

  LogF("Owner not known - searching player");
  for (int i=0; i<_players.Size(); i++)
  {
    NetworkPlayerInfo &pInfo = _players[i];
    if (pInfo.person == id)
    {
      // change owner
      LogF("Owner not known - player found");
      return pInfo.dpid;
    }
  }
  // not found
  return 0;
}

void NetworkServer::OnMessagePlayerRole(int from, NetworkMessageType type, NetworkMessageContext &ctx)
{
  // PATCHED
  // if (_serverState != NSSAssigningRoles) return;

  // JOIN IN PROGRESS
  if (_serverState < NSSAssigningRoles) return;

  PREPARE_TRANSFER(PlayerRole)

  int index;
  TRANSF_BASE(index, index);
  Assert(index < _playerRoles.Size());
  PlayerRole info;
  info.TransferMsg(ctx);

  int iFound = -1;
  if (info.player != AI_PLAYER)
    for (int i=0; i<_playerRoles.Size(); i++)
    {
      if (_playerRoles[i].player == info.player)
      {
        iFound = i;
        break;
      }
    }

  bool lock = 
#if _ENABLE_DEDICATED_SERVER
      from == _gameMaster ||
#endif
      from == BOT_CLIENT;

  if ( iFound>=0 && _playerRoles[iFound].GetFlag(PRFForced) || info.GetFlag(PRFForced))
  {
    //no action, role is forced, cannot be reassigned
    return;
  }

  if (lock)
  {
    if (iFound == index)
    {
      // only flags changed, do not lock / unlock slots
    }
    else
    {
      if (iFound >= 0)
      {
        _playerRoles[iFound].player = AI_PLAYER;
        _playerRoles[iFound].SetFlag(PRFLocked, false);
      }
      info.SetFlag(PRFLocked, info.player != AI_PLAYER && info.player != from);
    }
    _playerRoles[index] = info;
  }
  else
  {
    if (_playerRoles[index].GetFlag(PRFLocked)) return;
    if (iFound >= 0)
    {
      if (_playerRoles[iFound].GetFlag(PRFLocked)) return;
    }
    if
    (
      _playerRoles[index].player == AI_PLAYER && info.player == from ||
      _playerRoles[index].player == from && info.player == AI_PLAYER ||
      _playerRoles[index].player == AI_PLAYER && info.player == AI_PLAYER     // change of flags
    )
    {
      if (iFound >= 0)
      {
        _playerRoles[iFound].player = AI_PLAYER;
        _playerRoles[iFound].SetFlag(PRFLocked, false);
      }
      _playerRoles[index] = info;
    }
    else return;
  }

  {
    // create message
    Ref<NetworkMessage> msgSend = ::CreateNetworkMessage(type);
    msgSend->time = Glob.time;
    NetworkMessageContext ctxSend(msgSend, this, TO_SERVER, MSG_SEND);

    TMError err;
    NetworkMessagePlayerRole *message = (NetworkMessagePlayerRole *)msgSend.GetRef();
    message->_index = index;
    err = info.TransferMsg(ctxSend);
    if (err != TMOK) return;
    
    // send message
    for (int i=0; i<_players.Size(); i++)
    {
      NetworkPlayerInfo &player = _players[i];
      if (player.clientState < NCSConnected) continue;
//          SendMsg(player.dpid, &info, 0, index, NMFGuaranteed);
      NetworkComponent::SendMsg(player.dpid, msgSend, type, NMFGuaranteed);
    }
  }
  if (iFound >= 0 && iFound != index)
  {
    // create message
    Ref<NetworkMessage> msgSend = ::CreateNetworkMessage(type);
    msgSend->time = Glob.time;
    NetworkMessageContext ctxSend(msgSend, this, TO_SERVER, MSG_SEND);

    TMError err;
    NetworkMessagePlayerRole *message = (NetworkMessagePlayerRole *)msgSend.GetRef();
    message->_index = iFound;
    err = _playerRoles[iFound].TransferMsg(ctxSend);
    if (err != TMOK) return;
    
    // send message
    for (int i=0; i<_players.Size(); i++)
    {
      NetworkPlayerInfo &player = _players[i];
      if (player.clientState < NCSConnected) continue;
//          SendMsg(player.dpid, &info, 0, index, NMFGuaranteed);
      NetworkComponent::SendMsg(player.dpid, msgSend, type, NMFGuaranteed);
    }
  }
}

void NetworkServer::OnMessagePlayerRoleUpdate(int from, NetworkMessageType type, NetworkMessageContext &ctx)
{
  if (_serverState < NSSAssigningRoles) return;
  PlayerRoleUpdate update;
  update.TransferMsg(ctx);

  Assert(update._index < _playerRoles.Size());

  int iFound = -1;
  if (update._player != AI_PLAYER)
  {
    for (int i=0; i<_playerRoles.Size(); i++)
    {
      if (_playerRoles[i].player == update._player)
      {
        iFound = i;
        break;
      }
    }
  }
    
  if (_missionHeader._respawn!=RespawnAtPlace && _missionHeader._respawn!=RespawnInBase)
    _playerRoles[update._index].lifeState = (LifeState)update._lifeState;
  _playerRoles[update._index].player = update._player;
  // following test is a hot-fix against crashes reported on WinQual
  // maybe the message may be received in some unexpected moment?
  if (iFound != update._index && iFound>=0)
  { // clear the former role assignment
    _playerRoles[iFound].player = AI_PLAYER;
    SendPlayerRoleUpdate(iFound);
  }
  SendPlayerRoleUpdate(update._index);
}

DWORD NetworkServer::SendMsg
(
  int to, NetworkSimpleObject *object, NetMsgFlags dwFlags
)
{
  // check if message can be sent
  NetworkPlayerInfo *info = GetPlayerInfo(to);
  if (!info)
  {
    RptF("Server: cannot send message - player %d is not known.", to);
    return 0xFFFFFFFF;
  }
  if (info->clientState < NCSConnected && object->GetNMType() >= NMTFirstVariant)
  {
    RptF("Server: cannot send message - player's %s messages are not registred yet.", (const char *)info->name);
    return 0xFFFFFFFF;
  }
  return NetworkComponent::SendMsg(to, object, dwFlags);
}

void NetworkServer::EnqueueMsg
(
  int to, NetworkMessage *msg,
  NetworkMessageType type
)
{
  NetworkPlayerInfo *info = GetPlayerInfo(to);
  if (!info)
  {
    Fail("PlayerInfo");
    return;
  }
  // if 
  int index = info->_messageQueue.Add();
  info->_messageQueue[index].type = type;
  info->_messageQueue[index].msg = msg;
}

void NetworkServer::EnqueueMsgNonGuaranteed
(
  int to, NetworkMessage *msg,
  NetworkMessageType type
)
{
  NetworkPlayerInfo *info = GetPlayerInfo(to);
  if (!info)
  {
    Fail("PlayerInfo");
    return;
  }
  // if 
  int index = info->_messageQueueNonGuaranteed.Add();
  info->_messageQueueNonGuaranteed[index].type = type;
  info->_messageQueueNonGuaranteed[index].msg = msg;
}

/*!
\patch 1.43 Date 1/23/2002 by Ondra
- Fixed: Config protection message "... uses modified config file", is now more specific,
exact location of changed value is displayed.
- New: When user with different version is connected,
message containing version information is shown.
*/

//((EXE_CRC_CHECK
bool NetworkServer::IntegrityCheck(int dpnid, IntegrityQuestionType type, const IntegrityQuestion &q, int timeout)
//))EXE_CRC_CHECK
{
  #ifndef _XBOX
  NetworkPlayerInfo *info = GetPlayerInfo(dpnid);
  if (!info) return false;
  //if (info->integrityQuestions.SizeTimeout[type] != UINT_MAX) return false; // integrity check in progress

//((EXE_CRC_CHECK
//))EXE_CRC_CHECK

  int id = info->nextQuestionId++;

  //info->integrityQuestion = value;
  IntegrityQuestionInfo &qi = info->integrityQuestions.Append();
  qi.id = id;
  qi.type = type;
  qi.timeout = GlobalTickCount() + timeout;
  qi.q = q;

  #if _ENABLE_REPORT
    if (type==IQTData)
    {
      LogF("Asking %s about file %s",(const char *)info->name,(const char *)q.name);
    }
  #endif

  IntegrityQuestionMessage msg;
  msg._id = id;
  msg._type = type;
  msg._name = q.name;
  msg._offset = q.offset;
  msg._size = q.size;
  SendMsg(dpnid, &msg, NMFGuaranteed|NMFHighPriority);
  #endif
  return true;
}

ConstParamEntryPtr FindConfigParamEntry(const char *path);

#define CLASS_LINK(x) \
  ParamClassLockedPtr(const_cast<ParamClass *>(x))

IntegrityInvestigationConfig::IntegrityInvestigationConfig(const char *path)
{
  // find entry by query
  ConstParamEntryPtr entry = FindConfigParamEntry(path);
  if (!entry) entry = ConstParamEntryPtr(&Pars);
  if (!entry->IsClass())
  {
    return;
  }

  _class = CLASS_LINK(entry->GetClassInterface());
  _root = _class;
  _questionTimeout = UINT_MAX;
}

RString IntegrityInvestigationConfig::GetResult() const
{
  return _result;
}

static RString PathFirstFolder(const char *path)
{
  const char *next = strchr(path,'/');
  if (!next) return "";
  return RString(path,next-path);
}

RString ConvertContextToCfgPath(RString path)
{
  RString base = PathFirstFolder(path);
  if (!strcmpi(base,Pars.GetName()) )
  {
    RString stripBase = path.Substring(base.GetLength(),INT_MAX);
    return RString("cfg")+stripBase;
  }
/*
  else if (!strcmpi(base,Pars.GetName()) )
  {
    RString stripBase = path.Substring(base.GetLength(),INT_MAX);
    return RString("rsc")+stripBase;
  }
*/
  return path;  
}

bool IntegrityInvestigationConfig::Proceed(NetworkServer *server, NetworkPlayerInfo *pi)
{
  //if (_result.GetLength()>0) return false;
  if (!_class)
  {
    return false;
  }
  // if some question is in progress, wait until it is answered
  if (_questionTimeout<UINT_MAX)
  {
    // if investigation timed out, give result
    if (GlobalTickCount()>_questionTimeout)
    {
      Log("Investigation timed out");
      _result = "Time out (probably cheat is used)";
      return false;
    }
    return true;
  }

  // we may ask client only about verified class or about parent of verified class
  // otherwise we have no guarantee that he is able to reply, as class
  // may be non-existing in his config
  if (_class==_root)
  {
    // _root is already known to be invalid, there is no need to ask about it
    QuestionAnswered(false);
  }
  else if (_class->HasChecksum() && _class!=_root)
  {
    // perform test with current class
    RString cfgPath = ConvertContextToCfgPath(_class->GetContext());
    _question = IntegrityQuestion(cfgPath,0,0);
    // we expect to get answer within two minutes
    // if not, we consider this test failed
    _questionTimeout = GlobalTickCount()+120000;
    // ask given player this question
    #if 1
    LogF
    (
      "Asking player %s about %s",
      (const char *)pi->name,(const char *)_class->GetContext()
    );
    #endif
    server->IntegrityCheck(pi->dpid,IQTConfig,_question);
  }
  else
  {
    // class not protected - skip it
    // pretend we received an answer class is OK
    do
    {
      #if 0
      Log
      (
        "Skipping player %s about %s",
        (const char *)pi->name,(const char *)_class->GetContext()
      );
      #endif
      QuestionAnswered(true);
    }
    while (_class && !_class->HasChecksum());
  }
  
  return true;
}

bool IntegrityInvestigationConfig::QuestionMatching(const IntegrityQuestion &q) const
{
  if (q.name!=_question.name) return false;
  //if (q.offset!=_question.offset) return false;
  //if (q.size!=_question.size) return false;
  return true;
}

void IntegrityInvestigationConfig::QuestionAnswered(bool answerOK)
{
  _questionTimeout = UINT_MAX;
  // update investigation, if neccessary, set error code
  if (!answerOK)
  {
    _result = _question.name;
  }
  // if question was answered with no problems detected, we may proceed to next class
  // we should:
  // 1) pass control to our first child
  // 2) if there is none, find next class in our parent
  // if class reply is OK, there is no need to check children
  if (!answerOK)
  for (int i=0; i<_class->GetEntryCount(); i++)
  {
    ParamEntryVal entry = _class->GetEntry(i);
    if (!entry.IsClass()) continue;
    _class = CLASS_LINK(entry.GetClassInterface());
    return;
  }
  // no child, return back to our parent and find its next child
  // check if there are any more entries in current class
  /*
  if (_result.GetLength()>0)
  {
    // if we already have some result, we have to deepest node
    // we therefore want to finish
    _class = NULL;
    return;
  }
  */
  for(;;)
  {
    if (_class==_root)
    {
      // we reached root - terminate
      if (_class && _result.GetLength()==0)
      {
        _result = ConvertContextToCfgPath(_class->GetContext());
      }
      _class.Free();
      return;
    }
    const ParamClass *parent = _class->GetParent();
    if (!parent)
    {
      _class.Free();
      return;
    }
    // find _class in its parent
    int index = -1;
    for (int i=0; i<parent->GetEntryCount(); i++)
    {
      if (&parent->GetEntry(i)==_class) {index = i+1;break;}
    }
    Assert(index>=0);
    // find next child of our parent
    while (index<parent->GetEntryCount() && !parent->GetEntry(index).IsClass())
    {
      index++;
    }
    if (index>=parent->GetEntryCount())
    {
      // parent has no more children, proceed with its parent
      _class = CLASS_LINK(parent);
    }
    else
    {
      ParamEntryVal entry = parent->GetEntry(index);
      Assert (entry.IsClass());
      _class = CLASS_LINK(entry.GetClassInterface());
      return;
    }
  }
}

/*!
Check if given question is part of any investigation
and let investiogation process it.

\return true if question was part of investigation
*/


bool NetworkServer::IntegrityAnswerReceived
(
  NetworkPlayerInfo *pi,
  IntegrityQuestionType qType,
  const IntegrityQuestion &q, bool answerOK
)
{
  for (int qt=0; qt<IntegrityQuestionTypeCount; qt++)
  {
    IntegrityInvestigation *ii = pi->integrityInvestigation[qt];
    if (!ii) continue;
    // check if given answer is answer to any given question
    if (ii->QuestionMatching(q))
    {
      // update result
      ii->QuestionAnswered(answerOK);
      return true;
    }
  }
  return false;
}

/*!
  \patch 5093 Date 11/30/2006 by Ondra
  - Fixed: Modified config test is obsolete and no longer supported.
  Use verifySignatures=1 in the server.cfg to verify data are not modified by clients.
  Note: this currently prevents using any 3rd party addons as they are not signed.
*/

void NetworkServer::PerformInitialIntegrityCheck(NetworkPlayerInfo &pi)
{
  //IntegrityCheck(pi.dpid,IQTConfig,IntegrityQuestion());

//{ DEDICATED SERVER SUPPORT
#if _ENABLE_DEDICATED_SERVER

  ConstParamEntryPtr datafiles = _serverCfg.FindEntry("CheckFiles");
  if (datafiles)
  {
    for (int i=0; i<datafiles->GetSize(); i++)
    {
      RStringB file = (*datafiles)[i];
      IntegrityCheck(pi.dpid,IQTData,IntegrityQuestion(file,0,INT_MAX));
    }
  }


  //IntegrityCheck(player,IQTExe,IntegrityQuestion());
  //IntegrityCheck(player,IQTData,IntegrityQuestion());

#endif
//}

//((EXE_CRC_CHECK
#ifndef _XBOX
  const AutoArray<ExeCRCBlock> &GetCodeCheck();
  const AutoArray<ExeCRCBlock> &blocks = GetCodeCheck();
  if (blocks.Size() > 0)
  {
    // perform only the first test here - ID changer
    const ExeCRCBlock &crc = blocks[0];
    IntegrityCheck(pi.dpid, IQTExe, IntegrityQuestion("", crc.offset, crc.size));
  }
#endif
//))EXE_CRC_CHECK
}

void NetworkServer::PerformFileIntegrityCheck(const char *file, int dpid)
{
  #ifndef _XBOX
  IntegrityQuestion q;
  q.name = file;
  
  if (dpid>=0)
  {
    IntegrityCheck(dpid,IQTData,q);
  }
  else
  {
    for (int i=0; i<_players.Size(); i++)
    {
      const NetworkPlayerInfo &pi = _players[i];
      IntegrityCheck(pi.dpid,IQTData,q);
    }
  }
  #endif
}

void NetworkServer::PerformRandomIntegrityCheck(NetworkPlayerInfo &pi)
{
//((EXE_CRC_CHECK
#ifndef _XBOX
  const AutoArray<ExeCRCBlock> &GetCodeCheck();
  const AutoArray<ExeCRCBlock> &blocks = GetCodeCheck();
  if (blocks.Size() > 0)
  {
    // perform at most randomTestsAtOnce tests
    static const int randomTestsAtOnce = 4;
    int n = intMin(randomTestsAtOnce, blocks.Size()); // do not process several identical tests
    for (int i=0; i<n; i++)
    {
      if (pi.integrityCheckNextIndex >= blocks.Size()) pi.integrityCheckNextIndex = 0;
      const ExeCRCBlock &crc = blocks[pi.integrityCheckNextIndex++];
      IntegrityCheck(pi.dpid, IQTExe, IntegrityQuestion("", crc.offset, crc.size));
    }
  }
#endif
//))EXE_CRC_CHECK
}

Vector3 NetworkServer::RandomPlayerPos() const
{
  if (_players.Size())
  {
    return _players[rand() % _players.Size()].cameraPosition;
  }
  return VZero;
}

void NetworkServer::PerformIntegrityInvestigations()
{
  // scan all players
  for (int i=0; i<_players.Size(); i++)
  {
    NetworkPlayerInfo &pi = _players[i];
    bool someInvestigation = false;
    for (int q=0; q<IntegrityQuestionTypeCount; q++)
    {
      IntegrityInvestigation *ii = pi.integrityInvestigation[q];
      if (!ii) continue;
      someInvestigation = true;
      bool toBeContinued = ii->Proceed(this,&pi);
      if (!toBeContinued)
      {
        RString result = ii->GetResult();
        if (result.GetLength()>0)
        {
          // inform about result

          RString format = LocalizeString(IDS_MP_VALIDERROR + q);

          RString message = Format(format, cc_cast(pi.name)) + Format(" - %s", cc_cast(result));
          RefArray<NetworkObject> dummy;
          GNetworkManager.Chat(CCGlobal,"",dummy,message);
          GChatList.Add(CCGlobal, NULL, message, false, true);
#if _ENABLE_DEDICATED_SERVER
          ConsoleF("%s", cc_cast(message));
#endif
          Log(message);
        }
        // cancel test
        pi.integrityInvestigation[q].Free();
      }
    }

    if (!someInvestigation)
    {
      // if there is no investigation running, we may perform random integrity check
      DWORD time = GlobalTickCount();
      if (time>pi.integrityCheckNext)
      {
        int qIndex = pi.FindIntegrityQuestion(IQTConfig);
        if (qIndex>=0)
        {
          LogF("Integrity question in progress");
        }
        else
        {
          PerformRandomIntegrityCheck(pi);
          // DISABLED: random integrity check (just for sure here)
          #if _ENABLE_RANDOM_INTEGRITY_CHECK
            pi.integrityCheckNext = time+32000;
          #else
            pi.integrityCheckNext = UINT_MAX;
          #endif
        }
      }
    }

    if (pi.dpid != BOT_CLIENT)
    {
      // regular checking
      DWORD time = GlobalTickCount();
      if (time > pi.regularCheckNext && pi.clientState>=NCSGameLoaded && pi.clientState<=NCSBriefingRead)
      {
        bool handled = false;
#if _ENABLE_DEDICATED_SERVER
        const PlayerIdentity *identity = FindIdentity(pi.dpid);
        if (identity)
        {
          handled = OnEvent(SERegularCheck, identity->playerid, pi.regularCheckOrder);
          pi.regularCheckOrder++;
        }
#endif
        if (!handled && _verifySignatures)
        {
          // next random question
          if (UseSigVer2Full)
          {
            if (rand()%2)
            { // use DataSignatureAsk message
              int index = toIntFloor(GRandGen.RandomValue() * 0x8000);
#if _ENABLE_CHEATS
              static int forceValue = -1;
              if (forceValue>=0)
              {
                index = forceValue;
              }
#endif
              int level = pi.hashes.ActualCheckLevel();
              CheckSignature(pi.dpid, index, level);
            }
            else
            { // use FileSignatureAsk message
              int level = pi.hashes.ActualCheckLevel();
              RString filename = pi.hashes.RandomUsedFile(RandomPlayerPos(), level);
#if _ENABLE_CHEATS
              static bool doIt = false;
              if (doIt)
              {
                filename = "ca\\air_d_baf\\aw159_mainrotorblurred_baf.p3d";
                LogF("RandomUsedFile: %s", cc_cast(filename));
              }
#endif
              QFBank *bank = QFBankQueryFunctions::AutoBank(filename);
              if (bank)
              {
                CheckSignature(pi.dpid, filename, bank->GetPrefix(),level);
              }
#if _ENABLE_CHEATS
              else if (!filename.IsEmpty()) 
                LogF("RandomUsedFile test SKIPPED, bank not found! file: %s", cc_cast(filename));
#endif
            }
          }
          else // use SigVer2 only for new clients
          {
            if (rand()%2 || pi.hashes.banksCount<0) // client has NOT sent the count of loaded pbo files => it does not understand SigVer2
            { // use DataSignatureAsk message
              int index = toIntFloor(GRandGen.RandomValue() * 0x8000);
#if _ENABLE_CHEATS
              static int forceValue = -1;
              if (forceValue>=0)
              {
                index = forceValue;
              }
#endif
              int level = pi.hashes.ActualCheckLevel();
              CheckSignature(pi.dpid, index, level);
            }
            else
            { // use FileSignatureAsk message
              int level = pi.hashes.ActualCheckLevel();
              RString filename = pi.hashes.RandomUsedFile(RandomPlayerPos(), level);
              //LogF("RandomUsedFile: %s", cc_cast(filename));
              QFBank *bank = QFBankQueryFunctions::AutoBank(filename);
              if (bank)
              {
                CheckSignature(pi.dpid, filename, bank->GetPrefix(),level);
              }
            }
          }
        }
// The following is only for debugging purposes, define used only not to forget to set more test back to normal in some commit
#define _DEBUG_MORE_SIGNATURE_TESTS (_ENABLE_CHEATS && 0)
#if _DEBUG_MORE_SIGNATURE_TESTS
        // more test when debugging
        pi.regularCheckNext = time + 500 + toInt(1000 * GRandGen.RandomValue()); // 1 s - 3 s
#else
        // Note: timing 80s - 160s is no longer used for regular signature checks, but deep (level=1) checks
        //       regular signature checks are now more frequent, but only shallow (level=0)
        pi.regularCheckNext = time + 5000 + toInt(5000 * GRandGen.RandomValue()); // 5 s - 10 s
#endif
      }

      if (_verifySignatures)
      {
        ReportHackedData reportFunc(pi, *this);
        pi.signatureChecks.CheckTimeouts(reportFunc);
        pi.hashes.CheckTimeouts(reportFunc);
      }
    }
  }

//((EXE_CRC_CHECK
#if !_SUPER_RELEASE
  // Exe modifications investigation
  if (!_exeInvestigation._asked && _exeInvestigation._stack.Size() > 0)
  {
    const ExeSegmentInfo &info = _exeInvestigation._stack[0];
    IntegrityCheck(_exeInvestigation._client1._dpid, IQTExe, IntegrityQuestion("", info.offset, info.size), 3600000);
    IntegrityCheck(_exeInvestigation._client2._dpid, IQTExe, IntegrityQuestion("", info.offset, info.size), 3600000);
    _exeInvestigation._asked = true;
  }
#endif
//))EXE_CRC_CHECK
}

void PendingSignatureChecks::CheckTimeouts(CheckSignatureTimeoutFunc &func)
{
  DWORD time = GlobalTickCount();
  for (int i=0; i<pendingDataChecks.Size(); i++)
  {
    if (time > pendingDataChecks[i].checkTimeout)
    {
      if (func()) // do something the action
        return; 
      pendingDataChecks.Delete(i);
      i--;
    }
  }
  for (int i=0; i<pendingFileChecks.Size(); i++)
  {
    if (time > pendingFileChecks[i].checkTimeout)
    {
      if (func()) // do something the action
        return; 
      pendingFileChecks.Delete(i);
      i--;
    }
  }
}

bool NetworkServer::ProcessHackedData(NetworkPlayerInfo &pi)
{
#if _ENABLE_DEDICATED_SERVER
  ServerMessage(Format(
    LocalizeString(IDS_SIGNATURE_CHECK_TIMED_OUT),
    cc_cast(pi.name)));
#endif
  OnHackedData(HTHackedData, pi.dpid, RString()); // filename unknown
  return false; //continue
}

bool ReportHackedData::operator ()()
{
  // question timed out
  return _server.ProcessHackedData(_pi);
}

void NetworkServer::OnIntegrityCheckFailed
(
  int dpnid, IntegrityQuestionType type, const char *text, bool final
)
{
  #ifndef _XBOX
  NetworkPlayerInfo *info = GetPlayerInfo(dpnid);
  if (!info) return;
  RString format = LocalizeString(IDS_MP_VALIDERROR + type);

  RString message = Format(format, cc_cast(info->name));
  if (text && *text) message = message + Format(" - %s", cc_cast(text));
  if (final)
  {
    RefArray<NetworkObject> dummy;
    GNetworkManager.Chat(CCGlobal, "", dummy, message);
  }
  GChatList.Add(CCGlobal, NULL, message, false, true);
#if _ENABLE_DEDICATED_SERVER
  ConsoleF("%s", cc_cast(message));
#endif
  #endif
}

NetworkPlayerInfo *NetworkServer::GetPlayerInfo(int dpid)
{
  for (int i=0; i<_players.Size(); i++)
  {
    NetworkPlayerInfo &info = _players[i];
    if (info.dpid == dpid)
      return &info;
  }
  return NULL;
}

Time NetworkServer::GetPlayerJoined(int dpid) const
{
  for (int i=0; i<_players.Size(); i++)
  {
    const NetworkPlayerInfo &info = _players[i];
    if (info.dpid == dpid) return info.joined;
  }
  return TIME_MAX;
}

/*!
\patch 1.25 Date 10/01/2001 by Jirka
- Added: Send chat message "Player XXX connecting" when login process is initiated.
*/

NetworkPlayerInfo *NetworkServer::OnPlayerCreate(int dpid, bool privateSlot, const char *name, unsigned long inaddr)
{
//{ DEDICATED SERVER SUPPORT
#if _ENABLE_DEDICATED_SERVER
  bool server = _dedicated && dpid == BOT_CLIENT;
  if (server) name = "__SERVER__";
#else
  bool server = false;
#endif
//}

  char playerName[256];
  strcpy(playerName, name);

  bool ok = false;
  int suffix = 1;
  while (!ok)
  {
    ok = true;
    for (int i=0; i<_players.Size(); i++)
    {
      NetworkPlayerInfo &info = _players[i];
      if (info.dpid == dpid) continue;
      if (stricmp(info.name, playerName) == 0)
      {
        sprintf(playerName, "%s (%d)", name, ++suffix);
        ok = false;
        break;
      }
    }
  }

  {
    NetworkPlayerInfo *info = GetPlayerInfo(dpid);
    if (info)
    {
      DoAssert(strcmp(info->name, playerName) == 0);
      return info;
    }
  }

  int index = _players.Add();
  NetworkPlayerInfo &info = _players[index];
  info.dpid = dpid;
  info.dvid = dpid;
  info.clientState = NCSCreated;
  info.channel = CCNone;  
  info.name = playerName;
  info.cameraPosition = InvalidCamPos;
  info.cameraPositionTime = TIME_MIN;
  info.integrityCheckNext = UINT_MAX;
#if _VBS3
  info.cameraDirection = VForward;
  info.cameraFov = 0.75;
#endif
  info.privateSlot = privateSlot;
  info.connectionProblemsReported = false;
  info.kickedOff = false;
  info.nextQuestionId = 1;
  info._jip = false;
  info._jipProcessed = false;
  //LogF("_jip=false dpnid=%d setting in %s on line %d\n", dpid, __FILE__, __LINE__);
  info.joined = TIME_MAX;
  info._publicAddr = 0;
  info._privateAddr = 0;
  info._voicePort = 0;

  info.regularCheckNext = GlobalTickCount() + 40000 + toInt(40000 * GRandGen.RandomValue()); // initial delay 40 s - 80 s
  info.regularCheckOrder = 1;

  info._loginFailedCount = 0;
  info._loginDisabledUntil = 0;

#if LOG_PLAYERS
LogF
(
  "Server: Player info added - name %s, id %d (total %d identities, %d players)",
  (const char *)playerName, dpid, _identities.Size(), _players.Size()
);
#endif

//{ DEDICATED SERVER SUPPORT
#if _ENABLE_DEDICATED_SERVER
  if (_dedicated)
  {
    info.motdIndex = -1;
    if (dpid != BOT_CLIENT && _motd.Size() > 0) info.motdIndex = 0;
  }
#endif
//}

  //info.integrityQuestion = 0;
  /*
  for (int t=0; t<IntegrityQuestionTypeCount; t++)
  {
    info.integrityQuestionTimeout[t] = UINT_MAX;
    info.integrityInvestigation[t] = NULL;
  }
  */

  // register formats
  for (int mt=NMTFirstVariant; mt<NMTN; mt++)
  {
    NetworkMessageFormatBase *item = GetFormat(mt);

    // create message
    NetworkMessageType type = item->GetNMType(NMCCreate);
    Ref<NetworkMessage> msg = ::CreateNetworkMessage(type);
    msg->time = Glob.time;
    NetworkMessageContext ctx(msg, this, dpid, MSG_SEND);

    TMError err;

    NetworkMessageMsgFormat *message = (NetworkMessageMsgFormat *)msg.GetRef();
    message->_index = mt;

    err = item->TransferMsg(ctx);
    if (err != TMOK) continue;

    // send message
    NetworkComponent::SendMsg(dpid, msg, type, NMFGuaranteed);
  }

  // enable sending messages
  SetClientState(dpid, NCSConnected);

  if (_verifySignatures && dpid != BOT_CLIENT)
  {
    // send accepted keys
    for (int i=0; i<_acceptedKeys.Size(); i++)
    {
      AcceptedKeyMessage key(_acceptedKeys[i]);
      SendMsg(dpid, &key, NMFGuaranteed);
    }
    // send additional signed files
    AdditionalSignedFilesMessage files(_additionalSignedFiles);
    SendMsg(dpid, &files, NMFGuaranteed);
    // TODO: mark required signatures
  }

  PlayerMessage msg;
  msg._player = dpid;
  msg._name = info.name;
  msg._server = server;
  msg._vonDisabled = _parent->IsVoNDisabled();
  msg._vonCodecQuality = _parent->VoNCodecQuality();  
#if !_USE_BATTL_EYE_SERVER
  msg._battlEye = false;
#else
  msg._battlEye = _beIntegration.IsEnabled();
#endif
  msg._sigVerRequired = _sigVerRequired;
  msg._steamServerId = 0;
  msg._steamSecure = false;
#if _ENABLE_STEAM
  if (UseSteam && SteamGameServer())
  {
    msg._steamServerId = SteamGameServer()->GetSteamID().ConvertToUint64();
    msg._steamSecure = SteamGameServer()->BSecure();
  }
#endif
  //  msg._inaddr = inaddr;
  SendMsg(dpid, &msg, NMFGuaranteed);

  // Send chat message about player is connecting
#ifndef _XBOX
  if (dpid != BOT_CLIENT)
  {
    //char message[256];
    RefArray<NetworkObject> dummy;
    LocalizedChatMessage chat;
    chat._channel = CCGlobal;
    // format
    chat._args.Add(LocalizedString(LocalizedString::Stringtable,"STR_MP_CONNECTING"));
    // arguments
    chat._args.Add(LocalizedString(LocalizedString::PlainText,info.name));
    for (int i=0; i<_players.Size(); i++)
    {
      if (_players[i].clientState >= NCSConnected)
        SendMsg(_players[i].dpid, &chat, NMFGuaranteed);
    }
  }
#endif

  // send player int and name to himself
//{ DEDICATED SERVER SUPPORT
#if _ENABLE_DEDICATED_SERVER
  if (_dedicated)
  {
    if (server)
    {
      ConsoleF(LocalizeString(IDS_DS_SERVER_IDENTITY_CREATED));
    }
    else
    {
      ConsoleF(LocalizeString(IDS_DS_PLAYER_CONNECTING), (const char *)info.name);
    }
  }
#endif
//}

  // set the starting number of private and public slots on DS

  //{ DEDICATED SERVER SUPPORT
#if _ENABLE_DEDICATED_SERVER
  if (IsDedicatedServer())
  {
#if _GAMES_FOR_WINDOWS || defined _XBOX
    // update of max. number of players
    int perClient = 32000;
    int round = 1;
    int maxPlayers = GetMaxPlayersSafe(perClient, round);
    int GetDSPrivateSlots();
    int maxPrivate = GetDSPrivateSlots();
    saturateMin(maxPrivate, maxPlayers);
    UpdateMaxPlayers(maxPlayers, maxPrivate);
#else
    UpdateMaxPlayers(GetMaxPlayers(), 0);
#endif
  }
#endif
  //}

  return &info;
}

void NetworkServer::UpdateAdminState()
{
//{ DEDICATED SERVER SUPPORT
#if _ENABLE_DEDICATED_SERVER
  // send also info about new admin and gameMaster state to all clients
  // clients need to know whether there is admin on server
  // prepare message
  ServerAdminMessage samsg;
  samsg._admin = _admin;
  samsg._gameMaster = _gameMaster;
  // update all players and send them fresh admin message
  for (int i=0; i<_identities.Size(); i++)
  {
    PlayerIdentity &pi = _identities[i];
    pi._rights &= ~(PRAdmin|PRVotedAdmin);
    if (pi.dpnid==_gameMaster)
    {
      pi._rights |= _admin ? PRVotedAdmin : PRAdmin;
    }
    SendMsg(pi.dpnid, &samsg, NMFGuaranteed);
  }
  DWORD updatePingTime = GlobalTickCount() + 1000;
  if (_pingUpdateNext>updatePingTime) _pingUpdateNext = updatePingTime;
#endif
//}
}

void NetworkServer::OnPlayerDestroy(int dpid)
{
  if (dpid == BOT_CLIENT)
  {
    Fail("Not implemented.");
    // new bot client must be selected
    return;
  }

#if _ENABLE_GAMESPY && _VERIFY_CLIENT_KEY
  gcd_disconnect_user(GetGameID(), dpid);
#endif

//{ DEDICATED SERVER SUPPORT
#if _ENABLE_DEDICATED_SERVER
  RString name;
  if (dpid == _gameMaster)
  {
    for (int i=0; i<_players.Size(); i++)
      if (_players[i].dpid == _gameMaster)
      {
        name = _players[i].name;
        break;
      }
  }
#endif
//}

  NetworkPlayerInfo *info = GetPlayerInfo(dpid);
  NetworkId playerPerson;
  if (info)
  {
    LocalizedFormatedString message;
    message.SetFormat(LocalizedString::Stringtable,"STR_MP_DISCONNECT");
    message.AddArg(LocalizedString::PlainText,info->name);
    RefArray<NetworkObject> dummy;
    GNetworkManager.LocalizedChat(CCGlobal, "", dummy, message);
    GChatList.Add(CCGlobal, NULL, message.GetLocalizedValue(), false, true);

    if (info->clientState == NCSBriefingRead && !GNetworkPlayerDisconnected.GetNil())
    {
      GameState *state = GWorld->GetGameState();
      if (state)
      {
        GameVarSpace vars(false);
        state->BeginContext(&vars);
        state->VarSetLocal("_id", (float)info->dpid, true);
        PlayerIdentity *identity = FindIdentity(info->dpid);
        RString uid = identity ? identity->id : RString();
        state->VarSetLocal("_uid", uid, true);
        state->VarSetLocal("_name", info->name, true);
#if USE_PRECOMPILATION
        if (GNetworkPlayerDisconnected.GetType() == GameCode)
        {
          GameDataCode *code = static_cast<GameDataCode *>(GNetworkPlayerDisconnected.GetData());
          if (code->IsCompiled() && code->GetCode().Size() > 0)
            state->Evaluate(code->GetString(), code->GetCode(), GameState::EvalContext::_default, GWorld->GetMissionNamespace()); // mission namespace
        }
        else
#endif
        if (GNetworkPlayerDisconnected.GetType() == GameString)
        {
          // make sure string is not destructed while being evaluated
          RString code = GNetworkPlayerDisconnected;
          if (code.GetLength() > 0)
            state->EvaluateMultiple(code, GameState::EvalContext::_default, GWorld->GetMissionNamespace()); // mission namespace
        }
        state->EndContext();
      }
    }

    playerPerson = info->person;

//{ DEDICATED SERVER SUPPORT
#if _ENABLE_DEDICATED_SERVER
    if (_dedicated) ConsoleF(LocalizeString(IDS_DS_PLAYER_DISCONNECTED), (const char *)info->name);
#endif
//}

    // delete player info
    for (int i=0; i<_players.Size(); i++)
    {
      NetworkPlayerInfo &info = _players[i];
      if (info.dpid == dpid)
      {
#if LOG_PLAYERS
LogF
(
  "Server: Player info removed - name %s, id %d (total %d identities, %d players)",
  (const char *)info.name, dpid, _identities.Size(), _players.Size() - 1
);
#endif
        _players.Delete(i);
        break;
      }
    }
    if (_players.Size() == 0)
    {
      _nextPlayerId = 2; // value 1 is reserved for static objects
      _votings.Clear();
    }
//{ DEDICATED SERVER SUPPORT
#if _ENABLE_DEDICATED_SERVER
    else if (_dedicated && _players.Size() == 1)
    {
      _votings.Clear();
    }
#endif
//}
  }

#if _USE_BATTL_EYE_SERVER
  const PlayerIdentity *identity = FindIdentity(dpid);
  if (identity && identity->clientState == NCSBriefingRead) _beIntegration.RemovePlayer(dpid);
#endif

#if _ENABLE_STEAM
  if (UseSteam)
  {
    const PlayerIdentity *identity = FindIdentity(dpid);
    if (SteamGameServer() && identity)
    {
      SteamGameServer()->SendUserDisconnect(CSteamID((uint64)identity->_steamID));
    }
  }
#endif

  DisconnectPlayer(dpid, playerPerson);

  #if _ENABLE_DEDICATED_SERVER
    if (_dedicated)
    {
      _votings.OnPlayerDeleted(dpid);
    }
  #endif


  for (int i=0; i<_playerRoles.Size(); i++)
  {
    PlayerRole &role = _playerRoles[i];
    if (role.player == dpid)
    {
      role.player = AI_PLAYER;
      role.SetFlag(PRFLocked, false);

      // create message
      NetworkMessageType type = role.GetNMType(NMCCreate);
      Ref<NetworkMessage> msg = ::CreateNetworkMessage(type);
      msg->time = Glob.time;
      NetworkMessageContext ctx(msg, this, TO_SERVER, MSG_SEND);

      PREPARE_TRANSFER(PlayerRole)

      TMError err = TRANSF_BASE(index, i);
      if (err != TMOK) break;
      err = role.TransferMsg(ctx);
      if (err != TMOK) break;
      
      // send message
      for (int i=0; i<_players.Size(); i++)
      {
        NetworkPlayerInfo &player = _players[i];
        if (player.clientState < NCSConnected) continue;
        NetworkComponent::SendMsg(player.dpid, msg, type, NMFGuaranteed);
      }

      break;
    }
  }
  LogoutMessage logout;
  logout._dpnid = dpid;
  for (int i=0; i<_identities.Size(); i++)
  {
    if (_identities[i].dpnid == dpid)
    {
#if _ENABLE_DEDICATED_SERVER
      OnEvent(SEOnUserDisconnected, _identities[i].playerid);
#endif

      SquadIdentity *squad = _identities[i].squad;
#if LOG_PLAYERS
LogF
(
  "Server: Identity removed - name %s, id %d (total %d identities, %d players)",
  (const char *)_identities[i].name, dpid, _identities.Size() - 1, _players.Size()
);
#endif
      _identities.Delete(i);
      // check if player info was deleted properly
      NetworkPlayerInfo *check = GetPlayerInfo(dpid);
      if (check)
      {
        RptF
        (
          "Server error: identity deleted, player info still exist (%s, id %d)",
          (const char *)check->name, dpid
        );
      }

      if (squad)
      {
        // check if squad is used
        bool found = false;
        for (int j=0; j<_identities.Size(); j++)
        {
          if (_identities[j].squad == squad)
          {
            found = true;
            break;
          }
        }
        // if not, delete it
        if (!found) for (int j=0; j<_squads.Size(); j++)
        {
          if (_squads[j] == squad)
          {
            _squads.Delete(j);
            break;
          }
        }
      }
      _votings.Check(this);
      break;
    }
  }
  for (int i=0; i<_identities.Size(); i++)
  {
    PlayerIdentity &player = _identities[i];
    SendMsg(player.dpnid, &logout, NMFGuaranteed);
  }

//{ QUERY & REPORTING SDK
#if _ENABLE_DEDICATED_SERVER
  if (IsDedicatedServer())
  {
    NetworkClient *client = _parent->GetClient();
    DoAssert(client && client->GetPlayer() != logout._dpnid);
    // send new identity to bot client
    SendMsg(client->GetPlayer(), &logout, NMFGuaranteed);
  }
#endif
//}

  UpdateMatchmakingDescription(false);

//{ DEDICATED SERVER SUPPORT
#if _ENABLE_DEDICATED_SERVER
  if (_dedicated)
  {
    if (dpid == _gameMaster)
    {
      ConsoleF(LocalizeString(IDS_DS_ADMIN_LOGGED_OUT), (const char *)name);

      _gameMaster = AI_PLAYER;
      _sessionLocked = false;
      _debugOn.Clear();

      // on Xbox admin disconnect means nothing
      #ifndef _XBOX
        if (_serverState == NSSSelectingMission && _mission.GetLength() == 1 && _mission[0] == '?')
        {
          // enable other players vote mission or continue with mission defined in config
          _mission = "";
          _restart = false;
          _reassign = false;
        }
      #endif
      UpdateAdminState();

#if _XBOX_SECURE && _ENABLE_MP
      if (_identities.Size() > 0) SetGameMaster(_identities[0].dpnid, true);
#endif
    }
  }
#endif
//}

  if (_serverState == NSSSelectingMission)
  {
    // new clients should get mission voting echo as necessary, so that they can see voting
    MissionVotingEcho();
  }
#if _ENABLE_MP
  // update remote mute lists
  for (int i=0; i<_identities.Size(); i++)
  {
    SendMuteList(_identities[i]);
  }
#endif

#if USE_NAT_NEGOTIATION_MANAGER && _ENABLE_MP
  // this player should not start any negotiation or occupy any storage in PeerToPeerManager
  _p2pManager.DestroyPlayer(dpid);
#endif
}

void NetworkServer::SendPlayerRolesMessages()
{
  for (int i=0; i<_playerRoles.Size(); i++)
  {
    PlayerRole &role = _playerRoles[i];

    // create message
    NetworkMessageType type = role.GetNMType(NMCCreate);
    Ref<NetworkMessage> msg = ::CreateNetworkMessage(type);
    msg->time = Glob.time;
    NetworkMessageContext ctx(msg, this, TO_SERVER, MSG_SEND);

    PREPARE_TRANSFER(PlayerRole)

    TMError err = TRANSF_BASE(index, i);
    if (err != TMOK) break;
    err = role.TransferMsg(ctx);
    if (err != TMOK) break;

    // send message
    for (int j=0; j<_players.Size(); j++)
    {
      NetworkPlayerInfo &player = _players[j];
      if (player.clientState < NCSConnected) continue;
      NetworkComponent::SendMsg(player.dpid, msg, type, NMFGuaranteed);
    }
  }
}

void NetworkServer::AssignRolesAfterLoad()
{
  for (int j=0; j<_players.Size(); j++)
  {
    NetworkPlayerInfo *info = &_players[j];
    if (info)
    { 
      int free = SelectRole(info->name, info->dpid);
      if (free >= 0) 
      {
        // assign
        PlayerRole &role = _playerRoles[free];
        role.player = info->dpid;
      }
    }
  }
}

void NetworkServer::SetEstimatedEndTime(Time time)
{
  _missionHeader._estimatedEndTime = time;
  _missionHeader._updateOnly = true;
  _serverTime = ::GetTickCount() - _serverTimeInitTickCount;
  _secondsToEstimatedEnd = time.toInt() - Glob.time.toInt();
  _serverTimeOfEstimatedEnd = _serverTime + _secondsToEstimatedEnd;
  _nextServerTimeUpdate = _serverTime; //update endTime (and serverTime) to clients as soon as possible

  // send mission description and roles
  for (int i=0; i<_players.Size(); i++)
  {
    NetworkPlayerInfo &info = _players[i];
    if (info.clientState < NCSConnected) continue;
    SendMsg(info.dpid, &_missionHeader, NMFGuaranteed);
  }
}

NetworkObjectInfo *NetworkServer::GetObjectInfo(const NetworkId &id) const
{
  for (int i=0; i<_objects.Size(); i++)
  {
    NetworkObjectInfo &info = *_objects[i];
    if (info.id == id)
      return &info;
  }
  return NULL;
}

int NetworkServer::GetOwner(const NetworkId &id) const
{
  const NetworkObjectInfo *info = GetObjectInfo(id);
  if (!info) return 0;

  const PlayerIdentity *identity = FindIdentity(info->owner);
  if (!identity) return 1;

  return identity->playerid;
}

NetworkObjectInfo *NetworkServer::OnObjectCreate(const NetworkId &id, int owner, NetworkMessage *msg, NetworkMessageType type)
{
  {
    NetworkObjectInfo *info = GetObjectInfo(id);
    if (info)
    {
      info->owner = owner;
      return info;
    }
  }

  int index = _objects.Add();
  _objects[index] = new NetworkObjectInfo;
  NetworkObjectInfo &info = *_objects[index];
  info.id = id;
  info.owner = owner;
  info.create.from = owner;
  info.create.message = msg;
  info.create.type = type;
  info.pendingOwner = NO_CLIENT; //no pending
  return &info;
}

/*!
\patch_internal 1.28 Date 10/29/2001 by Jirka
- Fixed: update objects only by messages by object's owner
*/
NetworkObjectInfo *NetworkServer::OnObjectUpdate(NetworkId &id, int from, NetworkMessage *msg, NetworkMessageType type, NetworkMessageClass cls)
{
  NetworkObjectInfo *oInfo = GetObjectInfo(id);
  if (!oInfo)
  {
    RptFObjectNotFound(id.creator, id.id, type);
    return NULL;
  }

  if (oInfo->owner != from)
  {
    RptF("Server: Update of object %d:%d arrived from nonowner", id.creator, id.id);
    return NULL;
  }

  NetworkCurrentInfo &info = oInfo->current[cls];

  NetworkMessageFormatBase *format = GetFormat(/*from, */type);
  if (!format)
  {
    RptF("Server: Bad message %d(%s) format", (int)type, cc_cast(GetMsgTypeName(type)));
    return NULL;
  }
  
  if (info.message && info.message->time > msg->time)
  {
    // do not update
    if (DiagLevel >= 1)
      DiagLogF("Server: update message is old (%.3f s)", info.message->time - msg->time);
    return NULL;
  }

  info.from = from;
  info.type = type;
  info.message = msg;

  // invalidate cached error
  for (int i=0; i<oInfo->playerObjects.Size(); i++)
  {
    NetworkPlayerObjectInfo *poInfo = oInfo->playerObjects[i];
    poInfo->updates[cls].errorValid = false;

  }

  if (DiagLevel >= 4)
    DiagLogF("Server: object %d:%d updated", id.creator, id.id);

  return oInfo;
}

int NetworkServer::PerformObjectDestroy(const NetworkId &id)
{
  //DoAssert(CheckIntegrityOfPendingMessages());
  int owner = 0;
  for (int i=0; i<_objects.Size(); i++)
  {
    NetworkObjectInfo *info = _objects[i];
    if (info->id == id)
    {
      // check all pending messages
      for (int j=0; j<_pendingMessages.Size(); j++)
      {
        NetPendingMessage &pend = _pendingMessages[j];
        if (pend.info==info)
        {
          LogF("Deleted pending message %x for object %x",pend.msgID,pend.info);
          _pendingMessages.Delete(j);
          j--;
        }
      }
      owner = info->owner;
      //LogF("Performing Object Destroy for (%d:%d)", _objects[i]->id.creator, _objects[i]->id.id);
      _objects.Delete(i);
      //DoAssert(CheckIntegrityOfPendingMessages());
      break;
    }
  }
  return owner;
}

void NetworkServer::OnObjectDestroy(const NetworkId &id)
{
  int owner = PerformObjectDestroy(id);
  DeleteObjectMessage msg;
  msg._creator = id.creator;
  msg._id = id.id;
  
  for (int i=0; i<_players.Size(); i++)
  {
    NetworkPlayerInfo &pInfo = _players[i];
    if (pInfo.clientState < NCSMissionReceived) continue;
    if (pInfo.dpid == owner) continue;
    SendMsg(pInfo.dpid, &msg, NMFGuaranteed);
  }
}

int NetworkServer::UpdateObject(NetworkPlayerInfo *pInfo, NetworkObjectInfo *oInfo, NetworkMessageClass cls, NetMsgFlags dwFlags)
{
  if (!oInfo)
  {
    // TODO: BUG
    DiagLogF("Server: Object not found");
    return -1;
  }

  NetworkCurrentInfo &info = oInfo->current[cls];
  if (!info.message) return -1;

  NetworkMessageType type = info.type;
  int to = pInfo->dpid;

  DWORD dwMsgId = NetworkComponent::SendMsg(to, info.message, type, dwFlags);
  if (dwMsgId == 0xFFFFFFFF) return -1;

  // update object info
  NetworkPlayerObjectInfo *poInfo = oInfo->CreatePlayerObjectInfo(to);
  if (dwMsgId == 0)
  {
    // sent as sync message
    if(poInfo->updates[cls].lastCreatedMsg)
    {
      // some update is pending, but we want to send guaranteed update
      // remove the pending one
      DoAssert(dwFlags&NMFGuaranteed);
      //RptF("Guaranteed update superseded pending non-guaranteed update");
      int index = FindPendingMessage(&poInfo->updates[cls]);
      DoAssert (index>=0);
      _pendingMessages.Delete(index);

      // no more messages to the update may exist now
      DoAssert( FindPendingMessage(&poInfo->updates[cls])<0 );

      poInfo->updates[cls].lastCreatedMsg = NULL;
      poInfo->updates[cls].lastCreatedMsgId = 0xffffffff;
      poInfo->updates[cls].lastCreatedMsgTime = 0;

      //DoAssert(CheckIntegrityOfPendingMessages());
    }

    DoAssert(poInfo->updates[cls].lastCreatedMsg==NULL);
    DoAssert(poInfo->updates[cls].lastCreatedMsgId == 0xffffffff);

    poInfo->updates[cls].lastCreatedMsgId = 0xFFFFFFFF;
    poInfo->updates[cls].lastCreatedMsgTime = 0;
    poInfo->updates[cls].lastSentMsg = info.message;
    poInfo->updates[cls].errorValid = false;
  }
  else
  {
    DoAssert(dwMsgId == 1);
    // !!! Pointer to static structure - must be process before structure changed
    info.message->objectUpdateInfo = &poInfo->updates[cls];
    info.message->objectServerInfo = oInfo;
    info.message->objectPlayerInfo = poInfo;

    // we want to set lastCreatedMsg
    // if it was already set, we have a conflict
    DoAssert(poInfo->updates[cls].lastCreatedMsg==NULL);
    DoAssert(poInfo->updates[cls].lastCreatedMsgId == 0xffffffff);

    poInfo->updates[cls].lastCreatedMsg = info.message;
    poInfo->updates[cls].lastCreatedMsgId = MSGID_REPLACE;
    poInfo->updates[cls].lastCreatedMsgTime = ::GlobalTickCount();
    poInfo->updates[cls].canCancel = (dwFlags & NMFGuaranteed) == 0;

#if LOG_SEND_PROCESS1
    LogF("Server: Update info %x marked for send", &poInfo->updates[cls]);
#endif
  }
  return info.message->size;
}

//! Simplified enumeration of array items
#define FOR_EACH(type,var,array) \
  for (type *var = array.Data(), *end = array.Data() + array.Size(); var < end; var++)

void NetworkServer::DestroyAllObjects()
{
  _objects.Clear();
  _pendingMessages.Clear();
  FOR_EACH(NetworkPlayerInfo, player, _players)
  {
    player->person = NetworkId::Null();
    player->unit = NetworkId::Null();
    player->group = NetworkId::Null();
    player->_delayedMessages.Clear();
  }
  _mapPersonUnit.Clear();
//CheckInitMessages(_initMessages, "before _initMessages.Clear()");
  if ( _initMessages.Size() )
  {
    // log GetAddInitAndRemoveOverridden statisticks into the mpStatistics.log file
    LogMPStatistics( GetAddInitAndRemoveOverriddenStat() );
  }
  _initMessages.Clear();
//CheckInitMessages(_initMessages, "after _initMessages.Clear()");

  // avoid id duplicity at next missions
  // _nextId = 0;
}

bool NetworkServer::CheckIntegrity() const
{
  bool ret = true;
  if (!CheckIntegrityOfPendingMessages()) ret = false;
  return ret;
}

bool NetworkServer::CheckIntegrityOfPendingMessages() const
{
  // check if each pending message is using correct message ID
  bool ret = true;
  for (int p=0; p<_pendingMessages.Size(); p++)
  {
    const NetPendingMessage &pend = _pendingMessages[p];
    const NetworkObjectInfo *oInfo = pend.info;
    const NetworkPlayerObjectInfo *poInfo = pend.player;
    const NetworkUpdateInfo *uInfo = pend.update;
    // verify update is in player
    if (uInfo<poInfo->updates && uInfo>=poInfo->updates+NMCUpdateN)
    {
      RptF("Pending message %x - wrong update pointer",pend.msgID);
      ret = false;
      continue;
    }
    // verify player exists in given object
    bool found = false;
    for (int i=0; i<oInfo->playerObjects.Size(); i++)
    {
      if (oInfo->playerObjects[i]==poInfo) found = true;
    }
    if (!found)
    {
      RptF("Pending message %x not found in playerObjects",pend.msgID);
      ret = false;
      continue;
    }
    if(uInfo->lastCreatedMsgId!=pend.msgID)
    {
      RptF("Pending message invalid ID %x!=%x",uInfo->lastCreatedMsgId,pend.msgID);
      ret = false;
      continue;
    }
  }
  // check if each message that is created but not sent
  // is recorded in pending messages
  for (int o=0; o<_objects.Size(); o++)
  {
    const NetworkObjectInfo *oInfo = _objects[o];
    for (int i=0; i<oInfo->playerObjects.Size(); i++)
    {
      NetworkPlayerObjectInfo &poInfo = *oInfo->playerObjects[i];
      for (int j=NMCUpdateFirst; j<NMCUpdateN; j++)
      {
        const NetworkUpdateInfo &info = poInfo.updates[j];
        if (info.lastCreatedMsg && !info.lastSentMsg)
        {
          // message is created but not sent,
          // it is pending
          // check if we can find it in the pending list
          if (info.lastCreatedMsgId==0)
          {
            // message not created, creation in progress
          }
          else if (FindPendingMessage(info.lastCreatedMsgId)<0)
          {
            RptF("Pending message %x not found",info.lastCreatedMsgId);
            RptF("  info.lastCreatedMsg: %x",info.lastCreatedMsg.GetRef());
            ret = false;
          }
        }
      }
    }
  }

  return ret;
}

NetworkMessageFormatBase *NetworkServer::GetFormat(/*int client, */int type)
{
  Assert(type >= 0);
  if (type >= NMTN) return NULL; // unknown message
  return GMsgFormats[type];
}

const NetworkMessageFormatBase *NetworkServer::GetFormat(/*int client, */int type) const
{
  Assert(type >= 0);
  if (type >= NMTN) return NULL; // unknown message
  return GMsgFormats[type];
}

void NetworkServer::CreateMission(RString mission, RString world)
{
  _originalName = mission;
#if _ENABLE_UNSIGNED_MISSIONS
  if (mission.GetLength() > 0)
  {
#if _AAR
    RString filename = RString("mpmissions\\") + mission;
    if(mission.Find("__AAR") == -1)
      filename = filename + RString(".") + world; 
#else
    RString filename = RString("mpmissions\\") + mission + RString(".") + world;
#endif
    CreateMPMissionBank(filename, world);
  }
  else
  {
    GFileBanks.Remove("mpmissions\\__cur_mp.");
  }
#endif // _ENABLE_UNSIGNED_MISSIONS
}

/// detect client-side localization, if possible use it
LocalizedString ClientSideLocalizedString(RString value, RString mission)
{
  // if possible, localize client-side
  RString table;
  RString name = TryLocalize(value,table);
  if (!strcmpi(table,"global"))
  {
    // if localized using global table, client can localize it
    return LocalizedString(LocalizedString::Stringtable,(const char *)value+1);
  }
  else
  {
    if (value[0]=='@')
    {
      RptF(
        "%s: string %s cannot be localized client-side - move to global stringtable",
        cc_cast(mission),cc_cast(value)
      );
    }
    return LocalizedString(LocalizedString::PlainText,name);
  }
}


//! Text representation of RespawnMode enum values
const EnumName RespawnNames[]=
{
  EnumName(RespawnNone,"NONE"),
  EnumName(RespawnSeaGull,"BIRD"),
  EnumName(RespawnAtPlace,"INSTANT"),
  EnumName(RespawnInBase,"BASE"),
  EnumName(RespawnToGroup,"GROUP"),
  EnumName(RespawnToFriendly,"SIDE"),
  EnumName()
};

//! Map RespawnMode enum values to its text representation
template<>
const EnumName *GetEnumNames(RespawnMode dummy) {return RespawnNames;}

#define DIFF_SET(MessageName,name,ids,XX) _missionHeader._diff##name = item.flags[DT##name];

class AddTurretRoles : public ITurretTypeFunc
{
protected:
  AutoArray<PlayerRole> &_roles; // out
  PlayerRole &_role; // in - out
  int &_added; // out
  bool _gunner;
  bool _observer;
  bool _leader;
  const TurretType *_commandingTT;

public:
  AddTurretRoles(AutoArray<PlayerRole> &roles, PlayerRole &role, int &added, bool gunner, bool observer, bool leader, const TurretType *commandingTT)
    : _roles(roles), _role(role), _added(added), _gunner(gunner), _observer(observer), _leader(leader), _commandingTT(commandingTT) {}

  virtual bool operator () (const EntityAIType *entityType, const TurretType &turretType, TurretPath &path) const
  {
    if (!turretType._hasGunner) return false; // continue
    if (turretType._weapons._weapons.Size() > 0 && !turretType._primaryObserver)
    {
      // gunner
      if (!_gunner) return false; // continue
    }
    else
    {
      // observer
      if (!_observer) return false; // continue
    }
    int n = path.Size();
    _role.turret.Realloc(n);
    _role.turret.Resize(n);
    for (int i=0; i<n; i++) _role.turret[i] = path[i];
    _role.leader = &turretType==_commandingTT ? _leader : false;
    _roles.Add(_role);
    _added++;
    return false; // continue
  }
};

struct FindMaxCommanderContext
{
  const TurretType *commandingTT;
  float maxCommanding;
  FindMaxCommanderContext() : commandingTT(NULL), maxCommanding(0) {}
};

/// Functor finds the turret which should contain the commander
class FindMaxCommander : public ITurretTypeFunc
{
  FindMaxCommanderContext &_ctx;
public:
  const TurretType *commandingTT;
  FindMaxCommander(FindMaxCommanderContext &ctx) : _ctx(ctx) {}
  virtual bool operator () (const EntityAIType *entityType, const TurretType &turretType, TurretPath &path) const
  {
    if (turretType._commanding > _ctx.maxCommanding) 
    {
      _ctx.commandingTT = &turretType;
      _ctx.maxCommanding = turretType._commanding;
    }
    return false; //continue
  }
};

const TurretType *FindCommandingTT(EntityAIType *aiType)
{
  if (aiType)
  {
    // find the commander turret
    FindMaxCommanderContext ctx;
    FindMaxCommander func(ctx);
    aiType->ForEachTurret(func);
    return ctx.commandingTT;
  }
  return NULL;
}

RString GetRolesSaveName(SaveGameType type)
{
  RStringB GetExtensionSave();
  RString extstr("roles.");
  switch (type)
  {
  case SGTAutosave:
    return RString("autosave.") + extstr + GetExtensionSave();
  case SGTUsersave:
    return RString("save.") + extstr + GetExtensionSave();
  case SGTContinue:
    return RString("continue.") + extstr + GetExtensionSave();
  case SGTUsersave2:
    return RString("save2.") + extstr + GetExtensionSave();
  case SGTUsersave3:
    return RString("save3.") + extstr + GetExtensionSave();
  case SGTUsersave4:
    return RString("save4.") + extstr + GetExtensionSave();
  case SGTUsersave5:
    return RString("save5.") + extstr + GetExtensionSave();
  case SGTUsersave6:
    return RString("save6.") + extstr + GetExtensionSave();
  }
  Fail("Save game type");
  return RString();
}

bool NetworkServer::LoadRolesFromSave()
{
  SaveGameType saveType = (SaveGameType)GetNetworkManager().GetCurrentSaveType();
  RString fileName = GetRolesSaveName(saveType);

  GameDataNamespace globals(NULL, RString(), false); // save - no global namespace

#if defined _XBOX && _XBOX_VER >= 200
  XSaveGame save = NULL;
  bool needSave = false; //if we need to have correct save
  // LUKE - save can be already opened, so we have to check that
  if (!GSaveSystem.IsSomeSaveOpened())
  {
    // save not opened yet
    needSave = true;
    XSaveGame GetSaveGame(bool create, bool noErrorMsg = false);
    save = GetSaveGame(false);
  }
  if (!needSave || save) // Errors already handled
  {
    if (QIFileFunctions::FileExists(RString(SAVE_ROOT) + fileName))
    {
      ParamArchiveLoad ar;
      bool result = ar.LoadSigned(RString(SAVE_ROOT) + fileName, NULL, &globals);
      if (!result) return false;
      ar.Serialize("PlayerRoles", _playerRoles, 1);
      ar.Serialize("PlayerInfo", _role2name, 1);
      return true;
    }
  }
#else
  RString dir = GetSaveDirectory();
  if (dir.GetLength() > 0)
  {
    RString arPath = dir + fileName;
    if (QIFileFunctions::FileExists(arPath))
    {
      ParamArchiveLoad ar;
      bool result = ar.LoadSigned(arPath, NULL, &globals);
      if (!result) return false;
      ar.Serialize("PlayerRoles", _playerRoles, 1);
      ar.Serialize("PlayerInfo", _role2name, 1);
      return true;
    }
  }
#endif
  return false;
}

void NetworkServer::SaveRolesToSave(SaveGameType saveType)
{
  RString fileName = GetRolesSaveName(saveType);
  // refresh the name to roleIndex mapping
  _role2name.Resize(0);
  for (int i=0; i<_players.Size(); i++)
  {
    int dpid = _players[i].dpid;
    for (int j=0; j<_playerRoles.Size(); j++)
    {
      if (_playerRoles[j].player == dpid)
      {
        _role2name.Add(RoleIndex2PlayerName(_players[i].name,j));
        break;
      }
    }
  }
#if defined _XBOX && _XBOX_VER >= 200
  XSaveGame save = NULL;
  bool needSave = false; //if we need to have correct save
  // LUKE - save can be already opened, so we have to check that
  if (!GSaveSystem.IsSomeSaveOpened())
  {
    // no save opened yet
    needSave = true;
    XSaveGame GetSaveGame(bool create, bool noErrorMsg = false);
    save = GetSaveGame(true);
  }
  if (!needSave || save) // Errors already handled
  {
    ParamArchiveSave ar(RolesSerializeVersion);
    ar.Serialize("PlayerRoles", _playerRoles, 1);
    ar.Serialize("PlayerInfo", _role2name, 1);
    ar.SaveSigned(RString(SAVE_ROOT) + fileName);
  }
#else
  RString dir = GetSaveDirectory();
  if (dir.GetLength() > 0)
  {
    RString arPath = dir + fileName;

    ParamArchiveSave ar(RolesSerializeVersion);
    ar.Serialize("PlayerRoles", _playerRoles, 1);
    ar.Serialize("PlayerInfo", _role2name, 1);
    ar.SaveSigned(arPath);
  }
#endif
}

void NetworkServer::UndefMissionParams()
{
  _param1 = _param2 = FLT_MAX;
  _paramsArray.Clear();
}

void NetworkServer::InitMission(int difficulty, RString displayName, bool createRoles, bool noCopy, RString owner, bool isCampaignMission)
{
  PROFILE_SCOPE_EX(nsIM1, network)
  // unlock session
  _sessionLocked = false;

  _missionHeader._estimatedEndTime = TIME_MIN;
  _missionHeader._enableAIDisable = !isCampaignMission;

  // mission description
  _missionHeader._gameType = RString();
  ConstParamEntryPtr header = ExtParsMission.FindEntry("Header");
  if (header)
  {
    ConstParamEntryPtr gameType = header->FindEntry("gameType");
    if (gameType) _missionHeader._gameType = *gameType;
  }

  _missionHeader._island = Glob.header.worldname;
  if (displayName.GetLength() > 0)
  {
    _missionHeader._name = LocalizedString(LocalizedString::PlainText,displayName);
  }
  else
  {
    _missionHeader._name = ClientSideLocalizedString(CurrentTemplate.intel.briefingName,CurrentTemplate.intel.briefingName);
    if (_missionHeader._name._id.GetLength() == 0)
    {
      _missionHeader._name = LocalizedString(
        LocalizedString::PlainText,
        _originalName.GetLength() > 0 ? _originalName : Glob.header.filename
      );
    }
  }
  
  RString missionName = _missionHeader._name.GetLocalizedValue();
  
  _missionHeader._description = ClientSideLocalizedString(CurrentTemplate.intel.briefingDescription,missionName);

  // ADDED in patch 1.01 - checks of addons
  _missionHeader._addOns = CurrentTemplate.addOns;
  // make sure addon owning the mission is present on clients
  if (owner.GetLength() > 0) _missionHeader._addOns.AddUnique(owner);

  // ADDED in patch 1.01 - disable AI
#if _ENABLE_AI
  _missionHeader._disabledAI = false;
  ConstParamEntryPtr disabledAI = ExtParsMission.FindEntry("disabledAI");
  if (disabledAI) _missionHeader._disabledAI = *disabledAI;
#endif

  // difficulty
  _missionHeader._diffName = Glob.config.diffNames.GetName(difficulty);
  const DifficultySettings &item = Glob.config.diffSettings[difficulty];
  DIFFICULTY_TYPE_ENUM(DIFF_SET, ignored, ignored)

  // mission respawn info
  _missionHeader._respawn = RespawnSeaGull;
  _missionHeader._respawnDelay = 0;
  _missionHeader._respawnVehicleDelay = -1;
  _missionHeader._teamSwitchEnabled = false;
  ConstParamEntryPtr respawn = ExtParsMission.FindEntry("respawn");
  if (respawn)
  {
    // check if it is numeric name
    RStringB respawnMode = *respawn;
    int mode = GetEnumValue<RespawnMode>(respawnMode);
    if (mode < 0)
    {
      mode = *respawn;
    }
    if (mode < 0 || mode > RespawnToFriendly) mode = RespawnNone;
    _missionHeader._respawn = mode;
  }
  respawn = ExtParsMission.FindEntry("respawnDelay");
  if (respawn) _missionHeader._respawnDelay = *respawn;
  respawn = ExtParsMission.FindEntry("respawnVehicleDelay");
  if (respawn) _missionHeader._respawnVehicleDelay = *respawn;
  if (_missionHeader._respawn == RespawnToFriendly)
  {
    respawn = ExtParsMission.FindEntry("enableTeamSwitch");
    if (respawn) _missionHeader._teamSwitchEnabled = *respawn;
    else _missionHeader._teamSwitchEnabled = true; // by default true in this respawn mode
  }

  _missionHeader._aiKills = false;
  ConstParamEntryPtr aiKills = ExtParsMission.FindEntry("aiKills");
  if (aiKills) _missionHeader._aiKills = *aiKills;

  // mission parameters
  ConstParamEntryPtr entry = ExtParsMission.FindEntry("titleParam1");
  _missionHeader._titleParam1 = ClientSideLocalizedString(entry ? entry->operator RString() : RString(""),missionName);
  _missionHeader._valuesParam1.Resize(0);
  _missionHeader._textsParam1.Resize(0);
  entry = ExtParsMission.FindEntry("valuesParam1");
  if (entry)
  {
    int n = entry->GetSize();
    _missionHeader._valuesParam1.Resize(n);
    for (int i=0; i<n; i++) _missionHeader._valuesParam1[i] = (*entry)[i];
  
    ConstParamEntryPtr entryTexts = ExtParsMission.FindEntry("textsParam1");
    if (!entryTexts || entryTexts->GetSize() != n) entryTexts = entry;

    if (entryTexts)
    {
      _missionHeader._textsParam1.Resize(n);
      for (int i=0; i<n; i++)
      {
        _missionHeader._textsParam1[i] = ClientSideLocalizedString((*entryTexts)[i],missionName);
      }
    }
  }
  entry = ExtParsMission.FindEntry("defValueParam1");
  _missionHeader._defValueParam1 = entry ? *entry : 0;
  
  entry = ExtParsMission.FindEntry("titleParam2");
  _missionHeader._titleParam2 = ClientSideLocalizedString(entry ? entry->operator RString() : RString(""),missionName);
  _missionHeader._valuesParam2.Resize(0);
  _missionHeader._textsParam2.Resize(0);
  entry = ExtParsMission.FindEntry("valuesParam2");
  if (entry)
  {
    int n = entry->GetSize();
    _missionHeader._valuesParam2.Resize(n);
    for (int i=0; i<n; i++) _missionHeader._valuesParam2[i] = (*entry)[i];
  
    ConstParamEntryPtr entryTexts = ExtParsMission.FindEntry("textsParam2");
    if (!entryTexts || entryTexts->GetSize() != n) entryTexts = entry;

    if (entryTexts)
    {
      _missionHeader._textsParam2.Resize(n);
      for (int i=0; i<n; i++)
      {
        _missionHeader._textsParam2[i] = ClientSideLocalizedString((*entryTexts)[i],missionName);
      }
    }
  }
  entry = ExtParsMission.FindEntry("defValueParam2");
  _missionHeader._defValueParam2 = entry ? *entry : 0;

  bool setParams = (_paramsArray.Size()==0);
  if (setParams && _param1==FLT_MAX)
  {
    _param1 = _missionHeader._defValueParam1;
    _param2 = _missionHeader._defValueParam2;
  }

  _missionHeader._hasMissionParams = GetMissionParameters().GetLength() > 0;

  _missionHeader._noCopy = noCopy;

  //----------------------------------------------------------------//
  //----------------------------------------------------------------//
  //array of user mission params
  // mission parameters

  _missionHeader._valuesParamArray.Resize(0);
  _missionHeader._textsParamArray.Resize(0);
  _missionHeader._defaultParamArray.Resize(0);
  _missionHeader._titleParamArray.Resize(0);
  _missionHeader._sizesParamArray.Resize(0);
  if (setParams) _paramsArray.Clear();


  //copy param1/2 to new array
  _missionHeader._titleParamArray.Add(_missionHeader._titleParam1);
  _missionHeader._sizesParamArray.Add(0);
  for(int i=0; i<_missionHeader._valuesParam1.Size();i++)
  {
    _missionHeader._valuesParamArray.Add(_missionHeader._valuesParam1[i]);
    _missionHeader._textsParamArray.Add(_missionHeader._textsParam1[i]);
    _missionHeader._sizesParamArray[_missionHeader._sizesParamArray.Size()-1]++;
  } 
  _missionHeader._defaultParamArray.Add(_missionHeader._defValueParam1);
    if (setParams) _paramsArray.Add(_param1);

  _missionHeader._titleParamArray.Add(_missionHeader._titleParam2);
  _missionHeader._sizesParamArray.Add(0);
  for(int i=0; i<_missionHeader._valuesParam2.Size();i++)
  {
    _missionHeader._valuesParamArray.Add(_missionHeader._valuesParam2[i]);
    _missionHeader._textsParamArray.Add(_missionHeader._textsParam2[i]);
    _missionHeader._sizesParamArray[_missionHeader._sizesParamArray.Size()-1]++;
  }
  _missionHeader._defaultParamArray.Add(_missionHeader._defValueParam2);
  if (setParams) _paramsArray.Add(_param2);

  //read secondary parameters
  ConstParamEntryPtr paramsDesc = ExtParsMission.FindEntry("Params"); 
  //for all parameters add CStatic and CListBox to container
  if(paramsDesc) for(int i=0; i< paramsDesc->GetEntryCount();i++ )
  {
    //create listbox content text (parameters names)
    ParamEntryVal parameterDesc = paramsDesc->GetEntry(i);

    ConstParamEntryPtr entry = parameterDesc.FindEntry("title");
    _missionHeader._titleParamArray.Add(ClientSideLocalizedString(entry ? entry->operator RString() : RString(""),missionName));
    _missionHeader._sizesParamArray.Add(0);

    entry = parameterDesc.FindEntry("values");
    if (entry)
    {
      int n = entry->GetSize();

      for (int i=0; i<n; i++) 
      {
        _missionHeader._valuesParamArray.Add((*entry)[i]);
        _missionHeader._sizesParamArray[_missionHeader._sizesParamArray.Size()-1]++;
      }

      ConstParamEntryPtr entryTexts = parameterDesc.FindEntry("texts");
      if (!entryTexts || entryTexts->GetSize() != n) entryTexts = entry;

      if (entryTexts)
      {
        for (int i=0; i<n; i++)
        {
          _missionHeader._textsParamArray.Add(ClientSideLocalizedString((*entryTexts)[i],missionName));
        }
      }
    }

    entry = parameterDesc.FindEntry("default");
    _missionHeader._defaultParamArray.Add(entry ? *entry : 0);
    if (setParams) _paramsArray.Add(entry ? *entry : 0);
  }
  //----------------------------------------------------------------//
  //----------------------------------------------------------------//

  // mission file
  if (noCopy)
  {
    // mission inside addon is ready on the client side (is in the addons list)
    _missionHeader._fileDir = GetBaseDirectory() + GetBaseSubdirectory();
    _missionHeader._fileName = RString(Glob.header.filename) + RString(".") + RString(Glob.header.worldname);
  }
#if _ENABLE_UNSIGNED_MISSIONS
  else
  {
    QFBank *bank = FindBank("MPMissions\\__cur_mp.");
    if (bank)
    {
      // mission in bank
#ifdef _XBOX
      if (_originalName[0] != 0 && _originalName[1] == ':')
      {
        // downloaded mission
        const char *ptr = strrchr(_originalName, '\\');
        DoAssert(ptr);
        _missionHeader._fileDir = RString(_originalName, ptr + 1 - (const char *)_originalName);
        _missionHeader._fileName = ptr + 1;
      }
      else
#endif
      {
        if (_missionHeader._hasMissionParams)
        {
          _missionHeader._fileDir = RString();
          _missionHeader._fileName = _originalName;
        }
        else
        {
          _missionHeader._fileDir = RString("MPMissions\\");
          _missionHeader._fileName = _originalName + RString(".") + _missionHeader._island;
#if _AAR
          if(_missionHeader._fileName.Find("__AAR")  > -1) //cut off the island
            _missionHeader._fileName = _originalName;
#endif
        }
      }
      _missionBank = _missionHeader._fileDir + _missionHeader._fileName + RString(".pbo");
    }
    else
    {
      // char '$' is used as an indication of Local Settings directory (different on client than on server)
      _missionHeader._fileDir = RString("$\\tmp\\");
      _missionHeader._fileName = RString("__cur_mp");

      RString srcDir = GetServerTmpDir() + RString("\\");
      _missionBank = srcDir + _missionHeader._fileName + RString(".pbo"); 

      ::CreateDirectory(srcDir, NULL);

      DoVerify(QIFileFunctions::CleanUpFile(_missionBank));

      FileBankManager mgr;

      RString dir = GetMissionDirectory();
      int i = dir.GetLength() - 1;
      if (i>=0 && dir[i] == '\\') dir = dir.Substring(0, i); // remove trailing '\'
#ifdef _XBOX
      const char *ptr = strrchr(dir, ':');
      if (!ptr) dir = RString("#:\\") + dir;
#endif

#if _VBS2 && _VBS2_LITE
      void VBS2EncryptMission(RString fileName,RString directory);
      VBS2EncryptMission(_missionBank,dir);
#else
      // TODO: do not create temporary __CUR_MP
      mgr.Create(_missionBank, dir);
#endif
    }
  }
#endif // _ENABLE_UNSIGNED_MISSIONS

  {
    QIFStream f;
    f.open(_missionBank);

    CRCCalculator crc;
    crc.Reset();
    f.copy(crc);
    _missionHeader._fileCRC = crc.GetResult();

    _missionHeader._fileSizeH = 0;
    _missionHeader._fileSizeL = f.tellg();
  }

#if _GAMES_FOR_WINDOWS || (defined _XBOX && _XBOX_VER >= 200)
  // set the properties and contexts for matchmaking
  // - MPType
  int gameType = 0;
  if (_missionHeader._gameType.GetLength() > 0)
  {
    ConstParamEntryPtr cls = (Pars >> "CfgMPGameTypes").FindEntry(_missionHeader._gameType);
    if (cls) gameType = *cls >> "id";
  }
  GSaveSystem.SetContext(CONTEXT_GAME_MPTYPE, gameType);
  // - World
  int worldId = 0;
  if (_missionHeader._island.GetLength() > 0)
  {
    ConstParamEntryPtr cls = (Pars >> "CfgWorlds").FindEntry(_missionHeader._island);
    if (cls) worldId = *cls >> "worldId";
  }
  GSaveSystem.SetContext(CONTEXT_GAME_WORLD, worldId);
  // - Difficulty
  GSaveSystem.SetContext(CONTEXT_GAME_DIFFICULTY, difficulty);
  // - MPMission
  int missionId = 0;
  // TODOXNET: mission identification
  GSaveSystem.SetContext(CONTEXT_GAME_MPMISSION, missionId);
#endif

  // ADDED in patch 1.01 - disable AI
  #if _ENABLE_AI
    int flags = _missionHeader._disabledAI ? PRFNone : PRFEnabledAI;
  #else
    int flags = PRFNone;
  #endif

  // Create roles using CurrentTemplate or Load it (when playing Saved game)
  if (GetNetworkManager().GetCurrentSaveType()>=0)
  { // Load it
    LoadRolesFromSave();
  }
  else
  { // use CurrentTemplate
    _role2name.Resize(0);
    if (createRoles)
    {
      // create roles for players
      _playerRoles.Resize(0);
      int east = 0, west = 0, guer = 0, civl = 0;
      PlayerRole role;
      // common attributes
      role.flags = flags;

      for (int i=0; i<CurrentTemplate.groups.Size(); i++)
      {
        ArcadeGroupInfo &gInfo = CurrentTemplate.groups[i];
        TargetSide side = gInfo.side;
        int grp;
        switch (side)
        {
        case TEast:
          grp = east++; break;
        case TWest:
          grp = west++; break;
        case TGuerrila:
          grp = guer++; break;
        case TCivilian:
          grp = civl++; break;
        default:
          continue;
        }

        // group attributes
        role.side = side;
        role.group = grp;

        for (int j=0; j<gInfo.units.Size(); j++)
        {
          ArcadeUnitInfo &uInfo = gInfo.units[j];

          // unit attributes
          role.unit = j;
          role.vehicle = uInfo.vehicle;
          role.description = uInfo.description;
          role.unitName = uInfo.name;
          role.flags = flags;
          if (uInfo.forceInServer) 
          {
            role.flags |= PRFForced;
            role.player = BOT_CLIENT; //server is forced to play this role!
          }
          else 
            role.player = AI_PLAYER;

          switch (uInfo.player)
          {
          case APPlayerCommander:
            {
              Ref<EntityType> type = VehicleTypes.New(uInfo.vehicle);
              EntityAIType *aiType = dynamic_cast<EntityAIType *>(type.GetRef());
              const TurretType *commandingTT = FindCommandingTT(aiType);
              int added = 0;
              if (aiType)
              {
                // add all observers
                AddTurretRoles func(_playerRoles, role, added, false, true, uInfo.leader, commandingTT);
                aiType->ForEachTurret(func);
              }
              if (added == 0)
              {
                role.turret.Clear(); // driver is commander
                role.leader = uInfo.leader;
                _playerRoles.Add(role);
              }
            }
            break;
          case APPlayerDriver:
            {
              Ref<EntityType> type = VehicleTypes.New(uInfo.vehicle);
              EntityAIType *aiType = dynamic_cast<EntityAIType *>(type.GetRef());
              const TurretType *commandingTT = FindCommandingTT(aiType);
              role.turret.Clear();
              if (commandingTT)
                role.leader = false;
              else
                role.leader = uInfo.leader;
              _playerRoles.Add(role);
            }
            break;
          case APPlayerGunner:
            {
              Ref<EntityType> type = VehicleTypes.New(uInfo.vehicle);
              EntityAIType *aiType = dynamic_cast<EntityAIType *>(type.GetRef());
              const TurretType *commandingTT = FindCommandingTT(aiType);
              int added = 0;
              if (aiType)
              {
                // add all gunners
                AddTurretRoles func(_playerRoles, role, added, true, false, uInfo.leader, commandingTT);
                aiType->ForEachTurret(func);
              }
            }
            break;
          case APPlayableC:
            {
              Ref<EntityType> type = VehicleTypes.New(uInfo.vehicle);
              EntityAIType *aiType = dynamic_cast<EntityAIType *>(type.GetRef());
              const TurretType *commandingTT = FindCommandingTT(aiType);
              int added = 0;
              if (aiType)
              {
                // add all observers
                AddTurretRoles func(_playerRoles, role, added, false, true, uInfo.leader, commandingTT);
                aiType->ForEachTurret(func);
              }
            }
            break;
          case APPlayableD:
            {
              Ref<EntityType> type = VehicleTypes.New(uInfo.vehicle);
              EntityAIType *aiType = dynamic_cast<EntityAIType *>(type.GetRef());
              const TurretType *commandingTT = FindCommandingTT(aiType);
              role.turret.Clear();
              if (commandingTT)
                role.leader = false;
              else
                role.leader = uInfo.leader;
              _playerRoles.Add(role);
            }
            break;
          case APPlayableG:
            {
              Ref<EntityType> type = VehicleTypes.New(uInfo.vehicle);
              EntityAIType *aiType = dynamic_cast<EntityAIType *>(type.GetRef());
              const TurretType *commandingTT = FindCommandingTT(aiType);
              int added = 0;
              if (aiType)
              {
                // add all gunners
                AddTurretRoles func(_playerRoles, role, added, true, false, uInfo.leader, commandingTT);
                aiType->ForEachTurret(func);
              }
            }
            break;
          case APPlayableCD:
            {
              Ref<EntityType> type = VehicleTypes.New(uInfo.vehicle);
              EntityAIType *aiType = dynamic_cast<EntityAIType *>(type.GetRef());
              const TurretType *commandingTT = FindCommandingTT(aiType);
              int added = 0;
              if (aiType)
              {
                // add all observers
                AddTurretRoles func(_playerRoles, role, added, false, true, uInfo.leader, commandingTT);
                aiType->ForEachTurret(func);
              }
              role.turret.Clear();
              if (commandingTT)
                role.leader = false;
              else
                role.leader = uInfo.leader;
              _playerRoles.Add(role);
            }
            break;
          case APPlayableCG:
            {
              Ref<EntityType> type = VehicleTypes.New(uInfo.vehicle);
              EntityAIType *aiType = dynamic_cast<EntityAIType *>(type.GetRef());
              const TurretType *commandingTT = FindCommandingTT(aiType);
              int added = 0;
              if (aiType)
              {
                {
                  // add all observers
                  AddTurretRoles func(_playerRoles, role, added, false, true, uInfo.leader, commandingTT);
                  aiType->ForEachTurret(func);
                }
                {
                  // add all gunners
                  AddTurretRoles func(_playerRoles, role, added, true, false, uInfo.leader, commandingTT);
                  aiType->ForEachTurret(func);
                }
              }
            }
            break;
          case APPlayableDG:
            {
              Ref<EntityType> type = VehicleTypes.New(uInfo.vehicle);
              EntityAIType *aiType = dynamic_cast<EntityAIType *>(type.GetRef());
              const TurretType *commandingTT = FindCommandingTT(aiType);
              int added = 0;
              role.turret.Clear();
              if (commandingTT)
                role.leader = false;
              else
                role.leader = uInfo.leader;
              _playerRoles.Add(role);
              if (aiType)
              {
                // add all gunners
                AddTurretRoles func(_playerRoles, role, added, true, false, uInfo.leader, commandingTT);
                aiType->ForEachTurret(func);
              }
            }
            break;
          case APPlayableCDG:
            // all available
            {
              Ref<EntityType> type = VehicleTypes.New(uInfo.vehicle);
              EntityAIType *aiType = dynamic_cast<EntityAIType *>(type.GetRef());
              const TurretType *commandingTT = FindCommandingTT(aiType);
              int added = 0;
              if (aiType)
              {
                // add all observers
                AddTurretRoles func(_playerRoles, role, added, false, true, uInfo.leader, commandingTT);
                aiType->ForEachTurret(func);
              }
              // add driver
              role.turret.Clear();
              if (commandingTT)
                role.leader = false;
              else
                role.leader = uInfo.leader; //nobody in turret is leader, so the driver should be
              _playerRoles.Add(role);
              if (aiType)
              {
                // add all gunners
                AddTurretRoles func(_playerRoles, role, added, true, false, uInfo.leader, commandingTT);
                aiType->ForEachTurret(func);
              }
            }
            break;
          }
        }
      }
      
      // independent agents
      role.side = TAmbientLife;
      role.group = -1;

      for (int j=0; j<CurrentTemplate.emptyVehicles.Size(); j++)
      {
        ArcadeUnitInfo &uInfo = CurrentTemplate.emptyVehicles[j];

        // unit attributes
        role.unit = j;
        role.vehicle = uInfo.vehicle;
        role.description = uInfo.description;
        role.unitName = uInfo.name;

        switch (uInfo.player)
        {
        case APPlayerCommander:
          {
            Ref<EntityType> type = VehicleTypes.New(uInfo.vehicle);
            EntityAIType *aiType = dynamic_cast<EntityAIType *>(type.GetRef());
            const TurretType *commandingTT = FindCommandingTT(aiType);
            int added = 0;
            if (aiType)
            {
              // add all observers
              AddTurretRoles func(_playerRoles, role, added, false, true, uInfo.leader, commandingTT);
              aiType->ForEachTurret(func);
            }
            if (added == 0)
            {
              role.turret.Clear(); // driver is commander
              role.leader = uInfo.leader;
              _playerRoles.Add(role);
            }
          }
          break;
        case APPlayerDriver:
          {
            Ref<EntityType> type = VehicleTypes.New(uInfo.vehicle);
            EntityAIType *aiType = dynamic_cast<EntityAIType *>(type.GetRef());
            const TurretType *commandingTT = FindCommandingTT(aiType);
            role.turret.Clear();
            if (commandingTT)
              role.leader = false;
            else
              role.leader = uInfo.leader;
            _playerRoles.Add(role);
          }
          break;
        case APPlayerGunner:
          {
            Ref<EntityType> type = VehicleTypes.New(uInfo.vehicle);
            EntityAIType *aiType = dynamic_cast<EntityAIType *>(type.GetRef());
            const TurretType *commandingTT = FindCommandingTT(aiType);
            int added = 0;
            if (aiType)
            {
              // add all gunners
              AddTurretRoles func(_playerRoles, role, added, true, false, uInfo.leader, commandingTT);
              aiType->ForEachTurret(func);
            }
          }
          break;
        case APPlayableC:
          {
            Ref<EntityType> type = VehicleTypes.New(uInfo.vehicle);
            EntityAIType *aiType = dynamic_cast<EntityAIType *>(type.GetRef());
            const TurretType *commandingTT = FindCommandingTT(aiType);
            int added = 0;
            if (aiType)
            {
              // add all observers
              AddTurretRoles func(_playerRoles, role, added, false, true, uInfo.leader, commandingTT);
              aiType->ForEachTurret(func);
            }
          }
          break;
        case APPlayableD:
          {
            Ref<EntityType> type = VehicleTypes.New(uInfo.vehicle);
            EntityAIType *aiType = dynamic_cast<EntityAIType *>(type.GetRef());
            const TurretType *commandingTT = FindCommandingTT(aiType);
            role.turret.Clear();
            if (commandingTT)
              role.leader = false;
            else
              role.leader = uInfo.leader;
            _playerRoles.Add(role);
          }
          break;
        case APPlayableG:
          {
            Ref<EntityType> type = VehicleTypes.New(uInfo.vehicle);
            EntityAIType *aiType = dynamic_cast<EntityAIType *>(type.GetRef());
            const TurretType *commandingTT = FindCommandingTT(aiType);
            int added = 0;
            if (aiType)
            {
              // add all gunners
              AddTurretRoles func(_playerRoles, role, added, true, false, uInfo.leader, commandingTT);
              aiType->ForEachTurret(func);
            }
          }
          break;
        case APPlayableCD:
          {
            Ref<EntityType> type = VehicleTypes.New(uInfo.vehicle);
            EntityAIType *aiType = dynamic_cast<EntityAIType *>(type.GetRef());
            const TurretType *commandingTT = FindCommandingTT(aiType);
            int added = 0;
            if (aiType)
            {
              // add all observers
              AddTurretRoles func(_playerRoles, role, added, false, true, uInfo.leader, commandingTT);
              aiType->ForEachTurret(func);
            }
            role.turret.Clear();
            if (commandingTT)
              role.leader = false;
            else
              role.leader = uInfo.leader;
            _playerRoles.Add(role);
          }
          break;
        case APPlayableCG:
          {
            Ref<EntityType> type = VehicleTypes.New(uInfo.vehicle);
            EntityAIType *aiType = dynamic_cast<EntityAIType *>(type.GetRef());
            const TurretType *commandingTT = FindCommandingTT(aiType);
            int added = 0;
            if (aiType)
            {
              {
                // add all observers
                AddTurretRoles func(_playerRoles, role, added, false, true, uInfo.leader, commandingTT);
                aiType->ForEachTurret(func);
              }
              {
                // add all gunners
                AddTurretRoles func(_playerRoles, role, added, true, false, uInfo.leader, commandingTT);
                aiType->ForEachTurret(func);
              }
            }
          }
          break;
        case APPlayableDG:
          {
            Ref<EntityType> type = VehicleTypes.New(uInfo.vehicle);
            EntityAIType *aiType = dynamic_cast<EntityAIType *>(type.GetRef());
            const TurretType *commandingTT = FindCommandingTT(aiType);
            role.turret.Clear();
            if (commandingTT)
              role.leader = false;
            else
              role.leader = uInfo.leader;
            _playerRoles.Add(role);
            {
              int added = 0;
              if (aiType)
              {
                // add all gunners
                AddTurretRoles func(_playerRoles, role, added, true, false, uInfo.leader, commandingTT);
                aiType->ForEachTurret(func);
              }
            }
          }
          break;
        case APPlayableCDG:
          // all available
          {
            Ref<EntityType> type = VehicleTypes.New(uInfo.vehicle);
            EntityAIType *aiType = dynamic_cast<EntityAIType *>(type.GetRef());
            const TurretType *commandingTT = FindCommandingTT(aiType);
            int added = 0;
            if (aiType)
            {
              // add all observers
              AddTurretRoles func(_playerRoles, role, added, false, true, uInfo.leader, commandingTT);
              aiType->ForEachTurret(func);
            }
            // add driver
            role.turret.Clear();
            if (commandingTT)
              role.leader = false;
            else
              role.leader = uInfo.leader;
            _playerRoles.Add(role);
            if (aiType)
            {
              // add all gunners
              AddTurretRoles func(_playerRoles, role, added, true, false, uInfo.leader, commandingTT);
              aiType->ForEachTurret(func);
            }
          }
          break;
        }
      }
    }
    else
    {
      // init roles for players
      for (int i=0; i<_playerRoles.Size(); i++)
      {
        _playerRoles[i].flags = flags;
        _playerRoles[i].player = AI_PLAYER;
      }
    }
  }

  // send mission description and roles
  for (int i=0; i<_players.Size(); i++)
  {
    NetworkPlayerInfo &info = _players[i];
    if (info.clientState < NCSConnected) continue;
    SendMissionInfo(info.dpid);
/*
    info.state = NGSPrepareSide;
    SetPlayerState(info.dpid, NGSPrepareSide);
*/
    info.missionFileValid = false;
    info.paramFileValid = GetMissionParameters().GetLength() == 0;
  }

  if (header)
  {
    int minPlayers = -1;
    int maxPlayers = -1;
    ConstParamEntryPtr entry = header->FindEntry("minPlayers");
    if (entry) minPlayers = *entry;
    entry = header->FindEntry("maxPlayers");
    if (entry) maxPlayers = *entry;

    if (maxPlayers >= 0)
    {
      if (maxPlayers != _playerRoles.Size())
        RptF
        (
          "Mission %s: Number of roles (%d) is different from 'description.ext::Header::maxPlayer' (%d)",
          (const char *)(Glob.header.filenameReal + RString(".") + Glob.header.worldname),
          _playerRoles.Size(), maxPlayers
        );
    }
    else
    {
      RptF
      (
        "Mission %s: Missing 'description.ext::Header::maxPlayer'",
        (const char *)(Glob.header.filenameReal + RString(".") + Glob.header.worldname)
      );
    }
    if (minPlayers >= 0)
    {
      if (minPlayers < 1)
        RptF
        (
          "Mission %s: 'description.ext::Header::minPlayer' (%d) is wrong",
          (const char *)(Glob.header.filenameReal + RString(".") + Glob.header.worldname),
          minPlayers
        );
      if (minPlayers > _playerRoles.Size())
        RptF
        (
          "Mission %s: 'description.ext::Header::minPlayer' (%d) is too high - there is %d roles",
          (const char *)(Glob.header.filenameReal + RString(".") + Glob.header.worldname),
          minPlayers, _playerRoles.Size()
        );
    }
    else
    {
      RptF
      (
        "Mission %s: Missing 'description.ext::Header::minPlayer'",
        (const char *)(Glob.header.filenameReal + RString(".") + Glob.header.worldname)
      );
    }
  }
  else
  {
    RptF
    (
      "Mission %s: Missing 'description.ext::Header'",
      (const char *)(Glob.header.filenameReal + RString(".") + Glob.header.worldname)
    );
  }
}

void NetworkServer::ResetMPRoles()
{
#if _ENABLE_AI
  int flags = _missionHeader._disabledAI ? PRFNone : PRFEnabledAI;
#else
  int flags = PRFNone;
#endif
  // init roles for players
  for (int i=0; i<_playerRoles.Size(); i++)
  {
    if (_playerRoles[i].flags&PRFForced)
    {
      _playerRoles[i].flags = flags | PRFForced;
      _playerRoles[i].player = BOT_CLIENT;
    }
    else
    {
      _playerRoles[i].flags = flags;
      _playerRoles[i].player = AI_PLAYER;
    }
    _playerRoles[i].lifeState = LifeStateAlive;
  }
  SendPlayerRolesMessages();
}

/// save/load the MP game
LSError NetworkServer::SerializeMP(ParamArchive &ar)
{
  if (ar.IsSaving())
  { // roles and NetworkPlayerInfo for each player
    SaveRolesToSave((SaveGameType)GetNetworkManager().GetCurrentSaveType());
    // We want to serialize all IsSerializable() messages from _initMessages
    AutoArray<NetworkMessageQueueItem> initMsgItems;
    initMsgItems.Realloc(_initMessages.Size());
    for (int i=0; i<_initMessages.Size(); i++)
    {
      if (_initMessages[i].msg->IsSerializable()) initMsgItems.Add(_initMessages[i]);
    }
    CHECK(ar.Serialize("initMessages", initMsgItems, 1))
  }
  else
  {
    CHECK(ar.Serialize("initMessages", _initMessages, 1))
  }
  CHECK(ar.Serialize("nextPlayerId", _nextPlayerId, 1))

  // serialize serverTime related stuff
  if (ar.IsSaving())
  {
    int secondsToEnd = _serverTimeOfEstimatedEnd ? (_serverTimeOfEstimatedEnd-_serverTime) : 0;
    CHECK(ar.Serialize("secondsToEnd", secondsToEnd, 1, 0));
  }
  else 
  {
    CHECK(ar.Serialize("secondsToEnd", _secondsToEstimatedEnd, 1, 0));
    _serverTimeOfEstimatedEnd = _secondsToEstimatedEnd ? (_serverTime + _secondsToEstimatedEnd) : 0;
    _nextServerTimeUpdate = _serverTime + ServerTimeUpdateInterval;
  }

  return LSOK;
}

void NetworkServer::SetPlayerRoleLifeState(int roleIndex, LifeState lifeState)
{
  if (roleIndex>=0 && roleIndex<_playerRoles.Size())
  {
    switch (_missionHeader._respawn)
    {
    case RespawnAtPlace:
    case RespawnInBase:
      break; // do not disable MP slots (roles) for these Respawn Modes
    case RespawnNone:
    case RespawnSeaGull:
    case RespawnToGroup:
    case RespawnToFriendly:
      _playerRoles[roleIndex].lifeState = lifeState;
      // send PlayerRole update to all clients
      // create message
      SendPlayerRoleUpdate(roleIndex);
      break;
    }
  }
}

void NetworkServer::SendPlayerRoleUpdate(int roleIndex)
{
  PlayerRoleUpdate msg;
  msg._index = roleIndex;
  msg._player = _playerRoles[roleIndex].player;
  msg._lifeState = _playerRoles[roleIndex].lifeState;

  // send message
  for (int i=0; i<_players.Size(); i++)
  {
    NetworkPlayerInfo &player = _players[i];
    if (player.clientState < NCSConnected) continue;
    SendMsg(player.dpid, &msg, NMFGuaranteed);
  }
}

LSError NetworkPlayerInfo::Serialize(ParamArchive &ar)
{
  CHECK(ar.Serialize("Name", name, 1))
  //CHECK(person.Serialize(ar, "Person", 1));
  //CHECK(unit.Serialize(ar, "Unit", 1))
  //CHECK(group.Serialize(ar, "Group", 1))
  
  // dpid is serialized inside PlayerRole as player (useful for mapping 'name' to 'role')
  CHECK(ar.Serialize("player", dpid, 1, AI_PLAYER))

  return LSOK;
}

void NetworkServer::ClearRoles()
{
  if (_serverState < NSSLoadingGame)
  {
    // creating of roles
    _playerRoles.Resize(0);
  }
  else if (_serverState == NSSLoadingGame)
  {
    // attaching of roles
    _playerRoleToAttach = 0;
  }
}

int NetworkServer::AddRole(Person *person)
{
  if (!person) return -1;
  AIBrain *brain = person->Brain();
  if (!brain) return -1;

  if (_serverState < NSSLoadingGame)
  {
    // creating of roles
    int index = _playerRoles.Add();
    PlayerRole &role = _playerRoles[index];
    role.side = brain->GetSide();
    role.group = -1;
    role.unit = -1;
    role.vehicle = person->GetName();
    role.turret.Clear();
    role.leader = false;
    // role.description;

    return index;
  }
  else if (_serverState == NSSLoadingGame)
  {
    // attaching of role
    int index = _playerRoleToAttach++;
    if (index >= _playerRoles.Size()) return -1;

    brain->SetRoleIndex(index);

    PlayerRole &role = _playerRoles[index];
    person->SetRemotePlayer(role.player);
    if (role.player != AI_PLAYER)
    {
      _parent->SelectPlayer(role.player, person);
    }
    return index;
  }
  return -1; // not supported in-game
}

void NetworkServer::UpdateMaxPlayers(int maxPlayers, int privateSlots)
{
  _totalSlots = maxPlayers;

  maxPlayers = GetMaxPlayers();
#if _ENABLE_DEDICATED_SERVER
  if (IsDedicatedServer()) maxPlayers++; // one more slot for the dedicated server bot client
#endif

  if(privateSlots > maxPlayers)
    privateSlots = maxPlayers;
#if _XBOX_SECURE
  if (_server) _server->UpdateMaxPlayers(maxPlayers, privateSlots);
  UpdateMatchmakingDescription(false);
#else
  if (_server) _server->UpdateMaxPlayers(maxPlayers);
# if _ENABLE_GAMESPY
    ChangeStateQR();
# endif
#endif

#if _GAMES_FOR_WINDOWS || (defined _XBOX && _XBOX_VER >= 200)
  // update the session description
  if (_parent) _parent->UpdateSession(maxPlayers, privateSlots);
#endif
}

void NetworkServer::ChangeOwner(NetworkId &id, int from, int to)
{
  if (from == to) return;
  if (id.IsNull()) return;

  NetworkObjectInfo *oInfo = GetObjectInfo(id);
  if (!oInfo)
  {
    RptF("Server: Object info %d:%d not found.", id.creator, id.id);
    return;
  }

  if (oInfo->pendingOwner!=NO_CLIENT)
  { // change of owner of this object is already pending
    oInfo->pendingOwner = to; // BUT change pending owner to new one
    return;
  }
  oInfo->pendingOwner = to; // mark this change of owner as pending

  ChangeOwnerMessage msg;
  msg._creator = id.creator;
  msg._id = id.id;
  msg._owner = to;
  msg._disconnected = false;
  SendMsg(from, &msg, NMFGuaranteed);
  //note: new owner will be notified after the ownerChanged returns from previous owner
  //      so do not SendMsg(to, &msg, NMFGuaranteed); now
}

/*!
\patch_internal 5088 Date 11/15/2006 by Jirka
- Fixed: Network - handling of objects ownership changed (groups, subgroups)
*/
void NetworkServer::UpdateGroupLeader(NetworkId &group, NetworkId &leader)
{
  NetworkPlayerInfo *oldOwnerInfo = NULL;
  NetworkPlayerInfo *newOwnerInfo = NULL;
  for (int i=0; i<_players.Size(); i++)
  {
    NetworkPlayerInfo &pInfo = _players[i];
    if (pInfo.group == group)
      oldOwnerInfo = &pInfo;
    if (pInfo.unit == leader)
      newOwnerInfo = &pInfo;
  }

  // set the player group
  if (oldOwnerInfo) oldOwnerInfo->group = NetworkId::Null();
  if (newOwnerInfo) newOwnerInfo->group = group;

  NetworkObjectInfo *groupInfo = GetObjectInfo(group);
  if (!groupInfo) return; //FIX: server crashed here when client was in DisplayTeamSwitch and server aborted (suspend) the game

  NetworkObjectInfo *leaderInfo = leader.IsNull() ? NULL : GetObjectInfo(leader);
  int newOwner = leaderInfo ? leaderInfo->owner : groupInfo->owner; // when no leader exist, keep ownership where it is

  // change group owner
  {
    int oldOwner = groupInfo->owner;
    ChangeOwner(group, oldOwner, newOwner);
  }

  // change subgroups owner
  AutoArray<NetworkId> persons;
  for (int i=0; i<_objects.Size(); i++)
  {
    NetworkObjectInfo &oInfo = *_objects[i];
    NetworkCurrentInfo &info = oInfo.current[NMCUpdateGeneric];
    if (!info.message) continue;
    if (info.type != NMTUpdateAISubgroup) continue;
    NetworkMessageContext ctx(info.message, this, info.from, MSG_RECEIVE);

    PREPARE_TRANSFER(UpdateAISubgroup)

    NetworkId grp;
    if (TRANSF_GET_ID_BASE(group, grp) != TMOK) continue;
    if (grp != group) continue;
    ChangeOwner(oInfo.id, oInfo.owner, newOwner);
    
    AutoArray<NetworkId> units;
    if (TRANSF_GET_IDS_BASE(units, units) != TMOK) continue;
    for (int j=0; j<units.Size(); j++)
    {
      NetworkId unit = units[j];
      if (unit.IsNull()) continue;

      NetworkObjectInfo *oInfo = GetObjectInfo(unit);
      if (!oInfo) continue;

      // avoid remote controlled units change
      NetworkCurrentInfo &info = oInfo->current[NMCUpdateGeneric];
      if ( info.message && IsUpdateUnit(info.type) ) 
      {
        NetworkMessageContext ctx(info.message, this, info.from, MSG_RECEIVE);
        PREPARE_TRANSFER(UpdateAIBrain)
        NetworkId rmCtrled;
        if (TRANSF_GET_ID_BASE(remoteControlled, rmCtrled) == TMOK)
        {
          if (!rmCtrled.IsNull())  // remote controlled unit detected
            continue;
        }
      }

      // avoid players' units change
      bool found = false;
      for (int j=0; j<_players.Size(); j++)
      {
        NetworkPlayerInfo &pInfo = _players[j];
        if (pInfo.unit == unit)
        {
          found = true;
          break;
        }
      }
      if (found) continue;
      int oldOwner = oInfo->owner;
      ChangeOwner(unit, oldOwner, newOwner);
      // change person owner
      NetworkId person = UnitToPerson(unit);
      persons.Add(person);
      ChangeOwner(person, oldOwner, newOwner);
    }
  }

  // change transports owner
  for (int i=0; i<_objects.Size(); i++)
  {
    NetworkObjectInfo &oInfo = *_objects[i];
    NetworkCurrentInfo &info = oInfo.current[NMCUpdateGeneric];
    if (!info.message) continue;
    if (!IsUpdateTransport(info.type)) continue;
    NetworkMessageContext ctx(info.message, this, info.from, MSG_RECEIVE);

    PREPARE_TRANSFER(UpdateTransport)

    NetworkId driver;
    if (TRANSF_GET_ID_BASE(driver, driver) != TMOK) continue;
    bool found = false;
    for (int j=0; j<persons.Size(); j++)
      if (persons[j] == driver)
      {
        found = true;
        break;
      }
    if (!found) continue;
    ChangeOwner(oInfo.id, oInfo.owner, newOwner);
  }
}

void NetworkServer::SendMissionInfo(int to, bool onlyPlayers)
{
  _missionHeader._updateOnly = false;

  if (!onlyPlayers)
    SendMsg(to, &_missionHeader, NMFGuaranteed);

  for (int i=0; i<_playerRoles.Size(); i++)
  {
    // create message
    NetworkMessageType type = _playerRoles[i].GetNMType(NMCCreate);
    Ref<NetworkMessage> msg = ::CreateNetworkMessage(type);
    msg->time = Glob.time;
    NetworkMessageContext ctx(msg, this, to, MSG_SEND);

    PREPARE_TRANSFER(PlayerRole)

    TMError err;
    err = TRANSF_BASE(index, i);
    if (err != TMOK) continue;
    err = _playerRoles[i].TransferMsg(ctx);
    if (err != TMOK) continue;

    // send message
    NetworkComponent::SendMsg(to, msg, type, NMFGuaranteed);
  }

  if (!onlyPlayers)
  {
    // send last, client will update its state as reaction
    MissionParamsMessage msg;
    msg._param1 = _param1;
    msg._param2 = _param2;
    msg._paramsArray = _paramsArray;
    msg._updateOnly = false;
    SendMsg(to, &msg, NMFGuaranteed);
  }
}

void Encrypt(QOStrStream &out,const unsigned char *publicKey, int keySize);

//{ DEDICATED SERVER SUPPORT
bool NetworkServer::IsDedicatedBotClient(int dpnid) const
{
#if _ENABLE_DEDICATED_SERVER
  return _dedicated && dpnid == BOT_CLIENT;
#else
  return false;
#endif
}
//}

//! Text representation of NetworkServerState enum values
const EnumName NetworkServerStateNames[]=
{
  EnumName(NSSNone, "NONE"),
  EnumName(NSSSelectingMission, "SELECTING MISSION"),
  EnumName(NSSEditingMission, "EDITING MISSION"),
  EnumName(NSSAssigningRoles, "ASSIGNING ROLES"),
  EnumName(NSSSendingMission, "SENDING MISSION"),
  EnumName(NSSLoadingGame, "LOADING GAME"),
  EnumName(NSSBriefing, "BRIEFING"),
  EnumName(NSSPlaying, "PLAYING"),
  EnumName(NSSDebriefing, "DEBRIEFING"),
  EnumName(NSSMissionAborted, "MISSION ABORTED"),
  EnumName()
};

RString NetworkServer::GetAddInitAndRemoveOverriddenStat()
{
  int stats[NMTN];
  for (int i=0; i<NMTN; i++) stats[i] = 0; //zero statistics

  // get the statistics
  for (int i=0; i<_initMessages.Size(); i++)
  {
    int type = (int)_initMessages[i].type;
    if ( type >= 0 && type < NMTN ) stats[type]++;
  }

  RString retValStr;
  retValStr.CreateBuffer(NMTN * 256); // 256 chars for each line of statistics should be enough
  char *buf = retValStr.MutableData();
  sprintf(buf, "AddInitAndRemoveOverridden statistics ... total messages = %d\n", _initMessages.Size());
  for (int i=0; i<NMTN; i++)
  {
    if ( stats[i]>0 )
    {
      RString name = GetMsgTypeName( (NetworkMessageType)i );
      RString newLine = Format("%d ... %s\n", stats[i], cc_cast(name));
      strcat(buf, newLine);
    }
  }
  strcat(buf, "\n\n");
  return retValStr;
}

//! Map NetworkServerState enum values to its text representation
template<>
const EnumName *GetEnumNames(NetworkServerState dummy) {return NetworkServerStateNames;}

/*!
\patch 1.32 Date 11/26/2001 by Jirka
- Fixed: players' states are properly send to all clients
\patch 5142 Date 3/16/2007 by Jirka
- Fixed: MP - handling / reporting the current and maximum number of players was wrong sometimes
\patch 5154 Date 5/3/2007 by Jirka
- Fixed: MP mission statistics can be written to the file given by command line argument -ranking=...
\patch 5170 Date 10/8/2007 by Bebul
- New: Command line argument -ranking=... was removed. MP mission statistics can be written to file specified by logFile entry in the server config.
*/

void NetworkServer::SetServerState(NetworkServerState state)
{
//((TO_BE_REMOVED_IN_FINAL
#if _ENABLE_PERFLOG
  RptF("Server: SetServerState to %s", (const char *)FindEnumName(state));
#endif
//))TO_BE_REMOVED_IN_FINAL

  // Transition graph:
  switch (state)
  {
  case NSSNone:
    // nothing to do
    break;
  case NSSSelectingMission:
    break;
  case NSSEditingMission:
    DoAssert(_serverState == NSSSelectingMission);
    // nothing to do
    break;
  case NSSAssigningRoles:
    break;
  case NSSSendingMission:
    DoAssert(_serverState == NSSAssigningRoles);
    for (int i=0; i<NPlayerRoles(); i++)
    {
      const PlayerRole *role = GetPlayerRole(i);
      int dpnid = role->player;
      if (dpnid != AI_PLAYER)
      {
        if (GetClientState(dpnid) == NCSMissionAsked)
          SetClientState(dpnid, NCSRoleAssigned);
      }
    }
    // reset _jipProcessed flag for all players
    for (int i=0; i<_players.Size(); i++)
    {
      _players[i]._jipProcessed=false;
    }
    break;
  case NSSLoadingGame:
    _serverTimeOfEstimatedEnd = _secondsToEstimatedEnd = 0;
    for (int i=0; i<NPlayerRoles(); i++)
    {
      const PlayerRole *role = GetPlayerRole(i);
      int dpnid = role->player;
      if (dpnid != AI_PLAYER)
      { // send _serverTime on each client
        ServerTimeMessage stmsg;
        _serverTime = ::GetTickCount()-_serverTimeInitTickCount; // update _serverTime
        stmsg._time = _serverTime;
        stmsg._endTime = _serverTimeOfEstimatedEnd;
        SendMsg(dpnid, &stmsg, NMFGuaranteed | NMFHighPriority);
      }
    }
    break;
  case NSSBriefing:
    break;
  case NSSPlaying:
    // recompute estimatedTimeOfEnd to be without time spent in briefing
    _serverTime = ::GetTickCount() - _serverTimeInitTickCount; //make it fresh
    if (_serverTimeOfEstimatedEnd) //recompute
    {
      _serverTimeOfEstimatedEnd = _serverTime + _secondsToEstimatedEnd;
      for (int i=0; i<NPlayerRoles(); i++)
      {
        const PlayerRole *role = GetPlayerRole(i);
        int dpnid = role->player;
        if (dpnid != AI_PLAYER)
        {
          // send _serverTime on each client
          ServerTimeMessage stmsg;
          _serverTime = ::GetTickCount()-_serverTimeInitTickCount; // update _serverTime
          stmsg._time = _serverTime;
          stmsg._endTime = _serverTimeOfEstimatedEnd;
          SendMsg(dpnid, &stmsg, NMFGuaranteed | NMFHighPriority);
        }
      }
    }
    break;
  case NSSDebriefing:
    break;
  case NSSMissionAborted:
    break;
  }

  #if _ENABLE_MP
  if (state == NSSAssigningRoles && _serverState > NSSAssigningRoles)
  {
    // cancel all messages - updates from last sessions, file transfers etc.
    // !!! cannot do it in this way - some messages are important
    // _server->CancelAllMessages();

    // reset all camera information from previous session
    for (int i=0; i<_players.Size(); i++)
    {
      NetworkPlayerInfo &p = _players[i];
      p.cameraPosition = InvalidCamPos;
      p.cameraPositionTime = TIME_MIN;
    }
  }
  #endif

  if (state == NSSBriefing)
  {
    // TODO: Selecting players when entering briefing state - obsolete ???
    // select players
    for (int i=0; i<_players.Size(); i++)
    {
      NetworkPlayerInfo &info = _players[i];
      NetworkId &id = info.person;
      if (!id.IsNull())
      {
        NetworkObjectInfo *oInfo = GetObjectInfo(id);
        if (oInfo) ChangeOwner(id, oInfo->owner, info.dpid);
        SelectPlayerMessage pl;
        pl._player = info.dpid;
        pl._creator = id.creator;
        pl._id = id.id;
        pl._position = info.cameraPosition;
        pl._respawn = false;
        NetworkComponent::SendMsg(info.dpid, &pl, NMFGuaranteed);
      }
    }
  }

  if (state == NSSPlaying) _missionHeader._start = GlobalTickCount();

  #if _ENABLE_MP && !defined _XBOX
  if ((state == NSSDebriefing || state == NSSMissionAborted) && _serverState == NSSPlaying)
  {
    // end of mission - update log file
    FILE *LogFile = GetNetworkManager().GetDedicatedServerLogFile();
    if ( LogFile )
    {
      ParamFile f;
      ParamClassPtr root = f.AddClass("Session");
      if (root)
      {
        root->Add("mission", _missionHeader.GetLocalizedMissionName());
        root->Add("island", _missionHeader._island);
        root->Add("gameType", _missionHeader._gameType);
        root->Add("duration", 0.001f * (GlobalTickCount() - _missionHeader._start));
        RefArray<AIStatsMPRow> &table = GStats._mission._tableMP;
        for (int i=0; i<table.Size(); i++)
        {
          char buffer[32];
          sprintf(buffer, "Player%d", i + 1);
            ParamClassPtr cls = root->AddClass(buffer);
          if (cls)
          {
            AIStatsMPRow *row = table[i];
            cls->Add("name", row->_name);
            cls->Add("killsInfantry", row->_killsInfantry);
            cls->Add("killsSoft", row->_killsSoft);
            cls->Add("killsArmor", row->_killsArmor);
            cls->Add("killsAir", row->_killsAir);
            cls->Add("killsPlayers", row->_killsPlayers);
            cls->Add("customScore", row->_customScore);
            cls->Add("killsTotal", row->_killsTotal);
            cls->Add("killed", row->_killed);
          }
        }
      }
      // append ranking statistics to logFile
      QOStrStream out;
      f.Save(out, 0, "\r\n");
      fwrite(out.str(), 1, out.pcount(), LogFile);
      // flush is only at the end of mission (here) or some rare occasions
      fflush(LogFile);
    }
  }
#endif

#if _GAMES_FOR_WINDOWS || (defined _XBOX && _XBOX_VER >= 200)
  // Xbox Live - start / end the session
  if (state == NSSPlaying)
  {
    if (_serverState != NSSPlaying)
    {
      _parent->StartSession();
    }
  }
  else if (_serverState == NSSPlaying)
  {
    _parent->EndSession();
  }

  if (_parent->GetSessionType() == STRanked)
  {
    // Ranked match - finish the session when the first game is finished
    if (_serverState >= NSSPlaying && state < NSSPlaying)
    {
      Done();
      return;
    }
    // Ranked match - close the possibility to join and ask for arbitration
    if (state >= NSSSendingMission && _serverState < NSSSendingMission)
    {
      LockSession();
      // ask all clients for arbitration
      AskForArbitrationMessage msg;
      msg._sessionNonce = _parent->GetSessionNonce();
      for (int i=0; i<_identities.Size(); i++)
      {
        int player = _identities[i].dpnid;
        if (player != BOT_CLIENT) SendMsg(player, &msg, NMFGuaranteed);
      }
    }
  }
#endif

  _serverState = state;
  _serverStateTime = GlobalTickCount();
  _serverTimeOutSent = INT_MAX; // assume no value reported yet
  if (state == NSSNone) return; // cannot send messages

  // DS selecting the mission - set the correct maxPlayers
  if (state == NSSAssigningRoles)
  {
    //{ DEDICATED SERVER SUPPORT
#if _ENABLE_DEDICATED_SERVER
    if (IsDedicatedServer())
    {
#if _GAMES_FOR_WINDOWS || defined _XBOX
      // update of max. number of players
      int perClient = 32000;
      int round = 2;
      ConstParamEntryPtr header = ExtParsMission.FindEntry("Header");
      if (header)
      {
        ConstParamEntryPtr entry = header->FindEntry("bandwidthPerClient");
        if (entry) perClient = *entry;
        entry = header->FindEntry("playerCountMultipleOf");
        if (entry) round = *entry;
      }
      int maxPlayers = GetMaxPlayersSafe(perClient, round);
      int GetDSPrivateSlots();
      int maxPrivate = GetDSPrivateSlots();
      saturateMin(maxPrivate, maxPlayers);
      UpdateMaxPlayers(maxPlayers, maxPrivate);
#else
      UpdateMaxPlayers(INT_MAX, 0);
#endif
    }
#endif
    //}
  }

  // DS leaving the mission - reset the original maxPlayers
  if (state == NSSSelectingMission)
  {
    //{ DEDICATED SERVER SUPPORT
#if _ENABLE_DEDICATED_SERVER
    if (IsDedicatedServer())
    {
#if _GAMES_FOR_WINDOWS || defined _XBOX
      // update of max. number of players
      int perClient = 32000;
      int round = 1;
      int maxPlayers = GetMaxPlayersSafe(perClient, round);
      int GetDSPrivateSlots();
      int maxPrivate = GetDSPrivateSlots();
      saturateMin(maxPrivate, maxPlayers);
      UpdateMaxPlayers(maxPlayers, maxPrivate);
#else
      UpdateMaxPlayers(INT_MAX, 0);
#endif
    }
#endif
    //}
  }

  // send to all players
  ServerStateMessage msg;
  msg._serverState = state;
  // Do not send Network state update to clients when game is loaded from save and Loading is in its early stage
  // if ( state<=NSSAssigningRoles || state>=NSSBriefing || !IsMissionLoadedFromSave() )
  {
  for (int i=0; i<_players.Size(); i++)
  {
    NetworkPlayerInfo &info = _players[i];
    if (info.clientState < NCSConnected) continue;
    SendMsg(info.dpid, &msg, NMFGuaranteed);
  }
  }
  // else SendMsg(BOT_CLIENT, &msg, NMFGuaranteed);

  UpdateMatchmakingDescription(true);

#if _GAMES_FOR_WINDOWS || (defined _XBOX && _XBOX_VER >= 200)
  // set the properties and contexts for matchmaking
  // - ServerState
  GSaveSystem.SetContext(CONTEXT_GAME_SERVERSTATE, state);
  // reset the values which have not yet sense
  if (state < NSSAssigningRoles)
  {
    // - MPType
    GSaveSystem.SetContext(CONTEXT_GAME_MPTYPE, 0);
    // - World
    GSaveSystem.SetContext(CONTEXT_GAME_WORLD, 0);
    // - Difficulty
    GSaveSystem.SetContext(CONTEXT_GAME_DIFFICULTY, 0);
    // - MPMission
    GSaveSystem.SetContext(CONTEXT_GAME_MPMISSION, 0);
  }
  if (state < NSSPlaying)
  {
    // - TimeLeft
    GSaveSystem.SetProperty(PROPERTY_GAME_TIMELEFT, 0);
  }
#endif

  if (state == NSSEditingMission)
  {
    // No Load should be after mission was edited
    bool DeleteSaveGames();
    DeleteSaveGames();
    bool DeleteWeaponsFile();
    DeleteWeaponsFile();
  }
}

/*!
\patch 5111 Date 12/22/2006 by Jirka
- Fixed: DS - Mission voting results show correctly localized mission name for missions in addons
*/

void NetworkServer::MissionVotingEcho()
{
  const Voting *miss = _votings.Find(NCMTMission);
  const Voting *diff = _votings.Find(NCMTDifficulty);

  // if the voting is no longer here, it has probably completed
  if (diff && miss)
  {
    VotingMissionProgressMessage progress;
    const AutoArray<char> *diffValue = diff->GetValue();
    if (diffValue)
    {
      progress._diff = RString(diffValue->Data(),diffValue->Size());
    }
    AutoArray<Voting::VoteInfo> order;
    miss->GetOrder(order);
    for (int i=0; i<order.Size(); i++)
    {
      const AutoArray<char> &missValue = order[i].vote->value;
      int missVotes = order[i].count;
      LocalizedString FindLocalizedBriefingName(RString path);
      RString name = RString(missValue.Data(),missValue.Size());
      
      RString mission = name;
      RString dir;

      const char *ext = strrchr(name, '.');
      if (ext) mission = RString(name, ext - cc_cast(name));

#if _ENABLE_MISSION_CONFIG
      ConstParamEntryPtr entry = (Pars >> "CfgMissions" >> "MPMissions").FindEntry(mission);
      if (entry)
        dir = (*entry) >> "directory";
      else
#endif
        dir = RString("MPMissions\\") + name;

      LocalizedString displayName = FindLocalizedBriefingName(dir);
      if (displayName._id.GetLength() == 0)
      {
        displayName = LocalizedString(LocalizedString::PlainText,name);
      }

      progress._missions.Add(displayName);
      progress._missionVotes.Add(missVotes);
    }
    for (int i=0; i<_players.Size(); i++)
    {
      const NetworkPlayerInfo &player = _players[i];
      // notify all clients who might be interested
      if (player.clientState >= NCSConnected && player.clientState<=NCSMissionSelected)
      {
        SendMsg(player.dpid, &progress, NMFGuaranteed);
      }
    }
  }
}

void NetworkServer::UpdateMatchmakingDescription(bool changedParams)
{
  
//{ QUERY & REPORTING SDK
#if _ENABLE_GAMESPY
  ChangeStateQR();
#endif
//}

#if _XBOX_SECURE && _ENABLE_MP
  //! Session update for Xbox matchmaking service
  if (_parent->IsSignedIn() && !_parent->IsSystemLink()) XOnlineUpdateSession(changedParams); // also game state changed
#endif

  UpdateSessionDescription();
  
  // update both broadcast and matchmaking
  _timeLeftLastValueBcast = _timeLeftLastValue = GetSessionTimeLeft();
  _timeLeftLastTime = GlobalTickCount();
}

void NetworkServer::DisconnectPlayer(int dpnid, NetworkId playerPerson)
{
  // remove pending messages attached to this player (contains pointers to objects which will be destroyed)
  for (int i=0; i<_pendingMessages.Size();)
  {
    NetPendingMessage &pending = _pendingMessages[i];
    if (pending.player->player == dpnid)
      _pendingMessages.Delete(i);
    else
      i++;
  }

  // player is disconnecting from game, return ownership of objects to bot client
  for (int i=0; i<_objects.Size(); i++)
  {
    NetworkObjectInfo &oInfo = *_objects[i];
    oInfo.DeletePlayerObjectInfo(dpnid);

    for (int j=NMCUpdateFirst; j<NMCUpdateN; j++)
    {
      NetworkCurrentInfo &info = oInfo.current[j];
      if (info.from == dpnid)
      {
        info.from = BOT_CLIENT;
        info.message = NULL;
      }
    }
    
    // owner of this object has disconnected - possible pendingOwner can take the full ownership
    // or BOT_CLIENT should take it otherwise
    if (oInfo.owner == dpnid)
    {
      if (oInfo.pendingOwner!=NO_CLIENT && oInfo.pendingOwner!=dpnid)
      {
        NetworkPlayerInfo *pInfo = GetPlayerInfo(oInfo.pendingOwner);
        if (!pInfo)
        { // new owner has probably disconnected meanwhile
          oInfo.pendingOwner = BOT_CLIENT;
        }
        oInfo.owner = oInfo.pendingOwner;
      }
      else
      {
        oInfo.owner = BOT_CLIENT;
      }
      // not pending now
      oInfo.pendingOwner = NO_CLIENT;

      // change the owner
      if (!oInfo.id.IsNull())
      {
        ChangeOwnerMessage msg;
        msg._creator = oInfo.id.creator;
        msg._id = oInfo.id.id;
        msg._owner = oInfo.owner;
        msg._disconnected = true;
        SendMsg(oInfo.owner, &msg, NMFGuaranteed);

        // owner is chosen so send waiting messages
        for ( int i=0; i>oInfo.waitingMessages.Size(); i++ )
        {
          WaitingMessageInfo *wMsgInfo = oInfo.waitingMessages[i];
          NetworkComponent::SendMsg(oInfo.owner, wMsgInfo->msg, wMsgInfo->type, NMFGuaranteed);
        }
        oInfo.waitingMessages.Clear();
      }
    }
  }

  CleanupPlayerMessage msg;
  msg._dpnid = dpnid;
  msg._creator = playerPerson.creator;
  msg._id = playerPerson.id;
  SendMsg(BOT_CLIENT, &msg, NMFGuaranteed);
}

#if _ENABLE_DEDICATED_SERVER
void NetworkServer::AskForStatsWrite(
  int player,
  int board, const AutoArray<XONLINE_STAT> &stats,
  int boardTotal, const AutoArray<XONLINE_STAT> &statsTotal
  )
{
  AskForStatsWriteMessage msg;
  msg._board = board;
  msg._stats = stats;
  msg._boardTotal = boardTotal;
  msg._statsTotal = statsTotal;
  SendMsg(player, &msg, NMFGuaranteed);
}
#endif

void NetworkServer::OnClientStateChanged(int dpnid, NetworkClientState state)
{
  PROFILE_SCOPE_EX(onClS, network)
  NetworkClientState oldState = NCSNone;

  NetworkPlayerInfo *info = GetPlayerInfo(dpnid);
  Assert(info);
  if (info)
  {
    oldState = info->clientState;
    if (oldState == state) return;
    if (state == NCSRoleAssigned && oldState > NCSRoleAssigned) 
      return; // FIX: problems with mission launching 
// RptF("*** Server: OnClientStateChanged %s %s -> %s", (const char *)info->name, (const char *)FindEnumName(info->clientState), (const char *)FindEnumName(state));

    info->clientState = state;
  }

  //RptF("Server: OnClientStateChanged %s to %s", info ? (const char *)info->name : "NULL", (const char *)FindEnumName(state));

  PlayerIdentity *identity = FindIdentity(dpnid);
  if (identity)
  {    
#if _USE_BATTL_EYE_SERVER
#if _ENABLE_GAMESPY && _VERIFY_CLIENT_KEY
    if (state == NCSBriefingRead && identity->clientState != NCSBriefingRead)
    {
      const char *hash = gcd_getkeyhash(GetGameID(), identity->dpnid);
      sockaddr_in address;
      _server->GetClientAddress(identity->dpnid, address);
      _beIntegration.AddPlayer(identity->dpnid, address.sin_addr.s_addr, address.sin_port, identity->name, (void *)hash, strlen(hash));
    }
    if (identity->clientState == NCSBriefingRead && state != NCSBriefingRead)
    {
      _beIntegration.RemovePlayer(identity->dpnid);
    }
#endif
#endif

    identity->clientState = state;
  }
  
  PlayerClientStateMessage msg;
  msg._dpnid = dpnid;
  msg._clientState = state;
  for (int i=0; i<_players.Size(); i++)
  {
    NetworkPlayerInfo &player = _players[i];
    if (player.dpid != dpnid && player.clientState >= NCSConnected)
      SendMsg(player.dpid, &msg, NMFGuaranteed);
  }

  switch (state)
  {
  case NCSLoggedIn:
    if (_serverState >= NSSAssigningRoles)
    {
      // send mission info
      SendMissionInfo(info->dpid);
      info->missionFileValid = false;
      info->paramFileValid = GetMissionParameters().GetLength() == 0;
    }
    if (_serverState == NSSPlaying)
    {
      // if game is already in progress, send how long is played already
      NetworkCommandMessage msg;
      msg._type = NCMTMissionTimeElapsed;
      int timeElapsed = GlobalTickCount() - _missionHeader._start;
      msg._content.Write(&timeElapsed, sizeof(timeElapsed));
      SendMsg(dpnid, &msg, NMFGuaranteed);
    }
    if (_serverState == NSSSelectingMission)
    {
      // new clients should get mission voting echo as necessary, so that they can see voting
      MissionVotingEcho();
    }
    break;
  case NCSRoleAssigned:
    if (_serverState >= NSSSendingMission)
    {
      // send mission
      SendMissionFile(dpnid);
    }
    if (_serverState >= NSSLoadingGame)
    {
      info->_jipProcessed = true;
      // load game
      DoAssert(info->clientState == NCSMissionReceived);

      // send the info about network objects need to be created
      MissionStatsMessage stats;
      stats._toCreate = stats._toUpdate = _objects.Size();
      SendMsg(dpnid, &stats, NMFGuaranteed);

      // create all game objects
//RptF("*** Sending world state ***");
//RptF("1. all except AI objects");
      // 1. all except AI objects
      for (int i=0; i<_objects.Size(); i++)
      {
        const NetworkObjectInfo *info = _objects[i];
        NetworkMessageType type = info->create.type;
        if
        (
          type == NMTCreateAICenter || type == NMTCreateAIGroup || type == NMTCreateAISubgroup ||
          type == NMTCreateAIBrain || type == NMTCreateAIUnit || type == NMTCreateAIAgent ||
          type == NMTCreateCommand
        )
          continue;
        // ClientInfo is not the full featured network object, so it should be handeled different way
        if ( type == NMTNone && info->current[NMCUpdatePosition].type == NMTUpdateClientInfoDpid )
          continue;
        DoAssert(info->create.message);
#if 0
        if (!info->create.message)
        {
          LogF(" i=%d, info->create.type=%d message=%x", i, info->create.type, info->create.message.GetRef());
        }
        static NetworkId debugNetworkId;
        if (debugNetworkId==info->id)
        {
          _asm nop;
        }
#endif
        NetworkComponent::SendMsg(dpnid, info->create.message, type, NMFGuaranteed);
//RptF("  %s: %d:%d", cc_cast(GetMsgTypeName(type)), info->id.creator, info->id.id);
      }
      // 2. AICenter objects
//RptF("2. AICenter objects");
      for (int i=0; i<_objects.Size(); i++)
      {
        const NetworkObjectInfo *info = _objects[i];
        NetworkMessageType type = info->create.type;
        if (type != NMTCreateAICenter) continue;
        DoAssert(info->create.message);
        NetworkComponent::SendMsg(dpnid, info->create.message, type, NMFGuaranteed);
//RptF("  %s: %d:%d", cc_cast(GetMsgTypeName(type)), info->id.creator, info->id.id);
      }
      // 3. AIGroup objects
//RptF("3. AIGroup objects");
      for (int i=0; i<_objects.Size(); i++)
      {
        const NetworkObjectInfo *info = _objects[i];
        NetworkMessageType type = info->create.type;
        if (type != NMTCreateAIGroup) continue;
        DoAssert(info->create.message);
        NetworkComponent::SendMsg(dpnid, info->create.message, type, NMFGuaranteed);
//RptF("  %s: %d:%d", cc_cast(GetMsgTypeName(type)), info->id.creator, info->id.id);
      }
      // 4. AISubgroup objects
//RptF("4. AISubgroup objects");
      for (int i=0; i<_objects.Size(); i++)
      {
        const NetworkObjectInfo *info = _objects[i];
        NetworkMessageType type = info->create.type;
        if (type != NMTCreateAISubgroup) continue;
        DoAssert(info->create.message);
        NetworkComponent::SendMsg(dpnid, info->create.message, type, NMFGuaranteed);
//RptF("  %s: %d:%d", cc_cast(GetMsgTypeName(type)), info->id.creator, info->id.id);
      }
      // 5. AIUnit objects
//RptF("5. AIUnit objects");
      for (int i=0; i<_objects.Size(); i++)
      {
        const NetworkObjectInfo *info = _objects[i];
        NetworkMessageType type = info->create.type;
        if (type != NMTCreateAIBrain && type != NMTCreateAIUnit && type != NMTCreateAIAgent) continue;
        DoAssert(info->create.message);
        // Send copy of message - message itself can changed during sending
        Ref<NetworkMessage> copy = ::CreateNetworkMessage(type);
        NetworkMessageFormatBase *format = GetFormat(type);
        CopyMsg(copy, info->create.message, format);
        NetworkComponent::SendMsg(dpnid, copy, type, NMFGuaranteed);
//RptF("  %s: %d:%d", cc_cast(GetMsgTypeName(type)), info->id.creator, info->id.id);
      }
      // 6. Command objects
      //RptF("6. Command objects");
      for (int i=0; i<_objects.Size(); i++)
      {
        const NetworkObjectInfo *info = _objects[i];
        NetworkMessageType type = info->create.type;
        if (type != NMTCreateCommand) continue;
        if (type != NMTNone) continue;
        DoAssert(info->create.message);
        NetworkComponent::SendMsg(dpnid, info->create.message, type, NMFGuaranteed);
        //RptF("  %s: %d:%d", cc_cast(GetMsgTypeName(type)), info->id.creator, info->id.id);
      }
//RptF("*** World state sent ***");
      // update all game objects
      for (int i=0; i<_objects.Size(); i++)
      {
        const NetworkObjectInfo *info = _objects[i];
        for (int cls=NMCUpdateFirst; cls<NMCUpdateN; cls++)
        {
          if (info->current[cls].message)
          {
            if (info->current[cls].type==NMTUpdateClientInfoDpid) continue;
            NetworkComponent::SendMsg(dpnid, info->current[cls].message, info->current[cls].type, NMFGuaranteed);
          }
        }
      }
      // send delayed messages
      for (int i=0; i<_initMessages.Size(); i++)
      {
        if (_initMessages[i].msg)
          NetworkComponent::SendMsg(dpnid, _initMessages[i].msg, _initMessages[i].type, NMFGuaranteed);
      }
//      CheckInitMessages(_initMessages, "after NetworkComponent::SendMsg");
    }
    // TODO: process after mission is loaded
    if (_serverState >= NSSLoadingGame)
    {
      info->_jip = true;
      //LogF("_jip=true dpnid=%d setting in %s on line %d\n", dpnid, __FILE__, __LINE__);
      SetClientState(dpnid, NCSGameLoaded);
      
      if (_serverState < NSSPlaying)
      {
        // can select player now - game is not running yet
        DoAssert(_parent->GetClient());
        _parent->GetClient()->JIPSelectPlayer(dpnid); // mission must be loaded on client
        info->_jip = false; // do not process later
        //LogF("_jip=false dpnid=%d setting in %s on line %d\n", dpnid, __FILE__, __LINE__);
      }
    }
    else
    {
      info->_jip = false;
      //LogF("_jip=false dpnid=%d setting in %s on line %d\n", dpnid, __FILE__, __LINE__);
    }
    break;
  case NCSMissionReceived:
    if (dpnid == BOT_CLIENT && _serverState < NSSLoadingGame)
    {
      // mission is transferred, can load game
      SetServerState(NSSLoadingGame);
    }
    break;
  case NCSGameLoaded:
    if (dpnid == BOT_CLIENT && _serverState < NSSBriefing && !_missionWasLoadedFromSave)
    {
      // game is loaded, can switch to briefing
      GWeaponsPool.Import(ParamEntryVal(ExtParsMission)); // create pool of available weapons
      SetServerState(NSSBriefing);
    }
    if (_missionWasLoadedFromSave) 
    {
      info->_jip=true; // JIP is forced for all players when game is Loaded from Saved file. (included BOT_CLIENT!)
      //LogF("_jip=true dpnid=%d FORCED AFTER LOAD in %s on line %d\n", dpnid, __FILE__, __LINE__);
    }
    if (info->_jip && _serverState < NSSPlaying)
    {
      // can select player now - game is not running yet
      DoAssert(_parent->GetClient());
      _parent->GetClient()->JIPSelectPlayer(dpnid); // mission must be loaded on client
      info->_jip = false; // do not process later
      //LogF("_jip=false dpnid=%d setting in %s on line %d\n", dpnid, __FILE__, __LINE__);
    }
    break;
  case NCSBriefingRead:
    if (info && info->_jip)
    {
      // select player
      DoAssert(_parent->GetClient());
      _parent->GetClient()->JIPSelectPlayer(dpnid); // mission must be loaded on client
    }
/*
    {
      // ask player about world file CRC
      RString worldFile = GetWorldName(Glob.header.worldname);
      PerformFileIntegrityCheck(worldFile,dpnid);
    }
*/
    if (UseSigVer2Full)
    { // clients should both send the bankscount info and be level 2 asks aware
      if (    info->hashes.bankInfoTimeout==0 //waiting is not pending
          && (info->hashes.banksCount<0 || !info->hashes.iKnowLevel2) )
      {
        info->hashes.bankInfoTimeout = GlobalTickCount() + 10000; //10 seconds
      }
    }
    if (info && oldState != NCSBriefingRead && !GNetworkPlayerConnected.GetNil())
    {
      GameState *state = GWorld->GetGameState();
      if (state)
      {
        GameVarSpace vars(false);
        state->BeginContext(&vars);
        state->VarSetLocal("_id", (float)info->dpid, true);
        PlayerIdentity *identity = FindIdentity(info->dpid);
        RString uid = identity ? identity->id : RString();
        state->VarSetLocal("_uid", uid, true);
        state->VarSetLocal("_name", info->name, true);

#if USE_PRECOMPILATION
        if (GNetworkPlayerConnected.GetType() == GameCode)
        {
          GameDataCode *code = static_cast<GameDataCode *>(GNetworkPlayerConnected.GetData());
          if (code->IsCompiled() && code->GetCode().Size() > 0)
            state->Evaluate(code->GetString(), code->GetCode(), GameState::EvalContext::_default, GWorld->GetMissionNamespace()); // mission namespace
        }
        else
#endif
        if (GNetworkPlayerConnected.GetType() == GameString)
        {
          // make sure string is not destructed while being evaluated
          RString code = GNetworkPlayerConnected;
          if (code.GetLength() > 0)
            state->EvaluateMultiple(code, GameState::EvalContext::_default, GWorld->GetMissionNamespace()); // mission namespace
        }
        state->EndContext();
      }
    }
    // create statistics for the player on this client
    {
      TargetSide side = TWest;
      int role = -1;
      for (int i=0; i<_playerRoles.Size(); i++)
      {
        if (_playerRoles[i].player == dpnid)
        {
          role = i;
          side = _playerRoles[i].side;
        }
      }
      if (role!=-1)
      {
        GStats._mission.FindRow(info->name, side, dpnid, role, true);
      }
    }
    if (info) info->joined = Glob.time;

    { // send also current clock
      NetworkCommandMessage msg;
      Glob.clock.Write(msg._content);
      msg._type = NCMTMissionDate;
      SendMsg(dpnid, &msg, NMFGuaranteed);
    }

    // We need to set the Glob.time properly on client after MP Load
    if (IsMissionLoadedFromSave() && dpnid!=BOT_CLIENT)
    {
      // if game is already in progress (which is always true after MP Load), send how long is played already
      NetworkCommandMessage msg;
      msg._type = NCMTMissionTimeElapsed;
      int timeElapsed = Glob.time.toInt();
      msg._content.Write(&timeElapsed, sizeof(timeElapsed));
      SendMsg(dpnid, &msg, NMFGuaranteed);
    }
    break;
  case NCSGameFinished:
    // disconnected from game
    if (_serverState == NSSPlaying)
    {
      NetworkId playerPerson;
      NetworkPlayerInfo *info = GetPlayerInfo(dpnid);
      if (info) playerPerson = info->person;
      DisconnectPlayer(dpnid, playerPerson);
    }
    break;
  case NCSMissionAsked:
    if (IsDedicatedBotClient(dpnid)) break;
    // disconnected from loading / briefing
    if (_serverState >= NSSLoadingGame)
    {
      NetworkId playerPerson;
      NetworkPlayerInfo *info = GetPlayerInfo(dpnid);
      if (info) playerPerson = info->person;
      DisconnectPlayer(dpnid, playerPerson);
    }
    if (_serverState >= NSSAssigningRoles && oldState < NCSMissionAsked)
    {
#if _VBS3 && _EXT_CTRL
      // Only use automatic roles in AAR playback. Causes 
      // too many problems with multiplayer startup screen with 150 players, 4 groups
      if(GAAR.IsLoaded())
      {
#endif
      // automatic roles assignment
      // changed - try to do some balancing
      if (!FindPlayerRole(dpnid))
      {
        // not assigned yet
        NetworkPlayerInfo *info = GetPlayerInfo(dpnid);
        RString plName;
        if (info) plName = info->name;
        int free = SelectRole(plName, info ? info->dpid : 0);
        if (free >= 0)
        {
          // assign
          PlayerRole &role = _playerRoles[free];
          role.player = dpnid;

          // create message
          NetworkMessageType type = role.GetNMType(NMCCreate);
          Ref<NetworkMessage> msgSend = ::CreateNetworkMessage(type);
          msgSend->time = Glob.time;
          NetworkMessageContext ctxSend(msgSend, this, TO_SERVER, MSG_SEND);

          NetworkMessagePlayerRole *message = (NetworkMessagePlayerRole *)msgSend.GetRef();
          message->_index = free;
          role.TransferMsg(ctxSend);

          // send message
          for (int i=0; i<_players.Size(); i++)
          {
            NetworkPlayerInfo &player = _players[i];
            if (player.clientState < NCSConnected) continue;
            NetworkComponent::SendMsg(player.dpid, msgSend, type, NMFGuaranteed);
          }
        }
      }
#if _VBS3 && _EXT_CTRL
      }
#endif
    }
    break;
  }

  if (info && oldState == NCSBriefingRead && state != NCSBriefingRead && !GNetworkPlayerDisconnected.GetNil())
  {
    GameState *state = GWorld->GetGameState();
    if (state)
    {
      GameVarSpace vars(false);
      state->BeginContext(&vars);
      state->VarSetLocal("_id", (float)info->dpid, true);
      PlayerIdentity *identity = FindIdentity(info->dpid);
      RString uid = identity ? identity->id : RString();
      state->VarSetLocal("_uid", uid, true);
      state->VarSetLocal("_name", info->name, true);
#if USE_PRECOMPILATION
      if (GNetworkPlayerDisconnected.GetType() == GameCode)
      {
        GameDataCode *code = static_cast<GameDataCode *>(GNetworkPlayerDisconnected.GetData());
        if (code->IsCompiled() && code->GetCode().Size() > 0)
          state->Evaluate(code->GetString(), code->GetCode(), GameState::EvalContext::_default, GWorld->GetMissionNamespace()); // mission namespace
      }
      else
#endif
      if (GNetworkPlayerDisconnected.GetType() == GameString)
      {
        // make sure string is not destructed while being evaluated
        RString code = GNetworkPlayerDisconnected;
        if (code.GetLength() > 0)
          state->EvaluateMultiple(code, GameState::EvalContext::_default, GWorld->GetMissionNamespace()); // mission namespace
      }
      state->EndContext();
    }
  }
}

void NetworkServer::KickAllPlayers()
{
  for (int i=0; i<_players.Size(); i++)
  {
    NetworkPlayerInfo &player = _players[i];
    if (player.dpid!=BOT_CLIENT)
    {
      KickOff(player.dpid,KOROther, "Game Loaded from MP Save, please reconnect!");
    }
  }
}

int NetworkServer::SelectRole(RString name, int dpid) const
{
  if (dpid==BOT_CLIENT)
  { // if there is role with PRFForced flag set, we need to use this role
    for (int i=0; i<_playerRoles.Size(); i++)
    {
      if ( _playerRoles[i].flags&PRFForced ) return i;
    }
  }
  if (!name.IsEmpty())
  { //try to assign the role from possible load
    for (int i=0; i<_role2name.Size(); i++)
    {
      if (_role2name[i]._name==name)
      {
        const PlayerRole *role = GetPlayerRole(_role2name[i]._roleIndex);
        if ( role->player == AI_PLAYER && role->lifeState!=LifeStateDead && !(role->flags&PRFForced) )
        { //is empty and available
          return _role2name[i]._roleIndex;
        }
        break; //the role specified in save is already not empty 
      }
    }
  }
  const int nSides = 4;
  AUTO_STATIC_ARRAY(int, westEmpty, 32);
  AUTO_STATIC_ARRAY(int, eastEmpty, 32);
  AUTO_STATIC_ARRAY(int, guerEmpty, 32);
  AUTO_STATIC_ARRAY(int, civlEmpty, 32);
  StaticArrayAuto<int> *sideEmpty[nSides] = {&westEmpty, &eastEmpty, &guerEmpty, &civlEmpty};
  int sideFull[nSides] = {0, 0, 0, 0};
  int totalEmpty = 0;
  for (int i=0; i<NPlayerRoles(); i++)
  {
    const PlayerRole *role = GetPlayerRole(i);
    Assert(role);
    TargetSide side = role->side;
    if (side == TAmbientLife) side = TCivilian;
    Assert(side >= 0 && side < nSides);
    if (side >= 0 && side < nSides)
    {
      if (role->player == AI_PLAYER && role->lifeState!=LifeStateDead)
      {
        sideEmpty[side]->Add(i);
        totalEmpty++;
      }
      else
      {
        sideFull[side]++;
      }
    }
    else
      RptF("Unrecognized PlayerRole target side: %d, index: %d", (int)role->side, i);
  }

  if (totalEmpty > 0)
  {
    // select side with empty slots and minimum of filled slots
    int side = -1;
    int minFull = INT_MAX;
    for (int i=0; i<nSides; i++)
    {
      if (sideEmpty[i]->Size() == 0) continue; // no empty slots
      if (sideFull[i] < minFull)
      {
        side = i;
        minFull = sideFull[i];
      }
    }
    Assert(side >= 0);
    return sideEmpty[side]->Get(0);
  }

  return -1;
}

NetworkClientState NetworkServer::GetClientState(int dpid)
{
  NetworkPlayerInfo *info = GetPlayerInfo(dpid);
  if (info) return info->clientState;
  else return NCSNone;
}

void NetworkServer::SetClientState(int dpnid, NetworkClientState state)
{
// RptF("*** Server: Set client state");

  NetworkPlayerInfo *info = GetPlayerInfo(dpnid);
  Assert(info);
  if (info)
  {
    //RptF("    player %s, state %s -> %s", (const char *)info->name, (const char *)FindEnumName(info->clientState), (const char *)FindEnumName(state));
    info->clientState = state;
    if (state == NCSRoleAssigned && _serverState < NSSLoadingGame) 
    {
      //LogF("_jip=false dpnid=%d setting in %s on line %d\n", dpnid, __FILE__, __LINE__);
      info->_jip = false;
  }
  }

  PlayerIdentity *identity = FindIdentity(dpnid);
  if (identity)
  {    
#if _USE_BATTL_EYE_SERVER
#if _ENABLE_GAMESPY && _VERIFY_CLIENT_KEY
    if (state == NCSBriefingRead && identity->clientState != NCSBriefingRead)
    {
      const char *hash = gcd_getkeyhash(GetGameID(), identity->dpnid);
      sockaddr_in address;
      _server->GetClientAddress(identity->dpnid, address);
      _beIntegration.AddPlayer(identity->dpnid, address.sin_addr.s_addr, address.sin_port, identity->name, (void *)hash, strlen(hash));
    }
    if (identity->clientState == NCSBriefingRead && state != NCSBriefingRead)
    {
      _beIntegration.RemovePlayer(identity->dpnid);
    }
#endif
#endif
    
    identity->clientState = state;
  }
  
  ClientStateMessage msg1;
  msg1._clientState = state;
  SendMsg(dpnid, &msg1, NMFGuaranteed);

  PlayerClientStateMessage msg;
  msg._dpnid = dpnid;
  msg._clientState = state;
  for (int i=0; i<_players.Size(); i++)
  {
    NetworkPlayerInfo &player = _players[i];
    if (player.dpid != dpnid && player.clientState >= NCSConnected)
      SendMsg(player.dpid, &msg, NMFGuaranteed);
  }
}

RString NetworkServer::GetPlayerName(int dpid)
{
  NetworkPlayerInfo *info = GetPlayerInfo(dpid);
  Assert(info);
  if (info) return info->name;
  else return "Unknown player";
}

Vector3 NetworkServer::GetCameraPosition(int dpid)
{
  NetworkPlayerInfo *info = GetPlayerInfo(dpid);
  Assert(info);
  if (info) return info->cameraPosition;
  else return VZero;
}

NetworkObject *NetworkServer::GetObject(NetworkId &id)
{
  Fail("Server: GetObject is not implemented");
  return NULL;
}

//! Subsidiary structure for sorting messages by error
struct UpdateObjectInfo
{
  Ref<DelayedMessageInfo> msg;
  //! Object info
  NetworkObjectInfo *oInfo;
  //! Player object info
  NetworkPlayerObjectInfo *poInfo;
  //! Message class type
  NetworkMessageClass cls;
  //! Error (difference), distance adjusted
  float error;
  //! Error (difference)
  float errorNoCoef;
  //! Critical update
  bool criticalUpdate;
  
  UpdateObjectInfo()
  {
    oInfo = NULL;
    poInfo = NULL;
  }
};
TypeIsMovableZeroed(UpdateObjectInfo)

//! Compare player object infos by error
int CmpPlayerObjects
(
  const UpdateObjectInfo *info1,
  const UpdateObjectInfo *info2
)
{
  float diff = info2->error - info1->error;
  return sign(diff);
}

void NetworkServer::KickOff(int dpnid, KickOffReason reason, const char *reasonStr)
{
  RString format;
  switch (reason)
  {
    case KORBattlEye: format = Format("Player %%s kicked off by BattlEye: %s", reasonStr); break;
    case KORKick: format = LocalizeString(IDS_MP_KICKED); break;
    case KORBan: format = LocalizeString(IDS_MP_BANNED); break;
  }
  PlayerIdentity *id = FindIdentity(dpnid);
  if (id)
  {
    if (id->destroy)
    {
      LogF("%s: KickOff in progress, new request ignored",(const char *)id->name);
      return;
    }
    if (format.GetLength() > 0)
    {
      RString message = Format(format,(const char *)id->name);
      RefArray<NetworkObject> dummy;
      GNetworkManager.Chat(CCGlobal,NULL,dummy,message);
      GChatList.Add(CCGlobal, NULL, message, false, true);
    }
    id->destroy = true;
    id->destroyTime = Glob.uiTime + 15; // destroy player after 15 seconds if no system message arrive
  }
  // mark player kickoff is initiated
  for (int i=0; i<_players.Size(); i++)
  {
    if (_players[i].dpid==dpnid) _players[i].kickedOff = true;
  }
  NetTerminationReason netReason = NTROther;
  switch (reason)
  {
    case KORKick: netReason = NTRKicked; break;
    case KORBan: netReason = NTRBanned; break;
    case KORFade: netReason = NTROther; break;
    case KORAddon: netReason = NTRMissingAddon; break;
    case KORBadCDKey: netReason = NTRBadCDKey; break;
    case KORCDKeyInUse: netReason = NTRCDKeyInUse; break;
    case KORBattlEye: netReason = NTRBattlEye; break;
  }
  _server->KickOff(dpnid,netReason,reasonStr);
}

void NetworkServer::Ban(int dpnid)
{
  // add to ban list
  const PlayerIdentity *identity = FindIdentity(dpnid);
  if (!identity) return;
  _banListLocal.AddUnique(identity->id);

  // save ban list
  RString banList = GetUserDirectory() + RString("ban.txt");
  SaveBanList(banList, _banListLocal);

  // kick off
  _server->KickOff(dpnid,NTRBanned);
}

#if LOG_ERRORS
//! diagnostic logs of error (difference) between messages
/*
static void LogError
(
  NetworkMessageType type,
  int dpid1, NetworkMessage *msg1,
  int dpid2, NetworkMessage *msg2
)
{
  if (type < 0 || type >= NMTN) return;
  NetworkMessageFormat *format = GMsgFormats[type];

  float dt = msg1->time - msg2->time;
  float error = (
    msg1->CalculateErrorStatic(msg2) + 
    msg1->CalculateErrorDynamic(msg2) + CalculateErrorTime(type, msg1, msg2)
  );
  
  if (error > 0)
    DiagLogF
    (
      "    - %s: %g * %g = %g", (const char *)format->GetItem(i).name,
      coef, value, error
    );
  }
}
*/
#endif

#if _ENABLE_REPORT
  #define LOG_QUEUE_PROGRESS 0
#endif

/*!
\patch_internal 1.01 Date 6/26/2001 by Ondra. Added hacked version player kickoff.
*/
void NetworkServer::CheckFadeOut()
{
  if (_serverState == NSSPlaying)
  {
    for (int i=0; i<_identities.Size(); i++)
    {
      PlayerIdentity &identity = _identities[i];
/*
//testing only
      static volatile bool doIt=false;
      if (doIt)
      {
        if (identity.dpnid!=2)
          KickOff(identity.dpnid,KORBattlEye, "Hello boy!");
      }
*/
      if (Glob.uiTime>=identity.kickOffTime)
      {
        switch (identity.kickOffState)
        {
          case KOWait:
            {
              // transmit explanation
              identity.kickOffState = KOWarning;
              identity.kickOffTime = Glob.uiTime;
              RString string = LocalizeString(IDS_FADE_AWAY);
              RString senderName = identity.GetName();
              RefArray<NetworkObject> dummy;
              GNetworkManager.Chat(CCGlobal,senderName,dummy,string);
              GChatList.Add(CCGlobal, senderName, string, false, true);
            }
            break;
//((EXE_CRC_CHECK
          case KOExe:
//))EXE_CRC_CHECK
          case KOWarning:
            if (Glob.uiTime>identity.kickOffTime+15)
            {
//((EXE_CRC_CHECK
              bool wasExe = identity.kickOffState==KOExe;
//))EXE_CRC_CHECK
              identity.kickOffState = KODone;
              KickOff(identity.dpnid,KORFade);
//((EXE_CRC_CHECK
              if (wasExe)
              {
                RString format = LocalizeString(IDS_MP_VALIDERROR_1);

                RString message = Format(format, cc_cast(identity.GetName()));
                RefArray<NetworkObject> dummy;
                GNetworkManager.Chat(CCGlobal,"",dummy,message);
                GChatList.Add(CCGlobal, NULL, message, false, true);
#if _ENABLE_DEDICATED_SERVER
                ConsoleF("%s", cc_cast(message));
#endif
              }
//))EXE_CRC_CHECK
              UpdateMatchmakingDescription(false);
            }
            break;
          default:
            Fail("KickOffState!");
          case KODone:
            break;
        }
      }
    }
  }
}

/*!
\patch_internal 1.50 Date 4/8/2002 by Jirka
- Fixed: No limits for server - bot client communication.
\patch 1.53 Date 4/23/2002 by Jirka
- Fixed: Bandwidth estimation on dedicated server
*/
void NetworkServer::EstimateBandwidth(NetworkPlayerInfo &pInfo, int nPlayers, int &nMsgMax, int &nBytesMax)
{
  if (pInfo.dpid == BOT_CLIENT)
  {
    nBytesMax = INT_MAX;
    nMsgMax = INT_MAX;

    pInfo._ping.Sample(0);
    pInfo._bandwidth.Sample(INT_MAX);
  }
  else
  {
    int nMsg = 0, nBytes = 0, nMsgG = 0, nBytesG = 0;

    // calculate quotas
    _server->GetSendQueueInfo(pInfo.dpid, nMsg, nBytes, nMsgG, nBytesG);
    if (nMsgG > 0)
    {
      nMsg += nMsgG;
      nBytes += nBytesG;
    }

    int minBandwidth;
//{ DEDICATED SERVER SUPPORT
#if _ENABLE_DEDICATED_SERVER
    if (IsDedicatedServer())
    {
      nMsgMax = DSMaxMsgSend;
      minBandwidth = DSMinBandwidth;
    }
    else
#endif
//}
    {
      nMsgMax = MaxMsgSend;
      minBandwidth = MinBandwidth;
    }
    int bandwidth = 100; // add 800 bps as a basic bandwidth
  #if _ENABLE_MP
    int latencyMS = 0, throughputBPS = 0;
    if (_server->GetConnectionInfo(pInfo.dpid, latencyMS, throughputBPS))
    {
      // nBytesMax += 1.25 * throughputBPS;
      bandwidth += throughputBPS;
      bandwidth += throughputBPS >> 2; // throughputBPS / 4
    }
  #endif
    // assume server upload bandwidth is at least MinBandwidth
    if (nPlayers > 0)
    {
      // 8 is conversion from bps to Bps
      int minBandwidthPerPlayer = minBandwidth/(8*nPlayers);
      
      int lowLevelMaxBandwidth = INT_MAX;
      _server->GetConnectionLimits(lowLevelMaxBandwidth);
      // min limit must never be higher than low-level max limit
      saturateMin(minBandwidthPerPlayer, lowLevelMaxBandwidth);
      saturate(bandwidth, minBandwidthPerPlayer, MaxBandwidth/8);
    }

    // float frmDur = GEngine->GetAvgFrameDuration();
    // LogF("  avgFrame: %.3f", frmDur);
    nBytesMax = toInt(0.001 * GEngine->GetAvgFrameDuration() * bandwidth);
  
    nBytesMax -= nBytes;
    nMsgMax -= nMsg;

    {
      // statistics
      int latencyMSRaw = 0, throughputBPSRaw = 0;
    #if _ENABLE_MP
      _server->GetConnectionInfoRaw(pInfo.dpid, latencyMSRaw, throughputBPSRaw);
    #endif
      pInfo._ping.Sample(latencyMSRaw);
      pInfo._bandwidth.Sample(throughputBPSRaw);
    }

    // write statistics
#if _ENABLE_CHEATS
    if (outputLogs) LogF("Server to %s: Limit (%d, %d)", (const char *)pInfo.name, nBytesMax, nMsgMax);
#endif
  }
}

int NetworkServer::GetBandwidthEstimation(int dpnid, int nPlayers)
{
  int bandwidth;
  if (dpnid == BOT_CLIENT)
  {
    bandwidth = INT_MAX;
  }
  else
  {
    int minBandwidth;
#if _ENABLE_DEDICATED_SERVER
    if (IsDedicatedServer()) minBandwidth = DSMinBandwidth;
    else
#endif
    {
      minBandwidth = MinBandwidth;
    }
    bandwidth = 100; // add 800 bps as a basic bandwidth
#if _ENABLE_MP
    int latencyMS = 0, throughputBPS = 0;
    if (_server->GetConnectionInfo(dpnid, latencyMS, throughputBPS))
    {
      // nBytesMax += 1.25 * throughputBPS;
      bandwidth += throughputBPS;
      bandwidth += throughputBPS >> 2; // throughputBPS / 4
    }
#endif
    // assume server upload bandwidth is at least MinBandwidth
    if (nPlayers > 0)
    {
      // 8 is conversion from bps to Bps
      int minBandwidthPerPlayer = minBandwidth/(8*nPlayers);

      int lowLevelMaxBandwidth = INT_MAX;
      _server->GetConnectionLimits(lowLevelMaxBandwidth);
      // min limit must never be higher than low-level max limit
      saturateMin(minBandwidthPerPlayer, lowLevelMaxBandwidth);
      saturate(bandwidth, minBandwidthPerPlayer, MaxBandwidth/8);
    }
  }
  return bandwidth;
}

float NetworkServer::CalculateErrorCoef(const NetworkPlayerInfo &pInfo, const NetworkObjectInfo &oInfo)
{
  const float errorCoefMyVehicle = 2.0f;

  const NetworkCurrentInfo &update = oInfo.current[NMCUpdateGeneric];
  if (update.message && !pInfo.person.IsNull())
  {
    if (IsUpdateTransport(update.type))
    {
      // check if player is not inside given vehicle
      NetworkMessageUpdateTransport *msg = static_cast<NetworkMessageUpdateTransport *>(update.message.GetRef());
      if (msg)
      {
#if _VBS2
        if(msg->_UpdateCoef > 0.0f )
          return msg->_UpdateCoef;
#endif
        if (msg->_driver == pInfo.person) return errorCoefMyVehicle;
        for (int i=0; i<msg->_manCargo.Size(); i++)
        {
          if (msg->_manCargo[i] == pInfo.person) return errorCoefMyVehicle;
        }
      }
    }
    else if (update.type == NMTUpdateTurret)
    {
      // check if player is not inside given turret
      NetworkMessageUpdateTurret *msg = static_cast<NetworkMessageUpdateTurret *>(update.message.GetRef());
      if (msg)
      {
        if (msg->_gunner == pInfo.person) return errorCoefMyVehicle;
      }
    }
  }

  // find the last update of the object
  Time time = TIME_MIN;
  const NetworkCurrentInfo *best = NULL;
  for (int j=NMCUpdateFirst; j<NMCUpdateN; j++)
  {
    const NetworkCurrentInfo &info = oInfo.current[j];
    const NetworkMessage *msg = info.message;
    if (msg)
    {
      if (msg->time > time)
      {
        best = &info;
        time = msg->time; 
      }
    }
  }
  if (!best) return -1;

  // retrieve position of the object
  Vector3 bestPosition(-FLT_MAX, -FLT_MAX, -FLT_MAX);
  {
    NetworkMessageContext ctx(best->message, this, best->from, MSG_RECEIVE);

    PREPARE_TRANSFER(NetworkObject)
    if (TRANSF_BASE(objectPosition, bestPosition) != TMOK) return -1;
  }

  // calculate the error coef by object's position
#if _VBS3
  return CalculateErrorCoef(best->type, pInfo.cameraPosition, bestPosition, pInfo.cameraDirection, pInfo.cameraFov);
#else
  return CalculateErrorCoef(best->type, pInfo.cameraPosition, bestPosition);
#endif
}

/*!
\patch_internal 1.12 Date 08/07/2001 by Jirka
- Added: error calculation diagnostics
\patch 5115 Date 1/8/2007 by Jirka
- Fixed: MP - fire effects sometimes did not transferred (especially for JIP players)
*/

#if DEBUG_GIVEN_NETWORK_ID
bool DebugMeBoy(NetworkId id);
#endif

void NetworkServer::CreateObjectsList(AutoArray<UpdateObjectInfo, MemAllocSA> &objects, NetworkPlayerInfo &pInfo)
{
  // calculate errors and place object in list sorted by error
  for (int j=0; j<_objects.Size(); j++)
  {
    NetworkObjectInfo &oInfo = *_objects[j];
    if (oInfo.owner == pInfo.dpid) continue; // no updates for local objects

    float errCoef = CalculateErrorCoef(pInfo, oInfo);
    if (errCoef < 0) continue;

    NetworkPlayerObjectInfo *poInfo = oInfo.CreatePlayerObjectInfo(pInfo.dpid);

    for (int j=NMCUpdateFirst; j<NMCUpdateN; j++)
    {
      NetworkCurrentInfo &info = oInfo.current[j];
      if (!info.message) continue;  // no current state

      NetworkUpdateInfo &update = poInfo->updates[j];
      
      float error;
      // error without distance coef
      float errorNoCoef;
      if (update.lastCreatedMsg)
      {
        // message on the way
        continue;
      }
      else if (!update.lastSentMsg)
      {
        errorNoCoef = error = FLT_MAX;
#if LOG_ERRORS
        DiagLogF
        (
          "Object %s %d:%d - first update to %d",
          NetworkMessageTypeNames[info.type], oInfo.id.creator, oInfo.id.id,
          pInfo.dpid
        );
#endif
      }
      else
      {
        NetworkMessage *msg1 = info.message;
        NetworkMessage *msg2 = update.lastSentMsg;

        if (!update.errorValid) 
        {
          update.errorStatic = msg1->CalculateErrorStatic(msg2);
          update.errorValid = true;
        }
        float dt = msg1->time - msg2->time;
        float errorDynamic = msg1->CalculateErrorDynamic(msg2, dt);
        errorNoCoef = error = update.errorStatic + errorDynamic + CalculateErrorTime(info.type, msg1, msg2);

#if LOG_ERRORS
        if (!(error * errCoef < LogErrorLimit)) // handle NaN well
        {
          DiagLogF
          (
            "Object %s %d:%d - update to %d",
            NetworkMessageTypeNames[info.type], oInfo.id.creator, oInfo.id.id,
            pInfo.dpid
          );
          DiagLogF("  - error %.3f = stat %.3f + dyn %.3f", error, update.errorStatic, errorDynamic);
          DiagLogF("  - error %.3f * %.3f = %.3f", errCoef, error, errCoef * error);
          if (pInfo.cameraPosition[1]!=-FLT_MAX)
          {
            DiagLogF
            (
              "  - camera [%.0f, %.0f, %.0f]",
              pInfo.cameraPosition[0], pInfo.cameraPosition[1], pInfo.cameraPosition[2]
            );
          }

          msg1->CalculateErrorStatic(msg2);
          msg1->CalculateErrorDynamic(msg2, dt);
          CalculateErrorTime(info.type, msg1, msg2);
        }
#endif
        error *= errCoef;
      }

#if DEBUG_GIVEN_NETWORK_ID
      if (DebugMeBoy(oInfo.id) && j==NMCUpdatePosition)
      {
        float msgTime = update.lastSentMsg ? update.lastSentMsg->time.toFloat() : 0; 
        LogF("*** DebugMeBoy: {%d:%d}, Error: %.2f, lastMsgTime: %.3f", oInfo.id.creator, oInfo.id.id, error, msgTime);
      }
#endif

      // note: following condition seems to be identical to error>MinErrorToSend
      // but it is different when error is NaN
      if (!(error <= MinErrorToSend))
      {
        int index = objects.Add();
        // objects[index].pInfo = &pInfo;
        objects[index].oInfo = &oInfo;
        objects[index].poInfo = poInfo;
        objects[index].cls = (NetworkMessageClass)j;
        objects[index].error = error;
        objects[index].errorNoCoef = errorNoCoef;
        objects[index].criticalUpdate = false;

        #if _DEBUG
          #define NOP __asm {nop}
          switch (info.type)
          {
            case NMTUpdateAIUnit: NOP; break;
            case NMTUpdateMan: NOP; break;
            case NMTUpdatePositionMan: NOP; break;
            case NMTUpdateVehicleAI: NOP; break;
            case NMTUpdateDamageVehicleAI: NOP; break;
            case NMTUpdateDamageVehicle: NOP; break;
            case NMTUpdateObject: NOP; break;
            case NMTUpdateDamageObject: NOP; break;
          }
          if (update.lastSentMsg)
          {
            // tracing opportunity
            // place breakpoint depending on message type
            switch (info.type)
            {
              case NMTUpdateAIUnit: NOP; break;
              case NMTUpdateMan: NOP; break;
              case NMTUpdatePositionMan: NOP; break;
              case NMTUpdateVehicleAI: NOP; break;
              case NMTUpdateDamageVehicleAI: NOP; break;
              case NMTUpdateDamageVehicle: NOP; break;
              case NMTUpdateObject: NOP; break;
              case NMTUpdateDamageObject: NOP; break;
            }
            NetworkMessage *msg1 = info.message;
            NetworkMessage *msg2 = update.lastSentMsg;
            float errorStatic = msg1->CalculateErrorStatic(msg2);
            float dt = msg1->time - msg2->time;
            float errorDynamic = msg1->CalculateErrorDynamic(msg2, dt);
            (void)errorStatic;
            (void)errorDynamic;
            NOP;
          }
          
        #endif
      }
    }
  }
  
  for (int i=0; i<pInfo._delayedMessages.Size(); i++)
  {
    DelayedMessageInfo *item = pInfo._delayedMessages[i];

    float ageCoef = 1-(Glob.time-item->timeReceived)*item->invMaxAge;
    float errorCoef = 1;

	  if (pInfo.cameraPosition[0] != -FLT_MAX && item->pos[0]!=-FLT_MAX)
	  {
	    // both positions defined
	    // coef = limit / distance (maximum is 1)
	    const float limit = 20.0f;
	    float dist2 = item->pos.Distance2(pInfo.cameraPosition);
	    if (dist2 > Square(limit))
	    {
		    errorCoef = Square(limit) / dist2;
	    }
	  }
    
    float error = item->error*ageCoef;
    // if age is negative or zero, condition will certainly fail
    if (errorCoef*error<MinErrorToSend)
    {
      pInfo._delayedMessages.Delete(i--);
      continue;
    }
    
    UpdateObjectInfo &oInfo = objects.Append();
    oInfo.errorNoCoef = error;
    oInfo.error = errorCoef*error;
    oInfo.msg = item;
    oInfo.cls = NMCUpdateN;
    oInfo.criticalUpdate = false;
  }

  // sort objects by errors
  QSort(objects.Data(), objects.Size(), CmpPlayerObjects);
}

void NetworkServer::OnSendStarted(DWORD msgID, const NetworkMessageQueueItem &item)
{
  DoAssert(msgID != MSGID_REPLACE);
  if (item.msg->objectUpdateInfo)
  {
#if LOG_SEND_PROCESS
    NetworkObject *netwObj = GNetworkManager.GetObject(item.msg->objectServerInfo->id);
    LogF("  - update info %x updated (msgID=%x, %s, (%d:%d) - %s)", item.msg->objectUpdateInfo, msgID, 
      NetworkMessageTypeNames[item.type], item.msg->objectServerInfo->id.creator,item.msg->objectServerInfo->id.id,
      netwObj ? cc_cast(netwObj->GetDebugName()) : "UNKNOWN"
    );
#endif
    if (item.msg->objectUpdateInfo->lastCreatedMsg != item.msg)
    {
      RptF("Last created message is bad:");
      RptF("  sending message: %x (type %s), id = %x", item.msg.GetRef(), cc_cast(GetMsgTypeName(item.type)), msgID);
      RptF("  attached object info: %x", item.msg->objectUpdateInfo);
      RptF("  last created message: %x, id = %x", item.msg->objectUpdateInfo->lastCreatedMsg.GetRef(), item.msg->objectUpdateInfo->lastCreatedMsgId);
      RptF("  last sent message: %x", item.msg->objectUpdateInfo->lastSentMsg.GetRef());

      item.msg->objectUpdateInfo->lastCreatedMsg = item.msg;
    }
    DoAssert(item.msg->objectUpdateInfo->lastCreatedMsgId == MSGID_REPLACE);
    item.msg->objectUpdateInfo->lastCreatedMsgId = msgID;
    if (msgID == 0xffffffff)
    {
      item.msg->objectUpdateInfo->lastCreatedMsg = NULL;
      item.msg->objectUpdateInfo->lastCreatedMsgTime = 0;
      // no pending message may exist to this - it had id=MSGID_REPLACE
      //DoAssert(CheckIntegrityOfPendingMessages());
    }
    else
    {
      AddPendingMessage
      (
        msgID,item.msg->objectServerInfo,
        item.msg->objectUpdateInfo,item.msg->objectPlayerInfo
      );
      //DoAssert(CheckIntegrityOfPendingMessages());
    }
  }
}


NetworkMessageType NetworkServer::NetPendingMessage::GetNMT() const
{
  for (int i=0; i<NMCUpdateN; i++)
  {
    if ( (player->updates+i)==update )
      return info->current[i].type;
  }
  return NMTNone;
}

/*!
\patch 1.27 Date 10/09/2001 by Ondra
- Fixed: Ping times on clients were not updated when mission was not running.
\patch_internal 1.47 Date 3/11/2002 by Ondra
- Fixed: Improved latency on kill.
\patch_internal 1.48 Date 3/12/2002 by Ondra
- Fixed: Latency on kill further improved.
Bug introduced in 1.47 beta causing increased lag of movement fixed.
\patch_internal 1.49 Date 3/14/2002 by Ondra
- Fixed: Latence handling code reverted to version 1.45.
\patch 1.53 Date 4/23/2002 by Ondex
- Fixed: Kill/dammage message have now higher priority - this should improve latency on kill.
*/
void NetworkServer::SendMessages()
{
#if _ENABLE_CHEATS
  // remove statistics info
  static AutoArray<int> texts;
  for (int i=0; i<texts.Size(); i++)
  {
    if (texts[i] >= 0) GEngine->RemoveText(texts[i]);
  }
  texts.Resize(0);
#endif

#if _ENABLE_CHEATS
  #define LOG_DESYNC 1
#endif

#if LOG_DESYNC
  static DWORD time = 0;
  DWORD now = GlobalTickCount();
  bool logDesync = false;
  if (now >= time + 1000)
  {
    logDesync = true;
    time = now;
  }
#endif

  int nPlayers = _identities.Size() - 1;
//{ DEDICATED SERVER SUPPORT
#if _ENABLE_DEDICATED_SERVER
  if (IsDedicatedServer()) nPlayers++;
#endif
//}
#define DIAGNOZE_BANDWIDTH 0
#if DIAGNOZE_BANDWIDTH
  bool doDiagnozeBandwidth = false;
  static DWORD nextDiagTime = GetTickCount()+5000;
  DWORD curTime = GetTickCount();
  if (curTime>=nextDiagTime)
  {
    nextDiagTime=curTime+5000;
    doDiagnozeBandwidth = true;
  }
#endif

  for (int i=0; i<_players.Size(); i++)
  {
    NetworkPlayerInfo &pInfo = _players[i];

    int nMsg = 0;
    int nBytes = 0;
    int nMsgMax, nBytesMax;

    //LogF("  NsEstB %d, %d Bytes", nMsgMax, nBytesMax);
    EstimateBandwidth(pInfo, nPlayers, nMsgMax, nBytesMax);
#if DIAGNOZE_BANDWIDTH
    int diagGMsg=0, diagGBytes=0;  //state of nMsgMax and nBytesMax after the guaranteed messages are sent
    int diagCMsg=0, diagCBytes=0;  //state ... after critical updates are sent
#endif    
    //LogF("  NsEstB %d, %d Bytes", nMsgMax, nBytesMax);

  #if LOG_QUEUE_PROGRESS
    if (pInfo._messageQueue.Size() > 0)
    {
      LogF
      (
        "%s: nBytesMax %d, nBytes %d, queue size %d",
        (const char *)pInfo.name,
        nBytesMax, nBytes, pInfo._messageQueue.Size()
      );
    }
  #endif

    // send enqueued guaranteed messages
    int maxSize = MaxSizeGuaranteed;

    const int nMsgMaxGuaranteed = nMsgMax;
    const int nBytesMaxGuaranteed = nBytesMax;

    while (pInfo._messageQueue.Size() > 0)
    {
      if (nMsg >= nMsgMaxGuaranteed || nBytes >= nBytesMaxGuaranteed)
      {
      #if LOG_QUEUE_PROGRESS
        int qBytes = 0;
        for (int i=0; i<pInfo._messageQueue.Size(); i++)
        {
          qBytes += pInfo._messageQueue[i].msg->size;
        }
        LogF
        (
          "  -- queue left %d (%d B)",pInfo._messageQueue.Size(),qBytes
        );
      #endif
        break;
      }

      NetworkMessage *msg = pInfo._messageQueue[0].msg;
      int size = msg->size;
      if (size >= maxSize)
      {
        //DWORD dwMsgId =
        SendMsgRemote(pInfo.dpid, msg, pInfo._messageQueue[0].type, NMFGuaranteed|NMFStatsAlreadyDone);
        pInfo._messageQueue.Delete(0);
        nMsg++;
        nBytes += size;
      }
      else
      {
        int totalSize = 0;
        int i;
        for (i=0; i<pInfo._messageQueue.Size(); i++)
        {
          NetworkMessage *msg = pInfo._messageQueue[i].msg;
          int size = msg->size;
          if (totalSize + size > maxSize) break;
          totalSize += size;
        }
        //DWORD dwMsgId =
        SendMsgQueue(pInfo.dpid, pInfo._messageQueue, 0, i, NMFGuaranteed);
        pInfo._messageQueue.Delete(0, i);
        nMsg++;
        nBytes += totalSize;
      }
    }

    #if LOG_QUEUE_PROGRESS
    if (nBytes>0)
    {
      LogF
      (
        "  -- nMsg %d (nBytes %d)",nMsg,nBytes
      );
      if (pInfo._messageQueue.Size()==0)
      {
        LogF("  ** Queue empty");
      }
    }
    #endif

    AUTO_STATIC_ARRAY(UpdateObjectInfo, objects, 256);

  #define ENABLE_CRITICAL_UPDATES 1

#if DIAGNOZE_BANDWIDTH
    diagGBytes = nBytes;
    diagGMsg = nMsg;
#endif

  #if !ENABLE_CRITICAL_UPDATES
    if (nMsg >= nMsgMax || nBytes >= nBytesMax)
    {
      #if _ENABLE_CHEATS
        if (outputLogs) LogF("  Server to %s: limit reached (guaranteed)", (const char *)pInfo.name);
      #endif
      goto DoNotSendUpdates;
    }
  #endif

    // update objects
    if (_serverState == NSSPlaying && pInfo.clientState == NCSBriefingRead)
    {
      // update errors of objects
      CreateObjectsList(objects, pInfo);

      // create and send critical updates     
    #if ENABLE_CRITICAL_UPDATES
      for (int i=0; i<objects.Size(); i++)
      {
        UpdateObjectInfo &info = objects[i];
        NetworkObjectInfo *oInfo = info.oInfo;
        NetworkMessageClass cls = info.cls;

        float error = info.error;
        float errorNoCoef = info.errorNoCoef;

#if DEBUG_GIVEN_NETWORK_ID
        if (oInfo && DebugMeBoy(oInfo->id))
        {
          float times[NMCUpdateN];
          NetworkPlayerObjectInfo *poInfo = oInfo->CreatePlayerObjectInfo(pInfo.dpid);
          for (int j=0; j<NMCUpdateN; j++) times[j] = poInfo->updates[j].lastSentMsg ? poInfo->updates[j].lastSentMsg->time.toFloat() : 0;
          LogF("*** DebugMeBoy: {%d:%d} has error %.2f, position in _objects = %d, lastMsgSent: %.02f, %.02f, %.02f", 
            oInfo->id.creator, oInfo->id.id, error, i, times[NMCUpdateGeneric], times[NMCUpdatePosition], times[NMCUpdateDamage]);
        }
#endif

        // any message about dammage that transmit change
        // that could mean dead should be considered high priority
        // errorNoCoef - error with no regard to distance
        // error - distance adjusted error
        if
        (
          cls != NMCUpdateDamage ||
          error < ERR_COEF_STRUCTURE * 0.01 || errorNoCoef < ERR_COEF_STRUCTURE
        ) continue;

        Assert(oInfo);
        info.criticalUpdate = true;
        int bytes = UpdateObject(&pInfo, oInfo, cls, NMFGuaranteed | NMFHighPriority);


      #if _ENABLE_CHEATS
        if (outputLogs)
        {
          LogF
          (
            "  Server to %s: Object %d:%d updated - size %d bytes, critical",
            (const char *)pInfo.name, info.oInfo->id.creator, info.oInfo->id.id, bytes
          );
        }
      #endif

        if (bytes >= 0)
        {
          // no error
          nMsg++;
          nBytes += bytes;
        }
      }

#if DIAGNOZE_BANDWIDTH
      diagCBytes = nBytes;
      diagCMsg = nMsg;
#endif

      if (nMsg >= nMsgMax || nBytes >= nBytesMax)
      {
      #if _ENABLE_CHEATS
        if (outputLogs) LogF("  Server to %s: limit reached (guaranteed)", (const char *)pInfo.name);
      #endif
        goto DoNotSendUpdates;
      }
    #endif // ENABLE_CRITICAL_UPDATES
    }

DoNotSendUpdates:
#if DIAGNOZE_BANDWIDTH
    if (doDiagnozeBandwidth)
    { // test whether there are any pending messages for this player
      // if so, write a short diagnostic
      DWORD tickCount = ::GetTickCount();
      for (int k=0; k<_pendingMessages.Size(); k++)
      {
        NetworkUpdateInfo *uInfo = _pendingMessages[k].update;
        if (_pendingMessages[k].player->player==pInfo.dpid
          &&
          uInfo->lastCreatedMsg
          &&
          tickCount > uInfo->lastCreatedMsgTime + 2000 // pending message is pending for longer time already
          )
        {
          int nCMsg = diagCMsg ? (diagCMsg - diagGMsg) : diagCMsg;
          int nCBytes = diagCBytes ? (diagCBytes - diagGBytes) : diagCBytes;
          RptF("Player \"%s\" bandwidth total(%d,%d), guar(%d,%d), crit(%d,%d), rest(%d,%d)",
            cc_cast(pInfo.name), nMsgMax, nBytesMax, 
            diagGMsg, diagGBytes,
            nCMsg, nCBytes,
            nMsgMax-nMsg, nBytesMax-nBytes
            );
          NetworkMessageType pendingType = _pendingMessages[k].GetNMT();
          RptF("    pending message: %s", NetworkMessageTypeNames[pendingType]);
          break;
        }
      }
    }
#endif
    // send nonguaranteed enqueued messages
    maxSize = MaxSizeNonguaranteed;
    int next = 0; // index of next object to update
    bool empty = false;
    while (true)
    {
      // enforce send last message
      if (!empty || pInfo._messageQueueNonGuaranteed.Size() <= 0)
      {
        if (nMsg >= nMsgMax || nBytes >= nBytesMax) break;
      }

      if (pInfo._messageQueueNonGuaranteed.Size() <= 0)
      {
        empty = true;
        // starving, try to create further message
        if (!PrepareNextUpdate(pInfo, objects, next)) break;
        // local communication - message was already sent
        if (pInfo.dpid == BOT_CLIENT)
        {
          DoAssert(pInfo._messageQueueNonGuaranteed.Size() == 0);
          nMsg++;
          continue;
        }
        DoAssert(pInfo._messageQueueNonGuaranteed.Size() > 0);
      }

      NetworkMessageQueueItem &item = pInfo._messageQueueNonGuaranteed[0];
      int size = item.msg->size;
      // enforce send last message
      if (size >= maxSize || nMsg >= nMsgMax || nBytes >= nBytesMax)
      {
        DWORD dwMsgId = SendMsgRemote(pInfo.dpid, item.msg, item.type, NMFStatsAlreadyDone);
#if LOG_SEND_PROCESS
        LogF("Server: Message %x sent", dwMsgId);
#endif
        OnSendStarted(dwMsgId,item);
        pInfo._messageQueueNonGuaranteed.Delete(0);
        nMsg++;
        nBytes += size;
      }
      else
      {
        int totalSize = 0;
        int i;
        for (i=0; true; i++)
        {
          if (i >= pInfo._messageQueueNonGuaranteed.Size())
          {
            empty = true;
            // starving, try to create further message
            if (!PrepareNextUpdate(pInfo, objects, next)) break;
            DoAssert(pInfo.dpid != BOT_CLIENT);
            DoAssert(i < pInfo._messageQueueNonGuaranteed.Size());
          }

          NetworkMessageQueueItem &item = pInfo._messageQueueNonGuaranteed[i];
          int size = item.msg->size;
          if (totalSize + size > maxSize) break;
          totalSize += size;
        }
        DWORD dwMsgId = SendMsgQueue(pInfo.dpid, pInfo._messageQueueNonGuaranteed, 0, i, NMFNone);
#if LOG_SEND_PROCESS
        LogF("Server: Message %x sent", dwMsgId);
#endif
        for (int j=0; j<i; j++)
        {
          NetworkMessageQueueItem &item = pInfo._messageQueueNonGuaranteed[j];
          OnSendStarted(dwMsgId,item);
        }
        pInfo._messageQueueNonGuaranteed.Delete(0, i);
        nMsg++;
        nBytes += totalSize;
      }
    }
    DoAssert(!empty || pInfo._messageQueueNonGuaranteed.Size() == 0);

    // search for maximum error not sent
    //float sumError = 0; // messages not sent total error
    float sumError = 0;
    for (int i=next; i<objects.Size(); i++)
    {
      // FLT_MAX means message is new. We have no idea how serious visual impact such message has
      // we ignore it for purpose of error calculation
      if (objects[i].error < FLT_MAX)
      {
        sumError = objects[i].error;
        break;
      }
     }

    // avoid flt_max error
    saturateMin(sumError,1e9);
    pInfo._desync.Sample(sumError,1e-3);

#if 0 //LOG_DESYNC
    if (logDesync) ConsoleF("%s: desync %8.2f", (const char *)pInfo.name, sumError); // debug log - need not to be localized
#endif
    

    // show server diagnostics
#if _ENABLE_CHEATS

    if (outputDiags == ODMsgQueue+NOutputDiags)
    {
      int count = pInfo._messageQueue.Size(), size = 0;
      for (int i=0; i<count; i++) size += pInfo._messageQueue[i].msg->size;

      RString output = Format("%s: HLWait%3d(%5dB)", (const char *)pInfo.name, count, size);
      texts.Add(GEngine->ShowTextF(1000, output));
      texts.Add(GEngine->ShowTextF(1000, _server->GetStatistics(pInfo.dpid)));
      
      StatisticsById stats;
      for (int i=0; i<count; i++)
      {
        const NetworkMessageQueueItem &item = pInfo._messageQueue[i];
        stats.Count(safe_cast<int>(item.type),item.msg->size);
      }
      stats.SortData(false);
      int maxLines = 10;
      for (int i=0; i<stats.Size() && i<maxLines; i++)
      {
        const StatisticsById::Item &item = stats.Get(i);
        RString name = GetMsgTypeName((NetworkMessageType)item.id._id1);
        texts.Add(GEngine->ShowTextF(1000, "  %s: %d", cc_cast(name), item.count));
      }
    }
#endif
  }
}

bool NetworkServer::PrepareNextUpdate(NetworkPlayerInfo &pInfo, AutoArray<UpdateObjectInfo, MemAllocSA> &objects, int &next)
{
  while (next < objects.Size())
  {
    UpdateObjectInfo &info = objects[next++];
    if (info.criticalUpdate) continue;

/*
    #if _ENABLE_REPORT
    int oldCount = pInfo._messageQueueNonGuaranteed.Size();
    #endif
*/
    NetworkObjectInfo *oInfo = info.oInfo;
    if (oInfo)
    {
      NetworkMessageClass cls = info.cls;
      int bytes = UpdateObject(&pInfo, oInfo, cls, NMFNone);
      if (bytes < 0) continue;
  #if _ENABLE_CHEATS
      if (outputLogs) LogF("  Server to %s: Object updated - size %d bytes", (const char *)pInfo.name, bytes);
  #endif
    }
    else
    {
      //NetworkComponent::SendMsg(pInfo.dpid, info.msg, info.type, NMFGuaranteed);
      // non-guaranteed enqueues into the same queue as UpdateObject
      NetworkComponent::SendMsg(pInfo.dpid, info.msg->msg, info.msg->type, NMFNone);
      // set error to zero so that message is deleted
      info.msg->error = 0;
  #if _ENABLE_CHEATS
      if (outputLogs) LogF("  Server to %s: Delayed message sent", (const char *)pInfo.name);
  #endif
    }
/*
    #if _ENABLE_REPORT
    if (oldCount == pInfo._messageQueueNonGuaranteed.Size())
    {
      Fail("No message added during NetworkServer::PrepareNextUpdate");
      continue;
    }
    #endif
*/

    return true;
  }
  return false;
}

#if _VBS3
float NetworkServer::CalculateErrorCoef
(
  NetworkMessageType type, Vector3Par cameraPosition, Vector3Val position,
  Vector3Par cameraDirection, float cameraFov
)
{
  switch (type)
  {
  case NMTUpdateDamageObject:
  case NMTUpdateObject:
    return Object::CalculateErrorCoef(position, cameraPosition,cameraDirection,cameraFov);  
  case NMTUpdatePositionVehicle:
  case NMTUpdateVehicle:
    return Vehicle::CalculateErrorCoef(position, cameraPosition,cameraDirection,cameraFov);
  case NMTUpdateDetector:
    return Detector::CalculateErrorCoef(position, cameraPosition,cameraDirection,cameraFov);
  case NMTUpdateFlag:
    return FlagCarrier::CalculateErrorCoef(position, cameraPosition,cameraDirection,cameraFov);
  case NMTUpdateShot:
    return Shot::CalculateErrorCoef(position, cameraPosition,cameraDirection,cameraFov);
  case NMTUpdateMine:
    return Mine::CalculateErrorCoef(position, cameraPosition,cameraDirection,cameraFov);
  case NMTUpdateDamageVehicleAI:
  case NMTUpdateVehicleAI:
    return EntityAI::CalculateErrorCoef(position, cameraPosition,cameraDirection,cameraFov);
  case NMTUpdateVehicleBrain:
    return Person::CalculateErrorCoef(position, cameraPosition,cameraDirection,cameraFov);
  case NMTUpdateTransport:
    return Transport::CalculateErrorCoef(position, cameraPosition,cameraDirection,cameraFov);
  case NMTUpdatePositionMan:
  case NMTUpdateMan:
    return Man::CalculateErrorCoef(position, cameraPosition,cameraDirection,cameraFov);
  case NMTUpdateTankOrCar:
    return TankOrCar::CalculateErrorCoef(position, cameraPosition,cameraDirection,cameraFov);
  case NMTUpdatePositionTank:
  case NMTUpdateTank:
    return TankWithAI::CalculateErrorCoef(position, cameraPosition,cameraDirection,cameraFov);
  case NMTUpdatePositionMotorcycle:
  case NMTUpdateMotorcycle:
    return Motorcycle::CalculateErrorCoef(position, cameraPosition,cameraDirection,cameraFov);
  case NMTUpdatePositionCar:
  case NMTUpdateCar:
    return Car::CalculateErrorCoef(position, cameraPosition,cameraDirection,cameraFov);
  case NMTUpdatePositionAirplane:
  case NMTUpdateAirplane:
    return AirplaneAuto::CalculateErrorCoef(position, cameraPosition,cameraDirection,cameraFov);
  case NMTUpdatePositionHelicopter:
  case NMTUpdateHelicopter:
    return HelicopterAuto::CalculateErrorCoef(position, cameraPosition,cameraDirection,cameraFov);
  case NMTUpdateParachute:
    return ParachuteAuto::CalculateErrorCoef(position, cameraPosition,cameraDirection,cameraFov);
  case NMTUpdatePositionShip:
  case NMTUpdateShip:
    return ShipWithAI::CalculateErrorCoef(position, cameraPosition,cameraDirection,cameraFov);
  case NMTUpdatePositionDoor:
  case NMTUpdateDoor:
    return Door::CalculateErrorCoef(position, cameraPosition,cameraDirection,cameraFov);
  case NMTUpdatePositionSeagull:
  case NMTUpdateSeagull:
    return SeaGullAuto::CalculateErrorCoef(position, cameraPosition,cameraDirection,cameraFov);
  case NMTUpdateAICenter:
    return AICenter::CalculateErrorCoef(position, cameraPosition);
  case NMTUpdateAIGroup:
    return AIGroup::CalculateErrorCoef(position, cameraPosition,cameraDirection,cameraFov);
  case NMTUpdateAISubgroup:
    return AISubgroup::CalculateErrorCoef(position, cameraPosition,cameraDirection,cameraFov);
  case NMTUpdateAIBrain:
    return AIBrain::CalculateErrorCoef(position, cameraPosition,cameraDirection,cameraFov);
  case NMTUpdateAIUnit:
    return AIUnit::CalculateErrorCoef(position, cameraPosition,cameraDirection,cameraFov);
#if _ENABLE_INDEPENDENT_AGENTS
  case NMTUpdateAIAgent:
    return AIAgent::CalculateErrorCoef(position, cameraPosition,cameraDirection,cameraFov);
  case NMTUpdateAITeam:
    return AITeam::CalculateErrorCoef(position, cameraPosition,cameraDirection,cameraFov);
#endif
  case NMTUpdateCommand:
    return Command::CalculateErrorCoef(position, cameraPosition,cameraDirection,cameraFov);
  case NMTUpdateClientInfo:
    return ClientInfoObject::CalculateErrorCoef(position, cameraPosition);
  case NMTUpdateFireplace:
    return Fireplace::CalculateErrorCoef(position, cameraPosition,cameraDirection,cameraFov);
  case NMTUpdatePositionTurret:
  case NMTUpdateTurret:
    return Turret::CalculateErrorCoef(position, cameraPosition,cameraDirection,cameraFov);
  case NMTAIStatsMPRowUpdate:
    return AIStatsMPRow::CalculateErrorCoef(position, cameraPosition);
  case NMTUpdateClientCameraPosition:
    return ClientCameraPositionObject::CalculateErrorCoef(position, cameraPosition);
  default:
    RptF("Server: Unknown update message %d(%s)", (int)type, cc_cast(GetMsgTypeName(type)));
    return 0;
  }
}
#else
float NetworkServer::CalculateErrorCoef
(
  NetworkMessageType type, Vector3Par cameraPosition, Vector3Val position
)
{
  switch (type)
  {
  case NMTUpdateDamageObject:
  case NMTUpdateObject:
    return Object::CalculateErrorCoef(position, cameraPosition);
  case NMTUpdatePositionVehicle:
  case NMTUpdateDamageVehicle:
  case NMTUpdateVehicle:
    return Vehicle::CalculateErrorCoef(position, cameraPosition);
  case NMTUpdateDetector:
    return Detector::CalculateErrorCoef(position, cameraPosition);
  case NMTUpdateFlag:
    return FlagCarrier::CalculateErrorCoef(position, cameraPosition);
  case NMTUpdateShot:
    return Shot::CalculateErrorCoef(position, cameraPosition);
  case NMTUpdateMine:
    return Mine::CalculateErrorCoef(position, cameraPosition);
  case NMTUpdateDamageVehicleAI:
  case NMTUpdateVehicleAI:
    return EntityAI::CalculateErrorCoef(position, cameraPosition);
  case NMTUpdateVehicleBrain:
    return Person::CalculateErrorCoef(position, cameraPosition);
  case NMTUpdateTransport:
    return Transport::CalculateErrorCoef(position, cameraPosition);
  case NMTUpdatePositionMan:
  case NMTUpdateMan:
    return Man::CalculateErrorCoef(position, cameraPosition);
  case NMTUpdateTankOrCar:
    return TankOrCar::CalculateErrorCoef(position, cameraPosition);
  case NMTUpdatePositionTank:
  case NMTUpdateTank:
    return TankWithAI::CalculateErrorCoef(position, cameraPosition);
  case NMTUpdatePositionMotorcycle:
  case NMTUpdateMotorcycle:
    return Motorcycle::CalculateErrorCoef(position, cameraPosition);
  case NMTUpdatePositionCar:
  case NMTUpdateCar:
    return Car::CalculateErrorCoef(position, cameraPosition);
  case NMTUpdatePositionAirplane:
  case NMTUpdateAirplane:
    return AirplaneAuto::CalculateErrorCoef(position, cameraPosition);
  case NMTUpdatePositionHelicopter:
  case NMTUpdateHelicopter:
    return HelicopterAuto::CalculateErrorCoef(position, cameraPosition);
  case NMTUpdateParachute:
    return ParachuteAuto::CalculateErrorCoef(position, cameraPosition);
  case NMTUpdatePositionShip:
  case NMTUpdateShip:
    return ShipWithAI::CalculateErrorCoef(position, cameraPosition);
  case NMTUpdatePositionDoor:
  case NMTUpdateDoor:
    return Door::CalculateErrorCoef(position, cameraPosition);
  case NMTUpdatePositionSeagull:
  case NMTUpdateSeagull:
    return SeaGullAuto::CalculateErrorCoef(position, cameraPosition);
  case NMTUpdateAICenter:
    return AICenter::CalculateErrorCoef(position, cameraPosition);
  case NMTUpdateAIGroup:
    return AIGroup::CalculateErrorCoef(position, cameraPosition);
  case NMTUpdateAISubgroup:
    return AISubgroup::CalculateErrorCoef(position, cameraPosition);
  case NMTUpdateAIBrain:
    return AIBrain::CalculateErrorCoef(position, cameraPosition);
  case NMTUpdateAIUnit:
    return AIUnit::CalculateErrorCoef(position, cameraPosition);
#if _ENABLE_INDEPENDENT_AGENTS
  case NMTUpdateAIAgent:
    return AIAgent::CalculateErrorCoef(position, cameraPosition);
  case NMTUpdateAITeam:
    return AITeam::CalculateErrorCoef(position, cameraPosition);
#endif
  case NMTUpdateCommand:
    return Command::CalculateErrorCoef(position, cameraPosition);
  case NMTUpdateClientInfo:
  case NMTUpdateClientInfoDpid:
    return ClientInfoObject::CalculateErrorCoef(position, cameraPosition);
  case NMTUpdateFireplace:
    return Fireplace::CalculateErrorCoef(position, cameraPosition);
  case NMTUpdatePositionTurret:
  case NMTUpdateTurret:
    return Turret::CalculateErrorCoef(position, cameraPosition);
  case NMTAIStatsMPRowUpdate:
    return AIStatsMPRow::CalculateErrorCoef(position, cameraPosition);
  case NMTUpdateClientCameraPosition:
    return ClientCameraPositionObject::CalculateErrorCoef(position, cameraPosition);
  default:
    RptF("Server: Unknown update message %d(%s)", (int)type, cc_cast(GetMsgTypeName(type)));
    return 0;
  }
}
#endif

float NetworkServer::CalculateErrorTime
(
 NetworkMessageType type, NetworkMessage *msg1, NetworkMessage *msg2
)
{
  if (type < 0 || type >= NMTN)
  {
    RptF("Server: Bad message %d format", (int)type);
    return 0.0;
  }

  float errCoefTime;
  switch (type)
  {
  case NMTUpdatePositionVehicle:
  case NMTUpdatePositionMan:
  case NMTUpdatePositionTank:
  case NMTUpdatePositionCar:
  case NMTUpdatePositionAirplane:
  case NMTUpdatePositionHelicopter:
  case NMTUpdatePositionShip:
  case NMTUpdatePositionDoor:
  case NMTUpdatePositionSeagull:
  case NMTUpdatePositionMotorcycle:
    errCoefTime = ERR_COEF_TIME_POSITION;
    break;
  default:
    errCoefTime = ERR_COEF_TIME_GENERIC;
    break;
  }

  float dt = msg1->time - msg2->time;
  return errCoefTime * dt;
}

NetworkId NetworkServer::PersonToUnit(NetworkId &person)
{
  for (int i=0; i<_mapPersonUnit.Size(); i++)
  {
    PersonUnitPair &pair = _mapPersonUnit[i];
    if (pair.person == person) return pair.unit;
  }
  return NetworkId::Null();
}

NetworkId NetworkServer::UnitToPerson(NetworkId &unit)
{
  for (int i=0; i<_mapPersonUnit.Size(); i++)
  {
    PersonUnitPair &pair = _mapPersonUnit[i];
    if (pair.unit == unit) return pair.person;
  }
  return NetworkId::Null();
}

int NetworkServer::AddPersonUnitPair(NetworkId &person, NetworkId &unit)
{
  int index = _mapPersonUnit.Add();
  PersonUnitPair &pair = _mapPersonUnit[index];
  pair.person = person;
  pair.unit = unit;
  return index;
}

/*!
\patch_internal 5102 Date 12/14/2006 by Jirka
- Fixed: Starting of MP missions in Retail Demo was broken
*/

void NetworkServer::SendMissionFile(int dpnid)
{
#if _ENABLE_UNSIGNED_MISSIONS

// RptF("*** Server: Send mission file to %d", dpnid);

  bool found = false;
  bool foundParams = false;
  NetworkPlayerInfo *player = NULL;
  if (dpnid == 0)
  {
    for (int i=0; i<_players.Size(); i++)
    {
      NetworkPlayerInfo &info = _players[i];
      if (info.clientState != NCSRoleAssigned) continue;
#ifdef _XBOX
      if (!info.missionFileValid)
      {
        WarningMessage(
          "Player %s has different version of mission %s", (const char *)info.name, (const char *)_missionHeader._name.GetLocalizedValue()
        );
      }
#else
      if (info.dpid == BOT_CLIENT)
      {
        DoAssert(info.missionFileValid);
      }
      if (!info.missionFileValid) found = true;
#endif
      if (!info.paramFileValid) foundParams = true;
    }
  }
  else
  {
    DoAssert(dpnid != BOT_CLIENT);
    player = GetPlayerInfo(dpnid);
    if (!player) return;
    if (player->clientState != NCSRoleAssigned) return;
    if (!player->missionFileValid) found = true;
    if (!player->paramFileValid) foundParams = true;
  }

  int maxSegmentSize = 512 - 50;

  if (foundParams)
  {
    QIFStreamB f;
    RString src = GetMissionParameters();
#ifdef _XBOX
    RString dst = "."; // stored in memory
#else
    // char '$' is used as an indication of Local Settings directory (different on client than on server)
    RString dst = RString("$\\MPMissionsCache\\__cur_mp.") + GetExtensionWizardMission();
#endif
    f.AutoOpen(src);

    TransferFileMessage msg;
    msg._path = dst;
    msg._totSize = f.rest();
    msg._totSegments = (msg._totSize + maxSegmentSize - 1) / maxSegmentSize;
    msg._offset = 0;
    for (int i=0; i<msg._totSegments; i++)
    {
      msg._curSegment = i;
      int size = min(maxSegmentSize, msg._totSize - msg._offset);
      msg._data.Resize(size);
      f.read(msg._data.Data(),size);
      if (dpnid == 0)
      {
        for (int j=0; j<_players.Size(); j++)
        {
          NetworkPlayerInfo &info = _players[j];
          if (info.clientState == NCSRoleAssigned && !info.paramFileValid)
            SendMsg(info.dpid, &msg, NMFGuaranteed);
        }
      }
      else if (!player->paramFileValid)
        SendMsg(dpnid, &msg, NMFGuaranteed);

      msg._offset += size;
    }
  }

#ifndef _XBOX
  if (found)
  {
    QIFStreamB f;
    RString src = _missionBank;
    // FIX: transfer mission file always into tmp directory (do not rewrite original file)
    // char '$' is used as an indication of Local Settings directory (different on client than on server)
    RString dst = RString("$\\MPMissionsCache\\") + _missionHeader._fileName + RString(".pbo");
    f.AutoOpen(src);

    TransferMissionFileMessage msg;
    msg._path = dst;
    msg._totSize = f.rest();
    msg._totSegments = (msg._totSize + maxSegmentSize - 1) / maxSegmentSize;
    msg._offset = 0;
    for (int i=0; i<msg._totSegments; i++)
    {
      msg._curSegment = i;
      int size = min(maxSegmentSize, msg._totSize - msg._offset);
      msg._data.Resize(size);
      f.read(msg._data.Data(),size);
      if (dpnid == 0)
      {
        for (int j=0; j<_players.Size(); j++)
        {
          NetworkPlayerInfo &info = _players[j];
          if (info.clientState == NCSRoleAssigned && !info.missionFileValid)
            SendMsg(info.dpid, &msg, NMFGuaranteed);
        }
      }
      else if (!player->missionFileValid)
        SendMsg(dpnid, &msg, NMFGuaranteed);

      msg._offset += size;
    }
  }
#endif

  // do not send twice
  if (dpnid == 0)
  {
    for (int j=0; j<_players.Size(); j++)
    {
      NetworkPlayerInfo &info = _players[j];
// RptF("    player %s, has state %s", (const char *)info.name, (const char *)FindEnumName(info.clientState));
      if (info.clientState == NCSRoleAssigned)
      {
        info.missionFileValid = true;
        info.paramFileValid = true;
        SetClientState(info.dpid, NCSMissionReceived);
      }
    }
  }
  else
  {
    player->missionFileValid = true;
    player->paramFileValid = true;
    SetClientState(dpnid, NCSMissionReceived);
  }

#else

  bool foundParams = false;
  NetworkPlayerInfo *player = NULL;
  if (dpnid == 0)
  {
    for (int i=0; i<_players.Size(); i++)
    {
      NetworkPlayerInfo &info = _players[i];
      if (info.clientState != NCSRoleAssigned) continue;
      if (!info.paramFileValid) foundParams = true;
    }
  }
  else
  {
    DoAssert(dpnid != BOT_CLIENT);
    player = GetPlayerInfo(dpnid);
    if (!player) return;
    if (player->clientState != NCSRoleAssigned) return;
    if (!player->paramFileValid) foundParams = true;
  }

  int maxSegmentSize = 512 - 50;

  if (foundParams)
  {
    QIFStreamB f;
    RString src = GetMissionParameters();
  #ifdef _XBOX
    RString dst = "."; // stored in memory
  #else
    // char '$' is used as an indication of Local Settings directory (different on client than on server)
    RString dst = RString("$\\MPMissionsCache\\__cur_mp.") + GetExtensionWizardMission();
  #endif
    f.AutoOpen(src);

    TransferFileMessage msg;
    msg._path = dst;
    msg._totSize = f.rest();
    msg._totSegments = (msg._totSize + maxSegmentSize - 1) / maxSegmentSize;
    msg._offset = 0;
    for (int i=0; i<msg._totSegments; i++)
    {
      msg._curSegment = i;
      int size = min(maxSegmentSize, msg._totSize - msg._offset);
      msg._data.Resize(size);
      f.read(msg._data.Data(),size);
      if (dpnid == 0)
      {
        for (int j=0; j<_players.Size(); j++)
        {
          NetworkPlayerInfo &info = _players[j];
          if (info.clientState == NCSRoleAssigned && !info.paramFileValid)
            SendMsg(info.dpid, &msg, NMFGuaranteed);
        }
      }
      else if (!player->paramFileValid)
        SendMsg(dpnid, &msg, NMFGuaranteed);

      msg._offset += size;
    }
  }

  // do not send twice
  if (dpnid == 0)
  {
    for (int j=0; j<_players.Size(); j++)
    {
      NetworkPlayerInfo &info = _players[j];
      // RptF("    player %s, has state %s", (const char *)info.name, (const char *)FindEnumName(info.clientState));
      if (info.clientState == NCSRoleAssigned)
      {
        info.missionFileValid = true;
        info.paramFileValid = true;
        SetClientState(info.dpid, NCSMissionReceived);
      }
    }
  }
  else
  {
    player->missionFileValid = true;
    player->paramFileValid = true;
    SetClientState(dpnid, NCSMissionReceived);
  }

#endif // _ENABLE_UNSIGNED_MISSIONS
}

#if defined _XBOX && _XBOX_VER < 200
void ForceSignIn(RString account)
{
#if _XBOX_SECURE && _ENABLE_MP
  if (account.GetLength() > 0)
  {
    // try to sign in Xbox Live
    XONLINE_USER users[XONLINE_MAX_STORED_ONLINE_USERS];
    DWORD usersCount;
    int selected = -1;

    // HRESULT hr = XOnlineStartup(NULL);
    // if (SUCCEEDED(hr))
    {
      HRESULT hr = XOnlineGetUsers(users, &usersCount);
      if (!SUCCEEDED(hr)) usersCount = 0;
      for (DWORD i=0; i<usersCount; i++)
      {
        if (strcmp(users[i].szGamertag, account) == 0)
        {
          selected = i;
          break;
        }
      }
    }
    // XOnlineCleanup();  

    if (selected >= 0)
      GNetworkManager.SignIn(users[selected]);
  }
#endif // _XBOX_SECURE && _ENABLE_MP
}
#endif // defined _XBOX && _XBOX_VER < 200

//{ DEDICATED SERVER SUPPORT
#if _ENABLE_DEDICATED_SERVER

RString GetPidFileName ();
extern bool MyPidFile;

//! Create dedicated server
/*!
\param config name of server configuaration file
*/
bool CreateDedicatedServer(SessionType sessionType, RString config)
{
  if (GWorld->Options())
  {
    GWorld->Options()->DestroyHUD(-1);
  }

  RString GetLiveGamertag();
  RString gamertag = GetLiveGamertag();
  if (gamertag.GetLength()>0)
  {
    GNetworkManager.Init();
  }
  else
  {
    // create a system link dedicated server
    GNetworkManager.Init("", GetServerPort(), false);
  }

#if _GAMES_FOR_WINDOWS || defined _XBOX
  GWorld->CreateDSOptions();
#endif

  RString password("");
  int maxPlayers = MAX_PLAYERS_DS;

  GameDataNamespace globals(NULL, RString(), false); // TODO: parsing namespace if access to globals needed

  ParamFile serverCfg;
  serverCfg.Parse(config, NULL, NULL, &globals);

  ConstParamEntryPtr entry = serverCfg.FindEntry("password");
  if (entry) password = *entry;

  entry = serverCfg.FindEntry("maxPlayers");
  if (entry) maxPlayers = *entry;
  maxPlayers += 1; // dedicated server occupy single slot

  int port = GetServerPort();

  int rating = 0;
#if _XBOX_SECURE && _ENABLE_MP
  if (GNetworkManager.IsSignedIn() && !GNetworkManager.IsSystemLink())
  {
#if _XBOX_VER >= 200
    // Ranking is handled automatically for Ranked Matches
#else
    ULONGLONG ReadPlayerRating();
    rating = ReadPlayerRating();
#endif
  }
#endif

  GNetworkManager.CreateSession(sessionType, RString(), password, maxPlayers, false, port, true, config, rating);
  if (GNetworkManager.IsServer())
  {
    const char *imp = "Sockets";
    RString title = APP_NAME " Console version " APP_VERSION_TEXT " : port %d - %s";
    ConsoleTitle(title, GNetworkManager.GetServer()->GetSessionPort(), imp);
      // writting the pid of the server instance:
#ifndef _XBOX
    if ( GetPidFileName().GetLength() )
    {
      FILE *pidFp;
      if ( (pidFp = fopen((const char*)GetPidFileName(),"wt")) != NULL )
      {
        fprintf(pidFp,"%d\n",getpid());
        fclose(pidFp);
#ifdef _WIN32
#else
        fchmod(fileno(pidFp),(S_IRUSR|S_IWUSR|S_IRGRP|S_IROTH));
#endif
        MyPidFile = true;
      }
    }
#endif
    return true;
  }

  ConsoleTitle("Server creation failed : %d", port);
  return false;
}

#define ENUM_LOAD(type, prefix, name) \
  entry = _serverCfg.FindEntry(#name); \
  if (entry) _eventHandlers[prefix##name] = *entry;

/*!
\patch 1.24 Date 09/19/2001 by Jirka
- Added: parameters "motdInterval" and "motd" (for message of the day) added to dedicated server config
\patch 1.24 Date 09/21/2001 by Jirka
- Added: parameter "voteThreshold" added to dedicated server config
\patch 1.27 Date 10/16/2001 by Jirka
- Added: parameter "reportingIP" added to dedicated server config
to change / disable IP address of Query & Reporting master server.
 When empty, reporting is disabled.
 If entry missing, master.gamespy.com is used and server is advertised to GameSpy.
\patch 1.82 Date 8/27/2002 by Jirka
- Added: parameter "kickDuplicate" added to dedicated server config
\patch 5120 Date 1/18/2007 by Ondra
- New: parameter persistent in the server config to allow the mission to continue
  even when no players are connected
\patch 5180 Date 11/12/2007 by Jirka
- New: BattlEye protection integration
*/

bool NetworkServer::SetDedicated(RString config)
{
  _dedicated = true;

#if _XBOX_SECURE && _ENABLE_MP
  _nextStatsUpdate = GlobalTickCount() + 10 * 60 * 1000; // 10 min
#endif

  _serverCfg.Parse(config, NULL, NULL, &GParsingNamespace); // parsing namespace (persistent namespace needed for persistent ParamFile)
  _missionIndex = -1;

  _motdInterval = 5;
  ConstParamEntryPtr entry = _serverCfg.FindEntry("motdInterval");
  if (entry) _motdInterval = *entry;

  entry = _serverCfg.FindEntry("motd");
  if (entry)
  {
    for (int i=0; i<(*entry).GetSize(); i++) _motd.Add((*entry)[i]);
  }

#if _USE_BATTL_EYE_SERVER
  entry = _serverCfg.FindEntry("battlEye");
  if (!entry || (bool)*entry) //battlEye is on by default
  {
    if (!_beIntegration.Init())
    {
#if _ENABLE_DEDICATED_SERVER
      if (entry || (_beIntegration._initFailedReason!=BattlEye::BEReasonNotInstalled) )
      {
        ConsoleF("BattlEye initialization failed");
        return false;
      }
#endif
    }
  }
#endif

  _voteThreshold = 0.5;
  entry = _serverCfg.FindEntry("voteThreshold");
  if (entry) _voteThreshold = *entry;

  // empty string is default now
  // to disable reporting, use reportingIP = "noreport";
  entry = _serverCfg.FindEntry("reportingIP");
  if (entry) _reportingIP = *entry;

  entry = _serverCfg.FindEntry("kickDuplicate");
  if (entry) _kickDuplicate = *entry;

  entry = _serverCfg.FindEntry("equalModRequired");
  if (entry) _equalModRequired = *entry;

  entry = _serverCfg.FindEntry("verifySignatures");
  if (entry)
  {
    int verify = *entry;
    if (verify) SetVerifySignatures(verify);
  }

  entry = _serverCfg.FindEntry("requiredBuild");
  if (entry)
  {
    int buildNo = *entry;
    _buildNoRequired = buildNo;
  }
  else _buildNoRequired = 0;

  entry = _serverCfg.FindEntry("persistent");
  if (entry)
  {
    _persistent = *entry;
  }

  entry = _serverCfg.FindEntry("additionalSignedFiles");
  if (entry)
  {
    int n = entry->GetSize();
    _additionalSignedFiles.Realloc(n);
    _additionalSignedFiles.Resize(n);
    for (int i=0; i<n; i++)
    {
      _additionalSignedFiles[i] = (*entry)[i];
    }
  }

  _maxPlayersLimit = MAX_PLAYERS_DS;
  entry = _serverCfg.FindEntry("maxPlayers");
  if (entry) _maxPlayersLimit = *entry;

  entry = _serverCfg.FindEntry("timeStampFormat");
  if (entry && entry->IsTextValue())
  {
    RString timeStampFormat = *entry;
    if (!timeStampFormat.IsEmpty())
    {
      if (stricmp(timeStampFormat, "none")==0) GTimeStampFormat = TSFNone;
      else if (stricmp(timeStampFormat, "short")==0) GTimeStampFormat = TSFShort;
      else if (stricmp(timeStampFormat, "full")==0) GTimeStampFormat = TSFFull;
    }
  }

  // event handlers
  SERVER_EVENT_ENUM(ServerEvent, SE, ENUM_LOAD)

  return true;
}

void NetworkServer::Monitor(float interval)
{
  _monitorInterval = interval;
  _monitorFrames = 0;
  _monitorIn = 0;
  _monitorOut = 0;
  if (_monitorInterval > 0) _monitorNext = GlobalTickCount() + toInt(1000.0 * _monitorInterval);
  else _monitorInterval = UINT_MAX,_monitorNext = UINT_MAX;
}

void NetworkServer::OnMonitorIn(int size)
{
  if (_dedicated) _monitorIn += size;
}

#endif
//}

static bool LoadKey(const FileItem &file, AutoArray<DSKey> &keys)
{
  int index = keys.Add();
  keys[index].Load(file.path + file.filename);
  return false;
}

void NetworkServer::SetVerifySignatures(int reqVersion)
{
  _verifySignatures = true;
  _sigVerRequired = reqVersion;

  // load all accepted keys
  // core keys
  ForMaskedFile("Keys\\", "*.bikey", LoadKey, _acceptedKeys);
#if _ENABLE_DEDICATED_SERVER
  if (!_dedicated)
#endif
  {
    // user keys
    RString GetUserDirectory();
    ForMaskedFile(GetUserDirectory() + RString("Keys\\"), "*.bikey", LoadKey, _acceptedKeys);
  }
  ForMaskedFile(RString("Expansion\\Keys\\"), "*.bikey", LoadKey, _acceptedKeys);    
  _acceptedKeys.Compact();
}

/*!
\patch 5164 Date 10/3/2007 by Jirka
- Fixed: Signature verification timeout significantly relaxed.
*/

//! verify signature of the single file
void NetworkServer::CheckSignature(int dpnid, int fileIndex, int level)
{
  NetworkPlayerInfo *info = GetPlayerInfo(dpnid);
  if (!info) return;

  // check if the same question is not pending
  if (info->signatureChecks.Pending(fileIndex))
    return;

  // send the question
  DataSignatureAskMessage ask;
  ask._index = fileIndex;
  ask._level = level;
  SendMsg(dpnid, &ask, NMFGuaranteed);

  // add the question to the list of pending questions
  info->signatureChecks.Add(fileIndex);
}

//! verify signature of the single file
void NetworkServer::CheckSignature(int dpnid, RString filename, RString prefix, int level)
{
  NetworkPlayerInfo *info = GetPlayerInfo(dpnid);
  if (!info) return;

  // check if the same question is not pending
  if ( info->signatureChecks.Pending(filename) )
    return;

  // send the question
  FileSignatureAskMessage ask;
  ask._file = filename;
  ask._level = level;
  SendMsg(dpnid, &ask, NMFGuaranteed);

  // add the question to the list of pending questions
  info->signatureChecks.Add(filename, prefix);
}

bool NetworkServer::MyHash(Temp<char> &hash, RString prefix, int sigVer, int level)
{
  // find the stored hash using prefix as key in bankHashes array
  int i=0;
  for (; i<GBankHashes.Size(); i++)
  {
    if (GBankHashes[i].prefix==prefix) break;
  }
  // when hash found, return it
  if (i<GBankHashes.Size())
  {
    BankHashes &hashes = GBankHashes[i];
    switch (sigVer)
    {
    case 1: 
      hash = hashes.ver1Hash;
      return true; 
      //break;
    case 2:
      {
        switch (level)
        {
        case 0:
        case 1:
          {
            hash = hashes.ver2lev0Hash;
            return true;
          }
          //break; //unreachable code
        case 2:
          {
            hash = hashes.ver2lev2Hash;
            return true;
          }
          //break; //unreachable code
        }
      }
    }
  }
  return false; //fail
}


//((EXE_CRC_CHECK
#if !_SUPER_RELEASE

void CompareExeInvestigation::Init(int dpid1, int dpid2)
{
  _stack.Resize(0);
  for (int addr=0x00401000; addr<0x00c01000; addr+=0x1000)
  {
    ExeSegmentInfo &info = _stack.Append();
    info.offset = addr;
    info.size = 0x1000;
  }
  _modified.Resize(0);
  _asked = false;
  _client1._dpid = dpid1;
  _client1._answered = false;
  _client2._dpid = dpid2;
  _client2._answered = false;
}

void CompareExeInvestigation::OnAnswered(int dpid, int answer)
{
  static const int minBlockSize = 1;

  // store the answer
  bool done = false;
  if (dpid == _client1._dpid)
  {
    _client1._answer = answer;
    _client1._answered = true;
    done = _client2._answered;
  }
  else if (dpid == _client2._dpid)
  {
    _client2._answer = answer;
    _client2._answered = true;
    done = _client1._answered;
  }
  if (!done) return;

  // answer from both clients arrived
  if (_client1._answer != _client2._answer)
  {
    ExeSegmentInfo &info = _stack[0];
    if (info.size <= minBlockSize)
    {
      // add to the list of modifications
      int index = -1;
      for (int i=0; i<_modified.Size(); i++)
      {
        if (_modified[i].offset >= info.offset)
        {
          index = i;
          if (info.offset + info.size == _modified[i].offset)
          {
            // join with the next block
            _modified[index].offset = info.offset;
            _modified[index].size += info.size;
          }
          else
          {
            // insert the new block
            _modified.Insert(index, info);
          }
        }
      }
      if (index < 0)
      {
        // append it
        index = _modified.Add(info);
      }
      // check if the new block can be merged with the previous one
      if (index > 0)
      {
        ExeSegmentInfo &block1 = _modified[index - 1];
        ExeSegmentInfo &block2 = _modified[index];
        if (block1.offset + block1.size == block2.offset)
        {
          block1.size += block2.size;
          _modified.Delete(index);
        }
      }
    }
    else
    {
      // store further investigations
      ExeSegmentInfo &add1 = _stack.Append();
      ExeSegmentInfo &add2 = _stack.Append();
      add1.offset = info.offset;
      add1.size = info.size / 2;
      add2.offset = info.offset + add1.size;
      add2.size = info.size - add1.size;
    }
  }

  // remove the handled question
  _stack.Delete(0);
  _asked = false;
  _client1._answered = false;
  _client2._answered = false;

  if (_stack.Size() == 0)
  {
    // investigation finished, output the results
    QOFStream out;
    out.openForAppend("cheats.txt");
    for (int i=0; i<_modified.Size(); i++)
    {
      RString line = Format("  0x%08x:0x%08x\r\n", _modified[i].offset, _modified[i].size);
      out << line;
    }
    out.close();
  }
}

#endif
//))EXE_CRC_CHECK

bool NetworkServer::GetClientAddress(int client, sockaddr_in &address)
{
  if (client == BOT_CLIENT)
    return _server->GetServerAddress(address);
  else
    return _server->GetClientAddress(client, address);
}

//////////////////////////////////////////////////////////////////////////
//
// Server scripting functions and event handlers processing
//

/*!
\patch 5126 Date 2/1/2007 by Jirka
- Added: MP - new server scripting function lock 
- Added: MP - new commands #lock, #unlock
*/

#if _ENABLE_DEDICATED_SERVER

DEFINE_ENUM(ServerEvent, SE, SERVER_EVENT_ENUM)

void NetworkServer::SetEventHandler(RString name, RString value)
{
  ServerEvent event = GetEnumValue<ServerEvent>(cc_cast(name));
  if (event != INT_MIN) _eventHandlers[event] = value;
}

bool NetworkServer::OnEvent(ServerEvent event, float par)
{
  RString handler = _eventHandlers[event];
  if (handler.GetLength() == 0) return false;

  GameValue value = GWorld->GetGameState()->CreateGameValue(GameArray);
  GameArrayType &arguments = value;
  arguments.Add(par);

  GameVarSpace local(false);
  _evaluator.BeginContext(&local);
  _evaluator.VarSetLocal("_this", arguments, true);
  _evaluator.Execute(handler, GameState::EvalContext::_default, &_globals); // server namespace
  _evaluator.EndContext();

  return true;
}

bool NetworkServer::OnEvent(ServerEvent event, float par1, float par2)
{
  RString handler = _eventHandlers[event];
  if (handler.GetLength() == 0) return false;

  GameValue value = GWorld->GetGameState()->CreateGameValue(GameArray);
  GameArrayType &arguments = value;
  arguments.Add(par1);
  arguments.Add(par2);

  GameVarSpace local(false);
  _evaluator.BeginContext(&local);
  _evaluator.VarSetLocal("_this", arguments, true);
  _evaluator.Execute(handler, GameState::EvalContext::_default, &_globals); // server namespace
  _evaluator.EndContext();

  return true;
}

bool NetworkServer::OnEvent(ServerEvent event, float par1, RString par2)
{
  RString handler = _eventHandlers[event];
  if (handler.GetLength() == 0) return false;

  GameValue value = GWorld->GetGameState()->CreateGameValue(GameArray);
  GameArrayType &arguments = value;
  arguments.Add(par1);
  arguments.Add(par2);

  GameVarSpace local(false);
  _evaluator.BeginContext(&local);
  _evaluator.VarSetLocal("_this", arguments, true);
  _evaluator.Execute(handler, GameState::EvalContext::_default, &_globals); // server namespace
  _evaluator.EndContext();

  return true;
}

#if _VBS3
// forward declaration of checksize and checktype in gamestate.cpp
bool CheckSize(const GameState *state, GameArrayType array, int size);
#else
inline bool CheckSize(const GameState *state, GameArrayType array, int size)
{
  if (array.Size() == size) return true;
  state->SetError(EvalDim, array.Size(), size);
  return false;
}
#endif

#if _VBS3
// forward declaration of checksize and checktype in gamestate.cpp
bool CheckType(const GameState *state, GameValuePar oper, const GameType &type);
#else
inline bool CheckType(const GameState *state, GameValuePar oper, const GameType &type)
{
  if (oper.GetType() == type) return true;
  state->TypeError(type, oper.GetType());
  return false;
}
#endif

GameValue ServerUsers(const GameState *state)
{
  GameValue value = state->CreateGameValue(GameArray);

  NetworkServer *server = GNetworkManager.GetServer();
  if (!server) return value;
  const AutoArray<PlayerIdentity> *identities = server->GetIdentities();
  if (!identities) return value;

  int n = identities->Size();
  GameArrayType &array = value;
  array.Realloc(n);
  array.Resize(n);
  for (int i=0; i<n; i++)
  {
    const PlayerIdentity &identity = (*identities)[i];
    array[i] = state->CreateGameValue(GameArray);
    GameArrayType &subarray = array[i];
    subarray.Realloc(2);
    subarray.Resize(2);
    subarray[0] = (float)identity.playerid;
    subarray[1] = identity.GetName();
  }

  return value;
}

GameValue ServerBan(const GameState *state, GameValuePar oper1)
{
  NetworkServer *server = GNetworkManager.GetServer();
  if (!server) return NOTHING;

  int playerId = toInt((float)oper1);
  const PlayerIdentity *identity = server->FindIdentityByPlayerId(playerId);
  if (!identity) return NOTHING;

  // add global ban list
  FindArray<RString> banList;
  LoadBanList("ban.txt", banList);
  GFileServerFunctions->FlushReadHandle("ban.txt");
  banList.AddUnique(identity->id);
  SaveBanList("ban.txt", banList);

  // kick off
  server->KickOff(identity->dpnid, KORBan);

  return NOTHING;
}

GameValue ServerKick(const GameState *state, GameValuePar oper1)
{
  NetworkServer *server = GNetworkManager.GetServer();
  if (!server) return NOTHING;

  int playerId = toInt((float)oper1);
  const PlayerIdentity *identity = server->FindIdentityByPlayerId(playerId);
  if (!identity) return NOTHING;

  server->KickOff(identity->dpnid, KORKick);

  return NOTHING;
}

GameValue ServerKickUID(const GameState *state, GameValuePar oper1)
{
  NetworkServer *server = GNetworkManager.GetServer();
  if (!server) return NOTHING;

  if (oper1.GetType()==GameString)
  {
    RString id = oper1;
    const AutoArray<PlayerIdentity> *identities = server->GetIdentities();
    const PlayerIdentity *identity = NULL;
    for (int i=0; i<(*identities).Size(); i++)
    {
      if ( stricmp((*identities)[i].id, id)==0 )
      {
        identity = &(*identities)[i];
        break;
      }
    }
    if (!identity) return NOTHING;

    server->KickOff(identity->dpnid, KORKick);
  }
  return NOTHING;
}

GameValue ServerBanUID(const GameState *state, GameValuePar oper1)
{
  NetworkServer *server = GNetworkManager.GetServer();
  if (!server) return NOTHING;

  if (oper1.GetType()==GameString)
  {
    RString id = oper1;

    // add global ban list
    FindArray<RString> banList;
    LoadBanList("ban.txt", banList);
    GFileServerFunctions->FlushReadHandle("ban.txt");
    banList.AddUnique(id);
    SaveBanList("ban.txt", banList);

    // kick off
    return ServerKickUID(state, oper1);
  }
  return NOTHING;
}

GameValue ServerNFiles(const GameState *state, GameValuePar oper1)
{
  NetworkServer *server = GNetworkManager.GetServer();
  if (!server) return 0.0f;

  int playerId = toInt((float)oper1);
  const PlayerIdentity *identity = server->FindIdentityByPlayerId(playerId);
  if (!identity) return 0.0f;

  return (float)identity->filesCount;
}

GameValue ServerLock(const GameState *state, GameValuePar oper1)
{
  NetworkServer *server = GNetworkManager.GetServer();
  if (server) server->LockSession(oper1);
  return NOTHING;
}

GameValue ServerCheckFile(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
  int level = toInt((float)oper1);

  const GameArrayType &array = oper2;
  if (!CheckSize(state, array, 2)) return NOTHING;
  if (!CheckType(state, array[0], GameScalar)) return NOTHING;
  if (!CheckType(state, array[1], GameScalar)) return NOTHING;

  int playerId = toInt((float)array[0]);
  int fileIndex = toInt((float)array[1]);

  if (fileIndex < 0) return NOTHING;

  NetworkServer *server = GNetworkManager.GetServer();
  if (!server) return NOTHING;

  const PlayerIdentity *identity = server->FindIdentityByPlayerId(playerId);
  if (!identity) return NOTHING;

  server->CheckSignature(identity->dpnid, fileIndex, level);
  return NOTHING;
}

GameValue ServerCheckExe(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
  int level = toInt((float)oper1);
  (void)level; // not used yet
  int playerId = toInt((float)oper2);

  NetworkServer *server = GNetworkManager.GetServer();
  if (!server) return NOTHING;

  const PlayerIdentity *identity = server->FindIdentityByPlayerId(playerId);
  if (!identity) return NOTHING;

  server->IntegrityCheck(identity->dpnid, IQTExe, IntegrityQuestion());
  return NOTHING;
}

//((EXE_CRC_CHECK
#if !_SUPER_RELEASE
GameValue ServerCmpExe(const GameState *state)
{
  NetworkServer *server = GNetworkManager.GetServer();
  if (!server) return false;

  // check if exactly two identities are present
  int dpid1 = 0, dpid2 = 0, dpidCount = 0;
  const AutoArray<PlayerIdentity> *identities = server->GetIdentities();
  if (!identities) return false;
  for (int i=0; i<identities->Size(); i++)
  {
    const PlayerIdentity &identity = identities->Get(i);
    if (identity.dpnid == BOT_CLIENT) continue;
    dpidCount++;
    if (dpidCount == 1)
      dpid1 = identity.dpnid;
    else if (dpidCount == 2)
      dpid2 = identity.dpnid;
    else
      break;
  }
  if (dpidCount != 2) return false;

  // start the exe investigation
  server->CompareExe(dpid1, dpid2);
  return true;
}
#endif
//))EXE_CRC_CHECK

#define NULARS_SERVER(XX, Category) \
  XX(GameArray, "users", ServerUsers, "The list of the users connected to the server.", "", "", "2.92", "", Category) \

//((EXE_CRC_CHECK
#define NULARS_SERVER_DEBUG(XX, Category) \
  XX(GameBool, "cmpExe", ServerCmpExe, "Compare executable of two connected clients.", "", "", "5170", "", Category) \
//))EXE_CRC_CHECK

#define FUNCTIONS_SERVER(XX, Category) \
  XX(GameNothing, "ban", ServerBan, GameScalar, "user id", "Add the user to the server ban list.", "", "", "2.92", "", Category) \
  XX(GameNothing, "kick", ServerKick, GameScalar, "user id", "Kick the user from the server.", "", "", "2.92", "", Category) \
  XX(GameScalar, "numberOfFiles", ServerNFiles, GameScalar, "user id", "Return the number of files used by the user.", "", "", "2.92", "", Category) \
  XX(GameNothing, "lock", ServerLock, GameBool, "lock", "Lock / unlock the session.", "", "", "5126", "", Category) \

#define OPERATORS_SERVER(XX, Category) \
  XX(GameNothing, "checkFile", function, ServerCheckFile, GameScalar, GameArray, "level", "[user id, index]", "Start the integrity check of file given by index for given user. Level defines how exhaustive the test should be.", "", "", "2.92", "", Category) \
  XX(GameNothing, "checkExe", function, ServerCheckExe, GameScalar, GameScalar, "level", "user id", "Start the integrity check of game executable for given user. Level defines how exhaustive the test should be.", "", "", "2.92", "", Category) \

NULARS_SERVER(JNI_NULAR, "Server")
#if !_SUPER_RELEASE
  NULARS_SERVER_DEBUG(JNI_NULAR, "Server Debug")
#endif

static const GameNular ServerNular[]=
{
  NULARS_SERVER(REGISTER_NULAR, "Server")
//((EXE_CRC_CHECK
#if !_SUPER_RELEASE
  NULARS_SERVER_DEBUG(REGISTER_NULAR, "Server Debug")
#endif
//))EXE_CRC_CHECK
};

FUNCTIONS_SERVER(JNI_FUNCTION, "Server")

static const GameFunction ServerUnary[]=
{
  FUNCTIONS_SERVER(REGISTER_FUNCTION, "Server")
};

OPERATORS_SERVER(JNI_OPERATOR, "Server")

static const GameOperator ServerBinary[]=
{
  OPERATORS_SERVER(REGISTER_OPERATOR, "Server")
};

void NetworkServer::InitEvaluator()
{
  // global namespace need to be passed explicitly
  _evaluator.DisableDefaultGlobals();

  // no special types

  // functions
  for(int i=0; i<lenof(ServerNular); i++)
    _evaluator.NewNularOp(ServerNular[i]);
  for(int i=0; i<lenof(ServerUnary); i++)
    _evaluator.NewFunction(ServerUnary[i]);
  for(int i=0; i<lenof(ServerBinary); i++)
    _evaluator.NewOperator(ServerBinary[i]);
}

#if USE_NAT_NEGOTIATION_MANAGER

/*!
\patch 5180 Date 11/14/2007 by Bebul
- New: Better NAT traversal for VoN.
*/
//! inserting edge announce that NATs of these players should tried to be traversed when possible
void PeerToPeerManager::AddEdge(int dpid1, int dpid2)
{
  P2PPlayerInfo *pl1 = FindPlayer(dpid1);
  if (!pl1) pl1 = AddNewPlayer(dpid1);
  P2PPlayerInfo *pl2 = FindPlayer(dpid2);
  if (!pl2) pl2 = AddNewPlayer(dpid2);
  if (pl1 && pl2) 
  {
#if DEBUG_NAT_NEGOTIATION_MANAGER
    LogF("NNManager: adding edge %d, %d", dpid1, dpid2);
#endif
    _edges.Add(P2PEdge(pl1, pl2));
    _needUpdate = true;
  }
}

//! called when NAT of this player finished its negotiating
void PeerToPeerManager::UpdateNAT(int dpid)
{
  for (int i=0; i<_negotiatingPlayers.Size(); i++)
  {
    if ( _negotiatingPlayers[i] && (_negotiatingPlayers[i]->_dpid==dpid) ) 
    {
#if DEBUG_NAT_NEGOTIATION_MANAGER
      LogF("NNManager: negotiation finished for %d", dpid);
#endif
      _negotiatingPlayers[i]->_nat->_isNegotiating = false; //NAT negotiating finished
      _negotiatingPlayers[i] = NULL; //lazy removal
      _needUpdate = true;
      break;
    }
  }
}

//! Update the arrays. Delete what is obsolete. Remove NULLs.
void PeerToPeerManager::UpdateArrays()
{
  //no need to update players array (items deleted in DestroyPlayer)
  //update NATs array
  for (int i=0; i<_nats.Size(); i++)
  {
    if ( _nats[i] && (_nats[i]->RefCounter()==1) ) //this is last pointer to the NAT
    { //remove it
#if DEBUG_NAT_NEGOTIATION_MANAGER
      LogF("NNManager: removing NAT %x", _nats[i]->_publicAddr);
#endif
      _nats[i]=NULL; //lazy removal
    }
  }
  _nats.RemoveNulls();
  //update negotiating players
  _negotiatingPlayers.RemoveNulls();
}

//! Check whether some new NAT negotiations can be started and start them
void PeerToPeerManager::Update()
{
  // remove negotiations which lasts too long time
  DWORD tickCountNow = GetTickCount();
  static DWORD lastTimeoutCheck = tickCountNow;
  if (tickCountNow-lastTimeoutCheck > 500) //test twice a second
  {
    lastTimeoutCheck = tickCountNow;
    for (int i=0; i<_negotiatingPlayers.Size(); i++)
    {
      if (
        _negotiatingPlayers[i] &&
        ( (tickCountNow - _negotiatingPlayers[i]->_nat->_negotiationStartTime) > P2PNATInfo::NegotiationTimeout)
      )
      { // the timeout for negotiation has elapsed
#if DEBUG_NAT_NEGOTIATION_MANAGER
        LogF("NNManager: negotiation for %d removed for timeout", _negotiatingPlayers[i]->_dpid);
#endif
        _negotiatingPlayers[i]->_nat->_isNegotiating = false;
        _negotiatingPlayers[i] = NULL; //lazy removal
        _needUpdate = true;
      }
    }
  }
  // try to start some negotiations, when possible
  if (_needUpdate)
  {
    _needUpdate = false;
    //update the arrays first. (delete NULLs and obsolete items)
    UpdateArrays();
    //try to find all edges which can start NAT negotiating
    for (int i=0; i<_edges.Size(); i++)
    { //trick: when some edge is found, when deleting it, move the last array item into it
      //   This can help to shuffle the edges a little.
      if (_edges[i]._player1->_nat->_isNegotiating || _edges[i]._player2->_nat->_isNegotiating) 
        continue;
#if DEBUG_NAT_NEGOTIATION_MANAGER
      LogF("NNManager: starting negotiation for %d, %d", _edges[i]._player1->_dpid, _edges[i]._player2->_dpid);
#endif
      // this edge can start negotiation
      /* 1. store info: players are negotiating*/
      //NATs are negotiating
      _edges[i]._player1->_nat->_isNegotiating = _edges[i]._player2->_nat->_isNegotiating = true;
      _edges[i]._player1->_nat->_negotiationStartTime = _edges[i]._player2->_nat->_negotiationStartTime = tickCountNow;
      //Players are negotiating
      _negotiatingPlayers.Add(_edges[i]._player1);
      _negotiatingPlayers.Add(_edges[i]._player2);
      /* 2. ask to start NAT Negotiation */
      ConnectVoiceNatNegMessage connect;
      connect._cookie = rand32();
      int dpid1 = _edges[i]._player1->_dpid;
      int dpid2 = _edges[i]._player2->_dpid;
      //NAT negotiation for first player
      connect._clientIndex = 0;
      connect._dpnid = dpid2;
      _server->SendMsg(dpid1, &connect, NMFGuaranteed);
      //NAT negotiation for second player
      connect._clientIndex = 1;
      connect._dpnid = dpid1;
      _server->SendMsg(dpid2, &connect, NMFGuaranteed);
      /* 3. remove an edge from the array */
      int lastIx = _edges.Size()-1;
      if (lastIx!=i)
      { // move last item to current position
        _edges[i] = _edges[lastIx];
        i--; //current position need to be processed again (it contains new data)
      }
      _edges.Delete(lastIx); //last item should be deleted in any case
    }
  }
}

void PeerToPeerManager::DestroyPlayer(int dpid)
{
#if DEBUG_NAT_NEGOTIATION_MANAGER
  LogF("NNManager: destroying player %d", dpid);
#endif
  _needUpdate = true;
  //_edges array
  for (int i=0; i<_edges.Size(); i++)
  {
    if (_edges[i]._player1->_dpid==dpid || _edges[i]._player2->_dpid==dpid)
    {
      _edges.Delete(i);
      i--;
    }
  }
  //_players array
  for (int i=0; i<_players.Size(); i++)
  {
    if ( _players[i] && (_players[i]->_dpid==dpid) ) 
    { 
      _players.Delete(i);
      break;
    }
  }
  //In _negotiatingPlayers array - mark related NAT as not negotiating anymore
  for (int i=0; i<_negotiatingPlayers.Size(); i++)
  {
    if ( _negotiatingPlayers[i] && (_negotiatingPlayers[i]->_dpid==dpid) )
    { 
      // NAT of this player is negotiating for this player, so mark it as NOT negotiating
      _negotiatingPlayers[i]->_nat->_isNegotiating = false;
      // notice: negotiation was started for two players. 
      //   The second NAT negotiation will be processed (probably fail) and send NMTNatNegResult message to the server.
      _negotiatingPlayers[i]=NULL; //lazy remove
      break;
    }
  }
  // UpdateArrays(); //no need to call it, update is doing it every time
}

PeerToPeerManager::P2PPlayerInfo *PeerToPeerManager::AddNewPlayer(int dpid)
{ //assume the player cannot be found in _players!
  NetworkPlayerInfo *plInfo = _server->GetPlayerInfo(dpid);
  //PlayerIdentity *player = _server->FindIdentity(dpid);
  if (!plInfo)
  {
    LogF("NAT manager: cannot find NetworkPlayerInfo for dpid %d", dpid);
    return NULL;
  }
#if DEBUG_NAT_NEGOTIATION_MANAGER
  LogF("NNManager: adding new player %d", dpid);
#endif
  P2PPlayerInfo *info = new P2PPlayerInfo;
  info->_dpid = dpid;
  info->_publicAddr = plInfo->_publicAddr;
  info->_nat = FindNAT(info->_publicAddr);
  if (!info->_nat)
  { //new NAT, create it and add it
    P2PNATInfo *ninfo = new P2PNATInfo(info->_publicAddr);
    _nats.Add(ninfo);
    info->_nat = ninfo;
  }
  _players.AddReusingNull(info);
  return info;
}
#endif //USE_NAT_NEGOTIATION_MANAGER

#endif // _ENABLE_DEDICATED_SERVER
