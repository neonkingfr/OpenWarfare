#ifdef _MSC_VER
#pragma once
#endif

#ifndef _NETWORK_IMPL_HPP
#define _NETWORK_IMPL_HPP


#include "netTransport.hpp"
#include "network.hpp"
#include "../versionNo.h"

#include <Es/Common/win.h>
#include "../memHeap.hpp"

#include "../speaker.hpp"
#include "../arcadeWaypoint.hpp"

#include "../paramFileExt.hpp"

#include <El/Interfaces/iAppInfo.hpp>
#include <El/DebugWindow/debugWin.hpp>
#include <El/DataSignatures/dataSignatures.hpp>

#include <El/FileServer/multithread.hpp>

#include <El/Common/randomGen.hpp>

#if _ENABLE_STEAM

#include <steamworks/steam_api.h>
#include <steamworks/steam_gameserver.h>

extern bool UseSteam;
#endif

/*!
\file
Internal interface file for multiplayer game (included only from inside of multiplayer files)
*/

//! Magic for distinguish applications
#if _VBS1 || _VBS1_DEMO || _VBS2
  #define MAGIC_APP   0x11111111
#elif _FP2
  #define MAGIC_APP   0x33333333
#elif _DEMO
  // ArmA 2 demo
  #define MAGIC_APP   0x14141414
#elif _LIBERATION
  #define MAGIC_APP   0x40774077
#else // ArmA 2
  #define MAGIC_APP   0x25252525
#endif
/* ArmA 1 values
#elif _DEMO // demo and ArmA do not share LAN sessions
  #define MAGIC_APP   0x45454545
#else // ArmA
  #define MAGIC_APP   0x23232323
#endif
*/

// old OFP had #define MAGIC_APP   0x00000000

#define NO_CLIENT               0
#define TO_SERVER               1
// TODO: Avoid usage of this value as regular client ID
#define BOT_CLIENT              2

extern const int DiagLevel;
bool DiagFilter(NetworkMessageType type);
RString GetMsgTypeName(NetworkMessageType type);
// extern const char *NetworkMessageTypeNames[];

//////////////////////////////////////////////////////////////////////////////
// Network Messages

//! Encapsulation for block of data transferred over network.
/*!
Implement methods nedded for transfer of NetworkMessage into raw data.
\todo Implement compression methods.
\todo Work with bits, no bytes.
*/
class NetworkMessageRaw
{
protected:
  //! raw data itself
  AutoArray<char> _buffer;
  //! read / write position
  int _pos;

  //! external raw data
  char *_externalBuffer;
  //! size of external raw data
  int _externalBufferSize;
public:
  //! Constructor
  NetworkMessageRaw()
  {_pos = 0; _buffer.Realloc(1024); _externalBuffer = NULL; _externalBufferSize = 0;}
  //! Constructor
  /*!
    \param buffer external data
    \param size size of external data
  */
  NetworkMessageRaw(char *buffer, int size)
  {_pos = 0; _externalBuffer = buffer; _externalBufferSize = size;}

  //! Return total size of valid data
  int GetSize() const {return _externalBuffer ? _externalBufferSize : _buffer.Size();}
  //! Set new size of internal buffer, sets position to 0
  void SetSize(int size) {_buffer.Resize(size); _pos = 0;}
  //! Get (read only) pointer to valid data
  const char *GetData() const {return _externalBuffer ? _externalBuffer : _buffer.Data();}
  //! Get (read and write) pointer to internal buffer
  char *SetData() {return _buffer.Data();}
  //! Return current read / write position in message
  int GetPos() const {return _pos;}
  
  //@{
  //! Write single value to buffer
  /*!
  \param value value to write
  \param compression compression algorithm
  */
  void Put(bool value, NetworkCompressionType compression);
  void Put(char value, NetworkCompressionType compression);
  void Put(int value, NetworkCompressionType compression);
  void Put(NetInt64 &value, NetworkCompressionType compression);
  void Put(float value, NetworkCompressionType compression);
  void Put(RString value, NetworkCompressionType compression);
  void Put(const LocalizedString &value, NetworkCompressionType compression);
  void Put(Time value, NetworkCompressionType compression);
  void Put(Vector3Par value, NetworkCompressionType compression);
  void Put(Matrix3Par value, NetworkCompressionType compression);
  void Put(NetworkId &value, NetworkCompressionType compression);
  void Put(XNADDR &value, NetworkCompressionType compression);
  void Put(XONLINE_STAT &value, NetworkCompressionType compression);
  //@}

  //@{
  //! Read single value from buffer
  /*!
  \param value reference to variable read into
  \param compression compression algorithm
  \return true if value was read, otherwise false
  */
  bool GetNoCompression(RString &value);

  bool Get(bool &value, NetworkCompressionType compression);
  bool Get(char &value, NetworkCompressionType compression);
  bool Get(int &value, NetworkCompressionType compression);
  bool Get(NetInt64 &value, NetworkCompressionType compression);
  bool Get(float &value, NetworkCompressionType compression);
  bool Get(RString &value, NetworkCompressionType compression);
  bool Get(LocalizedString &value, NetworkCompressionType compression);
  bool Get(Time &value, NetworkCompressionType compression);
  bool Get(Vector3 &value, NetworkCompressionType compression);
  bool Get(Matrix3 &value, NetworkCompressionType compression);
  bool Get(NetworkId &value, NetworkCompressionType compression);
  bool Get(XNADDR &value, NetworkCompressionType compression);
  bool Get(XONLINE_STAT &value, NetworkCompressionType compression);
  //@}

protected:
  //! Write raw data to buffer
  /*!
  \param buffer raw data
  \param size raw data size
  */
  void Write(const void *buffer, int size)
  {
    int minSize = _pos + size;
    if (_buffer.Size() < minSize) _buffer.Resize(minSize);
    memcpy(_buffer.Data() + _pos, buffer, size);
    _pos += size;
  }
  //! Read raw data from buffer
  /*!
  \param buffer where read raw data
  \param size size of data to read
  \return true if data available in buffer, otherwise false
  */
  bool Read(void *buffer, int size)
  {
    int minSize = _pos + size;
    if (_externalBuffer)
    {
      if (_externalBufferSize < minSize) return false;
      memcpy(buffer, _externalBuffer + _pos, size);
    }
    else
    {
      if (_buffer.Size() < minSize) return false;
      memcpy(buffer, _buffer.Data() + _pos, size);
    }
    _pos += size;
    return true;
  }
};
TypeIsMovable(NetworkMessageRaw)

#define PLAYER_MSG(MessageName, XX) \
  XX(MessageName, int, player, NDTInteger, int, NCTNone, DEFVALUE(int, 0), DOC_MSG("Unique ID of client (player)"), TRANSF)\
  XX(MessageName, RString, name, NDTString, RString, NCTNone, DEFVALUE(RString, ""), DOC_MSG("Player's name"), TRANSF)\
  XX(MessageName, bool, server, NDTBool, bool, NCTNone, DEFVALUE(bool, false), DOC_MSG("Bot client (running in the same process as server)"), TRANSF)\
  XX(MessageName, bool, vonDisabled, NDTBool, bool, NCTNone, DEFVALUE(bool, false), DOC_MSG("Voice over Net disabled"), TRANSF)\
  XX(MessageName, int, vonCodecQuality, NDTInteger, int, NCTNone, DEFVALUE(int, 3), DOC_MSG("Voice over Net codec quality"), TRANSF)\
  XX(MessageName, bool, battlEye, NDTBool, bool, NCTNone, DEFVALUE(bool, false), DOC_MSG("BattlEye is required by the server"), TRANSF)\
  XX(MessageName, NetInt64, steamServerId, NDTInt64, NetInt64, NCTNone, DEFVALUE(NetInt64, 0), DOC_MSG("Steam server id"), TRANSF)\
  XX(MessageName, bool, steamSecure, NDTBool, bool, NCTNone, DEFVALUE(bool, false), DOC_MSG("Steam VAC protection is active"), TRANSF)\
  XX(MessageName, int, sigVerRequired, NDTInteger, int, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG("Required signatures version on the server"), TRANSF)\

DECLARE_NET_MESSAGE(Player, PLAYER_MSG)

#define LOGOUT_MSG(MessageName, XX) \
  XX(MessageName, int, dpnid, NDTInteger, int, NCTNone, DEFVALUE(int, 0), DOC_MSG("ID of client (player)"), TRANSF)

DECLARE_NET_MESSAGE(Logout, LOGOUT_MSG)

#define PUB_VAR_MSG(MessageName, XX) \
  XX(MessageName, RString, name, NDTString, RString, NCTNone, DEFVALUE(RString, ""), DOC_MSG("Variable name"), TRANSF) \
  XX(MessageName, AutoArray<char>, value, NDTRawData, AutoArray<char>, NCTNone, DEFVALUERAWDATA, DOC_MSG("Variable value"), TRANSF)

DECLARE_NET_MESSAGE_INIT_MSG(PublicVariable,PUB_VAR_MSG)

TypeIsMovable(PublicVariableMessage);

#define TEAM_SET_VAR_MSG(MessageName, XX) \
  XX(MessageName, int, id, NDTInteger, int, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG("Id of team member"), TRANSF) \
  XX(MessageName, int, creator, NDTInteger, int, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG("Id of team member"), TRANSF) \
  XX(MessageName, RString, name, NDTString, RString, NCTNone, DEFVALUE(RString, ""), DOC_MSG("Variable name"), TRANSF) \
  XX(MessageName, AutoArray<char>, value, NDTRawData, AutoArray<char>, NCTNone, DEFVALUERAWDATA, DOC_MSG("Variable value"), TRANSF)

DECLARE_NET_MESSAGE(TeamMemberSetVariable,TEAM_SET_VAR_MSG)

#define OBJ_SET_VAR_MSG(MessageName, XX) \
  XX(MessageName, OLinkPermO(Object), object, NDTRef, NetworkId, NCTNone, DEFVALUENULL, DOC_MSG("Variables container object"), TRANSF_REF) \
  XX(MessageName, RString, name, NDTString, RString, NCTNone, DEFVALUE(RString, ""), DOC_MSG("Variable name"), TRANSF) \
  XX(MessageName, AutoArray<char>, value, NDTRawData, AutoArray<char>, NCTNone, DEFVALUERAWDATA, DOC_MSG("Variable value"), TRANSF)

DECLARE_NET_MESSAGE_INIT_MSG(ObjectSetVariable, OBJ_SET_VAR_MSG)

#define GRP_SET_VAR_MSG(MessageName, XX) \
  XX(MessageName, OLinkPermNO(AIGroup), group, NDTRef, NetworkId, NCTNone, DEFVALUENULL, DOC_MSG("Variables container group"), TRANSF_REF) \
  XX(MessageName, RString, name, NDTString, RString, NCTNone, DEFVALUE(RString, ""), DOC_MSG("Variable name"), TRANSF) \
  XX(MessageName, AutoArray<char>, value, NDTRawData, AutoArray<char>, NCTNone, DEFVALUERAWDATA, DOC_MSG("Variable value"), TRANSF)

DECLARE_NET_MESSAGE_INIT_MSG(GroupSetVariable, GRP_SET_VAR_MSG)

#define GROUP_SYNC_MSG(MessageName, XX) \
  XX(MessageName, OLinkPermNO(AIGroup), group, NDTRef, NetworkId, NCTNone, DEFVALUENULL, DOC_MSG("Synchronized group"), TRANSF_REF) \
  XX(MessageName, int, synchronization, NDTInteger, int, NCTNone, DEFVALUE(int, 0), DOC_MSG("Index of synchronization"), TRANSF) \
  XX(MessageName, bool, active, NDTBool, bool, NCTNone, DEFVALUE(bool, true), DOC_MSG("Active / inactive"), TRANSF)

DECLARE_NET_MESSAGE(GroupSynchronization,GROUP_SYNC_MSG)

#define DET_ACT_MSG(MessageName, XX) \
  XX(MessageName, OLinkPermO(Detector), detector, NDTRef, NetworkId, NCTNone, DEFVALUENULL, DOC_MSG("Detector"), TRANSF_REF) \
  XX(MessageName, bool, active, NDTBool, bool, NCTNone, DEFVALUE(bool, true), DOC_MSG("Active / inactive"), TRANSF)

DECLARE_NET_MESSAGE(DetectorActivation,DET_ACT_MSG)

#define CREATE_UNIT_MSG(MessageName, XX) \
  XX(MessageName, OLinkPermNO(AIGroup), group, NDTRef, NetworkId, NCTNone, DEFVALUENULL, DOC_MSG("Group, where unit will be added"), TRANSF_REF) \
  XX(MessageName, RString, type, NDTString, RString, NCTNone, DEFVALUE(RString, ""), DOC_MSG("Entity type"), TRANSF) \
  XX(MessageName, Vector3, position, NDTVector, Vector3, NCTNone, DEFVALUE(Vector3, VZero), DOC_MSG("Position"), TRANSF) \
  XX(MessageName, RString, init, NDTString, RString, NCTNone, DEFVALUE(RString, ""), DOC_MSG("Initialization statement"), TRANSF) \
  XX(MessageName, float, skill, NDTFloat, float, NCTNone, DEFVALUE(float, 0.5), DOC_MSG("Initial skill"), TRANSF) \
  XX(MessageName, int, rank, NDTInteger, int, NCTNone, DEFVALUE(int, RankPrivate), DOC_MSG("Initial rank"), TRANSF)

DECLARE_NET_MESSAGE(AskForCreateUnit, CREATE_UNIT_MSG)

#define REMOTE_CONTROLLED_MSG(MessageName, XX) \
  XX(MessageName, OLinkPermNO(AIBrain), whom, NDTRef, NetworkId, NCTNone, DEFVALUENULL, DOC_MSG("Controlled unit."), TRANSF_REF) \
  XX(MessageName, OLinkPermO(Person), who, NDTRef, NetworkId, NCTNone, DEFVALUENULL, DOC_MSG("Controlling unit."), TRANSF_REF) \

DECLARE_NET_MESSAGE(AskRemoteControlled, REMOTE_CONTROLLED_MSG)

#if _VBS3
#define ENABLE_PERSONAL_ITEMS(MessageName,XX) \
  XX(MessageName, OLinkPermO(Person), object, NDTRef, NetworkId, NCTNone, DEFVALUENULL, DOC_MSG("turn on personalised items"), TRANSF_REF) \
  XX(MessageName, bool, active, NDTBool, bool, NCTNone, DEFVALUE(bool, false), DOC_MSG("turn it on or off"), TRANSF) \
  XX(MessageName, RString, animAction, NDTString, RString, NCTNone, DEFVALUE(RString, ""), DOC_MSG("animation Action"), TRANSF) \
  XX(MessageName, Vector3, personalItemsOffset, NDTVector, Vector3, NCTNone, DEFVALUE(Vector3, VZero), DOC_MSG("Position shift for proxy"), TRANSF)

DECLARE_NET_MESSAGE(EnablePersonalItems, ENABLE_PERSONAL_ITEMS)

#define AARDOUPDATE_MSG(MessageName,XX) \
  XX(MessageName, int,  type, NDTInteger, int, NCTNone, DEFVALUE(int, 0), DOC_MSG("Type of AAR message"), TRANSF) \
  XX(MessageName, int, intType, NDTInteger, int, NCTNone, DEFVALUE(int, 0), DOC_MSG("int type can be anything"), TRANSF) \
  XX(MessageName, RString, stringOne , NDTString, RString, NCTNone, DEFVALUE(RString, ""), DOC_MSG("string can be anything"), TRANSF) \
  XX(MessageName, RString, stringTwo , NDTString, RString, NCTNone, DEFVALUE(RString, ""), DOC_MSG("string can be anything"), TRANSF) \
  XX(MessageName, float  , floatType , NDTFloat, float, NCTNone, DEFVALUE(float, 0.5), DOC_MSG("float type can be anything"), TRANSF) \

DECLARE_NET_MESSAGE(AARDoUpdate, AARDOUPDATE_MSG)

#define AARASKUPDATE_MSG(MessageName,XX) \
  XX(MessageName, int,  type, NDTInteger, int, NCTNone, DEFVALUE(int, 0), DOC_MSG("Type of AAR message"), TRANSF) \
  XX(MessageName, int, intType, NDTInteger, int, NCTNone, DEFVALUE(int, 0), DOC_MSG("int type can be anything"), TRANSF) \
  XX(MessageName, RString, stringOne , NDTString, RString, NCTNone, DEFVALUE(RString, ""), DOC_MSG("string can be anything"), TRANSF) \
  XX(MessageName, RString, stringTwo , NDTString, RString, NCTNone, DEFVALUE(RString, ""), DOC_MSG("string can be anything"), TRANSF) \
  XX(MessageName, float  , floatType , NDTFloat, float, NCTNone, DEFVALUE(float, 0.5), DOC_MSG("float type can be anything"), TRANSF) \

DECLARE_NET_MESSAGE(AARAskUpdate, AARASKUPDATE_MSG)


#define ADDMPREPORT_MSG(MessageName,XX) \
  XX(MessageName, int, type , NDTInteger, int, NCTNone, DEFVALUE(int, 0), DOC_MSG("Type of string carried to add to all clients"), TRANSF) \
  XX(MessageName, RString, myString , NDTString, RString, NCTNone, DEFVALUE(RString, ""), DOC_MSG("string can be anything"), TRANSF) \

DECLARE_NET_MESSAGE(AddMPReport, ADDMPREPORT_MSG)

#define LOADISLAND_MSG(MessageName,XX) \
  XX(MessageName, RString, islandName , NDTString, RString, NCTNone, DEFVALUE(RString, ""), DOC_MSG("Island to load on the client"), TRANSF) \

DECLARE_NET_MESSAGE(LoadIsland, LOADISLAND_MSG)

#define APPLYWEATHER_MSG(MessageName,XX) \
  XX(MessageName, float,seaLevelOffset, NDTFloat, float, NCTNone, DEFVALUE(float, 0), DOC_MSG(""), TRANSF) \
  XX(MessageName, float,overcastSetSky, NDTFloat, float, NCTNone, DEFVALUE(float, 0), DOC_MSG(""), TRANSF) \
  XX(MessageName, float,overcastSetClouds, NDTFloat, float, NCTNone, DEFVALUE(float, 0), DOC_MSG(""), TRANSF) \
  XX(MessageName, float,fogSet, NDTFloat, float, NCTNone, DEFVALUE(float, 0), DOC_MSG(""), TRANSF) \
  XX(MessageName, float,cloudsPos, NDTFloat, float, NCTNone, DEFVALUE(float, 0), DOC_MSG(""), TRANSF) \
  XX(MessageName, float,cloudsAlpha, NDTFloat, float, NCTNone, DEFVALUE(float, 0), DOC_MSG(""), TRANSF) \
  XX(MessageName, float,cloudsBrightness, NDTFloat, float, NCTNone, DEFVALUE(float, 0), DOC_MSG(""), TRANSF) \
  XX(MessageName, float,cloudsSpeed, NDTFloat, float, NCTNone, DEFVALUE(float, 0), DOC_MSG(""), TRANSF) \
  XX(MessageName, float,skyThrough, NDTFloat, float, NCTNone, DEFVALUE(float, 0), DOC_MSG(""), TRANSF) \
  XX(MessageName, float,rainDensity, NDTFloat, float, NCTNone, DEFVALUE(float, 0), DOC_MSG(""), TRANSF) \
  XX(MessageName, float,rainDensityWanted, NDTFloat, float, NCTNone, DEFVALUE(float, 0), DOC_MSG(""), TRANSF) \
  XX(MessageName, float,rainDensitySpeed, NDTFloat, float, NCTNone, DEFVALUE(float, 0), DOC_MSG(""), TRANSF) \
  XX(MessageName, int,rainNextChange, NDTInteger, int, NCTNone, DEFVALUE(int, 0), DOC_MSG(""), TRANSF) \
  XX(MessageName, int,lastWindSpeedChange, NDTInteger, int, NCTNone, DEFVALUE(int, 0), DOC_MSG(""), TRANSF) \
  XX(MessageName, int,gustUntil, NDTInteger, int, NCTNone, DEFVALUE(int, 0), DOC_MSG(""), TRANSF) \
  XX(MessageName, Vector3,windSpeed, NDTVector, Vector3, NCTNone, DEFVALUE(Vector3, VZero), DOC_MSG(""), TRANSF) \
  XX(MessageName, Vector3,currentWindSpeed, NDTVector, Vector3, NCTNone, DEFVALUE(Vector3, VZero), DOC_MSG(""), TRANSF) \
  XX(MessageName, Vector3,currentWindSpeedSlow, NDTVector, Vector3, NCTNone, DEFVALUE(Vector3, VZero), DOC_MSG(""), TRANSF) \
  XX(MessageName, Vector3,gust, NDTVector, Vector3, NCTNone, DEFVALUE(Vector3, VZero), DOC_MSG(""), TRANSF) \
  XX(MessageName, int,year, NDTInteger, int, NCTNone, DEFVALUE(int, 0), DOC_MSG(""), TRANSF) \
  XX(MessageName, int,month, NDTInteger, int, NCTNone, DEFVALUE(int, 0), DOC_MSG(""), TRANSF) \
  XX(MessageName, int,day, NDTInteger, int, NCTNone, DEFVALUE(int, 0), DOC_MSG(""), TRANSF) \
  XX(MessageName, int,hour, NDTInteger, int, NCTNone, DEFVALUE(int, 0), DOC_MSG(""), TRANSF) \
  XX(MessageName, int,minute, NDTInteger, int, NCTNone, DEFVALUE(int, 0), DOC_MSG(""), TRANSF)

DECLARE_NET_MESSAGE(ApplyWeather, APPLYWEATHER_MSG)

#define ASKCOMMANDEROVERRIDE_MSG(MessageName,XX) \
  XX(MessageName, OLinkPermNO(Turret), turret, NDTRef, NetworkId, NCTNone, DEFVALUENULL, DOC_MSG("Specification of turret to override"), TRANSF_REF) \
  XX(MessageName, OLinkNO(Turret), overriddenByTurret, NDTRef, NetworkId,  NCTNone, DEFVALUENULL, DOC_MSG("Which turret takes control"), TRANSF_REF)

DECLARE_NET_MESSAGE(AskCommanderOverride, ASKCOMMANDEROVERRIDE_MSG)

#endif //_VBS3

#define DELETE_VEHICLE_MSG(MessageName, XX) \
  XX(MessageName, OLinkPermO(Entity), vehicle, NDTRef, NetworkId, NCTNone, DEFVALUENULL, DOC_MSG("Vehicle to destroy"), TRANSF_REF)

DECLARE_NET_MESSAGE(AskForDeleteVehicle, DELETE_VEHICLE_MSG)

#define UNIT_ANSWER_MSG(MessageName, XX) \
  XX(MessageName, OLinkPermNO(AIUnit), from, NDTRef, NetworkId, NCTNone, DEFVALUENULL, DOC_MSG("Sender unit"), TRANSF_REF) \
  XX(MessageName, OLinkPermNO(AISubgroup), to, NDTRef, NetworkId, NCTNone, DEFVALUENULL, DOC_MSG("Receiving subgroup"), TRANSF_REF) \
  XX(MessageName, int, answer, NDTInteger, int, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG("Answer"), TRANSF)

DECLARE_NET_MESSAGE(AskForReceiveUnitAnswer,UNIT_ANSWER_MSG)

#define ORDER_GET_IN_MSG(MessageName, XX) \
  XX(MessageName, OLinkPermNO(AIBrain), unit, NDTRef, NetworkId, NCTNone, DEFVALUENULL, DOC_MSG("Asked unit"), TRANSF_REF) \
  XX(MessageName, bool, flag, NDTBool, bool, NCTNone, DEFVALUE(bool, false), DOC_MSG("Get in ordered"), TRANSF)\
  
DECLARE_NET_MESSAGE(AskForOrderGetIn, ORDER_GET_IN_MSG)

#define ALLOW_GET_IN_MSG(MessageName, XX) \
  XX(MessageName, OLinkPermNO(AIBrain), unit, NDTRef, NetworkId, NCTNone, DEFVALUENULL, DOC_MSG("Asked unit"), TRANSF_REF) \
  XX(MessageName, bool, flag, NDTBool, bool, NCTNone, DEFVALUE(bool, false), DOC_MSG("Get in allowed"), TRANSF)\

DECLARE_NET_MESSAGE(AskForAllowGetIn, ALLOW_GET_IN_MSG)

#define GROUP_RESPAWN_MSG(MessageName, XX) \
  XX(MessageName, OLinkPermO(Person), person, NDTRef, NetworkId, NCTNone, DEFVALUENULL, DOC_MSG("Killed person"), TRANSF_REF) \
  XX(MessageName, OLinkPermO(EntityAI), killer, NDTRef, NetworkId, NCTNone, DEFVALUENULL, DOC_MSG("Killer"), TRANSF_REF) \
  XX(MessageName, OLinkPermNO(AIGroup), group, NDTRef, NetworkId, NCTNone, DEFVALUENULL, DOC_MSG("Group, where unit will respawn"), TRANSF_REF) \
  XX(MessageName, int, from, NDTInteger, int, NCTNone, DEFVALUE(int, 0), DOC_MSG("Client (player) ID of sender"), TRANSF)

DECLARE_NET_MESSAGE(AskForGroupRespawn,GROUP_RESPAWN_MSG)

#if _VBS3 // respawn command
#define PAUSESIMULATION_MSG(MessageName, XX) \
  XX(MessageName, bool, pause, NDTBool, bool, NCTNone, DEFVALUE(bool, false), DOC_MSG("Pause local simulation"), TRANSF)

DECLARE_NET_MESSAGE(PauseSimulation,PAUSESIMULATION_MSG)

#define RESPAWN_MSG(MessageName, XX) \
	XX(MessageName, OLinkPermO(Person), person, NDTRef, NetworkId, NCTNone, DEFVALUENULL, DOC_MSG("Person to respawn"), TRANSF_REF) \

DECLARE_NET_MESSAGE(AskForRespawn,RESPAWN_MSG)
#endif

#if _ENABLE_ATTACHED_OBJECTS
#define ATTACHOBJECT_MSG(MessageName, XX) \
  XX(MessageName, OLinkPermO(Object), obj, NDTRef, NetworkId, NCTNone, DEFVALUENULL, DOC_MSG("Object to attach"), TRANSF_REF) \
  XX(MessageName, OLinkPermO(Object), attachTo, NDTRef, NetworkId, NCTNone, DEFVALUENULL, DOC_MSG("Object to attach to"), TRANSF_REF) \
  XX(MessageName, int, memIndex, NDTInteger, int, NCTNone, DEFVALUE(int, -1), DOC_MSG("memory point index"), TRANSF) \
  XX(MessageName, Vector3, pos, NDTVector, Vector3, NCTNone, DEFVALUE(Vector3, Vector3(0, 0, 0)), DOC_MSG("offset"), TRANSF) \
  XX(MessageName, int, flags, NDTInteger, int, NCTNone, DEFVALUE(int, 0), DOC_MSG("Additional flags"), TRANSF)

DECLARE_NET_MESSAGE(AttachObject,ATTACHOBJECT_MSG)

#define DETACHOBJECT_MSG(MessageName, XX) \
  XX(MessageName, OLinkPermO(Object), obj, NDTRef, NetworkId, NCTNone, DEFVALUENULL, DOC_MSG("Object to detach"), TRANSF_REF)

DECLARE_NET_MESSAGE(DetachObject,DETACHOBJECT_MSG)
#endif

#define GROUP_RESPAWN_DONE_MSG(MessageName, XX) \
  XX(MessageName, OLinkPermO(Person), person, NDTRef, NetworkId, NCTNone, DEFVALUENULL, DOC_MSG("Killed person"), TRANSF_REF) \
  XX(MessageName, OLinkPermO(EntityAI), killer, NDTRef, NetworkId, NCTNone, DEFVALUENULL, DOC_MSG("Killer"), TRANSF_REF) \
  XX(MessageName, OLinkPermO(Person), respawn, NDTRef, NetworkId, NCTNone, DEFVALUENULL, DOC_MSG("Respawned person"), TRANSF_REF) \
  XX(MessageName, int, to, NDTInteger, int, NCTNone, DEFVALUE(int, 0), DOC_MSG("Client (player) ID of receiver"), TRANSF)

DECLARE_NET_MESSAGE(GroupRespawnDone,GROUP_RESPAWN_DONE_MSG)

#define MISSION_PARAMS_MSG(MessageName, XX) \
  XX(MessageName, float, param1, NDTFloat, float, NCTNone, DEFVALUE(float, 0), DOC_MSG("Mission parameter"), TRANSF) \
  XX(MessageName, float, param2, NDTFloat, float, NCTNone, DEFVALUE(float, 0), DOC_MSG("Mission parameter"), TRANSF) \
  XX(MessageName, AutoArray<float>, paramsArray, NDTFloatArray, AutoArray<float>, NCTNone, DEFVALUEFLOATARRAY, DOC_MSG("Array of mission parameters"), TRANSF) \
  XX(MessageName, bool, updateOnly, NDTBool, bool, NCTNone, DEFVALUE(bool, false), DOC_MSG("Change of params from dialog"), TRANSF)

DECLARE_NET_MESSAGE(MissionParams, MISSION_PARAMS_MSG)

#define ACTIVATE_MINE_MSG(MessageName, XX) \
  XX(MessageName, OLinkPermO(Mine), mine, NDTRef, NetworkId, NCTNone, DEFVALUENULL, DOC_MSG("Activated mine"), TRANSF_REF) \
  XX(MessageName, bool, activate, NDTBool, bool, NCTNone, DEFVALUE(bool, true), DOC_MSG("Activate / disactivate"), TRANSF)

DECLARE_NET_MESSAGE(AskForActivateMine,ACTIVATE_MINE_MSG)

#define INFLAME_FIRE_MSG(MessageName, XX) \
  XX(MessageName, OLinkPermO(Fireplace), fireplace, NDTRef, NetworkId, NCTNone, DEFVALUENULL, DOC_MSG("Fireplace"), TRANSF_REF) \
  XX(MessageName, bool, fire, NDTBool, bool, NCTNone, DEFVALUE(bool, false), DOC_MSG("Light / put out fire"), TRANSF)

DECLARE_NET_MESSAGE(AskForInflameFire,INFLAME_FIRE_MSG)

#define ANIMATION_PHASE_MSG(MessageName, XX) \
  XX(MessageName, OLink(Entity), vehicle, NDTRef, NetworkId, NCTNone, DEFVALUENULL, DOC_MSG("Animated vehicle"), TRANSF_REF) \
  XX(MessageName, RString, animation, NDTString, RString, NCTNone, DEFVALUE(RString, ""), DOC_MSG("Animation ID"), TRANSF) \
  XX(MessageName, float, phase, NDTFloat, float, NCTNone, DEFVALUE(float, 0), DOC_MSG("Animation state (phase)"), TRANSF)

DECLARE_NET_MESSAGE_INIT_MSG(AskForAnimationPhase,ANIMATION_PHASE_MSG)

#define VEHICLE_DAMAGED_MSG(MessageName, XX) \
  XX(MessageName, OLink(EntityAI), damaged, NDTRef, NetworkId, NCTNone, DEFVALUENULL, DOC_MSG("Damaged entity"), TRANSF_REF) \
  XX(MessageName, OLinkPermO(EntityAI), killer, NDTRef, NetworkId, NCTNone, DEFVALUENULL, DOC_MSG("Entity, causing damage"), TRANSF_REF) \
  XX(MessageName, float, damage, NDTFloat, float, NCTNone, DEFVALUE(float, 0), DOC_MSG("Amount of damage"), TRANSF) \
  XX(MessageName, RString, ammo, NDTString, RString, NCTDefault, DEFVALUE(RString, ""), DOC_MSG("Used ammunition type"), TRANSF)

DECLARE_NET_MESSAGE(VehicleDamaged, VEHICLE_DAMAGED_MSG)

#define INCOMING_MISSILE_MSG(MessageName, XX) \
  XX(MessageName, OLinkO(EntityAI), target, NDTRef, NetworkId, NCTNone, DEFVALUENULL, DOC_MSG("Missile target"), TRANSF_REF) \
  XX(MessageName, OLinkPermO(Entity), shot, NDTRef, NetworkId, NCTNone, DEFVALUENULL, DOC_MSG("missile"), TRANSF_REF) \
  XX(MessageName, OLinkPermO(EntityAI), owner, NDTRef, NetworkId, NCTNone, DEFVALUENULL, DOC_MSG("Firing entity"), TRANSF_REF)

DECLARE_NET_MESSAGE(IncomingMissile, INCOMING_MISSILE_MSG)

#define LAUNCHED_COUNTERMEASURES_MSG(MessageName, XX) \
  XX(MessageName, OLinkPermO(Entity), cm, NDTRef, NetworkId, NCTNone, DEFVALUENULL, DOC_MSG("counterMeasures"), TRANSF_REF) \
  XX(MessageName, OLinkPermO(Entity), testedSystem, NDTRef, NetworkId, NCTNone, DEFVALUENULL, DOC_MSG("missile or vehicle"), TRANSF_REF) \
  XX(MessageName, int, count, NDTInteger, int, NCTNone, DEFVALUE(int, 1), DOC_MSG("cm count"), TRANSF)

DECLARE_NET_MESSAGE(LaunchedCounterMeasures, LAUNCHED_COUNTERMEASURES_MSG)

#define WEAPON_LOCKED_MSG(MessageName, XX) \
  XX(MessageName, OLinkO(EntityAI), target, NDTRef, NetworkId, NCTNone, DEFVALUENULL, DOC_MSG("Missile target"), TRANSF_REF) \
  XX(MessageName, OLinkPermNO(Person), gunner, NDTRef, NetworkId, NCTNone, DEFVALUENULL, DOC_MSG("gunner"), TRANSF_REF) \
  XX(MessageName, bool, locked, NDTBool, bool, NCTNone, DEFVALUE(bool, false), DOC_MSG(""), TRANSF)

DECLARE_NET_MESSAGE(WeaponLocked, WEAPON_LOCKED_MSG)

#define SERVER_STATE_MSG(MessageName, XX) \
  XX(MessageName, int, serverState, NDTInteger, int, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG("Server state"), TRANSF)

DECLARE_NET_MESSAGE(ServerState, SERVER_STATE_MSG)

#define CLIENT_STATE_MSG(MessageName, XX) \
  XX(MessageName, int, clientState, NDTInteger, int, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG("Client state"), TRANSF)

DECLARE_NET_MESSAGE(ClientState, CLIENT_STATE_MSG)

#define PLAYER_CLIENT_STATE_MSG(MessageName, XX) \
  XX(MessageName, int, dpnid, NDTInteger, int, NCTNone, DEFVALUE(int, 0), DOC_MSG("Client (player) id"), TRANSF) \
  XX(MessageName, int, clientState, NDTInteger, int, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG("Client state"), TRANSF)

DECLARE_NET_MESSAGE(PlayerClientState, PLAYER_CLIENT_STATE_MSG)

#define APPLY_IDENTITY_MSG(MessageName, XX) \
  XX(MessageName, int, dpnid, NDTInteger, int, NCTNone, DEFVALUE(int, 0), DOC_MSG("Client (player) id"), TRANSF) \
  XX(MessageName, OLinkPermO(Person), person, NDTRef, NetworkId, NCTNone, DEFVALUENULL, DOC_MSG("Person, which identity will be changed"), TRANSF_REF)

#define FORCE_DELETE_OBJECT_MSG(MessageName, XX) \
  XX(MessageName, OLink(NetworkObject), object, NDTRef, NetworkId, NCTNone, DEFVALUENULL, DOC_MSG("Object to destroy"), TRANSF_REF)

DECLARE_NET_MESSAGE(ForceDeleteObject, FORCE_DELETE_OBJECT_MSG)

#define JOIN_INTO_UNIT_MSG(MessageName, XX) \
  XX(MessageName, int, dpnid, NDTInteger, int, NCTNone, DEFVALUE(int, 0), DOC_MSG("Client (player) id"), TRANSF) \
  XX(MessageName, int, roleIndex, NDTInteger, int, NCTNone, DEFVALUE(int, -1), DOC_MSG("Index of player role"), TRANSF) \
  XX(MessageName, OLinkPermNO(AIBrain), unit, NDTRef, NetworkId, NCTNone, DEFVALUENULL, DOC_MSG("Unit join into"), TRANSF_REF) \
  XX(MessageName, OLinkPermNO(AIGroup), group, NDTRef, NetworkId, NCTNone, DEFVALUENULL, DOC_MSG("Group join into"), TRANSF_REF)

DECLARE_NET_MESSAGE(JoinIntoUnit, JOIN_INTO_UNIT_MSG)

#define MUTE_LIST_MSG(MessageName, XX) \
  XX(MessageName, AutoArray<XUID>, list, NDTXUIDArray, AutoArray<XUID>, NCTNone, DEFVALUEXUIDARRAY, DOC_MSG("User ids"), TRANSF)

DECLARE_NET_MESSAGE(MuteList, MUTE_LIST_MSG)

#define REMOTE_MUTE_LIST_MSG(MessageName, XX) \
  XX(MessageName, AutoArray<int>, list, NDTIntArray, AutoArray<int>, NCTNone, DEFVALUEINTARRAY, DOC_MSG("List of muting players"), TRANSF)

DECLARE_NET_MESSAGE(RemoteMuteList, REMOTE_MUTE_LIST_MSG)

#define VOICE_ON_MSG(MessageName, XX) \
  XX(MessageName, int, dpnid, NDTInteger, int, NCTNone, DEFVALUE(int, 0), DOC_MSG("Client (player) id"), TRANSF) \
  XX(MessageName, bool, voiceOn, NDTBool, bool, NCTNone, DEFVALUE(bool, false), DOC_MSG("Voice communicator is connected"), TRANSF)

DECLARE_NET_MESSAGE(VoiceOn, VOICE_ON_MSG)

#define CLEANUP_PLAYER_MSG(MessageName, XX) \
  XX(MessageName, int, dpnid, NDTInteger, int, NCTNone, DEFVALUE(int, 0), DOC_MSG("Client (player) id"), TRANSF) \
  XX(MessageName, int, creator, NDTInteger, int, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG("Id of player person"), TRANSF) \
  XX(MessageName, int, id, NDTInteger, int, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG("Id of player person"), TRANSF)

DECLARE_NET_MESSAGE(CleanupPlayer, CLEANUP_PLAYER_MSG)

#define ASK_WEAPON_MSG(MessageName, XX) \
  XX(MessageName, OLinkPermO(EntityAI), from, NDTRef, NetworkId, NCTNone, DEFVALUENULL, DOC_MSG("Asked container or body"), TRANSF_REF) \
  XX(MessageName, OLinkPermO(EntityAIFull), to, NDTRef, NetworkId, NCTNone, DEFVALUENULL, DOC_MSG("Asking person"), TRANSF_REF) \
  XX(MessageName, RString, weapon, NDTString, RString, NCTDefault, DEFVALUE(RString, ""), DOC_MSG("Type of wanted weapon"), TRANSF) \
  XX(MessageName, int, slots, NDTInteger, int, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG("# of empty magazine slots"), TRANSF) \
  XX(MessageName, bool, useBackpack, NDTBool, bool, NCTNone, DEFVALUE(bool, false), DOC_MSG("using backpack"), TRANSF)

DECLARE_NET_MESSAGE(AskWeapon, ASK_WEAPON_MSG)

#define ASK_MAGAZINE_MSG(MessageName, XX) \
  XX(MessageName, OLinkPermO(EntityAI), from, NDTRef, NetworkId, NCTNone, DEFVALUENULL, DOC_MSG("Asked container or body"), TRANSF_REF) \
  XX(MessageName, OLinkPermO(EntityAIFull), to, NDTRef, NetworkId, NCTNone, DEFVALUENULL, DOC_MSG("Asking person"), TRANSF_REF) \
  XX(MessageName, RString, type, NDTString, RString, NCTDefault, DEFVALUE(RString, ""), DOC_MSG("Type of wanted magazine"), TRANSF) \
  XX(MessageName, bool, useBackpack, NDTBool, bool, NCTNone, DEFVALUE(bool, false), DOC_MSG("using backpack"), TRANSF)

DECLARE_NET_MESSAGE(AskMagazine, ASK_MAGAZINE_MSG) 

#define ASK_BACKPACK_MSG(MessageName, XX) \
  XX(MessageName, OLinkPermO(EntityAI), from, NDTRef, NetworkId, NCTNone, DEFVALUENULL, DOC_MSG("Asked container or body"), TRANSF_REF) \
  XX(MessageName, OLinkPermO(EntityAIFull), to, NDTRef, NetworkId, NCTNone, DEFVALUENULL, DOC_MSG("Asking person"), TRANSF_REF) \
  XX(MessageName, RString, name, NDTString, RString, NCTDefault, DEFVALUE(RString, ""), DOC_MSG("Type of wanted backpack"), TRANSF)

DECLARE_NET_MESSAGE(AskBackpack, ASK_BACKPACK_MSG) 

#define REPLACE_WEAPON_MSG(MessageName, XX) \
  XX(MessageName, OLinkPermO(EntityAI), from, NDTRef, NetworkId, NCTNone, DEFVALUENULL, DOC_MSG("Asking container or body"), TRANSF_REF) \
  XX(MessageName, OLinkPermO(EntityAIFull), to, NDTRef, NetworkId, NCTNone, DEFVALUENULL, DOC_MSG("Asked person"), TRANSF_REF) \
  XX(MessageName, RString, weapon, NDTString, RString, NCTDefault, DEFVALUE(RString, ""), DOC_MSG("Offered weapon"), TRANSF) \
  XX(MessageName, bool, useBackpack, NDTBool, bool, NCTNone, DEFVALUE(bool, false), DOC_MSG("using backpack"), TRANSF)

DECLARE_NET_MESSAGE(ReplaceWeapon, REPLACE_WEAPON_MSG)

#define REPLACE_MAGAZINE_MSG(MessageName, XX) \
  XX(MessageName, OLinkPermO(EntityAI), from, NDTRef, NetworkId,  NCTNone, DEFVALUENULL, DOC_MSG("Asking container or body"), TRANSF_REF) \
  XX(MessageName, OLinkPermO(EntityAIFull), to, NDTRef, NetworkId,  NCTNone, DEFVALUENULL, DOC_MSG("Asked person"), TRANSF_REF) \
  XX(MessageName, Ref<Magazine>, magazine, NDTObject, REF_MSG(NetworkMessageMagazine), NCTNone, DEFVALUE_MSG(NMTMagazine), DOC_MSG("Offered magazine"), TRANSF) \
  XX(MessageName, bool, reload, NDTBool, bool, NCTNone, DEFVALUE(bool, false), DOC_MSG("Try to reload magazine"), TRANSF) \
  XX(MessageName, bool, useBackpack, NDTBool, bool, NCTNone, DEFVALUE(bool, false), DOC_MSG("using backpack"), TRANSF)

DECLARE_NET_MESSAGE(ReplaceMagazine, REPLACE_MAGAZINE_MSG)

#define REPLACE_BACKPACK_MSG(MessageName, XX) \
  XX(MessageName, OLinkPermO(EntityAI), from, NDTRef, NetworkId,  NCTNone, DEFVALUENULL, DOC_MSG("Asking container or body"), TRANSF_REF) \
  XX(MessageName, OLinkPermO(EntityAIFull), to, NDTRef, NetworkId,  NCTNone, DEFVALUENULL, DOC_MSG("Asked person"), TRANSF_REF) \
  XX(MessageName, OLinkPermO(EntityAI), backpack, NDTRef, NetworkId,  NCTNone, DEFVALUENULL, DOC_MSG("backpack"), TRANSF_REF) \

DECLARE_NET_MESSAGE(ReplaceBackpack, REPLACE_BACKPACK_MSG)

#define RETURN_WEAPON_MSG(MessageName, XX) \
  XX(MessageName, OLinkPermO(EntityAI), from, NDTRef, NetworkId,  NCTNone, DEFVALUENULL, DOC_MSG("Asked container or body"), TRANSF_REF) \
  XX(MessageName, RString, weapon, NDTString, RString, NCTDefault, DEFVALUE(RString, ""), DOC_MSG("Offered weapon"), TRANSF)

DECLARE_NET_MESSAGE(ReturnWeapon, RETURN_WEAPON_MSG)

#define RETURN_MAGAZINE_MSG(MessageName, XX) \
  XX(MessageName, OLinkPermO(EntityAI), from, NDTRef, NetworkId,  NCTNone, DEFVALUENULL, DOC_MSG("Asked container or body"), TRANSF_REF) \
  XX(MessageName, Ref<Magazine>, magazine, NDTObject, REF_MSG(NetworkMessageMagazine), NCTNone, DEFVALUE_MSG(NMTMagazine), DOC_MSG("Offered magazine"), TRANSF)

DECLARE_NET_MESSAGE(ReturnMagazine, RETURN_MAGAZINE_MSG)

#define RETURN_BACKPACK_MSG(MessageName, XX) \
  XX(MessageName, OLinkPermO(EntityAI), from, NDTRef, NetworkId,  NCTNone, DEFVALUENULL, DOC_MSG("Asked container or body"), TRANSF_REF) \
  XX(MessageName, OLinkPermO(EntityAI), backpack, NDTRef, NetworkId,  NCTNone, DEFVALUENULL, DOC_MSG("backpack"), TRANSF_REF)

DECLARE_NET_MESSAGE(ReturnBackpack, RETURN_BACKPACK_MSG)

#define CANCEL_TAKE_FLAG_MSG(MessageName, XX) \
  XX(MessageName, OLinkPermO(EntityAI), flag, NDTRef, NetworkId,  NCTNone, DEFVALUENULL, DOC_MSG("Asked flag carrier"), TRANSF_REF)

DECLARE_NET_MESSAGE(CancelTakeFlag, CANCEL_TAKE_FLAG_MSG)

#define REMOVE_MAGAZINE_CARGO_MSG(MessageName, XX) \
  XX(MessageName, OLinkO(EntityAI), vehicle, NDTRef, NetworkId,  NCTNone, DEFVALUENULL, DOC_MSG("Changed container"), TRANSF_REF) \
  XX(MessageName, RString, type, NDTString, RString, NCTDefault, DEFVALUE(RString, ""), DOC_MSG("Magazine type"), TRANSF) \
  XX(MessageName, int, ammo, NDTInteger, int, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG("Remaining ammunition"), TRANSF)

DECLARE_NET_MESSAGE_INIT_MSG(RemoveMagazineCargo, REMOVE_MAGAZINE_CARGO_MSG)

#define CLEAR_MAGAZINE_CARGO_MSG(MessageName, XX) \
  XX(MessageName, OLinkO(EntityAI), vehicle, NDTRef, NetworkId,  NCTNone, DEFVALUENULL, DOC_MSG("Changed container"), TRANSF_REF)

DECLARE_NET_MESSAGE_INIT_MSG(ClearMagazineCargo, CLEAR_MAGAZINE_CARGO_MSG)

#define CLEAR_BACKPACK_CARGO_MSG(MessageName, XX) \
  XX(MessageName, OLinkO(EntityAI), vehicle, NDTRef, NetworkId,  NCTNone, DEFVALUENULL, DOC_MSG("Changed container"), TRANSF_REF)

DECLARE_NET_MESSAGE_INIT_MSG(ClearBackpackCargo, CLEAR_BACKPACK_CARGO_MSG)


#define POOL_ASK_WEAPON_MSG(MessageName, XX) \
  XX(MessageName, int, creator, NDTInteger, int, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG("Id of asking unit"), TRANSF) \
  XX(MessageName, int, id, NDTInteger, int, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG("Id of asking unit"), TRANSF) \
  XX(MessageName, RString, weapon, NDTString, RString, NCTDefault, DEFVALUE(RString, ""), DOC_MSG("Type of wanted weapon"), TRANSF) \
  XX(MessageName, int, slot, NDTInteger, int, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG("Slot where to put weapon"), TRANSF) \
  XX(MessageName, bool, useBackpack, NDTBool, bool, NCTNone, DEFVALUE(bool, false), DOC_MSG("using backpack"), TRANSF)

DECLARE_NET_MESSAGE(PoolAskWeapon, POOL_ASK_WEAPON_MSG)

#define POOL_ASK_MAGAZINE_MSG(MessageName, XX) \
  XX(MessageName, int, creator, NDTInteger, int, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG("Id of asking unit"), TRANSF) \
  XX(MessageName, int, id, NDTInteger, int, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG("Id of asking unit"), TRANSF) \
  XX(MessageName, RString, type, NDTString, RString, NCTDefault, DEFVALUE(RString, ""), DOC_MSG("Type of wanted magazine"), TRANSF) \
  XX(MessageName, int, slot, NDTInteger, int, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG("Slot where to put magazine"), TRANSF) \
  XX(MessageName, bool, useBackpack, NDTBool, bool, NCTNone, DEFVALUE(bool, false), DOC_MSG("using backpack"), TRANSF)

DECLARE_NET_MESSAGE(PoolAskMagazine, POOL_ASK_MAGAZINE_MSG) 

#define POOL_ASK_BACKPACK_MSG(MessageName, XX) \
  XX(MessageName, int, creator, NDTInteger, int, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG("Id of asking unit"), TRANSF) \
  XX(MessageName, int, id, NDTInteger, int, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG("Id of asking unit"), TRANSF) \
  XX(MessageName, RString, name, NDTString, RString, NCTDefault, DEFVALUE(RString, ""), DOC_MSG("Type of wanted backpack"), TRANSF) 

DECLARE_NET_MESSAGE(PoolAskBackpack, POOL_ASK_BACKPACK_MSG)

#define POOL_REPLACE_WEAPON_MSG(MessageName, XX) \
  XX(MessageName, int, creator, NDTInteger, int, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG("Id of asked unit"), TRANSF) \
  XX(MessageName, int, id, NDTInteger, int, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG("Id of asked unit"), TRANSF) \
  XX(MessageName, RString, weapon, NDTString, RString, NCTDefault, DEFVALUE(RString, ""), DOC_MSG("Offered weapon"), TRANSF) \
  XX(MessageName, int, slot, NDTInteger, int, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG("Slot where to put weapon"), TRANSF) \
  XX(MessageName, bool, useBackpack, NDTBool, bool, NCTNone, DEFVALUE(bool, false), DOC_MSG("using backpack"), TRANSF)

DECLARE_NET_MESSAGE(PoolReplaceWeapon, POOL_REPLACE_WEAPON_MSG)

#define POOL_REPLACE_MAGAZINE_MSG(MessageName, XX) \
  XX(MessageName, int, creator, NDTInteger, int, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG("Id of asked unit"), TRANSF) \
  XX(MessageName, int, id, NDTInteger, int, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG("Id of asked unit"), TRANSF) \
  XX(MessageName, Ref<Magazine>, magazine, NDTObject, REF_MSG(NetworkMessageMagazine), NCTNone, DEFVALUE_MSG(NMTMagazine), DOC_MSG("Offered magazine"), TRANSF) \
  XX(MessageName, int, slot, NDTInteger, int, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG("Slot where to put magazine"), TRANSF) \
  XX(MessageName, bool, useBackpack, NDTBool, bool, NCTNone, DEFVALUE(bool, false), DOC_MSG("using backpack"), TRANSF)

DECLARE_NET_MESSAGE(PoolReplaceMagazine, POOL_REPLACE_MAGAZINE_MSG)

#define POOL_REPLACE_BACKPACK_MSG(MessageName, XX) \
  XX(MessageName, int, creator, NDTInteger, int, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG("Id of asked unit"), TRANSF) \
  XX(MessageName, int, id, NDTInteger, int, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG("Id of asked unit"), TRANSF) \
  XX(MessageName, int, bagCreator, NDTInteger, int, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG("Id of asked unit"), TRANSF) \
  XX(MessageName, int, bagId, NDTInteger, int, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG("Id of asked unit"), TRANSF) \
  XX(MessageName, RString, typeName, NDTString, RString, NCTNone, DEFVALUE(RString, ""), DOC_MSG("backpack type name"), TRANSF)

DECLARE_NET_MESSAGE(PoolReplaceBackpack, POOL_REPLACE_BACKPACK_MSG)

#define POOL_RETURN_WEAPON_MSG(MessageName, XX) \
  XX(MessageName, RString, weapon, NDTString, RString, NCTDefault, DEFVALUE(RString, ""), DOC_MSG("Offered weapon"), TRANSF)

DECLARE_NET_MESSAGE(PoolReturnWeapon, POOL_RETURN_WEAPON_MSG)

#define POOL_RETURN_MAGAZINE_MSG(MessageName, XX) \
  XX(MessageName, Ref<Magazine>, magazine, NDTObject, REF_MSG(NetworkMessageMagazine), NCTNone, DEFVALUE_MSG(NMTMagazine), DOC_MSG("Offered magazine"), TRANSF)

DECLARE_NET_MESSAGE(PoolReturnMagazine, POOL_RETURN_MAGAZINE_MSG)

#define POOL_RETURN_BACKPACK_MSG(MessageName, XX) \
  XX(MessageName, int, creator, NDTInteger, int, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG(""), TRANSF) \
  XX(MessageName, int, id, NDTInteger, int, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG(""), TRANSF) \
  XX(MessageName, RString, typeName, NDTString, RString, NCTNone, DEFVALUE(RString, ""), DOC_MSG("backpack type name"), TRANSF)


DECLARE_NET_MESSAGE(PoolReturnBackpack, POOL_RETURN_BACKPACK_MSG)

#define CHAT_MSG(MessageName, XX) \
  XX(MessageName, int, channel, NDTInteger, int, NCTSmallSigned, DEFVALUE(int, 0), DOC_MSG("Radio channel"), TRANSF) \
  XX(MessageName, OLinkPermNO(AIBrain), sender, NDTRef, NetworkId,  NCTNone, DEFVALUENULL, DOC_MSG("Sender unit"), TRANSF_REF) \
  XX(MessageName, RefArray<NetworkObject>, units, NDTRefArray, AutoArray<NetworkId>, NCTNone, DEFVALUEREFARRAY, DOC_MSG("List of receiving units"), TRANSF_REFS) \
  XX(MessageName, RString, name, NDTString, RString, NCTNone, DEFVALUE(RString, ""), DOC_MSG("Sender name"), TRANSF) \
  XX(MessageName, RString, text, NDTString, RString, NCTNone, DEFVALUE(RString, ""), DOC_MSG("Message content"), TRANSF)

DECLARE_NET_MESSAGE(Chat, CHAT_MSG)

#define RADIO_CHAT_MSG(MessageName, XX) \
  XX(MessageName, int, channel, NDTInteger, int, NCTSmallSigned, DEFVALUE(int, 0), DOC_MSG("Radio channel"), TRANSF) \
  XX(MessageName, OLinkPermNO(AIBrain), sender, NDTRef, NetworkId,  NCTNone, DEFVALUENULL, DOC_MSG("Sender unit"), TRANSF_REF) \
  XX(MessageName, RefArray<NetworkObject>, units, NDTRefArray, AutoArray<NetworkId>, NCTNone, DEFVALUEREFARRAY, DOC_MSG("List of receiving units"), TRANSF_REFS) \
  XX(MessageName, RString, text, NDTString, RString, NCTNone, DEFVALUE(RString, ""), DOC_MSG("Message content"), TRANSF) \
  XX(MessageName, RadioSentence, sentence, NDTSentence, RadioSentence, NCTNone, DEFVALUE(RadioSentence, RadioSentence()), DOC_MSG("Content of message (list of words to say)"), TRANSF) \
  XX(MessageName, RString, wordsClass, NDTString, RString, NCTNone, DEFVALUE(RString, ""), DOC_MSG("Possible subclass in class words to define alternative set of words such as whisper"), TRANSF)

DECLARE_NET_MESSAGE(RadioChat, RADIO_CHAT_MSG)

#define RADIO_CHAT_WAVE_MSG(MessageName, XX) \
  XX(MessageName, int, channel, NDTInteger, int, NCTSmallSigned, DEFVALUE(int, 0), DOC_MSG("Radio channel"), TRANSF) \
  XX(MessageName, OLinkPermNO(AIBrain), sender, NDTRef, NetworkId,  NCTNone, DEFVALUENULL, DOC_MSG("Sender unit"), TRANSF_REF) \
  XX(MessageName, RefArray<NetworkObject>, units, NDTRefArray, AutoArray<NetworkId>, NCTNone, DEFVALUEREFARRAY, DOC_MSG("List of receiving units"), TRANSF_REFS) \
  XX(MessageName, RString, senderName, NDTString, RString, NCTNone, DEFVALUE(RString, ""), DOC_MSG("Sender name"), TRANSF) \
  XX(MessageName, RString, wave, NDTString, RString, NCTNone, DEFVALUE(RString, ""), DOC_MSG("Sound identifier"), TRANSF)

DECLARE_NET_MESSAGE(RadioChatWave, RADIO_CHAT_WAVE_MSG)

/** adapted from CHAT_MSG */
#define LOCALIZED_CHAT_MSG(MessageName, XX) \
  XX(MessageName, int, channel, NDTInteger, int, NCTSmallSigned, DEFVALUE(int, 0), DOC_MSG("Radio channel"), TRANSF) \
  XX(MessageName, OLinkPermNO(AIBrain), sender, NDTRef, NetworkId,  NCTNone, DEFVALUENULL, DOC_MSG("Sender unit"), TRANSF_REF) \
  XX(MessageName, RefArray<NetworkObject>, units, NDTRefArray, AutoArray<NetworkId>, NCTNone, DEFVALUEREFARRAY, DOC_MSG("List of receiving units"), TRANSF_REFS) \
  XX(MessageName, RString, name, NDTString, RString, NCTNone, DEFVALUE(RString, ""), DOC_MSG("Sender name"), TRANSF) \
  XX(MessageName, AutoArray<LocalizedString>, args, NDTLocalizedStringArray, AutoArray<LocalizedString>, NCTNone, DEFVALUELOCALIZEDSTRINGARRAY, DOC_MSG("Client-side localized text with arguments"), TRANSF) \

DECLARE_NET_MESSAGE(LocalizedChat,LOCALIZED_CHAT_MSG)

#define SET_SPEAKER_MSG(MessageName, XX) \
  XX(MessageName, int, player, NDTInteger, int, NCTNone, DEFVALUE(int, 0), DOC_MSG("Client (player) ID of speaking player"), TRANSF) \
  XX(MessageName, int, on, NDTBool, int, NCTNone, DEFVALUE(bool, false), DOC_MSG("Turn on / off direct speaking"), TRANSF) \
  XX(MessageName, int, creator, NDTInteger, int, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG("ID of speaking unit"), TRANSF) \
  XX(MessageName, int, id, NDTInteger, int, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG("ID of speaking unit"), TRANSF)

DECLARE_NET_MESSAGE(SetSpeaker, SET_SPEAKER_MSG)

#define SELECT_PLAYER_MSG(MessageName, XX) \
  XX(MessageName, int, player, NDTInteger, int, NCTNone, DEFVALUE(int, AI_PLAYER), DOC_MSG("Client (player) ID of player"), TRANSF) \
  XX(MessageName, int, creator, NDTInteger, int, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG("ID of player's unit"), TRANSF) \
  XX(MessageName, int, id, NDTInteger, int, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG("ID of player's unit"), TRANSF) \
  XX(MessageName, Vector3, position, NDTVector, Vector3, NCTNone, DEFVALUE(Vector3, Vector3(-FLT_MAX, -FLT_MAX, -FLT_MAX)), DOC_MSG("Player's unit position"), TRANSF) \
  XX(MessageName, bool, respawn, NDTBool, bool, NCTNone, DEFVALUE(bool, false), DOC_MSG("Selection of player's unit after respawn"), TRANSF)

DECLARE_NET_MESSAGE(SelectPlayer, SELECT_PLAYER_MSG)

#define CHANGE_OWNER_MSG(MessageName, XX) \
  XX(MessageName, int, creator, NDTInteger, int, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG("ID of object, which owner is changing"), TRANSF) \
  XX(MessageName, int, id, NDTInteger, int, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG("ID of object, whic player is changing"), TRANSF) \
  XX(MessageName, int, owner, NDTInteger, int, NCTNone, DEFVALUE(int, AI_PLAYER), DOC_MSG("Client ID of new owner"), TRANSF) \
  XX(MessageName, bool, disconnected, NDTBool, bool, NCTNone, DEFVALUE(bool, false), DOC_MSG("Disconnected player becomes AI"), TRANSF)

DECLARE_NET_MESSAGE(ChangeOwner, CHANGE_OWNER_MSG)

#define OWNER_CHANGED_MSG(MessageName, XX) \
  XX(MessageName, int, creator, NDTInteger, int, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG("ID of object, which owner was changed"), TRANSF) \
  XX(MessageName, int, id, NDTInteger, int, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG("ID of object, which owner was changed"), TRANSF)

DECLARE_NET_MESSAGE(OwnerChanged, OWNER_CHANGED_MSG)

#define PLAY_SOUND_MSG(MessageName, XX) \
  XX(MessageName, RString, name, NDTString, RString, NCTNone, DEFVALUE(RString, ""), DOC_MSG("Sound identifier"), TRANSF) \
  XX(MessageName, Vector3, position, NDTVector, Vector3, NCTNone, DEFVALUE(Vector3, VZero), DOC_MSG("Sound source position"), TRANSF) \
  XX(MessageName, Vector3, speed, NDTVector, Vector3, NCTNone, DEFVALUE(Vector3, VZero), DOC_MSG("Sound source speed"), TRANSF) \
  XX(MessageName, float, volume, NDTFloat, float, NCTNone, DEFVALUE(float, 0), DOC_MSG("Sound volume"), TRANSF) \
  XX(MessageName, float, frequency, NDTFloat, float, NCTNone, DEFVALUE(float, 0), DOC_MSG("Sound pitch"), TRANSF) \
  XX(MessageName, int, creator, NDTInteger, int, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG("Unique network ID of sound"), TRANSF) \
  XX(MessageName, int, soundId, NDTInteger, int, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG("Unique network ID of sound"), TRANSF)

DECLARE_NET_MESSAGE(PlaySound, PLAY_SOUND_MSG)

#define SOUND_STATE_MSG(MessageName, XX) \
  XX(MessageName, int, state, NDTInteger, int, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG("New state of sound"), TRANSF) \
  XX(MessageName, int, creator, NDTInteger, int, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG("Network ID of sound"), TRANSF) \
  XX(MessageName, int, soundId, NDTInteger, int, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG("Network ID of sound"), TRANSF)

DECLARE_NET_MESSAGE(SoundState, SOUND_STATE_MSG)

#define DELETE_OBJECT_MSG(MessageName, XX) \
  XX(MessageName, int, creator, NDTInteger, int, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG("Id of object to destroy"), TRANSF) \
  XX(MessageName, int, id, NDTInteger, int, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG("Id of object to destroy"), TRANSF)

DECLARE_NET_MESSAGE(DeleteObject, DELETE_OBJECT_MSG)

#define DELETE_COMMAND_MSG(MessageName, XX) \
  XX(MessageName, int, creator, NDTInteger, int, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG("Id of command to destroy"), TRANSF) \
  XX(MessageName, int, id, NDTInteger, int, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG("ID of command to destroy"), TRANSF) \
  XX(MessageName, OLinkPermNO(AISubgroup), subgrp, NDTRef, NetworkId,  NCTNone, DEFVALUENULL, DOC_MSG("Subgroup that owns command"), TRANSF_REF)

DECLARE_NET_MESSAGE(DeleteCommand, DELETE_COMMAND_MSG)

#define ASK_FOR_ENABLE_VISIONMODE_MSG(MessageName, XX) \
  XX(MessageName, OLinkPermO(Transport), vehicle, NDTRef, NetworkId,  NCTNone, DEFVALUENULL, DOC_MSG(""), TRANSF_REF) \
  XX(MessageName, bool, enable, NDTBool, bool, NCTNone, DEFVALUE(bool, true), DOC_MSG(""), TRANSF)

DECLARE_NET_MESSAGE(AskForEnableVMode, ASK_FOR_ENABLE_VISIONMODE_MSG)

#define ASK_FOR_FORCE_GUN_LIGHT_MSG(MessageName, XX) \
  XX(MessageName, OLinkPermO(Person), vehicle, NDTRef, NetworkId,  NCTNone, DEFVALUENULL, DOC_MSG("Force switch on AI gun light"), TRANSF_REF) \
  XX(MessageName, bool, force, NDTBool, bool, NCTNone, DEFVALUE(bool, true), DOC_MSG(""), TRANSF)

DECLARE_NET_MESSAGE(AskForForceGunLight, ASK_FOR_FORCE_GUN_LIGHT_MSG)

#define ASK_FOR_IR_LASER_MSG(MessageName, XX) \
  XX(MessageName, OLinkPermO(Person), vehicle, NDTRef, NetworkId,  NCTNone, DEFVALUENULL, DOC_MSG("Enable use of IR laser"), TRANSF_REF) \
  XX(MessageName, bool, enable, NDTBool, bool, NCTNone, DEFVALUE(bool, true), DOC_MSG(""), TRANSF)

DECLARE_NET_MESSAGE(AskForIRLaser, ASK_FOR_IR_LASER_MSG)

#define ASK_FOR_DAMMAGE_MSG(MessageName, XX) \
  XX(MessageName, OLink(Object), who, NDTRef, NetworkId,  NCTNone, DEFVALUENULL, DOC_MSG("Damaged object"), TRANSF_REF) \
  XX(MessageName, OLinkPermO(EntityAI), owner, NDTRef, NetworkId,  NCTNone, DEFVALUENULL, DOC_MSG("Who is responsible for damage"), TRANSF_REF) \
  XX(MessageName, Vector3, modelPos, NDTVector, Vector3, NCTNone, DEFVALUE(Vector3, VZero), DOC_MSG("Position of damage"), TRANSF) \
  XX(MessageName, float, val, NDTFloat, float, NCTNone, DEFVALUE(float, 0), DOC_MSG("Amount of damage"), TRANSF) \
  XX(MessageName, float, valRange, NDTFloat, float, NCTNone, DEFVALUE(float, 0), DOC_MSG("Range of damage"), TRANSF) \
  XX(MessageName, RString, ammo, NDTString, RString, NCTDefault, DEFVALUE(RString, RString()), DOC_MSG("Ammunition type"), TRANSF) \
  XX(MessageName, Vector3, originDir, NDTVector, Vector3, NCTNone, DEFVALUE(Vector3, VZero), DOC_MSG("Hit origin direction"), TRANSF)

DECLARE_NET_MESSAGE(AskForDamage, ASK_FOR_DAMMAGE_MSG)

#define ASK_FOR_SET_DAMMAGE_MSG(MessageName, XX) \
  XX(MessageName, OLink(Object), who, NDTRef, NetworkId,  NCTNone, DEFVALUENULL, DOC_MSG("Damaged object"), TRANSF_REF) \
  XX(MessageName, float, dammage, NDTFloat, float, NCTNone, DEFVALUE(float, 0), DOC_MSG("New value of total damage"), TRANSF)

DECLARE_NET_MESSAGE(AskForSetDamage, ASK_FOR_SET_DAMMAGE_MSG)

#define ASK_FOR_SET_MAX_DAMMAGE_MSG(MessageName, XX) \
  XX(MessageName, OLink(Object), who, NDTRef, NetworkId,  NCTNone, DEFVALUENULL, DOC_MSG("Damaged object"), TRANSF_REF) \
  XX(MessageName, float, dammage, NDTFloat, float, NCTNone, DEFVALUE(float, 0), DOC_MSG("New value of total damage"), TRANSF)

DECLARE_NET_MESSAGE(AskForSetMaxHitZoneDamage, ASK_FOR_SET_MAX_DAMMAGE_MSG)

#define ASK_FOR_APPLY_DO_DAMAGE_MSG(MessageName, XX) \
  XX(MessageName, OLink(Object), who, NDTRef, NetworkId,  NCTNone, DEFVALUENULL, DOC_MSG("Damaged object"), TRANSF_REF) \
  XX(MessageName, OLinkPermO(EntityAI), owner, NDTRef, NetworkId,  NCTNone, DEFVALUENULL, DOC_MSG("Who is responsible for damage"), TRANSF_REF) \
  XX(MessageName, RString, ammo, NDTString, RString, NCTDefault, DEFVALUE(RString, RString()), DOC_MSG("Ammunition type"), TRANSF) \
  XX(MessageName, float, damage, NDTFloat, float, NCTNone, DEFVALUE(float, 0), DOC_MSG("How much damage should be caused"), TRANSF) \
  XX(MessageName, AutoArray<float>, hits, NDTFloatArray, AutoArray<float>, NCTNone, DEFVALUEFLOATARRAY, DOC_MSG("How much local damage should be caused"), TRANSF)

DECLARE_NET_MESSAGE(AskForApplyDoDamage, ASK_FOR_APPLY_DO_DAMAGE_MSG)

#define ASK_FOR_GET_IN_MSG(MessageName, XX) \
  XX(MessageName, OLinkPermO(Person), soldier, NDTRef, NetworkId,  NCTNone, DEFVALUENULL, DOC_MSG("Who is getting in"), TRANSF_REF) \
  XX(MessageName, OLinkPermO(Transport), vehicle, NDTRef, NetworkId,  NCTNone, DEFVALUENULL, DOC_MSG("Vehicle to get in"), TRANSF_REF) \
  XX(MessageName, int, position, NDTInteger, int, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG("Position in vehicle to get in"), TRANSF) \
  XX(MessageName, OLinkPermNO(Turret), turret, NDTRef, NetworkId, NCTNone, DEFVALUENULL, DOC_MSG("Specification of turret to get in"), TRANSF_REF) \
  XX(MessageName, int, cargoIndex, NDTInteger, int, NCTSmallUnsigned, DEFVALUE(int, -1), DOC_MSG("Cargo index to get in"), TRANSF)

DECLARE_NET_MESSAGE(AskForGetIn, ASK_FOR_GET_IN_MSG)

#define ASK_FOR_GET_OUT_MSG(MessageName, XX) \
  XX(MessageName, OLinkPermO(Person), soldier, NDTRef, NetworkId,  NCTNone, DEFVALUENULL, DOC_MSG("Who is getting out"), TRANSF_REF) \
  XX(MessageName, OLinkPermO(Transport), vehicle, NDTRef, NetworkId,  NCTNone, DEFVALUENULL, DOC_MSG("Vehicle to get out"), TRANSF_REF) \
  XX(MessageName, OLinkPermNO(Turret), turret, NDTRef, NetworkId, NCTNone, DEFVALUENULL, DOC_MSG("Turret to get out"), TRANSF_REF) \
  XX(MessageName, bool, parachute, NDTBool, bool, NCTNone, DEFVALUE(bool, false), DOC_MSG("Parachute or plain ejection"), TRANSF) \
  XX(MessageName, bool, eject, NDTBool, bool, NCTNone, DEFVALUE(bool, false), DOC_MSG("Ejection - leave the engine on"), TRANSF)

DECLARE_NET_MESSAGE(AskForGetOut, ASK_FOR_GET_OUT_MSG)

#define ASK_WAIT_FOR_GET_OUT_MSG(MessageName, XX) \
  XX(MessageName, OLinkPermO(Transport), vehicle, NDTRef, NetworkId,  NCTNone, DEFVALUENULL, DOC_MSG("Vehicle to get out"), TRANSF_REF) \
  XX(MessageName, OLinkPermNO(AIUnit), unit, NDTRef, NetworkId,  NCTNone, DEFVALUENULL, DOC_MSG("Unit to unload"), TRANSF_REF)
DECLARE_NET_MESSAGE(AskWaitForGetOut, ASK_WAIT_FOR_GET_OUT_MSG)

#define ASK_FOR_CHANGE_POSITION_MSG(MessageName, XX) \
  XX(MessageName, OLinkPermO(Person), soldier, NDTRef, NetworkId,  NCTNone, DEFVALUENULL, DOC_MSG("Who is changing position"), TRANSF_REF) \
  XX(MessageName, OLinkPermO(Transport), vehicle, NDTRef, NetworkId,  NCTNone, DEFVALUENULL, DOC_MSG("Vehicle where position is changed"), TRANSF_REF) \
  XX(MessageName, int, type, NDTInteger, int, NCTSmallUnsigned, DEFVALUE(int, ATMoveToDriver), DOC_MSG("Performed action"), TRANSF) \
  XX(MessageName, OLinkPermNO(Turret), turret, NDTRef, NetworkId, NCTNone, DEFVALUENULL, DOC_MSG("Turret to which position is changed"), TRANSF_REF) \
  XX(MessageName, int, cargoIndex, NDTInteger, int, NCTSmallSigned, DEFVALUE(int, -1), DOC_MSG("Cargo position to where position is changed"), TRANSF)

DECLARE_NET_MESSAGE(AskForChangePosition, ASK_FOR_CHANGE_POSITION_MSG)

#define ASK_FOR_SELECT_WEAPON_MSG(MessageName, XX) \
  XX(MessageName, OLinkPermO(EntityAIFull), vehicle, NDTRef, NetworkId,  NCTNone, DEFVALUENULL, DOC_MSG("Vehicle which weapon is selecting"), TRANSF_REF) \
  XX(MessageName, OLinkPermNO(Turret), turret, NDTRef, NetworkId, NCTNone, DEFVALUENULL, DOC_MSG("Turret which weapon is selecting"), TRANSF_REF) \
  XX(MessageName, int, weapon, NDTInteger, int, NCTSmallSigned, DEFVALUE(int, 0), DOC_MSG("Selected weapon index"), TRANSF)

DECLARE_NET_MESSAGE(AskForSelectWeapon, ASK_FOR_SELECT_WEAPON_MSG)

#define ASK_FOR_AMMO_MSG(MessageName, XX) \
  XX(MessageName, OLinkPermO(EntityAIFull), vehicle, NDTRef, NetworkId,  NCTNone, DEFVALUENULL, DOC_MSG("Vehicle which ammo is changing"), TRANSF_REF) \
  XX(MessageName, OLinkPermNO(Turret), turret, NDTRef, NetworkId, NCTNone, DEFVALUENULL, DOC_MSG("Turret which ammo is changing"), TRANSF_REF) \
  XX(MessageName, int, weapon, NDTInteger, int, NCTSmallSigned, DEFVALUE(int, 0), DOC_MSG("Weapon index"), TRANSF) \
  XX(MessageName, int, burst, NDTInteger, int, NCTSmallUnsigned, DEFVALUE(int, 1), DOC_MSG("Amount of ammo to decrease"), TRANSF)

DECLARE_NET_MESSAGE(AskForAmmo, ASK_FOR_AMMO_MSG)

#define ASK_FOR_FIRE_WEAPON_MSG(MessageName, XX) \
  XX(MessageName, OLinkPermO(EntityAIFull), vehicle, NDTRef, NetworkId,  NCTNone, DEFVALUENULL, DOC_MSG("Vehicle which is firing"), TRANSF_REF) \
  XX(MessageName, OLinkPermNO(Turret), turret, NDTRef, NetworkId, NCTNone, DEFVALUENULL, DOC_MSG("Turret which is firing"), TRANSF_REF) \
  XX(MessageName, int, weapon, NDTInteger, int, NCTSmallSigned, DEFVALUE(int, 0), DOC_MSG("Weapon index"), TRANSF) \
  XX(MessageName, OLinkPermO(TargetType), target, NDTRef, NetworkId,  NCTNone, DEFVALUENULL, DOC_MSG("Fire target"), TRANSF_REF) \
  XX(MessageName, bool, forceLock, NDTBool, bool, NCTNone, DEFVALUE(bool, false), DOC_MSG("forceLock - to avoid weapon lock delay"), TRANSF)

DECLARE_NET_MESSAGE(AskForFireWeapon, ASK_FOR_FIRE_WEAPON_MSG)

#define ASK_FOR_AIRPORT_SETSIDE(MessageName, XX) \
  XX(MessageName, int, airportId, NDTInteger, int, NCTSmallSigned, DEFVALUE(int, 0), DOC_MSG("Airport index"), TRANSF) \
  XX(MessageName, int, side, NDTInteger, int,  NCTSmallSigned, DEFVALUE(int, 0), DOC_MSG("Side to set"), TRANSF)

DECLARE_NET_MESSAGE_INIT_MSG(AskForAirportSetSide, ASK_FOR_AIRPORT_SETSIDE)

#define ASK_FOR_ADD_IMPULSE_MSG(MessageName, XX) \
  XX(MessageName, OLinkPerm<Entity>, vehicle, NDTRef, NetworkId,  NCTNone, DEFVALUENULL, DOC_MSG("Vehicle impulse is applied to"), TRANSF_REF) \
  XX(MessageName, Vector3, force, NDTVector, Vector3, NCTNone, DEFVALUE(Vector3, VZero), DOC_MSG("Applied force"), TRANSF) \
  XX(MessageName, Vector3, torque, NDTVector, Vector3, NCTNone, DEFVALUE(Vector3, VZero), DOC_MSG("Applied torque"), TRANSF)

DECLARE_NET_MESSAGE(AskForAddImpulse, ASK_FOR_ADD_IMPULSE_MSG)

#define ASK_FOR_MOVE_VECTOR_MSG(MessageName, XX) \
  XX(MessageName, OLinkPerm<Object>, vehicle, NDTRef, NetworkId,  NCTNone, DEFVALUENULL, DOC_MSG("Moving object"), TRANSF_REF) \
  XX(MessageName, Vector3, pos, NDTVector, Vector3, NCTNone, DEFVALUE(Vector3, VZero), DOC_MSG("New position"), TRANSF)

DECLARE_NET_MESSAGE(AskForMoveVector, ASK_FOR_MOVE_VECTOR_MSG)

#define ASK_FOR_MOVE_MATRIX_MSG(MessageName, XX) \
  XX(MessageName, OLinkPerm<Object>, vehicle, NDTRef, NetworkId,  NCTNone, DEFVALUENULL, DOC_MSG("Moving object"), TRANSF_REF) \
  XX(MessageName, Vector3, pos, NDTVector, Vector3, NCTNone, DEFVALUE(Vector3, VZero), DOC_MSG("New position"), TRANSF) \
  XX(MessageName, Matrix3, orient, NDTMatrix, Matrix3, NCTNone, DEFVALUE(Matrix3, M3Identity), DOC_MSG("New orientation"), TRANSF)

DECLARE_NET_MESSAGE(AskForMoveMatrix, ASK_FOR_MOVE_MATRIX_MSG)

#define ASK_FOR_JOIN_GROUP_MSG(MessageName, XX) \
  XX(MessageName, OLinkPermNO(AIGroup), join, NDTRef, NetworkId,  NCTNone, DEFVALUENULL, DOC_MSG("Joined group"), TRANSF_REF) \
  XX(MessageName, OLinkPermNO(AIGroup), group, NDTRef, NetworkId,  NCTNone, DEFVALUENULL, DOC_MSG("Joining group"), TRANSF_REF) \
  XX(MessageName, bool, silent, NDTBool, bool, NCTNone, DEFVALUE(bool, false), DOC_MSG("Avoid radio communication"), TRANSF) \

DECLARE_NET_MESSAGE(AskForJoinGroup, ASK_FOR_JOIN_GROUP_MSG)

#define ASK_FOR_JOIN_UNITS_MSG(MessageName, XX) \
  XX(MessageName, OLinkPermNO(AIGroup), join, NDTRef, NetworkId,  NCTNone, DEFVALUENULL, DOC_MSG("Joined group"), TRANSF_REF) \
  XX(MessageName, OLinkPermNOArray(AIUnit), units, NDTRefArray, AutoArray<NetworkId>, NCTNone, DEFVALUEREFARRAY, DOC_MSG("Joining units"), TRANSF_REFS) \
  XX(MessageName, bool, silent, NDTBool, bool, NCTNone, DEFVALUE(bool, false), DOC_MSG("Avoid radio communication"), TRANSF) \
  XX(MessageName, int, id, NDTInteger, int, NCTSmallSigned, DEFVALUE(int, -1), DOC_MSG("Preferred slot id"), TRANSF) \

DECLARE_NET_MESSAGE(AskForJoinUnits, ASK_FOR_JOIN_UNITS_MSG)

#define ASK_FOR_CHANGE_SIDE_MSG(MessageName, XX) \
  XX(MessageName, OLinkPerm<Entity>, vehicle, NDTRef, NetworkId,  NCTNone, DEFVALUENULL, DOC_MSG("Vehicle impulse is applied to"), TRANSF_REF) \
  XX(MessageName, int, side, NDTInteger, int,  NCTSmallSigned, DEFVALUE(int, 0), DOC_MSG("Side to set"), TRANSF)

DECLARE_NET_MESSAGE(AskForChangeSide, ASK_FOR_CHANGE_SIDE_MSG)

#define ASK_FOR_HIDE_BODY_MSG(MessageName, XX) \
  XX(MessageName, OLinkPermO(Person), vehicle, NDTRef, NetworkId,  NCTNone, DEFVALUENULL, DOC_MSG("Body to hide"), TRANSF_REF)

DECLARE_NET_MESSAGE(AskForHideBody, ASK_FOR_HIDE_BODY_MSG)

#define EXPLOSION_DAMAGE_EFFECTS_MSG(MessageName, XX) \
  XX(MessageName, OLinkPermO(EntityAI), owner, NDTRef, NetworkId,  NCTNone, DEFVALUENULL, DOC_MSG("Shot owner (who is responsible for explosion)"), TRANSF_REF) \
  XX(MessageName, OLink(Object), directHit, NDTRef, NetworkId,  NCTNone, DEFVALUENULL, DOC_MSG("Hit object"), TRANSF_REF) \
  XX(MessageName, int, componentIndex, NDTInteger, int, NCTNone, DEFVALUE(int, -1), DOC_MSG("Index of hit convex component"), TRANSF) \
  XX(MessageName, RString, type, NDTString, RString, NCTDefault, DEFVALUE(RString, ""), DOC_MSG("Ammunition type"), TRANSF) \
  XX(MessageName, Vector3, pos, NDTVector, Vector3, NCTNone, DEFVALUE(Vector3, VZero), DOC_MSG("Explosion position"), TRANSF) \
  XX(MessageName, Vector3, surfNormal, NDTVector, Vector3, NCTNone, DEFVALUE(Vector3, VUp), DOC_MSG("Surface normal at the explosion"), TRANSF) \
  XX(MessageName, Vector3, inSpeed, NDTVector, Vector3, NCTNone, DEFVALUE(Vector3, VZero), DOC_MSG("Speed of the corresponding bullet/shell/missile"), TRANSF) \
  XX(MessageName, Vector3, outSpeed, NDTVector, Vector3, NCTNone, DEFVALUE(Vector3, VZero), DOC_MSG("Result speed (if any)"), TRANSF) \
  XX(MessageName, bool, enemyDamage, NDTBool, bool, NCTNone, DEFVALUE(bool, false), DOC_MSG("Some enemy was damaged"), TRANSF) \
  XX(MessageName, float, energyFactor, NDTFloat, float, NCTFloatMostly0To1, DEFVALUE(float, 1), DOC_MSG("Energy coefficient for distant hits"), TRANSF) \
  XX(MessageName, float, explosionFactor, NDTFloat, float, NCTFloat0To1, DEFVALUE(float, 0), DOC_MSG("How large explosion is"), TRANSF)

DECLARE_NET_MESSAGE(ExplosionDamageEffects, EXPLOSION_DAMAGE_EFFECTS_MSG)


#if _VBS3

// Transmit the projectile leaving the weapon
#define FIRE_WEAPON_MSG(MessageName, XX) \
  XX(MessageName, OLinkPermO(EntityAIFull), vehicle, NDTRef, NetworkId,  NCTNone, DEFVALUENULL, DOC_MSG("Firing vehicle"), TRANSF_REF) \
  XX(MessageName, OLinkPermO(Person), gunner, NDTRef, NetworkId,  NCTNone, DEFVALUENULL, DOC_MSG("Firing gunner"), TRANSF_REF) \
  XX(MessageName, OLink(EntityAI), target, NDTRef, NetworkId,  NCTNone, DEFVALUENULL, DOC_MSG("Aimed target"), TRANSF_REF) \
  XX(MessageName, Vector3, pos, NDTVector, Vector3, NCTNone, DEFVALUE(Vector3, VForward), DOC_MSG("Position of the fire"), TRANSF) \
  XX(MessageName, Vector3, dir, NDTVector, Vector3, NCTNone, DEFVALUE(Vector3, VForward), DOC_MSG("Direction of the fire"), TRANSF) \
  XX(MessageName, int, weapon, NDTInteger, int, NCTSmallSigned, DEFVALUE(int, 0), DOC_MSG("Firing weapon index"), TRANSF) \
  XX(MessageName, int, magazineCreator, NDTInteger, int, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG("Fired magazine id"), TRANSF) \
  XX(MessageName, int, magazineId, NDTInteger, int, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG("Fired magazine id"), TRANSF) \
  XX(MessageName, float, visible, NDTFloat, float, NCTNone, DEFVALUE(float, 0), DOC_MSG("How much is the fire visible"), TRANSF) \
  XX(MessageName, OLink(Entity), projectile, NDTRef, NetworkId,  NCTNone, DEFVALUENULL, DOC_MSG("Projectile leaving weapon"), TRANSF_REF)

#else

#define FIRE_WEAPON_MSG(MessageName, XX) \
  XX(MessageName, OLinkPermO(EntityAIFull), vehicle, NDTRef, NetworkId,  NCTNone, DEFVALUENULL, DOC_MSG("Firing vehicle"), TRANSF_REF) \
  XX(MessageName, OLinkPermO(Person), gunner, NDTRef, NetworkId,  NCTNone, DEFVALUENULL, DOC_MSG("Firing gunner"), TRANSF_REF) \
  XX(MessageName, OLink(EntityAI), target, NDTRef, NetworkId,  NCTNone, DEFVALUENULL, DOC_MSG("Aimed target"), TRANSF_REF) \
  XX(MessageName, Vector3, pos, NDTVector, Vector3, NCTNone, DEFVALUE(Vector3, VForward), DOC_MSG("Position of the fire"), TRANSF) \
  XX(MessageName, Vector3, dir, NDTVector, Vector3, NCTNone, DEFVALUE(Vector3, VForward), DOC_MSG("Direction of the fire"), TRANSF) \
  XX(MessageName, int, weapon, NDTInteger, int, NCTSmallSigned, DEFVALUE(int, 0), DOC_MSG("Firing weapon index"), TRANSF) \
  XX(MessageName, int, magazineCreator, NDTInteger, int, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG("Fired magazine id"), TRANSF) \
  XX(MessageName, int, magazineId, NDTInteger, int, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG("Fired magazine id"), TRANSF) \
  XX(MessageName, float, visible, NDTFloat, float, NCTNone, DEFVALUE(float, 0), DOC_MSG("How much is the fire visible"), TRANSF)

#endif

DECLARE_NET_MESSAGE(FireWeapon, FIRE_WEAPON_MSG)

#define ADD_WEAPON_CARGO_MSG(MessageName, XX) \
  XX(MessageName, OLinkO(EntityAI), vehicle, NDTRef, NetworkId,  NCTNone, DEFVALUENULL, DOC_MSG("Asked vehicle"), TRANSF_REF) \
  XX(MessageName, RString, weapon, NDTString, RString, NCTDefault, DEFVALUE(RString, ""), DOC_MSG("Name of weapon type to add"), TRANSF)

DECLARE_NET_MESSAGE_INIT_MSG(AddWeaponCargo, ADD_WEAPON_CARGO_MSG)

#define REMOVE_WEAPON_CARGO_MSG(MessageName, XX) \
  XX(MessageName, OLinkO(EntityAI), vehicle, NDTRef, NetworkId,  NCTNone, DEFVALUENULL, DOC_MSG("Asked vehicle"), TRANSF_REF) \
  XX(MessageName, RString, weapon, NDTString, RString, NCTDefault, DEFVALUE(RString, ""), DOC_MSG("Name of weapon type to remove"), TRANSF)

DECLARE_NET_MESSAGE_INIT_MSG(RemoveWeaponCargo, REMOVE_WEAPON_CARGO_MSG)

#define CLEAR_WEAPON_CARGO_MSG(MessageName, XX) \
  XX(MessageName, OLinkO(EntityAI), vehicle, NDTRef, NetworkId,  NCTNone, DEFVALUENULL, DOC_MSG("Asked vehicle"), TRANSF_REF)

DECLARE_NET_MESSAGE_INIT_MSG(ClearWeaponCargo, CLEAR_WEAPON_CARGO_MSG)

#define ADD_MAGAZINE_CARGO_MSG(MessageName, XX) \
  XX(MessageName, OLinkO(EntityAI), vehicle, NDTRef, NetworkId,  NCTNone, DEFVALUENULL, DOC_MSG("Asked vehicle"), TRANSF_REF) \
  XX(MessageName, Ref<Magazine>, magazine, NDTObjectSRef, REF_MSG(NetworkMessageMagazine), NCTNone, DEFVALUE_MSG(NMTMagazine), DOC_MSG("Magazine to add"), TRANSF_CONTENT)

DECLARE_NET_MESSAGE_INIT_MSG(AddMagazineCargo, ADD_MAGAZINE_CARGO_MSG)

#define ADD_BACKPACK_CARGO_MSG(MessageName, XX) \
  XX(MessageName, OLinkO(EntityAI), vehicle, NDTRef, NetworkId,  NCTNone, DEFVALUENULL, DOC_MSG("Asked vehicle"), TRANSF_REF) \
  XX(MessageName, OLinkO(EntityAI), backpack, NDTRef, NetworkId,  NCTNone, DEFVALUENULL, DOC_MSG("Asked backpack"), TRANSF_REF)

DECLARE_NET_MESSAGE_INIT_MSG(AddBackpackCargo, ADD_BACKPACK_CARGO_MSG)

#define REMOVE_BACKPACK_CARGO_MSG(MessageName, XX) \
  XX(MessageName, OLinkO(EntityAI), vehicle, NDTRef, NetworkId,  NCTNone, DEFVALUENULL, DOC_MSG("Asked vehicle"), TRANSF_REF) \
  XX(MessageName, OLinkO(EntityAI), backpack, NDTRef, NetworkId,  NCTNone, DEFVALUENULL, DOC_MSG("Asked backpack"), TRANSF_REF)

DECLARE_NET_MESSAGE_INIT_MSG(RemoveBackpackCargo, REMOVE_BACKPACK_CARGO_MSG)

#define UPDATE_WEAPONS_MSG(MessageName, XX) \
  XX(MessageName, OLinkPermO(EntityAIFull), vehicle, NDTRef, NetworkId,  NCTNone, DEFVALUENULL, DOC_MSG("Vehicle to update"), TRANSF_REF) \
  XX(MessageName, OLinkPermNO(Turret), turret, NDTRef, NetworkId, NCTNone, DEFVALUENULL, DOC_MSG("Turret to update"), TRANSF_REF) \
  XX(MessageName, OLinkPermO(Person), gunner, NDTRef, NetworkId, NCTNone, DEFVALUENULL, DOC_MSG("Gunner controlling the weapons"), TRANSF_REF) \
  XX(MessageName, UpdateEntityAIWeaponsMessage, weapons, NDTObject, REF_MSG(NetworkMessageUpdateEntityAIWeapons), NCTNone, DEFVALUE_MSG(NMTUpdateEntityAIWeapons), DOC_MSG("Weapons and magazines"), TRANSF_OBJECT)

DECLARE_NET_INDICES(UpdateWeapons, UPDATE_WEAPONS_MSG)

#define SET_ROLE_INDEX_MSG(MessageName, XX) \
  XX(MessageName, OLinkPermNO(AIUnit), unit, NDTRef, NetworkId,  NCTNone, DEFVALUENULL, DOC_MSG("Unit to update"), TRANSF_REF) \
  XX(MessageName, int, roleIndex, NDTInteger, int, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG("New role index"), TRANSF)

DECLARE_NET_MESSAGE(SetRoleIndex,SET_ROLE_INDEX_MSG)

#define REVEAL_TARGET_MSG(MessageName, XX) \
  XX(MessageName, int, to, NDTInteger, int, NCTNone, DEFVALUE(int, 0), DOC_MSG("Id of player where update must be processed"), TRANSF) \
  XX(MessageName, OLinkPermNO(AIGroup), group, NDTRef, NetworkId,  NCTNone, DEFVALUENULL, DOC_MSG("Group to update"), TRANSF_REF) \
  XX(MessageName, OLinkO(EntityAI), target, NDTRef, NetworkId,  NCTNone, DEFVALUENULL, DOC_MSG("Target to update"), TRANSF_REF)

DECLARE_NET_MESSAGE(RevealTarget,REVEAL_TARGET_MSG)

#define ASK_FOR_STATS_WRITE_MSG(MessageName, XX) \
  XX(MessageName, int, board, NDTInteger, int, NCTSmallSigned, DEFVALUE(int, 0), DOC_MSG("ID of statistics leaderboard"), TRANSF) \
  XX(MessageName, AutoArray<XONLINE_STAT>, stats, NDTXOnlineStatArray, AutoArray<XONLINE_STAT>, NCTNone, DEFVALUEXONLINESTATARRAY, DOC_MSG("Statistics entries"), TRANSF) \
  XX(MessageName, int, boardTotal, NDTInteger, int, NCTSmallSigned, DEFVALUE(int, 0), DOC_MSG("ID of statistics leaderboard (for total)"), TRANSF) \
  XX(MessageName, AutoArray<XONLINE_STAT>, statsTotal, NDTXOnlineStatArray, AutoArray<XONLINE_STAT>, NCTNone, DEFVALUEXONLINESTATARRAY, DOC_MSG("Statistics entries (for total)"), TRANSF)

DECLARE_NET_MESSAGE(AskForStatsWrite,ASK_FOR_STATS_WRITE_MSG)

#define VOTE_MISSION_MSG(MessageName, XX) \
  XX(MessageName, RString, name, NDTString, RString, NCTNone, DEFVALUE_DEF(RString), DOC_MSG("Name of the mission"), TRANSF) \
  XX(MessageName, RString, diff, NDTString, RString, NCTNone, DEFVALUE_DEF(RString), DOC_MSG("Wanted difficulty"), TRANSF) \

DECLARE_NET_MESSAGE(VoteMission,VOTE_MISSION_MSG)

#define SERVER_TIMEOUT_MSG(MessageName, XX) \
  XX(MessageName, int, timeout, NDTInteger, int, NCTSmallSigned, DEFVALUE(int, 0), DOC_MSG("Timeout in seconds"), TRANSF) \

DECLARE_NET_MESSAGE(ServerTimeout,SERVER_TIMEOUT_MSG)

//! Message is sent to update of weapons to vehicle owner
struct UpdateWeaponsMessage : public NetworkSimpleObject
{
  //! vehicle to update
  OLinkPermO(EntityAIFull) vehicle;
  //! turret to update
  OLinkPermNO(Turret) turret;
  //! gunner
  OLinkPermO(Person) gunner;

  DECLARE_DEFINE_NETWORK_OBJECT_SIMPLE(UpdateWeapons)
  static NetworkMessageFormat &CreateFormat
  (
    NetworkMessageClass cls,
    NetworkMessageFormat &format
  );
  TMError TransferMsg(NetworkMessageContext &ctx);
};

#define VEHICLE_DESTROYED_MSG(MessageName, XX) \
  XX(MessageName, OLinkO(EntityAI), killed, NDTRef, NetworkId,  NCTNone, DEFVALUENULL, DOC_MSG("Destroyed vehicle"), TRANSF_REF) \
  XX(MessageName, OLinkPermO(EntityAI), killer, NDTRef, NetworkId,  NCTNone, DEFVALUENULL, DOC_MSG("Who is responsible for destroying"), TRANSF_REF)

DECLARE_NET_MESSAGE(VehicleDestroyed, VEHICLE_DESTROYED_MSG)

#define MARKER_DELETE_MSG(MessageName, XX) \
  XX(MessageName, RString, name, NDTString, RString, NCTNone, DEFVALUE(RString, ""), DOC_MSG("Name of marker"), TRANSF)

DECLARE_NET_MESSAGE(MarkerDelete, MARKER_DELETE_MSG)

struct NetworkMessageMarker;

#define MARKER_CREATE_MSG(MessageName, XX) \
  XX(MessageName, int, channel, NDTInteger, int, NCTSmallSigned, DEFVALUE(int, 0), DOC_MSG("Chat channel (who will see the marker)"), TRANSF) \
  XX(MessageName, OLinkPermNO(AIBrain), sender, NDTRef, NetworkId,  NCTNone, DEFVALUENULL, DOC_MSG("Sender unit"), TRANSF_REF) \
  XX(MessageName, RefArray<NetworkObject>, units, NDTRefArray, AutoArray<NetworkId>, NCTNone, DEFVALUEREFARRAY, DOC_MSG("List of receiving units"), TRANSF_REFS) \
  XX(MessageName, ArcadeMarkerInfo, marker, NDTObject, REF_MSG(NetworkMessageMarker), NCTNone, DEFVALUE_MSG(NMTMarker), DOC_MSG("Marker object to create"), TRANSF_OBJECT)

DECLARE_NET_MESSAGE(MarkerCreate, MARKER_CREATE_MSG)

#define WAYPOINT_CREATE_MSG(MessageName, XX) \
  XX(MessageName, OLinkPermNO(AIGroup), group, NDTRef, NetworkId,  NCTNone, DEFVALUENULL, DOC_MSG("Owner of the waypoint"), TRANSF_REF) \
  XX(MessageName, int, index, NDTInteger, int, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG("Index of the waypoint in the owner's list"), TRANSF) \
  XX(MessageName, WaypointInfo, waypoint, NDTObject, REF_MSG(NetworkMessageWaypoint), NCTNone, DEFVALUE_MSG(NMTWaypoint), DOC_MSG("Waypoint object"), TRANSF_OBJECT) \

DECLARE_NET_MESSAGE_INIT_MSG(WaypointCreate, WAYPOINT_CREATE_MSG)

#define WAYPOINT_UPDATE_MSG(MessageName, XX) \
  XX(MessageName, OLinkPermNO(AIGroup), group, NDTRef, NetworkId,  NCTNone, DEFVALUENULL, DOC_MSG("Owner of the waypoint"), TRANSF_REF) \
  XX(MessageName, int, index, NDTInteger, int, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG("Index of the waypoint in the owner's list"), TRANSF) \
  XX(MessageName, WaypointInfo, waypoint, NDTObject, REF_MSG(NetworkMessageWaypoint), NCTNone, DEFVALUE_MSG(NMTWaypoint), DOC_MSG("Waypoint object"), TRANSF_OBJECT) \

DECLARE_NET_MESSAGE_INIT_MSG(WaypointUpdate, WAYPOINT_UPDATE_MSG)

#define WAYPOINT_DELETE_MSG(MessageName, XX) \
  XX(MessageName, OLinkPermNO(AIGroup), group, NDTRef, NetworkId,  NCTNone, DEFVALUENULL, DOC_MSG("Owner of the waypoint"), TRANSF_REF) \
  XX(MessageName, int, index, NDTInteger, int, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG("Index of the waypoint in the owner's list"), TRANSF) \
  XX(MessageName, WaypointInfo, waypoint, NDTObject, REF_MSG(NetworkMessageWaypoint), NCTNone, DEFVALUE_MSG(NMTWaypoint), DOC_MSG("Waypoint object"), TRANSF_OBJECT) \

DECLARE_NET_MESSAGE_INIT_MSG(WaypointDelete, WAYPOINT_DELETE_MSG)

#define WAYPOINTS_COPY_MSG(MessageName, XX) \
  XX(MessageName, OLinkPermNO(AIGroup), to, NDTRef, NetworkId,  NCTNone, DEFVALUENULL, DOC_MSG("Target for the waypoints"), TRANSF_REF) \
  XX(MessageName, OLinkPermNO(AIGroup), from, NDTRef, NetworkId,  NCTNone, DEFVALUENULL, DOC_MSG("Source of the waypoints"), TRANSF_REF) \

DECLARE_NET_MESSAGE_INIT_MSG(WaypointsCopy, WAYPOINTS_COPY_MSG)

#define HC_SET_GROUP_MSG(MessageName, XX) \
  XX(MessageName, OLinkPermNO(AIUnit), unit, NDTRef, NetworkId,  NCTNone, DEFVALUENULL, DOC_MSG("Unit to update"), TRANSF_REF) \
  XX(MessageName, OLinkPermNO(AIGroup), group, NDTRef, NetworkId,  NCTNone, DEFVALUENULL, DOC_MSG("Owner of the waypoint"), TRANSF_REF) \
  XX(MessageName, int, team, NDTInteger, int, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG("Team index."), TRANSF) \
  XX(MessageName, RString, name, NDTString, RString, NCTDefault, DEFVALUE(RString, ""), DOC_MSG("Name of group."), TRANSF) \

DECLARE_NET_MESSAGE_INIT_MSG(HCSetGroup, HC_SET_GROUP_MSG)

#define HC_REMOVE_GROUP_MSG(MessageName, XX) \
  XX(MessageName, OLinkPermNO(AIUnit), unit, NDTRef, NetworkId,  NCTNone, DEFVALUENULL, DOC_MSG("Unit to update"), TRANSF_REF) \
  XX(MessageName, OLinkPermNO(AIGroup), group, NDTRef, NetworkId,  NCTNone, DEFVALUENULL, DOC_MSG("Owner of the waypoint"), TRANSF_REF) \

DECLARE_NET_MESSAGE_INIT_MSG(HCRemoveGroup, HC_REMOVE_GROUP_MSG)

#define HC_CLEAR_GROUPS_MSG(MessageName, XX) \
  XX(MessageName, OLinkPermNO(AIUnit), unit, NDTRef, NetworkId,  NCTNone, DEFVALUENULL, DOC_MSG("Unit to update"), TRANSF_REF) \

DECLARE_NET_MESSAGE_INIT_MSG(HCClearGroups, HC_CLEAR_GROUPS_MSG)

#define GROUP_UNCONSCIOUS_LEADER_MSG(MessageName, XX) \
  XX(MessageName, OLinkPermNO(AIUnit), unit, NDTRef, NetworkId,  NCTNone, DEFVALUENULL, DOC_MSG("Unit to update"), TRANSF_REF) \
  XX(MessageName, OLinkPermNO(AIGroup), group, NDTRef, NetworkId,  NCTNone, DEFVALUENULL, DOC_MSG("Owner of the waypoint"), TRANSF_REF) \

DECLARE_NET_MESSAGE_INIT_MSG(GroupSetUnconsciousLeader, GROUP_UNCONSCIOUS_LEADER_MSG)

#define GROUP_SELECT_LEADER_MSG(MessageName, XX) \
  XX(MessageName, OLinkPermNO(AIUnit), unit, NDTRef, NetworkId,  NCTNone, DEFVALUENULL, DOC_MSG("Unit to update"), TRANSF_REF) \
  XX(MessageName, OLinkPermNO(AIGroup), group, NDTRef, NetworkId,  NCTNone, DEFVALUENULL, DOC_MSG("Owner of the waypoint"), TRANSF_REF) \

DECLARE_NET_MESSAGE_INIT_MSG(GroupSelectLeader, GROUP_SELECT_LEADER_MSG)

#define LOADED_FROM_SAVE_MSG(MessageName, XX)

DECLARE_NET_MESSAGE(LoadedFromSave, LOADED_FROM_SAVE_MSG)

#if _ENABLE_VBS

#define SET_OBJECT_TEXTURE_MSG(MessageName, XX) \
  XX(MessageName, OLink(EntityAI), veh, NDTRef, NetworkId,  NCTNone, DEFVALUENULL, DOC_MSG("Vehicle that will have a texture changed"), TRANSF_REF) \
  XX(MessageName, int, index, NDTInteger, int, NCTSmallUnsigned, DEFVALUE(int, -1), DOC_MSG("Index of selection to set texture for"), TRANSF) \
  XX(MessageName, RString, name, NDTString, RString, NCTDefault, DEFVALUE(RString, ""), DOC_MSG("Name of texture"), TRANSF) \
  XX(MessageName, RString, eval, NDTString, RString, NCTDefault, DEFVALUE(RString, "true"), DOC_MSG("Evaluation - must evaluate to true if texture is to be set on client"), TRANSF)

DECLARE_NET_MESSAGE(SetObjectTexture, SET_OBJECT_TEXTURE_MSG)

#define PUBLICEXEC_MSG(MessageName, XX) \
  XX(MessageName, RString, condition, NDTString, RString, NCTDefault, DEFVALUE(RString, ""), DOC_MSG("Condition to evaluate, if true then command will be executed"), TRANSF) \
  XX(MessageName, RString, command, NDTString, RString, NCTDefault, DEFVALUE(RString, ""), DOC_MSG("Command to execute if evaluation is true"), TRANSF) \
  XX(MessageName, OLink(Object), obj, NDTRef, NetworkId, NCTNone, DEFVALUENULL, DOC_MSG("Object passed into condition and command as _this"), TRANSF_REF) \
  XX(MessageName, OLinkArray(Object), objs, NDTRefArray, AutoArray<NetworkId>, NCTNone, DEFVALUEREFARRAY, DOC_MSG("Joining units"), TRANSF_REFS)

DECLARE_NET_MESSAGE(PublicExec, PUBLICEXEC_MSG)

#endif // _ENABLE_VBS

#define TRANSFER_FILE_MSG(MessageName, XX) \
  XX(MessageName, RString, path, NDTString, RString, NCTNone, DEFVALUE(RString, ""), DOC_MSG("Path of transferred file"), TRANSF) \
  XX(MessageName, AutoArray<char>, data, NDTRawData, AutoArray<char>, NCTNone, DEFVALUERAWDATA, DOC_MSG("Content of file (single segment)"), TRANSF) \
  XX(MessageName, int, totSize, NDTInteger, int, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG("Total size of file"), TRANSF) \
  XX(MessageName, int, offset, NDTInteger, int, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG("Offset of this segment"), TRANSF) \
  XX(MessageName, int, totSegments, NDTInteger, int, NCTSmallUnsigned, DEFVALUE(int, 1), DOC_MSG("Total number of segments"), TRANSF) \
  XX(MessageName, int, curSegment, NDTInteger, int, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG("Index of this segment"), TRANSF)

DECLARE_NET_MESSAGE(TransferFile, TRANSFER_FILE_MSG)

#define DROP_BACKPACK_MSG(MessageName, XX) \
  XX(MessageName, OLinkPermNO(EntityAI), backpack, NDTRef, NetworkId,  NCTNone, DEFVALUENULL, DOC_MSG("backpack to drop"), TRANSF_REF) \
  XX(MessageName, Vector3, pos, NDTVector, Vector3, NCTNone, DEFVALUE(Vector3, VZero), DOC_MSG("New position"), TRANSF) \
  XX(MessageName, Matrix3, orient, NDTMatrix, Matrix3, NCTNone, DEFVALUE(Matrix3, M3Identity), DOC_MSG("New orientation"), TRANSF)

DECLARE_NET_MESSAGE_INIT_MSG(DropBackpack, DROP_BACKPACK_MSG)

#define TAKE_BACKPACK_MSG(MessageName, XX) \
  XX(MessageName, OLinkPermNO(EntityAI), backpack, NDTRef, NetworkId,  NCTNone, DEFVALUENULL, DOC_MSG("backpack to take"), TRANSF_REF) \
  XX(MessageName, OLinkPermO(Person), soldier, NDTRef, NetworkId,  NCTNone, DEFVALUENULL, DOC_MSG(""), TRANSF_REF) \

DECLARE_NET_MESSAGE_INIT_MSG(TakeBackpack, TAKE_BACKPACK_MSG)

#define ASSEMBLE_MSG(MessageName, XX) \
  XX(MessageName, OLinkPermNO(EntityAI), weapon, NDTRef, NetworkId,  NCTNone, DEFVALUENULL, DOC_MSG("backpack to take"), TRANSF_REF) \
  XX(MessageName, Matrix3, transform, NDTMatrix, Matrix3, NCTNone, DEFVALUE(Matrix3, M3Identity), DOC_MSG("New orientation"), TRANSF) \
  XX(MessageName, Vector3, position, NDTVector, Vector3, NCTNone, DEFVALUE(Vector3, VZero), DOC_MSG("New position"), TRANSF)

DECLARE_NET_MESSAGE_INIT_MSG(Assemble, ASSEMBLE_MSG)

#define DISASSEMBLE_MSG(MessageName, XX) \
  XX(MessageName, OLinkPermNO(EntityAI), weapon, NDTRef, NetworkId,  NCTNone, DEFVALUENULL, DOC_MSG("disassembled weapon"), TRANSF_REF) \
  XX(MessageName, OLinkPermNO(EntityAI), backpack, NDTRef, NetworkId,  NCTNone, DEFVALUENULL, DOC_MSG(""), TRANSF_REF)

DECLARE_NET_MESSAGE_INIT_MSG(DisAssemble, DISASSEMBLE_MSG)

//! Message for transfer mission pbo file to other clients
struct TransferMissionFileMessage : public TransferFileMessage
{
  DECLARE_DEFINE_NETWORK_OBJECT_SIMPLE(TransferMissionFile)
};

#define TRANSFER_MISSION_FILE_MSG(MessageName, XX)

DECLARE_NET_INDICES_EX(TransferMissionFile, TransferFile, TRANSFER_MISSION_FILE_MSG)

//! Message for transfer file to server
struct TransferFileToServerMessage : public TransferFileMessage
{
  DECLARE_DEFINE_NETWORK_OBJECT_SIMPLE(TransferFileToServer)
};

#define TRANSFER_FILE_TO_SERVER_MSG(MessageName, XX)

DECLARE_NET_INDICES_EX(TransferFileToServer, TransferFile, TRANSFER_FILE_TO_SERVER_MSG)

#define ASK_MISSION_FILE_MSG(MessageName, XX) \
  XX(MessageName, bool, valid, NDTBool, bool, NCTNone, DEFVALUE(bool, false), DOC_MSG("Mission file is valid (present on client computer)"), TRANSF)

DECLARE_NET_MESSAGE(AskMissionFile, ASK_MISSION_FILE_MSG)

#define SHOW_TARGET_MSG(MessageName, XX) \
  XX(MessageName, OLinkPermO(Person), vehicle, NDTRef, NetworkId,  NCTNone, DEFVALUENULL, DOC_MSG("Player person"), TRANSF_REF) \
  XX(MessageName, OLink(TargetType), target, NDTRef, NetworkId,  NCTNone, DEFVALUENULL, DOC_MSG("Target to show"), TRANSF_REF)

DECLARE_NET_MESSAGE(ShowTarget, SHOW_TARGET_MSG)

#define SHOW_GROUP_DIR_MSG(MessageName, XX) \
  XX(MessageName, OLinkPermO(Person), vehicle, NDTRef, NetworkId,  NCTNone, DEFVALUENULL, DOC_MSG("Player person"), TRANSF_REF) \
  XX(MessageName, Vector3, dir, NDTVector, Vector3, NCTNone, DEFVALUE(Vector3, VForward), DOC_MSG("Direction to show"), TRANSF)

DECLARE_NET_MESSAGE(ShowGroupDir, SHOW_GROUP_DIR_MSG)

//! Single message in agregated message
struct NetworkMessageQueueItem
{
  //! message type
  NetworkMessageType type;
  //! message itself
  Ref<NetworkMessage> msg;
  //! MP Save serialization
  LSError Serialize(ParamArchive &ar)
  {
    CHECK(ar.Serialize("Message", msg, 1))
    if (ar.IsLoading()) type = msg->GetNMType();
    return LSOK;      
  }
};
TypeIsMovableZeroed(NetworkMessageQueueItem)

//! Agregated message
/*!
Message joins several small messages to decrease framework overhead.
*/
class NetworkMessageQueue : public NetworkSimpleObject, public AutoArray<NetworkMessageQueueItem>
{
public:
  DECLARE_DEFINE_NETWORK_OBJECT_SIMPLE(Messages)
  static NetworkMessageFormat &CreateFormat
  (
    NetworkMessageClass cls,
    NetworkMessageFormat &format
  );
  TMError TransferMsg(NetworkMessageContext &ctx);
};

//! Encapsulates info needed for messages sent directly (inside process) instead through DirectPlay
struct NetworkLocalMessageInfo
{
  //! DirectPlay ID of sender
  int from;
  //! message type
  NetworkMessageType type;
  //! message itself
  Ref<NetworkMessage> msg;
};
TypeIsMovableZeroed(NetworkLocalMessageInfo)

///////////////////////////////////////////////////////////////////////////////
// Network Components

class NetworkManager;

//! Info about file currently receiving
struct ReceivingFile
{
  //! name of receiving file
  RString fileName;
  //! info about which segments already received
  AutoArray<bool> fileSegments;
  //! content of receiving file
  AutoArray<char> fileData;
  //! total amount of bytes already received (used for statistics)
  int received;
};
TypeIsMovable(ReceivingFile)

#if _ENABLE_CHEATS
//! Statistic info about messages of given type
/*!
\patch_internal 1.08 Date 07/26/2001 by Jirka
- Improved: network diagnostics
*/
struct NetworkStatisticsItem
{
  //! message type
  int message;
  //! count of sent messages
  int msgSent;
  //! size of sent messages
  int sizeSent;
  //! count of received messages
  int msgReceived;
  //! size of received messages
  int sizeReceived;

  //! Constructor
  NetworkStatisticsItem()
  {
    message = 0;
    msgSent = 0;
    sizeSent = 0;
    msgReceived = 0;
    sizeReceived = 0;
  };
};
TypeIsMovableZeroed(NetworkStatisticsItem)
#endif

#if _ENABLE_CHEATS || _ENABLE_DEDICATED_SERVER
//! Statistic info about messages physically send / received by players
struct NetworkRawStatisticsItem
{
  //! player ID
  int player;
  //! count of sent messages
  int msgSent;
  //! size of sent messages
  int sizeSent;
  //! count of received messages
  int msgReceived;
  //! size of received messages
  int sizeReceived;

  //! Constructor
  NetworkRawStatisticsItem()
  {
    player = 0;
    msgSent = 0;
    sizeSent = 0;
    msgReceived = 0;
    sizeReceived = 0;
  };
};
TypeIsMovableZeroed(NetworkRawStatisticsItem)
#endif

//! Basic network component (client or server)
class NetworkComponent : public INetworkComponent
{
protected:
  //! creating network manager object
  NetworkManager *_parent;  // _parent must be valid
  //! id of next created network object in this component
  int _nextId;

  //! squads of logged players
  RefArray<SquadIdentity> _squads;
  //! info about logged players
  AutoArray<PlayerIdentity> _identities;

  //! info about current mission
  MissionHeader _missionHeader;
  //! role slots
  AutoArray<PlayerRole> _playerRoles;
  //! parameter of current mission
  float _param1;
  //! parameter of current mission
  float _param2;
  //! array of parameters of current mission
  AutoArray<float> _paramsArray;
  //! used to second phase of role creation
  int _playerRoleToAttach;
  
  //! messages received local (inside process)
  AutoArray<NetworkLocalMessageInfo> _receivedLocalMessages;

  //! files currently receiving
  AutoArray<ReceivingFile> _files;
  
#if _ENABLE_CHEATS
  //! Statistics about messages by types
  AutoArray<NetworkStatisticsItem> _statistics;
#endif

#if _ENABLE_DEDICATED_SERVER || _ENABLE_CHEATS
  //! Statistics about messages by players
  AutoArray<NetworkRawStatisticsItem> _rawStatistics;
#endif

  //! list of accepted keys for data signatures
  AutoArray<DSKey> _acceptedKeys;
  //! the least signature version the server accepts
  int              _sigVerRequired;

  //! list of files (except banks), where signature will be checked
  AutoArray<RString> _additionalSignedFiles;

public:
  //! Constructor
  /*!
  \param parent creating network manager object
  */
  NetworkComponent(NetworkManager *parent);
  //! Destructor
  ~NetworkComponent();

  //! Add message to local received messages
  /*!
  \param from DirectPlay ID of sender
  \param type message type
  \param msg message itself
  \return index of message in array of local received messages
  */
  int AddLocalMessage(int from, NetworkMessageType type, NetworkMessage *msg)
  {
#if _ENABLE_CHEATS
    int to = ((from == TO_SERVER) ? BOT_CLIENT : TO_SERVER);
    int GetDiagLevel(NetworkMessageType type, bool remote);
    int level = GetDiagLevel(type,false);
    if (level >= 2)
    {
      NetworkMessageFormatBase *format = GetFormat(type);
      void DiagLogF(const char *format, ...);
      DiagLogF("%s: add local message %s from %d", GetDebugName(), cc_cast(GetMsgTypeName(type)), from);
      NetworkMessageContext ctx(msg, this, to, MSG_SEND);
      ctx.LogMessage(format, level, "\t");
    }
#endif
    
    if (!msg)
    {
      Assert(msg);
      RptF("AddLocalMessage called with NULL NetworkMessage*, from: %d, type: %d", from, (int)type);
      return 0;
    }

    int index = _receivedLocalMessages.Add();
    _receivedLocalMessages[index].from = from;
    _receivedLocalMessages[index].type = type;
    _receivedLocalMessages[index].msg = msg;

if (type == 9)
  Log("Message %d, type %d", index, type);
    return index;
  }

  //! Implementation of direct message send through DirectPlay
  /*!
  \param to DirectPlay ID of message receiver
  \param rawMsg message content
  \param msgID out parameter to store DirectPlay ID of message
  \param dwFlags DirectPlay flags
  */
  virtual bool DXSendMsg(int to, NetworkMessageRaw &rawMsg, DWORD &msgID, NetMsgFlags dwFlags) = 0;

  //! Draw any UI indications you need
  virtual void OnDraw() = 0;
  //! Component simulation (process received messages, send update messages etc.)
  virtual void OnSimulate() = 0;
  //! Process (user) message
  /*!
  \param from DirectPlay ID of sender
  \param msg message itself
  \param type message type
  */
  virtual void OnMessage(int from, NetworkMessage *msg, NetworkMessageType type) = 0;

  //! perform regular memory clean-up
  virtual unsigned CleanUpMemory() = 0;

  //! Return if given client is local on server's computer
  virtual bool IsLocalClient(int client) const {return false;}
  
  //{ DEDICATED SERVER SUPPORT
  #if _ENABLE_DEDICATED_SERVER
  //! Called when message is received to calculate transfer rate on dedicated server
  virtual void OnMonitorIn(int size) {}
  #endif
  //}

  //! Hi-level send of message
  /*!
  \param to DirectPlay ID of message receiver
  \param object message object
  \param dwFlags DirectPlay flags
  */
  virtual DWORD SendMsg
  (
    int to,
    NetworkSimpleObject *object, /*int creator, int id,*/
    NetMsgFlags dwFlags
  );

  //! Return mission header
  const MissionHeader *GetMissionHeader() const {return &_missionHeader;}
  //! Return number of role slots for current mission
  int NPlayerRoles() const 
  {
#if _VBS2_LITE
    return _playerRoles.Size() > _VBS2_MAX_PLAYERS ? _VBS2_MAX_PLAYERS : _playerRoles.Size(); 
#else
    return _playerRoles.Size();
#endif
  }
  //! Return given role slot 
  const PlayerRole *GetPlayerRole(int role) const
  {
    if (role >= 0 && role < NPlayerRoles()) return &_playerRoles[role];
    return NULL;
  }
  //! Find slot for given client
  const PlayerRole *FindPlayerRole(int player) const;
  //! Search for identity with given player id
  const PlayerIdentity *FindIdentity(int dpnid) const;
  //! Search for identity with given player id
  PlayerIdentity *FindIdentity(int dpnid);
  //! Search for identity with given user friendly player id
  const PlayerIdentity *FindIdentityByPlayerId(int playerId) const;
  //! Return array of identities
  const AutoArray<PlayerIdentity> *GetIdentities() const {return &_identities;}
  //! Retrieves parameters for current mission
  void GetParams(float &param1, float &param2) const {param1 = _param1; param2 = _param2;}
  //! Retrieves parameters for current mission
  float GetParams(int index) const {
    if(index<_paramsArray.Size()) return _paramsArray[index];
    return 0.0;
  };
  //! Retrieves parameters for current mission
  AutoArray<float> GetParamsArray() const{return _paramsArray;};

  //! Return component type ("server" / "client") for debugging purposes
  virtual const char *GetDebugName() const = 0;

  //! Decode (and process) message from raw data
  /*!
  \param from DirectPlay ID of sender
  \param src source raw data
  */
  void DecodeMessage(int from, NetworkMessageRaw &src);
  //! Calculate size of message after serialization and compression
  /*!
  \param msg calculated message
  \param type type of calculated message
  */
  int CalculateMessageSize(NetworkMessage *msg, NetworkMessageType type);

  //! File transfer
  /*!
  Split file into segments and send segments using standard messages.
  \param to DirectPlay ID of file receiver
  \param dest filename on destination computer
  \param source local filename
  */
  void TransferFile(int to, RString dest, RString source);
  /*!
  Split file into segments and store them into msgList array
  \param to DirectPlay ID of file receiver
  \param dest filename on destination computer
  \param source local filename
  */
  void PrepareTransferFileMessages(AutoArray<TransferFileMessage> &msgList, RString dest, RString source);
  //! Transfer player's face from server to client
  /*!
  \param to DirectPlay ID of face receiver
  \param player player which face is transfered
  */
  void TransferFace(int to, RString player);
  //! Transfer player's custom radio messages from server to client
  /*!
  \param to DirectPlay ID of messages receiver
  \param player player which messages are transfered
  */
  void TransferCustomRadio(int to, RString player);

#if _ENABLE_CHEATS
  //! Write into statistics info about sent message
  /*!
  \param msg message type
  \param size size of sent message
  */
  void StatMsgSent(int msg, int size);
  //! Write into statistics info about received message
  /*!
  \param msg message type
  \param size size of received message
  */
  void StatMsgReceived(int msg, int size);
  //! Return statistics about messages by types
  AutoArray<NetworkStatisticsItem> &GetStatistics() {return _statistics;}
#endif

#if _ENABLE_DEDICATED_SERVER || _ENABLE_CHEATS
  //! Write raw statistics info about sent message
  /*!
  \param msg message type
  \param to player
  */
  void StatRawMsgSent(int to, int size);
  //! Write raw statistics info about received message
  /*!
  \param msg message type
  \param from player
  */
  void StatRawMsgReceived(int from, int size);
#endif

protected:
  //! Encode message into raw data
  /*!
  \param dst destination data
  \param msg source message
  \param format format of source message
  \param type type of destination message
  */
  void EncodeMsg(NetworkMessageRaw &dst, NetworkMessage *msg, NetworkMessageFormatBase *format, NetworkMessageType type);
  //! Encode single message item into raw data
  /*!
  \param dst destination data
  \param msg source message
  \param ptr pointer to item to encode
  \param item format of source message item
  */
  void EncodeMsgItem(NetworkMessageRaw &dst, NetworkMessage *msg, void *ptr, NetworkMessageFormatItem &item);
  //! Initialize message before decoding
  /*!
  \param msg destination message
  \param type type of destination message
  */
  virtual void InitMsg(NetworkMessage *msg, NetworkMessageType type) {}
  //! Decode message from raw data
  /*!
  \param msg destination message
  \param src source data
  \param format format of destination message
  \param type type of destination message
  */
  bool DecodeMsg(NetworkMessage *msg, NetworkMessageRaw &src, NetworkMessageFormatBase *format, NetworkMessageType type);
  //! Decode single message item from raw data
  /*!
  \param src source data
  \param msg destination message
  \param ptr pointer to item to decode
  \param item format of source message item
  */
  bool DecodeMsgItem(NetworkMessageRaw &src, NetworkMessage *msg, void *ptr, NetworkMessageFormatItem &item);
  //! Create copy of message
  /*!
  \param src source message
  \param format format of source message
  */
  void CopyMsg(NetworkMessage *dst, const NetworkMessage *src, const NetworkMessageFormatBase *format) const;
  //! Calculate size of message after serialization and compression
  /*!
  \param msg calculated message
  \param format format of calculated message
  \param type type of calculated message
  */
  int CalculateMsgSize(NetworkMessage *msg, NetworkMessageFormatBase *format, NetworkMessageType type);
  //! Calculate size of single message item after serialization and compression
  /*!
  \param msg calculated message
  \param ptr calculated message item
  \param item format of calculated message item
  */
  int CalculateMsgItemSize(void *ptr, const NetworkMessageFormatItem &item);

  //! Process all received messages (local, system and user)
  void ReceiveLocalMessages();


  //! Send single network message
  /*!
  This function determine, whic type of send will be used - local send, agregation or send using DirectPlay
  \param to DirectPlay ID of message receiver
  \param msg message itself
  \param type type of message
  \param dwFlags DirectPlay flags
  */
  DWORD SendMsg
  (
    int to, NetworkMessage *msg,
    NetworkMessageType type, NetMsgFlags dwFlags
  );
  //! Send single network message using DirectPlay
  /*!
  \param to DirectPlay ID of message receiver
  \param msg message itself
  \param type type of message
  \param dwFlags DirectPlay flags
  */
  DWORD SendMsgRemote
  (
    int to, NetworkMessage *msg,
    NetworkMessageType type, NetMsgFlags dwFlags
  );
  //! Send (part of) message queue as single DirectPlay message
  /*!
  \param to DirectPlay ID of message receiver
  \param queue messages to agregate
  \param begin index of first message to send
  \param end index of first message not to send
  \param dwFlags DirectPlay flags
  */
  DWORD SendMsgQueue
  (
    int to, NetworkMessageQueue &queue,
    int begin, int end, NetMsgFlags dwFlags
  );
  //! Lo-level data send using DirectPlay
  /*!
  \param to DirectPlay ID of message receiver
  \param rawMsg data to send
  \param dwFlags DirectPlay flags
  \param diagLevel level of diagnostics about message
  */
  DWORD SendMsgRaw(int to, NetworkMessageRaw &rawMsg, NetMsgFlags dwFlags, int diagLevel);
  //! Send single guaranteed network message to message queue (candidate for agregation)
  /*!
  \param to DirectPlay ID of message receiver
  \param msg message itself
  \param type type of message
  */
  virtual void EnqueueMsg
  (
    int to, NetworkMessage *msg,
    NetworkMessageType type
  ) = 0;
  //! Send single nonguaranteed network message to message queue (candidate for agregation)
  /*!
  \param to DirectPlay ID of message receiver
  \param msg message itself
  \param type type of message
  */
  virtual void EnqueueMsgNonGuaranteed
  (
    int to, NetworkMessage *msg,
    NetworkMessageType type
  ) = 0;

  //! Called when file segment is received
  /*!
  \param msg segment of file
  \return 1 if whole file is received (and saved)
  \return -1 if whole file is received, but with errors (save failed)
  */
  int ReceiveFileSegment(TransferFileMessage &msg);
};

//! Info about single type of update of network object
struct NetworkUpdateInfo
{
  //! last created update message
  Ref<NetworkMessage> lastCreatedMsg;
  //! DirectPlay ID of last created update message
  DWORD lastCreatedMsgId;
  //! send time of created update message
  DWORD lastCreatedMsgTime;
  //! last sent update message (sent was confirmed by DPN_MSGID_SEND_COMPLETE system message)
  Ref<NetworkMessage> lastSentMsg;
  //! cached value of error
  float errorStatic;
  //! cached value is valid
  bool errorValid;
  //! message can be canceled (false for initial update message)
  bool canCancel;

  NetworkUpdateInfo()
  {
    // avoid floating point exceptions
    errorStatic = 0;
    errorValid = false;
  }
  // update structure - pending message send was completed
  bool OnSendComplete(bool ok);
};

//! Info about local object on client
struct NetworkLocalObjectInfo //: public RefCount
{
  //! ID of object
  NetworkId id;
  //! object itself
  OLink(NetworkObject) object;
  //! state of updates
  NetworkUpdateInfo updates[NMCUpdateN];
};
TypeContainsOLink(NetworkLocalObjectInfo)
//TypeIsGeneric(NetworkLocalObjectInfo)

//! Info about remote object on client
struct NetworkRemoteObjectInfo
{
  //! ID of object
  NetworkId id;
  //! object itself
  OLink(NetworkObject) object;
};
TypeContainsOLink(NetworkRemoteObjectInfo)

class RespawnQueueItem;

//! map sounds transferred over network to their unique ids
struct PlaySoundInfo
{
  //! unique id of sound
  int creator,id;
  //! sound itself
  Link<AbstractWave> wave;
};
TypeIsGeneric(PlaySoundInfo)

//! map directly speaing player to object and sound buffer
struct Network3DSoundBuffer
{
  //! DirectPlay ID of speaking player
  int player;
  //! DirectSound buffer used for speaking player
  SRef<NetTranspSound3DBuffer> buffer;
  //! sound source object
  OLink(NetworkObject) object;
};
TypeContainsOLink(Network3DSoundBuffer)

//! Class used for transfer of client info (for example camera position) to server
/*!
\patch_internal 1.27 Date 10/11/2001 by Jirka
- Improved: camera position transfer in multiplayer game
*/
class ClientInfoObject : public NetworkObject
{
protected:
  NetworkId _id;

public:
  Vector3 _cameraPosition;
  int     _dpnid;   // Direct play id (necessary for hearing of VoN Direct Speech for dead players)

#if _VBS3
  Vector3 _cameraDirection;
  float   _cameraFov;
#endif

public:
  // Constructor
  ClientInfoObject()
  {
    _cameraPosition = InvalidCamPos;
#if _VBS3
    _cameraDirection = VForwardP;
    _cameraFov = 0.75;
#endif
  }

  void SetNetworkId(NetworkId &id) {_id = id;}
  NetworkId GetNetworkId() const {return _id;}
  Vector3 GetCurrentPosition() const {return VZero;}
  bool IsLocal() const {return true;}
  void SetLocal(bool local = true) {}
  void DestroyObject() {}
  
  DECLARE_NETWORK_OBJECT
  static NetworkMessageFormat &CreateFormat
  (
    NetworkMessageClass cls,
    NetworkMessageFormat &format
  );
  TMError TransferMsg(NetworkMessageContext &ctx);
  float CalculateError(NetworkMessageContextWithError &ctx);
  static float CalculateErrorCoef(Vector3Par position, Vector3Par cameraPosition) {return 1.0;}
  RString GetDebugName() const;
};

#if !_VBS3
#define UPDATE_CLIENT_INFO_MSG(MessageName, XX) \
  XX(MessageName, Vector3, cameraPosition, NDTVector, Vector3, NCTVectorPositionCamera, DEFVALUE(Vector3, InvalidCamPos), DOC_MSG("Position of camera on client"), TRANSF, ET_ABS_DIF, 1)

DECLARE_NET_INDICES_EX_ERR(UpdateClientInfo, NetworkObject, UPDATE_CLIENT_INFO_MSG)
#endif

#define UPDATE_CLIENT_INFO_DPID_MSG(MessageName, XX) \
  XX(MessageName, Vector3, cameraPosition, NDTVector, Vector3, NCTVectorPositionCamera, DEFVALUE(Vector3, InvalidCamPos), DOC_MSG("Position of camera on client"), TRANSF, ET_ABS_DIF, 1) \
  XX(MessageName, int, dpnid, NDTInteger, int, NCTNone, DEFVALUE(int, 0), DOC_MSG("Direct play id of dead player."), TRANSF, ET_NOT_EQUAL, 0)

DECLARE_NET_INDICES_EX_ERR(UpdateClientInfoDpid, NetworkObject, UPDATE_CLIENT_INFO_DPID_MSG)

struct UpdateLocalObjectInfo;

struct BodyInfo
{
  OLinkPermO(Person) body;
  Time hideTime;
  float value;
};
TypeContainsOLink(BodyInfo)

struct WreckInfo
{
  OLinkPermO(Transport) wreck;
  Time hideTime;
};
TypeContainsOLink(WreckInfo)

class NetworkServer;
void DiagLogF(const char *format, ...);

/// store event handler executed in reaction to PublicVariableMessage
struct PublicVariableEventHandler
{
  RString _name;
#if USE_PRECOMPILATION
  Ref<GameDataCode> _code;
#else
  RString _code;
#endif

  const char *GetKey() const {return _name;}
  LSError Serialize(ParamArchive &ar);
};
TypeIsMovableZeroed(PublicVariableEventHandler);

template <>
struct MapClassTraits<PublicVariableEventHandler> : public MapClassTraitsNoCase<PublicVariableEventHandler>
{
};

/// set of all registered event handlers executed in reaction to PublicVariableMessage
typedef MapStringToClass< PublicVariableEventHandler, AutoArray<PublicVariableEventHandler> > PublicVariableEventHandlers;

//! Network Command Types
/*!
\patch_internal 1.11 Date 07/31/2001 by Jirka
- Added: processing of commands entered in chat line
*/
enum NetworkCommandType
{
  CMDNone = -1,
  CMDLogin,
  CMDLogout,
  CMDKick,
  CMDRestart,
  CMDMission,
  CMDMissions,
  CMDShutdown,
  CMDReassign,
  CMDMonitor,
  CMDUserlist,
  CMDVote,
  CMDAdmin,
  CMDInit,
  CMDDebug,
  CMDExec,
  CMDLock,
  CMDUnlock,
  CMDBEServer,
  CMDBEClient
#if _VBS3
  ,CMDExitAll
#endif
};

struct CheckSignatureItem
{
  int _bankIndex;
  int _sigVer;
  int _level;
  //! network message type and the prepared message itself (NMTFileSignatureAnswer or NMTDataSignatureAnswer)
  int _type;
  NetworkSimpleObject *_msg;
  CheckSignatureItem() : _msg(NULL) {}
  ~CheckSignatureItem() 
  { 
    if(_msg!=NULL) 
    { 
       delete _msg; 
      _msg=NULL; 
    } 
  }
};
TypeIsMovable(CheckSignatureItem)

template <>
struct FindArrayKeyTraits<CheckSignatureItem>
{
  typedef int KeyType;
  static bool IsEqual(int a, int b) {return a == b;}
  static int GetKey(const CheckSignatureItem &a) {return a._bankIndex;}
};

//! Network Client
class NetworkClient : public NetworkComponent
{
protected:
  //! DirectPlay ID of attached player
  int _player;

  //! low-level client implementation
  SRef<NetTranspClient> _client;

#if _USE_BATTL_EYE_CLIENT
  /// BE Integration
  class BattlEye
  {
  protected:
    /// dll handle
    HMODULE _library;
    bool _initialized;

    //@{
    /// callback functions
    typedef void __cdecl PrintMessageF(const char *message);
    static void __cdecl PrintMessage(const char *message);
    //@}

    //@{
    /// dll functions
    typedef bool __cdecl ExitF();
    typedef bool __cdecl RunF();
    typedef void __cdecl CommandF(const char *command);
    typedef bool __cdecl InitF(unsigned long address, unsigned short port, PrintMessageF *fnPrintMessage, 
                               ExitF **fnExit, RunF **fnRun, CommandF **fnCommand, CommandF **fnOnScriptExec);
    typedef int __cdecl GetVerF();

    ExitF *_fnExit;
    RunF *_fnRun;
    CommandF *_fnCommand;
    CommandF *_fnOnScriptExec;
    //@}

  public:
    BattlEye();
    ~BattlEye();

    bool Init(NetworkClient *client);
    void Done();
    bool Run(NetworkClient *client);
    void Command(const char *command);
    bool OnScriptExec(const char *script);

  protected:
    void CleanUp()
    {
      _library = NULL;
      _fnExit = NULL;
      _fnRun = NULL;
      _fnCommand = NULL;
      _fnOnScriptExec = NULL;
    }
    int GetBEVersion(HMODULE library) const;
  };

  BattlEye _beIntegration;
  bool _beWanted;

public:
  bool BattlEyeOnScriptExec(const char *script)
  {
    return _beIntegration.OnScriptExec(script);
  }
protected:
#endif

  //! info about objects local on client
  AutoArray<NetworkLocalObjectInfo> _localObjects;
  //! info about objects non local (remote) on client
  AutoArray<NetworkRemoteObjectInfo> _remoteObjects;
  //! units waiting for respawn
  AutoArray< SRef<RespawnQueueItem> > _respawnQueue;
  //! dead bodies (max MAX_LOCAL_BODIES can be simulated)
  AutoArray<BodyInfo> _bodies;
  //! wrecks (max MAX_LOCAL_WRECKS can be simulated)
  AutoArray<WreckInfo> _wrecks;

  /// mark if team switch request is in progress
  DWORD _teamSwitchDisabledUntil;

  //! message formats
  AutoArray<NetworkMessageFormatBase> _formats;
  //! message queues (candidates for aggregation)
  NetworkMessageQueue _messageQueue, _messageQueueNonGuaranteed;
  
  //! map directly speaking player to object and sound buffer
  AutoArray<Network3DSoundBuffer> _soundBuffers;

  //! connection quality to server
  ConnectionQuality _connectionQuality;
  /// de-sync detected by server
  ConnectionDesync _connectionDesync;
  //! result of _dp->Connect
  ConnectResult _connectResult;

  //! current state
  NetworkClientState _clientState;
  //! current state on server
  NetworkServerState _serverState;

  /// information about last know mission voting results
  VotingMissionProgressMessage _missionVoting;

  /// last custom files progress state sent by NCMTCustomFilesProgress
  int _customFilesProgressMax;
  int _customFilesProgressPos;

  /// last timeout value received from the server, INT_MAX if none
  int _serverTimeout;

  //! mission pbo file is valid (actual)
  bool _missionFileValid;

  //! player is game master of dedicated server
  bool _gameMaster;
  //! gamemaster is only voted admin (cannot do shutdown etc.)
  bool _admin;
  //! show "select mission" dialog for dedicated server
  bool _selectMission;
  //! show "vote mission" dialog for dedicated server
  bool _voteMission;

  //! Game is paused due to disconnection state of game
  bool _controlsPaused;

  //! missions on dedicated server
  AutoArray<MPMissionInfo> _serverMissions;

  //! name of local player
  RString _localPlayerName;

  //! short id of player, used as creator in NetworkId
  int _playerId;

  //! id of next sound - incremented for each PlaySound
  int _soundId;
  //! list of sent sounds
  AutoArray<PlaySoundInfo> _sentSounds;
  //! list of received sounds
  AutoArray<PlaySoundInfo> _receivedSounds;

  //! sound introducing chat message
  SoundPars _chatSound;

  //! selected channel for voice chat
  int _voiceChannel;

  //! ip address of dedicated server
  sockaddr_in _dedicatedServerVoNAddr;
  //! connection type to server (Direct, NAT, ...)
  PlayerIdentity::EVoNDiagConType _vonDiagServerConType;

  //! is NAT negotiation is needed to access server?
  bool _natNegotiationNeeded;

  //! used for transfer of client info (for example camera position) to server
  Ref<ClientInfoObject> _clientInfo;

  //! used for make the client camera position synchronized on all clients
  Ref<ClientCameraPositionObject> _clientCameraPosition;

  //! number of bodies we want to hide
  int _hideBodies;

  //! number of wrecks we want to hide
  int _hideWrecks;

#if _VBS3 // player color
  bool _sentPlayerColour;
#endif

#if _ENABLE_CHEATS
  //! debugging window
  Link<DebugListWindow> _debugWindow;
#endif

#if _ENABLE_MP
  /// who muted me
  RemoteMuteListMessage _remoteMuteList;
#endif

#if _XBOX_SECURE && _ENABLE_MP
# if _XBOX_VER < 200
  XNKID _sessionKid;
  XNADDR _xnaddr;
# endif // _XBOX_VER < 200
  
  AutoArray<RecentPlayerInfo> _recentPlayers;

  int _playerRating;

#if _XBOX_VER >= 200
  /// who is muted by myself
  MuteListMessage _muteList;
#endif

#if _XBOX_VER >= 200
  // Statistics handled by the NetworkManager
#else
  XONLINETASK_HANDLE _writeStatsTask;
#endif
 
#endif // _XBOX_SECURE && _ENABLE_MP

#if _ENABLE_GAMESPY
  /// IP Address of the client (how is seen from matchmaking server)
  int _publicAddr;
#endif

  /// world transfer progress info
  int _totalToCreate;
  int _remainingToCreate;
  int _totalToUpdate;
  int _remainingToUpdate;

  /// event handlers executed in reaction to PublicVariableMessage
  PublicVariableEventHandlers _publicVariableEventHandlers;

  /// public variables which have Null networkId during PublicVariable are stored to be proccessed later again
  AutoArray<RString> _delayedPublicVariables;

  //! time synchronized from server to clients
  DWORD _serverTime;
  DWORD _serverTimeInitTickCount;
  DWORD _serverTimeOfEstimatedEnd;

  //! server admin and gameMaster variables got by ServerAdminMessage
  //  used to know whether some commands like #vote is available
  bool _serverAdmin;
  int _serverGameMaster;

public:

#if _XBOX_SECURE
  //! Constructor
  /*!
  \param parent creating network manager object
  \param addr host Xbox Title Address
  \param kid session id
  \param key key for session id
  \param port server port
  \param playerRating rating of player in statistics
  */
  NetworkClient(NetworkManager *parent, const XNADDR &addr, const XNKID &kid, const XNKEY &key, int port, bool privateSlot, int playerRating);
  //! Connect to server
  /*!
  \param addr host Xbox Title Address
  \param kid session id
  \param key key for session id
  \param botClient client is bot client
  \param serverPort server port
  \param playerRating rating of player in statistics
  \return true if client is connected to server
  */
  bool Init(const XNADDR &addr, const XNKID &kid, const XNKEY &key, int serverPort, bool privateSlot);
#else
  //! Constructor
  /*!
  \param parent creating network manager object
  \param address URL address of server
  \param password password of session
  \param botClient client is bot client
  */
  NetworkClient(NetworkManager *parent, RString address, RString password);
  //! Connect to server
  /*!
  \param addr host Xbox Title Address
  \param address URL address of server
  \param password password of session
  \return true if client is connected to server
  */
  bool Init(RString address, RString password);
#endif

  //! Constructor for bot client
  /*!
  \param parent creating network manager object
  \param playerRating rating of player in statistics
  */
  NetworkClient(NetworkManager *parent, NetworkServer *server, int playerRating);
  //! Connect bot client to server
  bool Init(NetworkServer *server);

  //! Return DirectPlay ID of attached player
  int GetPlayer() const {return _player;}

  //! Return role slot for local client
  const PlayerRole *GetMyPlayerRole() const {return FindPlayerRole(_player);}

#if _ENABLE_GAMESPY
  /// Set on which IP Address the client is visible from the matchmaking server
  void SetPublicAddress(int addr) {_publicAddr = addr;}
#endif

  /// retrieve the local address client communicate on
  bool GetLocalAddress(in_addr &addr) const {return _client ? _client->GetLocalAddress(addr) : false;}
  /// retrieve the port client is listening for voice peer-to-peer communication
  int GetVoicePort() const {return _client ? _client->GetVoicePort() : 0;}

  //! Voice over net initialization
  void InitVoice(bool isBotClient=false);
  //! Connect voice over net to the given player
  void ConnectVoice(int player, sockaddr_in &addr);

  /// Suspend voice playback temporarily
  void DisableVoice(bool disable);
  /// Switches on/off the voice transmition
  void SetVoiceTransmition(bool val) 
  {
    #if VOICE_OVER_NET
    if (_client) _client->SetVoiceTransmition(val);
    #endif
  }
  /// Sets if voice is toggled on (user is NOT using push to talk button)
  virtual void SetVoiceToggleOn(bool val)
  {
#if VOICE_OVER_NET
    if (_client) _client->SetVoiceToggleOn(val);
#endif
  }
  /// sets threshold for silence analyzer (0.0f - 1.0f)
  virtual void SetVoNRecThreshold(float val)
  {
#if VOICE_OVER_NET
    if (_client) _client->SetVoNRecThreshold(val);
#endif
  }

  /// Switches on/off the voice capturing
  virtual void SetVoiceCapture(bool val) 
  {
#if VOICE_OVER_NET
    if (_client) _client->VoiceCapture(val);
#endif
  }

  void CustomFilesProgressGet(int &max, int &pos)
  {
    max = _customFilesProgressMax; pos = _customFilesProgressPos;
  }
  void CustomFilesProgressClear() 
  {
    _customFilesProgressMax = 1; _customFilesProgressPos = 0;
  }

  //! Destructor
  ~NetworkClient();

  //! Disconnect from server
  void Done();

  bool IsValid() const {return _client != NULL;}

  //! Given player is currently speaking
  bool IsVoicePlaying(int dpnid) const {return _client ? _client->IsVoicePlaying(dpnid) : false;}

  //! Local player is currently speaking
  bool IsVoiceRecording() const {return _client ? _client->IsVoiceRecording() : false;}

  int CheckVoiceChannel(int dpnid, bool &audible2D, bool &audible3D) const
  {
    if (_client)
    {
      return _client->CheckVoiceChannel(dpnid,audible2D,audible3D);
    }
    audible2D = audible3D = false;
    return 0;
  }

  void IsVoice2D3DPlaying(int dpnid, bool &audible2D, bool &audible3D) const
  {
    if (_client)
    {
      _client->IsVoice2D3DPlaying(dpnid,audible2D,audible3D);
    }
    else
    {
      audible2D = audible3D = false;
    }
  }

  //! VoN codec quality for encoding
  void SetVoNCodecQuality(int quality) { 
#if  _ENABLE_VON
    if (_client) _client->SetVoNCodecQuality(quality); 
#endif
  }

#if _ENABLE_CHEATS
  void VoNSay(int dpnid, int channel, int frequency, int seconds)
  {
    if (_client) _client->VoNSay(dpnid, channel, frequency, seconds);
  }
#endif

  //! Return connection type info for each player along with the GDebugVoNBank output
  RString GetDebugVoNString();

  //! Server time synchronized to clients
  DWORD GetServerTime() 
  {
    _serverTime = ::GetTickCount() - _serverTimeInitTickCount; //make it fresh
    return _serverTime;
  }

  DWORD GetEstimatedEndServerTime() 
  {
    return _serverTimeOfEstimatedEnd;
  }

#if _XBOX_SECURE && _ENABLE_MP
# if _XBOX_VER < 200
  const XNKID *GetSessionKey() const {return _player == BOT_CLIENT ? NULL : &_sessionKid;}
# endif // _XBOX_VER < 200

  void AddRecentPlayer(PlayerIdentity &identity);
  const AutoArray<RecentPlayerInfo> &GetRecentPlayers() const {return _recentPlayers;}
#endif

  //! Create debugging window
  void CreateDebugWindow();

  //! Return name of local player
  RString GetLocalPlayerName() const {return _localPlayerName;}
  //! Return result of _dp->Connect
  ConnectResult GetConnectResult() const {return _connectResult;}

  //! Return if player is game master of dedicated server
  bool IsGameMaster() const {return _gameMaster;}

  //! Return if gamemaster is only voted admin (cannot do shutdown etc.)
  bool IsAdmin() const {return _admin;}

  //! Return if client is bot client
  inline bool IsBotClient() const;

  //! Return connection quality to server
  ConnectionQuality GetConnectionQuality() const {return _connectionQuality;}

  //! Return connection quality to server
  ConnectionDesync GetConnectionDesync() const {return _connectionDesync;}
  
  //! Return current state
  NetworkClientState GetClientState() const {return _clientState;}
  //! Set state and send notification to server
  void SetClientState(NetworkClientState state);

  //! Return how much part of world state was already transferred to client
  bool GetTransferProgress(float &progress) const;

  //! Handler called when client is joining game in progress
  void OnJoiningInProgress();

  //! Time elapsed from start of mission on server
  int TimeElapsed() const;
  
  //! Time remaining to player respawn
  float GetRespawnRemainingTime() const;
  //! time remaining to player's vehicle respawn
  float GetVehicleRespawnRemainingTime() const;
  //! set default time remaining to player respawn 
  void SetPlayerRespawnTime(float timeInterval);

  //! Return current state on server
  NetworkServerState GetServerState() const {return _serverState;}
  
  int GetServerTimeout() const {return _serverTimeout;}

  //! Return recommended chat priorty based on current game state
  NetMsgFlags GetChatPriority() const;

  //! Set parameters for current mission
  void SetParams(float param1, float param2, bool updateOnly);
  //! Set parameters for current mission
  void SetParams(float param1, float param2, AutoArray<float> paramsArray, bool updateOnly);

  void OnSimulate();
  /// time critical network update - need to be called several times in frame
  void FastUpdate();
  void OnDraw();
  void OnSendComplete(DWORD msgID, bool ok);
  void OnMessage(int from, NetworkMessage *msg, NetworkMessageType type);

  //! perform regular memory clean-up
  unsigned CleanUpMemory();

  //! Process received system messages
  void ReceiveSystemMessages();
  //! Process received user messages
  void ReceiveUserMessages();
  //! Destroy all received system messages
  void RemoveSystemMessages();
  //! Destroy all received user messages
  void RemoveUserMessages();

  
  //! Hi-level send of message to server
  /*!
  \param object message object
  \param dwFlags DirectPlay flags
  */
  DWORD SendMsg
  (
    NetworkSimpleObject *object, NetMsgFlags dwFlags
  );
  bool DXSendMsg(int to, NetworkMessageRaw &rawMsg, DWORD &msgID, NetMsgFlags dwFlags);
  NetworkMessageFormatBase *GetFormat(/*int client, */int type);
  const NetworkMessageFormatBase *GetFormat(/*int client, */int type) const;

  virtual void InitMsg(NetworkMessage *msg, NetworkMessageType type);

  //! Return short id of player, used as creator in NetworkId
  int GetPlayerId() const {return _playerId;}
  //! Set player id directly (used on dedicated server - BOT client has no identity)
  void SetPlayerId(int id) {_playerId = id;}

  //! Retrieve list of players
  void GetPlayers(AutoArray<NetPlayerInfo, MemAllocSA> &players);
  //! Assign player to role slot
  void AssignPlayer(int role, int player, int flags, bool update=false);

  //! Select person as player
  /*!
  \param player DirectX player id
  \param person player's person
  \param respawn play "ressurect" cutscene
  */
  void SelectPlayer(int player, Person *person, bool respawn = false);
  //! Attach person to unit
  /*!
  Send message to server, which person has which brain.
  \param person person which brain is attached to itself
  */
  void AttachPerson(Person *person);
  //! Play sound on all clients (except itself)
  /*!
  \param name name of sound file
  \param position source position
  \param speed source speed
  \param volume sound volume
  \param freq sound frequency
  \param wave local sound object (for mapping local sounds to sent sounds)
  */
  void PlaySound
  (
    RString name, Vector3Par position, Vector3Par speed, 
    float volume, float freq, AbstractWave *wave
  );
  //! Change state of played sound on all clients (except itself)
  /*!
  \param wave local sound object
  \param state state to set
  */
  void SoundState(AbstractWave *wave, SoundStateType state);
  //! Register vehicle on client and send message to server (and other clients)
  /*!
  \param veh registered vehicle
  \param type id of vehicle list to add into
  \param name name of variable vehicle is stored in
  \param idVeh id of vehicle (assigned in mission editor)
  */
  bool CreateVehicle(Vehicle *veh, VehicleListType type, RString name, int idVeh);
  //! Register subobject of vehicle on client and send message to server (and other clients)
  /*!
  \param veh owner vehicle
  \param index unique id of object in vehicle
  \param object created object
  */
  bool CreateSubobject(Vehicle *veh, TurretPath &path, NetworkObject *object, bool setNetworkId=true);
  //! Register whole AI structure for given AICenter (center, groups, subgroups, units)
  bool CreateCenter(AICenter *center);  // whole AI structure
  //! Register generic network object and send message to server (and other clients)
  bool CreateObject(NetworkObject *object, bool setNetworkId=true);
  //! Send first update of all created objects over network (as guaranteed message)
  void CreateAllObjects();
  //! Unregister all objects, both local and remote
  void DestroyAllObjects();
  //! Register command as network object, send message to others
  bool CreateCommand(AISubgroup *subgrp, Command *cmd, bool enqueued, bool setNetworkId=true);
  //! Unregister command, send message to others
  void DeleteCommand(AISubgroup *subgrp, Command *cmd);
  //! Ask transport to allow using vision modes
  void AskForEnableVisionModes(Transport *vehicle, bool enable);
  //! Force switch on gun light
  void AskForForceGunLight(Person *person, bool force);
  //! Enable use of IR laser
  void AskForIRLaser(Person *person, bool force);
  //! Ask object owner for damage of object
  /*!
  \param who damaged object
  \param owner who is responsible for damage
  \param modelPos position of damage
  \param val amount of damage
  \param valRange range of damage
  \param ammunition type
  */
  void AskForDamage(
    Object *who, EntityAI *owner,
    Vector3Par modelPos, float val, float valRange, RString ammo, Vector3Par originDir
  );
  //! Ask object owner for set of total damage of object
  /*!
  \param who damaged object
  \param damage new value of total damage
  */
  void AskForSetDamage
  (
    Object *who, float dammage
  );
  //! Ask object owner for set of max damage of object's hit zones
  /*!
  \param who damaged object
  \param damage new value of total damage
  */
  void AskForSetMaxHitZoneDamage
    (
    Object *who, float dammage
    );
  /// Ask object owner to inflict some damage
  /**
  @param who damaged object
  @param dammage how much damage should be caused
  */
  void AskForApplyDoDamage(Object *who, EntityAI *owner, const Object::DoDamageResult &result, RString ammo);

  /// Store on server the static object was destructed (for clients connecting later through JIP)
  /**
  @param object destructed static object
  */
  void StaticObjectDestructed(Object *object, bool now, bool immediate);

  //! Ask vehicle owner for get in person
  /*!
  \param soldier who is getting in
  \param vehicle vehicle to get in
  \param position position in vehicle to get in
  \param turret turret to get in
  */
  void AskForGetIn(
    Person *soldier, Transport *vehicle,
    GetInPosition position, Turret *turret = NULL,
    int cargoIndex = -1
  );
  //! Ask vehicle owner for get out person
  /*!
  \param soldier who is getting out
  \param vehicle vehicle to get out
  \param turret turret to get out
  \param getOutTo vehicle to get in
  */
  void AskForGetOut(
    Person *soldier, Transport *vehicle, Turret *turret, bool eject, bool parachute
  );
  //! Ask vehicle to unload unit
  /*!
  \param soldier who is getting out
  */
  void AskWaitForGetOut(Transport *vehicle, AIUnit *unit);
  //! Ask vehicle owner for change person position
  /*!
  \param soldier who is changing position
  \param vehicle vehicle where position is changed
  \param type performed action
  \param turret to which position is changed
  \param cargoIndex which cargo position is changed
  */
  void AskForChangePosition(
    Person *soldier, Transport *vehicle, UIActionType type, Turret *turret, int cargoIndex
  );
  //! Ask vehicle owner for select weapon
  /*!
  \param vehicle vehicle which weapon is selecting
  \param turret turret which weapon is selecting
  \param weapon selected weapon index
  */
  void AskForSelectWeapon(
    EntityAIFull *vehicle, Turret *turret, int weapon
  );
  //! Ask vehicle owner for change ammo state
  /*!
  \param vehicle vehicle which ammo is changing
  \param turret turret which ammo is changing
  \param weapon weapon index
  \param burst amount of ammo to decrease
  */
  void AskForAmmo(
    EntityAIFull *vehicle, Turret *turret, int weapon, int burst
  );
  //! Ask vehicle owner to fire weapon
  /*!
  \param vehicle vehicle which is firing
  \param turret turret which is firing
  \param weapon weapon index
  \param target fire target
  \param forceLock for avoiding weapon lock delay
  */
  void AskForFireWeapon(EntityAIFull *vehicle, Turret *turret, int weapon, TargetType *target, bool forceLock);

  void AskForAirportSetSide(int index, TargetSide side);
  //! Ask vehicle owner for add impulse
  /*!
  \param vehicle vehicle impulse is applied to
  \param force applied force
  \param torque applied torque
  */
  void AskForAddImpulse
  (
    Vehicle *vehicle, Vector3Par force, Vector3Par torque
  );
  //! Ask object owner for move object
  /*!
  \param vehicle moving object
  \param pos new position
  */
  void AskForMove
  (
    Object *vehicle, Vector3Par pos
  );
  //! Ask object owner for move object
  /*!
  \param vehicle moving object
  \param trans new transformation matrix
  */
  void AskForMove
  (
    Object *vehicle, Matrix4Par trans
  );
  //! Ask group owner for join groups
  /*!
  \param join joined group
  \param group joining group
  \param silent avoid radio communication
  */
  void AskForJoin
  (
    AIGroup *join, AIGroup *group, bool silent
  );
  //! Ask group owner for join groups
  /*!
  \param join joined group
  \param units joining units
  \param silent avoid radio communication
  \param id preferred slot id
  */
  void AskForJoin
  (
    AIGroup *join, OLinkPermNOArray(AIUnit) &units, bool silent, int id
  );
    //! Ask unit to change target side
  /*!
  \param vehicle
  \param side 
  */
  void AskForChangeSide
  (
    Entity *vehicle, TargetSide side
  );
  //! Ask to remote control given unit
  /*!
  \param who   unit which wants to control whom
  \param whom  unit to be controlled by who
  */
  void AskRemoteControlled(Person *who, AIBrain *whom);
  //! Ask person owner for hide body
  /*!
  \param vehicle body to hide
  */
  void AskForHideBody(Person *vehicle);
  //! Transfer explosion effects (explosion, smoke, etc.) to other clients
  /*!
  \param owner shot owner (who is responsible for explosion)
  \param shot shot
  \param directHit hitted object
  \param pos explosion position
  \param dir explosion direction
  \param dir index of the hit component
  \param type ammunition
  \param enemyDamage some enemy was damaged
  \param explosionFactor how large explosion is
  */
  void ExplosionDamageEffects
  (
    EntityAI *owner, Object *directHit, HitInfoPar hitInfo, int componentIndex, 
    bool enemyDamage, float energyFactor, float explosionFactor
  );
  //! Transfer fire effects (sound, fire, smoke, recoil effect, etc.) to other clients
  /*!
  \param vehicle firing vehicle
  \param weapon firing weapon index
  \param magazine fired magazine
  \param target aimed target
  */
  void FireWeapon(
    EntityAIFull *vehicle, Person *gunner, int weapon, const Magazine *magazine, EntityAI *target,
    const RemoteFireWeaponInfo &remoteInfo
  );
  //! Set a texture for a vehicle on other clients
  /*!
	\param vehicle vehicle that will have a texture changed
	\param index selection index to change
	\param name texture name to use
  */
  void SetObjectTexture(EntityAI *veh, int index, RString name, RString eval = "true");
  //! Execute game code on other clients
  /*!
	\param condition game code to evaluate to true or false
	\param command game code to execute
  \param obj object to pass as _this
  \param objs array of objects to pass as _this
  */
  void PublicExec(RString condition, RString command, Object* obj, OLinkArray(Object) &objs);
  //! Send update of weapons to vehicle owner
  /*!
  \param vehicle vehicle to update
  \param turret turret to update
  \param gunner gunner controlling the weapon system
  */
  void UpdateWeapons(EntityAIFull *vehicle, Turret *turret, Person *gunner);
  //! Ask vehicle to add weapon into cargo
  /*!
  \param vehicle asked vehicle
  \param weapon name of weapon type to add
  */
  void AddWeaponCargo(EntityAI *vehicle, RString weapon);
  //! Ask vehicle to remove weapon from cargo
  /*!
  \param vehicle asked vehicle
  \param weapon name of weapon type to remove
  */
  void RemoveWeaponCargo(EntityAI *vehicle, RString weapon);
  //! Ask vehicle to add magazine into cargo
  /*!
  \param vehicle asked vehicle
  \param magazine magazine to add
  */
  void AddMagazineCargo(EntityAI *vehicle, const Magazine *magazine);
  //! Ask vehicle to remove magazine from cargo
  /*!
  \param vehicle asked vehicle
  \param type type of magazine to remove
  \param ammo ammunition in magazine
  */
  void RemoveMagazineCargo(EntityAI *vehicle, RString type, int ammo);
  //! Ask vehicle to add backpack into cargo
  /*!
  \param vehicle asked vehicle
  \param magazine magazine to add
  */
  void AddBackpackCargo(EntityAI *vehicle, EntityAI *backpack);
  //! Ask vehicle to remove backpack into cargo
  /*!
  \param vehicle asked vehicle
  \param magazine magazine to add
  */
  void RemoveBackpackCargo(EntityAI *vehicle, EntityAI *backpack);
  //! Ask vehicle to clar weaponss from cargo
  /*!
  \param vehicle asked vehicle
  \param type type of magazine to remove
  \param ammo ammunition in magazine
  */
  void ClearWeaponCargo(EntityAI *vehicle);
  //! Ask vehicle to clear magazines from cargo
  /*!
  \param vehicle asked vehicle
  \param type type of magazine to remove
  \param ammo ammunition in magazine
  */
  void ClearMagazineCargo(EntityAI *vehicle);
  //! Ask vehicle to clear bags from cargo
  /*!
  \param vehicle asked vehicle
  \param type type of magazine to remove
  \param ammo ammunition in magazine
  */
  void ClearBackpackCargo(EntityAI *vehicle);
  //! Transfer init expression to other clients and execute it
  /*!
  \param init vehicle init expression description
  */
  void VehicleInit(VehicleInitMessage &init);
  //! Transfer message who is responsible to destroy vehicle to other clients
  /*!
  \param killed destroyed vehicle
  \param killer who is responsible for destroying
  */
  void OnVehicleDestroyed(EntityAI *killed, EntityAI *killer);
  //! Transfer message about damage of vehicle to other clients
  /*!
  \param damaged damaged vehicle
  \param damage amount of damage hit
  \param ammo ammunition type
  */
  void OnVehicleDamaged(EntityAI *damaged, EntityAI *killer, float damage, RString ammo);
  //! Transfer current MP Event Handlers of given vehicle to other clients
  /*!
  \param ai affected vehicle
  \param event the Event
  \param handlers event handlers array
  */
  void OnVehMPEventHandlersChanged(EntityAI *ai, int event, const AutoArray<RString> &handlers);
  //! Transfer message about fired missile to other clients
  /*!
  \param target missile target
  \param ammo ammunition type
  \param owner missile owner
  */
  void OnIncomingMissile(EntityAI *target, Entity *shot, EntityAI *owner);
  //! Transfer message about fired cm
  /*!
  \param cm
  \param missile
  */
  void OnLaunchedCounterMeasures(Entity *cm, Entity *testedSystem, int count);
  //! Transfer message about fired missile to other clients
  /*!
  \param target missile target
  \param ammo ammunition type
  \param owner missile owner
  */
  void OnWeaponLocked(EntityAI *target, Person *gunner, bool locked);
  //! Transfer info about (user made) marker creation to other clients
  /*!
  \param channel chat channel (who will see the marker)
  \param sender sender unit
  \param units receiving units
  \param info marker itself
  */
  void MarkerCreate(int channel, AIBrain *sender, RefArray<NetworkObject> &units, ArcadeMarkerInfo &info);
  //! Transfer info about (user made) marker was deleted to other clients
  /*!
  \param name name of marker
  */
  void MarkerDelete(RString name);
  //! Transfer info about waypoint creation to other clients
  /*!
  \param group owner of the waypoint
  \param index index of the waypoint in the owner's list
  \param wp waypoint object
  */
  void WaypointCreate(AIGroup *group, int index, WaypointInfo &wp);
  //! Transfer info about waypoint update to other clients
  /*!
  \param group owner of the waypoint
  \param index index of the waypoint in the owner's list
  \param wp waypoint object
  */
  void WaypointUpdate(AIGroup *group, int index, WaypointInfo &wp);
  //! Transfer info about waypoint removal to other clients
  /*!
  \param group owner of the waypoint
  \param index index of the waypoint in the owner's list
  */
  void WaypointDelete(AIGroup *group, int index);
  //! Copy the whole chain of waypoints on other clients
  /*!
  \param from source of the waypoints
  \param to target where to copy the waypoints
  */
  void WaypointsCopy(AIGroup *to, AIGroup *from);
  //! Ask flag (carrier) owner for assign new owner
  /*!
  \param owner new owner
  \param carrier flag carrier
  */
  //! Transfer info about group's new HC commander 
  /*!
  \param unit new HC commander
  \param hcGroup commanded group
  */
  void HCSetGroup(AIUnit *unit, AIHCGroup hcGroup);
  //! Transfer info about group's release from HC commander 
  /*!
  \param unit HC commander
  \param group released group
  */
  void HCRemoveGroup(AIUnit *unit, AIGroup *group);
  //! Transfer info about group's release from HC commander 
  /*!
  \param unit HC commander
  \param group released group
  */
  void HCClearGroups(AIUnit *unit);
  //! Transfer info about group's Unconscious Leader
  /*!
  \param unit commander
  \param group
  */
  void GroupSetUnconsciousLeader(AIUnit *unit,AIGroup *group);
  //! Transfer info about new group's  Leader
  /*!
  \param unit commander
  \param group
  */
  void GroupSelectLeader(AIUnit *unit,AIGroup *group);
  void DropBackpack(EntityAI *backpack, Matrix4Par trans);
  void TakeBackpack(Person *soldier, EntityAI *backpack);

  void Assemble(EntityAI *weapon, Matrix4Par transform);
  void DisAssemble(EntityAI *weapon, EntityAI *backpack);

  void SetFlagOwner
  (
    Person *owner, EntityAI *carrier
  );
  //! Ask client owns flag owner for change of flag ownership
  /*!
  \param owner new or old flag owner
  \param carrier flag carrier
  */
  void SetFlagCarrier
  (
    Person *owner, EntityAI *carrier
  );
  //! Ask flag (carrier) owner for cancel taking process
  /*!
  \param carrier flag carrier
  */
  void CancelTakeFlag(EntityAI *carrier);

  //! Send MP Event to be activated on other clients
  /*!
  \param veh affected vehicle
  \param event triggered event
  \param pars parameters to be passed corresponding OnMPEvent on all clients
  */
  void SendMPEvent(MPEntityEvent event, const GameValue &pars);

  //! Public variable to other clients
  /*!
  \param name variable name
  */
  void PublicVariable(RString name);

  //! Public variables which contains objects with NULL networkId are processed after CreateAllObjects again
  void ProcessDelayedPublicVariables();
  void ClearDelayedPublicVariables();
  void AddDelayedPublicVariable(RString name);

  //! Set teamMember variable
  /*!
  \param teamMember team member to modify  
  \param name variable name
  */
  void TeamMemberSetVariable(AITeamMember *teamMember,RString name);
  //! Set object variable
  /*!
  \param obj variable container object
  \param name variable name
  */
  void ObjectSetVariable(Object *obj, RString name);
  //! Set group variable
  /*!
  \param group variable container group
  \param name variable name
  */
  void GroupSetVariable(AIGroup *group, RString name);
  //! Send chat message
  /*!
  \param channel chat channel
  \param text chat text
  */
  void Chat(int channel, RString text);

  //! Send chat message
  /*!
  \param channel chat channel
  \param sender sender unit
  \param units receiving units
  \param text chat text
  */
  void Chat(int channel, AIBrain *sender, RefArray<NetworkObject> &units, RString text);
  //! Send chat message
  /*!
  \param channel chat channel
  \param sender sender name
  \param units receiving units
  \param text chat text
  */
  void Chat(int channel, RString sender, RefArray<NetworkObject> &units, RString text);

  /// Send client-side localized chat message
  void LocalizedChat(int channel, const LocalizedFormatedString &text);

  /// Send client-side localized chat message
  /*!
  \param channel chat channel
  \param sender sender unit
  \param units receiving units
  \param text chat text
  */
  void LocalizedChat(int channel, AIBrain *sender, RefArray<NetworkObject> &units, const LocalizedFormatedString &text);
  /// Send client-side localized chat message
  /*!
  \param channel chat channel
  \param sender sender name
  \param units receiving units
  \param text chat text
  */
  void LocalizedChat(int channel, RString sender, RefArray<NetworkObject> &units, const LocalizedFormatedString &text);
  
  //! Send radio message as chat (text and sentence)
  /*!
  \param channel chat channel
  \param sender sender name
  \param units receiving units
  \param text chat text
  \param sentence - list of words to be spoken
  */
  void RadioChat(int channel, AIBrain *sender, RefArray<NetworkObject> &units, RString text, RadioSentence &sentence);
  //! Send text radio message as chat (sound and title)
  /*!
  \param channel chat channel
  \param units receiving units
  \param wave name of class from CfgRadio containing sound and title
  \param sender sender unit
  \param senderName sender name
  */
  void RadioChatWave(int channel, RefArray<NetworkObject> &units, RString wave, AIBrain *sender, RString senderName);
  /// Return actual channel for voice chat
  int GetVoiceChannel() const {return _voiceChannel;}
  //! Set channel for Voice Over Net
  /*!
  \param channel chat channel
  */
  void SetVoiceChannel(int channel);
  //! Set position and speed of some Voice Over Net source (it affects OAL sources)
  /*!
  \param dpnid player id got from GetRemotePlayer()
  \param pos position of player
  \param speed speed of player
  */
  void SetVoNPosition(int dpnid, Vector3Par pos, Vector3Par speed);
  //! Set position and speed of player in Voice Over Net (it affects OAL listener)
  /*!
  \param pos position of player
  \param speed speed of player
  */
  void SetVoNPlayerPosition(Vector3Par pos, Vector3Par speed);
  //! Set obstruction of player in Voice Over Net (it affects OAL source)
  /*!
  \param dpnid player id got from GetRemotePlayer()
  */
  void SetVoNObstruction(int dpnid, float obstruction, float occlusion, float deltaT);
  //! Updates volume and ear accomodation of player in Voice Over Net (it affects OAL source)
  /*!
  \param dpnid player id got from GetRemotePlayer()
  */
  void UpdateVoNVolumeAndAccomodation(int dpnid, float volume, float accomodation);
  //! Advance all voices in VoN (even muted ones) 
  /*!
  \param deltaT time to advance
  \param paused whether game is paused
  \param quiet whether voices should be muted
  */
  void AdvanceAllVoNSounds( float deltaT, bool paused, bool quiet );

  //! Transfer file to server
  /*!
  \param dest destination filename
  \param source source filename
  */
  void TransferFileToServer(RString dest, RString source);
  //! Retrieve state of file transfer
  /*!
  \param curBytes amount of transfered bytes
  \param totBytes total amount bytes to transfer
  */
  void GetTransferStats(int &curBytes, int &totBytes);
  //! Ask player to show target
  /*!
  \param vehicle player person
  \param target target to show
  */
  void ShowTarget(Person *vehicle, TargetType *target);
  //! Ask player to show group direction
  /*!
  \param vehicle player person
  \param dir rirection to show
  */
  void ShowGroupDir(Person *vehicle, Vector3Par dir);
  //! Transfer activation of group synchronization
  /*
  \param grp synchronized group
  \param active state of synchronization
  */
  void GroupSynchronization(AIGroup *grp, int synchronization, bool active);
  //! Transfer activation of detector (through radio)
  /*
  \param grp synchronized group
  \param active state of synchronization
  */
  void DetectorActivation(Detector *det, bool active);
#if _VBS2 //Convoy trainer
  //! Send EnablePersonalItems message
  /*
  \param object object to have its personalised items enabled
  \param active turn on or off
  */
  void EnablePersonalItems(Person *person, bool active, RString animAction, Vector3 personalItemsOffset);
  void AARDoUpdate(int type ,int intType,RString stringOne = RString(), RString stringTwo = RString(), float floatType = 0.0);
  void AARAskUpdate(int type ,int intType,RString stringOne = RString(), RString stringTwo = RString(), float floatType = 0.0);
  void AddMPReport(int type, RString myString);
  void LoadIsland(RString islandName);
  void AskWeather();
  void ApplyWeather();
  void AskCommanderOverride(Turret *turret, Turret *overriddenByTurret);
#endif
  //! Ask group owner to create new unit
  /*!
  \param group group unit will be added to
  \param type name of vehicle type
  \param position position create at
  \param skill initial skill
  \param rank initial rank
  */
  void AskForCreateUnit(AIGroup *group, RString type, Vector3Par position, RString init, float skill, Rank rank);
  //! Ask vehicle owner to destroy vehicle
  /*!
  \param vehicle vehicle to destroy
  */
  void AskForDeleteVehicle(Entity *veh);

  //! Ask subgroup to receive answer from unit
  void AskForReceiveUnitAnswer
  (
    AIUnit *from, AISubgroup *to, int answer
  );
  /// Ask unit to set _getInOrdered flag
  void AskForOrderGetIn(AIBrain *unit, bool flag);
  /// Ask unit to set _getInAllowed flag
  void AskForAllowGetIn(AIBrain *unit, bool flag);

  //! Ask group owner to respawn player
  /*!
  \param person killed player
  \param killer killer entity
  */
  void AskForGroupRespawn(Person *person, EntityAI *killer);
  /// Collect units suitable for (group or side) respawn - free, living (playable)
  void GetSwitchableUnits(OLinkPermNOArray(AIBrain) &units, const AIBrain *player);
  /// Process the respawn to the selected unit on the client where the unit is local
  void TeamSwitch(Person *from, Person *to, EntityAI *killer, bool respawn);

#if _VBS3 // respawn command
  //! Tell local simulation to pause itself
  /*!
  \param pause simulation true or false
  */
  void PauseSimulation(bool pause);

	//! Ask to respawn player
	/*!
	\param person player to respawn
	*/
	void AskForRespawn(Person *person);
#endif

#if _ENABLE_ATTACHED_OBJECTS
  //! Attach Object
  /*!
  \param obj, object to attach
  \param attachTo, where does the object get attached to?
  \param memindex, memory point index were it get's attach to parent, (-1) for center
  \param pos, position offset from attachPoint
  \param flags, additional flags
  */
  void AttachObject(Object *obj, Object *attachTo, int memIndex, Vector3 pos, int flags);
  //! detach object
  /*!
  \param obj, object to detach
  */
  void DetachObject(Object *obj);
#endif

  //! Ask mine owner to activate it
  /*!
  \param mine mine
  \param activate activate / desactivate
  */
  void AskForActivateMine(Mine *mine, bool activate);
  //! Ask fireplace to inflame / put down
  /*!
  \param fireplace fireplace
  \param fire inflame / put down
  */
  void AskForInflameFire(Fireplace *fireplace, bool fire);
  //! Ask vehicle for user defined animation
  /*!
  \param vehicle animated vehicle
  \param animation animation name
  \param phase wanted animation phase
  */
  void AskForAnimationPhase(Entity *vehicle, RString animation, float phase);

  //! Ask container or body owner for weapon
  /*!
  \param from Asked container or body
  \param to Asking person
  \param weapon Type of wanted weapon
  \param slots # of empty magazine slots
  */
  void OfferWeapon(EntityAI *from, EntityAIFull *to, const WeaponType *weapon, int slots, bool useBackpack);
  //! Ask container or body owner for magazine
  /*!
  \param from Asked container or body
  \param to Asking person
  \param type Type of wanted magazine
  */
  void OfferMagazine(EntityAI *from, EntityAIFull *to, const MagazineType *type, bool useBackpack);
  //! Ask container or body owner for backpack
  /*!
  \param from Asked container or body
  \param to Asking person
  \param name Type of wanted backpack
  */
  void OfferBackpack(EntityAI *from, EntityAIFull *to, RString name);
  //! Ask vehicle to replace his weapon
  /*!
  \param from Asking container or body
  \param to Asked person
  \param weapon Offered weapon
  */
  void ReplaceWeapon(EntityAI *from, EntityAIFull *to, const WeaponType *weapon, bool useBackpack);
  //! Ask vehicle to replace his magazine
  /*!
  \param from Asking container or body
  \param to Asked person
  \param magazine Offered magazine
  */
  void ReplaceMagazine(EntityAI *from, EntityAIFull *to, Magazine *magazine, bool reload, bool useBackpack);
  //! Ask vehicle to replace his backpack
  /*!
  \param from Asking container or body
  \param to Asked person
  \param backpack Offered backpack
  */
  void ReplaceBackpack(EntityAI *from, EntityAIFull *to, EntityAI *backpack);
  //! Ask container or body to accept weapon
  /*!
  \param from Asked container or body
  \param weapon Offered weapon
  */
  void ReturnWeapon(EntityAI *from, const WeaponType *weapon);
  //! Ask container or body to accept magazine
  /*!
  \param from Asked container or body
  \param magazine Offered magazine
  */
  void ReturnMagazine(EntityAI *from, Magazine *magazine);
  //! Ask container or body to accept backpack
  /*!
  \param from Asked container or body
  \param backpack Offered backpack
  */
  void ReturnBackpack(EntityAI *from, EntityAI *backpack);

  //! Ask weapon pool for weapon
  /*!
  \param unit Asking unit
  \param weapon Type of wanted weapon
  \param slots # of empty magazine slots
  */
  void PoolAskWeapon(AIBrain *unit, const WeaponType *weapon, int slot, bool useBackpack);
  //! Ask weapon pool for magazine
  /*!
  \param unit Asking unit
  \param type Type of wanted magazine
  */
  void PoolAskMagazine(AIBrain *unit, const MagazineType *type, int slot, bool useBackpack);
  //! Ask weapon pool for magazine
  /*!
  \param unit Asking unit
  \param type Type of wanted magazine
  */
  void PoolAskBackpack(AIBrain *unit, RString name);
  //! Ask weapon pool to accept weapon
  /*!
  \param weapon Offered weapon
  */
  void PoolReturnWeapon(const WeaponType *weapon);
  //! Ask weapon pool to accept magazine
  /*!
  \param magazine Offered magazine
  */
  void PoolReturnMagazine(Magazine *magazine);
  //! Ask weapon pool to accept magazine
  /*!
  \param magazine Offered magazine
  */
  void PoolReturnBackpack(int creator, int id, RString typeName);

  //! Returns respawn mode
  RespawnMode GetRespawnMode() const {return (RespawnMode)_missionHeader._respawn;}
  //! Returns respawn delay
  float GetRespawnDelay() const {return _missionHeader._respawnDelay;}
  //! Returns respawn delay for vehicles
  float GetRespawnVehicleDelay() const {return _missionHeader._respawnVehicleDelay;}
  //! Add person to respawn queue (respawn after delay)
  /*!
  \param soldier person to respawn
  \param pos position where respawn
  */
  void Respawn(Person *soldier, Vector3Par pos, float timeCoef = 1.0f);
  //! Add vehicle to respawn queue (respawn after delay)
  /*!
  \param vehicle vehicle to respawn
  \param pos position where respawn
  */
  void Respawn(Transport *vehicle, Vector3Par pos, float timeCoef = 1.0f);

#if _ENABLE_CONVERSATION
  /// Ask client to react in the conversation
  void KBReact(AIBrain *from, AIBrain *to, const KBMessageInfo *message);
#endif

  //! Checks if gamemaster is to select mission on dedicated server
  bool CanSelectMission() const {return _selectMission;}
  //! Checks if client is to vote mission on dedicated server
  bool CanVoteMission() const {return _voteMission;}
  //! Returns array of missions available on dedicated server
  const AutoArray<MPMissionInfo> &GetServerMissions() const 
  {
    return _serverMissions;
  }

  /// check voting status
  const VotingMissionProgressMessage *GetMissionVotingState() const {return &_missionVoting;}
  //! Select mission on dedicated server
  /*!
  \param mission name of selected mission
  \param difficulty game difficulty level
  */
  void SelectMission(RString mission, int difficulty);
  //! Vote mission on dedicated server
  /*!
  \param mission name of selected mission
  \param difficulty game difficulty level
  */
  void VoteMission(RString mission, int difficulty);

  /// Find the object (local or remote) by its network id
  NetworkObject *GetObject(NetworkId &id);
  //! Unregister given network object, send message to others
  void DeleteObject(NetworkId &id, bool sendMsg);

  //! Process chat command for remote control of dedicated server
  /*!
  \param command chat command
  */
  bool ProcessCommand(RString command);
  bool IsCommandAvailable(NetworkCommandType type);
  bool IsCommandAvailable(RString command);

  //! Ask server to kick off player
  /*!
  \param player DirectPlay ID of player to kick off
  */
  void SendKick(int player);

  //! Ask server to disable/enable connection of further clients
  /*!
  \param lock true to disable connection
  */
  void SendLockSession(bool lock = true);

  //! player must disconnect due to some mission loading error
  void Disconnect(RString message);

  //! Force update of network object
  /*!
  \param object object to update
  \param dwFlags DirectPlay flags
  */
  void UpdateObject(NetworkObject *object, NetMsgFlags dwFlags);

  const char *GetDebugName() const {return "Client";}

  //! Return estimated end of mission time
  Time GetEstimatedEndTime() const;

  //! Body can be hidden (for better performance)
  void DisposeBody(Person *body);

  //! Wreck can be hidden (for better performance)
  void DisposeWreck(Transport *wreck);

  //! Game is paused due to disconnection state of game
  bool IsControlsPaused() {return _controlsPaused;}

  //! Last received message's age in seconds (used to eliminate "disconnect cheat")
  float GetLastMsgAgeReliable();

  //! Select player to join into when player join game in progress
  void JIPSelectPlayer(int dpnid);

  //! Process join into unit when player join game in progress
  void JoinIntoUnit(int dpnid, int roleIndex, AIBrain *playerUnit, AIGroup *playerGroup);

  //! Cleanup unit after player disconnection
  void CleanupPlayer(int dpnid, NetworkId id);

  //! Set voice mask to new value (Xbox)
  void ChangeVoiceMask(RString mask);

  //! Broadcast change of role index  
  void SetRoleIndex(AIUnit *unit, int roleIndex);

  //! Update target on other machines
  void RevealTarget(int to, AIGroup *grp, EntityAI *target);

  /// Reaction to detection of hacked data
  void HackedDataDetected(RString filename);

  /// add event handler executed in reaction to PublicVariableMessage
  void AddPublicVariableEventHandler(RString name, GameValuePar code);

  /// remember object being serialized in MP load
  AutoArray<NetworkObject*> _serializedObjects;
  void RememberSerializedObject(NetworkObject *obj);
  AutoArray<NetworkObject*> *GetSerializedObjects() { return &_serializedObjects; }
  void ClearSerializedObjects() { _serializedObjects.Clear(); }

  /// save/load the MP game
  LSError SerializeMP(ParamArchive &ar);
  /// calls createVehicles,Subobjects,Commands after load from save
  void CreateAllObjectsFromLoad();
  /// create AI structures after load from save
  void CreateCenterFromLoad(AICenter *center);
  /// create vehicle after load from save
  bool CreateVehicleFromLoad(Vehicle *veh, VehicleListType list, RString name, int idVeh);
  /// Inform server all create messages were sent after MP Load.
  void SendGameLoadedFromSave();

  // implementation
protected:
  //! Find info about local object with given id (NULL if not found)
  NetworkLocalObjectInfo *GetLocalObjectInfo(NetworkId &id);
  //! Find info about remote object with given id (NULL if not found)
  NetworkRemoteObjectInfo *GetRemoteObjectInfo(NetworkId &id);

  //! Register network object as local object, assign id to them
  NetworkId CreateLocalObject(NetworkObject *object, bool setNetworkId=true);

  //! Force update of network object (single type of update)
  /*!
  \param object object to update
  \param cls type of update
  \param dwFlags DirectPlay flags
  */
  int UpdateObject(NetworkObject *object, NetworkMessageClass cls, NetMsgFlags dwFlags);
  //! Update all network objects in AI structure for given AICenter (center, groups, subgroups, units)
  bool UpdateCenter(AICenter *center);

  //! Prepare multiplayer game
  /*!
  Switch landscape, parse mission sqm file, initialize GWorld.
  */
  bool PrepareGame();

  //! Perform actions needed when client reach state NCSBriefingRead
  void OnMissionStarted();

  //! Process all waiting respawns
  void ProcessRespawns();

  //! Respawn person by info given by item of respawn queue
  Person *DoRespawn(RespawnQueueItem *item);

  //! Respawn vehicle by info given by item of respawn queue
  Transport *DoVehicleRespawn(RespawnQueueItem *item);

  /// Implementation of the respawn to the selected unit on the local client
  bool ProcessTeamSwitch(int player, Person *from, Person *to, EntityAI *killer, bool respawn);

  /// Reaction to the team switch result on the original client
  void HandleTeamSwitchResult(int player, Person *from, Person *to, EntityAI *killer, bool respawn, bool result);

  /// Finishing the team switch on the original person's owner
  void FinishTeamSwitch(Person *from, bool respawn);

  //! Select player on local machine
  void ProcessSelectPlayer(Person *person, bool respawn, Vector3Par pos);

  ///! Create and Update messages can be received only when client is ready to play
  inline bool CanReceiveUpdateMessages()
  {
    return (_clientState >= NCSMissionReceived && _clientState <= NCSBriefingRead);
  }

  //! Create network object and register as remote object
  /*!
  \param ctx network message context of create message
  \param dummy dummy argument for specification type of created network object
  */
  template <class Type>
  bool CreateRemoteObject(NetworkMessageContext &ctx, Type *dummy)
  {
    // check if object already exist
    PREPARE_TRANSFER(NetworkObject)

    NetworkId id;
    TRANSF_BASE(objectCreator, id.creator);
    TRANSF_BASE(objectId, id.id);

    if ( !CanReceiveUpdateMessages() ) // client is unable to receive Update messages, mostly due to being in NCSGameFinished state after disconnect
    {
      RptF("Creation of %d:%d ignored, state %s", id.creator, id.id, (const char *)FindEnumName(_clientState));
      return false; // updates from the last session
    }

    // update the transfer progress
    if (_clientState == NCSMissionReceived && _remainingToCreate > 0) _remainingToCreate--;

    if (GetObject(id))
    {
      RptF("Object %d:%d already exists", id.creator, id.id);
      return false;
    }

    // create object
    ctx.SetClass(NMCCreate);
    NetworkObject *obj = Type::CreateObject(ctx);
    if (!obj)
    {
      RptF("Cannot create object %d:%d", id.creator, id.id);
      return false;
    }

    // add to remote objects
    int index = _remoteObjects.Add();
    NetworkRemoteObjectInfo &info = _remoteObjects[index];
    info.id = id;
    info.object = obj;

#if _EXT_CTRL 
    //ext. Controller like HLA, AAR 
    //moved so AAR does not ctd with unknown object.
    if(IsBotClient())
      _hla.CreateObject(obj);
#endif

    if (DiagLevel >= 1)
      DiagLogF("Client: remote object created %d:%d", id.creator, id.id);
    return true;
  }
  //! Unregister and destroy remote network object
  void DestroyRemoteObject(NetworkId id);

  /// helper functor for CleanupPlayer
  class RemoveSeaGull;
  
  //! Enforce destruction of (local or remote) object
  void ForceDeleteObject(NetworkObject *object);

  //! Send all agregated and update messages
  void SendMessages();
  //! Calculate limits for message sending
  void EstimateBandwidth(int &nMsgMax, int &nBytesMax);
  //! Calculate errors and place object in list sorted by error
  /*!
  \param objects output sorted list of objects
  */
  void CreateObjectsList(AutoArray<UpdateLocalObjectInfo, MemAllocSA> &objects);
  //! Prepare next object update message to message queue
  bool PrepareNextUpdate(AutoArray<UpdateLocalObjectInfo, MemAllocSA> &objects, int &next);

  //! Return number of auto destroyed local objects - used for debugging purposes
  int NLocalObjectsNULL() const;
  //! Check database of local objects - used for debugging purposes
  bool CheckLocalObjects() const;

  /// clear voting once we know it is no longer valid
  void ClearMissionVoting();
  //! Server state changed
  void OnServerStateChanged(NetworkServerState state);

  //! State change enforced by server
  void OnClientStateChanged(NetworkClientState state);

  void EnqueueMsg
  (
    int to, NetworkMessage *msg,
    NetworkMessageType type
  );
  void EnqueueMsgNonGuaranteed
  (
    int to, NetworkMessage *msg,
    NetworkMessageType type
  );

  //! Create new unit (for join in progress)
  AIBrain *CreateUnit(AIGroup *grp, int roleIndex);

  //! Returns which players hear me
  void GetPlayersOnChannel(AutoArray<int, MemAllocSA> &players, int channel) const;
  //! Returns which players can hear me in direct speak (are within the radius range depending on channel)
  void GetTargetsInDirectSpeakRange(AutoArray<TransmitTarget, MemAllocSA> &players, int channel) const;

  //! Check if player is in remote mute list (local player muted by given player)
  bool IsPlayerInRemoteMuteList(int player) const;

#if _XBOX_SECURE
# if _XBOX_VER >= 200
public:
  //! Check if player is in local mute list (given player muted by local player)
  bool IsPlayerInMuteList(const XUID &xuid) const;
  //! Mute list changed
  void UpdateMuteList();
# endif
#endif

protected:
  //! start validation of bank hash
  //! \param askedIndex index matching NMTDataSignatureAsk
  //! \param qBankIx index of bank inside global GFileBanks
  //! \return true when check was succesful
  bool CheckDataSignature(int askedIndex, int level, QFBank &bank, int gBankIx);
  //! start validation of other additional file hash
  bool CheckDataSignature(int askedIndex, int level, RString filename);
  //! start validation of bank hash (using filename to find inside some bank, sig.ver > 1)
  bool CheckFileSignature( int bankIndex, int level, RString filename );

  /// queue of banks waiting for signature check
  FindArrayKey<CheckSignatureItem> _checkSignatureQueue;
  /// currently checked signature
  SRef<BankSignatureCheckAsync> _checkingSignature;  

#if defined _XBOX && _XBOX_VER >= 200
  /// updates all needed based on new local player info list
  void UpdateLocalPlayerInfoList(const LocalPlayerInfoList& localPlayerInfoList);
#endif

public:
  // Delete respawn(old) body
  void DeleteBody(Person *body, bool deleteOldBody);
  // Delete respawn wreck;
  void DeleteWreck(Transport *wreck);
};

//! Info about single type of update of network object
struct NetworkCurrentInfo
{
  //! type of message
  NetworkMessageType type;
  //! sender of message
  int from;
  //! last received state of object
  Ref<NetworkMessage> message;
};

#include <Es/Memory/normalNew.hpp>

//! info about state of sent updates of network object for single client
struct NetworkPlayerObjectInfo: public RefCount
{
  //! ID of player
  int player;
  //! state of updates
  NetworkUpdateInfo updates[NMCUpdateN];
  //! allocated quite often
  USE_FAST_ALLOCATOR
};

#include <Es/Memory/debugNew.hpp>

//! Info about network object on server
/*!
\patch_internal 1.23 Date 9/18/2001 by Jirka
- Improved: storage of PlayerObjectInfo changes
*/
#include <Es/Memory/normalNew.hpp>

/// messages waiting to be sent until acknowledgement OwnerChanged arrives from previous owner in ChangeOwner process
struct WaitingMessageInfo: public RefCount
{
  Ref<NetworkMessage> msg;
  NetworkMessageType  type;

  WaitingMessageInfo(NetworkMessageType t, NetworkMessage *m) : msg(m), type(t) {};

  USE_FAST_ALLOCATOR
};

struct NetworkObjectInfo: public RefCount
{
  //! ID of object
  NetworkId id;
  //! DirectPlay ID of owner client
  int owner;
  //! DirectPlay ID of new owner client pending (ChangeOwner message sent but waiting to acknowledgment OwnerChanged message)
  //! when set to NO_CLIENT no change owner is pending
  int pendingOwner;
  //! create message (used for JOIN IN PROGRESS)
  NetworkCurrentInfo create;
  //! state of last received updates
  NetworkCurrentInfo current[NMCUpdateN];
  //! state of last sent updates (for each player)
  RefArray<NetworkPlayerObjectInfo> playerObjects;
  //! messages waiting to be sent to clients until acknowledgment OwnerChanged arrives from previous owner in ChangeOwner process
  RefArray<WaitingMessageInfo> waitingMessages;
  //! Find info about network object for given player
  NetworkPlayerObjectInfo *GetPlayerObjectInfo(int player);
  //! Create info (register) of new network object for given player
  NetworkPlayerObjectInfo *CreatePlayerObjectInfo(int player);
  //! Delete info (unregister) network object for given player
  void DeletePlayerObjectInfo(int player);
  //! Objects of this type will be allocated quite often
  USE_FAST_ALLOCATOR
};
//TypeIsMovable(NetworkObjectInfo)

#include <Es/Memory/debugNew.hpp>

//! stores info about integrity question
struct IntegrityQuestion
{
  //! question name
  RString name;
  //! region in question
  int offset,size;
  //! Constructor
  /*!
  \param n question name
  \param o region in question
  \param s region in question
  */
  IntegrityQuestion(const RString &n, int o, int s)
  :name(n),offset(o),size(s)
  {
  }
  //! Constructor
  IntegrityQuestion():name(""),offset(0),size(INT_MAX)
  {
  }
};

struct NetworkPlayerInfo;

//! inteface for more detailed integrity check tests
/*!
when integrity check failed, we may wish to peform more detailed test
*/
class IntegrityInvestigation: public RefCount
{
  public:
  //! after test is finished, return test result information (if any)
  virtual RString GetResult() const = 0;
  //! perform test iteration, return false if test finished
  virtual bool Proceed(NetworkServer *server, NetworkPlayerInfo *pi) = 0;
  //! check if investigation asked given question
  virtual bool QuestionMatching(const IntegrityQuestion &q) const = 0;
  // tell investigation its question have been answered
  virtual void QuestionAnswered(bool answerOK) = 0;
};

class IntegrityInvestigationConfig: public IntegrityInvestigation
{
  //! class where investigation started
  ParamClassLockedPtr _root;
  //! class currently being tested 
  ParamClassLockedPtr _class;
  //! test result, if test is not fisnished yet, empty string
  RString _result;
  //! last question - we are expecting an aswer to it
  IntegrityQuestion _question;
  //! when we are expecting an answer to our question
  DWORD _questionTimeout;

  public:

  IntegrityInvestigationConfig(const char *path);
  virtual RString GetResult() const;
  virtual bool Proceed(NetworkServer *server, NetworkPlayerInfo *pi);
  virtual bool QuestionMatching(const IntegrityQuestion &q) const;
  virtual void QuestionAnswered(bool answerOK);
};

//! calculate min, max and average from set of values
class CalculateMinMaxAvg
{
  float _min,_max,_sum;
  float _weight;
  /// last value sampled
  float _lastValue;

  public:
  CalculateMinMaxAvg();
  //! add sample
  void Sample(float value, float weight=1);
  //! reset 
  void Reset();
  //! get current results (min)
  float GetMin() const {return _weight>0 ? _min : 0;}
  //! get current results (max)
  float GetMax() const {return _weight>0 ? _max : 0;}
  //! get current results (average)
  float GetAvg() const {return _weight>0 ? _sum/_weight :0;}
  /// get last value
  float GetLastValue() const {return _lastValue;}
};

//! complete information about one question asked
struct IntegrityQuestionInfo
{
  int id;
  IntegrityQuestionType type;
  IntegrityQuestion q;
  //! Timeout for questions, UINT_MAX if question was never asked
  DWORD timeout;
  //! constructor
  IntegrityQuestionInfo()
  {
    id = 0;
    timeout = UINT_MAX;
  }
};

TypeIsMovable(IntegrityQuestionInfo)

struct PendingDataSignatureCheck
{
  //! the index of checked file
  DWORD answerIndex;
  //! timeout for question
  DWORD checkTimeout;
};

TypeIsSimple(PendingDataSignatureCheck)

struct PendingFileSignatureCheck
{
  //! the checked file
  RString filename;
  //! the pbo prefix
  RString prefix;
  //! timeout for question
  DWORD checkTimeout;
};

TypeIsMovableZeroed(PendingFileSignatureCheck)

// Helper functor
class CheckSignatureTimeoutFunc
{
public:
  virtual bool operator ()() = 0;
};

//! PendingSignatureChecks encapsulates both PendingDataSignatureCheck and PendingFileSignatureCheck
class PendingSignatureChecks
{
  AutoArray<PendingFileSignatureCheck> pendingFileChecks;
  AutoArray<PendingDataSignatureCheck> pendingDataChecks;
public:
  PendingSignatureChecks() {}

  // both  data and file signature pendings are checked for timeout and given action is processed when timeouted
  void CheckTimeouts(CheckSignatureTimeoutFunc &func);

  //@{ interface to work with PendingDataSignatureChecks
  bool Pending(int bankIndex)
  {
    for (int i=0; i<pendingDataChecks.Size(); i++)
    {
      if (pendingDataChecks[i].answerIndex==bankIndex) return true;
    }
    return false;
  }
  bool Remove(int bankIndex)
  {
    for (int i=0; i<pendingDataChecks.Size(); i++)
    {
      if (pendingDataChecks[i].answerIndex==bankIndex)
      {
        pendingDataChecks.Delete(i);
        return true;
      }
    }
    return false;
  }
  void Add(int bankIndex)
  {
    PendingDataSignatureCheck &check = pendingDataChecks.Append();
    check.answerIndex = bankIndex;
    check.checkTimeout = GlobalTickCount() + 90000; // 1 1/2 min
  }
  //@}

  //@{ interface to work with PendingFileSignatureChecks
  bool Pending(RString filename)
  {
    for (int i=0; i<pendingFileChecks.Size(); i++)
    {
      if ( stricmp(pendingFileChecks[i].filename,filename)==0 ) return true;
    }
    return false;
  }
  bool Remove(RString filename)
  {
    for (int i=0; i<pendingFileChecks.Size(); i++)
    {
      if ( stricmp(pendingFileChecks[i].filename,filename)==0 ) 
      {
        pendingFileChecks.Delete(i);
        return true;
      }
    }
    return false; // unexpected answer
  }
  void Add(RString filename, RString prefix)
  {
    PendingFileSignatureCheck &check = pendingFileChecks.Append();
    check.filename = filename;
    check.prefix = prefix;
    check.checkTimeout = GlobalTickCount() + 90000;  // 1 1/2 min
  }
  //@}
};

#include <Es/Memory/normalNew.hpp>

/// messages waiting to be sent
struct DelayedMessageInfo: public RefCount
{
  Ref<NetworkMessage> msg;
  NetworkMessageType type;
  float error;
  float invMaxAge;
  Vector3 pos;
  /// when the message was received
  Time timeReceived;

  DelayedMessageInfo(NetworkMessageType t, NetworkMessage *m);
  
  USE_FAST_ALLOCATOR
};

#include <Es/Memory/debugNew.hpp>

struct CDKeyCheck
{
  DWORD timeout;
  CDKeyCheck() : timeout(0) {}
};

//! Hashes collected from DataSignatureAnswer messages
class CollectedHashes
{
  struct CollectedHash
  {
    RString    fileName;
    RString    prefix;
    Temp<char> hash;          // DataContentHash
    Temp<char> ver2lev0hash;  // hash of (   DataContentHash + fileListHash + pbo prefix)
    Temp<char> ver2lev2hash;  // hash of (nonDataContentHash + fileListHash + pbo prefix)

    //! CollectedHash is initialized during initial SignatureCheck
    CollectedHash(RString filNam, Temp<char> &hs) : fileName(filNam), hash(hs) {}
    CollectedHash() {};
    ClassIsMovable(CollectedHash)
  };

  static const int SignatureCheckLevels = 3;
  struct RandomUsedFileCandidate
  {
    RString filename;
    RString prefix;
    int levelAsksCount[CollectedHashes::SignatureCheckLevels];

    RandomUsedFileCandidate() {}
    RandomUsedFileCandidate(RString fn, RString prf) : filename(fn), prefix(prf) 
    {
      for (int i=0; i<CollectedHashes::SignatureCheckLevels; i++) levelAsksCount[i]=0; //zero init
    }
    const char *GetKey() const { return prefix; }
    ClassIsMovableZeroed(RandomUsedFileCandidate)
  };

  //! collected hashes
  AutoArray<CollectedHash> hashes;
  //! collecting hashes from client is in progress (through NetworkServer::InitialSignaturesTest(...))
  bool  _collecting;
  //! maps prefix to the RandomUsedFileCandidate to track how often which asks are sent
  typedef MapStringToClass<RandomUsedFileCandidate, AutoArray<RandomUsedFileCandidate> >  PrefixToCandidate;
  PrefixToCandidate _candidatesMap;

public:
  static const int CollectEndIndex = 0x50be; //mnemo Sobe
  //! number of pbo files loaded
  int banksCount;  // -1 is default value meaning the client has not sent this information
  //! next time for full check (level 1)
  DWORD fullCheckNext;
  //! timeout time to get banksCount and iKnowLevel2 info
  DWORD bankInfoTimeout;
  //! client understands level 2 signature asks
  bool iKnowLevel2;

private:
  void Collecting(bool collect) { _collecting = collect; }
  void Clear() { hashes.Resize(0); }

  // return true when HACK detected
  bool TestLevelHash(CollectedHash &colHash, int index, int colIndex, Temp<char> &hash, int sigVer, int level)
  {
    if (!_collecting && index>=0)
    {
      if ( colIndex != (index % SignaturesCount()) ) 
        return true; //hack detected
    }
    switch (sigVer)
    {
    case 1:
      { // hash param is the DataContentHash
        if (hash.Size() != colHash.hash.Size() ||
          memcmp(hash.Data(), colHash.hash.Data(), hash.Size()) != 0) return true; //hack detected
      }
      break;
    case 2:
      {
        switch (level)
        {
        case 0:
        case 1: // hash param is the sigVer2, level=0 hash
          if (colHash.ver2lev0hash.Size()>0)
          {
            if (hash.Size() != colHash.ver2lev0hash.Size() ||
              memcmp(hash.Data(), colHash.ver2lev0hash.Data(), hash.Size()) != 0) return true; //hack detected
          }
          else
          { // collect ver2lev0hash
            colHash.ver2lev0hash = hash;
          }
          break;
        case 2: // hash param is the sigVer2, level=2 hash
          if (colHash.ver2lev2hash.Size()>0)
          {
            if (hash.Size() != colHash.ver2lev2hash.Size() ||
              memcmp(hash.Data(), colHash.ver2lev2hash.Data(), hash.Size()) != 0) return true; //hack detected
          }
          else
          { // collect ver2lev2hash
            colHash.ver2lev2hash = hash;
          }
          break;
        default:
          return true; // unrecognized level
        }
      }
      break;
    default: return true; //unknown version
    }
    return false; //ok
  }
  // Test the hash against the collectedHash with the given fileName
  // return true when HACK detected
  bool Test(RString fileName, int index, Temp<char> &hash, int sigVer, int level)
  {
    if (index>=0)
    {
      if (!SignaturesCount())
        return true; //HACK, unexpected message with index set during collecting phase
      index = index % SignaturesCount();
      if (hashes[index].fileName != fileName)
        return true; //HACK, index and fileName does not match
      return TestLevelHash(hashes[index], index, index, hash, sigVer, level);
    }
    else 
    {
      for (int i=0; i<hashes.Size(); i++)
      {
        if (hashes[i].fileName==fileName)
        {
          return TestLevelHash(hashes[i], index, i, hash, sigVer, level);
        }
      }
    }
    return true; //hack detected (sent signature not found)
  }

  // Test the hash against the collectedHash with the given bank Prefix
  // return true when HACK detected
  bool Test(int index, RString prefix, Temp<char> &dataContentHash, Temp<char> &levelHash, int sigVer, int level)
  {
    // use index when set
    if (index>=0)
    {
      if (!SignaturesCount())
        return true; //HACK, unexpected answer with index set during collecting phase
      // cut index to fit within range using modulo
      index = index % SignaturesCount();
      // Test prefix
      if (hashes[index].prefix.IsEmpty())
        hashes[index].prefix = prefix; // set it at last
      else if (hashes[index].prefix!=prefix)
        return true; //HACK, prefixes differ
      // Test dataContentHash
      Temp<char> &colDataContentHash = hashes[index].hash;
      if ( !colDataContentHash.Size() )
        colDataContentHash = dataContentHash; //possible collect it (but maybe empty again)
      else if ( 
                dataContentHash.Size() && 
               (dataContentHash.Size()!=colDataContentHash.Size() || (memcmp(dataContentHash.Data(), colDataContentHash.Data(), dataContentHash.Size()) != 0) )
              )
              return true; //dataContentHashes differ
      // Test levelHash
      if (levelHash.Size())
        return TestLevelHash(hashes[index], index, index, levelHash, sigVer, level);
      else
        return false; //OK, nothing else to check
    }
    else
    { // this happen only for clients before rev.78326
      if (levelHash.Size()>0)
      {
        for (int i=0; i<hashes.Size(); i++)
        {
          if (hashes[i].prefix==prefix)
          {
            return TestLevelHash(hashes[i], i, i, levelHash, sigVer, level);
          }
        }
      }
    }
#if 1
    // the following section is obsolete for clients from rev.78326, as these MUST send the index in their FileSignatureAnswers
    if (APP_VERSION_MINOR <= 58)
    {
      // index not set and prefix not found, but maybe the hash was collected already
      if (sigVer==1)
      { // test only DataContentHash
        bool allDataContentHashesCollected = true;
        for (int i=0; i<hashes.Size(); i++)
        {
          Temp<char> &colDataContentHash = hashes[i].hash;
          if (colDataContentHash.Size()==0)
          {
            allDataContentHashesCollected = false;
          }
          else if ( dataContentHash.Size()==colDataContentHash.Size() && 
            (memcmp(dataContentHash.Data(), colDataContentHash.Data(), dataContentHash.Size()) == 0) )
          { //found
            hashes[i].prefix = prefix; //set the prefix at last
            return false; //ok         
          }
        }
        return allDataContentHashesCollected; // when all collected but this one not found => HACK
      }
      else
      { // test levelHash
        bool allLevelHashesCollected = true;
        for (int i=0; i<hashes.Size(); i++)
        {
          Temp<char> &colDataContentHash = hashes[i].hash;
          switch (level)
          {
          case 0:
          case 1: 
            {
              Temp<char> &colLevelHash = hashes[i].ver2lev0hash;
              if ( levelHash.Size()==colLevelHash.Size() && 
                (memcmp(levelHash.Data(), colLevelHash.Data(), levelHash.Size()) == 0) )
              { //found
                if ( !hashes[i].prefix.IsEmpty() ) 
                  return true; //hack detected, previously collected prefix and hash does not match
                else
                {
                  hashes[i].prefix = prefix; //set the prefix at last
                  // test/store the dataContentHash (Note: it must be present for this level of sigVer 2)
                  if ( !colDataContentHash.Size() )
                    colDataContentHash = dataContentHash; //set it
                  else
                  { // test it
                    if ( dataContentHash.Size()==colDataContentHash.Size() && 
                      (memcmp(dataContentHash.Data(), colDataContentHash.Data(), dataContentHash.Size()) == 0) )
                      return false; //ok
                    else
                      return true; //HACK detected! - collected data hash does not match with this one
                  }
                }
                return false; //ok
              }
              else 
              {
                allLevelHashesCollected = false;
                if (dataContentHash.Size() && dataContentHash.Size()==colDataContentHash.Size() && 
                  (memcmp(dataContentHash.Data(), colDataContentHash.Data(), dataContentHash.Size()) == 0) )
                { // found by equal dataContentHash
                  if ( !hashes[i].prefix.IsEmpty() ) 
                    return true; //hack detected, previously collected prefix and hash does not match
                  else if (colLevelHash.Size()) 
                    return true; // HACK detected, already collected, but not equal
                  else
                  { //store it
                    colLevelHash = levelHash;
                  }
                  return false; //ok
                }
              }
            }
            //break; //unreachable code
          case 2: 
            {
              Temp<char> &colLevelHash = hashes[i].ver2lev2hash;
              if ( !colLevelHash.Size() )
              {
                allLevelHashesCollected = false;
                break; //continue in for cycle
              }
              if ( levelHash.Size()==colLevelHash.Size() && 
                (memcmp(levelHash.Data(), colLevelHash.Data(), levelHash.Size()) == 0) )
              { //found, but prefix was not found => HACK
                return true;
              }
              // continue in for cycle
            }
            break;
          }
        } //for cycle
        return allLevelHashesCollected; // HACK detected, all level hashes collected, but this new does not match
      }
    }
    else 
#endif
      return true; //HACK, index should be set in clients FileSignatureAnswers
  }

  // it search the hash
  int ArrayIx(RString fileName, Temp<char> &hash, int sigVer, int level)
  {
    for (int i=0; i<hashes.Size(); i++)
    {
      Temp<char> *colHash = NULL;
      if (sigVer==1) colHash = &hashes[i].hash;
      else switch (level)
      {
        case 0:
        case 1: colHash = &hashes[i].ver2lev0hash; break;
        case 2: colHash = &hashes[i].ver2lev2hash; break;
      }
      if ( hashes[i].fileName==fileName || 
        (colHash!=NULL && hash.Size() == colHash->Size() &&
        memcmp(hash.Data(), colHash->Data(), hash.Size()) == 0)
        )
        return i;
    }
    return -1; //not found
  }

public:
  CollectedHashes() : _collecting(false), banksCount(-1), iKnowLevel2(false) 
  {
    fullCheckNext = GlobalTickCount() + 40000 + toInt(40000 * GRandGen.RandomValue()); // initial delay 40 s - 80 s
    bankInfoTimeout = 0; // no timeout yet
  }

  void StartCollecting()
  {
    Clear();
    Collecting(true);
  }

  //! In spite of we are collecting hashes, the SignaturesCount suits better
  int SignaturesCount() const
  {
    if (_collecting) return 0;
    else 
    {
      return hashes.Size();
    }
  }

  //! AddHash based on fileName [DataSignatureAnswer]
  //! return true when HACK detected
  bool AddHash(int index, RString fileName, Temp<char> &hash, int sigVer, int level)
  {
    //RptF("AddHash: %s, ix=%d, ver=%d, lev=%d", cc_cast(fileName), index, sigVer, level);
    if (index<0)
    {
      if (!_collecting) return Test(fileName, index, hash, sigVer, level); //fileName based Test
      else  // we are collecting
      {
        Assert(level<2); //initial signature check should use level 0 in fact! We assume the hash is DataContentHash!
        int ix = ArrayIx(fileName, hash, sigVer, level);
        if (ix>=0) return true; //hack detected (the same answer sent second time)
        // new hash
        switch (sigVer)
        {
        case 1: hashes.Add(CollectedHash(fileName, hash)); break;
        case 2:
          {
            switch (level)
            {
            case 0:
            case 1:
              {
                int ix = hashes.Add(CollectedHash());
                CollectedHash &colHash = hashes[ix];
                colHash.fileName = fileName;
                colHash.ver2lev0hash = hash;
              }
              break;
            case 2:
              {
                int ix = hashes.Add(CollectedHash());
                CollectedHash &colHash = hashes[ix];
                colHash.fileName = fileName;
                colHash.ver2lev2hash = hash;
              }
              break;
            default: return true; //unrecognized level
            }
          }
          break;
        default: return true; //unrecognized version
        }
      }
      return false; //ok
    }
    else
    {
      if (_collecting)
      {
        if (index==CollectEndIndex)
        {
          _collecting = false;
          // when there are too few initial DataSignatureAnswers during "collecting" phase,
          // the client is supposed to be cheating. The limit is chosen conservative enough (30 banks).
          const int MinimalBankCount = 30;
          if ( SignaturesCount() < MinimalBankCount )
            return true;
        }
        else return true; // CollectEndIndex was expected! => HACK?
      }
      return Test(fileName, index, hash, sigVer, level); //fileName based Test
    }
  }
  //! AddHash based on bank prefix
  //! return true when HACK detected
  bool AddHash(int index, RString prefix, Temp<char> &dataContentHash, Temp<char> &levelHash, int sigVer, int level)
  {
    //RptF("AddHash: index=%d prefix=%s, ver=%d, lev=%d", index, cc_cast(prefix), sigVer, level);
    if (_collecting) return true; //this type of answers is not expected during initial collecting phase
    return Test(index, prefix, dataContentHash, levelHash, sigVer, level); //bank prefix based Test
  }

  //! Get the level to use for the next signature check
  int ActualCheckLevel()
  {
    DWORD time = GlobalTickCount();
    if (time > fullCheckNext)
    {
      fullCheckNext = time + 80000 + toInt(80000 * GRandGen.RandomValue()); // 80 s - 160 s
      return 1; // deep check (level 1)
    }
#if 1
    // FIX: level=2 checks are now processed async
    if ( rand()%4==0 && iKnowLevel2)
      return 2;
    else
      return 0; // shallow check (hash is taken from stored bank position, not recomputed from its content)
#else
    return 0;  //HOTFIX: Level 2 checks are too long to compute for both clients and server (async on clients and precomputation on servers should be implemented!)
#endif
  }

  //! Check timeout guarding whether client send her banksCount
  void CheckTimeouts(CheckSignatureTimeoutFunc &func)
  {
    if ( bankInfoTimeout && (bankInfoTimeout < GlobalTickCount()) )
    {
      (void)func();
    }
  }
 
  //! It calls the GWorld->RandomUsedFile more times to get the file which is not asked too often
  RString RandomUsedFile(Vector3Par pos, int level);
};

//! Server info about player
/*!
\patch_internal 1.12 Date 08/06/2001 by Jirka
- Added: Integrity check - config, exe, data
*/
struct NetworkPlayerInfo : public SerializeClass
{
  //! DirectPlay ID of player (client)
  int dpid;
  //! Voice over net ID of player (client)
  int dvid;

  //! chat channel player transmits
  int channel;  

  //! name of player
  RString name;
  //! client state for given player
  NetworkClientState clientState;
  //! message queue (candidates for aggregation)
  NetworkMessageQueue _messageQueue, _messageQueueNonGuaranteed;

  /// messages waiting to be sent to clients
  /** messages describing audio or visual effects, no delivery is guaranteed */
  RefArray<DelayedMessageInfo> _delayedMessages;

  //! ping statistics
  CalculateMinMaxAvg _ping;
  //! bandwidth statistics
  CalculateMinMaxAvg _bandwidth;
  //! desync statistics
  CalculateMinMaxAvg _desync; 

  //! player's person
  NetworkId person;
  //! player's unit
  NetworkId unit;
  //! player's group
  NetworkId group;

  //! player occupy private slot
  bool privateSlot;

  //! client has actual (valid) mission pbo file
  bool missionFileValid;

  //! client has actual (valid) mission pbo file
  bool paramFileValid;

  //! kick-off already requested
  bool kickedOff;

  //! Connection loosing (no message for 10 seconds) was already reported to other users
  bool connectionProblemsReported;

  //! Player is joining into game in progress
  bool _jip;
  //! Player was joined using JIP (used to distinguish between normal JIP and JIP forced after MP Load)
  bool _jipProcessed;

  /// Public IP address
  int _publicAddr;
  /// Private IP address
  int _privateAddr;
  /// Port where voice peer to peer socket is bind
  int _voicePort;

  /// how many times wrong login password was given
  int _loginFailedCount;
  /// how long is login disabled because of two many wrong attempts
  DWORD _loginDisabledUntil;

  /*!
  \name Integrity check 
  */
  //@{
  //! Questions asked
  AutoArray<IntegrityQuestionInfo> integrityQuestions;
  //IntegrityQuestion integrityQuestion[IntegrityQuestionTypeCount];
  //DWORD integrityQuestionTimeout[IntegrityQuestionTypeCount];
  //! when integrity check failed, we may wish to ask more detailed question
  Ref<IntegrityInvestigation> integrityInvestigation[IntegrityQuestionTypeCount];
  //! time when next random integrity check should be performed
  DWORD integrityCheckNext;
//((EXE_CRC_CHECK
  /// what test should be processed during the next random integrity check
  int integrityCheckNextIndex;
//))EXE_CRC_CHECK
  //@}

  /// Signature checks
  PendingSignatureChecks signatureChecks;

  /// Hashes collected during initial signature test
  CollectedHashes hashes;
  
  /// CDKey check
  CDKeyCheck cdKeyCheck;

  //! time when next regular check will take place
  DWORD regularCheckNext;
  //! order of the regular check
  DWORD regularCheckOrder;

  //! position of camera on given client
  Vector3 cameraPosition;
  //! time of last update of cameraPosition
  Time cameraPositionTime;

#if _VBS3
  //! Direction of camera on given client
  Vector3 cameraDirection;
  //! FOV on given client
  float cameraFov;
#endif

//{ DEDICATED SERVER SUPPORT
#if _ENABLE_DEDICATED_SERVER
  //! next message of the day index (-1 if done)
  int motdIndex;
  //! next message of the day time
  UITime motdTime;
#endif

  int nextQuestionId;

#if _ENABLE_MP
  MuteListMessage muteList;
#endif

  Time joined;

  /// key for encoding of CD Key validation communication
  RString challenge;

  int FindIntegrityQuestion(IntegrityQuestionType type, const IntegrityQuestion &q) const;
  int FindIntegrityQuestion(IntegrityQuestionType type) const;
  int FindIntegrityQuestion(int id) const;

  // Serialize for MP Save/Load
  LSError Serialize(ParamArchive &ar);


//}
};
TypeIsMovable(NetworkPlayerInfo)

//! map person to unit in server database
struct PersonUnitPair
{
  //! id of person
  NetworkId person;
  //! id of unit
  NetworkId unit;
};
TypeIsMovable(PersonUnitPair)

//! free Win32 allocated memory when variable is destructed

class AutoFree
{
  //! handle to Win32 memory obtained by GlobalAlloc call
  void *_mem;

  private:
  //! no copy constructor
  AutoFree(const AutoFree &src);

  public:
  //! attach Win32 memory handle
  /*!
  \param mem handle to Win32 memory obtained by GlobalAlloc call
  */
  void operator = (void *mem)
  {
    if (mem==_mem) return;
    Free();
    _mem = mem;
  }
  //! copy - reassing Win32 handle to new object
  /*! source will be NULL after assignement */
  void operator = (AutoFree &src)
  {
    if (src._mem == _mem) return;
    Free();
    _mem = src._mem;
    src._mem = NULL;
  }
  //! empty constructor
  AutoFree(){_mem = NULL;}
  //! construct from Win32 memory handle
  AutoFree(void *mem){_mem = mem;}
  //! convert to pointer
  operator void * () {return _mem;}
  //! destruct - free pointer
  ~AutoFree(){Free();}
#ifdef _WIN32
  //! free pointer explicitelly
  void Free(){if (_mem) GlobalFree(_mem),_mem = NULL;}
#else
  //! free pointer explicitelly
  void Free(){if (_mem) free(_mem),_mem = NULL;}
#endif
};

#pragma warning( disable: 4355 )
//! context structure for DownloadToFile function
struct DownloadToFileContext
{
  class DownloadToFileRunnable: public MultiThread::ThreadBase
  {
    DownloadToFileContext *_c;
  public:
    DownloadToFileRunnable(DownloadToFileContext *context) 
      : MultiThread::ThreadBase(MultiThread::ThreadBase::PriorityNormal, 64*1024), 
      _c(context) 
    {}
    unsigned long Run();
  };
  const char *url; //!< in: url to download from
  const char *file; //!< in: path of download destination
  bool result; //!< out: true when donwload was successful
  DownloadToFileRunnable _thread;
  const char *proxy; //! in: address of proxy server

  DownloadToFileContext() : _thread(this) {}
};

//! context structure for DownloadToMem function
/*!
\patch_internal 1.24 Date 09/19/2001 by Ondra
- Fix: more robust handling of memory deallocation.
May avoid memory leaks in case of download errors.
*/

struct DownloadToMemContext
{
  class DownloadToMemRunnable: public MultiThread::ThreadBase
  {
    DownloadToMemContext *_c;
  public:
    DownloadToMemRunnable(DownloadToMemContext *context) 
      : MultiThread::ThreadBase(MultiThread::ThreadBase::PriorityNormal, 64*1024), 
      _c(context) 
    {}
    unsigned long Run();
  };
  const char *url; //! in: url to download from
  AutoFree result; //! out: memory with downloaded file
  size_t *size; //! in: pointer to variable that will receive size of downloaded file
  DownloadToMemRunnable _thread;
  const char *proxy; //! in: address of proxy server

  DownloadToMemContext() : _thread(this) {}
};

//! Single vote in votings
struct Vote
{
  //! DirectPlay ID of player
  int player;
  //! Voted value
  AutoArray<char> value;

  //! Equality operator
  /*!
  \param val voted value
  \param valueSize size of voted value
  */
  bool HasValue(const char *val, int valueSize) const;
};
TypeIsMovable(Vote)

//! Single voting
class Voting : public AutoArray<Vote>
{
  friend class Votings;
  typedef AutoArray<Vote> base;

protected:
  /// Unique id of voting
  AutoArray<char> _id;
  /// Threshold when voting is valid
  float _threshold;
  /// Check if more than half players has the same value
  bool _selection;

  /// when there are to little players, assume other can still connect
  int _minVotePlayers;
public:
  //! Add new vote
  /*!
  \param player DirectPlay ID of voting player
  \param value voted value (NULL if none)
  \param valueSize size of voted value
  */
  int Add(int player, const char *value = NULL, int valueSize = 0);

  //! Return ID of Voting
  const AutoArray<char> &GetID() const {return _id;}

  //! Check if voting is valid
  bool Check(const AutoArray<PlayerIdentity> &identities) const;
  
  /// remote voting of any player which is no longer here
  void OnPlayerDeleted(int dpid);
  //! Get voted value
  /*!
  \param n number of votes
  */
  const AutoArray<char> *GetValue(int *n = NULL, int *n2=NULL) const;

  struct VoteInfo
  {
    const Vote *vote;
    int count;
    
    ClassIsSimple(VoteInfo)
  };
  
  static int CompareVoteInfo(const VoteInfo *v1, const VoteInfo *v2)
  {
    return v2->count-v1->count;
  }
  
  /// get ordered info about voting
  void GetOrder(AutoArray<VoteInfo> &result) const;
  //! Equality operator
  /*!
  \param id unique identification of voting
  \param idSize size of unique identification
  */
  bool HasID(const char *id, int idSize) const;
};
TypeIsMovable(Voting)

//! Voting system
class Votings : public AutoArray<Voting>
{
  typedef AutoArray<Voting> base;

public:
  //! Add new vote
  /*!
  \param server hosting server
  \param id unique identification of voting
  \param idSize size of unique identification
  \param threshold threshold when voting is valid
  \param player DirectPlay ID of voting player
  \param value voted value (NULL if none)
  \param valueSize size of voted value
  */
  void Add(
    NetworkServer *server, const char *id, int idSize, float threshold, int player, const char *value = NULL, int valueSize = 0,
    bool selection = false, int minVotePlayers=-1
  );

  /// find given voting
  const Voting *Find(int id) {return Find((const char *)&id,sizeof(id));}

  /// find given voting
  const Voting *Find(const char *id, int idSize) const;

  /// find index of a corresponding voting
  int FindIndex(const char *id, int idSize) const;

  /// delete voting
  void DeleteVoting(int id) {return DeleteVoting((const char *)&id,sizeof(id));}

  /// delete given voting
  void DeleteVoting(const char *id, int idSize);
  
  /// remote voting of any player which is no longer here
  void OnPlayerDeleted(int dpid);
  
  //! Check all votings (apply if satisfied)
  /*!
  \param server hosting server
  */
  void Check(NetworkServer *server);

  /// apply voting, if there are no votes return false
  bool Apply(NetworkServer *server, int id) {return Apply(server,(const char *)&id,sizeof(id));}
  /// apply voting, if there are no votes return false
  bool Apply(NetworkServer *server, const char *id, int idSize);
};

/// Custom file slot
struct CustomFilePaths
{
  RString _src;
  RString _dst;
  unsigned int _crc;
  int     _len;
  CustomFilePaths() {}
  CustomFilePaths(RString src, RString dst, unsigned int crc, int len) : _src(src), _dst(dst), _crc(crc), _len(len) {}

  DECLARE_NETWORK_OBJECT
  static NetworkMessageFormat &CreateFormat
    (
    NetworkMessageClass cls,
    NetworkMessageFormat &format
    );
  TMError TransferMsg(NetworkMessageContext &ctx);
};
TypeIsMovable(CustomFilePaths)

TypeIsMovable(TransferFileMessage)


//! pending download of squad information
class CheckSquadObject
{
friend class NetworkServer;

protected:
  //! identify player 
  PlayerIdentity _identity;
   //! out: squad information
  Ref<SquadIdentity> _squad;
  //! out: squad was created during download
  bool _newSquad;
#if _ENABLE_STEAM
  /// Steam authentication is completed (need not wait for it)
  bool _authDone;
  /// Steam authentication passed (client approved)
  bool _authOK;
#endif

  //! custom files subsets of which is can be sent to some clients
  AutoArray<CustomFilePaths> _customFileList;
  //! TransferFile messages for each custom file (created only when needed)
  AutoArray<AutoArray<TransferFileMessage> > _customFileMessages;
  //! custom files which given clients wants to sent
  struct CustomFilesForClient
  {
    int  _dpnid;   // client identification
    enum CFTState {CFSWaitingToList, CFSSendingFiles, CFSDone} _state;
    AutoArray<int> _filesWanted;
    //{ Files Transfer members 
    int _fileNo;          // index into _filesWanted, which file is currently being transfered
    int _fragmentNo;      // number of transfered fragments
    int _allFragmentCount; // used for progress bar
    int _allFragmentPos;   // used for progress bar
    DWORD _nextSendTime;  // time to send next bunch of TransferFile messages
    //}
    CustomFilesForClient() {}
    CustomFilesForClient(int dpnid) : _dpnid(dpnid), _state(CFSWaitingToList), _fileNo(0), _fragmentNo(0) , 
      _allFragmentPos(0), _allFragmentCount(200) //200 - approx 100kB before the valid count is not received
    {
      _nextSendTime=::GetTickCount();
    }
    ClassIsMovableZeroed(CustomFilesForClient)
  };
  AutoArray<CustomFilesForClient> _customFilesProgress;

  enum State {DownloadingSquad,DownloadingLogo,SendingCustomFiles,AllDone};
  //! state of download
  State _state;
  //! XML data of the squad
  AutoFree _squadXMLData;
  //! size of XML data
  size_t _squadXMLSize;
  //! URL of squad logo picture
  RString _logoUrl;
  //! path to squad logo picture (download destination)
  RString _logoFile;
  //! address of proxy server
  RString _proxy;

  //! state specific information (download logo file)
  DownloadToFileContext _logoContext;
  //! state specific information (download squad XML to memory)
  DownloadToMemContext _squadContext;
  //! active downloading thread (we need to know it in order to recognize the download is over (thread ends))
  MultiThread::ThreadBase *_activeThread;

public:
  //! start download
  CheckSquadObject(PlayerIdentity &identity, Ref<SquadIdentity> squad, bool newSquad, RString proxy, NetworkServer *server);
  //! destruct - wait until current download step is terminated
  ~CheckSquadObject();
  //! check if download is terminated
  bool IsDone(bool &canSendCustomFiles, NetworkServer *server);
  bool IsPlayer(int dpnid) { return _identity.dpnid==dpnid; };

  //! start XML data background download
  void StartDownloadingXMLSource();
  //! end XML data background download
  void EndDownloadingXMLSource();

  //! start logo file background download
  void StartDownloadingLogo(NetworkServer *server);

  //! custom files transfer to all clients. Returns true when no custom files are being sent.
  bool StartSendCustomFilesList(NetworkServer *server);

  //! process XML data, set default in case of error
  void ProcessXML();

  //! store content of NMTCustomFilesWanted message
  void SetCustomFilesWanted(int dpnid, const AutoArray<int> &filesWanted);

  State GetState() const { return _state; }

#if _ENABLE_STEAM

  bool IsAuthenticated() const {return _authDone;}

  // Tells us a client has been authenticated and approved to play by Steam (passes auth, license check, VAC status, etc...)
  STEAM_GAMESERVER_CALLBACK(CheckSquadObject, OnClientApprove, GSClientApprove_t, _callbackClientApprove);
  // Tells us we should deny a player from accessing the server
  STEAM_GAMESERVER_CALLBACK(CheckSquadObject, OnClientDeny, GSClientDeny_t, _callbackClientDeny);
#endif

protected:
  //! process XML data
  //! \return true if data are present and good
  bool DoProcessXML();

  void CreateCustomFilesList();
};

struct UpdateObjectInfo;
class LiveStatsUpdate;
static const int SessionAttributesCount = 12;

enum HackType
{
  HTUnsignedData,
  HTDifferentData,
  HTHackedData
};

#if _ENABLE_DEDICATED_SERVER

#define SERVER_EVENT_ENUM(type, prefix, XX) \
  XX(type, prefix, DoubleIdDetected) /* two users with the same id detected */ \
  XX(type, prefix, OnUserConnected) /* reaction to connected user */ \
  XX(type, prefix, OnUserDisconnected) /* reaction to disconnected user */ \
  XX(type, prefix, OnHackedData) /* reaction to pbo files manipulation */ \
  XX(type, prefix, OnDifferentData) /* reaction to file different from the server one */ \
  XX(type, prefix, OnUnsignedData) /* reaction to unsigned file */ \
  XX(type, prefix, RegularCheck) /* called regullary for each user  */ \

#ifndef DECL_ENUM_SERVER_EVENT
#define DECL_ENUM_SERVER_EVENT
DECL_ENUM(ServerEvent)
#endif
DECLARE_ENUM(ServerEvent, SE, SERVER_EVENT_ENUM)

#endif

typedef struct qr2_buffer_s *qr2_buffer_t;
typedef struct qr2_keybuffer_s *qr2_keybuffer_t;

//((EXE_CRC_CHECK
#if !_SUPER_RELEASE

/// info about exe segment to investigate
struct ExeSegmentInfo
{
  int offset;
  int size;
};
TypeIsSimple(ExeSegmentInfo);

/// the status of the client investigation
struct CompareExeClient
{
  int _dpid;
  int _answer;
  bool _answered;
};

/// Find the exe difference for given clients
struct CompareExeInvestigation
{
  /// list of segments to investigate
  AutoArray<ExeSegmentInfo> _stack;
  /// list of modified segments
  AutoArray<ExeSegmentInfo> _modified;
  /// item on the top of stack is already investigating
  bool _asked;
  /// the first client summary
  CompareExeClient _client1;
  /// the second client summary
  CompareExeClient _client2;

  /// prepare structure to investigation
  void Init(int dpid1, int dpid2);
  /// react to the answer
  void OnAnswered(int dpid, int answer);
};

#endif
//))EXE_CRC_CHECK

#if USE_NAT_NEGOTIATION_MANAGER
/// PeerToPeerManager's main task is to manage NAT negotiations
/// negotiations should be taking place one at a time
class PeerToPeerManager
{
  struct P2PNATInfo : public RefCount
  {
    static const DWORD NegotiationTimeout = 20000; //20sec
    int   _publicAddr;   //public IP identifies the NAT
    bool  _isNegotiating; //only one player behind the same NAT can negotiate at the same time
    DWORD _negotiationStartTime; //when does the negotiation started
    P2PNATInfo() : _isNegotiating(false) {};
    P2PNATInfo(unsigned int addr) : _publicAddr(addr), _isNegotiating(false) {};
  };
  struct P2PPlayerInfo : public RefCount
  {
    int           _dpid;       //direct play identification of player
    int           _publicAddr; //public IP identifies the NAT
    //bool        _behindNAT;   //player is behind NAT
    Ref<P2PNATInfo> _nat;     //different players behind the same NAT should share it
  };
  struct P2PEdge
  {
    Ref<P2PPlayerInfo> _player1;
    Ref<P2PPlayerInfo> _player2;
    P2PEdge() {};
    P2PEdge(P2PPlayerInfo *pl1, P2PPlayerInfo *pl2) : _player1(pl1), _player2(pl2) {}
    ClassIsMovableZeroed(P2PEdge)
  };
  //all players needing NAT negotiation (inserted in AddEdge, deleted in DestroyPlayer)
  RefArray<P2PPlayerInfo> _players;
  //negotiating players
  RefArray<P2PPlayerInfo> _negotiatingPlayers;
  //NATs corresponding to P2PEdges waiting for negotiation
  RefArray<P2PNATInfo> _nats;
  //couples of players (edges) waiting for negotiation
  AutoArray<P2PEdge> _edges;
  //we need server to get information about players (when these are new to PeerToPeerManager)
  NetworkServer *_server;
  //true when some something made structure dirty and update is needed
  bool _needUpdate;

  P2PPlayerInfo *FindPlayer(int dpid)
  {
    for (int i=0; i<_players.Size(); i++)
      if ( _players[i] && (_players[i]->_dpid==dpid) ) return _players[i].GetRef();
    return NULL;
  }
  P2PNATInfo *FindNAT(int addr)
  {
    for (int i=0; i<_nats.Size(); i++)
      if ( _nats[i] && (_nats[i]->_publicAddr==addr) ) return _nats[i].GetRef();
    return NULL;
  }
  P2PPlayerInfo *AddNewPlayer(int dpid);
  //! Update the arrays. Delete what is obsolete. Remove NULLs.
  void UpdateArrays();

public:  
  //! Constructor needs to know NetworkServer to send Connect and Result messages
  PeerToPeerManager(NetworkServer *server) : _server(server), _needUpdate(false) {}
  //! NATs of these players should be traversed when possible
  void AddEdge(int dpid1, int dpid2);
  //! announce that NAT of this player finished its negotiating (reaction to the NMTNatNegResult message)
  void UpdateNAT(int dpid);
  //! Check whether some new NAT negotiations can be started and start them
  void Update();
  //! When player need to be destroyed delete all it's storage
  void DestroyPlayer(int dpid);
  //! Diagnostics for #debug von
  RString GetDiagString()
  {
    BString<512> buf;
#ifdef _WIN32
    SYSTEMTIME syst;
    GetLocalTime(&syst);
    sprintf
      (
      buf, "Time: %2d:%02d:%02d",
      syst.wHour, syst.wMinute, syst.wSecond
      );
    return Format("P2PManager: %s waiting edges: %d, active negotiations: %d", 
      cc_cast(buf), _edges.Size(), _negotiatingPlayers.Size());
#else
    return Format("P2PManager: waiting edges: %d, active negotiations: %d", 
      _edges.Size(), _negotiatingPlayers.Size());
#endif
  }
};
#endif

//! Network Server
class NetworkServer : public NetworkComponent
{
protected:
  //! low-level server implementation
  SRef<NetTranspServer> _server;

#if USE_NAT_NEGOTIATION_MANAGER
  //! PeerToPeerManager is NAT negotiation scheduler to process only one NAT negotiation at a time
  PeerToPeerManager _p2pManager;
#endif

  //! server for peer to peer mission sharing
#ifdef PEER2PEER
  SRef<PeerToPeerServer> _p2pServer;
#endif

#if _USE_BATTL_EYE_SERVER
  /// BE Integration
  class BattlEye
  {
  public:
    enum BEInitFailedReason { BEReasonOther, BEReasonNotInstalled };
    BEInitFailedReason _initFailedReason;
  protected:
    /// dll handle
    HMODULE _library;
    bool _initialized;

    //@{
    /// callback functions
    typedef void __cdecl PrintMessageF(const char *message);
    typedef void __cdecl KickPlayerF(int player, const char *reason);
    typedef void __cdecl ExecuteCommandF(const char *command);
    static void __cdecl PrintMessage(const char *message);
    static void __cdecl KickPlayer(int player, const char *reason);
    static void __cdecl ExecuteCommand(const char *command);
    //@}

    //@{
    /// dll functions
    typedef bool __cdecl ExitF();
    typedef bool __cdecl RunF();
    typedef void __cdecl CommandF(const char *command);
    typedef void __cdecl AddPlayerF(int player, unsigned long address, unsigned short port, const char *name, void *guid, int guidlen);

    typedef void __cdecl RemovePlayerF(int player);
    typedef bool __cdecl InitF(const char *pstrGameVersion, 
      PrintMessageF *fnPrintMessage, KickPlayerF *fnKickPlayer, ExecuteCommandF *fnExecuteCommand, 
      ExitF **ppfnExit, RunF **fnRun, CommandF **fnCommand, AddPlayerF **fnAddPlayer, RemovePlayerF **fnRemovePlayer);
    typedef int __cdecl GetVerF();
  
    ExitF *_fnExit;
    RunF *_fnRun;
    CommandF *_fnCommand;
    AddPlayerF *_fnAddPlayer;
    RemovePlayerF *_fnRemovePlayer;
    //@}

  public:
    BattlEye();
    ~BattlEye();

    bool IsEnabled() const {return _library != NULL;}

    bool Init();
    bool Run(const NetworkServer *server);
    void Command(const char *command);
    void AddPlayer(int player, unsigned long address, unsigned short port, const char *name, void *guid, int guidSize);
    void RemovePlayer(int player);

  protected:
    void CleanUp()
    {
      _library = NULL;
      _fnExit = NULL;
      _fnRun = NULL;
      _fnCommand = NULL;
      _fnAddPlayer = NULL;
      _fnRemovePlayer = NULL;
      _initialized = false;
    }
    int GetBEVersion(HMODULE library) const;
  };
   BattlEye _beIntegration;
#endif

  //! list of players (server specific info)
  AutoArray<NetworkPlayerInfo> _players;
  //! list of network objects (actual state)
  RefArray<NetworkObjectInfo> _objects;

  //! mapping from roleIndex to player name (used in MP Saves)
  struct RoleIndex2PlayerName : public SerializeClass
  {
    RString _name;       //name of player
    int     _roleIndex;  //roleIndex
    RoleIndex2PlayerName() {}
    RoleIndex2PlayerName(RString name, int ix) : _name(name), _roleIndex(ix) {}

    LSError Serialize(ParamArchive &ar)
    {
      CHECK(ar.Serialize("name", _name, 1))
        CHECK(ar.Serialize("roleIndex", _roleIndex, 1))
        return LSOK;
    }
    ClassIsMovable(RoleIndex2PlayerName)
  };
  AutoArray<RoleIndex2PlayerName> _role2name;

  //! used to store pending messages
  struct NetPendingMessage
  {
    NetworkObjectInfo *info;
    NetworkPlayerObjectInfo *player;
    NetworkUpdateInfo *update;
    DWORD msgID;

    NetworkMessageType GetNMT() const;

    ClassIsSimple(NetPendingMessage)
  };

  //! list of pending messages - used for fast message lookup
  AutoArray<NetPendingMessage> _pendingMessages;
  //! map persons to units
  AutoArray<PersonUnitPair> _mapPersonUnit;
  //! original name of current mission
  RString _originalName;

  //! no other players are enabled to connect
  bool _sessionLocked;
  
  //! password protected session
  bool _password;

  //! equal mod list required by host
  bool _equalModRequired;

  //! data signatures required
  bool _verifySignatures;

  //! the least buildNo the server accepts
  int _buildNoRequired;

  /// validate CD Key using GameSpy SDK
  bool _validateCDKey;

  /// do not report session to GameSpy matchmaking
  bool _doNotReport;

  // InitQR was called (so SimulateQR should not call QR sdk)
  bool _qrInitDone;

  /// allow persistent missions - continue even when no players are connected
  bool _persistent;

//{ DEDICATED SERVER SUPPORT
#if _ENABLE_DEDICATED_SERVER
  //! DirectPlay ID of logged gamemaster (AI_PLAYER if no gamemaster)
  int _gameMaster;
  //! selected mission name
  RString _mission;
  //! selected mission cadet / veteran mode
  int _difficulty;

  //! gamemaster is only voted admin (cannot do shutdown etc.)
  bool _admin;

  //! kick off players with duplicate ID
  bool _kickDuplicate;

  //! is server dedicated
  bool _dedicated;
  //! server is asked to restart mission
  bool _restart;
  //! server is asked to reassign players
  bool _reassign;
  //! index of next mission in dedicated server config file
  int _missionIndex;
  //! preloaded dedicated server config file
  ParamFile _serverCfg;

  //@{
  //! Info about network trafic on dedicated server
  float _monitorInterval;
  DWORD _monitorNext;
  int _monitorFrames;
  int _monitorIn;
  int _monitorOut;
  //@}

  DWORD _debugNext;
  float _debugInterval;
  //! switched on debug flags
  FindArrayKey<int> _debugOn;

  //! messages of the day
  AutoArray<RString> _motd;
  //! messages of the day interval
  float _motdInterval;

  //! URL of Query & Reporting master server
  RString _reportingIP;

#if _XBOX_SECURE && _ENABLE_MP
  DWORD _nextStatsUpdate;
# if _XBOX_VER >= 200
  // Statistics handled by the NetworkManager
# else
  SRef<LiveStatsUpdate> _statsUpdate;
# endif
#endif

  /// evaluator for (dedicated) server scripts
  GameState _evaluator;
  /// global variables for _evaluator
  GameDataNamespace _globals;
  //! event handlers
  RString _eventHandlers[NServerEvent];

#endif
//}

  //! time when next ping status should be broadcast
  DWORD _pingUpdateNext;

  //! full path to mission pbo file
  RString _missionBank;

  //! Equals to _saveType for "Resume from load" or -1 otherwise
  int _currentSaveType;
  //! When mission was loaded from Save all players need to connect using JIP
  bool _missionWasLoadedFromSave;

  //! global (static) ban list
  FindArray<RString> _banListGlobal;
  //! local (dynamic) ban list
  FindArray<RString> _banListLocal;

  //! actually tested identities (squad competence)
  AutoArray< SRef<CheckSquadObject> > _squadChecks;
  DWORD _squadChecksTimeout;

  //! next available (short) id for player
  int _nextPlayerId;

  //! votings currently in progress
  Votings _votings;

  /// overall game de-sync - aggregated from all clients
  ConnectionDesync _connectionDesync;
  
  //@{ last valid result from _desyncTotal
  float _desyncTotalAvg;
  float _desyncTotalMax;
  //@}

  /// aggregate de-sync stats for all clients
  CalculateMinMaxAvg _desyncTotal;
  
  /// time when _desyncTotal was last averaged 
  DWORD _desyncTotalUpdateNext;
  
  //! threshold for votings
  float _voteThreshold;

  //! address of proxy server
  RString _proxy;

  //! current server state
  NetworkServerState _serverState;
  
  /// how long is the server state stable
  DWORD _serverStateTime;
  
  /// what timeout value was reported to the clients
  int _serverTimeOutSent;

  //! network messages, needed to send to joining player
  AutoArray<NetworkMessageQueueItem> _initMessages;

#if _ENABLE_CHEATS
  //! debugging window
  Link<DebugListWindow> _debugWindow;
#endif

#if _XBOX_SECURE && _ENABLE_MP

#if _XBOX_VER < 200
  XONLINETASK_HANDLE _sessionTask;
  XNKID _sessionKid;
  XNADDR _xnaddr;
#endif

#if _XBOX_VER >= 200

#else
  XONLINE_ATTRIBUTE _sessionAttributes[SessionAttributesCount];
#endif
  WCHAR _attrName[16];
  WCHAR _attrMission[40];
  WCHAR _attrIsland[16];
  /// server bandwidth in bps
  int _serverUpBandwidth; 
#endif

  int _totalSlots;

  /// last time when timeleft was reported to matchmaking
  DWORD _timeLeftLastTime;
  /// last value reported to matchmaking
  int _timeLeftLastValue;

  /// last value reported to broadcast / QoS
  int _timeLeftLastValueBcast;

  /// absolute limit of max. players allowed
  int _maxPlayersLimit;

  //! time synchronized to clients
  DWORD _serverTime;
  DWORD _serverTimeInitTickCount;
  DWORD _serverTimeOfEstimatedEnd;
  int   _secondsToEstimatedEnd;
  static const int ServerTimeUpdateInterval = 300*1000; // 5 minutes
  DWORD _nextServerTimeUpdate;

//((EXE_CRC_CHECK
#if !_SUPER_RELEASE
  CompareExeInvestigation _exeInvestigation;
#endif
//))EXE_CRC_CHECK

#if _VBS2 && _VBS2_LITE
  bool  _memoryWrite;
  DWORD _nextRanUpdate;
#endif

public:
  //! Constructor
  NetworkServer(NetworkManager *parent, RString name, RString password, int maxPlayers, int port);
  //! Destructor
  ~NetworkServer();

#if _XBOX_SECURE && _ENABLE_MP && _XBOX_VER < 200
  void GetSessionInfo(XNADDR &xnaddr, XNKID &xnkid) const
  {
    xnaddr = _xnaddr;
    xnkid = _sessionKid;
  }
#endif
  int GetSessionPort() const {return _server ? _server->GetSessionPort() : 0;}

  int NextPlayerID() {return _nextPlayerId++;}

  //! Creation of DirectPlay server and MP session
  /*!
  \param port IP port where server comunicate
  \param password session password
  */
  bool Init(RString name, RString password, int maxPlayers, int port);
  //! Destroying of MP session and DirectPlay server
  void Done();

  void InitVoice();

  //! Create debugging window
  void CreateDebugWindow();

  //! Kick off given player from game
  void KickOff(int dpnid, KickOffReason reason, const char *reasonStr=NULL);
  //! Ban given player (kick off + add to dynamic ban list)
  void Ban(int dpnid);

  //! Return if session was created
  bool IsValid() const {return _server != NULL;}

  bool IsDedicatedBotClient(int dpnid) const;

  bool IsLocalClient(int client) const
  {
    // Assume bot client is always local
    return client == BOT_CLIENT;
  }

  void RptFObjectNotFound(int creator, int id, NetworkMessageType type);

  void UpdateMaxPlayers(int maxPlayers, int privateSlots);

#if _XBOX_SECURE && _ENABLE_MP && _XBOX_VER < 200
  const XNKID *GetSessionKey() const {return &_sessionKid;}
#endif
  
//{ DEDICATED SERVER SUPPORT
#if _ENABLE_DEDICATED_SERVER
  //! Mark server as dedicated
  /*!
  \param config name (path) of config file
  */
  bool SetDedicated(RString config);

  /// add specific server scripting commands to the _evaluator
  void InitEvaluator();
#endif
//}
  //! force data signatures checking
  void SetVerifySignatures(int reqVersion);

  //! verify signature of the single file based on bank index (DataSignatureCheck)
  void CheckSignature(int dpnid, int fileIndex, int level);

  //! verify signature of the single file based on filename (FileSignatureCheck)
  void CheckSignature(int dpnid, RString filename, RString prefix, int level);

  //! signature check test with index=-1 was started and NetworkPlayerInfo::signatures are to be collected
  void InitSignatureCollecting(NetworkPlayerInfo *info);

  //! List of acceptedKey names in the form key1;key2;key3...
  //! When nonzero param 'maxLen', the return value has at most maxLen+4 chars. When truncated, the last one is ";..."
  RString GetSignaturesList(int maxLen=0);

  //! Get the hash precalculated during server initial banks load
  bool MyHash(Temp<char> &hash, RString prefix, int sigVer, int level);

  //! disable/enable connecting of further clients 
  void LockSession(bool lock = true)
  {
    if (_sessionLocked != lock)
    {
      _sessionLocked = lock;
      UpdateMatchmakingDescription(false);
      _server->UpdateLockedOrPassworded(_sessionLocked, _password);
    }
  }

  //! Retrieves URL of server
  bool GetURL(char *address, DWORD addressLen);
  int BuildNoRequired() { return _buildNoRequired; }

  //! Creates pbo file for selected mission
  void CreateMission(RString mission, RString world);
  //! Store name of pbo file for selected mission
  void SetOriginalName(RString name) {_originalName = name;}
  // force the _param1, _param2 and _paramsArray to contain undefined values, so InitMission will rewrite these by mission defaults
  void UndefMissionParams();
  //! Initialize mission
  /*!
  - create and send mission header structure (including difficulty settings)
  - create and send slots for sides and roles
  */
  void InitMission(int difficulty, RString displayName, bool createRoles, bool noCopy, RString owner, bool isCampaignMission);
  /// Delete all player roles
  void ClearRoles();
  /// Add player role
  int AddRole(Person *person);
  // set Mission to be loaded from save (detected in DisplayServer and processed in DisplayMultiplayerSetup)
  void SetCurrentSaveType(int saveType) { _currentSaveType=saveType; };
  void SetMissionLoadedFromSave(bool value) { _missionWasLoadedFromSave=value; }
  bool IsMissionLoadedFromSave() const { return _missionWasLoadedFromSave; }
  void KickAllPlayers();
  // Resets MP Roles, making them all available for connecting
  void ResetMPRoles();
  // get whether Mission should be loaded from save 
  int  GetCurrentSaveType() { return _currentSaveType; };
  /// reset current saveType (save was deleted)
  void ResetCurrentSaveType(int saveType) 
  {
    if ( saveType==_currentSaveType || saveType<0 ) _currentSaveType=-1;
  }
  void SetPlayerRoleLifeState(int roleIndex, LifeState lifeState);
  /// Send a PlayerRole update of _playerRoles[roleIndex]
  void SendPlayerRoleUpdate(int roleIndex);
  /// save/load the MP game (save of networking data)
  LSError SerializeMP(ParamArchive &ar);
  /// Save/Load Roles. It uses separate save file. [xxx].roles.saveFileExt
  bool LoadRolesFromSave();
  void SaveRolesToSave(SaveGameType type);

  void OnDraw();
  void OnSimulate();
  /// time critical network update - need to be called several times in frame
  void FastUpdate();
  void OnSendComplete(DWORD msgID, bool ok);
  void OnSendStarted(DWORD msgID, const NetworkMessageQueueItem &item);
  void OnCreatePlayer(int player, bool privateSlot, const char *name, unsigned long inaddr);
  bool OnCreateVoicePlayer(int player);
  void OnMessage(int from, NetworkMessage *msg, NetworkMessageType type);

  /// remove initMessages overridden by this one
  void AddInitAndRemoveOverridden(NetworkMessageType type, NetworkMessage * msg, int **updateI=NULL);
  /// get diagnostics statistics about initMessages
  RString GetAddInitAndRemoveOverriddenStat();

  //! perform regular memory clean-up
  unsigned CleanUpMemory();

  //! Process received system messages
  void ReceiveSystemMessages();
  //! Process received user messages
  void ReceiveUserMessages();
  //! Destroy all received system messages
  void RemoveSystemMessages();
  //! Destroy all received user messages
  void RemoveUserMessages();
  //! Get position of randomly chosen player
  Vector3 RandomPlayerPos() const;
  //! Perform all active integrity investigations
  void PerformIntegrityInvestigations();
  //! Perform initial (overall) integrity check
  void PerformInitialIntegrityCheck(NetworkPlayerInfo &pi);
  //! Perform quick random integrity check
  void PerformRandomIntegrityCheck(NetworkPlayerInfo &pi);
  //! perform check on single file
  void PerformFileIntegrityCheck(const char *file, int dpid=-1);

  //! Check if given question is part of any investigation
  bool IntegrityAnswerReceived
  (
    NetworkPlayerInfo *pi,
    IntegrityQuestionType qType,
    const IntegrityQuestion &q, bool answerOK
  );

  DWORD SendMsg
  (
    int to, NetworkSimpleObject *object, NetMsgFlags dwFlags
  );
  bool DXSendMsg(int to, NetworkMessageRaw &rawMsg, DWORD &msgID, NetMsgFlags dwFlags);

  //! Retrieve list of players
  void GetPlayers(AutoArray<NetPlayerInfo, MemAllocSA> &players);

  //! Return connection quality to server
  ConnectionDesync GetConnectionDesync() const {return _connectionDesync;}
  
  NetworkMessageFormatBase *GetFormat(/*int client, */int type);
  const NetworkMessageFormatBase *GetFormat(/*int client, */int type) const;

  //! Set current server state and send to clients
  void SetServerState(NetworkServerState state);
  //! Return client state for given player
  NetworkClientState GetClientState(int dpnid);
  //! Return name for given player
  RString GetPlayerName(int dpid);
  //! Return camera position for given player
  Vector3 GetCameraPosition(int dpid);

  /// Return the IP address of the connected client
  bool GetClientAddress(int client, sockaddr_in &address);

  NetworkObject *GetObject(NetworkId &id);

  //! Unregister all objects
  void DestroyAllObjects();

  //! Send mission pbo file to clients
  /*!
  \param dpnid client, server will send mission to (0 for all clients)
  */
  void SendMissionFile(int dpnid = 0);

  //! Calculate bandwidth estimation to given client
  int GetBandwidthEstimation(int dpnid, int nPlayers);

  //! Check if all internal structures are in correct state
  bool CheckIntegrity() const;

  //! Check if pending message structures are in correct state
  bool CheckIntegrityOfPendingMessages() const;

  const char *GetDebugName() const {return "Server";}

  //! Ask client for check of integrity
  /*!
  \param dpnid DirectPlay ID of asked client
  \param type type of integrity check
  \param q parameters of integrity check
//((EXE_CRC_CHECK
  \param timeout timeout in ms
//))EXE_CRC_CHECK
  */
//((EXE_CRC_CHECK
  // timeout should be at least 90+90 seconds to be sure possible disconnection timeout occurs first
  bool IntegrityCheck(int dpnid, IntegrityQuestionType type, const IntegrityQuestion &q, int timeout = 200000);
//))EXE_CRC_CHECK

  //! Client has sent her bankCount. Detect whether it differs to the previously sent value.
  void UpdatePlayerBankCount(NetworkPlayerInfo *info, int bankCount, bool knowLevel2);
  //! Report Wrong signatures (write also a line inside DS log window)
  void ReportWrongSignatures(int from, RString filename, const NetworkPlayerInfo *info);

  //! Reaction when check of integrity failed for given client
  /*!
  \param dpnid DirectPlay ID of asked client
  \param type type of integrity check
  \param info additional information
  \param final check is final and should be displayed to all players
  */
  void OnIntegrityCheckFailed
  (
    int dpnid, IntegrityQuestionType type,
    const char *info, bool final
  );

  //! Creates player identity
  /*!
  Called asynchronously when identity is confirmed
  \param ident player identity
  \param squad player's squad
  */
  void CreateIdentity(PlayerIdentity &ident, Ref<SquadIdentity> squad);

  //! Process result of voting when voting is valid
  /*!
  \param id unique identification of voting
  \param value voted value
  */
  void ApplyVoting(const AutoArray<char> &id, const AutoArray<char> *value);

  //! some admin state change - scan admin status of all players
  void UpdateAdminState();

  //! Delete player info (unregister player)
  void OnPlayerDestroy(int dpid);

  //! Set estimated end of mission time
  /*!
  \param time estimated time
  */
  void SetEstimatedEndTime(Time time);

  //! Roles were loaded from SAVE file and should be resent to all players
  void SendPlayerRolesMessages();
  void AssignRolesAfterLoad();

  /// read to time left estimation
  int GetSessionTimeLeft() const;

  //! Return absolute limit of max. players allowed
  /** does not include virtual slot on ded. server */
  int GetMaxPlayersLimit() const {return _maxPlayersLimit;}

  /// Return maximal number of players according to bandwidth estimation
  /** does not include virtual slot on ded. server */
  int GetMaxPlayersSafe(int bandwidthPerClient, int round) const;

  //! Return maximal allowed number of players on server based on mission and user settings
  /** does include virtual slot on ded. server */
  int GetMaxPlayers() const;
  
  //! Return name of mission pbo file on server to optimize transfer to bot client
  RString GetMissionFilename() const {return _missionBank;}

  //! When player joined to game
  Time GetPlayerJoined(int dpid) const;

  //! Find info about given player
  NetworkPlayerInfo *GetPlayerInfo(int dpid);

//((EXE_CRC_CHECK
#if !_SUPER_RELEASE
  /// Find the exe difference for given clients
  void CompareExe(int dpid1, int dpid2) {_exeInvestigation.Init(dpid1, dpid2);}
#endif
//))EXE_CRC_CHECK

  /// Find owner of given network object (used in scripted function owner)
  int GetOwner(const NetworkId &id) const;

protected:
  //! Create player info (register player)
  NetworkPlayerInfo *OnPlayerCreate(int dpid, bool privateSlot, const char *name, unsigned long inaddr);
  //! Force state change for given client
  void SetClientState(int dpnid, NetworkClientState state);

#if _ENABLE_DEDICATED_SERVER
  // select game master on dedicated server
  void SetGameMaster(int player, bool admin);

  /// set event handler
  void SetEventHandler(RString name, RString value);
  /// process event handler (return if the event was handled)
  bool OnEvent(ServerEvent event, float par);
  /// process event handler (return if the event was handled)
  bool OnEvent(ServerEvent event, float par1, float par2);
  /// process event handler (return if the event was handled)
  bool OnEvent(ServerEvent event, float par1, RString par2);
#endif

  //! find pending message in pending message list based on ID
  int FindPendingMessage(DWORD msgID) const;
  //! find pending message in pending message list based on update
  int FindPendingMessage(NetworkUpdateInfo *update) const;
  //! add new pending message
  void AddPendingMessage
  (
    DWORD msgID, NetworkObjectInfo *info,
    NetworkUpdateInfo *update, NetworkPlayerObjectInfo *player
  );

  /// Find owner of given network object
  int FindOwner(const NetworkId &id);

  //! Find info about given network object
  NetworkObjectInfo *GetObjectInfo(const NetworkId &id) const;
  //! Create object info (register object)
  NetworkObjectInfo *OnObjectCreate(const NetworkId &id, int owner, NetworkMessage *msg, NetworkMessageType type);
  //! Delete object info (unregister object), return owner id
  int PerformObjectDestroy(const NetworkId &id);
  //! Delete object info (unregister object) and send message about destruction
  void OnObjectDestroy(const NetworkId &id);

  //! Called when update message received
  /*!
  \param id network object ID
  \param from sender DirectPlay ID
  \param msg update message
  \param type update message type
  \param cls update message class
  */
  NetworkObjectInfo *OnObjectUpdate(NetworkId &id, int from, NetworkMessage *msg, NetworkMessageType type, NetworkMessageClass cls);
  //! Send update message
  /*!
  \param pInfo info about receiving player
  \param oInfo info about object to update
  \param cls update message class
  \param dwFlags DirectPlay flags
  */
  int UpdateObject(NetworkPlayerInfo *pInfo, NetworkObjectInfo *oInfo, NetworkMessageClass cls, NetMsgFlags dwFlags);
  //! Calculate error multiplier
  /*!
  \param type update message type
  \param cameraPosition position of camera on receiving client
  \param position updated object position
  */

#if _VBS3
  float CalculateErrorCoef
  (
    NetworkMessageType type, Vector3Par cameraPosition, Vector3Val position,Vector3Par cameraDirection = VForwardP, float cameraFov = 0.75 
  );
#else
  float CalculateErrorCoef
  (
    NetworkMessageType type, Vector3Par cameraPosition, Vector3Val position
  );
#endif
  //! Calculate time error (message fading) between two stored states (messages)
  /*!
  \param type update message type
  \param msg1 first message
  \param msg2 second message
  */
  float CalculateErrorTime
  (
    NetworkMessageType type, NetworkMessage *msg1, NetworkMessage *msg2
  );

  //! Owner of some object changes
  /*!
  \param id ID of network object
  \param from DirectPlay ID of old owner
  \param to DirectPlay ID of new owner
  */
  void ChangeOwner(NetworkId &id, int from, int to);
  //! Check and update objects ownership when group leader changes
  /*!
  \param group id of group
  \param leader id of new leader
  */
  void UpdateGroupLeader(NetworkId &group, NetworkId &leader);

  //! Send mission info (mission heade, side slots and role slots) to client
  /*!
  \param DirectPlay ID of receiving client
  \param onlyPlayers do not send mission header
  */
  void SendMissionInfo(int to, bool onlyPlayers = false);

  //! Send all agregated and update messages
  void SendMessages();
  //! Check if fade out players are to kicked off
  void CheckFadeOut();
  //! Calculate limits for message sending
  void EstimateBandwidth(NetworkPlayerInfo &pInfo, int nPlayers, int &nMsgMax, int &nBytesMax);
  //! Calculate errors and place object in list sorted by error
  /*!
  \param objects output sorted list of objects
  \param pInfo info about receiving player
  */
  void CreateObjectsList(AutoArray<UpdateObjectInfo, MemAllocSA> &objects, NetworkPlayerInfo &pInfo);
  //! Prepare next object update message to message queue
  bool PrepareNextUpdate(NetworkPlayerInfo &pInfo, AutoArray<UpdateObjectInfo, MemAllocSA> &objects, int &next);

  //! Dedicated server simulation
  void SimulateDS();

  /// broadcast current mission voting status to all clients
  void MissionVotingEcho();
  /// check time out for current server state
  bool CheckTimeOut(int readyCount, int totalCount, DWORD ready, DWORD notReady);

  /// check how much players is need to consider the server to be full
  int PlayerServerFull() const;

  //! Show message in both console and chat
  void ServerMessage(const char *text);

  //! Show client-side localized message in both console and chat
  void ServerMessage(const LocalizedFormatedString &text);
  
  //! Find unit id for given person id
  NetworkId PersonToUnit(NetworkId &person);
  //! Find person id for given unit id
  NetworkId UnitToPerson(NetworkId &unit);
  //! Add pair to map persons to unit
  int AddPersonUnitPair(NetworkId &person, NetworkId &unit);

  //! Find players on chat channel
  /*!
  \param players output array of players
  \param units chat target units 
  \param from DirectPlay ID of sender
  \param voice true for Voice Over Net transfer
  */
  void GetPlayersOnChannel(AutoArray<int, MemAllocSA> &players, AutoArray<NetworkId> units, DWORD from, bool voice);
  //! Find players on chat channel
  /*!
  \param players output array of players
  \param channel chat channel
  \param from DirectPlay ID of sender
  \param voice true for Voice Over Net transfer
  */
  void GetPlayersOnChannel(AutoArray<int, MemAllocSA> &players, int channel, DWORD from, bool voice);

  /// check if given player in out of game (dead, or in the lobby)
  bool CheckPlayerInLobby(const NetworkPlayerInfo &info) const;
  /*!
  \name GameSpy Query & Reporting SDK support
  */
  //@{
//{ QUERY & REPORTING SDK
#if _ENABLE_GAMESPY
  //! Close Query & Reporting SDK support
  void DoneQR();
  //! Simulation step of Query & Reporting SDK support
  void SimulateQR();
  //! Information about change of inner game state
  void ChangeStateQR();
public:
  //! Initialize Query & Reporting SDK support
  void InitQR(bool isPrivate);

  /// Called when a server key needs to be reported
  void OnQueryServer(int keyid, qr2_buffer_t outbuf);
  /// Called when a player key needs to be reported
  void OnQueryPlayer(int keyid, int index, qr2_buffer_t outbuf);
  /// Called when a team key needs to be reported
  void OnQueryTeam(int keyid, int index, qr2_buffer_t outbuf);
  /// Called when we need to report the list of keys we report values for
  void OnQueryKeys(int keytype, qr2_keybuffer_t keybuffer);
  /// Called when we need to report the number of players and teams
  int OnQueryCount(int keytype);

#if _ENABLE_STEAM
  /// Initialization of Steamworks SDK server reporting interface
  void InitSteam();
  /// Steamworks SDK server reporting interface clean up
  void DoneSteam();
  /// Simulation of Steamworks SDK callbacks
  void SimulateSteam();
  /// Inform Steam master server about the change of the inner game state
  void ChangeStateSteam();

  // Tells us when we have successfully connected to Steam
  STEAM_GAMESERVER_CALLBACK(NetworkServer, OnSteamServersConnected, SteamServersConnected_t, _callbackSteamServersConnected);
  // Tells us when we have been logged out of Steam
  STEAM_GAMESERVER_CALLBACK(NetworkServer, OnSteamServersDisconnected, SteamServersDisconnected_t, _callbackSteamServersDisconnected);
  // Tells us that Steam has set our security policy (VAC on or off)
  STEAM_GAMESERVER_CALLBACK(NetworkServer, OnPolicyResponse, GSPolicyResponse_t, _callbackPolicyResponse);
#endif

#if _VERIFY_CLIENT_KEY
   /// called when autorization of CD Key finished
  void OnCDKeyAuthorize(int player, int authenticated, const char *errmsg);
#endif
protected:
#endif
//}
  //@}

#if _ENABLE_DEDICATED_SERVER
public:
  //! Move statistics update to the client
  void AskForStatsWrite(
    int player,
    int board, const AutoArray<XONLINE_STAT> &stats,
    int boardTotal, const AutoArray<XONLINE_STAT> &statsTotal
    );
protected:
#endif

  //! Update session description server sending via LAN broadcast
  void UpdateSessionDescription();
  
  //! Update session description server sending via both mathmaking and LAN broadcast
  void UpdateMatchmakingDescription(bool changedParams);

#if _XBOX_SECURE && _ENABLE_MP

# if _XBOX_VER >= 200
  //! Initialize _sessionAttributes for Xbox matchmaking service
  void XOnlineInitSession();
# endif

  //! Session update for Xbox matchmaking service
  void XOnlineUpdateSession(bool updateAttributes, bool qosOnly=false);


#endif

  void SendMuteList(const PlayerIdentity &id);
  
  void EnqueueMsg(int to, NetworkMessage *msg, NetworkMessageType type);
  void EnqueueMsgNonGuaranteed(int to, NetworkMessage *msg, NetworkMessageType type);

//{ DEDICATED SERVER SUPPORT
#if _ENABLE_DEDICATED_SERVER
  //! Set interval in which dedicated server sends info about transfer rates to gamemaster
  void Monitor(float interval);
  void OnMonitorIn(int size);
  
public:
  void DebugAsk(RString str);
protected:
  void DebugAnswer(RString str);

  int ConsoleF(const char *format, ...);
#endif
//}

  //! Process message NMTPlayerRole
  void OnMessagePlayerRole(int from, NetworkMessageType type, NetworkMessageContext &ctx);
  void OnMessagePlayerRoleUpdate(int from, NetworkMessageType type, NetworkMessageContext &ctx);

  //! Client state changed
  void OnClientStateChanged(int dpnid, NetworkClientState state);

  //! Update of weapons pool in briefing
  void UpdateWeaponsPool();

  //! Update of magazines pool in briefing
  void UpdateMagazinesPool();

  //! Update of backpacks pool in briefing
  void UpdateBackpacksPool();

  /// update server database when player leave mission
  void DisconnectPlayer(int dpnid, NetworkId playerPerson);

  /// calculate how important is given object to given player
  float CalculateErrorCoef(const NetworkPlayerInfo &pInfo, const NetworkObjectInfo &oInfo);

  /// select the best role to assign player into (balancing)
  int SelectRole(RString name, int dpid) const;

  /// event handler - wrongly signed data or signature check question timed out
  void OnHackedData(HackType type, int dpid, RString filename);

  /// event handler - two players with the same CD-KEY
  void OnDoubleIdDetected(const PlayerIdentity &newIdentity, const PlayerIdentity &oldIdentity);
public:
  /// report hacked data of given player
  bool ProcessHackedData(NetworkPlayerInfo &pi);
};

class ReportHackedData : public CheckSignatureTimeoutFunc
{
  NetworkPlayerInfo &_pi;
  NetworkServer     &_server;
public:
  ReportHackedData(NetworkPlayerInfo &pi, NetworkServer &server) : _pi(pi) , _server(server) {}
  bool operator ()();
};

///////////////////////////////////////////////////////////////////////////////
// Network Manager

TypeIsSimple(INetworkUpdateCallback *)

//! Implementation of basic network functions
class NetworkManager : public INetworkManager
{
friend class NetworkComponent;

protected:
  SRef<NetTranspSessionEnum> _sessionEnum;
  //! time of last call of EnumHosts
  UITime _lastEnumHosts;
  //! IP address of computer (empty for local network) where search for sessions
  RString _ip;
  //! port where search for sessions
  int _port;
  //! additional remote hosts
  AutoArray<RemoteHostAddress> _hosts;

  //! network server component
  SRef<NetworkServer> _server;
  //! network client component
  SRef<NetworkClient> _client;
  //! voice over net disabled
  bool _disableVoN;
  //! voice over net codec quality for encoding
  int _vonCodecQuality;
  //! log file for dedicated server can be specified in server.cfg by entry logFile
  RString _dedServerLogFileName;
  FILE *_dedServerLogFile;

#if _XBOX_SECURE && _ENABLE_MP

#if _XBOX_VER >= 200
  // Xbox Live session registration
  
  /// type of the session
  SessionType _sessionType;
  /// handle of the session
  HANDLE _sessionHandle;
  /// info about the session
  XSESSION_INFO _sessionInfo;
  /// additional info for the arbitration
  ULONGLONG _sessionNonce;

  /// session is currently played and statistics collected, inside XSessionStart ... XSessionEnd block
  bool _sessionStarted;
#else
  XONLINETASK_HANDLE _logonTask;
  XONLINETASK_HANDLE _friendsTask;
  XONLINETASK_HANDLE _muteListTask;
#endif

  /// task result, like XONLINE_S_LOGON_CONNECTION_ESTABLISHED or XONLINE_S_LOGON_USER_HAS_MESSAGE
  HRESULT _logonResult;

  DWORD _muteBufferSize;
#if _XBOX_VER >= 200
  // Mute list is handled by the client
#else
  XONLINE_MUTELISTUSER _muteBuffer[MAX_MUTELISTUSERS];
#endif

  
  DWORD _playerState;
  DWORD _microphone;
  DWORD _headphone;
  // Pending invitations are handled by Xbox Guide on Xbox 360
#if _XBOX_VER < 200
  Ref<Texture> _pendingInvitationTexture; // for in-game notification
  PackedColor _pendingInvitationColor;
  float _pendingInvitationTimeout;
  float _pendingInvitationBlinkingPeriod;
  DWORD _pendingInvitationUntil;
  bool _pendingRequest;
  bool _pendingInvitation;
#endif
  bool _cloaked;
  /// microphone and headphone are allocated to XHV
  /** when voice is disabled, communicator is detached and voice is not mixed at all */
  bool _voiceDisabled;
#endif

#if _ENABLE_MP && (!_GAMES_FOR_WINDOWS && !defined _XBOX)
  // TODO: Unify X360 / PC mute list handling
  MuteListMessage _muteList;
#endif

  float _pendingInvitationX;
  float _pendingInvitationY;
  float _pendingInvitationW;
  float _pendingInvitationH;

  Ref<Texture> _cqPoorTexture;
  Ref<Texture> _cqBadTexture;
  Ref<Texture> _cqDesyncLow;
  Ref<Texture> _cqDesyncHigh;

  //! voice communicator connected
  bool _isVoice;

  FindArray<INetworkUpdateCallback *> _updateCallbacks;

#if _ENABLE_GAMESPY
  bool _trackUsageDone;
#endif

  /// request to disable voice
  EDisableVoiceRequest _disableVoiceRequest;
  /// time when the request was set
  UITime _disableVoiceRequestTime;

public:
  //! Constructor
  NetworkManager();
  //! Destructor
  ~NetworkManager();

  bool IsServer() const {return _server != NULL;}
  //! Return if client is created
  bool IsClient() const {return _client != NULL;}

  int RequiredBuildNo() { return (_server!=NULL) ? _server->BuildNoRequired() : 0; }

  //! Access to server component
  NetworkServer *GetServer() {return _server;}
  //! Access to client component
  NetworkClient *GetClient() {return _client;}

  virtual void TrackUsage();

  //! return if voice communicator is connected
  bool IsVoice() const {return _isVoice;}
  //! Return if player is voice banned
  bool IsVoiceBanned() const;
  //! return if voice over net is disabled
  bool IsVoNDisabled() const {return _disableVoN;}
  //! set whether voice over net is disabled
  void SetVoNDisabled(bool val) {_disableVoN=val;}
  //! return the log File for Dedicated server (entry logFile in server.cfg)
  RString GetDedicatedServerLogFileName() { return _dedServerLogFileName; }
  FILE *GetDedicatedServerLogFile();
  void CloseDedicatedServerLogFile() { if (_dedServerLogFile) { fclose(_dedServerLogFile); _dedServerLogFile=NULL; } }
  //! VoN codec quality for encoding
  int VoNCodecQuality() { return _vonCodecQuality; }
  //! VoN codec quality for encoding (should propagate into VoNRecorder too)
  void SetVoNCodecQuality(int quality);

  bool Init(RString ip, int port, bool startEnum = true);
  void Init();
  void Done();
  void RemoveEnumeration();

  bool IsVoicePlaying(int dpnid) const;
  bool IsVoiceRecording() const;
  int CheckVoiceChannel(int dpnid, bool &audible2D, bool &audible3D) const;
  void IsVoice2D3DPlaying(int dpnid, bool &audible2D, bool &audible3D) const;

  void SetVoiceON();
  void SetVoiceOFF();
  void SetVoiceToggleOn(bool value);
  /// sets threshold for silence analyzer (0.0f - 1.0f)
  void SetVoNRecThreshold(float val);
  void SetVoiceCapture(bool value);
  //! request disabling of voice
  virtual void SetDisableVoiceRequest(EDisableVoiceRequest request);


#if _ENABLE_CHEATS
  void VoNSay(int dpnid, int channel, int frequency, int seconds);
#endif

  void CustomFilesProgressClear();
  void CustomFilesProgressGet(int &max, int &pos);
  DWORD GetServerTime();
  DWORD GetEstimatedEndServerTime();

#if _GAMES_FOR_WINDOWS || (defined _XBOX && _XBOX_VER >= 200)

  virtual SessionType GetSessionType() const {return _sessionType;}

  /// returns if any session exists
  virtual bool SessionExists() const {return _sessionHandle != NULL;}

  virtual bool IsSignedIn() const {return true;}

  // Appear offline implemented by Xbox Guide on Xbox 360
  void Cloak(bool set = true) {}
  bool IsCloaked() const {return false;}

  void DisableVoice(bool disable);


#elif defined _XBOX

  virtual HRESULT SignIn(const XONLINE_USER &user);
  virtual void SignOff();

  virtual bool IsSignedIn() const
  {
#if _XBOX_SECURE && _ENABLE_MP
    return _logonTask != NULL;
#else
    return false;
#endif
  }

  virtual void SilentSignInStart();
  virtual void SilentSignInFinish();
  virtual HRESULT SilentSignInResult() const
  {
#if _XBOX_SECURE && _ENABLE_MP
    return _logonResult;
#else
    return S_OK;
#endif
  }
  virtual void IgnoreMessageAtSignIn()
  {
    #if _XBOX_SECURE && _ENABLE_MP
    Assert(_logonResult==XONLINE_S_LOGON_USER_HAS_MESSAGE);
    if (_logonResult==XONLINE_S_LOGON_USER_HAS_MESSAGE)
    {
      _logonResult = XONLINE_S_LOGON_CONNECTION_ESTABLISHED;
    }
    #endif
  }

  virtual void Cloak(bool set = true)
  {
#if _XBOX_SECURE && _ENABLE_MP
    _cloaked = set;
#endif
  }
  virtual bool IsCloaked() const
  {
#if _XBOX_SECURE && _ENABLE_MP
    return _cloaked;
#else
    return false;
#endif
  }

#if _XBOX_SECURE && _XBOX_VER < 200 && _ENABLE_MP
  MuteListMessage &GetMuteList() {return _muteList;}
#endif

  void DisableVoice(bool disable);

protected:
  HRESULT CheckSignInResult(HRESULT result);

  bool FriendsStartup();
  void FriendsShutdown();
#else
  bool IsSignedIn() const {return false;}
  void Cloak(bool set = true) {}
  bool IsCloaked() const {return false;}
  
  void DisableVoice(bool disable) {}
#endif

  bool IsPlayerInMuteList(const XUID &xuid) const;
  void UpdateMuteList();
  void TogglePlayerMute(const XUID &xuid);
  
  void SetPendingInvitationPos(RString resource);

public:
  bool IsSystemLink() const {return _sessionEnum != NULL;}

  void GetSessions(AutoArray<SessionInfo> &sessions);
  RString IPToGUID(RString ip, int port);
  bool CreateSession(SessionType sessionType, RString name, RString password, int maxPlayers, bool isPrivate, int port, bool dedicated, RString config, int playerRating);

  void UpdateMaxPlayers(int maxPlayers, int privateSlots);
  int GetMaxPlayersLimit() const;
  int GetMaxPlayersSafe(int requiredPerClient, int round) const;

#if _XBOX_SECURE
  ConnectResult JoinSession(SessionType sessionType, const XNADDR &addr, const XNKID &kid, const XNKEY &key, int port, bool privateSlot, int playerRating);
  ConnectResult JoinSession(SessionType sessionType, RString guid, RString password)
  {
    Fail("Not supported");
    return CRError;
  }
#elif defined _XBOX
  ConnectResult JoinSession(SessionType sessionType, const XNADDR &addr, const XNKID &kid, const XNKEY &key, int port)
  {
    Fail("Not supported");
    return CRError;
  }
  ConnectResult JoinSession(SessionType sessionType, RString guid, RString password);
#else
  ConnectResult JoinSession(SessionType sessionType, RString guid, RString password);
#endif

  bool WaitForSession();
  void GetPlayers(AutoArray<NetPlayerInfo, MemAllocSA> &players);
  void CreateMission(RString mission, RString world);
  void SetOriginalName(RString name);
  void InitMission(int difficulty, RString displayName, bool createRoles, bool noCopy, RString owner, bool isCampaignMission);
  // set Mission to be loaded from save (detected in DisplayServer and processed in DisplayMultiplayerSetup)
  void SetCurrentSaveType(int saveType);
  void SetMissionLoadedFromSave(bool value);
  bool IsMissionLoadedFromSave() const;
  void KickAllPlayers();
  void SendGameLoadedFromSave();
  void LoadRolesFromSave();
  /// Reset MP roles after MP Mission Restart
  void ResetMPRoles();
  // get whether Mission should be loaded from save 
  int  GetCurrentSaveType();
  /// reset current saveType (save was deleted)
  void ResetCurrentSaveType(int saveType);
  /// player roles dead/alive state need to be kept synchronized
  void SetPlayerRoleLifeState(int roleIndex, LifeState lifeState);
  /// save/load the MP game (save of networking data)
  LSError SerializeMP(ParamArchive &ar);
  
  void ClearRoles();
  int AddRole(Person *person);
  void Close();
  unsigned CleanUpMemory();

  void CreateServerDebugWindow();
  void CreateClientDebugWindow();

#if _XBOX_SECURE && _ENABLE_MP
  const XNKID *GetSessionKey() const;
  const AutoArray<RecentPlayerInfo> *GetRecentPlayers() const;

  // Pending invitations are handled by Xbox Guide on Xbox 360
# if _XBOX_VER < 200
  bool IsPendingRequest() const {return _pendingRequest;}
  bool IsPendingInvitation() const {return _pendingInvitation;}
# endif
#endif

  void KickOff(int dpnid, KickOffReason reason);
  void Ban(int dpnid);
  void LockSession(bool lock = true);

  float GetRespawnRemainingTime() const;
  float GetVehicleRespawnRemainingTime() const;
  void SetPlayerRespawnTime(float timeInterval);

  void OnSimulate();
  void FastUpdate();
  void RegisterUpdateCallback(INetworkUpdateCallback *callback);
  void UnregisterUpdateCallback(INetworkUpdateCallback *callback);
  void OnDraw();

  NetworkServerState GetServerState() const;
  int GetServerTimeout() const;
  void SetServerState(NetworkServerState state);

  NetworkClientState GetClientState() const;
  void SetClientState(NetworkClientState state);

  bool GetTransferProgress(float &progress) const;

  void OnJoiningInProgress();

  int TimeElapsed() const;

  NetworkClientState GetClientState(int dpnid) const;

  bool IsGameMaster() const;
  bool IsAdmin() const;
  ConnectionQuality GetConnectionQuality() const;
  ConnectionDesync GetConnectionDesync() const;
  void GetParams(float &param1, float &param2) const;
  float GetParams(int index) const;
  AutoArray<float> GetParamsArray() const;
  void SetParams(float param1, float param2, bool updateOnly);
  void SetParams(float param1, float param2, AutoArray<float> paramsArray, bool updateOnly);
  void SetParam(float param, int index, bool updateOnly);
  const MissionHeader *GetMissionHeader() const;
  int GetPlayer() const;
  int GetPlayerId() const;

  int NPlayerRoles() const;
  const PlayerRole *GetPlayerRole(int role) const;
  const PlayerRole *GetMyPlayerRole() const;
  const PlayerRole *FindPlayerRole(int player) const;
  const PlayerIdentity *FindIdentity(int dpnid) const;
  const AutoArray<PlayerIdentity> *GetIdentities() const;
  void AssignPlayer(int role, int player, int flags);
  void SelectPlayer(int player, Person *person, bool respawn = false);
  void PlaySound
  (
    RString name, Vector3Par position, Vector3Par speed, 
    float volume, float freq, AbstractWave *wave
  );
  void SoundState(AbstractWave *wave, SoundStateType state);
  RString GetPlayerName(int dpid);
  Vector3 GetCameraPosition(int dpid);
  NetworkObject *GetObject(NetworkId &id);
  int GetOwner(const NetworkId &id) const;
  bool CreateVehicle(Vehicle *veh, VehicleListType type, RString name, int idVeh);
  bool CreateSubobject(Vehicle *veh, TurretPath &path, NetworkObject *object);
  bool CreateCenter(AICenter *center);  // whole AI structure
  bool CreateObject(NetworkObject *object, bool setNetworkId=true);
  void CreateAllObjects();
  void CreateAllObjectsFromLoad();
  void DeleteObject(NetworkId &id);
  void DestroyAllObjects();
  bool CreateCommand(AISubgroup *subgrp, Command *cmd, bool enqueued, bool setNetworkId=true);
  void DeleteCommand(AISubgroup *subgrp, Command *cmd);
  void AskForEnableVisionModes(Transport *vehicle, bool enable);
  void AskForForceGunLight(Person *person, bool force);
  void AskForIRLaser(Person *person, bool enable);
  void AskForDamage(
    Object *who, EntityAI *owner,
    Vector3Par modelPos, float val, float valRange, RString ammo, Vector3Par originDir
  );
  void AskForSetDamage
  (
    Object *who, float dammage
  );
  void AskForSetMaxHitZoneDamage
    (
    Object *who, float dammage
    );
  void AskForApplyDoDamage(Object *who, EntityAI *owner, const Object::DoDamageResult &result, RString ammo);
  void StaticObjectDestructed(Object *object, bool now, bool immediate);
  void AskForGetIn
  (
    Person *soldier, Transport *vehicle,
    GetInPosition position, Turret *turret,
    int cargoIndex
  );
  void AskForGetOut(
    Person *soldier, Transport *vehicle, Turret *turret, bool eject, bool parachute
  );
  void AskWaitForGetOut(
    Transport *vehicle, AIUnit *unit
    );
  void AskForChangePosition(
    Person *soldier, Transport *vehicle, UIActionType type, Turret *turret, int cargoIndex
  );
  void AskForSelectWeapon(
    EntityAIFull *vehicle, Turret *turret, int weapon
  );
  void AskForAmmo(
    EntityAIFull *vehicle, Turret *turret, int weapon, int burst
  );
  void AskForFireWeapon(EntityAIFull *vehicle, Turret *turret, int weapon, TargetType *target, bool forceLock);
  void AskForAirportSetSide(int index, TargetSide side);
  void AskForAddImpulse(
    Vehicle *vehicle, Vector3Par force, Vector3Par torque
  );
  void AskForMove(
    Object *vehicle, Vector3Par pos
  );
  void AskForMove(
    Object *vehicle, Matrix4Par trans
  );
  void AskForJoin(
    AIGroup *join, AIGroup *group, bool silent
  );
  void AskForJoin(
    AIGroup *join, OLinkPermNOArray(AIUnit) &units, bool silent, int id
  );
  void AskForChangeSide(
    Entity *vehicle, TargetSide side
    );
  void AskForHideBody(Person *vehicle);
  void ExplosionDamageEffects(
    EntityAI *owner, Object *directHit, HitInfoPar hitInfo, int componentIndex,
    bool enemyDamage, float energyFactor, float explosionFactor
  );
  void FireWeapon
  (
    EntityAIFull *vehicle, Person *gunner, int weapon, const Magazine *magazine, EntityAI *target,
    const RemoteFireWeaponInfo &remoteInfo
  );
  void AskRemoteControlled(Person *who, AIBrain *whom);
  void SetObjectTexture(EntityAI *veh, int index, RString name, RString eval = "true");
  void PublicExec(RString condition, RString command, Object* obj, OLinkArray(Object) &objs);
  void UpdateWeapons(EntityAIFull *vehicle, Turret *turret, Person *gunner);
  void AddWeaponCargo(EntityAI *vehicle, RString weapon);
  void RemoveWeaponCargo(EntityAI *vehicle, RString weapon);
  void ClearWeaponCargo(EntityAI *vehicle);
  void AddMagazineCargo(EntityAI *vehicle, const Magazine *magazine);
  void RemoveMagazineCargo(EntityAI *vehicle, RString type, int ammo);
  void ClearMagazineCargo(EntityAI *vehicle);
  void AddBackpackCargo(EntityAI *vehicle, EntityAI *backpack);
  void RemoveBackpackCargo(EntityAI *vehicle, EntityAI *backpack);
  void ClearBackpackCargo(EntityAI *vehicle);
  void VehicleInit(VehicleInitMessage &init);
  void OnVehicleDestroyed(EntityAI *killed, EntityAI *killer);
  void OnVehicleDamaged(EntityAI *damaged, EntityAI *killer, float damage, RString ammo);
  void OnVehMPEventHandlersChanged(EntityAI *ai, int event, const AutoArray<RString> &handlers);
  void OnIncomingMissile(EntityAI *target, Entity *shot, EntityAI *owner);
  void OnLaunchedCounterMeasures(Entity *cm, Entity *testedSystem, int count);
  void OnWeaponLocked(EntityAI *target, Person *gunner, bool locked);
  void MarkerCreate(int channel, AIBrain *sender, RefArray<NetworkObject> &units, ArcadeMarkerInfo &info);
  void MarkerDelete(RString name);
  void WaypointCreate(AIGroup *group, int index, WaypointInfo &wp);
  void WaypointUpdate(AIGroup *group, int index, WaypointInfo &wp);
  void WaypointDelete(AIGroup *group, int index);
  void WaypointsCopy(AIGroup *to, AIGroup *from);
  void HCClearGroups(AIUnit *unit);
  void HCRemoveGroup(AIUnit *unit,AIGroup *group);
  void HCSetGroup(AIUnit *unit,AIHCGroup hcGroup);
  void GroupSetUnconsciousLeader(AIUnit *unit,AIGroup *group);
  void GroupSelectLeader(AIUnit *unit,AIGroup *group);
  void DropBackpack(EntityAI *backpack, Matrix4Par trans);
  void TakeBackpack(Person *soldier, EntityAI *backpack);
  void Assemble(EntityAI *weapon, Matrix4Par transform);
  void DisAssemble(EntityAI *weapon, EntityAI *backpack);
  void SetFlagOwner
  (
    Person *owner, EntityAI *carrier
  );
  void SetFlagCarrier
  (
    Person *owner, EntityAI *carrier
  );
  void CancelTakeFlag(EntityAI *carrier);
  void SendMsg(NetworkSimpleObject *msg);

  void PublicVariable(RString name);
  void ClearDelayedPublicVariables();
  void ProcessDelayedPublicVariables();

  void TeamMemberSetVariable(AITeamMember *teamMember,RString name);
  void ObjectSetVariable(Object *obj, RString name);
  void GroupSetVariable(AIGroup *group, RString name);
  void SendMPEvent(MPEntityEvent event, const GameValue &pars);
  void Chat(int channel, RString text);
  void Chat(int channel, AIBrain *sender, RefArray<NetworkObject> &units, RString text);
  void Chat(int channel, RString sender, RefArray<NetworkObject> &units, RString text);

  void LocalizedChat(int channel, const LocalizedFormatedString &text);
  void LocalizedChat(int channel, AIBrain *sender, RefArray<NetworkObject> &units, const LocalizedFormatedString & text);
  void LocalizedChat(int channel, RString sender, RefArray<NetworkObject> &units, const LocalizedFormatedString & text);

  void RadioChat(int channel, AIBrain *sender, RefArray<NetworkObject> &units, RString text, RadioSentence &sentence);
  void RadioChatWave(int channel, RefArray<NetworkObject> &units, RString wave, AIBrain *sender, RString senderName);
  int GetVoiceChannel() const;
  void SetVoiceChannel(int channel);
  void SetVoNPosition(int dpnid, Vector3Par pos, Vector3Par speed);
  void SetVoNPlayerPosition(Vector3Par pos, Vector3Par speed);
  void SetVoNObstruction(int dpnid, float obstruction, float occlusion, float deltaT);
  void UpdateVoNVolumeAndAccomodation(int dpnid, float volume, float accomodation);
  virtual void AdvanceAllVoNSounds( float deltaT, bool paused, bool quiet );

  void TransferFile(RString dest, RString source);
  void SendMissionFile();
  void GetTransferStats(int &curBytes, int &totBytes);

  void ShowTarget(Person *vehicle, TargetType *target);
  void ShowGroupDir(Person *vehicle, Vector3Par dir);

  void GroupSynchronization(AIGroup *grp, int synchronization, bool active);
  void DetectorActivation(Detector *det, bool active);

#if _VBS3
  void EnablePersonalItems(Person *person, bool active, RString animAction="", Vector3 personalItemsOffset=VZero);
  void AARDoUpdate(int type ,int intType,RString stringOne = RString(), RString stringTwo = RString(), float floatType = 0.0);
  void AARAskUpdate(int type ,int intType,RString stringOne = RString(), RString stringTwo = RString(), float floatType = 0.0);
  void AddMPReport(int type, RString myString);
  void LoadIsland(RString islandName);
  void ApplyWeather();
  void AskCommanderOverride(Turret *turret, Turret *overriddenByTurret);
#endif

  void AskForCreateUnit(AIGroup *group, RString type, Vector3Par position, RString init, float skill, Rank rank);
  void AskForDeleteVehicle(Entity *veh);
  void AskForReceiveUnitAnswer
  (
    AIUnit *from, AISubgroup *to, int answer
  );
  void AskForOrderGetIn(AIBrain *unit, bool flag);
  void AskForAllowGetIn(AIBrain *unit, bool flag);
  void AskForGroupRespawn(Person *person, EntityAI *killer);
#if _VBS3 // respawn command
  void PauseSimulation(bool pause);
  void AskForRespawn(Person *person);
#endif

#if _ENABLE_ATTACHED_OBJECTS
  void AttachObject(Object *obj, Object *attachTo, int memIndex, Vector3 pos, int flags);
  void DetachObject(Object *obj);
#endif

  void GetSwitchableUnits(OLinkPermNOArray(AIBrain) &units, const AIBrain *player);
  void TeamSwitch(Person *from, Person *to, EntityAI *killer, bool respawn);
  void AskForActivateMine(Mine *mine, bool activate);
  void AskForInflameFire(Fireplace *fireplace, bool fire);
  void AskForAnimationPhase(Entity *vehicle, RString animation, float phase);

  void OfferWeapon(EntityAI *from, EntityAIFull *to, const WeaponType *weapon, int slots, bool useBackpack);
  void OfferMagazine(EntityAI *from, EntityAIFull *to, const MagazineType *type,bool useBackpack);
  void OfferBackpack(EntityAI *from, EntityAIFull *to, RString name);
  void ReplaceWeapon(EntityAI *from, EntityAIFull *to, const WeaponType *weapon, bool useBackpack);
  void ReplaceMagazine(EntityAI *from, EntityAIFull *to, Magazine *magazine, bool reload, bool useBackpack);
  void ReplaceBackpack(EntityAI *from, EntityAIFull *to, EntityAI *backpack);
  void ReturnWeapon(EntityAI *from, const WeaponType *weapon);
  void ReturnMagazine(EntityAI *from, Magazine *magazine);
  void ReturnBackpack(EntityAI *from, EntityAI *backpack);

  void PoolAskWeapon(AIBrain *unit, const WeaponType *weapon, int slot, bool useBackpack);
  void PoolAskMagazine(AIBrain *unit, const MagazineType *type, int slot, bool useBackpack);
  void PoolAskBackpack(AIBrain *unit, RString name);
  void PoolReturnWeapon(const WeaponType *weapon);
  void PoolReturnMagazine(Magazine *magazine);
  void PoolReturnBackpack(int creator, int id, RString typeName);

  RespawnMode GetRespawnMode() const;
  float GetRespawnDelay() const;
  float GetRespawnVehicleDelay() const;
  void Respawn(Person *soldier, Vector3Par pos);
  void Respawn(Transport *vehicle, Vector3Par pos);

#if _ENABLE_CONVERSATION
  void KBReact(AIBrain *from, AIBrain *to, const KBMessageInfo *message);
#endif

  bool ProcessCommand(RString command);
  bool IsCommandAvailable(RString command);

  void SendKick(int player);
  void SendLockSession(bool lock = true);

  bool CanSelectMission() const;
  bool CanVoteMission() const;
  const VotingMissionProgressMessage *GetMissionVotingState() const;
  const AutoArray<MPMissionInfo> &GetServerMissions() const;
  void SelectMission(RString mission, int difficulty);
  void VoteMission(RString mission, int difficulty);

  void UpdateObject(NetworkObject *object);

  //! Cancel _dp->EnumHost operation
  void StopEnumHosts();
  //! Start _dp->EnumHost operation
  bool StartEnumHosts();

  //! Return original name of local player (without decoration used when several players vith the same name are connected)
  RString GetLocalPlayerName() const;

  Time GetEstimatedEndTime() const;
  void SetEstimatedEndTime(Time time);

  void DisposeBody(Person *body);
  bool IsControlsPaused();
  float GetLastMsgAgeReliable();

  #ifdef _XBOX
  /// configure comm ports based on active controller and port
  void SelectCommPorts(int communicatorPort);
  #endif
  /// react to communicator insertion/removal
  void UpdateVoiceStatus();

  void ChangeVoiceMask(RString mask);

  void SetRoleIndex(AIUnit *unit, int roleIndex);

  void RevealTarget(int to, AIGroup *grp, EntityAI *target);

  void DebugAsk(RString str);

  void AskForStatsWrite(
    int player,
    int board, const AutoArray<XONLINE_STAT> &stats,
    int boardTotal, const AutoArray<XONLINE_STAT> &statsTotal
    );

  Time GetPlayerJoined(int dpid) const;

  void HackedDataDetected(RString filename);

  void AddPublicVariableEventHandler(RString name, GameValuePar code);
  bool BattlEyeOnScriptExec(const char *script);

  void RememberSerializedObject(NetworkObject *obj);
  void ClearSerializedObjects();
  AutoArray<NetworkObject*> *GetSerializedObjects();

#if _ENABLE_GAMESPY
  /// Set on which IP Address the client is visible from the matchmaking server
  virtual void SetPublicAddress(int addr);
#endif

#if _GAMES_FOR_WINDOWS || (defined _XBOX && _XBOX_VER >= 200)
  /// Access to the session info
  const XSESSION_INFO &GetSessionInfo() const {return _sessionInfo;}
  /// Access to the session nonce - the unique key used for arbitration
  ULONGLONG GetSessionNonce() const {return _sessionNonce;}

  /// Update the player slots of the session
  bool UpdateSession(int maxPlayers, int privateSlots);

  /// Register arbitration for clients (final registration on server is processed in StartSession)
  bool ArbitrationRegister(ULONGLONG sessionNonce);

  /// Register player to the session
  bool RegisterPlayer(const PlayerIdentity &identity);
  /// Unregister player from the session
  bool UnregisterPlayer(const PlayerIdentity &identity);

  /// start the match itself, statistics collection begins
  bool StartSession();
  /// finish the match itself, statistics will be reported
  bool EndSession();

protected:
  /// Register session on Xbox Live
  bool RegisterSession(bool host, int maxPlayers, int maxPrivateSlots);
  /// Unregister session from Xbox Live
  void UnregisterSession();

  /// Based on game type, select the leaderboard(s) and algorithm and update the statistics
  bool UpdateStatistics(RString gameType);

#endif
};

extern NetworkManager GNetworkManager;
extern NetworkMessageFormat *GMsgFormats[NMTN];

extern RString ServerTmpDir;
RString GetServerTmpDir();
RString GetClientTmpDir();
RString GetClientCustomFilesDir();

//! total number of diagnostic types
/*!
\patch_internal 1.08 Date 07/26/2001 by Jirka
- Improved: network diagnostics
*/
enum OutputDiagsMode
{
  ODMsgQueue,
  ODMsgSent,
  ODMsgReceived,
  ODMRawStats,
  NOutputDiags
};

extern int outputDiags;
extern bool outputLogs;

// transfer parameters
//! When update error is bellow this threshold, update message is not sent
extern float MinErrorToSend;
//! Maximal number of messages sent in single simulation step
extern int MaxMsgSend;
//! Maximal number of messages sent in single simulation step on dedicated server
extern int DSMaxMsgSend;
//! Maximal size of agregated guaranteed message
extern int MaxSizeGuaranteed;
//! Maximal size of agregated guaranteed message
extern int MaxSizeNonguaranteed;
//! Limit (minimum) for bandwidth estimation
extern int MinBandwidth;
//! Limit (minimum) for bandwidth estimation on dedicated server
extern int DSMinBandwidth;
//! Limit (maximum) for bandwidth estimation
extern int MaxBandwidth;

//! == default MinErrorToSend / 2, after 2 s update is forced
#define ERR_COEF_TIME_POSITION        0.005
//! == default MinErrorToSend / 5, after 5 s update is forced
#define ERR_COEF_TIME_GENERIC         0.002

#define MSGID_REPLACE 0

void WriteDiagOutput(bool server);
int GetDiagLevel(NetworkMessageType type, bool remote);

//((EXE_CRC_CHECK
#ifndef _XBOX
/// information for client code verification
struct ExeCRCBlock
{
  int offset, size, crc;
};
TypeIsSimple(ExeCRCBlock);
#endif
//))EXE_CRC_CHECK

#ifdef _WIN32
HRESULT WINAPI ReceiveMessage(void *context, DWORD type, void *message);
#endif
void InitMsgFormats();
//((EXE_CRC_CHECK
unsigned int IntegrityCheckAnswer
(
  IntegrityQuestionType type, const IntegrityQuestion &q, bool server
);
//))EXE_CRC_CHECK
const char *FormatVal(float val, char *buffer);

#if _ENABLE_UNSIGNED_MISSIONS
RString CreateMPMissionBank(RString filename, RString island);
#endif

int GetNetworkPort();
int GetServerPort();

extern WINDOW_HANDLE hwndApp;

bool DeleteDirectoryStructure(const char *name, bool deleteDir);
void CreatePath(RString path);
bool ParseMission(bool multiplayer, bool avoidCheckIds = false, bool allowEmpty = false);

//! Release COM object
#define SAFE_RELEASE(p)             { if(p) {(p)->Release(); (p) = NULL;} }

// Multiplayer version info

#include "../versionNo.h"

//! Actual multiplayer version
#define MP_VERSION_ACTUAL           APP_VERSION_NUM
//! The earliest supported multiplayer version
#if APP_VERSION_NUM >= 100 && APP_VERSION_NUM <= 101
  #define MP_VERSION_REQUIRED         100
#else
  #define MP_VERSION_REQUIRED         APP_VERSION_NUM
#endif

//! Integrity question message
/*!
This message is sent by server to clients to check consistency of their data (to avoid cheaters).
*/

#define INTEGRITY_QUESTION_MSG(MessageName, XX) \
  XX(MessageName, int, id, NDTInteger, int, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG("Unique id of question"), TRANSF) \
  XX(MessageName, int, type, NDTInteger, int, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG("Type of question"), TRANSF) \
  XX(MessageName, RString, name, NDTString, RString, NCTNone, DEFVALUE(RString, ""), DOC_MSG("Question name"), TRANSF) \
  XX(MessageName, int, offset, NDTInteger, int, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG("Region in question"), TRANSF) \
  XX(MessageName, int, size, NDTInteger, int, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG("Region in question"), TRANSF)

DECLARE_NET_MESSAGE(IntegrityQuestion, INTEGRITY_QUESTION_MSG)

#define INTEGRITY_ANSWER_MSG(MessageName, XX) \
  XX(MessageName, int, id, NDTInteger, int, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG("Unique id of question"), TRANSF) \
  XX(MessageName, int, type, NDTInteger, int, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG("Type of question"), TRANSF) \
  XX(MessageName, int, answer, NDTInteger, int, NCTNone, DEFVALUE(int, 0), DOC_MSG("Answer value (CRC of selected data)"), TRANSF)

DECLARE_NET_MESSAGE(IntegrityAnswer, INTEGRITY_ANSWER_MSG)

#define ATTACH_PERSON_MSG(MessageName, XX) \
  XX(MessageName, OLinkPermO(Person), person, NDTRef, NetworkId,  NCTNone, DEFVALUENULL, DOC_MSG("Person to attach"), TRANSF_REF) \
  XX(MessageName, OLinkPermNO(AIBrain), unit, NDTRef, NetworkId,  NCTNone, DEFVALUENULL, DOC_MSG("Unit to attach"), TRANSF_REF)

DECLARE_NET_MESSAGE(AttachPerson, ATTACH_PERSON_MSG)

#define SET_FLAG_OWNER_MSG(MessageName, XX) \
  XX(MessageName, OLinkPermO(Person), owner, NDTRef, NetworkId,  NCTNone, DEFVALUENULL, DOC_MSG("Flag owner"), TRANSF_REF) \
  XX(MessageName, OLinkPermO(EntityAI), carrier, NDTRef, NetworkId,  NCTNone, DEFVALUENULL, DOC_MSG("Flag carrier"), TRANSF_REF)

DECLARE_NET_MESSAGE(SetFlagOwner, SET_FLAG_OWNER_MSG)

#define SET_FLAG_CARRIER_MSG(MessageName, XX)

DECLARE_NET_INDICES_EX(SetFlagCarrier, SetFlagOwner, SET_FLAG_CARRIER_MSG)

//! Message for ask client owns flag owner for change of flag ownership
struct SetFlagCarrierMessage : public SetFlagOwnerMessage
{
  DECLARE_DEFINE_NETWORK_OBJECT_SIMPLE(SetFlagCarrier)
};

//! Types of Network command messages
enum NetworkCommandMessageType
{
  NCMTLogin,
  NCMTLogged,
  NCMTLogout,
  NCMTKick,
  /// used as voting id - restart current mission
  NCMTRestart,
  /// used as voting id for mission voting
  NCMTMission,
  /// used as voting id - return to mission selection
  NCMTMissions,
  NCMTShutdown,
  /// used as voting id - return to role selection
  NCMTReassign,
  NCMTMonitorAsk,
  NCMTMonitorAnswer,
  NCMTVote,
  NCMTVoteMission,
  NCMTMissionTimeElapsed,
  /// used as voting id for admin voting
  NCMTAdmin,
  NCMTInit,
  NCMTLockSession,
  NCMTLoggedOut,
  NCMTDebugAsk,
  NCMTDebugAnswer,
  /// used only as voting id for mission difficulty voting
  NCMTDifficulty,
  /// execute the server scripting command
  NCMTExec,
  NCMTExecResult,
  /// BattlEye server command
  NCMTBattlEye,
  /// Progress of custom files uploading from server to clients
  NCMTCustomFilesProgress,
#if _VBS3
  NCMTExitAll,
#endif
  // server asks clients for their VoNFeatures diagnostics, clients responds
  NCMTAskVoNDiagnostics,
  NCMTMissionDate,
  NCMTVoteMissionExt
};

#define NETWORK_COMMAND_MSG(MessageName, XX) \
  XX(MessageName, int, type, NDTInteger, int, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG("Type of command"), TRANSF) \
  XX(MessageName, SimpleStream, content, NDTRawData, AutoArray<char>, NCTNone, DEFVALUERAWDATA, DOC_MSG("Parameters of command"), TRANSF)

DECLARE_NET_MESSAGE(NetworkCommand, NETWORK_COMMAND_MSG)

#define PLAYER_UPDATE_MSG(MessageName, XX) \
  XX(MessageName, int, dpnid, NDTInteger, int, NCTNone, DEFVALUE(int, 0), DOC_MSG("Client (player) ID"), TRANSF, ET_NONE, 0) \
  XX(MessageName, int, minPing,NDTInteger,int, NCTSmallUnsigned, DEFVALUE(int, 10), DOC_MSG("Ping range estimation"), TRANSF, ET_ABS_DIF, ERR_COEF_VALUE_MINOR) \
  XX(MessageName, int, avgPing,NDTInteger,int, NCTSmallUnsigned, DEFVALUE(int, 100), DOC_MSG("Ping range estimation"), TRANSF, ET_ABS_DIF, ERR_COEF_VALUE_MAJOR) \
  XX(MessageName, int, maxPing,NDTInteger,int, NCTSmallUnsigned, DEFVALUE(int, 1000), DOC_MSG("Ping range estimation"), TRANSF, ET_ABS_DIF, ERR_COEF_VALUE_MINOR) \
  XX(MessageName, int, minBandwidth,NDTInteger, int, NCTSmallUnsigned, DEFVALUE(int,  2), DOC_MSG("Bandwidth estimation (in kbps)"), TRANSF, ET_ABS_DIF, ERR_COEF_VALUE_MINOR) \
  XX(MessageName, int, avgBandwidth,NDTInteger, int, NCTSmallUnsigned, DEFVALUE(int, 14), DOC_MSG("Bandwidth estimation (in kbps)"), TRANSF, ET_ABS_DIF, ERR_COEF_VALUE_MINOR) \
  XX(MessageName, int, maxBandwidth,NDTInteger, int, NCTSmallUnsigned, DEFVALUE(int, 28), DOC_MSG("Bandwidth estimation (in kbps)"), TRANSF, ET_ABS_DIF, ERR_COEF_VALUE_MINOR) \
  XX(MessageName, int, desync,NDTInteger, int, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG("Current desync level (max. error of unsent messages)"), TRANSF, ET_ABS_DIF, ERR_COEF_VALUE_MAJOR) \
  XX(MessageName, int, rights,NDTInteger, int, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG("Special rights of given player"), TRANSF, ET_ABS_DIF, ERR_COEF_VALUE_MAJOR)

DECLARE_NET_INDICES_ERR(PlayerUpdate, PLAYER_UPDATE_MSG)

inline bool NetworkClient::IsBotClient() const
{
  return _parent->IsServer();
}

#define ACCEPTED_KEY_MSG(MessageName, XX) \
  XX(MessageName, RString, name, NDTString, RString, NCTNone, DEFVALUE(RString, ""), DOC_MSG("Key name"), TRANSF) \
  XX(MessageName, Temp<char>, content, NDTRawData, AutoArray<char>, NCTNone, DEFVALUERAWDATA, DOC_MSG("Key content"), TRANSF) \

DECLARE_NET_INDICES(AcceptedKey, ACCEPTED_KEY_MSG)

//! Accepted public key for data signatures
struct AcceptedKeyMessage : public NetworkSimpleObject
{
  DSKey &_key;

  AcceptedKeyMessage(DSKey &key) : _key(key) {}

  DECLARE_DEFINE_NETWORK_OBJECT_SIMPLE(AcceptedKey)
  static NetworkMessageFormat &CreateFormat
  (
    NetworkMessageClass cls,
    NetworkMessageFormat &format
  );
  TMError TransferMsg(NetworkMessageContext &ctx);
};

#define ADDITIONAL_SIGNED_FILES_MSG(MessageName, XX) \
  XX(MessageName, AutoArray<RString>, list, NDTStringArray, AutoArray<RString>, NCTNone, DEFVALUESTRINGARRAY, DOC_MSG("List of additional files to check."), TRANSF) \

DECLARE_NET_INDICES(AdditionalSignedFiles, ADDITIONAL_SIGNED_FILES_MSG)

//! Accepted public key for data signatures
struct AdditionalSignedFilesMessage : public NetworkSimpleObject
{
  AutoArray<RString> &_list;

  AdditionalSignedFilesMessage(AutoArray<RString> &list) : _list(list) {}

  DECLARE_DEFINE_NETWORK_OBJECT_SIMPLE(AdditionalSignedFiles)
  static NetworkMessageFormat &CreateFormat
  (
    NetworkMessageClass cls,
    NetworkMessageFormat &format
  );
  TMError TransferMsg(NetworkMessageContext &ctx);
};

#define DATA_SIGNATURE_ASK_MSG(MessageName, XX) \
  XX(MessageName, int, index, NDTInteger, int, NCTSmallSigned, DEFVALUE(int, -1), DOC_MSG("Index of file to check."), TRANSF) \
  XX(MessageName, int, level, NDTInteger, int, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG("The level of the check (higher level means deeper check)."), TRANSF) \

DECLARE_NET_MESSAGE(DataSignatureAsk, DATA_SIGNATURE_ASK_MSG)

#define DATA_SIGNATURE_ANSWER_MSG(MessageName, XX) \
  XX(MessageName, int, index, NDTInteger, int, NCTSmallSigned, DEFVALUE(int, -1), DOC_MSG("Index of file to check."), TRANSF) \
  XX(MessageName, RString, filename, NDTString, RString, NCTNone, DEFVALUE(RString, ""), DOC_MSG("Name of checked file."), TRANSF) \
  XX(MessageName, RString, keyName, NDTString, RString, NCTNone, DEFVALUE(RString, ""), DOC_MSG("Name of public key"), TRANSF) \
  XX(MessageName, Temp<char>, keyContent, NDTRawData, AutoArray<char>, NCTNone, DEFVALUERAWDATA, DOC_MSG("Content of public key"), TRANSF) \
  XX(MessageName, Temp<char>, signature, NDTRawData, AutoArray<char>, NCTNone, DEFVALUERAWDATA, DOC_MSG("Content of signature"), TRANSF) \
  XX(MessageName, Temp<char>, hash, NDTRawData, AutoArray<char>, NCTNone, DEFVALUERAWDATA, DOC_MSG("Content of hash"), TRANSF) \
  XX(MessageName, int, version, NDTInteger, int, NCTSmallUnsigned, DEFVALUE(int, 1), DOC_MSG("Signature version."), TRANSF) \
  XX(MessageName, int, level, NDTInteger, int, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG("Ask level."), TRANSF) \

DECLARE_NET_INDICES(DataSignatureAnswer, DATA_SIGNATURE_ANSWER_MSG)

//! Answer to data signature check
struct DataSignatureAnswerMessage : public NetworkSimpleObject
{
  int _index;
  RString _filename;
  DSSignature _signature;
  DSHash _hash;
  int _level;

  DECLARE_DEFINE_NETWORK_OBJECT_SIMPLE(DataSignatureAnswer)
  static NetworkMessageFormat &CreateFormat
  (
    NetworkMessageClass cls,
    NetworkMessageFormat &format
  );
  TMError TransferMsg(NetworkMessageContext &ctx);
};

#define FILE_SIGNATURE_ASK_MSG(MessageName, XX) \
  XX(MessageName, RString, file, NDTString, RString, NCTNone, DEFVALUE(RString, ""), DOC_MSG("File to check the signature."), TRANSF) \
  XX(MessageName, int, level, NDTInteger, int, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG("The level of the check (higher level means deeper check)."), TRANSF) \

DECLARE_NET_MESSAGE(FileSignatureAsk, FILE_SIGNATURE_ASK_MSG)

#define FILE_SIGNATURE_ANSWER_MSG(MessageName, XX) \
  XX(MessageName, RString, fileName, NDTString, RString, NCTNone, DEFVALUE(RString, ""), DOC_MSG("Checked filename."), TRANSF) \
  XX(MessageName, RString, keyName, NDTString, RString, NCTNone, DEFVALUE(RString, ""), DOC_MSG("Name of public key"), TRANSF) \
  XX(MessageName, Temp<char>, keyContent, NDTRawData, AutoArray<char>, NCTNone, DEFVALUERAWDATA, DOC_MSG("Content of public key"), TRANSF) \
  XX(MessageName, Temp<char>, signature, NDTRawData, AutoArray<char>, NCTNone, DEFVALUERAWDATA, DOC_MSG("Content of signature"), TRANSF) \
  XX(MessageName, Temp<char>, dataToHash, NDTRawData, AutoArray<char>, NCTNone, DEFVALUERAWDATA, DOC_MSG("Content which hash was signed"), TRANSF) \
  XX(MessageName, int, version, NDTInteger, int, NCTSmallUnsigned, DEFVALUE(int, 1), DOC_MSG("Signature version."), TRANSF) \
  XX(MessageName, int, level, NDTInteger, int, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG("Ask level."), TRANSF) \
  XX(MessageName, int, index, NDTInteger, int, NCTSmallSigned, DEFVALUE(int, -1), DOC_MSG("Index of pbo file matching the DataSignatureAsk"), TRANSF) \


DECLARE_NET_INDICES(FileSignatureAnswer, FILE_SIGNATURE_ANSWER_MSG)

//! Answer to file signature check
struct FileSignatureAnswerMessage : public NetworkSimpleObject
{
  RString _fileName;
  DSSignature _signature;
  Temp<char> _dataToHash;    // for sig.ver.2: (hash of pbo content) + (hash of pbo file list) + (pbo prefix)
  int _level;
  int _index;

  DECLARE_DEFINE_NETWORK_OBJECT_SIMPLE(FileSignatureAnswer)
  static NetworkMessageFormat &CreateFormat
    (
    NetworkMessageClass cls,
    NetworkMessageFormat &format
    );
  TMError TransferMsg(NetworkMessageContext &ctx);
  RString GetPrefix() const; // parse the dataToHash to find the stored pbo prefix
  void GetDataHash(Temp<char> &hash) const;
};

#define HACKED_DATA_MSG(MessageName, XX) \
  XX(MessageName, int, type, NDTInteger, int, NCTSmallUnsigned, DEFVALUE(int, HTUnsignedData), DOC_MSG("Type of violation."), TRANSF) \
  XX(MessageName, RString, filename, NDTString, RString, NCTNone, DEFVALUE(RString, ""), DOC_MSG("Name of hacked file."), TRANSF) \

DECLARE_NET_MESSAGE(HackedData, HACKED_DATA_MSG)

#define STATIC_OBJECT_DESTRUCTED_MSG(MessageName, XX) \
  XX(MessageName, OLink(Object), object, NDTRef, NetworkId,  NCTNone, DEFVALUENULL, DOC_MSG("Destructed object"), TRANSF_REF) \
  XX(MessageName, bool, now, NDTBool, bool,  NCTNone, DEFVALUE(bool, false), DOC_MSG("Broadcast the message immediatelly"), TRANSF) \
  XX(MessageName, bool, immediate, NDTBool, bool,  NCTNone, DEFVALUE(bool, true), DOC_MSG("Perform the destruction immediatelly"), TRANSF) \

DECLARE_NET_MESSAGE_INIT_MSG(StaticObjectDestructed, STATIC_OBJECT_DESTRUCTED_MSG)

#define CDKEY_CHECK_ASK_MSG(MessageName, XX) \
  XX(MessageName, RString, challenge, NDTString, RString, NCTNone, DEFVALUE(RString, ""), DOC_MSG("Chalenge string (see GameSpy SDK Help)."), TRANSF) \

DECLARE_NET_MESSAGE(CDKeyCheckAsk, CDKEY_CHECK_ASK_MSG)

#define CDKEY_CHECK_ANSWER_MSG(MessageName, XX) \
  XX(MessageName, RString, response, NDTString, RString, NCTNone, DEFVALUE(RString, ""), DOC_MSG("Response string (see GameSpy SDK Help)."), TRANSF) \

DECLARE_NET_MESSAGE(CDKeyCheckAnswer, CDKEY_CHECK_ANSWER_MSG)

#define CDKEY_RECHECK_ASK_MSG(MessageName, XX) \
  XX(MessageName, int, hint, NDTInteger, int, NCTNone, DEFVALUE(int, -1), DOC_MSG("Session key (see GameSpy SDK Help)."), TRANSF) \
  XX(MessageName, RString, challenge, NDTString, RString, NCTNone, DEFVALUE(RString, ""), DOC_MSG("Challenge string (see GameSpy SDK Help)."), TRANSF) \

DECLARE_NET_MESSAGE(CDKeyRecheckAsk, CDKEY_RECHECK_ASK_MSG)

#define CDKEY_RECHECK_ANSWER_MSG(MessageName, XX) \
  XX(MessageName, int, hint, NDTInteger, int, NCTNone, DEFVALUE(int, -1), DOC_MSG("Session key (see GameSpy SDK Help)."), TRANSF) \
  XX(MessageName, RString, response, NDTString, RString, NCTNone, DEFVALUE(RString, ""), DOC_MSG("Response string (see GameSpy SDK Help)."), TRANSF) \

DECLARE_NET_MESSAGE(CDKeyRecheckAnswer, CDKEY_RECHECK_ANSWER_MSG)

#define ASK_CONNECT_VOICE_MSG(MessageName, XX) \
  XX(MessageName, int, publicAddr, NDTInteger, int, NCTNone, DEFVALUE(int, 0), DOC_MSG("Public IP address of the client."), TRANSF) \
  XX(MessageName, int, privateAddr, NDTInteger, int, NCTNone, DEFVALUE(int, 0), DOC_MSG("Private IP address of the client."), TRANSF) \
  XX(MessageName, int, voicePort, NDTInteger, int, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG("Port voice channel is bind to."), TRANSF) \
  XX(MessageName, bool, serverAcc, NDTBool, bool, NCTNone, DEFVALUE(bool, false), DOC_MSG("There is direct connection to server."), TRANSF)\
  XX(MessageName, int, verNo, NDTInteger, int, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG("The build number of client exe file."), TRANSF) \

DECLARE_NET_MESSAGE(AskConnectVoice, ASK_CONNECT_VOICE_MSG)

#define CONNECT_VOICE_DIRECT_MSG(MessageName, XX) \
  XX(MessageName, int, dpnid, NDTInteger, int, NCTNone, DEFVALUE(int, 0), DOC_MSG("ID of player to connect with"), TRANSF) \
  XX(MessageName, int, addr, NDTInteger, int, NCTNone, DEFVALUE(int, 0), DOC_MSG("IP address where connect to."), TRANSF) \
  XX(MessageName, int, port, NDTInteger, int, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG("Port where connect to."), TRANSF) \

DECLARE_NET_MESSAGE(ConnectVoiceDirect, CONNECT_VOICE_DIRECT_MSG)

#define CONNECT_VOICE_NAT_NEG_MSG(MessageName, XX) \
  XX(MessageName, int, dpnid, NDTInteger, int, NCTNone, DEFVALUE(int, 0), DOC_MSG("ID of player to connect with"), TRANSF) \
  XX(MessageName, int, cookie, NDTInteger, int, NCTNone, DEFVALUE(int, 0), DOC_MSG("Cookie for Nat negotiation."), TRANSF) \
  XX(MessageName, int, clientIndex, NDTInteger, int, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG("Client index for Nat negotiation."), TRANSF) \

DECLARE_NET_MESSAGE(ConnectVoiceNatNeg, CONNECT_VOICE_NAT_NEG_MSG)

#define NAT_NEG_RESULT_MSG(MessageName, XX) \
  XX(MessageName, int, dpnid, NDTInteger, int, NCTNone, DEFVALUE(int, 0), DOC_MSG("ID of NAT negotiating player"), TRANSF) \
  XX(MessageName, bool, result, NDTBool, bool, NCTNone, DEFVALUE(bool, false), DOC_MSG("Result of NAT negotiation (true for success)."), TRANSF) \

DECLARE_NET_MESSAGE(NatNegResult, NAT_NEG_RESULT_MSG)

#define DISCONNECT_MSG(MessageName, XX) \
  XX(MessageName, RString, message, NDTString, RString, NCTNone, DEFVALUE(RString, ""), DOC_MSG("Cause why the player was disconnected."), TRANSF) \

DECLARE_NET_MESSAGE(Disconnect, DISCONNECT_MSG)

#define MISSION_STATS_MSG(MessageName, XX) \
  XX(MessageName, int, toCreate, NDTInteger, int, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG("Number of objects we will create on the client."), TRANSF) \
  XX(MessageName, int, toUpdate, NDTInteger, int, NCTSmallUnsigned, DEFVALUE(int, 0), DOC_MSG("Number of objects we will update on the client."), TRANSF) \

DECLARE_NET_MESSAGE(MissionStats, MISSION_STATS_MSG)

#define ASK_FOR_ARBITRATION_MSG(MessageName, XX) \
  XX(MessageName, NetInt64, sessionNonce, NDTInt64, NetInt64, NCTNone, DEFVALUE(NetInt64, 0), DOC_MSG("Key used for arbitration."), TRANSF) \

DECLARE_NET_MESSAGE(AskForArbitration, ASK_FOR_ARBITRATION_MSG)

#define BATTLEYE_MSG(MessageName, XX) \
  XX(MessageName, AutoArray<char>, message, NDTRawData, AutoArray<char>, NCTNone, DEFVALUERAWDATA, DOC_MSG("Message content"), TRANSF) \

DECLARE_NET_MESSAGE(BattlEye_Obsolete, BATTLEYE_MSG)

#define SERVERTIME_MSG(MessageName, XX) \
  XX(MessageName, int, time, NDTInteger, int, NCTNone, DEFVALUE(int, 0), DOC_MSG("Server time"), TRANSF) \
  XX(MessageName, int, endTime, NDTInteger, int, NCTNone, DEFVALUE(int, 0), DOC_MSG("Server time of estimated end of MP game."), TRANSF) \

DECLARE_NET_MESSAGE(ServerTime, SERVERTIME_MSG)

#define SERVER_ADMIN_MESSAGE(MessageName, XX) \
  XX(MessageName, bool, admin, NDTBool, bool, NCTNone, DEFVALUE(bool, false), DOC_MSG("GameMaster on server is voted admin."), TRANSF) \
  XX(MessageName, int, gameMaster, NDTInteger, int, NCTNone, DEFVALUE(int, 0), DOC_MSG("Id of gameMaster."), TRANSF) \

DECLARE_NET_MESSAGE(ServerAdmin, SERVER_ADMIN_MESSAGE)

#define ASK_FOR_TEAM_SWITCH_MSG(MessageName, XX) \
  XX(MessageName, int, player, NDTInteger, int, NCTNone, DEFVALUE(int, 0), DOC_MSG("ID of the switching player"), TRANSF) \
  XX(MessageName, OLinkPermO(Person), from, NDTRef, NetworkId,  NCTNone, DEFVALUENULL, DOC_MSG("Person switching from"), TRANSF_REF) \
  XX(MessageName, OLinkPermO(Person), to, NDTRef, NetworkId,  NCTNone, DEFVALUENULL, DOC_MSG("Person switching to"), TRANSF_REF) \
  XX(MessageName, OLinkPermO(EntityAI), killer, NDTRef, NetworkId,  NCTNone, DEFVALUENULL, DOC_MSG("Who killed the person"), TRANSF_REF) \
  XX(MessageName, bool, respawn, NDTBool, bool, NCTNone, DEFVALUE(bool, false), DOC_MSG("Switching because of respawn."), TRANSF)

DECLARE_NET_MESSAGE(AskForTeamSwitch, ASK_FOR_TEAM_SWITCH_MSG)

#define TEAM_SWITCH_RESULT_MSG(MessageName, XX) \
  XX(MessageName, int, player, NDTInteger, int, NCTNone, DEFVALUE(int, 0), DOC_MSG("ID of the switching player"), TRANSF) \
  XX(MessageName, OLinkPermO(Person), from, NDTRef, NetworkId,  NCTNone, DEFVALUENULL, DOC_MSG("Person switched from"), TRANSF_REF) \
  XX(MessageName, OLinkPermO(Person), to, NDTRef, NetworkId,  NCTNone, DEFVALUENULL, DOC_MSG("Person switched to"), TRANSF_REF) \
  XX(MessageName, OLinkPermO(EntityAI), killer, NDTRef, NetworkId,  NCTNone, DEFVALUENULL, DOC_MSG("Who killed the person"), TRANSF_REF) \
  XX(MessageName, bool, respawn, NDTBool, bool, NCTNone, DEFVALUE(bool, false), DOC_MSG("Switching because of respawn."), TRANSF) \
  XX(MessageName, bool, result, NDTBool, bool, NCTNone, DEFVALUE(bool, true), DOC_MSG("Switched successfully."), TRANSF)

DECLARE_NET_MESSAGE(TeamSwitchResult, TEAM_SWITCH_RESULT_MSG)

#define FINISH_TEAM_SWITCH_MSG(MessageName, XX) \
  XX(MessageName, int, player, NDTInteger, int, NCTNone, DEFVALUE(int, 0), DOC_MSG("ID of the switching player"), TRANSF) \
  XX(MessageName, OLinkPermO(Person), from, NDTRef, NetworkId,  NCTNone, DEFVALUENULL, DOC_MSG("Person switched from"), TRANSF_REF) \
  XX(MessageName, OLinkPermO(Person), to, NDTRef, NetworkId,  NCTNone, DEFVALUENULL, DOC_MSG("Person switched to"), TRANSF_REF) \
  XX(MessageName, OLinkPermO(EntityAI), killer, NDTRef, NetworkId,  NCTNone, DEFVALUENULL, DOC_MSG("Who killed the person"), TRANSF_REF) \
  XX(MessageName, bool, respawn, NDTBool, bool, NCTNone, DEFVALUE(bool, false), DOC_MSG("Switching because of respawn."), TRANSF) \
  XX(MessageName, OLinkPermNO(AIGroup), fromGroup, NDTRef, NetworkId, NCTNone, DEFVALUENULL, DOC_MSG("Group of person switched from"), TRANSF_REF) \
  XX(MessageName, OLinkPermNO(AIBrain), fromLeader, NDTRef, NetworkId,  NCTNone, DEFVALUENULL, DOC_MSG("Group leader of person switched from"), TRANSF_REF) \

DECLARE_NET_MESSAGE(FinishTeamSwitch, FINISH_TEAM_SWITCH_MSG)

#define KB_REACT_MSG(MessageName, XX) \
  XX(MessageName, OLinkPermNO(AIBrain), from, NDTRef, NetworkId,  NCTNone, DEFVALUENULL, DOC_MSG("Asking unit"), TRANSF_REF) \
  XX(MessageName, OLinkPermNO(AIBrain), to, NDTRef, NetworkId,  NCTNone, DEFVALUENULL, DOC_MSG("Asked unit"), TRANSF_REF) \
  XX(MessageName, RString, topic, NDTString, RString, NCTNone, DEFVALUE(RString, ""), DOC_MSG("Conversation topic ID."), TRANSF) \
  XX(MessageName, RString, message, NDTString, RString, NCTNone, DEFVALUE(RString, ""), DOC_MSG("Conversation message ID."), TRANSF) \
  XX(MessageName, AutoArray<PublicVariableMessage>, arguments, NDTObjectArray, REF_MSG_ARRAY(NetworkMessagePublicVariable), NCTNone, DEFVALUE_MSG(NMTPublicVariable), DOC_MSG("List of arguments"), TRANSF_ARRAY)

DECLARE_NET_MESSAGE(KBReact, KB_REACT_MSG)

/// Custom file message to check whether the transfer to client is neccessary
#define CUSTOM_FILE_PATHS_MSG(MessageName, XX) \
  XX(MessageName, RString, dst, NDTString, RString, NCTNone, DEFVALUE(RString, ""), DOC_MSG("Custom file path"), TRANSF) \
  XX(MessageName, int, crc, NDTInteger, int, NCTNone, DEFVALUE(int, 0), DOC_MSG("CRC of custom file"), TRANSF) \
  XX(MessageName, int, len, NDTInteger, int, NCTNone, DEFVALUE(int, 0), DOC_MSG("File length of custom file"), TRANSF)

DECLARE_NET_INDICES(CustomFile, CUSTOM_FILE_PATHS_MSG)

// NetworkMessageCustomFileList to transfer array of CustomFilePaths
#define CUSTOM_FILE_LIST_MSG(MessageName, XX) \
  XX(MessageName, int, player, NDTInteger, int, NCTNone, DEFVALUE(int, 0), DOC_MSG("Direct play id"), TRANSF) \
  XX(MessageName, RString, name, NDTString, RString, NCTNone, DEFVALUE(RString, ""), DOC_MSG("Player name"), TRANSF) \
  XX(MessageName, AutoArray<CustomFilePaths>, fileList, NDTObjectArray, REF_MSG_ARRAY(NetworkMessageCustomFile), NCTNone, DEFVALUE_MSG(NMTCustomFile), DOC_MSG("List of custom files"), TRANSF_ARRAY)

DECLARE_NET_MESSAGE(CustomFileList, CUSTOM_FILE_LIST_MSG)

#define CUSTOM_FILES_WANTED_MSG(MessageName, XX) \
  XX(MessageName, int, player, NDTInteger, int, NCTNone, DEFVALUE(int, 0), DOC_MSG("Direct play id"), TRANSF) \
  XX(MessageName, AutoArray<int>, filesWanted, NDTIntArray, AutoArray<int>, NCTSmallUnsigned, DEFVALUEINTARRAY, DOC_MSG("Array of indexes to custom file list which client wants to get"), TRANSF) \

DECLARE_NET_MESSAGE(CustomFilesWanted, CUSTOM_FILES_WANTED_MSG)

#define DELETE_CUSTOM_FILES_MSG(MessageName, XX) \
  XX(MessageName, AutoArray<RString>, list, NDTStringArray, AutoArray<RString>, NCTNone, DEFVALUESTRINGARRAY, DOC_MSG("Array of indexes to custom file list which client wants to get"), TRANSF) \

DECLARE_NET_MESSAGE(DeleteCustomFiles, DELETE_CUSTOM_FILES_MSG)

#define VEH_MP_EVENT_HANDLERS_MSG(MessageName, XX) \
  XX(MessageName, OLink(EntityAI), vehicle, NDTRef, NetworkId, NCTNone, DEFVALUENULL, DOC_MSG("Affected entity"), TRANSF_REF) \
  XX(MessageName, int, eventNum, NDTInteger, int, NCTNone, DEFVALUE(int, 0), DOC_MSG("EventHandler to be added"), TRANSF) \
  XX(MessageName, AutoArray<RString>, handlers, NDTStringArray, AutoArray<RString>, NCTNone, DEFVALUESTRINGARRAY, DOC_MSG("Array of MP Event Handlers."), TRANSF)

DECLARE_NET_MESSAGE(VehMPEventHandlers, VEH_MP_EVENT_HANDLERS_MSG)

#define VEH_MP_EVENT_MSG(MessageName, XX) \
  XX(MessageName, int, eventNum, NDTInteger, int, NCTNone, DEFVALUE(int, 0), DOC_MSG("EventHandler to be added"), TRANSF) \
  XX(MessageName, AutoArray<char>, pars, NDTRawData, AutoArray<char>, NCTNone, DEFVALUERAWDATA, DOC_MSG("Array of parameters related to given event."), TRANSF)

DECLARE_NET_MESSAGE(VehMPEvent, VEH_MP_EVENT_MSG)

#endif
