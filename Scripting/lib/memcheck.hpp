#ifndef _MEMCHECK_HPP
#define _MEMCHECK_HPP

// memory allocation tracking - find un-freed blocks
#include <Es/Containers/listBidir.hpp>

#include <El/Debugging/imexhnd.h>

#include <Es/Memory/normalNew.hpp>

/// size of the allocation prefix. Must be at least able to to hold SPARE_DWORD dwords
#define PREFIX_SIZE 16

/// space memory needed to hold pointer to memory info + magic
#define SPARE_DWORD 2

#if _DEBUG
  /// allocation postfix - memory guarding block boundaries
  #define GUARD_SIZE 16
#else
  #define GUARD_SIZE 0
#endif

#if !DO_MEM_STATS || !defined _XBOX
  #define USE_MEM_FILENAME 1
#else
  #define USE_MEM_FILENAME 1
#endif

#if defined _XBOX && _ENABLE_REPORT
  #include <XbDm.h>
#endif

#if DO_MEM_STATS
  #ifdef _XBOX
    #define USE_MEM_COUNT 0
  #else
    #define USE_MEM_COUNT 1
  #endif
  #define USE_MEM_CALLSTACK 1
  #define USE_MEM_CALLSTACK_CS 0 // report based on callstack
  #define USE_MEM_CALLSTACK_CP 1 // report based on call place pairs
  #define USE_MEM_CALLSTACK_CL 1 // report based on call place locations
#else
  #define USE_MEM_COUNT 0
  #define USE_MEM_CALLSTACK 0
  #define USE_MEM_CALLSTACK_CS 0 // report based on callstack
  #define USE_MEM_CALLSTACK_CP 0 // report based on call place
  #define USE_MEM_CALLSTACK_CL 0 // report based on call place locations
#endif

#if USE_MEM_CALLSTACK

#include <El/Debugging/mapFile.hpp>
extern MapFile GMapFile;

void InitMemoryCheck()
{
  // if map-file is already parsed, reuse it
  if (!GMapFile.Empty()) return;
	GMapFile.ParseMapFile();
	// fix offset
	int initMemoryCheckMap = GMapFile.PhysicalAddress("?InitMemoryCheck@@YAXXZ");

	int initMemoryCheckReal = (int)InitMemoryCheck;
	
	int offset = initMemoryCheckReal-initMemoryCheckMap;
	if (offset)
	{
	  LogF("Applying offset %x to mapfile",offset);
	  GMapFile.OffsetPhysicalAddress(offset);
	}
}

#else

void InitMemoryCheck()
{
}

#endif

class MemoryInfo: public TLinkBidirD
{
  public:
	#ifdef _XBOX
	  // TODO: some string pooling would be handy here
  	enum {FileBufferSize=60};
	#else
  	enum {FileBufferSize=108};
	#endif
	
	private:
	// record all memory blocks
	void *_mem;
	int _size;
	//const char *_file;
	#if USE_MEM_FILENAME
		char _file[FileBufferSize];
		int _line;
	#endif

	#if USE_MEM_CALLSTACK
		void *_callstack[26];
		int _calls;
	#endif

	public:
	MemoryInfo( void *m, int s )
	{
		_mem=m,_size=s;

		#if USE_MEM_FILENAME
			_file[0]=0,_line=0;
		#endif
		#if USE_MEM_CALLSTACK
			_calls = sizeof(_callstack)/sizeof(*_callstack);
			GDebugExceptionTrap.ExtractCallstack(_callstack,_calls,true,&GMapFile);
			//GDebugExceptionTrap.ExtractCallstack(_callstack,_calls,true);
		#endif
	}
	MemoryInfo( void *m, int s, const char *file, int line)
	{
		_mem=m,_size=s;
		#if USE_MEM_FILENAME
			strncpy(_file,file,sizeof(_file));
			_file[sizeof(_file)-1]=0;
			_line=line;
		#endif
		#if USE_MEM_CALLSTACK
			_calls = sizeof(_callstack)/sizeof(*_callstack);
			GDebugExceptionTrap.ExtractCallstack(_callstack,_calls,true,&GMapFile);
			//GDebugExceptionTrap.ExtractCallstack(_callstack,_calls,true);
		#endif
	}

	void Report() const;

	bool Valid() const {return _mem!=NULL && _size>=0;}
	void Invalidate() {_mem=NULL,_size=-1;}

	void *Addr() const {return _mem;}
	int Size() const {return _size;}

	#if USE_MEM_FILENAME
		const char *File() const {return _file;}
		int Line() const {return _line;}
	#endif

	#if USE_MEM_CALLSTACK
		void * const *Callstack() const {return _callstack;}
		int CallstackSize() const {return _calls;}
  #endif
	// override memory allocation
	// this array must not use the resources it monitors
  #ifdef _XBOX
	void *operator new ( size_t size ) {return DmAllocatePool(size);}
	void operator delete ( void *mem ) {DmFreePool(mem);}
  #else
	void *operator new ( size_t size ) {return malloc(size);}
	void operator delete ( void *mem ) {free(mem);}
  #endif
};

class MemList: public TListBidir<MemoryInfo>
{
	public:
	void *operator new ( size_t size ) {return malloc(size);}
	void operator delete ( void *mem ) {free(mem);}
};

// we are in hpp, however this hpp is included only in one cpp

static MemList *PAllocated=new MemList;
static CriticalSection PAllocatedLock;

void MemoryInfo::Report() const
{
	const int dstSize = 64;
	int srcSize = 128;
	if (srcSize>_size) srcSize = _size;
	char text[dstSize + 1];
	int j = 0;
	for (int i=0; i<srcSize; i++)
	{
		char c = ((char *)_mem)[i];
		if (c >= 32)
		{
			text[j++] = c;
			if (j >= dstSize) break;
		}
	}
	text[j] = 0;
	#if USE_MEM_FILENAME
		if( _file && *_file )
		{
			if( strchr(_file,'.') )
			{
				LogF("%s(%d): Memory %8p:%6d '%s'",_file,_line,_mem,_size,text);
			}
			else
			{
				// _file is probably class name
				LogF("Memory %8p:%6d: '%s':%d",_mem,_size,_file,_line);
			}
		}
		else
		{
			LogF("Memory %8p:%6d '%s'",_mem,_size,text);
		}
	#else
		LogF("Memory %8p:%6d '%s'",_mem,_size,text);
	#endif
	#if USE_MEM_CALLSTACK
		// report callstack
		for (int i=0; i<_calls; i++)
		{
			int eip = (int)_callstack[i];
			int lower;
			const char *name = GMapFile.MapNameFromPhysical(eip,&lower);
			LogF("  %8x: %8x + %s",eip,eip-lower,name);
			//LogF("  %8x",eip);
		}
	#endif

}

inline size_t GetRealSize(void *mem)
{
	int *block=(int *)mem-PREFIX_SIZE/sizeof(int);
	int magic=block[1];
	Assert( magic==15879634 );
	if( magic!=15879634 ) return 0;
	MemoryInfo *info=(MemoryInfo *)block[0];
	Assert( info && info->Valid() );
  return info->Size();
}

inline const MemoryInfo *GetMemoryInfo(void *mem)
{
	int *block=(int *)mem-PREFIX_SIZE/sizeof(int);
	int magic=block[1];
	Assert( magic==15879634 );
	if( magic!=15879634 ) return NULL;
	MemoryInfo *info=(MemoryInfo *)block[0];
	if (info && info->Valid())
	{
	  return info;
	}
	return NULL;
}


inline void *PrepareFree( void *mem, bool fill=true )
{
	int *block=(int *)mem-PREFIX_SIZE/sizeof(int);
	int magic=block[1];
	Assert( magic==15879634 );
	if( magic!=15879634 )
	{
	  Fail("Memory block magic not found");
	  return block;
	}
	MemoryInfo *info=(MemoryInfo *)block[0];
	if (!info || !info->Valid())
	{
	  Fail("Memory block info not found");
	  return block;
	
	}
	
			int fullSize=info->Size()+GUARD_SIZE+PREFIX_SIZE;
			char *guard=(char *)block+fullSize-GUARD_SIZE;
			for( int i=0; i<GUARD_SIZE; i++ )
			{
				if( guard[i]!=MEM_GUARD_VAL )
				{
					ErrF("GUARD (after %x) Memory changed.",mem);
					info->Report();
				}
			}
			guard = (char *)(block+2);
			for( int i=0; i<PREFIX_SIZE-SPARE_DWORD*sizeof(int); i++ )
			{
				if( guard[i]!=MEM_GUARD_VAL )
				{
					ErrF("GUARD (before %x) Memory changed.",mem);
					info->Report();
				}
			}

      {
        ScopeLockSection lock(PAllocatedLock);
			  // fill invalid memory with MEM_FREE_VAL
  			info->Delete();

      }
			if (fill)
			{
			  MemSet32(block,MEM_FREE_VAL_32,fullSize);
			}
			info->Invalidate();
			delete info;
	return block;
}

#define PREPARE_ALLOC_SLACK ( PREFIX_SIZE+GUARD_SIZE )

inline int PrepareAlloc( int size )
{
	return size+PREPARE_ALLOC_SLACK; // record memory info pointer
}


inline void *FinishAlloc( void *ret, int size, const char *file, int line, bool fill=true )
{
	if( ret )
	{
		int noGuardSize=size-GUARD_SIZE;
		int origSize=noGuardSize-PREFIX_SIZE;
		int *block=(int *)ret;
		MemoryInfo *info=new MemoryInfo(block+PREFIX_SIZE/sizeof(int),origSize,file,line);
		if (info)
		{
      ScopeLockSection lock(PAllocatedLock);
			PAllocated->Insert(info);
		}
		block[0]=(int)info;
		block[1]=15879634;
    memset(block+SPARE_DWORD,MEM_GUARD_VAL,PREFIX_SIZE-sizeof(int)*SPARE_DWORD);

		void *guard=(char *)ret+noGuardSize;
		ret=block+PREFIX_SIZE/sizeof(int);
		if (fill)
		{
		  MemSet32(ret,MEM_NEW_VAL_32,origSize);
		}
		memset(guard,MEM_GUARD_VAL,GUARD_SIZE);
	}
	return ret;
}

/// memory leak reporting
inline void ReportAllocated()
{
	// report all allocated blocks
  ScopeLockSection lock(PAllocatedLock);
	for
	(
		MemoryInfo *info=PAllocated->First();
		info;
		info=PAllocated->Next(info)
	)
	{
		// report:
		info->Report();
	}
}

#endif
