#ifdef _MSC_VER
#pragma once
#endif

#ifndef _PATH_ACTION_HPP
#define _PATH_ACTION_HPP

enum PathActionType
{
  PATLadderTop,
  PATLadderBottom,
  PATUserActionBegin,
  PATUserActionEnd
};

class PathAction : public RefCount
{
public:
  virtual PathActionType GetType() const = 0;

  virtual int GetLadderIndex() const {return -1;}
  virtual RString GetActionName() const {return RString();}
};

class PathActionLadderTop : public PathAction
{
protected:
  int _ladderIndex;

public:
  PathActionLadderTop(int ladderIndex) {_ladderIndex = ladderIndex;}

  PathActionType GetType() const {return PATLadderTop;}
  int GetLadderIndex() const {return _ladderIndex;}
};

class PathActionLadderBottom : public PathAction
{
protected:
  int _ladderIndex;

public:
  PathActionLadderBottom(int ladderIndex) {_ladderIndex = ladderIndex;}

  PathActionType GetType() const {return PATLadderBottom;}
  int GetLadderIndex() const {return _ladderIndex;}
};

class PathUserActionBegin : public PathAction
{
protected:
  RString _actionName;

public:
  PathUserActionBegin(RString actionName) {_actionName = actionName;}

  PathActionType GetType() const {return PATUserActionBegin;}
  RString GetActionName() const {return _actionName;}
};

class PathUserActionEnd : public PathAction
{
protected:
  RString _actionName;

public:
  PathUserActionEnd(RString actionName) {_actionName = actionName;}

  PathActionType GetType() const {return PATUserActionEnd;}
  RString GetActionName() const {return _actionName;}
};

#endif

