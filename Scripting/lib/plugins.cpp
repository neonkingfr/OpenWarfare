#include <Es/essencepch.hpp>
#include <Es/platform.hpp>
#include <El/Interfaces/iAppInfo.hpp>
#include "plugins.hpp"
#include "world.hpp"

#ifdef _PLUGINSYSTEM

void PluginInfo::Clear()
{
  _hDll = NULL;
  _onSimulationStep = NULL;
  _pluginFunction = NULL;
}

PluginInfo::~PluginInfo()
{
  if (_hDll) FreeLibrary(_hDll);
}

void PluginInfo::Init(HINSTANCE hDll, RStringB name, ONSIMULATIONSTEP onSimulationStep, PLUGINFUNCTION pluginFunction)
{
  _hDll = hDll;
  _name = name;
  _onSimulationStep = onSimulationStep;
  _pluginFunction = pluginFunction;
}

bool PluginInfo::NameMatches(RStringB pluginName) const
{
  return _name == pluginName;
}

void PluginInfo::BeginContext()
{
  GameState *gstate = GWorld->GetGameState();
  gstate->BeginContext(&_vars);
}

void PluginInfo::EndContext()
{
  GameState *gstate = GWorld->GetGameState();
  gstate->EndContext();
}

void PluginInfo::OnSimulationStep(float deltaT)
{
  if (_onSimulationStep != NULL)
  {
    // Set the own variable space
    BeginContext();

    // Run the event
    _onSimulationStep(deltaT);

    // End the variable space
    EndContext();
  }
}

const char *PluginInfo::PluginFunction(const char *input)
{
  const char *retVal = NULL;
  if (_pluginFunction != NULL)
  {
    // Set the own variable space
    BeginContext();

    // Run the event
    retVal = _pluginFunction(input);

    // End the variable space
    EndContext();
  }
  return retVal;
}

//! List of registered plugins
AutoArray< PluginInfo > Plugins;

int ExecuteCommandInt(const char *command, char *result, int resultLength)
{
  int retVal = 3; // This value should be overwritten
  GameState *gstate = GWorld->GetGameState();
  if (!gstate->CheckEvaluate(command, GWorld->GetMissionNamespace()))
  {
    RString exp(command);
    int pos = gstate->GetLastErrorPos(exp);
    Assert(pos >= 0 && pos <= exp.GetLength());
    if (result != NULL)
    {
      RString errorMessage =
        RString("Plugin Error: ") + gstate->GetLastErrorText() +
        RString(" '") + exp.Substring(0, pos) + RString("|#|") + exp.Substring(pos, INT_MAX) + RString("'");
      strncpy(result, cc_cast(errorMessage), min(resultLength, errorMessage.GetLength()));
    }
    retVal = -pos;
  }
  else
  {
    if (result == NULL)
    {
      gstate->Evaluate(command, GameState::EvalContext::_default, GWorld->GetMissionNamespace());
      retVal = 1;
    }
    else
    {
      GameValue val = gstate->Evaluate(command, GameState::EvalContext::_default, GWorld->GetMissionNamespace());
      RString r = val.GetText();
      if (r.GetLength() <= resultLength)
      {
        strcpy(result, cc_cast(r));
        retVal = 1;
      }
      else
      {
        strncpy(result, cc_cast(r), resultLength);
        retVal = 2;
      }
    }
  }
  DoAssert(retVal != 3);
  return retVal;
}

int WINAPI ExecuteCommand(const char *command, char *result, int resultLength)
{
  return ExecuteCommandInt(command, result, resultLength);
}

//! Load the plugins
void RegisterPlugins()
{
  RString dir = "plugins\\";
  WIN32_FIND_DATA info;
  HANDLE h = FindFirstFile(dir + "*.dll", &info);
  if (h != INVALID_HANDLE_VALUE)
  {
    do
    {
      if ((info.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) != 0) continue;
      RString name = dir + info.cFileName;
      HINSTANCE hDll = LoadLibrary(name);
      if (hDll)
      {
        REGISTERHWND pRegisterHWND = (REGISTERHWND)GetProcAddress(hDll, "RegisterHWND");
        if (pRegisterHWND != NULL) {pRegisterHWND(CurrentAppInfoFunctions->GetAppHWnd(), CurrentAppInfoFunctions->GetAppHInstance());}

        // Register the ExecuteCommand function
        REGISTERCOMMANDFNC pRegisterCommandFnc = (REGISTERCOMMANDFNC)GetProcAddress(hDll, "RegisterCommandFnc");
        if (pRegisterCommandFnc != NULL) {pRegisterCommandFnc(ExecuteCommand);}

        // Prepare name of the plugin (no path, no extension)
        RString pluginName = info.cFileName;
        int lastDot = pluginName.ReverseFind('.');
        if (lastDot >= 0) pluginName = pluginName.Substring(0, lastDot);
        pluginName.Lower();

        // Add new plugin
        PluginInfo &pi = Plugins.Append();
        pi.Init(hDll, pluginName,
          (ONSIMULATIONSTEP)GetProcAddress(hDll, "OnSimulationStep"),
          (PLUGINFUNCTION)GetProcAddress(hDll, "PluginFunction"));
      }
    }
    while (FindNextFile(h, &info));
    FindClose(h);
  }
}

#endif

