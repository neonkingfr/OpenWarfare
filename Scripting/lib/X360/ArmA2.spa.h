////////////////////////////////////////////////////////////////////
//
// ArmA2.spa.h
//
// Auto-generated on Monday, 28 January 2008 at 14:24:30
// XLAST project version 1.0.32.0
// SPA Compiler version 2.0.6813.0
//
////////////////////////////////////////////////////////////////////

#ifndef __ARMA_2_SPA_H__
#define __ARMA_2_SPA_H__

#ifdef __cplusplus
extern "C" {
#endif

//
// Title info
//

#define TITLEID_ARMA_2                              0x464F07D2

//
// Context ids
//
// These values are passed as the dwContextId to XUserSetContext.
//

#define CONTEXT_GAME_WORLD                          0
#define CONTEXT_GAME_DIFFICULTY                     1
#define CONTEXT_GAME_MPMISSION                      2
#define CONTEXT_GAME_MISSION                        3
#define CONTEXT_GAME_CAMPAIGN                       4
#define CONTEXT_GAME_SERVERSTATE                    5
#define CONTEXT_GAME_MPTYPE                         6

//
// Context values
//
// These values are passed as the dwContextValue to XUserSetContext.
//

// Values for CONTEXT_GAME_WORLD

#define CONTEXT_GAME_WORLD_UNKNOWN                  0
#define CONTEXT_GAME_WORLD_PORTO                    1
#define CONTEXT_GAME_WORLD_SAHRANI                  2
#define CONTEXT_GAME_WORLD_RAHMADI                  3
#define CONTEXT_GAME_WORLD_CHERNARUS                4

// Values for CONTEXT_GAME_DIFFICULTY

#define CONTEXT_GAME_DIFFICULTY_RECRUIT             0
#define CONTEXT_GAME_DIFFICULTY_REGULAR             1
#define CONTEXT_GAME_DIFFICULTY_VETERAN             2

// Values for CONTEXT_GAME_MPMISSION

#define CONTEXT_GAME_MPMISSION_UNKNOWN              0

// Values for CONTEXT_GAME_MISSION

#define CONTEXT_GAME_MISSION_UNKNOWN                0

// Values for CONTEXT_GAME_CAMPAIGN

#define CONTEXT_GAME_CAMPAIGN_UNKNOWN               0

// Values for CONTEXT_GAME_SERVERSTATE

#define CONTEXT_GAME_SERVERSTATE_NONE               0
#define CONTEXT_GAME_SERVERSTATE_SELECTINGMISSION   1
#define CONTEXT_GAME_SERVERSTATE_EDITINGMISSION     2
#define CONTEXT_GAME_SERVERSTATE_ASSIGNINGROLES     3
#define CONTEXT_GAME_SERVERSTATE_SENDINGMISSION     4
#define CONTEXT_GAME_SERVERSTATE_LOADINGGAME        5
#define CONTEXT_GAME_SERVERSTATE_BRIEFING           6
#define CONTEXT_GAME_SERVERSTATE_PLAYING            7
#define CONTEXT_GAME_SERVERSTATE_DEBRIEFING         8
#define CONTEXT_GAME_SERVERSTATE_MISSIONABORTED     9

// Values for CONTEXT_GAME_MPTYPE

#define CONTEXT_GAME_MPTYPE_UNKNOWN                 0
#define CONTEXT_GAME_MPTYPE_DM                      1
#define CONTEXT_GAME_MPTYPE_CTF                     2
#define CONTEXT_GAME_MPTYPE_FF                      3
#define CONTEXT_GAME_MPTYPE_COOP                    4
#define CONTEXT_GAME_MPTYPE_TEAM                    5
#define CONTEXT_GAME_MPTYPE_SCONT                   6
#define CONTEXT_GAME_MPTYPE_HOLD                    7
#define CONTEXT_GAME_MPTYPE_CTI                     8

// Values for X_CONTEXT_PRESENCE

#define CONTEXT_PRESENCE_STR_RP_INMENU              0
#define CONTEXT_PRESENCE_STR_RP_CAMPAIGN            1
#define CONTEXT_PRESENCE_STR_RP_CAMPAIGNREPLAY      2
#define CONTEXT_PRESENCE_STR_RP_SINGLEMISSION       3
#define CONTEXT_PRESENCE_STR_RP_USERMISSION         4
#define CONTEXT_PRESENCE_STR_RP_LIVE_WAITING        5
#define CONTEXT_PRESENCE_STR_RP_LIVE_SETUP          6
#define CONTEXT_PRESENCE_STR_RP_LIVE_PLAYING        7
#define CONTEXT_PRESENCE_STR_RP_SYSTEMLINK_WAITING  8
#define CONTEXT_PRESENCE_STR_RP_SYSTEMLINK_SETUP    9
#define CONTEXT_PRESENCE_STR_RP_SYSTEMLINK_PLAYING  10
#define CONTEXT_PRESENCE_STR_RP_DEDICATED_SERVER    11

// Values for X_CONTEXT_GAME_MODE

#define CONTEXT_GAME_MODE_NONE                      0
#define CONTEXT_GAME_MODE_MP                        1
#define CONTEXT_GAME_MODE_SINGLE                    2
#define CONTEXT_GAME_MODE_CAMPAIGN                  3

//
// Property ids
//
// These values are passed as the dwPropertyId value to XUserSetProperty
// and as the dwPropertyId value in the XUSER_PROPERTY structure.
//

#define PROPERTY_GAME_VERSION                       0x10000001
#define PROPERTY_GAME_REQUIRED_VERSION              0x10000002
#define PROPERTY_GAME_TIMELEFT                      0x10000003
#define PROPERTY_STATS_COLUMN_GAMESPLAYED           0x1000000B
#define PROPERTY_STATS_COLUMN_SCORE                 0x2000000A

//
// Achievement ids
//
// These values are used in the dwAchievementId member of the
// XUSER_ACHIEVEMENT structure that is used with
// XUserWriteAchievements and XUserCreateAchievementEnumerator.
//

#define ACHIEVEMENT_QUARTERMASTER                   1
#define ACHIEVEMENT_MARKSMAN                        2
#define ACHIEVEMENT_SPEEDFREAK                      3
#define ACHIEVEMENT_SKYHIGH                         4
#define ACHIEVEMENT_HUNTER                          5

//
// Stats view ids
//
// These are used in the dwViewId member of the XUSER_STATS_SPEC structure
// passed to the XUserReadStats* and XUserCreateStatsEnumerator* functions.
//

// Skill leaderboards for ranked game modes

#define STATS_VIEW_SKILL_RANKED_NONE                0xFFFF0000
#define STATS_VIEW_SKILL_RANKED_MP                  0xFFFF0001
#define STATS_VIEW_SKILL_RANKED_SINGLE              0xFFFF0002
#define STATS_VIEW_SKILL_RANKED_CAMPAIGN            0xFFFF0003

// Skill leaderboards for unranked (standard) game modes

#define STATS_VIEW_SKILL_STANDARD_NONE              0xFFFE0000
#define STATS_VIEW_SKILL_STANDARD_MP                0xFFFE0001
#define STATS_VIEW_SKILL_STANDARD_SINGLE            0xFFFE0002
#define STATS_VIEW_SKILL_STANDARD_CAMPAIGN          0xFFFE0003

// Title defined leaderboards

#define STATS_VIEW_STANDARD_DM                      1
#define STATS_VIEW_STANDARD_CTF                     2
#define STATS_VIEW_STANDARD_FF                      3
#define STATS_VIEW_STANDARD_TEAM                    4
#define STATS_VIEW_STANDARD_SCONTROL                5
#define STATS_VIEW_STANDARD_HOLD                    6
#define STATS_VIEW_STANDARD_UNKNOWN                 7
#define STATS_VIEW_RANKED_DM                        8
#define STATS_VIEW_RANKED_CTF                       9
#define STATS_VIEW_RANKED_FF                        10
#define STATS_VIEW_RANKED_TEAM                      11
#define STATS_VIEW_RANKED_SCONTROL                  12
#define STATS_VIEW_RANKED_HOLD                      13
#define STATS_VIEW_RANKED_UNKNOWN                   14

//
// Stats view column ids
//
// These ids are used to read columns of stats views.  They are specified in
// the rgwColumnIds array of the XUSER_STATS_SPEC structure.  Rank, rating
// and gamertag are not retrieved as custom columns and so are not included
// in the following definitions.  They can be retrieved from each row's
// header (e.g., pStatsResults->pViews[x].pRows[y].dwRank, etc.).
//

// Column ids for STANDARD_DM

#define STATS_COLUMN_STANDARD_DM_GAMES_PLAYED       1

// Column ids for STANDARD_CTF

#define STATS_COLUMN_STANDARD_CTF_GAMES_PLAYED      1

// Column ids for STANDARD_FF

#define STATS_COLUMN_STANDARD_FF_GAMES_PLAYED       1

// Column ids for STANDARD_TEAM

#define STATS_COLUMN_STANDARD_TEAM_GAMES_PLAYED     1

// Column ids for STANDARD_SCONTROL

#define STATS_COLUMN_STANDARD_SCONTROL_GAMES_PLAYED 1

// Column ids for STANDARD_HOLD

#define STATS_COLUMN_STANDARD_HOLD_GAMES_PLAYED     1

// Column ids for STANDARD_UNKNOWN

#define STATS_COLUMN_STANDARD_UNKNOWN_GAMES_PLAYED  1

// Column ids for RANKED_DM

#define STATS_COLUMN_RANKED_DM_GAMES_PLAYED         1

// Column ids for RANKED_CTF

#define STATS_COLUMN_RANKED_CTF_GAMES_PLAYED        1

// Column ids for RANKED_FF

#define STATS_COLUMN_RANKED_FF_GAMES_PLAYED         1

// Column ids for RANKED_TEAM

#define STATS_COLUMN_RANKED_TEAM_GAMES_PLAYED       1

// Column ids for RANKED_SCONTROL

#define STATS_COLUMN_RANKED_SCONTROL_GAMES_PLAYED   1

// Column ids for RANKED_HOLD

#define STATS_COLUMN_RANKED_HOLD_GAMES_PLAYED       1

// Column ids for RANKED_UNKNOWN

#define STATS_COLUMN_RANKED_UNKNOWN_GAMES_PLAYED    1

//
// Matchmaking queries
//
// These values are passed as the dwProcedureIndex parameter to
// XSessionSearch to indicate which matchmaking query to run.
//

#define SESSION_MATCH_QUERY_STANDARDMATCH           0

//
// Gamer pictures
//
// These ids are passed as the dwPictureId parameter to XUserAwardGamerTile.
//



#ifdef __cplusplus
}
#endif

#endif // __ARMA_2_SPA_H__


