#ifdef _MSC_VER
#pragma once
#endif

#ifndef _IN_GAME_UI_HPP
#define _IN_GAME_UI_HPP

#include "../types.hpp"
#include <El/Math/math3d.hpp>
#include "../AI/aiTypes.hpp"

//enum Team;
class EntityAIFull;
class Target;

class AIBrain;
class KBMessageInfo;
struct ConversationContext;

class INode;

#ifndef DECL_ENUM_TEAM
#define DECL_ENUM_TEAM
DECL_ENUM(Team)
#endif

#include "../vehicleAI.hpp"
class AbstractUI
{
protected:
	bool _showUnitInfo;
	bool _showTacticalDisplay;
	bool _showCompass;
	bool _showTankDirection;
	bool _showMenu;
	bool _showGroupInfo;
	bool _showCursors;

  float _weaponInfoAlpha;
  PackedColor _weaponInfoColor;

public:
  virtual const TargetList * VisibleList() const = 0;

  bool _drawHCCommand;
  bool _drawGroupIcons2D;
  bool _drawGroupIcons3D;
  bool _selectableGroupIcons;
  /// disable / enable HUD drawing by the script
  bool _showHUD;


  /// draw anything into the 3d scene as needed
  virtual void Draw3D() = 0;
	/// draw overlay - called before FinishDraw
	virtual void DrawHUD(const Camera &camera, CameraType cam)= 0;

  /// draw hints, need to be called later then DrawHUD
  virtual void DrawHints ()= 0;

	/// simulate controls - called during simulation
	virtual void SimulateHUD(const Camera &camera, CameraType cam, float deltaT)= 0;

	virtual void DrawHUDNonAI(const Camera &camera, Entity *vehicle, CameraType cam)= 0;

	/// simulate controls - called during simulation
	virtual void SimulateHUDNonAI(const Camera &camera, Entity *vehicle, CameraType cam, float deltaT)= 0;

  /// limited in-game UI drawing when camera effect active
  virtual void DrawHUDCameraEffect(const Camera &camera, CameraType cam) = 0;

  virtual void SelectUnit(AIUnit *unit) = 0;
  virtual void SelectUnit(AIUnit *unit, bool openMenu) = 0;
  virtual void UnselectUnit(AIUnit *unit) = 0;

#if _VBS3 // command mode in RTE
  virtual void ProcessMenu(const Camera &camera, CameraType cam, EntityAI *vehicle) = 0;
  virtual bool IsSelectedUnit(AIUnit *unit) const = 0;

  virtual void DrawMenu(bool isInEditor = false) = 0;
  virtual void DrawGroupInfo(EntityAI *vehicle) = 0;

  bool IsShownMenu() {return _showMenu;}
  bool IsShownGroupInfo() {return _showGroupInfo;}

  virtual void UpdateTMTime() = 0;
#endif

	// called in BrowseCamera - when HUD subject changes
	virtual void Init() = 0;
  // called when mission started / restarted
  virtual void InitMission() = 0;
	virtual void ResetHUD() = 0;
	virtual void ResetVehicle( EntityAIFull *vehicle ) = 0; // when vehicle is changed
  /// Preload textures needed to draw tank direction
  virtual void PreloadTankDirection() = 0;
  //reset weapon info alfa after reload
  virtual void ShowWeaponInfo() = 0;

  virtual void ResetLastWpTime() = 0;
  virtual void ResetLastCmdTime() = 0;

  virtual Team GetTeam(AIUnit *unit) = 0;
  virtual void SetTeam(AIUnit *unit, Team team) = 0;

  virtual void ClearSelectedUnits() = 0;
  virtual bool IsEmptySelectedUnits() = 0;
  
  virtual void ListSelectedUnits(OLinkPermNOArray(AIUnit) &list) = 0;
  virtual void ListSelectingUnits(OLinkPermNOArray(AIUnit) &list) = 0;

  /// check what target is currently locked
  virtual Target *GetLockedTarget() const = 0;
  /// check what target is the cursor pointing at
  virtual Target *GetCursorTarget() const = 0;
  /// check what object is the cursor pointing at
  virtual Object *GetCursorObject() const = 0;

  virtual void LockTarget(Target *tgt) = 0;
  /// Reveal target on cursor position
  virtual void RevealCursorTarget() = 0;

  /// Returns the name of the topmost commanding menu
  virtual RString GetMenu() const = 0;
  /// Create the commanding menu based on the given resource
  virtual void SetMenu(RString name) = 0;
  virtual void HideMenu() = 0;
  virtual bool IsMenuVisible() = 0;
	
	virtual const AutoArray<RString> &GetCustomRadio() const = 0;

	void ShowUnitInfo(bool show = true) {_showUnitInfo = show;}
	void ShowTacticalDisplay(bool show = true) {_showTacticalDisplay = show;}
	void ShowCompass(bool show = true) {_showCompass = show;}
	void ShowMenu(bool show = true) {_showMenu = show;}
	void ShowTankDirection(bool show = true) {_showTankDirection = show;}
	void ShowGroupInfo(bool show = true) {_showGroupInfo = show;}
	void ShowAll(bool show = true);

  void ScriptShowHUD(bool show = true) {_showHUD = show;}
  bool ScriptIsHUDShown() const {return _showHUD;}

  bool IsHUDShown() const;
  virtual bool IsCommandingMode() const = 0;
  /// check whether left mouse button is handled by the HUD
  virtual bool IsLMBHandled() const = 0;
  virtual bool IsCompassShown() const = 0;
  virtual bool IsCommandingForced() const = 0;

	void ShowCursors(bool show = true) {_showCursors = show;}

	virtual void ShowMe() = 0;
	virtual void ShowTarget() = 0;
	virtual void ShowCommand() = 0;
	virtual void ShowFormPosition() = 0;
	virtual void ShowWaypointPosition() = 0;
	virtual void ShowSelectedUnits() = 0;
	virtual void ShowFollowMe() = 0;
	virtual void ShowGroupDir() = 0;

	virtual void ShowHint(RString hint, bool sound) = 0;
  virtual void ShowHint(INode *hint, bool sound) = 0;

  virtual void ShowTaskHint(RString taskHint, PackedColor color, Texture *texture) = 0;
	
	virtual void LaunchMenu() = 0;

	// used to handle mouse movements
	virtual Vector3 GetCursorDirection() const = 0;
  virtual Vector3 GetCamGroupCameraPos() const = 0;
  virtual void InitCamGroupCameraPos() = 0;
  virtual void CamGroupActions(float deltaT) = 0;
	virtual void SetCursorDirection( Vector3Par dir ) = 0;

	virtual void SwitchToStrategy( EntityAIFull *vehicle ) = 0;
	virtual void SwitchToFire( EntityAIFull *vehicle ) = 0;
	
  virtual LONG ProcessUserMessage(UINT msg, WORD wParam, DWORD lParam) = 0;

  virtual Texture *GetCueTexture() const = 0;
  virtual PackedColor GetCueColor() const = 0;
  virtual PackedColor GetCueEnemyColor() const = 0;
  virtual PackedColor GetCueFriendlyColor() const = 0;
  virtual Texture *GetBloodTexture() const = 0;
  virtual PackedColor GetBloodColor() const = 0;
  virtual int GetCueShadow() const = 0;


  virtual Texture *GetHitZonesTexture() const = 0;
  virtual int const GetHitZonesShadow() const = 0;

  virtual void SetEventHandler(RString name, RString value) = 0;
  virtual bool IsXBOXStyle() = 0;

  // show high command bar
  virtual void ShowHCCommand(bool show) {_drawHCCommand = show;};
  bool IsHcCommandVisible() {return _drawHCCommand;};

  // show group markers
  void ShowGroupIcons2D(bool show) {_drawGroupIcons2D = show;};
  void ShowGroupIcons3D(bool show) {_drawGroupIcons3D = show;};
  bool IsGroupIconVisible2D() {return _drawGroupIcons2D;};
  bool IsGroupIconVisible3D() {return _drawGroupIcons3D;};
  // show high command bar
  void SelectableGroupIcons(bool show) {_selectableGroupIcons = show;};
  bool IsGroupIconSelectable() {return _selectableGroupIcons;};

  //player artillery controls
  virtual void SetAtilleryTarget(bool locked, Vector3 target = Vector3(0,0,0)) = 0;
  virtual Vector3 GetAtilleryTarget()  = 0;
  virtual bool GetAtilleryTargetLocked() = 0;

#if _ENABLE_CONVERSATION
  /// return the context of conversation menu
  virtual ConversationContext *GetConversationContext() = 0;
  /// open the conversation menu
  virtual void ProcessConversation(AIBrain *askingUnit, AIBrain *askedUnit, const KBMessageInfo *question) = 0;
#endif

	virtual ~AbstractUI(){}
};

AbstractUI *CreateInGameUI();

#endif
