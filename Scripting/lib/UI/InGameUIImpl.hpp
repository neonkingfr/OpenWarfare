#ifdef _MSC_VER
#pragma once
#endif

#ifndef _IN_GAME_UI_IMPL_HPP
#define _IN_GAME_UI_IMPL_HPP

#include "inGameUI.hpp"
#include "../global.hpp"
#include "../vehicle.hpp"

#include "uiControls.hpp"
#include "uiActions.hpp"
#include "menu.hpp"

#include "../dikCodes.h"

#include "../integrity.hpp"

#include <El/HierWalker/hierWalker.hpp>

#if _ENABLE_SPEECH_RECOGNITION
#include <RV/SpeechRecognition/fonix.hpp>
#include <RV/SpeechRecognition/voiceCapture.hpp>
#endif

#if _ENABLE_CONVERSATION
#include <RV/KBBase/kbCenter.hpp>
#endif

#ifndef _XBOX
#define SPEEREO_SPEECH  0
#else
#define SPEEREO_SPEECH  0
#endif

enum UIMode
{
	UIFire, // weapon aiming
	UIFirePosLock,	// possible lock
	UIStrategy, // no command
	UIStrategySelect, // select subordinate unit
	UIStrategyMove, // command unit: move to target
	UIStrategyAttack, // command unit: attack target
	UIStrategyFire, // command unit: fire
  UIStrategyFireAtPosition,  // command unit: fire at position
	UIStrategyGetIn, // command unit: get in vehicle
  UIStrategyGetOut, // command unit: get out vehicle
	UIStrategyWatch, // command unit: watch position or target
  UIStrategyHeal, // command unit: watch position or target
  UIStrategyRepairvehicle, // command unit: watch position or target
  UIStrategyTakeBackpack,
  UIStrategyAssemble,
  UIStrategyDisassemble,
  UIStrategyComplex, // command unit: watch position or target
  UIStrategyAction, // command unit: watch position or target
	// define command groups
	UIFireMin=UIFire,UIFireMax=UIFirePosLock,
	UIStrategyMin=UIStrategy,UIStrategyMax=UIStrategyAction,
};

#define ModeIsStrategy(x) ( (x)>=UIStrategyMin && (x)<=UIStrategyMax )

#define N_SHOW_COMMANDS 10

#include "../AI/aiRadio.hpp" // TODO: move Team to some other place (ai.hpp?)

struct UnitDescription
{
public:
	enum Status
	{
		none,
		wait,
		away,
		command,
		cargo,
		driver,
		gunner,
    commander
	};

	bool valid;
  bool selecting;
	bool selected;
	bool player;
	bool playerVehicle;
	OLinkPerm<Transport> vehicle;
	OLinkPerm<Person> person;
	AI::Semaphore semaphore;
	Status status;
  Status vehicleStatus;
	Command::Message cmd;
	int vehCommander;
	int leader;
	Team team;
  CombatMode combatMode;
  int problemsType;

	UnitDescription()	{valid = false;}
};

struct CursorText
{
	RString text;
	float upDown;
  bool allignRight;
};
TypeIsMovableZeroed(CursorText);

class CursorTexts : public AutoArray<CursorText>
{
typedef AutoArray<CursorText> base;

public:
	void Add(RString text, float upDown, bool allignRight = false)
	{
		int index = base::Add();
		Set(index).text = text;
		Set(index).upDown = upDown;
    Set(index).allignRight = allignRight;
	}
};

class DisplayHitZoneInfo : public CListBox
{
public:

  DisplayHitZoneInfo(ControlsContainer *parent, int idc, ParamEntryPar cls);
  virtual void DrawItem
    (
    UIViewport *vp, float alpha, int i, bool selected, float top, const Rect2DFloat &rect
    );
};

//! HUD unit info display
class DisplayUnitInfo : public Display
{
public:
	//@{
	//! fast access to control
	InitPtr<CStaticTime> time;
	InitPtr<CStatic> date;
	InitPtr<CStatic> name;
	InitPtr<CStatic> unit;
	InitPtr<CStatic> vehicle;
	InitPtr<CProgressBar> valueExp;
	InitPtr<CStatic> formation;
	InitPtr<CStatic> combatMode;
	InitPtr<CStatic> speed;
	InitPtr<CStatic> alt;
  InitPtr<CStatic> heading;
  InitPtr<CStatic> radarRange;

  InitPtr<CStatic> alt_wanted;
  InitPtr<CStatic> speed_wanted;
  InitPtr<CStatic> position;
  InitPtr<CStatic> opticZoom;
#if _VBS3
  InitPtr<CStatic> magazine;
  InitPtr<CStatic> override_status;
  //TI
  InitPtr<CStatic> ti_background;
  InitPtr<CStatic> ti_mode;
  InitPtr<CStatic> ti_brightness_txt;
  InitPtr<CProgressBar> ti_brightness;
  InitPtr<CStatic> ti_contrast_txt;
  InitPtr<CProgressBar> ti_contrast;
  InitPtr<CStatic> ti_autocontrast;

  //AVRS
  InitPtr<CStatic> nick;
  InitPtr<CStatic> nick_val;
  InitPtr<CStatic> bank;
  InitPtr<CStatic> bank_val;
#endif //_VBS3

  InitPtr<CProgressBar> valueHealth;
	InitPtr<DisplayHitZoneInfo> valueArmor;
	InitPtr<CProgressBar> valueFuel;
  InitPtr<CProgressBar> valueReload;
	InitPtr<CStatic> cargoMan;
	InitPtr<CStatic> cargoFuel;
	InitPtr<CStatic> cargoRepair;
	InitPtr<CStatic> cargoAmmo;
	InitPtr<CStatic> weapon;
	InitPtr<CStatic> ammo;
  InitPtr<CStatic> gunnerWeapon;
  InitPtr<CStatic> ammoMode;
  InitPtr<CStatic> counterMeasuresAmmo;
  InitPtr<CStatic> counterMeasuresMode;

	InitPtr<CStatic> background;
  float backgroundHeight;
  float backgroundWidth;

  InitPtr<CStatic> commander;
  InitPtr<CStatic> driver;
  InitPtr<CStatic> gunner;

  //@}

  //@{
  /// optional dialog size adaptation flags
  bool _updateHeightByCrew;
  bool _updateWidthByCrew;
  bool _updateWidthByWeapon;
  //@}

public:
	//! constructor
	/*!
		\param parent parent display
	*/
	DisplayUnitInfo(ControlsContainer *parent);
	//! Initialize fast access to controls (sets to NULL)
	void InitControls();
	//! Reload display from given resource template
	void Reload(ParamEntryVal clsEntry);
	Control *OnCreateCtrl(int type, int idc, ParamEntryPar cls);
};


//! HUD unit info display
class DisplayWeaponInfo : public Display
{
public:
  //@{
  //! fast access to control
  InitPtr<CStatic> distance;
  InitPtr<CStatic> visionMode;
  InitPtr<CStatic> FLIRMode;
  InitPtr<CStatic> FOVMode;
  InitPtr<CStatic> compass;
  InitPtr<CStatic> heading;
  InitPtr<CStatic> autohover;
  InitPtr<CStatic> laserMarkerON;
  InitPtr<CStatic> BallEnabled;
  InitPtr<CStatic> BallRange;
  InitPtr<CStatic> elevation;
  InitPtr<CStatic> elevationText;
  InitPtr<CStatic> GPSPlayer;
  InitPtr<CStatic> GPSTarget;

  InitPtr<CStatic> javelinDay;
  InitPtr<CStatic> javelinFLTR;
  InitPtr<CControlsGroup> javelinNFOV;
  InitPtr<CControlsGroup> javelinWFOV;
  InitPtr<CStatic> javelinSEEK;
  InitPtr<CStatic> javelinMISSILE;

  InitPtr<CControlsGroup> staticItems;

  InitPtr<CStatic> BallEnabledImg;
  bool showCompass;
  //@}

public:
  //! constructor
  /*!
  \param parent parent display
  */
  DisplayWeaponInfo(ControlsContainer *parent);
  //! Initialize fast access to controls (sets to NULL)
  void InitControls();
  //! Reload display from given resource template
  void Reload(ParamEntryVal clsEntry);
  Control *OnCreateCtrl(int type, int idc, ParamEntryPar cls);
};

#if _VBS3_LASE_LEAD
// laser range readout
class DisplayLaserRange : public Display
{
public:
  //@{
  //! fast access to control
  CStatic *range;
  CStatic *rangeNum;
  CStatic *background;
  //@}
public:
  //! constructor
  /*!
  \param parent parent display
  */
  DisplayLaserRange(ControlsContainer *parent);
  //! Initialize fast access to controls (sets to NULL)
  void InitControls();
  //! Reload display from given resource template
  void Reload(const ParamEntry &clsEntry);
  Control *OnCreateCtrl(int type, int idc, ParamEntryPar cls);
};
#endif

//! HUD hint display
class DisplayHint : public Display
{
protected:
	//@{
	//! fast access to control
	Ref<Control> _background;
	Ref<Control> _hint;
	//@}

public:
	//! constructor
	/*!
		\param parent parent display
	*/
	DisplayHint(ControlsContainer *parent);
	Control *OnCreateCtrl(int type, int idc, ParamEntryPar cls);
	//! returns displayed hint
	RString GetHint() const;
	//! sets displayed hint
	void SetHint(RString hint);
  //! sets displayed hint (structured text)
  void SetHint(INode *hint);
	//! sets new hint position
	void SetPosition(float top);	// height is calculate
};

//! HUD display info about new/completed/failed task
class DisplayTaskHint : public Display
{
protected:
  //@{
  //! fast access to control
  Ref<Control> _background;
  Ref<Control> _taskHint;
  //! task icon area
  Rect2DPixel _textureRect;

  AutoArray<Ref<Texture> > _textureQue;
  AutoArray<PackedColor> _colorQue;  
  AutoArray<RString> _textQue;
  //@}
public:
  UITime _timeUp;    //time  until alpha reaches 1
  UITime _timeDown;  //time until alpha reaches 0

public:
  //! constructor
  /*!
  \param parent parent display
  */
  DisplayTaskHint(ControlsContainer *parent);
  Control *OnCreateCtrl(int type, int idc, ParamEntryPar cls);
  //! returns displayed task hint
  RString GetTaskHint() const;
  //! Add task to Que
  void AddTaskHint(RString hint, PackedColor color = PackedColor(255,255,255,255),Ref<Texture> texture = NULL);
  //! sets displayed task hint
  void SetTaskHint();
  //! sets new task hint position
  void SetPosition(float top);	// height is calculate
  Rect2DPixel TextureRectangle() const {return _textureRect;}
  int Size() const {return _textQue.Size();}
  void Delete(int index)
  {
    if (_textureQue.Size() > index)
      _textureQue.Delete(index);
    if (_colorQue.Size() > index)
      _colorQue.Delete(index);
    if (_textQue.Size() > index)
      _textQue.Delete(index);
  }
  PackedColor GetColor(int index) const {return _colorQue[0];}
  Ref<Texture> GetTexture(int index) const {return _textureQue[0];}
  void ClearHints() 
  {
    _textQue.Clear();
    _textureQue.Clear();
    _colorQue.Clear();  
  }
};


DECL_ENUM(VCommand)

/// list of target - avoid allocation, provide iteration

class TargetBackupList: public LLinkArray<Target,MemAllocSA>, private NoCopy
{
  public:
  TargetBackupList(const MemAllocSA &storage);
  
  template <class Functor>
  bool ForEachEntity(const Functor &f) const
  {
    for (int i=0; i<Size(); i++)
    {
      EntityAI *ai = Get(i)->idExact;
      if (ai && f(ai)) return true;
    }
    return false;
  }
  template <class Functor>
  bool ForEach(const Functor &f) const
  {
    for (int i=0; i<Size(); i++)
    {
      if (f(Get(i))) return true;
    }
    return false;
  }
};

#define INGAMEUI_EVENT_ENUM(type, prefix, XX) \
  XX(type, prefix, PrevAction) \
  XX(type, prefix, NextAction) \
  XX(type, prefix, Action) \

#ifndef DECL_ENUM_INGAMEUI_EVENT
#define DECL_ENUM_INGAMEUI_EVENT
DECL_ENUM(InGameUIEvent)
#endif
DECLARE_ENUM(InGameUIEvent, IE, INGAMEUI_EVENT_ENUM)

struct SelectedUnitInfo
{
  OLinkPermNO(AIUnit) _unit;
  UITime _lastSelTime;

  SelectedUnitInfo()
  {
    _lastSelTime = Glob.uiTime - 120;
  }
};
TypeContainsOLink(SelectedUnitInfo)

typedef OLinkPermNOArray(AIUnit) TeamMembers;

struct UnitScreenPosition
{
public:
  OLinkPermNO(AIGroup) _group;
  float _x,_y;
  int _waipoint;
  float _radius;

  UnitScreenPosition()
  {
  }

  UnitScreenPosition(AIGroup *group, float x, float y, int waipoint, float radius)
  {
    _group = group;
    _x = x;
    _y = y;
    _waipoint = waipoint;
    _radius =radius;
  }
};
TypeIsMovable(UnitScreenPosition)

//! In-game UI layer
/*!
	UI elements important for gameplay,
	like cursors, tactical display, target info.

\patch_internal 1.01 Date 6/26/2001 by Ondra. Added timed protection message
*/

class InGameUI: public AbstractUI
{
	protected:

	UIMode _mode;
	UIMode _modeAuto;
  /// point on the ground the cursor is pointing to
	Point3 _groundPoint;
	float _groundPointDistance;

  /// active target (under cursor)
	LinkTarget _target;
  /// locked target (weapons locked to it)
	LinkTarget _lockTarget;
  /// active HC group (under cursor)
  OLink(AIGroup) _hcTarget;

  //player artillery helpers
  bool _artilleryLocked;
  Vector3 _artilleryTarget;

  //indication of locked / locking missiles
  RefAbstractWave _lockedSound;

  LinkTarget _lastTarget;
  LinkTarget _preferedTarget;
  
  /// active object (under cursor)
  OLink(Object) _cursorObject;
  /// position on the _cursorObject where are we point to with the cursor
  Vector3 _cursorObjectPos;
  
  Ref<CoverVars> _cover;
	int _housePos;
	bool _wantLock; // users wants to lock enemy target
	UITime _timeSendTarget;
	UITime _timeToPlay; //!< time of protection message

  AutoArray<UnitScreenPosition> _groupMarkersPosition;
  OLinkPermNO(AIGroup) _lastGroupOver;
  bool _hcSelected;

public:
	const TargetList *VisibleList() const
	{
		AIBrain *unit = GLOB_WORLD->FocusOn();
    return (unit) ? unit->AccessTargetList() : NULL;
	}
protected:

	int _visibleListTempStorage[128];
	TargetBackupList _visibleListTemp;
	UIActions _actionsTemp;

	mutable AutoArray<RString> _customRadio;

  AutoArray<SelectedUnitInfo> _selectedUnits;
  TeamMembers _teamMembers[NTeams];

  /// menu content changed during this frame
  bool _menuChanged;
	bool _xboxStyle;
  bool _playerIsLeader;

#if _ENABLE_CHEATS
	bool _showAll;
#endif

	bool _blinkState;
	bool _groundPointValid;
	UITime _blinkStateChange;

	Vector3 _modelCursor; // cursor direction relative to vehicle
  float _camGroupHeading, _camGroupDive, _camGroupZoom; // camera position for commanding camera
  float _camGroupDiveOld;
  Point2DFloat _camGroupCursor;

	bool _leftPressed;
	bool _rightPressed;

  bool _initialMenu;

  /// info about some unit changed
  bool _groupInfoChanged;
  /// the first unit shown in bar
  int _groupInfoOffset;
  /// the selected unit in the bar
  int _groupInfoSelected;
  /// one page of info about units
  UnitDescription _groupInfo[10];

  //scrolling menu
  bool _menuScroll;

	Vector3 _lockAim; // explicit gun direction
	UITime _lockAimValidUntil;

	UITime _lastGroupInfoTime;
	UITime _lastMeTime;
  UITime _lastFollowMeTime;
	UITime _lastCmdTime;
	UITime _lastTargetTime;
	UITime _lastGroupDirTime;
	UITime _lastFormTime;
	UITime _lastWpTime;
	UITime _lastMenuTime;
  UITime _lastMedicTime;

	int _lastCmdId;

	Ref<Menu> _units;
  Ref<Menu> _unitsSingle;
	RefArray<Menu> _menu;
  Ref<Menu> _basicMenu;
	
  struct UnitInfoDesc
  {
    RStringB _name;
    SRef<DisplayUnitInfo> _display;
    UITime _changed;

    ClassIsMovableZeroed(UnitInfoDesc);
  };
  AutoArray<UnitInfoDesc> _lastUnitInfos;

  struct WeaponInfoDesc
  {
    RStringB _name;
    SRef<DisplayWeaponInfo> _display;
    UITime _changed;

    ClassIsMovableZeroed(WeaponInfoDesc);
  };
  AutoArray<WeaponInfoDesc> _lastWeaponInfos;

#if _VBS3_LASE_LEAD
  SRef<DisplayLaserRange> _laserRange;
#endif

	SRef<DisplayHint> _hint;
  SRef<DisplayTaskHint> _taskHint;
	UITime _hintTime;
	float _hintTop;
	SoundPars _hintSound;

	float _tmPos;
	UITime _tmTime;
	float _tankPos;
	UITime _tankTime;
	bool _tmIn;
	bool _tmOut;
	bool _tankIn;
	bool _tankOut;

	bool _fireEnabled;

  /// start time of fire action (used for grenades)
  Time _fireStartTime;
  /// scale coef for weapon cursor
  float _weaponCursorScaleCoef;

  bool _mouseDown;
#ifndef _XBOX
  bool _dragging;
	UITime _mouseDownTime;

  Vector3 _startSelection;
	Vector3 _endSelection;
#endif

  UITime _showQuickMenu; // time when quick menu should be displayed (in reaction to UAForceCommandingMode)

	UIActionsDrawable _actions;

  //! event handlers
  RString _eventHandlers[NInGameUIEvent];

  enum SelectionType
  {
    STNone,
    STAll,
    STUnit,
    STVehicle,
    STTeamMain,
    STTeamRed,
    STTeamGreen,
    STTeamBlue,
    STTeamYellow,
  };
  SelectionType _selection;

#if SPEEREO_SPEECH
  int _idSpeechEngine;
#endif

#if _ENABLE_SPEECH_RECOGNITION
  RString _vocabulary;
  VoiceCapture _capture;
  FonixCommunicator _communicator;
#endif

#if _ENABLE_CONVERSATION
  SRef<ConversationContext> _conversationContext;
#endif

	public:
	InGameUI();
	~InGameUI();

	void DrawHUD(const Camera &camera, CameraType cam); // draw overlay - called before FinishDraw
	void Draw3D();
  void DrawHints(); // draw hints - needs to be called later than DrawHUD
	void SimulateHUD(const Camera &camera, CameraType cam, float deltaT);


  /// send target command to the gunner
  void SendTargetToGunner( bool gunnerContextValid, TurretContextV &gunnerContext, Transport * transport );
  /// find and lock target - react to mouse button (cursor sensitive)
  void FindAndLockTarget( EntityAIFull * vehicle, bool gunnerContextValid, TurretContextV &gunnerContext, Transport * transport );
	// simplified version - used for non-AI vehicles
	void DrawHUDNonAI(const Camera &camera, Entity *vehicle, CameraType cam); // draw overlay - called before FinishDraw
	void SimulateHUDNonAI(const Camera &camera, Entity *vehicle, CameraType cam, float deltaT);
  // simplified version - used when camera effect active
  void DrawHUDCameraEffect(const Camera &camera, CameraType cam);

	void ResetHUD();
	void ResetVehicle( EntityAIFull *vehicle );

  void InitMission();

	void ShowMessage( int channel, const char *text);
	void ShowMe() {_lastMeTime = Glob.uiTime;}
	void ShowFormPosition() {_lastFormTime = Glob.uiTime;}
	void ShowWaypointPosition() {_lastWpTime = Glob.uiTime;}
	void ShowTarget() {_lastTargetTime = Glob.uiTime;}
	void ShowCommand() {_lastCmdTime = Glob.uiTime;}
	void ShowSelectedUnits();
	void ShowFollowMe() {_lastFollowMeTime = Glob.uiTime;}
	void ShowGroupDir() {_lastGroupDirTime = Glob.uiTime;}

	void ShowHint(RString hint, bool sound);
  void ShowHint(INode *hint, bool sound);
  void ShowLastHint() {_hintTime = Glob.uiTime;}

  void ShowTaskHint(RString hint, PackedColor color,Texture *texture);

  void ResetLastWpTime() {_lastWpTime =  Glob.uiTime;};
  void ResetLastCmdTime() {_lastCmdTime =  Glob.uiTime;};

  void ShowMenu() {_lastMenuTime = Glob.uiTime; _tmOut = false; if (_tmPos > 0) _tmIn = true;}
	void LaunchMenu();

  bool IsCommandingMode() const;
  bool IsLMBHandled() const;
  bool IsCompassShown() const;
  bool IsMenuEmpty();
  bool IsCommandingForced() const {return /*_forceCommanding*/ ModeIsStrategy(_mode);}

#if _ENABLE_CONVERSATION
  virtual ConversationContext *GetConversationContext() {return _conversationContext;}
  virtual void ProcessConversation(AIBrain *askingUnit, AIBrain *askedUnit, const KBMessageInfo *question);
#endif

	// used to handle mouse movements
	Vector3 GetCursorDirection() const; // get world cursor direction
	void SetCursorDirection( Vector3Par dir ); // set world cursor direction
  Vector3 GetCamGroupCameraPos() const;
  virtual void InitCamGroupCameraPos();
  void CamGroupActions(float deltaT);

	void SwitchToStrategy( EntityAIFull *vehicle );
	void SwitchToFire( EntityAIFull *vehicle );
	void SelectWeapon(AIBrain *unit, const TurretContext &context, int weapon);

	TargetSide RadarTargetSide(AIBrain *unit, Target &tar);
	
  void RevealCursorTarget();
	/// lock given target
  void LockTarget(Target *tgt);

  virtual Target *GetCursorTarget() const;
  virtual Target *GetLockedTarget() const;
  virtual Object *GetCursorObject() const;

  // find target in a given direction
	void FindTarget(EntityAIFull *me, const TurretContext &context, bool prev);
  // find next target
	void NextTarget(EntityAIFull *me, const TurretContext &context) {FindTarget(me, context, false);}
  // find prev target
	void PrevTarget(EntityAIFull *me, const TurretContext &context) {FindTarget(me, context, true);}

	void RevealTarget( Target *target, float spot );

	void Init();

  Team GetTeam(AIUnit *unit);
  void SetTeam(AIUnit *unit, Team team);
  void ClearTeams();
  void ListTeam(OLinkPermNOArray(AIUnit) &list, Team team);

  bool IsSelectedUnit(AIUnit *unit) const;
  UITime WhenSelectedUnit(AIUnit *unit) const;
  /// select all units in the group of agent
  bool SelectAllUnits(AIBrain *agent);
  void SelectUnit(AIUnit *unit);
  void SelectUnit(AIUnit *unit, bool openMenu);
  void UnselectUnit(AIUnit *unit);
  void ClearSelectedUnits();
  bool IsEmptySelectedUnits();
  void ListSelectedUnits(OLinkPermNOArray(AIUnit) &list);
  // units that are selected or menu selection is on them
  void ListSelectingUnits(OLinkPermNOArray(AIUnit) &list);
  //number of selected HC groups
  int SelectHCGroupsCount(AIUnit *commande);

  // show high command bar
  virtual void ShowHCCommand(bool show);

  void SelectTeam(Team team);

  RString GetMenu() const;
  void SetMenu(RString name);
  void HideMenu() {_tmIn = false; _tmOut = true; _menuScroll = false;}
  bool IsMenuVisible();
	
	const AutoArray<RString> &GetCustomRadio() const
  {
    UpdateCustomRadio();
    return _customRadio;
  }

  LONG ProcessUserMessage(UINT msg, WORD wParam, DWORD lParam);

  virtual Texture *GetCueTexture() const {return _cueTexture;}
  virtual PackedColor GetCueColor() const {return _cueColor;}
  virtual PackedColor GetCueEnemyColor() const {return _cueEnemyColor;}
  virtual PackedColor GetCueFriendlyColor() const {return _cueFriendlyColor;}
  virtual Texture *GetBloodTexture() const {return _bloodTexture;}
  virtual PackedColor GetBloodColor() const {return _bloodColor;}
  virtual int GetCueShadow() const {return _cueShadow;}

  virtual void PreloadTankDirection();
  virtual Texture *GetHitZonesTexture() const {return _hitZones;}
  virtual int const GetHitZonesShadow() const {return _hitShadow;}

  /// set event handler
  virtual void SetEventHandler(RString name, RString value);
  //! process event handler - no additional arguments
  bool OnEvent(InGameUIEvent event);
  virtual bool IsXBOXStyle() { return _xboxStyle; }

  /// handle parameters in the menu texts
  RString ProcessMenuParams(const char *&ptr);
  //show weapon info
  virtual void ShowWeaponInfo();

  //player artillery controls
  virtual void SetAtilleryTarget(bool locked, Vector3 target = Vector3(0,0,0)) {_artilleryLocked = locked; _artilleryTarget = target;};
  virtual Vector3 GetAtilleryTarget() { return _artilleryTarget;};
  virtual bool GetAtilleryTargetLocked() { return _artilleryLocked;};

// implementation
private:
	void InitMenu();
	void CreateDynamicMenu(Menu *menu, const char *name);
	//! enable / disable / show / hide menu items
	void CheckMenuItems(Menu *menu);
  const Menu *GetCurrentMenu() const;
  Menu *GetCurrentMenu();

  // update the list of custom sounds
  void UpdateCustomRadio() const;

  void SetMode(UIMode mode) {_mode = mode;}
	void ToggleSelection(AIGroup *grp, int index);

	Target *CheckCursorTarget(
		Vector3 &itPos, Vector3Par cursorDir,
		const Camera &camera, CameraType cam, bool knownOnly, bool includeBuildings
	);

	void IssueVCommand(EntityAI *vehicle, VCommand cmd);
	bool IssueCommand(EntityAI *vehicle, Command::Message cmd = Command::NoCommand, Command::Discretion discretion=Command::Undefined, Command::Movement=Command::MoveUndefined);
  void IssueWatchDir(); // watch in direction (given by the cursor position)
	void IssueWatchAround(AIGroup *grp); // watch around
  void IssueVWatchAround(Transport *vehicle); // watch around
	void IssueEngage(AIGroup *grp); // engage current target
	void IssueFire(AIGroup *grp); // fire at current target
	void IssueWatchAuto(AIGroup *grp); // no target
  void IssueVWatchAuto(Transport *vehicle); // no target
	void IssueWatch(AIGroup *grp); // direction
	void IssueVWatchTarget(Transport *vehicle, MenuItem &item); // target from the list
	void IssueWatchTarget(AIGroup *grp, MenuItem &item); // target from the list
	void IssueVMove(Transport *vehicle);
	void IssueMove(AIGroup *grp);
	void IssueAttack(AIGroup *grp, int tgt);
  void IssueGetIn(AIGroup *grp, OLinkPermNOArray(AIUnit) &units, Transport *veh, int pos);
	void IssueAction(AIGroup *grp, MenuItem &item);
  void IssueHeal();
  void IssueRepair();
  void IssueTakeBackpack();
  void IssueAssemble();
  void IssueDisassemble();
	void SetSemaphore(AIBrain *unit, AI::Semaphore status);
	void SetBehaviour(CombatMode mode);
	void SetUnitPosition(AIBrain *unit, UnitPosition status);
	void SetFormationPos(AIBrain *unit, AI::FormationPos status);

	void SendFireReady(AIBrain *unit, bool ready);
	void SendAnswer(AIBrain *unit, AI::Answer answer);
	void SendConfirm(AIBrain *unit);
	void SendRepeat(AIBrain *unit);

	void SendKilled(AIBrain *unit, OLinkPermNOArray(AIUnit) &list); // report unit is killed
	void SendResourceState(AIBrain *unit,AI::Answer answer); // report some state
	void SendObjectDestroyed(AIBrain *unit,AIGroup *grp);

	bool CheckJoin(AIGroup *grp);

	void ProcessMenu(CameraType cam);
	bool ProcessMenuItem(MenuItem* item, bool &resetMenu, bool actionKey);

  void AssignTeam(AIBrain * agent, Team team);

  void ProcessActions(AIBrain *unit);
  void SelectPriorCommand(AIBrain *agent, bool vehicleCommander);
  /// handle instant team switching (prev / next person)
  bool ProcessTeamSwitch();
	Menu *PreviewMenu();
#if _VBS3 // command mode in RTE
  void DrawMenu(bool isInEditor = false);
#else
	void DrawMenu();
#endif
  void DrawMenu(Menu *menu, float tmX, float tmY, float &tmW, float tmH, float alpha);
  void DrawUnits(Menu *menu, float tmX, float tmY, float &tmW, float tmH, float alpha);
  void DrawSingleUnits(Menu *menu, float tmX, float tmY, float &tmW, float tmH, float alpha);
	Ref<INode> CreateUnitDesc(int id, AIUnit *u);
	Ref<INode> CreateVehicleDesc(RString text, Transport *veh);
  Ref<INode> CreateTeamDesc(RString text, Team team);
	void DrawTankDirection(const Camera &camera);
	PackedColor ColorFromHit(float hit);
	void DrawTacticalDisplay
	(
		const Camera &camera, AIBrain *unit, const TargetList &list
	);
  void DrawRadarObject(Entity *entity,Entity *owner, int w, int h, float distance, MipInfo mip, PackedColor color);
  void DrawRadar
    (
    const Camera &camera, AIBrain *unit, const TargetList &list, Texture *background
    );
#if _VBS3_LASE_LEAD
  void DrawLaserRange(const TurretContext &context);
#endif
  void DrawCompass(EntityAIFull *vehicle);
	void DrawUnitInfo(EntityAIFull *vehicle);
  void DrawWeaponInfo(const Camera &camera);
	void DrawGroupDir(const Camera &camera, AIGroup *grp);
	void DrawHint();
	void DrawTaskHint();
	void DrawGroupUnit(AIUnit *u, UnitDescription &info, float xScreen, float yScreen, float alpha, int align, float width, float height);
	void DrawGroupInfo(EntityAI *vehicle);
  void DrawHCInfo(EntityAI *vehicle);
  void DrawHCGroup(int index, RString name,PackedColor color, float xScreen, float yScreen, float alpha, int align, bool selected);
  void DrawCursors(const Camera &camera, bool enabled, bool td);
  void DrawCursorWp(const Camera &camera, bool td, float alphaCursor, float alphaText, float age);
	bool DrawMouseCursor(const Camera &camera, AIBrain *unit, bool td);
  void CheckLockDelay(AIBrain *unit);
	bool DrawTargetInfo(
		const Camera &camera, AIBrain *unit, Vector3Par dir,
		float cursorSize,
		Texture *cursor, const AutoArray<CursorTextureSection> *sections, float offset, Texture *cursor2,
		PackedColor color, int shadow, float cursorA, float cursor2A,
		const Target *target, bool drawHousePos,
		bool info, bool extended, bool td
	);

  void DrawTargetLine( Vector3Par wPos, const Camera &camera, int mx, int mw, int my, int mh, PackedColor color );
  void DrawTargetLine( Vector3Par wPosB, Vector3Par wPosE, const Camera &camera, PackedColor color );

  void DrawCursor(
		const Camera &camera, EntityAI *vehicle,
		Vector3Val dir, float size,
		Texture *texture, const AutoArray<CursorTextureSection> *sections, float offsetX,float offsetY, float width, float height,
		PackedColor color, bool drawInTD, int shadow, bool drawArrows = true,
		PackedColor colorText = PackedWhite, CursorTexts texts = CursorTexts(), bool fadeToCenter = false
	);
  void DrawCursorDetail(
    const Camera &camera, EntityAI *vehicle,
    Vector3Val dir, float size,
    Texture *texture,  float offsetX,float offsetY, float width, float height,
    PackedColor color,  int shadow, bool drawOnSide = true, bool fadeToCenter = false
    );

  Point2DFloat WorldToScreen
    (
    const Camera &camera, Vector3Val dir, float size
    );
 
	void DrawCommand(const Command &cmd, AIUnit *unit, const Camera &camera, bool td, float alphaCursor, float alphaText, float age);

	void BackupTargets(TargetBackupList &targets);
	void CollectActions(const TargetBackupList &targets, UIActions &actions, bool optimize = true);

  //! process group maker events
  void OnGroupMarkerClick(AIGroup *group,int RMB, float x, float y, int waypoint);

  //! process group maker events
  void OnGroupMarkerEvent(AIGroup *group, float x, float y, GameValue GGroupMarkerEvent, int waypoint);

	const IAttribute *FindMenuAttribute(RString name) const;

  // send command, either locally or via network
  void SendCommandHelper(Transport *transport, Person *receiver, SimpleCommand cmd);
	
  /// updates scale coef for weapon cursor (fading to normal state)
  void UpdateWeaponCursorScaleCoef(float deltaT);

#if _VBS3 // command mode in RTE
  void UpdateTMTime() {_tmTime = Glob.uiTime;}
#endif

// cfg parameters
private:
  /// where to align
  int tmAlign;
  /// hide animation
  int _tmHide;
	float tmX;
#if _VBS3 // command mode in RTE
  float tmEditorY;
#endif
  float tmWMin;
  float tmWMax;
	float tmY;
	float tmH;
	float tmX2;
	float tmY2;
	float tmW2;
	float tmH2;
	float tankX;
	float tankY;
	float tankW;
	float tankH;
	float tdX;
	float tdY;
	float tdW;
	float tdH;
  float radarX;
  float radarY;
  float radarW;
  float radarH;
  int   radarShadow;
	float coX;
	float coY;
	float coW;
	float coH;
  int   coShadow;
	float giX;
	float giY;
	float giW;
	float giH;
	float piX;
	float piY;
	float piW;
	float piH;
	float uiX;
	float uiY;
	float uiW;
	float uiH;
	float gd1X;
	float gd1Y;
	float gd1W;
	float gd1H;
  float gd2X;
  float gd2Y;
  float gd2W;
  float gd2H;
	float ppicW;
	float ppicH;
	float piSignH;
	float piSignSW;
	float piSignGW;
	float piSignUW;
	float piSideH;
	float piSideW;
	float barH;
	float hbarW;
	float abarW;
	float fbarW;
	float ebarW;
	float tdCurW;
	float tdCurH;
	float curSignH;
	float curSignSW;
	float curSignGW;
	float curSignUW;
	float semW;
	float semH;
	float actW;
	float actH;
	float actMin;
	float actMax;
	RString tdName;
	RString giName;
	RString piName;
	RString uiName;
  float unfocusMenuAlpha;
	PackedColor bgColor;
	PackedColor bgColorCmd;
	PackedColor bgColorHelp;
	PackedColor ftColor;
	PackedColor menuCheckedColor;
	PackedColor menuEnabledColor;
	PackedColor menuDisabledColor;
	PackedColor menuSelectedColor;
  PackedColor menuSelectedTextColor;
	PackedColor friendlyColor;
	PackedColor enemyColor;
	PackedColor neutralColor;
	PackedColor civilianColor;
	PackedColor unknownColor;
	PackedColor cameraColor;
	PackedColor barBgColor;
	PackedColor barGreenColor;
	PackedColor barYellowColor;
	PackedColor barRedColor;
	PackedColor barBlinkOnColor;
	PackedColor barBlinkOffColor;
	PackedColor ebarColor;
	PackedColor uiColorNone;
	PackedColor uiColorNormal;
	PackedColor uiColorSelected;
  PackedColor uiColorSelecting;
	PackedColor uiColorPlayer;
	PackedColor pictureColor;
	PackedColor pictureProblemsColor;
  int pictureShadow;
	PackedColor cursorColor;
	PackedColor cursorBgColor;
	PackedColor cursorLockColor;
	PackedColor compassColor;
	PackedColor compassDirColor;
	PackedColor compassTurretDirColor;
	PackedColor tdCursorColor;
  int tdCurShadow;
	PackedColor holdFireColor;
	PackedColor capBgColor;
	PackedColor capFtColor;
	PackedColor capLnColor;
  int capShadow;
	PackedColor enemyActColor;
	PackedColor timeColor;
	PackedColor meColor;
	PackedColor selectColor;
	PackedColor leaderColor;
	PackedColor missionColor1;
  PackedColor missionColor2;
  PackedColor unitUnconsciousColor;

  float stallWarning;

  PackedColor customMarkColor;

	PackedColor msg1Color;
	PackedColor msg2Color;
	PackedColor msg3Color;
  int msgShadow;

	PackedColor tankColor;
  int tankShadow;
	PackedColor tankColorHalfDammage;
	PackedColor tankColorFullDammage;

	PackedColor teamColors[NTeams];

	PackedColor dragColor;

  float blinkingSpeed;

	float cursorDim;
  int cursorShadow;
  int cursorInfoShadow;
	float messagesDim;
	float groupInfoDim;
	float meDim;
	float meDimStartTime;
	float meDimEndTime;
	float cmdDimStartTime;
	float cmdDimEndTime;
	float targetDimStartTime;
	float targetDimEndTime;
	float groupDirDimStartTime;
	float groupDirDimEndTime;
  int groupDirShadow;
	float formDimStartTime;
	float formDimEndTime;
	float piDimStartTime;
	float piDimEndTime;
	float hintDimStartTime;
	float hintDimEndTime;
  float taskHintDimStartTime;
  float taskHintDimEndTime;
  float taskHintDimShowTime;
  float medicDimEndTime;

	// menu
	float menuHideTime;
	Ref<Font> _menuFont;
	float _menuSize;
  int _menuShadow;

  /// how long to wait prior quick menu is displayed
  float _quickMenuDelay;

	// group info
	Ref<Font> _uiUnitIDFont;
	float _uiUnitIDSize;
  int _uiUnitIDShadow;
	Ref<Font> _uiCommandFont;
	float _uiCommandSize;
  int _uiCommandShadow;
  Ref<Font> _uiHCGroupFont;
  float _uiHCGroupSize;
  int  _uiHCGroupShadow;
  Ref<Font> _uiVehicleNumberFont;
  float _uiVehicleNumberSize;
  int _uiVehicleNumberShadow;

  float _uiUnitIDX;
  float _uiUnitIDY;
  float _uiHCGroupX;
  float _uiHCGroupY;
  float _uiCommandX;
  float _uiCommandY;
  float _uiVehicleNumberX;
  float _uiVehicleNumberY;


	// cursor
	Ref<Font> _cursorFont;
	float _cursorSize;

  //HC command cursors
  Ref<Texture> _hcGroupSelected;
  Ref<Texture> _hcGroupSelectable;
  Ref<Texture> _hcWaypoint;
  float _hcGroupSelectedSize;
  float _hcGroupSelectableSize;
  float _hcGroupWaypointSize;
  float _maxHCDistanceAlphaEnd;
  float _maxHCDistanceAlphaStart;
  float _HC3DGroupAlpha;

  int _hcGroupSelectableShadow;
  int _hcGroupSelectedShadow;

#if PROTECTION_ENABLED
	// fade
	Ref<Font> _fadeFont;
	float _fadeSize;
  int _fadeShadow; 
#endif

	Ref<Texture> _iconMe;
	Ref<Texture> _iconSelect;
	Ref<Texture> _iconLeader;
	Ref<Texture> _iconMission;
  float _iconMissionMaxFade;
  Ref<Texture> _iconCustomMark;
  Ref<Texture> _iconUnitUnconscious;

  Ref<Texture> _iconBoard;
  Ref<Texture> _iconBoardIn;
  Ref<Texture> _iconBoardOut;
  Ref<Texture> _iconAttack;
  Ref<Texture> _iconMove;
  Ref<Texture> _iconJoin;
  Ref<Texture> _iconHealAt;
  Ref<Texture> _iconRepairAt;
  Ref<Texture> _iconRearmAt;
  Ref<Texture> _iconRefuelAt;
  Ref<Texture> _iconSupport;
  Ref<Texture> _iconInFormation;
  Ref<Texture> _iconCallSupport;

  PackedColor _iconBoardColor;
  PackedColor _iconAttackColor;
  PackedColor _iconMoveColor;
  PackedColor _iconJoinColor;
  PackedColor _iconHealAtColor;
  PackedColor _iconRepairAtColor;
  PackedColor _iconRearmAtColor;
  PackedColor _iconRefuelAtColor;
  PackedColor _iconSupportColor;
  PackedColor _iconInFormationColor;
  
  Ref<Texture> _cmdbarBackground;
  Ref<Texture> _cmdbarNumber;
  Ref<Texture> _cmdbarCommander;
  Ref<Texture> _cmdbarDontFire;
  Ref<Texture> _cmdbarCommand;
  Ref<Texture> _cmdbarPlayer;
  Ref<Texture> _cmdbarSelected;
  Ref<Texture> _cmdbarFocus;

  Ref<Texture> _cmdbarMCareless;
  Ref<Texture> _cmdbarMSafe;
  Ref<Texture> _cmdbarMAware;
  Ref<Texture> _cmdbarMCombat;
  Ref<Texture> _cmdbarMStealth;

  float _cmdbarModeX;
  float _cmdbarModeY;
  float _cmdbarModeW;
  float _cmdbarModeH;
  int _cmdbarModeShadow;

	Ref<Texture> _imageSemaphore;
	Ref<Texture> _imageBar;

	// animated with main turret
	Ref<Texture> _imageTurret;
	Ref<Texture> _imageGun;

	// animated with observer turret
	Ref<Texture> _imageObsTurret;

	// animated with hull
	Ref<Texture> _imageHull;
	Ref<Texture> _imageEngine;
	Ref<Texture> _imageLTrack;
	Ref<Texture> _imageRTrack;

  //hit zones
  Ref<Texture> _hitZones;
  int _hitShadow;

  // vehicle movement
  Ref<Texture> _imageMoveStop;
  Ref<Texture> _imageMoveBack;
  Ref<Texture> _imageMoveForward;
  Ref<Texture> _imageMoveFast;
  Ref<Texture> _imageMoveLeft;
  Ref<Texture> _imageMoveRight;
  Ref<Texture> _imageMoveAuto;

	// group direction
	Ref<Texture> _imageGroupDir;

	// pictures in group info
	Ref<Texture> _imageDefaultWeapons;
	Ref<Texture> _imageNoWeapons;
  Ref<Texture> _imageCommander;
  Ref<Texture> _imageDriver;
  Ref<Texture> _imageGunner;
  Ref<Texture> _imageCargo;
  Ref<Texture> _imagePrevPage;
  Ref<Texture> _imageNextPage;
  Ref<Texture> _imageSpecialRole;

	// Connection Lost message
	float _clX;
	float _clY;
	float _clW;
	float _clH;
	Ref<Font> _clFont;
	float _clSize;
	PackedColor _clColor;
  int _clShadow;

  //@{ peripheral vision  
	Ref<Texture> _cueTexture;
	PackedColor _cueColor;
	PackedColor _cueEnemyColor;
	PackedColor _cueFriendlyColor;
	Ref<Texture> _bloodTexture;
  PackedColor _bloodColor;
  int _cueShadow;
	//@}

  /// target on the tactical display
  Ref<Texture> _radarGroundTargetTexture;
  Ref<Texture> _radarAirTargetTexture;
  ///radar textures
  Ref<Texture> _radarAirBackgroundTexture;
  Ref<Texture> _radarTankBackgroundTexture;
  Ref<Texture> _radarAirDangerSector;
  Ref<Texture> _radarIncommingMissile;
  Ref<Texture> _radarVehicleTarget;
  Ref<Texture> _radarTargetingEnemy;
  Ref<Texture> _radarFOV;
  
  PackedColor _turretFOVColor;
  PackedColor _turretPlayerFOVColor;
  PackedColor _radarLockDangerColor;
  PackedColor _radarIncommingDangerColor;

  PackedColor _weaponReadyColor;
  PackedColor _weaponPrepareColor;
  PackedColor _weaponUnloadColor;

  //command bar  
  float _prevPageX;
  float _prevPageY;
  float _prevPageW;
  float _prevPageH;
  PackedColor _prevPageColor;
  int _prevPageShadow;

  float _nextPageX;
  float _nextPageY;
  float _nextPageW;
  float _nextPageH;
  PackedColor _nextPageColor;
  int _nextPageShadow;

  float _unitBackgroundX;
  float _unitBackgroundY;
  float _unitBackgroundW;
  float _unitBackgroundH;
  int _unitBackgroundShadow;

  float _unitIconX;
  float _unitIconY;
  float _unitIconW;
  float _unitIconH;
  int _unitIconShadow;
  PackedColor _unitIconColor;
  PackedColor _unitIconColorPlayer;
  PackedColor _unitIconColorNoAmmo;
  PackedColor _unitIconColorWounded;
  PackedColor _unitIconColorNoFuel;
  PackedColor _unitIconColorWounded2;
  float _groupIconX;
  float _groupIconY;
  float _groupIconW;
  float _groupIconH;
  int _groupIconShadow;

  float _semaphoreX;
  float _semaphoreY;
  float _semaphoreW;
  float _semaphoreH;
  PackedColor _semaphoreColor;
  int _semaphoreShadow;
    
  float  _commandBackgroundX;
  float  _commandBackgroundY;
  float  _commandBackgroundW;
  float  _commandBackgroundH;
  PackedColor  _commandBackgroundColor;
  int _commandBackgroundShadow;

  float _unitSpecialRoleX;
  float _unitSpecialRoleY;
  float _unitSpecialRoleW;
  float _unitSpecialRoleH;
  PackedColor _unitSpecialRoleColor;
  int _unitSpecialRoleShadow;

  float _vehicleNumberBackgroundX;
  float _vehicleNumberBackgroundY;
  float _vehicleNumberBackgroundW;
  float _vehicleNumberBackgroundH;
  PackedColor _vehicleNumberBackgroundColor;
  int _vehicleNumberBackgroundShadow;

  float _unitRoleX;
  float _unitRoleY;
  float _unitRoleW;
  float _unitRoleH;
  PackedColor _unitRoleColor;
  int _unitRoleShadow;

  float _unitNumberBackgroundX;
  float _unitNumberBackgroundY;
  float _unitNumberBackgroundW;
  float _unitNumberBackgroundH;
  PackedColor _unitNumberBackgroundColor;
  int _unitNumberBackgroundShadow;

  RString _FLIRModeNames[16];

  // throw cursor scaling
  float _throwCursorMinScale;
  float _throwCursorMaxScale;
  float _throwCursorFadeSpeed;
};

#endif
