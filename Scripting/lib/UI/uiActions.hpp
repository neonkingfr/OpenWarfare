#ifdef _MSC_VER
#pragma once
#endif

#ifndef _UI_ACTIONS_HPP
#define _UI_ACTIONS_HPP

//! action types
/*!
\patch_internal 1.01 Date 06/15/2001 by Jirka
- added ATUser action type
\patch 1.75 Date 3/8/2002 by Jirka
- Added: user actions assigned to vehicle type (ATUserType)
*/

#define ACTIONS_DEFAULT(XX) \
  XX(None, ActionBasic) \
  XX(GetInCommander, ActionBasic) \
  XX(GetInDriver, ActionBasic) \
  XX(GetInPilot, ActionBasic) \
  XX(GetInGunner, ActionBasic) \
  XX(GetInCargo, ActionIndex) \
  XX(GetInTurret, ActionTurret) \
  XX(Heal, ActionBasic) \
  XX(Repair, ActionBasic) \
  XX(Refuel, ActionBasic) \
  XX(Rearm, ActionBasic) \
  XX(GetOut, ActionBasic) \
  XX(LightOn, ActionBasic) \
  XX(LightOff, ActionBasic) \
  XX(EngineOn, ActionBasic) \
  XX(EngineOff, ActionBasic) \
  XX(SwitchWeapon, ActionWeaponIndex) \
  XX(HideWeapon, ActionWeaponIndex) \
  XX(SwitchMagazine, ActionWeaponIndex) \
  XX(UseWeapon, ActionWeaponIndex) \
  XX(LoadMagazine, ActionMagazineAndMuzzle) \
  XX(LoadOtherMagazine, ActionMagazineAndMuzzle) \
  XX(LoadEmptyMagazine, ActionMagazineAndMuzzle) \
  XX(TakeWeapon, ActionWeapon) \
  XX(TakeDropWeapon, ActionWeapon) \
  XX(TakeMagazine, ActionMagazineType) \
  XX(TakeDropMagazine, ActionMagazineType) \
  XX(TakeFlag, ActionBasic) \
  XX(ReturnFlag, ActionBasic) \
  XX(TurnIn, ActionBasic) \
  XX(TurnOut, ActionBasic) \
  XX(WeaponInHand, ActionWeapon) /* take weapon in hands */ \
  XX(WeaponOnBack, ActionWeapon) /* take weapon in hands */ \
  XX(SitDown, ActionBasic) /* some special actions (can be used in safe mode) */ \
  XX(Land, ActionBasic) \
  XX(CancelLand, ActionBasic) \
  XX(Eject, ActionBasic) \
  XX(MoveToDriver, ActionBasic) \
  XX(MoveToPilot, ActionBasic) \
  XX(MoveToGunner, ActionBasic) \
  XX(MoveToCommander, ActionBasic) \
  XX(MoveToCargo, ActionIndex) \
  XX(MoveToTurret, ActionTurret) \
  XX(HideBody, ActionBasic) \
  XX(TouchOff, ActionBasic) \
  XX(StartTimer, ActionObject) \
  XX(SetTimer, ActionObject) \
  XX(Deactivate, ActionObject) \
  XX(NVGoggles, ActionBasic) \
  XX(NVGogglesOff, ActionBasic) \
  XX(ManualFire, ActionBasic) \
  XX(ManualFireCancel, ActionBasic) \
  XX(AutoHover, ActionBasic) \
  XX(AutoHoverCancel, ActionBasic) \
  XX(StrokeFist, ActionBasic) \
  XX(StrokeGun, ActionBasic) \
  XX(LadderUp, ActionLadderPos) \
  XX(LadderDown, ActionLadderPos) \
  XX(LadderOnDown, ActionLadderPos) \
  XX(LadderOnUp, ActionLadderPos) \
  XX(LadderOff, ActionLadder) \
  XX(FireInflame, ActionBasic) \
  XX(FirePutDown, ActionBasic) \
  XX(LandGear, ActionBasic) \
  XX(LandGearUp, ActionBasic) \
  XX(FlapsDown, ActionBasic) \
  XX(FlapsUp, ActionBasic) \
  XX(Salute, ActionBasic) \
  XX(ScudLaunch, ActionBasic) \
  XX(ScudStart, ActionBasic) \
  XX(ScudCancel, ActionBasic) \
  XX(User, ActionIndex) \
  XX(DropWeapon, ActionWeapon) \
  XX(PutWeapon, ActionWeapon) \
  XX(DropMagazine, ActionMagazineType) \
  XX(PutMagazine, ActionMagazineType) \
  XX(UserType, ActionIndex) \
  XX(HandGunOn, ActionWeapon) \
  XX(HandGunOnStand, ActionWeapon) \
  XX(HandGunOff, ActionWeapon) \
  XX(HandGunOffStand, ActionWeapon) \
  XX(TakeMine, ActionBasic) \
  XX(DeactivateMine, ActionBasic) \
  XX(UseMagazine, ActionMagazine) \
  XX(IngameMenu, ActionBasic) \
  XX(CancelTakeFlag, ActionBasic) \
  XX(CancelAction, ActionBasic) \
  XX(MarkEntity, ActionBasic) \
  XX(MarkWeapon, ActionBasic) \
  XX(TeamSwitch, ActionBasic) \
  XX(Gear, ActionBasic) \
  XX(HealSoldier, ActionBasic) \
  XX(Surrender, ActionBasic)\
  XX(GetOver, ActionBasic) \
  XX(FirstAid, ActionBasic) \
  XX(OpenBag, ActionBasic) \
  XX(TakeBag, ActionBasic) \
  XX(DropBag, ActionWeapon) \
  XX(PutBag, ActionBasic) \
  XX(AddBag, ActionWeapon) \
  XX(Assemble, ActionBasic) \
  XX(DisAssemble, ActionBasic) \
  XX(IRLaserOn, ActionBasic) \
  XX(IRLaserOff, ActionBasic) \
  XX(GunLightOn, ActionBasic) \
  XX(GunLightOff, ActionBasic) \
  XX(ArtilleryComputer, ActionBasic) \
  XX(RepairVehicle, ActionBasic)

#if _ENABLE_CONVERSATION
#define ACTIONS_CONVERSATION(XX) \
  XX(Talk, ActionBasic) \
  XX(Tell, ActionTell) \

#endif    

#if _VBS3 && _ENABLE_CARRY_BODY //BattlefieldClearance
#define ACTIONS_CARRY_BODY(XX) \
  XX(CarryBody, ActionBasic) \
  XX(PutWeaponOnBack, ActionWeapon) \
  XX(CaptureUnit, ActionBasic) \
  XX(ReleaseUnit, ActionBasic) \
  XX(Handcuff, ActionBasic) \
  XX(ReleaseHandcuff, ActionBasic) 

#endif

#if _VBS2
#define ACTIONS_VBS2(XX) \
  XX(InteractWithVehicle, ActionBasic) \
  XX(TogglePersonalItems, ActionBasic) \
  XX(QuickEnterVehicle, ActionBasic)
#endif    

#define ACTIONS_ENUM(name, type) AT##name,

#ifndef DECL_ENUM_UI_ACTION_TYPE
#define DECL_ENUM_UI_ACTION_TYPE
DECL_ENUM(UIActionType)
#endif
DEFINE_ENUM_BEG(UIActionType)
  ACTIONS_DEFAULT(ACTIONS_ENUM)
#if _ENABLE_CONVERSATION
  ACTIONS_CONVERSATION(ACTIONS_ENUM)
#endif    
#if _VBS3 && _ENABLE_CARRY_BODY //BattlefieldClearance
  ACTIONS_CARRY_BODY(ACTIONS_ENUM)
#endif
#if _VBS2
  ACTIONS_VBS2(ACTIONS_ENUM)
#endif
  NUIActionType
DEFINE_ENUM_END(UIActionType)

class InGameUI;
class INode;
typedef AutoArray<RefR<INode>, MemAllocLocal<RefR<INode>, 16> > UIActionParams;

/// basic action type
class Action : public RemoveLLinks
{
protected:
  UIActionType _type;
  TargetId _target;
  bool _fromGUI; ///< true if action was create like a response on player action in GUI, false otherwise. 
  bool _processed; ///< helper variable set when the action was processed

public:
  Action(UIActionType type) : _type(type) {_fromGUI = false; _processed = false;}
  Action(UIActionType type, TargetType *target) : _type(type), _target(target) {_fromGUI = false; _processed = false;}
  virtual ~Action() {}

  UIActionType GetType() const {return _type;}
  TargetType *GetTarget() const {return _target;}
  void SetTarget(TargetType *target) {_target = target;}
  bool IsFromGUI() const {return _fromGUI;}
  void SetFromGUI(bool set) {_fromGUI = set;}
  bool IsProcessed() const {return _processed;}
  void SetProcessed(bool set) {_processed = set;}

  virtual bool HasEqualType(const Action *to) const;
  virtual int Compare(const Action *with) const; // for sorting
  bool IsEqual(const Action *to) const {return to && GetTarget() == to->GetTarget() && HasEqualType(to);}
  
  virtual Ref<Action> Copy() const = 0;

  virtual bool Process(AIBrain *unit) const;

  RefR<INode> GetText(AIBrain *unit) const;
  RefR<INode> GetTextDefault(AIBrain *unit) const;
  RString GetTextSimple(AIBrain *unit) const;

  // helper for "action" scripting function
  virtual bool SetParams(const GameState *state, GameValuePar oper);

#if _VBS3
  //executes a scripted action which is stores in the statement
  bool PerformScripted(AIBrain *unit) const;
#endif

  static Action *CreateObject(ParamArchive &ar);
  virtual LSError Serialize(ParamArchive &ar); // needed for ActivityUIAction

protected:
  virtual bool GetParams(UIActionParams &params, AIBrain *unit) const;
};

Action *CreateAction(UIActionType type);

#include <Es/Memory/normalNew.hpp>

/// action with no additional arguments
class ActionBasic : public Action
{
  typedef Action base;

public:
  ActionBasic(UIActionType type) : base(type) {}
  ActionBasic(UIActionType type, TargetType *target) : base(type, target) {}

  // Action implementation
  virtual Ref<Action> Copy() const;

  virtual bool GetParams(UIActionParams &params, AIBrain *unit) const;
  virtual bool Process(AIBrain *unit) const;

  USE_FAST_ALLOCATOR
};

/// action referencing to turret
class ActionTurret : public Action
{
  typedef Action base;

protected:
  OLinkPermNO(Turret) _turret;

public:
  ActionTurret(UIActionType type) : base(type) {}
  ActionTurret(UIActionType type, TargetType *target, Turret *turret)
    : base(type, target), _turret(turret) {}

  Turret *GetTurret() const {return _turret;}

  // Action implementation
  virtual Ref<Action> Copy() const;

  virtual bool HasEqualType(const Action *to) const;
  virtual int Compare(const Action *with) const; // for sorting

  virtual bool GetParams(UIActionParams &params, AIBrain *unit) const;
  virtual bool Process(AIBrain *unit) const;

  virtual bool SetParams(const GameState *state, GameValuePar oper);

  virtual LSError Serialize(ParamArchive &ar); // needed for ActivityUIAction

  USE_FAST_ALLOCATOR
};

/// action referencing to pipe bomb
class ActionObject : public Action
{
  typedef Action base;

protected:
  OLinkPerm<Object> _object;

public:
  ActionObject(UIActionType type) : base(type) {}
  ActionObject(UIActionType type, TargetType *target, Object *object)
    : base(type, target), _object(object) {}

  Object *GetObject() const {return _object;}

  // Action implementation
  virtual Ref<Action> Copy() const;

  virtual bool HasEqualType(const Action *to) const;
  virtual int Compare(const Action *with) const; // for sorting

  virtual bool GetParams(UIActionParams &params, AIBrain *unit) const;
  virtual bool Process(AIBrain *unit) const;

  virtual bool SetParams(const GameState *state, GameValuePar oper);

  virtual LSError Serialize(ParamArchive &ar); // needed for ActivityUIAction

  USE_FAST_ALLOCATOR
};

/// action referencing to weapon
class ActionWeapon : public Action
{
  typedef Action base;

protected:
  RString _weapon;
  bool _backpack;

public:
  ActionWeapon(UIActionType type) : base(type) {}
  ActionWeapon(UIActionType type, TargetType *target, RString weapon, bool backpack) : base(type, target), _weapon(weapon), _backpack(backpack) {}

  RString GetWeapon() const {return _weapon;}
  bool UseBackpack() const {return _backpack;}

  // Action implementation
  virtual Ref<Action> Copy() const;

  virtual bool HasEqualType(const Action *to) const;
  virtual int Compare(const Action *with) const; // for sorting

  virtual bool GetParams(UIActionParams &params, AIBrain *unit) const;
  virtual bool Process(AIBrain *unit) const;

  virtual bool SetParams(const GameState *state, GameValuePar oper);

  virtual LSError Serialize(ParamArchive &ar); // needed for ActivityUIAction

  USE_FAST_ALLOCATOR
};

/// action referencing to magazine type
class ActionMagazineType : public Action
{
  typedef Action base;

protected:
  RString _magazineType;
  bool _backpack;

public:
  ActionMagazineType(UIActionType type) : base(type) {}
  ActionMagazineType(UIActionType type, TargetType *target, RString magazineType, bool backpack)
    : base(type, target), _magazineType(magazineType), _backpack(backpack) {}

  RString GetMagazineType() const {return _magazineType;}
  bool UseBackpack() const {return _backpack;}

  // Action implementation
  virtual Ref<Action> Copy() const;

  virtual bool HasEqualType(const Action *to) const;
  virtual int Compare(const Action *with) const; // for sorting

  virtual bool GetParams(UIActionParams &params, AIBrain *unit) const;
  virtual bool Process(AIBrain *unit) const;

  virtual bool SetParams(const GameState *state, GameValuePar oper);

  virtual LSError Serialize(ParamArchive &ar); // needed for ActivityUIAction

  USE_FAST_ALLOCATOR
};


/// action referencing to magazine slot
class ActionWeaponIndex : public Action
{
  typedef Action base;

protected:
  OLinkPermO(Person) _gunner;
  int _index;

public:
  ActionWeaponIndex(UIActionType type) : base(type) {_index = -1;}
  ActionWeaponIndex(UIActionType type, TargetType *target, Person *gunner, int index)
    : base(type, target), _gunner(gunner), _index(index) {}

  Person *GetGunner() const {return _gunner;}
  int GetWeaponIndex() const {return _index;}

  // Action implementation
  virtual Ref<Action> Copy() const;

  virtual bool HasEqualType(const Action *to) const;
  virtual int Compare(const Action *with) const; // for sorting

  virtual bool GetParams(UIActionParams &params, AIBrain *unit) const;
  virtual bool Process(AIBrain *unit) const;

  virtual bool SetParams(const GameState *state, GameValuePar oper);

  virtual LSError Serialize(ParamArchive &ar); // needed for ActivityUIAction

  USE_FAST_ALLOCATOR
};

/// action with generic index (used for user actions)
class ActionIndex : public Action
{
  typedef Action base;

protected:
  int _index;

public:
  ActionIndex(UIActionType type) : base(type) {_index = -1;}
  ActionIndex(UIActionType type, TargetType *target, int index) : base(type, target), _index(index) {}

  int GetIndex() const {return _index;}

  // Action implementation
  virtual Ref<Action> Copy() const;

  virtual bool HasEqualType(const Action *to) const;
  virtual int Compare(const Action *with) const; // for sorting

  virtual bool GetParams(UIActionParams &params, AIBrain *unit) const;
  virtual bool Process(AIBrain *unit) const;

  virtual bool SetParams(const GameState *state, GameValuePar oper);

  virtual LSError Serialize(ParamArchive &ar); // needed for ActivityUIAction

  USE_FAST_ALLOCATOR
};

/// action referencing to magazine
class ActionMagazine : public Action
{
  typedef Action base;

protected:
  OLinkPermO(Person) _gunner;
  int _creator;
  int _id;

public:
  ActionMagazine(UIActionType type) : base(type) {_creator = -1; _id = -1;}
  ActionMagazine(UIActionType type, TargetType *target, Person *gunner, int creator, int id)
    : base(type, target), _gunner(gunner), _creator(creator), _id(id) {}

  Person *GetGunner() const {return _gunner;}
  int GetCreator() const {return _creator;}
  int GetId() const {return _id;}

  // Action implementation
  virtual Ref<Action> Copy() const;

  virtual bool HasEqualType(const Action *to) const;
  virtual int Compare(const Action *with) const; // for sorting

  virtual bool GetParams(UIActionParams &params, AIBrain *unit) const;
  virtual bool Process(AIBrain *unit) const;

  virtual bool SetParams(const GameState *state, GameValuePar oper);

  virtual LSError Serialize(ParamArchive &ar); // needed for ActivityUIAction

  USE_FAST_ALLOCATOR
};

/// action referencing to magazine and muzzle
class ActionMagazineAndMuzzle : public ActionMagazine
{
  typedef ActionMagazine base;

protected:
  Ref<WeaponType> _weapon;
  Ref<MuzzleType> _muzzle;

public:
  ActionMagazineAndMuzzle(UIActionType type) : base(type) {}
  ActionMagazineAndMuzzle(UIActionType type, TargetType *target, Person *gunner, int creator, int id, WeaponType *weapon, MuzzleType *muzzle)
    : base(type, target, gunner, creator, id), _weapon(weapon), _muzzle(muzzle) {}

  WeaponType *GetWeapon() const {return _weapon;}
  MuzzleType *GetMuzzle() const {return _muzzle;}

  // Action implementation
  virtual Ref<Action> Copy() const;

  virtual bool HasEqualType(const Action *to) const;
  virtual int Compare(const Action *with) const; // for sorting

  virtual bool GetParams(UIActionParams &params, AIBrain *unit) const;
  virtual bool Process(AIBrain *unit) const;

  virtual bool SetParams(const GameState *state, GameValuePar oper);

  virtual LSError Serialize(ParamArchive &ar); // needed for ActivityUIAction

  USE_FAST_ALLOCATOR
};

/// action referencing to ladder
class ActionLadder : public Action
{
  typedef Action base;

protected:
  int _index;

public:
  ActionLadder(UIActionType type) : base(type) {_index = -1;}
  ActionLadder(UIActionType type, TargetType *target, int index) : base(type, target), _index(index) {}

  int GetLadder() const {return _index;}

  // Action implementation
  virtual Ref<Action> Copy() const;

  virtual bool HasEqualType(const Action *to) const;
  virtual int Compare(const Action *with) const; // for sorting

  virtual bool GetParams(UIActionParams &params, AIBrain *unit) const;
  virtual bool Process(AIBrain *unit) const;

  virtual bool SetParams(const GameState *state, GameValuePar oper);

  virtual LSError Serialize(ParamArchive &ar); // needed for ActivityUIAction

  USE_FAST_ALLOCATOR
};

/// action referencing to ladder position
class ActionLadderPos : public ActionLadder
{
  typedef ActionLadder base;

protected:
  int _pos;

public:
  ActionLadderPos(UIActionType type) : base(type) {_pos = -1;}
  ActionLadderPos(UIActionType type, TargetType *target, int index, int pos)
    : base(type, target, index), _pos(pos) {}

  int GetLadderPos() const {return _pos;}

  // Action implementation
  virtual Ref<Action> Copy() const;

  virtual bool HasEqualType(const Action *to) const;
  virtual int Compare(const Action *with) const; // for sorting

  virtual bool GetParams(UIActionParams &params, AIBrain *unit) const;
  virtual bool Process(AIBrain *unit) const;

  virtual bool SetParams(const GameState *state, GameValuePar oper);

  virtual LSError Serialize(ParamArchive &ar); // needed for ActivityUIAction

  USE_FAST_ALLOCATOR
};

#if _ENABLE_CONVERSATION

class KBMessageInfo;

/// action referencing to sentence
class ActionTell : public Action
{
  typedef Action base;

protected:
  /// UI text
  RString _text;
  /// message to tell
  Ref<KBMessageInfo> _message;

public:
  ActionTell(UIActionType type) : base(type) {}
  ActionTell(UIActionType type, TargetType *target, RString text, KBMessageInfo *message)
    : base(type, target), _text(text), _message(message) {}

  RString GetText() const {return _text;}
  const KBMessageInfo *GetMessage() const {return _message;}

  // Action implementation
  virtual Ref<Action> Copy() const;

  virtual bool HasEqualType(const Action *to) const;
  virtual int Compare(const Action *with) const; // for sorting

  virtual bool GetParams(UIActionParams &params, AIBrain *unit) const;
  virtual bool Process(AIBrain *unit) const;

  virtual bool SetParams(const GameState *state, GameValuePar oper);

  virtual LSError Serialize(ParamArchive &ar); // needed for ActivityUIAction

  USE_FAST_ALLOCATOR
};

#endif

#include <Es/Memory/debugNew.hpp>

class UIAction
{
public:
  /// action type and parameters  
  Ref<Action> action;

  /// exact action location
  Vector3 position;

  // UI flags
  float priority;
  bool show;
  bool showWindow;
  bool hideOnUse;
  bool highlight; ///< give player notice of this action
  UserAction shortcut;
  UITime created;

  UIAction();

  // action is needed now to decide type and 
  UIAction(Action *act);

  void Process(AIBrain *unit) const;

  bool IsEqual(const UIAction &to) const
  {
    return action->IsEqual(to.action);
  }
  bool HasEqualType(const UIAction &to) const
  {
    return action->HasEqualType(to.action);
  }
};
TypeIsMovable(UIAction)

/// description of action type, loaded from config
struct UIActionTypeDesc
{
  float priority;
  bool show;
  bool showWindow;
  bool hideOnUse;
  UserAction shortcut;
  RString text;
  RString textDefault;
  RString textSimple;

#if _VBS3 //used for scripted actions
  RString script;
#endif

  UIActionTypeDesc();
  void Load(ParamEntryPar cls);
};

class UIActions : public StaticArray<UIAction>
{
  typedef StaticArray<UIAction> base;
public:
  
  static UIActionTypeDesc actionTypes[NUIActionType];
  static void Load(ParamEntryPar cls);

  void Sort();
};

class UIActionsDrawable : public UIActions
{
  typedef UIActions base;

protected:
  Ref<Action> _selected;
  float _selectedPriority;

  AutoArray<UIAction> _newActions;
  bool _default;
  
  // default action parameters
  bool _showHint;
  bool _showNext;
  bool _showLine;
  Ref<Font> _defaultFont;
  float _defaultSize;
  PackedColor _defaultColor;
  Ref<Font> _nextFont;
  float _nextSize;
  float _xOffset;
  float _yOffset;
  float _xHotspot;
  float _yHotspot;
  float _relativeToCursor;
  Ref<Texture> _defaultBackground;
  int _defaultShadow;

  // actions menu parameters
  /// how the menu is aligned
  int _align;
  /// position of the menu
  float _x, _y;
  /// maximal number of rows
  int _rows;

  PackedColor _bgColor, _textColor, _selBgColor, _selColor;
  Ref<Font> _font;
  float _size;
  Ref<Texture> _background;

  int _shadow;

  bool _underlineSelected;

  /// scrolling arrows
  float _arrowWidth, _arrowHeight;
  Ref <Texture> _iconArrowUp, _iconArrowDown;

  UITime _dimStart;

public:
  UIActionsDrawable();

  void SelectPrev(bool cycle);
  void SelectNext(bool cycle);
  void ProcessAction(int index, AIBrain *unit, bool fromGUI = false, bool byShortcut=false);

  void OnDraw();

  void Refresh(bool user);
  void Hide();
  float GetAge() const;
  float GetAlpha() const;
  bool IsDefault() const {return _default;}
  bool IsListVisible() const;

  void Update(UIActions &src);

  int FindSelected();
};

#endif
