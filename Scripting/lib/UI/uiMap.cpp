// Implementation of main map display
#include "../wpch.hpp"
#include "uiMap.hpp"
//#include "win.h"
#include "../dikCodes.h"
#include "../person.hpp"
#include "../diagModes.hpp"
#include <El/Common/perfProf.hpp>

#include "resincl.hpp"
#include "../keyInput.hpp"
#include "../vkCodes.h"

#include "../landscape.hpp"
#include "../roads.hpp"

#include "../camera.hpp"
#include "../detector.hpp"

#include "../Shape/mapTypes.hpp"
#include "../gameStateExt.hpp"
#include "../paramArchiveExt.hpp"
#include "../fileLocator.hpp"
#include "../gameDirs.hpp"

#include <El/QStream/qbStream.hpp>
#include <El/XML/xml.hpp>

#include <El/Common/randomGen.hpp>
#include <El/ParamArchive/paramArchiveDb.hpp>
#include <El/Evaluator/express.hpp>

#include <Es/Algorithms/qsort.hpp>

#include <time.h>

#include <El/Enum/enumNames.hpp>

#include "chat.hpp"

#include "../txtPreload.hpp"

#include "../stringtableExt.hpp"
#include "../mbcs.hpp"

#include <Es/Strings/bString.hpp>
#include <ctype.h>

#include "../AI/aiRadio.hpp"

#include "uiActions.hpp"

#include "../keyInput.hpp"

#include "../saveGame.hpp"

#include "../progress.hpp"

#include "displayUI.hpp"
#include "missionDirs.hpp"

#include "../Network/network.hpp"

#include "../timeManager.hpp"

#include "uiViewport.hpp"

#include "../drawText.hpp"

/*!
\file
Implementation file for particular displays (maps, briefing, debriefing, mission editor).
*/

// static const int daysInMonth[]={31,28,31,30,31,30,31,31,30,31,30,31};
int GetDaysInMonth(int year, int month);

extern const float CameraZoom;
extern const float InvCameraZoom;

inline PackedColor ModAlpha( PackedColor color, float alpha )
{
  int a=toIntFloor(alpha*color.A8());
  saturate(a,0,255);
  return PackedColorRGB(color,a);
}

//LSError AbstractOptionsUI::Serialize(ParamArchive &ar) {return LSOK;}

LSError AbstractOptionsUI::Serialize(ParamArchive &ar)
{
  return LSOK;
}

///////////////////////////////////////////////////////////////////////////////
// Map (generic static control)

#define TO_INT(coef, arg) coef > 0 ? toIntFloor(arg) : toIntCeil(arg)

static void AddChar(BString<32> &buffer, char c)
{
  int n = strlen(buffer);
  if (n < 32 - 1)
  {
    buffer[n] = c;
    buffer[n + 1] = 0;
  }
}

static int Modulo(int &i, int base)
{
  int d = i >= 0 ? i / base : (i - base + 1) / base;
  int m = i - d * base;
  i = d;
  return m;
}

void GridFormat(BString<32> &buffer, RString format, int i)
{
  const char *first = NULL;
  for (const char *p=format; *p!=0; p++)
  {
    if (isalnum(*p))
    {
      first = p; break;
    }
  }
  if (!first) return;
  BString<32> revert;
  int cf = 0;
  const char *p;
  for (p=(const char *)format+format.GetLength()-1; p!=first; p--)
  {
    if (*p >= '0' && *p <= '9')
    {
      int m = Modulo(i, 10) + cf + *p - '0';
      int cf = m;
      m = Modulo(cf, 10);
      AddChar(revert, '0' + m);
    }
    else if (*p >= 'A' && *p <= 'J')
    {
      int m = Modulo(i, 10) + cf + *p - 'A';
      int cf = m;
      m = Modulo(cf, 10);
      AddChar(revert, 'A' + m);
    }
    else if (*p >= 'a' && *p <= 'j')
    {
      int m = Modulo(i, 10) + cf + *p - 'a';
      int cf = m;
      m = Modulo(cf, 10);
      AddChar(revert, 'a' + m);
    }
    else AddChar(revert, *p);
  }
  if (*p >= '0' && *p <= '9')
  {
    int m = Modulo(i, 10) + cf + *p - '0';
    int cf = m;
    m = Modulo(cf, 10);
    AddChar(revert, '0' + m);
  }
  else if (*p >= 'A' && *p <= 'Z')
  {
    int m = Modulo(i, 26) + cf + *p - 'A';
    int cf = m;
    m = Modulo(cf, 26);
    AddChar(revert, 'A' + m);
  }
  else if (*p >= 'a' && *p <= 'z')
  {
    int m = Modulo(i, 26) + cf + *p - 'a';
    int cf = m;
    m = Modulo(cf, 26);
    AddChar(revert, 'a' + m);
  }
  p--;
  for (; p>=(const char *)format; p--) AddChar(revert, *p);

  for (const char *p=revert+strlen(revert)-1; p>=(const char *)revert; p--) AddChar(buffer, *p);
}

static void AddChar(char *buffer, char c)
{
  int n = strlen(buffer);
  buffer[n] = c;
  buffer[n + 1] = 0;
}

void PositionToAA11(Vector3Val pos, char *buffer)
{
  float sizeLand = LandGrid * LandRange;
  *buffer = 0;

  if (!GWorld) return;
  const GridInfo *info = GWorld->GetGridInfo(0);
  if (!info) return;

  float offsetX = GWorld->GetGridOffsetX();
  float offsetY = GWorld->GetGridOffsetY();


  for (const char *p=info->format; *p!=0; p++)
  {
    if (*p == 'X' || *p == 'x')
    {
      float coefX = sizeLand * info->invStepX;
      int x = TO_INT(coefX, (pos.X() - offsetX) * info->invStepX);
      BString<32> buf;
      GridFormat(buf, info->formatX, coefX >= 0 ? x : x - 1);
      strcat(buffer, buf);
    }
    else if (*p == 'Y' || *p == 'y')
    {
      float coefY = sizeLand * info->invStepX;
      int y = TO_INT(coefY, (sizeLand - pos.Z() - offsetY) * info->invStepY);
      BString<32> buf;
      GridFormat(buf, info->formatY, coefY >= 0 ? y : y - 1);
      strcat(buffer, buf);
    }
    else AddChar(buffer, *p);
  }
}

void MapTypeInfo::Load(ParamEntryPar cls)
{
  icon = GlobLoadTexture
    (
    GetPictureName(cls >> "icon")
    );
  color = GetPackedColor(cls >> "color");
  size = cls >> "size";
  coefMin = cls >> "coefMin";
  coefMax = cls >> "coefMax";
  importance = cls >> "importance";
}

void MapTypeInfoTask::Load(ParamEntryPar cls)
{
  MapTypeInfo::Load(cls);

  iconCreated = GlobLoadTexture
    (
    GetPictureName(cls >> "iconCreated")
    );
  iconCanceled = GlobLoadTexture
    (
    GetPictureName(cls >> "iconCanceled")
    );
  iconDone = GlobLoadTexture
    (
    GetPictureName(cls >> "iconDone")
    );
  iconFailed = GlobLoadTexture
    (
    GetPictureName(cls >> "iconFailed")
    );

  colorCreated = GetPackedColor(cls >> "colorCreated");
  colorCanceled = GetPackedColor(cls >> "colorCanceled");
  colorDone = GetPackedColor(cls >> "colorDone");
  colorFailed = GetPackedColor(cls >> "colorFailed");
}

void LegendInfo::Load(ParamEntryPar cls)
{
  _x = cls >> "x";
  _y = cls >> "y";
  _w = cls >> "w";
  _h = cls >> "h";

  _font = GEngine->LoadFont(GetFontID(cls >> "font"));
  _size = cls >> "sizeEx";

  _colorBackground = GetPackedColor(cls >> "colorBackground");
  _color = GetPackedColor(cls >> "color");
}

CStatic *CreateStaticMap(ControlsContainer *parent, int idc, const ParamEntry &cls)
{
  CStaticMap *map = new CStaticMap(parent, idc, cls, cls >> "scaleMin", cls >> "scaleMax", cls >> "scaleDefault");
  map->SetVisibleRect(map->X(), map->Y(), map->W(), map->H());
  map->Center();
  return map;
}

CStaticMap::CStaticMap
(
 ControlsContainer *parent, int idc, ParamEntryPar cls,
 float scaleMin, float scaleMax, float scaleDefault
 )
 : CStatic(parent, idc, cls)
{
  _defaultCenter.Init();
  _defaultCenter[0] = (Pars >> "CfgWorlds" >> Glob.header.worldname >> "centerPosition")[0];
  _defaultCenter[1] = 0;
  _defaultCenter[2] = (Pars >> "CfgWorlds" >> Glob.header.worldname >> "centerPosition")[1];

  _gridNumbersOverLines = Pars >> "CfgWorlds" >> Glob.header.worldname >> "gridNumbersOverLines";

  _mapX = 0;
  _mapY = 0;

  float sizeLand = LandGrid * LandRange;
  saturate(_defaultCenter[0], 0, sizeLand);
  saturate(_defaultCenter[2], 0, sizeLand);

  _ptsPerSquareSea = cls >> "ptsPerSquareSea";   // seas
  _ptsPerSquareTxt = cls >> "ptsPerSquareTxt";   // textures
  _ptsPerSquareCLn = cls >> "ptsPerSquareCLn";   // count-lines
  _ptsPerSquareExp = cls >> "ptsPerSquareExp";   // exposure
  _ptsPerSquareCost = cls >> "ptsPerSquareCost";  // cost
  _ptsPerSquareFor = cls >> "ptsPerSquareFor";   // forests
  _ptsPerSquareForEdge = cls >> "ptsPerSquareForEdge";   // forest edges
  _ptsPerSquareRoad = cls >> "ptsPerSquareRoad";  // roads
  _ptsPerSquareObj = cls >> "ptsPerSquareObj";    // other objects

  _colorOutside = GetPackedColor(cls >> "colorOutside");

  _colorSeaPacked = GetPackedColor(cls >> "colorSea"); 
  _colorSea = Color((ColorVal)_colorSeaPacked);
  _colorForest = GetPackedColor(cls >> "colorForest");
  _colorRocks = GetPackedColor(cls >> "colorRocks");

  _colorCountlines = GetPackedColor(cls >> "colorCountlines");
  _colorCountlinesWater = GetPackedColor(cls >> "colorCountlinesWater");
  _colorMainCountlines = GetPackedColor(cls >> "colorMainCountlines");
  _colorMainCountlinesWater = GetPackedColor(cls >> "colorMainCountlinesWater");
  _colorForestBorder = GetPackedColor(cls >> "colorForestBorder");
  _colorRocksBorder = GetPackedColor(cls >> "colorRocksBorder");
  _colorPowerLines = GetPackedColor(cls >> "colorPowerLines");
  _colorRailWay = GetPackedColor(cls >> "colorRailWay");
  _colorNames = GetPackedColor(cls >> "colorNames");
  _colorLevels = GetPackedColor(cls >> "colorLevels");

  _maxSatelliteAlpha = cls >> "maxSatelliteAlpha";
  _alphaFadeStartScale = cls >> "alphaFadeStartScale";
  _alphaFadeEndScale = cls >> "alphaFadeEndScale";

  //_widthRailWay = cls >> "widthRailWay";

  _colorInactive = GetPackedColor(cls >> "colorInactive");

  _fontLabel = GLOB_ENGINE->LoadFont(GetFontID(cls>>"fontLabel"));
  _sizeLabel = cls >> "sizeExLabel";
  _fontGrid = GLOB_ENGINE->LoadFont(GetFontID(cls>>"fontGrid"));
  _sizeGrid = cls >> "sizeExGrid";
  _fontUnits = GLOB_ENGINE->LoadFont(GetFontID(cls>>"fontUnits"));
  _sizeUnits = cls >> "sizeExUnits";
  _fontNames = GLOB_ENGINE->LoadFont(GetFontID(cls>>"fontNames"));
  _sizeNames = cls >> "sizeExNames";
  _fontInfo = GLOB_ENGINE->LoadFont(GetFontID(cls>>"fontInfo"));
  _sizeInfo = cls >> "sizeExInfo";
  _fontLevels = GLOB_ENGINE->LoadFont(GetFontID(cls>>"fontLevel"));
  _sizeLevels = cls >> "sizeExLevel";

  _showCountourInterval = cls >> "showCountourInterval";
  _miniMapMode = false;

  //  _moveOnEdges = cls>>"moveOnEdges";
#if _VBS3 // markers moved to CStaticMap
  _onlyDrawMarkers = cls.ReadValue("onlyDrawMarkers",false);
  _drawAttachedMarkers = cls.ReadValue("drawAttachedMarkers",true);
  _drawConditionalMarkers = cls.ReadValue("drawConditionalMarkers",true);
#endif

  MAP_TYPES_ICONS_1(MAP_TYPE_INFO_LOAD)
    MAP_TYPES_ICONS_2(MAP_TYPE_INFO_LOAD)

    _infoWaypoint.Load(cls >> "Waypoint");
  _infoWaypointCompleted.Load(cls >> "WaypointCompleted");

#ifdef _XBOX
  _mapScrollXDZone = (cls>>"stickX")[0];
  _mapScrollYDZone = (cls>>"stickY")[0];
  _mapScrollX = CreateActionResponseCurve((cls>>"stickX")[1]);
  _mapScrollY = CreateActionResponseCurve((cls>>"stickY")[1]);
#endif

  if (cls.FindEntry("Legend"))
    _legend.Load(cls >> "Legend");
  else _showCountourInterval = false;

  EnableCtrl(true);

  // TODO: place in resource instead of config

  ParamEntryVal cfgMap = Pars >> "CfgInGameUI" >> "IslandMap";
  _iconPlayer = GlobLoadTexture
    (
    GetPictureName(cfgMap >> "iconPlayer")
    );
  _iconSelect = GlobLoadTexture
    (
    GetPictureName(cfgMap >> "iconSelect")
    );
  _iconCamera = GlobLoadTexture
    (
    GetPictureName(cfgMap >> "iconCamera")
    );
  _iconSensor = GlobLoadTexture
    (
    GetPictureName(cfgMap >> "iconSensor")
    );
  _sizeUnit = cfgMap >> "size";
  _sizeLeader = cfgMap >> "sizeLeader";
  _sizePlayer = cfgMap >> "sizePlayer";
  _colorFriendly = GetPackedColor(cfgMap >> "colorFriendly");
  _colorEnemy = GetPackedColor(cfgMap >> "colorEnemy");
  _colorCivilian = GetPackedColor(cfgMap >> "colorCivilian");
  _colorNeutral = GetPackedColor(cfgMap >> "colorNeutral");
  _colorUnknown = GetPackedColor(cfgMap >> "colorUnknown");
  _colorMe = GetPackedColor(cfgMap >> "colorMe");
  _shadow = cfgMap >> "shadow";
  _blinkingSpeedMe = 0;
  _colorMe2 = _colorMe;
  ConstParamEntryPtr entry = cfgMap.FindEntry("blinkingPeriodMe");
  if (entry)
  {
    float blinkingPeriod = *entry;
    if (blinkingPeriod > 0) _blinkingSpeedMe = 1.0f / blinkingPeriod;
    _colorMe2 = GetPackedColor(cfgMap >> "colorMe2");
  }

  _colorPlayable = GetPackedColor(cfgMap >> "colorPlayable");
  _colorSelect = GetPackedColor(cfgMap >> "colorSelect");
  _colorSensor = GetPackedColor(cfgMap >> "colorSensor");
  _colorDragging = GetPackedColor(cfgMap >> "colorDragging");

  _colorExposureEnemy = GetPackedColor(cfgMap >> "colorExposureEnemy");
  _colorExposureUnknown = GetPackedColor(cfgMap >> "colorExposureUnknown");
  _colorTracks = GetPackedColor(cfgMap >> "colorTracks");
  _colorRoads = GetPackedColor(cfgMap >> "colorRoads");
  _colorMainRoads = GetPackedColor(cfgMap >> "colorMainRoads");
  _colorTracksFill = GetPackedColor(cfgMap >> "colorTracksFill");
  _colorRoadsFill = GetPackedColor(cfgMap >> "colorRoadsFill");
  _colorMainRoadsFill = GetPackedColor(cfgMap >> "colorMainRoadsFill");
  _colorGrid = GetPackedColor(cfgMap >> "colorGrid");
  _colorGridMap = GetPackedColor(cfgMap >> "colorGridMap");
  _colorCheckpoints = GetPackedColor(cfgMap >> "colorCheckpoints");
  _colorCamera = GetPackedColor(cfgMap >> "colorCamera");
  _colorMissions = GetPackedColor(cfgMap >> "colorMissions");
  _colorActiveMission = GetPackedColor(cfgMap >> "colorActiveMission");

  _colorPath = GetPackedColor(cfgMap >> "colorPath");
  _colorPathDone = GetPackedColor(cfgMap >> "colorPathDone");
  _texturePath = GlobLoadTexture(GetPictureName(cfgMap >> "texturePath"));
  _texturePathDone = GlobLoadTexture(GetPictureName(cfgMap >> "texturePathDone"));
  _sizePath = cfgMap >> "sizePath";
  _sizePathDone = cfgMap >> "sizePathDone";

  _colorInfoMove = GetPackedColor(cfgMap >> "colorInfoMove");
  _colorGroups = GetPackedColor(cfgMap >> "colorGroups");
  _colorActiveGroup = GetPackedColor(cfgMap >> "colorActiveGroup");
  _colorSync = GetPackedColor(cfgMap >> "colorSync");
  _colorLabelBackground = GetPackedColor(cfgMap >> "colorLabelBackground");

  _cursorLineWidth = 0.5;
  entry = cfgMap.FindEntry("cursorLineWidth");
  if (entry) _cursorLineWidth = *entry;


#if _ENABLE_CHEATS
  _show = true;
  _showCost = false;
#endif

  _showIds = false;
  _showScale = true;
  _showScaleAlpha = 1;

  _moveKey = 0;

#ifdef _XBOX
  // force values for Xbox
  scaleMax = 0.40;
  scaleMin = 0.025;
  scaleDefault = 0.20;
#endif
  _scaleMin = scaleMin;
  _scaleMax = scaleMax;
  _scaleX = _scaleDefault = scaleDefault;

  Reset();
  Precalculate();

  SetVisibleRect(_x, _y, _w, _h);
}

CStaticMap::~CStaticMap()
{
  _mapUsed.Free();
}

#if _VBS3
Rect2DAbs CStaticMap::MapArea()
{
  float sizeLand = LandGrid * LandRange;

  // bottom left point
  float xMap = (_x - _mapX - _x) * _scaleX * sizeLand;
  float yMap = (1.0 - (_y + _h - _mapY - _y) * _scaleY) * sizeLand;

  // top right point
  float wMap = (_x + _w - _mapX - _x) * _scaleX * sizeLand;
  float hMap = (1.0 - (_y - _mapY - _y) * _scaleY) * sizeLand;

  return Rect2DAbs
    (      
    xMap,
    yMap,
    wMap - xMap,
    hMap - yMap
    );
}
#endif

void CStaticMap::SetScales(float scaleMin, float scaleMax, float scaleDefault)
{
  _scaleMin = scaleMin;
  _scaleMax = scaleMax;
  _scaleDefault = scaleDefault;
  SetScale(scaleDefault);
  Precalculate();
}

void CStaticMap::Reset()
{
  _moving = false;
  ClearAnimation();
  _moveKey = 0;
  _dragging = false;
  _selecting = false;
  _infoClick._type = signNone;
  _infoClickCandidate._type = signNone;
  _infoMove._type = signNone;

  //  Center();
}

// colors conversion
// TODO: move to PackedColor class

static PackedColor Mult2p00( PackedColor color )
{
  DWORD val=color;
  val&=0x7f7f7f7f;
  val<<=1;
  return PackedColor(val);
}

static PackedColor Mult1p50( PackedColor color )
{
  DWORD val=color;
  val&=0xfefefefe;
  val+=(val>>1);
  return PackedColor(val);
}

static PackedColor Mult0p75( PackedColor color )
{
  DWORD val=color;
  val&=0xfcfcfcfc;
  val+=(val>>1);
  return PackedColor(val>>1);
}

PackedColor Mult(PackedColor col1, PackedColor col2)
{
  int a = ((col1.A8() + 1) * (col2.A8() + 1) - 1) >> 8;
  int r = ((col1.R8() + 1) * (col2.R8() + 1) - 1) >> 8;
  int g = ((col1.G8() + 1) * (col2.G8() + 1) - 1) >> 8;
  int b = ((col1.B8() + 1) * (col2.B8() + 1) - 1) >> 8;
  return PackedColor(r, g, b, a);
}

PackedColor CStaticMap::InactiveColor(PackedColor color)
{
  return Mult(color, _colorInactive);
}

// coordinates conversion

DrawCoord CStaticMap::WorldToScreen(Vector3Val pt) const
{
  float invSizeLand = 1.0 / (LandGrid * LandRange);
  float xMap = pt.X() * invSizeLand * _invScaleX;
  float yMap = (1.0 - pt.Z() * invSizeLand) * _invScaleY;
  return DrawCoord(xMap + _mapX + _x, yMap + _mapY + _y);
}

DrawCoord CStaticMap::WorldSizeToScreen(Vector3Val pt) const
{
  float invSizeLand = 1.0 / (LandGrid * LandRange);
  float xMapD = pt.X() * invSizeLand * _invScaleX;
  float yMapD = - pt.Z() * invSizeLand * _invScaleY;
  return DrawCoord(xMapD,yMapD);
}

Point3 CStaticMap::ScreenToWorld(DrawCoord ptMap) const
{
  float sizeLand = LandGrid * LandRange;
  float x = (ptMap.x - _mapX - _x) * _scaleX * sizeLand;
  float y = (1.0 - (ptMap.y - _mapY - _y) * _scaleY) * sizeLand;
  // !!! WARNING: result.Y() == 0
  return Point3(x, 0, y);
}

// drawing
bool CStaticMap::DrawSign(MapTypeInfo &info, Vector3Val pos)
{
  float coef = _invScaleX * 0.05;
  saturate(coef, info.coefMin, info.coefMax);
  float size = coef * info.size;
  return DrawSign(info.icon, info.color, pos, size, size, 0, "", NULL , -1,  info.shadow);
}

#if _VBS2 // conditional markers
bool CStaticMap::DrawSignAutosize
(
 Texture *texture, PackedColor color,
 Vector3Val pos, float w, float h, float azimut, RString text, bool shadow,
 Font *font, float textSize, bool autosize
 )
{
  if (!autosize)
  {
    // correlates with new editor marker scaling
    w = 1 / (_scaleX / (InvLandSize * 1000)) * w;
    h = 1 / (_scaleX / (InvLandSize * 1000)) * h;
  }
  return DrawSign(texture, color, pos, w, h, azimut, text, shadow, font, textSize);
}
#endif

bool CStaticMap::DrawSign
(
 Texture *texture, PackedColor color,
 Vector3Val pos, float w, float h, float azimut, RString text,
 Font *font, float textSize, int shadow
 )
{
  DrawCoord posMap = WorldToScreen(pos);
  return DrawSign(texture, color, posMap, w, h, azimut, text, font, textSize,false, shadow);
}

void CStaticMap::DrawLine(Vector3Val pos1, Vector3Val pos2, PackedColor color)
{
  DrawCoord posMap1 = WorldToScreen(pos1);
  DrawCoord posMap2 = WorldToScreen(pos2);
  return DrawLine(posMap1, posMap2, color);
}

void CStaticMap::DrawLine(DrawCoord posMap1, DrawCoord posMap2, PackedColor color)
{
  GEngine->DrawLine(Line2DPixel(posMap1.x * _wScreen, posMap1.y * _hScreen, posMap2.x * _wScreen, posMap2.y * _hScreen), color, color, _clipRect);
}

void CStaticMap::DrawArrow(Vector3Val pos1, Vector3Val pos2, PackedColor color)
{
  DrawCoord posMap1 = WorldToScreen(pos1);
  DrawCoord posMap2 = WorldToScreen(pos2);

  posMap1.x *= _wScreen;
  posMap1.y *= _hScreen;
  posMap2.x *= _wScreen;
  posMap2.y *= _hScreen;

  GEngine->DrawLine(Line2DPixel(posMap1.x, posMap1.y, posMap2.x, posMap2.y), color, color, _clipRect);

  float dx = posMap2.x - posMap1.x;
  float dy = posMap2.y - posMap1.y;
  float size2 = Square(dx) + Square(dy);
  float invSize = InvSqrt(size2);
  dx *= 12.0 * invSize; dy *= 12.0 * invSize;
  float x = posMap2.x - dx + 0.5 * dy;
  float y = posMap2.y - dy - 0.5 * dx;
  GEngine->DrawLine
    (
    Line2DPixel(posMap2.x, posMap2.y, x, y),
    color, color, _clipRect
    );

  x = posMap2.x - dx - 0.5 * dy;
  y = posMap2.y - dy + 0.5 * dx;
  GEngine->DrawLine
    (
    Line2DPixel(posMap2.x, posMap2.y, x, y),
    color, color, _clipRect
    );
}

bool CStaticMap::DrawSign
(
 Texture *texture, PackedColor color,
 DrawCoord posMap, float ww, float hh, float azimut, RString text,
 Font *font, float textSize, bool textSizeConstant, int shadow
 )
{
  // default arguments
  if (textSize < 0) textSize = _sizeNames;
  if (!font) font = _fontNames;

  // normalize w, h
  ww *= 1.0 / 640;
  hh *= 1.0 / 480;

  float size = textSize;
  if(!textSizeConstant)
  {
    size = textSize * (_invScaleX * 0.05);
    saturate(size, 0.5 * textSize, 1.0 * textSize);
  }

  float offsetX = 0.03 * size;
  float offsetY = 0.04 * size;
  PackedColor shadowColor = PackedBlack;
  shadowColor.SetA8(toIntFloor(0.8 * color.A8()));

  // picture
  if (texture)
  {
    if (azimut == 0)
    {
      // draw via Draw2D

      // center sign
      posMap.x -= 0.5 * ww;
      posMap.y -= 0.5 * hh;

      if (posMap.x + ww < _x) 
        return false;
      if (posMap.y + hh < _y)
        return false;
      if (posMap.x > _x + _w)
        return false;
      if (posMap.y > _y + _h)
        return false;

      float w = _wScreen;
      float h = _hScreen;

      MipInfo mip = GEngine->TextBank()->UseMipmap(texture, 0, 0);
      if (shadow == 1 ) GEngine->Draw2D(
        mip, shadowColor,
        Rect2DPixel(CX(posMap.x + offsetX), CY(posMap.y + offsetY),
        CX(posMap.x+ww)-CX(posMap.x), CY(posMap.y+hh)-CY(posMap.y)),
        _clipRect);
      GEngine->Draw2D(
        mip, color,
        Rect2DPixel(CX(posMap.x), CY(posMap.y),
        CX(posMap.x+ww)-CX(posMap.x), CY(posMap.y+hh)-CY(posMap.y)),
        _clipRect,shadow);

      posMap.x += ww;
      posMap.y += 0.5 * hh;
    }
    else
    {
      // draw via DrawPoly
      float a = 0.5 * ww;
      float b = 0.5 * hh;

      // TODO: improve clipping
      float r = sqrt(Square(a) + Square(b));
      if (posMap.x + r < _x) 
        return false;
      if (posMap.y + r < _y)
        return false;
      if (posMap.x - r > _x + _w)
        return false;
      if (posMap.y - r > _y + _h)
        return false;

      float s = sin(azimut);
      float c = cos(azimut);
      float cx = posMap.x * _wScreen;
      float cy = posMap.y * _hScreen;
      a *= _wScreen;
      b *= _hScreen;

      const int n = 4;
      Vertex2DPixel vs[n];
      // 0
      vs[0].x = cx + c * a + s * b;
      vs[0].y = cy + s * a - c * b;
      vs[0].u = 1;
      vs[0].v = 0;
      // 1
      vs[1].x = cx + c * a + s * (-b);
      vs[1].y = cy + s * a - c * (-b);
      vs[1].u = 1;
      vs[1].v = 1;
      // 2
      vs[2].x = cx + c * (-a) + s * (-b);
      vs[2].y = cy + s * (-a) - c * (-b);
      vs[2].u = 0;
      vs[2].v = 1;
      // 3
      vs[3].x = cx + c * (-a) + s * b;
      vs[3].y = cy + s * (-a) - c * b;
      vs[3].u = 0;
      vs[3].v = 0;

      MipInfo mip = GEngine->TextBank()->UseMipmap(texture, 0, 0);

      if (shadow == 1)
      {
        for (int i=0; i<4; i++)
        {
          vs[i].x += offsetX * _wScreen;
          vs[i].y += offsetY * _hScreen;
          vs[i].color = shadowColor;
        }
        GEngine->DrawPoly(mip, vs, n, _clipRect);

        for (int i=0; i<4; i++)
        {
          vs[i].x -= offsetX * _wScreen;
          vs[i].y -= offsetY * _hScreen;
          vs[i].color = color;
        }
        GEngine->DrawPoly(mip, vs, n, _clipRect);
      }
      else
      {
        for (int i=0; i<4; i++)
        {
          vs[i].color = color;
        }
        GEngine->DrawPoly(mip, vs, n, _clipRect);
      }

      posMap.x += 0.5 * (fabs(c * ww) + fabs(s * hh));
    }
  }

  // text
  if (text.GetLength() > 0)
  {
    float hText = size;

    float top = posMap.y - 0.5 * hText;
    //float width = GEngine->GetTextWidth(size, font, text);

    if (shadow == 1) GEngine->DrawText
      (
      Point2DFloat(posMap.x + offsetX, top + offsetY), size,
      Rect2DFloat(_x, _y, _w, _h),
      font, shadowColor, text, 0
      );

    GEngine->DrawText
      (
      Point2DFloat(posMap.x, top), size,
      Rect2DFloat(_x, _y, _w, _h),
      font, color, text, shadow
      );
  }

  return true;
}

void CStaticMap::DrawLabel(Vector3Par pos, RString text, PackedColor color, int shadow)
{
  DrawCoord posMap = WorldToScreen(pos);

  // place label
  float w = GLOB_ENGINE->GetTextWidth(_sizeLabel, _fontLabel, text);
  float h = _sizeLabel;
  posMap.x -= 0.5 * w;
  posMap.y -= 0.01 + h;

  MipInfo mip = GLOB_ENGINE->TextBank()->UseMipmap(NULL, 0, 0);
  GLOB_ENGINE->Draw2D
    (
    mip, _colorLabelBackground,
    Rect2DPixel(posMap.x * _wScreen, posMap.y * _hScreen,
    w * _wScreen, h * _hScreen),
    _clipRect
    );

  GLOB_ENGINE->DrawText
    (
    posMap, _sizeLabel,
    Rect2DFloat(_x, _y, _w, _h),
    _fontLabel, color, text, shadow
    );
}

struct DetectCircle
{
  int _x;
  int _z;
  int _radius;

  DetectCircle(int x, int z, int radius)
    :_x(x),_z(z),_radius(radius)
  {
  }

  //! check if we overlapp with given rectangle 
  bool operator () (int x, int z, int size) const
  {
    // calculate distance of rectangle center from our center
    int centerX = x+(size>>1);
    int centerZ = z+(size>>1);

    float dist2 = Square(centerX-_x) + Square(centerZ-_z);
    // check if we can quickly reject or quickly accept
    if (dist2<Square(_radius+size))
    {
      // part of the circle is certainly contained in the rectangle
      return true;
    }
    if (dist2>Square(_radius+size)*2)
    {
      // circle is certianly of of the rectangle
      return false;
    }
    // circle may or may not be in the rectangle
    // we can perform stricter check, or we can return true and provide
    // some superfluous items
    return true;
  }
};

#if USE_LANDSCAPE_LOCATIONS

//! enumerate all locations we are interested in
struct LocationsRegion
{
  // in
  float _xMin,_xMax;
  float _zMin,_zMax;

  bool operator () (int x, int y, int size) const
  {
    float xMin = x, xMax = x+size;
    float zMin = y, zMax = y+size;
    bool ret = true;
    if (_xMin>xMax) ret = false;
    if (_xMax<xMin) ret = false;
    if (_zMin>zMax) ret = false;
    if (_zMax<zMin) ret = false;
    //LogF("Testing %d,%d:%d, result %d",x,y,size,ret);
    return ret;
  }
};

struct LocationsListResult
{
  RefArray<Location, MemAllocLocal< Ref<Location>,200 > > _locations;
};

struct LocationsList
{
  // out
  LocationsListResult *_result;

  LocationsList(LocationsListResult *result)
    : _result(result)
  {
  }

  bool operator () (const Ref<LocationsItem> &item) const
  {
    for (int i=0; i<item->_locations.Size(); i++)
    {
      Location *location = item->_locations[i];
      int style = location->_type->_drawStyle;
      _result[style]._locations.Add(location);
    }
    return false;
  }
};

/*
struct SkipNear
{
const Location *_location;
float _minDist2;
LocationDrawStyle _style;

SkipNear(const Location *location, float minDist)
: _location(location),
_minDist2(Square(minDist))
{
_style = _location->_type->_drawStyle;
}

bool operator () (const Ref<LocationsItem> &item) const
{
for (int i=0; i<item->_locations.Size(); i++)
{
const Location *tested = item->_locations[i];
if (tested == _location) continue;
if (tested->_type->_drawStyle != _style) continue;
// check if the location is more important
if (tested->_importance < _location->_importance) continue;
if (tested->_importance == _location->_importance)
{
// select random (but deterministically) as more important
if (
GLandscape->GetRandomValueForCoord(toInt(tested->_position.X()), toInt(tested->_position.Z())) <=
GLandscape->GetRandomValueForCoord(toInt(_location->_position.X()), toInt(_location->_position.Z()))
)
continue;
}
// check if the location is near enough
if (tested->_position.DistanceXZ2(_location->_position) < _minDist2) return true;
}
return false;
}
};
*/

#else

//! container which makes quick space queries possible
#include <Es/Containers/quadtreeCont.hpp>

struct MountainInfo: public Vector3
{
  int _index;
  bool operator == (const MountainInfo &with) const
  {
    return _index==with._index;
  }
  MountainInfo()
  {
    _index = -1;
  }
  MountainInfo(Vector3Par pos, int index)
    :Vector3(pos),_index(index)
  {
  }

  int GetX() const {return toLargeInt(X());}
  int GetY() const {return toLargeInt(Z());}
};

TypeIsMovable(MountainInfo)

struct SkipNear
{
  MountainInfo _pos;
  //bool _skipped;
  float _minDist;
  SkipNear(MountainInfo pos, float minDist)
    :_pos(pos),
    _minDist(minDist)
  {
  }
  bool operator () (const MountainInfo &arg) const
  {
    // check if the mountain is near enough
    // check if the mountain is higher
    if (arg._index>=_pos._index) return false;
    float dist2 = _pos.DistanceXZ2(arg);
    return dist2<Square(_minDist);
  }

};


//! enumerate all mountains we are interested in
struct MountainRegion
{
  // in
  float _xMin,_xMax;
  float _zMin,_zMax;

  bool operator () (int x, int y, int size) const
  {
    float xMin = x, xMax = x+size;
    float zMin = y, zMax = y+size;
    bool ret = true;
    if (_xMin>xMax) ret = false;
    if (_xMax<xMin) ret = false;
    if (_zMin>zMax) ret = false;
    if (_zMax<zMin) ret = false;
    //LogF("Testing %d,%d:%d, result %d",x,y,size,ret);
    return ret;
  }
};

struct MountainList
{
  // out
  AutoArray<int> _indices;

  bool operator () (const MountainInfo &pos)
  {
    _indices.Add(pos._index);
    return false;
  }
};
#endif

struct CStaticMap::DrawForestContext
{
  float xMin,yMin;
  float xStep,yStep;
  int iMin,jMin;
};

/**
Forest drawing functor
Same code is used for edge and area rendering
*/
struct CStaticMap::DrawForestsNewFunc
{
  const CStaticMap::DrawForestContext &_ctx;
  CStaticMap *_map;
  bool _drawLines;
  bool _rocks;

  DrawForestsNewFunc(const CStaticMap::DrawForestContext &ctx, CStaticMap *map, bool drawLines, bool rocks)
    :_ctx(ctx),_map(map),_drawLines(drawLines), _rocks(rocks)
  {}

  void operator () (const MapObjectList &list, int x, int z, int xSize, int zSize) const
  {
    _map->DrawForestsNew(list, x, z, _ctx, _drawLines, _rocks);
  }
};

struct CStaticMap::DrawRoadContext
{
  mutable LineArray _lines;
  CStaticMap *_map;

  DrawRoadContext(CStaticMap *map):_map(map){}

  void operator () (const MapObjectList &list, int x, int z, int xSize, int zSize) const
  {
    _map->DrawRoads(_lines, list, x, z);
  }
};


struct CStaticMap::DrawObjectsContext
{
  mutable LineArray _lines;
  CStaticMap *_map;

  DrawObjectsContext(CStaticMap *map):_map(map){}

  void operator () (const MapObjectList &list, int x, int z, int xSize, int zSize) const
  {
    _map->DrawObjects(list, x, z);
  }
};

template <class Func>
void ForRectangleMapObjects(int xBeg, int zBeg, int xEnd, int zEnd, const Func &f)
{
  if (xBeg<0) xBeg=0; if (xEnd>LandRangeMask) xEnd=LandRangeMask;
  if (zBeg<0) zBeg=0; if (zEnd>LandRangeMask) zEnd=LandRangeMask;
  DetectInRectangle rect(xBeg,zBeg,xEnd,zEnd);
  GLandscape->ForSomeMapObjects(rect,f);
  //  // slow and robust - usefull for ForSomeMapObjects verification
  //  for (int x=xBeg; x<xEnd; x++)
  //  for (int z=zBeg; z<zEnd; z++)
  //  {
  //    const MapObjectList &list = GLandscape->GetMapObjects(x,z);
  //    f(list,x,z,1,1);
  //  }
}

/// draw autopilot taxi guides
void CStaticMap::DrawTaxiGuide(const AutoArray<Vector3> &guide)
{
  for (int i=1; i<guide.Size(); i++)
  {
    Vector3Val beg = guide[i-1];
    Vector3Val end = guide[i];

    DrawCoord begCoord = WorldToScreen(beg);
    DrawCoord endCoord = WorldToScreen(end);

    Line2DPixel line;
    line.beg = Point2DPixel(begCoord.x * _wScreen, begCoord.y * _hScreen);
    line.end = Point2DPixel(endCoord.x * _wScreen, endCoord.y * _hScreen);

    GEngine->DrawLine(line,_colorRoads,_colorRoads,_clipRect);
  }
}

void CStaticMap::DrawRunway(const Vector3 *runway)
{
  // if runway is invalid, do not draw it
  if (runway[0].DistanceXZ2(runway[1])<Square(0.1)) return;
  if (runway[0].DistanceXZ2(runway[2])<Square(0.1)) return;
  // convert to screen space
  DrawCoord runwayTL = WorldToScreen(runway[0]);
  DrawCoord runwayTR = WorldToScreen(runway[1]);
  DrawCoord runwayBL = WorldToScreen(runway[2]);
  DrawCoord runwayBR = WorldToScreen(runway[3]);

  Point2DPixel pixelTL = Point2DPixel(runwayTL.x * _wScreen, runwayTL.y * _hScreen);
  Point2DPixel pixelTR = Point2DPixel(runwayTR.x * _wScreen, runwayTR.y * _hScreen);
  Point2DPixel pixelBL = Point2DPixel(runwayBL.x * _wScreen, runwayBL.y * _hScreen);
  Point2DPixel pixelBR = Point2DPixel(runwayBR.x * _wScreen, runwayBR.y * _hScreen);

  Line2DPixel left,right,top,bottom;
  left.beg = pixelTL, left.end = pixelBL;
  right.beg = pixelTR, right.end = pixelBR;

  top.beg = pixelTL, top.end = pixelTR;
  bottom.beg = pixelBL, bottom.end = pixelBR;

  GEngine->DrawLinePrepare();
  GEngine->DrawLineDo(left,_colorRoads,_colorRoads,_clipRect);
  GEngine->DrawLineDo(right,_colorRoads,_colorRoads,_clipRect);
  GEngine->DrawLineDo(top,_colorRoads,_colorRoads,_clipRect);
  GEngine->DrawLineDo(bottom,_colorRoads,_colorRoads,_clipRect);
}

void CStaticMap::DrawAirports()
{
  // check ILS and taxiing information and draw runway based on it

  for (int i=0; i<GWorld->GetAirportCount(); i++)
  {
    Vector3 runway[4],taxiway[4];
    GWorld->GetAirportRunwayRect(i,runway,taxiway);
    DrawRunway(runway);
    DrawRunway(taxiway);

#if _ENABLE_CHEATS
    DrawTaxiGuide(GWorld->GetTaxiInPath(TWest,runway[0],VZero));
    DrawTaxiGuide(GWorld->GetTaxiOffPath(TWest,runway[0],VZero));
#endif
  }

}


void CStaticMap::DrawBackground()
{
  PROFILE_SCOPE(mapDr);
  float ptsLand = 800.0 * _invScaleX * InvLandRange; // avoid dependece on video resolution
  float invPtsLand = 1.0 / ptsLand;

  float invTerrainRange = 1.0 / TerrainRange;
  float ptsTerrain = 800.0 * _invScaleX * invTerrainRange; // avoid dependece on video resolution
  float invPtsTerrain = 1.0 / ptsTerrain;

  //GEngine->ShowMessage(1000, "Scale = %.4f, PtsPerFld = %.2f", _scaleX, ptsLand);

  // calculate range of coordinates for the landscape
  float xMinBorder = _x + _mapX;
  float xMaxBorder = xMinBorder + _invScaleX;
  float yMinBorder = _y + _mapY;
  float yMaxBorder = yMinBorder + _invScaleY;

#if _ENABLE_CHEATS
  if (CHECK_DIAG(DEMapScale)) DIAG_MESSAGE(100, Format("Map scale: %.4f, ptsLand: %.1f", _scaleX, ptsLand));
#endif

  const bool skipOutside = !GLandscape->HasTerrainOutside();

  if (_texture)
  {
    // draw paper
    MipInfo mip = GLOB_ENGINE->TextBank()->UseMipmap(_texture, 0, 0);

    float xStep = (1.0 / 2.0) * _invScaleX;
    float yStep = (1.0 / 2.0) * _invScaleY;
    float xMin = _x - fastFmod(-_mapX, xStep);
    float yMin = _y - fastFmod(-_mapY, yStep);

    for (float y=yMin; y<_y+_h; y+=yStep)
      for (float x=xMin; x<_x+_w; x+=xStep)
      {
        if (skipOutside)
        {
          if (x <= xMinBorder - xStep) continue;
          if (x > xMaxBorder) continue;
          if (y <= yMinBorder - yStep) continue;
          if (y > yMaxBorder) continue;
        }
        GEngine->Draw2D(
          mip, _bgColor,
          Rect2DPixel(x * _wScreen, y * _hScreen, xStep * _wScreen, yStep * _hScreen),
          _clipRect
          );
      }
  }

  //#if _ENABLE_CHEATS
  if ((!_showScale || UserMapDrawing()) && _scaleX < _alphaFadeEndScale)
  {
    // draw fields (textures)
    int iStep = toIntCeil(_ptsPerSquareTxt * invPtsLand);
    if (iStep>1)
    {
      // once we start stepping, step over whole segments
      int zAlign;
      int satSeg = GLandscape->GetSatSegSize(zAlign);
      if (iStep<satSeg*2) iStep = satSeg;
      // there is some alignment as well
      // however, the alignment is done so that endpoint is aligned at 0
      DoAssert(LandRange%satSeg==zAlign);
    }
    int jStep = iStep;
    float xStep = iStep * InvLandRange * _invScaleX;
    float yStep = jStep * InvLandRange * _invScaleY;
    int iMin = iStep * toIntFloor(-_mapX / xStep);
    int jMin = LandRange - jStep * toIntFloor(-_mapY / yStep);
    float xMin = _x - fastFmod(-_mapX, xStep);
    float yMin = _y - fastFmod(-_mapY, yStep);

    // calculate mipmap level
    const float invLog4 = 0.72134752044; // 1.0 / log(4.0);
    _mipmapLevel = 7 - toIntFloor(log(xStep * _wScreen * yStep * _hScreen) * invLog4);
    if (_mipmapLevel < 0 ) _mipmapLevel = 0;
    if (_mipmapLevel > 8 ) _mipmapLevel = 8;

    float y = yMin;
    float j = jMin;
    while (y <= _y + _h)
    {
      float x = xMin;
      int i = iMin;
      while (x <= _x + _w)
      {
        if (!skipOutside || (i > -iStep && i < LandRange + iStep && j > -jStep && j < LandRange + jStep))
          DrawField(x, y, xStep, yStep, i, (int)j, iStep, -jStep);
        i += iStep;
        x += xStep;
      }
      j -= jStep;
      y += yStep;
    }
  }
  // #endif

  if (!UserMapIsBeingDrawn())
  {
    // draw sea
    int iStep = toIntCeil(_ptsPerSquareSea * invPtsLand);
    float xStep = iStep * InvLandRange * _invScaleX;
    int jStep = iStep;
    float yStep = jStep * InvLandRange * _invScaleY;
    int iMin = iStep * toIntFloor(-_mapX / xStep);
    float xMin = _x - fastFmod(-_mapX, xStep);
    int jMin = LandRange - jStep * toIntFloor(-_mapY / yStep);
    float yMin = _y - fastFmod(-_mapY, yStep);

    float y = yMin;
    int j = jMin;
    while (y <= _y + _h)
    {
      float x = xMin;
      int i = iMin;
      while (x <= _x + _w)
      {
        if (!skipOutside || (i > -iStep && i < LandRange + iStep && j > -jStep && j < LandRange + jStep))
          DrawSea(x, y, xStep, yStep, i, j, iStep, -jStep);
        i += iStep;
        x += xStep;
      }
      j -= jStep;
      y += yStep;
    }
  }


#if _ENABLE_CHEATS
  if (_show)
#endif
  {
#if _VBS3
    if(GVBSVisuals.GetShowContours())
#endif
      // draw count-lines
      if (!UserMapIsBeingDrawn())
      {
        LineArray lines;
        //PROFILE_SCOPE(mpCnt);
        int iStep = toIntCeil(_ptsPerSquareCLn * invPtsTerrain);
        float xStep = iStep * invTerrainRange * _invScaleX;
        int jStep = iStep;
        float yStep = jStep * invTerrainRange * _invScaleY;
        int iMin = iStep * toIntFloor(-_mapX / xStep);
        float xMin = _x - fastFmod(-_mapX, xStep);
        int jMin = TerrainRange - jStep * toIntFloor(-_mapY / yStep);
        float yMin = _y - fastFmod(-_mapY, yStep);

        float step = 70.0 * _scaleX;
        int stepexp = toIntFloor(log10(step));
        float step10 = pow(10.0, stepexp);
        step /= step10;
        if (step <= 2.0) step = 2.0;
        else if (step <= 5.0) step = 5.0;
        else step = 10.0;
        step *= step10;
        saturateMax(step, 1);

        float maxLevel = step * toIntCeil(10000.0 / step);

        float y = yMin;
        int j = jMin;
        while (y <= _y + _h)
        {
          float x = xMin;
          int i = iMin;
          while (x <= _x + _w)
          {
            if (!skipOutside || (i > -iStep && i < TerrainRange + iStep && j > -jStep && j < TerrainRange + jStep))
              DrawCountlines(lines, x, y, xStep, yStep, i, j, iStep, -jStep, step, 5*step, maxLevel);
            i += iStep;
            x += xStep;
          }
          j -= jStep;
          y += yStep;
        }
        GEngine->DrawLines(lines.Data(),lines.Size(),_clipRect);
      }

      {
        //PROFILE_SCOPE(mpObj);
        float xStep = InvLandRange * _invScaleX;
        float yStep = InvLandRange * _invScaleY;

        int iMin = toIntFloor(-_mapX / xStep);
        int jMin = LandRange - toIntFloor(-_mapY / yStep);

        // make sure viewed region is fully loaded
        if (!_mapUsed)
        {
          _mapUsed = new MapObjectListRectUsed;
        }
        // calculate object area
        int xBeg = iMin;
        int xEnd = xBeg + toIntCeil((_w + fastFmod(-_mapX, xStep))/xStep);

        int zEnd = jMin;
        int zBeg = zEnd - toIntCeil((_h + fastFmod(-_mapY, yStep))/yStep);

        // extend because the objects from neighbor fields can interfere with
        xBeg--; xEnd++; zBeg--; zEnd++;

        // saturate borders
        if (xBeg<0) xBeg = 0;
        if (xEnd>LandRangeMask) xEnd = LandRangeMask;
        if (zBeg<0) zBeg = 0;
        if (zEnd>LandRangeMask) zEnd = LandRangeMask;

        float minPtsPerSquare = floatMin(_ptsPerSquareFor,_ptsPerSquareRoad,_ptsPerSquareObj);

        if (ptsLand>=minPtsPerSquare)
        {
          // when no objects are needed at all, do not load them
          GLandscape->UseMapObjects(*_mapUsed,xBeg,zBeg,xEnd,zEnd);
        }

        if (ptsLand>=minPtsPerSquare*0.8f)
        {
          // when no objects are needed at all even with some zoom, do not preload them
          PROFILE_SCOPE(mapPM)

            // border should be based on the map size
            // 5 % on each side sounds reasonable
            int border=toIntCeil((zEnd-zBeg)*0.05)+1;
          // preload some border around
          GLandscape->PreloadMapObjects(*_mapUsed,xBeg-border,zBeg-border,xEnd+border,zEnd+border);
        }

        // draw objects
        if (ptsLand >= _ptsPerSquareFor && !UserMapIsBeingDrawn())
        {
          PROFILE_SCOPE(mapFo)
            // forests
            DrawForestContext ctx;

          float invPtsLand = (1.0/800.0) * _scaleX * LandRange;  // avoid dependence on video resolution

          int iStep = toIntCeil(_ptsPerSquareFor * invPtsLand);
          int jStep = iStep;

          ctx.xStep = iStep * InvLandRange * _invScaleX;
          ctx.yStep = jStep * InvLandRange * _invScaleY;

          ctx.xMin = _x - fastFmod(-_mapX, xStep);
          ctx.yMin = _y - fastFmod(-_mapY, yStep);

          ctx.iMin = iStep * toIntFloor(-_mapX / xStep);
          ctx.jMin = LandRangeMask - jStep * toIntFloor(-_mapY / yStep);

          if (GLandscape->GetMinTreesInForestSquare() > 0)
          {
            ForRectangleMapObjects(xBeg,zBeg,xEnd,zEnd,DrawForestsNewFunc(ctx,this,false,false));
            if (ptsLand >= _ptsPerSquareForEdge)
            {
              // shared state preparation
              GEngine->DrawLinePrepare();
              ForRectangleMapObjects(xBeg,zBeg,xEnd,zEnd,DrawForestsNewFunc(ctx,this,true,false));
            }
          }
          else
          {
            // old style (OFP) forests no longer supported
          }

          // rocky areas
          if (GLandscape->GetMinRocksInRockSquare() > 0)
          {
            ForRectangleMapObjects(xBeg,zBeg,xEnd,zEnd,DrawForestsNewFunc(ctx,this,false,true));
            if (ptsLand >= _ptsPerSquareForEdge)
            {
              // shared state preparation
              GEngine->DrawLinePrepare();
              ForRectangleMapObjects(xBeg,zBeg,xEnd,zEnd,DrawForestsNewFunc(ctx,this,true,true));
            }
          }
        }
        if (!UserMapIsBeingDrawn() || UserMapDrawObjects())
        {

          if (ptsLand >= _ptsPerSquareRoad)
          {
            PROFILE_SCOPE(mapRd)
              DrawRoadContext ctx(this);
            ForRectangleMapObjects(xBeg,zBeg,xEnd,zEnd,ctx);
            //GLandscape->_mapObjects->ForSomeItem(ctx);
            //_mapObj
            GEngine->DrawLines(ctx._lines.Data(),ctx._lines.Size(),_clipRect);
          }
          if (ptsLand >= _ptsPerSquareObj)
          {
            PROFILE_SCOPE(mapOb)
              DrawObjectsContext ctx(this);
            ForRectangleMapObjects(xBeg,zBeg,xEnd,zEnd,ctx);
          }
        }
#if 0 //_DEBUG
        _mapUsed.Free();
        // make sure no part of map is locked now - it may be cached
        for (int z=0; z<LandRange; z++) for (int x=0; x<LandRange; x++)
        {
          MapObjectListFull *list = GLandscape->GetMapObjects(x,z).GetList();
          if (list)
          {
            int cachedCount = list->IsInList();
            Assert(list->GetUseCount()==cachedCount);
          }
        }
#endif
      }
      if (!UserMapIsBeingDrawn() || UserMapDrawObjects())
      {
        DrawAirports();
      }

  }

  if (skipOutside)
  {
    // borders around the map
    MipInfo mip = GEngine->TextBank()->UseMipmap(NULL, 0, 0);
    float xLeft = _x;
    if (xMinBorder > xLeft)
    {
      GEngine->Draw2D(
        mip, _colorOutside,
        Rect2DPixel(xLeft * _wScreen, _y * _hScreen, (xMinBorder - xLeft) * _wScreen, _h * _hScreen),
        _clipRect
        );
      xLeft = xMinBorder;
    }
    float xRight = _x + _w;
    if (xMaxBorder < xRight)
    {
      GEngine->Draw2D(
        mip, _colorOutside,
        Rect2DPixel(xMaxBorder * _wScreen, _y * _hScreen, (xRight - xMaxBorder) * _wScreen, _h * _hScreen),
        _clipRect
        );
      xRight = xMaxBorder;
    }
    if (yMinBorder > _y)
    {
      GEngine->Draw2D(
        mip, _colorOutside,
        Rect2DPixel(xLeft * _wScreen, _y * _hScreen, (xRight - xLeft) * _wScreen, (yMinBorder - _y) * _hScreen),
        _clipRect
        );
    }
    if (yMinBorder < _y + _h)
    {
      GEngine->Draw2D(
        mip, _colorOutside,
        Rect2DPixel(xLeft * _wScreen, yMaxBorder * _hScreen, (xRight - xLeft) * _wScreen, (_y + _h - yMinBorder) * _hScreen),
        _clipRect
        );
    }
  }
  if (!UserMapIsBeingDrawn())
  {

#if USE_LANDSCAPE_LOCATIONS
    const Locations &locations = GLandscape->GetLocations();

    LocationsRegion region;
    Vector3 minPos = ScreenToWorld(Point2DFloat(_x, _y));
    Vector3 maxPos = ScreenToWorld(Point2DFloat(_x + _w,_y + _h));
    region._xMin = floatMin(minPos.X(), maxPos.X());
    region._zMin = floatMin(minPos.Z(), maxPos.Z());
    region._xMax = floatMax(minPos.X(), maxPos.X());
    region._zMax = floatMax(minPos.Z(), maxPos.Z());

    LocationsListResult lists[NLocationDrawStyle];
    {
      PROFILE_SCOPE_EX(locL,*);
      locations.ForEachInRegion(region, LocationsList(lists));
    }

    // TODO: define density in CfgLocationTypes
    const float density = 0.08;
    const float elevationOffset = GLandscape->GetLandElevationOffset();
    float sizeLand = LandGrid * LandRange;
    float minDistF = density * sizeLand * _scaleX;
    {
      PROFILE_SCOPE_EX(locD,*);
      for (int style = 0; style<NLocationDrawStyle; style++)
      {
        const LocationsListResult &list = lists[style];
        for (int i=0; i<list._locations.Size(); i++)
        {
          const Location *location = list._locations[i];
          if (location->_nearestMoreImportant >= minDistF)
            location->Draw(this,false,elevationOffset);
          /*
          // make space based query
          int x = toLargeInt(location->GetX());
          int z = toLargeInt(location->GetY());

          if (!locations.ForEachInRegion(DetectCircle(x, z, minDist), SkipNear(location, minDistF)))
          {
          location->Draw(this);
          }
          */
        }
      }
    }

#else
    // names
    ParamEntryVal world = Pars >> "CfgWorlds" >> Glob.header.worldname;
    ConstParamEntryPtr cls = world.FindEntry("Names");
    if (cls)
    {
      //PROFILE_SCOPE(mpPts);
      AUTO_STATIC_ARRAY(Vector3, points, 32);
      float minDist2 = Square(1000 * _scaleX);
      int n = cls->GetEntryCount();
      points.Resize(n);
      for (int i=0; i<n; i++)
      {
        ParamEntryVal item = cls->GetEntry(i);
        Vector3 pos;
        pos.Init();
        pos[0] = (item >> "position")[0];
        pos[2] = (item >> "position")[1];
        pos[1] = 0;
        points[i] = pos;
        bool skip = false;
        for (int j=0; j<i; j++)
        {
          if (pos.DistanceXZ2(points[j]) < minDist2)
          {
            skip = true;
            break;
          }
        }
        if (!skip) DrawName(item);
      }
    }
    else
    {
      Fail("Class <Names> expected");
    }

    // mountains
    {
      //PROFILE_SCOPE(mpMts);
#if 1

      QuadTreeCont<MountainInfo> mountainPos;
      const AutoArray<Vector3> &mountains = GLandscape->GetMountains();

      {
        PROFILE_SCOPE_EX(locC,*);
        for (int i=0; i<mountains.Size(); i++)
        {
          const Vector3& pos = mountains[i];
          // check if given pos is in the visible range
          int x = toLargeInt(pos.X());
          int z = toLargeInt(pos.Z());

          mountainPos.Set(x,z,MountainInfo(pos,i));
        }
      }

      MountainRegion region;
      Vector3 minPos = ScreenToWorld(Point2DFloat(_x,_y));
      Vector3 maxPos = ScreenToWorld(Point2DFloat(_x+_w,_y+_h));
      region._xMin = floatMin(minPos.X(),maxPos.X());
      region._zMin = floatMin(minPos.Z(),maxPos.Z());
      region._xMax = floatMax(minPos.X(),maxPos.X());
      region._zMax = floatMax(minPos.Z(),maxPos.Z());

      MountainList list;

      {
        PROFILE_SCOPE_EX(locL,*);
        mountainPos.ForEachInRegion(region,list);
      }

      float minDistF = 1000 * _scaleX;
      int minDist = toLargeInt( 1000 * _scaleX )+2;
      {
        PROFILE_SCOPE_EX(locD,*);
        for (int ii=0; ii<list._indices.Size(); ii++)
        {
          int i = list._indices[ii];
          // make space based query
          const Vector3 & pos = mountains[i];

          int x = toLargeInt(pos.X());
          int z = toLargeInt(pos.Z());
          SkipNear ctx(MountainInfo(pos,i),minDistF);

          if (!mountainPos.ForEachInRegion(DetectCircle(x,z,minDist),ctx))
          {
            DrawMount(pos);
          }
        }
      }
#endif
    }
#endif
  }
}

void CStaticMap::PreloadData(Vector3Par center)
{
  // calculate a new rectangle
  float xStep = InvLandRange * _invScaleX;
  float yStep = InvLandRange * _invScaleY;
  float invXStep = LandRange * _scaleX;
  float invYStep = LandRange * _scaleY;

  int iMin = toIntFloor(-_mapX * invXStep);
  int jMin = LandRange - toIntFloor(-_mapY * invYStep);

  // make sure viewed region is fully loaded
  if (!_mapUsed)
  {
    _mapUsed = new MapObjectListRectUsed;
  }
  // calculate object area
  int xBeg = iMin;
  int xEnd = xBeg + toIntCeil((_w + fastFmod(-_mapX, xStep))*invXStep);

  int zEnd = jMin;
  int zBeg = zEnd - toIntCeil((_h + fastFmod(-_mapY, yStep))*invYStep);

  // saturate borders
  if (xBeg<0) xBeg = 0;
  if (xEnd>LandRangeMask) xEnd = LandRangeMask;
  if (zBeg<0) zBeg = 0;
  if (zEnd>LandRangeMask) zEnd = LandRangeMask;

  if (_mapUsed)
  {
    GLandscape->PreloadMapObjects(*_mapUsed, xBeg, zBeg, xEnd, zEnd);
  }
}

/*!
\patch_internal 1.45 Date 2/21/2002 by Jirka
- Fixed: Added legend to map
\patch 5126 Date 2/2/2007 by Jirka
- New: Map legend parameters are now read from the resource
*/

void CStaticMap::DrawLegend()
{
  if (!_showCountourInterval) return;

  float w = _wScreen;
  float h = _hScreen;

  float cx = CX(_legend._x), cy = CY(_legend._y), cw = CW(_legend._w), ch = CH(_legend._h);

  Draw2DPars pars;
  pars.mip = GEngine->TextBank()->UseMipmap(NULL, 0, 0);
  pars.spec = NoZBuf|IsAlpha|ClampU|ClampV|IsAlphaFog;
  pars.SetColor(_legend._colorBackground);
  pars.SetU(0,1);
  pars.SetV(0,1);
  Rect2DPixel rect(cx, cy, cw, ch);
  GEngine->Draw2D(pars, rect, _clipRect);

  PackedColor color = _legend._color;
  GEngine->DrawLine(Line2DPixel(cx, cy, cx + cw, cy), color, color, _clipRect);
  GEngine->DrawLine(Line2DPixel(cx + cw, cy, cx + cw, cy + ch), color, color, _clipRect);
  GEngine->DrawLine(Line2DPixel(cx + cw, cy + ch, cx, cy + ch), color, color, _clipRect);
  GEngine->DrawLine(Line2DPixel(cx, cy + ch, cx, cy), color, color, _clipRect);

  float top = _legend._y + 0.01;
  float left = _legend._x + 0.01;

  float step = 70.0 * _scaleX;
  int stepexp = toIntFloor(log10(step));
  float step10 = pow(10.0, stepexp);
  step /= step10;
  if (step <= 2.0) step = 2.0;
  else if (step <= 5.0) step = 5.0;
  else step = 10.0;
  step *= step10;
  saturateMax(step, 1);

  GEngine->DrawTextF
    (
    Point2DFloat(left, top), _legend._size, 
    Rect2DFloat(_x, _y, _w, _h),
    _legend._font, color, 0, LocalizeString(IDS_MAP_INTERVAL), step
    );
  top += _legend._size;
  GEngine->DrawText
    (
    Point2DFloat(left, top), _legend._size, 
    Rect2DFloat(_x, _y, _w, _h),
    _legend._font, color, LocalizeString(IDS_MAP_LEGEND_INFO),0
    );
  top += 1.5 * _legend._size;

  // how much space is for scale picture
  float scaleSpace = _legend._w - 5.0f * _legend._size;
  step = LandGrid * LandRange * _scaleX * scaleSpace;
  // round to the 1, 2, 5, 10, ...
  stepexp = toIntFloor(log10(step));
  step10 = pow(10.0, stepexp);
  step /= step10;
  if (step >= 5) step = 5;
  else if (step >= 2) step = 2;
  else step = 1;
  step *= step10;

  float mapStep = InvLandGrid * InvLandRange * _invScaleX * step;

  GEngine->DrawLine(Line2DPixel(CX(left), CY(top), CX(left), CY(top + _legend._size)), color, color, _clipRect);
  GEngine->DrawLine(Line2DPixel(CX(left + mapStep), CY(top), CX(left + mapStep), CY(top + _legend._size)), color, color, _clipRect);
  GEngine->DrawLine(Line2DPixel(CX(left), CY(top + 0.5 * _legend._size), CX(left + mapStep), CY(top + 0.5 * _legend._size)), color, color, _clipRect);
  GEngine->DrawTextF
    (
    Point2DFloat(left + mapStep + 0.01, top), _legend._size, 
    Rect2DFloat(_x, _y, _w, _h),
    _legend._font, color, 0, LocalizeString(IDS_MAP_SCALE_FORMAT), step
    );
}

void CStaticMap::DrawName(Vector3Par pos, RString text, Font *font, float textSize, PackedColor color, int shadow)
{
  DrawCoord pt = WorldToScreen(pos);

  float size = textSize * (_invScaleX * 0.05);
  saturate(size, 0.5 * textSize, 2.0 * textSize);

  float h = size;
  float w = GEngine->GetTextWidth(size, font, text);

  if (pt.x + w < _x) return;
  if (pt.y + 0.5 * h < _y) return;
  if (pt.x > _x + _w) return;
  if (pt.y - 0.5 * h > _y + _h) return;

  GEngine->DrawText
    (
    Point2DFloat(pt.x, pt.y - 0.5 * h),
    size,
    Rect2DFloat(_x, _y, _w, _h),
    font,
    color,
    text,
    shadow
    );
}

void CStaticMap::DrawName(ParamEntryPar cls)
{
  Vector3 pos;
  pos.Init();
  pos[0] = (cls >> "position")[0];
  pos[2] = (cls >> "position")[1];
  pos[1] = 0;
  RString text = cls >> "name";
  DrawName(pos, text, _fontNames, _sizeNames, _colorNames, 0);
}

void CStaticMap::DrawMount(Vector3Par pos)
{
  DrawCoord pt = WorldToScreen(pos);

  char buffer[256];
  sprintf(buffer, "%.0f", pos.Y());

  float h = _sizeLevels;
  float w = GEngine->GetTextWidth(_sizeLevels, _fontLevels, buffer);

  if (pt.x + w + 0.005 < _x) return;
  if (pt.y + 0.5 * h < _y) return;
  if (pt.x > _x + _w) return;
  if (pt.y - 0.5 * h > _y + _h) return;

  int x = toIntFloor(pt.x * _wScreen);
  int y = toInt(pt.y * _hScreen);
  GEngine->DrawLine(Line2DPixel(x, y - 1, x + 1, y - 1), _colorCountlines, _colorCountlines, _clipRect);
  GEngine->DrawLine(Line2DPixel(x + 1, y - 1, x + 1, y + 1), _colorCountlines, _colorCountlines, _clipRect);
  GEngine->DrawLine(Line2DPixel(x + 1, y + 1, x, y + 1), _colorCountlines, _colorCountlines, _clipRect);
  GEngine->DrawLine(Line2DPixel(x, y + 1, x, y - 1), _colorCountlines, _colorCountlines, _clipRect);
  GEngine->DrawText
    (
    Point2DFloat(pt.x + 0.005, pt.y - 0.5 * h),
    _sizeLevels,
    Rect2DFloat(_x, _y, _w, _h),
    _fontLevels,
    _colorLevels,
    buffer, 0
    );
}

#define HEIGHT_LOG (GLandscape->GetSubdivLog())

#define GetHeightNT(j,i) GLandscape->GetHeight((j)<<HEIGHT_LOG,(i)<<HEIGHT_LOG)

/// draw background terrain texture
void CStaticMap::DrawField(
                           float x, float y, float xStep, float yStep,
                           int i, int j, int iStep, int jStep
                           )
{
  //if( InRange(i,j) || InRange(i+iStep,j+jStep) )
  {
    /*
    // draw only when there is a chance to draw anything

    const float waves = 5.0;
    //const float invWaves = 1.0 / (2.0 * waves);

    float heightTL = GetHeightNT(j, i);
    float heightTR = GetHeightNT(j, i + iStep);
    float heightBL = GetHeightNT(j + jStep, i);
    float heightBR = GetHeightNT(j + jStep, i + iStep);


    if(
    heightTL <= -waves && heightTR <= -waves &&
    heightBL <= -waves && heightBR <= -waves
    )
    {
    // simple case: sea only
    return;
    }
    */

    Draw2DParsExt pars;

    Texture *texture = NULL;

    pars.SetU(0, 1);
    pars.SetV(0, 1);

    Rect2DPixel rect(x * _wScreen, y * _hScreen, xStep * _wScreen, yStep * _hScreen);
    int mipLevel = _mipmapLevel;

    // load sat. map
    // where sat. map is not loaded, use white
    const Landscape::TextureInfo &info = GLandscape->ClippedTextureInfo(j+jStep,i);
    if (
      info._mat &&
      info._mat->GetPixelShaderID(0)>=PSTerrain1 && info._mat->GetPixelShaderID(0)<=PSTerrain15 ||
      info._mat->GetPixelShaderID(0)==PSTerrainX
      )
    {
      // sat. map always in stage 0
      Texture *sat = info._mat->_stage[0]._tex;

#if _VBS3_UDEFCHART
      // If non-satellite map is to be drawn, use the user chart
      if (_showScale)
      {
        // If we are in function CStaticMap::DrawField and non-satellite map is to be drawn, it can just mean the user chart was selected
        DoAssert(UserMapDrawing());

        // Get the user chart texture name
        RStringB userChartTextureName;
        {
          RStringB satName = sat->GetName();
          int bckSlashPos = satName.ReverseFind('\\');
          if (bckSlashPos >= 0)
          {
            userChartTextureName = GWorld->GetUserDefinedChartName() + satName.Substring(bckSlashPos, satName.GetLength());
          }
        }

        // We definitely don't want to use the sat texture
        sat = NULL;

        // Replace sat texture by the user defined one
        // Check if source does exist. If it doesn't exist use white texture
        if (!userChartTextureName.IsEmpty())
        {
          // Try to find the texture in the cache of stored textures
          bool userTextureFound = false;
          for (int i = 0; i < _userTextures.Size(); i++)
          {
            if (_userTextures[i]->GetName() == userChartTextureName)
            {
              sat = _userTextures[i];
              userTextureFound = true;
              break;
            }
          }

          // If texture not found, try to search through names that don't have corresponding files
          if (!userTextureFound)
          {
            // Look for names in not-found array
            bool userTextureNotFoundFound = false;
            for (int i = 0; i < _userTexturesNotFound.Size(); i++)
            {
              if (_userTexturesNotFound[i] == userChartTextureName)
              {
                sat = NULL;
                userTextureNotFoundFound = true;
                break;
              }
            }

            // Not found even in not-found names, do the loading
            if (!userTextureNotFoundFound)
            {
              ITextureSourceFactory *factory = SelectTextureSourceFactory(userChartTextureName);
              if (factory && factory->Check(userChartTextureName))
              {
                int newTextureIndex = _userTextures.Add(GlobLoadTexture(userChartTextureName));
                sat = _userTextures[newTextureIndex];
              }
              else
              {
                _userTexturesNotFound.Add(userChartTextureName);
                // Use the white texture
                sat = NULL;
              }
            }
          }
        }
      }
#endif
      // now we want to know the tex-gen for the stage
      const TexGenInfo &tgen = info._mat->_texGen[info._mat->_stage[0]._texGen];
      Assert(tgen._uvSource==UVWorldPos);
      // compute UV from world space coordinates
      Vector3 wPosTL(i*LandGrid,0,j*LandGrid);
      Vector3 wPosTR((i+iStep)*LandGrid,0,j*LandGrid);
      Vector3 wPosBL(i*LandGrid,0,(j+jStep)*LandGrid);
      Vector3 wPosBR((i+iStep)*LandGrid,0,(j+jStep)*LandGrid);
      Vector3 uvTL = tgen._uvTransform.FastTransform(wPosTL);
      Vector3 uvTR = tgen._uvTransform.FastTransform(wPosTR);
      Vector3 uvBL = tgen._uvTransform.FastTransform(wPosBL);
      Vector3 uvBR = tgen._uvTransform.FastTransform(wPosBR);
      pars.uTL = uvTL.X(), pars.vTL = uvTL.Y();
      pars.uTR = uvTR.X(), pars.vTR = uvTR.Y();
      pars.uBL = uvBL.X(), pars.vBL = uvBL.Y();
      pars.uBR = uvBR.X(), pars.vBR = uvBR.Y();

      // Determine the mip level
      if (sat)
      {
        // mipmap is log4(texel2/pixel2)
        // we assume mapping is rectangular - same U on left, same V on top
        float texArea = fabs(pars.uBR-pars.uTL)*fabs(pars.vBR-pars.vTL);
        float pixArea = rect.w*rect.h;

        float bestArea = sat->AWidth()*sat->AHeight();

        const float invLog4 = 0.72134752044; // 1.0 / log(4.0);
        //mipLevel = toIntFloor(log(pixArea/(texArea*bestArea)) * invLog4);
        mipLevel = toIntFloor(log(texArea*bestArea/pixArea) * invLog4);
        saturate(mipLevel,0,sat->ANMipmaps());
      }
      else
      {
        mipLevel = 0;
      }
      //0.05 - 0.15
      float scaleAlpha = (_scaleX > _alphaFadeStartScale)? ((_alphaFadeEndScale - _scaleX)/(_alphaFadeEndScale - _alphaFadeStartScale)) : 1.0f;
      texture = sat;
      pars.SetColor(PackedColor(255,255,255,scaleAlpha * _maxSatelliteAlpha * 255));
    }
    else
    {
      LogF("Only TerrainX shaders supported");
      float scaleAlpha = (_scaleX > _alphaFadeStartScale)? ((_alphaFadeEndScale - _scaleX)/(_alphaFadeEndScale - _alphaFadeStartScale)) : 1.0f;
      texture = NULL;
      pars.SetColor(PackedColor(255,255,255,scaleAlpha * _maxSatelliteAlpha * 255));
    }

    pars.spec = NoZBuf|IsAlpha|IsAlphaFog|NoClamp;
    pars.mip = GLOB_ENGINE->TextBank()->UseMipmap(texture, mipLevel+1, mipLevel);
    GLOB_ENGINE->Draw2D(pars, rect, _clipRect);
  }
}

void CStaticMap::DrawSea
(
 float x, float y, float xStep, float yStep,
 int i, int j, int iStep, int jStep
 )
{
  const float waves = 5.0;
  const float invWaves = 1.0 / (2.0 * waves);

  float heightTL = GetHeightNT(j, i);
  float heightTR = GetHeightNT(j, i + iStep);
  float heightBL = GetHeightNT(j + jStep, i);
  float heightBR = GetHeightNT(j + jStep, i + iStep);
  if
    (
    heightTL >= waves &&
    heightTR >= waves &&
    heightBL >= waves &&
    heightBR >= waves
    ) return;

  Draw2DPars pars;

  if(
    heightTL <= -waves && heightTR <= -waves &&
    heightBL <= -waves && heightBR <= -waves
    )
  {
    // simple case: sea only
    pars.colorTL = _colorSeaPacked;
    pars.colorTR = _colorSeaPacked;
    pars.colorBL = _colorSeaPacked;
    pars.colorBR = _colorSeaPacked;
  }
  else
  {

    heightTL += waves; heightTL *= invWaves;
    saturate(heightTL, 0, 1);
    heightTR += waves; heightTR *= invWaves;
    saturate(heightTR, 0, 1);
    heightBL += waves; heightBL *= invWaves;
    saturate(heightBL, 0, 1);
    heightBR += waves; heightBR *= invWaves;
    saturate(heightBR, 0, 1);

    Color color, colorLand;
    colorLand = _colorSea;
    colorLand.SetA(0);

    // TL
    color = colorLand * heightTL + _colorSea * (1 - heightTL);
    pars.colorTL = PackedColor(color);
    // TR
    color = colorLand * heightTR + _colorSea * (1 - heightTR);
    pars.colorTR = PackedColor(color);
    // BL
    color = colorLand * heightBL + _colorSea * (1 - heightBL);
    pars.colorBL = PackedColor(color);
    // BR
    color = colorLand * heightBR + _colorSea * (1 - heightBR);
    pars.colorBR = PackedColor(color);
  }

  Rect2DPixel rect(x * _wScreen, y * _hScreen, xStep * _wScreen, yStep * _hScreen);
  pars.mip = GLOB_ENGINE->TextBank()->UseMipmap(NULL, 0, 0);
  pars.spec = NoZBuf|IsAlpha|IsAlphaFog|NoClamp;

  pars.SetU(0, 1);
  pars.SetV(0, 1);

  GLOB_ENGINE->Draw2D(pars, rect, _clipRect);
}

void CStaticMap::DrawScale
(
 float x, float y, float xStep, float yStep,
 int i, int j, int iStep, int jStep
 )
{
  const float invMaxHeight = 1.0 / 700.0;

  float heightTL = GetHeightNT(j, i) * invMaxHeight;
  saturate(heightTL, 0, 1);
  float heightTR = GetHeightNT(j, i + iStep) * invMaxHeight;
  saturate(heightTR, 0, 1);
  float heightBL = GetHeightNT(j + jStep, i) * invMaxHeight;
  saturate(heightBL, 0, 1);
  float heightBR = GetHeightNT(j + jStep, i + iStep) * invMaxHeight;
  saturate(heightBR, 0, 1);

  Color color, colorL(0, 0, 0, 0), colorH(0, 0, 0, 1);
  Draw2DParsNoTex pars;

  // TL
  color = colorH * heightTL + colorL * (1 - heightTL);
  pars.colorTL = PackedColor(color);
  // TR
  color = colorH * heightTR + colorL * (1 - heightTR);
  pars.colorTR = PackedColor(color);
  // BL
  color = colorH * heightBL + colorL * (1 - heightBL);
  pars.colorBL = PackedColor(color);
  // BR
  color = colorH * heightBR + colorL * (1 - heightBR);
  pars.colorBR = PackedColor(color);

  Rect2DPixel rect(x * _wScreen, y * _hScreen, xStep * _wScreen, yStep * _hScreen);

  pars.spec = NoZBuf|IsAlpha|IsAlphaFog|NoClamp;
  GLOB_ENGINE->Draw2D(pars, rect, _clipRect);
}

inline static void DrawForestPoly3(
                                   float x1, float y1, float x2, float y2, float x3, float y3,
                                   PackedColor color, const Rect2DPixel &clipRect
                                   )
{
  const int n = 3;
  Vertex2DPixel vs[n];
  vs[0].u = 0;
  vs[0].v = 0;
  vs[0].x = x3;
  vs[0].y = y3;
  vs[0].color = color;
  vs[1].u = 0;
  vs[1].v = 0;
  vs[1].x = x2;
  vs[1].y = y2;
  vs[1].color = color;
  vs[2].u = 0;
  vs[2].v = 0;
  vs[2].x = x1;
  vs[2].y = y1;
  vs[2].color = color;
  MipInfo mip = GEngine->TextBank()->UseMipmap(NULL, 0, 0);
  GEngine->DrawPoly(mip, vs, n, clipRect);
}

inline static void DrawForestPoly4(
                                   float x1, float y1, float x2, float y2, float x3, float y3, float x4, float y4,
                                   PackedColor color, const Rect2DPixel &clipRect
                                   )
{
  const int n = 4;
  Vertex2DPixel vs[n];
  vs[0].u = 0;
  vs[0].v = 0;
  vs[0].x = x4;
  vs[0].y = y4;
  vs[0].color = color;
  vs[1].u = 0;
  vs[1].v = 0;
  vs[1].x = x3;
  vs[1].y = y3;
  vs[1].color = color;
  vs[2].u = 0;
  vs[2].v = 0;
  vs[2].x = x2;
  vs[2].y = y2;
  vs[2].color = color;
  vs[3].u = 0;
  vs[3].v = 0;
  vs[3].x = x1;
  vs[3].y = y1;
  vs[3].color = color;
  MipInfo mip = GEngine->TextBank()->UseMipmap(NULL, 0, 0);
  GEngine->DrawPoly(mip, vs, n, clipRect);
}

inline static void DrawForestPoly5(
                                   float x1, float y1, float x2, float y2, float x3, float y3, float x4, float y4, float x5, float y5,
                                   PackedColor color, const Rect2DPixel &clipRect
                                   )
{
  const int n = 5;
  Vertex2DPixel vs[n];
  vs[0].u = 0;
  vs[0].v = 0;
  vs[0].x = x5;
  vs[0].y = y5;
  vs[0].color = color;
  vs[1].u = 0;
  vs[1].v = 0;
  vs[1].x = x4;
  vs[1].y = y4;
  vs[1].color = color;
  vs[2].u = 0;
  vs[2].v = 0;
  vs[2].x = x3;
  vs[2].y = y3;
  vs[2].color = color;
  vs[3].u = 0;
  vs[3].v = 0;
  vs[3].x = x2;
  vs[3].y = y2;
  vs[3].color = color;
  vs[4].u = 0;
  vs[4].v = 0;
  vs[4].x = x1;
  vs[4].y = y1;
  vs[4].color = color;
  MipInfo mip = GEngine->TextBank()->UseMipmap(NULL, 0, 0);
  GEngine->DrawPoly(mip, vs, n, clipRect);
}

inline static void DrawForestLine(float x1, float y1, float x2, float y2, PackedColor color, const Rect2DPixel &clipRect)
{
  GEngine->DrawLineDo(Line2DPixel(x1, y1, x2, y2),color, color, clipRect);
}

void CStaticMap::DrawForestsNew(
                                const MapObjectList &list, int i, int j, const DrawForestContext &ctx, bool drawLines, bool rocks)
{
  // new style drawing (using isoarbors)
  float x = (ctx.xMin + (i - ctx.iMin) * ctx.xStep) * _wScreen;
  float y = (ctx.yMin + (ctx.jMin - j) * ctx.yStep) * _hScreen;
  float w = ctx.xStep * _wScreen;
  float h = ctx.yStep * _hScreen;

  PackedColor color = rocks ? _colorRocks : _colorForest;
  PackedColor colorBorder = rocks ? _colorRocksBorder : _colorForestBorder;

  for (int o=0; o<list.Size(); o++)
  {
    MapObject *obj = list[o];
    if (obj == NULL) continue;

    if (rocks)
    {
      if (obj->GetType() != MapRocks) continue;
    }
    else
    {
      if (obj->GetType() != MapForest) continue;
    }

    MapObjectForest *forest = static_cast<MapObjectForest *>(obj);

    int row = forest->_row;
    int col = forest->_col;

    float wGrid = w / forest->_cols;
    float hGrid = h / forest->_rows;

    float tl = forest->_tl;
    float tr = forest->_tr;
    float bl = forest->_bl;
    float br = forest->_br;
    float limit = 1.0f;

    int bits = 0;
    if (tl > limit) bits |= 8;
    if (tr > limit) bits |= 4;
    if (bl > limit) bits |= 2;
    if (br > limit) bits |= 1;
    switch (bits)
    {
    case 0:
      // nothing to draw - no forest
      // Fail("No forest");
      break;
    case 1:
      {
        // br
        float coef1 = (limit - bl) / (br - bl); 
        float coef2 = (limit - tr) / (br - tr);
        float x1 = x + (col + coef1) * wGrid;
        float y1 = y + h - (row + 1) * hGrid;
        float x2 = x + (col + 1) * wGrid;
        float y2 = y + h - (row + coef2) * hGrid;

        if (!drawLines) DrawForestPoly3(x1, y1, x2, y2, x2, y1, color, _clipRect);
        else DrawForestLine(x1, y1, x2, y2, colorBorder, _clipRect);
      }
      break;
    case 2:
      {
        // bl
        float coef1 = (limit - tl) / (bl - tl);
        float coef2 = (bl - limit) / (bl - br); 
        float x1 = x + col * wGrid;
        float y1 = y + h - (row + coef1) * hGrid;
        float x2 = x + (col + coef2) * wGrid;
        float y2 = y + h - (row + 1) * hGrid;

        if (!drawLines) DrawForestPoly3(x1, y1, x2, y2, x1, y2, color, _clipRect);
        else DrawForestLine(x1, y1, x2, y2, colorBorder, _clipRect);
      }
      break;
    case 3:
      {
        // bl, br
        float coef1 = (limit - tl) / (bl - tl);
        float coef2 = (limit - tr) / (br - tr); 
        float x1 = x + col * wGrid;
        float y1 = y + h - (row + coef1) * hGrid;
        float x2 = x + (col + 1) * wGrid;
        float y2 = y + h - (row + coef2) * hGrid;
        float y3 = y + h - (row + 1) * hGrid;

        if (!drawLines) DrawForestPoly4(x1, y1, x2, y2, x2, y3, x1, y3, color, _clipRect);
        else DrawForestLine(x1, y1, x2, y2, colorBorder, _clipRect);
      }
      break;
    case 4:
      {
        // tr
        float coef1 = (tr - limit) / (tr - br);
        float coef2 = (limit - tl) / (tr - tl); 
        float x1 = x + (col + 1) * wGrid;
        float y1 = y + h - (row + coef1) * hGrid;
        float x2 = x + (col + coef2) * wGrid;
        float y2 = y + h - row * hGrid;

        if (!drawLines) DrawForestPoly3(x1, y1, x2, y2, x1, y2, color, _clipRect);
        else DrawForestLine(x1, y1, x2, y2, colorBorder, _clipRect);
      }
      break;
    case 5:
      {
        // tr, br
        float coef1 = (limit - bl) / (br - bl);
        float coef2 = (limit - tl) / (tr - tl); 
        float x1 = x + (col + coef1) * wGrid;
        float y1 = y + h - (row + 1) * hGrid;
        float x2 = x + (col + coef2) * wGrid;
        float y2 = y + h - row * hGrid;
        float x3 = x + (col + 1) * wGrid;

        if (!drawLines) DrawForestPoly4(x1, y1, x2, y2, x3, y2, x3, y1, color, _clipRect);
        else DrawForestLine(x1, y1, x2, y2, colorBorder, _clipRect);
      }
      break;
    case 6:
      {
        // tr
        float coef1 = (tr - limit) / (tr - br);
        float coef2 = (limit - tl) / (tr - tl); 
        float x1 = x + (col + 1) * wGrid;
        float y1 = y + h - (row + coef1) * hGrid;
        float x2 = x + (col + coef2) * wGrid;
        float y2 = y + h - row * hGrid;

        if (!drawLines) DrawForestPoly3(x1, y1, x2, y2, x1, y2, color, _clipRect);
        else DrawForestLine(x1, y1, x2, y2, colorBorder, _clipRect);
      }
      {
        // bl
        float coef1 = (limit - tl) / (bl - tl);
        float coef2 = (bl - limit) / (bl - br); 
        float x1 = x + col * wGrid;
        float y1 = y + h - (row + coef1) * hGrid;
        float x2 = x + (col + coef2) * wGrid;
        float y2 = y + h - (row + 1) * hGrid;

        if (!drawLines) DrawForestPoly3(x1, y1, x2, y2, x1, y2, color, _clipRect);
        else DrawForestLine(x1, y1, x2, y2, colorBorder, _clipRect);
      }
      break;
    case 7:
      {
        // tr, bl, br
        float coef1 = (limit - tl) / (bl - tl);
        float coef2 = (limit - tl) / (tr - tl); 
        float x1 = x + col * wGrid;
        float y1 = y + h - (row + coef1) * hGrid;
        float x2 = x + (col + coef2) * wGrid;
        float y2 = y + h - row * hGrid;
        float x3 = x + (col + 1) * wGrid;
        float y3 = y + h - (row + 1) * hGrid;

        if (!drawLines) DrawForestPoly5(x1, y1, x2, y2, x3, y2, x3, y3, x1, y3, color, _clipRect);
        else DrawForestLine(x1, y1, x2, y2, colorBorder, _clipRect);
      }
      break;
    case 8:
      {
        // tl
        float coef1 = (tl - limit) / (tl - tr); 
        float coef2 = (tl - limit) / (tl - bl);
        float x1 = x + (col + coef1) * wGrid;
        float y1 = y + h - row * hGrid;
        float x2 = x + col * wGrid;
        float y2 = y + h - (row + coef2) * hGrid;

        if (!drawLines) DrawForestPoly3(x1, y1, x2, y2, x2, y1, color, _clipRect);
        else DrawForestLine(x1, y1, x2, y2, colorBorder, _clipRect);
      }
      break;
    case 9:
      {
        // tl
        float coef1 = (tl - limit) / (tl - tr); 
        float coef2 = (tl - limit) / (tl - bl);
        float x1 = x + (col + coef1) * wGrid;
        float y1 = y + h - row * hGrid;
        float x2 = x + col * wGrid;
        float y2 = y + h - (row + coef2) * hGrid;

        if (!drawLines) DrawForestPoly3(x1, y1, x2, y2, x2, y1, color, _clipRect);
        else DrawForestLine(x1, y1, x2, y2, colorBorder, _clipRect);
      }
      {
        // br
        float coef1 = (limit - bl) / (br - bl); 
        float coef2 = (limit - tr) / (br - tr);
        float x1 = x + (col + coef1) * wGrid;
        float y1 = y + h - (row + 1) * hGrid;
        float x2 = x + (col + 1) * wGrid;
        float y2 = y + h - (row + coef2) * hGrid;

        if (!drawLines) DrawForestPoly3(x1, y1, x2, y2, x2, y1, color, _clipRect);
        else DrawForestLine(x1, y1, x2, y2, colorBorder, _clipRect);
      }
      break;
    case 10:
      {
        // tl, bl
        float coef1 = (tl - limit) / (tl - tr);
        float coef2 = (bl - limit) / (bl - br); 
        float x1 = x + (col + coef1) * wGrid;
        float y1 = y + h - row * hGrid;
        float x2 = x + (col + coef2) * wGrid;
        float y2 = y + h - (row + 1) * hGrid;
        float x3 = x + col * wGrid;

        if (!drawLines) DrawForestPoly4(x1, y1, x2, y2, x3, y2, x3, y1, color, _clipRect);
        else DrawForestLine(x1, y1, x2, y2, colorBorder, _clipRect);
      }
      break;
    case 11:
      {
        // tl, bl, br
        float coef1 = (tl - limit) / (tl - tr);
        float coef2 = (limit - tr) / (br - tr);
        float x1 = x + (col + coef1) * wGrid;
        float y1 = y + h - row * hGrid;
        float x2 = x + (col + 1) * wGrid;
        float y2 = y + h - (row + coef2) * hGrid;
        float x3 = x + col * wGrid;
        float y3 = y + h - (row + 1) * hGrid;

        if (!drawLines) DrawForestPoly5(x1, y1, x2, y2, x2, y3, x3, y3, x3, y1, color, _clipRect);
        else DrawForestLine(x1, y1, x2, y2, colorBorder, _clipRect);
      }
      break;
    case 12:
      {
        // tl, tr
        float coef1 = (tr - limit) / (tr - br);
        float coef2 = (tl - limit) / (tl - bl); 
        float x1 = x + (col + 1) * wGrid;
        float y1 = y + h - (row + coef1) * hGrid;
        float x2 = x + col * wGrid;
        float y2 = y + h - (row + coef2) * hGrid;
        float y3 = y + h - row * hGrid;

        if (!drawLines) DrawForestPoly4(x1, y1, x2, y2, x2, y3, x1, y3, color, _clipRect);
        else DrawForestLine(x1, y1, x2, y2, colorBorder, _clipRect);
      }
      break;
    case 13:
      {
        // tl, tr, br
        float coef1 = (limit - bl) / (br - bl);
        float coef2 = (tl - limit) / (tl - bl); 
        float x1 = x + (col + coef1) * wGrid;
        float y1 = y + h - (row + 1) * hGrid;
        float x2 = x + col * wGrid;
        float y2 = y + h - (row + coef2) * hGrid;
        float x3 = x + (col + 1) * wGrid;
        float y3 = y + h - row * hGrid;

        if (!drawLines) DrawForestPoly5(x1, y1, x2, y2, x2, y3, x3, y3, x3, y1, color, _clipRect);
        else DrawForestLine(x1, y1, x2, y2, colorBorder, _clipRect);
      }
      break;
    case 14:
      {
        // tl, tr, bl
        float coef1 = (tr - limit) / (tr - br);
        float coef2 = (bl - limit) / (bl - br); 
        float x1 = x + (col + 1) * wGrid;
        float y1 = y + h - (row + coef1) * hGrid;
        float x2 = x + (col + coef2) * wGrid;
        float y2 = y + h - (row + 1) * hGrid;
        float x3 = x + col * wGrid;
        float y3 = y + h - row * hGrid;

        if (!drawLines) DrawForestPoly5(x1, y1, x2, y2, x3, y2, x3, y3, x1, y3, color, _clipRect);
        else DrawForestLine(x1, y1, x2, y2, colorBorder, _clipRect);
      }
      break;
    case 15:
      {
        // full forest
        float x1 = x + col * wGrid;
        float y1 = y + h - row * hGrid;
        float x2 = x + (col + 1) * wGrid;
        float y2 = y + h - (row + 1) * hGrid;

        if (!drawLines) DrawForestPoly4(x1, y1, x2, y1, x2, y2, x1, y2, color, _clipRect);
      }
      break;
    }
    /*
    // grid
    float x1 = x + col * wGrid;
    float y1 = y + h - row * hGrid;
    float x2 = x + (col + 1) * wGrid;
    float y2 = y + h - (row + 1) * hGrid;
    PackedColor color = PackedColor(Color(1, 0, 0, 1));
    GEngine->DrawLine(Line2DPixel(x1, y1, x1, y2), color, color, _clipRect);
    GEngine->DrawLine(Line2DPixel(x1, y2, x2, y2), color, color, _clipRect);
    GEngine->DrawLine(Line2DPixel(x2, y2, x2, y1), color, color, _clipRect);
    GEngine->DrawLine(Line2DPixel(x2, y1, x1, y1), color, color, _clipRect);
    */
  }
}


void CStaticMap::DrawBuilding(MapObject *obj, PackedColor color)
{
  DrawCoord mapTL = WorldToScreen(obj->GetTLPosition());
  DrawCoord mapTR = WorldToScreen(obj->GetTRPosition());
  DrawCoord mapBL = WorldToScreen(obj->GetBLPosition());
  DrawCoord mapBR = WorldToScreen(obj->GetBRPosition());

  const int n = 4;
  Vertex2DPixel vs[n];
  // 0
  vs[0].x = mapTL.x * _wScreen;
  vs[0].y = mapTL.y * _hScreen;
  vs[0].u = 0;
  vs[0].v = 0;
  vs[0].color = color;
  // 1
  vs[1].x = mapBL.x * _wScreen;
  vs[1].y = mapBL.y * _hScreen;
  vs[1].u = 0;
  vs[1].v = 0;
  vs[1].color = color;
  // 2
  vs[2].x = mapBR.x * _wScreen;
  vs[2].y = mapBR.y * _hScreen;
  vs[2].u = 0;
  vs[2].v = 0;
  vs[2].color = color;
  // 3
  vs[3].x = mapTR.x * _wScreen;
  vs[3].y = mapTR.y * _hScreen;
  vs[3].u = 0;
  vs[3].v = 0;
  vs[3].color = color;

  MipInfo mip = GLOB_ENGINE->TextBank()->UseMipmap(NULL, 0, 0);
  GLOB_ENGINE->DrawPoly(mip, vs, n, _clipRect);
}

void CStaticMap::DrawObjects(const MapObjectList &list, int i, int j)
{
  Vector3 defaultPos((i + 0.5) * LandGrid, 0, (j + 0.5) * LandGrid);
  for (int o=0; o<list.Size(); o++)
  {
    MapObject *obj = list[o];
    if (obj == NULL) continue;

    switch (obj->GetType())
    {
    case MapBuilding:
    case MapHouse:
    case MapFence:
    case MapWall:
      DrawBuilding(obj, obj->GetColor());
      break;
    case MapPowerLines:
      {
        DrawCoord mapTL = WorldToScreen(obj->GetTLPosition());
        DrawCoord mapTR = WorldToScreen(obj->GetTRPosition());
        GEngine->DrawLinePrepare();
        GEngine->DrawLineDo(
          Line2DPixel(mapTL.x * _wScreen, mapTL.y * _hScreen,
          mapTR.x * _wScreen, mapTR.y * _hScreen),
          _colorPowerLines, _colorPowerLines, _clipRect);
      }
      break;
    case MapRailWay:
      {        
        DrawCoord map1 = WorldToScreen(obj->GetTLPosition());
        DrawCoord map2 = WorldToScreen(obj->GetTRPosition());
        DrawCoord map3 = WorldToScreen(obj->GetBLPosition());

        GEngine->DrawLinePrepare();
        GEngine->DrawLineDo(
          Line2DPixel(map1.x * _wScreen, map1.y * _hScreen, map2.x * _wScreen, map2.y * _hScreen),
          _colorRailWay, _colorRailWay, _clipRect);

        if (!obj->HasMoreParts()) break;

        GEngine->DrawLinePrepare();
        GEngine->DrawLineDo(
          Line2DPixel(map2.x * _wScreen, map2.y * _hScreen,
          map3.x * _wScreen, map3.y * _hScreen),
          _colorRailWay, _colorRailWay, _clipRect);

      }
      break;
      // draw icon only
      MAP_TYPES_ICONS_1(MAP_TYPE_DRAW_ICON)
        // draw both icon and polygon
        MAP_TYPES_ICONS_2(MAP_TYPE_DRAW_BUILDING_AND_ICON)
    }

    if (_showIds && obj->GetType() != MapForest && obj->GetType() != MapRocks)
    {
      float ptsLand = 800.0 * _invScaleX * InvLandRange;  // avoid dependece on video resolution
      float invPtsLand = 1.0 / ptsLand;

      int iStep = toIntCeil(_ptsPerSquareCost * invPtsLand);
      float xStep = iStep * InvLandRange * _invScaleX;
      if (xStep>0.15)
      {
        // draw object ID
        DrawCoord pos = WorldToScreen(obj->GetPosition(defaultPos));
        const float size = 0.02;
        float width = GEngine->GetTextWidthF(size, _fontGrid, "%d", obj->GetId().Encode());
        float x = pos.x - 0.5 * width;
        float y = pos.y - 0.5 * size;
        MipInfo mip = GLOB_ENGINE->TextBank()->UseMipmap(NULL, 0, 0);
        GEngine->Draw2D
          (
          mip, PackedWhite,
          Rect2DPixel(x * _wScreen, y * _hScreen, width * _wScreen, size * _hScreen),
          Rect2DPixel(_x * _wScreen, _y * _hScreen, _w * _wScreen, _h * _hScreen)
          );
        GEngine->DrawTextF
          (
          Point2DFloat(x, y), size, 
          Rect2DFloat(_x, _y, _w, _h),
          _fontGrid, PackedBlack, 0, "%d", obj->GetId().Encode()
          );
      }
    }
  }
}

void CStaticMap::DrawForestBorders(const MapObjectList &list, int i, int j, const DrawForestContext &ctx)
{
  if (GLandscape->GetMinTreesInForestSquare() > 0) return;

  float x = ctx.xMin+(i-ctx.iMin)*ctx.xStep;
  float y = ctx.yMin+(ctx.jMin-j)*ctx.yStep;
  float w = ctx.xStep;
  float h = ctx.yStep;

  for (int o=0; o<list.Size(); o++)
  {
    MapObject *obj = list[o];
    if (obj == NULL) continue;
    switch (obj->GetType())
    {
    case MapForestTriangle:
      {
        float angle = obj->GetAngle();
        switch (toInt(angle * 2.0 / H_PI))
        {
        case -1:
        case 1:
          GLOB_ENGINE->DrawLineDo
            (
            Line2DPixel(x * _wScreen, y * _hScreen,
            (x + w) * _wScreen, (y + h) * _hScreen),
            _colorForestBorder, _colorForestBorder, _clipRect
            );
          break;
        case -2:
        case 0:
        case 2:
          GLOB_ENGINE->DrawLineDo
            (
            Line2DPixel(x * _wScreen, (y + h) * _hScreen,
            (x + w) * _wScreen, y * _hScreen),
            _colorForestBorder, _colorForestBorder, _clipRect
            );
          break;
        }
      }
      return;
    case MapForestSquare:
      {
        GeographyInfo geogr = GLOB_LAND->GetGeography(i, j+1);
        if (!geogr.u.forest)
        {
          GLOB_ENGINE->DrawLineDo
            (
            Line2DPixel(x * _wScreen, y * _hScreen,
            (x + w) * _wScreen, y * _hScreen),
            _colorForestBorder, _colorForestBorder, _clipRect
            );
        }
        geogr = GLOB_LAND->GetGeography(i, j - 1);
        if (!geogr.u.forest)
        {
          GLOB_ENGINE->DrawLineDo
            (
            Line2DPixel(x * _wScreen, (y + h) * _hScreen,
            (x + w) * _wScreen, (y + h) * _hScreen),
            _colorForestBorder, _colorForestBorder, _clipRect
            );
        }
        geogr = GLOB_LAND->GetGeography(i - 1, j);
        if (!geogr.u.forest)
        {
          GLOB_ENGINE->DrawLineDo
            (
            Line2DPixel(x * _wScreen, y * _hScreen,
            x * _wScreen, (y + h) * _hScreen),
            _colorForestBorder, _colorForestBorder, _clipRect
            );
        }
        geogr = GLOB_LAND->GetGeography(i + 1, j);
        if (!geogr.u.forest)
        {
          GLOB_ENGINE->DrawLineDo
            (
            Line2DPixel((x + w) * _wScreen, y * _hScreen,
            (x + w) * _wScreen, (y + h) * _hScreen),
            _colorForestBorder, _colorForestBorder, _clipRect
            );
        }
      }
      return;
    }
  }
}

void CStaticMap::DrawRoads(LineArray &lines, const MapObjectList &listU, int i, int j)
{
  const MapObjectList &list = listU;
  for (int o=0; o<list.Size(); o++)
  {
    MapObject *obj = list[o];
    if (obj == NULL) continue;
    PackedColor color;
    PackedColor colorFill;
    switch (obj->GetType())
    {
    case MapTrack:
      color = _colorTracks;
      colorFill = _colorTracksFill;
      break;
    case MapRoad:
      color = _colorRoads;
      colorFill = _colorRoadsFill;
      break;
    case MapMainRoad:
      color = _colorMainRoads;
      colorFill = _colorMainRoadsFill;
      break;
    default:
      // not a road
      continue;
    }

    if (obj->GetType() == MapRoad || obj->GetType() == MapTrack || obj->GetType() == MapMainRoad)
    {
      // if there are many lines, we flush to avoid memory allocation
      if (lines.Size()>AllocLines-2)
      {
        GEngine->DrawLines(lines.Data(),lines.Size(),_clipRect);
        lines.Resize(0);
      }
      DrawCoord mapTL = WorldToScreen(obj->GetTLPosition());
      DrawCoord mapTR = WorldToScreen(obj->GetTRPosition());
      DrawCoord mapBL = WorldToScreen(obj->GetBLPosition());
      DrawCoord mapBR = WorldToScreen(obj->GetBRPosition());

      if (color.A8() > 0)
      {
        Line2DPixelInfo &line1 = lines.Append();
        line1.beg = Point2DPixel(mapTL.x * _wScreen, mapTL.y * _hScreen);
        line1.end = Point2DPixel(mapBL.x * _wScreen, mapBL.y * _hScreen);
        line1.c0 = color;
        line1.c1 = color;

        Line2DPixelInfo &line2 = lines.Append();
        line2.beg = Point2DPixel(mapTR.x * _wScreen, mapTR.y * _hScreen);
        line2.end = Point2DPixel(mapBR.x * _wScreen, mapBR.y * _hScreen);
        line2.c0 = color;
        line2.c1 = color;
      }
      if (colorFill.A8() > 0)
      {
        const int n = 4;
        Vertex2DPixel vs[n];
        // 0
        vs[0].x = mapTL.x * _wScreen;
        vs[0].y = mapTL.y * _hScreen;
        vs[0].u = 0;
        vs[0].v = 0;
        // 1
        vs[1].x = mapBL.x * _wScreen;
        vs[1].y = mapBL.y * _hScreen;
        vs[1].u = 1;
        vs[1].v = 0;
        // 2
        vs[2].x = mapBR.x * _wScreen;
        vs[2].y = mapBR.y * _hScreen;
        vs[2].u = 1;
        vs[2].v = 1;
        // 3
        vs[3].x = mapTR.x * _wScreen;
        vs[3].y = mapTR.y * _hScreen;
        vs[3].u = 0;
        vs[3].v = 1;

        MipInfo mip = GEngine->TextBank()->UseMipmap(NULL, 0, 0);
        for (int i=0; i<4; i++)
        {
          vs[i].color = colorFill;
        }
        GEngine->DrawPoly(mip, vs, n, _clipRect);
      }
    }
  }
}

/*!
\patch 1.62 Date 5/31/2002 by Ondra
- Fixed: Map contour drawing was not correct in some places
with highly variable gradient.
*/

void CStaticMap::DrawLines
(
 LineArray &lines, DrawCoord *pt, float *height,
 float step, float stepMain, float minLevel, float maxLevel, PackedColor color, PackedColor colorMain
 )
{
  float invStep = 1.0 / step;

  int n0, n1, n2, nt;
  float xs, ys, xe, ye;
  // original (wrong):
  // draw triangle <0, 1, 2>
  // draw triangle <1, 2, 3>
  // correct:
  // draw triangle <0, 1, 3>
  // draw triangle <0, 2, 3>
  for (int t=0; t<2; t++)
  {
    // t = 0, 1
    // draw triangel <t, t+ 1, t+2>
    n0 = 0; n1 = t + 1; n2 = 3;
    // sort vertices by height
    if (height[n0] > height[n1])
    { // swap n0, n1
      nt = n0; n0 = n1; n1 = nt;
    }
    if (height[n0] > height[n2])
    { // swap n0, n2
      nt = n0; n0 = n2; n2 = nt;
    }
    if (height[n1] > height[n2])
    { // swap n1, n2
      nt = n1; n1 = n2; n2 = nt;
    }

    // fastCeil
    float level = step * toIntCeil(height[n0] * invStep);
    saturateMax(level, minLevel);
    float toLevel = floatMin(height[n2], maxLevel);
    for (; level<=toLevel; level+=step)
    {
      PackedColor col;
      if (fastFmod(level + 0.5 * step, stepMain) < step)
        col = colorMain;
      else
        col = color;
      if (height[n2] == height[n0])
      {
        // all 3 points have the same height - draw the triangle
        {
          Line2DPixelInfo &line = lines.Append();
          line.beg = Point2DPixel(pt[n0].x * _wScreen, pt[n0].y * _hScreen);
          line.end = Point2DPixel(pt[n1].x * _wScreen, pt[n1].y * _hScreen);
          line.c0 = col;
          line.c1 = col;
        }
        {
          Line2DPixelInfo &line = lines.Append();
          line.beg = Point2DPixel(pt[n1].x * _wScreen, pt[n1].y * _hScreen);
          line.end = Point2DPixel(pt[n2].x * _wScreen, pt[n2].y * _hScreen);
          line.c0 = col;
          line.c1 = col;
        }
        {
          Line2DPixelInfo &line = lines.Append();
          line.beg = Point2DPixel(pt[n2].x * _wScreen, pt[n2].y * _hScreen);
          line.end = Point2DPixel(pt[n0].x * _wScreen, pt[n0].y * _hScreen);
          line.c0 = col;
          line.c1 = col;
        }
        continue;
      }
      // draw one line (at level <level>)
      float coef = (level - height[n0]) * (1.0 / (height[n2] - height[n0]));
      xe = pt[n0].x + coef * (pt[n2].x - pt[n0].x);
      ye = pt[n0].y + coef * (pt[n2].y - pt[n0].y);
      if (level == height[n1])
      {
        xs = pt[n1].x;
        ys = pt[n1].y;
      }
      else if (level < height[n1])
      {
        float coef = (level - height[n0]) * (1.0 / (height[n1] - height[n0]));
        xs = pt[n0].x + coef * (pt[n1].x - pt[n0].x);
        ys = pt[n0].y + coef * (pt[n1].y - pt[n0].y);
      }
      else
      {
        float coef = (level - height[n1]) * (1.0 / (height[n2] - height[n1]));
        xs = pt[n1].x + coef * (pt[n2].x - pt[n1].x);
        ys = pt[n1].y + coef * (pt[n2].y - pt[n1].y);
      }
      // draw line from <xs, ys> to <xe, ye>
      Line2DPixelInfo &line = lines.Append();
      line.beg = Point2DPixel(xs * _wScreen, ys * _hScreen);
      line.end = Point2DPixel(xe * _wScreen, ye * _hScreen);
      line.c0 = col;
      line.c1 = col;
    }
  }
}

/** draw map contour lines */

void CStaticMap::DrawCountlines
(
 LineArray &lines,
 float x, float y, float xStep, float yStep,
 int i, int j, int iStep, int jStep,
 float step, float stepMain, float maxLevel
 )
{
  // if there are many lines, we flush to avoid memory allocation
  if (lines.Size()>AllocLines-2)
  {
    GEngine->DrawLines(lines.Data(),lines.Size(),_clipRect);
    lines.Resize(0);
  }
  // insert bounds into fields
  DrawCoord pt[4];
  float height[4];
  pt[0].x = x; pt[0].y = y;
  pt[1].x = x + xStep; pt[1].y = y;
  pt[2].x = x; pt[2].y = y + yStep;
  pt[3].x = x + xStep; pt[3].y = y + yStep;
  height[0] = GLOB_LAND->ClippedDataXZ(i, j);
  height[1] = GLOB_LAND->ClippedDataXZ(i + iStep, j);
  height[2] = GLOB_LAND->ClippedDataXZ(i, j + jStep);
  height[3] = GLOB_LAND->ClippedDataXZ(i + iStep, j + jStep);

  DrawLines(lines, pt, height, step, stepMain, -maxLevel, 0.5 * step, _colorCountlinesWater, _colorMainCountlinesWater);
  DrawLines(lines, pt, height, step, stepMain, step, maxLevel, _colorCountlines, _colorMainCountlines);

}

/*!
\patch_internal 1.45 Date 2/21/2002 by Jirka
- Improved: Map grid format controlled by config
*/

void CStaticMap::DrawGrid()
{
  float sizeLand = LandGrid * LandRange;
  float invSizeLand = 1.0f / sizeLand;

  const GridInfo *info = GWorld->GetGridInfo(_scaleX);
  if (!info) return;

  float offsetX = GWorld->GetGridOffsetX();
  float offsetY = GWorld->GetGridOffsetY();

  float coefX = sizeLand * info->invStepX;
  float invCoefX = invSizeLand * info->stepX;

  float coefY = sizeLand * info->invStepY;
  float invCoefY = invSizeLand * info->stepY;

  int xFrom = TO_INT(coefX, (-_mapX * _scaleX * sizeLand - offsetX) * info->invStepX);
  int zFrom = TO_INT(coefY, (-_mapY * _scaleY * sizeLand - offsetY) * info->invStepY);
  int xTo = TO_INT(coefX, ((_w - _mapX) * _scaleX * sizeLand - offsetX) * info->invStepX);
  int zTo = TO_INT(coefY, ((_h - _mapY) * _scaleY * sizeLand - offsetY) * info->invStepY);

  float xBeg = _x + _mapX + (xFrom * info->stepX + offsetX) * _invScaleX * invSizeLand;
  float zBeg = _y + _mapY + (zFrom * info->stepY + offsetY) * _invScaleY * invSizeLand;
  float xFld = fabs(_invScaleX * invCoefX);
  float zFld = fabs(_invScaleY * invCoefY);

  float h = _sizeGrid;
  const float textBorder = TEXT_BORDER;

  float zAct = zBeg;
  int zSign = coefY > 0 ? 1 : -1; 
  AspectSettings asp;
  GEngine->GetAspectSettings(asp);

  for (int z = zFrom; zSign * (zTo - z) >= 0; z+=zSign)
  {
    BString<32> buffer;
    GridFormat(buffer, info->formatY, zSign == 1 ? z : z - 1);

    float width = GEngine->GetTextWidth(_sizeGrid, _fontGrid, buffer);

    float offset = _gridNumbersOverLines ?  (width + textBorder + 0.001) * _wScreen : 0;

    if (zAct >= _y && zAct <= _y + _h)
    {
      GLOB_ENGINE->DrawLine
        (
        Line2DPixel(_clipRect.x + offset, zAct * _hScreen,
        _clipRect.x + _clipRect.w - offset, zAct * _hScreen),
        _colorGridMap, _colorGridMap
        );
      if(!(z % 10))
        GLOB_ENGINE->DrawLine
        (
        Line2DPixel(_clipRect.x + offset, zAct * _hScreen +1,
        _clipRect.x + _clipRect.w - offset, zAct * _hScreen +1),
        _colorGridMap, _colorGridMap
        );
    }

    //wide screen map-coordinates position
    float ratio =(float)GEngine->WidthBB()/max(GEngine->Width2D(),1);
#ifdef _XBOX
    float safe = 0.075; // Safe area (85 %)
    float sfX =  (-asp.uiTopLeftX + safe) * ratio;
#else
    float sfX =  (-asp.uiTopLeftX) * ratio;
#endif

#if _VBS3_UDEFCHART
    float left = _x + textBorder;
    float right = _x + _w - width - textBorder;
#else
    float left = max(_x, sfX) + textBorder;
    float right = min(_x + _w, 1-sfX) - width - textBorder;
#endif
    float top = _gridNumbersOverLines ? (zAct + zFld - 0.5 * _sizeGrid) : (zAct + 0.5 * (zFld - h));

    GLOB_ENGINE->DrawText
      (
      Point2DFloat(left, top), _sizeGrid,
      Rect2DFloat(_x, _y, _w, _h),
      _fontGrid, _colorGrid, buffer, 0
      );
    GLOB_ENGINE->DrawText
      (
      Point2DFloat(right, top), _sizeGrid,
      Rect2DFloat(_x, _y, _w, _h),
      _fontGrid, _colorGrid, buffer, 0
      );
    zAct += zFld;
  }

  float xAct = xBeg;
  int xSign = coefX > 0 ? 1 : -1; 

  float offset = _gridNumbersOverLines ?  _sizeGrid * _hScreen : 0;

  for (int x = xFrom; xSign * (xTo - x) >= 0; x+=xSign)
  {
    if (xAct >= _x && xAct <= _x + _w)
    {
      GLOB_ENGINE->DrawLine
        (
        Line2DPixel(xAct * _wScreen, _clipRect.y + offset,
        xAct * _wScreen, _clipRect.y + _clipRect.h - offset),
        _colorGridMap, _colorGridMap
        );
      if(!(x % 10))
        GLOB_ENGINE->DrawLine
        (
        Line2DPixel(xAct * _wScreen+1, _clipRect.y + offset,
        xAct * _wScreen+1, _clipRect.y + _clipRect.h - offset),
        _colorGridMap, _colorGridMap
        );
    }

    BString<32> buffer;
    GridFormat(buffer, info->formatX, xSign == 1 ? x : x - 1);

    float left = _gridNumbersOverLines ? 
      xAct - 0.5 * GEngine->GetTextWidth(_sizeGrid, _fontGrid, buffer)
      : xAct + 0.5 * (xFld - GEngine->GetTextWidth(_sizeGrid, _fontGrid, buffer));

    //wide screen map-coordinates position
    float ratio =(float)GEngine->WidthBB()/max(GEngine->Width2D(),1);
#ifdef _XBOX
    float safe = 0.075; // Safe area (85 %)
    float sfY =  (-asp.uiTopLeftY + safe) * ratio;
#else
    float sfY =  (-asp.uiTopLeftY) * ratio;
#endif

#if _VBS3_UDEFCHART
    float top = _y;
    float bottom = _y + _h - _sizeGrid;
#else
    float top = max(_y, sfY);
    float bottom = min(_y + _h, 1-sfY) - _sizeGrid;
#endif
    GLOB_ENGINE->DrawText
      (
      Point2DFloat(left, top), _sizeGrid,
      Rect2DFloat(_x, _y, _w, _h),
      _fontGrid, _colorGrid, buffer, 0
      ); 
    GLOB_ENGINE->DrawText
      (
      Point2DFloat(left, bottom), _sizeGrid,
      Rect2DFloat(_x, _y, _w, _h),
      _fontGrid, _colorGrid, buffer, 0
      );
    xAct += xFld;
  }
}

void CStaticMap::OnHide()
{
  _mapUsed.Free();
}

//static ActionResponseCurve MapScrollX(0.20,1,2.0);
//static ActionResponseCurve MapScrollY(0.20,1,2.0);

void CStaticMap::OnDraw(UIViewport *vp, float alpha)
{
  Precalculate();

  float dt = Glob.uiTime - _moveLast;
  saturateMin(dt, 0.1); // minimal fps 10 - avoid jumps of cursor

  bool canAnimate = true;

  if (static_cast<Display *>(_parent.GetLink()) != GWorld->Map() || GWorld->IsShowMapWanted()) // check only for in-game map
  {
#ifdef _XBOX
    // Xbox doesn't call OnMouseHold
    if (!_moving && IsInside(0.5, 0.5))
      _infoMove = FindSign(0.5, 0.5);

    IControl *ctrl = _parent->GetCtrl(IDC_NOTEPAD_PICTURE);
    if (!_interpolator && (!ctrl || !ctrl->IsVisible()) && !_parent->Child())
    {
      float moveX = GInput.GetXInputButton(XBOX_RightThumbXRight) - GInput.GetXInputButton(XBOX_RightThumbXLeft);
      moveX = _mapScrollX->ApplyWithDeadZone(moveX,_mapScrollXDZone,1);
      if (moveX != 0)
      {
        ClearAnimation();
        _mapX -= moveX * dt; SaturateX(_mapX);
      }
      float moveY = GInput.GetXInputButton(XBOX_RightThumbYDown) - GInput.GetXInputButton(XBOX_RightThumbYUp);
      moveY = _mapScrollY->ApplyWithDeadZone(moveY,_mapScrollYDZone,1);
      if (moveY != 0)
      {
        ClearAnimation();
        _mapY -= moveY * dt; SaturateY(_mapY);
      }

      float scale = _scaleDefault - (_scaleDefault - _scaleMin) * GInput.GetXInputButton(XBOX_LeftTrigger);
      if (scale != _scaleX)
      {
        ClearAnimation();
        SetScale(scale);
      }
      // GEngine->ShowMessage(100, "Scale %g", scale);
    }

    _moveLast = Glob.uiTime;
#endif

    if (_moveKey != 0)
    {
      _moveLast = Glob.uiTime;
      if (Glob.uiTime - _moveStart < 0.5) dt *= 0.5;

      switch (_moveKey)
      {
      case DIK_ADD:
        SetScale(exp(-dt) * GetScale());
        break;
      case DIK_SUBTRACT:
        SetScale(exp(dt) * GetScale());
        break;
      case DIK_NUMPAD1:
        _mapX += dt; SaturateX(_mapX);
        _mapY -= dt; SaturateY(_mapY);
        break;
      case DIK_NUMPAD2:
        _mapY -= dt; SaturateY(_mapY);
        break;
      case DIK_NUMPAD3:
        _mapX -= dt; SaturateX(_mapX);
        _mapY -= dt; SaturateY(_mapY);
        break;
      case DIK_NUMPAD4:
        _mapX += dt; SaturateX(_mapX);
        break;
      case DIK_NUMPAD6:
        _mapX -= dt; SaturateX(_mapX);
        break;
      case DIK_NUMPAD7:
        _mapX += dt; SaturateX(_mapX);
        _mapY += dt; SaturateY(_mapY);
        break;
      case DIK_NUMPAD8:
        _mapY += dt; SaturateY(_mapY);
        break;
      case DIK_NUMPAD9:
        _mapX -= dt; SaturateX(_mapX);
        _mapY += dt; SaturateY(_mapY);
        break;
      }
      canAnimate = false;
    }
  }

  if (canAnimate && _interpolator)
  {
    float t = Glob.uiTime - _animationStart;
    Vector3 pos = (*_interpolator)(t).Position();
    _scaleX = exp(pos[1]);
    pos[1] = 0;
    saturate(_scaleX, _scaleMin, _scaleMax);
    Precalculate();
    Center(pos, false);
    _animationLast = Glob.uiTime;
    if (t > _interpolator->MaxArg())
      ClearAnimation();
    else
    {
      float predictTime = 0.2f;
      Vector3 predictPos = (*_interpolator)(t+predictTime).Position();
      PreloadData(predictPos);
    }
  }

  DrawBackground();

  if (!UserMapIsBeingDrawn())
  {
#if _ENABLE_CHEATS
    if (_show)
#endif
#if _VBS3
      if(GVBSVisuals.GetShowGrid())
#endif
        DrawGrid();
  }
  DrawExt(alpha);
#ifndef _XBOX
  OnEvent(CEDraw);
#endif

  if (_parent && _parent->IsTop() && !GWorld->GetCameraEffect())
  {
    RString cursorName = _parent->GetCursorName();
    if (cursorName.GetLength() > 0 && stricmp(cursorName, "Arrow") != 0)
    {
      int wScreen = GLOB_ENGINE->Width2D();
      int hScreen = GLOB_ENGINE->Height2D();
      const float mouseH = 16.0 / 600;
      const float mouseW = 16.0 / 800;
#ifdef _XBOX
      float mouseX = 0.5;
      float mouseY = 0.5;
#else
      float mouseX = 0.5 + GInput.cursorX * 0.5;
      float mouseY = 0.5 + GInput.cursorY * 0.5;
#endif
      PackedColor color = _parent->GetCursorColor();
      MipInfo mip = GEngine->TextBank()->UseMipmap(NULL, 0, 0);
      float pt = mouseX - mouseW;
      if (pt > _x)
        GEngine->Draw2D
        (
        mip, color,
        Rect2DPixel(wScreen * _x, hScreen * mouseY - 0.5 * _cursorLineWidth, wScreen * (pt - _x), _cursorLineWidth)
        );
      pt = mouseX + mouseW;
      if (pt < _x + _w)
        GEngine->Draw2D
        (
        mip, color,
        Rect2DPixel(wScreen * pt, hScreen * mouseY - 0.5 * _cursorLineWidth, wScreen * (_x + _w - pt), _cursorLineWidth)
        );
      pt = mouseY - mouseH;
      if (pt > _y)
        GEngine->Draw2D
        (
        mip, color,
        Rect2DPixel(wScreen * mouseX - 0.5 * _cursorLineWidth, hScreen * _y, _cursorLineWidth, hScreen * (pt - _y))
        );
      pt = mouseY + mouseH;
      if (pt < _y + _h)
        GEngine->Draw2D
        (
        mip, color,
        Rect2DPixel(wScreen * mouseX - 0.5 * _cursorLineWidth, hScreen * pt, _cursorLineWidth, hScreen * (_y + _h - pt))
        );
    }
  }
}

// mouse / keyboard events
bool CStaticMap::OnKeyUp(int dikCode)
{
  switch (dikCode)
  {
  case DIK_ADD:
  case DIK_SUBTRACT:
  case DIK_NUMPAD1:
  case DIK_NUMPAD2:
  case DIK_NUMPAD3:
  case DIK_NUMPAD4:
  case DIK_NUMPAD6:
  case DIK_NUMPAD7:
  case DIK_NUMPAD8:
  case DIK_NUMPAD9:
    // case DIK_NUMPAD5:
    _moveKey = 0;
    return true;
  default:
    return false;
  }
}

bool CStaticMap::OnKeyDown(int dikCode)
{
  switch (dikCode)
  {
  case DIK_ADD:
  case DIK_SUBTRACT:
  case DIK_NUMPAD1:
  case DIK_NUMPAD2:
  case DIK_NUMPAD3:
  case DIK_NUMPAD4:
  case DIK_NUMPAD6:
  case DIK_NUMPAD7:
  case DIK_NUMPAD8:
  case DIK_NUMPAD9:
    ClearAnimation();
    if (_moveKey != dikCode)
    {
      _moveKey = dikCode;
      _moveStart = Glob.uiTime;
      _moveLast = Glob.uiTime;
    }
    return true;
    /*
    case DIK_NUMPAD5:
    Center();
    return true;
    */
  case DIK_MULTIPLY:
    _moveKey = 0;
    float time;
    if (_scaleDefault > _scaleX)
      time = log(_scaleDefault / _scaleX);
    else
      time = log(_scaleX / _scaleDefault);
    ClearAnimation();
    AddAnimationPhase(time, _scaleDefault, GetCenter());
    CreateInterpolator();
    return true;
  default:
    return false;
  }
}

void ExportWMF(const char *name, bool grid
#if _VBS3
               ,bool contour = true
#endif
               );

void CStaticMap::ProcessCheats()
{
  if (GInput.CheatActivated() == CheatExportMap)
  {
    char buffer[256];
    sprintf(buffer, "C:\\%s.emf", Glob.header.worldname);
    ExportWMF(buffer, true);
    GInput.CheatServed();
  }
#if _ENABLE_CHEATS
  if (GInput.GetCheat2ToDo(DIK_D))
  {
    char buffer[256];
    sprintf(buffer, "C:\\%s.emf", Glob.header.worldname);
    ExportWMF(buffer, true);
  }
  if (GInput.GetCheat2ToDo(DIK_F))
  {
    char buffer[256];
    sprintf(buffer, "C:\\%s.emf", Glob.header.worldname);
    ExportWMF(buffer, false);
  }
  /*
  if (GInput.GetCheat1ToDo(DIK_V))
  {
  _showCost = !_showCost;
  }
  */
  /*
  if (GInput.GetCheat1ToDo(DIK_B))
  {
  _showScale = !_showScale;
  }
  */
  if (GInput.GetCheat1ToDo(DIK_N))
  {
    _show = !_show;
  }
  if (GInput.GetCheat1ToDo(DIK_W))
  {
    switch (_infoClick._type)
    {
    case signUnit:
      if (_infoClick._unit)
      {
        EntityAI *veh = _infoClick._unit->GetVehicle();
        if (veh)
        {
          GLOB_WORLD->SwitchCameraTo(veh, GLOB_WORLD->GetCameraType(), true);
          GWorld->UI()->ResetHUD();
        }
      }
      break;
    case signVehicle:
    case signStatic:
      if (_infoClick._id!=NULL)
      {
        GLOB_WORLD->SwitchCameraTo(_infoClick._id, GLOB_WORLD->GetCameraType(), true);
        GWorld->UI()->ResetHUD();
      }
      break;
    }
  }
#endif
}

void CStaticMap::OnRButtonDown(float x, float y)
{
  _mouseWorld = ScreenToWorld(DrawCoord(x, y));
  _moving = true;
}

void CStaticMap::OnRButtonUp(float x, float y)
{
  _moving = false;
}

void CStaticMap::OnMouseHold(float x, float y, bool active)
{
  if (!_moving && IsInside(x, y))
    _infoMove = FindSign(x, y);
}

void CStaticMap::OnMouseMove(float x, float y, bool active)
{
#ifndef _XBOX
  if (_moving)
  {
    DrawCoord mouseMap = WorldToScreen(_mouseWorld);
    _mapX += x - mouseMap.x;
    SaturateX(_mapX);
    _mapY += y - mouseMap.y;
    SaturateY(_mapY);
  }
  else
#endif
  {
    OnMouseHold(x, y);
    return;
  }
}

void CStaticMap::OnMouseZChanged(float dz)
{
  if (dz == 0) return;

  _moveKey = 0;
  float scale = exp(0.1 * dz) * _scaleX;
  saturate(scale, _scaleMin, _scaleMax);
  SetScale(scale);
}

void CStaticMap::ClearAnimation()
{
  _animation.Resize(0);
  _animMatrices.Free();
  _animTimes.Free();
  _interpolator = NULL;
}

void CStaticMap::AddAnimationPhase(float dt, float scale, Vector3Par pos)
{
  int i = _animation.Add();
  MapAnimationPhase &phase = _animation[i];
  if (i == 0)
  {
    _animationStart = Glob.uiTime;
    _animationLast = Glob.uiTime;
    phase.time = Glob.uiTime + dt;
  }
  else
  {
    phase.time = _animation[i - 1].time + dt;
  }
  phase.scale = scale;
  phase.pos = pos;
}

void CStaticMap::CreateInterpolator()
{
  int n = _animation.Size();

  _animTimes.Realloc(n + 1);
  _animMatrices.Realloc(n + 1);
  Vector3 pos;

  pos = GetCenter();
  pos[1] = log(_scaleX);
  _animTimes[0] = 0;
  _animMatrices[0].SetPosition(pos);
  _animMatrices[0].SetOrientation(M3Identity);
  for (int i=0; i<n; i++)
  {
    pos = _animation[i].pos;
    pos[1] = log(_animation[i].scale);
    _animTimes[i + 1] = _animation[i].time - _animationStart;
    _animMatrices[i + 1].SetPosition(pos);
    _animMatrices[i + 1].SetOrientation(M3Identity);
  }
  _interpolator = new InterpolatorLinear(_animMatrices, _animTimes, n + 1);
}

Vector3 CStaticMap::GetCenter()
{
  DrawCoord pt(_center.x + _x, _center.y + _y);
  return ScreenToWorld(pt);
}

// implementation
bool CStaticMap::UserMapDrawing() const
{
#if _VBS3_UDEFCHART
  return !GWorld->GetUserDefinedChartName().IsEmpty();
#else
  return false;
#endif
}

bool CStaticMap::UserMapIsBeingDrawn() const
{
#if _VBS3_UDEFCHART
  return UserMapDrawing() && _showScale;
#else
  return false;
#endif
}

bool CStaticMap::UserMapDrawObjects() const
{
#if _VBS3_UDEFCHART
  return GWorld->GetDrawObjectsOnUserDefinedChart();
#else
  return false;
#endif
}


void CStaticMap::Precalculate()
{
  // precalculates for increasing drawing speed
  _wScreen = GLOB_ENGINE->Width2D();
  _hScreen = GLOB_ENGINE->Height2D();

  float coef = _hScreen / _wScreen;
  _scaleY = coef * _scaleX;
  _invScaleX = 1.0 / _scaleX;
  _invScaleY = 1.0 / _scaleY;

  _clipRect.x = _x * _wScreen;
  _clipRect.y = _y * _hScreen;
  _clipRect.w = _w * _wScreen;
  _clipRect.h = _h * _hScreen;
}

void CStaticMap::SaturateX(float &x)
{
  /// old auto-center mode - disable 
  //if (_invScaleX < _w)
  //  x = 0.5 * (_w - _invScaleX);
  //else
  saturate(x, 0.5 * _w - _invScaleX, 0.5 * _w);
}

void CStaticMap::SaturateY(float &y)
{
  /// old auto-center mode - disable 
  //if (_invScaleY < _h)
  //  y = 0.5 * (_h - _invScaleY);
  //else
  saturate(y, 0.5 * _h - _invScaleY, 0.5 * _h);
}

void CStaticMap::ScrollX(float dif)
{
  _mapX += dif;
  SaturateX(_mapX);
}

void CStaticMap::ScrollY(float dif)
{
  _mapY += dif;
  SaturateY(_mapY);
}

void CStaticMap::SetVisibleRect(float x, float y, float w, float h)
{
  _center.x = x - _x + 0.5 * w; // offset + 0.5 * width
  _center.y = y - _y + 0.5 * h; // offset + 0.5 * height
  _halfSize.x = 0.5 * w;
  _halfSize.y = 0.5 * h;
}

void CStaticMap::Center(Vector3Val pt, bool soft)
{
  float invSizeLand = 1.0 / (LandGrid * LandRange);
  float xMap = pt.X() * invSizeLand * _invScaleX;
  float yMap = (1.0 - pt.Z() * invSizeLand) * _invScaleY;
  if (soft)
  {
    float mapX = _center.x - xMap;
    float mapY = _center.y - yMap;

    saturate(_mapX, mapX - 0.5 * _halfSize.x, mapX + 0.5 * _halfSize.x);
    saturate(_mapY, mapY - 0.5 * _halfSize.y, mapY + 0.5 * _halfSize.y);
  }
  else
  {
    _mapX = _center.x - xMap;
    _mapY = _center.y - yMap;
  }
  SaturateX(_mapX);
  SaturateY(_mapY);
}

void CStaticMap::SetScale(float scale)
{
  if (scale < 0) scale = _scaleDefault;
  float scaleMin = _scaleMin;
  saturate(scale, scaleMin, _scaleMax);

#ifdef _XBOX
  float mouseX = 0.5;
  float mouseY = 0.5;
#else
  float mouseX = 0.5 + GInput.cursorX * 0.5;
  float mouseY = 0.5 + GInput.cursorY * 0.5;
#endif
  saturate(mouseX, _x, _x + _w);
  saturate(mouseY, _y, _y + _h);

  DrawCoord ptMap(mouseX, mouseY);
  DrawCoord offset(mouseX - _x, mouseY - _y);
  Vector3 pt = ScreenToWorld(ptMap);

  _scaleX = scale;
  Precalculate();

  float invSizeLand = 1.0 / (LandGrid * LandRange);
  float xMap = pt.X() * invSizeLand * _invScaleX;
  float yMap = (1.0 - pt.Z() * invSizeLand) * _invScaleY;
  _mapX = offset.x - xMap;
  _mapY = offset.y - yMap;
  SaturateX(_mapX);
  SaturateY(_mapY);
}

int GetCursorIndex(RString cursorType)
{//returns cursotType of default textures
  if(strcmp("Arrow",cursorType)==0)
    return CursorArrow;
  if(strcmp("Track",cursorType)==0)
    return CursorTrack;
  if(strcmp("Move",cursorType)==0)
    return CursorMove;
  if(strcmp("Scroll",cursorType)==0)
    return CursorScroll;
  if(strcmp("Watch",cursorType)==0)
    return CursorWatch;
  if(strcmp("Rotate",cursorType)==0)
    return CursorRotate;
#if _VBS3
  if(strcmp"Drag",cursorType)==0)
    return CursorDrag;
  if(strcmp("Raise",cursorType)==0)
    return CursorRaise;
#else
  if(strcmp("Rotate3D",cursorType)==0)
    return CursorRotate3D;
  if(strcmp("Raise3D",cursorType)==0)
    return CursorRaise3D;
  if(strcmp("Move3D",cursorType)==0)
    return CursorMove3D;
  if(strcmp("Track3D",cursorType)==0)
    return CursorTrack3D;
#endif

  return -1;
}

void CStaticMap::SetOverrideCursor(RString cursorName, RString overrideTexture)
{
  //first try to open new texture...
  ParamEntryVal entry = Pars>> "CfgWrapperUI" >> "Cursors";
  //... if new texture does not exist, use default texture name
  if(!entry.FindEntry(overrideTexture))
    overrideTexture = cursorName;

  int index = GetCursorIndex(cursorName);
  if(index>=0)
  {//save new texture to array
    _cursorMapOverride[index] = overrideTexture;
  }

  //if changed cursor is the same we are using at the moment, change used texture 
  if(_parent && strcmp(_cursorName,cursorName)==0)
    _parent->SetCursor(overrideTexture);
}

RString CStaticMap::OverrideCursor(RString cursorName)
{ //save current cursor type
  _cursorName = cursorName;
  RString name;
  int index = GetCursorIndex(cursorName);

  if(index>=0) name = _cursorMapOverride[index];
  //no override texture is prepared, so we use default
  if(name.GetLength()<=0) name = cursorName;

  return name;
}

RString CStaticMap::OverrideCursor(CursorType cursorType)
{
  RString name;
  //if(cursorType>=0)
  //  name = _cursorMapOverride[cursorType];

  //if(name.GetLength()<=0)
  //  name = cursorName;
  return name;
}

///////////////////////////////////////////////////////////////////////////////
// Main (ingame) map

CStatic *CreateStaticMapMain(ControlsContainer *parent, int idc, const ParamEntry &cls)
{
  CStaticMap *map = new CStaticMapMain(parent, idc, cls);
  map->SetVisibleRect(map->X(), map->Y(), map->W(), map->H());
  map->Center();
  return map;
}

#define EXPOSURE_COEF     50.0F

#if _VBS2
CStaticMapMain::CStaticMapMain
(
 ControlsContainer *parent, int idc, ParamEntryPar cls,
 float scaleMin, float scaleMax, float scaleDefault
 ) : CStaticMap(parent, idc, cls, scaleMin, scaleMax, scaleDefault), _autoCenter(false)
{
}
#endif

CStaticMapMain::CStaticMapMain(ControlsContainer *parent, int idc, ParamEntryPar cls)
#if _ENABLE_CHEATS
: CStaticMap(parent, idc, cls, 0.001, 1.0, 0.16)
#else
: CStaticMap(parent, idc, cls, 0.02, 1.0, 0.16)
#endif
, _autoCenter(false)
{
#if _ENABLE_CHEATS
  _showUnits = false;
  _showTargets = false;
  _showSensors = false;
  _showVariables = false;
#endif
  _activeMarker = -1;

  _infoCommand.Load(cls >> "Command");
  _infoTask.Load(cls >> "Task");
  _infoCustomMark.Load(cls >> "CustomMark");
  _colorActiveMarker = GetPackedColor(cls >> "ActiveMarker" >> "color");
  _sizeActiveMarker = cls >> "ActiveMarker" >> "size";

  //HC command cursors
  ParamEntryVal cfg = Pars >> "cfgGroupIcons";
  _hcMapInfo._hcGroupSelected = GLOB_ENGINE->TextBank()->Load (GetPictureName(cfg >> "hc_selected" >> "icon"));
  _hcMapInfo._hcGroupSelectable = GLOB_ENGINE->TextBank()->Load(GetPictureName(cfg >> "hc_selectable" >> "icon"));
  _hcMapInfo._hcWaypoint = GLOB_ENGINE->TextBank()->Load(GetPictureName(cfg >> "waypoint" >> "icon"));
  _hcMapInfo._hcGroupSelectedSize = cfg >> "hc_selected" >> "size";
  _hcMapInfo._hcGroupSelectableSize = cfg >> "hc_selectable" >> "size";
  _hcMapInfo._hcGroupWaypointSize= cfg >> "waypoint" >> "size";
  _hcMapInfo._hcGroupWaypointShadow = cfg >> "waypoint" >> "shadow";
  _hcMapInfo._hcGroupSelectedShadow = cfg >> "hc_selected" >> "shadow";

  _iconPosition = GEngine->TextBank()->Load(GetPictureName(Pars >> "CfgInGameUI" >> "Cursor" >> "mission"));

#if _VBS2 // conditional markers
  _lastEvalMarkerConditions = UITime(0);
#endif

  _lastGroupOver = NULL;
  _hcTarget = NULL;
  _mapInfo.Clear();
  ShowScale(false);
  _showScaleAlpha = 0.66f;

  _mouseClickHandled = false;
}

void SetCommandState(int id, CommandState state, AISubgroup *subgrp)
{
  // find a map
  DisplayMap *display = dynamic_cast<DisplayMainMap *>(GWorld->Map());
  if (!display) return;
  CStaticMapMain *map = display->GetMap();
  if (!map) return;

  map->SetCommandState(id, state, subgrp);
}

void CStaticMapMain::SetCommandState(int id, int state, AISubgroup *subgrp)
{
  AIGroup *grp = subgrp->GetGroup();
  for (int i=0; i<_mapInfo.commands.Size();)
  {
    CommandInfo &cmd = _mapInfo.commands[i];
    if (cmd.grp == grp && cmd.id == id)
    {
      cmd.state = (CommandState)state;
      cmd.subgrp = subgrp;
      i++;
    }
    else if (cmd.subgrp == subgrp)
    {
      _mapInfo.commands.Delete(i);
    }
    else
    {
      i++;
    }
  }
}

void AddUnitInfo(AIUnit *unit)
{
  if (!unit) return;
  DisplayMap *display = dynamic_cast<DisplayMainMap *>(GWorld->Map());
  if (!display) return;
  CStaticMapMain *map = display->GetMap();
  if (!map) return;
  map->AddUnitInfo(unit);
}

void AddEnemyInfo
(
 TargetType *id, const VehicleType *type, int x, int z
 )
{
  if (!id) return;
  DisplayMap *display = dynamic_cast<DisplayMainMap *>(GWorld->Map());
  if (!display) return;
  CStaticMapMain *map = display->GetMap();
  if (!map) return;
  map->AddEnemyInfo(id, type, x, z);
}

void CStaticMapMain::AddUnitInfo(AIUnit *unit)
{
  Assert(unit);
  int index = -1;
  for (int i=0; i<_mapInfo.unitInfo.Size(); i++)
    if (_mapInfo.unitInfo[i].unit == unit)
    {
      index = i; break;
    }
    if (index < 0) index = _mapInfo.unitInfo.Add();
    MapUnitInfo &info = _mapInfo.unitInfo[index];
    info.unit = unit;
    info.time = Glob.clock.GetTimeOfDay();
    if (info.time >= 1.0) info.time--;
    info.time *= 24;
    float coef = 100.0 / (LandGrid * LandRange);
    Vector3Val pos = unit->Position(unit->GetRenderVisualState());
    info.x = toIntFloor(coef * pos.X());
    info.z = toIntFloor(coef * pos.Z());
}

void CStaticMapMain::AddEnemyInfo
(
 TargetType *id, const VehicleType *type, int x, int z
 )
{
  Assert(id);
  int index = -1;
  for (int i=0; i<_mapInfo.enemyInfo.Size(); i++)
    if (_mapInfo.enemyInfo[i].id == id)
    {
      index = i; break;
    }
    if (index < 0) index = _mapInfo.enemyInfo.Add();
    MapEnemyInfo &info = _mapInfo.enemyInfo[index];
    info.id = id;
    info.type = type;
    info.time = Glob.clock.GetTimeOfDay();
    if (info.time >= 1.0) info.time--;
    info.time *= 24;
    info.x = x;
    info.z = z;
}

static const EnumName CommandStateNames[]=
{
  EnumName(CSSent, "SENT"),
  EnumName(CSReceived, "RECEIVED"),
  EnumName(CSSucceed, "SUCCEED"),
  EnumName(CSFailed, "FAILED"),
  EnumName()
};
template<>
const EnumName *GetEnumNames(CommandState dummy)
{
  return CommandStateNames;
}

LSError CommandInfo::Serialize(ParamArchive &ar)
{
  CHECK(ar.SerializeRef("Group", grp, 1))
    CHECK(ar.Serialize("id", id, 1))
    CHECK(ar.SerializeEnum("state", state, 1))
    CHECK(ar.SerializeRef("Subgroup", subgrp, 1))
    CHECK(::Serialize(ar, "position", position, 1))
    return LSOK;
}

LSError MapUnitInfo::Serialize(ParamArchive &ar)
{
  CHECK(ar.SerializeRef("Unit", unit, 1))
    CHECK(ar.Serialize("time", time, 1))
    CHECK(ar.Serialize("x", x, 1))
    CHECK(ar.Serialize("z", z, 1))
    return LSOK;
}

LSError MapEnemyInfo::Serialize(ParamArchive &ar)
{
  CHECK(ar.SerializeRef("Target", id, 1))
    CHECK(::Serialize(ar, "type", type, 1))
    CHECK(ar.Serialize("time", time, 1))
    CHECK(ar.Serialize("x", x, 1))
    CHECK(ar.Serialize("z", z, 1))
    return LSOK;
}

LSError MainMapInfo::Serialize(ParamArchive &ar)
{
  CHECK(ar.Serialize("Commands", commands, 1))
    CHECK(ar.Serialize("UnitInfo", unitInfo, 1))
    CHECK(ar.Serialize("EnemyInfo", enemyInfo, 1))
    return LSOK;
}

void MainMapInfo::Clear()
{
  unitInfo.Clear();
  enemyInfo.Clear();
  commands.Clear();
}

EntityAI *CStaticMapMain::GetTarget() const
{ 
  switch (_infoMove._type)
  {
  case signVehicle:
  case signVehicleGroup:
  case signStatic:
    return _infoMove._id;
  case signUnit:
    if (_infoMove._unit) return _infoMove._unit->GetVehicle();
    return NULL;
  }
  return NULL;
}

AIGroup *CStaticMapMain::GetHCTarget() const
{ 
  return _hcTarget;
}

bool CStaticMapMain::GetPosition(Vector3 &pos) const
{
#ifdef _XBOX
  float mouseX = 0.5;
  float mouseY = 0.5;
#else
  float mouseX = 0.5 + GInput.cursorX * 0.5;
  float mouseY = 0.5 + GInput.cursorY * 0.5;
#endif
  saturate(mouseX, _x, _x + _w);
  saturate(mouseY, _y, _y + _h);

  DrawCoord ptMap(mouseX, mouseY);
  pos = ScreenToWorld(ptMap);
  pos[1] = GLandscape->SurfaceYAboveWater(pos[0], pos[2]);

  return true;
}

LSError CStaticMapMain::Serialize(ParamArchive &ar, RString name, int minVersion)
{
  CHECK(ar.Serialize(name, _mapInfo, minVersion))
    return LSOK;
}

/*!
\patch 1.01 Date 06/07/2001 by Ondra
- Fixed: clipping command info in map (see FIX clipping)
- was return before
- first clipped command caused all others to be clipped)
- repaired with continue
*/

void CStaticMapMain::DrawCommands()
{
  for (int i=0; i<_mapInfo.commands.Size();)
  {
    CommandInfo &cmd = _mapInfo.commands[i];
    if
      (
      cmd.state == CSFailed ||                    // command failed
      (cmd.state != CSSent && cmd.subgrp == NULL) // subgroup destroyed
      )
    {
      _mapInfo.commands.Delete(i);
    }
    else
    {
      DrawCoord pt = WorldToScreen(cmd.position);
      DrawSign
        (
        _infoCommand.icon, _infoCommand.color, pt,
        _infoCommand.size, _infoCommand.size, 0
        );
      if (cmd.subgrp)
      {
        //pt.x += 0.015;
        pt.x += 0.5 * _size;

        char buffer[256];
        OLinkPermNOArray(AIUnit) list;
        cmd.subgrp->GetUnitsList(list);
        CreateUnitsList(list, buffer, sizeof(buffer));
        float h = _size;
        float w = GEngine->GetTextWidth(_size, _font, buffer);

        // FIX clipping
        if
          (
          pt.x + w < _x ||
          pt.y + 0.5 * h < _y ||
          pt.x > _x + _w ||
          pt.y - 0.5 * h > _y + _h
          )
        {
          i++;
          continue;
        }
        GEngine->DrawText
          (
          Point2DFloat(pt.x, pt.y - 0.5 * h),
          _size,
          Rect2DFloat(_x, _y, _w, _h),
          _font,
          _infoCommand.color,
          buffer, 0
          );
      }
      i++;
    }
  }
}

void CStaticMapMain::DrawMarkers()
{
#if _VBS2 // conditional markers
  bool evalCondition = false;
  if (Glob.uiTime - _lastEvalMarkerConditions > 0.5)
  {
    evalCondition = true;
    _lastEvalMarkerConditions = Glob.uiTime;
  }
#endif
  int n = markersMap.Size();
  for (int i=0; i<n; i++)
  {
    ArcadeMarkerInfo &mInfo = markersMap[i];
#if _VBS3 // markers moved to CStaticMap
    if (evalCondition)
    {      
      if (mInfo.condition.GetLength() > 0)
        mInfo.isHidden = !_drawAttachedMarkers || !GWorld->GetGameState()->EvaluateBool(mInfo.condition, GameState::EvalContext::_default, GWorld->GetMissionNamespace());
      if (mInfo.attachCondition.GetLength() > 0)
      {
        if (!_drawConditionalMarkers) continue;
        GameState *state = GWorld->GetGameState();

        GameVarSpace vars(false);
        state->BeginContext(&vars);

        GameValue value = state->CreateGameValue(GameArray);
        GameArrayType &array = value;
        array.Resize(mInfo.attachCtx.Size());
        for (int i=0; i<mInfo.attachCtx.Size(); i++)
          array[i] = GameValueExt(mInfo.attachCtx[i]);
        state->VarSetLocal("_this", value, true);

        GameValue result = state->EvaluateMultiple(mInfo.attachCondition, GameState::EvalContext::_default, GWorld->GetMissionNamespace());
        if (!result.GetNil() && result.GetType() == GameObject)
          mInfo.attachTo = dynamic_cast<GameDataObject *>(result.GetData())->GetObject();

        state->EndContext();
      }
    }
    if (mInfo.isHidden) continue;
    if (mInfo.attachTo)
      mInfo.position =  mInfo.attachTo->WorldPosition(mInfo.attachTo->FutureVisualState()) + mInfo.attachOffset;
#endif
    //set color and update alpha according to marker alpha property    
    PackedColor fadedMarkerColor = ModAlpha(mInfo.color,mInfo.alpha);

    switch (mInfo.markerType)
    {
    case MTIcon:
      {
        if (mInfo.size == 0) break;
#if _VBS2 // conditional markers
        if (mInfo.layeredIcons.Size() > 0)
        {
          for (int i=0; i<mInfo.layeredIcons.Size(); i++)
          {
            LayeredMarkerInfo *layeredIcon = mInfo.layeredIcons[i];
            if (!layeredIcon->icon) continue;
            RString text = i == 0 ? Localize(mInfo.text) : ""; 
            Vector3 pos = mInfo.position;
            pos[0] += layeredIcon->x;
            pos[2] += layeredIcon->y;
            DrawSignAutosize
              (
              layeredIcon->icon, mInfo.color,
              mInfo.position,
              mInfo.size * layeredIcon->a, mInfo.size * layeredIcon->b,
              mInfo.angle * (H_PI / 180.0), text, true, NULL, -1, mInfo.autosize
              );
          }
          break;
        }
        if (!mInfo.icon) break;
        DrawSignAutosize
          (
          mInfo.icon, mInfo.color,
          mInfo.position,
          mInfo.size * mInfo.a, mInfo.size * mInfo.b,
          mInfo.angle * (H_PI / 180.0), Localize(mInfo.text), true, NULL, -1, mInfo.autosize
          );
#else     
        if (!mInfo.icon) break;
        DrawSign
          (
          mInfo.icon, fadedMarkerColor,
          mInfo.position,
          mInfo.size * mInfo.a, mInfo.size * mInfo.b,
          mInfo.angle * (H_PI / 180.0), Localize(mInfo.text),NULL,-1, mInfo.shadow
          );
#endif
      }
      break;
    case MTRectangle:
      FillRectangle
        (
        mInfo.position, mInfo.a, mInfo.b, mInfo.angle * (H_PI / 180.0),
        fadedMarkerColor, mInfo.fill
        );
      if(mInfo.drawBorder)
        DrawRectangle(mInfo.position, mInfo.a, mInfo.b, mInfo.angle * (H_PI / 180.0),
        fadedMarkerColor);
      break;
    case MTEllipse:
      FillEllipse
        (
        mInfo.position, mInfo.a, mInfo.b, mInfo.angle * (H_PI / 180.0),
        fadedMarkerColor, mInfo.fill
        );
      if(mInfo.drawBorder)
        DrawEllipse(mInfo.position, mInfo.a, mInfo.b, mInfo.angle * (H_PI / 180.0),
        fadedMarkerColor);
      break;
    }
  }
}

void CStaticMapMain::DrawGroupIcons()
{
  if (GWorld->UI()->IsGroupIconVisible2D())
  {
    for (int i=0; i<TSideUnknown; i++)
    {
      AICenter *center = GWorld->GetCenter((TargetSide)i);
      if (center)
      {
        for (int j=0; j<center->NGroups(); j++)
        {
          AIGroup *group = center->GetGroup(j);
          if (group && group->GetGroupIcon().IsVisible())
          {
            AIUnit *unit = group->Leader();
            if(unit)
            {
              group->GetCurrentPosition();
              AutoArray<GIcon> markers = group->GetGroupIcons();
              DrawCoord posMap = WorldToScreen(unit->Position(unit->GetRenderVisualState()));

              if(markers.Size()>0)
              {
                int i=0;
                DrawCoord pos = DrawCoord((posMap.x + markers[i]._offsetX*markers[i]._size*(1.0 / 640)), (posMap.y +  markers[i]._offsetY*markers[i]._size*(1.0 / 480)));
                DrawSign
                  (
                  markers[i]._groupMaker, group->GetGroupIcon().IconColor(),
                  pos,
                  markers[i]._size,markers[i]._size,
                  0," " + group->GetGroupIcon()._text, NULL, _sizeNames * 0.6f, true,markers[i]._shadow
                  );
              }

              for(int i=1; i< markers.Size(); i++)
              {
                DrawCoord pos = DrawCoord((posMap.x + markers[i]._offsetX*markers[i]._size*(1.0 / 640)), (posMap.y +  markers[i]._offsetY*markers[i]._size*(1.0 / 480)));
                DrawSign
                  (
                  markers[i]._groupMaker, group->GetGroupIcon().IconColor(),
                  pos,
                  markers[i]._size,markers[i]._size,
                  0, "", false, -1,true , markers[i]._shadow
                  );
              }              
            }
            if(unit && group->GetGroupIcon().ShowWaypoints())
            {
              int wSize = group->NWaypoints();
              int current = group->GetCurrentWaypoint();
              if(current<0 || wSize<=0) continue;
              DrawCoord posMapStart = WorldToScreen(unit->Position(unit->GetRenderVisualState()));

              for(int i=current; i< wSize; i++) 
              {
                DrawCoord posMap = WorldToScreen( group->GetWaypointPosition(i));

                DrawSign
                  (
                  _hcMapInfo._hcWaypoint, group->GetGroupIcon().WaypointColor(),
                  posMap,
                  _hcMapInfo._hcGroupWaypointSize,_hcMapInfo._hcGroupWaypointSize,
                  0, FormatNumber(i-current+1), false, -1, false, _hcMapInfo._hcGroupWaypointShadow
                  );


                GEngine->DrawLine(
                  Line2DPixel(posMapStart.x * _wScreen, posMapStart.y * _hScreen, posMap.x * _wScreen, posMap.y * _hScreen),
                  group->GetGroupIcon().WaypointColor(), group->GetGroupIcon().WaypointColor(), _clipRect
                  );
                posMapStart = posMap;
              }
            }
          }
        }
      }
    }
    if(GWorld->UI()->IsHcCommandVisible())
    {
      //draw group selection icon
      // draw unit related information
      AIBrain *agent = GWorld->FocusOn();
      if (!agent) return;
      AIUnit *player = agent->GetUnit();
      AutoArray<AIHCGroup> &groups = player->GetHCGroups();

      for(int i=0; i<groups.Size();i++)
      {
        if(!groups[i].GetGroup()) continue;
        if(!groups[i].GetGroup()->GetGroupIcon().IsVisible()) continue;
        int size;
        Ref<Texture> picture;
        if(groups[i].IsSelected())
        {
          picture = _hcMapInfo._hcGroupSelected;
          size = _hcMapInfo._hcGroupSelectedSize;
        }
        else
        {
          picture = _hcMapInfo._hcGroupSelectable;
          size = _hcMapInfo._hcGroupSelectableSize;
        }

        groups[i].GetGroup()->GetCurrentPosition();
        AIUnit *unit = groups[i].GetGroup()->Leader();
        if(unit)
        {
          DrawCoord posMap = WorldToScreen(unit->Position(unit->GetRenderVisualState()));
          PackedColor color = PackedColorRGB(PackedWhite, toIntFloor(groups[i].GetGroup()->GetGroupIcon()._markerColor.A8()));

          DrawSign
            (
            picture, color,
            posMap,
            size,size,
            0, "", false, -1, false, _hcMapInfo._hcGroupSelectedShadow 
            );
        }
      }
    }
  }
}

void CStaticMapMain::DrawSensors()
{
  int n = sensorsMap.Size();
  for (int i=0; i<n; i++)
  {
    Vehicle *veh = sensorsMap[i];
    Detector *det = dyn_cast<Detector>(veh);
    if (!det) continue;
    PackedColor color = _colorSensor;
    if (!det->IsActive())
      color.SetA8(color.A8() / 2);

    float angle = (180 / H_PI) * atan2(det->_sinAngle, det->_cosAngle);
    if (det->_rectangular)
      DrawRectangle(det->RenderVisualState().Position(), det->_a, det->_b, angle * (H_PI / 180.0), color);
    else
      DrawEllipse(det->RenderVisualState().Position(), det->_a, det->_b, angle * (H_PI / 180.0), color);
    DrawSign
      (
      _iconSensor, color,
      det->RenderVisualState().Position(),
      16, 16, 0
      );
  }
}

DrawCoord CStaticMapMain::GetWaypointPosition(const WaypointInfo &wInfo)
{
#if 0
  int type = wInfo.type;
  Point3 pos = wInfo.position;
  if
    (
    type == ACDESTROY ||
    type == ACGETIN ||
    type == ACJOIN ||
    type == ACLEADER
    )
  {
    if (wInfo.id >= 0 && wInfo.id < vehiclesMap.Size())
    {
      Vehicle *veh = vehiclesMap[wInfo.id];
      if (veh) pos = veh->Position();
    }
  }
#else
  // do not update waypoints position
  // waypoints are now only signs in map
  Point3 pos = wInfo.position;
#endif

  return WorldToScreen(pos);
}

void CStaticMapMain::DrawWaypoints(AICenter *center, AIGroup *myGroup)
{
#if _ENABLE_CHEATS
  if (_showUnits)
  {
    EntityAI *veh = dyn_cast<EntityAI>(GWorld->CameraOn());
    if (!veh) return;
    myGroup = veh->GetGroup();
    if (!myGroup) return;
    center = myGroup->GetCenter();
  }
#endif

  int index = INT_MAX;
  if (myGroup->GetCurrent())
    index = myGroup->GetCurrent()->_fsm->Var(0);

  int m = myGroup->NWaypoints();

#if _ENABLE_CHEATS
  if (!_showUnits)
#endif
  {
    int nshow = 0;
    for (int j=1; j<m; j++)
    {
      const WaypointInfo &wInfo = myGroup->GetWaypoint(j);
      bool wpShow = wInfo.showWP == ShowAlways ||
        wInfo.showWP == ShowEasy && Glob.config.ShowCadetWP();
      if (wpShow) nshow++;
    }
    if (nshow == 0) return;
  }

  // lines
  if (m > 1)
  {
    const WaypointInfo &wInfo = myGroup->GetWaypoint(0);
    DrawCoord pt1 = GetWaypointPosition(wInfo);
    for (int j=1; j<m; j++)
    {
      const WaypointInfo &wInfo = myGroup->GetWaypoint(j);
      bool wpShow = wInfo.showWP == ShowAlways ||
        wInfo.showWP == ShowEasy && Glob.config.ShowCadetWP();
      if
        (
#if _ENABLE_CHEATS
        !_showUnits && 
#endif
        !wpShow
        ) continue;
      // draw line
      DrawCoord pt2 = GetWaypointPosition(wInfo);
      GEngine->DrawLine(
        Line2DPixel(pt1.x * _wScreen, pt1.y * _hScreen, pt2.x * _wScreen, pt2.y * _hScreen),
        _colorActiveMission, _colorActiveMission, _clipRect
        );
      pt1 = pt2;

      // if waypoint is a cycle, draw a cycle line
      if (wInfo.type==ACCYCLE)
      {
        int cycle = myGroup->FindNearestPreviousWaypoint(j-1,wInfo.position);
        if (cycle>=0)
        {
          const WaypointInfo &cInfo = myGroup->GetWaypoint(cycle);
          DrawCoord ptC = GetWaypointPosition(cInfo);
          // draw a connecting line
          GEngine->DrawLine(
            Line2DPixel(pt1.x * _wScreen, pt1.y * _hScreen, ptC.x * _wScreen, ptC.y * _hScreen),
            _colorActiveMission, _colorActiveMission, _clipRect
            );
        }
      }

    }
  }

  // waypoints
  for (int j=0; j<m; j++)
  {
    const WaypointInfo &wInfo = myGroup->GetWaypoint(j);
    bool wpShow = wInfo.showWP == ShowAlways ||
      wInfo.showWP == ShowEasy && Glob.config.ShowCadetWP();
    if
      (
      j > 0 &&
#if _ENABLE_CHEATS
      !_showUnits &&
#endif
      !wpShow
      ) continue;
    MapTypeInfo &info = j < index ? _infoWaypointCompleted : _infoWaypoint;
    DrawCoord pt = GetWaypointPosition(wInfo);
    // when size is explicit, we want to display it
    // 

    if (wInfo.completitionRadius>0)
    {
      DrawEllipse(wInfo.position, wInfo.completitionRadius, wInfo.completitionRadius, 0, info.color);
    }
    DrawSign(info.icon, info.color, pt, info.size, info.size, 0);
    if (wInfo.HasEffect())
    {
      pt.x += 0.015;
      DrawSign(_iconCamera, _colorCamera, pt, 12, 12, 0);
    }
  }
}

/// helper will extract vehicle from the SignInfo
static EntityAI *GetVehicle(const SignInfo &info)
{
  switch (info._type)
  {
  case signUnit:
    if (info._unit) return info._unit->GetVehicle();
    return NULL;
  case signVehicle:
  case signVehicleGroup:
  case signStatic:
    return info._id;
  default:
    return NULL;
  }
}

#if _ENABLE_CHEATS
Ref<const TargetNormal> diagTarget;
static inline RString GetTargetInfo(const TargetNormal *target)
{
  Vector3 pos = target->LandAimingPosition();
  diagTarget = target;
  return 
    Format(" lastSeen %.1f", Glob.time.Diff(target->lastSeen)) +
    Format(" posAge %.1f",Glob.time.Diff(target->posAccuracyTime)) +
    Format(" prec %.1f", target->posAccuracy) +
    Format(" distRP %.1f", pos.Distance(target->idExact->AimingPosition(target->idExact->FutureVisualState()))) +
    Format(" distPos %.1f", pos.Distance(target->position)) +
    Format(" cost %.0f", target->subjectiveCost);
}

static inline RString GetTargetModeInfo(AIBrain *unit)
{
  RString result;

  AIGroup *grp = unit->GetGroup();
  if (grp) result = result + Format(" CM Minor: %s", cc_cast(FindEnumName(grp->GetCombatModeMinor())));

  result = result + Format(" CM Major: %s", cc_cast(FindEnumName(unit->GetCombatModeMajor())));

  result = result + Format(" CM: %s", cc_cast(FindEnumName(unit->GetCombatMode())));

  return result;
}
#endif

//to skip animals and civilians
bool CStaticMapMain::CheckItemWantedDraw(const TargetNormal *target)
{
  if(target->type->IsAnimal() || target->side == TCivilian) return false;
  return true;
};

void CStaticMapMain::DrawLabel(struct SignInfo &info, PackedColor color, int shadow)
{
  // show info from the eyes of observer in Release version, use the player always in Retail
#if _ENABLE_CHEATS
  EntityAI *observer = GetVehicle(_infoClick);
  AIBrain *me = observer ? observer->CommanderUnit() : NULL;
  if (!me) me = GetMyUnit();
#else
  AIBrain *me = GetMyUnit();
#endif
  AIGroup *myGroup = me ? me->GetGroup() : NULL;
  AICenter *myCenter = myGroup ? myGroup->GetCenter() : NULL;

  RString text;
  Vector3 pos;

  EntityAI *vehicle = GetVehicle(info);
  if (vehicle)
  {
#if _ENABLE_CHEATS
    if (_showUnits || CHECK_DIAG(DEFSM))
    {
      AIBrain *unit = vehicle->CommanderUnit();
      if (unit)
        text = unit->GetDebugName();
      else
        text = vehicle->GetType()->GetDisplayName();

      pos = vehicle->RenderVisualState().Position();
      if (CHECK_DIAG(DECombat))
      {
        // display some additional informations about the target
        const TargetList *list = me ? me->AccessTargetList() : NULL;
        if (list)
        {
          const TargetNormal *target = list->FindTarget(vehicle);
          if (target) text = text + GetTargetInfo(target);
        }
      }
      if (CHECK_DIAG(DEFSM))
      {
        AIBrain *brain = vehicle->PilotUnit();
        AIUnit *unit = brain ? brain->GetUnit() : NULL;
        if (unit)
        {
          text = text + Format(" Behind: %g", unit->GetBehindFormation());
          /*
          int n = unit->NBehaviourDiags();
          for (int i=0; i<n; i++)
          {
          RString str = unit->BehaviourDiags(i);
          if (str.GetLength() > 0) text = text + RString(", ") + str;
          }
          */
        }
      }
    }
    else if (_showTargets)
    {
      // find in the center's list of targets
      if (myCenter)
      {
        const AITargetInfo *info = myCenter->FindTargetInfo(vehicle);
        if (info)
        {
          text = info->_type->GetDisplayName();
          pos = info->_realPos;
        }
      }
    }
    else 
#endif
      if (Glob.config.IsEnabled(DTMap) && !_miniMapMode)
      {
        const TargetList *list = me ? me->AccessTargetList() : NULL;
        if (list)
        {
          const TargetNormal *target = list->FindTarget(vehicle);
          if (target)
          {
            //skip animals and civilians
            if(CheckItemWantedDraw(target)) 
            {
              text = target->GetType()->GetDisplayName();
              pos = target->LandAimingPosition();
#if _ENABLE_CHEATS
              // display some additional informations about the target
              if (CHECK_DIAG(DECombat)) text = text + GetTargetInfo(target);
#endif
            }
          }
        }
      }
#if _ENABLE_CHEATS
      if (!CHECK_DIAG(DECombat) && CHECK_DIAG(DECombatMode))
      {
        AIBrain *unit = vehicle->CommanderUnit();
        if (unit) text = text + GetTargetModeInfo(unit);
      }
#endif
  }
  else switch (info._type)
  {
case signNone:
  break;
#if _ENABLE_IDENTITIES
case signTask:
  if (info._unit)
  {
    Task *task = info._unit->GetPerson()->GetIdentity().FindTask(info._index);
    if (task && task->GetDestination(pos))
    {
      task->GetDescriptionHUD(text);
    }
  }
  break;
#endif
case signArcadeWaypoint:
  if (myCenter)
  {
    if (info._indexGroup < 0 || info._indexGroup >= myCenter->NGroups()) break;
    AIGroup *group = myCenter->GetGroup(info._indexGroup);
    if (group)
    {
      // FIX: can occur when groups joined
      if (info._index >= group->NWaypoints()) break;
      const WaypointInfo &wInfo = group->GetWaypoint(info._index);
      text = LocalizeString(IDS_AC_MOVE + wInfo.type - ACMOVE);
      pos = wInfo.position;
    }
  }
  break;
case signArcadeSensor:
  {
    Vehicle *veh = sensorsMap[info._index];
    Detector *det = dyn_cast<Detector>(veh);
    if (!det) break;
    pos = det->RenderVisualState().Position();
    if (det->GetText().GetLength() > 0)
    {
      text = det->GetText();
    }
    else
    {
      text = LocalizeString(IDS_SENSOR);
    }
    int type = det->GetAction();
    if (type != ASTNone)
    {
      text = text + RString(" (") + LocalizeString(IDS_SENSORTYPE_NONE + type) + RString(")");
    }
    int activation = det->GetActivationType();
    int activationBy = det->GetActivationBy();
    if (activationBy!=ASANone)
    {
      text = text + RString(" [") + FindEnumName(ArcadeSensorActivation(activationBy)) + " " + LocalizeString(IDS_DISP_ARCSENS_PRESYES + activation) + RString("]");
    }
    
    RString cond,act,deact;
    det->GetStatements(cond,act,deact);
    if (!cond.IsEmpty())
    {
      text = text + " " + cond;
    }
  }
  break;
  }

  if (text.GetLength() > 0) CStaticMap::DrawLabel(pos, text, color, shadow);
}

void CStaticMapMain::DrawExposureEnemy
(
 AICenter *center,
 float x, float y, float xStep, float yStep,
 int i, int j, int iStep, int jStep
 )
{
  int maxAlpha8=0;
  const float coef = 0.5 / (256 * EXPOSURE_COEF);

  float alpha = coef * center->GetExposureOptimistic(i, j);
  int alpha8TL = toIntFloor( alpha * 255 );
  alpha = coef * center->GetExposureOptimistic(i + iStep, j);
  int alpha8TR = toIntFloor( alpha * 255 );
  alpha = coef * center->GetExposureOptimistic(i, j + jStep);
  int alpha8BL = toIntFloor( alpha * 255 );
  alpha = coef * center->GetExposureOptimistic(i + iStep, j + jStep);
  int alpha8BR = toIntFloor( alpha * 255 );

  if( maxAlpha8<=alpha8TL ) {if( alpha8TL>255 ) alpha8TL=255;maxAlpha8=alpha8TL;}
  else if (alpha8TL < 0) alpha8TL = 0;
  if( maxAlpha8<=alpha8TR ) {if( alpha8TR>255 ) alpha8TR=255;maxAlpha8=alpha8TR;}
  else if (alpha8TR < 0) alpha8TR = 0;
  if( maxAlpha8<=alpha8BL ) {if( alpha8BL>255 ) alpha8BL=255;maxAlpha8=alpha8BL;}
  else if (alpha8BL < 0) alpha8BL = 0;
  if( maxAlpha8<=alpha8BR ) {if( alpha8BR>255 ) alpha8BR=255;maxAlpha8=alpha8BR;}
  else if (alpha8BR < 0) alpha8BR = 0;

  if( maxAlpha8>0 )
  {
    Draw2DParsNoTex pars;
    pars.SetColor(_colorExposureEnemy);
    // TL
    pars.colorTL.SetA8(alpha8TL);
    // TR
    pars.colorTR.SetA8(alpha8TR);
    // BL
    pars.colorBL.SetA8(alpha8BL);
    // BR
    pars.colorBR.SetA8(alpha8BR);

    Rect2DPixel rect(x * _wScreen, y * _hScreen, xStep * _wScreen, yStep * _hScreen);

    pars.spec = NoZBuf|IsAlpha|IsAlphaFog|NoClamp;
    GLOB_ENGINE->Draw2D(pars, rect, _clipRect);

    // draw exposure text

    // draw text information about the field
    float tsize = xStep * floatMax(_sizeInfo,0.1f);
    if (tsize >= 0.014f)
    {
      float exposure = center->GetExposureOptimistic(i, j-1);
      if (exposure>0)
      {
        float ty = y;
        //float tsize = 0.2;
        ty += tsize;
        ty += tsize;
        ty += tsize;
        GEngine->DrawTextF
          (
          Point2DFloat(x, ty), tsize,
          Rect2DFloat(x, y, xStep, yStep),
          _fontInfo, _colorNames, 0,
          "enemy %.0f", exposure
          );
        ty += tsize;
      }
    }
  }
}


inline PackedColor CostToColor(float cost)
{
  static const float int1 = 1.0;
  static const float invint1 = (1.0 / int1);
  static const float int2 = 4.0;
  static const float invint2 = (1.0 / int2);
  static const float int12 = int1 + int2;

  static const float alpha = 0.5;
  static const Color colorR(1, 0, 0, alpha);
  static const Color colorG(0, 1, 0, alpha);
  static const Color colorY(1, 1, 0, alpha);

  saturateMin(cost, int12);
  if (cost <= int1)
  {
    return PackedColor
      (
      colorY * (cost * invint1)  + colorG * (1 - cost * invint1)
      );
  }
  else
  {
    cost -= int1;
    return PackedColor
      (
      colorR * (cost * invint2) + colorY * (1 - cost * invint2)
      );
  }
}

void CStaticMapMain::DrawCost
(
 AISubgroup *subgroup, float invCost,
 float x, float y, float xStep, float yStep,
 int i, int j, int iStep, int jStep
 )
{
  GeographyInfo geogr = GLOB_LAND->GetGeography(i, j - 1);

  Draw2DParsNoTex pars;

  float cost;
  if (iStep == 1 && jStep == -1)
  {
    // ALL
    cost = subgroup->GetFieldCost(i, j + jStep, false) * invCost;
    pars.SetColor(CostToColor(cost));
  }
  else
  {
    // TL
    cost = subgroup->GetFieldCost(i, j, false) * invCost;
    pars.colorTL = CostToColor(cost);
    // TR
    cost = subgroup->GetFieldCost(i + iStep, j, false) * invCost;
    pars.colorTR = CostToColor(cost);
    // BL
    cost = subgroup->GetFieldCost(i, j + jStep, false) * invCost;
    pars.colorBL = CostToColor(cost);
    // BR
    cost = subgroup->GetFieldCost(i + iStep, j + jStep, false) * invCost;
    pars.colorBR = CostToColor(cost);
  }

  pars.spec = NoZBuf|IsAlpha|IsAlphaFog|NoClamp;
  Rect2DPixel rect(x * _wScreen, y * _hScreen, xStep * _wScreen, yStep * _hScreen);
  GEngine->Draw2D(pars, rect, _clipRect);

  // draw text information about the field
  float tsize = xStep * floatMax(_sizeInfo,0.1f);
  if (tsize >= 0.014f)
  {
    float ty = y;

    if (InRange(j - 1, i))
    {
      const ObjectList &ol = GLOB_LAND->GetObjects(j - 1, i);
      if (ol.GetList())
      {
        bool cached = ol.GetList()->IsInList();
        GEngine->DrawTextF
          (
          Point2DFloat(x, ty), tsize,
          Rect2DFloat(x, y, xStep, yStep),
          _fontInfo, _colorNames, 0,
          "Used %d%s", ol.GetList()->GetUsedCount()-cached, cached ? "+C" : ""
          );
        ty += tsize;
      }

      const MapObjectList &ml = GLOB_LAND->GetMapObjects(i, j - 1);
      if (ml.GetList())
      {
        bool cached = ml.GetList()->IsInList();
        GEngine->DrawTextF
          (
          Point2DFloat(x, ty), tsize,
          Rect2DFloat(x, y, xStep, yStep),
          _fontInfo, _colorNames, 0,
          "MapUsed %d%s", ml.GetList()->GetUseCount()-cached, cached ? "+C" : ""
          );
        ty += tsize;
      }
    }

    //float tsize = 0.2;
    GEngine->DrawTextF
      (
      Point2DFloat(x, ty), tsize,
      Rect2DFloat(x, y, xStep, yStep),
      _fontInfo, _colorNames, 0,
      "softObj %d", geogr.u.howManyObjects
      );
    ty += tsize;

    if (geogr.u.howManyHardObjects>0)
    {
      GEngine->DrawTextF
        (
        Point2DFloat(x, ty), tsize,
        Rect2DFloat(x, y, xStep, yStep),
        _fontInfo, _colorNames, 0,
        "hardObj %d", geogr.u.howManyHardObjects
        );
      ty += tsize;
    }

    GEngine->DrawTextF
      (
      Point2DFloat(x, ty), tsize,
      Rect2DFloat(x, y, xStep, yStep),
      _fontInfo, _colorNames, 0,
      "grad %d", geogr.u.gradient
      );
    ty += tsize;

    if (geogr.u.forest)
    {
      GEngine->DrawText
        (
        Point2DFloat(x, ty), tsize,
        Rect2DFloat(x, y, xStep, yStep),
        _fontInfo, _colorNames,
        "forest",0
        );
      ty += tsize;
    }

    if (geogr.u.road)
    {
      GEngine->DrawText
        (
        Point2DFloat(x, ty), tsize,
        Rect2DFloat(x, y, xStep, yStep),
        _fontInfo, _colorNames,
        "Road",0
        );
      ty += tsize;
    }

    if (geogr.u.minWaterDepth>0 || geogr.u.maxWaterDepth>0)
    {
      GEngine->DrawTextF(
        Point2DFloat(x, ty), tsize,
        Rect2DFloat(x, y, xStep, yStep),
        _fontInfo, _colorNames, 0,
        "Water %d..%d",geogr.u.minWaterDepth,geogr.u.maxWaterDepth
        );
      ty += tsize;
    }
    if (geogr.u.full)
    {
      GEngine->DrawText
        (
        Point2DFloat(x, ty), tsize,
        Rect2DFloat(x, y, xStep, yStep),
        _fontInfo, _colorNames,
        "Square full",0
        );
      ty += tsize;
    }
  }

  if (iStep == 1 && jStep == -1)
  {
    float xTL = x * _wScreen, yTL = y * _hScreen;
    float xBR = (x+xStep) * _wScreen, yBR = (y+yStep) * _hScreen;

    const RoadList &roadList = GLandscape->GetRoadNet()->GetRoadList(i, j - 1);
    int n = roadList.Size();
    if (n > 0)
    {
      bool found = false;
      for (int i=0; i<n; i++)
      {
        RoadLink *item = roadList[i];
        for (int j=0; j<item->NConnections(); j++)
        {
          if (!item->Connections()[j])
          {
            found = true;
            break;
          }
        }
        if (found) break;
      }
      PackedColor color = PackedColor(found ? Color(1,.2,.2,1) : Color(1,1,1,1));
      GLOB_ENGINE->DrawLine
        (
        Line2DPixel(xTL, yTL, xBR, yBR),
        color, color, _clipRect
        );
      GLOB_ENGINE->DrawLine
        (
        Line2DPixel(xTL, yBR, xBR, yTL),
        color, color, _clipRect
        );
    }

    if (InRange(j - 1, i))
    {
      const ObjectList &ol = GLOB_LAND->GetObjects(j - 1, i);
      if (ol.GetList())
      {
        bool cached = ol.GetList()->IsInList();
        bool locked = ol.GetList()->GetUsedCount()>int(cached);
        PackedColor color = PackedColor(Color(0.1,0.8,0.1,0.85));
        if (cached && locked)
        {
          color = PackedColor(Color(0.25,0.25,0.7,0.75));
        }
        else if (cached)
        {
          color = PackedColor(Color(0.25,0.25,0.25,0.75));
        }
        else if (locked)
        {
          color = PackedColor(Color(0.7,0.1,0.1,0.85));
        }
        GLOB_ENGINE->DrawLine
          (
          Line2DPixel(xTL, (yTL*1.4+yBR*0.6)*0.5, xBR, (yTL*0.6+yBR*1.4)*0.5),
          color, color, _clipRect
          );
        GLOB_ENGINE->DrawLine
          (
          Line2DPixel((xTL*1.4+xBR*0.6)*0.5, yBR, (xTL*0.6+xBR*1.4)*0.5, yTL),
          color, color, _clipRect
          );
      }

      const MapObjectList &ml = GLOB_LAND->GetMapObjects(i, j - 1);
      if (ml.GetList())
      {
        bool cached = ml.GetList()->IsInList();
        bool locked = ml.GetList()->GetUseCount()>int(cached);
        PackedColor color = PackedColor(Color(0.1,0.8,0.1,0.85));
        if (cached && locked)
        {
          color = PackedColor(Color(0.25,0.25,0.7,0.75));
        }
        else if (cached)
        {
          color = PackedColor(Color(0.25,0.25,0.25,0.75));
        }
        else if (locked)
        {
          color = PackedColor(Color(0.7,0.1,0.1,0.85));
        }
        GLOB_ENGINE->DrawLine
          (
          Line2DPixel(xTL, (yTL+yBR)*0.5, xBR, (yTL+yBR)*0.5),
          color, color, _clipRect
          );
        GLOB_ENGINE->DrawLine
          (
          Line2DPixel((xTL+xBR)*0.5, yBR, (xTL+xBR)*0.5, yTL),
          color, color, _clipRect
          );
      }
    }
  }

}

void CStaticMapMain::DrawInfo()
{
  float coef = 100.0; // width of field in global coord.
  float invCoef = 0.01; // 1.0 / coef;

  float xFld = _invScaleX * invCoef;
  float zFld = _invScaleY * invCoef;

  float tsize = xFld * _sizeInfo;
  if (tsize < 0.022) return;

  int xFrom = toIntFloor(-_mapX * _scaleX * coef);
  int zFrom = toIntFloor((_invScaleY - (_h - _mapY)) * _scaleY * coef);
  int xTo = toIntFloor((_w - _mapX) * _scaleX * coef);
  int zTo = toIntFloor((_invScaleY - (-_mapY)) * _scaleY * coef);

  float xBeg = _x + _mapX + xFrom * _invScaleX * invCoef;
  float zBeg = _y + _mapY + (_invScaleY - zFrom * _invScaleY * invCoef);

  float zAct = zBeg;
  for (int z = zFrom; z <= zTo; z++)
  {
    float xAct = xBeg;
    for (int x = xFrom; x <= xTo; x++)
    {
      DrawInfo(xAct, zAct - zFld, xFld, zFld, x, z);
      xAct += xFld;
    }
    zAct -= zFld;
  }
}

void CStaticMapMain::DrawInfo
(
 float x, float y, float xStep, float yStep, int i, int j
 )
{
  static const int maxSubgroups = 3;
  static const int maxEnemies = 3;

  float tsize = xStep * _sizeInfo;
  float ty = y + 0.5 * (yStep - (maxSubgroups + 1) * tsize);

  int indices[maxEnemies];
  for (int l=0; l<maxEnemies; l++) indices[l] = -1;
  for (int k=0; k<_mapInfo.enemyInfo.Size(); k++)
  {
    MapEnemyInfo &info = _mapInfo.enemyInfo[k];
    if (info.x != i || info.z != j) continue;
    for (int l=0; l<maxEnemies; l++)
    {
      if (indices[l] < 0)
      {
        indices[l] = k;
        break;
      }
      else
      {
        float kCost = info.type->GetCost();
        float lCost = _mapInfo.enemyInfo[indices[l]].type->GetCost();
        if (kCost >= lCost)
        {
          for (int i=maxEnemies-1; i>l; i--) indices[i] = indices[i - 1];
          indices[l] = k;
          break;
        }
      }
    }
  }
  if (indices[0] >= 0)
  {
    float left = x;
    float maxTime = 0;
    for (int l=0; l<maxEnemies; l++)
    {
      int k = indices[l];
      if (k < 0) break;
      MapEnemyInfo &info = _mapInfo.enemyInfo[k];
      saturateMax(maxTime, info.time);
    }
    int hour = toIntFloor(maxTime);
    int minute = toIntFloor(60.0 * (maxTime - hour));
    if (minute >= 60)
    {
      hour++;
      minute -= 60;
    }
    GEngine->DrawTextF
      (
      Point2DFloat(x, ty), tsize,
      Rect2DFloat(x, y, xStep, yStep),
      _fontInfo, _colorEnemy, 0,
      "% 2d:%02d", hour, minute
      );
    left += GEngine->GetTextWidthF(tsize, _fontNames, "% 2d:%02d", hour, minute);
    float height = tsize;
    float width = height * (_hScreen / _wScreen);
    for (int l=0; l<maxEnemies; l++)
    {
      int k = indices[l];
      if (k < 0) break;
      MapEnemyInfo &info = _mapInfo.enemyInfo[k];
      Texture *texture = info.type->GetIcon();
      MipInfo mip = GEngine->TextBank()->UseMipmap(texture, 0, 0);
      GEngine->Draw2D
        (
        mip, _colorEnemy,
        Rect2DPixel(left * _wScreen, ty * _hScreen, width * _wScreen, height * _hScreen),
        Rect2DPixel(x * _wScreen, y * _hScreen, xStep * _wScreen, yStep * _hScreen)
        );
      left += width;
    }
    ty += height;
  }

  // newest infos first
  int n = 0;
  for (int k=_mapInfo.unitInfo.Size()-1; k>=0; k--)
  {
    MapUnitInfo &info = _mapInfo.unitInfo[k];
    AIUnit *unit = info.unit;
    if (!unit)
    {
      _mapInfo.unitInfo.Delete(k);
      continue;
    }
    if (info.x != i || info.z != j) continue;
    int hour = toIntFloor(info.time);
    int minute = toIntFloor(60.0 * (info.time - hour));
    if (minute >= 60)
    {
      hour++;
      minute -= 60;
    }
    GEngine->DrawTextF
      (
      Point2DFloat(x, ty), tsize,
      Rect2DFloat(x, y, xStep, yStep),
      _fontInfo, _colorFriendly, 0,
      "% 2d:%02d %d", hour, minute, (const char *)unit->ID()
      );
    ty += tsize;
    n++;
    if (n >= maxSubgroups) break;
  }
}

AIBrain *CStaticMapMain::GetMyUnit()
{
  return GWorld->FocusOn();
}

AICenter *CStaticMapMain::GetMyCenter()
{
  AIBrain *me = GetMyUnit();
  AIGroup *myGroup = me ? me->GetGroup() : NULL;
  AICenter *myCenter = myGroup ? myGroup->GetCenter() : NULL;

  if (myCenter) return myCenter;

  switch (Glob.header.playerSide)
  {
  case TEast:
    return GLOB_WORLD->GetEastCenter();
  case TWest:
    return GLOB_WORLD->GetWestCenter();
  case TGuerrila:
    return GLOB_WORLD->GetGuerrilaCenter();
  case TCivilian:
    return GLOB_WORLD->GetCivilianCenter();
  default:
    Fail("Side");
    return NULL;
  }
}

void CStaticMapMain::DrawExposure()
{
#if _ENABLE_CHEATS
  if (!_show) return;
#endif

  AICenter *myCenter = GetMyCenter();
  if (!myCenter) return;

  float ptsLand = 800.0 * _invScaleX * InvLandRange;  // avoid dependece on video resolution
  float invPtsLand = 1.0 / ptsLand;


  int iStep = toIntCeil(_ptsPerSquareExp * invPtsLand);
  float xStep = iStep * InvLandRange * _invScaleX;
  int jStep = iStep;
  float yStep = jStep * InvLandRange * _invScaleY;
  int iMin = iStep * toIntFloor(-_mapX / xStep);
  float xMin = _x - fastFmod(-_mapX, xStep);
  int jMin = LandRange - jStep * toIntFloor(-_mapY / yStep);
  float yMin = _y - fastFmod(-_mapY, yStep);

  float y = yMin;
  int j = jMin;
  while (j >= 0 && y <= _y + _h)
  {
    float x = xMin;
    int i = iMin;
    while (i < LandRange && x <= _x + _w)
    {
      DrawExposureEnemy(myCenter, x, y, xStep, yStep, i, j, iStep, -jStep);
      i += iStep;
      x += xStep;
    }
    j -= jStep;
    y += yStep;
  }
}

void CStaticMapMain::DrawCost()
{
  AIBrain *me = GetMyUnit();
  if (!me) return;
  AIUnit *myUnit = me->GetUnit();
  if (!myUnit) return;
  AISubgroup *mySubgroup = myUnit->GetSubgroup();
  if (!mySubgroup) return;

  GeographyInfo geogr(0);
  float costNormal = -FLT_MAX;
  for (int i=0; i<mySubgroup->NUnits(); i++)
  {
    AIUnit *unit = mySubgroup->GetUnit(i);
    if (!unit || !unit->IsUnit())
      continue;
    float cost = unit->GetVehicle()->GetBaseCost(geogr, false, true)
      * unit->GetVehicle()->GetFieldCost(geogr);
    if (cost > costNormal) costNormal = cost;
  }
  float invCost = 1.0 / (costNormal * LandGrid);

  float ptsLand = 800.0 * _invScaleX * InvLandRange;  // avoid dependence on video resolution
  float invPtsLand = 1.0 / ptsLand;

  int iStep = toIntCeil(_ptsPerSquareCost * invPtsLand);
  float xStep = iStep * InvLandRange * _invScaleX;
  int jStep = iStep;
  float yStep = jStep * InvLandRange * _invScaleY;
  int iMin = iStep * toIntFloor(-_mapX / xStep);
  float xMin = _x - fastFmod(-_mapX, xStep);
  int jMin = LandRange - jStep * toIntFloor(-_mapY / yStep);
  float yMin = _y - fastFmod(-_mapY, yStep);

  float y = yMin;
  int j = jMin;
  while (y <= _y + _h)
  {
    float x = xMin;
    int i = iMin;
    while (x <= _x + _w)
    {
      // get geography info
      DrawCost(mySubgroup, invCost, x, y, xStep, yStep, i, j, iStep, -jStep);
      i += iStep;
      x += xStep;
    }
    j -= jStep;
    y += yStep;
  }
}

#if _ENABLE_CHEATS

inline PackedColor AgeToColor(float age)
{
  static const float alpha = 0.5f;
  static const float maxAge = 60.0f;
  static const float invMaxAge = 1.0f / maxAge;
  static const Color color0(0, 1, 0, alpha);
  static const Color color1(1, 1, 0, alpha);

  if (age < 0) return PackedColor(Color(1, 0, 0, alpha));
  saturateMin(age, maxAge);
  return PackedColor(color0 * (1.0f - age * invMaxAge) + color1 * (age * invMaxAge));
}

void CStaticMapMain::DrawOperCache(float x, float y, float xStep, float yStep, int i, int j, int iStep, int jStep)
{
  Draw2DParsNoTex pars;

  if (iStep == 1 && jStep == -1)
  {
    // ALL
    const OperField *field = GLandscape->OperationalCache()->CheckField(LandIndex(i), LandIndex(j + jStep));
    if (field)
      pars.SetColor(AgeToColor(field->GetAge()));
    else
      pars.SetColor(PackedColor(0));
  }
  else
  {
    // TL
    const OperField *field = GLandscape->OperationalCache()->CheckField(LandIndex(i), LandIndex(j));
    if (field)
      pars.colorTL = AgeToColor(field->GetAge());
    else
      pars.colorTL = PackedColor(0);
    // TR
    field = GLandscape->OperationalCache()->CheckField(LandIndex(i + iStep), LandIndex(j));
    if (field)
      pars.colorTR = AgeToColor(field->GetAge());
    else
      pars.colorTR = PackedColor(0);
    // BL
    field = GLandscape->OperationalCache()->CheckField(LandIndex(i), LandIndex(j + jStep));
    if (field)
      pars.colorBL = AgeToColor(field->GetAge());
    else
      pars.colorBL = PackedColor(0);
    // BR
    field = GLandscape->OperationalCache()->CheckField(LandIndex(i + iStep), LandIndex(j + jStep));
    if (field)
      pars.colorBR = AgeToColor(field->GetAge());
    else
      pars.colorBR = PackedColor(0);
  }

  pars.spec = NoZBuf|IsAlpha|IsAlphaFog|NoClamp;
  Rect2DPixel rect(x * _wScreen, y * _hScreen, xStep * _wScreen, yStep * _hScreen);
  GEngine->Draw2D(pars, rect, _clipRect);
}

void CStaticMapMain::DrawOperCache()
{
  float ptsLand = 800.0 * _invScaleX * InvLandRange;  // avoid dependence on video resolution
  float invPtsLand = 1.0 / ptsLand;

  int iStep = toIntCeil(_ptsPerSquareCost * invPtsLand);
  if (iStep != 1) return; // draw diagnostics only when zoomed enough
  float xStep = iStep * InvLandRange * _invScaleX;
  int jStep = iStep;
  float yStep = jStep * InvLandRange * _invScaleY;
  int iMin = iStep * toIntFloor(-_mapX / xStep);
  float xMin = _x - fastFmod(-_mapX, xStep);
  int jMin = LandRange - jStep * toIntFloor(-_mapY / yStep);
  float yMin = _y - fastFmod(-_mapY, yStep);

  float y = yMin;
  int j = jMin;
  while (y <= _y + _h)
  {
    float x = xMin;
    int i = iMin;
    while (x <= _x + _w)
    {
      DrawOperCache(x, y, xStep, yStep, i, j, iStep, -jStep);
      i += iStep;
      x += xStep;
    }
    j -= jStep;
    y += yStep;
  }
}

void CStaticMapMain::DrawRoadNet(float x, float y, float xStep, float yStep, int i, int j)
{
  AIBrain *me = GetMyUnit();
  Transport *veh = me ? me->GetVehicleIn() : NULL;
  bool soldier = veh == NULL;
  float vehWidth = 0;
  if (veh)
  {
    LODShape *shape = veh->GetShape();
    if (shape) vehWidth = shape->Max().X() - shape->Min().X();
  }

  PackedColor color(Color(0, 1, 1, 1));
  float textSize = xStep * floatMax(_sizeInfo, 0.1f);

  const RoadList &roadList = GLandscape->GetRoadNet()->GetRoadList(i, j - 1);
  for (int i=0; i<roadList.Size(); i++)
  {
    const RoadLink *item = roadList[i];
    if (!item) continue;
    // each RoadLink contain several nodes for A*
    for (int j=0; j<item->NNodes(soldier); j++)
    {
      RoadNode node = item->GetNode(j);
      if (!node.IsValid()) continue;

      Vector3 posB = node.Position(soldier, vehWidth);
      DrawSign(_texturePath, color, posB, 4, 4, 0);

#if 0 // nodes description
      if (0.2f * textSize >= 0.01f)
      {
        DrawCoord pt = WorldToScreen(posB);
        GEngine->DrawText(Point2DFloat(pt.x + 0.01, pt.y), 0.2f * textSize, Rect2DFloat(_x, _y, _w, _h), _fontInfo, _colorNames, cc_cast(node.GetDebugName()));
      }
#endif

#if 1 // draw connections
      for (int k=0; k<node.NConnections(soldier); k++)
      {
        RoadNode to = node.GetConnection(soldier, k);
        if (!to.IsValid()) continue;

        Vector3 posE = to.Position(soldier, vehWidth);
        DrawArrow(posB, posE, color);
      }
#else // draw orientation
      Vector3 dir = node.Direction();
      if (dir.SquareSizeXZ() > 0.01)
      {
        DrawArrow(posB, posB + 5 * dir, color);
      }
#endif
    }

    if (textSize >= 0.01f)
    {
      RString text;
      if (item->IsBridge()) text = "Bridge - ";
      else text = "Road - ";
      switch (item->GetRoadType())
      {
      case RoadLink::RTError:
        text = text + "Error";
        break;
      case RoadLink::RTDisconnected:
        text = text + "Disconnected";
        break;
      case RoadLink::RTBegin:
        text = text + "Begin";
        break;
      case RoadLink::RTEnd:
        text = text + "End";
        break;
      case RoadLink::RTI:
        text = text + "I";
        break;
      case RoadLink::RTTR:
        text = text + "TR";
        break;
      case RoadLink::RTTL:
        text = text + "TL";
        break;
      case RoadLink::RTX:
        text = text + "X";
        break;
      }

      DrawCoord pt = WorldToScreen(item->Transform().Position());
      GEngine->DrawText(Point2DFloat(pt.x + 0.01, pt.y), textSize, Rect2DFloat(_x, _y, _w, _h), _fontInfo, _colorNames, text,0);
    }
  }
}

void CStaticMapMain::DrawRoadNet()
{
  float ptsLand = 800.0 * _invScaleX * InvLandRange;  // avoid dependence on video resolution
  float invPtsLand = 1.0 / ptsLand;

  int iStep = toIntCeil(_ptsPerSquareCost * invPtsLand);
  float xStep = iStep * InvLandRange * _invScaleX;
  int jStep = iStep;
  float yStep = jStep * InvLandRange * _invScaleY;
  int iMin = iStep * toIntFloor(-_mapX / xStep);
  float xMin = _x - fastFmod(-_mapX, xStep);
  int jMin = LandRange - jStep * toIntFloor(-_mapY / yStep);
  float yMin = _y - fastFmod(-_mapY, yStep);

  float y = yMin;
  int j = jMin;
  while (y <= _y + _h)
  {
    float x = xMin;
    int i = iMin;
    while (x <= _x + _w)
    {
      DrawRoadNet(x, y, xStep, yStep, i, j);
      i += iStep;
      x += xStep;
    }
    j -= jStep;
    y += yStep;
  }
}

#endif

void CStaticMapMain::DrawUnits(AICenter *center)
{
  if (!center) return;
  AICenter *myCenter = GetMyCenter();
  if (!myCenter) return;

  PackedColor colorAlive;
  PackedColor colorDead;
  TargetSide side = center->GetSide();

  if (myCenter->IsFriendly(side))
  {
    // colorAlive = _colorFriendly;
    // TODO: move colordead to config
    colorDead = PackedColor(Color(0,1,1));
  }
  else if (myCenter->IsEnemy(side))
  {
    // colorAlive = _colorEnemy;
    colorDead = PackedColor(Color(1,1,0));
  }
  else if (myCenter->IsNeutral(side))
  {
    //   colorAlive = _colorNeutral;
    colorDead = PackedColor(Color(1,0,1));
  }
  else
  {
    //  colorAlive = _colorUnknown;
    colorDead = PackedColor(Color(1,0,1));
  }

  switch(side)
  {
  case TWest:
    {
      colorAlive = _colorCivilian;
      break;
    }
  case TEast:
    {
      colorAlive = _colorEnemy;
      break;
    }
  case TGuerrila:
    {
      colorAlive = _colorFriendly;
      break;
    }
  default:
    {
      colorAlive = _colorNeutral;
      break;
    }
  }

  for (int i=0; i<center->NGroups(); i++)
  {
    AIGroup *grp = center->GetGroup(i);
    if (!grp) continue;
    for (int j=0; j<grp->NUnits(); j++)
    {
      AIUnit *unit = grp->GetUnit(j);
      if (!unit) continue;
      if (!unit->GetVehicle()) continue;
      int size = _sizeUnit;
      if (unit->IsGroupLeader()) size = _sizeLeader;
      PackedColor color = colorAlive;
      if (!unit->LSIsAlive()) color = colorDead;
      Vector3 dir = unit->GetVehicle()->RenderVisualState().Direction();
      float azimut = atan2(dir.X(), dir.Z());
      DrawSign
        (
        unit->GetVehicle()->GetIcon(), color,
        unit->Position(unit->GetRenderVisualState()),
        size, size, azimut, RString(), false, -1,  _shadow
        );

#if _ENABLE_CHEATS
      if (CHECK_DIAG(DEFSM))
      {
        #if 0
        AIUnit *next = unit->FSMGetFormationNext();
        if (next && next->FSMGetFormationPrev() == unit)
          DrawArrow(unit->Position(), next->Position(), PackedColor(Color(0, 1, 0, 1)));
        else if (next)
        {
          DrawArrow(unit->Position(), next->Position(), PackedColor(Color(0, 0, 1, 1)));
        }

        AIUnit *prev = unit->FSMGetFormationPrev();
        if (prev && prev->FSMGetFormationNext() != unit)
          DrawArrow(prev->Position(), unit->Position() , PackedColor(Color(1, 0, 0, 1)));
        #endif

        struct DrawPrev: AIUnit::ForEachFormationCallback
        {
          CStaticMap &map;
          
          DrawPrev(CStaticMap &map):map(map){}
          void Do(const AIUnit *unit, const AIUnit *next)
          {
            map.DrawArrow(unit->Position(unit->GetRenderVisualState()), next->Position(next->GetRenderVisualState()), PackedColor(Color(1, 1, 0, 1)));
          }
        } drawPrev(*this);
        
        unit->ForEachFormationPrev(drawPrev);
        
        {
          EntityAIFull *veh = unit->GetVehicle();
          Vector3Val moveDir = unit->GetSubgroup()->GetMoveDirection();
          float offset = veh->GetType()->GetFormationZ()* 10; // GetFormationZ() is 5 m for a soldier in Arma 2
          // final destination of a leader (AI) - how to get? leaderveh->GetWantedPosition()?
          // estimated final destination of a leader (player)
          Vector3 formPosNow = unit->GetFormationAbsolute();
          Vector3 formPosTo = formPosNow+moveDir*offset;
          Vector3 formPosIdeal = unit->GetFormationAbsoluteFreeY(0,true);
          Vector3 formPosWanted = unit->IsAnyPlayer() ? unit->GetFormationAbsoluteFreeY() : unit->GetWantedPosition();

          DrawArrow(unit->Position(unit->GetRenderVisualState()), formPosNow , PackedColor(Color(0, 0, 0.7, 1)));
          if (formPosIdeal.DistanceXZ(formPosNow)>0.1f) DrawArrow(formPosNow, formPosIdeal , PackedColor(Color(0, 0.7, 0.7, 1)));
          DrawArrow(formPosNow, formPosTo , PackedColor(Color(1, 0, 0.5, 1)));
          if (formPosWanted.DistanceXZ(formPosNow)>0.1f) DrawArrow(formPosNow, formPosWanted , PackedColor(Color(0, 0.5, 0.2, 1)));
        }

        if (unit->IsSubgroupLeader())
        {
          AISubgroup *subgrp = unit->GetSubgroup();
          DrawCoord pt0 = WorldToScreen(unit->Position(unit->GetRenderVisualState()));

          {
            // draw formation direction
            Vector3 dir = subgrp->GetFormationDirection();
            dir[1] = 0;
            dir.Normalize();
            DrawCoord ptB = pt0 + DrawCoord(0.02f * dir.X(),-0.02f*dir.Z());
            DrawCoord ptE = pt0 + DrawCoord(0.04f * dir.X(),-0.04f*dir.Z());
            GEngine->DrawLine(
              Line2DPixel(ptB.x * _wScreen, ptB.y * _hScreen, ptE.x * _wScreen, ptE.y * _hScreen),
              _colorMe, _colorMe, _clipRect
            );
          }
          {
            // draw formation movement direction
            Vector3 dir = subgrp->GetMoveDirection();
            dir[1] = 0;
            dir.Normalize();
            DrawCoord ptB = pt0 + DrawCoord(0.04f * dir.X(),-0.04f*dir.Z());
            DrawCoord ptE = pt0 + DrawCoord(0.06f * dir.X(),-0.06f*dir.Z());
            GEngine->DrawLine(
              Line2DPixel(ptB.x * _wScreen, ptB.y * _hScreen, ptE.x * _wScreen, ptE.y * _hScreen),
              _colorMe, _colorMe, _clipRect
            );
          }


        
        }


      }
#endif
    }
  }
}

void CStaticMapMain::DrawPath()
{
#if _ENABLE_CHEATS
  AIBrain *me = GetMyUnit();
  AIBrain *myPilot = me ? me->GetVehicle()->PilotUnit() : NULL;
  if (!myPilot) return;

  Texture *texture = GPreloadedTextures.New(TextureDefault);

  const IAIPathPlanner &planner = myPilot->GetPlanner();
  DrawSign(texture, PackedColor(Color(1, 0, 0, 1)), planner.GetDestination(), 4, 4, 0);

  AutoArray<Vector3> array;
  planner.GetClosedList(array);
  for (int i=0; i<array.Size(); i++)
  {
    DrawSign(texture, PackedColor(Color(0.5, 0.5, 0.5, 0.5)), array[i], 2, 2, 0);
  }
  planner.GetOpenList(array);
  for (int i=0; i<array.Size(); i++)
  {
    DrawSign(texture, PackedColor(Color(1, 0.5, 0.5, 0.5)), array[i], 2, 2, 0);
  }
  DrawSign(texture, PackedColor(Color(1, 1, 0, 0.5)), planner.GetBest(), 4, 4, 0);
#endif

  /*
  DrawCoord ptB = WorldToScreen(pos);
  DrawCoord ptE;
  ptE.x = ptB.x + 0.02 * dir.X();
  ptE.y = ptB.y - 0.02 * dir.Z();
  GLOB_ENGINE->DrawLine
  (
  Line2DPixel(ptB.x * _wScreen, ptB.y * _hScreen,
  ptE.x * _wScreen, ptE.y * _hScreen),
  _colorMe, _colorMe, _clipRect
  );
  */

}

bool CStaticMapMain::CanCenter()
{
  if
    (
#if _ENABLE_CHEATS
    _showUnits ||
#endif
    Glob.config.IsEnabled(DTMap)
    ) return true;

  AIBrain *me = GetMyUnit();
  if (!me) return false;

  const VehicleType *type = me->GetVehicle()->GetType();
  return type->IsKindOf(GWorld->Preloaded(VTypeAir)) || type->IsKindOf(GWorld->Preloaded(VTypeTank));
}

#if _ENABLE_CHEATS
class DrawUpdateState
{
  CStaticMap *_map;

public:
  explicit DrawUpdateState(CStaticMap *map) : _map(map) {}

  bool operator () (Entity *obj) const
  {
    if (!obj->IsLocal() && !obj->GetNetworkId().IsNull())
    {
      float age = Glob.time - obj->GetLastPositionUpdate();
      float size = 10.0 / (1.0 + age);
      _map->DrawEllipse(obj->RenderVisualState().Position(), size, size, 0, PackedColor(Color(1, 0, 0, 1)));

      age = Glob.time - obj->GetLastGenericUpdate();
      size = 10.0 / (1.0 + age);
      _map->DrawEllipse(obj->RenderVisualState().Position(), size, size, 0, PackedColor(Color(0.25, 0, 0.5, 1)));

      age = Glob.time - obj->GetLastDamageUpdate();
      size = 10.0 / (1.0 + age);
      _map->DrawEllipse(obj->RenderVisualState().Position(), size, size, 0, PackedColor(Color(0, 0.5, 0.25, 1)));
    }
    return false;
  }
};
#endif

#if _ENABLE_CHEATS
EntityAI *CStaticMapMain::GetObserver() const
{
  return GetVehicle(_infoClick);
}
#endif

void CStaticMapMain::DrawExt(float alpha)
{
  //for(int i=0;i<N_MOUSE_BUTTONS;i++)
  //  GInput.ProcessMouseButtonLevel(i,255);

  CStaticMap::DrawExt(alpha);

#if _ENABLE_CHEATS
  if (!_show) return;

  if (_showUnits || _showTargets) DrawExposure();
  if (CHECK_DIAG(DEOperCache)) DrawOperCache();
  if (CHECK_DIAG(DECostMap)) DrawCost();
  if (CHECK_DIAG(DEPath)) DrawPath();
  if (CHECK_DIAG(DERoads)) DrawRoadNet();
#endif

  // draw dynamic locations
  const float elevationOffset = GLandscape->GetLandElevationOffset();
  for (int i=0; i<GWorld->GetLocations().Size(); i++)
    GWorld->GetLocations()[i]->Draw(this,false,elevationOffset);

  AIBrain *me = GetMyUnit();
  AIGroup *myGroup = me ? me->GetGroup() : NULL;
  AICenter *myCenter = GetMyCenter();

#if _ENABLE_CHEATS
  EntityAI *observer = GetVehicle(_infoClick);
  AIBrain *observerUnit = observer ? observer->CommanderUnit() : NULL;
  if (!observerUnit) observerUnit = me;
  AIGroup *observerGroup = observerUnit ? observerUnit->GetGroup() : NULL;
  AICenter *observerCenter = observerGroup ? observerGroup->GetCenter() : NULL;
#else
  AIBrain *observerUnit = me;
  //AIGroup *observerGroup = myGroup;
  //AICenter *observerCenter = myCenter;
#endif

#if _VBS3 // markers moved to CStaticMap
  if (!_onlyDrawMarkers) DrawCommands();
  DrawMarkers();
  if (_onlyDrawMarkers) return;
#else
  DrawCommands();
  DrawMarkers();
#endif

#if _ENABLE_CHEATS
  if (_showSensors) DrawSensors();
#endif
  if (myGroup && myCenter) DrawWaypoints(myCenter, myGroup);

#if _ENABLE_IDENTITIES
  if (me && me->GetPerson())
  {
    const Identity &identity = me->GetPerson()->GetIdentity();

    // simple tasks
    const RefArray<Task> &tasks = identity.GetSimpleTasks();
    for (int i=0; i<tasks.Size(); i++)
    {
      const Task *task = tasks[i];
      Vector3 pos;
      if (task && task->GetDestination(pos))
      {
        float alpha = 0.5f;
        if (task == identity.GetCurrentTask()) alpha = 1;
        PackedColor color = ModAlpha(_infoTask.color, alpha);
        Ref<Texture> taskTexture = NULL; 
        switch (task->GetState())
        {
        case TSAssigned:
          {
            taskTexture = _infoTask.icon;
            break;
          }
        case TSSucceeded:
          {
            color = ModAlpha(_infoTask.colorDone, alpha);
            taskTexture = _infoTask.iconDone;
            break;
          }
        case TSCanceled:
          {
            color = ModAlpha(_infoTask.colorCanceled, alpha);
            taskTexture = _infoTask.iconCanceled;
            break;
          }
        case TSFailed:
          {
            color = ModAlpha(_infoTask.colorFailed, alpha);
            taskTexture = _infoTask.iconFailed;
            break;
          }
        case TSCreated:
        case TSNone:
          {
            color = ModAlpha(_infoTask.colorCreated, alpha);
            taskTexture = _infoTask.iconCreated;
            break;
          }
        }
        DrawSign(taskTexture, color, pos, _infoTask.size, _infoTask.size, 0);
      }
    }

    // custom mark
    Vector3 pos;
    if (identity.GetCustomMark(pos))
    {
      DrawSign(_infoCustomMark.icon, _infoCustomMark.color, pos, _infoCustomMark.size, _infoCustomMark.size, 0);
    }
  }
#endif

#if _ENABLE_CHEATS
  if (CHECK_DIAG(DEUpdates))
  {
    GWorld->ForEachVehicle(DrawUpdateState(this));
  }
  if (_showUnits || _showTargets)
  {
    // draw camera positions on server
    const AutoArray<PlayerIdentity> *identities = GetNetworkManager().GetIdentities();
    if (identities) for (int i=0; i<identities->Size(); i++)
    {
      int dpnid = identities->Get(i).dpnid;
      Vector3 pos = GetNetworkManager().GetCameraPosition(dpnid);
      if (pos.SquareSize() > 0.01)
      {
        DrawSign(_iconCamera, PackedColor(Color(0.8, 0.4, 0, 1)), pos, 24, 24, 0);
      }
    }
  }
  if (_showUnits || CHECK_DIAG(DEFSM))
  {
    DrawUnits(GWorld->GetWestCenter());
    DrawUnits(GWorld->GetEastCenter());
    DrawUnits(GWorld->GetGuerrilaCenter());
    DrawUnits(GWorld->GetCivilianCenter());
    DrawUnits(GWorld->GetLogicCenter());
  }
  else if (_showTargets)
  {
    // draw targets
    int n = observerCenter ? observerCenter->NTargets() : 0;
    for (int i=0; i<n; i++)
    {
      const AITargetInfo &info = observerCenter->GetTarget(i);
      if (info._vanished) continue;
      Object *objTgt = info._idExact;
      EntityAI *veh = dyn_cast<EntityAI>(objTgt);
      if (!veh) continue;
      //const VehicleType *type = veh->GetType(info.FadingTypeAccuracy());
      //TargetSide side = veh->GetTargetSide(info.FadingSideAccuracy());
      const VehicleType *type = info._type;
      TargetSide side = info._destroyed ? TCivilian : info._side;

      //if (info._disappeared) side = TCivilian;

      int alpha8 = 255;
      if (!type->IsKindOf(GLOB_WORLD->Preloaded(VTypeStrategic)) )
      {
        if (!type->IsKindOf(GLOB_WORLD->Preloaded(VTypeAllVehicles))) continue;
        float alpha = info.FadingPositionAccuracy();
        if (alpha < 0.1) continue;
        alpha8 = toIntFloor(alpha * 255);
        if (alpha8 < 0) alpha8 = 0;
        if (alpha8 > 255) alpha8 = 255;
      }

      PackedColor color;
      /*if (side == TCivilian)
      color = _colorCivilian;
      else if (observerCenter->IsFriendly(side))
      color = _colorFriendly;
      else if (observerCenter->IsEnemy(side))
      color = _colorEnemy;
      else if (observerCenter->IsNeutral(side))
      color = _colorNeutral;
      else
      color = _colorUnknown;*/

      switch (side)
      {
      case TWest:
        {
          color = _colorCivilian;
          break;
        }
      case TEast:
        {
          color = _colorEnemy;
          break;
        }
      case TGuerrila:
        {
          color = _colorFriendly;
          break;
        }
      default:
        {
          color = _colorNeutral;
          break;
        }
      }
      color.SetA8(alpha8);

      float azimut = atan2(info._dir.X(), info._dir.Z());
      DrawSign(type->GetIcon(), color, info._realPos, _sizeLeader, _sizeLeader, azimut, RString(),NULL, -1, _shadow);
    }
  }
  else 
#endif
    if (Glob.config.IsEnabled(DTMap) && !_miniMapMode)
    {
      const TargetList *list = observerUnit ? observerUnit->AccessTargetList() : NULL;
      if (list)
      {
        int n = list->AnyCount();
        for (int i=0; i<n; i++)
        {
          const TargetNormal *target = list->GetAny(i);
          if (!target->IsKnownBy(observerUnit) || target->vanished || target->destroyed) continue;

          const VehicleType *type = target->type;
          if
            (
            !type->IsKindOf(GWorld->Preloaded(VTypeAllVehicles)) &&
            !type->IsKindOf(GWorld->Preloaded(VTypeStrategic))
            ) continue;

          // visibility
          float visible = target->FadingSpotability();
          if (visible <= 0.01) continue;
          saturateMin(visible, 1);

          if(!CheckItemWantedDraw(target)) continue;
          // color
          TargetSide side = target->side;

          PackedColor color;
          /*if (observerUnit->IsFriendly(side))
          color = _colorFriendly;
          else if (observerUnit->IsEnemy(side))
          color = _colorEnemy;
          else if (observerUnit->IsNeutral(side))
          color = _colorNeutral;
          else
          color = _colorUnknown;*/
          switch (side)
          {
          case TWest:
            {
              color = _colorCivilian;
              break;
            }
          case TEast:
            {
              color = _colorEnemy;
              break;
            }
          case TGuerrila:
            {
              color = _colorFriendly;
              break;
            }
          default:
            {
              color = _colorNeutral;
              break;
            }
          }
          color.SetA8(toIntFloor(color.A8() * visible));

          Vector3 pos = target->LandAimingPosition();
          // azimut not used (known) for group targets
          //Vector3 dir = VZero; // ???
          float azimut = atan2(target->dir.X(), target->dir.Z());
          DrawSign(target->type->GetIcon(), color, pos, _sizeLeader, _sizeLeader, azimut, RString(), NULL, -1, _shadow);
        }
      }
    }
    else DrawInfo();

    // draw selected units
#if _ENABLE_CHEATS
    if (_showUnits && myGroup && me == myGroup->Leader() || Glob.config.IsEnabled(DTMap))
    {
      OLinkPermNOArray(AIUnit) list;
      GWorld->UI()->ListSelectingUnits(list);
      AutoArray<RString> vehicleUnitsText;
      RefArray<Transport> vehicleUnits;

      for (int i=0; i<list.Size(); i++)
      {
        AIUnit *unit = list[i];
        Assert(unit);
        //units in one vehicle will be drawn at once at the end to avoid several numbers on one place
        Transport *t =  unit->GetVehicleIn();
        if(t)
        {
          int i;
          for(i=0;i<vehicleUnits.Size();i++)  
          {
            if (t == vehicleUnits[i]) 
            {//put all unit's IDs in one string
              vehicleUnitsText[i] = vehicleUnitsText[i] + "," + FormatNumber(unit->ID());             
              break;
            }
          }
          if(i == vehicleUnits.Size())
          {//nobody from this vehicle in not yet to be drawn
            vehicleUnits.Add(t); vehicleUnitsText.Add(FormatNumber(unit->ID()));
          }
          continue;
        }
        //draw selected units
        Point3 pos = unit->Position(unit->GetRenderVisualState());
        DrawCoord posMap = WorldToScreen(pos);
        bool draw = DrawSign
          (
          _iconSelect, _colorSelect,
          posMap,
          16, 16, 0
          );
        if (draw)
        {
          float x = posMap.x + 0.015;
          float y = posMap.y - 0.015;
          GLOB_ENGINE->DrawTextF
            (
            Point2DFloat(x, y), _sizeUnits, _fontUnits, _colorSelect, 0,
            "%d", unit->ID()
            );
        }
      }
      //draw all units in one vehicle at once, so we can put IDs in one string
      for(int i=0;i<vehicleUnits.Size();i++) 
      {
        Point3 pos = vehicleUnits[i]->RenderVisualState().Position();
        DrawCoord posMap = WorldToScreen(pos);
        bool draw = DrawSign
          (
          _iconSelect, _colorSelect,
          posMap,
          16, 16, 0
          );
        if (draw)
        {
          float x = posMap.x + 0.015;
          float y = posMap.y - 0.015;
          GLOB_ENGINE->DrawTextF
            (
            Point2DFloat(x, y), _sizeUnits, _fontUnits, _colorSelect, 0,
            vehicleUnitsText[i]
          );
        }
      }
    }
#endif

    // draw clicked unit (helpful for cheats)
#if _ENABLE_CHEATS
    EntityAI *vehClick = GetVehicle(_infoClick);
    if (vehClick)
    {
      PackedColor color(Color(1, 1, 1, 0.5));
      if (_showUnits)
      {
        // draw directly on the vehicle position
        DrawSign(_iconPlayer, color, vehClick->RenderVisualState().Position(), _sizePlayer, _sizePlayer, 0);
      }
      else if (_showTargets)
      {
        // find in the center's list of targets
        AITargetInfo *info = observerCenter->FindTargetInfo(vehClick);
        if (info)
        {
          DrawSign(_iconPlayer, color, info->_realPos, _sizePlayer, _sizePlayer, 0);
        }
      }
      else if (Glob.config.IsEnabled(DTMap) && !_miniMapMode)
      {
        const TargetList *list = observerUnit ? observerUnit->AccessTargetList() : NULL;
        if (list)
        {
          const TargetNormal *target = list->FindTarget(vehClick);
          if (target)
          {
            DrawSign(_iconPlayer, color, target->LandAimingPosition(), _sizePlayer, _sizePlayer, 0);
          }
        }
      }
    }
#endif

    // draw myself
    if
      (
#if _ENABLE_CHEATS
      _showUnits ||
#endif
      Glob.config.IsEnabled(DTMap) || _miniMapMode
      )
    {
      Object *myVehicle = NULL;
      if (me)
      {
        myVehicle = me->GetVehicle();
      }
      else if (GLOB_WORLD->CameraOn())
      {
        myVehicle = GLOB_WORLD->CameraOn();
      }

      if (myVehicle)
      {
        Point3 pos = myVehicle->RenderVisualState().Position();
        PackedColor BlinkColor(PackedColor color1, PackedColor color2, float t);
        PackedColor color = BlinkColor(_colorMe, _colorMe2, _blinkingSpeedMe * Glob.uiTime.toFloat());
        bool draw = DrawSign
          (
          _iconPlayer, color,
          pos,
          _sizePlayer, _sizePlayer, 0
          );
        if (draw)
        {
          // draw orientation
          Vector3 dir = myVehicle->RenderVisualState().Direction();
          dir[1] = 0;
          dir.Normalize();
          DrawCoord ptB = WorldToScreen(pos);
          DrawCoord ptE;
          ptE.x = ptB.x + 0.02 * dir.X();
          ptE.y = ptB.y - 0.02 * dir.Z();
          GLOB_ENGINE->DrawLine
            (
            Line2DPixel(ptB.x * _wScreen, ptB.y * _hScreen,
            ptE.x * _wScreen, ptE.y * _hScreen),
            color, color, _clipRect
            );
        }
      }

    }
    else if (me)
    {
      const VehicleType *type = me->GetVehicle()->GetType();
      if
        (
        type->IsKindOf(GWorld->Preloaded(VTypeAir)) ||
        type->IsKindOf(GWorld->Preloaded(VTypeTank))
        )
      {
        PackedColor color = PackedColor(Color(0, 0, 0, 1));
        const float h1 = 4.0 * (1.0 / 480);
        const float w1 = 4.0 * (1.0 / 640);
        const float h2 = 8.0 * (1.0 / 480);
        const float w2 = 8.0 * (1.0 / 640);
        DrawCoord pt = WorldToScreen(me->Position(me->GetRenderVisualState()));

        float w = _wScreen;
        float h = _hScreen;

        GEngine->DrawLine
          (
          Line2DPixel(_clipRect.x, pt.y * h, (pt.x - w1) * w, pt.y * h),
          color, color, _clipRect
          );
        GEngine->DrawLine
          (
          Line2DPixel((pt.x + w1) * w, pt.y * h, _clipRect.x + _clipRect.w, pt.y * h),
          color, color, _clipRect
          );
        GEngine->DrawLine
          (
          Line2DPixel(pt.x * w, _clipRect.y, pt.x * w, (pt.y - h1) * h),
          color, color, _clipRect
          );
        GEngine->DrawLine
          (
          Line2DPixel(pt.x * w, (pt.y + h1) * h, pt.x * w, _clipRect.y + _clipRect.h),
          color, color, _clipRect
          );
        MipInfo mip = GEngine->TextBank()->UseMipmap(_iconPosition, 0, 0);
        GEngine->Draw2D
          (
          mip, color,
          Rect2DPixel(CX(pt.x - w2), CY(pt.y - h2), CW(2 * w2), CH(2 * h2)), _clipRect
          );
      }
    }
    //draw HC group icons
    DrawGroupIcons();

    //  if (_showTargets || _showUnits || Glob.config.easyMode)
    {
      DrawLabel(_infoMove, _colorInfoMove,0);
    }
#if _ENABLE_CHEATS
    if (_showUnits || _showTargets)
    {
      // draw labels
      // draw my path
      AIBrain *myPilot = me ? me->GetVehicle()->PilotUnit() : NULL;
      if (myPilot)
      {
        int n = myPilot->GetPlanner().GetPlanSize();
        int m = myPilot->GetPlanner().FindBestIndex(me->Position(me->GetRenderVisualState()));
        Vector3 oldPos;
        for (int i=0; i<m; i++)
        {
          PlanPosition pos;
          myPilot->GetPlanner().GetPlanPosition(i, pos);
          if (i > 0) DrawLine(oldPos, pos, _colorPathDone);
          oldPos = pos;
          DrawSign(_texturePathDone, _colorPathDone, pos, _sizePathDone, _sizePathDone, 0);
        }
        for (int i=m; i<n; i++)
        {
          PlanPosition pos;
          myPilot->GetPlanner().GetPlanPosition(i, pos);
          if (i > 0) DrawLine(oldPos, pos, _colorPath);
          oldPos = pos;
          DrawSign(_texturePath, _colorPath, pos, _sizePath, _sizePath, 0);
        }
      }

      // TODO: another drawing
      int n = sensorsMap.Size();
      for (int i=0; i<n; i++)
      {
        Vehicle *veh = sensorsMap[i];
        if (!veh) continue;
        Detector *sensor = dyn_cast<Detector>(veh);
        Assert(sensor);
        if (!sensor) continue;
        if (!sensor->IsCountdown()) continue;
        float x = floatMax(_x, 0);
        float y = floatMax(_y, 0);
        GLOB_ENGINE->DrawTextF
          (
          Point2DFloat(x, y), 21.0f/408.0f, _font, _ftColor, 0,
          "COUNTDOWN: %.0f",
          sensor->GetCountdown()
          );
      }
    }
#endif

    if (_activeMarker >= 0)
    {
      const float rectH2 = 0.5 * _sizeActiveMarker * _hScreen * (1.0 / 480);
      const float rectW2 = 0.5 * _sizeActiveMarker * _wScreen * (1.0 / 640);
      ArcadeMarkerInfo &mInfo = markersMap[_activeMarker];
      DrawCoord pt = WorldToScreen(mInfo.position);
      pt.x *= _wScreen;
      pt.y *= _hScreen;
      GEngine->DrawLine
        (
        Line2DPixel(pt.x - rectW2, pt.y - rectH2, pt.x + rectW2, pt.y - rectH2),
        _colorActiveMarker, _colorActiveMarker, _clipRect
        );
      GEngine->DrawLine
        (
        Line2DPixel(pt.x + rectW2, pt.y - rectH2, pt.x + rectW2, pt.y + rectH2),
        _colorActiveMarker, _colorActiveMarker, _clipRect
        );
      GEngine->DrawLine
        (
        Line2DPixel(pt.x + rectW2, pt.y + rectH2, pt.x - rectW2, pt.y + rectH2),
        _colorActiveMarker, _colorActiveMarker, _clipRect
        );
      GEngine->DrawLine
        (
        Line2DPixel(pt.x - rectW2, pt.y + rectH2, pt.x - rectW2, pt.y - rectH2),
        _colorActiveMarker, _colorActiveMarker, _clipRect
        );
      GEngine->DrawLine
        (
        Line2DPixel(_clipRect.x, pt.y, pt.x - rectW2, pt.y),
        _colorActiveMarker, _colorActiveMarker, _clipRect
        );
      GEngine->DrawLine
        (
        Line2DPixel(pt.x + rectW2, pt.y, _clipRect.x + _clipRect.w, pt.y),
        _colorActiveMarker, _colorActiveMarker, _clipRect
        );
      GEngine->DrawLine
        (
        Line2DPixel(pt.x, _clipRect.y, pt.x, pt.y - rectH2),
        _colorActiveMarker, _colorActiveMarker, _clipRect
        );
      GEngine->DrawLine
        (
        Line2DPixel(pt.x, pt.y + rectH2, pt.x, _clipRect.y + _clipRect.h),
        _colorActiveMarker, _colorActiveMarker, _clipRect
        );
    }

#if _ENABLE_CHEATS
    if (_showVariables)
    {
      float top = _y + 0.2;
      float left = _x + 0.02;
      const VarBankType &vars = GWorld->GetMissionNamespace()->GetVariables(); // mission namespace
      if (vars.NItems() > 0)
      {
        // !!! avoid GetTable when NItems == 0
        for (int i=0; i<vars.NTables(); i++)
        {
          for (int j=0; j<vars.GetTable(i).Size(); j++)
          {
            GameVariable &var = vars.GetTable(i)[j];
            if (GWorld->GetGameState()->IsVisible(var)) continue;
            GLOB_ENGINE->DrawTextF
              (
              Point2DFloat(left, top), _sizeLabel, _fontLabel, PackedColor(Color(0, 0, 0, 1)), 0,
              "%s = %s",
              (const char *)var.GetName(),
              (const char *)var.GetValueText()
              );
            top += _sizeLabel;
          }
        }
      }
    }
#endif

    DrawLegend();
}

// simulation
void CStaticMapMain::Center(bool soft)
{
  Point3 pt = _defaultCenter;
  if (GLOB_WORLD->FocusOn())
    pt = GLOB_WORLD->FocusOn()->Position(GLOB_WORLD->FocusOn()->GetRenderVisualState());
  else if (GLOB_WORLD->CameraOn())
    pt = GLOB_WORLD->CameraOn()->RenderVisualState().Position();
  CStaticMap::Center(pt, soft);
}

void CStaticMapMain::CenterMiniMap()
{
  Point3 pt = _defaultCenter;
  float maxSpeed = 0;
  Vector3 vspeed(VZero);
  if (GLOB_WORLD->FocusOn())
  {
    pt = GLOB_WORLD->FocusOn()->Position(GLOB_WORLD->FocusOn()->GetRenderVisualState());
    EntityAI *entity = GLOB_WORLD->FocusOn()->GetVehicle();
    if (entity) 
    {
      maxSpeed = entity->GetType()->GetMaxSpeedMs();
      vspeed = entity->RenderVisualState().Speed();
    }
  }
  saturateMax(maxSpeed, 0.1f);
  float speed = vspeed.Size();
  const float timeCoef = 30.0f; //we want to see timeCoef * speed map location
  const float minSpeed = 7.0f;
  bool centralizeOnly = false;
  if (speed < minSpeed)
  {
    speed = minSpeed;
    centralizeOnly = true; //no map scaling nor player icon moving
  }

  float invSizeLand = 1 / (LandGrid * LandRange);
  //_scaleX is set so that map width corresponds to speed*timeCoef length in world space
  _scaleX = timeCoef * speed * invSizeLand / W();
  Precalculate();

  float xMap = pt.X() * invSizeLand * _invScaleX;
  float yMap = (1.0 - pt.Z() * invSizeLand) * _invScaleY;
  _mapX = _center.x - xMap;
  _mapY = _center.y - yMap;
  if (!centralizeOnly)
  {
    //shift the map in such a way, player is on elipse around map center (diameter given by speed / maxSpeed)
    float speedXZ = vspeed.SizeXZ();
    if (speedXZ > minSpeed) //for slow movement no effect
    {
      const float maxDiamCoef = 0.75f; //75%
      float speedCoef = (speed-minSpeed) / maxSpeed;
      saturateMin(speedCoef,1.0f);
      float sinus = vspeed.Z() / speedXZ;
      float cosinus = vspeed.X() / speedXZ;
      _mapX -= cosinus * maxDiamCoef * speedCoef * _halfSize.x;
      _mapY += sinus * maxDiamCoef * speedCoef * _halfSize.y;
    }
  }
  SaturateX(_mapX);
  SaturateY(_mapY);
}

void CStaticMapMain::OnDraw(UIViewport *vp, float alpha)
{
  if (_autoCenter) Center(false);
  CStaticMap::OnDraw(vp, alpha);
}

void CStaticMapMain::FindUnit(AICenter *center, Vector3Par pt, SignInfo &info, float &minDist)
{
  if (!center) return;

  for (int i=0; i<center->NGroups(); i++)
  {
    AIGroup *grp = center->GetGroup(i);
    if (!grp) continue;
    for (int j=0; j<grp->NUnits(); j++)
    {
      AIUnit *unit = grp->GetUnit(j);
      if (!unit) continue;
      float dist = (pt - unit->Position(unit->GetRenderVisualState())).SquareSizeXZ();
      if (dist < minDist)
      {
        minDist = dist;
        info._type = signUnit;
        info._unit = unit;
      }
    }
  }
}

void CStaticMapMain::FindWaypoint
(
 AICenter *myCenter, AIGroup *myGroup,
 Vector3Par pt, SignInfo &info, float &minDist
 )
{
#if _ENABLE_CHEATS
  if (_showUnits)
  {
    EntityAI *veh = dyn_cast<EntityAI>(GWorld->CameraOn());
    if (!veh) return;
    myGroup = veh->GetGroup();
    if (!myGroup) return;
    myCenter = myGroup->GetCenter();
  }
#endif

  int i = myGroup->ID() - 1;
  int m = myGroup->NWaypoints();

#if _ENABLE_CHEATS
  if (!_showUnits)
#endif
  {
    int nshow = 0;
    for (int j=1; j<m; j++)
    {
      const WaypointInfo &wInfo = myGroup->GetWaypoint(j);
      bool wpShow = wInfo.showWP == ShowAlways ||
        wInfo.showWP == ShowEasy && Glob.config.ShowCadetWP();
      if (wpShow) nshow++;
    }
    if (nshow == 0) return;
  }

  // waypoints
  for (int j=0; j<m; j++)
  {
    const WaypointInfo &wInfo = myGroup->GetWaypoint(j);
    bool wpShow = wInfo.showWP == ShowAlways ||
      wInfo.showWP == ShowEasy && Glob.config.ShowCadetWP();
    if
      (
      j > 0 &&
#if _ENABLE_CHEATS
      !_showUnits &&
#endif
      !wpShow
      ) continue;
    float dist = (pt - wInfo.position).SquareSizeXZ();
    if (dist < minDist)
    {
      minDist = dist;
      info._type = signArcadeWaypoint;
      info._indexGroup = i;
      info._index = j;
    }
  }
}

SignInfo CStaticMapMain::FindSign(float x, float y)
{
  AIBrain *me = GetMyUnit();
  AIGroup *myGroup = me ? me->GetGroup() : NULL;
  AICenter *myCenter = GetMyCenter();

  struct SignInfo info;
  info._type = signNone;
  info._unit = NULL;
  info._id = NULL;

#if _ENABLE_CHEATS
  EntityAI *observer = GetVehicle(_infoClick);
  AIBrain *observerUnit = observer ? observer->CommanderUnit() : NULL;
  if (!observerUnit) observerUnit = me;
  AIGroup *observerGroup = observerUnit ? observerUnit->GetGroup() : NULL;
  AICenter *observerCenter = observerGroup ? observerGroup->GetCenter() : NULL;
#else
  AIBrain *observerUnit = me;
  //AIGroup *observerGroup = myGroup;
  //AICenter *observerCenter = myCenter;
#endif

  Point3 pt = ScreenToWorld(DrawCoord(x, y));
  float sizeLand = LandGrid * LandRange;
  float dist, minDist = 0.02 * sizeLand * _scaleX;
  minDist *= minDist;

#if _ENABLE_CHEATS
  if (_showSensors)
  {
    for (int i=0; i<sensorsMap.Size(); i++)
    {
      Vehicle *veh = sensorsMap[i];
      Detector *det = dyn_cast<Detector>(veh);
      if (!det) continue;

      dist = (pt - det->RenderVisualState().Position()).SquareSizeXZ();
      if (dist < minDist)
      {
        minDist = dist;
        info._type = signArcadeSensor;
        info._index = i;
      }
    }
  }

  if (_showUnits)
  {
    FindUnit(GWorld->GetWestCenter(), pt, info, minDist);
    FindUnit(GWorld->GetEastCenter(), pt, info, minDist);
    FindUnit(GWorld->GetGuerrilaCenter(), pt, info, minDist);
    FindUnit(GWorld->GetCivilianCenter(), pt, info, minDist);
    FindUnit(GWorld->GetLogicCenter(), pt, info, minDist);
  }
  else if (_showTargets)
  {
    // find targets
    int n = observerCenter ? observerCenter->NTargets() : 0;
    for (int i=0; i<n; i++)
    {
      const AITargetInfo &target = observerCenter->GetTarget(i);
      if (target._vanished) continue;
      Object *objTgt = target._idExact;
      EntityAI *veh = dyn_cast<EntityAI>(objTgt);
      if (!veh) continue;
      const VehicleType *type = veh->GetType(target.FadingTypeAccuracy());
      //      TargetSide side = veh->GetTargetSide(target.FadingSideAccuracy());

      if (!type->IsKindOf(GLOB_WORLD->Preloaded(VTypeStrategic)) )
      {
        if (!type->IsKindOf(GLOB_WORLD->Preloaded(VTypeAllVehicles))) continue;
        float alpha = target.FadingPositionAccuracy();
        if (alpha < 0.1) continue;
      }

      dist = (pt - target._realPos).SquareSizeXZ();
      if (dist < minDist)
      {
        if (target._type->IsKindOf(GLOB_WORLD->Preloaded(VTypeStatic)) )
        {
          minDist = dist;
          info._type = signStatic;
          info._id = target._idExact;
        }
        else
        {
          minDist = dist;
          info._type = signVehicle;
          info._id = target._idExact;
        }
      }
    }
  }
  else 
#endif
    if (Glob.config.IsEnabled(DTMap) && !_miniMapMode)
    {
      const TargetList *list = observerUnit ? observerUnit->AccessTargetList() : NULL;
      if (list)
      {
        int n = list->AnyCount();
        for (int i=0; i<n; i++)
        {
          const TargetNormal *target = list->GetAny(i);
          if (!target->IsKnownBy(observerUnit) || target->vanished || target->destroyed) continue;

          // visibility
          float visible = target->FadingSpotability();
          if (visible <= 0.01) continue;

          dist = (pt - target->LandAimingPosition()).SquareSizeXZ();
          if (dist < minDist)
          {
            if (target->type->IsKindOf(GWorld->Preloaded(VTypeStrategic)))
            {
              minDist = dist;
              info._type = signStatic;
              info._id = target->idExact;
            }
            else if (target->type->IsKindOf(GWorld->Preloaded(VTypeAllVehicles)))
            {
              minDist = dist;
              info._type = signVehicleGroup;
              info._id = target->idExact;
            }
          }
        }
      }
    }

    // check waypoints
    if (myGroup && myCenter) FindWaypoint(myCenter, myGroup, pt, info, minDist);

    // check tasks
#if _ENABLE_IDENTITIES
    if (me)
    {
      const Identity &identity = me->GetPerson()->GetIdentity();

      // custom mark
      Vector3 pos;
      if (identity.GetCustomMark(pos))
      {
        float dist = (pt - pos).SquareSizeXZ();
        if (dist < minDist)
        {
          minDist = dist;
          info._type = signCustomMark;
          info._unit = me;
        }
      }

      // simple tasks
      const RefArray<Task> &tasks = identity.GetSimpleTasks();
      for (int i=0; i<tasks.Size(); i++)
      {
        const Task *task = tasks[i];
        Vector3 pos;
        if (task && task->GetDestination(pos))
        {
          float dist = (pt - pos).SquareSizeXZ();
          if (dist < minDist)
          {
            minDist = dist;
            info._type = signTask;
            info._unit = me;
            info._index = task->GetId();
          }
        }
      }
    }
#endif

    // check markers
    int n = markersMap.Size();
    for (int i=0; i<n; i++)
    {
      ArcadeMarkerInfo &mInfo = markersMap[i];
      dist = (pt - mInfo.position).SquareSizeXZ();
      if (dist < minDist)
      {
        minDist = dist;
        info._type = signArcadeMarker;
        info._index = i;
      }
    }
    return info;
}

AIGroup *CStaticMapMain::FindGroupMarker(float x, float y, int &waipoint)
{
  Point3 pt = ScreenToWorld(DrawCoord(x, y));
  float sizeLand = LandGrid * LandRange;
  float  minDist = 0.02 * sizeLand * _scaleX;
  minDist *= minDist;

  for (int i=0; i<TSideUnknown; i++)
  {
    AICenter *center = GWorld->GetCenter((TargetSide)i);
    if (center)
    {
      for (int i=0; i<center->NGroups(); i++)
      {//check all groups
        AIGroup *grp = center->GetGroup(i);
        if (!grp || grp->GetGroupIcons().Size()<=0) continue;

        AIUnit *unit = grp->Leader();
        if (!unit) continue;
        float dist = (pt - unit->Position(unit->GetRenderVisualState())).SquareSizeXZ();
        if (dist < minDist) return grp;

        if(grp->GetGroupIcon().ShowWaypoints())
        {//and it's waypoints
          for(int i=grp->GetCurrentWaypoint(); i< grp->NWaypoints(); i++) 
          {
            float dist = (pt - grp->GetWaypointPosition(i)).SquareSizeXZ();
            if (dist < minDist)
            {
              waipoint = i;
              return grp;
            }
          }
        }
      }
    }
  }
  return NULL;
}

void CStaticMapMain::IssueMove(float x, float y)
{
  AIBrain *me = GetMyUnit();
  AIGroup *myGroup = me ? me->GetGroup() : NULL;
  if (!myGroup) return;

  OLinkPermNOArray(AIUnit) list;
  GWorld->UI()->ListSelectingUnits(list);
  if (me == myGroup->Leader() && list.Size() > 0)
  {
    // command for units
    Command cmd;
    cmd._message = Command::Move;
    cmd._context = Command::CtxUI;
    cmd._destination = ScreenToWorld(DrawCoord(x, y));
    cmd._destination[1] = GLOB_LAND->RoadSurfaceY
      (
      cmd._destination[0], cmd._destination[2]
    );
    myGroup->SendCommand(cmd, list);

    int index = _mapInfo.commands.Add();
    _mapInfo.commands[index].grp = myGroup;
    _mapInfo.commands[index].id = cmd._id;
    _mapInfo.commands[index].state = CSSent;
    _mapInfo.commands[index].position = cmd._destination;
    GWorld->UI()->ClearSelectedUnits();
    GWorld->UI()->HideMenu();
  }
  else
  {
    Transport *veh = me->GetVehicleIn();
    if (veh && veh->CommanderUnit() == me && veh->PilotUnit() != me)
    {
      // command for vehicle
      Vector3 pos = ScreenToWorld(DrawCoord(x, y));
      pos[1] = GLOB_LAND->RoadSurfaceYAboveWater(pos[0], pos[2]);
      veh->SendMove(pos);
    }
  }
}

/*!
\patch 1.50 Date 4/16/2002 by Jirka
- Added: It is now possible to issue watch command from map interface
using left ALT key and click.
*/

void CStaticMapMain::IssueWatch(float x, float y)
{
  AIBrain *me = GetMyUnit();
  AIGroup *myGroup = me ? me->GetGroup() : NULL;
  if (!myGroup) return;

  OLinkPermNOArray(AIUnit) list;
  GWorld->UI()->ListSelectingUnits(list);
  if (me == myGroup->Leader() && list.Size() > 0)
  {
    // command for units
    Vector3 pos;
    pos = ScreenToWorld(DrawCoord(x, y));
    pos[1] = GLOB_LAND->RoadSurfaceYAboveWater(pos[0], pos[2]);
    myGroup->SendState(new RadioMessageWatchPos(myGroup, list, pos));
    GWorld->UI()->ClearSelectedUnits();
    GWorld->UI()->HideMenu();
  }
}

void CStaticMapMain::IssueAttack(TargetType *target)
{
  AIBrain *me = GetMyUnit();
  AIGroup *myGroup = me ? me->GetGroup() : NULL;
  if (myGroup && me == myGroup->Leader())
  {
    OLinkPermNOArray(AIUnit) list;
    GWorld->UI()->ListSelectingUnits(list);
    Command cmd;
    cmd._message = Command::AttackAndFire;
    cmd._context = Command::CtxUI;
    cmd._target = target;
    myGroup->SendCommand(cmd, list);
    GWorld->UI()->ClearSelectedUnits();
    GWorld->UI()->HideMenu();
  }
}

void CStaticMapMain::IssueGetIn(TargetType *target)
{
  AIBrain *me = GetMyUnit();
  AIGroup *myGroup = me ? me->GetGroup() : NULL;
  if (myGroup && me == myGroup->Leader())
  {
    OLinkPermNOArray(AIUnit) list;
    GWorld->UI()->ListSelectingUnits(list);
    Command cmd;
    cmd._message = Command::GetIn;
    cmd._context = Command::CtxUI;
    cmd._target = target;
    myGroup->SendCommand(cmd, list);
    GWorld->UI()->ClearSelectedUnits();
    GWorld->UI()->HideMenu();
  }
}

inline bool IsWatchRequired()
{
#ifdef _XBOX
  // return GInput.xInputButtons[XBOX_LeftTrigger] >= 1;
  return GInput.xInputButtons[XBOX_LeftThumb] > 0;
#else
  return GInput.keys[DIK_LMENU]>0;
#endif
}

#if _ENABLE_IDENTITIES
/// in-game navigate to the given diary topic, open a diary if needed

void ProcessDiaryLink(RString link, DisplayMap *map)
{//need to find better solution
  DisplayMainMap *mapIngame = dynamic_cast<DisplayMainMap *>(map);

  if (mapIngame)
  {
    GWorld->ShowMap(true);
    mapIngame->ProcessDiaryLink(link, DCInGame);
  }
  else
  {
    DisplayGetReady *readyMap = dynamic_cast<DisplayGetReady *>(map);
    if (readyMap)
    {
      readyMap->ProcessDiaryLink(link, DCBriefing);
    }
  }
}

void ProcessDiaryLink(RString link)
{
  ProcessDiaryLink(link, dynamic_cast<DisplayMap *>(GWorld->Map()));
}


#endif

bool CStaticMapMain::OnKeyDown(int dikCode)
{
  if (dikCode == 0) return false;

  if (CStaticMap::OnKeyDown(dikCode))
    return true;

  switch (dikCode)
  {
  case INPUT_DEVICE_XINPUT + XBOX_RightTrigger:
    {
      float mouseX = 0.5 + GInput.cursorX * 0.5;
      float mouseY = 0.5 + GInput.cursorY * 0.5;
      saturate(mouseX, 0, 1);
      saturate(mouseY, 0, 1);

      bool alt = IsWatchRequired();
      IssueCommand(mouseX, mouseY, alt, false);
    }
    return true;
  case DIK_DELETE:
    if (_infoMove._type == signArcadeMarker)
    {
      const char *userDefined = "_user_defined";
      RString name = markersMap[_infoMove._index].name;
      if (strnicmp(name, userDefined, strlen(userDefined)) == 0)
      {
#if _VBS3 // marker EH
        void ProcessDeleteMarkerEH(RString name);
        ProcessDeleteMarkerEH(name);
#endif
        GetNetworkManager().MarkerDelete(name);
        markersMap.Delete(_infoMove._index);
        _infoClick._type = signNone;
        _infoClickCandidate._type = signNone;
        _infoMove._type = signNone;
        _dragging = false;
        _selecting = false;
      }
      return true;
    }
    // continue
  case INPUT_DEVICE_XINPUT + XBOX_X:
#if _ENABLE_IDENTITIES
    if (_infoMove._type == signCustomMark)
    {
      Person *person = _infoMove._unit ? _infoMove._unit->GetPerson() : NULL;
      if (person)
      {
        person->GetIdentity().ClearCustomMark();
        _infoClick._type = signNone;
        _infoClickCandidate._type = signNone;
        _infoMove._type = signNone;
        _dragging = false;
        _selecting = false;
      }
      return true;
    }
#endif
    return true;
  case INPUT_DEVICE_XINPUT + XBOX_Y:
#if _ENABLE_IDENTITIES
    {
      AIBrain *me = GetMyUnit();
      Person *person = me ? me->GetPerson() : NULL;
      if (person)
      {
        float mouseX = 0.5 + GInput.cursorX * 0.5;
        float mouseY = 0.5 + GInput.cursorY * 0.5;
        saturate(mouseX, 0, 1);
        saturate(mouseY, 0, 1);
        Vector3 pos = ScreenToWorld(DrawCoord(mouseX, mouseY));
        pos[1] = GLandscape->SurfaceYAboveWater(pos[0],pos[2]);
        person->GetIdentity().SetCustomMark(pos);
      }

    }
#endif
    return true;
  case INPUT_DEVICE_XINPUT + XBOX_A:
#if _ENABLE_IDENTITIES
    if (_infoMove._type == signTask)
    {
      RString link = Format("task:%d", _infoClick._index);
      ProcessDiaryLink(link);
      return true;
    }
#endif
    return true;
  }
  return false;
}

void CStaticMapMain::ProcessCheats()
{
#if _ENABLE_CHEATS
  if (GInput.GetCheat1ToDo(DIK_A) || GInput.GetCheatXToDo(CXTMapShowUnits))
  {
    _showUnits = !_showUnits;
    GlobalShowMessage(500,"Show Units %s",_showUnits ? "On" : "Off");
  }
  if (GInput.GetCheat2ToDo(DIK_A) || GInput.GetCheatXToDo(CXTMapShowTriggers))
  {
    _showSensors = !_showSensors;
    GlobalShowMessage(500,"Show Triggers %s",_showSensors ? "On" : "Off");
  }
  if (GInput.GetCheat1ToDo(DIK_D))
  {
    _showTargets = !_showTargets;
    GlobalShowMessage(500,"Show Targets %s",_showTargets ? "On" : "Off");
  }
  if (GInput.GetCheat2ToDo(DIK_V))
  {
    _showVariables = !_showVariables;
    GlobalShowMessage(500,"Show Variables %s",_showVariables ? "On" : "Off");
  }
#endif
  CStaticMap::ProcessCheats();
}

extern GameValue GGroupMarkerClicked;
void CStaticMapMain::OnGroupMarkerClick(AIGroup *group,int RMB, float x, float y, int waypoint)
{
  bool issueCommand = true;

  bool eventHandlerValid = !GGroupMarkerClicked.GetNil();
  if (eventHandlerValid)
  {
#if USE_PRECOMPILATION
    if (GGroupMarkerClicked.GetType() == GameCode)
    {
      GameDataCode *code = static_cast<GameDataCode *>(GGroupMarkerClicked.GetData());
      eventHandlerValid = code->IsCompiled() && code->GetCode().Size() > 0;
    }
    else
#endif
    {
      RString code = GGroupMarkerClicked;
      eventHandlerValid = code.GetLength() > 0;
    }
  }

  if (eventHandlerValid)
  {
    GameValue value = GWorld->GetGameState()->CreateGameValue(GameArray);
    GameArrayType &arguments = value;

    arguments.Add(false);
    arguments.Add(GameValueExt(group));
    arguments.Add((float)waypoint);
    arguments.Add(float(RMB));
    arguments.Add(x);
    arguments.Add(y);

    arguments.Add((GInput.keyPressed[DIK_LSHIFT]||GInput.keyPressed[DIK_RSHIFT]));
    arguments.Add((GInput.keyPressed[DIK_LCONTROL]||GInput.keyPressed[DIK_RCONTROL]));
    arguments.Add((GInput.keyPressed[DIK_LALT]||GInput.keyPressed[DIK_RALT]));

    // Vector3 pos = ScreenToWorld(DrawCoord(x, y));
    GameState *state = GWorld->GetGameState();
    GameVarSpace vars(false);
    state->BeginContext(&vars);
    //selected group
    state->VarSetLocal("_this",value,true); 

#if USE_PRECOMPILATION
    if (GGroupMarkerClicked.GetType() == GameCode)
    {
      GameDataCode *code = static_cast<GameDataCode *>(GGroupMarkerClicked.GetData());
      issueCommand = !state->EvaluateBool(code->GetString(), code->GetCode(), GameState::EvalContext::_default, GWorld->GetMissionNamespace()); // mission namespace
    }
    else
#endif
    {
      // make sure string is not destructed while being evaluated
      RString code = GGroupMarkerClicked;
      issueCommand = !state->EvaluateMultipleBool(code, GameState::EvalContext::_default, GWorld->GetMissionNamespace()); // mission namespace
    }
    state->EndContext();
  }
} 

extern GameValue GGroupMarkerEnter;
extern GameValue GGroupMarkerLeave;
//perform scrip action when unit selection has changed
void CStaticMapMain::OnGroupMarkerEvent(AIGroup *group,GameValue GGroupMarkerEvent, int waypoint)
{
  bool issueCommand = true;

  DrawCoord coord;   
  if(waypoint<0)
  {
    AIUnit *unit = group->Leader();
    if(!unit) return;
    coord =  WorldToScreen(unit->Position(unit->GetRenderVisualState()));
  }
  else 
  {
    if(waypoint >= group->NWaypoints()) return;
    coord =  WorldToScreen(group->GetWaypointPosition(waypoint));
  }

  bool eventHandlerValid = !GGroupMarkerEvent.GetNil();
  if (eventHandlerValid)
  {
#if USE_PRECOMPILATION
    if (GGroupMarkerEvent.GetType() == GameCode)
    {
      GameDataCode *code = static_cast<GameDataCode *>(GGroupMarkerEvent.GetData());
      eventHandlerValid = code->IsCompiled() && code->GetCode().Size() > 0;
    }
    else
#endif
    {
      RString code = GGroupMarkerEvent;
      eventHandlerValid = code.GetLength() > 0;
    }
  }

  if (eventHandlerValid)
  {
    GameValue value = GWorld->GetGameState()->CreateGameValue(GameArray);
    GameArrayType &arguments = value;

    arguments.Add(false);
    arguments.Add(GameValueExt(group));
    arguments.Add((float)waypoint);
    arguments.Add(coord.x);
    arguments.Add(coord.y);

    arguments.Add((GInput.keyPressed[DIK_LSHIFT]||GInput.keyPressed[DIK_RSHIFT]));
    arguments.Add((GInput.keyPressed[DIK_LCONTROL]||GInput.keyPressed[DIK_RCONTROL]));
    arguments.Add((GInput.keyPressed[DIK_LALT]||GInput.keyPressed[DIK_RALT]));

    // Vector3 pos = ScreenToWorld(DrawCoord(x, y));
    GameState *state = GWorld->GetGameState();
    GameVarSpace vars(false);
    state->BeginContext(&vars);
    //selected group
    state->VarSetLocal("_this",value,true); 

#if USE_PRECOMPILATION
    if (GGroupMarkerEvent.GetType() == GameCode)
    {
      GameDataCode *code = static_cast<GameDataCode *>(GGroupMarkerEvent.GetData());
      issueCommand = !state->EvaluateBool(code->GetString(), code->GetCode(), GameState::EvalContext::_default, GWorld->GetMissionNamespace()); // mission namespace
    }
    else
#endif
    {
      // make sure string is not destructed while being evaluated
      RString code = GGroupMarkerEvent;
      issueCommand = !state->EvaluateMultipleBool(code, GameState::EvalContext::_default, GWorld->GetMissionNamespace()); // mission namespace
    }
    state->EndContext();
  }
}

void CStaticMapMain::OnLButtonDown(float x, float y)
{
  if(GWorld->UI()->IsGroupIconVisible2D())
  {
    int waypoint = -1;
    AIGroup *group = FindGroupMarker(x, y, waypoint);
    if(group) 
    {
      DrawCoord coord;
      if(waypoint<0)
      {
        AIUnit *unit = group->Leader();
        if(!unit) return;
        coord =  WorldToScreen(unit->Position(unit->GetRenderVisualState()));
      }
      else coord =  WorldToScreen(group->GetWaypointPosition(waypoint));
      OnGroupMarkerClick(group,0,coord.x,coord.y, waypoint);
      return;
    }
  }

  _mouseClickHandled = false;
  if (GWorld->UI() && GWorld->UI()->IsLMBHandled())
  {
    _mouseClickHandled = true;
    return;
  }

  _infoClick = FindSign(x, y);
  switch (_infoClick._type)
  {
  case signArcadeWaypoint:
    {
      AICenter *center = GetMyCenter();
      if (!center) break;
      Assert(_infoClick._indexGroup < center->NGroups());
      if (_infoClick._indexGroup < 0 || _infoClick._indexGroup >= center->NGroups()) break;
      AIGroup *group = center->GetGroup(_infoClick._indexGroup);
      if (!group) break;
      if (_infoClick._index<0 || _infoClick._index>=group->NWaypoints()) break;
      const WaypointInfo &wInfo = group->GetWaypoint(_infoClick._index);
      bool wpShow = wInfo.showWP == ShowAlways ||
        wInfo.showWP == ShowEasy && Glob.config.ShowCadetWP();
      if
        (
#if _ENABLE_CHEATS
        !_showUnits &&
#endif
        !wpShow
        ) break;

      _dragging = true;
      _lastPos = ScreenToWorld(DrawCoord(x, y));
    }
    return; // avoid IssueMove
#if _ENABLE_IDENTITIES
  case signCustomMark:
    {
      _dragging = true;
      _lastPos = ScreenToWorld(DrawCoord(x, y));
    }
#endif
  }
}

void CStaticMapMain::OnRButtonDown(float x, float y)
{
  if(GWorld->UI()->IsGroupIconVisible2D())
  {
    int waypoint = -1;
    AIGroup *group = FindGroupMarker(x, y,waypoint);
    if(group) 
    {
      DrawCoord coord;
      if(waypoint<0)
      {
        AIUnit *unit = group->Leader();
        if(!unit) return;
        coord =  WorldToScreen(unit->Position(unit->GetRenderVisualState()));
      }
      else coord =  WorldToScreen(group->GetWaypointPosition(waypoint));
      OnGroupMarkerClick(group,1,coord.x,coord.y,waypoint);
    }
  }
  CStaticMap::OnRButtonDown(x,y);
}


extern GameValue GMapOnSingleClick;
extern GameValue GMapOnSingleClickParams;

void CStaticMapMain::IssueCommand(float x, float y, bool alt, bool shift)
{
  bool issueCommand = true;

  bool eventHandlerValid = !GMapOnSingleClick.GetNil();
  if (eventHandlerValid)
  {
#if USE_PRECOMPILATION
    if (GMapOnSingleClick.GetType() == GameCode)
    {
      GameDataCode *code = static_cast<GameDataCode *>(GMapOnSingleClick.GetData());
      eventHandlerValid = code->IsCompiled() && code->GetCode().Size() > 0;
    }
    else
#endif
    {
      RString code = GMapOnSingleClick;
      eventHandlerValid = code.GetLength() > 0;
    }
  }

  if (eventHandlerValid)
  {
    Vector3 pos = ScreenToWorld(DrawCoord(x, y));
    GameState *state = GWorld->GetGameState();
    GameValue value = state->CreateGameValue(GameArray);
    GameArrayType &array = value;
    AIBrain *me = GetMyUnit();
    AIGroup *myGroup = me ? me->GetGroup() : NULL;
    if (myGroup && me == myGroup->Leader())
    {
      OLinkPermNOArray(AIUnit) list;
      GWorld->UI()->ListSelectingUnits(list);
      for (int i=0; i<list.Size(); i++)
      {
        AIUnit *unit = list[i];
        Assert(unit);
        array.Add(GameValueExt(unit->GetPerson()));
      }
    }

    GameValue posValue = state->CreateGameValue(GameArray);
    GameArrayType &posArray = posValue;
    posArray.Realloc(3);
    posArray.Add(pos[0]);
    posArray.Add(pos[2]);
    posArray.Add(pos[1]-GLandscape->SurfaceYAboveWater(pos[0],pos[2]));

    GameVarSpace vars(false);
    state->BeginContext(&vars);
    state->VarSetLocal("_units",value,true);
    state->VarSetLocal("_pos",posArray,true);
    state->VarSetLocal("_alt",alt,true);
    state->VarSetLocal("_shift",shift,true);
    state->VarSetLocal("_this", GMapOnSingleClickParams, true);
#if USE_PRECOMPILATION
    if (GMapOnSingleClick.GetType() == GameCode)
    {
      GameDataCode *code = static_cast<GameDataCode *>(GMapOnSingleClick.GetData());
      issueCommand = !state->EvaluateBool(code->GetString(), code->GetCode(), GameState::EvalContext::_default, GWorld->GetMissionNamespace()); // mission namespace
    }
    else
#endif
    {
      // make sure string is not destructed while being evaluated
      RString code = GMapOnSingleClick;
      issueCommand = !state->EvaluateMultipleBool(code, GameState::EvalContext::_default, GWorld->GetMissionNamespace()); // mission namespace
    }
    state->EndContext();
  }

  //if HC is used, unit's group control is not required
  if(GWorld->UI()->IsHcCommandVisible()) return;

  if (issueCommand)
  {
    if (alt)
    {
      IssueWatch(x, y);
    }
#if _ENABLE_IDENTITIES
    else if (shift)
    {
      AIBrain *me = GetMyUnit();
      Person *person = me ? me->GetPerson() : NULL;
      if (person)
      {
        Vector3 pos = ScreenToWorld(DrawCoord(x, y));
        pos[1] = GLandscape->SurfaceYAboveWater(pos[0],pos[2]);
        person->GetIdentity().SetCustomMark(pos);
      }
    }
#endif
    else
    {
      IssueMove(x, y);
    }
  }
}

void CStaticMapMain::OnLButtonClick(float x, float y)
{
  if(_mouseClickHandled)
  {
    _mouseClickHandled = false;
    return;
  }
  if (!_dragging)
  {
    bool alt = IsWatchRequired();
    bool shift = GInput.keys[DIK_LSHIFT]>0;
    IssueCommand(x, y, alt, shift);
  }
}

void CStaticMapMain::OnLButtonDblClick(float x, float y)
{
  if (GWorld->UI() && GWorld->UI()->IsLMBHandled()) return;

#ifndef _XBOX

# if _ENABLE_IDENTITIES
  // double click on the task icon -> find the task in the diary
  if (_infoClick._type == signTask)
  {
    RString link = Format("task:%d", _infoClick._index);
    ProcessDiaryLink(link, dynamic_cast<DisplayMap *>(_parent.GetLink()));
    return;
  }
# endif // _ENABLE_IDENTITIES

  if (dynamic_cast<DisplayMap *>(_parent.GetLink()))
    _parent->CreateChild(new DisplayInsertMarker(_parent, x, y, _x, _y, _w, _h, _parent->SimulationEnabled()));

#endif // !defined _XBOX
}

void CStaticMapMain::OnLButtonUp(float x, float y)
{
  if (_dragging)
  {
    _dragging = false;

    if (_infoClick._type == signArcadeWaypoint)
    {
      AICenter *center = GetMyCenter();
      if (!center) return;
      if (_infoClick._indexGroup < 0) return;
      Assert(_infoClick._indexGroup < center->NGroups());
      AIGroup *group = center->GetGroup(_infoClick._indexGroup);
      if (!group) return;
      if (!group->GetCurrent()) return;

      int &index = group->GetCurrent()->_fsm->Var(0);
      if (index == _infoClick._index)
      {
        AIGroupContext ctx(group);
        ctx._fsm = group->GetCurrent()->_fsm;
        ctx._task = const_cast<Mission *>(group->GetMission());
        ctx._fsm->SetState(1, &ctx);
        AIBrain *player = GWorld->FocusOn();
        if (player && player->GetGroup() == group)
          GWorld->UI()->ShowWaypointPosition();
      }
    }
  }
}


void CStaticMapMain::OnGroupIconOver(float x, float y)
{
  _hcTarget = NULL;
  if(GWorld->UI()->IsGroupIconVisible2D() &&  GWorld->UI()->IsGroupIconSelectable())
  {//if events are allowed
    int waypoint = -1;//if waypoint >=0, icon is a waypoint of a given group
    AIGroup *group = FindGroupMarker(x, y,waypoint);
    if(waypoint<0) _hcTarget = group;
    if(group && group ==_lastGroupOver)
    {//cursor remains on same icon as last frame
      OnGroupMarkerEvent(group,GGroupMarkerEnter,waypoint);
    }
    else if(group && group!= _lastGroupOver)
    {//cursor moved from one icon to another
      if(_lastGroupOver)
      {//icon which has been left
        OnGroupMarkerEvent(_lastGroupOver,GGroupMarkerLeave,waypoint);
      }
      //current icon
      OnGroupMarkerEvent(group,GGroupMarkerEnter,waypoint);
      _lastGroupOver = group;
    }
    else if(_lastGroupOver)
    {//icon has been left and cursor now points to nothing
      OnGroupMarkerEvent(_lastGroupOver,GGroupMarkerLeave,waypoint);
      _lastGroupOver = NULL;
    }
  }
}

void CStaticMapMain::OnMouseHold(float x, float y, bool active)
{
  if (_dragging)
  {
    if (_infoClick._type == signArcadeWaypoint)
    {
      AICenter *center = GetMyCenter();
      if (!center) return;
      if (_infoClick._indexGroup < 0) return;
      Assert(_infoClick._indexGroup < center->NGroups());
      AIGroup *group = center->GetGroup(_infoClick._indexGroup);
      if (!group) return;
      WaypointInfo &wInfo = group->GetWaypoint(_infoClick._index);

      Vector3 curPos = ScreenToWorld(DrawCoord(x, y));
      Vector3 offset = curPos - _lastPos;

      Vector3 pos = wInfo.position + offset;
      pos[1] = GLOB_LAND->RoadSurfaceYAboveWater(pos[0], pos[2]);
      wInfo.position = pos;

      _lastPos = curPos;
    }
#if _ENABLE_IDENTITIES
    else if (_infoClick._type == signCustomMark)
    {
      Person *person = _infoMove._unit ? _infoMove._unit->GetPerson() : NULL;
      if (person)
      {
        Vector3 pos;
        if (person->GetIdentity().GetCustomMark(pos))
        {
          Vector3 curPos = ScreenToWorld(DrawCoord(x, y));
          Vector3 offset = curPos - _lastPos;

          pos = pos + offset;
          pos[1] = GLOB_LAND->RoadSurfaceYAboveWater(pos[0], pos[2]);
          person->GetIdentity().SetCustomMark(pos);

          _lastPos = curPos;
        }
      }
    }
#endif
  }

  OnGroupIconOver(x,y);
  CStaticMap::OnMouseHold(x, y);
}

void CStaticMapMain::OnMouseMove(float x, float y, bool active)
{
  OnGroupIconOver(x,y);
  CStaticMap::OnMouseMove(x, y);
}

void CStaticMapMain::OnMouseZChanged(float dz)
{
  //now can zoom in commanding mode, when Ctrl is pressed
  // if (GWorld->UI() && GWorld->UI()->IsCommandingMode()) return;

  if
    (
    GWorld->UI()->GetMenu().GetLength()>0 &&
    !GInput.keyPressed[DIK_LCONTROL] && !GInput.keyPressed[DIK_RCONTROL]
  ) return;

  CStaticMap::OnMouseZChanged(dz);
}

#ifndef _XBOX

///////////////////////////////////////////////////////////////////////////////
// Warrant control

CWarrant::CWarrant(ControlsContainer *parent, int idc, ParamEntryPar cls)
: Control(parent, CT_USER, idc, cls)
{
  _font = GLOB_ENGINE->LoadFont(GetFontID(cls>>"font"));
  _size = cls >> "sizeEx";
  _color = GetPackedColor(cls >> "color");
}


void CWarrant::OnDraw(UIViewport *vp, float alpha)
{
  int w = GLOB_ENGINE->Width2D();
  int h = GLOB_ENGINE->Height2D();
  float size = _scale * _size;
  PackedColor color = ModAlpha(_color, alpha);

#if 0
  {
    // used for debugging purposes only
    float xx = CX(_x);
    float yy = CY(_y);
    float ww = CW(_w);
    float hh = CH(_h);
    MipInfo mip = GEngine->TextBank()->UseMipmap(NULL, 0, 0);
    PackedColor c = PackedColor(Color(0,0,1,0.3*alpha));
    GEngine->Draw2D(mip, c, xx, yy, ww, hh);
  }
#endif

  Person *soldier = GWorld->GetRealPlayer();
  AIBrain *agent = soldier ? soldier->Brain() : NULL;
  AIUnit *unit = agent ? agent->GetUnit() : NULL;
  AIStatsCampaign &stats = GStats._campaign;
  //AIUnitHeader &info = stats._playerInfo;

  float height = size;
  float top = _y;
  char buffer[256];

  // rank, name
  sprintf
    (
    buffer,
    LocalizeString(IDS_MAIN_RANK),
    (const char *)LocalizeString(IDS_PRIVATE)
    );
  GEngine->DrawText(Point2DFloat(_x, top), size, _font, color, buffer,0);
  top += height;

  // assignment
  sprintf
    (
    buffer,
    "Assignement: %s",
    "infantry"
    );
  GEngine->DrawText(Point2DFloat(_x, top), size, _font, color, buffer,0);
  top += height;

  // number in group
  if (unit)
  {
    sprintf
      (
      buffer,
      "Number in group: %d",
      unit->ID()
      );
    GEngine->DrawText(Point2DFloat(_x, top), size, _font, color, buffer,0);
    top += height;
  }

  top += height;

  // in combat
  sprintf
    (
    buffer,
    "In combat: %d minutes",
    toInt(stats._inCombat / 60.0)
    );
  GEngine->DrawText(Point2DFloat(_x, top), size, _font, color, buffer,0);
  top += height;

  top += height;

  // kills
  GEngine->DrawText(Point2DFloat(_x, top), size, _font, color, "Kills:",0);
  top += height;
  // draw table
  float w1 = 0.2 * _w;
  float ht = 0.2 * height;
  float h1 = height + 2.0 * ht;
  float x0 = _x, xx0 = x0 * w;
  float x1 = x0 + w1, xx1 = x1 * w;
  float x2 = x1 + w1, xx2 = x2 * w;
  float x3 = x2 + w1, xx3 = x3 * w;
  float x4 = x3 + w1, xx4 = x4 * w;
  float x5 = x4 + w1, xx5 = x5 * w;
  float y0 = top, yy0 = y0 * h;
  float y1 = y0 + h1, yy1 = y1 * h;
  float y2 = y1 + h1, yy2 = y2 * h;
  float y3 = y2 + h1, yy3 = y3 * h;
  float y4 = y3 + h1, yy4 = y4 * h;
  GEngine->DrawLine(Line2DPixel(xx1, yy0, xx5, yy0), color, color);
  GEngine->DrawLine(Line2DPixel(xx0, yy1, xx5, yy1), color, color);
  GEngine->DrawLine(Line2DPixel(xx0, yy2, xx5, yy2), color, color);
  GEngine->DrawLine(Line2DPixel(xx0, yy3, xx5, yy3), color, color);
  GEngine->DrawLine(Line2DPixel(xx0, yy4, xx5, yy4), color, color);
  GEngine->DrawLine(Line2DPixel(xx0, yy1, xx0, yy4), color, color);
  GEngine->DrawLine(Line2DPixel(xx1, yy0, xx1, yy4), color, color);
  GEngine->DrawLine(Line2DPixel(xx2, yy0, xx2, yy4), color, color);
  GEngine->DrawLine(Line2DPixel(xx3, yy0, xx3, yy4), color, color);
  GEngine->DrawLine(Line2DPixel(xx4, yy0, xx4, yy4), color, color);
  GEngine->DrawLine(Line2DPixel(xx5, yy0, xx5, yy4), color, color);
  float wt = GEngine->GetTextWidth(size, _font, "Enemies");
  GEngine->DrawText(Point2DFloat(x0 + 0.5 * (w1 - wt), y1 + ht), size, _font, color, "Enemies",0);
  wt = GEngine->GetTextWidth(size, _font, "Friends");
  GEngine->DrawText(Point2DFloat(x0 + 0.5 * (w1 - wt), y2 + ht), size, _font, color, "Friends",0);
  wt = GEngine->GetTextWidth(size, _font, "Civilians");
  GEngine->DrawText(Point2DFloat(x0 + 0.5 * (w1 - wt), y3 + ht), size, _font, color, "Civilians",0);
  wt = GEngine->GetTextWidth(size, _font, "Infantry");
  GEngine->DrawText(Point2DFloat(x1 + 0.5 * (w1 - wt), y0 + ht), size, _font, color, "Infantry",0);
  wt = GEngine->GetTextWidth(size, _font, "Mobile");
  GEngine->DrawText(Point2DFloat(x2 + 0.5 * (w1 - wt), y0 + ht), size, _font, color, "Mobile",0);
  wt = GEngine->GetTextWidth(size, _font, "Armored");
  GEngine->DrawText(Point2DFloat(x3 + 0.5 * (w1 - wt), y0 + ht), size, _font, color, "Armored",0);
  wt = GEngine->GetTextWidth(size, _font, "Air");
  GEngine->DrawText(Point2DFloat(x4 + 0.5 * (w1 - wt), y0 + ht), size, _font, color, "Air",0);
  int n = stats._kills[SKEnemyInfantry];
  if (n > 0)
  {
    wt = GEngine->GetTextWidthF(size, _font, "%d", n);
    GEngine->DrawTextF(Point2DFloat(x1 + 0.5 * (w1 - wt), y1 + ht), size, _font, color, 0, "%d", n);
  }
  n = stats._kills[SKEnemySoft];
  if (n > 0)
  {
    wt = GEngine->GetTextWidthF(size, _font, "%d", n);
    GEngine->DrawTextF(Point2DFloat(x2 + 0.5 * (w1 - wt), y1 + ht), size, _font, color, 0, "%d", n);
  }
  n = stats._kills[SKEnemyArmor];
  if (n > 0)
  {
    wt = GEngine->GetTextWidthF(size, _font, "%d", n);
    GEngine->DrawTextF(Point2DFloat(x3 + 0.5 * (w1 - wt), y1 + ht), size, _font, color, 0, "%d", n);
  }
  n = stats._kills[SKEnemyAir];
  if (n > 0)
  {
    wt = GEngine->GetTextWidthF(size, _font, "%d", n);
    GEngine->DrawTextF(Point2DFloat(x4 + 0.5 * (w1 - wt), y1 + ht), size, _font, color, 0, "%d", n);
  }
  n = stats._kills[SKFriendlyInfantry];
  if (n > 0)
  {
    wt = GEngine->GetTextWidthF(size, _font, "%d", n);
    GEngine->DrawTextF(Point2DFloat(x1 + 0.5 * (w1 - wt), y2 + ht), size, _font, color, 0, "%d", n);
  }
  n = stats._kills[SKFriendlySoft];
  if (n > 0)
  {
    wt = GEngine->GetTextWidthF(size, _font, "%d", n);
    GEngine->DrawTextF(Point2DFloat(x2 + 0.5 * (w1 - wt), y2 + ht), size, _font, color, 0, "%d", n);
  }
  n = stats._kills[SKFriendlyArmor];
  if (n > 0)
  {
    wt = GEngine->GetTextWidthF(size, _font, "%d", n);
    GEngine->DrawTextF(Point2DFloat(x3 + 0.5 * (w1 - wt), y2 + ht), size, _font, color, 0, "%d", n);
  }
  n = stats._kills[SKFriendlyAir];
  if (n > 0)
  {
    wt = GEngine->GetTextWidthF(size, _font, "%d", n);
    GEngine->DrawTextF(Point2DFloat(x4 + 0.5 * (w1 - wt), y2 + ht), size, _font, color, 0, "%d", n);
  }
  n = stats._kills[SKCivilianInfantry];
  if (n > 0)
  {
    wt = GEngine->GetTextWidthF(size, _font, "%d", n);
    GEngine->DrawTextF(Point2DFloat(x1 + 0.5 * (w1 - wt), y3 + ht), size, _font, color, 0, "%d", n);
  }
  n = stats._kills[SKCivilianSoft];
  if (n > 0)
  {
    wt = GEngine->GetTextWidthF(size, _font, "%d", n);
    GEngine->DrawTextF(Point2DFloat(x2 + 0.5 * (w1 - wt), y3 + ht), size, _font, color, 0, "%d", n);
  }
  n = stats._kills[SKCivilianArmor];
  if (n > 0)
  {
    wt = GEngine->GetTextWidthF(size, _font, "%d", n);
    GEngine->DrawTextF(Point2DFloat(x3 + 0.5 * (w1 - wt), y3 + ht), size, _font, color, 0, "%d", n);
  }
  n = stats._kills[SKCivilianAir];
  if (n > 0)
  {
    wt = GEngine->GetTextWidthF(size, _font, "%d", n);
    GEngine->DrawTextF(Point2DFloat(x4 + 0.5 * (w1 - wt), y3 + ht), size, _font, color, 0, "%d", n);
  }
}
#endif

///////////////////////////////////////////////////////////////////////////////
// Main Map display

WeatherState GetWeatherState(float overcast)
{
  if (overcast < 0.2) return WSClear;
  else if (overcast < 0.4) return WSCloudly;
  else if (overcast < 0.6) return WSOvercast;
  else if (overcast < 0.8) return WSRainy;
  else return WSStormy;
}

static const EnumName ObjectiveStatusNames[]=
{
  EnumName(OSActive, "ACTIVE"),
  EnumName(OSDone, "DONE"),
  EnumName(OSFailed, "FAILED"),
  EnumName(OSHidden, "HIDDEN"),
  EnumName()
};

template<>
const EnumName *GetEnumNames(ObjectiveStatus dummy)
{
  return ObjectiveStatusNames;
}

#include "../saveVersion.hpp"

LSError UnitWeaponsInfo::Serialize(ParamArchive &ar)
{
    CHECK(ar.SerializeRef("Unit", unit, 1))
    CHECK(ar.Serialize("name", name, 1))
    CHECK(ar.Serialize("weaponSlots", weaponSlots, 1))
    for (int i=0; i<WEAPON_SLOTS; i++)
    {
      char buffer[256];
      sprintf(buffer, "Weapon%d", i);
      CHECK(ar.Serialize(buffer, weapons[i], 1))
    }
    for (int i=0; i<MAGAZINE_SLOTS; i++)
    {
      char buffer[256];
      sprintf(buffer, "Magazine%d", i);
      CHECK(ar.Serialize(buffer, magazines[i], 1))
    }

    AutoArray<RString> magazinesRsc;
    AutoArray<RString> weaponsRsc;
    
    //save backpack type and it's content
    if(ar.IsSaving())
    {
      RString typmeName;
      if(backpack) typmeName = backpack->Type()->GetName();
      CHECK(ar.Serialize("typmeName", typmeName, 1))

      //bag's content
      if (backpack && backpack->GetSupply())
      {
        for (int i=0; i<backpack->GetSupply()->GetMagazineCargoSize(); i++)
        {
          const Magazine *mag =  backpack->GetSupply()->GetMagazineCargo(i);
          if(mag) magazinesRsc.Add(mag->_type->GetName());
        }
        for (int i=0; i<backpack->GetSupply()->GetWeaponCargoSize(); i++)
        {
          const WeaponType *weapon =  backpack->GetSupply()->GetWeaponCargo(i);
          if(weapon) weaponsRsc.Add(weapon->GetName());
        }
      }

      CHECK(ar.SerializeArray("weaponsRsc", weaponsRsc, 1))
      CHECK(ar.SerializeArray("magazineRsc", magazinesRsc, 1))
    }

    //load bag ant it's content
    if(ar.IsLoading())
    {
      //create bag
      RString typmeName;
      CHECK(ar.Serialize("typmeName", typmeName, 1))
        if(typmeName.GetLength() > 0)
        {
          EntityAI *bag = GWorld->NewVehicleWithID(typmeName);
          if (bag)
          {
            // Add into World
            GWorld->AddSlowVehicle(bag);
            if (GWorld->GetMode() == GModeNetware) GetNetworkManager().CreateVehicle(bag, VLTSlowVehicle, "", -1);

            backpack = bag;
          }
        }

      //create content
      if(ar.GetPass() == ParamArchive::PassSecond)
      {
        ar.FirstPass();
        CHECK(ar.SerializeArray("weaponsRsc", weaponsRsc, 1))
        CHECK(ar.SerializeArray("magazineRsc", magazinesRsc, 1))
        ar.SecondPass();

        if (backpack && backpack->GetSupply())
        {
          backpack->GetSupply()->ClearMagazineCargo(backpack);
          backpack->GetSupply()->ClearWeaponCargo(backpack);

          for (int i=0; i<magazinesRsc.Size(); i++)
          {
            Ref<MagazineType> mag = MagazineTypes.New(magazinesRsc[i]);
            if(mag) backpack->GetSupply()->AddMagazineCargoCount(backpack, mag, 1, false);
          }
          for (int i=0; i < weaponsRsc.Size(); i++)
          {
            Ref<WeaponType> weapon = WeaponTypes.New(weaponsRsc[i]);
            if(weapon) backpack->GetSupply()->AddWeaponCargo(backpack, weapon,1,false);
          }
        }
      }
    }

    return LSOK;
}

#define UPDATE_WEAPONS_INFO_MSG(MessageName, XX) \
  XX(MessageName, OLinkPerm<AIUnit>, unit, NDTRef, NetworkId, NCTNone, DEFVALUENULL, DOC_MSG("Updated unit"), TRANSF_REF) \
  XX(MessageName, AutoArray<RString>, weapons, NDTStringArray, AutoArray<RString>, NCTDefault, DEFVALUESTRINGARRAY, DOC_MSG("List of weapons"), TRANSF) \
  XX(MessageName, AutoArray<Magazine>, magazines, NDTObjectArray, REF_MSG_ARRAY(NetworkMessageMagazine), NCTNone, DEFVALUE_MSG(NMTMagazine), DOC_MSG("List of magazines"), TRANSF_OBJ_ARRAY) \
  XX(MessageName, OLinkPermO(EntityAI), backpack, NDTRef, NetworkId,  NCTNone, DEFVALUENULL, DOC_MSG("backpack"), TRANSF_REF_EX)

DECLARE_NET_INDICES(UpdateWeaponsInfo, UPDATE_WEAPONS_INFO_MSG)
DEFINE_NET_INDICES(UpdateWeaponsInfo, UPDATE_WEAPONS_INFO_MSG)

DEFINE_NETWORK_OBJECT_SIMPLE(UnitWeaponsInfo, UpdateWeaponsInfo)

NetworkMessageFormat &UnitWeaponsInfo::CreateFormat
(
 NetworkMessageClass cls,
 NetworkMessageFormat &format
 )
{
  UPDATE_WEAPONS_INFO_MSG(UpdateWeaponsInfo, MSG_FORMAT)
    return format;
}

TMError UnitWeaponsInfo::TransferMsg(NetworkMessageContext &ctx)
{
  PREPARE_TRANSFER(UpdateWeaponsInfo)

    TRANSF_REF_EX(unit, unit)
    if (ctx.IsSending())
    {
      AutoArray<RString> w;
      // primary and secondary weapons
      if (weapons[0]) w.Add(weapons[0]->GetName());
      else w.Add(RString());
      if (weapons[1] && weapons[1] != weapons[0]) w.Add(weapons[1]->GetName());
      else w.Add(RString());
      // binoculars
      if (weapons[2]) w.Add(weapons[2]->GetName());
      else w.Add(RString());
      if (weapons[3]) w.Add(weapons[3]->GetName());
      else w.Add(RString());
      // handgun
      if (weapons[4]) w.Add(weapons[4]->GetName());
      else w.Add(RString());
      // inventory items
      for (int i=FIRST_INVENTORY_SLOT; i<WEAPON_SLOTS; i++)
      {
        if (weapons[i]) w.Add(weapons[i]->GetName());
        else w.Add(RString());
      }
      TRANSF_EX(weapons, w)

        // magazines
        RefArray<Magazine> m;
      int slots = 0;
      for (int i=0; i<MAGAZINE_SLOTS; i++)
      {
        if (slots > 0)
        {
          slots--;
        }
        else
        {
          Magazine *mag = magazines[i];
          if (mag)
          {
            int type = mag->_type->_magazineType;
            bool handGun = (type & MaskSlotHandGunItem) != 0;
            slots = handGun ? GetHandGunItemSlotsCount(type) : GetItemSlotsCount(type);
            slots--;
          }
          m.Add(mag);
        }
      }
      TRANSF_OBJ_ARRAY_EX(magazines, m)

        TRANSF_REF_EX(backpack, backpack);
    }
    else
    {
      AutoArray<RString> w;
      TRANSF_EX(weapons, w)

        for (int i=0; i<WEAPON_SLOTS; i++) weapons[i] = NULL;
      // primary and secondary weapons
      if (w[0].GetLength() > 0)
      {
        WeaponType *weapon = WeaponTypes.New(w[0]);
        Assert(weapon);
        if (weapon->_weaponType & MaskSlotPrimary)
          weapons[0] = weapon;
        if (weapon->_weaponType & MaskSlotSecondary)
        {
          Assert(w[1].GetLength() == 0);
          weapons[1] = weapon;
        }
      }
      if (w[1].GetLength() > 0)
      {
        WeaponType *weapon = WeaponTypes.New(w[1]);
        Assert(weapon);
        Assert(weapon->_weaponType & MaskSlotSecondary);
        weapons[1] = weapon;
      }
      // binoculars
      if (w[2].GetLength() > 0)
      {
        WeaponType *weapon = WeaponTypes.New(w[2]);
        Assert(weapon);
        Assert(weapon->_weaponType & MaskSlotBinocular);
        weapons[2] = weapon;
      }
      if (w[3].GetLength() > 0)
      {
        WeaponType *weapon = WeaponTypes.New(w[3]);
        Assert(weapon);
        Assert(weapon->_weaponType & MaskSlotBinocular);
        weapons[1] = weapon;
      }
      // handgun
      if (w[4].GetLength() > 0)
      {
        WeaponType *weapon = WeaponTypes.New(w[4]);
        Assert(weapon);
        Assert(weapon->_weaponType & MaskSlotHandGun);
        weapons[4] = weapon;
      }
      // inventory items
      for (int i=FIRST_INVENTORY_SLOT; i<WEAPON_SLOTS; i++)
      {
        if (w[i].GetLength() > 0)
        {
          WeaponType *weapon = WeaponTypes.New(w[i]);
          Assert(weapon);
          Assert(weapon->_weaponType & MaskSlotInventory);
          weapons[i] = weapon;
        }
      }

      // magazines
      RefArray<Magazine> m;
      TRANSF_OBJ_ARRAY_EX(magazines, m)

        int slot = 0;
      for (int i=0; i<m.Size(); i++)
      {
        Magazine *mag = m[i];
        if (mag)
        {
          int type = mag->_type->_magazineType;
          bool handGun = (type & MaskSlotHandGunItem) != 0;
          int slots = handGun ? GetHandGunItemSlotsCount(type) : GetItemSlotsCount(type);
          for (int j=0; j<slots; j++)
          {
            if (slot < MAGAZINE_SLOTS)
              magazines[slot++] = mag;
            else
            {
              Fail("Too much magazines in UnitWeaponsInfo::TransferMsg");
            }
          }
        }
        else
        {
          if (slot < MAGAZINE_SLOTS)
            magazines[slot++] = NULL;
          else
          {
            Fail("Too much magazines in UnitWeaponsInfo::TransferMsg");
          }
        }
      }

      TRANSF_REF_EX(backpack,backpack);
    }
    return TMOK;
}

#define UPDATE_WEAPON_POOL_MSG(MessageName, XX) \
  XX(MessageName, AutoArray<RString>, weapons, NDTStringArray, AutoArray<RString>, NCTDefault, DEFVALUESTRINGARRAY, DOC_MSG("List of weapons"), TRANSF)

DECLARE_NET_INDICES(UpdateWeaponsPool, UPDATE_WEAPON_POOL_MSG)
DEFINE_NET_INDICES(UpdateWeaponsPool, UPDATE_WEAPON_POOL_MSG)

DEFINE_NETWORK_OBJECT_SIMPLE(WeaponsPool, UpdateWeaponsPool)

NetworkMessageFormat &WeaponsPool::CreateFormat
(
 NetworkMessageClass cls,
 NetworkMessageFormat &format
 )
{
  UPDATE_WEAPON_POOL_MSG(UpdateWeaponsPool, MSG_FORMAT)
    return format;
}

TMError WeaponsPool::TransferMsg(NetworkMessageContext &ctx)
{
  PREPARE_TRANSFER(UpdateWeaponsPool)

    if (ctx.IsSending())
    {
      int n = Size();
      AutoArray<RString> w;
      w.Realloc(n);
      w.Resize(n);
      for (int i=0; i<n; i++) w[i] = Get(i)->GetName();
      TRANSF_EX(weapons, w)
    }
    else
    {
      AutoArray<RString> w;
      TRANSF_EX(weapons, w)
        int n = w.Size();
      Realloc(n);
      Resize(n);
      for (int i=0; i<n; i++) Set(i) = WeaponTypes.New(w[i]);
    }
    return TMOK;
}

#define UPDATE_MAGAZINES_POOL_MSG(MessageName, XX) \
  XX(MessageName, RefArray<Magazine>, magazines, NDTObjectArray, REF_MSG_ARRAY(NetworkMessageMagazine), NCTNone, DEFVALUE_MSG(NMTMagazine), DOC_MSG("List of magazines"), TRANSF_OBJ_ARRAY)

DECLARE_NET_INDICES(UpdateMagazinesPool, UPDATE_MAGAZINES_POOL_MSG)
DEFINE_NET_INDICES(UpdateMagazinesPool, UPDATE_MAGAZINES_POOL_MSG)

DEFINE_NETWORK_OBJECT_SIMPLE(MagazinesPool, UpdateMagazinesPool)

NetworkMessageFormat &MagazinesPool::CreateFormat
(
 NetworkMessageClass cls,
 NetworkMessageFormat &format
 )
{
  UPDATE_MAGAZINES_POOL_MSG(UpdateMagazinesPool, MSG_FORMAT)
    return format;
}

TMError MagazinesPool::TransferMsg(NetworkMessageContext &ctx)
{
  PREPARE_TRANSFER(UpdateMagazinesPool)

    TRANSF_OBJ_ARRAY_EX(magazines, *(RefArray<Magazine> *)this)
    return TMOK;
}

#define UPDATE_BACKPACKS_POOL_MSG(MessageName, XX) \
  XX(MessageName, AutoArray<int>, creators, NDTIntArray, AutoArray<int>, NCTSmallUnsigned, DEFVALUEINTARRAY, DOC_MSG(""), TRANSF) \
  XX(MessageName, AutoArray<int>, ids, NDTIntArray, AutoArray<int>, NCTSmallUnsigned, DEFVALUEINTARRAY, DOC_MSG(""), TRANSF)

DECLARE_NET_INDICES(UpdateBackpacksPool, UPDATE_BACKPACKS_POOL_MSG)
DEFINE_NET_INDICES(UpdateBackpacksPool, UPDATE_BACKPACKS_POOL_MSG)

DEFINE_NETWORK_OBJECT_SIMPLE(BackpacksPool, UpdateBackpacksPool)

NetworkMessageFormat &BackpacksPool::CreateFormat
(
 NetworkMessageClass cls,
 NetworkMessageFormat &format
 )
{
  UPDATE_BACKPACKS_POOL_MSG(UpdateBackpacksPool, MSG_FORMAT)
    return format;
}

TMError BackpacksPool::TransferMsg(NetworkMessageContext &ctx)
{
  PREPARE_TRANSFER(UpdateBackpacksPool)

    if (ctx.IsSending())
    {
      AutoArray<RString> names;
      AutoArray<int> ids;
      AutoArray<int> creators;
      // primary and secondary weapons
      for (int i=0; i<Size(); i++)
      {
        if(!this->_data[i]._id.IsNull())
        {
          ids.Add(this->_data[i]._id.id);
          creators.Add(this->_data[i]._id.creator);
        }
      }

      TRANSF_EX(ids, ids)
        TRANSF_EX(creators, creators)
    }
    else
    {
      AutoArray<int> ids;
      AutoArray<int> creators;

      TRANSF_EX(ids, ids)
        TRANSF_EX(creators, creators)

        if(ids.Size() == creators.Size())
        {
          Clear();
          for (int i=0; i< ids.Size(); i++)
          {
            NetworkId idBag;
            idBag.creator = creators[i];
            idBag.id = ids[i];

            NetworkObject *objBag = GetNetworkManager().GetObject(idBag);
            if (!objBag) break;
            Assert(dynamic_cast<EntityAI *>(objBag));
            EntityAI *backpack = static_cast<EntityAI *>(objBag);

            Add(BackpacksPoolItem(backpack));
          }
        }
    }

    return TMOK;
}


LSError BackpacksPoolItem::Serialize(ParamArchive &ar)
{
  AutoArray<RString> magazinesRsc;
  AutoArray<RString> weaponsRsc;

  if (ar.IsLoading())
  {
    CHECK(ar.Serialize("typmeName", _typmeName, 1))

    EntityAI *bag = GWorld->NewVehicleWithID(_typmeName);
    if (bag)
    {
      // Add into World
      GWorld->AddSlowVehicle(bag);
      if (GWorld->GetMode() == GModeNetware)
        GetNetworkManager().CreateVehicle(bag, VLTSlowVehicle, "", -1);

      if(!bag->GetNetworkId().IsNull()) _id =bag->GetNetworkId();
      _backpack = bag;
    }

    if(ar.GetPass() == ParamArchive::PassSecond)
    {//load content
      ar.FirstPass();
      CHECK(ar.SerializeArray("weaponsRsc", weaponsRsc, 1))
      CHECK(ar.SerializeArray("magazineRsc", magazinesRsc, 1))
      ar.SecondPass();

      if (_backpack && _backpack->GetSupply())
      {
        _backpack->GetSupply()->ClearMagazineCargo(bag);
        _backpack->GetSupply()->ClearWeaponCargo(bag);

        for (int i=0; i<magazinesRsc.Size(); i++)
        {
          Ref<MagazineType> mag = MagazineTypes.New(magazinesRsc[i]);
          if(mag) _backpack->GetSupply()->AddMagazineCargoCount(_backpack, mag, 1, false);
        }
        for (int i=0; i < weaponsRsc.Size(); i++)
        {
          Ref<WeaponType> weapon = WeaponTypes.New(weaponsRsc[i]);
          if(weapon) _backpack->GetSupply()->AddWeaponCargo(_backpack, weapon,1,false);
        }
      }
    }
  }
  else
  {//save content
    CHECK(ar.Serialize("typmeName", _typmeName, 1))

      if (_backpack && _backpack->GetSupply())
      {
        for (int i=0; i<_backpack->GetSupply()->GetMagazineCargoSize(); i++)
        {
          const Magazine *mag =  _backpack->GetSupply()->GetMagazineCargo(i);
          if(mag) magazinesRsc.Add(mag->_type->GetName());
        }
        for (int i=0; i<_backpack->GetSupply()->GetWeaponCargoSize(); i++)
        {
          const WeaponType *weapon =  _backpack->GetSupply()->GetWeaponCargo(i);
          if(weapon) weaponsRsc.Add(weapon->GetName());
        }
      }

      CHECK(ar.SerializeArray("weaponsRsc", weaponsRsc, 1))
      CHECK(ar.SerializeArray("magazineRsc", magazinesRsc, 1))
  }

  return LSOK;
}

static bool WeaponUsesMagazine(const WeaponType *weapon, const MagazineType *type)
{
  for (int j=0; j<weapon->_muzzles.Size(); j++)
  {
    const MuzzleType *muzzle = weapon->_muzzles[j];
    for (int k=0; k<muzzle->_magazines.Size(); k++)
      if (muzzle->_magazines[k] == type) return true;
  }
  return false;
}

bool FixedItemsPool::CanDrop(const AIBrain *unit) const
{
  if (!unit) return false;

  Person *player = GWorld->GetRealPlayer();
  AIBrain *me = player ? player->Brain() : NULL;
  if (!me) return false;

  if (GWorld->GetMode() == GModeNetware)
  {
    if (me->IsGroupLeader())
      return unit == me || !unit->IsAnyPlayer();
    else
      return unit == me;
  }
  else
    return me && (me->IsGroupLeader() || (_canTakeWeapon && unit == me));
}

bool FixedItemsPool::CanReplace(const AIBrain *unit) const
{
  return CanDrop(unit);
}

bool FixedItemsPool::CanRearm(const AIBrain *unit) const
{
  return false;
}

RString FixedItemsPool::GetDropActionName(const AIBrain *unit, bool magazine) const
{
  return LocalizeString(IDS_XBOX_HINT_GEAR_DROP);
}

void FixedItemsPool::DropWeapon(UnitWeaponsInfo &info, WeaponType *weapon, bool backpack)
{
  if(!backpack && !info.HasWeapon(weapon)) return;

  ReturnWeapon(weapon);
  if(!backpack)
  {
    info.RemoveWeapon(weapon);
    RemoveUnusableMagazines(info);
  }
  else if(info.HasBackpack() && info.backpack->GetSupply())
  {
    info.backpack->GetSupply()->RemoveWeaponCargo(info.backpack, weapon);
    GetNetworkManager().RemoveWeaponCargo(info.backpack, weapon->GetName());
  }
  if (GWorld->GetMode() == GModeNetware)
    GetNetworkManager().SendMsg(&info);
}

void FixedItemsPool::DropMagazine(UnitWeaponsInfo &info, Magazine *magazine, bool backpack)
{
  MagazineType *type = magazine->_type;
  if (!type) return;

  if(!backpack && !info.HasMagazine(magazine)) return;

  ReturnMagazine(magazine);
  if(!backpack)
  {
    info.RemoveMagazine(magazine);
  }
  else if(info.HasBackpack() && info.backpack->GetSupply())
  {
    info.backpack->GetSupply()->RemoveMagazineCargo(info.backpack, magazine);
    GetNetworkManager().RemoveMagazineCargo(info.backpack, type->GetName(), magazine->GetAmmo());
  }
  if (GWorld->GetMode() == GModeNetware)
    GetNetworkManager().SendMsg(&info);
}

void FixedItemsPool::DropBackpack(UnitWeaponsInfo &info, RString name)
{
  if(name.GetLength()<=0) return;
  if(!info.HasBackpack()) return;

  Ref<EntityAI> backpack = info.RemoveBackpack(name);
  if(backpack) ReturnBackpack(backpack);

  if (GWorld->GetMode() == GModeNetware)
    GetNetworkManager().SendMsg(&info);
}

void FixedItemsPool::AddWeapon(UnitWeaponsInfo &info, WeaponType *weapon, bool backpack)
{
  if(!FindWeapon(weapon)) return;

  if (!info.unit) return;
  Person *person = info.unit->GetPerson();
  if (!person) return;

  if(!backpack)
  {
    // find weapons we need to drop
    AUTO_STATIC_ARRAY(Ref<WeaponType>, weapons, 32);
    if (info.weapons[0] && info.weapons[0] != info.weapons[1]) weapons.Add(info.weapons[0]);
    for (int i=1; i<WEAPON_SLOTS; i++)
      if (info.weapons[i]) weapons.Add(info.weapons[i]);

    AUTO_STATIC_ARRAY(Ref<const WeaponType>, conflict, 32);
    if (!person->CheckWeapon(weapon, conflict, weapons.Data(), weapons.Size())) return;

    for (int i=0; i<conflict.Size(); i++)
      DropWeapon(info, unconst_cast(conflict[i].GetRef()), backpack);

    // we are ready to take weapon
    if (weapon->_weaponType & MaskSlotPrimary)
      ReplaceWeapon(info, 0, weapon, backpack);
    else if (weapon->_weaponType & MaskSlotSecondary)
      ReplaceWeapon(info, 1, weapon, backpack);
    else if (weapon->_weaponType & MaskSlotHandGun)
      ReplaceWeapon(info, 4, weapon, backpack);
    else if (weapon->_weaponType & MaskSlotBinocular)
    {
      if (!info.weapons[2])
        ReplaceWeapon(info, 2, weapon, backpack);
      else
        ReplaceWeapon(info, 3, weapon, backpack);
    }
    else if (weapon->_weaponType & MaskSlotInventory)
    {
      bool done = false;
      for (int i=FIRST_INVENTORY_SLOT; i<WEAPON_SLOTS-1; i++)
      {
        // search for the free slot
        if (!info.weapons[i])
        {
          ReplaceWeapon(info, i, weapon, backpack);
          done = true;
          break;
        }
        // replace the last slot
        if (!done) ReplaceWeapon(info, WEAPON_SLOTS - 1, weapon, backpack);
      }
    }
  }
  else
  {
    AUTO_STATIC_ARRAY(Ref<const Magazine>, conflictMagazines, 16);
    AUTO_STATIC_ARRAY(Ref<WeaponType>, conflictWeapons, 16);
    if (!person->CheckBackpackSpace(weapon, conflictMagazines, conflictWeapons, info.backpack)) return;

    // remove magazines
    //remove weapons
    for (int i=0; i<conflictMagazines.Size(); i++)
      DropMagazine(info, unconst_cast(conflictMagazines[i].GetRef()), backpack);

    //remove weapons
    for (int i=0; i<conflictWeapons.Size(); i++)
      DropWeapon(info, unconst_cast(conflictWeapons[i].GetRef()), backpack);


    ReplaceWeapon(info, 0, weapon, backpack);
  }
}

void FixedItemsPool::AddMagazine(UnitWeaponsInfo &info, MagazineType *type, bool backpack)
{
  if(!FindMagazine(type)) return;

  if (!info.unit) return;
  Person *person = info.unit->GetPerson();
  if (!person) return;

  if(!backpack)
  {
    // find magazines we need to drop
    AUTO_STATIC_ARRAY(Ref<Magazine>, magazines, 16);
    for (int i=0; i<MAGAZINE_SLOTS;)
    {
      Magazine *magazine = info.magazines[i];
      if (magazine)
      {
        magazines.Add(magazine);
        if ((magazine->_type->_magazineType & MaskSlotItem) != 0)
        {
          i +=  GetItemSlotsCount(magazine->_type->_magazineType);
        }
        else if ((magazine->_type->_magazineType & MaskSlotHandGunItem) != 0)
        {
          i +=  GetHandGunItemSlotsCount(magazine->_type->_magazineType);
        }
        else
        {
          Fail("Bad magazine type");
          i++;
        }
      }
      else i++;
    }

    AUTO_STATIC_ARRAY(Ref<const Magazine>, conflict, 16);
    if (!person->CheckMagazine(type, conflict, magazines.Data(), magazines.Size())) return;

    for (int i=0; i<conflict.Size(); i++)
      DropMagazine(info, unconst_cast(conflict[i].GetRef()), backpack);

    if (type->_magazineType & MaskSlotItem)
    {
      // compact items
      int src = FIRST_PRIMARY_SLOT;
      int dst = FIRST_PRIMARY_SLOT;
      while (src < FIRST_HANDGUN_SLOT)
      {
        if (!info.magazines[src]) src++;
        else
        {
          if (dst != src)
          {
            info.magazines[dst] = info.magazines[src];
            info.magazines[src] = NULL;
          }
          dst++; src++;
        }
      }
      ReplaceMagazine(info, dst, type, backpack);
    }
    else if (type->_magazineType & MaskSlotHandGunItem)
    {
      // compact items
      int src = FIRST_HANDGUN_SLOT;
      int dst = FIRST_HANDGUN_SLOT;
      while (src < MAGAZINE_SLOTS)
      {
        if (!info.magazines[src]) src++;
        else
        {
          if (dst != src)
          {
            info.magazines[dst] = info.magazines[src];
            info.magazines[src] = NULL;
          }
          dst++; src++;
        }
      }
      ReplaceMagazine(info, dst, type, backpack);
    }
  }
  else
  {
    AUTO_STATIC_ARRAY(Ref<const Magazine>, conflictMagazines, 16);
    AUTO_STATIC_ARRAY(Ref<WeaponType>, conflictWeapons, 16);
    if (!person->CheckBackpackSpace(type, conflictMagazines, conflictWeapons, info.backpack)) return;

    // remove magazines
    // remove weapons
    for (int i=0; i<conflictMagazines.Size(); i++)
      DropMagazine(info, unconst_cast(conflictMagazines[i].GetRef()), backpack);

    //remove weapons
    for (int i=0; i<conflictWeapons.Size(); i++)
      DropWeapon(info, unconst_cast(conflictWeapons[i].GetRef()), backpack);

    ReplaceMagazine(info, 0, type, backpack);
  }
}

void FixedItemsPool::AddBackpack(UnitWeaponsInfo &info, RString name)
{
  if(name.GetLength()<=0) return;
  if (!info.unit) return;
  Person *person = info.unit->GetPerson();
  if (!person) return;

  if(info.HasBackpack())
    if(strcmp(name,info.backpack->GetType()->GetName())==0) return;
  // we are ready to take weapon
  ReplaceBackpack(info, name);
}

void FixedItemsPool::ReplaceWeapon(UnitWeaponsInfo &info, int slot, WeaponType *weapon, bool backpack)
{
  if (GWorld->GetMode() == GModeNetware)
  {
    if (weapon->_weaponType & MaskSlotBinocular)
    {
      // two binocular slots
      if (info.weaponSlots & MaskSlotBinocular)
      {
        GetNetworkManager().PoolAskWeapon(info.unit, weapon, slot, backpack);
      }
    }
    else if (weapon->_weaponType & MaskSlotInventory)
    {
      // multiple inventory slots
      if (info.weaponSlots & MaskSlotInventory)
      {
        GetNetworkManager().PoolAskWeapon(info.unit, weapon, slot, backpack);
      }
    }
    else if ((weapon->_weaponType & info.weaponSlots) == weapon->_weaponType)
    {
      // other types has single slots
      GetNetworkManager().PoolAskWeapon(info.unit, weapon, slot, backpack);
    }
    return;
  }

  if(!backpack)
  {

    // get old weapon
    WeaponType *oldWeapon = info.weapons[slot];
    if (oldWeapon && oldWeapon == weapon) return; // no change

    // remove old weapon
    if (oldWeapon)
    {
      AddWeapon(oldWeapon);
      info.RemoveWeapon(oldWeapon);
      RemoveUnusableMagazines(info);
    }

    // find weapon in pool
    if (weapon->_weaponType & MaskSlotBinocular)
    {
      if (info.weaponSlots & MaskSlotBinocular)
      {
        // add weapon
        Verify(RemoveWeapon(weapon));
        Assert(!info.weapons[slot]);
        info.weapons[slot] = weapon;
      }
    }
    else if (weapon->_weaponType & MaskSlotInventory)
    {
      if (info.weaponSlots & MaskSlotInventory)
      {
        // add weapon
        Verify(RemoveWeapon(weapon));
        Assert(!info.weapons[slot]);
        info.weapons[slot] = weapon;
      }
    }
    else if ((weapon->_weaponType & info.weaponSlots) == weapon->_weaponType)
    {
      if ((weapon->_weaponType & MaskSlotPrimary) && (weapon->_weaponType & MaskSlotSecondary))
      {
        // remove old weapons
        if (info.weapons[0])
        {
          AddWeapon(info.weapons[0]);
          info.RemoveWeapon(info.weapons[0]);
          RemoveUnusableMagazines(info);
        }
        if (info.weapons[1])
        {
          AddWeapon(info.weapons[1]);
          info.RemoveWeapon(info.weapons[1]);
          RemoveUnusableMagazines(info);
        }

        if(info.HasBackpack())
          DropBackpack(info,info.backpack ->GetType()->GetName());

        // add weapon
        Verify(RemoveWeapon(weapon));
        info.weapons[0] = weapon;
        info.weapons[1] = weapon;
        AddUsableMagazines(info, weapon, FIRST_PRIMARY_SLOT, FIRST_HANDGUN_SLOT);
      }
      else if (weapon->_weaponType & MaskSlotPrimary)
      {
        // remove old weapon
        if (info.weapons[0])
        {
          AddWeapon(info.weapons[0]);
          info.RemoveWeapon(info.weapons[0]);
          RemoveUnusableMagazines(info);
        }
        // add weapon
        Verify(RemoveWeapon(weapon));
        info.weapons[0] = weapon;
        AddUsableMagazines(info, weapon, FIRST_PRIMARY_SLOT, FIRST_SECONDARY_SLOT);
      }
      else if (weapon->_weaponType & MaskSlotSecondary)
      {
        // remove old weapon
        if (info.weapons[1])
        {
          AddWeapon(info.weapons[1]);
          info.RemoveWeapon(info.weapons[1]);
          RemoveUnusableMagazines(info);
        }

        if(info.HasBackpack())
          DropBackpack(info,info.backpack->GetType()->GetName());

        // add weapon
        Verify(RemoveWeapon(weapon));
        info.weapons[1] = weapon;
        AddUsableMagazines(info, weapon, FIRST_SECONDARY_SLOT, FIRST_HANDGUN_SLOT);
      }
      else if (weapon->_weaponType & MaskSlotHandGun)
      {
        // remove old weapon
        if (info.weapons[4])
        {
          AddWeapon(info.weapons[4]);
          info.RemoveWeapon(info.weapons[4]);
          RemoveUnusableMagazines(info);
        }
        // add weapon
        Verify(RemoveWeapon(weapon));
        info.weapons[4] = weapon;
        AddUsableHandGunMagazines(info, weapon, FIRST_HANDGUN_SLOT, MAGAZINE_SLOTS);
      }
    }
  }
  else if (info.backpack && info.backpack->GetSupply())
  {
    RemoveWeapon(weapon);
    info.backpack->GetSupply()->AddWeaponCargo(info.backpack,weapon,1,false);
  }
}

void FixedItemsPool::ReplaceMagazine(UnitWeaponsInfo &info, int slot, MagazineType *type, bool backpack)
{
  bool handGun = (type->_magazineType & MaskSlotHandGunItem) != 0;
  int maxItems = handGun ? GetHandGunItemSlotsCount(info.weaponSlots) : GetItemSlotsCount(info.weaponSlots);
  int slots = handGun ? GetHandGunItemSlotsCount(type->_magazineType) : GetItemSlotsCount(type->_magazineType);
  if (slots > maxItems) return;

  if (GWorld->GetMode() == GModeNetware)
  {
    GetNetworkManager().PoolAskMagazine(info.unit, type, slot, backpack);
    return;
  }

  if(!backpack)
  {
    Magazine *oldMagazine = info.magazines[slot];
    if (oldMagazine)
    {
      AddMagazine(oldMagazine);
      info.RemoveMagazine(oldMagazine);
    }

    Ref<Magazine> magazine = RemoveMagazine(type);
    if (!magazine) return;


    if (handGun) maxItems += FIRST_HANDGUN_SLOT;
    saturateMin(slot, maxItems - slots);
    for (int i=slot; i<slot+slots; i++)
    {
      if (info.magazines[i])
      {
        AddMagazine(info.magazines[i]);
        info.RemoveMagazine(info.magazines[i]);
      }
      info.magazines[i] = magazine;
    }
  }
  else if (info.backpack && info.backpack->GetSupply())
  {
    Ref<Magazine> magazine = RemoveMagazine(type);
    if (!magazine) return;

    info.backpack->GetSupply()->AddMagazineCargo(info.backpack,magazine,false);
  }
}

void FixedItemsPool::ReplaceBackpack(UnitWeaponsInfo &info, RString name)
{
  if (GWorld->GetMode() == GModeNetware)
  {
    GetNetworkManager().PoolAskBackpack(info.unit, name);
    return;
  }

  EntityAI *backpack = NULL;

  for (int i=0; i<_backpacksPool.Size(); i++)
  {
    if(strcmp(_backpacksPool[i].GetName(),name)==0)
      backpack = _backpacksPool[i]._backpack;
  }
  if(!backpack) return;

  if(info.HasBackpack())
  {
    if(backpack->GetType() ==  info.backpack->GetType()) return;
    DropBackpack(info, info.backpack->GetType()->GetName());
  }

  for(int i=0; i< WEAPON_SLOTS; i++)
  {
    if(info.weapons[i] && (info.weapons[i]->_weaponType  & MaskSlotSecondary) !=0)
    {
      AddWeapon(info.weapons[i]);
      info.RemoveWeapon(info.weapons[i]);
      RemoveUnusableMagazines(info);
    }
  }

  if(RemoveBackpack(backpack)) info.backpack = backpack;
}


void FixedItemsPool::Rearm(UnitWeaponsInfo &info)
{
  // not supported
}

void FixedItemsPool::ReturnMagazine(Magazine *magazine)
{
  if (GWorld->GetMode() == GModeNetware)
    GetNetworkManager().PoolReturnMagazine(magazine);
  else
    _magazinesPool.Add(magazine);
}

void FixedItemsPool::ReturnWeapon(WeaponType *weapon)
{
  if (GWorld->GetMode() == GModeNetware)
    GetNetworkManager().PoolReturnWeapon(weapon);
  else
    _weaponsPool.Add(weapon);
}

void FixedItemsPool::ReturnBackpack(EntityAI *backpack)
{
  if (GWorld->GetMode() == GModeNetware)
  {
    if(backpack && !backpack->GetNetworkId().IsNull())
      GetNetworkManager().PoolReturnBackpack(
      backpack->GetNetworkId().creator, backpack->GetNetworkId().id, backpack->GetType()->GetName());
  }
  else
    AddBackpack(backpack);
}


Magazine *FixedItemsPool::FindMagazine(const MagazineType *type) const
{
  for (int i=0; i<_magazinesPool.Size(); i++)
  {
    Magazine *magazine = _magazinesPool[i];
    if (!magazine) continue;
    if (magazine->_type == type) return magazine;
  }
  return NULL;
}

bool FixedItemsPool::FindWeapon(const WeaponType *type) const
{
  for (int i=0; i<_weaponsPool.Size(); i++)
  {
    WeaponType *weapon = _weaponsPool[i];
    if (!weapon) continue;
    if (weapon == type) return true;
  }
  return false;
}

Magazine *FixedItemsPool::FindMagazine(int mask) const
{
  for (int i=0; i<_magazinesPool.Size(); i++)
  {
    Magazine *magazine = _magazinesPool[i];
    MagazineType *type = magazine->_type;
    int slots = type->_magazineType;
    if ((slots & mask) != 0) return magazine;
  }
  return NULL;
}

Ref<Magazine> FixedItemsPool::RemoveMagazine(const MagazineType *type)
{
  // find the magazine of given type
  for (int i=0; i<_magazinesPool.Size(); i++)
  {
    if (_magazinesPool[i]->_type == type)
    {
      // remove and return it
      Ref<Magazine> magazine = _magazinesPool[i];
      _magazinesPool.Delete(i);
      return magazine;
    }
  }
  return NULL;
}

bool FixedItemsPool::RemoveBackpack(EntityAI *backpack)
{
  // remove given backpack
  for (int i=0; i<_backpacksPool.Size(); i++)
  {
    if (_backpacksPool[i]._backpack == backpack)
    {
      // remove and return it
      _backpacksPool.Delete(i);
      return true;
    }
  }
  return false;
}

bool FixedItemsPool::RemoveBackpack(NetworkId id)
{
  // remove given backpack
  for (int i=0; i<_backpacksPool.Size(); i++)
  {
    if (_backpacksPool[i]._id == id)
    {
      // remove and return it
      _backpacksPool.Delete(i);
      return true;
    }
  }
  return false;
}

void FixedItemsPool::Clear()
{
  _magazinesPool.Clear();
  _weaponsPool.Clear();
  _backpacksPool.Clear();
}

void FixedItemsPool::AddMagazine(Magazine *magazine)
{
  _magazinesPool.Add(magazine);
}

void FixedItemsPool::AddBackpack(EntityAI *backpack)
{
  if(backpack){
    _backpacksPool.Add(backpack);
  }
}

WeaponType *FixedItemsPool::FindWeapon(int mask, int maskExclude) const
{
  for (int i=0; i<_weaponsPool.Size(); i++)
  {
    WeaponType *weapon = _weaponsPool[i];
    int slots = weapon->_weaponType;
    if ((slots & mask) != 0 && (slots & maskExclude) == 0) return weapon;
  }
  return NULL;
}

bool FixedItemsPool::RemoveWeapon(WeaponType *weapon)
{
  // find the given weapon
  for (int i=0; i<_weaponsPool.Size(); i++)
  {
    if (_weaponsPool[i] == weapon)
    {
      // remove it
      _weaponsPool.Delete(i);
      return true;
    }
  }
  return false;
}

void FixedItemsPool::AddWeapon(WeaponType *weapon)
{
  _weaponsPool.Add(weapon);
}

void FixedItemsPool::SummarizeMagazines(MagazinesSummary &result, int mask, int maskExclude) const
{
  for (int i=0; i<_magazinesPool.Size(); i++)
  {
    Magazine *magazine = _magazinesPool[i];
    const MagazineType *type = magazine->_type;
    int slots = type->_magazineType;
    if ((slots & mask) == 0) continue;
    if ((slots & maskExclude) != 0) continue;

    bool found = false;
    for (int j=0; j<result.Size(); j++)
    {
      if (result[j].type == type)
      {
        result[j].count++;
        found = true;
        break;
      }
    }
    if (!found)
    {
      int j = result.Add();
      result[j].type = type;
      result[j].count = 1;
    }
  }
}

void FixedItemsPool::SummarizeWeapons(WeaponsSummary &result, int mask, int maskExclude) const
{
  for (int i=0; i<_weaponsPool.Size(); i++)
  {
    const WeaponType *type = _weaponsPool[i];
    int slots = type->_weaponType;
    if ((slots & mask) == 0) continue;
    if ((slots & maskExclude) != 0) continue;

    bool found = false;
    for (int j=0; j<result.Size(); j++)
    {
      if (result[j].type == type)
      {
        result[j].count++;
        found = true;
        break;
      }
    }
    if (!found)
    {
      int j = result.Add();
      result[j].type = type;
      result[j].count = 1;
    }
  }
}

RefArray<EntityAI> FixedItemsPool::SummarizeBackpacks() const
{
  RefArray<EntityAI> bags;
  for (int i= 0; i< _backpacksPool.Size();i++)
  {
    if(_backpacksPool[i]._backpack) bags.Add(_backpacksPool[i]._backpack);
  }
  return bags;
}

bool FixedItemsPool::Import(ParamEntryVal superclass)
{
  _weaponsPool.Clear();
  _magazinesPool.Clear();
  _backpacksPool.Clear();
  ConstParamEntryPtr cls = superclass.FindEntry("Weapons");
  if (cls)
  {
    int mask = MaskSlotPrimary | MaskSlotSecondary | MaskSlotBinocular | MaskSlotHandGun | MaskSlotInventory;
    for (int i=0; i<cls->GetEntryCount(); i++)
    {
      ParamEntryVal entry = cls->GetEntry(i);
      Ref<WeaponType> weapon = WeaponTypes.New(entry.GetName());
      if (!weapon) continue;
      if (weapon->_scope < 2) continue;
      if ((weapon->_weaponType & mask) == 0) continue;
      int count = entry >> "count";
      for (int j=0; j<count; j++) _weaponsPool.Add(weapon);
    }
  }
  cls = superclass.FindEntry("Magazines");
  if (cls)
  {
    int mask = MaskSlotItem | MaskSlotHandGunItem;
    for (int i=0; i<cls->GetEntryCount(); i++)
    {
      ParamEntryVal entry = cls->GetEntry(i);
      Ref<MagazineType> type = MagazineTypes.New(entry.GetName());
      if (!type) continue;
      if (type->_scope < 2) continue;
      if ((type->_magazineType & mask) == 0) continue;
      int count = entry >> "count";
      for (int j=0; j<count; j++)
      {
        Ref<Magazine> magazine = new Magazine(type);
        magazine->SetAmmo(type->_maxAmmo);
        magazine->_reloadMagazine = 0;
        magazine->_reload = 1;
        magazine->_reloadDuration = 0.8 + 0.2 * GRandGen.RandomValue();
        _magazinesPool.Add(magazine);
      }
    }
  }
  cls = superclass.FindEntry("Backpacks");
  if (cls)
  {
    for (int i=0; i<cls->GetEntryCount(); i++)
    {
      ParamEntryVal entry = cls->GetEntry(i);
      RString bagName = entry.GetName();
      int count = entry >> "count";
      if( bagName.GetLength()>0)
      {
        for (int j=0; j<count; j++)
        {
          EntityAI *backpack = GWorld->NewVehicleWithID(bagName);
          if (!backpack) continue;

          // Add into World
          GWorld->AddSlowVehicle(backpack);
          if (GWorld->GetMode() == GModeNetware)
            GetNetworkManager().CreateVehicle(backpack, VLTSlowVehicle, "", -1);

          AddBackpack(backpack);
        }
      }

    }
  }
  return true;
}

void FixedItemsPool::RemoveUnusableMagazines(UnitWeaponsInfo &info)
{
  for (int i=0; i<MAGAZINE_SLOTS;)
  {
    Magazine *magazine = info.magazines[i];
    if (!magazine) {i++; continue;}
    MagazineType *type = magazine->_type;
    if (!type) {i++; continue;}
    if ((type->_magazineType & MaskSlotItem) != 0)
    {
      int nItems = GetItemSlotsCount(type->_magazineType);
      if (!info.IsMagazineUsable(type))
      {
        ReturnMagazine(magazine);
        info.RemoveMagazine(magazine);
      }
      i += nItems;
    }
    else if ((type->_magazineType & MaskSlotHandGunItem) != 0)
    {
      int nItems = GetHandGunItemSlotsCount(type->_magazineType);
      if (!info.IsMagazineUsable(type))
      {
        ReturnMagazine(magazine);
        info.RemoveMagazine(magazine);
      }
      i += nItems;
    }
  }
}

/*!
\patch 5143 Date 3/22/2007 by Jirka
- Fixed: Gear screen in briefing - magazines was multiplied sometimes
*/

void FixedItemsPool::AddUsableMagazines(UnitWeaponsInfo &info, WeaponType *weapon, int from, int to)
{
  // new (Xbox) implementation
  Assert(weapon);
  int nSlots = GetItemSlotsCount(info.weaponSlots);
  saturateMin(to, nSlots);
  int wantedSlots = to - from;
  if (wantedSlots <= 0) return;

  // 1. Check if some magazine is in pool
  if (weapon->_muzzles.Size() == 0) return;
  const MuzzleType *muzzle = weapon->_muzzles[0];
  if (muzzle->_magazines.Size() == 0) return;
  const MagazineType *type = muzzle->_magazines[0];
  if (!FindMagazine(type)) return;
  int minSlots = GetItemSlotsCount(type->_magazineType);
  if (minSlots >= nSlots) return;

  if (minSlots == 0)
  {  //try handgun ammo (or grenades for mk13)
    if(GetHandGunItemSlotsCount(type->_magazineType) >0)
      AddUsableHandGunMagazines(info, weapon,FIRST_HANDGUN_SLOT, MAGAZINE_SLOTS);
    return;
  }

  // 2. Calculation of free-able slots
  const WeaponType *otherWeapon = NULL;
  bool isPrimaryWeapon = info.weapons[0] == weapon;
  if (info.weapons[1] == weapon)
    otherWeapon = info.weapons[0];
  else
    otherWeapon = info.weapons[1];

  AUTO_STATIC_ARRAY(Ref<Magazine>, otherWeaponMagazines, 32);
  AUTO_STATIC_ARRAY(Ref<Magazine>, otherMagazines, 32);
  AUTO_STATIC_ARRAY(Ref<Magazine>, addedMagazines, 32);

  // Invariant: each magazine need to be in each moment on a single place:
  // a) in info
  // b) in pool
  // c) in otherWeaponMagazines, otherMagazines or addedMagazines

  for (int i=0; i<nSlots;)
  {
    Magazine *magazine = info.magazines[i];
    if (!magazine || !magazine->_type)
    {
      i++;
      continue;
    }

    // move from info to otherWeaponMagazines or otherMagazines
    if (otherWeapon && WeaponUsesMagazine(otherWeapon, magazine->_type))
      otherWeaponMagazines.Add(magazine);
    else
      otherMagazines.Add(magazine);
    info.RemoveMagazine(magazine);

    i += GetItemSlotsCount(magazine->_type->_magazineType);
  }

  int n = nSlots;
  if (otherWeaponMagazines.Size() > 0)
  {
    // check if free space can be obtain without remove last magazine of otherWeapon
    const Magazine *magazine = otherWeaponMagazines[0];
    int slots = GetItemSlotsCount(magazine->_type->_magazineType);
    if (nSlots - slots < minSlots) return;
    n -= slots;

    for (int i=1; i<otherWeaponMagazines.Size(); i++)
    {
      const Magazine *magazine = otherWeaponMagazines[i];
      n -= GetItemSlotsCount(magazine->_type->_magazineType);
    }
  }

  // 3. Create space for wanted magazines
  if (minSlots <= n)
  {
    saturateMin(wantedSlots, n);
    for (int i=0; i<otherMagazines.Size(); i++)
    {
      const Magazine *magazine = otherMagazines[i];
      n -= GetItemSlotsCount(magazine->_type->_magazineType);
    }
    while (wantedSlots - minSlots >= 0)
    {
      // magazine to add - move from pool to addedMagazines
      Ref<Magazine> magazine = RemoveMagazine(type);
      if (!magazine) break;
      // free some slots
      while (minSlots > n)
      {
        int i = otherMagazines.Size() - 1;
        Assert(i >= 0);
        const Magazine *magazine = otherMagazines[i];
        n += GetItemSlotsCount(magazine->_type->_magazineType);
        // move from otherMagazines to pool
        AddMagazine(otherMagazines[i]);
        otherMagazines.Delete(i);
      }
      // add magazine
      addedMagazines.Add(magazine);
      wantedSlots -= minSlots;
      n -= minSlots;
    }
  }
  else
  {
    // move all from otherMagazines to pool
    for (int i=0; i<otherMagazines.Size(); i++)
      AddMagazine(otherMagazines[i]);
    otherMagazines.Clear();

    while (minSlots > n)
    {
      int i = otherWeaponMagazines.Size() - 1;
      Assert(i > 0);
      const Magazine *magazine = otherWeaponMagazines[i];
      n += GetItemSlotsCount(magazine->_type->_magazineType);
      // move from otherWeaponMagazines to pool
      AddMagazine(otherWeaponMagazines[i]);
      otherWeaponMagazines.Delete(i);
    }

    // move from pool to addedMagazines
    Ref<Magazine> magazine = RemoveMagazine(type);
    DoAssert(magazine);
    addedMagazines.Add(magazine);
  }

  // 4. Check
  for (int i=0; i<nSlots; i++)
  {
    Assert(info.magazines[i] == NULL);
  }

  // 5. Add magazines
  int index = 0;
  if (isPrimaryWeapon)
  {
    // move from addedMagazines to info
    for (int i=0; i<addedMagazines.Size(); i++)
    {
      Magazine *magazine = addedMagazines[i];
      int slots = GetItemSlotsCount(magazine->_type->_magazineType);
      for (int j=0; j<slots; j++) info.magazines[index++] = magazine;
    }
    // move from otherWeaponMagazines to info
    for (int i=0; i<otherWeaponMagazines.Size(); i++)
    {
      Magazine *magazine = otherWeaponMagazines[i];
      int slots = GetItemSlotsCount(magazine->_type->_magazineType);
      for (int j=0; j<slots; j++) info.magazines[index++] = magazine;
    }
  }
  else
  {
    // move from otherWeaponMagazines to info
    for (int i=0; i<otherWeaponMagazines.Size(); i++)
    {
      Magazine *magazine = otherWeaponMagazines[i];
      int slots = GetItemSlotsCount(magazine->_type->_magazineType);
      for (int j=0; j<slots; j++) info.magazines[index++] = magazine;
    }
    // move from addedMagazines to info
    for (int i=0; i<addedMagazines.Size(); i++)
    {
      Magazine *magazine = addedMagazines[i];
      int slots = GetItemSlotsCount(magazine->_type->_magazineType);
      for (int j=0; j<slots; j++) info.magazines[index++] = magazine;
    }
  }
  // move from otherMagazines to info
  for (int i=0; i<otherMagazines.Size(); i++)
  {
    Magazine *magazine = otherMagazines[i];
    int slots = GetItemSlotsCount(magazine->_type->_magazineType);
    for (int j=0; j<slots; j++) info.magazines[index++] = magazine;
  }
}

void FixedItemsPool::AddUsableHandGunMagazines(UnitWeaponsInfo &info, WeaponType *weapon, int from, int to)
{
  saturateMin(to, FIRST_HANDGUN_SLOT + GetHandGunItemSlotsCount(info.weaponSlots));
  if (from >= to) return;

  if (weapon->_muzzles.Size() == 0) return;
  const MuzzleType *muzzle = weapon->_muzzles[0];

  if (muzzle->_magazines.Size() == 0) return;
  const MagazineType *type = muzzle->_magazines[0];

  int nSlots = GetHandGunItemSlotsCount(type->_magazineType);
  if (nSlots == 0) return;

  for (int i=from; i<=to-nSlots; i++)
  {
    if(info.magazines[i]) 
    {
      ReturnMagazine(info.magazines[i]);
      info.RemoveMagazine(info.magazines[i]);
    }
  }

  for (int i=from; i<=to-nSlots;)
  {
    bool free = true;
    for (int j=0; j<nSlots; j++)
      if (info.magazines[i + j])
      {
        free = false;
        break;
      }
      if (free)
      {
        // find magazine
        Ref<Magazine> magazine = RemoveMagazine(type);
        if (!magazine) return;
        for (int j=0; j<nSlots; j++)
          info.magazines[i++] = magazine;
      }
      else i++;
  }
}

void FixedItemsPool::OnWeaponReplaced(UnitWeaponsInfo &info, int slot, WeaponType *weapon, bool useBackpack)
{
  if(!useBackpack)
  {
    bool primary = false, secondary = false, handgun = false;

    // check slots
    if (weapon->_weaponType & MaskSlotBinocular)
    {
      if (info.weapons[slot])
      {
        // remove old weapon
        GetNetworkManager().PoolReturnWeapon(info.weapons[slot]);
        info.RemoveWeapon(info.weapons[slot]);
      }
      // add weapon
      info.weapons[slot] = weapon;
    }
    if (weapon->_weaponType & MaskSlotInventory)
    {
      if (info.weapons[slot])
      {
        // remove old weapon
        GetNetworkManager().PoolReturnWeapon(info.weapons[slot]);
        info.RemoveWeapon(info.weapons[slot]);
      }
      // add weapon
      info.weapons[slot] = weapon;
    }
    if (weapon->_weaponType & MaskSlotPrimary)
    {
      if (info.weapons[0])
      {
        // remove old weapon
        GetNetworkManager().PoolReturnWeapon(info.weapons[0]);
        info.RemoveWeapon(info.weapons[0]);
        RemoveUnusableMagazines(info);
      }
      info.weapons[0] = weapon;
      primary = true;
    }
    if (weapon->_weaponType & MaskSlotSecondary)
    {
      if (info.weapons[1])
      {
        GetNetworkManager().PoolReturnWeapon(info.weapons[1]);
        info.RemoveWeapon(info.weapons[1]);
        RemoveUnusableMagazines(info);
      }
      info.weapons[1] = weapon;
      secondary = true;

      if(info.HasBackpack())
        DropBackpack(info,info.backpack->GetType()->GetName());

    }
    if (weapon->_weaponType & MaskSlotHandGun)
    {
      if (info.weapons[4])
      {
        GetNetworkManager().PoolReturnWeapon(info.weapons[4]);
        info.RemoveWeapon(info.weapons[4]);
        RemoveUnusableMagazines(info);
      }
      info.weapons[4] = weapon;
      handgun = true;
    }

    // ask for usable magazines
    if (primary || secondary)
    {
      int totalSlots = GetItemSlotsCount(info.weaponSlots);
      int totalSlotsHandGun = GetHandGunItemSlotsCount(info.weaponSlots);
      // select magazine type
      if (weapon->_muzzles.Size() > 0)
      {
        const MuzzleType *muzzle = weapon->_muzzles[0];
        if (muzzle->_magazines.Size() > 0)
        {
          const MagazineType *type = muzzle->_magazines[0];
          int slots = GetItemSlotsCount(type->_magazineType);
          if(slots > 0)
          {
            if (slots <= totalSlots)
            {
              AUTO_STATIC_ARRAY(Ref<Magazine>, weaponMagazines, 32);
              AUTO_STATIC_ARRAY(Ref<Magazine>, otherMagazines, 32);

              const WeaponType *otherWeapon = NULL;
              if (info.weapons[0] != weapon)
                otherWeapon = info.weapons[0];
              else if (info.weapons[1] != weapon)
                otherWeapon = info.weapons[1];

              int freeSlots = totalSlots;
              for (int i=0; i<totalSlots;)
              {
                Magazine *magazine = info.magazines[i];
                if (!magazine || !magazine->_type)
                {
                  i++;
                  continue;
                }
                if (otherWeapon && WeaponUsesMagazine(otherWeapon, magazine->_type))
                  weaponMagazines.Add(magazine);
                else
                  otherMagazines.Add(magazine);
                info.RemoveMagazine(magazine);
                int itemSlots = GetItemSlotsCount(magazine->_type->_magazineType);
                i += itemSlots;
                freeSlots -= itemSlots;
              }

              int wantedSlots = 0;
              if (primary) wantedSlots += FIRST_SECONDARY_SLOT - FIRST_PRIMARY_SLOT;
              if (secondary) wantedSlots += FIRST_HANDGUN_SLOT - FIRST_SECONDARY_SLOT;
              int n = wantedSlots / slots;
              saturateMax(n, 1);
              wantedSlots = n * slots;

              if (freeSlots < wantedSlots)
              {
                for (int i=otherMagazines.Size()-1; i>=0; i--)
                {
                  Magazine *magazine = otherMagazines[i];
                  int slots = GetItemSlotsCount(magazine->_type->_magazineType);
                  if (freeSlots + slots > wantedSlots) break;
                  GetNetworkManager().PoolReturnMagazine(magazine);
                  otherMagazines.Delete(i);
                  freeSlots += slots;
                }
              }
              if (freeSlots < slots)
              {
                for (int i=weaponMagazines.Size()-1; i>=0; i--)
                {
                  Magazine *magazine = weaponMagazines[i];
                  int s = GetItemSlotsCount(magazine->_type->_magazineType);
                  if (freeSlots + s > slots) break;
                  GetNetworkManager().PoolReturnMagazine(magazine);
                  weaponMagazines.Delete(i);
                  freeSlots += s;
                }
              }
              n = min(freeSlots, wantedSlots) / slots;
              int slot = 0;
              if (primary)
              {
                for (int i=0; i<n; i++)
                {
                  GetNetworkManager().PoolAskMagazine(info.unit, type, slot, useBackpack);
                  slot += slots;
                }
                for (int i=0; i<weaponMagazines.Size(); i++)
                {
                  Magazine *magazine = weaponMagazines[i];
                  int slots = GetItemSlotsCount(magazine->_type->_magazineType);
                  for (int j=0; j<slots; j++) info.magazines[slot++] = magazine;
                }
              }
              else
              {
                for (int i=0; i<weaponMagazines.Size(); i++)
                {
                  Magazine *magazine = weaponMagazines[i];
                  int slots = GetItemSlotsCount(magazine->_type->_magazineType);
                  for (int j=0; j<slots; j++) info.magazines[slot++] = magazine;
                }
                for (int i=0; i<n; i++)
                {
                  GetNetworkManager().PoolAskMagazine(info.unit, type, slot, useBackpack);
                  slot += slots;
                }
              }
              for (int i=0; i<otherMagazines.Size(); i++)
              {
                Magazine *magazine = otherMagazines[i];
                int slots = GetItemSlotsCount(magazine->_type->_magazineType);
                for (int j=0; j<slots; j++) info.magazines[slot++] = magazine;
              }
            }
          }
          else
          {
            int slotsHand = GetHandGunItemSlotsCount(type->_magazineType);
            if(slotsHand > 0)
            {
              // count of magazines
              int n = totalSlotsHandGun / slotsHand; //fill just half of slots
              if (n > 0)
              {
                // we can add up to n magazines
                int slot = FIRST_HANDGUN_SLOT;
                for (int i=0; i<n; i++)
                {
                  GetNetworkManager().PoolAskMagazine(info.unit, type, slot, useBackpack);
                  slot += slotsHand;
                }
              }
            }
          }
        }
      }
    }
    if (handgun)
    {
      int totalSlots = GetHandGunItemSlotsCount(info.weaponSlots);
      // remove all magazines
      for (int i=0; i<totalSlots;)
      {
        Magazine *mag = info.magazines[FIRST_HANDGUN_SLOT + i];
        if (mag)
        {
          int slots = GetHandGunItemSlotsCount(mag->_type->_magazineType);
          GetNetworkManager().PoolReturnMagazine(mag);
          info.RemoveMagazine(mag);
          i += slots;
        }
        else i++;
      }
      // select magazine type
      if (weapon->_muzzles.Size() > 0)
      {
        const MuzzleType *muzzle = weapon->_muzzles[0];
        if (muzzle->_magazines.Size() > 0)
        {
          const MagazineType *type = muzzle->_magazines[0];
          int slots = GetHandGunItemSlotsCount(type->_magazineType);

          // count of magazines
          int n = totalSlots / slots;
          if (n > 0)
          {
            // we can add up to n magazines
            int slot = FIRST_HANDGUN_SLOT;
            for (int i=0; i<n; i++)
            {
              GetNetworkManager().PoolAskMagazine(info.unit, type, slot, useBackpack);
              slot += slots;
            }
          }
        }
      }
    }
  }
  else
  {
    if(!info.backpack || !info.backpack->GetSupply()) return;

    info.backpack->GetSupply()->AddWeaponCargo(info.backpack, weapon,1, false);
  }
  GetNetworkManager().SendMsg(&info);
}

void FixedItemsPool::OnMagazineReplaced(UnitWeaponsInfo &info, int slot, Magazine *magazine, bool useBackpack)
{
  // select slots
  MagazineType *type = magazine->_type;
  if(!useBackpack)
  {

    bool handGun = (type->_magazineType & MaskSlotHandGunItem) != 0;
    int maxItems = handGun ? GetHandGunItemSlotsCount(info.weaponSlots) : GetItemSlotsCount(info.weaponSlots);
    int slots = handGun ? GetHandGunItemSlotsCount(type->_magazineType) : GetItemSlotsCount(type->_magazineType);
    if (handGun) maxItems += FIRST_HANDGUN_SLOT;
    saturateMin(slot, maxItems - slots);

    // check slots
    for (int i=slot; i<slot+slots; i++)
    {
      if (info.magazines[i])
      {
        GetNetworkManager().PoolReturnMagazine(info.magazines[i]);
        info.RemoveMagazine(info.magazines[i]);
      }
      info.magazines[i] = magazine;
    }
  }
  else
  {
    if(!info.backpack || !info.backpack->GetSupply()) return;
    info.backpack->GetSupply()->AddMagazineCargo(info.backpack,magazine,false);
  }


  GetNetworkManager().SendMsg(&info);
}

void FixedItemsPool::OnBackpackReplaced(UnitWeaponsInfo &info, EntityAI *backpack)
{
  if (info.HasBackpack())
  {
    // remove old weapon
    if(!info.backpack->GetNetworkId().IsNull())
      GetNetworkManager().PoolReturnBackpack( info.backpack->GetNetworkId().creator,  info.backpack->GetNetworkId().id,  info.backpack->GetType()->GetName());
  }

  for (int i=0; i< WEAPON_SLOTS; i++)
  {
    if (info.weapons[i] && info.weapons[i]->_weaponType & MaskSlotSecondary)
    {
      // remove old weapon
      GetNetworkManager().PoolReturnWeapon(info.weapons[i]);
      info.RemoveWeapon(info.weapons[i]);
    }
  }

  GetNetworkManager().SendMsg(&info);
}

/// Implementation of IItemsPool interface handling weapons and magazines of body
class ItemsPoolBody : public IItemsPool
{
public:
  OLink(EntityAI) _source;

public:
  ItemsPoolBody(EntityAI *source) : _source(source) {}
  EntityAI *GetSource() const {return _source;}

  // implementation of IItemsPool
  virtual bool CanDrop(const AIBrain *unit) const;
  virtual bool CanReplace(const AIBrain *unit) const;
  virtual bool CanRearm(const AIBrain *unit) const;
  virtual RString GetDropActionName(const AIBrain *unit, bool magazine) const;
  virtual void DropWeapon(UnitWeaponsInfo &info, WeaponType *weapon, bool backpack);
  virtual void DropMagazine(UnitWeaponsInfo &info, Magazine *magazine, bool backpack);
  virtual void DropBackpack(UnitWeaponsInfo &info, RString name);
  virtual void AddWeapon(UnitWeaponsInfo &info, WeaponType *weapon, bool backpack);
  virtual void AddMagazine(UnitWeaponsInfo &info, MagazineType *type, bool backpack);
  virtual void AddBackpack(UnitWeaponsInfo &info, RString name);
  virtual void ReplaceWeapon(UnitWeaponsInfo &info, int slot, WeaponType *weapon, bool backpack);
  virtual void ReplaceMagazine(UnitWeaponsInfo &info, int slot, MagazineType *type, bool backpack);
  virtual void ReplaceBackpack(UnitWeaponsInfo &info, RString name);
  virtual void Rearm(UnitWeaponsInfo &info);
  virtual WeaponType *FindWeapon(int mask, int maskExclude) const;
  virtual Magazine *FindMagazine(int mask) const;
  virtual void SummarizeWeapons(WeaponsSummary &result, int mask, int maskExclude) const;
  virtual void SummarizeMagazines(MagazinesSummary &result, int mask, int maskExclude) const;
  virtual  RefArray<EntityAI> SummarizeBackpacks() const;

  virtual RString GetDisplayName()
  {
    if(_source)
    {
      if(_source->GetType()->IsBackpack() && _source->GetSupply())
      {
        RString free =
          " ("  +  FormatNumber(_source->GetSupply()->GetFreeCargoBackpack(_source)) + "/" +
          FormatNumber((int)_source->GetType()->GetMaxMagazinesCargo())  +  ")";

        return (_source->GetType()->GetDisplayName() + free);
      }
      else return _source->GetType()->GetDisplayName();
    }
    return "";
  }
  virtual EntityAI *GetSource() {return _source;}

};

bool ItemsPoolBody::CanDrop(const AIBrain *unit) const
{
  if (!unit) return false;
  if (!_source) return false;

  Person *player = GWorld->GetRealPlayer();
  AIBrain *me = player ? player->Brain() : NULL;
  if (!me) return false;

  if (me->IsGroupLeader())
    return unit == me || !unit->IsAnyPlayer();
  else
    return unit == me;
}

bool ItemsPoolBody::CanReplace(const AIBrain *unit) const
{
  if (!unit) return false;
  if (!_source) return false;

  Person *player = GWorld->GetRealPlayer();
  AIBrain *me = player ? player->Brain() : NULL;
  if (!me) return false;

  if (me->IsGroupLeader())
    return unit == me || !unit->IsAnyPlayer();
  else
    return unit == me;
}

bool ItemsPoolBody::CanRearm(const AIBrain *unit) const
{
  return CanReplace(unit);
}

RString ItemsPoolBody::GetDropActionName(const AIBrain *unit, bool magazine) const
{
  if (!unit || !_source) return RString();
  return Format(LocalizeString(IDS_XBOX_HINT_GEAR_PUT), cc_cast(_source->GetDisplayName()));
}

void ItemsPoolBody::DropWeapon(UnitWeaponsInfo &info, WeaponType *weapon, bool backpack)
{
  if (!CanDrop(info.unit)) return;

  // create action and perform on unit
  Ref<Action> action = new ActionWeapon(ATPutWeapon, _source, weapon->GetName(), backpack);
  action->SetFromGUI(true);
  action->Process(info.unit);
}

void ItemsPoolBody::DropMagazine(UnitWeaponsInfo &info, Magazine *magazine, bool backpack)
{
  MagazineType *type = magazine->_type;
  if (!type) return;

  if (!CanDrop(info.unit)) return;

  // create action and perform on unit
  Ref<Action> action = new ActionMagazineType(ATPutMagazine, _source, type->GetName(), backpack);
  action->SetFromGUI(true);
  action->Process(info.unit);
}

void ItemsPoolBody::DropBackpack(UnitWeaponsInfo &info, RString name)
{
  if (!CanDrop(info.unit)) return;

  // create action and perform on unit
  Ref<Action> action = new ActionWeapon(ATDropBag, _source, name, false);
  action->SetFromGUI(true);
  action->Process(info.unit);
}

void ItemsPoolBody::AddWeapon(UnitWeaponsInfo &info, WeaponType *weapon, bool backpack)
{
  if (!CanReplace(info.unit)) return;

  int type = weapon->_weaponType;
  if(backpack && info.backpack && info.backpack->GetSupply())
    if(info.backpack->GetType()->_maxWeaponsCargo <= 0
      && ((type & MaskSlotPrimary) != 0 || (type & MaskSlotSecondary) != 0 || (type & MaskSlotHandGun) != 0)) return;


  // create action and perform on unit
  Ref<Action> action = new ActionWeapon(ATTakeWeapon, _source, weapon->GetName(), backpack);
  action->SetFromGUI(true);
  action->Process(info.unit);
}

void ItemsPoolBody::AddMagazine(UnitWeaponsInfo &info, MagazineType *type, bool backpack)
{
  if (!CanReplace(info.unit)) return;

  // create action and perform on unit
  Ref<Action> action = new ActionMagazineType(ATTakeMagazine, _source, type->GetName(), backpack);
  action->SetFromGUI(true);
  action->Process(info.unit);
}

void ItemsPoolBody::AddBackpack(UnitWeaponsInfo &info, RString name)
{
  if (!CanReplace(info.unit)) return;

  // create action and perform on unit
  Ref<Action> action = new ActionWeapon(ATAddBag, _source, name, false);
  action->SetFromGUI(true);
  action->Process(info.unit);
}

void ItemsPoolBody::ReplaceWeapon(UnitWeaponsInfo &info, int slot, WeaponType *weapon, bool backpack)
{
  AddWeapon(info, weapon, backpack);
}

void ItemsPoolBody::ReplaceMagazine(UnitWeaponsInfo &info, int slot, MagazineType *type, bool backpack)
{
  AddMagazine(info, type, backpack);
}

void ItemsPoolBody::ReplaceBackpack(UnitWeaponsInfo &info, RString name)
{
  AddBackpack(info, name);
}

void ItemsPoolBody::Rearm(UnitWeaponsInfo &info)
{
  if (!CanRearm(info.unit)) return;

  // create action and perform on unit
  Ref<Action> action = new ActionBasic(ATRearm, _source);
  action->SetFromGUI(true);
  action->Process(info.unit);
}

WeaponType *ItemsPoolBody::FindWeapon(int mask, int maskExclude) const
{
  if (!_source) return NULL;

  TurretContext context;
  if (!_source->GetPrimaryGunnerTurret(context)) return NULL;

  for (int i=0; i<context._weapons->_weapons.Size(); i++)
  {
    WeaponType *type = context._weapons->_weapons[i];
    int slots = type->_weaponType;
    if ((slots & mask) != 0 && (slots & maskExclude) == 0) return type;
  }
  return NULL;
}

Magazine *ItemsPoolBody::FindMagazine(int mask) const
{
  if (!_source) return NULL;

  TurretContext context;
  if (!_source->GetPrimaryGunnerTurret(context)) return NULL;

  for (int i=0; i<context._weapons->_magazines.Size(); i++)
  {
    Magazine *magazine = context._weapons->_magazines[i];
    MagazineType *type = magazine->_type;
    int slots = type->_magazineType;
    if ((slots & mask) != 0) return magazine;
  }
  return NULL;
}

void ItemsPoolBody::SummarizeMagazines(MagazinesSummary &result, int mask, int maskExclude) const
{
  if (!_source) return;

  TurretContext context;
  if (!_source->GetPrimaryGunnerTurret(context)) return;

  for (int i=0; i<context._weapons->_magazines.Size(); i++)
  {
    Magazine *magazine = context._weapons->_magazines[i];
    const MagazineType *type = magazine->_type;
    int slots = type->_magazineType;
    if ((slots & mask) == 0) continue;
    if ((slots & maskExclude) != 0) continue;

    bool found = false;
    for (int j=0; j<result.Size(); j++)
    {
      if (result[j].type == type)
      {
        result[j].count++;
        found = true;
        break;
      }
    }
    if (!found)
    {
      int j = result.Add();
      result[j].type = type;
      result[j].count = 1;
    }
  }
}

void ItemsPoolBody::SummarizeWeapons(WeaponsSummary &result, int mask, int maskExclude) const
{
  if (!_source) return;

  TurretContext context;
  if (!_source->GetPrimaryGunnerTurret(context)) return;

  for (int i=0; i<context._weapons->_weapons.Size(); i++)
  {
    const WeaponType *type = context._weapons->_weapons[i];
    int slots = type->_weaponType;
    if ((slots & mask) == 0) continue;
    if ((slots & maskExclude) != 0) continue;

    bool found = false;
    for (int j=0; j<result.Size(); j++)
    {
      if (result[j].type == type)
      {
        result[j].count++;
        found = true;
        break;
      }
    }
    if (!found)
    {
      int j = result.Add();
      result[j].type = type;
      result[j].count = 1;
    }
  }
}

RefArray<EntityAI> ItemsPoolBody::SummarizeBackpacks() const
{
  RefArray<EntityAI> bags;

  if(_source  && _source->GetBackpack())
    bags.Add(_source->GetBackpack());
  return bags;
}

/// Implementation of IItemsPool interface handling weapons and magazines of supply entity
class ItemsPoolSupply : public IItemsPool
{
public:
  OLink(EntityAI) _source;
  OLink(EntityAI) _sourceSecondary;

public:
  ItemsPoolSupply(EntityAI *source) : _source(source) {_sourceSecondary = NULL;}
  ItemsPoolSupply(EntityAI *source, EntityAI *sourceSecondary) : _source(source),_sourceSecondary(sourceSecondary) {}
  EntityAI *GetSource() const {return _source;}
  EntityAI *GetSourceSecondary() const {return _sourceSecondary;}
  virtual void SwitchSources()
  {
    if(_sourceSecondary)
    {
      EntityAI *tmpSource = _source;
      _source = _sourceSecondary;
      _sourceSecondary = tmpSource;
    }
  }

  // implementation of IItemsPool
  virtual bool CanDrop(const AIBrain *unit) const;
  virtual bool CanReplace(const AIBrain *unit) const;
  virtual bool CanRearm(const AIBrain *unit) const;
  virtual RString GetDropActionName(const AIBrain *unit, bool magazine) const;
  virtual void DropWeapon(UnitWeaponsInfo &info, WeaponType *weapon, bool backpack);
  virtual void DropMagazine(UnitWeaponsInfo &info, Magazine *magazine, bool backpack);
  virtual void DropBackpack(UnitWeaponsInfo &info, RString name);
  virtual void AddWeapon(UnitWeaponsInfo &info, WeaponType *weapon, bool backpack);
  virtual void AddMagazine(UnitWeaponsInfo &info, MagazineType *type, bool backpack);
  virtual void AddBackpack(UnitWeaponsInfo &info, RString name);
  virtual void ReplaceWeapon(UnitWeaponsInfo &info, int slot, WeaponType *weapon, bool backpack);
  virtual void ReplaceMagazine(UnitWeaponsInfo &info, int slot, MagazineType *type, bool backpack);
  virtual void ReplaceBackpack(UnitWeaponsInfo &info, RString name);
  virtual void Rearm(UnitWeaponsInfo &info);
  virtual WeaponType *FindWeapon(int mask, int maskExclude) const;
  virtual Magazine *FindMagazine(int mask) const;
  virtual void SummarizeWeapons(WeaponsSummary &result, int mask, int maskExclude) const;
  virtual void SummarizeMagazines(MagazinesSummary &result, int mask, int maskExclude) const;
  virtual  RefArray<EntityAI> SummarizeBackpacks() const;

  virtual RString GetDisplayName()
  {
    if(_source)
    {
      if(_source->GetType()->IsBackpack())
      {
        RString free =
          " ("  +  FormatNumber(_source->GetSupply()->GetFreeCargoBackpack(_source)) + "/" +
          FormatNumber((int)_source->GetType()->GetMaxMagazinesCargo())  +  ")";

        return _source->GetType()->GetDisplayName() + free;
      }
      else return _source->GetType()->GetDisplayName();
    }
    return "";
  }
  virtual EntityAI *GetSource() {return _source;}
};

bool ItemsPoolSupply::CanDrop(const AIBrain *unit) const
{
  if (!unit) return false;
  if (!_source) return false;

  Person *player = GWorld->GetRealPlayer();
  AIBrain *me = player ? player->Brain() : NULL;
  if (!me) return false;

  if (me->IsGroupLeader())
    return unit == me || !unit->IsAnyPlayer();
  else
    return unit == me;
}

bool ItemsPoolSupply::CanReplace(const AIBrain *unit) const
{
  if (!unit) return false;
  if (!_source) return false;

  Person *player = GWorld->GetRealPlayer();
  AIBrain *me = player ? player->Brain() : NULL;
  if (!me) return false;

  if (me->IsGroupLeader())
    return unit == me || !unit->IsAnyPlayer();
  else
    return unit == me;
}

bool ItemsPoolSupply::CanRearm(const AIBrain *unit) const
{
  return CanReplace(unit);
}

RString ItemsPoolSupply::GetDropActionName(const AIBrain *unit, bool magazine) const
{
  if (!unit || !_source) return RString();
#if !_VBS3 //changed handling of cargo space
  if (magazine)
  {
    if (_source->GetFreeMagazineCargo() <= 0) return RString();
  }
  else
  {
    if (_source->GetFreeWeaponCargo() <= 0) return RString();
  }
#endif
  return Format(LocalizeString(IDS_XBOX_HINT_GEAR_PUT), cc_cast(_source->GetDisplayName()));
}

void ItemsPoolSupply::DropWeapon(UnitWeaponsInfo &info, WeaponType *weapon, bool backpack)
{
  if (!CanDrop(info.unit)) return;

  // create action and perform on unit
  Ref<Action> action = new ActionWeapon(ATPutWeapon, _source, weapon->GetName(), backpack);
  action->SetFromGUI(true);
  action->Process(info.unit);
}

void ItemsPoolSupply::DropMagazine(UnitWeaponsInfo &info, Magazine *magazine, bool backpack)
{
  MagazineType *type = magazine->_type;
  if (!type) return;

  if (!CanDrop(info.unit)) return;

  // create action and perform on unit
  Ref<Action> action = new ActionMagazineType(ATPutMagazine, _source, type->GetName(), backpack);
  action->SetFromGUI(true);
  action->Process(info.unit);
}

void ItemsPoolSupply::DropBackpack(UnitWeaponsInfo &info, RString name)
{
  if (!CanDrop(info.unit)) return;

  // create action and perform on unit
  Ref<Action> action = new ActionWeapon(ATDropBag, _source, name, false);
  action->SetFromGUI(true);
  action->Process(info.unit);
}

void ItemsPoolSupply::AddWeapon(UnitWeaponsInfo &info, WeaponType *weapon, bool backpack)
{
  if (!CanDrop(info.unit)) return;

  int type = weapon->_weaponType;
  if(backpack && info.backpack && info.backpack->GetSupply())
    if(info.backpack->GetType()->_maxWeaponsCargo <= 0
      && ((type & MaskSlotPrimary) != 0 || (type & MaskSlotSecondary) != 0 || (type & MaskSlotHandGun) != 0)) return;

  // create action and perform on unit
  Ref<Action> action = new ActionWeapon(ATTakeWeapon, _source, weapon->GetName(), backpack);
  action->SetFromGUI(true);
  action->Process(info.unit);
}

void ItemsPoolSupply::AddMagazine(UnitWeaponsInfo &info, MagazineType *type, bool backpack)
{
  if (!CanReplace(info.unit)) return;

  // create action and perform on unit
  Ref<Action> action = new ActionMagazineType(ATTakeMagazine, _source, type->GetName(), backpack);
  action->SetFromGUI(true);
  action->Process(info.unit);
}


void ItemsPoolSupply::AddBackpack(UnitWeaponsInfo &info, RString name)
{
  if (!CanReplace(info.unit)) return;

  // create action and perform on unit
  Ref<Action> action = new ActionWeapon(ATAddBag, _source, name, false);
  action->SetFromGUI(true);
  action->Process(info.unit);
}

void ItemsPoolSupply::ReplaceWeapon(UnitWeaponsInfo &info, int slot, WeaponType *weapon, bool backpack)
{
  AddWeapon(info, weapon, backpack);
}

void ItemsPoolSupply::ReplaceMagazine(UnitWeaponsInfo &info, int slot, MagazineType *type, bool backpack)
{
  AddMagazine(info, type, backpack);
}

void ItemsPoolSupply::ReplaceBackpack(UnitWeaponsInfo &info, RString name)
{
  AddBackpack(info, name);
}

void ItemsPoolSupply::Rearm(UnitWeaponsInfo &info)
{
  if (!CanRearm(info.unit)) return;

  // create action and perform on unit
  Ref<Action> action = new ActionBasic(ATRearm, _source);
  action->SetFromGUI(true);
  action->Process(info.unit);
}

WeaponType *ItemsPoolSupply::FindWeapon(int mask, int maskExclude) const
{
  if (!_source) return NULL;

  const ResourceSupply *supply = _source->GetSupply();
  if (!supply) return NULL;

  for (int i=0; i<supply->GetWeaponCargoSize(); i++)
  {
    WeaponType *type = unconst_cast(supply->GetWeaponCargo(i));
    int slots = type->_weaponType;
    if ((slots & mask) != 0 && (slots & maskExclude) == 0) return type;
  }
  return NULL;
}

Magazine *ItemsPoolSupply::FindMagazine(int mask) const
{
  if (!_source) return NULL;

  const ResourceSupply *supply = _source->GetSupply();
  if (!supply) return NULL;

  for (int i=0; i<supply->GetMagazineCargoSize(); i++)
  {
    Magazine *magazine = unconst_cast(supply->GetMagazineCargo(i));
    MagazineType *type = magazine->_type;
    int slots = type->_magazineType;
    if ((slots & mask) != 0) return magazine;
  }
  return NULL;
}

void ItemsPoolSupply::SummarizeMagazines(MagazinesSummary &result, int mask, int maskExclude) const
{
  if (!_source) return;

  const ResourceSupply *supply = _source->GetSupply();
  if (!supply) return;

  for (int i=0; i<supply->GetMagazineCargoSize(); i++)
  {
    const Magazine *magazine = supply->GetMagazineCargo(i);
    const MagazineType *type = magazine->_type;
    int slots = type->_magazineType;
    if ((slots & mask) == 0) continue;
    if ((slots & maskExclude) != 0) continue;

    bool found = false;
    for (int j=0; j<result.Size(); j++)
    {
      if (result[j].type == type)
      {
        result[j].count++;
        found = true;
        break;
      }
    }
    if (!found)
    {
      int j = result.Add();
      result[j].type = type;
      result[j].count = 1;
    }
  }
}

void ItemsPoolSupply::SummarizeWeapons(WeaponsSummary &result, int mask, int maskExclude) const
{
  if (!_source) return;

  const ResourceSupply *supply = _source->GetSupply();
  if (!supply) return;

  for (int i=0; i<supply->GetWeaponCargoSize(); i++)
  {
    const WeaponType *type = supply->GetWeaponCargo(i);
    int slots = type->_weaponType;
    if ((slots & mask) == 0) continue;
    if ((slots & maskExclude) != 0) continue;

    bool found = false;
    for (int j=0; j<result.Size(); j++)
    {
      if (result[j].type == type)
      {
        result[j].count++;
        found = true;
        break;
      }
    }
    if (!found)
    {
      int j = result.Add();
      result[j].type = type;
      result[j].count = 1;
    }
  }
}

RefArray<EntityAI> ItemsPoolSupply::SummarizeBackpacks() const
{
  RefArray<EntityAI> bags;
  if (!_source) return bags;

  const ResourceSupply *supply = _source->GetSupply();
  if (!supply) return bags;

  return supply->GetBackpacks();
}

#if defined _XBOX && _XBOX_VER >= 200
bool WeaponsInfo::Load(QIStream &in, FixedItemsPool &pool)
#else
bool WeaponsInfo::Load(RString filename, FixedItemsPool &pool)
#endif
{
  GameDataNamespace globals(NULL, RString(), false); // save - no global namespace

  ParamArchiveLoad ar;
  ar.SetParams(&pool);

#if defined _XBOX && _XBOX_VER >= 200
  if (!ar.LoadBin(in, NULL, &globals)) return false;
#else
  if (!ar.LoadBin(filename, NULL, &globals) && !ar.Load(filename, NULL, &globals)) return false;
#endif
  ar.FirstPass();
  if (Serialize(ar) != LSOK) return false;
  ar.SecondPass();
  if (Serialize(ar) != LSOK) return false;

  // assign new ID to all magazines
  for (int i=0; i<_weapons.Size(); i++)
  {
    UnitWeaponsInfo &info = _weapons[i];
    for (int j=0; j<MAGAZINE_SLOTS;)
    {
      Magazine *magazine = info.magazines[j++];
      if (!magazine) continue;
      int oldId = magazine->_id;
      magazine->_id = GWorld->GetMagazineID();
      while (j < MAGAZINE_SLOTS)
      {
        Magazine *m = info.magazines[j];
        if (!m || m->_id != oldId) break;
        info.magazines[j++] = magazine;
      }
    }
  }

  for (int j=0; j<pool._magazinesPool.Size();)
  {
    Magazine *magazine = pool._magazinesPool[j++];
    if (!magazine) continue;
    int oldId = magazine->_id;
    magazine->_id = GWorld->GetMagazineID();
    while (j < pool._magazinesPool.Size())
    {
      Magazine *m = pool._magazinesPool[j];
      if (!m || m->_id != oldId) break;
      pool._magazinesPool[j++] = magazine;
    }
  }

  return true;
}

#if defined _XBOX && _XBOX_VER >= 200
bool WeaponsInfo::Save(QOStream &out, FixedItemsPool &pool)
#else
bool WeaponsInfo::Save(RString filename, FixedItemsPool &pool)
#endif
{
  ParamArchiveSave ar(UserInfoVersion);
  ar.SetParams(&pool);

  if (Serialize(ar) != LSOK) return false;
#if defined _XBOX && _XBOX_VER >= 200
  return ar.SaveBin(out);
#elif _ENABLE_CHEATS
  return ar.Save(filename);
#else
  return ar.SaveBin(filename);
#endif
}

LSError WeaponsInfo::Serialize(ParamArchive &ar)
{
  FixedItemsPool *pool = reinterpret_cast<FixedItemsPool *>(ar.GetParams());

  ar.Serialize("Weapons", _weapons, 1);
  ar.Serialize("WeaponsPool", pool->_weaponsPool, 1);
  ar.Serialize("MagazinesPool", pool->_magazinesPool, 1);
  ar.Serialize("BackpacksPool", pool->_backpacksPool, 1);
  return LSOK;
}

/*!
\patch 1.80 Date 8/5/2002 by Jirka
- Fixed: When replaying mission from campaign, weapon pool was not loaded.
*/

bool EnabledWeaponPool()
{
#if _ENABLE_DATADISC
  // if (!IsCampaign()) return false;
  ConstParamEntryPtr entry = ExtParsCampaign.FindEntry("weaponPool");
  if (!entry) return false;
  return (bool)(*entry);
#else
  return false;
#endif
}

bool EnableMarkEntities()
{
  if (!EnabledWeaponPool()) return false;

  ConstParamEntryPtr entry = ExtParsMission.FindEntry("markWeapons");
  if (!entry) return false;
  return (bool)(*entry);
}

/*!
\patch 1.14 Date 08/10/2001 by Jirka
- Fixed: changes of weapons in briefing are sent over network in MP
*/
void WeaponsInfo::Apply()
{
  Person *player = GWorld->GetRealPlayer();
  AIBrain *playerUnit = player ? player->Brain() : NULL;
  // add weapons
  for (int i=0; i<_weapons.Size(); i++)
  {
    UnitWeaponsInfo &info = _weapons[i];
    AIBrain *unit = info.unit;
    if (!unit) continue;
    if (GWorld->GetMode() == GModeNetware)
    {
      if (playerUnit->IsGroupLeader())
      {
        if (unit != playerUnit && unit->IsAnyPlayer()) continue;
      }
      else
      {
        if (unit != playerUnit) continue;
      }
    }
    Person *veh = unit->GetPerson();
    veh->RemoveAllWeapons(true);
    veh->RemoveAllMagazines();
    veh->RemoveBackpack();
    for (int j=0; j<MAGAZINE_SLOTS; j++)
    {
      Ref<Magazine> magazine = info.magazines[j];
      if (!magazine) continue;
      info.RemoveMagazine(magazine);
      veh->AddMagazine(magazine);
    }
    for (int j=0; j<WEAPON_SLOTS; j++)
    {
      Ref<WeaponType> weapon = info.weapons[j];
      if (!weapon) continue;
      info.RemoveWeapon(weapon);
      veh->AddWeapon(weapon);
    }
    if(info.HasBackpack())
    {
      Ref<EntityAI> bag = info.backpack;
      info.RemoveBackpack();
      if(bag)
      {
        bag->MoveOut(veh); // remove reference from the world
        veh->GetWeapons().SetBackpack(bag);
      }
    }
    veh->AddDefaultWeapons();
    if (GWorld->FocusOn() && GWorld->FocusOn()->GetVehicle() == veh)
      GWorld->UI()->ResetVehicle(veh);

    veh->AutoReloadAll(true);

    // FIX
    TurretContext context;
    if (veh->GetPrimaryGunnerTurret(context))
    {
      if (context._weapons->_currentWeapon < 0)
        context._weapons->SelectWeapon(veh, context._weapons->FirstWeapon(veh), true);
      if (!veh->IsLocal()) GetNetworkManager().UpdateWeapons(veh, context._turret, context._gunner);
    }
  }

  _weapons.Clear();
}

class TextToHTMLParser : public SAXParser
{
protected:
  CHTMLContainer *_html;
  int _section;

public:
  TextToHTMLParser(CHTMLContainer *html, int section);
  void OnStartElement(RString name, XMLAttributes &attributes);
  void OnEndElement(RString name);
  void OnCharacters(RString chars);
};

TextToHTMLParser::TextToHTMLParser(CHTMLContainer *html, int section)
: _html(html), _section(section)
{
}

void TextToHTMLParser::OnStartElement(RString name, XMLAttributes &attributes)
{
  if (strcmp(name, "br") == 0)
  {
    _html->AddBreak(_section, false);
  }
  else
  {
    RptF("Unknown text element '%s'", (const char *)name);
  }
}

void TextToHTMLParser::OnEndElement(RString name)
{
}

void TextToHTMLParser::OnCharacters(RString chars)
{
  _html->AddText(_section, chars, HFP, HALeft, false, false, RString());
}



CItemSlot::CItemSlot(ControlsContainer *parent, int idc, ParamEntryPar cls)
: Control(parent, CT_USER, idc, cls)
{
  SetText(cls>>"text");
  _color = GetPackedColor(cls >> "color");
  _bgColor = GetPackedColor(cls >> "colorBackground");
  _bgColorSelected = GetPackedColor(cls >> "colorBackgroundSelected");
  _colorFocused = GetPackedColor(cls >> "colorFocused");

  GetValue(_pushSound, cls >> "soundPush");
  GetValue(_clickSound, cls >> "soundClick");
  GetValue(_dblClickSound, cls >> "soundDoubleClick");

  _selected = false;
}

bool CItemSlot::OnKeyDown(int dikCode)
{
  if (dikCode == DIK_SPACE || dikCode == DIK_RETURN || dikCode == DIK_NUMPADENTER ||
    dikCode == INPUT_DEVICE_XINPUT + XBOX_A || dikCode == INPUT_DEVICE_XINPUT + XBOX_Start)
  {
    if (IsEnabled())
    {
#ifndef _XBOX
      OnEvent(CEButtonDown);
#endif
      PlaySound(_pushSound);
      return true;
    }
  }
  return false;
}

bool CItemSlot::OnKeyUp(int dikCode)
{
  if (dikCode == DIK_SPACE || dikCode == INPUT_DEVICE_XINPUT + XBOX_A)
  {
    if (IsEnabled())
    {
      DoClick();
      return true;
    }
  }
  else if (dikCode == DIK_RETURN || dikCode == DIK_NUMPADENTER || dikCode == INPUT_DEVICE_XINPUT + XBOX_Start)
  {
    if (IsEnabled())
    {
      DoDblClick();
      return true;
    }
  }
  return false;
}

void CItemSlot::OnLButtonDown(float x, float y)
{
#ifndef _XBOX
  OnEvent(CEButtonDown);
#endif
  PlaySound(_pushSound);
}

void CItemSlot::OnLButtonUp(float x, float y)
{
  if (IsInside(x, y))
  {
    DoClick();
  }
}

void CItemSlot::OnLButtonDblClick(float x, float y)
{
  if (IsInside(x, y))
  {
    DoDblClick();
  }
}

void CItemSlot::OnDraw(UIViewport *vp, float alpha)
{
  PackedColor bgColor = ModAlpha(_selected ? _bgColorSelected : _bgColor, alpha);
  PackedColor color = ModAlpha(_color, alpha);

  const int w = GLOB_ENGINE->Width2D();
  const int h = GLOB_ENGINE->Height2D();

  float xx = toInt(_x * w);
  float yy = toInt(_y * h);
  float ww = toInt((_x + SCALED(_w)) * w) - xx;
  float hh = toInt((_y + SCALED(_h)) * h) - yy;

  // draw background
  Draw2DParsExt pars;
  pars.mip = GEngine->TextBank()->UseMipmap(NULL, 0, 0);
  pars.SetColor(bgColor);
  pars.Init();
  vp->Draw2D(pars, Rect2DPixel(xx + 0.5, yy + 0.5, ww, hh));

  if (_texture)
  {
    float x = _x, y = _y, width = SCALED(_w), height = SCALED(_h);
    if (_style & ST_KEEP_ASPECT_RATIO)
    {
      float xCoef = (SCALED(_w) * w) / _texture->AWidth();
      float yCoef = (SCALED(_h) * h) / _texture->AHeight();
      if (xCoef > yCoef)
      {
        width *= yCoef / xCoef;
        x += 0.5 * (SCALED(_w) - width);
      }
      else
      {
        height *= xCoef / yCoef;
        y += 0.5 * (SCALED(_h) - height);
      }
    }
    float xx = toInt(x * w);
    float yy = toInt(y * h);
    float ww = toInt((x + width) * w) - xx;
    float hh = toInt((y + height) * h) - yy;

    // draw picture
    Draw2DParsExt pars;
    pars.mip = GEngine->TextBank()->UseMipmap(_texture, 0, 0);
    pars.SetColor(color);
    pars.Init();
    vp->Draw2D(pars, Rect2DPixel(xx + 0.5, yy + 0.5, ww, hh));
  }

  if (IsFocused())
  {
    PackedColor colorFocused = ModAlpha(_colorFocused, alpha);

    Line2DPixelInfo lines[4];
    for (int i=0; i<4; i++)
    {
      lines[i].c0 = colorFocused;
      lines[i].c1 = colorFocused;
    }

    lines[0].beg = Point2DPixel(xx, yy);
    lines[0].end = Point2DPixel(xx + ww, yy);
    lines[1].beg = Point2DPixel(xx + ww, yy);
    lines[1].end = Point2DPixel(xx + ww, yy + hh);
    lines[2].beg = Point2DPixel(xx + ww, yy + hh);
    lines[2].end = Point2DPixel(xx, yy + hh);
    lines[3].beg = Point2DPixel(xx, yy + hh);
    lines[3].end = Point2DPixel(xx, yy);

    vp->DrawLines(lines, 4);
  }
}

bool CItemSlot::SetText(RString text)
{
  if (!CTextContainer::SetText(text)) return false;

  text = FindPicture(text);
  text.Lower();
  _texture = GlobLoadTexture(text);
  if (_texture) _texture->SetUsageType(Texture::TexUI); // no limits
  return true;
}

void CItemSlot::SetTexture(Texture *texture, RString text)
{
  if (!CTextContainer::SetText(text)) return;
  _texture = texture;
  if (_texture) _texture->SetUsageType(Texture::TexUI); // no limits
}

void CItemSlot::SetData(RString text)
{
  _data = text;
}

RString CItemSlot::GetData()
{
  return _data;
}

void CItemSlot::SetItemType(int itemType) {_itemType = itemType;};
int CItemSlot::GetItemType() {return _itemType;}

void CItemSlot::DoClick()
{
  PlaySound(_clickSound);

#ifndef _XBOX
  if (OnCheckEvent(CEButtonClick)) return;
#endif
  if (_parent) _parent->OnButtonClicked(IDC());
}

void CItemSlot::DoDblClick()
{
  PlaySound(_dblClickSound);

#ifndef _XBOX
  if (OnCheckEvent(CEButtonDblClick)) return;
#endif
  if (_parent) _parent->OnButtonDblClick(IDC());
}

static RString GetWeaponPicture(const WeaponType *weapon);


class GearPoolListBox : public CListNBox
{
private:
  PackedColor _colorPlayerItem;

public:

  GearPoolListBox(ControlsContainer *parent, int idc, ParamEntryPar cls)
    : CListNBox(parent, idc, cls)
  {//columns positions (will be defined by config file)

    _drawSideArrows = true;
    _canDrag = true;
    _primaryColumn = 1;

    _colorPlayerItem = GetPackedColor(cls >> "colorPlayerItem");

  };

  //!set row items color based on amount of available items
  void SetColors(int countAvail,int countTaken,int row)
  { //(colors will be defined by config file)
    PackedColor avail, item, taken;
    //pool - black if none is available, green if some item is avail., white if unit has some
    if(countAvail == 0) avail = PackedColor(0,0,0,0);
    else if(countTaken>0)  avail = _colorPlayerItem;
    else   avail = PackedColor(255,255,255,255);
    //item name - white if unit has some, green otherwise
    if(countTaken>0) item = _colorPlayerItem;
    else item = PackedColor(255,255,255,255);
    //unit pool - white if unit has any item, black otherwise 
    if(countTaken==0) taken = PackedColor(0,0,0,0);
    else taken = _colorPlayerItem;

    SetFtColor(row,0,avail);
    SetFtColor(row,1,item);
    SetFtColor(row,2,taken);
  }

  //add row with three columns
  int AddItem(RString text, int count, int wCount)
  {
    int index;
    if(count>0) index = this->AddString(FormatNumber(count));
    else index = this->AddString("");

    this->AddString(text);

    if(wCount>0) this->AddString(FormatNumber(wCount)); //classical listbox text in not displayed - will be removed
    else  this->AddString("");
    for(int i = 3; i< _nColumns; i++) this->AddString(""); //fill extra columns
    index = index/_nColumns;
    SetColors(count,wCount,index);  //columns colors 
    this->SetValue(index,0,count);
    this->SetValue(index,2,wCount);
    return index;
  }

  //! add weapon from pool to list box 
  int AddGearString(WeaponsSummaryItem summary,const UnitWeaponsInfo *info, bool openedBag)
  { //weapon
    const WeaponType *type = summary.type;
    int count = summary.count;
    RString text = type->GetDisplayName();
    //how many pieces of this weapon unit has 
    int wCount = 0;
    if(!openedBag)
    {
      for(int i=0; i< WEAPON_SLOTS; i++)
      {
        if(info->weapons[i] && type == info->weapons[i]) wCount++;
        //weapon in 2 slots (4+1)
        if (type->_weaponType == 5) i++;
      }
    }
    else if(info->HasBackpack() && info->backpack->GetSupply())
    {
      for(int i=0; i< info->backpack->GetSupply()->GetWeaponCargoSize(); i++)
      {
        if(info->backpack->GetSupply()->GetWeaponCargo(i) && type == info->backpack->GetSupply()->GetWeaponCargo(i))
          wCount++;
      }
    }
    //add columns
    return AddItem(text,count,wCount);
  }

  //! add magazines from pool to list box 
  int AddGearString(MagazinesSummaryItem summary,const UnitWeaponsInfo *info, bool openedBag)
  { //magazine
    const MagazineType *type = summary.type;
    int count = summary.count;
    RString text = type->GetDisplayName();
    //how many pieces of this magazine unit has 
    int wCount = 0;
    Magazine *magazine;
    if(!openedBag)
    {
      for (int i=0; i<FIRST_HANDGUN_SLOT; i++)
      {
        magazine =info->magazines[i];
        if (magazine && type == magazine->_type)
        {
          wCount++;
          //rockets takes more than one slot
          i += max(0,GetItemSlotsCount(magazine->_type->_magazineType) - 1);
        }
      }
      // handgun magazines
      for (int i=FIRST_HANDGUN_SLOT; i<MAGAZINE_SLOTS; i++)
      {
        magazine =info->magazines[i];
        if (magazine && type == magazine->_type)
        {
          wCount++;
          //rockets takes more than one slot
          i += max(0,GetHandGunItemSlotsCount(magazine->_type->_magazineType) - 1);
        }
      }
    }
    else if(info->HasBackpack() && info->backpack->GetSupply())
    {
      for(int i=0; i< info->backpack->GetSupply()->GetMagazineCargoSize(); i++)
      {
        if(info->backpack->GetSupply()->GetMagazineCargo(i) && type == info->backpack->GetSupply()->GetMagazineCargo(i)->_type) 
          wCount++;
      }
    }

    //add columns
    return AddItem(text,count,wCount);
  }

  //add unit's items to listbox
  int AddUnitPool(const WeaponType *type,const UnitWeaponsInfo *info, bool backpack)
  { //check if item is already in listBox
    int rows = this->Size()/_nColumns;
    for(int i=0; i< rows; i++)
    { //check if item is weapon  
      if(this->GetValue(i,_primaryColumn)==0 || this->GetValue(i,_primaryColumn)==2)
      {//item already present  - end
        Ref<WeaponType> weapon = WeaponTypes.New(this->GetData(i,_primaryColumn));
        if(weapon == type) return -1;
      }
    }

    int wCount = 0;
    if(!backpack)
    {
      //how many pieces of this weapon unit has 
      for(int i=0; i< WEAPON_SLOTS; i++)
      {
        if(info->weapons[i] && type == info->weapons[i]) wCount++;
        //weapon in 2 slots (4+1)
        if (type->_weaponType == 5) i++;
      }
    }
    else
    {
      if(info->HasBackpack() && info->backpack->GetSupply())
      {
        for(int i=0; i< info->backpack->GetSupply()->GetWeaponCargoSize(); i++)
        {
          if(info->backpack->GetSupply()->GetWeaponCargo(i) && type == info->backpack->GetSupply()->GetWeaponCargo(i))
            wCount++;
        }
      }
    }
    //add columns
    return AddItem(type->_displayName,0,wCount);
  }

  int AddUnitPool(const MagazineType *type,const UnitWeaponsInfo *info, bool backpack)
  {//check if item is already in listBox
    int rows = this->Size()/_nColumns;
    for(int i=0; i< rows; i++)
    {//check if item is magazine  
      if(this->GetValue(i,_primaryColumn)==1)
      {//item already present  - end
        Ref<MagazineType> magazine = MagazineTypes.New(this->GetData(i,_primaryColumn));
        if(magazine == type) return -1;
      }
    }
    // how many pieces of this magazine unit has 
    int wCount = 0;

    if(!backpack)
    {   
      // magazines for primary and secondary weapon
      for (int i=0; i<FIRST_HANDGUN_SLOT; i++)
      {
        Magazine *magazine = info->magazines[i];
        if (magazine && type == magazine->_type)
        {
          wCount++;
          //rockets takes more than one slot
          i += GetItemSlotsCount(magazine->_type->_magazineType) - 1;
        }
      }
      // handgun magazines
      for (int i=FIRST_HANDGUN_SLOT; i<MAGAZINE_SLOTS; i++)
      {
        Magazine *magazine = info->magazines[i];
        if (magazine && type == magazine->_type)
        {
          wCount++;
          //rockets takes more than one slot
          i += GetHandGunItemSlotsCount(magazine->_type->_magazineType) - 1;
        }
      }
    }
    else if(info->HasBackpack() && info->backpack->GetSupply())
    {
      for(int i=0; i< info->backpack->GetSupply()->GetMagazineCargoSize(); i++)
      {
        if(info->backpack->GetSupply()->GetMagazineCargo(i) && type == info->backpack->GetSupply()->GetMagazineCargo(i)->_type) 
          wCount++;
      }
    }

    //add columns
    return AddItem(type->_displayName,0,wCount);
  }

  int AddUnitPool(const EntityAIType *type)
  {//check if item is already in listBox
    //how many pieces of this weapon unit has 
    int wCount = 1;
    //add columns
    int rows = this->Size()/_nColumns;
    for(int i=0; i< rows; i++)
    {//check if item is magazine  
      if(this->GetValue(i,_primaryColumn)==3)
      {//item already present  - end
        if(strcmp(type->GetName(),this->GetData(i,_primaryColumn))==0) return -1;
      }
    }
    return AddItem(type->_displayName,0,wCount);
  }

  static int CompareGearItems( const SortLisNItem *str0, const SortLisNItem *str1 )
  {
    if(str0->value == str1->value)
      return stricmp(str0->text, str1->text);
    else 
      return str0->value - str1->value;
  }

  virtual void SortItems()
  {
    AutoArray<SortLisNItem> primaryItems;
    int index = 0;
    for(int i = 0; i<RowsCount();i++)
    {    
      AutoArray<CListBoxItem> rowItems;
      for(int j=0;j<NColumns();j++)
        rowItems.Add(this->Get(index++));
      primaryItems.Add(SortLisNItem(GetText(i,_primaryColumn),GetValue(i,_primaryColumn),rowItems));
    }

    QSort(primaryItems.Data(), primaryItems.Size(), CompareGearItems);

    int rows = RowsCount();
    int columns = NColumns();
    this->Clear();

    for(int i = 0; i<rows;i++)
      for(int j=0;j<columns;j++)
        this->Add(primaryItems[i].rowItems[j]);
  }
};

struct GearFilter 
{
  RString name;
  Ref<Texture> image;
  int mask;
};
TypeIsMovable(GearFilter)

class DisplayGear : public Display
{
protected:
  InitPtr<CStatic> _skill;
  InitPtr<CStatic> _vehicle;

  InitPtr<CHTMLContainer> _overview;
  Ref<CItemSlot> _slotPrimary;
  Ref<CItemSlot> _slotSecondary;
  Ref<CItemSlot> _slotItem[FIRST_HANDGUN_SLOT - FIRST_PRIMARY_SLOT];
  Ref<CItemSlot> _slotHandgun;
  Ref<CItemSlot> _slotHandgunItem[MAGAZINE_SLOTS - FIRST_HANDGUN_SLOT];
  Ref<CItemSlot> _slotSpecial[2];
  Ref<CItemSlot> _slotInventory[WEAPON_SLOTS - FIRST_INVENTORY_SLOT];
  Ref<CItemSlot> _selectedSlot;

  RefArray<CItemSlot> _bagItems;

  WeaponsInfo &_weaponsInfo;
  IItemsPool &_pool;
  Ref<IItemsPool> _poolSecondary; //to store original pool when bag is opened
  OLinkPermNO(AIBrain) _unit;
  bool _forced;

  Ref<GearPoolListBox> _poolListBox;
  AutoArray<GearFilter> _filters;

  bool _openedBag, _openedBagOnGround;
  bool _onGround;
  RString _resource;

  enum
  {
    ActionNone, ActionRearm, ActionTake, ActionDrop, ActionOpen
  } _defaultAction;

  int _activeFilter;

  RString _emptyGun;
  RString _emptySec;
  RString _emptyEq;
  RString _emptyMag;
  RString _emptyMag2;
  RString _emptyHGun;
  RString _emptyHGunMag;

  /// number of instances
  static int _running;

  float _magW, _magH, _weaponW, _weaponH, _gunW, _gunH, _spacing;

public:
  DisplayGear(ControlsContainer *parent, bool enableSimulation, WeaponsInfo &weaponsInfo, IItemsPool &pool, AIBrain *unit, bool 
    forced,RString resource = "RscDisplayGear", bool onGround = false);

  ~DisplayGear();
  Control *OnCreateCtrl(int type, int idc, ParamEntryPar cls);
  void OnSimulate(EntityAI *vehicle);

  void OnButtonClicked(int idc);
  void OnButtonDblClick(int idc);
  void OnLBSelChanged(IControl *ctrl, int curSel);
  void OnLBDblClick(int idc, int curSel);
  bool OnLBDragging(IControl *ctrl, float x, float y);
  bool OnLBDrop(IControl *ctrl, float x, float y);
  void OnHTMLLink(int idc, RString link);
  void OnCtrlFocused(IControl *ctrl);

  void OnUnitChanged();
  virtual void UpdateWeapons(EntityAI *owner = NULL);

  bool CheckOwner(EntityAI *owner);

  static bool IsRunning() {return _running > 0;}

protected:
  /// drop an item its slot is described by a given idc
  void DropItem(int idc);

  /// show description of weapon in the overview control
  void UpdateOverview(const WeaponType *weapon);
  /// show description of magazine in the overview control
  void UpdateOverview(const MagazineType *magazine);
  /// show description of backpack in the overview control
  void UpdateOverview(RString name);

  // update description and state of buttons
  void UpdateButtons();

  /// helper for retrieving of current unit's items
  const UnitWeaponsInfo *GetCurrentInfo() const;
  /// helper for retrieving of current unit's items
  UnitWeaponsInfo *GetCurrentInfo();

  //select item in list box
  void SelectWeapon(WeaponType *weapon);
  void SelectMagazine(MagazineType *type);
  void SelectBackpack(RString name);
};

bool DisplayGearIsRunning()
{
  return DisplayGear::IsRunning();
}

int DisplayGear::_running = 0;

DisplayGear::DisplayGear(ControlsContainer *parent, bool enableSimulation, WeaponsInfo &weaponsInfo, IItemsPool &pool, AIBrain *unit, 
                         bool forced,RString resource, bool onGround)
                         : Display(parent), _weaponsInfo(weaponsInfo), _pool(pool), _unit(unit)
{
  _running++;

  _forced = forced;
  _defaultAction = ActionNone;

  ParamEntryVal cls = Pars >> resource;
  _emptyGun = cls >> "emptyGun";
  _emptySec = cls >> "emptySec";
  _emptyEq = cls >> "emptyEq";
  _emptyMag = cls >> "emptyMag";
  _emptyMag2 = cls >> "emptyMag2";
  _emptyHGun = cls >> "emptyHGun";
  _emptyHGunMag = cls >> "emptyHGunMag";

  _resource = resource;

  ParamEntryVal controls = cls >> "controls";
  if(controls.FindEntry("BagItemsGroup"))
  {
    ParamEntryVal bagItems = controls >> "BagItemsGroup";
    _magW = bagItems >> "magW";
    _magH = bagItems >> "magH";
    _weaponW = bagItems >> "weaponW";
    _weaponH  = bagItems >> "weaponH";
    _gunW = bagItems >> "gunW";
    _gunH = bagItems >> "gunH";
    _spacing = bagItems >> "spacing";
  }

  Load(resource);

  ITextContainer *control = GetTextContainer(GetCtrl(IDC_GEAR_UNIT));
  if (control)
  {
    int id = unit->GetUnit() ? unit->GetUnit()->ID() : 0;
    RString name;
    int rank = RankUndefined;
    if (unit->GetPerson())
    { 
      name = unit->GetPerson()->GetPersonName();
      rank = unit->GetPerson()->GetRank();
    }
    control->SetText(FormatNumber(id) + " - " + LocalizeString(IDS_SHORT_PRIVATE + rank) + " " +  name);
  }

  _enableSimulation = _enableSimulation && enableSimulation;

  IControl *ctrl = GetCtrl(IDC_GEAR_CONTINUE);
  if (ctrl) ctrl->EnableCtrl(_forced);

  ConstParamEntryPtr filters = cls.FindEntry("Filters");
  if(filters) for(int i=0; i<filters->GetEntryCount(); i++)
  {
    ParamEntryVal val = filters->GetEntry(i);
    GearFilter filter;
    filter.name = val >> "name";
    filter.mask = val >> "mask";
    filter.image = GlobLoadTexture(GetPictureName(val >> "image"));
    _filters.Add(filter);
  }

  _activeFilter = 0;
  _openedBag = _openedBagOnGround = false;
  _onGround = onGround;

  if(_filters.Size()>0)
  {
    CStatic *pict = dynamic_cast<CStatic *> (GetCtrl(IDC_GEAR_FILTER));
    pict->SetTexture(_filters[_activeFilter].image);
  }

  OnUnitChanged();
}

DisplayGear::~DisplayGear()
{
  _running--;
  if (_unit && _unit->GetUnit()) _unit->GetUnit()->SetGearActionProcessed(true);
}

const UnitWeaponsInfo *DisplayGear::GetCurrentInfo() const
{
  if (!_unit) return NULL;
  for (int i=0; i<_weaponsInfo._weapons.Size(); i++)
  {
    if (_weaponsInfo._weapons[i].unit == _unit) return &_weaponsInfo._weapons[i];
  }
  return NULL;
}

UnitWeaponsInfo *DisplayGear::GetCurrentInfo()
{
  if (!_unit) return NULL;
  for (int i=0; i<_weaponsInfo._weapons.Size(); i++)
  {
    if (_weaponsInfo._weapons[i].unit == _unit) return &_weaponsInfo._weapons[i];
  }
  return NULL;
}

Control *DisplayGear::OnCreateCtrl(int type, int idc, ParamEntryPar cls)
{
  // user controls for slots
  switch (idc)
  {
  case IDC_GEAR_SLOT_PRIMARY:
    {
      CItemSlot *ctrl = new CItemSlot(this, idc, cls);
      _slotPrimary = ctrl;
      return ctrl;
    }
  case IDC_GEAR_SLOT_SECONDARY:
    {
      CItemSlot *ctrl = new CItemSlot(this, idc, cls);
      _slotSecondary = ctrl;
      return ctrl;
    }
  case IDC_GEAR_SLOT_ITEM1:
  case IDC_GEAR_SLOT_ITEM2:
  case IDC_GEAR_SLOT_ITEM3:
  case IDC_GEAR_SLOT_ITEM4:
  case IDC_GEAR_SLOT_ITEM5:
  case IDC_GEAR_SLOT_ITEM6:
  case IDC_GEAR_SLOT_ITEM7:
  case IDC_GEAR_SLOT_ITEM8:
  case IDC_GEAR_SLOT_ITEM9:
  case IDC_GEAR_SLOT_ITEM10:
  case IDC_GEAR_SLOT_ITEM11:
  case IDC_GEAR_SLOT_ITEM12:
    {
      int index = idc - IDC_GEAR_SLOT_ITEM1;
      if (index < lenof(_slotItem))
      {
        CItemSlot *ctrl = new CItemSlot(this, idc, cls);
        _slotItem[index] = ctrl;
        return ctrl;
      }
      return NULL;
    }
  case IDC_GEAR_SLOT_HANDGUN:
    {
      CItemSlot *ctrl = new CItemSlot(this, idc, cls);
      _slotHandgun = ctrl;
      return ctrl;
    }
  case IDC_GEAR_SLOT_HANGUN_ITEM1:
  case IDC_GEAR_SLOT_HANGUN_ITEM2:
  case IDC_GEAR_SLOT_HANGUN_ITEM3:
  case IDC_GEAR_SLOT_HANGUN_ITEM4:
  case IDC_GEAR_SLOT_HANGUN_ITEM5:
  case IDC_GEAR_SLOT_HANGUN_ITEM6:
  case IDC_GEAR_SLOT_HANGUN_ITEM7:
  case IDC_GEAR_SLOT_HANGUN_ITEM8:
    {
      int index = idc - IDC_GEAR_SLOT_HANGUN_ITEM1;
      if (index < lenof(_slotHandgunItem))
      {
        CItemSlot *ctrl = new CItemSlot(this, idc, cls);
        _slotHandgunItem[index] = ctrl;
        return ctrl;
      }
      return NULL;
    }
  case IDC_GEAR_SLOT_SPECIAL1:
  case IDC_GEAR_SLOT_SPECIAL2:
    {
      CItemSlot *ctrl = new CItemSlot(this, idc, cls);
      _slotSpecial[idc - IDC_GEAR_SLOT_SPECIAL1] = ctrl;
      return ctrl;
    }
  case IDC_GEAR_SLOT_INVENTORY1:
  case IDC_GEAR_SLOT_INVENTORY2:
  case IDC_GEAR_SLOT_INVENTORY3:
  case IDC_GEAR_SLOT_INVENTORY4:
  case IDC_GEAR_SLOT_INVENTORY5:
  case IDC_GEAR_SLOT_INVENTORY6:
  case IDC_GEAR_SLOT_INVENTORY7:
  case IDC_GEAR_SLOT_INVENTORY8:
  case IDC_GEAR_SLOT_INVENTORY9:
  case IDC_GEAR_SLOT_INVENTORY10:
  case IDC_GEAR_SLOT_INVENTORY11:
  case IDC_GEAR_SLOT_INVENTORY12:
    {
      CItemSlot *ctrl = new CItemSlot(this, idc, cls);
      _slotInventory[idc - IDC_GEAR_SLOT_INVENTORY1] = ctrl;
      return ctrl;
    }
  case IDC_BAG_ITEMS_GROUP:
    {
      CControlsGroup *ctrlGroup = new CControlsGroup(this, idc, cls);
      if(_unit->GetVehicle() && _unit->GetVehicle()->GetBackpack())
      {
        int size =_unit->GetVehicle()->GetBackpack()->GetType()->GetMaxMagazinesCargo();
        CItemSlot *defslot = dynamic_cast<CItemSlot *>(GetCtrl(IDC_GEAR_SLOT_PRIMARY));

        if(defslot) for (int i =0; i < min(IDC_GEAR_BAG_FULL, size) ;i++)
        {
          CItemSlot *ctrl = new CItemSlot(this, IDC_GEAR_BAG_EMPTY + i, cls);
          _bagItems.Add(ctrl);
          ctrl->SetPos((i%8)*(_magW+_spacing),(i/8) * (_magH+_spacing),_magW, _magH);
          ctrl->SetStyle(defslot->GetStyle(), defslot->_color,defslot->_bgColor, defslot->_bgColorSelected, defslot->_colorFocused,false);

          ctrlGroup->AddControl(ctrl);
        }
      }
      ctrlGroup->ShowCtrl(false);
      return ctrlGroup;
    }
  }

  if (idc == IDC_GEAR_POOL)
  {
    _poolListBox = new GearPoolListBox(this, idc, cls);
    return _poolListBox;
  }

  Control *ctrl = Display::OnCreateCtrl(type, idc, cls);
  switch (idc)
  {
  case IDC_GEAR_SKILL:
    _skill = dynamic_cast<CStatic *>(ctrl);
    break;
  case IDC_GEAR_VEHICLE:
    _vehicle = dynamic_cast<CStatic *>(ctrl);
    break;
  case IDC_GEAR_OVERVIEW:
    _overview = GetHTMLContainer(ctrl);
    ctrl->EnableCtrl(false);
    break;
  case IDC_GEAR_OPEN_BAG:
  case IDC_GEAR_CLOSE_BAG:
    {
      ctrl->ShowCtrl(false);
      break;
    }
  case IDC_GEAR_REARM:
    {
      ctrl->ShowCtrl(false);
      break;
    }
  case IDC_GEAR_SOURCE_NAME:
    {
      ITextContainer *text = dynamic_cast<ITextContainer *>(ctrl);
      text->SetText(_pool.GetDisplayName());
      break;
    }
  }
  return ctrl;
}

/*!
\patch 5153 Date 4/12/2007 by Jirka
- Fixed: Gear dialog is closed when player dies
*/

void DisplayGear::OnSimulate(EntityAI *vehicle)
{
  if (GInput.GetActionToDo(UAGear))
  {
    Exit(IDC_CANCEL);
    return;
  }

  Display::OnSimulate(vehicle);

  // in MP, check if name of the units did not changed
  if (GWorld->GetMode() == GModeNetware)
  {
    // patch the names of the units
    for (int i=0; i<_weaponsInfo._weapons.Size(); i++)
    {
      UnitWeaponsInfo &info = _weaponsInfo._weapons[i];
      AIBrain *unit = info.unit;
      if (!unit) continue;
      Person *person = unit->GetPerson();
      if (!person) continue;
      info.name = person->GetPersonName();
    }

    // TODO: update the name
  }
  //mantain weapon info displayed
  GWorld->UI()->ShowWeaponInfo();

  // if in briefing, preload data for a mission
  bool inGame = _pool.IsInGame();
  if (!inGame)
  {
    GWorld->PreloadAroundCamera(0.030f); // keep reasonable framerate
  }
  if (GWorld->GetMode() == GModeNetware)
  {
    if (inGame)
    {
      if (GetNetworkManager().GetServerState() != NSSPlaying) Exit(IDC_CANCEL);
    }
    else
    {
      if (GetNetworkManager().GetServerState() != NSSBriefing) Exit(IDC_CANCEL);
    }
  }
  if (inGame)
  {
    // close when the unit is dead
    if (!_unit || !_unit->LSIsAlive())
    {
      Exit(IDC_CANCEL);
      return;
    }
  }
}

void DisplayGear::OnCtrlFocused(IControl *ctrl)
{
  int idc = ctrl->IDC();

  if(idc>=IDC_GEAR_BAG_EMPTY && idc<IDC_GEAR_BAG_FULL)
  {
    _defaultAction = ActionDrop;
    UpdateButtons();
    return;
  }

  switch (idc)
  {
  case IDC_GEAR_SLOT_PRIMARY:
    {
      const UnitWeaponsInfo *info = GetCurrentInfo();
      if (info)
      {
        UpdateOverview(info->weapons[0]);
        _defaultAction = ActionDrop;
        UpdateButtons();
      }
    }
    break;
  case IDC_GEAR_SLOT_SECONDARY:
    {
      const UnitWeaponsInfo *info = GetCurrentInfo();
      if (info)
      {
        UpdateOverview(info->weapons[1]);
        _defaultAction = ActionDrop;
        UpdateButtons();
      }
    }
    break;
  case IDC_GEAR_SLOT_ITEM1:
  case IDC_GEAR_SLOT_ITEM2:
  case IDC_GEAR_SLOT_ITEM3:
  case IDC_GEAR_SLOT_ITEM4:
  case IDC_GEAR_SLOT_ITEM5:
  case IDC_GEAR_SLOT_ITEM6:
  case IDC_GEAR_SLOT_ITEM7:
  case IDC_GEAR_SLOT_ITEM8:
  case IDC_GEAR_SLOT_ITEM9:
  case IDC_GEAR_SLOT_ITEM10:
  case IDC_GEAR_SLOT_ITEM11:
  case IDC_GEAR_SLOT_ITEM12:
    {
      const UnitWeaponsInfo *info = GetCurrentInfo();
      if (info && idc - IDC_GEAR_SLOT_ITEM1 < FIRST_HANDGUN_SLOT)
      {
        Magazine *magazine = info->magazines[idc - IDC_GEAR_SLOT_ITEM1];
        UpdateOverview(magazine ? magazine->_type : NULL);
        _defaultAction = ActionDrop;
        UpdateButtons();
      }
    }
    break;
  case IDC_GEAR_SLOT_HANDGUN:
    {
      const UnitWeaponsInfo *info = GetCurrentInfo();
      if (info)
      {
        UpdateOverview(info->weapons[4]);
        _defaultAction = ActionDrop;
        UpdateButtons();
      }
    }
    break;
  case IDC_GEAR_SLOT_HANGUN_ITEM1:
  case IDC_GEAR_SLOT_HANGUN_ITEM2:
  case IDC_GEAR_SLOT_HANGUN_ITEM3:
  case IDC_GEAR_SLOT_HANGUN_ITEM4:
  case IDC_GEAR_SLOT_HANGUN_ITEM5:
  case IDC_GEAR_SLOT_HANGUN_ITEM6:
  case IDC_GEAR_SLOT_HANGUN_ITEM7:
  case IDC_GEAR_SLOT_HANGUN_ITEM8:
    {
      const UnitWeaponsInfo *info = GetCurrentInfo();
      if (info && FIRST_HANDGUN_SLOT + idc - IDC_GEAR_SLOT_HANGUN_ITEM1 < MAGAZINE_SLOTS)
      {
        Magazine *magazine = info->magazines[FIRST_HANDGUN_SLOT + idc - IDC_GEAR_SLOT_HANGUN_ITEM1];
        UpdateOverview(magazine ? magazine->_type : NULL);
        _defaultAction = ActionDrop;
        UpdateButtons();
      }
    }
    break;
  case IDC_GEAR_SLOT_SPECIAL1:
  case IDC_GEAR_SLOT_SPECIAL2:
    {
      const UnitWeaponsInfo *info = GetCurrentInfo();
      if (info)
      {
        UpdateOverview(info->weapons[2 + idc - IDC_GEAR_SLOT_SPECIAL1]);
        _defaultAction = ActionDrop;
        UpdateButtons();
      }
    }
    break;
  case IDC_GEAR_SLOT_INVENTORY1:
  case IDC_GEAR_SLOT_INVENTORY2:
  case IDC_GEAR_SLOT_INVENTORY3:
  case IDC_GEAR_SLOT_INVENTORY4:
  case IDC_GEAR_SLOT_INVENTORY5:
  case IDC_GEAR_SLOT_INVENTORY6:
  case IDC_GEAR_SLOT_INVENTORY7:
  case IDC_GEAR_SLOT_INVENTORY8:
  case IDC_GEAR_SLOT_INVENTORY9:
  case IDC_GEAR_SLOT_INVENTORY10:
  case IDC_GEAR_SLOT_INVENTORY11:
  case IDC_GEAR_SLOT_INVENTORY12:
    {
      const UnitWeaponsInfo *info = GetCurrentInfo();
      if (info)
      {
        UpdateOverview(info->weapons[FIRST_INVENTORY_SLOT + idc - IDC_GEAR_SLOT_INVENTORY1]);
        _defaultAction = ActionDrop;
        UpdateButtons();
      }
    }
    break;
  case IDC_GEAR_UNIT:
    _defaultAction = ActionRearm;
    UpdateButtons();
    break;
  case IDC_GEAR_POOL:
  case IDC_GEAR_POOL_GROUP:
    _defaultAction = ActionTake;
    UpdateButtons();
    break;
  default:
    Display::OnCtrlFocused(ctrl);
    break;
  }
}

void DisplayGear::SelectWeapon(WeaponType *weapon)
{
  if(!_poolListBox) return;
  for(int i=0; i< _poolListBox->RowsCount(); i++)
  {
    if(_poolListBox->GetValue(i,_poolListBox->PrimaryColumn())==0 || _poolListBox->GetValue(i,_poolListBox->PrimaryColumn())==2) 
      if(strcmp(_poolListBox->GetData(i,_poolListBox->PrimaryColumn()),weapon->GetName())==0)
      {
        _poolListBox->SetCurSel(i);
        return;
      }
  }
}

void DisplayGear::SelectMagazine(MagazineType *type)
{
  if(!_poolListBox) return;
  for(int i=0; i< _poolListBox->RowsCount(); i++)
  {
    if(_poolListBox->GetValue(i,_poolListBox->PrimaryColumn()==1))
      if(strcmp(_poolListBox->GetData(i,_poolListBox->PrimaryColumn()),type->GetName())==0)
      {
        _poolListBox->SetCurSel(i);
        return;
      }
  }
}

void DisplayGear::SelectBackpack(RString  name)
{
  if(!_poolListBox) return;
  for(int i=0; i< _poolListBox->RowsCount(); i++)
  {
    if(_poolListBox->GetValue(i,_poolListBox->PrimaryColumn())==3) 
      if(strcmp(_poolListBox->GetData(i,_poolListBox->PrimaryColumn()),name)==0)
      {
        _poolListBox->SetCurSel(i);
        return;
      }
  }
}


void DisplayGear::OnButtonClicked(int idc)
{
  UnitWeaponsInfo *info = GetCurrentInfo();
  if (!info || !info->unit) return;

  WeaponType *weapon = NULL;
  Magazine *magazine = NULL;
  EntityAI *backpack = NULL;

  if(idc >= IDC_GEAR_BAG_EMPTY && idc<IDC_GEAR_BAG_FULL)
  {
    if (_selectedSlot) _selectedSlot->SetSelected(false);
    _selectedSlot = static_cast<CItemSlot *>(GetCtrl(idc));
    if (_selectedSlot) _selectedSlot->SetSelected(true);
    if(_selectedSlot->GetItemType() == 1) 
    {
      Ref<WeaponType> weapon = WeaponTypes.New(_selectedSlot->GetData());
      SelectWeapon(weapon);
    }
    else if(_selectedSlot->GetItemType() == 2) 
    {
      Ref<MagazineType> magazine = MagazineTypes.New(_selectedSlot->GetData());
      SelectMagazine(magazine);
    }
    UpdateButtons();
    return;
  }

  switch (idc)
  {
  case IDC_GEAR_SLOT_PRIMARY:
    weapon = info->weapons[0];
    goto selectItem;
  case IDC_GEAR_SLOT_SECONDARY:
    {
      if(info->weapons[1]) weapon = info->weapons[1];
      else if(info->HasBackpack()) backpack = info->backpack;
    }
    goto selectItem;
  case IDC_GEAR_SLOT_ITEM1:
  case IDC_GEAR_SLOT_ITEM2:
  case IDC_GEAR_SLOT_ITEM3:
  case IDC_GEAR_SLOT_ITEM4:
  case IDC_GEAR_SLOT_ITEM5:
  case IDC_GEAR_SLOT_ITEM6:
  case IDC_GEAR_SLOT_ITEM7:
  case IDC_GEAR_SLOT_ITEM8:
  case IDC_GEAR_SLOT_ITEM9:
  case IDC_GEAR_SLOT_ITEM10:
  case IDC_GEAR_SLOT_ITEM11:
  case IDC_GEAR_SLOT_ITEM12:
    if (idc - IDC_GEAR_SLOT_ITEM1 < FIRST_HANDGUN_SLOT)
      magazine = info->magazines[idc - IDC_GEAR_SLOT_ITEM1];
    goto selectItem;
  case IDC_GEAR_SLOT_HANDGUN:
    weapon = info->weapons[4];
    goto selectItem;
  case IDC_GEAR_SLOT_HANGUN_ITEM1:
  case IDC_GEAR_SLOT_HANGUN_ITEM2:
  case IDC_GEAR_SLOT_HANGUN_ITEM3:
  case IDC_GEAR_SLOT_HANGUN_ITEM4:
  case IDC_GEAR_SLOT_HANGUN_ITEM5:
  case IDC_GEAR_SLOT_HANGUN_ITEM6:
  case IDC_GEAR_SLOT_HANGUN_ITEM7:
  case IDC_GEAR_SLOT_HANGUN_ITEM8:
    if (FIRST_HANDGUN_SLOT + idc - IDC_GEAR_SLOT_HANGUN_ITEM1 < MAGAZINE_SLOTS)
      magazine = info->magazines[FIRST_HANDGUN_SLOT + idc - IDC_GEAR_SLOT_HANGUN_ITEM1];
    goto selectItem;
  case IDC_GEAR_SLOT_SPECIAL1:
  case IDC_GEAR_SLOT_SPECIAL2:
    weapon = info->weapons[2 + idc - IDC_GEAR_SLOT_SPECIAL1];
    goto selectItem;
  case IDC_GEAR_SLOT_INVENTORY1:
  case IDC_GEAR_SLOT_INVENTORY2:
  case IDC_GEAR_SLOT_INVENTORY3:
  case IDC_GEAR_SLOT_INVENTORY4:
  case IDC_GEAR_SLOT_INVENTORY5:
  case IDC_GEAR_SLOT_INVENTORY6:
  case IDC_GEAR_SLOT_INVENTORY7:
  case IDC_GEAR_SLOT_INVENTORY8:
  case IDC_GEAR_SLOT_INVENTORY9:
  case IDC_GEAR_SLOT_INVENTORY10:
  case IDC_GEAR_SLOT_INVENTORY11:
  case IDC_GEAR_SLOT_INVENTORY12:
    weapon = info->weapons[FIRST_INVENTORY_SLOT + idc - IDC_GEAR_SLOT_INVENTORY1];
selectItem:
    // select item
    {
      if (_selectedSlot) _selectedSlot->SetSelected(false);
      _selectedSlot = static_cast<CItemSlot *>(GetCtrl(idc));
      if (_selectedSlot) _selectedSlot->SetSelected(true);
      if(weapon) SelectWeapon(weapon);
      else if(magazine) SelectMagazine(magazine->_type);
      else if(backpack) SelectBackpack(backpack->Type()->GetName());
      UpdateButtons();
    }
    break;
  case IDC_OK:
    // Drop item
    switch (_defaultAction)
    {
    case ActionRearm:
      {
        UnitWeaponsInfo *info = GetCurrentInfo();
        if (!info) break;
        _pool.Rearm(*info);
        UpdateWeapons();
      }
      break;
    case ActionTake:
      if (_poolListBox)
      {
        int curSel = _poolListBox->GetCurSel();
        if (curSel < 0) break;
        UnitWeaponsInfo *info = GetCurrentInfo();
        if (!info) break;
        if (!_pool.CanReplace(info->unit)) break;
        RString name = _poolListBox->GetData(curSel,_poolListBox->PrimaryColumn());
        if (_poolListBox->GetValue(curSel,_poolListBox->PrimaryColumn()) == 0 || _poolListBox->GetValue(curSel,_poolListBox->PrimaryColumn()) == 2)
        {
          // weapon
          Ref<WeaponType> weapon = WeaponTypes.New(name);
          _pool.AddWeapon(*info, weapon, _openedBag);
        }
        else if (_poolListBox->GetValue(curSel,_poolListBox->PrimaryColumn()) == 1)
        {
          // magazine
          Ref<MagazineType> magazine = MagazineTypes.New(name);
          _pool.AddMagazine(*info, magazine, _openedBag);
        }
        else if (_poolListBox->GetValue(curSel,_poolListBox->PrimaryColumn()) == 3)
        {
          // backpack
          if(info->unit && info->unit->GetPerson() && info->unit->GetPerson()->CanCarryBackpack())
            _pool.AddBackpack(*info, name);
        }
        UpdateWeapons();
      }
      break;
    case ActionDrop:
      if (_selectedSlot)
      {
        DropItem(_selectedSlot->IDC());
        UpdateWeapons();
      }
      break;
    }
    break;
  case IDC_GEAR_CONTINUE:
    if (_forced) Exit(IDC_OK);
    break;
  case IDC_GEAR_REMOVE_ITEM:
    {//remove from ammo box, add to player
      int curSel = _poolListBox->GetCurSel();
      if (curSel < 0) break;
      UnitWeaponsInfo *info = GetCurrentInfo();
      if (!info) break;
      if (!_pool.CanReplace(info->unit)) break;
      RString name = _poolListBox->GetData(curSel,_poolListBox->PrimaryColumn());
      if (_poolListBox->GetValue(curSel,_poolListBox->PrimaryColumn()) == 0 || _poolListBox->GetValue(curSel,_poolListBox->PrimaryColumn()) == 2)
      {
        // weapon
        Ref<WeaponType> weapon = WeaponTypes.New(name);
        _pool.AddWeapon(*info, weapon, _openedBag);
      }
      else if (_poolListBox->GetValue(curSel,_poolListBox->PrimaryColumn()) == 1)
      {
        // magazine
        Ref<MagazineType> magazine = MagazineTypes.New(name);
        _pool.AddMagazine(*info, magazine, _openedBag);
      }
      else if (_poolListBox->GetValue(curSel,_poolListBox->PrimaryColumn()) == 3)
      {
        if(info->unit && info->unit->GetPerson() && info->unit->GetPerson()->CanCarryBackpack())
          _pool.AddBackpack(*info,name);
      }
      UpdateWeapons();
    }
    break;
  case IDC_GEAR_ADD_ITEM:
    {
      int curSel = _poolListBox->GetCurSel();
      if (curSel < 0) break;
      UnitWeaponsInfo *info = GetCurrentInfo();
      if (!info) break;
      if(!_pool.CanDrop(info->unit)) break;
      RString name = _poolListBox->GetData(curSel,_poolListBox->PrimaryColumn());

      if (_poolListBox->GetValue(curSel,_poolListBox->PrimaryColumn()) == 0 || _poolListBox->GetValue(curSel,_poolListBox->PrimaryColumn()) == 2)
      {
        // weapon
        Ref<WeaponType> weapon = WeaponTypes.New(name);
        _pool.DropWeapon(*info,weapon, _openedBag);
      }
      else  if (_poolListBox->GetValue(curSel,_poolListBox->PrimaryColumn()) == 1)
      {
        // magazine
        Ref<MagazineType> magazine = MagazineTypes.New(name);

        if(!_openedBag)
        {
          for(int i=0; i< MAGAZINE_SLOTS; i++)
          {
            Magazine *mag =info->magazines[i];
            if(mag && magazine == mag->_type)
            {
              _pool.DropMagazine(*info,mag, _openedBag);
            }
          }
        }
        else
        {
          if(info->HasBackpack() && info->backpack->GetSupply())
          {
            const Magazine *mag = info->backpack->GetSupply()->FindMagazine(magazine);
            if(mag && magazine == mag->_type)
            {
              _pool.DropMagazine(*info, const_cast<Magazine *>(mag), _openedBag);
            }
          }
        }
      }
      else if (_poolListBox->GetValue(curSel,_poolListBox->PrimaryColumn()) == 3)
      {
        if(!_onGround)
        {
          if(info->unit && info->unit->GetPerson())
            _pool.DropBackpack(*info,name);
        }
        else if(info->unit->GetPerson()) info->unit->GetPerson()->DropBackpack();
      }
      UpdateWeapons();
    }
    break;
  case IDC_GEAR_FILTER_ICON2:
    {
      if(_filters.Size()>0) _activeFilter = (_filters.Size() + _activeFilter-1)%_filters.Size();
      UpdateWeapons();

      if(_filters.Size()>0)
      {
        CStatic *pict = dynamic_cast<CStatic *> (GetCtrl(IDC_GEAR_FILTER));
        pict->SetTexture(_filters[_activeFilter].image);
      }
      break;
    }
  case IDC_GEAR_FILTER:
  case IDC_GEAR_FILTER_ICON1:
  case IDC_GEAR_FILTER_ICON3:
  case IDC_GEAR_FILTER_ICON4:
  case IDC_GEAR_FILTER_ICON5:
  case IDC_GEAR_FILTER_ICON6:      
    {
      if(_filters.Size()>0) _activeFilter = (_activeFilter+1)%_filters.Size();
      UpdateWeapons();

      if(_filters.Size()>0)
      {
        CStatic *pict = dynamic_cast<CStatic *> (GetCtrl(IDC_GEAR_FILTER));
        pict->SetTexture(_filters[_activeFilter].image);
      }
      break;
    }
  case IDC_GEAR_REARM:
    {
      UnitWeaponsInfo *info = GetCurrentInfo();
      if (!info) break;
      _pool.Rearm(*info);
      UpdateWeapons();
      break;
    }
  case IDC_GEAR_OPEN_BAG:
    {
      UnitWeaponsInfo *info = GetCurrentInfo();
      if (!info) break;
      if(!_onGround)
      {
        _openedBag = true;
      
        IControl *ctrl = GetCtrl(IDC_UNIT_ITEMS_GROUP);
        if(ctrl) ctrl->ShowCtrl(false);
        ctrl = GetCtrl(IDC_BAG_ITEMS_GROUP);
        if(ctrl) ctrl->ShowCtrl(true);
      }
      else
      {
       _openedBagOnGround = true;
       _pool.SwitchSources();
      }

      _poolListBox->SetCurSel(0);
      UpdateWeapons();
      break;
    }
  case IDC_GEAR_CLOSE_BAG:
    {
      UnitWeaponsInfo *info = GetCurrentInfo();
      if (!info) break;
      _openedBag = false;
      if(_openedBagOnGround) _pool.SwitchSources();
      _openedBagOnGround = false;

      IControl *ctrl = GetCtrl(IDC_BAG_ITEMS_GROUP);
      if(ctrl) ctrl->ShowCtrl(false);
      ctrl = GetCtrl(IDC_UNIT_ITEMS_GROUP);
      if(ctrl) ctrl->ShowCtrl(true);
      ctrl = GetCtrl(IDC_GEAR_CLOSE_BAG);
      if(ctrl) ctrl->ShowCtrl(false);

      _poolListBox->SetCurSel(0);
      UpdateWeapons();
      break;
    }
  default:
    Display::OnButtonClicked(idc);
    break;
  }
}

void DisplayGear::OnButtonDblClick(int idc)
{
  UnitWeaponsInfo *info = GetCurrentInfo();
  if (!info) return;
  if(idc == IDC_GEAR_SLOT_SECONDARY && info->HasBackpack()) 
  {
    UnitWeaponsInfo *info = GetCurrentInfo();
    if(!info) return;
    if(!info->backpack->GetSupply()) return;
    if(_pool.GetSource() == info->backpack)  return;
    if(!_onGround )
    {
      _openedBag = true;
    
      IControl *ctrl = GetCtrl(IDC_UNIT_ITEMS_GROUP);
      if(ctrl) ctrl->ShowCtrl(false);
      ctrl = GetCtrl(IDC_BAG_ITEMS_GROUP);
      if(ctrl) ctrl->ShowCtrl(true);
    }
    else
    {
      _openedBagOnGround = true;
      _pool.SwitchSources();
    }

    UpdateWeapons();
    return;
  }
  else DropItem(idc);

  UpdateWeapons();
}

void DisplayGear::DropItem(int idc)
{
  UnitWeaponsInfo *info = GetCurrentInfo();
  if (!info) return;
  if (!_pool.CanDrop(info->unit)) return;

  if(idc >= IDC_GEAR_BAG_EMPTY && idc<IDC_GEAR_BAG_FULL)
  {
    if(_selectedSlot->GetItemType() == 1) 
    {
      Ref<WeaponType> weapon = WeaponTypes.New(_selectedSlot->GetData());
      if (weapon) _pool.DropWeapon(*info, weapon, _openedBag);
    }
    else if(_selectedSlot->GetItemType() == 2) 
    {
      Ref<MagazineType> magazine = MagazineTypes.New(_selectedSlot->GetData());
      if(info->HasBackpack() && info->backpack->GetSupply())
      {
        const Magazine *mag = info->backpack->GetSupply()->FindMagazine(magazine);
        if (magazine) _pool.DropMagazine(*info, const_cast<Magazine *>(mag), _openedBag);
      }
    }
    UpdateButtons();
  }

  switch (idc)
  {
  case IDC_GEAR_SLOT_PRIMARY:
    {
      WeaponType *weapon = info->weapons[0];
      if (weapon) _pool.DropWeapon(*info, weapon, false);
    }
    break;
  case IDC_GEAR_SLOT_SECONDARY:
    {
      WeaponType *weapon = info->weapons[1];
      if (weapon) _pool.DropWeapon(*info, weapon, false);
      else if(info->HasBackpack()) 
      {
        if(!_onGround)
        {
          if(info->unit && info->unit->GetPerson())
            _pool.DropBackpack(*info,info->backpack->GetType()->GetName());
        }
        else if(info->unit->GetPerson()) info->unit->GetPerson()->DropBackpack();
      }
    }
    break;
  case IDC_GEAR_SLOT_ITEM1:
  case IDC_GEAR_SLOT_ITEM2:
  case IDC_GEAR_SLOT_ITEM3:
  case IDC_GEAR_SLOT_ITEM4:
  case IDC_GEAR_SLOT_ITEM5:
  case IDC_GEAR_SLOT_ITEM6:
  case IDC_GEAR_SLOT_ITEM7:
  case IDC_GEAR_SLOT_ITEM8:
  case IDC_GEAR_SLOT_ITEM9:
  case IDC_GEAR_SLOT_ITEM10:
  case IDC_GEAR_SLOT_ITEM11:
  case IDC_GEAR_SLOT_ITEM12:
    if (idc - IDC_GEAR_SLOT_ITEM1 < FIRST_HANDGUN_SLOT)
    {
      Magazine *magazine = info->magazines[idc - IDC_GEAR_SLOT_ITEM1];
      if (magazine) _pool.DropMagazine(*info, magazine, false);
    }
    break;
  case IDC_GEAR_SLOT_HANDGUN:
    {
      WeaponType *weapon = info->weapons[4];
      if (weapon) _pool.DropWeapon(*info, weapon, false);
    }
    break;
  case IDC_GEAR_SLOT_HANGUN_ITEM1:
  case IDC_GEAR_SLOT_HANGUN_ITEM2:
  case IDC_GEAR_SLOT_HANGUN_ITEM3:
  case IDC_GEAR_SLOT_HANGUN_ITEM4:
  case IDC_GEAR_SLOT_HANGUN_ITEM5:
  case IDC_GEAR_SLOT_HANGUN_ITEM6:
  case IDC_GEAR_SLOT_HANGUN_ITEM7:
  case IDC_GEAR_SLOT_HANGUN_ITEM8:
    if (FIRST_HANDGUN_SLOT + idc - IDC_GEAR_SLOT_HANGUN_ITEM1 < MAGAZINE_SLOTS)
    {
      Magazine *magazine = info->magazines[FIRST_HANDGUN_SLOT + idc - IDC_GEAR_SLOT_HANGUN_ITEM1];
      if (magazine) _pool.DropMagazine(*info, magazine, false);
    }
    break;
  case IDC_GEAR_SLOT_SPECIAL1:
  case IDC_GEAR_SLOT_SPECIAL2:
    {
      WeaponType *weapon = info->weapons[2 + idc - IDC_GEAR_SLOT_SPECIAL1];
      if (weapon) _pool.DropWeapon(*info, weapon, false);
    }
    break;
  case IDC_GEAR_SLOT_INVENTORY1:
  case IDC_GEAR_SLOT_INVENTORY2:
  case IDC_GEAR_SLOT_INVENTORY3:
  case IDC_GEAR_SLOT_INVENTORY4:
  case IDC_GEAR_SLOT_INVENTORY5:
  case IDC_GEAR_SLOT_INVENTORY6:
  case IDC_GEAR_SLOT_INVENTORY7:
  case IDC_GEAR_SLOT_INVENTORY8:
  case IDC_GEAR_SLOT_INVENTORY9:
  case IDC_GEAR_SLOT_INVENTORY10:
  case IDC_GEAR_SLOT_INVENTORY11:
  case IDC_GEAR_SLOT_INVENTORY12:
    {
      WeaponType *weapon = info->weapons[FIRST_INVENTORY_SLOT + idc - IDC_GEAR_SLOT_INVENTORY1];
      if (weapon) _pool.DropWeapon(*info, weapon, false);
    }
    break;
  }
}

void DisplayGear::OnLBSelChanged(IControl *ctrl, int curSel)
{
  switch (ctrl->IDC())
  {
    /*
    case IDC_GEAR_UNIT:
    UpdateWeapons();
    break;
    */
  case IDC_GEAR_POOL:
    if (_poolListBox && curSel >= 0)
    {
      RString name = _poolListBox->GetData(curSel,_poolListBox->PrimaryColumn());
      if (_poolListBox->GetValue(curSel,_poolListBox->PrimaryColumn()) == 0 || _poolListBox->GetValue(curSel,_poolListBox->PrimaryColumn()) == 2)
      {
        // weapon
        Ref<WeaponType> weapon = WeaponTypes.New(name);
        UpdateOverview(weapon);
      }
      else if (_poolListBox->GetValue(curSel,_poolListBox->PrimaryColumn()) == 1)
      {
        // magazine
        Ref<MagazineType> magazine = MagazineTypes.New(name);
        UpdateOverview(magazine);
      }
      else if (_poolListBox->GetValue(curSel,_poolListBox->PrimaryColumn()) == 3)
      {
        // magazine
        UpdateOverview(name);
      }
    }
    break;
  default:
    Display::OnLBSelChanged(ctrl, curSel);
    break;
  }
  UpdateButtons();
}

void DisplayGear::OnLBDblClick(int idc, int curSel)
{
  switch (idc)
  {
  case IDC_GEAR_POOL:
    /*
    case IDC_GEAR_UNIT:
    */
    OnButtonClicked(IDC_GEAR_REMOVE_ITEM);
    break;
  default:
    Display::OnLBDblClick(idc, curSel);
    break;
  }
}

bool DisplayGear::OnLBDragging(IControl *ctrl, float x, float y)
{
  if (_dragCtrl == IDC_GEAR_POOL && _dragRows.Size() == 1 && ctrl)
  {
    UnitWeaponsInfo *info = GetCurrentInfo();
    if (!info) return false;
    if (!_pool.CanReplace(info->unit)) return false;

    int idc = ctrl->IDC();
    if(idc == IDC_BAG_ITEMS_GROUP || idc == IDC_UNIT_ITEMS_GROUP)
    {
      CControlsGroup *group = static_cast<CControlsGroup *>(ctrl);
      if(group)
      {
        IControl *ctrlTmp = group->GetCtrl(x,y);
        if(ctrlTmp) idc = ctrlTmp->IDC();
      }
    }

    RString name = _dragRows[0].data;
    if (_dragRows[0].value == 0)
    {
      // weapon
      Ref<WeaponType> weapon = WeaponTypes.New(name);
      int mask = weapon->_weaponType;
      switch (idc)
      {
      case IDC_GEAR_SLOT_PRIMARY:
        return (mask & MaskSlotPrimary) != 0;
      case IDC_GEAR_SLOT_SECONDARY:
        return (mask & MaskSlotSecondary) != 0;
      case IDC_GEAR_SLOT_HANDGUN:
        return (mask & MaskSlotHandGun) != 0;
      case IDC_GEAR_SLOT_SPECIAL1:
      case IDC_GEAR_SLOT_SPECIAL2:
        return (mask & MaskSlotBinocular) != 0;
      case IDC_GEAR_SLOT_INVENTORY1:
      case IDC_GEAR_SLOT_INVENTORY2:
      case IDC_GEAR_SLOT_INVENTORY3:
      case IDC_GEAR_SLOT_INVENTORY4:
      case IDC_GEAR_SLOT_INVENTORY5:
      case IDC_GEAR_SLOT_INVENTORY6:
      case IDC_GEAR_SLOT_INVENTORY7:
      case IDC_GEAR_SLOT_INVENTORY8:
      case IDC_GEAR_SLOT_INVENTORY9:
      case IDC_GEAR_SLOT_INVENTORY10:
      case IDC_GEAR_SLOT_INVENTORY11:
      case IDC_GEAR_SLOT_INVENTORY12:
        return (mask & MaskSlotInventory) != 0;
      }

      if(idc >= IDC_GEAR_BAG_EMPTY && idc<IDC_GEAR_BAG_FULL) return true;
    }
    else if (_dragRows[0].value == 1)
    {
      // magazine
      Ref<MagazineType> magazine = MagazineTypes.New(name);
      int mask = magazine->_magazineType;
      switch (idc)
      {
      case IDC_GEAR_SLOT_ITEM1:
      case IDC_GEAR_SLOT_ITEM2:
      case IDC_GEAR_SLOT_ITEM3:
      case IDC_GEAR_SLOT_ITEM4:
      case IDC_GEAR_SLOT_ITEM5:
      case IDC_GEAR_SLOT_ITEM6:
      case IDC_GEAR_SLOT_ITEM7:
      case IDC_GEAR_SLOT_ITEM8:
      case IDC_GEAR_SLOT_ITEM9:
      case IDC_GEAR_SLOT_ITEM10:
      case IDC_GEAR_SLOT_ITEM11:
      case IDC_GEAR_SLOT_ITEM12:
        return (mask & MaskSlotItem) != 0;
      case IDC_GEAR_SLOT_HANGUN_ITEM1:
      case IDC_GEAR_SLOT_HANGUN_ITEM2:
      case IDC_GEAR_SLOT_HANGUN_ITEM3:
      case IDC_GEAR_SLOT_HANGUN_ITEM4:
      case IDC_GEAR_SLOT_HANGUN_ITEM5:
      case IDC_GEAR_SLOT_HANGUN_ITEM6:
      case IDC_GEAR_SLOT_HANGUN_ITEM7:
      case IDC_GEAR_SLOT_HANGUN_ITEM8:
        return (mask & MaskSlotHandGunItem) != 0;
      }

      if(idc >= IDC_GEAR_BAG_EMPTY && idc<IDC_GEAR_BAG_FULL) return true;
    }
    else if (_dragRows[0].value == 2)
    {
      // backpack
      if (idc == IDC_GEAR_SLOT_SECONDARY)
        return true;
      else 
        return false;
    }
  }

  return Display::OnLBDragging(ctrl, x, y);
}

bool DisplayGear::OnLBDrop(IControl *ctrl, float x, float y)
{
  if (_dragCtrl == IDC_GEAR_POOL && _dragRows.Size() == 1 && ctrl)
  {
    UnitWeaponsInfo *info = GetCurrentInfo();
    if (!info) return false;
    if (!_pool.CanReplace(info->unit)) return false;

    int idc = ctrl->IDC();
    if(idc == IDC_BAG_ITEMS_GROUP || idc == IDC_UNIT_ITEMS_GROUP)
    {
      CControlsGroup *group = static_cast<CControlsGroup *>(ctrl);
      if(group)
      {
        IControl *ctrlTmp = group->GetCtrl(x,y);
        if(ctrlTmp) idc = ctrlTmp->IDC();
      }
    }

    RString name = _dragRows[0].data;
    if (_dragRows[0].value == 0)
    {
      // weapon
      Ref<WeaponType> weapon = WeaponTypes.New(name);
      int mask = weapon->_weaponType;

      if(idc >= IDC_GEAR_BAG_EMPTY && idc<IDC_GEAR_BAG_FULL)
      {
        _pool.ReplaceWeapon(*info, 0, weapon, true);
        UpdateWeapons();
        return true;
      }

      switch (idc)
      {
      case IDC_GEAR_SLOT_PRIMARY:
        if ((mask & MaskSlotPrimary) != 0)
        {
          _pool.ReplaceWeapon(*info, 0, weapon, false);
          UpdateWeapons();
          return true;
        }
        return false;
      case IDC_GEAR_SLOT_SECONDARY:
        if ((mask & MaskSlotSecondary) != 0)
        {
          _pool.ReplaceWeapon(*info, 1, weapon, false);
          UpdateWeapons();
          return true;
        }
        return false;
      case IDC_GEAR_SLOT_HANDGUN:
        if ((mask & MaskSlotHandGun) != 0)
        {
          _pool.ReplaceWeapon(*info, 4, weapon, false);
          UpdateWeapons();
          return true;
        }
        return false;
      case IDC_GEAR_SLOT_SPECIAL1:
      case IDC_GEAR_SLOT_SPECIAL2:
        if ((mask & MaskSlotBinocular) != 0)
        {
          _pool.ReplaceWeapon(*info, 2 + idc - IDC_GEAR_SLOT_SPECIAL1, weapon, false);
          UpdateWeapons();
          return true;
        }
        return false;
      case IDC_GEAR_SLOT_INVENTORY1:
      case IDC_GEAR_SLOT_INVENTORY2:
      case IDC_GEAR_SLOT_INVENTORY3:
      case IDC_GEAR_SLOT_INVENTORY4:
      case IDC_GEAR_SLOT_INVENTORY5:
      case IDC_GEAR_SLOT_INVENTORY6:
      case IDC_GEAR_SLOT_INVENTORY7:
      case IDC_GEAR_SLOT_INVENTORY8:
      case IDC_GEAR_SLOT_INVENTORY9:
      case IDC_GEAR_SLOT_INVENTORY10:
      case IDC_GEAR_SLOT_INVENTORY11:
      case IDC_GEAR_SLOT_INVENTORY12:
        if ((mask & MaskSlotInventory) != 0)
        {
          _pool.ReplaceWeapon(*info, FIRST_INVENTORY_SLOT + idc - IDC_GEAR_SLOT_INVENTORY1, weapon, false);
          UpdateWeapons();
          return true;
        }
        return false;
      }

    }
    else if (_dragRows[0].value == 1)
    {
      // magazine
      Ref<MagazineType> magazine = MagazineTypes.New(name);
      int mask = magazine->_magazineType;

      if(idc >= IDC_GEAR_BAG_EMPTY && idc<IDC_GEAR_BAG_FULL)
      {
        _pool.ReplaceMagazine(*info, 0, magazine, true);
        UpdateWeapons();
        return true;
      }

      switch (idc)
      {
      case IDC_GEAR_SLOT_ITEM1:
      case IDC_GEAR_SLOT_ITEM2:
      case IDC_GEAR_SLOT_ITEM3:
      case IDC_GEAR_SLOT_ITEM4:
      case IDC_GEAR_SLOT_ITEM5:
      case IDC_GEAR_SLOT_ITEM6:
      case IDC_GEAR_SLOT_ITEM7:
      case IDC_GEAR_SLOT_ITEM8:
      case IDC_GEAR_SLOT_ITEM9:
      case IDC_GEAR_SLOT_ITEM10:
      case IDC_GEAR_SLOT_ITEM11:
      case IDC_GEAR_SLOT_ITEM12:
        if ((mask & MaskSlotItem) != 0)
        {
          _pool.ReplaceMagazine(*info, idc - IDC_GEAR_SLOT_ITEM1, magazine, false);
          UpdateWeapons();
          return true;
        }
        return false;
      case IDC_GEAR_SLOT_HANGUN_ITEM1:
      case IDC_GEAR_SLOT_HANGUN_ITEM2:
      case IDC_GEAR_SLOT_HANGUN_ITEM3:
      case IDC_GEAR_SLOT_HANGUN_ITEM4:
      case IDC_GEAR_SLOT_HANGUN_ITEM5:
      case IDC_GEAR_SLOT_HANGUN_ITEM6:
      case IDC_GEAR_SLOT_HANGUN_ITEM7:
      case IDC_GEAR_SLOT_HANGUN_ITEM8:
        if ((mask & MaskSlotHandGunItem) != 0)
        {
          _pool.ReplaceMagazine(*info, FIRST_HANDGUN_SLOT + idc - IDC_GEAR_SLOT_HANGUN_ITEM1, magazine,false);
          UpdateWeapons();
          return true;
        }
        return false;
      }
    }
    else if (_dragRows[0].value == 2)
    {
      switch (idc)
      {
      case IDC_GEAR_SLOT_SECONDARY:
        {
          _pool.ReplaceBackpack(*info, name);
          UpdateWeapons();
          return true;
        }
      }
      return false;
    }
  }

  return Display::OnLBDrop(ctrl, x, y);
}

void DisplayGear::OnHTMLLink(int idc, RString link)
{
  Display::OnHTMLLink(idc, link);
}

/*!
\patch_internal 1.06 Date 7/19/2001 by Jirka
- Added: enable briefing pictures for weapons in AddOns
*/

static RString GetWeaponPicture(const WeaponType *weapon)
{
  RString name = weapon->GetPictureName();
  if (name[0] == '\\')
    return name;
  else
  {
    RString prefixW = "equip\\w\\w_";
    return prefixW + name + RString(".paa");
  }
}


static RString GetMagazinePicture(const MagazineType *type)
{
  RString name = type->GetPictureName();
  if (name[0] == '\\')
    return name;
  else
  {
    RString prefixM = "equip\\m\\m_";
    return prefixM + name + RString(".paa");
  }
}

/*!
\patch 5153 Date 4/13/2007 by Jirka
- Fixed: UI - Gear display - icons for empty slots
*/

bool DisplayGear::CheckOwner(EntityAI *owner)
{
  if (!owner) return true;

  const UnitWeaponsInfo *info = GetCurrentInfo();
  if (!info) return false;

  AIBrain *unit = info->unit;

  //check if we care about the changes
  if(  (unit && unit->GetVehicle() && owner== info->unit->GetVehicle()) 
    || (info->backpack && owner== info->backpack)
    || (_pool.GetSource() && owner== _pool.GetSource())) return true;

  return false;
}

void DisplayGear::OnUnitChanged()
{
  const UnitWeaponsInfo *info = GetCurrentInfo();
  if (!info) return;

  AIBrain *unit = info->unit;

  ResourceSupply *supply = NULL;
  if(info->HasBackpack() && info->backpack->GetSupply()) supply = info->backpack->GetSupply();

  if (_skill)
  {
    Texture *texture = NULL;
    if (unit)
    {
      texture = unit->GetPerson()->GetPicture();
    }
    _skill->SetTexture(texture);
  }
  if (_vehicle)
  {
    Texture *texture = NULL;
    if (unit && unit->GetVehicleIn())
    {
      texture = unit->GetVehicleIn()->GetPicture();
    }
    _vehicle->SetTexture(texture);
  }

  // slot controls

  if(!_openedBag)
  {
    // primary weapon
    if (_slotPrimary)
    {
      if (info->weaponSlots & MaskSlotPrimary)
      {
        _slotPrimary->GetControl()->ShowCtrl(true);

        const WeaponType *weapon = info->weapons[0];
        RString img = weapon ? GetWeaponPicture(weapon) : _emptyGun;
        _slotPrimary->SetText(img);
        if(weapon) _slotPrimary->SetData(weapon->GetName());
        else _slotPrimary->SetData("");
      }
      else
      {
        _slotPrimary->GetControl()->ShowCtrl(false);
      }
    }
    // secondary weapon
    if (_slotSecondary)
    {
      if ((info->weaponSlots & MaskSlotSecondary) && !(info->weapons[1] && info->weapons[1] == info->weapons[0]))
      {
        _slotSecondary->GetControl()->ShowCtrl(true);

        const WeaponType *weapon = info->weapons[1];
        RString img = weapon ? GetWeaponPicture(weapon) : _emptySec;
        _slotSecondary->SetText(img);
        if(weapon) _slotSecondary->SetData(weapon->GetName());
        else _slotSecondary->SetData("");

        if(_slotSecondary && _unit && _unit->GetVehicle())
        {
          if(info->HasBackpack())
          {
            _slotSecondary->SetTexture(info->backpack->GetPicture(),info->backpack->GetType()->GetName());
            _slotSecondary->SetData(info->backpack->GetType()->GetName());
          }
        }
      }
      else
      {
        _slotSecondary->GetControl()->ShowCtrl(false);
      }
    }
    // hand gun
    if (_slotHandgun)
    {
      if (info->weaponSlots & MaskSlotHandGun)
      {
        _slotHandgun->GetControl()->ShowCtrl(true);

        const WeaponType *weapon = info->weapons[4];
        RString img = weapon ? GetWeaponPicture(weapon) : _emptyHGun;
        _slotHandgun->SetText(img);
        if(weapon) _slotHandgun->SetData(weapon->GetName());
        else _slotHandgun->SetData("");
      }
      else
      {
        _slotHandgun->GetControl()->ShowCtrl(false);
      }
    }
    // special items
    for (int i=0; i<2; i++)
    {
      if (_slotSpecial[i])
      {
        if (info->weaponSlots & MaskSlotBinocular)
        {
          _slotSpecial[i]->GetControl()->ShowCtrl(true);

          const WeaponType *weapon = info->weapons[2 + i];
          RString img = weapon ? GetWeaponPicture(weapon) : _emptyEq;
          _slotSpecial[i]->SetText(img);
          if(weapon) _slotSpecial[i]->SetData(weapon->GetName());
          else  _slotSpecial[i]->SetData("");
        }
        else
        {
          _slotSpecial[i]->GetControl()->ShowCtrl(false);
        }
      }
    }
    // inventory items
    for (int i=0; i<WEAPON_SLOTS-FIRST_INVENTORY_SLOT; i++)
    {
      if (_slotInventory[i])
      {
        if (info->weaponSlots & MaskSlotInventory)
        {
          _slotInventory[i]->GetControl()->ShowCtrl(true);

          const WeaponType *weapon = info->weapons[FIRST_INVENTORY_SLOT + i];
          RString img = weapon ? GetWeaponPicture(weapon) : _emptyEq;
          _slotInventory[i]->SetText(img);
          if(weapon) _slotInventory[i]->SetData(weapon->GetName());
          else _slotInventory[i]->SetData("");
        }
        else
        {
          _slotInventory[i]->GetControl()->ShowCtrl(false);
        }
      }
    }
    // magazines
    int maxItems = GetItemSlotsCount(info->weaponSlots);
    if (maxItems > FIRST_HANDGUN_SLOT)
    {
      LogF("Magazines don't fit to slots");
      maxItems = FIRST_HANDGUN_SLOT;
    }
    int nextValid = 0;
    for (int i=0; i<lenof(_slotItem); i++)
    {
      if (_slotItem[i])
      {
        if (i >= maxItems || i < nextValid)
        {
          _slotItem[i]->GetControl()->ShowCtrl(false);
        }
        else
        {
          _slotItem[i]->GetControl()->ShowCtrl(true);

          Magazine *magazine = info->magazines[i];
          if (magazine)
          {
            nextValid = i + GetItemSlotsCount(magazine->_type->_magazineType);
            RString img = GetMagazinePicture(magazine->_type);
            _slotItem[i]->SetText(img);
            _slotItem[i]->SetData(magazine->_type->GetName());
          }
          else
          {
            nextValid = i + 1;
            _slotItem[i]->SetText(i < FIRST_SECONDARY_SLOT ? _emptyMag : _emptyMag2);
            _slotItem[i]->SetData("");
          }
        }
      }
    }
    // handgun magazines
    int maxItemsHG = GetHandGunItemSlotsCount(info->weaponSlots);
    if (FIRST_HANDGUN_SLOT + maxItemsHG > MAGAZINE_SLOTS)
    {
      LogF("Magazines don't fit to slots");
      maxItemsHG = MAGAZINE_SLOTS - FIRST_HANDGUN_SLOT;
    }
    nextValid = 0;
    for (int i=0; i<lenof(_slotHandgunItem); i++)
    {
      if (_slotHandgunItem[i])
      {
        if (i >= maxItemsHG || i < nextValid)
        {
          _slotHandgunItem[i]->GetControl()->ShowCtrl(false);
        }
        else
        {
          _slotHandgunItem[i]->GetControl()->ShowCtrl(true);

          Magazine *magazine = info->magazines[FIRST_HANDGUN_SLOT + i];
          if (magazine)
          {
            nextValid = i + GetItemSlotsCount(magazine->_type->_magazineType);
            RString img = GetMagazinePicture(magazine->_type);
            _slotHandgunItem[i]->SetText(img);
            _slotHandgunItem[i]->SetData(magazine->_type->GetName());
          }
          else
          {
            nextValid = i + 1;
            _slotHandgunItem[i]->SetText(_emptyHGunMag);
            _slotHandgunItem[i]->SetData("");
          }
        }
      }
    }
  }

  //backpack items (automaticly generated CItems)
  {
    if (_openedBag && supply)
    {
      int index = 0; 
      int pos = 0;
      float currHeight = 0;
      float nextHeight = _magH + _spacing;

      int bagSize =info->backpack->GetType()->GetMaxMagazinesCargo();
      if(_bagItems.Size() != min(IDC_GEAR_BAG_FULL, bagSize))
      {//need to reload bag slots
        CControlsGroup *ctrlGroup =  dynamic_cast<CControlsGroup *>(GetCtrl(IDC_BAG_ITEMS_GROUP));
        CItemSlot *defslot = dynamic_cast<CItemSlot *>(GetCtrl(IDC_GEAR_SLOT_PRIMARY));
        if(info->HasBackpack()  && ctrlGroup && defslot)
        { 
          _bagItems.Clear();
          ctrlGroup->RemoveAllControls();
         ParamEntryVal cls = Pars >> _resource >> "controls";
         if(cls.FindEntry("BagItemsGroup")) cls = cls >> "BagItemsGroup";

          for (int i =0; i < min(IDC_GEAR_BAG_FULL, bagSize) ;i++)
          {
            CItemSlot *ctrl = new CItemSlot(this, IDC_GEAR_BAG_EMPTY + i, cls);
            _bagItems.Add(ctrl);
            ctrl->SetPos((i%8)*(_magW+_spacing),(i/8) * (_magH+_spacing),_magW, _magH);
            ctrl->SetStyle(defslot->GetStyle(), defslot->_color,defslot->_bgColor, defslot->_bgColorSelected, defslot->_colorFocused,false);
            ctrlGroup->AddControl(ctrl);
          }
        }
      }
    

      //weapons
      for (int i = 0; i < supply->GetWeaponCargoSize(); i++)
      {
        if(index>=_bagItems.Size()) break;
        int tmpPos = pos;
        const WeaponType *weapon = supply->GetWeaponCargo(i);
        if((weapon->_weaponType & MaskSlotHandGun) ==0 && (weapon->_weaponType & MaskSlotPrimary) ==0
          && (weapon->_weaponType & MaskSlotSecondary) ==0) continue;

        RString img = weapon ? GetWeaponPicture(weapon) : _emptyGun;
        _bagItems[index]->SetText(img);
        _bagItems[index]->SetData(weapon->GetName());
        _bagItems[index]->SetItemType(1);
        _bagItems[index]->ShowCtrl(true);

        if((weapon->_weaponType & MaskSlotHandGun) !=0 )
        { 
          pos+= (pos & 1);//force next line, if there is not enough space
          if(tmpPos/8 != pos/8) 
          {
            currHeight = nextHeight; nextHeight = currHeight + _gunH + _spacing;
            tmpPos = pos;
          }

          nextHeight = max(nextHeight,currHeight+_gunH + _spacing);

          _bagItems[index]->SetPos((pos%8)*(_magW + _spacing),currHeight,_gunW, _gunH);
          pos+=2;//handgun takes four slot in UI
          if(tmpPos/8 != pos/8)  //move to next line
          {
            currHeight = nextHeight;
            nextHeight = currHeight + _magH + _spacing;
          }
        }
        else if((weapon->_weaponType & MaskSlotPrimary) !=0 || (weapon->_weaponType & MaskSlotSecondary) !=0)
        {
          if((pos & 7)>4)//force next line, if there is not enough space
          {
            currHeight = nextHeight;  nextHeight = currHeight + _weaponH + _spacing;
            tmpPos = pos = 0;
          }

          nextHeight = max(nextHeight,currHeight+_weaponH + _spacing);

          _bagItems[index]->SetPos((pos%8)*(_magW + _spacing),currHeight,_weaponW, _weaponH);
          pos+=4; //weapon takes four slot in UI
          if(tmpPos/8 != pos/8) //move to next line
          {
            currHeight = nextHeight;
            nextHeight = currHeight + _magH + _spacing;
          }
        }

        index++;
      }
      //magazines and items starts at new line
      if(pos%8 !=0)
      {
        pos = 0;
        currHeight = nextHeight;
        nextHeight = currHeight + _magH + _spacing;
      }

      //items
      for (int i = 0; i < supply->GetWeaponCargoSize(); i++)
      {
        const WeaponType *weapon = supply->GetWeaponCargo(i);    
        if((weapon->_weaponType & MaskSlotHandGun) ==0 && (weapon->_weaponType & MaskSlotPrimary) ==0
          && (weapon->_weaponType & MaskSlotSecondary) ==0) 
        { 
          if(index>=_bagItems.Size()) break;
          int tmpPos = pos;

          RString img = weapon ? GetWeaponPicture(weapon) : _emptyGun;
          _bagItems[index]->SetText(img);
          _bagItems[index]->SetData(weapon->GetName());
          _bagItems[index]->SetItemType(1);
          _bagItems[index]->ShowCtrl(true);

          _bagItems[index]->SetPos((pos%8)*(_magW + _spacing),currHeight,_magW, _magH);
          pos++;
          if(tmpPos/8 != pos/8) 
          {
            currHeight = nextHeight;
            nextHeight = currHeight + _magH + _spacing;
          }
          index++;
        }
      }
      //magazines
      for (int i = 0; i < supply->GetMagazineCargoSize(); i++)
      {
        int tmpPos = pos;
        const Magazine *magazine = supply->GetMagazineCargo(i);
        if (magazine)
        {
          if(index>=_bagItems.Size()) break;
          RString img = GetMagazinePicture(magazine->_type);
          _bagItems[index]->SetText(img);
          _bagItems[index]->SetData(magazine->_type->GetName());
          _bagItems[index]->SetItemType(2);
          _bagItems[index]->ShowCtrl(true);
          _bagItems[index]->SetPos((pos%8)*(_magW + _spacing),currHeight,_magW, _magH);

          int itemI = 1;
          if((magazine->_type->_magazineType & MaskSlotItem) !=0) itemI = (magazine->_type->_magazineType & MaskSlotItem) >> 8;
          else if((magazine->_type->_magazineType & MaskSlotHandGunItem) !=0) itemI = (magazine->_type->_magazineType & MaskSlotHandGunItem) >> 4;

          index++; pos+=itemI;
          if(tmpPos/8 != pos/8) 
          {
            currHeight = nextHeight;
            nextHeight = currHeight + _magH + _spacing;
          }
        }
      }
      //empty slots
      int size = supply->GetFreeCargoBackpack(info->backpack);
      for (int i = 0; i < size; i++)
      {
        if(index>=_bagItems.Size()) break;
        int tmpPos = pos;
        _bagItems[index]->SetText(_emptyMag);
        _bagItems[index]->SetData("");
        _bagItems[index]->SetItemType(-1);
        _bagItems[index]->ShowCtrl(true);
        _bagItems[index]->SetPos((pos%8)*(_magW + _spacing),currHeight,_magW, _magH);

        index++; pos++;
        if(tmpPos/8 != pos/8) 
        {
          currHeight = nextHeight;
          nextHeight = currHeight + _magH + _spacing;
        }
      }   
      //hidden slots
      for (int i = index; i < _bagItems.Size(); i++)
      {
        if(index>=_bagItems.Size()) break;
        _bagItems[index]->SetText(_emptyMag);
        _bagItems[index]->SetData("");
        _bagItems[index]->SetItemType(-1);
        _bagItems[index]->ShowCtrl(false);
        index++;
      }
    }
  }

  if (_poolListBox)
  {
    bool selMagazine = false;
    RStringB selType;
    int sel = _poolListBox->GetCurSel();
    if (sel >= 0)
    {
      selMagazine = _poolListBox->GetValue(sel,0) == 1;
      selType = _poolListBox->GetData(sel,0);
    }

    _poolListBox->ClearStrings();
    sel = 0;    

    {
      // weapons
      WeaponsSummary summary;
      _pool.SummarizeWeapons(summary, 0xffffffff, 0);

      for (int i=0; i<summary.Size(); i++)
      {
        const WeaponType *type = summary[i].type;
        int count = summary[i].count;
        //filter
        if(_filters.Size()>_activeFilter && _filters[_activeFilter].mask>0)
          if((type->_weaponType & _filters[_activeFilter].mask)==0) continue;

        RString text;
        if (count > 1)
          text = Format(LocalizeString(IDS_NUM_MAGAZINES), cc_cast(type->GetDisplayName()), count);
        else
          text = type->GetDisplayName();

        int itemType = ((summary[i].type->_weaponType & MaskSlotPrimary) != 0 || (summary[i].type->_weaponType  & MaskSlotSecondary) != 0)? 0:2;
        int row = _poolListBox->AddGearString(summary[i],info, _openedBag);
        _poolListBox->SetValue(row,_poolListBox->PrimaryColumn(), itemType);
        _poolListBox->SetData(row,_poolListBox->PrimaryColumn(), type->GetName());
        RString pictureName = FindPicture(GetWeaponPicture(type));
        if (pictureName.GetLength() > 0)
        {
          pictureName.Lower();
          Ref<Texture> picture = GlobLoadTexture(pictureName);
          if (picture) _poolListBox->SetTexture(row,1, picture);
        }
        if (!selMagazine && type->GetName() == selType) sel = row;
      }
    }

    //player weapons
    if(!_openedBag)
    {
      for(int i=0; i< WEAPON_SLOTS; i++)
      {
        if(!info->weapons[i]) continue;
        const WeaponType *type = info->weapons[i];
        //filter
        if(_filters.Size()>_activeFilter && _filters[_activeFilter].mask>0)
          if((type->_weaponType & _filters[_activeFilter].mask)==0) continue;

        int row = _poolListBox->AddUnitPool(type,info, false);
        if(row>=0)
        {
          int itemType = ((type->_weaponType  & MaskSlotPrimary) != 0 || (type->_weaponType  & MaskSlotSecondary) != 0)? 0:2;
          _poolListBox->SetValue(row,_poolListBox->PrimaryColumn(), itemType);
          _poolListBox->SetData(row,_poolListBox->PrimaryColumn(), type->GetName());
          RString pictureName = FindPicture(GetWeaponPicture(type));
          if (pictureName.GetLength() > 0)
          {
            pictureName.Lower();
            Ref<Texture> picture = GlobLoadTexture(pictureName);
            if (picture) _poolListBox->SetTexture(row,1, picture);
          }
          if (!selMagazine && type->GetName() == selType) sel = row;
        }
      }
    }//player weapons in backpack
    else if (supply)
    {      
      for(int i=0; i< supply->GetWeaponCargoSize(); i++)
      {
        if(!supply->GetWeaponCargo(i)) continue;
        const WeaponType *type = supply->GetWeaponCargo(i);
        //filter
        if(_filters.Size()>_activeFilter && _filters[_activeFilter].mask>0)
          if((type->_weaponType & _filters[_activeFilter].mask)==0) continue;

        int row = _poolListBox->AddUnitPool(type,info, true);
        if(row>=0)
        {
          int itemType = ((type->_weaponType  & MaskSlotPrimary) != 0 || (type->_weaponType  & MaskSlotSecondary) != 0)? 0:2;
          _poolListBox->SetValue(row,_poolListBox->PrimaryColumn(), itemType);
          _poolListBox->SetData(row,_poolListBox->PrimaryColumn(), type->GetName());
          RString pictureName = FindPicture(GetWeaponPicture(type));
          if (pictureName.GetLength() > 0)
          {
            pictureName.Lower();
            Ref<Texture> picture = GlobLoadTexture(pictureName);
            if (picture) _poolListBox->SetTexture(row,1, picture);
          }
          if (!selMagazine && type->GetName() == selType) sel = row;
        }
      }

    }

    {
      // magazines
      MagazinesSummary summary;
      _pool.SummarizeMagazines(summary, 0xffffffff, 0);

      for (int i=0; i<summary.Size(); i++)
      {
        const MagazineType *type = summary[i].type;
        int count = summary[i].count;
        //filter
        if(_filters.Size()>_activeFilter && _filters[_activeFilter].mask>0)     
          if((type->_magazineType & _filters[_activeFilter].mask)==0) continue;

        RString text;
        if (count > 1)
          text = Format(LocalizeString(IDS_NUM_MAGAZINES), cc_cast(type->GetDisplayName()), count);
        else
          text = type->GetDisplayName();

        int row = _poolListBox->AddGearString(summary[i],info, _openedBag);
        _poolListBox->SetValue(row,_poolListBox->PrimaryColumn(), 1);
        _poolListBox->SetData(row,_poolListBox->PrimaryColumn(), type->GetName());
        RString pictureName = FindPicture(GetMagazinePicture(type));
        if (pictureName.GetLength() > 0)
        {
          pictureName.Lower();
          Ref<Texture> picture = GlobLoadTexture(pictureName);
          if (picture) _poolListBox->SetTexture(row,1, picture);
        }

        if (selMagazine && type->GetName() == selType) sel = row;
      }
    }


    if(!_openedBag)
    {
      //player magazines
      for(int i=0; i< MAGAZINE_SLOTS; i++)
      {
        if(!info->magazines[i]) continue;
        Ref<Magazine> magazine = info->magazines[i];
        const MagazineType *type = magazine->_type;
        //filter
        if(_filters.Size()>_activeFilter && _filters[_activeFilter].mask>0)        
          if((type->_magazineType & _filters[_activeFilter].mask)==0) continue;

        int row = _poolListBox->AddUnitPool(type,info, false);
        if(row>=0)
        {
          _poolListBox->SetValue(row,_poolListBox->PrimaryColumn(), 1);
          _poolListBox->SetData(row,_poolListBox->PrimaryColumn(), type->GetName());
          RString pictureName = FindPicture(GetMagazinePicture(type));
          if (pictureName.GetLength() > 0)
          {
            pictureName.Lower();
            Ref<Texture> picture = GlobLoadTexture(pictureName);
            if (picture) _poolListBox->SetTexture(row,1, picture);
          }
          if (selMagazine && type->GetName() == selType) sel = row;
        }
      }
    }
    else if (supply)
    {
      //player magazines
      for(int i=0; i< supply->GetMagazineCargoSize(); i++)
      {
        if(!supply->GetMagazineCargo(i)) continue;
        const Magazine *magazine = supply->GetMagazineCargo(i);
        const MagazineType *type = magazine->_type;
        //filter
        if(_filters.Size()>_activeFilter && _filters[_activeFilter].mask>0)        
          if((type->_magazineType & _filters[_activeFilter].mask)==0) continue;

        int row = _poolListBox->AddUnitPool(type,info, true);
        if(row>=0)
        {
          _poolListBox->SetValue(row,_poolListBox->PrimaryColumn(), 1);
          _poolListBox->SetData(row,_poolListBox->PrimaryColumn(), type->GetName());
          RString pictureName = FindPicture(GetMagazinePicture(type));
          if (pictureName.GetLength() > 0)
          {
            pictureName.Lower();
            Ref<Texture> picture = GlobLoadTexture(pictureName);
            if (picture) _poolListBox->SetTexture(row,1, picture);
          }
          if (selMagazine && type->GetName() == selType) sel = row;
        }
      }
    }

    if(!_openedBag)
    {
      // backpacks
      EntityAI *unitbackpack = NULL;
      if(info->HasBackpack())
        unitbackpack =  info->backpack;

      RefArray<EntityAI> backpacks = _pool.SummarizeBackpacks();

      for(int i=0; i<backpacks.Size(); i++)
      {
        bool used = false;
        for(int j=0; j<i; j++) 
          if(backpacks[i]->GetType() == backpacks[j]->GetType()) used = true;
        if(used) continue;

        EntityAI *backpack =  backpacks[i];
        if(!backpack) continue;
        const EntityAIType *type = backpack->GetType();
        RString name = type->_displayName;
        //filter
        if(_filters.Size()>_activeFilter && 
          (_filters[_activeFilter].mask<=0 || ((_filters[_activeFilter].mask & MaskSlotSecondary)!=0)))        
        { 
          int count = 0;
          for(int j=0; j<backpacks.Size(); j++)
            if(backpacks[i]->GetType() == backpacks[j]->GetType()) count++; 

          int wcount = 0;
          if(unitbackpack && type == unitbackpack->GetType()) wcount++;
          int row = _poolListBox->AddItem(type->_displayName,count,wcount);
          if(row>=0)
          {
            _poolListBox->SetValue(row,_poolListBox->PrimaryColumn(), 3);
            _poolListBox->SetData(row,_poolListBox->PrimaryColumn(), type->GetName());  
            _poolListBox->SetTexture(row,1, backpack->GetPicture());
          }
          if (type->GetName() == selType) sel = row;
        }
      }

      //player's backpack
      if(unitbackpack)
      {
        const EntityAIType *type = unitbackpack->GetType();
        //filter
        if(_filters.Size()>_activeFilter && 
          (_filters[_activeFilter].mask<=0 || ((_filters[_activeFilter].mask & MaskSlotSecondary)!=0)))       
        {      
          int row = _poolListBox->AddUnitPool(type);
          if(row>=0)
          {
            _poolListBox->SetValue(row,_poolListBox->PrimaryColumn(), 3);
            _poolListBox->SetData(row,_poolListBox->PrimaryColumn(), type->GetName());  
            _poolListBox->SetTexture(row,1, unitbackpack->GetPicture());
          }
          if (type->GetName() == selType) sel = row;
        }
      }
    }

    _poolListBox->SortItems();
    if(_poolListBox->GetCurSel()<0)
      _poolListBox->SetCurSel(sel);

  }
  UpdateButtons();
}

void DisplayGear::UpdateWeapons(EntityAI *owner)
{

  if(!CheckOwner(owner)) return;
  // nothing to do with weapons - change processed by the briefing screen
  OnUnitChanged();
}


/*!
\patch 5142 Date 3/16/2007 by Jirka
- Fixed: UI - Gear dialog - weapon overview improved
*/


void DisplayGear::UpdateButtons()
{
  IControl *ctrl = GetCtrl(IDC_OK);
  IControl *ctrlLeftArrow = GetCtrl(IDC_GEAR_ADD_ITEM);
  IControl *ctrlRightArrow = GetCtrl(IDC_GEAR_REMOVE_ITEM);
  IControl *ctrlRearm = GetCtrl(IDC_GEAR_REARM);
  if (!ctrl) return;
  ITextContainer *text = GetTextContainer(ctrl);
  if (!text)
  {
    ctrl->ShowCtrl(false);
    return;
  }
  UnitWeaponsInfo *info = GetCurrentInfo();
  if (!info)
  {
    ctrl->ShowCtrl(false);
    return;
  }

  if(ctrlRearm)
    ctrlRearm->EnableCtrl(_pool.CanRearm(info->unit));

  switch (_defaultAction)
  {
  case ActionNone:
    ctrl->ShowCtrl(false);
    break;
  case ActionRearm:
    ctrl->ShowCtrl(true);
    ctrl->EnableCtrl(_pool.CanRearm(info->unit));
    text->SetText(LocalizeString(IDS_GEAR_REARM));
    break;
  case ActionTake:
    ctrl->ShowCtrl(true);
    ctrl->EnableCtrl(_pool.CanReplace(info->unit) && 
      _poolListBox && _poolListBox->GetCurSel() >= 0 && _poolListBox->GetValue(_poolListBox->GetCurSel(),0)!=0 );
    text->SetText(LocalizeString(IDS_GEAR_TAKE));
    break;
  case ActionDrop:
    ctrl->ShowCtrl(true);
    {
      bool canDrop = false;
      if (_pool.CanDrop(info->unit) && _selectedSlot)
      {
        int idc = _selectedSlot->IDC();

        if (idc >=IDC_GEAR_BAG_EMPTY && idc<IDC_GEAR_BAG_FULL)
        {
          canDrop = _selectedSlot->GetText().GetLength()>0;
        }

        switch (idc)
        {
        case IDC_GEAR_SLOT_PRIMARY:
          canDrop = info->weapons[0] != NULL;
          break;
        case IDC_GEAR_SLOT_SECONDARY:
          canDrop = (info->weapons[1] != NULL || info->HasBackpack());
          break;
        case IDC_GEAR_SLOT_ITEM1:
        case IDC_GEAR_SLOT_ITEM2:
        case IDC_GEAR_SLOT_ITEM3:
        case IDC_GEAR_SLOT_ITEM4:
        case IDC_GEAR_SLOT_ITEM5:
        case IDC_GEAR_SLOT_ITEM6:
        case IDC_GEAR_SLOT_ITEM7:
        case IDC_GEAR_SLOT_ITEM8:
        case IDC_GEAR_SLOT_ITEM9:
        case IDC_GEAR_SLOT_ITEM10:
        case IDC_GEAR_SLOT_ITEM11:
        case IDC_GEAR_SLOT_ITEM12:
          if (idc - IDC_GEAR_SLOT_ITEM1 < FIRST_HANDGUN_SLOT)
          {
            canDrop = info->magazines[idc - IDC_GEAR_SLOT_ITEM1] != NULL;
          }
          break;
        case IDC_GEAR_SLOT_HANDGUN:
          canDrop = info->weapons[4] != NULL;
          break;
        case IDC_GEAR_SLOT_HANGUN_ITEM1:
        case IDC_GEAR_SLOT_HANGUN_ITEM2:
        case IDC_GEAR_SLOT_HANGUN_ITEM3:
        case IDC_GEAR_SLOT_HANGUN_ITEM4:
        case IDC_GEAR_SLOT_HANGUN_ITEM5:
        case IDC_GEAR_SLOT_HANGUN_ITEM6:
        case IDC_GEAR_SLOT_HANGUN_ITEM7:
        case IDC_GEAR_SLOT_HANGUN_ITEM8:
          if (FIRST_HANDGUN_SLOT + idc - IDC_GEAR_SLOT_HANGUN_ITEM1 < MAGAZINE_SLOTS)
          {
            canDrop = info->magazines[FIRST_HANDGUN_SLOT + idc - IDC_GEAR_SLOT_HANGUN_ITEM1] != NULL;
          }
          break;
        case IDC_GEAR_SLOT_SPECIAL1:
        case IDC_GEAR_SLOT_SPECIAL2:
          canDrop = info->weapons[2 + idc - IDC_GEAR_SLOT_SPECIAL1] != NULL;
          break;
        case IDC_GEAR_SLOT_INVENTORY1:
        case IDC_GEAR_SLOT_INVENTORY2:
        case IDC_GEAR_SLOT_INVENTORY3:
        case IDC_GEAR_SLOT_INVENTORY4:
        case IDC_GEAR_SLOT_INVENTORY5:
        case IDC_GEAR_SLOT_INVENTORY6:
        case IDC_GEAR_SLOT_INVENTORY7:
        case IDC_GEAR_SLOT_INVENTORY8:
        case IDC_GEAR_SLOT_INVENTORY9:
        case IDC_GEAR_SLOT_INVENTORY10:
        case IDC_GEAR_SLOT_INVENTORY11:
        case IDC_GEAR_SLOT_INVENTORY12:
          canDrop = info->weapons[FIRST_INVENTORY_SLOT + idc - IDC_GEAR_SLOT_INVENTORY1] != NULL;
          break;
        }
      }
      ctrl->EnableCtrl(canDrop);
    }
    text->SetText(LocalizeString(IDS_GEAR_DROP));
    break;
  }

  if(_openedBag || _openedBagOnGround)
  {
    IControl *ctrl = GetCtrl(IDC_GEAR_REARM);
    if(ctrl) ctrl->ShowCtrl(false);
    ctrl = GetCtrl(IDC_GEAR_OPEN_BAG);
    if(ctrl) ctrl->ShowCtrl(false);
    ctrl = GetCtrl(IDC_GEAR_CLOSE_BAG);
    if(ctrl) ctrl->ShowCtrl(true);
  }
  else if 
    (_poolListBox->GetCurSel() >=0 
    && _poolListBox->GetValue(_poolListBox->GetCurSel(),_poolListBox->PrimaryColumn()) == 3)
  {
    IControl *ctrl = GetCtrl(IDC_GEAR_REARM);
    if(ctrl) ctrl->ShowCtrl(false);

    if(info->HasBackpack() && info->backpack->GetSupply() && strcmp(_poolListBox->GetData(_poolListBox->GetCurSel(),_poolListBox->PrimaryColumn()) ,info->backpack->GetType()->GetName()) == 0)
    {
      if(!_pool.GetSource() ||  _pool.GetSource() != info->backpack)
      {
        ctrl = GetCtrl(IDC_GEAR_OPEN_BAG);
        if(ctrl) ctrl->ShowCtrl(true);
      }
    }
    else
    {
      ctrl = GetCtrl(IDC_GEAR_OPEN_BAG);
      if(ctrl) ctrl->ShowCtrl(false);
    }
  }
  else
  {
    IControl *ctrl = GetCtrl(IDC_GEAR_OPEN_BAG);
    if(ctrl) ctrl->ShowCtrl(false);

    ctrl = GetCtrl(IDC_GEAR_REARM);
    if(ctrl) ctrl->ShowCtrl(true);
  }

  ITextContainer *itext = dynamic_cast<ITextContainer *>(GetCtrl(IDC_GEAR_SOURCE_NAME));
  if(itext) itext->SetText(_pool.GetDisplayName());

  if(ctrlRightArrow) ctrlRightArrow->EnableCtrl(_pool.CanReplace(info->unit) && _poolListBox && _poolListBox->GetCurSel() >= 0);
  if(ctrlLeftArrow) ctrlLeftArrow->EnableCtrl(_pool.CanDrop(info->unit));
}

void DisplayGear::UpdateOverview(const WeaponType *weapon)
{
  if (!_overview) return;
  _overview->Init();

  if (!weapon) return;

  int section = _overview->AddSection();

  _overview->AddText(section, weapon->GetDisplayName(), HFH2, HACenter, false, false, RString());
  _overview->AddBreak(section, false);
  _overview->AddBreak(section, false);

  _overview->AddText(section, LocalizeString(IDS_LIB_LABEL_DESCRIPTION), HFH3, HALeft, false, false, RString());
  _overview->AddBreak(section, false);
  RString desc = weapon->GetDescription();
  if (desc.GetLength() == 0) desc = LocalizeString(IDS_LIB_INFO_NO_TEXT);
  QIStrStream in(cc_cast(desc), desc.GetLength());
  TextToHTMLParser parser(_overview, section);
  parser.Parse(in);
  _overview->AddBreak(section, false);
  _overview->AddBreak(section, false);

  int nMagazines = 0;
  for (int i=0; i<weapon->_muzzles.Size(); i++)
  {
    const MuzzleType *muzzle = weapon->_muzzles[i];
    nMagazines += muzzle->_magazines.Size();
  }

  if (nMagazines > 0)
  {
    _overview->AddText(section, LocalizeString(IDS_LIB_INFO_MAGAZINE), HFH3, HALeft, false, false, RString());
    _overview->AddBreak(section, false);
    for (int i=0; i<weapon->_muzzles.Size(); i++)
    {
      const MuzzleType *muzzle = weapon->_muzzles[i];
      for (int j=0; j<muzzle->_magazines.Size(); j++)
      {
        if (j != 0) _overview->AddText(section, ", ", HFP, HALeft, false, false, RString());
        const MagazineType *magazine = muzzle->_magazines[j];
        _overview->AddText(section, magazine->GetDisplayName(), HFP, HALeft, false, false, RString());
        /*
        const MagazineType *magazine = muzzle->_magazines[j];
        RString href = RString("InfoMagazine:") + magazine->GetName();
        _overview->AddText(section, magazine->GetDisplayName(), HFP, HALeft, false, false, href);
        _overview->AddBreak(section, false);
        */
      }
    }
  }

  _overview->FormatSection(section);
}

void DisplayGear::UpdateOverview(const MagazineType *magazine)
{
  if (!_overview) return;
  _overview->Init();

  if (!magazine) return;

  int section = _overview->AddSection();

  _overview->AddText(section, magazine->GetDisplayName(), HFH2, HACenter, false, false, RString());
  _overview->AddBreak(section, false);
  _overview->AddBreak(section, false);

  _overview->AddText(section, LocalizeString(IDS_LIB_LABEL_DESCRIPTION), HFH3, HALeft, false, false, RString());
  _overview->AddBreak(section, false);
  RString desc = magazine->GetDescription();
  if (desc.GetLength() == 0) desc = LocalizeString(IDS_LIB_INFO_NO_TEXT);
  QIStrStream in(cc_cast(desc), desc.GetLength());
  TextToHTMLParser parser(_overview, section);
  parser.Parse(in);
  _overview->AddBreak(section, false);
  _overview->AddBreak(section, false);

  _overview->AddText(section, LocalizeString(IDS_LIB_LABEL_STATISTICS), HFH3, HALeft, false, false, RString());
  _overview->AddBreak(section, false);
  desc = LocalizeString(IDS_LIB_INFO_MUZZLE_VEL) + Format(" %.0f ", magazine->_initSpeed) + 

    LocalizeString(IDS_LIB_INFO_UNIT_METERS_PER_SECOND);
  _overview->AddText(section, desc, HFP, HALeft, false, false, RString());
  _overview->AddBreak(section, false);
  desc = LocalizeString(IDS_LIB_INFO_AMMO_COUNT) + Format(" %d ", magazine->_maxAmmo);
  _overview->AddText(section, desc, HFP, HALeft, false, false, RString());
  _overview->AddBreak(section, false);

  _overview->FormatSection(section);
}

void DisplayGear::UpdateOverview(RString name)
{
  if (!_overview) return;
  _overview->Init();

  if (name.GetLength()<=0) return;

  int section = _overview->AddSection();

  _overview->AddText(section, name, HFH2, HACenter, false, false, RString());
  _overview->AddBreak(section, false);
  _overview->AddBreak(section, false);

  //_overview->AddText(section, LocalizeString(IDS_LIB_LABEL_DESCRIPTION), HFH3, HALeft, false, false, RString());
  //_overview->AddBreak(section, false);
  //RString desc = magazine->GetDescription();
  //if (desc.GetLength() == 0) desc = LocalizeString(IDS_LIB_INFO_NO_TEXT);
  //QIStrStream in(cc_cast(desc), desc.GetLength());
  //TextToHTMLParser parser(_overview, section);
  //parser.Parse(in);
  //_overview->AddBreak(section, false);
  //_overview->AddBreak(section, false);

  //_overview->AddText(section, LocalizeString(IDS_LIB_LABEL_STATISTICS), HFH3, HALeft, false, false, RString());
  //_overview->AddBreak(section, false);
  //desc = LocalizeString(IDS_LIB_INFO_MUZZLE_VEL) + Format(" %.0f ", magazine->_initSpeed) + 

  //  LocalizeString(IDS_LIB_INFO_UNIT_METERS_PER_SECOND);
  //_overview->AddText(section, desc, HFP, HALeft, false, false, RString());
  //_overview->AddBreak(section, false);
  //desc = LocalizeString(IDS_LIB_INFO_AMMO_COUNT) + Format(" %d ", magazine->_maxAmmo);
  //_overview->AddText(section, desc, HFP, HALeft, false, false, RString());
  //_overview->AddBreak(section, false);

  _overview->FormatSection(section);
}

class GearBodyHelper
{
protected:
  ItemsPoolBody _body;
  WeaponsInfo _target;

public:
  GearBodyHelper(AIBrain *unit, EntityAI *body);
};

GearBodyHelper::GearBodyHelper(AIBrain *unit, EntityAI *body)
: _body(body)
{
  // create structure
  _target._weapons.Realloc(1);
  _target._weapons.Add(UnitWeaponsInfo(unit));
}

class DisplayGearBody : public GearBodyHelper, public DisplayGear
{
  typedef DisplayGear base;

public:
  DisplayGearBody(ControlsContainer *parent, AIBrain *unit, EntityAI *body)
    : GearBodyHelper(unit, body),
    DisplayGear(parent, true, _target, _body, unit, false) {}

  void DestroyHUD(int exit)
  {
    GWorld->DestroyUserDialog();
  }
  void OnSimulate(EntityAI *vehicle);

  virtual void UpdateWeapons(EntityAI *owner = NULL);
};

void DisplayGearBody::OnSimulate(EntityAI *vehicle)
{
  // close the dialog when supply source is out of range
  EntityAI *source = _body.GetSource();
  EntityAI *veh = _unit ? _unit->GetVehicle() : NULL;
  if (!veh || !source)
  {
    Exit(IDC_CANCEL);
    return;
  }
  else
  {
    // check distance
    Vector3 supplyPos = source->FutureVisualState().PositionModelToWorld(source->GetType()->GetSupplyPoint());
    float supplyRadius = source->GetType()->GetSupplyRadius();

    float dist2 = veh->FutureVisualState().Position().Distance2(supplyPos);
    float maxDistance = floatMin(1.5 * (veh->CollisionSize() * 1.2 + 1 + supplyRadius), 40);
    if (dist2 > Square(maxDistance))
    {
      Exit(IDC_CANCEL);
      return;
    }
  }

  base::OnSimulate(vehicle);
}

void DisplayGearBody::UpdateWeapons(EntityAI *owner)
{
  if(!CheckOwner(owner)) return;

  for (int i=0; i<_target._weapons.Size(); i++)
    _target._weapons[i].Update();

  OnUnitChanged();
}

AbstractOptionsUI *CreateGearBodyDialog(AIBrain *unit, EntityAI *body)
{
  return new DisplayGearBody(NULL, unit, body);
}

class GearSupplyHelper
{
protected:
  ItemsPoolSupply _supply;
  WeaponsInfo _target;

public:
  GearSupplyHelper(AIBrain *unit, EntityAI *supply, EntityAI *supplySecondary);
  virtual ~GearSupplyHelper();
};

GearSupplyHelper::GearSupplyHelper(AIBrain *unit, EntityAI *supply, EntityAI *supplySecondary)
: _supply(supply, supplySecondary)
{
  // create structure
  Assert(unit);
  _target._weapons.Realloc(1);
  _target._weapons.Add(UnitWeaponsInfo(unit->GetUnit()));

  EntityAI *source = _supply.GetSource();
  if (source) source->LockResources();
}

GearSupplyHelper::~GearSupplyHelper()
{
  EntityAI *source = _supply.GetSource();
  if (source)
  {
    source->UnlockResources();
    source->DeleteIfNotNeeded();
  }

  source = _supply.GetSourceSecondary();
  if (source)
  {
    source->UnlockResources();
    source->DeleteIfNotNeeded();
  }
}

class DisplayGearSupply : public GearSupplyHelper, public DisplayGear
{
  typedef DisplayGear base;

public:
  DisplayGearSupply(ControlsContainer *parent, AIBrain *unit, EntityAI *supply, EntityAI *supplySecondary, RString resource = "RscDisplayGear", bool onGround = false)
    : GearSupplyHelper(unit, supply, supplySecondary),
    DisplayGear(parent, true, _target, _supply, unit, false,resource, onGround) {}

  void DestroyHUD(int exit)
  {
    GWorld->DestroyUserDialog();
  }
  void OnSimulate(EntityAI *vehicle);

  virtual void UpdateWeapons(EntityAI *owner = NULL);
};

void DisplayGearSupply::OnSimulate(EntityAI *vehicle)
{
  // close the dialog when supply source is out of range
  EntityAI *source = _supply.GetSource();
  EntityAI *veh = _unit ? _unit->GetVehicle() : NULL;
  if (!veh || !source)
  {
    Exit(IDC_CANCEL);
    return;
  }
  else
  {
    // check distance
    Vector3 supplyPos = source->FutureVisualState().PositionModelToWorld(source->GetType()->GetSupplyPoint());
    float supplyRadius = source->GetType()->GetSupplyRadius();

    float dist2 = veh->FutureVisualState().Position().Distance2(supplyPos);
    float maxDistance = floatMin(1.5 * (veh->CollisionSize() * 1.2 + 1 + supplyRadius), 40);
    if (dist2 > Square(maxDistance))
    {
      Exit(IDC_CANCEL);
      return;
    }
  }

  base::OnSimulate(vehicle);
}

void DisplayGearSupply::UpdateWeapons(EntityAI *owner)
{
  if(!CheckOwner(owner)) return;

  for (int i=0; i<_target._weapons.Size(); i++)
    _target._weapons[i].Update();

  OnUnitChanged();
}


class GearBackpackHelper
{
protected:
  ItemsPoolSupply _supply;
  WeaponsInfo _target;
  EntityAI *_owner;

public:
  GearBackpackHelper(AIBrain *unit, EntityAI *owner, EntityAI *supply);
  virtual ~GearBackpackHelper();
};

GearBackpackHelper::GearBackpackHelper(AIBrain *unit, EntityAI *owner, EntityAI *supply)
: _owner(owner),_supply(supply)
{
  // create structure
  Assert(unit);
  _target._weapons.Realloc(1);
  _target._weapons.Add(UnitWeaponsInfo(unit->GetUnit()));

  EntityAI *source = _supply.GetSource();
  if (source) source->LockResources();
}

GearBackpackHelper::~GearBackpackHelper()
{
  EntityAI *source = _supply.GetSource();
  if (source)
  {
    source->UnlockResources();
    source->DeleteIfNotNeeded();
  }
}

class DisplayGearBackpack : public GearBackpackHelper, public DisplayGear
{
  typedef DisplayGear base;

public:
  DisplayGearBackpack(ControlsContainer *parent, AIBrain *unit, EntityAI *owner, EntityAI *supply, RString resource = "RscDisplayGear")
    : GearBackpackHelper(unit, owner, supply),
    DisplayGear(parent, true, _target, _supply, unit, false,resource) {}

  void DestroyHUD(int exit)
  {
    GWorld->DestroyUserDialog();
  }
  void OnSimulate(EntityAI *vehicle);

  virtual void UpdateWeapons(EntityAI *owner = NULL);
};

void DisplayGearBackpack::OnSimulate(EntityAI *vehicle)
{
  // close the dialog when supply source is out of range
  EntityAI *source = _supply.GetSource();
  EntityAI *veh = _unit ? _unit->GetVehicle() : NULL;
  if (!veh || !source || !_owner )
  {
    Exit(IDC_CANCEL);
    return;
  }
  else
  {
    // check distance
    Vector3 supplyPos = _owner->FutureVisualState().Position();
    float supplyRadius = source->GetType()->GetSupplyRadius();
    if(source->GetType()->IsBackpack() && source->GetHierachyParent())
    {
      supplyPos = source->GetHierachyParent()->FutureVisualState().Position();
    }
    else supplyPos = source->FutureVisualState().Position();

    float dist2 = veh->FutureVisualState().Position().Distance2(supplyPos);
    float maxDistance = floatMin(1.5 * (veh->CollisionSize() * 1.2 + 1 + supplyRadius), 40);
    if (dist2 > Square(maxDistance))
    {
      Exit(IDC_CANCEL);
      return;
    }
  }

  base::OnSimulate(vehicle);
}

void DisplayGearBackpack::UpdateWeapons(EntityAI *owner)
{
  if(!CheckOwner(owner)) return;

  for (int i=0; i<_target._weapons.Size(); i++)
    _target._weapons[i].Update();

  OnUnitChanged();
}

AbstractOptionsUI *CreateGearSupplyDialog(AIBrain *unit, EntityAI *supply)
{
  return new DisplayGearSupply(NULL, unit, supply, NULL);
}

AbstractOptionsUI *CreateGearBackpackDialog(AIBrain *unit, EntityAI *owner)
{
  if(!owner->GetBackpack()) return NULL;
  return new DisplayGearBackpack(NULL, unit, owner,owner->GetBackpack());
}

/*!
\patch 5138 Date 3/12/2007 by Jirka
- Fixed: Gear action, when inside vehicle, gear action use this vehicle as an items container
*/

Display *CreateGearSimpleDialog(ControlsContainer *parent, AIBrain *unit, RString resource = "RscDisplayGear")
{
  EntityAIFull *vehicle = unit->GetVehicle();
  if (!vehicle) return NULL;

  Vector3 pos = vehicle->FutureVisualState().Position() + 0.5f * vehicle->FutureVisualState().Direction() + VUp*0.5f;
  Matrix3 dir;
  dir.SetUpAndDirection(VUp, vehicle->FutureVisualState().Direction());
  Matrix4 transform;
  transform.SetPosition(pos);
  transform.SetOrientation(dir);

  Transport *inside = unit->GetVehicleIn();
  if (inside)
  {
    if (!inside->HasSupply()) return NULL;
    return new DisplayGearSupply(parent, unit,  inside, NULL, resource);
  }
  else
  {
    //swimming players cannot use Gear (supply source was often located too far and Gear dialog was closed anyway)
    Person *person = unit->GetPerson();
    if ( person && person->IsSwimming() ) return NULL;
    // create container
    Ref<EntityAI> veh = GWorld->NewVehicleWithID("WeaponHolder");
    if (!veh) return NULL;

    veh->PlaceOnSurfaceUnderWater(transform); //we want weapons on the floor
    veh->SetTransform(transform);
    veh->Init(transform, true);
    veh->OnEvent(EEInit);

    GWorld->AddSlowVehicle(veh);
    if (GWorld->GetMode() == GModeNetware)
      GetNetworkManager().CreateVehicle(veh, VLTSlowVehicle, "", -1);
    GWorld->OnEntityMovedFar(veh);

    return new DisplayGearSupply(parent, unit, veh, person->GetBackpack(),resource, true);
  }
}

AbstractOptionsUI *CreateGearSimpleDialog(AIBrain *unit, RString resource = "RscDisplayGear")
{
  return CreateGearSimpleDialog(NULL, unit, resource);
}

#if _ENABLE_IDENTITIES

class DisplayEditDiaryRecord : public Display
{
protected:
  int _recordId;
  InitPtr<CEditContainer> _title;
  InitPtr<CEditContainer> _text;

public:
  DisplayEditDiaryRecord(ControlsContainer *parent, int recordId, RString title, RString text);
  Control *OnCreateCtrl(int type, int idc, ParamEntryPar cls);
  bool CanDestroy();
  int GetRecordId() const {return _recordId;}
  RString GetTitle() const;
  RString GetText() const;
};

DisplayEditDiaryRecord::DisplayEditDiaryRecord(ControlsContainer *parent, int recordId, RString title, RString text)
: Display(parent)
{
  _recordId = recordId;
  Load("RscDisplayEditDiaryRecord");
  if (parent && !parent->SimulationEnabled()) _enableSimulation = false;
  if (_title) _title->SetText(title);
  if (_text) _text->SetText(text);
}

Control *DisplayEditDiaryRecord::OnCreateCtrl(int type, int idc, ParamEntryPar cls)
{
  Control *ctrl = Display::OnCreateCtrl(type, idc, cls);
  switch (idc)
  {
  case IDC_EDIT_DIARY_TITLE:
    _title = GetEditContainer(ctrl);
    break;
  case IDC_EDIT_DIARY_RECORD:
    _text = GetEditContainer(ctrl);
    break;
  }
  return ctrl;
}

bool DisplayEditDiaryRecord::CanDestroy()
{
  if (!Display::CanDestroy()) return false;

  if (_exit == IDC_OK)
  {
    if (GetTitle().IsEmpty())
    {
      CreateMsgBox(MB_BUTTON_OK, LocalizeString(IDS_MSG_RECORDTITLE_EMPTY));
      return false;
    }
  }

  return true;
}

RString DisplayEditDiaryRecord::GetText() const
{
  if (!_text) return RString();
  return _text->GetText();
}

RString DisplayEditDiaryRecord::GetTitle() const
{
  if (!_title) return RString();
  return _title->GetText();
}

#endif

#if _VBS2
DisplayMap::DisplayMap(ControlsContainer *parent)
: Display(parent)
{
  _alwaysShow = true;
  _cursor = CursorArrow;
  _selectWeapons = false;
  _showGearOnExit = false;
  _briefingEnabled = true;
  _saveParams = true;
  _xboxStyle = false;
  _updatePlanAsked = false;
  _compassDir = Vector3(0, 0, 1);
}
#endif

/*!
\patch 1.07 Date 7/20/2001 by Jirka
- Fixed: after install or new user create marker centering in map does not work properly
\patch 5126 Date 2/1/2007 by Jirka
- New: Possibility to show satellite textures in the in-game map and briefing
*/

DisplayMap::DisplayMap(ControlsContainer *parent, RString resource)
: Display(parent)
{
  _alwaysShow = true;

  _cursor = CursorArrow;
  //  _debriefing = false;
  _selectWeapons = false;
  _showGearOnExit = false;

  _briefingEnabled = true;
#if _VBS3
  _compassDir = Vector3(0, 0, 1);
#endif

  ProgressRefresh();
  Load(resource);
  ProgressRefresh();

  ParamEntryVal cls = Pars >> resource;

  _saveParams = true;
  ConstParamEntryPtr entry = cls.FindEntry("saveParams");
  if (entry) _saveParams = *entry;

  _xboxStyle = false;
  entry = cls.FindEntry("xboxStyle");
  if (entry) _xboxStyle = *entry;

  _updatePlanAsked = false;

  // FIX
  AdjustMapVisibleRect();

  Init();
  ResetHUD();

  ShowMap(true);
  ShowCompass(true);
  ShowWatch(true);
  ShowWalkieTalkie(true);
  ShowNotepad(true);
  ShowWarrant(false);
  ShowGPS(true);

  ShowBriefing(false);

  UpdateTexturesButton();
}

void ValidateMarker(CHTMLContainer *html, bool inc, bool next)
{
  int section = html->CurrentSection();
  if (section < 0) return;
  const HTMLSection &s = html->GetSection(section);
  int n = s.fields.Size();
  if (n == 0) return;
  int i = html->GetActiveFieldIndex();
  if (next)
  {
    if (inc)
    {
      i++;
      if (i >= n) i = 0;
    }
    else
    {
      i--;
      if (i < 0) i = n - 1;
    }
  }
  saturate(i, 0, n);
  const char *name = "marker:";
  int len = strlen(name);
  if (inc)
  {
    for (int j=i; j<n; j++)
      if (strnicmp(s.fields[j].href, name, len) == 0)
      {
        html->SetActiveFieldIndex(j);
        return;
      }
      for (int j=0; j<i; j++)
        if (strnicmp(s.fields[j].href, name, len) == 0)
        {
          html->SetActiveFieldIndex(j);
          return;
        }
  }
  else
  {
    for (int j=i; j>=0; j--)
      if (strnicmp(s.fields[j].href, name, len) == 0)
      {
        html->SetActiveFieldIndex(j);
        return;
      }
      for (int j=n-1; j>i; j--)
        if (strnicmp(s.fields[j].href, name, len) == 0)
        {
          html->SetActiveFieldIndex(j);
          return;
        }
  }
}

void DisplayMap::SwitchBriefingSection(RString section)
{
  if (_briefing)
  {
    _briefing->SwitchSection(section);
    ValidateMarker(_briefing, true, false);
  }
}

DisplayMap::~DisplayMap()
{
  SaveParams();
}

bool DisplayMap::IsShownWalkieTalkie() const
{
  return _walkieTalkie ? _walkieTalkie->IsVisible() : false;  
}

bool DisplayMap::IsShownNotepad() const
{
  return _notepad ? _notepad->IsVisible() : false;  
}

bool DisplayMap::IsBriefingUsed() const
{
  return GetCtrl(IDC_NOTEPAD_PICTURE) != NULL;
}

bool DisplayMap::IsShownBriefing() const
{
  const IControl *pad = GetCtrl(IDC_NOTEPAD_PICTURE);
  return pad ? pad->IsVisible() : false;
}

bool DisplayMap::IsShownWarrant() const
{
  return _warrant ? _warrant->IsVisible() : false;  
}

void DisplayMap::ShowWalkieTalkie(bool show)
{
  if (_walkieTalkie) _walkieTalkie->ShowCtrl(show);
}

void DisplayMap::ShowNotepad(bool show)
{
  if (_notepad)
  {
    _notepad->ShowCtrl(show);
    AdjustMapVisibleRect();
  }
  _briefingEnabled = show;
  if (!show) ShowBriefing(false);
}

void DisplayMap::ShowWarrant(bool show)
{
  if (_warrant) _warrant->ShowCtrl(show);
}

int FindSectionForUnit(HTMLContent &html, RString sectionName, TargetSide side, AIUnit *unit)
{
  RString prefix = sectionName + RString(".");
  int section = -1;
  if (unit)
  {
    RString name = unit->GetVehicle()->GetVarName();
    if (name.GetLength() > 0)
    {
      section = html.FindSection(prefix + name);
    }
    if (section < 0)
    {
      AIGroup *grp = unit->GetGroup();
      if (grp)
      {
        AIUnit *leader = grp->Leader();
        if (leader && leader != unit && leader->GetVehicle())
        {
          name = leader->GetVehicle()->GetVarName();
          if (name.GetLength() > 0)
          {
            section = html.FindSection(prefix + name);
          }
        }
      }
    }
  }
  if (section < 0)
  {
    RString name;
    switch (side)
    {
    case TWest:
      name = prefix + RString("West"); goto SideFound;
    case TEast:
      name = prefix + RString("East"); goto SideFound;
    case TGuerrila:
      name = prefix + RString("Guerrila"); goto SideFound;
    case TCivilian:
      name = prefix + RString("Civilian"); goto SideFound;
SideFound:
      section = html.FindSection(name);
      break;
    }
  }
  if (section < 0) section = html.FindSection(sectionName);
  return section;
}

int FindSectionForUnit(HTMLContent &html, RString sectionName, AIUnit *unit)
{
  TargetSide side = TSideUnknown;
  if (unit)
  {
    AIGroup *grp = unit->GetGroup();
    if (grp)
    {
      AICenter *center = grp->GetCenter();
      if (center)
      {
        side = center->GetSide();
      }
    }
  }
  return FindSectionForUnit(html, sectionName, side, unit);
}

static void AddObjective(CHTMLContainer *html, int src, int dst, int value)
{
  if (value == OSHidden) return;

  ParamEntryVal cls = Pars >> "RscObjectives";
  RString picture;
  switch (value)
  {
  case OSDone:
    picture = cls >> "done";
    break;
  case OSFailed:
    picture = cls >> "failed";
    break;
  default:
    Fail("Unknown objective type");
  case OSActive:
    picture = cls >> "active";
    break;
  }
  html->AddBreak(dst, false);
  float imgHeight = 480.0f * 1.0f * html->GetPHeight();
  HTMLField *fld = html->AddImage(dst, picture, HALeft, false, -1, imgHeight, "");
  fld->exclude = true;
  float indent = 1.2 * fld->width;
  html->SetIndent(indent);
  html->CopySection(src, dst);
  html->SetIndent(0);
}

static void AddTasks(CHTMLContainer *html, int dst, Person *player)
{
#if _ENABLE_IDENTITIES

  const RefArray<Task> tasks = player->GetIdentity().GetSimpleTasks();
  ParamEntryVal cls = Pars >> "RscObjectives";
  RString picture;

  for (int i=0; i< tasks.Size(); i++)
  {
    switch (tasks[i]->GetState())
    {
    case TSSucceeded:
      picture = cls >> "done";
      break;
    case TSFailed:
      picture = cls >> "failed";
      break;
    case TSCanceled:
      picture = cls >> "cancled";
      break;
    case TSCreated:
    case TSNone:
    case TSAssigned:
      picture = cls >> "active";
      break;
    default:
      Fail("Unknown objective type");
      break;
    }

    html->AddBreak(dst, false);
    float imgHeight = 480.0f * 1.0f * html->GetPHeight();
    html->SetIndent(0);
    HTMLField *fld = html->AddImage(dst, picture, HALeft, false, -1, imgHeight, "");
    fld->exclude = true;
    float indent = 1.2 * fld->width;

    RString taskdesc;
    tasks[i]->GetDescriptionShort(taskdesc);
    html->SetIndent(indent);
    html->AddText(dst,taskdesc,HFP,HALeft,false,false,"",0,0,false);
    html->AddBreak(dst, false);
  }
  html->SetIndent(0);


#endif
}

//! updates mission plan section of briefing
bool UpdatePlan(CHTMLContainer *briefing, TargetSide side, AIUnit *unit)
{
  // init section
  bool actual = false;
  int section = briefing->FindSection("__PLAN");
  if (section < 0)
  {
    section = briefing->AddSection();
  }
  else
  {
    briefing->InitSection(section);
    if (briefing->CurrentSection() == section) actual = true;
    int s;
    while ((s = briefing->FindSection("__PLAN")) >= 0)
    {
      if (briefing->CurrentSection() == s) actual = true;
      briefing->RemoveSection(s);
    }
  }
  briefing->AddName(section, "__PLAN");

  // plan header
  int source = FindSectionForUnit(*briefing, "Plan", side, unit);
  if (source >= 0) briefing->CopySection(source, section);

  // objectives
  for (int s=0; s<briefing->NSections(); s++)
  {
    const HTMLSection &src = briefing->GetSection(s);
    for (int n=0; n<src.names.Size(); n++)
    {
      RString name = src.names[n];
      static const char *prefix = "OBJ_";
      static const char *prefixWest = "OBJ_WEST_";
      static const char *prefixEast = "OBJ_EAST_";
      static const char *prefixGuerrila = "OBJ_GUER_";
      static const char *prefixCivilian = "OBJ_CIVIL_";
      static const char *prefixHidden = "OBJ_HIDDEN_";
      static const char *prefixWestHidden = "OBJ_WEST_HIDDEN_";
      static const char *prefixEastHidden = "OBJ_EAST_HIDDEN_";
      static const char *prefixGuerrilaHidden = "OBJ_GUER_HIDDEN_";
      static const char *prefixCivilianHidden = "OBJ_CIVIL_HIDDEN_";
      if (strnicmp(name, prefix, strlen(prefix)) == 0)
      {
        bool hidden = false;
        if (strnicmp(name, prefixWest, strlen(prefixWest)) == 0)
        {
          if (side != TWest) continue;
        }
        else if (strnicmp(name, prefixEast, strlen(prefixEast)) == 0)
        {
          if (side != TEast) continue;
        }
        else if (strnicmp(name, prefixGuerrila, strlen(prefixGuerrila)) == 0)
        {
          if (side != TGuerrila) continue;
        }
        else if (strnicmp(name, prefixCivilian, strlen(prefixCivilian)) == 0)
        {
          if (side != TCivilian) continue;
        }
        else if (strnicmp(name, prefixHidden, strlen(prefixHidden)) == 0)
        {
          hidden = true;
          name = prefix + name.Substring(strlen(prefixHidden), INT_MAX);
        }
        else if (strnicmp(name, prefixWestHidden, strlen(prefixWestHidden)) == 0)
        {
          if (side != TWest) continue;
          hidden = true;
          name = prefixWest + name.Substring(strlen(prefixWestHidden), INT_MAX);
        }
        else if (strnicmp(name, prefixEastHidden, strlen(prefixEastHidden)) == 0)
        {
          if (side != TEast) continue;
          hidden = true;
          name = prefixEast + name.Substring(strlen(prefixEastHidden), INT_MAX);
        }
        else if (strnicmp(name, prefixGuerrilaHidden, strlen(prefixGuerrilaHidden)) == 0)
        {
          if (side != TGuerrila) continue;
          hidden = true;
          name = prefixGuerrila + name.Substring(strlen(prefixGuerrilaHidden), INT_MAX);
        }
        else if (strnicmp(name, prefixCivilianHidden, strlen(prefixCivilianHidden)) == 0)
        {
          if (side != TCivilian) continue;
          hidden = true;
          name = prefixCivilian + name.Substring(strlen(prefixCivilianHidden), INT_MAX);
        }
        int value;
        if (unit)
        {
          GameState *state = GWorld->GetGameState();
          value = toInt((float)state->Evaluate(name, GameState::EvalContext::_default, GWorld->GetMissionNamespace())); // mission namespace
        }
        else
        {
          value = hidden ? OSHidden : OSActive; 
        }
        AddObjective(briefing, s, section, value);
        break; // next section
      }
    }
  }

  briefing->FormatSection(section);
  return actual;
}

#if _VBS3
#include "../hla/AAR.hpp"
#endif
#if !_VBS3 // support for dynamic briefing
void DisplayMap::Init()
#else
void DisplayMap::Init(const RString &alternateBriefing)
#endif

{
  // setting default values
  if (_map)
  {
    _map->SetScale(-1);
    _map->Center();
    _map->Reset();
  }

  // load values
  LoadParams();

  // load briefing
  if (_briefing)
  {
    RString briefing = GetBriefingFile();
    if (briefing.GetLength() > 0)
      _briefing->Load(briefing);

#if _VBS3 && _EXT_CTRL
    // disabled until parsing is fixed

    if(GAAR.IsReplayMode()) _briefing->Parse(GAAR.GetBriefingData());

    // support for dynamic briefing
    if (alternateBriefing.GetLength() > 0) _briefing->Parse(alternateBriefing);
#endif

    // assign briefing
    AIBrain *unit = GWorld->FocusOn();
    int section = FindSectionForUnit(*_briefing, "Main", unit ? unit->GetUnit() : NULL);
    if (section >= 0)
      _briefing->AddName(section, "__BRIEFING");
    UpdatePlan();

    // find bookmarks
    _briefing->AddBookmark("__PLAN");
    _briefing->AddBookmark("__BRIEFING");
  }
#if _ENABLE_BRIEF_GRP
  if (_briefing)
  {
    _briefing->AddBookmark("Group");
    _briefing->AddBookmark("TeamSwitch");
  }
#else
  if (_bookmark3) _bookmark3->SetText("");
  if (_bookmark4) _bookmark4->SetText("");
#endif

  if (_briefing)
  {
    if (_briefing->FindSection("__PLAN") >= 0)
      SwitchBriefingSection("__PLAN");
    else if (_briefing->FindSection("__BRIEFING") >= 0)
      SwitchBriefingSection("__BRIEFING");
  }
  UpdateKeyHints();
  UpdateButtons();

#if 0
  Vector3 pos = _watch->Position();
  Vector3 posMin = _watch->PositionModelToWorld(_watch->GetShape()->Min());
  Vector3 posMax = _watch->PositionModelToWorld(_watch->GetShape()->Max());
  float dmin = posMin.Z() - pos.Z();
  float dmax = posMax.Z() - pos.Z();
  LogF("Watch %.3f - %.3f", _posWatch.Z() + dmin, _posWatch.Z() + dmax);
  LogF("Watch zoomed %.3f - %.3f", _posWatchZoomed.Z() + dmin, _posWatchZoomed.Z() + dmax);

  pos = _compass->Position();
  posMin = _compass->PositionModelToWorld(_compass->GetShape()->Min());
  posMax = _compass->PositionModelToWorld(_compass->GetShape()->Max());
  dmin = posMin.Z() - pos.Z();
  dmax = posMax.Z() - pos.Z();
  LogF("Compass %.3f - %.3f", _posCompass.Z() + dmin, _posCompass.Z() + dmax);
  LogF("Compass zoomed %.3f - %.3f", _posCompassZoomed.Z() + dmin, _posCompassZoomed.Z() + dmax);

  pos = _walkieTalkie->Position();
  posMin = _walkieTalkie->PositionModelToWorld(_walkieTalkie->GetShape()->Min());
  posMax = _walkieTalkie->PositionModelToWorld(_walkieTalkie->GetShape()->Max());
  dmin = posMin.Z() - pos.Z();
  dmax = posMax.Z() - pos.Z();
  LogF("WalkieTalkie %.3f - %.3f", _posWalkieTalkie.Z() + dmin, _posWalkieTalkie.Z() + dmax);
  LogF("WalkieTalkie zoomed %.3f - %.3f", _posWalkieTalkieZoomed.Z() + dmin, _posWalkieTalkieZoomed.Z() + dmax);

  pos = _notepad->Position();
  posMin = _notepad->PositionModelToWorld(_notepad->GetShape()->Min());
  posMax = _notepad->PositionModelToWorld(_notepad->GetShape()->Max());
  dmin = posMin.Z() - pos.Z();
  dmax = posMax.Z() - pos.Z();
  LogF("Notepad %.3f - %.3f", _posNotepad.Z() + dmin, _posNotepad.Z() + dmax);
  LogF("Notepad zoomed %.3f - %.3f", _posNotepadZoomed.Z() + dmin, _posNotepadZoomed.Z() + dmax);

  pos = _warrant->Position();
  posMin = _warrant->PositionModelToWorld(_warrant->GetShape()->Min());
  posMax = _warrant->PositionModelToWorld(_warrant->GetShape()->Max());
  dmin = posMin.Z() - pos.Z();
  dmax = posMax.Z() - pos.Z();
  LogF("Warrant %.3f - %.3f", _posWarrant.Z() + dmin, _posWarrant.Z() + dmax);
  LogF("Warrant zoomed %.3f - %.3f", _posWarrantZoomed.Z() + dmin, _posWarrantZoomed.Z() + dmax);

  pos = _gps->Position();
  posMin = _gps->PositionModelToWorld(_gps->GetShape()->Min());
  posMax = _gps->PositionModelToWorld(_gps->GetShape()->Max());
  dmin = posMin.Z() - pos.Z();
  dmax = posMax.Z() - pos.Z();
  LogF("GPS %.3f - %.3f", _posGPS.Z() + dmin, _posGPS.Z() + dmax);
  LogF("GPS zoomed %.3f - %.3f", _posGPSZoomed.Z() + dmin, _posGPSZoomed.Z() + dmax);

#endif
}

void DisplayMap::InitHUD()
{
  if (_map) _map->Center();
}

void DisplayMap::ResetHUD()
{
  if (_map)
  {
    _map->Reset();
    if (_map->CanCenter()) _map->Center(true);
  }
  if (_briefing)
  {
    UpdateWeaponsInBriefing();
    UpdateUnitsInBriefing();
    //    UpdateDebriefing();
    if (_updatePlanAsked) UpdatePlan();
  }
  GInput.cursorX = 0;
  GInput.cursorY = 0;
}

ControlObject *DisplayMap::OnCreateObject(int type, int idc, ParamEntryPar cls)
{
  switch (idc)
  {
  case IDC_MAP_WATCH:
    _watch = new Watch(this, idc, cls, true);
    return _watch;
  case IDC_MAP_COMPASS:
    _compass = new Compass(this, idc, cls, true);
    return _compass;
  case IDC_MAP_WALKIE_TALKIE:
    _walkieTalkie = new ControlObjectContainer(this, idc, cls);
    return _walkieTalkie;
    /*
    case IDC_MAP_NOTEPAD:
    _notepad = new Notepad(this, idc, cls);
    _notepad->SetBriefing(_briefing);
    return _notepad;
    */
  case IDC_MAP_NOTEPAD:
    // Notepad disabled, log is using instead
    return NULL;
  case IDC_MAP_WARRANT:
    _warrant = new ControlObjectContainer(this, idc, cls);
    return _warrant;
  case IDC_MAP_GPS:
    _gps = new ControlObjectContainer(this, idc, cls);
    return _gps;
  default:
    return Display::OnCreateObject(type, idc, cls);
  }
}

void DisplayMap::AskForUpdatePlan()
{
  if (GWorld && GWorld->HasMap()) UpdatePlan();
  else _updatePlanAsked = true;
}

void DisplayMap::UpdatePlan()
{
  TargetSide side = TSideUnknown;
  AIBrain *unit = GWorld->FocusOn();
  if (unit)
  {
    AIGroup *grp = unit->GetGroup();
    if (grp)
    {
      AICenter *center = grp->GetCenter();
      if (center) side = center->GetSide();
    }
  }
  if (_briefing && ::UpdatePlan(_briefing, side, unit ? unit->GetUnit() : NULL))
  {
    SwitchBriefingSection("__PLAN");
  }
  _updatePlanAsked = false;
}

void DisplayMap::ShowBriefing(bool show)
{
  if (show && !_briefingEnabled) return;
  if (GetCtrl(IDC_NOTEPAD_PICTURE))
  {
    GetCtrl(IDC_NOTEPAD_PICTURE)->ShowCtrl(show);
    if (_briefing)
    {
      if (!show && _map) FocusCtrl(_map->IDC());
      _briefingCtrl->ShowCtrl(show);
      if (show)
      {
        FocusCtrl(_briefingCtrl->IDC());
        ValidateMarker(_briefing, true, false);
      }
      else
      {
        if (_briefing->FindSection("__PLAN") >= 0)
          SwitchBriefingSection("__PLAN");
      }
    }
    IControl *ctrl = GetCtrl(IDC_MAP_NAME);
    if (ctrl) ctrl->ShowCtrl(show);
    ctrl = GetCtrl(IDC_MAP_NOTES);
    if (ctrl) ctrl->ShowCtrl(show);
    ctrl = GetCtrl(IDC_MAP_PLAN);
    if (ctrl) ctrl->ShowCtrl(show);
    ctrl = GetCtrl(IDC_MAP_GROUP);
    if (ctrl) ctrl->ShowCtrl(show);
    ctrl = GetCtrl(IDC_MAP_TEAM_SWITCH);
    if (ctrl) ctrl->ShowCtrl(show);
    UpdateButtons();
  }
}

RString GetNextBookmark(CHTMLContainer *html)
{
  static const char *bookmarks[] = {"__PLAN", "__BRIEFING", "Group", "TeamSwitch"};
  static int n = sizeof(bookmarks) / sizeof(*bookmarks);

  RString bookmark = html->ActiveBookmarkName();
  if (bookmark.GetLength() == 0) return RString();

  for (int i=0; i<n; i++)
  {
    if (stricmp(bookmark, bookmarks[i]) == 0)
    {
      for (int j=i+1; j<n; j++)
        if (html->FindSection(bookmarks[j]) >= 0) return bookmarks[j];
      for (int j=0; j<i; j++)
        if (html->FindSection(bookmarks[j]) >= 0) return bookmarks[j];
      return RString();
    }
  }
  return RString();
}

bool DisplayMap::OnKeyDown(int dikCode)
{
  if (dikCode == INPUT_DEVICE_XINPUT + XBOX_A && GetCtrl(IDC_NOTEPAD_PICTURE) && !GetCtrl(IDC_NOTEPAD_PICTURE)->IsVisible() && 

    _briefing)
  {
    _briefingCtrl->OnKeyDown(dikCode);
    return true;
  }
  if (_map && GetCtrl(IDC_MAP) && GetCtrl(IDC_MAP)->IsVisible())
  {
    if (_map->OnKeyDown(dikCode)) return true;
  }
  if (GetCtrl(IDC_NOTEPAD_PICTURE))
  {
    // X - mod
    switch (dikCode)
    {
    case DIK_UP:
      if (GetCtrl(IDC_NOTEPAD_PICTURE) && !GetCtrl(IDC_NOTEPAD_PICTURE)->IsVisible())
      {
        if (_briefing) ValidateMarker(_briefing, false, true);
        UpdateKeyHints();
        return true;
      }
      else break;
    case DIK_DOWN:
      if (GetCtrl(IDC_NOTEPAD_PICTURE) && !GetCtrl(IDC_NOTEPAD_PICTURE)->IsVisible())
      {
        if (_briefing) ValidateMarker(_briefing, true, true);
        UpdateKeyHints();
        return true;
      }
      else break;
    case DIK_SPACE:
      if (GetCtrl(IDC_NOTEPAD_PICTURE) && !GetCtrl(IDC_NOTEPAD_PICTURE)->IsVisible() && _briefing)
      {
        _briefingCtrl->OnKeyDown(dikCode);
        return true;
      }
      else break;
    }
  }
  return Display::OnKeyDown(dikCode);
}

bool DisplayMap::OnKeyUp(int dikCode)
{
  if (dikCode == INPUT_DEVICE_XINPUT + XBOX_A && GetCtrl(IDC_NOTEPAD_PICTURE) && !GetCtrl(IDC_NOTEPAD_PICTURE)->IsVisible() && 

    _briefing)
  {
    _briefingCtrl->OnKeyUp(dikCode);
    return true;
  }
  if (_map && GetCtrl(IDC_MAP) && GetCtrl(IDC_MAP)->IsVisible())
  {
    if (_map->OnKeyUp(dikCode)) return true;
  }
  if (GetCtrl(IDC_NOTEPAD_PICTURE))
  {
    // X - mod
    switch (dikCode)
    {
    case DIK_SPACE:
      if (GetCtrl(IDC_NOTEPAD_PICTURE) && !GetCtrl(IDC_NOTEPAD_PICTURE)->IsVisible() && _briefing)
      {
        _briefingCtrl->OnKeyUp(dikCode);
        return true;
      }
      else break;
    }
  }
  return Display::OnKeyUp(dikCode);
}

static RString GetTooltip(const UnitWeaponsInfo &info, RString href)
{
  const char *name = "fill1:";
  int n = strlen(name);
  if (strnicmp(href, name, n) == 0) return LocalizeString(IDS_TOOLTIP_FILL_ALL_SLOTS);

  name = "fill2:";
  n = strlen(name);
  if (strnicmp(href, name, n) == 0) return LocalizeString(IDS_TOOLTIP_FILL_ALL_SLOTS);

  name = "fill3:";
  n = strlen(name);
  if (strnicmp(href, name, n) == 0) return LocalizeString(IDS_TOOLTIP_FILL_ALL_SLOTS);

  name = "infoweapon:";
  n = strlen(name);
  if (strnicmp(href, name, n) == 0) return LocalizeString(IDS_TOOLTIP_INFORMATION);

  name = "infomagazine:";
  n = strlen(name);
  if (strnicmp(href, name, n) == 0) return LocalizeString(IDS_TOOLTIP_INFORMATION);

  name = "dropweapon:";
  n = strlen(name);
  if (strnicmp(href, name, n) == 0) return LocalizeString(IDS_TOOLTIP_DROP);

  name = "dropmagazine:";
  n = strlen(name);
  if (strnicmp(href, name, n) == 0) return LocalizeString(IDS_TOOLTIP_DROP);

  name = "slot:";
  n = strlen(name);
  if (strnicmp(href, name, n) == 0)
  {
    const char *ptr = href;
    int slot = *(ptr + n) - 'A';

    if (slot < WEAPON_SLOTS)
    {
      return RString();
    }
    else
    {
      const Magazine *magazine = info.magazines[slot - WEAPON_SLOTS];
      const MagazineType *type = magazine ? magazine->_type : NULL;
      if (type) return type->GetDisplayName();
      else return LocalizeString(IDS_EMPTY_SLOT);
    }
  }

  return RString();
}

void DisplayMap::UpdateButtons()
{
  IControl *bg = GetCtrl(IDC_NOTEPAD_PICTURE);

  IControl *ctrl = GetCtrl(IDC_MAP_BRIEFING_SHOW);
  if (ctrl)
  {
    if (_briefingEnabled && bg)
    {
      ctrl->ShowCtrl(true);
      ITextContainer *text = GetTextContainer(ctrl);
      if (text) text->SetText(LocalizeString(bg->IsVisible() ? IDS_MAP_BRIEFING_HIDE : IDS_MAP_BRIEFING_SHOW));
    }
    else ctrl->ShowCtrl(false);
  }
  ctrl = GetCtrl(IDC_MAP_BRIEFING_PAGE);
  if (ctrl)
  {
    if (_briefingEnabled && bg && bg->IsVisible() && _briefing)
    {
      RString str;
      RString next = GetNextBookmark(_briefing);
      if (stricmp(next, "__PLAN") == 0) str = LocalizeString(IDS_MAP_BRIEFING_PLAN);
      else if (stricmp(next, "__BRIEFING") == 0) str = LocalizeString(IDS_MAP_BRIEFING_NOTES);
      else if (stricmp(next, "Group") == 0) str = LocalizeString(IDS_MAP_BRIEFING_GROUP);
      else if (stricmp(next, "TeamSwitch") == 0) str = LocalizeString(IDS_MAP_BRIEFING_TEAM_SWITCH);

      ctrl->ShowCtrl(str.GetLength() > 0);
      ITextContainer *text = GetTextContainer(ctrl);
      if (text) text->SetText(str);
    }
    else ctrl->ShowCtrl(false);
  }
  ctrl = GetCtrl(IDC_MAP_GEAR);
  if (ctrl)
  {
    ctrl->ShowCtrl(!IsInGame() || GWorld->FocusOn() && GWorld->FocusOn()->LSIsAlive());
  }
}

RString DisplayMap::GetKeyHint(int button, bool &structured) const
{
  switch (button)
  {
  case INPUT_DEVICE_XINPUT + XBOX_A:
    if (GetCtrl(IDC_NOTEPAD_PICTURE) && _briefing && (_briefingCtrl->IsFocused() || !_briefingCtrl->IsVisible()))
    {
      const HTMLField *field = _briefing->GetActiveFieldKeyboard();

      if (field && field->href.GetLength() > 0)
      {
        const char *ptr = field->href;
        const char *name = "marker:";
        int n = strlen(name);
        if (strnicmp(ptr, name, n) == 0)
        {
          ptr += n;
          for (int i=0; i<markersMap.Size(); i++)
          {
            ArcadeMarkerInfo &mInfo = markersMap[i];
            if (stricmp(ptr, mInfo.name) == 0)
            {
              RString text = Localize(mInfo.text);
              if (text.GetLength() > 0)
                return Format(LocalizeString(IDS_BRIEFING_LINK_MARKER), (const char *)text);
            }
          }
        }
        if (field->text.GetLength() > 0)
          return Format(LocalizeString(IDS_BRIEFING_LINK_MARKER), (const char *)field->text);
        return LocalizeString(IDS_BRIEFING_LINK_SECTION);
      }
    }
    return RString();
  default:
    return Display::GetKeyHint(button, structured);
  }
}

UnitWeaponsInfo::UnitWeaponsInfo(AIBrain *u)
{
  Assert(u->GetLifeState() != LifeStateDead)
    unit = u;
  Update();
}

/*!
\patch 5129 Date 2/19/2007 by Jirka
- Fixed: The game crashed when gear screen was opened and the player was vanished (using scripting functions)
*/

void UnitWeaponsInfo::Update()
{
  Person *veh = unit ? unit->GetPerson() : NULL;
  if (!veh) return;

  name = veh->GetPersonName();
  weaponSlots = veh->GetType()->_weaponSlots;

  for (int i=0; i<WEAPON_SLOTS; i++) weapons[i] = NULL;
  for (int i=0; i<MAGAZINE_SLOTS; i++) magazines[i] = NULL;

  for (int i=0; i<veh->NWeaponSystems(); i++)
  {
    WeaponType *weapon = const_cast<WeaponType *>(veh->GetWeaponSystem(i));
    if (weapon->_scope < 2) continue;

    int slots = weapon->_weaponType;
    if (slots & MaskSlotPrimary)
      weapons[0] = weapon;
    if (slots & MaskSlotSecondary)
      weapons[1] = weapon;
    if (slots & MaskSlotBinocular)
    {
      if (!weapons[2])
        weapons[2] = weapon;
      else
        weapons[3] = weapon;
    }
    if (slots & MaskSlotHandGun)
    {
      weapons[4] = weapon;
    }
    if (slots & MaskSlotInventory)
    {
      // find the first empty slot
      bool done = false;
      for (int i=FIRST_INVENTORY_SLOT; i<WEAPON_SLOTS-1; i++)
      {
        if (!weapons[i])
        {
          weapons[i] = weapon;
          done = true;
          break;
        }
      }
      // not found, replace the last
      if(done == false) weapons[WEAPON_SLOTS - 1] = weapon;
    }
  }
  int item = 0, itemHandGun = FIRST_HANDGUN_SLOT;
  for (int i=0; i<veh->NMagazines(); i++)
  {
    Magazine *magazine = const_cast<Magazine *>(veh->GetMagazine(i));
    if (!magazine) continue;
    if (magazine->GetAmmo() == 0) continue;
    MagazineType *type = magazine->_type;
    if (type->_scope < 2) continue;
    int slots = type->_magazineType;
    if (slots & MaskSlotItem)
    {
      int nItems = GetItemSlotsCount(slots);
      if (nItems == 0) continue;
      if (item + nItems > FIRST_HANDGUN_SLOT)
      {
        LogF("Magazines doesn't fit to slots");
        continue;
      }
      for (int j=0; j<nItems; j++) magazines[item++] = magazine;
    }
    else if (slots & MaskSlotHandGunItem)
    {
      int nItems = GetHandGunItemSlotsCount(slots);
      if (nItems == 0) continue;
      if (itemHandGun + nItems > MAGAZINE_SLOTS)
      {
        LogF("Magazines doesn't fit to slots");
        continue;
      }
      for (int j=0; j<nItems; j++) magazines[itemHandGun++] = magazine;
    }
  }

  backpack = veh->GetBackpack();
}

bool UnitWeaponsInfo::IsMagazineUsable(const MagazineType *type)
{
  for (int i=0; i<WEAPON_SLOTS; i++)
  {
    const WeaponType *weapon = weapons[i];
    if (!weapon) continue;
    for (int j=0; j<weapon->_muzzles.Size(); j++)
    {
      const MuzzleType *muzzle = weapon->_muzzles[j];
      for (int k=0; k<muzzle->_magazines.Size(); k++)
        if (muzzle->_magazines[k] == type) return true;
    }
  }
  Ref<const WeaponType> always[2] =
  {
    WeaponTypes.New("Throw"),
    WeaponTypes.New("Put"),
    //    WeaponTypes.New("PipeBomb")
  };
  for (int i=0; i<sizeof(always)/sizeof(*always); i++)
  {
    const WeaponType *weapon = always[i];
    if (!weapon) continue;
    for (int j=0; j<weapon->_muzzles.Size(); j++)
    {
      const MuzzleType *muzzle = weapon->_muzzles[j];
      for (int k=0; k<muzzle->_magazines.Size(); k++)
        if (muzzle->_magazines[k] == type) return true;
    }
  }
  return false;
}

DisplayGetReady *GetBriefing() {return GBriefing;}

void UpdateWeaponsInBriefing(EntityAI *owner)
{
  DisplayMap *map = dynamic_cast<DisplayMap *>(GWorld->Map());
  if (map) map->UpdateWeaponsInBriefing(owner);

  if(GBriefing) GBriefing->UpdateWeaponsInBriefing(owner);

  DisplayGear *display = dynamic_cast<DisplayGear *>(GWorld->UserDialog());
  if (display) display->UpdateWeapons(owner);
}

void UpdateWeaponsInBriefing()
{
  UpdateWeaponsInBriefing(NULL);
}

void DisplayMap::UpdateWeaponsInBriefing(EntityAI *owner, bool load)
{
  // create structure
  AIBrain *me = GWorld->FocusOn();
  if (!me) return;
  AIGroup *grp = me->GetGroup();
  if (!grp) return;

  if(owner)
  {
    bool ownerPresent = false;
    for (int i=0; i<grp->NUnits(); i++)
    {
      AIUnit *unit = grp->GetUnit(i);
      if (unit && unit->GetVehicle() &&  unit->GetVehicle()== owner) 
      {
        ownerPresent = true;
        break;
      }
    }
    if(!ownerPresent) return;
  }

  _weaponsInfo._weapons.Resize(0);
  for (int i=0; i<grp->NUnits(); i++)
  {
    AIUnit *unit = grp->GetUnit(i);
    if (unit && unit->GetLifeState() != LifeStateDead) _weaponsInfo._weapons.Add(UnitWeaponsInfo(unit));
  }

  if (_selectWeapons && load)
  {
    // load weapons pool
    if (EnabledWeaponPool())
    {
      void CampaignLoadWeaponPool(FixedItemsPool &pool);
      CampaignLoadWeaponPool(_pool);
    }
    else
      _pool.Import(ParamEntryVal(ExtParsMission));
  }

  if (_child && _child->IDD() == IDD_GEAR)
  {
    DisplayGear *display = static_cast<DisplayGear *>(_child.GetRef());
    display->UpdateWeapons();
  }
}

#ifndef min
#ifndef _XBOX
inline int min(int a, int b)
{
  return a<b ? a : b;
}
#endif
#endif

static RString GetSkill(float skill)
{
  RString text(" (");
  if (skill <= 0.25)
    text = text + LocalizeString(IDS_SKILL_NOVICE);
  else if (skill <= 0.45)
    text = text + LocalizeString(IDS_SKILL_ROOKIE);
  else if (skill <= 0.65)
    text = text + LocalizeString(IDS_SKILL_RECRUIT);
  else if (skill <= 0.85)
    text = text + LocalizeString(IDS_SKILL_VETERAN);
  else
    text = text + LocalizeString(IDS_SKILL_EXPERT);
  return text + RString(")");
}

void UpdateUnitsInBriefing()
{
  // update briefing
  Display *CurrentDisplay();
  Display *disp = CurrentDisplay();
  DisplayMap *map = dynamic_cast<DisplayMap *>(disp);
  if (map)
  {
    map->UpdateUnitsInBriefing();
    map->UpdateWeaponsInBriefing();
  }

  // update map
  map = dynamic_cast<DisplayMap *>(GWorld->Map());
  if (map)
  {
    map->UpdateUnitsInBriefing();
    map->UpdateWeaponsInBriefing();
  }
}

/*!
\patch 1.75 Date 2/11/2002 by Jirka
- Improved: Group page in briefing 
*/
void DisplayMap::UpdateUnitsInBriefing()
{
  if (_xboxStyle) return;
  if (!_briefing) return;

#if _ENABLE_BRIEF_GRP
  // Group section

  int section = _briefing->FindSection("Group");
  if (section < 0)
  {
    section = _briefing->AddSection();
  }
  else
  {
    _briefing->InitSection(section);
  }

  _briefing->AddName(section, "Group");
  _briefing->AddText(section, LocalizeString(IDS_BRIEF_GROUP), HFH1, HALeft, false, false, "");
  _briefing->AddBreak(section, false);

  Person *player = GWorld->PlayerOn();
  AIBrain *unit = player ? player->Brain() : NULL;
  AIGroup *grp = unit ? unit->GetGroup() : NULL;
  if (grp)
  {
    for (int i=0; i<grp->NUnits(); i++)
    {
      AIUnit *unit = grp->GetUnit(i);
      if (!unit || !unit->IsUnit()) continue;
      if (unit->IsFreeSoldier())
      {
        char id[8]; sprintf(id, "%d: ", unit->ID());
        RString text = RString(id) +
          LocalizeString(IDS_SHORT_PRIVATE + unit->GetPerson()->GetRank()) +
          RString(". ") +
          unit->GetPerson()->GetPersonName();
        if (unit->IsGroupLeader())
          text = text + LocalizeString(IDS_BRIEF_GROUP_LEADER);
        text = text + GetSkill(unit->GetRawAbility());
#if _ENABLE_DATADISC
        char href[32]; sprintf(href, "gear:%d", unit->ID());
#else
        RString href = "";
#endif
        bool bold = unit->IsPlayer();
        _briefing->AddText(section, text, HFP, HALeft, false, bold, href);
        _briefing->AddBreak(section, false);
      }
      else
      {
        Transport *veh = unit->GetVehicleIn();
        Assert(veh);
        RString text = veh->GetType()->GetDisplayName();
        RString href = "";
        _briefing->AddText(section, text, HFP, HALeft, false, false, href);
        _briefing->AddBreak(section, false);
        AIBrain *u = veh->ObserverBrain();
        if (u)
        {
          RString text = LocalizeString(IDS_BRIEF_COMMANDER);
          text = text + LocalizeString(IDS_PRIVATE + u->GetPerson()->GetRank());
          text = text + RString(" ");
          text = text + u->GetPerson()->GetPersonName();
          if (u->IsGroupLeader())
            text = text + LocalizeString(IDS_BRIEF_GROUP_LEADER);
          text = text + GetSkill(u->GetRawAbility());
#if _ENABLE_DATADISC
          char href[32]; href[0] = 0;
          if (u->GetUnit())
            sprintf(href, "gear:%d", u->GetUnit()->ID());
#else
          RString href = "";
#endif
          bool bold = u->IsPlayer();
          _briefing->AddText(section, text, HFP, HALeft, false, bold, href);
          _briefing->AddBreak(section, false);
        }
        u = veh->DriverBrain();
        if (u)
        {
          RString text = LocalizeString(IDS_BRIEF_DRIVER);
          text = text + LocalizeString(IDS_PRIVATE + u->GetPerson()->GetRank());
          text = text + RString(" ");
          text = text + u->GetPerson()->GetPersonName();
          if (u->IsGroupLeader())
            text = text + LocalizeString(IDS_BRIEF_GROUP_LEADER);
          text = text + GetSkill(u->GetRawAbility());
#if _ENABLE_DATADISC
          char href[32]; href[0] = 0;
          if (u->GetUnit())
            sprintf(href, "gear:%d", u->GetUnit()->ID());
#else
          RString href = "";
#endif
          bool bold = u->IsPlayer();
          _briefing->AddText(section, text, HFP, HALeft, false, bold, href);
          _briefing->AddBreak(section, false);
        }
        u = veh->GunnerBrain();
        if (u)
        {
          RString text = LocalizeString(IDS_BRIEF_GUNNER);
          text = text + LocalizeString(IDS_PRIVATE + u->GetPerson()->GetRank());
          text = text + RString(" ");
          text = text + u->GetPerson()->GetPersonName();
          if (u->IsGroupLeader())
            text = text + LocalizeString(IDS_BRIEF_GROUP_LEADER);
          text = text + GetSkill(u->GetRawAbility());
#if _ENABLE_DATADISC
          char href[32]; href[0] = 0;
          if (u->GetUnit())
            sprintf(href, "gear:%d", u->GetUnit()->ID());
#else
          RString href = "";
#endif
          bool bold = unit->IsPlayer();
          _briefing->AddText(section, text, HFP, HALeft, false, bold, href);
          _briefing->AddBreak(section, false);
        }
        for (int j=0; j<veh->GetManCargo().Size(); j++)
        {
          Person *soldier = veh->GetManCargo()[j];
          if (!soldier) continue;
          AIBrain *agent = soldier->Brain();
          if (!agent) continue;
          RString text = LocalizeString(IDS_BRIEF_CARGO);
          text = text + LocalizeString(IDS_PRIVATE + soldier->GetRank());
          text = text + RString(" ");
          text = text + soldier->GetPersonName();
          if (agent->IsGroupLeader())
            text = text + LocalizeString(IDS_BRIEF_GROUP_LEADER);
          text = text + GetSkill(agent->GetRawAbility());
#if _ENABLE_DATADISC
          char href[32]; href[0] = 0;
          if (u && u->GetUnit())
            sprintf(href, "gear:%d", u->GetUnit()->ID());
#else
          RString href = "";
#endif
          bool bold = unit->IsPlayer();
          _briefing->AddText(section, text, HFP, HALeft, false, bold, href);
          _briefing->AddBreak(section, false);
        }
      }
    }
  }
  _briefing->FormatSection(section);

  // Team Switch section
  section = _briefing->FindSection("TeamSwitch");
  if (section < 0)
  {
    section = _briefing->AddSection();
  }
  else
  {
    _briefing->InitSection(section);
  }

  _briefing->AddName(section, "TeamSwitch");
  _briefing->AddText(section, LocalizeString(IDS_BRIEF_TEAM_SWITCH), HFH1, HALeft, false, false, "");
  _briefing->AddBreak(section, false);

  // collect the units
  bool multiplayer = GWorld->GetMode() == GModeNetware;
  OLinkPermNOArray(AIBrain) netUnits;
  if (multiplayer && unit) GetNetworkManager().GetSwitchableUnits(netUnits, unit);
  const OLinkPermNOArray(AIBrain) &units = multiplayer ? netUnits : GWorld->GetSwitchableUnits();
  for (int i=0; i<units.Size(); i++)
  {
    AIBrain *unit = units[i];
    if (!unit || !unit->LSIsAlive()) continue;
    if (!unit->GetPerson()) continue;

    RString GetRoleDescription(AIBrain *unit);

    RString text = GetRoleDescription(unit);
    RString href = Format("teamswitch:%d", unit->GetPerson()->ID().Encode());
    bool bold = unit->IsPlayer();
    _briefing->AddText(section, text, HFP, HALeft, false, bold, href);
    _briefing->AddBreak(section, false);
  }
  _briefing->FormatSection(section);

#endif // _ENABLE_BRIEF_GRP
}

DrawCoord SceneToScreen(Vector3Par pos)
{
  AspectSettings as;
  GEngine->GetAspectSettings(as);
  float invz = CameraZoom / pos.Z();
  float x3d = 1/as.leftFOV * pos.X() * invz + 0.5;
  float y3d = 0.5 - 1/as.topFOV * pos.Y() * invz;
  DrawCoord pt;
  pt.x = (x3d-as.uiTopLeftX)/(as.uiBottomRightX-as.uiTopLeftX);
  pt.y = (y3d-as.uiTopLeftY)/(as.uiBottomRightY-as.uiTopLeftY);
  return pt;
}

void DisplayMap::SetRadioText()
{
  if (_radioAlpha) _radioAlpha->SetText(LocalizeString(IDS_RADIO_UNASSIGNED));
  if (_radioBravo) _radioBravo->SetText(LocalizeString(IDS_RADIO_UNASSIGNED));
  if (_radioCharlie) _radioCharlie->SetText(LocalizeString(IDS_RADIO_UNASSIGNED));
  if (_radioDelta) _radioDelta->SetText(LocalizeString(IDS_RADIO_UNASSIGNED));
  if (_radioEcho) _radioEcho->SetText(LocalizeString(IDS_RADIO_UNASSIGNED));
  if (_radioFoxtrot) _radioFoxtrot->SetText(LocalizeString(IDS_RADIO_UNASSIGNED));
  if (_radioGolf) _radioGolf->SetText(LocalizeString(IDS_RADIO_UNASSIGNED));
  if (_radioHotel) _radioHotel->SetText(LocalizeString(IDS_RADIO_UNASSIGNED));
  if (_radioIndia) _radioIndia->SetText(LocalizeString(IDS_RADIO_UNASSIGNED));
  if (_radioJuliet) _radioJuliet->SetText(LocalizeString(IDS_RADIO_UNASSIGNED));

  Person *player = GWorld->GetRealPlayer();
  if (!player || player->IsDamageDestroyed()) return;

  for (int i=sensorsMap.Size()-1; i>=0; i--)
  {
    Vehicle *veh = sensorsMap[i];
    if (!veh) continue;
    Detector *det = dyn_cast<Detector>(veh);
    Assert(det);
    if (det->IsActive() && !det->IsRepeating()) continue;
    ITextContainer *ctrl;
    int ids;
    switch (det->GetActivationBy())
    {
    case ASAAlpha:
      ctrl = _radioAlpha; ids = IDS_RADIO_ALPHA;
      goto showRadio;
    case ASABravo:
      ctrl = _radioBravo; ids = IDS_RADIO_BRAVO;
      goto showRadio;
    case ASACharlie:
      ctrl = _radioCharlie; ids = IDS_RADIO_CHARLIE;
      goto showRadio;
    case ASADelta:
      ctrl = _radioDelta; ids = IDS_RADIO_DELTA;
      goto showRadio;
    case ASAEcho:
      ctrl = _radioEcho; ids = IDS_RADIO_ECHO;
      goto showRadio;
    case ASAFoxtrot:
      ctrl = _radioFoxtrot; ids = IDS_RADIO_FOXTROT;
      goto showRadio;
    case ASAGolf:
      ctrl = _radioGolf; ids = IDS_RADIO_GOLF;
      goto showRadio;
    case ASAHotel:
      ctrl = _radioHotel; ids = IDS_RADIO_HOTEL;
      goto showRadio;
    case ASAIndia:
      ctrl = _radioIndia; ids = IDS_RADIO_INDIA;
      goto showRadio;
    case ASAJuliet:
      ctrl = _radioJuliet; ids = IDS_RADIO_JULIET;
      goto showRadio;
showRadio:
      if (ctrl)
      {
        RString text = det->GetText();
        if (stricmp(text, "null") == 0) continue;
        text = Localize(text);
        if (text.GetLength() > 0)
          ctrl->SetText(text);
        else
          ctrl->SetText(LocalizeString(ids));
      }
    }
  }
}

/*!
\patch 5142 Date 3/20/2007 by Jirka
- Fixed: Map info (commands, friendly and enemy units) is now saving into game save
*/

LSError DisplayMap::Serialize(ParamArchive &ar)
{
  CHECK(ar.Serialize("showMap", _showMap, 1, true))  
    CHECK(ar.Serialize("showWatch", _showWatch, 1, true))  
    CHECK(ar.Serialize("showCompass", _showCompass, 1, true))  
    CHECK(ar.Serialize("showGPS", _showGPS, 1, false))
    if (_map)
    {
      // ignore the return value, map may not be saved
      _map->Serialize(ar, "Map", 1);
    }
    return LSOK;
}

LSError DisplayMap::SerializeParams(ParamArchive &ar)
{
  if (_compass) CHECK(ar.Serialize("Compass", *(SerializeClass *)_compass, 1));
  if (_watch) CHECK(ar.Serialize("Watch", *(SerializeClass *)_watch, 1));
  if (_walkieTalkie) CHECK(ar.Serialize("WalkieTalkie", *(SerializeClass *)_walkieTalkie, 1));
  if (_notepad) CHECK(ar.Serialize("Notepad", *(SerializeClass *)_notepad, 1));
  if (_warrant) CHECK(ar.Serialize("Warrant", *(SerializeClass *)_warrant, 1));
  if (_gps) CHECK(ar.Serialize("GPS2", *(SerializeClass *)_gps, 1));
  return LSOK;
}

bool ParseUserParamsArchive(ParamArchiveLoad &ar, GameDataNamespace *globals);
bool ParseUserParamsArchive(ParamArchiveSave &ar, GameDataNamespace *globals);
void SaveUserParamsArchive(ParamArchiveSave &ar);

void DisplayMap::LoadParams()
{
  if (!_saveParams) return;

  GameDataNamespace globals(NULL, RString(), false); // save - no global namespace
  ParamArchiveLoad ar;
  if (!ParseUserParamsArchive(ar, &globals)) return;

  ParamArchive arSubcls;
  if (!ar.OpenSubclass("MainMap", arSubcls))
  {
    // TODO: load failed
    return;
  }
  if (SerializeParams(arSubcls) != LSOK)
  {
    // TODO: load failed
    return;
  }
}

void DisplayMap::SaveParams()
{
  if (!_saveParams) return;
  // when out of memory, saving could fail severely
  if (IsOutOfMemory()) return;

  GameDataNamespace globals(NULL, RString(), false); // save - no global namespace
  ParamArchiveSave ar(UserInfoVersion);
  if (!ParseUserParamsArchive(ar, &globals)) return;

  ParamArchive arSubcls;
  if (!ar.OpenSubclass("MainMap", arSubcls))
  {
    // TODO: save failed
    return;
  }
  if (SerializeParams(arSubcls) != LSOK)
  {
    // TODO: save failed
    return;
  }

  SaveUserParamsArchive(ar);
}

Control *DisplayMap::OnCreateCtrl(int type, int idc, ParamEntryPar cls)
{
  switch (idc)
  {
  case IDC_MAP:
    {
      _map = new CStaticMapMain(this, idc, cls);
      return _map;
    }
  case IDC_NOTEPAD_PICTURE:
    {
      // Notepad disabled, log is using instead
      return NULL;
    }
#if _ENABLE_IDENTITIES
  case IDC_DIARY_TOPIC_LIST:
  case IDC_DIARY_TOPIC_LISTINDEX:
    {
      CMapListBox *mapList = new CMapListBox(this,idc,cls);
      return mapList;
    }
  case IDC_DIARY_TOPIC_HTML_GROUP:
    {
      CMapCGroup *cgroup  = new CMapCGroup(this,idc,cls);
      return cgroup;
    }
#endif
  }

  Control *ctrl = Display::OnCreateCtrl(type, idc, cls);
  switch (idc)
  {
  case IDC_RADIO_ALPHA:
    _radioAlpha = GetTextContainer(ctrl);
    break;
  case IDC_RADIO_BRAVO:
    _radioBravo = GetTextContainer(ctrl);
    break;
  case IDC_RADIO_CHARLIE:
    _radioCharlie = GetTextContainer(ctrl);
    break;
  case IDC_RADIO_DELTA:
    _radioDelta = GetTextContainer(ctrl);
    break;
  case IDC_RADIO_ECHO:
    _radioEcho = GetTextContainer(ctrl);
    break;
  case IDC_RADIO_FOXTROT:
    _radioFoxtrot = GetTextContainer(ctrl);
    break;
  case IDC_RADIO_GOLF:
    _radioGolf = GetTextContainer(ctrl);
    break;
  case IDC_RADIO_HOTEL:
    _radioHotel = GetTextContainer(ctrl);
    break;
  case IDC_RADIO_INDIA:
    _radioIndia = GetTextContainer(ctrl);
    break;
  case IDC_RADIO_JULIET:
    _radioJuliet = GetTextContainer(ctrl);
    break;
  case IDC_MAP_NAME:
    _name = GetTextContainer(ctrl);
    {
      RString name;
      if (GWorld->GetMode() == GModeNetware)
      {
        const MissionHeader *header = GetNetworkManager().GetMissionHeader();
        if (header) name = header->GetLocalizedMissionName();
      }
      if (name.GetLength() == 0)
      {
        name = Localize(CurrentTemplate.intel.briefingName);
        if (name.GetLength() == 0)
        {
          name = Glob.header.filenameReal;
          // user mission file name is encoded
          if (IsUserMission()) name = DecodeFileName(name);
        }
      }
#if _VBS3
      // Quick fix for AAR mission name
      if(stricmp(name,"__AAR")==0)
        name = RString("After Action Review");
#endif
      _name->SetText(name);
    }
    break;
  case IDC_BRIEFING:
    _briefing = GetHTMLContainer(ctrl);
    _briefingCtrl = ctrl;
    break;
  case IDC_MAP_NOTES:
    _bookmark1 = GetTextContainer(ctrl);
    break;
  case IDC_MAP_PLAN:
    _bookmark2 = GetTextContainer(ctrl);
    break;
  case IDC_MAP_GROUP:
    _bookmark3 = GetTextContainer(ctrl);
    break;
  case IDC_MAP_TEAM_SWITCH:
    _bookmark4 = GetTextContainer(ctrl);
    break;
#ifndef _XBOX
  case IDC_WARRANT:
    return new CWarrant(this, idc, cls);
#endif
  case IDC_GPS:
    _gpsCtrl = GetTextContainer(ctrl);
    break;
  case IDC_GPS_ALT:
    _gpsAltCtrl = GetTextContainer(ctrl);
    break;
  case IDC_GPS_HEADING:
    _gpsHeadingCtrl = GetTextContainer(ctrl);
    break;
  case IDC_GETREADY_TITLE:
    {
      ITextContainer *text = GetTextContainer(ctrl);
      AIBrain *agent = GWorld->FocusOn();
      AIUnit *unit = agent ? agent->GetUnit() : NULL;
      AIGroup *grp = unit ? unit->GetGroup() : NULL;
      if (grp)
      {
        char buffer[256];
        sprintf
          (
          buffer, LocalizeString(IDS_BRIEFING),
          (const char *)unit->GetPerson()->GetPersonName(),
          (const char *)grp->GetName(),
          unit->ID()
          );
        text->SetText(buffer);
      }
      else text->SetText("");
    }
    break;

    /*
    case IDC_NOTEPAD_PICTURE:
    {
    RString FindPicture(RString name);

    RString text = FindPicture(cls >> "picturePlan");
    text.Lower();
    _picturePlan = GlobLoadTexture(text);
    if (_picturePlan) _picturePlan->SetUsageType(Texture::TexUI); // no limits

    text = FindPicture(cls >> "pictureNotes");
    text.Lower();
    _pictureNotes = GlobLoadTexture(text);
    if (_pictureNotes) _pictureNotes->SetUsageType(Texture::TexUI); // no limits

    text = FindPicture(cls >> "pictureGroup");
    text.Lower();
    _pictureGroup = GlobLoadTexture(text);
    if (_pictureGroup) _pictureGroup->SetUsageType(Texture::TexUI); // no limits

    text = FindPicture(cls >> "picture");
    text.Lower();
    _picture = GlobLoadTexture(text);
    if (_picture) _picture->SetUsageType(Texture::TexUI); // no limits
    }
    break;
    */
  }
  return ctrl;
}

void ActivateSensor(ArcadeSensorActivation activ)
{
  Person *player = GWorld->GetRealPlayer();
  if (!player || player->IsDamageDestroyed()) return;

  for (int i=0; i<sensorsMap.Size(); i++)
  {
    Vehicle *veh = sensorsMap[i];
    if (!veh) continue;
    Detector *det = dyn_cast<Detector>(veh);
    Assert(det);
    if (det->GetActivationBy() == activ)
    {
      if (stricmp(det->GetText(), "null") == 0) continue;
      if (!det->IsActive() || det->IsRepeating())
      {
        det->DoActivate();
        GetNetworkManager().DetectorActivation(det, true);
      }
    }
  }
}

void DisplayMap::OnButtonClicked(int idc)
{
  switch (idc)
  {
  case IDC_RADIO_ALPHA:
    ActivateSensor(ASAAlpha);
    break;
  case IDC_RADIO_BRAVO:
    ActivateSensor(ASABravo);
    break;
  case IDC_RADIO_CHARLIE:
    ActivateSensor(ASACharlie);
    break;
  case IDC_RADIO_DELTA:
    ActivateSensor(ASADelta);
    break;
  case IDC_RADIO_ECHO:
    ActivateSensor(ASAEcho);
    break;
  case IDC_RADIO_FOXTROT:
    ActivateSensor(ASAFoxtrot);
    break;
  case IDC_RADIO_GOLF:
    ActivateSensor(ASAGolf);
    break;
  case IDC_RADIO_HOTEL:
    ActivateSensor(ASAHotel);
    break;
  case IDC_RADIO_INDIA:
    ActivateSensor(ASAIndia);
    break;
  case IDC_RADIO_JULIET:
    ActivateSensor(ASAJuliet);
    break;
  case IDC_MAP_NOTES:
    SwitchBriefingSection("__BRIEFING");
    break;
  case IDC_MAP_PLAN:
    SwitchBriefingSection("__PLAN");
    break;
#if _ENABLE_BRIEF_GRP
  case IDC_MAP_GROUP:
    SwitchBriefingSection("Group");
    break;
  case IDC_MAP_TEAM_SWITCH:
    SwitchBriefingSection("TeamSwitch");
    break;
#endif
  case IDC_MAP_TEXTURES:
    if (_map)
    {
      _map->ShowScale(!_map->IsShowingScale());
      UpdateTexturesButton();
    }
    break;
  case IDC_MAP_BRIEFING_SHOW:
    if (_briefingEnabled && GetCtrl(IDC_NOTEPAD_PICTURE))
    {
      bool show = !GetCtrl(IDC_NOTEPAD_PICTURE)->IsVisible();
      ShowBriefing(show);
      UpdateButtons();
    }
    break;
  case IDC_MAP_BRIEFING_PAGE:
    if (_briefingEnabled && GetCtrl(IDC_NOTEPAD_PICTURE) && _briefing && idc == _briefingCtrl->IDC())
    {
      RString next = GetNextBookmark(_briefing);
      if (next.GetLength() > 0) SwitchBriefingSection(next);
      UpdateButtons();
    }
    break;
  case IDC_MAP_PREV_MARKER:
    if (GetCtrl(IDC_NOTEPAD_PICTURE) && !GetCtrl(IDC_NOTEPAD_PICTURE)->IsVisible())
    {
      if (_briefing) ValidateMarker(_briefing, false, true);
      UpdateKeyHints();
    }
    break;
  case IDC_MAP_NEXT_MARKER:
    if (GetCtrl(IDC_NOTEPAD_PICTURE) && !GetCtrl(IDC_NOTEPAD_PICTURE)->IsVisible())
    {
      if (_briefing) ValidateMarker(_briefing, true, true);
      UpdateKeyHints();
    }
    break;
  case IDC_MAP_GEAR:
    {
      AIBrain *me = GWorld->FocusOn();
      bool inGame = IsInGame();
      if (!inGame || me && me->LSIsAlive())
      {
        if (inGame)
          CreateChild(CreateGearSimpleDialog(this, me));
        else
          CreateChild(new DisplayGear(this, _enableSimulation, _weaponsInfo, _pool, me, false));
      }
    }
    break;
  case IDC_CANCEL:
    break;
  }
}

void DisplayMap::UpdateTexturesButton()
{
  ITextContainer *button = GetTextContainer(GetCtrl(IDC_MAP_TEXTURES));
  if (button)
  {
    if (_map && _map->IsVisible())
    {
      button->GetControl()->ShowCtrl(true);
      if (_map->IsShowingScale())
        button->SetText(LocalizeString(IDS_ARCMAP_SHOW_TEXTURES));
      else
        button->SetText(LocalizeString(IDS_ARCMAP_HIDE_TEXTURES));
    }
    else
      button->GetControl()->ShowCtrl(false);
  }
}

void DisplayMap::AnimateTo(Vector3Par position)
{
  float invSizeLand = 1.0 / (LandGrid * LandRange);
  Vector3 curPos = _map->GetCenter();
  float curScale = _map->GetScale();
  float endScale = curScale;
  float diff = curPos.Distance(position);
  float scale = 2.0 * diff * invSizeLand;

  saturate(scale,_map->_scaleMin,_map->_scaleMax);

  int sizeAnim = _map->_animation.Size();
  if (sizeAnim > 0)
  {
    // animation interrupted, save end scale
    MapAnimationPhase &phase = _map->_animation[sizeAnim - 1];
    endScale = phase.scale;
  }
  _map->ClearAnimation();
  const float maxSpeed = 3000;
  float minTime = diff/maxSpeed;
  if (scale > curScale)
  {
    float time = log(scale / curScale);
    _map->AddAnimationPhase(time, scale, curPos);
    float moveTime = floatMax(minTime,1);
    _map->AddAnimationPhase(moveTime, scale, position);
  }
  else
  {
    float time = floatMax(minTime, diff * invSizeLand / curScale);
    _map->AddAnimationPhase(time, curScale, position);
    scale = curScale;
  }
  if (scale > endScale)
  {
    float time = log(scale / endScale);
    _map->AddAnimationPhase(time, endScale, position);
  }
  _map->CreateInterpolator();

  if (GetCtrl(IDC_NOTEPAD_PICTURE))
  {
    ShowBriefing(false);
    UpdateButtons();
  }
}

/*!
\patch 1.78 Date 7/10/2002 by Jirka
- Fixed: In briefing weapon disappear if trying to assign where doesn't fit
\patch 1.8 Date 9/16/2002 by Ondra
- Fixed: When some units are dead, links in notebook Group section may
point to wrong units.
*/

void DisplayMap::OnHTMLLink(int idc, RString link)
{
  const char *ptr = link;
  const char *name = "marker:";
  int n = strlen(name);
  if (strnicmp(ptr, name, n) == 0)
  {
    ptr += n;
    for (int i=0; i<markersMap.Size(); i++)
    {
      ArcadeMarkerInfo &mInfo = markersMap[i];
      if (stricmp(ptr, mInfo.name) == 0)
      {
        AnimateTo(mInfo.position);
        _map->SetActiveMarker(i);
        return;
      }
    }
    return;
  }

  name = "gear:";
  n = strlen(name);
  if (strnicmp(ptr, name, n) == 0)
  {
    AIBrain *unit = GWorld->FocusOn();
    bool inGame = IsInGame();
    if (!inGame || unit && unit->LSIsAlive())
    {
      ptr += n;
      int unitId = atoi(ptr);
      for (int i=0; i<_weaponsInfo._weapons.Size(); i++)
      {
        const UnitWeaponsInfo &info = _weaponsInfo._weapons[i];
        if (info.unit && info.unit->GetUnit() && info.unit->GetUnit()->ID() == unitId)
        {
          unit = info.unit;
          break;
        }
      }
      if (inGame)
        CreateChild(CreateGearSimpleDialog(this, unit));
      else
        CreateChild(new DisplayGear(this, _enableSimulation, _weaponsInfo, _pool, unit, false));
    }
    return;
  }

  name = "teamswitch:";
  n = strlen(name);
  if (strnicmp(ptr, name, n) == 0)
  {
    if (Glob.config.IsEnabled(DTMap))
    {
      ptr += n;
      int id = atoi(ptr);

      // collect the units
      bool multiplayer = GWorld->GetMode() == GModeNetware;
      Person *player = GWorld->PlayerOn();
      AIBrain *unit = player ? player->Brain() : NULL;
      OLinkPermNOArray(AIBrain) netUnits;
      if (multiplayer && unit) GetNetworkManager().GetSwitchableUnits(netUnits, unit);
      const OLinkPermNOArray(AIBrain) &units = multiplayer ? netUnits : GWorld->GetSwitchableUnits();

      for (int i=0; i<units.Size(); i++)
      {
        AIBrain *unit = units[i];
        if (!unit || !unit->LSIsAlive()) continue;
        if (!unit->GetPerson()) continue;

        if (unit->GetPerson()->ID() == id)
        {
          AnimateTo(unit->Position(unit->GetRenderVisualState()));
          return;
        }
      }
    }
    return;
  }
}

void DisplayMap::OnDraw(EntityAI *vehicle, float alpha)
{
  if (_child && _child->IDD() == IDD_GEAR)
  {
    // special drawing under Gear dialog
    if (_map && _map->IsVisible())
    {
      UIViewport *vp = Create2DViewport();
      _map->OnDraw(vp, alpha);
      Destroy2DViewport(vp);
    }
    return;
  }

  // show items based on mission and availability
  if (_map) _map->ShowCtrl(_showMap && GWorld->HasPlayerMap() || _showGPS && GWorld->HasPlayerGPS());
  if (_compass) _compass->ShowCtrl(_showCompass && GWorld->HasPlayerCompass());
  if (_watch) _watch->ShowCtrl(_showWatch && GWorld->HasPlayerWatch());
  if (_gps) _gps->ShowCtrl(_showGPS && GWorld->HasPlayerGPS());

  // Compass orientation
  if (_compass)
  {
    Camera *camera = GScene->GetCamera();
    if (camera)
    {
#if _VBS2 && !_VBS2_LITE
      Vector3 dir;
#if _VBS3 // ShowArmAMap difficulty setting
      if (Glob.config.IsEnabled(DTShowArmAMap))
        dir = camera->Direction();
      else
#endif
        dir = _compassDir;
#else
      Vector3Val dir = camera->Direction();
#endif
      float scale = _compass->RenderVisualState().Scale();
      Matrix3 orient(MUpAndDirection, Vector3(0, 0, -1), Vector3(dir.X(), dir.Z(), dir.Y()));
      _compass->SetOrient(orient);
      _compass->SetScale(scale);
    }
  }
  // Radio slots
  SetRadioText();
  // Show / hide mission name
  IControl *ctrl = GetCtrl(IDC_MAP_NAME);
  if (ctrl) ctrl->ShowCtrl(_briefing ? _briefing->ActiveBookmark() >= 0 : false);
  // GPS position
  if (_gpsCtrl)
  {
    Object *cameraOn = GWorld->CameraOn();
    if (cameraOn)
    {
      //VBS supports bigger grids
      char buffer[32];
      PositionToAA11(cameraOn->RenderVisualState().Position(), buffer);
      _gpsCtrl->SetText(buffer);
    }
    else
      _gpsCtrl->SetText("----");
  }

  if (_gpsAltCtrl && vehicle && vehicle->GetShape())
  {
    Vector3Val pos = vehicle->RenderVisualState().Position();
    float y = pos.Y() + vehicle->GetShape()->Min().Y();
    const float elevationOffset = GLandscape->GetLandElevationOffset();
   // float y0 = GLOB_LAND->RoadSurfaceYAboveWater(pos.X(), pos.Z()); 
    y += elevationOffset;
    int h = int(y /* - y0*/);
    saturate(h, 0, 99999);
    char buffer[32];
    sprintf(buffer, "%d", h);
    _gpsAltCtrl->SetText(cc_cast(buffer));
  }
  if (_gpsHeadingCtrl)
  {
    Camera &cam = *GLOB_SCENE->GetCamera();
    Vector3Val dir = cam.Direction();

    // avoid singularity when heading up
    float angle;
    if (fabs(dir.X()) + fabs(dir.Z()) > 1e-3)
      angle = atan2(dir.X(), dir.Z());
    else
      angle = 0;

    int degAngle = toInt(angle * (180.0f / H_PI));
    if (degAngle < 0) degAngle += 360;
    BString<16> buffer;
    sprintf(buffer, "%d", degAngle);

    _gpsHeadingCtrl->SetText(cc_cast(buffer));
  }

  // FIX: do not show notepad for dead units
#if _VBS3 // allow notepad to be shown in new editor
  if (_idd != IDD_MISSION_EDITOR)
  {
#endif
    Person *player = GWorld->GetRealPlayer();
    AIBrain *playerUnit = player ? player->Brain() : NULL;
    if (!playerUnit || playerUnit->GetLifeState() == LifeStateDead) ShowNotepad(false);
#if _VBS3 // allow notepad to be shown in new editor
  }
#endif

  // notepad texture
  ctrl = GetCtrl(IDC_NOTEPAD_PICTURE);
  if (ctrl && ctrl->IsVisible() && _briefing)
  {
    DoAssert(ctrl->GetType() == CT_STATIC);
    CStatic *text = static_cast<CStatic *>(ctrl);
    switch (_briefing->ActiveBookmark())
    {
    case 0:
      text->SetTexture(_picturePlan);
      break;
    case 1:
      text->SetTexture(_pictureNotes);
      break;
#if _ENABLE_BRIEF_GRP
    case 2:
      text->SetTexture(_pictureGroup);
      break;
#endif
    default:
      text->SetTexture(_picture);
      break;
    }
  }

  // if possible, do not allow map gain focus
  if (_map && _map->IsFocused() && _briefing && _briefingCtrl->IsVisible()) FocusCtrl(_briefingCtrl->IDC());

  if (_map && _map->IsVisible())
  {
#ifdef _XBOX
    if (!GetCtrl(IDC_NOTEPAD_PICTURE) || !GetCtrl(IDC_NOTEPAD_PICTURE)->IsVisible())
    {
      bool alt = IsWatchRequired();
      if (alt)
      {
        if (_cursor != CursorWatch)
        {
          _cursor = CursorWatch;
          SetCursor(_map->OverrideCursor("Watch"));
        }
      }
      else
      {
        if (_cursor != CursorTrack)
        {
          _cursor = CursorTrack;
          SetCursor(_map->OverrideCursor("Track"));
        }
      }
    }
    else
    {
      if (_cursor != CursorArrow)
      {
        _cursor = CursorArrow;
        SetCursor(NULL);
      }
    }
#else
    float mouseX = 0.5 + GInput.cursorX * 0.5;
    float mouseY = 0.5 + GInput.cursorY * 0.5;

    //  saturate(mouseX,0,1);
    //  saturate(mouseY,0,1);

    IControl *ctrl = GetCtrl(mouseX, mouseY);
    if (ctrl && ctrl == _map)
    {
      if (_map->_dragging || _map->_selecting)
      {
        if (_cursor != CursorMove)
        {
          _cursor = CursorMove;
          SetCursor(_map->OverrideCursor("Move"));
        }
      }
      else if (_map->_moving)
      {
        if (_cursor != CursorScroll)
        {
          _cursor = CursorScroll;
          SetCursor(_map->OverrideCursor("Scroll"));
        }
      }
      else
      {
        if (_cursor != CursorTrack)
        {
          _cursor = CursorTrack;
          SetCursor(_map->OverrideCursor("Track"));
        }
      }
    }
    else
    {
      if (_cursor != CursorArrow)
      {
        _cursor = CursorArrow;
        SetCursor(_map->OverrideCursor("Arrow"));
      }
    }
#endif
  }

  Display::OnDraw(vehicle, alpha);
}

void DisplayMap::OnHide()
{
  if (_map) _map->OnHide();
}

/*!
\patch 5129 Date 2/20/2007 by Ondra
- Improved: Delay when returning from map to game is now much shorter.
*/
void DisplayMap::OnSimulate(EntityAI *vehicle)
{
  Display::OnSimulate(vehicle);
  if (_map && _map->IsVisible())
  {
    _map->ProcessCheats();

#ifndef _XBOX

    if (!_map->IsMoving())
    {
      float ratioW =(float)GEngine->WidthBB()/GEngine->Width2D();
      float ratioH =(float)GEngine->HeightBB()/GEngine->Height2D();
      AspectSettings asp;
      GEngine->GetAspectSettings(asp);
#ifdef _XBOX
      //float safe = 0.075; // Safe area (85 %)
      float sfX = (-asp.uiTopLeftX + 0.075) * ratioW;
      float sfY = (-asp.uiTopLeftY + 0.075) * ratioH;
#else
      float sfX = (-asp.uiTopLeftX) * ratioW;
      float sfY = (-asp.uiTopLeftY) * ratioH;
#endif

      float mouseX = 0.5 + GInput.cursorX * 0.5;
      float mouseY = 0.5 + GInput.cursorY * 0.5;

      saturate(mouseX,sfX,1-sfX);
      saturate(mouseY,sfY,1-sfY);

      // automatic map movement on edges
      bool moveEdge = false;
      float dif = 0.01 - (mouseX-sfX);
      if (dif > 0) _map->ScrollX(0.003 * exp(dif * 100.0f)),moveEdge = true;
      else
      {
        dif = (mouseX+sfX) - 0.99;
        if (dif > 0) _map->ScrollX(-0.003 * exp(dif * 100.0f)),moveEdge = true;
      }

      dif = 0.01 - (mouseY-sfY);
      if (dif > 0) _map->ScrollY(0.003 * exp(dif * 100.0f)),moveEdge = true;
      else
      {
        dif = (mouseY+sfY) - 0.99;
        if (dif > 0) _map->ScrollY(-0.003 * exp(dif * 100.0f)),moveEdge = true;
      }

      if (!_map->_interpolator)
      {
        // we are willing to dedicate 50 ms to preload if necessary
        // unless player is moving a lot, it should not be used
        bool done = GWorld->PreloadAroundCamera(0.050f); // keep reasonable framerate
        (void)done;
#if 0 // _ENABLE_CHEATS
        DIAG_MESSAGE(100, done ? "Scene preload done" : "Scene preload not done");
#endif
      }
    }
#endif
  }
}

/*!
\patch 1.61 Date 5/29/2002 by Jirka
- Fixed: Map has wrong center when notepad is hidded
*/

void DisplayMap::AdjustMapVisibleRect()
{
  float x = _map->X();
  float y = _map->Y();
  float w = _map->W();
  float h = _map->H();

  if (!_notepad || !_notepad->IsVisible())
  {
    _map->SetVisibleRect(x, y, w, h);
    return;
  }

  // adjust map visible rectangle
  Vector3 min = _notepad->FutureVisualState().PositionModelToWorld
    (
    _notepad->GetShape()->Min()
    );
  Vector3 max = _notepad->FutureVisualState().PositionModelToWorld
    (
    _notepad->GetShape()->Max()
    );
  DrawCoord pt, ptMin, ptMax;
  ptMin.x = 1; ptMin.y = 1;
  ptMax.x = 0; ptMax.y = 0;

  pt = SceneToScreen(min);
  saturateMin(ptMin.x, pt.x);
  saturateMin(ptMin.y, pt.y);
  saturateMax(ptMax.x, pt.x);
  saturateMax(ptMax.y, pt.y);
  pt = SceneToScreen(Vector3(min.X(), min.Y(), max.Z()));
  saturateMin(ptMin.x, pt.x);
  saturateMin(ptMin.y, pt.y);
  saturateMax(ptMax.x, pt.x);
  saturateMax(ptMax.y, pt.y);
  pt = SceneToScreen(Vector3(min.X(), max.Y(), min.Z()));
  saturateMin(ptMin.x, pt.x);
  saturateMin(ptMin.y, pt.y);
  saturateMax(ptMax.x, pt.x);
  saturateMax(ptMax.y, pt.y);
  pt = SceneToScreen(Vector3(min.X(), max.Y(), max.Z()));
  saturateMin(ptMin.x, pt.x);
  saturateMin(ptMin.y, pt.y);
  saturateMax(ptMax.x, pt.x);
  saturateMax(ptMax.y, pt.y);
  pt = SceneToScreen(Vector3(max.X(), min.Y(), min.Z()));
  saturateMin(ptMin.x, pt.x);
  saturateMin(ptMin.y, pt.y);
  saturateMax(ptMax.x, pt.x);
  saturateMax(ptMax.y, pt.y);
  pt = SceneToScreen(Vector3(max.X(), min.Y(), max.Z()));
  saturateMin(ptMin.x, pt.x);
  saturateMin(ptMin.y, pt.y);
  saturateMax(ptMax.x, pt.x);
  saturateMax(ptMax.y, pt.y);
  pt = SceneToScreen(Vector3(max.X(), max.Y(), min.Z()));
  saturateMin(ptMin.x, pt.x);
  saturateMin(ptMin.y, pt.y);
  saturateMax(ptMax.x, pt.x);
  saturateMax(ptMax.y, pt.y);
  pt = SceneToScreen(max);
  saturateMin(ptMin.x, pt.x);
  saturateMin(ptMin.y, pt.y);
  saturateMax(ptMax.x, pt.x);
  saturateMax(ptMax.y, pt.y);

  float aLeft = ptMin.x > x ? (ptMin.x - x) * h : 0;
  float aRight = ptMax.x < x + w ? (x + w - ptMax.x) * h : 0;
  float aTop = ptMin.y > y ? (ptMin.y - y) * w : 0;
  float aBottom = ptMax.y < y + h ? (y + h - ptMax.y) * w : 0;

  if (aLeft > aRight)
  {
    if (aLeft > aTop)
    {
      if (aLeft > aBottom) // left
        _map->SetVisibleRect(x, y, ptMin.x - x, h);
      else // bottom
        _map->SetVisibleRect(x, ptMax.y, w, y + h - ptMax.y);
    }
    else if (aTop > aBottom) // top
      _map->SetVisibleRect(x, y, w, ptMin.y - y);
    else // bottom
      _map->SetVisibleRect(x, ptMax.y, w, y + h - ptMax.y);
  }
  else if (aRight > aTop)
  {
    if (aRight > aBottom) // right
      _map->SetVisibleRect(ptMax.x, y, x + w - ptMax.x, h);
    else // bottom
      _map->SetVisibleRect(x, ptMax.y, w, y + h - ptMax.y);
  }
  else if (aTop > aBottom) // top
    _map->SetVisibleRect(x, y, w, ptMin.y - y);
  else if (aBottom > 0) // bottomf
    _map->SetVisibleRect(x, ptMax.y, w, y + h - ptMax.y);
  else
    // TODO: move notepad
    _map->SetVisibleRect(x, y, w, h);
}

void DisplayMap::OnChildDestroyed(int idd, int exit)
{
#ifndef _XBOX
  if (idd == IDD_INSERT_MARKER)
  {
    if (exit == IDC_OK)
    {
      DisplayInsertMarker *display = dynamic_cast<DisplayInsertMarker *>((ControlsContainer *)_child);
      Assert(display);

      int index = markersMap.Add();
      ArcadeMarkerInfo &marker = markersMap[index];
      marker.position = _map->ScreenToWorld(DrawCoord(display->_x, display->_y));
      char buffer[256]; sprintf(buffer, "_USER_DEFINED #%d/%d", GetNetworkManager().GetPlayer(), index);
      marker.name = buffer;
      marker.text = display->_text;
      marker.markerType = MTIcon;
      ParamEntryVal markers = Pars >> "CfgMarkers";
      if (display->_picture >= 0 && display->_picture < markers.GetEntryCount())
      {
        ParamEntryVal markerCfg = markers.GetEntry(display->_picture);
        marker.type = markerCfg.GetName();
        marker.OnTypeChanged();
      }

      ParamEntryVal colors = Pars >> "CfgMarkerColors";
      if (display->_color >= 0 && display->_color < colors.GetEntryCount())
      {
        ParamEntryVal colorCfg = colors.GetEntry(display->_color);
        marker.colorName = colorCfg.GetName();
        marker.OnColorChanged();
      }

      marker.fillName = "Solid";
      marker.OnFillChanged();
      marker.a = 1;
      marker.b = 1;
#if _VBS3 // marker EH
      void ProcessCreateMarkerEH(RString name, bool updated = true);
      ProcessCreateMarkerEH(buffer,false);
#endif
      ChatChannel channel = ActualChatChannel();
      Person *veh = GWorld->GetRealPlayer();
      AIBrain *unit = veh ? veh->Brain() : NULL;
      if (channel == CCGlobal || unit)
        SendMarker(channel, unit, marker);
    }
  }
#endif
  Display::OnChildDestroyed(idd, exit);
}

#if _ENABLE_IDENTITIES

static void InitDiaryTopicList(ControlsContainer *display, AutoArray<RString> &topics, Person *person, DiaryContext context = DCBriefing )
{
  if (!person)
  {
    topics.Clear();

    CListBox *list = dynamic_cast<CListBox *> (display->GetCtrl(IDC_DIARY_TOPIC_LIST));
    list->Clear();
    return;
  }

  // number of topics (restrict to available buttons)
  Identity &identity = person->GetIdentity();
  int n = identity.NDiarySubjects();

  CListBox *list = dynamic_cast<CListBox *> (display->GetCtrl(IDC_DIARY_TOPIC_LIST));
  if(!list) return;

  int currentStr = -1;
  if(list->GetCurSel()>=0) currentStr = list->GetValue(list->GetCurSel());

  topics.Realloc(n);
  topics.Resize(n);
  list->Clear();

  if(context != DCInGame)
  {
    int index = list->AddString(LocalizeString(IDS_USRACT_SHOW_MAP));
    list->SetValue(index,IDC_DIARY_TOPIC_MAP);
  }
  for (int i=0; i<n; i++)
  {
    DiaryPage *page = identity.GetDiarySubject(i);
    topics[i] = page->GetName();

    bool enable = !page->IsReadOnly();
    if (!enable)
    {
      // always enable writable pages (user can add record) 
      if (page->IsChanged()) page->Update(person);
      enable = page->NRecords() > 0;
    }

    if(enable || page->ShowEmpty())
    {
      RString name = page->GetName();
      if(context != DCInGame)
      {
        int index = list->AddString(page->GetDisplayName());
        list->SetValue(index,IDC_DIARY_TOPIC_1 + i);
        list->SetData(index,page->GetName());
      }
      else
      {
        if(stricmp(name,"tasks")==0 || (GWorld->GetMode()  == GModeNetware && stricmp(name,"players")==0))
        {
          int index = list->AddString(page->GetDisplayName());
          list->SetValue(index,IDC_DIARY_TOPIC_1 + i);
          list->SetData(index,page->GetName());
        }
      }
    }
  }
  if(currentStr >=0 && currentStr != list->GetValue(list->GetCurSel()))
  {
    for (int j=0; j< list->Size(); j++)
    {      
      if(currentStr == list->GetValue(j))
      {
        list->SetCurSel(j);
        break;
      }
    }
  }
}

static void UpdateDiaryTopicList(ControlsContainer *display, AutoArray<RString> &topics, Person *person, DiaryContext context = DCBriefing)
{
  InitDiaryTopicList(display, topics, person, context);
}

#endif // _ENABLE_IDENTITIES

DisplayMainMap::DisplayMainMap(ControlsContainer *parent)
: DisplayMap(parent, "RscDisplayMainMap")
{
  _enableDisplay = false;

  ShowWarrant(false);
  ShowWalkieTalkie(false);
  ShowGPS(true);
  for (int i=sensorsMap.Size()-1; i>=0; i--)
  {
    Vehicle *veh = sensorsMap[i];
    if (!veh) continue;
    Detector *det = dyn_cast<Detector>(veh);
    Assert(det);
    if (det->IsActive() && !det->IsRepeating()) continue;
    if
      (
      det->GetActivationBy() >= ASAAlpha &&
      det->GetActivationBy() <= ASAJuliet
      )
    {
      ShowWalkieTalkie(true);
      break;
    }
  }
  //  SwitchDebriefing(false);
#if _ENABLE_IDENTITIES
  _displayDiaryMap = DisplayDiaryInMap(this);
  _displayDiaryMap.SyncTopicList(0);  
  _displayDiaryMap.SyncDiary();
  CListBox *list = dynamic_cast<CListBox *> (this->GetCtrl(IDC_DIARY_TOPIC_LIST));
  if(list && list->Size()>0) 
  {//select map by default
    list->SetCurSel(0);
  }
  _displayDiaryMap.ResetListBox();
#endif
}

void DisplayMainMap::ResetHUD()
{
  DisplayMap::ResetHUD();
#if _ENABLE_IDENTITIES
  // switch diary to the current player
  _displayDiaryMap.UpdateOwner();
#endif
}

void DisplayMainMap::OnButtonClicked(int idc)
{
  // do not ignore Cancel command
  switch (idc)
  {
  case IDC_CANCEL:
    if (IsInGame()) GWorld->ShowMap(false);
    break;
  default:
    DisplayMap::OnButtonClicked(idc);
    break;
  }
#if _ENABLE_IDENTITIES
  _displayDiaryMap.OnButtonClicked(idc);
#endif
}

void DisplayMainMap::OnLBSelChanged(IControl *ctrl, int curSel)
{
#if _ENABLE_IDENTITIES
  _displayDiaryMap.OnLBSelChanged(ctrl, curSel);
#endif
  DisplayMap::OnLBSelChanged(ctrl, curSel);
}

void DisplayMainMap::OnLBDblClick(int idc, int curSel)
{
#if _ENABLE_IDENTITIES
  _displayDiaryMap.OnLBDblClick(idc,  curSel);
#endif
  DisplayMap::OnLBDblClick(idc,  curSel);
}

bool DisplayMainMap::OnKeyDown(int dikCode)
{
  if (GWorld->UI() && GWorld->UI()->IsCommandingMode()) return false;
  return DisplayMap::OnKeyDown(dikCode);
}

bool DisplayMainMap::OnKeyUp(int dikCode)
{
  if (GWorld->UI() && GWorld->UI()->IsCommandingMode()) return false;
  return DisplayMap::OnKeyUp(dikCode);
}

void DisplayMainMap::OnSimulate(EntityAI *vehicle)
{
#if _ENABLE_IDENTITIES
  // init buttons based on diary content
  _displayDiaryMap.OnSimulate(vehicle);
  _displayDiaryMap.UpdateTopics();
  _displayDiaryMap.UpdateDiary();
  _displayDiaryMap.ResetListBox();
#endif

#ifdef _XBOX
  if (GWorld->UI() && GWorld->UI()->IsCommandingMode())
  {
    // process only "click" to map
    if (_map)
    {
      if (GInput.xInputButtonsToDo[XBOX_RightTrigger])
      {
        _map->OnKeyDown(INPUT_DEVICE_XINPUT + XBOX_RightTrigger);
        GInput.xInputButtonsToDo[XBOX_RightTrigger] = false;
      }
    }
    return;
  }
  // store dpad buttons for in-game menu
  bool left = GInput.xInputButtonsToDo[XBOX_Left]; GInput.xInputButtonsToDo[XBOX_Left] = false;
  bool right = GInput.xInputButtonsToDo[XBOX_Right]; GInput.xInputButtonsToDo[XBOX_Right] = false;
  bool leftP = GInput.xInputButtonsPressed[XBOX_Left]; GInput.xInputButtonsPressed[XBOX_Left] = false;
  bool rightP = GInput.xInputButtonsPressed[XBOX_Right]; GInput.xInputButtonsPressed[XBOX_Right] = false;

  // store left thumb buttons for movement
  bool leftL = GInput.xInputButtonsToDo[XBOX_LeftThumbXLeft]; GInput.xInputButtonsToDo[XBOX_LeftThumbXLeft] = false;
  bool rightL = GInput.xInputButtonsToDo[XBOX_LeftThumbXRight]; GInput.xInputButtonsToDo[XBOX_LeftThumbXRight] = false;
  bool upL = GInput.xInputButtonsToDo[XBOX_LeftThumbYUp]; GInput.xInputButtonsToDo[XBOX_LeftThumbYUp] = false;
  bool downL = GInput.xInputButtonsToDo[XBOX_LeftThumbYDown]; GInput.xInputButtonsToDo[XBOX_LeftThumbYDown] = false;
  bool leftLP = GInput.xInputButtonsPressed[XBOX_LeftThumbXLeft]; GInput.xInputButtonsPressed[XBOX_LeftThumbXLeft] = false;
  bool rightLP = GInput.xInputButtonsPressed[XBOX_LeftThumbXRight]; GInput.xInputButtonsPressed[XBOX_LeftThumbXRight] = false;
  bool upLP = GInput.xInputButtonsPressed[XBOX_LeftThumbYUp]; GInput.xInputButtonsPressed[XBOX_LeftThumbYUp] = false;
  bool downLP = GInput.xInputButtonsPressed[XBOX_LeftThumbYDown]; GInput.xInputButtonsPressed[XBOX_LeftThumbYDown] = false;

  // store buttons button
  bool start = GInput.xInputButtonsToDo[XBOX_Start]; GInput.xInputButtonsToDo[XBOX_Start] = false;
  bool b = GInput.xInputButtonsToDo[XBOX_B]; GInput.xInputButtonsToDo[XBOX_B] = false;

  DisplayMap::OnSimulate(vehicle);

  // restore dpad buttons
  GInput.xInputButtonsToDo[XBOX_Left] = left;
  GInput.xInputButtonsToDo[XBOX_Right] = right;
  GInput.xInputButtonsPressed[XBOX_Left] = leftP;
  GInput.xInputButtonsPressed[XBOX_Right] = rightP;

  // restore left thumb buttons
  GInput.xInputButtonsToDo[XBOX_LeftThumbXLeft] = leftL;
  GInput.xInputButtonsToDo[XBOX_LeftThumbXRight] = rightL;
  GInput.xInputButtonsToDo[XBOX_LeftThumbYUp] = upL;
  GInput.xInputButtonsToDo[XBOX_LeftThumbYDown] = downL;
  GInput.xInputButtonsPressed[XBOX_LeftThumbXLeft] = leftLP;
  GInput.xInputButtonsPressed[XBOX_LeftThumbXRight] = rightLP;
  GInput.xInputButtonsPressed[XBOX_LeftThumbYUp] = upLP;
  GInput.xInputButtonsPressed[XBOX_LeftThumbYDown] = downLP;

  // restore buttons
  GInput.xInputButtonsToDo[XBOX_Start] = start;
  GInput.xInputButtonsToDo[XBOX_B] = b;

#else

  DisplayMap::OnSimulate(vehicle);

#endif
}

void DisplayMainMap::OnHTMLLink(int idc, RString link)
{
#if _ENABLE_IDENTITIES
  _displayDiaryMap.OnHTMLLink(idc,link, DCInGame);
#endif
}

void DisplayMainMap::OnChildDestroyed(int idd, int exit)
{
#if _ENABLE_IDENTITIES
  _displayDiaryMap.OnChildDestroyed(idd,exit, _child);
#endif
  DisplayMap::OnChildDestroyed(idd, exit);
}

void DisplayMainMap::ProcessDiaryLink(RString link,DiaryContext contex)
{
#if _ENABLE_IDENTITIES
  _displayDiaryMap.ProcessLink(link, contex);
#endif
}


#ifndef _XBOX

DisplayInsertMarker::DisplayInsertMarker(ControlsContainer *parent, float x, float y, float cx, float cy, float cw, float ch, bool 

                                         enableSimulation)
                                         : Display(parent)
{
  GInput.ChangeGameFocus(+1); //call it as early as possible in order not to skip it by some possible return
  _enableSimulation = _enableSimulation && enableSimulation;

  Load("RscDisplayInsertMarker");

  // make sure chat in a screen which disabled display does not enable it
  _enableDisplay = _enableDisplay && (!parent || parent->DisplayEnabled());
  _x = x; _y = y;
  _cx = cx; _cy = cy; _cw = cw; _ch = ch;

  _picture = 0;
  _color = 0;
  UpdatePicture();

  Control *picture = dynamic_cast<Control *>(GetCtrl(IDC_INSERT_MARKER_PICTURE));
  Control *edit = dynamic_cast<Control *>(GetCtrl(IDC_INSERT_MARKER));
  if (picture && edit)
  {
    float wP = picture->W();
    float wE = edit->W();
    float w = wP + wE;
    float hP = picture->H();
    float hE = edit->H();
    float h = floatMax(hP, hE);
    y -= 0.5 * h;
    x -= 0.5 * wP;
    saturate(x, _cx, _cx + _cw - w);
    saturate(y, _cy, _cy + _ch - h);
    picture->SetPos(x, y + 0.5 * (h - hP), wP, hP);
    edit->SetPos(x + wP, y + 0.5 * (h - hE), wE, hE);
  }
}

DisplayInsertMarker::~DisplayInsertMarker()
{
  GInput.ChangeGameFocus(-1);
}

bool DisplayInsertMarker::OnKeyDown(int dikCode)
{
  switch (dikCode)
  {
  case DIK_DOWN:
    if (GInput.keys[DIK_LSHIFT] || GInput.GetKey(DIK_RSHIFT))
      NextColor();
    else
      NextPicture();
    UpdatePicture();
    return true;
  case DIK_UP:
    if (GInput.keys[DIK_LSHIFT] || GInput.GetKey(DIK_RSHIFT))
      PrevColor();
    else
      PrevPicture();
    UpdatePicture();
    return true;
  }
  return Display::OnKeyDown(dikCode);
}

bool DisplayInsertMarker::OnKeyUp(int dikCode)
{
  switch (dikCode)
  {
  case DIK_DOWN:
  case DIK_UP:
    return true;
  }
  return Display::OnKeyUp(dikCode);
}

void DisplayInsertMarker::OnButtonClicked(int idc)
{
  switch (idc)
  {
  case IDC_CANCEL:
  case IDC_OK:
    Exit(idc);
    break;
  default:
    Display::OnButtonClicked(idc);
    break;
  }
}

void DisplayInsertMarker::Destroy()
{
  Display::Destroy();

  if (_exit != IDC_OK) return;

  CEdit *edit = dynamic_cast<CEdit *>(GetCtrl(IDC_INSERT_MARKER));
  if (edit)
    _text = edit->GetText();
}

void DisplayInsertMarker::UpdatePicture()
{
  CStatic *ctrl = dynamic_cast<CStatic *>(GetCtrl(IDC_INSERT_MARKER_PICTURE));
  if (ctrl)
  {
    ParamEntryVal markers = Pars >> "CfgMarkers";
    if (_picture < 0 || _picture >= markers.GetEntryCount()) return;
    ParamEntryVal colors = Pars >> "CfgMarkerColors";
    if (_color < 0 || _color >= colors.GetEntryCount()) return;

    ParamEntryVal markerCfg = markers.GetEntry(_picture);
    if( (int)(markerCfg >> "scope") <2) 
    {
      NextPicture();
      markerCfg = markers.GetEntry(_picture);
    }
    RString picture = markerCfg >> "icon";
    ParamEntryVal colorCfg = colors.GetEntry(_color);
    PackedColor color;
    if (stricmp(colorCfg.GetName(), "default") == 0)
      color = GetPackedColor(markerCfg >> "color");
    else
      color = GetPackedColor(colorCfg >> "color");
    ctrl->SetText(picture);
    ctrl->SetTextColor(color);
  }
}

void DisplayInsertMarker::PrevPicture()
{
  ParamEntryVal markers = Pars >> "CfgMarkers";
  int n = markers.GetEntryCount();
  int loop = _picture;
  int scope = 0;

  do 
  { 
    _picture--;
    if (_picture < 0) _picture = n - 1;
    ParamEntryVal entry = markers.GetEntry(_picture);
    scope = entry >> "scope";
  } while (scope<2 && loop != _picture);
}

void DisplayInsertMarker::NextPicture()
{
  ParamEntryVal markers = Pars >> "CfgMarkers";
  int n = markers.GetEntryCount();
  int loop = _picture;
  int scope = 0;

  do 
  { 
    _picture++;
    if (_picture >= n) _picture = 0;
    ParamEntryVal entry = markers.GetEntry(_picture);
    scope = entry >> "scope";
  } while (scope<2 && loop != _picture);
}

void DisplayInsertMarker::PrevColor()
{
  ParamEntryVal colors = Pars >> "CfgMarkerColors";
  int n = colors.GetEntryCount();
  int loop = _color;
  int scope = 0;

  do 
  { 
    _color--;
    if (_color < 0) _color = n - 1;
    ParamEntryVal entry = colors.GetEntry(_color);
    scope = entry >> "scope";
  } while (scope==1 && loop != _color);
}

void DisplayInsertMarker::NextColor()
{
  ParamEntryVal colors = Pars >> "CfgMarkerColors";
  int n = colors.GetEntryCount();
  int loop = _color;
  int scope = 0;

  do 
  {
    _color++;
    if (_color >= n) _color = 0;
    ParamEntryVal entry = colors.GetEntry(_color);
    scope = entry >> "scope";
  } while (scope==1 && loop != _color);
}

#endif

#if _ENABLE_IDENTITIES

DisplayDiary::DisplayDiary(Display *parent, RString link, DiaryContext context)
: Display(parent)
{
  GInput.ChangeGameFocus(+1); //call it as early as possible in order not to skip it by some possible return
  _enableUI = true;

  _alwaysShow = true;
  _context = DCInGame;
  _parent = parent;

  Load("RscDisplayDiary");
  _enableSimulation = true;

  // load list of index textures
  ParamEntryVal list = Pars >> "CfgDiary" >> "Icons";
  for (int i=0; i<list.GetEntryCount(); i++)
  {
    ParamEntryVal entry = list.GetEntry(i);
    Ref<Texture> texture = GlobLoadTexture(GetPictureName(entry));
    if (texture) _indexTextures.Add(TextureByName(entry.GetName(), texture));
  }

  // for backward compatibility, handle content of briefing.html (notes, plan, debriefing)
  RString briefing = GetBriefingFile();
  if (briefing.GetLength() > 0) _briefing.Load(briefing);

  AIBrain *unit = GWorld->FocusOn();
  _diaryPerson = unit ? unit->GetPerson() : NULL;
  
  if(unit && unit->GetRemoteControlled())
    _diaryPerson = unit->GetRemoteControlled();

  if(_diaryPerson) InitDiaryTopicList(this, _topics, _diaryPerson, DCInGame);


  //diary topics list
  _diaryTopics = dynamic_cast<CListBox *> (GetCtrl(IDC_DIARY_TOPIC_LIST));
  if(_diaryTopics) 
  {
    _diaryTopics->EnableQuickSearch(false);
    _maxDiaryTopicsH = _diaryTopics->H();
  }
  //diary sub-topics list
  _diaryIndex = dynamic_cast<CListBox *> (GetCtrl(IDC_DIARY_TOPIC_LISTINDEX));
  if(_diaryIndex) 
  {
    _diaryIndex->EnableQuickSearch(false);
    _maxDiaryIndexH = _diaryIndex->H();
  }
  //diary HTML text
  _diary = dynamic_cast<CHTML *> (GetCtrl(IDC_DIARY_TOPIC_HTML));
  if(_diary) _maxDiaryH = _diary->H();

  IControl* ctrl = GetCtrl(IDC_DIARY_TOPIC_ADD_RECORD);
  if(ctrl) 
  {
    _diaryAdd = GetTextContainer(ctrl);
    ctrl->ShowCtrl(false);
  }
  //HMTML container
  _controlsGroup = dynamic_cast<CControlsGroup *> (GetCtrl(IDC_DIARY_TOPIC_HTML_GROUP));
  if(_controlsGroup)
    _maxControlsGroupH = _controlsGroup->H();
  //mission description
  _playerName = dynamic_cast<ITextContainer *> (GetCtrl(IDC_DIARY_PLAYER_NAME));
  _currentTask = dynamic_cast<ITextContainer *>(GetCtrl(IDC_DIARY_CURRENT_TASK));
  _currentMission = dynamic_cast<ITextContainer *> (GetCtrl(IDC_DIARY_MISSION_NAME));

  GWorld->UI()->ResetLastWpTime();
  GWorld->UI()->ResetLastCmdTime();
  if(_currentMission)
  {//get mission name
    RString name;
    if (GWorld->GetMode() == GModeNetware)
    {
      const MissionHeader *header = GetNetworkManager().GetMissionHeader();
      if (header) name = header->GetLocalizedMissionName();
    }
    if (name.GetLength() == 0)
    {
      name = Localize(CurrentTemplate.intel.briefingName);
      if (name.GetLength() == 0)
      {
        name = Glob.header.filenameReal;
        // user mission file name is encoded
        if (IsUserMission()) name = DecodeFileName(name);
      }
    }
    _currentMission->SetText(name);
  }

  if (_diaryPerson)
  {
    if (link.GetLength() > 0) ProcessLink(link, DCInGame);
  }

  for (int i= 0; i< _diaryTopics->Size();i++)
  {
    if (stricmp(_diaryTopics->GetData(i),link)==0)
    {
      _diaryTopics->SetCurSel(i);
      return;
    }
  }

  SyncDiary();
  UpdateDiary();
  ResetListBox();

  if (_diaryIndex) FocusCtrl(_diaryIndex->GetControl());
}

DisplayDiary::~DisplayDiary()
{
  GInput.ChangeGameFocus(-1);
}

Control *DisplayDiary::OnCreateCtrl(int type, int idc, ParamEntryPar cls)
{
  switch (idc)
  {
#if _ENABLE_IDENTITIES
case IDC_DIARY_TOPIC_LIST:
case IDC_DIARY_TOPIC_LISTINDEX:
  {
    CMapListBox *mapList = new CMapListBox(this,idc,cls);
    return mapList;
  }
case IDC_DIARY_TOPIC_HTML_GROUP:
  {
    CMapCGroup *cgroup  = new CMapCGroup(this,idc,cls);
    return cgroup;
  }
#endif
  }
  return Display::OnCreateCtrl(type, idc, cls);
}

void DisplayDiary::ResetListBox()
{ //resize index list and it's background
  //bool showMenu = GWorld->UI()->IsMenuVisible();

  if(_diaryIndex)
  { 
    Control *ctrl = dynamic_cast<Control *> (GetCtrl(IDC_DIARY_SUBTOPIC_BACKGROUND));
    if(_diaryIndex->Size()<=0) 
    {
      _diaryIndex->ShowCtrl(false);
      if(ctrl) ctrl->ShowCtrl(false);
    }
    else 
    {//hide if empty
      if(!_diaryIndex->IsVisible()) _diaryIndex->ShowCtrl(true);
      if(ctrl) ctrl->ShowCtrl(true);
      float size = floatMin(0.002f +_diaryIndex->Size() * _diaryIndex->GetRowHeight(),_maxDiaryIndexH);
      _diaryIndex->SetPos(_diaryIndex->X(),_diaryIndex->Y(),_diaryIndex->W(),size);
      if(ctrl) ctrl->SetPos(ctrl->X(),ctrl->Y(),ctrl->W(),size+0.04);
    }  
  }
  //reset HMTL control and it's background
  if (_diary)
  {
    Control *ctrl = dynamic_cast<Control *> (GetCtrl(IDC_DIARY_CONTENT_BACKGROUND));
    float size = 0;

    if(_diary->NSections()>0)
    {
      HTMLSection &section = _diary->GetSection(_diary->CurrentSection());
      //get HTML control height
      for (int r=0; r<section.rows.Size(); r++)
      {
        HTMLRow &row = section.rows[r];
        size += row.height;
      }
    }
    //if HTML control is visible and !empty
    if(_diary->NSections()>0 && size>0)
    {
      size = floatMin(size,_maxDiaryH);
      _diary->ShowCtrl(true);       
      _diary->SetPos(_diary->X(),_diary->Y(),_diary->W(),size);
      if(_controlsGroup)
      {
        _controlsGroup->ShowCtrl(true);
        size = floatMin(size,_maxControlsGroupH);
        _controlsGroup->SetPos(_controlsGroup->X(),_controlsGroup->Y(),_controlsGroup->W(),size+0.005f);
        if(ctrl)
        {
          ctrl->ShowCtrl(true);
          ctrl->SetPos(ctrl->X(),ctrl->Y(),ctrl->W(),size+0.005f+0.04);
        }
      }
    }
    else  
    {//hide (if not hidden)
      if(_controlsGroup->IsVisible())
      {
        if(_controlsGroup)_controlsGroup->ShowCtrl(false);
        _diary->ShowCtrl(false);
        if(ctrl) ctrl->ShowCtrl(false);

        IControl *ctrl = GetCtrl(IDC_DIARY_TOPIC_LIST);
        if(ctrl) FocusCtrl(ctrl);
      }
    }
  }
  //resize topic listbox and it's background
  if(_diaryTopics)
  {
    Control *ctrl = dynamic_cast<Control *> (GetCtrl(IDC_DIARY_TOPIC_BACKGROUND));
    float size = floatMin(0.002f + _diaryTopics->Size() * _diaryTopics->GetRowHeight(),_maxDiaryTopicsH);
    _diaryTopics->SetPos(_diaryTopics->X(),_diaryTopics->Y(),_diaryTopics->W(),size);
    if(!_diaryTopics->IsVisible())  _diaryTopics->ShowCtrl(true);
    if(ctrl)
    {
      ctrl->SetPos(ctrl->X(), ctrl->Y(), ctrl->W(), size+0.04);
      ctrl->ShowCtrl(true);
    }
  }
  else
  {
    Control *ctrl = dynamic_cast<Control *> (GetCtrl(IDC_DIARY_TOPIC_BACKGROUND));
    if(_diaryTopics)
      _diaryTopics->ShowCtrl(false);
    if(ctrl) ctrl->ShowCtrl(false);
  }
}


void DisplayDiary::OnButtonClicked(int idc)
{
  switch (idc)
  {
  case IDC_DIARY_TOPIC_ADD_RECORD:
    {//add diary record
      RString title;
      if (_diaryIndex)
      {
        int sel = _diaryIndex->GetCurSel();
        if (sel >= 0) title = _diaryIndex->GetText(sel);
      }
      CreateChild(new DisplayEditDiaryRecord(this, -1, title, RString()));
      return;
    }
  }
  Display::OnButtonClicked(idc);
}

bool DisplayDiary::OnKeyDown(int dikCode)
{
  switch (dikCode)
  {
  case INPUT_DEVICE_XINPUT + XBOX_A:
    // navigation on Xbox
    if (GetFocused() && _diaryIndex && GetFocused() == _diaryIndex->GetControl())
    {
      return true;
    }
    break;
  case INPUT_DEVICE_XINPUT + XBOX_B:
    // navigation on Xbox
    if (GetFocused() && _diary && GetFocused() == _diary->GetControl())
    {
      return true;
    }
    break;;
  }
  return Display::OnKeyDown(dikCode);
}

bool DisplayDiary::OnKeyUp(int dikCode)
{
  switch (dikCode)
  {
  case INPUT_DEVICE_XINPUT + XBOX_A:
    // navigation on Xbox
    if (GetFocused() && _diaryIndex && GetFocused() == _diaryIndex->GetControl())
    {
      if (_diary) FocusCtrl(_diary->GetControl());
      return true;
    }
    break;;
  case INPUT_DEVICE_XINPUT + XBOX_B:
    // navigation on Xbox
    if (GetFocused() && _diary && GetFocused() == _diary->GetControl())
    {
      if (_diaryIndex) FocusCtrl(_diaryIndex->GetControl());
      return true;
    }
    break;;
  }
  return Display::OnKeyUp(dikCode);
}

void DisplayDiary::OnLBSelChanged(IControl *ctrl, int curSel)
{
  if(ctrl->IDC() == IDC_DIARY_TOPIC_LIST && _diaryTopics)
  {
    int diary_idc = _diaryTopics->GetValue(curSel);
    if(diary_idc>=IDC_DIARY_TOPIC_1)
    {
      RString link = _topics[diary_idc - IDC_DIARY_TOPIC_1];
      ProcessLink(link);
    }
    else if(diary_idc==IDC_DIARY_TOPIC_MAP) ClearLink();
    ResetListBox();
  }
  else if (ctrl->IDC() == IDC_DIARY_TOPIC_LISTINDEX)
  {
    SyncDiary();
    ResetListBox();
    return;
  }
  Display::OnLBSelChanged(ctrl, curSel);
}


void DisplayDiary::ClearLink()
{//hide everything to see only map 
  SelectPage("");
  ProcessLink("");
}

void DisplayDiary::OnHTMLLink(int idc, RString link)
{
  const char *ptr = link;
  const char *name = "marker:";
  int n = strlen(name);
  if (strnicmp(ptr, name, n) == 0)
  {
    ptr += n;
    for (int i=0; i<markersMap.Size(); i++)
    {
      ArcadeMarkerInfo &mInfo = markersMap[i];
      if (stricmp(ptr, mInfo.name) == 0)
      {
        if (_parent)
        {
          //_map->AnimateTo(mInfo.position);
          //_map->SetActiveMarker(i);
          //   Exit(IDC_AUTOCANCEL);
        }
        break;
      }
    }
    return;
  }
  name = "edit:";
  n = strlen(name);
  if (strnicmp(ptr, name, n) == 0)
  {
    ptr += n;
    int recordId = atoi(ptr);
    if (_diaryPerson)
    {
      RString subject = GetCurrentPage();
      if (subject.GetLength() > 0)
      {
        Identity &identity = _diaryPerson->GetIdentity();
        const DiaryPage *page = identity.FindDiarySubject(subject);
        if (page && !page->IsReadOnly())
        {
          DiaryRecord *record = page->FindRecord(recordId);
          if (record && !record->IsReadOnly()) CreateChild(new DisplayEditDiaryRecord(this, recordId, record->GetTitle(),

            record->GetText()));
        }
      }
    }
    return;
  }
  name = "delete:";
  n = strlen(name);
  if (strnicmp(ptr, name, n) == 0)
  {
    ptr += n;
    int recordId = atoi(ptr);
    if (_diaryPerson)
    {
      RString subject = GetCurrentPage();
      Identity &identity = _diaryPerson->GetIdentity();
      if (subject.GetLength() > 0 && !identity.IsDiaryReadOnly(subject))
      {
        _toDeleteSubject = subject;
        _toDeleteRecord = recordId;
        MsgBoxButton buttonOK(LocalizeString(IDS_DISP_XBOX_HINT_YES), INPUT_DEVICE_XINPUT + XBOX_A);
        MsgBoxButton buttonCancel(LocalizeString(IDS_DISP_XBOX_HINT_NO), INPUT_DEVICE_XINPUT + XBOX_B);
        CreateMsgBox(MB_BUTTON_OK | MB_BUTTON_CANCEL, LocalizeString(IDS_SURE),
          IDD_MSG_DELETE_DIARY_RECORD, false, &buttonOK, &buttonCancel);
        identity.DeleteDiaryRecord(subject, recordId);
      }
    }
    return;
  }
  name = "currentTask:";
  n = strlen(name);
  if (strnicmp(ptr, name, n) == 0)
  {
    ptr += n;
    int taskId = atoi(ptr);
    if (_diaryPerson)
    {
      Identity &identity = _diaryPerson->GetIdentity();
      Task *task = identity.FindTask(taskId);
      Task *current = identity.GetCurrentTask();
      if (task)
      {
        if(current && current->GetState() == TSAssigned ) current->SetState(TSCreated);
        identity.SetCurrentTask(task);
        task->SetState(TSAssigned);
        GWorld->UI()->ResetLastWpTime();
      }
    }
    return;
  }
  name = "gear:";
  n = strlen(name);
  if (strnicmp(ptr, name, n) == 0)
  {
    ptr += n;
    ObjectId id; id.Decode(atoi(ptr));
    Object *obj = GWorld->FindObject(id);
    Person *person = dyn_cast<Person>(obj);
    if (person && person->Brain())
    {
      if (_context == DCInGame)
        CreateChild(CreateGearSimpleDialog(this, person->Brain()));
    }
    return;
  }
  name = "teamswitch:";
  n = strlen(name);
  if (strnicmp(ptr, name, n) == 0)
  {
    if (_context == DCInGame)
    {
      ptr += n;
      ObjectId id; id.Decode(atoi(ptr));
      Object *obj = GWorld->FindObject(id);
      Person *person = dyn_cast<Person>(obj);
      if (person && _diaryPerson)
      {
        void ProcessTeamSwitch(Person *newPlayer, Person *oldPlayer, EntityAI *killer, bool respawn);
        ProcessTeamSwitch(person, _diaryPerson, NULL, false);
        GWorld->ShowMap(false);
        //  Exit(IDC_AUTOCANCEL);
      }
    }
    return;
  }
  name = "kick:";
  n = strlen(name);
  if (strnicmp(ptr, name, n) == 0)
  {
    if (_context == DCInGame)
    {
      ptr += n;
      int dpnid = atoi(ptr);
      if (GetNetworkManager().IsServer())
      {
        if (dpnid != GetNetworkManager().GetPlayer())
          GetNetworkManager().KickOff(dpnid, KORKick);
      }
      else if (GetNetworkManager().IsGameMaster())
      {
        if (dpnid != GetNetworkManager().GetPlayer())
          GetNetworkManager().SendKick(dpnid);
      }
    }
    return;
  }
  name = "ban:";
  n = strlen(name);
  if (strnicmp(ptr, name, n) == 0)
  {
    if (_context == DCInGame)
    {
      ptr += n;
      int dpnid = atoi(ptr);
      if (GetNetworkManager().IsServer())
      {
        if (dpnid != GetNetworkManager().GetPlayer())
          GetNetworkManager().Ban(dpnid);
      }
    }
    return;
  }
  name = "mute:";
  n = strlen(name);
  if (strnicmp(ptr, name, n) == 0)
  {
    if (_context == DCInGame)
    {
      ptr += n;
      int dpnid = atoi(ptr);
      if (dpnid != GetNetworkManager().GetPlayer())
      {
        const PlayerIdentity *identity = GetNetworkManager().FindIdentity(dpnid);
        if (identity) GetNetworkManager().TogglePlayerMute(identity->xuid);
      }
    }
    return;
  }
  name = "execute:";
  n = strlen(name);
  if (strnicmp(ptr, name, n) == 0)
  {
    ptr += n;

    GameState *state = GWorld->GetGameState();

    GameVarSpace vars(false);
    state->BeginContext(&vars);
    state->Execute(ptr, GameState::EvalContext::_default, GWorld->GetMissionNamespace()); // mission namespace
    state->EndContext();

    return;
  }
  name = "executeClose:";
  n = strlen(name);
  if (strnicmp(ptr, name, n) == 0)
  {
    ptr += n;

    GameState *state = GWorld->GetGameState();

    GameVarSpace vars(false);
    state->BeginContext(&vars);
    state->Execute(ptr, GameState::EvalContext::_default, GWorld->GetMissionNamespace()); // mission namespace
    state->EndContext();

    GWorld->ShowMap(false);
    // Exit(IDC_AUTOCANCEL);
    return;
  }

  // handle as an external link
  ProcessLink(link);
}

void DisplayDiary::OnChildDestroyed(int idd, int exit)
{
  if (idd == IDD_EDIT_DIARY_RECORD)
  {
    if (exit == IDC_OK)
    {
      if (_diaryPerson)
      {
        ResizeHTML();
        RString page = GetCurrentPage();
        if (page.GetLength() > 0)
        {
          Identity &identity = _diaryPerson->GetIdentity();
          if (!identity.IsDiaryReadOnly(page))
          {
            DisplayEditDiaryRecord *disp = dynamic_cast<DisplayEditDiaryRecord *>((ControlsContainer *)_child);
            Assert(disp);
            int recordId = disp->GetRecordId();
            RString title = disp->GetTitle();
            RString text = disp->GetText();
            if (recordId >= 0) identity.EditDiaryRecord(page, recordId, title, text);
            else identity.AddDiaryRecord(page, title, text, NULL, TSNone, NULL, NULL, false); // not read-only
          }
        }
      }
    }
    UpdateDiary();
  }
  else if (idd == IDD_MSG_DELETE_DIARY_RECORD)
  {
    if (exit == IDC_OK && _diaryPerson)
    {
      Identity &identity = _diaryPerson->GetIdentity();
      identity.DeleteDiaryRecord(_toDeleteSubject, _toDeleteRecord);
    }
    UpdateDiary();
  }
  ResetListBox();
  Display::OnChildDestroyed(idd, exit);
}

void DisplayDiary::OnDraw(EntityAI *vehicle, float alpha)
{
  UpdateDiaryTopicList(this, _topics, _diaryPerson, _context);
  if (_diaryPerson)
  {
    if (_diary) UpdateDiary();
  }
  UpdateButtons();
  Display::OnDraw(vehicle, alpha);
}

void DisplayDiary::OnSimulate(EntityAI *vehicle)
{
  //unit is dead, close diary
  if(!GWorld->FocusOn() || !GWorld->FocusOn()->GetUnit() || !GWorld->FocusOn()->GetUnit()->LSIsAlive())
  {
    Exit(IDC_CANCEL);
    return;
  }

  InitDiaryTopicList(this, _topics, _diaryPerson,DCInGame);
  UpdateDiary();
  ResetListBox();

  ////close diary when 'j' is pressed ('j' is default key for open diary)
  if( GInput.GetActionToDo(UADiary, true, false, false, 2) )
  {
    CListBox *list = dynamic_cast<CListBox *> (GetCtrl(IDC_DIARY_TOPIC_LIST));

    if (stricmp(list->GetData(list->GetCurSel()),"tasks")==0)
      Exit(IDC_AUTOCANCEL);
    else
    {
      for (int i= 0; i< list->Size();i++)
      {
        if (stricmp(list->GetData(i),"tasks")==0)
        {
          list->SetCurSel(i);
          return;
        }
      }
      Exit(IDC_AUTOCANCEL);
    }
  }
  if(GWorld->GetMode()  == GModeNetware)
  {
    //close diary when 'j' is pressed ('j' is default key for open diary)
    if( GInput.GetActionToDo(UANetworkPlayers, true, false, false, 2) )
    {
      CListBox *list = dynamic_cast<CListBox *> (GetCtrl(IDC_DIARY_TOPIC_LIST));

      if (stricmp(list->GetData(list->GetCurSel()),"players")==0)
        Exit(IDC_AUTOCANCEL);
      else
      {
        for (int i= 0; i< list->Size();i++)
        {
          if (stricmp(list->GetData(i),"players")==0)
          {
            list->SetCurSel(i);
            return;
          }
        }
        Exit(IDC_AUTOCANCEL);
      }
    }
  }

  if (GInput.GetActionDo(UAHideMap, false))
    Exit(IDC_AUTOCANCEL);

  //if (GInput.GetActionDo(UAGear, false))
  //  Exit(1003);

  Display::OnSimulate(vehicle);
}

static int FindItemWithData(const CListBoxContainer *list, RString data)
{
  for (int i=0; i<list->GetSize(); i++)
  {
    if (strcmp(list->GetData(i), data) == 0) return i;
  }
  return -1;
}

bool DisplayDiary::SelectPage(RString name)
{
  if (_diaryPerson)
  {
    _diaryPerson->GetIdentity().SetCurrentDiarySubject(name);
    UpdateDiary(true);
    return true;
  }
  return false;
}

void DisplayDiary::ProcessLink(RString link,DiaryContext context)
{
  _context = context;
  AIBrain *unit = GWorld->FocusOn();
  _diaryPerson = unit ? unit->GetPerson() : NULL;

  if(unit && unit->GetRemoteControlled())
    _diaryPerson = unit->GetRemoteControlled();

  // load list of index textures
  ParamEntryVal list = Pars >> "CfgDiary" >> "Icons";
  for (int i=0; i<list.GetEntryCount(); i++)
  {
    ParamEntryVal entry = list.GetEntry(i);
    Ref<Texture> texture = GlobLoadTexture(GetPictureName(entry));
    if (texture) _indexTextures.Add(TextureByName(entry.GetName(), texture));
  }

  InitDiaryTopicList(this, _topics, _diaryPerson, context);
  ProcessLink(link);
}

void DisplayDiary::ProcessLink(RString link)
{
  // Unlike OnHTMLLink, global (external) links to diary are processed here
  const char *ptr = link;

  ResizeHTML();

  // link to the task
  const char *name = "task:";
  int n = strlen(name);
  if (strnicmp(ptr, name, n) == 0)
  {
    ptr += n;
    int taskId = atoi(ptr);
    if (_diaryPerson)
    {
      Identity &identity = _diaryPerson->GetIdentity();
      Task *task = identity.FindTask(taskId);
      if (task)
      {
        // which page contains the list of tasks
        RString pageName;
        for (int i=0; i<identity.NDiarySubjects(); i++)
        {
          const DiaryPage *page = identity.GetDiarySubject(i);
          if (strcmp(page->GetPageType(), "DiaryPageTasks") == 0)
          {
            pageName = page->GetName();
            break;
          }
        }

        if (pageName.GetLength() > 0)
        {
          // switch to the correct page
          SelectPage(pageName);

          // switch to the correct record
          if (_diaryIndex)
          {
            // find the record in the index, select it and synchronize the content
            RString name = Format("Task%d", taskId);
            int i = FindItemWithData(_diaryIndex, name);
            if (i >= 0)
            {
              _diaryIndex->SetCurSel(i);
              SyncDiary();
            }
          }
        }
      }
    }
    return;
  }

  // link to the diary page / diary record
  if (_diaryPerson)
  {
    Identity &identity = _diaryPerson->GetIdentity();
    for (int i=0; i<identity.NDiarySubjects(); i++)
    {
      const DiaryPage *page = identity.GetDiarySubject(i);
      RString name = page->GetName();
      if (stricmp(ptr, name) == 0)
      {
        // switch to the correct page
        SelectPage(name);
      }
      else
      {
        RString prefix = name + RString(":");
        int n = prefix.GetLength();
        if (strnicmp(ptr, prefix, n) == 0)
        {
          ptr += n;
          RString recordId = ptr;

          // TODO: switch sort order by the id type

          // switch to the correct page
          SelectPage(name);

          // switch to the correct record
          if (_diaryIndex)
          {
            // find the record in the index, select it and synchronize the content
            int i = FindItemWithData(_diaryIndex, recordId);
            if (i >= 0)
            {
              _diaryIndex->SetCurSel(i);
              SyncDiary();
            }
          }
        }
      }
    }
  }
}

struct DiaryParserStackItem
{
  RString _name;
  XMLAttributes _attributes;
  //text properties for HTMLContainer
  HTMLFormat format;  //html format (headline, paragraph,..)
  PackedColor color;  //specific color, used only if colored == true
  bool colored;       //used, because i don't know default color yet 
  RString fontName;   //font type
  float fontSize;     //font size (number of pixels / 408)
};
TypeIsMovable(DiaryParserStackItem)

/// XML parser used to convert XML-based diary text to the HTML control
class DiaryParser : public SAXParser
{
protected:
  CHTMLContainer *_html;
  int _section;

  /// this enable to keep context of the text segment
  AutoArray<DiaryParserStackItem> _stack;

public:
  DiaryParser(CHTMLContainer *html, int section);
  void OnStartElement(RString name, XMLAttributes &attributes);
  void OnEndElement(RString name);
  void OnCharacters(RString chars);

protected:
  RString GetReference(const DiaryParserStackItem &item) const;
};

DiaryParser::DiaryParser(CHTMLContainer *html, int section)
: _html(html), _section(section)
{//by default is trim=true and then when font props. are changed, spaces beetween words are erased
  _trim = false;
}

void DiaryParser::OnStartElement(RString name, XMLAttributes &attributes)
{
  // add the context to the stack
  DiaryParserStackItem &item = _stack.Append();
  item._name = name;
  item._attributes = attributes;

  // attribute "image" in any tag will force the image instead text
  const XMLAttribute *attr = item._attributes.Find("image");
  if (attr)
  {
    RString image = attr->value;
    float wImage = -1, hImage = -1; 
    attr = item._attributes.Find("width");
    if (attr) wImage = atoi(attr->value);
    attr = item._attributes.Find("height");
    if (attr) hImage = atoi(attr->value);

    _html->AddImage(_section, image, HALeft, false, wImage, hImage, GetReference(item));
  }


  if (_stack.Size() > 1)
  {//if there are at least two items(last one is current), take font properties from previous 
    const DiaryParserStackItem &previousItem = _stack[_stack.Size() - 2]; //_stack.Size() - 1 is current
    item.colored = previousItem.colored;
    item.fontName = previousItem.fontName;
    item.fontSize = previousItem.fontSize;
    item.color = previousItem.color;
    item.format = previousItem.format;
  }
  else
  {//if this is the first item in stack, set default font properties 
    item.colored = false; 
    item.fontName = "";  
    item.fontSize = -1; 
    item.format = HFP;
  }

  //set format type
  if (stricmp(name, "p") == 0)
  {//paragraph
    item.format = HFP;
  }
  if (stricmp(name, "h1") == 0)
  {//headline
    item.format = HFH1;
  }
  else if (stricmp(name, "h2") == 0)
  {
    item.format = HFH2;
  }
  else if (stricmp(name, "h3") == 0)
  {
    item.format = HFH3;
  }
  else if (stricmp(name, "h4") == 0)
  {
    item.format = HFH4;
  }
  else if (stricmp(name, "h5") == 0)
  {
    item.format = HFH5;
  }
  else if (stricmp(name, "h6") == 0)
  {
    item.format = HFH6;
  }
  else if (stricmp(name, "font") == 0)
  {//read specific font properties
    attr = item._attributes.Find("color");
    if (attr) 
    {
      item.color =  HTMLContent::ParseColorHexa(attr->value); //convert #FFFFFF to PackedColor
      item.colored = true;
    }
    attr = item._attributes.Find("size");       
    if (attr) item.fontSize = atoi(attr->value)/408.0f;  //408.0f - see commonDefs.hpp
    attr = item._attributes.Find("face");   
    if (attr) item.fontName = attr->value;               //font name (font is loaded later)
  }
}

void DiaryParser::OnEndElement(RString name)
{
  // check the syntax
  int n = _stack.Size();
  if (n == 0)
  {
    RptF("DiaryParser: <%s/> without corresponding <%s>", cc_cast(name), cc_cast(name));
    return;
  }
  DiaryParserStackItem &item = _stack[n - 1];
  if (strcmp(name, item._name) != 0)
  {
    RptF("DiaryParser: <%s/> does not match to <%s>", cc_cast(name), cc_cast(item._name));
    return;
  }

  // handle some special tags
  if (strcmp(name, "br") == 0)
  {
    _html->AddBreak(_section, false);
  }

  // remove the context from the stack
  _stack.Resize(n - 1);
}

void DiaryParser::OnCharacters(RString chars)
{
  RString href;
  int n = _stack.Size();
  if (n > 0)
  {
    const DiaryParserStackItem &item = _stack[n - 1];

    // when image attribute present, tag was already processed (during OnStartElement)
    const XMLAttribute *attr = item._attributes.Find("image");
    if (attr) return;

    href = GetReference(item);
    //if possible, use font properties from DiaryParserStackItem
    _html->AddText(_section, chars, HFP, HALeft, false, false, href,0,0,item.colored,item.color,item.fontSize,item.fontName);
  }
  else _html->AddText(_section, chars, HFP, HALeft, false, false, href);
}

RString DiaryParser::GetReference(const DiaryParserStackItem &item) const
{
  if (strcmp(item._name, "marker") == 0) // marker in the map
  {
    const XMLAttribute *attr = item._attributes.Find("name");
    if (attr) return RString("marker:") + attr->value;
  }
  else if (strcmp(item._name, "currentTask") == 0) // set as the current task
  {
    const XMLAttribute *attr = item._attributes.Find("id");
    if (attr) return RString("currentTask:") + attr->value;
  }
  else if (strcmp(item._name, "gear") == 0) // access the gear screen
  {
    const XMLAttribute *attr = item._attributes.Find("unit");
    if (attr) return RString("gear:") + attr->value;
  }
  else if (strcmp(item._name, "teamSwitch") == 0) // switch to the unit
  {
    const XMLAttribute *attr = item._attributes.Find("unit");
    if (attr) return RString("teamSwitch:") + attr->value;
  }
  else if (strcmp(item._name, "kick") == 0) // kick off the player
  {
    const XMLAttribute *attr = item._attributes.Find("id");
    if (attr) return RString("kick:") + attr->value;
  }
  else if (strcmp(item._name, "ban") == 0) // ban the player
  {
    const XMLAttribute *attr = item._attributes.Find("id");
    if (attr) return RString("ban:") + attr->value;
  }
  else if (strcmp(item._name, "mute") == 0) // mute the player
  {
    const XMLAttribute *attr = item._attributes.Find("id");
    if (attr) return RString("mute:") + attr->value;
  }
  else if (strcmp(item._name, "log") == 0) // common reference to the log
  {
    const XMLAttribute *attr1 = item._attributes.Find("subject");
    const XMLAttribute *attr2 = item._attributes.Find("record");
    if (attr1 && attr2) return attr1->value + RString(":") + attr2->value;
  }
  else if (strcmp(item._name, "execute") == 0) // execute the expression
  {
    const XMLAttribute *attr = item._attributes.Find("expression");
    if (attr) return RString("execute:") + attr->value;
  }
  else if (strcmp(item._name, "executeClose") == 0) // execute the expression
  {
    const XMLAttribute *attr = item._attributes.Find("expression");
    if (attr) return RString("executeClose:") + attr->value;
  }
  return RString();
}

class WriteToDiary
{
protected:
  mutable CListBoxContainer *_index;
  mutable CHTMLContainer *_text;
  mutable int _section;
  mutable const DiaryRecord *_last;
  const DiaryPage *_page;
  const TexturesByName &_indexTextures;
  const Identity &_identity;
  RString _selected;

public:
  WriteToDiary(CListBoxContainer *index, CHTMLContainer *text, const DiaryPage *page, const TexturesByName &indexTextures, const 

    Identity &identity, RString selected)
    : _indexTextures(indexTextures), _identity(identity)
  {
    _index = index;
    _text = text;
    _section = -1;
    _last = NULL;
    _page = page;
    _selected = selected;
  }
  bool operator ()(int index, const DiaryRecord *record) const;
  void Finish();
};

bool WriteToDiary::operator ()(int index, const DiaryRecord *record) const
{
  // record internal name - used for navigation
  RString name = Format("Record%d", record->GetId());
  // for some dynamic pages, records are better identified by different data than record ID
  DiarySortOrder sortOrder = _page->GetSortOrder();
  switch (sortOrder)
  {
  case DSOTask:
    {
      const Task *task = record->GetTask();
      if (task) name = Format("Task%d", task->GetId());
    }
    break;
  case DSOUnit:
    {
      const AIBrain *unit = record->GetUnit();
      if (unit && unit->GetPerson())
      {
        int id = unit->GetPerson()->GetObjectId().Encode();
        name = Format("Unit%d", id);
      }
    }
    break;
  }

  // title of the current record in index and text
  RString indexTitle = _page->GetIndexTitle(record);
  RString recordTitle = _page->GetRecordTitle(record);
  bool indexChanged = true;
  if (_last)
  {
    switch (sortOrder)
    {
    case DSONone:
    case DSODate:
      indexChanged = strcmp(indexTitle, _page->GetIndexTitle(_last)) != 0;
      break;
    case DSOTask:
      indexChanged = record->GetTask() != _last->GetTask();
      break;
    case DSOUnit:
      indexChanged = record->GetUnit() != _last->GetUnit();
      break;
    }
  }

  if (indexChanged)
  {
    // index title changed, new HTML section and an index item needed
    if (_index)
    {
      // create new item in index
      int index = _index->AddString(indexTitle);
      _index->SetData(index, name);
      // set icon to the item
      RString indexIcon = _page->GetIndexIcon(record);
      if (indexIcon.GetLength() > 0)
      {
        const TextureByName &texture = _indexTextures[indexIcon];
        _index->SetTexture(index, texture._texture);
      }
    }

    // finish current section
    if (_section >= 0) _text->FormatSection(_section);
    // create a new one
    _section = _text->AddSection();

    // write the record title always on the new page
    _last = NULL;
  }

  // enable navigation in HTML
  _text->AddName(_section, name);

  // record title
  if (recordTitle.GetLength() > 0 && (!_last || strcmp(recordTitle, _page->GetRecordTitle(_last)) != 0))
  {
    _text->AddText(_section, recordTitle, HFH1, HALeft, false, false, RString());
    _text->AddBreak(_section, false);
  }

  // record text
  _text->SetIndent(0.03);
  RString text = _page->GetRecordText(record);
  // interpret the text as a XML description
  DiaryParser parser(_text, _section);
  QIStrStream in(cc_cast(text), text.GetLength());
  parser.Parse(in);
  // actions
  if (!_page->IsReadOnly() && !record->IsReadOnly())
  {
    _text->AddText(_section, " [", HFP, HALeft, false, false, RString());
    _text->AddText(_section, LocalizeString(IDS_LOG_EDIT), HFP, HALeft, false, false, Format("edit:%d", record->GetId()));
    _text->AddText(_section, "]", HFP, HALeft, false, false, RString());

    _text->AddText(_section, "[", HFP, HALeft, false, false, RString());
    _text->AddText(_section, LocalizeString(IDS_LOG_DELETE), HFP, HALeft, false, false, Format("delete:%d", record->GetId()));
    _text->AddText(_section, "]", HFP, HALeft, false, false, RString());
  }
  // finish
  _text->SetIndent(0);
  _text->AddBreak(_section, false);
  if (sortOrder != DSONone) _text->AddBreak(_section, false);

  _last = record;
  return false; // continue with the next record
}

void WriteToDiary::Finish()
{
  if (_section >= 0) _text->FormatSection(_section);

  // select last item in index
  if (_index)
  {
    int sel = -1;
    if (_selected.GetLength()) sel = FindItemWithData(_index, _selected);
    if (sel < 0)
    {
      sel = 0; // by default, select the first item
      if (_page->GetSortOrder() == DSOTask)
      {
        // when sorted by the class, try to select the current task
        Task *currentTask = _identity.GetCurrentTask();
        if (currentTask) sel = FindItemWithData(_index, Format("Task%d", currentTask->GetId()));
      }
    }
    if(_index->GetCurSel() != sel) _index->SetCurSel(sel);
  }
}

void DisplayDiary::HandleReferences(const HTMLSection &section)
{
  Assert(_diary);
  for (int i=0; i<section.fields.Size(); i++)
  {
    const char *href = section.fields[i].href;
    if (*href != '#') continue;
    // link to other section
    RString name = href + 1;
    // check if already in diary
    if (_diary->FindSection(name) >= 0) continue;
    // find in the briefing
    int ss = _briefing.FindSection(name);
    if (ss < 0) continue;

    // need to be copied
    int s = _diary->AddSection();
    HTMLSection &dstSection = _diary->GetSection(s);
    const HTMLSection &srcSection = _briefing.GetSection(ss);
    // create the index item
    if (_diaryIndex)
    {
      int index = _diaryIndex->AddString(section.fields[i].text);
      _diaryIndex->SetData(index, name);
    }
    // copy the content
    dstSection = srcSection;
    // finish
    _diary->FormatSection(s);
    // handle references to other sections
    HandleReferences(srcSection);
  }
}

void DisplayDiary::WriteBriefingNotes()
{
  AIBrain *brain = _diaryPerson ? _diaryPerson->Brain() : NULL;
  AIUnit *unit = brain ? brain->GetUnit() : NULL;

  int section = FindSectionForUnit(_briefing, "Main", unit);
  if (section >= 0)
  {
    // add the section to the diary
    if (_diary)
    {
      const HTMLSection &srcSection = _briefing.GetSection(section);
      int s = _diary->AddSection();
      HTMLSection &dstSection = _diary->GetSection(s);
      RString name = srcSection.names.Size() > 0 ? srcSection.names[0] : "Notes";
      // create the index item
      if (_diaryIndex)
      {
        int index = _diaryIndex->AddString(LocalizeString(IDS_MAP_NOTES));
        _diaryIndex->SetData(index, name);
      }
      // title
      _diary->AddText(s, LocalizeString(IDS_MAP_NOTES), HFH1, HALeft, false, false, RString());
      _diary->AddBreak(s, false);
      // text
      for (int i=0; i<srcSection.fields.Size(); i++) dstSection.fields.Add(srcSection.fields[i]);
      dstSection.fields.Compact();
      // names
      dstSection.names = srcSection.names;
      if (dstSection.names.Size() == 0) dstSection.names.Add(name);
      // finish
      _diary->FormatSection(s);
      // handle references to other sections
      HandleReferences(srcSection);
    }
  }
}

void DisplayDiary::WriteBriefingPlan()
{
  AIBrain *brain = _diaryPerson ? _diaryPerson->Brain() : NULL;
  AIUnit *unit = brain ? brain->GetUnit() : NULL;
  TargetSide side = brain ? brain->GetSide() : TSideUnknown;

  // plan header
  int section = FindSectionForUnit(_briefing, "Plan", side, unit);
  if (section >= 0)
  {
    // add the section to the diary
    if (_diary)
    {
      const HTMLSection &srcSection = _briefing.GetSection(section);
      int s = _diary->AddSection();
      HTMLSection &dstSection = _diary->GetSection(s);
      RString name = srcSection.names.Size() > 0 ? srcSection.names[0] : "Plan";
      // create the index item
      if (_diaryIndex)
      {
        int index = _diaryIndex->AddString(LocalizeString(IDS_MAP_PLAN));
        _diaryIndex->SetData(index, name);
      }
      // title
      _diary->AddText(s, LocalizeString(IDS_MAP_PLAN), HFH1, HALeft, false, false, RString());
      _diary->AddBreak(s, false);
      // text
      for (int i=0; i<srcSection.fields.Size(); i++) dstSection.fields.Add(srcSection.fields[i]);
      dstSection.fields.Compact();
      // names
      dstSection.names = srcSection.names;
      if (dstSection.names.Size() == 0) dstSection.names.Add(name);
      // finish
      _diary->FormatSection(s);
      // handle references to other sections
      HandleReferences(srcSection);
    }
  }

  // objectives
  int objective = 1;
  for (int section=0; section<_briefing.NSections(); section++)
  {
    const HTMLSection &src = _briefing.GetSection(section);
    for (int n=0; n<src.names.Size(); n++)
    {
      RString srcName = src.names[n];
      if (strnicmp(srcName, "OBJ_", strlen("OBJ_")) != 0) continue; // optimization

      struct NamePrefix
      {
        const char *prefix;
        const char *prefixReplace;
        TargetSide side;
        bool hidden;
      };

      NamePrefix prefixes[] =
      {
        {"OBJ_WEST_HIDDEN_", "OBJ_WEST_", TWest, true},
        {"OBJ_EAST_HIDDEN_", "OBJ_EAST_", TEast, true},
        {"OBJ_GUER_HIDDEN_", "OBJ_GUER_", TGuerrila, true},
        {"OBJ_CIVIL_HIDDEN_", "OBJ_CIVIL_", TCivilian, true},
        {"OBJ_HIDDEN_", "OBJ_", TSideUnknown, true},
        {"OBJ_WEST_", NULL, TWest, false},
        {"OBJ_EAST_", NULL, TEast, false},
        {"OBJ_GUER_", NULL, TGuerrila, false},
        {"OBJ_CIVIL_", NULL, TCivilian, false},
        {"OBJ_", NULL, TSideUnknown, false}
      };

      // find the longest matching prefix
      int match = -1;
      for (int i=0; i<lenof(prefixes); i++)
      {
        if (strnicmp(srcName, prefixes[i].prefix, strlen(prefixes[i].prefix)) == 0)
        {
          match = i;
          break;
        }
      }

      // valid objective found
      Assert(match >= 0); // because of optimization
      NamePrefix &prefix = prefixes[match];
      if (prefix.side != TSideUnknown && side != prefix.side) continue;
      if (prefix.prefixReplace) srcName = RString(prefix.prefixReplace) + srcName.Substring(strlen(prefix.prefix), INT_MAX); // fix the name

      int value;
      if (unit)
      {
        GameState *state = GWorld->GetGameState();
        value = toInt((float)state->Evaluate(srcName, GameState::EvalContext::_default, GWorld->GetMissionNamespace())); // mission namespace
      }
      else
      {
        value = prefix.hidden ? OSHidden : OSActive; 
      }

      if (value == OSHidden) break;  // next objective

      // add objective to the diary
      ParamEntryVal cls = Pars >> "RscObjectives";
      RString picture;
      switch (value)
      {
      case OSDone:
        picture = cls >> "done";
        break;
      case OSFailed:
        picture = cls >> "failed";
        break;
      default:
        Fail("Unknown objective type");
      case OSActive:
        picture = cls >> "active";
        break;
      }

      RString name = src.names.Size() > 0 ? src.names[0] : Format("Objective_%d", objective);
      RString displayName = Format(LocalizeString(IDS_LOG_TITLE_OBJECTIVE), objective);
      objective++;

      // index
      if (_diaryIndex)
      {
        int index = _diaryIndex->AddString(displayName);
        _diaryIndex->SetData(index, name);
      }
      // content
      if (_diary)
      {
        int s = _diary->AddSection();
        HTMLSection &dstSection = _diary->GetSection(s);
        // title
        _diary->AddText(s, displayName, HFH1, HALeft, false, false, RString());
        _diary->AddBreak(s, false);
        // picture
        float imgHeight = 480.0f * 1.0f * _diary->GetPHeight();
        HTMLField *fld = _diary->AddImage(s, picture, HALeft, false, -1, imgHeight, "");
        fld->exclude = true;
        float indent = 1.2 * fld->width;
        // text
        for (int i=0; i<src.fields.Size(); i++)
        {
          int index = dstSection.fields.Add(src.fields[i]);
          dstSection.fields[index].indent += indent;
        }
        dstSection.fields.Compact();
        // name
        dstSection.names = src.names;
        if (dstSection.names.Size() == 0) dstSection.names.Add(name);
        // finish
        _diary->FormatSection(s);
        // handle references to other sections
        HandleReferences(src);
      }
      break; // next objective
    }
  }
}

void DisplayDiary::ResizeHTML()
{
  //if diary content changes, this ensure that HMTL text will not be broken into smaller pages
  if (_diary) _diary->SetPos(_diary->X(),_diary->Y(),_diary->W(),_maxDiaryH);
  if (_controlsGroup) _controlsGroup->SetPos(_controlsGroup->X(),_controlsGroup->Y(),_controlsGroup->W(),_maxDiaryH);
}

void DisplayDiary::SyncTopicList(int index)
{
  //if external link has been processed, synchronize diary topic 
  if(_diaryTopics)
  {
    if(_diaryTopics->GetValue(_diaryTopics->GetCurSel()) != index + IDC_DIARY_TOPIC_1)

      for (int i = 0; i< _diaryTopics->Size();i++)
      {
        if (_diaryTopics->GetValue(i) == index + IDC_DIARY_TOPIC_1)
        {
          _diaryTopics->SetCurSel(i);
        }
      }
  }
}

void DisplayDiary::UpdateDiary(bool force)
{
  AIBrain *unit = GWorld->FocusOn();
  if(!unit) return;
  AIUnit* aiUnit = unit->GetUnit();
  if(!_diaryPerson)
    _diaryPerson = unit ? unit->GetPerson() : NULL;

  if(unit && unit->GetRemoteControlled())
    _diaryPerson = unit->GetRemoteControlled();

  Assert(_diary);
  Assert(_diaryPerson);

  ResizeHTML();

  Identity &identity = _diaryPerson->GetIdentity();
  RString pageName = GetCurrentPage();
  AIGroup *group = unit->GetGroup();

  if(_diaryPerson)
  {
    RString groupName;
    RString taskDesc;
    int rank = RankUndefined;
    rank = _diaryPerson->GetRank();

    RString name =  _diaryPerson->GetPersonName();
    if(group) groupName =group->GetName();
    if(_playerName && aiUnit)
      _playerName->SetText(LocalizeString(IDS_SHORT_PRIVATE + rank) + " " +  name + ", " + groupName + " " + FormatNumber(aiUnit->ID()));
    Task *task = identity.GetCurrentTask();
    if(task) task->GetDescriptionShort(taskDesc);
    if(_currentTask) _currentTask->SetText(taskDesc);
  }
  else
  {
    if(_currentTask) _currentTask->SetText("");
    if(_playerName) _playerName->SetText("");
  }

  if (force || pageName.GetLength() > 0 && identity.IsDiaryChanged(pageName))
  {
    // store selected item in the index
    RString selected;
    if (_diaryIndex)
    {
      int index = _diaryIndex->GetCurSel();
      if (index >= 0) selected = _diaryIndex->GetData(index);
      _diaryIndex->ClearStrings();
    }

    // store selected section in the HTML
    RString selectedSection;
    int currentSection = _diary->CurrentSection();
    if (currentSection >= 0 && currentSection < _diary->NSections())
    {
      const HTMLSection &section = _diary->GetSection(currentSection);
      for (int i=0; i<section.names.Size(); i++)
      {
        if (stricmp(section.names[i], selected) != 0)
        {
          selectedSection = section.names[i]; // first which name is different from the core page name
          break;
        }
      }
    }

    int activeField = _diary->GetActiveFieldIndex();

    _diary->Init();

    // create new content of diary
    DiaryPage *page = identity.FindDiarySubject(pageName);
    if (page)
    {
      // for backward compatibility, handle content of briefing.html (notes, plan)
      RString pageType = page->GetPageType();
      if (stricmp(pageType, "DiaryPageDiary") == 0) WriteBriefingNotes();
      //else if (stricmp(pageType, "DiaryPageTasks") == 0) WriteBriefingPlan();

      // now process regular diary records
      WriteToDiary func(_diaryIndex, _diary, page, _indexTextures, identity, selected);
      page->ForEachRecord(func, _diaryPerson);
      func.Finish();
      SyncDiary();
      // try to recover HTML state before the update
      if (selectedSection.GetLength() > 0) _diary->SwitchSection(selectedSection);
      if (activeField >= 0)
      {
        int currentSection = _diary->CurrentSection();
        if (currentSection >= 0 && currentSection < _diary->NSections())
        {
          const HTMLSection &section = _diary->GetSection(currentSection);
          if (activeField < section.fields.Size() && section.fields[activeField].href.GetLength() > 0)
            _diary->SetActiveFieldIndex(activeField);
        }
      }
    }
  }
  UpdateButtons();
}

void DisplayDiary::SyncDiary()
{
  if (!_diary || !_diaryIndex) return;

  int index = _diaryIndex->GetCurSel();
  if (index >= 0)
    _diary->SwitchSection(_diaryIndex->GetData(index));
  else
  {
    // select the last section in the topic
    int n = intMax(_diary->NSections() - 1, 0);
    _diary->SwitchSectionRaw(n);
  }
}

void DisplayDiary::UpdateButtons()
{
  if (_diaryAdd)
  {
    IControl *ctrl = _diaryAdd->GetControl();
    if (_diaryPerson == NULL)
    {
      ctrl->ShowCtrl(false);
    }
    else
    {
      RString page = GetCurrentPage();
      if (page.GetLength() == 0)
      {
        ctrl->ShowCtrl(false);
      }
      else
      {
        Identity &identity = _diaryPerson->GetIdentity();
        ctrl->ShowCtrl(!identity.IsDiaryReadOnly(page));
      }
    }
  }
}

RString DisplayDiary::GetCurrentPage() const
{
  if (!_diaryPerson) return RString();
  return _diaryPerson->GetIdentity().GetCurrentDiarySubject();
}

/******************************************************************************/
/******************************************************************************/
CMapListBox::CMapListBox(DisplayMap *map, int idc, ParamEntryPar cls) 
: CListBox(map, idc, cls)
{
  _map = map;
}

void CMapListBox::OnMouseZChanged(float dz)
{
  if(GInput.keyPressed[DIK_LCONTROL] || GInput.keyPressed[DIK_RCONTROL])
  {
    if(_map && _map->GetMap())
      _map->GetMap()->OnMouseZChanged(dz);
  }
  else
  {
    if(dz<0 && GetCurSel()<Size()-1) SetCurSel(GetCurSel()+1);
    else if(dz>0 && GetCurSel()>0) SetCurSel(GetCurSel()-1);
  }
}

CMapCGroup::CMapCGroup(DisplayMap *map, int idc, ParamEntryPar cls) 
: CControlsGroup(map, idc, cls)
{
  _map = map;
}

// for listbox and combobox
#define SCROLL_SPEED    0.1
#define SCROLL_MAX      0.1

void CMapCGroup::OnMouseZChanged(float dz)
{
  if(GInput.keyPressed[DIK_LCONTROL] || GInput.keyPressed[DIK_RCONTROL])
  {
    if(_map && _map->GetMap())
      _map->GetMap()->OnMouseZChanged(dz);
  }
  else
  {
    float scroll = SCROLL_SPEED * dz;
    saturate(scroll, -SCROLL_MAX, SCROLL_MAX);
    _vScrollbar.SetPos(_vScrollbar.GetPos()-scroll);
  }
}



DisplayDiaryInMap::DisplayDiaryInMap(DisplayMap *parent)
{
  _map = parent;
  _context = DCBriefing;
  // init buttons based on diary content
  AIBrain *unit = GWorld->FocusOn();
  _diaryPerson = unit ? unit->GetPerson() : NULL;

  if(unit && unit->GetRemoteControlled())
    _diaryPerson = unit->GetRemoteControlled();

  if(_diaryPerson) InitDiaryTopicList(_map, _topics, _diaryPerson);
  //diary topics list
  _diaryTopics = dynamic_cast<CListBox *> (_map->GetCtrl(IDC_DIARY_TOPIC_LIST));
  if(_diaryTopics) 
  {
    _diaryTopics->EnableQuickSearch(false);
    _maxDiaryTopicsH = _diaryTopics->H();
  }

  //diary sub-topics list
  _diaryIndex = dynamic_cast<CListBox *> (_map->GetCtrl(IDC_DIARY_TOPIC_LISTINDEX));
  if(_diaryIndex) 
  {
    _diaryIndex->EnableQuickSearch(false);
    _maxDiaryIndexH = _diaryIndex->H();
  }
  //diary HTML text
  _diary = dynamic_cast<CHTML *> (_map->GetCtrl(IDC_DIARY_TOPIC_HTML));
  if(_diary)
    _maxDiaryH = _diary->H();

  IControl* ctrl = _map->GetCtrl(IDC_DIARY_TOPIC_ADD_RECORD);
  if(ctrl) 
  {
    _diaryAdd = GetTextContainer(ctrl);
    ctrl->ShowCtrl(false);
  }
  //HMTML container
  _controlsGroup = dynamic_cast<CControlsGroup *> (_map->GetCtrl(IDC_DIARY_TOPIC_HTML_GROUP));
  if(_controlsGroup)
    _maxControlsGroupH = _controlsGroup->H();
  //mission description
  _playerName = dynamic_cast<ITextContainer *> (_map->GetCtrl(IDC_DIARY_PLAYER_NAME));
  _currentTask = dynamic_cast<ITextContainer *>(_map->GetCtrl(IDC_DIARY_CURRENT_TASK));
  _currentMission = dynamic_cast<ITextContainer *> (_map->GetCtrl(IDC_DIARY_MISSION_NAME));

  if(_currentMission)
  {//get mission name
    RString name;
    if (GWorld->GetMode() == GModeNetware)
    {
      const MissionHeader *header = GetNetworkManager().GetMissionHeader();
      if (header) name = header->GetLocalizedMissionName();
    }
    if (name.GetLength() == 0)
    {
      name = Localize(CurrentTemplate.intel.briefingName);
      if (name.GetLength() == 0)
      {
        name = Glob.header.filenameReal;
        // user mission file name is encoded
        if (IsUserMission()) name = DecodeFileName(name);
      }
    }
    _currentMission->SetText(name);
  }
}

void DisplayDiaryInMap::OnButtonClicked(int idc)
{
  switch (idc)
  {
  case IDC_DIARY_TOPIC_ADD_RECORD:
    {//add diary record
      RString title;
      if (_diaryIndex)
      {
        int sel = _diaryIndex->GetCurSel();
        if (sel >= 0) title = _diaryIndex->GetText(sel);
      }
      _map->CreateChild(new DisplayEditDiaryRecord(_map, -1, title, RString()));
    }
    break;
  }
}


void DisplayDiaryInMap::OnLBSelChanged(IControl *ctrl, int curSel)
{
  if(ctrl->IDC() == IDC_DIARY_TOPIC_LIST && _diaryTopics)
  {
    int diary_idc = _diaryTopics->GetValue(curSel);
    if(diary_idc>=IDC_DIARY_TOPIC_1)
    {
      RString link = _topics[diary_idc - IDC_DIARY_TOPIC_1];
      ProcessLink(link, DCBriefing);
    }
    else if(diary_idc==IDC_DIARY_TOPIC_MAP) ClearLink();
    ResetListBox();
  }
  else if (ctrl->IDC() == IDC_DIARY_TOPIC_LISTINDEX)
  {
    SyncDiary();
    ResetListBox();
    return;
  }
}

void DisplayDiaryInMap::OnLBDblClick(int idc, int curSel)
{
  if (idc == IDC_DIARY_TOPIC_LISTINDEX && _map && stricmp(_diaryTopics->GetData(_diaryTopics->GetCurSel()),"tasks")==0)
  {
    if (_diaryPerson)
    {
      Identity &identity = _diaryPerson->GetIdentity();
      const char *name = "Task";
      const char *ptr = _diaryIndex->GetData(curSel);
      int n = strlen(name);
      if (strnicmp(ptr, name, n) == 0)
      {
        ptr += n;
        int taksID = atoi(ptr);
        Task *tsk = identity.FindTask(taksID);
        if (tsk) 
        { 
          Vector3 dest;
          if(tsk->GetDestination(dest))
            _map->AnimateTo(dest);
        }
      }
    }
  }
}

void DisplayDiaryInMap::OnHTMLLink(int idc, RString link, DiaryContext context)
{
  _context = context;
  const char *ptr = link;
  const char *name = "marker:";
  int n = strlen(name);
  if (strnicmp(ptr, name, n) == 0)
  {
    ptr += n;
    for (int i=0; i<markersMap.Size(); i++)
    {
      ArcadeMarkerInfo &mInfo = markersMap[i];
      if (stricmp(ptr, mInfo.name) == 0)
      {
        if (_map)
        {
          _map->AnimateTo(mInfo.position);
          _map->SetActiveMarker(i);
          //   Exit(IDC_AUTOCANCEL);
        }
        break;
      }
    }
    return;
  }
  name = "edit:";
  n = strlen(name);
  if (strnicmp(ptr, name, n) == 0)
  {
    ptr += n;
    int recordId = atoi(ptr);
    if (_diaryPerson)
    {
      RString subject = GetCurrentPage();
      if (subject.GetLength() > 0)
      {
        Identity &identity = _diaryPerson->GetIdentity();
        const DiaryPage *page = identity.FindDiarySubject(subject);
        if (page && !page->IsReadOnly())
        {
          DiaryRecord *record = page->FindRecord(recordId);
          if (record && !record->IsReadOnly()) _map->CreateChild(new DisplayEditDiaryRecord(_map, recordId, record->GetTitle(), 

            record->GetText()));
        }
      }
    }
    return;
  }
  name = "delete:";
  n = strlen(name);
  if (strnicmp(ptr, name, n) == 0)
  {
    ptr += n;
    int recordId = atoi(ptr);
    if (_diaryPerson)
    {
      RString subject = GetCurrentPage();
      Identity &identity = _diaryPerson->GetIdentity();
      if (subject.GetLength() > 0 && !identity.IsDiaryReadOnly(subject))
      {
        _toDeleteSubject = subject;
        _toDeleteRecord = recordId;
        MsgBoxButton buttonOK(LocalizeString(IDS_DISP_XBOX_HINT_YES), INPUT_DEVICE_XINPUT + XBOX_A);
        MsgBoxButton buttonCancel(LocalizeString(IDS_DISP_XBOX_HINT_NO), INPUT_DEVICE_XINPUT + XBOX_B);
        _map->CreateMsgBox(MB_BUTTON_OK | MB_BUTTON_CANCEL, LocalizeString(IDS_SURE),
          IDD_MSG_DELETE_DIARY_RECORD, false, &buttonOK, &buttonCancel);
        identity.DeleteDiaryRecord(subject, recordId);
      }
    }
    return;
  }
  name = "currentTask:";
  n = strlen(name);
  if (strnicmp(ptr, name, n) == 0)
  {
    ptr += n;
    int taskId = atoi(ptr);
    if (_diaryPerson)
    {
      Identity &identity = _diaryPerson->GetIdentity();
      Task *task = identity.FindTask(taskId);
      Task *current = identity.GetCurrentTask();
      if (task) 
      {
        if(current && current->GetState() == TSAssigned ) current->SetState(TSCreated);
        identity.SetCurrentTask(task);
        task->SetState(TSAssigned);
        GWorld->UI()->ResetLastWpTime();
      }
    }
    return;
  }
  name = "gear:";
  n = strlen(name);
  if (strnicmp(ptr, name, n) == 0)
  {
    ptr += n;
    ObjectId id; id.Decode(atoi(ptr));
    Object *obj = GWorld->FindObject(id);
    Person *person = dyn_cast<Person>(obj);
    if (person && person->Brain())
    {
      if (_context == DCInGame)
        _map->CreateChild(CreateGearSimpleDialog(_map, person->Brain()));
      else if (_context == DCBriefing && _map)
      {
        _map->CreateChild(new DisplayGear(_map, false, _map->GetWeaponsInfo(), _map->GetWeaponsPool(), person->Brain(), false));
      }
    }
    return;
  }
  name = "teamswitch:";
  n = strlen(name);
  if (strnicmp(ptr, name, n) == 0)
  {
    if (_context == DCInGame)
    {
      ptr += n;
      ObjectId id; id.Decode(atoi(ptr));
      Object *obj = GWorld->FindObject(id);
      Person *person = dyn_cast<Person>(obj);
      if (person && _diaryPerson)
      {
        void ProcessTeamSwitch(Person *newPlayer, Person *oldPlayer, EntityAI *killer, bool respawn);
        ProcessTeamSwitch(person, _diaryPerson, NULL, false);
        GWorld->ShowMap(false);
        //  Exit(IDC_AUTOCANCEL);
      }
    }
    return;
  }
  name = "kick:";
  n = strlen(name);
  if (strnicmp(ptr, name, n) == 0)
  {
    if (_context == DCInGame)
    {
      ptr += n;
      int dpnid = atoi(ptr);
      if (GetNetworkManager().IsServer())
      {
        if (dpnid != GetNetworkManager().GetPlayer())
          GetNetworkManager().KickOff(dpnid, KORKick);
      }
      else if (GetNetworkManager().IsGameMaster())
      {
        if (dpnid != GetNetworkManager().GetPlayer())
          GetNetworkManager().SendKick(dpnid);
      }
    }
    return;
  }
  name = "ban:";
  n = strlen(name);
  if (strnicmp(ptr, name, n) == 0)
  {
    if (_context == DCInGame)
    {
      ptr += n;
      int dpnid = atoi(ptr);
      if (GetNetworkManager().IsServer())
      {
        if (dpnid != GetNetworkManager().GetPlayer())
          GetNetworkManager().Ban(dpnid);
      }
    }
    return;
  }
  name = "mute:";
  n = strlen(name);
  if (strnicmp(ptr, name, n) == 0)
  {
    if (_context == DCInGame)
    {
      ptr += n;
      int dpnid = atoi(ptr);
      if (dpnid != GetNetworkManager().GetPlayer())
      {
        const PlayerIdentity *identity = GetNetworkManager().FindIdentity(dpnid);
        if (identity) GetNetworkManager().TogglePlayerMute(identity->xuid);
      }
    }
    return;
  }
  name = "execute:";
  n = strlen(name);
  if (strnicmp(ptr, name, n) == 0)
  {
    ptr += n;

    GameState *state = GWorld->GetGameState();

    GameVarSpace vars(false);
    state->BeginContext(&vars);
    state->Execute(ptr, GameState::EvalContext::_default, GWorld->GetMissionNamespace()); // mission namespace
    state->EndContext();

    return;
  }
  name = "executeClose:";
  n = strlen(name);
  if (strnicmp(ptr, name, n) == 0)
  {
    ptr += n;

    GameState *state = GWorld->GetGameState();

    GameVarSpace vars(false);
    state->BeginContext(&vars);
    state->Execute(ptr, GameState::EvalContext::_default, GWorld->GetMissionNamespace()); // mission namespace
    state->EndContext();

    GWorld->ShowMap(false);
    // Exit(IDC_AUTOCANCEL);
    return;
  }

  // handle as an external link
  ProcessLink(link);
}

void DisplayDiaryInMap::OnChildDestroyed(int idd, int exit, Ref<ControlsContainer> child)
{
  if (idd == IDD_EDIT_DIARY_RECORD)
  {
    if (exit == IDC_OK)
    {
      if (_diaryPerson)
      {
        ResizeHTML();
        RString page = GetCurrentPage();
        if (page.GetLength() > 0)
        {
          Identity &identity = _diaryPerson->GetIdentity();
          if (!identity.IsDiaryReadOnly(page))
          {
            DisplayEditDiaryRecord *disp = dynamic_cast<DisplayEditDiaryRecord *>((ControlsContainer *)child);
            Assert(disp);
            int recordId = disp->GetRecordId();
            RString title = disp->GetTitle();
            RString text = disp->GetText();
            if (recordId >= 0) identity.EditDiaryRecord(page, recordId, title, text);
            else identity.AddDiaryRecord(page, title, text, NULL, TSNone, NULL, NULL, false); // not read-only
          }
        }
      }
    }
    UpdateDiary();

  }
  else if (idd == IDD_MSG_DELETE_DIARY_RECORD)
  {
    if (exit == IDC_OK && _diaryPerson)
    {
      Identity &identity = _diaryPerson->GetIdentity();
      identity.DeleteDiaryRecord(_toDeleteSubject, _toDeleteRecord);
    }
    UpdateDiary();
    ResetListBox();
  }
  else if (idd == 106)
  {
    UpdateTopics();
    UpdateDiary();
    ResetListBox();
  }
}

void DisplayDiaryInMap::OnSimulate(EntityAI *vehicle)
{
  if(_diaryTopics && _diaryTopics->GetCurSel()<0)
  {
    AIBrain *unit = GWorld->FocusOn();
    _diaryPerson = unit ? unit->GetPerson() : NULL;

    if(unit && unit->GetRemoteControlled())
      _diaryPerson = unit->GetRemoteControlled();

    if(_diaryPerson) InitDiaryTopicList(_map, _topics, _diaryPerson, DCBriefing);

    if(_diaryTopics->Size()>0) _diaryTopics->SetCurSel(0);
  }
  else if(_diaryPerson && _diaryTopics)
  {
    Identity &identity = _diaryPerson->GetIdentity();
    RString nas = identity.GetCurrentDiarySubject();
    if(_diaryTopics->GetCurSel() == 0 && nas.GetLength()>0) ClearLink(); 
    else if (stricmp(_diaryTopics->GetData(_diaryTopics->GetCurSel()), nas) != 0)
    {
      ProcessLink(_diaryTopics->GetData(_diaryTopics->GetCurSel()));
    }
  }
  ////close diary when 'j' is pressed ('j' is default key for open diary)
  if( GInput.GetActionToDo(UADiary, true, false, false, 2) )
  {
    CListBox *list = dynamic_cast<CListBox *> (_map->GetCtrl(IDC_DIARY_TOPIC_LIST));

    if (stricmp(list->GetData(list->GetCurSel()),"tasks")==0)
      list->SetCurSel(0);
    else
    {
      for (int i= 1; i< list->Size();i++)
      {
        if (stricmp(list->GetData(i),"tasks")==0)
        {
          list->SetCurSel(i);
          return;
        }
      }
    }
  }
  //close diary when 'j' is pressed ('j' is default key for open diary)
  if( GInput.GetActionToDo(UANetworkPlayers, true, false, false, 2) )
  {
    CListBox *list = dynamic_cast<CListBox *> (_map->GetCtrl(IDC_DIARY_TOPIC_LIST));

    if (stricmp(list->GetData(list->GetCurSel()),"players")==0)
      list->SetCurSel(0);
    else
    {
      for (int i= 0; i< list->Size();i++)
      {
        if (stricmp(list->GetData(i),"players")==0)
        {
          list->SetCurSel(i);
          return;
        }
      }
    }
  }
}

bool DisplayDiaryInMap::SelectPage(RString name)
{
  if (_diaryPerson)
  {
    _diaryPerson->GetIdentity().SetCurrentDiarySubject(name);
    UpdateDiary(true);
    return true;
  }
  return false;
}

void DisplayDiaryInMap::UpdateOwner()
{
  AIBrain *unit = GWorld->FocusOn();
  Person *person = unit ? unit->GetPerson() : NULL;
  if (person && person != _diaryPerson)
  {
    _diaryPerson = person;

    if(unit && unit->GetRemoteControlled())
      _diaryPerson = unit->GetRemoteControlled();
  }
}


void DisplayDiaryInMap::ProcessLink(RString link,DiaryContext context)
{
  _context = context;
  AIBrain *unit = GWorld->FocusOn();
  _diaryPerson = unit ? unit->GetPerson() : NULL;

  if(unit && unit->GetRemoteControlled())
    _diaryPerson = unit->GetRemoteControlled();

  // load list of index textures
  ParamEntryVal list = Pars >> "CfgDiary" >> "Icons";
  for (int i=0; i<list.GetEntryCount(); i++)
  {
    ParamEntryVal entry = list.GetEntry(i);
    Ref<Texture> texture = GlobLoadTexture(GetPictureName(entry));
    if (texture) _indexTextures.Add(TextureByName(entry.GetName(), texture));
  }

  InitDiaryTopicList(_map, _topics, _diaryPerson);
  ProcessLink(link);
}

void DisplayDiaryInMap::ProcessLink(RString link)
{
  // Unlike OnHTMLLink, global (external) links to diary are processed here
  const char *ptr = link;
  ResizeHTML();

  // link to the task
  const char *name = "task:";
  int n = strlen(name);
  if (strnicmp(ptr, name, n) == 0)
  {
    ptr += n;
    int taskId = atoi(ptr);
    int index = -1;
    if (_diaryPerson)
    {
      Identity &identity = _diaryPerson->GetIdentity();
      Task *task = identity.FindTask(taskId);
      if (task)
      {
        // which page contains the list of tasks
        RString pageName;
        for (int i=0; i<identity.NDiarySubjects(); i++)
        {
          const DiaryPage *page = identity.GetDiarySubject(i);
          if (strcmp(page->GetPageType(), "DiaryPageTasks") == 0)
          {
            index = i;
            pageName = page->GetName();
            break;
          }
        }

        if (pageName.GetLength() > 0)
        {
          // switch to the correct page
          SelectPage(pageName);

          // switch to the correct record
          if (_diaryIndex)
          {
            // find the record in the index, select it and synchronize the content
            RString name = Format("Task%d", taskId);
            int i = FindItemWithData(_diaryIndex, name);
            if (i >= 0)
            {
              _diaryIndex->SetCurSel(i);
              SyncDiary();
            }
          }
        }
      }
      SyncTopicList(index);
    }
    return;
  }

  // link to the diary page / diary record
  if (_diaryPerson)
  {
    Identity &identity = _diaryPerson->GetIdentity();
    int index = -1;
    for (int i=0; i<identity.NDiarySubjects(); i++)
    {
      const DiaryPage *page = identity.GetDiarySubject(i);
      RString name = page->GetName();
      if (stricmp(ptr, name) == 0)
      {
        // switch to the correct page
        SelectPage(name);
        index = i;
      }
      else
      {
        RString prefix = name + RString(":");
        int n = prefix.GetLength();
        if (strnicmp(ptr, prefix, n) == 0)
        {
          ptr += n;
          RString recordId = ptr;

          // TODO: switch sort order by the id type

          // switch to the correct page
          SelectPage(name);
          index = i;

          // switch to the correct record
          if (_diaryIndex)
          {
            // find the record in the index, select it and synchronize the content
            int i = FindItemWithData(_diaryIndex, recordId);
            if (i >= 0)
            {
              _diaryIndex->SetCurSel(i);
              SyncDiary();
            }
          }
        }
      }
    }
    SyncTopicList(index);
  }
}

void DisplayDiaryInMap::SyncTopicList(int index)
{
  //if external link has been processed, synchronize diary topic 
  if(_diaryTopics)
  {
    if(_diaryTopics->GetValue(_diaryTopics->GetCurSel()) != index + IDC_DIARY_TOPIC_1)

      for (int i = 0; i< _diaryTopics->Size();i++)
      {
        if (_diaryTopics->GetValue(i) == index + IDC_DIARY_TOPIC_1)
        {
          _diaryTopics->SetCurSel(i);
        }
      }
  }
}

void DisplayDiaryInMap::ClearLink()
{//hide everything to see only map 
  SelectPage("");
  ProcessLink("");
}

void DisplayDiaryInMap::ResizeHTML()
{
  //if diary content changes, this ensure that HMTL text will not be broken into smaller pages
  if (_diary) _diary->SetPos(_diary->X(),_diary->Y(),_diary->W(),_maxDiaryH);
  if (_controlsGroup) _controlsGroup->SetPos(_controlsGroup->X(),_controlsGroup->Y(),_controlsGroup->W(),_maxDiaryH);
}

void DisplayDiaryInMap::ResetListBox()
{ //resize index list and it's background
  bool showMenu = GWorld->UI()->IsMenuVisible();

  if(_diaryIndex)
  { 
    Control *ctrl = dynamic_cast<Control *> (_map->GetCtrl(IDC_DIARY_SUBTOPIC_BACKGROUND));
    if(_diaryIndex->Size()<=0 || showMenu) 
    {
      _diaryIndex->ShowCtrl(false);
      if(ctrl) ctrl->ShowCtrl(false);
    }
    else 
    {//hide if empty
      if(!_diaryIndex->IsVisible()) _diaryIndex->ShowCtrl(true);
      if(ctrl) ctrl->ShowCtrl(true);
      float size = floatMin(0.002f +_diaryIndex->Size() * _diaryIndex->GetRowHeight(),_maxDiaryIndexH);
      _diaryIndex->SetPos(_diaryIndex->X(),_diaryIndex->Y(),_diaryIndex->W(),size);
      if(ctrl) ctrl->SetPos(ctrl->X(),ctrl->Y(),ctrl->W(),size+0.04);
    }  
  }
  //reset HMTL control and it's background
  if (_diary)
  {
    Control *ctrl = dynamic_cast<Control *> (_map->GetCtrl(IDC_DIARY_CONTENT_BACKGROUND));
    float size = 0;

    if(_diary->NSections()>0)
    {
      HTMLSection &section = _diary->GetSection(_diary->CurrentSection());
      //get HTML control height
      for (int r=0; r<section.rows.Size(); r++)
      {
        HTMLRow &row = section.rows[r];
        size += row.height;
      }
    }
    //if HTML control is visible and !empty
    if(_diary->NSections()>0 && size>0 && !showMenu)
    {
      size = floatMin(size,_maxDiaryH);
      _diary->ShowCtrl(true);       
      _diary->SetPos(_diary->X(),_diary->Y(),_diary->W(),size);
      if(_controlsGroup)
      {
        _controlsGroup->ShowCtrl(true);
        size = floatMin(size,_maxControlsGroupH);
        _controlsGroup->SetPos(_controlsGroup->X(),_controlsGroup->Y(),_controlsGroup->W(),size+0.005f);
        if(ctrl)
        {
          ctrl->ShowCtrl(true);
          ctrl->SetPos(ctrl->X(),ctrl->Y(),ctrl->W(),size+0.005f+0.04);
        }
      }
    }
    else  
    {//hide (if not hidden)
      if(_controlsGroup->IsVisible())
      {
        if(_controlsGroup)_controlsGroup->ShowCtrl(false);
        _diary->ShowCtrl(false);
        if(ctrl) ctrl->ShowCtrl(false);

        IControl *ctrl = _map->GetCtrl(IDC_DIARY_TOPIC_LIST);
        if(ctrl) _map->FocusCtrl(ctrl);
      }
    }
  }
  //resize topic listbox and it's background
  if(_diaryTopics && !showMenu)
  {
    Control *ctrl = dynamic_cast<Control *> (_map->GetCtrl(IDC_DIARY_TOPIC_BACKGROUND));
    float size = floatMin(0.002f + _diaryTopics->Size() * _diaryTopics->GetRowHeight(),_maxDiaryTopicsH);
    _diaryTopics->SetPos(_diaryTopics->X(),_diaryTopics->Y(),_diaryTopics->W(),size);
    if(!_diaryTopics->IsVisible())  _diaryTopics->ShowCtrl(true);
    if(ctrl)
    {
      ctrl->SetPos(ctrl->X(), ctrl->Y(), ctrl->W(), size+0.04);
      ctrl->ShowCtrl(true);
    }
  }
  else
  {
    Control *ctrl = dynamic_cast<Control *> (_map->GetCtrl(IDC_DIARY_TOPIC_BACKGROUND));
    if(_diaryTopics)
      _diaryTopics->ShowCtrl(false);
    if(ctrl) ctrl->ShowCtrl(false);
  }
}

void DisplayDiaryInMap::HandleReferences(const HTMLSection &section)
{
  Assert(_diary);
  for (int i=0; i<section.fields.Size(); i++)
  {
    const char *href = section.fields[i].href;
    if (*href != '#') continue;
    // link to other section
    RString name = href + 1;
    // check if already in diary
    if (_diary->FindSection(name) >= 0) continue;
    // find in the briefing
    int ss = _briefing.FindSection(name);
    if (ss < 0) continue;

    // need to be copied
    int s = _diary->AddSection();
    HTMLSection &dstSection = _diary->GetSection(s);
    const HTMLSection &srcSection = _briefing.GetSection(ss);
    // create the index item
    if (_diaryIndex)
    {
      int index = _diaryIndex->AddString(section.fields[i].text);
      _diaryIndex->SetData(index, name);
    }
    // copy the content
    dstSection = srcSection;
    // finish
    _diary->FormatSection(s);
    // handle references to other sections
    HandleReferences(srcSection);
  }
}

void DisplayDiaryInMap::WriteBriefingNotes()
{
  AIBrain *brain = _diaryPerson ? _diaryPerson->Brain() : NULL;
  AIUnit *unit = brain ? brain->GetUnit() : NULL;

  int section = FindSectionForUnit(_briefing, "Main", unit);
  if (section >= 0)
  {
    // add the section to the diary
    if (_diary)
    {
      const HTMLSection &srcSection = _briefing.GetSection(section);
      int s = _diary->AddSection();
      HTMLSection &dstSection = _diary->GetSection(s);
      RString name = srcSection.names.Size() > 0 ? srcSection.names[0] : "Notes";
      // create the index item
      if (_diaryIndex)
      {
        int index = _diaryIndex->AddString(LocalizeString(IDS_MAP_NOTES));
        _diaryIndex->SetData(index, name);
      }
      // title
      _diary->AddText(s, LocalizeString(IDS_MAP_NOTES), HFH1, HALeft, false, false, RString());
      _diary->AddBreak(s, false);
      // text
      for (int i=0; i<srcSection.fields.Size(); i++) dstSection.fields.Add(srcSection.fields[i]);
      dstSection.fields.Compact();
      // names
      dstSection.names = srcSection.names;
      if (dstSection.names.Size() == 0) dstSection.names.Add(name);
      // finish
      _diary->FormatSection(s);
      // handle references to other sections
      HandleReferences(srcSection);
    }
  }
}

void DisplayDiaryInMap::WriteBriefingPlan()
{
  AIBrain *brain = _diaryPerson ? _diaryPerson->Brain() : NULL;
  AIUnit *unit = brain ? brain->GetUnit() : NULL;
  TargetSide side = brain ? brain->GetSide() : TSideUnknown;

  // plan header
  int section = FindSectionForUnit(_briefing, "Plan", side, unit);
  if (section >= 0)
  {
    // add the section to the diary
    if (_diary)
    {
      const HTMLSection &srcSection = _briefing.GetSection(section);
      int s = _diary->AddSection();
      HTMLSection &dstSection = _diary->GetSection(s);
      RString name = srcSection.names.Size() > 0 ? srcSection.names[0] : "Plan";
      // create the index item
      if (_diaryIndex)
      {
        int index = _diaryIndex->AddString(LocalizeString(IDS_MAP_PLAN));
        _diaryIndex->SetData(index, name);
      }
      // title
      _diary->AddText(s, LocalizeString(IDS_MAP_PLAN), HFH1, HALeft, false, false, RString());
      _diary->AddBreak(s, false);
      // text
      for (int i=0; i<srcSection.fields.Size(); i++) dstSection.fields.Add(srcSection.fields[i]);
      dstSection.fields.Compact();
      // names
      dstSection.names = srcSection.names;
      if (dstSection.names.Size() == 0) dstSection.names.Add(name);
      // finish
      _diary->FormatSection(s);
      // handle references to other sections
      HandleReferences(srcSection);
    }
  }

  // objectives
  int objective = 1;
  for (int section=0; section<_briefing.NSections(); section++)
  {
    const HTMLSection &src = _briefing.GetSection(section);
    for (int n=0; n<src.names.Size(); n++)
    {
      RString srcName = src.names[n];
      if (strnicmp(srcName, "OBJ_", strlen("OBJ_")) != 0) continue; // optimization

      struct NamePrefix
      {
        const char *prefix;
        const char *prefixReplace;
        TargetSide side;
        bool hidden;
      };

      NamePrefix prefixes[] =
      {
        {"OBJ_WEST_HIDDEN_", "OBJ_WEST_", TWest, true},
        {"OBJ_EAST_HIDDEN_", "OBJ_EAST_", TEast, true},
        {"OBJ_GUER_HIDDEN_", "OBJ_GUER_", TGuerrila, true},
        {"OBJ_CIVIL_HIDDEN_", "OBJ_CIVIL_", TCivilian, true},
        {"OBJ_HIDDEN_", "OBJ_", TSideUnknown, true},
        {"OBJ_WEST_", NULL, TWest, false},
        {"OBJ_EAST_", NULL, TEast, false},
        {"OBJ_GUER_", NULL, TGuerrila, false},
        {"OBJ_CIVIL_", NULL, TCivilian, false},
        {"OBJ_", NULL, TSideUnknown, false}
      };

      // find the longest matching prefix
      int match = -1;
      for (int i=0; i<lenof(prefixes); i++)
      {
        if (strnicmp(srcName, prefixes[i].prefix, strlen(prefixes[i].prefix)) == 0)
        {
          match = i;
          break;
        }
      }

      // valid objective found
      Assert(match >= 0); // because of optimization
      NamePrefix &prefix = prefixes[match];
      if (prefix.side != TSideUnknown && side != prefix.side) continue;
      if (prefix.prefixReplace) srcName = RString(prefix.prefixReplace) + srcName.Substring(strlen(prefix.prefix), INT_MAX); // fix the name

      int value;
      if (unit)
      {
        GameState *state = GWorld->GetGameState();
        value = toInt((float)state->Evaluate(srcName, GameState::EvalContext::_default, GWorld->GetMissionNamespace())); // mission namespace
      }
      else
      {
        value = prefix.hidden ? OSHidden : OSActive; 
      }

      if (value == OSHidden) break;  // next objective

      // add objective to the diary
      ParamEntryVal cls = Pars >> "RscObjectives";
      RString picture;
      switch (value)
      {
      case OSDone:
        picture = cls >> "done";
        break;
      case OSFailed:
        picture = cls >> "failed";
        break;
      default:
        Fail("Unknown objective type");
      case OSActive:
        picture = cls >> "active";
        break;
      }

      RString name = src.names.Size() > 0 ? src.names[0] : Format("Objective_%d", objective);
      RString displayName = Format(LocalizeString(IDS_LOG_TITLE_OBJECTIVE), objective);
      objective++;

      // index
      if (_diaryIndex)
      {
        int index = _diaryIndex->AddString(displayName);
        _diaryIndex->SetData(index, name);
      }
      // content
      if (_diary)
      {
        int s = _diary->AddSection();
        HTMLSection &dstSection = _diary->GetSection(s);
        // title
        _diary->AddText(s, displayName, HFH1, HALeft, false, false, RString());
        _diary->AddBreak(s, false);
        // picture
        float imgHeight = 480.0f * 1.0f * _diary->GetPHeight();
        HTMLField *fld = _diary->AddImage(s, picture, HALeft, false, -1, imgHeight, "");
        fld->exclude = true;
        float indent = 1.2 * fld->width;
        // text
        for (int i=0; i<src.fields.Size(); i++)
        {
          int index = dstSection.fields.Add(src.fields[i]);
          dstSection.fields[index].indent += indent;
        }
        dstSection.fields.Compact();
        // name
        dstSection.names = src.names;
        if (dstSection.names.Size() == 0) dstSection.names.Add(name);
        // finish
        _diary->FormatSection(s);
        // handle references to other sections
        HandleReferences(src);
      }
      break; // next objective
    }
  }
}

void DisplayDiaryInMap::UpdateDiary(bool force)
{
  AIBrain *unit = GWorld->FocusOn();
  if(!unit) return;
  AIUnit* aiUnit = unit->GetUnit();
  if(!_diaryPerson)
    _diaryPerson = unit ? unit->GetPerson() : NULL;

  if(unit && unit->GetRemoteControlled())
    _diaryPerson = unit->GetRemoteControlled();

  Assert(_diary);
  Assert(_diaryPerson);

  ResizeHTML();

  Identity &identity = _diaryPerson->GetIdentity();
  RString pageName = GetCurrentPage();
  AIGroup *group = unit->GetGroup();

  if(_diaryPerson)
  {
    RString groupName;
    RString taskDesc;
    int rank = RankUndefined; 
    rank = _diaryPerson->GetRank();

    RString name =  _diaryPerson->GetPersonName();
    if(group) groupName =group->GetName();
    if(_playerName && aiUnit) 
      _playerName->SetText(LocalizeString(IDS_SHORT_PRIVATE + rank) + " " +  name + ", " + groupName + " " + FormatNumber(aiUnit->ID()));
    Task *task = identity.GetCurrentTask();
    if(task) task->GetDescriptionShort(taskDesc);
    if(_currentTask) _currentTask->SetText(taskDesc);
  }   
  else
  { 
    if(_currentTask) _currentTask->SetText("");
    if(_playerName) _playerName->SetText("");
  }

  if (force || pageName.GetLength() > 0 && identity.IsDiaryChanged(pageName))
  {
    // store selected item in the index
    RString selected;
    if (_diaryIndex)
    {
      int index = _diaryIndex->GetCurSel();
      if (index >= 0) selected = _diaryIndex->GetData(index);
      _diaryIndex->ClearStrings();
    }

    // store selected section in the HTML
    RString selectedSection;
    int currentSection = _diary->CurrentSection();
    if (currentSection >= 0 && currentSection < _diary->NSections())
    {
      const HTMLSection &section = _diary->GetSection(currentSection);
      for (int i=0; i<section.names.Size(); i++)
      {
        if (stricmp(section.names[i], selected) != 0)
        {
          selectedSection = section.names[i]; // first which name is different from the core page name
          break;
        }
      }
    }

    int activeField = _diary->GetActiveFieldIndex();

    _diary->Init();

    // create new content of diary
    DiaryPage *page = identity.FindDiarySubject(pageName);
    if (page)
    {
      // for backward compatibility, handle content of briefing.html (notes, plan)
      RString pageType = page->GetPageType();
      if (stricmp(pageType, "DiaryPageDiary") == 0) WriteBriefingNotes();
      else if (stricmp(pageType, "DiaryPageTasks") == 0) WriteBriefingPlan();

      // now process regular diary records
      WriteToDiary func(_diaryIndex, _diary, page, _indexTextures, identity, selected);
      page->ForEachRecord(func, _diaryPerson);
      func.Finish();
      SyncDiary();
      // try to recover HTML state before the update
      if (selectedSection.GetLength() > 0) _diary->SwitchSection(selectedSection);
      if (activeField >= 0)
      {
        int currentSection = _diary->CurrentSection();
        if (currentSection >= 0 && currentSection < _diary->NSections())
        {
          const HTMLSection &section = _diary->GetSection(currentSection);
          if (activeField < section.fields.Size() && section.fields[activeField].href.GetLength() > 0)
            _diary->SetActiveFieldIndex(activeField);
        }
      }
    }
  }
  UpdateButtons();
}

void DisplayDiaryInMap::SyncDiary()
{

  if (!_diary || !_diaryIndex) return;
  int index = _diaryIndex->GetCurSel();
  if (index >= 0)
  {
    _diary->SwitchSection(_diaryIndex->GetData(index));
  }
  else
  {
    // select the last section in the topic
    int n = intMax(_diary->NSections() - 1, 0);
    _diary->SwitchSectionRaw(n);
  }
}

void DisplayDiaryInMap::UpdateTopics()
{
  if(_diaryPerson) InitDiaryTopicList(_map, _topics, _diaryPerson, DCBriefing);
}

void DisplayDiaryInMap::UpdateButtons()
{
  if (_diaryAdd)
  {
    IControl *ctrl = _diaryAdd->GetControl();
    if (_diaryPerson == NULL)
    {
      ctrl->ShowCtrl(false);
    }
    else
    {
      RString page = GetCurrentPage();
      if (page.GetLength() == 0)
      {
        ctrl->ShowCtrl(false);
      }
      else
      {
        Identity &identity = _diaryPerson->GetIdentity();
        ctrl->ShowCtrl(!identity.IsDiaryReadOnly(page));
      }
    }
  }
}

RString DisplayDiaryInMap::GetCurrentPage() const
{
  if (!_diaryPerson) return RString();
  return _diaryPerson->GetIdentity().GetCurrentDiarySubject();
}

#endif

ChatChannel ActualChatChannel();
void SetChatChannel(ChatChannel channel);


void PoolReplaceWeapon(AIUnit *unit, WeaponType *weapon, int slot, bool useBackpack)
{
  if (GBriefing) GBriefing->PoolReplaceWeapon(unit, weapon, slot, useBackpack);
}

void PoolReplaceMagazine(AIUnit *unit, Magazine *magazine, int slot, bool useBackpack)
{
  if (GBriefing) GBriefing->PoolReplaceMagazine(unit, magazine, slot, useBackpack);
}

void PoolReplaceBackpack(AIUnit *unit, EntityAI *backpack)
{
  if (GBriefing) GBriefing->PoolReplaceBackpack(unit, backpack);
}

void PoolUpdateWeapons(NetworkMessageContext &ctx)
{
  if (GBriefing) GBriefing->PoolUpdateWeapons(ctx);
}

void PoolUpdateWeaponsPool(NetworkMessageContext &ctx)
{
  if (GBriefing) GBriefing->PoolUpdateWeaponsPool(ctx);
}

void PoolUpdateMagazinesPool(NetworkMessageContext &ctx)
{
  if (GBriefing) GBriefing->PoolUpdateMagazinesPool(ctx);
}

void PoolUpdateBackpacksPool(NetworkMessageContext &ctx)
{
  if (GBriefing) GBriefing->PoolUpdateBackpacksPool(ctx);
}

void DisplayGetReady::PoolReplaceWeapon(AIUnit *unit, WeaponType *weapon, int slot, bool useBackpack)
{
  if (!unit) return;

  // find weapon info
  int index = -1;
  UnitWeaponsInfo *info = NULL;
  for (int i=0; i<_weaponsInfo._weapons.Size(); i++)
  {
    if (_weaponsInfo._weapons[i].unit == unit)
    {
      index = i;
      info = &_weaponsInfo._weapons[i];
      break;
    }
  }
  if (!info) return;

  _pool.OnWeaponReplaced(*info, slot, weapon, useBackpack);

  // update gear dialog
  if (_child && _child->IDD() == IDD_GEAR)
  {
    DisplayGear *display = static_cast<DisplayGear *>(_child.GetRef());
    display->UpdateWeapons();
  }
}

void DisplayGetReady::PoolReplaceMagazine(AIUnit *unit, Magazine *magazine, int slot, bool useBackpack)
{
  if (!unit) return;

  // find weapon info
  UnitWeaponsInfo *info = NULL;
  for (int i=0; i<_weaponsInfo._weapons.Size(); i++)
  {
    if (_weaponsInfo._weapons[i].unit == unit)
    {
      info = &_weaponsInfo._weapons[i];
      break;
    }
  }
  if (!info) return;

  _pool.OnMagazineReplaced(*info, slot, magazine, useBackpack);

  // update gear dialog
  if (_child && _child->IDD() == IDD_GEAR)
  {
    DisplayGear *display = static_cast<DisplayGear *>(_child.GetRef());
    display->UpdateWeapons();
  }
}

void DisplayGetReady::PoolReplaceBackpack(AIUnit *unit, EntityAI *backpack)
{
  if (!unit) return;

  // find weapon info
  int index = -1;
  UnitWeaponsInfo *info = NULL;
  for (int i=0; i<_weaponsInfo._weapons.Size(); i++)
  {
    if (_weaponsInfo._weapons[i].unit == unit)
    {
      index = i;
      info = &_weaponsInfo._weapons[i];
      break;
    }
  }
  if (!info) return;

  _pool.OnBackpackReplaced(*info, backpack);


  // add weapon
  info->backpack = backpack;

  // update gear dialog
  if (_child && _child->IDD() == IDD_GEAR)
  {
    DisplayGear *display = static_cast<DisplayGear *>(_child.GetRef());
    display->UpdateWeapons();
  }
}

void DisplayGetReady::PoolUpdateWeapons(NetworkMessageContext &ctx)
{
  PREPARE_TRANSFER(UpdateWeaponsInfo)

    OLinkPerm<AIUnit> unit;
  if (TRANSF_REF_BASE(unit, unit) != TMOK) return;
  if (!unit) return;

  for (int i=0; i<_weaponsInfo._weapons.Size(); i++)
  {
    if (_weaponsInfo._weapons[i].unit == unit)
    {
      _weaponsInfo._weapons[i].TransferMsg(ctx);

      // update gear dialog
      if (_child && _child->IDD() == IDD_GEAR)
      {
        DisplayGear *display = static_cast<DisplayGear *>(_child.GetRef());
        display->UpdateWeapons();
      }

      return;
    }
  }
}

#if _GAMES_FOR_WINDOWS || (defined _XBOX && _XBOX_VER >= 200)

void DisplayGetReady::OnInvited()
{
  if (_msgBox) return; // confirmation in progress

  if (GSaveSystem.IsInvitationConfirmed() || GetNetworkManager().GetClientState() == NCSNone)
  {
    // no warning is needed in the single player
    Exit(IDC_OK);
  }
  else
  {
    GSaveSystem.ConfirmInvitation();
    // let the user confirm the action because of possible progress lost
    MsgBoxButton buttonOK(LocalizeString(IDS_DISP_XBOX_HINT_YES), INPUT_DEVICE_XINPUT + XBOX_A);
    MsgBoxButton buttonCancel(LocalizeString(IDS_DISP_XBOX_HINT_NO), INPUT_DEVICE_XINPUT + XBOX_B);
    RString message;
    if (GetNetworkManager().IsServer())
      message = LocalizeString(IDS_MSG_CONFIRM_TERMINATE_SESSION);
    else
      message = LocalizeString(IDS_MSG_CONFIRM_TERMINATE_SESSION_CLIENT);
    // special type of message box which will not close because of invitation
    _msgBox = new MsgBoxIgnoreInvitation(this, MB_BUTTON_OK | MB_BUTTON_CANCEL, message, IDD_MSG_ACCEPT_INVITATION, false, &buttonOK, 

      &buttonCancel);
  }
}

#endif

void DisplayGetReady::PoolUpdateWeaponsPool(NetworkMessageContext &ctx)
{
  _pool._weaponsPool.TransferMsg(ctx);
  // update gear dialog
  if (_child && _child->IDD() == IDD_GEAR)
  {
    DisplayGear *display = static_cast<DisplayGear *>(_child.GetRef());
    display->UpdateWeapons();
  }
}

void DisplayGetReady::PoolUpdateMagazinesPool(NetworkMessageContext &ctx)
{
  _pool._magazinesPool.TransferMsg(ctx);
  // update gear dialog
  if (_child && _child->IDD() == IDD_GEAR)
  {
    DisplayGear *display = static_cast<DisplayGear *>(_child.GetRef());
    display->UpdateWeapons();
  }
}

void DisplayGetReady::PoolUpdateBackpacksPool(NetworkMessageContext &ctx)
{
  _pool._backpacksPool.TransferMsg(ctx);
  // update gear dialog
  if (_child && _child->IDD() == IDD_GEAR)
  {
    DisplayGear *display = static_cast<DisplayGear *>(_child.GetRef());
    display->UpdateWeapons();
  }
}

/*!
\patch 1.50 Date 4/15/2002 by Jirka
- Improved: side chat channel selected when entering briefing screen
*/
DisplayGetReady::DisplayGetReady(ControlsContainer *parent)
: DisplayMap(parent, "RscDisplayGetReady")
{
  Assert(!GBriefing);
  GBriefing = this;

  _enableSimulation = false;
  _enableDisplay = false;
  ShowCompass(false);
  ShowWatch(false);
  ShowWalkieTalkie(false);
  ShowGPS(false);
  ConstParamEntryPtr cls = ExtParsMission.FindEntry("showMap");
  ShowMap(!cls ? true : (*cls));
  ShowBriefing(true);
  cls = ExtParsMission.FindEntry("showNotepad");
  ShowNotepad(!cls ? true : (*cls));

  // initialization
  _soundPlanPlayed = false;
  _soundNotesPlayed = false;
  _soundGroupPlayed = false;
  _soundTeamSwitchPlayed = false;

  //  SwitchDebriefing(false);

  AIBrain *unit = GWorld->FocusOn();
  ConstParamEntryPtr clsWeapon = ExtParsMission.FindEntry("allowSubordinatesTakeWeapons");
  _canTakeWeapon = (clsWeapon ? (*clsWeapon) : false) && unit;

  if (GWorld->GetMode() == GModeNetware)
    _selectWeapons = unit != NULL; // everybody can change his weapons in MP
  else
    _selectWeapons = unit && (unit->IsGroupLeader() || _canTakeWeapon) ;

#if defined _XBOX && _XBOX_VER >= 200
  QIStrStream in;
  if (GSaveSystem.ReadWeaponConfig(in))
    _weaponsInfo.Load(in, _pool);
  else
    UpdateWeaponsInBriefing(NULL, true);
#else
  RString dir = GetSaveDirectory();
  RString weapons;
  if (dir.GetLength() > 0) weapons = dir + RString("weapons.cfg");
  if (weapons.GetLength() > 0 && QIFileFunctions::FileExists(weapons))
    _weaponsInfo.Load(weapons, _pool);
  else
    UpdateWeaponsInBriefing(NULL, true);
#endif

  // _showGearOnExit = _weaponsInfo._weaponsPool.Size() > 0 || _weaponsInfo._magazinesPool.Size() > 0;
  _showGearOnExit = true;
  ConstParamEntryPtr entry = (Pars >> "RscDisplayGetReady").FindEntry("showGearOnExit");
  if (entry) _showGearOnExit = *entry;

  // FIX - cancel user input
  void RemoveInputMessages();
  RemoveInputMessages();

  GChatList.Load(Pars >> "RscChatListBriefing");

  if (GetNetworkManager().GetClientState() >= NCSConnected)
  {
    // Multiplayer specific code
    if (ActualChatChannel() != CCSide)
    {
      // enforce side chat channel
      SetChatChannel(CCSide);
      if (GWorld->ChatChannel()) GWorld->ChatChannel()->ResetHUD();
      if (GWorld->VoiceChat()) GWorld->VoiceChat()->ResetHUD();
#ifndef _XBOX
      GWorld->OnChannelChanged();
#endif
    }
  }

  if (_briefing)
  {
    int index = _briefing->CurrentSection();
    const HTMLSection &section = _briefing->GetSection(index);
    if (section.names.Size() > 0)
      SwitchBriefingSection(section.names[0]);
  }

#if _ENABLE_IDENTITIES
  _displayDiaryMap = DisplayDiaryInMap(this);
  CListBox *list = dynamic_cast<CListBox *> (this->GetCtrl(IDC_DIARY_TOPIC_LIST));
  if(list && list->Size()>0) 
  {
    if(GWorld->GetMode()  == GModeNetware)
      _displayDiaryMap.ProcessLink("players",DCBriefing);
    else 
      _displayDiaryMap.ProcessLink("diary",DCBriefing);
  }
  _displayDiaryMap.ResetListBox();
#endif
}

DisplayGetReady::DisplayGetReady(ControlsContainer *parent, RString resource)
: DisplayMap(parent, resource)
{
  Assert(!GBriefing);
  GBriefing = this;

  _enableSimulation = false;
  _enableDisplay = false;
  ShowCompass(false);
  ShowWatch(false);
  ShowWalkieTalkie(false);
  ShowGPS(false);
  ConstParamEntryPtr cls = ExtParsMission.FindEntry("showMap");
  ShowMap(!cls ? true : (*cls));
  cls = ExtParsMission.FindEntry("showNotepad");
  ShowNotepad(!cls ? true : (*cls));

  // FIX: initialization
  _soundPlanPlayed = false;
  _soundNotesPlayed = false;
  _soundGroupPlayed = false;
  _soundTeamSwitchPlayed = false;

  //  SwitchDebriefing(false);

  AIBrain *unit = GWorld->FocusOn();
  ConstParamEntryPtr clsWeapon = ExtParsMission.FindEntry("allowSubordinatesTakeWeapons");
  _canTakeWeapon = (clsWeapon ? (*clsWeapon) : false) && unit;

  if (GWorld->GetMode() == GModeNetware)
    _selectWeapons = unit != NULL; // everybody can change his weapons in MP
  else
    _selectWeapons = unit && (unit->IsGroupLeader() || _canTakeWeapon) ;

#if defined _XBOX && _XBOX_VER >= 200
  QIStrStream in;
  if (GSaveSystem.ReadWeaponConfig(in))
    _weaponsInfo.Load(in, _pool);
  else
    UpdateWeaponsInBriefing(NULL, true);
#else
  RString dir = GetSaveDirectory();
  RString weapons;
  if (dir.GetLength() > 0) weapons = dir + RString("weapons.cfg");
  if (weapons.GetLength() > 0 && QIFileFunctions::FileExists(weapons))
    _weaponsInfo.Load(weapons, _pool);
  else
    UpdateWeaponsInBriefing(NULL, true);
#endif

  // _showGearOnExit = _weaponsInfo._weaponsPool.Size() > 0 || _weaponsInfo._magazinesPool.Size() > 0;
  _showGearOnExit = true;
  ConstParamEntryPtr entry = (Pars >> "RscDisplayGetReady").FindEntry("showGearOnExit");
  if (entry) _showGearOnExit = *entry;

  // FIX - cancel user input
  void RemoveInputMessages();
  RemoveInputMessages();

  GChatList.Load(Pars >> "RscChatListBriefing");

  if
    (
    GetNetworkManager().GetClientState() >= NCSConnected &&
    ActualChatChannel() != CCSide
    )
  {
    SetChatChannel(CCSide);
    if (GWorld->ChatChannel()) GWorld->ChatChannel()->ResetHUD();
    if (GWorld->VoiceChat()) GWorld->VoiceChat()->ResetHUD();
#ifndef _XBOX
    GWorld->OnChannelChanged();
#endif
  }

#if _ENABLE_IDENTITIES
  _displayDiaryMap = DisplayDiaryInMap(this);
  CListBox *list = dynamic_cast<CListBox *> (this->GetCtrl(IDC_DIARY_TOPIC_LIST));
  if(list && list->Size()>0) 
  {
    if(list && list->Size()>0) 
    {
      if(GWorld->GetMode()  == GModeNetware)
        _displayDiaryMap.ProcessLink("players",DCBriefing);
      else 
        _displayDiaryMap.ProcessLink("diary",DCBriefing);
    }
  }
  _displayDiaryMap.ResetListBox();
#endif
}

DisplayGetReady::~DisplayGetReady()
{
  GChatList.Load(Pars >> "RscChatListDefault");
  GBriefing = NULL;
}

/*!
\patch 1.75 Date 2/7/2002 by Jirka
- Added: Play user defined sound (voice) when section in briefing changes
*/

extern RString GBriefingOnPlan;
extern RString GBriefingOnNotes;
extern RString GBriefingOnGroup;
extern RString GBriefingOnTeamSwitch;


void DisplayGetReady::SwitchBriefingSection(RString section)
{
  DisplayMap::SwitchBriefingSection(section);
#if _ENABLE_DATADISC
  _sound = NULL;
  if (stricmp(section, "__plan") == 0)
  {
    if (!_soundPlanPlayed)
    {
      PlaySound(GBriefingOnPlan);
      _soundPlanPlayed = true;
    }
  }
  else if (stricmp(section, "__briefing") == 0)
  {
    if (!_soundNotesPlayed)
    {
      PlaySound(GBriefingOnNotes);
      _soundNotesPlayed = true;
    }
  }
  else if (stricmp(section, "group") == 0)
  {
    if (!_soundGroupPlayed)
    {
      PlaySound(GBriefingOnGroup);
      _soundGroupPlayed = true;
    }
  }
  else if (stricmp(section, "teamswitch") == 0)
  {
    if (!_soundTeamSwitchPlayed)
    {
      PlaySound(GBriefingOnTeamSwitch);
      _soundTeamSwitchPlayed = true;
    }
  }
#endif
}

ConstParamEntryPtr FindSound(RString name, SoundPars &pars);

void DisplayGetReady::PlaySound(RString name)
{
  if (name.GetLength() == 0) return;

  SoundPars pars;
  if (!FindSound(name, pars)) return;
  _sound = GSoundScene->OpenAndPlayOnce2D
    (
    pars.name, pars.vol, pars.freq, false
    );
  _sound->SetKind(WaveMusic);
  _sound->SetSticky(true);
}

void DisplayGetReady::OnButtonClicked(int idc)
{
  switch (idc)
  {
  case IDC_OK:
    if (_showGearOnExit)
    {
      // _showGearOnExit = false;
      AIBrain *me = GWorld->FocusOn();
      Assert(!IsInGame());
      CreateChild(new DisplayGear(this, _enableSimulation, _weaponsInfo, _pool, me, true));
    }
    else Display::OnButtonClicked(idc);
    // before proceeding make sure we have preloading anything necessary
    // time limit can be quite relaxed here
    //GWorld->PreloadAroundCamera(0.5f);
    break;
  case IDC_CANCEL:
    if (GetNetworkManager().IsServer())
    {
      MsgBoxButton buttonOK(LocalizeString(IDS_DISP_XBOX_HINT_YES), INPUT_DEVICE_XINPUT + XBOX_A);
      MsgBoxButton buttonCancel(LocalizeString(IDS_DISP_XBOX_HINT_NO), INPUT_DEVICE_XINPUT + XBOX_B);
      CreateMsgBox(
        MB_BUTTON_OK | MB_BUTTON_CANCEL,
        LocalizeString(IDS_MSG_CONFIRM_RETURN_LOBBY),
        IDD_MSG_TERMINATE_SESSION, false, &buttonOK, &buttonCancel);
    }
    else if (GWorld->GetMode() == GModeNetware)
    {
      MsgBoxButton buttonOK(LocalizeString(IDS_DISP_XBOX_HINT_YES), INPUT_DEVICE_XINPUT + XBOX_A);
      MsgBoxButton buttonCancel(LocalizeString(IDS_DISP_XBOX_HINT_NO), INPUT_DEVICE_XINPUT + XBOX_B);
      CreateMsgBox(
        MB_BUTTON_OK | MB_BUTTON_CANCEL,
        LocalizeString(IDS_MSG_CONFIRM_RETURN_LOBBY_CLIENT),
        IDD_MSG_TERMINATE_SESSION, false, &buttonOK, &buttonCancel);
    }
    else Display::OnButtonClicked(idc);
    break;
  default:
#if _ENABLE_IDENTITIES
    _displayDiaryMap.OnButtonClicked(idc);
#endif
    DisplayMap::OnButtonClicked(idc);
    break;
  }
}


void DisplayGetReady::OnChildDestroyed(int idd, int exit)
{
#if _ENABLE_IDENTITIES
  _displayDiaryMap.OnChildDestroyed(idd,exit, _child);
#endif

  DisplayMap::OnChildDestroyed(idd, exit);

  switch (idd)
  {
  case IDD_GEAR:
    if (exit == IDC_OK) Display::OnButtonClicked(IDC_OK);
    break;
  case IDD_MSG_TERMINATE_SESSION:
    if (exit == IDC_OK) Exit(IDC_CANCEL);
    break;
#if _GAMES_FOR_WINDOWS || (defined _XBOX && _XBOX_VER >= 200)
  case IDD_MSG_ACCEPT_INVITATION:
    if (exit == IDC_OK)
    {
      Exit(IDC_CANCEL);
    }
    else
    {
      // Invitation rejected
      GSaveSystem.InvitationHandled();
    }
    break;    
#endif
  }
}

void DisplayGetReady::OnSimulate(EntityAI *vehicle)
{
  /// we can preload any data as necessary here
  if (!_map->_interpolator)
  {
    GWorld->PreloadAroundCamera(0.030f); // keep reasonable framerate
  }

#if _ENABLE_IDENTITIES
  _displayDiaryMap.UpdateTopics();
  _displayDiaryMap.UpdateDiary();
  _displayDiaryMap.ResetListBox();
#endif

  // avoid DisplayMapMain::OnSimulate
  DisplayMap::OnSimulate(vehicle);
}

void DisplayGetReady::OnLBSelChanged(IControl *ctrl, int curSel)
{
#if _ENABLE_IDENTITIES
  _displayDiaryMap.OnLBSelChanged(ctrl,  curSel);
#endif
  DisplayMap::OnLBSelChanged(ctrl,curSel);
}

void DisplayGetReady::OnLBDblClick(int idc,  int curSel)
{ 
#if _ENABLE_IDENTITIES
  _displayDiaryMap.OnLBDblClick(idc,  curSel);
#endif
  DisplayMap::OnLBDblClick(idc,  curSel);
}

void DeleteVehicle(Entity *veh);
void DisplayGetReady::Destroy()
{
  // FIX: do not store weapons.cfg for MP games (no restart support)
  if (_selectWeapons)
  {
    if (GWorld->GetMode() != GModeNetware)
    {
#if defined _XBOX && _XBOX_VER >= 200
      QOStrStream out;
      _weaponsInfo.Save(out, _pool);
      GSaveSystem.WriteWeaponConfig(out);
#else
      RString dir = GetSaveDirectory();
      if (dir.GetLength() > 0)
      {
        RString weapons = dir + RString("weapons.cfg");
        _weaponsInfo.Save(weapons, _pool);
      }
#endif
    }
    _weaponsInfo.Apply();
    if (EnabledWeaponPool())
    {
      void CampaignSaveWeaponPool(FixedItemsPool &pool);
      CampaignSaveWeaponPool(_pool);
    }

    for (int i=0; i<_pool._backpacksPool.Size(); i++)
    {
      if (_pool._backpacksPool[i]._backpack && _pool._backpacksPool[i]._backpack->IsLocal())
      {
        DeleteVehicle(_pool._backpacksPool[i]._backpack);
      }
    }
  }
  DisplayMap::Destroy();
}

void DisplayGetReady::ProcessDiaryLink(RString link,DiaryContext contex)
{
#if _ENABLE_IDENTITIES
  _displayDiaryMap.ProcessLink(link, contex);
#endif
}


/*!
\patch 1.21 Date 08/21/2001 by Jirka
- Improved: Multiplayer debriefing display
*/
DisplayDebriefing::DisplayDebriefing(ControlsContainer *parent, bool animation, bool restartEnabled)
: Display(parent)
{
  //  GWorld->EnableDisplay(false);
  Load("RscDisplayDebriefing");

  _statisticsLinks = true;
  ConstParamEntryPtr entry = (Pars >> "RscDisplayDebriefing").FindEntry("statisticsLinks");
  if (entry) _statisticsLinks = *entry;

  _animation = animation;
  _restartEnabled = restartEnabled;
  _enableSimulation = false;

  _wantedExit = -1;

  if (GetCtrl(IDC_DEBRIEFING_PAD2))
  {
    // Xbox
    GetCtrl(IDC_DEBRIEFING_PAD2)->ShowCtrl(false);
    /*
    if (GetCtrl(IDC_DEBRIEFING_LEFT)) GetCtrl(IDC_DEBRIEFING_LEFT)->ShowCtrl(false);
    */
  }
  else
  {
    // PC
    if (GetCtrl(IDC_DEBRIEFING_STAT)) GetCtrl(IDC_DEBRIEFING_STAT)->ShowCtrl(false);
  }

#if _XBOX_SECURE && _XBOX_VER < 200 && _ENABLE_MP
  // SP statistics are not possible on Xbox 360
  if
    (
    GetNetworkManager().IsServer() && GetNetworkManager().IsSignedIn() && !GetNetworkManager().IsSystemLink() &&
    GWorld->GetEndMode() != EMContinue && GWorld->GetEndMode() != EMKilled
    )
    _statsUpdate = new LiveStatsUpdate();
#endif

  if (GetNetworkManager().IsServer() || GetNetworkManager().IsGameMaster())
  {
    // server
    _server = true;
    _client = false;
    // GetNetworkManager().ClientReady(NGSDebriefingOK);
    IControl *ctrl = GetCtrl(IDC_DEBRIEFING_RESTART);
    if (ctrl)
    {
      ctrl->ShowCtrl(false);
      ITextContainer *text = GetTextContainer(ctrl);
      if (text) text->SetText(LocalizeString(IDS_DISP_CLIENT_READY));
    }
  }
  else if (GetNetworkManager().GetClientState() > NCSNone)
  {
    // client
    _server = false;
    _client = true;
    IControl *ctrl = GetCtrl(IDC_DEBRIEFING_RESTART);
    if (ctrl)
    {
      ITextContainer *text = GetTextContainer(ctrl);
      if (text) text->SetText(LocalizeString(IDS_DISP_CLIENT_READY));
    }
    ctrl = GetCtrl(IDC_CANCEL);
    if (ctrl)
    {
      ITextContainer *text = GetTextContainer(ctrl);
      if (text) text->SetText(LocalizeString(IDS_DISP_DISCONNECT));
    }
  }
  else
  {
    // single player
    _server = false;
    _client = false;
    if (GetCtrl(IDC_DEBRIEFING_PLAYERS_TITLE_BG)) GetCtrl(IDC_DEBRIEFING_PLAYERS_TITLE_BG)->ShowCtrl(false);
    if (GetCtrl(IDC_DEBRIEFING_PLAYERS_TITLE)) GetCtrl(IDC_DEBRIEFING_PLAYERS_TITLE)->ShowCtrl(false);
    if (GetCtrl(IDC_DEBRIEFING_PLAYERS_BG)) GetCtrl(IDC_DEBRIEFING_PLAYERS_BG)->ShowCtrl(false);
    if (GetCtrl(IDC_DEBRIEFING_PLAYERS)) GetCtrl(IDC_DEBRIEFING_PLAYERS)->ShowCtrl(false);
    if (GetCtrl(IDC_DEBRIEFING_SHOW_PLAYERS)) GetCtrl(IDC_DEBRIEFING_SHOW_PLAYERS)->ShowCtrl(false);

    if (GetCtrl(IDC_DEBRIEFING_RESTART)) GetCtrl(IDC_DEBRIEFING_RESTART)->ShowCtrl(_restartEnabled);
  }

#if _XBOX_SECURE && _XBOX_VER < 200 && _ENABLE_MP
  _exiting = false;
#endif

  _objectives = false;

  //CHTML *htmlText = dynamic_cast<CHTML *>(GetCtrl(IDC_DEBRIEFING_STAT));
  //_maxStatsText =  (htmlText)? htmlText->H():0.699f;

  if(!GWorld->IsEndFailed())
    CreateDebriefing();
  else CreateDebriefingFail();

  UpdateButtons();


  CHTML *htmlText = dynamic_cast<CHTML *>(GetCtrl(IDC_DEBRIEFING_DEBRIEFING));
  if(htmlText && _debriefing)
  {
    float size = 0;
    if(_debriefing->NSections()>0)
    {
      HTMLSection &section = _debriefing->GetSection(_debriefing->CurrentSection());
      //get HTML control height
      for (int r=0; r<section.rows.Size(); r++)
      {
        HTMLRow &row = section.rows[r];
        size += row.height;
      }
    }
    htmlText->SetPos(htmlText->X(),htmlText->Y(),htmlText->W(),size);
  }

  htmlText = dynamic_cast<CHTML *>(GetCtrl(IDC_DEBRIEFING_OBJECTIVES));
  if(htmlText && _overview)
  {
    float size = 0;
    if(_overview->NSections()>0)
    {
      HTMLSection &section = _overview->GetSection(_overview->CurrentSection());
      //get HTML control height
      for (int r=0; r<section.rows.Size(); r++)
      {
        HTMLRow &row = section.rows[r];
        size += row.height;
      }
    }
    htmlText->SetPos(htmlText->X(),htmlText->Y(),htmlText->W(),size);
  }

  htmlText = dynamic_cast<CHTML *>(GetCtrl(IDC_DEBRIEFING_STAT));
  if(htmlText && _stats)
  {
    float size = 0;
    if(_stats->NSections()>0)
    {
      HTMLSection &section = _stats->GetSection(_stats->CurrentSection());
      //get HTML control height
      for (int r=0; r<section.rows.Size(); r++)
      {
        HTMLRow &row = section.rows[r];
        size += row.height;
      }
    }
    htmlText->SetPos(htmlText->X(),htmlText->Y(),htmlText->W(),size);
  }
}

void DisplayDebriefing::Destroy()
{
  Display::Destroy();
  //  GWorld->EnableDisplay(true);
}

bool IsCampaignReplay();
bool ParseUserParams(ParamFile &cfg, GameDataNamespace *globals);

static bool ManualStatsEnabled()
{
  if (Glob.demo) return false;
  if (GWorld->GetMode() == GModeNetware) return false;
  if (GetMissionParameters().GetLength() > 0) return false;
  if (GWorld->IsEndModeCheated()) return false;
  if (IsCampaignReplay()) return false;
  if (IsCampaign())
    return ExtParsCampaign.FindEntry("stats");
  else
    return ExtParsMission.FindEntry("statsColumn");
}

bool DisplayDebriefing::CanDestroy()
{
  if (!Display::CanDestroy()) return false;

  // SP statistics are not possible on Xbox 360
#if _XBOX_SECURE && _XBOX_VER < 200 && _ENABLE_MP
  if (_exiting) return true;
  if (_statsUpdate) return true;
  if (!ManualStatsEnabled()) return true;

  RString account;
  ParamFile cfg;
  if (ParseUserParams(cfg))
  {
    ConstParamEntryPtr entry = cfg.FindEntry("userAccount");
    if (entry) account = *entry;
  }
  if (account.GetLength() == 0) return true;

  _wantedExit = _exit;

  MsgBoxButton buttonOK(LocalizeString(IDS_XBOX_UPLOAD_NOW), INPUT_DEVICE_XINPUT + XBOX_Y);
  MsgBoxButton buttonCancel(LocalizeString(IDS_DISP_CONTINUE), INPUT_DEVICE_XINPUT + XBOX_A);
  CreateMsgBox(
    MB_BUTTON_OK | MB_BUTTON_CANCEL,
    LocalizeString(IDS_XBOX_MSG_STATS_NOT_UPLOADED),
    IDD_MSG_STATS_NOT_UPLOADED, false, &buttonOK, &buttonCancel);

  return false;
#else
  return true;
#endif
}

Control *DisplayDebriefing::OnCreateCtrl(int type, int idc, ParamEntryPar cls)
{
  Control *ctrl = Display::OnCreateCtrl(type, idc, cls);
  switch (idc)
  {
  case IDC_DEBRIEFING_RESULT:
    _result = GetTextContainer(ctrl);
    break;
  case IDC_DEBRIEFING_TITLE:
    _title = GetTextContainer(ctrl);
    break;
  case IDC_DEBRIEFING_DEBRIEFING:
    _debriefing = GetHTMLContainer(ctrl);
    ctrl->EnableCtrl(false);
    break;
  case IDC_DEBRIEFING_OBJECTIVES:
    _overview = GetHTMLContainer(ctrl);
    break;
  case IDC_DEBRIEFING_INFO:
    _info = GetHTMLContainer(ctrl);
    ctrl->EnableCtrl(false);
    break;
  case IDC_DEBRIEFING_STAT:
    _stats = GetHTMLContainer(ctrl);
    break;
  case 1001:
    ctrl->ShowCtrl(false);
    break;
  case IDC_DEBRIEFING_STATS_GROUP:
    ctrl->ShowCtrl(false);
    break;
  }
  return ctrl;
}

/*!
\patch 1.28 Date 10/22/2001 by Jirka
- Fixed: Debriefing for gamemaster of dedicated server disappear with no user action.
*/

bool AutoCancelDebriefing()
{
  NetworkServerState state = GetNetworkManager().GetServerState();
  return state != NSSPlaying && state != NSSDebriefing && state != NSSMissionAborted;
}

void DisplayDebriefing::OnButtonClicked(int idc)
{
  switch (idc)
  {
    // by default continue
  case IDC_OK:
  case IDC_CANCEL:
    if (GetNetworkManager().IsServer() || GetNetworkManager().IsGameMaster())
      GetNetworkManager().SetClientState(NCSDebriefingRead);
    Exit(idc);
    break;
  case IDC_AUTOCANCEL:
    Exit(IDC_AUTOCANCEL);
    break;
  case IDC_DEBRIEFING_RESTART:
    if (_restartEnabled) Exit(IDC_DEBRIEFING_RESTART);
    break;
  case IDC_DEBRIEFING_SHOW_PLAYERS:
    if (_server || _client)
    {
#if _GAMES_FOR_WINDOWS || (defined _XBOX && _XBOX_VER >= 200)
      {
        int userIndex = GSaveSystem.GetUserIndex();
        if (userIndex >= 0) XShowPlayersUI(userIndex);
      }
#elif defined _XBOX
      CreateChild(new DisplayXPlayers(this, AutoCancelDebriefing));
#else
      CreateChild(new DisplayMPPlayers(this));
#endif
    }
    break;
  case IDC_DEBRIEFING_SHOW_PAGE2:
    if (GetCtrl(IDC_DEBRIEFING_PAD2) && _objectives)
    {
      bool show = !GetCtrl(IDC_DEBRIEFING_PAD2)->IsVisible();
      GetCtrl(IDC_DEBRIEFING_PAD2)->ShowCtrl(show);
      UpdateButtons();
    }
    break;
  case IDC_DEBRIEFING_LIVE_STATS:
    // SP statistics are not possible on Xbox 360
#if _XBOX_SECURE && _XBOX_VER < 200 && _ENABLE_MP
    if (_statsUpdate) break;
    if (!ManualStatsEnabled()) break;
    SignIn();
#endif
    break;
  case IDC_DEBRIEFING_PREV_PAGE:
    if (GetCtrl(IDC_DEBRIEFING_PAD2) && GetCtrl(IDC_DEBRIEFING_PAD2)->IsVisible()) break;
    if (_stats)
    {
      int i = _stats->CurrentSection();
      if (i > 0)
      {
        _stats->SwitchSectionRaw(i - 1);
        UpdateButtons();
      }
    }
    break;
  case IDC_DEBRIEFING_NEXT_PAGE:
    if (GetCtrl(IDC_DEBRIEFING_PAD2) && GetCtrl(IDC_DEBRIEFING_PAD2)->IsVisible()) break;
    if (_stats)
    {
      int i = _stats->CurrentSection();
      int n = _stats->NSections();
      if (i < n - 1)
      {
        _stats->SwitchSectionRaw(i + 1);
        UpdateButtons();
      }
    }
    break;
  default:
    Display::OnButtonClicked(idc);
    break;
  }
}

void DisplayDebriefing::OnHTMLLink(int idc, RString link)
{
  if (idc == IDC_DEBRIEFING_OBJECTIVES)
  {
    if (stricmp(link, "stat:open") == 0)
    {
      if (GetCtrl(IDC_DEBRIEFING_PAD2)) GetCtrl(IDC_DEBRIEFING_PAD2)->ShowCtrl(true);
      if (GetCtrl(IDC_DEBRIEFING_STATS_GROUP)) GetCtrl(IDC_DEBRIEFING_STATS_GROUP)->ShowCtrl(true);
      if (GetCtrl(IDC_DEBRIEFING_OBJECTIVES_GROUP)) GetCtrl(IDC_DEBRIEFING_OBJECTIVES_GROUP)->ShowCtrl(false);
      UpdateButtons();
      return;
    }
  }
  else if (idc == IDC_DEBRIEFING_STAT)
  {
    if (stricmp(link, "stat:close") == 0)
    {
      if (GetCtrl(IDC_DEBRIEFING_PAD2)) GetCtrl(IDC_DEBRIEFING_PAD2)->ShowCtrl(false);
      if (GetCtrl(IDC_DEBRIEFING_STATS_GROUP)) GetCtrl(IDC_DEBRIEFING_STATS_GROUP)->ShowCtrl(false);
      if (GetCtrl(IDC_DEBRIEFING_OBJECTIVES_GROUP)) GetCtrl(IDC_DEBRIEFING_OBJECTIVES_GROUP)->ShowCtrl(true);
      UpdateButtons();
      return;
    }
  }

  Display::OnHTMLLink(idc, link);
}

bool DisplayDebriefing::OnKeyUp(int dikCode)
{
  // avoid processing of space by HTML container
  if (dikCode == DIK_SPACE) OnButtonClicked(IDC_OK);
  return Display::OnKeyUp(dikCode);
}

bool DisplayDebriefing::OnKeyDown(int dikCode)
{
  // avoid processing of space by HTML container
  if (dikCode == DIK_SPACE) return true;
  return Display::OnKeyDown(dikCode);
}

void DisplayDebriefing::UpdateButtons()
{
  IControl *ctrl = GetCtrl(IDC_DEBRIEFING_SHOW_PAGE2);
  if (ctrl)
  {
    IControl *pad = GetCtrl(IDC_DEBRIEFING_PAD2);
    if (pad && _objectives)
    {
      ctrl->ShowCtrl(true);
      ITextContainer *text = GetTextContainer(ctrl);
      if (text) text->SetText(LocalizeString(pad->IsVisible() ? IDS_DISP_XBOX_HINT_OBJECTIVES_HIDE : IDS_DISP_XBOX_HINT_OBJECTIVES));
    }
    else ctrl->ShowCtrl(false);
  }
  ctrl = GetCtrl(IDC_DEBRIEFING_LIVE_STATS);
  if (ctrl)
  {
    bool enable = false;
    // TODOXNET: SP statistics
#if _XBOX_SECURE && _XBOX_VER < 200 && _ENABLE_MP
    if (!_statsUpdate && ManualStatsEnabled()) enable = true;
#endif
    ctrl->ShowCtrl(enable);
  }
  ctrl = GetCtrl(IDC_DEBRIEFING_PREV_PAGE);
  if (ctrl)
  {
    bool enable = false;
    IControl *pad = GetCtrl(IDC_DEBRIEFING_PAD2);
    if (!pad || !pad->IsVisible())
    {
      if (_stats)
      {
        int i = _stats->CurrentSection();
        enable = i > 0;
      }
    }
    ctrl->ShowCtrl(enable);
  }
  ctrl = GetCtrl(IDC_DEBRIEFING_NEXT_PAGE);
  if (ctrl)
  {
    bool enable = false;
    IControl *pad = GetCtrl(IDC_DEBRIEFING_PAD2);
    if (!pad || !pad->IsVisible())
    {
      if (_stats)
      {
        int i = _stats->CurrentSection();
        int n = _stats->NSections();
        enable = i < n - 1;
      }
    }
    ctrl->ShowCtrl(enable);
  }
}

void DisplayDebriefing::OnSimulate(EntityAI *vehicle)
{
  if (_server)
  {
    if (!GetNetworkManager().IsServer() && !GetNetworkManager().IsGameMaster())
    {
      _server = false;
      if (GetCtrl(IDC_DEBRIEFING_RESTART)) GetCtrl(IDC_DEBRIEFING_RESTART)->ShowCtrl(true);
      ITextContainer *ctrl = GetTextContainer(GetCtrl(IDC_CANCEL));
      if (ctrl) ctrl->SetText(LocalizeString(IDS_DISP_DISCONNECT));
    }
    else
    {
      if (GetNetworkManager().GetCurrentSaveType()>=0)
      { // Load game
        Exit(IDC_OK);
      }
    }
  }
  else
  {
    if (GetNetworkManager().IsServer() || GetNetworkManager().IsGameMaster())
    {
      _server = true;
      if (GetCtrl(IDC_DEBRIEFING_RESTART)) GetCtrl(IDC_DEBRIEFING_RESTART)->ShowCtrl(false);
      ITextContainer *ctrl = GetTextContainer(GetCtrl(IDC_CANCEL));
      if (ctrl) ctrl->SetText(LocalizeString(IDS_DISP_CONTINUE));
    }
  }

  // update player list
  CListBox *lbox = dynamic_cast<CListBox *>(GetCtrl(IDC_DEBRIEFING_PLAYERS));
  if (lbox)
  {
    lbox->SetReadOnly();
    int sel = lbox->GetCurSel(); saturateMax(sel, 0);
    //    lbox->ShowSelected(false);
    lbox->ClearStrings();

    for (int i=0; i<GetNetworkManager().NPlayerRoles(); i++)
    {
      int dpnid = GetNetworkManager().GetPlayerRole(i)->player;
      if (dpnid == AI_PLAYER) continue;
      const PlayerIdentity *identity = GetNetworkManager().FindIdentity(dpnid);
      if (!identity) continue;
      int index = lbox->AddString(identity->name);
      switch (identity->clientState)
      {
      case NCSNone:
      case NCSCreated:
      case NCSConnected:
      case NCSLoggedIn:
      case NCSMissionSelected:
      case NCSMissionAsked:
      case NCSRoleAssigned:
      case NCSDebriefingRead:
      default:
        lbox->SetFtColor(index, PackedColor(Color(0, 1, 0, 1)));
        lbox->SetSelColor(index, PackedColor(Color(0, 1, 0, 1)));
        lbox->SetValue(index, 2);
        break;
      case NCSMissionReceived:
      case NCSGameLoaded:
      case NCSBriefingShown:
      case NCSBriefingRead:
        lbox->SetFtColor(index, PackedColor(Color(1, 0, 0, 1)));
        lbox->SetSelColor(index, PackedColor(Color(1, 0, 0, 1)));
        lbox->SetValue(index, 0);
        break;
      case NCSGameFinished:
        lbox->SetFtColor(index, PackedColor(Color(1, 1, 0, 1)));
        lbox->SetSelColor(index, PackedColor(Color(1, 1, 0, 1)));
        lbox->SetValue(index, 1);
        break;
      }
    }
    lbox->SortItemsByValue();
    lbox->SetCurSel(sel, false);
  }

  // SP statistics are not possible on Xbox 360
#if _XBOX_SECURE && _XBOX_VER < 200 && _ENABLE_MP
  if (_statsUpdate) _statsUpdate->Process();
#endif

  Display::OnSimulate(vehicle);
}

#pragma warning(disable:4060)

void DisplayDebriefing::OnChildDestroyed(int idd, int exit)
{
  switch (idd)
  {
    // SP statistics are not possible on Xbox 360
#if _XBOX_SECURE && _XBOX_VER < 200 && _ENABLE_MP
case IDD_MSG_SIGN_OUT:
# ifdef _XBOX
  if (exit == IDC_OK)
  {
    GetNetworkManager().SignOff();
  }
# endif
  if (_exiting) Exit(_wantedExit);
  break;
case IDD_PROFILE_LIVE:
  if (exit == IDC_OK)
  {
    Display::OnChildDestroyed(idd, exit);
    UpdateSPStats(true);
    return;
  }
  break;
case IDD_LIVE_STATS:
case IDD_LIVE_STATS_BOARD:
  GetNetworkManager().Done();
# ifdef _XBOX
  {
    HRESULT hr = GetNetworkManager().SilentSignInResult();
    if (hr == S_FALSE || !SUCCEEDED(hr))
    {
      Display::OnChildDestroyed(idd, exit);

      MsgBoxButton buttonOK(LocalizeString(IDS_DISP_XBOX_HINT_YES), INPUT_DEVICE_XINPUT + XBOX_A);
      MsgBoxButton buttonCancel(LocalizeString(IDS_DISP_XBOX_HINT_NO), INPUT_DEVICE_XINPUT + XBOX_B);
      CreateMsgBox(
        MB_BUTTON_OK | MB_BUTTON_CANCEL,
        LocalizeString(IDS_MSG_SIGN_OUT),
        IDD_MSG_SIGN_OUT, false, &buttonOK, &buttonCancel);
      return;
    }
  }
# endif
  if (_exiting) Exit(_wantedExit);
  break;
case IDD_MSG_STATS_NOT_UPLOADED:
  Display::OnChildDestroyed(idd, exit);
# if _XBOX_SECURE
  _exiting = true;
  if (exit == IDC_OK) SignIn();
  else Exit(_wantedExit);
# endif
  return;
#endif
#if _GAMES_FOR_WINDOWS || (defined _XBOX && _XBOX_VER >= 200)
case IDD_MSG_ACCEPT_INVITATION:
  Display::OnChildDestroyed(idd, exit);
  if (exit == IDC_OK)
  {
    Exit(IDC_CANCEL);
  }
  else
  {
    // Invitation rejected
    GSaveSystem.InvitationHandled();
  }
  return;    
#endif
  }
  Display::OnChildDestroyed(idd, exit);
}

#if _GAMES_FOR_WINDOWS || (defined _XBOX && _XBOX_VER >= 200)

void DisplayDebriefing::OnInvited()
{
  if (_msgBox) return; // confirmation in progress

  if (GSaveSystem.IsInvitationConfirmed() || GetNetworkManager().GetClientState() == NCSNone)
  {
    // no warning is needed in the single player
    Exit(IDC_OK);
  }
  else
  {
    GSaveSystem.ConfirmInvitation();
    // let the user confirm the action because of possible progress lost
    MsgBoxButton buttonOK(LocalizeString(IDS_DISP_XBOX_HINT_YES), INPUT_DEVICE_XINPUT + XBOX_A);
    MsgBoxButton buttonCancel(LocalizeString(IDS_DISP_XBOX_HINT_NO), INPUT_DEVICE_XINPUT + XBOX_B);
    RString message;
    if (GetNetworkManager().IsServer())
      message = LocalizeString(IDS_MSG_CONFIRM_TERMINATE_SESSION);
    else
      message = LocalizeString(IDS_MSG_CONFIRM_TERMINATE_SESSION_CLIENT);
    // special type of message box which will not close because of invitation
    _msgBox = new MsgBoxIgnoreInvitation(this, MB_BUTTON_OK | MB_BUTTON_CANCEL, message, IDD_MSG_ACCEPT_INVITATION, false, &buttonOK, 

      &buttonCancel);
  }
}

#endif

#if _XBOX_SECURE && _XBOX_VER < 200 && _ENABLE_MP

void DisplayDebriefing::UpdateSPStats(bool tempSignIn)
{
  // update statistics
  _statsUpdate = new LiveStatsUpdate();
  _statsUpdate->Close(); // update immediatelly
  RString name = _statsUpdate->GetName();
  UpdateButtons();
  // show statistics
  if (name.GetLength() == 0)
    CreateChild(new DisplayLiveStats(this));
  else
    CreateChild(new DisplayLiveStatsBoard(this, name));
}

void DisplayDebriefing::SignIn()
{
  if (GetNetworkManager().IsSignedIn())
  {
    UpdateSPStats(false);
    return;
  }

  RString account;
  ParamFile cfg;
  if (ParseUserParams(cfg))
  {
    ConstParamEntryPtr entry = cfg.FindEntry("userAccount");
    if (entry) account = *entry;
  }

  GetNetworkManager().Init();
  CreateChild(new DisplayProfileLive(this, true, account));
}

#endif //  _XBOX_SECURE && _XBOX_VER < 200 && _ENABLE_MP

struct KillsInfo
{
  Ref<const EntityAIType> type;
  RString playerName;
  int n;
};
TypeIsMovableZeroed(KillsInfo)

struct CasualtiesInfo
{
  bool player;
  RString killedName;
  RString killerName;
  int n;
};
TypeIsMovableZeroed(CasualtiesInfo)

static int CmpKillsInfo(const KillsInfo *info1, const KillsInfo *info2)
{
  if (info2->playerName.GetLength() > 0)
    if (info1->playerName.GetLength() > 0) return info2->n - info1->n;
    else return 1;
  else if (info1->playerName.GetLength() > 0) return -1;
  else return (int)(info2->type->_cost - info1->type->_cost);
}

static int CmpCasualtiesInfo(const CasualtiesInfo *info1, const CasualtiesInfo *info2)
{
  if (info2->player)
    if (info1->player) return info2->n - info1->n;
    else return 1;
  else if (info1->player) return -1;
  else return strcmp(info1->killedName, info2->killedName);
}

#if _ENABLE_CHEATS
#define LOG_DEBRIEFING 1
#else
#define LOG_DEBRIEFING 0
#endif

/*!
\patch 5088 Date 11/15/2006 by Jirka
- Fixed: Debriefing - mission duration info was wrong in campaign
\patch 5151 Date 3/30/2007 by Jirka
- Fixed: Debriefing - in campaign, objectives was not properly marked
*/


void DisplayDebriefing::CreateDebriefing()
{
  LogF("Creating debriefing");

  // prepare statistics 
  _oldStats = GStats;
  GStats.Update();

  // FIX: EndMission resets Glob.clock
  float currentTime = Glob.clock.GetTimeInYear();

  AIUnitHeader &oldInfo = _oldStats._campaign._playerInfo;
  AIUnitHeader &newInfo = GStats._campaign._playerInfo;
  RString playerName = GetLocalPlayerName();
  if (newInfo.unit) playerName = newInfo.unit->GetPerson()->GetPersonName();

  // load the briefing
  RString briefing = GetBriefingFile();
  if (briefing.GetLength() > 0)
  {
    if (_debriefing) _debriefing->Load(briefing);
    if (_overview) _overview->Load(briefing);
  }
#if _VBS3 && _EXT_CTRL
  if(GAAR.IsReplayMode())
  {
    // disabled until parsing is fixed
    /*  if (_debriefing)
    _debriefing->Parse(GAAR.GetBriefingData());

    if (_overview) 
    _overview->Parse(GAAR.GetBriefingData());
    */
  }
#endif

#if LOG_DEBRIEFING
  bool doOutput = false;
  const char *end = "";
  switch (GWorld->GetEndMode())
  {
  case EMLoser:
    doOutput = true;
    end = "LOST";
    break;
  case EMEnd1:
    doOutput = true;
    end = "END 1";
    break;
  case EMEnd2:
    doOutput = true;
    end = "END 2";
    break;
  case EMEnd3:
    doOutput = true;
    end = "END 3";
    break;
  case EMEnd4:
    doOutput = true;
    end = "END 4";
    break;
  case EMEnd5:
    doOutput = true;
    end = "END 5";
    break;
  case EMEnd6:
    doOutput = true;
    end = "END 6";
    break;
  }

  FILE *file = NULL;
  if (doOutput)
  {
    char logFilename[MAX_PATH];
    if (GetLocalSettingsDir(logFilename))
    {
      CreateDirectory(logFilename, NULL);
      TerminateBy(logFilename, '\\');
      strcat(logFilename, "rating.log");

      file = fopen(logFilename, "a+");
    }
  }
#endif

  // mission name
  if (_title)
  {
    RString text;
    if (GWorld->GetMode() == GModeNetware)
    {
      const MissionHeader *header = GetNetworkManager().GetMissionHeader();
      if (header) text = header->GetLocalizedMissionName();
    }
    if (text.GetLength() == 0)
    {
      text = Localize(CurrentTemplate.intel.briefingName);
      if (text.GetLength() == 0)
      {
        text = Glob.header.filenameReal;
        // user mission file name is encoded
        if (IsUserMission()) text = DecodeFileName(text);
      }
    }
    _title->SetText(text);
  }

#if LOG_DEBRIEFING
  if (file)
  {
    fprintf(file, "************************************************************\n");
    fprintf(file, "User: %s\n", (const char *)Glob.header.GetPlayerName());
    fprintf(file, "Mission: %s (%s)\n", (const char *)Glob.header.filename, end);
    time_t t;
    time(&t);
    fprintf(file, "Time: %s\n", ctime(&t));
  }
#endif

  if (_info)
  {
    int section = _info->AddSection();
    _info->AddName(section, "__INFO");

    // rank, name of soldier
    RString text = LocalizeString(IDS_PRIVATE + newInfo.rank) + RString(" ") + playerName;
    _info->AddText(section, text, HFP, HALeft, false, false, "");
    _info->AddBreak(section, false);

    // mission duration
    int day = CurrentTemplate.intel.day - 1;
    int year = CurrentTemplate.intel.year;
    for (int m=0; m<CurrentTemplate.intel.month-1; m++) day += GetDaysInMonth(year, m);
    float time = CurrentTemplate.intel.hour * OneHour + CurrentTemplate.intel.minute * OneMinute + day * OneDay + 0.5 * OneSecond;
    float dt = floatMax(currentTime - time, 0);
    dt *= 365 * 24; int hours = toIntFloor(dt); dt -= hours;
    dt *= 60; int minutes = toInt(dt); dt -= minutes;
    if (hours > 0)
      text = Format(LocalizeString(IDS_BRIEF_DURATION_LONG), hours, minutes);
    else
      text = Format(LocalizeString(IDS_BRIEF_DURATION_SHORT), minutes);
    _info->AddText(section, text, HFP, HALeft, false, false, "");
    _info->AddBreak(section, false);
#if LOG_DEBRIEFING
    if (file) fprintf(file, "%s\n", cc_cast(text));
#endif

    if (!_client && !_server) // single player
    {
      // score
      float score = newInfo.experience - oldInfo.experience;
      text = Format(LocalizeString(IDS_BRIEF_SCORE), score);
      _info->AddText(section, text, HFP, HALeft, false, false, "");
      int points = (int)(GStats._campaign._score - _oldStats._campaign._score);
      if (points >= 0)
      {
        RString image = Glob.config.GetScoreImage();
#if LOG_DEBRIEFING
        RString sign = Glob.config.GetScoreChar();
#endif
        for (int i=0; i<points + 1; i++)
        {
          _info->AddImage(section, image, HALeft, false, 16, 16, "");
#if LOG_DEBRIEFING
          text = text + sign;
#endif
        }
      }
      else
      {
        RString image = Glob.config.GetBadScoreImage();
#if LOG_DEBRIEFING
        RString sign = Glob.config.GetBadScoreChar();
#endif
        for (int i=0; i<-points; i++)
        {
          _info->AddImage(section, image, HALeft, false, 16, 16, "");
#if LOG_DEBRIEFING
          text = text + sign;
#endif
        }
      }
      _info->AddBreak(section, false);

#if LOG_DEBRIEFING
      if (file)
      {
        fprintf(file, "%s\n", text);
        fprintf(file, "Total score in campaign: %d\n", GStats._campaign._score);
      }
#endif
    }

    _info->FormatSection(section);
    _info->SwitchSection("__INFO");
  }

  // mission end
  if (_debriefing)
  {
    int section = _debriefing->AddSection();
    _debriefing->AddName(section, "__DEBRIEFING");

    int src = -1;
    switch (GWorld->GetEndMode())
    {
    case EMLoser:
      src = _debriefing->FindSection("Debriefing:Loser");
      break;
    case EMEnd1:
      src = _debriefing->FindSection("Debriefing:End1");
      break;
    case EMEnd2:
      src = _debriefing->FindSection("Debriefing:End2");
      break;
    case EMEnd3:
      src = _debriefing->FindSection("Debriefing:End3");
      break;
    case EMEnd4:
      src = _debriefing->FindSection("Debriefing:End4");
      break;
    case EMEnd5:
      src = _debriefing->FindSection("Debriefing:End5");
      break;
    case EMEnd6:
      src = _debriefing->FindSection("Debriefing:End6");
      break;
    }
    if (src >= 0) _debriefing->CopySection(src, section);

    _debriefing->FormatSection(section);
    _debriefing->SwitchSection("__DEBRIEFING");
  }

  // objectives
  _objectives = false;
  if (_overview)
  {
    int lSection = _overview->AddSection();
    _overview->AddName(lSection, "__OBJECTIVES");

    _overview->AddText(lSection, LocalizeString(IDS_BRIEF_OBJECTIVES), HFH1, HACenter, false, false, "");
    _overview->AddBreak(lSection, false);
    TargetSide side = TargetSide(Glob.header.playerSide);
    for (int s=0; s<_overview->NSections(); s++)
    {
      const HTMLSection &src = _overview->GetSection(s);
      for (int n=0; n<src.names.Size(); n++)
      {
        RString name = src.names[n];
        static const char *prefix = "OBJ_";
        static const char *prefixWest = "OBJ_WEST_";
        static const char *prefixEast = "OBJ_EAST_";
        static const char *prefixGuerrila = "OBJ_GUER_";
        static const char *prefixCivilian = "OBJ_CIVIL_";
        static const char *prefixHidden = "OBJ_HIDDEN_";
        static const char *prefixWestHidden = "OBJ_WEST_HIDDEN_";
        static const char *prefixEastHidden = "OBJ_EAST_HIDDEN_";
        static const char *prefixGuerrilaHidden = "OBJ_GUER_HIDDEN_";
        static const char *prefixCivilianHidden = "OBJ_CIVIL_HIDDEN_";

        if (strnicmp(name, prefix, strlen(prefix)) == 0)
        {
          if (strnicmp(name, prefixWest, strlen(prefixWest)) == 0)
          {
            if (side != TWest) continue;
          }
          else if (strnicmp(name, prefixEast, strlen(prefixEast)) == 0)
          {
            if (side != TEast) continue;
          }
          else if (strnicmp(name, prefixGuerrila, strlen(prefixGuerrila)) == 0)
          {
            if (side != TGuerrila) continue;
          }
          else if (strnicmp(name, prefixCivilian, strlen(prefixCivilian)) == 0)
          {
            if (side != TCivilian) continue;
          }
          else if (strnicmp(name, prefixHidden, strlen(prefixHidden)) == 0)
          {
            name = prefix + name.Substring(strlen(prefixHidden), INT_MAX);
          }
          else if (strnicmp(name, prefixWestHidden, strlen(prefixWestHidden)) == 0)
          {
            if (side != TWest) continue;
            name = prefixWest + name.Substring(strlen(prefixWestHidden), INT_MAX);
          }
          else if (strnicmp(name, prefixEastHidden, strlen(prefixEastHidden)) == 0)
          {
            if (side != TEast) continue;
            name = prefixEast + name.Substring(strlen(prefixEastHidden), INT_MAX);
          }
          else if (strnicmp(name, prefixGuerrilaHidden, strlen(prefixGuerrilaHidden)) == 0)
          {
            if (side != TGuerrila) continue;
            name = prefixGuerrila + name.Substring(strlen(prefixGuerrilaHidden), INT_MAX);
          }
          else if (strnicmp(name, prefixCivilianHidden, strlen(prefixCivilianHidden)) == 0)
          {
            if (side != TCivilian) continue;
            name = prefixCivilian + name.Substring(strlen(prefixCivilianHidden), INT_MAX);
          }
          GameState *state = GWorld->GetGameState();
          int value = toInt((float)state->Evaluate(name, GameState::EvalContext::_default, GWorld->GetMissionNamespace()));
          AddObjective(_overview, s, lSection, value);
          if (value != OSHidden) _objectives = true;
          break; // next section
        }
      }
    }
    if (newInfo.unit)
    {
      AddTasks(_overview, lSection, newInfo.unit->GetPerson());
      _overview->AddBreak(lSection, false);
    }

    if (_statisticsLinks)
    {
      _overview->AddText(lSection, LocalizeString(IDS_BRIEF_STAT_OPEN), HFP, HALeft, false, false, "Stat:open");
      _overview->AddBreak(lSection, false);
    }

    _overview->FormatSection(lSection);
    _overview->SwitchSection("__OBJECTIVES");
  }

  // statistics
  if (_stats)
  {
    int sSection = _stats->AddSection();
    _stats->AddName(sSection, "__STATISTICS");

    _stats->AddText(sSection, LocalizeString(IDS_BRIEF_STATISTICS), HFH1, HACenter, false, false, "");
    _stats->AddBreak(sSection, false);

    char buffer[256];

    AutoArray<AIStatsEvent> &events = _oldStats._mission._events;

    AUTO_STATIC_ARRAY(KillsInfo, kills, 32);
    // kills - enemies
    for (int i=0; i<events.Size(); i++)
    {
      AIStatsEvent &event = events[i];
      switch (event.type)
      {
      case SETKillsEnemyInfantry:
      case SETKillsEnemySoft:
      case SETKillsEnemyArmor:
      case SETKillsEnemyAir:
        {
          RString playerName = event.killedPlayer ? event.killedName : "";
          for (int j=0; j<kills.Size(); j++)
          {
            if (kills[j].type->GetName() == event.killedType && kills[j].playerName == playerName)
            {
              kills[j].n++;
              goto ExitSwitchEnemies;
            }
          }
          {
            int index = kills.Add();
            Ref<EntityType> eType = VehicleTypes.New(event.killedType);
            kills[index].type = dynamic_cast<EntityAIType *>(eType.GetRef());
            if (!kills[index].type) kills[index].type = GWorld->Preloaded(VTypeAllVehicles);
            kills[index].playerName = playerName;
            kills[index].n = 1;
          }
        }
ExitSwitchEnemies:
        break;
      }
    }
    if (kills.Size() > 0)
    {
      _stats->AddText(sSection, LocalizeString(IDS_BRIEF_YOURKILLS), HFH3, HALeft, false, false, "");
      _stats->AddBreak(sSection, false);
#if LOG_DEBRIEFING
      if (file) fprintf(file, "\nYour kills:\n");
#endif

      QSort(kills.Data(), kills.Size(), CmpKillsInfo);
      for (int j=0; j<kills.Size(); j++)
      {
        KillsInfo &info = kills[j];
        RString killed = info.playerName;
        if (killed.GetLength() == 0) killed = info.type->GetDisplayName();
        if (info.n > 1)
          sprintf(buffer, LocalizeString(IDS_BRIEF_FORMAT_GENERIC_TIMES), info.n, (const char *)killed);
        else
          strcpy(buffer, killed);
        _stats->AddText(sSection, buffer, HFP, HALeft, false, false, "");
        _stats->AddBreak(sSection, false);
#if LOG_DEBRIEFING
        if (file) fprintf(file, "%s\n", buffer);
#endif
      }
      _stats->AddBreak(sSection, false);
    }

    // kills - friends
    kills.Resize(0);
    for (int i=0; i<events.Size(); i++)
    {
      AIStatsEvent &event = events[i];
      switch (event.type)
      {
      case SETKillsFriendlyInfantry:
      case SETKillsFriendlySoft:
      case SETKillsFriendlyArmor:
      case SETKillsFriendlyAir:
        //    case SETUnitLost:
        {
          RString playerName = event.killedPlayer ? event.killedName : "";
          for (int j=0; j<kills.Size(); j++)
          {
            if (kills[j].type->GetName() == event.killedType && kills[j].playerName == playerName)
            {
              kills[j].n++;
              goto ExitSwitchFriends;
            }
          }
          {
            int index = kills.Add();
            Ref<EntityType> eType = VehicleTypes.New(event.killedType);
            kills[index].type = dynamic_cast<EntityAIType *>(eType.GetRef());
            if (!kills[index].type) kills[index].type = GWorld->Preloaded(VTypeAllVehicles);
            kills[index].playerName = playerName;
            kills[index].n = 1;
          }
        }
ExitSwitchFriends:
        break;
      }
    }
    if (kills.Size() > 0)
    {
      _stats->AddText(sSection, LocalizeString(IDS_BRIEF_YOURKILLS_FRIENDLY), HFP, HALeft, false, false, "");
      _stats->AddBreak(sSection, false);
#if LOG_DEBRIEFING
      if (file) fprintf(file, "\nYour kills - friendly units:\n");
#endif

      QSort(kills.Data(), kills.Size(), CmpKillsInfo);
      for (int j=0; j<kills.Size(); j++)
      {
        KillsInfo &info = kills[j];
        RString killed = info.playerName;
        if (killed.GetLength() == 0) killed = info.type->GetDisplayName();
        if (info.n > 1)
          sprintf(buffer, LocalizeString(IDS_BRIEF_FORMAT_GENERIC_TIMES), info.n, (const char *)killed);
        else
          strcpy(buffer, killed);
        _stats->AddText(sSection, buffer, HFP, HALeft, false, false, "");
        _stats->AddBreak(sSection, false);
#if LOG_DEBRIEFING
        if (file) fprintf(file, "%s\n", buffer);
#endif
      }
      _stats->AddBreak(sSection, false);
    }

    // kills - civilians
    kills.Resize(0);
    for (int i=0; i<events.Size(); i++)
    {
      AIStatsEvent &event = events[i];
      switch (event.type)
      {
      case SETKillsCivilInfantry:
      case SETKillsCivilSoft:
      case SETKillsCivilArmor:
      case SETKillsCivilAir:
        {
          RString playerName = event.killedPlayer ? event.killedName : "";
          for (int j=0; j<kills.Size(); j++)
          {
            if (kills[j].type->GetName() == event.killedType && kills[j].playerName == playerName)
            {
              kills[j].n++;
              goto ExitSwitchCivil;
            }
          }
          {
            int index = kills.Add();
            Ref<EntityType> eType = VehicleTypes.New(event.killedType);
            kills[index].type = dynamic_cast<EntityAIType *>(eType.GetRef());
            if (!kills[index].type) kills[index].type = GWorld->Preloaded(VTypeAllVehicles);
            kills[index].playerName = playerName;
            kills[index].n = 1;
          }
        }
ExitSwitchCivil:
        break;
      }
    }
    if (kills.Size() > 0)
    {
      _stats->AddText(sSection, LocalizeString(IDS_BRIEF_YOURKILLS_CIVIL), HFP, HALeft, false, false, "");
      _stats->AddBreak(sSection, false);
#if LOG_DEBRIEFING
      if (file) fprintf(file, "\nYour kills - civilians:\n");
#endif

      QSort(kills.Data(), kills.Size(), CmpKillsInfo);
      for (int j=0; j<kills.Size(); j++)
      {
        KillsInfo &info = kills[j];
        RString killed = info.playerName;
        if (killed.GetLength() == 0) killed = info.type->GetDisplayName();
        if (info.n > 1)
          sprintf(buffer, LocalizeString(IDS_BRIEF_FORMAT_GENERIC_TIMES), info.n, (const char *)killed);
        else
          strcpy(buffer, killed);
        _stats->AddText(sSection, buffer, HFP, HALeft, false, false, "");
        _stats->AddBreak(sSection, false);
#if LOG_DEBRIEFING
        if (file) fprintf(file, "%s\n", buffer);
#endif
      }
      _stats->AddBreak(sSection, false);
    }

    AUTO_STATIC_ARRAY(CasualtiesInfo, casual, 32);
    int playerTotal = 0;
    for (int i=0; i<events.Size(); i++)
    {
      AIStatsEvent &event = events[i];
      if (event.type != SETUnitLost) continue;

      RString killerName;
      if (event.killedPlayer)
      {
        playerTotal++;
        if (!event.killerPlayer) continue;
        killerName = event.killerName;
        //      playerName = event.killedName;
      }
      for (int j=0; j<casual.Size(); j++)
      {
        if (casual[j].killedName == event.killedName && casual[j].killerName == killerName)
        {
          casual[j].n++;
          goto ExitSwitch2;
        }
      }
      {
        int index = casual.Add();
        casual[index].player = event.killedPlayer;
        casual[index].killedName = event.killedName;
        casual[index].killerName = killerName;
        casual[index].n = 1;
      }
ExitSwitch2:
      ;
    }
    if (playerTotal > 0 || casual.Size() > 0)
    {
      _stats->AddText(sSection, LocalizeString(IDS_BRIEF_CASUALTIES), HFP, HALeft, false, false, "");
      _stats->AddBreak(sSection, false);

#if LOG_DEBRIEFING
      if (file) fprintf(file, "\nCasualties:\n");
#endif

      QSort(casual.Data(), casual.Size(), CmpCasualtiesInfo);
      if (playerTotal > 0)
      {
        if (playerTotal > 1)
          sprintf(buffer, LocalizeString(IDS_BRIEF_FORMAT_YOU_TIMES), playerTotal, (const char *)playerName);
        else
          sprintf(buffer, LocalizeString(IDS_BRIEF_FORMAT_YOU_ONCE), (const char *)playerName);
        _stats->AddText(sSection, buffer, HFP, HALeft, false, false, "");
        _stats->AddBreak(sSection, false);
#if LOG_DEBRIEFING
        if (file) fprintf(file, "%s\n", buffer);
#endif
      }
      for (int j=0; j<casual.Size(); j++)
      {
        CasualtiesInfo &info = casual[j];
        if (info.player)
        {
          Assert(info.killerName.GetLength() > 0);
          if (info.n > 1)
            sprintf(buffer, LocalizeString(IDS_BRIEF_FORMAT_BY_TIMES), info.n, (const char *)info.killerName);
          else
            sprintf(buffer, LocalizeString(IDS_BRIEF_FORMAT_BY_ONCE), (const char *)info.killerName);
        }
        else
        {
          if (info.n > 1)
            sprintf(buffer, LocalizeString(IDS_BRIEF_FORMAT_GENERIC_TIMES), info.n, (const char *)info.killedName);
          else
            strcpy(buffer, info.killedName);
        }
        _stats->AddText(sSection, buffer, HFP, HALeft, false, false, "");
        _stats->AddBreak(sSection, false);
#if LOG_DEBRIEFING
        if (file) fprintf(file, "%s\n", buffer);
#endif
      }
    }

    if (_statisticsLinks)
    {
      _stats->AddText(sSection, LocalizeString(IDS_BRIEF_STAT_CLOSE), HFP, HALeft, false, false, "Stat:close");
      _stats->AddBreak(sSection, false);
    }

    _stats->FormatSection(sSection);
    _stats->SwitchSection("__STATISTICS");
  }

#if LOG_DEBRIEFING
  if (file) fclose(file);
#endif

  // finish mission
  if (IsCampaign())
  {
    void EndMission(EndMode mode);

    EndMode missionEnd = GWorld->GetEndMode();
    EndMission(missionEnd);
  }
}

void DisplayDebriefing::CreateDebriefingFail()
{
  // prepare statistics 
  _oldStats = GStats;
  _client = false;
  _server = false;

  AIUnitHeader &newInfo = GStats._campaign._playerInfo;
  //  AIUnitHeader &oldInfo = _oldStats._campaign._playerInfo;
  RString playerName = GetLocalPlayerName();
  if (newInfo.unit) playerName = newInfo.unit->GetPerson()->GetPersonName();

  // load the briefing
  RString briefing = GetBriefingFile();
  if (briefing.GetLength() > 0)
  {
    if (_debriefing) _debriefing->Load(briefing);
    if (_overview) _overview->Load(briefing);
  }

  // FIX: EndMission resets Glob.clock
  float currentTime = Glob.clock.GetTimeInYear();

  // mission name
  if (_title)
  {
    RString text;
    if (GWorld->GetMode() == GModeNetware)
    {
      const MissionHeader *header = GetNetworkManager().GetMissionHeader();
      if (header) text = header->GetLocalizedMissionName();
    }
    if (text.GetLength() == 0)
    {
      text = Localize(CurrentTemplate.intel.briefingName);
      if (text.GetLength() == 0)
      {
        text = Glob.header.filenameReal;
        // user mission file name is encoded
        if (IsUserMission()) text = DecodeFileName(text);
      }
    }
    _title->SetText(text);
  }


  if (_info)
  {
    int section = _info->AddSection();
    _info->AddName(section, "__INFO");

    // rank, name of soldier
    RString text = LocalizeString(IDS_PRIVATE + newInfo.rank) + RString(" ") + playerName;
    _info->AddText(section, text, HFP, HALeft, false, false, "");
    _info->AddBreak(section, false);

    int GetDaysInMonth(int year, int month);
    // mission duration
    int day = CurrentTemplate.intel.day - 1;
    int year = CurrentTemplate.intel.year;
    for (int m=0; m<CurrentTemplate.intel.month-1; m++) day += GetDaysInMonth(year, m);
    float time = CurrentTemplate.intel.hour * OneHour + CurrentTemplate.intel.minute * OneMinute + day * OneDay + 0.5 * OneSecond;
    float dt = floatMax(currentTime - time, 0);
    dt *= 365 * 24; int hours = toIntFloor(dt); dt -= hours;
    dt *= 60; int minutes = toInt(dt); dt -= minutes;
    if (hours > 0)
      text = Format(LocalizeString(IDS_BRIEF_DURATION_LONG), hours, minutes);
    else
      text = Format(LocalizeString(IDS_BRIEF_DURATION_SHORT), minutes);
    _info->AddText(section, text, HFP, HALeft, false, false, "");
    _info->AddBreak(section, false);

    if (!_client && !_server) // single player
    {
      // score 
      //float score = newInfo.experience - oldInfo.experience;;
      //text = Format(LocalizeString(IDS_BRIEF_SCORE), score);
      //_info->AddText(section, text, HFP, HALeft, false, false, "");
      //int points = (int)(GStats._campaign._score - _oldStats._campaign._score);
      //if (points >= 0)
      //{
      //  RString image = Glob.config.GetDifficultySettings().scoreImage;

      //  for (int i=0; i<points + 1; i++)
      //  {
      //    _info->AddImage(section, image, HALeft, false, 16, 16, "");

      //  }
      //}
      //else
      //{
      //  RString image = Glob.config.GetDifficultySettings().badScoreImage;

      //  for (int i=0; i<-points; i++)
      //  {
      //    _info->AddImage(section, image, HALeft, false, 16, 16, "");
      //  }
      //}
      _info->AddBreak(section, false);

    }

    _info->FormatSection(section);
    _info->SwitchSection("__INFO");
  }

  // mission end
  if (_debriefing)
  {
    int section = _debriefing->AddSection();
    _debriefing->AddName(section, "__DEBRIEFING");

    int src = -1;
    switch (GWorld->GetEndMode())
    {
    case EMLoser:
      src = _debriefing->FindSection("Debriefing:Loser");
      break;
    case EMEnd1:
      src = _debriefing->FindSection("Debriefing:End1");
      break;
    case EMEnd2:
      src = _debriefing->FindSection("Debriefing:End2");
      break;
    case EMEnd3:
      src = _debriefing->FindSection("Debriefing:End3");
      break;
    case EMEnd4:
      src = _debriefing->FindSection("Debriefing:End4");
      break;
    case EMEnd5:
      src = _debriefing->FindSection("Debriefing:End5");
      break;
    case EMEnd6:
      src = _debriefing->FindSection("Debriefing:End6");
      break;
    }
    if (src >= 0) _debriefing->CopySection(src, section);

    _debriefing->FormatSection(section);
    _debriefing->SwitchSection("__DEBRIEFING");
  }

  // objectives
  _objectives = false;
  if (_overview)
  {
    int lSection = _overview->AddSection();
    _overview->AddName(lSection, "__OBJECTIVES");

    _overview->AddText(lSection, LocalizeString(IDS_BRIEF_OBJECTIVES), HFH1, HACenter, false, false, "");
    _overview->AddBreak(lSection, false);
    TargetSide side = TargetSide(Glob.header.playerSide);
    for (int s=0; s<_overview->NSections(); s++)
    {
      const HTMLSection &src = _overview->GetSection(s);
      for (int n=0; n<src.names.Size(); n++)
      {
        RString name = src.names[n];
        static const char *prefix = "OBJ_";
        static const char *prefixWest = "OBJ_WEST_";
        static const char *prefixEast = "OBJ_EAST_";
        static const char *prefixGuerrila = "OBJ_GUER_";
        static const char *prefixCivilian = "OBJ_CIVIL_";
        static const char *prefixHidden = "OBJ_HIDDEN_";
        static const char *prefixWestHidden = "OBJ_WEST_HIDDEN_";
        static const char *prefixEastHidden = "OBJ_EAST_HIDDEN_";
        static const char *prefixGuerrilaHidden = "OBJ_GUER_HIDDEN_";
        static const char *prefixCivilianHidden = "OBJ_CIVIL_HIDDEN_";

        if (strnicmp(name, prefix, strlen(prefix)) == 0)
        {
          if (strnicmp(name, prefixWest, strlen(prefixWest)) == 0)
          {
            if (side != TWest) continue;
          }
          else if (strnicmp(name, prefixEast, strlen(prefixEast)) == 0)
          {
            if (side != TEast) continue;
          }
          else if (strnicmp(name, prefixGuerrila, strlen(prefixGuerrila)) == 0)
          {
            if (side != TGuerrila) continue;
          }
          else if (strnicmp(name, prefixCivilian, strlen(prefixCivilian)) == 0)
          {
            if (side != TCivilian) continue;
          }
          else if (strnicmp(name, prefixHidden, strlen(prefixHidden)) == 0)
          {
            name = prefix + name.Substring(strlen(prefixHidden), INT_MAX);
          }
          else if (strnicmp(name, prefixWestHidden, strlen(prefixWestHidden)) == 0)
          {
            if (side != TWest) continue;
            name = prefixWest + name.Substring(strlen(prefixWestHidden), INT_MAX);
          }
          else if (strnicmp(name, prefixEastHidden, strlen(prefixEastHidden)) == 0)
          {
            if (side != TEast) continue;
            name = prefixEast + name.Substring(strlen(prefixEastHidden), INT_MAX);
          }
          else if (strnicmp(name, prefixGuerrilaHidden, strlen(prefixGuerrilaHidden)) == 0)
          {
            if (side != TGuerrila) continue;
            name = prefixGuerrila + name.Substring(strlen(prefixGuerrilaHidden), INT_MAX);
          }
          else if (strnicmp(name, prefixCivilianHidden, strlen(prefixCivilianHidden)) == 0)
          {
            if (side != TCivilian) continue;
            name = prefixCivilian + name.Substring(strlen(prefixCivilianHidden), INT_MAX);
          }
          GameState *state = GWorld->GetGameState();
          int value = toInt((float)state->Evaluate(name, GameState::EvalContext::_default, GWorld->GetMissionNamespace()));
          AddObjective(_overview, s, lSection, value);
          if (value != OSHidden) _objectives = true;
          break; // next section
        }
      }
    }
    if(newInfo.unit && newInfo.unit->GetPerson())
      AddTasks(_overview, lSection, newInfo.unit->GetPerson());
    _overview->AddBreak(lSection, false);

    if (_statisticsLinks)
    {
      _overview->AddText(lSection, LocalizeString(IDS_BRIEF_STAT_OPEN), HFP, HALeft, false, false, "Stat:open");
      _overview->AddBreak(lSection, false);
    }

    _overview->FormatSection(lSection);
    _overview->SwitchSection("__OBJECTIVES");
  }

  // statistics
  if (_stats)
  {
    int sSection = _stats->AddSection();
    _stats->AddName(sSection, "__STATISTICS");

    _stats->AddText(sSection, LocalizeString(IDS_BRIEF_STATISTICS), HFH1, HACenter, false, false, "");
    _stats->AddBreak(sSection, false);

    char buffer[256];

    AutoArray<AIStatsEvent> &events = _oldStats._mission._events;

    AUTO_STATIC_ARRAY(KillsInfo, kills, 32);
    // kills - enemies
    for (int i=0; i<events.Size(); i++)
    {
      AIStatsEvent &event = events[i];
      switch (event.type)
      {
      case SETKillsEnemyInfantry:
      case SETKillsEnemySoft:
      case SETKillsEnemyArmor:
      case SETKillsEnemyAir:
        {
          RString playerName = event.killedPlayer ? event.killedName : "";
          for (int j=0; j<kills.Size(); j++)
          {
            if (kills[j].type->GetName() == event.killedType && kills[j].playerName == playerName)
            {
              kills[j].n++;
              goto ExitSwitchEnemies;
            }
          }
          {
            int index = kills.Add();
            Ref<EntityType> eType = VehicleTypes.New(event.killedType);
            kills[index].type = dynamic_cast<EntityAIType *>(eType.GetRef());
            if (!kills[index].type) kills[index].type = GWorld->Preloaded(VTypeAllVehicles);
            kills[index].playerName = playerName;
            kills[index].n = 1;
          }
        }
ExitSwitchEnemies:
        break;
      }
    }
    if (kills.Size() > 0)
    {
      _stats->AddText(sSection, LocalizeString(IDS_BRIEF_YOURKILLS), HFH3, HALeft, false, false, "");
      _stats->AddBreak(sSection, false);


      QSort(kills.Data(), kills.Size(), CmpKillsInfo);
      for (int j=0; j<kills.Size(); j++)
      {
        KillsInfo &info = kills[j];
        RString killed = info.playerName;
        if (killed.GetLength() == 0) killed = info.type->GetDisplayName();
        if (info.n > 1)
          sprintf(buffer, LocalizeString(IDS_BRIEF_FORMAT_GENERIC_TIMES), info.n, (const char *)killed);
        else
          strcpy(buffer, killed);
        _stats->AddText(sSection, buffer, HFP, HALeft, false, false, "");
        _stats->AddBreak(sSection, false);

      }
      _stats->AddBreak(sSection, false);
    }

    // kills - friends
    kills.Resize(0);
    for (int i=0; i<events.Size(); i++)
    {
      AIStatsEvent &event = events[i];
      switch (event.type)
      {
      case SETKillsFriendlyInfantry:
      case SETKillsFriendlySoft:
      case SETKillsFriendlyArmor:
      case SETKillsFriendlyAir:
        //    case SETUnitLost:
        {
          RString playerName = event.killedPlayer ? event.killedName : "";
          for (int j=0; j<kills.Size(); j++)
          {
            if (kills[j].type->GetName() == event.killedType && kills[j].playerName == playerName)
            {
              kills[j].n++;
              goto ExitSwitchFriends;
            }
          }
          {
            int index = kills.Add();
            Ref<EntityType> eType = VehicleTypes.New(event.killedType);
            kills[index].type = dynamic_cast<EntityAIType *>(eType.GetRef());
            if (!kills[index].type) kills[index].type = GWorld->Preloaded(VTypeAllVehicles);
            kills[index].playerName = playerName;
            kills[index].n = 1;
          }
        }
ExitSwitchFriends:
        break;
      }
    }
    if (kills.Size() > 0)
    {
      _stats->AddText(sSection, LocalizeString(IDS_BRIEF_YOURKILLS_FRIENDLY), HFP, HALeft, false, false, "");
      _stats->AddBreak(sSection, false);

      QSort(kills.Data(), kills.Size(), CmpKillsInfo);
      for (int j=0; j<kills.Size(); j++)
      {
        KillsInfo &info = kills[j];
        RString killed = info.playerName;
        if (killed.GetLength() == 0) killed = info.type->GetDisplayName();
        if (info.n > 1)
          sprintf(buffer, LocalizeString(IDS_BRIEF_FORMAT_GENERIC_TIMES), info.n, (const char *)killed);
        else
          strcpy(buffer, killed);
        _stats->AddText(sSection, buffer, HFP, HALeft, false, false, "");
        _stats->AddBreak(sSection, false);

      }
      _stats->AddBreak(sSection, false);
    }

    // kills - civilians
    kills.Resize(0);
    for (int i=0; i<events.Size(); i++)
    {
      AIStatsEvent &event = events[i];
      switch (event.type)
      {
      case SETKillsCivilInfantry:
      case SETKillsCivilSoft:
      case SETKillsCivilArmor:
      case SETKillsCivilAir:
        {
          RString playerName = event.killedPlayer ? event.killedName : "";
          for (int j=0; j<kills.Size(); j++)
          {
            if (kills[j].type->GetName() == event.killedType && kills[j].playerName == playerName)
            {
              kills[j].n++;
              goto ExitSwitchCivil;
            }
          }
          {
            int index = kills.Add();
            Ref<EntityType> eType = VehicleTypes.New(event.killedType);
            kills[index].type = dynamic_cast<EntityAIType *>(eType.GetRef());
            if (!kills[index].type) kills[index].type = GWorld->Preloaded(VTypeAllVehicles);
            kills[index].playerName = playerName;
            kills[index].n = 1;
          }
        }
ExitSwitchCivil:
        break;
      }
    }
    if (kills.Size() > 0)
    {
      _stats->AddText(sSection, LocalizeString(IDS_BRIEF_YOURKILLS_CIVIL), HFP, HALeft, false, false, "");
      _stats->AddBreak(sSection, false);


      QSort(kills.Data(), kills.Size(), CmpKillsInfo);
      for (int j=0; j<kills.Size(); j++)
      {
        KillsInfo &info = kills[j];
        RString killed = info.playerName;
        if (killed.GetLength() == 0) killed = info.type->GetDisplayName();
        if (info.n > 1)
          sprintf(buffer, LocalizeString(IDS_BRIEF_FORMAT_GENERIC_TIMES), info.n, (const char *)killed);
        else
          strcpy(buffer, killed);
        _stats->AddText(sSection, buffer, HFP, HALeft, false, false, "");
        _stats->AddBreak(sSection, false);

      }
      _stats->AddBreak(sSection, false);
    }

    AUTO_STATIC_ARRAY(CasualtiesInfo, casual, 32);
    int playerTotal = 0;
    for (int i=0; i<events.Size(); i++)
    {
      AIStatsEvent &event = events[i];
      if (event.type != SETUnitLost) continue;

      RString killerName;
      if (event.killedPlayer)
      {
        playerTotal++;
        if (!event.killerPlayer) continue;
        killerName = event.killerName;
        //      playerName = event.killedName;
      }
      for (int j=0; j<casual.Size(); j++)
      {
        if (casual[j].killedName == event.killedName && casual[j].killerName == killerName)
        {
          casual[j].n++;
          goto ExitSwitch2;
        }
      }
      {
        int index = casual.Add();
        casual[index].player = event.killedPlayer;
        casual[index].killedName = event.killedName;
        casual[index].killerName = killerName;
        casual[index].n = 1;
      }
ExitSwitch2:
      ;
    }
    if (playerTotal > 0 || casual.Size() > 0)
    {
      _stats->AddText(sSection, LocalizeString(IDS_BRIEF_CASUALTIES), HFP, HALeft, false, false, "");
      _stats->AddBreak(sSection, false);

      QSort(casual.Data(), casual.Size(), CmpCasualtiesInfo);
      if (playerTotal > 0)
      {
        if (playerTotal > 1)
          sprintf(buffer, LocalizeString(IDS_BRIEF_FORMAT_YOU_TIMES), playerTotal, (const char *)playerName);
        else
          sprintf(buffer, LocalizeString(IDS_BRIEF_FORMAT_YOU_ONCE), (const char *)playerName);
        _stats->AddText(sSection, buffer, HFP, HALeft, false, false, "");
        _stats->AddBreak(sSection, false);

      }
      for (int j=0; j<casual.Size(); j++)
      {
        CasualtiesInfo &info = casual[j];
        if (info.player)
        {
          Assert(info.killerName.GetLength() > 0);
          if (info.n > 1)
            sprintf(buffer, LocalizeString(IDS_BRIEF_FORMAT_BY_TIMES), info.n, (const char *)info.killerName);
          else
            sprintf(buffer, LocalizeString(IDS_BRIEF_FORMAT_BY_ONCE), (const char *)info.killerName);
        }
        else
        {
          if (info.n > 1)
            sprintf(buffer, LocalizeString(IDS_BRIEF_FORMAT_GENERIC_TIMES), info.n, (const char *)info.killedName);
          else
            strcpy(buffer, info.killedName);
        }
        _stats->AddText(sSection, buffer, HFP, HALeft, false, false, "");
        _stats->AddBreak(sSection, false);

      }
    }

    if (_statisticsLinks)
    {
      _stats->AddText(sSection, LocalizeString(IDS_BRIEF_STAT_CLOSE), HFP, HALeft, false, false, "Stat:close");
      _stats->AddBreak(sSection, false);
    }

    _stats->FormatSection(sSection);
    _stats->SwitchSection("__STATISTICS");
  }
}

static const RStringB LowCaseName(RStringB name)
{
  if (name.IsLowCase()) return name;
  RString copy = name;
  copy.Lower();
  return copy;
}
Notepad::Notepad(ControlsContainer *parent, int idc, ParamEntryPar cls)
: ControlObjectContainer(parent, idc, cls)
{
  _paper1 = GlobLoadTexture(LowCaseName(cls >> "paper1"));
  _paper2 = GlobLoadTexture(LowCaseName(cls >> "paper2"));
  _paper3 = GlobLoadTexture(LowCaseName(cls >> "paper3"));
  _paper4 = GlobLoadTexture(LowCaseName(cls >> "paper4"));
  _paper5 = GlobLoadTexture(LowCaseName(cls >> "paper5"));
  _paper6 = GlobLoadTexture(LowCaseName(cls >> "paper6"));
  _paper7 = GlobLoadTexture(LowCaseName(cls >> "paper7"));
  _paperSelection = cls >> "selectionPaper";
  _shape->AddLoadHandler(this);
  _shape->SetCanUsePushBuffer(false);
}

Notepad::~Notepad()
{
  _shape->RemoveLoadHandler(this);
}

void Notepad::LODShapeLoaded(LODShape *shape)
{
  _paper.Init(_shape, _paperSelection, NULL);
}

void Notepad::LODShapeUnloaded(LODShape *shape)
{
}

void Notepad::ShapeLoaded(LODShape *shape, int level)
{
  _paper.InitLevel(_shape, level);
}

void Notepad::ShapeUnloaded(LODShape *shape, int level)
{
  _paper.DeinitLevel(_shape, level);
}

void Notepad::SetPosition(Vector3Par pos)
{
  ControlObjectContainer::SetPosition(pos);

  DisplayMap *parent = dynamic_cast<DisplayMap *>(_parent.GetLink());
  if (parent) parent->AdjustMapVisibleRect();
}

void Notepad::Animate(AnimationContext &animContext, int level, bool setEngineStuff, Object *parentObject, float dist2)
{
  if (!_briefing) return;
  switch (_briefing->ActiveBookmark())
  {
  case 0:
    _paper.SetTexture(animContext, _shape, level, _paper1);
    break;
  case 1:
    _paper.SetTexture(animContext, _shape, level, _paper2);
    break;
  case 2:
    _paper.SetTexture(animContext, _shape, level, _paper3);
    break;
  case 3:
    _paper.SetTexture(animContext, _shape, level, _paper4);
    break;
  case 4:
    _paper.SetTexture(animContext, _shape, level, _paper6);
    break;
  case 5:
    _paper.SetTexture(animContext, _shape, level, _paper7);
    break;
  default:
    _paper.SetTexture(animContext, _shape, level, _paper5);
    break;
  }
}

void Notepad::Deanimate(int level, bool setEngineStuff)
{
}

ControlObjectAnimated::ControlObjectAnimated(ControlsContainer *parent, int idc, ParamEntryPar cls, bool ui)
: base(parent, idc, cls, ui)
{
  _par = cls.GetClassInterface();
  //  _shape->AddLoadHandler(this);
}

ControlObjectAnimated::~ControlObjectAnimated()
{
  //  _shape->RemoveLoadHandler(this);
}

void ControlObjectAnimated::LODShapeLoaded(LODShape *shape)
{
  Assert(shape == _shape);

  _shape->SetAnimationType(AnimTypeHardware);
  _shape->LoadSkeletonFromSource();

  ConstParamEntryPtr array = _par->FindEntry("Animations");
  if (array)
  {
    if (!_shape->GetAnimations())
    {
      _animations.Load(_shape, *array);
    }
  }
  _animSources.Load(this, GetAnimations());
}

void ControlObjectAnimated::LODShapeUnloaded(LODShape *shape)
{
  _animations.Clear();
  _animSources.Clear();
}

void ControlObjectAnimated::ShapeLoaded(LODShape *shape, int level)
{
  if (!_shape->GetAnimations() && _shape->GetSkeleton())
  {
    _animations.InitLevel(shape, level);
  }
}

void ControlObjectAnimated::ShapeUnloaded(LODShape *shape, int level)
{
  if (!_shape->GetAnimations() && _shape->GetSkeleton())
  {
    _animations.DeinitLevel(_shape,level);
  }
}

bool ControlObjectAnimated::PrepareShapeDrawMatrices(Matrix4Array &matrices, const ObjectVisualState &vs, LODShape *shape, int level)
{
  const Skeleton *sk = _shape->GetSkeleton();
  if (!sk)
  {
    RptF("No skeleton for shape %s",cc_cast(shape->GetName()));
    return false;
  }
  _shape->InitDrawMatrices(matrices,level);

  base::PrepareShapeDrawMatrices(matrices,vs,shape,level);

  return _animSources.PrepareShapeDrawMatrices(matrices, _shape, level, vs, this, GetAnimations());
}

void ControlObjectAnimated::Animate(AnimationContext &animContext, int level, bool setEngineStuff, Object *parentObject, float dist2)
{
}

void ControlObjectAnimated::Deanimate(int level, bool setEngineStuff)
{
}

Compass::Compass(ControlsContainer *parent, int idc, ParamEntryPar cls, bool ui)
: base(parent, idc, cls, ui)
{
  /*!
  \patch 1.01 Date 06/07/2001 by Ondra
  - Fixed watch and compass animation with T&L
  */
  _pointerAngle = 0;
  _lastPointerUpdateTime = UITime(-10000);

  _groupAngle = 0;
  _groupAngleWanted = 0;
  _lastUpdateTime = UITime(-10000);
  _shape->AddLoadHandler(this);
  _shape->SetCanUsePushBuffer(false);

  // store original orientation
  _orient = FutureVisualState().Orientation();

#if _VBS2
  _trackNorth = false;
#endif
}

Compass::~Compass()
{
  _shape->RemoveLoadHandler(this);
}

extern Camera OriginalCamera;

float Compass::GetPointerPos(const ObjectVisualState &vs) const
{
  Vector3 dir = OriginalCamera.Direction();
  float angle = atan2(dir.X(), dir.Z());
#if _VBS2 
  if (_trackNorth) 
  {
    Vector3 ndir = Vector3(VRotate,GetInvTransform(),VUp);
    angle = atan2(-ndir.X(), ndir.Z());   
  } 
  //smoothing compass to look more like a magnetic device
  float diff = AngleDifference(_pointerAngle, angle);
  float maxDiff = 2.0 * (Glob.uiTime - _lastPointerUpdateTime);
  saturate(diff, -maxDiff, maxDiff);
  _lastPointerUpdateTime = Glob.uiTime;
  _pointerAngle = AngleDifference(_pointerAngle, diff);
  angle = _pointerAngle;
#endif
  return angle;
}

float Compass::GetArrowPos(const ObjectVisualState &vs) const
{
  AIBrain *agent = GWorld->FocusOn();
  if (!agent) return 0;
  AIUnit *unit = agent->GetUnit();
  if (!unit) return 0;
  AISubgroup *subgrp = unit->GetSubgroup();
  if (!subgrp) return 0;
  AIGroup *grp = subgrp->GetGroup();
  if (!grp) return 0;

  Vector3 dir = OriginalCamera.Direction();
  float angle = atan2(dir.X(), dir.Z());

  if (subgrp == grp->MainSubgroup())
  {
    // waypoint
    if (grp->GetCurrent())
    {
      int index = grp->GetCurrent()->_fsm->Var(0);
      if (index >= 0 && index < grp->NWaypoints())
      {
        dir = grp->GetWaypointPosition(index) - unit->Position(unit->GetRenderVisualState());
      }
    }
  }
  else
  {
    // command
    Command *cmd = subgrp->GetCommand();
    if (cmd)
    {
      dir = cmd->_destination - unit->Position(unit->GetRenderVisualState());
    }
  }

  _groupAngleWanted = atan2(dir.X(), dir.Z());

  float diff = AngleDifference(_groupAngleWanted, _groupAngle);
  float maxDiff = 2.0 * (Glob.uiTime - _lastUpdateTime);
  saturate(diff, -maxDiff, maxDiff);
  _lastUpdateTime = Glob.uiTime;
  _groupAngle = AngleDifference(_groupAngle + diff, 0);

  return AngleDifference(angle, _groupAngle);
}

#include <Es/Common/delegate.hpp>

AnimationSource *Compass::CreateAnimationSource(const AnimationType *type, RStringB source)
{
  if (!strcmpi(source, "compassPointer")) return _animSources.CreateAnimationSource(&Compass::GetPointerPos);
  if (!strcmpi(source, "compassArrow")) return _animSources.CreateAnimationSource(&Compass::GetArrowPos);
  if (!strcmpi(source, "compassCover")) return _animSources.CreateAnimationSource(&Compass::GetCoverPos);

  AnimationSource *animSource = base::CreateAnimationSource(type, source);
  if (animSource) return animSource;

  RptF
    (
    "%s - unknown animation source %s",
    cc_cast(type->GetName()), cc_cast(source)
    );
  return base::CreateAnimationSource(type, "time");
}

void Compass::LODShapeLoaded(LODShape *shape)
{
  base::LODShapeLoaded(shape);
  const ParamEntry &par = *_par;
  _arrowHide.Init(_shape, (par >> "selectionArrow").operator RString(), NULL);
}

void Compass::LODShapeUnloaded(LODShape *shape)
{
  base::LODShapeUnloaded(shape);
}

void Compass::ShapeLoaded(LODShape *shape, int level)
{
  base::ShapeLoaded(shape, level);
  _arrowHide.InitLevel(_shape, level);
}
void Compass::ShapeUnloaded(LODShape *shape, int level)
{
  base::ShapeUnloaded(shape, level);
  _arrowHide.DeinitLevel(_shape, level);
}

Vector3 Compass::Center() const
{
  //  return _pointer.Center();
  return VZero;
}

void Compass::Animate(AnimationContext &animContext, int level, bool setEngineStuff, Object *parentObject, float dist2)
{
  AIBrain *agent = GWorld->FocusOn();
  AIUnit *unit = agent ? agent->GetUnit() : NULL;
  AISubgroup *subgrp = unit ? unit->GetSubgroup() : NULL;
  AIGroup *grp = subgrp ? subgrp->GetGroup() : NULL;
  bool hideArrow = true;
  if (grp)
  {
    if (subgrp == grp->MainSubgroup())
    {
      // waypoint
      if (grp->GetCurrent())
      {
        int index = grp->GetCurrent()->_fsm->Var(0);
        if (index >= 0 && index < grp->NWaypoints())
        {
          hideArrow = false;
        }
      }
    }
    else
    {
      // command
      Command *cmd = subgrp->GetCommand();
      if (cmd)
      {
        hideArrow = false;
      }
    }
  }

  if (hideArrow)
  {
    _arrowHide.Hide(animContext, _shape, level);
  }
}

void Compass::Deanimate(int level, bool setEngineStuff)
{
}

Watch::Watch(ControlsContainer *parent, int idc, ParamEntryPar cls, bool ui)
: base(parent, idc, cls, ui)
{
  _shape->AddLoadHandler(this);
  _shape->SetCanUsePushBuffer(false);
}

Watch::~Watch()
{
  _shape->RemoveLoadHandler(this);
}

void Watch::LODShapeLoaded(LODShape *shape)
{
  base::LODShapeLoaded(shape);

  const ParamEntry &par = *_par;
  _date1.Init(_shape, (par >> "selectionDate1").operator RString());
  _date2.Init(_shape, (par >> "selectionDate2").operator RString());
  _day.Init(_shape, (par >> "selectionDay").operator RString());
}

void Watch::LODShapeUnloaded(LODShape *shape)
{
  base::LODShapeUnloaded(shape);
}

void Watch::ShapeLoaded(LODShape *shape, int level)
{
  base::ShapeLoaded(shape, level);

  _date1.InitLevel(_shape,level);
  _date2.InitLevel(_shape,level);
  _day.InitLevel(_shape,level);
}

void Watch::ShapeUnloaded(LODShape *shape, int level)
{
  base::ShapeUnloaded(shape, level);

  _date1.DeinitLevel(_shape,level);
  _date2.DeinitLevel(_shape,level);
  _day.DeinitLevel(_shape,level);
}

void Watch::Animate(AnimationContext &animContext, int level, bool setEngineStuff, Object *parentObject, float dist2)
{
  if (setEngineStuff)
  {
    int year = Glob.clock.GetYear();
    int dayOfYear = toIntFloor(Glob.clock.GetTimeInYear() * 365);
    float timeOfDay = Glob.clock.GetTimeOfDay();
    if (timeOfDay >= 1.0)
    {
      timeOfDay--;
      dayOfYear++;
    }
    Assert(timeOfDay >= 0 && timeOfDay < 1.0);
    Assert(dayOfYear >= 0 && dayOfYear < 365);
    struct tm tmDate = { 0, 0, 0, 0, 0, 0 };
    tmDate.tm_year = year - 1900;
    tmDate.tm_yday = dayOfYear;
    int m = 0;
    while (tmDate.tm_yday >= GetDaysInMonth(year, m))
    {
      tmDate.tm_yday -= GetDaysInMonth(year, m);
      m++;
    }
    tmDate.tm_mday = tmDate.tm_yday + 1;
    tmDate.tm_mon = m;
    mktime(&tmDate);
    Assert(tmDate.tm_yday == dayOfYear);

    int day = tmDate.tm_wday - 1;
    if (day < 0) day = 6;
    _day.UVOffset(animContext, _shape, 0, 0.1 * day, level);

    day = tmDate.tm_mday / 10 - 1;
    if (day < 0) day = 9;
    _date1.UVOffset(animContext, _shape, 0, 0.1 * day, level);

    day = tmDate.tm_mday % 10 - 1;
    if (day < 0) day = 9;
    _date2.UVOffset(animContext, _shape, 0, 0.1 * day, level);
  }
}

void Watch::Deanimate(int level, bool setEngineStuff)
{
}

ControlObjectWithZoom *CreateCompass()
{
  return new Compass(NULL, -1, Pars >> "RscCompass", false);
}

ControlObjectWithZoom *CreateWatch()
{
  return new Watch(NULL, -1, Pars >> "RscWatch", false);
}

///////////////////////////////////////////////////////////////////////////////


ArtilleryStaticMapMain::ArtilleryStaticMapMain(DisplayArtilleryComputer *parent, int idc, ParamEntryPar cls, Transport *transport, float minRange, float maxRange, float defaultTime)
: CStaticMapMain(parent, idc, cls)
{
  _transport = transport;
  _minRange = minRange;
  _maxRange = maxRange;
  _defaultTime = defaultTime;

  _artilleryComputer = parent;
}

void ArtilleryStaticMapMain::SetRanges(float minRange, float maxRange, float defaultTime)
{
  _minRange = minRange;
  _maxRange = maxRange;
  _defaultTime = defaultTime;
}

void ArtilleryStaticMapMain::DrawExt(float alpha)
{
  CStaticMapMain::DrawExt(alpha);
  if(!_transport) return;
  if(!_transport->CommanderUnit()) return;

  PackedColor color;
  switch (_transport->CommanderUnit()->GetSide())
  {
  case TWest:
    {
      color = _colorCivilian;
      break;
    }
  case TEast:
    {
      color = _colorEnemy;
      break;
    }
  case TGuerrila:
    {
      color = _colorFriendly;
      break;
    }
  default:
    {
      color = _colorNeutral;
      break;
    }
  }

  char buffer[64];  
  DrawCoord posMapStart = WorldToScreen(_transport->RenderVisualState().Position() + Vector3(0,0,_minRange));
  sprintf(buffer, "%d", (int)_minRange);
  float width = GEngine->GetTextWidth(_sizeNames * 0.5f, _fontGrid, buffer);
  DrawSign(NULL,color,posMapStart + DrawCoord(-0.5f*width, _sizeNames * -0.35f),10,10,0, buffer, NULL, _sizeNames * 0.5f, true,false);
  
  DrawEllipse(_transport->RenderVisualState().Position(), _minRange, _minRange, 0,
    color);

  posMapStart = WorldToScreen(_transport->RenderVisualState().Position() + Vector3(0,0,_maxRange));
  sprintf(buffer, "%d", (int)_maxRange);
  width = GEngine->GetTextWidth(_sizeNames * 0.5f, _fontGrid, buffer);
  DrawSign(NULL,color,posMapStart + DrawCoord(-0.5f*width, _sizeNames *  0.35f),10,10,0, buffer, NULL, _sizeNames * 0.5f, true,false);
 
  DrawEllipse(_transport->RenderVisualState().Position(), _maxRange, _maxRange, 0,
    color);

  for (int i=_targetinfo.Size()-1; i>=0; i--)
  {
    if(Glob.time - _targetinfo[i].time > _targetinfo[i].eta) 
    {
      _targetinfo.Delete(i);
    }
    else
    {
      Vector3 end = _targetinfo[i].start + (_targetinfo[i].end - _targetinfo[i].start) * ((Glob.time - _targetinfo[i].time)/ _targetinfo[i].eta);

      DrawLine(_targetinfo[i].start , end,
        color);

      sprintf(buffer, "%d", (int) (_targetinfo[i].eta - (Glob.time - _targetinfo[i].time)));
      RString eta = "ETA " + RString(buffer) +"s";
      
      posMapStart = WorldToScreen(end);
      DrawSign(NULL,color,posMapStart + DrawCoord(_sizeNames * 0.2f, 0),10,10,0, eta, NULL, _sizeNames * 0.5f, true,false);

      DrawLine(_targetinfo[i].end + Vector3(-0.5*_targetinfo[i].spread,0,0), _targetinfo[i].end + Vector3(+0.5*_targetinfo[i].spread,0,0),
        PackedColor(125,125,125,125));
      DrawLine(_targetinfo[i].end + Vector3(0,0,-0.5*_targetinfo[i].spread), _targetinfo[i].end + Vector3(0,0,0.5*_targetinfo[i].spread),
        PackedColor(125,125,125,125));
    }
  }


  int shells = 0;
  if(GWorld->UI()->GetAtilleryTargetLocked())
  {
    float aimed = 0.0f;
    float time = 0;
    float radius = 0;

    Person *unit = GWorld->PlayerOn();
    if(unit && unit->Brain())
    {
      TurretContext context;
      if(_transport->FindTurret(unit,context))
      {
        int weapon = context._weapons->_currentWeapon;
        Vector3 tgtPos = GWorld->UI()->GetAtilleryTarget();
        tgtPos[1] = GLandscape->SurfaceY(tgtPos.X(), tgtPos.Z());

        if(weapon >=0 && weapon < context._weapons->_magazineSlots.Size())
        {
          const Magazine *magazine = context._weapons->_magazineSlots[weapon]._magazine;
          const MagazineType *aInfo = magazine ? magazine->_type : NULL;
          if(!aInfo) goto DrawTarget;
          const AmmoType *ammo = aInfo ? aInfo->_ammo : NULL;
          if(!ammo) goto DrawTarget;
          shells = context._weapons->_magazineSlots[weapon]._weaponMode->_burst;
          WeaponModeType *mode = context._weapons->_magazineSlots[weapon]._weaponMode;

          radius = _defaultTime* mode->artilleryDispersion;

          float fall = 0;
          switch (ammo->_simulation)
          {
          case AmmoShotMissile: case AmmoShotRocket:
            if (ammo->maxControlRange<10)
            {
              time = _transport->PreviewRocketBallistics(tgtPos , _transport->RenderVisualState().Position(), aInfo, ammo, fall, mode, true);
            }
            break;
          default:
            time = _transport->PreviewBallistics(tgtPos, _transport->RenderVisualState().Position(), aInfo, ammo, fall, mode, true);
            break;
          }

          if (time>=FLT_MAX) goto DrawTarget;
          tgtPos[1] += fall; // consider ballistics
          radius = time * mode->artilleryDispersion;

          Vector3 relDir =  _transport->PositionWorldToModel(tgtPos);
          aimed = _transport->FireAngleInRange(context, weapon, relDir);
        }
      }

DrawTarget:

        DrawLine(GWorld->UI()->GetAtilleryTarget() + Vector3(-0.5*radius,0,0), GWorld->UI()->GetAtilleryTarget() + Vector3(+0.5*radius,0,0),
          color);
        DrawLine(GWorld->UI()->GetAtilleryTarget() + Vector3(0,0,-0.5*radius), GWorld->UI()->GetAtilleryTarget() + Vector3(0,0,0.5*radius),
          color);

        if(aimed > 0.1f)
        {
          posMapStart = WorldToScreen(GWorld->UI()->GetAtilleryTarget());

          sprintf(buffer, "%d", (int)time);
          RString eta = "ETA " + RString(buffer) +"s";
          DrawSign(NULL,color,posMapStart + DrawCoord(_sizeNames * 0.1f, _sizeNames * 0.35f),10,10,0, eta, NULL, _sizeNames * 0.5f, true,false);

          int distance = Vector3(GWorld->UI()->GetAtilleryTarget().X(), 0, GWorld->UI()->GetAtilleryTarget().Z()).Distance(_transport->RenderVisualState().Position()); 
          sprintf(buffer, "%d", distance);
          DrawSign(NULL,color,posMapStart + DrawCoord(_sizeNames * 0.1f, _sizeNames * -0.35f),10,10,0, buffer, NULL, _sizeNames * 0.5f, true,false);

          sprintf(buffer, "%d", shells); 
          width = GEngine->GetTextWidth(_sizeNames * 0.5f, _fontGrid, buffer);
          DrawSign(NULL,color,posMapStart + DrawCoord(-width - _sizeNames * 0.1f, _sizeNames * 0.35f),10,10,0, buffer, NULL, _sizeNames * 0.5f, true,false);


          _artilleryComputer->SetETA(time,radius);
        }
        else
        {
          posMapStart = WorldToScreen(GWorld->UI()->GetAtilleryTarget());

          RString eta = "ETA --";
          DrawSign(NULL,color,posMapStart + DrawCoord(_sizeNames * 0.1f, _sizeNames * 0.35f),10,10,0, eta, NULL, _sizeNames * 0.5f, true,false);

          int distance = Vector3(GWorld->UI()->GetAtilleryTarget().X(), 0, GWorld->UI()->GetAtilleryTarget().Z()).Distance(_transport->RenderVisualState().Position()); 
          sprintf(buffer, "%d", distance);
          DrawSign(NULL,color,posMapStart + DrawCoord(_sizeNames * 0.1f, _sizeNames * -0.35f),10,10,0, buffer, NULL, _sizeNames * 0.5f, true,false);

          sprintf(buffer, "%d", shells); 
          width = GEngine->GetTextWidth(_sizeNames * 0.5f, _fontGrid, buffer);
          DrawSign(NULL,color,posMapStart + DrawCoord(-width - _sizeNames * 0.1f, _sizeNames * 0.35f),10,10,0, buffer, NULL, _sizeNames * 0.5f, true,false);

          _artilleryComputer->SetETA(-1,-1);
        }
    }
  }
}

void ArtilleryStaticMapMain::OnLButtonClick(float x, float y)
{
  Vector3 pos = ScreenToWorld(DrawCoord(x, y));
  pos[1] = GLandscape->SurfaceY(pos.X(), pos.Z());

  float fall = 0;
  float flyTime = 0;

  Person *player = GWorld->PlayerOn();
  AIBrain *unit = player ? player->Brain() : NULL;
  EntityAIFull *vehicle = unit? unit->GetVehicleIn(): NULL;

  if(!vehicle) return;
  // TODO: which turret?
  TurretContext context;
  if (vehicle->GetPrimaryGunnerTurret(context))
  {
    int weapon = context._weapons->_currentWeapon;
    const Magazine *magazine = context._weapons->_magazineSlots[weapon]._magazine;
    const MagazineType *aInfo = magazine ? magazine->_type : NULL;
    if(!aInfo) return;
    const AmmoType *ammo = aInfo ? aInfo->_ammo : NULL;
    if(!ammo) return;
    WeaponModeType *mode = context._weapons->_magazineSlots[weapon]._weaponMode;
    if(!mode) return;

    _artilleryComputer->SetETA(-1.0f,-1.0);

    switch (ammo->_simulation)
    {
    case AmmoShotMissile: case AmmoShotRocket:
      if (ammo->maxControlRange<10)
      {
        flyTime = vehicle->PreviewRocketBallistics(pos, vehicle->RenderVisualState().Position(), aInfo, ammo, fall, mode, true);
        GWorld->UI()->SetAtilleryTarget(true, pos);
        if (flyTime>=FLT_MAX) 
        {
          return;
        }
      }
      else
      {
        flyTime = 0.0f;
      }
      break;
    default:
      flyTime = vehicle->PreviewBallistics(pos, vehicle->RenderVisualState().Position(), aInfo, ammo, fall,  mode, true);
      GWorld->UI()->SetAtilleryTarget(true, pos);
      if (flyTime>=FLT_MAX) 
      {
        return;
      }
      break;
    }

    pos[1] += fall; // consider ballistics
    GWorld->UI()->SetAtilleryTarget(true, pos);
  }
};

void ArtilleryStaticMapMain::OnMouseMove(float x, float y, bool active)
{
  Vector3 pos = ScreenToWorld(DrawCoord(x, y));
  pos[1] = GLandscape->SurfaceY(pos.X(), pos.Z());

  _artilleryComputer->SetGrid(pos);

  CStaticMapMain::OnMouseMove(x, y, active);
}

void ArtilleryStaticMapMain::AddArtilleryTargetInfo(ArtilleryTargetInfo atinfo)
{
  _targetinfo.Add(atinfo);
}


int DisplayArtilleryComputer::_running = 0;
/// artillery 
DisplayArtilleryComputer::DisplayArtilleryComputer(ControlsContainer *parent, bool userDialog)
: Display(parent)
{
  _running++;
  _enableSimulation = true;
  _targetETA = -1;

  _minRangeValue = _maxRangeValue =0;

  _map = NULL;
  _name = NULL;
  _id = NULL;
  _grid = NULL;
  _distance = NULL;
  _minRange = NULL;
  _maxRange = NULL;
  _direction = NULL;
  _altitude = NULL;
  _mode = NULL;
  _ammo = NULL;
  _shells = NULL;
  _spread = NULL;
  _ETA = NULL;
  _transport = NULL;
  _ammoCount = NULL;
  _warning = NULL;

  // store original coordinates
  Load("RscDisplayArtillery");

  if(_name && _transport) 
    _name->SetText(_transport->Type()->GetDisplayName());
  
  char buffer[256];  
  if(_minRange)
  {
    sprintf(buffer, "%d", (int)_minRangeValue);
    _minRange->SetText(buffer);
  }
  if(_maxRange)
  {
    sprintf(buffer, "%d", (int)_maxRangeValue);
    _maxRange->SetText(buffer);
  }


  Person *unit = GWorld->PlayerOn();
  if(_transport && unit) 
  {
    TurretContext context;
    if(_transport->FindTurret(unit,context))
    {
      if (context._weapons->_currentWeapon >=0 && context._weapons->_currentWeapon< context._weapons->_magazineSlots.Size())
      {
        const MagazineSlot &slot = context._weapons->_magazineSlots[context._weapons->_currentWeapon]; 
        Magazine *mag = slot._magazine;
        char buffer[32];  

        if(_ammo && mag)
        {
          const MagazineType *aInfo = mag ? mag->_type : NULL;
          if(aInfo) _ammo->SetText(aInfo->_displayNameShort);
        }

        if(_shells && mag)
        {
          sprintf(buffer, "%d", slot._weaponMode->_burst);
          _shells->SetText(buffer);
        }
      }

      if(_mode)
      {
        for (int i = 0; i< context._weapons->_magazineSlots.Size(); i++)
        {
          if (context._weapons->_magazineSlots[i]._weaponMode->_showToPlayer)
          {
            int index = _mode->AddString(context._weapons->_magazineSlots[i]._weaponMode->GetDisplayName());
            _mode->SetValue(index,i);
            if(i == context._weapons->_currentWeapon)
              _mode->SetCurSel(index);
          }
        }
      }
    }
  }

}

bool DisplayArtilleryComputer::GetWeaponRanges(TurretContext context, float &minRange, float &maxRange, float &defaultTime)
{
  defaultTime = 0;
  minRange = 0;
  maxRange = 0;

  int weapon = context._weapons->_currentWeapon;
  if(weapon<0 || weapon>=context._weapons->_magazineSlots.Size())
      return true;

  const Magazine *magazine = context._weapons->_magazineSlots[weapon]._magazine;
  const MagazineType *aInfo = magazine ? magazine->_type : NULL;
  WeaponModeType *mode = context._weapons->_magazineSlots[weapon]._weaponMode;
  if(!aInfo) return true;
  if(!mode) return true;
  
 // const AmmoType *ammo = aInfo->_ammo;

  float minElev = max((H_PI * 0.25f),context._turretType->_minElev+ context._turretType->_neutralXRot);
  float maxElev = context._turretType->_maxElev + context._turretType->_neutralXRot;

  defaultTime = ((2 * sin(minElev) * aInfo->_initSpeed * mode->artilleryCharge) / G_CONST);
  maxRange = defaultTime * cos(minElev) * aInfo->_initSpeed * mode->artilleryCharge; 
  minRange = ((2 * sin(maxElev) * aInfo->_initSpeed *  mode->artilleryCharge) / G_CONST) * cos(maxElev) * (aInfo->_initSpeed* mode->artilleryCharge); 

  return true;
}


Control *DisplayArtilleryComputer::OnCreateCtrl(int type, int idc, ParamEntryPar cls)
{
  switch (idc)
  {
  case IDC_ARTILLERY_MAP:
    {
      Person *unit = GWorld->PlayerOn();
      if(!unit || !unit->Brain()) break;
      _transport = unit->Brain()->GetVehicleIn();
      if(!_transport) break;

      TurretContext context;
      if(_transport->FindTurret(unit,context))
      {
        float defaultTime;
        if(GetWeaponRanges(context,_minRangeValue,_maxRangeValue, defaultTime))
        {
          _map = new ArtilleryStaticMapMain(this, idc, cls, _transport, _minRangeValue, _maxRangeValue, defaultTime);
          if(_map)
          {
            _map->SetVisibleRect(_map->X(), _map->Y(), _map->W(), _map->H());
            _map->Center();
          }
          return _map;
        }
      }
    }
  case IDC_ARTILLERY_MAP_NAME :      
    {
      _name  = new CStatic(this, idc, cls);
      return _name ;
    }
  case IDC_ARTILLERY_MAP_ID:         
    {
      _id   = new CStatic(this, idc, cls);
      return _id ;
    }
  case IDC_ARTILLERY_MAP_GRID:       
    {
      _grid   = new CStatic(this, idc, cls);
      return _grid  ;
    }
  case IDC_ARTILLERY_MAP_DIST:
    {
      _distance   = new CStatic(this, idc, cls);
      return _distance  ;
    }
  case IDC_ARTILLERY_MAP_MIN: 
    {
      _minRange   = new CStatic(this, idc, cls);
      return _minRange  ;
    }
  case IDC_ARTILLERY_MAP_MAX:
    {
      _maxRange   = new CStatic(this, idc, cls);
      return _maxRange  ;
    }
  case IDC_ARTILLERY_MAP_DIR: 
    {
      _direction   = new CStatic(this, idc, cls);
      return _direction  ;
    }
  case IDC_ARTILLERY_MAP_ALT: 
    {
      _altitude   = new CStatic(this, idc, cls);
      return _altitude  ;
    }
  case IDC_ARTILLERY_MAP_MODE: 
    {
      _mode   = new CCombo(this, idc, cls);
      return _mode  ;
    }
  case IDC_ARTILLERY_MAP_AMMO : 
    {
      _ammo   = new CStatic(this, idc, cls);
      return _ammo  ;
    }      
  case IDC_ARTILLERY_MAP_SHELLS:  
    {
      _shells   = new CStatic(this, idc, cls);
      return _shells  ;
    }
  case IDC_ARTILLERY_MAP_SPREAD:
    {
      _spread   = new CStatic(this, idc, cls);
      return _spread  ;
    }
  case IDC_ARTILLERY_MAP_ETA: 
    {
      _ETA   = new CStatic(this, idc, cls);
      return _ETA  ;
    }
  case IDC_ARTILLERY_MAP_AMMOCOUNT: 
    {
      _ammoCount   = new CStatic(this, idc, cls);
      return _ammoCount  ;
    }
  case IDC_ARTILLERY_MAP_WARNING: 
    {
      _warning   = new CStatic(this, idc, cls);
      if(_warning) _warning->ShowCtrl(false);
      return _warning  ;
    }
  }

  Control *ctrl = Display::OnCreateCtrl(type, idc, cls);
  
  if(idc == IDC_ARTILLERY_MAP_FIRE)
    ctrl->EnableCtrl(false);

  return ctrl;
}

void DisplayArtilleryComputer::OnSimulate(EntityAI *vehicle)
{

  // higher priority to override actions when map is shown
  if (GInput.GetActionToDo(UAHideMap, true, true, false, 3))
    Exit(IDC_CANCEL);

  if(GWorld->PlayerOn() && GWorld->PlayerOn()->Brain())
  {
    AIBrain *unit = GWorld->PlayerOn()->Brain();
    if(
      !unit->GetUnit() || unit->GetUnit()->GetLifeState() != LifeStateAlive
      )
      Exit(IDC_CANCEL);

    if(
      !unit->GetVehicleIn() || unit->GetVehicleIn()->IsDamageDestroyed()
      )
      Exit(IDC_CANCEL);
  }
  else  Exit(IDC_CANCEL);


  Display::OnSimulate(vehicle);
}


void DisplayArtilleryComputer::OnButtonClicked(int idc)
{
  Person *player = GWorld->PlayerOn();
  AIBrain *unit = player ? player->Brain() : NULL;
  EntityAIFull *vehicle = unit? unit->GetVehicleIn(): NULL;

  switch (idc)
  {
  case IDC_ARTILLERY_MAP_FIRE:
    {
      if(!vehicle) return;
      // TODO: which turret?
      TurretContextV context;
      if (vehicle->GetPrimaryGunnerTurret(unconst_cast(vehicle->FutureVisualState()), context))
      {
        int weapon = context._weapons->_currentWeapon;
        if(vehicle->FireWeapon(context, weapon, NULL, false))
        {
          ArtilleryTargetInfo atinfo;
          atinfo.start = _transport->RenderVisualState().Position();
          atinfo.end =  GWorld->UI()->GetAtilleryTarget();
          atinfo.eta = _targetETA;
          atinfo.time = Glob.time;
          atinfo.spread = _targetSpread;
          if(_map) _map->AddArtilleryTargetInfo(atinfo);
        }
      }
      return;
    }
  default:
    Display::OnButtonClicked(idc);
  }
}

void DisplayArtilleryComputer::OnLBSelChanged(IControl *ctrl, int curSel)
{
  Person *unit = GWorld->PlayerOn();
  if(_transport && unit) 
  {
    TurretContext context;
    if(_transport->FindTurret(unit,context))
    {
      context._weapons->SelectWeapon(_transport, _mode->GetValue(curSel));

      //reset weapon ranges
      char buffer[256];  
      float defaultTime;
      if(GetWeaponRanges(context,_minRangeValue,_maxRangeValue, defaultTime))
      if(_minRange)
      {
        sprintf(buffer, "%d", (int)_minRangeValue);
        _minRange->SetText(buffer);
      }
      if(_maxRange)
      {
        sprintf(buffer, "%d", (int)_maxRangeValue);
        _maxRange->SetText(buffer);
      }

      if(_map) _map->SetRanges(_minRangeValue, _maxRangeValue, defaultTime);

      //reset mode and ammo name
      if (context._weapons->_currentWeapon >=0 && context._weapons->_currentWeapon< context._weapons->_magazineSlots.Size())
      {
        const MagazineSlot &slot = context._weapons->_magazineSlots[context._weapons->_currentWeapon]; 
        Magazine *mag = slot._magazine;
        char buffer[32];  

        if(_ammo && mag)
        {
          const MagazineType *aInfo = mag ? mag->_type : NULL;
          if(aInfo && aInfo->_displayNameShort.GetLength()>0) _ammo->SetText(aInfo->_displayNameShort);
          else _ammo->SetText("--");
        }

        if(_shells && mag)
        {
          sprintf(buffer, "%d", slot._weaponMode->_burst);
          _shells->SetText(buffer);
        }

      }
      else
      {
        if(_ammo) _ammo->SetText("--");
        if(_shells) _shells->SetText("--");
      }

    }
  }

}

void DisplayArtilleryComputer::SetETA(float time, float radius)
{
  char buffer[32];  
  if(time> 0.0f)
  {
    if(_warning) _warning->ShowCtrl(false);
    IControl *ctrl = GetCtrl(IDC_ARTILLERY_MAP_FIRE);
    if(ctrl) ctrl->EnableCtrl(true);
    _targetETA = time;
    _targetSpread = radius;

    if(_ETA)
    {
      sprintf(buffer, "%d",int(time));
      _ETA->SetText(buffer);
    }

    if(_spread)
    {
      sprintf(buffer, "%d",int(radius));
      _spread->SetText(buffer);
    }

    Person *unit = GWorld->PlayerOn();
    if(_transport && unit) 
    {
      TurretContext context;
      if(_transport->FindTurret(unit,context))
      {
        //reset mode and ammo name
        if (context._weapons->_currentWeapon >=0 && context._weapons->_currentWeapon< context._weapons->_magazineSlots.Size())
        {
          const MagazineSlot &slot = context._weapons->_magazineSlots[context._weapons->_currentWeapon]; 
          char buffer[32];  
          if(_ammoCount && slot._magazine)
          {
            sprintf(buffer, "%d", slot._magazine->GetAmmo());
            _ammoCount->SetText(buffer);

            float reload = 0;
            if (slot._magazine->_reloadMagazine > 0) reload = 1;
            else reload = slot._magazine->_reload;

            if(slot._magazine->GetAmmo() <=0 || reload > 0)
            {
              IControl *ctrl = GetCtrl(IDC_ARTILLERY_MAP_FIRE);
              if(ctrl) ctrl->EnableCtrl(false);
            }
          }
        }
      }
    }
  }
  else
  {
    _targetETA = -1;
    _targetSpread = -1;
    IControl *ctrl = GetCtrl(IDC_ARTILLERY_MAP_FIRE);
    if(ctrl) ctrl->EnableCtrl(false);
    if(_warning )_warning->ShowCtrl(true);

    if(_ETA) _ETA->SetText("--");
    if(_spread) _spread->SetText("--");
  }



}

void DisplayArtilleryComputer::SetGrid(Vector3 pos)
{
  char buffer[32];  

  if(_distance && _transport)
  {
    sprintf(buffer, "%d",int(pos.Distance(_transport->RenderVisualState().Position())));
    _distance->SetText(buffer);
  }

  if(_direction && _transport)
  {
    Vector3 dir = pos - _transport->RenderVisualState().Position();

    float angle;
    if (fabs(dir.X()) + fabs(dir.Z()) > 1e-3)
      angle = atan2(dir.X(), dir.Z());
    else
      angle = 0;

    int degAngle = toInt(angle * (180.0f / H_PI));
    if (degAngle < 0) degAngle += 360;

    sprintf(buffer, "%d",degAngle);

    _direction->SetText(buffer);
  }

  if(_grid)
  {
    PositionToAA11(pos,buffer);
    _grid->SetText(buffer);
  }

  if(_altitude)
  {
    sprintf(buffer, "%d",(int)(pos.Y()));
    _altitude->SetText(buffer);
  }
}

void DisplayArtilleryComputer::DestroyHUD(int exit)
{
  GWorld->DestroyUserDialog();
}

DisplayArtilleryComputer::~DisplayArtilleryComputer()
{
  GWorld->UI()->SetAtilleryTarget(false);
  _running--;
}

AbstractOptionsUI *CreateArtilleryComputerDialog(bool userDialog)
{
  return new DisplayArtilleryComputer(NULL, userDialog);
}


bool DisplayArtilleryComputerIsRunning()
{
  return DisplayArtilleryComputer::IsRunning();
}

///////////////////////////////////////////////////////////////////////////////
// creation of main display

AbstractOptionsUI *CreateMainMapUI()
{
  return new DisplayMainMap(NULL);
}

void DisplayMainMap::DestroyHUD(int exit)
{
  GLOB_WORLD->DestroyMap(exit);
}
