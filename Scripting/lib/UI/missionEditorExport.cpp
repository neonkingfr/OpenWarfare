#include "../wpch.hpp"

#if _ENABLE_EDITOR2

// void ExportMission(ArcadeTemplate &t, RString filename) - export from old mission editor

#if _VBS2
  #include "missionEditorVBS.hpp"
#else
  #include "missionEditor.hpp"
#endif

#include "../arcadeTemplate.hpp"
#include "../Network/network.hpp"
#include "../soldierOld.hpp"
#include "../invisibleVeh.hpp"

template <class Type>
inline RString ValueToString(const Type &value)
{
  return FindEnumName(value);
}

template <>
inline RString ValueToString(const RString &value)
{
  return value;
}

template <>
inline RString ValueToString(const float &value)
{
  return Format("%.8g", value);
}

template <>
inline RString ValueToString(const int &value)
{
  return Format("%d", value);
}

template <>
inline RString ValueToString(const VisitorObjId &value)
{
  return Format("%d", value.Encode());
}

template <>
inline RString ValueToString(const ObjectId &value)
{
  return Format("%d", value.Encode());
}

template <>
inline RString ValueToString(const bool &value)
{
  if (value)
    return "true";
  else
    return "false";
}

template <>
inline RString ValueToString(const Vector3 &value)
{
  return Format("[%.8g, %.8g]", value.X(), value.Z());
}

template <>
inline RString ValueToString(const TargetSide &value)
{
  switch (value)
  {
  case TWest:
    return "west";
  case TEast:
    return "east";
  case TCivilian:
    return "civilian";
  case TGuerrila:
    return "resistance";
  case TLogic:
    return "sideLogic";
  default:
    Fail("Side");
    return "ERROR";
  }
}

template <class Type>
void SetObjectArgument(EditorObject *object, RString name, const Type &value)
{
  object->SetArgument(name, ValueToString(value));
};

struct CenterInfo
{
  TargetSide side;
  EditorObject *object;
};
TypeIsMovableZeroed(CenterInfo)

struct MarkerInfo
{
  RString name;
  EditorObject *object;
};
TypeIsMovableZeroed(MarkerInfo)

struct VehicleInfo
{
  int id;
  EditorObject *object;
};
TypeIsMovableZeroed(VehicleInfo)

struct SyncInfo
{
  int sync;
  EditorObject *object1;
  EditorObject *object2;
};
TypeIsMovableZeroed(SyncInfo)

TypeIsSimple(EditorObject *)

// export

static void ExportIntel(EditorWorkspace &ws, const ArcadeIntel &intel)
{
  EditorObjectType *type = ws.FindObjectType("intel");
  if (!type)
  {
    RptF("Missing intel object type");
    return;
  }
  EditorObject *object = ws.AddObject(type);
  SetObjectArgument(object, "OVERCAST", intel.weather);
  SetObjectArgument(object, "OVERCAST_WANTED", intel.weatherForecast);
  SetObjectArgument(object, "FOG", intel.fog);
  SetObjectArgument(object, "FOG_WANTED", intel.fogForecast);
  SetObjectArgument(object, "YEAR", intel.year);
  SetObjectArgument(object, "MONTH", intel.month);
  SetObjectArgument(object, "DAY", intel.day);
  SetObjectArgument(object, "HOUR", intel.hour);
  SetObjectArgument(object, "MINUTE", intel.minute);
}

static EditorObject *ExportCenter(EditorWorkspace &ws, TargetSide side, const ArcadeIntel &intel)
{
  EditorObjectType *type = ws.FindObjectType("center");
  if (!type)
  {
    RptF("Missing center object type");
    return NULL;
  }
  EditorObject *object = ws.AddObject(type);
  SetObjectArgument(object, "SIDE", side);
  if (side == TLogic)
  {
    SetObjectArgument(object, "FRIEND_EAST", 1.0f);
    SetObjectArgument(object, "FRIEND_WEST", 1.0f);
    SetObjectArgument(object, "FRIEND_RESISTANCE", 1.0f);
  }
  else if (side == TCivilian)
  {
    SetObjectArgument(object, "FRIEND_EAST", intel.friends[TGuerrila][TEast]);
    SetObjectArgument(object, "FRIEND_WEST", intel.friends[TGuerrila][TWest]);
    SetObjectArgument(object, "FRIEND_RESISTANCE", intel.friends[TGuerrila][TGuerrila]);
  }
  else
  {
    SetObjectArgument(object, "FRIEND_EAST", intel.friends[side][TEast]);
    SetObjectArgument(object, "FRIEND_WEST", intel.friends[side][TWest]);
    SetObjectArgument(object, "FRIEND_RESISTANCE", intel.friends[side][TGuerrila]);
  }
  SetObjectArgument(object, "FRIEND_CIVILIAN", 1.0f);
  return object;
}

static EditorObject *ExportMarker(EditorWorkspace &ws, const ArcadeMarkerInfo &marker)
{
  EditorObjectType *type = ws.FindObjectType("marker");
  if (!type)
  {
    RptF("Missing marker object type");
    return NULL;
  }
  EditorObject *object = ws.AddObject(type);
  SetObjectArgument(object, "POSITION", marker.position);
  SetObjectArgument(object, "NAME", marker.name);
  SetObjectArgument(object, "TEXT", marker.text);
  SetObjectArgument(object, "MARKER_TYPE", marker.markerType);
  SetObjectArgument(object, "TYPE", marker.type);
  SetObjectArgument(object, "COLOR", marker.colorName);
  SetObjectArgument(object, "FILL", marker.fillName);
  float size = 1.0f;
  if (marker.markerType == MTIcon)
  {
    size = marker.size;
    const float defaultMarkerSize = 32.0f;
    if (size == 0) size = defaultMarkerSize;
  }
  
  SetObjectArgument(object, "A", marker.a * size);
  SetObjectArgument(object, "B", marker.b * size);
  SetObjectArgument(object, "ANGLE", marker.angle);
  SetObjectArgument(object, "DRAWBORDER", marker.drawBorder);
  return object;
}

static EditorObject *ExportGroup(EditorWorkspace &ws, EditorObject *center, const ArcadeGroupInfo &group)
{
  EditorObjectType *type = ws.FindObjectType("group");
  if (!type)
  {
    RptF("Missing group object type");
    return NULL;
  }
  EditorObject *object = ws.AddObject(type);
  SetObjectArgument(object, "CENTER", center->GetArgument("VARIABLE_NAME"));
  return object;
}

RString CreateCondition(RString presenceCondition, float presence)
{
  if (presence < 1)
  {
    if (strcmp(presenceCondition, "true") == 0) return Format("%.8g > random 1", presence);
    return Format("(%s) and (%.8g > random 1)", (const char *)presenceCondition, presence);
  }
  else return presenceCondition;
}

static EditorObject *ExportVehicle(EditorWorkspace &ws, const ArcadeUnitInfo &unit, AutoArray<MarkerInfo> &markerMap)
{
  RString vehClass = Pars >> "CfgVehicles" >> unit.vehicle >> "vehicleClass";

  bool vehicle = false;
  EditorObjectType *type = NULL;

  if (stricmp(vehClass, "Sounds") == 0)
  {
    // sound source
    type = ws.FindObjectType("soundSource");
    if (!type)
    {
      RptF("Missing soundSource object type");
      return NULL;
    }
  }
  else if (stricmp(vehClass, "Mines") == 0)
  {
    // mine
    type = ws.FindObjectType("mine");
    if (!type)
    {
      RptF("Missing mine object type");
      return NULL;
    }
  }
  else
  {
    // regular vehicle
    type = ws.FindObjectType("vehicle");
    if (!type)
    {
      RptF("Missing vehicle object type");
      return NULL;
    }
    vehicle = true;
  }

  EditorObject *object = ws.AddObject(type);

  RString condition = CreateCondition(unit.presenceCondition, unit.presence);
  SetObjectArgument(object, "PRESENCE_CONDITION", condition);
  SetObjectArgument(object, "POSITION", unit.position);
  SetObjectArgument(object, "TYPE", unit.vehicle);
  for (int i=0; i<unit.markers.Size(); i++)
    for (int j=0; j<markerMap.Size(); j++)
      if (markerMap[j].name == unit.markers[i])
      {
        SetObjectArgument(object, "MARKER", markerMap[j].object->GetArgument("VARIABLE_NAME"));
        break;
      }
  SetObjectArgument(object, "PLACEMENT", unit.placement);
  if (!vehicle) return NULL; // return object only for vehicles

  SetObjectArgument(object, "SPECIAL", unit.special);
  SetObjectArgument(object, "AZIMUT", unit.azimut);
  SetObjectArgument(object, "NAME", unit.name);
  SetObjectArgument(object, "HEALTH", unit.health);
  SetObjectArgument(object, "FUEL", unit.fuel);
  SetObjectArgument(object, "AMMO", unit.ammo);
  SetObjectArgument(object, "ID", unit.id);
  SetObjectArgument(object, "AGE", unit.age);
  SetObjectArgument(object, "LOCK", unit.lock);
  SetObjectArgument(object, "INIT", unit.init);
  return object;  
}

static void ExportCrew
(
  EditorWorkspace &ws, const ArcadeUnitInfo &unit, EditorObject *group, EditorObject *vehicle,
  RString crewType, Rank rank, GetInPosition pos, bool player, bool leader
)
{
  Assert(group);
  Assert(vehicle);

  // soldier in vehicle
  EditorObjectType *type = ws.FindObjectType("unit");
  if (!type)
  {
    RptF("Missing unit object type");
    return;
  }
  EditorObject *object = ws.AddObject(type);

  RString grpName = group->GetArgument("VARIABLE_NAME");
  RString vehName = vehicle->GetArgument("VARIABLE_NAME");

  SetObjectArgument(object, "PRESENCE_CONDITION", Format("not isNull %s", (const char *)vehName));
  SetObjectArgument(object, "POSITION", unit.position);
  SetObjectArgument(object, "GROUP", grpName);
  SetObjectArgument(object, "TYPE", crewType);
  SetObjectArgument(object, "PLACEMENT", 0);
  SetObjectArgument(object, "SPECIAL", ASpNone);
  SetObjectArgument(object, "AZIMUT", 0);
  SetObjectArgument(object, "HEALTH", 1);
  SetObjectArgument(object, "AMMO", 1);
  SetObjectArgument(object, "ID", -1);
  SetObjectArgument(object, "AGE", AAUnknown);
  SetObjectArgument(object, "INIT", RString());
  SetObjectArgument(object, "RANK", rank);
  SetObjectArgument(object, "SKILL", unit.skill);
  RString name;
  switch (pos)
  {
  case GIPCommander:
    SetObjectArgument(object, "COMMANDER", vehName);
    if (unit.name.GetLength() > 0) name = unit.name + RString("c");
    break;
  case GIPDriver:
    SetObjectArgument(object, "DRIVER", vehName);
    if (unit.name.GetLength() > 0) name = unit.name + RString("d");
    break;
  case GIPGunner:
    SetObjectArgument(object, "GUNNER", vehName);
    if (unit.name.GetLength() > 0) name = unit.name + RString("g");
    break;
  }
  SetObjectArgument(object, "NAME", name);
  SetObjectArgument(object, "PLAYER", player);
  if (leader) SetObjectArgument(object, "LEADER", grpName);
}

static EditorObject *ExportUnit(EditorWorkspace &ws, const ArcadeUnitInfo &unit, EditorObject *group, AutoArray<MarkerInfo> &markerMap)
{
  // free soldier / soldier in cargo
  Assert(group);
  //Assert(vehicle);

  // soldier in vehicle
  EditorObjectType *type = ws.FindObjectType("unit");
  if (!type)
  {
    RptF("Missing unit object type");
    return NULL;
  }
  EditorObject *object = ws.AddObject(type);

  RString grpName = group->GetArgument("VARIABLE_NAME");

  RString condition = CreateCondition(unit.presenceCondition, unit.presence);
  SetObjectArgument(object, "PRESENCE_CONDITION", condition);
  SetObjectArgument(object, "POSITION", unit.position);
  SetObjectArgument(object, "GROUP", grpName);
  SetObjectArgument(object, "TYPE", unit.vehicle);
  for (int i=0; i<unit.markers.Size(); i++)
    for (int j=0; j<markerMap.Size(); j++)
      if (markerMap[j].name == unit.markers[i])
      {
        SetObjectArgument(object, "MARKER", markerMap[j].object->GetArgument("VARIABLE_NAME"));
        break;
      }
  SetObjectArgument(object, "PLACEMENT", unit.placement);
  SetObjectArgument(object, "SPECIAL", unit.special);

  SetObjectArgument(object, "AZIMUT", unit.azimut);
  SetObjectArgument(object, "NAME", unit.name);
  SetObjectArgument(object, "HEALTH", unit.health);
  SetObjectArgument(object, "AMMO", unit.ammo);
  SetObjectArgument(object, "ID", unit.id);
  SetObjectArgument(object, "AGE", unit.age);
  SetObjectArgument(object, "INIT", unit.init);
  SetObjectArgument(object, "RANK", unit.rank);
  SetObjectArgument(object, "SKILL", unit.skill);

  SetObjectArgument(object, "PLAYER", unit.player == APPlayerCommander);
  if (unit.leader) SetObjectArgument(object, "LEADER", grpName);

  return object;
}

static void ExportEffects(EditorObject *object, const ArcadeEffects &effects)
{
  SetObjectArgument(object, "EFFECT_CONDITION", effects.condition);
  SetObjectArgument(object, "SOUND_EFFECT", effects.sound);
  SetObjectArgument(object, "VOICE_EFFECT", effects.voice);
  SetObjectArgument(object, "SOUND_ENV_EFFECT", effects.soundEnv);
  SetObjectArgument(object, "SOUND_DET_EFFECT", effects.soundDet);
  SetObjectArgument(object, "MUSIC_EFFECT", effects.track);
  SetObjectArgument(object, "TITLE_EFFECT_TYPE", effects.titleType);
  SetObjectArgument(object, "TITLE_EFFECT_EFFECT", effects.titleEffect);
  SetObjectArgument(object, "TITLE_EFFECT_TITLE", effects.title);
}

static EditorObject *ExportWaypoint(EditorWorkspace &ws, const ArcadeWaypointInfo &wp, EditorObject *group, AutoArray<VehicleInfo> &vehicleMap, AutoArray<SyncInfo> &waypointMap, EditorObject *prev)
{
  Assert(group);

  EditorObjectType *type = ws.FindObjectType("waypoint");
  if (!type)
  {
    RptF("Missing waypoint object type");
    return NULL;
  }
  EditorObject *object = ws.AddObject(type);

  RString grpName = group->GetArgument("VARIABLE_NAME");

  SetObjectArgument(object, "POSITION", wp.position);
  SetObjectArgument(object, "PLACEMENT", wp.placement);
  SetObjectArgument(object, "GROUP", grpName);
  SetObjectArgument(object, "TYPE", wp.type);
  if (wp.id >= 0)
  {
    bool found = false;
    for (int i=0; i<vehicleMap.Size(); i++)
      if (wp.id == vehicleMap[i].id)
      {
        SetObjectArgument(object, "VEHICLE", vehicleMap[i].object->GetArgument("VARIABLE_NAME"));
        found = true;
        break;
      }
      DoAssert(found);
  }
  // TODO: use link to object instead
  SetObjectArgument(object, "ID_STATIC", wp.idObject);
  SetObjectArgument(object, "HOUSE_POS", wp.housePos);
  SetObjectArgument(object, "COMBAT_MODE", wp.combatMode);
  SetObjectArgument(object, "FORMATION", wp.formation);
  SetObjectArgument(object, "SPEED", wp.speed);
  SetObjectArgument(object, "COMBAT", wp.combat);
  SetObjectArgument(object, "DESCRIPTION", wp.description);
  SetObjectArgument(object, "EXP_COND", wp._expCond);
  SetObjectArgument(object, "EXP_ACTIV", wp._expActiv);
  SetObjectArgument(object, "SCRIPT", wp.script);
  SetObjectArgument(object, "TIMEOUT_MIN", wp.timeoutMin);
  SetObjectArgument(object, "TIMEOUT_MID", wp.timeoutMid);
  SetObjectArgument(object, "TIMEOUT_MAX", wp.timeoutMax);
  SetObjectArgument(object, "SHOW", wp.showWP);
  if (prev) SetObjectArgument(object, "PREVIOUS", prev->GetArgument("VARIABLE_NAME"));
  ExportEffects(object, wp.effects);

  // synchronizations
  for (int s=0; s<wp.synchronizations.Size(); s++)
  {
    int sync = wp.synchronizations[s];
    EditorObject *with = NULL;
    for (int k=0; k<waypointMap.Size(); k++)
    {
      if (sync == waypointMap[k].sync)
      {
        DoAssert(!waypointMap[k].object2);
        with = waypointMap[k].object1;
        waypointMap[k].object2 = object;
        break;
      }
    }
    if (!with)
    {
      int index = waypointMap.Add();
      waypointMap[index].sync = sync;
      waypointMap[index].object1 = object;
    }
    else
    {
      SetObjectArgument(object, "SYNC", with->GetArgument("VARIABLE_NAME"));
    }
  }
  return object;
}

static void ExportTrigger(EditorWorkspace &ws, const ArcadeSensorInfo &sensor, AutoArray<VehicleInfo> &vehicleMap, AutoArray<SyncInfo> &waypointMap)
{
  TargetSide side = TSideUnknown;
  switch (sensor.type)	// special cases - guarded by ...
  {
  case ASTEastGuarded:
    side = TEast;
    break;
  case ASTWestGuarded:
    side = TWest;
    break;
  case ASTGuerrilaGuarded:
    side = TGuerrila;
    break;
  }
  if (side == TSideUnknown)
  {
    // add real trigger
    EditorObjectType *type = ws.FindObjectType("trigger");
    if (!type)
    {
      RptF("Missing trigger object type");
      return;
    }
    EditorObject *object = ws.AddObject(type);

    SetObjectArgument(object, "POSITION", sensor.position);
    SetObjectArgument(object, "OBJECT", sensor.object);
    SetObjectArgument(object, "A", sensor.a);
    SetObjectArgument(object, "B", sensor.b);
    SetObjectArgument(object, "ANGLE", sensor.angle);
    SetObjectArgument(object, "RECTANGULAR", sensor.rectangular);
    SetObjectArgument(object, "ACTIVATION_BY", sensor.activationBy);
    SetObjectArgument(object, "ACTIVATION_TYPE", sensor.activationType);
    SetObjectArgument(object, "REPEATING", sensor.repeating);
    SetObjectArgument(object, "TYPE", sensor.type);
    SetObjectArgument(object, "TIMEOUT_MIN", sensor.timeoutMin);
    SetObjectArgument(object, "TIMEOUT_MID", sensor.timeoutMid);
    SetObjectArgument(object, "TIMEOUT_MAX", sensor.timeoutMax);
    SetObjectArgument(object, "INTERRUPTABLE", sensor.interruptable);
    // TODO: use link to object instead
    SetObjectArgument(object, "ID_STATIC", sensor.idObject);
    if (sensor.idVehicle >= 0)
    {
      bool found = false;
      for (int i=0; i<vehicleMap.Size(); i++)
      {
        if (sensor.idVehicle == vehicleMap[i].id)
        {
          SetObjectArgument(object, "VEHICLE", vehicleMap[i].object->GetArgument("VARIABLE_NAME"));
          found = true;
          break;
        }
      }
      DoAssert(found);
    }
    SetObjectArgument(object, "TEXT", sensor.text);
    SetObjectArgument(object, "EXP_COND", sensor.expCond);
    SetObjectArgument(object, "EXP_ACTIV", sensor.expActiv);
    SetObjectArgument(object, "EXP_DESACTIV", sensor.expDesactiv);
    SetObjectArgument(object, "AGE", sensor.age);
    SetObjectArgument(object, "NAME", sensor.name);
    ExportEffects(object, sensor.effects);

    // synchronizations
    for (int s=0; s<sensor.synchronizations.Size(); s++)
    {
      int sync = sensor.synchronizations[s];
      EditorObject *wp = NULL;
      for (int k=0; k<waypointMap.Size(); k++)
      {
        if (sync == waypointMap[k].sync)
        {
          DoAssert(!waypointMap[k].object2);
          wp = waypointMap[k].object1;
          waypointMap[k].object2 = object;
          break;
        }
      }
      DoAssert(wp);
      SetObjectArgument(object, "SYNC", wp->GetArgument("VARIABLE_NAME"));
    }
  }
  else
  {
    // add guarded point
    EditorObjectType *type = ws.FindObjectType("guardedPoint");
    if (!type)
    {
      RptF("Missing guardedPoint object type");
      return;
    }
    EditorObject *object = ws.AddObject(type);

    SetObjectArgument(object, "POSITION", sensor.position);
    SetObjectArgument(object, "SIDE", side);
    // TODO: use link to object instead
    SetObjectArgument(object, "ID_STATIC", sensor.idObject);
    if (sensor.idVehicle >= 0)
    {
      bool found = false;
      for (int i=0; i<vehicleMap.Size(); i++)
      {
        if (sensor.idVehicle == vehicleMap[i].id)
        {
          SetObjectArgument(object, "VEHICLE", vehicleMap[i].object->GetArgument("VARIABLE_NAME"));
          found = true;
          break;
        }
      }
      DoAssert(found);
    }
  }
}

void ExportMission(ArcadeTemplate &t, RString filename)
{
  EditorWorkspace ws;

  // addons
  EditorObject *object = ws.GetPrefixObject();
  for (int i=0; i<t.addOns.Size(); i++) SetObjectArgument(object, "ADDON", t.addOns[i]);
  
  // helper structures for fast searching
  AutoArray<CenterInfo> centerMap;
  AutoArray<MarkerInfo> markerMap;
  AutoArray<VehicleInfo> vehicleMap;
  AutoArray<SyncInfo> waypointMap;
  AutoArray<EditorObject *> groupMap;

  // intel
  ExportIntel(ws, t.intel);

  // centers
  for (int i=0; i<TSideUnknown; i++)
  {
    // select only nonempty
    bool found = false;
    for (int j=0; j<t.groups.Size(); j++)
    {
      if (t.groups[j].side == i)
      {
        found = true;
        break;
      }
    }
    if (found)
    {
      int index = centerMap.Add();
      centerMap[index].side = (TargetSide)i;
      centerMap[index].object = ExportCenter(ws, (TargetSide)i, t.intel);
    }
  }
  {
    // game logic
    bool found = false;
    for (int j=0; j<t.groups.Size(); j++)
    {
      if (t.groups[j].side == TLogic)
      {
        found = true;
        break;
      }
    }
    if (found)
    {
      int index = centerMap.Add();
      centerMap[index].side = TLogic;
      centerMap[index].object = ExportCenter(ws, TLogic, t.intel);
    }
  }

  // markers
  for (int i=0; i<t.markers.Size(); i++)
  {
    int index = markerMap.Add();
    markerMap[index].name = t.markers[i].name;
    markerMap[index].object = ExportMarker(ws, t.markers[i]);
  }

  // groups, units
  groupMap.Realloc(t.groups.Size());
  groupMap.Resize(t.groups.Size());
  for (int i=0; i<t.groups.Size(); i++)
  {
    const ArcadeGroupInfo &grpInfo = t.groups[i];

    EditorObject *center = NULL;
    for (int j=0; j<centerMap.Size(); j++)
      if (centerMap[j].side == grpInfo.side)
      {
        center = centerMap[j].object;
        break;
      }
    Assert(center);
    EditorObject *group = ExportGroup(ws, center, grpInfo);
    groupMap[i] = group;

    // units (except cargo)
    for (int j=0; j<grpInfo.units.Size(); j++)
    {
      const ArcadeUnitInfo &unit = grpInfo.units[j];
      if (unit.special == ASpCargo) continue;

      Ref<EntityType> type = VehicleTypes.New(unit.vehicle);
      TransportType *transportType = dynamic_cast<TransportType *>(type.GetRef());

      if (transportType)
      {
        EditorObject *vehicle = ExportVehicle(ws, unit, markerMap);
        Assert(vehicle);
        if (unit.id >= 0)
        {
          int index = vehicleMap.Add();
          vehicleMap[index].id = unit.id;
          vehicleMap[index].object = vehicle;
        }

        // crew
        RString crewType = transportType->GetCrew();
        int commanderOffset = 1;
        int gunnerOffset = -1;
        // for vehicle where gunner is commander gunner rank
        // should be higher that driver
        bool driverIsCommander = transportType->DriverIsCommander();
        if (!driverIsCommander)
        {
          commanderOffset = 2;
          gunnerOffset = 1;
        }

        bool hasCommander = transportType->HasObserver();
        bool hasDriver = transportType->HasDriver();
        bool hasGunner = transportType->HasGunner();

        bool leaderCommander = false;
        bool leaderDriver = false;
        bool leaderGunner = false;
        if (unit.leader)
        {
          if (hasCommander) leaderCommander = true;
          else if (driverIsCommander)
          {
            if (hasDriver) leaderDriver = true;
            else leaderGunner = true;
          }
          else
          {
            if (hasGunner) leaderGunner = true;
            else leaderDriver = true;
          }
        }
        if (hasDriver)
        {
          ExportCrew(ws, unit, group, vehicle, crewType, unit.rank, GIPDriver, unit.player == APPlayerDriver, leaderDriver);
        }
        if (hasCommander)
        {
          int commanderRank = unit.rank + commanderOffset;
          saturate(commanderRank, 0, NRanks - 1);
          ExportCrew(ws, unit, group, vehicle, crewType, (Rank)commanderRank, GIPCommander, unit.player == APPlayerCommander, leaderCommander);
        }
        if (hasGunner)
        {
          int gunnerRank = unit.rank + gunnerOffset;
          saturate(gunnerRank, 0, NRanks - 1);
          ExportCrew(ws, unit, group, vehicle, crewType, (Rank)gunnerRank, GIPGunner, unit.player == APPlayerGunner, leaderGunner);
        }
      }
      else if (type->IsPerson())
      {
        EditorObject *u = ExportUnit(ws, unit, group, markerMap);
        if (unit.id >= 0)
        {
          int index = vehicleMap.Add();
          vehicleMap[index].id = unit.id;
          vehicleMap[index].object = u;
        }
      }
      else
      {
        ErrF("Unit in group is not Man nor Transport");
        EditorObject *vehicle = ExportVehicle(ws, unit, markerMap);
        Assert(vehicle);
        if (unit.id >= 0)
        {
          int index = vehicleMap.Add();
          vehicleMap[index].id = unit.id;
          vehicleMap[index].object = vehicle;
        }
      }
    }

    // units (cargo)
    for (int j=0; j<grpInfo.units.Size(); j++)
    {
      const ArcadeUnitInfo &unit = grpInfo.units[j];
      if (unit.special != ASpCargo) continue;

      // only Man can be in cargo
      Ref<EntityType> type = VehicleTypes.New(unit.vehicle);
      DoAssert(dynamic_cast<ManType *>(type.GetRef()));

      EditorObject *u = ExportUnit(ws, unit, group, markerMap);
      if (unit.id >= 0)
      {
        int index = vehicleMap.Add();
        vehicleMap[index].id = unit.id;
        vehicleMap[index].object = u;
      }
    }
  }

  // empty vehicles
  for (int i=0; i<t.emptyVehicles.Size(); i++)
  {
    const ArcadeUnitInfo &unit = t.emptyVehicles[i];
    EditorObject *vehicle = ExportVehicle(ws, unit, markerMap);
    Assert(vehicle);
    if (unit.id >= 0)
    {
      int index = vehicleMap.Add();
      vehicleMap[index].id = unit.id;
      vehicleMap[index].object = vehicle;
    }
  }

  // waypoints
  for (int i=0; i<t.groups.Size(); i++)
  {
    const ArcadeGroupInfo &grpInfo = t.groups[i];
    EditorObject *group = groupMap[i];
    EditorObject *prev = NULL;

    for (int j=0; j<grpInfo.waypoints.Size(); j++)
    {
      prev = ExportWaypoint(ws, grpInfo.waypoints[j], group, vehicleMap, waypointMap, prev);
    }
  }

  // sensors
  for (int i=0; i<t.sensors.Size(); i++)
    ExportTrigger(ws, t.sensors[i], vehicleMap, waypointMap);

  ws.WriteCreateScript(filename + RString(".sqf"));
  ws.Save(filename + RString(".biedi"));
}

#endif // _ENABLE_EDITOR2