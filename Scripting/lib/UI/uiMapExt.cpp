// Implementation of mission editor
#include "../wpch.hpp"
#include "uiMap.hpp"
#include <Es/Common/win.h>
#include "../dikCodes.h"

#include "resincl.hpp"
#include "../keyInput.hpp"
#include <El/QStream/qbStream.hpp>
#include <El/ParamArchive/paramArchiveDb.hpp>

#include "../landscape.hpp"
#include "../txtPreload.hpp"
#include "../fileLocator.hpp"

#include <El/QStream/packFiles.hpp>
#include "../progress.hpp"

#if defined _WIN32 && !defined _XBOX
#include <mapi.h>
#endif

#include <El/Debugging/debugTrap.hpp>
#include "../stringtableExt.hpp"

#include <Es/Strings/bString.hpp>

#include <El/Clipboard/clipboard.hpp>

#include <El/PreprocC/preprocC.hpp>

#include "missionDirs.hpp"
#include "../saveVersion.hpp"
#include "../gameDirs.hpp"

/*!
\file
Implementation file for particular displays (maps, briefing, debriefing, mission editor).
*/

ArcadeTemplate GClipboard;

void CStaticMap::DrawEllipse(
  Vector3Val position, float a, float b, float angle, PackedColor color,
  float dashSolid, float dashGap
)
{
	DrawCoord posMap = WorldToScreen(position);
	float cx = posMap.x * _wScreen;
	float cy = posMap.y * _hScreen;

	const float invSizeLand = InvLandSize;
	float aMap = a * invSizeLand * _invScaleX * _wScreen;
	float bMap = b * invSizeLand * _invScaleY * _hScreen;
	
	// TODO: improve clipping
	float r = floatMax(aMap, bMap);
	if (cx + r < _clipRect.x) return;
	if (cy + r < _clipRect.y) return;
	if (cx - r > _clipRect.x + _clipRect.w) return;
	if (cy - r > _clipRect.y + _clipRect.h) return;

	float s = sin(angle);
	float c = cos(angle);

	static const float linePts = 5.0f;
	int nSteps = toIntCeil(2 * H_PI * aMap * (1.0f / linePts));
	saturate(nSteps, 6, 720);
  bool dashy = false;
  if (dashGap>0)
  {
    // TODO: more flexible dashing
    nSteps = (nSteps+1)/2*2;
    dashy = true;
  }
	float step = 2 * H_PI * (1.0f / nSteps);
	float lastX=cx, lastY=cy;
	bool lastValid = false;
	float st = 0;
	float ct = 1;
	float sstep = sin(step);
	float cstep = cos(step);
  // alternate solid/gap when dashy, stay true otherwise
  bool dash = true;
	for (float t=0; t<2*H_PI + 0.5 * step; t+=step)
	{
		float sta = st * aMap;
		float ctb = ct * bMap;
		float x = cx + c * sta + s * ctb;
		float y = cy + s * sta - c * ctb;
		if (lastValid)
		{
			if (dash) GEngine->DrawLine(
				Line2DPixel(lastX, lastY, x, y),
				color, color, _clipRect
			);
      if (dashy) dash = !dash;
		}
		lastX = x;
		lastY = y;
		lastValid = true;
		float stnew = st * cstep + ct * sstep;
		float ctnew = ct * cstep - st * sstep;
		st = stnew; ct = ctnew;
	}
}

void CStaticMap::DrawRectangle(Vector3Val position, float a, float b, float angle, PackedColor color)
{
	DrawCoord posMap = WorldToScreen(position);
	float cx = posMap.x * _wScreen;
	float cy = posMap.y * _hScreen;

	const float invSizeLand = InvLandSize;
	float aMap = a * invSizeLand * _invScaleX * _wScreen;
	float bMap = b * invSizeLand * _invScaleY * _hScreen;

	// TODO: improve clipping
	float r = sqrt(Square(aMap) + Square(bMap));
	if (cx + r < _clipRect.x) return;
	if (cy + r < _clipRect.y) return;
	if (cx - r > _clipRect.x + _clipRect.w) return;
	if (cy - r > _clipRect.y + _clipRect.h) return;

	float s = sin(angle);
	float c = cos(angle);

	float x1 = cx + c * aMap + s * bMap;
	float y1 = cy + s * aMap - c * bMap;
	float x2 = cx + c * aMap + s * (-bMap);
	float y2 = cy + s * aMap - c * (-bMap);
	float x3 = cx + c * (-aMap) + s * (-bMap);
	float y3 = cy + s * (-aMap) - c * (-bMap);
	float x4 = cx + c * (-aMap) + s * bMap;
	float y4 = cy + s * (-aMap) - c * bMap;

	GLOB_ENGINE->DrawLine(Line2DPixel(x1, y1, x2, y2), color, color, _clipRect);
	GLOB_ENGINE->DrawLine(Line2DPixel(x2, y2, x3, y3), color, color, _clipRect);
	GLOB_ENGINE->DrawLine(Line2DPixel(x3, y3, x4, y4), color, color, _clipRect);
	GLOB_ENGINE->DrawLine(Line2DPixel(x4, y4, x1, y1), color, color, _clipRect);
}

inline PackedColor ModAlpha( PackedColor color, float alpha )
{
	int a=toIntFloor(alpha*color.A8());
	saturate(a,0,255);
	return PackedColorRGB(color,a);
}

const static int NoClamp2D=(DefSpecFlags2D|NoClamp)&~(ClampU|ClampV);

void CStaticMap::FillEllipse(Vector3Val position, float a, float b, float angle, PackedColor color, Texture *texture)
{
	DrawCoord posMap = WorldToScreen(position);
	float cx = posMap.x * _wScreen;
	float cy = posMap.y * _hScreen;

	const float invSizeLand = InvLandSize;
	float aMap = a * invSizeLand * _invScaleX * _wScreen;
	float bMap = b * invSizeLand * _invScaleY * _hScreen;
	
	// TODO: improve clipping
	float r = floatMax(aMap, bMap);
	if (cx + r < _clipRect.x) return;
	if (cy + r < _clipRect.y) return;
	if (cx - r > _clipRect.x + _clipRect.w) return;
	if (cy - r > _clipRect.y + _clipRect.h) return;

	float s = sin(angle);
	float c = cos(angle);

	PackedColor fillColor = color;

	float coefU = 0;
	float coefV = 0;
	float offsetX = (_mapX + _x) * _wScreen;
	float offsetY = (_mapY + _y) * _hScreen;
	if (texture)
	{
		coefU = 1.0 / texture->AWidth();
		coefV = 1.0 / texture->AHeight();
	}
	else
	{
		// special case
		fillColor = ModAlpha(color, 0.5);
	}
	MipInfo mip = GEngine->TextBank()->UseMipmap(texture, 0, 0);

	const int n = 3;
	Vertex2DPixel vs[n];
	// 0
	vs[0].x = cx;
	vs[0].y = cy;
	vs[0].u = coefU * (vs[0].x - offsetX);
	vs[0].v = coefV * (vs[0].y - offsetY);
	vs[0].color = fillColor;
	// 1
	vs[1].color = fillColor;
	// 2
	vs[2].color = fillColor;

	static const float linePts = 5.0f;
	int nSteps = toIntCeil(2 * H_PI * aMap * (1.0f / linePts));
	saturate(nSteps, 6, 720);
	float step = 2 * H_PI * (1.0f / nSteps);
	float lastX = cx, lastY = cy;
	bool lastValid = false;
	float st = 0;
	float ct = 1;
	float sstep = sin(step);
	float cstep = cos(step);
	for (float t=0; t<2*H_PI + 0.5 * step; t+=step)
	{
		float sta = st * aMap;
		float ctb = ct * bMap;
		float x = cx + c * sta + s * ctb;
		float y = cy + s * sta - c * ctb;
		if (lastValid)
		{
			// 1
			vs[1].x = lastX;
			vs[1].y = lastY;
			vs[1].u = coefU * (vs[1].x - offsetX);
			vs[1].v = coefV * (vs[1].y - offsetY);
			// 2
			vs[2].x = x;
			vs[2].y = y;
			vs[2].u = coefU * (vs[2].x - offsetX);
			vs[2].v = coefV * (vs[2].y - offsetY);
			GEngine->DrawPoly(mip, vs, n, _clipRect,NoClamp2D);
		}
		lastX = x;
		lastY = y;
		lastValid = true;
		float stnew = st * cstep + ct * sstep;
		float ctnew = ct * cstep - st * sstep;
		st = stnew; ct = ctnew;
	}
}

void CStaticMap::FillRectangle(Vector3Val position, float a, float b, float angle, PackedColor color, Texture *texture)
{
	DrawCoord posMap = WorldToScreen(position);
	float cx = posMap.x * _wScreen;
	float cy = posMap.y * _hScreen;

	const float invSizeLand = InvLandSize;
	float aMap = a * invSizeLand * _invScaleX * _wScreen;
	float bMap = b * invSizeLand * _invScaleY * _hScreen;

	// TODO: improve clipping
	float r = sqrt(Square(aMap) + Square(bMap));
	if (cx + r < _clipRect.x) return;
	if (cy + r < _clipRect.y) return;
	if (cx - r > _clipRect.x + _clipRect.w) return;
	if (cy - r > _clipRect.y + _clipRect.h) return;

	float s = sin(angle);
	float c = cos(angle);

	PackedColor fillColor = color;

	float coefU = 0;
	float coefV = 0;
	float offsetX = (_mapX + _x) * _wScreen;
	float offsetY = (_mapY + _y) * _hScreen;
	if (texture)
	{
		coefU = 1.0 / texture->AWidth();
		coefV = 1.0 / texture->AHeight();
	}
	else
	{
		// special case
		fillColor = ModAlpha(color, 0.5);
	}

	const int n = 4;
	Vertex2DPixel vs[n];
	// 0
	vs[0].x = cx + c * aMap + s * bMap;
	vs[0].y = cy + s * aMap - c * bMap;
	vs[0].u = coefU * (vs[0].x - offsetX);
	vs[0].v = coefV * (vs[0].y - offsetY);
	vs[0].color = fillColor;
	// 1
	vs[1].x = cx + c * aMap + s * (-bMap);
	vs[1].y = cy + s * aMap - c * (-bMap);
	vs[1].u = coefU * (vs[1].x - offsetX);
	vs[1].v = coefV * (vs[1].y - offsetY);
	vs[1].color = fillColor;
	// 2
	vs[2].x = cx + c * (-aMap) + s * (-bMap);
	vs[2].y = cy + s * (-aMap) - c * (-bMap);
	vs[2].u = coefU * (vs[2].x - offsetX);
	vs[2].v = coefV * (vs[2].y - offsetY);
	vs[2].color = fillColor;
	// 3
	vs[3].x = cx + c * (-aMap) + s * bMap;
	vs[3].y = cy + s * (-aMap) - c * bMap;
	vs[3].u = coefU * (vs[3].x - offsetX);
	vs[3].v = coefV * (vs[3].y - offsetY);
	vs[3].color = fillColor;

	MipInfo mip = GEngine->TextBank()->UseMipmap(texture, 0, 0);
	GEngine->DrawPoly(mip, vs, n, _clipRect, NoClamp2D);
}

///////////////////////////////////////////////////////////////////////////////
// Arcade Map control

#if _ENABLE_EDITOR

CStaticMapArcadeViewer:: CStaticMapArcadeViewer
(
	ControlsContainer *parent, int idc, ParamEntryPar cls,
	float scaleMin, float scaleMax, float scaleDefault
)
: CStaticMap(parent, idc, cls, scaleMin, scaleMax, scaleDefault)
{
	_sizeEmptyMarker = 32;
}

AIBrain *CStaticMapArcadeViewer::GetMyUnit()
{
	return NULL;
}

AICenter *CStaticMapArcadeViewer::GetMyCenter()
{
	return NULL;
}

// drawing

TypeIsSimple(DrawCoord)

void CStaticMapArcadeViewer::DrawExt(float alpha)
{
	CStaticMap::DrawExt(alpha);

#if _ENABLE_CHEATS
	if (!_show) return;
#endif

	// update waypoint positions
	int i, n = _template->groups.Size();
	for (i=0; i<n; i++)
	{
		ArcadeGroupInfo &gInfo = _template->groups[i];
		int j, m = gInfo.waypoints.Size();
		for (j=0; j<m; j++)
		{
			ArcadeWaypointInfo &wpInfo = gInfo.waypoints[j];
			if (wpInfo.id >= 0)
			{
				int idGroup, idUnit;
				ArcadeUnitInfo *uInfo = _template->FindUnit(wpInfo.id, idGroup, idUnit);
				if (uInfo)
				{
					wpInfo.position = uInfo->position;
				}
			}
		}
	}

	// draw synchronizations
	AutoArray< AutoArray<DrawCoord> > synchroMap;
	AutoArray< AutoArray<bool> > synchroSel;
	synchroMap.Resize(_template->nextSyncId);
	synchroSel.Resize(_template->nextSyncId);
	for (i=0; i<n; i++)
	{
		ArcadeGroupInfo &gInfo = _template->groups[i];
		int j, m = gInfo.waypoints.Size();
		for (j=0; j<m; j++)
		{
			ArcadeWaypointInfo &wpInfo = gInfo.waypoints[j];
			bool isActive1 = wpInfo.selected;
			DrawCoord ptMap = WorldToScreen(wpInfo.position);
			ptMap.x *= _wScreen;
			ptMap.y *= _hScreen;
			int p = wpInfo.synchronizations.Size();
			for (int l=0; l<p; l++)
			{
				int sync = wpInfo.synchronizations[l];
				Assert(sync >= 0);
				int o = synchroMap[sync].Size();
				for (int k=0; k<o; k++)
				{
					bool isActive2 = synchroSel[sync][k];

					PackedColor color = isActive1 || isActive2 ? _colorSync : InactiveColor(_colorSync);
					GLOB_ENGINE->DrawLine
					(
						Line2DPixel(ptMap.x, ptMap.y,
						synchroMap[sync][k].x, synchroMap[sync][k].y),
						color, color, _clipRect
					);
				}
				synchroMap[sync].Add(ptMap);
				synchroSel[sync].Add(isActive1);
			}
		}
	}
	for (i=0; i<n; i++)
	{
		ArcadeGroupInfo &gInfo = _template->groups[i];
		int j, m = gInfo.sensors.Size();
		for (j=0; j<m; j++)
		{
			ArcadeSensorInfo &sInfo = gInfo.sensors[j];
			bool isActive1 = sInfo.selected;
			DrawCoord ptMap = WorldToScreen(sInfo.position);
			ptMap.x *= _wScreen;
			ptMap.y *= _hScreen;
			int p = sInfo.synchronizations.Size();
			for (int l=0; l<p; l++)
			{
				int sync = sInfo.synchronizations[l];
				Assert(sync >= 0);
				int o = synchroMap[sync].Size();
				for (int k=0; k<o; k++)
				{
					bool isActive2 = synchroSel[sync][k];
					PackedColor color = isActive1 || isActive2 ? _colorSync : InactiveColor(_colorSync);
					GLOB_ENGINE->DrawLine
					(
						Line2DPixel(ptMap.x, ptMap.y,
						synchroMap[sync][k].x, synchroMap[sync][k].y),
						color, color, _clipRect
					);
				}
			}
		}
	}
	int j, m = _template->sensors.Size();
	for (j=0; j<m; j++)
	{
		ArcadeSensorInfo &sInfo = _template->sensors[j];
		bool isActive1 = sInfo.selected;
		DrawCoord ptMap = WorldToScreen(sInfo.position);
		ptMap.x *= _wScreen;
		ptMap.y *= _hScreen;
		int p = sInfo.synchronizations.Size();
		for (int l=0; l<p; l++)
		{
			int sync = sInfo.synchronizations[l];
			Assert(sync >= 0);
			int k, o = synchroMap[sync].Size();
			for (k=0; k<o; k++)
			{
				bool isActive2 = synchroSel[sync][k];
				PackedColor color = isActive1 || isActive2 ? _colorSync : InactiveColor(_colorSync);
				GLOB_ENGINE->DrawLine
				(
					Line2DPixel(ptMap.x, ptMap.y,
					synchroMap[sync][k].x, synchroMap[sync][k].y),
					color, color, _clipRect
				);
			}
		}
	}

	// draw units / waypoints		
	for (i=0; i<n; i++)
	{
		ArcadeGroupInfo &gInfo = _template->groups[i];
		bool activeGroup = false;
		for (int j=0; j<gInfo.units.Size(); j++)
			if (gInfo.units[j].selected)
			{
				activeGroup = true; break;
			}
		if (!activeGroup) for (int j=0; j<gInfo.waypoints.Size(); j++)
			if (gInfo.waypoints[j].selected)
			{
				activeGroup = true; break;
			}

		DrawCoord ptLeader(0, 0);
		bool leaderVisible = false;
		int j, m = gInfo.units.Size();
		for (j=0; j<m; j++)
		{
			ArcadeUnitInfo &uInfo = gInfo.units[j];
			if (uInfo.leader)
			{
				ptLeader = WorldToScreen(uInfo.position);
				ptLeader.x *= _wScreen;
				ptLeader.y *= _hScreen;
				leaderVisible = HasFullRights() || uInfo.side == Glob.header.playerSide || uInfo.age != AAUnknown;
				break;
			}
		}
		 
		// Draw waypoints
		if (leaderVisible)
		{
			DrawCoord pt1 = ptLeader;
			m = gInfo.waypoints.Size();
			for (j=0; j<m; j++)
			{
				ArcadeWaypointInfo &wInfo = gInfo.waypoints[j];
				// draw line
				PackedColor color = activeGroup ? _colorActiveMission : InactiveColor(_colorActiveMission);
				DrawCoord pt2 = WorldToScreen(wInfo.position);
				pt2.x *= _wScreen;
				pt2.y *= _hScreen;
				GLOB_ENGINE->DrawLine
				(
					Line2DPixel(pt1.x, pt1.y, pt2.x, pt2.y),
					color, color, _clipRect
				);
				// arrow
				float dx = pt2.x - pt1.x;
				float dy = pt2.y - pt1.y;
				float size2 = Square(dx) + Square(dy);
        float invSize = size2 <= FLT_MIN ? 0 : InvSqrt(size2);
				dx *= 12.0 * invSize; dy *= 12.0 * invSize;
				float x = pt2.x - dx + 0.5 * dy;
				float y = pt2.y - dy - 0.5 * dx;
				GEngine->DrawLine
				(
					Line2DPixel(pt2.x, pt2.y, x, y),
					color, color, _clipRect
				);
				x = pt2.x - dx - 0.5 * dy;
				y = pt2.y - dy + 0.5 * dx;
				GEngine->DrawLine
				(
					Line2DPixel(pt2.x, pt2.y, x, y),
					color, color, _clipRect
				);
				pt1 = pt2;
			}
			for (j=0; j<m; j++)
			{
				ArcadeWaypointInfo &wInfo = gInfo.waypoints[j];
				PackedColor color = wInfo.selected ?
					_infoWaypoint.color : InactiveColor(_infoWaypoint.color);
				DrawCoord pt = WorldToScreen(wInfo.position);
				DrawSign
				(
					_infoWaypoint.icon, color, pt,
					_infoWaypoint.size, _infoWaypoint.size, 0
				);
				if (wInfo.placement > 0)
					DrawEllipse(wInfo.position, wInfo.placement, wInfo.placement, 0, color, 5,3);
				if (wInfo.completitionRadius > 0)
					DrawEllipse(wInfo.position, wInfo.completitionRadius, wInfo.completitionRadius, 0, color);
				if (wInfo.HasEffect())
				{
					PackedColor color = activeGroup ?
						_colorCamera : InactiveColor(_colorCamera);
					pt.x += 0.02;
					DrawSign(_iconCamera, color, pt, 12, 12, 0);
				}
			}
		}

		// Draw sensors
		m = gInfo.sensors.Size();
		if (leaderVisible) for (j=0; j<m; j++)
		{
			ArcadeSensorInfo &sInfo = gInfo.sensors[j];
			Assert(sInfo.idVisitorObj == VisitorObjId());
			Assert(sInfo.idVehicle == -1);
			
			PackedColor color;
			if (sInfo.selected)
			{
				color = _colorActiveGroup;
			}
			else
			{
				color = _colorGroups;
			}

			// draw line
			DrawCoord pt = WorldToScreen(sInfo.position);
			pt.x *= _wScreen;
			pt.y *= _hScreen;
			GLOB_ENGINE->DrawLine
			(
				Line2DPixel(ptLeader.x, ptLeader.y, pt.x, pt.y),
				color, color, _clipRect
			);
		}

		for (j=0; j<m; j++)
		{
			ArcadeSensorInfo &sInfo = gInfo.sensors[j];

			PackedColor color = _colorSensor;
			if (!sInfo.selected)
			{
				color.SetA8(color.A8() / 2);
			}
			
			if (sInfo.rectangular)
				DrawRectangle(sInfo.position, sInfo.a, sInfo.b, sInfo.angle * (H_PI / 180.0), color);
			else
				DrawEllipse(sInfo.position, sInfo.a, sInfo.b, sInfo.angle * (H_PI / 180.0), color);
			DrawSign
			(
				_iconSensor, color,
				sInfo.position,
				16, 16, 0
			);
		}

		// draw groups
		m = gInfo.units.Size();
		if (leaderVisible) for (j=0; j<m; j++)
		{
			ArcadeUnitInfo &uInfo = gInfo.units[j];
			if (uInfo.leader) continue;
			PackedColor color = activeGroup ? _colorActiveGroup : _colorGroups;
			DrawCoord pt = WorldToScreen(uInfo.position);
			pt.x *= _wScreen;
			pt.y *= _hScreen;
			GLOB_ENGINE->DrawLine
			(
				Line2DPixel(ptLeader.x, ptLeader.y, pt.x, pt.y),
				color, color, _clipRect
			);
		}

		// draw units
		for (j=0; j<m; j++)
		{
			ArcadeUnitInfo &uInfo = gInfo.units[j];
			if
			(
				uInfo.side != Glob.header.playerSide &&
				uInfo.age == AAUnknown &&
				!HasFullRights()
			) continue;

			DrawCoord pt = WorldToScreen(uInfo.position);
			DrawCoord pt1(pt.x * _wScreen, pt.y * _hScreen);

			bool activeUnit = uInfo.selected;
      PackedColor lineColor = activeUnit ? _colorActiveGroup : _colorGroups;

			int o = uInfo.markers.Size();
			for (int k=0; k<o; k++)
			{
				ArcadeMarkerInfo *mInfo = _template->FindMarker(uInfo.markers[k]);
				if (mInfo)
				{
					DrawCoord pt2 = WorldToScreen(mInfo->position);
					pt2.x *= _wScreen;
					pt2.y *= _hScreen;
					GLOB_ENGINE->DrawLine
					(
						Line2DPixel(pt1.x, pt1.y, pt2.x, pt2.y),
						lineColor, lineColor, _clipRect
					);
				}
			}

      // unit - unit synchronizations
      for (int i=0; i<uInfo.synchronizations.Size(); i++)
      {
        int id = uInfo.synchronizations[i];
        if (id < uInfo.id) continue; // synchronizations are symmetric, draw only once
        int indexUnit, indexGroup;
        ArcadeUnitInfo *unitWith = _template->FindUnit(id, indexUnit, indexGroup);
        if (unitWith)
        {
          DrawCoord pt2 = WorldToScreen(unitWith->position);
          pt2.x *= _wScreen;
          pt2.y *= _hScreen;
          PackedColor color = activeUnit || unitWith->selected ? _colorSync : InactiveColor(_colorSync);
          GEngine->DrawLine(
            Line2DPixel(pt1.x, pt1.y, pt2.x, pt2.y),
            color, color, _clipRect);
        }
      }

			PackedColor color;
			if (uInfo.side == TEmpty)
				color = _colorUnknown;
			else if (uInfo.side == TLogic || uInfo.side == TAmbientLife)
				color = _colorCivilian;
      else if(uInfo.side == TCivilian)
				color = _colorNeutral;
			else
			{
        int index = Glob.header.playerSide;
        saturateMin(index, TGuerrila); // for civilians, use resistance friendship
				//float friends = _template->intel.friends[index][uInfo.side];
        switch (uInfo.side)
        {
        case TWest:
          {
            color = _colorCivilian;
            break;
          }
        case TEast:
          {
            color = _colorEnemy;
            break;
          }
        case TGuerrila:
          {
            color = _colorFriendly;
            break;
          }
        default:
          {
            color = _colorNeutral;
            break;
          }
        }
				//if (friends >= 0.5)
				//	color = _colorFriendly;
				//else
				//	color = _colorEnemy;
			}
			if (!activeUnit)
			{
				color.SetA8(color.A8() / 2);
			}

			const float invSizeLand = InvLandSize;
			float size = uInfo.size * invSizeLand * _invScaleX * 640;
			saturateMax(size, _sizeLeader);

			if (uInfo.icon)
				DrawSign
				(
					uInfo.icon, color,
					pt,
					size, size, HDegree(uInfo.azimut), RString(), false, -1, false,  _shadow
				);
			if (uInfo.placement > 0)
				DrawEllipse(uInfo.position, uInfo.placement, uInfo.placement, 0, color, 5,3);
			switch (uInfo.player)
			{
			case APNonplayable:
				break;
			case APPlayerCommander:
			case APPlayerDriver:
			case APPlayerGunner:
				DrawSign
				(
					_iconPlayer, _colorMe,
					pt,
					_sizePlayer, _sizePlayer, 0
				);
				break;
			case APPlayableC:
			case APPlayableD:
			case APPlayableG:
			case APPlayableCD:
			case APPlayableCG:
			case APPlayableDG:
			case APPlayableCDG:
				DrawSign
				(
					_iconPlayer, _colorPlayable,
					pt,
					_sizePlayer, _sizePlayer, 0
				);
				break;
			}
		}
	}

	// draw empty vehicles
	n = _template->emptyVehicles.Size();
	for (i=0; i<n; i++)
	{
		ArcadeUnitInfo &uInfo = _template->emptyVehicles[i];
		if
		(
			uInfo.age == AAUnknown &&
			!HasFullRights()
		) continue;

		Assert(uInfo.side == TEmpty || uInfo.side == TAmbientLife);

		bool activeUnit = uInfo.selected;
    PackedColor lineColor = activeUnit ? _colorActiveGroup : _colorGroups;

		DrawCoord pt = WorldToScreen(uInfo.position);
		DrawCoord pt1(pt.x * _wScreen, pt.y * _hScreen);
		int o = uInfo.markers.Size();
		for (int k=0; k<o; k++)
		{
			ArcadeMarkerInfo *mInfo = _template->FindMarker(uInfo.markers[k]);
			if (mInfo)
			{
				DrawCoord pt2 = WorldToScreen(mInfo->position);
				pt2.x *= _wScreen;
				pt2.y *= _hScreen;
				GLOB_ENGINE->DrawLine
				(
					Line2DPixel(pt1.x, pt1.y, pt2.x, pt2.y),
					lineColor, lineColor, _clipRect
				);
			}
		}

    // unit - unit synchronizations
    for (int i=0; i<uInfo.synchronizations.Size(); i++)
    {
      int id = uInfo.synchronizations[i];
      if (id < uInfo.id) continue; // synchronizations are symmetric, draw only once
      int indexUnit, indexGroup;
      ArcadeUnitInfo *unitWith = _template->FindUnit(id, indexUnit, indexGroup);
      if (unitWith)
      {
        DrawCoord pt2 = WorldToScreen(unitWith->position);
        pt2.x *= _wScreen;
        pt2.y *= _hScreen;
        PackedColor color = activeUnit || unitWith->selected ? _colorSync : InactiveColor(_colorSync);
        GEngine->DrawLine(
          Line2DPixel(pt1.x, pt1.y, pt2.x, pt2.y),
          color, color, _clipRect);
      }
    }

    PackedColor color = uInfo.side == TEmpty ? _colorUnknown : _colorCivilian;
		if (!activeUnit)
			color.SetA8(color.A8() / 2);

		const float invSizeLand = InvLandSize;
		float size = uInfo.size * invSizeLand * _invScaleX * 640;
		saturateMax(size, _sizeLeader);

		if (uInfo.icon)
			DrawSign
			(
				uInfo.icon, color,
				pt,
				size, size, HDegree(uInfo.azimut), RString(), false
			);
		if (uInfo.placement > 0)
			DrawEllipse(uInfo.position, uInfo.placement, uInfo.placement, 0, color, 5,3);
    switch (uInfo.player)
    {
    case APNonplayable:
      break;
    case APPlayerCommander:
    case APPlayerDriver:
    case APPlayerGunner:
      DrawSign(
        _iconPlayer, _colorMe, pt,
        _sizePlayer, _sizePlayer, 0);
      break;
    case APPlayableC:
    case APPlayableD:
    case APPlayableG:
    case APPlayableCD:
    case APPlayableCG:
    case APPlayableDG:
    case APPlayableCDG:
      DrawSign(
        _iconPlayer, _colorPlayable, pt,
        _sizePlayer, _sizePlayer, 0);
      break;
    }
	}

	// draw sensors
	n = _template->sensors.Size();
	for (i=0; i<n; i++)
	{
		ArcadeSensorInfo &sInfo = _template->sensors[i];
		bool active = sInfo.selected;

		if (sInfo.idVisitorObj >= 0)
		{
			sInfo.RepairObjectIds();
			Object *obj = GLandscape->GetObject(sInfo.idObject);
			if (obj)
			{
				PackedColor color = active ? _colorActiveGroup : _colorGroups;
				DrawCoord pt1 = WorldToScreen(sInfo.position);
				pt1.x *= _wScreen;
				pt1.y *= _hScreen;
				DrawCoord pt2 = WorldToScreen(obj->RenderVisualState().Position());
				pt2.x *= _wScreen;
				pt2.y *= _hScreen;
				GLOB_ENGINE->DrawLine
				(
					Line2DPixel(pt1.x, pt1.y, pt2.x, pt2.y),
					color, color, _clipRect
				);
			}
		}
		else if (sInfo.idVehicle >= 0)
		{
			int indexUnit, indexGroup;
			ArcadeUnitInfo *uInfo = _template->FindUnit
			(
				sInfo.idVehicle, indexUnit, indexGroup
			);
			if (uInfo)
			{
				PackedColor color = active ? _colorActiveGroup : _colorGroups;
				DrawCoord pt1 = WorldToScreen(sInfo.position);
				pt1.x *= _wScreen;
				pt1.y *= _hScreen;
				DrawCoord pt2 = WorldToScreen(uInfo->position);
				pt2.x *= _wScreen;
				pt2.y *= _hScreen;
				GLOB_ENGINE->DrawLine
				(
					Line2DPixel(pt1.x, pt1.y, pt2.x, pt2.y),
					color, color, _clipRect
				);
			}
		}

		PackedColor color = _colorSensor;
		if (!active) color.SetA8(color.A8() / 2);

		if (sInfo.rectangular)
			DrawRectangle(sInfo.position, sInfo.a, sInfo.b, sInfo.angle * (H_PI / 180.0), color);
		else
			DrawEllipse(sInfo.position, sInfo.a, sInfo.b, sInfo.angle * (H_PI / 180.0), color);
		DrawSign
		(
			_iconSensor, color,
			sInfo.position,
			16, 16, 0
		);
	}

	// draw markers
	if (GetMode() == IMMarkers)
	{
		n = _template->markers.Size();
		for (i=0; i<n; i++)
		{
			ArcadeMarkerInfo &mInfo = _template->markers[i];
      //set color and update alpha according to marker alpha property 
      PackedColor color = ModAlpha(mInfo.color,mInfo.alpha);
			if (!mInfo.selected)
			{
				color.SetA8(color.A8() / 2);
			}

			switch (mInfo.markerType)
			{
			case MTIcon:
				{
					float size = mInfo.size;
					if (size == 0) size = _sizeEmptyMarker;
					if (!mInfo.icon) break;
					DrawSign
					(
						mInfo.icon, color,
						mInfo.position,
						size * mInfo.a, size * mInfo.b,
						mInfo.angle * (H_PI / 180.0),
						Localize(mInfo.text),NULL, -1, mInfo.shadow
					);
				}
				break;
			case MTRectangle:
          FillRectangle
          (
          mInfo.position, mInfo.a, mInfo.b, mInfo.angle * (H_PI / 180.0),
          color, mInfo.fill
          );
          if(mInfo.drawBorder)
            DrawRectangle(mInfo.position, mInfo.a, mInfo.b, mInfo.angle * (H_PI / 180.0),
            color);
				break;
			case MTEllipse:
          FillEllipse
          (
          mInfo.position, mInfo.a, mInfo.b, mInfo.angle * (H_PI / 180.0),
          color, mInfo.fill
          );
          if(mInfo.drawBorder)
            DrawEllipse(mInfo.position, mInfo.a, mInfo.b, mInfo.angle * (H_PI / 180.0),
            color);
				break;
			}
		}
	}

	DrawLabel(_infoMove, _colorInfoMove,0);
	return;
}

void CStaticMapArcade::DrawExt(float alpha)
{
	CStaticMapArcadeViewer::DrawExt(alpha);

#if _ENABLE_CHEATS
	if (!_show) return;
#endif

	if (_dragging)
	{
		DrawCoord pt;
		if (GetMode() == IMGroups)
		{
			switch (_infoClick._type)
			{
			case signArcadeUnit:
				if (_infoClick._indexGroup >= 0)
				{
					Assert(_infoClick._indexGroup < _template->groups.Size());
					ArcadeGroupInfo &gInfo = _template->groups[_infoClick._indexGroup];
					Assert(_infoClick._index < gInfo.units.Size());
					ArcadeUnitInfo &uInfo = gInfo.units[_infoClick._index];
					pt = WorldToScreen(uInfo.position);
				}
				else
				{
					ArcadeUnitInfo &uInfo = _template->emptyVehicles[_infoClick._index];
					pt = WorldToScreen(uInfo.position);
				}
				break;
			case signArcadeSensor:
				if (_infoClick._indexGroup >= 0)
				{
					ArcadeGroupInfo &gInfo = _template->groups[_infoClick._indexGroup];
					ArcadeSensorInfo &sInfo = gInfo.sensors[_infoClick._index];
					pt = WorldToScreen(sInfo.position);
				}
				else
				{
					ArcadeSensorInfo &sInfo = _template->sensors[_infoClick._index];
					pt = WorldToScreen(sInfo.position);
				}
				break;
			case signArcadeMarker:
				{
					ArcadeMarkerInfo &mInfo = _template->markers[_infoClick._index];
					pt = WorldToScreen(mInfo.position);
				}
				break;
			case signStatic:
				{
					Object *obj = _infoClick._id;
					Assert(obj);
					if (!obj) return;
					pt = WorldToScreen(obj->RenderVisualState().Position());
				}
				break;
			default:
				return;
			}
		}
		else if (GetMode() == IMSynchronize)
		{
			switch (_infoClick._type)
			{
      case signArcadeUnit:
        if (_infoClick._indexGroup >= 0)
        {
          Assert(_infoClick._indexGroup < _template->groups.Size());
          ArcadeGroupInfo &gInfo = _template->groups[_infoClick._indexGroup];
          Assert(_infoClick._index < gInfo.units.Size());
          ArcadeUnitInfo &uInfo = gInfo.units[_infoClick._index];
          pt = WorldToScreen(uInfo.position);
        }
        else
        {
          ArcadeUnitInfo &uInfo = _template->emptyVehicles[_infoClick._index];
          pt = WorldToScreen(uInfo.position);
        }
        break;
			case signArcadeWaypoint:
				{
					ArcadeGroupInfo &gInfo = _template->groups[_infoClick._indexGroup];
					ArcadeWaypointInfo &wInfo = gInfo.waypoints[_infoClick._index];
					pt = WorldToScreen(wInfo.position);
				}
				break;
			case signArcadeSensor:
				if (_infoClick._indexGroup >= 0)
				{
					ArcadeGroupInfo &gInfo = _template->groups[_infoClick._indexGroup];
					ArcadeSensorInfo &sInfo = gInfo.sensors[_infoClick._index];
					pt = WorldToScreen(sInfo.position);
				}
				else
				{
					ArcadeSensorInfo &sInfo = _template->sensors[_infoClick._index];
					pt = WorldToScreen(sInfo.position);
				}
				break;
			default:
				return;
			}
		}
		else return;

		DrawCoord pt2 = WorldToScreen(_special);

		GEngine->DrawLine
		(
			Line2DPixel(pt.x * _wScreen, pt.y * _hScreen,
			pt2.x * _wScreen, pt2.y * _hScreen),
			_colorDragging, _colorDragging, _clipRect
		);
	}
	else if (_selecting)
	{
		PackedColor color = PackedColor(Color(0,1,0,1));
		DrawCoord pt1 = WorldToScreen(_special);
		DrawCoord pt2 = WorldToScreen(_lastPos);
		GEngine->DrawLine
		(
			Line2DPixel(pt1.x * _wScreen, pt1.y * _hScreen,
			pt2.x * _wScreen, pt1.y * _hScreen),
			color, color, _clipRect
		);
		GEngine->DrawLine
		(
			Line2DPixel(pt2.x * _wScreen, pt1.y * _hScreen,
			pt2.x * _wScreen, pt2.y * _hScreen),
			color, color, _clipRect
		);
		GEngine->DrawLine
		(
			Line2DPixel(pt2.x * _wScreen, pt2.y * _hScreen,
			pt1.x * _wScreen, pt2.y * _hScreen),
			color, color, _clipRect
		);
		GEngine->DrawLine
		(
			Line2DPixel(pt1.x * _wScreen, pt2.y * _hScreen,
			pt1.x * _wScreen, pt1.y * _hScreen),
			color, color, _clipRect
		);
	}
}

// simulation

void CStaticMapArcadeViewer::Center(bool soft)
{
	ArcadeUnitInfo *uInfo = _template->FindPlayer();
	Point3 pt = _defaultCenter;
	if (uInfo) pt = uInfo->position;
	CStaticMap::Center(pt, soft);
}

Vector3 GClipboardPos;

void SelectLeader(ArcadeGroupInfo &gInfo);

void CStaticMapArcade::ClipboardDelete()
{
	for (int i=0; i<_template->emptyVehicles.Size();)
	{
		ArcadeUnitInfo &uInfo = _template->emptyVehicles[i];
		if (uInfo.selected)
			_template->emptyVehicles.Delete(i);
		else
			i++;
	}
	for (int i=0; i<_template->sensors.Size();)
	{
		ArcadeSensorInfo &sInfo = _template->sensors[i];
		if (sInfo.selected)
			_template->sensors.Delete(i);
		else
			i++;
	}
	for (int i=0; i<_template->markers.Size();)
	{
		ArcadeMarkerInfo &mInfo = _template->markers[i];
		if (mInfo.selected)
			_template->markers.Delete(i);
		else
			i++;
	}
	for (int i=0; i<_template->groups.Size();)
	{
		ArcadeGroupInfo &gInfo = _template->groups[i];
		for (int j=0; j<gInfo.units.Size();)
		{
			ArcadeUnitInfo &uInfo = gInfo.units[j];
			if (uInfo.selected)
				gInfo.units.Delete(j);
			else
				j++;
		}
		if (gInfo.units.Size() == 0)
		{
			_template->groups.Delete(i);
			continue;
		}
		for (int j=0; j<gInfo.sensors.Size();)
		{
			ArcadeSensorInfo &sInfo = gInfo.sensors[j];
			if (sInfo.selected)
				gInfo.sensors.Delete(j);
			else
				j++;
		}
		for (int j=0; j<gInfo.waypoints.Size();)
		{
			ArcadeWaypointInfo &wInfo = gInfo.waypoints[j];
			if (wInfo.selected)
				gInfo.waypoints.Delete(j);
			else
				j++;
		}
		SelectLeader(gInfo);
		i++;
	}
	_template->CheckSynchro(); // repair the synchronizations with the deleted items
	_template->Compact();
}

void CStaticMapArcade::ClipboardCut()
{
	ClipboardCopy();
	ClipboardDelete();
}

void CStaticMapArcade::ClipboardCopy()
{
	GClipboard.Clear();
#ifdef _XBOX
	float mouseX = 0.5;
	float mouseY = 0.5;
#else
	float mouseX = 0.5 + GInput.cursorX * 0.5;
	float mouseY = 0.5 + GInput.cursorY * 0.5;
#endif
	GClipboardPos = ScreenToWorld(DrawCoord(mouseX, mouseY));
	for (int i=0; i<_template->emptyVehicles.Size(); i++)
	{
		ArcadeUnitInfo &uInfo = _template->emptyVehicles[i];
		if (uInfo.selected)
			GClipboard.emptyVehicles.Add(uInfo);
	}
	for (int i=0; i<_template->sensors.Size(); i++)
	{
		ArcadeSensorInfo &sInfo = _template->sensors[i];
		if (sInfo.selected)
			GClipboard.sensors.Add(sInfo);
	}
	for (int i=0; i<_template->markers.Size(); i++)
	{
		ArcadeMarkerInfo &mInfo = _template->markers[i];
		if (mInfo.selected)
			GClipboard.markers.Add(mInfo);
	}
	for (int i=0; i<_template->groups.Size(); i++)
	{
		ArcadeGroupInfo &gInfo = _template->groups[i];
		bool grpSelected = false;
		for (int j=0; j<gInfo.units.Size(); j++)
		{
			ArcadeUnitInfo &uInfo = gInfo.units[j];
			if (uInfo.selected)
			{
				grpSelected = true;
				break;
			}
		}
		if (!grpSelected) continue;
		int index = GClipboard.groups.Add();
		ArcadeGroupInfo &gDest = GClipboard.groups[index];
		for (int j=0; j<gInfo.units.Size(); j++)
		{
			ArcadeUnitInfo &uInfo = gInfo.units[j];
			if (uInfo.selected)
				gDest.units.Add(uInfo);
		}
		for (int j=0; j<gInfo.sensors.Size(); j++)
		{
			ArcadeSensorInfo &sInfo = gInfo.sensors[j];
			if (sInfo.selected)
				gDest.sensors.Add(sInfo);
		}
		for (int j=0; j<gInfo.waypoints.Size(); j++)
		{
			ArcadeWaypointInfo &wInfo = gInfo.waypoints[j];
			if (wInfo.selected)
				gDest.waypoints.Add(wInfo);
		}
		SelectLeader(gDest);
	}
	GClipboard.nextVehId = _template->nextVehId;
	GClipboard.nextSyncId = _template->nextSyncId;
	GClipboard.CheckSynchro(); // keep only synchronizations inside the clipboard
	GClipboard.Compact();
}

void CStaticMapArcade::ClipboardPaste()
{
	_template->ClearSelection();

#ifdef _XBOX
	float mouseX = 0.5;
	float mouseY = 0.5;
#else
	float mouseX = 0.5 + GInput.cursorX * 0.5;
	float mouseY = 0.5 + GInput.cursorY * 0.5;
#endif
	Vector3 pos = ScreenToWorld(DrawCoord(mouseX, mouseY));

	// offset inserted objects
	GClipboard.AddOffset(pos - GClipboardPos);
	GClipboardPos = pos;
	
	_template->Merge(GClipboard);
}

void CStaticMapArcade::ClipboardPasteAbsolute()
{
	_template->ClearSelection();
	_template->Merge(GClipboard);
}

InsertMode CStaticMapArcadeViewer::GetMode()
{
	return IMUnits;
}

InsertMode CStaticMapArcade::GetMode()
{
	if (_parent->IDD() == IDD_ARCADE_MAP)
	{
		DisplayArcadeMap *disp = dynamic_cast<DisplayArcadeMap *>(_parent.GetLink());
		return disp->_mode;
	}
	else if (_parent->IDD() == IDD_INTEL_GETREADY)
	{
		// TODO: IMWaypoints / IMSynchronize - switching
		return IMWaypoints;
/*
		CToolBox *ctrl = dynamic_cast<CToolBox *>(_parent->GetCtrl(IDC_GETREADY_EDITMODE));
		switch (ctrl->GetCurSel())
		{
		case 0:
			return IMWaypoints;
		case 1:
			return IMSynchronize;
		default:
			Fail("Selection");
			return IMWaypoints;
		}
*/
	}
	else
		return IMUnits;
}

ArcadeUnitInfo *CStaticMapArcade::GetLastUnit()
{
	Assert(_parent->IDD() == IDD_ARCADE_MAP);
	if (_parent->IDD() == IDD_ARCADE_MAP)
	{
		DisplayArcadeMap *disp = dynamic_cast<DisplayArcadeMap *>(_parent.GetLink());
		return &disp->_lastUnit;
	}
	else
		return NULL;
}

ArcadeUnitInfo *CStaticMapArcade::GetLastLogic()
{
  Assert(_parent->IDD() == IDD_ARCADE_MAP);
  if (_parent->IDD() == IDD_ARCADE_MAP)
  {
    DisplayArcadeMap *disp = dynamic_cast<DisplayArcadeMap *>(_parent.GetLink());
    return &disp->_lastLogic;
  }
  else
    return NULL;
}

bool CStaticMapArcade::HasRight(int ig)
{
	switch (_editRights)
	{
		case ERNone:
			return false;
		case ERGroupWP:
			{
				if (ig < 0) return false;
				ArcadeGroupInfo &gInfo = _template->groups[ig];
				int i, n = gInfo.units.Size();
				for (i=0; i<n; i++)
				{
					switch (gInfo.units[i].player)
					{
					case APPlayerCommander:
					case APPlayerDriver:
					case APPlayerGunner:
						return true;
					}
				}
				return false;
			}
		case ERSideWP:
			{
				if (ig < 0) return false;
				ArcadeGroupInfo &gInfo = _template->groups[ig];
				return gInfo.side == Glob.header.playerSide;
			}
		case ERFull:
			return true;;
		default:
			Fail("Edit rights");
			return false;
	}
}

bool CStaticMapArcadeViewer::OnKeyDown(int dikCode)
{
#ifdef _WIN32
	switch (dikCode)
	{
		case DIK_F1:
			{
				if (_parent->IDD() != IDD_INTEL_GETREADY) break;
				CToolBox *box = dynamic_cast<CToolBox *>(_parent->GetCtrl(IDC_GETREADY_EDITMODE));
				if (!box) break;
				box->SetCurSel(0);
				return true;
			}
		case DIK_F2:
			{
				if (_parent->IDD() != IDD_INTEL_GETREADY) break;
				CToolBox *box = dynamic_cast<CToolBox *>(_parent->GetCtrl(IDC_GETREADY_EDITMODE));
				if (!box) break;
				box->SetCurSel(1);
				return true;
			}
	}
#endif
	return CStaticMap::OnKeyDown(dikCode);
}

bool CStaticMapArcade::OnKeyDown(int dikCode)
{
#ifdef _WIN32
	switch (dikCode)
	{
		case DIK_NUMPAD5:
			{
				const float sizeLand = LandGrid * LandRange;
				const float invSizeLand = 1.0 / sizeLand;
				Vector3 curPos = GetCenter();
				ArcadeUnitInfo *uInfo = _template->FindPlayer();
				Vector3 pos = Vector3(0.5 * sizeLand, 0, 0.5 * sizeLand);
				if (uInfo) pos = uInfo->position;
				float diff = curPos.Distance(pos);
				float scale = 2.0 * diff * invSizeLand;
				if (scale > _scaleX)
				{
					float time = log(scale / _scaleX);
					ClearAnimation();
					AddAnimationPhase(time, scale, curPos);
					AddAnimationPhase(1.0, scale, pos);
					AddAnimationPhase(time, _scaleX, pos);
					CreateInterpolator();
				}
				else
				{
					float time = diff * invSizeLand * _invScaleX;
					ClearAnimation();
					AddAnimationPhase(time, _scaleX, pos);
					CreateInterpolator();
				}
			}
			return true;
		case DIK_F1:
		{
			if (_parent->IDD() != IDD_ARCADE_MAP) break;

			DisplayArcadeMap *disp = dynamic_cast<DisplayArcadeMap *>(_parent.GetLink());
			disp->_mode = IMUnits;
			CToolBox *box = dynamic_cast<CToolBox *>(_parent->GetCtrl(IDC_ARCMAP_MODE));
			if (box)
				box->SetCurSel(disp->_mode);
			return true;
		}
		case DIK_F2:
		{
			if (_parent->IDD() != IDD_ARCADE_MAP) break;

			DisplayArcadeMap *disp = dynamic_cast<DisplayArcadeMap *>(_parent.GetLink());
			disp->_mode = IMGroups;
			CToolBox *box = dynamic_cast<CToolBox *>(_parent->GetCtrl(IDC_ARCMAP_MODE));
			if (box)
				box->SetCurSel(disp->_mode);
			return true;
		}
		case DIK_F3:
		{
			if (_parent->IDD() != IDD_ARCADE_MAP) break;

			DisplayArcadeMap *disp = dynamic_cast<DisplayArcadeMap *>(_parent.GetLink());
			disp->_mode = IMSensors;
			CToolBox *box = dynamic_cast<CToolBox *>(_parent->GetCtrl(IDC_ARCMAP_MODE));
			if (box)
				box->SetCurSel(disp->_mode);
			return true;
		}
		case DIK_F4:
		{
			if (_parent->IDD() != IDD_ARCADE_MAP) break;

			DisplayArcadeMap *disp = dynamic_cast<DisplayArcadeMap *>(_parent.GetLink());
			disp->_mode = IMWaypoints;
			CToolBox *box = dynamic_cast<CToolBox *>(_parent->GetCtrl(IDC_ARCMAP_MODE));
			if (box)
				box->SetCurSel(disp->_mode);
			return true;
		}
		case DIK_F5:
		{
			if (_parent->IDD() != IDD_ARCADE_MAP) break;

			DisplayArcadeMap *disp = dynamic_cast<DisplayArcadeMap *>(_parent.GetLink());
			disp->_mode = IMSynchronize;
			CToolBox *box = dynamic_cast<CToolBox *>(_parent->GetCtrl(IDC_ARCMAP_MODE));
			if (box)
				box->SetCurSel(disp->_mode);
			return true;
		}
		case DIK_F6:
		{
			if (_parent->IDD() != IDD_ARCADE_MAP) break;

			DisplayArcadeMap *disp = dynamic_cast<DisplayArcadeMap *>(_parent.GetLink());
			disp->_mode = IMMarkers;
			CToolBox *box = dynamic_cast<CToolBox *>(_parent->GetCtrl(IDC_ARCMAP_MODE));
			if (box)
				box->SetCurSel(disp->_mode);
			return true;
		}
    case DIK_F7:
      {
        if (_parent->IDD() != IDD_ARCADE_MAP) break;

        DisplayArcadeMap *disp = dynamic_cast<DisplayArcadeMap *>(_parent.GetLink());
        disp->_mode = IMModules;
        CToolBox *box = dynamic_cast<CToolBox *>(_parent->GetCtrl(IDC_ARCMAP_MODE));
        if (box)
          box->SetCurSel(disp->_mode);
        return true;
      }
		case DIK_DELETE:
		{
			if (GInput.keys[DIK_LSHIFT] || GInput.GetKey(DIK_RSHIFT))
			{
				if (HasFullRights())
				{
					ClipboardCut();
				}
			}
			else switch (_infoMove._type)
			{
				case signArcadeUnit:
					if (HasFullRights())
					{
						_template->UnitDelete(_infoMove._indexGroup, _infoMove._index);
						_infoClick._type = signNone;
						_infoClickCandidate._type = signNone;
						_infoMove._type = signNone;
						_dragging = false;
						_selecting = false;
					}
					break;
				case signArcadeWaypoint:
					if (HasRight(_infoMove._indexGroup))
					{
						_template->WaypointDelete(_infoMove._indexGroup, _infoMove._index);
						_infoClick._type = signNone;
						_infoClickCandidate._type = signNone;
						_infoMove._type = signNone;
						_dragging = false;
						_selecting = false;
					}
					break;
				case signArcadeSensor:
					if (HasFullRights())
					{
						_template->SensorDelete(_infoMove._indexGroup, _infoMove._index);
						_infoClick._type = signNone;
						_infoClickCandidate._type = signNone;
						_infoMove._type = signNone;
						_dragging = false;
						_selecting = false;
					}
					break;
				case signArcadeMarker:
					if (HasFullRights())
					{
						_template->MarkerDelete(_infoMove._index);
						_infoClick._type = signNone;
						_infoClickCandidate._type = signNone;
						_infoMove._type = signNone;
						_dragging = false;
						_selecting = false;
					}
					break;
			}

			if (_parent->IDD() == IDD_ARCADE_MAP)
			{
				DisplayArcadeMap *disp = dynamic_cast<DisplayArcadeMap *>(_parent.GetLink());
				Assert(disp);
				disp->ShowButtons();
			}
      UpdateMissionName(true);
			return true;
		}
		case DIK_INSERT:
			if (GInput.keys[DIK_LCONTROL] || GInput.keys[DIK_RCONTROL])
			{
				if (HasFullRights())
				{
					ClipboardCopy();
          UpdateMissionName(true);
					return true;
				}
			}
			else if (GInput.keys[DIK_LSHIFT] || GInput.GetKey(DIK_RSHIFT))
			{
				if (HasFullRights())
				{
					ClipboardPaste();
          UpdateMissionName(true);
					return true;
				}
			}
			break;
		case DIK_X:
			if (GInput.keys[DIK_LCONTROL] || GInput.keys[DIK_RCONTROL])
			{
				if (HasFullRights())
				{
					ClipboardCut();
          UpdateMissionName(true);
					return true;
				}
			}
			break;
		case DIK_C:
			if (GInput.keys[DIK_LCONTROL] || GInput.keys[DIK_RCONTROL])
			{
				if (HasFullRights())
				{
					ClipboardCopy();
					return true;
				}
			}
			break;
		case DIK_V:
			if (GInput.keys[DIK_LCONTROL] || GInput.keys[DIK_RCONTROL])
			{
				if (HasFullRights())
				{
          UpdateMissionName(true);
					if (GInput.keys[DIK_LSHIFT] || GInput.GetKey(DIK_RSHIFT))
						ClipboardPasteAbsolute();
					else
						ClipboardPaste();
					return true;
				}
			}
			break;
	}
#endif
	return CStaticMapArcadeViewer::OnKeyDown(dikCode);
}

void CStaticMapArcade::UpdateMissionName(bool edited)
{
  RString name = DecodeFileName(Glob.header.filename);
  if(name.IsEmpty()) name = "";
  if(edited)
  {
    CStatic *ctrl =  dynamic_cast<CStatic *> (_parent->GetCtrl(IDC_ARCMAP_MISSION_NAME));
    if(ctrl) ctrl->SetText("*"+name);
  }
  else
  {
    CStatic *ctrl =  dynamic_cast<CStatic *> (_parent->GetCtrl(IDC_ARCMAP_MISSION_NAME));
    if(ctrl) ctrl->SetText(name);
  }
}

void CStaticMapArcade::ProcessCheats()
{
#if _ENABLE_CHEATS
//	if (GInput.GetActionToDo(UAFire))
  if (GInput.GetKeyToDo(DIK_LCONTROL))
	{
		QOStrStream out;

#ifdef _XBOX
		float mouseX = 0.5;
		float mouseY = 0.5;
#else
		float mouseX = 0.5 + GInput.cursorX * 0.5;
		float mouseY = 0.5 + GInput.cursorY * 0.5;
#endif
		Vector3 pos = ScreenToWorld(DrawCoord(mouseX, mouseY));

		char buf[1024];
		sprintf
		(
			buf, "[%.3f,%.3f,%.3f]\r\n", pos.X(), pos.Z(), pos.Y()
		);
		out.write(buf,strlen(buf));
		
    char logFilename[MAX_PATH];
    if (GetLocalSettingsDir(logFilename))
    {
      CreateDirectory(logFilename, NULL);
      TerminateBy(logFilename, '\\');
    }
    else logFilename[0] = 0;
    strcat(logFilename, "clipboard.txt");
		ExportToClipboardAndFile(out.str(), out.pcount(), logFilename);
	}

	if (GInput.GetCheat2ToDo(DIK_SLASH))
	{
		RString vehicle;
		Vector3 pos = VZero;
		if (_infoClick._type == signArcadeUnit)
		{
			if (_infoClick._indexGroup >= 0)
			{
				Assert(_infoClick._indexGroup < _template->groups.Size());
				ArcadeGroupInfo &gInfo = _template->groups[_infoClick._indexGroup];
				Assert(_infoClick._index < gInfo.units.Size());
				ArcadeUnitInfo &uInfo = gInfo.units[_infoClick._index];
				vehicle = uInfo.vehicle;
				pos = uInfo.position;
			}
			else
			{
				ArcadeUnitInfo &uInfo = _template->emptyVehicles[_infoClick._index];
				vehicle = uInfo.vehicle;
				pos = uInfo.position;
			}
		}
		void ExportOperMaps(RString prefix, RString vehicle, Vector3Par pos);
		ExportOperMaps(Glob.header.worldname, vehicle, pos);
	}
#endif

	CStaticMapArcadeViewer::ProcessCheats();
}

static int CharRPos(const char *t, char c)
{
	const char *cp = strrchr(t,c);
	if (!cp) return 0;
	return cp-t;
}

static RString ShortExpress(RString ex)
{
	if (ex.GetLength()<32) return ex;
	// try to shorten it in some reasonable way first
	RString shortEx = ex.Substring(0,29);
	int cp;
	if ((cp=CharRPos(shortEx,';'))>22)
	{
		return ex.Substring(0,cp+1)+RString("...");
	}
	return ex.Substring(0,26)+RString("...");
}

static RString CatWords(RString cond, RString cond2)
{
	if (cond.GetLength()>0 && cond2.GetLength()>0)
	{
		return cond+RString(" ")+cond2;
	}
	return cond+cond2;
}

/*!
\patch 1.50 Date 4/3/2002 by Jirka
- Added: Label of waypoint in mission editor contains name of group leader
\patch 1.61 Date 5/30/2002 by Ondra
- Added: Label of waypoint, unit or trigger in mission editor
now contains extended information.
*/

void CStaticMapArcadeViewer::DrawLabel(struct SignInfo &info, PackedColor color, int shadow)
{
	RString text;
	// additional text
	AutoArray<RString> addText;

	Point3 pos;
	switch (info._type)
	{
	case signNone:
		break;
	case signArcadeUnit:
		{
			ArcadeUnitInfo *uInfo = NULL;
			if (info._indexGroup == -1)
			{
				// empty vehicle
				if ( info._index < _template->emptyVehicles.Size() ) uInfo = &_template->emptyVehicles[info._index];
			}
			else
			{
				if ( info._indexGroup < _template->groups.Size() )
        {
          ArcadeGroupInfo &gInfo = _template->groups[info._indexGroup];
          if ( info._index < gInfo.units.Size() ) uInfo = &gInfo.units[info._index];
        }
			}
      if (uInfo)
      {
			  text = Pars >> "CfgVehicles" >> uInfo->vehicle >> "displayName";
			  if (uInfo->name && uInfo->name.GetLength() > 0)
			  {
				  text = text + RString(" - ");
				  text = text + uInfo->name;
			  }
			  pos = uInfo->position;

			  RString cond;
			  if (uInfo->presence<0.9999f)
			  {
				  cond = Format("%d %%",toInt(uInfo->presence*100));
			  }
			  if (strcmp(uInfo->presenceCondition,"true"))
			  {
				  cond = CatWords(cond,RString("? ")+ShortExpress(uInfo->presenceCondition));
			  }
			  if (cond.GetLength()>0)
			  {
				  addText.Add(cond);
			  }
			  if (uInfo->init.GetLength()>0)
			  {
				  addText.Add(ShortExpress(uInfo->init));
			  }
      }

			break;
		}
	case signArcadeWaypoint:
		{
			Assert(info._indexGroup >= 0);
			Assert(info._indexGroup < _template->groups.Size());
			ArcadeGroupInfo &gInfo = _template->groups[info._indexGroup];
			Assert(info._index < gInfo.waypoints.Size());
			ArcadeWaypointInfo &wInfo = gInfo.waypoints[info._index];
			text = LocalizeString(IDS_AC_MOVE + wInfo.type - ACMOVE);
			
			ArcadeUnitInfo *leader = NULL;
			for (int i=0; i<gInfo.units.Size(); i++)
			{
				ArcadeUnitInfo &uInfo = gInfo.units[i];
				if (uInfo.leader)
				{
					leader = &uInfo;
					break;
				}
			}
			if (leader && leader->name.GetLength() > 0)
			{
				text = text + RString(" (") + leader->name + RString(")");
			}

			pos = wInfo.position;
			//if (wInfo.type!=ACMOVE) addText.Add(LocalizeString(IDS_AC_MOVE + wInfo.type - ACMOVE));
			if (wInfo.combatMode>=0) addText.Add(LocalizeString(IDS_IGNORE + wInfo.combatMode));
			if (wInfo.combat>CMUnchanged) addText.Add(LocalizeString(IDS_COMBAT_UNCHANGED+wInfo.combat));
			if (wInfo.formation>=0) addText.Add(LocalizeString(IDS_COLUMN+wInfo.formation));
			if (wInfo.speed>SpeedUnchanged) addText.Add(LocalizeString(IDS_SPEED_UNCHANGED+wInfo.speed));
			if (wInfo._expActiv.GetLength()>0)
			{
				addText.Add(ShortExpress(wInfo._expActiv));
			}
			break;
		}
	case signArcadeSensor:
		{
			ArcadeSensorInfo *sInfo = NULL;
			if (info._indexGroup < 0)
			{
				sInfo = &_template->sensors[info._index];
			}
			else
			{
				ArcadeGroupInfo &gInfo = _template->groups[info._indexGroup];
				sInfo = &gInfo.sensors[info._index];
			}
			if (sInfo->text && sInfo->text.GetLength() > 0)
			{
				text = sInfo->text;
			}
			else
			{
				text = LocalizeString(IDS_SENSOR);
			}
			if (strcmp(sInfo->expCond,"this"))
			{
				addText.Add(RString("? ")+ShortExpress(sInfo->expCond));
			}
			else if (sInfo->activationBy>ASANone && sInfo->activationBy<ASAStatic)
			{
				const static int act[]=
				{
					IDS_SENSORACTIV_NONE,
					IDS_EAST,
					IDS_WEST,
					IDS_GUERRILA,
					IDS_CIVILIAN,
					IDS_LOGIC,
					IDS_SENSORACTIV_ANYBODY,
					IDS_SENSORACTIV_ALPHA,
					IDS_SENSORACTIV_BRAVO,
					IDS_SENSORACTIV_CHARLIE,
					IDS_SENSORACTIV_DELTA,
					IDS_SENSORACTIV_ECHO,
					IDS_SENSORACTIV_FOXTROT,
					IDS_SENSORACTIV_GOLF,
					IDS_SENSORACTIV_HOTEL,
					IDS_SENSORACTIV_INDIA,
					IDS_SENSORACTIV_JULIET,
					IDS_EAST_SEIZED,
					IDS_WEST_SEIZED,
					IDS_GUERRILA_SEIZED,
				};
				if (sInfo->activationBy<lenof(act))
				{
				  RString at = LocalizeString(act[sInfo->activationBy]);
				  /*
				  activationType does not have IDS 
				  if (sInfo->activationType!=ASATPresent)
				  {
					  at = at + RString(" ") + LocalizeString(IDS_xxx+sInfo->activationType);
				  }
				  */
				  addText.Add(at);
				}
			}
			if (sInfo->type>ASTNone)
			{
				addText.Add(LocalizeString(IDS_SENSORTYPE_NONE+sInfo->type));
			}

			if (sInfo->expActiv.GetLength()>0)
			{
				addText.Add(ShortExpress(sInfo->expActiv));
			}
			pos = sInfo->position;
			break;
		}
	case signArcadeMarker:
		{
			ArcadeMarkerInfo &mInfo = _template->markers[info._index];
			text = mInfo.name;
			pos = mInfo.position;
			break;
		}
	case signStatic:
		{
			Object *obj = info._id;
			EntityAI *veh = dyn_cast<EntityAI>(obj);
			if (veh)
			{
				text = veh->GetType()->GetDisplayName();
				pos = veh->RenderVisualState().Position();
			}
		}
		break;
	}

	if (text.GetLength()>0)
	{
		DrawCoord posMap = WorldToScreen(pos);

		// place label
		float w = GEngine->GetTextWidth(_sizeLabel, _fontLabel, text);
		float h = _sizeLabel;
		float ha = 0;
		float wa = 0;
		float offset = 0.01;

		const int maxAddTexts = 6;
		if (addText.Size()>maxAddTexts)
		{
			addText.Resize(maxAddTexts);
			addText[maxAddTexts-1] = "...";
		}
		for (int i=0; i<addText.Size(); i++)
		{
			float wt = GEngine->GetTextWidth(_sizeLabel, _fontLabel, addText[i]);
			saturateMax(wa,wt);
			ha += _sizeLabel;
		}


		posMap.x -= 0.5 * w;
		if (posMap.y<0.7)
		{
			posMap.y += offset;
		}
		else
		{
			posMap.y -= offset+h+ha;
		}

		Texture *texture = GLOB_SCENE->Preloaded(TextureWhite);
		MipInfo mip = GEngine->TextBank()->UseMipmap(texture, 0, 0);
		GEngine->Draw2D
		(
			mip, _colorLabelBackground,
			Rect2DPixel(posMap.x * _wScreen, posMap.y * _hScreen,
			w * _wScreen, h * _hScreen),
			_clipRect
		);
		if (ha>0) GEngine->Draw2D
		(
			mip, PackedColorRGB(_colorLabelBackground,_colorLabelBackground.A8()*3/4),
			Rect2DPixel(posMap.x * _wScreen, (posMap.y+h) * _hScreen,
			wa * _wScreen, ha * _hScreen),
			_clipRect
		);

		GEngine->DrawText
		(
			posMap, _sizeLabel,
			Rect2DFloat(_x, _y, _w, _h),
			_fontLabel, color, text, 0
		);
		for (int i=0; i<addText.Size(); i++)
		{
			GLOB_ENGINE->DrawText
			(
				Point2DFloat(posMap.x, posMap.y + h + _sizeLabel*i),
				_sizeLabel,
				Rect2DFloat(_x, _y, _w, _h),
				_fontLabel, color, addText[i], 0
			);
		}
	}
}

static bool IsStaticEntityAI(Object *obj, float dist2, void *context)
{
	EntityAI *veh = dyn_cast<EntityAI>(obj);
	if (!veh) return false;
  if (!veh->GetObjectId().IsObject()) return false;
	return veh->GetType()->IsKindOf(GWorld->Preloaded(VTypeStatic));
}

SignInfo CStaticMapArcadeViewer::FindSign(float x, float y)
{
	bool reverse = GInput.keys[DIK_LSHIFT] > 0 || GInput.GetKey(DIK_RSHIFT) > 0;

	struct SignInfo info;
	info._type = signNone;
	info._unit = NULL;
	info._id = NULL;

	Point3 pt = ScreenToWorld(DrawCoord(x, y));
	const float sizeLand = LandGrid * LandRange;
	float dist, minDist = 0.02 * sizeLand * _scaleX;
	minDist *= minDist;

	int i, n = _template->groups.Size();
	if (!reverse) // search in waypoints first
	{
		for (i=0; i<n; i++)
		{
			ArcadeGroupInfo &gInfo = _template->groups[i];
			int j, m;
			m = gInfo.waypoints.Size();
			for (j=0; j<m; j++)
			{
				ArcadeWaypointInfo &wInfo = gInfo.waypoints[j];
				dist = (pt - wInfo.position).SquareSizeXZ();
				if (dist < minDist)
				{
					minDist = dist;
					info._type = signArcadeWaypoint;
					info._indexGroup = i;
					info._index = j;
				}
			}
		}
	}

	// units, flags and sensors in groups
	for (i=0; i<n; i++)
	{
		ArcadeGroupInfo &gInfo = _template->groups[i];
		int j, m;
		m = gInfo.units.Size();
		for (j=0; j<m; j++)
		{
			ArcadeUnitInfo &uInfo = gInfo.units[j];
			if
			(
				uInfo.side != Glob.header.playerSide &&
				uInfo.age == AAUnknown &&
				!HasFullRights()
			) continue;

			dist = (pt - uInfo.position).SquareSizeXZ();
			if (dist < minDist)
			{
				minDist = dist;
				info._type = signArcadeUnit;
				info._indexGroup = i;
				info._index = j;
			}
		}

		m = gInfo.sensors.Size();
		for (j=0; j<m; j++)
		{
			ArcadeSensorInfo &sInfo = gInfo.sensors[j];
			dist = (pt - sInfo.position).SquareSizeXZ();
			if (dist < minDist)
			{
				minDist = dist;
				info._type = signArcadeSensor;
				info._indexGroup = i;
				info._index = j;
			}
		}
	}

	// empty units
	n = _template->emptyVehicles.Size();
	for (i=0; i<n; i++)
	{
		ArcadeUnitInfo &uInfo = _template->emptyVehicles[i];
		if
		(
			uInfo.age == AAUnknown &&
			!HasFullRights()
		) continue;

		dist = (pt - uInfo.position).SquareSizeXZ();
		if (dist < minDist)
		{
			minDist = dist;
			info._type = signArcadeUnit;
			info._indexGroup = -1;
			info._index = i;
		}
	}
	
	// sensors
	n = _template->sensors.Size();
	for (i=0; i<n; i++)
	{
		ArcadeSensorInfo &sInfo = _template->sensors[i];
		dist = (pt - sInfo.position).SquareSizeXZ();
		if (dist < minDist)
		{
			minDist = dist;
			info._type = signArcadeSensor;
			info._indexGroup = -1;
			info._index = i;
		}
	}

	// markers
	n = _template->markers.Size();
	for (i=0; i<n; i++)
	{
		ArcadeMarkerInfo &mInfo = _template->markers[i];
		dist = (pt - mInfo.position).SquareSizeXZ();
		if (dist < minDist)
		{
			minDist = dist;
			info._type = signArcadeMarker;
			info._index = i;
		}
	}

	// buildings
	if (minDist > 0)
	{
    float ptsLand = 800.0 * _invScaleX * InvLandRange; // avoid dependece on video resolution
    // search for buildings only when are drawn
    if (ptsLand >= _ptsPerSquareObj)
    {
      Object *obj = GLandscape->NearestObject(pt, sqrt(minDist), IsStaticEntityAI, NULL, true);
      if (obj)
      {
        info._type = signStatic;
        info._id = dyn_cast<EntityAI>(obj);
        minDist = pt.DistanceXZ2(obj->RenderVisualState().Position());
      }
    }
	}
	
	if (reverse) // search in waypoints last
	{
		n = _template->groups.Size();
		for (i=0; i<n; i++)
		{
			ArcadeGroupInfo &gInfo = _template->groups[i];
			int j, m;
			m = gInfo.waypoints.Size();
			for (j=0; j<m; j++)
			{
				ArcadeWaypointInfo &wInfo = gInfo.waypoints[j];
				dist = (pt - wInfo.position).SquareSizeXZ();
				if (dist < minDist)
				{
					minDist = dist;
					info._type = signArcadeWaypoint;
					info._indexGroup = i;
					info._index = j;
				}
			}
		}
	}

	return info;
}

bool CStaticMapArcadeViewer::IsSelected(const SignInfo &info) const
{
	switch (info._type)
	{
	case signArcadeUnit:
		{
			ArcadeUnitInfo *uInfo = NULL;
			if (info._indexGroup == -1)
			{
				if ( info._index < _template->emptyVehicles.Size() ) uInfo = &_template->emptyVehicles[info._index];
			}
			else
			{
        if (info._indexGroup < _template->groups.Size() )
        {
  				ArcadeGroupInfo &gInfo = _template->groups[info._indexGroup];
  				if ( info._index < gInfo.units.Size() ) uInfo = &gInfo.units[info._index];
        }
			}
      return uInfo ? uInfo->selected : false;
		}
	case signArcadeSensor:
		{
			ArcadeSensorInfo *sInfo = NULL;
			if (info._indexGroup == -1)
			{
				if ( info._index < _template->sensors.Size() ) sInfo = &_template->sensors[info._index];
			}
			else
			{
        if ( info._indexGroup < _template->groups.Size() )
        {
  				ArcadeGroupInfo &gInfo = _template->groups[info._indexGroup];
	  			if ( info._index < gInfo.sensors.Size() ) sInfo = &gInfo.sensors[info._index];
        }
			}
      return sInfo ? sInfo->selected : false;
		}
	case signArcadeMarker:
		{
			ArcadeMarkerInfo &mInfo = _template->markers[info._index];
			return mInfo.selected;
		}
	case signArcadeWaypoint:
		{
			ArcadeGroupInfo &gInfo = _template->groups[info._indexGroup];
			ArcadeWaypointInfo &wInfo = gInfo.waypoints[info._index];
			return wInfo.selected;
		}
	default:
		return false;
	}
}

void CStaticMapArcadeViewer::InvertSelection(const SignInfo &info)
{
	switch (info._type)
	{
	case signArcadeUnit:
		{
      ArcadeUnitInfo *uInfo = NULL;
      if (info._indexGroup == -1)
      {
        if ( info._index < _template->emptyVehicles.Size() ) uInfo = &_template->emptyVehicles[info._index];
      }
      else
      {
        if (info._indexGroup < _template->groups.Size() )
        {
          ArcadeGroupInfo &gInfo = _template->groups[info._indexGroup];
          if ( info._index < gInfo.units.Size() ) uInfo = &gInfo.units[info._index];
        }
      }
			if (uInfo) uInfo->selected = !uInfo->selected;
			break;
		}
	case signArcadeSensor:
		{
      ArcadeSensorInfo *sInfo = NULL;
      if (info._indexGroup == -1)
      {
        if ( info._index < _template->sensors.Size() ) sInfo = &_template->sensors[info._index];
      }
      else
      {
        if ( info._indexGroup < _template->groups.Size() )
        {
          ArcadeGroupInfo &gInfo = _template->groups[info._indexGroup];
          if ( info._index < gInfo.sensors.Size() ) sInfo = &gInfo.sensors[info._index];
        }
      }
      if (sInfo) sInfo->selected = !sInfo->selected;
			break;
		}
	case signArcadeMarker:
		{
			ArcadeMarkerInfo &mInfo = _template->markers[info._index];
			mInfo.selected = !mInfo.selected;
			break;
		}
	case signArcadeWaypoint:
		{
			ArcadeGroupInfo &gInfo = _template->groups[info._indexGroup];
			ArcadeWaypointInfo &wInfo = gInfo.waypoints[info._index];
			wInfo.selected = !wInfo.selected;
			break;
		}
	}
}

void CStaticMapArcadeViewer::OnLButtonDown(float x, float y)
{
	_infoClick = FindSign(x, y);

	// set selection
	//if (!GInput.keys[DIK_LCONTROL])
	_template->ClearSelection();
	InvertSelection(_infoClick);
}

void CStaticMapArcade::OnLButtonDown(float x, float y)
{
	SignInfo info = FindSign(x, y);
	_infoClickCandidate = info;
	_lastPos = ScreenToWorld(DrawCoord(x, y));
  if(_infoClickCandidate._type != signNone) UpdateMissionName(true);
}

void CStaticMapArcade::OnLButtonUp(float x, float y)
{
	if (_dragging)
	{
		_dragging = false;
		if (GetMode() != IMGroups && GetMode() != IMSynchronize) return;

		SignInfo info = FindSign(x, y);

		if (GetMode() == IMGroups)
		{
			switch (_infoClick._type)
			{
			case signArcadeUnit:
				if (!HasFullRights()) return;
				if (info._type == signArcadeUnit)
				{
					// try to assign unit into another group
					if (_infoClick._indexGroup >= 0 && info._indexGroup >= 0)
					{
						bool ok = _template->UnitChangeGroup
						(
							_infoClick._indexGroup, _infoClick._index,
							info._indexGroup
						);
						(void)ok;
					}
				}
				else if (info._type == signNone)
				{
					// unassign from group
					if (_infoClick._indexGroup >= 0)
					{
						bool ok = _template->UnitChangeGroup
						(
							_infoClick._indexGroup, _infoClick._index,
							-1
						);
						(void)ok;
					}
				}
				else if (info._type == signArcadeSensor)
				{
					ArcadeUnitInfo *uInfo = NULL;
					if (_infoClick._indexGroup >= 0)
					{
						if ( _infoClick._indexGroup < _template->groups.Size() )
            {
              ArcadeGroupInfo &gInfo = _template->groups[_infoClick._indexGroup];
              if ( _infoClick._index < gInfo.units.Size() ) uInfo = &gInfo.units[_infoClick._index];
            }
					}
					else
					{
						if ( _infoClick._index < _template->emptyVehicles.Size() ) uInfo = &_template->emptyVehicles[_infoClick._index];
					}
					if (uInfo)
            _template->SensorChangeVehicle
					  (
						  info._indexGroup, info._index,
						  uInfo->id
					  );
					// TODO: vehicles are not sent over network
				}
				else if (info._type == signArcadeMarker)
				{
					_template->UnitAddMarker
					(
						_infoClick._indexGroup, _infoClick._index,
						info._index
					);
					// TODO: markers are not sent over network
				}
				break;
			case signArcadeSensor:
				if (info._type == signArcadeUnit)
				{
					ArcadeUnitInfo *uInfo = NULL;
					if (info._indexGroup >= 0)
					{
						if ( info._indexGroup < _template->groups.Size() )
            {
              ArcadeGroupInfo &gInfo = _template->groups[info._indexGroup];
              if ( info._index < gInfo.units.Size() ) uInfo = &gInfo.units[info._index];
            }
					}
					else
					{
						if ( info._index < _template->emptyVehicles.Size() ) uInfo = &_template->emptyVehicles[info._index];
					}
					if (uInfo)
            _template->SensorChangeVehicle
					  (
						  _infoClick._indexGroup, _infoClick._index,
						  uInfo->id
					  );
					// TODO: vehicles are not sent over network
				}
				else if (info._type == signStatic)
				{
					Object *obj = info._id;
					Assert(obj);
					if (!obj) return;
					_template->SensorChangeStatic
					(
						_infoClick._indexGroup, _infoClick._index,
						obj->ID(), obj->RenderVisualState().Position()
					);
					// TODO: buildings are not sent over network
				}
				else if (info._type == signNone)
				{
					// unassign
					//ArcadeSensorInfo *sInfo = NULL;
					if (_infoClick._indexGroup >= 0)
					{
						_template->SensorChangeGroup
						(
							_infoClick._indexGroup, _infoClick._index,
							-1
						);
					}
					else
					{
						ArcadeSensorInfo &sInfo = _template->sensors[_infoClick._index];
						if (sInfo.idVisitorObj >= 0)
						{
							_template->SensorChangeStatic
							(
								_infoClick._indexGroup, _infoClick._index,
								VISITOR_NO_ID, VZero
							);
							// TODO: buildings are not sent over network
						}
						else if (sInfo.idVehicle >= 0)
						{
							_template->SensorChangeVehicle
							(
								_infoClick._indexGroup, _infoClick._index,
								-1
							);
							// TODO: vehicles are not sent over network
						}
					}
				}
				break;
			case signArcadeMarker:
				if (info._type == signArcadeUnit)
				{
					_template->UnitAddMarker
					(
						info._indexGroup, info._index,
						_infoClick._index
					);
					// TODO: markers are not sent over network
				}
				else if (info._type == signNone)
				{
					_template->RemoveMarker
					(
						_infoClick._index
					);
					// TODO: markers are not sent over network
				}
				break;
			case signStatic:
				if (info._type == signArcadeSensor)
				{
					Object *obj = _infoClick._id;
					Assert(obj);
					if (!obj) return;
					_template->SensorChangeStatic
					(
						info._indexGroup, info._index,
						obj->ID(), obj->RenderVisualState().Position()
					);
					// TODO: buildings are not sent over network
				}
				// remove from this side is too hard - do not use it
				break;
			}
		}
		else // GetMode() == IMSynchronize
		{
			switch (_infoClick._type)
			{
			case signArcadeWaypoint:
				if (info._type == signArcadeWaypoint)
				{
					if (HasRight(_infoClick._indexGroup) || HasRight(info._indexGroup))
					{
						_template->WaypointChangeSynchro
						(
							_infoClick._indexGroup, _infoClick._index,
							info._indexGroup, info._index
						);
					}
				}
				else if (info._type == signArcadeSensor)
				{
					if (HasRight(_infoClick._indexGroup))
					{
						_template->SensorChangeSynchro
						(
							info._indexGroup, info._index,
							_infoClick._indexGroup, _infoClick._index
						);
					}
				}
				else if (info._type == signNone)
				{
					if (HasRight(_infoClick._indexGroup))
					{
						_template->WaypointChangeSynchro
						(
							_infoClick._indexGroup, _infoClick._index,
							-1, -1
						);
					}
				}
				break;
			case signArcadeSensor:
				if (info._type == signArcadeWaypoint)
				{
					if (HasRight(info._indexGroup))
					{
						_template->SensorChangeSynchro
						(
							_infoClick._indexGroup, _infoClick._index,
							info._indexGroup, info._index
						);
					}
				}
				else if (info._type == signNone)
				{
					if (HasFullRights())
					{
						_template->SensorChangeSynchro
						(
							_infoClick._indexGroup, _infoClick._index,
							-1, -1
						);
					}
				}
				break;
      case signArcadeUnit:
        if (info._type == signArcadeUnit)
        {
          if (HasRight(info._indexGroup))
          {
            _template->UnitChangeSynchro(
              _infoClick._indexGroup, _infoClick._index,
              info._indexGroup, info._index);
          }
        }
        else if (info._type == signNone)
        {
          if (HasFullRights())
          {
            _template->UnitChangeSynchro(
              _infoClick._indexGroup, _infoClick._index,
              -1, -1);
          }
        }
        break;
			}
		}

		_infoClick._type = signNone;
		_infoClickCandidate._type = signNone;
		_infoMove._type = signNone;
	}
	else if (_selecting)
	{
		_selecting = false;
		if (!GInput.keys[DIK_LCONTROL] && !GInput.keys[DIK_RCONTROL])
			_template->ClearSelection();

		float xMin = floatMin(_special.X(), _lastPos.X());
		float xMax = floatMax(_special.X(), _lastPos.X());
		float zMin = floatMin(_special.Z(), _lastPos.Z());
		float zMax = floatMax(_special.Z(), _lastPos.Z());

		for (int i=0; i<_template->emptyVehicles.Size(); i++)
		{
			ArcadeUnitInfo &info = _template->emptyVehicles[i];
			if
			(
				info.position.X() >= xMin && info.position.X() < xMax &&
				info.position.Z() >= zMin && info.position.Z() < zMax
			) info.selected = !info.selected;
		}
		for (int i=0; i<_template->sensors.Size(); i++)
		{
			ArcadeSensorInfo &info = _template->sensors[i];
			if
			(
				info.position.X() >= xMin && info.position.X() < xMax &&
				info.position.Z() >= zMin && info.position.Z() < zMax
			) info.selected = !info.selected;
		}
		for (int i=0; i<_template->markers.Size(); i++)
		{
			ArcadeMarkerInfo &info = _template->markers[i];
			if
			(
				info.position.X() >= xMin && info.position.X() < xMax &&
				info.position.Z() >= zMin && info.position.Z() < zMax
			) info.selected = !info.selected;
		}
		for (int i=0; i<_template->groups.Size(); i++)
		{
			ArcadeGroupInfo &gInfo = _template->groups[i];
			for (int j=0; j<gInfo.units.Size(); j++)
			{
				ArcadeUnitInfo &info = gInfo.units[j];
				if
				(
					info.position.X() >= xMin && info.position.X() < xMax &&
					info.position.Z() >= zMin && info.position.Z() < zMax
				) info.selected = !info.selected;
			}
			for (int j=0; j<gInfo.sensors.Size(); j++)
			{
				ArcadeSensorInfo &info = gInfo.sensors[j];
				if
				(
					info.position.X() >= xMin && info.position.X() < xMax &&
					info.position.Z() >= zMin && info.position.Z() < zMax
				) info.selected = !info.selected;
			}
			for (int j=0; j<gInfo.waypoints.Size(); j++)
			{
				ArcadeWaypointInfo &info = gInfo.waypoints[j];
				if
				(
					info.position.X() >= xMin && info.position.X() < xMax &&
					info.position.Z() >= zMin && info.position.Z() < zMax
				) info.selected = !info.selected;
			}
		}
	}
}

void CStaticMapArcade::OnLButtonClick(float x, float y)
{
	bool canSelectGroup = false;
	switch (_infoClickCandidate._type)
	{
	case signArcadeUnit:
	case signArcadeSensor:
		canSelectGroup = _infoClickCandidate._indexGroup >= 0;
    // continue
	case signArcadeMarker:
		_infoClick = _infoClickCandidate;
		_dragging = GInput.mouseL && HasFullRights();
		break;		
	case signStatic:
		_infoClick = _infoClickCandidate;
    _dragging = false;
    if (GInput.mouseL)
    {
      // enable selecting when starting on the static object (unable to drag)
      _selecting = true;
      _special = _lastPos;
      return;
    }
		break;		
	case signArcadeWaypoint:
		_infoClick = _infoClickCandidate;
		_dragging = GInput.mouseL && HasRight(_infoClick._indexGroup);
		canSelectGroup = true;
		{
			ArcadeGroupInfo &gInfo = _template->groups[_infoClick._indexGroup];
			ArcadeWaypointInfo &wInfo = gInfo.waypoints[_infoClick._index];
			if (wInfo.id >= 0)
			{
				int idGroup, idUnit;
				_template->FindUnit(wInfo.id, idGroup, idUnit);
				_infoClick._type = signArcadeUnit;
				_infoClick._indexGroup = idGroup;
				_infoClick._index = idUnit;
			}
			else if (wInfo.idVisitorObj >= 0)
			{
				_dragging = false;
			}
		}
		break;
	case signNone:
		if (GInput.mouseL)
		{
			_dragging = false;
			_selecting = true;
			_special = _lastPos;
			return;
		}
		break;
	}

	bool selected = IsSelected(_infoClickCandidate);
	if (!_dragging || !selected)
	{
		// change selection
		if (!GInput.keys[DIK_LCONTROL] && !GInput.keys[DIK_RCONTROL])
		{
			_template->ClearSelection();
			selected = false;
		}
		if ((GInput.keys[DIK_LSHIFT] || GInput.GetKey(DIK_RSHIFT)) && canSelectGroup)
		{
			ArcadeGroupInfo &gInfo =
				_template->groups[_infoClickCandidate._indexGroup];
			gInfo.Select(!selected);
		}
		else
			InvertSelection(_infoClickCandidate);
	}
	CStatic::OnLButtonClick(x, y);
}

/*!
\patch 1.50 Date 4/8/2002 by Jirka
- Fixed: Mission editor - list of markers is not longer in default settings for newly added units.
*/

void CStaticMapArcade::OnLButtonDblClick(float x, float y)
{
	ClearAnimation();
	_moveKey = 0;

	SignInfo info = FindSign(x, y);

	if (info._type == signArcadeWaypoint)
	{
		if (HasRight(info._indexGroup))
		{
			// Edit waypoint
			ArcadeGroupInfo &gInfo = _template->groups[info._indexGroup];
			ArcadeWaypointInfo &wInfo = gInfo.waypoints[info._index];
			_parent->CreateChild(new DisplayArcadeWaypoint
			(
				_parent,
				_template,
				info._indexGroup, info._index,
				wInfo
			));
		}
	}
	else if
	(
		GetMode() == IMWaypoints &&
		(
			_infoClick._type == signArcadeUnit && _infoClick._indexGroup >= 0 ||
			_infoClick._type == signArcadeWaypoint
		)
	)
	{
		if (HasRight(_infoClick._indexGroup))
		{
			// Insert waypoint
			//ArcadeGroupInfo &gInfo = _template->groups[_infoClick._indexGroup];
			ArcadeWaypointInfo wInfo;
			if (info._type == signArcadeUnit)
			{
        ArcadeUnitInfo *uInfo = NULL;
        if (info._indexGroup == -1)
        {
          if ( info._index < _template->emptyVehicles.Size() ) uInfo = &_template->emptyVehicles[info._index];
        }
        else
        {
          if ( info._indexGroup < _template->groups.Size() )
          {
            ArcadeGroupInfo &gInfo = _template->groups[info._indexGroup];
            if ( info._index < gInfo.units.Size() ) uInfo = &gInfo.units[info._index];
          }
        }
				if (uInfo) 
        {
          wInfo.position = uInfo->position;
				  wInfo.id = uInfo->id;
        }
			}
			else if (info._type == signStatic)
			{
				Object *obj = info._id;
				Assert(obj);
				wInfo.position = obj->RenderVisualState().Position();
				wInfo.idVisitorObj = obj->ID();
				wInfo.idObject = GLandscape->GetObjectId(obj->ID(), obj->RenderVisualState().Position());
			}
			else
			{
				wInfo.position = ScreenToWorld(DrawCoord(x, y));
				wInfo.position[1] = GLOB_LAND->RoadSurfaceY(wInfo.position[0], wInfo.position[2]);
			}
			_parent->CreateChild(new DisplayArcadeWaypoint
			(
				_parent,
				_template,
				_infoClick._indexGroup, -1,
				wInfo
			));
		}
	}
	else if (info._type == signArcadeSensor)
	{
		if (HasFullRights())
		{
			// Edit sensor
      ArcadeSensorInfo *sInfo = NULL;
      if (info._indexGroup == -1)
      {
        if ( info._index < _template->sensors.Size() ) sInfo = &_template->sensors[info._index];
      }
      else
      {
        if ( info._indexGroup < _template->groups.Size() )
        {
          ArcadeGroupInfo &gInfo = _template->groups[info._indexGroup];
          if ( info._index < gInfo.sensors.Size() ) sInfo = &gInfo.sensors[info._index];
        }
      }
			if (sInfo) _parent->CreateChild(new DisplayArcadeSensor
			(
				_parent,
				info._indexGroup, info._index,
				*sInfo, _template
			));
		}
	}
	else if (info._type == signArcadeMarker)
	{
		if (HasFullRights())
		{
			// Edit marker
			ArcadeMarkerInfo &mInfo = _template->markers[info._index];
			_parent->CreateChild(new DisplayArcadeMarker
			(
				_parent,
				info._index,
				mInfo,
				_template
			));
		}
	}
	else if (info._type == signArcadeUnit)
	{
		if (HasFullRights())
		{
			// Edit unit
      ArcadeUnitInfo *uInfo = NULL;
      if (info._indexGroup == -1)
      {
        if ( info._index < _template->emptyVehicles.Size() ) uInfo = &_template->emptyVehicles[info._index];
      }
      else
      {
        if ( info._indexGroup < _template->groups.Size() )
        {
          ArcadeGroupInfo &gInfo = _template->groups[info._indexGroup];
          if ( info._index < gInfo.units.Size() ) uInfo = &gInfo.units[info._index];
        }
      }

      if (uInfo)
      {
        if(uInfo->side == TLogic && uInfo->module)
          _parent->CreateChild(new DisplayArcadeModules
          (
          _parent, info._indexGroup, info._index,
          *uInfo, _template
          ));
        else
          _parent->CreateChild(new DisplayArcadeUnit
          (
          _parent, info._indexGroup, info._index,
          *uInfo, _template
          ));
      }
		}
	}
	else if (GetMode() == IMSensors)
	{
		// Insert sensor
		if (HasFullRights())
		{
			ArcadeSensorInfo sInfo;
			sInfo.position = ScreenToWorld(DrawCoord(x, y));
			sInfo.position[1] = GLOB_LAND->RoadSurfaceY(sInfo.position[0], sInfo.position[2]);
			_parent->CreateChild(new DisplayArcadeSensor
			(
				_parent,
				-1, -1,
				sInfo, _template
			));
		}
	}
	else if (GetMode() == IMMarkers)
	{
		// Insert marker
		if (HasFullRights())
		{
			ArcadeMarkerInfo mInfo;
			mInfo.position = ScreenToWorld(DrawCoord(x, y));
			mInfo.position[1] = GLOB_LAND->RoadSurfaceY(mInfo.position[0], mInfo.position[2]);
			_parent->CreateChild(new DisplayArcadeMarker
			(
				_parent,
				-1,
				mInfo,
				_template
			));
		}
	}
	else if (GetMode() == IMGroups)
	{
		// Insert group
		if (HasFullRights())
		{
			Vector3 position = ScreenToWorld(DrawCoord(x, y));
			position[1] = GLandscape->RoadSurfaceY(position[0], position[2]);
			_parent->CreateChild
			(
				new DisplayArcadeGroup(_parent, position, _lastGroupSide, _lastGroupFaction, _lastGroupType, _lastGroupName, 0)
			);
		}
	}
	else if (GetMode() == IMUnits)
	{
		if (HasFullRights())
		{
			// Insert unit
			ArcadeUnitInfo uInfo, *lastInfo = GetLastUnit();
			if (lastInfo) uInfo = *lastInfo;

			uInfo.position = ScreenToWorld(DrawCoord(x, y));
			uInfo.position[1] = GLOB_LAND->RoadSurfaceY(uInfo.position[0], uInfo.position[2]);

			uInfo.name = "";
			uInfo.init = "";

			uInfo.player = APNonplayable;
      if (!_template->FindPlayer())
      {
        uInfo.player = APPlayerCommander;
        //fix: if side is TLOGIC, than rank and faction remained hidden
        if(uInfo.side != TWest || uInfo.side != TEast || uInfo.side != TCivilian || uInfo.side != TGuerrila)
          uInfo.side = TWest;
      }
			
			if (uInfo.player != APNonplayable && uInfo.side == TEmpty) uInfo.side = TWest;

			uInfo.health = 1.0;
			uInfo.fuel = 1.0;
			uInfo.ammo = 1.0;
			uInfo.presence = 1.0;
			uInfo.placement = 0;
			
			uInfo.markers.Clear();
      uInfo.synchronizations.Clear();

			_parent->CreateChild
			(
				new DisplayArcadeUnit(_parent, -1, -1, uInfo, _template)
			);
		}
	}
  else if (GetMode() == IMModules)
  {
    if (HasFullRights())
    {
      // Insert unit
      ArcadeUnitInfo uInfo, *lastInfo = GetLastLogic();
      if (lastInfo) uInfo = *lastInfo;

      uInfo.position = ScreenToWorld(DrawCoord(x, y));
      uInfo.position[1] = GLOB_LAND->RoadSurfaceY(uInfo.position[0], uInfo.position[2]);

      uInfo.name = "";
      uInfo.init = "";

      uInfo.player = APNonplayable;
      if (!_template->FindPlayer())
      {
        uInfo.player = APPlayerCommander;
        //fix: if side is TLOGIC, than rank and faction remained hidden
        if(uInfo.side != TWest || uInfo.side != TEast || uInfo.side != TCivilian || uInfo.side != TGuerrila)
          uInfo.side = TWest;
      }

      if (uInfo.player != APNonplayable && uInfo.side == TEmpty) uInfo.side = TWest;

      uInfo.health = 1.0;
      uInfo.fuel = 1.0;
      uInfo.ammo = 1.0;
      uInfo.presence = 1.0;
      uInfo.placement = 0;

      uInfo.markers.Clear();
      uInfo.synchronizations.Clear();
      uInfo.description = "";
      uInfo.lock = LSUnlocked;

      _parent->CreateChild
        (
        new DisplayArcadeModules(_parent, -1, -1, uInfo, _template)
        );
    }
  }
}

void CStaticMapArcade::OnMouseHold(float x, float y, bool active)
{
	if (_dragging)
	{
		Vector3 curPos = ScreenToWorld(DrawCoord(x, y));
		switch (GetMode())
		{
		case IMGroups:
			switch (_infoClick._type)
			{
			case signArcadeUnit:
			case signArcadeSensor:
			case signArcadeMarker:
			case signStatic:
				_special = curPos;
				break;
			default:
				goto MoveSelection;
			}
			break;
		case IMSynchronize:
			switch (_infoClick._type)
			{
      case signArcadeUnit:
			case signArcadeSensor:
			case signArcadeWaypoint:
				_special = curPos;
				break;
			default:
				goto MoveSelection;
			}
			break;
		default:
		MoveSelection:
			if (GInput.keys[DIK_LSHIFT] > 0 || GInput.keys[DIK_RSHIFT] > 0)
			{
				// rotate
				Vector3 sum = VZero;
				int count = 0;
				_template->CalculateCenter(sum, count, true);
				if (count > 0)
				{
					Vector3 center = (1.0f / count) * sum;
					Vector3 oldDir = _lastPos - center;
					Vector3 dir = curPos - center;
					float angle = AngleDifference(atan2(dir.X(), dir.Z()), atan2(oldDir.X(), oldDir.Z()));
					_template->Rotate(center, angle, true);
				}
			}
			else
			{
				Vector3 offset = curPos - _lastPos;
				// move all selected items by offset
				for (int i=0; i<_template->emptyVehicles.Size(); i++)
					if (_template->emptyVehicles[i].selected)
					{
						Vector3 pos = _template->emptyVehicles[i].position + offset;
						pos[1] = GLOB_LAND->RoadSurfaceYAboveWater(pos[0], pos[2]);
						if (_template->UnitChangePosition(-1, i, pos))
						{
						}
					}
				for (int i=0; i<_template->sensors.Size(); i++)
					if (_template->sensors[i].selected)
					{
						Vector3 pos = _template->sensors[i].position + offset;
						pos[1] = GLOB_LAND->RoadSurfaceYAboveWater(pos[0], pos[2]);
						if (_template->SensorChangePosition(-1, i, pos))
						{
						}
					}
				for (int i=0; i<_template->markers.Size(); i++)
					if (_template->markers[i].selected)
					{
						Vector3 pos = _template->markers[i].position + offset;
						pos[1] = GLOB_LAND->RoadSurfaceYAboveWater(pos[0], pos[2]);
						if (_template->MarkerChangePosition(i, pos))
						{
						}
					}
				for (int i=0; i<_template->groups.Size(); i++)
				{
					ArcadeGroupInfo &gInfo = _template->groups[i];
					for (int j=0; j<gInfo.units.Size(); j++)
						if (gInfo.units[j].selected)
						{
							Vector3 pos = gInfo.units[j].position + offset;
							pos[1] = GLOB_LAND->RoadSurfaceYAboveWater(pos[0], pos[2]);
							if (_template->UnitChangePosition(i, j, pos))
							{
							}
						}
					for (int j=0; j<gInfo.sensors.Size(); j++)
						if (gInfo.sensors[j].selected)
						{
							Vector3 pos = gInfo.sensors[j].position + offset;
							pos[1] = GLOB_LAND->RoadSurfaceYAboveWater(pos[0], pos[2]);
							if (_template->SensorChangePosition(i, j, pos))
							{
							}
						}
					for (int j=0; j<gInfo.waypoints.Size(); j++)
						if (gInfo.waypoints[j].selected)
						{
							Vector3 pos = gInfo.waypoints[j].position + offset;
							pos[1] = GLOB_LAND->RoadSurfaceYAboveWater(pos[0], pos[2]);
							if (_template->WaypointChangePosition(i, j, pos))
							{
							}
						}
				}
			}
		}

		_lastPos = curPos;
	}
	else if (_selecting)
	{
		_lastPos = ScreenToWorld(DrawCoord(x, y));
	}

	CStaticMap::OnMouseHold(x, y, active);
}

// Arcade map display

DisplayArcadeMap::DisplayArcadeMap(ControlsContainer *parent, bool multiplayer, bool setDirectory)
: DisplayMapEditor(parent, Pars >> "RscDisplayArcadeMap")
{
	_enableSimulation = false;
	_enableDisplay = false;
//	GWorld->EnableDisplay(false);

	_setDirectory = setDirectory;
	_multiplayer = multiplayer;
	_running = false;

	_alwaysShow = true;
	_mode = IMUnits;

  // no mission was previewed
  _canContinue = false;

  _map = NULL; // FIX: when IDC_MAP control is not created first, other controls were trying to use it and crashed
  Load("RscDisplayArcadeMap");

	if (*Glob.header.filename)
	{
		char buffer[256];

		sprintf
		(
			buffer, "%smission.sqm",
			(const char *)GetMissionDirectory()
		);

		LoadTemplates(buffer);

		ArcadeUnitInfo *uInfo = _templateMission.FindPlayer(); 
		if (uInfo)
		{
			Glob.header.playerSide = (TargetSide)uInfo->side;
		}
	}

	if (_map)
	{
		_map->SetScale(-1); // default
		_map->Center();
		_map->Reset();
	}

	ShowButtons();
	UpdateIdsButton();
	UpdateTexturesButton();
  UpdateMissionName(false);
}

LSError DisplayArcadeMap::SerializeAll(ParamArchive &ar, bool merge)
{
	ATSParams params;
	ar.SetParams(&params);
	if (merge)
	{
		ArcadeTemplate t;
		CHECK(ar.Serialize("Mission", t, 1))
		_templateMission.Merge(t);
		CHECK(ar.Serialize("Intro", t, 1))
		_templateIntro.Merge(t);
		CHECK(ar.Serialize("OutroWin", t, 1))
		_templateOutroWin.Merge(t);
		CHECK(ar.Serialize("OutroLoose", t, 1))
		_templateOutroLoose.Merge(t);
		// do not merge cutscenes
	}
	else
	{
		CHECK(ar.Serialize("Mission", _templateMission, 1))
		CHECK(ar.Serialize("Intro", _templateIntro, 1))
		CHECK(ar.Serialize("OutroWin", _templateOutroWin, 1))
		CHECK(ar.Serialize("OutroLoose", _templateOutroLoose, 1))
		if (ar.IsLoading() && ar.GetArVersion() < 7)
		{
			ArcadeIntel intel;
			CHECK(ar.Serialize("Intel", intel, 1))
			_templateMission.intel = intel;
			_templateIntro.intel = intel;
			_templateOutroWin.intel = intel;
			_templateOutroLoose.intel = intel;
		}
	}
	return LSOK;
}

void DisplayArcadeMap::ReportMissingAddons()
{
  RString message = LocalizeString(IDS_MSG_ADDON_MISSING);
#ifndef _XBOX
  bool first = true;
  for (int i=0; i<_templateMission.missingAddOns.Size(); i++)
  {
    if (first) first = false;
    else message = message + RString(", ");
    message = message + _templateMission.missingAddOns[i];
  }
  for (int i=0; i<_templateIntro.missingAddOns.Size(); i++)
  {
    if (first) first = false;
    else message = message + RString(", ");
    message = message + _templateIntro.missingAddOns[i];
  }
  for (int i=0; i<_templateOutroWin.missingAddOns.Size(); i++)
  {
    if (first) first = false;
    else message = message + RString(", ");
    message = message + _templateOutroWin.missingAddOns[i];
  }
  for (int i=0; i<_templateOutroLoose.missingAddOns.Size(); i++)
  {
    if (first) first = false;
    else message = message + RString(", ");
    message = message + _templateOutroLoose.missingAddOns[i];
  }
#endif
  CreateMsgBox(MB_BUTTON_OK, message);
}

bool DisplayArcadeMap::LoadTemplates(const char *filename)
{
	QIFStreamB test;
	test.AutoOpen(filename);
	if (test.get() == 0)
	{
		// binary file
		return false;
	}

  GameDataNamespace globals(NULL, RString(), false); // save - no global namespace

	ParamArchiveLoad ar(filename, NULL, NULL, &globals);
	LSError result = SerializeAll(ar);
	if (result != LSOK)
	{
		if (result == LSNoAddOn)
		{
      ReportMissingAddons();
		}
		else
			CreateMsgBox(MB_BUTTON_OK, LocalizeString(IDS_MSG_LOAD_TEMPL_FAIL));
		_templateMission.Clear();
		_templateIntro.Clear();
		_templateOutroWin.Clear();
		_templateOutroLoose.Clear();
		return false;
	}

	_currentTemplate = &_templateMission;
	CCombo *combo = dynamic_cast<CCombo *>(GetCtrl(IDC_ARCMAP_SECTION));
	if (combo) combo->SetCurSel(0);
	if (_map) _map->SetTemplate(_currentTemplate);

	// loaded

	// warning for rad only files
	if (QIFileFunctions::FileReadOnly(filename))
	{
		CreateMsgBox(MB_BUTTON_OK, LocalizeString(IDS_MSG_TEMPL_READONLY));
	}
  UpdateMissionName(false);
	return true;
}

bool DisplayArcadeMap::MergeTemplates(const char *filename)
{
	QIFStreamB test;
	test.AutoOpen(filename);
	if (test.get() == 0)
	{
		// binary file
		return false;
	}

  GameDataNamespace globals(NULL, RString(), false); // save - no global namespace

	ParamArchiveLoad ar(filename, NULL, NULL, &globals);
	LSError result = SerializeAll(ar, true);
	if (result == LSNoAddOn)
	{
    ReportMissingAddons();
		return false;
	}
	else if (result != LSOK)
	{
		CreateMsgBox(MB_BUTTON_OK, LocalizeString(IDS_MSG_LOAD_TEMPL_FAIL));
		return false;
	}

//	_currentTemplate = &_templateMission;
  UpdateMissionName(true);
	return true;
}

bool DisplayArcadeMap::SaveTemplates(const char *filename)
{
	ParamArchiveSave ar(MissionsVersion);
	if (SerializeAll(ar) != LSOK) return false;
/*	
	LSError err = ar.Save(filename);
	if (err != LSOK)
	{
		if (err == LSAccessDenied)
			CreateMsgBox(MB_BUTTON_OK, LocalizeString(IDS_MSG_TEMPL_ACCESSDENIED));
		else
			CreateMsgBox(MB_BUTTON_OK, LocalizeString(IDS_MSG_SAVE_TEMPL_FAIL));
		return false;
	}
*/
	if (!ar.Save(filename))
	{
		CreateMsgBox(MB_BUTTON_OK, LocalizeString(IDS_MSG_SAVE_TEMPL_FAIL));
		return false;
	}
	return true;
}

void DisplayArcadeMap::UpdateMissionName(bool edited)
{
  _map->UpdateMissionName(edited);
}

WeatherState DisplayArcadeMap::GetWeather()
{
	return GetWeatherState(_currentTemplate->intel.weather);
}

// static const int daysInMonth[]={31,28,31,30,31,30,31,31,30,31,30,31};
int GetDaysInMonth(int year, int month);

Clock DisplayArcadeMap::GetTime()
{
	int day = _currentTemplate->intel.day - 1;
	int year = _currentTemplate->intel.year;
	for (int m=0; m<_currentTemplate->intel.month-1; m++) day += GetDaysInMonth(year, m);
	
	Clock clock;
  float time = OneHour * _currentTemplate->intel.hour + OneMinute * _currentTemplate->intel.minute;
	clock.SetTime(time, OneDay * day, year);
	return clock;
}

bool DisplayArcadeMap::GetPosition(Point3 &pos)
{
	ArcadeUnitInfo *me = _currentTemplate->FindPlayer();
	if (me)
	{
		pos = me->position;
		return true;
	}
	else
		return false;
}

void DisplayArcadeMap::ShowButtons()
{
	IControl *ctrl = GetCtrl(IDC_ARCMAP_PREVIEW);
	if (ctrl)
	{
		bool show = _currentTemplate->IsConsistent(NULL, _multiplayer, false);
		if (show && _multiplayer) show = Glob.header.filename[0] != 0;
		ctrl->ShowCtrl(show);
	}
	ctrl = GetCtrl(IDC_ARCMAP_CONTINUE);
	if (ctrl) ctrl->ShowCtrl(_canContinue);
}

///////////////////////////////////////////////////////////////////////////////
// Arcade Map display

Control *DisplayArcadeMap::OnCreateCtrl(int type, int idc, ParamEntryPar cls)
{
	switch (idc)
	{
	case IDC_MAP:
		{
			_map = new CStaticMapArcade(this, idc, cls, ERFull, 0.001, 1.0, 0.1);
			_map->SetTemplate(&_templateMission);
			return _map;
		}	
	case IDC_ARCMAP_SECTION:
		{
			CCombo *combo = new CCombo(this, idc, cls);
			combo->AddString(LocalizeString(IDS_SECTION_MISSION));
			combo->AddString(LocalizeString(IDS_SECTION_INTRO));
			combo->AddString(LocalizeString(IDS_SECTION_OUTRO_WIN));
			combo->AddString(LocalizeString(IDS_SECTION_OUTRO_LOOSE));
			combo->SetCurSel(0);
			return combo;
		}
	default:
		return DisplayMapEditor::OnCreateCtrl(type, idc, cls);
	}
}

/*!
\patch 1.53 Date 4/30/2002 by Jirka
- Added: mission editor - buttons "Show IDs" and "Show Textures"
\patch 5130 Date 2/21/2007 by Jirka
- Fixed: mission editor - condition changed when Continue button is shown
*/

void DisplayArcadeMap::OnButtonClicked(int idc)
{
	switch (idc)
	{
		case IDC_ARCMAP_LOAD:
			CreateChild(new DisplayTemplateLoad(this));
			return;
		case IDC_ARCMAP_MERGE:
			CreateChild(new DisplayTemplateLoad(this, true));
			return;
		case IDC_ARCMAP_SAVE:
			CreateChild(new DisplayTemplateSave(this));
			return;
		case IDC_ARCMAP_INTEL:
			CreateChild(new DisplayIntel(this, _currentTemplate->intel));
			return;
    case IDC_ARCMAP_CLEAR: 
    {
      MsgBoxButton buttonOK(LocalizeString(IDS_DISP_XBOX_HINT_YES), INPUT_DEVICE_XINPUT + XBOX_A);
      MsgBoxButton buttonCancel(LocalizeString(IDS_DISP_XBOX_HINT_NO), INPUT_DEVICE_XINPUT + XBOX_B);
			CreateMsgBox(
				MB_BUTTON_OK | MB_BUTTON_CANCEL,
				LocalizeString(IDS_SURE),
				IDD_MSG_CLEARTEMPLATE, false, &buttonOK, &buttonCancel);
			return;
    }
		case IDC_CANCEL:
    {
      MsgBoxButton buttonOK(LocalizeString(IDS_DISP_XBOX_HINT_YES), INPUT_DEVICE_XINPUT + XBOX_A);
      MsgBoxButton buttonCancel(LocalizeString(IDS_DISP_XBOX_HINT_NO), INPUT_DEVICE_XINPUT + XBOX_B);
			CreateMsgBox(
				MB_BUTTON_OK | MB_BUTTON_CANCEL,
				LocalizeString(IDS_SURE),
				IDD_MSG_EXITTEMPLATE, false, &buttonOK, &buttonCancel);
			return;
    }
		case IDC_ARCMAP_PREVIEW:
			if (_running) return;
			_running = true;
			if (_currentTemplate->IsConsistent(this, _multiplayer)) 
			{
				_currentTemplate->ScanRequiredAddons(Glob.header.worldname);
				_currentTemplate->CheckObjectIds();

				if (_multiplayer)
				{
					// save current version
					RString directory;
					char buffer[256];
					strcpy(buffer, GetMissionsDirectory());
					::CreateDirectory(buffer, NULL);
					strcat(buffer, Glob.header.filename);
					strcat(buffer, ".");
					strcat(buffer, Glob.header.worldname);
					directory = buffer;
					::CreateDirectory(directory, NULL);
					strcat(buffer, "\\mission.sqm");
					SaveTemplates(buffer);

					Exit(IDC_OK);
					return;
				}

				CurrentTemplate = *_currentTemplate;
				_alwaysShow = false;

        bool DeleteWeaponsFile();
        DoVerify(DeleteWeaponsFile());

        GameMode gameMode = _currentTemplate == &_templateMission ? GModeArcade : GModeIntro;

        Ref<ProgressHandle> p = GWorld->BegLoadMissionProgress(Glob.header.worldname, CurrentTemplate, gameMode);

        GWorld->SwitchLandscape(Glob.header.worldname);
				GWorld->ActivateAddons(CurrentTemplate.addOns,Glob.header.worldname);
				GWorld->InitGeneral(CurrentTemplate.intel);

        // Continue is valid only if the mission is loaded correctly
        _canContinue = false;

        if (_currentTemplate == &_templateMission)
				{
					GStats.ClearAll();
					bool ok = GWorld->InitVehicles(GModeArcade, CurrentTemplate);
          ProgressFinish(p);

					if (ok)
					{
            bool DeleteSaveGames();
            DeleteSaveGames();
            bool DeleteWeaponsFile();
            DeleteWeaponsFile();

						if 
						(
							(GInput.keys[DIK_LSHIFT] > 0 || GInput.keys[DIK_RSHIFT] > 0) &&
							GetBriefingFile().GetLength() > 0
						)
						{
							CreateChild(new DisplayGetReady(this));
							_running = false;
							return;
						}
            // Continue is valid now
            _canContinue = true;
						CreateChild(new DisplayMission(this, true));
					}
					else
					{
						_alwaysShow = true;
					}
				}
				else
				{
					bool ok = GWorld->InitVehicles(GModeIntro, CurrentTemplate);
          ProgressFinish(p);

					if (ok)
					{
						CreateChild(new DisplayIntro(this, DisplayIntro::noInit, false));
					}
					else
					{
						_alwaysShow = true;
					}
				}
			}
			_running = false;
			return;
		// TODO: remove continue button
		case IDC_ARCMAP_CONTINUE:
			if (_canContinue)
			{
				_alwaysShow = false;
        
        // recover camera settings
        GWorld->SwitchCameraTo(_backupCamera, GWorld->GetCameraType(), true);
        GWorld->SwitchPlayerTo(_backupPlayer);
        GWorld->SetRealPlayer(_backupRealPlayer);

        if (_backupRealPlayer && _backupRealPlayer->Brain())
        {
          Glob.header.playerSide = _backupRealPlayer->Brain()->GetSide();
        }

				CreateChild(new DisplayMission(this));
			}
			return;
		case IDC_ARCMAP_IDS:
			if (_map)
			{
				_map->ShowIds(!_map->IsShowingIds());
				UpdateIdsButton();
			}
			break;
		case IDC_ARCMAP_TEXTURES:
			if (_map)
			{
				_map->ShowScale(!_map->IsShowingScale());
				UpdateTexturesButton();
			}
			break;
		default:
			DisplayMapEditor::OnButtonClicked(idc);
			break;
	}
}

void DisplayArcadeMap::UpdateIdsButton()
{
	CButton *button = dynamic_cast<CButton *>(GetCtrl(IDC_ARCMAP_IDS));
	if (button && _map)
	{
		if (_map->IsShowingIds())
			button->SetText(LocalizeString(IDS_ARCMAP_HIDE_IDS));
		else
			button->SetText(LocalizeString(IDS_ARCMAP_SHOW_IDS));
	}
}

void DisplayArcadeMap::UpdateTexturesButton()
{
	CButton *button = dynamic_cast<CButton *>(GetCtrl(IDC_ARCMAP_TEXTURES));
	if (button && _map)
	{
		if (_map->IsShowingScale())
			button->SetText(LocalizeString(IDS_ARCMAP_SHOW_TEXTURES));
		else
			button->SetText(LocalizeString(IDS_ARCMAP_HIDE_TEXTURES));
	}
}

void DisplayArcadeMap::OnLBSelChanged(IControl *ctrl, int curSel)
{
	switch (ctrl->IDC())
	{
		case IDC_ARCMAP_SECTION:
			if (curSel == 0)
				_currentTemplate = &_templateMission;
			else if (curSel == 1)
				_currentTemplate = &_templateIntro;
			else if (curSel == 2)
				_currentTemplate = &_templateOutroWin;
			else if (curSel == 3)
				_currentTemplate = &_templateOutroLoose;
      if (_map)
      {
        _map->_infoClick._type = signNone;
        _map->_infoClickCandidate._type = signNone;
        _map->_infoMove._type = signNone;
        _map->SetTemplate(_currentTemplate);
      }
			// Intel does not change
			ArcadeUnitInfo *uInfo = _currentTemplate->FindPlayer(); 
			if (uInfo)
			{
				Glob.header.playerSide = (TargetSide)uInfo->side;
			}
			ShowButtons();
			break;
	}
	Display::OnLBSelChanged(ctrl, curSel);
}

void DisplayArcadeMap::OnToolBoxSelChanged(int idc, int curSel)
{
	if (idc == IDC_ARCMAP_MODE)
	{
		_mode = (InsertMode)curSel;
	}
}

void DisplayArcadeMap::OnSimulate(EntityAI *vehicle)
{
	DisplayMapEditor::OnSimulate(vehicle);
	if (_map) _map->ProcessCheats();
#if _ENABLE_EDITOR2 && _ENABLE_CHEATS
	if (GInput.GetCheat1ToDo(DIK_Z) && _currentTemplate)
  {
    void ExportMission(ArcadeTemplate &t, RString filename);
    ExportMission(*_currentTemplate, GetMissionDirectory() + RString("mission"));
  }
  if (GInput.GetCheat1ToDo(DIK_NUMPAD0))
  {
    bool RunScriptedMission(GameMode mode);
    if (!RunScriptedMission(GModeArcade)) return;

    _alwaysShow = false;

    if (GetBriefingFile().GetLength() > 0)
      CreateChild(new DisplayGetReady(this));
    else
      CreateChild(new DisplayMission(this, true));
  }
#endif // _ENABLE_EDITOR2 && _ENABLE_CHEATS
}

bool DisplayArcadeMap::OnUnregisteredAddonUsed( RString addon )
{
	_currentTemplate->addOns.AddUnique(addon);
	return true;
}

#ifdef _WIN32

struct SendViaEmailBackgroundContext
{
	HMODULE hLib;
	MapiMessage *msg;
};

static DWORD WINAPI SendViaEmailBackground(void *ctxv)
{
	SendViaEmailBackgroundContext *ctx = (SendViaEmailBackgroundContext *)ctxv;
	LPMAPISENDMAIL fnc = (LPMAPISENDMAIL)GetProcAddress(ctx->hLib, "MAPISendMail");
	if (fnc)
	{
		ULONG res = (*fnc)
		(
			0, 0, ctx->msg, MAPI_DIALOG | MAPI_NEW_SESSION, 0
		);
		return res;
	}
	return SUCCESS_SUCCESS;
}

void ProcessMessagesNoWait();

static ULONG SendViaEmailProcessMessages(HMODULE hLib, MapiMessage &msg)
{
	SendViaEmailBackgroundContext ctx;
	ctx.hLib = hLib;
	ctx.msg = &msg;
	DWORD threadId;
	HANDLE threadHandle = ::CreateThread
	(
		NULL,64*1024,SendViaEmailBackground,&ctx,CREATE_SUSPENDED,&threadId
	);

	if (!threadHandle)
	{
		return SendViaEmailBackground(&ctx);
	}
	else
	{
		::ResumeThread(threadHandle);
		for (;;)
		{
			DWORD ret = ::MsgWaitForMultipleObjects(1,&threadHandle,FALSE,500,QS_ALLEVENTS);
			// check if thread terminated
			if (ret==WAIT_OBJECT_0) break;
			ProcessMessagesNoWait();
		}
		ULONG ret = 0;
		BOOL ok = GetExitCodeThread(threadHandle,&ret);
		if (!ok) ret = 0;
		::CloseHandle(threadHandle);
		return ret;
	}
}

#endif

void EnableDesktopCursor(bool enable);

/*!
\patch 1.85 Date 9/18/2002 by Ondra
- Fixed: Save/Send by e-mail did not change focus to e-mail application.
*/

static void SendViaEmail(const char *fileName)
{
#ifdef _WIN32
	// send file
	char currentDirectory[1024];
	GetCurrentDirectory(1024, currentDirectory);

	MapiFileDesc MapiFile;
	memset(&MapiFile, 0, sizeof(MapiFile));
	MapiFile.nPosition = 0xFFFFFFFF;

  RString fullName;
  if (strchr(fileName, ':') != NULL)
  {
    // full path is given
    fullName = fileName;
  }
  else
  {
    fullName = RString(currentDirectory) + RString("\\") + RString(fileName);
  }
	MapiFile.lpszPathName = (char *)cc_cast(fullName);

	MapiMessage MapiMsg;
	memset(&MapiMsg, 0, sizeof(MapiMsg));
  MapiMsg.lpszSubject = APP_NAME " Mission";
	MapiMsg.lpszNoteText = "Copy to your mission directory.";
//							MapiMsg.nRecipCount = 1;
//							MapiMsg.lpRecips = &MapiRecp;
	MapiMsg.nFileCount = 1;
	MapiMsg.lpFiles = &MapiFile;

	
	GDebugger.PauseCheckingAlive();
	EnableDesktopCursor(true);

	HINSTANCE hLib = LoadLibrary("mapi32.dll");
	if (hLib != NULL)
	{
		ULONG res = SendViaEmailProcessMessages(hLib,MapiMsg);
    if (res == MAPI_E_USER_ABORT)
    {
      WarningMessage("The mail was not sent.");
    }
		else if (res != SUCCESS_SUCCESS)
		{
			WarningMessage("MAPI error, mail not sent.");
		}

		FreeLibrary(hLib);
	}
	else
	{
		WarningMessage("MAPI library not found.");
	}
	EnableDesktopCursor(false);
	GDebugger.ResumeCheckingAlive();

	// MAPI function can change it
	SetCurrentDirectory(currentDirectory);
#endif
}

#if _VBS2 && _VBS2_LITE
void VBS2EncryptMission(RString fileName,RString directory)
{
  AutoArray<QFProperty> properties;
  QFProperty property;
  property.name = "encryption";
  property.value = RString("VBS2M");
  properties.Add(property);
  
  Ref<IFilebankEncryption> encrypt;
  encrypt = CreateFilebankEncryption("VBS2M","password");
  
  if(encrypt)
  {
    DoVerify(QIFileFunctions::CleanUpFile(fileName));

    QOFStream out;
    out.open(fileName);  

    FileBankManager man;
    man.Create(
      out,directory,encrypt,
      properties.Data(),properties.Size()
    );
    out.close();
  }
}
#endif

/*!
\patch 1.28 Date 10/22/2001 by Jirka
- Fixed: Editing of waypoints in simple mode do not change order now.
\patch 1.43 Date 1/29/2002 by Jirka
- Added: Enable briefing in mission editor preview (hold SHIFT when pressing "Preview" button)
\patch 1.77 Date 6/26/2002 by Jirka
- Fixed: Mission editor - after load mission from other island object labels was not updated
\patch_internal 1.85 Date 9/18/2002 by Ondra
- Fixed: Send by e-mail caused application was detected as frozen and terminated.
*/

void DisplayArcadeMap::OnChildDestroyed(int idd, int exit)
{
	switch (idd)
	{
		case IDD_INTEL_GETREADY:
			{
				DisplayMapEditor::OnChildDestroyed(idd, exit);
				if (exit != IDC_CANCEL)
				{
					CreateChild(new DisplayMission(this));
				}
			}
			break;
		case IDD_MISSION:
		{
      _backupCamera = GWorld->CameraOn();
      _backupPlayer = GWorld->PlayerOn();
      _backupRealPlayer = GWorld->GetRealPlayer();

      ArcadeUnitInfo *uInfo = _currentTemplate->FindPlayer();
      if (uInfo)
      {
        Glob.header.playerSide = (TargetSide)uInfo->side;
      }

			DisplayMapEditor::OnChildDestroyed(idd, exit);
      if (exit == IDC_MAIN_QUIT)
      {
        goto StartOutro;
      }
			ConstParamEntryPtr entry = ExtParsMission.FindEntry("debriefing");
			if (entry && !(bool)(*entry))
			{
				GStats.Update();
				goto StartOutro;
			}
			else
				CreateChild(new DisplayDebriefing(this, false));
		}
		break;
		case IDD_DEBRIEFING:
		case IDD_INTRO:
			DisplayMapEditor::OnChildDestroyed(idd, exit);
			GWorld->DestroyMap(IDC_OK);
StartOutro:
			_alwaysShow = true;
			ShowButtons();
			break;
		case IDD_ARCADE_UNIT:
			if (exit == IDC_OK)
			{
				DisplayArcadeUnit *display = dynamic_cast<DisplayArcadeUnit *>((ControlsContainer *)_child);
				Assert(display);

				int ig = display->_indexGroup;
				int iu = display->_index;

				_lastUnit = display->_unit;

				_currentTemplate->ClearSelection();
				display->_unit.selected = true;
				_currentTemplate->UnitUpdate
				(
					ig, iu,
					display->_unit
				);

				_currentTemplate->IsConsistent(this, _multiplayer);
				ShowButtons();

        if (_map)
        {
          _map->_infoClick._type = signArcadeUnit;
          _map->_infoClick._indexGroup = ig;
          _map->_infoClick._index = iu;
        }
			  UpdateMissionName(true);
      }
			DisplayMapEditor::OnChildDestroyed(idd, exit);
			break;
    case IDD_ARCADE_MODULES:
      if (exit == IDC_OK)
      {
        DisplayArcadeModules *display = dynamic_cast<DisplayArcadeModules *>((ControlsContainer *)_child);
        Assert(display);

        int ig = display->_indexGroup;
        int iu = display->_index;

        _lastLogic = display->_unit;

        _currentTemplate->ClearSelection();
        display->_unit.selected = true;
        _currentTemplate->UnitUpdate
          (
          ig, iu,
          display->_unit
          );

        //_currentTemplate->IsConsistent(this, _multiplayer);
        //ShowButtons();

        if (_map)
        {
          _map->_infoClick._type = signArcadeUnit;
          _map->_infoClick._indexGroup = ig;
          _map->_infoClick._index = iu;
        }
        UpdateMissionName(true);
      }
      DisplayMapEditor::OnChildDestroyed(idd, exit);
      break;
		case IDD_ARCADE_GROUP:
      if (exit == IDC_OK)
      {
        DisplayArcadeGroup *display = dynamic_cast<DisplayArcadeGroup *>((ControlsContainer *)_child);
        Assert(display);

        Vector3Val position = display->_position;

        float azimut = 0;
        CEdit *edit = dynamic_cast<CEdit *>(display->GetCtrl(IDC_ARCGRP_AZIMUT));
        if (edit) azimut = edit->GetText() ? atof(edit->GetText()) : 0;

        CCombo *combo = dynamic_cast<CCombo *>(display->GetCtrl(IDC_ARCGRP_SIDE));
        if (combo && combo->GetCurSel() >= 0)
        {
          RString side = combo->GetData(combo->GetCurSel());

          combo = dynamic_cast<CCombo *>(display->GetCtrl(IDC_ARCGRP_FACTION));
          if (combo && combo->GetCurSel() >= 0)
          {
            RString faction = combo->GetData(combo->GetCurSel());

            combo = dynamic_cast<CCombo *>(display->GetCtrl(IDC_ARCGRP_TYPE));
            if (combo && combo->GetCurSel() >= 0)
            {
              RString type = combo->GetData(combo->GetCurSel());

              combo = dynamic_cast<CCombo *>(display->GetCtrl(IDC_ARCGRP_NAME));
              if (combo && combo->GetCurSel() >= 0)
              {
                RString name = combo->GetData(combo->GetCurSel());

                _currentTemplate->ClearSelection();
                _currentTemplate->AddGroup(Pars >> "CfgGroups" >> side >> faction >> type >> name, position);
                int index = _currentTemplate->groups.Size() - 1;
                _currentTemplate->groups[index].Rotate(position, (H_PI / 180.0f) * azimut, false);

                if (_map)
                {
                  _map->SetLastGroup(side,faction, type, name);
                  _map->_infoClick._type = signNone;
                  _map->_infoClickCandidate._type = signNone;
                }

                _currentTemplate->IsConsistent(this, _multiplayer);
                ShowButtons();
              }
            }
          }
        }
        UpdateMissionName(true);
      }
			DisplayMapEditor::OnChildDestroyed(idd, exit);
			break;
		case IDD_ARCADE_SENSOR:
			if (exit == IDC_OK)
			{
				DisplayArcadeSensor *display = dynamic_cast<DisplayArcadeSensor *>((ControlsContainer *)_child);
				Assert(display);

				_currentTemplate->ClearSelection();
				display->_sensor.selected = true;
				_currentTemplate->SensorUpdate
				(
					display->_ig, display->_index,
					display->_sensor
				);

        if (_map)
        {
          _map->_infoClick._type = signArcadeSensor;
          _map->_infoClick._indexGroup = display->_ig;
          _map->_infoClick._index = display->_index;
        }
        UpdateMissionName(true);
			}
			DisplayMapEditor::OnChildDestroyed(idd, exit);
			break;
		case IDD_ARCADE_MARKER:
			if (exit == IDC_OK)
			{
				DisplayArcadeMarker *display = dynamic_cast<DisplayArcadeMarker *>((ControlsContainer *)_child);
				Assert(display);

				_currentTemplate->ClearSelection();
				display->_marker.selected = true;
				_currentTemplate->MarkerUpdate
				(
					display->_index,
					display->_marker
				);

        if (_map)
        {
          _map->_infoClick._type = signArcadeMarker;
          _map->_infoClick._index = display->_index;
        }
        UpdateMissionName(true);
			}
			DisplayMapEditor::OnChildDestroyed(idd, exit);
			break;
		case IDD_ARCADE_WAYPOINT:
			if (exit == IDC_OK)
			{
				DisplayArcadeWaypoint *display = dynamic_cast<DisplayArcadeWaypoint *>((ControlsContainer *)_child);
				Assert(display);

				int ig = display->_indexGroup;
				int iw = display->_index;

				int iwnew = iw < 0 ? _currentTemplate->groups[ig].waypoints.Size() : iw;
				CCombo *combo = dynamic_cast<CCombo *>(display->GetCtrl(IDC_ARCWP_SEQ));
				if (combo)
				{
					iwnew = combo->GetCurSel();
					if (iw < 0 && iwnew < 0) iwnew = combo->GetSize() - 1;
				}

				_currentTemplate->ClearSelection();
				display->_waypoint.selected = true;
				_currentTemplate->WaypointUpdate
				(
					ig, iw, iwnew,
					display->_waypoint
				);

        if (_map)
        {
          _map->_infoClick._type = signArcadeWaypoint;
          _map->_infoClick._indexGroup = ig;
          _map->_infoClick._index = iwnew;
        }
        UpdateMissionName(true);
			}
			DisplayMapEditor::OnChildDestroyed(idd, exit);
			break;
		case IDD_TEMPLATE_SAVE:
			if (exit == IDC_OK)
			{
				CEdit *edit = dynamic_cast<CEdit *>(_child->GetCtrl(IDC_TEMPL_NAME));
				if (!edit)
				{
					DisplayMapEditor::OnChildDestroyed(idd, exit);
					break;
				}
				RString text = EncodeFileName(edit->GetText());
				if (text.GetLength() > 0)
				{
					if (!_setDirectory)
						SetMission(Glob.header.worldname, text, GetBaseSubdirectory());
					else if (_multiplayer)
						SetMission(Glob.header.worldname, text, MPMissionsDir);
					else
						SetMission(Glob.header.worldname, text, MissionsDir);

					RString directory;
					char buffer[256];
					strcpy(buffer, GetMissionsDirectory());
					::CreateDirectory(buffer, NULL);
					strcat(buffer, text);
					strcat(buffer, ".");
					strcat(buffer, Glob.header.worldname);
					directory = buffer;
					::CreateDirectory(directory, NULL);
					strcat(buffer, "\\mission.sqm");
					SaveTemplates(buffer);

#ifndef _XBOX
					CCombo *combo = dynamic_cast<CCombo *>(_child->GetCtrl(IDC_TEMPL_MODE));
					if (!combo)
					{
						DisplayMapEditor::OnChildDestroyed(idd, exit);
						break;
					}
					switch (combo->GetCurSel())
					{
					case 0:
						// user mission
						break;
					case 1:
						// single mission
						{
							RString fileName =
								RString("Missions\\") +
								text + RString(".") +
								RString(Glob.header.worldname) + RString(".pbo");
#if _VBS2 && _VBS2_LITE
              VBS2EncryptMission(fileName,directory);
#else
							DoVerify(QIFileFunctions::CleanUpFile(fileName));
							FileBankManager mgr;
							mgr.Create(fileName, directory);
#endif
						}
						break;
					case 2:
						// multiplayer mission
						{
							RString fileName =
								RString("MPMissions\\") +
								text + RString(".") +
								RString(Glob.header.worldname) + RString(".pbo");
#if _VBS2 && _VBS2_LITE
              VBS2EncryptMission(fileName,directory);
#else
							DoVerify(QIFileFunctions::CleanUpFile(fileName));
							FileBankManager mgr;
							mgr.Create(fileName, directory);
#endif
						}
						break;
					case 3:
						// send by e-mail
						#ifdef _WIN32
						{
							// create file
							RString fileName = directory + RString(".pbo");
#if _VBS2 && _VBS2_LITE
              VBS2EncryptMission(fileName,directory);
#else
							DoVerify(QIFileFunctions::CleanUpFile(fileName));
							FileBankManager mgr;
							mgr.Create(fileName, directory);
#endif
							SendViaEmail(fileName);
							DoVerify(QIFileFunctions::CleanUpFile(fileName));
						}
						#endif
						break;
					}
#endif
					ShowButtons();
				}
        UpdateMissionName(false);
			}
			DisplayMapEditor::OnChildDestroyed(idd, exit);
			break;
		case IDD_TEMPLATE_LOAD:
			if (exit == IDC_OK)
			{
				CCombo *combo = dynamic_cast<CCombo *>(_child->GetCtrl(IDC_TEMPL_ISLAND));
				if (!combo)
				{
					DisplayMapEditor::OnChildDestroyed(idd, exit);
					break;
				}
				int index = combo->GetCurSel();
				Assert(index >= 0);
				RString island = combo->GetData(index);
				combo = dynamic_cast<CCombo *>(_child->GetCtrl(IDC_TEMPL_NAME));
				if (!combo)
				{
					DisplayMapEditor::OnChildDestroyed(idd, exit);
					break;
				}

        RString name;
				int i = combo->GetCurSel();
        if (i >= 0) name = combo->GetData(i);

				if (name.GetLength() > 0)
				{
					RString filename = GetMissionsDirectory() +
						name + RString(".") +
						island + RString("\\mission.sqm");
					DisplayTemplateLoad *display = dynamic_cast<DisplayTemplateLoad *>((ControlsContainer *)_child);
					Assert(display);
					if (display->_merge)
						MergeTemplates(filename);
					else
					{
						if (stricmp(island, Glob.header.worldname) != 0)
						{
              OnHide();
							GWorld->SwitchLandscape(island);
						}

						if (!_setDirectory)
							SetMission(island, name, GetBaseSubdirectory());
						else if (_multiplayer)
							SetMission(island, name, MPMissionsDir);
						else
							SetMission(island, name, MissionsDir);
						LoadTemplates(filename);
						ArcadeUnitInfo *uInfo = _currentTemplate->FindPlayer();
						if (uInfo)
						{
							Glob.header.playerSide = (TargetSide)uInfo->side;
						}
					}
					if (_map)
					{
						_map->SetScale(-1); // default
						_map->Center();
						_map->Reset();
					}
				}
        else
        {
					if (stricmp(island, Glob.header.worldname) != 0)
					{
            OnHide();
						GWorld->SwitchLandscape(island);
					}

          // clear all templates and their intels
          _templateMission.Clear();
          _templateMission.intel.Init();
          _templateIntro.Clear();
          _templateIntro.intel.Init();
          _templateOutroWin.Clear();
          _templateOutroWin.intel.Init();
          _templateOutroLoose.Clear();
          _templateOutroLoose.intel.Init();
          // set the mission as the current template
          _currentTemplate = &_templateMission;
          CCombo *combo = dynamic_cast<CCombo *>(GetCtrl(IDC_ARCMAP_SECTION));
          if (combo) combo->SetCurSel(0);

				  if (!_setDirectory)
					  SetMission(Glob.header.worldname, "", GetBaseSubdirectory());
				  else if (_multiplayer)
					  SetMission(Glob.header.worldname, "", MPMissionsDir);
				  else
					  SetMission(Glob.header.worldname, "", MissionsDir);
				  if (_map)
				  {
					  _map->_infoClick._type = signNone;
					  _map->_infoClickCandidate._type = signNone;
					  _map->_infoMove._type = signNone;
						_map->SetScale(-1); // default
						_map->Center();
						_map->Reset();
				  }
        }
				ShowButtons();
			}
			DisplayMapEditor::OnChildDestroyed(idd, exit);
			break;
		case IDD_INTEL:
			if (exit == IDC_OK)
			{
				DisplayIntel *display = dynamic_cast<DisplayIntel *>((ControlsContainer *)_child);
				Assert(display);
				_currentTemplate->intel = display->_intel;
        UpdateMissionName(true);
			}
			DisplayMapEditor::OnChildDestroyed(idd, exit);
			break;
		case IDD_MSG_CLEARTEMPLATE:
			if (exit == IDC_OK)
			{
				_currentTemplate->Clear();
				if (!_setDirectory)
					SetMission(Glob.header.worldname, "", GetBaseSubdirectory());
				else if (_multiplayer)
					SetMission(Glob.header.worldname, "", MPMissionsDir);
				else
					SetMission(Glob.header.worldname, "", MissionsDir);
				if (_map)
				{
					_map->_infoClick._type = signNone;
					_map->_infoClickCandidate._type = signNone;
					_map->_infoMove._type = signNone;
				}
				ShowButtons();
        UpdateMissionName(true);
			}
			DisplayMapEditor::OnChildDestroyed(idd, exit);
			break;
		case IDD_MSG_EXITTEMPLATE:
			if (exit == IDC_OK)
			{
				Exit(IDC_CANCEL);
			}
			DisplayMapEditor::OnChildDestroyed(idd, exit);
			break;
		default:
			DisplayMapEditor::OnChildDestroyed(idd, exit);
			break;
	}
}

void DisplayArcadeMap::OnHide()
{
  if (_map) _map->OnHide();
}

bool DisplayArcadeMap::CanDestroy()
{
	if (!DisplayMapEditor::CanDestroy()) return false;

	if (_exit == IDC_OK) 
		return _templateMission.IsConsistent(this, _multiplayer);
	else
		return true;
}

void DisplayArcadeMap::Destroy()
{
	DisplayMapEditor::Destroy();
//	GWorld->EnableDisplay(true);
}

/*
const EnumName *GetEnumNames(ObjectId id)
{
	// this function is required by the linker, but the reason is unknown
	// I suppose it might should be a compiler/linker error
	// it someone reaches this point, be sure to send callstack to crash NG
	ErrF("Compiler / linker error? - archive callstack wanted!!!");
	Fail("This function should be never used???");
	return NULL;
}
*/
#endif // #if _ENABLE_EDITOR
