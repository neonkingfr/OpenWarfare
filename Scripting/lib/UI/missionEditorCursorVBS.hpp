/*!
\file
Mission editor cursor
*/

#ifdef _MSC_VER
#pragma once
#endif

#ifndef _MISSION_EDITOR_CURSOR_HPP
#define _MISSION_EDITOR_CURSOR_HPP

#if _ENABLE_EDITOR2

#include "../vehicle.hpp"

struct EditorProxy
{
  EditorObject *edObj;
  OLink(Object) object;
  RString id;
  // used with raising objects - avoids double move if 2 editor objects share a proxy object
  bool isMoved;
  // proxy only visible in the editor?
  bool proxyVisibleInPreview;
  EditorProxy() {isMoved = false; proxyVisibleInPreview = true;}
};
TypeContainsOLink(EditorProxy)

class MissionEditCursorContainer
{
protected:
  AutoArray<EditorProxy> _proxies;

public:
  const EditorProxy *FindProxy(Vector3Par pos, GameState *gstate) const;
  const EditorProxy *FindProxy(Object *obj, bool onlyVisibleIn3D = false) const;
  void AddProxy(Object *object,EditorObject *obj);
  void UpdateProxy(Object *object,EditorObject *edObj);
  void DeleteProxy(EditorObject *edObj);

  // events
  virtual void OnObjectMoved(EditorObject *obj, Vector3Par offset) = NULL;
  virtual void RotateObject(EditorObject *obj, Vector3Par center, float angle) = NULL;
  virtual bool IsDragging() = NULL;
};

// mission editor camera
#include "../cameraHold.hpp"

#define MAX_CAMERA_SPEED 25

class MissionEditorCamera : public CameraVehicle
{
  typedef CameraVehicle base;

protected:
  // currently selected proxy object
  //const EditorProxy *_selected;

  // records the last mouse position when a proxy object is selected by left-click
    // used to smooth out dragging so the selected proxy object doesn't jump to the mouse position
  Vector3 _lastPos;

  // this is the marker used to highlight proxy objects when the mouse is over them
  Ref<Texture> _selectedCursor;
  float _selectedCursorWidth, _selectedCursorHeight;

  Ref<Texture> _icon2D;
  float _icon2Dh, _icon2Dw;
  PackedColor _icon2Dcolor;

  // objects to ignore when detecting ground intercept
  OLinkArray(Object) _ignoreObjects;

  // set to true by camera movement code, allows camera to freelook again
  bool _resetTargets;

  // used to prevent mouse triggering on-edge camera movement if a unit was recently selected
  bool _dragging;

  // lock onto target?
  bool _lockToTarget;

  // allow move on edge
  bool _allowMoveOnEdge;

  // moving camera quickly
  bool _isMoving;

  // camera should rotate?
  bool _isRotating;

  // maximum speed of camera
  float _maxSpeed;

  // contained by _parent, simple pointer is OK
  MissionEditCursorContainer *_parent;

  PackedColor _color;

  // settings from config
  float _speedFwdBack;
  float _speedLeftRight;
  float _speedUpDown;
  float _speedRotate;
  float _speedElevation;
  float _speedTurboMultiplier;

public:
  MissionEditorCamera(MissionEditCursorContainer *parent);
  ~MissionEditorCamera();

  float MaxSpeed();
  float SpeedFwdBack() {return _speedFwdBack;}
  float SpeedUpDown() {return _speedUpDown;}
  float SpeedLeftRight() {return _speedLeftRight;}

  bool IsDragging() {return _dragging;}
  bool IsRotating() {return _isRotating;}
  void SetRotating(bool rotating) {_isRotating = rotating;}

  void Simulate(float deltaT, SimulationImportance prec, GameState *gstate, bool cursorOverCamera);
  void ResetTargets();

  void LookAt(Object *target, bool lockCamera = false, Vector3 pos = Vector3());
  void LookAt(Vector3Par target);

  bool IsLocked() {return _lockToTarget;}
  void UnLock();
  void LockTo(Object *lockTo);

  //const EditorProxy *SelectedProxy() const {return _selected;}
  //void ClearSelected() {_selected = NULL;}

  bool IsMoving() {return _isMoving;}

  PackedColor SelectedColor() {return _color;}
  void DrawSelectedIcon(Vector3Val pos, PackedColor color);

  Ref<Texture> GetIcon2D() {return _icon2D;}
  float GetHeightIcon2D() {return _icon2Dh;}
  float GetWidthIcon2D() {return _icon2Dw;}
  PackedColor GetColorIcon2D() {return _icon2Dcolor;}  

  // get ground intercept of current mouse x,y
  bool GetGroundIntercept(Vector3& isect, bool groundOnly = true, bool subSurface = false, bool camOnly = false);

  // get ground intercept of specified x,y
  bool GetGroundIntercept(Vector3& isect, float x, float y, bool groundOnly = true, bool subSurface = false, bool camOnly = false);

  // ignore objects
  void ClearIgnoreObjects() {_ignoreObjects.Clear();}
  void AddIgnoreObject(Object *obj) {_ignoreObjects.Add(obj);}

  // return direction of cursor
  Vector3 GetCursorDir();

  bool DrawIcon
  (
    Vector3Val dir, Texture *texture, float width, float height, PackedColor color, bool shadow
  );

  void DrawLine
  (
    Vector3Val from, Vector3Val to, PackedColor color, bool shadow
  );
};

#endif // _ENABLE_EDITOR2

#endif
