#ifdef _MSC_VER
#pragma once
#endif

#ifndef _LIGHTS_HPP
#define _LIGHTS_HPP

#include <El/Math/math3d.hpp>
#include "visual.hpp"
#include "vehicle.hpp"

#define LIGHT_SATURATION 0.3f

// abstract light class

struct LightContext
{
  Vector3 position;
  float radius;
};

class ParamEntry;
struct TLMaterial;

enum LightKind
{
  LTDirectional,
  LTPoint,
  LTSpotLight
};
struct LightDescription // D3D-like light description
{
  Vector3 pos,dir;
  Color diffuse,ambient;
  LightKind type;
  float startAtten; // distance with intensity 1, falloff starts here
  // for spotlight only: inner and outer cone angles
  float theta,phi; // see corresponding D3DLIGHT8 members
  float tgPhiHalf;
};

class Light: public RefCountWithLinks, virtual public Frame
{
protected:
  bool _on;

  public:
  virtual float FlareIntensity( Vector3Par camPos, Vector3Par camDir ) const {return 0;}
  virtual void Prepare( const Matrix4 &worldToModel ) = 0;
  virtual void SetMaterial(const TLMaterial &mat, ColorVal accom) = 0;
  virtual Color ApplyPrecalc( Vector3Par pos, Vector3Par normal ) = 0;
  virtual Color Apply( Vector3Par pos, Vector3Par normal ) = 0;
  virtual float Brightness() const = 0;
  virtual float Brightness(ColorVal acoomEye) const = 0;
  virtual float SortBrightness() const;
  virtual Color GetObjectColor() const = 0;
  virtual void ToDraw( ClipFlags clipFlags=ClipAll, bool dimmed=false )= 0;
  virtual bool Visible( const Object *obj ) const;

  virtual void GetDescription(LightDescription &desc, ColorVal accom) const = 0;
  virtual bool Cull(Vector3Par pos, float radius) const = 0;

  //virtual Vehicle *AttachedOn(){return NULL;}
  Object *AttachedOn(){return NULL;}
  //virtual void UpdatePosition() {}

  Light();
  ~Light();

  bool IsOn() const {return _on;}
  void Switch(bool on = true) {_on = on;}

  float SquareDistance( Vector3Par from ) const
  {
    Vector3 temp = Position()-from;
    return temp.SquareSize();
  }

  int Compare( const Light &with, const LightContext &context ) const;
  int Compare( const Light &with ) const;
  bool operator < ( const Light &with ) const {return Compare(with)<0;}
  bool operator > ( const Light &with ) const {return Compare(with)>0;}
};

//! Class to represent a simple point light. Note it has not initialized shape, which is ok as we're going to use Landscape::DrawPoint to draw the point
class PointLight : public Object
{
protected:
  //! Color of the point
  Color _color;
  //! Intensity of the point
  float _intensity;
public:
  //! Constructor
  PointLight(LODShapeWithShadow *shape, const CreateObjectId &id) : Object(shape, id)
  {
    _color = HWhite;
    _intensity = 100.0f;
  }
  //! Set the point properties
  void SetColorAndIntensity(const Color &color, float intensity)
  {
    _color = color;
    _intensity = intensity;
  }
  //! This object should always use alpha pass (2)
  virtual int PassNum(int lod) {return 2;}
  //! Object drawing (note it doesn't use any shape)
  virtual void Draw(int cb, int level, const SectionMaterialLODs &matLOD, ClipFlags clipFlags,
    const DrawParameters &dp, const InstanceParameters &ip, float dist2, const PositionRender &pos, SortObject *oi);
  //! Disable instancing
  virtual bool CanBeInstanced(int level, float distance2, const DrawParameters &dp) const {return false;}
  virtual bool CanObjDrawAsTask(int level) const;
  virtual bool CanObjDrawShadowAsTask(int level) const;
  //! Consider the area to be big enough for the point to be drawn
  virtual float EstimateArea() const {return 1000.0f;}
  /// pretend we are a feature, so that object size limits are not applied to us
  virtual float GetFeatureSize() const {return 25.0f;}
};

//! major directional+ambient light (Sun)

class LightSun
{
  private:

  Vector3 _direction;
  //! Direction of the light - sun during day, moon during night
  Vector3 _lightDirection;
  Vector3 _shadowDirection;

  Vector3 _sunDirection;
  Vector3 _moonDirection,_moonDirectionUp;
  Matrix3 _starsOrientation;

  //! Color without considering clouds
  Color _diffuse;
  Color _ambient;
  Color _sunObjectColor,_sunHaloObjectColor;
  Color _moonObjectColor,_moonHaloObjectColor;

  Color _ambientPrecalc;
  Color _diffusePrecalc;

  float _moonPhase; // 0.5 - full moon
  float _nightEffect;
  float _starsVisible;

  /// 1=Sun, 0 = Moon
  float _sunOrMoon; 
  //@{
  /*!
  \name sun and light color configuration parameters
  */
  Color _groundReflection; /// diffuse light reflected from the ground

  Color _moonObjectColorFull; //!< color of full moon
  Color _moonHaloObjectColorFull; //!< color of full moon halo
  Color _moonsetObjectColor; //!< color of setting moon
  Color _moonsetHaloObjectColor; //!< color of setting moon halo

  //! angle at which sunset/sunrise is started and sunset/sunrise color is gradually used for sun picture
  float _sunSunset;
  //! when sun is the angle below horizon, sun lighting is no longer applied
  float _endSunset;
  //! night effect angle - angle of sun above horizon in which nigth effect are gradually applied
  float _nightAngle;

  float _sinSunSunset,_invSinSunSunset;
  float _sinEndSunset,_invSinEndSunset;
  float _sinNightAngle,_invSinNightAngle;
  //@}


  public:

  LightSun();
  void Load(ParamEntryPar cfg);
  void LoadDefault();

  float NightEffect() const {return _nightEffect;}
  float StarsVisibility() const {return _starsVisible;}
  float SunOrMoon() const {return _sunOrMoon;}

  void SetMaterial(const TLMaterial &mat, ColorVal accom);

  void Recalculate(World *world=NULL);

  Vector3Val ShadowDirection() const {return _shadowDirection;}
  Vector3Val SunDirection() const {return _sunDirection;}
  Vector3Val MoonDirection() const {return _moonDirection;}
  const Matrix3 &StarsOrientation() const {return _starsOrientation;}
  Vector3Val MoonDirectionUp() const {return _moonDirectionUp;}

  ColorVal GroundReflection() const {return _groundReflection;}

  ColorVal SunObjectColor() const {return _sunObjectColor;}
  ColorVal SunHaloObjectColor() const {return _sunHaloObjectColor;}
  
  ColorVal MoonObjectColor() const {return _moonObjectColor;}
  ColorVal MoonHaloObjectColor() const {return _moonHaloObjectColor;}

  float MoonPhase() const {return _moonPhase;}

  // properties
  ColorVal GetDiffuse() const {return _diffuse;}
  void SetDiffuse( ColorVal diffuse ) {_diffuse=diffuse;}
  ColorVal GetAmbient() const {return _ambient;}

  void SetLighting(ColorVal amb, ColorVal dif, ColorVal sun, float sunOrMoon);

  //ColorVal Diffuse(ColorVal accom) const {return _diffuse * accom;}
  //ColorVal Ambient(ColorVal accom) const {return _ambient * accom;}
  Vector3Val Direction() const {return _direction;}
  Vector3 LightDirection() const {return _lightDirection;}

  Color AmbientResult() const;
  Color FullResult( float diffuse=1.0 ) const; // full or partial diffuse+ambinet
  Color SimulateDiffuse( float diffuse=1.0 ) const; // full or partial diffuse+ambinet

  /// get eye adaptation - including sun blinding
  float EyeAdaptation(VisualStateAge age, Vector3Par observerDir, float fovLeft, float fovTop, bool fast=false) const;

  //@{ eye adaptation limits
  float GetEyeAdaptMin() const;  
  float GetEyeAdaptMax() const;
  //@}
  // calculate lighting result for a given normal and material  
  Color Apply(Vector3Par normal, const TLMaterial &mat) const;
  /// part of Apply corresponding to diffuse light
  Color ApplyDiffuse(Vector3Par normal, const TLMaterial &mat) const;
  /// part of Apply corresponding to ambient light
  Color ApplyAmbient(Vector3Par normal, const TLMaterial &mat) const;

  ColorVal DiffusePrecalc() const {return _diffusePrecalc;}
  ColorVal AmbientPrecalc() const {return _ambientPrecalc;}
};

class LightPositioned: public Light
{
  typedef Light base;

  protected:
  Vector3 _modelPos,_modelDir;
  
  public:
  LightPositioned();
  // prepare light to be applied in model space
  void Prepare( const Matrix4 &worldToModel );
};

/// helper base class for lights with a color
class LightPositionedColored: public LightPositioned
{
  typedef LightPositioned base;

  protected:
  Color _ambientPrecalc;
  Color _diffusePrecalc;

  Color _ambient;
  Color _diffuse;

  public:
  LightPositionedColored();
  LightPositionedColored( ColorVal diffuse, ColorVal ambient );
  void SetMaterial(const TLMaterial &mat, ColorVal accom);

  void SetDiffuse( ColorVal diffuse ){_diffuse=diffuse;}
  ColorVal GetDiffuse() const {return _diffuse;}
  
  void SetAmbient( ColorVal ambient ){_ambient=ambient;}
  ColorVal GetAmbient() const {return _ambient;}

};

/// omnidirectional (spherical or point) light

class LightPoint: public LightPositionedColored
{
  // omnidirectional point light
  typedef LightPositionedColored base;

  protected:
  float _startAtten;

  public:
  // constructors
  LightPoint();
  LightPoint( ColorVal diffuse, ColorVal ambient );
  void SetBrightness( float coef );
  // properties
  float Brightness() const;
  float Brightness(ColorVal accomEye) const;
  float SortBrightness() const;
  Color GetObjectColor() const;

  //bool Visible( const Object *obj ) const;

  float FlareIntensity( Vector3Par camPos, Vector3Par camDir ) const;
  virtual Color Apply( Vector3Par pos, Vector3Par normal );
  virtual Color ApplyPrecalc( Vector3Par point, Vector3Par normal );
  virtual void GetDescription(LightDescription &desc, ColorVal accom) const;
  virtual bool Cull(Vector3Par pos, float radius) const;
  void ToDraw( ClipFlags clipFlags=ClipAll, bool dimmed=false );

  void Load(ParamEntryPar cls);
  void Load(ParamEntryPar cls, EffectVariablesPar evars);

  static LightPoint *CreateObject(ParamArchive &ar);
  LSError Serialize(ParamArchive &ar);
};

/// omnidirectional (spherical or point) light, with a simulation

class LightPointEntity : public Entity
{
  typedef Entity base;

protected:
  Ref<LightPoint> _light;

  OLink(Object) _object;
  Vector3 _modelPos;

public:
  LightPointEntity();
  
  void SetDiffuse(ColorVal diffuse) {if (_light) _light->SetDiffuse(diffuse);}
  ColorVal GetDiffuse() const {return _light ? _light->GetAmbient() : HBlack;}
  void SetAmbient(ColorVal ambient) {if (_light) _light->SetAmbient(ambient);}
  ColorVal GetAmbient() const {return _light ? _light->GetAmbient() : HBlack;}
  void SetBrightness(float coef) {if (_light) _light->SetBrightness(coef);}
  float Brightness() const {return _light ? _light->Brightness() : 0;}

  bool SimulationReady(SimulationImportance prec) const;
  void Simulate(float deltaT, SimulationImportance prec);

  void Attach(Object *object, Vector3Par modelPos) {_object = object; _modelPos = modelPos;}
  void Detach() {_object = NULL;}

  USE_CASTING(base)
};

class LightPointVisible: public LightPoint
{
  Ref<LODShapeWithShadow> _shape;
  float _size;
  //! Determines whether volume light associated with the light point is orthogonal to the player or not
  bool _vlOrthogonalToPlayer;

  // Object to represent the point light. It will be added to pass2
  Ref<PointLight> _pointLight;
public:
  LightPointVisible(bool vlOrthogonalToPlayer);
  LightPointVisible
  (
    LODShapeWithShadow *shape,
    ColorVal diffuse, ColorVal ambient, bool vlOrthogonalToPlayer, float size=1
  );
  void ToDraw( ClipFlags clipFlags=ClipAll, bool dimmed=false );
  void SetSize( float size ){_size=size;}

  void Load(ParamEntryPar cls);
};

/// reflector - a spot light (cone light)
class LightReflector: public LightPositionedColored
{
  // omnidirectional point light
  typedef LightPositionedColored base;

protected:
  //! Angle corresponding to phi, in degrees
  float _angle;
  //!{ Values used in some calculation
  float _tgAngleHalf;
  float _minInside;
  float _maxInside;
  //!}
  //! Shape of the associated volume light
  Ref<LODShapeWithShadow> _shape;
  float _startAtten;
  float _size;
  
  //! Determines whether volume light associated with the light point is orthogonal to the player or not
  bool _vlOrthogonalToPlayer;
  //! Determines whether volume light is used as flash light
  bool _isFlashLight;
  Vector3 _scale;

  //! Recalculation of coefficients dependent on angle
  void AngleHasChanged();

public:
  //! Constructor
  LightReflector
  (
    LODShapeWithShadow *shape,
    ColorVal diffuse, ColorVal ambient, float angle, bool vlOrthogonalToPlayer, float size
  );

  //! Constructor
  LightReflector
  (
    LODShapeWithShadow *shape, ColorVal diffuse, ColorVal ambient, float angle, Vector3Val scale, bool isFlashLight
  );
  
  // properties
  LODShapeWithShadow *GetShape() {return _shape;}

  float Brightness(ColorVal accomEye) const;
  float Brightness() const;
  void SetBrightness(float coef);
  float GetBrightness() const;
  Color GetObjectColor() const;

  void SetDiffuse( ColorVal diffuse ){_diffuse=diffuse;}
  ColorVal GetDiffuse() const {return _diffuse;}
  
  void SetAmbient( ColorVal ambient ){_ambient=ambient;}
  ColorVal GetAmbient() const {return _ambient;}

  void SetSize( float size ){size=_size;}
  float GetSize() const {return _size;}

  void SetAngle( float angle ) {_angle = angle; AngleHasChanged();}
  float Angle() const {return _angle;}

  float GetAttenuation() const {return _startAtten;}
  void SetAttenuation(float startAtten) {_startAtten = startAtten;}

  bool Visible( const Object *obj ) const;
  float FlareIntensity( Vector3Par camPos, Vector3Par camDir ) const;
  
  Color Apply( Vector3Par pos, Vector3Par normal );
  Color ApplyPrecalc( Vector3Par point, Vector3Par normal );
  virtual void GetDescription(LightDescription &desc, ColorVal accom) const;
  virtual bool Cull(Vector3Par pos, float radius) const;
  void ToDraw( ClipFlags clipFlags=ClipAll, bool dimmed=false );
};

#pragma warning(disable:4250)
/// LightReflector able to attach to an entity
class LightReflectorOnVehicle: public LightReflector,public AttachedOnVehicle
{
  public:
  LightReflectorOnVehicle
  (
    LODShapeWithShadow *shape,
    ColorVal diffuse, ColorVal ambient,
    Object *vehicle,
    Vector3Par position, Vector3Par direction, float angle, float size
  )
  :LightReflector(shape,diffuse,ambient,angle,false,size),
  AttachedOnVehicle(vehicle,position,direction)
  {
  }
  Object *AttachedOn(){return AttachedOnVehicle::AttachedOn();}
};
/// LightPoint able to attach to an entity
class LightPointOnVehicle: public LightPointVisible,public AttachedOnVehicle
{
  public:
  LightPointOnVehicle(
    LODShapeWithShadow *shape,
    ColorVal diffuse, ColorVal ambient,
    Object *vehicle,
    Vector3Par position, float size=1
  );
  LightPointOnVehicle(
    Object *vehicle, Vector3Par position
  );
  ~LightPointOnVehicle();
  Object *AttachedOn(){return AttachedOnVehicle::AttachedOn();}

  void Load(ParamEntryPar cls);
};

#pragma warning(default:4250)

#endif

