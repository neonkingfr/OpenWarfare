/*!
\file
Implementation for EntityAI class - general functions
*/

#include "wpch.hpp"

#include "dikCodes.h"
#include "transport.hpp"
#include "AI/ai.hpp"
#include "AI/aiRadio.hpp"
#include "world.hpp"
#include "landscape.hpp"
#ifdef _WIN32
  #include "joystick.hpp"
#endif
#include "shots.hpp"
#include "keyInput.hpp"
#include <El/Common/randomGen.hpp>
#include <Es/Strings/bString.hpp>
#include "camera.hpp"
#include "visibility.hpp"
#include "roads.hpp"
#include "thing.hpp"
#include "pathSteer.hpp"
#include "AI/operMap.hpp"
#include "txtPreload.hpp"
#include "diagModes.hpp"
#include "fileLocator.hpp"

#include "Shape/specLods.hpp"
#include "engine.hpp"
#include "integrity.hpp"

#include "tlVertex.hpp"
#include "stringtableExt.hpp"
#include "objLine.hpp"

#include <El/Common/perfLog.hpp>
#include <El/Common/perfProf.hpp>
#include <Es/ErrorProp/errorProp.hpp>
#include <El/Evaluator/express.hpp>

#include "Network/network.hpp"
#include "UI/uiActions.hpp"

#include <Es/Algorithms/qsort.hpp>

#include "move_actions.hpp"

#include "detector.hpp" // flag

#include <El/Enum/enumNames.hpp>

#include "gameStateExt.hpp"
#include "paramArchiveExt.hpp"

#include "mbcs.hpp"
#include "drawText.hpp"

#if _VBS3
#include "laserTarget.hpp"
#endif

static const Color MarkerRedColor(1.0, 0.1, 0.1);
static const Color MarkerRedAmbient(0.1, 0.01, 0.01);
static const float MarkerRedBrightness = 0.002;

static const Color MarkerGreenColor(0.1, 1.0, 0.1);
static const Color MarkerGreenAmbient(0.01, 0.1, 0.01);
static const float MarkerGreenBrightness = 0.002;

static const Color MarkerBlueColor(0.1, 0.1, 1.0);
static const Color MarkerBlueAmbient(0.01, 0.01, 0.1);
static const float MarkerBlueBrightness = 0.002;

static const Color MarkerWhiteColor(1.0, 1.0, 1.0);
static const Color MarkerWhiteAmbient(0.1, 0.1, 0.1);
static const float MarkerWhiteBrightness = 0.001;

static BankArray<RecoilFunction> RecoilFunctions;

const float LostUnitMin = 150.0f;
const float LostUnitMax = 500.0f;

#pragma warning(disable:4355)

void ReflectorInfo::Load(EntityAIType &type, ParamEntryPar cls, float armor, Ref<WoundInfo> wound)
{
  LODShape *shape = type.GetShape();
  color = GetColor(cls >> "color");
  colorAmbient = GetColor(cls >> "ambient");
  positionIndex = -1;
  directionIndex = -1;
  position = VZero;
  direction = VForward;
  const Shape *memory = shape->MemoryLevel();
  if (memory)
  {
    RString name = cls >> "position";
    positionIndex = shape->PointIndex(memory,name);
    if (positionIndex >= 0) position = memory->Pos(positionIndex);
    name = cls >> "direction";
    directionIndex = shape->PointIndex(memory,name);
    if (directionIndex >= 0 && positionIndex >= 0)
    {
      direction = memory->Pos(directionIndex) - position;
      direction.Normalize();
    }
  }
  size = cls >> "size";
  brightness = cls >> "brightness";
  RString selName = cls >> "selection";
  selection.Init(shape, selName, NULL);
  RString hitName = cls >> "hitpoint";
  hitPoint = type.AddHitPoint(hitName, armor, -1);

  speed = -1;
  phaseLimit = 0.5;
  ConstParamEntryPtr entry = cls.FindEntry("period");
  if (entry)
  {
    if (entry->IsArray())
    {
      DoAssert(entry->GetSize() == 2);
      float on = (*entry)[0];
      float off = (*entry)[1];
      float period = on + off;
      if (period > 0)
      {
        speed = 1.0f / period;
        phaseLimit = on / period;
      }
    }
    else
    {
      float period = *entry;
      if (period > 0)
        speed = 1.0f / period;
    }
  }

  entry = cls.FindEntry("angle");
  if (entry)
    angle = entry->operator float();
  else 
    angle = 36.0f; 
};

void PlateInfo::Init(const Shape *shape, Offset face)
{
  //_face = face;
  const Poly &f = shape->Face(face);
  
  _center = VZero;
  for (int v=0; v<f.N(); v++ )
  {
    int vertex = f.GetVertex(v);
    _center += shape->Pos(vertex);
  }
  float invFN = 1.0f/f.N();
  _center *=invFN;
  Vector3 normal = f.GetNormal(shape->GetPosArray()); 
  _normal = normal.Normalized();
  // TODO: calculate actual size (based on max. distance of points)
  _size = sqrt(normal.Size())*0.4;
}

void PlateInfos::Init(LODShape *shape, ParamEntryPar cls, FontID font)
{
  _plates.Init(shape->NLevels());

  // TODO: create font indirection
  _font = GEngine->LoadFont(font);
  GetValue(_color, cls >> "color");
  _name = cls >> "name";
}

void PlateInfos::InitLevel(LODShape *shape, int level)
{
  ShapeUsed sShape = shape->Level(level);
  int sel = shape->FindNamedSel(sShape,_name);
  if (sel>=0)
  {
    ShapeUsedGeometryLock<> lock(shape,level);
    const FaceSelection &faces = sShape->NamedSel(sel).FaceOffsets(sShape.GetShape());
    Assert(faces.GetNeedsSections());
    // scan sections
    for (int s=0; s<faces.NSections(); s++)
    {
      const ShapeSection &ss = sShape->GetSection(faces.GetSection(s));
      //const Texture *tex = ss.GetTexture();
      int nFaces = 0;
      for (Offset off=ss.beg; off<ss.end; sShape->NextFace(off))
        nFaces++;
      _plates[level].Realloc(nFaces);
      _plates[level].Resize(nFaces);
      int f;
      Offset off;
      for (f=0,off=ss.beg; off<ss.end; sShape->NextFace(off),f++)
        _plates[level][f].Init(sShape, off);
    }
  }
}

void PlateInfos::Draw(int cb, int level, ClipFlags clipFlags, const PositionRender &pos, const RString &text) const
{
  // find position to draw at (based on _plate selection)
  for (int plate=0; plate<_plates[level].Size(); plate++)
  {
    const PlateInfo &pi = _plates[level][plate];

    // plate rendering is very inefficient
    // perform some culling to avoid rendering the plates too far
    
    // estimate size
    const Camera &camera = *GScene->GetCamera();
    Matrix4Val project=camera.Projection();

    float dist2 = pos.PositionWorld(camera).Distance2(camera.Position());

    // perspective screen size - based on Object::DrawDecal code
    float area = project(0,0)*-project(1,1)*camera.InvLeft()*camera.InvTop()*Square(pi._size);
    // note: similar to Camera::_aotFactor

    // area/dist2 should be approximate area of one letter in square pixels
    static float minArea = 40.0f;
    if( area>=minArea*dist2 )
    {
      // plates are never visible in 1st person view, we do not mind if we lose camSpace for them
      Matrix4 transWorld = pos.TransformWorld(camera);
      Vector3 center = transWorld.FastTransform(pi._center);
      Vector3 normal = transWorld.Rotate(pi._normal);
    
      Vector3 up = transWorld.DirectionUp();
      Vector3 aside = up.CrossProduct(normal).Normalized();
      up = normal.CrossProduct(aside).Normalized();
      
      /*
      // render axis diagnostics
      GEngine->DrawLine3D(center,center+aside,PackedColor(Color(1,0,0)),0);
      GEngine->DrawLine3D(center,center+up,PackedColor(Color(0,1,0)),0);
      GEngine->DrawLine3D(center,center-normal,PackedColor(Color(0,0,1)),0);
      */

      // when not caring about predication, we should be hopefully recorded in the part
      // which is played back in all predicated versions
      up *= pi._size;
      aside *= pi._size*0.8;
      center -= normal*0.01;
      GEngine->DrawText3DCenteredAsync(center, up, aside, clipFlags, _font, _color, 0, text);
    }
  }
}

/// animation of reloading weapon attached to vehicle
class AnimationSourceReload : public AnimationSource
{
  typedef AnimationSource base;
  
protected:
  Ref<WeaponType> _weapon;

public:  
  AnimationSourceReload(const RStringB &name, WeaponType *weapon) :base(name),_weapon(weapon) {}
  float GetPhase(const ObjectVisualState &vs, const Animated *obj) const;
  void SetPhaseWanted(Animated *obj, float phase) {}
};

/// animation of revolving weapon attached to vehicle
class AnimationSourceRevolving : public AnimationSource
{
  typedef AnimationSource base;
  
protected:
  Ref<WeaponType> _weapon;

public:  
  AnimationSourceRevolving(const RStringB &name, WeaponType *weapon) :base(name),_weapon(weapon) {}
  float GetPhase(const ObjectVisualState &vs, const Animated *obj) const;
  void SetPhaseWanted(Animated *obj, float phase) {}
};

/// animation of reloading magazine of weapon attached to vehicle
class AnimationSourceReloadMagazine : public AnimationSource
{
  typedef AnimationSource base;
  
protected:
  Ref<WeaponType> _weapon;

public:  
  AnimationSourceReloadMagazine(const RStringB &name, WeaponType *weapon) :base(name),_weapon(weapon) {}
  float GetPhase(const ObjectVisualState &vs, const Animated *obj) const;
  void SetPhaseWanted(Animated *obj, float phase) {}
};


class FindWeaponsFunc : public ITurretFunc
{
protected:
  const WeaponsState *&_weapons; // out
  const WeaponType *_weapon;

public:
  FindWeaponsFunc(const WeaponsState *&weapons, const WeaponType *weapon) : _weapons(weapons), _weapon(weapon) {}
  bool operator () (EntityAIFull *entity, TurretContext &context)
  {
    for (int i=0; i<context._weapons->_weapons.Size(); i++)
    {
      if (context._weapons->_weapons[i] == _weapon)
      {
        // found
        _weapons = context._weapons;
        return true;
      }
    }
    return false; // not found
  }
};

float AnimationSourceReload::GetPhase(const ObjectVisualState &vs, const Animated *obj) const
{
  Assert(dynamic_cast<const EntityAIFull *>(obj));
  const EntityAIFull *entity = static_cast<const EntityAIFull *>(obj);

  // search for the weapon in all weapon systems
  const WeaponsState *weapons = NULL;
  FindWeaponsFunc func(weapons, _weapon);
  if (!entity->ForEachTurret(func))
    return 0;
  Assert(weapons);

  // TODO: non-active weapons can reload as well
  int w = weapons->_currentWeapon;
  if (w < 0)
    return 0;

  const MagazineSlot &slot = weapons->_magazineSlots[w];
  if (slot._weapon != _weapon)
    return 0;
  return slot.GetReloadPos(vs);
}

float AnimationSourceRevolving::GetPhase(const ObjectVisualState &vs, const Animated *obj) const
{
  Assert(dynamic_cast<const EntityAIFull *>(obj));
  const EntityAIFull *entity = static_cast<const EntityAIFull *>(obj);

  // search for the weapon in all weapon systems
  const WeaponsState *weapons = NULL;
  FindWeaponsFunc func(weapons, _weapon);
  if (!entity->ForEachTurret(func))
    return 0;
  Assert(weapons);

  // TODO: non-active weapons can reload as well
  int w = weapons->_currentWeapon;
  if (w < 0)
    return 0;

  const MagazineSlot &slot = weapons->_magazineSlots[w];
  if (slot._weapon != _weapon)
    return 0;
  const Magazine *magazine = slot._magazine;
  if (!magazine)
    return 0;
  const MagazineType *magazineType = magazine->_type;
  if (!magazineType)
    return 0;
  const WeaponModeType *mode = slot._weaponMode;
  if (!mode)
    return 0;

  float reload = magazine->_reload;
  saturate(reload, 0, 1);
  const float rotPerBurst = 1.0f / magazineType->_maxAmmo;
  float bulletsPerBurst = mode->_mult;
  float rotPerBullet = rotPerBurst / bulletsPerBurst;
  int ammo = magazine->GetAmmo();
  float prevRot = rotPerBullet * (ammo + bulletsPerBurst);
  float currRot = rotPerBullet * (ammo);
  return fastFmod(currRot - (currRot - prevRot) * reload, 1);
}

float AnimationSourceReloadMagazine::GetPhase(const ObjectVisualState &vs, const Animated *obj) const
{
  Assert(dynamic_cast<const EntityAIFull *>(obj));
  const EntityAIFull *entity = static_cast<const EntityAIFull *>(obj);

  // search for the weapon in all weapon systems
  const WeaponsState *weapons = NULL;
  FindWeaponsFunc func(weapons, _weapon);
  if (!entity->ForEachTurret(func))
    return 0;
  Assert(weapons);

  // TODO: non-active weapons can reload as well
  int w = weapons->_currentWeapon;
  if (w < 0)
    return 0;

  const MagazineSlot &slot = weapons->_magazineSlots[w];
  if (slot._weapon != _weapon)
    return 0;
  return slot.GetMagazineReloadPos(vs);
}

void ObjectTypeReflectors::Init(EntityAIType *type, const ParamEntry &par, float objectArmor, Ref<WoundInfo> wound)
{
  float armor = objectArmor * (float)(par >> "armorLights");
  ParamEntryVal reflectors = par >> "Reflectors";
  int n = reflectors.GetEntryCount();
  _aggReflectors.Realloc(n); // n is a good upper limit
  _aggReflectors.Resize(0);

  // Prepare the touched array
  VerySmallArray<int, sizeof(int) * 10 + sizeof(int)> touched;
  touched.Resize(n);
  for (int i = 0; i < n; i++)
    touched[i] = false;

  // First add aggregated lights
  {
    ParamEntryVal aggregateReflectors = par >> "aggregateReflectors";
    if (aggregateReflectors.IsArray())
    {
      int size = aggregateReflectors.GetSize();
      for (int i = 0; i < size; i++)
      {
        ReflectorInfoArray aggGroup;
        const IParamArrayValue &a = aggregateReflectors[i];
        if (a.IsArrayValue())
        {
          int itemSize = a.GetItemCount();
          aggGroup.Realloc(itemSize);
          aggGroup.Resize(0);
          for (int j = 0; j < itemSize; j++)
          {
            RStringB reflectorName = a[j];
            for (int k = 0; k < n; k++)
            {
              if (reflectorName == reflectors.GetEntry(k).GetName())
              {
                // Load particular reflector
                ReflectorInfo *reflectorInfo = new ReflectorInfo;
                aggGroup.Add(reflectorInfo);
                reflectorInfo->Load(*type, reflectors.GetEntry(k), armor, wound);
                touched[k] = true;
                break;
              }
            }
          }
          if (aggGroup.Size() > 0)
          {
            _aggReflectors.Add(aggGroup);
            _aggReflectors[_aggReflectors.Size() - 1].Compact();
          }
        }
        else
          RptF("Error: Item of aggregateReflectors is not an array");
      }
    }
    else
      RptF("Error: aggregateReflectors is not an array");
  }

  // Then add the rest (untouched reflectors)
  for (int i = 0; i < n; i++)
  {
    if (!touched[i])
    {
      ReflectorInfoArray &aggGroup = _aggReflectors.Append();
      aggGroup.Realloc(1);
      aggGroup.Resize(1);
      ReflectorInfo *info = new ReflectorInfo;
      aggGroup[0] = info;
      info->Load(*type, reflectors.GetEntry(i), armor, wound);
    }
  }

  // Compact the arrays
  _aggReflectors.Compact();
  type->CompactHitpoints();

  // Final check
  if ((_aggReflectors.Size() > 0) && (_aggReflectors[0].Size() == 0))
    Fail("Error: Not valid EntityAIType::_aggReflectors array");
}

void ObjectTypeReflectors::InitLevel(LODShape *shape, int level)
{
  for (int i=0; i<_aggReflectors.Size(); i++)
  {
    ReflectorInfoArray &ria = _aggReflectors[i];
    for (int j = 0; j < ria.Size(); j++)
    {
      ReflectorInfo &info = *ria[j];
      info.selection.InitLevel(shape, level);
    }
  }
}

void ObjectTypeReflectors::DeinitLevel(LODShape *shape, int level)
{
  for (int i=0; i<_aggReflectors.Size(); i++)
  {
    ReflectorInfoArray &ria = _aggReflectors[i];
    for (int j = 0; j < ria.Size(); j++)
    {
      ReflectorInfo &info = *ria[j];
      info.selection.DeinitLevel(shape, level);
    }
  }
}

EntityAIType::EntityAIType( ParamEntryPar param )
:EntityType(param)
{
  _scopeLevel=0;
}

EntityAIType::~EntityAIType()
{
  // make sure all vehicles of this type are already released
  Assert( _refVehicles==0 );
}

AnimationSource *EntityAIType::CreateAnimationSourcePar(const AnimationType *type, ParamEntryPar entry)
{
  // TODO: type is not needed here
  RStringB sourceType = entry>>"source";
  if (!strcmpi(sourceType,"reload"))
  {
    RStringB weapon = entry >> "weapon";
    return new AnimationSourceReload(entry.GetName(),WeaponTypes.New(weapon));
  }
  else if (!strcmpi(sourceType,"revolving"))
  {
    RStringB weapon = entry >> "weapon";
    return new AnimationSourceRevolving(entry.GetName(),WeaponTypes.New(weapon));
  }
  else if (!strcmpi(sourceType, "reloadmagazine"))
  {
    RStringB weapon = entry >> "weapon";
    return new AnimationSourceReloadMagazine(entry.GetName(),WeaponTypes.New(weapon));
  }
  else if (!strcmpi(sourceType,"user"))
  {
    return CreateAnimationSourceUser(entry);
  }
  else if (!strcmpi(sourceType,"direct"))
  {
    return CreateAnimationSourceDirect(sourceType);
  }
  /// argument not known, call a non-argument version
  return base::CreateAnimationSourcePar(type, entry);
}

AnimationSource *EntityAIType::CreateAnimationSource(const AnimationType *type, RStringB source)
{
  if (!strcmpi(source,"reload"))
  {
    ConstParamEntryPtr animSources = _par ? _par->FindEntry("AnimationSources") : ParamEntryPtr(NULL);
    if (animSources)
    {
      ParamEntryVal entry = (*animSources)>> type->GetName();
      RStringB weapon = entry >> "weapon";
      // TODO: do not allow anonymous reload animations
      RptF("Old style reload used in %s",cc_cast(animSources->GetContext()));
      return new AnimationSourceReload(RStringB(),WeaponTypes.New(weapon));
    }
  }

  return base::CreateAnimationSource(type, source);
}

void HeadPars::Load(ParamEntryPar cfg)
{
#define GET_PAR(x) _##x=cfg>>#x
  GET_PAR(initAngleY);
  GET_PAR(minAngleY);
  GET_PAR(maxAngleY);
  GET_PAR(initAngleX);
  GET_PAR(minAngleX);
  GET_PAR(maxAngleX);
#undef GET_PAR
}

void HeadPars::InitVirtual(CameraType camType, float &heading, float &dive) const
{
  dive = _initAngleX*(H_PI/180);
  heading = _initAngleY*(H_PI/180);
}

void HeadPars::LimitVirtual(CameraType camType, float &heading, float &dive) const
{
  float minHeading = _minAngleY*(H_PI/180);
  float maxHeading = _maxAngleY*(H_PI/180);
  if( maxHeading-minHeading<H_PI*15/8 )
  {
    float initHeading = _initAngleY*(H_PI/180);
    minHeading = AngleDifference(minHeading,initHeading);
    maxHeading = AngleDifference(maxHeading,initHeading);
    heading = AngleDifference(heading,initHeading);

    saturate(heading,minHeading,maxHeading);

    heading += initHeading;
  }
  float minDive = _minAngleX*(H_PI/180);
  float maxDive = _maxAngleX*(H_PI/180);
  saturate(dive,minDive,maxDive);
}

RString GetVehicleIcon(RString name)
{
  ConstParamEntryPtr entry = (Pars >> "CfgVehicleIcons").FindEntry(name);
  if (entry) name = *entry;
  return GetPictureName(name);
}

#if _ENABLE_CONVERSATION
//note: renamed from LoadSpeech as there are multiple LoadSpeech functions, not all of them static
//      GCC: error: 'void LoadSpeech(AutoArray<RStringCT<char>, MemAllocD>&, ParamEntryVal)' was declared 'extern' and later 'static'
static void LoadSpeechVeh(AutoArray<RString> &result, ParamEntryVal entry)
{
  Assert(entry.IsArray());
  int n = entry.GetSize();
  result.Realloc(n);
  result.Resize(n);
  for (int i=0; i<n; i++) result[i] = entry[i];
}
#endif

void WeaponsType::Load(ParamEntryPar cfg)
{
  // weapons
  ParamEntryVal weapons = cfg >> "weapons";
  int n = weapons.GetSize();
  _weapons.Realloc(n);
  _weapons.Resize(n);
  for (int i=0; i<n; i++)
  {
    RString name = weapons[i];
    _weapons[i] = WeaponTypes.New(name);
  }

  // magazines
  ParamEntryVal magazines = cfg >> "magazines";
  n = magazines.GetSize();
  _magazines.Realloc(n);
  _magazines.Resize(n);
  for (int i=0; i<n; i++)
  {
    RString name = magazines[i];
    _magazines[i] = MagazineTypes.New(name);
  }
  _magazines.Compact();
}

float WeaponsType::GetMaxRange() const
{
  float maxRange = 0;
  for (int w=0; w<_weapons.Size(); w++)
  {
    const WeaponType *weapon = _weapons[w];
    for (int i=0; i<weapon->_muzzles.Size(); i++)
    {
      const MuzzleType *muzzle = weapon->_muzzles[i];
      if (!muzzle) continue;
      if (muzzle->_magazines.Size()<=0) continue; // nothing to load - not a real weapon
      for (int j=0; j<muzzle->_modes.Size(); j++)
      {
        const WeaponModeType *mode = muzzle->_modes[j];
        if (mode) saturateMax(maxRange, mode->maxRange);
      }
    }
  }
  return maxRange;
}

void EntityAIType::Load(ParamEntryPar par, const char *shape)
{
  // ERROR_CATCH_ANY can be executed multiple times, but we want only one error message
  bool errorOnce = false;
  ERROR_TRY()
  
  base::Load(par,shape);

  #define GET_PAR(x) _##x=par>>#x

  GET_PAR(accuracy);
  GET_PAR(camouflage);
  GET_PAR(audible);
  GET_PAR(displayName);

  ConstParamEntryPtr entry = par.FindEntry("displayNameShort");
  if (entry) _shortName = *entry;
  else _shortName = _displayName;

#if _ENABLE_CONVERSATION
  GET_PAR(textSingular);
  LoadSpeechVeh(_speechSingular, par >> "speechSingular");
  GET_PAR(textPlural);
  LoadSpeechVeh(_speechPlural, par >> "speechPlural");

  _speechVariants.Resize(0);
  entry = par.FindEntry("SpeechVariants");
  if (entry)
  {
    for (int i=0; i<entry->GetEntryCount(); i++)
    {
      ParamEntryVal cls = entry->GetEntry(i);
      if (!cls.IsClass()) continue;
      EntitySpeechVariant variant;
      variant._variant = cls.GetName();
      LoadSpeechVeh(variant._speechSingular, cls >> "speechSingular");
      LoadSpeechVeh(variant._speechPlural, cls >> "speechPlural");
      _speechVariants.AddUnique(variant);
    }
  }
  _speechVariants.Compact();
#endif

  GET_PAR(weaponSlots);

  GET_PAR(spotableNightLightsOff);
  GET_PAR(spotableDarkNightLightsOff);
  GET_PAR(spotableNightLightsOn);
    
  GET_PAR(visibleNightLightsOff);
  GET_PAR(visibleNightLightsOn);

  _nameSound=par>>"nameSound";
  _typicalSide=(TargetSide)(par>>"side").GetInt();
  _destrType=(DestructType)(par>>"destrType").GetInt();

  //Assert( _parClass );
  //RString model=par>>"model";
  RString picture=par>>"picture";
  // some parent vehicle classes have no model or picture defined
  //if( !picture || !picture[0] ) picture=model; 
  if( picture.GetLength()>0 )
    _picture=GlobLoadTexture(GetVehicleIcon(picture));
  RString portrait=par>>"portrait";
  if( portrait.GetLength()>0 )
    _portrait=GlobLoadTexture(GetVehicleIcon(portrait));

  RString icon=par>>"icon";
  if( icon.GetLength()>0 )
    _icon=GlobLoadTexture(GetVehicleIcon(icon));

  GET_PAR(cost);
  GET_PAR(fuelCapacity);
  GET_PAR(armor);

  GET_PAR(damageResistance);

  _extCameraPosition.Init();
  _extCameraPosition[0]=(par>>"extCameraPosition")[0];
  _extCameraPosition[1]=(par>>"extCameraPosition")[1];
  _extCameraPosition[2]=(par>>"extCameraPosition")[2];

  _groupCameraPosition.Init();
  _groupCameraPosition[0]=(par>>"groupCameraPosition")[0];
  _groupCameraPosition[1]=(par>>"groupCameraPosition")[1];
  _groupCameraPosition[2]=(par>>"groupCameraPosition")[2];

  GET_PAR(maxSpeed);
  GET_PAR(limitedSpeedCoef);
  GET_PAR(secondaryExplosion);
  GET_PAR(sensitivity);
  GET_PAR(sensitivityEar);

  GET_PAR(brakeDistance);
  GET_PAR(precision);
  GET_PAR(formationX);
  GET_PAR(formationZ);
  GET_PAR(formationTime);
  GET_PAR(steerAheadSimul);
  GET_PAR(steerAheadPlan);

  // get camera limits / initial values
  _viewPilot.Load(par>>"ViewPilot");
  _headLimits.Load(par>>"HeadLimits");

  // get control / simulation properties
  GET_PAR(predictTurnSimul);
  GET_PAR(predictTurnPlan);
  GET_PAR(minFireTime);

  GET_PAR(enableWatch);
  GET_PAR(enableRadio);

  if (_armor>1e-10)
  {
    _invArmor = 1/_armor;
    _logArmor = log(_armor);
  }
  else
  {
    _invArmor = 1e10;
    _logArmor = 25;
  }

  _invFormationTime = _formationTime> 0 ? 1/_formationTime : 1e10f;

  // cargo definition in config
  _maxFuelCargo=par>>"transportFuel";
  _maxRepairCargo=par>>"transportRepair";
  _maxAmmoCargo=par>>"transportAmmo";
  _maxWeaponsCargo=par>>"transportMaxWeapons";
  _maxMagazinesCargo=par>>"transportMaxMagazines";
  _maxBackpacksCargo= par>>"transportMaxBackpacks";

  if(par.FindEntry("isbackpack"))
  {
    _isBackpack = par>>"isbackpack";
    _maxBackpacksCargo = 0;
  }
  else _isBackpack = false;

  if(par.FindEntry("assembleInfo"))
  {
        ParamEntryVal assemble = par>>"assembleInfo";
        bool primary = assemble>> "primary";
        _assembleInfo.base = assemble >> "base";
        _assembleInfo.assembleTo = assemble >> "assembleTo";
        _assembleInfo.displayName = assemble >> "displayName";

        if(primary) _assembleInfo.type = PrimaryPart;
        else _assembleInfo.type = SecondaryPart;
        
        ParamEntryVal dissasembleTo = assemble >> "dissasembleTo";
        if(dissasembleTo.GetSize() > 0)
        {//object which can be disassembled
          _assembleInfo.type = Assembled;
          _assembleInfo.dissasembleTo[0] = dissasembleTo[0];
          if(dissasembleTo.GetSize() > 1) _assembleInfo.dissasembleTo[1] = dissasembleTo[1];
        }

  }
  else _assembleInfo.type = Fixed;

  
  _lockDetectionSystem=par>>"lockDetectionSystem";
  _incommingMisslieDetectionSystem =par>>"incommingMisslieDetectionSystem";

  ParamEntryVal weaponCargo = par >> "TransportWeapons";
  int n = weaponCargo.GetEntryCount();
  _weaponCargo.Resize(n);
  for (int i=0; i<n; i++)
  {
    ParamEntryVal par = weaponCargo.GetEntry(i);
    RStringB weapon = par >> "weapon";
    _weaponCargo[i].weapon = WeaponTypes.New(weapon);
    _weaponCargo[i].count = par >> "count";
  }
  ParamEntryVal magazineCargo = par >> "TransportMagazines";
  n = magazineCargo.GetEntryCount();
  _magazineCargo.Resize(n);
  for (int i=0; i<n; i++)
  {
    ParamEntryVal par = magazineCargo.GetEntry(i);
    RStringB magazine = par >> "magazine";
    _magazineCargo[i].magazine = MagazineTypes.New(magazine);
    _magazineCargo[i].count = par >> "count";
  }
  if(par.FindEntry("TransportBackpacks"))
  {
    ParamEntryVal backpacksCargo = par >> "TransportBackpacks";
    n = backpacksCargo.GetEntryCount();
    _backpackCargo.Resize(n);
    for (int i=0; i<n; i++)
    {
      ParamEntryVal par = backpacksCargo.GetEntry(i);
      RStringB bag = par >> "backpack";
      _backpackCargo[i].backpack = bag;
      _backpackCargo[i].count = par >> "count";
    }
  }
  
  _minCost=_maxSpeed>0 ? 3.6/_maxSpeed : 1e10;

  GET_PAR(alwaysTarget);
  
  GET_PAR(irTarget);
  if (!par.FindEntry("irScanRange"))
  {
    GET_PAR(irScanToEyeFactor);
    GET_PAR(irScanRangeMin);
    GET_PAR(irScanRangeMax);
  }
  else
  {
    LogF("Old irScanRange used in %s",(const char *)GetName());
    // compatibility settings
    float irRange = par>>"irScanRange";
    _irScanRangeMin = irRange;
    _irScanRangeMax = irRange;
    _irScanToEyeFactor = 1;
  }

  GET_PAR(irScanGround);
  GET_PAR(nightVision);

  GET_PAR(laserScanner);
  GET_PAR(laserTarget);
  GET_PAR(nvTarget);
  GET_PAR(artilleryTarget);
  GET_PAR(artilleryScanner);

  GET_PAR(commanderCanSee);
  GET_PAR(driverCanSee);
  GET_PAR(gunnerCanSee);

  GET_PAR(radarType);

  GET_PAR(attendant);
  GET_PAR(engineer);
  GET_PAR(preferRoads);

  ParamEntryVal types = par >> "unitInfoType";
  if (types.IsArray())
  {
    int n = types.GetSize();
    _unitInfoTypes.Realloc(n);
    _unitInfoTypes.Resize(n);
    for (int i=0; i<n; i++) _unitInfoTypes[i] = types[i];
  }
  else
  {
    _unitInfoTypes.Realloc(1);
    _unitInfoTypes.Resize(1);
    _unitInfoTypes[0] = types;
  }

  GET_PAR(hideUnitInfo);
  
  GetValue(_mainSound, par>>"soundEngine");
  GetValue(_envSound, par>>"soundEnviron");
  GetValue(_dmgSound, par>>"soundDammage");
  GetValue(_crashSound, par>>"soundCrash");
  GetValue(_landCrashSound, par>>"soundLandCrash");
  GetValue(_waterCrashSound, par>>"soundWaterCrash");
  GetValue(_getInSound, par>>"soundGetIn");
  GetValue(_getOutSound, par>>"soundGetOut");
  GetValue(_servoSound, par>>"soundServo");
  GetValue(_engineOnSoundInt, par>>"soundEngineOnInt");
  GetValue(_engineOffSoundInt, par>>"soundEngineOffInt");
  GetValue(_engineOnSoundExt, par>>"soundEngineOnExt");
  GetValue(_engineOffSoundExt, par>>"soundEngineOffExt");
  GetValue(_lockedSound, par>>"soundLocked");
  GetValue(_incommingSound, par>>"soundIncommingMissile");


  ConstParamEntryPtr woodCrashEntry = par.FindEntry("soundWoodCrash");
  if (woodCrashEntry)
  {
    _woodCrashSound.Load(par, "soundWoodCrash");
    //GetValue(_woodCrashSound, par>>"soundWoodCrash");
  }

  ConstParamEntryPtr buildCrashEntry = par.FindEntry("soundBuildingCrash");
  if (buildCrashEntry)
  {
    _buildingCrashSound.Load(par, "soundBuildingCrash");
    //GetValue(_buildingCrashSound, par>>"soundBuildingCrash");
  }

  ConstParamEntryPtr armorCrashEntry = par.FindEntry("soundArmorCrash");
  if (armorCrashEntry)
  {
    _armorCrashSound.Load(par, "soundArmorCrash");
    //GetValue(_armorCrashSound, par >> "soundArmorCrash");
  }

  _extEnvSoundsMaxVol = 0;
  if (_envSound.name.GetLength()>0)
    _extEnvSoundsMaxVol = _envSound.vol;

  ParamEntryVal extSounds = par >> "SoundEnvironExt";
  n = extSounds.GetEntryCount();
  _extEnvSounds2D.Realloc(n);
  _extEnvSounds2D.Resize(n);
  for (int i=0; i<n; i++)
  {
    ParamEntryVal par = extSounds.GetEntry(i);
    int m = par.GetSize();
    ExtSoundInfo2D &info2D = _extEnvSounds2D[i];
    // par should be array of arrays
    info2D.name = par.GetName();
    info2D.infos.Resize(0);
    info2D.pars.Resize(0);
    for (int j=0; j<m; j++)
    {
      // check which format is used
      const IParamArrayValue &item = par[j];
      if (item.GetItemCount() == 2)
      {
        // 2D format
        RString name = item[0];
        // find / create the table
        int index = -1;
        for (int k=0; k<info2D.infos.Size(); k++)
        {
          if (stricmp(info2D.infos[k].name, name) == 0)
          {
            index = k;
            break;
          }
        }
        if (index < 0)
        {
          index = info2D.infos.Add();
          info2D.infos[index].name = name;
        }
        ExtSoundInfo &info = info2D.infos[index];
        // add the sound
        int index2 = info.pars.Add();
        SoundPars &pars = info.pars[index2];
        GetValue(pars, item[1]);
        if (pars.name.GetLength()>0 && pars.vol>_extEnvSoundsMaxVol)
          _extEnvSoundsMaxVol = pars.vol;
      }
      else
      {
        // 1D format
        if(item.GetItemCount() != 4)
          RptF("4 items expected in %s, got %d",cc_cast(extSounds.GetContext(par.GetName())),item.GetItemCount());
        int index = info2D.pars.Add();
        SoundPars &pars = info2D.pars[index];
        GetValue(pars, item);
        if (pars.name.GetLength()>0 && pars.vol>_extEnvSoundsMaxVol)
           _extEnvSoundsMaxVol = pars.vol;
      }
    }
    info2D.pars.Compact();
    info2D.infos.Compact();
    for (int j=0; j<info2D.infos.Size(); j++)
      info2D.infos[j].pars.Compact();
  }
  _extEnvSounds2D.Compact();

  ParamEntryVal soundMunition = par >> "SoundGear";
  if (soundMunition.IsClass())
  {
    LoadExtSoundInfo2D(soundMunition, _gearSounds2D);
    _gearSounds2D.Compact();
  }

  ParamEntryVal equipPars = par >> "SoundEquipment";
  if (equipPars.IsClass())
  {
    LoadExtSoundInfo2D(equipPars, _equipSounds2D);
    _equipSounds2D.Compact();
  }

  // Load weapons info
  _weapons.Load(par);

  // threat
  ParamEntryVal threat=par>>"threat";
  _threat[VSoft]=threat[0];
  _threat[VArmor]=threat[1];
  _threat[VAir]=threat[2];

  // cargo
  #if _DEBUG
    if (strstr(_name._className,"GRAD"))
    {
      __asm nop;
    }
  #endif

  // TODO: walk all weapons, use heuristics to adjust threats accordingly, e.g. check if there are any weapons able to hit air targets

  _kind=(VehicleKind)(par>>"type").GetInt();

  entry = par.FindEntry("forceSupply");
  if (entry) _forceSupply = *entry;
  else _forceSupply = false;

  entry = par.FindEntry("showWeaponCargo");
  if (entry) _showWeaponCargo = *entry;
  else _showWeaponCargo = false;

  _showDmgPoint = VZero;
  _shapeReversed=par>>"reversed";
  EntityType::_shapeReversed=_shapeReversed;

  _lockTargetAction = UALockTargets;

#if _ENABLE_INDEPENDENT_AGENTS && _ENABLE_IDENTITIES
  entry = par.FindEntry("agentTasks");
  if (entry)
  {
    int n = entry->GetSize();
    _agentTasks.Realloc(n);
    _agentTasks.Resize(n);
    for (int i=0; i<n; i++)
      _agentTasks[i] = (*entry)[i];
  }
#endif

  _headAimDown=-(float)(par>>"HeadAimDown")*(H_PI/180);


  ERROR_CATCH_ANY(exc)
    if (!errorOnce)
    {
      errorOnce = true;
      LogF("Error loading EntityAIType %s",cc_cast(par.GetContext()));
    }
  ERROR_END()
}

void EntityAIType::LoadExtSoundInfo2D(ParamEntryVal &entry, AutoArray<ExtSoundInfo2D> &data)
{
  int n = entry.GetEntryCount();

  data.Realloc(n);
  data.Resize(n);

  if (entry.IsClass())
  {
    for (int i = 0; i < n; i++)
    {
      // ExtSoundInfo2D arrays
      ParamEntryVal arrayPars = entry.GetEntry(i);

      if (arrayPars.IsArray())
      {
        ExtSoundInfo2D &info2D = data[i];

        info2D.name = arrayPars.GetName();
        info2D.infos.Resize(0);

        // number of items in array
        int m = arrayPars.GetSize();

        if (m > 0 && info2D.name.GetLength() > 0)
          LoadExtSoundInfo(arrayPars, info2D.infos);

        info2D.infos.Compact();
      }
    }
  }
}

void EntityAIType::LoadExtSoundInfo(ParamEntryVal &entry, AutoArray<ExtSoundInfo> &data)
{
  // number of items in array
  int n = entry.GetSize();
  
  for (int i = 0; i < n; i++)
  {
    // {"name", {"wav_name", db0, 1}}
    const IParamArrayValue &item = entry[i];

    if (item.GetItemCount() == 2)
    {
      // string group identifier
      RString distrType = item[0];

      if (distrType.GetLength() == 0) continue;

      // find / create the table
      int index = -1;

      for (int k = 0; k < data.Size(); k++)
      {
        if (stricmp(data[k].name, distrType) == 0)
        {
          index = k;
          break;
        }
      }
      // new item
      if (index < 0)
      {
        index = data.Add();
        data[index].name = distrType;
      }

      ExtSoundInfo &info = data[index];
      SoundPars &sPars = info.pars.Append();
      // sound pars
      GetValue(sPars, item[1]);            
    }
  }

  for (int i = 0; i < data.Size(); i++)
    data[i].pars.Compact();
}

const SoundPars &EntityAIType::GetEnvSoundExtRandom(RString surfaceSound, RString soundOverride) const
{
  const AutoArray<SoundPars> *pars = NULL;

  // phase 1 - search for sound for [surfaceSound, soundOverride]
  const ExtSoundInfo2D *info2D = NULL;
  for (int i=0; i<_extEnvSounds2D.Size(); i++)
  {
    if (stricmp(_extEnvSounds2D[i].name, surfaceSound) == 0)
    {
      info2D = &_extEnvSounds2D[i];
      break;
    }
  }
  if (info2D)
  {
    for (int i=0; i<info2D->infos.Size(); i++)
    {
      if (stricmp(info2D->infos[i].name, soundOverride) == 0)
      {
        pars = &info2D->infos[i].pars;
        break;
      }
    }
  }

  // phase 2 - search for sound for soundOverride only
  if (!pars)
  {
    for (int i=0; i<_extEnvSounds2D.Size(); i++)
    {
      if (stricmp(_extEnvSounds2D[i].name, soundOverride) == 0)
      {
        pars = &_extEnvSounds2D[i].pars;
        break;
      }
    }
  }

  // phase 3 - search for sound for surfaceSound only
  if (!pars && info2D)
    pars = &info2D->pars;

  // select the random sound
  if (pars)
  {
    int n = pars->Size();
    if (n > 0)
    {
      if (n == 1)
        return (*pars)[0];
      float val = GRandGen.RandomValue();
      int i = toIntFloor(val * n);
      saturate(i, 0, n - 1);
      return (*pars)[i];
    }
  }
  // use the default
  return _envSound;
}

const SoundPars* EntityAIType::GetRandomSound(const AutoArray<ExtSoundInfo2D> &data, RString first, RString second) const
{
  const ExtSoundInfo2D *info2D = NULL;

  // select side
  for (int i = 0; i < data.Size(); i++)
  {
    if (stricmp(data[i].name, first) == 0)
    {
      info2D = &data[i];
      break;
    }
  }

  const AutoArray<SoundPars> *pars = NULL;

  // select action
  if (info2D)
  {
    for (int i = 0; i < info2D->infos.Size(); i++)
    {
      if (stricmp(info2D->infos[i].name, second) == 0)
      {
        pars = &info2D->infos[i].pars;
        break;
      }
    }
  }

  // select the random sound
  if (pars)
  {
    int n = pars->Size();

    if (n > 0)
    {
      if (n == 1) return &((*pars)[0]);
      float val = GRandGen.RandomValue();
      int i = toIntFloor(val * n);
      saturate(i, 0, n - 1);

      return &((*pars)[i]);
    }
  }
  return NULL;
}

bool EntityAIFull::CanLock(const TargetType *type, const TurretContext &context, int weapon) const
{
  // check current ammo
  if (weapon < 0)
    weapon = context._weapons->ValidatedCurrentWeapon();
  if (weapon < 0)
    return false;

  const AmmoType *ammo = context._weapons->GetAmmoType(weapon);
  if (!ammo)
  {
    // FIX
    // if there is no mode, use some other way to check if it is LAW
    // check weapon typical magazine
    const MagazineSlot &slot = context._weapons->_magazineSlots[weapon];
    const MuzzleType *muzzle = slot._muzzle;
    if (!muzzle)
      return false;
    if (muzzle->_magazines.Size()<=0)
      return false;
    MagazineType *magazine = muzzle->_magazines[0];
    if (!magazine)
      magazine = muzzle->_typicalMagazine;
    if (!magazine)
      return false;
    // check first magazine mode
    ammo = magazine->_ammo;
  }
  if (!ammo)
    return false;
  return type->LockPossible(ammo);
}

/*!
\patch 1.33 Date 11/28/2001 by Ondra.
- Fixed: Laser guided bomb lock was possible on IR targets.
*/

bool EntityAI::LockPossible( const AmmoType *ammo ) const
{
  const EntityAIType *type=GetType();
  // laser target can locked with laser capable weapon only
  if (type->GetLaserTarget())
    return ammo->laserLock;
  else if (type->GetNvTarget())
    return ammo->nvLock;
  else if (type->GetArtilleryTarget())
    return ammo->artilleryLock;
  if (ammo->laserLock && !ammo->irLock && !type->GetLaserTarget() && !type->GetNvTarget())
    return false;
  //if (ammo->artilleryLock && !type->GetArtilleryTarget())
  //  return false;
  // disable locking of airborne targets
  if (Airborne() && !ammo->airLock)
    return false;
  // some weapons can lock only ir targets
  if (!type->GetIRTarget() && ammo->irLock)
    return false;
  return true;
}

bool EntityAI::IsArtilleryTarget()
{
  const EntityAIType *type=GetType();
  return type->GetArtilleryTarget();
}

#if _VBS3
float EntityAI::GetTotalWeightKg()
{
  float weight = 0.0;
  if(_supply)
  {
    weight += _supply->GetWeaponWeightKg();
    weight += _supply->GetMagazineWeightKg();
  }

  return weight;
}
#endif

#if _VBS2 // delete EH
void EntityAI::OnSetDelete()
{
  SetDeleteRaw(); // so OnSetDelete() isn't called again by editor
  OnEvent(EEDelete);
  Entity::OnSetDelete();
}

void EntityAI::OnAARMoveTime()
{
  _historyLine.RemoveOldUpdates();
  _historyLine.AddPath();
}

#endif 

/*!
\patch 1.78 Date 7/16/2002 by Ondra
- Fixed: setObjectTexture often did not work when repeating mission.
*/

void EntityAIType::InitShape()
{
  base::InitShape();

  // after shape is loaded
  const ParamEntry &par = *_par;

  _weapons.Init(_shape, par);

  _clan.Init(_shape,(par >> "selectionClan").operator RString(),NULL);
  _dashboard.Init(_shape,(par >> "selectionDashboard").operator RString(),NULL);
  _showDmg.Init(_shape,(par >> "selectionShowDamage").operator RString(),NULL);

  _backLights.Init(_shape,(par >> "selectionBackLights").operator RString(),NULL);

  _lights.Clear();

  RStringB nameSupplyPoint = par >> "memoryPointSupply";
  bool isSupplyPoint = _shape && _shape->MemoryPointExists(nameSupplyPoint);
  if( isSupplyPoint )
  {
    _supplyPoint=_shape->MemoryPoint(nameSupplyPoint);
    _supplyRadius=par>>"supplyRadius";
    if (_supplyRadius<0)
      _supplyRadius = _shape->GeometrySphere()*-_supplyRadius;
  }
  else
  {
    _supplyPoint=VZero;
    _supplyRadius=_shape ? (_shape->GeometrySphere()+1)*1.1 : 1;
  }

  _enableGPS = par >> "enableGPS";

  // check if point is outside of the model
  // if not, issue warning
  if (IsSupply() && _shape && _shape->IsInside(_supplyPoint))
  {
    LogF("%s: supply point inside the model.",(const char *)_shape->Name());
    // we should find some solution
    // quick patch is moving it to bounding sphere
    Vector3 supplyDir = _supplyPoint-_shape->GeometryCenter();
    float minDist = _shape->GeometrySphere();

    if (supplyDir.SquareSize()<Square(minDist))
    {
      if (supplyDir.SquareSize()<1e-6)
        supplyDir = VForward;
      else
        supplyDir.Normalize();
      supplyDir *= minDist;
      _supplyPoint = _shape->GeometryCenter()+supplyDir;
      LogF("  supply point patched.");
      if (!isSupplyPoint)
        _supplyRadius= 1.5f+ _shape->GeometrySphere()*0.1f;
    }
  }

  const Shape *memory = _shape ? _shape->MemoryLevel() : NULL;
  if (memory)
  {
    ParamEntryVal list = par >> "MarkerLights";
    for (int i=0; i<list.GetEntryCount(); i++)
    {
      ParamEntryVal entry = list.GetEntry(i);
      if (!entry.IsClass()) continue;
      
      int index = _shape->FindNamedSel(memory,(entry >> "name").operator RString());
      if (index < 0) continue;

      PackedColor color; GetValue(color, entry >> "color");
      PackedColor ambient; GetValue(ambient, entry >> "ambient");
      float brightness = entry >> "brightness";
      bool blinking = entry >> "blinking";

      const NamedSelection::Access sel(memory->NamedSel(index),_shape,memory);
      for (int i=0; i<sel.Size(); i++)
      {
        int pt = sel[i];
        Vector3Val point = memory->Pos(pt);
        LightInfo &light = _lights.Set(_lights.Add());
        light.type = blinking ? LightTypeMarkerBlink : LightTypeMarker;
        light.position = point;
        light.direction = VUp;
        light.color = color;
        light.ambient = ambient;
        light.brightness = brightness;
        light.nvgMarker = false;
      }
    }

    list = par >> "NVGMarkers";
    for (int i = 0; i < list.GetEntryCount(); i++)
    {
      ParamEntryVal entry = list.GetEntry(i);
      if (!entry.IsClass()) continue;

      int index = _shape->FindNamedSel(memory,(entry >> "name").operator RString());
      if (index < 0) continue;

      PackedColor color; GetValue(color, entry >> "color");
      PackedColor ambient; GetValue(ambient, entry >> "ambient");
      float brightness = entry >> "brightness";
      bool blinking = entry >> "blinking";

      const NamedSelection::Access sel(memory->NamedSel(index),_shape,memory);
      for (int i=0; i<sel.Size(); i++)
      {
        int pt = sel[i];
        Vector3Val point = memory->Pos(pt);
        LightInfo &light = _lights.Append();
        light.type = blinking ? LightTypeMarkerBlink : LightTypeMarker;
        light.position = point;
        light.direction = VUp;
        light.color = color;
        light.ambient = ambient;
        light.brightness = brightness;
        light.nvgMarker = true;
      }
    }
  }


  if (_shape)
  {
    // hidden selections
    ParamEntryVal hiddenSelections = par >> "hiddenSelections";
    int n = hiddenSelections.GetSize();
    _hiddenSelections.Realloc(n);
    _hiddenSelections.Resize(n);
    for (int i=0; i<n; i++)
    {
      RString name = hiddenSelections[i];
      _hiddenSelections[i].Init(_shape, name, NULL);
    }
    // textures on hidden selections
    hiddenSelections = par >> "hiddenSelectionsTextures";
    int m = hiddenSelections.GetSize();
    saturateMin(m, n);
    _hiddenSelectionsInitTextures.Realloc(n);
    _hiddenSelectionsInitTextures.Resize(n);
    for (int i=0; i<m; i++)
    {
      RString name = hiddenSelections[i];
      if (name.GetLength() > 0)
      {
        name.Lower();
        _hiddenSelectionsInitTextures[i] = GlobLoadTexture(GetPictureName(name));
      }
    }
    
    RStringB damageSel = par >> "selectionDamage";
    _totalDamage.Init(_shape, _damageInfo, damageSel, NULL);
  }
  else
  {
    _hiddenSelections.Clear();
    _hiddenSelectionsInitTextures.Clear();
  }

  // Reflectors
  _reflectors.Init(this, par, GetArmor(), _damageInfo);

  ConstParamEntryPtr array = par.FindEntry("EventHandlers");
  if (array)
  {
    for (int i=0; i<NEntityEvent; i++)
    {
      EntityEvent e = (EntityEvent)i;
      RString name = FindEnumName(e);
      ConstParamEntryPtr entry = array->FindEntry(name);
      if (entry)
      {
        RStringB expression = *entry;
        _eventHandlers[e] = expression;
      }
    }
  }

  // user type actions
  ConstParamEntryPtr userActions = par.FindEntry("UserActions");
  if(userActions)
  {
    const UIActionTypeDesc &desc = UIActions::actionTypes[ATUserType];
    for (ParamClass::Iterator<> i(userActions->GetClassInterface()); i; ++i)
    {
      UserTypeAction &action = _userTypeActions.Append();
      ParamEntryVal entry = *i;

      action.name = entry.GetName();
      action.displayName = entry >> "displayName";
      RString pos = entry >> "position";
      action.modelPosition = _shape ? _shape->MemoryPoint(pos) : VZero;
#if _VBS3 //store pointIndex to animate
      const Shape* mem = _shape->MemoryLevel();
      action.positionIndex = (_shape && mem) ? _shape->PointIndex(mem,pos) : -1;
#endif

      action.radius = entry >> "radius";
      action.condition = entry >> "condition";
      action.statement = entry >> "statement";
      action.onlyForPlayer = entry>>"onlyForPlayer";

      ConstParamEntryPtr subentry;
      
      subentry = entry.FindEntry("displayNameDefault");
      action.displayNameDefault = subentry ? *subentry : action.displayName;

      subentry = entry.FindEntry("priority");
      action.priority = subentry ? *subentry : desc.priority;
      
      subentry = entry.FindEntry("showWindow");
      action.showWindow = subentry ? *subentry : desc.showWindow;
      
      subentry = entry.FindEntry("hideOnUse");
      action.hideOnUse = subentry ? *subentry : desc.hideOnUse;

      action.shortcut = desc.shortcut;
      subentry = entry.FindEntry("shortcut");
      if (subentry)
      {
        RString name = *subentry;
        if (name.GetLength() > 0) 
        {
          for (int i=0; i<UAN; i++)
          {
            if (stricmp(name, Input::userActionDesc[i].name) == 0)
            {
              action.shortcut = (UserAction)i;
              break;
            }
          }
        }
      }
    }
  }
}

void EntityAIType::DeinitShape()
{
  _weapons.Deinit(_shape);

  _hiddenSelections.Clear();
  _hiddenSelectionsInitTextures.Clear();
  _totalDamage.Unload();

  base::DeinitShape();
}

bool EntityAIType::IsShapeReady
(
  const LODShape *lodShape, int level,
  const Ref<ProxyObject> *proxies, int nProxies,
  bool requestIfNotReady
)
{
  bool ret = base::IsShapeReady(lodShape,level,proxies,nProxies,requestIfNotReady);
  return ret;
}

void EntityAIType::InitShapeLevel(int level)
{
  base::InitShapeLevel(level);

  _weapons.InitLevel(_shape, level);

  _clan.InitLevel(_shape,level);
  _dashboard.InitLevel(_shape,level);
  _showDmg.InitLevel(_shape,level);

  _backLights.InitLevel(_shape,level);

  if (level==_shape->FindSpecLevel(VIEW_PILOT))
  {
    _showDmgPoint = _shape->NamedPoint(level,"poskozeni");
  }
  
  _reflectors.InitLevel(_shape, level);

  for (int i=0; i<_hiddenSelections.Size(); i++)
    _hiddenSelections[i].InitLevel(_shape,level);
  if (_damageInfo && !_damageInfo->IsEmpty())
    _totalDamage.InitLevel(_shape,level);
}

void EntityAIType::DeinitShapeLevel(int level)
{
  _weapons.DeinitLevel(_shape, level);

  _clan.DeinitLevel(_shape,level);
  _dashboard.DeinitLevel(_shape,level);
  _showDmg.DeinitLevel(_shape,level);

  _backLights.DeinitLevel(_shape,level);

  _reflectors.DeinitLevel(_shape, level);

  for (int i=0; i<_hiddenSelections.Size(); i++)
    _hiddenSelections[i].DeinitLevel(_shape,level);
  if (_damageInfo && !_damageInfo->IsEmpty())
    _totalDamage.DeinitLevel(_shape,level);
  base::DeinitShapeLevel(level);
}

#if _VBS3
int EntityAIType::GetHiddenSelectionIndex(RString selection) const
{
  selection.Lower();
  for(int i=0; i < _hiddenSelections.Size(); ++i)
  {
    const AnimationDef *def = _hiddenSelections[i].GetDef();
    if(def && def->name == selection)
      return i;
  }
  return -1;
}
#endif
/*!
\patch 1.50 Date 4/15/2002 by Ondra
- Fixed: When setViewDistance is used to increase visible range,
tanks did not adjust their IR detector range accordingly.
*/

float EntityAIType::GetIRScanRange() const
{
  //float eyeRange = TACTICAL_VISIBILITY;
  //float eyeRange = Glob.config.tacticalZ;
  float eyeRange = Glob.config.tacticalZ;
  float irRange = eyeRange*_irScanToEyeFactor;
  saturate(irRange,_irScanRangeMin,_irScanRangeMax);
  return irRange;
}

float EntityAIType::VisibleSize() const
{
  // assume default vehicle radius is 3 m
  return _shape ? _shape->BoundingSphere()*0.5f : 3.0f;
}

LSError Serialize(ParamArchive &ar, RString name, Ref<const EntityType> &value, int minVersion)
{
  if (ar.GetArVersion() < minVersion) return LSOK;
  if (ar.IsSaving())
  {
    RString str = value ? value->GetName() : "";
    CHECK(ar.Serialize(name, str, minVersion));
  }
  else
  {
    if (ar.GetPass() != ParamArchive::PassFirst) return LSOK;
    RString str;
    CHECK(ar.Serialize(name, str, minVersion));
    if (str.GetLength() > 0)
      value = VehicleTypes.New(str);
    else
      value = NULL;
  }
  return LSOK;
}

LSError Serialize(ParamArchive &ar, RString name, Ref<const EntityType> &value, int minVersion, const EntityType *defValue)
{
  if (ar.GetArVersion() < minVersion)
  {
    if (!ar.IsSaving()) value = defValue;
    return LSOK;
  }
  if (ar.IsSaving())
  {
    RString str = value ? value->GetName() : "";
    CHECK(ar.Serialize(name, str, minVersion));
  }
  else
  {
    if (ar.GetPass() != ParamArchive::PassFirst) return LSOK;
    RString str;
    CHECK(ar.Serialize(name, str, minVersion));
    if (str.GetLength() > 0)
      value = VehicleTypes.New(str);
    else
      value = NULL;
  }
  return LSOK;
}

LSError Serialize(ParamArchive &ar, RString name, Ref<const EntityAIType> &value, int minVersion)
{
  Ref<const EntityType> tValue(value);
  LSError ok = Serialize(ar,name,tValue,minVersion);
  value = static_cast<const EntityAIType *>(tValue.GetRef());
  return ok;
}

LSError Serialize(ParamArchive &ar, RString name, Ref<const EntityAIType> &value, int minVersion, const EntityAIType *defValue)
{
  Ref<const EntityType> tValue(value);
  LSError ok = Serialize(ar,name,tValue,minVersion,defValue);
  value = static_cast<const EntityAIType *>(tValue.GetRef());
  return ok;
}

VehicleTypeBank VehicleTypes;

void ObjectReflectors::UpdateAggReflectors(EntityAI *object)
{
  // Go through all the lights groups
  for (int i = 0; i < _aggReflectors.Size(); i++)
  {
    // Get aggregated values
    Color aggColor = HBlack;
    Color aggColorAmbient = HBlack;
    Vector3 aggPosition = Vector3(0, 0, 0);
    Vector3 aggDirection = Vector3(0, 0, 0);
    float aggAngle = 0.0f;
    float aggSize = 0.0f;
    float aggBrightness = 0.0f;
    AggReflectorGroup &aggGroup = _aggReflectors[i];
    int realReflectorsSize = aggGroup._realReflectors.Size();
    DoAssert(realReflectorsSize > 0);
    int aggOnCount = 0;
    bool isAttached = false;
    for (int j = 0; j < realReflectorsSize; j++)
    {
      const Ref<LightReflectorOnVehicle> &reflector = aggGroup._realReflectors[j];
      if (reflector->IsOn())
      {
        aggColor += reflector->GetDiffuse();
        aggColorAmbient += reflector->GetAmbient();
        aggPosition += reflector->GetAttachedPosition();
        aggDirection += reflector->GetAttachedDirection();
        aggAngle += reflector->Angle();
        aggSize += reflector->GetSize();
        //aggBrightness += Square(reflector->GetBrightness());
        aggBrightness += reflector->GetBrightness();
        isAttached |= reflector->AttachedOn()!=NULL;
        aggOnCount++;
      }
    }

    // Add aggregated light into scene
    float invReflectorsSize = (aggOnCount > 0) ? 1.0f / aggOnCount : 1.0f;
    //aggColor = aggColor * invReflectorsSize;
    //aggColorAmbient = aggColorAmbient * invReflectorsSize;
    aggPosition *= invReflectorsSize;
    aggDirection *= invReflectorsSize;
    aggAngle *= invReflectorsSize;
    aggSize *= invReflectorsSize;
    //aggBrightness = sqrt(aggBrightness);
    aggBrightness *= invReflectorsSize;
    if (aggGroup._aggReflector.IsNull())
    {
      LightReflectorOnVehicle *light = new LightReflectorOnVehicle(
        aggGroup._realReflectors[0]->GetShape(), aggColor, aggColorAmbient,
        isAttached ? object : NULL, aggPosition, aggDirection,
        aggAngle, aggSize);
      light->SetBrightness(aggBrightness);
      GScene->AddLightAgg(light);
      aggGroup._aggReflector = light;
    }
    else
    {
      aggGroup._aggReflector->SetDiffuse(aggColor);
      aggGroup._aggReflector->SetAmbient(aggColorAmbient);
      aggGroup._aggReflector->SetAttachedPos(aggPosition, aggDirection);
      aggGroup._aggReflector->SetAngle(aggAngle);
      aggGroup._aggReflector->SetSize(aggSize);
      aggGroup._aggReflector->SetBrightness(aggBrightness);
    }
    aggGroup._aggReflector->Switch(aggOnCount > 0);
  }
}

void ObjectReflectors::Animate(AnimationContext &animContext, int level, const EntityAI *object, const ObjectTypeReflectors &typeReflectors)
{
  for (int i=0; i<_aggReflectors.Size(); i++)
  {
    AggReflectorGroup &arg = _aggReflectors[i];
    const ReflectorInfoArray &ria = typeReflectors._aggReflectors[i];
    for (int j = 0; j < arg._realReflectors.Size(); j++)
    {
      const ReflectorInfo &info = *ria[j];
      bool on = object->IsLightOn(animContext.GetVisualState()) && object->GetHit(info.hitPoint) < 0.9;
      if (on && !GEngine->GetThermalVision())
        info.selection.Unhide(animContext, object->GetShape(), level);
      else
        info.selection.Hide(animContext, object->GetShape(), level);
    }
  }
}

static bool NeedsPositionUpdates(Object *object, int positionIndex)
{
  LODShape *objShape = object->GetShape();
  if (objShape && positionIndex>=0)
  {
    int objLevel = objShape->FindMemoryLevel();
    if (object->IsPointAnimated(objLevel,positionIndex))
      return true;
  }
  return !object->Static() || object->GetDestructType()!=DestructNo;
}

ObjectReflectors:: ObjectReflectors()
:_objectIsAttached(false)
{

}

void ObjectReflectors::Create(EntityAI *object, const ObjectTypeReflectors &typeReflectors)
{
  int n = typeReflectors._aggReflectors.Size();
  if ((n > 0) && (typeReflectors._aggReflectors[0].Size() == 0))
  {
    Fail("Error: Not valid _aggReflectors type array");
  }
  _aggReflectors.Realloc(n);
  _aggReflectors.Resize(n);
  if (GScene)
  {
    LODShapeWithShadow *shape = GScene->Preloaded(CobraLight);
    if (shape)
    {
      for (int i=0; i<n; i++)
      {
        AggReflectorGroup &arg = _aggReflectors[i];
        const ReflectorInfoArray &ria = typeReflectors._aggReflectors[i];
        int riaSize = ria.Size();
        arg._realReflectors.Realloc(riaSize);
        arg._realReflectors.Resize(riaSize);
        for (int j = 0; j < ria.Size(); j++)
        {
          const ReflectorInfo &info = *ria[j];
#if _VBS3_PERPIXELPSLIGHTS
          static float angle = 50.0f;
#else
          const float angle = info.angle; // 36.0f;
#endif
          // when the light is permanent and the point not animated, we can do with a static permanent light
          bool needsPositionUpdates = NeedsPositionUpdates(object,info.positionIndex);
          if (needsPositionUpdates) object->AttachLights();
          bool needsAttachment = needsPositionUpdates || info.speed>0;
          LightReflectorOnVehicle *light = new LightReflectorOnVehicle(
            shape, info.color, info.colorAmbient,
            needsAttachment ? object : NULL,
            info.position, info.direction,
            angle, info.size);
          light->SetBrightness(info.brightness);
          GScene->AddLightReal(light);
          arg._realReflectors[j] = light;
          if (!needsAttachment)
          {
            object->UpdateReflectors(); // we need to positions to be updated so that we can create the lights
            // TODO: updating this particular reflector would be enough
            light->UpdatePositionTo(object->FutureVisualState().Transform());
          }
        }
      }
      UpdateAggReflectors(object);
    }
  }
}

void ObjectReflectors::Switch(const EntityAI *object, const ObjectTypeReflectors &typeReflectors)
{
  for (int i=0; i<_aggReflectors.Size(); i++)
  {
    AggReflectorGroup &arg = _aggReflectors[i];
    const ReflectorInfoArray &ria = typeReflectors._aggReflectors[i];
    for (int j = 0; j < arg._realReflectors.Size(); j++)
    {
      Light *light = arg._realReflectors[j];
      const ReflectorInfo &info = *ria[j];
      bool on = object->IsLightOn(object->FutureVisualState()) && object->GetHit(info.hitPoint) < 0.9;
      if (on && info.speed > 0)
      {
        // blinking
        float phase = info.speed * Glob.time.toFloat();
        phase -= toIntFloor(phase);
        on = phase < info.phaseLimit;
      }
      light->Switch(on);
    }
  }
}

void ObjectReflectors::AnimatePosition(const EntityAI *object, const ObjectTypeReflectors &typeReflectors)
{
  if (!object->GetShape()) return;

  int level = object->GetShape()->FindMemoryLevel();
  if (level < 0) return;

  const ObjectVisualState &vs = object->RenderVisualState();
  for (int i=0; i<_aggReflectors.Size(); i++)
  {
    AggReflectorGroup &arg = _aggReflectors[i];
    const ReflectorInfoArray &ria = typeReflectors._aggReflectors[i];
    for (int j = 0; j < arg._realReflectors.Size(); j++)
    {
      const ReflectorInfo &info = *ria[j];
      if (info.positionIndex >= 0)
      {
        Vector3Val position = object->AnimatePoint(vs,level, info.positionIndex);
        Vector3 direction = VForward;
        if (info.directionIndex >= 0)
        {
          direction = object->AnimatePoint(vs, level, info.directionIndex) - position;
          direction.Normalize();
        }
        LightReflectorOnVehicle *light = arg._realReflectors[j];
        light->SetAttachedPos(position, direction);
      }
    }
  }
}

void ObjectReflectors::Clear()
{
  _aggReflectors.Clear();
}

void ObjectReflectors::SwitchLight(bool on)
{
  for (int i=0; i<_aggReflectors.Size(); i++)
  {
    AggReflectorGroup &aggGroup = _aggReflectors[i];
    aggGroup._aggReflector->Switch(on);
    for (int j = 0; j < aggGroup._realReflectors.Size(); j++)
    {
      aggGroup._realReflectors[j]->Switch(on);
    }
  }
}

DEFINE_CASTING(EntityAI)

#if _VBS2
#include "hla/AAR.hpp"

#if _MOTIONCONTROLLER
#include "hla/MotionController.hpp"
#endif 

#endif


/*!
\param type Entity type
\param fullCreate When fullCreate is false, entity will be serialized
   or transfered over network and many thing need not be initialized
   (e.g. weapon list).
*/

EntityAI::EntityAI(const EntityAIType *type, bool fullCreate)
:Entity(type->_shape, type, CreateObjectId()),  // bubble allocateVisualState to Entity

_stratGoToPos(VZero),

_avoidSpeed(1e5),
_avoidSpeedTime(TIME_MIN),

_avoidAside(0),_avoidAsideWanted(0), // obstacle avoidance offset
_lastSimplePath(TIME_MIN),_simplePathFrom(VZero),_simplePathDst(VZero),

_isDead(false),_isStopped(false),_isUpsideDown(false),
_userStopped(false), _pilotLight(false),
#if _VBS3
  _lightMode(-1),
#endif

_showFlag(true),

_inFormation(true),
_whenDestroyed(TIME_MAX),

_sensorColID(-1),

#if ENABLE_ALLOW_DAMAGE
_allowDamage(true),
#endif

_locked(false),_lockedAsWaiting(false),
_locker(new AILocker()),

_lastDammageTime(Glob.time - 120),
_limitSpeed(GetType()->GetMaxSpeedMs()*2),

_nextUserActionId(0),
_vars(true),
_snappedPosition(VZero)

// fireat/goto automat
//_moveMode(gotoWait),
{
  Assert(type == GetType())
  Assert( type );
  Assert( !type->IsAbstract() );
  Assert( _shape==type->_shape );

  _targetSide=type->_typicalSide;
  _lastMovement=Glob.time;

  _hiddenSelectionsTextures = type->_hiddenSelectionsInitTextures;

  _assembleTo = NULL;

  if( type->IsSupply() )
  {
    _supply=new ResourceSupply(this, fullCreate);
  }

#if _VBS2
  _postFrame = M4Identity;
# if _AAR
    _historyLine.Init(this);
# endif
#endif

}

EntityAI::~EntityAI()
{
  if (_supply)
    _supply->Destroy(this);

  if(_assembleTo)
  {
    if(GWorld->IsOutVehicle(_assembleTo))
    {    
      GWorld->RemoveOutVehicle(_assembleTo);
      _assembleTo->SetDelete();
    }
    SetAseembleTo(NULL);
  }

  if (_lightsAttached)
  {
    GWorld->RemoveAttachment(this);
  }
  PerformUnlock();
}

void EntityAI::SetObjectTexture(int index, Texture *texture)
{
  if (index >= 0 && index < _hiddenSelectionsTextures.Size())
    _hiddenSelectionsTextures[index] = texture;
}

#if _VBS3
void EntityAI::SetObjectTexture(RString selection, Texture *texture)
{
  SetObjectTexture(GetType()->GetHiddenSelectionIndex(selection), texture);
}

void EntityAI::ResetObjectTexture(int index)
{
  if (index >= 0 && index < GetType()->_hiddenSelectionsInitTextures.Size())
    SetObjectTexture(index, GetType()->_hiddenSelectionsInitTextures[index]);
}
void EntityAI::ResetObjectTexture(RString selection)
{
  ResetObjectTexture(GetType()->GetHiddenSelectionIndex(selection));
}
RString EntityAI::GetHiddenSelectionTexture(int index)
{
  if (index >= 0 && index < _hiddenSelectionsTextures.Size())
  {
    if(_hiddenSelectionsTextures[index])
      return _hiddenSelectionsTextures[index]->GetName();
  }
  return RString();
}
#endif
//bool EntityAI::HasSomeWeapons() const
//{
//  return false;
//}

bool EntityAI::FindWeapon(const WeaponType *weapon, int *count) const
{
  return _supply && _supply->FindWeapon(weapon, count);
}

class HasWeaponsFunc : public ITurretFunc
{
public:
  bool operator () (EntityAIFull *entity, TurretContext &context)
  {
    // if we have no weapon or no ammo, we are out of fight
    return context._weapons->_weapons.Size() > 0 && context._weapons->_magazines.Size() > 0;
  }
};

bool EntityAIFull::HasSomeWeapons() const
{
  HasWeaponsFunc func;
  return ForEachTurret(func);
}

class FindWeaponFunc : public ITurretFunc
{
protected:
  int *_count; // out
  const WeaponType *_weapon;
public:
  FindWeaponFunc(const WeaponType *weapon, int *count) : _count(count), _weapon(weapon) {}
  bool operator () (EntityAIFull *entity, TurretContext &context)
  {
    if (_count)
    {
      *_count = 0;
      for (int i=0; i<context._weapons->_weapons.Size(); i++)
        if (context._weapons->_weapons[i] == _weapon)
          (*_count)++;
      return false; // count them all
    }
    else
    {
      for (int i=0; i<context._weapons->_weapons.Size(); i++)
        if (context._weapons->_weapons[i] == _weapon)
          return true; // found
      return false; // continue
    }
  }
};

bool EntityAIFull::FindWeapon(const WeaponType *weapon, int *count) const
{
  if (base::FindWeapon(weapon, count))
    return true;
  FindWeaponFunc func(weapon, count);
  bool found = ForEachTurret(func);
  return count ? *count > 0 : found;
}

int EntityAIFull::AutoselectWeapon(const Person *gunner)
{
  TurretContext context;
  if (!FindTurret(gunner, context))
    return -1;
  int weapon = context._weapons->_currentWeapon;
  if (weapon < 0) weapon = context._weapons->FirstWeapon(this);
  weapon = context._weapons->ValidateWeapon(weapon);
  // check if we are gunner now. If yes, make sure some weapon is active
  context._weapons->SelectWeapon(this, weapon);
  return context._weapons->_currentWeapon;
}

const Magazine *EntityAI::FindMagazine(RString name, int *count) const
{
  return _supply ? _supply->FindMagazine(name, count) : NULL;
}

bool EntityAI::FindMagazine(const Magazine *magazine) const
{
  return _supply && _supply->FindMagazine(magazine);
}

const Magazine *EntityAI::FindMagazine(int creator, int id) const
{
  return _supply ? _supply->FindMagazine(creator, id) : NULL;
}

void EntityAI::OfferWeapon(EntityAIFull *to, const WeaponType *weapon, int slots, bool useBackpack)
{
  if (_supply)
    _supply->OfferWeapon(this, to, weapon, slots, useBackpack);
}

void EntityAI::OfferMagazine(EntityAIFull *to, const MagazineType *type, bool useBackpack)
{
  if (_supply)
    _supply->OfferMagazine(this, to, type, useBackpack);
}

void EntityAI::OfferBackpack(EntityAIFull *to, RString name)
{
  if (_supply)
    _supply->OfferBackpack(this, to, name);
}

void EntityAI::ReturnWeapon(const WeaponType *weapon)
{
  if (_supply)
    _supply->ReturnWeapon(this, weapon);
}

void EntityAI::ReturnMagazine(Magazine *magazine)
{
  if (_supply)
    _supply->ReturnMagazine(this, magazine);
}

void EntityAI::ReturnBackpack(EntityAI *backpack)
{
  if (_supply)
    _supply->ReturnBackpack(this, backpack);
}

class FindMagazineFunc : public ITurretFunc
{
protected:
  const Magazine *_magazine;

public:
  FindMagazineFunc(const Magazine *magazine) : _magazine(magazine) {}
  bool operator () (EntityAIFull *entity, TurretContext &context)
  {
    for (int i=0; i<context._weapons->_magazines.Size(); i++)
      if (context._weapons->_magazines[i] == _magazine)
        return true;
    return false;
  }
};

/*!
\param magazine checked magazine
\return true if magazine found, false otherwise
*/
bool EntityAIFull::FindMagazine(const Magazine *magazine) const
{
  if (base::FindMagazine(magazine))
    return true;
  FindMagazineFunc func(magazine);
  return ForEachTurret(func);
}

bool CheckAccess(ParamEntryPar entry);
bool CheckAccessCreate(ParamEntryPar entry);


/*!
\param muzzle muzzle where magazine will be attached
\param magazine attached magazine
*/
void EntityAIFull::AttachMagazine(const TurretContext &context, const MuzzleType *muzzle, Magazine *magazine)
{
  if (!muzzle->CanUse(magazine->_type))
  {
    RptF("Cannot use magazine %s in muzzle %s",
      (const char *)magazine->_type->GetName(),
      (const char *)muzzle->GetName());
    return;
  }
  for (int i=0; i<context._weapons->_magazineSlots.Size(); i++)
  {
    if (context._weapons->_magazineSlots[i]._muzzle == muzzle)
      context._weapons->_magazineSlots[i]._magazine = magazine;
  }
}

void EntityAIFull::SelectWeaponCommander(AIBrain *unit, const TurretContext &context, int weapon)
{
  if (weapon < 0 || weapon >= context._weapons->_magazineSlots.Size()) weapon = -1;
  if (context._turret)
  {
    if (context._turret->IsLocal())
      context._weapons->SelectWeapon(this, weapon);
    else
      GetNetworkManager().AskForSelectWeapon(this, context._turret, weapon);
  }
  else
  {
    if (IsLocal())
      context._weapons->SelectWeapon(this, weapon);
    else
      GetNetworkManager().AskForSelectWeapon(this, context._turret, weapon);
  }
}

/*!
During some operations weapon manipulation is not possible
like during burst or during reload animation
*/

bool EntityAIFull::EnableWeaponManipulation() const
{
  // TODO: check for all turrets
  TurretContext context;
  if (!GetPrimaryGunnerTurret(context)) return true;

  int selected = context._weapons->_currentWeapon;

  /**/
  // during some operations weapon manipulation is not possible
  // like during burst or during reload animation
  // check if we are in the middle of the burst
  if (selected >= 0 && selected < context._weapons->_magazineSlots.Size())
  {
    // safefy patch:
    // if new weapon is selected, cancel rest of the burst
    const MagazineSlot &slot = context._weapons->_magazineSlots[selected];
    Magazine *magazine = slot._magazine;

    if (magazine && magazine->_burstLeft>0)
      return false;
  }
  return true;
}

bool EntityAI::IsCautious() const
{
  AIBrain *unit = PilotUnit();
  if (!unit)
    return false;
  CombatMode mode = unit->GetCombatMode();
  return mode == CMStealth || mode == CMCombat || mode == CMAware;
}

bool EntityAI::IsCrewed() const
{
  return false;
}

bool EntityAI::IsCautiousOrDanger() const
{
  AIBrain *unit = PilotUnit();
  return unit && (unit->IsDanger() || IsCautious());
}

/*!
\patch 5099 Date 12/8/2006 by Jirka
- Fixed: AI are better planning the path over the bridge

*/

bool EntityAI::WantsToMoveOnRoad(int level) const
{
  /*
  similar to CheckFormationOnRoad, but can terminate earlier once level is sufficient
  */
  // find the driver we are following
  AIUnit *commander = CommanderUnit() ? CommanderUnit()->GetUnit() : NULL;
  if (!commander) return false;
  AISubgroup *subgrp = commander->GetSubgroup();
  if (!subgrp) return false;
  AIUnit *leader = subgrp->Leader();
  if (!leader) return false;
  EntityAI *lVehicle = leader->GetVehicle();
  if (!lVehicle) return false;
  AIBrain *lPilot = lVehicle->PilotUnit();
  if (lPilot && lPilot->IsOnRoad()>=level) return true;

  // check if anybody previous in on bridge
  for (int i=0; i<subgrp->NUnits(); i++)
  {
    AIUnit *u = subgrp->GetUnit(i);
    if (!u || u == commander || u == leader) continue; // already tested
    if (!u->IsUnit()) continue;
    if (u->ID() > commander->ID()) continue;
    // previous unit
    EntityAI *pVehicle = u->GetVehicle();
    if (!pVehicle) continue;
    AIBrain *pPilot = pVehicle->PilotUnit();
    if (pPilot && pPilot->IsOnRoad()>=level) return true;
  }
  return false;
}

int EntityAI::CheckFormationOnRoad() const
{
  // find the driver we are following
  AIUnit *commander = CommanderUnit() ? CommanderUnit()->GetUnit() : NULL;
  if (!commander) return 0;
  AISubgroup *subgrp = commander->GetSubgroup();
  if (!subgrp) return 0;
  AIUnit *leader = subgrp->Leader();
  if (!leader) return 0;
  EntityAI *lVehicle = leader->GetVehicle();
  if (!lVehicle) return 0;
  AIBrain *lPilot = lVehicle->PilotUnit();
  
  int onRoad = 0;
  if (lPilot)
  {
    int level = lPilot->IsOnRoad();
    if (level>onRoad)
    {
      onRoad = level;
      if (onRoad>=2)
        return onRoad;
    }
  }

  // check if anybody previous in on bridge
  for (int i=0; i<subgrp->NUnits(); i++)
  {
    AIUnit *u = subgrp->GetUnit(i);
    if (!u || u == commander || u == leader) continue; // already tested
    if (!u->IsUnit()) continue;
    if (u->ID() > commander->ID()) continue;
    // previous unit
    EntityAI *pVehicle = u->GetVehicle();
    if (!pVehicle) continue;
    AIBrain *pPilot = pVehicle->PilotUnit();
    if (!pPilot) continue;
    int level = pPilot->IsOnRoad();
    if (level>onRoad)
    {
      onRoad = level;
      if (onRoad>=2)
        return onRoad;
    }
  }
  return onRoad;
}

float EntityAI::CalculateExposure(int x, int z) const
{
  AIBrain *unit = GunnerUnit();
  if (!unit) return 0;
  AIGroup *grp = unit->GetGroup();
  if (!grp) return 0;
  AICenter *center = grp->GetCenter();
  if (!center) return 0;

  // use Threat + type

  AIThreat t = center->GetThreatOptimistic(x, z); // damage per minute - per second
  float tFactor = EXPOSURE_COEF*(1/60.0f); // damage per minute - per second;
  Threat expField(t.u.soft*tFactor,t.u.armor*tFactor,t.u.air*tFactor);

  float cost = CalculateTotalCost();
  float damage = Threat(GetType()->GetKind(),GetType()->GetInvArmor()) * expField;
  // damage higher than complete destruction is nonsense
  saturateMin(damage,1);

  return damage * cost;
}

float EntityAIFull::CalculateExposure(int x, int z) const
{
  AIBrain *unit = GunnerUnit();
  if (!unit) return 0;

  float expo = base::CalculateExposure(x,z);
  bool ableToDefend = CheckAmmoHitOver(50) && !unit->IsHoldingFire();
  // TODO: if we are already considering a full damage, it has no sense to even increase it
  if (!ableToDefend) expo *= COEF_EXPOSURE_UNABLE_TO_DEFEND / COEF_EXPOSURE;
  return expo;
}

inline float FloatSign(float x)
{
  if (x>=0) return 1;
  return -1;
}

float EntityAIFull::EstimateTravelCost(Vector3Par from, Vector3Par to, float costPerSecond) const
{
  float invLandGrid = InvLandGrid;
  float landGrid = LandGrid;

  // traversal adapted from Landscape::IntersectWithGround
  
  Vector3 dir = to-from;
  float size2 = dir.SquareSize();
  if (size2<=0)
  {
    return 0;
  }
  float invSize = InvSqrt(size2);
  float size = invSize * size2;
  Vector3 dNorm = dir*invSize;
  
  // initialize
  Vector3 pos = from;

  float deltaX = dNorm.X();
  float deltaZ = dNorm.Z();

  int xInt = toIntFloor(pos.X()*invLandGrid);
  int zInt = toIntFloor(pos.Z()*invLandGrid);

  float invDeltaX = fabs(deltaX)<1e-10 ? FloatSign(deltaX)*1e10 : 1/deltaX;
  float invDeltaZ = fabs(deltaZ)<1e-10 ? FloatSign(deltaZ)*1e10 : 1/deltaZ;
  int ddx = deltaX>=0 ? 1 : -1;
  int ddz = deltaZ>=0 ? 1 : -1;
  float dnx = deltaX>=0 ? landGrid : 0;
  float dnz = deltaZ>=0 ? landGrid : 0;

  // maintain beg, end on current square
  float tRest = size;
  float tDone = 0;
  float totalCost = 0;
  while (tRest>0)
  {
    Vector3 beg = pos;

    //advance to next relevant neighbour
    int xio = xInt, zio = zInt;
    float tx = (xInt*landGrid+dnx-pos.X()) * invDeltaX;
    float tz = (zInt*landGrid+dnz-pos.Z()) * invDeltaZ;
    //Assert( tx>=-0.01 );
    //Assert( tz>=-0.01 );
    //float tDoneBeg = tDone;
    if (tx<=tz)
    {
      saturateMin(tx,tRest);
      xInt += ddx;
      tRest -= tx;
      tDone += tx;
      pos += dNorm*tx;
    }
    else
    {
      saturateMin(tz,tRest);
      zInt += ddz;
      tRest -= tz;
      tDone += tz;
      pos += dNorm*tz;
    }

    // pos-beg segment is the line we are traversing in the xio / zio grid
    float dist = pos.Distance(beg);

    // estimate time we will spend in this grid    
    GeographyInfo geogr = GLandscape->GetGeography(xio,zio);
    float costPerM = GetBaseCost(geogr, false, true);
    // if the field is inaccessible, we still want to get some exposure estimation
    // it is likely path finder will select a different path in such case
    float maxCost = GetType()->GetMinCost()*10;
    if (costPerM>maxCost) costPerM = maxCost;
    float time = dist*costPerM;
    
    totalCost += time*costPerSecond;
    // now add cost for exposure to enemy weapons
    // 0.1 is empirical factor - we are unlikely to draw full enemy fire
    // moreover, we want to prefer short exposure over a long one
    // because this means the kill will be less likely
    float exposurePerSec = CalculateExposure(xio,zio)*0.1;
    totalCost += time*exposurePerSec;
  }

  return totalCost;
}

/**
Based on ShotShell::SimulateMovement
@return time to fly
*/

float EntityAIFull::PreviewBallistics(Vector3Par tgtPos, Vector3Par shotPos, const MagazineType *aInfo, const AmmoType *ammo, float &fall, WeaponModeType *mode, bool artillery) const
{
  Vector3 shotDir = (tgtPos - shotPos).Normalized();
  Vector3 speed = aInfo->_initSpeed * shotDir;
  float time = 0.0f;
  const float maxDeltaT = 0.2f; // experimental: 0.5 sec is too much
  float timeToLive = ammo->_timeToLive;
  Vector3 curShotPos = shotPos;

  if(artillery)
  {
    Vector3 xz = Vector3(tgtPos.X() - shotPos.X(), 0, tgtPos.Z() - shotPos.Z()); 
    float x = xz.Size();
    float y = tgtPos.Y() - shotPos.Y();
    float g = G_CONST;
    float v = aInfo->_initSpeed;
    if(mode) v *= mode->artilleryCharge;

    float square = pow(v,4.0f) - g*(g*pow(x,2.0f) + 2 * y * pow(v,2.0f));
    if(square < 0) 
      return FLT_MAX;

    square = sqrtf(square);
    float angle = max(atan((pow(v,2.0f) + square) / (g*x)),atan((pow(v,2.0f) - square) / (g*x)));
    float time = x / (v * cos(angle)); 

    fall = -y + tan(angle) * x;
    return time;
  }
  else
  {
    while(true)
    {
      float distLeft = shotDir*(tgtPos-curShotPos);
      // speed we are closing to the target
      float relSpeed = speed*shotDir;
      float traveled = relSpeed*maxDeltaT;
      // we assume at 1% of original velocity we are not interested at all
      // however, most often we will expire because of timeToLive
      if (relSpeed < aInfo->_initSpeed*0.01f || timeToLive < 0.0f)
        return FLT_MAX;
      if (distLeft < traveled)
      {
        float deltaT = distLeft / (speed * shotDir);
        speed[1] -= ammo->_coefGravity * G_CONST * deltaT;
        curShotPos += speed * deltaT;
        time += deltaT;
        // in the last iteration there is no need to update speed any more
        break;
      }
      else
      {
        float deltaT = maxDeltaT;
        curShotPos += speed * deltaT;
        time += deltaT;
        timeToLive -= deltaT;
        speed += speed * (speed.Size() * ammo->_airFriction * deltaT);
        speed[1] -= ammo->_coefGravity * G_CONST * deltaT;
      }
    }
    fall = tgtPos.Y() - curShotPos.Y();
  }
  return time;
}

/**
Based on Missile::Simulate
@return time to fly
*/

float EntityAIFull::PreviewRocketBallistics(Vector3Par tgtPos, Vector3Par shotPos, const MagazineType *aInfo, const AmmoType *ammo, float &fall, WeaponModeType *mode, bool artillery) const
{
  Vector3 shotDir = (tgtPos - shotPos).Normalized();
  // we assume rocket orientation does not change during the flight
  // this assumption is correct for unguided rockets (see usage of torque in Missile::Simulate)
  Matrix3 toWorld(MDirection,shotDir,VUp);
  Matrix3 toModel(MInverseRotation,toWorld);

  // gravity converted to model space  
  Vector3 gModel = toModel * Vector3(0.0f, -G_CONST, 0.0f);
  // we need to consider "model space" speed
  Vector3 speed = Vector3(0,0,aInfo->_initSpeed);
  float time = 0.0f;
  const float maxDeltaT = 0.2f; // experimental: 0.5 sec is too much
  float timeToLive = ammo->_timeToLive;
  Vector3 curShotPos = shotPos;

  if(artillery)
  {
    Vector3 xz = Vector3(tgtPos.X() - shotPos.X(), 0, tgtPos.Z() - shotPos.Z()); 
    float x = xz.Size();
    float y = tgtPos.Y() - shotPos.Y();
    float g = G_CONST;
    float v = aInfo->_initSpeed +  ((7.0f/8.0f) * ammo->thrustTime* ammo->thrust);
    if(mode) v*= mode->artilleryCharge;
   
    float square = pow(v,4.0f) - g*(g*pow(x,2.0f) + 2 * y * pow(v,2.0f));
    if(square < 0) 
      return FLT_MAX;

    square = sqrtf(square);
    float angle = max(atan((pow(v,2.0f) + square) / (g*x)),atan((pow(v,2.0f) - square) / (g*x)));
    float time = x / (v * cos(angle)); 
    
    fall = -y + tan(angle) * x;
    return time;
  }
  else
  {

    Missile::EngineState engine = Missile::Init;
    float initTime = ammo->initTime;
    float thrustTime = ammo->thrustTime;
    while(true)
    {
      float distLeft = shotDir*(tgtPos-curShotPos);

      // body air friction
      Vector3 accel(
        speed[0] * speed[0] * speed[0] * 5e-4f + speed[0] * fabs(speed[0]) * 10.0f + speed[0] * 10.0f,
        speed[1] * speed[1] * speed[1] * 5e-4f + speed[1] * fabs(speed[1]) * 10.0f + speed[1] * 10.0f,
        speed[2] * speed[2] * speed[2] * 1e-5f + speed[2] * fabs(speed[2]) * 0.01f + speed[2] * 2.0f
        );
      accel *= ammo->sideAirFriction * (-1.0f / 10.0f);

      float deltaT = maxDeltaT;
      switch (engine)
      {
      case Missile::Init:
        if (deltaT>=initTime)
        {
          deltaT = initTime;
          initTime = 0;
          engine = thrustTime > 0.0f ? Missile::Thrust : Missile::Fly;
        }
        else
          initTime -= deltaT;
        break;
      case Missile::Thrust:
        {
          if (deltaT>=thrustTime)
          {
            deltaT = thrustTime;
            thrustTime = 0;
            engine = Missile::Fly;
          }
          else
          {
            thrustTime -= deltaT;
            // fade in the last part (1/4) of flight
            accel[2] += ammo->thrust * floatMin(thrustTime * 4.0f / ammo->thrustTime, 1.0f);
          }
        }
        break;
      }

      // add gravity
      accel += gModel;

      // speed we are closing to the target
      float traveled = speed.Z()*deltaT;
      // prevent infinite loop: once we are not closing to the target any more, we are not interested in the result
      // however, most often we will expire because of timeToLive
      if (speed.Z() < (aInfo->_initSpeed+1)*0.01f || timeToLive < 0.0f)
        return FLT_MAX;

      if (distLeft < traveled)
      {
        deltaT = distLeft / speed.Z();
        curShotPos += toWorld*( ( speed + accel*0.5*deltaT ) * deltaT );
        time += deltaT;
        // in the last iteration there is no need to update speed any more
        break;
      }
      else
      {
        curShotPos += toWorld*( ( speed + accel*0.5*deltaT ) * deltaT );
        speed += accel * deltaT;
        time += deltaT;
        timeToLive -= deltaT;
      }
    }
    fall = tgtPos.Y() - curShotPos.Y();
    return time;
  }
}

#if _VBS3_LASE_LEAD
bool EntityAIFull::CalculateAimWeaponPos(const TurretContext &context, int weapon, Vector3 &pos, float &timeToLead, const Vector3 tgtPos) const
{
  timeToLead = 0;
  if (weapon < 0)
  {
    if (context._weapons->_magazineSlots.Size() <= 0) return false;
    weapon = 0;
  }

  const Magazine *magazine = context._weapons->_magazineSlots[weapon]._magazine;
  const MagazineType *aInfo = magazine ? magazine->_type : NULL;
  const AmmoType *ammo = aInfo ? aInfo->_ammo : NULL;
  // predict his and my movement
  Vector3 position = Position();
  if(context._turret && context._turretType)
  {
    position = PositionModelToWorld(context._turret->GetCenter(Type(), *context._turretType));
  }
  float dist=tgtPos.Distance(position);
  float time = 0;
  if (aInfo) time = dist * aInfo->_invInitSpeed;

  float fall = 0; //=0.5f*G_CONST*Square(time);
  if (ammo && ammo->_simulation!=AmmoShotMissile)
  {
    // full bullet flight simulation preview
    // approximate pos and direction
    Vector3 shotPos = position;
    Vector3 shotDir = (tgtPos-shotPos).Normalized();
    Vector3 speed = aInfo->_initSpeed*shotDir;
    time = 0;
    // check how far are we still from the target
    const float maxDeltaT = 0.05f;
    float timeToLive = ammo->_timeToLive;
    for(;;)
    {
      float distLeft = shotDir*(tgtPos-shotPos);
      // speed we are closing to the target
      float relSpeed = speed*shotDir;
      float traveled = speed*shotDir*maxDeltaT;
      // we assume at 1% of original velocity we are not interested at all
      // however, most often we will expire because of timeToLive
      if (relSpeed<aInfo->_initSpeed*0.01f || timeToLive<0)
      {
        return false;
      }
      if (distLeft<traveled)
      {
        float deltaT = distLeft/(speed*shotDir);
        shotPos += speed*deltaT;
        time += deltaT;
        // in the last iteration there is no need to update speed any more
        //speed += speed*(speed.Size()*ammo->_airFriction*deltaT);
        //speed[1] -= ammo->_coefGravity * G_CONST;
        break;
      }
      else
      {
        float deltaT = maxDeltaT;
        shotPos += speed*deltaT;
        time += deltaT;
        speed += speed*(speed.Size()*ammo->_airFriction*deltaT);
        speed[1] -= ammo->_coefGravity * G_CONST*deltaT;
      }

    }
    fall = tgtPos.Y()-shotPos.Y();
    timeToLead = time;
  }
  /*
  const float minPredTime=0.25f;
  float predTime=floatMax(time+0.1f,minPredTime);

  // note: bullet drop is affected by friction as well
  //float fall=0.5f*G_CONST*Square(time);
  // calculate ballistics

  tgtPos[1]+=fall; // consider ballistics
  if( aInfo )
  {
  Vector3 speedEst=tgtSpd-Speed();
  const float maxSpeedEst=aInfo->_maxLeadSpeed;
  if( speedEst.SquareSize()>Square(maxSpeedEst) ) speedEst=speedEst.Normalized()*maxSpeedEst;
  tgtPos+=speedEst*predTime;
  }
  */
  pos = tgtPos;
  pos[1] += fall;

  return true;
}

bool EntityAIFull::CalculateAimWeapon(const TurretContext &context, int weapon, Vector3 &dir, float &timeToLead, const Vector3 tgtPos) const
{
  Vector3 aimPos;
  bool ret = CalculateAimWeaponPos(context, weapon,aimPos,timeToLead,tgtPos);
  if (ret)
  {
    Vector3 myPos=PositionModelToWorld(GetWeaponPoint(context, weapon));
    dir = aimPos - myPos;
  }
  return ret;
}
#endif //_VBS3_LASE_LEAD

bool EntityAIFull::CalculateAimWeapon(const TurretContext &context, int weapon, Vector3 &dir, float &timeToLead, Target *target, bool exact) const
{
  Vector3 tgtPos;
  bool ret = CalculateAimWeaponPos(context, weapon, tgtPos, timeToLead, target, exact);
  if (ret)
  {
    const ProtectedVisualState<const VisualState> &vs = RenderVisualStateScope(true);
    Vector3 myPos=vs->PositionModelToWorld(GetWeaponPoint(*vs, context, weapon));
    dir = tgtPos - myPos;
  }
  return ret;
}

void EntityAI::GoToStrategic(Vector3Par pos)
{
  // if destination has changed, we cannot be in position
  if (_stratGoToPos.Distance2(pos) > 0.5f)
    _inFormation = false;
  _stratGoToPos=pos;
}

#define DIAG_SPEED 0
#define DIAG_COL 0

const float MaxBackAngle = H_PI*0.25;

#if _ENABLE_AI

Vector3 EntityAIFull::DesiredFormationPosition() const
{
  return FutureVisualState().Position();
}

Vector3 EntityAIFull::PredictFormationSpeed() const
{
  Vector3 predSpeed = PredictVelocity();
  float spSize2 = predSpeed.SquareSize();
  float assumedSpeed = GetType()->GetMaxSpeedMs()*0.5;
  if (spSize2<Square(assumedSpeed) && spSize2>Square(assumedSpeed*0.2))
  {
    predSpeed.Normalize();
    // when leader is moving, but slow, assume he is moving at least at nominal speed
    predSpeed *= assumedSpeed;
  }
  #if 0
  float assumedSpeed = floatMin(GetLimitSpeed(),GetType()->GetMaxSpeedMs()*0.5f);
  // when leader is moving too slow, assume he is moving at least at nominal speed in the formation direction
  if (spSize2<Square(assumedSpeed) && PilotUnit())
  {
    AIUnit *unit = PilotUnit()->GetUnit();
    if (unit && unit->GetSubgroup() && unit->GetCombatMode()>=CMCombat && unit->IsFreeSoldier())
    {
      Vector3Val formDir = unit->GetSubgroup()->GetMoveDirection();
      // the lower the predicted speed, the lower its importance
      Vector3 assumedDir = predSpeed+formDir*assumedSpeed;
      predSpeed = assumedDir.Normalized()*assumedSpeed;
    }
  }
  #endif
  return predSpeed;
}

/*! Common functionality for FormationPilot and LeaderPilot*/

/*!
\patch 1.63 Date 6/1/2002 by Ondra
- Fixed: AIPilot headChange was wrong if vehicle was banked (like motorcycle).
\patch 5117 Date 1/16/2007 by Bebul
- Fixed: swimming AI were unable to get in
*/

bool EntityAIFull::PathPilot(SteerInfo &info, float speedCoef)
{
  // check path position
  AIBrain *unit = PilotUnit();
  Path &path=unit->GetPath();

  // FSM needs to confirm any cover leaving/entering cover
  if (!OnPathFoundToCover())
    // if we are not cleared to use the path, do not move
    // we still may need to report the path is complete to initiate a new planning
    return path.CheckCompleted();

  Assert(path.Size()>=2);
  Vector3 steerPos=SteerPoint(GetSteerAheadSimul(),GetSteerAheadPlan(),&info.action,&info.building,true);
  Vector3Val steerPredict=SteerPoint(GetPredictTurnSimul(),GetPredictTurnPlan());
  
  // we are not interested in Y distance
  float distFromPath = DistanceXZFromPath();
  float hcOffset=0;

  float spdFactor=FutureVisualState().ModelSpeed()[2]*(1.0/15);
  saturate(spdFactor,0,1);

  Vector3 steerWant;
  if (spdFactor>0)
  {
    steerWant=PositionWorldToModel(steerPos);
    if (steerWant.Z()>0)
    {
      hcOffset=steerWant.X()*0.02;
      saturate(hcOffset,-0.25,+0.25);
    }
  }

  float cost=path.CostAtPos(FutureVisualState().Position());
  Vector3Val pos=path.PosAtCost(cost,0,FutureVisualState().Position(),cost);

  // for soldiers, use side steps instead of turning
  bool sideStep = unit->GetVehicleIn() == NULL;
  float avoidX = 0;
  if (!sideStep)
  {
    steerPos += FutureVisualState().DirectionAside() * _avoidAside;
  }
  else
  {
    Vector3Val offToPathRel = DirectionWorldToModel(FutureVisualState().Position()-pos);

    avoidX = offToPathRel.X();
    saturate(avoidX,-fabs(_avoidAside),+fabs(_avoidAside));
    if (fabs(avoidX)<0.2f) avoidX = 0;
    steerPos += FutureVisualState().DirectionAside() * avoidX;
  }

  Matrix4 vertical, invVertical;
  vertical.SetUpAndDirection(VUp,FutureVisualState().Direction());
  vertical.SetPosition(FutureVisualState().Position());
  invVertical = vertical.InverseRotation();

  steerWant = invVertical.FastTransform(steerPos);

  Vector3Val steerPredictRel = invVertical.FastTransform(steerPredict);

  if (speedCoef >= 0)
  {
    info.headChange = atan2(steerWant.X(), steerWant.Z()) + hcOffset * spdFactor;
    info.turnPredict=atan2(steerPredictRel.X(),steerPredictRel.Z());
  }
  else
  {
    info.headChange = atan2(-steerWant.X(), -steerWant.Z()) - hcOffset * spdFactor;
    info.turnPredict=atan2(-steerPredictRel.X(),-steerPredictRel.Z());
  }

  float precision=GetPrecision();

  EngineOn();
  //_moveMode=gotoNormal;

  float distPath2=FutureVisualState().Position().Distance2(pos);
  float distEnd2=FutureVisualState().Position().Distance2(path.End());

  AILockerSavedState state;
  PerformUnlock(&state);
  float speedAtCost = path.SpeedAtCost(cost,unit->IsFreeSoldier());
  PerformRelock(state);

  info.speedWanted = speedAtCost * speedCoef;


  // hotfix - if planned cost is too low, force some minimum speed
  float minSpeedNeeded = floatMax(GetType()->GetMaxSpeedMs()*0.05f,0.5f);
  if (info.speedWanted<minSpeedNeeded && info.speedWanted>0)
  {
    if (path.Size() == 2 && path[1]._cost == 0)
    {
      // do not report, degenerate (zero length) path
    }
    else
      LogF("Too slow speed in planned path (%g<%g)",info.speedWanted,minSpeedNeeded);
    info.speedWanted = minSpeedNeeded;
  }

  { // when we strayed off the path, we should slow down  
    // path is planned with OperGrid precision
    // the more we are outside of this corridor, the more careful we should be
    float safeDist,badDist;
    GetType()->GetPathPrecision(speedAtCost,safeDist,badDist);


    // when path is planned fast, there is probably less penalty for straying away from it a little bit

    // speedAtCost
    
    // if moving on road, a stray can be reasonable, as it can be caused by avoidance
    if (unit->IsOnRoad())
    {
      // TODO: straying to left (overtaking) less dangerous than straying to right (out of the road)
      float distXFromPath = DistanceXFromPath();
      // moving 3 m right and 1 m left is considered safe on the road
      if (distXFromPath<0) // negative means right
      {
        const float safeRight = 3.0f;
        if (-distFromPath>safeRight) distFromPath = -distXFromPath-safeRight;
        else distFromPath = 0;
      }
      else
      {
        const float safeLeft = 1.0f;
        if (distFromPath>safeLeft) distFromPath = distXFromPath-safeLeft;
        else distFromPath = 0;
      }

    }
    float maxPathSpeed = Interpolativ(distFromPath,safeDist,badDist,1.0f,0.05f)*GetType()->GetMaxSpeedMs();
    // nothing should be forced to move slower than 1.8 km/h
    saturateMax(maxPathSpeed,0.5f);
    
#if _ENABLE_REPORT
      if (CHECK_DIAG(DEPath) && this==GWorld->CameraOn())
        DIAG_MESSAGE(500,Format("%s: path speed %.1f, stray %.1f, avoid %.1f (for %.1fs, aside %.1f), time left %.1f of %.1f",
          cc_cast(GetDebugName()),info.speedWanted,maxPathSpeed,_avoidSpeed,_avoidSpeedTime.Diff(Glob.time),_avoidAsideWanted,path.EndCost()-cost,path.EndCost()));
#endif

    saturate(info.speedWanted,-maxPathSpeed,+maxPathSpeed);
  }


  float tholdDist2 = Square(precision);
  if (unit->GetPlanningMode() == AIBrain::LeaderDirect)
  {
    tholdDist2 = Square(precision*0.9);
    // if swimming, we should force nonzero speedWanted (if the end of path not reached)
    if (IsSwimming())
      saturateMax(info.speedWanted,1.2f);
  }
  else if (_inFormation)
    tholdDist2 = Square(precision*3);

  // note: path pilot may be used for the leader as well
  AIBrain *leader = unit->GetFormationLeader();
  if(
    // close enough
    distEnd2<tholdDist2 &&
    (
      // we are leader or leader is moving slow
      leader->GetVehicle()==this || leader->GetVehicle()->PredictVelocity().SquareSize()<Square(1) ||
      // or the path is not dependent on leader, as it is planned into a cover
      unit->GetPath().IsIntoCover()
    )
    // when the path is completed, soldier will no longer follow it. We need to report it as done
    || path.CheckCompleted()
  )
  {
    _inFormation=true;
    StopPilot(unit,info);
  }
  else
  {
    _inFormation=false;
    if (distPath2>tholdDist2 && info.speedWanted!=0)
      saturateMax(info.speedWanted,GetType()->GetMaxSpeedMs()*0.25);
  }

  if( Glob.time<_avoidSpeedTime )
    saturate(info.speedWanted,-_avoidSpeed,+_avoidSpeed);

  if (sideStep)
  {
    // try to avoid collision in 0.5 s, but do not move aside faster than forward
    info.sideSpeedWanted = (_avoidAside-avoidX)*0.5;
    // avoid using slow side speed with fast forward speed
    if (fabs(_avoidAside)<=fabs(avoidX) )
    {
      info.sideSpeedWanted = 0;
    }
    saturate(info.sideSpeedWanted, -fabs(info.speedWanted), +fabs(info.speedWanted));
  }

  return _inFormation;
}

/*!
\patch 5142 Date 3/21/2007 by Ondra
- Fixed: AI vehicle braking now smoother.
*/
float EntityAIFull::BrakeBeforeReached(float finalDist2) const
{
  float bDist=GetType()->GetBrakeDistance();
  float maxSpeed=GetType()->GetMaxSpeedMs();
  float refDist = bDist*4;
  if( finalDist2<Square(refDist) )
  {
    float limSpd = maxSpeed;
    // really close - brake hard
    const float minNearSpeed = 0.1f;

    //float minNearSpeed = finalDist2/Square(refDist);

    //float minNearSpeed = sqrt(finalDist2)/refDist;
    //Square(minNearSpeed*refDist) = finalDist2;
    if (finalDist2<Square(minNearSpeed*refDist))
      limSpd=maxSpeed*minNearSpeed;
    else
    {
      // smooth slowing down
      float dist = finalDist2*InvSqrt(finalDist2);
      float nearFactor = dist/refDist;
      // was:
      //   bDist*2 -> maxSpeed*0.3
      //   bDist*3 -> maxSpeed*0.5
      //   bDist*4 -> maxSpeed*0.5
      // now:
      //   bDist*4 -> maxSpeed*1.0
      //   bDist*3 -> maxSpeed*0.75
      //   bDist*2 -> maxSpeed*0.5
      limSpd = maxSpeed * nearFactor;
    }
    // never slow down to less than 1 m/s
    saturateMax(limSpd,1);
    return limSpd;
  }
  return FLT_MAX;
}

void EntityAIFull::StopPilot(AIBrain *unit, SteerInfo &info)
{
  info.speedWanted=0;
  info.turnPredict=0;
  info.headChange=0;
}

void EntityAIFull::LeaderPathPilot(AIBrain *unit, SteerInfo &info, float speedCoef)
{
  // check path position
  const Path &path=unit->GetPath();
  if (path.Size()>=2 || path.IsIntoCover())
  {
    bool done=PathPilot(info, speedCoef);

    //_moveMode=gotoNormal;
    float cost=path.CostAtPos(FutureVisualState().Position());
    Vector3Val pos=path.PosAtCost(cost,0,FutureVisualState().Position(),cost);

    // measure distance from corresponding surface point?
    float distPath2=(FutureVisualState().Position()-pos).SquareSizeXZ();
    float distEnd2=FutureVisualState().Position().Distance2(path.End());

    float precision=GetPrecision();
    AILockerSavedState state;
    PerformUnlock(&state);
    float speedAtCost = path.SpeedAtCost(cost,unit->IsFreeSoldier());
    PerformRelock(state);
    float safeDist,badDist;
    GetType()->GetPathPrecision(speedAtCost,safeDist,badDist);
    // using pathPrecision only could be more logical, however we want to limit the possible impact of the change
    // only precision was used before 5143
    // without the GetPathPrecision the boats were updating path too often
    float pathPrecision = floatMax(badDist,precision);

#if DIAG_SPEED
    if( this==GWorld->CameraOn() )
      LogF("SpeedAtCost %.1f",speedWanted*3.6);
#endif


    if (unit->GetPlanningMode() != AIBrain::LeaderDirect) // do not report in DirectGo mode
    {
      if (distEnd2 < Square(precision) || done)
      {
#if DIAG_WANTED_POSITION
if (GWorld->FocusOn() && GWorld->FocusOn()->GetVehicle() == this)
{
  LogF(
    "Vehicle %s: step completed - %.0f, %.0f",
    cc_cast(GetDebugName()),
    path.End().X(), path.End().Z());
}
#endif
        unit->OnStepCompleted();
      }
      else if( distPath2>Square(pathPrecision*0.8f) )
      {
        if( distPath2>Square(pathPrecision*2) || path.GetSearchTime()<Glob.time-2 && unit->GetState() != AIBrain::Planning)
        {
          // path is not recent or the error is very high
          // unit->OnStepTimedOut();
          // do not use the old path when far from it
          // note: we may be using this path because we are recovering from an impossible position
          // how can we recognize that?
          unit->UpdateOperativePlan();
        }
      }
      else
      {
        if (path.GetForceReplanCost() > 0 && cost > path.GetForceReplanCost())
          // we need to avoid further movement on this path (may lead through objects)
          unit->UpdateOperativePlan();
        else if (path.GetReplanCost() > 0 && cost > path.GetReplanCost())
          // we need to replan
          unit->OnStepTimedOut();
      }
    }
  }
  else
    StopPilot(unit,info);

  // saturate speed (may even stop us - used for danger.fsm)
  float forceSpeed=unit->GetForceSpeed();
  if (forceSpeed>=0) //use forcedSpeed only for saturation (forcing FAST speed possibly do not work in all cases)
    saturate(info.speedWanted, -forceSpeed, forceSpeed);
  
  AlignToTarget(info);

  //LogF("LeaderPilot: speedWanted1 = %.1f", info.speedWanted);

  if( GetStopped() && fabs(info.headChange)<0.05f && fabs(info.speedWanted)<0.1f) 
  {
    if (_engineOffTimeOutInProgress)
    {
      if (Glob.time > _engineOffTimeOut) 
      {
        _engineOffTimeOutInProgress = false;
        EngineOff();
      }
    }
    else
    {
      _engineOffTimeOutInProgress = true;
      _engineOffTimeOut = Glob.time + GRandGen.RandomValue()*20+20; // from 20 to 40 second
    }
  }
  else
    _engineOffTimeOutInProgress = false;

  if(
    _stratGoToPos.SquareSize()>0.1 &&
    unit->GetPlanningMode() != AIBrain::LeaderDirect &&
    StopAtStrategicPos()
  )
  {
    // in some modes we should not stop
    // we will re-plan route before we reach the position

    // strategic target known
    float finalDist2=_stratGoToPos.Distance2(FutureVisualState().Position());
    float limSpd = BrakeBeforeReached(finalDist2);
    if (limSpd<FLT_MAX)
      saturate(info.speedWanted, -limSpd,+limSpd);
  }

  // DirectGo is issued only on short distance
  // we should not move too fast
  if (unit->GetPlanningMode() == AIBrain::LeaderDirect)
  {
    float limSpd = 4; // 4m/s - i.e. 14 km/h
    saturate(info.speedWanted,-limSpd,+limSpd);
  }

  // check exposure
  AIBrain *commanderAgent = CommanderUnit();
  if (!commanderAgent)
  {
    LogF("Pilot, no commander");
    return;
  }

  AIUnit *commander = commanderAgent->GetUnit();
  if (!commander)
  {
    // independent commander
    saturateMin(info.speedWanted, _limitSpeed); // move max. by given speed
    return;
  }

  AIGroup *group = unit->GetGroup();

  // do not limit speed if we are in danger and fleeing, or when moving into a cover
  if (group && (!commander->IsFreeSoldier() || !group->GetFlee()) && !commander->GetPath().IsIntoCover())
  {
    if (unit->GetForceSpeed()>=_limitSpeed) //as it is currently saturated to +/- forcedSpeed
    { //do not allow leader to escape from his subgroup even when his forceSpeed is still high
      const float limFactor = 0.15f; //(0.22 is SpeedLimited && !CMCombat)
      if (_limitSpeed<limFactor*GetType()->GetMaxSpeedMs())
        saturateMin(info.speedWanted,_limitSpeed);
    }
    else saturateMin(info.speedWanted,_limitSpeed); // move max. by given speed

    float maxSpeed=GetType()->GetMaxSpeedMs();
    // wait for convoy successor
    AISubgroup *subgrp = unit->GetUnit()->GetSubgroup();
    AIUnit *unitFollowed = subgrp->GetFormationNext(commander);
    if (unitFollowed)
    {
      EntityAI *followed = unitFollowed->GetVehicle();
      // each of vehicles has half influence to formation distance
      float factorZ = ( GetFormationZ() +followed->GetFormationZ() )*0.5;

      float dist = followed->FutureVisualState().Position().Distance(FutureVisualState().Position());
      // wanted distance must be bigger than 1.0
      // to force followed to catch me up
      float wantedDistance = factorZ*0.6 + fabs(FutureVisualState().ModelSpeed().Z())*0.5;
      // if we are at wantedDistance from him, we want to go at his speed
      // see also FormationPilot
      float isFar=(dist-wantedDistance*2)*(0.1f/factorZ);
      saturate(isFar,-1,1);
      // if we are further, we want to go slower (positive isFar)
      // if we are nearer, we want to go faster (negative isFar)
      float waitSpeed=-GetType()->GetMaxSpeedMs()*isFar;
#if _ENABLE_CHEATS
      if (CHECK_DIAG(DEConvoy))
      {
        DIAG_MESSAGE_ID(
          1000,unit->GetUnit()->ID(),"%d: Wait for %s, dist %.1f m (%.1f m), rel. speed %.1f m/s",
          unit->GetUnit()->ID(),cc_cast(unitFollowed->GetDebugName()),dist,wantedDistance*2,waitSpeed
        );
      }
#endif
      Vector3 followedRelSpeed=DirectionWorldToModel(followed->PredictVelocity());
      // his speed relative to me (positiove - he's going away)
      float followedSpeedZ=followedRelSpeed.Z();
      // wanted speed is given by his speed (followedSpeedZ)
      // and by waitSpeed (my wanted speed relative to him)
      saturateMin(info.speedWanted, floatMax(followedSpeedZ + waitSpeed, maxSpeed*0.1));
    }
  }
  //LogF("  LeaderPilot: speedWanted2 = %.1f", info.speedWanted);
}


bool EntityAIFull::IsStandingStill() const
{
  float maxSpeed = GetType()->GetMaxSpeedMs();
  // when moving very slow, assume we are stopped, if we did not detect a significant movement within two seconds
  return FutureVisualState()._speed.SquareSize()<maxSpeed*0.1f && _lastMoved<Glob.time-2.0f;
}

void EntityAIFull::LeaderPilot(SteerInfo &info)
{ // subgroup leader -
  // if we are near the target we have to operate more precisely
  PROFILE_SCOPE(entLP);
  if (_userStopped)
  {
    info.speedWanted=0;
    info.headChange=0;
    info.turnPredict=0;
    return;
  }
    
  info.speedWanted=0; // go faster
  AIBrain *unit = PilotUnit();
  
  if (unit->GetAIDisabled() & (AIUnit::DAMove | AIUnit::DATeamSwitch)) return;

#if DIAG_SPEED
  if( this==GWorld->CameraOn() )
    LogF("Basic speed %.1f",speedWanted*3.6);
#endif

  // check if hiding (similar to keeping formation)
  LeaderPathPilot(unit, info);
}

#endif //_ENABLE_AI

float EntityAI::PilotSpeed() const
{
  float speedWanted=0; // go faster 
  // check path position
  const Path &path=PilotUnit()->GetPath();
  if (path.Size()>=2)
  {
    float cost=path.CostAtPos(FutureVisualState().Position());
    AILockerSavedState state;
    PerformUnlock(&state);
    speedWanted=path.SpeedAtCost(cost,PilotUnit()->IsFreeSoldier());
    PerformRelock(state);
  }
  return speedWanted;
}

#if _ENABLE_AI

void SpeedEstimation::Update(Vector3Par pos)
{
  const float lastSpeedInterval = 0.5f;
  dword tmstm = GetTickCount();
  float deltaT = (tmstm-timeStamp)/1000.0f;
  float dist = lastPos.Distance(pos);
  speed = (speed*lastSpeedInterval + dist)/(lastSpeedInterval+deltaT);
  lastPos=pos;
  timeStamp = tmstm;
}

 // ignore vehicles - we need permanent information
 struct FilterStaticOnly
 {
   bool operator()(const Object *obj) const {return obj->Static();};
 };

void EntityAIFull::TraceFireSector(TurretContextEx &context)
{
  PROFILE_SCOPE_EX(enTFS, sim);

  // if we have recently fired, do not update
  if (context._weapons->_lastShotTime>Glob.time-1) return;
  // assume we would be firing with the currently selected weapon
  
#ifdef _XBOX
  if (context._weapons->_lastTrace>Glob.time-0.6f) return;
#else
  if (context._weapons->_lastTrace>Glob.time-0.2f) return;
#endif
  
  // if we are moving fast, rasterizing anything has little sense - it would become obsolete soon anyway
  if (FutureVisualState()._speed.SquareSize()>Square(10)) return;
  
  context._weapons->_lastTrace = Glob.time;
  
  int weapon = context._weapons->_currentWeapon;
  if (weapon<0) weapon = 0;
  weapon = context._weapons->ValidateWeapon(weapon);
  if (weapon<0) return;
  
  Vector3 pos = FutureVisualState().PositionModelToWorld(GetWeaponPoint(FutureVisualState(),context,weapon));
  // we use GetWeaponDirectionWanted rather than GetWeaponDirection
  // with Wanted we get:
  // - in Aware we still get direction where the soldier would be aiming
  // - when weapon is moving in animations we still get the intended weapon direction
  Vector3 dir = GetWeaponDirectionWanted(context,weapon);
  
  const MagazineSlot &slot = context._weapons->_magazineSlots[weapon];
  const Magazine *mag = slot._magazine;
  if (!mag || !mag->_type) return;
  
  const AmmoType *ammo = mag->_type->_ammo;
  if (!ammo) return;
  
  //  never rasterize beyond weapon range
  float dist = 150.0f;
  // both AI and player can switch weapon quickly.
  if (context._turretType)
  {
    // TODO: consider checking the range for the same ammo only
    saturateMin(dist, context._turretType->_weapons.GetMaxRange());
  }

  ADD_COUNTER(tfsRa, 1);

  // randomly change firing angle
  const float period = 10.0f;
  const float angle = H_PI*0.167f;
  
  // by applying a reversed bit pattern we perform big steps first, small later
  // with 10 seconds period and step 0.2 we have 50 steps
  // reversing 8 bits should be quite enough
  const int bits = 8;
  const int maxVal = (1<<bits)-1;
  int gradual = toInt(Glob.time.Mod(period)*(maxVal/period));
  int swizzled = 0;
  for (int i=0; i<bits; i++)
    swizzled |= ((gradual>>i)&1)<<(bits-1-i);
  float angleMod = swizzled*(1.0f/maxVal)*2*angle-angle;
  // randomize between steps to get better coverage
  angleMod += GRandGen.RandomValue()*angle-angle*0.5f;
  Matrix3 mod(MRotationY,angleMod);
  
  // sweep left/right with the tracing
  dir = mod*dir;
  
  { // always trace assuming the unit is not aiming into the ground, as that would lead to a short trace
    const float traceRange = GRandGen.PlusMinus(80.0,30.0);
    float curY = GLandscape->SurfaceYAboveWaterNoWaves(pos.X(),pos.Z());
    float rangeY = GLandscape->SurfaceYAboveWaterNoWaves(pos.X()+dir.X()*traceRange,pos.Z()+dir.Z()*traceRange);
    float minElevation = (rangeY-curY)*(1.0f/traceRange);
    if (dir[1]<minElevation)
    {
      dir[1] = minElevation;
    }
    // avoid firing too much above the ground - this helps when covering downhill locations
    dir[1] = Lerp(dir[1],minElevation,GRandGen.RandomValue());
    dir.Normalize();
  }
  
  // compute intersection of a predicted bullet
  float tRet = GLandscape->IntersectWithGroundOrSea(NULL,pos,dir,0,dist);
  if (tRet>dist) tRet = dist;
  if (tRet>OperItemGrid)
  {
    CollisionBuffer col;
    Vector3 end = pos+ dir*tRet;
    float dist = tRet;

    GLandscape->ObjectCollisionLine(GetFutureVisualStateAge(),col,Landscape::FilterCombine(Landscape::FilterIgnoreOne(this),FilterStaticOnly()),pos,end,0);
    if (col.Size()>0)
    {
      for (int i=0; i<col.Size(); i++)
      {
        const CollisionInfo &ci = col[i];
        #if _DEBUG
          Object *obj = ci.hierLevel>0 ? ci.parentObject : ci.object;
          Assert(obj->Static()); // vehicles should already be handled by a filter
        #endif
        if (tRet>ci.under*dist) tRet = ci.under*dist;
      }
      end = pos+ dir*tRet;
    }
    if (pos.Distance2(end)>Square(OperItemGrid))
      // when rasterizing fire sectors we use somewhat lower costs
      GLandscape->SuppressCache()->Trace(this,ammo->hit*0.3f,dist,pos,end);
  }
}

/**
@param preferTarget return direction to a weapon target even when we are only "preparing" (watching) it
@return world space direction to the watched target (returned as offset - not normalized)
*/

Vector3 EntityAIFull::GetTargetAimDir( Vector3Par dir, bool preferTarget ) const
{
  AIBrain *commander = CommanderUnit();

  // pilot checks only the main weapon system
  TurretContext context;
  bool valid = GetPrimaryGunnerTurret(context);

  float dirSize2 = dir.SquareSize();
  if (valid && context._weapons->_fire._fireTarget && (!context._weapons->_fire._firePrepareOnly || preferTarget))
  {
    // fall-through to the target handling
  }
  else if (commander && commander->GetWatchMode() != AIUnit::WMNo && dirSize2>=0.1f)
    // watch is commanded - look in the given direction
    return dir;
  else if (valid && context._weapons->_fire._fireTarget)
  {
    // fall-through to the target handling
  }
  else if (dirSize2>0)
    // formation watch - if no interesting target
    return dir;
  else
    return VZero;
  // this location get reached only via fall-though from the if chain above
  // we need to fire - this forces aiming at the target
  // when we are ordered to suppress, we follow the last position
  Vector3 tgtPos;
  if (CheckSuppressiveFire()<SuppressYes)
    tgtPos = context._weapons->_fire._fireTarget->LandAimingPosition(commander);
  else
    tgtPos = context._weapons->_fire._fireTarget->LastKnownPosition(commander);
  return tgtPos-FutureVisualState().Position();
}


bool EntityAIFull::UseBackwardMovement(Vector3Par toMove, EntityAI *leaderVehicle) const
{
  return FutureVisualState().Direction() * leaderVehicle->FutureVisualState().Direction() > 0;
}
/**
When this function is called with useWantedPosition = false, it needs to be called with true 2nd time.

@param useWantedPosition when false, the purpose of this function is to maintain _wantedPosition only
*/

void EntityAIFull::PositionPilot(SteerInfo &info,
  Vector3Par pos, Vector3Par predSpeed, Vector3Par dir, float precision,
  bool useWantedPosition, EntityAI *leaderVehicle)
{
  AIBrain *unit = PilotUnit();

  // control vehicle to get to given position
  // default: no speed limit
  _limitSpeed=GetType()->GetMaxSpeedMs()*1.5;

  saturateMin(precision,floatMax(0.1,GetPrecision()));
  float limit=precision;
  if( !_inFormation ) limit*=0.75;
  else limit*=1.5;

  // check where the unit is currently going
  // for formation movement, give a chance to FSM to modify the destination
  PlanPosition setPos;
  if (useWantedPosition)
    setPos = unit->GetWantedPosition();
  else
    setPos = unit->GetWantedDestination();
  if (
    // path is planned to a different place 
    setPos.Distance2(pos) > Square(limit) ||
    // or both simple and searched solution is too old
    !IsSimplePathValid(Glob.time) &&
    Glob.time - MaxSearchedPathAge > unit->GetPath().GetSearchTime() && unit->GetState() != AIBrain::Planning)
  {
    setPos = PlanPosition(pos,setPos._cover);
    // because setPos == pos, following two lines do nothing at all
    //float height = pos[1] - GLandscape->SurfaceYAboveWater(pos[0], pos[2]);
    //setPos[1] = GLandscape->SurfaceYAboveWater(setPos[0], setPos[2]) + height;

      // leave the current planning mode
      if (useWantedPosition)
        unit->SetWantedPosition(setPos, 0, unit->GetPlanningMode(), false);
      else
        unit->SetWantedDestination(setPos, unit->GetPlanningMode(), false);
    }

  // when useWantedPosition is false, there is no need to update info or any other variables
  // it will be called 2nd time with true anyway
  if (useWantedPosition)
  {
    Vector3 wantedPos = unit->GetWantedDestination();

    _wantedPosSpeed.Update(wantedPos);

    //wantedPos += predSpeed*GetFormationTime();
    float dist2 = wantedPos.Distance2(FutureVisualState().Position());
    if(
      dist2<Square(limit) &&
      // we cannot complete a path which is into a cover and is not completed yet
      (unit->GetPath().Size()<2 || unit->GetPath().CheckCompleted() || !unit->GetPath().IsIntoCover())
    )
    {
      // in position - turn to direction
      _inFormation=true;

      StopPilot(unit,info);

      if (GetStopped() && fabs(info.headChange) < 0.05f) EngineOff();
    }
    else
    {
      // out of position
      _inFormation=false;

      // check if trivial solution is good enough
      bool simple=false;
      if (!IsSimplePathValid(Glob.time) && unit->IsSimplePathReady(FutureVisualState().Position(), wantedPos))
      {
        simple=unit->IsSimplePath(FutureVisualState().Position(), wantedPos);
        if( simple )
        {
          _lastSimplePath=Glob.time;
          _simplePathFrom = FutureVisualState().Position();
          _simplePathDst = wantedPos;
        }
#if _ENABLE_CHEATS
        if (CHECK_DIAG(DEPath))
        {
          Ref<Object> obj=new ObjectColored(GScene->Preloaded(SphereModel),VISITOR_NO_ID);
          obj->SetPosition(wantedPos);
          obj->SetScale(0.1f);
          obj->SetConstantColor(PackedColor(simple ? Color(0,1,0) : Color(1,0,0)));
          GScene->ShowObject(obj);
        }
#endif
      }
      else
      {
        simple=true;
#if _ENABLE_CHEATS
        if (CHECK_DIAG(DEPath))
        {
          Ref<Object> obj=new ObjectColored(GScene->Preloaded(SphereModel),VISITOR_NO_ID);
          obj->SetPosition(_simplePathDst);
          obj->SetScale(0.1f);
          float age = (Glob.time - _lastSimplePath) / MaxSimplePathAge;
          saturate(age, 0, 1);
          obj->SetConstantColor(PackedColor(Color(age,1,0)));
          GScene->ShowObject(obj);
        }
#endif
      }
      if( simple )
      {
        // trivial solution works
        Vector3 moveDir;
        float forcedSpeed = unit->GetForceSpeed();
        if ( forcedSpeed >=0 )
        { 
          moveDir=PositionWorldToModel(_simplePathDst);
          info.headChange=atan2(moveDir.X(),moveDir.Z());
          info.speedWanted = forcedSpeed; //floatMin(forcedSpeed, maxSpeed);
  #define DIAG_DEST_SPEED 0
  #if DIAG_DEST_SPEED
          char speedMsg[1024]; sprintf(speedMsg, "DestinationSpeed=%.1f", _wantedPosSpeed.Speed());
          DIAG_MESSAGE(200,speedMsg);
  #endif
          //we must be careful and not force the high speed when the position is too near
          //we use wantedPos "speed" as the minimal braking speed
          float limSpd = BrakeBeforeReached(dist2);
          if (limSpd<FLT_MAX)
          {
            static float speedFactor=1.3f; //speedEstimation is not precise enough. This forces follower to reach the position
            limSpd += _wantedPosSpeed.Speed()*speedFactor;
            float maxSpeed=GetType()->GetMaxSpeedMs();
            saturateMin(limSpd, maxSpeed);
            saturate(info.speedWanted,-limSpd,+limSpd);
          }
        }
        else
        {
          static float c1=5.0f;
          static float c2=1.0f;
          Vector3 moveWDir = wantedPos - FutureVisualState().Position();
          if (CHECK_DIAG(DEPath) && GWorld->CameraOn()==this)
          {
            DIAG_MESSAGE(300,Format(
              "predSpd=[%.1f,%.1f] moveWDir=[%.1f,%.1f] wantedPos=[%.1f,%.1f]", 
              predSpeed.X(),predSpeed.Z(),moveWDir.X(),moveWDir.Z(),wantedPos.X(),wantedPos.Z()
            ));
          }
          Vector3 speedToDestination = c1*moveWDir*GetInvFormationTime() + c2*predSpeed;
          Vector3 expPos = FutureVisualState().Position() + speedToDestination;
          moveDir=PositionWorldToModel(expPos);
          info.headChange=atan2(moveDir.X(),moveDir.Z());
          info.speedWanted = speedToDestination.Size();
          saturateMax(info.speedWanted,1.5);
        }
        if (fabs(AngleDifference(info.headChange,H_PI)) < MaxBackAngle)
        {
          bool goBack = false;
          if (leaderVehicle)
          {
            // when we are moving in formation, go back if leader is going back
            goBack = UseBackwardMovement(moveDir,leaderVehicle);
            // special case: some vehicles are moving back quite slowly
            // they may prefer turning around
          }
          else
          {
            // when no leader vehicle is given, decide by the distance we need to move
            goBack = moveDir.SquareSize() < Square(GetType()->GetMaxSpeedMs() * 3);
          }
          if (goBack)
          {
            // go back
            info.headChange=AngleDifference(info.headChange,H_PI);
            info.speedWanted=-info.speedWanted;
          }
        }
        // we will follow the simple path, so do not plan and empty current plan
        unit->SetPlanningMode(AIUnit::DoNotPlanFormation, true);
      }
      else
      {
        // non-trivial solution needed
        Path &path=unit->GetPath();
        bool update=false;

        if (!unit->IsPlanning())
        {
          bool pathInvalid = path.Size()<2 || path.End().DistanceXZ2(wantedPos)>Square(limit);

          // avoid searching too often
          if( path.Size()<2 )
          { // no known solution
            if (Glob.time-MaxSearchedPathAge>path.GetSearchTime() && unit->GetState() != AIBrain::Planning)
              update = true;
          }
          else if (pathInvalid && Glob.time-path.GetSearchTime()>5 && unit->GetState() != AIBrain::Planning)
            // solution is invalid and too old
            update = true;
          else if (Glob.time-path.GetSearchTime()>15 && unit->GetState() != AIBrain::Planning)
            // solution is valid, but too old
            update = true;
        }
        unit->SetPlanningMode(AIUnit::FormationPlanned, update);

        if( path.Size()>=2 )
        {
          bool done = PathPilot(info);
          // LeaderDirect impossible - see SetPlanningMode above
          Assert(unit->GetPlanningMode() != AIBrain::LeaderDirect);
          if (done)
            // OnStepCompleted needed to complete partial (alternate goal) plans
            unit->OnStepCompleted();
          // note: pathpilot does AvoidCollision
        }
        else info.speedWanted=0,info.headChange=0;
        // plan is used by default handler (steer pos)
      }
    }
    info.turnPredict=info.headChange;
  }
}

#endif //_ENABLE_AI

int EntityAI::IsOnRoad() const
{
  AIBrain *unit = PilotUnit();
  if( !unit ) return false;
  return unit->IsOnRoad();
}

bool EntityAI::IsOnRoadMoving( float minSpeed ) const
{
  if( FutureVisualState().Speed().SquareSize()<Square(minSpeed) ) return false;
  return IsOnRoad()>0;
}

void EntityAI::CheckAway()
{
  AIUnit *unit = CommanderUnit() ? CommanderUnit()->GetUnit() : NULL;
  if( !unit ) return;
  bool away = IsAway();
  if (away)
    unit->SetAway();
}

bool EntityAI::IsAway(float factor)
{
  AIUnit *unit = CommanderUnit() ? CommanderUnit()->GetUnit() : NULL;
  if( !unit ) return false;
  AISubgroup *subgrp = unit->GetSubgroup();
  // avoid CTD that occurs when gunner is shot and driver is a player
  if (!subgrp) return false;

  float lostUnit = 12.0 * floatMax(GetFormationX(),GetFormationZ());
  saturate(lostUnit, LostUnitMin, LostUnitMax);
  lostUnit *= factor;

  if (!unit->IsSubgroupLeader())
  {
    // check if we keep in formation
    if (IsMovingInConvoy())
    {
      EntityAI *follow=subgrp->GetFormationPrevious(unit)->GetVehicle();
      // hotfix: crashing when no subgroup leader present
      if (!follow) return false;
      // each of vehicles has half influence to formation distance
      float factorZ = ( GetFormationZ() +follow->GetFormationZ() )*0.5;
      Vector3 relPos(0, 0, -0.4 * factorZ);

      if ((follow->FutureVisualState().Position()-FutureVisualState().Position()).SquareSizeXZ() > Square(lostUnit))
        return true;
    }
    else
    {
      Vector3 formPos = unit->GetFormationAbsolute();
      if (formPos.Distance2(FutureVisualState().Position())>Square(lostUnit))
        return true;
    }
  }
  return false;
}

#if _ENABLE_AI

void EntityAIFull::AlignToTarget( SteerInfo &info ) const
{
#if _ENABLE_CHEATS
  if (GWorld->CameraOn()==this)
    __asm nop;
#endif
  // when not moving, we may rotate to aim at the target
  if (fabs(info.speedWanted)<0.1f && fabs(info.sideSpeedWanted)<0.1f && info.allowFreeTurn)
  {
    AIBrain *commander = CommanderUnit();
    if (commander)
    {
      Vector3Val watchDir = commander->GetWatchDirection();
      Vector3Val tgtDir = GetTargetAimDir(watchDir);
      if (tgtDir.SquareSize()>0.1f)
      {
        Vector3Val tgtDirRel = DirectionWorldToModel(tgtDir);
        info.headChange = atan2(tgtDirRel.X(),tgtDirRel.Z());
        // check if we could aim primary weapon there
        // if we could, no need to turn body
        TurretContext context;
        if (GetPrimaryGunnerTurret(context))
        {
          // check what are the aiming limits of the weapon
          float minAngle,maxAngle;
          FireAimingLimits(minAngle,maxAngle,context,context._weapons->_currentWeapon);

          if (info.headChange<minAngle || info.headChange>maxAngle)
          {
            // TODO: we still may want to turn not a lot more than needed
          }
          else
            info.headChange = 0;
        }
      }
    }
  }
}

#if _PROFILE
#pragma optimize("",off)
#endif

/*!
\patch 2.01 Date 1/8/2003 by Ondra
- Improved: AI can better predict leader movement when leader is lying down.
\patch 5143 Date 3/21/2007 by Jirka
- Fixed: Better movement of convoys on roads
*/

bool EntityAIFull::FormationPilot(SteerInfo &info)
{
  PROFILE_SCOPE(entFP);
  if (_userStopped)
  {
    info.speedWanted=0;
    info.headChange=0;
    info.turnPredict=0;
    return true;
  }

  AIBrain *agent = PilotUnit();
  if (!agent) return true;
  AIUnit *unit = agent->GetUnit();
  if (!unit) return true; // standalone agents cannot move in formation
  AIUnit *commander = CommanderUnit() ? CommanderUnit()->GetUnit() : NULL;
  if( !commander ) return true;
  if (unit->GetAIDisabled() & (AIUnit::DAMove | AIUnit::DATeamSwitch)) return true;

  AISubgroup *subgrp=commander->GetSubgroup();
  Assert(subgrp);
  if (!subgrp) return true;
  
  AIGroup *grp=commander->GetGroup();
  Assert(grp);
  if (!grp) return true;
  
  AIUnit *leader=subgrp->Leader();
  Assert(leader);
  if (!leader) return true;

  // check if leader is on road
  int onRoad = 0;
  if (IsMovingInConvoy())
  {
    // use follow mode - ignore formation

    // special case - the leader or the previous unit is on road
    // follow previous vehicle in subgroup
    EntityAIFull *follow=subgrp->GetFormationPrevious(commander)->GetVehicle();
    // check highest onRoad level for all vehicles we follow
    onRoad = CheckFormationOnRoad();

#if _ENABLE_CHEATS
    // diagnostic arrows changing color by the movement mode
    bool showDiags = CHECK_DIAG(DEConvoy) && this == GWorld->GetObserver();
    PackedColor onRoadColor = PackedColor(Color(1, 0, 0, 1));
    if (showDiags)
    {
      switch (onRoad)
      {
      case 0:
        // free landscape
        onRoadColor = PackedColor(Color(1, 1, 0, 1));
        break;
      case 1:
        // road
        onRoadColor = PackedColor(Color(0, 0, 0, 1));
        break;
      case 2:
        // bridge
        onRoadColor = PackedColor(Color(0, 0, 1, 1));
        break;
      }
      // chain of vehicles prior me
      int i = 0;
      AIUnit *u = commander;
      while (u)
      {
        static int handle;
        GDiagsShown.ShowArrow(handle, u->ID(), 0.5, Format("%d", i), u->Position(u->GetFutureVisualState()), VUp, 3, onRoadColor, 5);
        i--;
        AIUnit *old = u;
        u = subgrp->GetFormationPrevious(old);
        if (u == old) break;
      }
      // chain of vehicles after me
      i = 1;
      u = subgrp->GetFormationNext(commander);
      while (u)
      {
        static int handle;
        GDiagsShown.ShowArrow(handle, u->ID(), 0.5, Format("+%d", i), u->Position(u->GetFutureVisualState()), VUp, 3, onRoadColor, 5);
        i++;
        AIUnit *old = u;
        u = subgrp->GetFormationNext(u);
        if (u == old) break;
      }
    }
#endif

    // each of vehicles has half influence to formation distance
    float factorZ = ( GetFormationZ() +follow->GetFormationZ() )*0.5;
    float leaderSpeed = follow->PredictVelocity().Size();
    // on the road we want to plan the path as closely as we can
    float speedOffset = onRoad ? 0 : leaderSpeed*GetFormationTime()*0.6f;
    
    if (follow->IsLockedPosition() || leaderSpeed<Square(3))
    { // when locked or lock is possible (moving slow), make sure the point we choose is not in the center of the lock
      saturateMax(speedOffset,CollisionSize()*1.5f+2.5f);
    }
    
    Vector3 followPos = follow->GetHistoryPosition(speedOffset);

    // wait with movement until the position we are following is reasonable
    // it is reasonable when:
    // 1) is is in front of us (easy to get to) and it is clear it brings us in the direction the followed unit will move in
    // 2) or it is far enough it is clear we will have to follow it anyway
    bool wait = false; // we do not want to start moving
    bool stop = false; // we want to stop even when we are already moving
    if (followPos.DistanceXZ2(FutureVisualState().Position())<Square(factorZ*2.5f)) // factorZ is 20m for a typical car
    {
      
      Vector3 followRel = PositionWorldToModel(followPos);
      if (FutureVisualState().Speed().SquareSize()<Square(2) || leaderSpeed<Square(1))
      {
        // when both leader and we are already moving, only a severe complications will cause us to stop planning
        if (followRel.Z()<0)
        {
          wait = true;
          if (followRel.Z()<-CollisionSize())
            stop = true;
        }
      }
      else
      {
        if (fabs(followRel.X())>=followRel.Z() || followRel.Z()<CollisionSize())
        {
          wait = stop = true;
        }
      }
    }
    
#if _ENABLE_CHEATS
    RString onRoadText;
    if (showDiags)
    {
      static int handle;
      GDiagsShown.ShowArrow(handle, ID().Encode(), 0.5, "Raw follow pos.", followPos + 2.0 * VUp, -VUp, 1.5, PackedWhite, 2);
      onRoadText = "Follow pos.";
    }
#endif

    bool forceReplan = false;

    Path &path=unit->GetPath();
    const float roadPlanInterval = 0.5f;
 
    // FIX: check if the position of the land contact is on the road (simple Position() sometimes did not pass y test)
    Vector3 lcPosition;
    // Get the land contact level (or geometry level as a fallback)
    const Shape *lcLevel = GetShape()->LandContactLevel();
    if (!lcLevel) lcLevel = GetShape()->GeometryLevel();
    if (lcLevel) lcPosition = FastTransform(Vector3(0, lcLevel->Min().Y(), 0));
    else lcPosition = FutureVisualState().Position();

    // make sure path finding target is on road when moving on road
    // this is extremely important for bridges, but can help on all roads
    //if (onRoad>=2 || onRoad>0 && unit->GetVehicleIn() && GLandscape->GetRoadNet()->IsOnRoad(lcPosition, CollisionSize(), CollisionSize()) != NULL)
    if (onRoad>0)
    {
      // force planning to the road point to be sure plan will follow the road
      float minDist = floatMax(leaderSpeed * roadPlanInterval, OperItemGrid * 4);
      float vehSize = GetShape() ? GetShape()->GeometrySphere() : 0;
      bool soldier = unit->IsFreeSoldier();
      float vehWidth = 0;
      if (!soldier)
      {
        LODShape *shape = GetShape();
        if (shape) vehWidth = shape->Max().X() - shape->Min().X();
      }
      followPos = GLandscape->GetRoadNet()->GetNearestRoadPoint(lcPosition, followPos, minDist, vehSize, soldier, vehWidth);
#if _ENABLE_CHEATS
      if (showDiags)
        onRoadText = "Follow road pos.";
#endif

      // leader is moving on road and I'm on road as well
      // road path refresh should be quite cheap
      if (path.Size()>=2 && path.CostAtPos(FutureVisualState().Position()+FutureVisualState().Speed())>=path.EndCost() ||
        Glob.time-path.GetSearchTime()>roadPlanInterval && unit->GetState() != AIBrain::Planning)
        forceReplan = true;
    }
    else
    {
      float precision=GetPrecision();
      if (path.Size()>=2 && path.End().Distance2(FutureVisualState().Position())<Square(precision) &&
        Glob.time-path.GetSearchTime()>roadPlanInterval && unit->GetState() != AIBrain::Planning)
      {
        // end of path reached - replan
        if (unit->NearestEmptyReady(followPos))
        {
          unit->FindNearestEmpty(followPos);
          forceReplan = true;
#if _ENABLE_CHEATS
          if (showDiags)
            onRoadText = "Follow free pos.";
#endif
        }
      }
      if (Glob.time-path.GetSearchTime()>2 && unit->GetState() != AIBrain::Planning)
      {
        // path is old - consider replan
        if( path.Size()<2 || path.End().Distance2(followPos)>Square(precision) )
        {
          if (unit->NearestEmptyReady(followPos))
          {
            unit->FindNearestEmpty(followPos);
            // no path or path invalid
            forceReplan = true;
#if _ENABLE_CHEATS
            if (showDiags)
              onRoadText = "Follow free pos.";
#endif
          }
        }
      }
    }

    if (!wait)
    {
      // ask unit to create path
      unit->SetWantedPosition(PlanPosition(followPos,false), 0, AIUnit::FormationPlanned, forceReplan);
    }
    else if (stop)
    {
      // clear the path if we have any - we want to wait
      unit->SetPlanningMode(AIUnit::DoNotPlanFormation, true);
    }

#if _ENABLE_CHEATS
    if (showDiags)
    {
      static int handle;
      PackedColor color = PackedColor(forceReplan ? Color(1, 0, 0, 1) : Color(0, 1, 0, 1));
      GDiagsShown.ShowArrow(handle, ID().Encode(), 0.5, onRoadText, followPos + 2.0 * VUp, -VUp, 2, color, 2);

      static int handle2;
      GDiagsShown.ShowArrow(handle2, ID().Encode(), 0.5, "Destination", unit->GetWantedDestination() + VUp, VUp, 2.5, PackedColor(Color(0, 1, 1, 1)), 2);
    }
#endif

    if( path.Size()>=2 )
    {
      bool done = PathPilot(info);
      // note: path pilot does AvoidCollision
      // LeaderDirect impossible - see SetPlanningMode above
      Assert(unit->GetPlanningMode() != AIBrain::LeaderDirect);
      if (done)
        // OnStepCompleted needed to complete partial (alternate goal) plans
        unit->OnStepCompleted();

      float forceSpeed=unit->GetForceSpeed();
      if (forceSpeed>=0) 
      {
        //float oldSpeed = info.speedWanted;
        saturate(info.speedWanted, -forceSpeed, forceSpeed);
        //LogF("FormationPilot1(%s): speedWanted %.1f->%.1f", cc_cast((GetEnumNames(CMCombat))[unit->GetCombatMode()].GetName()), oldSpeed, info.speedWanted);
        return true;
      }

      // dynamic stability
      float limitSpeed;
      float maxSpeed=GetType()->GetMaxSpeedMs();
      {
        // do not try to overtake the vehicle you should follow
        Vector3 followPosDiff = follow->FutureVisualState().Position() - FutureVisualState().Position();
        float dist = followPosDiff.Size();
        // keep safe distance based on his speed
        float wantedDistance = factorZ*0.6 + fabs(follow->FutureVisualState().ModelSpeed().Z())*0.5;
        // if we are at wantedDistance from him, we want to go at his speed
        float isFar = (dist-wantedDistance)*(0.4/factorZ);
        saturate(isFar,-0.3,1);
        // if we are further, we want to go faster (positive isFar)
        // if we are nearer, we want to go slower (negative isFar)
        float catchUpSpeed=maxSpeed*isFar;
        
#if _ENABLE_CHEATS
        if (CHECK_DIAG(DEConvoy) && unit->GetUnit())
        {
          DIAG_MESSAGE_ID(
            1000,unit->GetUnit()->ID()+10000,"%d:Hunt for %s, dist %.1f m (%.1f m), rel. speed %.1f m/s",
            unit->GetUnit()->ID(),cc_cast(follow->GetDebugName()),dist,wantedDistance,catchUpSpeed
          );
        }
#endif
        // velocity will cause a real change based on relative position
        // if it is head to head, its effect will be full
        // if the vehicle is aside of us, its velocity does not matter at all
        // (positive means he is going away)
        float followSpeedZ = followPosDiff.Normalized()*follow->PredictVelocity();

        // wanted speed is given by his speed (followSpeedZ)
        // and by catchUpSpeed (my wanted speed relative to him)
        limitSpeed=floatMax(followSpeedZ+catchUpSpeed,maxSpeed*0.1);
        // speedWanted is currently max speed given by path planning
      }

      AIUnit *unitFollowed = subgrp->GetFormationNext(commander);
      if (unitFollowed)
      {
        EntityAI *followed = unitFollowed->GetVehicle();
        // each of vehicles has half influence to formation distance
        float factorZ = ( GetFormationZ() +followed->GetFormationZ() )*0.5;

        Vector3 followedPosDiff = followed->FutureVisualState().Position() - FutureVisualState().Position();
        float dist = followedPosDiff.Size();
        // wanted distance must be bigger than 1.0
        // to force followed to catch me up
        float wantedDistance = factorZ*0.6 + fabs(FutureVisualState().ModelSpeed().Z())*0.5;
        // if we are significantly further than wantedDistance from him, we want to go at his speed
        // we need to use distance higher than wantedDistance here, as otherwise there will be a "decision loop"
        // he will be limiting his speed to our speed, and the same will be done by us
        // once he is behind us a lot we know he should be catching us and he cannot be moving any faster
        float isFar=(dist-wantedDistance*2.0f)*(0.1/factorZ);
        saturate(isFar,-1,1);
        // if we are further, we want to go slower (positive isFar)
        // if we are nearer, we want to go faster (negative isFar)
        float waitSpeed = -maxSpeed*isFar;

#if _ENABLE_CHEATS
        if (CHECK_DIAG(DEConvoy))
        {
          DIAG_MESSAGE_ID(
            1000,unit->ID(),"%d: Wait for %s, dist %.1f m (%.1f m), rel. speed %.1f m/s",
            unit->ID(),cc_cast(unitFollowed->GetDebugName()),dist,wantedDistance*2,waitSpeed
          );
        }
#endif
        
        // his speed relative to me (positive - he's going away)
        // negate, because it is normal to be in front of us
        float followedSpeedZ = -followedPosDiff.Normalized()*followed->PredictVelocity();
        // wanted speed is given by his speed (followedSpeedZ)
        // and by waitSpeed (my wanted speed relative to him)
        saturateMin(limitSpeed, floatMax(followedSpeedZ + waitSpeed, maxSpeed*0.1));
      }

      // Avoid running for soldiers moving to limited speed waypoint
      if (unit->IsFreeSoldier())
      {
        // limit speed by the waypoint speed mode
        if (subgrp->GetSpeedMode() == SpeedLimited)
        {
          if (leader->GetCombatMode() != CMCombat)
          {
            // allow fast walking, but avoid running
            float limitedSpeed = 1.1f * GetType()->GetLimitedSpeedMs();
            saturateMax(limitedSpeed, 0.1); // avoid dividing by zero
            saturateMin(limitSpeed, limitedSpeed);
          }
        }
      }

      saturate(info.speedWanted,-limitSpeed,+limitSpeed);
      // in convoy it really does not matter if we are in formation or not
      // always assume we are
      return true;
    }
    else
    {
      info.speedWanted=0;
      info.headChange=0;
      info.turnPredict=0;
      return true;
    }
  }

  AIUnit *ledBy = commander->LedBy();
  // TODO: consider checking our formation neighbours instead of the leader
  EntityAIFull *lVehicle=ledBy->GetVehicle();

  float estT=GetFormationTime();

  Vector3 predSpeed = lVehicle->PredictFormationSpeed();
  Vector3 movePos =  commander->GetFormationAbsolute(estT);
  
  Vector3Val moveDir = commander->GetWatchDirection();

  // check position is combat height
  float height = GetCombatHeight();
  movePos[1] = GLandscape->SurfaceYAboveWater(movePos[0],movePos[2])+height;
  
#if _ENABLE_CHEATS
    if (CHECK_DIAG(DEPath))
    {
      Ref<Object> obj=new ObjectColored(GScene->Preloaded(SphereModel),VISITOR_NO_ID);
      obj->SetPosition(movePos);
      obj->SetScale(0.3f);
      obj->SetConstantColor(PackedColor(Color(1,0,1)));
      GScene->ShowObject(obj);
    }
#endif

  PositionPilot(info,movePos,predSpeed,moveDir,1e10,true,lVehicle);
  float formPosRel = 0;
  float speedZ = 0;
  if (unit->GetForceSpeed()>=0)
  {
    saturate(info.speedWanted, -unit->GetForceSpeed(),+unit->GetForceSpeed());
  }

  AlignToTarget(info);

  // when free turn is disabled, it means cover fine maneuvering is being done
  if (info.allowFreeTurn && !CheckMovementNeeded())
  { // wait for other formation members
    // calculate "ideal" speed
    // based on current position and current formation position
    Vector3Val formPosAct = commander->GetFormationAbsolute();

    // if there is no path, use a simple solution
    formPosRel = PositionWorldToModel(formPosAct).Z();

    // calculate a distance along the planned path (if there is any)
    AIBrain *unit = PilotUnit();
    if (unit)
    {
      const Path &path = unit->GetPath();
      if (path.Size()>=2)
      {
        float costForm = path.CostAtPos(formPosAct);
        float costCurrent = path.CostAtPos(FutureVisualState().Position());
        float distForm = path.DistanceAtCost(costForm);
        float distCurrent = path.DistanceAtCost(costCurrent);
        formPosRel = distForm-distCurrent;
      }
    }

    Vector3Val leadSpdRel = DirectionWorldToModel(lVehicle->PredictVelocity());
    // ideal speed is such speed that we will reach position in estT time
    speedZ = floatMax(formPosRel/estT+leadSpdRel.Z(),1);
    saturateMin(info.speedWanted,speedZ);
  }

  
  // check how far are we from the desired position
  
  float precision=GetPrecision();
  #if _ENABLE_REPORT
    if (CHECK_DIAG(DEPath))
    {
      if (this==GWorld->CameraOn())
      {
        DIAG_MESSAGE(500,Format(
          "%s: form. dist %.2f, prec %.2f, spd %.1f, spdZ %.1f ",
          cc_cast(GetDebugName()),formPosRel,precision,info.speedWanted,speedZ
        ));
      }
    }
  #endif
  
  return formPosRel<precision;
}

#if _PROFILE
#pragma optimize("",on)
#endif

#endif

void EntityAI::PerformFF( FFEffects &effects )
{
  //effects=_ff;
  //_ff.gunCount=0; // reset guns
  // calculate acceleration effects
  //Vector3Val accel=DirectionWorldToModel(Acceleration());
  //Vector3Val accelR=accel*0.1;
  //effects.forceX=-accelR[0]*0.5;
  //effects.forceY=-accelR[1]-accelR[2];
  effects.engineMag=0;
  effects.engineFreq=1;
}

void EntityAI::ResetFF()
{
  //_ff.gunCount=0; // reset guns
}

AIGroup* EntityAI::GetGroup() const
{
  AIBrain *unit = CommanderUnit();
  if (!unit)
    unit = PilotUnit();
  if (!unit)
    unit = GunnerUnit();
  return unit ? unit->GetGroup() : NULL;
}

/*!
\patch 2.06 Date 1/5/2004 by Jirka
- Fixed: AI units could not take any magazines.
*/

bool EntityAI::GetActionParams(UIActionParams &params, const Action *action, AIBrain *unit) const
{
  char buffer[1024]; buffer[0] = 0;
  switch (action->GetType())
  {
    case ATUser:
      {
        const ActionIndex *actionTyped = static_cast<const ActionIndex *>(action);
        const UserActionDescription *desc = FindUserAction(actionTyped->GetIndex());
        if (!desc) return false;
        params.Add(CreateTextStructured(NULL, desc->text));
        return true;
      }
    case ATUserType:
      {
        const ActionIndex *actionTyped = static_cast<const ActionIndex *>(action);
        const EntityAIType *type = GetType();
        const UserTypeAction &info = type->_userTypeActions[actionTyped->GetIndex()];
        params.Add(CreateTextStructured(NULL, info.displayName));
        params.Add(CreateTextStructured(NULL, info.displayNameDefault));
        return true;
      }
    case ATTakeWeapon:
    case ATTakeDropWeapon:
      {
        const ActionWeapon *actionTyped = static_cast<const ActionWeapon *>(action);
        Ref<WeaponType> weapon = WeaponTypes.New(actionTyped->GetWeapon());
        AUTO_STATIC_ARRAY(Ref<const WeaponType>, conflict, 16);
#if _ENABLE_REPORT
        int count = 0;
        bool hasWeapon = FindWeapon(weapon, &count);
#else
        bool hasWeapon = FindWeapon(weapon);
#endif
        if (hasWeapon && (!unit || unit->GetPerson()->CheckWeapon(weapon, conflict)))
        {
          params.Add(CreateTextASCII(NULL, weapon->GetDisplayName()));
          if (action->GetType() == ATTakeDropWeapon)
          {
            RString drop;
            for (int i=0; i<conflict.Size(); i++)
            {
              if (i > 0) drop = drop + RString(", ");
              drop = drop + conflict[i]->GetDisplayName();
            }
            params.Add(CreateTextASCII(NULL, drop));
          }
#if _ENABLE_REPORT
          params.Add(CreateTextASCII(NULL, Format(" (%d "+LocalizeString(IDS_DISP_LEFT)+")", count)));
#else
          params.Add(CreateTextASCII(NULL, RString()));
#endif
          return true;
        }
        else
        {
          RptF(
            "Action %s - bad weapon 0x%x %s",
            cc_cast(FindEnumName(action->GetType())),
            (void *)weapon,
            weapon ? cc_cast(weapon->GetName()) : "");
          return false;
        }
      }
    case ATTakeMagazine:
    case ATTakeDropMagazine:
      {
        const ActionMagazineType *actionTyped = static_cast<const ActionMagazineType *>(action);
#if _ENABLE_REPORT
        int count = 0;
        Ref<const Magazine> magazine = FindMagazine(actionTyped->GetMagazineType(), &count);
#else
        Ref<const Magazine> magazine = FindMagazine(actionTyped->GetMagazineType());
#endif
        AUTO_STATIC_ARRAY(Ref<const Magazine>, conflict, 16);
        if (magazine && (!unit || unit->GetPerson()->CheckMagazine(magazine, conflict)))
        {
          params.Add(CreateTextASCII(NULL, magazine->_type->GetDisplayName()));
          if (action->GetType() == ATTakeDropMagazine)
          {
            AUTO_STATIC_ARRAY(MagazineType *, types, 16)
            AUTO_STATIC_ARRAY(int, counts, 16)

            for (int i=0; i<conflict.Size(); i++)
            {
              MagazineType *type = conflict[i]->_type;
              if (conflict[i]->GetAmmo() > 0 && type != magazine->_type)
              {
                bool found = false;
                for (int j=0; j<types.Size(); j++)
                {
                  if (types[j] == type)
                  {
                    counts[j]++;
                    found = true;
                    break;
                  }
                  }
                if (!found)
                {
                  types.Add(type);
                  counts.Add(1);
                }
              }
            }

            RString drop;
            for (int i=0; i<types.Size(); i++)
            {
              if (i > 0) drop = drop + RString(", ");
              RString name = types[i]->GetDisplayName();
              if (counts[i] == 1)
                drop = drop + name;
              else
                drop = drop + Format("%d x %s", counts[i], cc_cast(name));
            }
            params.Add(CreateTextASCII(NULL, drop));
          }
#if _ENABLE_REPORT
          params.Add(CreateTextASCII(NULL, Format(" (%d "+LocalizeString(IDS_DISP_LEFT)+")", count)));
#else
          params.Add(CreateTextASCII(NULL, RString()));
#endif
          return true;
        }
        else
        {
          RptF(
            "Action %s - bad magazine 0x%x %s",
            cc_cast(FindEnumName(action->GetType())),
            (const void *)magazine,
            magazine ? cc_cast(magazine->_type->GetName()) : "");
          return false;
        }
      }
  }
  RptF("Unknown action %s", cc_cast(FindEnumName(action->GetType())));
  return false;
}

static bool FormatWeapon(UIActionParams &params, UIActionType action, const EntityAIFull *veh, const TurretContext &context, int weapon)
{
  if (veh)
  {
    if (weapon >= 0 && weapon < context._weapons->_magazineSlots.Size())
    {
      const MagazineSlot &slot = context._weapons->_magazineSlots[weapon];
      RStringB displayName = slot._weapon->_displayName;
/*
      RStringB displayName = slot._muzzle->_displayName;
      const WeaponModeType *mode = slot._weaponMode;
      if (mode) displayName = mode->GetDisplayName();
*/
      params.Add(CreateTextASCII(NULL, displayName));
      return true;
    }
    else
    {
      RptF("Action %s - wrong weapon %d", cc_cast(FindEnumName(action)), weapon);
      return false;
    }
  }
  else
  {
    RptF("Action %s - wrong target", cc_cast(FindEnumName(action)));
    return false;
  }
}

INode *FormatText(const char *format, RefR<INode> *args, int nArgs, bool structured);

bool EntityAIFull::GetActionParams(UIActionParams &params, const Action *action, AIBrain *unit) const
{
  switch (action->GetType())
  {
    case ATSwitchWeapon:
    case ATHideWeapon:
    case ATSwitchMagazine:
      {
        const ActionWeaponIndex *actionTyped = static_cast<const ActionWeaponIndex *>(action);
        TurretContext context;
        if (FindTurret(actionTyped->GetGunner(), context))
        {
          int weapon = actionTyped->GetWeaponIndex();
          if (weapon < 0) weapon = context._weapons->_currentWeapon;
          return FormatWeapon(params, action->GetType(), this, context, weapon);
        }
        else
        {
          RptF("Action %s - wrong weapon system", cc_cast(FindEnumName(action->GetType())));
          return false;
        }
      }
    case ATUseWeapon:
      {
        const ActionWeaponIndex *actionTyped = static_cast<const ActionWeaponIndex *>(action);

        TurretContext context;
        if (FindTurret(actionTyped->GetGunner(), context))
        {
          const WeaponModeType *mode = context._weapons->GetWeaponMode(actionTyped->GetWeaponIndex());
          if (mode)
          {
            int left = 0;
            const MagazineSlot &slot = context._weapons->_magazineSlots[actionTyped->GetWeaponIndex()];
            const Magazine *magazine = slot._magazine;
            // count magazines
            for (int i=0; i<context._weapons->_magazines.Size(); i++)
            {
              const Magazine *m = context._weapons->_magazines[i];
              if (!m || m->GetAmmo() == 0) continue;
              if (m->_type == magazine->_type) left++;
            }
            RefR<INode> subparams[2];
            subparams[0] = CreateTextASCII(NULL, mode->GetDisplayName());
            subparams[1] = CreateTextASCII(NULL, Format("%d", left));
            params.Add(FormatText(mode->_useActionTitle, subparams, 2, true));
            return true;
          }
          else
          {
            RptF("Action %s - wrong weapon %d", cc_cast(FindEnumName(action->GetType())), actionTyped->GetWeaponIndex());
            return false;
          }
        }
        else
        {
          RptF("Action %s - wrong weapon system", cc_cast(FindEnumName(action->GetType())));
          return false;
        }
      }
    case ATUseMagazine:
      {
        // TODO: what turret?
        TurretContext context;
        if (GetPrimaryGunnerTurret(context))
        {
          const ActionMagazine *actionTyped = static_cast<const ActionMagazine *>(action);
          const Magazine *magazine = FindMagazine(actionTyped->GetCreator(), actionTyped->GetId());
          if (magazine)
          {
            // count magazines
            int left = 0;
            for (int i=0; i<context._weapons->_magazines.Size(); i++)
            {
              const Magazine *m = context._weapons->_magazines[i];
              if (!m || m->GetAmmo() == 0) continue;
              if (m->_type == magazine->_type) left++;
            }
            RefR<INode> subparams[2];
            subparams[0] = CreateTextASCII(NULL, magazine->_type->GetDisplayName());
            subparams[1] = CreateTextASCII(NULL, Format("%d", left));
            params.Add(FormatText(magazine->_type->_useActionTitle, subparams, 2, true));
            return true;
          }
          else
          {
            RptF("Action %s - wrong magazine %d:%d", cc_cast(FindEnumName(action->GetType())), actionTyped->GetCreator(), actionTyped->GetId());
            return false;
          }
        }
        else
        {
          RptF("Action %s - wrong weapon system", cc_cast(FindEnumName(action->GetType())));
          return false;
        }
      }
    case ATLoadMagazine:
    case ATLoadOtherMagazine:
    case ATLoadEmptyMagazine:
      {
        // ActionMagazineWithMuzzle not needed here
        const ActionMagazine *actionTyped = static_cast<const ActionMagazine *>(action);
        const Magazine *magazine = FindMagazine(actionTyped->GetCreator(), actionTyped->GetId());
        if (magazine)
        {
          params.Add(CreateTextASCII(NULL, magazine->_type->GetDisplayName()));
          return true;
        }
        else
        {
          RptF("Action %s - wrong magazine %d:%d", cc_cast(FindEnumName(action->GetType())), actionTyped->GetCreator(), actionTyped->GetId());
          return false;
        }
      }
  }
  return base::GetActionParams(params, action, unit);
}

void EntityAI::ProcessUserAction(RString name)
{
  const EntityAIType *type = GetType();
  for (int i=0; i<type->_userTypeActions.Size(); i++)
  {
    const UserTypeAction &act = GetType()->_userTypeActions[i];
    if (stricmp(act.name, name) == 0)
    {
      GameState *state = GWorld->GetGameState();
      GameVarSpace local(state->GetContext(), false);
      state->BeginContext(&local);

      state->VarSetLocal("this", GameValueExt(this), true, true);
      state->Execute(act.statement, GameState::EvalContext::_default, GWorld->GetMissionNamespace()); // mission namespace

      state->EndContext();
      return;
    }
  }
}

bool EntityAI::DoneUserAction(RString name)
{
  const EntityAIType *type = GetType();
  for (int i=0; i<type->_userTypeActions.Size(); i++)
  {
    const UserTypeAction &act = GetType()->_userTypeActions[i];
    if (stricmp(act.name, name) == 0)
    {
      GameState *state = GWorld->GetGameState();
      GameVarSpace local(state->GetContext(), false);
      state->BeginContext(&local);

      state->VarSetLocal("this", GameValueExt(this), true, true);
      bool notDone = state->EvaluateBool(act.condition, GameState::EvalContext::_default, GWorld->GetMissionNamespace()); // mission namespace

      state->EndContext();

      return !notDone;
    }
  }
  return true;
}

void EntityAI::StartActionProcessing(Action *action, AIBrain *unit)
{
  // process action directly by default
  action->Process(unit);
}


/*!
\patch 1.61 Date 5/30/2002 by Jirka
- Fixed: Enable put action for all owned explosives
*/
void DeleteVehicle(Entity *veh);
void EntityAI::PerformAction(const Action *action, AIBrain *unit)
{
  EntityAIFull *veh = unit->GetVehicle();
  switch (action->GetType())
  {
    case ATLightOn:
      SetPilotLight(true);
      return;
    case ATLightOff:
      SetPilotLight(false);
      return;
    case ATUser:
      if (unit)
      {
        const ActionIndex *actionTyped = static_cast<const ActionIndex *>(action);
        const UserActionDescription *desc = FindUserAction(actionTyped->GetIndex());
        if (desc && desc->script.GetLength() > 0)
        {
          GameArrayType arguments;
          arguments.Add(GameValueExt(this));
          arguments.Add(GameValueExt(unit->GetPerson()));
          arguments.Add(GameValue((float)desc->id));
          arguments.Add(desc->param);
#if USE_PRECOMPILATION
          const char *ext = strrchr(desc->script, '.');
          if (ext && stricmp(ext, ".sqf") == 0)
          {
            RString FindScript(RString name);
            RString fullName = FindScript(desc->script);
            if (fullName.GetLength() > 0)
            {
              ScriptVM *script = new ScriptVM(fullName, GameValue(arguments), GWorld->GetMissionNamespace()); // mission namespace
              GWorld->AddScriptVM(script, false);
            }
          }
          else
#endif
          {
            Script *script = new Script(desc->script, GameValue(arguments), GWorld->GetMissionNamespace()); // mission namespace
            GWorld->AddScript(script, false);
          }
        }
      }
      return;
    case ATUserType:
      if (unit)
      {
        const ActionIndex *actionTyped = static_cast<const ActionIndex *>(action);
        if(actionTyped->GetIndex() < GetType()->_userTypeActions.Size())
        {
          const UserTypeAction &act = GetType()->_userTypeActions[actionTyped->GetIndex()];

          GameState *state = GWorld->GetGameState();
          GameVarSpace local(state->GetContext(), false);
          state->BeginContext(&local);

          state->VarSetLocal("this", GameValueExt(this), true, true);
          state->Execute(act.statement, GameState::EvalContext::_default, GWorld->GetMissionNamespace()); // mission namespace

          state->EndContext();
        }
      }
      return;
    case ATTakeWeapon:
    case ATTakeDropWeapon:
    case ATTakeMagazine:
    case ATTakeDropMagazine:
    case ATDropWeapon:
    case ATPutWeapon:
    case ATDropMagazine:
    case ATPutMagazine:
      veh = unit->GetPerson();
      // continue
    case ATHeal:
    case ATRepair:
    case ATRefuel:
    case ATRearm:
      if (!veh->IsActionInProgress(MFReload))
        Supply(veh, action);
      return;
    case ATHealSoldier:
      if (!veh->IsActionInProgress(MFReload))
        SupplyInverse(veh, action);
      return;
    case ATRepairVehicle:
      if (!veh->IsActionInProgress(MFReload))
        SupplyInverse(veh, action);
      return;
    case ATFirstAid:
      if (!veh->IsActionInProgress(MFReload))
      {

      }
      return;
    case ATDropBag:
    case ATAddBag:
      {
        veh = unit->GetPerson();
        if (!veh->IsActionInProgress(MFReload))
          Supply(veh, action);
        return;
      }
      return;
    case ATPutBag:
      {
        veh = unit->GetPerson();
        if (!veh->IsActionInProgress(MFReload))
          Supply(veh, action);
        return;
      }
      return;
   case ATTakeBag:
        if(unit->GetPerson())
        {
          unit->GetPerson()->TakeBackpack(action->GetTarget());
        }
      return;
    case ATDisAssemble:
      {
        RString disassembleTo[2];
        Vector3 outPos;
        EntityAI *assembled = NULL;
        EntityAI *primary = NULL;
        EntityAI *secondary = NULL;

        if(action->GetTarget())
        {
          assembled = action->GetTarget();
          outPos = action->GetTarget()->FutureVisualState().Position();
          disassembleTo[0] = action->GetTarget()->Type()->_assembleInfo.dissasembleTo[0];
          disassembleTo[1] = action->GetTarget()->Type()->_assembleInfo.dissasembleTo[1];
        }

        if(disassembleTo[0].GetLength()>0)
        {
          Ref<EntityAI> backpack = GWorld->NewVehicleWithID(disassembleTo[0]);
          if(backpack && backpack->GetShape())
          {
            Vector3 dirXZ = (Vector3(FutureVisualState().Direction().Z(),0,-FutureVisualState().Direction().X()));
            dirXZ.Normalize();
            Vector3 position = outPos + VUp*0.5f;
            if(disassembleTo[1].GetLength()>0) position += 0.5f * dirXZ * backpack->GetShape()->BoundingSphere();

            Matrix3 dir;
            dir.SetUpAndDirection(VUp, FutureVisualState().Direction());

            Matrix4 transform;
            transform.SetPosition(position);
            transform.SetOrientation(dir);

            backpack->PlaceOnSurfaceUnderWater(transform); //we want weapons on the floor
            backpack->SetTransform(transform);

            backpack->Init(transform, true); // do not reset movement
            backpack->OnEvent(EEInit);

            // Add into World
            GWorld->AddSlowVehicle(backpack);
            if (GWorld->GetMode() == GModeNetware)
              GetNetworkManager().CreateVehicle(backpack, VLTSlowVehicle, "", -1);

            GWorld->OnEntityMovedFar(backpack);
            if(backpack->Type()->_assembleInfo.type == PrimaryPart) primary = backpack;
            else secondary = backpack;
          }
        }

        if(disassembleTo[1].GetLength()>0)
        {
          Ref<EntityAI> backpack = GWorld->NewVehicleWithID(disassembleTo[1]);
          if(backpack && backpack->GetShape())
          {
            Vector3 dirXZ = (Vector3(FutureVisualState().Direction().Z(),0,-FutureVisualState().Direction().X()));
            dirXZ.Normalize();
            Vector3 position = outPos - 0.5f * dirXZ * backpack->GetShape()->BoundingSphere() + VUp*0.5f;

            Matrix3 dir;
            dir.SetUpAndDirection(VUp, FutureVisualState().Direction());

            Matrix4 transform;
            transform.SetPosition(position);
            transform.SetOrientation(dir);

            backpack->PlaceOnSurfaceUnderWater(transform); //we want weapons on the floor
            backpack->SetTransform(transform);

            backpack->Init(transform, true); // do not reset movement
            backpack->OnEvent(EEInit);

            // Add into World
            GWorld->AddSlowVehicle(backpack);
            if (GWorld->GetMode() == GModeNetware)
              GetNetworkManager().CreateVehicle(backpack, VLTSlowVehicle, "", -1);

            GWorld->OnEntityMovedFar(backpack);
            if(backpack->Type()->_assembleInfo.type == PrimaryPart) primary = backpack;
            else secondary = backpack;
          }
        }

        if(primary)
        {
          if(primary->IsLocal())
          {
            assembled->UnlockPosition();
            if (assembled->IsInLandscape())
            {
              primary->SetAseembleTo(assembled);
              assembled->SetMoveOut(primary); // remove reference from the world
            }
          }
          else if(GWorld->GetMode()  == GModeNetware) GetNetworkManager().DisAssemble(assembled, primary);     
        }

        if (unit && unit->GetPerson() && unit->GetPerson()->IsEventHandler(EEWeaponDisassembled))
        {
          unit->GetPerson()->OnEvent(EEWeaponDisassembled, primary, secondary);
        } 
      }
      return;
    case ATAssemble:
      {
        RString assembleTo;
        EntityAI *assembled = NULL;
        Vector3 direction = (unit!=NULL) ? unit->Direction(unit->GetFutureVisualState()) : FutureVisualState().Direction();
        Vector3 position = (unit!=NULL) ? unit->Position(unit->GetFutureVisualState()) : FutureVisualState().Position();

        if(unit->GetPerson() && unit->GetPerson()->GetBackpack())
        {
          EntityAI *backpack =  unit->GetPerson()->GetBackpack();
          if(backpack->Type()->_assembleInfo.type == PrimaryPart) 
          {
            assembleTo = backpack->Type()->_assembleInfo.assembleTo;
            assembled = backpack->GetAseembleTo();
            backpack->SetAseembleTo(NULL);
          }
          unit->GetPerson()->DropBackpack();
          if (backpack->IsLocal())
            DeleteVehicle(backpack);
          else
            GetNetworkManager().AskForDeleteVehicle(backpack);
        }

        if(action->GetTarget() && action->GetTarget()->GetType()->IsBackpack())
        {
          if(action->GetTarget()->Type()->_assembleInfo.type == PrimaryPart)
          {
            assembleTo = action->GetTarget()->Type()->_assembleInfo.assembleTo;
            assembled = action->GetTarget()->GetAseembleTo();
            action->GetTarget()->SetAseembleTo(NULL);
          }
          if (action->GetTarget()->IsLocal())
            DeleteVehicle(action->GetTarget());
          else
            GetNetworkManager().AskForDeleteVehicle(action->GetTarget());
        }

        if(assembled)
        {
          Vector3 dirXZ = (Vector3(direction.X(),0,direction.Z()));
          dirXZ.Normalize();

          Vector3 pos = position + dirXZ * assembled->GetShape()->BoundingSphere() + VUp*0.5f;
          Matrix3 dir;
          dir.SetUpAndDirection(VUp, direction);
          Matrix4 transform;
          transform.SetPosition(pos);
          transform.SetOrientation(dir);

          assembled->PlaceOnSurfaceUnderWater(transform); //we want weapons on the floor
          assembled->SetTransform(transform);

          assembled->Init(transform, true); // do not reset movement
          assembled->OnEvent(EEInit);

          if (unit && unit->GetPerson() && unit->GetPerson()->IsEventHandler(EEWeaponAssembled))
          {
            unit->GetPerson()->OnEvent(EEWeaponAssembled, assembled);
          }  

          //if (assembled->IsLocal()) 
          {
            assembled->ResetMoveOut(); // mark it is in landscape
            GLOB_WORLD->AddVehicle(assembled);
            GLOB_WORLD->RemoveOutVehicle(assembled);
            // we need other entities nearby to be notified of our presence
            assembled->IsMoved();
            GWorld->OnEntityMovedFar(assembled);
          }  
          if(GWorld->GetMode()  == GModeNetware)
          {      
            GetNetworkManager().Assemble(assembled, transform);
          }
        }
        else if(assembleTo.GetLength() > 0)
        {
          assembled = GWorld->NewVehicleWithID(assembleTo);
          if (assembled)
          {
            Vector3 dirXZ = (Vector3(direction.X(),0,direction.Z()));
            dirXZ.Normalize();

            Vector3 pos = position + dirXZ * assembled->GetShape()->BoundingSphere() + VUp*0.5f;
            Matrix3 dir;
            dir.SetUpAndDirection(VUp, direction);
            Matrix4 transform;
            transform.SetPosition(pos);
            transform.SetOrientation(dir);

            assembled->PlaceOnSurfaceUnderWater(transform); //we want weapons on the floor
            assembled->SetTransform(transform);

            assembled->Init(transform, true); // do not reset movement
            assembled->OnEvent(EEInit);

            if (unit && unit->GetPerson() && unit->GetPerson()->IsEventHandler(EEWeaponAssembled))
            {
              unit->GetPerson()->OnEvent(EEWeaponAssembled, assembled);
            }          

            // Add into World
            GWorld->AddVehicle(assembled);
            if (GWorld->GetMode() == GModeNetware)
              GetNetworkManager().CreateVehicle(assembled, VLTVehicle, "", -1);

            GWorld->OnEntityMovedFar(assembled);
          }
        }       
      }
      return;
/*
    case ATCancelAction:
      CancelSupply();
      {
        // Attendant
        Person *person = dyn_cast<Person>(this);
        if (person) person->PutDownEnd();
      }
      unit->GetPerson()->PutDownEnd();
      return;
*/
  }
  // note: action is performed by target of the action
  // unit is who activated the action
  ErrF
  (
    "%s (%s): Unknown action %s, target %s",
    (const char *)GetDebugName(),
    (const char *)GetType()->GetDisplayName(),
    (const char *)FindEnumName(action->GetType()),
    (const char *)action->GetTarget()->GetDebugName()
  );
}

void EntityAIFull::PerformAction(const Action *action, AIBrain *unit)
{
  //EntityAIFull *veh = unit->GetVehicle();
  switch (action->GetType())
  {
    case ATSwitchWeapon:
    case ATHideWeapon:
    case ATSwitchMagazine:
      {
        const ActionWeaponIndex *actionTyped = static_cast<const ActionWeaponIndex *>(action);

        TurretContext context; 
        if (FindTurret(actionTyped->GetGunner(), context))
        {
          SelectWeaponCommander(unit, context, actionTyped->GetWeaponIndex());
          OnWeaponSelected();
        }
      }
      return;
    case ATUseWeapon:
      {
        const ActionWeaponIndex *actionTyped = static_cast<const ActionWeaponIndex *>(action);
        
        TurretContextV context; 
        if (FindTurret(unconst_cast(FutureVisualState()), actionTyped->GetGunner(), context))
          FireWeapon(context, actionTyped->GetWeaponIndex(), NULL, false);
      }
      return;
    case ATUseMagazine:
      {
        const ActionMagazine *actionTyped = static_cast<const ActionMagazine *>(action);
        int creator = actionTyped->GetCreator();
        int id = actionTyped->GetId();

        // find magazine
        TurretContextV context; 
        if (!FindTurret(unconst_cast(FutureVisualState()), actionTyped->GetGunner(), context)) return;

        int m = -1;
        for (int i=0; i<context._weapons->_magazines.Size(); i++)
        {
          Magazine *magazine = context._weapons->_magazines[i];
          if (magazine && magazine->_creator == creator && magazine->_id == id)
          {
            m = i;
            break;
          }
        }
        if (m < 0)
          return;
        Magazine *magazine = context._weapons->_magazines[m];

        // find slot
        int s = -1;
        for (int i=0; i<context._weapons->_magazineSlots.Size();)
        {
          MagazineSlot &slot = context._weapons->_magazineSlots[i];
          if (!slot._muzzle)
          {
            i++;
            continue;
          }
          if (slot._muzzle->CanUse(magazine->_type))
          {
            s = i;
            break;
          }
          i += slot._muzzle->_modes.Size();
        }
        if (s < 0)
          return;

        // reload and fire
        ReloadMagazineTimed(*context._weapons, s, m, true);
        magazine->_reload = 0;
        FireWeapon(context, s, NULL, false);
      }
      return;
    case ATLoadMagazine:
    case ATLoadOtherMagazine:
    case ATLoadEmptyMagazine:
      if ( unit &&!IsActionInProgress(MFReload) && this==unit->GetVehicle()
#if _VBS2 //Convoy trainer
        || unit->GetPerson()->IsPersonalItemsEnabled()
#endif
        )
      {
        const ActionMagazineAndMuzzle *actionTyped = static_cast<const ActionMagazineAndMuzzle *>(action);
       
        TurretContext context;
        if(actionTyped->GetWeapon() && actionTyped->GetMuzzle())
        {
          if (FindTurret(unit->GetPerson(), context) && CanFireUsing(context) && unit->GetPerson()==actionTyped->GetGunner())
            GunnerLoadMagazine(context, actionTyped->GetCreator(), actionTyped->GetId(), actionTyped->GetWeapon()->GetName(), actionTyped->GetMuzzle()->GetName());
          else
            CommanderLoadMagazine(actionTyped->GetCreator(), actionTyped->GetId(), actionTyped->GetWeapon()->GetName(), actionTyped->GetMuzzle()->GetName());
          GWorld->UI()->ShowWeaponInfo();
        }
      }
      return;
  }
  base::PerformAction(action,unit);
}

void EntityAIFull::GunnerLoadMagazine(const TurretContext &context, int creator, int id, RString weapon, RString muzzle)
{
  int iMagazine = -1;
  for (int i=0; i<context._weapons->_magazines.Size(); i++)
  {
    const Magazine *mag = context._weapons->_magazines[i];
    if (!mag) continue;
    if (mag->_creator == creator && mag->_id == id)
    {
      iMagazine = i;
      break;
    }
  }
  if (iMagazine < 0) return;

  int iSlot = -1;
  for (int i=0; i<context._weapons->_magazineSlots.Size(); i++)
  {
    const MagazineSlot &slot = context._weapons->_magazineSlots[i];
    if (slot._weapon->GetName() == weapon && slot._muzzle->GetName() == muzzle)
    {
      iSlot = i;
      break;
    }
  }
  if (iSlot < 0) return;

  ReloadMagazine(*context._weapons, iSlot,iMagazine);

  // FIX
  if (!IsLocal()) GetNetworkManager().UpdateWeapons(this, context._turret, context._gunner);

  void UpdateWeaponsInBriefing();
  UpdateWeaponsInBriefing();
}

void EntityAIFull::CommanderLoadMagazine(int creator, int id, RString weapon, RString muzzle)
{
  // default implementation, changed in Transport
  TurretContext context;
  if (GetPrimaryGunnerTurret(context))
    GunnerLoadMagazine(context, creator, id, weapon, muzzle);
}

LODShapeWithShadow *EntityAIFull::GetMissileShape(int proxyIndex) const
{
  // TODO: what turret?

  // track which missile index we are currently processing
  int total = 0;
  TurretContext context;
  if (GetPrimaryGunnerTurret(context))
  {
    for (int i=0; i<context._weapons->_magazines.Size(); i++)
    {
      const Magazine *mag = context._weapons->_magazines[i];
      const MagazineType *type = mag->_type;
      if (!type) continue;
      const AmmoType *ammo = type->_ammo;
      if (!ammo) continue;
      if (ammo->_simulation != AmmoShotMissile) continue;
      if (ammo->maxControlRange < 10) continue;
      
      total+=mag->_type->_maxAmmo;
      // is this the missile we are interested in?
      if (total<proxyIndex) continue;
      if (total+mag->GetAmmo()-mag->_type->_maxAmmo<proxyIndex)return NULL;

      if (ammo->_proxyShape) return ammo->_proxyShape;
    }
  }
  return NULL;
}

int EntityAIFull::CountMissiles() const
{
  int total = 0;
  // TODO: what turret?
  TurretContext context;
  if (GetPrimaryGunnerTurret(context))
  {
    for (int i=0; i<context._weapons->_magazines.Size(); i++)
    {
      const Magazine *mag = context._weapons->_magazines[i];
      const MagazineType *type = mag->_type;

      if (!type) continue;
      const AmmoType *ammo = type->_ammo;
      if (!ammo) continue;
      if (ammo->_simulation!=AmmoShotMissile) continue;
      if (ammo->maxControlRange<10) continue;
      total += mag->GetAmmo();
    }
  }
  return total;   
}

static void GetReloadActions(EntityAIFull *veh, UIActions &actions,
  const TurretContext &context, int iSlot, const MuzzleType *currentMuzzle)
{
  const MagazineSlot &slot = context._weapons->_magazineSlots[iSlot];
  const Magazine *magazine = slot._magazine;
  const MagazineType *mType = magazine ? magazine->_type : NULL;
  bool empty = false;
  bool shortcut = veh->GetType()->IsPerson(); // allow reload only for soldiers, 'R' is reserved for CMs in vehicles
  if (mType)
  {
    if (mType->_ammo && mType->_maxAmmo > 0)
      empty = magazine->GetAmmo() == 0;
    if (!empty && currentMuzzle->_autoReload &&
      (mType->_maxAmmo == 1 || (slot._weapon->_weaponType & MaskHardMounted) != 0))
    {
      // do not offer reload HandGrenade etc.
    }
    else
    {
      // magazine of the same type
      int best = context._weapons->FindBestMagazine(mType, 0);
      if (best >= 0)
      {
        const Magazine *magazine = context._weapons->_magazines[best];
        // RString muzzleID = slot._weapon->GetName() + "|" + slot._muzzle->GetName();
        UIAction action(new ActionMagazineAndMuzzle(
          empty ? ATLoadEmptyMagazine : ATLoadMagazine, veh, context._gunner, magazine->_creator, magazine->_id,
          slot._weapon, slot._muzzle));
        actions.Add(action);
        shortcut = false;
      }
    }
  }
  else empty = true;
  // magazines of other type
  for (int i=0; i<currentMuzzle->_magazines.Size(); i++)
  {
    const MagazineType *change = currentMuzzle->_magazines[i];
    if (change == mType) continue;
    int best = context._weapons->FindBestMagazine(change, 0);
    if (best >= 0)
    {
      const Magazine *magazine = context._weapons->_magazines[best];
      UIAction action(new ActionMagazineAndMuzzle(
        empty ? ATLoadEmptyMagazine : ATLoadOtherMagazine, veh, context._gunner, magazine->_creator, magazine->_id,
        slot._weapon, slot._muzzle));
      if (!shortcut) action.shortcut = (UserAction)-1; 
      actions.Add(action);
      shortcut = false;
    }
  }
}

TypeIsSimple(MagazineType *)

class IsCrewEmpty : public ICrewFunc
{
protected:
  int  *_count;

public:
  IsCrewEmpty(int &count) {_count = &count;}

  virtual bool operator () (Person *person)
  {
    if(person) return true; // continue
    return false;
  }
};

/*!
\param actions List to which result should be appended.
\param unit Unit that tries to perform action on this entity
\param now If true, only actions that can be performed
from current unit position are listed.
\patch 1.11 Date 08/03/2001 by Jirka
- Improved: user actions enabled also for AI units
\patch 1.34 Date 12/6/2001 by Jirka
- Fixed: commander can switch ligths only if not in cargo
\patch 1.89 Date 10/25/2002 by Ondra
- Fixed: Custom action inside vehicle was enable only until vehicle moved.
*/

bool CheckSupply(EntityAI *vehicle, EntityAI *parent, SupportCheckF check, float limit, bool now);
void EntityAI::GetActions(UIActions &actions, AIBrain *unit, bool now, bool strict)
{
  if (!unit) return;
  // user actions
  // FIX
  if (_userActions.Size() > 0)
  {
    bool ok = true;
    if (now && unit->GetVehicle() != this)
    {
      // check if in front
      Vector3Val relPos = unit->GetPerson()->PositionWorldToModel(RenderVisualState().Position());
      ok = relPos.Z() > 0 && relPos.SquareSize() < Square(10);
    }
    if (ok)
    {
      for (int i=0; i<_userActions.Size(); i++)
      {
        UserActionDescription &desc = _userActions[i];
        if (desc.condition.GetLength() > 0)
        {
          GameVarSpace local(false);
          GameState *gstate = GWorld->GetGameState();
          gstate->BeginContext(&local);
          gstate->VarSetLocal("_this", GameValueExt(unit->GetPerson()), true);
          gstate->VarSetLocal("_target", GameValueExt(this), true);
#if USE_PRECOMPILATION
          bool ok = gstate->EvaluateBool(desc.condition, desc.conditionExp, GameState::EvalContext::_default, GWorld->GetMissionNamespace()); // mission namespace
#else
          bool ok = gstate->EvaluateBool(desc.condition, GameState::EvalContext::_default, GWorld->GetMissionNamespace()); // mission namespace
#endif
          gstate->EndContext();
          if (!ok) continue;
        }

        UIAction action(new ActionIndex(ATUser, this, desc.id));
        action.priority = desc.priority - 0.001 * desc.id;
        action.showWindow = desc.showWindow;
        action.hideOnUse = desc.hideOnUse;
        action.shortcut = desc.shortcut;
        actions.Add(action);
      }
    }
  }

  // user type actions
#if _ENABLE_DATADISC
  const EntityAIType *type = GetType();
  if (type->_userTypeActions.Size() > 0)
  {
    // TODO: when in vehicle, we should consider aiming position of the person instead
    // however, AimingPosition currently does not work for men inside vehicles
    Vector3Val personActionPos = unit->GetVehicle()->AimingPosition(unit->GetVehicle()->RenderVisualState());
    Vector3Val relPos = RenderVisualState().PositionWorldToModel(personActionPos);

    GameState *state = GWorld->GetGameState();
    GameVarSpace local(state->GetContext(), false);
    state->BeginContext(&local);
    state->VarSetLocal("this", GameValueExt(this), true, true);

    for (int i=0; i<type->_userTypeActions.Size(); i++)
    {
      const UserTypeAction &act = type->_userTypeActions[i];
      // when gathering actions for commanding menu (!now), skip actions which are marked as "for player only"
      if (!now && act.onlyForPlayer) continue;

      Vector3 modelPos = act.modelPosition;
#if _VBS3 // animate memory points for actions
      if( act.positionIndex > -1)
      {
        int mem = _shape->FindMemoryLevel();
        if(mem > -1)
          modelPos = AnimatePoint(RenderVisualState(), mem, act.positionIndex);
      }
#endif //, needs modifications below as well (modelPos)

      if (now)
      {
        if(relPos.Distance2(modelPos) > Square(act.radius)) continue;
#if _VBS2
          Vector3Val relPos = unit->GetPerson()->PositionWorldToModel(RenderVisualState().PositionModelToWorld(modelPos));
          if(relPos.Z() < 0) continue;      //check if in front
#endif
      }
      if (!state->EvaluateBool(act.condition, GameState::EvalContext::_default, GWorld->GetMissionNamespace())) continue; // mission namespace

      UIAction action(new ActionIndex(ATUserType, this, i));
      action.position = RenderVisualState().PositionModelToWorld(modelPos);
      action.priority = act.priority -0.001 * i;
      action.showWindow = act.showWindow;
      action.hideOnUse = act.hideOnUse;
      action.shortcut = act.shortcut;
      actions.Add(action);
    }

    state->EndContext();
  }
#endif

  if (unit->GetVehicle() == this && CommanderUnit() == unit && !unit->IsInCargo())
  {
    // lights on / off
    if (unit->IsPlayer() && _reflectors.SomeLights())
    {
      if (IsPilotLight())
      {
        UIAction action(new ActionBasic(ATLightOff, this));
        actions.Add(action);
      }
      else
      {
        if (GScene->MainLight()->NightEffect() > 0)
        {
          UIAction action(new ActionBasic(ATLightOn, this));
          actions.Add(action);
        }
      }
    }
  }

  if (unit && !IsDamageDestroyed() )
  {
    if (_supply)
    {
      if (unit->IsEnemy(GetTargetSide())) return;
      _supply->GetActions(this,actions, unit, now, strict);
    }

    if(unit->GetPerson() && unit->GetVehicle())
    {
      bool check = false;
      if(unit->GetVehicle()->GetShape()) check = CheckSupply(unit->GetVehicle(), this, NULL, 0, now); // check distance
      if(check && type->IsBackpack())
      {
        if ((unit->IsFreeSoldier() && !unit->GetPerson()->IsActionInProgress(MFReload)))
        {
          //take backpack
          EntityAIFull *veh = unit->GetVehicle();
          if(veh->CanCarryBackpack() )
          {//medics already have bag
            UIAction action(new ActionBasic(ATTakeBag, this));
            action.position = RenderVisualStateScope(true)->PositionModelToWorld(type->GetSupplyPoint());
            actions.Add(action);
          }

          //assemble weapon
          EntityAI *bag = unit->GetPerson()->GetBackpack();
          if(bag && bag->Type()->_assembleInfo.type == PrimaryPart && type->_assembleInfo.type == SecondaryPart)
          {
            if( strcmpi(bag->Type()->_assembleInfo.base, type->GetName())== 0)
            {
              {
                UIAction action(new ActionWeapon(ATAssemble, this, bag->Type()->_assembleInfo.displayName, false));
                action.position = RenderVisualStateScope(true)->PositionModelToWorld(type->GetSupplyPoint());
                actions.Add(action);
              }
            }
          }
          else if(bag && bag->Type()->_assembleInfo.type == SecondaryPart && type->_assembleInfo.type == PrimaryPart)
          {
            if( strcmpi(bag->Type()->GetName(), type->_assembleInfo.base) == 0)
            {
              {
                UIAction action(new ActionWeapon(ATAssemble, this, type->_assembleInfo.displayName, false));
                action.position = RenderVisualStateScope(true)->PositionModelToWorld(type->GetSupplyPoint());
                actions.Add(action);
              }
            }
          }
        }
      }

      EntityAIFull *veh = unit->GetVehicle();
      if(veh)
      {
        bool check =  veh->GetSupply()->Check(veh, this, NULL, 0, now); // check distance
        if(check && type->_assembleInfo.type == Assembled)
        {
          int count = 0;
          IsCrewEmpty func(count);
          if(!ForEachCrew(func))
          {
            UIAction action(new ActionBasic(ATDisAssemble, this));
            action.position = RenderVisualStateScope(true)->Position();
            actions.Add(action);
          }
        }
      }

    }

    if(this->CommanderUnit() &&  this->CommanderUnit()->IsFreeSoldier()) //you cannot take action from medic
    { //unit does not have supply, but needs to be healed 
      EntityAIFull *veh = unit->GetVehicle();
      if (!(now && !veh->IsLocal())&& this->CommanderUnit() && this->CommanderUnit()->IsFreeSoldier()) 
      {
        bool soldier = unit->IsFreeSoldier();
        const EntityAIType *vehType = veh->GetType();
        bool check =  veh->GetSupply()->Check(veh, this, NULL, 0, now); // check distance

        if ((check || (unit->GetVehicle() && this == unit->GetVehicle())) 
          && vehType->IsAttendant() && soldier)
        {
          if (!this->IsAnimal() && this->NeedsAmbulance() > 0)
          {
            UIAction action(new ActionBasic(ATHealSoldier, this));
            action.position = this->RenderVisualStateScope(true)->PositionModelToWorld(vehType->GetSupplyPoint());
            actions.Add(action);
          }
        }
        else
        {
          float dist2 = veh->RenderVisualStateScope(true)->Position().Distance2(RenderVisualStateScope(true)->Position());
          if(this->CommanderUnit() && (dist2<6 || !now) )
            if(!this->IsAnimal() && this->CommanderUnit()->GetLifeState() == LifeStateUnconscious && soldier)
            {
              UIAction action(new ActionBasic(ATFirstAid, this));
              action.position = this->RenderVisualState().PositionModelToWorld(veh->RenderVisualState().Position());
              actions.Add(action);
            }
        }
      }
    }

    Transport *transport = dyn_cast<Transport>(this);
    if(transport) //you cannot take action from medic
    { //unit does not have supply, but needs to be healed 
      EntityAIFull *veh = unit->GetVehicle();
      if (!(now && !veh->IsLocal())) 
      {
        bool soldier = unit->IsFreeSoldier();
        const EntityAIType *vehType = veh->GetType();
        bool check =  veh->GetSupply()->Check(veh, this, NULL, 0, now); // check distance

        if (check && vehType->IsEngineer() && soldier)
        {
          float damage =  transport->GetMaxHitCont();
          if (damage > 0.7f)
          {
            UIAction action(new ActionBasic(ATRepairVehicle, this));
            action.position = this->RenderVisualState().PositionModelToWorld(transport->RenderVisualState().Position());
            actions.Add(action);
          }
        }
      }
    }
  }
}

void EntityAIFull::GetActions(UIActions &actions, AIBrain *unit, bool now, bool strict)
{
  if (!unit)
    return;
  base::GetActions(actions,unit,now, strict);
  
#if _VBS2 //convoy trainer 
  // Enable actions from men inside the cargo area!
  if (unit->GetVehicle() == this || (unit->GetPerson() && unit->GetPerson()->IsPersonalItemsEnabled()) )
#else
  if (unit->GetVehicle() == this && !unit->IsInCargo())
#endif
  {
    if (CommanderUnit() == unit)
    {
      bool primGunnerDone = false;
      TurretContext context;
      if (
        (FindTurret(unit->GetPerson(), context) && CanFireUsing(context)) || // controls own weapons
        (primGunnerDone=GetPrimaryGunnerTurret(context))) // controls primary turret
      {
        int selected = context._weapons->ValidatedCurrentWeapon();

        // FIX: less restrictive conditions for switching of weapons
        if (QIsManual() && !IsActionInProgress(MFReload)) // only for player
        {
          // secondary weapons
          int primary = -1;
          bool isPrimaryWeapon = false;
          int primaryMask = IsHandGunSelected() ? MaskSlotHandGun : MaskSlotPrimary;
          for (int i=0; i<context._weapons->_magazineSlots.Size(); i++)
          {
            if (i == selected) continue;
            const MagazineSlot &slot = context._weapons->_magazineSlots[i];
            const MuzzleType *muzzle = slot._muzzle;
            if (!muzzle->_showSwitchAction)
            {
              if (primary < 0)
              {
                primary = i;
                isPrimaryWeapon = (slot._weapon->_weaponType & primaryMask) != 0;
              }
              else if (!isPrimaryWeapon && (slot._weapon->_weaponType & primaryMask) != 0)
              {
                primary = i;
                isPrimaryWeapon = true;
              }
            }
            else
            {
              if (!slot.ShowToPlayer()) continue;
              if (!slot._muzzle->_showEmpty && context._weapons->EmptySlot(slot)) continue;
              if (slot._weapon->GetSimulation() == WSBinocular) continue; // Do not show "Weapon Binoculars" in Action menu
              if (slot._weapon->GetSimulation() == WSCMLauncher) continue; // Do not show "Weapon Flares" in Action menu
              const WeaponModeType *mode = slot._weaponMode;        
              if (mode && mode->_useAction)
              {
                // actions.Add(ATUseWeapon, this, 0.5 - 0.01 * i, i);
              }
              else
              {
                UIAction action(new ActionWeaponIndex(ATSwitchWeapon, this, context._gunner, i));
                action.priority += -0.01 * i;
                actions.Add(action);
              }
            }
          }
          // primary weapon
          if (selected >= 0)
          {
            const MagazineSlot &slot = context._weapons->_magazineSlots[selected];
            const MuzzleType *muzzle = slot._muzzle;
            if (muzzle->_showSwitchAction && primary >= 0)
            {
              // search for primary weapon
              const MagazineSlot &primarySlot = context._weapons->_magazineSlots[primary];
              if (!primarySlot.ShowToPlayer() || !primarySlot._muzzle->_showEmpty && context._weapons->EmptySlot(primarySlot))
              {
                UIAction action(new ActionWeaponIndex(ATHideWeapon, this, context._gunner, -1));
                action.priority += 0.01;
                actions.Add(action);
              }
              else
              {
                UIAction action(new ActionWeaponIndex(ATSwitchWeapon, this, context._gunner, primary));
                action.priority += 0.01;
                actions.Add(action);
              }
            }
          }
        }
        if (EnableWeaponManipulation())
        {
          // use magazines
          AUTO_STATIC_ARRAY(MagazineType *,usedTypes,32);
          for (int i=0; i<context._weapons->_magazines.Size(); i++)
          {
            Magazine *magazine = context._weapons->_magazines[i];
            if (magazine && magazine->GetAmmo() > 0 && magazine->_type.NotNull() && magazine->_type->_useAction)
            {
              // check if this magazine type wasn't processed already
              bool found = false;
              for (int j=0; j<usedTypes.Size(); j++)
                if (magazine->_type == usedTypes[j])
                {
                  found = true; break;
                }

                if (!found)
                {
                  UIAction action(new ActionMagazine(ATUseMagazine, this, context._gunner, magazine->_creator, magazine->_id));
                  action.priority += -0.0001 * i;
                  actions.Add(action);
                  usedTypes.Add(magazine->_type);
                }
            }
          }
          // magazines
          const MuzzleType *currentMuzzle = NULL;
          if (selected >= 0)
          {
            currentMuzzle = context._weapons->_magazineSlots[selected]._muzzle;
            if (CanReloadCurrentWeapon())
            {
              GetReloadActions(this, actions, context, selected, currentMuzzle);
            }
            else
            {
              UIAction action(new ActionWeaponIndex(ATSwitchMagazine, this, context._gunner, selected));
              action.priority += -0.01 * selected;
              actions.Add(action);
            }
          }
          // background reloading
          int iSlot = 0;
          for (int w=0; w<context._weapons->_weapons.Size(); w++)
          {
            const WeaponType *weapon = context._weapons->_weapons[w];
            for (int m=0; m<weapon->_muzzles.Size(); m++)
            {
              const MuzzleType *muzzle = weapon->_muzzles[m];
              if (muzzle != currentMuzzle && muzzle->_backgroundReload)
              {
                GetReloadActions(this, actions, context, iSlot, muzzle);
              }
              iSlot += muzzle->_modes.Size();
            }
          }
        }
        else // !EnableWeaponManipulation()
        {
          if (selected >= 0 && !CanReloadCurrentWeapon())
          {
            UIAction action(new ActionWeaponIndex(ATSwitchMagazine, this, context._gunner, selected));
            action.priority += -0.01 * selected;
            actions.Add(action);
          }
        }
      }

      //if prim. gunner wasn't set in preview block and prim. gunner exist and unit(player) isn't in prim.turret then list prim. gunner weapons too
      if (!primGunnerDone && GetPrimaryGunnerTurret(context) && context._gunner!=unit->GetPerson()) 
      {
        int selected = context._weapons->ValidatedCurrentWeapon();
        const MuzzleType *currentMuzzle = NULL;
        if (selected >= 0)
        {
          currentMuzzle = context._weapons->_magazineSlots[selected]._muzzle;
          if (CanReloadCurrentWeapon())
          {
            GetReloadActions(this, actions, context, selected, currentMuzzle);
          }
          else
          {
            UIAction action(new ActionWeaponIndex(ATSwitchMagazine, this, context._gunner, selected));
            action.priority += -0.01 * (selected+100);
            actions.Add(action);
          }
        }
      }
    }
    else
    {
      // gunner units
      TurretContext context;
      if (FindTurret(unit->GetPerson(), context))
      {
        int selected = context._weapons->ValidatedCurrentWeapon();
        if (EnableWeaponManipulation())
        {
          // magazines
          const MuzzleType *currentMuzzle = NULL;

          if (selected >= 0 && CanReloadCurrentWeapon())
          {
            currentMuzzle = context._weapons->_magazineSlots[selected]._muzzle;
            GetReloadActions(this, actions, context, selected, currentMuzzle);
          }
          // background reloading
          int iSlot = 0;
          for (int w=0; w<context._weapons->_weapons.Size(); w++)
          {
            const WeaponType *weapon = context._weapons->_weapons[w];
            for (int m=0; m<weapon->_muzzles.Size(); m++)
            {
              const MuzzleType *muzzle = weapon->_muzzles[m];
              if (muzzle != currentMuzzle && muzzle->_backgroundReload)
              {
                GetReloadActions(this, actions, context, iSlot, muzzle);
              }
              iSlot += muzzle->_modes.Size();
            }
          }
        }
      }
    }
  }
}

/*!
  \param text displayed text of action
  \param script this script will be performed when action is activated
  \return id of action
*/
int EntityAI::AddUserAction(RString text, RString script, GameValuePar param, float priority, bool showWindow, bool hideOnUse, UserAction shortcut, RString condition)
{
  int index = _userActions.Add();
  UserActionDescription &action = _userActions[index];
  action.id = _nextUserActionId++;
  action.text = text;
  action.script = script;
  action.param = param;
  action.priority = priority;
  action.showWindow = showWindow;
  action.hideOnUse = hideOnUse;
  action.shortcut = shortcut;
  action.condition = condition;
#if USE_PRECOMPILATION
  GGameState.CompileMultiple(condition, action.conditionExp, GWorld->GetMissionNamespace()); // mission namespace
#endif
  return action.id;
}

/*!
  \param id of action
*/
void EntityAI::RemoveUserAction(int id)
{
  for (int i=0; i<_userActions.Size(); i++)
  {
    if (_userActions[i].id == id)
    {
      _userActions.Delete(i);
      return;
    }
  }
}

/*!
  \param id of action
*/
const UserActionDescription *EntityAI::FindUserAction(int id) const
{
  for (int i=0; i<_userActions.Size(); i++)
  {
    if (_userActions[i].id == id)
      return &_userActions[i];
  }
  return NULL;
}

DEFINE_ENUM(EntityEvent,EE,ENTITY_EVENT_ENUM)

DEFINE_ENUM(MPEntityEvent,EE,ENTITY_MP_EVENT_ENUM)

class GameArrayTypeRef
{
  const GameArrayType &_ref;
  public:
  GameArrayTypeRef(const GameArrayType &ref):_ref(ref){}
  operator const GameArrayType &() const {return _ref;}
  operator GameValue() const {return GameValue(_ref);}
};

bool EntityAI::IsEventHandler(EntityEvent event) const
{
  const AutoArray<RString> &handlers = GetEventHandlers(event);
  if (handlers.Size()>0) return true;
  RString handler = GetType()->_eventHandlers[event];
  if (handler.GetLength()>0) return true;
  return false;
}

// Postpone void event handlers evaluation wrapper, used for EH evaluation after simulation
class EventVoidCall: public IPostponedEvent
{
  OLinkO(EntityAI) _entityAI;
  EntityEvent _event;
  const GameValue _value;

public:
  EventVoidCall(EntityAI *entityAI, EntityEvent event, const GameValue &value)
    : _entityAI(entityAI), _event(event), _value(value) 
  {
  }

  void DoEvent() const
  {
    if (_entityAI)
      _entityAI->DoEvent(_event, _value);
  }

  USE_FAST_ALLOCATOR
};

void EntityAI::OnEvent(EntityEvent event, const GameValue &pars)
{
  GWorld->AppendPostponedEvent(new EventVoidCall(this, event, pars));
}

void EntityAI::OnMPEvent(MPEntityEvent event, const GameValue &pars)
{
  const AutoArray<RString> &handlers = GetMPEventHandlers(event);
  for (int i=0; i<handlers.Size(); i++)
  {
    GameVarSpace local(false);
    GameState *gstate = GWorld->GetGameState();
    gstate->BeginContext(&local);
    gstate->VarSetLocal("_this",pars,true);
    RString handler = handlers[i];
    gstate->Execute(handler, GameState::EvalContext::_default, GWorld->GetMissionNamespace()); // mission namespace
    gstate->EndContext();
  }
}

Vector3 EntityAI::OnMPEventRetVector3(MPEntityEvent event, const GameValue &pars)
{
  const AutoArray<RString> &handlers = GetMPEventHandlers(event);
  GameValue last;
  for (int i=0; i<handlers.Size(); i++)
  {
    GameVarSpace local(false);
    GameState *gstate = GWorld->GetGameState();
    gstate->BeginContext(&local);
    gstate->VarSetLocal("_this",pars,true);
    RString handler = handlers[i];
    last = gstate->EvaluateMultiple(handler, GameState::EvalContext::_default, GWorld->GetMissionNamespace()); // mission namespace
    gstate->EndContext();
  }
  if (last.GetNil() || last.GetType() != GameArray) return VZero;

  GameArrayType &vecArray = last;
  if (vecArray.Size() != 3) return VZero;
  Vector3 moveVec(vecArray[0], vecArray[2], vecArray[1]);
  return moveVec;
}
/*!
\patch 1.82 Date 8/20/2002 by Ondra
- New: Addons and scripting: Customizable event handlers for events:
  Killed, Hit, Dammaged, GetIn, GetOut, Init,
  Engine, Gear, Fuel, Fired, IncomingMissile.
*/
void EntityAI::DoEvent(EntityEvent event, const GameValue &pars)
{
  PROFILE_SCOPE_EX(entEH, *);
  if (PROFILE_SCOPE_NAME(entEH).IsActive())
  {
    PROFILE_SCOPE_NAME(entEH).AddMoreInfo(Format("%s:%s", cc_cast(GetDebugName()), cc_cast(FindEnumName(event))));
  }

  const AutoArray<RString> &handlers = GetEventHandlers(event);
  for (int i=0; i<handlers.Size(); i++)
  {
    GameVarSpace local(false);
    GameState *gstate = GWorld->GetGameState();
    gstate->BeginContext(&local);
    gstate->VarSetLocal("_this",pars,true);
    RString handler = handlers[i];
    gstate->Execute(handler, GameState::EvalContext::_default, GWorld->GetMissionNamespace()); // mission namespace
    gstate->EndContext();
  }

  {
    // execute type based event handler
    RString handler = GetType()->_eventHandlers[event];
    GameVarSpace local(false);
    GameState *gstate = GWorld->GetGameState();
    gstate->BeginContext(&local);
    gstate->VarSetLocal("_this",pars,true);
    gstate->Execute(handler, GameState::EvalContext::_default, GWorld->GetMissionNamespace()); // mission namespace
    gstate->EndContext();
  }
}

GameValue EntityAI::OnEventRet(EntityEvent event, const GameValue &pars)
{
  PROFILE_SCOPE_EX(entEH, *);
  if (PROFILE_SCOPE_NAME(entEH).IsActive())
  {
    PROFILE_SCOPE_NAME(entEH).AddMoreInfo(Format("%s:%s", cc_cast(GetDebugName()), cc_cast(FindEnumName(event))));
  }

  GameValue last;
  const AutoArray<RString> &handlers = GetEventHandlers(event);
  {
    // execute type based event handler
    RString handler = GetType()->_eventHandlers[event];
    GameVarSpace local(false);
    GameState *gstate = GWorld->GetGameState();
    gstate->BeginContext(&local);
    gstate->VarSetLocal("_this",pars,true);
    last = gstate->EvaluateMultiple(handler, GameState::EvalContext::_default, GWorld->GetMissionNamespace()); // mission namespace
    gstate->EndContext();
  }
  for (int i=0; i<handlers.Size(); i++)
  {
    GameVarSpace local(false);
    GameState *gstate = GWorld->GetGameState();
    gstate->BeginContext(&local);
    gstate->VarSetLocal("_this",pars,true);
    RString handler = handlers[i];
    last = gstate->EvaluateMultiple(handler, GameState::EvalContext::_default, GWorld->GetMissionNamespace()); // mission namespace
    gstate->EndContext();
  }
  return last;
}


void EntityAI::OnEvent(EntityEvent event, bool par1)
{
  if (!IsEventHandler(event)) return;
  GameValue value = GWorld->GetGameState()->CreateGameValue(GameArray);
  GameArrayType &arguments = value;
  arguments.Add(GameValueExt(this));
  arguments.Add(GameValue(par1));
  OnEvent(event,value);
}

void EntityAI::OnEvent(EntityEvent event, float par1)
{
  if (!IsEventHandler(event)) return;
  GameValue value = GWorld->GetGameState()->CreateGameValue(GameArray);
  GameArrayType &arguments = value;
  arguments.Add(GameValueExt(this));
  arguments.Add(GameValue(par1));
  OnEvent(event,value);
}


void EntityAI::OnEvent(EntityEvent event, RString par1)
{
  if (!IsEventHandler(event)) return;
  GameValue value = GWorld->GetGameState()->CreateGameValue(GameArray);
  GameArrayType &arguments = value;
  arguments.Add(GameValueExt(this));
  arguments.Add(GameValue(par1));
  OnEvent(event,value);
}

void EntityAI::OnEvent(EntityEvent event, RString par1, EntityAI *par2)
{
  if (!IsEventHandler(event)) return;
  GameValue value = GWorld->GetGameState()->CreateGameValue(GameArray);
  GameArrayType &arguments = value;
  arguments.Add(GameValueExt(this));
  arguments.Add(GameValue(par1));
  arguments.Add(GameValueExt(par2));
  OnEvent(event,value);
}

void EntityAI::OnEvent(EntityEvent event, EntityAI *ea2, RString name, float dist, Vector3 where) {
  if (!IsEventHandler(event)) return;
  GameValue value = GWorld->GetGameState()->CreateGameValue(GameArray);
  GameArrayType &arguments = value;
  arguments.Add(GameValueExt(this));
  arguments.Add(GameValueExt(ea2));
  arguments.Add(GameValue(name));
  arguments.Add(GameValue(dist));
  GameValue v2 = GWorld->GetGameState()->CreateGameValue(GameArray);
  GameArrayType &arg2 = v2;
  arg2.Add(GameValue(where[0]));
  arg2.Add(GameValue(where[1]));
  arg2.Add(GameValue(where[2]));
  arguments.Add(GameValue(arg2));
  OnEvent(event,value);
}

void EntityAI::OnEvent(EntityEvent event, RString par1, float par2)
{
  if (!IsEventHandler(event)) return;
  GameValue value = GWorld->GetGameState()->CreateGameValue(GameArray);
  GameArrayType &arguments = value;
  arguments.Add(GameValueExt(this));
  arguments.Add(GameValue(par1));
  arguments.Add(GameValue(par2));
  OnEvent(event,value);
}

float EntityAI::OnEventRetFloat(EntityEvent event, RString par1, float par2, EntityAI *par3, RString par4)
{
  GameValue value = GWorld->GetGameState()->CreateGameValue(GameArray);
  GameArrayType &arguments = value;
  arguments.Add(GameValueExt(this));
  arguments.Add(GameValue(par1));
  arguments.Add(GameValue(par2));
  arguments.Add(GameValueExt(par3));
  arguments.Add(GameValue(par4));
  return OnEventRet(event, value);
}

bool EntityAI::OnEventRetBool(EntityEvent event)
{
  if (!IsEventHandler(event)) return false;

  GameValue value = GWorld->GetGameState()->CreateGameValue(GameArray);
  GameArrayType &arguments = value;
  arguments.Add(GameValueExt(this));
  GameValue ret = OnEventRet(event, value);
  if (ret.GetNil() || ret.GetType() != GameBool) return false;
  return ret;
}

Vector3 EntityAI::OnEventRetVector3(EntityEvent event, EntityAI *par1)
{
  if (!IsEventHandler(event)) return VZero;

  GameValue value = GWorld->GetGameState()->CreateGameValue(GameArray);
  GameArrayType &arguments = value;
  arguments.Add(GameValueExt(this));
  arguments.Add(GameValueExt(par1));
  GameValue ret = OnEventRet(event, value);

  if (ret.GetNil() || ret.GetType() != GameArray) return VZero;

  GameArrayType &vecArray = ret;
  if (vecArray.Size() != 3) return VZero;

  Vector3 moveVec(vecArray[0], vecArray[2], vecArray[1]);

  return moveVec;
}

bool EntityAI::OnEventRetBool(EntityEvent event, EntityAI *par1, bool par2)
{
  if (!IsEventHandler(event)) return false;

  GameValue value = GWorld->GetGameState()->CreateGameValue(GameArray);
  GameArrayType &arguments = value;
  arguments.Add(GameValueExt(this));
  arguments.Add(GameValueExt(par1));
  arguments.Add(GameValue(par2));
  GameValue ret = OnEventRet(event, value);
  if (ret.GetNil() || ret.GetType() != GameBool) return false;
  return ret;
}

void EntityAI::OnEvent(EntityEvent event)
{
  if (!IsEventHandler(event)) return;
  GameValue value = GWorld->GetGameState()->CreateGameValue(GameArray);
  GameArrayType &arguments = value;
  arguments.Add(GameValueExt(this));
  OnEvent(event,value);
}

#if _VBS3
void EntityAI::OnHideEvent(EntityEvent event,EntityAI *par1,Turret *turret)
{
  if(!IsEventHandler(event)) return;

  GameValue value = GWorld->GetGameState()->CreateGameValue(GameArray);
  GameArrayType &arguments = value;
  arguments.Add(GameValueExt(this));
  arguments.Add(GameValueExt(par1));

  GameValue path = GWorld->GetGameState()->CreateGameValue(GameArray);
  GameArrayType &pathA = path;

  if(turret)
  {
    Assert(dyn_cast<Transport>(this));
    Transport *tran = static_cast<Transport*>(this);

    AutoArray<int, MemAllocLocal<int, 16> > path;
    tran->GetTurretPath(path,turret);

    pathA.Resize(path.Size());
    for(int i=0; i < path.Size(); ++i)
      pathA[i]=float(path[i]);
  }

  // Add the path to the turret/none if its a driver.
  arguments.Add(path);
  OnEvent(event,value);
}

void EntityAI::OnAttachToEvent(EntityEvent event,Object *obj,const Vector3 &pos)
{
  if(!IsEventHandler(event)) return;

  GameValue eventArray = GWorld->GetGameState()->CreateGameValue(GameArray);
  GameArrayType &eventArrayType = eventArray;

  GameValue vectorArray = GWorld->GetGameState()->CreateGameValue(GameArray);
  GameArrayType &vectorArrayType = vectorArray;
  vectorArrayType.Add(pos.X());
  vectorArrayType.Add(pos.Z());
  vectorArrayType.Add(pos.Y());

  eventArrayType.Add(GameValueExt(this));
  eventArrayType.Add(GameValueExt(obj));
  eventArrayType.Add(vectorArray);

  OnEvent(event,eventArray);
}


void EntityAI::OnHitPartEvent(EntityEvent event,AutoArray<HitEventElement> &data)
{
  if (!IsEventHandler(event)) return;

  GameValue gvHitEventsOfArray = GWorld->GetGameState()->CreateGameValue(GameArray);
  GameArrayType &gatHitEventsOfArray = gvHitEventsOfArray;

  for( int i = 0; i < data.Size(); ++i)
  {
    HitEventElement &hEvent = data[i];

    GameValue gpos      = GWorld->GetGameState()->CreateGameValue(GameArray);
    GameValue gvel      = GWorld->GetGameState()->CreateGameValue(GameArray);
    GameValue gcomStr   = GWorld->GetGameState()->CreateGameValue(GameArray);
    GameValue gAmmoStat = GWorld->GetGameState()->CreateGameValue(GameArray);
    GameValue gNormal   = GWorld->GetGameState()->CreateGameValue(GameArray);

    GameArrayType &posArgu = gpos;
    posArgu.Add(GameValue(hEvent._pos.X()));
    posArgu.Add(GameValue(hEvent._pos.Z()));
    posArgu.Add(GameValue(hEvent._pos.Y()));

    GameArrayType &velArgu = gvel;
    velArgu.Add(GameValue(hEvent._velocity.X()));
    velArgu.Add(GameValue(hEvent._velocity.Z()));
    velArgu.Add(GameValue(hEvent._velocity.Y())); 

    GameArrayType &normArgu = gNormal;
    normArgu.Add(GameValue(hEvent._normal.X()));
    normArgu.Add(GameValue(hEvent._normal.Z()));
    normArgu.Add(GameValue(hEvent._normal.Y())); 

    GameArrayType &comArgu = gcomStr;
    for( int i = 0; i < hEvent._component.Size(); ++i)
      comArgu.Add(GameValue(hEvent._component[i]));

    GameArrayType &ammoStat = gAmmoStat;
    if(hEvent._shot) // Quick fix, requires looking into how to create a munition ID
    {
      ammoStat.Add(GameValue(hEvent._shot->Type()->hit)); 
      ammoStat.Add(GameValue(hEvent._shot->Type()->indirectHit)); 
      ammoStat.Add(GameValue(hEvent._shot->Type()->indirectHitRange)); 
      ammoStat.Add(GameValue(hEvent._shot->Type()->explosive)); 
    }
    else //use the impulse value as hit
    {
      ammoStat.Add(hEvent._impulse);
      ammoStat.Add(0.0f);
      ammoStat.Add(0.0f);
      ammoStat.Add(0.0f);
    }

    GameValue value = GWorld->GetGameState()->CreateGameValue(GameArray);
    GameArrayType &arguments = value;
    arguments.Add(GameValueExt(this));  // object it hit.
    arguments.Add(GameValueExt(hEvent._shoter));// who fired the shot
    arguments.Add(GameValueExt(hEvent._shot));  // shot
    arguments.Add(gpos);
    arguments.Add(gvel);
    arguments.Add(gcomStr);
    arguments.Add(gAmmoStat);
    arguments.Add(gNormal);
    arguments.Add(hEvent._radius);
    arguments.Add(GameValue(hEvent._surfaceType));
    arguments.Add(hEvent._directHit);

    gatHitEventsOfArray.Add(value);
  }

  OnEvent(event,gatHitEventsOfArray);
}
#endif

void EntityAI::OnEvent(EntityEvent event, EntityAI *par1, float par2)
{
  if (!IsEventHandler(event)) return;
  GameValue value = GWorld->GetGameState()->CreateGameValue(GameArray);
  GameArrayType &arguments = value;
  arguments.Add(GameValueExt(this));
  arguments.Add(GameValueExt(par1));
  arguments.Add(GameValue(par2));
  OnEvent(event,value);
}


void EntityAI::OnEvent(EntityEvent event, EntityAI *par1)
{
  if (!IsEventHandler(event)) return;
  GameValue value = GWorld->GetGameState()->CreateGameValue(GameArray);
  GameArrayType &arguments = value;
  arguments.Add(GameValueExt(this));
  arguments.Add(GameValueExt(par1));
  OnEvent(event,value);
}

void EntityAI::OnEvent(EntityEvent event, EntityAI *par1, EntityAI *par2)
{
  if (!IsEventHandler(event)) return;
  GameValue value = GWorld->GetGameState()->CreateGameValue(GameArray);
  GameArrayType &arguments = value;
  arguments.Add(GameValueExt(this));
  arguments.Add(GameValueExt(par1));
  arguments.Add(GameValueExt(par2));
  OnEvent(event,value);
}

void EntityAI::PerformRelock(AILockerSavedState &state) const
{
  if (_locked)
    _locker->Relock(state);
}

void EntityAI::PerformUnlock(AILockerSavedState *state) const
{
  if (_locked)
    _locker->Unlock(state);
}

void EntityAI::LockPosition()
{
  if (_static)
    return; // no lock for buildings ...

  // helper functions for lock/unlock
  // lock current position and area around it
  float radius=CollisionSize()*1.5f+2.5f;
  saturateMax(radius,4);
  _locked=true;
  // make some shift aside - we prefer having right side free
  // FIX: AimingPosition did not work well in Y axis
  Vector3Val lockPos = FutureVisualState().Position();
  // this also helps getting in (drivers gets in from the left)
  // the only place where Lock is performed (_locked == true)
  bool soldier = CommanderUnit() ? CommanderUnit()->GetPerson() == this : false;
  bool canWait = PilotUnit() && IsAbleToMove() && !IsDamageDestroyed();
  _locker->Lock(lockPos, radius, soldier, canWait, GetShape()->GeometrySphere());
}

void EntityAI::UnlockPosition()
{
  // lock some area around vehicle
  PerformUnlock();
  _locker->Move();
  _locked=false;
}

#if _PROFILE
#pragma optimize("",off)
#endif

void EntityAI::AvoidCollision(float deltaT, float &speedWanted, float &headChange)
{
  PROFILE_SCOPE_EX(entAC,sim);

  AIBrain *agent = PilotUnit();
  if (!agent)
    return;

  const float neutralAside = 0.0f;

  _avoidAsideWanted=neutralAside;

  if (agent->GetPlanningMode() == AIBrain::LeaderDirect) return;
  // if we are stopped, do not try to avoid
  // TODO: some vehicles (esp. men) should go out of way of tanks
  const float maxSpeedStopped=0.05;
  if( fabs(speedWanted)<=maxSpeedStopped ) return;
  // avoid collisions
  float mySize = CollisionSize(); // assume vehicle is not round
  float mySpeedSize = fabs(FutureVisualState().ModelSpeed()[2]);
  float maxSpeed = GetType()->GetMaxSpeedMs();
  float myBrakeDist = Square(mySpeedSize)*0.2;
  float myBrakeTime = mySpeedSize*0.05;
  float myMaxBrakeTime = maxSpeed*0.05;
  float maxDist=floatMax(GetPrecision()*4,5)+myBrakeDist*1.3;
  // check if we are on collision course
  VehicleCollisionBuffer ret;
  float gapFactor=Interpolativ(mySpeedSize,maxSpeed*0.5,maxSpeed,0.5,1);
  float gap=mySize*gapFactor;
  float maxTime=1.5+myBrakeTime*1.3;

  //LogF("%s: Predict gap %.1f, maxDist %.1f",(const char *)GetDebugName(),gap,maxDist);

  GLOB_LAND->PredictCollision(ret,this,maxTime,gap,maxDist);

  // TODO: PredictCollision can be quite expensive
  // as we are merging with _avoid anyway, there is no need to call PredictCollision in each simulation step

  // merge PredictCollision results with _avoid list
  for (int i=0; i<_avoid.Size(); i++)
  {
    const EntityAI *ai = _avoid[i]._who;
    if (_avoid[i]._until<Glob.time || !ai)
    {
      _avoid.DeleteAt(i);
      continue;
    }
    int index=ret.Find(ai);
    if (index>=0) continue;
    // adapted from PredictCollision
    float vRadius=CollisionSize()*0.8;
    float maxDist2=FutureVisualState().Speed().SquareSize()*Square(maxTime);
    saturateMax(maxDist2,Square(maxDist));


    // find analytical solution
    float minTime=NearestPoint(FutureVisualState().Position(),FutureVisualState().Speed(),ai->FutureVisualState().Position(),ai->FutureVisualState().Speed());
    saturate(minTime,0,maxTime);
    Vector3 pt1 = FutureVisualState().Position()+FutureVisualState().Speed()*minTime;
    Vector3 pt2 = ai->FutureVisualState().Position()+ai->FutureVisualState().Speed()*minTime;
    float minDist = pt1.Distance(pt2);

    float cRadius=ai->CollisionSize();

    VehicleCollision &info=ret.Append();
    // calculate collision point
    info.pos = pt2;
    info.who = ai;
    // use at most gap to make sure the collision will not be ignored in the future
    info.time = minTime;
    info.distance = floatMin(minDist-vRadius-cRadius,gap);
    info.xSeparation = (pt2-pt1)*FutureVisualState().DirectionAside();
    
  }

  if (ret.Size()<=0)
    return;
  

  bool iAmMan=agent->IsSoldier();
  bool iAmHeavy = GetMass()>5000;

  AIGroup *myGroup = agent->GetGroup();
  AICenter *myCenter = myGroup ? myGroup->GetCenter() : NULL;

  // precalculate
  float invMaxSpeed=1/maxSpeed;

  saturate(speedWanted,-maxSpeed,maxSpeed);
  Vector3 mySpeed=FutureVisualState().Direction()*speedWanted;

  float maxAvoid=GetPrecision()*0.5f;
  if (iAmMan)
    // man often needs to avoid multiple other mans
    // he may also need to avoid a large vehicle
    saturateMax(maxAvoid,CollisionSize()*2+2.0f);
  else
    saturateMax(maxAvoid,CollisionSize());
  saturate(maxAvoid,1,4); // 4m is enough to avoid on any road with any vehicle
  float wantAvoid=0;

  for( int i=0; i<ret.Size(); i++ )
  {
    const VehicleCollision &info=ret[i];
    const EntityAI *who=info.who;

    // something is near
    // some vehicle
    // determine who should slow down
    // if who is in front of us, slow down to his speed
    // if we are heave and he is enemy soldier, ignore him
    if (iAmHeavy)
    {
      AIBrain *whoUnit = who->CommanderUnit();
      if (whoUnit)
      {
        AIGroup *whoGroup = whoUnit->GetGroup();
        if (whoGroup)
        {
          AICenter *whoCenter = whoGroup->GetCenter();
          if (myCenter->IsEnemy(whoCenter->GetSide()))
            continue;
        }
      }

    }

#if _ENABLE_CHEATS
    if( CHECK_DIAG(DEPath) )
    {
      Ref<Object> obj=new ObjectColored(GScene->Preloaded(SphereModel),VISITOR_NO_ID);
      obj->SetPosition(info.pos);
      Color color(1,1,0,0.1f);
      obj->SetScale(info.distance<gap ? 0.5f : 0.25f);
      obj->SetConstantColor(color);
      GScene->ShowObject(obj);
    }
#endif

    if( info.distance<=gap )
    {
      // check relative speed and position

#if DIAG_COL
      if( this==GWorld->CameraOn() )
        LogF("%s vs %s",(const char *)GetDebugName(),(const char *)who->GetDebugName());
#endif

      Vector3Val v = mySpeed-who->PredictVelocity();
      Vector3Val r = who->FutureVisualState().Position() - FutureVisualState().Position();

      float nearer = 0;
      float actDistance = 0;
      float inbound = 0; // inbound factor: -1 = going straight away, +1 going directly towards a collision
      float timeToCollision = 0; // estimate how long would it take until we touch
      if (r.SquareSize()>0)
      {
        // projection of v to r is how much we are getting nearer
        Vector3 nearerV=v.Project(r);
        // nearer is oriented relative speed size
        Vector3 rNorm = r.Normalized(); 
        nearer = nearerV*rNorm;
        actDistance = r*rNorm;
        inbound = rNorm*v.Normalized();
        timeToCollision = nearer>0 ? actDistance/nearer : FLT_MAX;
      }

      // predicated closest distance after info.time
      float distance=info.distance;
      float isNear;
      if( distance<gap )
        isNear=Interpolativ(distance,0,gap,-maxSpeed*0.5,-maxSpeed*0.1);
      else
        isNear=Interpolativ(distance,gap,gap+myBrakeDist,-maxSpeed*0.1,maxSpeed);
      
      float dirFactor=nearer*invMaxSpeed;

#if DIAG_COL
      if( this==GWorld->CameraOn() )
        LogF("  nearer %.3f, dirFactor %.3f",nearer,dirFactor);
#endif
      if( dirFactor<=-0.1 )
      {
#if DIAG_COL
        if( this==GWorld->CameraOn() )
          LogF("  going away");
#endif
        continue; // safe: going away
      }

      if( Airborne() )
      {
        // check if obstacle is dangerous to airborne vehicle
        float maxY=who->FutureVisualState().Position().Y()+who->GetShape()->Max().Y();
        if( maxY<FutureVisualState().Position().Y()-10 ) continue;
      }

      if (
        who->Static() ||
        // soldier vs. soldier needs always to be handled dynamically
        who->IsLockedPosition() && (!iAmMan || !who->CommanderUnit() || !who->CommanderUnit()->IsFreeSoldier())
      )
      {
        float limSpeed=isNear;
        // static object - should be considered in path planner
        saturateMax(limSpeed,maxSpeed*0.5);
        saturate(speedWanted,-limSpeed,+limSpeed);

#if DIAG_COL
        if( this==GWorld->CameraOn() )
          LogF("  Obj %.1f, idist %.1f, dist %.1f, gap %.1f, brakeDist %.1f",
            speedWanted*3.6,info.distance,distance,gap,myBrakeDist);
#endif
      }
      else
      {
        Vector3Val hisSpeedR=DirectionWorldToModel(who->PredictVelocity());
        float hisSpeedRZ=hisSpeedR.Z();

        // check collision on predicted position

        float t=info.time;
        Vector3 myPos=FutureVisualState().Position()+t*PredictVelocity();
        Vector3 whoPos=who->FutureVisualState().Position()+t*who->PredictVelocity();

        bool dangerous=true;
        AIBrain *whoUnit = who->CommanderUnit();
        if( whoUnit && !whoUnit->IsFreeSoldier() )
        {
          Ref<ObjectVisualState> myTrans,whoTrans;
          myTrans = CloneFutureVisualState();
          myTrans->SetPosition(myPos);

          whoTrans = who->CloneFutureVisualState();
          whoTrans->SetPosition(whoPos);
          // enlarge slightly - be carefull
          myTrans->SetScale(1.1);
          whoTrans->SetScale(1.1);
          
          // do not check collision with men
          // assume collision will happen
          CollisionBuffer objCol;
          Intersect(objCol,const_cast<EntityAI *>(who),*myTrans,*whoTrans);
          dangerous=( objCol.Size()>0 );
        }

        bool sameSubgroup=false;
        if (whoUnit && whoUnit->GetUnit())
        {
          AISubgroup *whoSubgroup=whoUnit->GetUnit()->GetSubgroup();
          AISubgroup *mySubgrp = agent->GetUnit() ? agent->GetUnit()->GetSubgroup() : NULL;
          sameSubgroup = whoSubgroup == mySubgrp;
        }

#if _ENABLE_CHEATS
        if (CHECK_DIAG(DEPath))
        {
          Ref<Object> obj=new ObjectColored(GScene->Preloaded(SphereModel),VISITOR_NO_ID);
          float scale=(maxTime-info.time)/maxTime;
          //float scale=
          saturate(scale,0.1f,0.5f);
          Color color;
          obj->SetPosition(info.pos);
          float danger = -hisSpeedRZ*(1.0f/10);
          saturate(danger,0,1);
          color=Color(1,0,dangerous)*danger+Color(0,1,dangerous)*(1-danger);
          color.SetA(0.1f);
          //color=Color(1,-1,0)*danger+Color(0,1,0);
          obj->SetScale(scale);
          obj->SetConstantColor(color);
          GScene->ShowObject(obj);

          GScene->DrawCollisionStar(myPos,0.1f);
          GScene->DrawCollisionStar(whoPos,0.1f);
        }
#endif  

        // check direction of target
        bool overtaken = false;
        bool overtaking = false;
        float whoSpeedD=who->PredictVelocity()*FutureVisualState().Direction();
        float mySpeedD=PredictVelocity()*FutureVisualState().Direction();
        // in the same subgroup assume collisions are less likely thanks to mutual coordination
        float timeSeparation = sameSubgroup ? myMaxBrakeTime*2.0f : myMaxBrakeTime*4.0f;
        if (iAmMan)
        {
          // man is using a slightly different collision avoidance
          // based on potential fields
          // around each soldier there is an elliptic potential field repulsing any obstacle
          // ellipse two foci: A = the soldier itself, B = where the soldier is moving to
          Vector3Val focusA = FutureVisualState().Position();
          const float predictTime = 2.0f;
          Vector3Val focusB = FutureVisualState().Position()+PredictVelocity()*predictTime;
          // ellipse "diameter" (sum of distances to foci): needed separation value
          float diameter =  who->CollisionSize()+CollisionSize()+focusA.Distance(focusB);
          // how deep is the obstacle in the ellipsis now
          float sumDistToFoci = who->FutureVisualState().Position().Distance(focusA) + who->FutureVisualState().Position().Distance(focusB);
          if (sumDistToFoci<diameter)
          { // obstacle is in the ellipse
            // how much in
            float inFactor = sumDistToFoci / diameter;
            // repulsion direction
            float isAside = PositionWorldToModel(who->FutureVisualState().Position()).X();
            // to prevent discontinuity we avoid +1/-1 type decision
            // if he is left, we want to avoid right
            float avoidThis = inFactor*floatMinMax(-isAside*4.0f,-1,+1);
            
            // analyze the situation: current distance, approach speed
            float collisionFactor = InterpolativC(timeToCollision,timeSeparation*0.1f,timeSeparation,1,0);
            // sum all avoidance impulses
            wantAvoid += avoidThis*collisionFactor;
          } 
        }
        else if (!sameSubgroup)
        {
          // no overtaking/avoiding in same group
          float avoidThis = (who->CollisionSize()+CollisionSize())*0.6;
          Vector3 whoRelPos=PositionWorldToModel(who->FutureVisualState().Position());
          if( fabs(whoRelPos.X())>20 && fabs(whoRelPos.X())>whoRelPos.Z()*0.5 )
            // aside of our direction - do not avoid
            avoidThis = 0;
          else if( FutureVisualState().Direction()*who->FutureVisualState().Direction()<-0.2 )
          {
            // opposite direction - both should avoid
          }
          else if( whoRelPos.Z()<0 && whoRelPos.X()<0 )
            // he's overtaking us
            overtaken = true;
          else if( whoRelPos.Z()>0 && whoRelPos.X()>0 )
            // we're overtaking him
            overtaking = true;
          else if( mySpeedD>5 )
          {
            // I am moving forward
            overtaking = whoSpeedD<mySpeedD;
            overtaken = !overtaking;
          }
          
          if (!dangerous)
          {
            avoidThis *= 0.5;
            saturate(avoidThis,-maxAvoid*0.5,+maxAvoid*0.5);
#if DIAG_COL
            if( this==GWorld->CameraOn() )
              LogF("  not dangerous");
#endif
          }

          if (overtaking)
          {
            // overtake - same direction
            // positive separation is good - this means he is on our left
            // assume we already do avoid to some degree (as indicated by _avoidAside)
            // 
            float timeFactor = InterpolativC(info.time,1.0f,3.5f,1,0);
            float wantAvoidThis = _avoidAside-(avoidThis-floatMin(+info.xSeparation,avoidThis))*timeFactor;
            // if there is a different direction avoidance, let it take precedence
            if (wantAvoid<=0 && wantAvoidThis<wantAvoid) wantAvoid = wantAvoidThis;
#if DIAG_COL
            if( this==GWorld->CameraOn() )
              LogF("  overtaking %.2f",wantAvoid);
#endif
          }
          else
          { // avoid - different direction, or being overtaken
            // negative separation is good - this means he is on our right
            // assume we already do avoid to some degree (as indicated by _avoidAside)
            float timeFactor = InterpolativC(info.time,1.0f,3.5f,1,0);
            float wantAvoidThis = _avoidAside+(avoidThis-floatMin(-info.xSeparation,avoidThis))*timeFactor;
            if (wantAvoidThis>wantAvoid) wantAvoid = wantAvoidThis;
#if DIAG_COL
            if( this==GWorld->CameraOn() )
              LogF("  overtaken %.2f",wantAvoid);
#endif
          }
        } //if( mySubgrp!=whoSubgrp )

        // check if he is behind us

        Vector3 whoRelPos=PositionWorldToModel(who->FutureVisualState().Position());
        if( whoRelPos.Z()<0 )
        {
          // he's behind us
          // do not brake - it would be only worse
          if( speedWanted>0 )
          {
#if DIAG_COL
            if( this==GWorld->CameraOn() )
              LogF("  behind us");
#endif
            continue;
          }
        }
        else if( hisSpeedRZ>maxSpeed*0.1 )
        {
          // he is is front of us - going forward
          // we can limit speed to his speed
          float curDist=who->FutureVisualState().Position().Distance(FutureVisualState().Position());
          float maxNear=mySize*2+myBrakeDist*0.6;
          float curNear=(curDist>1000*maxNear ? 1000 : curDist/maxNear)-1;
          if (!dangerous)
            // he is not dangerous - move
            saturateMax(curNear,0.1*maxSpeed);
          saturateMin(isNear,curNear);

          //LogF("%s: in front of us %.2f",(const char *)GetDebugName(),curNear);
        }

        if (who->PredictVelocity().SquareSize()<Square(2))
        {
          // he is stopped - we have to move
          if (!sameSubgroup)
            saturateMax(isNear,0.1*maxSpeed);
        }

        {
          float limSpeed=floatMax(isNear+hisSpeedRZ,0);
          // check relative position of collision
          //Vector3Val relPos=PositionWorldToModel(pos);
          float carelessSpeed=hisSpeedRZ+maxSpeed*0.3;
          //saturateMax(limSpeed,GetType()->GetMaxSpeedMs()*0.05);
          //saturateMax(limSpeed,0);
          limSpeed=Interpolativ(dirFactor,0,0.1,carelessSpeed,limSpeed);
          // moving vehicle very near - slow down
          if (!whoUnit)
          {
            // vehicle empty - should be considered in path planner
            limSpeed=carelessSpeed;
          }
          else
          {
            float limSpeedMin = 0;
            if (iAmMan && whoUnit->IsSoldier())
              limSpeedMin = Interpolativ(timeToCollision,timeSeparation*0.1f,timeSeparation,1,maxSpeed);
            else if( who->IsOnRoadMoving(1.0f) && IsOnRoadMoving(1.0f) )
            {
              // if both are moving and on road, there is no need to stop
              if (overtaking)
                limSpeedMin = floatMax(hisSpeedRZ+maxSpeed*0.2,maxSpeed*0.5f);
              else
                limSpeedMin = floatMax(hisSpeedRZ,0) + (dangerous ? 6 : 12);
              // if we are heading same direction
              // we will overtake and should not brake under his speed + some reserve
            }
            else
              // stop if necessary
              limSpeedMin = floatMax(hisSpeedRZ,0) + (dangerous ? 0 : 6);
            saturateMax(limSpeed,limSpeedMin);
          }

          // add into the avoid list as well
          //int index = _avoid.FindOrAdd(who);
          //_avoid[index]._until = Glob.time+4; // make sure it is considered even when its effects have already expired
          
          // keep braking for some time
          if( Glob.time>_avoidSpeedTime || _avoidSpeed>=limSpeed )
          {
            _avoidSpeedTime = Glob.time+2;
            _avoidSpeed = limSpeed;
#if DIAG_COL
            if( this==GWorld->CameraOn() )
              LogF("  limit speed %.1f",limSpeed);
#endif
          }
          saturate(speedWanted,-limSpeed,+limSpeed);
        }
        
#if DIAG_COL
        if (this==GWorld->CameraOn())
          LogF("  Col %.1f dist %.1f, gap %.1f, brakeDist %.1f, dir %.2f",
            speedWanted*3.6,distance,gap,myBrakeDist,dirFactor);
#endif
      }
    } // if( info.distance<=gap )
    else
    {
#if DIAG_COL
      if (this==GWorld->CameraOn())
        LogF("%s vs %s ignored (%.2f>%.2f)",
          (const char *)GetDebugName(),(const char *)who->GetDebugName(),
          info.distance,gap);
#endif
    }
  } // for(i)

  saturate(wantAvoid,-maxAvoid,+maxAvoid);
  _avoidAsideWanted=wantAvoid+neutralAside;

  if (fabs(speedWanted)<maxSpeedStopped || _objectContact)
    CreateFreshPlan();
}

#if _PROFILE
#pragma optimize("",on)
#endif

void EntityAI::CreateFreshPlan()
{
  // do not report in DirectGo mode
  AIBrain *unit = PilotUnit();
  if (!unit) return;
  if (unit->GetPlanningMode() == AIBrain::LeaderDirect) return;
  Path &path=unit->GetPath();
  if( path.Size()>=2 )
  {
    // check if we have recent path
    if (path.GetSearchTime()<Glob.time-5 && unit->GetState() != AIBrain::Planning)
    {
      // re-plan
      if (unit->GetFormationLeader() == unit)
        unit->OnStepTimedOut();
      else
        unit->ForceReplan();
    }
  }
}

float EntityAI::DistanceXZFromPath() const
{
  // calculate point on trajectory in time (relative in sec)
  // estimate position after time
  AIBrain *unit = PilotUnit();
  if (!unit)
    return 0;
  Path &path=unit->GetPath();
  // no path - no steering
  if (path.Size()<1)
    return 0;
  // we might consider better calculation, which would handle first and last segments as half-infite
  // however, when moving somewhere where path is not planned we should be careful anyway
  const VisualState &vs = FutureVisualState();
  Vector3Val pos=path.NearestPos(vs.Position());
  return pos.DistanceXZ(vs.Position());
}


float EntityAI::DistanceXFromPath() const
{
  // calculate point on trajectory in time (relative in sec)
  // estimate position after time
  AIBrain *unit = PilotUnit();
  if (!unit)
    return 0;
  Path &path=unit->GetPath();
  // no path - no steering
  if (path.Size()<1)
    return 0;
  // we might consider better calculation, which would handle first and last segments as half-infite
  // however, when moving somewhere where path is not planned we should be careful anyway
  const VisualState &vs = FutureVisualState();
  Vector3Val pos=path.NearestPos(vs.Position());
  return (vs.Position()-pos)*vs.DirectionAside();
}

Vector3 EntityAI::SteerPoint(float spdTime, float costTime, Ref<PathAction> *action, OLink(Object) *building, bool updateCost)
{
  // calculate point on trajectory in time (relative in sec)
  // estimate position after time
  AIBrain *unit = PilotUnit();
  if( !unit ) return FutureVisualState().Position();
  Path &path=unit->GetPath();
  // no path - no steering
  if( path.Size()<2 ) return FutureVisualState().Position()+FutureVisualState().Direction();
  // predict simulation result
  Vector3Val sPos=FutureVisualState().Position()+FutureVisualState().Speed()*spdTime+0.5*spdTime*spdTime*FutureVisualState()._acceleration;
  // find cost corresponding to the simulated result
  float cost=path.CostAtPos(sPos, updateCost);
  if (updateCost)
    // set min. cost to prevent steer point going back from a position it has already reached
    path.SetMinCost(cost);

  // steer point needs to be at least radius of the vehicle away, otherwise it is useless
  // min. distance - make dependent on precision and object radius
  float minDist = floatMax(GetRadius()*0.5f,GetPrecision()*0.1f);
  Vector3Val pos=path.PosAtCost(cost+costTime,minDist,sPos,cost,action,building);

  return pos;
}

float EntityAIType::GetPathCost( const GeographyInfo &info, float dist, CombatMode mode ) const
{
  // note: we are not passing mode into GetCost - the argument is used only for searching cover
  float cost=GetFieldCost(info,mode)*GetBaseCost(info, false, true);
  if( cost>GET_UNACCESSIBLE ) cost=GetMinCost()*2;
  return cost*dist;
}


void EntityAIType::GetPathPrecision(float speedAtCost, float &safeDist, float &badDist) const
{
  safeDist = OperItemGrid*0.5f;
  badDist = OperItemGrid*1.5f;
}

float EntityAI::GetFieldCost( const GeographyInfo &info) const
{
  AIBrain *unit = PilotUnit();
  CombatMode cm = unit ? unit->GetCombatMode() : CMSafe;
  return GetType()->GetFieldCost(info,cm);
}

float EntityAI::VisibleMovementNoSize() const
{
  float camouflage=GetType()->_camouflage;
  float vis=camouflage;

  float rSpeed=fabs(FutureVisualState().ModelSpeed().Z())*3;
  if( rSpeed>vis*GetRadius() )
  { // moving target is more visible
    float relSpeed=rSpeed/GetRadius();
    saturateMin(relSpeed,8);
    saturateMax(vis,relSpeed*camouflage);
  }
  return vis;
}

float EntityAI::VisibleMovement() const
{
  return EntityAI::VisibleMovementNoSize();
}

float EntityAIFull::VisibleMovement() const
{
  float vis=base::VisibleMovement();
  saturateMax(vis,_shootVisible); // firing target is more visible
  return vis;
}

float EntityAIFull::VisibleMovementNoSize() const
{
  float vis=base::VisibleMovementNoSize();
  saturateMax(vis,_shootVisible); // firing target is more visible
  return vis;
}

float EntityAI::Audible() const
{
  static float offCoef = 0.1f; // engine off
  static float stopCoef = 0.75f; // engine on, but vehicle not moving
  static float onCoef = 1.5f; // engine on, vehicle moving
  static float moveCoef = 0.6f;
  static float moveMaxCoef = 4.0f;
  float radius = GetRadius();
  float aud = EngineIsOn() ? stopCoef : offCoef;
  float rSpeed=fabs(FutureVisualState().ModelSpeed().Z());
  if( rSpeed>radius*0.1f )
  { // moving target is more audible
    aud = onCoef + floatMin(rSpeed*moveCoef/radius,moveMaxCoef);
  }
  aud *= GetType()->GetAudible();
  return aud;

}

float EntityAIFull::Audible() const
{
  float aud = base::Audible();
  saturateMax(aud,_shootAudible); // firing target is more audible
  return aud;
}

float EntityAI::GetExplosives() const
{
  float expl=GetType()->_secondaryExplosion;
  if( expl>=0 ) return expl;
  // how much explosives is in
  // how much explosives is in
  // convert fuel and ammo to common value
  const float AmmoCostToExplosion=0.05;
  //const float IAmmoCostToExplosion=0.01;
  const float FuelCostToExplosion=0.10;
  // estimate explosiveness from cost
  float explosion = 0;
  // calculate total fuel load
  float fuelExplosion = (GetFuel()+GetFuelCargo())*FuelCostToExplosion;
  // calculate total ammo load
  float cargoExplosion = GetAmmoCargo()*AmmoCostToExplosion;
  //LogF("Ammo explosion %.1f",explosion);
  //LogF("Fuel explosion %.1f",fuelExplosion);
  //LogF("Ammo cargo explosion %.1f",cargoExplosion);
  explosion += fuelExplosion;
  explosion += cargoExplosion;
  // multiply result by config factor
  return explosion*fabs(expl);
}

float EntityAIFull::GetExplosives() const
{
  float expl=GetType()->_secondaryExplosion;
  if( expl>=0 ) return expl;
  float explosion = GetAmmoHit()*0.015f; // ammunition
  float explo = base::GetExplosives();
  explo += explosion*fabs(expl);
  return explo;
}

RString EntityAI::HitpointName(int i) const
{
  const HitPointList &hitpoints=GetType()->GetHitPoints();
  if (i>=hitpoints.Size()) return RString();
  if (i<0) return RString();
  int sel = hitpoints[i].GetSelection();
  LODShape *lShape = GetShape();
  if (!lShape) return RString();

  if (sel < 0)
    // hitpoint may be attached to no selection
    return RString();
  
  const Shape *hits = lShape->HitpointsLevel();
  if (!hits) return RString();
  const NamedSelection &nsel = hits->NamedSel(sel);
  return nsel.GetName();
}

float EntityAI::DirectLocalHit(DoDamageResult &result, int component, float val)
{
#if ENABLE_ALLOW_DAMAGE
  if (!_allowDamage) return 0;
#endif
  return base::DirectLocalHit(result, component, val);
}

float EntityAI::LocalHit( DoDamageResult &result, Vector3Par pos, float val, float valRange )
{
#if ENABLE_ALLOW_DAMAGE
  if (!_allowDamage) return 0;
#endif
  return base::LocalHit(result, pos, val, valRange);
}

class FindCeaseFireMessage
{
protected:
  AIBrain *_to;

public:
  FindCeaseFireMessage(AIBrain *to) {_to = to;}
  bool operator()(RadioChannel *channel, RadioMessage *msg, bool actual) const
  {
    if (msg->GetType() != RMTCeaseFire) return false;
    Assert(dynamic_cast<RadioMessageCeaseFire *>(msg));
    RadioMessageCeaseFire *msgTyped = static_cast<RadioMessageCeaseFire *>(msg);
    Assert(msgTyped);
    return msgTyped->GetTo() == _to;
  }
};

static bool FindCeaseFireInRadio(RadioChannel &radio, AIBrain *to)
{
  FindCeaseFireMessage func(to);
  return radio.ForEachMessage(func);
}

float EntityAI::HandleDamage(int part, float val, EntityAI *owner, RString ammo, bool &handlerCalled)
{
  if (!IsEventHandler(EEHandleDamage))
  {
    handlerCalled = false;
    return val;
  }
  
  RString name;
  if (part>=0)
  {
    name = HitpointName(part);
    // prevent empty name, as it has a special meaning (total damage)
    if (name.GetLength()<=0) name = "?";
  }
  handlerCalled = true;
  return OnEventRetFloat(EEHandleDamage, name, val, owner, ammo);
}

void EntityAI::ShowDamage(int part, EntityAI *killer)
{
  if (part<0 || part>=_hit.Size())
    return;
  RString name = HitpointName(part);
  if (name.GetLength()>0)
    OnEvent(EEDammaged, name, GetHitCont(part));
  base::ShowDamage(part, killer);
}

void EntityAI::HitBy( EntityAI *killer, float howMuch, RString ammo, bool wasDestroyed)
{
#if _VBS2
  if (IsLocal() && howMuch >= 0.05 && killer && !wasDestroyed)
  {
    GStats.OnVehicleDamaged(this, killer, howMuch, ammo);
    GetNetworkManager().OnVehicleDamaged(this, killer, howMuch, ammo);
  }
#endif

  if (IsLocal() && howMuch >= 0.05 && !wasDestroyed)
  {
    const AutoArray<RString> &handlers = GetMPEventHandlers(EEMPHit);
    if (handlers.Size()>0) //there are some event handlers => trigger the MP Event on all clients
    {
      GameValue value = GWorld->GetGameState()->CreateGameValue(GameArray);
      GameArrayType &arguments = value;
      arguments.Add(GameValueExt(this));
      arguments.Add(GameValueExt(killer));
      arguments.Add(GameValue(howMuch));
      GetNetworkManager().SendMPEvent(EEMPHit, value);
      // and triger the MP Event here
      OnMPEvent(EEMPHit, value);
    }
    OnEvent(EEHit,killer,howMuch);
  }
  base::HitBy(killer,howMuch,ammo,wasDestroyed);
  AIGroup *g=GetGroup();
  EntityAIFull *killerVehicle = NULL;
  if( g && killer )
  {
    AIBrain *killerUnit = killer->CommanderUnit();
    if (killerUnit) killerVehicle = killerUnit->GetVehicle();
  }

  if (howMuch>0)
  {
    ReactToDamage();
    if (!_isDead && killerVehicle)
    {
      AIBrain *brain = CommanderUnit();
      if (brain && !brain->IsAnyPlayer() && brain->LSIsAlive())
      {
        // if we are dead, we cannot reveal anyone any longer
        // reveal killer (at random position around us)
        Vector3 pos=FutureVisualState().Position();
        float accuracy = 0.1;
        float sideAcc = Target::GetSideAccuracyExact();
        float delay = 0.8f*brain->GetInvAbility(AKSpotTime);
        float delayOtherUnits = 15;
        // reveal side of killer
        // reporting units knows almost immediately about the target
        // other units will notice it after a short delay
        brain->AddTarget(killerVehicle,accuracy,sideAcc,delayOtherUnits,&pos,brain,delay);
      }
    }
  }

  if( g && killer )
  {
    AIBrain *killerUnit = killer->CommanderUnit();
    if (!killerUnit) return;
    
    AIGroup *killerGroup = killerUnit->GetGroup();
    // if we are damaged by other unit of the same group,  do not react
    if (!killerGroup) return;
    AICenter *killerCenter = killerGroup->GetCenter();
    if (!killerCenter) return;
    if (killerGroup == g && !IsRenegade())
    {
      AIUnit *sender = CommanderUnit() ? CommanderUnit()->GetUnit() : NULL;
      if (howMuch >= 0.05 && killer != this && killerVehicle != this &&
        (!sender || sender->GetVehicleIn() != killerVehicle))
      {
        Log("Friendly fire (in group): %s by %s (%s)",
          (const char *)GetDebugName(),
          (const char *)killer->GetDebugName(),
          (const char *)killer->GetType()->GetName());
        RadioChannel &radio = g->GetRadio();
        if (!FindCeaseFireInRadio(radio, killerUnit))
        {
          if (!sender || sender->GetPerson()->IsDamageDestroyed())
            sender = g->Leader();
          if (sender && !sender->GetPerson()->IsDamageDestroyed() && !sender->IsAnyPlayer())
            radio.Transmit(new RadioMessageCeaseFire(sender, killerUnit, true));
        }
      }
      return;
    }

    // do not disclose when killer is friendly
    AICenter *gCenter = g->GetCenter();
    if (!gCenter)
      return;
    if (gCenter->IsEnemy(killerCenter->GetSide()))
    {
      AIBrain *vehBrain=CommanderUnit();
      if (vehBrain)
      {
        vehBrain->Disclose(DCHit, killer->FutureVisualState().Position(), killer);
        if (PilotUnit() && PilotUnit() != vehBrain)
          PilotUnit()->Disclose(DCHit, killer->FutureVisualState().Position(), killer);
        if (GunnerUnit() && GunnerUnit() != vehBrain)
          GunnerUnit()->Disclose(DCHit, killer->FutureVisualState().Position(), killer);
      }
    }
    else if (gCenter == killerCenter && !IsRenegade())
    {
      AIUnit *sender = CommanderUnit() ? CommanderUnit()->GetUnit() : NULL;
      if (howMuch >= 0.05 && killer != this && killerVehicle != this &&
        (!sender || sender->GetVehicleIn() != killerVehicle))
      {
        Log("Friendly fire: %s by %s (%s), dist %.1f",
          (const char *)GetDebugName(),
          (const char *)killer->GetDebugName(),
          (const char *)killer->GetType()->GetName(),
          killer->FutureVisualState().Position().Distance(FutureVisualState().Position()));
        RadioChannel &radio = gCenter->GetSideRadio();
        if (!FindCeaseFireInRadio(radio, killerUnit))
        {
          if (!sender || sender->GetPerson()->IsDamageDestroyed()) sender = g->Leader();
          if (sender && !sender->GetPerson()->IsDamageDestroyed() && !sender->IsAnyPlayer())
            radio.Transmit(new RadioMessageCeaseFire(sender, killerUnit, false));
        }
      }
    }
  }
}

bool EntityAI::HandleHeal(EntityAI *medic, bool attendant, bool &handlerCalled)
{
  if (!IsEventHandler(EEHandleHeal))
  {
    handlerCalled = false;
    return false;
  }
  handlerCalled = true;
  return OnEventRetBool(EEHandleHeal, medic, attendant);
}



void EntityAI::OnIncomingMissile(EntityAI *target, Shot *shot, EntityAI *owner)
{
  Missile *missile = dyn_cast<Missile>(shot);
  if(missile)
  {
    if (target && target->IsEventHandler(EEIncomingMissile))
      target->OnEvent(EEIncomingMissile,shot->Type()->GetName(),this);
  }
}

void EntityAI::OnNewTarget(Target *target, Time knownSince)
{
  // let us change the fire decision once the target is known
  TurretContext ctx;
  if (GetPrimaryGunnerTurret(ctx))
  {
    //LogF("+++ %s set eval target %s to  %.2f",cc_cast(GetDebugName()),cc_cast(target->GetDebugName()),knownSince.toFloat());
    // TODO: sometimes we may want to delay _nextTargetAquire etc. a little to align with the time when target will be revealed
    if (ctx._weapons->_fire._nextTargetAquire>knownSince) ctx._weapons->_fire._nextTargetAquire = knownSince;
    if (ctx._weapons->_fire._nextWeaponSwitch>knownSince) ctx._weapons->_fire._nextWeaponSwitch = knownSince;
    if (ctx._weapons->_fire._nextTargetChange>knownSince) ctx._weapons->_fire._nextTargetChange = knownSince;
  }
}


void EntityAI::LocalDamageMyself(Vector3Par modelPos, float val, float valRange)
{
  // if the vehicle has a driver, he is responsible for the crash
  AIBrain *brain = PilotUnit();
  EntityAI *responsible = (brain && brain->GetPerson()) ? brain->GetPerson() : this;
  DoAssert(IsLocal());
  Vector3 originDir = FutureVisualState().DirectionModelToWorld(modelPos);
  DoDamageResult result;
  DoDamage(result, modelPos, val, valRange, RString(), originDir);
  ApplyDoDamageLocalHandleHitBy(result,responsible,RString());
}

void EntityAI::ApplyDoDamage(const DoDamageResult &result, EntityAI *owner, RString ammo)
{
  if (owner && !IsDamageDestroyed())
  {
    // if there is a recent information about being damaged by somebody else,
    // ignore self damage
    if (owner!=this || _lastDammageTime<Glob.time-60)
    {
      _lastDammage = owner;
      _lastDammageTime = Glob.time;
    }
  }
  base::ApplyDoDamage(result,owner,ammo);
}

Time EntityAI::GetDestroyedTime() const
{
  if (!_isDead)
    return TIME_MAX;
  // if kill time is not known, assume it was killed recently
  if (_whenDestroyed>Glob.time)
    _whenDestroyed = Glob.time;
  return _whenDestroyed; // helpers for suspending dead body after a while
}

void EntityAI::SetTotalDamageHandleDead(float value)
{
  base::SetTotalDamageHandleDead(value);
  ReactToDamage();
  _isDead = IsDamageDestroyed();
  if (_isDead && (_whenDestroyed>Glob.time))
    _whenDestroyed = Glob.time;
}

void EntityAI::Destroy(EntityAI *killer, float overkill, float minExp, float maxExp)
{
  if (IsLocal())
  {
    const AutoArray<RString> &handlers = GetMPEventHandlers(EEMPKilled);
    if (handlers.Size()>0) //there are some event handlers => trigger the MP Event on all clients
    {
      GameValue value = GWorld->GetGameState()->CreateGameValue(GameArray);
      GameArrayType &arguments = value;
      arguments.Add(GameValueExt(this));
      arguments.Add(GameValueExt(killer));
      GetNetworkManager().SendMPEvent(EEMPKilled, value);
      // and trigger the MP Event here
      OnMPEvent(EEMPKilled, value);
    }
    OnEvent(EEKilled,killer);
    GStats.OnVehicleDestroyed(this, killer);
    GetNetworkManager().OnVehicleDestroyed(this, killer);
  }

  // other units which have seen me being killed might want to react as well
  GWorld->OnKill(killer,this);

  base::Destroy(killer,overkill,minExp,maxExp);
  if (_whenDestroyed>Glob.time)
    _whenDestroyed = Glob.time;

  if (killer)
  {
    // use Entity member to get original target side
    // all dead bodies are considered civilian
    TargetSide origSide = Entity::GetTargetSide();

    // increase killer's experience
    AIBrain *kBrain=killer->CommanderUnit();
    if (kBrain)
    {
      kBrain->IncreaseExperience(*GetType(),origSide);
      // send radio message
      if (kBrain->IsEnemy(origSide))
      {
        // find corresponding target
        Target *tar = kBrain->FindTargetAll(this);
        if (tar)
        {
          // mark killer
          // when destroyed will be set, it will be marked for reporting
          tar->MarkKiller(killer);
        }
      }
    }
    if (killer->GunnerUnit() && killer->GunnerUnit() != kBrain)
      killer->GunnerUnit()->IncreaseExperience(*GetType(),origSide);
    if (killer->PilotUnit() && killer->PilotUnit() != kBrain)
      killer->PilotUnit()->IncreaseExperience(*GetType(),origSide);
  }
}


class FindMagazineByIdFunc : public ITurretFunc
{
protected:
  const Magazine *&_magazine; // out
  int _creator;
  int _id;

public:
  FindMagazineByIdFunc(const Magazine *&magazine, int creator, int id)
    : _magazine(magazine), _creator(creator), _id(id) {}
  bool operator () (EntityAIFull *entity, TurretContext &context)
  {
    for (int i=0; i<context._weapons->_magazines.Size(); i++)
    {
      const Magazine *magazine = context._weapons->_magazines[i];
      if (!magazine) continue;
      if (magazine->_creator == _creator && magazine->_id == _id)
      {
        _magazine = magazine;
        return true; // found
      }
    }
    return false; // continue
  }
};

/*!
\param creator player id of client where magazine was created
\param id unique id of magazine on given client
\return pointer to magazine or NULL when not found
*/
const Magazine *EntityAIFull::FindMagazine(int creator, int id) const
{
  const Magazine *mag = base::FindMagazine(creator,id);
  if (mag)
    return mag;
  const Magazine *magazine = NULL;
  FindMagazineByIdFunc func(magazine, creator, id);
  ForEachTurret(func);
  return magazine;
}

class FindMagazineByTypeFunc : public ITurretFunc
{
protected:
  const Magazine *&_magazine; // out
  int *_count; // out
  const MagazineType *_type;
  int _ammo;

public:
  FindMagazineByTypeFunc(const Magazine *&magazine, int *count, const MagazineType *type)
    : _magazine(magazine), _count(count), _type(type), _ammo(0) {}
  bool operator () (EntityAIFull *entity, TurretContext &context)
  {
    for (int i=0; i<context._weapons->_magazines.Size(); i++)
    {
      const Magazine *magazine = context._weapons->_magazines[i];
      if (!magazine)
        continue;
      if (magazine->_type != _type)
        continue;
      if (_count && magazine->GetAmmo() > 0)
        (*_count)++;
      if (magazine->GetAmmo() > _ammo)
      {
        _magazine = magazine;
        _ammo = magazine->GetAmmo();
      }
    }
    return false; // continue
  }
};

const Magazine *EntityAIFull::FindMagazine(RString name, int *count) const
{
  const Magazine *magazine = base::FindMagazine(name, count);
  if (magazine)
    return magazine;
  if (count)
    *count = 0;
  Ref<MagazineType> type = MagazineTypes.New(name);
  FindMagazineByTypeFunc func(magazine, count, type);
  ForEachTurret(func);
  return magazine;
}

/*!
\param s index of slot reload to
\param m index of reloading magazine
\param afterAnimation true if reload immediately (no delay)
*/
bool EntityAIFull::ReloadMagazineTimed(WeaponsState &weapons, int s, int m, bool afterAnimation)
{
  if (m < 0 || m >= weapons._magazines.Size() || s < 0 || s >= weapons._magazineSlots.Size())
    return false;
  Magazine *magazine = weapons._magazines[m];
  if (!magazine)
    return false;

#if 0
  LogF("%s: ReloadMagazineTimed slot %d, magazine %s",
    (const char *)GetDebugName(),s,
    (const char *)magazine->_type->GetName());
#endif

  const MagazineSlot &slot = weapons._magazineSlots[s];
  if (slot._magazine == magazine) return false; // do not reload with the same magazine
  const MuzzleType *muzzle = slot._muzzle;
  if (!muzzle->CanUse(magazine->_type))
  {
    RptF("Cannot use magazine %s in muzzle %s",
      (const char *)magazine->_type->GetName(),
      (const char *)muzzle->GetName());
    return false;
  }

  // destroy empty magazine
  Magazine *oldMagazine = slot._magazine;
  if (oldMagazine && oldMagazine->GetAmmo() == 0)
    if(CommanderUnit() && CommanderUnit()->IsFreeSoldier())
      weapons.RemoveMagazine(this, oldMagazine);

  // prepare magazine
  magazine->_reloadMagazine = afterAnimation ? 0 :
    muzzle->_magazineReloadTime *
#if !_VBS3 
    // ignore skill when reloading vehicle weapons. personal weapons use animation
    // M1A1 had very long reload times when commander wasn't skilled
    GetInvAbility(AKReloadSpeed) *
#endif
    GRandGen.PlusMinus(1, 0.2);
  // remember the total time so that controller may return 0..1
  magazine->_reloadMagazineTotal = magazine->_reloadMagazine;

  // vary reload time depending on skill in range 0..2
#if _VBS3
  float reloadAbility = 1.0f;
#else
  float reloadAbility = (GetInvAbility(AKReloadSpeed)-1)*0.25+1;
#endif
  // compress reload ability to range 1..2

  magazine->_reload = 1;
  magazine->_reloadDuration = reloadAbility * GRandGen.PlusMinus(1, 0.1);

  LogF("ReloadMagazine %.2f, coef %.2f", magazine->_reloadMagazine, magazine->_reload);

  // change in all slots with the current muzzle
  for (int j=0; j<weapons._magazineSlots.Size(); j++)
  {
    MagazineSlot &slot = weapons._magazineSlots[j];
    if (slot._muzzle == muzzle)
      slot._magazine = magazine;
  }
  return true;
}


/*!
\param slotIndex magazine slot to reload
*/
bool EntityAIFull::ReloadMagazine(WeaponsState &weapons, int slotIndex, bool loaded)
{
  const MagazineSlot &slot = weapons._magazineSlots[slotIndex];
  const MuzzleType *muzzle = slot._muzzle;
  Magazine *oldMagazine = slot._magazine;
  const MagazineType *oldMagazineType = oldMagazine ? oldMagazine->_type : NULL;
  int iMagazine = weapons.FindMagazineByType(muzzle, oldMagazineType);
  if (iMagazine<0)
    return false;
  if (loaded)
  {
    if (ReloadMagazineTimed(weapons, slotIndex, iMagazine, true))
    {
      // load not only the magazine, but also the bullet
      if (slot._magazine)
        slot._magazine->_reload = 0;
      return true;
    }
    return false;
  }
  else
    return ReloadMagazine(weapons, slotIndex, iMagazine);
}

/*!
\param weapon magazine slot index
\param muzzle reloading muzzle
*/
void EntityAIFull::PlayReloadMagazineSound(const TurretContext &context, int weapon, const MuzzleType *muzzle)
{
  const SoundPars &pars = muzzle->_reloadMagazineSound;
  if (pars.name.GetLength() > 0)
  {
    float rndFreq = GRandGen.RandomValue() * 0.1 + 0.95;
    AbstractWave *wave = GSoundScene->OpenAndPlayOnce(pars.name, this, true, GetWeaponSoundPos(context, weapon), FutureVisualState().Speed(),
      pars.vol, pars.freq * rndFreq, pars.distance);
    if (wave)
    {
      GSoundScene->SimulateSpeedOfSound(wave);
      GSoundScene->AddSound(wave);
      context._weapons->_reloadMagazineSound = wave;
    }
  }
}

/*!
\param weapon magazine slot index
\patch 1.24 Date 09/26/2001 by Ondra
- Fixed: Speed of sound delay was missing on weapon firing sound.
*/
void EntityAIFull::PlayEmptyMagazineSound(const TurretContext &context, int weapon)
{
  const MuzzleType *muzzle = context._weapons->_magazineSlots[weapon]._muzzle;
  const SoundPars &pars = muzzle->_sound;
  context._weapons->_sound.Access(weapon); // playing sound of the weapon
  WeaponSounds &ws = context._weapons->_sound[weapon];
  ws.PlayEmptyMagazineSound(this, context, weapon, pars);
}

bool EntityAIFull::GetOwnHeatSource(Vector3 &ownHeatSource) const
{
  ownHeatSource = _heatSourceDirection;
  return true;
}

void EntityAIFull::SetMaxHitZoneDamage(float dammage)
{
  for (int i=0; i< _hit.Size(); i++)
  {
    _hit[i] = min(_hit[i], dammage);
  }
}

void EntityAIFull::SetMaxHitZoneDamageNetAware(float dammage)
{
  // set dammage for local objects only
  if (IsLocal())
    SetMaxHitZoneDamage(dammage);
  else
    GetNetworkManager().AskForSetMaxHitZoneDamage(this, dammage);
}

bool EntityAIFull::AutoReload(WeaponsState &weapons, int weapon, bool reloadAI, bool loaded)
{
  // this should be called whenever when:
  // slot is empty
  // there is some magazine that fits into the slot
  // such cases are:
  //   FireWeapon
  //   AddMagazine ????
  //   AddWeapon ????

  const MagazineSlot &slot = weapons._magazineSlots[weapon];
  const MuzzleType *muzzle = slot._muzzle;
  // TODO: flag reload on background
  //Magazine *magazine = slot._magazine;
  if (muzzle->_autoReload || Glob.config.GetAutoReload() ||
    reloadAI && (!GWorld->PlayerOn() || !GWorld->PlayerOn()->Brain() || GWorld->PlayerOn()->Brain() != CommanderUnit()))
    return ReloadMagazine(weapons, weapon, loaded);
  return false;
}

int EntityAIFull::AutoReloadAll(WeaponsState &weapons, bool loaded)
{
  int ret = -1;
  for (int s=0; s<weapons._magazineSlots.Size();)
  {
    const MagazineSlot &slot = weapons._magazineSlots[s];
    if (slot._muzzle)
    {
      if (!slot._magazine)
      {
        if (AutoReload(weapons, s, true, loaded) && ret < 0)
          ret = s;
      }
      else if (loaded)
        // load immediately
        slot._magazine->_reload = 0;
      s += slot._muzzle->_modes.Size();
    }
    else
      s++;
  }
  return ret;
}

class AutoReloadFunc : public ITurretFunc
{
protected:
  /// magazine will be loaded immediately
  bool _loaded;

public:
  AutoReloadFunc(bool loaded) : _loaded(loaded) {}
  bool operator () (EntityAIFull *entity, TurretContext &context)
  {
    entity->AutoReloadAll(*context._weapons, _loaded);
    return false; // for all turrets
  }
};

void EntityAIFull::AutoReloadAll(bool loaded)
{
  AutoReloadFunc func(loaded);
  ForEachTurret(func);
}

void EntityAIFull::DrawPeripheralVision(Person *person, CameraType camType)
{
  int canSee = 0;
  if (person && person->Brain() && person->Brain()->LSIsAlive())
  {
    if (person->Brain()==ObserverUnit())
      canSee = GetType()->_commanderCanSee;
    else if (person->Brain()==PilotUnit())
      canSee = GetType()->_driverCanSee;
    else if (person->Brain()==GunnerUnit())
      canSee = GetType()->_gunnerCanSee;
  }
  float visionAid = Glob.config.GetVisionAid();
  float peripheralVisionAid = Glob.config.GetPeripheralVisionAid();
  
  bool drawPeripheralVision = (
    !IsDamageDestroyed() && (visionAid>0 || peripheralVisionAid>0) &&
    (canSee&CanSeePeripheral)!=0
  );
  
  if (drawPeripheralVision && camType == CamGunner)
  {
    TurretContext context;
    if (FindTurret(person, context))
    {
      int selected = context._weapons->_currentWeapon;

      // isn't drawing disabled in optics?
      if (selected >= 0 && selected < context._weapons->_magazineSlots.Size())
      {
        const MagazineSlot &slot = context._weapons->_magazineSlots[selected];
        const MuzzleType *muzzle = slot._muzzle;
        const OpticsInfo &opticsInfo = slot._muzzle->_opticsInfo[slot._opticsMode];
        if (muzzle)
        {
          float factor = opticsInfo._opticsDisablePeripherialVision;
          if (factor>=0.95f) drawPeripheralVision = false;
          visionAid *= 1-factor;
          peripheralVisionAid *= 1-factor;
        }
      }
    }
  }

  if (drawPeripheralVision)
  {
    SCOPE_GRF("drawPV",0);
    // identify visible / audible cues
    AIBrain *unit = CommanderUnit();
    if (!unit) return;
    const TargetList *tgts = unit->AccessTargetList();
    if (!tgts) return;
    if (!GScene) return;
    Camera *camera = GScene->GetCamera();
    if (!camera) return;
    Matrix4 camSpace = camera->GetInvTransform();
    Matrix4 project = camera->Projection();
    
    AspectSettings as;
    GEngine->GetAspectSettings(as);
    
    float safeLeft, safeTop, safeRight, safeBottom;
    as.GetScreenSafeArea(safeLeft, safeTop, safeRight, safeBottom);

    float w = GEngine->WidthBB();
    float h = GEngine->HeightBB();
    
    float minXV = safeLeft*2-1;
    float maxXV = safeRight*2-1;
    float minYV = safeTop*2-1;
    float maxYV = safeBottom*2-1;
    
    float minX = safeLeft*w;
    float maxX = safeRight*w;
    float minY = safeTop*h;
    float maxY = safeBottom*h;
    
    float xBorder = 0.03f;
    float yBorder = 0.05f;
     
    Texture *tex = GWorld->UI()->GetCueTexture();
    Draw2DParsExt pars;
    pars.mip = GEngine->TextBank()->UseMipmap(tex,0,0);
    pars.spec = IsAlpha|IsAlphaFog|DstBlendOne|NoZBuf|ClampU|ClampV;
    pars.spec |= (GWorld->UI()->GetCueShadow() == 2)?UISHADOW : 0; 
    pars.SetU(0,1);
    pars.SetV(0,1);
    
    int anyCount = tgts->AnyCount();
    for (int i=0; i<anyCount; i++)
    {
      TargetNormal *tgt = tgts->GetAny(i);
      // check if the target is visible
      // if it is, check how much is is visible/audible
      //if (!tgt->IsKnownBy(unit)) return;
      EntityAI *ai = tgt->idExact;
      if (!ai)
        continue;
      // do not check static objects
      if (ai->Static())
        continue;
      if (!ai->SeenByPeriperalVision())
        continue;
      if (!ai->IsInLandscape())
        continue;
      // check if the cue is visible
      if (ai==this)
        continue;
      // note: far cues can be checked not as often
      float distAprox = ai->FutureVisualState().Position().Distance(camera->Position());
      float maxAge = InterpolativC(distAprox,20.0f,700.0f,0.5f,12.0f);
      float visible = GWorld->Visibility(unit,ai,maxAge);

      // consider fog
      if( distAprox>TACTICAL_VISIBILITY ) visible = 0;
      visible *= 1-GScene->TacticalFog8(Square(distAprox))*(1.0f/255);

      const LightSun *sun = GScene->MainLight();
      float night=sun->NightEffect();
      if (night>0)
      {
        // check light intensity
        float dark = 1.02f-sun->GetAmbient().Brightness()-sun->GetDiffuse().Brightness();
        saturateMin(night,dark);
        saturateMin(night,0.97f);
      }
      visible *= 1-night;
      if (visible<0.1f)
        continue;
      
      //saturate(markerAlpha,0.05f,0.25f);
      Vector3 tgtPos = ai->AimingPosition(ai->RenderVisualState());
      // project the position into the camera space
      Vector3 modelDir = camera->GetInvTransform()*tgtPos;
      //Vector3 camSpacePos = GScene->ScaledInvTransform()*tgtPos;

      //Vector3 camSpacePos = camSpace.FastTransform(tgtPos);
      // project into the screen space
      if (modelDir.Z()>=1e-3)
      {
        // front half-space - it might be visible
        float invZ = 1/modelDir.Z();
        float invDist = InvSqrt(tgtPos.Distance2(camera->Position()));

        float markerSize = ai->VisibleSize(ai->RenderVisualState())*invDist*4.0f*camera->InvTop();
        float markerAlpha = ai->VisibleMovementNoSize()*0.05f*visible;

        // following factor is 1 when target is in most sensitive area
        // and goes to zero when out of sight
        float aside2 = Square(modelDir.X())+Square(modelDir.Y());
        float peripheralFactor = modelDir.Z()*InvSqrt(aside2)*2;
        
        saturate (peripheralFactor,0,1);
        
        markerAlpha *= peripheralFactor;
        
        if (markerSize<0.01f) continue;
        if (markerAlpha<0.01f) continue;
        saturate(markerSize,0.01f,0.15f);
        saturate(markerAlpha,0.02f,0.3f);

        float scrX = modelDir.X()*invZ*camera->InvLeft();
        float scrY = -modelDir.Y()*invZ*camera->InvTop();
        
        // check if we are inside of visible area, and if not, how far we are
        
        // x borders are minX, maxX
        // y borders are minY, maxY
        
        if (scrX>=minXV && scrX<=maxXV && scrY>=minYV && scrY<=maxYV)
        {
          float insideX = floatMin(scrX-minXV,maxXV-scrX)*(1/xBorder);
          float insideY = floatMin(scrY-minYV,maxYV-scrY)*(1/yBorder);
          // both insideX and insideY is always >=0
          float inside = floatMax(0,floatMin(insideX,insideY));
          markerAlpha *= visionAid*inside+peripheralVisionAid*(1-inside);
        }
        else
          markerAlpha *= peripheralVisionAid;

        if (markerAlpha<0.01f)
          continue;
        
        Rect2DAbs rect;
        AIGroup *grp = ai->GetGroup();
        //units in group have another icon
        if(Glob.config.IsEnabled(DTHUDGroupInfo) && grp == unit->GetGroup()) continue;
        
        // check cue "color"
        PackedColor cueColor;
        
        TargetSide side = tgt->GetType()->_typicalSide;
        if (!ai->IRSignatureOn())
        {
          AIGroup *grp = ai->GetGroup();
          if (!grp || grp != unit->GetGroup())
            side = TCivilian; // empty or inactive from other groups marked as civilian
        }
        else if (tgt->GetSide()==TEnemy) // check for renegade
          side = TEnemy;
        
        if (Glob.config.IsEnabled(DTEnemyTag) && tgt->destroyed)
          side = TCivilian;

        if (unit->IsEnemy(side))
          cueColor = GWorld->UI()->GetCueEnemyColor();
        else if (side==TCivilian || unit->IsNeutral(side))
          cueColor = GWorld->UI()->GetCueColor();
        else if (unit->IsFriendly(side))
          cueColor = GWorld->UI()->GetCueFriendlyColor();
        else
          cueColor = GWorld->UI()->GetCueColor();
        
        int a = PackColorComponent(markerAlpha);
        PackedColor color = PackedColorRGB(cueColor,a);
        
        pars.SetColor(color);
        
        rect.w = +project(0,0)*markerSize*camera->Top()*camera->InvLeft()*0.5f;
        rect.h = -project(1,1)*markerSize*0.5f;
        
        // if we are out of the screen, normalize to get there
        if (fabs(scrX)>1)
        {
          float coef = 1/fabs(scrX);
          scrX *= coef; // result should be very close to +1 or -1
          scrY *= coef;
        }
        if (fabs(scrY)>1)
        {
          float coef = 1/fabs(scrY);
          scrX *= coef;
          scrY *= coef; // result should be very close to +1 or -1
        }

        rect.x = (scrX+1)*(w*0.5f);
        rect.y = (scrY+1)*(h*0.5f);
        
        saturate(rect.x,minX,maxX);
        saturate(rect.y,minY,maxY);
        
        rect.x -= rect.w*0.5f;
        rect.y -= rect.h*0.5f;
        
        GEngine->Draw2D(pars,rect);
      }
    }
  }
}

bool EntityAIFull::AimWeaponForceFire(const TurretContextEx &context, int weapon)
{
  Vector3 dir = FutureVisualState().Direction();
  dir[1] = 3;
  dir.Normalize();
#if _VBS3
  if(context._weapons->_forceFirePosition != Vector3(0,0,0))
  {
    float timeToLead = 10.0f;
    CalculateAimWeapon(context,weapon,dir,timeToLead,context._weapons->_forceFirePosition);
  }
#endif
  return AimWeapon(context, context._weapons->_currentWeapon, dir);
}

/*!
\patch 5117 Date 1/11/2007 by Jirka
- Fixed: Burst simulation in MP (Blackhawk M134 fired even when key released)
\patch 5153 Date 4/19/2007 by Jirka
- Fixed: AI - gunners was sometimes unable to fire simultaneously
*/

class SimulateWeaponActivityFunc : public ITurretFuncEx
{
protected:
  float _deltaT;
  SimulationImportance _prec;

public:
  SimulateWeaponActivityFunc(float deltaT, SimulationImportance prec)
    : _deltaT(deltaT), _prec(prec) {}

  bool operator () (EntityAIFull *entity, TurretContextEx &context)
  {
    // reload all muzzles
    const Magazine *lastMag = NULL; // handle only one mode for each magazine
    const Magazine *curMag = NULL; // magazine loaded in the current weapon
    const Magazine *curCM = NULL; // magazine loaded in the current counter measures system
    if (context._weapons->_currentWeapon >= 0)
      curMag = context._weapons->_magazineSlots[context._weapons->_currentWeapon]._magazine;
    if (context._weapons->_currentCounterMeasures >= 0)
      curCM = context._weapons->_magazineSlots[context._weapons->_currentCounterMeasures]._magazine;

    for (int i=0; i<context._weapons->_magazineSlots.Size(); i++)
    {
      // TODO: flag reload on background
      
      // select muzzle and magazine from the slot
      const MuzzleType *muzzle = context._weapons->_magazineSlots[i]._muzzle;
      Magazine *magazine = context._weapons->_magazineSlots[i]._magazine;
      
      // for the current weapon/CM, select the current mode, for others the first one
      int index = curMag && magazine == curMag ? context._weapons->_currentWeapon : i;
      index = curCM && magazine == curCM ? context._weapons->_currentCounterMeasures : index;
      const WeaponModeType *mode = context._weapons->_magazineSlots[index]._weaponMode;
      
      if (magazine && magazine->GetAmmo() > 0)
      {
        // reload each magazine once
        if (lastMag == magazine)
        {
          // same magazine in two magazine slots
          continue;
        }
#if 1
        bool found = false;
        for (int j=0; j<i; j++)
          if (context._weapons->_magazineSlots[j]._magazine == magazine)
          {
            found = true;
            const WeaponModeType *modeJ = context._weapons->_magazineSlots[j]._weaponMode;
            const WeaponModeType *modeI = context._weapons->_magazineSlots[i]._weaponMode;
            ErrF(
              "Duplicate magazine %s detected (id %d:%d) in slots %s and %s",
              cc_cast(magazine->_type->GetName()),
              magazine->_creator,magazine->_id,
              modeJ ? cc_cast(modeJ->GetName()) : "<null>", modeI ? cc_cast(modeI->GetName()) : "<null>"
            );
            break;
          }
          if (found)
            continue;
#endif
          lastMag = magazine;
          if (magazine->_reloadMagazine > 0)
          {
            magazine->_reloadMagazine -= _deltaT;
            if (magazine->_reloadMagazine <= muzzle->_reloadMagazineSoundDuration &&
              !context._weapons->_reloadMagazineSound)
            {
              // make sound when reloaded
              entity->PlayReloadMagazineSound(context, i, muzzle);
              saturateMax(magazine->_reloadMagazine, 0);
            }
          }
          else if (magazine->_reload > 0)
          {
            if (mode->_reloadTime <= 0) magazine->_reload = 0;
            else magazine->_reload -= _deltaT / (mode->_reloadTime * magazine->_reloadDuration);
            if(magazine->_reload * magazine->_reloadDuration * mode->_reloadTime <= muzzle->_reloadSoundDuration &&
              !context._weapons->_reloadSound)
            {
              // make sound when reloaded
              const SoundPars &pars = muzzle->_reloadSound;
              if (pars.name.GetLength() > 0)
              {
                float rndFreq = GRandGen.RandomValue() * 0.1 + 0.95;
                AbstractWave *wave = GSoundScene->OpenAndPlayOnce(pars.name, entity, true, entity->FutureVisualState().Position(), entity->FutureVisualState().Speed(),
                  pars.vol, pars.freq * rndFreq, pars.distance);
                if (wave)
                {
                  GSoundScene->SimulateSpeedOfSound(wave);
                  GSoundScene->AddSound(wave);
                  context._weapons->_reloadSound = wave;
                }
              }
              saturateMax(magazine->_reload, 0);
            }
          }
      }
    }

    if (context._weapons->_forceFireWeapon >= 0 && !entity->IsDeadSet())
    {
      int selected = context._weapons->_currentWeapon;
      if (context._weapons->_forceFireWeapon == selected)
      {
        bool aimed = entity->AimWeaponForceFire(context, selected);
        if (
          entity->GetWeaponLoaded(*context._weapons, selected) &&
          (entity->GetWeaponDirection(entity->RenderVisualState(), context, selected).Y() >= 0.7 || aimed))
        {
          const WeaponModeType *mode = context._weapons->_magazineSlots[selected]._weaponMode;
          if (mode && mode->_useAction)
          {
            if (context._gunner && context._gunner->Brain())
              entity->StartActionProcessing(new ActionWeaponIndex(ATUseWeapon, entity, context._gunner, selected), context._gunner->Brain());
            context._weapons->_forceFireWeapon = -1;
          }
          else
          {
            if (entity->FireWeapon(context, selected, NULL, false))
              context._weapons->_forceFireWeapon = -1;
          }
        }
      }
    }

    // advance time for all looping weapon sounds
    for (int i = 0; i < context._weapons->_sound.Size(); i++)
    {
      WeaponSounds &ws = context._weapons->_sound[i];
      ws.SetStopTime(Glob.time.toFloat());
    }
    // simulate burst fire

    bool isLocal = context._turret ? context._turret->IsLocal() : entity->IsLocal();
    if (isLocal)
    {
      int weapon = context._weapons->_currentWeapon;
      if (weapon>=0 && weapon<context._weapons->_magazineSlots.Size())
      {
        const Magazine *magazine = context._weapons->_magazineSlots[weapon]._magazine;
        if (magazine && magazine->_burstLeft > 0)
          // fire on same target as before
          entity->FireWeapon(context, weapon, entity->GetShootTarget(), false);
      }

      // simulate burst couterMeasures
      int counterMeasures = context._weapons->_currentCounterMeasures;
      if (weapon != counterMeasures && counterMeasures>=0 && counterMeasures<context._weapons->_magazineSlots.Size())
      {
        const Magazine *magazine = context._weapons->_magazineSlots[counterMeasures]._magazine;
        if (magazine && magazine->_burstLeft > 0)
          // fire on same target as before
          entity->FireWeapon(context, counterMeasures, NULL, false);
      }

      Target* fireTarget = (entity == GWorld->CameraOn())? GWorld->UI()->GetLockedTarget() : (Target *)context._weapons->_fire._fireTarget;
      if(fireTarget != context._weapons->_aimedTarget)
      {       
        if(context._weapons->_aimedTarget && context._weapons->_aimedTarget->idExact)
        {
          EntityAI *vehExact = context._weapons->_aimedTarget->idExact;
          if(vehExact && vehExact->CommanderUnit() && vehExact->CommanderUnit()->GetVehicleIn() && context._gunner)
          {
            if(vehExact->IsLocal())
              vehExact->CommanderUnit()->GetVehicleIn()->OnWeaponLock(vehExact, context._gunner, false);
            else GetNetworkManager().OnWeaponLocked(vehExact, context._gunner,false);
          }
          context._weapons->_aimedTarget = NULL;
        }
        
        if(fireTarget && fireTarget->idExact)
        {
          EntityAI *vehExact = fireTarget->idExact;
          if(context._weapons->_currentWeapon >= 0 && context._weapons->_magazineSlots[context._weapons->_currentWeapon]._weapon->_weaponLockSystem > 0)
          {
            if(vehExact->CommanderUnit() && vehExact->CommanderUnit()->GetVehicleIn() && context._gunner)
            {
              context._weapons->_aimedTarget = fireTarget;
              if(vehExact->IsLocal())
                vehExact->CommanderUnit()->GetVehicleIn()->OnWeaponLock(vehExact, context._gunner, true);
              else GetNetworkManager().OnWeaponLocked(vehExact, context._gunner, true);
            }      
          }
        }
      }
      if(fireTarget)
        context._weapons->_targetAimed = entity->GetAimed(context, context._weapons->_currentWeapon, fireTarget);
    }
    
#if _VBS3
    // Gunner may be remote, and vehicle local
#else
    if (isLocal)
    {
#endif
      if (!context._weapons->_laserTargetOn || !entity->GetGroup())
        entity->StopLaser(context);
      else
        entity->TrackLaser(context, context._weapons->_currentWeapon);
#if _VBS3
#else
    }
#endif
    return false; // continue
  }
};

void EntityAIFull::SimulateWeaponActivity( float deltaT, SimulationImportance prec )
{
  SimulateWeaponActivityFunc func(deltaT, prec);
  ForEachTurretEx(unconst_cast(FutureVisualState()), func);

  // simulate shooting visibility
  _shootTimeRest -= deltaT;
#if _VBS3 //adding _shootTimeRestAudible
  _shootTimeRestAudible -= deltaT;
  if( _shootTimeRestAudible<0 || IsDamageDestroyed())
  {
    // return to default
    _shootTimeRestAudible = FLT_MAX;
    _shootAudible = 0;
  }
#endif

  if( _shootTimeRest<0 || IsDamageDestroyed())
  {
    // return to default
    _shootTimeRest = FLT_MAX;
    _shootVisible = 0;
#if !_VBS3
    _shootAudible = 0;
#endif
  }

  AIBrain *unit = GWorld->FocusOn();
  if (unit && unit == CommanderUnit() && !unit->IsInCargo() && !GWorld->GetCameraEffect())
  {
    if (GInput.GetActionToDo(UAHeadlights))
      _pilotLight=!_pilotLight;
  }

  //((ARMAFIX
  // recoil simulation moved into Man::MoveWeapons

  if (this==GWorld->CameraOn())
  {
    // force feedback simulation is necessary
    // StartRecoilFF will both start and stop the effect
    StartRecoilFF();
  }
  //))ARMAFIX
}

void EntityAI::IsMoved()
{
  // move condition detected
  _lastMovement = Glob.time;
  CancelStop();
}
void EntityAI::StopDetected()
{
  // stop condition detected
  if (Glob.time>_lastMovement+TimeToStop())
  {
    Stop();
    FutureVisualState()._speed=VZero;
    _angMomentum=VZero;
    _angVelocity=VZero; //Added by pf. also velocity must ve zero.
  }
}

void EntityAI::SwitchLight(bool on)
{
  Assert(!on); // Only switching off is working (switching on could use damaged light)
  _reflectors.SwitchLight(on);
}


void WeaponSounds::RegisterBulletSound()
{
  if (_count == 0)
    _delay = Glob.time + 0.5f;
  static const int MaxBulletsSound = 10;
  if (_count < MaxBulletsSound)
    _count++;
}

void WeaponSounds::UpdateSound(const TurretContext &context, int weapon, const Vector3 &pos, const Vector3 &speed)
{
  // play bullets sounds
  UpdateBulletSound(context, weapon);

  // shot sound
  if (_fireSound._sound)
  {
    if (_fireSound._sound->IsTerminated())
      _fireSound._sound.Free();
    else
      _fireSound._sound->SetPosition(pos, speed);
  }

  // bullet sound
  if (_bulletSound)
  {
    if (_bulletSound->IsTerminated()) 
      _bulletSound.Free();
    else
      _bulletSound->SetPosition(pos, speed);
  }

  // empty magazine sound
  if (_emptyMagSound)
  {
    if (!_emptyMagSound->IsTerminated())
      _emptyMagSound->SetPosition(pos, speed);
    else
      _emptyMagSound.Free();
  }
}

void WeaponSounds::PlayEmptyMagazineSound(EntityAIFull *entity, const TurretContext &context, int weapon, const SoundPars &pars)
{
  if (pars.name.GetLength() > 0 && entity)
  {
    _entity = entity;
    _position = _entity->GetWeaponSoundPos(context, weapon);

    if (pars.name.GetLength() > 0 && entity && _emptyMagSound.IsNull())
      // create new wave
      _emptyMagSound = GSoundScene->OpenAndPlayOnce(pars.name, _entity, false, _position, _entity->FutureVisualState().Speed(), pars.vol, pars.freq, pars.distance);
    else if (_emptyMagSound && !_emptyMagSound->IsWaiting())
       _emptyMagSound->Restart();
    if (_emptyMagSound && pars.name.GetLength() > 0)
      GetNetworkManager().PlaySound(pars.name, _position, _entity->FutureVisualState().Speed(), pars.vol, pars.freq, _emptyMagSound);
  }
}

void WeaponSounds::SetStopTime(float time)
{
  _fireSound._time = Time(time);
  if (_fireSound._sound)
    _fireSound._sound->SetStopValue(time);
}

void WeaponSounds::UnloadSound()
{
  // shoot sound
  _fireSound._sound.Free();
  _bulletSound.Free();
  _emptyMagSound.Free();
}

void WeaponSounds::UpdateBulletSound(const TurretContext &context, int weapon)
{
  // delay is set when first shot is fired
  if (_delay < Glob.time)
  {
    if (_count != 0)
    {
      if (_nextBulletTime < Glob.time)
      {
        const Entity::VisualState &vs = _entity->RenderVisualState();
        if (_bulletSound.IsNull() && _entity)
        {
          const SoundPars& pars = _entity->SelectBulletSoundPars(context, weapon);

          if (pars.name.GetLength() > 0)
            // create new wave
            _bulletSound = GSoundScene->OpenAndPlayOnce(pars.name, _entity, false, _position, vs.Speed(), pars.vol, pars.freq, pars.distance);
        }
        else
        {
          if (_count % 3 == 0)
          {
            _bulletSound.Free();

            // fixed - entity == NULL
            if (_entity)
            {
              // select new sample
              const SoundPars& pars = _entity->SelectBulletSoundPars(context, weapon);
              _bulletSound = GSoundScene->OpenAndPlayOnce(pars.name, _entity, false, _position, vs.Speed(), pars.vol, pars.freq, pars.distance);
            }
          }
          else if (_bulletSound.NotNull() && !_bulletSound->IsWaiting())
            _bulletSound->Restart();
        }
        _count--;
        _nextBulletTime = Glob.time + _reloadTime;
      }
    }
  }
}

const SoundPars& EntityAIFull::SelectBulletSoundPars(const TurretContext &context, int weapon) const
{
  static SoundPars empty;
  return empty;
}

void WeaponSounds::ShotSound(EntityAIFull *entity, const TurretContext &context, int weapon, const Magazine *magazine)
{
  const MagazineSlot &slot = context._weapons->_magazineSlots[weapon];
  const WeaponModeType *mode = slot._weaponMode;

  if (!entity || !magazine) return;

  _entity = entity;

  // position used as position of sound
  _position = entity->GetWeaponSoundPos(context, weapon);

  // reload time
  _reloadTime = mode->_reloadTime;

  // check if player is inside
  bool isInside = GWorld->IsCameraInsideVeh();

  if (_fireSound._sound.IsNull())
  {
    float rand = GRandGen.RandomValue();
    // select sample ... select one from group
    const SoundPars &pars = mode->_beginSound.SelectSound(rand);
    
    if (pars.name.GetLength() > 0)
    {
      // create wave
      _fireSound._sound = GSoundScene->OpenAndPlayOnce(pars.name, entity, false, _position, _entity->FutureVisualState().Speed()
        , pars.vol, pars.freq, pars.distance);

      if (_fireSound._sound)
      {
        GSoundScene->SimulateSpeedOfSound(_fireSound._sound);
        float obstruction, occlusion;
        GWorld->CheckSoundObstruction(entity, false, obstruction, occlusion);
        _fireSound._sound->SetObstruction(obstruction, occlusion);
        _fireSound._sound->EnableFilter(true, isInside ? InternalFireLowPFilter : GenericLowPFilter);
      }
      if (!mode->_soundContinuous) RegisterBulletSound();

      /*
      static const float MinDist2 = 169;

      // fast test - distance listener and entity center
      if (magazine->_type && MinDist2 > GSoundsys->GetListenerPosition().Distance2(_position))
      {
        const AmmoType *ammoType = magazine->_type->_ammo;

        // effect enable on fire
        if (ammoType && (ammoType->_whistleOnFire != 0) && ammoType->_whistleDist > 0)
        {
          Transport *tr = dynamic_cast<Transport*>(entity);

          Transport *tFocOn = GWorld->FocusOn() ? GWorld->FocusOn()->GetVehicleIn() : NULL;
          
          if (tr != tFocOn && tr && context._turret && context._turretType && !GWorld->GetCameraEffect())
          {
            Matrix4Val shootTrans = tr->GunTurretTransform(*context._turretType);
            Vector3Val gPosL = context._turretType->GetTurretPos(shootTrans);
            Vector3Val gPosW = entity->PositionModelToWorld(gPosL);

            _fireSound._sound->SetEWPars(true, gPosW, ammoType->_whistleDist);
          }
        }
      }*/
    }
  }
  else
  {
    // restart sample
    if (!mode->_soundContinuous)
    {
      if (!_fireSound._sound->IsWaiting())
      {
        float obstruction, occlusion;
        GWorld->CheckSoundObstruction(entity, false, obstruction, occlusion);
        //GSoundScene->SimulateSpeedOfSound(_fireSound._sound); // removed for patch 1.04: cause sounds cutting
        _fireSound._sound->Restart(); 
        _fireSound._sound->EnableFilter(true, isInside ? InternalFireLowPFilter : GenericLowPFilter);
        _fireSound._sound->SetPosition(_position, _entity->FutureVisualState().Speed(), true);
        RegisterBulletSound();
      }
    }
    else
    {
      _fireSound._time = Glob.time;
      _fireSound._sound->EnableFilter(true, isInside ? InternalFireLowPFilter : GenericLowPFilter);
      _fireSound._sound->PlayUntilStopValue(Glob.time.toFloat() + _fireSound._sound->GetLength());
    }
  }
}


/// Functor for sound simulation
class WeaponsSoundFunc : public ITurretFunc
{
public:
  bool operator () (EntityAIFull *entity, TurretContext &context)
  {
    for (int i = 0; i < context._weapons->_sound.Size(); i++)
    {
      WeaponSounds &ws = context._weapons->_sound[i];
      ws.UpdateSound(context, i, entity->GetWeaponSoundPos(context, i), entity->RenderVisualState().Speed());
    }

    int selected = context._weapons->_currentWeapon;

    if (selected >= 0)
    {
      AbstractWave *wave = context._weapons->_reloadMagazineSound;
      if (wave) wave->SetPosition(entity->GetWeaponSoundPos(context, selected), entity->RenderVisualState().Speed());
    }
    {
      AbstractWave *wave = context._weapons->_reloadSound;
      if (wave) wave->SetPosition(entity->RenderVisualState().Position(), entity->RenderVisualState().Speed());
    }
    return false; // for all weapon systems
  }
};


void EntityAIFull::Sound( bool inside, float deltaT )
{
  WeaponsSoundFunc func;
  ForEachTurret(func);
  base::Sound(inside,deltaT);
}


/// Functor for sound unload
class WeaponsUnloadSoundFunc : public ITurretFunc
{
public:
  bool operator () (EntityAIFull *entity, TurretContext &context)
  {
    for (int i = 0; i < context._weapons->_sound.Size(); i++)
    {
      context._weapons->_sound[i].UnloadSound();
    }
    context._weapons->_sound.Clear();

    if (context._weapons->_reloadMagazineSound)
    {
      context._weapons->_reloadMagazineSound->Stop();
      context._weapons->_reloadMagazineSound = NULL;
    }
    if (context._weapons->_reloadSound)
    {
      context._weapons->_reloadSound->Stop();
      context._weapons->_reloadSound = NULL;
    }
    return false; // for all weapon systems
  }
};


void EntityAIFull::UnloadSound()
{
  WeaponsUnloadSoundFunc func;
  ForEachTurret(func);
  base::UnloadSound();
}

void EntityAIFull::StartRecoilFF()
{
#ifdef _WIN32
  if (_recoil)
  {
    RecoilFFRamp ramp;
    if (_recoil->GetFFRamp(_recoilTime,ramp))
    {
      float ffCoef = 10;
      float begMag = ramp._begAmplitude * _recoilFactor*ffCoef;
      float endMag = ramp._endAmplitude * _recoilFactor*ffCoef;
      if (GJoystickDevices)
        GJoystickDevices->PlayRamp(begMag,endMag,ramp._duration);
      if (GInput.IsXInputPresent())
        XInputDev->PlayRamp(begMag,endMag,ramp._duration);
    }
    else
    {
      if (GJoystickDevices)
        GJoystickDevices->PlayRamp(0,0,0);
      if (XInputDev)
        XInputDev->PlayRamp(0,0,0);
    }      
  }
  else
  {
    if (GJoystickDevices)
      GJoystickDevices->PlayRamp(0,0,0);
    if (XInputDev)
      XInputDev->PlayRamp(0,0,0);
  }
#endif
}


/*!
React to impulse by force feedback / recoil effect
*/

void EntityAIFull::StartRecoil(RecoilFunction *recoil, float recoilFactor)
{
  if (recoil->GetTerminated(0,0))
    // no need to start recoil that has already been terminated
    return;
  if (_recoil)
  {
    // TODO: several recoils may be running at the same time
    if (recoilFactor<_recoilFactor)
      // current recoil is stronger - prefer it
      return;
  }
  _recoil = recoil;
  _recoilTime = 0;
  _recoilFactor = recoilFactor;
  _recoilIndex = 0;
  _recoilRandomX=GRandGen.RandomValue()+0.5;
  _recoilRandomY=GRandGen.RandomValue()+0.5;
  _recoilRandomZ=GRandGen.RandomValue()+0.5;
  
  if (this==GWorld->CameraOn())
    StartRecoilFF();
}

void EntityAIFull::StartRecoil( RStringB name, float recoilFactor )
{
  Ref<RecoilFunction> recoil = RecoilFunctions.New(name);
  //LogF("Impulse %.2f",factor);
  StartRecoil(recoil,recoilFactor);
}

void EntityAI::CleanUp()
{
  // reflectors contain links back to the entity - make sure they are released
  _reflectors.Clear();
  _markers.Clear();
  _markersBlink.Clear();
  _nvgMarkers.Clear();
  _nvgMarkersBlink.Clear();
  UnloadSound();
  PerformUnlock();
  base::CleanUp();
}

void EntityAI::CleanUpMoveOut()
{
  UnloadSound();
  PerformUnlock();
  base::CleanUpMoveOut();

}

#if _VBS3
#include "soldierOld.hpp"
#endif


struct LightInfoExt: public LightInfo
{
  Ref<LightPointOnVehicle> light;
  InitPtr<Object> attachTo;

  float TotalBrightness() const {return (color.Brightness()+ambient.Brightness())*Square(brightness);}
};

TypeIsMovableZeroed(LightInfoExt);

void EntityAI::AttachLights()
{
  if (!_lightsAttached)
  {
    GWorld->AddAttachment(this);
    _lightsAttached = true;
  }
}

void EntityAI::Simulate(float deltaT, SimulationImportance prec)
{
  // avoid call of ResourceSupply::Simulate when _supplying != NULL && _action == NULL
  // this can happen on remote entity - _supplying is synchronized, _action is not
  // however we need to call remote ResourceSupply::Simulate for taking weapons / magazines
  if (_supply && !IsDamageDestroyed() && (IsLocal() || _supply->GetAction()))
    _supply->Simulate(this,deltaT,SimulateVisibleFar);

  if (_shape && !_static) // do not lock buildings
  {
#if _VBS3 
    bool personalItems = false;
    const Man *man = dyn_cast<const Man>(this);

    if(man&&man->IsPersonalItemsEnabled())
      personalItems = true;
#endif

    if
    (
      HasGeometry() && ( _isStopped || FutureVisualState()._speed.SquareSize()<Square(1.0f) ) &&
      (
        !Airborne() || // lock for vehicle that are not airborne
        // lock also for low static airborne vehicle
        FutureVisualState().Position().Y() < GLandscape->SurfaceYAboveWater(FutureVisualState().Position().X(),FutureVisualState().Position().Z())+15
      )
#if _VBS3
      && !personalItems
#endif
    )
      LockPosition();
    else
      UnlockPosition();
  }
  float delta;
  delta=_avoidAsideWanted-_avoidAside;
  // if we need more avoidance, or avoidance in a different direction, react fast
  if (fabs(_avoidAsideWanted)>fabs(_avoidAside) || _avoidAside*_avoidAsideWanted<0)
    saturate(delta,-20*deltaT,+20*deltaT);
  else
    saturate(delta,-0.2*deltaT,+0.2*deltaT);
  _avoidAside+=delta;

  SimulateWeaponActivity(deltaT,prec);

  // marker lights
  if (_markers.Size() == 0 && _markersBlink.Size() == 0 && _nvgMarkers.Size() == 0 && _nvgMarkersBlink.Size() == 0)
  {
    if (_pilotLight)
    {
      const int maxLights = 4;
      
      LODShapeWithShadow *shape = GLOB_SCENE->Preloaded(Marker);
        
      AutoArray<LightInfoExt, MemAllocLocal<LightInfoExt,8,AllocAlign16> > aggLights;
      for (int i=0; i<GetType()->_lights.Size(); i++)
      {
        const LightInfo &info = GetType()->_lights[i];
        // when the light is permanent and the point not animated, we can do with a static permanent light
        bool needsPositionUpdates = NeedsPositionUpdates(this,-1);
        if (needsPositionUpdates) AttachLights();
        bool needsAttachment = needsPositionUpdates || info.type!=LightTypeMarker;

        Ref<LightPointOnVehicle> light = new LightPointOnVehicle(
          shape, info.color, info.ambient,needsAttachment ? this : NULL,info.position
        );
        if (!needsAttachment)
          light->UpdatePositionTo(FutureVisualState().Transform());
        light->SetBrightness(info.brightness);

        if (!info.nvgMarker)
        {
          if (info.type == LightTypeMarker)
            _markers.Add(light);
          else if (info.type == LightTypeMarkerBlink)
            _markersBlink.Add(light);
          else
            ErrF("Bad light type %d in %s",info.type,cc_cast(GetType()->GetName()));
        }
        else
        {
          if (info.type == LightTypeMarker)
            _nvgMarkers.Add(light);
          else if (info.type == LightTypeMarkerBlink)
            _nvgMarkersBlink.Add(light);
          else
            ErrF("Bad light type %d in %s",info.type,cc_cast(GetType()->GetName()));
        }

        LightInfoExt &aggInfo = aggLights.Append();
        aggInfo.LightInfo::operator =(info);
        aggInfo.light = light;
        aggInfo.attachTo = needsAttachment ?  this : NULL;

        GLOB_SCENE->AddLightReal(light);
      }

      // caution: this loop is O(N^3)
      while (aggLights.Size()>maxLights)
      {
        // select two most similar lights and merge them
        float minDist = FLT_MAX;
        int bestA=-1,bestB=-1;
        for (int i=0; i<aggLights.Size(); i++) for (int j=i+1; j<aggLights.Size(); j++)
        {
          const LightInfoExt &l1 = aggLights[i];
          const LightInfoExt &l2 = aggLights[j];
          // consider: blink / no blink, distance, color and brightness
          if (l1.type!=l2.type) continue; // different blink never matches
          if (l1.nvgMarker != l2.nvgMarker) continue; // both should be nvgMarker
          if (l1.attachTo != l2.attachTo) continue; // both should have the same attachment status
          float dist = 
          (
            l1.position.Distance(l2.position)
            +(l1.color-l2.color).Brightness()*0.5
            // prefer merging lights with lower brightness
            +floatMin(l1.TotalBrightness(),l2.TotalBrightness())*3.0
          );
          if (minDist>dist)
          {
            bestA = i;
            bestB = j;
            minDist = dist;
          }
        }
        Assert(bestA>=0 && bestB>=0); // some match must be found, only blinking prevents matching
        { // now merge A and B and delete B
          LightInfoExt &a = aggLights[bestA];
          LightInfoExt &b = aggLights[bestB];
          float aw = Square(a.brightness);
          float bw = Square(b.brightness);
          float invW = 1/(aw+bw);
          // all parameters as weighted average
          a.ambient = (a.ambient*aw + b.ambient*bw)*invW;
          a.color = (a.color*aw + b.color*bw)*invW;
          a.position = (a.position*aw + b.position*bw)*invW;
          // only brightness as as sum
          a.brightness = sqrt(aw+bw);
          a.light = NULL; // light parameters changed, we need to recreate it
          aggLights.Delete(bestB);
        }
      }
      // process top selected lights
      for (int i=0; i<aggLights.Size(); i++)
      {
        if (i>=maxLights)
          break;
        
        LightInfoExt &info = aggLights[i];
        if (!info.light)
        {
          info.light = new LightPointOnVehicle(shape, info.color, info.ambient,info.attachTo, info.position);
          if (!info.attachTo)
          {
            info.light->UpdatePositionTo(FutureVisualState().Transform());
          }
          info.light->SetBrightness(info.brightness);
          // add new light into markers/_markersBlink so that we can control it
          (info.type==LightTypeMarkerBlink ? _markersBlink : _markers).Add(info.light);
        }
        GLOB_SCENE->AddLightAgg(info.light);
      }
      
      _markers.Compact();
      _markersBlink.Compact();
      _nvgMarkers.Compact();
      _nvgMarkersBlink.Compact();
      _markersOn = Glob.time - GRandGen.RandomValue();
      _nvBlink = true;
      _timeElapsed = 0.0f;
    }
  }
  else
  {
    if (!_pilotLight)
    {
      _markers.Resize(0);
      _markersBlink.Resize(0);
      _nvgMarkers.Resize(0);
      _nvgMarkersBlink.Resize(0);
    }
  }
  if (_pilotLight)
  {
    bool on = (toInt(2.0 * (Glob.time - _markersOn)) % 2) != 0; 
    for (int i=0; i<_markersBlink.Size(); i++)
      _markersBlink[i]->Switch(on);

    const Person *person = GWorld->FocusOn() ? GWorld->FocusOn()->GetPerson() : NULL;
    bool nightVision = person ? person->IsNVWanted() : false;

    // marker light visible only in nvg
    for (int i = 0; i < _nvgMarkers.Size(); i++)
      _nvgMarkers[i]->Switch(nightVision);

    static float blinkingPeriod = 1.5f;

    _timeElapsed += deltaT;
    if (_timeElapsed > blinkingPeriod)
    {
      _nvBlink = !_nvBlink;
      _timeElapsed = 0.0f;
    }

    for (int i = 0; i < _nvgMarkersBlink.Size(); i++)
      _nvgMarkersBlink[i]->Switch(_nvBlink & nightVision);
  }

  // Reflectors

#if _AAR
  _historyLine.RemoveOldUpdates();
  _historyLine.AddPath();
#endif

  base::Simulate(deltaT, prec);
}

void EntityAI::SimulatePost(float deltaT, SimulationImportance prec)
{
  // simulate flag
  Texture *texture = GetFlagTexture();
  if (texture)
  {
    int level = _shape->FindSimplestLevel();
    if (level>=0)
    {
      ShapeUsed lock = _shape->Level(level);
      for (int i=0; i<GetType()->GetProxyCountForLevel(level); i++)
      {
        const ProxyObjectTyped &proxy = GetType()->GetProxyForLevel(level,i);
        Flag *veh = dyn_cast<Flag>(proxy.obj.GetRef());
        if (veh)
        {
          SkeletonIndex boneIndex = GetType()->GetProxyBoneForLevel(level,i);
          Matrix4 proxyTransform = AnimateProxyMatrix(RenderVisualState(), level,proxy.GetTransform(),boneIndex);

          Matrix4 pTransform = FutureVisualState().Transform() * proxyTransform;

          AdjustFlagTransform(pTransform);
          Vector3 speed(VZero);
          if (_flag)
          {
            if (deltaT > 0)
              speed = (1.0 / deltaT) * (pTransform.Position() - _flag->FutureVisualState().Position());           
          }
          else
          {
            RString shapeName = ::GetShapeName(proxy.name);
            // copy flag - create a new one with the same type, but different shape
            Ref<Entity> flag = GWorld->NewNonAIVehicleWithID(veh->GetNonAIType()->GetName(), shapeName);
            _flag = dyn_cast<Flag,Entity>(flag);
            if (!_flag)
            {
              RptF("No flag - %s,%s,%s",(const char *)flag->GetDebugName(),(const char *)shapeName,
                (const char *)veh->GetNonAIType()->GetName());
              Fail("No flag");
            }
          }
          // proxy may be animation
          if (_flag && _flag->InitAsync(pTransform))
          {
            _flag->SetTransformPossiblySkewed(pTransform);
            _flag->SetSpeed(speed);
            _flag->FlagSimulate(pTransform,deltaT, prec);
          }
          // if we have one flag, we are done - multiple flags are not supported
          break;
        }
      }
    }
  }
  else if (_flag)
    _flag = NULL;

#if _ENABLE_ATTACHED_OBJECTS
  MoveAttached(deltaT);
#endif

#if _VBS2
  _postFrame = *this;   //convoy trainer
#endif
}

void EntityAI::Init(Matrix4Par pos, bool init)
{
  base::Init(pos, init);
  // called before vehicle is placed
  if (_flag)
  {
    Matrix4 trans = pos * _flag->FutureVisualState().Transform();
    _flag->Init(trans, init);
  }

  const EntityAIType *type = GetType();
  
  // create reflectors
  _reflectors.Create(this, type->_reflectors);

  // Moved outside function to avoid launching in some situations
  // OnEvent(EEInit);
}


/*\deprecated Currently not used*/

Texture *EntityAI::GetSideSign() const
{
  if( _targetSide==TEast ) return GLOB_SCENE->Preloaded(SignSideE);
  else if( _targetSide==TWest ) return GLOB_SCENE->Preloaded(SignSideW);
  else if( _targetSide==TGuerrila ) return GLOB_SCENE->Preloaded(SignSideG);
  return NULL; // neutral, guerilla .. etc
}

bool EntityAI::CanBeInstanced(int level, float distance2, const DrawParameters &dp) const
{
  const EntityAIType *type = GetType();
  if (type->_hiddenSelections.Size() > 0) return false;
  return base::CanBeInstanced(level, distance2, dp);
}

/*!
\patch 1.75 Date 3/7/2002 by Jirka
- Added: Support for reload animations for vehicles
\patch 1.78 Date 7/16/2002 by Ondra
- Fixed: AH1 cannon was aiming, but not moving (wrong animation).
*/

void EntityAI::Animate(AnimationContext &animContext, int level, bool setEngineStuff, Object *parentObject, float dist2)
{
  ShapeUsed shape=_shape->Level(level);
  if (shape.IsNull())
    return;
  base::Animate(animContext, level, setEngineStuff, parentObject, dist2);
  const EntityAIType *type=GetType();
  Texture *tex;
/*
  tex = GetUnitSign();
  type->_unitNumber.SetTexture(_shape,level,tex);
  if (tex) type->_unitNumber.Unhide(_shape,level);
  else  type->_unitNumber.Hide(_shape,level);
  tex = GetGroupSign();
  type->_groupSign.SetTexture(_shape,level,tex);
  if (tex) type->_groupSign.Unhide(_shape,level);
  else  type->_groupSign.Hide(_shape,level);
  tex = GetSectorSign();
  type->_sectorSign.SetTexture(_shape,level,tex);
  if (tex) type->_sectorSign.Unhide(_shape,level);
  else  type->_sectorSign.Hide(_shape,level);
*/

  if (CommanderUnit())
    tex = CommanderUnit()->GetPerson()->GetInfo()._squadPicture;
  else
    tex = NULL;
  if (tex != _squadTexture)
    _squadTexture = tex;
  type->_clan.SetTexture(animContext, _shape, level, tex);
  if (tex)
    type->_clan.Unhide(animContext, _shape, level);
  else
    type->_clan.Hide(animContext, _shape, level);

  if( _pilotLight )
    type->_backLights.Unhide(animContext, _shape, level);
  else
    type->_backLights.Hide(animContext, _shape, level);

  // reflectors
  _reflectors.Animate(animContext, level, this, type->_reflectors);

  // hidden selections
  for (int i=0; i<type->_hiddenSelections.Size(); i++)
  {
    if (_hiddenSelectionsTextures[i])
    {
      type->_hiddenSelections[i].Unhide(animContext, _shape, level);
      type->_hiddenSelections[i].SetTexture(animContext, _shape, level, _hiddenSelectionsTextures[i]);
    }
    else
      type->_hiddenSelections[i].Hide(animContext, _shape, level);
  }


  // animate materials (we could do hitpoint based animation, but we currently do whole veh. only)
  // check wound level
  if (type->_damageInfo && !type->_damageInfo->IsEmpty())
  {
    float dam = GetRawTotalDamage();
    int damLevel = 0;
    if (dam>=1.0)
      damLevel = 2;
    else if (dam>=0.5)
      damLevel = 1;
    if (damLevel>0)
      type->_totalDamage.Apply(animContext, _shape, level, damLevel, dist2);
  }
}

/// implement ClassTypeInfo for ActivityFadeIn
class ActivityClassTypeInfoFadeIn: public ActivityClassTypeInfo
{
  typedef ActivityClassTypeInfo base;
  
public:
  ActivityClassTypeInfoFadeIn():base("FadeIn"){}
  //@{ implement ActivityClassTypeInfo
  virtual Activity *CreateObject(ParamArchive &ar) const {return NULL;}
  virtual Activity *LoadRef(ParamArchive &ar) const {return NULL;}
  //@}  
};


#include <Es/Memory/normalNew.hpp>

/// perform a fade out to hide rendering artifacts or camera change
class ActivityFadeIn: public EntityActivity
{
  typedef EntityActivity base;
  
  static ActivityClassTypeInfoFadeIn _typeInfo;
  
  OLinkPermNO(AIBrain) _unit;
  float _fadeIn;
  
public:
  ActivityFadeIn(AIBrain *unit);

  //@{ implemented Activity
  virtual void OnEvent(ActivityEvent *event){}
  virtual const ActivityClassTypeInfo &GetTypeInfo() const {return _typeInfo;}
  virtual LSError Serialize(ParamArchive &ar){return LSOK;}
  //@}

  //@{ implementation of EntityActivity
  virtual bool Simulate(Entity *entity, float deltaT, SimulationImportance prec);
  virtual void Draw(Entity *entity);
  //@}
  
  USE_FAST_ALLOCATOR
};

ActivityClassTypeInfoFadeIn ActivityFadeIn::_typeInfo;

CameraType ValidateCamera(CameraType cam);


#include <Es/Memory/debugNew.hpp>

DEFINE_FAST_ALLOCATOR(ActivityFadeIn)

ActivityFadeIn::ActivityFadeIn(AIBrain *unit)
:_unit(unit),_fadeIn(1)
{
}

bool ActivityFadeIn::Simulate(Entity *entity, float deltaT, SimulationImportance prec)
{
  if (_unit && GWorld->FocusOn() == _unit)
    _fadeIn -= deltaT*3;
  else
    _fadeIn = 0;
  return _fadeIn>0;
}

void ActivityFadeIn::Draw(Entity *entity)
{
  if (_unit && GWorld->FocusOn() == _unit && _unit->GetVehicle()==entity)
    GEngine->Draw2DWholeScreen(PackedBlack,_fadeIn);
}


/**
@param unit camera focus unit
*/
void EntityAIFull::StartFadeIn(AIBrain *unit)
{
  _activities.Add(new ActivityFadeIn(unit));
}

bool EntityAI::DrawInPass3() const
{
  Object *parent = GetHierachyParent();
  if (parent)
  {
    EntityAI *veh = dyn_cast<EntityAI>(parent);
    if (veh && veh->DrawInPass3())
      return true;
  }

  CameraType cam = GWorld->GetCameraTypePreferInternal();
  // when there is a camera effect, we cannot use cockpit pass
  if (GWorld->CameraOn() == this && GWorld->GetCameraEffect() == NULL)
  {
    // when 1st person, pass 3 is always needed
    if (cam == CamInternal) return true;  // cockpits
    // when looking through 3d optics, pass 3 is always needed
    if (cam == CamGunner)
    {
      AIBrain *unit = GWorld->FocusOn();
      Person *person = unit ? unit->GetPerson() : NULL;
      if (person)
      {
        LODShapeWithShadow *oShape = GetOpticsModel(person);
        if (oShape)
          return true;
      }
    }
  }
  // otherwise perform default processing
  return false;
};

/*!
\patch 5127 Date 2/7/2007 by Ondra
- Fixed: Fully closed vehicle interiors were rendered with strange gray shadows with shadow detail Normal or lower.
*/

float EntityAIFull::Pass3IntensityMod(int level, const DrawParameters &dp) const
{
  // Determine own z-space usage
  bool zSpace = dp.ZSpace()!=ZSpaceNormal;
  // Determine whether shadows should be casted and get the shadow coefficients
  float castShadowDiff;
  float castShadowAmb;
  CastPass3VolShadow(level, zSpace, castShadowDiff, castShadowAmb);
  return castShadowDiff;
}

/**
@return tan of the angle difference between current and wanted weapon direction
*/
float EntityAIFull::ComputeAimError(Vector3Par ap, const TurretContext & context, int weapon, Target * target, bool exact) const
{
  // world space direction we need to aim accurately
  Vector3 aimDir;
  // calculate what direction should the weapon be aiming to
  float timeToLead;
  // if aim is not possible, return impossible
  if (!CalculateAimWeapon(context, weapon, aimDir, timeToLead, target, exact))
    return FLT_MAX;
  Vector3 curDir = GetWeaponDirection(FutureVisualState(), context,weapon);

  // check angular difference in X/Y separately

#if _ENABLE_CHEATS
  if (CHECK_DIAG(DECombat) && this == GWorld->CameraOn())
  {
    Matrix3 weaponToWorld(MDirection,curDir,VUp);
    Matrix3 worldToWeapon(MInverseRotation,weaponToWorld);
    Vector3 aimDirRel = worldToWeapon*aimDir;
    float dist = ap.Distance(FutureVisualState().Position());
    float alphaX = atan2(aimDirRel.X(),aimDirRel.Z());
    float alphaY = atan2(aimDirRel.Y(),aimDirRel.Z());
    DIAG_MESSAGE(2000,"Error X %.3f, Y %.3f (dist %.1f, alt %.1f)",alphaX*dist,alphaY*dist,dist,ap.Y()-FutureVisualState().Position().Y());
  }
#endif


  if (GIsManual(context._gunner))
  {
    // get fov from the current camera settings
    //CameraType camType = GWorld->GetCameraType();
    //float fov = GScene->GetCamera()->Left();
    float fov = GetCameraFOV();
    // let vehicle adjust the weapon depending on fov
    curDir = AdjustWeapon(context, weapon,fov, curDir);
  }

  // check current direction, and depending on weapon type estimate the error
  // assume the error is proportional to distance and angle difference

  float cosAlpha = curDir.CosAngle(aimDir);
  if (cosAlpha <= 0.0f)
    return FLT_MAX;
  // if we have cosAlpha^2, following computation could be optimized somewhat
  // probably not worth the effort, though
  // cosAlpha may be above 1 due to rounding errors
  float tanAlpha = (cosAlpha < 1.0f) ? sqrt(1.0f - Square(cosAlpha)) / cosAlpha : 0.0f;

  return tanAlpha;
}


float EntityAIFull::CheckAimed(
  Vector3Par ap, Vector3 speed, const TurretContext &context, int weapon, Target *target, bool exact, const AmmoType *ammo
) const
{
  float dist = ap.Distance(FutureVisualState().Position());
  float error = ComputeAimError(ap, context, weapon, target, exact);
  if (error>=FLT_MAX)
    return 0.0f;

  error *= dist;

  Vector3 leadSpeed = speed - FutureVisualState().Speed();

  float aimPrecision = (GetInvAbility(AKAimingAccuracy) - 1.0f) * 1.5f + 1.0f;
  saturateMax(aimPrecision, 1.0f);

  float tgtSize = target->idExact->AimingSize() * aimPrecision;
  float maxError = tgtSize + ammo->indirectHitRange * 0.3f;
  const WeaponModeType *mode = context._weapons->_magazineSlots[weapon]._weaponMode;
  maxError += dist * mode->_dispersion * aimPrecision;

  if (leadSpeed.SquareSize() > Square(10))
    maxError *= 2.0f;
  else if (mode->_autoFire && mode->_reloadTime < 0.1f)
    maxError *= 2.0f; // more aggressive use of high rate continuous fire weapon - suppress the enemy

#if _ENABLE_CHEATS
  if ((Object *)this == GWorld->CameraOn() && CHECK_DIAG(DECombat))
    DIAG_MESSAGE(2000,Format("Error %.1f, tgtSize %.1f, maxError %.1f",error,tgtSize,maxError));
#endif
  return error < maxError;
}

float EntityAIFull::GetAimed(const TurretContext &context, int weapon, Target *target, bool exact, bool checkLockDelay) const
{
  if (weapon < 0 || weapon >= context._weapons->_magazineSlots.Size())
    return 0.0f;
  if (!target || !target->idExact)
    return 0.0f;

  const Magazine *magazine = context._weapons->_magazineSlots[weapon]._magazine;
  const MagazineType *aInfo = magazine ? magazine->_type : NULL;
  const AmmoType *ammo = aInfo ? aInfo->_ammo : NULL;
  if (!ammo)
    return 1.0f;
  
  if (!CheckTargetKnown(context,target))
    return 0.0f;
  if (!context._gunner)
    return 0.0f;

  AIBrain *unit = context._gunner ? context._gunner->Brain() : NULL;
  if (!unit)
    return 0.0f;
  
  if (!exact && (CheckSuppressiveFire() < SuppressYes) && !target->AccurateEnoughForHit(ammo,unit))
    return 0.0f;

  Vector3 speed;
  Vector3 ap;
  float seenBefore;
  float visible = GetAimingPosition(ap, speed, seenBefore, exact, context, target, ammo);

  if (!exact)
  {
    // even when suppressive fire is allowed, we require a visibility
    // in such case the visibility returned from GetAimingPosition is against the last seen position
    if (visible < 0.3f)
      return 0.0f;
    if (CheckSuppressiveFire()<SuppressYes)
    {
      // check if target was seen recently
      if (target->GetLastSeen() < Glob.time - 5.0f)
        return 0.0f;
    }
    else
    {
      float pauseWanted = SuppressiveFireInterval(seenBefore, context, GetSuppressiveFireBoost());
      // if pause was not long enough, wait some more
      if (pauseWanted>=FLT_MAX || context._weapons->_lastShotTime>Glob.time-pauseWanted)
        return 0.0f;
    }
  }

  if (ammo && ammo->_simulation != AmmoShotMissile && ammo->_simulation!=AmmoShotRocket)
  {
    return CheckAimed(ap, speed, context, weapon, target, exact, ammo)*visible;
  }
  else
  {
    Vector3 relPos=ap-FutureVisualState().PositionModelToWorld(GetWeaponPoint(FutureVisualState(),context, weapon));
    // check if target is in front of us
    if (relPos.SquareSize() <= Square(30))
      return 0.0f; // missile fire impossible
    // check if target position is withing missile lock cone
    Vector3 wDir=GetWeaponDirection(FutureVisualState(), context, weapon);
    float wepCos=relPos.CosAngle(wDir);
    float wepAimed = 1.0f - (1.0f - wepCos) * 30.0f;
    saturateMax(wepAimed, 0.0f);


    return visible * wepAimed;
  }
}

bool EntityAIFull::GetWeaponLockReady(const TurretContext &context, Target *target, int weapon, float aimed)
{
  
  //weapon lock delay

  if(!context._weapons->_lockingTarget || context._weapons->_lockingTarget != target || aimed < 0.25f)
  {
    if(weapon >= 0) context._weapons->_timeToMissileLock = Glob.time + context._weapons->_magazineSlots[weapon]._weapon->_weaponLockDelay;
  }
  context._weapons->_lockingTarget = target;

  if(Glob.time < context._weapons->_timeToMissileLock)  return false;
  return true;
}

typedef StaticArrayAuto< OLink(EntityAI) > EntityAIList;

static void CheckNearFriendlies(EntityAIList &res, const EntityAI *from, const EntityAI *target,
  Vector3Par dir, float maxDistance, float maxAside, float shotSpread)
{
  // check all near friendly troops
  // do not check myself and target
  // collect information about near troops
  int xMin,xMax,zMin,zMax;
  Vector3Val fromPos = from->FutureVisualState().Position();
  ObjRadiusRectangle(xMin,xMax,zMin,zMax,fromPos,fromPos,maxDistance);
  AIGroup *grp = from->GetGroup();
  if (!grp)
    return;
  AICenter *center = grp->GetCenter();
  if (!center)
    return;

  for (int z=zMin; z<=zMax; z++) for (int x=xMin; x<=xMax; x++)
  {
    const ObjectList &list = GLandscape->GetObjects(z,x);
    for (int oi=0; oi<list.Size(); oi++)
    {
      Object *obj = list[oi];
      Vector3Val tgtPos = obj->FutureVisualState().Position();
      
      // if the object is static, we do not have to care
      // if line-of-sight test returned we are clear, we know we are
      if (obj->Static())
        continue;
      
      if (tgtPos.Distance2(fromPos)>Square(maxDistance))
      {
#if 0 //_ENABLE_CHEATS
        EntityAI *tgt = dyn_cast<EntityAI>(obj);
        if (!tgt)
          continue;
        LogF("%s: Friendly %s too far (%.1f)",
          (const char *)from->GetDebugName(),
          (const char *)tgt->GetDebugName(),
          tgtPos.Distance(fromPos));
#endif
        continue;
      }
      EntityAI *tgt = dyn_cast<EntityAI>(obj);
      if (!tgt)
        continue;
      // target or we cannot prevent LOS
      if (tgt==target || tgt==from)
        continue;
      // check side of the unit
      TargetSide side = tgt->GetTargetSide();
      if (!center->IsFriendly(side))
        continue;
      if (tgt->IsDamageDestroyed())
        continue;

      // if the object is empty, it will not move, and if we hit it by accident, we will do little harm anyway
      if (!tgt->CommanderUnit())
        continue;      

      // calculate aside distance
      // this is distance of tgt from line fromPos,dir

      Vector3 tgtAimPos = tgt->AimingPosition(tgt->FutureVisualState());
      // find nearest point on line fromPos,dir to tgtAimPos
      float t = dir*(tgtAimPos-fromPos);
      // t is front-back distance from fromPos to tgtAimPos
      if (t<-maxAside*0.5f)
      { // if the friendly unit is behind us, we do not care about it
        /*
        LogF
        (
          "%s: Friendly %s out of aside range (back %.1f)",
          (const char *)from->GetDebugName(),
          (const char *)tgt->GetDebugName(),
          -t
        );
        */
        continue;
      }

      // dir is normalized - for such case NearestPointInfinite can be simplified a lot
      //Vector3 nearestLOF = NearestPointInfinite(fromPos,fromPos+dir,tgtAimPos);
      Vector3 nearestLOF = fromPos+dir*t;
      float asideWanted = floatMin(maxAside,tgt->CollisionSize()*2.0f);
      
      //shotgun spread
      if(shotSpread >0) asideWanted += tan(shotSpread) * fromPos.Distance(nearestLOF);

      // nearest point on the line of fire
      // we predict units movement - how fast is it closing to the line of fire
      Vector3 dirToLOF = (nearestLOF-tgtAimPos).Normalized();
      float movingIntoFire = tgt->PredictVelocity()*dirToLOF;
      // if standing still or moving away, we should not be afraid to fire even when it is quite close
      if (movingIntoFire>0)
      {
        const float estT = 0.5f;
        asideWanted = maxAside+movingIntoFire*estT;
      }

      float nDist2 = nearestLOF.Distance2(tgtAimPos);
      if (nDist2>Square(asideWanted))
      {
        /*
        LogF
        (
          "%s: Friendly %s out of aside range (aside %.1f)",
          (const char *)from->GetDebugName(),
          (const char *)tgt->GetDebugName(),
          nearest.Distance(tgtPos)
        );
        */
        continue;
      }
      /*
      LogF
      (
        "%s: Near friendly - %s",
        (const char *)from->GetDebugName(),
        (const char *)tgt->GetDebugName()
      );
      */
      res.Add(tgt);
    }
  }
}

bool EntityAIFull::CheckFriendlyFire(const TurretContext &context, int weapon, Target *target) const
{
  // if we have no ammo, we can safely fire, we can do no harm
  const AmmoType *ammo = context._weapons->GetAmmoType(weapon);
  if (!ammo)
    return true;

  float shotSpread =0;
  switch(ammo->_simulation)
  {
    case AmmoShotSpread: //case AmmoShotSpread:
      if(weapon >= 0 && weapon < context._weapons->_magazineSlots.Size())
      {
        const MagazineSlot &slot = context._weapons->_magazineSlots[weapon];
        const MuzzleType *muzzle = slot._muzzle;
        shotSpread = (H_PI/180) * muzzle->_shotSpreadAngle;
      }
    case AmmoShotBullet: //case AmmoShotShell:
    {
      typedef OLink(EntityAI) HelperType;
      AUTO_STATIC_ARRAY(HelperType,nearFriendlies,32);
      // check near part of bullet trajectory
      // 
      Vector3 dir = GetWeaponDirection(FutureVisualState(), context, weapon);

      float distToTarget = target->AimingPosition().Distance(FutureVisualState().Position());
      // if target is very near, be less careful about friendly fire
      // note: the radius seems to be tuned for humans only
      // however, CheckNearFriendlies checks other vehicle types as well
      // still, friendly firing at human is most common and most fatal, therefore we are most interested in it
      float radius = 2.0f;
      float maxDist = 50;
      if (distToTarget<maxDist)
      {
        maxDist = distToTarget;
        radius = 1.0f;
        if (distToTarget<25) radius = 0.5f;
      }
      CheckNearFriendlies(nearFriendlies,this,target->idExact,dir,maxDist,radius,shotSpread);
      if (nearFriendlies.Size()>0)
        // some friendly unit blocks fire - we have to wait
        return false;
    }
  }
  // default: unknown weapon - assume it is safe to fire
  return true;
}

/*!
Armored vehicles do not care much for collisions, but
for soft vehicles, air vehicles or men collision can do much damage
or it can be even critical. 
*/

float EntityAI::AfraidOfCollision(VehicleKind with) const
{
  switch( GetType()->GetKind())
  {
    case VArmor:
      return 1;
    case VSoft:
      if (with==VSoft)
        return 1;
      return 2;
    default:
      return 4; // be very careful
  }
}

/// magnetize aiming speed
// if difference is very low, there is no need to "magnetize"
// if difference is very high, user probably wants to to somethings else
static float Magnetize(float aimX, float autoAimX, float factor, float veryLow, float normal, float veryHigh)
{
  float diff = fabs(aimX-autoAimX);
  float magFactor = 0;
  const float veryLowMag = 0.3f;
  const float normalMag = 1.0f;
  const float veryHighMag = 0.1f;
  if (diff>=veryHigh)
    magFactor = veryHighMag;
  else if (diff>=normal)
    // normal..veryHigh max. magnetization with normal, veryHighMag with veryHigh
    magFactor = (diff-normal)/(veryHigh-normal)*(veryHighMag-normalMag)+normalMag;
  else if (diff>=veryLow)
    // veryLow..normal max. magnetization with normalMag, veryLowMag with veryLow 
    magFactor = (diff-veryLow)/(normal-veryLow)*(normalMag-veryLowMag)+veryLowMag;
  else
    magFactor = veryLowMag;
  /*
  DIAG_MESSAGE(
    500,Format(
      "Mag factor %+.3f, diff %+.3f, normal %+.3f",
      magFactor,diff,normal
    )
  );
  */
  magFactor *= factor;
  
  // if seems some things should never be done:
  // 1) never change sign
  // 2) never move when user does not move
  //float magnetized = aimX*(1-factor)+autoAimX*factor;
  //return magFactor*magnetized+(1-magFactor)*aimX;
  float magnetized = aimX*(1-magFactor)+autoAimX*magFactor;
  
  if (magnetized*aimX<0)
    // we attempt to change a sign - this is bad
    return 0.01f*fSign(aimX);
  return magnetized;
}

/// adjust cursor movement based on user input and 
void EntityAIFull::AutoAimCursor(const TurretContextEx &context, ValueWithCurve &aimX, ValueWithCurve &aimY, float deltaT, float fov)
{
  if (/*Square(aimX._value)+Square(aimY._value)<=Square(0.005) ||*/ deltaT<=0)
    return;
  int weapon = context._weapons->ValidateWeapon(context._weapons->_currentWeapon);
  bool autoAim = true;
  Vector3 weaponPos = FutureVisualState().PositionModelToWorld(GetWeaponPoint(FutureVisualState(), context, weapon));
  if (weapon>=0 && weapon<context._weapons->_magazineSlots.Size())
  {
    const MagazineSlot &slot = context._weapons->_magazineSlots[weapon];
    const MuzzleType *muzzle = slot._muzzle;
    autoAim = muzzle->_canAutoAim;
  }
  if(!autoAim || !Glob.config.IsEnabled(DTAutoAim)) return;
  Vector3 cursorDir = GetWeaponDirectionWanted(context, weapon);
  Target *autoAimTarget = _autoAimLocked.NotNull() ?
    safe_cast<Target *>(_autoAimLocked) : SelectAutoAimTarget(context, weapon,cursorDir,4,false);
  if (autoAimTarget)
  {
    // check which target is autoaim-locked
    TargetType *tgt = autoAimTarget->idExact;
    if (tgt && weapon>=0)
    {
      Vector3 weaponPosModel = GetWeaponPoint(FutureVisualState(), context, weapon);
      //Vector3 weaponPos = PositionModelToWorld(GetWeaponPoint(weapon));
      Vector3 tgtSpd = DirectionWorldToModel(tgt->PredictVelocity() - PredictVelocity());
      // this requires z-coordinate from the position
      // we need predicted aiming position for given weapon
      Vector3 aimRelPos = PositionWorldToModel(tgt->AimingPosition(tgt->FutureVisualState()))-weaponPosModel;
      
      Vector3 aimPos;
      float timeToLead;
      bool aim = CalculateAimWeapon(context, weapon, aimRelPos, timeToLead,autoAimTarget, true);
      if (aim)
        aimRelPos = DirectionWorldToModel(aimRelPos);     
      
      // calculate angular speed
      // this is a speed needed to keep cursor position to the relative target unchanged
      float autoAimX = atan2(tgtSpd.X(),aimRelPos.Z());
      float autoAimY = atan2(tgtSpd.Y(),aimRelPos.Z());

      // besides of magnetization apply slight auto-aim
      if (aim)
      {
        // calculate angular speed needed to aim at the target within given time
        // auto-aim time should depend on current fov
        const float invAimTime = fov/2.0f;
        Vector3 wepDir = GetWeaponDirectionWanted(context, weapon);
        // convert to model space
        Vector3 wepDirModel = DirectionWorldToModel(wepDir);
        // aimRelPos is already in model space
        // x,y difference
        // note: if aimRelPos.Z() is big, atan2 may be replaced with 1/aimRelPos.Z()
        float aimDifX = atan2(aimRelPos.X(),aimRelPos.Z())-atan2(wepDirModel.X(),wepDirModel.Z());
        float aimDifY = atan2(aimRelPos.Y(),aimRelPos.Z())-atan2(wepDirModel.Y(),wepDirModel.Z());
        
        aimDifX *= invAimTime;
        aimDifY *= invAimTime;
  
#if 0
        DIAG_MESSAGE(500,Format("Aim %+.3f,%+.3f -> %+.3f,%+.3f, spd %+.3f,%+.3f",
            aimX._value,aimY._value,autoAimX,autoAimY,aimDifX,aimDifY));
#endif
        autoAimX += aimDifX;
        autoAimY += aimDifY;
      }
      
      //const float factor = GWorld->GetCameraType()!=CamGunner ? 0.9f : 0.5f;
      //const float factor = 0.9f;
      // magnetize aiming
      // what is "low" depends on target distance
      //float veryLow = ( aimRelPos.Z()>25 ? 2.5f/aimRelPos.Z() : 2.5f/100 );
      //float normal = veryLow*2;
      //float veryHigh = veryLow*4;

      // apply curve centered around autoAimX, autoAimY
      // centering should be done by stretching the curve
      // we need both positive and negative area
      //IActionResponseCurve curve;
      
      //aimX._value = Magnetize(aimX._value,autoAimX,factor,veryLow,normal,veryHigh);
      //aimY._value = Magnetize(aimY._value,autoAimY,factor,veryLow,normal,veryHigh);
      aimX._center = autoAimX;
      aimY._center = autoAimY;

      //DIAG_MESSAGE(500,Format(" -> %.2f,%.2f",aimX._value,aimY._value));
    }
  }
}

/*!
\patch 2.01 Date 1/8/2003 by Ondra
- New: Auto aiming option.
@param enlarge multiplying factor controlling autoaim area
@param limitSize when true, only reasonably large targets are auto aimed
*/

Target *EntityAIFull::SelectAutoAimTarget(const TurretContext &context, int weapon, Vector3Par direction, float enlarge, bool limitSize) const
{
  const float maxAllowDist = Glob.config.GetAutoAimDistance()*enlarge;
  const float maxAngle = Glob.config.GetAutoAimAngle()*enlarge;
  const float autoAimSizeFactor = Glob.config.GetAutoAimSizeFactor();
  
  AIBrain *unit = EffectiveGunnerUnit(context);
  if (!unit) return NULL;
  
  //bool autoAim = true;
  Vector3 weaponPos = FutureVisualState().PositionModelToWorld(GetWeaponPoint(FutureVisualState(),context, weapon));
  //if (weapon>=0 && weapon<NMagazineSlots())
  //{
  //  const MagazineSlot &slot = GetMagazineSlot(weapon);
  //  const MuzzleType *muzzle = slot._muzzle;
  //  autoAim = muzzle->_canAutoAim;
  //}
  
  // check if any target is close enough to the aiming direction
  const TargetList *tgts = unit->AccessTargetList();
  if (!tgts) return NULL;
  float minDist = FLT_MAX;
  Target *nearestTgt = NULL;
  // TODO: use Unknown as well
  for (int i=0; i<tgts->EnemyCount(); i++)
  {
    Target *tgt = tgts->GetEnemy(i);
    if (!tgt) continue;
    //if (!tgt->isKnown) continue;
    TargetType *tgtExact = tgt->idExact;
    if (tgtExact==NULL || tgtExact==this) continue;
    if (!tgtExact->IsInLandscape()) continue;
    //if (tgt->posError.SquareSize()>Square(1)) continue;
    // check based on real side
    TargetSide side = tgtExact->GetTargetSide();
    if (!unit->IsEnemy(side) && side!=TSideUnknown) continue;
    
    // if angle to target is too high, do not aim
    Vector3 aimPos = tgtExact->AimingPosition(tgtExact->FutureVisualState());
    float cosAlpha = direction.CosAngle(aimPos-weaponPos);
    if (cosAlpha<0) continue; // behind us - ignore

    // if possible, calculate position including lead
    //bool aimed =
    float timeToLead;
    CalculateAimWeaponPos(context, weapon,aimPos,timeToLead,tgt,true);
    
    // check distance of the target from the direction line
    float dist = NearestPointDistance(weaponPos,weaponPos+direction,aimPos,0,FLT_MAX);

    LODShape *shape = tgtExact->GetShape();
    float visibleRadius = shape ? shape->GeometrySphere() : 0.1f;

    float tgtRadius = visibleRadius;
    tgtRadius *= autoAimSizeFactor;
    if (dist-tgtRadius>maxAllowDist) continue;
    if (dist>minDist) continue;
    // if we are pointing outside of the aiming sphere, check how much out we are
    if (dist>tgtRadius)
    {
      float tgtDist = aimPos.Distance(weaponPos);
      // check angular distance
      // angular size of given target
      float gamma = atan2(tgtRadius,tgtDist);
      // angular size of distance of given target from the weapon line
      float alpha = atan2(dist,tgtDist);
      float angle = alpha-gamma;
      // note: break was here - but it makes no sense.
      // If the target is ignored, we should check another one
      if (angle>maxAngle) continue;
    }
    if (limitSize)
    {
      // check visual size of the target
      // check fogginess
      // reasonable size calculated on TV screen (480 lines display)
      const float minSize = 20.0f/480;
      float tgtDist = aimPos.Distance(weaponPos);
      // we use linear approximation, however we want the attenuation
      // to start quite far from the viewer
      const float fogFactor = 1.6f;
      float fogAttenuation = floatMin(1,fogFactor-tgtDist*fogFactor/Glob.config.tacticalZ);
      
      #if _DEBUG
        float screenSize = GScene->GetCamera()->InvTop()*(2*visibleRadius)/tgtDist;
        (void)screenSize;
      #endif //if (screenSize<minSize) continue;
      if (GScene->GetCamera()->InvTop()*visibleRadius*fogAttenuation<(minSize*0.5f)*tgtDist) continue;
    }
    // possible optimization:
    // atg x1 - atg x2 = atg ((x1-x2)/(1+x1*x2))
    // alpha-gamma>maxAngle should be equivalent to
    // atg(dist/tgtDist) - atg(tgtRadius/tgtDist) > maxAngle
    // ((dist-tgtRadius)*tgtDist) /(tgtDist^2+dist*tgtRadius)> tg maxAngle
    // ((dist-tgtRadius)*tgtDist) > tg maxAngle * (tgtDist^2+dist*tgtRadius)

    // check if engaging target with the weapon makes sense in given situation
    bool fire = weapon>=0 && ShootHasSense(*tgt,context,weapon,dist);
    if (!fire) continue;
    // check if there is currently some visibility
    // if not, ignore the target
    bool headOnly = false;
    float visibility = GetTrackedVisibility(headOnly,unit,context,weapon,*tgt,0.9f);
    if (visibility<0.1f) continue;
    // GetTrackedVisibility return if we can shoot through
    // we should check if we can really see the target
    /*float visible=GLandscape->Visible
    (
      weaponPos,this,tgtExact,0.9f,ObjIntersectView
    );*/
    float visible = GWorld->Visibility(unit,tgtExact,0.15f);
    if (visible<0.1f) continue;

    minDist = dist;
    nearestTgt = tgt;
  }
  return nearestTgt;
}

/**
This function is used to select a target for weapons which can auto-lock on a plane
*/
Target *EntityAIFull::SelectLockTarget(const TurretContext &context, int weapon, Vector3Par direction, const AmmoType *ammo, float minAngle) const
{
  const float minCosAngle = (minAngle < 0)? 0.93969262079 : minAngle; // cos 20deg
  
  AIBrain *unit = EffectiveGunnerUnit(context);
  if (!unit) return NULL;
  
  Vector3 weaponPos = RenderVisualState().PositionModelToWorld(GetWeaponPoint(RenderVisualState(),context, weapon));
  
  // physical device - check real positions
  const TargetList *tgts = unit->AccessTargetList();
  if (!tgts) return NULL;
  float maxCosAngle = minCosAngle;
  Target *nearestTgt = NULL;
  // we might optimize this, but as it is used only for player, it does not seem necessary
  for (int i=0; i<tgts->AnyCount(); i++)
  {
    Target *tgt = tgts->GetAny(i);
    if (!tgt) continue;
    //if (!tgt->isKnown) continue;
    TargetType *tgtExact = tgt->idExact;
    if (tgtExact==NULL || tgtExact==this) continue;
    if (!tgtExact->IsInLandscape()) continue;
    
    // if angle to target is too high, do not aim
    Vector3 aimPos = tgtExact->AimingPosition(tgtExact->RenderVisualState());
    float cosAlpha = direction.CosAngle(aimPos-weaponPos);
    if (cosAlpha<maxCosAngle) continue; // no better - ignore

    if (!tgtExact->LockPossible(ammo))
      continue;

    // if not, ignore the target
    bool headOnly = false;
    float visibility = GetTrackedVisibility(headOnly,unit,context,weapon,*tgt,0.9f);
    if (visibility<0.1f)
      continue;
    // GetTrackedVisibility return if we can shoot through
    // we should check if we can really see the target
    float visible = GWorld->Visibility(unit,tgtExact,0.15f);
    if (visible<0.1f)
      continue;

    maxCosAngle = cosAlpha;
    nearestTgt = tgt;
  }
  return nearestTgt;
}

bool EntityAIFull::IsAutoAimEnabled(const TurretContext &context, int weapon) const
{
  bool autoAim = true;
  if (weapon >= 0 && weapon < context._weapons->_magazineSlots.Size())
  {
    const MagazineSlot &slot = context._weapons->_magazineSlots[weapon];
    const MuzzleType *muzzle = slot._muzzle;
    autoAim = muzzle->_canAutoAim;
  }
  return autoAim;
}

/// in a small area around the aim point the player may override autoaim

const float AutoAimOverride = 0.15f;

void EntityAIFull::AimWeaponManDir(const TurretContextEx &context, int weapon, Vector3Par direction )
{
  _autoAimLocked = NULL;
  AIBrain *unit = EffectiveGunnerUnit(context);
  
  bool autoAim = IsAutoAimEnabled(context, weapon);
  
  if( unit && autoAim && Glob.config.IsEnabled(DTAutoAim) && GWorld->GetCameraType()!=CamGunner)
  {
    Target *nearestTgt = SelectAutoAimTarget(context, weapon, direction);
    if (nearestTgt)
    {
      Vector3 adjDir;
      // lead target, consider gravity ...
      float timeToLead;
      if (!CalculateAimWeapon(context, weapon,adjDir,timeToLead,nearestTgt,true)) return;
      
      // calculate how large the hit area around the target is
      EntityAI *tgtExact = nearestTgt->idExact;
      if (!tgtExact) return;
      LODShape *shape = tgtExact->GetShape();
      float visibleRadius = shape ? shape->GeometrySphere()*AutoAimOverride : 0.1f;
      // angle of the hit area
      float maxAngle = visibleRadius*adjDir.InvSize();
      
      // force direction toward the adjDir, keep max. difference at angle
      float diffCosAngle = direction.CosAngle(adjDir);
      float diffAngle = acos(diffCosAngle);
      if (diffAngle<maxAngle)
      {
        // we are within the autoaim area, do not change direction at all
#if 0 //_PROFILE
        DIAG_MESSAGE(100,Format("Angle %g",diffAngle/maxAngle));
#endif
        adjDir = direction;
      }
      else
      {
        Vector3 adNorm = adjDir.Normalized();
        Vector3 dirDiff = direction.Normalized()-adNorm;
        dirDiff.Normalize();
        adjDir = adNorm + dirDiff*maxAngle;
      }
      
      AimWeaponAdjusted(context, weapon, adjDir);
      _autoAimLocked = nearestTgt;
      return;
    }
  }
  AimWeapon(context, weapon, direction);
}

float EntityAIFull::GetAimAccuracy(const TurretContext &context, int weapon, Vector3Par direction) const
{
  Target *nearestTgt = SelectAutoAimTarget(context, weapon, direction);
  if (!nearestTgt)
    return 0.0f;
  // calculate how large the hit area around the target is
  float autoaimAcc = 1.0f;
  EntityAI *tgtExact = nearestTgt->idExact;
  if (tgtExact)
  {
    Vector3 adjDir;
    // lead target, consider gravity ...
    float timeToLead;
    if (CalculateAimWeapon(context, weapon, adjDir, timeToLead, nearestTgt, true))
    {
      LODShape *shape = tgtExact->GetShape();
      float visibleRadius = shape ? shape->GeometrySphere() * AutoAimOverride : 0.1f;
      // angle of the hit area
      float maxAngle = visibleRadius * adjDir.InvSize();
      
      // force direction toward the adjDir, keep max. difference at angle
      float diffAngle = acos(direction.CosAngle(adjDir));
      autoaimAcc = (diffAngle < maxAngle) ? 1.0f - 0.5f * diffAngle / maxAngle : 0.5f;
    }
  }
  // if not aimed, do not indicated auto-aim accuracy  
  float aimed = GetAimed(context, weapon, nearestTgt, true, false);
  return aimed * autoaimAcc;
}

bool EntityAIFull::GetWeaponCartridgePos(const TurretContext &context, int weapon, Matrix4 &pos, Vector3 &vel) const
{
  ObjectVisualState const& vs = RenderVisualState();

  if (weapon >= 0 && weapon < context._weapons->_magazineSlots.Size())
  {
    const MuzzleType *muzzle = context._weapons->_magazineSlots[weapon]._muzzle;

    if (muzzle->_cartridgeOutPosIndex<0 || muzzle->_cartridgeOutEndIndex<0)
      return false;
    int memLevel = _shape->FindMemoryLevel();

    Vector3 cPos = AnimatePoint(vs,memLevel,muzzle->_cartridgeOutPosIndex);
    Vector3 cEnd = AnimatePoint(vs,memLevel,muzzle->_cartridgeOutEndIndex);

    Vector3 cDir = GetWeaponDirection(vs, context, weapon);

    vs.PositionModelToWorld(cPos,cPos);
    vs.PositionModelToWorld(cEnd,cEnd);

    vel = (cEnd - cPos)*3;

    // create matrix for cartridge position
    pos.SetDirectionAndUp(cDir,VUp);
    pos.SetPosition(cPos);
    return true;
  }
  // convert neutral gun position
  pos = MIdentity;
  vel = VZero;
  return false;
}

bool EntityAIFull::SetWeaponReloadTime(RString muzzleName, float reloadTime)
{
#if 1//_VBS3_WEAPON_PROPS
  if (0 > reloadTime) {reloadTime = 0;}

  AIBrain *agent = GWorld->FocusOn();
#if 1 //_VBS3_MULTIPLAYER_SERIALIZATION
  if (!agent || agent->GetLifeState() != LifeStateAlive) {return false;}
#else
  if (!agent || agent->GetLifeState() != AIUnit::LSAlive) {return false;}
#endif

  TurretContextEx currContext; 
#if _VBS3 //commander override
  bool currContextValid = this->FindControlledTurret(agent->GetPerson(), currContext);
#else
  bool currContextValid = this->FindTurret(agent->GetPerson(), currContext);
#endif
  if (!currContextValid) {return false;}

  WeaponsState *weapons = currContext._weapons;

  bool found = false;
  for (int ind = 0; ind < weapons->_magazineSlots.Size(); ind++)
  {
    RString curName = weapons->_magazineSlots[ind]._muzzle->_displayName;
    if (!strnicmp(muzzleName.Data(), curName.Data(), muzzleName.GetLength()))
    {
      weapons->_magazineSlots[ind]._weaponMode->_reloadTime = reloadTime;
      found = true;
    }
  }
  return found;
#else
  return false;
#endif
}

bool EntityAIFull::GetWeaponLoaded(const WeaponsState &weapons, int weapon) const
{
  if (weapon < 0 || weapon >= weapons._magazineSlots.Size()) return true;
  const Magazine *magazine = weapons._magazineSlots[weapon]._magazine;
  if (!magazine || !magazine->_type) return true; // no ammo
  const AmmoType *ammo = magazine->_type->_ammo;
  if (!ammo) return true; // no ammo
  if (ammo->_simulation == AmmoNone) return true; // no ammo
  if (magazine->_reload > 0) return false; // not loaded
  if( magazine->_reloadMagazine>0 ) return false; // not loaded
  if (IsActionInProgress(MFReload)) return false; // not loaded

  if( magazine->GetAmmo()<=0 ) return false; // no ammo
  return true;
}

bool EntityAIFull::GetWeaponReady(const WeaponsState &weapons, int weapon, Target *target) const
{
  if (weapon < 0 || weapon >= weapons._magazineSlots.Size()) return false;
  const MagazineSlot &slot = weapons._magazineSlots[weapon];
  const WeaponModeType *mode = slot._weaponMode;
  if (!mode) return false;

  if (!target) return true;
  // for guided missiles / bombs wait until previous shot is done
  
  if (GetType()->CanBeAirborne())
  {
    if (weapons._lastShot && slot._magazine && slot._magazine->_type->_ammo)
    {
      const AmmoType *ammo = slot._magazine->_type->_ammo;
      if (ammo->_simulation==AmmoShotMissile && ammo->maxControlRange>10)
      {
        // we are getting ready to fire a guided weapon
        // check if the last shot is a guided weapon
        Missile *pending = dyn_cast<Missile,Entity>(weapons._lastShot);
        if (pending && pending->Type()->maxControlRange>10)
        {
          // a guided shot is pending - wait until it explodes
          // TODO: after a hit we should still wait for a while to check if the target is destroyed
          return false;
        }
        // TODO: if the explosion is pending, wait until we see the result
      }
    }
  }
  float timeFromLastShot = Glob.time-weapons._lastShotTime;
  float rateOfFire = mode->_aiRateOfFire;
  if (timeFromLastShot>rateOfFire) return true;
  float dist2 = target->AimingPosition().Distance2(FutureVisualState().Position());
  float maxDist2 = Square(mode->_aiRateOfFireDistance);
  if (dist2<maxDist2)
    return Square(timeFromLastShot)*maxDist2>Square(rateOfFire)*dist2;
  else
    return timeFromLastShot>rateOfFire;
}


bool EntityAIFull::FireMissile(const TurretContext &context, int weapon, Vector3Par offset, Vector3Par direction, Vector3Par initSpeed,
  TargetType *target, bool createNetworkObject, bool remote, bool forceLock)
{
  if (_isDead || _isUpsideDown)
    return false;

  const AmmoType *type = context._weapons->GetAmmoType(weapon);
  //if( !type || !aInfo ) return false;

  context._weapons->_fire._nextWeaponSwitch = Glob.time+GetInvAbility(AKReloadSpeed)*0.5;

  const MagazineSlot &slot = context._weapons->_magazineSlots[weapon];
  const WeaponModeType *mode = slot._weaponMode;
  Assert(mode);

  bool real;
  if (mode->_autoFire)
  {
    // in this case, fire is processed where the effective gunner is (workaround)
    AIBrain *gunnerUnit = EffectiveGunnerUnit(context);
    real = gunnerUnit ? gunnerUnit->GetPerson()->IsLocal() : IsLocal();
  }
  else
    // correctly, the fire is simulated where the weapon system is local
    real = context._turret ? context._turret->IsLocal() : IsLocal();
  if (!real && !remote)
    return true;
  
  const VisualState &vs = RenderVisualState();
  Vector3 initSpeedWorld = vs.DirectionModelToWorld(initSpeed);

  //add some dispersion for artillery
  if(type->artilleryLock)
  { 
    //set charge power
    initSpeedWorld *= mode->artilleryCharge;
    //adds 1 meter of dispersion with every second of flight 
    Vector3 initSpeedRnd = Vector3((GRandGen.Gauss(0,0.5f,1.0f)- 0.5f),0,(GRandGen.RandomValue(0,0.5f,1.0f)- 0.5f));
    initSpeedWorld += mode->artilleryDispersion * initSpeedRnd;
  }

  Shot *shot = NewShot(this, type, &slot, target, !remote);
  shot->SetOrient(direction, VUp);
  shot->SetSpeed(vs._speed + initSpeedWorld);
  shot->SetPosition(vs.PositionModelToWorld(offset));
  GWorld->AddFastVehicle(shot);
  GWorld->AddSupersonicSource(shot);

  if(Glob.time < context._weapons->_timeToMissileLock && !forceLock)
  {
    Missile *missile = dyn_cast<Missile>(shot);
    if(missile->HasTarget()) missile->UnlockMissile();
  }

  if (createNetworkObject && GWorld->GetMode() == GModeNetware)
    // shot created
    GetNetworkManager().CreateObject(shot);
#if _ENABLE_CHEATS
  if (this==GWorld->CameraOn() && CHECK_DIAG(DECombat))
    GWorld->SwitchCameraTo(shot,CamInternal,true);
#endif

  if (target)
  {
    target->OnIncomingMissile(target, shot, this);
    GetNetworkManager().OnIncomingMissile(target, shot, this);
  }


  AIBrain *unit = CommanderUnit();
  Target *assigned = unit ? unit->GetTargetAssigned() : NULL;
  if (assigned && target==assigned->idExact)
    _lastShotAtAssignedTarget = Glob.time;
  context._weapons->_lastShot = shot;
  AfterFire();
  return true;
}

bool EntityAIFull::FireShell(const TurretContext &context, int weapon, Vector3Par offset, Vector3Par direction,
  TargetType *target, bool createNetworkObject, bool remote)
{
  // fire
  if (_isDead || _isUpsideDown)
    return false;

  context._weapons->_fire._nextWeaponSwitch=Glob.time+GetInvAbility(AKReloadSpeed)*0.5;

  if (weapon < 0 || weapon >= context._weapons->_magazineSlots.Size()) return false;
  const MagazineSlot &slot = context._weapons->_magazineSlots[weapon];
  const Magazine *magazine = slot._magazine;
  if (!magazine) return false;
  const MagazineType *aInfo = magazine->_type;
  const WeaponModeType *mode = slot._weaponMode;
  Assert(mode);
  const AmmoType *type = aInfo ? aInfo->_ammo : NULL;
  if (!type) return false;

  bool real;
  if (mode->_autoFire)
  {
    // in this case, fire is processed where the effective gunner is (workaround)
    AIBrain *gunnerUnit = EffectiveGunnerUnit(context);
    real = gunnerUnit ? gunnerUnit->GetPerson()->IsLocal() : IsLocal();
  }
  else
    // correctly, the fire is simulated where the weapon system is local
    real = context._turret ? context._turret->IsLocal() : IsLocal();
  if (!real && !remote)
    return true;
  

  bool showTracer = magazine->GetAmmo() <= aInfo->_lastRoundsTracer
      || (aInfo->_tracersEvery > 0 
          && ((magazine->GetAmmo() - aInfo->_lastRoundsTracer) % aInfo->_tracersEvery) == 0
         );
  Shot *shot = NewShot(this,type, &slot, NULL,!remote, showTracer);
  
  const VisualState &vs = RenderVisualState();
  const Shot::VisualState &svs = shot->FutureVisualState();

  shot->SetOrient(direction,VUp);
  // do random dispersion of projectiles
  const int gausPassNum=4;
  float gaussX=0,gaussY=0;
  for( int c=gausPassNum; --c>=0;  )
  {
    gaussX+=GRandGen.RandomValue();
    gaussY+=GRandGen.RandomValue();
  }
  gaussX*=2.0/gausPassNum;
  gaussY*=2.0/gausPassNum;

  float randomX=(gaussX-1)*mode->_dispersion;
  float randomY=(gaussY-1)*mode->_dispersion;

  if( QIsManual() )
  {
    IF_FADE()
    {
      randomX *= 20;
      randomY *= 20;
    }
  }

  Vector3 dir(randomX,randomY,1);
  svs.DirectionModelToWorld(dir,dir);
  shot->SetOrient(dir,svs.DirectionUp());
  // final speed and position

  float ammoSpeed = aInfo->_initSpeed;

  if (context._weapons && context._weapons->IsThrowable(weapon))
  {
    // modify speed by coef used for throwing
    float coef = context._weapons->_throwIntensityCoef;
    ammoSpeed *= coef;
    // better reset coef to default
    context._weapons->_throwIntensityCoef = 1.0f;
  }
#if _VBS3 //reduce speed of throwing objects when laying down
  ammoSpeed *= GetAmmoInitSpeedFactor(aInfo);
#endif

  Vector3 initSpeedWorld = svs.Direction()*ammoSpeed;


  //add some dispersion for artillery
  if(type->artilleryLock)
  { 
    //set charge power
    initSpeedWorld *= mode->artilleryCharge;
    //adds 1 meter of dispersion with every second of flight 
    Vector3 initSpeedRnd = Vector3((GRandGen.Gauss(0,0.5f,1.0f)- 0.5f),0,(GRandGen.RandomValue(0,0.5f,1.0f)- 0.5f));
    initSpeedWorld += mode->artilleryDispersion * initSpeedRnd;
  }

  shot->SetSpeed(vs._speed+ initSpeedWorld);
  shot->SetPosition(vs.PositionModelToWorld(offset));
#if _VBS2 // suppression and Morale
  // Inform the soldier's morale class that he's fired a round.
  Man* theShooter = dyn_cast<Man>(this);
  if (theShooter) theShooter->HaveFired();

  // Make a list of soldiers who may be suppressed by round
  ShotShell *ss = dyn_cast<ShotShell>(shot);
  ss->BuildSuppressionList();
#endif
  GWorld->AddFastVehicle(shot);
  GWorld->AddSupersonicSource(shot);
  GWorld->AddFakeBulletsSource(shot);

  if (createNetworkObject && GWorld->GetMode() == GModeNetware)
    // shot created
    GetNetworkManager().CreateObject(shot);

  AIBrain *unit = CommanderUnit();
  Target *assigned = unit ? unit->GetTargetAssigned() : NULL;
  if( assigned && target==assigned->idExact )
    _lastShotAtAssignedTarget = Glob.time;
  context._weapons->_lastShot = shot;

  //ADD_COUNTER(wFire,1);
  AfterFire();
  return true;
}

void EntityAIFull::AfterFire()
{
  _aimRandom = GRandGen.RandomValue();
}


bool EntityAIFull::FireMGun(const TurretContext &context, int weapon,
  Vector3Par offset, Vector3Par direction,
  TargetType *target, bool createNetworkObject, bool remote)
{
  if( _isDead || _isUpsideDown ) return false;

  // fire
  if (weapon < 0 || weapon >= context._weapons->_magazineSlots.Size()) return false;
  const MagazineSlot &slot = context._weapons->_magazineSlots[weapon];
  const Magazine *magazine = slot._magazine;
  if (!magazine) return false;
  const MagazineType *aInfo = magazine->_type;
  if (!aInfo) return false;
  const AmmoType *type = aInfo->_ammo;
  if (!type) return false;
  const WeaponModeType *mode = slot._weaponMode;
  const MuzzleType &info = *slot._muzzle;

  bool real;
  if (mode->_autoFire)
  {
    // in this case, fire is processed where the effective gunner is (workaround)
    AIBrain *gunnerUnit = EffectiveGunnerUnit(context);
    real = gunnerUnit ? gunnerUnit->GetPerson()->IsLocal() : IsLocal();
  }
  else
  {
    // correctly, the fire is simulated where the weapon system is local
    real = context._turret ? context._turret->IsLocal() : IsLocal();
  }
  if (!real && !remote) return true;

  bool showTracer = magazine->GetAmmo() <= aInfo->_lastRoundsTracer
    || (aInfo->_tracersEvery > 0 
         && ((magazine->GetAmmo() - aInfo->_lastRoundsTracer) % aInfo->_tracersEvery) == 0
       );
  Shot *shell=NewShot(this,type, &slot, NULL,!remote, showTracer);
  const Shot::VisualState &svs = shell->FutureVisualState();
  const VisualState &vs = RenderVisualState();

  shell->SetOrient(direction,VUp);
  // do random dispersion of projectiles
  const int gausPassNum=4;
  float gaussX=0,gaussY=0;
  for( int c=gausPassNum; --c>=0; )
  {
    gaussX+=GRandGen.RandomValue();
    gaussY+=GRandGen.RandomValue();
  }
  gaussX*=2.0/gausPassNum;
  gaussY*=2.0/gausPassNum;
  float randomX=(gaussX-1)*(mode->_dispersion);
  float randomY=(gaussY-1)*(mode->_dispersion);
  if( !QIsManual() )
  {
    AIBrain *unit = context._gunner ? context._gunner->Brain() : NULL;
    float invAimingAccuracy = unit ? unit->GetInvAbility(AKAimingAccuracy) : 1;
    float coefX,coefY;
    // never allow AI to have more precise weapons than the player has
    info.GetDispersionCoefs(coefX,coefY,floatMinMax(invAimingAccuracy,1,1e3));
    randomX*=coefX;
    randomY*=coefY;
  }
  else
  {
    IF_FADE()
    {
      randomX *= 20;
      randomY *= 20;
    }
  }


  context._weapons->_fire._nextWeaponSwitch=Glob.time+GetInvAbility(AKReloadSpeed)*0.5;

  Vector3 dir(randomX,randomY,1);
  svs.DirectionModelToWorld(dir,dir);
  shell->SetOrient(dir,svs.DirectionUp());
  // final speed and position
  shell->SetSpeed(vs._speed+dir*aInfo->_initSpeed);
  shell->SetPosition(vs.PositionModelToWorld(offset));
#if _VBS2 // suppression
  // Inform the soldier's morale class that he's fired a round.
  Man* theShooter = dyn_cast<Man>(this);
  if (theShooter) theShooter->HaveFired();

  // Make a list of soldiers who may be suppressed by round.
  ShotShell *ss = dyn_cast<ShotShell>(shell);
  ss->BuildSuppressionList();
#endif
  GWorld->AddFastVehicle(shell);
  GWorld->AddSupersonicSource(shell);
  GWorld->AddFakeBulletsSource(shell);

  if (createNetworkObject && GWorld->GetMode() == GModeNetware)
  {
    // shot created
    GetNetworkManager().CreateObject(shell);
  } 

  AIBrain *unit = CommanderUnit();
  Target *assigned = unit ? unit->GetTargetAssigned() : NULL;
  if( assigned && target==assigned->idExact )
  {
    _lastShotAtAssignedTarget = Glob.time;
  }
  context._weapons->_lastShot = shell;

  //ADD_COUNTER(wFire,1);
  AfterFire();
  return true;
}

bool EntityAIFull::FireCM(const TurretContext &context, int weapon, Vector3Par offset, Vector3Par direction, Vector3Par initSpeed,
                               TargetType *target, bool createNetworkObject, bool remote)
{
  if (_isDead || _isUpsideDown)
    return false;

  const AmmoType *type = context._weapons->GetAmmoType(weapon);
 // context._weapons->_fire._nextWeaponSwitch = Glob.time+GetInvAbility(AKReloadSpeed)*0.5;

  const MagazineSlot &slot = context._weapons->_magazineSlots[weapon];
  const WeaponModeType *mode = slot._weaponMode;
  Assert(mode);

  bool real;
  if (mode->_autoFire)
  {
    // in this case, fire is processed where the effective gunner is (workaround)
    AIBrain *gunnerUnit = EffectiveGunnerUnit(context);
    real = gunnerUnit ? gunnerUnit->GetPerson()->IsLocal() : IsLocal();
  }
  else
  {
    // correctly, the fire is simulated where the weapon system is local
    real = context._turret ? context._turret->IsLocal() : IsLocal();
  }
  if (!real && !remote) return true;

  Shot *shot = NewShot(this, type, &slot, target, !remote);
  const VisualState &vs = RenderVisualState();

  shot->SetOrient(direction, VUp);
  shot->SetSpeed(vs._speed + vs.DirectionModelToWorld(initSpeed));
  shot->SetPosition(vs.PositionModelToWorld(offset));
  GWorld->AddFastVehicle(shot);
  GWorld->AddSupersonicSource(shot);

  _activeCounterMeasures.Add(shot);
  if (createNetworkObject && GWorld->GetMode() == GModeNetware)
    // shot created
    GetNetworkManager().CreateObject(shot);

  //try to confuse incoming missile
  for (int i=_incomingMissiles.Size()-1; i>=0; i--)
  {
    if(!_incomingMissiles[i] || _incomingMissiles[i]->GetTimeToLive()<0) _incomingMissiles.DeleteAt(i);
    else if((shot->Type()->weaponLockSystem  & _incomingMissiles[i]->Type()->weaponLockSystem)>0) 
    {
      if(_incomingMissiles[i]->IsLocal())_incomingMissiles[i]->TestCounterMeasures(shot,1);
      else GetNetworkManager().OnLaunchedCounterMeasures(shot, _incomingMissiles[i], 1);
    }
  }

  //try to confuse vehicle targeting system
  for (int i=0; i<_targetingEnemys.Size(); i++)
  {
    if(_targetingEnemys[i] && _targetingEnemys[i]->CommanderUnit() && _targetingEnemys[i]->CommanderUnit()->GetVehicle())
    {
      TurretContextV context;
      EntityAIFull *vehicle = _targetingEnemys[i]->CommanderUnit()->GetVehicle();
      if(!vehicle->FindTurret(unconst_cast(vehicle->FutureVisualState()), _targetingEnemys[i], context)) continue;
      if(context._weapons->_currentWeapon>=0 && context._weapons->_currentWeapon < context._weapons->_magazineSlots.Size())
      {
        const WeaponType *type = context._weapons->_magazineSlots[context._weapons->_currentWeapon]._weapon;
        //do not send anything if CM can not affect weapon system
        if((shot->Type()->weaponLockSystem & type->_weaponLockSystem) > 0)
        {
          if(_targetingEnemys[i]->IsLocal()) context._weapons->TestCounterMeasures(shot,1);
          else GetNetworkManager().OnLaunchedCounterMeasures(shot, vehicle, 1);
        }
      }
    }
  }

  return true;
}

bool EntityAIFull::CalculateLaser(Vector3 &pos, Vector3 &dir, const TurretContext &context, int weapon, Vector3Par aimDir) const
{
  const VisualState &vs = RenderVisualState();
#if _VBS3_LASE_LEAD
  pos = VZero;
  if (aimDir != VZero)
    dir = aimDir;
  else
#endif //_VBS3_LASE_LEAD
    dir = GetWeaponDirection(vs, context, weapon);

  dir.Normalize();
  Vector3 weaponPos = vs.PositionModelToWorld(GetWeaponPoint(vs,context, weapon));

  //+dir*50;


  // calculate intersection with land
  // between Position() and position
  float maxDist = Glob.config.horizontZ;

  float t = GLandscape->IntersectWithGroundOrSea(&pos,weaponPos,dir,0,maxDist*1.1);
  bool found = false;
  if (t<=maxDist)
  {
    // some intersection with land found
    found = true;
    pos[1] = GLandscape->SurfaceY(pos[0],pos[2]);
    pos[1] += 0.05f;
  }
  else
  {
    t = maxDist;
    pos = weaponPos + maxDist*dir;
  }

  // try to find some intersection with object

  CollisionBuffer collision;
  GLandscape->ObjectCollisionLine(
    GetRenderVisualStateAge(),collision,Landscape::FilterIgnoreTwo(context._weapons->_laserTarget,this),
    weaponPos,pos,0.2f
  );
  if (collision.Size()>0)
  {
    // check first non-glass collision
    float minT = FLT_MAX;
    int minI = -1;

    Texture *glass = GPreloadedTextures.New(TextureBlack);
    for( int i=0; i<collision.Size(); i++ )
    {
      // info.pos is relative to object
      CollisionInfo &info=collision[i];
      // we can go through some textures

      if (info.texture == glass)
        continue;
      if (info.object && minT>info.under)
      {
        minT = info.under;
        minI = i;
      }
    }
    if (minI>=0)
    {
      CollisionInfo &info=collision[minI];
      pos=info.pos;
      // make point a little bit off-surface to make sure it is visible
      pos -= dir*0.07f;
      found = true;
    }
  }

  return found;
}

/*!
\patch 1.28 Date 10/25/2001 by Ondra.
- New: Laser target designators.
\patch 1.58 Date 5/17/2002 by Ondra
- Fixed: AI targeting laser designated targets improved.
*/

void EntityAIFull::StopLaser(const TurretContext &context)
{
  if (context._weapons->_laserTarget)
    context._weapons->_laserTarget->SetDelete();
}

#include "laserTarget.hpp"

void EntityAIFull::TrackLaser(const TurretContext &context, int weapon)
{
  // create a new laser target
  Vector3 pos,dir;
  if (CalculateLaser(pos, dir, context, weapon))
  {
    // Get the laser target origin
    Matrix4 origin;
    // laser target is oriented to face target
    // its forward direction goes away from laser source
    if (dir.Distance2(VUp)>Square(0.2))
      origin.SetDirectionAndUp(dir,VUp);
    else
      origin.SetDirectionAndAside(dir,VAside);
    origin.SetPosition(pos);

    // Either create or move the laser target
    if (!context._weapons->_laserTarget)
    {
      TargetSide side = (context._gunner && context._gunner->Brain()) ? context._gunner->Brain()->GetSide() : GetTargetSide();
      const char *laserName = "LaserTargetC";
      if (side==TEast) laserName = "LaserTargetE";
      else if (side==TWest) laserName = "LaserTargetW";
      EntityAI *ltgt = GWorld->NewVehicleWithID(laserName,"");
      if (ltgt)
      {
        ltgt->Init(origin, true);
        ltgt->SetTransform(origin);
        ltgt->OnEvent(EEInit);
        GWorld->AddVehicle(ltgt);
        GetNetworkManager().CreateVehicle(ltgt, VLTVehicle, "", -1);
        context._weapons->_laserTarget = ltgt;

        LaserTarget *lTarget = dyn_cast<LaserTarget, EntityAI> (ltgt);
        if (lTarget) lTarget->SetOwnerFov(GetCameraFOV());

#if _VBS3
        // set who is shooting this laser target
        LaserTarget *laser = dyn_cast<LaserTarget>(ltgt);
        Assert(laser);

        laser->SetOwner(this);

        const MagazineSlot &slot = context._weapons->_magazineSlots[weapon];

        RemoteFireWeaponInfo remoteInfo;
        remoteInfo._projectile = ltgt;
        // make sure everyone receives the message
        remoteInfo._visible = 4000.0f;

        // Even handler, for local client
        if (IsEventHandler(EEFired) || slot._muzzle->IsEventHandler(MEFired))
        {
          GameValue value = GWorld->GetGameState()->CreateGameValue(GameArray);
          GameArrayType &arguments = value;
          const MagazineSlot &slot = context._weapons->_magazineSlots[weapon];
          arguments.Add(GameValueExt(this));
          arguments.Add(RString(slot._weapon->GetName()));
          arguments.Add(RString(slot._muzzle->GetName()));
          arguments.Add(RString());
          arguments.Add(RString(laserName));
          RString magazineName = slot._magazine ? slot._magazine->_type->GetName() : "";
          arguments.Add(magazineName);

          // Add the laser designator
          arguments.Add(GameValueExt(ltgt));

          if(IsEventHandler(EEFired))
            OnEvent(EEFired,value);
          if(slot._muzzle->IsEventHandler(MEFired))
            slot._muzzle->OnEvent(MEFired,value);
        }

        // Fire the weapon network  message.
        // the projectile/missle should be within remoteInfo
        EntityAI *target = dyn_cast<EntityAI>(laser->GetDesinatedTarget());
        GetNetworkManager().FireWeapon(this, context._gunner, weapon, slot._magazine, target, remoteInfo);
#endif
      }
    }
    else
    {
      context._weapons->_laserTarget->Move(origin);
      LaserTarget *lTarget = dyn_cast<LaserTarget, EntityAI> (context._weapons->_laserTarget);
      if (lTarget) lTarget->SetOwnerFov(GetCameraFOV());
    }
  }
  // when we do not call move, target is not activated
  // it stops shining and after some time is autodestroys
}

void EntityAIFull::FireLaser(WeaponsState &weapons, int weapon, TargetType *target)
{
  weapons._laserTargetOn = !weapons._laserTargetOn;
}

const float DeadAbility=0.2; // private

float EntityAI::GetAbility(AIBrain *unit, AbilityKind kind) const
{
  if (!unit) unit = CommanderUnit();
  if( !unit ) return 1/DeadAbility;
  return unit->GetInvAbility(kind);
}

float EntityAI::GetInvAbility(AIBrain *unit, AbilityKind kind) const
{
  if (!unit) unit = CommanderUnit();
  if( !unit ) return DeadAbility;
  return unit->GetAbility(kind);
}

/**
@return value from 1 (able) to 0.2 (unable)
*/
float EntityAI::GetAbility(AbilityKind kind) const
{
  AIBrain *brain=CommanderUnit();
  if( !brain ) return DeadAbility;
  return brain->GetAbility(kind);
}

/**
@return value from 1 (able) to 5 (unable)
*/
float EntityAI::GetInvAbility(AbilityKind kind) const
{
  AIBrain *brain=CommanderUnit();
  if( !brain ) return 1/DeadAbility;
  return brain->GetInvAbility(kind);
}

float EntityAIType::GetCostTurn(int difDir, int &debet, bool forceFullTurn) const
{
  debet = 0;
  return 1.0;
}

FieldCost EntityAIType::GetRoadCost(CombatMode mode) const
{
  return GetTypeCost(OITRoad, mode)*0.7f;
}

FieldCost EntityAIType::GetTypeCost(OperItemType type, CombatMode mode) const
{
  static const float costs[] =
  {
    1.0, // OITNormal,
    3.0, 3.0, 3.0, //OITAvoidBush, OITAvoidTree, OITAvoid
    SET_UNACCESSIBLE,  // OITWater,
    SET_UNACCESSIBLE, // OITSpaceRoad
    0.5, //  OITRoad
    SET_UNACCESSIBLE, // OITSpaceBush
    SET_UNACCESSIBLE, // OITSpaceHardBush
    SET_UNACCESSIBLE, // OITSpaceTree
    SET_UNACCESSIBLE, // OITSpace
    0.5 // OITRoadForced
  };
  Assert(sizeof(costs)/sizeof(*costs)==NOperItemType);
  return FieldCost(costs[type],costs[type]);
}


/*!
\patch 1.12 Date 08/07/2001 by Ondra
- New: Force feedback effect on fire / hit.
\patch 1.30 Date 11/02/2001 by Ondra
- Fixed: Speed of sound simulation caused distant fullauto sounds muted (since 1.27)

\patch 5143 Date 3/22/2007 by Ondra
- New: New config value soundBurst to allow playing sound for each bullet in a burst
*/

void EntityAIFull::FireWeaponEffects(const TurretContext &context, int weapon, const Magazine *magazine, EntityAI *target, const RemoteFireWeaponInfo *remoteInfo)
{
  if (weapon < 0 || weapon >= context._weapons->_magazineSlots.Size()) return;
  const MagazineSlot &slot = context._weapons->_magazineSlots[weapon];
  if (slot._magazine != magazine) return;

  if (remoteInfo)
  {
    // create a fake shot if necessary for remote units
    ProcessFireWeapon(context, weapon, magazine, target, remoteInfo, NULL, false);
  }
 
  // Shot occurred, add some heat    
  int weaponType = slot._weapon->_weaponType;
  _weaponsState.SimulateHeatFactors(weaponType, true);
  
  const MuzzleType *muzzle = slot._muzzle;
  const MagazineType *mType = magazine ? magazine->_type : NULL;
  const WeaponModeType *mode = slot._weaponMode;
  bool continuous = muzzle ? muzzle->_soundContinuous : false;
  bool noSound = false;
  /**/
  float soundDuration = muzzle ? muzzle->_soundDuration : 0;
  if (mType && mode)
  {
    soundDuration = mode->_soundDuration;
    continuous = mode->_soundContinuous;
    // sound only on first shot in burst
    if (magazine->_burstLeft>0 && mode->_soundBurst) noSound = true;
  }
  /**/

  // play sound
  if (!noSound)
  {
    context._weapons->_sound.Access(weapon);
    // select corresponding weapon sounds
    WeaponSounds &wSounds = context._weapons->_sound[weapon];
    // play shot sound
    wSounds.ShotSound(this, context, weapon, magazine);
  }

  const VisualState &vs = RenderVisualState();
  const AmmoType *type = mType ? mType->_ammo : NULL;
  if (type)
  {
    _shootVisible = type->visibleFire;
    _shootAudible = type->audibleFire;
    _shootTimeRest = type->visibleFireTime;
#if _VBS3
    _shootTimeRestAudible = type->audibleFireTime;
#endif
    _shootTarget = target;
    // perform cartridge effects
    #if 1
    if (type->_cartridgeType && EnableVisualEffects(SimulateVisibleNear,100))
    {
      Vector3 viewerPos=GScene->GetCamera()->Position();

      SimulationImportance importance = CalculateImportance(&viewerPos,1);
      if (importance<=SimulateVisibleNear)
      {
        // check current muzzle cartridge position / velocity
        // create a transformation from weapon direction
        Matrix4 pos;
        Vector3 vel;
        if (GetWeaponCartridgePos(context, weapon, pos, vel))
        {
          pos = vs.Transform() * pos;
          vel = vs.Transform().Rotate(vel);
          pos.Orthogonalize();

          Entity *fx = CreateThing(type->_cartridgeType,pos,vel,true);
          if (fx)
          {
            // any additional processing
          }
        }
      }
    }
    #endif
  }

  // check which recoil table should be used
  RStringB recoilName = GetRecoilName(mode);
  Ref<RecoilFunction> recoil = RecoilFunctions.New(recoilName);
  StartRecoil(recoil,GetRecoilFactor());

  if (IsEventHandler(EEFired) 
#if _VBS3 //MuzzleEvent
    || slot._muzzle->IsEventHandler(MEFired)
#endif
    )
  {
    GameValue value = GWorld->GetGameState()->CreateGameValue(GameArray);
    GameArrayType &arguments = value;
    const MagazineSlot &slot = context._weapons->_magazineSlots[weapon];
    arguments.Add(GameValueExt(this));
    arguments.Add(RString(slot._weapon->GetName()));
    arguments.Add(RString(slot._muzzle->GetName()));
    arguments.Add(mode ? RString(mode->GetName()) : RString());
    arguments.Add(type ? RString(type->GetName()) : RString());
    arguments.Add(slot._magazine ? RString(slot._magazine->_type->GetName()) : RString());
    // Add the object that was spawned

    // here we handle remote objects passed to us from
    // the NetworkClient
    Object *missle = context._weapons->_lastShot;
#if _VBS3
    if(!missle && remoteInfo)
      missle = remoteInfo->_projectile;
#endif

    // add missle to the end of the event
    arguments.Add(GameValueExt(missle));
    
    if(IsEventHandler(EEFired))
      OnEvent(EEFired,value);
#if _VBS3//MuzzleEvent
    if(slot._muzzle->IsEventHandler(MEFired))
      slot._muzzle->OnEvent(MEFired,value);
#endif
  }

  // process all nearby entities
  int xMin,xMax,zMin,zMax;
  ObjRadiusRectangle(xMin,xMax,zMin,zMax,vs.Position(),50); // TODO: 50 - make distance adjustable?
  for (int z=zMin; z<=zMax; z++) for (int x=xMin; x<=xMax; x++)
  {
    const ObjectList &list = GLandscape->GetObjects(z,x);
    for (int i=0,n=list.Size(); i<n; i++)
    {
      Object *obj = list[i];
      if (!obj) continue;
      EntityAI *ai = dyn_cast<EntityAI>(obj);
      if (!ai) continue;
      if (ai->IsEventHandler(EEFiredNear))
      {
        GameValue value = GWorld->GetGameState()->CreateGameValue(GameArray);
        GameArrayType &arguments = value;
        const MagazineSlot &slot = context._weapons->_magazineSlots[weapon];
        arguments.Add(GameValueExt(ai));
        arguments.Add(GameValueExt(this));
        arguments.Add(vs.Position().Distance(obj->RenderVisualState().Position()));
        arguments.Add(RString(slot._weapon->GetName()));
        arguments.Add(RString(slot._muzzle->GetName()));
        arguments.Add(RString(mode ? RString(mode->GetName()) : RString()));
        arguments.Add(RString(type ? RString(type->GetName()) : RString()));
        ai->OnEvent(EEFiredNear,value);
      }
    }
  }

  // camera shake effect
  GWorld->GetCameraShakeManager().AddSource(CameraShakeParams(CameraShakeSource::CSWeaponFire, vs.Position(), type));
}

/**
Preloaded content currently includes:
  sound
  cartridge model
  muzzle flash animated texture
  
More can be added:
  bullet model, particle textures, cartridge model.
  
Does not need preloading:
    cartridge type (stored in the weapon)
*/
bool EntityAIFull::PreloadFireWeaponEffects(const WeaponsState &weapons, int weapon)
{
  // if the unit is far, do not preload anything for it
  if (!EnableVisualEffects(SimulateVisibleNear,200)) return true;
  if (weapon < 0 || weapon >= weapons._magazineSlots.Size()) return true;
  const MagazineSlot &slot = weapons._magazineSlots[weapon];
  const Magazine *magazine = slot._magazine;
  if (!magazine) return true;

  //const MuzzleType *muzzle = slot._muzzle;
  //const SoundPars *sound = &muzzle->_sound;
  const MagazineType *mType = magazine ? magazine->_type : NULL;
  const WeaponModeType *mode = slot._weaponMode;

  bool ret = true;

  if (mode)
  {
    // preload shot samples
    if (!mode->_beginSound.PreloadSounds())
      ret = false;
  }

  if (!PreloadMuzzleFlash(weapons, weapon))
    ret = false;
  
  const AmmoType *type = mType ? mType->_ammo : NULL;
  if (type)
  {
    // perform cartridge effects
#if 1
    if (type->_cartridgeType && EnableVisualEffects(SimulateVisibleNear,100))
    {
      Vector3 viewerPos=GScene->GetCamera()->Position();

      SimulationImportance importance = CalculateImportance(&viewerPos,1);
      if (importance<=SimulateVisibleNear)
      {
        // we can preload model for type->_cartridgeType now
        if (!type->_cartridgeType->PreloadShape(FileRequestPriority(200)))
          ret = false;
      }
    }
#endif
  }
  return ret;
}

bool EntityAIFull::FireWeapon(const TurretContextV &context, int weapon, TargetType *target, bool forceLock)
{
  Fail("Entity cannot fire unless it defines FireWeapon");
  // by default entity cannot does not fire
  RemoteFireWeaponInfo localInfo;
  localInfo._direction = VForward;
  return PostFireWeapon(context, weapon, target, localInfo);
}

#if _VBS3
bool EntityAIFull::PostFireWeapon(const TurretContext &context, int weapon, TargetType *target, RemoteFireWeaponInfo &remoteInfo)
#else
bool EntityAIFull::PostFireWeapon(const TurretContext &context, int weapon, TargetType *target, const RemoteFireWeaponInfo &remoteInfo)
#endif
{
  if (weapon >= context._weapons->_magazineSlots.Size() || weapon < 0)
    return false; // nothing to fire
  
  context._weapons->_lastShotTime = Glob.time;

  const MagazineSlot &slot = context._weapons->_magazineSlots[weapon];
  Magazine *magazine = slot._magazine;

  FireWeaponEffects(context,weapon,magazine,target,NULL);

  if (context._turret && context._turretType && context._turretType->_primaryGunner)
  {
    static const float HeatUpFactor = 1.0f / 30.0f;
    context._turret->_heatFactor += HeatUpFactor;
    saturate(context._turret->_heatFactor, 0.0f, 1.0f);
  }

#if _VBS3
  Entity *proj = context._weapons->_lastShot;
  remoteInfo._projectile = proj;

  // Every shot other than shot bullet spawns a network object
  // for bullets we just assign null, because they're spawned localy
  // and mostly harmless
  ShotBullet *bullet = dyn_cast<ShotBullet>(proj);
  if(bullet)
    remoteInfo._projectile = NULL;   
  else
    remoteInfo._visible = FLT_MAX;  
#endif

  GetNetworkManager().FireWeapon(this, context._gunner, weapon, magazine, target, remoteInfo);

  if (!magazine)
    return false;

  const WeaponModeType *mode = slot._weaponMode;

  if (!mode)
    return false;

  // only last bullet in burst should be reloaded based on ability

  if (magazine->_burstLeft<=0)
  {
    magazine->_burstLeft = mode->_burst;
    saturateMin(magazine->_burstLeft, magazine->GetAmmo()/mode->_mult);
  }

  if (!mode->_autoFire && (magazine->_burstLeft==1 || mode->_burst == 0))
  {
    AIBrain *unit = EffectiveGunnerUnit(context);
    if (!unit) unit = CommanderUnit();
    float invAbility = unit ? unit->GetInvAbility(AKReloadSpeed) : 1/DeadAbility;
    magazine->_reloadDuration = invAbility * GRandGen.PlusMinus(1, 0.1);
  }
  else
    magazine->_reloadDuration = 1.0;
  magazine->_reload = 1.0;

  magazine->_burstLeft--; // count shots in burst

  int burst = mode->_mult;
  saturateMin(burst, magazine->GetAmmo());
#if _ENABLE_CHEATS
  static bool testAmmoCheat = false;
#else
  const bool testAmmoCheat = false;
#endif
  if (!testAmmoCheat)
  {
    ADD_ENCRYPTED(magazine->_ammo,magazine->_ammoSupport, -burst);
  }
  // TODO: ?? auto reload magazine here?
  if (context._turret)
  {
    if (!context._turret->IsLocal())
      GetNetworkManager().AskForAmmo(this, context._turret, weapon, burst);
  }
  else if (!IsLocal())
    GetNetworkManager().AskForAmmo(this, context._turret, weapon, burst);

  // remove magazine
  if (magazine->GetAmmo() <= 0)
  {
    magazine->_reload = 0; // for better weapon animation
    // following code was never hit during normal game-play (active weapon is always fired) and caused issue http://dev-heaven.net/issues/24046
//     if (weapon != context._weapons->_currentWeapon && weapon != context._weapons->_currentCounterMeasures )
//       context._weapons->RemoveMagazine(this, magazine);
    // try to start auto-reloading
    AutoReload(*context._weapons, weapon, _reloadEnabled, false);
  }
  return true;
}

RefArray<Object> MapDiags;

#if _RELEASE
  #define DIAG_VEHICLE 0
#else
  #define DIAG_VEHICLE 0
#endif

static const EnumName SimulationImportanceNames[]=
{
  EnumName(SimulateVisibleNear,"Near"),
  EnumName(SimulateVisibleFar,"Far"),
  EnumName(SimulateInvisibleNear,"InvisNear"),
  EnumName(SimulateInvisibleFar,"InvisFar"),
  EnumName(SimulateDefault,"Default"),
  EnumName()
};

template<>
const EnumName *GetEnumNames(SimulationImportance dummy) {return SimulationImportanceNames;}


RString EntityAI::DiagText() const
{
  AIBrain *unit = PilotUnit();
  BString<4096> buf;
  strcpy(buf,"");
  if (unit)
  {
    float pSpeed=0;
    const Path &path=unit->GetPath();
    if (path.Size()>=2)
    {
      float cost=path.CostAtPos(FutureVisualState().Position());
      AILockerSavedState state;
      PerformUnlock(&state);
      pSpeed=path.SpeedAtCost(cost,unit->IsFreeSoldier());
      PerformRelock(state);
    }
    sprintf(buf, "Spd %.1f (path %d: v:%.1f, vMax:%.1f: t:%.2f)",
      FutureVisualState().ModelSpeed().Z()*3.6,path.Size(),pSpeed*3.6,_limitSpeed*3.6,
      Glob.time-path.GetSearchTime());
  }
  if (GetStopped())
    strcat(buf," STOP");
  strcat(buf," ");
  strcat(buf,FindEnumName(_prec.GetEnumValue()));

  if (!IsLocal())
  {
    if (Glob.time-_lastUpdateTime>0.100)
    {
      BString<256> add;
      sprintf(add," Last update %.3f",Glob.time-_lastUpdateTime);
      strcat(buf,add);
    }
    if (CheckPredictionFrozen())
      strcat(buf," Frozen");
  }

  const Shape *hitShape=_shape->HitpointsLevel();
  if( hitShape )
  {
    //bool first=true;
    // report damages
#if _ENABLE_CHEATS
    if (CHECK_DIAG(DEDammage))
    {
      char hits[512];
      hits[0]=0;
      if (GetRawTotalDamage()>0)
        sprintf(hits+strlen(hits)," Tot:%.2f",GetRawTotalDamage());
      const HitPointList &hitpoints=GetType()->GetHitPoints();
      for( int i=0; i<hitpoints.Size(); i++ )
      {
        const HitPoint &hit = hitpoints[i];
        int index=hit.GetSelection();
        if( index<0 ) continue;
        const NamedSelection &sel=hitShape->NamedSel(index);
        float hitVal=_hit[i];
        if( hitVal>0.3 )
          sprintf(hits+strlen(hits)," %s:%.2f",sel.Name(),hitVal);
      }
      strcat(buf,hits);
    }
#endif
  }
  return (const char *)buf;
}

/*!
Check what axis is controlled by what peripheral.
Principal axes are:
  mouse cursor
  thrust

\patch_internal 1.11 Date 7/27/2001 by Ondra.
Different control device detection method
(decentralized, vehicle simulation is responsible for decision).
*/

inline PackedColor CostToColor(float cost)
{
  static const float int1 = 1.0;
  static const float invint1 = (1.0 / int1);
  static const float int2 = 4.0;
  static const float invint2 = (1.0 / int2);
  static const float int12 = int1 + int2;

  static const float alpha = 0.5;
  static const Color colorR(1, 0.5f, 0, alpha);
  static const Color colorG(0, 1, 0, alpha);
  static const Color colorY(1, 1, 0, alpha);

  saturateMin(cost, int12);
  if (cost <= int1)
    return PackedColor(colorY * (cost * invint1)  + colorG * (1 - cost * invint1));
  cost -= int1;
  return PackedColor(colorR * (cost * invint2) + colorY * (1 - cost * invint2));
}


Matrix4 EntityAI::AnimateProxyMatrix(ObjectVisualState const& vs, int level, Matrix4 proxyTransform, SkeletonIndex boneIndex) const
{
  AnimateBoneMatrix(proxyTransform,vs,level,boneIndex);
  return proxyTransform;
}

int EntityAI::GetProxyComplexity(int level, const Matrix4 &pos, float dist2) const
{
  // TODO: calculate flag
  // consider all proxies with no type or always show ones

  int nFaces = 0;

  // calculate default proxies (included in shape)
  ShapeUsed lock = _shape->Level(level);
  int n = _type->GetProxyCountForLevel(level);
  for( int i=0; i<n; i++ )
  {
    const ProxyObjectTyped &proxy=_type->GetProxyForLevel(level,i);
    
    const EntityType *type = proxy.obj->GetEntityType();
    if (type && strcmp(type->_simName, "alwaysshow")!= 0)
      // ignore typed proxies - they are handled in derived classes
      continue;

    // smart clipping par of obj->Draw
    Matrix4Val pTransform=pos*proxy.obj->RenderVisualState().Transform();

    // LOD detection
    LODShapeWithShadow *pshape = proxy.obj->GetShapeOnPos(pTransform.Position());
    if (!pshape)
      continue;
    int level = GScene->LevelFromDistance2(pshape,dist2,pTransform.Scale());
    if (level==LOD_INVISIBLE)
      continue;
    nFaces += proxy.obj->GetComplexity(level,pTransform);
  }
  return nFaces;
}

bool EntityAI::PreloadProxies(int level, float dist2, bool preloadTextures) const
{
  bool ret = true;
  // draw flag
  ShapeUsed lock = _shape->Level(level);
  for (int i=0; i<GetType()->GetProxyCountForLevel(level); i++)
  {
    const ProxyObjectTyped &proxy = GetType()->GetProxyForLevel(level,i);
    Object *obj = proxy.obj;
    const EntityType *type = obj->GetEntityType();
    LODShapeWithShadow *pshape = NULL;
    if (!type || strcmp(type->_simName, "alwaysshow") == 0)
      pshape = obj->GetShape();
    else if (_flag && _showFlag && !stricmp(type->_simName, "flag"))
      pshape = obj->GetShape();
    if (!pshape)
      continue;
    int pLevel = GScene->PreloadLevelFromDistance2(pshape, dist2, FutureVisualState().Scale()*obj->FutureVisualState().Scale());     
    if (pLevel == LOD_INVISIBLE)
      continue;
    if (pLevel<0)
    {
      ret = false;
      continue;
    }
    // preload textures
    ShapeUsed shape = pshape->Level(pLevel);
    if (!shape->PreloadTextures(dist2, NULL))
      ret = false;
  }
  return ret;
}

void EntityAI::AdjustFlagTransform(Matrix4 &pTransform)
{
  float forceVertical = ForceVerticalFlag();
  if (forceVertical>0)
  {
    if (forceVertical>=1)
      // we want to force vertical, b-center should be on the ground; interpolate between normal and vertical
      pTransform.SetOrientation(M3Identity);
    else
    {
      Matrix3 orient = M3Identity*forceVertical+pTransform.Orientation()*(1-forceVertical);
      orient.Orthogonalize();
      pTransform.SetOrientation(orient);
    }
  }
}

Matrix4 EntityAI::GetProxyTransform(int level, int i, Matrix4Par proxyPos, Matrix4Val parent) const
{
  SkeletonIndex boneIndex = _type->GetProxyBoneForLevel(level, i);
  Matrix4Val proxyTransform = AnimateProxyMatrix(RenderVisualState(), level, proxyPos, boneIndex);
  return parent * proxyTransform;
}

void EntityAI::PrepareProxiesForDrawing(Object *rootObj, int rootLevel, const AnimationContext &animContext,
  const PositionRender &transform, int level, ClipFlags clipFlags, SortObject *so)
{
  // prepare the flag proxy (when need to draw)
  if (_flag && _showFlag)
  {
    float dist2 = so->distance2;
    const EntityType *entType = Type();
    for (int i=0; i<entType->GetProxyCountForLevel(level); i++)
    {
      const ProxyObjectTyped &proxy = entType->GetProxyForLevel(level, i);
      Object *obj = proxy.obj;
      const EntityType *type = obj->GetEntityType();
      if (type && stricmp(type->_simName, "flag") == 0)
      {
        // flag proxy found
        LODShapeWithShadow *lodShape = obj->GetShape();

        SkeletonIndex boneIndex = entType->GetProxyBoneForLevel(level, i);
        Matrix4 proxyTransform = AnimateProxyMatrix(RenderVisualState(), level, proxy.GetTransform(), boneIndex);

        // TODO: smart clipping
        Matrix4 pTransform = transform * proxyTransform;

        AdjustFlagTransform(pTransform);
        int pLevel = GScene->LevelFromDistance2(lodShape, dist2, pTransform.Scale());
        if (pLevel != LOD_INVISIBLE)
        {
          Texture *tex = GetFlagTexture();
          if (tex)
          {
            _flag->SetFlagTexture(tex); // flag texture can be set here (_flag is the only instance for this proxy)
            _flag->SetTransform(pTransform); // store the matrix, it is needed from Flag::Animate
            // bounding box of the flag proxy does not cover the flag well, so avoid clipping at all
            GScene->ProxyForDrawing(_flag, rootObj, pLevel, rootLevel, 0, dist2, pTransform);
          }
        }
        // no more flag proxies supported
        break;
      }
    }
  }
  base::PrepareProxiesForDrawing(rootObj, rootLevel, animContext, transform, level, clipFlags, so);
}

Vector3 EntityAI::ExternalCameraPosition(CameraType camType) const
{
  return (camType == CamGroup) ? GetType()->_groupCameraPosition : GetType()->_extCameraPosition;
}

bool EntityAI::PreloadView(Person *person, CameraType camType) const
{
  bool ret = true;
  LODShapeWithShadow *shape = GetShape();
  int level = -1;
  // if loading a different shape, it makes no sense to preload entity proxies
  bool proxies = true;
  switch (camType)
  {
    case CamInternal:
      level = InsideLOD(camType);
      break;
    case CamExternal:
      if (shape->NLevels()>0) level = 0;
      break;
    case CamGunner:
      {
        LODShapeWithShadow *optics = GetOpticsModel(person);
        if (optics && optics->NLevels()>0)
        {
          shape = optics;
          level = 0;
          proxies = false;
        }
      }
      break;
  }
  if (level>=0 && shape && level<shape->NLevels())
  {
    if (!shape->CheckLevelLoaded(level,true))
      ret = false;
    else
    {
      ShapeUsed lock = shape->Level(level);
      if (!lock->PreloadTextures(0, NULL))
        ret = false;
      if (proxies && !PreloadProxies(level,0))
        ret = false;
    }
  }
  return ret;
}

bool EntityAIFull::ShowAim(const TurretContextV &context, int weapon, CameraType camType, Person *person) const
{
  if (camType==CamGunner)
    return false;
  bool autoAim = true;

  if (weapon>=0 && weapon<context._weapons->_magazineSlots.Size())
  {
    const MagazineSlot &slot = context._weapons->_magazineSlots[weapon];
    const MuzzleType *muzzle = slot._muzzle;
    autoAim = muzzle->_canAutoAim;

    if (camType == CamInternal && !muzzle->_showAimCursorInternal)
      return false;
  }
  AIBrain *unit = person->Brain();
  if (unit==GunnerUnit() && autoAim && // FIX: was test to EffectiveGunnerUnit()
    Glob.config.IsEnabled(DTAutoAim))
    return false; // no aiming point drawn
  if (CommanderUnit()!=unit && GunnerUnit()!=unit && EffectiveGunnerUnit(context)!=unit)
    return false;
  return base::ShowAim(context, weapon, camType, person);
}

float EntityAIFull::GetCursorSize(Person *person) const
{
  // check actual weapon
  TurretContext context;
  if (!FindTurret(person, context))
    return 1.0f;
  int weapon = context._weapons->_currentWeapon;
  if (weapon<0 || weapon>context._weapons->_magazineSlots.Size())
    // return no weapon cursor
    return 1.0f;
  const MagazineSlot &slot = context._weapons->_magazineSlots[weapon];
  const MuzzleType *muzzle = slot._muzzle;
  return muzzle->GetCursorSize();
}

const CursorTextureInfo *EntityAIFull::GetCursorTexture(Person *person)
{
  // check actual weapon
  TurretContext context;
  if (!FindTurret(person, context))
    return NULL;
  int weapon = context._weapons->_currentWeapon;
  if (weapon<0 || weapon>context._weapons->_magazineSlots.Size())
    // return no weapon cursor
    return NULL;
  const MagazineSlot &slot = context._weapons->_magazineSlots[weapon];
  const MuzzleType *muzzle = slot._muzzle;
  return muzzle->GetCursorTexture();
}

const CursorTextureInfo *EntityAIFull::GetCursorAimTexture(Person *person)
{
  // check actual weapon
  TurretContext context;
  if (!FindTurret(person, context))
    return NULL;
  int weapon = context._weapons->_currentWeapon;
  if (weapon<0 || weapon>context._weapons->_magazineSlots.Size())
    // return no weapon cursor
    return NULL;
  const MagazineSlot &slot = context._weapons->_magazineSlots[weapon];
  const MuzzleType *muzzle = slot._muzzle;
  return muzzle->GetCursorAimTexture(context._weapons->_laserTargetOn);
}

bool EntityAIFull::GetCursorLocked(Person *person) const
{
  Fail("Not used");
  return false;
/*
  if (person->Brain()!=EffectiveGunnerUnit()) return false;
  return _autoAimLocked.NotNull();
*/
}

void EntityAI::LimitVirtual(CameraType camType, float &heading, float &dive, float &fov) const
{
  //if( camType==CamInternal ) fov=GetType()->_fov;
  if (camType==CamInternal)
    GetType()->_viewPilot.LimitVirtual(camType,heading,dive,fov);
  else
    base::LimitVirtual(camType,heading,dive,fov);
}

void EntityAI::InitVirtual(CameraType camType, float &heading, float &dive, float &fov) const
{
  if( camType==CamInternal )
    GetType()->_viewPilot.InitVirtual(camType,heading,dive,fov);
  else
    base::InitVirtual(camType,heading,dive,fov);
}


class AmmoCostFunc : public ITurretFunc
{
protected:
  float &_curAmmo; // out

public:
  AmmoCostFunc(float &curAmmo) : _curAmmo(curAmmo) {}
  bool operator () (EntityAIFull *entity, TurretContext &context)
  {
    int n = context._weapons->_magazines.Size();
    for (int i=0; i<n; i++)
    {
      const Magazine *magazine = context._weapons->_magazines[i];
      if (!magazine)
        continue;
      if (magazine->GetAmmo() == 0)
        continue;
      const MagazineType *type = magazine->_type;
      if (!type)
        continue;
      const AmmoType *ammo = type->_ammo;
      if (!ammo)
        continue;
      _curAmmo += magazine->GetAmmo() * ammo->cost;
    }
    return false; // for all turrets
  }
};

float EntityAIFull::GetAmmoCost() const
{
  // sum-up ammo state
  float curAmmo = 0;
  AmmoCostFunc func(curAmmo);
  ForEachTurret(func);
  return curAmmo;
}

/// functor - sum all ammo values
class AmmoHitFunc : public ITurretFunc
{
protected:
  float &_curAmmo; // out

public:
  AmmoHitFunc(float &curAmmo) : _curAmmo(curAmmo) {}
  bool operator () (EntityAIFull *entity, TurretContext &context)
  {
    int n = context._weapons->_magazines.Size();
    for (int i=0; i<n; i++)
    {
      const Magazine *magazine = context._weapons->_magazines[i];
      if (!magazine)
        continue;
      if (magazine->GetAmmo() == 0)
        continue;
      const MagazineType *type = magazine->_type;
      if (!type)
        continue;
      const AmmoType *ammo = type->_ammo;
      if (!ammo)
        continue;
      _curAmmo += magazine->GetAmmo() * ammo->hit;
    }
    return false; // for all turrets
  }
};

float EntityAIFull::GetAmmoHit() const
{
  // sum-up ammo state
  float curAmmo = 0;
  AmmoHitFunc func(curAmmo);
  ForEachTurret(func);
  return curAmmo;
}

/// functor - sum all ammo values
class CheckAmmoHitFunc : public ITurretFunc
{
protected:
  float _curAmmo;
  float _limit;

public:
  CheckAmmoHitFunc(float limit) : _curAmmo(0),_limit(limit) {}
  bool operator () (EntityAIFull *entity, TurretContext &context)
  {
    int n = context._weapons->_magazines.Size();
    for (int i=0; i<n; i++)
    {
      const Magazine *magazine = context._weapons->_magazines[i];
      if (!magazine)
        continue;
      if (magazine->GetAmmo() == 0)
        continue;
      const MagazineType *type = magazine->_type;
      if (!type)
        continue;
      const AmmoType *ammo = type->_ammo;
      if (!ammo)
        continue;
      _curAmmo += magazine->GetAmmo() * ammo->hit;
      // once we are over, no need to continue
      if (_curAmmo>=_limit)
        return true;
    }
    return false; // for all turrets
  }
};

/// check if there are enough ammo resources
bool EntityAIFull::CheckAmmoHitOver(float hit) const
{
  // sum-up ammo state
  CheckAmmoHitFunc func(hit);
  return ForEachTurret(func);
}

/*!
\patch 5139 Date 3/13/2007 by Jirka
- Fixed: Vehicles did not rearm when no ammo was remaining
*/
class MaxAmmoCostFunc : public ITurretFunc
{
protected:
  float &_curAmmo; // out

public:
  MaxAmmoCostFunc(float &curAmmo) : _curAmmo(curAmmo) {}
  bool operator () (EntityAIFull *entity, TurretContext &context)
  {
    int n = context._weapons->_magazines.Size();
    for (int i=0; i<n; i++)
    {
      const Magazine *magazine = context._weapons->_magazines[i];
      if (!magazine)
        continue;
      const MagazineType *type = magazine->_type;
      if (!type)
        continue;
      if (type->_maxAmmo == 0)
        continue;
      const AmmoType *ammo = type->_ammo;
      if (!ammo)
        continue;
      _curAmmo += type->_maxAmmo * ammo->cost;
    }
    return false; // for all turrets
  }
};

float EntityAIFull::GetMaxAmmoCost() const
{
  // sum-up ammo state
  float curAmmo = 0;
  MaxAmmoCostFunc func(curAmmo);
  ForEachTurret(func);
  return curAmmo;
}

class MaxAmmoHitFunc : public ITurretFunc
{
protected:
  float &_curAmmo; // out

public:
  MaxAmmoHitFunc(float &curAmmo) : _curAmmo(curAmmo) {}
  bool operator () (EntityAIFull *entity, TurretContext &context)
  {
    int n = context._weapons->_magazines.Size();
    for (int i=0; i<n; i++)
    {
      const Magazine *magazine = context._weapons->_magazines[i];
      if (!magazine)
        continue;
      const MagazineType *type = magazine->_type;
      if (!type)
        continue;
      if (type->_maxAmmo == 0)
        continue;
      const AmmoType *ammo = type->_ammo;
      if (!ammo)
        continue;

      _curAmmo += type->_maxAmmo * ammo->hit;
    }
    return false; // for all turrets
  }
};

float EntityAIFull::GetMaxAmmoHit() const
{
  // sum-up ammo state
  float curAmmo = 0;
  MaxAmmoHitFunc func(curAmmo);
  ForEachTurret(func);
  return curAmmo;
}

class RearmFunc : public ITurretFunc
{
protected:
  float &_resources; // out

public:
  RearmFunc(float &resources) : _resources(resources) {}
  bool operator () (EntityAIFull *entity, TurretContext &context)
  {
    int n = context._weapons->_magazines.Size();
    for (int i=0; i<n; i++)
    {
      Magazine *magazine = context._weapons->_magazines[i];
      if (!magazine)
        continue;
      const MagazineType *type = magazine->_type;
      if (!type)
        continue;
      if (type->_maxAmmo == 0)
        continue;
      const AmmoType *ammo = type->_ammo;
      if (!ammo)
        continue;
      float cost = ammo->cost;
      int need = type->_maxAmmo - magazine->GetAmmo();
      if (need * cost > _resources)
        need = toIntFloor(_resources / cost);
      if (need == 0)
        continue;

      ADD_ENCRYPTED(magazine->_ammo,magazine->_ammoSupport, need);
      _resources -= need * cost;
      // ?? TODO: ?? change _reload
    }
    return false; // for all turrets
  }
};

float EntityAIFull::Rearm( float resources )
{
  resources += _rearmCredit;

  RearmFunc func(resources);
  ForEachTurret(func);

  _rearmCredit = resources; // accumulate what's rest
  return resources;
}


class NeedRearmFunc : public ITurretFunc
{
protected:
  float &_minWeapon; // out

public:
  NeedRearmFunc(float &minWeapon) : _minWeapon(minWeapon) {}
  bool operator () (EntityAIFull *entity, TurretContext &context)
  {
    int n = context._weapons->_magazines.Size();
    for (int i=0; i<n; i++)
    {
      const Magazine *magazine = context._weapons->_magazines[i];
      if (!magazine)
        continue;
      const MagazineType *type = magazine->_type;
      if (!type)
        continue;
      if (type->_maxAmmo == 0)
        continue;
      float ratio = (float)magazine->GetAmmo() / type->_maxAmmo;
      saturateMin(_minWeapon, ratio * 1.5);
    }
    return false; // for all turrets
  }
};

float EntityAIFull::NeedsRearm() const
{
  if (IsDamageDestroyed())
    return 0;

  // sum-up ammo state
  float minWeapon = 1.0;
  NeedRearmFunc func(minWeapon);
  ForEachTurret(func);
  saturate(minWeapon, 0, 1);

  float max = GetMaxAmmoHit();
  if (max <= 0)
    return 0;
  float act = GetAmmoHit();

  return 1.0 - floatMin(act / max, minWeapon);
}

float EntityAIFull::GetReloadNeeded() const
{
  float value = 0;
  TurretContext context;
  if (GetPrimaryGunnerTurret(context))
  {
    for (int i=0; i<context._weapons->_magazineSlots.Size();)
    {
      const MagazineSlot &slot = context._weapons->_magazineSlots[i];
      const MuzzleType *muzzle = slot._muzzle;
      const Magazine *magazine = slot._magazine;
      const MagazineType *type = magazine ? magazine->_type : NULL;
      int index = context._weapons->FindMagazineByType(muzzle, type);
      if (index >= 0)
      {
        const Magazine *newMagazine = context._weapons->_magazines[index];
        if (magazine)
        {
          if (newMagazine->_type == type && type->_maxAmmo > 0 && newMagazine->GetAmmo() > magazine->GetAmmo())
            saturateMax(value, 1 - magazine->GetAmmo() / (float)type->_maxAmmo);
        }
        else
          return 1.0f; // max. value can be returned immediately
      }
      i += muzzle->_modes.Size();
    }
  }
  return value;
}

void EntityAIFull::Reload()
{
  TurretContext context;
  if (GetPrimaryGunnerTurret(context))
  {
    for (int i=0; i<context._weapons->_magazineSlots.Size();)
    {
      const MagazineSlot &slot = context._weapons->_magazineSlots[i];
      const MuzzleType *muzzle = slot._muzzle;
      const Magazine *magazine = slot._magazine;
      const MagazineType *type = magazine ? magazine->_type : NULL;
      int index = context._weapons->FindMagazineByType(muzzle, type);
      if (index >= 0)
      {
        const Magazine *newMagazine = context._weapons->_magazines[index];
        if (magazine)
        {
          if (newMagazine->_type == type && type->_maxAmmo > 0 && newMagazine->GetAmmo() > magazine->GetAmmo())
            ReloadMagazine(*context._weapons, i, index);
        }
        else
          ReloadMagazine(*context._weapons, i, index);
      }
      i += muzzle->_modes.Size();
    }
  }
}

float EntityAI::NeedsAmbulance() const
{
  return CommanderUnit() ? CommanderUnit()->GetPerson()->NeedsAmbulance() : 0.0f;
}

float EntityAI::NeedsRepair() const
{
  return IsDamageDestroyed() ? 1.0f : GetTotalDamage();
}

float EntityAI::NeedsRefuel() const
{
  if (IsDamageDestroyed())
    return 0.0f;
  float max = GetType()->GetFuelCapacity();
  if (max <= 0.0f)
    return 0.0f;
  float act = GetFuel();
  return 1.0f - act / max;
}

bool EntityAI::IsAbleToStand() const
{
  return CommanderUnit() && CommanderUnit()->GetPerson()->IsAbleToStand();
}


// accuracy simulation

const EntityAIType *EntityAI::GetTypeAtLeast(float accuracy) const
{
  // start with certain information
  const EntityType *type = GetType();
  while (type->_parentType.NotNull() && type->_parentType->_accuracy>=accuracy)
    type = type->_parentType;
  const EntityAIType *vType = dynamic_cast<const EntityAIType *>(type);
  if (!vType)
    Fail("Non-ai type");
  return vType;
}

const EntityAIType *EntityAI::GetType(float accuracy) const
{
  // start with certain information
  const EntityType *type = GetType();
  while (type->_parentType.NotNull() && type->_accuracy>=accuracy)
    type = type->_parentType;
  Assert(dynamic_cast<const EntityAIType *>(type));
  const EntityAIType *vType = static_cast<const EntityAIType *>(type);
  return vType;
}

TargetSide EntityAI::GetTargetSideAsIfAlive() const
{
  AIBrain *unit = CommanderUnit();
  if (unit)
  {
    Person *person = unit->GetPerson();
    // for vehicles, redirect to the commander
    if (person && person != this)
      return person->GetTargetSideAsIfAlive();
    // for person, check special cases
    if (unit->GetCaptive())
      return TCivilian;
    if (person && person->IsRenegade())
      // he is crazy - shooting at friendlies - enemy to all sides
      return TEnemy;
  }
  return base::GetTargetSide();
}


TargetSide EntityAI::GetTargetSide() const
{
  return _isDead ? TCivilian : GetTargetSideAsIfAlive();
}

bool EntityAI::IsRenegade() const
{
#if _VBS3_DISABLE_RENEGADE
  return false;
#endif
  AIBrain *unit = CommanderUnit();
  return unit && unit->GetPerson()->GetExperience() < ExperienceRenegadeLimit;
}


/*
TargetSide EntityAI::GetTargetSide() const
{
  if ( !CommanderUnit() && GetTotalDamage()<1 && !_fake ) return TCivilian; // empty - report neutral
  return base::GetTargetSide();
}
*/

TargetSide EntityAI::GetTargetSide(float accuracy) const
{
#if _VBS3_DEADENEMYDISABLESVEHICLEFIX
  if (_isDead) return TCivilian;
#endif
  AIBrain *unit = CommanderUnit();
  if (accuracy < Target::GetSideAccuracyExact())
  {
    const EntityAIType *type = GetType(accuracy);
    TargetSide side = type->_typicalSide;
    if (side != TSideUnknown && unit && unit->GetCaptive())
      return TCivilian;
    return side;
  }
  return GetTargetSideAsIfAlive();
}

RString EntityAI::GetDebugName() const
{
#define NETWORK_ID_IN_DEBUG_NAMES 0
#if NETWORK_ID_IN_DEBUG_NAMES
  RString prefix = Format("(%d,%d): ", GetNetworkId().creator, GetNetworkId().id);
  if (_varName.GetLength()>0)
  {
    return prefix + _varName;
  }
  if (CommanderUnit())
    return prefix+CommanderUnit()->GetDebugName();
  char ptr[256];
  sprintf(ptr,"%x# ",this);
  return prefix+RString(ptr)+base::GetDebugName()+(IsLocal()?"":" REMOTE");
#else
  if (_varName.GetLength()>0)
  {
    return _varName;
  }
  if (CommanderUnit())
    return CommanderUnit()->GetDebugName();
  char ptr[256];
  sprintf(ptr,"%x# ",this);
  return RString(ptr)+base::GetDebugName()+(IsLocal()?"":" REMOTE");
#endif
}

void EntityAI::ResetStatus()
{
  if( GetType()->IsSupply() )
  {
    if (_supply) _supply->Destroy(this);
    _supply=new ResourceSupply(this);
  }

  _isDead = false;

  _whenDestroyed = TIME_MAX;
  _sensorColID=SensorColID(-1);
  for (int i=0; i<NEntityEvent; i++)
    _eventHandlers[i].Clear();

  _userActions.Clear();

  base::ResetStatus();
}

void EntityAIFull::ResetStatus()
{
  _visTracker.Clear();
  base::ResetStatus();
}

AIBrain *EntityAI::EffectiveGunnerUnit(const TurretContext &context) const
{
  return context._gunner ? context._gunner->Brain() : NULL;
}

static const EnumName FireStateNames[]=
{
  EnumName(EntityAI::FireInit,"INIT"),
  EnumName(EntityAI::FireAim,"AIM"),
  EnumName(EntityAI::FireAimed,"AIMED"),
  EnumName(EntityAI::FireDone,"DONE"),
  EnumName()
};

template<>
const EnumName *GetEnumNames( EntityAI::FireState dummy ) {return FireStateNames;}

static const EnumName TargetStateNames[]=
{
  EnumName(TargetDestroyed,"DESTROYED"),
  EnumName(TargetAlive,"ALIVE"),
  EnumName(TargetEnemyEmpty,"ENEMYEMPTY"),
  EnumName(TargetEnemy,"ENEMY"),
  EnumName(TargetEnemyCombat,"COMBAT"),
  EnumName()
};

template<>
const EnumName *GetEnumNames(TargetState dummy)
{
  return TargetStateNames;
}

LSError UserActionDescription::Serialize(ParamArchive &ar)
{
  CHECK(ar.Serialize("id", id, 1))
  CHECK(ar.Serialize("text", text, 1, ""))
  CHECK(ar.Serialize("script", script, 1, ""))
  void *old=ar.GetParams();
  ar.SetParams(&GGameState);
  CHECK(ar.Serialize("param", param, 1))
  CHECK(ar.Serialize("priority", priority, 1, 0))
  CHECK(ar.Serialize("showWindow", showWindow, 1, true))
  CHECK(ar.Serialize("hideOnUse", hideOnUse, 1, false))
  CHECK(ar.Serialize("shortcut", (int &)shortcut, 1, -1))
  CHECK(ar.Serialize("condition", condition, 1, RString()))
#if USE_PRECOMPILATION
  if (ar.IsLoading() && ar.GetPass() == ParamArchive::PassFirst)
    GGameState.CompileMultiple(condition, conditionExp, GWorld->GetMissionNamespace()); // mission namespace
#endif
  ar.SetParams(old);

  return LSOK;
}

bool EntityAI::MustBeSaved() const
{
  // save whenever the unit has the brain, otherwise invalid references will occur
  return CommanderUnit() || base::MustBeSaved();
}

/*!
\patch_internal 1.23 Date 09/11/2001 by Ondra
- New: Some more flags serialized.
\patch 5153 Date 4/20/2007 by Jirka
- Fixed: Functions getVariable, setVariable can be used for more object types now
*/

LSError EntityAI::Serialize(ParamArchive &ar)
{
  CHECK(base::Serialize(ar))
  if (ar.IsSaving())
  {
    RString type = GetType()->GetName();
    CHECK(ar.Serialize("type", type, 1))
  }
  if (!IS_UNIT_STATUS_BRANCH(ar.GetArVersion()))
  {
    bool moveOut = _moveOutState.GetEnumValue()>MOIn;
    DoAssert(_moveOutState!=MOMovingOut);
    CHECK(ar.Serialize("moveOut", moveOut, 1, false))
    if (ar.IsLoading())
      _moveOutState = moveOut ? MOMovedOut : MOIn;

    CHECK(::Serialize(ar,"stratGoToPos", _stratGoToPos, 1, VZero))
    CHECK(ar.Serialize("limitSpeed", _limitSpeed, 1, 0))
  }
  SerializeBitBool(ar, "isDead", _isDead, 1, false)
  CHECK(::Serialize(ar,"whenDestroyed", _whenDestroyed, 1, TIME_MAX))
  if (!IS_UNIT_STATUS_BRANCH(ar.GetArVersion()))
  {

  #if ENABLE_ALLOW_DAMAGE
    CHECK(ar.Serialize("allowDamage", _allowDamage, 1, true))
  #endif

    CHECK(ar.Serialize("sensorColID", _sensorColID, 1, SensorColID(-1)))

    SerializeBitBool(ar,"landContact",_landContact,1,false); // TODO: default is true
    SerializeBitBool(ar,"objectContact",_objectContact,1,false);
    SerializeBitBool(ar,"objectContact",_waterContact,1,false);
    SerializeBitBool(ar,"inFormation",_inFormation,1,true);
    
    CHECK(ar.Serialize("isStopped", _isStopped, 1, false))
    CHECK(ar.Serialize("upsideDown", _isUpsideDown, 1, false))

    CHECK(ar.Serialize("nextUserActionId", _nextUserActionId, 1, 0))
    CHECK(ar.Serialize("UserActions", _userActions, 1))
    for (int i=0; i<NEntityEvent; i++)
    {
      const char *eventName = FindEnumName((EntityEvent)i);
      BString<80> eHandlerName;
      sprintf(eHandlerName,"EventHandlers%s",eventName);
      CHECK(ar.SerializeArray(eHandlerName.cstr(), _eventHandlers[i], 1))
    }
    CHECK(ar.SerializeRef("assembleTo", _assembleTo, 1))
    CHECK(ar.SerializeRef("assignedAttendant", _assignedAttendant, 1))

    // variables
    {
      void *old = ar.GetParams();
      ar.SetParams(&GGameState);
      DoAssert(_vars.IsSerializationEnabled());
      LSError result = ar.Serialize("Variables", _vars._vars, 1);
      ar.SetParams(old);
      // class Variables can be missing in older saves
      if (result != LSOK && result != LSNoEntry) return result;
    }
    
    
    if (ar.IsLoading())
    {
      if (ar.GetPass() == ParamArchive::PassFirst)
      {
        AutoArray<RStringB> textures;
        CHECK(ar.SerializeArray("setObjectTextures",textures,0))
        for (int i=0; i<_hiddenSelectionsTextures.Size(); i++)
        {
          if (i>=textures.Size()) break;
          Ref<Texture> texture = !textures[i].IsEmpty() ? GlobLoadTexture(textures[i]) : NULL;
          SetObjectTexture(i, texture);
        }
      }
    }
    else
    {
      AutoArray<RStringB> textures;
      textures.Resize(_hiddenSelectionsTextures.Size());
      for (int i=0; i<_hiddenSelectionsTextures.Size(); i++)
      {
        textures[i] = _hiddenSelectionsTextures[i].NotNull() ? _hiddenSelectionsTextures[i]->GetName() : RStringB();
      }
      CHECK(ar.SerializeArray("setObjectTextures",textures,0))
    
    }
    
    
  }

  if (ar.IsLoading() && ar.GetPass() == ParamArchive::PassFirst)
  {
    // destroy the current supply
    if (_supply)
    {
      _supply->Destroy(this);
      _supply = NULL;
    }
  }
  void *oldParams = ar.GetParams();
  ar.SetParams(this);
  CHECK(ar.Serialize("Supply", _supply, 1))
  ar.SetParams(oldParams);
  if (_supply)
  {
    SupplyUnitList &list = GetSupplyUnits();
    CHECK(ar.SerializeRefs("SupplyUnits", list, 1))
  }

  CHECK(ar.Serialize("userStopped", _userStopped, 1, false))

  if (ar.IsLoading())
  {
    _snappedPosition = VZero;
    UpdateSnappedPosition();
  }
  
  // TODO: check and remove _condition
  return LSOK;

/* TODO: need serialization ?
  
  Vector3 _lockedBeg; // where is the lock?

  // locking
  bool _locked :1,_tempLocked:1; // is locked?
  bool _isUpsideDown:1;
  bool _lockedSoldier; // type of lock
  Time _lastMovement; // when last movement prevented stopping
  float _shootVisible; // how much are we visible (1.0 default)
  float _shootTimeRest; // how long will current visibility last (before returning to 1.0)
  OLink(Object) _shootTarget; // what we fired at
  float _lockedRadius; // how large is the lock?
  //Ref<AILocker> _locker; // who locked - no real AI ...
  Ptr<AILocker> _locker; // who locked - no real AI ...
  SensorColID _sensorColID;
  AutoArray<MuzzleState> _muzzleState;
  OLink(FlashGunFire) _gunFlash;
  mutable VisibilityTrackerCache _visTracker;
  float _rearmCredit; // used during rearm
  // GoTo/FireAt
  float _avoidAside,_avoidAsideWanted; // obstacle avoidance offset
  Time _lastSimplePath; // formation pilot helper
  LinkTarget _fireTarget;

  // current attack test status
  // set during engage
  mutable LinkTarget _attackTarget; // which target
  mutable Time _attackEngageTime; // how is this information old
  mutable Time _attackRefreshTime; // last refresh of _attackXXXResult
  mutable Vector3 _attackAggressivePos;
  mutable Vector3 _attackEconomicalPos;
  mutable FireResult _attackAggresiveResult;
  mutable FireResult _attackEconomicalResult;
  FFEffects _ff; // result of ff simulation - only in SimCamera mode

  // support for getting in/out
  float _nearestEnemy; // nearest known enemy
*/
}

DEFINE_NETWORK_OBJECT_SIMPLE(UpdateEntityAIWeaponsMessage, UpdateEntityAIWeapons)

#define ENTITY_AI_MSG_LIST(XX) \
  XX(UpdateGeneric, UpdateVehicleAI) \
  XX(UpdateDamage, UpdateDamageVehicleAI)

DEFINE_NETWORK_OBJECT(EntityAI, base, ENTITY_AI_MSG_LIST)

#define ENTITY_AI_FULL_MSG_LIST(XX) \
  XX(UpdateGeneric, UpdateVehicleAIFull)

DEFINE_NETWORK_OBJECT(EntityAIFull, base, ENTITY_AI_FULL_MSG_LIST)

DEFINE_NET_INDICES_ERR(UpdateEntityAIWeapons, UPDATE_ENTITY_AI_WEAPONS_MSG)
DEFINE_NET_INDICES_EX_ERR(UpdateVehicleAI, UpdateVehicle, UPDATE_VEHICLE_AI_MSG)
DEFINE_NET_INDICES_EX_ERR(UpdateDamageVehicleAI, UpdateDamageVehicle, UPDATE_DAMMAGE_VEHICLE_AI_MSG)
DEFINE_NET_INDICES_EX_ERR(UpdateVehicleAIFull, UpdateVehicleAI, UPDATE_VEHICLE_AI_FULL_MSG)

NetworkMessageFormat &UpdateEntityAIWeaponsMessage::CreateFormat
(
  NetworkMessageClass cls,
  NetworkMessageFormat &format
)
{
  UPDATE_ENTITY_AI_WEAPONS_MSG(UpdateEntityAIWeapons, MSG_FORMAT_ERR)
  return format;
}

TMError UpdateEntityAIWeaponsMessage::TransferMsg(NetworkMessageContext &ctx)
{
  return _weapons.TransferMsg(ctx, _vehicle, _gunner);
}

/*!
\patch 1.24 Date 09/26/2001 by Ondra
- Fixed: vehicle light state was not transferred across network.
*/
NetworkMessageFormat &EntityAI::CreateFormat(NetworkMessageClass cls, NetworkMessageFormat &format)
{
  switch (cls)
  {
  case NMCUpdateDamage:
    base::CreateFormat(cls, format);
    UPDATE_DAMMAGE_VEHICLE_AI_MSG(UpdateDamageVehicleAI, MSG_FORMAT_ERR)
    break;
  case NMCUpdateGeneric:
    base::CreateFormat(cls, format);
    UPDATE_VEHICLE_AI_MSG(UpdateVehicleAI, MSG_FORMAT_ERR)
    break;
  default:
    base::CreateFormat(cls, format);
    break;
  }
  return format;
}

NetworkMessageFormat &EntityAIFull::CreateFormat(NetworkMessageClass cls, NetworkMessageFormat &format)
{
  switch (cls)
  {
  case NMCUpdateDamage:
    base::CreateFormat(cls, format);
    break;
  case NMCUpdateGeneric:
    base::CreateFormat(cls, format);
    UPDATE_VEHICLE_AI_FULL_MSG(UpdateVehicleAIFull, MSG_FORMAT_ERR)
    break;
  default:
    base::CreateFormat(cls, format);
    break;
  }
  return format;
}

TMError EntityAI::TransferMsg(NetworkMessageContext &ctx)
{
  switch (ctx.GetClass())
  {
  case NMCUpdateDamage:
    TMCHECK(base::TransferMsg(ctx))
    {
      PREPARE_TRANSFER(UpdateDamageVehicleAI)
      TRANSF(isDead)
    }
    break;
  case NMCUpdateGeneric:
    TMCHECK(base::TransferMsg(ctx))
    {
      PREPARE_TRANSFER(UpdateVehicleAI)

      TRANSF(pilotLight)
      TRANSF_CONTENT(supply)
      if(ctx.IsSending())
      {
        TRANSF_REF(assembleTo)
      }
      else
      {
        Ref<EntityAI> assemble;
        TRANSF_REF_EX(assembleTo,assemble)
        if(assemble != _assembleTo)
        {
          _assembleTo = assemble;
          if(_assembleTo)
          {
            _assembleTo->UnlockPosition();
            if (_assembleTo->IsInLandscape())
            {
              _assembleTo->SetMoveOut(this); // remove reference from the world
            }
          }
        }
      }
#if _VBS3
      TRANSF_ENUM(lightMode)
#endif
    }
    break;
  default:
    return base::TransferMsg(ctx);
  }
  return TMOK;
}

TMError EntityAIFull::TransferMsg(NetworkMessageContext &ctx)
{
  switch (ctx.GetClass())
  {
  case NMCUpdateDamage:
    TMCHECK(base::TransferMsg(ctx))
    break;
  case NMCUpdateGeneric:
    TMCHECK(base::TransferMsg(ctx))
    {
      PREPARE_TRANSFER(UpdateVehicleAIFull)

      if (ctx.IsSending())
      {
        OLink(EntityAI) target;
        if (_weaponsState._fire._fireTarget) target = _weaponsState._fire._fireTarget->idExact;
        TRANSF_REF_EX(fireTarget, target)
      }
      else
      {
        _weaponsState._fire._fireTarget = NULL;
        OLink(EntityAI) target;
        TRANSF_REF_EX(fireTarget, target)
        if (target)
        {
          AIBrain *unit = CommanderUnit();
          if (unit)
          {
            Target *newTarget = unit->FindTargetAll(target);
            if (!newTarget)
            {
              // add target to target database
              newTarget = unit->AddTarget(target, 4.0f, 4.0f, 0);
            }
            if (newTarget)
            {
              if (newTarget != _weaponsState._fire._fireTarget && GunnerUnit() == GWorld->FocusOn() && GWorld->UI())
                GWorld->UI()->ShowTarget();
              _weaponsState._fire._fireTarget = newTarget;
              if (!unit->IsLocal())
              {
                // update position if commander is remote
                _weaponsState._fire._fireTarget->UpdateRemotePosition(target);
              }
            }
          }
        }
      } 
      UpdateEntityAIWeaponsMessage weapons(this, PilotUnit() ? PilotUnit()->GetPerson() : NULL, _weaponsState);
      TRANSF_OBJECT_EX(weapons, weapons)
    }
    break;
  default:
    return base::TransferMsg(ctx);
  }
  return TMOK;
}

float EntityAI::CalculateError(NetworkMessageContextWithError &ctx)
{
  float error = 0;
  switch (ctx.GetClass())
  {
  case NMCUpdateDamage:
    {
      error += base::CalculateError(ctx);
      {
        PREPARE_TRANSFER(UpdateDamageVehicleAI)
        ICALCERR_NEQ(bool, isDead, ERR_COEF_STRUCTURE)
      }
    }
    break;
  case NMCUpdateGeneric:
    {
      error += base::CalculateError(ctx);
      {
        PREPARE_TRANSFER(UpdateVehicleAI)

        ICALCERR_NEQ(bool, pilotLight, ERR_COEF_VALUE_MAJOR)
#if _VBS3
        ICALCERR_NEQ(int, lightMode, ERR_COEF_VALUE_MAJOR)
#endif
        if (_supply)
        {
          NetworkMessageContextWithError subctx(message->_supply, ctx);
          error += _supply->CalculateError(subctx);
        }

        ICALCERR_NEQREF_SOFT(EntityAI, assembleTo, ERR_COEF_MODE)
      }
    }
    break;
  default:
    error += base::CalculateError(ctx);
    break;
  }
  return error;
}

float EntityAIFull::CalculateError(NetworkMessageContextWithError &ctx)
{
  float error = 0;
  switch (ctx.GetClass())
  {
  case NMCUpdateDamage:
    {
      error += base::CalculateError(ctx);
    }
    break;
  case NMCUpdateGeneric:
    {
      error += base::CalculateError(ctx);
      {
        PREPARE_TRANSFER(UpdateVehicleAIFull)

        OLink(EntityAI) target;
        if (_weaponsState._fire._fireTarget) target = _weaponsState._fire._fireTarget->idExact;
        ICALCERRE_NEQREF_SOFT(Object, fireTarget, target, ERR_COEF_MODE)

        NetworkMessageContextWithError subctx(message->_weapons, ctx);
        error += _weaponsState.CalculateError(subctx);
      }
    }
    break;
  default:
    error += base::CalculateError(ctx);
    break;
  }
  return error;
}


DEFINE_CASTING(EntityAIFull)

EntityAIFull::EntityAIFull(const EntityAIType *type, bool fullCreate)
:base(type, fullCreate),  // bubble allocateVisualState to EntityAI

_shootVisible(0),_shootAudible(0),_shootTimeRest(1e10),
#if _VBS3
  _shootTimeRestAudible(1e10),
#endif
_lastWeaponNotReady(Glob.time),_lastWeaponReady(Glob.time-10),

_fireState(FireDone),
_fireStateDelay(Glob.time-60),

// when we scanned for visible targets
_newTargetsTime(TIME_MIN),
_nearestEnemy(1e10), // nearest known enemy

_cameraInterest(0),

_rearmCredit(0), // used during rearm
//_autoAimLocked(false),

// attack position search
_attackEngageTime(Glob.time-60),
_attackRefreshTime(Glob.time-60),

_attackAggressivePos(VZero),
_attackEconomicalPos(VZero),

_reloadEnabled(true),
_useSuppressiveFire(SuppressNo),
_suppressUntil(TIME_MIN),
 
_nextSampleIndex(0),
_engineOffTimeOutInProgress(false),
_heatSourceDirection(VZero),
_nextPositionToHistory(Glob.time + GRandGen.RandomValue() * PositionsHistoryInterval)

{
  for (int i=0; i<NTgtCategory; i++)
    _trackTargetsTime[i] = TIME_MIN;
  for (int i=0; i<PositionsHistorySamples; i++)
    _positionsHistory[i] = VZero;

  // for !fullCreate, do not create magazines and weapons, but still initialize the parameters of effects
  _weaponsState.Init(type->_weapons, *type->_par, this, fullCreate);
  // prepare everything for the next shot
  AfterFire();
}
  // destructor
EntityAIFull::~EntityAIFull()
{
}

void EntityAIFull::Init(Matrix4Par pos, bool init)
{
  base::Init(pos, init);
  InitPositionHistory(pos);
}

void EntityAIFull::InitPositionHistory(Matrix4Par pos)
{
  float factorZ = GetFormationZ();
  AIUnit *commander = CommanderUnit() ? CommanderUnit()->GetUnit() : NULL;
  Vector3 posDirection = pos.Direction();
  if (commander)
  {
    AISubgroup *subgrp = commander->GetSubgroup();
    if (subgrp)
    {
      posDirection = subgrp->GetFormationDirection();
      AIUnit *unitFollowed = subgrp->GetFormationNext(commander);
      if (unitFollowed)
      {
        EntityAI *followed = unitFollowed->GetVehicle();
        // each of vehicles has half influence to formation distance
        factorZ = (factorZ + followed->GetFormationZ()) * 0.5f;
      }
    }
  }

  Vector3 position = pos.Position();
  float minDistance = (1.0f / PositionsHistorySamples) * factorZ;
  Vector3 dir = minDistance * posDirection;
  for (int i=PositionsHistorySamples-1; i>=0; i--)
  {
    _positionsHistory[i] = position;
    position -= dir;
  }

  _nextSampleIndex = 0;
}

/// find the weapons system shoot last
class FindLastShotFunc : public ITurretFunc
{
private:
  Time &_lastShotTime;
  WeaponsState *&_lastShotWeapons;

public:
  FindLastShotFunc(Time &lastShotTime, WeaponsState *&lastShotWeapons)
    : _lastShotTime(lastShotTime), _lastShotWeapons(lastShotWeapons) {}

  bool operator () (EntityAIFull *entity, TurretContext &context)
  {
    if (context._weapons->_lastShotTime > _lastShotTime)
    {
      _lastShotTime = context._weapons->_lastShotTime;
      _lastShotWeapons = context._weapons;
    }
    return false; // continue
  }
};

Time EntityAIFull::GetLastShotTime() const
{
  Time lastShotTime = TIME_MIN;
  WeaponsState *lastShotWeapons = NULL;
  FindLastShotFunc func(lastShotTime, lastShotWeapons);
  ForEachTurret(func);
  return lastShotTime;
}

Entity *EntityAIFull::GetLastShot() const
{
  Time lastShotTime = TIME_MIN;
  WeaponsState *lastShotWeapons = NULL;
  FindLastShotFunc func(lastShotTime, lastShotWeapons);
  ForEachTurret(func);
  return lastShotWeapons ? lastShotWeapons->_lastShot : NULL;
}

Vector3Val EntityAIFull::GetHistoryPosition(float skipEnd) const
{
  // scan cyclical buffer
  int sample = _nextSampleIndex;
  --sample;if (sample<0) sample = PositionsHistorySamples-1;

  Vector3 pos = _positionsHistory[sample];

  // advance to the next sample
  --sample;if (sample<0) sample = PositionsHistorySamples-1;
  
  // we skipped one, we know we need the last - no need to test it
  for (int i=0; i<PositionsHistorySamples-2; i++)
  {
    Vector3Val act = _positionsHistory[sample];
    float dist = act.Distance(pos);
    pos = act;
    skipEnd -= dist;
    if (skipEnd<0)
    {
      return act;
    }
    // advance to the next sample
    --sample;if (sample<0) sample = PositionsHistorySamples-1;
  }
  return _positionsHistory[sample];
}

#if _PROFILE // _ENABLE_CHEATS
#pragma optimize("",off)
#endif

static inline Vector3 Nlerp(Vector3Par v0, Vector3Par v1, float t)
{
  return (v0*(1-t)+v1*t).Normalized();
}

static Vector3 Slerp(Vector3Par v0, Vector3Par v1, float t)
{ // see http://number-none.com/product/Understanding%20Slerp,%20Then%20Not%20Using%20It/
  float dot = v0*v1;
  const float tolerance = 0.0005f;
  if ((1-dot)<tolerance && t<2)
  {
    return Nlerp(v0,v1,t);
  }
  else
  {
    float theta_0 = acos(dot);
    float theta = theta_0*t;

    Vector3 v2 = (v1 - v0*dot).Normalized();
    return v0*cos(theta)+v2*sin(theta);
  }
}

static Vector3 ExtrapolateOnCurve(Vector3Par past1, Vector3Par past2, Vector3Par past3, Vector3Par currPos, float distanceFromNow)
{
  #if _PROFILE
    // simple linear extrapolation
    Vector3 nearest = NearestPointInfinite(past1,past2,currPos);
    Vector3 simple = nearest+(past1-past2).Normalized()*distanceFromNow;
  #endif
  // compute orientation difference between two successive points
  
  // orientation at point past1
  Vector3Val dir1 = (past1-past2).Normalized();
  // orientation at point past2
  Vector3Val dir2 = (past2-past3).Normalized();

  Vector3 nearest12 = NearestPointInfinite(past1,past2,currPos);
  float distC1 = nearest12.Distance(past1);
  float distC2 = nearest12.Distance(past2);
  float dist12 = past1.Distance(past2);
  
  // when distFrom2
  Vector3 dir = Slerp(dir2,dir1,(distC2+distanceFromNow)/dist12);
  Vector3 ret = dir*(distC1+distanceFromNow)+past1;
  return ret;
  
}
Vector3 EntityAIFull::ExtrapolateHistoryPosition(float distanceFromNow, Vector3 *direction) const
{
  // simple prototype
  Vector3 horizontalDirection = FutureVisualState().Direction();
  horizontalDirection[1] = 0;
  horizontalDirection.Normalize();
  
  if (direction)
    *direction = horizontalDirection;

  #if _PROFILE
    Vector3 simpleRet = FutureVisualState().Position()+horizontalDirection*distanceFromNow;
  #endif

  // take last three samples
  Vector3Val past1 = _positionsHistory[(_nextSampleIndex+PositionsHistorySamples-1)%PositionsHistorySamples];
  Vector3Val past2 = _positionsHistory[(_nextSampleIndex+PositionsHistorySamples-2)%PositionsHistorySamples];
  Vector3Val past3 = _positionsHistory[(_nextSampleIndex+PositionsHistorySamples-3)%PositionsHistorySamples];
  Vector3Val curr1 = FutureVisualState().Position();
  Vector3Val curr2 = past1;
  Vector3Val curr3 = past2;
  // avoid singularity - past1 very close to current position 
  float distCurr23 = curr2.Distance(curr3);
  float distCurr12 = curr1.Distance(curr2);
  if (distCurr23>Square(0.01f)) // solution is possible only when past is not singular
  {
    Vector3Val pastExtrapolate = ExtrapolateOnCurve(past1,past2,past3,FutureVisualState().Position(),distanceFromNow);
    if (distCurr12>Square(0.01f))
    {
      Vector3 currExtrapolate = ExtrapolateOnCurve(curr1,curr2,curr3,FutureVisualState().Position(),distanceFromNow);
      float factor = floatMin(distCurr12/distCurr23,1.0f);
      return pastExtrapolate*(1-factor)+currExtrapolate*factor;
      
    }
    return pastExtrapolate;  
  }
  
  // take last two samples + current position
  // depending on how far current position is, interpolate between the two solutions
  
  return FutureVisualState().Position()+horizontalDirection*distanceFromNow;
}


Vector3 EntityAIFull::NearestHistoryPosition(Vector3Par pos, Vector3 *direction) const
{ // similar to Path::FindNext
  // start with current position as a "most recent history"
  Vector3 curr = FutureVisualState().Position();
  int sample = _nextSampleIndex;
  if (--sample<=0) sample = PositionsHistorySamples-1; // advance to the previous sample
  
  float nearestDist2 = FLT_MAX;
  Vector3 nearestPos;
  // we skipped one, we know we need the last - no need to test it
  for (int i=0; i<PositionsHistorySamples-2; i++)
  {
    Vector3 prev = curr;
    curr = _positionsHistory[sample];
    Vector3 nearest = NearestPoint(curr,prev,pos);
    float dist2 = nearest.Distance2(pos);
		if (nearestDist2 >= dist2)
		  nearestDist2 = dist2, nearestPos = nearest;
    if (--sample<=0) sample = PositionsHistorySamples-1; // advance to the previous sample
  }
  return nearestPos;
}

#if _PROFILE // _ENABLE_CHEATS
#pragma optimize("",on)
#endif

bool EntityAIFull::SimulationReady(SimulationImportance prec) const
{
  int xMin,xMax,zMin,zMax;
  float radius = GetRadius()*2+5;
  ObjRadiusRectangle(xMin,xMax,zMin,zMax,FutureVisualState().Position(),FutureVisualState().Position(),radius);
  // all data near any entity need to be ready so that it can be simulated
  // we need to load enough so that RoadSurfaceY is ready
  xMin--,xMax++;
  zMin--,zMax++;
  bool ret = ObjRectangleReady(xMin, xMax, zMin, zMax);
  return ret;
}

void EntityAIFull::Simulate(float deltaT, SimulationImportance prec)
{
#if _EXT_CTRL  //ext. Controller like HLA, AAR: apply the current speed
  if(IsExternalControlled() && !GAAR.IsPlaying())
  {
//    Matrix4 movePos;
//    ApplySpeed(movePos,deltaT);
    Vector3 position=Position();
    position+=FutureVisualState()._speed*deltaT;
    
    Frame moveTrans = *this;
//    moveTrans.SetTransform(movePos);
    moveTrans.SetPosition(position);
    Move(moveTrans); // finally apply move
  }
#endif

  base::Simulate(deltaT,prec);

  // positions history
  if (Glob.time > _nextPositionToHistory)
  {
    Vector3Val pos = FutureVisualState().Position();

    int lastIndex = _nextSampleIndex - 1;
    if (lastIndex < 0) lastIndex = PositionsHistorySamples - 1;
    Vector3Val lastPos = _positionsHistory[lastIndex];

    float factorZ = GetFormationZ();
    AIUnit *commander = CommanderUnit() ? CommanderUnit()->GetUnit() : NULL;
    if (commander)
    {
      AISubgroup *subgrp = commander->GetSubgroup();
      if (subgrp)
      {
        AIUnit *unitFollowed = subgrp->GetFormationNext(commander);
        if (unitFollowed)
        {
          EntityAI *followed = unitFollowed->GetVehicle();
          // each of vehicles has half influence to formation distance
          factorZ = (factorZ + followed->GetFormationZ()) * 0.5f;
        }
      }
    }
    float minDistance = (1.0f / PositionsHistorySamples) * factorZ;

    if (lastPos.Distance2(pos) > Square(minDistance))
    {
      // moved, store the sample
      _positionsHistory[_nextSampleIndex] = pos;
      _nextSampleIndex++;
      if (_nextSampleIndex >= PositionsHistorySamples) _nextSampleIndex = 0;
      _lastMoved = Glob.time;
    }

    _nextPositionToHistory = Glob.time + PositionsHistoryInterval;
  }
  // Simulate fire heat cooling
  {
    _weaponsState.SimulateHeatFactors(-1, false, deltaT);
  }

  // Simulate the heat source transition
  {
    // Get the heat source direction from the sun
    // See HEATSOURCEDIRECTION
    LightSun *sun = GScene->MainLight();
    Vector3 worldHeatDirection = sun->Direction();

    // Transform the heat source direction into model coordinates and normalize it (since orientation may contain a scale)
    Vector3 objectHeatDirection = FutureVisualState().Orientation().InverseRotation() * sun->Direction();
    objectHeatDirection.Normalize();

    // Interpolate between the current local heat source and actual value
    float invFullTransitionTime = 1.0f / 60.0f;
    _heatSourceDirection = _heatSourceDirection + (objectHeatDirection - _heatSourceDirection) * invFullTransitionTime * deltaT;
  }
}

LSError EntityAIFull::Serialize(ParamArchive &ar)
{
  CHECK(base::Serialize(ar))
  if (!IS_UNIT_STATUS_BRANCH(ar.GetArVersion()))
  {
    CHECK(ar.SerializeRef("attackTarget", _attackTarget, 1))
    CHECK(::Serialize(ar, "attackAggresivePos", _attackAggressivePos, 1, VZero))
    CHECK(::Serialize(ar, "attackEconomicalPos", _attackEconomicalPos, 1, VZero))

    CHECK(ar.SerializeEnum("fireState", _fireState, 1, FireDone))
    CHECK(::Serialize(ar, "fireStateDelay", _fireStateDelay, 1))

    CHECK(::Serialize(ar, "lastShotAtAssignedTarget", _lastShotAtAssignedTarget, 1, Time(0)))
    //mutable Time _attackEngageTime; // how is this information old
    //mutable Time _attackRefreshTime; // last refresh of _attackXXXResult
    //mutable FireResult _attackAggresiveResult;
    //mutable FireResult _attackEconomicalResult;

    CHECK(::Serialize(ar, "newTargetsTime", _newTargetsTime, 1, Glob.time))
    CHECK(::Serialize(ar, "trackTargetsTimeEn", _trackTargetsTime[TgtCatEnemy], 1, Glob.time))
    CHECK(::Serialize(ar, "trackTargetsTimeFr", _trackTargetsTime[TgtCatFriendly], 1, Glob.time))
    CHECK(ar.Serialize("cameraInterest", _cameraInterest, 1, 0))

    // positions history
    for (int i=0; i<PositionsHistorySamples; i++)
    {
      RString name = Format("positionsHistory%d", i);
      CHECK(::Serialize(ar, name, _positionsHistory[i], 1, VZero))
    }
    CHECK(::Serialize(ar, "nextPositionToHistory", _nextPositionToHistory, 1, Time(0)))
    CHECK(::Serialize(ar, "lastMoved", _lastMoved, 1, Time(0)))
    CHECK(ar.Serialize("nextSampleIndex", _nextSampleIndex, 1, 0))
    CHECK(ar.SerializeEnum("useSuppressiveFire", _useSuppressiveFire, 1, SuppressNo))
    CHECK(::Serialize(ar, "suppressUntil", _suppressUntil, 1, Time(0)))
  }

  CHECK(_weaponsState.Serialize(this, ar))

  return LSOK;
}

/*!
\patch_internal 5090 Date 11/28/2006 by Jirka
- Fixed: Diagnostics DECostMap did not show water fields
*/

void EntityAIFull::DrawDiags()
{
  Vector3 camPos = GScene->GetCamera()->Position();
    
#if _ENABLE_CHEATS
  AIBrain *unit=CommanderUnit();
  if( !unit )
  {
    base::DrawDiags();
    return;
  }
#define DRAW_OBJ(obj) GScene->DrawObject(obj)

  LODShapeWithShadow *forceArrow=GScene->ForceArrow();

  if (CHECK_DIAG(DEFSM))
  {
    AIBrain *driver = PilotUnit();
    AIUnit *driverUnit = driver ? driver->GetUnit() : NULL;
    if (driverUnit)
    {
      float size = driverUnit->GetFSMDiagSize();
      if (size > 0)
      {
        PackedColor color = driverUnit->GetFSMDiagColor();

        Ref<Object> obj = new ObjectColored(GScene->Preloaded(SphereModel), VISITOR_NO_ID);
        obj->SetPosition(RenderVisualState().Position() + 2 * VUp);
        obj->SetScale(size);
        obj->SetConstantColor(color);
        DRAW_OBJ(obj);
      }
    }
  }

  if (!QIsManual() && CHECK_DIAG(DECombat))
  {
    float attackSize=_attackTarget.IdExact() ? 5 : 2;
    GScene->DrawDiagArrow("Attack Aggres.",_attackAggressivePos,Vector3(0,+1,0),attackSize*0.9,Color(1,0,0),true);
    GScene->DrawDiagArrow("Attack Econom.",_attackEconomicalPos,Vector3(0,+1,0),attackSize,Color(1,0.5,0),true);

    // let brain draw diagnostics as well    
    AIBrain *driver = PilotUnit();
    if (driver)
      driver->DrawDiags();
  }

  if (CHECK_DIAG(DECombat) || CHECK_DIAG(DEPath))
  {
    RString fsm,path;
    AIBrain *driver = PilotUnit();
    if (driver)
    {
      int nBeh = driver->NBehaviourDiags();
      for (int i=0; i<nBeh; i++)
      {
        RString str = driver->BehaviourDiags(i);
        if (str.GetLength()>0)
          fsm = fsm + " " + str;
      }
      path = Format(" path %d",driver->GetPathId());
    }
    GScene->DrawDiagText(GetDebugName()+fsm+path,AimingPosition(RenderVisualState())+VUp*CollisionSize());
  }

#if 1

  if (CHECK_DIAG(DEPath) &&
    !GetType()->IsKindOf(GLOB_WORLD->Preloaded(VTypeStatic)) &&
    !_isDead)
  {
    // positions history
    LODShapeWithShadow *sphere = GScene->Preloaded(SphereModel);
    for (int i=0; i<PositionsHistorySamples; i++)
    {
      Ref<Object> obj = new ObjectColored(sphere, VISITOR_NO_ID);
      obj->SetPosition(_positionsHistory[i]);
      obj->SetConstantColor(Color(0, 0, 1, 0.5));
      obj->SetScale(0.10f);
      DRAW_OBJ(obj);
    }
    /*
    // extrapolated positions
    for (int i=0; i<10; i++)
    {
      Vector3 extraPos = ExtrapolateHistoryPosition(i*5.0f,NULL);
      Ref<Object> obj = new ObjectColored(sphere, VISITOR_NO_ID);
      obj->SetPosition(extraPos);
      obj->SetConstantColor(Color(0, 0.5, 1, 0.5));
      obj->SetScale(0.15f);
      DRAW_OBJ(obj);
    }
    */
    
    // extrapolated positions
    for (int i=0; i<20; i++)
    {
      Vector3 extraPos = NearestHistoryPosition(RenderVisualState().Position()-RenderVisualState().Direction()*(i*5.0f),NULL);
      Ref<Object> obj = new ObjectColored(sphere, VISITOR_NO_ID);
      obj->SetPosition(extraPos);
      obj->SetConstantColor(Color(0, 1.0, 0.5, 0.5));
      obj->SetScale(0.10f);
      DRAW_OBJ(obj);
    }
  }

  if (CHECK_DIAG(DECombat) || CHECK_DIAG(DEVisualStates))
  {
    LODShapeWithShadow *sphere = GScene->Preloaded(SphereModel);
    { // future visual state - green/blue
      Ref<Object> obj = new ObjectColored(sphere, VISITOR_NO_ID);
      obj->SetPosition(AimingPosition(FutureVisualState(),NULL));
      obj->SetConstantColor(Color(0, 1.0, 0.0, 0.5));
      obj->SetScale(0.50f);
      DRAW_OBJ(obj);
    }
    if (_remoteState)
    { 
      { // remote state - blue/green
        Ref<Object> obj = new ObjectColored(sphere, VISITOR_NO_ID);
        obj->SetPosition(_remoteState->pos);
        obj->SetConstantColor(Color(0, 0.5, 0.0, 1.0));
        obj->SetScale(0.73f);
        DRAW_OBJ(obj);
      }
      { // extrapolated remote state - blue
        Ref<Object> obj = new ObjectColored(sphere, VISITOR_NO_ID);
        float age = Glob.time-_remoteState->time;
        obj->SetPosition(_remoteState->pos+age*_remoteState->speed);
        obj->SetConstantColor(Color(0, 0.1, 0.0, 1.0));
        obj->SetScale(0.40f);
        DRAW_OBJ(obj);
      }
    }
  }
    
  if ((CHECK_DIAG(DEPath) || CHECK_DIAG(DEPathFind)) &&
    !GetType()->IsKindOf(GLOB_WORLD->Preloaded(VTypeStatic)) &&
    !_isDead &&
    !(GWorld->PlayerManual() && GWorld->PlayerOn() && GWorld->PlayerOn()->Brain() == PilotUnit()))
  {
    // steerpoints
    {
      Point3 steerPos=SteerPoint(GetSteerAheadSimul(),GetSteerAheadPlan(),NULL,NULL,false);
      //AvoidCollision(steerPos);

      {
        Ref<Object> arrow=new ObjectColored(forceArrow,VISITOR_NO_ID);
        Point3 pos=steerPos;

        float size=0.3f;
        arrow->SetPosition(pos);
        arrow->SetOrient(VUp,VForward);
        arrow->SetPosition(arrow->RenderVisualState().PositionModelToWorld(forceArrow->BoundingCenter()*size));
        arrow->SetScale(size);
        arrow->SetConstantColor(Color(0.7,0.5,0));
        //#define DRAW_OBJ(obj) (obj)->Draw(0,ClipAll,*(obj))
        DRAW_OBJ(arrow);
      }
      
      steerPos += RenderVisualState().DirectionAside()*_avoidAside;
      
      {
        Ref<Object> arrow=new ObjectColored(forceArrow,VISITOR_NO_ID);
        Point3 pos=steerPos;

        float size=0.1f;
        arrow->SetPosition(pos);
        arrow->SetOrient(VUp,VForward);
        arrow->SetPosition(arrow->RenderVisualState().PositionModelToWorld(forceArrow->BoundingCenter()*size));
        arrow->SetScale(size);
        arrow->SetConstantColor(Color(0.7,0.5,0));
        //#define DRAW_OBJ(obj) (obj)->Draw(0,ClipAll,*(obj))
        DRAW_OBJ(arrow);
      }
    }

    {
      Point3 steerPos=SteerPoint(GetPredictTurnSimul(),GetPredictTurnPlan());

      Ref<Object> arrow=new ObjectColored(forceArrow,VISITOR_NO_ID);
      Point3 pos=steerPos;

      float size=0.2f;
      arrow->SetPosition(pos);
      arrow->SetOrient(VUp,VForward);
      arrow->SetPosition(arrow->RenderVisualState().PositionModelToWorld(forceArrow->BoundingCenter()*size));
      arrow->SetScale(size);
      arrow->SetConstantColor(Color(0.9,0.3,0.3));
      DRAW_OBJ(arrow);
    }

    if (unit->GetFormationLeader() == unit)
    {
      { // draw strategic GoTo destination
        float size=GLOB_WORLD->CameraOn()==this ? 8 : 4;
        GScene->DrawDiagArrow(GetDebugName()+" Go To",_stratGoToPos,VUp,size,Color(1,1,1),true);
      }
      { // draw command destination, if different from _stratGoToPos
        AISubgroup *subgrp = unit->GetUnit() ? unit->GetUnit()->GetSubgroup() : NULL;
        if( subgrp && subgrp->GetCommand() )
        {
          Vector3Val pos=subgrp->GetCommand()->_destination;
          //if (_stratGoToPos.Distance2(pos)>Square(0.1f))
          {
            float size=GLOB_WORLD->CameraOn()==this ? 6 : 3;
            GScene->DrawDiagArrow(GetDebugName()+" Command",pos,VUp,size,Color(0,1,0.5),true);
          }
        }
      }
    }
    else if (IsMovingInConvoy())
    {
      // what position are we really following
      Vector3 pos = unit->GetWantedPosition();

      Ref<Object> arrow=new ObjectColored(forceArrow,VISITOR_NO_ID);

      float size=0.5;
      if( GLOB_WORLD->CameraOn()==this ) size=2;
      arrow->SetPosition(pos);
      arrow->SetOrient(VUp,VForward);
      arrow->SetPosition(arrow->RenderVisualState().PositionModelToWorld(forceArrow->BoundingCenter()*size));
      arrow->SetScale(size);
      arrow->SetConstantColor(Color(0.5,1,1));

      DRAW_OBJ(arrow);
    }
    else
    {
      // expected position in the formation
      AIGroup *grp = unit->GetGroup();
      AIUnit *leader = grp ? grp->Leader() : NULL;
      if (leader)
      {
        Assert(unit->GetUnit());
        // predict leader position
        float estT=GetFormationTime();
        
        // when soldier are moving in combat, they use leap frogging, and we do not want them
        // to leap ahead of their leader too much
        Vector3Val movePos = unit->GetUnit()->GetFormationAbsolute(estT);
        Vector3Val formAbs = unit->GetUnit()->GetFormationAbsolute();

        { // draw predicted formation position
          Ref<Object> arrow=new ObjectColored(forceArrow,VISITOR_NO_ID);

          float size=0.3;
          if( GLOB_WORLD->CameraOn()==this ) size=2;
          arrow->SetPosition(movePos);
          arrow->SetOrient(VUp,VForward);
          arrow->SetPosition(arrow->RenderVisualState().PositionModelToWorld(forceArrow->BoundingCenter()*size));
          arrow->SetScale(size);
          arrow->SetConstantColor(Color(0.5,1,1));

          DRAW_OBJ(arrow);
        }

        { // draw current formation position
          Ref<Object> arrow=new ObjectColored(forceArrow,VISITOR_NO_ID);

          Assert(unit->GetUnit());
          arrow->SetPosition(formAbs);
          arrow->SetOrient(VUp,VForward);
          float size = 0.4;
          arrow->SetPosition(arrow->RenderVisualState().PositionModelToWorld(forceArrow->BoundingCenter()*size));
          arrow->SetScale(size);
          arrow->SetConstantColor(Color(1,1,1));

          DRAW_OBJ(arrow);
        }
      }
    }
  }
#endif

#if 1
    // draw current weapon direction
  if (CHECK_DIAG(DECombat))
  {
    // TODO: which turret?
    TurretContextEx context;
    bool valid = GetPrimaryGunnerTurret(unconst_cast(RenderVisualState()), context);

    {
      Vector3 dir;
      if (valid)
        dir = GetWeaponDirection(RenderVisualState(), context, 0);
      else
        dir = RenderVisualState().Direction();
      if (dir.SquareSize()>0.1)
      {
        Ref<Object> arrow=new ObjectColored(forceArrow,VISITOR_NO_ID);

        float size=0.1;
        arrow->SetPosition(RenderVisualState().Position());
        // avoid singular matrix
        if (dir.Y()<0.9f) arrow->SetOrient(dir,VUp);
        else arrow->SetOrient(dir,VAside);
        arrow->SetPosition(arrow->RenderVisualState().PositionModelToWorld(forceArrow->BoundingCenter()*size));
        arrow->SetScale(size);
        arrow->SetConstantColor(Color(1,0,0,0.5));

        DRAW_OBJ(arrow);
      }
    }
    {
      Vector3 dir;
      if (valid)
        dir = GetWeaponDirectionWanted(context, 0);
      else
        dir = RenderVisualState().Direction();
      if (dir.SquareSize()>0.1)
      {
        Ref<Object> arrow=new ObjectColored(forceArrow,VISITOR_NO_ID);

        float size=0.2;
        arrow->SetPosition(RenderVisualState().Position());
        arrow->SetOrient(dir,VUp);
        arrow->SetPosition(arrow->RenderVisualState().PositionModelToWorld(forceArrow->BoundingCenter()*size));
        arrow->SetScale(size);
        arrow->SetConstantColor(Color(1,0.1,0,0.5));

        DRAW_OBJ(arrow);
      }
    }
    {
      Vector3 dir;
      if (valid)
        dir = GetEyeDirection(RenderVisualState(), context);
      else
        dir = RenderVisualState().Direction();
      if (dir.SquareSize()>0.1)
      {
        Ref<Object> arrow=new ObjectColored(forceArrow,VISITOR_NO_ID);

        float size=0.2;
        arrow->SetPosition(RenderVisualState().Position());
        arrow->SetOrient(dir,VUp);
        arrow->SetPosition(arrow->RenderVisualState().PositionModelToWorld(forceArrow->BoundingCenter()*size));
        arrow->SetScale(size);
        arrow->SetConstantColor(Color(1,1,0,0.5));

        DRAW_OBJ(arrow);
      }
    }
  }
#endif

  if (CHECK_DIAG(DEPath) && GetType()->IsSupply())
  {
    Ref<Object> arrow=new ObjectColored(forceArrow,VISITOR_NO_ID);
    float size=0.2;
    Vector3 aPos=RenderVisualState().PositionModelToWorld(GetType()->_supplyPoint);
    arrow->SetPosition(aPos);
    arrow->SetOrient(Vector3(0,-1,0),Vector3(0,0,-1));
    arrow->SetPosition(arrow->RenderVisualState().PositionModelToWorld(-forceArrow->BoundingCenter()*size));
    arrow->SetScale(size);
    arrow->SetConstantColor(Color(0,1,0));
    DRAW_OBJ(arrow);
  }

  // draw path
  if (PilotUnit())
  {
    if(CHECK_DIAG(DEPath))
    {
      const Path &path=PilotUnit()->GetPath();
      float size=0.05;
      PackedColor color(Color(1,0,1,1));
      if (this==GLOB_WORLD->CameraOn())
        size*=2;
      for (int i=1; i<path.Size(); i++)
      {
        if (unit->GetFormationLeader() == unit)
          color=PackedColor(Color(1,1,0,1));
        Ref<LODShapeWithShadow> shape=ObjectLine::CreateShape();
        Ref<Object> lineObj=new ObjectLineDiag(shape);
        lineObj->SetConstantColor(color);

        const OperInfoResult &cur=path[i-1];
        const OperInfoResult &nxt=path[i];
        Vector3 cPos = cur._pos;
        Vector3 nPos = nxt._pos;
        saturateMax(cPos[1],GLandscape->SurfaceYAboveWater(cPos[0],cPos[2])+0.05f);
        saturateMax(nPos[1],GLandscape->SurfaceYAboveWater(nPos[0],nPos[2])+0.05f);
        lineObj->SetPosition(cPos);
        ObjectLine::SetPos(shape,VZero,nPos-cPos);

        DRAW_OBJ(lineObj);
      }
      for (int i=0; i<path.Size(); i++)
      {
        const OperInfoResult &info=path[i];

        Ref<Object> arrow=new ObjectColored(forceArrow,VISITOR_NO_ID);

        Vector3Val pos = info._pos;
        arrow->SetPosition(pos);
        bool revert = info._cost > path.GetReplanCost();
        if (revert)
        {
          arrow->SetOrient(Vector3(0,-1,0),Vector3(0,0,-1));
          arrow->SetPosition(arrow->RenderVisualState().PositionModelToWorld(-forceArrow->BoundingCenter()*size));
        }
        else
        {
          arrow->SetOrient(Vector3(0,+1,0),Vector3(0,0,+1));
          arrow->SetPosition(arrow->RenderVisualState().PositionModelToWorld(forceArrow->BoundingCenter()*size));
        }
        arrow->SetScale(size);
        if(info._type == NTRoad)
          color=PackedColor(Color(0,1,1,1));

        arrow->SetConstantColor(color);

        DRAW_OBJ(arrow);
      }
      // some special points on the path
      if (path.Size() >= 2)
      {
        // replan point
        Vector3 pos = path.PosAtCost(path.GetReplanCost());

        Ref<Object> obj = new ObjectColored(GScene->Preloaded(SphereModel), VISITOR_NO_ID);
        obj->SetPosition(pos);
        obj->SetScale(4.0f * size);
        obj->SetConstantColor(PackedColor(Color(1, 1, 0, 1)));
        DRAW_OBJ(obj);
      }
      if (path.Size() >= 2)
      {
        // force replan point
        Vector3 pos = path.PosAtCost(path.GetForceReplanCost());

        Ref<Object> obj = new ObjectColored(GScene->Preloaded(SphereModel), VISITOR_NO_ID);
        obj->SetPosition(pos);
        obj->SetScale(4.0f * size);
        obj->SetConstantColor(PackedColor(Color(1, 0, 0, 1)));
        DRAW_OBJ(obj);
      }
      {
        // replanning from
        Vector3Val pos = PilotUnit()->GetPlanningFrom();

        Ref<Object> obj = new ObjectColored(GScene->Preloaded(SphereModel), VISITOR_NO_ID);
        obj->SetPosition(pos);
        obj->SetScale(4.0f * size);
        obj->SetConstantColor(PackedColor(Color(0, 1, 0, 1)));
        DRAW_OBJ(obj);
      }
    }
  }

  base::DrawDiags();
#endif
}

void EntityAIFull::DrawFocusedDiags()
{
#if _ENABLE_CHEATS
  AIBrain *unit=CommanderUnit();
  if (unit)
  {
    const float maxDist = 50.0f;
    const float maxDistDetailed = 30.0f;
    const float maxDistExtended = 20.0f;
    int diagRange = toIntCeil(maxDist*InvLandGrid);
    
    Vector3 camPos = GScene->GetCamera()->Position();
      
    // draw cost for a focused unit, or if there is none, for a player (usefull for free camera)
    {
      LODShapeWithShadow *rectShape = GScene->Preloaded(RectangleModel);
      int index=0;
  
#if 1
      // draw fields costs
      if (CHECK_DIAG(DECostMap))
      {
        bool soldier = unit->IsFreeSoldier();
        LandIndex xLand(toIntFloor(camPos.X() * InvLandGrid));
        LandIndex zLand(toIntFloor(camPos.Z() * InvLandGrid));

        Vector3 hideL, hideR;

        AIUnit *uu = unit->GetUnit();
        if (uu)
          // computing - works for the player as well
          uu->ComputeHideFrom(hideL,hideR);
        else
          // fallback - get values as given by FSM, works only for AI units
          unit->GetHideFrom(hideL,hideR);

        for (LandIndex z(zLand-diagRange); z<=zLand+diagRange; z++) for (LandIndex x(xLand-diagRange); x<=xLand+diagRange; x++)
        {
          Ref<OperField> fld = GLOB_LAND->OperationalCache()->GetOperField(
            x, z, MASK_AVOID_OBJECTS | MASK_PREFER_ROADS | MASK_USE_BUFFER
          );
          if (!fld)
            continue;
          // we want to display the cover as well
          fld->PrepareForPathing();

          for (OperFieldIndex zz(0); zz<OperItemRange; zz++) for (OperFieldIndex xx(0); xx<OperItemRange; xx++)
          {

            Vector3 pos;
            pos.Init();
            pos[0] = x * LandGrid + xx * OperItemGrid + 0.5 * OperItemGrid;
            pos[2] = z * LandGrid + zz * OperItemGrid + 0.5 * OperItemGrid;
            pos[1] = 0;

            if (pos.DistanceXZ2(camPos) <= Square(maxDist))
            {
              OperItemType type = soldier ?
                fld->GetItem(xx, zz).GetTypeSoldier() : fld->GetItem(xx, zz).GetType();
              static const Color typeColor[NOperItemType]=
              {
                Color(1, 1, 1, 0), // OITNormal,
                Color(0, 1, 1, 0.25), // OITAvoidBush,
                Color(0, 1, 0, 0.25), // OITAvoidTree,
                Color(0, 0, 0, 0.25), // OITAvoid,
                Color(0, 0, 1, 1), // OITWater,
                Color(1, 0, 1, 1), // OITSpaceRoad,
                Color(1, 1, 1, 1), // OITRoad,
                Color(0, 1, 1, 1), // OITSpaceBush,
                Color(0, 1, 0.5, 1), // OITSpaceHardBush,
                Color(0, 1, 0, 1), // OITSpaceTree,
                Color(0, 0, 0, 1), // OITSpace
                Color(1, 1, 0, 1), // OITRoadForced
              };
              ColorVal color = typeColor[type];

              pos[1] = GLOB_LAND->SurfaceYAboveWater(pos[0],pos[2]) + 0.2;
              Ref<Object> rect;
              if (index<MapDiags.Size())
                rect = MapDiags[index];
              else
              {
                rect = new ObjectColored(rectShape, VISITOR_NO_ID);
                // scale the rectangle by the grid
                float size = rectShape->BoundingSphere() * H_SQRT2;
                rect->SetScale(OperItemGrid / size);
                MapDiags.Access(index);
                MapDiags[index]=rect;
              }
              index++;
              rect->SetPosition(pos);
              rect->SetConstantColor(color);
              DRAW_OBJ(rect);

              // we want to show the cover only close
              if (pos.DistanceXZ2(camPos) <= Square(maxDistDetailed))
              {
                float CoverSuitableAgainst(const OperCover &cover, Vector3Par hideLPos, Vector3Par hideRPos);

                int nCover = 0;

                // we want to show diagnostics about cover suitability only when being very close
                bool extended = pos.DistanceXZ2(camPos) <= Square(maxDistExtended);

                // display diagnostics for each cover candidate
                const OperCover *cover = fld->GetCover(xx,zz,nCover);
                for (int c=0; c<nCover; c++)
                {
                  Matrix3 orient(MRotationY,-cover[c]._heading);
                  Vector3Val cPos = cover[c]._pos;

                  float suitable = 0.5; 
                  if (extended)
                  {
                    Ref<Object> DrawDiagLine(Vector3Par from, Vector3Par to, ColorVal color);
                    
                    suitable = CoverSuitableAgainst(cover[c],hideL,hideR);
                    
#if DIAG_FALSE_COVER
                      bool valid = cover[c]._valid==OperCover::Valid;
#else
                      const bool valid = true;
#endif
                    // indicate what field is this cover associated with (such field is an entry)
                    Ref<Object> line = DrawDiagLine(cPos+orient.Direction()*0.45f,pos,valid ? Color(0,0.5,0) : Color(0.25,0.25,0));
                    DRAW_OBJ(line);
                  }
                  
                  LODShapeWithShadow *forceArrow=GScene->ForceArrow();

                  Ref<Object> arrow=new ObjectColored(forceArrow,VISITOR_NO_ID);
                  float size=0.05f*(suitable+1);
                  arrow->SetPosition(cPos);
                  arrow->SetOrient(orient);
                  arrow->SetPosition(arrow->RenderVisualState().PositionModelToWorld(forceArrow->BoundingCenter()*size));
                  arrow->SetScale(size);
                  
#if DIAG_FALSE_COVER
                    // when not valid, suitable would be 0
                    switch (cover[c]._valid)
                    {
                      case OperCover::Valid: arrow->SetConstantColor(Color(1-suitable,suitable,0)); break; // red..green
                      case OperCover::NoFreeSpace: arrow->SetConstantColor(Color(0,0,1.0)); break; // light blue
                      case OperCover::NoCollision: arrow->SetConstantColor(Color(0.5,0,0.5)); break; // dark magenta
                      case OperCover::Unaccessible: arrow->SetConstantColor(Color(0.1,0.1,0.0)); break; // very dark yellow
                      default: arrow->SetConstantColor(Color(0.3,0.3,0.3)); break; // dark gray
                    }
#else
                    arrow->SetConstantColor(Color(1-suitable,suitable,0));
#endif
                  DRAW_OBJ(arrow);
                }
              }
            }
          }
        }
      }
#endif
      // done: draw field costs

      // prepare list of targets for suppress calculations
      SuppressTargetList suppressTargets;
      if (CHECK_DIAG(DERoads) || CHECK_DIAG(DECombat))
        suppressTargets.CollectTargets(unit);

      // draw road connections
      if (CHECK_DIAG(DERoads))
      {
        bool soldier = CommanderUnit() && CommanderUnit()->IsFreeSoldier();
        LODShape *shape = GetShape();
        float vehWidth = 0;
        if (shape) vehWidth = shape->Max().X() - shape->Min().X();

        LODShapeWithShadow *sphere = GScene->Preloaded(SphereModel);

        int x, xLand = toIntFloor(camPos.X() * InvLandGrid);
        int z, zLand = toIntFloor(camPos.Z() * InvLandGrid);
        for (z=zLand-diagRange; z<=zLand+diagRange; z++) for (x=xLand-diagRange; x<=xLand+diagRange; x++)
        if (InRange(x,z))
        {
          // adapted from  CStaticMapMain.DrawRoadNet
          const RoadList &list = GLandscape->GetRoadNet()->GetRoadList(x, z);
          for( int r=0; r<list.Size(); r++ )
          {
            const RoadLink *item = list[r];
            if (!item) continue;

            static const Vector3 above(0, 1, 0);
            static const Vector3 aboveOff(0, 1.2f, 0);
            static const PackedColor colorCenter(Color(1, 1, 1, 0.5));
            static const PackedColor colorCenterBridge(Color(1, 1, 0.5, 0.5));
            static const PackedColor colorCenterLocked(Color(1, 0, 0, 0.5));
            static const PackedColor colorCenterWaiting(Color(1, 1, 0, 0.5));
            static const PackedColor colorConnection(Color(0, 0.5, 1, 0.5));
            static const PackedColor colorConnectionLine(Color(0, 1, 0, 1));
            static const PackedColor colorLinkLine(Color(0, 0.5, 0.4, 1));
            static const PackedColor colorNoConnectionLine(Color(1, 0, 0, 1));
            Vector3 center = item->AIPosition() + above;
            // road center
            {
              Ref<Object> obj = new ObjectColored(sphere, VISITOR_NO_ID);
              obj->SetPosition(center);
              PackedColor color = item->IsBridge() ? colorCenterBridge : colorCenter;
              if (item->IsLocked(unit->GetPerson() == this)) color = colorCenterLocked;
              else if (item->IsLockedByWaiting(unit->GetPerson() == this)) color = colorCenterWaiting;
              obj->SetConstantColor(color);
              obj->SetScale(0.1);
              DRAW_OBJ(obj);
            }
            {
              float supCost = item->GetSuppressionCost(suppressTargets);
              if (supCost>0)
              {
                float safe = InterpolativC(supCost,0,27,1,0);
                float danger = InterpolativC(supCost,27,1000,0,1);
                Color color(danger,1-danger-safe,safe);
                // draw the sphere somewhat above the standard road sphere
                Ref<Object> obj = new ObjectColored(sphere, VISITOR_NO_ID);
                obj->SetPosition(center+VUp*0.2f);
                obj->SetConstantColor(color);
                obj->SetScale(0.1);
                DRAW_OBJ(obj);
              }
            }

            for (int j=0; j<item->NNodes(soldier); j++)
            {
              RoadNode node = item->GetNode(j);
              if (!node.IsValid()) continue;

              Vector3 posB = node.Position(soldier, vehWidth);

              for (int k=0; k<node.NConnections(soldier); k++)
              {
                RoadNode to = node.GetConnection(soldier, k);
                if (!to.IsValid()) continue;

                Vector3 posE = to.Position(soldier, vehWidth);

                // connection position
                Ref<Object> obj = new ObjectColored(sphere, VISITOR_NO_ID);
                obj->SetPosition(posE);
                obj->SetConstantColor(colorConnection);
                obj->SetScale(0.07);
                DRAW_OBJ(obj);

                // connection link
                GEngine->DrawVerticalArrow(posB+aboveOff, posE+aboveOff,colorConnectionLine, 3, 0, 0.7);
              }
            }
          }
        }
      }

#if 1
      if (CHECK_DIAG(DELockMap))
      {
        bool soldier = unit->IsFreeSoldier();
        OperMapIndex operX(toInt(camPos.X()*InvOperItemGrid));
        OperMapIndex operZ(toInt(camPos.Z()*InvOperItemGrid));
        ILockCache *locks=GLandscape->LockingCache();
        const int range=50;
        for (OperMapIndex x(operX-range); x<=operX+range; x++)
        for (OperMapIndex z(operZ-range); z<=operZ+range; z++)
        {
          bool lockV = locks->IsLocked(x,z,false) > 0;
          bool lockS = locks->IsLocked(x,z,true) > 0;

          if (!lockV && !lockS)
            continue;

          float alpha = (soldier ? lockS : lockV) ? 1.0f : 0.3f;
          Color color(Color(lockV,0,lockS,alpha));

          Vector3 pos;
          pos.Init();
          pos[0] = x * OperItemGrid + 0.5 * OperItemGrid;
          pos[2] = z * OperItemGrid + 0.5 * OperItemGrid;
#if _ENABLE_WALK_ON_GEOMETRY
          pos[1] = GLOB_LAND->RoadSurfaceYAboveWater(pos[0],pos[2],Landscape::FilterIgnoreOne(this))+0.2;
#else
          pos[1] = GLOB_LAND->RoadSurfaceYAboveWater(pos[0],pos[2])+0.2;
#endif

          if ((pos - camPos).SquareSizeXZ() <= Square(50))
          {
            Ref<Object> rect;
            if (index < MapDiags.Size())
              rect = MapDiags[index];
            else
            {
              rect = new ObjectColored(rectShape, VISITOR_NO_ID);
              float size = rectShape->BoundingSphere() * H_SQRT2;
              rect->SetScale(OperItemGrid / size);
              MapDiags.Access(index);
              MapDiags[index]=rect;
            }
            index++;
            rect->SetPosition(pos);
            rect->SetConstantColor(color);
            DRAW_OBJ(rect);
          }
        }
      }
#endif

#if 1
      // draw suppressive fire maps
      if (CHECK_DIAG(DECombat))
      {
        LandIndex xLand(toIntFloor(camPos.X() * InvLandGrid));
        LandIndex zLand(toIntFloor(camPos.Z() * InvLandGrid));
        for (LandIndex z(zLand-diagRange); z<=zLand+diagRange; z++) for (LandIndex x(xLand-diagRange); x<=xLand+diagRange; x++)
        {
          Ref<SuppressField> fld = GLOB_LAND->SuppressCache()->GetField(x, z, MASK_NO_UPDATE|MASK_NO_CREATE);
          if (!fld) continue;
          
          for (OperFieldIndex zz(0); zz<OperItemRange; zz++) for (OperFieldIndex xx(0); xx<OperItemRange; xx++)
          {
            Vector3 pos;
            pos.Init();
            pos[0] = x * LandGrid + xx * OperItemGrid + 0.5 * OperItemGrid;
            pos[2] = z * LandGrid + zz * OperItemGrid + 0.5 * OperItemGrid;
            pos[1] = 0;

            if (pos.DistanceXZ2(camPos) <= Square(maxDist))
            {
              // get cost as viewed by focused unit
              
              // typical friendly cost is something like 270
              // typical enemy cost is something like 9000
              float supCost = fld->GetCost(xx,zz,suppressTargets);
              if (supCost==0)
                continue;
              
              float safe = InterpolativC(supCost,0,27,1,0);
              float danger = InterpolativC(supCost,27,1000,0,1);
              Color color(danger,1-danger-safe,safe);

              pos[1] = GLOB_LAND->SurfaceYAboveWater(pos[0],pos[2]) + 0.2f;
              Ref<Object> rect;
              if (index<MapDiags.Size())
                rect = MapDiags[index];
              else
              {
                rect = new ObjectColored(rectShape, VISITOR_NO_ID);
                // scale the rectangle by the grid
                float size = rectShape->BoundingSphere() * H_SQRT2;
                rect->SetScale(OperItemGrid / size);
                MapDiags.Access(index);
                MapDiags[index]=rect;
              }
              index++;
              rect->SetPosition(pos);
              rect->SetConstantColor(color);
              DRAW_OBJ(rect);
            }
          }
        }
      }
#endif
    }
    
    if (CHECK_DIAG(DEPathFind))
    {
      AIBrain *unit = PilotUnit();
      if (unit)
      {
        const OperMap &map = unit->GetOperMap();
        map.Path3DDiags(unit);
      }
    }
    
  }
#endif
  base::DrawDiags();
}


class IsCombatUnitFunc : public ITurretFunc
{
public:
  bool operator () (EntityAIFull *entity, TurretContext &context)
  {
    if (context._weapons->_magazines.Size() > 0)
      return true;
    if (context._weapons->_magazineSlots.Size() <= 0)
      return false;
    // we do not have magazines, but we have some magazine slots
    // check if we have some real weapon
    for (int i=0; i<context._weapons->_magazineSlots.Size(); i++)
    {
      const MagazineSlot &slot = context._weapons->_magazineSlots[i];
      const MuzzleType *muzzle = slot._muzzle;
      if (muzzle->_magazines.Size() > 0)
        return true;
    }
    return false;
  }
};

/*!
\patch 1.17 Date 8/14/2001 by Ondra.
- Fixed: AI not attacking on weapon-less targets.
*/

bool EntityAIFull::IsCombatUnit() const
{
  IsCombatUnitFunc func;
  return ForEachTurret(func);
}

SuppressState EntityAIFull::CheckSuppressiveFire() const
{
  if (Glob.time<_suppressUntil) return SuppressIntense;
  return _useSuppressiveFire;
}

CombatMode EntityAIFull::GetCombatModeAutoDetected(const AIBrain*brain) const
{
  return CMSafe;
}

void EntityAI::ClassCleanUp()
{
  RecoilFunctions.Clear();
}


WeaponsState::WeaponsState()
: _currentWeapon(-1),
  _forceFireWeapon(-1),
  _laserTargetOn(false),
  _mGunFireUntilFrame(0),
  _mGunFireTime(TIME_MIN),
  _mGunFirePhase(0),
  _lastShotTime(Glob.time-120),
  _targetAimed(0),
  _currentCounterMeasures(-1),
  _primaryHeatFactor(0),
  _secondaryHeatFactor(0),
  _handGunHeatFactor(0),
  _throwIntensityCoef(1.0f)
{
#if _VBS3
  _forceFirePosition = VZero;
#endif
  _lastTrace = Glob.time;
  _backpack = NULL;
}

void WeaponsState::Init(const WeaponsType &type, ParamEntryPar cfg, EntityAIFull *owner, bool createWeapons)
{
  // on remote clients, do not create magazines and weapons, but still initialize the parameters of effects
  if (createWeapons)
  {
    // Magazines
    int n = type._magazines.Size();
    _magazines.Realloc(n);
    _magazines.Resize(n);
    int j = 0;
    for (int i=0; i<n; i++)
    {
      const MagazineType *mType = type._magazines[i];
      if (!mType) continue;
      _magazines[j] = new Magazine(mType);
      _magazines[j]->SetAmmo(mType->_maxAmmo);
      _magazines[j]->_reload = 1;
      _magazines[j]->_reloadDuration = 0.8 + 0.2 * GRandGen.RandomValue();
      _magazines[j]->_reloadMagazine = 0;
      _magazines[j]->_reloadMagazineTotal = 0;
      j++;
    }
    _magazines.RemoveNulls();

    // Weapons
    n = type._weapons.Size();
    for (int i=0; i<n; i++)
      AddWeapon(owner, type._weapons[i]);
    _weapons.Compact();
    _magazineSlots.Compact();

    if (owner)
      owner->AutoReloadAll(*this, true);

    n = _magazineSlots.Size();

    _visionPars.Realloc(n);
    _visionPars.Resize(n);


    _primaryHeatFactor = 0.0f;
    _secondaryHeatFactor = 0.0f;
    _handGunHeatFactor = 0.0f;

    for (int i = 0; i < n; i++)
    {
      MagazineSlot &slot = _magazineSlots[i];
      VisionModePars &vPars = _visionPars[i];

      if(slot._muzzle && slot._muzzle->HasVisionModes(slot._opticsMode))
      {
        slot._muzzle->GetInitVisionMode(vPars._visionMode, vPars._flirIndex, slot._opticsMode);
      }
      else
      {
        vPars._flirIndex = 0;
        vPars._visionMode = VMNormal;
      }

      if(slot._muzzle)
      {
        slot._discreteIndex = slot._muzzle->_opticsInfo[slot._opticsMode]._discreteInitIndex;
        slot._discreteDistanceIndex  = slot._muzzle->_opticsInfo[slot._opticsMode]._discreteDistanceInitIndex;
      }
      else
      {
        slot._discreteIndex = 0;
        slot._discreteDistanceIndex = 0;
      }
    }
  }

  _mGunClouds.Load(cfg >> "MGunClouds");
  _gunClouds.Load(cfg >> "GunClouds");
  _gunFire.Load(cfg >> "GunFire");

  _lockingTarget = NULL;
  _aimedTarget = NULL;
  _throwIntensityCoef = 1.0f;
}

WeaponsState::~WeaponsState()
{
  // if there are any weapons left, release their shapes
  for (int i=0; i<_weapons.Size(); i++)
  {
    WeaponType *weapon = _weapons[i];
    weapon->ShapeRelease();
  }
  if(_backpack) 
  {
    if(GWorld->IsOutVehicle(_backpack))
    { 
      GWorld->RemoveOutVehicle(_backpack);
      //delete unit's bag
      _backpack->SetDelete();
    }
    _backpack = NULL;
  }
}

void WeaponsState::GetVisionModePars(VisionMode &vMode, int &flirIndex) const
{
  const VisionModePars &vPars = _visionPars[_currentWeapon];

  vMode = vPars._visionMode;
  flirIndex = vPars._flirIndex;
}

bool WeaponsState::HasCurrentWeaponTi() const
{
  if (_currentWeapon >= 0 && _currentWeapon < _magazineSlots.Size())
  {
    const MagazineSlot &slot = _magazineSlots[_currentWeapon];
    if (slot._muzzle) return slot._muzzle->HasTiVision();
  }

  return false;
}

bool WeaponsState::NextVisionMode()
{
  if (_currentWeapon >= 0 && _currentWeapon < _magazineSlots.Size())
  {
    MagazineSlot &slot = _magazineSlots[_currentWeapon];

    if (slot._muzzle && slot._muzzle->HasVisionModes(slot._opticsMode))
    {
      VisionModePars &vPars = _visionPars[_currentWeapon];

      // select next vision mode based on config optics params
      slot._muzzle->NextVisionMode(vPars._visionMode, vPars._flirIndex,slot._opticsMode);

      for (int i = 0; i<_magazineSlots.Size(); i++)
      {
        if(_currentWeapon == i) continue;
        MagazineSlot &otherSlot = _magazineSlots[i];
        if(slot._muzzle != otherSlot._muzzle)continue;
        if(slot._opticsMode != otherSlot._opticsMode) continue;

        _visionPars[i]._visionMode = vPars._visionMode;
        _visionPars[i]._flirIndex = vPars._flirIndex;
      }

      return true;
    }
  }

  return false;
}

void WeaponsState::SimulateHeatFactors(int weaponType, bool simulateHeat, float deltaT)
{
  if (simulateHeat)
  {
    // Shot occurred, add some heat
    static float fireHeatDelta = 1.0f / 30.0f;

    if (weaponType & MaskSlotPrimary)   
    {
      _primaryHeatFactor += fireHeatDelta;
      saturateMin(_primaryHeatFactor, 1.0f);
    }

    if (weaponType & MaskSlotSecondary) 
    {
      _secondaryHeatFactor += fireHeatDelta;
      saturateMin(_secondaryHeatFactor, 1.0f);
    }

    if (weaponType & MaskSlotHandGun)   
    {
      _handGunHeatFactor += fireHeatDelta;
      saturateMin(_handGunHeatFactor, 1.0f);
    }
  }
  else
  {
    // weapon cooling
    static float invTimeToCoolDown = 1.0f / (60.0f * 4.0f);

    float tmp = deltaT * invTimeToCoolDown;

    _primaryHeatFactor -= tmp;
    _secondaryHeatFactor -= tmp;
    _handGunHeatFactor -= tmp;

    saturateMax(_primaryHeatFactor, 0.0f);
    saturateMax(_secondaryHeatFactor, 0.0f);
    saturateMax(_handGunHeatFactor, 0.0f);
  }
}

bool WeaponsState::HasVisionModes() const
{
  if (_currentWeapon < 0 || _currentWeapon >= _magazineSlots.Size()) return false;

  const MagazineSlot &slot = _magazineSlots[_currentWeapon];

  if (slot._muzzle) return slot._muzzle->HasVisionModes(slot._opticsMode);
  return false;
}

int WeaponsState::VisionModesCount() const
{
  if (_currentWeapon < 0 || _currentWeapon >= _magazineSlots.Size()) return 0;

  const MagazineSlot &slot = _magazineSlots[_currentWeapon];

  if (slot._muzzle) return slot._muzzle->VisionModesCount(slot._opticsMode);
  return 0;
}

void WeaponsState::TestCounterMeasures(Shot *cm, int count)
{
  if(_currentWeapon>=0 && _currentWeapon<_magazineSlots.Size())
  {
    const WeaponType *type = _magazineSlots[_currentWeapon]._weapon;
    if(GRandGen.RandomValue() >= (pow(type->_cmImmunity,count)))
    {
      _timeToMissileLock = Glob.time + cm->GetTimeToLive();
    }
  }
}



void WeaponsState::RemoveBackpack()
{
  //  _backpack->ResetMoveOut(); // mark it is in landscape   
  _backpack = NULL;
}

void WeaponsState::GiveBackpack(EntityAI *backpack, EntityAI *man)
{
  if(backpack && !backpack->ToDelete())
  {
    _backpack = backpack;
    _backpack->MoveOut(man);
  }
}

bool FindDropPos(Matrix4 &transform);
void WeaponsState::DropBackpack( EntityAI *man)
{
  if(_backpack)
  {
    Vector3 dirXZ = (Vector3(man->FutureVisualState().Direction().X(),0,man->FutureVisualState().Direction().Z()));
    dirXZ.Normalize();
    Vector3 position = man->FutureVisualState().Position()+ 0.5f*dirXZ + VUp*0.5f;

    Matrix3 dir;
    dir.SetUpAndDirection(VUp, man->FutureVisualState().Direction());

    Matrix4 transform;
    transform.SetPosition(position);
    transform.SetOrientation(dir);

    _backpack->PlaceOnSurfaceUnderWater(transform); //we want weapons on the floor
    _backpack->SetTransform(transform);

    _backpack->Init(transform, true); // do not reset movement
    _backpack->OnEvent(EEInit);

   // if (_backpack->IsLocal()) 
    {
      _backpack->ResetMoveOut(); // mark it is in landscape
      GLOB_WORLD->AddSlowVehicle(_backpack);
      GLOB_WORLD->RemoveOutVehicle(_backpack);
      // we need other entities nearby to be notified of our presence
      _backpack->IsMoved();
      GWorld->OnEntityMovedFar(_backpack);
    }  
    if(GWorld->GetMode()  == GModeNetware)
    {      
      GetNetworkManager().DropBackpack(_backpack, transform);
    }
    _backpack = NULL;
  }
}

void WeaponsState::TakeBackpack(EntityAI *backpack, Person *man)
{
  if(backpack && backpack->GetType()->IsBackpack())
  {
    if(!_backpack && backpack)
    {
      if(man->IsLocal())
      {
        backpack->UnlockPosition();
        _backpack = backpack;
        backpack->MoveOut(man); // remove reference from the world
      } 
      else if(GWorld->GetMode()  == GModeNetware) GetNetworkManager().TakeBackpack(man, backpack);          
    }
    else if(backpack)
    {
      EntityAI *bagTmp = _backpack; 

      if(man->IsLocal())
      {
        backpack->UnlockPosition();
        _backpack = backpack;
        _backpack->MoveOut(man);  
      } 
      else if(GWorld->GetMode()  == GModeNetware) GetNetworkManager().TakeBackpack(man, backpack);   

      // if (FindDropPos(transform)) outPos = transform.Position();
      Vector3 dirXZ = (Vector3(man->FutureVisualState().Direction().X(),0,man->FutureVisualState().Direction().Z()));
      dirXZ.Normalize();
      Vector3 position = man->FutureVisualState().Position()+ 0.5f*dirXZ + VUp*0.5f;

      Matrix3 dir;
      dir.SetUpAndDirection(VUp, man->FutureVisualState().Direction());

      Matrix4 transform;
      transform.SetPosition(position);
      transform.SetOrientation(dir);
      FindDropPos(transform);

      bagTmp->PlaceOnSurfaceUnderWater(transform); //we want weapons on the floor
      bagTmp->SetTransform(transform);

      bagTmp->Init(transform, true); // do not reset movement
      bagTmp->OnEvent(EEInit);

     // if (bagTmp->IsLocal()) 
      {
        bagTmp->ResetMoveOut(); // mark it is in landscape
        GLOB_WORLD->AddSlowVehicle(bagTmp);
        GLOB_WORLD->RemoveOutVehicle(bagTmp);
        // we need other entities nearby to be notified of our presence
        _backpack->IsMoved();
        GWorld->OnEntityMovedFar(bagTmp);
      }  
      if(GWorld->GetMode()  == GModeNetware)
      {      
        GetNetworkManager().DropBackpack(bagTmp, transform);
      }
    }
  }
  if(GetBackpack() && man->FindWeaponType(MaskSlotSecondary)>=0)
  {
    // create container
    Ref<EntityAI> veh = GWorld->NewVehicleWithID("WeaponHolder");
    if (!veh) return;

    Vector3 pos = man->FutureVisualState().Position() + 0.5f * man->FutureVisualState().Direction() + VUp*0.5f;
    Matrix3 dir;
    dir.SetUpAndDirection(VUp, man->FutureVisualState().Direction());
    Matrix4 transform;
    transform.SetPosition(pos);
    transform.SetOrientation(dir);

    veh->PlaceOnSurfaceUnderWater(transform); //we want weapons on the floor
    veh->SetTransform(transform);
    veh->Init(transform, true);
    veh->OnEvent(EEInit);

    GWorld->AddSlowVehicle(veh);
    if (GWorld->GetMode() == GModeNetware)
      GetNetworkManager().CreateVehicle(veh, VLTSlowVehicle, "", -1);

    RefArray<WeaponType> &weapons = man->GetWeapons()._weapons;
    for(int i=0; i<weapons.Size(); i++)
    {
      if((weapons[i]->_weaponType  & MaskSlotSecondary) !=0)
      {
        Ref<Action> action = new ActionWeapon(ATPutWeapon, veh, weapons[i]->GetName(), false);
        //action->SetFromGUI(true);
        //action->Process(unit);
        ResourceSupply *rs = veh->GetSupply();
        rs->Supply(man, action);
        rs->PutWeapon(veh);
      }
    }
  }
}

const AmmoType *WeaponsState::GetAmmoType(int weapon) const
{
  if (weapon < 0 || weapon >= _magazineSlots.Size())
    return NULL;
  const MagazineSlot &slot = _magazineSlots[weapon];
  const Magazine *magazine = slot._magazine;
  if (!magazine)
    return NULL;
  const MagazineType *type = magazine->_type;
  if (!type)
    return NULL;
  return type->_ammo;
}

bool WeaponsState::IsThrowable(int weapon) const
{
  if (weapon < 0 || weapon >= _magazineSlots.Size())
    return false;
  const MagazineSlot &slot = _magazineSlots[weapon];
  const Magazine *magazine = slot._magazine;
  if (!magazine)
    return false;
  const MagazineType *type = magazine->_type;
  if (!type)
    return false;
  const AmmoType* ammoType = type->_ammo;
  if (!ammoType)
    return false;


  // TODO - beter detection! (add throwable param to magazine config)
  switch (ammoType->_simulation)
  {
  case AmmoShotShell:
  case AmmoShotSmoke:
  case AmmoShotIlluminating:
  case AmmoShotMarker:
    return type->_initSpeed<30;
  }
  return false;
}

float WeaponsState::GetMaxThrowHoldTime(int weapon)
{
  if (weapon < 0 || weapon >= _magazineSlots.Size())
    return 0.0f;
  const MagazineSlot &slot = _magazineSlots[weapon];
  const Magazine *magazine = slot._magazine;
  if (!magazine)
    return 0.0f;
  const MagazineType *type = magazine->_type;
  if (!type)
    return 0.0f;
  return type->_maxThrowHoldTime;
}

float WeaponsState::GetMinThrowIntensityCoef(int weapon)
{
  if (weapon < 0 || weapon >= _magazineSlots.Size())
    return 1.0f;
  const MagazineSlot &slot = _magazineSlots[weapon];
  const Magazine *magazine = slot._magazine;
  if (!magazine)
    return 1.0f;
  const MagazineType *type = magazine->_type;
  if (!type)
    return 1.0f;
  return type->_minThrowIntensityCoef;
}

float WeaponsState::GetMaxThrowIntensityCoef(int weapon)
{
  if (weapon < 0 || weapon >= _magazineSlots.Size())
    return 1.0f;
  const MagazineSlot &slot = _magazineSlots[weapon];
  const Magazine *magazine = slot._magazine;
  if (!magazine)
    return 1.0f;
  const MagazineType *type = magazine->_type;
  if (!type)
    return 1.0f;
  return type->_maxThrowIntensityCoef;
}

const WeaponModeType *WeaponsState::GetWeaponMode(int weapon) const
{
  if (weapon < 0 || weapon >= _magazineSlots.Size())
    return NULL;
  const MagazineSlot &slot = _magazineSlots[weapon];
  return slot._weaponMode;
}

int WeaponsState::ValidateWeapon(int weapon) const
{
  return (weapon >= 0 && weapon < _magazineSlots.Size()) ? weapon : -1;
}

int WeaponsState::AddWeapon(EntityAIFull *entity, RStringB name, bool reload, bool checkSelected)
{
  Ref<WeaponType> weapon = WeaponTypes.New(name);
  return AddWeapon(entity, weapon, reload, checkSelected);
}

int WeaponsState::AddWeapon(EntityAIFull *entity, WeaponType *weapon, bool reload, bool checkSelected)
{
  if (!CheckAccessCreate(*weapon->_parClass))
    return -1;
  // test whether the same weapon is already added before
  if (entity && !entity->IsDeadSet()) // dead soldiers can be used as weapon containers
  {
    for (int i=0; i<_weapons.Size(); i++)
    {
      if (_weapons[i]==weapon)
      {
        RptF("Duplicate weapon %s detected for %s", cc_cast(weapon->GetName()), entity->GetName());
        return -1;
      }
    }
  }

  if (weapon && weapon->_weaponType & MaskSlotPrimary) _primaryHeatFactor = 0.0f;
  if (weapon && weapon->_weaponType & MaskSlotSecondary) _secondaryHeatFactor = 0.0f;
  if (weapon && weapon->_weaponType & MaskSlotHandGun) _handGunHeatFactor = 0.0f;

  // add weapon
  weapon->ShapeAddRef();
  int index = _weapons.Add(weapon);

  // add magazine slots
  for (int j=0; j<weapon->_muzzles.Size(); j++)
  {
    MuzzleType *muzzle = weapon->_muzzles[j];

    Magazine *magazine = NULL;
    if (reload)
    {
      int index = FindMagazineByType(muzzle);
      if (index >= 0) magazine = _magazines[index];
    }
    for (int mode=0; mode<muzzle->_modes.Size(); mode++)
    {
      int slot = _magazineSlots.Add();
      _magazineSlots[slot]._weapon = weapon;
      _magazineSlots[slot]._muzzle = muzzle;
      _magazineSlots[slot]._weaponMode = muzzle->_modes[mode];
      _magazineSlots[slot]._magazine = magazine;

      _magazineSlots[slot].SelectOpticsMode();
      
      _visionPars.Access(slot);
      // init vision pars
      VisionModePars &vPars = _visionPars[slot];
      if (muzzle)
      {
        muzzle->GetInitVisionMode(vPars._visionMode, vPars._flirIndex, _magazineSlots[slot]._opticsMode);
      }
      else
      {
        vPars._flirIndex = 0;
        vPars._visionMode = VMNormal;
      }
    }
  }

  for (int i = 0; i < _magazineSlots.Size(); i++)
  {
    MagazineSlot &slot = _magazineSlots[i];
    if(slot._muzzle)
    {
      slot._discreteIndex = slot._muzzle->_opticsInfo[slot._opticsMode]._discreteInitIndex;
      slot._discreteDistanceIndex  = slot._muzzle->_opticsInfo[slot._opticsMode]._discreteDistanceInitIndex;
    }
    else
    {
      slot._discreteIndex = 0;
      slot._discreteDistanceIndex = 0;
    }
  }

  if (checkSelected) entity->OnWeaponAdded();
#if _VBS3
  if(entity->IsLocal())
    entity->OnEvent(EELoadOutChanged);
#endif
  return index;
}

void WeaponsState::RemoveWeapon(EntityAIFull *entity, RStringB name, bool checkSelected)
{
  // find and remove weapon
  Ref<const WeaponType> weapon;
  for (int i=0; i<_weapons.Size(); i++)
  {
    if (stricmp(_weapons[i]->GetName(), name) == 0)
    {
      weapon = _weapons[i];
      // deinit muzzles
      unconst_cast(weapon.GetRef())->ShapeRelease();
      // remove weapon
      _weapons.Delete(i);
      break;
    }
  }
  if (!weapon)
    return;
  // remove attached magazine slots
  for (int i=0; i<_magazineSlots.Size();)
  {
    if (_magazineSlots[i]._weapon == weapon)
      _magazineSlots.Delete(i);
    else
      i++;
  }
  if (checkSelected)
  {
    entity->OnWeaponRemoved();
    // TODO: move to OnWeaponRemoved
    if (_weapons.Size() > 0)
      SelectWeapon(entity, FirstWeapon(entity), true);
    else
      SelectWeapon(entity, -1, true);
    if (GWorld->FocusOn() && GWorld->FocusOn()->GetVehicle() == entity)
      GWorld->UI()->ResetVehicle(entity);
#if _VBS3
    entity->OnEvent(EELoadOutChanged);
#endif
  }
}

void WeaponsState::RemoveWeapon(EntityAIFull *entity, const WeaponType *weapon, bool checkSelected)
{
  // find and remove weapon
  bool found = false;
  for (int i=0; i<_weapons.Size(); i++)
  {
    if (_weapons[i] == weapon)
    {
      found = true;
      // deinit muzzle
      unconst_cast(weapon)->ShapeRelease();

      _weapons.Delete(i);
      break;
    }
  }
  if (!found)
    return;
  // remove attached magazine slots
  for (int i=0; i<_magazineSlots.Size();)
  {
    if (_magazineSlots[i]._weapon == weapon)
      _magazineSlots.Delete(i);
    else
      i++;
  }
  if (checkSelected)
  {
    entity->OnWeaponRemoved();
    if (_weapons.Size() > 0)
      SelectWeapon(entity, FirstWeapon(entity), true);
    else
      SelectWeapon(entity, -1, true);
    if (GWorld->FocusOn() && GWorld->FocusOn()->GetVehicle() == entity)
      GWorld->UI()->ResetVehicle(entity);
  }
#if _VBS3
  entity->OnEvent(EELoadOutChanged);
#endif
}

void WeaponsState::RemoveAllWeapons(EntityAIFull *entity, bool removeItems)
{
  if (removeItems)
  {
    for (int i=0; i<_weapons.Size(); i++)
      _weapons[i]->ShapeRelease();
    _weapons.Clear();
  }
  else
  {
    // do not remove special items
    for (int i=_weapons.Size()-1; i>=0; i--)
    {
      if (_weapons[i] && (_weapons[i]->_weaponType & MaskSlotInventory) != 0)
        continue;
      _weapons[i]->ShapeRelease();
      _weapons.DeleteAt(i);
    }
  }
  _magazineSlots.Clear();
  entity->OnWeaponRemoved();
  SelectWeapon(entity, -1, true);
  _fire._gunner = NULL;
  _fire._weapon = -1;
  _forceFireWeapon = -1;
  if (GWorld->FocusOn() && GWorld->FocusOn()->GetVehicle() == entity)
    GWorld->UI()->ResetVehicle(entity);
#if _VBS3
  if(entity->IsLocal())
    entity->OnEvent(EELoadOutChanged);
#endif
}

void WeaponsState::RemoveAllItems(EntityAIFull *entity)
{
  for (int i=_weapons.Size()-1; i>=0; i--)
  {
    if (_weapons[i] && (_weapons[i]->_weaponType & MaskSlotInventory) == 0)
      continue;
    _weapons[i]->ShapeRelease();
    _weapons.DeleteAt(i);
  }
}

int WeaponsState::AddMagazine(EntityAIFull *entity, RStringB name)
{
  Ref<MagazineType> magazineType = MagazineTypes.New(name);
  if (magazineType.IsNull()) return -1;
  Ref<Magazine> magazine = new Magazine(magazineType);
  magazine->SetAmmo(magazineType->_maxAmmo);
  int index = AddMagazine(entity, magazine);
  if (index < 0)
    return index;

  magazine->_reload = 1;
  magazine->_reloadDuration = 0.8 + 0.2 * GRandGen.RandomValue();
  magazine->_reloadMagazine = 0;
  magazine->_reloadMagazineTotal = 0;
  return index;
}

int WeaponsState::AddMagazine(EntityAIFull *entity, Magazine *magazine, bool autoload)
{
  if (!CheckAccessCreate(*magazine->_type->_parClass))
    return -1;

  int index = _magazines.Add(magazine);
  if (autoload)
  {
    for (int weapon=0; weapon<_magazineSlots.Size(); weapon++)
    {
      const MagazineSlot &slot = _magazineSlots[weapon];
      const MuzzleType *muzzle = slot._muzzle;
      if (!muzzle) continue;
      // if slot is not empty, there is no need to reload
      if (slot._magazine && slot._magazine->GetAmmo()>0) continue;

      for (int k=0; k<muzzle->_magazines.Size(); k++)
        if (muzzle->_magazines[k] == magazine->_type)
        {
          entity->AutoReload(*this, weapon, true, false);
#if _VBS3
          entity->OnEvent(EELoadOutChanged);
#endif
          return index;
        }
    }   
  }
#if _VBS3
  entity->OnEvent(EELoadOutChanged);
#endif
  return index;
}

void WeaponsState::RemoveMagazine(EntityAIFull *entity, RStringB name)
{
  for (int i=0; i<_magazines.Size(); i++)
  {
    Ref<Magazine> magazine = _magazines[i];
    if (stricmp(magazine->_type->GetName(), name) == 0)
    {
      _magazines.Delete(i);
      for (int j=0; j<_magazineSlots.Size();)
      {
        if (_magazineSlots[j]._magazine == magazine)
          _magazineSlots[j]._magazine = NULL;
        else
          j++;
      }
#if _VBS3
      entity->OnEvent(EELoadOutChanged);
#endif
      return;
    }
  }
}

void WeaponsState::RemoveMagazine(EntityAIFull *entity, const Magazine *magazine)
{
  for (int i=0; i<_magazines.Size(); i++)
  {
    if (_magazines[i] == magazine)
    {
      _magazines.Delete(i);
      for (int j=0; j<_magazineSlots.Size();)
      {
        if (_magazineSlots[j]._magazine == magazine)
          _magazineSlots[j]._magazine = NULL;
        else
          j++;
      }
#if _VBS3
      entity->OnEvent(EELoadOutChanged);
#endif
      return;
    }
  }
}

void WeaponsState::RemoveMagazines(EntityAIFull *entity, RStringB name)
{
  for (int i=0; i<_magazines.Size();)
  {
    Ref<Magazine> magazine = _magazines[i];
    if (stricmp(magazine->_type->GetName(), name) == 0)
    {
      _magazines.Delete(i);
      for (int j=0; j<_magazineSlots.Size(); j++)
      {
        if (_magazineSlots[j]._magazine == magazine)
          _magazineSlots[j]._magazine = NULL;
      }
    }
    else
      i++;
  }
#if _VBS3
  entity->OnEvent(EELoadOutChanged);
#endif
}

void WeaponsState::RemoveAllMagazines(EntityAIFull *entity)
{
  _magazines.Clear();
  for (int i=0; i<_magazineSlots.Size(); i++)
    _magazineSlots[i]._magazine = NULL;
#if _VBS3
  entity->OnEvent(EELoadOutChanged);
#endif
}

void WeaponsState::SelectWeapon(EntityAIFull *entity, int weapon, bool changed)
{
  if (weapon >= _magazineSlots.Size())
  {
    Fail("Wrong weapon selection");
    return;
  }

  if (!changed && _currentWeapon == weapon)
    return;
  if (_currentWeapon >= 0 && _currentWeapon < _magazineSlots.Size())
  {
    // safety patch:
    // if new weapon is selected, cancel rest of the burst
    const MagazineSlot &slot = _magazineSlots[_currentWeapon];
    Magazine *magazine = slot._magazine;

    if (magazine)
      magazine->_burstLeft = 0;
  }

  _currentWeapon = weapon;

  // check if handgun or primary weapon was selected
  if (entity && _currentWeapon>=0 && _currentWeapon < _magazineSlots.Size())
  {
    const MagazineSlot &slot = _magazineSlots[_currentWeapon];
    WeaponType *weaponType = slot._weapon;
    if (weaponType)
    {
      int mask = weaponType->_weaponType;
      if (mask & MaskSlotPrimary)
        entity->SelectHandGun(false);
      else if (mask & MaskSlotHandGun)
        entity->SelectHandGun(true);
    }
    //some weapons needs a while to lock
    _timeToMissileLock = Glob.time + weaponType->_weaponLockDelay;
  }

}

void WeaponsState::SelectCounterMeasures(const EntityAIFull *entity, int counterMeasures, bool changed)
{
  if (counterMeasures >= _magazineSlots.Size())
  {
    Fail("Wrong weapon selection");
    return;
  }

  if (!changed && _currentCounterMeasures == counterMeasures)
    return;
  if (_currentCounterMeasures >= 0 && _currentCounterMeasures < _magazineSlots.Size())
  {
    // safety patch:
    // if new weapon is selected, cancel rest of the burst
    const MagazineSlot &slot = _magazineSlots[_currentCounterMeasures];
    Magazine *magazine = slot._magazine;

    if (magazine)
      magazine->_burstLeft = 0;
  }

  _currentCounterMeasures = counterMeasures;
}

/*!
\param muzzle reloaded muzzle
\param oldMagazineType preferred magazine type
\return index of magazine if found, otherwise -1
*/
int WeaponsState::FindMagazineByType(const MuzzleType *muzzle, const MagazineType *oldMagazineType)
{
  // search for reserve magazine
  // first search the same type as old magazine
  if (oldMagazineType)
  {
    int ammoMax = 0;
    int jBest = -1;
    for (int j=0; j<_magazines.Size(); j++)
    {
      Magazine *magazine = _magazines[j];
      if (magazine && magazine->_type == oldMagazineType && magazine->GetAmmo() > ammoMax)
      {
        ammoMax = magazine->GetAmmo();
        jBest = j;
      }
    }
    if (jBest >= 0)
      return jBest;
  }
  // then other magazines fits into muzzle
  for (int i=0; i<muzzle->_magazines.Size(); i++)
  {
    const MagazineType *type = muzzle->_magazines[i];
    if (type == oldMagazineType) continue;
    int ammoMax = 0;
    int jBest = -1;
    for (int j=0; j<_magazines.Size(); j++)
    {
      Magazine *magazine = _magazines[j];
      if (magazine && magazine->_type == type && magazine->GetAmmo() > ammoMax)
      {
        ammoMax = magazine->GetAmmo();
        jBest = j;
      }
    }
    if (jBest >= 0)
      return jBest;
  }
  return -1;
}

int WeaponsState::FirstWeapon(const EntityAIFull *entity) const
{
  int maxPrimary = MaxPrimaryLevel();
  // check max. primary level available
  // try to find primary weapon with max level
  for (int i=0; i<_magazineSlots.Size(); i++)
  {
    const MagazineSlot &slot = _magazineSlots[i];
    if (slot._muzzle->_primary<maxPrimary)
      continue;
    if (!slot.ShowToPlayer())
      continue;
    if (slot._weapon->GetSimulation() == WSCMLauncher)
      continue;
    if (!slot._muzzle->_showEmpty && EmptySlot(slot))
      continue;
    if (entity->IsHandGunSelected())
    {
      if (slot._weapon->_weaponType & MaskSlotHandGun)
        return i;
    }
    else
    {
      if (slot._weapon->_weaponType & MaskSlotPrimary)
        return i;
    }
  }
  // find any weapon with max level
  for (int i=0; i<_magazineSlots.Size(); i++)
  {
    const MagazineSlot &slot = _magazineSlots[i];
    if (slot._muzzle->_primary<maxPrimary)
      continue;
    if (!slot.ShowToPlayer())
      continue;
    if (slot._weapon->GetSimulation() == WSCMLauncher)
      continue;
    if (!slot._muzzle->_showEmpty && EmptySlot(slot))
      continue;
    if (entity->IsHandGunSelected())
    {
      // ignore primary weapon
      if (slot._weapon->_weaponType & MaskSlotPrimary)
        continue;
    }
    else
    {
      // ignore handgun
      if (slot._weapon->_weaponType & MaskSlotHandGun)
        continue;
    }
    return i;
  }
  return -1;
}

static void InitZoom(CameraType camTypeAct, float &zoom, Object* object, const ViewPars *viewPars)
{
  float dummyHead=0, dummyDive=0;
  if (viewPars)
    viewPars->InitVirtual(camTypeAct, dummyHead, dummyDive, zoom);
  else 
    object->InitVirtual(camTypeAct, dummyHead, dummyDive, zoom);
}


int WeaponsState::NextWeapon(const EntityAIFull *entity, int weapon) const
{
  // check max. primary level
  int maxPrimary = MaxPrimaryLevel();

  if (weapon >= 0)
  {
    const MagazineSlot &slot = _magazineSlots[weapon];
    if (slot._muzzle->_primary < maxPrimary)
      return FirstWeapon(entity);
  }

  int n = _magazineSlots.Size();

  for (int i=0; i<n; i++)
  {
    weapon++;
    if (weapon > n - 1) weapon = 0;
    const MagazineSlot &slot = _magazineSlots[weapon];
    if (slot._muzzle->_primary < maxPrimary)
      continue;
    if (!slot.ShowToPlayer())
      continue;
    if (slot._weapon->GetSimulation() == WSCMLauncher)
      continue;
    if (!slot._muzzle->_showEmpty && EmptySlot(slot))
      continue;
    if (entity->IsHandGunSelected())
    {
      // ignore primary weapon
      if (slot._weapon->_weaponType & MaskSlotPrimary)
        continue;
    }
    else
    {
      // ignore handgun
      if (slot._weapon->_weaponType & MaskSlotHandGun)
        continue;
    }

    return weapon;
  }
  return -1;
}

int WeaponsState::PrevWeapon(const EntityAIFull *entity, int weapon) const
{
  int maxPrimary = MaxPrimaryLevel();
  if (weapon >= 0)
  {
    const MagazineSlot &slot = _magazineSlots[weapon];
    if (slot._muzzle->_primary < maxPrimary)
      return FirstWeapon(entity);
  }

  int n = _magazineSlots.Size();
  for (int i=0; i<n; i++)
  {
    weapon--;
    if (weapon < 0) weapon = n - 1;
    const MagazineSlot &slot = _magazineSlots[weapon];
    if (slot._muzzle->_primary < maxPrimary)
      continue;
    if (!slot.ShowToPlayer())
      continue;
    if (slot._weapon->GetSimulation() == WSCMLauncher)
      continue;
    if (!slot._muzzle->_showEmpty && EmptySlot(slot))
      continue;
    if (entity->IsHandGunSelected())
    {
      // ignore primary weapon
      if (slot._weapon->_weaponType & MaskSlotPrimary)
        continue;
    }
    else
    {
      // ignore handgun
      if (slot._weapon->_weaponType & MaskSlotHandGun)
        continue;
    }
    return weapon;
  }
  return -1;
}

void WeaponsState::NextOpticsMode(Person *person)
{
  if (_currentWeapon >= 0 && _currentWeapon < _magazineSlots.Size())
  {
    if(_magazineSlots[_currentWeapon]._muzzle->_opticsInfo.Size() <= 1)
      return;

    MagazineSlot &slot = _magazineSlots[_currentWeapon];   

    _magazineSlots[_currentWeapon]._opticsMode = ((_magazineSlots[_currentWeapon]._opticsMode + 1) % _magazineSlots[_currentWeapon]._muzzle->_opticsInfo.Size());
    const OpticsInfo &opticsInfo = slot._muzzle->_opticsInfo[slot._opticsMode];

    //reset fov
    slot._discreteIndex = opticsInfo._discreteInitIndex;
    slot._discreteDistanceIndex = opticsInfo._discreteDistanceInitIndex;
    //reset vision mode, if needed
    if (slot._muzzle->HasVisionModes(slot._opticsMode))
    {
      bool allowed = false;
      VisionModePars &vPars = _visionPars[_currentWeapon];
      //check if current vision mode is enabled
      for (int i = 0; i < opticsInfo._visionModes.Size(); i++)
      {
        if (opticsInfo._visionModes[i] == vPars._visionMode) 
        { //check flir mode
          if(vPars._visionMode == VMTi)
          {
            for (int j = 0; j < opticsInfo._thermalModes.Size(); j++)
            {
              if (opticsInfo._thermalModes[j] == vPars._flirIndex) 
              { 
                allowed = true;
                break;
              }
            }
          }
          else allowed = true;
        }
      }
      if(!allowed)
      {
        slot._muzzle->GetInitVisionMode(vPars._visionMode, vPars._flirIndex, slot._opticsMode);
      }
    }

    float initFOV;
    CameraType camTypeAct = GWorld->GetCameraType();
    InitZoom(camTypeAct, initFOV, person, NULL);

    Object::Zoom *zoom = person->GetZoom();
    zoom->Init(initFOV);

    //copy selection to other slots, if possible
    for (int i=0; i<_magazineSlots.Size(); i++)
    {
      if(i == _currentWeapon) continue;
      const MagazineSlot &magslot = _magazineSlots[i];
      if(magslot._muzzle && magslot._muzzle ==  slot._muzzle)
      {      
        _magazineSlots[i]._opticsMode  = _magazineSlots[_currentWeapon]._opticsMode;
        _visionPars[i]._visionMode = _visionPars[_currentWeapon]._visionMode;
        _visionPars[i]._flirIndex = _visionPars[_currentWeapon]._flirIndex;
      }
    }
  }
}

int WeaponsState::FirstCM(const EntityAIFull *entity) 
{
  for (int i = 0; i < _magazineSlots.Size(); i++)      
  {      
    const WeaponType *type = _magazineSlots[i]._weapon;
    MagazineSlot mag = _magazineSlots[i];
    if (!mag.ShowToPlayer())
      continue;
    if (mag._magazine && type->GetSimulation() == WSCMLauncher)
    {
      SelectCounterMeasures(entity,i);
      return i;
    }
  }
  return -1;
}

int WeaponsState::NextCM(const EntityAIFull *entity)
{
  for (int i = _currentCounterMeasures +1; i < _magazineSlots.Size(); i++)      
  {      
    const WeaponType *type = _magazineSlots[i]._weapon;
    MagazineSlot mag = _magazineSlots[i];
    if (!mag.ShowToPlayer())
      continue;
    if (mag._magazine && type->GetSimulation() == WSCMLauncher)
    {
      SelectCounterMeasures(entity,i);
      return i;
    }
  }
  for (int i = 0; i < _currentCounterMeasures; i++)      
  {        
    const WeaponType *type = _magazineSlots[i]._weapon;
    MagazineSlot mag = _magazineSlots[i];
    if (!mag.ShowToPlayer())
      continue;
    if (mag._magazine && type->GetSimulation() == WSCMLauncher)
    {      
      SelectCounterMeasures(entity,i);
      return i;
    }
  }
  return _currentCounterMeasures;
}

bool WeaponsState::ShowFire() const
{
  return Glob.frameID <= _mGunFireUntilFrame || Glob.time < _mGunFireTime + 0.05f;
}

bool WeaponsState::EmptySlot(const MagazineSlot &slot) const
{
  const Magazine *magazine = slot._magazine;
  if (!magazine)
    return true;
  if (magazine->GetAmmo() > 0)
    return false;
  const MagazineType *type = magazine->_type;
  if (!type)
    return true;
  if (!type->_ammo)
    return false;

  for (int i=0; i<_magazines.Size(); i++)
  {
    const Magazine *reserve = _magazines[i];
    if (!reserve || reserve == magazine)
      continue;
    if (reserve->_type != magazine->_type)
      continue;
    if (reserve->GetAmmo() > 0)
      return false;
  }
  return true;
}

int WeaponsState::MaxPrimaryLevel() const
{
  int maxPrimary = 0;
  for (int i=0; i<_magazineSlots.Size(); i++)
  {
    const MagazineSlot &slot = _magazineSlots[i];
    if (!slot.ShowToPlayer())
      continue;
    if (slot._weapon->GetSimulation() == WSCMLauncher)
      continue;
    if (!slot._muzzle->_showEmpty && EmptySlot(slot))
      continue;
    if (!slot._magazine)
      continue;
    if (slot._magazine->GetAmmo()<=0)
      continue;
    saturateMax(maxPrimary,slot._muzzle->_primary);
  }
  if (maxPrimary>0) return maxPrimary;
  // check max. primary level of all weapons
  for (int i=0; i<_magazineSlots.Size(); i++)
  {
    const MagazineSlot &slot = _magazineSlots[i];
    if (!slot.ShowToPlayer())
      continue;
    if (slot._weapon->GetSimulation() == WSCMLauncher)
      continue;
    if (!slot._muzzle->_showEmpty && EmptySlot(slot))
      continue;
    saturateMax(maxPrimary,slot._muzzle->_primary);
  }
  return maxPrimary;
}

/*!
\param type given magazine type
\param ammo minimal amount of ammo magazine must contain
\return index of magazine in array or -1 if not found
*/
int WeaponsState::FindBestMagazine(const MagazineType *type, int ammo) const
{
  int best = -1;
  for (int j=0; j<_magazines.Size(); j++)
  {
    const Magazine *reserve = _magazines[j];
    if (!reserve)
      continue;
    if (reserve->_type != type)
      continue;
    if (reserve->GetAmmo() > ammo)
    {
      ammo = reserve->GetAmmo();
      best = j;
    }
  }
  return best;
}

LSError WeaponsState::Serialize(EntityAIFull *owner, ParamArchive &ar)
{
  if (!IS_UNIT_STATUS_BRANCH(ar.GetArVersion()))
  {
    CHECK(ar.SerializeRef("fireGunner", _fire._gunner, 1))
    CHECK(ar.Serialize("fireMode", _fire._weapon, 1, -1))
    CHECK(ar.Serialize("firePrepareOnly", _fire._firePrepareOnly, 1, true))
    CHECK(ar.Serialize("fireCommanded", _fire._fireCommanded, 1, false))
    CHECK(ar.SerializeRef("fireTarget", _fire._fireTarget, 1))
    CHECK(ar.SerializeEnum("fireInitState", _fire._initState, 1, TargetAlive))
    CHECK(ar.Serialize("forceFireWeapon", _forceFireWeapon, 1, -1))
    CHECK(ar.SerializeDef("gunFire", _gunFire, 1))
    CHECK(ar.SerializeDef("gunClouds", _gunClouds, 1))
    CHECK(ar.SerializeDef("mGunFire", _mGunFire, 1))
    CHECK(ar.SerializeDef("mGunClouds", _mGunClouds, 1))
    CHECK(ar.Serialize("mGunFireUntilFrame", _mGunFireUntilFrame, 1, 0))
    CHECK(::Serialize(ar,"mGunFireTime", _mGunFireTime, 1, TIME_MIN))
    CHECK(ar.Serialize("mGunFirePhase", _mGunFirePhase, 1, 0))

    CHECK(::Serialize(ar, "lastShotTime", _lastShotTime, 1, Time(0)))
    CHECK(ar.SerializeRef("lastShot", _lastShot, 1))
  }

  // serialize weapons
  if (ar.GetArVersion() >= 7)
  {
    if (ar.IsLoading() && ar.GetPass() == ParamArchive::PassFirst)
    {
      RemoveAllWeapons(owner, true);
      RemoveAllMagazines(owner);
    }
    CHECK(ar.Serialize("Weapons", _weapons, 1));
    if (!ar.IsSaving() && ar.GetPass() == ParamArchive::PassFirst)
    {
      // remove NULLs and finish WeaponType serialization
      for (int i=0; i<_weapons.Size();)
      {
        if (_weapons[i])
        {
          _weapons[i]->ShapeAddRef();
          i++;
        }
        else _weapons.Delete(i);
      }
    }
    CHECK(ar.Serialize("Magazines", _magazines, 1));
    if (ar.IsSaving())
    {
      AutoArray<int> slots;
      int n = _magazineSlots.Size();
      slots.Resize(n);
      for (int i=0; i<n; i++)
      {
        slots[i] = -1;
        Magazine *magazine = _magazineSlots[i]._magazine;
        if (magazine)
        {
          for (int j=0; j<_magazines.Size(); j++)
          {
            if (_magazines[j] == magazine)
            {
              slots[i] = j;
              break;
            }
          }
        }
      }
      CHECK(ar.SerializeArray("magazineSlots", slots, 1));
      if (!IS_UNIT_STATUS_BRANCH(ar.GetArVersion()))
      {
        CHECK(ar.Serialize("currentWeapon", _currentWeapon, 1, -1))
        CHECK(ar.Serialize("currentCounterMeasures", _currentCounterMeasures, 1, -1))   
      }
    }
    else if (ar.GetPass() == ParamArchive::PassSecond)
    {
      int n = 0;
      for (int i=0; i<_weapons.Size(); i++)
      {
        WeaponType *weapon = _weapons[i];
        for (int j=0; j<weapon->_muzzles.Size(); j++)
        {
          MuzzleType *muzzle = weapon->_muzzles[j];
          n += muzzle->_modes.Size();
        }
      }
      _magazineSlots.Resize(n);
      int slot = 0;
      for (int i=0; i<_weapons.Size(); i++)
      {
        WeaponType *weapon = _weapons[i];
        for (int j=0; j<weapon->_muzzles.Size(); j++)
        {
          MuzzleType *muzzle = weapon->_muzzles[j];
          Magazine *magazine = NULL;
          for (int mode=0; mode<muzzle->_modes.Size(); mode++)
          {
            _magazineSlots[slot]._weapon = weapon;
            _magazineSlots[slot]._muzzle = muzzle;
            _magazineSlots[slot]._weaponMode = muzzle->_modes[mode];
            _magazineSlots[slot]._magazine = magazine;
            _magazineSlots[slot]._reloadMagazineProgress = 0;

            slot++;
          }
        }
      }
      AutoArray<int> slots;
      ar.FirstPass();
      CHECK(ar.SerializeArray("magazineSlots", slots, 1));
      Assert(slots.Size() == n);
      for (int i=0; i<n; i++)
      {
        MagazineSlot &slot = _magazineSlots[i];
        int index = slots[i];
        if (index < 0 || index >= _magazines.Size())
          continue;
        Magazine *magazine = _magazines[index];
        if (!magazine || !magazine->_type)
          continue;
        if (!slot._muzzle->CanUse(magazine->_type))
        {
          RptF("Cannot use magazine %s in muzzle %s",
            (const char *)magazine->_type->GetName(),
            (const char *)slot._muzzle->GetName());
          continue;
        }
        slot._magazine = magazine;
      }
      // must be after RemoveAllWeapons
      if (!IS_UNIT_STATUS_BRANCH(ar.GetArVersion()))
      {
        int currentWeapon;
        CHECK(ar.Serialize("currentWeapon", currentWeapon, 1, -1))
        SelectWeapon(owner, currentWeapon, true);
       
        int currentCounterMeasures;
        CHECK(ar.Serialize("currentCounterMeasures", currentCounterMeasures, 1, -1))
        SelectCounterMeasures(owner, currentCounterMeasures, true);
      }
      ar.SecondPass();

      if (n > 0 && _currentWeapon < 0) FirstWeapon(owner);
      /* Call to owner-AutoReloadAll removed, as we suppose it is a relict trying to ensure AI have their guns reloaded after Load.
         It is now obsolete as animations are serialized. (see news:gu11b2$hih$1@new-server.localdomain)
      */
      // owner->AutoReloadAll(*this, false);
    }

    CHECK(ar.Serialize("visionPars", _visionPars, 1))

    CHECK(ar.Serialize("magazineSlotsOpticsModes", _magazineSlots, 1))
    if (!IS_UNIT_STATUS_BRANCH(ar.GetArVersion()))
    {
      CHECK(ar.SerializeRef("backpack", _backpack, 1))
    }
    else
    {
      if(ar.IsSaving())
      {
        Ref<Entity> backpack = _backpack.GetRef();
        CHECK(ar.Serialize("backpack",backpack,1));
      }
      else 
      {
        Ref<Entity> backpack = NULL;
        if(_backpack && ar.GetPass() == ParamArchive::PassFirst)
        {
          _backpack->SetDelete();
          _backpack = NULL;
        }
        else backpack = _backpack.GetRef();

        CHECK(ar.Serialize("backpack",backpack,1));
        _backpack = dyn_cast<EntityAI>((Entity *)backpack);

       if(_backpack && ar.GetPass() == ParamArchive::PassSecond)
       { 
         _backpack->SetMoveOutDone(owner);
         GWorld->AddOutVehicle(_backpack);
         _backpack->SetAseembleTo(NULL);
       }
      }
    }

    CHECK(ar.Serialize("primaryHeatFactor", _primaryHeatFactor, 1, 0.0f))
    CHECK(ar.Serialize("secondaryHeatFactor", _secondaryHeatFactor, 1, 0.0f))
    CHECK(ar.Serialize("handGunHeatFactor", _handGunHeatFactor, 1, 0.0f))
  }

  return LSOK;
}

/*!
\patch 1.12 Date 08/06/2001 by Jirka
- Fixed: Initial state of ammunition was bad on gunner's computer
\patch 1.12 Date 08/08/2001 by Jirka
- Fixed: Ammo reload by gunner in MP (sabot / heat switching)
*/
TMError WeaponsState::TransferMsg(NetworkMessageContext &ctx, EntityAIFull *entity, Person *gunner)
{
  PREPARE_TRANSFER(UpdateEntityAIWeapons)

  TRANSF_EX(currentWeapon, _currentWeapon)
  TRANSF_EX(targetAimed,_targetAimed)
  TRANSF_EX(laserTargetOn,_laserTargetOn)
  // FIX
  bool initialUpdate;
  if (ctx.IsSending()) initialUpdate = ctx.GetInitialUpdate();
  TRANSF_EX(initialUpdate, initialUpdate)
    if (!ctx.IsSending() && GetNetworkManager().GetClientState()<NCSGameLoaded) initialUpdate = true;
#if _VBS3
  if(!ctx.IsSending())
  {
    int difference = abs(_magazines.Size()-message->_magazines.Size());

    //TODO: Chad
    //what is the point in firing the event that often? (Mark)
    while(difference > 0)
    {
      entity->OnEvent(EELoadOutChanged);
      --difference;
    }          
  }
#endif   

  // magazines
  if (ctx.IsSending() || initialUpdate || !(gunner && gunner->IsLocal()))
  {
    TRANSF_OBJ_ARRAY_EX(magazines, _magazines)
  }
  else
  {
    int n = message->_magazines.Size();
    RefArray<Magazine> oldMagazines = _magazines;
    int oldN = oldMagazines.Size();
    _magazines.Resize(n);
    for (int i=0; i<n; i++)
    {
      NetworkMessageMagazine *magazine = (NetworkMessageMagazine *)message->_magazines[i].GetRef();
      int creator = magazine->_creator;
      int id = magazine->_id;
      int found = -1;
      if (i < oldN && oldMagazines[i]->_creator == creator && oldMagazines[i]->_id == id)
      {
        found = i;
      }
      else for (int j=0; j<oldN; j++)
      {
        if (j != i && oldMagazines[j]->_creator == creator && oldMagazines[j]->_id == id)
        {
          found = j;
          break;
        }
      }
      if (found >= 0)
      {
        if (stricmp(oldMagazines[found]->_type->GetName(), magazine->_type) != 0)
          Fail("Magazines with equal ids and different type");
        _magazines[i] = oldMagazines[found];
      }
      else
      {
        Ref<MagazineType> type = MagazineTypes.New(magazine->_type);
        if (type.IsNull())
        {
          _magazines[i] = NULL;
          continue;
        }
        _magazines[i] = new Magazine(type);
        _magazines[i]->_creator = creator;
        _magazines[i]->_id = id;
        _magazines[i]->_reload = magazine->_reload;
        _magazines[i]->_reloadDuration = magazine->_reloadDuration;
        _magazines[i]->_reloadMagazine = magazine->_reloadMagazine;
        _magazines[i]->_reloadMagazineTotal = magazine->_reloadMagazineTotal;
      }
      _magazines[i]->SetAmmo(magazine->_ammo);
      _magazines[i]->_burstLeft = magazine->_burstLeft;
    }    
    _magazines.RemoveNulls();
  }
  if (ctx.IsSending())
  {
    TRANSF_REF(backpack)
    // weapons
    AUTO_STATIC_ARRAY(RString, weapons, 32);
    int n = _weapons.Size();
    weapons.Reserve(n, n);
    weapons.Resize(n);
    for (int i=0; i<n; i++)
      weapons[i] = _weapons[i]->GetName();
    TRANSF_EX(weapons, weapons)
    // magazine slots
    AUTO_STATIC_ARRAY(int, slots, 32);
    n = _magazineSlots.Size();
    slots.Reserve(n, n);
    slots.Resize(n);
    for (int i=0; i<n; i++)
    {
      slots[i] = -1;
      Magazine *magazine = _magazineSlots[i]._magazine;
      if (magazine)
      {
        for (int j=0; j<_magazines.Size(); j++)
        {
          if (_magazines[j] == magazine)
          {
            slots[i] = j;
            break;
          }
        }
      }
    }
    TRANSF_EX(magazineSlots, slots)
  }
  else
  {
    Ref<EntityAI> bag;
    TRANSF_REF_EX(backpack,bag);

    if(bag != _backpack)
    {        
      _backpack = bag;
      if(_backpack)
      {
        _backpack->UnlockPosition();
        _backpack->MoveOut(entity);
      }
    }

    // weapons
    AUTO_STATIC_ARRAY(RString, weapons, 32);
    TRANSF_EX(weapons, weapons)
    bool changed = weapons.Size() != _weapons.Size();
    if (!changed) for (int i=0; i<_weapons.Size(); i++)
      if (weapons[i] != _weapons[i]->GetName())
      {
        changed = true;
        break;
      }
      if (changed)
      {
        RemoveAllWeapons(entity, true);
        for (int i=0; i<weapons.Size(); i++)
          AddWeapon(entity, weapons[i]);
#if _VBS3 
        //TODO: Chad
        //AddWeapon already fires the event for local units?!
        entity->OnEvent(EELoadOutChanged);
#endif
      }
      // magazine slots
      // FIX
      if (initialUpdate || !(gunner && gunner->IsLocal()))
      {
        AUTO_STATIC_ARRAY(int, slots, 32);
        TRANSF_EX(magazineSlots, slots)
        int n = _magazineSlots.Size();

        // FIX
        int m = slots.Size();
        DoAssert(m == n);
        for (int i=0; i<n; i++)
        {
          MagazineSlot &slot = _magazineSlots[i];
          slot._magazine = NULL;
          if (i >= m)
            continue;
          int index = slots[i];
          if (index < 0 || index >= _magazines.Size())
            continue;
          Magazine *magazine = _magazines[index];
          if (!magazine || !magazine->_type)
            continue;
          if (!slot._muzzle->CanUse(magazine->_type))
          {
            RptF("Cannot use magazine %s in muzzle %s",
              (const char *)magazine->_type->GetName(),
              (const char *)slot._muzzle->GetName());
            continue;
          }
          slot._magazine = magazine;
          slot._reloadMagazineProgress = 0;
        }
      }

      if (_currentWeapon < 0 && GWorld->FocusOn() &&
        GWorld->FocusOn()->GetVehicle() == entity && GWorld->FocusOn()->GetPerson() == gunner)
        GWorld->UI()->ResetVehicle(entity);

      //update dead bodies and briefing gear
      void UpdateWeaponsInBriefing(EntityAI *owner);
      UpdateWeaponsInBriefing(entity);

  }
  return TMOK;
}

float WeaponsState::CalculateError(NetworkMessageContextWithError &ctx)
{
  PREPARE_TRANSFER(UpdateEntityAIWeapons)

  float error = 0;
  if (message->_currentWeapon != _currentWeapon) error += ERR_COEF_MODE;

  // weapons
  bool changed = message->_weapons.Size() != _weapons.Size();
  if (!changed)
  {
    for (int i=0; i<_weapons.Size(); i++)
      if (stricmp(message->_weapons[i], _weapons[i]->GetName()) != 0)
      {
        changed = true;
        break;
      }
  }
  if (changed) error += ERR_COEF_MODE;

  // magazines
  changed = message->_magazines.Size() != _magazines.Size();
  int ammoDiff = 0;
  if (!changed)
  {
    for (int i=0; i<_magazines.Size(); i++)
    {
      NetworkMessageMagazine *mag = (NetworkMessageMagazine *)(message->_magazines[i].GetRef());
      if (mag->_creator != _magazines[i]->_creator || mag->_id != _magazines[i]->_id)
      {
        changed = true;
        break;
      }
      else
        ammoDiff += abs(mag->_ammo - _magazines[i]->GetAmmo());
    }
  }
  if (changed)
    error += ERR_COEF_MODE;
  else
    error += ammoDiff * ERR_COEF_VALUE_MINOR;

  // magazine slots
  changed = message->_magazineSlots.Size() != _magazineSlots.Size();
  if (!changed)
  {
    for (int i=0; i<_magazineSlots.Size(); i++)
    {
      if (message->_magazineSlots[i] >= _magazines.Size())
      {
        changed = true;
        break;
      }
      else if (message->_magazineSlots[i] >= 0)
      {
        if (_magazineSlots[i]._magazine != _magazines[message->_magazineSlots[i]])
        {
          changed = true;
          break;
        }
      }
      else
      {
        if (_magazineSlots[i]._magazine != NULL)
        {
          changed = true;
          break;
        }
      }
    }
  }
  if (changed)
    error += ERR_COEF_MODE;

  ICALCERR_NEQREF_SOFT(EntityAI, backpack, ERR_COEF_MODE)


  return error;
}

void EntityAI::UpdateSnappedPosition()
{
  // update snappedPosition
  float gridDist = GetType()->GetTypSpeed() * 5.0f/3.6f; //distance using typical speed after 5 seconds
  saturate(gridDist, 20.0f, 500.0f);
  const float gridDist2 = gridDist*gridDist;
  if (FutureVisualState().Position().Distance2(_snappedPosition)>gridDist2)
  {
    _snappedPosition = (FutureVisualState().Position() + Vector3(gridDist/2, gridDist/2, gridDist/2));
    _snappedPosition = _snappedPosition - Vector3(fmod(_snappedPosition.X(),gridDist), fmod(_snappedPosition.Y(),gridDist), fmod(_snappedPosition.Z(),gridDist));
  }
}

void EntityAI::UpdateReflectors()
{ // TODO: this may be called from multiple attachments. Make sure we do it once per frame only
  // animate position
  _reflectors.AnimatePosition(this, GetType()->_reflectors);

  // switch on / off
  _reflectors.Switch(this, GetType()->_reflectors);

  // Update the reflectors after changes
  _reflectors.UpdateAggReflectors(this);
}

void EntityAI::UpdatePosition()
{
  UpdateReflectors();
}

// WIP:BALLISTICS_COMPUTER:BEGIN
bool BallisticsComputer::SetParams(const EntityAIFull *shooter, const TurretContext &context, Target *target)
{
  _shooter = shooter;
  _context = context;
  _target = target;
  if (!_shooter || !_target || !_context._turret)
    return false;
  const WeaponsState *weapons = _context._weapons;
  if (!weapons)
    return false;
  _weapon = weapons->_currentWeapon;
  if (_weapon < 0)
  {
    if (weapons->_magazineSlots.Size() <= 0)
      return false;
    _weapon = 0;
  }
  if (!_shooter->CheckTargetKnown(_context, _target))
    return false;
  const MagazineSlot &slot = weapons->_magazineSlots[_weapon];
  const MuzzleType *muzzle = slot._muzzle;
  if (!muzzle || muzzle->_ballisticsComputer != 1)
    return false;
  const Magazine *magazine = slot._magazine;
  if (!magazine)
    return false;
  _aInfo = magazine->_type;
  if (!_aInfo)
    return false;
  _ammo = _aInfo->_ammo;
  if (!_ammo)
    return false;
  return true;
}

bool BallisticsComputer::SimulateBallistics(Vector3Par relPos, Vector3Par relSpeed, const MagazineType *ammoInfo, float& time, Vector3& correction)
{
  // rotate so the shot is aligned with the X axis
  float angle = -atan2(relPos.Z(), relPos.X());
  Matrix3 aimRot(MRotationY, angle);
  Vector3 pos = aimRot * relPos;
  Vector3 speed = aimRot * relSpeed;
  Vector3 shotSpeed = pos.Normalized() * ammoInfo->_initSpeed;
  ///LogF("rotation by %.1f deg -> relPos = (%.2f, %.2f, %.2f), relSpeed = (%.2f, %.2f, %.2f), shotSpeed = (%.2f, %.2f, %.2f), ideal time = %.3f", angle * 180.0f / H_PI, pos.X(), pos.Y(), pos.Z(), speed.X(), speed.Y(), speed.Z(), shotSpeed.X(), shotSpeed.Y(), shotSpeed.Z(), pos.X() / shotSpeed.X());

  const float dist = pos.X();
  float posX = -pos.X();
  float posY = -pos.Y();
  float speedX = shotSpeed.X();
  float speedY = shotSpeed.Y();

  // just to be sure
  Assert (fabs(pos.Z()) < 1e-3);
  Assert (fabs(shotSpeed.Z()) < 1e-3);
  Assert((posX < 0.0f) && (speedX > 0.0f));

  const float relSpeedX = speed.X();
  const float relSpeedY = speed.Y();
  const float relSpeedZ = speed.Z();
  const AmmoType *_ammo = ammoInfo->_ammo;
  const float fCoef = _ammo->_airFriction;
  const float gAcc = _ammo->_coefGravity * G_CONST;
  // conditions for stopping the simulation (we assume at 1% of original velocity we are not interested at all)
  const float stopTime = _ammo->_timeToLive;
  const float stopSpeed = speedX * 0.01f;
  // maximum simulation time step
  // const float maxDeltaT = 0.01f; FIXME RHAL: use smaller delta for improved simulation (or use time step dependent on the distance and/or speed?)
  const float maxDeltaT = 0.1f;

  time = 0.0f;
  bool terminate = false;
  while(!terminate)
  {
    // should the simulation be stopped?
    if ((time > stopTime) || (speedX < stopSpeed))
    {  // target cannot be hit
      correction = Vector3(posX, posY, dist);
      return false;
    }
    // update time (note that delta should be exact in the last step)
    terminate = speedX * maxDeltaT > -posX;
    float deltaT = terminate ? -posX / speedX : maxDeltaT;
    time += deltaT;
    // update pos
    posX += (speedX - relSpeedX) * deltaT;
    posY += (speedY - relSpeedY) * deltaT;
    // get current friction
    float speed = hypot(speedX, speedY);
    // this is speed^2 * f * sin(a)
    float fAccX = speed * fCoef * speedX;
    // and this one speed^2 * f * cos(a)
    float fAccY = speed * fCoef * speedY;
    // update speed
    speedX += fAccX * deltaT;
    speedY += (fAccY - gAcc) * deltaT;
  }
  correction = Vector3(relSpeedZ * time, posY, dist);
  return true;
}

bool BallisticsComputer::Compute(const EntityAIFull *shooter, const TurretContext &context, Target *target, bool exact)
{
  if (!SetParams(shooter, context, target))
  { 
    _status = BCinvalid;
    return false;
  }

  // position of the shooter (in world coords)
  _weaponPos = _shooter->FutureVisualState().PositionModelToWorld(_shooter->GetWeaponPoint(_shooter->FutureVisualState(),_context, _weapon));
  // estimate target info
  _shooter->GetAimingPosition(_targetPos, _targetSpeed, _seenBefore, exact, _context, _target, _ammo);
  // simulate ballistics
  float time;
  Vector3 correction;
  bool canHit = SimulateBallistics(_targetPos - _weaponPos, _targetSpeed - _shooter->FutureVisualState().Speed(), _aInfo, time, correction);
#if 1
  if (canHit)
  { // on big angle differences, assume that we can't hit the target at all
    float tana = -correction.Y() / correction.Z();
    ///LogF("tana = %.2f, angle = %.0f", tana, atan2(-correction.Y(), correction.Z()) * 180.0f / H_PI);
    if (fabs(tana) > 0.1)
      canHit = false;
  }
#endif
  if (!canHit)
  { // target cannot be hit
    _status = BCready;
    ///LogF("BC: cannot hit!");
    return false;
  }
  _time = time;
  _targetCorrection = correction;
  _status = BCupdated;
  ///LogF("BC: can hit at %.3f sec with correction (%.3f, %.3f, %.3f) (angle %.1f deg)", _time, _targetCorrection.X(), _targetCorrection.Y(), _targetCorrection.Z(), atan2(_targetCorrection.Y(), _targetCorrection.Z()) * 180.0f / H_PI);
  return true;
}

Matrix3 BallisticsComputer::ViewCorrection() const
{
  if ((_status == BCinvalid) || (_status == BCready))
    return M3Identity;
  return Matrix3(MDirection, _targetCorrection, VUp);
}

// WIP:BALLISTICS_COMPUTER:END
