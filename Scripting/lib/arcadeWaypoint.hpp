#ifdef _MSC_VER
#pragma once
#endif

#ifndef _ARCADE_WAYPOINT_HPP
#define _ARCADE_WAYPOINT_HPP

//#include "wpch.hpp"
#include <Es/Strings/rString.hpp>
//#include "loadStream.hpp"
#include <El/Color/colors.hpp>
#include <El/ParamArchive/serializeClass.hpp>

#include "AI/pathPlanner.hpp"	// AI::Rank, AI::Formation, AI::Semaphore
#include "world.hpp"        // CamEffectPosition, TitEffectName

#include "objectId.hpp"

struct EFFECTS_INFO;
struct WAYPOINT_INFO;
struct MARKER_INFO;

enum TitleType
{
	TitleNone,
	TitleObject,
	TitleResource,
	TitleText
};

struct ArcadeEffects : public SerializeClass
{
	RString condition;

	RString sound;
	RString voice;
	RString soundEnv;
	RString soundDet;
	RString track;

	TitleType titleType;
	TitEffectName titleEffect;
	RString title;

	ArcadeEffects();
	ArcadeEffects(const ArcadeEffects &src);

	void Init();

	void FromNet(const EFFECTS_INFO &msg);
	void ToNet(EFFECTS_INFO &msg) const;

	LSError Serialize(ParamArchive &ar);
	LSError WorldSerialize(ParamArchive &ar);

  DECLARE_NETWORK_OBJECT
  static NetworkMessageFormat &CreateFormat
	(
		NetworkMessageClass cls,
		NetworkMessageFormat &format
	);
	TMError TransferMsg(NetworkMessageContext &ctx);
};

enum ArcadeWaypointType
{
	ACUNDEFINED,
	ACMOVE,
	ACDESTROY,
	ACGETIN,
	ACSEEKANDDESTROY,
	ACJOIN,
	ACLEADER,
	ACGETOUT,
	ACCYCLE,
	ACLOAD,
	ACUNLOAD,
	ACTRANSPORTUNLOAD,
	ACHOLD,
	ACSENTRY,
	ACGUARD,
	ACTALK,
	ACSCRIPTED,
	ACSUPPORT,
  ACGETINNEAREST,
  ACDISMISS,
	ACN,
	ACLOGIC = ACN,
	ACAND = ACLOGIC,
	ACOR,
	ACLOGICN
};

enum SpeedMode
{
	SpeedUnchanged,
	SpeedLimited,
	SpeedNormal,
	SpeedFull
};

enum VehSpeedMode
{
  VehSpeedAuto,
  VehSpeedSlow,
  VehSpeedNormal,
  VehSpeedFast
};

#ifndef DECL_ENUM_COMBAT_MODE
#define DECL_ENUM_COMBAT_MODE
DECL_ENUM(CombatMode)
#endif
DEFINE_ENUM_BEG(CombatMode)
        CMUnchanged,
        CMCareless,
        CMSafe,
        CMAware,
        CMCombat,
        CMStealth
DEFINE_ENUM_END(CombatMode)

struct CombatModeRange
{
  CombatMode beg,end;
  
  CombatModeRange(CombatMode beg, CombatMode end=CMStealth):beg(beg),end(end){}
};


struct CautionRange
{
  float beg,end;
  
  CautionRange(float beg=0.0f, float end=1.0f):beg(beg),end(end){}
};
enum AWPShow
{
	ShowNever,
	ShowEasy,
	ShowAlways
};

/// waypoint information
struct WaypointInfo
{
  /// waypoint position (often waypoint destination as well)
	Vector3 position;
  /// placement randomization radius
	float placement;
  /// precision which is required for the waypoint to be completed
	float completitionRadius;
	int id;					// target vehicle's id
	ObjectId idObject;
	int housePos;		// position in house
	ArcadeWaypointType type;
	AI::Semaphore combatMode;
	AI::Formation formation;
	SpeedMode speed;
	CombatMode combat;
	float timeoutMin;
	float timeoutMid;
	float timeoutMax;
	RString description;
	RString _expCond;
	RString _expActiv;
#if USE_PRECOMPILATION
  CompiledExpression _compiledCond;
  CompiledExpression _compiledActiv;
#endif
	RString script;
	AWPShow showWP;
	bool selected;
  bool visible;
	AutoArray<int> synchronizations;
	ArcadeEffects effects;

	WaypointInfo();
	void InitWp();
	bool HasEffect() const;
  bool RequiresStop() const;
  
#if _VBS3 // allow waypoints to have a variable space
  GameVarSpace vars;
#endif

  DECLARE_NETWORK_OBJECT
	static NetworkMessageFormat &CreateFormat(
		NetworkMessageClass cls,
		NetworkMessageFormat &format
	);
	TMError TransferMsg(NetworkMessageContext &ctx);

	// World serialization
	LSError Serialize(ParamArchive &ar);
};
TypeIsMovable(WaypointInfo);

struct ArcadeWaypointInfo : public WaypointInfo
{
	typedef WaypointInfo base;

	VisitorObjId idVisitorObj;	// target building's id

	ArcadeWaypointInfo();
	ArcadeWaypointInfo(const ArcadeWaypointInfo &src);

	void Init();

	void AddOffset(Vector3Par offset);
	void Rotate(Vector3Par center, float angle, bool sel);
	void CalculateCenter(Vector3 &sum, int &count, bool sel);

	bool CheckObjectIds();
	void RepairObjectIds();

	// Mission serialization
	LSError Serialize(ParamArchive &ar);
};
TypeIsMovable(ArcadeWaypointInfo);

enum ArcadeUnitSpecial
{
	ASpNone,
	ASpCargo,
	ASpFlying,
	ASpForm,
  ASpCanCollide
};

enum ArcadeUnitAge
{
	AAActual,
	AA5Min,
	AA10Min,
	AA15Min,
	AA30Min,
	AA60Min,
	AA120Min,
	AAUnknown,
	AAN
};

enum ArcadeUnitPlayer
{
	APNonplayable,
	APPlayerCommander,
	APPlayerDriver,
	APPlayerGunner,
	APPlayableC,
	APPlayableD,
	APPlayableG,
	APPlayableCD,
	APPlayableCG,
	APPlayableDG,
	APPlayableCDG,
};

DEFINE_ENUM_BEG(LockState)
	LSUnlocked,
	LSDefault,
	LSLocked
DEFINE_ENUM_END(LockState)

DEFINE_ENUM_BEG(LockedState)
	LdSFree,
	LdSWeaponLocked,
	LdSWeaponAimed,
	LdSIncomigMissile
DEFINE_ENUM_END(LockedState)

enum ArcadeSensorActivation
{
	ASANone,
	ASAEast,
	ASAWest,
	ASAGuerrila,
	ASACivilian,
	ASALogic,
	ASAAnybody,
	ASAAlpha,
	ASABravo,
	ASACharlie,
	ASADelta,
	ASAEcho,
	ASAFoxtrot,
	ASAGolf,
	ASAHotel,
	ASAIndia,
	ASAJuliet,
  ASAEastSeized,
  ASAWestSeized,
  ASAGuerrilaSeized,
	ASAStatic = 253,
	ASAVehicle,
	ASAGroup,
	ASALeader,
	ASAMember,
};

enum ArcadeSensorActivationType
{
	ASATPresent,
	ASATNotPresent,
	ASATWestDetected,
	ASATEastDetected,
	ASATGuerrilaDetected,
	ASATCiviliansDetected,
};

enum ArcadeSensorType
{
	ASTNone,
	ASTEastGuarded,
	ASTWestGuarded,
	ASTGuerrilaGuarded,
	ASTSwitch,
	ASTEnd1,
	ASTEnd2,
	ASTEnd3,
	ASTEnd4,
	ASTEnd5,
	ASTEnd6,
	ASTLoose,
	ASTN
};

enum MarkerType
{
	MTIcon,
	MTRectangle,
	MTEllipse
};

#if _VBS2 // conditional markers
struct LayeredMarkerInfo : public RefCount
{
  Ref<Texture> icon;
	float a;
	float b;
	float x;
	float y;
  LayeredMarkerInfo() {icon = NULL; a = 0; b = 0; x = 0; y = 0;}
};
#endif

struct ArcadeMarkerInfo
{
	Point3 position;
	RString name;
	RString text;
	MarkerType markerType;
	RString type;
	RString colorName;
	PackedColor color;
  float alpha;
	Ref<Texture> icon;
	RString fillName;
	Ref<Texture> fill;
	float size;
	float a;
	float b;
	float angle;
  bool drawBorder;
  int shadow;

	bool selected;	

#if _VBS2 // conditional markers
  bool autosize;  // if true, marker will maintain size regardless of map zoom (default)
  RString attachCondition;  // for attaching to an object
  Ref<Object> attachTo;
  OLinkArray(Object) attachCtx;
  Vector3 attachOffset;  
  RString condition;        // for hiding/showing the marker
  bool isHidden;
  RefArray<LayeredMarkerInfo> layeredIcons;        // for layered markers
#endif

	ArcadeMarkerInfo();
	ArcadeMarkerInfo(const ArcadeMarkerInfo &src);

	void Init();

	LSError Serialize(ParamArchive &ar);

  DECLARE_NETWORK_OBJECT
  static NetworkMessageFormat &CreateFormat
  (
    NetworkMessageClass cls,
    NetworkMessageFormat &format
  );
	TMError TransferMsg(NetworkMessageContext &ctx);

	void AddOffset(Vector3Par offset);
	void Rotate(Vector3Par center, float angle, bool sel);
	void CalculateCenter(Vector3 &sum, int &count, bool sel);

	void OnColorChanged();
	void OnFillChanged();
	void OnTypeChanged();
};
TypeIsMovable(ArcadeMarkerInfo);

#endif
