#ifdef _MSC_VER
#pragma once
#endif

#ifndef _JVM_INIT_HPP
#define _JVM_INIT_HPP

#include <Java/jni.h>
#include <Es/Strings/rString.hpp>

// Access to the JNI interface
JNIEnv *GetJNIEnv();

// Initialization of Java VM
void InitJavaVM(RString jre, int debugPort, bool suspend);

// Destruction of Java VM
void DestroyJavaVM();

// Creation of RVEngine.java interface file
void ExportJavaInterface(RString jniSources);

// Performance test
void PerfTestJavaVM();

#endif

