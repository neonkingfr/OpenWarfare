// Poseidon - vehicle class
// (C) 1997, Suma
#include "wpch.hpp"

#include "planeOrHeli.hpp"
#include "landscape.hpp"
#include "world.hpp"
#include "AI/ai.hpp"

PlaneOrHeli::PlaneOrHeli(const EntityAIType *name, Person *driver, bool fullCreate)
:base(name, driver, fullCreate),
_stopMode(SMNone),
_stopPosition(VZero),
_stopResult(SPNotReady)
{
}

struct IsHelipadContext
{
  AIBrain *unit;
};
static bool IsHelipad(Object *obj, float dist2, void *context)
{
  Entity *veh = dyn_cast<Entity>(obj);
  if (!veh) return false;
  const VehicleNonAIType *type = veh->GetNonAIType();

  //VBS3 should be more efficient to check the type before checking if it's empty
  if (!type->IsKindOf(GWorld->Preloaded(VTypeHelipad))) return false;

  // check for locks only - ignore cost, the one who placed the H probably knows what is he doing
  IsHelipadContext *ctx = (IsHelipadContext *)context;
  EntityAI *vehAI = ctx->unit->GetVehicle();

  AILockerSavedState state;
  vehAI->PerformUnlock(&state);

  int x = toIntFloor(obj->FutureVisualState().Position().X() * InvOperItemGrid);
  int z = toIntFloor(obj->FutureVisualState().Position().Z() * InvOperItemGrid);

  bool ret = !GLOB_LAND->LockingCache()->IsLocked(OperMapIndex(x), OperMapIndex(z), false);
  vehAI->PerformRelock(state);

  return ret;
}

PlaneOrHeli::StopPositionResult PlaneOrHeli::FindStopPosition()
{
  AIBrain *unit = DriverBrain();
  if (!unit)
  {
    Fail("No pilot");
    return SPFound;
  }

  StopPositionResult found = SPNotFound;

  Vector3 bestPos=FutureVisualState().Position();

  Vector3 preferredPos = _stratGoToPos;
  float maxStopDistance = floatMax(Type()->GetStopDistance()*1.3f,500);
  if (preferredPos.SquareSize() <= 0.1f || preferredPos.Distance2(bestPos) > Square(maxStopDistance))
  {
    // preferred position lies in front of us
    preferredPos = FutureVisualState().Position()+FutureVisualState().Speed()*6;
  }

  {
    IsHelipadContext ctx;
    ctx.unit = unit;
    bool ready = true;
    OLink(Object)  obj = GLandscape->NearestObjectAsync(ready,true,preferredPos,500,IsHelipad,&ctx,true);
    if (!ready) return SPNotReady;
    if (obj)
    {
      bestPos = obj->FutureVisualState().Position();
      found = SPFound;
    }
  }

  if (found == SPNotFound)
  {
    // unlock while searching
    AILockerSavedState state;
    PerformUnlock(&state);

    float bestCost=1e10;
    int range=2;
    do
    {
      // check neighbourhood
      // increase range if necessary
      int x=toIntFloor(preferredPos.X()*InvLandGrid);
      int z=toIntFloor(preferredPos.Z()*InvLandGrid);
      for( int xx=x-range; xx<=x+range; xx++ )
      for( int zz=z-range; zz<=z+range; zz++ )
      {
        GeographyInfo info=GLOB_LAND->GetGeography(xx,zz);
        float cost=0;
        if( info.u.minWaterDepth>1 ) continue;
        if( info.u.full ) continue;
        if( info.u.howManyObjects>0 ) continue;
        int grad=info.u.gradient;
        if (_stopMode == SMLand)
        {
          if( grad>=4 ) continue;
          const static int gradPenalty[4]={0,5,10,30};
          cost+=gradPenalty[grad];
          if (info.u.road) cost+=10;
        }
        else
        {
          if( grad>=5 ) continue;
          const static int gradPenalty[5]={0,0,2,5,10};
          cost+=gradPenalty[grad];
          if (info.u.road) cost+=2;
        }
        // some penalty for a water present
        cost += info.u.maxWaterDepth*5;
        cost+=sqrt((xx-x)*(xx-x)+(zz-z)*(zz-z))*0.5; // small penalty for distance
        Vector3 pos,normal;
        pos.Init();
        normal.Init();
        pos[0]=xx*LandGrid+LandGrid*0.5,pos[2]=zz*LandGrid+LandGrid*0.5;
#if _ENABLE_WALK_ON_GEOMETRY
        pos[1]=GLOB_LAND->RoadSurfaceYAboveWater(pos[0],pos[2], Landscape::FilterIgnoreOne(this));
#else
        pos[1]=GLOB_LAND->RoadSurfaceYAboveWater(pos[0],pos[2]);
#endif
        // check for object collision at given place
        if( bestCost>cost )
        {
          //AIUnit *unit=DriverBrain();
          if( !AIUnit::FindFreePosition(pos,normal,false,GetType()) ) continue;
          bestPos=pos;
          bestCost=cost;
          found = SPFound;
        }
      }
    } while( bestCost>10 && (range+=2)<8 );

    PerformRelock(state);
  }

  if ((_stopPosition-bestPos).SquareSize()>Square(4))
  {
    // reset only when there is significant change
    ResetAutopilot();
  }
  _stopPosition=bestPos;
  return found;
}

void PlaneOrHeli::UpdateStopMode(AIBrain *unit)
{
  if (unit->GetState() == AIUnit::Stopping || unit->GetState() == AIUnit::Stopped)
  {
    _getinUnits.RemoveNulls();
    _getoutUnits.RemoveNulls();
    bool getin = _getinUnits.Size() > 0;
    bool getout = _getoutUnits.Size() > 0;
    bool supply = _supply && GetSupplyUnits().Size() > 0;
    bool pilot = false;
    for (int i=0; i<_getoutUnits.Size(); i++)
      if (_getoutUnits[i] == unit)
      {
        pilot = true;
        break;
      }
    // if we are not a helicopter, we always need a full stop (VTOL plane)
    StopMode mode = SMNone;
    if (pilot || _landing == LMLand) mode = SMLand;
    else if (getin || supply || _landing == LMGetIn) mode = SMGetIn;
    else if (getout || _landing == LMGetOut) mode = SMGetOut;

    if
    (
      mode != _stopMode ||
      unit->GetState() == AIUnit::Stopping && !unit->CheckEmpty(_stopPosition)
    )
    {
      switch (FindStopPosition())
      {
      case SPFound:
        _stopMode = mode;
        _stopResult = SPFound;
        break;
      case SPNotFound:
        // we will report a failure, hence we could cancel the landing - however we leave this up to the script
        //_landing = LMNone;
        unit->OnStepTimedOut();
        _stopResult = SPNotFound;
        break;
      case SPNotReady:
        // try it later
        break;
      }
    }
  }
  else
  {
    _stopMode = SMNone;
  }
}

void PlaneOrHeli::LandStarted(LandingMode landing)
{
  base::LandStarted(landing);
  if (landing != LMNone) _stopResult = SPNotReady;
}

int PlaneOrHeli::GetLastMissileIndex(const TurretContext &context,int weapon) const
{
  int total = 0;
  const Magazine *currentMag = context._weapons->_magazineSlots[weapon]._magazine;
  for (int i=0; i<context._weapons->_magazines.Size(); i++)
  {
    const Magazine *mag = context._weapons->_magazines[i];
    const MagazineType *type = mag->_type;
    if (!type) continue;
    const AmmoType *ammo = type->_ammo;
    if (!ammo) continue;
    if (ammo->_simulation != AmmoShotMissile) continue;
    if (ammo->maxControlRange < 10) continue;

    if (currentMag==mag)
      return total+=mag->GetAmmo();

    total+=mag->_type->_maxAmmo;
  }
  return -1;
}

static const EnumName StopPositionResultNames[]=
{
  EnumName(PlaneOrHeli::SPFound, "Found"),
  EnumName(PlaneOrHeli::SPNotFound, "NotFound"),
  EnumName(PlaneOrHeli::SPNotReady, "NotReady"),
  EnumName()
};

template<>
const EnumName *GetEnumNames(PlaneOrHeli::StopPositionResult dummy)
{
  return StopPositionResultNames;
}

static const EnumName StopModeNames[]=
{
  EnumName(PlaneOrHeli::SMNone, "None"),
  EnumName(PlaneOrHeli::SMLand, "Land"),
  EnumName(PlaneOrHeli::SMGetIn, "GetIn"),
  EnumName(PlaneOrHeli::SMGetOut, "GetOut"),
  EnumName()
};

template<>
const EnumName *GetEnumNames(PlaneOrHeli::StopMode dummy)
{
  return StopModeNames;
}
