/*!
  \file
  Implementation of EntityAI functions related to target tracking,
  target list management and weapon selection
*/


#include "wpch.hpp"

#include "vehicleAI.hpp"
#include "person.hpp"
#include "global.hpp"
#include <El/Common/randomGen.hpp>
#include "paramArchiveExt.hpp"
#include "world.hpp"
#include "AI/ai.hpp"
#include "landscape.hpp"
#include "visibility.hpp"
#include "diagModes.hpp"

#include <El/Common/perfLog.hpp>
#include <El/Common/perfProf.hpp>
#include <Es/Containers/sortedArray.hpp>
#include <Es/ReportStack/reportStack.hpp>

#if _ENABLE_CHEATS

# define DIAG_RESULT 0
# define DIAG_SHOOT_RESULT 0
# define DIAG_ATTACK 0

# define DIAG_TARGET 0 // SelectFireWeapon function (0..3)

# define LOG_TARGETS 0

# if DIAG_TARGET
#   define SUSPEND_OTHER_AI 0 // when 1, only camera watched AI is performing fire decisions
#   define NODIAG_OTHER_AI 0 // when 1, only camera watched AI is logging its fire decisions
# endif

#endif

const float ValidTime=5.0f;

/*!
\patch 1.28 Date 10/24/2001 by Ondra.
- Fixed: AI was not able to fire at armored training target.
*/
const float MinVisibleFire=0.55f;

float TargetNormal::FadingSpotability() const
{
  // apply accuracy rules
  float validTime = floatMax(ValidTime,_validTime);
  float old=Glob.time-spotabilityTime-validTime-1;
  if( old<0 ) return spotability;  
  float ret=spotability-old*(1.0f/90);
  saturate(ret,0,4);
  return ret;
}

void TargetNormal::OnSuppressed(const AmmoType *ammo)
{
  _timeSuppressed = Glob.time;
  // check suppression
  // assume ammo worth of 7mm of armor hit causes 100 % suppression
  // this corresponds to a rifle bullet 
  _suppressed = ammo->hit*(1.0f/7);
}

float TargetNormal::FadingSuppressed() const
{
  const float suppressTimeout = 60.0f;
  if (_timeSuppressed<Glob.time-suppressTimeout) return 0;
  float suppressedAge = Glob.time-_timeSuppressed;
  // never considered the target completely suppresed - even when we are firing at it, it might fire back
  // if possible, we still want to stay out of its line of fire
  float factor = InterpolativC(suppressedAge,5.0f,suppressTimeout,0.95,0.0f);
  // source _suppressed may be bigger than 1 when strong ammo is used
  return floatMin(1,_suppressed*factor);
}

/*!
\patch 1.01 Date 6/26/2001 by Ondra.
- Fixed: slower forgetting target type and side.
\patch 2.92 Date 1/23/2006 by Jirka
- Fixed: forgetting of target type and size disabled (only position is fading)
*/

float TargetNormal::FadingAccuracy() const
{
  // the side and type accuracy is something which does not reduce when older
  return accuracy;
}

bool TargetNormal::IsVanishedOrDestroyed() const
{
  return vanished || destroyed;
}
AIBrain *TargetNormal::SeenBy() const
{
  return idSensor;
}

void TargetNormal::UpdateWhenUnassigned(AIGroup *group)
{
  damagePerMinute = group->GetDamagePerMinute(this);
}

void TargetNormal::UpdateRemotePosition(EntityAI *ent)
{
  Assert(ent==idExact);
  position=ent->AimingPosition(ent->FutureVisualState());
  dir = ent->FutureVisualState().Transform().Direction();
  UpdateSnappedPosition();
}


float TargetNormal::FadingSideAccuracy() const
{
  return sideAccuracy;
}

float TargetNormal::FadingPosAccuracy() const
{
  // we assume error 1 m per 1 sec unless we know a type
  //float errorGrowInTime = 1.0f;
  float errorGrowInTime = 1.0f;
  // FIX: avoid moving of static objects
  if (type)
  {
    // if the target is known to be empty, we should suppose it does not move
    // as it is quite likely we know such thing if we can see the target, we use the actual information
    // (minor cheat on AI side)
    if (idExact && idExact->PilotUnit()==NULL)
    {
      errorGrowInTime = 0;
    }
    else
    {
      errorGrowInTime = 0.25f * type->GetTypSpeedMs();
    }
  }
  // max. error possible is maxAge*errorGrowInTime
  const float maxAge = 180; // max. error about 54 m
  float validTime = floatMax(ValidTime,_validTime);
  if (Glob.time<posAccuracyTime+validTime) return posAccuracy;
  float age = posAccuracyTime>Glob.time-maxAge ? Glob.time-posAccuracyTime : maxAge;
  // first grow error quite fast
  // after a few seconds grow much slower
  // without this hot-fix helicopter has forgotten the position before it came back to engage
  float errorAge = age-validTime;
  const float fastForget = 5.0f;
  float error = 0;
  if (errorAge<fastForget)
  {
    error= errorAge*errorGrowInTime;
  }
  else
  {
    error= fastForget*errorGrowInTime+(errorAge-fastForget)*errorGrowInTime*0.1f;
  }
  return posAccuracy+error;
}

void TargetNormal::UpdatePosition(Vector3Par pos, float typeAccuracy, float posAccuracy)
{
  position = pos;
  posAccuracy = posAccuracy;
  accuracy = typeAccuracy;
  sideAccuracy = typeAccuracy;
  UpdateSnappedPosition();
}

void TargetNormal::UpdateSnappedPosition()
{
  // update snappedPosition
  float gridDist;
  const EntityAIType *tarType = GetType();
  if (tarType)
  {
    gridDist =  tarType->GetTypSpeed() * 5.0f/3.6f; //distance using typical speed after 5 seconds
    saturate(gridDist, 20.0f, 500.0f);    
  }
  else gridDist = 20.0f;
  const float gridDist2 = gridDist*gridDist;
  if ( position.Distance2(snappedPosition)>gridDist2 )
  {
    snappedPosition = (position + Vector3(gridDist/2, gridDist/2, gridDist/2));
    snappedPosition = snappedPosition - Vector3(fmod(snappedPosition.X(),gridDist), fmod(snappedPosition.Y(),gridDist), fmod(snappedPosition.Z(),gridDist));
  }
}

Target::Target()
{
}

/// general "interest" value
float Target::HowMuchInteresting() const
{
  float interesting = 1;
  // we get some points for being human
  if (IsKindOf(GWorld->Preloaded(VTypeMan))) interesting += 10;
  if (idExact)
  {
    // we get many points for firing, screaming and moving
    // this is all calculated in audibility - making noise
    // if target is visible, we should also check VisibleMovement()
    // we check it always - hopefully it will not matter too much
    interesting += idExact->Audible()*5;
    // we get some points for moving
    interesting += idExact->VisibleMovement();
    // and also for speaking
    interesting += idExact->GetSpeaking()*30;
    // we add something very small to ensure stable sort ordering
    interesting += (idExact->ID().Encode()&0x3ff)*(1.0f/0x3ff);
  }
  return interesting;
}


DEFINE_FAST_ALLOCATOR(TargetNormal)

TargetNormal::~TargetNormal()
{
}

TargetNormal::TargetNormal()
{
  Init();
}

void TargetNormal::RandomPosError()
{
  posErrorDirection = Vector3(
    GRandGen.RandomValue()*2-1,
    GRandGen.RandomValue()*2-1,
    GRandGen.RandomValue()*2-1
  );
}

void TargetNormal::Init()
{
  position=VZero;
  snappedPosition=VZero;
  dir = VForward;
  RandomPosError();
  // create
  speed=VZero;
  side=TSideUnknown;
  sideChecked = false;
  type=NULL;
  spotability=0; // fading
  spotabilityTime=Time(0);
  accuracy=0; // fading
  sideAccuracy=0; // fading
  posAccuracy = 1000; // no accuracy by default
  _validTime = 0;
  posAccuracyTime = TIME_MIN;
  _isKnown = false; // do we remember it
  posReported=VZero;
  timeReported = TIME_MIN;
  vanished=false; // send disappeared units until disappearConfirmed
  hasDisclosedUs=false;
  destroyed=false; // send confirmed kill
  category = TgtCatEnemy;
  reactedToDestruction = false;
  idExact=NULL;
  idSensor=NULL;
  delay=TIME_MAX; // never seen
  delaySensor=TIME_MAX;
  delayVehicle=TIME_MAX;
  lastSeen=TIME_MIN;
  damagePerMinute=0;
  subjectiveCost=0;
  _timeSuppressed = TIME_MIN;
  _suppressed = 0;
  //@{ helpers for target aggregation
  aggrGroupCost = 0;
  aggrIsReported = aggrPresent = false;
  aggrPosition = VZero;
  //@}
}

void TargetNormal::Init(EntityAI *ai)
{
  // it is visible - update state information
  position=ai->AimingPosition(ai->FutureVisualState());
  snappedPosition = VZero;
  UpdateSnappedPosition();
  dir = ai->FutureVisualState().Direction();
  speed=ai->PredictVelocity();
  // TODO: error based on visibility
  sideChecked = false;
  side = TSideUnknown;
  category = TgtCatEnemy;
  type=ai->GetType(0); // unknown type
  //dammage=ai->GetTotalDamage();
  //idSensor=this;
  idExact=ai;
  spotabilityTime=Glob.time-60;
  //visibility=0;
  accuracy=0;
  sideAccuracy=0;
  spotability=0;
  _isKnown = false;
  timeReported=TIME_MIN;
  posReported = VZero;
  destroyed=false;
  reactedToDestruction = false;
  vanished=false;
  hasDisclosedUs=false;
  _timeSuppressed = TIME_MIN;
  _suppressed = 0;
  #if LOG_TARGETS
    LogF
    (
      "New target %s %s",
      (const char *)GetDebugName(),(const char *)ai->GetDebugName()
    );
  #endif
}

bool TargetNormal::IsKnownNoDelay() const
{
  if (!_isKnown) return false;

  const float minSpotability = 0.06f;
  if (FadingSpotability() < minSpotability)
  {
    // target forgotten
    // forget targets only when you do not seen them for a long time
    if (lastSeen < Glob.time - 120)
    {
      return false;
    }
  }

  return true;
}

bool TargetNormal::IsKnownBy( const AIBrain *unit ) const
{
  // known by specific group member
  if (!IsKnownNoDelay()) return false;
  if( delay<=Glob.time ) return true; // whole group already knows the target
  // the same vehicle
  if (idSensor && idSensor->GetVehicle()==unit->GetVehicle())
  {
    if (delayVehicle<=Glob.time) return true;
  }
  if (delaySensor>Glob.time) return false; // even sensor does not know it yet
  if( unit!=idSensor )
  {
    const AIUnit *sensorUnit = unconst_cast(unit)->GetUnit();
    if (!sensorUnit) return false; // each agent has its own target list, no need to use _idSeen
    if (_idSeen.Get(sensorUnit->ID()))
      return false; // not seen by us, use radio delay
  }
  return true;
}

bool TargetNormal::IsKnownByAll() const
{
  // known by all group members
  return IsKnownNoDelay() && delay<=Glob.time;
}

bool TargetNormal::IsKnownBySome() const
{
  // know by at least one group member
  return IsKnownNoDelay() && ( delay<=Glob.time || delaySensor<=Glob.time );
}

void TargetNormal::Forget()
{
  _isKnown = false;
}

void TargetNormal::Reveal(const SideInfo &side, TargetList *list)
{
  if (_isKnown) return;
  _isKnown = true;
  //LogF("TargetNormal::Reveal %s",cc_cast(GetDebugName()));
  category = CheckCategory(side);
  list->ForceReport(Glob.time+0.2);
}

/*!
\patch 5111 Date 12/21/2006 by Ondra
- Fixed: Targets are made known to AI once message about them is transmitted over radio.
  This prevents situations where AI gunner silently rejected a command because not knowing the target.
\patch 5112 Date 1/4/2007 by Ondra
- Fixed: Crash caused by target handling change in 5111.
*/
void TargetNormal::RevealInVehicle(Transport *veh)
{
  Time maxDelay = Glob.time + 0.3f;
  if (delayVehicle>maxDelay)
  {
    // check if it is the vehicle of idSensor
    // if not, we cannot do anything useful
    if (idSensor && veh==idSensor->GetVehicle())
    {
      delayVehicle = maxDelay;

    }
  }
}


bool TargetNormal::IsKnown() const
{
  return IsKnownBySome();
}

bool TargetNormal::AccurateEnoughForHit(const AmmoType *ammo, AIBrain *brain) const
{
  // check if there is current LOS 
  // _visTracker used before tests LOS for fire geometries
  // we want normal visibility here
  // use normal cached visibility query
  float memoryError = 0;
  if (idExact && brain)
  {
    // check last visibility information and its age
    float vis = GWorld->Visibility(brain,idExact);
    Time lastVisible = GWorld->VisibilityTime(brain,idExact);
    // if never seen, we have to assume error is large
    if (lastVisible<=TIME_MIN) return false;
    // simulate prediction error growing based on time from last seen
    // if the most recent entry says it is visible, trust it - there is some time between updates 
    // if we cannot see him now, always assume some time must have elapsed since we have seen him
    float memoryAge = vis>0 ? 0 : floatMax(Glob.time-lastVisible,1.0f);
    // for more distant targets the memory error should be growing faster
    if (memoryAge>0)
    {
      Vector3 brainPos = brain->GetVehicle()->FutureVisualState().Position();
      memoryError = memoryAge * idExact->FutureVisualState().Position().Distance(brainPos)*0.005f;
    }
  }

  float curError = floatMax(FadingPosAccuracy(),memoryError);
  LODShape *shape = type->GetShape();
  float accuracyRequired = shape ? shape->GeometrySphere()*0.5f : 0.5f;
  return curError < (ammo ? ammo->indirectHitRange*2 : 0)+accuracyRequired;
}

/*!
\patch 5158 Date 5/16/2007 by Ondra
- Fixed: AI target position accuracy sometimes was fluctuating a little bit for distant targets.
*/
bool TargetNormal::LimitError(float error, float *oldError)
{
  float curError = FadingPosAccuracy();
  if (oldError) *oldError = curError;
  if (error<=curError)
  {
    // note: sometimes older information may be actually more accurate
    // however we prefer the newer one if we think the old is too old (faded away)
    posAccuracy = error;
    posAccuracyTime = Glob.time;
    return true;
  }
  return false;
}


/// how much do we extrapolate the unit movement
const float MaxPredictMovementTime = 10.0f;
/*!
@param moved how much the function moved the position since some which is know to be valid.
  This is done so that called may keep surface relative Y when interested.

\patch 5122 Date 1/23/2007 by Ondra
- Fixed: Explosion sometimes revealed the exact unit position to the AI units.
*/

Vector3 TargetNormal::ExactAimingPosition(
  const AIBrain *who, const AmmoType *ammo, float &moved, bool head, float maxPredict, bool *isExact
) const
{
  // calculate position directly if target is fresh known
  // if it is never seen (not isKnown), position is complete crap
  if( idExact )
  {
    bool seen = true;
    if (who && who->LSIsAlive())
    {
      seen = GWorld->Visibility(who,idExact)>0;
    }
    // seen very recently? If yes, track actual position
    // if it is friendly, the delay may be much larger
    if( seen && Glob.time<lastSeen+1.0f)
    {
      Object::ProtectedVisualState<const EntityVisualState> vs = idExact->RenderVisualStateScope(true);
      // fix 5122 - only return exact information if almost exact information was already available      
      if (FadingPosAccuracy()<idExact->VisibleSize(vs))
      {
        moved = 0;
        if (isExact) *isExact = true;
        // when we are returning exact information, we may be able to return head position as well
        return head ? idExact->AimingPositionHeadShot(vs, ammo) : idExact->AimingPosition(vs, ammo);
      }
    }
  }
  if (isExact) *isExact = false;
  // estimate target position - do not use lastSeen (may be in future), use posAccuracy
  
  float timeFromSeen=Glob.time.Diff(posAccuracyTime);
  saturateMin(timeFromSeen,maxPredict);
  // speed prediction should not change height above surface - it should not make a solider flying
  moved = speed.Size()*timeFromSeen;
  return position+speed*timeFromSeen;
}

Vector3 TargetNormal::GetSpeed(const AIBrain *who) const
{
  // if we have a fresh position, assume we have a fresh speed as well
  // calculate position directly is target is fresh known
  // if it is never seen (not isKnown), position is complete crap
  Object *exact=idExact;
  if( exact )
  {
    bool seen = true;
    if (who)
    {
      seen = GWorld->Visibility(who,idExact)>0;
    }
    // seen very recently? If yes, track actual position
    // if it is friendly, the delay may be much larger
    if( seen && Glob.time<lastSeen+1.0f)
    {
      // fix 5122 - only return exact information if almost exact information was already available      
      if (FadingPosAccuracy()<idExact->VisibleSize(idExact->RenderVisualState()))      
      {
        return exact->PredictVelocity();
      }
    }
  }
  return speed;
}

static bool TransportCanMove(Transport *veh)
{
  if (!veh->GetType()->HasDriver()) return true;
  if (!veh->IsPossibleToGetIn()) return false;
  // check if there is a driver, and the driver is alive
  AIBrain *driver = veh->PilotUnit();
  if (!driver || !driver->LSIsAlive())
  {
    // check if we can know the driver is dead
    if (driver)
    {
      const float longEnough = 3.0f;
      Person *person = driver->GetPerson();
      // prevent immediate reaction
      if (person && person->GetDestroyedTime()>Glob.time-longEnough) return true;
    }
    // no driver: check if some replacement is possible
    // check owning group
    AIGroup *group = veh->GetGroupAssigned();
    if (!group || group->NAliveUnits()==0) return false;
  }
  return true;
}

TargetState TargetNormal::State(AIBrain *sensor) const
{
  // check if target is alive
  if (destroyed) return TargetDestroyed;
  if (vanished) return TargetDestroyed;
  if (!sensor) return TargetAlive;
  if (!sensor->IsEnemy(side)) return TargetAlive;
  if (!idExact) return TargetEnemyEmpty;
  Transport *trans = dyn_cast<Transport,TargetType>(idExact);
  if (trans && !TransportCanMove(trans))
  {
    // enemy vehicles with alive gunner are dangerous even when they cannot move
    if (idExact->IsAbleToFireAnyTurret())
    {
      return TargetEnemyCombat;
    }
    // if there is any cargo, do not report as empty
    if (idExact->IsCrewed()) return TargetEnemy;
    return TargetEnemyEmpty;
  }
  
  if (idExact->IsCombatUnit()) // if target has no weapons, ignore it cannot fire and still consider it combatant
  {
    if (!idExact->IsAbleToFireAnyTurret())
    { // if target has no gunner, it is less dangerous - TODO: handle assigned replacement gunner
      // when no driver, it is a static defense - not able to fire means empty
      return idExact->GetType()->HasDriver() ? TargetEnemy : TargetEnemyEmpty;
    }
  }
  return TargetEnemyCombat;
}

/*!
\patch 5122 Date 1/23/2007 by Ondra
- Fixed: AI could sometimes aim into the air when targeting an unknown land unit.
*/

Vector3 TargetNormal::LandAimingPosition(const AIBrain *who, const AmmoType *ammo, bool head, bool *isExact) const
{
  float moved = 0;
  Vector3 pos = ExactAimingPosition(who,ammo,moved,head,MaxPredictMovementTime,isExact);
  // check last actually seen position above surface
  float aboveSurface=position.Y()-GLandscape->SurfaceYAboveWater(position.X(),position.Z());
  pos += posErrorDirection*posAccuracy;
  // if it was not flying, assume it is not flying now - same height
  // posAccuracy>X is an optimization (avoid SurfaceYAboveWater) - if accurate, no need to update
  if ((aboveSurface<5 || !type->CanBeAirborne()) && posAccuracy+moved>0.10f)
  {
    pos[1]=GLandscape->SurfaceYAboveWater(pos.X(),pos.Z())+aboveSurface;
  }
  return pos;
}

Vector3 TargetNormal::LastKnownPosition(const AIBrain *who, const AmmoType *ammo, bool head, bool *isExact) const
{
  float moved = 0;
  // we want to use very little prediction here, if any
  static float predict = 0.1f;
  Vector3 pos = ExactAimingPosition(who,ammo,moved,head,predict,isExact);
  // check last actually seen position above surface
  float aboveSurface=position.Y()-GLandscape->SurfaceYAboveWater(position.X(),position.Z());
  pos += posErrorDirection*posAccuracy;
  // if it was not flying, assume it is not flying now - same height
  // posAccuracy>X is an optimization (avoid SurfaceYAboveWater) - if accurate, no need to update
  if ((aboveSurface<5 || !type->CanBeAirborne()) && posAccuracy+moved>0.10f)
  {
    pos[1]=GLandscape->SurfaceYAboveWater(pos.X(),pos.Z())+aboveSurface;
  }
  return pos;
}

Vector3 TargetNormal::LandAimingPosition() const
{
  float moved = 0;
  Vector3 pos = ExactAimingPosition(NULL,NULL,moved,false,MaxPredictMovementTime,NULL);
  // check last actually seen position above surface
  float aboveSurface=position.Y()-GLandscape->SurfaceYAboveWater(position.X(),position.Z());
  pos += posErrorDirection*posAccuracy;
  if ((aboveSurface<5 || !type->CanBeAirborne()) && posAccuracy+moved>0.10f)
  {
    pos[1]=GLandscape->SurfaceYAboveWater(pos.X(),pos.Z())+aboveSurface;
  }
  return pos;
}

Vector3 TargetNormal::AimingPosition() const
{
  // calculate position directly if target is fresh known
  float moved = 0;
  return ExactAimingPosition(NULL,NULL,moved,false,MaxPredictMovementTime,NULL)+posErrorDirection*posAccuracy;
}

float TargetNormal::VisibleSize() const
{
  Object *exact=idExact;
  if( exact )
  {
    return exact->VisibleSize(exact->FutureVisualState());
  }
  return type->VisibleSize();
}

TargetNormal *TargetNormal::LoadRef(ParamArchive &ar)
{
  Target *tgt = base::LoadRef(ar);
  Assert( !tgt || dynamic_cast<TargetNormal *>(tgt) );
  return static_cast<TargetNormal *>(tgt);
}

DEFINE_FAST_ALLOCATOR(TargetInGroup)

TargetInGroup::TargetInGroup(AIGroup *group)
: _groupOwner(group)
{
}

LSError TargetInGroup::Serialize(ParamArchive &ar)
{
  base::Serialize(ar);
  CHECK(ar.SerializeRef("group", _groupOwner, 1))
  if (ar.IsSaving())
  {
    RString type("group");
    CHECK(ar.Serialize("targetType", type, 1))
  }
  return LSOK;
}

const SideInfo *TargetInGroup::GetOwnerSideInfo() const
{
  if (!_groupOwner) return NULL;
  return _groupOwner->GetCenter();
}


LSError TargetInGroup::SaveRef(ParamArchive &ar)
{
  RString type("group");
  CHECK(ar.Serialize("targetType", type, 1))

  AIGroup *grp = _groupOwner;
  AICenter *center = grp ? grp->GetCenter() : NULL;
  TargetSide side = center ? center->GetSide() : TSideUnknown;
  int idGroup = grp ? grp->ID() : -1;
  int index = -1;
  if (grp)
  {
    const TargetList &list=grp->GetTargetList();
    for (int i=0; i<list.AnyCount(); i++)
    {
      const Target *tgt=list.GetAny(i);
      if (tgt==this)
      {
        index = i;
        break;
      }
    }
  }
  CHECK(ar.SerializeEnum("side", side, 1))
  CHECK(ar.Serialize("idGroup", idGroup, 1))
  CHECK(ar.Serialize("index", index, 1))
  return LSOK;
}

#if _ENABLE_INDEPENDENT_AGENTS

DEFINE_FAST_ALLOCATOR(TargetInAgent)

TargetInAgent::TargetInAgent(AIAgent *agent)
: _agentOwner(agent)
{
}

LSError TargetInAgent::Serialize(ParamArchive &ar)
{
  base::Serialize(ar);
  CHECK(ar.SerializeRef("agent", _agentOwner, 1))
  if (ar.IsSaving())
  {
    RString type("agent");
    CHECK(ar.Serialize("targetType", type, 1))
  }
  return LSOK;
}

const SideInfo *TargetInAgent::GetOwnerSideInfo() const
{
  return _agentOwner;
}

LSError TargetInAgent::SaveRef(ParamArchive &ar)
{
  RString type("agent");
  CHECK(ar.Serialize("targetType", type, 1))

  int index = -1;

  AIAgent *agent = _agentOwner;
  if (agent)
  {
    const TargetList &list = agent->GetTargetList();
    for (int i=0; i<list.AnyCount(); i++)
    {
      Target *tgt = list.GetAny(i);
      if (tgt == this)
      {
        index = i;
        break;
      }
    }
    CHECK(agent->SaveRef(ar));
    CHECK(ar.Serialize("index", index, 1))
  }
  return LSOK;
}

#endif

DEFINE_FAST_ALLOCATOR(TargetKnowledge)

void TargetKnowledge::Init()
{
  idExact = NULL;
  position = VZero;
  side = TSideUnknown;
  type = NULL;
  lastSeen = TIME_MIN;
  vanished = false; // send disappeared units until disappearConfirmed
  destroyed = false; // send confirmed kill
}

void TargetKnowledge::Init(Target *src)
{
  idExact = src->idExact;
  position = src->GetPosition();
  side = src->GetSide();
  type = src->GetType();
  lastSeen = src->GetLastSeen();
  vanished = src->IsVanished();
  destroyed = src->IsDestroyed();
}

TargetKnowledge *TargetKnowledge::CreateObject(ParamArchive &ar)
{
  return new TargetKnowledge();
}

LSError TargetKnowledge::Serialize(ParamArchive &ar)
{
  base::Serialize(ar);

  if (ar.IsLoading() && ar.GetPass() == ParamArchive::PassFirst) Init();

  CHECK(::Serialize(ar, "position", position, 1))
  CHECK(ar.SerializeEnum("side", side, 1, TSideUnknown))
  CHECK(::Serialize(ar, "type", type, 1))
  CHECK(::Serialize(ar,"lastSeen",lastSeen,1, TIME_MIN));

  CHECK (ar.Serialize("vanished",vanished,1, false));
  CHECK (ar.Serialize("destroyed",destroyed,1, false));

  return LSOK;
}

LSError TargetKnowledge::SaveRef(ParamArchive &ar)
{
  Fail("Reference not allowed");
  return LSStructure;
}

float TargetKnowledge::FadingSideAccuracy() const {return 4;}
float TargetKnowledge::FadingAccuracy() const {return 4;}
float TargetKnowledge::FadingSpotability() const {return 4;}
float TargetKnowledge::FadingPosAccuracy() const {return 0;}

bool TargetKnowledge::IsKnownBy( const AIBrain *unit ) const {return true;}
bool TargetKnowledge::IsKnownByAll() const {return true;}
bool TargetKnowledge::IsKnownBySome() const {return true;}
void TargetKnowledge::Reveal(const SideInfo &side, TargetList *list) {}
void TargetKnowledge::RevealInVehicle(Transport *veh) {}

void TargetKnowledge::UpdatePosition(Vector3Par pos, float typeAccuracy, float posAccuracy)
{
  position = pos;
}

bool TargetKnowledge::AccurateEnoughForHit(const AmmoType *ammo, AIBrain *brain) const 
{
  return true;
}
bool TargetKnowledge::IsKnown() const {return true;}

// ??? better implementation needed ???
float TargetKnowledge::VisibleSize() const
{
  return 1;
}
Vector3 TargetKnowledge::AimingPosition() const
{
  return position;
}
Vector3 TargetKnowledge::LandAimingPosition() const
{
  return position;
}
Vector3 TargetKnowledge::LandAimingPosition(const AIBrain *who, const AmmoType *ammo, bool head, bool *isExact) const
{
  return position;
}

TargetState TargetKnowledge::State(AIBrain *sensor) const
{
  if (destroyed) return TargetDestroyed;
  if (vanished) return TargetDestroyed;
  return TargetAlive;
}

DEFINE_FAST_ALLOCATOR(TargetMinimal)

TargetMinimal::TargetMinimal(TargetId entity)
{
  idExact = entity;
}

const EntityAIType *TargetMinimal::GetType() const
{
  // return type info only with displayable accuracy
  return idExact->GetType(4.0f);
}

float TargetMinimal::FadingSideAccuracy() const {return 4;}
float TargetMinimal::FadingAccuracy() const {return 4;}
float TargetMinimal::FadingSpotability() const {return 4;}
float TargetMinimal::FadingPosAccuracy() const {return 0;}

void TargetMinimal::UpdatePosition(Vector3Par pos, float typeAccuracy, float posAccuracy)
{
  // no updates possible on virtual targets
}


bool TargetMinimal::IsKnownBy( const AIBrain *unit ) const {return true;}
bool TargetMinimal::IsKnownByAll() const {return true;}
bool TargetMinimal::IsKnownBySome() const {return true;}
bool TargetMinimal::IsKnown() const {return true;}
void TargetMinimal::Reveal(const SideInfo &side, TargetList *list) {}
void TargetMinimal::RevealInVehicle(Transport *veh) {}

Time TargetMinimal::GetLastSeen() const {return Glob.time;}

bool TargetMinimal::AccurateEnoughForHit(const AmmoType *ammo, AIBrain *brain) const {return true;}

float TargetMinimal::VisibleSize() const
{
  // note: objects used for minimal can never stop existing
  return idExact->VisibleSize(idExact->FutureVisualState());
}
Vector3 TargetMinimal::AimingPosition() const
{
  return idExact->AimingPosition(idExact->RenderVisualState());
}
Vector3 TargetMinimal::LandAimingPosition() const
{
  return idExact->AimingPosition(idExact->RenderVisualState());
}
Vector3 TargetMinimal::LandAimingPosition(const AIBrain *who, const AmmoType *ammo, bool head, bool *isExact) const
{
  return head ? idExact->AimingPosition(idExact->RenderVisualState(), ammo) : idExact->AimingPositionHeadShot(idExact->RenderVisualState(), ammo);
}

TargetState TargetMinimal::State(AIBrain *sensor) const
{
  // combat units should never by handled by this class
  Assert (!idExact->IsCombatUnit());
  // check if target is alive
  if (idExact->IsDamageDestroyed()) return TargetDestroyed;
  return TargetAlive;
}

LSError TargetMinimal::Serialize(ParamArchive &ar)
{
  base::Serialize(ar);

  return LSOK;
}

// TODO: move to paramArchiveExt.hpp

template <class BoolArray>
LSError SerializeBoolArray(ParamArchive &ar, const RStringB &name, BoolArray &value, int minVersion)
{
  if (ar.GetArVersion() < minVersion) return LSError(0);
  if (ar.IsSaving())
  {
    if (value.Size()==0) return LSError(0); // default value - empty array
    //ParamArchive arArray;
    SRef<ClassEntry> array = ar.OpenArray(name);
    array->ReserveArrayElements(value.GetCount());
    for (int i=0; i<value.Size(); i++)
    {
      if (value.Get(i))
      {
        array->AddValue(i);
      }
    }
    ar.CloseArray(array);
  }
  else
  {
    if (ar.GetPass() != ParamArchive::PassFirst) return LSError(0);
    SRef<ClassEntry> array = ar.OpenArray(name);
    if (!array)
    {
      value.Clear();
      return LSError(0);
    }
    int n = array->GetSize();
    for (int i=0; i<n; i++)
    {
      SRef<ClassArrayItem> item = (*array)[i];
      value.Set(int(*item),true);
    }
  }
  return LSError(0);
}

LSError TargetNormal::Serialize(ParamArchive &ar)
{
  base::Serialize(ar);

  if (ar.IsLoading() && ar.GetPass() == ParamArchive::PassFirst) Init();

  CHECK(ar.SerializeRef("idSensor", idSensor, 1))
  CHECK(ar.SerializeRef("idKiller", idKiller, 1))

  CHECK(SerializeBoolArray(ar, "idSeen", _idSeen, 1))
  
  if (ar.IsLoading())
  {
    // always make sure idSensor is between those who can see
    // - this makes sure loading old save with no _idSeen works fine
    if (idSensor && idSensor->GetUnit())
    {
      _idSeen.Set(idSensor->GetUnit()->ID(),true);
    }
  }


  CHECK(::Serialize(ar, "position", position, 1))
  if (ar.IsLoading())
  {
    snappedPosition = VZero;
    UpdateSnappedPosition(); //align position to grid
  }
  CHECK(::Serialize(ar, "dir", dir, 1, VForward))
  CHECK(::Serialize(ar, "posErrorDirection", posErrorDirection, 1, VZero))
  CHECK(::Serialize(ar, "speed", speed, 1, VZero))
  CHECK(ar.SerializeEnum("side", side, 1, TSideUnknown))
  CHECK(ar.Serialize("sideChecked", sideChecked, 1, true))
  CHECK(::Serialize(ar, "type", type, 1))

  CHECK(ar.Serialize("spotability",spotability,1,4.0));
  CHECK(::Serialize(ar, "spotabilityTime",spotabilityTime,1));
  CHECK(ar.Serialize("accuracy",accuracy,1,4.0));
  CHECK(ar.Serialize("sideAccuracy",sideAccuracy,1,4.0));
  
  CHECK(ar.Serialize("posAccuracy",posAccuracy,1,0));
  CHECK(::Serialize(ar,"posAccuracyTime",posAccuracyTime,1,TIME_MIN));

  CHECK(::Serialize(ar,"delay",delay,1, Time(0)));
  CHECK(::Serialize(ar,"delaySensor",delaySensor,1, Time(0)));
  CHECK(::Serialize(ar,"delayVehicle",delaySensor,1, Time(0)));
  CHECK(::Serialize(ar,"lastSeen",lastSeen,1, TIME_MIN));

  CHECK (ar.Serialize("isKnown",_isKnown,1, false));
  CHECK (ar.Serialize("vanished",vanished,1, false));
  CHECK (ar.Serialize("destroyed",destroyed,1, false));
  CHECK (ar.Serialize("reactedToDestruction",reactedToDestruction,1, false));
  CHECK (ar.Serialize("hasDisclosedUs",hasDisclosedUs,1, false));
  CHECK (::Serialize(ar,"timeReported",timeReported,1, TIME_MIN));
  CHECK (::Serialize(ar,"posReported",posReported,1, VZero));

  CHECK(ar.Serialize("damagePerMinute",damagePerMinute,1,0.0));
  CHECK(ar.Serialize("subjectiveCost",subjectiveCost,1,0.0));
  CHECK(::Serialize(ar,"timeSuppressed",_timeSuppressed,1,TIME_MIN));
  CHECK(ar.Serialize("suppressed",_suppressed,1,0.0));

  //@{ helper variables for TargetAggregation
  CHECK(::Serialize(ar, "aggrPosition", aggrPosition, 1, VZero));
  CHECK(ar.Serialize("aggrGroupCost", aggrGroupCost, 1, 0));
  CHECK(ar.Serialize("aggrIsReported", aggrIsReported, 1, false));
  CHECK(ar.Serialize("aggrPresent", aggrPresent, 1, false));
  //@}
  
  return LSOK;
}

LSError Target::Serialize(ParamArchive &ar)
{
  CHECK(ar.SerializeRef("idExact", idExact, 1))

  return LSOK;
}

TargetNormal *TargetNormal::CreateObject(ParamArchive &ar)
{
  // only normal targets are stored explicitly
  // TODO: implement Target::CreateObject instead
  RString type;
  if (ar.Serialize("targetType", type, 1, RString("group")) != LSOK) return NULL;

  if (strcmp(type, "group") == 0)
    return new TargetInGroup(NULL);

#if _ENABLE_INDEPENDENT_AGENTS

  if (strcmp(type, "agent") == 0)
    return new TargetInAgent(NULL);

#endif

  Fail("Unknown target type");
  return NULL;
}

Target *LoadTargetGhostRef(ParamArchive &ar);

Target *Target::LoadRef(ParamArchive &ar)
{
  RString type;
  if (ar.Serialize("targetType", type, 1, RString("group")) != LSOK) return NULL;

  if (strcmp(type, "minimal") == 0)
  {
    int id;
    if (ar.Serialize("id", id, 0) != LSOK) return NULL;
    // create a virtual target
    ObjectId objId;
    objId.Decode(id);
    const StaticEntityLink *link = GLandscape->FindBuildingInfo(objId);
    if (!link) return NULL;
    return link->GetVirtualTarget();
  }
  
  if (strcmp(type,"ghost")==0)
  {
    // we need to 
    return LoadTargetGhostRef(ar);
  }

  if (strcmp(type, "group") == 0)
  {
    int idGroup;
    if (ar.Serialize("idGroup", idGroup, 1, -2) != LSOK) return NULL;
    TargetSide side = TSideUnknown;
    int index;
    if (ar.SerializeEnum("side", side, 1) != LSOK) return NULL;
    if (ar.Serialize("index", index, 1) != LSOK) return NULL;
    AICenter *center = GWorld->GetCenter(side);
    if (!center) return NULL;
    AIGroup *group = NULL;
    for (int i=0; i<center->NGroups(); i++)
    {
      AIGroup *grp = center->GetGroup(i);
      if (grp && grp->ID() == idGroup)
      {
        group = grp;
        break; 
      }
    }
    if (!group) return NULL;
    if (index<0) return NULL;
    const TargetList &list=group->GetTargetList();
    return list.GetAny(index);
  }

#if _ENABLE_INDEPENDENT_AGENTS

  if (strcmp(type, "agent") == 0)
  {
    AIAgent *agent = static_cast<AIAgent *>(AIAgent::LoadRef(ar));
    if (!agent) return NULL;
    int index;
    if (ar.Serialize("index", index, 1) != LSOK) return NULL;
    const TargetList &list = agent->GetTargetList();
    return list.GetAny(index);
  }

#endif

  return NULL;
}

LSError TargetMinimal::SaveRef(ParamArchive &ar)
{
  RString type("minimal");
  CHECK(ar.Serialize("targetType", type, 1))

  // save object id only - nothing else is used
  int id = idExact ? idExact->GetObjectId().Encode() : -1;
  CHECK(ar.Serialize("id",id,0,-1));
  return LSOK;
}

/// context used for EntityAIFull::TrackTargets, CanSee

struct TargetNormal::TrackContext: public SideInfo
{
  const EntityAIType *type;

  float invAbilitySpotDistance;
  float invAbilitySpotTime;

  float maxDist;
  float irRange;
  bool notManual;
  EntityAIFull *vehicle;
  Vector3 eDir;
  Vector3 wepDir;

  float opticsFov;
  float opticsZoom;
  float cosOpt;
  /// what we can see
  float cosEye;
  /// center zone of the screen
  float cosEyeFocus;
  /// what we can see, including peripheral vision
  float cosEyePeripheral;

  float night;

  float sensitivityEar;

  bool initialize;

  float deadDetectTime;
  float nearestEnemy2;
  const TargetList *targets;

  virtual bool IsValid() const = 0;

  /// conditions for danger state detected
  virtual void OnDanger(AIBrain *me, Target *detected) = 0;
  /// dead body detected
  virtual void OnDeadDetected(AIBrain *me, AIBrain *detected) = 0;
  virtual void CheckAdditionalTargetDatabases() = 0;
};

struct AICenterSideInfo: public SideInfo
{
  AICenter *_center;

  AICenterSideInfo(AICenter *center):_center(center){}
  virtual bool IsFriendly(TargetSide side) const {return _center->IsFriendly(side);}
  virtual bool IsEnemy(TargetSide side) const {return _center->IsEnemy(side);}
  virtual bool IsNeutral(TargetSide side) const {return _center->IsNeutral(side);}
};

struct TargetInGroup::TrackContext : public TargetNormal::TrackContext
{
  AIGroup *group;
  AICenter *center;

  virtual bool IsValid() const {return group != NULL && center != NULL;}

  virtual bool SideIsFriendly(TargetSide side) const {return center->IsFriendly(side);}
  virtual bool SideIsEnemy(TargetSide side) const {return center->IsEnemy(side);}
  virtual bool SideIsNeutral(TargetSide side) const {return center->IsNeutral(side);}

  virtual void OnDanger(AIBrain *me, Target *detected);
  virtual void OnDeadDetected(AIBrain *me, AIBrain *detected);
  virtual void CheckAdditionalTargetDatabases();
};

void TargetInGroup::TrackContext::OnDanger(AIBrain *me, Target *detected)
{
  switch (me->GetCombatMode()) // see news:eg2b04$3d0$1@new_server.localdomain
  {
    case CMSafe:
    case CMAware:
  EntityAI *detectedAI = detected->idExact;
  if(
    detectedAI && group != detectedAI->GetGroup() && // not for target from my group
    !group->IsAnyPlayerGroup() // not for player controlled group
  )
  {
    me->SetDanger(DCFire, detected->AimingPosition(), detected);
  }
      break;
  }
}

void TargetInGroup::TrackContext::OnDeadDetected(AIBrain *me, AIBrain *detected)
{
  // check if unit is from my group
  if (detected && detected->GetGroup() == group)
  {
    // verify it is dead
    if (!detected->LSIsAlive())
    {
      // report that unit is dead
      // only local unit may report
      if (me->IsLocal())
      {
        // only group leader may report remote units
        if (detected->IsLocal() || me->IsGroupLeader())
        {
          group->SendUnitDown(me->GetUnit(), detected->GetUnit());
        }
      }
    }
  }
}

extern const float ExposureTimeout;

void TargetInGroup::TrackContext::CheckAdditionalTargetDatabases()
{
  PROFILE_SCOPE_EX(tgATD,ai)
  // check AICenter target list for nearest enemy (update _nearestEnemy if needed)
  for (int i=0; i<center->NTargets(); i++)
  {
    const AITargetInfo &target = center->GetTarget(i);
    if (target._vanished) continue;
    if (target._destroyed) continue;
    if (Glob.time > target._time + ExposureTimeout) continue;

    if (center->IsEnemy(target._side))
    {
      EntityAI *ai = target._idExact;
      if (ai && (ai->GetType()->GetIRTarget() || !ai->GetType()->GetLaserTarget() || !ai->GetType()->GetNvTarget()))
      {
        float dist2 = target._realPos.Distance2Inline(vehicle->FutureVisualState().Position());
        saturateMin(nearestEnemy2, dist2);
      }
    }
  }
}

const SideInfo *TargetListInGroup::GetSideInfo(AIBrain *brain) const
{
  if (!brain) return NULL;
  AIGroup *grp = brain->GetGroup();
  if (!grp) return NULL;
  return grp->GetCenter();  
}

TargetNormal::TrackContext &TargetListInGroup::InitTrackContext(AIBrain *brain)
{
  static TargetInGroup::TrackContext context;

  context.group = brain ? brain->GetGroup() : NULL;
  context.center = context.group ? context.group->GetCenter() : NULL;

  return context;
}

TargetNormal *TargetListInGroup::CreateTarget(AIBrain *brain)
{
  return new TargetInGroup(brain->GetGroup());
}

typedef MapStringToClass< Ref<TargetNormal>, RefArray<TargetNormal>, EntityAIPresentTraits> PresenceType;

/// check if given target can be tracked on radar/IR scanner/laser scanner extended range

static inline bool IsNotRadarTracked(const EntityAIType *type, const EntityAIType *vehType, const EntityAI *ai)
{
  return (
    !type->GetIRTarget() && (!type->GetLaserTarget() || type->GetLaserScanner()) ||
    !vehType->GetIRScanGround() && !ai->Airborne() && !type->GetNvTarget() && !type->GetArtilleryTarget()
    );

}


static bool DeleteNullTarget(Ref<TargetNormal> &ref, PresenceType *list)
{
  if (ref && ref->idExact==NULL)
  {
    ref.Free();
  }
  return false;
}

void TargetListInGroup::Manage( AIGroup *group )
{
  int nullsRemoved = 0;
  for( int s=0; s<AnyCount(); )
  {
    TargetNormal *tVis = GetAny(s);

    // check if information is not too old
    EntityAI *ai = tVis->idExact;
    if( !ai )
    {
      // target object disappeared
      if (tVis->IsKnown()) group->OnTargetDeleted(tVis);
      AnyDeleteAt(s);
      // note: presence is now likely to contain some reference to NULL as well
      nullsRemoved++;
      continue;
    }
    
    PackedBoolAutoArrayT< MemAllocLocal<PackedBoolArray,1> > seenMask;
    
    // check if some group vehicle can see it
    bool canSee=false;
    // check if some group vehicle which knows about the target can still see it
    bool canSeeKnow=false;
    for( int u=0; u<group->NUnits(); u++ )
    {
      AIUnit *unit=group->GetUnit(u);
      if( !unit ) continue;
      EntityAI *veh=unit->GetVehicle();
      // assume cargo units cannot see anything but the vehicle they are in
      if (unit->IsInCargo() && veh!=ai) continue;
      if (!veh)
      {
        ErrF("Unit %s has no vehicle",(const char *)unit->GetDebugName());
        // avoid further "has no vehicle" detection and possible crash inside some unprotected code
        if ( unit->GetGroup()==group ) 
          unit->ForceRemoveFromGroup();
        else
        {
          unit->ForceRemoveFromGroup();
          group->SetUnit(u, NULL);
        }
        continue;
      }
      float dist2=veh->FutureVisualState().Position().Distance2(ai->FutureVisualState().Position());
      float irRange = IsNotRadarTracked(ai->GetType(),veh->GetType(),ai) ? 0 : veh->GetType()->GetIRScanRange();

      float maxDistance2=Square(floatMax(irRange,TACTICAL_VISIBILITY));
      if( dist2<maxDistance2 )
      {
        canSee=true;
        seenMask.Set(unit->ID(),true);
        if (tVis->_idSeen.Get(unit->ID()) && unit->LSCanTrackTargets())
        {
          canSeeKnow = true;
        }
      }
    }

    //artillery targets are not required to see ()
    if(tVis && tVis->idExact && tVis->idExact->IsArtilleryTarget())
    {
      canSeeKnow = true;
      canSee=true;
    }

    // reset fields which cannot be seen
    tVis->_idSeen &= seenMask;
    
    if( !canSee )
    {
      if (tVis->IsKnown()) group->OnTargetDeleted(tVis);
      AnyDeleteAt(s);
      continue;
    }
    
    
    //Assert(tVis->_idSeen.GetCount()>0);

    if( tVis->IsKnownNoDelay() )
    {
      if (!canSeeKnow && tVis->delay>Glob.time)
      {
        // if the target is not known to whole group yet and no unit which have seen it is alive and in range,
        // we will make the target unknown again
        tVis->Forget();
      }
      // handle vanishing units
      tVis->vanished = !ai->IsInLandscape();
    }
    else
    {
      // delete unknown units that got-in something
      if( !ai->IsInLandscape() )
      {
        if (tVis->IsKnown()) group->OnTargetDeleted(tVis);
        AnyDeleteAt(s);
        continue;
      }
    }
    s++;
  }
  
  if (nullsRemoved>0)
  {
    // TODO: CleanUp member of  MapStringToClass
    // each target which was removed because idExact became NULL should still be in _presence
  }
}

#if _ENABLE_INDEPENDENT_AGENTS

struct TargetInAgent::TrackContext : public TargetNormal::TrackContext
{
  AIBrain *brain;

  virtual bool IsValid() const {return brain != NULL;}

  virtual bool SideIsFriendly(TargetSide side) const {return brain->IsFriendly(side);}
  virtual bool SideIsEnemy(TargetSide side) const {return brain->IsEnemy(side);}
  virtual bool SideIsNeutral(TargetSide side) const {return brain->IsNeutral(side);}

  virtual void OnDanger(AIBrain *me, Target *detected);
  virtual void OnDeadDetected(AIBrain *me, AIBrain *detected);
  virtual void CheckAdditionalTargetDatabases();
};

void TargetInAgent::TrackContext::OnDanger(AIBrain *me, Target *detected)
{
  switch (me->GetCombatMode())
  {
    case CMSafe:
    case CMAware:
  EntityAI *detectedAI = detected->idExact;
  if(
    detectedAI && me->GetVehicle()!=detectedAI && // not for my vehicle
    !me->IsAnyPlayer() // not for player
  )
  {
    me->SetDanger(DCFire, detected->AimingPosition(), detected);
  }
      break;
  }
}

void TargetInAgent::TrackContext::OnDeadDetected(AIBrain *me, AIBrain *detected)
{
  // no reaction yet
}

void TargetInAgent::TrackContext::CheckAdditionalTargetDatabases()
{
  // no additional target databases yet
}

const SideInfo *TargetListInAgent::GetSideInfo(AIBrain *brain) const
{
  return brain->GetAgent();
}

TargetNormal::TrackContext &TargetListInAgent::InitTrackContext(AIBrain *brain)
{
  static TargetInAgent::TrackContext context;

  context.brain = brain;

  return context;
}

TargetNormal *TargetListInAgent::CreateTarget(AIBrain *brain)
{
  return new TargetInAgent(brain->GetAgent());
}

void TargetListInAgent::Manage(AIAgent *agent)
{
  Assert(agent);
  EntityAI *veh = agent->GetVehicle();
  if (!veh)
  {
    ErrF("Agent %s has no vehicle", (const char *)agent->GetDebugName());
    return;
  }
  if (agent->IsInCargo()) return;

  for (int s=0; s<AnyCount();)
  {
    TargetNormal *tVis = GetAny(s);

    // check if information is not too old
    EntityAI *ai = tVis->idExact;
    if (!ai)
    {
      // target object disappeared
      if (tVis->_isKnown) agent->OnTargetDeleted(tVis);
      AnyDeleteAt(s);
      continue;
    }

    float dist2 = veh->FutureVisualState().Position().Distance2(ai->FutureVisualState().Position());

    float irRange = IsNotRadarTracked(ai->GetType(),veh->GetType(),ai) ? 0 : veh->GetType()->GetIRScanRange();

    float maxDistance2 = Square(floatMax(irRange, TACTICAL_VISIBILITY));
    bool canSee = dist2 < maxDistance2;

    if (!canSee)
    {
      if (tVis->_isKnown) agent->OnTargetDeleted(tVis);
      AnyDeleteAt(s);
      continue;
    }

    if (tVis->IsKnownNoDelay())
    {
      // handle vanishing units
      tVis->vanished = !ai->IsInLandscape();
    }
    else
    {
      // delete unknown units that got-in something
      if (!ai->IsInLandscape())
      {
        if (tVis->_isKnown) agent->OnTargetDeleted(tVis);
        AnyDeleteAt(s);
        continue;
      }
    }
    s++;
  }
}

#endif

TargetNormal *TargetList::GetAny(int i) const
{
  for (int c=0; c<NTgtCategory; c++)
  {
    if (i<_list[c].Size()) return _list[c].Get(i);
    i -= _list[c].Size();
  }
  Fail("Out of range");
  return NULL;
}


bool TargetList::DeleteKey(TargetNormal *tgt)
{
  EntityAI *ai = tgt->idExact;
  if (ai) _presence.Remove(ai);
  // first of all try removing from list based on tgt properties
  TgtCategory cat = tgt->category;
  if (_list[cat].DeleteKey(tgt)) return true;
  Fail("Target to delete not found by category");
  for (int i=0; i<NTgtCategory; i++)
  {
    if (_list[i].DeleteKey(tgt)) return true;
  }
  return false;
}

void TargetList::AnyDeleteAt(int i)
{
  for (int c=0; c<NTgtCategory; c++)
  {
    if (i<_list[c].Size())
    {
      TargetNormal *tgt = _list[c][i];
      if (tgt->idExact) _presence.Remove(tgt->idExact);
      return _list[c].Delete(i);
    }
    i -= _list[c].Size();
  }
}

void TargetList::Add(const SideInfo &who, TargetNormal *tgt)
{
  if (tgt->idExact) _presence.Add(tgt);
  TgtCategory cat = tgt->CheckCategory(who);
  // note: category does not change very often
  tgt->category = cat;
  _list[cat].Add(tgt);
}
void TargetList::Clear()
{
  for (int c=0; c<NTgtCategory; c++)
  {
    _list[c].Clear();
  }
  _presence.Clear();
}


void TargetList::CompactIfNeeded()
{
  _list[TgtCatEnemy].CompactIfNeeded(2,8);
  _list[TgtCatEnemyUnknown].CompactIfNeeded(2,8);
  _list[TgtCatFriendly].CompactIfNeeded(2,8);
  
  // optimize performs rounding to pow2 on its own, no need to check overhead here
  _presence.Optimize();
}

LSError TargetList::Serialize(ParamArchive &ar, const RStringB &name, int minVersion)
{
  // we want to serialize one list only
  if (ar.IsSaving())
  {
    RefArray<TargetNormal,MemAllocLocal<Ref<TargetNormal>,512> > list;
    for (int i=0; i<NTgtCategory; i++)
    {
      list.Merge(_list[i]);
    }
    // if we want to do this, we need to perform it prior serialization,
    // otherwise indices of targets in target list will change and saved references to targets will become invalid
    /*
    // we have to avoid saving references to objects which are not serialized
    struct IgnoreNonSaved
    {
      bool operator () (const TargetNormal *tgt) const
      {
        return tgt->idExact && !tgt->idExact->MustBeSaved();
      }
    };
    list.ForEachWithDelete(IgnoreNonSaved());
    */
    CHECK(ar.Serialize(name,list,minVersion));
    CHECK(_aggrTargets.Serialize(ar));
    return LSOK;
  }
  else
  {
    if (ar.GetPass()==ParamArchive::PassFirst)
    {
      for (int i=0; i<NTgtCategory; i++)
      {
        _list[i].Clear();
      }
      _presence.Clear();
      _aggrTargets.ClearGroups();
    }
    CHECK(ar.Serialize(name,_list[TgtCatEnemy],minVersion));
    CHECK(_aggrTargets.Serialize(ar));
    if (ar.GetPass()==ParamArchive::PassSecond)
    {
      // rebuild the hash table
      // force category check
      _presence.Reserve(_list[TgtCatEnemy].Size());
      for (int i=0; i<_list[TgtCatEnemy].Size(); i++)
      {
        TargetNormal *tgt = _list[TgtCatEnemy][i];
        if (!tgt) continue;
        const SideInfo *info = tgt->GetOwnerSideInfo();
        if (info) tgt->category = tgt->CheckCategory(*info);
        if (tgt->idExact) _presence.Add(tgt);
      }
      DistributeTargets(TgtCatEnemy);
    }
    return LSOK;
  }
}

void TargetList::AggregatedTargets::AggregatedGroup::UpdatePosition(int index, float tgtNewCost)
{ // index into _toReport, as nothing inside reported can be changed
  TargetNormal *tgt = _targets[index];
  float costAll = _toReportCost+_reportedCost;
  /*
    NewPosition as subjectiveCost weighted target group center
    We also need to remove the former position from this weighted center

              SUM_t[tPos*tCost] - tgtOldPos*tgtOldCost + tgtNewPos*tgtNewCost
    newPos = -----------------------------------------------------------------
                       SUM_t[tCost] - tgtOldCost + tgtNewCost
  */
  Vector3 sumtPosXtCost = position * costAll;
  Vector3 newPos = (sumtPosXtCost - tgt->aggrPosition*tgt->aggrGroupCost + tgt->position*tgtNewCost)
                 / (costAll - tgt->aggrGroupCost + tgtNewCost);
  float posDiff = newPos.DistanceXZ(position);
  radius += posDiff; // circle(newPos,radius+posDiff) contains previous circle(position,radius)
  position = newPos;
  // update target position as reported (to prevent AIGroup::CreateTargetList to add it again)
  tgt->posReported = tgt->position;
  tgt->aggrPosition = tgt->position; //TODO: for the AIGroup::CreateTargetList purposes use the aggrPosition instead of posReported?!
}

void TargetList::AggregatedTargets::AggregatedGroup::Add(TargetNormal *target)
{
  // update position and radius
  float costAll = _toReportCost+_reportedCost+target->aggrGroupCost;
  // newPosition as subjectiveCost weighted target group center
  Vector3 newPos = ((_toReportCost+_reportedCost)*position + target->aggrGroupCost*target->position) / costAll;
  float posDiff = newPos.DistanceXZ(position);
  radius += posDiff; // circle(newPos,radius+posDiff) contains previous circle(position,radius)
  position = newPos;
  _targets.Add(target);
  // update cost
  if (target->aggrIsReported)
  {
    _reportedCost += target->aggrGroupCost;
    // update target position as reported (to prevent AIGroup::CreateTargetList to add it again)
    target->posReported = target->aggrPosition = target->position;
  }
  else
  {
    _toReportCost += target->aggrGroupCost;
    target->posReported = target->aggrPosition = target->position;
  }
}

void TargetList::AggregatedTargets::AggregatedGroup::Remove(int ix, TargetList::AggregatedTargets *parent)
{
  Ref<TargetNormal> target = _targets[ix]; 
  if (!target) // can happen after Load
  {
    _targets.DeleteAt(ix);
    return;
  }
  float costAll = _toReportCost+_reportedCost;
  if (target->aggrIsReported)
    _reportedCost -= target->aggrGroupCost;
  else
    _toReportCost -= target->aggrGroupCost;
  // remove it
  _targets.DeleteAt(ix);
  // update position (radius can be used the same)
  float cost = _toReportCost+_reportedCost;
  if (cost>0.001f) //the group is empty otherwise
    position = (costAll*position - target->aggrGroupCost*target->aggrPosition) / cost;
  else
  { // possibly split some other group to use one more slot
    parent->SetEmptySlotAvailable();
    position = VZero;
  }
}

void TargetList::AggregatedTargets::SetEmptySlotAvailable()
{
  // the split is rather expensive operation - do it only when there is a chance to succeed
  // One slot is fresh empty. But if there is another slot already empty, there is no need to try splitting, as
  // targets were added into full slot rather than to empty one (aggregation of neighboring targets)
  int count = 0;
  for (int i=0; i<NAggrGroups; i++)
  {
    if (!_targetGroup[i].TargetsCount()) count++;
  }
  if ( count>1 )
    _tryToSplitGroup = true;
}

void TargetList::AddAggrTarget(TargetNormal *vtar, float cost)
{ 
  //LogF("TargetList::AddAggrTarget %s",cc_cast(vtar->GetDebugName()));
  vtar->timeReported = Glob.time;
  //vtar->posReported is updated inside AddTarget
  _aggrTargets.AddTarget(vtar, cost);
#if DIAG_AGGREGATED_TARGETS
  if (_aggrTargets._debugMe) _aggrTargets.DumpState(NULL);
#endif
}

void TargetList::ConsiderAggrTargetsReported()
{
  _aggrTargets.MakeAllReported();
}

// AggregatedTargets implementation
void TargetList::AggregatedTargets::AddTarget(TargetNormal *target, float cost)
{
#if DIAG_AGGREGATED_TARGETS
  if (_debugMe) 
    LogF("  .AGGR. AddTarget");
#endif
  // check whether the target is already present (if so, move it possibly into _toReport list)
  for (int gn=0; gn<NAggrGroups; gn++)
  {
    AggregatedGroup &grp = _targetGroup[gn];
    // search
    for (int i=0, siz=grp._targets.Size(); i<siz; i++)
    {
      if (grp._targets[i]==target)
      {
        if (target->aggrIsReported)
        { // already reported - change it to not reported (update)
          // the position should be updated too
          grp.UpdatePosition(i, cost);
          //make it unreported
          target->aggrIsReported = false;
          // update costs
          grp._reportedCost -= target->aggrGroupCost;
          target->aggrGroupCost = cost;
          grp._toReportCost += cost;
          // test it for report
        }
        else
        { // not reported yet
          grp.UpdatePosition(i, cost);
          // update costs
          grp._toReportCost -= target->aggrGroupCost;
          target->aggrGroupCost = cost;
          grp._toReportCost += cost;
          // test it for report
        }
        return;
      }
    }
  }
  
  // set the cost (it is new target, so aggrGroupCost is not set yet)
  target->aggrGroupCost = cost;

  // choose the aggregated group (possibly empty) to add it and possibly reorganize groups
  AddNewTarget(target);
}

void TargetList::AggregatedTargets::MakeAllReported()
{
  for (int i=0; i<NAggrGroups; i++)
  {
    AggregatedGroup &grp = _targetGroup[i];
    for (int j=0; j<grp._targets.Size(); j++)
    {
      // update costs
      grp._reportedCost += grp._toReportCost;
      grp._toReportCost = 0;
      // set as reported
      grp._targets[j]->aggrIsReported = true;
    }
  }
}

int TargetList::AggregatedTargets::FindNearestTargetGroup(TargetNormal *target)
{
  float best2 = 1e10;
  int bestIx = 0;
  for (int i=0; i<NAggrGroups; i++)
  {
    // empty aggregated group is considered to be at the effectiveBattle distance of the target
    // So the aggregation is possible (for targets close enough) even there are still empty slots
    float dist2 = _targetGroup[i].TargetsCount() ? 
      target->position.DistanceXZ2(_targetGroup[i].position) : Square(GWorld->GetMartaEffectiveBattleDistance(target->GetType()));
    if (dist2 < best2)
    {
      bestIx = i;
      best2 = dist2;
    }
  }
  return bestIx;
}

void TargetList::AggregatedTargets::AddNewTarget(TargetNormal *target)
{
  target->aggrIsReported = false;
  target->aggrPosition = target->position;
  // find the nearest group
  int nearestIx = FindNearestTargetGroup(target);
  AggregatedGroup &tgtGrp = _targetGroup[nearestIx];
  // compute what would happen to position and radius if the target is added directly into tgtGrp;
  float costAll = tgtGrp.GetGRCost()+target->aggrGroupCost;
  Vector3 newPos = (tgtGrp.GetGRCost()*tgtGrp.position + target->aggrGroupCost*target->position) / costAll;

  // Are there any targets in tgtGrp closer to another group? Move them.
  MoveTargetsToNearestGroup(tgtGrp, newPos, target, nearestIx);

  // compute newRadius precisely
  float rad2 = 1e10;
  for (int i=0; i<tgtGrp._targets.Size(); i++)
  {
    saturateMin(rad2, newPos.DistanceXZ2(tgtGrp._targets[i]->aggrPosition));
  }
  saturateMin(rad2, newPos.DistanceXZ2(target->position));
  float newRadius = sqrt(rad2);

  // is newRadius too large, that union of other groups is better? Join them then.
  if (newRadius - tgtGrp.radius > 0)
  {
    int ugr1=0, ugr2=0;
    float unionRadiusEst=newRadius;
    for (int gr1=0; gr1<NAggrGroups-1; gr1++)
    {
      const AggregatedGroup &g1 = _targetGroup[gr1];
      if (!g1.TargetsCount()) continue; // skip empty groups
      for (int gr2=gr1+1; gr2<NAggrGroups; gr2++)
      {
        // compute unionRadius estimation
        // determine unionPos
        const AggregatedGroup &g2 = _targetGroup[gr2];
        if (!g2.TargetsCount()) continue; // skip empty groups
        float unionCost = g1.GetGRCost()+g2.GetGRCost();
        saturateMax(unionCost, FLT_MIN);
        Vector3 unionPos = (g1.position*g1.GetGRCost() + g2.position*g2.GetGRCost())
                         / unionCost;
        float unionRad = g1.radius + unionPos.DistanceXZ(g1.position);
        saturateMax(unionRad, g2.radius + unionPos.DistanceXZ(g2.position));
        if (unionRad < unionRadiusEst)
        {
          unionRadiusEst = unionRad;
          ugr1 = gr1; ugr2 = gr2;
        }
      }
    }
    if (unionRadiusEst < newRadius)
    { // ugr1 and ugr2 points to groups which can be joined
      int grIx = _targetGroup[ugr1].TargetsCount()<_targetGroup[ugr2].TargetsCount() ? ugr1 : ugr2;
      AggregatedGroup &joinGrp = _targetGroup[grIx];
      RefArray<TargetNormal> joinTargets = joinGrp._targets.GetRefArray();
      // make the grIx group co contain ONLY new target
      joinGrp._targets.Clear();
      joinGrp._targets.Add(target);
      joinGrp._reportedCost = 0;
      joinGrp._toReportCost = target->aggrGroupCost;
      joinGrp.position = target->position;
      joinGrp.radius = 0;
      // add all targets from joinTargetR and jointTargetTR to their nearest groups
      MoveTargetsToNearestGroup(joinTargets);
      return; //target is added already
    }
  }
  // add the target finally
  tgtGrp._targets.Add(target);
  // update position, radius and cost
  tgtGrp.position = newPos;
  tgtGrp.radius = newRadius; //this is precise value
  tgtGrp._toReportCost += target->aggrGroupCost;
}

void TargetList::AggregatedTargets::MoveTargetsToNearestGroup(AggregatedGroup &tgtGrp, Vector3 &newPos, TargetNormal *target, int ignoreGrp)
{
  RefArray<TargetNormal> &targets = tgtGrp._targets.GetRefArray();
  for (int i=0; i<targets.Size(); i++)
  {
    TargetNormal *tgt = targets[i];
    float dist2 = tgt->aggrPosition.DistanceXZ2(newPos);
    float bestDist2grp = 1e10;
    int bestIxGrp = 0;
    for (int gi=0; gi<NAggrGroups; gi++)
    {
      //all groups except for tgtGrp and emptyGroups
      if (gi==ignoreGrp || !_targetGroup[gi].TargetsCount()) continue;
      float dist2grp = tgt->aggrPosition.DistanceXZ2(_targetGroup[gi].position);
      if (dist2grp < bestDist2grp)
      {
        bestDist2grp = dist2grp;
        bestIxGrp = gi;
      }
    }
    // prevent frequent target migration by hysteresis
    const float AggrMigCoef = 1.1f; //10%
    if (bestDist2grp*AggrMigCoef < dist2)
    { // move target from tgtGrp into _targetGroup[bestIxGrp]
      MoveTarget(tgtGrp,i,bestIxGrp);
      i--; //i refers to different target now
      // update newPos
      float costAll = tgtGrp.GetGRCost()+target->aggrGroupCost;
      newPos = (tgtGrp.GetGRCost()*tgtGrp.position + target->aggrGroupCost*target->aggrPosition) / costAll;
    }
  }
}

void TargetList::AggregatedTargets::MoveTargetsToNearestGroup(RefArray<TargetNormal> &targets)
{
  for (int i=0; i<targets.Size(); i++)
  {
    TargetNormal *tgt = targets[i];
    float bestDist2grp = 1e10;
    int bestIxGrp = 0; //choose some (even empty)
    for (int gi=0; gi<NAggrGroups; gi++)
    {
      //all groups except the empty ones
      if (!_targetGroup[gi].TargetsCount()) continue;
      float dist2grp = tgt->aggrPosition.DistanceXZ2(_targetGroup[gi].position);
      if (dist2grp < bestDist2grp)
      {
        bestDist2grp = dist2grp;
        bestIxGrp = gi;
      }
    }
    _targetGroup[bestIxGrp].Add(tgt);
  }
}

void TargetList::AggregatedTargets::MoveTarget(AggregatedGroup &tgtGrpFrom, int ixFrom, int groupIxTo)
{
  TargetNormal *tgt = tgtGrpFrom._targets[ixFrom];
  //NOTE: ADD first, REMOVE later! (because of possible refcount==1 before the operation!)
  _targetGroup[groupIxTo].Add(tgt);
  tgtGrpFrom.Remove(ixFrom, this);
  // But Remove has cleared the _isAggregated flag which still should be true
  tgt->aggrPresent = true;
}

void TargetList::AggregatedTargets::RecomputePositionAndRadius()
{
  for (int i=0; i<NAggrGroups; i++)
  {
    AggregatedGroup &grp = _targetGroup[i];
    if (!grp.TargetsCount()) 
    {
      grp._toReportCost = grp._reportedCost = 0;
      continue; // skip empty group
    }
    Vector3 sumtPosXCost = VZero;
    // update and SUM position based on _targets
    grp._reportedCost = grp._toReportCost = 0;
    for (int j=0; j<grp._targets.Size(); j++)
    {
      TargetNormal *tgt = grp._targets[j];
      if (tgt->aggrIsReported)
        grp._reportedCost += tgt->aggrGroupCost;
      else
        grp._toReportCost += tgt->aggrGroupCost;
      sumtPosXCost += tgt->aggrGroupCost * tgt->aggrPosition;
    }
    // update position (using both _reported and _toReport targets)
    float sumCost = grp._reportedCost + grp._toReportCost;
    saturateMax(sumCost, FLT_MIN);
    grp.position = sumtPosXCost / sumCost;
    // update radius
    float rad2 = 0;
    for (int j=0; j<grp._targets.Size(); j++)
    {
      TargetNormal *tgt = grp._targets[j];
      float dist2 = grp.position.DistanceXZ2(tgt->aggrPosition);
      if (dist2 > rad2) rad2 = dist2;
    }
    grp.radius = sqrt(rad2);
  }
}

void TargetList::AggregatedTargets::GlobalUpdate(AIGroup *group)
{
#if DIAG_AGGREGATED_TARGETS
  if (_debugMe) LogF("  .AGGR. GlobalUpdate");
#endif
  // update (recompute) group compactness
  group->UpdateMartaType();
  group->UpdateCompactness();
  RemoveObsoleteTargets(group);
  if (_tryToSplitGroup)
  {
    _tryToSplitGroup = false;
    TryToSplitGroup();
  }
  RecomputePositionAndRadius();
}

void TargetList::AggregatedTargets::TryToSplitGroup()
{
  // try to split the largest group only
  int grpIx = 0;
  int maxCount = 0;
  for (int i=0; i<NAggrGroups; i++)
  {
    if (_targetGroup[i].TargetsCount()>maxCount)
    {
      maxCount = _targetGroup[i].TargetsCount();
      grpIx = i;
    }
  }
  if ( maxCount>1 ) //at least two targets needed for split
  {
#if DIAG_AGGREGATED_TARGETS
    LogF("... TryToSplitGroup");
#endif
    AggregatedGroup &grp = _targetGroup[grpIx];
    // select the representatives of the new aggregations (couple of most distant targets)
    int c1 = -1, c2 = -1;
    float maxDist2 = -FLT_MAX; // FIX crash: zero was not sufficient, as DistanceXZ2 happened to be zero
    for (int i=1; i<maxCount; i++)
    {
      Vector3 &pos1 = grp._targets[i]->aggrPosition;
      for (int j=0; j<i; j++)
      {
        float dist2 = pos1.DistanceXZ2(grp._targets[j]->aggrPosition);
        if (dist2 > maxDist2)
        {
          c1 = i; c2 = j; maxDist2 = dist2; // c1 > c2 holds!
        }
      }
    }
    Assert(c1 > c2 && c2 >= 0);
    // c1 and c2 points to targets which could possibly starts its own aggregated groups
    float minBattleDist = min( GWorld->GetMartaEffectiveBattleDistance(grp._targets[c1]->GetType()), GWorld->GetMartaEffectiveBattleDistance(grp._targets[c2]->GetType()) );
    if ( maxDist2 > Square(minBattleDist) )
    { // split is feasible
#if DIAG_AGGREGATED_TARGETS
      LogF("((DumpState before Group Split");
      if (_debugMe) DumpState(NULL);
#endif
      // find the empty group
      int grpIx2 = 0;
      for (int i=0; i<NAggrGroups; i++)
      {
        if (!_targetGroup[i].TargetsCount())
        {
          grpIx2 = i;
          break;
        }
      }
      AggregatedGroup &grp2 = _targetGroup[grpIx2];
      // put the splitTargets outside the group first
      RefArray<TargetNormal> splitTargets = grp._targets.GetRefArray();
      grp._targets.Clear();
      grp.StartNewGroup(splitTargets[c1]);
      grp2.StartNewGroup(splitTargets[c2]);
      splitTargets.DeleteAt(c1); //c1 > c2 holds!
      splitTargets.DeleteAt(c2);
      // add all other targets to their nearest groups
      MoveTargetsToNearestGroup(splitTargets);
      // estimation of the radius is not enough after upper bunch 
      RecomputePositionAndRadius();
#if DIAG_AGGREGATED_TARGETS
      if (_debugMe) DumpState(NULL);
      LogF("^^^ DumpState after Group Split))");
#endif
    }
  }
}

void TargetList::AggregatedTargets::RemoveObsoleteTargets(AIGroup *group)
{
#if DIAG_AGGREGATED_TARGETS
  if (_debugMe) LogF("  .AGGR. RemoveObsoleteTargets");
#endif
  for (int i=0; i<NAggrGroups; i++)
    _targetGroup[i].RemoveObsoleteTargets(group, this);
}

void TargetList::AggregatedTargets::AggregatedGroup::StartNewGroup(TargetNormal *tgt)
{
  _targets.Add(tgt); //add the first
  if (tgt->aggrIsReported)
  {
    _reportedCost = tgt->aggrGroupCost;
    _toReportCost = 0;
  }
  else
  {
    _toReportCost = tgt->aggrGroupCost;
    _reportedCost = 0;
  }
  position = tgt->position;
  radius = 0;
}

void TargetList::AggregatedTargets::AggregatedGroup::RemoveObsoleteTargets(AIGroup *grp, TargetList::AggregatedTargets *parent)
{
  AICenter *center = grp ? grp->GetCenter() : NULL;
  if (!center) return;
  // process toReport
  for (int i=0; i<_targets.Size(); i++)
  {
    TargetNormal *tgt = _targets[i];
    // Note: it happened NULL tgt has occurred after Load
    if ( !tgt || center->IsFriendly( tgt->GetSide() ) || !tgt->idExact )
    {
#define DEBUG_AGGR_TGT_REMOVE 0
#if DEBUG_AGGR_TGT_REMOVE
      RString name = (tgt->idExact ? tgt->idExact->GetDebugName() : "???") + tgt->GetType()->GetTextSingular();
      if (grp->GetDebugName()=="B 1-1-A" && (tgt->GetType()->GetTextSingular()=="officer" || tgt->GetType()->GetTextSingular()=="man"))
        _asm nop;
      LogF("*** Group %s: Target %s Removed for %s reason!", cc_cast(grp->GetDebugName()), cc_cast(name), !tgt->idExact ? "!idExact" : "isFriendly");
#endif
      Remove(i, parent);
      i--; //different item under this item, process again
    }
  }
}

bool TargetList::AggregatedTargets::SomethingToReport() const
{
  for (int i=0; i<NAggrGroups; i++)
    if (_targetGroup[i]._toReportCost>0) return true;
  return false;
}

// helper to group the targets with the same sensor together by QSort
struct AggrSensorTarget
{
  Ref<TargetNormal> target;
  int targetIx; // index into corresponding _toReport (only reported items can be moved into _reported array)
  AIUnit *from;
  
  AggrSensorTarget() {}
  AggrSensorTarget(TargetNormal *tg, AIUnit *unit, int ix) : target(tg), from(unit), targetIx(ix) {}

  ClassIsMovable(AggrSensorTarget);
};

struct CompareAggrSensorTarget
{
  int operator () (const AggrSensorTarget *t0, const AggrSensorTarget *t1) const
  {
    return sign(t1->from->ID() - t0->from->ID());
  }
};

#if DIAG_AGGREGATED_TARGETS
void TargetList::AggregatedTargets::DumpState(const AIGroup *group)
{
  LogF("*** AggregatedTargets::DumpState %s ***", group ? cc_cast(group->GetDebugName()) : "NULL");
  for (int i=0; i<NAggrGroups; i++)
  {
    if (_targetGroup[i].TargetsCount())
    {
      RString aggregatedNameRep;
      RString aggregatedNameToRep;
      int repSize=0, toRepSize=0;
      for (int j=0; j<_targetGroup[i]._targets.Size(); j++)
      {
        if (_targetGroup[i]._targets[j]->idExact)
        {
          if (!_targetGroup[i]._targets[j]->aggrIsReported)
          {
            if (toRepSize++) aggregatedNameToRep = aggregatedNameToRep + ", ";
            aggregatedNameToRep = aggregatedNameToRep + _targetGroup[i]._targets[j]->GetType()->GetTextSingular();
          }
          else
          {
            if (repSize++) aggregatedNameRep = aggregatedNameRep + ", ";
            aggregatedNameRep = aggregatedNameRep + _targetGroup[i]._targets[j]->GetType()->GetTextSingular();
          }
        }
      }
      LogF("  %d: to report %d(%.1f) {%s}", i, toRepSize, _targetGroup[i]._toReportCost, cc_cast(aggregatedNameToRep));
      LogF("     reported  %d(%.1f) {%s}",    repSize, _targetGroup[i]._reportedCost, cc_cast(aggregatedNameRep));
    }
  }
}
#endif

struct CompareInt
{
  int operator () (const int *a, const int *b) const
  { // reversed order, bigger first
    return *b-*a;
  }
};

// Report targets (called after some delay)
void TargetList::AggregatedTargets::DoReport(AIGroup *group)
{
#if DIAG_AGGREGATED_TARGETS
  if (_debugMe) LogF("  .AGGR. DoReport");
#endif
  if (!SomethingToReport()) return;
  if (group->UnitsCount() <= 1) return; //nobody is interested in

  AICenter *center = group ? group->GetCenter() : NULL;
  for (int gi=0; gi<NAggrGroups; gi++)
  { // make separate report for each aggrGroup
    AggregatedGroup &grp = _targetGroup[gi];
    AutoArray<AggrSensorTarget, MemAllocLocal<AggrSensorTarget, 64> > targets;
    // moreover report for each Sensor separately 
    // select reporting unit and collect all targets the unit should report (include these without sensor)
    AIUnit *someFrom = NULL;
    bool noFromTargetExists = false;
    for (int i=0; i<grp._targets.Size(); i++)
    { // collect all targets along with the sensor
      // remove possible Friendly first
      TargetNormal *vtar = grp._targets[i];
      if (vtar->aggrIsReported) continue; // consider only report adepts
      if ( center && center->IsFriendly( vtar->GetSide() ) || !vtar->idExact )
      {
#if DEBUG_AGGR_TGT_REMOVE
        RString name = (vtar->idExact ? vtar->idExact->GetDebugName() : "???") + vtar->GetType()->GetTextSingular();
        if (group->GetDebugName()=="B 1-1-A" && (vtar->GetType()->GetTextSingular()=="officer" || vtar->GetType()->GetTextSingular()=="man"))
          _asm nop;
        LogF("*** Group %s: Target %s Removed for %s reason!", cc_cast(group->GetDebugName()), cc_cast(name), !vtar->idExact ? "!idExact" : "isFriendly");
#endif
        grp.Remove(i, this);
        i--; //there is different item under this item, process again
        continue;
      }
      if (vtar->idExact->Invisible()) continue; //never report invisible targets
      AIBrain *agent = vtar->idSensor;
      AIUnit *sensor = agent ? agent->GetUnit() : NULL;
      if (sensor) someFrom = sensor; //remember some sensor to use it for targets without sensor later
      else noFromTargetExists = true;
      targets.Add(AggrSensorTarget(vtar, sensor, i));
    }
    // Targets are prepared, but we do not know whether there are sufficient changes
    // inside _toReport to shout it using Radio Target Report.
    // Report only relevant changes
    saturateMin(_reportChangeFactorIx, _reportTargetsChangeFactors.Size()-1);
    float RelevancyThreshold = _reportTargetsChangeFactors[_reportChangeFactorIx];
    _reportChangeFactorIx++; // old value used, we can proceed to next value
    float reportedCost = grp._reportedCost;
    saturateMax(reportedCost, _reportTargetsEmptyGroupCost);
    if (grp.GetGRCost() / reportedCost < RelevancyThreshold) continue; //do not report little changes
    // something should be reported otherwise
    if (targets.Size())
    {
      if (noFromTargetExists)
      { // we need to set the From for all such targets before QSorting (uses from->ID())
        if (!someFrom)
        { // need to find some
          for (int i=0; i<group->UnitsCount(); i++)
          {
            AIUnit *unit = group->GetUnit(i);
            if (unit && unit->LSCanSpeak())
            {
              someFrom = unit;
              break;
            }
          }
        }
        if (someFrom) 
        {
          for (int i=0; i<targets.Size(); i++)
          {
            if ( !targets[i].from ) targets[i].from = someFrom;
          }
        }
      }
      if (someFrom) // if no unit can do the report, skip it
      { 
#if DIAG_AGGREGATED_TARGETS && 0
        if (_debugMe) LogF("Preparing report with _reportChangeFactorIx==%d", _reportChangeFactorIx-1);
#endif
        _reportChangeFactorIx = 0; // when something is reported the conditions for other reports should be strict again
        // group the targets with the same sensor
        ::QSort(targets,CompareAggrSensorTarget());

        //LogF("*** TARGETs Report Start ***");
        for( int targetIx=0; targetIx<targets.Size(); )
        {
          int next = targetIx+1;
          while(next<targets.Size() && targets[targetIx].from->ID()==targets[next].from->ID()) next++;

          // sent the report for targets[targetIx],...,targets[next-1]
          AIGroup::ReportTargetList reportTargets;
          for (int i=targetIx; i<next; i++)
          {
            TargetNormal *vtar = targets[i].target;
            // note: vtar->timeReported and vtar->posReported are set already
            // note: ghost targets are already excluded 
            reportTargets.Add(vtar);
            // we must update cost to test the resulting cost after report
            grp._reportedCost += vtar->aggrGroupCost;
            grp._toReportCost -= vtar->aggrGroupCost;
            vtar->aggrIsReported = true; //make it reported
          }
          //LogF("   Reporting %d targets", reportTargets.Size());
          group->SendRadioReport(reportTargets, targets[targetIx].from);

          float reportedCost = grp._reportedCost;
          saturateMax(reportedCost, FLT_MIN);
          if (grp.GetGRCost() / reportedCost < RelevancyThreshold) 
          { // FINISHED! All other targets together are not important enough to make a radio report.
            //LogF("*** TARGETs Report End (%d targets left)***", targets.Size()-next);
            break; //note: this break is always reached
          }
          // proceed next bunch of targets
          targetIx = next;
        }
      }
    }
  }
#if DIAG_AGGREGATED_TARGETS
  if (_debugMe) DumpState(group);
#endif
}

const float TargetList::AggregatedTargets::ReportDelay = 1.0f; // seconds
const float TargetList::AggregatedTargets::GlobalUpdateDelay = 7.0f; // seconds
void TargetList::AggregatedTargets::Process(AIGroup *group)
{
  if (group->UnitsCount() <= 1)
  {
    if (!_finished)
    { // after AIGroup contains one man, the aggr. groups can be cleared, but only once
      _finished = true;
      ClearGroups();
    }
    return; //nobody is interested in
  }
  _finished = false;
  if ( _reportTime <= Glob.time )
  {
    // try to DoReport each ReportDelay period. (Using RTChangeFactors the report is gradualy more and more probable.)
    _reportTime = Glob.time+ReportDelay;
    DoReport(group);
  }
  if ( _globalUpdateTime <= Glob.time )
  {
    //Random used not to call all global updates at the same frame
    _globalUpdateTime = Glob.time+GlobalUpdateDelay+GRandGen.RandomValue();
    GlobalUpdate(group);
  }
}

LSError TargetList::AggregatedTargets::Serialize(ParamArchive &ar)
{ 
  CHECK(ar.Serialize("TargetGroup", _targetGroup, 1));
  CHECK(::Serialize(ar, "ReportTime", _reportTime, 1));
  CHECK(::Serialize(ar, "GlobalUpdateTime", _globalUpdateTime, 1));
  CHECK(ar.Serialize("Finished", _finished, 1, true)); //true is most common
  return LSOK; 
}

void TargetList::AggregatedTargets::ClearGroups()
{
  for (int i=0; i<NAggrGroups; i++)
  {
    _targetGroup[i].Clear();
  }
}

void TargetList::AggregatedTargets::AggregatedGroup::Clear()
{
  _reportedCost = _toReportCost = radius = 0;
  position = VZero; 
  _targets.Clear();
}

// enable to test if results using _presence are correct

#define VERIFY_PRESENCE 0

bool TargetList::IsPresent(EntityAI *ai)
{
  const Ref<TargetNormal> &find = _presence.Get(ai);
  bool fast = _presence.NotNull(find);
  #if VERIFY_PRESENCE
    bool slow = false;
    for( int i=0; i<Size(); i++ )
    {
      if( Get(i)->idExact==ai ) {slow=true;break;}
    }
    DoAssert(fast==slow);
  #endif
  return fast;
}

const TargetNormal *TargetList::FindTarget( TargetType *ai ) const
{
  const Ref<TargetNormal> &find = _presence.Get(ai);
  TargetNormal *fast = find;
  #if VERIFY_PRESENCE
    TargetNormal *slow = NULL;
    for( int i=0; i<Size(); i++ )
    {
      if( Get(i)->idExact==ai ) {slow=Get(i);break;}
    }
    DoAssert(fast==slow);
  #endif
  return fast;
}

TargetNormal *TargetList::FindTarget( TargetType *ai )
{
  const Ref<TargetNormal> &find = _presence.Get(ai);
  TargetNormal *fast = find;
  #if VERIFY_PRESENCE
    TargetNormal *slow = NULL;
    for( int i=0; i<Size(); i++ )
    {
      if( Get(i)->idExact==ai ) {slow=Get(i);break;}
    }
    DoAssert(fast==slow);
  #endif
  return fast;
}

/*!
\patch 5156 Date 5/10/2007 by Ondra
- Fixed: AI no longer starts firing in Hold fire unless the enemy is really threatening it.
*/

static bool CheckIfDangerousToMe(EntityAI *who, EntityAI *me)
{
  // check if some weapon is aligned to fire at me
  // some preliminary tests
  // 1. are there any weapons?
  // 2. if AI, what about combat mode / behavior
  
  AIBrain *unit = who->CommanderUnit();
  if (!unit) return false;
  // when in safe mode, he poses no threat to us
  if (unit->GetCombatModeLowLevel()<=CMSafe) return false;
  return true;
}

/**
Test if unit ai is under a direct threat by who

@param me the entity being disclosed
@param who the entity disclosing

this TargetList belongs to 'who'
*/
bool TargetList::IsDisclosing(TargetType *me, EntityAI *who) const
{
  const TargetNormal *target = FindTarget(me);
  if (!target) return false;
  
  // the group is aware of us - react
  if (target->IsKnownBySome())
  {
    // check if they know our position close enough to be dangerous to us
    // they may know about us because they have seen some explosion caused by us
    // in such case there is no need to panic
    if (target->posAccuracy < me->VisibleSize(me->FutureVisualState())*10 && CheckIfDangerousToMe(who,me))
    {
      return true;
    }
  }

  if (who->CommanderUnit() && who->CommanderUnit()->IsAnyPlayer())
  {
    // apply somewhat stricter threshold than during the real simulation
    // if the enemy knows about us, the condition above should already succeed
    // if the player opened fire, DCFire or DCHit already probably disclosed the unit
    // this is just to avoid enemy staring at player
    float spotTreshold = 0.75f;
    if( target->FadingSpotability()>spotTreshold)
    {
      // we could check some more conditions for the player:
      // e.g. does he have a weapon raised?
      return true;
    }
  
  }

  return false;
}

void TargetList::AggregatedTargets::ForceReport(Time time)
{
  // force target aggregation to report the target soon
  if ( _reportTime >time)
  {
    _reportTime = time;
  }
//   if ( _globalUpdateTime > time)
//   {
//     _globalUpdateTime = time;
//   }
}

struct EntityAIFull::AddNewTargetContext
{
  float maxVisibility2;
  float nearestEnemyDist2;
  AIBrain *brain;
  const SideInfo &_info;
  
  explicit AddNewTargetContext(const SideInfo &info):_info(info){}
};

/** Typical usage:
  when one particular target changed location abruptly,
  we wan to update target lists for nearby enemies
*/

void EntityAIFull::AddNewTarget(TargetList &res, bool initialize, EntityAI *obj)
{
  PROFILE_SCOPE_EX(tgNew,ai);

  AIBrain *unit = CommanderUnit();
  if (!unit) return;

  const SideInfo *info = res.GetSideInfo(unit);
  if (!info) return;
  
  // interested in enemies only?
  // can we bear with slight delay for other units?

  
  // first do a basic test for a distance
  // if the unit is quite far, there is no need to hurry and we can wait for a regular update
  // we want to simulate only TrackNearTargets
  // see also other nearTargetLimit
  const float nearTargetLimit = 100.f;
  
  float dist2=obj->FutureVisualState().Position().Distance2Inline(FutureVisualState().Position());
  if( dist2>Square(nearTargetLimit) ) return;
  
  AddNewTargetContext ctx(*info);

  float irRange=GetType()->GetIRScanRange();
  ctx.maxVisibility2 = Square(floatMax(TACTICAL_VISIBILITY,irRange));

  ctx.brain = unit;

  // update the target list
  ctx.nearestEnemyDist2 = unit->GetNearestEnemyDist2();

  AddNewTarget(res,initialize,ctx,obj);

  ctx.brain->SetNearestEnemyDist2(ctx.nearestEnemyDist2);
}

void EntityAIFull::AddNewTarget(TargetList &res, bool initialize, AddNewTargetContext &ctx, Entity *obj)
{
  // TODO: perform unit traversal here, group together units which are close to each other
  // and have the same scanning range
  float dist2=obj->FutureVisualState().Position().Distance2Inline(FutureVisualState().Position());
  if( dist2>ctx.maxVisibility2 ) return;
  // note: max range may be different for different vehicles

  // ignore temporary vehicles (tracks...)
  EntityAI *ai=dyn_cast<EntityAI,Entity>(obj);
  if( !ai ) return;

  const EntityAIType *type = ai->GetType();
  if(dist2>Square(TACTICAL_VISIBILITY))
  {
    // far target - check if it can be seen by radar
    if (IsNotRadarTracked(type,GetType(),ai))
    {
      return;
    }
  }

  // record all (even invisible)  vehicles
  // out vehicles - if not in list, no need to add them
  if (!ai->IsInLandscape()) return;
  // check if it is enemy
  if (ctx.brain->IsEnemy(ai->GetTargetSide()))
  {
    saturateMin(ctx.nearestEnemyDist2,dist2);
  }

  if (res.IsPresent(ai))
  {
    return;
  }

  TargetNormal *target = res.CreateTarget(ctx.brain);
  target->Init(ai);
  res.Add(ctx._info,target);
}

void EntityAIFull::ReactToGetIn(TargetList &res, EntityAI *obj, Transport *trans)
{
  // if the target side of the transport and entity is the same, there is nothing to do
  // this is a common case - units boarding friendly vehicles
  // TODO: this makes the function useless. When units boards a vehicle, it changes its side to its own
  if (obj->GetTargetSide()==trans->GetTargetSide()) return;

  // if the unit is too far, we do not perform any reaction
  // if the target has no IR signature, we may reduce the max. distance
  float maxVisibility2 = (
    obj->GetType()->GetIRTarget() ?
    Square(floatMax(TACTICAL_VISIBILITY,GetType()->GetIRScanRange())):
    Square(TACTICAL_VISIBILITY)
  );

  float dist2=trans->FutureVisualState().Position().Distance2Inline(FutureVisualState().Position());
  if( dist2>maxVisibility2 ) return;


  AIBrain *unit = CommanderUnit();
  if (!unit) return;

  // if the entity getting in is know, we cannot do anything
  TargetNormal *objTgt = res.FindTarget(obj);
  if (!objTgt || !objTgt->IsKnownBy(unit)) return;
  TargetNormal *transTgt = res.FindTarget(trans);
  // TODO: check: we might want to reveal the vehicle here if necessary
  if (!transTgt || !transTgt->IsKnownBy(unit)) return;

  // we could perform a more elaborate handling, but we are mostly interested in enemies only
  const SideInfo *info = res.GetSideInfo(unit);
  if (!info) return;

  // if the unit is friendly or the vehicle already enemy, we are not interested
  if (!info->SideIsEnemy(objTgt->side) || info->SideIsEnemy(transTgt->side))
  {
    return;
  }

  if (objTgt->side==transTgt->side) return;

  // if we cannot see the unit now, we cannot see it is getting in
  // however, if we see a vehicle nearby, we will most likely notice what has happened
  if (GWorld->Visibility(unit,obj)<0.2f && GWorld->Visibility(unit,trans)<0.2f) return;

  // now it is a time to update the trans side info
  // we know side has changed
  transTgt->side = objTgt->side;
  transTgt->sideAccuracy = objTgt->sideAccuracy;
  transTgt->sideChecked = objTgt->sideChecked;

  transTgt->category = transTgt->CheckCategory(*info);
}

/**
give a target list opportunity to react to a kill which was seen
\patch 5106 Date 12/20/2006 by Ondra
- Fixed: Dead units might sometimes be able to report kills in their neighbourhood.
*/

void EntityAIFull::CheckKiller( TargetList &res, EntityAI *killer, EntityAI *obj )
{
  // check if we can see the kill

  // check distance from the kill
  const float seeKillLimit = 100.f;
  float dist2=obj->FutureVisualState().Position().Distance2Inline(FutureVisualState().Position());
  if( dist2>Square(seeKillLimit) ) return;

  // check if we know the target
  AIBrain *unit = CommanderUnit();
  if (!unit || !unit->LSCanTrackTargets() || !GetType()->ScanTargets()) return;

  TargetNormal *tgt = res.FindTarget(obj);
  if (!tgt || !tgt->IsKnownBy(unit)) return;

  // no reason to react to my own destruction - I am dead anyway
  // (and even if not, there is another mechanism for this in place)
  if (obj==this) return;
  // check if there is LOS
  if (GWorld->Visibility(unit,obj)<0.2f) return;

  const SideInfo *info = res.GetSideInfo(unit);
  if (!info) return;

  TargetNormal::TrackContext &ctx = res.InitTrackContext(unit);

  // unit should be still valid
  AIBrain *aiUnit = obj->CommanderUnit();

  // we have seen the kill - react
  tgt->SeenKill(killer,ctx,obj,unit,aiUnit,0.5);
}

class EntityAIFullAddNewTarget
{
  EntityAIFull *_entity;
  EntityAIFull::AddNewTargetContext &_ctx;
  TargetList &_res;
  bool _initialize;
  
  public:
  EntityAIFullAddNewTarget(
    EntityAIFull *entity, EntityAIFull::AddNewTargetContext &ctx,
    TargetList &res, bool initialize
  )
  :_entity(entity),_ctx(ctx),_res(res),_initialize(initialize)
  {
  }
  
  __forceinline bool operator () (Entity *obj) const
  {
    _entity->AddNewTarget(_res,_initialize,_ctx,obj);
    return false;
  }
};

/*!
\patch 5161 Date 5/28/2007 by Ondra
- Fixed: Laser target acquire distance for AI was limited by rendering distance.
*/
void EntityAIFull::AddNewTargets(TargetList &res, bool initialize)
{

  _newTargetsTime=Glob.time;

  PROFILE_SCOPE_EX(tgNew,ai);

  AIBrain *unit = CommanderUnit();
  if (!unit) return;

  const SideInfo *info = res.GetSideInfo(unit);
  if (!info) return;
  
  AddNewTargetContext ctx(*info);

  ctx.brain = unit;

  ctx.nearestEnemyDist2 = 1e10;

  // check which vehicles are visible from this one

  float irRange=GetType()->GetIRScanRange();
  ctx.maxVisibility2=Square(floatMax(TACTICAL_VISIBILITY,irRange));

  EntityAIFullAddNewTarget forEach(this,ctx,res,initialize);
  GWorld->ForEachVehicle(forEach);
  // TODO: perhaps we could walk only slow vehicles, and ignore very slow ones? (Very slow are usually lamps)
  GWorld->ForEachSlowVehicle(forEach);

  ctx.brain->SetNearestEnemyDist2(ctx.nearestEnemyDist2);
}

float TargetNormal::AdjustMyDelay(AIBrain *unit, float spotability) const
{
  Target *assigned = unit->GetTargetAssigned();
  if (assigned && assigned->IsKnownBy(unit) && assigned!=this)
  {
    // we are already busy - longer reaction time
    return InterpolativC(spotability,0.75f,4.0f,3.0f,1.0f); // when the target is visible very well, do not make the reaction time much longer
  }
  return 1;
}


/*!
\patch 5164 Date 7/9/2007 by Ondra
- Fixed: AI was able to pinpoint enemy by ear after seeing him for a while.
\patch 5164 Date 9/14/2007 by Ondra
- Fixed: AI was sometimes unable to move when seeing enemy very near.

\patch 5209 Date 12/31/2007 by Ondra
- Fixed: Sometimes AI might wrongly favor audible information over a visual one after not seeing the target for a while.
*/

void TargetNormal::Sees(
  EntityAI *ai, AIBrain *unit, float dist2,
  TrackContext &ctx, float trackTargetsPeriod
)
{
  #if _ENABLE_CHEATS
    bool diag = false;
    // debugging opportunity - camera unit checking the cursor target
    if (ctx.vehicle==GWorld->GetObserver())
    {
      if (GWorld->UI()->GetCursorTarget() && ai==GWorld->UI()->GetCursorTarget()->idExact)
      {
        __asm nop;
      }
    }
    // debugging opportunity - camera unit checking the cursor target
    Object *obs = GWorld->GetObserver();
    EntityAIFull *obsAI = dyn_cast<EntityAIFull>(obs);
    if (obsAI && unit->GetGroup()==obsAI->GetGroup())
    {
      if (ai==GWorld->CameraOn() && ai!=obsAI)
      {
        __asm nop;
        diag = true;
      }
    }
  #endif
  // AI FIX in 1.33
  // {{ distance is increased for units with low skill
  float incDist2 = 1+(ctx.invAbilitySpotDistance-1)*0.4f;
  saturate(incDist2,0.001f,1000.0f);
  dist2 *= incDist2;
  if (dist2>Square(ctx.maxDist)) return;
  
  // no need to track invisible targets
  // note: this was introduced to handle ghost targets, but they use TargetGhost now
  if (ai->Invisible() && idSensor!=unit)
  {
    return;
  }

  if (ai->GetType()->GetArtilleryTarget())
  {//nobody sees arty target itself
    return;
  }

  _validTime = trackTargetsPeriod+1.0f;

  // }}

  bool laserContact =
  (
    (ai->GetType()->GetLaserTarget() && ctx.type->GetLaserScanner()) ||
    (ai->GetType()->GetNvTarget() && ctx.type->GetNVScanner())
  );
  bool radarContact=
  (
    dist2<Square(ctx.irRange) && ai->GetType()->GetIRTarget() &&
    (ctx.type->GetIRScanGround() || ai->Airborne())
  );
#if _ENABLE_CHEATS
  if (ctx.vehicle==GWorld->GetObserver())
  {
    // debug opportunity
    __asm nop;
  }
#endif
  // variable spotability limit depending on unit "busy" status
  Target *assigned = unit->GetTargetAssigned();
  // threshold for radar contacts is derived from tactical display rendering
  // - see InGameUI::DrawTacticalDisplay - if (visible<0.1f)
  float spotTreshold = radarContact ? 0.1f : 0.75f;
  // when busy, spotting other targets than the assigned one may be somewhat more difficult
  if (assigned && assigned!=this) spotTreshold *= 1.2f;
  if (!ctx.notManual)
  {
    // player auto-spotting has more strict conditions
    // currently condition for veteran mode is never auto-spot
    // we can auto spot friendly or neutral targets
    // they are never reported in radio
    TargetSide aiSide = ai->GetTargetSide();

    spotTreshold = Glob.config.IsEnabled(DTAutoSpot) ? 0.75f : 100.0f; 

    if (ctx.SideIsFriendly(aiSide) || ctx.SideIsNeutral(aiSide))
    {
      // override player auto-report
      // but leave threshold a little bit stricter than normal
      saturateMin(spotTreshold,0.6f);
    }
  }

  float movement = ai->VisibleMovement();
  float aiAudible = ai->Audible();

  float audibility = 0;
  float visibility = 0;
  float sharpness = 0; // when picture is foggy, no zooming can improve it
  if (!ai->GetType()->GetLaserTarget() || ctx.type->GetLaserScanner() || !ai->GetType()->GetNvTarget() || ctx.type->GetNVScanner())
    visibility = ctx.vehicle->CalcVisibility(ai,dist2,&audibility,&sharpness);

  /*
  LogF
  (
    "Track: %s from %s - %g, night %g",
    (const char *)ai->GetDebugName(),
    (const char *)GetDebugName(),
    visibility,night
  );
  */

  // small object are less visible
  // if sensor is moving fast, sensibility may be decreased
  if (!radarContact && !laserContact)
  {
    if (ctx.vehicle->FutureVisualState().Speed().SquareSize()>Square(10))
    {
      // we should compute relative speed only
      // what we are really interested about it our velocity relative to the target surroundings
      // when we are moving towards the target, it is stationary and the movement is not distracting us at all
      // compute the part of the velocity we can discard (the closing/moving away one)
      Vector3 relPos = ai->FutureVisualState().Position()-ctx.vehicle->FutureVisualState().Position();
      float relPosSize2 = relPos.SquareSize();
      if (relPosSize2>0)
      {
        // direction of relPos
        float relPosInvSize = InvSqrt(relPosSize2);
        Vector3 relPosNorm = relPos*relPosInvSize;
        float speedFactor = relPos.CosAngle(ctx.vehicle->FutureVisualState().Speed());
        Vector3 apparentSpeed = ctx.vehicle->FutureVisualState().Speed()-relPosNorm*speedFactor;
        float mySpeed = apparentSpeed.Size();
        // compute angular speed
        // assume when moving 10 rad/sec we can barely see anything
        // 1 rad/s already makes recognition much harder
        float speedCoef = mySpeed*relPosInvSize*0.5f;
        saturateMin(speedCoef,0.95f);
        visibility *= 1-speedCoef;
      }
    }

    {
      // sensibility to audio should actually depend on how much noise we are making
      // this may be unrelated to the speed
      // TODO: use GetEngineVol, GetEnvironVol, or Audible itself?
      float mySpeed=ctx.vehicle->FutureVisualState().Speed().Size();
      float speedCoef=mySpeed*(1.0f/200);
      saturateMin(speedCoef,0.8f);
      audibility -= speedCoef;
      saturateMax(audibility,0);
    }
  }

  float actSpotabilityEye=visibility;
  float actSpotabilityEar=audibility;

  //LogF("  actSpotability %g",actSpotability);

  if (!radarContact && !laserContact)
    saturate(visibility,0,10);
  saturate(audibility,0,120);

  // update visible movement/fire
  // spot
  float visibleAccuracy=visibility*3;
  saturateMin(visibleAccuracy,30);

  float audibleAccuracy=audibility*3;

  //Vector3Val eDir=ctx.eDir;
  float cosFiEye=ctx.eDir.CosAngle(ai->FutureVisualState().Position()-ctx.vehicle->FutureVisualState().Position());
  float cosFiWep=cosFiEye;

  // TODO: which turret?
  TurretContext context;
  if (ctx.vehicle->GetPrimaryGunnerTurret(context) && context._weapons->_currentWeapon >= 0)
    cosFiWep = ctx.wepDir.CosAngle(ai->FutureVisualState().Position()-ctx.vehicle->FutureVisualState().Position());

  float tgtNight = floatMax(0, ctx.night - ai->GetLit());
  //LogF("  pre visibleAccuracy %g",visibleAccuracy);
  // FIX: target recognition accuracy in night
  float cosEyeAdjusted = ctx.cosEye;
  if (ai->GetType()->GetLaserTarget() || ai->GetType()->GetNvTarget())
    // eye cannot be used for laser targets
    cosEyeAdjusted = -1;

  float nightAccCoef = 1-tgtNight;
  if (radarContact || laserContact) visibleAccuracy*=ctx.type->_sensitivity;
  else if (cosFiWep>ctx.cosOpt) visibleAccuracy*=ctx.type->_sensitivity*ctx.opticsZoom*nightAccCoef; // optics
  else if (cosFiEye>ctx.cosEyeFocus) visibleAccuracy*=ctx.type->_sensitivity*nightAccCoef;
  else if (cosFiEye>cosEyeAdjusted) visibleAccuracy*=ctx.type->_sensitivity*0.5f*nightAccCoef;
  else if (cosFiEye>ctx.cosEyePeripheral) visibleAccuracy*=ctx.type->_sensitivity*0.05f*nightAccCoef;
  else visibleAccuracy = 0;

  // when sharpness is extremely low, assume accuracy may not go high
  static float foggy = 0.25f;
  float maxVisAccuracy = sharpness>foggy ? InterpolativC(sharpness,foggy,1,foggy,27) : sharpness;

  if (visibleAccuracy>maxVisAccuracy)
    visibleAccuracy = maxVisAccuracy; // zooming prevented to provide more accurate info by fogging
  audibleAccuracy *= ctx.sensitivityEar*aiAudible*0.25;
  //LogF("  post visibleAccuracy %g",visibleAccuracy);

  if ( ai->GetType()->GetArtilleryTarget())
  { // eye cannot be used for laser targets
    cosEyeAdjusted = -1;
    audibleAccuracy= visibleAccuracy = 0;
  }

  
  // we can never recognize "real" side only by hearing
  // real side is recognized when accuracy is GetSideAccuracyExact() or higher
  float audibleSideAccuracy = floatMin(audibleAccuracy*0.5f,GetSideAccuracyExact()*0.9f);
  float visibleSideAccuracy = floatMin(visibleAccuracy*0.15f,4);

  saturateMin(visibleAccuracy,4);
  saturateMin(audibleAccuracy,4);

  float fadingAccuracy=FadingAccuracy();
  float fadingSideAccuracy=FadingSideAccuracy();
  float sensorAccuracy = floatMax(audibleAccuracy,visibleAccuracy);
  if (ctx.initialize)
  {
    // during init assume all friendly units within range are fully known
    if (ctx.SideIsFriendly(ai->GetTargetSide()))
    {
      sensorAccuracy = 4;
      visibleAccuracy = 4;
      audibleAccuracy = 4;
      if (ai->CommanderUnit())
      {
        saturateMax(visibleSideAccuracy,1.6);
        saturateMax(audibleSideAccuracy,1.6);
      }
    }
  }

  if (destroyed)
  {
    // special handling of destroyed targets
    // dead body or vehicle is always civilian - and not checked, as it is not "real" side
    // when unit is respawn, it may become enemy again
    side = TCivilian;
    sideChecked = false;
  }
  else
  {
    float sensorSideAccuracy = floatMax(audibleSideAccuracy,visibleSideAccuracy);
    // It has no sense to compare the values above GetSideAccuracyExact() - everything means we know the exact information
    // comparing it exactly means we cannot replace older information by a newer one unless we are closer to the target
    TargetSide tSide = ai->GetTargetSide(sensorSideAccuracy);
    // hack - enable to change side to enemy when unit becomes a renegade
    bool checked = sensorSideAccuracy>=GetSideAccuracyExact();
    if (sensorSideAccuracy>=fadingSideAccuracy || checked || tSide == TEnemy && side != TEnemy)
    {
      // special check for a vehicle "stolen" by a renegade
      if (ctx.SideIsFriendly(tSide) || tSide==TCivilian)
      {
        if (sensorSideAccuracy>=GetSideAccuracyExact()*0.9f && ctx.SideIsEnemy(ai->GetTargetSide()))
        {
          // unless we know for sure it is enemy, we consider stolen vehicles as unknown
          // as they are not supposed to be moving
          tSide = TSideUnknown;
          checked = true;
        }
      }
      bool checkCategory = side!=tSide;
      side = tSide;
      sideChecked = checked;
      // do not replace a larger value with a smaller one without reason (side changed)
      if (checkCategory || sensorSideAccuracy>=fadingSideAccuracy)
      {
        sideAccuracy = floatMax(sensorSideAccuracy,sideAccuracy);
        fadingSideAccuracy = sideAccuracy;
      }
      if (checkCategory)
        category = CheckCategory(ctx);
    }
  }
  // check if object is visible
  //float visibility=FadingVisibility();
  float fSpotability=FadingSpotability();      
  
  const EntityAIType *aiType=ai->GetType();
  float lightCoef=1;

  if (!radarContact && !laserContact && tgtNight>0)
  {
    float lightsOnOff=ai->VisibleLights();
    // dark night value is chosen experimentally, based on observation of AI detection, see news:hse1mm$95m$2@new-server.localdomain
    const float lightNight = 0.997f;
    const float darkNight = 0.999f;
    float spotableLightsOff = InterpolativC(
      tgtNight,lightNight,darkNight,aiType->GetSpotableNightLightsOff(),aiType->GetSpotableDarkNightLightsOff()
    );
    float nightSpotable = Lerp(spotableLightsOff,aiType->GetSpotableNightLightsOn(),lightsOnOff);
    lightCoef = Lerp(1,nightSpotable,tgtNight);
  }

  float dist = sqrt(dist2);

  float delayCoef = 1;
  float hidden = ai->GetHidden(dist); // actually opposite of hidden
  // pattern hides is lower on short distance
  const float patternFullDistance = 60;
  if (dist2<Square(patternFullDistance) && hidden<1)
  {
    float patternFactor = dist/patternFullDistance;
    // if unit is not moving, this effect is not so strong
    float moveFactor = movement;
    saturate(moveFactor,0,1);
    // the bigger patternFactor, the better is unit concealed in patterns
    //LogF(" pattern %.3f, move %.3f, hidden %.3f",patternFactor,moveFactor,hidden);
    patternFactor = 1-(1-patternFactor)*moveFactor;
    hidden = 1 - (1-hidden)*patternFactor;
    //LogF("   patternFactor %.3f, result %.3f",patternFactor,hidden);
  }
  

  float visibleFire=visibility*ai->VisibleFire();
  float audibleFire=audibility*ai->AudibleFire()*0.3f;

  // assume visual error of plain eye is about 10 cm per 100 m
  const float visualError = 0.10f/100;
  
  // we have no clue where the target is
  float spotErrorEye = 1e10;
  float spotErrorEar = 1e10;
  float spotCoefEye = 0;
  if (radarContact || laserContact)
  {
    spotCoefEye = ctx.type->_sensitivity*8;
    delayCoef = 0.5f; // faster reaction
    spotErrorEye = 0;
    spotCoefEye = 1;
  }
  else
  {
    if (visibility>1e-4)
    {
      // we can see something
      if (cosFiWep>ctx.cosOpt)
      {
        // optics vision
        float spotOptics = floatMax(ctx.opticsZoom*0.5,1);
        spotCoefEye = movement*spotOptics*ctx.type->_sensitivity;
        spotCoefEye*=lightCoef * hidden;
        delayCoef = 1.0f;
        spotErrorEye = dist*visualError*ctx.opticsFov;
      }
      else if (cosFiEye>cosEyeAdjusted)
      {
        // plan eye vision
        spotCoefEye = movement*ctx.type->_sensitivity;
        spotCoefEye *= lightCoef * hidden;
        delayCoef = 1.5f; // slightly slower reaction
        spotErrorEye = dist*visualError;
      }
      else if (cosFiEye>ctx.cosEyePeripheral)
      {
        // peripheral vision - much lower sensitivity and precision
        spotCoefEye = movement*ctx.type->_sensitivity*0.1f;
        spotCoefEye *= lightCoef*hidden;
        delayCoef = 3.0f; // much slower reaction
        spotErrorEye = dist*visualError*50;
      }
      else
      {
        visibleFire = 0;
        spotCoefEye = 0;
      }
    }
    float howMuchWeHear = aiAudible*audibility*ctx.sensitivityEar;
    if (howMuchWeHear>1e-3)
    {
      // we can hear something
      // position prediction is very inaccurate
      spotErrorEar = dist*0.7f;
    }
  }
  
  spotErrorEye *= ctx.invAbilitySpotDistance;
  spotErrorEar *= ctx.invAbilitySpotDistance;

  // make sure error is never too high, and at least appoximate direction is known
  saturateMin(spotErrorEar,dist*0.9f);
  
  actSpotabilityEye *= spotCoefEye;
  actSpotabilityEar *= aiAudible*ctx.sensitivityEar;

  // select if we better hear or see
  float actSpotability = actSpotabilityEye;
  float spotError = spotErrorEye;
  if (actSpotability<1 && actSpotability<actSpotabilityEar)
  {
    spotError = spotErrorEar;
    delayCoef = 2.5f; // slower reaction
    actSpotability =  actSpotabilityEar;
    hidden = 1;
#if 0 //_ENABLE_CHEATS
    if (ai->CommanderUnit())
      LogF("%s heard by %s, spot %.3f, spotEye %.3f",
        (const char *)ai->GetDebugName(),
        (const char *)GetDebugName(),
        actSpotabilityEar,actSpotabilityEye);
#endif
  }

  //LogF("  spotCoef %g, actSpotability %g",spotCoef,actSpotability);
  saturateMin(actSpotability,4);
  float newSpotability=actSpotability;

  float newSideAccuracy=fadingSideAccuracy;

  // consider if unit is firing
  float sensorFire = floatMax(visibleFire,audibleFire);
  if( sensorFire>0.8f ) // we can see somebody firing
  {
    //LogF("Fire vis %.2f, aud %.2f",visibleFire,audibleFire);
    TargetType *fireTarget=ai->GetFiredAt();
    // for player we do not know what we fired at
    if (fireTarget)
    {
      // check what we know about the target
      const TargetNormal *fireTVis = ctx.targets->FindTarget(fireTarget);
      if (fireTVis)
      {
        saturateMax(newSideAccuracy,fireTVis->FadingAccuracy());
        saturateMax(newSideAccuracy,fireTVis->FadingSideAccuracy());
        // TODO: this might be also used to reveal something about the target
        // 1) disclose the target
        // 2) estimate side of the target
      }
      // if we are not in combat - this means danger?
    }
    ctx.OnDanger(unit, this);
  }

  float myDelay = 1.0f*ctx.invAbilitySpotTime*delayCoef;

  if (radarContact || laserContact)
  {
    // radar equipped vehicles have much shorter reaction time
    if (dist<ctx.irRange)
    {
      float maxTime = dist2*0.0000003f+0.07f*ctx.invAbilitySpotTime;
      saturateMin(myDelay,maxTime);
    }     
  }
  else
  {
    if (dist2<Square(100) )
    {
      if ( spotError<1 )
      {
        // visible target is very close - improve reaction time
        float maxTime = dist2*0.0002f+0.02f*ctx.invAbilitySpotTime;
        saturateMin(myDelay,maxTime);
      }
      else
      {
        // audible target very close - improve reaction time
        float maxTime = dist2*0.001f+0.1f*ctx.invAbilitySpotTime;
        saturateMin(myDelay,maxTime);
      }
    }
  }

  if( IsKnownNoDelay() )
  {
    if (ctx.initialize)
    {
      delay = Glob.time;
      delaySensor = Glob.time;
      delayVehicle = Glob.time;
      myDelay = 0;
    }
    else if (newSpotability>=FadingSpotability())
    {
      // unit may improve delay
      myDelay *= AdjustMyDelay(unit, newSpotability);
      if (ctx.notManual)
      {
#if 0
        if (unit->GetUnit())
        {
          int id = unit->GetUnit()->ID();
          _idSeen.Set(id,true);
        }
#endif // _DEBUG
        Time newDelay = Glob.time+myDelay;
        if (newDelay<delaySensor)
        {
          //LogF("+++ %s reduce delay to %s at %.2f, delay to %.2f",cc_cast(unit->GetDebugName()),cc_cast(GetDebugName()),Glob.time.toFloat(),newDelay.toFloat());
          delaySensor = newDelay;
          idSensor = unit;
          unit->OnNewTarget(this,delaySensor);
         }
      }
    }
  }
  else
  {
    // target not known yet
    // idSensor should be the unit that sees it best
    // we take unit with best spotability
    if( newSpotability>spotTreshold )
    {
#if 0
      LogF("Set init error %s to %s, %.1f",(const char *)GetDebugName(),(const char *)ai->GetDebugName(),spotError);
#endif
      RandomPosError();

      bool checkCategory = !_isKnown;
      _isKnown = true;
#if DIAG_AGGREGATED_TARGETS
      {
        AIGroup *grp = unit->GetGroup();
        if (grp && grp->GetDebugName()=="B 1-1-A")
          LogF("Target %s:%s _isKnown=true!", cc_cast(ai->GetType()->GetTextSingular()), idExact ? cc_cast(idExact->GetDebugName()) : "???");
      }
#endif
      if (checkCategory)
        category = CheckCategory(ctx);
      position = ai->AimingPosition(ai->FutureVisualState());
      UpdateSnappedPosition();
      speed = ai->PredictVelocity();      
      // whenever updating position, we need to update position accuracy as well
      posAccuracy = spotError;
      posAccuracyTime = Glob.time;      

      // TODO: direction is not always known
      dir = ai->FutureVisualState().Direction();

      // reveal to me
      float groupDelay = 2.5f*myDelay+2.5f;
      myDelay *= AdjustMyDelay(unit, newSpotability);

      // max. 10 sec. radio delay
      saturateMin(groupDelay,myDelay+10.0f);

      if (ctx.initialize)
      {
        myDelay = 0;
        groupDelay = 0;
      }

      // reveal to rest of the group
      delaySensor = Glob.time + myDelay;
      delay = Glob.time + groupDelay;
      delayVehicle = Glob.time + groupDelay;
      idSensor = unit;
      //LogF("+++ %s notice %s at %.2f, delay to %.2f",cc_cast(unit->GetDebugName()),cc_cast(GetDebugName()),Glob.time.toFloat(),delaySensor.toFloat());

#if 0
      if (unit->GetUnit())
      {
        int id = unit->GetUnit()->ID();
        _idSeen.Set(id,true);
      }
#endif // 0
    }
  }

  bool seenNow = false;

  // using lower bound we can ignore targets which are sure to be not known
  const float minSpotabilityLimit = 0.02f;
  if (_isKnown && actSpotability>minSpotabilityLimit)
  {
    // spotability limits depends on whether exact target location is already known
    // we assume less sensory input is needed to track already known target that to discover it

    // note: using posAccuracy is not enough, as even position may be completely different than real target position
    // we should distinguish this somehow in the target
    // for now we assume posAccuracy is set meaningfully and in such siltation it will large enough

    float eyeSpotabilityLimit = minSpotabilityLimit;
    float earSpotabilityLimit = minSpotabilityLimit;

    // spotErrorEye tells what precision we would see the target with
    // this is related to the eye resolution and distance
    // we need the posAccuracy to be within a certain limit based on this

    // check if discovery is needed
    float maxAccuracyForTracking = spotErrorEye*20.0f;
    // posAccuracy-idExact->VisibleSize() given how much we may be looking "aside" of the object
    // besides of low posAccuracy the error may be caused by the fact unit has moved elsewhere meanwhile
    // if we were able to predict the movement, it does not make the detection harder
    float predictAge = floatMinMax(Glob.time.Diff(posAccuracyTime),0,MaxPredictMovementTime);
    float distanceFromPredictedLine = NearestPointDistance(
      position,position+speed*predictAge,idExact->AimingPosition(idExact->FutureVisualState())
    );

    if (posAccuracy+distanceFromPredictedLine-idExact->VisibleSize(idExact->FutureVisualState())>maxAccuracyForTracking)
    {
      // if it is, the stimulus need to be much higher (cf. spotTreshold, which is even larger)
      eyeSpotabilityLimit = 0.20f;
      earSpotabilityLimit = 0.10f;
    }

    if (actSpotabilityEye>eyeSpotabilityLimit || actSpotabilityEar>earSpotabilityLimit)
    {
      seenNow = true;
    }
  }
  
  // it is quite possible the target is known and it is close enough to have a significant accuracy
  // but is was not noticed yet - in such case we cannot update its info

  if (seenNow)
  {
    if (unit && unit->GetUnit())
    {
      if (!_idSeen.Get(unit->GetUnit()->ID()))
      {
        // unit sees the target for the first time, let it react once the target is really known (may be delayed a bit)
        unit->OnNewTarget(this,delaySensor);
        // TODO: implement similar functionality for agents as well
      _idSeen.Set(unit->GetUnit()->ID(),true);
    }
    
    }
    
    if( newSideAccuracy>fadingSideAccuracy )
    {
      //LogF("  set sideAccuracy from newSideAccuracy %g",newSideAccuracy);
      // introduce slight delay before detecting dead unit side
      if (!ai->IsDamageDestroyed() || ai->GetDestroyedTime()<=Glob.time-ctx.deadDetectTime)
      {
#if 0 //_ENABLE_REPORT
        if (ai->IsDamageDestroyed() && unit->GetGroup()->GetCenter()->GetSide()==TWest)
          LogF("  dead %s",(const char *)ai->GetDebugName());
#endif
        side = ai->GetTargetSide(newSideAccuracy);
        sideChecked = true;
        sideAccuracy=newSideAccuracy;
        fadingSideAccuracy=newSideAccuracy;
      }
    }

    //LogF("  accuracy: aud %.2f, vis %.2f",audibleAccuracy,visibleAccuracy);
    if (sensorAccuracy>fadingAccuracy)
    {
      accuracy = sensorAccuracy;
      fadingAccuracy = sensorAccuracy;
    }

    //side=ai->GetTargetSide(fadingSideAccuracy);
    type=ai->GetType(fadingAccuracy);
    if (!sideChecked && !destroyed)
    {
      TargetSide tSide = type->_typicalSide;
      if (tSide!=TSideUnknown && (!ai->IsDamageDestroyed() || ai->GetDestroyedTime()<=Glob.time-ctx.deadDetectTime))
      {
        AIBrain *aiUnit = ai->CommanderUnit();
        if (aiUnit)
        {
          if (aiUnit->GetCaptive()) tSide = TCivilian;
          else
          {
            // check if it is some vehicle friendly units have stolen
            TargetSide realSide = ai->GetTargetSide(4);
            if (ctx.SideIsFriendly(realSide)) tSide = realSide;
          }
        }
        else
        {
          // no commander unit - it is probably empty
          if (!ai->IRSignatureOn() && ctx.SideIsEnemy(tSide))
          {
            // if it is empty, recognize it magically
            tSide = TCivilian;
          }
        }
        #if 0 //_ENABLE_REPORT
        if (ai->IsDamageDestroyed() && unit->GetGroup()->GetCenter()->GetSide()==TWest)
        {
          LogF("  dead 2 %s",(const char *)ai->GetDebugName());
        }
        #endif
        side = tSide;
      }
    }
  }
  if (!destroyed && ai->IsDamageDestroyed() && ai->GetDestroyedTime()<=Glob.time-ctx.deadDetectTime)
  {
    // not reported as destroyed yet
    // set target for reporting
    #if 0 //_ENABLE_REPORT
    if (unit->GetGroup()->GetCenter()->GetSide()==TWest)
    {
      LogF
      (
        "%s dead detected by %s at %.3f, died at %.3f",
        (const char *)ai->GetDebugName(),(const char *)unit->GetDebugName(),
        Glob.time.toFloat(),ai->GetDestroyedTime().toFloat()
      );
    }
    #endif
    destroyed = true;
    reactedToDestruction = false;
    timeReported = TIME_MIN;
    posReported = VZero;
    side = TCivilian;
    sideChecked = false;
    category = CheckCategory(ctx);
  }

  if (seenNow)
  {
    // spot. error is gradually growing - unless there is something which will limit it
    float oldError;
    bool improved = LimitError(spotError,&oldError);
    #if 0 // _ENABLE_REPORT
    if (improved && ai->GetTargetSide()!=TCivilian)
    {
      LogF(
        "%s to %s: spot error %g, spot eye %g, spot ear %g, angle %g",
        cc_cast(ctx.vehicle->GetDebugName()),cc_cast(ai->GetDebugName()),
        spotError,actSpotabilityEye,actSpotabilityEar,
        acos(cosFiEye)*(180/H_PI)
      );
    }
    #endif
#if 0
    // if someone is claiming the information is recent and accurate, but in fact it is very wrong, hot-fix it
    if (
      !improved && oldError<1.0 && Glob.time<posAccuracyTime+5.0f &&
      ai->AimingPosition().Distance2(position)>Square(oldError*5)
    )
    {
      if (ai->CommanderUnit()->GetUnit())
      {
        LogF(
          "Hotfixing target %s from %s: reported accuracy %.2f by %s, actual %.2f",
          cc_cast(ai->GetDebugName()),cc_cast(ctx.vehicle->GetDebugName()),
          oldError,idSensor ? cc_cast(idSensor->GetDebugName()) : "",ai->AimingPosition().Distance(position)
        );
      }
      oldError = FLT_MAX;
      improved = true;
    }
#endif

    // use float oldError = FadingPosAccuracy() or change LimitError prototype
    // the information we have may have high precision, but it may be still inaccurate because it is old
    // to counteract this: when spotError is higher than it was before, we limit the position update
    if (improved)
    {
      position = ai->AimingPosition(ai->FutureVisualState());
      UpdateSnappedPosition();
      speed = ai->PredictVelocity();
            
      // whenever updating position, we need to update position accuracy as well
      posAccuracy = spotError; // TODO: done twice, already done in LimitError, eliminate one
      posAccuracyTime = Glob.time;      
      // TODO: direction is not always known
      dir = ai->FutureVisualState().Direction();
      // when our information is a lot more precise than the stored one, become the primary reporter
      if (ctx.notManual && spotError<oldError*0.8f && IsKnownBy(unit))
      {
        idSensor = unit;
      }
    }
    else if (idSensor == unit)
    {
      // when multiple observers provide contradictory information, we want to use only the most precise one
      // which is tracked by idSensor

      // both new and old information can be viewed as a sphere
      // old sphere: position,FadingPosAccuracy()
      // new sphere: AimingPosition(),spotError
      float dist2 = ai->AimingPosition(ai->FutureVisualState()).Distance2(position);
      // old is part of new: dist<=rNew-rOld
      // old overlays new: dist<=rNew+rOld
      if (dist2>=Square(oldError+spotError))
      {
        // new information contradicts the old one
        // use the new one - the old must be obsolete now
        posAccuracy = spotError;
        posAccuracyTime = Glob.time;

        position = ai->AimingPosition(ai->FutureVisualState());
        UpdateSnappedPosition();
        speed = ai->PredictVelocity();
        // TODO: direction is not always known
        dir = ai->FutureVisualState().Direction();
      }
    }
        
    AIBrain *aiUnit = ai->CommanderUnit();
    if (ctx.notManual || !aiUnit || aiUnit->GetLifeState()!=LifeStateDead)
    {
      // player should not report dead bodies as seen, while AI should
      // this is because when AI is not able to report they are dead
      // it will almost certainly see it still in the next sample
      lastSeen=Glob.time+trackTargetsPeriod;
      
      if (unit && unit->GetUnit())
      {
        _idSeen.Set(unit->GetUnit()->ID(),true);
      }
    }

    // check if it is my group dead unit
    if(
      (destroyed || aiUnit && aiUnit->GetLifeState()==LifeStateDead) &&
      // only AI or AutoSpot will report casualties
      (ctx.notManual || Glob.config.IsEnabled(DTAutoSpot))
    )
    {
      // we see a dead body - somebody must have killed it
      if (!reactedToDestruction && ctx.notManual)
      {
        // make sure we react only once
        reactedToDestruction = true;
        // if it was killed by enemy, reveal that enemy
        EntityAI *killer = ai->GetKiller();

        // reaction is supposed to be quite slow here
        // we do not see the kill, we see only a body
        SeenKill(killer, ctx, ai, unit, aiUnit,3.0f);
      }
      
      // TODO: report all units in it as dead
      ctx.OnDeadDetected(unit, aiUnit);
    }
  }

  if( newSpotability>fSpotability )
  {
    spotability = newSpotability;
    spotabilityTime = Glob.time;
  }

  
  // !hasDisclosedUs - each enemy can disclose us only once
  if (IsKnownByAll() && ctx.SideIsEnemy(side) && !destroyed && !vanished && !hasDisclosedUs)
  {
    if (ctx.SideIsEnemy(ai->GetTargetSide()) && ai->CommanderUnit())
    {
      // hack - check if I'm in the enemy target list
      const TargetList *list = ai->CommanderUnit()->AccessTargetList();
      if (list && list->IsDisclosing(ctx.vehicle,ai) )
      {
        // enemy knows about me - we are probably disclosed
        // open fire and change behavior to danger
        // remember it has disclosed the owner of the target list
        hasDisclosedUs = true;
        if (unit->IsLocal() && !unit->IsPlayer())
        {
          /*
          LogF
          (
          "%s disclosed by %s",
          (const char *)GetDebugName(),(const char *)ai->GetDebugName()
          );
          */
          unit->Disclose(DCEnemyNear, position, ai);

          // hasDisclosedUs will prevent calling this for other units - therefore will call it here as necessary
          AIGroup * grp = unit->GetGroup();
          if (grp)
          {
            for (int u=0; u<grp->NUnits(); u++)
            {
              AIUnit *uu = grp->GetUnit(u);
              // ignore units which cannot react, ignore ourselves - we have already reacted
              if (!uu || !uu->IsUnit() || uu==unit) continue;
              // ignore remote and player units - no reaction possible for them
              if (!uu->IsLocal() || uu->IsPlayer()) continue;
              // check if the vehicle is disclosing that unit as well
              if (list->IsDisclosing(ctx.vehicle,ai) )
              {
                unit->Disclose(DCEnemyNear, position, ai);
              }

            }
          }
        }
      }
    }
  }

  //Assert( FadingSideAccuracy()<3 || side!=TSideUnknown );

  // check nearest known enemy
  if
  (
    ctx.SideIsEnemy(side) && IsKnownNoDelay() &&
    (ai->GetType()->GetIRTarget() || !ai->GetType()->GetLaserTarget() || !ai->GetType()->GetNvTarget() || !ai->GetType()->GetArtilleryTarget())
  )
  {
    float dist2=position.Distance2(ctx.vehicle->FutureVisualState().Position());
    saturateMin(ctx.nearestEnemy2,dist2);
  }

}

/** 
This determines category. Category is normally cached - this function should not be called very often.
*/

TgtCategory TargetNormal::CheckCategory(const SideInfo &who)
{
  TgtCategory cat = TgtCatFriendly;
  // dead bodies should be handled as friendly
  // they are no longer much important
  if (!destroyed)
  {
    // if we think it is an enemy or if it is an enemy we want it to be listed in the enemy list
    if (who.SideIsEnemy(side) || idExact && who.SideIsEnemy(idExact->GetTargetSide(4)))
    {
      cat = TgtCatEnemy;
      if (!_isKnown) cat = TgtCatEnemyUnknown;
    }
  }
  return cat;
}

void TargetNormal::SeesItself(EntityAI *ai, AIBrain *unit, const SideInfo &sideInfo, float trackTargetsPeriod)
{ // anyone sees himself perfectly
  type=ai->GetType(4);
  TargetSide tSide = ai->GetTargetSide(4);
  bool checkCategory = side != tSide;
  side = tSide;
  if (checkCategory)
  {
    category = CheckCategory(sideInfo);
  }
  sideChecked = true;
  spotability = 4;
  accuracy = 4;
  sideAccuracy = 4;
  posAccuracy = 0;
  _validTime = 0;
  spotabilityTime = Glob.time;
  posAccuracyTime = Glob.time;
  _isKnown = true;
  // it will almost certainly see itself until next sample
  lastSeen = Glob.time+trackTargetsPeriod;
  // multiple units may be present in the same vehicle. Check most preferred sensor
  // we do not care who sees it, but we want it to be stable
  if (!idSensor || idSensor->GetVehicle()!=ai || idSensor.GetLink()<unit)
    idSensor = unit;

  if (unit->GetUnit())
  {
    int id = unit->GetUnit()->ID();
    _idSeen.Set(id,true);
  }

  delay = Glob.time-5; // no delay
  delaySensor = Glob.time-5; // no delay
  delayVehicle = Glob.time-5;
  position=ai->AimingPosition(ai->FutureVisualState());
  UpdateSnappedPosition();
  dir = ai->FutureVisualState().Direction();
  posErrorDirection=VZero;
  speed=ai->PredictVelocity();
  timeReported=Glob.time; // no need to report itself
  posReported = position;
}

/*!
  \patch 5091 Date 11/28/2006 by Ondra
  - Fixed: Improved AI reaction to friendly units observed being killed.
*/


void TargetNormal::SeenKill(
  EntityAI * killer, TrackContext &ctx, EntityAI * ai, AIBrain * unit, AIBrain * aiUnit,
  float basicReactionTime
) const
{
  Target *tgt = NULL;
  if (killer && ctx.SideIsEnemy(killer->GetTargetSide()))
  {
    // we guess it was an enemy, but we do not have a slightest idea of what kind
    float accuracy = 0.1f;
    float sideAcc = 1.5f;
    // assume the enemy should be someplace near
    Vector3 pos = ai->FutureVisualState().Position();

    // add owner to group sensor list
    float delay = basicReactionTime*unit->GetInvAbility(AKSpotTime);
    // if vehicle already busy, reaction is slower
    if (unit->GetTargetAssigned()) delay *= 2;

    tgt = unit->AddTarget(killer,accuracy,sideAcc,delay,&pos);
  }

  if (aiUnit && aiUnit->GetGroup() == unit->GetGroup())
    unit->SetDanger(DCDeadBodyGroup, AimingPosition(), tgt);
  else
    unit->SetDanger(DCDeadBody, AimingPosition(), tgt);
}
/*!
\param ai target considered
\param dist2 square distance to target
\param audibility (optional) pointer receiving target audibility
\return
0 when full occluded, 1 when fully visible
Multiplied by inverse square distance and constant factor
\patch 1.22 Date 8/27/2001 by Ondra.
- Fixed: AI Sound perception less decreased by obstructions.
\patch 1.27 Date 10/18/2001 by Ondra.
- Fixed: AI hearing through solid objects was too good.
\patch_internal 1.22 Date 8/27/2001 by Ondra.
- New: Separate audibility / visibility calculation.
\patch 5150 Date 3/28/2007 by Ondra
- Fixed: Improved laser target detection and engaging by AI.
*/
float EntityAIFull::CalcVisibility(
  EntityAI *ai, float dist2,  float *audibility, float *sharpness, bool assumeLOS
)
{
  if (audibility) *audibility = 0;
  if (sharpness) *sharpness = 0;


  AIBrain *brain=CommanderUnit();
  if (!brain || !brain->LSIsAlive())
  {
    // dead unit cannot see anything
    return 0;
  }

  float vis = 1;
  const EntityAIType *type = GetType();
  float irRange = type->GetIRScanRange();
  bool irScan= dist2<Square(irRange) && ai->GetType()->GetIRTarget();
  if (ai->GetType()->GetLaserTarget() || ai->GetType()->GetNvTarget())
  {
    // laser targets require special processing
    if (!type->GetLaserScanner()) 
    // perform the same distance calculations as with IR scanning
    irScan = true;
  }

  //ignore artillery targets
  if (ai->GetType()->GetArtilleryTarget())
    return 0;


  if (!type->GetIRScanGround() && !ai->Airborne()) irScan = false;
  if( !irScan )
  { // no IR - use eye
    if( dist2>Square(GScene->GetTacticalVisibility()) ) vis=0;
    else vis=1-GScene->TacticalFog8(dist2)*(1.0f/255);
    if (sharpness) *sharpness = vis;
  }
  else
  { // use IR
    if( dist2>Square(irRange) ) vis=0;
    else
    {
      float dist=dist2*InvSqrt(dist2); // sqrt(dist2)
      vis=1-dist/irRange;
      // we want radar accuracy to degrade only in last 25 % of the range
      if (sharpness) *sharpness = floatMin(1,vis*4);
    }
  }

  // Note: aiRadius should really be VisibleSize
  // but it was discovered too late (1.28)
  // and impact of fixing it would be too unpredictable
  //float aiRadius=ai->VisibleSize();
  // size is used in calculations from VisibleMovement instead
  // see also Man::VisibleMovement and Man::VisibleSize
  float aiRadius=ai->GetRadius();
  float landVis=assumeLOS ? 1 : GWorld->Visibility(brain,ai);

  if (ai->GetType()->GetLaserTarget() || ai->GetType()->GetNvTarget())
  {
    aiRadius = 160;
  }
  float radiusCoef=5000;
  if( irScan )
  {
    const float increaseIRSensitivity = 1500.0f;
    if (Airborne() && irRange>increaseIRSensitivity)
    {
      // depending on the scan range, 25x magnification may be not enough
      radiusCoef *= irRange * (1.0f/ increaseIRSensitivity);
    }
    radiusCoef*=25;
  }
  float sizeVis = dist2 > 0 ? aiRadius * radiusCoef / dist2 : 100;
  // FIX: near hearing/seeing
  saturateMin(sizeVis,40);

  vis*=sizeVis;
  vis*=landVis;

  if (audibility)
  {
    // calculate audibility
    float aud = 1;

    // units can hear even behind obstructions to some extent
    // we assume landVis is between 0 and 1
    Assert (landVis>=0 && landVis<=1);
    float landAud = 1 - (1-landVis)*0.90f;
    float audRadiusCoef=5000;
    // we assume bigger objects are also more noisy
    float audRadius = ai->GetRadius();
    float sizeAud = dist2 > 0 ? audRadius * audRadiusCoef / dist2 : 1000;
    saturateMin(sizeAud,240);

    aud*=sizeAud;
    aud*=landAud;

    *audibility = aud;
  }
  return vis;
}

//#if _ENABLE_CHEATS
//#include <El/Statistics/statistics.hpp>
//#endif

/*!
\patch 1.01 Date 06/11/2001 by Ondra
- Fixed: too low visibility/spotability of near targets.
- Most problems were related to too strict clamping of near values
- Following updates were made:
- near spotability (day & night) is more sensitive, especially on lying soldiers
- night vision is more sensitive
- recognition in night is less sensitive
\patch 1.01 Date 6/26/2001 by Ondra.
- Fixed: Too fast target forgetting.
\patch 1.01 Date 6/26/2001 by Ondra.
- Improved: AI takes NV goggles on/off.
\patch 1.27 Date 10/18/2001 by Ondra.
- Fixed: AI ability to distinguish enemy from friendly by ear was too good.
\patch 1.28 Date 10/19/2001 by Ondra.
- Improved: AI reaction to weapon sound.
\patch 1.28 Date 10/22/2001 by Ondra.
- Fixed: AI: When hearing target that has been seen and then not seen
for some time, position information was too accurate.
\patch 1.28 Date 10/22/2001 by Ondra.
- Fixed: AI: When target was very loud, is was sometimes
considered audible but not visible.
\patch 1.30 Date 11/02/2001 by Ondra.
- Fixed: Vehicles destroyed and later repaired with setdammage 0
were considered dead by AI.
\patch 1.33 Date 11/28/2001 by Ondra.
- Fixed: Detecting laser targets not in front of vehicle.
\patch 1.33 Date 11/30/2001 by Ondra.
- Improved: Low skill soldier have less sensitive eyes and ears.
This make game easier when playing against low skill enemy and
makes difference between low and high skill more important.
\patch_internal 1.59 Date 5/22/2002 by Ondra
- Fixed: AI sensitivity was very high, seeing even back (introduced in 1.58).
\patch 1.85 Date 9/21/2002 by Ondra
- Fixed: More realistic side detection by AI, should make stealing enemy equipment easier.
\patch 1.85 Date 9/21/2002 by Ondra
- Fixed: AI often fired on empty targets.
*/
TargetNormal::TrackContext *EntityAIFull::InitTrackContext( TargetList &res, AIBrain * unit, int canSee, bool initialize, float maxDist )
{
  TargetNormal::TrackContext &ctx = res.InitTrackContext(unit);
  if (!ctx.IsValid()) return NULL;

  ctx.vehicle = this;
  ctx.type = GetType();

  // check which vehicles are visible from this one
  //const float maxVisibility=TACTICAL_VISIBILITY;

  //Assert( GLOB_WORLD->CheckVehicleStructure() );

  //int id=brain->ID()-1;


  // set default zoom - corresponding to r-button zoom
  ctx.opticsFov = 0.30;
  ctx.opticsZoom = 1;
  ctx.cosOpt = 0.95393920142; // no optics - cos 17 deg
  ctx.cosEye=0.7f; // cos 45 deg
  ctx.cosEyePeripheral = (canSee&CanSeePeripheral) ? 0.5f : ctx.cosEye; // cos 60 deg
  ctx.cosEyeFocus = 0.96592582629f; // cos 15 deg (r-button zoom ability)
  ctx.initialize = initialize;

  if (canSee&CanSeeOptics)
  {
    // different vehicles have different optics zoom
    // check max. zoom or check zoom of current weapon
    TurretContext context;
    if (FindTurret(unit ? unit->GetPerson() : NULL, context))
    {
      int selected = context._weapons->ValidatedCurrentWeapon();

      if (selected>=0 && EnableViewThroughOptics())
      {
        // if we are manual, check current optics
        // TODO: check if current weapon is binocular
        // if it is, check if binoculars are enabled
        const MagazineSlot &slot = context._weapons->_magazineSlots[selected];
        const OpticsInfo &opticsInfo = slot._muzzle->_opticsInfo[slot._opticsMode];
        if (slot._muzzle) ctx.opticsFov = opticsInfo._opticsZoomMax;
      }
    }

    if (ctx.opticsFov<0.3)
    {
      // calculate max. cos based on fov
      // fov is sin of angle
      ctx.cosOpt = sqrt(1-ctx.opticsFov*ctx.opticsFov);
      // calculate zoom in terms of area
      ctx.opticsZoom = Square(floatMax(0.30/ctx.opticsFov,1));
      /*
      LogF
      (
      "%s: fov %.2f, zoom %.2f, cos %.3f, ang %.3f",
      (const char *)GetDebugName(),
      opticsFov,opticsZoom,cosOpt,acos(cosOpt)*(180/H_PI)
      );
      */
    }
  }

  if (!(canSee&CanSeeEye))
  {
    // some units cannot see with eye, disable eye to optics level
    // if there is no optices, eye is disabled completely
    ctx.cosEye = ctx.cosOpt;
  }

  ctx.sensitivityEar = GetType()->_sensitivityEar;
  if (!(canSee&CanSeeEar))
  {
    ctx.sensitivityEar = 0;
  }

  ctx.notManual = unit->HasAI() && !unit->GetPerson()->IsRemotePlayer();
  ctx.nearestEnemy2=1e10;

  ctx.irRange = GetType()->GetIRScanRange();
  if (!(canSee&CanSeeRadar)) ctx.irRange=0;

  ctx.maxDist=maxDist;
  saturateMax(ctx.maxDist,ctx.irRange);
  saturateMax(ctx.maxDist,TACTICAL_VISIBILITY);

  ctx.invAbilitySpotTime = unit->GetInvAbility(AKSpotTime);
  ctx.invAbilitySpotDistance = unit->GetInvAbility(AKSpotDistance);
  ctx.deadDetectTime = ctx.invAbilitySpotTime*0.3f+0.75f;
  ctx.targets = &res;

  const LightSun *sun = GScene->MainLight();
  ctx.night=sun->NightEffect();
  if (ctx.night>0)
  {
    // check light intensity
    // with light brightness 0.02 we can recognize shapes, not colors
    // this allows for some enemy identification, but somewhat harder
    float dark = 1.02-sun->GetAmbient().Brightness()-sun->GetDiffuse().Brightness();
    saturateMin(ctx.night,dark);

    // check if soldier has NV goggles
    //bool hasNV = unit->GetPerson()->IsNVEnabled() || GetType()->GetNightVision();
    Person *person = unit->GetPerson();
    bool hasNV =
      (
      person->IsNVEnabled() && person->IsNVWanted() || GetType()->GetNightVision()
      );

    if (hasNV || person->HasTiOptics())
    {
      saturateMin(ctx.night,0.75f);
    }
    else
    {
      // 1 Oct 2008: dark = 1.01979
      // 11 Oct 2008: dark = 0.975
      static float darkestNight = 0.999f; // was 0.997 until rev. 70321
      saturateMin(ctx.night,darkestNight);
    }
  }

  ctx.wepDir = FutureVisualState().Direction();
  ctx.eDir = FutureVisualState().Direction();

  Person *gunner = unit ? unit->GetPerson() : NULL;
  TurretContext context;
  if (FindTurret(gunner, context))
  {
    int selected = context._weapons->ValidatedCurrentWeapon();
    if (selected >= 0)
    {
      ctx.wepDir = GetWeaponDirection(FutureVisualState(), context, selected);
    }
    ctx.eDir = GetLookAroundDirection(FutureVisualState(), context);
  }
  return &ctx;
}

void EntityAIFull::TrackTargets(
  TargetList &res, AIBrain *unit, int canSee,
  bool initialize, float maxDist,
  float trackTargetsPeriod[NTgtCategory]
)
{
  if (!canSee) return; // nothing to test

  PROFILE_SCOPE_EX(tgTrk,ai);

  // track targets for single unit (gunner or commander)
  TargetNormal::TrackContext *ctx = InitTrackContext(res, unit, canSee, initialize, maxDist);
  if (ctx)
  {
    res.TrackTargets(unit,this,trackTargetsPeriod,maxDist,*ctx);

    ctx->CheckAdditionalTargetDatabases();

    _nearestEnemy=sqrt(ctx->nearestEnemy2);
  }
}

/*!
\patch 5142 Date 3/19/2007 by Ondra
- Fixed: Improved target scanning for fast moving units.
*/

void EntityAIFull::TrackOneTarget(TargetList &res, Target *target, float trackTargetsPeriod)
{
  const EntityAIType *type = GetType();
  AIBrain *unit=CommanderUnit();
  if (unit)
  {
    float maxDist = TACTICAL_VISIBILITY;
    TargetNormal::TrackContext *ctx = InitTrackContext(res,unit,type->_commanderCanSee,false,maxDist);
    if (ctx)
    {
      TrackOneTarget(target,unit,trackTargetsPeriod,maxDist,*ctx);
    }
  }
}

void TargetList::DistributeTargets(TgtCategory from)
{
  // array of all categories
  RefArray<TargetNormal> &fromList = _list[from];
  int dst = 0;
  for (int src=0; src<fromList.Size(); src++)
  {
    TargetNormal *tgt = fromList[src];
    // check which category this target should belong to
    TgtCategory cat = tgt->category;
    if (cat!=from)
    {
      // move into a different list
      RefArray<TargetNormal> &tgtList = _list[cat];
      tgtList.Add(tgt);
      continue;
    }
    fromList[dst++]=fromList[src];
  }
  fromList.Resize(dst);
}

void TargetList::TrackTargets(
  AIBrain *unit, EntityAIFull *veh,
  float trackTargetsPeriod[NTgtCategory], float maxDist, TargetNormal::TrackContext &ctx
)
{
  // TODO: remove scopes, use loop
  // scan first, distribute later
  // we do not want one item to be scanned twice because of being distribute meanwhile
  if (trackTargetsPeriod[TgtCatEnemy]>=0)
  {
    PROFILE_SCOPE_EX(tgTrE,ai);
    veh->TrackTargetsPart(_list[TgtCatEnemy],unit,trackTargetsPeriod[TgtCatEnemy],maxDist,ctx);
  }
  if (trackTargetsPeriod[TgtCatEnemyUnknown]>=0)
  {
    PROFILE_SCOPE_EX(tgTrU,ai);
    veh->TrackTargetsPart(_list[TgtCatEnemyUnknown],unit,trackTargetsPeriod[TgtCatEnemyUnknown],maxDist,ctx);
  }
  if (trackTargetsPeriod[TgtCatFriendly]>=0)
  {
    PROFILE_SCOPE_EX(tgTrF,ai);
    veh->TrackTargetsPart(_list[TgtCatFriendly],unit,trackTargetsPeriod[TgtCatFriendly],maxDist,ctx);
  }
  // now make sure targets are distributed as needed
  for (int i=0; i<NTgtCategory; i++)
  {
    if (trackTargetsPeriod[i]>=0)
    {
      PROFILE_SCOPE_EX(tgTrD,ai);
      DistributeTargets((TgtCategory)i);
    }
  }
}

void EntityAIFull::TrackMyVehicle(TargetNormal *target, AIBrain *unit, float trackTargetsPeriod, const SideInfo &ctx)
{
  EntityAI *ai=target->idExact;
  if( !ai ) return;

  Assert(ai==this);
  
  if( ai==this )
  {
    target->SeesItself(ai,unit,ctx,trackTargetsPeriod);
  }

}

void EntityAIFull::TrackOneTarget(
  Target *target, AIBrain *unit,
  float trackTargetsPeriod, float maxDist, TargetNormal::TrackContext &ctx
)
{
  // TODO: avoid dynamic cast here
  TargetNormal *tgt = dynamic_cast<TargetNormal *>(target);
  // select needs to be a part of the active target list
  if(!tgt) return;

  EntityAI *ai=tgt->idExact;
  if( !ai ) return;

  // update visibility information
  // distant objects are less visible
  if( ai==this )
  {
    tgt->SeesItself(ai,unit,ctx,trackTargetsPeriod);
    return;
  }
  if (!ai->IsInLandscape()) return; // cannot be visible - is not in landscape

  // check if target is within tracked range
  float dist2=ai->FutureVisualState().Position().Distance2Inline(FutureVisualState().Position());
  if (dist2>Square(maxDist)) return;

  tgt->Sees(ai,unit,dist2,ctx,trackTargetsPeriod);

}

/**
Traverse one category of targets
*/

void EntityAIFull::TrackTargetsPart(
  RefArray<TargetNormal> &tgt, AIBrain *unit,
  float trackTargetsPeriod, float maxDist, TargetNormal::TrackContext &ctx
)
{
  for( int index=0; index<tgt.Size(); index++ )
  {
    TargetNormal *target=tgt[index];
    EntityAI *ai=target->idExact;
    if( !ai ) continue;
    
    // update visibility information
    // distant objects are less visible
    if( ai==this )
    {
      target->SeesItself(ai,unit,ctx,trackTargetsPeriod);
      continue;
    }
    if (!ai->IsInLandscape()) continue; // cannot be visible - is not in landscape

    // check if target is within tracked range
    float dist2=ai->FutureVisualState().Position().Distance2Inline(FutureVisualState().Position());
    if (dist2>Square(maxDist)) continue;

    PROFILE_SCOPE_DETAIL_EX(tgTgS,ai);
    #if _PROFILE
    if (PROFILE_SCOPE_NAME(tgTgS).IsActive())
    {
      PROFILE_SCOPE_NAME(tgTgS).AddMoreInfo(Format("%s -> %s",cc_cast(GetType()->GetName()),cc_cast(ai->GetType()->GetName())));
    }
    #endif

    target->Sees(ai,unit,dist2,ctx,trackTargetsPeriod);

  } // for( all vehicles )

}

/*!
\param res target list to track
\param initialize set when initializing. When true, situation is considered steady
and all values should be set as it was the same for very long time.
\param trackTargetsPeriod how long it is expected
until next TrackTargets will execute
*/

void EntityAIFull::TrackTargets(
  TargetList &res, bool initialize, float trackTargetsPeriod[NTgtCategory]
)
{
  const EntityAIType *type = GetType();
  AIBrain *unit=CommanderUnit();
  if (unit)
  {
    TrackTargets
    (
      res,unit,type->_commanderCanSee,initialize,1e10,trackTargetsPeriod
    );
  }

}

const float TrackNearPeriod = 1.0f;

/*!\note
When some enemy is near, it is called often,
in every AIGroup::Think (about 0.1 sec)
*/

void EntityAIFull::TrackNearTargets(TargetList &res, float maxDist)
{
  const EntityAIType *type = GetType();
  AIBrain *unit=CommanderUnit();
  // avoid using zero period, as this could make targets invalid when moving
  // across the distance border
  // we want to scan enemies only here
  if (unit)
  {
    // we want also to find new enemies
    static float trackTargetsPeriod[]=
    {
      TrackNearPeriod, // TgtCatEnemy
      TrackNearPeriod, // TgtCatEnemyUnknown
      -1.0f, // TgtCatFriendly
    };
    Assert(lenof(trackTargetsPeriod)==NTgtCategory);
    TrackTargets(res,unit,type->_commanderCanSee,false,maxDist,trackTargetsPeriod);
  }
}

void EntityAIFull::TrackLockedTargets(TargetList &res)
{
  // track assigned target or fire target
  Target *oneTarget = NULL;
  // first try to check what are we firing at
  if (!oneTarget)
  {
    TurretContext context;
    // TODO: multiple turrets (should be done also in Transport::TrackTargets)
    if (GetPrimaryGunnerTurret(context))
    {
      oneTarget = context._weapons ? context._weapons->_fire._fireTarget : NULL;
    }
  }
  if (!oneTarget)
  {
    AIBrain *unit=CommanderUnit();
    if (unit)
    {
      oneTarget = unit->GetTargetAssigned();
    }
  }
  if (oneTarget)
  {
    TrackOneTarget(res,oneTarget,TrackNearPeriod);
  }
}

/*!
  Perform target tracking
  Update target list to maintain actual visibility and spotability
  information about all potentially visible targets.
  (uses EntityAI::AddNewTargets and EntityAI::TrackTargets)
*/

void EntityAIFull::WhatIsVisible( TargetList &res, bool initialize )
{
  if (initialize)
  {
    float trackAllPeriod[NTgtCategory]=
    {
      1.0f, // TgtCatEnemy
      1.0f, // TgtCatEnemyUnknown
      1.0f, // TgtCatFriendly
    };
    AddNewTargets(res,initialize);
    TrackTargets(res,initialize,trackAllPeriod);
    return;
  }
  // note:
  // this function is called only from CreateTargetList
  // CreateTargetList itself is called 
  // once per 1 second in Combat mode

  AIBrain *unit = CommanderUnit();
  if (!unit) return;

  float newTargetsPeriod = 1.0f;
  float trackTargetsPeriod[NTgtCategory]=
  {
    1.0f, // TgtCatEnemy
    2.0f, // TgtCatEnemyUnknown
    5.0f, // TgtCatFriendly
  };
  
  if (GetType()->_irScanRangeMax<=0) newTargetsPeriod = 5.0f;

  float trackRange = floatMax(GetType()->GetIRScanRange(),TACTICAL_VISIBILITY);


  // check if there is some enemy in tracked range
  if (unit->GetNearestEnemyDist2()>Square(trackRange))
  {
    // track 5x less when no enemy is near
    newTargetsPeriod *= 5.0f;
  }

  // TODO: perform AddNewTargets for all units at once
  if (Glob.time>_newTargetsTime+newTargetsPeriod)
  {
    AddNewTargets(res,initialize);
  }
  // disable categories which did not reach their time yet
  bool doAny = false;
  for (int i=0; i<NTgtCategory; i++)
  {
    if (Glob.time<=_trackTargetsTime[i]+trackTargetsPeriod[i])
    {
      trackTargetsPeriod[i] = -1.0f;
    }
    else
    {
      _trackTargetsTime[i] = Glob.time;
      doAny = true;
    }
  }
  
  if (doAny)
  {
    TrackTargets(res,initialize,trackTargetsPeriod);
  }

  // note: new targets are added once for all units the vehicle
}


VisibilityTracker::VisibilityTracker()
{
}

VisibilityTracker::VisibilityTracker( EntityAI *obj )
:_obj(obj),_lastTime(Glob.time-60),_lastTimeForLastVisiblePos(TIME_MIN),_lastVisiblePositionValid(false)
{
}

VisibilityTracker::~VisibilityTracker()
{
}


/*!
\patch 1.31 Date 11/23/2001 by Ondra.
- Fixed: When firing, line of fire between weapon and target is checked.
Line between eye and target was checked before, which sometimes
could lead to friendly fire.
*/

float VisibilityTracker::Value(
  bool &headOnly,
  const EntityAIFull *sensor, const TurretContext &context, int weapon, float reserve, float maxDelay
)
{
  // sensor and weapon are assumed const (tracker hard linked to sensor)
  if (!_obj)
  {
    LogF("VisibilityTracker expired");
    headOnly = _lastHeadOnly;
    return _lastValue;
  }
  if (_lastTime<Glob.time-maxDelay)
  {
    _lastTime=Glob.time;

    // check if it is within tactical range
    EntityAI *ai = _obj;
    float irRange = sensor->GetType()->GetIRScanRange();
    if (!ai->GetType()->GetIRTarget()) irRange = 0;
    else if (!sensor->GetType()->GetIRScanGround() && !ai->Airborne()) irRange = 0;
    float maxDistance2=Square(floatMax(irRange,TACTICAL_VISIBILITY));
    float visible=0;
    bool headOnly = false;
    if( sensor->FutureVisualState().Position().Distance2(ai->FutureVisualState().Position())<maxDistance2 )
    {
      // in OFP 1.33 VisibilityTime was used here
      // this is not very robust, as line of fire is checked here, not line of sight
      // it was moved into GetAimed
      // it was seen in near past. Check if we can fire.
      Vector3 weaponPos;
      if (weapon<0)
      {
        weaponPos = sensor->EyePosition(sensor->FutureVisualState());
      }
      else
      {
        weaponPos = sensor->FutureVisualState().PositionModelToWorld(sensor->GetWeaponPoint(sensor->FutureVisualState(),context, weapon));
      }
      visible = GLandscape->Visible(headOnly,weaponPos,sensor,ai,reserve,ObjIntersectFire);
    }
    _lastHeadOnly = headOnly;
    _lastValue = visible;
    if (_lastValue>0.5f)
    {
      _lastVisiblePosition = headOnly ? ai->AimingPositionHeadShot(ai->FutureVisualState()) : ai->AimingPosition(ai->FutureVisualState());
      _lastVisiblePositionValid = true;
      _lastVisiblePositionTime = Glob.time;
    }
    return visible;
  }
  headOnly = _lastHeadOnly;
  return _lastValue;
}

float VisibilityTracker::ValueForLastVisible(
  Time &time, const EntityAIFull *sensor, const TurretContext &context, int weapon, float reserve, float maxDelay
)
{
  // sensor and weapon are assumed const (tracker hard linked to sensor)
  // if there is no last visible position, we cannot return any data for it
  if (!_lastVisiblePositionValid) return 0;

  if (!_obj)
  {
    LogF("VisibilityTracker expired");
    time = _lastVisiblePositionTime;
    return _lastValueForLastVisiblePos;
  }

  if (_lastTimeForLastVisiblePos<Glob.time-maxDelay)
  {
    _lastTimeForLastVisiblePos=Glob.time;

    // check if it is within tactical range
    EntityAI *ai = _obj;
    // visual range only
    float maxDistance2=Square(TACTICAL_VISIBILITY);
    float visible=0;
    if( sensor->FutureVisualState().Position().Distance2(ai->FutureVisualState().Position())<maxDistance2 )
    {
      // in OFP 1.33 VisibilityTime was used here
      // this is not very robust, as line of fire is checked here, not line of sight
      // it was moved into GetAimed
      // it was seen in near past. Check if we can fire.
      // if weapon is not selected, no need to test for a suppressive fire
      if (weapon>=0)
      {
        Vector3 weaponPos = sensor->FutureVisualState().PositionModelToWorld(sensor->GetWeaponPoint(sensor->FutureVisualState(), context, weapon));
        visible=GLandscape->Visible(sensor->GetFutureVisualStateAge(),weaponPos,_lastVisiblePosition,ai->VisibleSize(ai->FutureVisualState()),sensor,ai,ObjIntersectFire);
      }
    }
    _lastValueForLastVisiblePos=visible;
  }
  time = _lastVisiblePositionTime;
  return _lastValueForLastVisiblePos;
}

VisibilityTrackerCache::VisibilityTrackerCache()
{
}

void VisibilityTrackerCache::Clear()
{
  _trackers.Clear();
}

VisibilityTrackerCache::~VisibilityTrackerCache()
{
}

float VisibilityTrackerCache::CheckKnownValue(
  bool &headOnly, 
  const EntityAIFull *sensor, const TurretContext &context, int weapon, EntityAI *obj, float reserve, float maxDelay
)
{
  DoAssert( obj );
  // check cached results
  for( int i=0; i<_trackers.Size(); i++ )
  {
    VisibilityTracker &tracker=_trackers[i];
    if (tracker._obj==obj && tracker._turret==context._turret)
    {
      // if there is a recent negative about last visible position, we do not test direct visibility at all
      if (tracker._lastTimeForLastVisiblePos>=Glob.time-maxDelay && tracker._lastValueForLastVisiblePos<=0.01f)
      {
        return 0;
      }

      // check this one
      if( tracker._lastTime>=Glob.time-maxDelay )
      {
        headOnly = tracker._lastHeadOnly;
        return tracker._lastValue;
      }
      return tracker.Value(headOnly,sensor,context,weapon,reserve,maxDelay);
    }
  }
  return -1;
}

/*!
\patch 1.27 Date 10/12/2001 by Ondra.
- Fixed: bug in visibility management
  that could cause excessive memory allocation during mission.
*/

float VisibilityTrackerCache::KnownValue(
  bool &headOnly, 
  const EntityAIFull *sensor, const TurretContext &context, int weapon, EntityAI *obj, float reserve, float maxDelay
)
{
  // check cached results
  float ret = CheckKnownValue(headOnly,sensor,context,weapon,obj,reserve,maxDelay);
  if (ret>=0)
  {
    return ret;
  }
  if (_trackers.Size()<=4)
  {
    // create a new tracker if needed, but only if there are not too many trackers yet
    // this makes this function somewhat more lightweight than Value
    VisibilityTracker &tracker=_trackers[_trackers.Add(obj)];
    tracker._turret = context._turret;
    return tracker.Value(headOnly,sensor,context,weapon,reserve,maxDelay);
  }
  return -1;
}

float VisibilityTrackerCache::Value(
  bool &headOnly, 
  const EntityAIFull *sensor, const TurretContext &context, int weapon, EntityAI *obj, float reserve, float maxDelay
)
{
  // check cached results
  float ret = CheckKnownValue(headOnly,sensor,context,weapon,obj,reserve,maxDelay);
  if (ret>=0)
  {
    return ret;
  }

  // before creating a new tracker discard the old ones 
  const float discardAfter = 20.0f;
  for( int i=0; i<_trackers.Size(); i++ )
  {
    VisibilityTracker &tracker=_trackers[i];
    if( Glob.time-tracker._lastTime>discardAfter || !tracker._obj )
    {
      // if tracker is out of date, remove it
      _trackers.Delete(i),i--;
    }
  }
  
  // create a new tracker
  VisibilityTracker &tracker=_trackers[_trackers.Add(obj)];
  tracker._turret = context._turret;
  return tracker.Value(headOnly,sensor,context,weapon,reserve,maxDelay);
}

float VisibilityTrackerCache::ValueForLastVisible(
  Time &time, const EntityAIFull *sensor, const TurretContext &context, int weapon, EntityAI *obj,
  float reserve, float maxDelay
)
{
  DoAssert( obj );
  // check cached results
  for( int i=0; i<_trackers.Size(); i++ )
  {
    VisibilityTracker &tracker=_trackers[i];
    if( tracker._obj==obj && tracker._turret==context._turret)
    {
      // if there is positive direct visibility information which is recent enough, we may use it
      if (tracker._lastValue>0.5 && tracker._lastTime>tracker._lastTimeForLastVisiblePos && tracker._lastTime>=Glob.time-maxDelay)
      {
        time = tracker._lastTime;
        return tracker._lastValue;
      }

      // otherwise check last pos visibility only
      if (tracker._lastTimeForLastVisiblePos>=Glob.time-maxDelay)
      {
        time = tracker._lastVisiblePositionTime;
        return tracker._lastValueForLastVisiblePos;
      }

      return tracker.ValueForLastVisible(time,sensor,context,weapon,reserve,maxDelay);

    }
  }

  // creating a new tracker has no sense here - if there is no last visible pos, there is nothing to test
  return 0;
}


bool VisibilityTrackerCache::GetLastKnownVisiblePosition(Vector3 &ret, EntityAI *obj) const
{
  DoAssert( obj );
  // check cached results
  for( int i=0; i<_trackers.Size(); i++ )
  {
    const VisibilityTracker &tracker=_trackers[i];
    // we do not care which turret is this from
    // consider: with multiple turrets we might search for most recent info based on _lastVisiblePositionTime
    if( tracker._obj==obj && tracker._lastVisiblePositionValid)
    {
      ret = tracker._lastVisiblePosition;
      return true;
    }
  }
  return false;
}

inline float MaxProbability(const WeaponModeType &mode)
{
  return floatMax
  (
    floatMax(mode.midRangeProbab, mode.minRangeProbab),
    mode.maxRangeProbab
  );
}

static float BestDistance(const WeaponModeType &mode)
{
  if (mode.midRangeProbab > mode.minRangeProbab)
  {
    if (mode.maxRangeProbab > mode.midRangeProbab)
    {
      return mode.maxRange;
    }
    return mode.midRange;
  }
  return mode.minRange;
}

static float BestDistance(const WeaponModeType &mode, float &propab)
{
  if (mode.midRangeProbab > mode.minRangeProbab)
  {
    if (mode.maxRangeProbab > mode.midRangeProbab)
    {
      propab = mode.maxRangeProbab;
      return mode.maxRange;
    }
    propab = mode.midRangeProbab;
    return mode.midRange;
  }
  propab = mode.minRangeProbab;
  return mode.minRange;
}

static float HitProbability(float dist, const WeaponModeType &mode)
{
  if (dist < mode.midRange)
  {
    if (dist < mode.minRange) return 0;
    return
    (
      (dist - mode.minRange) * mode.invMidRangeMinusMinRange + mode.minRangeProbab
    );
  }
  else
  {
    if (dist > mode.maxRange) return 0;
    return
    (
      (dist - mode.maxRange) * mode.invMidRangeMinusMaxRange + mode.maxRangeProbab
    );
  }
}

// Computes maximum of HitProbability function over the <distMin,distMax> interval
static float HitIntervalProbability(float distMin, float distMax, const WeaponModeType &mode)
{
  // due to linearity, the max value must occur in one of the following pts:
  //   distMin, distMax, minRange, midRange, maxRange
  float maxProb = 0;
  if (distMin>mode.maxRange) return 0;
  if (distMax<mode.minRange) return 0;

  if (distMin>mode.minRange) 
    maxProb = HitProbability(distMin, mode); //value in distMin
  else
    maxProb = mode.minRangeProbab; //value in minRange
  if (distMax<mode.maxRange)
    saturateMax(maxProb, HitProbability(distMax, mode)); //value in distMax
  else
    saturateMax(maxProb, mode.maxRangeProbab); //value in maxRange

  if (mode.midRange>distMin && mode.midRange<distMax)
    saturateMax(maxProb, mode.midRangeProbab); //value in midRange

  return maxProb;
}

/*!
Used as quick check for autoaim purposes.
*/

bool EntityAIFull::ShootHasSense(
  const Target &target, const TurretContext &context, int weapon, float distance
) const
{
  EntityAI *tgt = target.idExact;
  if (!tgt) return false;

  if (weapon < 0 || weapon > context._weapons->_magazineSlots.Size()) return false;

  const Magazine *magazine = context._weapons->_magazineSlots[weapon]._magazine;
  if (!magazine) return false;

  const MagazineType *ammoInfo = magazine->_type;
  if (!ammoInfo) return false;

  const AmmoType *aInfo = ammoInfo->_ammo;
  if (!aInfo) return false;

  const WeaponModeType *mode = context._weapons->GetWeaponMode(weapon);
  Assert(mode);

  if (distance > mode->maxRange)
  {
    return false;
  }
  if (!tgt->LockPossible(aInfo))
  {
    return false;
  }

  const EntityAIType *type = target.GetType();
  const LODShape *tgtShape = type->GetShape();
  // if the assumed type has no shape loaded, get real size of the vehicle
  // this will most likely be more accurate than using the global default value
  // it is little cheating, but not much - size of the target is very likely to be known
  if (!tgtShape) tgtShape = tgt->GetShape();
  float tgtRadius=tgtShape ? tgtShape->GeometrySphere() : 3;
  // avoid divide by zero
  saturateMax(tgtRadius,0.1f);

  AIBrain *gunnerBrain = context._gunner ? context._gunner->Brain() : NULL;
  if (!gunnerBrain || !gunnerBrain->LSIsAlive())
  {
    gunnerBrain = CommanderUnit();
    if (!gunnerBrain || !gunnerBrain->LSIsAlive()) return false;
  }
  // if target is moving fast it is difficult to lead it
  float ammoSpeed=floatMax(floatMax(ammoInfo->_initSpeed,aInfo->maxSpeed),0.5);
#if _VBS3 //take stance into account for grenades
  ammoSpeed *= GetAmmoInitSpeedFactor(ammoInfo);
#endif

  Vector3 tgtVelocity = target.GetSpeed(gunnerBrain);
  if( Square(ammoInfo->_maxLeadSpeed)<tgtVelocity.SquareSize() )
  {

    float tgtSpeed=tgtVelocity.Size();
    float leadMiss=tgtSpeed-ammoInfo->_maxLeadSpeed;
    float leadError=(distance/ammoSpeed+0.3f)*leadMiss; // time to fly * speed error
    float maxError=(aInfo->indirectHitRange+tgtRadius)*0.3f; // max distance to ignore lead error
    if( leadError>maxError )
    {
      float considerLead=maxError/leadError; // 0 <= considerLead <= 1
      if( considerLead<0.2 )
      {
        return false;
      }
    }
  }
  
  float targetInvArmor = type->GetInvArmor();
  // larger targets are less hit
  // check ammo strength (single point)
  float relStrength = aInfo->hit*targetInvArmor;
  if (relStrength<0.1)
  {
    return false;
  }
  float changeDammage=Square(aInfo->hit)*targetInvArmor*Square(0.27f/tgtRadius);  
  // very low dammage is not cumulated
  if( changeDammage<type->_damageResistance)
  {
    return false;
  }
  return true;
}

class WhatShootResultGunner : public ITurretFuncV
{
protected:
  FireResult &_result; // out
  const EntityAIFull *_who;
  Vector3Par _speed;
  const TurretContext &_weaponContext;
  int _weapon;
  float _inRange;
  float _timeToAim;
  float _timeToLive;
  float _visible;
  float _dist;
  float _timeToShoot;
  bool _potentialCheck;

public:
  WhatShootResultGunner(
    FireResult &result, const EntityAIFull *who, Vector3Par speed,
    const TurretContext &weaponContext, int weapon, float inRange, float timeToAim, float timeToLive,
    float visible, float dist, float timeToShoot, bool potentialCheck)
  : _result(result), _who(who), _speed(speed), _weaponContext(weaponContext), _weapon(weapon), _inRange(inRange),
  _timeToAim(timeToAim), _timeToLive(timeToLive), _visible(visible), _dist(dist), _timeToShoot(timeToShoot),
  _potentialCheck(potentialCheck)
  {}
  bool operator () (EntityAIFull *entity, TurretContextV &context)
  {
    if (context._gunner && context._turret && !context._turretVisualState->IsGunnerHidden() && !context._gunner->IsDamageDestroyed())
    {
      if (_who->WhatShootResultEntity(
        _result, context._gunner, context._gunner->GetType(), _speed,
        _weaponContext, _weapon, _inRange, _timeToAim, _timeToLive,
        _visible, _dist, _timeToShoot, _potentialCheck
        )) return true;
    }
    return false; // continue
  }
};

bool EntityAIFull::WhatShootResultOnCrew(
  FireResult &result, const EntityAI *tgt, Vector3Par speed,
  const TurretContext &context, int weapon, float inRange, float timeToAim, float timeToLive,
  float visible, float dist, float timeToShoot, bool potentialCheck
) const
{
  // check if crew can be hit
  if (!tgt->CheckCrewVulnerable())
  {
    return false;
  }
  // check the result on the crew
  // check if our weapon is suitable for hitting the crew
  // this should be done for Transport targets only
  const Transport *veh = dyn_cast<const Transport,const EntityAI>(tgt);
  if (!veh)
  {
    return false;
  }
  // check target gunner / commander / driver
  if (veh->Driver() && !veh->FutureVisualState().IsDriverHidden() && !veh->Driver()->IsDamageDestroyed())
  {
    bool hit = WhatShootResultEntity(
      result,veh->Driver(),veh->Driver()->GetType(),speed,
      context, weapon,inRange,timeToAim,timeToLive,
      visible,dist,timeToShoot,potentialCheck
    );
    if (hit) return hit;
  }
  WhatShootResultGunner func(
    result, this, speed, context, weapon, inRange, timeToAim, timeToLive,
    visible, dist, timeToShoot, potentialCheck
  );
  return veh->ForEachTurret(unconst_cast(veh->FutureVisualState()), func);
}

/*
@param potentialCheck used for a long term decision, like fleeing (the weapon may not be loaded)
*/

bool EntityAIFull::WhatShootResultEntity
(
  FireResult &result,
  const EntityAI *tgt, const EntityAIType *type, Vector3Par speed,
  const TurretContext &context, int weapon, float inRange, float timeToAim, float timeToLive,
  float visible, float dist, float timeToShoot, bool potentialCheck
) const
{
  if( !IsAbleToFire(context) ) return false;

  #if DIAG_SHOOT_RESULT
    LogF(
      "   WhatShootResult %s to %s (with %d), dist %.1f",
      (const char *)GetDebugName(),(const char *)type->GetName(),weapon,dist
    );
  #endif

  // Crash hotfix - bad weapon given (different context)
  if (weapon<0 || weapon>=context._weapons->_magazineSlots.Size())
  {
    return false;
  }
  const MagazineSlot &slot = context._weapons->_magazineSlots[weapon];
  const Magazine *magazine = slot._magazine;
  if (!magazine)
  {
    // no magazine in - check if some other magazine might be found
    // we want this only when planning longterm (done to fix the fleeing)
    /*
    if (potentialCheck)
    {
      const MuzzleType *muzzle = slot._muzzle;
      if (muzzle)
      {
        int magNumber = context._weapons->FindMagazineByType(muzzle,NULL);
        if (magNumber>=0)
        {
          magazine = _weaponsState._magazines[magNumber];
          if (magazine) goto TryOtherMagazine;
        }
      }
    }
    */
    #if DIAG_SHOOT_RESULT
      LogF("    x: no magazine.");
    #endif
    return false;
  }
  TryOtherMagazine:
  const MagazineType *ammoInfo = magazine->_type;
  #if DIAG_SHOOT_RESULT
  LogF("    weapon name %s:%s",cc_cast(slot._weaponMode->GetName()),cc_cast(ammoInfo->GetName()));
  #endif
  Assert(ammoInfo);
  const AmmoType *aInfo = ammoInfo->_ammo;
  if (!aInfo) return false;

  if (magazine->GetAmmo() <= 0)
  {
    // let us check if we have another magazine for this weapon slot
    // no magazine in - check if some other magazine might be found
    // we want this only when planning longterm (done to fix the fleeing)
    if (potentialCheck)
    {
      const MuzzleType *muzzle = slot._muzzle;
      if (muzzle)
      {
        int magNumber = context._weapons->FindMagazineByType(muzzle,ammoInfo);
        if (magNumber>=0)
        {
          magazine = context._weapons->_magazines[magNumber];
          if (magazine && magazine->GetAmmo()>0) goto TryOtherMagazine;
        }
      }
    }

    #if DIAG_SHOOT_RESULT
      LogF("    x: no ammo.");
    #endif
    return false;
  }

  const WeaponModeType *mode = context._weapons->GetWeaponMode(weapon);
  Assert(mode);
  if (dist > mode->maxRange || dist < mode->minRange)
  {
    #if DIAG_SHOOT_RESULT
      LogF("    x: distance (%.1f <> %.1f..%.1f)", dist, mode->minRange, mode->maxRange);
    #endif
    return false;
  }
  const EntityAI *ai = tgt;

  if( !ai->LockPossible(aInfo) )
  {
    // check if lock is possible
    #if DIAG_SHOOT_RESULT
      LogF("    x: cannot lock.");
    #endif
    return false;
  }
  float myCost=GetType()->_cost;
  // estimate time to live

  float mySecondCost=myCost/timeToLive;
  result.cost = 0;
  result.damage = 0;

  saturateMin(timeToShoot,2*60); // consider max 2 minutes of shooting

  result.loan= timeToAim*mySecondCost;
  float hitProbab = HitProbability(dist, *mode);
  // check if target vehicle is visible
  if( visible<MinVisibleFire )
  {
    #if DIAG_SHOOT_RESULT
      LogF("    x: visibility %.3f",visible);
    #endif
    return false;
  }
  else
  {
    // when the visibility is good, it affects hit probability very little
    //hitProbab*=Square(visible);
    hitProbab *= visible;
  }
  if( hitProbab<0.05 )
  {
    #if DIAG_SHOOT_RESULT
      LogF("    x: hit probability %.3f",hitProbab);
    #endif
    return false; // do not waste ammo
  }

  hitProbab*=inRange;

  const LODShape *tgtShape=type->GetShape();
  // if the assumed type has no shape loaded, get real size of the vehicle
  // this will most likely be more accurate than using the global default value
  // it is little cheating, but not much - size of the target is very likely to be known
  if (!tgtShape) tgtShape = tgt->GetShape();
  float tgtRadius=tgtShape ? tgtShape->GeometrySphere() : 3;
  // avoid divide by zero
  saturateMax(tgtRadius,0.1f);

  // if target is moving fast it is difficult to lead it
  float ammoSpeed=floatMax(floatMax(ammoInfo->_initSpeed,aInfo->maxSpeed),0.5);
  if( Square(ammoInfo->_maxLeadSpeed)<speed.SquareSize() )
  {
    float tgtSpeed=speed.Size();
    float leadMiss=tgtSpeed-ammoInfo->_maxLeadSpeed;
    float leadError=(dist/ammoSpeed+0.3f)*leadMiss; // time to fly * speed error
    float maxError=(aInfo->indirectHitRange+tgtRadius)*0.3f; // max distance to ignore lead error
    if( leadError>maxError )
    {
      float considerLead=maxError/leadError; // 0 <= considerLead <= 1
      #if DIAG_SHOOT_RESULT
      LogF
      (
        "    lead %.1f, spd %.1f, error %.1f, max %.1f, factor %.4f",
        ammoInfo->_maxLeadSpeed,tgtSpeed,
        leadError,maxError,considerLead
      );
      #endif
      if( considerLead<0.2 )
      {
        #if DIAG_SHOOT_RESULT
          LogF("    x: lead %.3f",considerLead);
        #endif
        return false;
      }
      hitProbab*=considerLead;
    }
  }
  // it is more difficult to hit fast moving vehicles
  // it is easier to hit with faster ammunition
  if (speed.SquareSize()>Square(0.5))
  {
    saturateMax(ammoSpeed,2); // assume at least speed of crawling man
    float considerSpeed = ammoSpeed*speed.InvSize()*0.03f;
    saturate(considerSpeed,0,1);
    #if DIAG_SHOOT_RESULT
    LogF
    (
      "    speed %.1f, ammo %.1f, factor %.4f",
      speed.Size(),ammoSpeed,
      considerSpeed
    );
    #endif
    hitProbab*=considerSpeed;
  }
  float danger=0; // how much armor is the enemy able to destroy on us
  // check how much dangerous is the target
  // use visible and dist for
  // check all enemy weapons
  if( tgt->GetTotalDamage()<MaxDammageWorking )
  {
    // predict dangerousness with regard to our movement
    // assume enemy will become dangerous if we will move closer to it
    const float predictTime = 2.0f;
    float distPredict = dist - GetType()->GetTypSpeedMs()*predictTime;
    float dist2 = Square(floatMax(distPredict,0));

    danger = GetDangerCost(type,dist2,1.0f,timeToLive);

        #if DIAG_SHOOT_RESULT
      LogF("    TTL %.2f, danger %.2f",timeToLive,danger);
        #endif
    }
  
  float targetInvArmor=type->GetInvArmor();
  // larger targets are less hit
  // check ammo strength (single point)
  float relStrength = aInfo->hit*targetInvArmor;
  if (relStrength<0.1f)
  {
    #if DIAG_SHOOT_RESULT
      LogF("    x: armor strength %.4f",relStrength);
    #endif
    return WhatShootResultOnCrew(
      result,tgt,speed,context,weapon,inRange,timeToAim,timeToLive,
      visible,dist,timeToShoot,potentialCheck
    );
  }
  float changeDammage = Square(aInfo->hit)*targetInvArmor*Square(0.27f/tgtRadius);
  
  // very low dammage is not cumulated
  if( changeDammage<type->_damageResistance)
  {
    #if DIAG_SHOOT_RESULT
      LogF("    x: dammage change %.4f",changeDammage);
    #endif
    return WhatShootResultOnCrew(
      result,tgt,speed,context,weapon,inRange,timeToAim,timeToLive,
      visible,dist,timeToShoot,potentialCheck
    );
  }
  saturateMin(changeDammage,4);


  float targetCost=type->GetCost()+danger;
  timeToShoot-=timeToAim;

  // count ammo left in current magazine
  int ammoInMag = magazine->GetAmmo();
  int ammoLeftInMag = ammoInMag;
  
  /// for AI time per shot is given not only by reload rate, but also by AI rate of fire
  float timePerShot = mode->_reloadTime;
  if (timePerShot<mode->_aiRateOfFire)
  {
    if (dist>=mode->_aiRateOfFireDistance)
    {
      // very far - maximum delay between shots
      if (mode->_aiRateOfFire>timePerShot) timePerShot = mode->_aiRateOfFire;
    }
    else
    {
      // compute the AI rate of fire distance fall-of
      //float timePerShotAI = mode->_aiRateOfFire*dist/mode->_aiRateOfFireDistance;
      //if (mode->timePerShotAI) timePerShot = timePerShotAI;
      // optimized to avoid division as much as possible:
      if (mode->_aiRateOfFire*dist>timePerShot*mode->_aiRateOfFireDistance)
      {
        timePerShot = mode->_aiRateOfFire*dist/mode->_aiRateOfFireDistance;
      }
    
    }
  }

  float ammoCost = aInfo->cost;
  // when ammo type is not appropriate for the target, increase its cost artificially
  if ((aInfo->_simulation==AmmoShotMissile || aInfo->_simulation==AmmoShotRocket) && aInfo->maxControlRange>10)
  {
    // increase the cost about our own cost (using the ammo can cost us a life later)
    if (aInfo->airLock)
    { // using AA missiles weapons against a ground target
      if (!tgt->Airborne())
      {
        ammoCost += myCost*2;
        if (timeToLive>=60)
        {
          #if DIAG_SHOOT_RESULT
            LogF("    x: not desperate, conserve AA missiles (TTL=%.1f)",timeToLive);
          #endif
          return false;
        }
      }
    }
    else if (tgt->GetType()->GetKind()==VSoft)
    { // using RPG/missile against a soldier / soft target
      ammoCost += myCost;
      if (timeToLive>=60)
      {
        #if DIAG_SHOOT_RESULT
          LogF("    x: not desperate, conserve missiles (TTL=%.1f)",timeToLive);
        #endif
        return false;
      }
    }
  }

  /*
  Two possible probability hit damage models:
  
  Statistical: compute how many shots in average will hit the target, and sum the damage made by them.
  Probabilistic: compute for each shot chance with which it will hit the target, compute gain, and reduce the gain left.

  We use the more optimistic of the two.
  */
  
  #if 1
  {
    // Probabilistic model
    // consider the damage which the vehicle already has
    // note: AI is cheating here - it should consider perceived damage instead
    float curDamage = tgt->GetTotalDamage();

    // avoid paying more for destruction that the total cost of the target
    float curDestruct = 0;
    // compute cost per shot including loan
    float costPerShot = ammoCost + timePerShot*mySecondCost;

    // loop until firing another shot has no economical sense, or until we are out of time
    // or until the target is dead, or until there is no more ammo
    bool once = true; // we always want to count at least one shot, even when it is not effective
    for(float timeSpent=0; timeSpent<timeToShoot && curDamage<1; timeSpent += timePerShot)
    {
      if (curDamage+changeDammage>=1)
      { // first handle a situation where one shot is enough to kill the target
        // in such case probability handling should be a bit different

        float gain = ((1-curDamage)*0.5f + (1-curDestruct)*0.5f)*targetCost*hitProbab;
        if (gain<costPerShot && !once) break;
        // we will get the destruction bonus as well, but compute only proportional part of it based on probability
        result.gain += gain;
        result.damage += (1-curDamage)*hitProbab;
        // for the next loop assume average failure
        curDamage += (1-curDamage)*hitProbab;
        curDestruct += hitProbab;
        // avoid out of range values
        saturateMin(curDestruct,1);
      }
      else
      {
        // the target will not be finished yet - no destruction bonus
        float gain = changeDammage*hitProbab*targetCost*0.5f;
        // really break? Perhaps we could reach for the destruction bonus if we continue firing?
        if (gain<costPerShot && !once) break;
        result.gain += gain;
        result.damage += changeDammage*hitProbab;

        // for the next loop assume average failure
        curDamage += changeDammage*hitProbab;
      }
      ammoLeftInMag--;

      result.loan += timePerShot*mySecondCost;
      result.cost += ammoCost;
      once = false; // next shot may break the loop if not efficient enough
      // if there is no more ammo, stop
      if (ammoLeftInMag<=0)
      {
        // TODO: consider possibility of reloading magazine
        break;
      }
    }
  }
  #endif

  { // Statistical model:
    
    // consider the damage which the vehicle already has
    // note: AI is cheating here - it should consider perceived damage instead
    float curDamage = tgt->GetTotalDamage();

    // max. shots fired are given by time and ammo available
    int maxShots = toIntFloor(timeToShoot/timePerShot);
    if (maxShots>ammoInMag) maxShots = ammoInMag;
    for (int i=1; i<maxShots; i++)
    {
      // compute number of hits, knowing hit/miss ratio
      float hits = i*hitProbab;
      // we want to be pessimistic here, hence the rounding down
      int nHits = toIntFloor(hits);
      // now simulate how much damage will those hits do (if any)
      float changeDammageByHits = changeDammage*nHits;
      if (curDamage+changeDammageByHits>=1)
      { // target killed
        // limit the damage bonus to what is still left
        // we will get the destruction bonus as well
        float gain = ((1-curDamage)*0.5f + 0.5f)*targetCost;
        if (result.gain<gain)
        {
          result.gain = gain;
          result.damage = 1-curDamage;
          // compute only reload time between the shots, not the shot itself
          result.loan = timePerShot*(i-1)*mySecondCost;
          result.cost = ammoCost*i;
          ammoLeftInMag = ammoInMag-i;
        }
        // we cannot destroy more that completely - no need to continue
        break;
      }
      else
      {
        // the target will not be finished yet - no destruction bonus
        float gain = changeDammageByHits*targetCost*0.5f;
        if (result.gain<gain)
        {
          result.gain = gain;
          result.damage = changeDammageByHits;
          result.loan = timePerShot*(i-1)*mySecondCost;
          result.cost = ammoCost*i;
          ammoLeftInMag = ammoInMag-i;
        }
      }
    }
  }
  
  result.weapon = weapon;
  #if DIAG_SHOOT_RESULT
    LogF
    (
      "    probab %.4f, inRange %.2f, visible %.3f, danger %.2f, time %.2f, shots %d, timePerShot %.2f",
      hitProbab,inRange,visible,danger,timeToShoot,magazine->GetAmmo()-ammoLeftInMag,timePerShot
    );
    LogF
    (
      "    %c: dammage %.4f, gain %.0f, cost %.0f, loan %.0f, weapon %d",
      result.gain>0 ? '#' : 'x',
      result.damage,result.gain,result.cost,result.loan,result.weapon
    );
  #endif
  return result.gain>0;
}

/*!
\patch 1.08 Date 7/21/2001 by Ondra.
- Fixed: tank commander weapon selection more stable.
Weapons were changed too often with no obvious reason to do so,
this happened especially when engaging cars.
- Fixed: when there were no dangerous enemies visible,
importance of weak targets was overestimated.
As a result much stronger weapon was selected than necessary.
\patch 1.78 Date 7/19/2002 by Ondra
- Improved: AI: Smarter target selection, especially for planes.
*/

bool EntityAIFull::WhatShootResult(
  FireResult &result, const Target &target, const TurretContext &context, int weapon,
  float inRange, float timeToAim, float timeToLive,
  float visible, float dist, float timeToShoot,
  bool considerIndirect, bool potentialCheck
) const
{
  if( !IsAbleToFire(context) ) return false;

  if (target.IsVanishedOrDestroyed()) return false;

  EntityAI *tgt=target.idExact;
  if( !tgt ) return false;
  
  AIBrain *gunnerBrain = context._gunner ? context._gunner->Brain() : NULL;
  if (!gunnerBrain || !gunnerBrain->LSIsAlive())
  {
    gunnerBrain = CommanderUnit();
    if (!gunnerBrain || !gunnerBrain->LSIsAlive()) return false;
  }

  WhatShootResultEntity(
    result,tgt,target.GetType(),target.GetSpeed(gunnerBrain),context,weapon,inRange,timeToAim,timeToLive,
    visible,dist,timeToShoot,potentialCheck
  );

  if (considerIndirect && result.gain>0)
  {
    const AmmoType *aInfo = context._weapons->GetAmmoType(weapon);
    if (!aInfo) return result.gain>0;

    // some damage will be done
    // consider also secondary (indirect) dammage on other targets
    // some range estimation
    float ihRange = aInfo->indirectHitRange;
    float secRange =  floatMin(ihRange * aInfo->indirectHit*(1.0/20), ihRange*4);
    if (secRange>=4)
    {
      #define DIAG_SEC 0
      #if DIAG_SEC
      LogF
      (
        "%s: Analyze indirect dammage on %s - range %.1f",
        (const char *)GetDebugName(),
        (const char *)target.idExact->GetDebugName(),
        secRange
      );
      #endif
      // we have some significant indirect hit
      // check group target list for other near targets
      AIBrain *unit = CommanderUnit();
      if (!unit) return false;
      Vector3 secCenter = target.AimingPosition();
      
      const TargetList *tgt = unit->AccessTargetList();
      if (!tgt) return false;
      
      // TODO: avoid GetAny here, use specific categories
      for (int i=0; i<tgt->AnyCount(); i++)
      {
        const Target *tgtI = tgt->GetAny(i);
        if (tgtI->IsVanishedOrDestroyed()) continue;
        if (tgtI->idExact==target.idExact) continue;
        // check if unit is in range
        float dist2 = tgtI->AimingPosition().Distance2(secCenter);
        if (dist2>=Square(secRange)) continue;
        if (!tgtI->IsKnownBy(unit)) continue;
        #if DIAG_SEC
          LogF
          (
            "  %s in range",
            (const char *)tgtI->idExact->GetDebugName()
          );
        #endif
        TargetSide side = tgtI->GetSide();
        // calculate gain coefficient by side
        float coef = 1;
        if (unit->IsEnemy(side)) coef = 1;
        else if (unit->IsFriendly(side)) coef = -6;
        else if (side==TCivilian) coef = -6;
        else if (side==TSideUnknown) coef = -0.5;
        // rough hit estimation
        const EntityAIType *typeI = tgtI->GetType();
        float rangeCoef = dist2<Square(ihRange) ? 1 : Square(ihRange)/dist2;
        float dammage = aInfo->indirectHit*typeI->GetInvArmor()*rangeCoef;
        saturate(dammage,0,1);
        if (coef>0)
        {
          result.gain += dammage*coef*typeI->GetCost();
        }
        else
        {
          result.cost += dammage*-coef*typeI->GetCost();
        }
        #if DIAG_SEC
        LogF
        (
          "  dammage - %.1f, gain %.1f",
          dammage,dammage*coef*typeI->GetCost()
        );
        #endif

      }
    }

  }
  return result.gain>0;
}

inline void UseMax( int &val, int v ) {if( val<v ) val=v;}

const float TimeToAttack=5.0f; // attack aim time
const float MinTimeToLive=5.0f; // see also AIBrain::GetTimeToLive


/// Functor checking if direction is in fire angle of some weapons
class FireAngleInRangeFunc : public ITurretFunc
{
protected:
  float &_inRange; // out
  Vector3 _direction;

public:
  FireAngleInRangeFunc(float &inRange, Vector3Par direction) : _inRange(inRange), _direction(direction) {}
  bool operator () (EntityAIFull *entity, TurretContext &context)
  {
    float inRange = entity->FireAngleInRange(context, 0, _direction);
    saturateMax(_inRange, inRange);
    return false; // continue
  }
};

/// Functor checking visibility from some weapons
class VisibleFunc : public ITurretFunc
{
protected:
  float &_visible; // out
  Matrix4Val _transform;
  const EntityAIFull *_who;
  EntityAI *_ai;

public:
  VisibleFunc(float &visible, Matrix4Val transform, const EntityAIFull *who, EntityAI *ai)
    : _visible(visible), _transform(transform), _who(who), _ai(ai) {}
  bool operator () (EntityAIFull *entity, TurretContext &context)
  {
    Vector3Val weaponPos = _transform * entity->GetWeaponCenter(entity->FutureVisualState(), context, 0);
    bool headOnly = false;
    float visible = GLandscape->Visible(headOnly,weaponPos, _who, _ai, 1.5);
    saturateMax(_visible, visible);
    return false; // continue
  }
};

/// Functor checking result of attack for some weapons
class CheckAttackResultFunc : public ITurretFunc
{
protected:
  FireResult &_attackEconomicalResult; // out
  Vector3 &_attackEconomicalPos; // out
  FireResult &_attackAggresiveResult; // out
  Vector3 &_attackAggresivePos; // out
  int &_improved; // out
  Vector3Val _position;
  const Target &_attackTarget;
  float _positionCost;
  float _inRange;
  float _timeToAim;
  float _timeToLive;
  float _visible;
  float _dist;
  bool _cargo;

public:
  CheckAttackResultFunc(
    FireResult &attackEconomicalResult, Vector3 &attackEconomicalPos,
    FireResult &attackAggresiveResult, Vector3 &attackAggresivePos, int &improved,
    Vector3Val position, const Target &attackTarget, float positionCost, float inRange, float timeToAim,
    float timeToLive, float visible, float dist, bool cargo)
    : _attackEconomicalResult(attackEconomicalResult), _attackEconomicalPos(attackEconomicalPos),
    _attackAggresiveResult(attackAggresiveResult), _attackAggresivePos(attackAggresivePos), _improved(improved),
    _position(position), _attackTarget(attackTarget), _positionCost(positionCost), _inRange(inRange), _timeToAim(timeToAim),
    _timeToLive(timeToLive), _visible(visible), _dist(dist), _cargo(cargo) {}
  bool operator () (EntityAIFull *entity, TurretContext &context)
  {
    if (!context._gunner) return false;
    // check if vehicle is not too low/high
    // assume all weapons are usable in the same range
    // align vehicle with surface at given point
    // check for all weapons
    for (int w=0; w<context._weapons->_magazineSlots.Size(); w++)
    {
      FireResult result;
#if DIAG_ATTACK>3
      LogF("  weapon %d WhatShootResult", w);
#endif
      if (
        entity->WhatShootResult(
          result, _attackTarget, context, w, _inRange,
          _timeToAim, _timeToLive,
          _visible, _dist, 30 + _timeToAim, false, true // TODO: time estimation
        ))
      {
#if DIAG_ATTACK>3
        LogF("  est attack shoot %d",result.weapon);
        LogF("  sursplus %.1f",result.Surplus());
        LogF("  clean sursplus %.1f",result.CleanSurplus());
        LogF("  gain %.1f, cost %.1f, loan %.1f",result.gain,result.cost,result.loan);
#endif
        if (_cargo)
        {
          // cargo attack planned
          result.weapon = -1;
        }

        // add the positionCost to the cost, so that it is considered for both economical and aggressive results
        result.cost += _positionCost;
        // select most economical possibility
        const float conservative=0.7f;
        if( _attackEconomicalResult.Surplus()<result.Surplus()*conservative )
        {
          AIBrain *unit = entity->CommanderUnit();
          if (unit && unit->CheckEmpty(_position))
          {
            _attackEconomicalResult = result;
            _attackEconomicalPos = _position;
            _improved |= EntityAIFull::EstImproved;
  #if DIAG_ATTACK>0
            LogF(
              "  est attack econom improved %.0f, gain %.0f, cost %.0f, loan %.0f at %.1f,%.1f (dist %.1f) by %s",
              result.Surplus(),result.gain,result.cost,result.loan,
              _position.X(),_position.Z(),entity->Position().Distance(_position),GETREPORTSTACK()
            );
  #endif
          }
        }
        if( _attackAggresiveResult.CleanSurplus()<result.CleanSurplus()*conservative )
        {
          AIBrain *unit = entity->CommanderUnit();
          if (unit && unit->CheckEmpty(_position))
          {
            _attackAggresiveResult = result;
            _attackAggresivePos = _position;
            _improved |= EntityAIFull::EstImproved;
  #if DIAG_ATTACK>0
            LogF(
            "  est attack aggress improved %.0f, gain %.0f, cost %.0f, loan %.0f at %.1f,%.1f (dist %.1f) by %s",
              result.CleanSurplus(),result.gain,result.cost,result.loan,
              _position.X(),_position.Z(),entity->Position().Distance(_position),GETREPORTSTACK()
            );
  #endif
          }
        }
      }
      else
      {
#if DIAG_ATTACK>2
        LogF("  impossible");
#endif
      }
    }

    return false; // continue
  }
};

/*!
\patch_internal 1.22 Date 9/10/2001 by Ondra.
- Fixed: GetIRScanGround returned float instead of bool.
  This caused unnecessary conversions.
\patch 1.36 Date 12/13/2001 by Ondra
- Fixed: Random crash when AI was engaging target and target was deleted.
*/

int EntityAIFull::EstimateAttack(const Vector3 &hPos, float height, const EntityAIFull *who) const
{
  //ADD_COUNTER(attck,1);
  PROFILE_SCOPE_EX(thAtE, ai);

# if DIAG_ATTACK>0
    LogF("Est attack %s at %.1f,%.1f - %.1f (dist %.1f)",GETREPORTSTACK(),hPos.X(),hPos.Z(),hPos.Y(),who->Position().Distance(hPos));
#   if DIAG_ATTACK>1
      LogF(
        "  econom %.0f at %.1f,%.1f (dist %.1f)",
        _attackEconomicalResult.Surplus(),_attackEconomicalPos.X(),_attackEconomicalPos.Z(),
        who->Position().Distance(_attackEconomicalPos)
      );
      LogF(
        "  aggres %.0f at %.1f,%.1f (dist %.1f)",
        _attackAggresiveResult.CleanSurplus(),_attackAggressivePos.X(),_attackAggressivePos.Z()
        who->Position().Distance(_attackAggressivePos)
      );
#   endif
# endif
  Matrix4 transform; // predict what position will the vehicle have

  Vector3 hePos = hPos;
  AIBrain *unit = CommanderUnit();
  if( unit )
  {
    // make sure the position is ready - otherwise the CheckEmpty or FindNearestEmpty tests could block
    if (!unit->NearestEmptyReady(hePos)) return 0; // wait until empty position can be retrieved
    //unit->FindNearestEmpty(hePos);
  }

  float dx,dz;
#if _ENABLE_WALK_ON_GEOMETRY
  float posY=GLandscape->RoadSurfaceYAboveWater(hePos.X(),hePos.Z(),Landscape::FilterPrimary(), &dx,&dz);
#else
  float posY=GLandscape->RoadSurfaceYAboveWater(hePos.X(),hePos.Z(),&dx,&dz);
#endif
  Vector3 normal(-dx,1,-dz);
  if( who->GetType()->GetKind()==VAir ) normal=VUp;
  transform.SetPosition(Vector3(hePos.X(),posY+height,hePos.Z()));
  Vector3Val pos=transform.Position();
  Vector3Val tgtPos=_attackTarget->LandAimingPosition(unit);

  Vector3Val tgtRel=tgtPos-pos;
  transform.SetUpAndDirection(normal,tgtRel);
  Matrix4 invTransform(MInverseRotation,transform);

  // check is pos is not unreachable
  int ix=toIntFloor(pos.X()*InvLandGrid),iz=toIntFloor(pos.Z()*InvLandGrid);
  GeographyInfo info=GLOB_LAND->GetGeography(ix,iz);
  float typicalCost=who->GetType()->GetBaseCost(info, false, true);
  if( typicalCost>1e10 )
  {
    #if DIAG_ATTACK>2
    LogF("Field unreachable.");
    #endif
    return 0;
  }

  AIGroup *grp = unit ? unit->GetGroup() : NULL;
  if (!grp)
  {
    #if DIAG_ATTACK>3
    LogF("Est attack failed - no group");
    #endif
    return 0; // not suitable for firing
  }
  AICenter *center = grp->GetCenter();
  float exposureInit = center->GetExposureOptimistic(pos);
  float timeToLiveInit = 120;
  if (exposureInit>0)
  {
    timeToLiveInit = 120*who->GetType()->GetArmor()/exposureInit;
    saturate(timeToLiveInit,5,120);
  }
    
  // Check how much is the position suitable. unsuitable positions are: no cover, high danger
  // assign a cost to an unsuitable position give a cost
  // we prefer positions with some cover available
  float myCost = who->GetType()->GetCost();
  float uncovered = (3-info.u.howManyObjects)*0.33f; // no objects - uncovered 1, many objects, uncovered 0
  float positionCost = uncovered*0.1f*myCost;
  
  // TODO: allow exposure maps for agents
  AIUnit *aiUnit = unit->GetUnit();
  if (aiUnit)
  {
    // the exposure is cost for one second. We assume we need to stay a little bit longer there
    float timeInPlace = 10.0f;
    float exposureCost = who->CalculateExposure(ix,iz)*timeInPlace;
    positionCost += exposureCost;
    
    // we should also check the cost of getting there
    // we can estimate this by direct line traversal
    float mySecondCost = myCost/timeToLiveInit;
    positionCost += who->EstimateTravelCost(who->FutureVisualState().Position(),pos,mySecondCost);
    
    // consider: integrate somehow with A* - would give much better results
  }
  
  // I cannot be killed more than once
  if (positionCost>myCost) positionCost = myCost;
    
  int improved=0;
  float bestDist,minRange,maxRange;
  FireResult bestFire;
  who->BestFireResult(bestFire,*_attackTarget,bestDist,minRange,maxRange,30,true);
  // estimate attack at 
  // check if xPos,zPos is in circle 
  float dist2=tgtRel.SquareSize();
  if( dist2>Square(minRange) && dist2<Square(maxRange) )
  {
    // estimate surplus 
    // check if it is within tactical range
    EntityAI *ai = _attackTarget->idExact;
    float irRange = GetType()->GetIRScanRange();
    if (!ai) return 0;
    if (!ai->GetType()->GetIRTarget()) irRange = 0;
    else if (!GetType()->GetIRScanGround() && !ai->Airborne()) irRange = 0;

    float maxDistance2=Square(floatMax(irRange,TACTICAL_VISIBILITY));
    if( dist2>maxDistance2 ) return 0; // too far to see it - cannot fire 

    float inRange = 0;
    FireAngleInRangeFunc funcR(inRange, invTransform * tgtPos);
    who->ForEachTurret(funcR);
    if( inRange<0.5 ) goto Done;
    
    float visible = 0;
    VisibleFunc funcV(visible, transform, who, ai);
    who->ForEachTurret(funcV);

    improved|=EstVisibility;

    // TODO: not working on fences?
    #if DIAG_ATTACK>3
    LogF("  est attack vis %.2f",visible);
    #endif
    if( visible<MinVisibleFire )
    {
      #if DIAG_ATTACK>3
      LogF("Est attack failed");
      #endif
      return improved; // not suitable for firing
    }
    // estimate time to live at given position
    //int x = toIntFloor(Position().X() * InvLandGrid);
    //int z = toIntFloor(Position().Z() * InvLandGrid);
    //float timeToLive=CommanderUnit()->GetTimeToLive();

    float distance = pos.Distance(FutureVisualState().Position());
    float exposure = center->GetExposureOptimistic(pos);
    float timeToLive = 120;
    if (exposure>0)
    {
      timeToLive = 120*who->GetType()->GetArmor()/exposure;
      saturate(timeToLive,5,120);
    }

    float timeToAim=TimeToAttack+distance*typicalCost;
    #if DIAG_ATTACK>3
    LogF("  exposure %.1f",exposure);
    LogF("  timeToLive %.1f",timeToLive);
    LogF("  distance %.1f",distance);
    LogF("  timeToAim %.1f",timeToAim);
    #endif

    float dist=dist2*InvSqrt(dist2);

    CheckAttackResultFunc func(
      _attackEconomicalResult, _attackEconomicalPos, _attackAggresiveResult, _attackAggressivePos, improved,
      transform.Position(), *_attackTarget, positionCost, inRange, timeToAim, timeToLive, visible, dist, this != who);
    who->ForEachTurret(func);
  }
  else
  {
    #if DIAG_ATTACK>2
    LogF("  out of range");
    #endif
  }
  Done:
  #if 1
  // plan attack using cargo soldiers
  if( !(improved&EstImproved) && who->GetType()->HasCargo() )
  {
    Assert( dyn_cast<const Transport>(who) );
    const Transport *transport=static_cast<const Transport *>(who);
    // consider also cargo soldier weapons (if any)
    // select 'best' soldier in cargo
    // if there is any LAW soldier, use him
    const ManCargo &cargo=transport->GetManCargo();
    //Person *bestSoldier=NULL;
    for( int c=0; c<cargo.Size(); c++ )
    {
      Person *soldier=cargo[c];
      if( !soldier ) continue;
      if( soldier->GetGroup()!=GetGroup() ) continue;
      improved|=EstimateAttack(pos,1.1f,soldier);
      break;
    }
  }
  #endif
  #if DIAG_ATTACK>3
  LogF("Est attack done");
  #endif
  return improved;
}

int EntityAIFull::EstimateAttack( const Vector3 &hPos, float height ) const
{
  return EstimateAttack(hPos,height,this);
}

void EntityAIFull::BegAttack( Target *target )
{
  Assert( target );
  if( !_attackTarget || _attackTarget->idExact!=target->idExact )
  {
    _attackTarget=target;
    #if DIAG_ATTACK>2
    LogF("%s: Engage target %s.", cc_cast(GetDebugName()), cc_cast(target->idExact->GetDebugName()));
    #endif
    // target changed - reset information
    FireResult bestFire;
    float bestDist,minDist,maxDist;
    BestFireResult(bestFire,*target,bestDist,minDist,maxDist,30,true);
    //Vector3Val tgtPos=target->AimingPosition();
    AIBrain *unit = CommanderUnit();
    Vector3Val tgtPos=_attackTarget->LandAimingPosition(unit);
    Vector3Val relTgt=tgtPos-FutureVisualState().Position();
    Vector3Val dir=relTgt.Normalized(); // direction from this to target
    float dist=dir*relTgt; // distance to target
    if( minDist>maxDist || dist<maxDist )
    { // near attack - start some reasonable estimations
      // even if the position is not empty, we still want to use it as a starting point
      _attackAggressivePos=(FutureVisualState().Position()+tgtPos)*0.5;
      _attackEconomicalPos=FutureVisualState().Position();
    }
    else
    {
      // far attack - start near target
      // even if the position is not empty, we still want to use it as a starting point
      _attackEconomicalPos=tgtPos-dir*maxDist;
      _attackAggressivePos=tgtPos-dir*(maxDist+minDist)*0.5;
    }
    _attackEngageTime=Glob.time;
    _attackRefreshTime=Glob.time-60;
    _attackEconomicalResult.Reset();
    _attackAggresiveResult.Reset();
    EstimateAttack(FutureVisualState().Position(),GetCombatHeight());
  }
  _attackTarget=target; // update pos. info
}

void EntityAIFull::EndAttack()
{
  _attackTarget=NULL;
  _attackEconomicalResult.Reset();
  _attackAggresiveResult.Reset();
  _attackAggressivePos=VZero;
  _attackEconomicalPos=VZero;
}

ShowDiags GDiagsShown;

ShowDiags::ShowDiags()
{
  _handle = 0;
}

void ShowDiagDelegateDefault::Draw()
{
  if (_obj->RenderVisualState().Scale()>1e-3f) // no need to show zero sized or very small objects
  {
    GScene->ObjectForDrawing(_obj);    
    GScene->DrawDiagText(_text, _textPos, _textPrior, _textSize);
  }
}

void ShowDiags::ShowArrow(int &handle, int id, float timeout, RString text, Vector3Par pos, Vector3Par dir, float size, ColorVal color, float textSize)
{
  ShowDiagDelegateDefault *show = new ShowDiagDelegateDefault;
  
  LODShapeWithShadow *forceArrow=GScene->ForceArrow();
  show->_obj = new ObjectColored(forceArrow,VISITOR_NO_ID);

  show->_text = text;
  show->_textPos = pos+dir.Normalized()*size*0.8;
  show->_textSize = textSize;
  // make priority dependent on the arrow size and the text size
  float prior = size * 10;
  saturate(prior, 2, 20);
  show->_textPrior = toInt(textSize * prior);
  
  float length = forceArrow->Max()[2] - forceArrow->Min()[2];
  float scale = size/length;

  Vector3 aside=dir.CrossProduct(VAside);
  Vector3 zAside=dir.CrossProduct(VForward);
  if( zAside.SquareSize()>aside.SquareSize() ) aside=zAside;
  show->_obj->SetPosition(pos);
  show->_obj->SetOrient(dir,aside);
  show->_obj->SetPosition(show->_obj->RenderVisualState().PositionModelToWorld(forceArrow->BoundingCenter()*scale));
  show->_obj->SetScale(scale);
  show->_obj->SetConstantColor(color);
  
  Show(handle,id,timeout,show);
}
  
void ShowDiags::Show(int &handle, int id, float timeout, IShowDiagDelegate *show)
{
  int index = -1;
  if (handle<=0)
  {
    // first valid handle is 1, 0 is invalid (this is done so that static int handle; provides invalid handle)
    handle = ++_handle;
  }
  // first check if the handle,id combination has a slot assigned
  index = FindHandle(handle,id);
  if (index<0)
  {
    // new slot needed
    index = _diags.Add();
    _diags[index]._handle = handle;
    _diags[index]._id = id;
  }
  ShowDiagInfo &info = _diags[index];

  info._hideTime = Glob.time+timeout;
  info._show = show;
}

void ShowDiags::Render()
{
  // remove expired diags
  for (int i=0; i<_diags.Size(); i++)
  {
    ShowDiagInfo &info = _diags[i];
    if (info._hideTime<Glob.time)
    {
      _diags.Delete(i);
      i--;
    }
  }
  // submit all diagnostics for rendering
  for (int i=0; i<_diags.Size(); i++)
  {
    const ShowDiagInfo &info = _diags[i];
    if (info._show) info._show->Draw();
  }
}

void ShowDiags::Reset()
{
  _diags.Clear();
  // we cannot reset handles - the are used by static variables
}


bool EntityAIFull::AttackThink( FireResult &result, Vector3 &pos )
{ // true when attack is planned
  PROFILE_SCOPE_EX(thAtk,ai);
  if( !_attackTarget ) return false;
  Object *tgt=_attackTarget->idExact;
  if( !tgt ) return false;
  if (tgt->IsDamageDestroyed())
  {
    EndAttack();
    return false;
  }
  AIBrain *brain=CommanderUnit();
  if( !brain ) return false;
  if (!_attackTarget->IsKnown())
  {
    // if not know, we cannot engage
    return false;
  }

#if DIAG_ATTACK>3
  LogF("%s: Attack think", cc_cast(GetDebugName()));
#endif
  
  if (!_attackTarget->IsKnownBy(brain))
  {
    // is known - but not yet by me
    #if DIAG_ATTACK>2
    LogF("Target not known by unit");
    #endif
    pos=FutureVisualState().Position();
    return true;
  }
  // check best fire possible
  float bestDist,minDist,maxDist;
  FireResult bestFire;
  BestFireResult(bestFire,*_attackTarget,bestDist,minDist,maxDist,30,true);
  // update target information

  #if 1
  // plan attack using cargo soldiers
  if( minDist>maxDist && GetType()->HasCargo() )
  {
    Assert( dyn_cast<const Transport>(this) );
    const Transport *transport=static_cast<const Transport *>(this);
    // consider also cargo soldier weapons (if any)
    // select 'best' soldier in cargo
    // if there is any LAW soldier, use him
    const ManCargo &cargo=transport->GetManCargo();
    //Person *bestSoldier=NULL;
    for( int c=0; c<cargo.Size(); c++ )
    {
      Person *soldier=cargo[c];
      if( !soldier ) continue;
      if( soldier->GetGroup()!=GetGroup() ) continue;
      soldier->BestFireResult(bestFire,*_attackTarget,bestDist,minDist,maxDist,30,true);
      // cargo attack - no weapon
      bestFire.weapon = -1;
      break;
    }
  }
  #endif

  #if DIAG_ATTACK>2
  LogF("EngageThink best gain %.1f, cost %.1f",bestFire.gain,bestFire.cost);
  #endif
  if( minDist>maxDist ) return false; // no weapons
  if( bestFire.gain<=0 ) return false; // no gain possible
  const float goodEnough=0.9f;
  const float notBad=0.4f;
  const float bad=0.1f;

  if (bestFire.weapon>=0)
  {
    // used only for pipe bombs and time bombs
    // - primary gunner should be enough
    TurretContext context;
    if (GetPrimaryGunnerTurret(context) && bestFire.weapon < context._weapons->_magazineSlots.Size())
    {
      const AmmoType *ammo = context._weapons->GetAmmoType(bestFire.weapon);
      if (ammo)
      {
        AIBrain *gunnerBrain = context._gunner ? context._gunner->Brain() : NULL;
        if (!gunnerBrain || !gunnerBrain->LSIsAlive())
        {
          gunnerBrain = brain;
          if (!gunnerBrain || !gunnerBrain->LSIsAlive()) return false;
        }
        switch (ammo->_simulation)
        {
        case AmmoShotPipeBomb:
        case AmmoShotTimeBomb:
          {
            // place bomb as near as possible
            pos=_attackTarget->LandAimingPosition(gunnerBrain);
            // select position opposite to vehicle weapon
            // assume we always know vehicle weapon direction
            // we place bombs very near
            if (_attackTarget->idExact)
            {
              // note: eye direction is much cheaper for soldiers
              // it is also more appropriate (we want to avoid being seen)
              Vector3 weaponDir = _attackTarget->idExact->GetLookAroundDirection(FutureVisualState(), context);
              pos -= weaponDir*3;
            }
            bool found = brain->FindNearestEmpty(pos);
#if _ENABLE_WALK_ON_GEOMETRY
            pos[1]=GLandscape->RoadSurfaceYAboveWater(pos.X(),pos.Z(), Landscape::FilterPrimary());
#else
            pos[1]=GLandscape->RoadSurfaceYAboveWater(pos.X(),pos.Z());
#endif
            pos[1]+=GetCombatHeight();

#if DIAG_ATTACK>2
            LogF("Bomb result %.1f,%.1f",pos.X(),pos.Z());
#endif

            result = bestFire;

            return found;
          } 
        }
      }
    }
  }
  // init estimations
  Vector3Val tgtPos=_attackTarget->LandAimingPosition(brain)+_attackTarget->GetSpeed(brain)*5;
  if( (_attackEconomicalPos-tgtPos).SquareSizeXZ()>Square(maxDist) )
  {
    // move towards the target
    // even if the position is not empty, we still want to use it as a starting point
    Vector3 norm = (_attackEconomicalPos-tgtPos);
    Vector3Val off=norm.Normalized()*maxDist*0.7f;
    _attackEconomicalPos=tgtPos+off;
    _attackEconomicalResult.Reset();
  }
  if( (_attackAggressivePos-tgtPos).SquareSizeXZ()>Square(maxDist) )
  {
    // even if the position is not empty, we still want to use it as a starting point
    Vector3 norm = (_attackAggressivePos-tgtPos);
    Vector3Val off=norm.Normalized()*maxDist*0.5;
    _attackAggressivePos=tgtPos+off;
    _attackAggresiveResult.Reset();
  }
  
  // always check: current position, economical and attack position
  const float myHeight=GetCombatHeight();
  const float maxHeight=GetMaxCombatHeight();
  const float minHeight=GetMinCombatHeight();

  const float refreshDelay=6; // how often we refresh results we have
  int visTests=0;
  if( Glob.time-_attackRefreshTime>refreshDelay )
  {
    _attackEconomicalResult.Reset(); // forget how good the old results were - we will reevaluate the functions for them now
    _attackAggresiveResult.Reset();
    // the tests are conservative - the positions found first have highest chance of being accepted
    {
      // check current position
      #if DIAG_ATTACK>0
        REPORTSTACKITEM("CheckCurrent");
      #endif
      if( EstVisibility&EstimateAttack(FutureVisualState().Position(),GetCombatHeight()) ) visTests++;
    
    }
    {
      // check position at optimal distance from target
      #if DIAG_ATTACK>0
        REPORTSTACKITEM("CheckOptiDist");
      #endif
      Vector3 norm = (FutureVisualState().Position()-tgtPos);
      Vector3Val pos= tgtPos+norm.Normalized()*bestDist;
      if( EstVisibility&EstimateAttack(pos,GetCombatHeight()) ) visTests++;
      
      if (CHECK_DIAG(DECombat))
      {
        static int handle;
        GDiagsShown.ShowArrow(handle,ID().Encode(),10,"CheckOptiDist",pos,VUp,3,Color(0,0,1));
      }
    }
    {
      #if DIAG_ATTACK>0
        REPORTSTACKITEM("CheckEconom");
      #endif
      if( EstVisibility&EstimateAttack(_attackEconomicalPos,GetCombatHeight()) ) visTests++;
      if (CHECK_DIAG(DECombat))
      {
        static int handle;
        GDiagsShown.ShowArrow(handle,ID().Encode(),10,"CheckEconom",_attackEconomicalPos,VUp,3,Color(0,1,0));
      }
    }
    {
      #if DIAG_ATTACK>0
        REPORTSTACKITEM("CheckAggress");
      #endif
      // check best know economical and aggressive positions
      if( EstVisibility&EstimateAttack(_attackAggressivePos,GetCombatHeight()) ) visTests++;
      if (CHECK_DIAG(DECombat))
      {
        static int handle;
        GDiagsShown.ShowArrow(handle,ID().Encode(),10,"CheckAggress",_attackAggressivePos,VUp,3,Color(1,0,0));
      }
    }
    
    // if no new results are found, we should still use the old ones
    // this may happen e.g. when target is moving, and old position are not confirmed to be valid any longer
    _attackRefreshTime=Glob.time;
  }
  
  // perform some attack iterations
  // random spread around aggressive and economical points
  // if we have some good enough economical position, do not try to improve it
  if( _attackEconomicalResult.Surplus()>=goodEnough*bestFire.Surplus() )
  {
    pos=_attackEconomicalPos;
    result=_attackEconomicalResult;
    #if DIAG_ATTACK>1
    LogF("Good EconomicalResult %.1f,%.1f",pos.X(),pos.Z());
    #endif
    return true;
  }

  bool noSolution=true;
  bool noEconomSolution=true;
  if( _attackEconomicalResult.Surplus()>=notBad*bestFire.Surplus() )
  {
    noSolution=false;
    noEconomSolution=false;
  }
  else if( _attackAggresiveResult.CleanSurplus()>=notBad*bestFire.CleanSurplus() )
  {
    noSolution=false;
  }

  // random height for helicopters/plane
  //const float spread=noSolution ? 100 : 50;
  const float spread=noSolution ? 200 : 100;
  int maxTests=noSolution ? 8 : 2;
  int maxN=noSolution ? 8 : 2;
  for( int i=maxN; --i>=0 && visTests<maxTests; )
  {
    // try to create a new result by placing new random samples around the target
    
    {
      #if DIAG_ATTACK>0
        REPORTSTACKITEM("Random");
      #endif
      // polar coordinates - angle + radius
      float a=GRandGen.RandomValue()*(2*H_PI);
      float r=GRandGen.RandomValue();
      // note: for uniform distribution we need to use sqrt(r)
      // we need random result between minDist/maxDist..1
      // ==== sqrt(r)>=minDist/maxDist === r>=(minDist/maxDist)^2
      float skipR = Square(minDist/maxDist);
      r = r-r*skipR+skipR; // r*(1-skipR) + skipR
      float sqrtR = sqrt(r)*maxDist; // result between minDist..maxDist
      float xRand = sqrtR*sin(a), zRand = sqrtR*cos(a);
      
      float height=myHeight;
      if( maxHeight>minHeight )
      {
        float yRand=GRandGen.RandomValue()+GRandGen.RandomValue();
        if( yRand>1 ) height=myHeight+(maxHeight-myHeight)*(yRand-1);
        else height=minHeight+(myHeight-minHeight)*yRand;
      }
      Vector3 pos = tgtPos+Vector3(xRand,0,zRand);
      if( EstVisibility&EstimateAttack(pos,height) ) visTests++;
      if (CHECK_DIAG(DECombat))
      {
        static int handle;
        // avoid diagnostics being under the ground
        saturateMax(pos[1], GLandscape->SurfaceY(pos));
        GDiagsShown.ShowArrow(handle,ID().Encode(),10,"Random",pos,VUp,3,Color(1,1,0));
      }
    }
    
    // try to improve economical result by using random samples around it
    {
      #if DIAG_ATTACK>0
        REPORTSTACKITEM("ImproveEconom");
      #endif
      float xRand=GRandGen.RandomValue()*spread*2-spread;
      float zRand=GRandGen.RandomValue()*spread*2-spread;
      float height=myHeight;
      if( maxHeight>minHeight )
      {
        float yRand=GRandGen.RandomValue()+GRandGen.RandomValue();
        if( yRand>1 ) height=myHeight+(maxHeight-myHeight)*(yRand-1);
        else height=minHeight+(myHeight-minHeight)*yRand;
      }
      Vector3Val pos=_attackEconomicalPos+Vector3(xRand,0,zRand);
      if( EstVisibility&EstimateAttack(pos,height) ) visTests++;
      if (CHECK_DIAG(DECombat))
      {
        static int handle;
        GDiagsShown.ShowArrow(handle,ID().Encode(),10,"ImproveEconom",pos,VUp,3,Color(0,1,0));
      }
    }
    // if we have only aggressive result, update it
    if( noEconomSolution && !noSolution )
    {
      #if DIAG_ATTACK>0
        REPORTSTACKITEM("ImproveAggress");
      #endif
      float xRand=GRandGen.RandomValue()*spread*2-spread;
      float zRand=GRandGen.RandomValue()*spread*2-spread;
      float height=myHeight;
      if( maxHeight>minHeight )
      {
        float yRand=GRandGen.RandomValue()+GRandGen.RandomValue();
        if( yRand>1 ) height=myHeight+(maxHeight-myHeight)*(yRand-1);
        else height=minHeight+(myHeight-minHeight)*yRand;
      }
      Vector3Val pos=_attackAggressivePos+Vector3(xRand,0,zRand);
      if( EstVisibility&EstimateAttack(pos,height) ) visTests++;
      if (CHECK_DIAG(DECombat))
      {
        static int handle;
        GDiagsShown.ShowArrow(handle,ID().Encode(),10,"ImproveAggress",pos,VUp,3,Color(1,0,0));
      }
    }
  }

  #if 0
    GLOB_ENGINE->ShowMessage
    (
      500,"Est attack ec %.3f ag %.3f",
      _attackEconomicalResult.Surplus()/bestFire.Surplus(),
      _attackAggresiveResult.CleanSurplus()/bestFire.CleanSurplus()
    );
    LogF
    (
      "Est attack ec %.3f ag %.3f",
      _attackEconomicalResult.Surplus()/bestFire.Surplus(),
      _attackAggresiveResult.CleanSurplus()/bestFire.CleanSurplus()
    );
  #endif

  // if not good enough, wait - maybe it will be better
  if( Glob.time-_attackEngageTime<5.0 )
  {
    #if DIAG_ATTACK>2
    LogF("  time %.1f",Glob.time-_attackEngageTime);
    #endif
    return false;
  }
  #if DIAG_ATTACK>2
    LogF("  econom best %.1f",bestFire.Surplus());
    LogF("         curr %.1f",_attackEconomicalResult.Surplus());
    LogF("  aggres best %.1f",bestFire.CleanSurplus());
    LogF("         curr %.1f",_attackAggresiveResult.CleanSurplus());
  #endif
  if( _attackEconomicalResult.Surplus()>=bad*bestFire.Surplus() )
  {
    pos=_attackEconomicalPos;
    result=_attackEconomicalResult;
    #if DIAG_ATTACK>2
    LogF("Bad EconomicalResult %.1f,%.1f",pos.X(),pos.Z());
    #endif
    return true;
  }
  else if( _attackAggresiveResult.CleanSurplus()>=bad*bestFire.CleanSurplus() )
  {
    pos=_attackAggressivePos;
    result=_attackAggresiveResult;
    #if DIAG_ATTACK>2
    LogF("Bad AggresiveResult %.1f,%.1f",pos.X(),pos.Z());
    #endif
    return true;
  }
  return false;
}

bool EntityAIFull::AttackReady()
{
  float bestDist,minDist,maxDist;
  FireResult bestFire;
  BestFireResult(bestFire,*_attackTarget,bestDist,minDist,maxDist,30,true);

  #if 1
  // plan attack using cargo soldiers
  if( minDist>maxDist && GetType()->HasCargo() )
  {
    Assert( dyn_cast<const Transport>(this) );
    const Transport *transport=static_cast<const Transport *>(this);
    // consider also cargo soldier weapons (if any)
    // select 'best' soldier in cargo
    // if there is any LAW soldier, use him
    const ManCargo &cargo=transport->GetManCargo();
    //Person *bestSoldier=NULL;
    for( int c=0; c<cargo.Size(); c++ )
    {
      Person *soldier=cargo[c];
      if( !soldier ) continue;
      if( soldier->GetGroup()!=GetGroup() ) continue;
      soldier->BestFireResult(bestFire,*_attackTarget,bestDist,minDist,maxDist,30,true);
      // cargo attack - no weapon
      bestFire.weapon = -1;
      break;
    }
  }
  #endif

  const float bad=0.1f;
  if( _attackEconomicalResult.Surplus()>=bad*bestFire.Surplus() )
  {
    return true;
  }
  else if( _attackAggresiveResult.CleanSurplus()>=bad*bestFire.CleanSurplus() )
  {
    return true;
  }
  return false;
}

#define HIDE_DIAGS 0


Target *EntityAIFull::FindNearestEnemy(Vector3Par pos)
{
  AIBrain *unit = CommanderUnit();
  if (!unit) return NULL;

  Target *target = NULL;
  const TargetList *targetList = unit->AccessTargetList();
  if (targetList)
  {
    float minFunc = 1e10;
    // in stealth we need to track Unknown as well
    // enemy list currently contains enemies which were not identified yet
    for (int i=0; i<targetList->EnemyCount(); i++)
    {
      Target *tar = targetList->GetEnemy(i);
      if (!tar->idExact) continue;
      if (tar->idExact->IsDamageDestroyed()) continue;
      if (tar->IsVanishedOrDestroyed()) continue;
      if (tar->idExact->IsArtilleryTarget()) continue;
      if (!tar->IsKnownBy(unit) ) continue;
      // do not hide in front of yourself (could happen with renegade status)
      if (tar->idExact==unit->GetVehicle()) continue;
      // in stealth hide also from unknown targets
      TargetSide side = tar->GetSide();
      if( unit->GetCombatMode()!=CMStealth )
      {
        if (!unit->IsEnemy(side)) break;
      }
      else
      {
        if (!unit->IsEnemy(side) && side!=TSideUnknown ) break;
      }

      // TODO: better hide target selection
      float func=tar->AimingPosition().Distance(pos);
      if( func<minFunc )
      {
        minFunc = func;
        target  = tar;
      }
    }
  }
  return target;
}

#if _VBS3
float EntityAIFull::GetTotalWeightKg()
{
  float weight = base::GetTotalWeightKg();

  for (int i=0; i<_weaponsState._weapons.Size(); i++)
  {
    const WeaponType *w = _weaponsState._weapons[i];
    if (!w) continue;
    weight += w->GetMass();
  }
  for (int i=0; i<_weaponsState._magazines.Size(); i++)
  {
    const Magazine *m = _weaponsState._magazines[i];
    if (!m) continue;
    const MagazineType *mType = m->_type;
    if (!mType || mType->_maxAmmo == 0) continue;      
    float ammoRatio = (float)m->GetAmmo() / (float)mType->_maxAmmo;
    weight += ammoRatio * mType->_mass;
  }

  return weight;
}
#endif

bool EntityAIFull::CheckTargetKnown(const TurretContext &context, Target * target) const
{
  if (context._gunner)
  {
    AIBrain *brain = context._gunner->Brain();
    if (!brain || !brain->LSIsAlive() || !target->IsKnownBy(brain))
    {
      // we cannot aim until we know the target
      return false;
    }
  }
  return true;
}



float EntityAIFull::FireAngleInRange(const TurretContext &context, int weapon, Vector3Par rel) const
{
  // most vehicle cannot fire high and low
  float size2=rel.SquareSizeXZ();
  if (size2 == 0.0f) return 0;
  float y2=Square(rel.Y());
  const float maxY=0.5;
  if( y2>size2*Square(maxY) ) return 0;
  // nearly same level
  return 1-fabs(rel.Y())*InvSqrt(size2)*(1/maxY);
}

void EntityAIFull::FireAimingLimits(float &minAngle, float &maxAngle, const TurretContext &context, int weapon) const
{
  // by default assume no aiming is possible and body needs to be turned
  // this is safest setting as it makes sure all vehicles can engage their targets
  minAngle = maxAngle = 0;
}

/**
@param seenBefore [out] how much time elapsed since we had clean line of sight to the target
*/
float EntityAIFull::GetAimingPosition(Vector3 &pos, Vector3 &speed, float &seenBefore,
  bool exact, const TurretContext &context, Target *target, const AmmoType *ammo) const
{
  seenBefore = FLT_MAX;
  if (!context._gunner)
    return 0;
  AIBrain *brain = context._gunner->Brain();
  if (!brain)
    return 0;
  if (exact)
  {
    pos = target->idExact->AimingPosition(target->idExact->RenderVisualState());
    speed = target->idExact->PredictVelocity();
    seenBefore = 0;
    return 1;
  }
  else if (CheckSuppressiveFire()<SuppressYes)
  {
    bool headOnly = false;
    float visible=0;
    if (target->idExact && target->GetLastSeen()>Glob.time-5)
      visible = _visTracker.Value(headOnly,this, context, context._weapons->ValidatedCurrentWeapon(), target->idExact, 0.9f);
    seenBefore = 0;
    pos = target->LandAimingPosition(brain,ammo,headOnly);
    speed = target->GetSpeed(brain);

    // decrease landscape occlusion significance (see bellow)
    visible=1-(1-visible)*0.3f;
    return visible;
  }
  else
  {
    bool headOnly = false;
    float visible=0;
    if (target->idExact && target->GetLastSeen()>Glob.time-5)
    { // do not test visibility 
      int selected = context._weapons->ValidatedCurrentWeapon();
      visible = _visTracker.Value(headOnly,this, context, selected, target->idExact, 0.9f);
      if (visible<0.1f)
      {
        Time time;
        visible  = _visTracker.ValueForLastVisible(time,this,context, selected, target->idExact, 0.9f);
        if (visible>0)
        {
          seenBefore = Glob.time.Diff(time);
        }
      }
      else
        seenBefore = 0;
    }

    bool isExact = false;
    pos = target->LastKnownPosition(brain,ammo,headOnly,&isExact);

    speed = target->GetSpeed(brain);
    if (!isExact && target->idExact)
    {
      // when LastKnownPosition did not return an exact position, but we have seen the target recently, we want to check _visTracker cache
      Vector3 lastPos;
      bool knownPos = _visTracker.GetLastKnownVisiblePosition(lastPos,target->idExact);
      if (knownPos)
      {
        pos = lastPos;
        // we are aiming at a point in a space, not at the target - point is not moving
        speed = VZero;
      }
    }

    if(target && target->idExact && target->idExact->IsArtilleryTarget()) 
    { //artillery target is always good to aim
      pos = target->idExact->FutureVisualState().Position();
      return 1;
    }

    // TODO: improved handling for large or non-spherical objects
    // we should be interested only in the visibility of the area we are aiming at
    // decrease landscape occlusion significance
    visible=1-(1-visible)*0.3f;
    return visible;
  }
}

float EntityAIFull::GetSuppressiveFireBoost() const
{
  return CheckSuppressiveFire()>=SuppressIntense ? 0.33f : 1.0f;
}
/**
@param boost in some situations we want suppressive fire to be more intense. Smaller number means more intense fire.
*/
float EntityAIFull::SuppressiveFireInterval(float seenBefore, const TurretContext &context, float boost) const
{
  // if the target is too old, stop suppressing it
  if (seenBefore>300)
    return FLT_MAX;
  // count ammo which can be used for suppressive fire
  int nSupressiveAmmo = 0;
  for (int a=0; a<context._weapons->_magazines.Size(); a++)
  {
    const Magazine *mag = context._weapons->_magazines[a];
    if (!mag || !mag->_type)
      continue;
    const MagazineType *type = mag->_type;
    // we want to suppress using bullets only
    if (!type->_ammo || (type->_ammo->_simulation!=AmmoShotBullet && type->_ammo->_simulation!=AmmoShotSpread))
      continue;
    nSupressiveAmmo += mag->GetAmmo();
  }
  float lotOfAmmoFactor = InterpolativC(nSupressiveAmmo,150,300,1.0f,0.5f);
  // the longer the target was not seen, the longer will the pauses between firing be
  // we cannot use GetLastSeen, as the target may be seen by one unit only
  float agePause = seenBefore<30 ? InterpolativC(seenBefore,0,30,0,15) : InterpolativC(seenBefore,30,120,15,30);

  // the less ammo we have, the longer pause between the shots we want
  float ammoLowPause = InterpolativC(nSupressiveAmmo,20.0f,150.0f,15.0f,0.0f);
  // with a lot of ammo, we may want to provide a more reliable suppression even on older targets

#if _ENABLE_CHEATS
  if (CHECK_DIAG(DECombat) && GWorld->GetObserver()==this)
  {
    float maxAge = InterpolativC(nSupressiveAmmo,100,300,60,240);
    DIAG_MESSAGE(1000,Format("%s: Suppressive pause: A %.1f*%.1f, C %.1f (age %.1f<%.1f, ammo %d), last shot %.1f",
      cc_cast(GetDebugName()),agePause,lotOfAmmoFactor,ammoLowPause,seenBefore,maxAge,nSupressiveAmmo,
      Glob.time-context._weapons->_lastShotTime));
  }
#endif

  return floatMax(agePause*lotOfAmmoFactor,ammoLowPause)*boost;
}


/// helper for EntityAIFull::BestFireResult
class BestFireResultForTurret: public ITurretFuncV
{
  friend class EntityAIFull;
  
  const Target &_target;
  /// result decision
  FireResult &_result;
  ///@{ results
  float _bestDist;
  float _minDist;
  float _maxDist;
  float _timeToShoot;
  bool _enableAttack;
  bool _someFire;
  //@}
  
  public:
  BestFireResultForTurret(
    FireResult &result, const Target &target,
    float timeToShoot, bool enableAttack
  ):_result(result),_target(target),_timeToShoot(timeToShoot),_enableAttack(enableAttack)
  {
    _bestDist=600; // default best distance - be far
    _minDist=+FLT_MAX;
    _maxDist=-FLT_MAX;
    _someFire=false;
  }
  virtual bool operator () (EntityAIFull *entity, TurretContextV &context)
  {
    if (!context._gunner) return false;
    
    if( !entity->IsAbleToFire(context) ) return false;

    if( !entity->CommanderUnit() ) return false;

    float timeToLive=entity->CommanderUnit()->GetTimeToLive();

    for (int w=0; w<context._weapons->_magazineSlots.Size(); w++ )
    {
      const MagazineSlot &slot = context._weapons->_magazineSlots[w];
      if (_enableAttack && !slot._muzzle->_enableAttack) continue;
      const WeaponModeType *mode = slot._weaponMode;
      if (!mode) continue;
      //counterMeasures are not for assault 
      if(context._weapons->_magazineSlots[w]._weapon->_simulation == WSCMLauncher) continue;

      float aMinDist = mode->minRange * 0.75f + mode->midRange * 0.25f;
      float aMaxDist = mode->midRange * 0.25f + mode->maxRange * 0.75f;
      // check max. possible gain with this weapon
      float bestDistance=BestDistance(*mode);
      // we want to avoid being too close or too far, even if that may be sometimes optimal in terms of hit probability
      saturate(bestDistance,aMinDist,aMaxDist);

      // we cannot engage over the sensory range
      float irRange = entity->GetType()->GetIRScanRange();
      const EntityAIType *tgtType = _target.GetType();
      if (!tgtType->GetIRTarget()) irRange = 0;
      else if (!entity->GetType()->GetIRScanGround() && (!_target.idExact || !_target.idExact->Airborne())) irRange = 0;
      float maxDistance= floatMax(irRange,TACTICAL_VISIBILITY);
      
      if (aMinDist>maxDistance) continue; // only out of sensory range - cannot use
      // clamp max values to be within the sensory range
      if (bestDistance>maxDistance) bestDistance = maxDistance;
      if (aMaxDist>maxDistance) aMaxDist = maxDistance;

      #if DIAG_SHOOT_RESULT
        LogF("  BestFireResult %d",w);
      #endif

      FireResult tResult;
      if(
        entity->WhatShootResult(
          tResult,_target,context,w,1.0,
          0,timeToLive,1.0,bestDistance,_timeToShoot,false,true // TODO: timeToAim argument
        )
      )
      {
        // only when we have some possible result from the best distance with given weapon,
        // use its ranges to saturate the possible ranges
        saturateMin(_minDist, aMinDist);
        saturateMax(_maxDist, aMaxDist);
        if( tResult.CleanSurplus()>_result.CleanSurplus() )
        {
          _result=tResult;
          _someFire=true;
          _bestDist=bestDistance;
        }
      }
    }
    return false;
  }
};

bool EntityAIFull::BestFireResult(
  FireResult &result, const Target &target,
  float &bestDist, float &minDist, float &maxDist,
  float timeToShoot, bool enableAttack
) const
{
  BestFireResultForTurret bestResult(result,target,timeToShoot,enableAttack);
  ForEachTurret(unconst_cast(FutureVisualState()), bestResult);
  bestDist = bestResult._bestDist;
  minDist = bestResult._minDist;
  maxDist = bestResult._maxDist;
  return bestResult._someFire;
}

bool EntityAIFull::WhatAttackResult(
  FireResult &result, const Target &target, const TurretContext &context, float timeToShoot
) const
{
  if( !CommanderUnit() ) return false;

  // if vehicle cannot move, it cannot engage
  if (GetType()->GetMaxSpeedMs()<=0) return false;
  
  #if DIAG_RESULT
    LogF
    (
      "WhatAttackResult %s to %s",
      (const char *)GetDebugName(),(const char *)target.GetType()->GetDisplayName()
    );
  #endif

  bool someFire=false;

  if (IsAbleToFire(context))
  {
    //int x = toIntFloor(Position().X() * InvLandGrid);
    //int z = toIntFloor(Position().Z() * InvLandGrid);
    float timeToLive=CommanderUnit()->GetTimeToLive();

    // keep timeToLive in reasonable dimensions
    // even if we are safe now, we will not be safe when attacking
    saturate(timeToLive,5,60);

    // include time to attack
    float invSpeed = 2.0f/GetType()->GetMaxSpeedMs();
    float targetDistance = target.AimingPosition().Distance(FutureVisualState().Position());

    for (int w=0; w<context._weapons->_magazineSlots.Size(); w++)
    {
      const MagazineSlot &slot = context._weapons->_magazineSlots[w];
      if (!slot._muzzle->_enableAttack) continue;
      const WeaponModeType *mode = slot._weaponMode;
      if (!mode) continue;
      //counterMeasures are not for assault 
      if(context._weapons->_magazineSlots[w]._weapon->_simulation == WSCMLauncher) continue;

      // check max. possible gain with this weapon
      float bestDistance=BestDistance(*mode);

      float travelDistance = targetDistance - bestDistance;
      saturateMax(travelDistance,0);

      #if DIAG_RESULT
        LogF
        (
          "bestDist %.1f, travel %.1f",bestDistance,travelDistance
        );
      #endif

      // estimate travel time
      float travelTime = travelDistance*invSpeed;
      if (travelTime>60)
      {
        #if DIAG_RESULT
        LogF("   traveltime %3.1f",travelTime);
        #endif
        continue;
      }
      const float timeToAim=TimeToAttack + travelTime;
      FireResult tResult;
      if
      (
        WhatShootResult
        (
          tResult,target,context,w,1.0,timeToAim,timeToLive,
          1.0,bestDistance,timeToShoot,false,true
        )
      )
      {
        if( tResult.CleanSurplus()>result.CleanSurplus() )
        {
          result=tResult;
          someFire=true;
        }
      }
    }
  }

  #if 1
    // consider soldier attack
    if( !someFire && GetType()->HasCargo() )
    {
      // no weapons - check if we have some soldiers in cargo
      Assert( dyn_cast<const Transport>(this) );
      const Transport *transport=static_cast<const Transport *>(this);
      // select 'best' soldier in cargo
      // if there is any LAW soldier, use him
      const ManCargo &cargo=transport->GetManCargo();
      for( int c=0; c<cargo.Size(); c++ )
      {
        Person *soldier=cargo[c];
        if( !soldier ) continue;
        if( soldier->GetGroup()!=GetGroup() ) continue;
        FireResult tResult;
        float bestDist,minDist,maxDist;
        if( soldier->BestFireResult(tResult,target,bestDist,minDist,maxDist,timeToShoot,true) )
        {
          result=tResult;
          result.weapon = -1;
          someFire=true;
        }
        break;
      }
    }
  #endif
  #if DIAG_RESULT
    LogF
    (
      "  dammage %.2f, gain %.0f, cost %.0f, loan %.0f, weapon %d",
      result.damage,result.gain,result.cost,result.loan,result.weapon
    );
  #endif
  return someFire;
}

float EntityAIFull::GetTrackedVisibility(
  bool &headOnly,
  AIBrain *unit, const TurretContext &context, int weapon, const Target &tgt, float reserve
) const
{
  // if visibility is tracked, check it
  float visibility = _visTracker.KnownValue(headOnly,this,context,weapon,tgt.idExact,reserve);
  if( visibility<0 )
  {
    // if not, check visibility matrix
    // TODO: add head-only into World::Visibility
    headOnly = false;
    visibility = GWorld->Visibility(unit,tgt.idExact);
  }
  return visibility;
}

bool EntityAIFull::SomeFirePossible(const TurretContext &context, const Target &target, bool onlyAutoFire) const
{
  if (!IsAbleToFire(context))
  {
#if DIAG_RESULT
    LogF( "  cannot fire.");
#endif
    return false;
  }
  for (int w=0; w<context._weapons->_magazineSlots.Size(); w++)
  {
    if (onlyAutoFire)
    {
      const WeaponModeType *mode = context._weapons->_magazineSlots[w]._weaponMode;
      if (!mode || !mode->_autoFire) continue;
    }
    //counterMeasures are not for assault 
    if(context._weapons->_magazineSlots[w]._weapon->_simulation == WSCMLauncher) continue;

    float timeToAim = 0;
    float inRange = FireInRange(context, w, timeToAim, target);
#if DIAG_RESULT
    WeaponType *weapon = context._weapons->_magazineSlots[w]._weapon;
    LogF(" weapon %s: inRange %.3f", weapon ? cc_cast(weapon->GetName()) : "NULL", inRange);
#endif
    if (inRange <= 1e-6) continue;
    const Magazine *magazine = context._weapons->_magazineSlots[w]._magazine;
    if (!magazine)
    {
#if DIAG_RESULT
      LogF( "  no magazine");
#endif
      continue;
    }
    const MagazineType *type = magazine->_type;
    if (!type) continue;
    //LogF("  magazine %s",(const char *)type->GetDisplayName());
    const AmmoType *ammo = type->_ammo;
    if (!ammo)
    {
#if DIAG_RESULT
      LogF( "  no ammo");
#endif
      continue;
    }

#if DIAG_RESULT
    LogF(" fire possible");
#endif
    return true; // found
  }
  return false;
}

float EntityAIFull::AdjustedTimeToLive( AIBrain * unit, const Target &target, float visibility ) const
{
  // estimate time to live
  //int x = toIntFloor(Position().X() * InvLandGrid);
  //int z = toIntFloor(Position().Z() * InvLandGrid);
  float timeToLive = unit->GetTimeToLive();
  // timeToLive computed from threat maps may be quite old
  // we consider the current target to make sure we are not underestimating obvious threats

  if (target.idExact && target.idExact->GetTotalDamage()<MaxDammageWorking && target.HasDiclosedUs())
  {
    float dist2 = Square(target.Distance(FutureVisualState().Position()));
    Threat threat=target.GetType()->GetDamagePerMinute(dist2,visibility);
    VehicleKind myKind=GetType()->GetKind();
    float damagePerMinute=threat[myKind];
    if( damagePerMinute>0 )
    {
      // my expected time to live when engaged by this target
      float lTimeToLive=GetArmor()*60/damagePerMinute;
      timeToLive = floatMin(timeToLive,lTimeToLive);
    }
  }
  return floatMax(timeToLive,MinTimeToLive);
}

/**
@param assumeVisAtLeast sometimes we want to check as if there is Line Of Sight even if in fact there is none
*/
EntityAIFull::FirePossibility EntityAIFull::FirePossible(
  const TurretContext &context,
  FireResult &result, const Target &target, float timeToShoot, AIBrain *unit, bool suppressiveFire, bool onlyAutoFire
) const
{
  float maxSurplus = -1e10f;
  FirePossibility possible = FPCannot;

  EntityAI *tgd = target.idExact;

  // ??? - assume we always see friendly targets and can always fire at them? Why?
  float visibility = 1;
  TargetSide side = target.GetSide();
  if(tgd && tgd->IsArtilleryTarget())
  {//artillery does not need to see the target
    visibility = 1; 
  }
  else if (!unit->IsFriendly(side))
  {
    int selected = context._weapons->ValidatedCurrentWeapon();
    if (target.GetLastSeen()<Glob.time-5)
    { // when target was not seen recently, do not perform an exact test
      visibility = 0;
    }
    else
    {
      // if visibility is tracked, check it
      bool headOnly = false;
      // fast - accurate only when tracked, otherwise only from the matrix
      visibility = GetTrackedVisibility(headOnly,unit, context, selected, target, 0.9f);
    }
    if (suppressiveFire && visibility<MinVisibleFire && target.idExact)
    {
      // no visibility at the target - we try if we have a visibility against the suppressed position
      Time time;
      float supVis  = _visTracker.ValueForLastVisible(time,this,context, selected, target.idExact, 0.9f);
      float seenBefore = Glob.time.Diff(time);
      if (seenBefore<180) // suppression max. 3 minutes after seeing the target
        visibility = floatMax(visibility,supVis);
    }
  }

  if (visibility < MinVisibleFire)
  {
#if DIAG_RESULT
    LogF( "  visibility low (%.3f)", visibility);
#endif
    return FPCannot; // cannot fire by this turret
  }

  float timeToLive = AdjustedTimeToLive(unit, target, visibility);
  for (int w=0; w<context._weapons->_magazineSlots.Size(); w++)
  {
    float timeToAim = 0;
    float inRange = FireInRange(context, w, timeToAim, target);
#if DIAG_RESULT
    WeaponType *weapon = context._weapons->_magazineSlots[w]._weapon;
    LogF(" weapon %s: inRange %.3f", weapon ? cc_cast(weapon->GetName()) : "NULL", inRange);
#endif
    if (inRange <= 1e-6) continue;
    const Magazine *magazine = context._weapons->_magazineSlots[w]._magazine;
    if (!magazine) continue;
    const MagazineType *type = magazine->_type;
    if (!type) continue;
    //LogF("  magazine %s",(const char *)type->GetDisplayName());
    const AmmoType *ammo = type->_ammo;
    if (!ammo) continue;
    //counterMeasures are not for assault 
    if(context._weapons->_magazineSlots[w]._weapon->_simulation == WSCMLauncher) continue;

    if (!suppressiveFire)
    {
      // target position not known - no fire
      if (!target.AccurateEnoughForHit(ammo,unit))
      {
  #if DIAG_RESULT
        LogF("   inaccurate to hit");
  #endif
        continue;
      }

      // target not seen recently - no fire
      if (target.GetLastSeen() < Glob.time - 10 &&  !(tgd && tgd->IsArtilleryTarget()))
      {
  #if DIAG_RESULT
        LogF("   lastSeen %.1f", Glob.time - 10 - target.GetLastSeen());
  #endif
        continue;
      }
    }
    else
    {
      // use only bullets for suppressive fire
      // TODO: sometimes consider using other ammo types as well?
      if (ammo->_simulation!=AmmoShotBullet && ammo->_simulation!=AmmoShotSpread) continue;
    }

    const WeaponModeType *mode = context._weapons->_magazineSlots[w]._weaponMode;
    Assert(mode);
    if (onlyAutoFire)
    {
      if (!mode->_autoFire) continue;
    }


    float timeToReload = magazine->_reload * magazine->_reloadDuration * mode->_reloadTime + magazine->_reloadMagazine;
    // time to aim is often zero or close to zero
    timeToAim = floatMax(timeToReload, timeToAim);
    //if (inRange <= 0.5) continue;
    float distance = target.Distance(FutureVisualState().Position());
    FireResult tResult;
#if DIAG_RESULT
    LogF(" times: live %.3f, aim %.3f", timeToShoot, timeToAim);
#endif
    if (!WhatShootResult(
      tResult, target, context, w, inRange,
      timeToAim, timeToLive,
      visibility, distance, timeToShoot, true, false))
    {
      continue;
    }

    // we can get some result - evaluate it
    float surplus = tResult.Surplus();
    if (timeToReload > timeToShoot)
    { // do not have enough time to shoot - make result worse
      surplus *= 0.2;
    }
    if (maxSurplus < surplus)
    {
      possible = FPCan;
      maxSurplus = surplus;
      result = tResult;
    }
  }
  return possible;
}

/*!
\patch_internal 1.24 Date 9/20/2001 by Ondra.
- Fixed: destroyed target (idExact==NULL) was sometimes checked for visibility.
*/

EntityAIFull::FirePossibility EntityAIFull::WhatFireResult(
  FireResult &result, const Target &target, const TurretContext &context, float timeToShoot, bool suppresiveFire, bool onlyAutoFire
) const
{
  result.weapon=-1;
  #if DIAG_RESULT
    LogF
    (
      "WhatFireResult %s to %s",
      (const char *)GetDebugName(),(const char *)target.GetType()->GetDisplayName()
    );
  #endif

  AIBrain *unit = CommanderUnit();
  if( !unit ) return FPCannot;
  if (unit->GetLifeState()==LifeStateDead) return FPCannot;

  if (!target.idExact) return FPCannot;

  // first of all quick check if any of the weapons we have can fire at given target
  // FIX: check only single turret
  bool someFirePossible = SomeFirePossible(context, target, onlyAutoFire);
  
  FirePossibility possible = FPCannot;
  if (someFirePossible)
  {
    // FIX: check only single turret
    possible = FirePossible(context, result, target, timeToShoot, unit, suppresiveFire, onlyAutoFire);
  }
  #if DIAG_RESULT
    LogF
    (
      "  dammage %.2f, gain %.0f, cost %.0f, loan %.0f, weapon %d",
      result.damage,result.gain,result.cost,result.loan,result.weapon
    );
    if( possible==FPCannot ) LogF("  impossible");
    else if( possible==FPCannotNow ) LogF("  impossible now");
  #endif
  return possible;
}

EntityAIFull::FirePossibility EntityAIFull::WhatFireResult(
  FireResult &result, const Target &target, const TurretContext &context, int weapon, float timeToShoot, bool suppresiveFire
) const
{
  result.weapon = weapon;
  #if DIAG_RESULT
    LogF
    (
      "WhatFireResult %s to %s (using %d)",
      (const char *)GetDebugName(),(const char *)target.GetType()->GetDisplayName(),weapon
    );
  #endif
  if( !IsAbleToFire(context) )
  {
    #if DIAG_RESULT
      LogF( "  cannot fire.");
    #endif
    return FPCannot; // cannot fire
  }

  AIBrain *unit = CommanderUnit();
  if (!unit) return FPCannot;
  if (unit->GetLifeState()==LifeStateDead) return FPCannot;

  if (!target.idExact) return FPCannot;

  // if visibility is tracked, check it
  bool headOnly = false;
  float visibility = 1;
  if (!suppresiveFire)
  {
    // is cheap: only check, never perform the test
    visibility = _visTracker.KnownValue(headOnly,this, context, context._weapons->ValidatedCurrentWeapon(),target.idExact,0.9f);
    if( visibility<0 )
    {
      visibility = GLOB_WORLD->Visibility(unit,target.idExact);
      // special handling of friendly units
      // friendly assumed always visible
      if (unit->IsFriendly(target.GetSide()))
      {
        visibility = 1;
      }
    }

    if( visibility<MinVisibleFire )
    {
      #if DIAG_RESULT
        LogF( "  visibility low (%.3f)",visibility);
      #endif
      return FPCannot; // cannot fire
    }
  }

  FirePossibility possible = FPCannot;
  float maxSurplus=-1e10f;

  float timeToLive = AdjustedTimeToLive(unit, target, visibility);

  float timeToAim=0;
  float inRange = FireInRange(context, weapon, timeToAim, target);
  #if DIAG_RESULT
    LogF(" inRange %.3f",inRange);
  #endif
  const Magazine *magazine = context._weapons->_magazineSlots[weapon]._magazine;
  if (!magazine) return FPCannot;
  const MagazineType *type = magazine->_type;
  if (!type) return FPCannot;
  const AmmoType *ammo = type->_ammo;
  if (!ammo) return FPCannot;

  if (!suppresiveFire)
  {
    // target position not known - no fire
    if (!target.AccurateEnoughForHit(ammo,unit)) return FPCannot;

    // target not seen recently - no fire
    if (target.GetLastSeen() < Glob.time - 10) return FPCannot;
  }

  const WeaponModeType *mode = context._weapons->_magazineSlots[weapon]._weaponMode;
  Assert(mode);

  float timeToReload = magazine->_reload * magazine->_reloadDuration * mode->_reloadTime + magazine->_reloadMagazine;
  // time to aim is often zero or close to zero
  timeToAim=floatMax(timeToReload,timeToAim);
  //if( inRange<=0.5 ) continue;
  float distance=target.Distance(FutureVisualState().Position());
  FireResult tResult;
  #if DIAG_RESULT
    LogF(" times: live %.3f, aim %.3f",timeToShoot,timeToAim);
  #endif
  if
  (
    timeToReload<=timeToShoot && inRange>1e-6 &&
    WhatShootResult(
      tResult,target,context,weapon,inRange,timeToAim,
      timeToLive,visibility,distance,timeToShoot,true,false
    )
  )
  {
    //tResult.loan=0; // no loan
    float surplus=tResult.Surplus();
    if( maxSurplus<surplus )
    {
      possible = FPCan;
      maxSurplus = surplus;
      result = tResult;
    }
  }
  #if DIAG_RESULT
    LogF
    (
      "  dammage %.2f, gain %.0f, cost %.0f, loan %.0f, weapon %d",
      result.damage,result.gain,result.cost,result.loan,result.weapon
    );
    if( !possible ) LogF("  impossible");
  #endif
  return possible;
}

static float SumWeaponsTypeStrategicTreat(const EntityAIType *entityType, const WeaponsType &weapons, float distance2, float visibility, float cosAngle)
{
  // sum for all weapons
  float maxRange = weapons.GetMaxRange();
  float sum=0;
  if( distance2<maxRange*maxRange )
  {
    float distance = sqrt(distance2);
    for (int i=0; i<weapons._weapons.Size(); i++)
    {
      const WeaponType *weapon = weapons._weapons[i];
      for (int j=0; j<weapon->_muzzles.Size(); j++)
      {
        const MuzzleType *muzzle = weapon->_muzzles[j];
        const MagazineType *magazine = muzzle->_typicalMagazine;
        if (!magazine) continue;
        const AmmoType *ammo = magazine->_ammo;
        if (!ammo) continue;
        // fix: cannot use all weapon modes at once
        float muzzleHit = 0;
        for (int k=0; k<muzzle->_modes.Size(); k++)
        {
          const WeaponModeType *mode = muzzle->_modes[k];

          // do not try to get under best distance of weapons
          // consider: for very long range weapons this could be very clever
          float probab;
          float bestDist = BestDistance(*mode, probab);
          if (distance > bestDist)
          {
            probab = HitProbability(distance, *mode) * visibility;
          }
          float oneHit = probab * ammo->hit * visibility;
          float hitsPerMinute = 60.0f / floatMax(mode->_reloadTime,0.01f);
          saturateMin(hitsPerMinute, magazine->_maxAmmo);
          saturateMax(muzzleHit, hitsPerMinute * oneHit);
        }
        sum += muzzleHit;
      }
    }
    float backCoef = 1.0;
    float frontCoef = 1.0;
    if (distance>150)
    {
      // no drop down on minimal distance
      // spot danger
      sum *= 1-distance/maxRange;
      // small difference betweem front and back
      frontCoef = 2.5f * entityType->_sensitivity;
      // assume it will not look back
      backCoef = 2.5f * (entityType->_sensitivity*0.3+entityType->_sensitivityEar*0.7);
    }
    else
    {
      frontCoef = 2.5f * entityType->_sensitivity;
      // may easily look back
      backCoef = 2.5f * (entityType->_sensitivity*0.7+entityType->_sensitivityEar*0.3);
    }

    if (cosAngle>0) sum *= frontCoef;
    else sum *=backCoef;

    //LogF
    //(
    //  "  %s: GetStrategicThreat cosA %.2f, sum %.1f, dist %.1f, vis %.1f",
    //  (const char *)GetDisplayName(),cosAngle,sum,distance,visibility
    //);

    // check vehicle sensitivity
  }
  return sum;
}

/// Functor calculate the total strategy threat for all vehicle turrets
class SumEntityTypeStrategicThreat : public ITurretTypeFunc
{
protected:
  float _distance2;
  float _visibility;
  float _cosAngle;
  float &_result;

public:
  SumEntityTypeStrategicThreat(float distance2, float visibility, float cosAngle, float &result)
    : _distance2(distance2), _visibility(visibility), _cosAngle(cosAngle), _result(result)
  {
  }

  virtual bool operator () (const EntityAIType *entityType, const TurretType &turretType, TurretPath &path) const
  {
    _result += SumWeaponsTypeStrategicTreat(entityType, turretType._weapons, _distance2, _visibility, _cosAngle);
    return false; // continue
  };
};

Threat EntityAIType::GetStrategicThreat
(
  float distance2, float visibility, float cosAngle
) const
{
  float sum = SumWeaponsTypeStrategicTreat(this, _weapons, distance2, visibility, cosAngle);
  SumEntityTypeStrategicThreat func(distance2, visibility, cosAngle, sum);
  ForEachTurret(func);
  return _threat*sum;
}

static float SumWeaponsTypeTreat(const WeaponsType &weapons, float distMin, float distMax, float visibility)
{
  // sum for all weapons
  float minRange = 0; // TODO: remove, has no sense, will always be 0
  float maxRange = 0;
  for (int i=0; i<weapons._weapons.Size(); i++)
  {
    const WeaponType *weapon = weapons._weapons[i];
    for (int j=0; j<weapon->_muzzles.Size(); j++)
    {
      const MuzzleType *muzzle = weapon->_muzzles[j];
      if (muzzle->_magazines.Size()>0)
      {
        for (int k=0; k<muzzle->_modes.Size(); k++)
        {
          const WeaponModeType *mode = muzzle->_modes[k];
          if (mode) 
          {
            saturateMin(minRange, mode->minRange);
            saturateMax(maxRange, mode->maxRange);
          }
        }
      }
    }
  }
  float sum = 0;

  if( distMin<maxRange && distMax>minRange )
  {
    for (int i=0; i<weapons._weapons.Size(); i++)
    {
      const WeaponType *weapon = weapons._weapons[i];
      for (int j=0; j<weapon->_muzzles.Size(); j++)
      {
        const MuzzleType *muzzle = weapon->_muzzles[j];
        const MagazineType *magazine = muzzle->_typicalMagazine;
        if (!magazine) continue;
        const AmmoType *ammo = magazine->_ammo;
        if (!ammo) continue;
        // fix: cannot use all weapon modes at once
        float muzzleHit = 0;
        for (int k=0; k<muzzle->_modes.Size(); k++)
        {
          const WeaponModeType *mode = muzzle->_modes[k];
          float probab = HitIntervalProbability(distMin, distMax, *mode) * visibility;
          float oneHit = probab * ammo->hit * visibility;
          float hitsPerMinute = 60.0f / floatMax(mode->_reloadTime,0.01f);
          saturateMin(hitsPerMinute, magazine->_maxAmmo);
          saturateMax(muzzleHit, hitsPerMinute * oneHit);
        }
        sum += muzzleHit;
      }
    }
  }
  return sum;
}

/// Functor calculate the total threat for all vehicle turrets
class SumEntityTypeThreat : public ITurretTypeFunc
{
protected:
  float _distMin;
  float _distMax;
  float _visibility;
  float &_result;

public:
  SumEntityTypeThreat(float distMin, float distMax, float visibility, float &result)
    : _distMin(distMin), _distMax(distMax), _visibility(visibility), _result(result)
  {
  }

  virtual bool operator () (const EntityAIType *entityType, const TurretType &turretType, TurretPath &path) const
  {
    _result += SumWeaponsTypeTreat(turretType._weapons, _distMin, _distMax, _visibility);
    return false; // continue
  };
};

Threat EntityAIType::GetDamagePerMinute(float distance2, float visibility) const
{
  // Get the distance interval as the distance this type can move while moving its typical speed over some 10 seconds
  const float TimeToTransfer = 10.0f / 3.6f; //seconds & change units from km/hod to m/s
  float distInt = GetTypSpeed()*TimeToTransfer; 
  float distance = sqrt(distance2);
  float distMin = floatMax(distance-distInt,0);
  float distMax = distance+distInt;
  
  float sum = SumWeaponsTypeTreat(_weapons, distMin, distMax, visibility);
  SumEntityTypeThreat func(distMin, distMax, visibility, sum);
  ForEachTurret(func);
  return _threat * sum;
}

/**
TODO: we could precalculate this
*/
float EntityAIType::GetMaxWeaponRange() const
{
  return _weapons.GetMaxRange();
}

Time EntityAI::GetLastShotTime() const
{
  return Glob.time - 120;
} 

Threat EntityAI::GetDamagePerMinute(float distance2, float visibility) const
{
  return GetType()->GetDamagePerMinute(distance2,visibility);
}

/**
@param type type of the target engaging us
@param dist2 squared distance to the target
@param visible line of sight visibility
@param timeToLive our estimated time to live
*/
float EntityAI::GetDangerCost(const EntityAIType * type, float dist2, float visible, float timeToLive) const
{
  VehicleKind vehKind = GetType()->GetKind();
  Threat threat = type->GetDamagePerMinute(dist2, 1.0);
  float damagePerMinute = threat[vehKind];

  if (damagePerMinute > 0)
  {
    float lTimeToLive = GetArmor() * 60 / damagePerMinute;
    float danger = timeToLive / lTimeToLive;
    // note: lTimeToLive may be higher that timeToLive if tgt is relatively new
    const float maxNormalDanger = 10.0f;
    if (danger>maxNormalDanger)
    { // higher danger usually means the unit is closer (has a higher hit probability)
      // we want such targets to be prioritized a bit
      danger = maxNormalDanger + (1-maxNormalDanger/danger)*maxNormalDanger; // over maxNormalDanger grow only slowly
    }
    // consider not only damage - for enemies which are very near boost their danger, as they are very likely to engage us
    danger *= InterpolativC(dist2,Square(20),Square(60),2.0f,1.0f);
    return danger*GetType()->GetCost();
  }
  return 0;
}


/*!
\patch 5127 Date 2/7/2007 by Jirka
- Fixed: Threat calculation was inaccurate for weapons with multiple modes 
*/

/// Functor calculate the total threat for all vehicle weapons
class SumEntityThreat : public ITurretFunc
{
protected:
  float _distance2;
  float _visibility;
  float &_result;

public:
  SumEntityThreat(float distance2, float visibility, float &result)
    : _distance2(distance2), _visibility(visibility), _result(result)
  {
  }

  bool operator () (EntityAIFull *entity, TurretContext &context)
  {
    if (context._gunner)
    {
      // sum for all weapons
      float maxRange=0;
      for (int i=0; i<context._weapons->_weapons.Size(); i++)
      {
        const WeaponType *weapon = context._weapons->_weapons[i];
        for (int j=0; j<weapon->_muzzles.Size(); j++)
        {
          const MuzzleType *muzzle = weapon->_muzzles[j];
          for (int k=0; k<muzzle->_modes.Size(); k++)
          {
            const WeaponModeType *mode = muzzle->_modes[k];
            if (mode) saturateMax(maxRange, mode->maxRange);
          }
        }
      }
      if( _distance2<maxRange*maxRange )
      {
        float distance=sqrt(_distance2);
        for (int i=0; i<context._weapons->_magazineSlots.Size(); i++)
        {
          const MagazineSlot &slot = context._weapons->_magazineSlots[i];
          const MuzzleType *muzzle = slot._muzzle;
          if (!muzzle) continue;
          const Magazine *magazine = slot._magazine;
          if (!magazine) continue;
          const AmmoType *ammo = magazine->_type ? magazine->_type->_ammo : NULL;
          if (!ammo) continue;
          // fix: cannot use all weapon modes at once
          float muzzleHit = 0;
          int m = muzzle->_modes.Size();
          Assert(m >= 1);
          for (int j=0; j<m; j++)
          {
            const MagazineSlot &slot = context._weapons->_magazineSlots[i + j];
            DoAssert(slot._muzzle == muzzle);
            const WeaponModeType *mode = slot._weaponMode;
            
            if (!mode)
            { // hotfix: do not allow to continue with NULL mode (avoids crash inside HitProbability)
              static bool alreadyReported = false;
              if (!alreadyReported)
              {
                alreadyReported = true;
                // Make a report (weapon, muzzle, magazine, entity)
                RptF("Error: Slot with muzzle but no mode!");
                if (slot._weapon) 
                {
                  RStringB name = slot._weapon->GetName();
                  RptF("  weapon = \"%s\"", cc_cast(name));  
                }
                if (muzzle)
                {
                  RStringB name = muzzle->GetName();
                  RptF("  muzzle = \"%s\"", cc_cast(name));  
                }
                if (magazine && magazine->_type)
                {
                  RStringB name = magazine->_type->GetName();
                  RptF("  magazine = \"%s\"", cc_cast(name));
                }
                if (entity)
                {
                  RString name = entity->GetType()->GetName();
                  RptF("  entity = \"%s\"", cc_cast(name));
                }
              }
              continue; //hotfix
            }

            float probab = HitProbability(distance, *mode) * _visibility;
            float oneHit = probab * ammo->hit * _visibility;
            float hitsPerMinute = 60.0f / floatMax(mode->_reloadTime,0.01f);
            // note: we assume max. one magazine per minute? This does not work well
            // where there is only one shot per magazine (grenades, rockets...)
            saturateMin(hitsPerMinute, magazine->GetAmmo());
            saturateMax(muzzleHit, hitsPerMinute * oneHit);
          }
          _result += muzzleHit;
          // skip to the next muzzle
          i += m - 1;
        }
      }
    }
    return false; // continue
  }
};

Threat EntityAIFull::GetDamagePerMinute(float distance2, float visibility) const
{
  float sum = 0;
  SumEntityThreat func(distance2, visibility, sum);
  ForEachTurret(func);
  return GetType()->_threat * sum;
}

bool EntityAIFull::GetAIFireEnabled(Target *tgt) const
{
  // check if commander have fire enabled
  AIBrain *unit = CommanderUnit();
  if (!unit) return false;
  return unit->IsFireEnabled(tgt);
}

void EntityAIFull::ReportFireReady() const
{
  // report theat we would like to fire
  // wait until state is steady
  if (_lastWeaponNotReady>Glob.time-5) return;
  // report when changed or did not report for a while
  if (_lastWeaponReady<Glob.time-30 || _lastWeaponNotReady>_lastWeaponReady)
  {
    _lastWeaponReady = Glob.time;
    // send report
    AIBrain *unit = CommanderUnit();
    if (!unit) return;
    AIGroup *grp = unit->GetGroup();
    if (!grp) return;
    grp->ReportFire(unit->GetUnit(), true);
  }
  
}

void EntityAIFull::ReportFireNotReady() const
{
  // wait until state is steady
  if (_lastWeaponReady>Glob.time-5) return;
  // report when changed or did not report for a while
  if (_lastWeaponNotReady<Glob.time-30 || _lastWeaponReady>_lastWeaponNotReady)
  {
    _lastWeaponNotReady = Glob.time;
    // send report
    AIBrain *unit = CommanderUnit();
    if (!unit) return;
    AIGroup *grp = unit->GetGroup();
    if (!grp) return;
    grp->ReportFire(unit->GetUnit(), true);
  }
}


FireDecision::FireDecision()
{
  _weapon = -1; // what weapon should be used
  //LinkTarget _fireTarget; // what target vehicle should fire at
  _firePrepareOnly = true;
   _fireCommanded = false;
  _nextWeaponSwitch = Glob.time; // change weapon
  _nextTargetAquire = Glob.time; // acquire target when no target is acquired
  _nextTargetChange = Glob.time; // change target (when target is acquired)
  _initState = TargetAlive;
  _lastFirePossible = TIME_MIN;
}

void FireDecision::SetTargetWithInitState(AIBrain *sensor, Target *tgt)
{
  _fireTarget = tgt;
  if (!tgt) _initState = TargetAlive;
  else _initState = tgt->State(sensor);
  #if _PROFILE
    if (tgt && tgt->idExact && _initState<TargetFireMin && !_firePrepareOnly)
    {
      LogF("Suspicious target %s, empty",cc_cast(tgt->idExact->GetDebugName()));
    }
  #endif
#if LOG_TARGET_CHANGES
  LogF(
    "%s: at %.1f Set target to %s, state %d",
    sensor ? cc_cast(sensor->GetDebugName()) : "", Glob.time.toFloat(),
    tgt && tgt->idExact ? (const char *)tgt->idExact->GetDebugName() : "NULL",_initState
  );
#endif
}

void FireDecision::SetTarget(AIBrain *sensor, Target *tgt)
{
  if (tgt==_fireTarget) return;
  SetTargetWithInitState(sensor,tgt);
}

void FireDecision::SetTargetWithFire(AIBrain *sensor, Target *tgt, bool firePrepareOnly)
{
  if (tgt==_fireTarget && _firePrepareOnly==firePrepareOnly) return;
  SetTargetWithInitState(sensor,tgt);
  _firePrepareOnly = firePrepareOnly;
}

bool FireDecision::GetTargetFinished(AIBrain *sensor) const
{
  if (_firePrepareOnly) return false; // target never finished
  TargetState state = _fireTarget->State(sensor);
  return state<_initState;
}

int FireDecision::GetTargetCaptive() const
{
  if (_fireTarget && _fireTarget->idExact && _fireTarget->idExact->CommanderUnit())
    return _fireTarget->idExact->CommanderUnit()->GetCaptive();
  return 0;
}

#if 0 // _ENABLE_CHEATS && _PROFILE
# pragma optimize("",off)
#endif

void EntityAIFull::SelectFireWeapon(TurretContext &context)
{
  SelectFireWeapon(context, context._weapons->_fire);
}

/*!
\patch 1.08 Date 7/21/2001 by Ondra.
- Fixed: when using weapon only to look, prefer actual weapon.
This helps to avoid unnecessary weapon changes.
- Fixed: AI watched non-combat enemies.
This made weapon selection unstable,
binoculars were also selected with no apparent reason.
Only combat enemies are watched now.
\patch 1.24 Date 9/20/2001 by Ondra.
- Fixed: AI sometimes did not unlock target after destroying it.
\patch 1.27 Date 10/18/2001 by Ondra.
- Fixed: AI: If there are no other targets,
 watch first enemy that was only heard but not seen yet.
\patch 1.42 Date 1/4/2002 by Ondra
- Fixed: AI used binocular too often.
\patch 1.85 Date 9/20/2002 by Ondra
- Fixed: AI units often looked backward.
\patch 5158 Date 5/16/2007 by Ondra
- Fixed: AI no longer uses RPG/LAW for looking at unknown enemies.
\patch 5164 Date 6/25/2007 by Ondra
- Fixed: leader, AI tanks subordinate to a player sometimes did not open fire against some enemy tanks.
*/
void EntityAIFull::SelectFireWeapon(TurretContext &context, FireDecision &fire)
{
  PROFILE_SCOPE_EX(aiSFW,ai);
  AIBrain *unit=CommanderUnit();

  if (_userStopped) return; // no targeting
  
  Assert( unit );
  if (!unit) return;

  if (context._weapons->_forceFireWeapon >= 0)
  {
    fire.SetTarget(unit,NULL);
    if (context._weapons->_forceFireWeapon != context._weapons->_currentWeapon)
    {
      context._weapons->SelectWeapon(this, context._weapons->_forceFireWeapon);
    }
    return;
  }
  
  #if SUSPEND_OTHER_AI
    if (this!=GWorld->GetObserver()) return;
  #endif
  #if DIAG_TARGET
    #if NODIAG_OTHER_AI
      bool diag = this==GWorld->GetObserver();
    #else
      const bool diag = true;
    #endif
  #endif
#if _ENABLE_CHEATS
  if (this==GWorld->GetObserver())
  {
    // debug opportunity
    __asm nop;
  }
#endif

  // do not change weapons too often

  // _nextTargetAquire is used when we are in combat and do not have any target acquired
  // _nextWeaponSwitch is used in all other situations
  // _nextTargetAquire is generally sooner
  if (fire._fireTarget)
  {
    if (fire.GetTargetFinished(unit))
    {
      // do not fire on vanished or destroyed target
      fire.SetTarget(unit,NULL);
    }
    else if (fire._fireTarget->IsVanished())
    {
      fire.SetTarget(unit,NULL);
    }
    else if (fire.GetTargetCaptive())
    {
      fire.SetTarget(unit,NULL);
    }
  }
  if (unit->GetCombatModeLowLevel()<=CMAware || fire._fireTarget)
  {
    if( fire._nextWeaponSwitch>Glob.time )
    {
      return;
    }
  }
  else
  {
    // use _nextTargetAquire
    if( fire._nextTargetAquire>Glob.time )
    {
      return;
    }
  }

  //LogF("%s: SelectFireWeapon",(const char *)GetDebugName());
  float targetMinSubjCost = 0;

  Target *tgt=unit->GetTargetAssigned();
  if( tgt )
  {
    if (tgt->State(unit)>TargetDestroyed)
    {
      targetMinSubjCost = tgt->GetSubjectiveCost()*0.5f; // if we are assigned to something very important, do not switch for something small
      if(!tgt->IsKnownBy(unit) || tgt->GetLastSeen()<Glob.time-5)
      { // if we do not know target precision exactly enough,  target may be very old
        if(!tgt->idExact || !tgt->idExact->IsArtilleryTarget())
          tgt=NULL;
      }
    }
    else
    {
      // assigned target already destroyed
      tgt = NULL;
    }
  }

  if(tgt)
  {
    EntityAI *targetId = tgt->idExact; 
    if (targetId && targetId->CommanderUnit() && targetId->CommanderUnit()->GetCaptive() )
    {
      // assigned target has been captured
      tgt = NULL;
      unit->AssignTarget(NULL);
    }
  }

  float timeToLive=unit->GetTimeToLive();
  //saturateMax(timeToLive,5);

  FireResult fResult;
  bool fResultPossible = false;
  // check if we may change target
  bool enableOtherTarget = false;
  bool enableOtherTargetOnlyNew = false;

#if _ENABLE_CHEATS
  if (this==GWorld->GetObserver())
  {
    // debug opportunity
    __asm nop;
  }
#endif

  if (fire._fireTarget && !fire._firePrepareOnly && fire._nextTargetChange>=Glob.time)
  {
    // target is very recent
    // select most efficient weapon for given target only
    tgt = fire._fireTarget;
    fResultPossible = WhatFireResult(fResult, *tgt, context, timeToLive)==FPCan;

    #if DIAG_TARGET>=3
    if (diag && fResult.weapon!=fire._weapon)
    {
      LogF(
        "%s: SelectWeapon %.1f, weapon %d",
        (const char *)GetDebugName(),Glob.time-Time(0),fResult.weapon
      );
    }
    #endif
  }
  else
  {
    // select weapon and target
    if( tgt ) fResultPossible = WhatFireResult(fResult, *tgt, context, timeToLive)==FPCan;
    enableOtherTarget = true;
    enableOtherTargetOnlyNew = false;

    //LogF("+++ %s eval target at %.2f",cc_cast(GetDebugName()),Glob.time.toFloat());

    #if DIAG_TARGET>2
    if (diag) LogF(
      "%s: SelectTarget %.1f, weapon %d",
      (const char *)GetDebugName(),Glob.time-Time(0),fResult.weapon
    );
    LogF("  time expired (%.2f)", Glob.time - fire._nextTargetChange);
    LogF("  target %s", (fire._fireTarget && fire._fireTarget->idExact) ? cc_cast(fire._fireTarget->idExact->GetDebugName()) : "<null>");
    #endif
  }

  if (unit->GetAIDisabled()&AIUnit::DAAutoTarget)
  {
    enableOtherTarget = false;
  }

  // FIX
  // when engaging vehicle, crew may be killed
  // vehicle is that considered to be civilian
  // check below will prevent firing at it.
  // FIX END

  // note: target may be friendly. In that case do not fire unless explicitly ordered
  if (tgt && !unit->IsEnemy(tgt->GetSide()))
  {
    // check if fire has been ordered
    if (unit->GetEnableFireTarget()!=tgt)
    {
      // target not suitable for firing
      fResultPossible = false;
    }
  }
  if (fResultPossible && !enableOtherTarget && !unit->IsHoldingFire())
  {
    #if DIAG_TARGET>=2
    if (diag && fire._firePrepareOnly)
    {
      LogF("  fire possible, active fire enabled");
    }
    #endif
    fire._firePrepareOnly = false;
  }

#if _ENABLE_CHEATS
  if (this==GWorld->GetObserver())
  {
    // debug opportunity
    __asm nop;
  }
#endif

  if (enableOtherTarget)
  {
    if (fResultPossible)
    {
      // we can fire on the assigned target, we need real and significant reason to fire on anything else
      if (unit->GetGroup() && unit->GetGroup()->IsAnyPlayerGroup())
      {
        // when player gives an order, we need very strong reason to disobey
        targetMinSubjCost *= 3.0f;
      }
      else
      {
        // other target assigned, we need a strong reason to disobey, and it must be because of a new target
        targetMinSubjCost *= 1.8f;
        enableOtherTargetOnlyNew = true;
      }
    }


    // preferred target not visible - fire on anything else
    // select only few nearest enemy targets
    // select enemy target with best fire result
    
    // if we are able to fire or laser designated targets, we should strongly prefer them
    //(( LASER_HARD
    bool preferLased = GetType()->GetLaserScanner();
    bool preferNv = GetType()->GetNVScanner();
    //)) LASER_HARD
    const TargetList *list = unit->AccessTargetList();
    if (list)
    {
      int maxEnemies = 4; // limit not considering FPCannot
      int maxEnemiesRelaxed = 16; // limit considering even FPCannot
      // targets are sorted by subj. cost
      for( int i=0; i<list->EnemyCount(); i++ )
      {
        TargetNormal *tgtI = list->GetEnemy(i);
        if (tgtI==tgt) continue;
        // once we are below threshold, there is no need to continue
        if (tgtI->subjectiveCost<=targetMinSubjCost) break;
        if (enableOtherTargetOnlyNew)
        {
          // if the target is already cover by assigned units, and will be finished soon, do not care about it
          const float minTTL = 10.0f/60.0f; // 10 seconds
          if (tgtI->type->GetArmor()<tgtI->damagePerMinute*minTTL) continue;
        }
        if (!tgtI->IsKnownBy(unit) || tgtI->destroyed || tgtI->vanished) continue;
        if( !unit->IsEnemy(tgtI->side) ) continue;
        if( tgtI->idExact && tgtI->idExact->IsArtilleryTarget()) continue;
        if (tgtI->State(unit)<TargetEnemyCombat) continue;
        //(( LASER_HARD
        if ((preferLased && tgtI->type->GetLaserTarget()) || (preferNv && tgtI->type->GetNvTarget()))
        {
          // even if there is currently no result, we want to engage the target
          // select a weapon suitable for engaging such target
          // first check current weapon,
          int selected = context._weapons->ValidatedCurrentWeapon();
          if (selected>=0 && CanLock(tgtI->idExact,context,selected))
          {
            fResult.weapon = selected;
          }
          else
          {
            // if current weapon unable to lock, select any other
            for (int i=0; i<context._weapons->_magazineSlots.Size(); i++)
            {
              if (CanLock(tgtI->idExact,context,i))
              {
                fResult.weapon = i;
                break;
              }
            }
          }
          // when we have the target, there is no need to search for another one
          if (fResult.weapon>=0)
          {
            tgt = tgtI;
            fResultPossible = true;
            break;
          }
        }
        //))LASER_HARD or NV_HARD
        
        if (tgtI->lastSeen<Glob.time-5) continue;
        FireResult tResult;
        FirePossibility fp = WhatFireResult(tResult, *tgtI, context, timeToLive);
        if (fp==FPCan)
        {
#if DIAG_TARGET
          if (diag) LogF(
            "%s to %s: surplus %.0f",
            (const char *)GetDebugName(),
            (const char *)tgtI->idExact->GetDebugName(),
            tResult.Surplus()
          );
#endif
          // allow firing in a desperate situation (surplus including loan is negative)
          // however avoid shooting which is not economical (clean surplus negative, meaning the shot costs more than a target)
          // such shooting needs to be decided (ordered) by the group leader
          if (!fResultPossible && tResult.CleanSurplus()>0 || tResult.Surplus()>fResult.Surplus())
          {
            fResult = tResult;
            tgt = tgtI;
            fResultPossible = true;
          }
        }
#if DIAG_TARGET>=2
        else
        {
          if (diag) LogF(
            "%s to %s: WhatFireResult => false",
            (const char *)GetDebugName(),
            (const char *)tgtI->idExact->GetDebugName()
          );
        }
#endif
        // if result is FPCannot, the test was very quick and we do not count it against the limit
        // this is done to avoid a few high cost, but impossible to fire at targets (like aircraft) exhausting the limit
        if( --maxEnemiesRelaxed<=0 || fp!=FPCannot && --maxEnemies<=0 ) break;
      }
      if (unit->IsHoldingFire())
      {
        fire._firePrepareOnly=true;
      }
    }
    //LogF("+++ %s eval target at %.2f, selected %s",cc_cast(GetDebugName()),Glob.time.toFloat(),tgt ? cc_cast(tgt->GetDebugName()) : "NULL");
  }

  // when no result so far and suppressive fire is enabled, check for a suitable suppressive fire target
  if (CheckSuppressiveFire()>=SuppressYes && !fResultPossible)
  {
    if (tgt) // first try suppressing assigned target
    {
      // even when fire will not have any real effect, when suppressing we still want to select a weapon and fire
      fResultPossible = WhatFireResult(fResult, *tgt, context, timeToLive, true)==FPCan;
    }
    if (!fResultPossible)
    {
      int maxEnemies = 4; // limit not considering FPCannot
      int maxEnemiesRelaxed = 16; // limit considering even FPCannot
      // targets are sorted by subj. cost
      const TargetList *list = unit->AccessTargetList();
      for( int i=0; i<list->EnemyCount(); i++ )
      {
        TargetNormal *tgtI = list->GetEnemy(i);
        // once we are below threshold, there is no need to continue
        if (tgtI->subjectiveCost<=targetMinSubjCost) break;
        if (!tgtI->IsKnownBy(unit) || tgtI->destroyed || tgtI->vanished) continue;
        if( !unit->IsEnemy(tgtI->side) ) continue;
        if (tgtI->State(unit)<TargetEnemyCombat) continue;
        FireResult tResult;
        FirePossibility fp = WhatFireResult(tResult, *tgtI, context, timeToLive, true);
        if (fp==FPCan)
        {
#if DIAG_TARGET
          if (diag) LogF(
            "%s to %s: surplus %g",
            (const char *)GetDebugName(),
            (const char *)tgtI->idExact->GetDebugName(),
            tResult.Surplus()
            );
#endif
          if (!fResultPossible && tResult.CleanSurplus()>0 || tResult.Surplus()>fResult.Surplus())
          {
            fResult = tResult;
            tgt = tgtI;
            fResultPossible = true;
          }
        }
#if DIAG_TARGET>=2
        else
        {
          if (diag) LogF(
            "%s to %s: WhatFireResult supressive => false",
            (const char *)GetDebugName(),
            (const char *)tgtI->idExact->GetDebugName()
          );
        }
#endif
        // if result is FPCannot, the test was very quick and we do not count it against the limit
        // this is done to avoid a few high cost, but impossible to fire at targets (like aircraft) exhausting the limit
        if( --maxEnemiesRelaxed<=0 || fp!=FPCannot && --maxEnemies<=0 ) break;
      }
    
    }
  }

  // prefer fire sector before any unknown targets
  if (!tgt && enableOtherTarget)
  {
    tgt = unit->GetFireSectorTarget();
  }
  if (!tgt && enableOtherTarget)
  {
    // try to find some unknown target
    // so that we can use scope to look at it
    // this applies only when we have no target assigned
    // consider using UnknownEnemy instead of Enemy
    const TargetList *list = unit->AccessTargetList();
    if (!unit->GetTargetAssigned() && list)
    {
      for( int i=0; i<list->EnemyUnknownCount(); i++ )
      {
        TargetNormal *tgtI = list->GetEnemyUnknown(i);
        // once we are in negative cost area, we can no longer meet any unknown
        if (tgtI->subjectiveCost<0) break;
        if( !tgtI->IsKnownBy(unit) ) continue;
        if (tgtI->vanished) continue;
        if (tgtI->destroyed) continue;
        if (tgtI->side!=TSideUnknown) continue;
        if (tgtI->lastSeen<Glob.time-60) continue;
        // do not watch unknown static targets - no need to watch them
        if (tgtI->idExact && tgtI->idExact->Static()) continue;
        tgt=tgtI;
        break;
      }
    }
    if (!tgt)
    {
      tgt = unit->GetTargetAssigned();
      // check if alive
      if (tgt && tgt->State(unit) < TargetAlive) tgt = NULL;
    }
    if (!tgt && list)
    {
      // if there is no unknown nor assigned target
      // watch first enemy that is was only heard
      // {{ FIX 1.27
      for( int i=0; i<list->EnemyCount(); i++ )
      {
        TargetNormal *tgtI = list->GetEnemy(i);
        // once we are in non-positive cost area, we can no longer meet any enemy
        if (tgtI->subjectiveCost<=0) break;
        if (tgtI->vanished) continue;
        if (tgtI->destroyed) continue;
        if (!tgtI->IsKnownBy(unit)) continue;
        if (!unit->IsEnemy(tgtI->side)) continue;
        if (tgtI->State(unit)<TargetFireMin) continue;
        // check if the position is accurate enough - if not, use optics
        if (tgtI->FadingPosAccuracy()<1.0) continue;
        if (tgtI->lastSeen<Glob.time-60) continue;
        tgt=tgtI;
        break;
      }
      // }} FIX
    }

    if( tgt )
    {
      // select weapon with best zoom
      float minFov = 1e10;
      // FIX: for looking prefer actual weapon
      int weapon = fire._gunner == context._gunner ? fire._weapon : -1;
      if (weapon<0) weapon = context._weapons->_currentWeapon;
      if (weapon >= 0 && weapon < context._weapons->_magazineSlots.Size())
      {
        const MagazineSlot &slot = context._weapons->_magazineSlots[weapon];
        //const MuzzleType *muzzle = slot._muzzle;
        const OpticsInfo &opticsInfo = slot._muzzle->_opticsInfo[slot._opticsMode];
        // 
        minFov = opticsInfo._opticsZoomMax;
      }
      // FIX END

      // check if enemy can be seen by plain eye
      // if not, do not use binoculars or other optics in not current weapon
      if (tgt->FadingPosAccuracy()<1.0f)
      {

        // note: weapons without magazines need to be checked too?
        // TODO: some weapons should not be used for looking (AT missiles)

        for (int i=0; i<context._weapons->_magazineSlots.Size(); i++)
        {
          const MagazineSlot &slot = context._weapons->_magazineSlots[i];
          //const MuzzleType *muzzle = slot._muzzle;
          const OpticsInfo &opticsInfo = slot._muzzle->_opticsInfo[slot._opticsMode];
          // do not use rocket launcher or pistols just for looking, it looks funny
          // and switching to it takes too long to be practical
          int type = slot._weapon->_weaponType;
          if (type&(MaskSlotHandGun|MaskSlotSecondary))
          {
            continue;
          }
          
          // if the 
          float fov = opticsInfo._opticsZoomMax;
          if (minFov>fov)
          {
            minFov = fov;
            weapon = i;
          }
        }
        if
        (
          _fireState==FireDone && Glob.time>_fireStateDelay ||
          fire._gunner == NULL || fire._weapon < 0 || fire._fireTarget == NULL
        )
        {
          _fireState=FireInit;
          _fireStateDelay=Glob.time+0.2+GRandGen.RandomValue()*0.5;
        }
      }
      #if DIAG_TARGET>=2
        if (diag && fire._weapon!=weapon)
        {
          LogF("Change look weapon %d to %d",fire._weapon,weapon);
        }
      #endif

      fire._gunner = context._gunner;
      fire._weapon = weapon;
      DoAssert(weapon < context._weapons->_magazineSlots.Size());
    }
    else
    {
      // select some weapon
      // preferred: machine gun or gun
      //LogF("  No Target");
      fire._gunner = NULL;
      fire._weapon = -1;
    }

    if (fire._fireTarget != tgt)
    {
      // some minimal time to watch given 
      fire._nextTargetChange = Glob.time + floatMin(10,GetType()->GetMinFireTime());
      fire.SetTarget(unit,tgt);
#if _ENABLE_CHEATS
      if (this==GWorld->GetObserver())
      {
        // debug opportunity
        __asm nop;
      }
#endif
      #if DIAG_TARGET
        if (diag) LogF(
          "%s : prepare target %s until %.1f, state %d",
          (const char *)GetDebugName(),
          tgt ? (const char *)tgt->idExact->GetDebugName() : "<null>",
          fire._nextTargetChange-Time(0),
          tgt ? tgt->State(unit) : -1
        );
      #endif
    }
    fire._firePrepareOnly=true;
    // we cannot force state to TargetEnemyCombat here, as when changing _firePrepareOnly to false nobody will reset it,
    // and it would actually prevent changing _firePrepareOnly
    //fire._initState = TargetEnemyCombat; // if switched to fire, stop immediately
    // no need to reconsider targets to often: no enemy
    //LogF("%s: weapon switch %.2f",(const char *)GetDebugName(),Glob.time-Time(0));
    fire._nextWeaponSwitch = Glob.time+0.5;
    fire._nextTargetAquire = Glob.time+0.1;

#if 0
if (this == GWorld->GetObserver())
{
  if (tgt)
  {
    RString msg = Format("# Looking at %s, weapon %d", tgt->idExact ? (const char *)tgt->idExact->GetDebugName() : "NULL", fire._fireMode);
    DIAG_MESSAGE(100, msg);
    LogF(msg);
  }
}
#endif
    return;
  }

  if (!tgt)
  {
    fire._firePrepareOnly=true;
    fire.SetTarget(unit,NULL);
    fire._nextWeaponSwitch = Glob.time+0.5;
    fire._nextTargetAquire = Glob.time+0.1;
    return;
  }

  #if 0
  if( this==GWorld->GetObserver() && tgt && tgt->idExact)
  {
    GlobalShowMessage
    (
      100,"Sec target %s",
      (const char *)tgt->idExact->GetDebugName()
    );
  }
  #endif
  
  // when fire opportunity is detected, order it
  if (fResultPossible && fResult.weapon>=0)
  {
    if(
      _fireState==FireDone && Glob.time>_fireStateDelay ||
      fire._gunner == NULL || fire._weapon < 0 || fire._fireTarget == NULL
    )
    {
      _fireState=FireInit;
      _fireStateDelay=Glob.time+0.2+GRandGen.RandomValue()*0.5;
    }
    // select best weapons against this target
    int weapon=fResult.weapon;
    if (fire._gunner != context._gunner || fire._weapon != weapon)
    {
      fire._nextWeaponSwitch=Glob.time+1.5;
      fire._nextTargetAquire = Glob.time+1.5;
      //LogF("%s: weapon switch sw %.2f",(const char *)GetDebugName(),Glob.time-Time(0));
    }

    //SelectWeapon(weapon);
    fire._gunner = context._gunner;
    fire._weapon = weapon;
    DoAssert(weapon < context._weapons->_magazineSlots.Size());

    // it must be at least 2 seconds since we generated the last DCCanFire event
    if (fire._lastFirePossible<Glob.time-2.0f && fire._fireTarget!=tgt && unit->IsFireEnabled(tgt) && unit->GetCombatModeMajor()!=CMCareless)
    {
      //LogF("+++ %s can fire at %s, at %.2f",cc_cast(unit->GetDebugName()),cc_cast(tgt->GetDebugName()),Glob.time.toFloat());
      unit->SetDanger(DCCanFire,tgt->LandAimingPosition(unit),tgt);
      fire._lastFirePossible = Glob.time;
    }

  }
  else
  {
    // check if there is some ammo in weapon
    if (fire._gunner == context._gunner && fire._weapon >= 0 && fire._weapon < context._weapons->_magazineSlots.Size())
    {
      const Magazine *magazine = context._weapons->_magazineSlots[fire._weapon]._magazine;
      if (!magazine || magazine->GetAmmo() == 0)
      {
        fire._gunner = NULL;
        fire._weapon = -1;
      }
    }
  }
  if (fire._fireTarget!=tgt)
  {
    fire._nextWeaponSwitch=Glob.time+1.5;
    fire._nextTargetAquire = Glob.time+1.5;
    fire._nextTargetChange = Glob.time + GetType()->GetMinFireTime();
    #if DIAG_TARGET
    if (diag) LogF(
      "%s : select target %s until %.1f, state %d, weapon %d",
      (const char *)GetDebugName(),
      tgt->idExact ? (const char *)tgt->idExact->GetDebugName() : "<null>",
      fire._nextTargetChange-Time(0),
      tgt ? tgt->State(unit) : -1,
      fire._weapon
    );
    #endif
  }
  //LogF("  Set Fire Target %s",(const char *)tgt->idExact->GetDebugName());
  bool firePrepareOnly = (
    !fResultPossible || fResult.weapon<0
    // cannot use fire.GetTargetFinished here, as it contains a test for _firePrepareOnly
    || tgt->State(unit)<fire._initState
    || tgt->State(unit)<TargetFireMin && unit->GetEnableFireTarget()!=tgt
  );
  if(tgt && tgt->idExact && tgt->idExact->IsArtilleryTarget())
    firePrepareOnly=true;
  fire.SetTargetWithFire(unit,tgt,firePrepareOnly);

  //do not fire at artillery target by own will


  #if DIAG_TARGET
    if (diag && tgt && fire._firePrepareOnly)
    {
      LogF("  prepare only");
    }
  #endif
}

#if 0 // _ENABLE_CHEATS && _PROFILE
# pragma optimize("",on)
#endif
