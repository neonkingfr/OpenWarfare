#include "../wpch.hpp"

#if _VBS3 && _VBS_TRACKING

#ifndef _VBS_TRACKING_MODULE
#define _VBS_TRACKING_MODULE

// Includes need to connect to polhemus system
#include <Es/Common/win.h>
#include <Es/Framework/appFrame.hpp>
#include <Es/Containers/array.hpp>
#include <Es/Strings/rString.hpp>

#include "../soldierOld.hpp"
#include "../objLink.hpp"

#include <winsock2.h>

// this should really go into its own header file
// incase the data format chanages
struct SensorData
{
  int   appId;
  int   sensorId;

  float pos[3];   // position
  float orien[3]; // orientation

};
TypeIsSimple(SensorData);

struct TObjectLink
{
  OLink(Object) obj;
  int appId;
  int sensorId;

  ClassIsGeneric(TObjectLink);
};


// Tracking class used to hold information about sensor data
class Tracking
{
  // container to hold the sensor data
  // match on sensorID, overwrite for the time being
  // if new data is allready inside the buffer
  AutoArray<SensorData> _buffer;

  // objects that have trackers enabled for them
  AutoArray<TObjectLink> _linkedObjects;

  // Socket declarations
  int    _inPort;
  SOCKET _inSocket;
  struct fd_set fdread,fdwrite,fdexcept;

  // stats on arriving frames
  DWORD  _lastFrame;
  DWORD  _howLong;
  int    _frameCount;
  int    _lateArrival;
  int    _invalidData;

  // Checking if socket was init successfull
  bool _initOk;

  // Returns true if everthing went okay
  bool CreateInSocket();
  bool Init();

  void RemoveObjLink(Object *obj);
public:
  Tracking():_inPort(2103),_initOk(false)
  {
    _lastFrame  = ::GlobalTickCount();
    _howLong = 0;
    _frameCount = 0; // average timeing over 32 frames
    _lateArrival = 0; // how many frames did we collect in one rendering pass!
    _invalidData = 0;
    _initOk = false;
  }

  void DisableTracking(Object *obj);
  void AddObject(Object *obj,int sensorId,int appId);


  ~Tracking();
   
  // Update called to recieve the necceary frames
  void Update();
  

};

// extern declaration for other classes
extern Tracking GTracking;

#endif

#endif