#if _EXT_CTRL

#ifndef __HLA_BASE_H__
#define __HLA_BASE_H__

#include "../wpch.hpp"

#if _HLA
  #include "HLA/LVCGame_API.h"
#else
  #include "HLA/LVCGame_API_Datatypes.h"
#endif

#include "../network/nettransport.hpp" //required here, otherwise we'll receive: winsock2.h(400) : error C2011: 'sockaddr_in' : 'struct' type redefinition
#include "../network/network.hpp"
#include "../Network/networkImpl.hpp"

// include the necceary extra data types for AAR
#include "AARHLAExtraTypes.hpp"


enum ExtCtrl {EC_NONE, EC_HLA, EC_AAR};

class AAR;

enum FaultCode
{
  CNRSUCCESS      =  0, // Successfull API call (Routine performed liked expected)
  CNRCOMEXCEPTION =  1, // Communication exception has occured (Very Bad)
  API_NOT_PRESENT =  2, // No precence of LVCGameAPI
  INTERNAL_ERROR  = 1000,
  BAD_PREFS_FILE  = 1001,
  CANT_SAVE_PREFS = 1002,
  CANT_CREATE_AUDIO_SESSION_FOLDER = 1003,
  CORRUPT_AUDIO_SESSION            = 1004,
  NETWORK_ERROR                    = 1005,
  COULD_NOT_SAVE_AUDIO             = 1006,
  COULD_NOT_SEEK                   = 1007,
  INCOMPATIBLE_AUDIO_SESSION       = 1008,
  COULD_NOT_SAVE_AUDIO_IOEXCEPTION = 1009,
  AUDIO_RENDER_ERROR               = 1010,
  RECORDING_ERROR                  = 1011,
  RECORDING_ERROR_E                = 1012,
  XMLRPC_SERVER_ERROR              = 1013,
  SESSION_ERROR                    = 1014,
  COULD_NOT_STOP_PLAYING           = 1015,
  COULD_NOT_START_PLAYING          = 1016,
  RECORDING_FILE_ERROR             = 1017,
  COULD_NOT_DELETE_SESSION         = 1018
};

struct CNRLogFake
{
  long  time;     // resolution milliseconds
  int   id;
  long  freq;
  long  duration; // resolution milliseconds
  char *source;
};
TypeIsSimpleZeroed(CNRLogFake);

class HlaInterface
{
protected:
  friend class AAR;
  enum EUpDegree {UD_None, UD_Up, UD};
  bool _AARSnapShotActive;

#if _HLA
  LVCGame_API* _hlaAPI; //only available for real HLA
  bool _HLALibLoaded; //true when the HLALib is Loaded

  bool _HLAInit;
  bool _HLADummyInit;  // dummy connected via AAR When AAR is replaying
  inline bool IsHLAActive(){return (_HLAInit && _hlaAPI->isInitialised() && !_hlaAPI->isStopped() && !_AARSnapShotActive);}

  bool _isCNRInitSession;
  bool _isCNRRecording;

  AutoArray<CNRLogFake> _demoEvents;  
#else
  inline bool IsHLAActive(){return false;}
#endif

  NetworkClient* _client;
  int _debugHLAUpdate;

  Entity *CreateVehicle(ExtCtrl Ctrl, Vector3 &pos, RString type, TargetSide side
                        , RString Name);

  void Eject(Person *man, Transport *trans);
//  void DeleteVehicle(Entity *veh);

  /// Get the upDegree as UPUp,UPMiddle,UPDown,NUnitPositions
  UnitPosition GetUpDegree(const StanceType stance);
  /// Get the required stance based on the current man
  StanceType GetStance(const Man* man);
  /// Get weapon posture based on the current man
  WeaponPostureType GetWeaponPosture(const Man* man);
  /// Get the required action based on the stance and speed
  ManAction GetAction(const Man* man, StanceType stance, WeaponPostureType posture);
  /// returns true if the Stance has to be changed and modifies selAction
  bool ChangeStance(const StanceType newStance,ManAction& selAction
                    , const Man* man);
  bool ChangeStance(const StanceType& newStance,const WeaponPostureType& posture
                    , ManAction& selAction, const Man* man);

  void RegisterHLACallBacks();
  void RegisterAARCallBacks();

  /// fill in Crew Info details based on the person
  void FillCrewInfo(const Person* person, EntityId &parent, int &crewIndex, EPosInfo &ePos);
  void UpdateEntity(Entity* vehAI);

  void CenterCreated(const AICenter& id);
  void GroupCreated(const AIGroup& id);
  void UnitCreated(const AIUnit& id);

  void UpdateUnit(AIUnit* unit);
  void UpdateGroup(AIGroup* group);


  bool SendToHLA(ExtCtrl ctrl);
public:

  HlaInterface();

  bool ExternalActive();

  //loads the LVC dll
  bool LoadLVCLib();

  NetworkObject* GetNetObjFromHLAId(const EntityId& Id);
  EntityAI* GetEntityAI(const EntityId& Id);
  Entity* GetEntity(const EntityId& Id);

  Entity* GetEntity(NetworkObject* nwObj);
  EntityAI* GetEntityAI(NetworkObject* nwObj);
  const EntityAI* GetEntityAI(const NetworkObject* nwObj);

  //! used by AAR when start recording to get all current centers, groups, units, obj
  void SendSnapshot();
  //! send new map origin to LVC
  void SetOrigin(double easting, double northing, long zone, char hemisphere);
  //////////////////////////////////////////////////////////////////////////
  // called from NetworkClient
  //////////////////////////////////////////////////////////////////////////
  void DisconnectDummyInterface();
  bool ConnectDummyInterface();
  bool DummyInterfaceOn();

  bool Connect(NetworkClient* client);
  void Disconnect();
  void OnSimulate();

  void UpdateVehicle(const NetworkId& id, const Vehicle *veh);
  void CreateVehicle(const NetworkId& id, const Vehicle *veh
                    , const RString& name, int vehId);
  void ChangeSimulationState(const NetworkServerState state);
  void CreateObject(const NetworkObject* nwObj);
  void UpdateObject(NetworkObject* nwObj);
  void VehicleDamaged(EntityAI *damaged, EntityAI *killer, float damage
    , RString ammo);
  void DeleteObject(NetworkId &id);
  void FireWeapon(EntityAIFull *vehicle, Person *gunner, int weapon
    , const Magazine *magazine
    , EntityAI *target, const RemoteFireWeaponInfo &remoteInfo);
  //special version, only used for Lasershot
  void FireWeapon(Person *gunner, RString ammoName, Vector3 position, Vector3 velocity, int timeToLive);

  void ExplosionDamageEffects(EntityAI *owner, Object *directHit, Vector3Par pos
    , Vector3Par dir, const AmmoHitInfo &hitInfo
    , bool enemyDamage, float energyFactor
    , float explosionFactor);
  void VehicleDestroyed(EntityAI *killed, EntityAI *killer);
  void AttachToMsg(Object *obj, Object *attachTo = NULL, int memIndex = 0, Vector3 pos = Vector3(0,0,0), int flags = 0);
  void CreateMarkerMsg(ArcadeMarkerInfo *data);
  void DeleteMarkerMsg(const RString &name);
  void OnTurretUpdate(Entity *entity);
  void WeatherUpdate(ApplyWeatherMessage *msg);
  void RecordAnimationPhase(Entity *entity,RString name,float phase);

  //////////////////////////////////////////////////////////////////////////
  // Calls to the CNR log recording system, can use script command for this
  //////////////////////////////////////////////////////////////////////////
  void TestCNRConnection();

  // Connects to the Remote logger
  // Return types
  //  0 - Success
  //  1 - Failure
  FaultCode CNRInitRemoteLogger();

  // Checks to see if we have inited a remote logger
  // Return types
  // 1 - true
  // 0 - false
  bool IsCNRSessionStarted();

  // Delete instance of remote logger
  // requires a instance to be alive before calling function
  void CNRReleaseRemoteLogger();

  // Stop recording, if currently recording
  // Return types
  // 0 - Successs
  // 1 - Failure
  FaultCode CNRStopRecording();

  // Starts recording.
  // Return types
  // 0 - Success 
  // 1 - Failure
  FaultCode CNRStartRecording();
  
  // Checks to see if currently recording.
  // Right now, not usefull at all does not return 
  // the recording state.
  // Return Types
  // 0 - Success 
  // 1 - Failure 
  FaultCode CNRIsRecording(bool &isRecording);

  // Open session name on server, if no session exsists
  // then create one of the same name.
  // Return Types
  // 0 - Success
  // 1 - Failure
  FaultCode CNROpenSession(const char *sessionName);

  // Gets the information from the event. 
  // 1 - Successfull Call
  // 0 - Failure
  bool CNRGetEvent(int index,CNRLogEvent &event);

  // Returns the event size
  // 0 means no recordings
  int CNRGetEventSize();

  // Stop playing the events on the server
  // Return Types
  // 0 - Success
  // 1 - Failure
  FaultCode CNRStopPlaying();

  // Plays the current record at this index.
  // Return Types
  // 0 - Success
  // 1 - Failure
  FaultCode CNRPlayOneRecord(int index);

  // Plays all the current recordings in the session
  // Return Types
  // 0 - Success
  // 1 - Failure
  FaultCode CNRPlayAllRecords();

  // Returns the start time of the most current recording session
  // Return type
  // >0 - Recording time
  // -1 - no recording time
  long CNRGetStartTime();

  // Deletes session from storage folder
  // Return Types
  // 0 - Success
  // 1 - Failure
  FaultCode CNRDeleteSession(const char *sessionName);

  //////////////////////////////////////////////////////////////////////////
  // Called over external callbacks (AAR only) 
  //////////////////////////////////////////////////////////////////////////
  EntityId  HLACenterCreated(ExtCtrl ctrl, const CenterCreateData data);
  EntityId  HLAGroupCreated(ExtCtrl ctrl, const GroupCreateData data);
  EntityId  HLAUnitCreated(ExtCtrl ctrl, const UnitCreateData data);
  void      HLAGroupDeleted(ExtCtrl ctrl, const GroupDeleteData data);
  void      HLAUnitUpdated(ExtCtrl ctrl, UnitUpdateData data);
  void      HLAGroupUpdated(ExtCtrl ctrl, UpdateGroupData data);
  void      HLAAttachToUpdated(ExtCtrl ctrl, AttachToData data);
  void      HLACreateMarker(ExtCtrl ctrl, MarkerCreateData data);
  void      HLAOnTurrentUpdate(ExtCtrl ctrl, TurretUpdate data);

  //////////////////////////////////////////////////////////////////////////
  // Called over external callbacks
  //////////////////////////////////////////////////////////////////////////
  EntityId HLAEntityCreated(ExtCtrl ctrl, EntityCreateData data);
  int  HLAEntityUpdated(ExtCtrl ctrl, EntityUpdateData data);
  void HLAEntityDeleted(ExtCtrl ctrl, EntityDeleteData data);
  void HLAWeaponFired(ExtCtrl ctrl, FireWeaponData data);
  void HLADetonateMunition(ExtCtrl ctrl, DetonationData data);
  void HLADoDamage(ExtCtrl ctrl, DoDamageData data);
  void HLAVehicleDestroyed(ExtCtrl ctrl, EntityDestroyed data);
  //  void HLAEntityDamaged(EntityDamagedData data);

  //////////////////////////////////////////////////////////////////////////
  // direct scripting interface
  //////////////////////////////////////////////////////////////////////////

  void SendLaserDesignatorCreate
  (
    Entity* Id,                        // Designator ID
    Entity* DesignatingId,             // ID of the entity controlling the desig
    Entity* DesignatedId,              // ID of the designated entity (or 0,0)
    int DesignatorNameCode,             // 
    int DesignatorFunctionCode,         //
    float DesignatorPower,              // power in watts
    float DesignatorWavelength,         // wavelength in microns
    Vector3& RelativePosition,  // desig pos relative to entity centre
    Vector3& WorldPosition,     // desig pos in the world
    Vector3& LinearAcceleration    // linear accel in m/s^2
  );
};

extern HlaInterface _hla;

#endif //_EXT_CTRL
#endif //__HLA_BASE_H__
