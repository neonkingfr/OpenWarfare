#include "../wpch.hpp"
#if _VBS3 && _VBS_TRACKING

#include "Tracking.hpp"
#include <El/Math/math3d.hpp>

Tracking GTracking;

bool Tracking::CreateInSocket()
{
  struct sockaddr_in outGoing;
  outGoing.sin_family      = AF_INET;
  outGoing.sin_addr.s_addr = ADDR_ANY;
  outGoing.sin_port        = htons(_inPort);

  _inSocket = socket(AF_INET,SOCK_DGRAM,0);
  if (_inSocket == INVALID_SOCKET)
  {
    LogF("socket() failed with error %d\n",WSAGetLastError());
    WSACleanup();
    return false;
  }

  // bind the socket!
  if ( bind( _inSocket,(struct sockaddr*)&outGoing,sizeof(outGoing )) == SOCKET_ERROR) 
  {
    LogF("bind() failed with error %d will attempt next port:%d\n",WSAGetLastError(),_inPort);
    _inPort++;

    closesocket(_inSocket);
    WSACleanup();

    return false;
  }

  // turn the socket into a non blocking socket
  u_long iMode = 1;
  ioctlsocket(_inSocket, FIONBIO, &iMode);


  return true;
}

bool Tracking::Init()
{
  int  NumberBytes = 0;

  WSADATA wsaData;
  if ( ( NumberBytes = WSAStartup( 0x202,&wsaData ) ) != 0 ) 
  {
    LogF("WSAStartup failed with error %d\n",NumberBytes);
    WSACleanup();
    return false;
  }

  // Create the input socket
  if(CreateInSocket())
    _initOk = true;
  else
  {
    _initOk = false;
    return false;
  }

  return true;
}

Tracking::~Tracking()
{
  closesocket(_inSocket);
  WSACleanup(); 
}

// Remove this object from the arraylist
void Tracking::RemoveObjLink(Object *obj)
{
  // scan through the array and remove any duplicated objects
  for( int i = 0; i < _linkedObjects.Size(); ++i)
    if(_linkedObjects[i].obj == obj)
    {
      _linkedObjects.Delete(i);
      --i;
    }
}

void Tracking::DisableTracking(Object *obj)
{
  Assert(obj);

  RemoveObjLink(obj);
  obj->DisableTracking();
}

void Tracking::AddObject(Object *obj,int sensorId,int appId = 1)
{
  int index = 0;
  Assert(obj);

  // remove any duplication
  RemoveObjLink(obj);

  {
    TObjectLink olink;
    olink.obj = obj;
    olink.sensorId = sensorId;
    olink.appId = appId;

  
    index = _linkedObjects.Add(olink);
  }

  Assert(_linkedObjects[index].obj      == obj);
  Assert(_linkedObjects[index].sensorId == sensorId);
  Assert(_linkedObjects[index].appId    == appId);
}


void Tracking::Update()
{
  // need to load the WSA dll, keep trying until
  // we can load it up
  if(!_initOk)
  {
    Init();
    return;
  }

  FD_ZERO(&fdread);// initializes the set to the NULL set.
  FD_ZERO(&fdwrite);
  FD_ZERO(&fdexcept);

  FD_SET(_inSocket, &fdread); // adds decription of in socket to the set
  FD_SET(_inSocket, &fdwrite);// adds descrption of out socket to the set
  FD_SET(_inSocket, &fdexcept);// adds descrption of out socket to the set

  struct timeval timeout;
  timeout.tv_sec = 0;
  timeout.tv_usec = 0;
  int rc = 0;

  bool firstFrame = true;
  bool frameSampleDone = false;

  // clearing necceary buffers and null
  // links to objects
  _buffer.Clear();
  RemoveObjLink(NULL);

  while(true)
  {
    rc = select(0, &fdread,&fdwrite,&fdexcept, &timeout);
    
    if(rc == SOCKET_ERROR)
    {
      LogF("Winsock error: select function returned with error %d\n", WSAGetLastError());
      return;
    }

    // selection failed
    if(rc == 0)
      break;

    // while there is still data in the socket we want to read it
    if(FD_ISSET(_inSocket, &fdread))
    {
      char buffer[256] = {0};

      // recieving socket
      struct sockaddr_in from;

      int NumberBytes = 0;
      int fromlen = sizeof(from);

      // should never mix error condition with data
      NumberBytes = recvfrom
      (
        _inSocket,
        buffer,
        sizeof(buffer),
        0, 
        (struct sockaddr *)&from,
        &fromlen
      );

      if(firstFrame)
      {
        firstFrame = false;
        
        _howLong += ::GlobalTickCount() - _lastFrame;
        _lastFrame = ::GlobalTickCount();

        _frameCount++;
        if(_frameCount >= 32)
        {
          frameSampleDone = true;
          _frameCount = 0;
        }
      }
      else
        _lateArrival++;

      // only accept data matching what we expect
      if(NumberBytes==sizeof(SensorData))
      {
        SensorData *ptr = (SensorData*) buffer;
        
        for( int i = 0; i < _buffer.Size(); ++i)
        {
          if( _buffer[i].appId    == ptr->appId  &&
              _buffer[i].sensorId == ptr->sensorId )
          {
            _buffer.Delete(i);
            --i;
          }
        }

        // Add the sensor
        int index = _buffer.Add();
        
        _buffer[index].appId = ptr->appId;
        _buffer[index].sensorId = ptr->sensorId;
        
        memcpy(_buffer[index].pos  , ptr->pos  , sizeof(float)*3);
        memcpy(_buffer[index].orien, ptr->orien, sizeof(float)*3);
      }   
      else
         _invalidData++;
    }
    else
      break;      // no data is avaliable
  }

  // new frame has arrived
  if(!firstFrame)
  {
    for( int x = 0; x < _buffer.Size(); ++x)
      for( int y = 0; y < _linkedObjects.Size(); ++y)
      {
        if( _buffer[x].appId    == _linkedObjects[y].appId &&
            _buffer[x].sensorId == _linkedObjects[y].sensorId )
        {
          Vector3 pos(_buffer[x].pos[0],
                      _buffer[x].pos[1],
                      _buffer[x].pos[2]);

          Vector3 orien(_buffer[x].orien[0],
                        _buffer[x].orien[1],
                        _buffer[x].orien[2]);

          if(_linkedObjects[y].obj)
            _linkedObjects[y].obj->TrackerUpdate(pos,orien);
        }
      }
  }

  if(frameSampleDone && _invalidData > 0 )
  {
    LogF("Average incoming time:%f Average late arrival:%f invalidData:%d",(_howLong/32.0f),(_lateArrival/32.0f),_invalidData);
    _howLong = 0;
    _lateArrival = 0;
  }
}



#endif