#include "../wpch.hpp"


#ifndef _FILETRANSFER_UTIL
#define _FILETRANSFER_UTIL

#include <Es/Threads/pocritical.hpp>
#include <Es/Common/global.hpp>
#include <winsock2.h>
#include <winsock.h>
#include <Es/Types/pointers.hpp>
#include <El/QStream/qStream.hpp>

#include "AARTypes.hpp"
#include "../arcadeWaypoint.hpp"
#include "../object.hpp"

#define MAXSERVERNAME      256
#define MAXFILENAME        260
#define FILEBUFFER         1024
#define SERVERTCPLISTEN    1337

#define MAGIC_FILELIST_RESPONSE 0xabcdefae
#define MAGIC_SERVERNAME        0xabcdefaa
#define MAGIC_FILE_REQUEST      0xabcdeffa
#define MAGIC_FILE_RESPONSE     0xabcdeffb

struct ServerInfo
{
  unsigned32 magic;
  char       serverName[MAXSERVERNAME];

  ServerInfo()
  {
    ZeroMemory(this,sizeof(ServerInfo));
  }
};

// File infomation broadcast over the network

struct AARNetFileInfo
{
  unsigned32 magic;
  DWORD      size;
  char       fileName[MAXFILENAME];

  AARNetFileInfo()
  {
    ZeroMemory(this,sizeof(AARNetFileInfo));
  }
};

struct FileSegmentRequest
{
  unsigned32 magic;
  char       fileName[MAXFILENAME];
  DWORD      fileSegment;

  FileSegmentRequest()
  {
    ZeroMemory(this,sizeof(FileSegmentRequest));
  }
};

// file segment, being sent, recieved
struct FileSegmentResponse
{
  unsigned32 magic;
  unsigned32 crcCHECK;   // crc check on buffer
  DWORD      fileSegment;// file segment.
  DWORD      size;
  char       buffer[FILEBUFFER];

  FileSegmentResponse()
  {
    ZeroMemory(this,sizeof(FileSegmentResponse));
  }
};


// holds the file information in a buffer
struct AARFileInfo
{
  char    fileName[260];
  DWORD   fileSize;

  AARFileInfo(){ Init(); }

  void Init()
  {
    ZeroMemory(this,sizeof(AARFileInfo));
  }
};
TypeIsGeneric(AARFileInfo)

void SendFileInfo( unsigned8* ptr, int size, struct sockaddr_in &distant );

//////////////////////////////////////////////////////////////////////////
// Main thread base, used by all threads
//////////////////////////////////////////////////////////////////////////

class ThreadBase : public RefCount
{
public:
  DWORD   _threadId;
  HANDLE  _threadHandle;

  ThreadBase();

  static DWORD WINAPI RunThread(void *args)
  {
    ThreadBase *base = reinterpret_cast<ThreadBase*>(args);
    if(base)
      base->Run();

    // will kill the thread when finished
    return 0;
  }

  // if we're still alive, kill the thread
  virtual ~ThreadBase();
  // Implement your own start
  virtual void Start();
  // Terminate this thread now
  virtual void Terminate();
  // returns if the thread is still active
  virtual bool IsAlive();
  // Implement your own run method
  virtual void Run() = 0;
};



// Here, we take in clients requests and send them the files via the Run method
class ServiceClient : public ThreadBase
{
  SOCKET _socket;// my socket I'm using given to me by the listen thread
public:
  ServiceClient(SOCKET socket);
  ~ServiceClient()
  {
    LogF("DeCon ServiceClient called");
  }

  void Run();
};

// Here, the client thread makes an connection to the server.
// holds the connection open and downloads the file.
class ConnectToServer : public ThreadBase
{
  SOCKET _socket;

  struct sockaddr_in _server;
  int     _port;
  char    _fileName[MAX_PATH];
  int     _fileSize;

  WSADATA _wsaData;
  

  bool InitWSA();

  bool    _downloadFinished;
  int     _downloadedBytes;
  int     _error;
  bool    _abort;
public:
  ConnectToServer(struct sockaddr_in server, int port, const char *fileName, int filesize);
  ~ConnectToServer()
  {
    LogF("DeCon Connect server called");
  }

  // Connect to the server return response.
  bool Connect();

  //! do not run terminate. 
  void Run();

  void Abort(){ _abort = true; }
  bool IsDownloadFinished(){ return _downloadFinished; }
  int  ByesDownloaded(){ return _downloadedBytes; }
  int  GetError(){ return _error; }
};

class ServerListen
{
  int         _port;
  SOCKET      _listenSocket;
  WSADATA     _wsaData;
  sockaddr_in _local;
  bool        _failedToStart;
  RefArray<ServiceClient> _clients;
  

  bool InitWSA();
public:
  ServerListen();
  ~ServerListen();

  bool SocketCreated(){ return _listenSocket != NULL; }
  bool CreateListenSocket(int port);
  bool BindAndListen();
  void ListenForConnections();
};

//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////



// Do not write add elements to the auto array
// you have to call add element then write to it when
// one becomes avaliable.
class Session 
{
private:
  char               _name[256];
  struct sockaddr_in _distant;     // servers ip address!
  bool               _used;
  DWORD              _lastUpdate;

  AutoArray<AARFileInfo> _myFilesDetails;
  bool _addExtraFile;

public:
  Session();
  void Init();

  void SetName(const char *name);
  void UpdateTick(DWORD time);
  const char* GetName();

  AutoArray<AARFileInfo> &GetFileList();
  void AddExtraFile();

  void SetDist(struct sockaddr_in &dist );
  struct sockaddr_in &GetDist();

  void SetUsed(bool used);
  bool GetUsed();
  void Tick();
};
TypeIsGeneric(Session)

RString GetDefaultUserRootDir();

class MachineSessions
{
  ServerListen         _listenServer;
  Ref<ConnectToServer> _downloadFrom;

  PoCriticalSection  _cs;
  AutoArray<Session> _remoteMachines;
  bool               _addSession;

  bool               _downloadFinished;
  bool               _downloadStarted;
  bool               _downloadCanceled;
  int                _downloadedBytes;

  // user dir, because threads suck and 
  // don't allow me to allocate memory on the
  // heap
  char    _userDir[MAX_PATH];

  // who to send the request too.
  struct sockaddr_in _server;

  // download this file from the server.
  RString _downLoadFile;
  DWORD   _fileSize;
public:
  MachineSessions();
  
  void Enter(){ _cs.Lock(); }
  void Leave(){ _cs.leave(); }

  DWORD    GetFileSize();
  RString &GetFileName();
  float    GetCompleted();
  
  void SetDownloadedBytes(int bytes){_downloadedBytes = bytes; }
  void SetDownloadFinished(bool finished){_downloadFinished = finished; }
  bool DownloadFinished(){ return _downloadFinished; }
  bool DownloadCanceled(){ return _downloadCanceled; }
  bool DownloadStarted(){ return _downloadStarted; }

  void CancelDownload();
  void DownLoadFile(RString name,DWORD size, struct sockaddr_in server );
  void SetUserDirectoryForThread();

  // return the user directory for the thread.
  const char *GetUserDir(){ return _userDir; }

  // Get the avaliable session size, and
  // add extra session to the list.
  void     AddSession();
  int      GetSize();
  Session &Get(int index);

  // Tick called from main simulation
  void Tick();
};

extern MachineSessions GMachineSessions;

#endif