#ifdef _MSC_VER
#pragma once
#endif

#ifndef _VBSVISUALS_H
#define _VBSVISUALS_H

#if _VBS2

#include "../wpch.hpp"
#include "../object.hpp"

struct PathTimes: TLinkBidirD
{
  float   arriveTime;
  Vector3 pos;
};

TypeIsSimple(PathTimes);

class EntityAI;
class Entity;
class CStaticMap;

class SideColor
{
protected:
  PackedColor _color[TSideUnknown];
public:
  void Init(ParamEntryPar param);
  PackedColor GetColor(TargetSide side)
  {
    if(side >= TEast && side <= TSideUnknown)
      return _color[side];
    return PackedColor(Color(0,0,0,255));
  }
};

#if _VBS3_RANKS
enum Rank;
struct VBS_Rank
{
  RString Name;
  Rank gameRank;
  Ref<Texture> _image;
  Ref<Texture> _imageCombat;
  VBS_Rank()
  {
    gameRank = (Rank)-1;
    _image = NULL;
    _imageCombat = NULL;
  }
  VBS_Rank(ParamEntryPar rank);
};
TypeIsMovable(VBS_Rank)

class RankSystem
{
protected:
  RString _name;
  //index to the rank which represents the GameRank in the order of gameRanks
  AutoArray<int> _gameRankIndex;

public:
  AutoArray<VBS_Rank> _ranks;

  RankSystem(){};
  RankSystem(ParamEntryPar rankSystem);

  RString GetName() { return _name;}
  void Load(ParamEntryPar rankSystem);

  //get the rank image based on the gameRank
  Texture* GetGameRankImage(Rank rank, bool officialVersion = false);
};
TypeIsMovable(RankSystem)
#endif


struct Line3d
{
  Vector3 _from;
  Vector3 _to;
  PackedColor _color;
  float _width;
  Line3d(Vector3 from, Vector3 to, PackedColor color, float width, bool pp = true) : _from(from), _to(to), _color(color), _width(width){};
};
TypeIsSimple(Line3d);

struct NameText
{
  PackedColor color;
  float x,y;
};

struct DistanceText
{
  PackedColor color;
  float x,y;
};

struct HealthBar
{
  PackedColor barGreen;
  PackedColor barYellow;
  PackedColor barRed;
  float x,y,h;
};

struct RankImage
{
  PackedColor color;
  float x,y,w,h;
};

struct SConfig
{
  NameText nameText;
  DistanceText distanceText;
  HealthBar healthBar;
  RankImage rankImage;
};

struct SoldierName
{
  float   position3DOffset;
  float   screen2DOffset;
  float   textSize;
  float   maxDistance;
  float   alphaOutStart;
  SConfig config[TSideUnknown];

  void Init();
  void LoadNameText(SConfig &cfg,ParamEntryPar param);
  void LoadDistanceText(SConfig &cfg,ParamEntryPar param);
  void LoadHealthBar(SConfig &cfg,ParamEntryPar param);
  void LoadRankImage(SConfig &cfg,ParamEntryPar param);
};

// Drawing of sphere, usually for debugging purposes
void DrawSphere(Vector3 pos, float size, PackedColor color);

class VBSVisuals
{
  float _historyLifeTime;
  bool _drawHitLine;
  bool _drawTrail;
  bool _showTextures;
  bool _showGrid;
  bool _showContours;
  SoldierName _soldierNameCfg;

#if _VBS3_RANKS
  AutoArray <RankSystem> _rankSystems;
#endif


  // Holds an array of additional lines to draw
  // Emptied after each render cycle
  AutoArray <Line3d> _draw3dLines;
  AutoArray <Line3d> _draw3DLinesNoPP;
  void DrawSoldiersNames();

  //draws heightnodes as small spheres around the camera
  void DrawHeightNodes();

public:
  VBSVisuals(): rteVisible(false), editor3DVisible(false){};
  void Init();

  void ReleaseTextures();

  void Draw3DLines(bool postProccesing = true);

  //! Return minimum distance to the 3D line (used for near plane shifting saturation)
  float GetMinDist2Line() const;

#if _VBS3_RANKS
  void LoadRanks(ParamEntryPar ranks);
  Texture* GetRankImage(int rankSystem, Rank rank, bool officialVersion = false);
#endif

  SideColor historyColor;
  float GetHistoryLifeTime() {return _historyLifeTime;};
  void SetHistoryLifeTime(float time);

  bool GetDrawHitLine() {return _drawHitLine;}
  void SetDrawHitLine(bool val);

  bool GetDrawTrail() {return _drawTrail;}
  void SetDrawTrail(bool val);

  bool GetShowTextures() {return _showTextures;}
  void SetShowTextures(bool val) {_showTextures = val;}

  bool GetShowGrid() {return _showGrid;}
  void SetShowGrid(bool val) {_showGrid = val;}

  bool GetShowContours() {return _showContours;}
  void SetShowContours(bool val) {_showContours = val;}

  //Update all trails
  void UpdateTrails();
  bool rteVisible; //set to true, when RTE is visible
  bool editor3DVisible; //set when 3D editor is visible
  bool interfaceHidden; //interface in 3D Editor is hidden
  //draws all 3d elements
  bool Draw3D();
  void Add3dLine(Vector3 from, Vector3 to, PackedColor color, float width=3.0f, bool pp = true)
  {
    Line3d line(from,to,color,width);

    if(pp)
      _draw3dLines.Add(line);
    else
      _draw3DLinesNoPP.Add(line);
  }

  void DrawPausedScreen();

  //draws all map elements
  bool DrawExt(float alpha, CStaticMap& map);
};

extern VBSVisuals GVBSVisuals;

class HistoryLine
{
  TListBidir<PathTimes> _path;
  EntityAI *_entity;
  bool _doDraw;

  void DeletePath(PathTimes *item);
public:
  HistoryLine():_entity(NULL),_doDraw(false){};
  void Clear();
  void Init(EntityAI *entiy);  
  void AddPath();
  void RemoveOldUpdates(); 
  void Draw();
  void DrawExt(float alpha,CStaticMap &map);
  void SetDraw(bool enable){_doDraw = enable;};
  bool GetDraw(){ return _doDraw; };
};

//Functor for updating all entity lines
class RefreshLinesEntitys
{
public:
  bool operator () (Entity *obj) const;
};
//Functor for drawing Line for every Vehicle in 3D
class DrawLinesEntitys
{
public:
  bool operator () (Entity *obj) const;
};
//Functor for drawing all entitys display Text
class DrawDisplayTextEntitys
{
public:
  bool operator () (Entity *obj) const;
};
//Functor for drawing selected objects in 3D
class DrawSelectedObjects
{
public:
  bool operator () (Object *obj) const;
};

//Functor for drawing attached objects in 3D
class DrawAttachedObjects
{
public:
  bool operator () (Object *obj) const;
};

//Functor for drawing Lines on the map
class DrawLinesExt
{
  float _alpha;
  CStaticMap &_map;
public:
  DrawLinesExt(float alpha,CStaticMap &map):_alpha(alpha),_map(map){}; 
  bool operator () (Entity *obj) const;
};

//Functor to enable and disable trail drawing
class EnableDraw
{
  bool enable;
public:
  EnableDraw(bool draw):enable(draw){}; 
  bool operator () (Entity *obj) const;
};

extern void DrawSelectBox(Object* obj, PackedColor color, int highlightFlags = HIGHLIGHT_FLAG);
#endif //_VBS2
#endif //_VBSVISUALS_H


