#include "../wpch.hpp"

#if _AAR
#ifndef AAR_DATATYPES
#define AAR_DATATYPES

#include "../global.hpp"
#include "../objLink.hpp"

#include <El\QStream\serializeBin.hpp>
#include <El\Color\colorsFloat.hpp>

#include <extern/hla/LVCGame_API_Datatypes.h>
#include "AARHLAExtraTypes.hpp"

#include "../object.hpp"

class AAREntityUpdateData;

//! Message types
#define MyEnumType(XX)     \
  XX(enumLoadWorld),       \
  XX(enumEntityLifeSpan),  \
  XX(enumCenterLifeSpan),  \
  XX(enumGroupLifeSpan),   \
  XX(enumUnitLifeSpan),    \
  XX(enumEntityCreateData),\
  XX(enumEntityUpdateData),\
  XX(enumEntityDeleteData),\
  XX(enumFireWeaponData),  \
  XX(enumDetonationData),  \
  XX(enumCenterCreateData),\
  XX(enumGroupCreateData), \
  XX(enumUnitCreateData),  \
  XX(enumTurretUpdate),    \
  XX(enumDoDamage),        \
  XX(enumEntityDestroyed), \
  XX(enumUnitUpdateData),  \
  XX(enumUpdateGroupData), \
  XX(enumBookMarkLifeSpan),\
  XX(enumAttachToUpdate),\
  XX(enumCreateMarker),\
  XX(enumBriefingData),\
  XX(enumDeleteMarker),\
  XX(enumWeatherUpdate),\
  XX(enumAnimationPhase),\
  XX(enumCNRSessionName),\
  XX(enumUnitDeleteData),\
  XX(enumGroupDeleteData),\
  XX(enumCenterDeleteData),\
  XX(enumEntityLifeSpanIndex),\
  XX(enumUnitLifeSpanIndex),\
  XX(enumGroupLifeSpanIndex),\
  XX(enumCenterLifeSpanIndex),\
  XX(enumAddBookMark),\
  XX(enumDeleteBookMark),\
  XX(TypeLimit)

#define ENUM(XX) XX
#define ENUMSTRING(XX) #XX

enum AARDataType 
{
  MyEnumType(ENUM)
};

//////////////////////////////////////////////////////////////////////////
// LifeSpan
//////////////////////////////////////////////////////////////////////////
class LifeSpan :public RefCount
{
public:
  AARDataType _dtype;       // what data type is this object?

  EntityId _newId;  //id which this element is currently linked to

  int _lastMsgIndex; // Last message that was used for this object, used for pause

  float _birth;
  float _death;

  LifeSpan(AARDataType dtype, float birth, float death);
  LifeSpan();

  bool CheckEntityNewId(EntityId id);
  bool CheckEntityId(EntityId id);

  EntityId GetNewIdCanBeNull()
  {
    // This should only be called for example for vehicleId, this can be
    // null, because some people are not attached to vehicles.
    return _newId;
  }

  bool IsNewIdNull()
  {
    if(_newId.ApplicationNumber == 0 && _newId.EntityNumber == 0)
      return true;
    return false;
  }

  EntityId GetNewId();

  virtual void SerializeBin(SerializeBinStream &f);
  virtual bool PreProccess(){return false;};
  virtual const EntityId& GetDataId() const = 0; //used in CheckEntityId
};
TypeIsSimple(LifeSpan)

// Functor to preprocess LifeSpans
struct PreprocessLifeSpan
{
private:
  mutable bool _error;
public:
  PreprocessLifeSpan():_error(false)
  {
  };
  bool operator () (LifeSpan* lifeSpan) const
  {
    if(!lifeSpan->PreProccess())
      _error = true;

    return false;
  };
  bool GetError(){ return _error; };
};
// Functor to search for a EntityID
class FindEntityID
{
private:
  mutable int _result;
  EntityId _id;
  mutable int _index;
public:
  FindEntityID(const EntityId& id):_id(id),_result(-1),_index(0){}

  int GetResult(){return _result;}
  bool operator () (LifeSpan* lifeSpan) const
  {
    if(lifeSpan->GetDataId().ApplicationNumber == _id.ApplicationNumber &&
       lifeSpan->GetDataId().EntityNumber == _id.EntityNumber )
    {
      _result = _index;      
      return true;
    }
    ++_index;
    return false;
  }
};

struct Wounded
{
  int who;
  int num;
  Wounded():who(0),num(0){};
};
TypeIsSimpleZeroed(Wounded);
//////////////////////////////////////////////////////////////////////////
// Object AARStats information, increamented and de-increament durring mission
//////////////////////////////////////////////////////////////////////////
struct AARStat
{
  int roundsFired;
  int enemyKilled;
  int friendlyKilled;
  int enemyWounded;
  int friendlyWounded;
  AARStat():
  roundsFired(0),
    enemyKilled(0),
    friendlyKilled(0),
    enemyWounded(0),
    friendlyWounded(0){};

  AARStat & operator += (const AARStat toAdd)
  {
    roundsFired     += toAdd.roundsFired;
    enemyKilled     += toAdd.enemyKilled;
    friendlyKilled  += toAdd.friendlyKilled;
    enemyWounded    += toAdd.enemyWounded;
    friendlyWounded += toAdd.friendlyWounded;

    return *this;
  }
};

struct AARStats
{
  // real time data
  int roundsFired;
  int explosivesFired;

  // contains the target id
  AutoArray<Wounded> enemyWounded;
  AutoArray<Wounded> friendlyWounded;

  int enemyKilled;
  int friendlyKilled;

  // not good practice
  AARStats(){Init();};
  void Init()
  {
    enemyKilled    = 0;
    friendlyKilled = 0;

    roundsFired = 0;
    explosivesFired = 0;
    enemyWounded.Clear();
    friendlyWounded.Clear();
  };

  void GetStats(AARStat &stat) const
  {
    stat.roundsFired     = roundsFired;
    stat.enemyKilled     = enemyKilled;
    stat.friendlyKilled  = friendlyKilled;
    stat.enemyWounded    = enemyWounded.Size();
    stat.friendlyWounded = friendlyWounded.Size();
  }
};

//////////////////////////////////////////////////////////////////////////
// Object LifeSpan
//////////////////////////////////////////////////////////////////////////
class ObjectLifeSpan : public LifeSpan
{
  typedef LifeSpan base;

public:
  EntityCreateData _data;  // Contains the object information
  //OLink(Entity)    _entity;

  // We keep a track of the states depending on the objects side!
  AARStats         _stats[TSideUnknown]; // Contains stat information on the unit

  RString          _simulation;  // Used for all units, in case addon isn't present
  Ref<AAREntityUpdateData> _lastmsg;

  bool _isInVeh; // depcreated no longer needed

  // Record whos inside the vehicle.
  RefArray<ObjectLifeSpan> _peopleInVehicle;
  
  // Used to remove myself from that vehicle.
  Ref<ObjectLifeSpan>      _lastVehicle;

  ObjectLifeSpan(EntityCreateData &data, AARDataType dtype, float birth, float death);
  ObjectLifeSpan():LifeSpan(){};

  static ObjectLifeSpan *CreateObject(SerializeBinStream &f){ return new ObjectLifeSpan; }

  void SerializeBin(SerializeBinStream &f);
  bool PreProccess();

  bool IsSameEntityId(const Entity *obj);
  
  EntityId GetNewId();
  virtual const EntityId& GetDataId() const {return _data.Id;} //used in CheckEntityId
};
TypeIsSimple(ObjectLifeSpan)

//////////////////////////////////////////////////////////////////////////
// Center LifeSpan
//////////////////////////////////////////////////////////////////////////
class CenterLifeSpan : public LifeSpan
{
  typedef LifeSpan base;

public:
  const CenterCreateData _data; // Contains the object information

  CenterLifeSpan(CenterCreateData &data, AARDataType dtype, float birth, float death)
    :LifeSpan(dtype, birth, death),_data(data){}

  CenterLifeSpan():LifeSpan(){};

  static CenterLifeSpan *CreateObject(SerializeBinStream &f){ return new CenterLifeSpan; }

  void SerializeBin(SerializeBinStream &f);

  virtual const EntityId& GetDataId() const {return _data.Id;} //used in CheckEntityId
};
TypeIsSimple(CenterLifeSpan)

//////////////////////////////////////////////////////////////////////////
// Group LifeSpan 
//////////////////////////////////////////////////////////////////////////
class GroupLifeSpan : public LifeSpan //templates would be nicer, but the serialize functions are not generic yet, maybe rather use AARMessage instead of structs!
{
  typedef LifeSpan base;

public:
  // Direct link to group to reduce type conversion.
  //OLink(AIGroup) _group;

  int _centerLifeSpanId;
  int _leaderLifeSpanId;

  const GroupCreateData _data; // Contains the object information

  GroupLifeSpan(GroupCreateData &data, AARDataType dtype, float birth, float death)
    :LifeSpan(dtype, birth, death),_data(data), _centerLifeSpanId(-1), _leaderLifeSpanId(-1){}

  GroupLifeSpan():LifeSpan(){};

  static GroupLifeSpan *CreateObject(SerializeBinStream &f){ return new GroupLifeSpan; }

  void SerializeBin(SerializeBinStream &f);

  EntityId GetNewId();
  virtual const EntityId& GetDataId() const {return _data.Id;} //used in CheckEntityId
};
TypeIsSimple(GroupLifeSpan)

//////////////////////////////////////////////////////////////////////////
// BookMark Life Span
//////////////////////////////////////////////////////////////////////////
class BookMarkLifeSpan : public LifeSpan
{
  typedef LifeSpan base;

  RString _name;     // bookmark name
  float   _jumpTo;   // time to jump to, user can set his own time
  RString _message;  // message added
public:
  //
  // Possibly, a char array that contains a buffer of the audio to play here.
  //
  BookMarkLifeSpan(RString name,float jumpTo,RString message,AARDataType dtype)
    :LifeSpan(dtype,0.0f,0.0f),
    _name(name),
    _jumpTo(jumpTo),
    _message(message){};

  BookMarkLifeSpan():LifeSpan(){};

  static BookMarkLifeSpan *CreateObject(SerializeBinStream &f){ return new BookMarkLifeSpan; };

  // serialize the message to hard drive.
  void SerializeBin(SerializeBinStream &f);

  const EntityId &GetDataId() const 
  {
    return _newId; 
  };


  RString GetName()   { return _name;    };
  float   GetJumpTo() { return _jumpTo;  };
  RString GetMessage(){ return _message; };
};
TypeIsSimple(BookMarkLifeSpan)


//////////////////////////////////////////////////////////////////////////
// Unit UnitLifeSpan
//////////////////////////////////////////////////////////////////////////
class UnitLifeSpan : public LifeSpan
{
  typedef LifeSpan base;

public:
  // Direct link into games created AIUnit.
  //OLink(AIUnit)  _aiUnit;

  const UnitCreateData _data; // Contains the object information

  int _groupLifeSpanId;
  int _entityLifeSpanId;

  UnitLifeSpan(UnitCreateData &data, AARDataType dtype, float birth, float death)
    :LifeSpan(dtype, birth, death), 
    _data(data), 
    _groupLifeSpanId(-1), 
    _entityLifeSpanId(-1) {}

  UnitLifeSpan():LifeSpan(){};

  static UnitLifeSpan *CreateObject(SerializeBinStream &f){ return new UnitLifeSpan; }

  void SerializeBin(SerializeBinStream &f);
  
  EntityId GetNewId();
  virtual const EntityId& GetDataId() const {return _data.Id;} //used in CheckEntityId
};
TypeIsSimple(UnitLifeSpan)

//////////////////////////////////////////////////////////////////////////
//! Message header, used for reading in the type and the data size
//////////////////////////////////////////////////////////////////////////
class AARMessageHeader
{
public:
  AARDataType _mtype; // type of the message
  int _size;          // size of the AAR message
  AARMessageHeader(){};
  AARMessageHeader(AARDataType mtype,int size){_mtype = mtype;_size= size;};
  void Serialize(SerializeBinStream &f);
};

//! Message base is used to hold all
//! messages timestamp, and types via vtable
class AARMessageBase : public RefCount
{
public:
  float _time;  // Time of which the message was 
  bool  _passPP;// Did we pass pre proccessing
 
  AARMessageBase();
  virtual void Apply(){}; // implimented but not required
  virtual void Serialize(SerializeBinStream &f) = 0;
  
  virtual void PreProccess(int messageIndex){}
  virtual void ProccessStatistics(bool moveForward){}

  virtual AARDataType TypeIs() = 0;
  virtual int SizeOf() = 0;
};

class AARMessageWorldLoad : public AARMessageBase
{
  RString _worldName;
  AutoArray<RString> _addons; //! Currently no longer required

public:
  AARMessageWorldLoad():AARMessageBase(){};
  void Init();
  void Serialize(SerializeBinStream &f);
  void Apply();

  AARDataType TypeIs(){ return enumLoadWorld; };
  int SizeOf(){ return _worldName.GetLength(); };
};

class AARMessageCNRSession : public AARMessageBase
{
public:
  RString _sessionName;
  long    _sessionTime;
  
  AARMessageCNRSession():AARMessageBase(){Init(); };

  // Init this special message
  void Init()
  {
    _sessionName = RString();
    _sessionTime = 0;
  };

  void Serialize(SerializeBinStream &f);

  AARDataType TypeIs(){ return enumCNRSessionName; };
  int SizeOf(){ return _sessionName.GetLength();};
};

#define LIFE_SPAN_TEMPLATE(CLASSNAME,ENUMTYPE) \
class CLASSNAME : public AARMessageBase \
{ \
public: \
  int _index; \
  float _time; \
  CLASSNAME (int index,float time):_index(index),_time(time){}; \
  void Serialize(SerializeBinStream &f){}\
  AARDataType TypeIs(){ return ENUMTYPE;}\
  int SizeOf(){ return sizeof(CLASSNAME); }\
}; \

LIFE_SPAN_TEMPLATE(AAREntityLifeSpanIndex,enumEntityLifeSpanIndex)
LIFE_SPAN_TEMPLATE(AARUnitLifeSpanIndex,enumUnitLifeSpanIndex)
LIFE_SPAN_TEMPLATE(AARGroupLifeSpanIndex,enumGroupLifeSpanIndex)

class AAREntityCreateData : public AARMessageBase
{
  const EntityCreateData _data;
public:
  AAREntityCreateData(){};
  AAREntityCreateData(const EntityCreateData &data,float time):_data(data){_time = time;};
  void Serialize(SerializeBinStream &f);
  void PreProccess(int messageIndex){};
  EntityCreateData GetData()
  {
    return _data;
  }
  const EntityCreateData &GetRawData(){ return _data; };

  AARDataType TypeIs(){ return enumEntityCreateData; };
  int SizeOf(){ return sizeof(*this); };
};

class AAREntityUpdateData : public AARMessageBase
{
  const EntityUpdateData _data;
  int _lastMessageIndex;
  int _idIndex;
  int _vehicleIdIndex;
public:
  AAREntityUpdateData();
  AAREntityUpdateData(const EntityUpdateData &data,float time);
  ~AAREntityUpdateData();

  void Serialize(SerializeBinStream &f);
  int GetIndex(){ return _idIndex; };
  void PreProccess(int messageIndex);
  bool KeyFrameCanPlay(float time);
  void ProccessStatistics(bool moveForward);
  int  GetPreviousMessageIndex(){ return _lastMessageIndex; };
  EntityUpdateData GetDataUpdateIndex(int msgIndex, bool forwardTimeLine = true);
  EntityUpdateData GetData();
  const EntityUpdateData &GetRawData(){ return _data; };

  AARDataType TypeIs(){ return enumEntityUpdateData; };
  int SizeOf(){ return sizeof(*this); };
  bool operator==(const AAREntityUpdateData &other);
};

class AAREntityDeleteData : public AARMessageBase
{
  const EntityDeleteData _data;
  int _entityIdIndex;
public:
  AAREntityDeleteData(){};
  AAREntityDeleteData(const EntityDeleteData &data,float time):_data(data){_time = time;};
  void Serialize(SerializeBinStream &f);
  void PreProccess(int messageIndex){}
  EntityDeleteData GetData(){return _data;}
  const EntityDeleteData &GetRawData(){ return _data; };

  AARDataType TypeIs(){ return enumEntityDeleteData; };
  int SizeOf(){ return sizeof(*this); };
};

class AARFireWeaponData : public AARMessageBase
{
  const FireWeaponData _data;
  int _fireIdIndex;
  int _targetIdIndex;
  
public:
  AARFireWeaponData()
  {
    _fireIdIndex   = -1;
    _targetIdIndex = -1;
  };
  AARFireWeaponData(const FireWeaponData &data,float time):_data(data){_time = time;};
  void Serialize(SerializeBinStream &f);
  void PreProccess(int messageIndex);
  FireWeaponData GetData();
  const FireWeaponData &GetRawData(){ return _data; };
  void ProccessStatistics(bool moveForward);

  AARDataType TypeIs(){ return enumFireWeaponData; };
  int SizeOf(){ return sizeof(*this); };
};

class AAREntityDestroyed : public AARMessageBase
{
  const EntityDestroyed _data;
  int _killed;
  int _killer;
public:
  AAREntityDestroyed()
  {
    _killed = -1;
    _killer = -1;
  };
  AAREntityDestroyed(const EntityDestroyed &data,float time):_data(data){_time = time;};
  void Serialize(SerializeBinStream &f);
  void PreProccess(int messageIndex);
  EntityDestroyed GetData();
  const EntityDestroyed &GetRawData(){ return _data; };
  void ProccessStatistics(bool moveForward);

  AARDataType TypeIs(){ return enumEntityDestroyed; };
  int SizeOf(){ return sizeof(*this); };
};


class AARUnitUpdateData : public AARMessageBase
{
  const UnitUpdateData _data;

  int _idIndex;    // Index refering to the UnitLifeSpan. (So, what happens when a brain switches persons?)
  int _personIndex;// AIUnit refering to the ObjectLifeSpan, not referenced....
  int _groupIndex; // AIGroup refering to the GroupLifeSpan.

  bool _insertedMessaged; // are we the inserted message?
public:
  AARUnitUpdateData(bool insertedMessaged):
  _insertedMessaged(insertedMessaged),
  _idIndex(-1),
  _personIndex(-1),
  _groupIndex(-1)
  {};

  AARUnitUpdateData():_idIndex(-1),_personIndex(-1),_groupIndex(-1),_insertedMessaged(false){};
  AARUnitUpdateData(const UnitUpdateData &data,float time):_data(data){_time = time;};
  void Serialize(SerializeBinStream &f);
  void PreProccess(int messageIndex);
  UnitUpdateData GetData();
  const UnitUpdateData &GetRawData(){ return _data; };
  // no stastics 
  void ProccessStatistics(bool moveForward){};

  AARDataType TypeIs(){ return enumUnitUpdateData; };
  int SizeOf(){ return sizeof(*this); };
};

class AARDoDamageData : public AARMessageBase
{
  const DoDamageData _data;
  int _who;
  int _killer;
public:
  AARDoDamageData()
  {
    _who    = -1;
    _killer = -1;
  };
  AARDoDamageData(const DoDamageData &data, float time):_data(data){_time = time;};
  void Serialize(SerializeBinStream &f);
  void PreProccess(int messageIndex);
  DoDamageData GetData();
  const DoDamageData &GetRawData(){ return _data; };
  void ProccessStatistics(bool moveForward);

  AARDataType TypeIs(){ return enumDoDamage; };
  int SizeOf(){ return sizeof(*this); };
};

class AARDetonationData : public AARMessageBase
{
  const DetonationData _data;
  int _idFiringId;
  int _idTargetId;
public:
  Vector3 _shoterPos;
  AARDetonationData()
  {
    _shoterPos = Vector3(0.0,0.0,0.0);
    _idFiringId = -1;
    _idTargetId = -1;
  };
  AARDetonationData(const DetonationData &data,float time):_data(data){_time = time;};
  void Serialize(SerializeBinStream &f);
  void ProccessStatistics(bool moveForward);
  void PreProccess(int messageIndex);
  DetonationData GetData();
  const DetonationData &GetRawData(){ return _data; };

  AARDataType TypeIs(){ return enumDetonationData; };
  int SizeOf(){ return sizeof(*this); };
};

class AARCenterCreateData : public AARMessageBase
{
  const CenterCreateData _data;
  int _idIndex;
public:
  AARCenterCreateData():_idIndex(-1){};
  AARCenterCreateData(const CenterCreateData &data,float time):_data(data){_time=time;};
  void Serialize(SerializeBinStream &f);
  void PreProccess(int messageIndex);
  CenterCreateData GetData();
  const CenterCreateData &GetRawData(){ return _data; };

  AARDataType TypeIs(){ return enumCenterCreateData; };
  int SizeOf(){ return sizeof(*this); };
};

class AARGroupCreateData : public AARMessageBase
{
  const GroupCreateData _data;
  int _idIndex;
  int _centerIdIndex;
  int _leaderIndex;
public:
  AARGroupCreateData():_idIndex(-1),_centerIdIndex(-1),_leaderIndex(-1){};
  AARGroupCreateData(const GroupCreateData &data, float time):_data(data){_time=time;};
  void Serialize(SerializeBinStream &f);
  void PreProccess(int messageIndex);
  GroupCreateData GetData();
  const GroupCreateData &GetRawData(){ return _data; };

  AARDataType TypeIs(){ return enumGroupCreateData; };
  int SizeOf(){ return sizeof(*this); };
};

class AARUpdateGroupData : public AARMessageBase
{
  const UpdateGroupData _data;
  int _idIndex;
  int _centerIdIndex;
  int _leaderId;
public:
  AARUpdateGroupData():_idIndex(-1),_centerIdIndex(-1),_leaderId(-1){};
  AARUpdateGroupData(const UpdateGroupData &data,float time):_data(data){_time=time;};
  void Serialize(SerializeBinStream &f);
  void PreProccess(int messageIndex);
  UpdateGroupData GetData();
  const UpdateGroupData &GetRawData(){ return _data; };

  AARDataType TypeIs(){ return enumUpdateGroupData; };
  int SizeOf(){ return sizeof(*this); };
};

class AARUnitCreateData : public AARMessageBase
{
  const UnitCreateData _data;
  int _idIndex;
  int _personIndex;
  int _groupIndex;

public:
  AARUnitCreateData():_idIndex(-1),_personIndex(-1),_groupIndex(-1){};
  AARUnitCreateData(const UnitCreateData &data, float time):_data(data){_time=time;};
  void Serialize(SerializeBinStream &f);
  void PreProccess(int messageIndex);
  UnitCreateData GetData();
  const UnitCreateData &GetRawData(){ return _data; };

  AARDataType TypeIs(){ return enumUnitCreateData; };
  int SizeOf(){ return sizeof(*this); };
};

class AARTurretUpdate : public AARMessageBase
{
  const TurretUpdate _data;
  int _idIndex;
public:
  AARTurretUpdate();
  AARTurretUpdate(const TurretUpdate &data, float time):_data(data){_time=time;};
  void Serialize(SerializeBinStream &f);
  void PreProccess(int messageIndex);
  TurretUpdate GetData();
  const TurretUpdate &GetRawData(){ return _data; };

  AARDataType TypeIs(){ return enumTurretUpdate; };
  int SizeOf(){ return sizeof(*this); };
};

class AARAttachToUpdate : public AARMessageBase
{
  const AttachToData _data;
  int _objIndex;
  int _attachToIndex;
  int _previousMessage;

public:
    AARAttachToUpdate();
    AARAttachToUpdate(const AttachToData &data, float time):_data(data)
    {
      _time = time;
      _objIndex = _attachToIndex = -1;
      _previousMessage = -1;
    };
    void Serialize(SerializeBinStream &f);
    void PreProccess(int messageIndex);
    AttachToData GetData();
    
    // get the previous message..
    int PreviousMessage(){ return _previousMessage; };
    AARDataType TypeIs(){ return enumAttachToUpdate; };
    int SizeOf(){ return sizeof(*this); };
};

class AARUnitDeleteData : public AARMessageBase
{
  EntityId _data;
public:
  AARUnitDeleteData(){}
  AARUnitDeleteData(const EntityId &id, float time)
  {
    _data = id;
    _time = time;
  }
  void Serialize(SerializeBinStream &f);
  void PreProccess(int messageIndex){}
  EntityId GetData(){ return _data; }

  AARDataType TypeIs(){ return enumUnitDeleteData; }
  int SizeOf(){ return sizeof(*this); }
};

class AARGroupDeleteData : public AARMessageBase
{
  EntityId _data;
public:
  AARGroupDeleteData(){}
  AARGroupDeleteData(const EntityId &id, float time)
  {
    _data = id;
    _time = time;
  }
  void Serialize(SerializeBinStream &f);
  void PreProccess(int messageIndex){}
  EntityId GetData(){ return _data; }

  AARDataType TypeIs(){ return enumGroupDeleteData; }
  int SizeOf(){ return sizeof(*this); }
};


class AARCenterDeleteData : public AARMessageBase
{
  EntityId _data;
public:
  AARCenterDeleteData(){}
  AARCenterDeleteData(const EntityId &id, float time)
  {
    _data = id;
    _time = time;
  }
  void Serialize(SerializeBinStream &f);
  void PreProccess(int messageIndex){}
  EntityId GetData(){ return _data; }

  AARDataType TypeIs(){ return enumCenterDeleteData; }
  int SizeOf(){ return sizeof(*this); }
};



struct MarkerLayered
{
  RString tName;
  float   a;
  float   b;
  float   x;
  float   y;

  void SerializeBin(SerializeBinStream &f)
  {
    f << tName;
    f.TransferBinary(a);
    f.TransferBinary(b);
    f.TransferBinary(x);
    f.TransferBinary(y);
  }
};
TypeIsGeneric(MarkerLayered);

typedef unsigned long ULONG;
// Note lots of text
struct MarkerCreateData
{
  Vector3P pos;
  RString  name;
  RString  text;
  int      markerType;
  RString  type;
  RString  colorName;
  ULONG    color;
  RString  fillName;
  float    size;
  float    a;
  float    b;
  float    angle;
  bool     selected;
  bool     autosize;
  bool     isHidden;

  AutoArray<MarkerLayered> layers;
};


class AARCreateMarker : public AARMessageBase
{
  friend class AAR;

  const MarkerCreateData _data;
  friend class AARDeleteMarker;
  int     _previousMessage;
public:
  AARCreateMarker();
  AARCreateMarker(const ArcadeMarkerInfo *marker, float time);
  void Serialize(SerializeBinStream &f);
  void PreProccess(int messageIndex);
  int PreviousMessage(){ return _previousMessage; };
  void RunMessage();

  AARDataType TypeIs(){ return enumCreateMarker; };
  int SizeOf(){ return sizeof(MarkerCreateData); };
};

struct WeatherUpdateStruct
{
  float   _seaLevelOffset;
  float   _overcastSetSky;
  float   _overcastSetClouds;
  float   _fogSet;
  float   _cloudsPos;
  float   _cloudsAlpha;
  float   _cloudsBrightness;
  float   _cloudsSpeed;
  float   _skyThrough;
  float   _rainDensity;
  float   _rainDensityWanted;
  float   _rainDensitySpeed;
  int     _rainNextChange;
  int     _lastWindSpeedChange;
  int     _gustUntil;
  Vector3 _windSpeed;
  Vector3 _currentWindSpeed;
  Vector3 _currentWindSpeedSlow;
  Vector3 _gust;
  int     _year;
  int     _month;
  int     _day;
  int     _hour;
  int     _minute;
};

struct ApplyWeatherMessage;

class AARWeatherUpdate : public AARMessageBase 
{
  friend class AAR;
  WeatherUpdateStruct _data;
  int     _previousMessage;
public:
  AARWeatherUpdate();
  AARWeatherUpdate(const ApplyWeatherMessage *weather,float time);
  void Serialize(SerializeBinStream &f);
  void PreProccess(int messageIndex);
  void RunMessage();

  int PreviousMessage(){ return _previousMessage; };
  AARDataType TypeIs(){ return enumWeatherUpdate; };
  int SizeOf(){ return sizeof(*this); };
};

class AARAnimationPhase : public AARMessageBase
{
  const EntityAnimationPhase _data;
  int _previousMessage;
  int _idIndex;

public:
  AARAnimationPhase();
  AARAnimationPhase(const EntityAnimationPhase &data, float time );
  void Serialize(SerializeBinStream &f);
  void PreProccess(int messageIndex);
  void RunMessage();

  int PreviousMessage(){ return _previousMessage; }
  AARDataType TypeIs(){ return enumAnimationPhase; }
  int SizeOf(){ return sizeof(*this); }
};

class AARAddBookMark : public AARMessageBase
{
  RString _name;     // bookmark name
  float   _jumpTo;   // time to jump to, user can set his own time
  RString _message;  // message added
public:
  friend class AAR;

  AARAddBookMark(){};
  AARAddBookMark(RString name,float jumpTo,RString message):_name(name),_jumpTo(jumpTo),_message(message){}

  void Serialize(SerializeBinStream &f);
  void PreProccess(int messageIndex){ _passPP = true; }
  AARDataType TypeIs(){ return enumAddBookMark; }
  int SizeOf(){ return sizeof(*this); }
};

class AARDeleteBookMark : public AARMessageBase
{
  RString _name;
public:
  friend class AAR;

  AARDeleteBookMark(){};
  AARDeleteBookMark(RString name):_name(name){}
  void PreProccess(int messageIndex){ _passPP = true; }
  void Serialize(SerializeBinStream &f);
  AARDataType TypeIs(){ return enumDeleteBookMark; }
  int SizeOf(){ return sizeof(*this); }
};



class AARDeleteMarker : public AARMessageBase
{
  RString _name;
  int     _previousMessage;
public:
  AARDeleteMarker();
  AARDeleteMarker(const RString &name, float time);
  void Serialize(SerializeBinStream &f);
  void PreProccess(int messageIndex);
  void RunMessage();

  int PreviousMessage(){ return _previousMessage; };
  AARDataType TypeIs(){ return enumDeleteMarker; };
  int SizeOf(){ return sizeof(*this);};
};

class AARBriefingData : public AARMessageBase
{
  RString _data;
public:
  AARBriefingData();
  AARBriefingData(const RString &data, float time );
  void Serialize(SerializeBinStream &f);
  void PreProccess(int messageIndex);
  const RString &GetData(){ return _data; }

  AARDataType TypeIs(){ return enumBriefingData; };
  int SizeOf(){ return sizeof(*this); };
};

class LandScapeRemoveAnimals
{
  OLinkArray(Entity) &_objects;
public:
  LandScapeRemoveAnimals(OLinkArray(Entity) &objects):_objects(objects){};
  bool operator () (Entity *obj) const;
};

//! Serialization methods for the above HLA data types
void SerializeEntityCreateData(const EntityCreateData &input,SerializeBinStream &f);
void SerializeEntityUpdateData(const EntityUpdateData &input,SerializeBinStream &f);
void SerializeDoDamage(const DoDamageData& input,SerializeBinStream &f);
void SerializeCenterCreateData(const CenterCreateData &input,SerializeBinStream &f);

void SerializeUpdateGroupData(const UpdateGroupData &input,SerializeBinStream &f);
void SerializeGroupCreateData(const GroupCreateData &input,SerializeBinStream &f);

void SerializeUnitUpdateData(const UnitUpdateData &input,SerializeBinStream &f);
void SerializeUnitCreateData(const UnitCreateData &input,SerializeBinStream &f);

void SerializeFireWeaponData(const FireWeaponData &input,SerializeBinStream &f);
void SerializeDetonationData(const DetonationData &input,SerializeBinStream &f);

void SerializeEntityId(const EntityId &input,SerializeBinStream &f);
void SerializeCharArray(const char *input,int arraySize,SerializeBinStream &f);
void SerializeGeneric3DType(const Generic3DType &input,SerializeBinStream &f);

#endif
#endif
