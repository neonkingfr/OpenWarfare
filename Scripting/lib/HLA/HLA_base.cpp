#include "../wpch.hpp"

#if _EXT_CTRL

#include "HLA_base.hpp"
#include "../gameStateExt.hpp"
#include "../shots.hpp"
#include "../soldierOld.hpp"
#include "../dikcodes.h"
#include "../airplane.hpp"
#include "../helicopter.hpp"
#include "AAR.hpp"
#include "el/Common/perfProf.hpp"
#include "../LaserTarget.hpp"
#include "../landscape.hpp"

#if _HLA
  #include "DelayImp.h"
#endif

HlaInterface _hla;

#define HLADebug 0 && _ENABLE_CHEATS

#if HLADebug 
#define ALogF LogF
#else
#define ALogF
#endif

//! Union for vehicles
union UnionWeapon
{
  int weaponId;
  struct  
  {
    short vehWeaponId;
    short vehTurretId;
  };
};

HlaInterface::HlaInterface()
{
  _client = NULL;

#if _ENABLE_CHEATS
  _debugHLAUpdate = 1;
#else
  _debugHLAUpdate = 0;
#endif
#if _HLA
  _HLAInit = _HLALibLoaded = false;
  _isCNRInitSession = false;
  _isCNRRecording = false;
  _hlaAPI = NULL;
#endif
  _AARSnapShotActive = false;
}

bool HlaInterface::ExternalActive()
{
  return IsHLAActive() 
#if _AAR
    || GAAR.IsRecording()
#endif    
    ;
}

// Do we need to send out information to HLA?
bool HlaInterface::SendToHLA(ExtCtrl ctrl)
{
  return IsHLAActive() && ctrl != EC_HLA && Glob.vbsEnableLVC;
};

inline void CpyEntity(EntityId &dest, const Object *src)
{
  if(!src)
  {
    dest.ApplicationNumber = 0;
    dest.EntityNumber = 0;
    return;
  }
  dest.ApplicationNumber = src->GetNetworkId().creator;
  dest.EntityNumber = src->GetNetworkId().id;
}


inline void CpyEntity(EntityId &dest, const NetworkObject *src)
{
  if(!src)
  {
    dest.ApplicationNumber = 0;
    dest.EntityNumber = 0;
    return;
  }
  dest.ApplicationNumber = src->GetNetworkId().creator;
  dest.EntityNumber = src->GetNetworkId().id;
}

inline void CpyEntity(Entity *dest, const EntityId &src)
{
  if(!src.ApplicationNumber && !src.EntityNumber)
  {
    dest = NULL;
    return;
  }
  dest = _hla.GetEntity(src);
}

inline void CpyCoord(Coordinate3DType &dest, const Vector3 &src)
{
  dest.x = src.X(); dest.y = src.Y(); dest.z = src.Z();
}
inline void CpyCoord(Vector3 &dest, Coordinate3DType &src)
{
  dest[0] = src.x; dest[1] = src.y; dest[2] = src.z;
}


///returns the VBS Object based on the HLA EntityId
NetworkObject* HlaInterface::GetNetObjFromHLAId(const EntityId& Id)
{
  NetworkId nwID = NetworkId(Id.ApplicationNumber, Id.EntityNumber);
  return _client->GetObject(nwID);
}

Entity* HlaInterface::GetEntity(const EntityId& Id)
{
  return GetEntity(GetNetObjFromHLAId(Id));
}

EntityAI* HlaInterface::GetEntityAI(const EntityId& Id)
{
  return GetEntityAI(GetNetObjFromHLAId(Id));
}

Entity* HlaInterface::GetEntity(NetworkObject* nwObj)
{
  if(!nwObj)
    return NULL;
  return dyn_cast<Entity>(nwObj);
}

EntityAI* HlaInterface::GetEntityAI(NetworkObject* nwObj)
{
  if(!nwObj)
    return NULL;
  return dyn_cast<EntityAI>(nwObj);
}

///returns EntityAI from a NetworkObject
const EntityAI* HlaInterface::GetEntityAI(const NetworkObject* nwObj)
{
  if(!nwObj)
    return NULL;
  return dyn_cast<const EntityAI>(nwObj);
}

//! send new map origin to LVC
void HlaInterface::SetOrigin(double easting, double northing, long zone, char hemisphere)
{
#if _HLA
  if(IsHLAActive() && Glob.vbsEnableLVC)
  {
    if(_debugHLAUpdate & 2)
      ALogF("[to HLA] SetOrigin: Easting: %f, Northing: %f, Zone: %d, hemisphere: %c"
            , easting, northing, (int)zone, hemisphere
      );
    _hlaAPI->setOrigin(easting, northing, zone, hemisphere);
  }
#endif
}

//////////////////////////////////////////////////////////////////////////
/// incoming UpdateEvent from HLA
//////////////////////////////////////////////////////////////////////////
EntityId onHLAEntityCreated(EntityCreateData data)
{
  return _hla.HLAEntityCreated(EC_HLA, data);
}
int onHLAEntityUpdated(EntityUpdateData data)
{
  return _hla.HLAEntityUpdated(EC_HLA, data);    
}
void onHLAEntityDeleted(EntityDeleteData data)
{
  _hla.HLAEntityDeleted(EC_HLA, data);
}
void onHLAWeaponFired(FireWeaponData data)
{
  _hla.HLAWeaponFired(EC_HLA, data);
}

void onHLADetonateMunition(DetonationData data)
{
  _hla.HLADetonateMunition(EC_HLA, data);
}

//////////////////////////////////////////////////////////////////////////
/// incoming UpdateEvent from AAR
//////////////////////////////////////////////////////////////////////////

EntityId onAAREntityCreated(EntityCreateData data)
{
  return _hla.HLAEntityCreated(EC_AAR, data);
}
int onAAREntityUpdated(EntityUpdateData data)
{
  return _hla.HLAEntityUpdated(EC_AAR, data);    
}

void onAAREntityDeleted(EntityDeleteData data)
{
  _hla.HLAEntityDeleted(EC_AAR, data);
}
void onAARGroupDeleted(GroupDeleteData data)
{
  _hla.HLAGroupDeleted(EC_AAR, data);
}

void onAARWeaponFired(FireWeaponData data)
{
  _hla.HLAWeaponFired(EC_AAR, data);
}
void onAARDetonateMunition(DetonationData data)
{
  _hla.HLADetonateMunition(EC_AAR, data);
}

void onAARDoDamage(DoDamageData data)
{
  _hla.HLADoDamage(EC_AAR,data);
}

void onAARVehicleDestroyed(EntityDestroyed data)
{
  _hla.HLAVehicleDestroyed(EC_AAR,data);
}

EntityId onAARCenterCreated(const CenterCreateData data)
{
  return _hla.HLACenterCreated(EC_AAR, data);
}
EntityId onAARGroupCreated(const GroupCreateData data)
{
  return _hla.HLAGroupCreated(EC_AAR, data);
}
EntityId onAARUnitCreated(const UnitCreateData data)
{
  return _hla.HLAUnitCreated(EC_AAR, data);
}
void onAARUnitUpdated(UnitUpdateData data)
{
  return _hla.HLAUnitUpdated(EC_AAR, data);
}
void onAARGroupUpdated(UpdateGroupData data)
{
  return _hla.HLAGroupUpdated(EC_AAR, data);
}

void onAARAttachToUpdated(AttachToData data)
{
  return _hla.HLAAttachToUpdated(EC_AAR, data);
}

void onAARTurretUpdate(TurretUpdate data)
{
  return _hla.HLAOnTurrentUpdate(EC_AAR, data);
}

//////////////////////////////////////////////////////////////////////////
// Create Vehicle is based on gamestateExt function. 
// Needed here to set the hla property 
//////////////////////////////////////////////////////////////////////////
Entity *HlaInterface::CreateVehicle(ExtCtrl ctrl, Vector3 &pos, RString type, TargetSide side, RString Name)
{
  Vector3 normal = VUp;
  Ref<Entity> veh = GWorld->NewNonAIVehicleWithID(type,NULL);
  if (!veh) return NULL;

  veh->SetExternalController(ctrl);

  EntityAI *vehAI = dyn_cast<EntityAI>(veh.GetRef());
  Matrix3 dir;
  Matrix4 transform;

  transform.SetPosition(pos);
  dir.SetUpAndDirection(normal,VForward);
  transform.SetOrientation(dir);

  if (vehAI) veh->PlaceOnSurface(transform);

  veh->SetTransform(transform);

  if (true)// init
  {
    veh->Init(transform, true);
    if (vehAI) vehAI->OnEvent(EEInit);
  }

  // if vehicle is static, add it to building list

  EntityAIFull * vehAIFull = dyn_cast<EntityAIFull>(vehAI);
  if(vehAIFull)
  {
    vehAIFull->SetTargetSide(side);
    //    TODO: need to set the name
  }
  if (vehAI)
  {
    const EntityAIType *type = vehAI->GetType();
    if( type->IsKindOf(GWorld->Preloaded(VTypeStatic)) )
    {
      // to prevent any problems we will insert all user created objects into slow vehicle list
#if 0
      // ask vehicle type if we can add it to the building list
      if (!type->IsSimulationRequired() && !type->IsSoundRequired())
      {
        GWorld->AddBuilding(veh);
        GetNetworkManager().CreateVehicle(veh, VLTBuilding, "", -1);
      }
      else
#endif
      {                                               
        GWorld->AddSlowVehicle(veh);
        GetNetworkManager().CreateVehicle(veh, VLTSlowVehicle, "", -1);
      }
    }
    else
    {
      int id = vehiclesMap.Size();
      vehiclesMap.Resize(id + 1);

      GWorld->AddVehicle(veh);
      GetNetworkManager().CreateVehicle(veh, VLTVehicle, "", id);

      vehiclesMap[id] = vehAI;
    }
  }
  else if (dyn_cast<Shot>(veh.GetRef()))
  {
    GWorld->AddFastVehicle(veh);
    GWorld->AddSupersonicSource(veh);
    GetNetworkManager().CreateObject(veh);
  }
  else
  {
    GWorld->AddAnimal(veh);
    GetNetworkManager().CreateVehicle(veh, VLTAnimal, "", -1);
  }

  return veh;
}

EntityId HlaInterface::HLAEntityCreated(ExtCtrl ctrl, EntityCreateData data)
{
  EntityId RetId = {0,0};
  
  Vector3 pos; CpyCoord(pos, data.Pos);
  RString name = ctrl== EC_HLA ? RString("LVC_")+ RString(data.Name) : RString(data.Name);
  Entity* ent = CreateVehicle(ctrl, pos, RString(data.EntityType), TargetSide(data.Side), name);
  if(!ent)
    ALogF("[HLA] Error creating entity: %s", data.EntityType);
  else
  {
    NetworkId nwId = ent->GetNetworkId();

    RetId.ApplicationNumber = nwId.creator;
    RetId.EntityNumber = nwId.id;

    // set the variable name, eg... this will be the hla controler
    ent->SetVarName(name);

    Person *person = dyn_cast<Person>(ent);
    if(person)
    {
      AIUnitInfo &info = person->GetInfo();

      // set the recorded profile
      info._name = RString(data.Profile);
    }

    if (_debugHLAUpdate & 1)
      ALogF("[from %s] entity:%s unitName:%s Profile:%s Id:(%d/%d)"
      , (ctrl == EC_HLA) ? "HLA" : "AAR"
      , data.EntityType
      , data.Name
      , data.Profile
      , RetId.ApplicationNumber
      , RetId.EntityNumber
      );
  }
  return RetId;
}

/// Functor to get the turret based on a index
class GetTurretFromIndex : public ITurretFunc
{
protected:
  int _findIndex;
public:
  int actIndex;
  Turret *turret;
  TurretContext context;

  GetTurretFromIndex(const int index)
  {
    _findIndex = index;
    actIndex = 0;
    turret = NULL;
  }

  bool operator () (EntityAIFull *entity, TurretContext &context)
  {
    if (!context._turret) return false; // continue
    if (actIndex == _findIndex)
    {
      turret = context._turret;
      this->context = context;

      return true;
    }
    ++actIndex;
    return false; // continue
  }
};

int offset = 0;

int HlaInterface::HLAEntityUpdated(ExtCtrl ctrl, EntityUpdateData data)
{
  NetworkObject* nwObj = GetNetObjFromHLAId(data.Id);
  if(!nwObj || !nwObj->AllowExternalControl(ctrl))
    return 0;
  
  Entity* veh = GetEntity(nwObj);
  if(!veh)
    return 0;

    ALogF("[from %s] Update (%d/%d) Lights: %04x PP: %04x Damage:%.3f Destoryed:%d Pos: [%.3f, %.3f, %.3f], Speed: [%.3f, %.3f, %.3f], Weapon: %d, Elev: %.2f, Azi: %.2f, Stance: %d, WeaponPost: %d, inVeh (%d/%d) ePos:%d crewIndex:%d"
    , ctrl == EC_HLA ? "HLA" : "AAR"
    , data.Id.ApplicationNumber
    , data.Id.EntityNumber
    , (int) (data.Lights & LVC_LIGHT_HEAD )
    , (int) (data.Appearance & LVC_POWER_PLANT_ON)
    , data.Damage
    , (data.Appearance & LVC_DESTROYED)
    , data.Pos.x, data.Pos.y, data.Pos.z
    , data.Velocity.x,  data.Velocity.y,  data.Velocity.z
    , data.SelectedWeapon
    , data.Elevation, data.Azimuth
    , data.Stance
    , data.PrWeaponPosture
    , data.VehicleId.ApplicationNumber
    , data.VehicleId.EntityNumber
    , data.EPos
    , data.CrewIndex
    );

  Vector3 pos; CpyCoord(pos, data.Pos);
  Vector3 dir; CpyCoord(dir, data.Dir);
  Vector3 up;  CpyCoord(up, data.Up);
  Vector3 speed;CpyCoord(speed, data.Velocity);

  Matrix3 orient;
  Matrix4 trans;
  trans.SetIdentity();
  orient.SetUpAndDirection(up,dir);
  trans.SetOrientation(orient);
  trans.SetPosition(pos);
  veh->SetSpeed(speed);

  // fix for AAR versions above 12. When objects are slaved
  // they use their relative position and attached orientation
  if(veh->IsAttached() && GAAR.GetVersion()>=12 && veh->GetAttachFlags() == 0)
  {
    veh->SetAttachedOffset(pos);
    veh->SetAttachMatrix(orient);
  }
  
  float destroyed = (data.Appearance & LVC_DESTROYED) ? 1.0f : 0.0f; 
  bool heal = veh->IsDestroyed() && !(data.Appearance & LVC_DESTROYED);

  veh->SetDestroyed(destroyed);

  // if we're a transport, and we're traving back int ime
  // and our health is bellow total damage then reset our status
  Transport *transportTest = dyn_cast<Transport>(veh);
  if(transportTest && (data.Damage < veh->GetTotalDamage()))
  {
    veh->SetDamage(data.Damage);
    veh->ResetStatus();
  }
  else
  {
    if(data.Damage < veh->GetTotalDamage())
      heal = true;

    // SetDamage calls event handelers
    if(veh->GetTotalDamage() != data.Damage)
      veh->SetDamage(data.Damage);
  }

  EntityAIFull* vehAI = dyn_cast<EntityAIFull>(veh);
  if(!vehAI) //e.g. Shot
  {
    veh->Move(trans);
    return 1;
  }

  GameVarSpace *vars = veh->GetVars();
  if (vars)
  {
    RString urn(data.URN);
    GameValue var = urn;
    vars->VarSet("URN", var);
  }

  Man* man = dyn_cast<Man>(vehAI);

  if(man && !nwObj->AllowExternalControl(EC_HLA)) //doesn't work for HLA yet, since we need to create a unit!
  {
    AIBrain* brain = man->Brain();
    if(brain)
    {
      Transport* trans = NULL;
      trans = man->Brain()->GetVehicleIn();
      if(trans) //man is currently in a transport
      {
        if(!data.VehicleId.EntityNumber) //new data has no parent vehicle
        {

          Eject(man, trans);
          brain->UnassignVehicle();
          trans->GetOutFinished(man->Brain());


          ALogF("[AAR] Eject done");
        }
        else //check if the position has changed
        {
          int crewIndex; //current index
          EPosInfo epos; //current position
          EntityId dummy;
          FillCrewInfo(man, dummy, crewIndex, epos);
          if(data.EPos >= Pos_Driver && data.EPos <= Pos_Cargo)
          {
            if(data.EPos != epos || data.CrewIndex != crewIndex)
            { //we have to change position
              switch(data.EPos)
              {
                case Pos_Driver:
                  {
                    ActionBasic action(ATMoveToDriver);
                    trans->ChangePosition(&action, man, true);
                    break;
                  }
                case Pos_Turret:
                  {
                    GetTurretFromIndex func(data.CrewIndex);
                    if(trans->ForEachTurret(func)) //found
                    {
                      ActionTurret action(ATMoveToTurret, trans, func.turret);
                      trans->ChangePosition(&action, man, true);
                    }else
                      ALogF("[aar] turret not found");
                    break;
                  }
                case Pos_Cargo:
                  {
                    ActionIndex action(ATMoveToCargo, trans, data.CrewIndex);
                    trans->ChangePosition(&action, man, true);
                    break;
                  }
              }
            }
          }else
            ALogF("[AAR] invalid EPos: %d" , data.EPos);
        }
        return 1; //no further calculation needed, since we are in a vehicle
      }
      else //man is currently not in a transport yet
      {
        if(data.VehicleId.EntityNumber)
        {//we have to getin!
          EntityAI* transEnt = GetEntityAI(data.VehicleId);
          Transport* trans2 = dyn_cast<Transport>(transEnt);
          if(trans2)
          {
            switch(data.EPos)
            {
              case Pos_Driver:
                {
                  //! Wait until person moves out of his position!
                  if(!trans2->QCanIGetIn()) return true;

                  trans2->GetInDriver(man,false);
                  brain->AssignAsDriver(trans2); //do we actually need a brain?
                  brain->OrderGetIn(true);
                  ALogF("[AAR] GetInDriver done");
                  break;
                }
              case Pos_Turret:
                {
                  GetTurretFromIndex func(data.CrewIndex);
                  if(trans2->ForEachTurret(func)) //found
                  {
                    AIBrain *oldGunner = trans2->GetGunnerAssigned();
                    if(man && man->Brain() && oldGunner)
                      if(oldGunner != man->Brain())
                        oldGunner->UnassignVehicle();

                    //! Wait until person moves out of turret position
                    if(!trans2->QCanIGetInTurret(func.turret,man)) return true;

                    trans2->GetInTurret(func.turret, man);
                    brain->AssignAsGunner(trans2); //do we actually need a brain?
                    brain->OrderGetIn(true);
                  }else
                    ALogF("[aar] turret not found");
                  break;
                }
              case Pos_Cargo:
                {
                  //! Wait until person moves from cargo position
                  if(trans2->QCanIGetInCargo(man,data.CrewIndex) == -1) return true;

                  trans2->GetInCargo(man, true, data.CrewIndex);
                  brain->AssignAsCargo(trans2); //do we actually need a brain?
                  brain->OrderGetIn(true);
                  break;
                }
              default: ALogF("[aar] Epos invalid, cannot getin!");
            }
          }else
            ALogF("[AAR] Error getin vehicle (trans2==NULL) invalid!");
          return 1; //no further calculation needed, since we are in a vehicle
        }
      }
    }
    else
      ALogF("No Brain");
  }

  bool powerPlantOn = (data.Appearance & LVC_POWER_PLANT_ON) != 0;
  if(powerPlantOn && !vehAI->EngineIsOn())
    vehAI->EngineOn();
  else
    if(!powerPlantOn && vehAI->EngineIsOn())
      vehAI->EngineOff();

  if(nwObj->GetExternalController() != EC_AAR) //no ground align for AAR elements!
  {
    Airplane* plane = dyn_cast<Airplane>(vehAI);
    Helicopter* heli = dyn_cast<Helicopter>(vehAI);
    
    bool AlignToGround = !((heli || plane ) && ((data.Appearance & LVC_POWER_PLANT_ON) || speed.SizeXZ() > 0.1));
    if(AlignToGround) 
    {
      vehAI->PlaceOnSurface(trans); //also changes the orientation!
      orient.SetUpAndDirection(trans.Orientation().DirectionUp(),orient.Direction());

      trans.SetOrientation(orient);
    }
  }

  vehAI->Move(trans);

  bool lightsOn = (data.Lights & LVC_LIGHT_HEAD) > 0;
  vehAI->SetPilotLight(lightsOn);

//  vehAI->SetTotalDamage(data.Damage, true);
  
  if(man)
  {
    man->RecalcPositions(trans);
    ManAction selAction;

    if(heal)
    {
      man->_isDead = false;
      man->ResetMovement(0,-1);
    }
  
    
    if(data.Appearance & LVC_DESTROYED)
      selAction = ManActDie;
    else
      selAction = GetAction(man, data.Stance, data.PrWeaponPosture);
    
    //TODO: Need conversion for HLA gateway
    TurretContext context;
    if (vehAI->GetPrimaryGunnerTurret(context))
    {
      //////////////////////////////////////////////////////////////////////////
      /// Because weapon mag index does not stay the same from recording to playback
      /// we need to convert the data.selectWeapon that is a weapon type to 
      /// the corresponding mags index. Then use that index
      /// to tell the engine to change to that weapon.
      //////////////////////////////////////////////////////////////////////////
      AutoArray<MagazineSlot> &mRef = context._weapons->_magazineSlots;

      int magIndex = -1;
      bool found = false;

      // If the selected weapon is nothing, then we've allready found it
      if(data.SelectedWeapon == -1 ) 
      {
        found = true;
        magIndex = -1;
      }
      else
      {     
        for( magIndex = 0; magIndex < mRef.Size(); ++magIndex )
          if((mRef[magIndex]._weapon->_weaponType & data.SelectedWeapon) != 0)
          {
            found = true;
            break;
          }
      }

      // If the weapon still cannot be found,
      // default back to generic primary weapon, if 
      // no primary weapon can be found, then we dont play any animations
      if(!found)
      {
        magIndex = man->FindPrimaryWeapon();
        if(magIndex != -1)
          found = true;
      }

      if(man->_weaponsState._currentWeapon != magIndex)
      {
        context._weapons->SelectWeapon(vehAI,magIndex);
        man->OnWeaponSelected(); 

        if(man->FindPrimaryWeapon() == magIndex)
        {
          man->SwitchAction(ManActCombat);
          ALogF("[HLA] Selecting primary weapon");
        }
        else if(man->BinocularSelected())
        {
          man->SwitchAction(ManActBinocOn);        
          ALogF("[HLA] Play Binoc Move");
        }
        else if(man->LauncherSelected())
        {
          man->SwitchAction(ManActWeaponOn);        
          ALogF("[HLA] Play Launcher Move");
        }
        else if (man->IsHandGunSelected())
        {
          man->SwitchAction(ManActHandGunOn);
          ALogF("[HLA] Play HandGun Move");
        }
        else
        {
          man->SwitchAction(ManActStand);
          ALogF("[HLA] Play Nothing Move");
        }
      }

      // dont perform any movement animation
      if
      (
        !man->BinocularSelected() && 
        !man->LauncherSelected()  &&
        !man->IsHandGunSelected()
      )
      {
        ActionMap *map = man->Type()->GetActionMap(man->GetCurrentMoveId());
        MoveId moveWanted = map ? man->Type()->GetMove(map, selAction) : man->GetDefaultMove();
        man->SetPublicMoveQueue(MotionPathItem(moveWanted),false); 
      }
    }

    man->_gunXRot = data.Elevation;
    man->_gunXRotWanted = data.ElevationWanted;
    man->_gunYRot = data.Azimuth;
    man->_gunYRotWanted = data.AzimuthWanted;
    man->RecalcGunTransform();
  }//man
  else
  { 
    TurretContextV turret;
    ZeroMemory(&turret,sizeof(turret));
    if(vehAI->GetPrimaryGunnerTurret(turret))
    {
      //if there is a VBS gunner in the turret we don't set the articulation
      //this enables us to get into remote controlled turrets
      if(turret._turret && (!turret._gunner || (turret._gunner && turret._gunner->IsExternalControlled())))
      {
        //Setting an external controller will disable the ability to get into a vehicle as gunner
        //turret._turret->SetExternalController(ctrl);         
        turret._turretVisualState->_xRot = data.Elevation;
        turret._turret->_xRotWanted = data.ElevationWanted;
        turret._turretVisualState->_yRot = data.Azimuth;
        turret._turret->_yRotWanted = data.AzimuthWanted;
      }

      /**
       For Vehicles, we're in luck, the weapon selection does not change
       from recording and playback. So here, we select directly the weapon index
       that we want, but just do some validation to make sure the selected weapon
       is in fact valid.
       */
      if(turret._weapons && (turret._weapons->ValidateWeapon(data.SelectedWeapon) != -1))
      {
        turret._weapons->SelectWeapon(vehAI, data.SelectedWeapon);
        vehAI->OnWeaponSelected();
      }
    }
  }


/* Missing:
  bool FlamesPresent;                 
*/
  return 1;
}

//copy from gamestateExt.cpp
void HlaInterface::Eject(Person *man, Transport *trans)
{
  if (!man) return;

  AIBrain *unit = man->Brain();
  if (!unit)
  {
    man->SetDelete();
    return;
  }
  unit->DoGetOut(trans, false, false);

  AIGroup *group = unit->GetGroup();
  if (group) group->UnassignVehicle(trans);
}

void DeleteVehicle(Entity *veh); //gamestateExt.cpp

void HlaInterface::HLAEntityDeleted(ExtCtrl ctrl, EntityDeleteData data)
{
  ALogF("[from HLA] Entity deleted");

  NetworkObject* nwObj = GetNetObjFromHLAId(data);

  if(!nwObj || !nwObj->AllowExternalControl(ctrl))
  {
    ALogF("HLAEntityDelete recieved: is not a network object and is not externaly controlled");
    return;
  }

  //_client->DeleteObject(nwObj->GetNetworkId(), true);
  EntityAI* vehAI = GetEntityAI(nwObj);
  if(vehAI && vehAI->IsLocal()) 
  {
    LogF("Is local deleting object");
    DeleteVehicle(vehAI);
  }
}

int SearchMagContext(TurretContext &context,int weaponType)
{
  AutoArray<MagazineSlot> &mRef = context._weapons->_magazineSlots;

  // find matching type for weaponID
  for( int i = 0; i < mRef.Size(); ++i )
    if((weaponType & mRef[i]._weapon->_weaponType) != 0)
      return i;

  return -1;
}


//only called from AAR
void HlaInterface::HLAGroupDeleted(ExtCtrl ctrl, GroupDeleteData data)
{
  //make sure to delete units of group first!

  if (_debugHLAUpdate & 1)
    ALogF("[from HLA] Group deleted");

  NetworkObject* nwObj = GetNetObjFromHLAId(data);
  if(!nwObj || !nwObj->AllowExternalControl(ctrl))
    return;

  //  _client->DeleteObject(nwObj->GetNetworkId(), true);
  AIGroup* group = dynamic_cast<AIGroup*>(nwObj);
  if(group)
  {
//    AICenter* center = group->GetCenter();
//    int NumGroupsBefore = center->NGroups();

      // remove all units in this group
      for( int i = 0; i < group->NUnits(); ++i )
      {
        AIUnit *unit = group->GetUnit(i);
        if(unit)
          unit->RemoveFromGroup();
      }

    // people currently inside this group!
    //if(group->NUnits()==0)
      group->RemoveFromCenter();
//    int NumGroupsAfter = center->NGroups();
//    ALogF("[aar] delete group: center before %d after %d"
//      , NumGroupsBefore, NumGroupsAfter);
  }
}

// HLA - Means command coming in to fire weapon
void HlaInterface::HLAWeaponFired(ExtCtrl ctrl, FireWeaponData data)
{
  if (_debugHLAUpdate & 1)
    ALogF("[from HLA] Weapon %d fired by (%d/%d) at (%d/%d)"
      , data.WeaponId
      , data.FiringId.ApplicationNumber
      , data.FiringId.EntityNumber
      , data.TargetId.ApplicationNumber
      , data.TargetId.EntityNumber
      ); 

  NetworkObject* nwObj = GetNetObjFromHLAId(data.FiringId);
  if(!nwObj || !nwObj->AllowExternalControl(ctrl))
    return;

  EntityAIFull* vehAI =  dyn_cast<EntityAIFull>(GetEntityAI(nwObj));
  if(vehAI)
  {
    TurretContext context;
    ZeroMemory(&context,sizeof(TurretContext));

    int magIndex = -1;

    Man *man = dyn_cast<Man>(vehAI);
    if(man)
    {
      if(man->GetPrimaryGunnerTurret(context))
      {
        RString ammoType(data.MunitionType);

        // We ignore the weapon type here, no use to us.
        // just find the matching magazine and that becomes our weaponId
        AutoArray<MagazineSlot> &mRef = context._weapons->_magazineSlots;

        int index = -1;
        for( int i = 0; i < mRef.Size(); ++i )
          if((data.WeaponId & mRef[i]._weapon->_weaponType) != 0)
            if(mRef[i]._magazine && mRef[i]._magazine->_type )
            {
              if(mRef[i]._magazine->_type->_ammo)
              {
                  RString ammoName = mRef[i]._magazine->_type->_ammo->GetName();
                  if(ammoType == ammoName)
                  {
                    index = i; 
                    break;
                  }
              }
            }
          
        
        // update mag index
        magIndex = index;
        if(magIndex==-1)
          magIndex = SearchMagContext(context,MaskSlotPrimary);
      }
     
      if(magIndex==-1)
      {
        ALogF("Cannot find mag index or pimary turret for man");
        return;        
      }
    }

    Transport *tran = dyn_cast<Transport>(vehAI);
    if(tran)
    {
      UnionWeapon *sweapon = (UnionWeapon*) &data.WeaponId;
      int turret   = (int) sweapon->vehTurretId;
      int weaponId = (int) sweapon->vehWeaponId;

      GetTurretFromIndex indexTurret(turret);
      if(tran->ForEachTurret(indexTurret))
      {
        magIndex = weaponId;    
        context = indexTurret.context;
      }
      else
      {
        // failed to we attempt to use the primary weapon, old recording
        if(tran->GetPrimaryGunnerTurret(context))
        {
          AutoArray<MagazineSlot> &mRef = context._weapons->_magazineSlots;

          if(data.WeaponId >= 0 && data.WeaponId < mRef.Size())
            magIndex = data.WeaponId;

          // Finaly attempt, we just grab the primary turrent!
          if(magIndex == -1)
          {
            ALogF("Failed to get mag index on transport, using primary!");
            magIndex = SearchMagContext(context,MaskSlotPrimary);  
          }
        }
      }       
    }

    if((magIndex != -1) && context._weapons)
    {
      const MagazineSlot &slot = context._weapons->_magazineSlots[magIndex];
      const Magazine *magazine = slot._magazine;

      EntityAI* target = GetEntityAI(data.TargetId);

      RemoteFireWeaponInfo remoteInfo;

      CpyCoord(remoteInfo._position, data.Pos);
      CpyCoord(remoteInfo._direction, data.Velocity);
      remoteInfo._direction.Normalize();

      remoteInfo._visible = 1;
      vehAI->FireWeaponEffects(context, magIndex, magazine, target,&remoteInfo);
      GetNetworkManager().FireWeapon(vehAI, context._gunner, magIndex, magazine, target, remoteInfo);
    }
    else
    {
      ALogF("Cannot find mag index or pimary turret");
      return;
    }
  }

/*
EntityAI* vehAI = GetEntityAI(nwObj);
if(vehAI)
{
EntityAIFull* vehAIFull = dyn_cast<EntityAIFull>(vehAI);
if(vehAIFull)
{
TurretContext context;
if (vehAIFull->GetPrimaryGunnerTurret(context))
{
int weapon = data.WeaponId;
const MagazineSlot &slot = context._weapons->_magazineSlots[0];
const Magazine *magazine = slot._magazine;

EntityAI* target = GetEntityAI(data.TargetId);

RemoteFireWeaponInfo remoteInfo;

CpyCoord(remoteInfo._position, data.Pos);
CpyCoord(remoteInfo._direction, data.Velocity);
remoteInfo._direction.Normalize();

remoteInfo._visible = 1;
vehAIFull->FireWeaponEffects(context, weapon, magazine, target,&remoteInfo);
GetNetworkManager().FireWeapon(vehAIFull, context._gunner, weapon, magazine, target, remoteInfo);
//        vehAIFull->FireWeapon(context, weapon, target);
}
}
}
*/
}

void HlaInterface::HLAVehicleDestroyed(ExtCtrl ctrl,EntityDestroyed data)
{
  //?
}

// From aiArcade.cpp
void ProcessJoinGroups(AIGroup *from, AIGroup *to, bool silent);

void HlaInterface::HLAUnitUpdated(ExtCtrl ctrl, UnitUpdateData data)
{

  if (_debugHLAUpdate & 1)
    ALogF("[from HLA] Unit Updated");
  
  NetworkObject* unitNwObj = GetNetObjFromHLAId(data.Id);

  if(!unitNwObj || !unitNwObj->AllowExternalControl(ctrl))
    return;

  AIUnit* unit = dynamic_cast<AIUnit*>(unitNwObj);
  if(!unit)
  {
    ALogF("[from AAR] HLAUnitUpdated: No unit! return.");
    return;
  }

  NetworkObject* grpNwObj = GetNetObjFromHLAId(data.GroupId);
  AIGroup* grp = dynamic_cast<AIGroup*>(grpNwObj);
  if(!grp)
  {
    ALogF("[from AAR] HLAUnitUpdated: No group! return.");
    return;
  }

  if(unit->GetGroup() != grp)
  {
    unit->RemoveFromGroup();
    bool emptyGroup = (grp->UnitsCount() == 0);
    grp->AddUnit(unit); //TODO: check

    // quickfix
    // when we join an empty group, we're the leader
    if(emptyGroup)
      grp->SelectLeader(unit);
  }

  //TODO: assert person?
 
}

void HlaInterface::HLAGroupUpdated(ExtCtrl ctrl, UpdateGroupData data)
{

  if (_debugHLAUpdate & 1)
    ALogF("[from HLA] Group Updated");

  NetworkObject* grpNwObj = GetNetObjFromHLAId(data.Id);

  if(!grpNwObj || !grpNwObj->AllowExternalControl(ctrl))
    return;

  AIGroup* grp = dynamic_cast<AIGroup*>(grpNwObj);
  //  Assert(unit);
  if(!grp)
  {
    ALogF("[from AAR] HLAGroupUpdated: No group! return.");
    return;
  }

  NetworkObject* leaderNwObj = GetNetObjFromHLAId(data.LeaderId);

  AIUnit* leader = dynamic_cast<AIUnit*>(leaderNwObj);
  //  Assert(grp);
  if(!leader)
  {
    ALogF("[from AAR] HLAGroupUpdated: Invalid leader id: (%d,%d)! return."
      , data.LeaderId.ApplicationNumber
      , data.LeaderId.EntityNumber
      );
    return;
  }

  // leave my current group and join this one
  if(leader->GetGroup() != grp)
  {
    leader->RemoveFromGroup();
    grp->AddUnit(leader);
  }

  if(grp->Leader() != leader)
  {
    grp->SelectLeader(leader);
  }
  // can the center id change as well?
  // update number?
}

/// Functor to get all the turrets
class GetAllTurretsIntoArray : public ITurretFuncV
{
public:// i hate access methods!
  RefArray<Turret> _turrets;
  RefArray<TurretVisualState> _turretVisualStates;
  GetAllTurretsIntoArray(){};
  bool operator () (EntityAIFull *entity, TurretContextV &context)
  {
    if(!context._turret) return false;
    _turrets.Add(context._turret);
    _turretVisualStates.Add(context._turretVisualState);
    return false;
  }
};


void HlaInterface::OnTurretUpdate(Entity *entity)
{
  Transport *tran = dyn_cast<Transport>(entity);
  if(!tran) return;

  GetAllTurretsIntoArray functor;
  tran->ForEachTurret(functor);// got get them baby!

  for( int i = 0; i < functor._turrets.Size(); ++i)
  {
    TurretUpdate data;
    ZeroMemory(&data,sizeof(data));

    CpyEntity(data.Id,tran);
    data.TurretNum = i;

    data.Azimuth         = functor._turretVisualStates[i]->_yRot;
    data.Elevation       = functor._turretVisualStates[i]->_xRot;
    data.AzimuthWanted   = functor._turrets[i]->_xRotWanted;
    data.ElevationWanted = functor._turrets[i]->_yRotWanted;

#if _AAR
    if((ExtCtrl)tran->GetExternalController() != EC_AAR)
      GAAR.UpdateTurret(data);
#endif
  }
}

void HlaInterface::HLAOnTurrentUpdate(ExtCtrl ctrl, TurretUpdate data)
{
  if (_debugHLAUpdate & 1)
    ALogF("[from HLA] AttachToUpdate");

  NetworkObject *nObj = GetNetObjFromHLAId(data.Id);
  Transport *tran = dyn_cast<Transport>(nObj);
  if(!tran) return;
  
  // find the matching turret id.
  GetAllTurretsIntoArray functor;
  tran->ForEachTurret(functor);// got get them baby!

  for( int i = 0; i < functor._turrets.Size(); ++i)
  {
    if(data.TurretNum == i)
    {
      functor._turretVisualStates[i]->_yRot = data.Azimuth;
      functor._turretVisualStates[i]->_xRot = data.Elevation;
      functor._turrets[i]->_xRotWanted = data.AzimuthWanted;
      functor._turrets[i]->_yRotWanted = data.ElevationWanted;
      break;
    }
  }
}

void HlaInterface::WeatherUpdate(ApplyWeatherMessage *msg)
{
  if(!GAAR.IsRecording())
    return;

  GAAR.WeatherUpdate(msg);
}

void HlaInterface::RecordAnimationPhase(Entity *entity,RString name,float phase)
{
  if(!GAAR.IsRecording()) return;
  if(!entity) return;

  EntityAnimationPhase data;
  data.obj = NullEntityId;

  CpyEntity(data.obj,entity);
  data.animation = name;
  data.phase     = phase;

  GAAR.RecordAnimationPhase(data);
}
/*
 HLA CNR interface intergration
  
 Description:
  Its the responablity for the HLA CNR interface when
  a communication exception occures. To Release the 
  interface to CNR and to set the _isCNRInitSession to false.
  All preceding calls to the function for CNR shall return a CNRException
  its not the responability of the caller to release the interface. Only to 
  re-init the interface from a safe location.

  Calling the functions when a HLA interface or dummy interface is not
  started is a cause of a bug.
*/

bool HlaInterface::ConnectDummyInterface()
{
#if _HLA
  // Dont connect, if cnrLog has been disabled
  if(!Glob.vbsEnableCNR) return false;

  if(!_HLAInit && !_HLADummyInit)
  {
    bool   result = _hlaAPI->init();
    _HLADummyInit = result;

    if(result)
      ALogF("[CNRLOG] Dummy interface inited");
    else
      ALogF("[CNRLOG] Dummy interface not inited");


    if(result && _hlaAPI->CNRLogInitRemoteLogger()== CNRSUCCESS)
    {
      LogF("[CNRLOG] Dummy interface connected to remote logger");     
      _isCNRInitSession = true;
    }
    else
    {
      if(result)
        ALogF("[CNRLOG] Dunny interface failed to connect to remote logger, but connected to HLA");
      else
        ALogF("[CNRLOG] Both failed to connect to hla and remote logger");
    }

    return result;
  }

  return false;
#else
  return false;
#endif
}

bool HlaInterface::DummyInterfaceOn()
{
#if _HLA
  return _HLADummyInit; 
#else
  return false;
#endif
}

void HlaInterface::DisconnectDummyInterface()
{
#if _HLA 
  if(_HLADummyInit)
  {
    if(_isCNRInitSession)
    {
      ALogF("[CNRLOG] release remote logger, still active");
      _isCNRInitSession = false;
      _hlaAPI->CNRLogReleaseRemoteLogger();
    }

    ALogF("[CNRLOG] Dummy Disconnect");   
    _hlaAPI->shutdown();
    Time start = Glob.time;
    while(!_hlaAPI->isStopped() && Glob.time - start < 2) 
      _hlaAPI->tick(1);

    if(!_hlaAPI->isStopped())
      ALogF("[CNRLOG] Error, couldn't disconnect HLA!");

    _HLADummyInit = false;
  }
#endif
}


// THIS IS THE ONLY METHOD THAT IS NONE BLOCKING
// WE USE THIS TO TEST IF THE CONNECTION IS STILL ALIVE
// IF ITS ISN'T WE HAVE TO REMOVE THE INTERFACE, AND RECONNECT
// AGAIN. NOTIFY USER IF CONNECTION FAILED
void HlaInterface::TestCNRConnection()
{
}

FaultCode HlaInterface::CNRInitRemoteLogger()
{
#if _HLA
  if(!Glob.vbsEnableCNR)
    return API_NOT_PRESENT;
  
  ALogF("[CNRLOG] init remote logger");

  FaultCode result = (FaultCode) _hlaAPI->CNRLogInitRemoteLogger();
  if(result == CNRSUCCESS)
  {
    ALogF("[CNRLOG] connection successfull");
    _isCNRInitSession = true;
  }
  else
  {
    ALogF("[CNRLOG] Failed to connect to remote logger");
  }

  return result;
#else
  return API_NOT_PRESENT;
#endif
}

bool HlaInterface::IsCNRSessionStarted()
{
#if _HLA
  return _isCNRInitSession;
#else
  return false;
#endif
}

void HlaInterface::CNRReleaseRemoteLogger()
{
#if _HLA
  if(_isCNRInitSession)
  {
    ALogF("[CNRLOG] Release remote logger");
    _hlaAPI->CNRLogReleaseRemoteLogger();
    _isCNRInitSession = false;
  }
  else
  {
    ALogF("[CNRLOG] nothing to release");
  }
#endif
}

FaultCode HlaInterface::CNRStopRecording()
{
#if _HLA
  Assert(_HLAInit || _HLADummyInit);

  TestCNRConnection();

  if(!_isCNRInitSession) 
    return CNRCOMEXCEPTION;

  FaultCode result = (FaultCode) _hlaAPI->CNRLogStopRecording();
  if(result == CNRCOMEXCEPTION)
    CNRReleaseRemoteLogger();

  return result;
#else
  return API_NOT_PRESENT;
#endif
}

FaultCode HlaInterface::CNRStartRecording()
{
#if _HLA
  Assert(_HLAInit || _HLADummyInit);

  TestCNRConnection();

  if(!_isCNRInitSession)
    return CNRCOMEXCEPTION;

  FaultCode result = (FaultCode) _hlaAPI->CNRLogStartRecording();
  if(result == CNRCOMEXCEPTION)
    CNRReleaseRemoteLogger();

  return result;
#else
  return API_NOT_PRESENT;
#endif
}

FaultCode HlaInterface::CNRIsRecording(bool &isRecording)
{
#if _HLA
  Assert(_HLAInit || _HLADummyInit);

  TestCNRConnection();

  if(!_isCNRInitSession)
    return CNRCOMEXCEPTION;

  FaultCode result = (FaultCode) _hlaAPI->CNRLogIsRecording(isRecording);
  if(result == CNRCOMEXCEPTION)
    CNRReleaseRemoteLogger();

  return result;
#else
  return API_NOT_PRESENT;
#endif
}

FaultCode HlaInterface::CNROpenSession(const char *sessionName)
{
#if _HLA 
  Assert(_HLAInit || _HLADummyInit);

  TestCNRConnection();

  if(!_isCNRInitSession)
    return CNRCOMEXCEPTION;

  FaultCode result = (FaultCode) _hlaAPI->CNRLogOpenSession(sessionName);
  if(result == CNRCOMEXCEPTION)
    CNRReleaseRemoteLogger();

  return result;
#else
  return API_NOT_PRESENT;
#endif
}

bool HlaInterface::CNRGetEvent(int index,CNRLogEvent &event)
{
#if _HLA
  Assert(_HLAInit || _HLADummyInit);

  TestCNRConnection();

  if(!_isCNRInitSession)
    return false;

  return _hlaAPI->CNRLogGetEvent(index,event);
#else
  return API_NOT_PRESENT;
#endif
}

int HlaInterface::CNRGetEventSize()
{
#if _HLA
  Assert(_HLAInit || _HLADummyInit);

  TestCNRConnection();

  if(!_isCNRInitSession)
    return 0;

  return (FaultCode) _hlaAPI->CNRLogGetEventSize();
#else
  return 0;
#endif 
}

FaultCode HlaInterface::CNRStopPlaying()
{
#if _HLA
  Assert(_HLAInit || _HLADummyInit);

  TestCNRConnection();

  if(!_isCNRInitSession)
    return CNRCOMEXCEPTION;

  FaultCode result = (FaultCode) _hlaAPI->CNRLogStopPlaying();
  if(result == CNRCOMEXCEPTION)
    CNRReleaseRemoteLogger();
  
  return result;
#else
  return API_NOT_PRESENT;
#endif
}

FaultCode HlaInterface::CNRPlayOneRecord(int index)
{
#if _HLA 
  Assert(_HLAInit || _HLADummyInit);

  TestCNRConnection();

  if(!_isCNRInitSession)
    return CNRCOMEXCEPTION;

  FaultCode result = (FaultCode) _hlaAPI->CNRLogPlayOneRecord(index);
  if(result == CNRCOMEXCEPTION)
    CNRReleaseRemoteLogger();

  return result;
#else
  return API_NOT_PRESENT;
#endif
}

FaultCode HlaInterface::CNRPlayAllRecords()
{
#if _HLA 
  Assert(_HLAInit || _HLADummyInit);

  TestCNRConnection();

  if(!_isCNRInitSession)
    return CNRCOMEXCEPTION;

  FaultCode result = (FaultCode) _hlaAPI->CNRLogPlayAllRecords();
  if(result == CNRCOMEXCEPTION)
    CNRReleaseRemoteLogger();

  return result;
#else
  return API_NOT_PRESENT;
#endif
}

long HlaInterface::CNRGetStartTime()
{
#if _HLA
  Assert(_HLAInit || _HLADummyInit);

  TestCNRConnection();

  if(!_isCNRInitSession)
    return CNRCOMEXCEPTION;
  
  return _hlaAPI->CNRLogGetStartTime();
#else
  return 0.0;
#endif
}

FaultCode HlaInterface::CNRDeleteSession(const char *sessionName)
{
#if _HLA 
  Assert(_HLAInit || _HLADummyInit);

  if(!_isCNRInitSession)
    return CNRCOMEXCEPTION;

  FaultCode result = (FaultCode) _hlaAPI->CNRLogDeleteSession(sessionName);
  if(result == CNRCOMEXCEPTION)
    CNRReleaseRemoteLogger();

  return result;
#else
  return API_NOT_PRESENT;
#endif
}

void HlaInterface::HLAAttachToUpdated(ExtCtrl ctrl, AttachToData data)
{
  if (_debugHLAUpdate & 1)
    ALogF("[from HLA] AttachToUpdate");

  NetworkObject* nObj      = GetNetObjFromHLAId(data.obj);
  NetworkObject* nAttachTo = GetNetObjFromHLAId(data.attachTo);
  
  Object *obj      = dyn_cast<Object>(nObj);
  Object *attachTo = dyn_cast<Object>(nAttachTo);

  // we have a invalid object
  // no point continuing.
  if(!obj) return;

  Vector3 attachPos;
  CpyCoord(attachPos, data.com);

  // at this stage obj is valid
  if(attachTo) // we want to attach
    obj->AttachTo(attachTo,attachPos,data.memoryIndex,data.flags);
  else // we want to disconnect
    obj->Detach();
}

void HlaInterface::HLADoDamage(ExtCtrl ctrl, DoDamageData data)
{
  // at this stage, I dont know what the callback should do.
  // typicaly the damage should be updated in the next updateentity
  // frame, that should shortly follow this if everthing goes correct.
  // typicaly it wont.
}

void HlaInterface::HLADetonateMunition(ExtCtrl ctrl, DetonationData data)
{
//  if (_debugHLAUpdate & 1)
    ALogF("[from %s] Detonate Ammo ""%s"": Owner (%d/%d)"
       ", DirectHit (%d/%d), Speed [%.1f, %.1f, %.1f]"
       ", Pos [%.1f, %.1f, %.1f]"
       , ctrl == EC_HLA ? "HLA":"AAR"   
       , data.MunitionType
       , data.FiringId.ApplicationNumber
       , data.FiringId.EntityNumber
       , data.TargetId.ApplicationNumber
       , data.TargetId.EntityNumber
       , data.Velocity.x, data.Velocity.y, data.Velocity.z
       , data.Pos.x, data.Pos.y, data.Pos.z
    );

  //copy from ExplosionCode

  Vector3 pos; CpyCoord(pos, data.Pos);
  //ground clamping for position!
  pos[1] = GLandscape->SurfaceYAboveWater(pos.X(), pos.Z());

  Vector3 dir(0,1,0); //Todo: check 
//  float explosionFactor = 0.8;
//  float energyFactor = 0.8;
//  bool enemyDamage = true;


  EntityAI* owner = GetEntityAI(data.FiringId);
  Object* directHit = dyn_cast<Object>(GetNetObjFromHLAId(data.TargetId));
  //Shot* shot = dyn_cast<Shot>(GetNetObjFromHLAId(data.MunitionId));

  Ref<EntityType> type = VehicleTypes.New(data.MunitionType);
  AmmoType *aType = dynamic_cast<AmmoType *>(type.GetRef());
  if (!aType)
  {
    ALogF("  [HLA_AAR] Error: AmmoType %s not found!", data.MunitionType);
    return;
  }
  AmmoHitInfo hitInfo(aType);


  // okay got all the info to recreate the shot
  // can be null actualy, but it gets deleted right after inilization.
  Ref<Shot> shot = NewShot(owner,aType,directHit);

  
  hitInfo.external = ctrl;
 GLandscape->ExplosionDamage(owner, shot, directHit, pos, dir, -1, hitInfo);
//                              energyFactor, explosionFactor, &resultSpeed);

//  GLandscape->ExplosionDamageEffects(owner,directHit,pos,dir,hitInfo,enemyDamage,energyFactor,explosionFactor);
//  GetNetworkManager().ExplosionDamageEffects(owner,directHit,pos,dir,hitInfo,enemyDamage,energyFactor,explosionFactor);

}

EntityId HlaInterface::HLACenterCreated(ExtCtrl ctrl, const CenterCreateData data)
{
  if (_debugHLAUpdate & 1)
    ALogF("[from %s] Create Center (%d/%d), Side : %d"
    , ctrl == EC_HLA ? "HLA":"AAR"   
    , data.Id.ApplicationNumber
    , data.Id.EntityNumber
    , data.Side
    );

  EntityId newId;
  CpyEntity(newId, (Object*)NULL);

  TargetSide side = (TargetSide)data.Side;

  AICenter* existingCenter = GWorld->GetCenter(side);
  if (!existingCenter)
  {
    AICenter *center = GWorld->CreateCenter(side);
    if (center)
    {
      CpyEntity(newId, center);
      center->SetExternalController(ctrl);
      GetNetworkManager().CreateObject(center);
    }
  }else
    CpyEntity(newId, existingCenter);

  return newId;
}
EntityId HlaInterface::HLAGroupCreated(ExtCtrl ctrl, const GroupCreateData data)
{
  if (_debugHLAUpdate & 1)
    ALogF("[from %s] Create Group (%d/%d), Center: (%d/%d)"
    "Leader: (%d/%d), Number : %d"
    , ctrl == EC_HLA ? "HLA":"AAR"   
    , data.Id.ApplicationNumber
    , data.Id.EntityNumber
    , data.CenterId.ApplicationNumber
    , data.CenterId.EntityNumber
    , data.LeaderId.ApplicationNumber
    , data.LeaderId.EntityNumber
    , data.Number
    );

  EntityId newId;
  CpyEntity(newId, (Object*)NULL);

  NetworkObject* nwObj = GetNetObjFromHLAId(data.CenterId);
  AICenter *center = dynamic_cast<AICenter*>(nwObj);

  if(center)
  {
    Ref<AIGroup> group = new AIGroup();
    group->SetExternalController(ctrl);

    center->AddGroup(group);

    group->AddFirstWaypoint(VZero);

    Mission mis;
    mis._action = Mission::Arcade;
    center->SendMission(group, mis);
    
/*
    NetworkObject* nwObj = GetNetObjFromHLAId(data.LeaderId);
    AIUnit *unit = dynamic_cast<AIUnit*>(nwObj);

    if (unit)
    {
      int id = -1;
      unit->ForceRemoveFromGroup();
      group->AddUnit(unit, id);
    }
 */
    GetNetworkManager().CreateObject(group);

    CpyEntity(newId, group);

    if (_debugHLAUpdate & 1)
      ALogF("[from %s]   new Group id(%d/%d), GroupId(%d/%d)"
      , ctrl == EC_HLA ? "HLA":"AAR" 
      , newId.ApplicationNumber
      , newId.EntityNumber
      , group->GetNetworkId().creator
      , group->GetNetworkId().id
      );

  }
  else
    ALogF("[AAR] Error in HLAGroupCreated: Invalid Center Id: (%d/%d)", data.CenterId.ApplicationNumber, data.CenterId.EntityNumber);  

  return newId;
}

EntityId HlaInterface::HLAUnitCreated(ExtCtrl ctrl, const UnitCreateData data)
{
  if (_debugHLAUpdate & 1)
    ALogF("[from %s] Create Unit (%d/%d), Person: (%d/%d)"
    "Group: (%d/%d), Number : %d"
    , ctrl == EC_HLA ? "HLA":"AAR"   
    , data.Id.ApplicationNumber
    , data.Id.EntityNumber
    , data.PersonId.ApplicationNumber
    , data.PersonId.EntityNumber
    , data.GroupId.ApplicationNumber
    , data.GroupId.EntityNumber
    , data.Number
    );

  EntityId newId;
  CpyEntity(newId, (Object*)NULL);

  NetworkObject* nwObj = GetNetObjFromHLAId(data.PersonId);
  Person *soldier = dyn_cast<Person>(nwObj);

  if (!soldier)
  {
    ALogF("[AAR] Invalid Person Id: (%d/%d)", data.PersonId.ApplicationNumber, data.PersonId.EntityNumber);  
    return newId;
  }

  AIBrain *agent = new AIUnit(soldier);
  agent->SetExternalController(ctrl);

  soldier->SetBrain(agent);

 // veh->OnEvent(EEInit);

  NetworkObject* nwObj2 = GetNetObjFromHLAId(data.GroupId);
  AIGroup *group = dynamic_cast<AIGroup*>(nwObj2);

  if(!group)
  {
    // Sometimes we cannot provid a group, spawn unit without group..
    GetNetworkManager().CreateObject(agent);
    CpyEntity(newId, agent);

    ALogF("[AAR] Invalid Group Id: (%d/%d)", data.GroupId.ApplicationNumber, data.GroupId.EntityNumber);  
    return newId;
  }

  AICenter *center = group->GetCenter();
  Assert(center);

  // soldier is free soldier - we should add sensor
//  GWorld->AddSensor(soldier);

/*  IdentityInfo info;
  center->NextSoldierIdentity(info, soldier);
  agent->Load(info);

  AIUnitInfo &aiInfo = soldier->GetInfo();
  aiInfo._rank = rank;
  aiInfo._initExperience = aiInfo._experience = AI::ExpForRank(rank);
  agent->SetRawAbility(skill);
*/
  AIUnit *leader = group->Leader(); 
  AIUnit *unit = agent->GetUnit();
  {
    // Only AIUnit implementation can be inserted to static AI structures
    // Add into AIGroup
    AISubgroup *subgroup = group->MainSubgroup();
    group->AddUnit(unit);
    if (!subgroup)
    {
      Assert(group->MainSubgroup());
      GetNetworkManager().CreateObject(group->MainSubgroup());
      // update group as well to let know about main subgroup
      GetNetworkManager().UpdateObject(group);
    }
    if (!group->Leader()) center->SelectLeader(group);
  }
  GetNetworkManager().CreateObject(agent);
  // update group as well to let know leader changed
  // from here on we have a valid Network ID!

  CpyEntity(newId, agent);

  if (group->Leader() != leader)
  {
    GetNetworkManager().UpdateObject(group);
  }
  
  return newId;
}

//from plugins.hpp
extern int ExecuteCommandInt(const char *command, char *result, int resultLength);

void HlaInterface::RegisterHLACallBacks()
{

#if _HLA
  _hlaAPI->setCreateEntity(&onHLAEntityCreated);
  _hlaAPI->setUpdateEntity(&onHLAEntityUpdated);
  _hlaAPI->setDeleteEntity(&onHLAEntityDeleted);
  _hlaAPI->setFireWeapon(&onHLAWeaponFired);
  _hlaAPI->setDetonateMunition(&onHLADetonateMunition);
  _hlaAPI->setExecuteCommand(&ExecuteCommandInt);
#endif
}

void HlaInterface::RegisterAARCallBacks()
{
  GAAR.setCreateEntity(&onAAREntityCreated);
  GAAR.setUpdateEntity(&onAAREntityUpdated);
  GAAR.setDeleteEntity(&onAAREntityDeleted);
  GAAR.setDeleteGroup(&onAARGroupDeleted);
  GAAR.setFireWeapon(&onAARWeaponFired);
  GAAR.setDetonateMunition(&onAARDetonateMunition);
  GAAR.setCreateCenter(&onAARCenterCreated);
  GAAR.setCreateGroup(&onAARGroupCreated);
  GAAR.setCreateUnit(&onAARUnitCreated);
  GAAR.setDoDamage(&onAARDoDamage);
  GAAR.setVehicleDestroyed(&onAARVehicleDestroyed);
  GAAR.setUpdateUnit(&onAARUnitUpdated);
  GAAR.setUpdateGroup(&onAARGroupUpdated);
  GAAR.setAttachToUpdate(&onAARAttachToUpdated);
  GAAR.setTurretUpdate(&onAARTurretUpdate);
}

bool HlaInterface::LoadLVCLib()
{
  char oldDirectory[1024];
  GetCurrentDirectory(1024, oldDirectory);
  RString curDir(oldDirectory);
  curDir = curDir + "\\lib";
  SetCurrentDirectory(curDir.Data());

  HRESULT loadErr = __HrLoadAllImportsForDll("lvcGame.dll");
  if (FAILED(loadErr))
  {
    WarningMessage("Cannot load LVCGame.dll.\n");
    return false;
  }
  _hlaAPI = new LVCGame_API();

  SetCurrentDirectory(oldDirectory);
  return true;
}

bool HlaInterface::Connect(NetworkClient* client)
{
  _client = client;
#if _HLA
  if(Glob.vbsEnableLVC || Glob.vbsEnableCNR)
  {
    if(!_HLALibLoaded)
    {
      _HLALibLoaded = LoadLVCLib();
      if(!_HLALibLoaded) return false;
    }

    if(!IsHLAActive()
  # if _AAR
      && !GAAR.IsReplayMode()
  # endif
      )
    { 
      _HLAInit      = false;
      _HLADummyInit = false;
      ALogF("[HLA] Connect");
      bool result = _hlaAPI->init();
      if(!result)
        ALogF("[HLA] Init failed!");
      //no need to initialize callbacks if only CNR is required
      if(_hlaAPI->isInitialised() && Glob.vbsEnableLVC) 
      {
        RegisterHLACallBacks();
        _HLAInit = true;
      }
      else
        ALogF("[HLA] not initialised!");

      if(CNRInitRemoteLogger()== CNRSUCCESS) _isCNRInitSession = true;
    }
  }
#endif
#if _AAR
  ALogF("[GAAR] Connect");
  if(!GAAR.init()) ALogF("[GAAR] Connection failed");
  else RegisterAARCallBacks();
#endif
  return true;
}

void HlaInterface::Disconnect()
{
#if _HLA
  if(IsHLAActive())
  {
    // Cannot do anything here, try to release
    if(_isCNRInitSession)
    {
      _isCNRInitSession = false;
      _hlaAPI->CNRLogReleaseRemoteLogger();
    }

    ALogF("[CNRLOG] hla Disconnect");   
    _hlaAPI->shutdown();
    Time start = Glob.time;
    while(!_hlaAPI->isStopped() && Glob.time - start < 2) 
      _hlaAPI->tick(1);

    if(!_hlaAPI->isStopped())
      ALogF("[CNRLOG] Error, couldn't disconnect HLA!");
  }
  _HLAInit      = false;
  _HLADummyInit = false;
#endif
#if _AAR
  GAAR.shutdown();
#endif
}

void HlaInterface::OnSimulate()
{
  PROFILE_SCOPE_EX(HLA_SIM,*);
#if _HLA
  if(SendToHLA(EC_NONE))
  {
     DWORD start = GetTickCount();
     int stack = 1;
     while(stack > 0 && GetTickCount() - start < 10)
     {
       stack = _hlaAPI->tick(1);  //processes HLA events
     }
     if (_debugHLAUpdate & 4)
       if(stack)
         ALogF("[HLA] Stack: %d", stack);
  }
#endif
#if _AAR
  GAAR.tick();
#endif
#if _ENABLE_CHEATS
  if(GInput.GetCheat2ToDo(DIK_EQUALS)) //toggle debugHLA
  {
    _debugHLAUpdate = (_debugHLAUpdate + 1) %5;
    GlobalShowMessage(1000, "Debug HLA : %d", _debugHLAUpdate);
  }
#endif
}

/// Functor to get the index of a turret
class GetTurretIndex : public ITurretFunc
{
protected:
  const Person* _pers;
public:
  int index;
  GetTurretIndex(const Person* person)
  {
    index = 0;
    _pers = person;
  }

  bool operator () (EntityAIFull *entity, TurretContext &context)
  {
    if (!context._turret) return false; // continue
    if(context._turret->Gunner() == _pers)
      return true;
    ++index;
    return false; // continue
  }
};

void HlaInterface::FillCrewInfo(const Person* person, EntityId &parent, int &crewIndex, EPosInfo &ePos)
{
//  Object* par = person->GetHierachyParent();

  Transport* trans = NULL;
  if(person->Brain())
    trans = person->Brain()->GetVehicleIn();

  CpyEntity(parent, trans);
  crewIndex = 0;
  
  if(trans)
  {
    if(trans->Driver() == person)
      ePos = Pos_Driver;
    else
    {
      TurretContext turret;
      if(trans->FindTurret(person, turret))
      {
        ePos = Pos_Turret;
        GetTurretIndex IndexTurret(person);
        trans->ForEachTurret(IndexTurret);
        crewIndex = IndexTurret.index;
      }
      else 
      {//must be cargo
        ePos = Pos_Cargo;

        const ManCargo &cargo = trans->GetManCargo();
        for (int j=cargo.Size() - 1; j>=0; j--)
          if(cargo[j] == person)
          {
            crewIndex = j;
            break;
          }
      }
    }
  }
}

void HlaInterface::CreateVehicle(const NetworkId& id, const Vehicle *veh, const RString& name, int vehId)
{
  if (!veh) return;

  if(!ExternalActive()) return;

  // We do not consider ShotShell to be network objects.
  if(dyn_cast<const ShotShell>(veh)) return;

  ExtCtrl controller = ExtCtrl(veh->GetExternalController());

  EntityCreateData data;
  CpyEntity(data.VehicleId, (Object*)NULL);
  CpyEntity(data.Id, veh);

  CpyCoord(data.Pos, veh->WorldPosition(veh->FutureVisualState()));
  
  data.EPos = Pos_None;

  data.Side = UnitSide(veh->GetTargetSide());

  data.Name[0] = 0;
  if(name.GetLength())
    strcpy(data.Name, cc_cast(name));

  data.Profile[0] = 0;

  Person *person = dyn_cast<Person>(const_cast<Vehicle*>(veh));
  if(person)
  {
    RString name = person->GetInfo()._name;
    AIBrain *brain = person->Brain();
    if(brain && brain->IsPlayer())
      name = name + RString(" (P)");

    strncpy(data.Profile,name.Data(),sizeof(data.Profile));
    data.Profile[sizeof(data.Profile)-1] = 0;
  }

  const Man* man = dyn_cast<const Man>(veh);
  if(man)
  {
    FillCrewInfo(man, data.VehicleId, data.CrewIndex, data.EPos);
  }

  data.Dimensions.x = 1;
  data.Dimensions.y = 1;
  data.Dimensions.z = 1;

  RString type = "";
  RString classType = "";
  const EntityType *etype = veh->Type();
  if(etype)
  {
    classType = etype->GetName();
    strcpy(data.EntityType, (const char*)classType);

    if (_debugHLAUpdate & 2)
      ALogF("[HLA_AAR] Entity created Type:%s Name:%s Profile:%s (%d/%d) inVeh: (%d/%d) (vehID: %d) Side: %d"
      , classType.Data()
      , data.Name
      , data.Profile
      , data.Id.ApplicationNumber
      , data.Id.EntityNumber
      , data.VehicleId.ApplicationNumber
      , data.VehicleId.EntityNumber
      , vehId
      , data.Side);

    LODShapeWithShadow * shape = etype->GetShape();
    if(shape)
    {
      Vector3 bounding = shape->Max() - shape->Min();
      CpyCoord(data.Dimensions, bounding);
    }
#if _HLA
      if(SendToHLA(controller))
      {
        if(!_hlaAPI->entityCreated(data))
          ALogF("[HLA] Entity creation of class: %s failed!", data.EntityType);
      if (_debugHLAUpdate & 2)
        ALogF("[to HLA] Sent..");
      }
#endif
      if(controller != EC_AAR)
      {
        GAAR.entityCreated(data);
        if (_debugHLAUpdate & 2)
          ALogF("[to AAR] Sent..");
      }
  }else
    ALogF("[HLA_AAR] Error: Non etype!");
}

void HlaInterface::CenterCreated(const AICenter& center)
{
#if _AAR
  CenterCreateData data;
  CpyEntity(data.Id, &center);
  data.Side = (UnitSide)center.GetSide();

  GAAR.CenterCreated(data);
#endif
}

void HlaInterface::GroupCreated(const AIGroup& group)
{
#if _AAR
  GroupCreateData data;
  CpyEntity(data.Id, &group);
  CpyEntity(data.CenterId, group.GetCenter());
  data.Number = group.ID();
  CpyEntity(data.LeaderId, group.Leader());

  GAAR.GroupCreated(data);
#endif
}

void HlaInterface::UnitCreated(const AIUnit& unit)
{
#if _AAR
  UnitCreateData data;
  CpyEntity(data.Id, &unit);
  CpyEntity(data.GroupId, unit.GetGroup());
  CpyEntity(data.PersonId, unit.GetPerson());
  data.Number = unit.ID();
  GAAR.UnitCreated(data);
#endif
}

void HlaInterface::CreateObject(const NetworkObject* nwObj)
{
  if(!ExternalActive())
    return;

  const Vehicle *veh = dyn_cast<const Vehicle>(nwObj);
  if(veh)
  {
    CreateVehicle(nwObj->GetNetworkId(), veh, veh->GetVarName(), 0);
    if(dyn_cast<const Shot>(veh))
      UpdateEntity(const_cast<Vehicle*>(veh)); 
    return;
  }

  const AICenter* center = dynamic_cast<const AICenter*>(nwObj);
  if(center)
  {
    CenterCreated(*center);
    ALogF("[HLA_AAR] Center created(%d/%d): side: %d"
      , center->GetNetworkId().creator
      , center->GetNetworkId().id
      , center->GetSide()
      );
    return;
  }
  const AIGroup* group  = dynamic_cast<const AIGroup*>(nwObj);
  if(group){
    GroupCreated(*group);
    ALogF("[HLA_AAR] Group created(%d/%d): Center: (%d/%d), Id:%d, Leader: (%d/%d)"
      , group->GetNetworkId().creator
      , group->GetNetworkId().id
      , group->GetCenter()->GetNetworkId().creator
      , group->GetCenter()->GetNetworkId().id
      , group->ID()
      , group->Leader() ? group->Leader()->GetNetworkId().creator : 0
      , group->Leader() ? group->Leader()->GetNetworkId().id : 0
     );
    return;
  }
  const AISubgroup* subgrp  = dynamic_cast<const AISubgroup*>(nwObj);
  if(subgrp){
    ALogF("[HLA_AAR] Subgroup created(%d/%d): Superior group:(%d/%d)"
      , subgrp->GetNetworkId().creator
      , subgrp->GetNetworkId().id
      , subgrp->GetGroup()->GetNetworkId().creator
      , subgrp->GetGroup()->GetNetworkId().id
      );
    return;
  }
  const AIUnit* unit  = dynamic_cast<const AIUnit*>(nwObj);
  if(unit){
    ALogF("[HLA_AAR] Group:%s",(const char*)unit->GetDebugName());
    ALogF("[HLA_AAR] AIUnit created(%d/%d): person:(%d/%d), group:(%d/%d), Subgroup:(%d/%d), ID:%d, "
      , unit->GetNetworkId().creator
      , unit->GetNetworkId().id
      , unit->GetPerson()->GetNetworkId().creator
      , unit->GetPerson()->GetNetworkId().id
      , unit->GetGroup()->GetNetworkId().creator
      , unit->GetGroup()->GetNetworkId().id
      , unit->GetSubgroup()->GetNetworkId().creator
      , unit->GetSubgroup()->GetNetworkId().id
      , unit->ID()
      );
    UnitCreated(*unit);

    return;
  }

//  AISubgroup
//  bool NetworkClient::UpdateCenter(AICenter *center)
}

/*
 We dont covert the network ID, to a NetworkObject or any type. This
 is because deleteObject can be called when the object no longer exists.
*/
void HlaInterface::DeleteObject(NetworkId &id)
{
//  ALogF("[HLA] sending Delete Object: (%d/%d)",id.creator,id.id );

  if(!SendToHLA(EC_NONE) && !GAAR.IsRecording())
  {
//    ALogF("Delete command recieved. HLA is not active, and AAR is not recording");
    return;
  }

  EntityId Ent = {id.creator, id.id };

#if _HLA 
  if(SendToHLA(EC_NONE))
  {
    ALogF("[to HLA] Sent..");
   _hlaAPI->entityDeleted(Ent);
  }
#endif

#if _AAR
  if(GAAR.IsRecording())
  {
    ALogF("[to AAR] Sent..");
    GAAR.entityDeleted(Ent);
  }
#endif
}

UnitPosition HlaInterface::GetUpDegree(const StanceType stance)
{
  if(stance >= Stance_Prone && stance <= Stance_Crawling)
    return UPDown; 
  if(stance >= Stance_Kneeling && stance <= Stance_Crouching)
    return UPMiddle; 
  if(stance >= Stance_StandingStill && stance <= Stance_StandingRun)
    return UPUp; 
  
  return NUnitPositions;
}

/// returns true if the Stance has to be changed and modifies selAction
bool HlaInterface::ChangeStance(const StanceType& newStance,const WeaponPostureType& newPosture, ManAction& selAction, const Man* man)
{
  StanceType oldStance = GetStance(man);
  WeaponPostureType oldPosture = GetWeaponPosture(man);
  if(oldStance == newStance && oldPosture == newPosture)
    return false; // we don't have to change the Position

  UnitPosition oldPos = GetUpDegree(oldStance);
  UnitPosition newPos = GetUpDegree(newStance);

  if(newPos != NUnitPositions && oldPos == newPos && oldPosture == newPosture)
    return false; // we don't have to change the Position

  if(newPos == NUnitPositions)//special 
  {
      switch(newStance)
      {
        case Stance_Swimming:
          selAction = ManActStartSwim;    
          break;
      }
  }
  else
    switch(newPos)
    {
      case UPUp:
          if(newPosture == WeaponPosture_None || newPosture == WeaponPosture_Stowed)
            selAction = ManActCivil;
          else 
            if(newPosture == WeaponPosture_Deployed)
              selAction = ManActStand;
            else 
              selAction = ManActCombat;
        break;
      case UPMiddle:
        selAction = ManActCrouch;
        break;
      case UPDown:
        selAction = (newPosture == WeaponPosture_None || newPosture == WeaponPosture_Stowed) 
                    ? ManActCivilLying : ManActLying;
        break;
    }

  return true;
}
/// Get the required action based on the stance and speed
ManAction HlaInterface::GetAction(const Man* man, StanceType stance, WeaponPostureType posture)
{
  if(!man)
    return ManActStand;

  ActionMap *map = man->Type()->GetActionMap(man->GetCurrentMoveId());
  if(!map)
    return ManActStand;

  ManAction selAction = ManActStand;

  if(!ChangeStance(stance, posture, selAction, man)) //detect if we need to update our AnimationMap (modify selAction)
  {

    float limitFast = map ? map->GetLimitFast() : 5;
    float limitSlow = map ? map->GetLimitFast()*0.6f : 0.2;

    float speedVal = man->Speed().SizeXZ();

    int fastFlag = 0;
    int fbFlag = 0;
    int rlFlag = 0;

    if( speedVal > limitFast ) fastFlag = 2;
    else if( speedVal > limitSlow ) fastFlag = 1;
    
    float fbSpeed = man->ModelSpeed().Z();
    float lrSpeed = man->ModelSpeed().X();


    if(fabs(fbSpeed) > 0.2) fbFlag = fbSpeed / fabs(fbSpeed);
    if(fabs(lrSpeed) > 0.2) rlFlag = lrSpeed / fabs(lrSpeed);

    static const ManAction selectAction[3][3][3]=
    {
      { // walk action
        {ManActWalkLB,ManActWalkB,ManActWalkRB},// back L|R 
        {ManActWalkL,ManActStop,ManActWalkR},// L|R 
        {ManActWalkLF,ManActWalkF,ManActWalkRF},// front L|R 
      },
      { // slow action
        {ManActSlowLB,ManActSlowB,ManActSlowRB},// back L|R 
        {ManActSlowL,ManActStop,ManActSlowR},// L|R 
        {ManActSlowLF,ManActSlowF,ManActSlowRF},// front L|R 
        },
        { // fast action
          {ManActFastLB,ManActFastB,ManActFastRB},// back L|R 
          {ManActFastL,ManActStop,ManActFastR},// L|R 
          {ManActFastLF,ManActFastF,ManActFastRF},// front L|R 
        },
    };
    // get action ID

    selAction = ENUM_CAST(ManAction,selectAction[fastFlag][fbFlag+1][rlFlag+1]);
#if 0
    if(_debugHLAUpdate & 4)
      ALogF("[HLA] selaction: %d, fastflag: %d, fbFlag: %d, rlFlag: %d"
      ", speedVal: %.1f, MSpdX :%.1f, MSpdZ :%.1f"
          , selAction, fastFlag, fbFlag, rlFlag
          , speedVal, man->ModelSpeed().Z(), man->ModelSpeed().Z());
#endif
  }  
  return selAction;
}

StanceType HlaInterface::GetStance(const Man* man)
{
  if(!man)
    return Stance_StandingStill;
  
  ActionMap *map = man->Type()->GetActionMap(man->GetCurrentMoveId());
  if(!map)
    return Stance_StandingStill;

  static StanceType ProneStances[3] = {Stance_Prone, Stance_Crawling, Stance_Crawling};
  static StanceType KneelStances[3] = {Stance_Kneeling, Stance_Crouching, Stance_Crouching};
  static StanceType StandStances[3] = {Stance_StandingStill, Stance_StandingWalk, Stance_StandingRun};

  float limitFast = map ? map->GetLimitFast() : 5;
  float limitSlow = 0.2;

  int SpeedIndex = 0;
  float speed = man->Speed().Size();
  if(speed >= limitFast) SpeedIndex = 2;
  else if(speed >= limitSlow) SpeedIndex = 1;

  switch(map->GetUpDegree())
  {
    case ManPosDead:
      return Stance_Prone;
      break;
    case ManPosBinocLying:
    case ManPosLyingNoWeapon:
    case ManPosLying:
    case ManPosHandGunLying:
        return ProneStances[SpeedIndex];
      break;
    case ManPosWeapon:// special weapon - AT
    case ManPosCrouch:
    case ManPosHandGunCrouch:
        return KneelStances[SpeedIndex];
      break;
    case ManPosCombat:
    case ManPosHandGunStand:
    case ManPosStand: // moves with weapon on the back
    case ManPosNoWeapon:
    case ManPosBinocStand:
    case ManPosBinoc:
        return StandStances[SpeedIndex]; 
      break;
    case ManPosSwimming:
        return Stance_Swimming;
      break;
  }

  return Stance_None;
}

WeaponPostureType HlaInterface::GetWeaponPosture(const Man* man)
{
  if(!man)
    return WeaponPosture_None;

  ActionMap *map = man->Type()->GetActionMap(man->GetCurrentMoveId());

  switch(map->GetUpDegree())
  {
  case ManPosDead:
  case ManPosLyingNoWeapon:
  case ManPosNoWeapon:
    return WeaponPosture_None;

  case ManPosBinocLying:
  case ManPosSwimming:
  case ManPosBinoc:
  case ManPosBinocStand:
     return WeaponPosture_Stowed;

  case ManPosLying:
  case ManPosHandGunLying:
  case ManPosWeapon:// special weapon - AT
  case ManPosCrouch:
  case ManPosHandGunCrouch:
  case ManPosCombat:
  case ManPosHandGunStand:
    return WeaponPosture_Raised;

  case ManPosStand: // moves with weapon on the back
    return WeaponPosture_Deployed;
  }

  return WeaponPosture_None;
}
/// send an object update out to HLA
void HlaInterface::UpdateObject(NetworkObject *nwObj)
{
  if( !SendToHLA((ExtCtrl)nwObj->GetExternalController())
      && (!GAAR.IsRecording() || ExtCtrl(nwObj->GetExternalController()) == EC_AAR)
    )
    return;

  Entity* veh = GetEntity(nwObj);

  /*
  EntityAI* vehAI = dyn_cast<EntityAI>(veh);
  Shot* shot = dyn_cast<Shot>(veh);
  if(vehAI || shot) //also update shots
    UpdateEntity(veh);
  */

/*
  Turret *turret = dyn_cast<Turret>(nwObj); //dyncast not working yet!!!
  if(turret)
  {
    // Code here for turret....
    //return;
  }
*/
  if(veh)
  {
    // can be also a turret update too!
   UpdateEntity(veh);
   OnTurretUpdate(veh);
  }


  AIGroup* group = dynamic_cast<AIGroup*>(nwObj);
  if(group)
  {
    UpdateGroup(group);
    return;
  }

  AIUnit* unit = dynamic_cast<AIUnit*>(nwObj);
  if(unit)
  {
    UpdateUnit(unit);
    return;
  }

  //also update center, subgroups ?
}

void HlaInterface::UpdateUnit(AIUnit* unit)
{
  if(!GAAR.IsRecording())
    return;

  UnitUpdateData data;
  CpyEntity(data.Id, unit);
  CpyEntity(data.GroupId, unit->GetGroup());
  CpyEntity(data.PersonId, unit->GetPerson());
  data.Number = unit->ID();

  GAAR.UpdateUnit(data);
}


void HlaInterface::UpdateGroup(AIGroup* group)
{
  if(!GAAR.IsRecording())
  return;
  
  UpdateGroupData data;

  CpyEntity(data.Id, group);
  CpyEntity(data.CenterId, group->GetCenter());
  CpyEntity(data.LeaderId, group->Leader());
  data.Number = group->ID();

  GAAR.UpdateGroup(data);
}


void HlaInterface::UpdateEntity(Entity* veh)
{
  if(!ExternalActive())
    return;

  // check to see if we're dealing with a laser.
  LaserTarget *laser = dyn_cast<LaserTarget>(veh);
  if(laser)
  {
    DesignatorCreateData createLaser;
    CpyEntity(createLaser.Id, laser);
    CpyEntity(createLaser.DesignatingId, laser->GetOwner() );
    CpyEntity(createLaser.DesignatedId, laser->GetDesinatedTarget() );

    createLaser.DesignatorNameCode     = 0;
    createLaser.DesignatorFunctionCode = LD_FUNCTION_CODE_MARKING;     //hardcoded function code for marking
    createLaser.DesignatorPower        = 0.0f;
    createLaser.DesignatorWavelength   = 0.0f;

    Vector3 relativePos = VZero;
    if(laser->GetDesinatedTarget())
    {
      Entity *ent = dyn_cast<Entity>(laser->GetDesinatedTarget());
      if(ent)
        ent->PositionWorldToModel(relativePos,laser->Position());
    }

    CpyCoord(createLaser.RelativePosition  ,relativePos);
    CpyCoord(createLaser.WorldPosition     ,laser->Position());
    CpyCoord(createLaser.LinearAcceleration,VZeroP);

    if (_debugHLAUpdate & 2) 
      ALogF("[to HLA] Sending Laserdesignator message...");

#if _HLA
    if(SendToHLA(EC_NONE))
    {
      _hlaAPI->designatorUpdated(createLaser);
      if (_debugHLAUpdate & 2) ALogF("[to HLA] sent.");
    }
#endif
  }

  EntityUpdateData data;
  ZeroMemory(&data,sizeof(data));
  
  data.VehicleId = NullEntityId;

  CpyEntity(data.Id, veh);

  CpyCoord(data.Pos,    veh->Position());
  CpyCoord(data.Dir,    veh->Direction());
  CpyCoord(data.Up,     veh->DirectionUp());
  CpyCoord(data.Velocity,  veh->Speed());

  // we record the attached position/orientation only for static objects
  // we do not do this for normal swing load objects
  if(veh->IsAttached() && veh->GetAttachFlags() == 0)
  {
    // Sometimes on the server we have tracking position from 
    // localy owned entitys
    CpyCoord(data.Pos, veh->GetAttachedOffset()
#if _VBS_TRACKING
      + veh->GetTrackingPos()
#endif
      );
    CpyCoord(data.Dir, veh->GetAttachMatrix().Direction());
    CpyCoord(data.Up,  veh->GetAttachMatrix().DirectionUp());
  }

  data.Damage = veh->GetTotalDamage();
  if(veh->IsDamageDestroyed())
    data.Appearance |= LVC_DESTROYED;

  data.HasTurret = false;
  data.EPos = Pos_None;
  data.Azimuth = 0.0f;
  data.AzimuthWanted = 0.0f;
  data.Elevation = 0.0f;
  data.ElevationWanted = 0.0f;
  data.SelectedWeapon = 0; 

  data.URN[0] = 0;

  GameVarSpace *vars = veh->GetVars();
  if (vars)
  {
    GameValue var;
    if (vars->VarGet("URN", var))
    {
      RString tmpStr = var.GetData()->GetString();
      int length = tmpStr.GetLength();
      if(length > 0)
      {
        if(length > 11)
          tmpStr = tmpStr.Substring(0, 10);
        strcpy(data.URN, cc_cast(tmpStr));
      }
    }
  }
  
  EntityAI* vehAI = dyn_cast<EntityAI>(veh);
  if(vehAI)
  {
    if(vehAI->IsPilotLight())
      data.Lights |= LVC_LIGHT_HEAD;

    if(vehAI->EngineIsOn())
      data.Appearance |= LVC_POWER_PLANT_ON;

    if(!vehAI->CanFire())
      data.Appearance |= LVC_FIREPOWER_DISABLED;

    if(!vehAI->IsAbleToMove())
      data.Appearance |= LVC_MOBILITY_DISABLED;

    if(vehAI->GetSmoke() != NULL)
    {
      data.Appearance |= LVC_SMOKE_PLUME | LVC_FLAMES_PRESENT;
    }
    
    EntityAIFull* vehAIFull = dyn_cast<EntityAIFull>(vehAI);
    if(vehAIFull)
    {

      TurretContextV context;
      if (vehAIFull->GetPrimaryGunnerTurret(context))
      {
        data.HasTurret = (context._turret != NULL);
        if(context._turret)
        {
          data.Elevation = context._turretVisualState->_xRot;
          data.ElevationWanted = context._turret->_xRotWanted;
          data.Azimuth = context._turretVisualState->_yRot;
          data.AzimuthWanted = context._turret->_yRotWanted;
        }

        int weapon = context._weapons->_currentWeapon;     
        data.SelectedWeapon = weapon;
      }
      const Man* man = dyn_cast<const Man>(vehAIFull); //duplicate check createVehicle!
      if(man)
      {
        AutoArray<MagazineSlot> &mRef = context._weapons->_magazineSlots;
        int weapon = data.SelectedWeapon;

        //! NEEDED conversion from HLA gateway to VBS2 Weapon type.
        


        // Note:
        //  Both of the following values are correct and valid.
        //  we handel them during playback.
        //  type  0: is a dummy type used for gernades
        //  type -1: Is saying no weapon currently selected
        if(weapon >= 0 && weapon < mRef.Size())
          weapon = mRef[weapon]._weapon->_weaponType;

        data.Stance = GetStance(man);
        data.PrWeaponPosture = GetWeaponPosture(man);

        data.EPos = Pos_None;
        
        FillCrewInfo(man, data.VehicleId, data.CrewIndex, data.EPos);

        data.Elevation = man->_gunXRot;
        data.ElevationWanted = man->_gunXRotWanted;
        data.Azimuth = man->_gunYRot;
        data.AzimuthWanted = man->_gunYRotWanted;
        data.SelectedWeapon = weapon;
      }
    }
  } //vehAI
#if _HLA
  if(SendToHLA((ExtCtrl)veh->GetExternalController()))
    _hlaAPI->entityUpdated(data);
#endif
#if _AAR
  if((ExtCtrl)veh->GetExternalController() != EC_AAR)
    GAAR.entityUpdated(data);
#endif
 /* Missing:

  bool FlamesPresent;                 
  WeaponPostureType PrWeaponPosture;  //Posture for primary weapon as per enums
  */

    LogF("[to HLA] Update (%d/%d) PP:%d Pos: [%.3f, %.3f, %.3f], Speed: [%.3f, %.3f, %.3f], Elev: %.2f, Azi: %.2f, Stance: %d, WeaponPost: %d, inVeh (%d/%d)"
      , data.Id.ApplicationNumber
      , data.Id.EntityNumber
      , data.Appearance & LVC_POWER_PLANT_ON
      , data.Pos.x, data.Pos.y, data.Pos.z
      , data.Velocity.x,  data.Velocity.y,  data.Velocity.z
      , data.Elevation, data.Azimuth
      , data.Stance
      , data.PrWeaponPosture
      , data.VehicleId.ApplicationNumber
      , data.VehicleId.EntityNumber
      );
}


void HlaInterface::VehicleDestroyed(EntityAI *killed, EntityAI *killer)
{
  // We do not listen to vehicledestroyed 
  // messages from hla. We have no way of knowing
  // who killed who..
  if(!GAAR.IsRecording()
    || ( killed && killed->GetExternalController() == EC_HLA ) 
    || ( killer && killer->GetExternalController() == EC_HLA )
    ) return;

  EntityDestroyed data;
  data.Killed = NullEntityId;
  data.Killer = NullEntityId;

  CpyEntity(data.Killed,killed);
  CpyEntity(data.Killer,killer);

  if(!killer || !killed)
    return;

#if _AAR
  if((ExtCtrl)killer->GetExternalController() != EC_AAR)
    GAAR.VehicleDestroyed(data);
#endif
}

/*
\VBS_patch 1.00 Date [2/18/2008] by clion
- Fixed: Slaved object detach command now is recorded in AAR
*/
void HlaInterface::AttachToMsg(Object *obj, Object *attachTo, int memIndex, Vector3 pos, int flags)
{
  if(!GAAR.IsRecording()) return;

  AttachToData data;
  ZeroMemory(&data,sizeof(data));

  CpyEntity(data.obj,obj);

  // Some objects are slaved localy, with no network ID.
  // we ignore these.
  if(IsNullEntityId(data.obj)) return;

  CpyEntity(data.attachTo,attachTo);
  CpyCoord(data.com,pos);
  data.flags = flags;
  data.memoryIndex = memIndex;
  

  // has to be an object
  if(!obj) return;

#if _AAR
  if((ExtCtrl)obj->GetExternalController() != EC_AAR)
    GAAR.AttachToUpdate(data);
#endif
}

void HlaInterface::DeleteMarkerMsg(const RString &name)
{
  if(!GAAR.IsRecording()) return;

#if _AAR
  GAAR.DeleteMarker(name);
#endif  
}

void HlaInterface::CreateMarkerMsg(ArcadeMarkerInfo *marker)
{
  Assert(marker);
  if(!GAAR.IsRecording()) return;

  /*
   If, we've sent a create marker to AAR, we just need to tell it
   to delete the marker because Real time editor will handle recreation 
   of markers
  */
#if _AAR
  if(marker->attachCondition.GetLength()>0)
    GAAR.DeleteMarker(marker->name);
  else
    GAAR.CreateMarker(marker);
#endif
}


// HLA entity is damaged? use detonation event
//   vbs fires and issues munition creation
//   hits another vbs entity which trigger munition detonation
//   then update on vbs damage state comes through as a entity update
void HlaInterface::VehicleDamaged(EntityAI *damaged, EntityAI *killer, float damage, RString ammo)
{
  if(!damaged) return;

  if(!GAAR.IsRecording()
    || ( damaged && damaged->GetExternalController() == EC_HLA ) 
    || ( killer && killer->GetExternalController() == EC_HLA )
    ) return;
  
  // old call to dupllicate old behaviour
  UpdateEntity(damaged);

  DoDamageData data;
  data.Who    = NullEntityId;
  data.Killer = NullEntityId;
  data.DamageAmount = 0.0f;

  CpyEntity(data.Who, damaged);
  CpyEntity(data.Killer, killer);
  data.DamageAmount = damage;

  // copy the ammo
  if(ammo.GetLength())
    strcpy(data.Ammo, cc_cast(ammo));

#if _AAR
  if((ExtCtrl)killer->GetExternalController() != EC_AAR)
    GAAR.DoDamage(data);
#endif
}
//special version for LS
void HlaInterface::FireWeapon(Person *gunner, RString ammoName, Vector3 position, Vector3 velocity, int timeToLive)
{
  if(!GAAR.IsRecording() && !SendToHLA(gunner ? (ExtCtrl)gunner->GetExternalController() : EC_NONE)) return;
  FireWeaponData data;

  CpyEntity(data.FiringId, gunner);
  CpyEntity(data.TargetId, (Object*)NULL);
  CpyEntity(data.MunitionId, (Object*)NULL);
  data.WeaponId = -1;
  strcpy(data.MunitionType, cc_cast(ammoName));

  CpyCoord(data.Velocity, velocity);
  CpyCoord(data.Pos, position);
  data.TimeToLive = timeToLive;

  if (_debugHLAUpdate & 2)
    ALogF("[to HLA] FireWeapon(LS) %s from (%d/%d) Pos[%.1f,%.1f,%.1f] Speed[%.1f,%.1f,%.1f] TimetoL: %.2f "
    ,data.MunitionType
    ,data.FiringId.ApplicationNumber
    ,data.FiringId.EntityNumber
    ,data.Pos.x, data.Pos.y, data.Pos.z
    ,data.Velocity.x, data.Velocity.y, data.Velocity.z
    ,data.TimeToLive
    );

#if _HLA
  if(SendToHLA(gunner ? (ExtCtrl)gunner->GetExternalController() : EC_NONE))
  {
    _hlaAPI->weaponFired(data);
    if (_debugHLAUpdate & 2) ALogF("[to HLA] sent.");
  }
#endif
  if(ExtCtrl(gunner->GetExternalController()) != EC_AAR)
  {
    GAAR.weaponFired(data);
    if (_debugHLAUpdate & 2) ALogF("[to AAR] sent.");
  }
}

void HlaInterface::FireWeapon(EntityAIFull *vehicle, Person *gunner, int weapon, const Magazine *magazine
                              , EntityAI *target, const RemoteFireWeaponInfo &remoteInfo)
{
  if(!GAAR.IsRecording() && !SendToHLA((ExtCtrl)vehicle->GetExternalController())) return;

  // check to see if we're dealing with a laser.
  LaserTarget *laser = dyn_cast<LaserTarget>(remoteInfo._projectile.GetLink());
  if(laser)
  {
    DesignatorCreateData createLaser;
    CpyEntity(createLaser.Id, laser);
    CpyEntity(createLaser.DesignatingId, vehicle);
    CpyEntity(createLaser.DesignatedId, laser->GetDesinatedTarget() );

    createLaser.DesignatorNameCode     = 0;
    createLaser.DesignatorFunctionCode = LD_FUNCTION_CODE_MARKING;     //hardcoded function code for marking
    createLaser.DesignatorPower        = 0.0f;
    createLaser.DesignatorWavelength   = 0.0f;
    
    Vector3 relativePos = VZero;
    if(laser->GetDesinatedTarget())
    {
      Entity *ent = dyn_cast<Entity>(laser->GetDesinatedTarget());
      if(ent)
        ent->PositionWorldToModel(relativePos,laser->Position());
    }

    CpyCoord(createLaser.RelativePosition  ,relativePos);
    CpyCoord(createLaser.WorldPosition     ,laser->Position());
    CpyCoord(createLaser.LinearAcceleration,VZeroP);
     
    if (_debugHLAUpdate & 2) ALogF("[to HLA] Sending Laserdesignator message...");

#if _HLA
    if(SendToHLA((ExtCtrl)vehicle->GetExternalController()))
    {
      _hlaAPI->designatorCreated(createLaser);
      if (_debugHLAUpdate & 2) ALogF("[to HLA] sent.");
    }
#endif

    return;
  }

  FireWeaponData data;
  
  CpyEntity(data.FiringId, vehicle);
  CpyEntity(data.TargetId, target);

  CpyEntity(data.MunitionId, (Object*)NULL); //Todo: get ID of shot? Is that broadcasted?

  float initVelocity = 600;      //set some default, gets overridden by MagazineType
  float timeToLive = 20;      //set some default, gets overridden by ammotype

  data.MunitionType[0] = 0; //set empty string
  data.WeaponId = weapon;

  // shot from vehicle
  Transport *tran = dyn_cast<Transport>(vehicle);
  if(tran)
  {
    GetTurretIndex indexTurret(gunner);
    tran->ForEachTurret(indexTurret);
    
    int turretId = indexTurret.index;
    Assert(turretId!=-1);

    UnionWeapon *uWeapon = (UnionWeapon*) &data.WeaponId;
    uWeapon->vehTurretId = (short) turretId;
  }

  // weapon ID is correct for Tanks/Vehicles, but incorrect for man/people 
  // because of recording and playback mag arrangement being different
  const Man *man = dyn_cast<const Man>(vehicle); //duplicate check createVehicle!
  if(man)
  {
    TurretContext context;
    if (vehicle->GetPrimaryGunnerTurret(context))
    {
      AutoArray<MagazineSlot> &mRef = context._weapons->_magazineSlots;
      //! TODO: Conversion from HLA gateway to VBS2 Weapon type.

      // Note:
      //  Both of the following values are correct and valid.
      //  we handel them during playback.
      //  type  0: is a dummy type used for gernades
      //  type -1: Is saying no weapon currently selected
      if(weapon >= 0 && weapon < mRef.Size())
      {
        weapon = mRef[weapon]._weapon->_weaponType;
        data.WeaponId = weapon;
      }
    }
  }

  const MagazineType *aInfo = magazine ? magazine->_type : NULL;
  if(aInfo)
  {
    initVelocity = aInfo->_initSpeed;
    const AmmoType *ammoType = aInfo->_ammo;
    if(ammoType)
    {
      timeToLive = ammoType->_timeToLive;
      RString ammoName = ammoType->GetName();
      if(ammoName.GetLength())
        strcpy(data.MunitionType, cc_cast(ammoName));
    }
  }
  Vector3 velocity = remoteInfo._direction.Normalized() * initVelocity;
  CpyCoord(data.Velocity, velocity);

  CpyCoord(data.Pos, remoteInfo._position);

  data.TimeToLive = timeToLive;

  if (_debugHLAUpdate & 2)
    ALogF("[to HLA] FireWeapon %s from (%d/%d) Pos[%.1f,%.1f,%.1f]([%.1f,%.1f,%.1f]) Speed[%.1f,%.1f,%.1f](%.2f) Time: %.2f "
    ,data.MunitionType
    ,data.FiringId.ApplicationNumber
    ,data.FiringId.EntityNumber
    ,data.Pos.x, data.Pos.y, data.Pos.z
    ,remoteInfo._position.X(), remoteInfo._position.Y(), remoteInfo._position.Z()
    ,data.Velocity.x, data.Velocity.y, data.Velocity.z
    ,initVelocity
    ,data.TimeToLive
    );
  
#if _HLA
  if(SendToHLA((ExtCtrl)vehicle->GetExternalController()))
  {
    _hlaAPI->weaponFired(data);
    if (_debugHLAUpdate & 2)
      ALogF("[to HLA] sent.");
  }
#endif
  if(ExtCtrl(vehicle->GetExternalController()) != EC_AAR)
  {
    GAAR.weaponFired(data);
    if (_debugHLAUpdate & 2)
      ALogF("[to AAR] sent.");
  }

}

void HlaInterface::ExplosionDamageEffects(EntityAI *owner, Object *directHit, Vector3Par pos, Vector3Par dir
                                          , const AmmoHitInfo &hitInfo, bool enemyDamage
                                          , float energyFactor, float explosionFactor
                                          )
{
  if(!GAAR.IsRecording() && !SendToHLA((ExtCtrl)owner->GetExternalController())) return;

  if(!hitInfo._type->GetName().GetLength())
  {
    ALogF("[to HLA] Error no ammo name!");   
    return;
  }

  DetonationData data;
  strcpy(data.MunitionType, cc_cast(hitInfo._type->GetName()));
  
  CpyEntity(data.FiringId, owner);

  Entity* veh = dyn_cast<Entity>(directHit); //if it's an Object, we don't broadcast it
  CpyEntity(data.TargetId, veh);

  EntityId munitionId = {0,0}; //todo: see if we can get that info
  data.MunitionId = munitionId;

  CpyCoord(data.Pos, pos);
  CpyCoord(data.Velocity, VZero);

//  if (_debugHLAUpdate & 2) 
    ALogF("[to HLA_AAR] Detonate Ammo ""%s"": Owner (%d/%d)"
    ", DirectHit (%d/%d)"
    ", Pos [%.1f, %.1f, %.1f]"
    ", External? %d"
    , data.MunitionType
    , data.FiringId.ApplicationNumber
    , data.FiringId.EntityNumber
    , data.TargetId.ApplicationNumber
    , data.TargetId.EntityNumber
    , data.Pos.x, data.Pos.y, data.Pos.z
    , hitInfo.external
    );

#if _HLA
  if(SendToHLA((ExtCtrl)hitInfo.external))
  {
    _hlaAPI->munitionDetonation(data);
    if (_debugHLAUpdate & 2)
      ALogF("[to HLA] sent.");
  }
#endif
  GAAR.munitionDetonation(data);
  if (_debugHLAUpdate & 2)
    ALogF("[to AAR] sent.");
}

void HlaInterface::ChangeSimulationState(const NetworkServerState state)
{
   SimStateData data;
   switch(state){
      case NSSNone: data = SNone;
                    break;
      case NSSSelectingMission: data = SCreate;
                                break;
      case NSSEditingMission: data = SEdit;
                                break;
      case NSSAssigningRoles: data = SPrepareRole;
                                break;
      case NSSSendingMission: data = STransferMission;
                                break;
      case NSSLoadingGame: data = SLoadIsland;
                                break;
      case NSSBriefing: data = SBriefing;
                                break;
      case NSSPlaying: data = SSimulation;
                                break;
      case NSSDebriefing: data = SDebriefing;
                                break;
      case NSSMissionAborted: data = SNone;
                                break;
      default: data = SNone;
   }
#if _HLA
     if(IsHLAActive())
       _hlaAPI->simulationStateChanged(data);
#endif
#if _AAR
     GAAR.simulationStateChanged(data);
#endif
   if(state == NSSPlaying)
   {
#if _HLA    
     if(SendToHLA(EC_NONE))
     {
       _hlaAPI->enableCallbacks();
       if(Glob.header.utmInfo.zone != -1)
         SetOrigin(Glob.header.utmInfo.easting
                 , Glob.header.utmInfo.northing
                 , Glob.header.utmInfo.zone
                 , Glob.header.utmInfo.north ? 'N' : 'S'
                 );
     }
#endif
#if _AAR
     GAAR.enableCallbacks();
#endif
   }
   else
   {
#if _HLA  
     if(SendToHLA(EC_NONE)) _hlaAPI->disableCallbacks();
#endif
#if _AAR
     GAAR.disableCallbacks();
#endif
   }
}


//Functor for creating all vehicles
class CreateAllEntitys
{
public:
  bool CreateAllEntitys::operator() (Entity *obj) const
  {
    _hla.CreateObject(obj);
    return false;
  };
};

//Functor for network message to be attached to something
class ScanAllAttached
{
public:
  bool ScanAllAttached::operator() (Entity *obj) const
  {
    if(obj && obj->IsAttached())
    {
      Object *attachTo = obj->GetAttachedTo();
      Vector3 pos      = obj->GetAttachedOffset();
      int     memp     = obj->GetAttachedMemPoint();
      int     flag     = obj->GetAttachFlags();

      // Send of command to show this object is attached
      // to something else at the start of the mission
      _hla.AttachToMsg(obj,attachTo,memp,pos,flag);
    }

    return false;
  };
};

// Functions to send all current centers, groups, ...
// used by AAR when started recording
/*
\VBS_patch_internal 1.00 Date [2/22/2008] by clion
- Fixed: Markers now are saved at the start of recording.
*/
void HlaInterface::SendSnapshot()
{
  _AARSnapShotActive = true;

  CreateAllEntitys func;
  GWorld->ForEachVehicle(func);
  GWorld->ForEachOutVehicle(func);

  ScanAllAttached scan;
  GWorld->ForEachVehicle(scan);
  GWorld->ForEachOutVehicle(scan);

  // Add all centers to the current centerlifespan list
  for(int t = TEast; t < NTargetSide; t++)
  {
    if(AICenter *center = GWorld->GetCenter((TargetSide)t))
    {
      CenterCreated(*center);

      for (int g=0; g<center->NGroups(); g++)
      {
        AIGroup *grp = center->GetGroup(g);
        if (!grp) continue;
        GroupCreated(*grp);

        for (int s=0; s<grp->NSubgroups(); s++)
        {
          AISubgroup *subgrp = grp->GetSubgroup(s);
          if (!subgrp) continue;
//TODO:          SubGroupCreated(*subgroup);
          for (int u=0; u<subgrp->NUnits(); u++)
          {
            AIUnit *unit = subgrp->GetUnit(u);
            if (!unit) continue;
            UnitCreated(*unit);
          }
        }
      }
    }
  }

  // We need to make just a neccearly inital 
  // update to everthing on the map. It seems like
  // remote objects dont update there group information
  // at all over the network unless it changes. From 
  // mission start
  for(int t = TEast; t < NTargetSide; t++)
  {
    if(AICenter *center = GWorld->GetCenter((TargetSide)t))
    {
      for (int g=0; g<center->NGroups(); g++)
      {
        AIGroup *grp = center->GetGroup(g);
        if (!grp) continue;

        // groups will change
        UpdateGroup(grp);

        for (int s=0; s<grp->NSubgroups(); s++)
        {
          AISubgroup *subgrp = grp->GetSubgroup(s);
          if (!subgrp) continue;
          //TODO:          SubGroupCreated(*subgroup);
          for (int u=0; u<subgrp->NUnits(); u++)
          {
            AIUnit *unit = subgrp->GetUnit(u);
            if (!unit) continue;

            // AIUnits will change
            UpdateUnit(unit);
          }
        }
      }
    }
  }

  // record all the inital markers on the map
  for (int i=0; i< markersMap.Size(); i++)
  {
    ArcadeMarkerInfo &info = markersMap[i];
    CreateMarkerMsg(&info);
  }

  // Save the current weather state
  ApplyWeatherMessage weather;
  GLandscape->TranferMsg(weather);
  WeatherUpdate(&weather);

  _AARSnapShotActive = false;
}

void HlaInterface::SendLaserDesignatorCreate
(
  Entity* Id,                        // Designator ID
  Entity* DesignatingId,             // ID of the entity controlling the desig
  Entity* DesignatedId,              // ID of the designated entity (or 0,0)
  int DesignatorNameCode,             // 
  int DesignatorFunctionCode,         //
  float DesignatorPower,              // power in watts
  float DesignatorWavelength,         // wavelength in microns
  Vector3& RelativePosition,  // desig pos relative to entity centre
  Vector3& WorldPosition,     // desig pos in the world
  Vector3& LinearAcceleration    // linear accel in m/s^2
)
{
#if _HLA
  if(!SendToHLA(EC_NONE)) return;

  // overwrite the Id with the vehicle the person is in.
  Person *person = dyn_cast<Person>(Id);
  if(person && person->Brain() && person->Brain()->GetVehicleIn())
    Id = person->Brain()->GetVehicleIn();

  DesignatorCreateData data;
  CpyEntity(data.Id, Id);
  CpyEntity(data.DesignatingId, DesignatingId);
  CpyEntity(data.DesignatedId, DesignatedId);
  data.DesignatorNameCode = DesignatorNameCode;
  data.DesignatorFunctionCode =  DesignatorFunctionCode;
  data.DesignatorPower = DesignatorPower;
  data.DesignatorWavelength = DesignatorWavelength;

  CpyCoord(data.RelativePosition,  RelativePosition);
  CpyCoord(data.WorldPosition, WorldPosition);    
  CpyCoord(data.LinearAcceleration, WorldPosition);

  _hlaAPI->designatorCreated(data);
#endif
}
#endif  //_VBS2
