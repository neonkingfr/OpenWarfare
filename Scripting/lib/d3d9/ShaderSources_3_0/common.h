/*!
  aperture change is typically very small
  We want to encode it so that we are able to represent it with low precision (8b or 10b)

  We need to encode:
  - very small value, like exp(+_deltaT*(log2/getUsedToDark))
  - fps 100: exp(0.01f*(log2/getUsedToDark)) ~= 1.0013872
  - fps 1000: exp(0.001f*(log2/getUsedToDark)) ~= 1.0001386

  - quite large value: 1.05
  - log(1.05)~=0.04879
  - ln(1.0001386)~=0.0001386
*/
#define EncodeApertureChange(x) (log(x)*(2.5e3/255)+128.0/255)
#define DecodeApertureChange(encoded) (exp(((encoded)-128.0/255)*(255/2.5e3)))

// Main light enumeration - vertex shader values
#define VML_None     0
#define VML_Sun      1
#define VML_Sky      2
#define VML_SetColor 3


//! Limit of the fixed space (all constants with smaller indices are being fixed)
//! Constants bigger than this limit can be fixed too, but they must be set before every usage
#define NONFIXED_SPACE_BEGINNING  43
#define NFS_0 43
#define NFS_1 44
#define NFS_2 45

//! First register of the free constant space
#define FREESPACE_START_REGISTER  46
#define FREESPACE_START_REGISTER0 46
#define FREESPACE_START_REGISTER1 47
#define FREESPACE_START_REGISTER2 48
#define FREESPACE_START_REGISTER3 49
#define FREESPACE_START_REGISTER4 50

//! First register of the space designed for lights (note it is being shared with freespace)
//! 214 = 46+168 (see maxFreeConstants in EngineDD9::CreateShaderFragments)
//! 200 = 224 - MAX_LIGHTS * 4
#define LIGHTSPACE_START_REGISTER 214
#define LIGHTSPACE_START_REGISTER_PS 200

//! Maximum number of lights to be used in one batch
//! See MAX_LIGHTS
#define MAX_LIGHTS 6

// CALM WATER defines
//! use parallax mapping
#define CALMWATER_PARALLAX
//! don't use "GetCurrRenderTargetAsTexture()"
//#define CALMWATER_SIMPLE_REFRACT
//! don't use shadow variants
//#define CALMWATER_NOSHADOWS
//! show undistort refraction only with blue tint (for debuging)
//#define CALMWATER_REFRACT_DEBUG

// TREE
// faster noise alpha for TreeAdv shader (use _CAN and _NO textures instead of _CA and _NON)
#define NEW_TREE_NOISE

// param arrays sizes
#define CALMWATER_VSPARS_START 172
#define CALMWATER_PARS_SIZE_PS1 12
#define CALMWATER_PARS_SIZE_PS2 16
#define CALMWATER_PARS_SIZE (CALMWATER_PARS_SIZE_PS1 + CALMWATER_PARS_SIZE_PS2)
// sea water param arrays sizes
#define SEAWATER_VSPARS_START 172
#define SEAWATER_VSPARS_SIZE 16
#define SEAWATER_PSPARS_SIZE_1 16

//! Enable/disable blur on the SSSM
//#define SSSMBLUR

//!{ Vertex shader constants
#define VSC_LIST(XXS,XXA) \
  XXS(float4x3,  VSC_ViewMatrix,                                        c,0)     /*World View matrix*/\
  XXS(float4x4,  VSC_ProjMatrix,                                        c,3)     /*Projection matrix*/\
  XXS(float4,    VSC_CameraPosition,                                    c,7)     /*Position of the camera in model space*/\
  XXA(float4x2,  VSC_TexTransform,                                      c,8, 8)  /*Texture coordinates*/\
  XXS(float4,    VSC_AE,                                                c,24)    /*(_lightAmbient * _matAmbient + _matEmmisive) in case one directional light is present, (_matEmmisive) elsewhere. A_E.w == diffuse back coeficient*/\
  XXS(float4,    VSC_GE,                                                c,30)    /*Color of light reflected by ground and Emmisive color. Used by ordinary shaders, that means shader with hemispherical lighting*/\
  XXS(float4,    VSC_InstanceColor,                                     c,25)    /*Color (inc. alpha of given instance)*/\
  XXS(float4,    VSC_UDirectionTransformedDir,                          c,26)    /*Direction UP in model space*/\
  XXS(float4,    VSC_LDirectionTransformedDir,                          c,27)    /*Directional light parameters*/\
  XXS(float4,    VSC_LDirectionD,                                       c,28)    /*Directional light parameters*/\
  XXS(float4,    VSC_LDirectionS,                                       c,29)    /*Directional light parameters*/\
  XXS(float4,    VSC_LDirectionGround,                                  c,42)    /*Directional light parameters*/\
  XXS(float4,    VSC_SpecularPower_Alpha_FogEnd_RCPFogEndMinusFogStart, c,31)    /*Specular power common for all lights & Alpha value & Fog end & 1/(fogEnd-fogStart)*/\
  XXS(float4,    VSC_Free_ExpFog_AFogEnd_RCPAFogEndMinusAFogStart,      c,34)    /*alpha fog - register shared with shadowVolumeLength*/\
  XXS(float,     VSC_MatrixOffset,                                      c,32)    /*Offset of the matrices in the global array*/\
  XXS(float,     VSC_BonesStride,                                       c,33)    /*Stride of bones (aligned number of bones in the shape) in the skinned instancing*/\
  XXS(float,     VSC_ShadowVolumeLength,                                c,34)    /**/\
  XXS(float4x3,  VSC_ShadowmapMatrix,                                   c,35)    /*Matrix to transform from view space to shadowmap space*/\
  XXS(float4,    VSC_InstanceLandShadowIntensity,                       c,38)    /*Intensity of the land shadow of the given instance*/\
  XXS(float4,    VSC_CameraDirection,                                   c,39)    /*Direction of the camera in model space*/\
  XXA(float4,    VSC_TexCoordType,                                      c,40, 2)  /*Type of texcoord identification*/\
  XXS(float4,    VSC_LAND_SHADOWGRID_GRID__1__GRIDd2, c,FREESPACE_START_REGISTER0)      /*Land shadow parameters*/\
  XXA(float4,    VSC_LAND_SHADOW,                     c,FREESPACE_START_REGISTER1, 144) /*Land shadow parameters*/\
  XXS(float4,    VSC_TerrainAlphaAdd,                                   c,NFS_0) /*Terrain parameters*/\
  XXS(float4,    VSC_TerrainAlphaMul,                                   c,NFS_1) /*Terrain parameters*/\
  XXS(float4,    VSC_TerrainLODPars,                                    c,33)    /*Terrain parameters*/\
  XXA(float4,    VSC_TreeCrown,                                         c,NFS_0, 2)     /*Tree crown parameters*/\
  XXS(float4x3,  VSC_LWSMatrix,                                         c,NFS_0) /*Local World Space matrix*/\
  XXS(float4,    VSC_Period_X_X_SeaLevel,             c,FREESPACE_START_REGISTER0) /*Shore parameters*/\
  XXS(float4,    VSC_WaveHeight,                      c,FREESPACE_START_REGISTER0)    /*Water parameters*/\
  XXS(float4,    VSC_WaveGrid,                        c,FREESPACE_START_REGISTER1)    /*Water parameters - {0.9999f/grid,waterSegSize,1,(waterSegSize-1)*0.5f+0.0001f};*/\
  XXS(float4,    VSC_WaterPeakWhite,                  c,FREESPACE_START_REGISTER2)    /*Water parameters - {mul peak, add peak, mul shore, add shore};*/\
  XXS(float4,    VSC_WaterSeaLevel,                   c,FREESPACE_START_REGISTER3)    /*Water parameters - {seaLevel, deep water mul, deep water add, deep water alpha};*/\
  XXA(float4,    VSC_WaterDepth,                      c,FREESPACE_START_REGISTER4, 9*9) /*Water parameters - Depth*/\
  XXS(int4,      VSC_PointLoopCount,                                    i,0)     /*holds number of point lights, it is being used for static looping*/\
  XXS(int4,      VSC_SpotLoopCount,                                     i,1)     /*holds number of spot lights, it is being used for static looping*/\
  XXS(float4,    VSC_ResolX_ResolY_FovX_FovY,         c,FREESPACE_START_REGISTER4) /*Resolution and FOV parameters (for VSPoint shader)*/\
  XXA(float4,    VSC_CalmWaterPars,                   c, CALMWATER_VSPARS_START, CALMWATER_PARS_SIZE ) /*calm water specific params*/\
  XXA(float4,    VSC_SeaWaterPars,                    c, SEAWATER_VSPARS_START, SEAWATER_VSPARS_SIZE ) /*sea water specific params*/\
  XXS(float4,    VSC_TreeAdvPars,                     c, NFS_0 )  /*advanced tree specific params*/\

#define VSC_LDirectionTransformedDirSky VSC_LDirectionTransformedDir  /*Directional Sky light parameters*/
#define VSC_LDirectionSunSkyColor       VSC_LDirectionD               /*Directional Sky light parameters*/
#define VSC_LDirectionSkyColor          VSC_LDirectionS               /*Directional Sky light parameters*/
#define VSC_LDirectionSetColorColor     VSC_LDirectionTransformedDir  /*Directional setColor light parameters*/
#define VSC_ShadowRampMulAdd            VSC_LDirectionTransformedDir  /*used in postprocess effect - shared location with other constant above*/
//!}

//! Start PS constant register designed for craters
#define PSC_CRATER_START 27
//! Maximum number of craters
#define MAX_CRATERS (41-PSC_CRATER_START)

#define PSC_CRATERTEMP_START 42

//!{ Pixel shader constants
#define PSC_LIST(XXS,XXA) \
  XXS(float4, PSC_AE,                              c,0)  /* Color of light reflected by air - formerly known as ambient color - and Emmisive color*/\
  XXS(float3, PSC_GE,                              c,7)  /* Color of light reflected by ground and Emmisive color. Used by ordinary shaders, that means shader with hemispherical lighting*/\
  XXS(float4, PSC_DForced,                         c,9)  /**/\
  XXS(float4, PSC_Diffuse,                         c,1)  /**/\
  XXS(float4, PSC_DiffuseBack,                     c,2)  /* Used by trees, trees don't use the bidirectional lighting */\
  XXS(float3, PSC_LDirectionGround,                c,2)  /* Used by ordinary shaders, they don't use "through" light calculation */\
  XXS(float4, PSC_Specular,                        c,3)  /**/\
  XXS(float3, PSC_GroundReflColor,                 c,4)  /**/\
  XXS(float3, PSC_SkyReflColor,                    c,5)  /**/\
  XXS(float3, PSC_WaveColor,                       c,7)  /* Used by water, water doesn't use the hemispherical lighting */\
  XXS(float4, PSC_GlassEnvColor,                   c,4)  /**/\
  XXS(float4, PSC_GlassMatSpecular,                c,5)  /**/\
  XXS(float4, PSC_T0AvgColor,                      c,6)  /**/\
  XXS(float4, PSC_MaxColor,                        c,8)  /**/\
  XXS(float4, PSC_Shadow_Factor_ZHalf,             c,10) /**/\
  XXS(float4, PSC_DepthClip,                       c,11) /*W stores the value of the z-clipping plane for depth buffer rendering*/\
  XXS(float4, PSC_FogColor,                        c,12) /*Fog color*/\
  XXS(float4, PSC_DitherCoef,                      c,13) /*Dither coefficient for AlphaToCoverage - dependent on used FSAA level*/\
  XXS(float4, PSC_SBTSize_invSBTSize_X_AlphaMult,  c,14) /*Alpha multiplier - used for AlphaToCoverage to make the trees thickers*/\
  XXS(float4, PSC_WLight,                          c,15) /*Main light direction in world coordinates*/\
  XXS(float4, PSC_HDRMultiply,                     c,16) /*x: amount of glow*/\
  XXS(float4, PSC_InvW_InvH_X_X,                   c,17) /*Inverse of backbuffers W and H*/\
  XXS(float4, PSC_X_X_X_AlphaRef,                  c,18) /**/\
  XXA(float,  PSC_Layers,                          c,19,6) /*Toggle individual layers on/off */\
  XXS(float4, PSC_HTMin_HTMax_AFMax_MFMax,         c,25) /*TI properties - _VBS3_TI*/\
  XXS(float4, PSC_MFact_TBody_X_HSDiffCoef,        c,26) /*TI properties - _VBS3_TI*/\
  XXA(float4, PSC_Crater,                          c,PSC_CRATER_START, MAX_CRATERS) /*Craters from this to 32*/\
  XXA(bool,   PSC_ShadingDetail,                   b,0,4) /*Video options Shading detail setting is Low, Normal, High, Very High (or better) */\
  XXA(float4, PSC_NoTexSizeLog2,                   c,41,2) /* Height maps size for individual layers */\
  XXA(float4, PSCGPU_TOffX_TOffY_X_X,              c,44,4) /*X360 specific - constant owned by the GPU - holding offest of a tile*/\
  XXA(float4, PSC_PRTConstantDirect,               c,48,3) /*Constants used by the TreePRT shaders - direct lights*/\
  XXA(float4, PSC_PRTConstantIndirect,             c,51,3) /*Constants used by the TreePRT shaders - indirect lights*/\
  XXS(float4, PSC_TreeAdjust,                      c,54) /*Various parameters to adjust tree rendering*/\
  XXS(int4,   PSC_PointLoopCount,                  i,0)    /*holds number of point lights, it is being used for static looping*/\
  XXS(int4,   PSC_SpotLoopCount,                   i,1)    /*holds number of spot lights, it is being used for static looping*/\
  XXA(float4, PSC_SkinEffectPars,                  c, 48, 4 )  /*skin shader specific params*/\
  XXA(float4, PSC_CalmWaterPars1,                  c, 27, CALMWATER_PARS_SIZE_PS1 )  /*calm water specific params block1*/\
  XXA(float4, PSC_CalmWaterPars2,                  c, 48, CALMWATER_PARS_SIZE_PS2 )  /*calm water specific params block2*/\
  XXA(float4, PSC_SeaWaterPars1,                   c, 48, SEAWATER_PSPARS_SIZE_1 )  /*sea water specific params block1*/\
  XXA(float4, PSC_TreeAdvPars,                     c, 27, 5 )  /*tree adv specific*/\
  XXA(float4, PSC_CraterTemp,                      c, PSC_CRATERTEMP_START, MAX_CRATERS) /*crater temperate*/\
  XXS(float4, PSC_DiscretizeAlpha,                 c,56) /*If alpha should be discretized*/\

//!{ Special texture ID's
/*!
Several textures at the end of the stages are designed to keep textures across the whole
rendering process. Neither other textures, nor NULL can be put on their places during rendering.
*/
#define TEXID_FIRST 15        //! First stage dedicated to special textures
#define TEXID_SHADOWMAP 15    //! Shadow map texture
#define TEXID_TC 15           //! Temperature Tc table // _VBS3_TI
#define TEXID_TI 14           //! TI texture can share space with regular textures // _VBS3_TI
//!}

//! Value used in Point vertex shader for stars to influence the point size. Inverse value is used for other than star purposes (f.i. for point drawing) where 
//! exact size need to be specified
#define MAX_STAR_INTENSITY 0.0015

// Temperature range to be handled by our system
// _VBS3_TI
#define TempMin 0.0f
#define TempMax 60.0f
#define ContrastMax 30.0f //(TempMax - TempMin)*0.5
#define InvTempDiff 0.016666667 //1.0f / (TempMax - TempMin)

#define TIConversionDimensionW 256
#define TIConversionDimensionH 64

#define EncodeTemperatureToRGB(temperature, R, G, B) \
  float temperature03 = saturate(((temperature) - TempMin) * InvTempDiff) * 3.0f; \
  if (temperature03 < 1) \
  { \
  R = temperature03; \
  G = 0.0f; \
  B = 0.0f; \
  } \
  else if (temperature03 < 2) \
  { \
  R = 1.0f; \
  G = temperature03 - 1.0f; \
  B = 0.0f; \
  } \
  else \
  { \
  R = 1.0f; \
  G = 1.0f; \
  B = temperature03 - 2.0f; \
  }
