// Handling of video playback

#include "../wpch.hpp"

#if !_DISABLE_GUI

#include "engdd9.hpp"
#include "txtd3d9.hpp"

#ifdef _XBOX

#include <xmedia2.h>
#if _DEBUG
#pragma comment(lib,"xmediad2.lib")
#else
#pragma comment(lib,"xmedia2.lib")
#endif

/// Support for video playback using XMedia library
class VideoPlaybackXMV: public VideoPlayback, private NoCopy
{
protected:
  ComRef<IXMedia2XmvPlayer> _player;

  QIFStream _file;
  DWORD _offset;
  DWORD _size;

public:
  explicit VideoPlaybackXMV(IDirect3DDevice9 *device, const char *path, float volume);
  ~VideoPlaybackXMV();

  bool IsValid() const {return _player != NULL;}
  
  IXMedia2XmvPlayer *GetPlayer() const {return _player;}
  
  HRESULT ReadStream(ULONGLONG offset, PVOID buffer, DWORD bytesToRead, DWORD *bytesRead);

private:
  void PreRead(QFileSize offset);
};

static HRESULT CALLBACK ReadStream(PVOID context, ULONGLONG offset, PVOID buffer, DWORD bytesToRead, DWORD *bytesRead)
{
  VideoPlaybackXMV *video = (VideoPlaybackXMV *)context;
  Assert(video);
  return video->ReadStream(offset, buffer, bytesToRead, bytesRead);
}

VideoPlaybackXMV::VideoPlaybackXMV(IDirect3DDevice9 *device, const char *path, float volume)
{
  // open the source file
  _file.open(path);
  if (_file.fail() || _file.eof()) return;

  // create the player
  XMEDIA_XMV_CREATE_PARAMETERS pars;
  pars.dwFlags = 0;
  pars.dwAudioStreamId = XMEDIA_STREAM_ID_USE_DEFAULT;
  pars.dwVideoStreamId = XMEDIA_STREAM_ID_USE_DEFAULT;
  pars.createFromUserIo.pfnAudioStreamReadCallback = &::ReadStream;
  pars.createFromUserIo.pfnVideoStreamReadCallback = &::ReadStream;
  pars.createFromUserIo.pvAudioStreamContext = this;
  pars.createFromUserIo.pvVideoStreamContext = this;
  // TODO: pass XAudio2 here
  HRESULT result = XMedia2CreateXmvPlayer(device, NULL, &pars, _player.Init());
  if (FAILED(result))
  {
    _player.Free();
    return;
  }

  // pre-read the file
  PreRead(0);

  /*
  // set the volume
  ComRef<IXAudio2SourceVoice> voice;
  result = _player->GetSourceVoice(voice.Init());
  if (SUCCEEDED(result)) voice->SetVolume(volume);
  */
}

VideoPlaybackXMV::~VideoPlaybackXMV()
{
  _player.Free();
}

const int PreloadSize = 2 * 1024 * 1024;
const int PreloadAlign = 1 * 1024 * 1024;

void VideoPlaybackXMV::PreRead(QFileSize offset)
{
  const QFileSize requestSize = 4096;

  // preload aligned to 1 MB
  QFileSize end = offset + PreloadSize;
  // align preloading to make sure we load big chunks
  end &= ~(PreloadAlign - 1);

  if (end > offset)
  {
    for (QFileSize total=0; total<end-offset; total+=requestSize)
    { // this loop is actually not needed - PreRead does it internally
      _file.PreRead(FileRequestPriority(600), offset + total, requestSize);
    }
  }
}

HRESULT VideoPlaybackXMV::ReadStream(ULONGLONG offset, PVOID buffer, DWORD bytesToRead, DWORD *bytesRead)
{
  // check if data are ready
  /*
  if (!_file.PreRead(FileRequestPriority(1), offset, bytesToRead))
  {
    bytesRead = 0;
    return S_OK;
  }
  */

  _file.seekg(offset, QIOS::beg);
  *bytesRead = _file.read(buffer, bytesToRead);
  if (_file.fail() || _file.eof()) return E_FAIL;

  PreRead(offset + *bytesRead);
  return S_OK;
}

// implementation of high level (Engine) interface

VideoPlayback *EngineDD9::VideoOpen(const char *path, float volume)
{
  // TODO: check do we need some device synchronization?
  // it seems we are doing all device access on our own
  VideoPlaybackXMV *player = new VideoPlaybackXMV(_d3DDevice.GetRef(), path, volume);
  if (!player->IsValid())
  {
    RptF("Video decoder not created for %s", cc_cast(path));
    delete player;
    return NULL;
  }

  // switch to presentation interval 1
/*
  if (_presentationInterval!=1)
  {
    D3DPRESENT_PARAMETERS pp = _pp;
    pp.FullScreen_PresentationInterval = D3DPRESENT_INTERVAL_ONE;
    _presentationInterval = 1;

    _d3DDevice->Reset(&pp);
  }
*/
  return player;
}

void EngineDD9::VideoClose(VideoPlayback *video)
{
  VideoPlaybackXMV *player = static_cast<VideoPlaybackXMV *>(video);
  delete player;

  // switch to normal presentation interval
  /*
  if (_presentationInterval!=_defPresentationInterval)
  {
    _d3DDevice->Reset(&_pp);
    _presentationInterval = _defPresentationInterval;
  }
  */
}

void EngineDD9::VideoFrame(VideoPlayback *video)
{
  VideoPlaybackXMV *xmv = static_cast<VideoPlaybackXMV *>(video);
  IXMedia2XmvPlayer *player = xmv->GetPlayer();
  Assert(player);

  //_workerContext.StartUsingMainThread();
  XMEDIA_VIDEO_DESCRIPTOR desc;
  HRESULT result = player->GetVideoDescriptor(&desc);
  //_workerContext.StopUsingMainThread();
  if (FAILED(result))
  {
    LogF("GetVideoDescriptor failed with 0x%x", result);
    return;
  }
  // video resolution is desc.dwWidth x desc.dwHeight
  int movieWidth = desc.dwWidth;
  int movieHeight = desc.dwHeight;

  // create source textures
  ComRef<IDirect3DTexture9> textureY, textureU, textureV;
  result = _d3DDevice->CreateTexture(movieWidth, movieHeight, 1, 0, D3DFMT_LIN_L8, 0, textureY.Init(), NULL);
  if (FAILED(result))
  {
    LogF("CreateTexture failed with 0x%x", result);
    return;
  }
  result = _d3DDevice->CreateTexture(movieWidth, movieHeight, 1, 0, D3DFMT_LIN_L8, 0, textureU.Init(), NULL);
  if (FAILED(result))
  {
    LogF("CreateTexture failed with 0x%x", result);
    return;
  }
  result = _d3DDevice->CreateTexture(movieWidth, movieHeight, 1, 0, D3DFMT_LIN_L8, 0, textureV.Init(), NULL);
  if (FAILED(result))
  {
    LogF("CreateTexture failed with 0x%x", result);
    return;
  }

  D3DLOCKED_RECT lockRectY;
  D3DLOCKED_RECT lockRectU;
  D3DLOCKED_RECT lockRectV;
  textureY->LockRect(0, &lockRectY, NULL, 0);
  textureU->LockRect(0, &lockRectU, NULL, 0);
  textureV->LockRect(0, &lockRectV, NULL, 0 );

  DWORD luminanceSize = movieHeight * lockRectY.Pitch;
  DWORD chrominanceSize = movieHeight / 2 * lockRectU.Pitch;

  XMEDIA_VIDEO_FRAME videoFrame;
  ZeroMemory(&videoFrame, sizeof(XMEDIA_VIDEO_FRAME));
  videoFrame.videoFormat = XMEDIA_VIDEO_FORMAT_I420;
  videoFrame.dwFlags = 0;
  videoFrame.lTimeToPresent = NULL;
  videoFrame.i420.pvYBuffer = lockRectY.pBits;
  videoFrame.i420.pvUBuffer = lockRectU.pBits;
  videoFrame.i420.pvVBuffer = lockRectV.pBits;
  videoFrame.i420.dwYBufferSize = luminanceSize;
  videoFrame.i420.dwUBufferSize = chrominanceSize;
  videoFrame.i420.dwVBufferSize = chrominanceSize;
  videoFrame.i420.dwYPitch = lockRectY.Pitch;
  videoFrame.i420.dwUPitch = lockRectU.Pitch;
  videoFrame.i420.dwVPitch = lockRectV.Pitch;

  //_workerContext.StartUsingMainThread();
  result = player->GetNextFrame(&videoFrame);
  //_workerContext.StopUsingMainThread();

  textureY->UnlockRect(0);
  textureU->UnlockRect(0);
  textureV->UnlockRect(0);

  if (FAILED(result))
  {
    LogF("GetNextFrame failed with 0x%x", result);
    return;
  }

  // TODO: rendering (see ms-help://MS.VSCC.v80/MS.Xbox360SDK/Xbox360SDK/ixmediaxmvplayer_getnextframe.htm)
/*
  _d3DDevice->SetTexture( 0, m_pYTexture[m_dwTextureID] );
  _d3DDevice->SetTexture( 1, m_pUTexture[m_dwTextureID] );
  _d3DDevice->SetTexture( 2, m_pVTexture[m_dwTextureID] );

  _d3DDevice->SetSamplerState( 0, D3DSAMP_ADDRESSU, D3DTADDRESS_CLAMP );
  _d3DDevice->SetSamplerState( 0, D3DSAMP_ADDRESSV, D3DTADDRESS_CLAMP );
  _d3DDevice->SetSamplerState( 0, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR );
  _d3DDevice->SetSamplerState( 0, D3DSAMP_MINFILTER, D3DTEXF_LINEAR );

  _d3DDevice->SetVertexDeclaration( m_pVDecl );
  _d3DDevice->SetVertexShader( m_pVShader );
  _d3DDevice->SetPixelShader( m_pPShader );
  _d3DDevice->SetStreamSource( 0, m_pVBuffer, 0, sizeof( VertexData ) );

  _d3DDevice->DrawPrimitive( D3DPT_QUADLIST, 0, 1 );
*/
}

#else // !defined _XBOX

VideoPlayback *EngineDD9::VideoOpen(const char *path, float volume)
{
  return NULL;
}

void EngineDD9::VideoClose(VideoPlayback *video)
{

}

void EngineDD9::VideoFrame(VideoPlayback *video)
{

}

#endif // !defined _XBOX

#endif // !_DISABLE_GUI