#include "../wpch.hpp"
#if !defined _SERVER
#include "postProcess.hpp"
#include "engdd9.hpp"
#include "txtd3d9.hpp"
#include "../textbank.hpp"
#include "../diagModes.hpp"
#include <Es/Strings/bString.hpp>
#include "../Shape/shape.hpp"
#include "../appFrameExt.hpp"
#include "../global.hpp"
#include <El/Math/mathStore.hpp>
#include "../keyInput.hpp"
#include "../dikCodes.h"
#include "ShaderSources_3_0/common.h"
#include "../scene.hpp"
#include "../camera.hpp"

//because rain needs to cooperate with camera inside vehicle
#include "../world.hpp"
//...and sealevel
#include "../landscape.hpp"

#include <El/Evaluator/express.hpp>
#include <El/Modules/modules.hpp>
#include <El/Common/randomGen.hpp>

#if _ENABLE_COMPILED_SHADER_CACHE
#include "ShaderCompileCache.h"
#endif

EngineDD9 *EngineDD9::PostProcess::_engine;

void EngineDD9::PostProcess::Prepare()
{
  const int cb = -1;
  _engine->FlushAndFreeAllQueues();
  // we want all render states to be set as needed
  // postprocess coordinates are given in 3D, but there is no camera space
  _engine->SwitchTL(cb,TLViewspace);

  // Set the src blend and dest blend somehow (they are not going to be set in the DoPrepareTriangle and the temporary setting returning would set a undefined value)
  _engine->D3DSetRenderState(cb, D3DRS_SRCBLEND,D3DBLEND_ONE);
  _engine->D3DSetRenderState(cb, D3DRS_DESTBLEND,D3DBLEND_ONE);

  // Set standard render states (it is possible to change it in every descendant, but
  // it must return it also back)
  // No alpha blending - we will do everything in the pixel shader
  // Use point filter
  EngineShapeProperties prop;
  _engine->DoPrepareTriangle(cb,NULL,TexMaterialLODInfo(NULL, 0),0,NoZBuf|NoZWrite|ClampU|ClampV|FilterPoint,prop);
  _engine->DoShadowsStatesSettings(cb,SS_Transparent,false);
}

void EngineDD9::PostProcess::Finish()
{
  int cb = -1;
  // Restore usual rendering settings
  _engine->SwitchTL(cb, TLDisabled);
  
  #if _DEBUG || _PROFILE
    if (!_engine->_hlRecording && Glob.config._renderThread==Config::ThreadingEmulated)
    {
      _engine->KickOffCBDebug();
    }
  #endif
}

bool EngineDD9::PostProcess::CompilePixelShader(
  const LazyLoadFile &hlsl, const char *psName, ComRef<IDirect3DPixelShader9> &ps,
  bool fullPrec
) const
{
  // define macros for the shaders
  D3DXMacroList defines;
  _engine->CreateBasicDefines(defines);

  D3DXCompileShader_Result result;  
  
  HRESULT hr;
  DWORD flags = fullPrec ? 0 : D3DXSHADER_PARTIALPRECISION;

  hr = D3DXCompileShader_Cached(hlsl, defines.Data(), &_engine->_hlslInclude, psName, _engine->_caps._psVersion, flags, result);
  if (FAILED(hr))
  {
    LogF("Error: %x in D3DXCompileShader (%s).", hr, psName);
    if (result.errorMsgs.NotNull())
    {
      LogF("%s", (const char*)result.errorMsgs->GetBufferPointer());
    }
    return false;
  };
  if (FAILED(_engine->GetDirect3DDevice()->CreatePixelShader((const DWORD*)result.shader->GetBufferPointer(), ps.Init())))
  {
    LogF("Error: %s shader creation failed.",psName);
    return false;
  }

  return true;
}

bool EngineDD9::PostProcess::CompileVertexShader(
  const LazyLoadFile &hlsl, const char *vsName, ComRef<IDirect3DVertexShader9> &vs
) const
{
  // define macros for the shaders
  D3DXMacroList defines;
  _engine->CreateBasicDefines(defines);
  
  D3DXCompileShader_Result result;  
  DWORD flags = DebugVS ? D3DXSHADER_SKIPOPTIMIZATION|D3DXSHADER_DEBUG : 0;
  
  HRESULT hr;
  hr = D3DXCompileShader_Cached(hlsl, defines.Data(), &_engine->_hlslInclude, vsName, _engine->_caps._vsVersion, flags, result);
  if (FAILED(hr))
  {
    LogF("Error: %x in D3DXCompileShader (%s).", hr, vsName);
    if (result.errorMsgs.NotNull())
    {
      LogF("%s", (const char*)result.errorMsgs->GetBufferPointer());
    }
    return false;
  };
  if (FAILED(_engine->GetDirect3DDevice()->CreateVertexShader((const DWORD*)result.shader->GetBufferPointer(), vs.Init())))
  {
    LogF("Error: %s shader creation failed.",vsName);
    return false;
  }

  return true;
}

//////////////////////////////////////////////////////////////////////////
void EngineDD9::PPSimple::Prepare()
{
  base::Prepare();

  const int cb = -1;
  // Set the stream source
  _engine->SetStreamSource(cb,_vb, sizeof(Vertex), 4);

  // Set the vertex declaration
  _engine->SetVertexDeclaration(cb,_vd);

  // Set the vertex shader
  _engine->SetVertexShader(cb,_vs);

  // Set the pixel shader
  _engine->SetPixelShader(cb,_ps);
}
bool EngineDD9::PPSimple::Init(const LazyLoadFile &hlsl, const LazyLoadFile &vs, const char *vsName)
{
  if (!base::Init(hlsl,vs)) return false;

  // Create the VB
  ComRef<IDirect3DVertexBuffer9> vb;
  if (FAILED(_engine->GetDirect3DDevice()->CreateVertexBuffer(sizeof(Vertex) * 4, D3DUSAGE_WRITEONLY, 0, D3DPOOL_DEFAULT, vb.Init(), NULL)))
  {
    LogF("Error: Vertex buffer creation failed.");
    return false;
  }
  Vertex *v;
  float offsetU = 1.0f / _engine->Width() * 0.5f; //See the Nearest-Point Sampling in the DX9 documentation
  float offsetV = 1.0f / _engine->Height() * 0.5f;
  vb->Lock(0, 0, (void**)&v, 0);
  v[0].pos = Vector3P(-0.5, -0.5, 0.5); v[0].t0.u = 0 + offsetU; v[0].t0.v = 1 + offsetV;
  v[1].pos = Vector3P(-0.5,  0.5, 0.5); v[1].t0.u = 0 + offsetU; v[1].t0.v = 0 + offsetV;
  v[2].pos = Vector3P( 0.5, -0.5, 0.5); v[2].t0.u = 1 + offsetU; v[2].t0.v = 1 + offsetV;
  v[3].pos = Vector3P( 0.5,  0.5, 0.5); v[3].t0.u = 1 + offsetU; v[3].t0.v = 0 + offsetV;

  vb->Unlock();

  // Create the VD
  const D3DVERTEXELEMENT9 vertexDecl[] =
  {
    {0,  0, D3DDECLTYPE_FLOAT3,   D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION, 0},
    {0, 12, D3DDECLTYPE_FLOAT2,   D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 0},
    D3DDECL_END()
  };
  ComRef<IDirect3DVertexDeclaration9> vd;
  if (FAILED(_engine->GetDirect3DDevice()->CreateVertexDeclaration(vertexDecl, vd.Init())))
  {
    LogF("Error: Vertex declaration creation failed.");
    return false;
  }

  // note: vertex shader is stored in PS.hlsl for simple effects
  if (!CompileVertexShader(hlsl,vsName,_vs))
  {
    return false;
  }

  _vb = VertexStaticData9::BufferFuture(vb);
  _vd = vd;

  return true;
}

bool EngineDD9::PPFinal::Init(const LazyLoadFile &hlsl, const LazyLoadFile &vs)
{
  if (!base::Init(hlsl,vs)) return false;

  if (!CompilePixelShader(hlsl,"PSPostProcessCopy", _ps))
  {
    return false;
  }
  return true;
}


void EngineDD9::PPFinal::Do(bool isLast)
{
  Do(isLast,true);
}

void EngineDD9::PPFinal::Do(bool isLast,  bool forceNoSRGBOnRead)
{
  const int cb = -1;
  
  // not much to do if current render target is back-buffer (common situation, happens whenever we were doing 3D rendering)
  if (_engine->_currRenderTargetSurface!=_engine->_backBuffer)
  {
    PROFILE_SCOPE_GRF_EX(ppFin,pp,SCOPE_COLOR_YELLOW);

    
    bool inScene = _engine->_d3dFrameOpen;
    if (!inScene) _engine->D3DBeginScene();
    // StretchRect not possible here?
    // Prepare drawing
    Prepare();

    // Set temporary render states
    SetRenderStateTemp rsCOLORWRITEENABLE(D3DRS_COLORWRITEENABLE, D3DCOLORWRITEENABLE_RED | D3DCOLORWRITEENABLE_GREEN | D3DCOLORWRITEENABLE_BLUE | D3DCOLORWRITEENABLE_ALPHA);

    #ifdef _XBOX
      _engine->ResolveRenderTarget();
    #endif
    // Remember the current render target (to be used as a texture later)
    IDirect3DTexture9 *rt = _engine->GetCurrRenderTargetAsTexture();
    DoAssert(rt);

    // Use the back buffer as render target
    _engine->SwitchToBackBufferRenderTarget();

    // Use the texture
    SetTextureTemp tRenderTarget(0, rt);

    bool rescale = _engine->_wRT!=_engine->_h || _engine->_hRT!=_engine->_h;
    
    // note: stage0 is set to point sampling by default (see FilterPoint in EngineDD9::PostProcess::Prepare)
    D3DTEXTUREFILTERTYPE filter = rescale ? D3DTEXF_LINEAR : D3DTEXF_POINT;
    SetSamplerStateTemp sampFilterMag0(0, D3DSAMP_MAGFILTER, filter);
    SetSamplerStateTemp sampFilterMin0(0, D3DSAMP_MINFILTER, filter);

#ifndef _X360SHADERGEN
    BOOL rtIsSRGB = _engine->_caps._rtIsSRGB && _engine->_caps._canReadSRGB && !forceNoSRGBOnRead;
    SetSamplerStateTemp tempSRGB0(0, D3DSAMP_SRGBTEXTURE, rtIsSRGB);
#endif

    
    PushVertexShaderConstant pushC0(0);
    PushVertexShaderConstant pushC1(1);
    PushVertexShaderConstant pushC2(2);
    PushVertexShaderConstant pushC3(3);
    PushVertexShaderConstant pushC4(4);
    PushVertexShaderConstant pushC5(5);
    PushVertexShaderConstant pushC6(6);
    PushVertexShaderConstant pushC7(7);
    PushVertexShaderConstant pushC8(8);
    
    // Prepare the UV coordinates offset coefficients
    static const float baseU[4] = {0, 0, 1, 1};
    static const float baseV[4] = {1, 0, 1, 0};

    // older version, well tuned
    const float tgtOffsetU = 0;
    const float tgtOffsetV = 0;
    const float srcOffsetU = rescale ? 0 : 0.5f / _engine->_wRT;
    const float srcOffsetV = rescale ? 0 : 0.5f / _engine->_hRT;

    IDirect3DTexture9 *glow = _engine->_pp._bloom->GetBlurredTexture();

    D3DSURFACE_DESC glowDesc;
    glow->GetLevelDesc(0, &glowDesc);

    float U[4][4];
    float V[4][4];
    for (int vertex = 0; vertex < 4; vertex++)
    {
      // Texture 0 - source image
      U[vertex][0] = baseU[vertex] + srcOffsetU;
      V[vertex][0] = baseV[vertex] + srcOffsetV;
      // Texture 1
      U[vertex][1] = 0;
      V[vertex][1] = 0;
      // Texture 2
      U[vertex][2] = 0;
      V[vertex][2] = 0;
      // Texture 3
      U[vertex][3] = 0;
      V[vertex][3] = 0;
    }
    const float posOffset[4] = {tgtOffsetU,tgtOffsetV,0,0};
    _engine->SetVertexShaderConstantF(cb, 0, U[0], 4);
    _engine->SetVertexShaderConstantF(cb, 4, V[0], 4);
    _engine->SetVertexShaderConstantF(cb, 8, posOffset, 1);
    _engine->SetPixelShader(cb,_ps);


    // WIP: RESCALER remove _vsCustomEdge from PPGlow (always use point sampling there)

    // Drawing
    _engine->DrawPrimitive(cb, D3DPT_TRIANGLESTRIP, 0, 2);
    
    if (!inScene) _engine->D3DEndScene();
  }
  Finish();
}

/*
	Generate coefficients used in filtering process. Coefficients define offsets and weights for neighbouring
	samples used for sample determination. More information about coefficients can be found in GPU Gems 2,
	page 316.
*/
static void GenCoefficients(AutoArray<float> &vec, int itemsNum)
{
	const float wMult = 1.0f / 6.0f;

  float invDivider = 1.0f/ (itemsNum - 1);
	for (int i = 0; i < itemsNum; i++)
	{
		float alpha = i * invDivider; // alpha [0, 1], fractional part of texel coords
		float alpha2 = alpha * alpha;
		float alpha3 = alpha2 * alpha;

		// B-spline kernel - Coons cubics
		float w0 =		-alpha3 + 3.0f * alpha2 - 3.0f * alpha + 1.0f;		// w0 = -1*a^3 + 3*a^2 - 3*a + 1
		float w1 =  3.0f * alpha3 - 6.0f * alpha2 + 4.0f;						// w1 =  3*a^3 - 6*a^2 + 0*a + 4
		//w1 =  3.0f * alpha3 - 6.0f * alpha2 + 3.0f * alpha;
		float w2 = -3.0f * alpha3 + 3.0f * alpha2 + 3.0f * alpha + 1.0f;		// w2 = -3*a^3 + 3*a^2 + 3*a + 0
		//w2 = - 3.0f * alpha3 + 3.0f * alpha2;
		float w3 =		 alpha3;											// w3 =  1*a^3 + 0*a^2 + 0*a + 0

		w0 *= wMult;
		w1 *= wMult;
		w2 *= wMult;
		w3 *= wMult;

		// weights
		float g0 = w0 + w1;
		float g1 = w2 + w3;

		// offsets
		float h0 = 1 - (w1 / g0) + alpha;
		float h1 = 1 + (w3 / g1) - alpha;

		//std::cout << h1 << ", " << h0 << ", " << g1 << ", " << g0 << std::endl;

		vec.Add(h1);
		vec.Add(h0);
		vec.Add(g1);
		vec.Add(g0);
	}
}


bool EngineDD9::PPRescaleBicubic::Init(const LazyLoadFile &hlsl, const LazyLoadFile &vs)
{
  if (!base::Init(hlsl,vs,"VSPostProcessRescaleBicubic")) return false;

  if (!CompilePixelShader(hlsl,"PSPostProcessRescaleBicubic", _ps))
  {
    return false;
  }

  // create lookup texture
  {
    const int width = 128;
	  const int lookupTextureSize = width * 4;
	  
    IDirect3DDevice9 *dev = _engine->_d3DDevice.GetRef();
    #define FLOAT_COEFS 1
    
    #if FLOAT_COEFS
      D3DFORMAT coefFormat = D3DFMT_A16B16G16R16F;
    #else
  	  // it seems all coefficients are 0..1, 8b precision should be enough for blending weights
      D3DFORMAT coefFormat = D3DFMT_A8B8G8R8;
    #endif
    
    #ifdef _XBOX
      coefFormat = (D3DFORMAT)MAKELINFMT(coefFormat);
    #endif
    
	  if ( FAILED(dev->CreateTexture(
		  width, 1, /*levels*/ 1, /*usage*/ 0, coefFormat, D3DPOOL_DEFAULT, _lookUpTexture.Init(), NULL
		  )) ) { return false; }
		
	  ComRef<IDirect3DTexture9> tmpTexture;

	  
    #if !XBOX_D3D
	  // temporary texture created in system pool used data filling
	  if ( FAILED(dev->CreateTexture(
		  width, 1, /*levels*/ 1, /*usage*/ 0, coefFormat, D3DPOOL_SYSTEMMEM, tmpTexture.Init(), NULL
		  )) ) return false;
		#else
		tmpTexture = _lookUpTexture;
		#endif


	  AutoArray<float> vec;
	  vec.Realloc(lookupTextureSize);

	  //prepare coefficients vector (h0, h1, g0 & g1)
	  GenCoefficients(vec, width);

	  // lock and fill texture by coefficients - convert to float16
	  D3DLOCKED_RECT lockRect;
	  if ( FAILED(tmpTexture->LockRect(0, &lockRect, 0, 0)) ) { return false; }
	  #if FLOAT_COEFS
	    D3DXFloat32To16Array((D3DXFLOAT16 *)lockRect.pBits,vec.Data(), lookupTextureSize);
	  #else
	    // caution: what about endians?
	    BYTE *coefs = (BYTE *)lockRect.pBits;
	    for (int i=0; i<lookupTextureSize; i++)
	    {
	      int coefI = toInt(vec[i]*255);
	      saturate(coefI,0,255);
	      coefs[i] = coefI;
	    }
	  #endif
	  tmpTexture->UnlockRect(0);

    #if !XBOX_D3D
	  // update texture
	  if ( FAILED(dev->UpdateTexture(tmpTexture, _lookUpTexture)) ) { return false; }
	  #endif
  }
  
  return true;
}


void EngineDD9::PPRescaleBicubic::Do(bool isLast)
{
  const int cb = -1;
  
  PROFILE_SCOPE_GRF_EX(ppBic,pp,SCOPE_COLOR_YELLOW);

  
  bool inScene = _engine->_d3dFrameOpen;
  if (!inScene) _engine->D3DBeginScene();
  // StretchRect not possible here?
  // Prepare drawing
  Prepare();

  // Set temporary render states
  SetRenderStateTemp rsCOLORWRITEENABLE(D3DRS_COLORWRITEENABLE, D3DCOLORWRITEENABLE_RED | D3DCOLORWRITEENABLE_GREEN | D3DCOLORWRITEENABLE_BLUE | D3DCOLORWRITEENABLE_ALPHA);

  #ifdef _XBOX
    _engine->ResolveRenderTarget();
  #endif
  // Remember the current render target (to be used as a texture later)
  IDirect3DTexture9 *rt = _engine->GetCurrRenderTargetAsTexture();
  DoAssert(rt);

  // Use the back buffer as render target
  _engine->SwitchToBackBufferRenderTarget();

  Assert(_engine->_wRT<_engine->_w || _engine->_hRT<_engine->_h);
  
#if !XBOX_D3D
  BOOL rtIsSRGB = _engine->_caps._rtIsSRGB && _engine->_caps._canReadSRGB;
  SetSamplerStateTemp tempSRGB0(0, D3DSAMP_SRGBTEXTURE, rtIsSRGB);
#endif

  
  PushVertexShaderConstant pushC0(0);
  PushVertexShaderConstant pushC1(1);
  PushVertexShaderConstant pushC2(2);
  PushVertexShaderConstant pushC3(3);
  PushVertexShaderConstant pushC4(4);
  PushVertexShaderConstant pushC5(5);
  PushVertexShaderConstant pushC6(6);
  PushVertexShaderConstant pushC7(7);
  PushVertexShaderConstant pushC8(8);
  
  PushPixelShaderConstant pushP0(0);
  PushPixelShaderConstant pushP1(1);

	// sampler 0 - texture to filtering
  SetTextureTemp tRenderTarget(0, rt);
	SetSamplerStateTemp s0Mag(0, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR);
	SetSamplerStateTemp s0Min(0, D3DSAMP_MINFILTER, D3DTEXF_LINEAR);
	SetSamplerStateTemp s0Mip(0, D3DSAMP_MIPFILTER, 0);
	SetSamplerStateTemp s0U(0, D3DSAMP_ADDRESSU, D3DTADDRESS_CLAMP);
	SetSamplerStateTemp s0V(0, D3DSAMP_ADDRESSV, D3DTADDRESS_CLAMP);

	// sampler 1 - lookup texture
	SetTextureTemp t1(1, _lookUpTexture);
	SetSamplerStateTemp s1Mag(1, D3DSAMP_MAGFILTER, D3DTEXF_POINT);
	SetSamplerStateTemp s1Min(1, D3DSAMP_MINFILTER, D3DTEXF_POINT);
	SetSamplerStateTemp s1Mip(1, D3DSAMP_ADDRESSU, D3DTADDRESS_WRAP);

  int srcW = _engine->_wRT, srcH = _engine->_hRT;
	D3DXVECTOR4 srcTexSize(float(srcW), float(srcH), 0.0f, 0.0f);
	D3DXVECTOR4 texelSize(1.0f / (float) srcW, 0.0f, 0.0f, 1.0f / (float) srcH);

  _engine->SetPixelShaderConstantF(cb, 0, (const float *)&srcTexSize, 1);
  _engine->SetPixelShaderConstantF(cb, 1, (const float *)&texelSize, 1);
  _engine->SetPixelShader(cb,_ps);

  // WIP: RESCALER remove _vsCustomEdge from PPGlow (always use point sampling there)

  // Drawing
  _engine->DrawPrimitive(cb, D3DPT_TRIANGLESTRIP, 0, 2);
  
  if (!inScene) _engine->D3DEndScene();
  
  Finish();
}

//////////////////////////////////////////////////////////////////////////

bool EngineDD9::PostProcessCustomEdge::Init(const LazyLoadFile &hlsl, const LazyLoadFile &vs)
{
  // Create the VB
  ComRef<IDirect3DVertexBuffer9> vb;
  if (FAILED(_engine->GetDirect3DDevice()->CreateVertexBuffer(sizeof(VertexCustomEdge) * 4, D3DUSAGE_WRITEONLY, 0, D3DPOOL_DEFAULT, vb.Init(), NULL)))
  {
    LogF("Error: Vertex buffer creation failed.");
    return false;
  }
  void *data;
  vb->Lock(0, 0, &data, 0);
  VertexCustomEdge *v = (VertexCustomEdge *)data;
  v[0].pos = Vector3P(-0.5, -0.5, 0.5); v[0].index = 0;
  v[1].pos = Vector3P(-0.5,  0.5, 0.5); v[1].index = 1;
  v[2].pos = Vector3P( 0.5, -0.5, 0.5); v[2].index = 2;
  v[3].pos = Vector3P( 0.5,  0.5, 0.5); v[3].index = 3;
  vb->Unlock();

  _vbCustomEdge = VertexStaticData9::BufferFuture(vb);

  // Create the VD
  const D3DVERTEXELEMENT9 vertexDeclCustomEdge[] =
  {
    {0,  0, D3DDECLTYPE_FLOAT3,   D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION, 0},
    {0, 12, D3DDECLTYPE_FLOAT1,   D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_BLENDINDICES, 0},
    D3DDECL_END()
  };
  if (FAILED(_engine->GetDirect3DDevice()->CreateVertexDeclaration(vertexDeclCustomEdge, _vdCustomEdge.Init())))
  {
    LogF("Error: Vertex declaration creation failed.");
    return false;
  }

  // Create the VS
  if (!CompileVertexShader(vs,"VSPostProcessCustomEdge4T", _vsCustomEdge))
  {
    return false;
  }


  // Success
  return true;
}

void EngineDD9::PostProcessCustomEdge::Prepare()
{
  base::Prepare();
  
  int cb = -1;
  // Set the vertex shader + stream source
  _engine->SetStreamSource(cb,_vbCustomEdge, sizeof(VertexCustomEdge), 4);
  _engine->SetVertexDeclaration(cb,_vdCustomEdge);
  _engine->SetVertexShader(cb,_vsCustomEdge);

}

static const int GBSizeDivisor = 4;

bool EngineDD9::PPGaussianBlur::Init(const LazyLoadFile &hlsl, const LazyLoadFile &vs)
{
  if (!base::Init(hlsl,vs)) return false;

  // Create pixel shaders
  {
    if (!CompilePixelShader(hlsl,"PSPostProcessGaussianBlurH", _psHorizontal, true)) return false;
    if (!CompilePixelShader(hlsl,"PSPostProcessGaussianBlurV", _psBlur, true)) return false;
  }

  // Create horizontal blur and final blur render targets
  #ifndef _X360SHADERGEN
  {
    int fullWidth = _engine->Width();
    int fullHeight = _engine->Height();
    int partWidth = fullWidth / GBSizeDivisor;
    int partHeight = fullHeight / GBSizeDivisor;
    HRESULT err;
    err = _engine->CreateTextureRT(partWidth, fullHeight, 1, _engine->_rtFormat, D3DPOOL_DEFAULT, _rtHorizontal, _rtsHorizontal, true);
    if (err != D3D_OK) {_engine->DDError9("Error: Render target texture creation failed.", err); return false;}
    err = _engine->CreateTextureRT(partWidth, partHeight, 1, _engine->_rtFormat, D3DPOOL_DEFAULT, _rtBlur, _rtsBlur, true);
    if (err != D3D_OK) {_engine->DDError9("Error: Render target texture creation failed.", err); return false;}
  }
  #endif

  _toBlur = 0;
  // Success
  return true;
}

// perform blur on specified texture (toBlur) instead back buffer
void EngineDD9::PPGaussianBlur::Do(bool isLast, IDirect3DTexture9 *toBlur)
{
  Assert(_toBlur == 0);
  _toBlur = toBlur; // set blur source
  Do(isLast);
  _toBlur = 0;  // remove
}

void EngineDD9::PPGaussianBlur::Do(bool isLast)
{
  // Draw
  PROFILE_SCOPE_GRF_EX(ppGSB,pp,SCOPE_COLOR_YELLOW);

  // If scene is not being rendered initialize scene
  bool inScene = _engine->_d3dFrameOpen;
  if (!inScene) _engine->D3DBeginScene();

  // Prepare drawing
  Prepare();

  const int cb = -1;
  
  // Perform blur in 2 steps
  {
    // Protect VS constants from changing
    PushVertexShaderConstant pushC0(0);
    PushVertexShaderConstant pushC1(1);
    PushVertexShaderConstant pushC2(2);
    PushVertexShaderConstant pushC3(3);
    PushVertexShaderConstant pushC4(4);
    PushVertexShaderConstant pushC5(5);
    PushVertexShaderConstant pushC6(6);
    PushVertexShaderConstant pushC7(7);
    PushVertexShaderConstant pushC8(8);

    // Prepare the UV coordinates offset coefficients
    static const float baseU[4] = {0, 0, 1, 1};
    static const float baseV[4] = {1, 0, 1, 0};
    int fullWidth = _engine->Width();
    int fullHeight = _engine->Height();
    int partWidth = fullWidth / GBSizeDivisor;
    int partHeight = fullHeight / GBSizeDivisor;

    // No posOffset needed
    const float posOffset[4] = {0, 0, 0, 0};
    _engine->SetVertexShaderConstantF(cb,8, posOffset, 1);

    IDirect3DTexture9 *rt = (_toBlur == 0) ? _engine->CreateRenderTargetCopyAsTexture() : _toBlur;

    // Set temporarily render target and depth stencil to horizontally shrunk texture
    _engine->SetTemporaryRenderTarget(_rtHorizontal, _rtsHorizontal, NULL);

    // We need to set the viewport as it was set to previous values inside the SetTemporaryRenderTarget.
    // We cannot use the SetDepthRange method here, because we need to use custom Width and Height
    {
      D3DVIEWPORT9 viewData;
      memset(&viewData,0,sizeof(viewData));
      viewData.X = 0;
      viewData.Y = 0;
      viewData.Width  = partWidth;
      viewData.Height = fullHeight;
      viewData.MinZ = 0.0f;
      viewData.MaxZ = 1.0f;
      CALL_D3D_MEMBER(_engine,SetViewport,viewData);
    }

    // Set temporary render states
    SetRenderStateTemp rsCOLORWRITEENABLE(D3DRS_COLORWRITEENABLE, D3DCOLORWRITEENABLE_RED | D3DCOLORWRITEENABLE_GREEN | D3DCOLORWRITEENABLE_BLUE | D3DCOLORWRITEENABLE_ALPHA);
#ifndef _X360SHADERGEN

    // Use SRGB conversion in texture read
    SetSamplerStateTemp ssSRGBTEXTURE(0, D3DSAMP_SRGBTEXTURE, _engine->_caps._rtIsSRGB);

    // Write in the same space as you read // _VBS3_TI
    SetRenderStateTemp rsSRGBSRGBWRITEENABLE(D3DRS_SRGBWRITEENABLE, _engine->_caps._rtIsSRGB);

#endif

    // Use linear sampling and clamping for the texture on stage 0
    SetSamplerStateTemp ss0MINFILTER(0, D3DSAMP_MINFILTER, D3DTEXF_LINEAR);
    SetSamplerStateTemp ss0MAGFILTER(0, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR);
    SetSamplerStateTemp ss0ADDRESSU(0, D3DSAMP_ADDRESSU, D3DTADDRESS_CLAMP);
    SetSamplerStateTemp ss0ADDRESSV(0, D3DSAMP_ADDRESSV, D3DTADDRESS_CLAMP);

    // First step - render horizontally shrunk texture
    {
      // Prepare the UV coordinates offsets
      const float srcOffsetU = 0.5f / partWidth;
      const float srcOffsetV = 0.5f / fullHeight;
      float U[4][4];
      float V[4][4];
      for (int vertex = 0; vertex < 4; vertex++)
      {
        // Texture 0 - source image
        U[vertex][0] = baseU[vertex] + srcOffsetU;
        V[vertex][0] = baseV[vertex] + srcOffsetV;
        // Rest
        U[vertex][1] = U[vertex][2] = U[vertex][3] = 0;
        V[vertex][1] = V[vertex][2] = V[vertex][3] = 0;
      }
      _engine->SetVertexShaderConstantF(cb, 0, U[0], 4);
      _engine->SetVertexShaderConstantF(cb, 4, V[0], 4);

      // Set the pixel shader for final blur
      _engine->SetPixelShader(cb, _psHorizontal);

      // Set current render target as temporary texture (we know we don't render into it)
      SetTextureTemp tTex0(0, rt);

      // Prepare PS constants
      float invW = 1.0f / _engine->Width();
      SetPixelShaderConstantTemp spc0(0, D3DXVECTOR4(invW, invW, invW, invW));

      // Drawing
      _engine->DrawPrimitive(cb, D3DPT_TRIANGLESTRIP, 0, 2);

#ifdef _XBOX
      // Resolve content of the render target
      CALL_D3D_MEMBER(_engine,Resolve,D3DRESOLVE_RENDERTARGET0|D3DRESOLVE_ALLFRAGMENTS, NULL, _rtHorizontal, NULL, 0, 0, NULL, 1.0f, 0L, NULL);
#endif
    }

    // Rewrite render target and depth stencil surfaces by the blurred ones
    _engine->SetRenderTargetAndDepthStencil(_rtsBlur, _rtBlur, NULL);

    // Second step - render finally blurred texture from the horizontally shrunked one
    {
      // Prepare the UV coordinates offsets
      const float srcOffsetU = 0.5f / partWidth;
      const float srcOffsetV = 0.5f / partHeight;
      float U[4][4];
      float V[4][4];
      for (int vertex = 0; vertex < 4; vertex++)
      {
        // Texture 0 - source image
        U[vertex][0] = baseU[vertex] + srcOffsetU;
        V[vertex][0] = baseV[vertex] + srcOffsetV;
        // Rest
        U[vertex][1] = U[vertex][2] = U[vertex][3] = 0;
        V[vertex][1] = V[vertex][2] = V[vertex][3] = 0;
      }
      _engine->SetVertexShaderConstantF(cb, 0, U[0], 4);
      _engine->SetVertexShaderConstantF(cb, 4, V[0], 4);

      // Set the pixel shader for final blur
      _engine->SetPixelShader(cb, _psBlur);

      // Set current render target as temporary texture (we know we don't render into it)
      SetTextureTemp tTex0(0, _rtHorizontal);

      // Prepare PS constants
      float invH = 1.0f / _engine->Height();
      SetPixelShaderConstantTemp spc0(0, D3DXVECTOR4(invH, invH, invH, invH));

      // Drawing
      _engine->DrawPrimitive(cb, D3DPT_TRIANGLESTRIP, 0, 2);

#ifdef _XBOX
      // Resolve content of the render target
      CALL_D3D_MEMBER(_engine, Resolve, D3DRESOLVE_RENDERTARGET0|D3DRESOLVE_ALLFRAGMENTS, NULL, _rtBlur, NULL, 0, 0, NULL, 1.0f, 0L, NULL);
#endif
    }

    // Restore the original render target and depth stencil buffer
    _engine->RestoreOldRenderTarget();
  }

  // If scene is not being rendered finish initialized scene
  if (!inScene) _engine->D3DEndScene();

  // Restore usual rendering settings
  _engine->SwitchTL(cb,TLDisabled);
}

//////////////////////////////////////////////////////////////////////////

static const int BloomSizeDivisor = 8;

bool EngineDD9::PPBloom::InitCustomEdge(const LazyLoadFile &vs)
{
  // Create the VB
  ComRef<IDirect3DVertexBuffer9> vb;
  if (FAILED(_engine->GetDirect3DDevice()->CreateVertexBuffer(sizeof(VertexCustomEdge) * 4, D3DUSAGE_WRITEONLY, 0, D3DPOOL_DEFAULT, vb.Init(), NULL)))
  {
    LogF("Error: Vertex buffer creation failed.");
    return false;
  }
  void *data;
  vb->Lock(0, 0, &data, 0);
  VertexCustomEdge *v = (VertexCustomEdge *)data;
  v[0].pos = Vector3P(-1.0, -1.0, 1.0); v[0].index = 0;
  v[1].pos = Vector3P(-1.0,  1.0, 1.0); v[1].index = 1;
  v[2].pos = Vector3P( 1.0, -1.0, 1.0); v[2].index = 2;
  v[3].pos = Vector3P( 1.0,  1.0, 1.0); v[3].index = 3;
  vb->Unlock();

  _vbCustomEdge = VertexStaticData9::BufferFuture(vb);

  // Create the VD
  const D3DVERTEXELEMENT9 vertexDeclCustomEdge[] =
  {
    {0,  0, D3DDECLTYPE_FLOAT3,   D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION, 0},
    {0, 12, D3DDECLTYPE_FLOAT1,   D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_BLENDINDICES, 0},
    D3DDECL_END()
  };
  if (FAILED(_engine->GetDirect3DDevice()->CreateVertexDeclaration(vertexDeclCustomEdge, _vdCustomEdge.Init())))
  {
    LogF("Error: Vertex declaration creation failed.");
    return false;
  }

  // Create the VS
  if (!CompileVertexShader(vs,"VSPostProcessCustomEdge4T", _vsCustomEdge))
  {
    return false;
  }

  // Create horizontal blur and final blur render targets
#ifndef _X360SHADERGEN
  {
    int fullWidth = _engine->Width();
    int fullHeight = _engine->Height();
    int partWidth = fullWidth / BloomSizeDivisor;
    int partHeight = fullHeight / BloomSizeDivisor;
    HRESULT err;
    err = _engine->CreateTextureRT(fullWidth/2, fullHeight/2, 1, _engine->_rtFormat, D3DPOOL_DEFAULT, _rtBright, _rtsBright, true);
    if (err != D3D_OK) {_engine->DDError9("Error: Render target texture creation failed.", err); return false;}
    err = _engine->CreateTextureRT(partWidth, partHeight, 1, _engine->_rtFormat, D3DPOOL_DEFAULT, _rtHorizontal, _rtsHorizontal, true);
    if (err != D3D_OK) {_engine->DDError9("Error: Render target texture creation failed.", err); return false;}
    err = _engine->CreateTextureRT(partWidth, partHeight, 1, _engine->_rtFormat, D3DPOOL_DEFAULT, _rtBlur, _rtsBlur, true);
    if (err != D3D_OK) {_engine->DDError9("Error: Render target texture creation failed.", err); return false;}
  }
#endif

  // Success
  return true;
}

int EngineDD9::PPBloom::SetParams(const float *pars, int nPars)
{
  if(nPars > 0)
    _threshold = pars[0];

  if(nPars > 1)
    _blurScale = pars[1];

  if(nPars > 2)
    _imageScale = pars[2];

  return 0;
}

bool EngineDD9::PPBloom::Init(const LazyLoadFile &hlsl, const LazyLoadFile &vs)
{
  if (!base::Init(hlsl,vs)) return false;

  _imageScale = 0.8f;
  _blurScale = 0.25f;
  _threshold = 0.0f;

  // Create custom edge vertex resources
  if (!InitCustomEdge(vs)) return false;

  // Create pixel shaders
  {
    if (!CompilePixelShader(hlsl,"PSPostProcessGaussBlur", _psBlur, true)) return false;
    if (!CompilePixelShader(hlsl,"PSPostProcessBloomDownsample2", _psDownsample2, true)) return false;
    if (!CompilePixelShader(hlsl,"PSPostProcessBloomDownsample4", _psDownsample4, true)) return false;
    if (!CompilePixelShader(hlsl,"PSPostProcessBloomCombine", _psCombine, true)) return false;
  }

  // Success
  return true;
}


float ComputeGaussianValue( float x, float mean, float std_deviation )
{
  // The gaussian equation is defined as such:
  /*    
  -(x - mean)^2
  -------------
  1.0               2*std_dev^2
  f(x,mean,std_dev) = -------------------- * e^
  sqrt(2*pi*std_dev^2)

  */

  return ( 1.0f / sqrt( 2.0f * 3.14159265f * std_deviation * std_deviation ) ) 
    * expf( (-((x-mean)*(x-mean)))/(2.0f * std_deviation * std_deviation) );
}

const float GAUSS_MEAN = 0.0f;
const float GAUSS_DEVIATION = 0.5f;
const int KERNEL_SIZE = 9;

typedef float Vec4[4];

static void GaussKernel(Vec4* hkernel, Vec4* vkernel, int kernelSize, int width, int height)
{
  float	biwidth = 1.0f / (float)width;
  float biheight = 1.0f / (float)height;
  float hsize = (float)(kernelSize - 1) * 0.5f;


  for(int i = 0; i < kernelSize; i++ )
  {
    float f = ((float)i - hsize) / hsize;
    float gauss = ComputeGaussianValue( f, GAUSS_MEAN, GAUSS_DEVIATION );
    hkernel[i][0] = ((float)i - hsize) * biwidth;
    hkernel[i][1] = 0.0f;
    hkernel[i][2] = 0.0f;
    hkernel[i][3] = gauss;

    vkernel[i][0] = 0.0f;
    vkernel[i][1] = ((float)i - hsize) * biheight;
    vkernel[i][2] = 0.0f;
    vkernel[i][3] = gauss;
  }

  // the kernel needs to be normalized, so that we do not change image brightness
  float kernelSum = 0;
  for (int i=0; i<kernelSize; i++)
  {
    kernelSum += hkernel[i][3];
  }
  float normF = 1.0f/kernelSum;
  for (int i=0; i<kernelSize; i++)
  {
    hkernel[i][3] *= normF;
    vkernel[i][3] = hkernel[i][3];
  }
}

void EngineDD9::PPBloom::Do(bool isLast)
{ 
  // If scene is not being rendered initialize scene
  bool inScene = _engine->_d3dFrameOpen;
  if (!inScene) _engine->D3DBeginScene();

  // Prepare drawing
  Prepare();
  
  // Remember the current render target
  IDirect3DTexture9 *rt = _engine->CreateRenderTargetCopyAsTexture();
  Do(isLast,rt);

  // If scene is not being rendered finish initialized scene
  if (!inScene) _engine->D3DEndScene();

  // Restore usual rendering settings
  Finish();
}

void EngineDD9::PPBloom::Do(bool isLast, IDirect3DTexture9 *rt)
{
  // Draw
  PROFILE_SCOPE_GRF_EX(ppBlo,pp,SCOPE_COLOR_YELLOW);

  const int cb = -1;
  
  // Protect VS constants from changing
  PushVertexShaderConstant pushC0(0);
  PushVertexShaderConstant pushC1(1);
  PushVertexShaderConstant pushC2(2);
  PushVertexShaderConstant pushC3(3);
  PushVertexShaderConstant pushC4(4);
  PushVertexShaderConstant pushC5(5);
  PushVertexShaderConstant pushC6(6);
  PushVertexShaderConstant pushC7(7);
  PushVertexShaderConstant pushC8(8);

  PushPixelShaderConstant ppushC0(0);
  PushPixelShaderConstant ppushC1(1);
  PushPixelShaderConstant ppushC2(2);
  PushPixelShaderConstant ppushC3(3);
  PushPixelShaderConstant ppushC4(4);
  PushPixelShaderConstant ppushC5(5);
  PushPixelShaderConstant ppushC6(6);
  PushPixelShaderConstant ppushC7(7);
  PushPixelShaderConstant ppushC8(8);
  PushPixelShaderConstant ppushC9(9);
  PushPixelShaderConstant ppushC10(10);
  PushPixelShaderConstant ppushC11(11);
  PushPixelShaderConstant ppushC12(12);
  PushPixelShaderConstant ppushC13(13);
  PushPixelShaderConstant ppushC14(14);
  PushPixelShaderConstant ppushC15(15);

  // Use clamping for the textures
  SetSamplerStateTemp ss0(0, D3DSAMP_ADDRESSU, D3DTADDRESS_CLAMP);
  SetSamplerStateTemp ss1(0, D3DSAMP_ADDRESSV, D3DTADDRESS_CLAMP);
  SetSamplerStateTemp ss2(1, D3DSAMP_ADDRESSU, D3DTADDRESS_CLAMP);
  SetSamplerStateTemp ss3(1, D3DSAMP_ADDRESSV, D3DTADDRESS_CLAMP);

  float scales[16][4];
  int fullWidth = _engine->Width();
  int fullHeight = _engine->Height();
  int brightWidth = fullWidth / 2;
  int brightHeight = fullHeight / 2;
  int partWidth = fullWidth / BloomSizeDivisor;
  int partHeight = fullHeight / BloomSizeDivisor;

  {
    // Set the vertex shader + stream source
    _engine->SetStreamSource(cb, _vbCustomEdge, sizeof(VertexCustomEdge), 4);
    _engine->SetVertexDeclaration(cb, _vdCustomEdge);
    _engine->SetVertexShader(cb, _vsCustomEdge);

    // Prepare the UV coordinates offset coefficients
    static const float baseU[4] = {0, 0, 1, 1};
    static const float baseV[4] = {1, 0, 1, 0};

    // Set temporarily render target and depth stencil to 2x2 shrunk texture
    ComRef<IDirect3DSurface9> zeroDepth;
    _engine->SetTemporaryRenderTarget(_rtBright, _rtsBright, zeroDepth);

    // We need to set the viewport as it was set to previous values inside the SetTemporaryRenderTarget.
    // We cannot use the SetDepthRange method here, because we need to use custom Width and Height
    {
      D3DVIEWPORT9 viewData;
      memset(&viewData,0,sizeof(viewData));
      viewData.X = 0;
      viewData.Y = 0;
      viewData.Width  = brightWidth;
      viewData.Height = brightHeight;
      viewData.MinZ = 0.0f;
      viewData.MaxZ = 1.0f;
      CALL_D3D_MEMBER(_engine,SetViewport,viewData);
    }

    // Set temporary render states
    SetRenderStateTemp rsCOLORWRITEENABLE(D3DRS_COLORWRITEENABLE, D3DCOLORWRITEENABLE_RED | D3DCOLORWRITEENABLE_GREEN | D3DCOLORWRITEENABLE_BLUE | D3DCOLORWRITEENABLE_ALPHA);
#ifndef _X360SHADERGEN

    // Use SRGB conversion in texture read
    SetSamplerStateTemp ssSRGBTEXTURE0(0, D3DSAMP_SRGBTEXTURE, _engine->_caps._rtIsSRGB);
    SetSamplerStateTemp ssSRGBTEXTURE1(0, D3DSAMP_SRGBTEXTURE, FALSE);

    // Write in the same space as you read // _VBS3_TI
    SetRenderStateTemp rsSRGBSRGBWRITEENABLE(D3DRS_SRGBWRITEENABLE, _engine->_caps._rtIsSRGB);

#endif

    // First step - render bright pass
    {
      // Use point sampling for the textures
      SetSamplerStateTemp ss0MINFILTER(0, D3DSAMP_MINFILTER, D3DTEXF_POINT);
      SetSamplerStateTemp ss0MAGFILTER(0, D3DSAMP_MAGFILTER, D3DTEXF_POINT);
      SetSamplerStateTemp ss1MINFILTER(1, D3DSAMP_MINFILTER, D3DTEXF_POINT);
      SetSamplerStateTemp ss1MAGFILTER(1, D3DSAMP_MAGFILTER, D3DTEXF_POINT);

      // Prepare the UV coordinates offsets
      const float srcOffsetU = 0.5f / fullWidth;
      const float srcOffsetV = 0.5f / fullHeight;
      float U[4][4];
      float V[4][4];
      for (int vertex = 0; vertex < 4; vertex++)
      {
        // Texture 0 - source image
        U[vertex][0] = baseU[vertex] + srcOffsetU;
        V[vertex][0] = baseV[vertex] + srcOffsetV;
        // Rest
        U[vertex][1] = U[vertex][2] = U[vertex][3] = 0;
        V[vertex][1] = V[vertex][2] = V[vertex][3] = 0;
      }
      _engine->SetVertexShaderConstantF(cb, 0, U[0], 4);
      _engine->SetVertexShaderConstantF(cb, 4, V[0], 4);
      
      // No posOffset needed
      const float posOffset[4] = {0, 0, 0, 0};
      _engine->SetVertexShaderConstantF(cb, 8, posOffset, 1);

      // Set the pixel shader for bright pass
      _engine->SetPixelShader(cb, _psDownsample2);

      // Set current render target as temporary texture (we know we don't render into it)
      SetTextureTemp tTex0(0, rt);
      SetTextureTemp tTex2(1, _engine->_pp._glow->GetApertureTexture()); // aperture control texture
    //	SetTextureTemp tTex1(1, ((TextureD3D9*)_gammaTexture.GetRef())->GetHandle());

      // Prepare PS constants - PSSceneOffsets2;
      int index = 0;
      for(int y = -1; y < 1; y++)
      {
        for(int x = -1; x < 1; x++)
        {
          scales[index][0] = (1.0f / (float)fullWidth) * ((float)y + 0.5f);
          scales[index][1] = (1.0f / (float)fullHeight) * ((float)x + 0.5f);
          scales[index][2] = 0;
          scales[index][3] = 0;
          index++;
        }
      }
      _engine->SetPixelShaderConstantF(cb, 0, scales[0], 4);
      float treshold[4];
      treshold[0] = treshold[1] = treshold[2] = treshold[3] = _threshold;
      _engine->SetPixelShaderConstantF(cb, 4, treshold, 1);



      // Drawing
      _engine->DrawPrimitive(cb, D3DPT_TRIANGLESTRIP, 0, 2);

#ifdef _XBOX
      // Resolve content of the render target
      CALL_D3D_MEMBER(_engine,Resolve,D3DRESOLVE_RENDERTARGET0|D3DRESOLVE_ALLFRAGMENTS, NULL, _rtBright, NULL, 0, 0, NULL, 1.0f, 0L, NULL);
#endif
    }

    // Second step - 4x4 downscale
    {
      ComRef<IDirect3DSurface9> zeroDepth;
      _engine->SetRenderTargetAndDepthStencil(_rtsBlur, _rtBlur, zeroDepth);

      // We need to set the viewport as it was set to previous values inside the SetTemporaryRenderTarget.
      // We cannot use the SetDepthRange method here, because we need to use custom Width and Height
      {
        D3DVIEWPORT9 viewData;
        memset(&viewData,0,sizeof(viewData));
        viewData.X = 0;
        viewData.Y = 0;
        viewData.Width  = partWidth;
        viewData.Height = partHeight;
        viewData.MinZ = 0.0f;
        viewData.MaxZ = 1.0f;
        CALL_D3D_MEMBER(_engine,SetViewport,viewData);
      }

      // Use point sampling for the textures
      SetSamplerStateTemp ss0MINFILTER(0, D3DSAMP_MINFILTER, D3DTEXF_POINT);
      SetSamplerStateTemp ss0MAGFILTER(0, D3DSAMP_MAGFILTER, D3DTEXF_POINT);

      // Prepare the UV coordinates offsets
      const float srcOffsetU = 0.5f / brightWidth;
      const float srcOffsetV = 0.5f / brightHeight;
      float U[4][4];
      float V[4][4];
      for (int vertex = 0; vertex < 4; vertex++)
      {
        // Texture 0 - source image
        U[vertex][0] = baseU[vertex] + srcOffsetU;
        V[vertex][0] = baseV[vertex] + srcOffsetV;
        // Rest
        U[vertex][1] = U[vertex][2] = U[vertex][3] = 0;
        V[vertex][1] = V[vertex][2] = V[vertex][3] = 0;
      }
      _engine->SetVertexShaderConstantF(cb, 0, U[0], 4);
      _engine->SetVertexShaderConstantF(cb, 4, V[0], 4);
      // No posOffset needed
      const float posOffset[4] = {0, 0, 0, 0};
      _engine->SetVertexShaderConstantF(cb, 8, posOffset, 1);

      // Set the pixel shader for downsample pass
      _engine->SetPixelShader(cb, _psDownsample4);

      // Set current render target as temporary texture (we know we don't render into it)
      SetTextureTemp tTex0(0, _rtBright);

      // Prepare PS constants - PSSceneOffsets4;
      int index = 0;
      index = 0;
      for(int y = -2; y < 2; y++)
      {
        for(int x = -2; x < 2; x++)
        {
          scales[index][0] = (1.0f / (float)brightWidth) * ((float)x + 0.5f);
          scales[index][1] = (1.0f / (float)brightHeight) * ((float)y + 0.5f);
          scales[index][2] = 0.0f;
          scales[index][3] = 0.0f;
          index++;
        }
      }
      _engine->SetPixelShaderConstantF(cb, 0, scales[0], 16);

      // Drawing
      _engine->DrawPrimitive(cb, D3DPT_TRIANGLESTRIP, 0, 2);

#ifdef _XBOX
      // Resolve content of the render target
      CALL_D3D_MEMBER(_engine,Resolve,D3DRESOLVE_RENDERTARGET0|D3DRESOLVE_ALLFRAGMENTS, NULL, _rtBlur, NULL, 0, 0, NULL, 1.0f, 0L, NULL);
#endif
    }

    Vec4 hkernel[KERNEL_SIZE];
    Vec4 vkernel[KERNEL_SIZE];

    GaussKernel(hkernel, vkernel, KERNEL_SIZE, partWidth, partHeight);

    // Third step - horizontal blur
    {
      ComRef<IDirect3DSurface9> zeroDepth;
      _engine->SetRenderTargetAndDepthStencil(_rtsHorizontal, _rtHorizontal, zeroDepth);

      // We need to set the viewport as it was set to previous values inside the SetTemporaryRenderTarget.
      // We cannot use the SetDepthRange method here, because we need to use custom Width and Height
      {
        D3DVIEWPORT9 viewData;
        memset(&viewData,0,sizeof(viewData));
        viewData.X = 0;
        viewData.Y = 0;
        viewData.Width  = partWidth;
        viewData.Height = partHeight;
        viewData.MinZ = 0.0f;
        viewData.MaxZ = 1.0f;
        CALL_D3D_MEMBER(_engine,SetViewport,viewData);
      }

      // Use point sampling for the textures
      SetSamplerStateTemp ss0MINFILTER(0, D3DSAMP_MINFILTER, D3DTEXF_POINT);
      SetSamplerStateTemp ss0MAGFILTER(0, D3DSAMP_MAGFILTER, D3DTEXF_POINT);

      // Prepare the UV coordinates offsets
      const float srcOffsetU = 0.5f / partWidth;
      const float srcOffsetV = 0.5f / partHeight;
      float U[4][4];
      float V[4][4];
      for (int vertex = 0; vertex < 4; vertex++)
      {
        // Texture 0 - source image
        U[vertex][0] = baseU[vertex] + srcOffsetU;
        V[vertex][0] = baseV[vertex] + srcOffsetV;
        // Rest
        U[vertex][1] = U[vertex][2] = U[vertex][3] = 0;
        V[vertex][1] = V[vertex][2] = V[vertex][3] = 0;
      }
      _engine->SetVertexShaderConstantF(cb, 0, U[0], 4);
      _engine->SetVertexShaderConstantF(cb, 4, V[0], 4);
      // No posOffset needed
      const float posOffset[4] = {0, 0, 0, 0};
      _engine->SetVertexShaderConstantF(cb, 8, posOffset, 1);

      // Set the pixel shader for blur pass
      _engine->SetPixelShader(cb, _psBlur);

      // Set current render target as temporary texture (we know we don't render into it)
      SetTextureTemp tTex0(0, _rtBlur);

      _engine->SetPixelShaderConstantF(cb, 0, hkernel[0], KERNEL_SIZE);

      // Drawing
      _engine->DrawPrimitive(cb, D3DPT_TRIANGLESTRIP, 0, 2);

#ifdef _XBOX
      // Resolve content of the render target
      CALL_D3D_MEMBER(_engine,Resolve,D3DRESOLVE_RENDERTARGET0|D3DRESOLVE_ALLFRAGMENTS, NULL, _rtHorizontal, NULL, 0, 0, NULL, 1.0f, 0L, NULL);
#endif
    }

    // Fourth step - vertical blur
    {
      ComRef<IDirect3DSurface9> zeroDepth;
      _engine->SetRenderTargetAndDepthStencil(_rtsBlur, _rtBlur, zeroDepth);

      // We need to set the viewport as it was set to previous values inside the SetTemporaryRenderTarget.
      // We cannot use the SetDepthRange method here, because we need to use custom Width and Height
      {
        D3DVIEWPORT9 viewData;
        memset(&viewData,0,sizeof(viewData));
        viewData.X = 0;
        viewData.Y = 0;
        viewData.Width  = partWidth;
        viewData.Height = partHeight;
        viewData.MinZ = 0.0f;
        viewData.MaxZ = 1.0f;
        CALL_D3D_MEMBER(_engine,SetViewport,viewData);
      }

      // Use point sampling for the textures
      SetSamplerStateTemp ss0MINFILTER(0, D3DSAMP_MINFILTER, D3DTEXF_POINT);
      SetSamplerStateTemp ss0MAGFILTER(0, D3DSAMP_MAGFILTER, D3DTEXF_POINT);

      // Set current render target as temporary texture (we know we don't render into it)
      SetTextureTemp tTex0(0, _rtHorizontal);

      _engine->SetPixelShaderConstantF(cb, 0, vkernel[0], KERNEL_SIZE);

      // Drawing
      _engine->DrawPrimitive(cb, D3DPT_TRIANGLESTRIP, 0, 2);

#ifdef _XBOX
      // Resolve content of the render target
      CALL_D3D_MEMBER(_engine,Resolve,D3DRESOLVE_RENDERTARGET0|D3DRESOLVE_ALLFRAGMENTS, NULL, _rtBlur, NULL, 0, 0, NULL, 1.0f, 0L, NULL);
#endif
    }

    _engine->RestoreOldRenderTarget();

    // Fifth step - combine blurred image and the original one
    if(isLast)
    {
      SetTextureTemp tTex0(0, rt);
      SetTextureTemp tTex1(1, _rtBlur);

      // Use point sampling for the textures
      SetSamplerStateTemp ss0MINFILTER(0, D3DSAMP_MINFILTER, D3DTEXF_POINT);
      SetSamplerStateTemp ss0MAGFILTER(0, D3DSAMP_MAGFILTER, D3DTEXF_POINT);
      SetSamplerStateTemp ss1MINFILTER(1, D3DSAMP_MINFILTER, D3DTEXF_LINEAR);
      SetSamplerStateTemp ss1MAGFILTER(1, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR);

      // Prepare the UV coordinates offsets
      const float srcOffsetU1 = 0.5f / partWidth;
      const float srcOffsetV1 = 0.5f / partHeight;
      const float srcOffsetU2 = 0.5f / fullWidth;
      const float srcOffsetV2 = 0.5f / fullHeight;
      float U[4][4];
      float V[4][4];
      for (int vertex = 0; vertex < 4; vertex++)
      {
        // Texture 0 - blurred image
        U[vertex][0] = baseU[vertex] + srcOffsetU1;
        V[vertex][0] = baseV[vertex] + srcOffsetV1;
        // Texture 1 - source image
        U[vertex][1] = baseU[vertex] + srcOffsetU2;
        V[vertex][1] = baseV[vertex] + srcOffsetV2;
        // Rest
        U[vertex][2] = U[vertex][3] = 0;
        V[vertex][2] = V[vertex][3] = 0;
      }
      _engine->SetVertexShaderConstantF(cb, 0, U[0], 4);
      _engine->SetVertexShaderConstantF(cb, 4, V[0], 4);

      Vec4 scales[2];
      scales[0][0] = scales[0][1] = scales[0][2] = scales[0][3] = _blurScale;
      scales[1][0] = scales[1][1] = scales[1][2] = scales[1][3] = _imageScale;
      _engine->SetPixelShaderConstantF(cb, 0, scales[0], 2);
      
      // Set the pixel shader for combine pass
      _engine->SetPixelShader(cb, _psCombine);

      // Drawing
      _engine->DrawPrimitive(cb, D3DPT_TRIANGLESTRIP, 0, 2);
    }
  }

}

//////////////////////////////////////////////////////////////////////////
bool EngineDD9::PPRain3D::Init(PPResources *resources, bool firstInit)
{
  _textureReady = false;
  _rainStrength = 0.0f;
  _currentOccluder = 0.0f;
  _targetOccluder = 0.0f;

  return true;
}

//////////////////////////////////////////////////////////////////////////
bool EngineDD9::PPRain3D::Init(const LazyLoadFile &hlsl, const LazyLoadFile &vs)
{
  if (!base::Init(hlsl,vs)) return false;

  _textureReady = false;
  
  _resrc = new PPRain3DResources;
  _rainStrength = 0.0f;
  _currentOccluder = 0.0f;
  _targetOccluder = 0.0f;

  //not initialized yet
  if(GEngine == NULL)
    GEngine = _engine;

  // Create the VB
  ComRef<IDirect3DVertexBuffer9> vb;
  if (FAILED(_engine->GetDirect3DDevice()->CreateVertexBuffer(sizeof(Vertex) * numLayers * 4 * 3 * 2, D3DUSAGE_WRITEONLY, 0, D3DPOOL_DEFAULT, vb.Init(), NULL)))
  {
    LogF("Error: Vertex buffer creation failed.");
    return false;
  }

  void *data;
  vb->Lock(0, 0, &data, 0);
  Vertex *verts = (Vertex *)data;

  float voff = 0;
  for(int b = 0; b < numLayers; b++)
  {
    Vector3 v0, v1, v2, v3;

    float layer = b / (float)(numLayers - 1);
    //we must go from largest to smallest, because of alpha blending
    layer = 1.0f - layer;
    //X+
		v0 = Vector3( 1.0f, -1.0f, -1.0f);
		v1 = Vector3( 1.0f,  1.0f, -1.0f);
		v2 = Vector3( 1.0f,  1.0f,  1.0f);
		v3 = Vector3( 1.0f, -1.0f,  1.0f);
    verts[0].Copy(v0);
    verts[0].u = -1.0f;
    verts[0].v = -1.0f + voff;
    verts[0].layer = layer;
    verts[1].Copy(v1);
    verts[1].u =  1.0f;
    verts[1].v = -1.0f + voff;
    verts[1].layer = layer;
    verts[2].Copy(v2);
    verts[2].u =  1.0f;
    verts[2].v =  1.0f + voff;
    verts[2].layer = layer;
    verts[3].Copy(v0);
    verts[3].u = -1.0f;
    verts[3].v = -1.0f + voff;
    verts[3].layer = layer;
    verts[4].Copy(v2);
    verts[4].u =  1.0f;
    verts[4].v =  1.0f + voff;
    verts[4].layer = layer;
    verts[5].Copy(v3);
    verts[5].u = -1.0f;
    verts[5].v =  1.0f + voff;
    verts[5].layer = layer;
    verts += 6;

    //X-
	  v0 = Vector3(-1.0f, -1.0f, -1.0f);
	  v1 = Vector3(-1.0f,  1.0f, -1.0f);
	  v2 = Vector3(-1.0f,  1.0f,  1.0f);
	  v3 = Vector3(-1.0f, -1.0f,  1.0f);
    verts[0].Copy(v0);
    verts[0].u =  1.0f;
    verts[0].v = -1.0f + voff;
    verts[0].layer = layer;
    verts[1].Copy(v1);
    verts[1].u = -1.0f;
    verts[1].v = -1.0f + voff;
    verts[1].layer = layer;
    verts[2].Copy(v2);
    verts[2].u = -1.0f;
    verts[2].v =  1.0f + voff;
    verts[2].layer = layer;
    verts[3].Copy(v0);
    verts[3].u =  1.0f;
    verts[3].v = -1.0f + voff;
    verts[3].layer = layer;
    verts[4].Copy(v2);
    verts[4].u = -1.0f;
    verts[4].v =  1.0f + voff;
    verts[4].layer = layer;
    verts[5].Copy(v3);
    verts[5].u =  1.0f;
    verts[5].v =  1.0f + voff;
    verts[5].layer = layer;
    verts += 6;

    //Z+
		v0 = Vector3(-1.0f,  1.0f, -1.0f);
		v1 = Vector3( 1.0f,  1.0f, -1.0f);
		v2 = Vector3( 1.0f,  1.0f,  1.0f);
		v3 = Vector3(-1.0f,  1.0f,  1.0f);
    verts[0].Copy(v0);
    verts[0].u =  1.0f;
    verts[0].v = -1.0f + voff;
    verts[0].layer = layer;
    verts[1].Copy(v1);
    verts[1].u = -1.0f;
    verts[1].v = -1.0f + voff;
    verts[1].layer = layer;
    verts[2].Copy(v2);
    verts[2].u = -1.0f;
    verts[2].v =  1.0f + voff;
    verts[2].layer = layer;
    verts[3].Copy(v0);
    verts[3].u =  1.0f;
    verts[3].v = -1.0f + voff;
    verts[3].layer = layer;
    verts[4].Copy(v2);
    verts[4].u = -1.0f;
    verts[4].v =  1.0f + voff;
    verts[4].layer = layer;
    verts[5].Copy(v3);
    verts[5].u =  1.0f;
    verts[5].v =  1.0f + voff;
    verts[5].layer = layer;
    verts += 6;

    //Z-
		v0 = Vector3(-1.0f, -1.0f, -1.0f);
		v1 = Vector3( 1.0f, -1.0f, -1.0f);
		v2 = Vector3( 1.0f, -1.0f,  1.0f);
		v3 = Vector3(-1.0f, -1.0f,  1.0f);
    verts[0].Copy(v0);
    verts[0].u = -1.0f;
    verts[0].v = -1.0f + voff;
    verts[0].layer = layer;
    verts[1].Copy(v1);
    verts[1].u =  1.0f;
    verts[1].v = -1.0f + voff;
    verts[1].layer = layer;
    verts[2].Copy(v2);
    verts[2].u =  1.0f;
    verts[2].v =  1.0f + voff;
    verts[2].layer = layer;
    verts[3].Copy(v0);
    verts[3].u = -1.0f;
    verts[3].v = -1.0f + voff;
    verts[3].layer = layer;
    verts[4].Copy(v2);
    verts[4].u =  1.0f;
    verts[4].v =  1.0f + voff;
    verts[4].layer = layer;
    verts[5].Copy(v3);
    verts[5].u = -1.0f;
    verts[5].v =  1.0f + voff;
    verts[5].layer = layer;
    verts += 6;
    voff += 0.25f;
  }

  vb->Unlock();

  _resrc->_vbRainGeometry = VertexStaticData9::BufferFuture(vb);

  // Create the VD
  const D3DVERTEXELEMENT9 vertexDeclCustomEdge[] =
  {
    {0,  0, D3DDECLTYPE_FLOAT3,   D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION, 0},
    {0, 12, D3DDECLTYPE_FLOAT3,   D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 0},
    D3DDECL_END()
  };
  if (FAILED(_engine->GetDirect3DDevice()->CreateVertexDeclaration(vertexDeclCustomEdge, _resrc->_vdRain.Init())))
  {
    LogF("Error: Vertex declaration creation failed.");
    return false;
  }

  // Create shaders
  {
    if (!CompilePixelShader(hlsl,"PSPostProcessRain3D", _resrc->_psRain, true)) return false;
    if (!CompileVertexShader(hlsl,"VSPostProcessRain3D", _resrc->_vsRain)) return false;
  }

  // Success
  return true;
}

void EngineDD9::PPRain3D::SetTexture(TextureD3D9 *tex)
{
  _resrc->_rainTexture = tex;
  // no need to preload or make permanent - we can handle it async
  //if (_rainTexture) _rainTexture->PreloadTexture(1, 1, true);
}

void ConvertMatrixTransposed(PASSMATRIXTYPE &mat, Matrix4Val src);
void ConvertProjectionMatrix(D3DMATRIX &mat, Matrix4Val src, float zBias);

//#ifdef _PROFILE
//#pragma optimize("", off)
//#endif
void EngineDD9::PPRain3D::Prepare(float deltaT)
{
  if(_rainStrength == 0 || _resrc->_rainTexture.IsNull())
    return;

	//no data, no rain
	MipInfo info = _engine->TextBank()->UseMipmap(_resrc->_rainTexture, 0, 0);
	_textureReady = (info._texture == _resrc->_rainTexture && info._level==0);

  const Camera* camera = GScene->GetCamera();
  
  //trace above us, and find the "rain occluder"
  CollisionBuffer col;
  Object *camOn = GWorld->GetCameraEffect() ? NULL : GWorld->CameraOn();
  Object* obj = GWorld->GetCamInsideVehicle();
  VisualStateAge cameraAge = camera->GetAge();
  GScene->GetLandscape()->ObjectCollisionLine(cameraAge,col,Landscape::FilterIgnoreTwo(camOn,obj),camera->Position(),camera->Position() + Vector3(0,10,0),0.15f,ObjIntersectView);
  float crad = 0;
  if(col.Size() != 0)
  {
    for(int n = 0; n < col.Size(); n++)
    {
      const Object* cObject = col[n].object;
      // check the size of the component above us
      const ConvexComponents* comps = cObject->GetShape()->GetConvexComponents(col[n].geomLevel);
      if(comps)
      {
        const ConvexComponent* comp = (*comps)[col[n].component];
        if(comp)
        {
          // we are interested in horizontal radius only
          // the distincion is important for tall objects like trees
          float xzRad = comp->Min().DistanceXZ(comp->Max())*0.5f;
          crad = floatMax(crad, floatMin(comp->GetRadius(),xzRad));
        }
      }
    }
  }

  static float maxRadius = 25.0f;
  //don't make it too large, otherwise the rain is barely visible
  _targetOccluder = floatMin(crad, maxRadius);

  static float fadespeed = 10.0f;
  _currentOccluder = (_targetOccluder - _currentOccluder) * deltaT * fadespeed + _currentOccluder;
}

void EngineDD9::PPRain3D::Do(bool isLast)
{
  if(!_textureReady || _rainStrength == 0)
    return;

  // If scene is not being rendered initialize scene
  bool inScene = _engine->_d3dFrameOpen;
  if (!inScene) _engine->D3DBeginScene();

	Matrix4 viewWorld;
	const int cb = -1;

	PASSMATRIXTYPE viewWorldMatrix;

	//we are at camera position
	Matrix4 modelToWorldOffset;
  modelToWorldOffset.SetIdentity();
  //here we can set orientation of rain
  //modelToWorldOffset.SetOrientation(xxx);
	PushVertexShaderConstant vsc0(0);
	PushVertexShaderConstant vsc1(1);
	PushVertexShaderConstant vsc2(2);
	PushVertexShaderConstant vsc3(3);
	PushVertexShaderConstant vsc4(4);
	PushVertexShaderConstant vsc5(5);
	PushVertexShaderConstant vsc6(6);
	PushVertexShaderConstant vsc7(7);
	PushVertexShaderConstant vsc8(8);
	PushVertexShaderConstant vsc9(9);
	PushVertexShaderConstant vsc10(10);
	PushPixelShaderConstant psc0(0);
	PushPixelShaderConstant psc1(1);
	PushPixelShaderConstant psc2(2);
	PushPixelShaderConstant psc3(3);
  const Camera* camera = GScene->GetCamera();
  float intensity = GScene->MainLight()->GetAmbient().Brightness() + GScene->MainLight()->GetDiffuse().Brightness();

  //float eyeaccom = 0.006f;
  float eyeaccom = GWorld->GetEyeAccom();
  //limit this. We don't want to shine in dark
  eyeaccom = floatMin(eyeaccom, 0.05f);
  intensity *= eyeaccom;
  saturate(intensity, 0, 1);

  static float affectByLight = 0.8f;
  if(!_engine->GetNightVision())
    intensity = intensity * affectByLight + (1.0f - affectByLight);

	Matrix4 viewZero;
	viewZero.SetOrientation(camera->InverseScaled().Orientation());
	viewZero.SetPosition(VZero);
	viewWorld = viewZero * modelToWorldOffset;
  viewWorld.SetPosition(VZero);
	ConvertMatrixTransposed(viewWorldMatrix, viewWorld);
	_engine->SetVertexShaderConstantF(cb, 0, (float*)&viewWorldMatrix, 3);

	D3DXMATRIX projMatrix;
	ConvertProjectionMatrix(projMatrix, camera->ProjectionNormal(), 0.0f);

	_engine->SetVertexShaderConstantF(cb, 3, (float*)&projMatrix, 4);

  //static so we have opportunity to tune values during debugging
	static float shiftScale[4] = {0.0f, 0.0f, 2.0f, 0.5f};
  static float rainspeed = 2.0f;
	shiftScale[1] = Glob.time.toFloat() * rainspeed * shiftScale[3];
	_engine->SetVertexShaderConstantF(cb, 7, shiftScale, 1);

  static float minMax[2][4] = {
    {0.4f, 8.0f, 0.4f, 0.0f},
    {2.5f, 8.0f, 2.5f, 0.0f}
  };

  float minMax2[2][4];
  float radius = 0;
  //when we are inside some object, make the geometry bigger
  Object* obj = GWorld->GetCamInsideVehicle();
  if(obj)
  {
    Vector3 camMinMax[2];
    float lradius = obj->ClippingInfo(obj->RenderVisualState(),camMinMax,Object::ClipGeometry);
    //we must make it smaller, otherwise even player as a man has 2meters radius
    lradius = floatMax(lradius - 2.0f, 0.0f);
    radius += lradius;
  }

  radius += _currentOccluder;
  for(int m = 0; m < 2; m++)
  {
    for(int n = 0; n < 4; n++)
    {
      minMax2[m][n] = minMax[m][n] + radius;
    }
  }
  _engine->SetVertexShaderConstantF(cb, 8, minMax2[0], 2);

  float sealevel = GScene->GetLandscape()->GetSeaLevel(camera->Position().X(), camera->Position().Z()) - camera->Position().Y();

  float seaLevel[4] = {sealevel,sealevel, sealevel, sealevel};
  _engine->SetVertexShaderConstantF(cb, 10, seaLevel, 1);

	_engine->SetVertexDeclaration(cb, _resrc->_vdRain);
	_engine->SetVertexShader(cb, _resrc->_vsRain);

  //affect intensity by light - more during day, less during night
	float alpha[4] = {1.0f, 1.0f, 1.0f, _rainStrength * intensity};
  saturate(alpha[3], 0, 1);

	_engine->SetPixelShaderConstantF(cb, 0, alpha, 1);

	float screenRes[4] = {1.0f / (float)_engine->Width(), 1.0f / (float)_engine->Height(), 0.0f, 0.0f};
  _engine->SetPixelShaderConstantF(cb, 1, screenRes, 1);
	_engine->SetPixelShader(cb, _resrc->_psRain);

	SetRenderStateTemp rs0(D3DRS_COLORWRITEENABLE, D3DCOLORWRITEENABLE_RED | D3DCOLORWRITEENABLE_GREEN | D3DCOLORWRITEENABLE_BLUE | D3DCOLORWRITEENABLE_ALPHA);
	SetRenderStateTemp rs1(D3DRS_ALPHABLENDENABLE, true);
	SetRenderStateTemp rs2(D3DRS_ALPHATESTENABLE, false);
  // note: cannot use Temp here - they are not going to be set in the DoPrepareTriangle and the temporary setting returning would set a undefined value
  _engine->D3DSetRenderState(cb, D3DRS_SRCBLEND,D3DBLEND_ONE);
  _engine->D3DSetRenderState(cb, D3DRS_DESTBLEND,D3DBLEND_INVSRCALPHA);
#ifndef _X360SHADERGEN
	SetRenderStateTemp rs5(D3DRS_FOGENABLE, false);
#endif
	SetRenderStateTemp rs6(D3DRS_CULLMODE, D3DCULL_NONE);
	//SetRenderStateTemp rs7(D3DRS_FILLMODE, D3DFILL_SOLID);
  //SetRenderStateTemp rs8(D3DRS_ZENABLE, true);
	SetRenderStateTemp rs8(D3DRS_ZENABLE, FALSE);
	SetRenderStateTemp rs9(D3DRS_ZFUNC, D3DCMP_LESSEQUAL);
	SetRenderStateTemp rs10(D3DRS_ZWRITEENABLE, FALSE);
	SetRenderStateTemp rs11(D3DRS_STENCILENABLE, FALSE);
	SetSamplerStateTemp ss0(0, D3DSAMP_ADDRESSU, D3DTADDRESS_WRAP);
	SetSamplerStateTemp ss1(0, D3DSAMP_ADDRESSV, D3DTADDRESS_WRAP);
	SetSamplerStateTemp ss2(0, D3DSAMP_MINFILTER, D3DTEXF_LINEAR);
	SetSamplerStateTemp ss3(0, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR);
	SetSamplerStateTemp ss4(0, D3DSAMP_MIPFILTER, D3DTEXF_LINEAR);
	SetSamplerStateTemp ss6(1, D3DSAMP_ADDRESSU, D3DTADDRESS_CLAMP);
	SetSamplerStateTemp ss7(1, D3DSAMP_ADDRESSV, D3DTADDRESS_CLAMP);
	SetSamplerStateTemp ss8(1, D3DSAMP_MINFILTER, D3DTEXF_POINT);
	SetSamplerStateTemp ss9(1, D3DSAMP_MAGFILTER, D3DTEXF_POINT);
	SetSamplerStateTemp ss10(1, D3DSAMP_MIPFILTER, D3DTEXF_NONE);

  
  #if ENCAPSULATE_TEXTURE_REF
	SetTextureTemp tTex1(0, _resrc->_rainTexture->GetHandle() ? _resrc->_rainTexture->GetHandle()->_surface : NULL);
	#else
	SetTextureTemp tTex1(0, _resrc->_rainTexture->GetHandle());
	#endif
  SetTextureTemp tTex2(1, _engine->GetDepthAsTexture());

#ifndef _X360SHADERGEN
  SetSamplerStateTemp ss1SRGBTEXTURE(0, D3DSAMP_SRGBTEXTURE, FALSE);
  SetSamplerStateTemp ss2SRGBTEXTURE(1, D3DSAMP_SRGBTEXTURE, FALSE);
#endif

	_engine->SetStreamSource(cb, _resrc->_vbRainGeometry, sizeof(Vertex), numLayers * 4 * 2 * 3);
	_engine->DrawPrimitive(cb, D3DPT_TRIANGLELIST, 0, numLayers * 4 * 2);

  // restore state into a default
  _engine->D3DSetRenderState(cb, D3DRS_STENCILENABLE, TRUE);
  _engine->D3DSetRenderState(cb, D3DRS_ZFUNC,D3DCMP_LESSEQUAL);
  _engine->D3DSetRenderState(cb, D3DRS_ZWRITEENABLE,TRUE);
  _engine->D3DSetRenderState(cb, D3DRS_ZENABLE,TRUE);
  _engine->D3DSetRenderState(cb, D3DRS_CULLMODE,D3DCULL_CCW);
  _engine->D3DSetRenderState(cb, D3DRS_DESTBLEND,D3DBLEND_INVSRCALPHA);
  //_engine->D3DSetRenderState(cb, D3DRS_SRCBLEND,D3DBLEND_ONE);
  _engine->D3DSetRenderState(cb, D3DRS_ALPHABLENDENABLE,FALSE);
  _engine->D3DSetRenderState(cb, D3DRS_COLORWRITEENABLE,D3DCOLORWRITEENABLE_RED | D3DCOLORWRITEENABLE_GREEN | D3DCOLORWRITEENABLE_BLUE | D3DCOLORWRITEENABLE_ALPHA);

  // If scene is not being rendered finish initialized scene
  if (!inScene) _engine->D3DEndScene();
}
//#ifdef _PROFILE
//#pragma optimize("", on)
//#endif

int EngineDD9::PPRain3D::SetParams(const float *pars, int nPars)
{
  if(pars && nPars > 0)
    _rainStrength = pars[0];

  return 0;
}

void EngineDD9::PPRain3D::Cleanup()
{
  _resrc->_rainTexture.Free();
}


//////////////////////////////////////////////////////////////////////////
void EngineDD9::PPSSSM::Prepare()
{
  // This method overrides the PPSimple::Prepare method, that's why we call the PostProcess::Prepare here instead of usual base::Prepare
  PostProcess::Prepare();

  const int cb = -1;
  
  // Set the stream source
  _engine->SetStreamSource(cb, _vb, sizeof(Vertex), 4);

  // Set the vertex declaration
  _engine->SetVertexDeclaration(cb, _vd);

  // Set the vertex shader
  _engine->SetVertexShader(cb, _vs);

  // Set the pixel shader
  if (_engine->_sbTechnique == SBT_nVidia)
  {
    _engine->SetPixelShader(cb, _psNVidia);
  }
  else
  {
    _engine->SetPixelShader(cb, _ps);
  }
}

bool EngineDD9::PPSSSM::Init(const LazyLoadFile &hlsl, const LazyLoadFile &vs)
{
  if (!base::Init(hlsl,vs,"VSPostProcess")) return false;
  if (!CompilePixelShader(hlsl,"PSPostProcessSSSM_Default", _ps, true)) return false;
  if (!CompilePixelShader(hlsl,"PSPostProcessSSSM_nVidia", _psNVidia, true)) return false;
  return true;
}

void EngineDD9::PPSSSM::Do(float minShadowsZ, float maxShadowsZ, bool firstLayer, bool lastLayer)
{
  // Draw
  PROFILE_SCOPE_GRF_EX(ppSSSM,pp,SCOPE_COLOR_YELLOW);

  const int cb = -1;

  // If scene is not being rendered initialize scene
  bool inScene = _engine->_d3dFrameOpen;
  if (!inScene) _engine->D3DBeginScene();

  // Prepare drawing
  Prepare();

  // Pass the FOV into the shader
  float fovW = 1.0f;
  float fovH = 1.0f;
  if (_engine->CBState(cb)._proj.IsFinite())
  {
    fovW = 1.0f / _engine->CBState(cb)._proj(0, 0);
    fovH = 1.0f / _engine->CBState(cb)._proj(1, 1);
  }
  SetPixelShaderConstantTemp spc0(0, D3DXVECTOR4(fovW, fovH, 0, 0));

  // Influence density of trees by multiplying the final alpha
  Color pscSBTSize_invSBTSize_X_AlphaMult(_engine->_sbSize, 1.0f / _engine->_sbSize, 0, 1.5f); // Empirical coefficient controlling density of trees
  SetPixelShaderConstantTemp spc1(1, (float*)&pscSBTSize_invSBTSize_X_AlphaMult);

  // Setup the matrix that converts the transformedPosition into shadow map
  _engine->SetPixelShaderConstantF(cb, 20, (float*)&_engine->_shadowMapMatrixDX, 3);

  // Setup the SB depth scale
  Color pscMaxDepth(_engine->_maxDepth, _engine->_maxDepth, _engine->_maxDepth, _engine->_maxDepth);
  _engine->SetPixelShaderConstantF(cb, 23, (float*)&pscMaxDepth, 1);

  // Setup the border constants
  D3DXVECTOR4 layerBorder;
  layerBorder = D3DXVECTOR4(maxShadowsZ, maxShadowsZ/*lastLayer ? 1.0f : 0.0f*/, maxShadowsZ, minShadowsZ);
  _engine->SetPixelShaderConstantF(cb, 40, layerBorder, 1);

  // Set temporary render states
  SetRenderStateTemp rsCOLORWRITEENABLE(D3DRS_COLORWRITEENABLE, D3DCOLORWRITEENABLE_RED | D3DCOLORWRITEENABLE_GREEN | D3DCOLORWRITEENABLE_BLUE | D3DCOLORWRITEENABLE_ALPHA);
  SetRenderStateTemp rsSRGBSRGBWRITEENABLE(D3DRS_SRGBWRITEENABLE, FALSE);

  // We're going to render to SSSM
  ComRef<IDirect3DSurface9> zeroDepth;
  _engine->SetTemporaryRenderTarget(_engine->_sssm, _engine->_sssmSurface, zeroDepth);

#ifndef _XBOX
  // Clear the SSSM, as it's possible nothing will be drawn on certain places
  if (firstLayer)
  {
    CALL_D3D_MEMBER(_engine,Clear,0,NULL,D3DCLEAR_TARGET,PackedColor(255, 0, 0, 0),0,0);
  }
#endif

  // Set depth map as a input
  SetTextureTemp tTex0(0, _engine->_renderTargetDepthInfo);
  SetSamplerStateTemp ss0MINFILTER(0, D3DSAMP_MINFILTER, D3DTEXF_POINT);
  SetSamplerStateTemp ss0MAGFILTER(0, D3DSAMP_MAGFILTER, D3DTEXF_POINT);
  SetSamplerStateTemp ss0MIPFILTER(0, D3DSAMP_MIPFILTER, D3DTEXF_NONE);
  SetSamplerStateTemp ss0SRGBTEXTURE(0, D3DSAMP_SRGBTEXTURE, FALSE);
  
#ifdef _XBOX
  // Set SSSM as source - X360 needs it, as previous content of SSSM in EDRAM was replaced when rendering to SB
  SetTextureTemp tTex1(1, _engine->_sssm);
  SetSamplerStateTemp ss1MINFILTER(1, D3DSAMP_MINFILTER, D3DTEXF_POINT);
  SetSamplerStateTemp ss1MAGFILTER(1, D3DSAMP_MAGFILTER, D3DTEXF_POINT);
  SetSamplerStateTemp ss1MIPFILTER(1, D3DSAMP_MIPFILTER, D3DTEXF_NONE);
  SetSamplerStateTemp ss1SRGBTEXTURE(1, D3DSAMP_SRGBTEXTURE, FALSE);
#endif

  // Set the SB as shadow map
  _engine->UseShadowMapSB(cb);

  /*
  // because of VSM we want bilinear sampling - UseShadowMapSB set point
  _engine->D3DSetSamplerState(TEXID_SHADOWMAP, D3DSAMP_MINFILTER, D3DTEXF_LINEAR);
  _engine->D3DSetSamplerState(TEXID_SHADOWMAP, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR);
  */

  // Drawing
  _engine->DrawPrimitive(cb, D3DPT_TRIANGLESTRIP, 0, 2);

#ifdef _XBOX
  // Resolve content of the render target
  CALL_D3D_MEMBER(_engine,Resolve,D3DRESOLVE_RENDERTARGET0|D3DRESOLVE_ALLFRAGMENTS, NULL, _engine->_sssm, NULL, 0, 0, NULL, 1.0f, 0L, NULL);
#endif

  // Restore previous render target settings
  _engine->RestoreOldRenderTarget();

  // If scene is not being rendered finish initialized scene
  if (!inScene) _engine->D3DEndScene();

  
  #if !defined _XBOX && !defined _X360SHADERGEN
  // if it was the last layer, and we are using multisampling, it is a time to Resolve / StretchRect
  // we are currently not using multisampling on SSSM on the PC, and Xbox handles it elsewhere
//     if (_engine->GetFSAA()>0)
//     {
//       // TODO: make compatible with background rendering
//       //DoAssert(!_engine->_backgroundD3D);
//       ComRef<IDirect3DSurface9> sssmTexLevel0;
//       _engine->_sssm->GetSurfaceLevel(0,sssmTexLevel0.Init());
//       CALL_D3D_MEMBER(_engine,StretchRect,_engine->_sssmSurface,NULL,sssmTexLevel0,NULL,D3DTEXF_POINT);
//     }
  #endif

  Finish();
}

//////////////////////////////////////////////////////////////////////////

bool EngineDD9::PPSSSMStencil::Init(const LazyLoadFile &hlsl, const LazyLoadFile &vs)
{
  if (!base::Init(hlsl,vs,"VSPostProcess")) return false;
  if (!CompilePixelShader(hlsl,"PSPostProcessSSSMStencil", _ps, true)) return false;
  return true;
}


void EngineDD9::PPSSSMStencil::Do(bool isLast)
{
  PROFILE_SCOPE_GRF_EX(ppSSSMStencil,pp,SCOPE_COLOR_YELLOW);

  // If scene is not being rendered initialize scene
  bool inScene = _engine->_d3dFrameOpen;
  if (!inScene) _engine->D3DBeginScene();

  // Prepare drawing
  Prepare();

  const int cb = -1;
  
  {
    // make sure SSSM is not mapped as a texture
    _engine->SetTexture(cb, TEXID_SHADOWMAP,NULL);
    // SetTextureTemp did not work well - caused strange error "Texture not created with this device. SetTexture failed."
    //SetTextureTemp noSSSM(TEXID_SHADOWMAP,NULL);


    bool restore = true;
    if(_engine->_renderTargetDepthBufferSurfaceSaved.IsNull())
    {
      // We're going to render to SSSM
      //_engine->SetTemporaryRenderTarget(_engine->_sssm, _engine->_sssmSurface, _engine->_renderTargetDepthInfoZ);
      if (_engine->_depthBufferRT)
      {
        #if _XBOX
        _engine->SetTemporaryRenderTarget(_engine->_sssm, _engine->_sssmSurface, _engine->_depthBufferRT);
        #else
        if (_engine->GetFSAA()>0)
        {
          // on PC SSSM is never multisampled
          _engine->SetTemporaryRenderTarget(_engine->_sssm, _engine->_sssmSurface, _engine->_renderTargetDepthInfoZ);
        }
        else
        {
          _engine->SetTemporaryRenderTarget(_engine->_sssm, _engine->_sssmSurface, _engine->_depthBufferRT);
        }
        #endif
      }
      else
      {
        _engine->SetTemporaryRenderTarget(_engine->_sssm, _engine->_sssmSurface, _engine->_currRenderTargetDepthBufferSurface);
      }
    }
    else
    {
      _engine->SetRenderTargetAndDepthStencil(_engine->_sssmSurface, _engine->_sssm, _engine->_currRenderTargetDepthBufferSurface);
      // Set the depth range to make sure the depth range has not been changed
      _engine->SetDepthRange(_engine->_minZRange,_engine->_maxZRange);
      restore = false;
    }

#ifdef _XBOX
    // Clear the SSSM map to 1 in R (no shadows)
    if (restore || !_engine->IsSBEnabled()) CALL_D3D_MEMBER(_engine,Clear,0,NULL,D3DCLEAR_TARGET,PackedColor(255, 0, 0, 0),0,0);
#else
    if (isLast)
    {
      CALL_D3D_MEMBER(_engine,Clear,0,NULL,D3DCLEAR_TARGET,PackedColor(255, 0, 0, 0),0,0);
    }
#endif

    // Set usual PP render states
    SetRenderStateTemp rsCOLORWRITEENABLE(D3DRS_COLORWRITEENABLE, D3DCOLORWRITEENABLE_RED | D3DCOLORWRITEENABLE_GREEN | D3DCOLORWRITEENABLE_BLUE | D3DCOLORWRITEENABLE_ALPHA);
    SetRenderStateTemp rsSRGBSRGBWRITEENABLE(D3DRS_SRGBWRITEENABLE, FALSE);

    // Set render states to control the stencil visualization
    SetRenderStateTemp rsSTENCILFUNC(D3DRS_STENCILFUNC, D3DCMP_LESS);
    SetRenderStateTemp rsSTENCILMASK(D3DRS_STENCILMASK, STENCIL_COUNTER);
    SetRenderStateTemp rsSTENCILREF(D3DRS_STENCILREF, 0);
    SetRenderStateTemp rsZTEST(D3DRS_ZENABLE,D3DZB_FALSE);
    SetRenderStateTemp rsALPHABLENDENABLE(D3DRS_ALPHABLENDENABLE, FALSE);
    SetRenderStateTemp rsSRCBLEND(D3DRS_SRCBLEND, D3DBLEND_ONE);
    SetRenderStateTemp rsDESTBLEND(D3DRS_DESTBLEND, D3DBLEND_ONE);

    // Drawing
    _engine->DrawPrimitive(cb, D3DPT_TRIANGLESTRIP, 0, 2);

#ifdef _XBOX
  #if 0 // PRT_SHADOWS
    // Resolve rendertarget into _renderTargetD
    for (int i = 0; i < _engine->_tilingRectsCount; i++)
    {
      // Set predication to tile i.
      CALL_D3D_MEMBER(_engine,SetPredication, D3DPRED_TILE( i ) );

      // Destination point is the upper left corner of the tiling rect.
      D3DPOINT* pDestPoint = (D3DPOINT*)&_engine->_tilingRects[i];

      D3DVECTOR4 clearColor;
      clearColor.x = _engine->_clearColor.R();
      clearColor.y = _engine->_clearColor.G();
      clearColor.z = _engine->_clearColor.B();
      clearColor.w = _engine->_clearColor.A();
      
      // Resolve render target into _renderTargetD, clear the source tile to the clear values
      CALL_D3D_MEMBER(_engine,Resolve,
        D3DRESOLVE_RENDERTARGET0|D3DRESOLVE_ALLFRAGMENTS|D3DRESOLVE_CLEARRENDERTARGET|D3DRESOLVE_CLEARDEPTHSTENCIL,
        &_engine->_tilingRects[i], _engine->_sssm, pDestPoint, 0, 0, &clearColor, 1.0f, 0L, NULL
      );
    }
  #else
    CALL_D3D_MEMBER(_engine,Resolve,D3DRESOLVE_RENDERTARGET0|D3DRESOLVE_ALLFRAGMENTS, NULL, _engine->_sssm, NULL, 0, 0, NULL, 1.0f, 0L, NULL);
  #endif
#endif

    if (restore)
    {
      // Restore previous render target settings
      _engine->RestoreOldRenderTarget();
    }

  }

  // If scene is not being rendered finish initialized scene
  if (!inScene) _engine->D3DEndScene();
  // Restore usual rendering settings
  Finish();
}

//////////////////////////////////////////////////////////////////////////
#ifdef SSSMBLUR
bool EngineDD9::PPSSSMB::Init(const LazyLoadFile &hlsl, const LazyLoadFile &vs)
{
  if (!PostProcess::Init(hlsl,vs)) return false;

  // Create custom edge vertex resources
  if (!InitCustomEdge(vs)) return false;

  // Create pixel shaders
  {
    if (!CompilePixelShader(hlsl,"PSPostProcessSSSMBH", _psHorizontal, true)) return false;
    if (!CompilePixelShader(hlsl,"PSPostProcessSSSMBV", _psBlur, true)) return false;
    if (!CompilePixelShader(hlsl,"PSPostProcessSSSMBFinalBlur", _psFinalBlur, true)) return false;
  }

  // Success
  return true;
}

void EngineDD9::PPSSSMB::Do(bool isLast)
{
  // Draw
  PROFILE_SCOPE_GRF_EX(ppSSSMB,pp,SCOPE_COLOR_YELLOW);

  // If scene is not being rendered initialize scene
  bool inScene = _engine->_d3dFrameOpen;
  if (!inScene) _engine->D3DBeginScene();

  // Prepare drawing
  Prepare();

  // Perform blur in 2 steps
  {
    // Protect VS constants from changing
    PushVertexShaderConstant pushC0(0);
    PushVertexShaderConstant pushC1(1);
    PushVertexShaderConstant pushC2(2);
    PushVertexShaderConstant pushC3(3);
    PushVertexShaderConstant pushC4(4);
    PushVertexShaderConstant pushC5(5);
    PushVertexShaderConstant pushC6(6);
    PushVertexShaderConstant pushC7(7);
    PushVertexShaderConstant pushC8(8);

    // Set the vertex shader + stream source
    _engine->SetStreamSource(_vbCustomEdge, sizeof(VertexCustomEdge), 4);
    _engine->SetVertexDeclaration(_vdCustomEdge);
    _engine->SetVertexShader(_vsCustomEdge);

    // Prepare the UV coordinates offset coefficients
    static const float baseU[4] = {0, 0, 1, 1};
    static const float baseV[4] = {1, 0, 1, 0};
    int fullWidth = _engine->Width();
    int fullHeight = _engine->Height();
    int partWidth = fullWidth / GBSizeDivisor;
    int partHeight = fullHeight / GBSizeDivisor;

    // No posOffset needed
    const float posOffset[4] = {0, 0, 0, 0};
    _engine->SetVertexShaderConstantF(8, posOffset, 1);

    // Set temporarily render target and depth stencil to horizontally shrunk texture
    ComRef<IDirect3DSurface9> zeroDepth;
    _engine->SetTemporaryRenderTarget(_rtHorizontal, _rtsHorizontal, zeroDepth);

    // We need to set the viewport as it was set to previous values inside the SetTemporaryRenderTarget.
    // We cannot use the SetDepthRange method here, because we need to use custom Width and Height
    {
      D3DVIEWPORT9 viewData;
      memset(&viewData,0,sizeof(viewData));
      viewData.X = 0;
      viewData.Y = 0;
      viewData.Width  = partWidth;
      viewData.Height = fullHeight;
      viewData.MinZ = 0.0f;
      viewData.MaxZ = 1.0f;
      CALL_D3D_MEMBER(_engine,SetViewport,viewData);
    }

    // Set temporary render states
    SetRenderStateTemp rsCOLORWRITEENABLE(D3DRS_COLORWRITEENABLE, D3DCOLORWRITEENABLE_RED | D3DCOLORWRITEENABLE_GREEN | D3DCOLORWRITEENABLE_BLUE | D3DCOLORWRITEENABLE_ALPHA);
#ifndef _X360SHADERGEN

    // Use SRGB conversion in texture read
    SetSamplerStateTemp ss0SRGBTEXTURE(0, D3DSAMP_SRGBTEXTURE, FALSE);
    SetSamplerStateTemp ss1SRGBTEXTURE(1, D3DSAMP_SRGBTEXTURE, FALSE);

    // Write in the same space as you read
    SetRenderStateTemp rsSRGBSRGBWRITEENABLE(D3DRS_SRGBWRITEENABLE, FALSE);

#endif

    // Use linear sampling and clamping for the texture on stage 0
    SetSamplerStateTemp ss0MINFILTER(0, D3DSAMP_MINFILTER, D3DTEXF_LINEAR);
    SetSamplerStateTemp ss0MAGFILTER(0, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR);
    SetSamplerStateTemp ss0ADDRESSU(0, D3DSAMP_ADDRESSU, D3DTADDRESS_CLAMP);
    SetSamplerStateTemp ss0ADDRESSV(0, D3DSAMP_ADDRESSV, D3DTADDRESS_CLAMP);
    SetSamplerStateTemp ss1MINFILTER(1, D3DSAMP_MINFILTER, D3DTEXF_LINEAR);
    SetSamplerStateTemp ss1MAGFILTER(1, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR);
    SetSamplerStateTemp ss1ADDRESSU(1, D3DSAMP_ADDRESSU, D3DTADDRESS_CLAMP);
    SetSamplerStateTemp ss1ADDRESSV(1, D3DSAMP_ADDRESSV, D3DTADDRESS_CLAMP);

    // First step - render horizontally shrunk texture
    {
      // Prepare the UV coordinates offsets
      const float srcOffsetU = 0.5f / partWidth;
      const float srcOffsetV = 0.5f / fullHeight;
      float U[4][4];
      float V[4][4];
      for (int vertex = 0; vertex < 4; vertex++)
      {
        // Texture 0 - source image
        U[vertex][0] = baseU[vertex] + srcOffsetU;
        V[vertex][0] = baseV[vertex] + srcOffsetV;
        // Rest
        U[vertex][1] = U[vertex][2] = U[vertex][3] = 0;
        V[vertex][1] = V[vertex][2] = V[vertex][3] = 0;
      }
      _engine->SetVertexShaderConstantF(0, U[0], 4);
      _engine->SetVertexShaderConstantF(4, V[0], 4);

      // Set the pixel shader for final blur
      _engine->SetPixelShader(_psHorizontal);

      // Set sssm as temporary texture
      SetTextureTemp tTex0(0, _engine->_sssm);

      // Prepare PS constants
      float invW = 1.0f / _engine->Width();
      SetPixelShaderConstantTemp spc0(0, D3DXVECTOR4(invW, invW, invW, invW));

      // Drawing
      _engine->DrawPrimitive(D3DPT_TRIANGLESTRIP, 0, 2);

#ifdef _XBOX
      // Resolve content of the render target
      CALL_D3D_MEMBER(_engine,Resolve,D3DRESOLVE_RENDERTARGET0|D3DRESOLVE_ALLFRAGMENTS, NULL, _rtHorizontal, NULL, 0, 0, NULL, 1.0f, 0L, NULL);
#endif
    }

    // Rewrite render target and depth stencil surfaces by the blurred ones
    _engine->SetRenderTargetAndDepthStencil(_rtsBlur, _rtBlur, zeroDepth);

    // Second step - render finally blurred texture from the horizontally shrunked one
    {
      // Prepare the UV coordinates offsets
      const float srcOffsetU = 0.5f / partWidth;
      const float srcOffsetV = 0.5f / partHeight;
      float U[4][4];
      float V[4][4];
      for (int vertex = 0; vertex < 4; vertex++)
      {
        // Texture 0 - source image
        U[vertex][0] = baseU[vertex] + srcOffsetU;
        V[vertex][0] = baseV[vertex] + srcOffsetV;
        // Rest
        U[vertex][1] = U[vertex][2] = U[vertex][3] = 0;
        V[vertex][1] = V[vertex][2] = V[vertex][3] = 0;
      }
      _engine->SetVertexShaderConstantF(0, U[0], 4);
      _engine->SetVertexShaderConstantF(4, V[0], 4);

      // Set the pixel shader for final blur
      _engine->SetPixelShader(_psBlur);

      // Set current render target as temporary texture (we know we don't render into it)
      SetTextureTemp tTex0(0, _rtHorizontal);

      // Prepare PS constants
      float invH = 1.0f / _engine->Height();
      SetPixelShaderConstantTemp spc0(0, D3DXVECTOR4(invH, invH, invH, invH));

      // Drawing
      _engine->DrawPrimitive(D3DPT_TRIANGLESTRIP, 0, 2);

#ifdef _XBOX
      // Resolve content of the render target
      CALL_D3D_MEMBER(_engine,Resolve,D3DRESOLVE_RENDERTARGET0|D3DRESOLVE_ALLFRAGMENTS, NULL, _rtBlur, NULL, 0, 0, NULL, 1.0f, 0L, NULL);
#endif
    }

    // Third step - create the SSSMB map
    {
      // Rewrite render target and depth stencil surfaces by the SSSMB
      _engine->SetRenderTargetAndDepthStencil(_engine->_sssmBSurface, _engine->_sssmB, zeroDepth);

      // Prepare the UV coordinates offsets
      const float srcOffsetU = 0.5f / fullWidth;
      const float srcOffsetV = 0.5f / fullHeight;
      float U[4][4];
      float V[4][4];
      for (int vertex = 0; vertex < 4; vertex++)
      {
        // Texture 0 - source image
        U[vertex][0] = baseU[vertex] + srcOffsetU;
        V[vertex][0] = baseV[vertex] + srcOffsetV;
        // Rest
        U[vertex][1] = U[vertex][2] = U[vertex][3] = 0;
        V[vertex][1] = V[vertex][2] = V[vertex][3] = 0;
      }
      _engine->SetVertexShaderConstantF(0, U[0], 4);
      _engine->SetVertexShaderConstantF(4, V[0], 4);

      // Set the pixel shader for final blur
      _engine->SetPixelShader(_psFinalBlur);

      // Set the textures
      SetTextureTemp tTex0(0, _engine->_sssm);
      SetTextureTemp tTex1(1, _rtBlur);

      // Drawing
      _engine->DrawPrimitive(D3DPT_TRIANGLESTRIP, 0, 2);

#ifdef _XBOX
      // Resolve content of the render target
      CALL_D3D_MEMBER(_engine,Resolve,D3DRESOLVE_RENDERTARGET0|D3DRESOLVE_ALLFRAGMENTS, NULL, _rtBlur, NULL, 0, 0, NULL, 1.0f, 0L, NULL);
#endif
    }

    // Restore the original render target and depth stencil buffer
    _engine->RestoreOldRenderTarget();
  }

  // If scene is not being rendered finish initialized scene
  if (!inScene) _engine->D3DEndScene();

  // Restore usual rendering settings
  _engine->SwitchTL(TLDisabled);
}
#endif


//////////////////////////////////////////////////////////////////////////
//
// SSAO postprocess effect
//
//////////////////////////////////////////////////////////////////////////
void EngineDD9::PPSSAO::Prepare()
{
  const PPSSAOResources *res = Resources();
  if (!res) return;

  // const int cb = -1;
  
  // TODO
  // base::Prepare();

  _engine->_d3DDevice->SetStreamSource(0, res->_vb, 0, sizeof(VertexP3T2));

  _engine->_d3DDevice->SetVertexDeclaration(res->_vd);

  _engine->_d3DDevice->SetVertexShader(res->_vsSSAO);

  /*
  // Set the stream source
  _engine->SetStreamSource(cb, res->_vb, sizeof(VertexP3T2), 4);
  // Set the vertex declaration
  _engine->SetVertexDeclaration(cb, res->_vd);
  // Set the vertex shader
  _engine->SetVertexShader(cb, res->_vsSSAO);
  */
}

//==================================================================================================
bool EngineDD9::PPSSAO::Init(const LazyLoadFile &hlsl, const LazyLoadFile &vs, bool firstInit)
{
  _bDebug = false;

  if (!base::Init(hlsl,vs)) return false;

  if (firstInit)
  {
    SetDefaultValues();
    _enabled = false;
    _pInterp.Initialize();
  }

  Ref<PPSSAOResources> resrc = new PPSSAOResources;

  // Create the VB
  ComRef<IDirect3DVertexBuffer9> vb;
  if (FAILED(_engine->GetDirect3DDevice()->CreateVertexBuffer(sizeof(Vertex) * 4, D3DUSAGE_WRITEONLY, 0, D3DPOOL_DEFAULT, vb.Init(), NULL)))
  {
    LogF("Error: Vertex buffer creation failed.");
    return false;
  }

  Vertex *v;
  vb->Lock(0, 0, (void**)&v, 0);
    v[0].pos = Vector3P(-0.5, -0.5, 0.5); v[0].t0.u = 0; v[0].t0.v = 1;
    v[1].pos = Vector3P(-0.5,  0.5, 0.5); v[1].t0.u = 0; v[1].t0.v = 0;
    v[2].pos = Vector3P( 0.5, -0.5, 0.5); v[2].t0.u = 1; v[2].t0.v = 1;
    v[3].pos = Vector3P( 0.5,  0.5, 0.5); v[3].t0.u = 1; v[3].t0.v = 0;
  vb->Unlock();

  // Create the VD
  const D3DVERTEXELEMENT9 vertexDecl[] =
  {
    {0,  0, D3DDECLTYPE_FLOAT3,   D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION, 0},
    {0, 12, D3DDECLTYPE_FLOAT2,   D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 0},
    D3DDECL_END()
  };
  ComRef<IDirect3DVertexDeclaration9> vd;

  if (FAILED(_engine->GetDirect3DDevice()->CreateVertexDeclaration(vertexDecl, vd.Init())))
  {
    LogF("Error: Vertex declaration creation failed.");
    return false;
  }

  //vertex shader
  if (!CompileVertexShader(hlsl, "VSPostProcessSSAO", resrc->_vsSSAO))
    return false;

  if (!CompileVertexShader(hlsl, "VSPostProcessSSAODown", resrc->_vsSSAODown))
    return false;

  //pixel shaders
  if (!CompilePixelShader(hlsl,"PSPostProcessSSAO", resrc->_psSSAO, false))
    return false;

  if (!CompilePixelShader(hlsl,"PSPostProcessSSAOBlurVert", resrc->_psBlurVert, false))
    return false;

  if (!CompilePixelShader(hlsl,"PSPostProcessSSAOBlurHorz", resrc->_psBlurHorz, false))
    return false;

  if (!CompilePixelShader(hlsl,"PSPostProcessSSAOBlurHorzDepth", resrc->_psBlurHorzDepth, false))
    return false;

  if (!CompilePixelShader(hlsl,"PSPostProcessSSAOBlurVertDepth", resrc->_psBlurVertDepth, false))
    return false;

  if (!CompilePixelShader(hlsl,"PSPostProcessSSAOFinal", resrc->_psSSAOFinal, false))
    return false;

  if (!CompilePixelShader(hlsl,"PSPostProcessSSAODebug", resrc->_psSSAODebug, false))
    return false;

  if (!CompilePixelShader(hlsl,"PSPostProcessSSAODown", resrc->_psSSAODown, false))
    return false;

  _bHalfResOld = _pInterp._lastPars._halfres;
  _bBlurHalfResOld = _pInterp._lastPars._blurhalfres;

  if (_engine->_d3DDevice.IsNull()) return false;
  ComRef<IDirect3DDevice9> &device = _engine->_d3DDevice->GetRefForCB();

  // render targets
  if (!SetRT(device.GetRef(), resrc, _bHalfResOld, _bBlurHalfResOld))
  {
    return false;
  }

  //create noise texture
  const int texsize = 128;
  ComRef<IDirect3DTexture9> noiseTexture;
  if (FAILED(_engine->_d3DDevice->CreateTexture(texsize, texsize, 1, 0, D3DFMT_A8R8G8B8, D3DPOOL_DEFAULT, noiseTexture.Init(), NULL)))
  {
    RptF("Error: Cannot create noise texture");
    return false;
  }

#ifndef _XBOX
  ComRef<IDirect3DTexture9> sysMem;
  if (FAILED(_engine->_d3DDevice->CreateTexture(texsize, texsize, 1, 0, D3DFMT_A8R8G8B8, D3DPOOL_SYSTEMMEM, sysMem.Init(), NULL)))
  {
    RptF("Error: Cannot create noise sysmem texture");
    return false;
  }
#else
  ComRef<IDirect3DTexture9> sysMem = noiseTexture;
#endif

  //for each mipmap 
  ComRef<IDirect3DSurface9> surface;
  if (FAILED(sysMem->GetSurfaceLevel(0, surface.Init())))
  {
    RptF("Error: Cannot get noise texture surface ");
    return false;
  }

  D3DSURFACE_DESC desc;
  if (FAILED(surface->GetDesc(&desc)))
  {
    RptF("Error: Cannot get noise texture surface description");
    return false;
  }

  D3DLOCKED_RECT lr;
  if (FAILED(surface->LockRect(&lr, NULL, 0)))
  {
    RptF("Error: Cannot lock noise texture surface");
    return false;
  }

  //for each texel
  for (int y = 0; y < (int) desc.Height; y++)
  {
    unsigned char *data = (unsigned char*) ((char*) lr.pBits + y*lr.Pitch);

    for (int x = 0; x < (int) desc.Width; x++, data += 4)
    {
      float angle = GRandGen.RandomValue()*3.14159265f*2.0f;

      float s = sin(angle);
      float c = cos(angle);

      //random matrix
      data[0] = (unsigned char) (( c*0.5f + 0.5f)*255.0f);
      data[1] = (unsigned char) (( s*0.5f + 0.5f)*255.0f);
      data[2] = (unsigned char) (( s*0.5f + 0.5f)*255.0f);
      data[3] = (unsigned char) ((-c*0.5f + 0.5f)*255.0f);
    }
  }

  surface->UnlockRect();

#if !XBOX_D3D
  _engine->_d3DDevice->UpdateTexture(sysMem, noiseTexture);
#endif

  // assign to shared resources
  resrc->_vd = vd;
  resrc->_vb = VertexStaticData9::BufferFuture(vb);
  resrc->_noiseTexture = noiseTexture;

  resrc->_texWidth  = texsize;
  resrc->_texHeight = texsize;

  _resrc = resrc;

  return true;
}

//==================================================================================================
bool EngineDD9::PPSSAO::SetRT(IDirect3DDevice9 *device, PPSSAOResources *res, bool halfres, bool blurhalfres)
{
  int wantedheight = _engine->Height() >> (halfres ? 1 : 0);
  int wantedwidth  = _engine->Width() >> (halfres ? 1 : 0);

  res->_SSAOWidth = wantedwidth;
  res->_SSAOHeight= wantedheight;

  _bHalfResOld = halfres;
  _bBlurHalfResOld = blurhalfres;

  D3DFORMAT rtFmt = D3DFMT_A8R8G8B8;

  HRESULT err;  
  err = device->CreateTexture(wantedwidth, wantedheight, 1, D3DUSAGE_RENDERTARGET, rtFmt, D3DPOOL_DEFAULT, res->_rtSSAO.Init(), 0);
  if (FAILED(err))
  {
    _engine->DDError9("Error: Render target texture creation failed.", err); 
    return false;
  }
  err = res->_rtSSAO->GetSurfaceLevel(0, res->_rtsSSAO.Init());
  if (FAILED(err))
  {
    _engine->DDError9("Error: GetSurfaceLevel failed.", err); 
    return false;
  }

  wantedheight = _engine->Height() >> ((halfres || blurhalfres) ? 1 : 0);
  wantedwidth  = _engine->Width() >> ((halfres || blurhalfres) ? 1 : 0);

  res->_blurWidth = wantedwidth;
  res->_blurHeight= wantedheight;
  
  err = device->CreateTexture(wantedwidth, wantedheight, 1, D3DUSAGE_RENDERTARGET, rtFmt, D3DPOOL_DEFAULT, res->_rtBlur.Init(), 0);
  if (FAILED(err))
  {
    _engine->DDError9("Error: Render target texture creation failed.", err); 
    return false;
  }
  err = res->_rtBlur->GetSurfaceLevel(0, res->_rtsBlur.Init());
  if (FAILED(err))
  {
    _engine->DDError9("Error: GetSurfaceLevel failed.", err); 
    return false;
  }
  
  if (!halfres && blurhalfres)
  {
    err = device->CreateTexture(wantedwidth, wantedheight, 1, D3DUSAGE_RENDERTARGET, rtFmt, D3DPOOL_DEFAULT, res->_rtSmall.Init(), 0);
    if (FAILED(err))
    {
      _engine->DDError9("Error: Render target texture creation failed.", err); 
      return false;
    }
    err = res->_rtSmall->GetSurfaceLevel(0, res->_rtsSmall.Init());
    if (FAILED(err))
    {
      _engine->DDError9("Error: GetSurfaceLevel failed.", err); 
      return false;
    }
  }

  return true;
}


//==================================================================================================
void EngineDD9::PPSSAO::SetDefaultValues()
{
  _pInterp._pars.SetHighestSetting();
}

//==================================================================================================
bool EngineDD9::PPSSAO::Init(PPResources *resources, bool firstInit)
{
  DoAssert(false);

  if (resources)
  {
    _resrc = resources;

    if (firstInit)
    {
      _bDebug = false;

      SetDefaultValues();
      _enabled = false;
      _pInterp.Initialize();
    }

    return true;
  }

  return false;
}


//==================================================================================================
bool EngineDD9::PPSSAO::UnInit()
{
  if (_resrc)
  {
    _resrc->UnInit();

    return true;
  }

  return false;
}

//==================================================================================================
int EngineDD9::PPSSAO::SetParams(const float *pars, int nPars)
{
  if(nPars > 0)
  {
    if (pars[0] < 0)
    {
      _bDebug = !_bDebug;
      return 0;
    }

    setIntensity(pars[0]);
  }

  if(nPars > 1)
    setTreshold0(pars[1]);

  if(nPars > 2)
    setTreshold1(pars[2]);

  if(nPars > 3)
    setNearRadius(pars[3]);

  if(nPars > 4)
    setFarRadius(pars[4]);

  if(nPars > 5)
    setNearDist(pars[5]);

  if(nPars > 6)
    setFarDist(pars[6]);

  if(nPars > 7)
    setDepthBlurDist(pars[7]);

  if(nPars > 8)
    setBlurPasses(pars[8]);

  if(nPars > 9)
    SetHalfRes(fabs(pars[9]) < 0.01f);

  if(nPars > 10)
    SetBlurHalfRes(fabs(pars[10]) < 0.01f);

  return 0;
}

//==================================================================================================
int EngineDD9::PPSSAO::SetParams(const AutoArray<float> &pars)
{
  int nPars = pars.Size();

  if(nPars > 0)
  {
    if (pars[0] < 0)
    {
      _bDebug = !_bDebug;
      return 0;
    }

    setIntensity(pars[0]);
  }

  if(nPars > 1)
    setTreshold0(pars[1]);

  if(nPars > 2)
    setTreshold1(pars[2]);

  if(nPars > 3)
    setNearRadius(pars[3]);

  if(nPars > 4)
    setFarRadius(pars[4]);

  if(nPars > 5)
    setNearDist(pars[5]);

  if(nPars > 6)
    setFarDist(pars[6]);

  if(nPars > 7)
    setDepthBlurDist(pars[7]);

  if(nPars > 8)
    setBlurPasses(pars[8]);

  if(nPars > 9)
    SetHalfRes(fabs(pars[9]) < 0.01f);

  if(nPars > 10)
    SetBlurHalfRes(fabs(pars[10]) < 0.01f);

  return 0;
}

//==================================================================================================
LSError EngineDD9::PPSSAO::Serialize( ParamArchive &ar )
{
  CHECK(ar.Serialize("enabled",_enabled,0,false));
  CHECK(ar.Serialize("interp", _pInterp, 0));
  CHECK(ar.Serialize("prior", _priority, 0, -1));

  return LSOK;
}


//==================================================================================================
void EngineDD9::PPSSAO::SetQuality(int quality)
{
  switch (quality)
  {
    case 5 : _pInterp._lastPars.SetHighestSetting(); break;
    case 4 : _pInterp._lastPars.SetHighSetting(); break;
    case 3 : _pInterp._lastPars.SetMediumSetting(); break;
  }
}

//==================================================================================================

#if 0//_ENABLE_CHEATS
  #define SSAO_DEBUG
#endif

#ifdef SSAO_DEBUG
  static bool ssaoon = true;
  static bool ssaoblurdepth = false;
  static int ssaomode = 0;
  static bool ssaotest = false;
  static bool ssaosave = false;
  static int ssaoparam = 0;
#endif

//==================================================================================================
void EngineDD9::PPSSAO::Do(float fogEnd, float fogStart, float fogCoef)
{
  #ifdef SSAO_DEBUG
    int plus = 0, minus = 0;

    // lctrl + lalt + 
    if((::GetAsyncKeyState(VK_LCONTROL) & 0x8000) && (::GetAsyncKeyState(VK_LMENU) & 0x8000))
    {
       /* O */ if (::GetAsyncKeyState(0x4F) & 0x8000)    {  ssaoon = !ssaoon; ::Sleep(100); LogF("SSAO %s", ssaoon ? "on" : "off"); }
       /* I */ if (::GetAsyncKeyState(0x49) & 0x8000)    {  ssaomode = ssaomode == 2 ? 0 : ssaomode + 1; ::Sleep(100); LogF("SSAO mode : %s", ssaomode == 0 ? "full (Highest setting)" : ssaomode == 1 ? "full ssao, half blur (High setting)" : "half all (medium setting)" );       switch (ssaomode) 
                   {
                      case 0 : _pInterp._lastPars.SetHighestSetting(); break;
                      case 1 : _pInterp._lastPars.SetHighSetting(); break;
                      case 2 : _pInterp._lastPars.SetMediumSetting(); break;
                   }} 
       /* T */ if (::GetAsyncKeyState('T') & 0x8000)    {  _pInterp._lastPars._intensity += 0.05f; ::Sleep(100); }
       /* U */ if (::GetAsyncKeyState('U') & 0x8000)    {  _pInterp._lastPars._intensity -= 0.05f; ::Sleep(100); }

       /* D */ if (::GetAsyncKeyState(0x44) & 0x8000)    {  ssaotest = !ssaotest; ::Sleep(100); LogF("%s", ssaotest ?  "SSAO debug mode" : "SSAO normal mode"); }
      // /* U */ if (::GetAsyncKeyState(0x55) & 0x8000)    {  ssaoblurdepth= !ssaoblurdepth; ::Sleep(100); LogF("%s", ssaoblurdepth ?  "SSAO bluring with edge preserving" : "SSAO blur normal mode"); }
       /* S */ if (::GetAsyncKeyState(0x53) & 0x8000)    {  ssaosave = true; ::Sleep(100); LogF("SSAO data saved into c:/"); }
       /* P */ if (::GetAsyncKeyState(0x50) & 0x8000)    {  ssaoparam = ssaoparam == 8 ? ssaoparam = 0 : ssaoparam+1; ::Sleep(100); int p=ssaoparam; LogF("SSAO param : %s = %3.3g", 
                    p==0?"intensity":
                    p==1?"treshold0":
                    p==2?"treshold1":
                    p==3?"near radius":
                    p==4?"far radius":
                    p==5?"near distance":
                    p==6?"far distance":
                    p==7?"blur depth distance":
                    "blur passes",
                    
                    p==0?_pInterp._lastPars._intensity:
                    p==1?_pInterp._lastPars._treshold0:
                    p==2?_pInterp._lastPars._treshold1:
                    p==3?_pInterp._lastPars._nearradius:
                    p==4?_pInterp._lastPars._farradius:
                    p==5?_pInterp._lastPars._neardist:
                    p==6?_pInterp._lastPars._fardist:
                    p==7?_pInterp._lastPars._depthblurdist:
                    _pInterp._lastPars._ssaoblurpasses); }

       /* *=+ /=- */
       plus  = GetAsyncKeyState(0xBE/*0x6A*/) & 0x8000;
       minus = GetAsyncKeyState(0xBC/*0x6F*/) & 0x8000; 
    }

    if (!ssaoon)
    { 
      return;
    }

    if (plus|minus)
    {
      switch (ssaoparam)
      {
        case 0 : _pInterp._lastPars._intensity      = plus ? _pInterp._lastPars._intensity + 0.01 : max (0.0, _pInterp._lastPars._intensity - 0.01); LogF("SSAO intensity %3.3g", _pInterp._lastPars._intensity); break;
        case 1 : _pInterp._lastPars._treshold0      = plus ? min(1.0f, _pInterp._lastPars._treshold0 + 0.001) : max (0.0, _pInterp._lastPars._treshold0 - 0.001); LogF("SSAO treshold0 %3.3g", _pInterp._lastPars._treshold0); break;
        case 2 : _pInterp._lastPars._treshold1      = plus ? min(1.0f, _pInterp._lastPars._treshold1 + 0.001) : max (0.0, _pInterp._lastPars._treshold1 - 0.001); LogF("SSAO treshold1 %3.3g", _pInterp._lastPars._treshold1); break;
        case 3 : _pInterp._lastPars._nearradius     = plus ? min(200.0f, _pInterp._lastPars._nearradius + 1.0) : max (2.0, _pInterp._lastPars._nearradius - 1.0f); LogF("SSAO near radius %3.3g", _pInterp._lastPars._nearradius); break;
        case 4 : _pInterp._lastPars._farradius      = plus ? min(200.0f, _pInterp._lastPars._farradius + 1.0) : max (1.0, _pInterp._lastPars._farradius - 1.0f); LogF("SSAO far radius %3.3g", _pInterp._lastPars._farradius); break;
        case 5 : _pInterp._lastPars._neardist       = plus ? min(2.0f, _pInterp._lastPars._neardist + 0.005) : max (0.0f, _pInterp._lastPars._neardist - 0.005f); LogF("SSAO near distance %3.3g", _pInterp._lastPars._neardist); break;
        case 6 : _pInterp._lastPars._fardist        = plus ? min(200.0f, _pInterp._lastPars._fardist + 0.1) : max (0.1f, _pInterp._lastPars._fardist - 0.1f); LogF("SSAO far distance %3.3g", _pInterp._lastPars._fardist); break;
        case 7 : _pInterp._lastPars._depthblurdist  = plus ? min(20.0f, _pInterp._lastPars._depthblurdist + 0.1) : max (0.05f, _pInterp._lastPars._depthblurdist - 0.05f); LogF("SSAO depth blur distance %3.3g", _pInterp._lastPars._depthblurdist); break;
        case 8 : _pInterp._lastPars._ssaoblurpasses = plus ? min(4, _pInterp._lastPars._ssaoblurpasses + 1) : max (0, _pInterp._lastPars._ssaoblurpasses - 1); LogF("SSAO blur passes %d", _pInterp._lastPars._ssaoblurpasses); Sleep(200); break;
      }
    }
  #endif

  //DoAssert(!CheckMainThread()); // can be main thread when not HL recording

  if (_engine->_d3DDevice.IsNull()) return;
  ComRef<IDirect3DDevice9> &device = _engine->_d3DDevice->GetRefForCB();

  DoAssert(device.NotNull());
  if (device.IsNull()) return;

  if (_params._intensity < 0.001f || !_enabled) return;

  const PPSSAOResources *res = Resources();
  if (!res) return;

  PROFILE_SCOPE_GRF_EX(ppSSAO,pp,SCOPE_COLOR_RED);

  if (_bHalfResOld != _params._halfres || _bBlurHalfResOld != _params._blurhalfres)
  {
    SetRT(device.GetRef(), (PPSSAOResources *) res, _params._halfres, _params._blurhalfres);
  }

  bool halfres  = _params._halfres;
  bool blurhalfres = _params._blurhalfres;
  bool smallres = blurhalfres && !halfres;

  // If scene is not being rendered initialize scene
  bool inScene = _engine->_d3dFrameOpen;
  if (!inScene) device->BeginScene();
  DoAssert(inScene);

  // Prepare drawing
  device->SetStreamSource(0, res->_vb.GetResult(), 0, sizeof(VertexP3T2));
  device->SetVertexDeclaration(res->_vd);
  device->SetVertexShader(res->_vsSSAO);

  device->SetRenderState(D3DRS_COLORWRITEENABLE, D3DCOLORWRITEENABLE_RED | D3DCOLORWRITEENABLE_GREEN | D3DCOLORWRITEENABLE_BLUE | D3DCOLORWRITEENABLE_ALPHA);

  //render states
  device->SetRenderState(D3DRS_ZWRITEENABLE, FALSE);
  device->SetRenderState(D3DRS_ZENABLE, FALSE);
  device->SetRenderState(D3DRS_CULLMODE, D3DCULL_NONE);
  device->SetRenderState(D3DRS_FILLMODE, D3DFILL_SOLID);
  device->SetRenderState(D3DRS_ALPHATESTENABLE, FALSE);
  device->SetRenderState(D3DRS_ALPHABLENDENABLE, FALSE);
  device->SetRenderState(D3DRS_SRGBWRITEENABLE, FALSE);

  //sampler states, depth texture
  IDirect3DTexture9* depth = _engine->GetDepthAsTexture();

#ifdef SSAO_DEBUG
  if (ssaosave)
  {
    D3DXSaveTextureToFile("c:/depth.dds", D3DXIFF_DDS, depth, NULL);
  }
#endif

  device->SetTexture(0, depth);

  device->SetSamplerState(0, D3DSAMP_MINFILTER, D3DTEXF_LINEAR);
  device->SetSamplerState(0, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR);
  device->SetSamplerState(0, D3DSAMP_ADDRESSU, D3DTADDRESS_BORDER);
  device->SetSamplerState(0, D3DSAMP_ADDRESSV, D3DTADDRESS_BORDER);
  device->SetSamplerState(0, D3DSAMP_BORDERCOLOR, 0xFFFFFFFF);
  device->SetSamplerState(0, D3DSAMP_SRGBTEXTURE, 0);

  //computed ssao
  device->SetSamplerState(1, D3DSAMP_ADDRESSU, D3DTADDRESS_CLAMP);
  device->SetSamplerState(1, D3DSAMP_ADDRESSV, D3DTADDRESS_CLAMP);
  device->SetSamplerState(1, D3DSAMP_MINFILTER, D3DTEXF_LINEAR);
  device->SetSamplerState(1, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR);
  device->SetSamplerState(1, D3DSAMP_SRGBTEXTURE, 0);

  //noise
  device->SetTexture(2, res->_noiseTexture);

  device->SetSamplerState(2, D3DSAMP_ADDRESSU, D3DTADDRESS_WRAP);
  device->SetSamplerState(2, D3DSAMP_ADDRESSV, D3DTADDRESS_WRAP);
  device->SetSamplerState(2, D3DSAMP_MINFILTER, D3DTEXF_POINT);
  device->SetSamplerState(2, D3DSAMP_MAGFILTER, D3DTEXF_POINT);
  device->SetSamplerState(2, D3DSAMP_SRGBTEXTURE, 0);

  float depthwidth	= _engine->Width();
  float depthheight	= _engine->Height();

  //---------------------------------------------------------------------------------------
  //render SSAO info RTTSSAO buffer
  //---------------------------------------------------------------------------------------
  float rendwidth	  = res->_SSAOWidth;
  float rendheight	= res->_SSAOHeight;

  float noisewidth	= res->_texWidth;
  float noiseheight	= res->_texHeight;

  D3DXVECTOR4 vec191192[2];
  //half pixel offsets for current and for noise texture, VSH
  vec191192[0] = D3DXVECTOR4(
    0.5f/depthwidth,
    0.5f/depthheight,
    0.5f/noisewidth,
    0.5f/noiseheight);

  //to convert from <0..1> screen space into noise texture space
  vec191192[1] = D3DXVECTOR4(
    rendwidth/noisewidth,
    rendheight/noiseheight,
    0.0f,
    0.0f);

  D3DXVECTOR4 vec[5];

  vec[0] = D3DXVECTOR4(
    1.0f/depthwidth,
    1.0f/depthheight,
    0.0f,
    0.0f);

  float radnear = _params._nearradius;
  float radfar  = _params._farradius;;
  vec[1] = D3DXVECTOR4(
    radnear*radfar,
    radnear - radfar,
    radfar,
    0.0f);

  float dfarplane = Glob.config.shadowsZ;
  float distfar   = max(0.01f, _params._fardist);
  float distnear  = min(_params._neardist, distfar - 0.05f);
  float sqrdist   = distfar/dfarplane;
  float sqrblurdepth = _params._depthblurdist/dfarplane;
  vec[2] = D3DXVECTOR4(
    dfarplane/distfar,
    distnear/dfarplane,
    (1.0f - 0.025f)/(0.025f*sqrdist*sqrdist),
    (1.0f - 0.025f)/(0.025f*sqrblurdepth*sqrblurdepth));  //0.025 is a param in equation 1/(1 + a*d^2)

  //fog
  float endf   = fogEnd / dfarplane;
  float startf = fogStart / dfarplane;
  float fogexp = fogCoef;

  vec[4] = D3DXVECTOR4(
    endf,
    1.0f / (endf - startf),
    fogexp*dfarplane,
    0.0f);

  //image operations
  float x0		= _params._treshold0;
  float x1		= _params._treshold1;
  float delta = 1.0f/max((x1 - x0), 0.001f);
  float intensity = _params._intensity*exp(fogexp*10);  //fog in 10 m distance
  vec[3] = D3DXVECTOR4(
    delta,
    -delta*x0,
    intensity,
    0.0f);

  //set constants
  device->SetPixelShaderConstantF(0, vec[0], 5);

  device->SetVertexShaderConstantF(191, vec191192[0], 2);

  ComRef<IDirect3DSurface9> origRT;
  HRESULT hr = device->GetRenderTarget(0, origRT.Init());
  DoAssert(hr == S_OK);
  ComRef<IDirect3DSurface9> origDepth;
  hr = device->GetDepthStencilSurface(origDepth.Init());
  DoAssert(hr == S_OK);

#ifdef SSAO_DEBUG
  if (ssaosave)
    D3DXSaveSurfaceToFile("C:\\origRT.png", D3DXIFF_PNG, origRT, 0, 0);
#endif

  hr = device->SetRenderTarget(0, res->_rtsSSAO);
  DoAssert(hr == S_OK);
  hr = device->SetDepthStencilSurface(NULL);
  DoAssert(hr == S_OK);

  hr = device->Clear(0, 0, D3DCLEAR_TARGET, 0, 1.0f, 0);
  DoAssert(hr == S_OK);

  if (halfres)
  {
    D3DVIEWPORT9 viewData;
    memset(&viewData,0,sizeof(viewData));
    viewData.X = 0;
    viewData.Y = 0;
    viewData.Width  = res->_SSAOWidth;
    viewData.Height = res->_SSAOHeight;
    viewData.MinZ = 0.0f;
    viewData.MaxZ = 1.0f;
    device->SetViewport(&viewData);
  }

  device->SetVertexShader(res->_vsSSAO);
  device->SetPixelShader(res->_psSSAO);

  //draw 
  hr = device->DrawPrimitive(D3DPT_TRIANGLESTRIP, 0, 2);
  DoAssert(hr == S_OK);
  
  #ifdef _XBOX
    CALL_D3D_MEMBER(_engine, Resolve, D3DRESOLVE_RENDERTARGET0|D3DRESOLVE_ALLFRAGMENTS, NULL, res->_rtSSAO, NULL, 0, 0, NULL, 1.0f, 0L, NULL);
  #endif

  #ifdef SSAO_DEBUG
  if (ssaosave)
  {
    D3DXSaveTextureToFile("c:/ssao.tga", D3DXIFF_TGA, res->_rtSSAO, NULL);
    D3DXSaveTextureToFile("c:/depth.dds", D3DXIFF_DDS, depth, NULL);
  }
  #endif
  //---------------------------------------------------------------------------------------
  //to half resolution
  //---------------------------------------------------------------------------------------
  if (smallres)
  {
    device->SetRenderTarget(0, res->_rtsSmall);

    D3DVIEWPORT9 viewData;
    memset(&viewData,0,sizeof(viewData));
    viewData.X = 0;
    viewData.Y = 0;
    viewData.Width  = res->_blurWidth;
    viewData.Height = res->_blurHeight;
    viewData.MinZ = 0.0f;
    viewData.MaxZ = 1.0f;
    device->SetViewport(&viewData);

     D3DXVECTOR4 vec191b = D3DXVECTOR4 (
      0.5/rendwidth, 
      0.5/rendheight, 
      1.0/rendwidth, 
      1.0/rendheight);

    device->SetTexture(0, res->_rtSSAO);
    device->SetVertexShaderConstantF(191, &vec191b[0], 1);

    device->SetVertexShader(res->_vsSSAODown);
    device->SetPixelShader(res->_psSSAODown);

    //draw 
    hr = device->DrawPrimitive(D3DPT_TRIANGLESTRIP, 0, 2);
    Assert(hr == S_OK);

    #ifdef _XBOX
      CALL_D3D_MEMBER(_engine, Resolve, D3DRESOLVE_RENDERTARGET0|D3DRESOLVE_ALLFRAGMENTS, NULL, res->_rtSmall, NULL, 0, 0, NULL, 1.0f, 0L, NULL);
    #endif

    #ifdef SSAO_DEBUG
      if (ssaosave)
      {
        D3DXSaveTextureToFile("c:/small.tga", D3DXIFF_TGA, res->_rtSmall, NULL);
      }
    #endif
  }

  //---------------------------------------------------------------------------------------
  //blur image
  //---------------------------------------------------------------------------------------
  vec191192[0] = D3DXVECTOR4(
    smallres ? 0.5/res->_blurWidth: 0.5/rendwidth, 
    smallres ? 0.5/res->_blurHeight : 0.5/rendheight, 
    0.5/depthwidth, 
    0.5/depthheight);	

  vec191192[1] = D3DXVECTOR4(
    1.0, 
    1.0,
    1.0, 
    1.0);	

  D3DXVECTOR4 vec0a = D3DXVECTOR4(
    smallres ? 1.0/res->_blurWidth: 1.0/rendwidth, 
    smallres ? 1.0/res->_blurHeight: 1.0/rendheight, 
    1.0/depthwidth, 
    1.0/depthheight);	

  device->SetPixelShaderConstantF(0, &vec0a[0], 1);
  device->SetVertexShaderConstantF(191, vec191192[0], 2);
  
  for (int i = 0; i < _pInterp._lastPars._ssaoblurpasses; i++)
  {
    //horizontal blur   
    hr = device->SetRenderTarget(0, res->_rtsBlur);
    Assert(hr == S_OK);

    #ifdef SSAO_DEBUG
      device->SetPixelShader((ssaoblurdepth && _params._depthblurdist > 0) ? res->_psBlurHorzDepth : res->_psBlurHorz);
    #else
      hr = device->SetPixelShader(_params._depthblurdist > 0 ? res->_psBlurHorzDepth : res->_psBlurHorz);
      Assert(hr == S_OK);
    #endif
    
    hr = device->SetTexture(1, smallres ? res->_rtSmall : res->_rtSSAO);
    Assert(hr == S_OK);
    hr = device->DrawPrimitive(D3DPT_TRIANGLESTRIP, 0, 2); 
    DoAssert(hr == S_OK);

    #ifdef _XBOX
      CALL_D3D_MEMBER(_engine, Resolve, D3DRESOLVE_RENDERTARGET0|D3DRESOLVE_ALLFRAGMENTS, NULL, res->_rtBlur, NULL, 0, 0, NULL, 1.0f, 0L, NULL);
    #endif
   
    //vertical blur
    hr = device->SetRenderTarget(0, smallres ? res->_rtsSmall : res->_rtsSSAO);
    Assert(hr == S_OK);

    #ifdef SSAO_DEBUG
      device->SetPixelShader((ssaoblurdepth && _params._depthblurdist > 0) ? res->_psBlurVertDepth : res->_psBlurVert);
    #else
      device->SetPixelShader(_params._depthblurdist > 0 ? res->_psBlurVertDepth : res->_psBlurVert);
    #endif
   
    device->SetTexture(1, res->_rtBlur);
    hr = device->DrawPrimitive(D3DPT_TRIANGLESTRIP, 0, 2);
    DoAssert(hr == S_OK);

    #ifdef _XBOX
      CALL_D3D_MEMBER(_engine, Resolve, D3DRESOLVE_RENDERTARGET0|D3DRESOLVE_ALLFRAGMENTS, NULL, res->_rtSSAO, NULL, 0, 0, NULL, 1.0f, 0L, NULL);
    #endif
  }

  //---------------------------------------------------------------------------------------
  //render SSAO into final image
  //---------------------------------------------------------------------------------------
  #ifdef SSAO_DEBUG
  if (ssaosave)
  {
    D3DXSaveTextureToFile("c:/blur.tga", D3DXIFF_TGA, smallres ? res->_rtSmall : res->_rtSSAO, NULL);  
    ssaosave = false;
  }
  #endif

  // _engine->RestoreOldRenderTarget();
  // Restore original RT & Depth
  hr = device->SetRenderTarget(0, origRT);
  DoAssert(hr == S_OK);
  hr = device->SetDepthStencilSurface(origDepth);
  DoAssert(hr == S_OK);

  D3DVIEWPORT9 viewData;
  memset(&viewData,0,sizeof(viewData));
  viewData.X = 0;
  viewData.Y = 0;
  viewData.Width  = _engine->Width();
  viewData.Height = _engine->Height();
  viewData.MinZ = _engine->_minZRange;
  viewData.MaxZ = _engine->_maxZRange;
  device->SetViewport(&viewData);

   D3DXVECTOR4 vec191c = D3DXVECTOR4(
     smallres ? 0.5/res->_blurWidth: 0.5/rendwidth, 
     smallres ? 0.5/res->_blurHeight : 0.5/rendheight, 
     0.0, 
     0.0);	
 
   device->SetVertexShaderConstantF(191, &vec191c[0], 1);
 
   device->SetTexture(1, smallres ? res->_rtSmall : res->_rtSSAO);
   
   device->SetRenderState(D3DRS_BLENDOP, D3DBLENDOP_ADD);
   device->SetRenderState(D3DRS_ALPHABLENDENABLE, TRUE);
   device->SetRenderState(D3DRS_SRCBLEND, _bDebug ? D3DBLEND_ONE : D3DBLEND_ZERO);
   device->SetRenderState(D3DRS_DESTBLEND, _bDebug ? D3DBLEND_ZERO : D3DBLEND_SRCCOLOR);
 
   device->SetPixelShader(_bDebug ? res->_psSSAODebug : res->_psSSAOFinal);
 
   hr = device->DrawPrimitive(D3DPT_TRIANGLESTRIP, 0, 2);
   DoAssert(hr == S_OK);
  
  if (!inScene) device->EndScene();

  device->SetPixelShader(NULL);
  device->SetVertexShader(NULL);

  // restore SamplerState 0, fixed: news:hrc1q6$5ah$1@new-server.localdomain
  device->SetSamplerState(0, D3DSAMP_BORDERCOLOR, 0x0);

  // restore RenderState
  device->SetRenderState(D3DRS_ZWRITEENABLE, TRUE);
  device->SetRenderState(D3DRS_ZENABLE, TRUE);
  device->SetRenderState(D3DRS_CULLMODE, D3DCULL_CCW);
  device->SetRenderState(D3DRS_ALPHABLENDENABLE, FALSE);
    
  // SetPixelShaderConstant_A_DFORCED_E_Black
  Color newE = HBlack;
  Color zero = Color(0, 0, 0, 0);

  device->SetPixelShaderConstantF(PSC_AE, (float*)&newE, 1);
  device->SetPixelShaderConstantF(PSC_GE, (float*)&newE, 1);
  device->SetPixelShaderConstantF(PSC_DForced, (float*)&zero, 1);

  // SetPixelShaderConstantDBSZero
  static const Color zeroes[3]={Color(0, 0, 0, 0), Color(0, 0, 0, 0), Color(0, 0, 0, 0)};
  device->SetPixelShaderConstantF(PSC_Diffuse, (float*)zeroes, 3);
}

//////////////////////////////////////////////////////////////////////////
//
// FilmGrain postprocess effect
//
//////////////////////////////////////////////////////////////////////////
void EngineDD9::PPFilmGrain::Prepare()
{
  const PPFilmGrainResources *res = Resources();
  if (!res) return;

  const int cb = -1;
  base::Prepare();

  // Set the stream source
  _engine->SetStreamSource(cb, res->_vb, sizeof(VertexP3T2), 4);
  // Set the vertex declaration
  _engine->SetVertexDeclaration(cb, res->_vd);
  // Set the vertex shader
  _engine->SetVertexShader(cb, res->_vs);
}

bool EngineDD9::PPFilmGrain::Init(const LazyLoadFile &hlsl, const LazyLoadFile &vs, bool firstInit)
{
  if (!base::Init(hlsl,vs)) return false;
  
  Ref<PPFilmGrainResources> resrc = new PPFilmGrainResources;

  resrc->_invtexWidth    = 1.0f;
  resrc->_invtexHeight   = 1.0f;

  resrc->_texWidth       = 1;
  resrc->_texHeight      = 1;

  // Create the VB
  ComRef<IDirect3DVertexBuffer9> vb;
  if (FAILED(_engine->GetDirect3DDevice()->CreateVertexBuffer(sizeof(Vertex) * 4, D3DUSAGE_WRITEONLY, 0, D3DPOOL_DEFAULT, vb.Init(), NULL)))
  {
    LogF("Error: Vertex buffer creation failed.");
    return false;
  }

  Vertex *v;
  float offsetU = 1.0f / _engine->Width() * 0.5f; //See the Nearest-Point Sampling in the DX9 documentation
  float offsetV = 1.0f / _engine->Height() * 0.5f;
  vb->Lock(0, 0, (void**)&v, 0);
  v[0].pos = Vector3P(-0.5, -0.5, 0.5); v[0].t0.u = 0 + offsetU; v[0].t0.v = 1 + offsetV;
  v[1].pos = Vector3P(-0.5,  0.5, 0.5); v[1].t0.u = 0 + offsetU; v[1].t0.v = 0 + offsetV;
  v[2].pos = Vector3P( 0.5, -0.5, 0.5); v[2].t0.u = 1 + offsetU; v[2].t0.v = 1 + offsetV;
  v[3].pos = Vector3P( 0.5,  0.5, 0.5); v[3].t0.u = 1 + offsetU; v[3].t0.v = 0 + offsetV;
  vb->Unlock();

  // Create the VD
  const D3DVERTEXELEMENT9 vertexDecl[] =
  {
    {0,  0, D3DDECLTYPE_FLOAT3,   D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION, 0},
    {0, 12, D3DDECLTYPE_FLOAT2,   D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 0},
    D3DDECL_END()
  };
  ComRef<IDirect3DVertexDeclaration9> vd;
  if (FAILED(_engine->GetDirect3DDevice()->CreateVertexDeclaration(vertexDecl, vd.Init())))
  {
    LogF("Error: Vertex declaration creation failed.");
    return false;
  }
  if (!CompileVertexShader(hlsl, "VSPostProcess", resrc->_vs))
    return false;

  if (!CompilePixelShader(hlsl,"PSPostProcessFilmGrainColor", resrc->_psColor, false))
    return false;

  if (!CompilePixelShader(hlsl,"PSPostProcessFilmGrainMono", resrc->_psMono, false))
    return false;

  //create noise texture
  const int texsize = 128;
  ComRef<IDirect3DTexture9> noiseTexture;
  if (FAILED(_engine->_d3DDevice->CreateTexture(texsize, texsize, 0, 0, D3DFMT_X8R8G8B8, D3DPOOL_DEFAULT, noiseTexture.Init(), NULL)))
  {
    RptF("Error: Cannot create noise texture");
    return false;
  }

  #ifndef _XBOX
  ComRef<IDirect3DTexture9> sysMem;
  if (FAILED(_engine->_d3DDevice->CreateTexture(texsize, texsize, 0, 0, D3DFMT_X8R8G8B8, D3DPOOL_SYSTEMMEM, sysMem.Init(), NULL)))
  {
    RptF("Error: Cannot create noise sysmem texture");
    return false;
  }
  #else
  ComRef<IDirect3DTexture9> sysMem = noiseTexture;
  #endif

  //for each mipmap 
  int mipmap = 0, i = texsize << 1;
  while (i >>= 1)
  {
    ComRef<IDirect3DSurface9> surface;
    if (FAILED(sysMem->GetSurfaceLevel(mipmap++, surface.Init())))
    {
      RptF("Error: Cannot get noise texture surface ");
      return false;
    }

    D3DSURFACE_DESC desc;
    if (FAILED(surface->GetDesc(&desc)))
    {
      RptF("Error: Cannot get noise texture surface description");
      return false;
    }
    

    D3DLOCKED_RECT lr;
    if (FAILED(surface->LockRect(&lr, NULL, 0)))
    {
      RptF("Error: Cannot lock noise texture surface");
      return false;
    }

    //for each texel
    for (int y = 0; y < (int) desc.Width; y++)
    {
      unsigned int *data = (unsigned int*) ((char*) lr.pBits + y*lr.Pitch);

      for (int x = 0; x < (int) desc.Width; x++)
      {
        data[x] = 
          (((unsigned int) (GRandGen.RandomValue()*255.0f)) << 16) + 
          (((unsigned int) (GRandGen.RandomValue()*255.0f)) << 8) + 
           ((unsigned int) (GRandGen.RandomValue()*255.0f)); 
      }
    }
    
    surface->UnlockRect();
  }

#if !XBOX_D3D
  _engine->_d3DDevice->UpdateTexture(sysMem, noiseTexture);
#endif

  // assign to shared resources
  resrc->_vd = vd;
  resrc->_vb = VertexStaticData9::BufferFuture(vb);
  resrc->_noiseTexture = noiseTexture;

  resrc->_invtexWidth  = 1.0f/texsize;
  resrc->_invtexHeight = 1.0f/texsize;

  resrc->_texWidth     = texsize;
  resrc->_texHeight    = texsize;

  _resrc = resrc;

  if (firstInit)
  {
    SetDefaultValues();
    _enabled = false;
    _pInterp.Initialize();
  }

  return true;
}

//==================================================================================================
void EngineDD9::PPFilmGrain::SetDefaultValues()
{
  _pInterp._pars._sharpness     = 1.5f;
  _pInterp._pars._intensity	    = 0.1f;
  _pInterp._pars._grainSize	    = 1.7f;
  _pInterp._pars._intensityX0	  = 0.2f;
  _pInterp._pars._intensityX1	  = 1.0f;

  _pInterp._pars._monochromatic = true;
}

//==================================================================================================
bool EngineDD9::PPFilmGrain::Init(PPResources *resources, bool firstInit)
{
  if (resources)
  {
    _resrc = resources;

    if (firstInit)
    {
      SetDefaultValues();
      _enabled = false;
      _pInterp.Initialize();
    }

    return true;
  }

  return false;
}


//==================================================================================================
bool EngineDD9::PPFilmGrain::UnInit()
{
  if (_resrc)
  {
    _resrc->UnInit();

    return true;
  }

  return false;
}

//==================================================================================================
int EngineDD9::PPFilmGrain::SetParams(const float *pars, int nPars)
{
  if(nPars > 0)
    setIntensity(pars[0]);

  if(nPars > 1)
    setSharpness(pars[1]);

  if(nPars > 2)
    setGrainSize(pars[2]);

  if(nPars > 3)
    setIntensityX0(pars[3]);

  if(nPars > 4)
    setIntensityX1(pars[4]);

  if(nPars > 5)
    SetMonochromatic(fabs(pars[4]) < 0.01f);

  return 0;
}

//==================================================================================================
int EngineDD9::PPFilmGrain::SetParams(const AutoArray<float> &pars)
{
  int nPars = pars.Size();

  if(nPars > 0)
    setIntensity(pars[0]);

  if(nPars > 1)
    setSharpness(pars[1]);

  if(nPars > 2)
    setGrainSize(pars[2]);

  if(nPars > 3)
    setIntensityX0(pars[3]);

  if(nPars > 4)
    setIntensityX1(pars[4]);

  if(nPars > 5)
    SetMonochromatic(fabs(pars[4]) < 0.01f);

  return 0;
}

//==================================================================================================
LSError EngineDD9::PPFilmGrain::Serialize( ParamArchive &ar )
{
  CHECK(ar.Serialize("enabled",_enabled,0,false));
  CHECK(ar.Serialize("interp", _pInterp, 0));
  CHECK(ar.Serialize("prior", _priority, 0, -1));

  return LSOK;
}


//==================================================================================================
void EngineDD9::PPFilmGrain::Do(bool isLast)
{
  if (!_enabled)
    return;

  // Draw
  PROFILE_SCOPE_GRF_EX(ppFilmGrain,pp,SCOPE_COLOR_YELLOW);

  // If scene is not being rendered initialize scene
  bool inScene = _engine->_d3dFrameOpen;
  if (!inScene) _engine->D3DBeginScene();

  const PPFilmGrainResources *res = Resources();
  if (!res) return;
  
  //interpolated parameters
  FilmGrainPar pars = _pInterp.Simulate();

  // Prepare drawing
  Prepare();
  const int cb = -1;

  SetRenderStateTemp rsCOLORWRITEENABLE(D3DRS_COLORWRITEENABLE, D3DCOLORWRITEENABLE_RED | D3DCOLORWRITEENABLE_GREEN | D3DCOLORWRITEENABLE_BLUE | D3DCOLORWRITEENABLE_ALPHA);

  // Stage 0 - base texture - do the copy (to make sure same data is both in source texture and destination render target)
  IDirect3DTexture9 *rt = _engine->CreateRenderTargetCopyAsTexture();

  // if effect is last then switch to back buffer
  if (isLast) _engine->SwitchToBackBufferRenderTarget();

  //0 texture is source frame buffer
  SetTextureTemp tTex0(0, rt);
  SetSamplerStateTemp ss0MINFILTER(0, D3DSAMP_MINFILTER, D3DTEXF_LINEAR);
  SetSamplerStateTemp ss0MAGFILTER(0, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR);
  SetSamplerStateTemp ss0MIPFILTER(0, D3DSAMP_MIPFILTER, D3DTEXF_NONE);
  SetSamplerStateTemp ss0ADDRESSU(0, D3DSAMP_ADDRESSU, D3DTADDRESS_CLAMP);
  SetSamplerStateTemp ss0ADDRESSV(0, D3DSAMP_ADDRESSV, D3DTADDRESS_CLAMP);

  //1 texture is uniform noise
  SetTextureTemp tTex1(1, res->_noiseTexture);
  SetSamplerStateTemp ss1MINFILTER(1, D3DSAMP_MINFILTER, D3DTEXF_LINEAR);
  SetSamplerStateTemp ss1MAGFILTER(1, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR);
  SetSamplerStateTemp ss1MIPFILTER(1, D3DSAMP_MIPFILTER, D3DTEXF_LINEAR);
  SetSamplerStateTemp ss1ADDRESSU(1, D3DSAMP_ADDRESSU, D3DTADDRESS_WRAP);
  SetSamplerStateTemp ss1ADDRESSV(1, D3DSAMP_ADDRESSV, D3DTADDRESS_WRAP);

  //noise texture 
  float randmovew = toIntFloor(GRandGen.RandomValue()*res->_texWidth);
  float randmoveh = toIntFloor(GRandGen.RandomValue()*res->_texHeight);

  float texw      = (float) _engine->Width()/ (float) res->_texWidth;
  float texh      = (float) _engine->Height()/ (float) res->_texHeight;

  //parameters
  static bool  stop = false;

  if (stop)
  {
    randmoveh = 0.0f;
    randmovew = 0.0f;
  }

  D3DXVECTOR4 vec1 = D3DXVECTOR4(
    pars._intensity, 
    pars._sharpness, 
    pars._grainSize - 1.0f, 
    0.0f);

  //to compute intensity of pixel according the line, function y = kx + q = saturate((X1 - X0)*x + X0),
  //where X0 is intensity of black pixel and X1 is intensity of white pixel
  D3DXVECTOR4 vec2 = D3DXVECTOR4(
    pars._intensityX1 - pars._intensityX0, 
    pars._intensityX0, 
    0.0f, 
    0.0f);

  SetPixelShaderConstantTemp PS0(0, D3DXVECTOR4(texw, texh, texw*res->_invtexWidth*(0.5f + randmovew), texh*res->_invtexHeight*(0.5f + randmoveh)));
  SetPixelShaderConstantTemp PS1(1, vec1);
  SetPixelShaderConstantTemp PS2(2, vec2);

  //pixel shader according the mode
  _engine->SetPixelShader(cb, pars._monochromatic ? res->_psMono : res->_psColor);

  //draw 
  _engine->DrawPrimitive(cb, D3DPT_TRIANGLESTRIP, 0, 2);

  #ifdef _XBOX
    // Resolve content of the render target
    CALL_D3D_MEMBER(_engine,Resolve,D3DRESOLVE_RENDERTARGET0|D3DRESOLVE_ALLFRAGMENTS, NULL, _engine->_currRenderTarget, NULL, 0, 0, NULL, 1.0f, 0L, NULL);
  #endif

  if (!inScene) _engine->D3DEndScene();
  Finish();
}


//////////////////////////////////////////////////////////////////////////
bool EngineDD9::PPDOF::Init(const LazyLoadFile &hlsl, const LazyLoadFile &vs, const Ref<PPGaussianBlur> &ppGaussianBlur)
{
  if (!base::Init(hlsl,vs,"VSPostProcess")) return false;
  if (!CompilePixelShader(hlsl,"PSPostProcessDOF", _ps, true)) return false;
  _ppGaussianBlur = ppGaussianBlur;
  _useFarDistance = false;
  return true;
}

void EngineDD9::PPDOF::Do(bool isLast)
{
  // Draw
  PROFILE_SCOPE_GRF_EX(ppDOF,pp,SCOPE_COLOR_YELLOW);

  const int cb = -1;
  
  // If scene is not being rendered initialize scene
  bool inScene = _engine->_d3dFrameOpen;
  if (!inScene) _engine->D3DBeginScene();

  // Prepare drawing
  Prepare();

  // Set temporary render states
  SetRenderStateTemp rsCOLORWRITEENABLE(D3DRS_COLORWRITEENABLE, D3DCOLORWRITEENABLE_RED | D3DCOLORWRITEENABLE_GREEN | D3DCOLORWRITEENABLE_BLUE | D3DCOLORWRITEENABLE_ALPHA);

  // Stage 0 - base texture - do the copy (to make sure same data is both in source texture and destination render target)
  IDirect3DTexture9 *rt = _engine->CreateRenderTargetCopyAsTexture();
  SetTextureTemp tTex0(0, rt);
#ifndef _X360SHADERGEN
  SetSamplerStateTemp ss0SRGBTEXTURE(0, D3DSAMP_SRGBTEXTURE, _engine->_caps._rtIsSRGB);
#endif
  SetSamplerStateTemp ss0MINFILTER(0, D3DSAMP_MINFILTER, D3DTEXF_LINEAR);
  SetSamplerStateTemp ss0MAGFILTER(0, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR);
  SetSamplerStateTemp ss0ADDRESSU(0, D3DSAMP_ADDRESSU, D3DTADDRESS_CLAMP);
  SetSamplerStateTemp ss0ADDRESSV(0, D3DSAMP_ADDRESSV, D3DTADDRESS_CLAMP);

  // Stage 1 - blurred texture
  SetTextureTemp tTex1(1, _ppGaussianBlur->GetBlurredTexture());
#ifndef _X360SHADERGEN
  SetSamplerStateTemp ss1SRGBTEXTURE(1, D3DSAMP_SRGBTEXTURE, _engine->_caps._rtIsSRGB);
#endif
  SetSamplerStateTemp ss1MINFILTER(1, D3DSAMP_MINFILTER, D3DTEXF_LINEAR);
  SetSamplerStateTemp ss1MAGFILTER(1, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR);
  SetSamplerStateTemp ss1ADDRESSU(1, D3DSAMP_ADDRESSU, D3DTADDRESS_CLAMP);
  SetSamplerStateTemp ss1ADDRESSV(1, D3DSAMP_ADDRESSV, D3DTADDRESS_CLAMP);

  // Stage 2 - depth map
  SetTextureTemp tTex2(2, _engine->GetDepthAsTexture());
#ifndef _X360SHADERGEN
  SetSamplerStateTemp ss2SRGBTEXTURE(2, D3DSAMP_SRGBTEXTURE, FALSE);
#endif
  SetSamplerStateTemp ss2MINFILTER(2, D3DSAMP_MINFILTER, D3DTEXF_POINT);
  SetSamplerStateTemp ss2MAGFILTER(2, D3DSAMP_MAGFILTER, D3DTEXF_POINT);
  SetSamplerStateTemp ss2ADDRESSU(2, D3DSAMP_ADDRESSU, D3DTADDRESS_CLAMP);
  SetSamplerStateTemp ss2ADDRESSV(2, D3DSAMP_ADDRESSV, D3DTADDRESS_CLAMP);

  // Prepare PS constants
  // with zoomed camera / binoculars there is more blur
  Matrix4Val proj = _engine->CBState(cb)._proj;
  
  AspectSettings as;
  GEngine->GetAspectSettings(as);

  static float adjustZoom = 0.5f;
  float zoom = proj(0,0)/as.leftFOV*adjustZoom;

  //1.5 taken from hlsl 
  float blurFactor = _useFarDistance ? 1.5f : zoom;

  static float minBlurFactor = 1;
  static float maxBlurFactor = 10;
  saturate(blurFactor,minBlurFactor,maxBlurFactor);
  static float minZoom = 1;
  static float maxZoom = 2.5; // we want to avoid too large offsets
  saturate(zoom,minZoom,maxZoom);

  const float blurRadiusInPixels = 3.0f*zoom*_engine->_blurCoefThis;
  
  float invW = 1.0f / _engine->Width();
  float invH = 1.0f / _engine->Height();

  #ifdef _M_PPC
    float zfar		= GScene->GetCamera()->Far();
    float znear		= GScene->GetCamera()->Near();
    float zdelta	= zfar - znear;

    const float depth		  = 110.0f;
    const float testdepth	= 100.0f;

    float maxdepth	= zdelta/depth;

    float invFocalPlane = _engine->_focusDist/testdepth;

    //to recompute from nonlinear z buffer to linear
    SetPixelShaderConstantTemp spc2(2, D3DXVECTOR4(maxdepth*znear, zfar, zdelta, 0));

    //do not know now why on xbox the effect is more visible, so make it less visible
    blurFactor *= 0.18f;

  #else
   float invFocalPlane = 1.0f/_engine->_focusDistThis;

   static float blurmulfar	= 1.25f;
   static float blurmulzoom	= 2.0f;

   blurFactor = blurFactor*(_useFarDistance ? blurmulfar : blurmulzoom);
  #endif

//  SetPixelShaderConstantTemp spc1(1, D3DXVECTOR4(invFocalPlane, invFocalPlane, invFocalPlane, invFocalPlane));
  static float xblurfactor = 1.0f;
  static float xblurpower  = 2.0f;

  SetPixelShaderConstantTemp spc0(0, D3DXVECTOR4(invW * blurRadiusInPixels, invH * blurRadiusInPixels, blurFactor, 0.0f));
  SetPixelShaderConstantTemp spc1(1, D3DXVECTOR4(invFocalPlane, xblurfactor, xblurpower, 0.0f));

  // Drawing
  _engine->DrawPrimitive(cb, D3DPT_TRIANGLESTRIP, 0, 2);

#ifdef _XBOX
  // Resolve content of the render target
  CALL_D3D_MEMBER(_engine,Resolve,D3DRESOLVE_RENDERTARGET0|D3DRESOLVE_ALLFRAGMENTS, NULL, _engine->_currRenderTarget, NULL, 0, 0, NULL, 1.0f, 0L, NULL);
#endif

  // If scene is not being rendered finish initialized scene
  if (!inScene) _engine->D3DEndScene();

  // Restore usual rendering settings
  Finish();
}

//////////////////////////////////////////////////////////////////////////

bool EngineDD9::PPDistanceDOF::Init(
  const LazyLoadFile &hlsl, const LazyLoadFile &vs, PPGaussianBlur *ppGaussianBlur
)
{
  if (!grandBase::Init(hlsl,vs,"VSPostProcess")) return false;
  if (!CompilePixelShader(hlsl,"PSPostProcessDistanceDOF", _ps, true)) return false;
  _ppGaussianBlur = ppGaussianBlur;
  _useFarDistance = true;
  return true;
}

bool EngineDD9::PPRain::Init(const LazyLoadFile &hlsl, const LazyLoadFile &vs)
{
  if (!base::Init(hlsl,vs,"VSRain")) return false;
  if (!CompilePixelShader(hlsl,"PSRain", _ps, true)) return false;
  return true;
}



//////////////////////////////////////////////////////////////////////////

struct EdgeParams
{
  float u[4][4];
  float v[4][4];
  float posOffset[4];

  void SetupBilinear(int dstW, int dstH, int srcW, int srcH)
  {
    // for pos. offset explanation cf. DX docs:
    // Directly Mapping Texels to Pixels 
    // Texture Coordinates
    // TODO: verify if this works well for 2x and 4x decimation case as well
    posOffset[0] = 0; //-0.5f/dstW;
    posOffset[1] = 0; //-0.5f/dstH;
    posOffset[2] = posOffset[3] = 0;
    // we may perform any decimation, like 2x or 4x decimation
    int decimationW = srcW/dstW;
    int decimationH = srcH/dstH;

    const float sampOffsetU = 1.0/srcW;
    const float sampOffsetV = 1.0/srcH;
    static const float baseU[4] = {0, 0, 1, 1};
    static const float baseV[4] = {1, 0, 1, 0};
    static const float nU[4] = {0, 0,  1, 1};
    static const float nV[4] = {0, 1, 0,  1};
    for (int vertex = 0; vertex < 4; vertex++)
    {
      for (int t = 0; t < 4; t++)
      {
        u[vertex][t] = baseU[vertex] + sampOffsetU * nU[t]*decimationW;
        v[vertex][t] = baseV[vertex] + sampOffsetV * nV[t]*decimationH;
      }
    }
  }
  void SetupPoint(int dstW, int dstH, int srcW, int srcH)
  {
    posOffset[0] = 0; //-0.5f/dstW;
    posOffset[1] = 0; //-0.5f/dstH;
    posOffset[2] = posOffset[3] = 0;
    // we may perform any decimation, like 2x or 4x decimation
    int decimationW = srcW/dstW;
    int decimationH = srcH/dstH;
    const float sampOffsetU = 1.0/srcW;
    const float sampOffsetV = 1.0/srcH;
    static const float baseU[4] = {0, 0, 1, 1};
    static const float baseV[4] = {1, 0, 1, 0};
    static const float nU[4] = {0, 0,  1, 1};
    static const float nV[4] = {0, 1, 0,  1};
    for (int vertex = 0; vertex < 4; vertex++)
    {
      for (int t = 0; t < 4; t++)
      {
        // Cf. DX docs Nearest-Point Sampling for UV offset explanation
        u[vertex][t] = baseU[vertex] + sampOffsetU * (nU[t]*decimationW + 0.5f);
        v[vertex][t] = baseV[vertex] + sampOffsetV * (nV[t]*decimationH + 0.5f);
      }
    }
  }
};

bool EngineDD9::PPDecimate::Init(const LazyLoadFile &hlsl, const LazyLoadFile &vs)
{
  if (!base::Init(hlsl,vs)) return false;

  // Create the low resolution textures cascade

  // we need textures for 4x average decimation
  // and then some for 2x avg/min/max decimation
  // we want to start calculating min/max once the picture is coarse enough
  const int startMinMaxAtSize = 80;

  int fullWidth = _engine->Width();
  int fullHeight = _engine->Height();

  // decimation 2x uses point sampling and should therefore be very accurate
  // we will round to nearest bigger pow2
  int w = roundTo2PowerNCeil(fullWidth);
  int h = roundTo2PowerNCeil(fullHeight);

  int maxSize = max(w,h);
  int minSize = min(w,h);

  D3DFORMAT rtFormat = _engine->_downsampleFormat;
  int level = 0;
  // we need at least one 4x decimation step
  do
  {
    DecimationPass &dec = _dec[level];
    w = w>>2;
    h = h>>2;
    maxSize = maxSize>>2;
    minSize = minSize>>2;
    // create the supplementary RT
    int effW = max(w,1);
    int effH = max(h,1);
    LogF("Decimation 4x: %d,%d",effW,effH);
    HRESULT err = _engine->CreateTextureRT(effW, effH, 1, rtFormat, D3DPOOL_DEFAULT, dec._rt, dec._rts, true);
    if (FAILED(err))
    {
      _engine->DDError9("Post process Low resolution texture creation failed",err);
      return false;
    }
    dec._w = effW;
    dec._h = effH;
    // proceed to next level
    level++;
  } while (minSize>startMinMaxAtSize);
  _decimations = level;

  // Create measurement textures
  const DecimationPass &smallestDec = _dec[_decimations - 1];
  HRESULT err = _engine->CreateTextureRT(smallestDec._w, smallestDec._h, 1, rtFormat, D3DPOOL_DEFAULT, _rtMeasure, _rtsMeasure, true);
  if (FAILED(err))
  {
    _engine->DDError9("Post process Low resolution texture creation failed",err);
    return false;
  }

  // Create depth buffer (as it must be set in order the query to work with some ATI graphics cards)
  err = _engine->CreateDepthStencilSurface(smallestDec._w, smallestDec._h, D3DFMT_D24S8, D3DMULTISAMPLE_NONE, 0, TRUE, _rtsMeasureDepth, _rtsMeasure, NULL);
  if (FAILED(err))
  {
    _engine->DDError9("Measure depth buffer creation failed",err);
    return false;
  }

  {
    ComRef<IDirect3DVertexBuffer9> vb;
    // Create the VB + declaration
    if (FAILED(_engine->GetDirect3DDevice()->CreateVertexBuffer(sizeof(VertexCustomEdge) * 4, D3DUSAGE_WRITEONLY, 0, D3DPOOL_DEFAULT, vb.Init(), NULL)))
    {
      LogF("Error: Vertex buffer creation failed.");
      return false;
    }
    void *data;
    vb->Lock(0, 0, &data, 0);
    VertexCustomEdge *v = (VertexCustomEdge *)data;
    v[0].pos = Vector3P(-0.5, -0.5, 0.5); v[0].index = 0;
    v[1].pos = Vector3P(-0.5,  0.5, 0.5); v[1].index = 1;
    v[2].pos = Vector3P( 0.5, -0.5, 0.5); v[2].index = 2;
    v[3].pos = Vector3P( 0.5,  0.5, 0.5); v[3].index = 3;
    vb->Unlock();

    _vbCustomEdge = VertexStaticData9::BufferFuture(vb);

    // Create the VD of the blur vertex
    const D3DVERTEXELEMENT9 vertexDeclCustomEdge[] =
    {
      {0,  0, D3DDECLTYPE_FLOAT3,   D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION, 0},
      {0, 12, D3DDECLTYPE_FLOAT1,   D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_BLENDINDICES, 0},
      D3DDECL_END()
    };
    if (FAILED(_engine->GetDirect3DDevice()->CreateVertexDeclaration(vertexDeclCustomEdge, _vdCustomEdge.Init())))
    {
      LogF("Error: Vertex declaration creation failed.");
      return false;
    }
  }

  // Create the shaders
  if (!CompileVertexShader(vs,"VSPostProcessCustomEdge4T",_vsCustomEdge))
  {
    return false;
  }
  if (!CompilePixelShader(hlsl,"PSPostProcessDownSampleThermalLuminance",_psLuminance))
  {
    return false;
  }
  if (!CompilePixelShader(hlsl,"PSPostProcessDownSampleAvgLuminance",_psAvgLuminance))
  {
    return false;
  }
  if (!CompilePixelShader(hlsl,"PSPostProcessQuerySimple",_psQuery))
  {
    return false;
  }

  // Initialize the histogram values
  for (int i = 0; i < NBrightQueries; i++)
  {
    _histogram[i] = -1.0f;
  }

  return true;
}

void EngineDD9::PPDecimate::Do(bool isLast)
{
  PROFILE_SCOPE_GRF_EX(ppDec,pp,SCOPE_COLOR_YELLOW);
  
  const int cb = -1;
  
  Prepare();

  // Set the vertex shader + stream source
  _engine->SetStreamSource(cb,_vbCustomEdge, sizeof(VertexCustomEdge), 4);
  _engine->SetVertexDeclaration(cb,_vdCustomEdge);
  _engine->SetVertexShader(cb,_vsCustomEdge);

  // Set temporary states
  SetRenderStateTemp rsSRGBSRGBWRITEENABLE(D3DRS_SRGBWRITEENABLE, FALSE);
  SetRenderStateTemp rsCOLORWRITEENABLE(D3DRS_COLORWRITEENABLE, D3DCOLORWRITEENABLE_RED | D3DCOLORWRITEENABLE_GREEN | D3DCOLORWRITEENABLE_BLUE | D3DCOLORWRITEENABLE_ALPHA);
  // no alpha or fog done here
  SetRenderStateTemp rsFOGENABLE(D3DRS_FOGENABLE, FALSE);
  SetRenderStateTemp rsALPHATESTENABLE(D3DRS_ALPHATESTENABLE, FALSE);
  SetRenderStateTemp rsALPHABLENDENABLE(D3DRS_ALPHABLENDENABLE, FALSE);
  // no SRGB
  SetSamplerStateTemp ss0SRGBTEXTURE(0, D3DSAMP_SRGBTEXTURE, FALSE);
  SetSamplerStateTemp ss1SRGBTEXTURE(1, D3DSAMP_SRGBTEXTURE, FALSE);
  SetSamplerStateTemp ss2SRGBTEXTURE(2, D3DSAMP_SRGBTEXTURE, FALSE);
  SetSamplerStateTemp ss3SRGBTEXTURE(3, D3DSAMP_SRGBTEXTURE, FALSE);
  // by default assume bi-linear filtering
  SetSamplerStateTemp ss0MINFILTER(0, D3DSAMP_MINFILTER, D3DTEXF_LINEAR);
  SetSamplerStateTemp ss0MAGFILTER(0, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR);
  SetSamplerStateTemp ss1MINFILTER(1, D3DSAMP_MINFILTER, D3DTEXF_LINEAR);
  SetSamplerStateTemp ss1MAGFILTER(1, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR);
  SetSamplerStateTemp ss2MINFILTER(2, D3DSAMP_MINFILTER, D3DTEXF_LINEAR);
  SetSamplerStateTemp ss2MAGFILTER(2, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR);
  SetSamplerStateTemp ss3MINFILTER(3, D3DSAMP_MINFILTER, D3DTEXF_LINEAR);
  SetSamplerStateTemp ss3MAGFILTER(3, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR);
  // tri-linear filtering never done 
  SetSamplerStateTemp ss0MIPFILTER(0, D3DSAMP_MIPFILTER, D3DTEXF_POINT);
  SetSamplerStateTemp ss1MIPFILTER(1, D3DSAMP_MIPFILTER, D3DTEXF_POINT);
  SetSamplerStateTemp ss2MIPFILTER(2, D3DSAMP_MIPFILTER, D3DTEXF_POINT);
  SetSamplerStateTemp ss3MIPFILTER(3, D3DSAMP_MIPFILTER, D3DTEXF_POINT);
  // clamping all stages
  SetSamplerStateTemp ss0ADDRESSU(0, D3DSAMP_ADDRESSU, D3DTADDRESS_CLAMP);
  SetSamplerStateTemp ss0ADDRESSV(0, D3DSAMP_ADDRESSV, D3DTADDRESS_CLAMP);
  SetSamplerStateTemp ss1ADDRESSU(1, D3DSAMP_ADDRESSU, D3DTADDRESS_CLAMP);
  SetSamplerStateTemp ss1ADDRESSV(1, D3DSAMP_ADDRESSV, D3DTADDRESS_CLAMP);
  SetSamplerStateTemp ss2ADDRESSU(2, D3DSAMP_ADDRESSU, D3DTADDRESS_CLAMP);
  SetSamplerStateTemp ss2ADDRESSV(2, D3DSAMP_ADDRESSV, D3DTADDRESS_CLAMP);
  SetSamplerStateTemp ss3ADDRESSU(3, D3DSAMP_ADDRESSU, D3DTADDRESS_CLAMP);
  SetSamplerStateTemp ss3ADDRESSV(3, D3DSAMP_ADDRESSV, D3DTADDRESS_CLAMP);

  // Save VS constants and restore them at the end
  PushVertexShaderConstant pushC0(0);
  PushVertexShaderConstant pushC1(1);
  PushVertexShaderConstant pushC2(2);
  PushVertexShaderConstant pushC3(3);
  PushVertexShaderConstant pushC4(4);
  PushVertexShaderConstant pushC5(5);
  PushVertexShaderConstant pushC6(6);
  PushVertexShaderConstant pushC7(7);
  PushVertexShaderConstant pushC8(8);

  // perform initial luminance calculation + 4x decimation
  {
    EdgeParams pars;
    pars.SetupBilinear(_dec[0]._w,_dec[0]._h,_engine->Width(),_engine->Height());
    _engine->SetVertexShaderConstantF(cb,0, pars.u[0], 9);

    // Set the pixel shader
    _engine->SetPixelShader(cb,_psLuminance);

    // Remember the current render target (prior we set the temporary one)
    IDirect3DTexture9 *rt = _engine->CreateRenderTargetCopyAsTexture();

     // Set temporarily render target and depth stencil to horizontally shrunk texture
    _engine->SetTemporaryRenderTarget(_dec[0]._rt, _dec[0]._rts, NULL);

    {
      D3DVIEWPORT9 viewData;
      memset(&viewData, 0, sizeof(viewData));
      viewData.X = 0;
      viewData.Y = 0;
      viewData.Width  = _dec[0]._w;
      viewData.Height = _dec[0]._h;
      viewData.MinZ = 0.0f;
      viewData.MaxZ = 1.0f;
      CALL_D3D_MEMBER(_engine, SetViewport, viewData);
    }

    // set the source as 4 textures    
    SetTextureTemp tTex0(0, rt);
    SetTextureTemp tTex1(1, rt);
    SetTextureTemp tTex2(2, rt);
    SetTextureTemp tTex3(3, rt);

    _engine->DrawPrimitive(cb,D3DPT_TRIANGLESTRIP, 0, 2);
    
    _engine->ResolveRenderTarget();
  }

  // perform 4x decimation + average calculation
  {
    for (int i=1; i<_decimations; i++)
    {
      // perform initial luminance calculation + 4x decimation
      EdgeParams pars;
      pars.SetupBilinear(_dec[i]._w,_dec[i]._h,_dec[i-1]._w,_dec[i-1]._h);

      _engine->SetVertexShaderConstantF(cb,0, pars.u[0], 9);

      // Set the pixel shader
      _engine->SetPixelShader(cb,_psAvgLuminance);

      // Set the render target (we can use the SetRenderTarget method here, because we know the original is already stored and w)
      _engine->SetRenderTarget(_dec[i]._rts, _dec[i]._rt, NULL);

      {
        D3DVIEWPORT9 viewData;
        memset(&viewData, 0, sizeof(viewData));
        viewData.X = 0;
        viewData.Y = 0;
        viewData.Width  = _dec[i]._w;
        viewData.Height = _dec[i]._h;
        viewData.MinZ = 0.0f;
        viewData.MaxZ = 1.0f;
        CALL_D3D_MEMBER(_engine, SetViewport, viewData);
      }

      IDirect3DTexture9 *tex = _dec[i-1]._rt;

      // set the source as 4 textures    
      SetTextureTemp tTex0(0, tex);
      SetTextureTemp tTex1(1, tex);
      SetTextureTemp tTex2(2, tex);
      SetTextureTemp tTex3(3, tex);

      _engine->DrawPrimitive(cb,D3DPT_TRIANGLESTRIP, 0, 2);
      _engine->ResolveRenderTarget();
    }
  }

  // Do the measurement
  {
    // It looks Z-buffer must be enabled in order the occlusion query to work on some ATI HW
    // see http://discussms.hosting.lsoft.com/SCRIPTS/WA-MSD.EXE?A2=ind0502B&L=DIRECTXDEV&D=0&I=-3&X=60AD7D5C1D013612AF&P=3658
    SetRenderStateTemp rsZENABLE(D3DRS_ZENABLE, TRUE);
    SetRenderStateTemp rsZFUNC(D3DRS_ZFUNC, D3DCMP_ALWAYS);

    _engine->SetRenderTarget(_rtsMeasure, _rtMeasure, _rtsMeasureDepth);
    _engine->SetTexture(cb,0, _dec[_decimations-1]._rt);
    _engine->SetPixelShader(cb,_psQuery);

    // Issue a query
    float alphas[NBrightQueries];
    for (int i = 0; i < NBrightQueries; i++)
    {
      alphas[i] = (float)i / NBrightQueries;
    }
    _query.Issue(NULL, alphas, -1.0f);
  }

  // Build histogram
  {
    // Get pixel counts for every layer
    int numberOfPixelsDrawn[NBrightQueries];
    _query.GetNumberOfPixelsDrawn(numberOfPixelsDrawn, NULL, true);

    // Build the histogram
    int totalPixels = _dec[_decimations - 1]._w * _dec[_decimations - 1]._h;
    for (int i = 0; i < NBrightQueries - 1; i++)
    {
      _histogram[i] = (float)(numberOfPixelsDrawn[i] - numberOfPixelsDrawn[i + 1]) / totalPixels;
      _temperatures[i] = (numberOfPixelsDrawn[i]);
    }
    _histogram[NBrightQueries - 1] = (float)numberOfPixelsDrawn[NBrightQueries - 1] / totalPixels;
    _temperatures[NBrightQueries - 1] = numberOfPixelsDrawn[NBrightQueries - 1];

    if (CHECK_DIAG(DETIHistogram))
    {
      DIAG_MESSAGE(200, Format(" 0: %f, %d", _histogram[0], _temperatures[0]));
      DIAG_MESSAGE(200, Format(" 1: %f, %d", _histogram[1], _temperatures[1]));
      DIAG_MESSAGE(200, Format(" 2: %f, %d", _histogram[2], _temperatures[2]));
      DIAG_MESSAGE(200, Format(" 3: %f, %d", _histogram[3], _temperatures[3]));
      DIAG_MESSAGE(200, Format(" 4: %f, %d", _histogram[4], _temperatures[4]));
      DIAG_MESSAGE(200, Format(" 5: %f, %d", _histogram[5], _temperatures[5]));
      DIAG_MESSAGE(200, Format(" 6: %f, %d", _histogram[6], _temperatures[6]));
      DIAG_MESSAGE(200, Format(" 7: %f, %d", _histogram[7], _temperatures[7]));
      DIAG_MESSAGE(200, Format(" 8: %f, %d", _histogram[8], _temperatures[8]));
      DIAG_MESSAGE(200, Format(" 9: %f, %d", _histogram[9], _temperatures[9]));
      DIAG_MESSAGE(200, Format("10: %f, %d", _histogram[10], _temperatures[10]));
      DIAG_MESSAGE(200, Format("11: %f, %d", _histogram[11], _temperatures[11]));
      DIAG_MESSAGE(200, Format("12: %f, %d", _histogram[12], _temperatures[12]));
      DIAG_MESSAGE(200, Format("13: %f, %d", _histogram[13], _temperatures[13]));
      DIAG_MESSAGE(200, Format("14: %f, %d", _histogram[14], _temperatures[14]));
      DIAG_MESSAGE(200, Format("15: %f, %d", _histogram[15], _temperatures[15]));
    }
  }

  // Restore the original render target
  _engine->RestoreOldRenderTarget();
}

int EngineDD9::PPFinalThermal::SetParams(const float *pars, int nPars)
{
  Assert(nPars==1);
  _deltaT = pars[0];

  return 0;
}

bool EngineDD9::PPFinalThermal::Init(const LazyLoadFile &hlsl, const LazyLoadFile &vs)
{
  if (!base::Init(hlsl,vs,"VSPostProcess")) return false;
  if (!CompilePixelShader(hlsl,"PSPostProcessThermalPresent", _ps)) return false;
  if (!CompilePixelShader(hlsl,"PSPostProcessThermal", _psThermal)) return false;
  if (!_ppDecimate.Init(hlsl, vs)) return false;
  if (!_ppGaussianBlur.Init(hlsl, vs)) return false;

  UINT width = _engine->Width();
  UINT height = _engine->Height();

  HRESULT err = _engine->CreateTextureRT(width, height, 1, _engine->_rtFormat, D3DPOOL_DEFAULT, _rtTexture, _rtSurface, true);
  if (err != S_OK)
  {
    _engine->DDError9("Error: Render target texture creation failed.", err); 
    return false;
  }

  _deltaT = 0;

  return true;
}

// TODO: avoid scene here
#include "../scene.hpp"
#include "../txtPreload.hpp"

extern bool ForceTIAlive;
extern bool ForceTIMovement;
extern bool ForceTIMetabolism;

void EngineDD9::PPFinalThermal::Do(bool isLast)
{
  PROFILE_SCOPE_GRF_EX(ppThr,pp,SCOPE_COLOR_YELLOW);
 
  const int cb = -1;
  
  // If the _currRenderTargetSurface is equal to _backBuffer, then it's strange and we probably have got a problem here
  DoAssert(_engine->_currRenderTargetSurface != _engine->_backBuffer);

#if (_ENABLE_CHEATS | _BULDOZER)

# if(_BULDOZER)
#  define TI_KEY GInput.GetKeyToDo
# else 
#  define TI_KEY GInput.GetCheat2ToDo
# endif
  /*if (TI_KEY(DIK_F9))
  {
    _engine->SetTIBrightnessRelative(_engine->GetTIBrightness() + 1.0f);
    GlobalShowMessage(500, Format("TI Brightness: %f", _engine->GetTIBrightness()));
  }
  if (TI_KEY(DIK_F10))
  {
    _engine->SetTIBrightnessRelative(_engine->GetTIBrightness() - 1.0f);
    GlobalShowMessage(500, Format("TI Brightness: %f", _engine->GetTIBrightness()));
  }
  if (TI_KEY(DIK_F11))
  {
    _engine->_tiContrast += 1.0f;
    GlobalShowMessage(500, Format("TI Contrast: %f", _engine->_tiContrast));
  }
  if (TI_KEY(DIK_F12))
  {
    _engine->_tiContrast -= 1.0f;
    GlobalShowMessage(500, Format("TI Contrast: %f", _engine->_tiContrast));
  }*/
  if (TI_KEY(DIK_F8))
  {
    _engine->_tiMode++;
    GlobalShowMessage(500, Format("TI Mode: %d", _engine->_tiMode));
  }
  if (TI_KEY(DIK_F7))
  {
    _engine->_tiMode--;
    GlobalShowMessage(500, Format("TI Mode: %d", _engine->_tiMode));
  }
  if (TI_KEY(DIK_F6))
  {
    _engine->_tiBlurCoef += 0.1f;
    if (_engine->_tiBlurCoef > 1.001f) _engine->_tiBlurCoef = 0.0f;
    GlobalShowMessage(500, Format("TI Blur level: %f", _engine->_tiBlurCoef));
  }
  if (TI_KEY(DIK_F1))
  {
    ForceTIAlive = !ForceTIAlive;
    GlobalShowMessage(500, Format("ForceTIAlive: %s", ForceTIAlive ? "on" : "off"));
  }
  if (TI_KEY(DIK_F2))
  {
    ForceTIMovement = !ForceTIMovement;
    GlobalShowMessage(500, Format("ForceTIMovement: %s", ForceTIMovement ? "on" : "off"));
  }
  if (TI_KEY(DIK_F3))
  {
    ForceTIMetabolism = !ForceTIMetabolism;
    GlobalShowMessage(500, Format("ForceTIMetabolism: %s", ForceTIMetabolism ? "on" : "off"));
  }
  if (TI_KEY(DIK_F5))
  {
    _engine->_tiAutoBC = !_engine->_tiAutoBC;
    GlobalShowMessage(500, Format("TI AutoContrast: %s", _engine->_tiAutoBC ? "on" : "off"));
  }
#undef TI_KEY 
#endif

  // Perform the automatic setting of brightness and contrast
  if (_engine->_tiAutoBC)
  {
    // Get the temperature histogram (decode the temperature)
    _ppDecimate.Do(isLast);

    // Calculate the expected value
    //float e = 0.0f;
    float maxTemperature = 0.0f;

    float dTemp = (float) (TempMax - TempMin) / NBrightQueries;

    // 1% per pixels from image(4096)
    static int MinPixelsDrawn = 100;

    for (int i = 0; i < NBrightQueries; i++)
    {
      //e += i * _ppDecimate._histogram[i];
      if (_ppDecimate._temperatures[i] > MinPixelsDrawn) maxTemperature = (i + 1) * dTemp;
    }

#if 0
    // Calculate the variance
    float var = 0.0f;
    for (int i = 0; i < NBrightQueries; i++)
    {
      var += (i-e) * (i-e) * _ppDecimate._histogram[i];
    }
   
    // Convert e and var to temperatures
    float tempE = TempMin + (TempMax - TempMin) * (e + 0.5f) / NBrightQueries;
    float tempVAR = TempMin + (TempMax - TempMin) * var / NBrightQueries;

    // Calculate the desired brightness and contrast
    static float ConstrastMin = 8.0f;
    static float ContrastMult = 5.0f;

    // Calculate the desired brightness and contrast
    float brightnessWanted = tempE;
    float contrastWanted = max(tempVAR * ContrastMult, ConstrastMin);
#endif

    float maxTempWanted = maxTemperature + (dTemp * 0.25f);

    // Consider some latency and change the real values accordingly
    static float latency = 0.85f;

#if 0
    _engine->SetTIBrightness(_engine->_tiBrightness + (brightnessWanted - _engine->_tiBrightness) * _deltaT * latency);
    _engine->SetTIContrast(_engine->_tiContrast + (contrastWanted - _engine->_tiContrast) * _deltaT * latency);
#endif
    _engine->_maxTemperature = (_engine->_maxTemperature + (maxTempWanted - _engine->_maxTemperature) * _deltaT * latency);
  }
  else
  {
    _engine->SetTIBrightness(_engine->_tiBrightness);
    _engine->SetTIContrast(_engine->_tiContrast);
    _engine->_maxTemperature = 30.0f;
  }

  // Do the thermal visualization (decode the temperature)
  {
    // Start the scene
    bool inScene = _engine->_d3dFrameOpen;
    if (!inScene) _engine->D3DBeginScene();

    // Prepare drawing
    Prepare();

    // Rewrite the pixel shader
    _engine->SetPixelShader(cb,_psThermal);

    // Current render target as texture
    IDirect3DTexture9 *rt = _engine->CreateRenderTargetCopyAsTexture();
    DoAssert(rt);

    // Rewrite render target and depth stencil surfaces
    _engine->SetTemporaryRenderTarget(_rtTexture, _rtSurface, NULL);

    D3DSURFACE_DESC desc;
    _rtTexture->GetLevelDesc(0, &desc);
    {
      D3DVIEWPORT9 viewData;
      memset(&viewData,0,sizeof(viewData));
      viewData.X = 0;
      viewData.Y = 0;
      viewData.Width  = desc.Width;
      viewData.Height = desc.Height;
      viewData.MinZ = 0.0f;
      viewData.MaxZ = 1.0f;
      CALL_D3D_MEMBER(_engine,SetViewport,viewData);
    }

    SetTextureTemp tTex0(0, rt);
    SetSamplerStateTemp ss0SRGBTEXTURE(0, D3DSAMP_SRGBTEXTURE, FALSE);
    SetSamplerStateTemp ss0MINFILTER(0, D3DSAMP_MINFILTER, D3DTEXF_LINEAR);
    SetSamplerStateTemp ss0MAGFILTER(0, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR);
    SetSamplerStateTemp ss0ADDRESSU(0, D3DSAMP_ADDRESSU, D3DTADDRESS_CLAMP);
    SetSamplerStateTemp ss0ADDRESSV(0, D3DSAMP_ADDRESSV, D3DTADDRESS_CLAMP);

    // Get the TIConversion texture to the input
    SetTextureTemp tTex1(1, _engine->_tiConversion);
    SetSamplerStateTemp ss1SRGBTEXTURE(1, D3DSAMP_SRGBTEXTURE, FALSE);
    SetSamplerStateTemp ss1MINFILTER(1, D3DSAMP_MINFILTER, D3DTEXF_LINEAR);
    SetSamplerStateTemp ss1MAGFILTER(1, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR);
    SetSamplerStateTemp ss1ADDRESSU(1, D3DSAMP_ADDRESSU, D3DTADDRESS_CLAMP);
    SetSamplerStateTemp ss1ADDRESSV(1, D3DSAMP_ADDRESSV, D3DTADDRESS_CLAMP);

    // Set temporary render states
    SetRenderStateTemp rsCOLORWRITEENABLE(D3DRS_COLORWRITEENABLE, D3DCOLORWRITEENABLE_RED | D3DCOLORWRITEENABLE_GREEN | D3DCOLORWRITEENABLE_BLUE | D3DCOLORWRITEENABLE_ALPHA);
    SetRenderStateTemp rsZENABLE(D3DRS_ZENABLE, FALSE);
#ifndef _X360SHADERGEN
    SetRenderStateTemp rsFOGENABLE(D3DRS_FOGENABLE, FALSE);
#endif
    SetRenderStateTemp rsSRGBWRITEENABLE(D3DRS_SRGBWRITEENABLE, FALSE);

    // Control the temperature range to be displayed by constants
    // const float tempRangeMin = 0.0f;
    // const float tempRangeMax = 60.0f;
    // #define TempRangeMin01 0.18181818182f //(tempRangeMin - TempMin) * InvTempDiff
    // #define TempRangeMax01 0.63636363636f //(tempRangeMax - TempMin) * InvTempDiff
    // #define InvTempRangeDiff 2.2f // 1.0f / (tempRangeMax01 - tempRangeMin01)

    static float TemperatureMin = 0.5f;
    static float TemperatureMax = 30.0f;
    static float TemeperatureInvDiff = 1.0f / (TemperatureMax - TemperatureMin);

    float tempDiff = (_engine->_maxTemperature - _engine->_airTemperature);
    saturate(tempDiff, TemperatureMin, TemperatureMax);
    float dT = (tempDiff - TemperatureMin) * TemeperatureInvDiff;
    tempDiff *= (1.0f - sqrtf(dT)) + 1.0f;

    float tempRangeMin = _engine->_airTemperature - tempDiff;
    float tempRangeMax = _engine->_airTemperature + tempDiff;
    saturateMax(tempRangeMin, 0.0f);

    float tempRangeMin01 = (tempRangeMin - TempMin) * InvTempDiff;
    float tempRangeMax01 = (tempRangeMax - TempMin) * InvTempDiff;
    float invTempRangeDiff = 1.0f / (tempRangeMax01 - tempRangeMin01);

    if (CHECK_DIAG(DETIHistogram))
    {
      DIAG_MESSAGE(150, "dT: %.3f, temp: %.3f", dT, tempDiff);
      DIAG_MESSAGE(150, "min: %.3f, max: %.3f", tempRangeMin, tempRangeMax);
      DIAG_MESSAGE(150, "min: %.3f, max: %.3f, diff: %.3f", tempRangeMin01, tempRangeMax01, invTempRangeDiff);
    }

    SetPixelShaderConstantTemp spc0(0, D3DXVECTOR4(tempRangeMin01, tempRangeMax01, invTempRangeDiff, _engine->_tiMode));

    // Drawing
    _engine->DrawPrimitive(cb,D3DPT_TRIANGLESTRIP, 0, 2);

    _engine->RestoreOldRenderTarget();
  }

  {
    // Blur the render texture
    _ppGaussianBlur.Do(false, _rtTexture);


    PROFILE_SCOPE_GRF_EX(ppFin,pp,SCOPE_COLOR_YELLOW);

    bool inScene = _engine->_d3dFrameOpen;
    if (!inScene) _engine->D3DBeginScene();

    // Prepare drawing
    Prepare();

    // Set temporary render states
    SetRenderStateTemp rsCOLORWRITEENABLE(D3DRS_COLORWRITEENABLE, D3DCOLORWRITEENABLE_RED | D3DCOLORWRITEENABLE_GREEN | D3DCOLORWRITEENABLE_BLUE | D3DCOLORWRITEENABLE_ALPHA);
    SetRenderStateTemp rsZENABLE(D3DRS_ZENABLE, FALSE);
#ifndef _X360SHADERGEN
    SetRenderStateTemp rsFOGENABLE(D3DRS_FOGENABLE, FALSE);
#endif

    // Use the back buffer as render target
    _engine->SwitchToBackBufferRenderTarget();

    D3DSURFACE_DESC desc;
    _engine->_backBuffer->GetDesc(&desc);
    {
      D3DVIEWPORT9 viewData;
      memset(&viewData,0,sizeof(viewData));
      viewData.X = 0;
      viewData.Y = 0;
      viewData.Width  = desc.Width;
      viewData.Height = desc.Height;
      viewData.MinZ = 0.0f;
      viewData.MaxZ = 1.0f;
      CALL_D3D_MEMBER(_engine,SetViewport,viewData);
    }

    // get blurred target
    IDirect3DTexture9 *rtBlur = _ppGaussianBlur.GetBlurredTexture();
    DoAssert(rtBlur);

    // Use the textures
    SetTextureTemp tRenderTarget0(0, _rtTexture);
    SetTextureTemp tRenderTarget1(1, rtBlur);

    // Disable SRGB
#ifndef _X360SHADERGEN
    SetSamplerStateTemp tempNoSRGB0(0, D3DSAMP_SRGBTEXTURE, FALSE);
    SetSamplerStateTemp tempNoSRGB1(1, D3DSAMP_SRGBTEXTURE, FALSE);
    SetSamplerStateTemp tempMINFILTER0(0, D3DSAMP_MINFILTER, D3DTEXF_LINEAR);
    SetSamplerStateTemp tempMAGFILTER0(0, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR);
    SetSamplerStateTemp tempMINFILTER1(1, D3DSAMP_MINFILTER, D3DTEXF_LINEAR);
    SetSamplerStateTemp tempMAGFILTER1(1, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR);
#endif

    // Set the blur factor
    SetPixelShaderConstantTemp spc0(0, D3DXVECTOR4(0, 0, 0, _engine->_tiBlurCoef));

    // Drawing
    _engine->DrawPrimitive(cb,D3DPT_TRIANGLESTRIP, 0, 2);

    if (!inScene) _engine->D3DEndScene();
  }

  Finish();
}

//////////////////////////////////////////////////////////////////////////

EngineDD9 *EngineDD9::QueryManager::_engine;

EngineDD9::QueryManager::QueryManager()
{
#ifndef _X360SHADERGEN
  // Create the query objects
  for (int i = 0; i < NItems; i++)
  {
    for (int b=0; b<NBrightQueries; b++)
    {
      if (FAILED(_engine->GetDirect3DDevice()->CreateQuery(D3DQUERYTYPE_OCCLUSION, _query[i][b].Init())))
      {
        LogF("Warning: Current HW doesn't support queries");
        goto Break;
      }
    }
  }
  Break:
#endif

  // Initialize the startup values
  ResetQueries();
}

void EngineDD9::QueryManager::ResetQueries()
{
  // Initialize the startup values
  _newQuery = 0;
  _oldestQuery = 0;
  for (int b=0; b<NBrightQueries; b++)
  {
    _mostRecentResult[b] = -1;
  }
  _queriesIssued = 0;
  _currentResultIndex = 0;
}

bool EngineDD9::QueryManager::Issue(const float *value, const float *alpha, float masterValue)
{
  Assert(!_engine->IsCBRecording());
  const int cb = -1;
  
  if (_query[_newQuery][0].IsNull()) return false;
  if (_queriesIssued>=NItems) return false;
  
  PushPixelShaderConstant psc7(7);
  for (int b=0; b<NBrightQueries; b++)
  {
    const float queryDot[4]={b,0,0,alpha[b]};
    _engine->SetPixelShaderConstantF(cb,7, queryDot,1);
    _query[_newQuery][b]->Issue(D3DISSUE_BEGIN);
    _value[_newQuery][b] = (value) ? value[b] : -1.0f;
    _engine->DrawPrimitive(cb,D3DPT_TRIANGLESTRIP, 0, 2);
    _query[_newQuery][b]->Issue(D3DISSUE_END);
  }
  _masterValue[_newQuery] = masterValue;
  _queriesIssued++;
  DoAssert(_queriesIssued<=NItems);
  _newQuery = Next(_newQuery);
  return true;
}

float EngineDD9::QueryManager::GetNumberOfPixelsDrawn(int *npd, float *value, bool usePixelCount)
{
  #if _DEBUG
  // we may want to introduce artificial latency when debugging
  static int latency = 1;
  #else
  const int latency = 0;
  #endif
  while (_queriesIssued>latency)
  {
    // Get the newest available query result
    // check from beginning to the last
    // once the last one is done, all are done
    Assert(_currentResultIndex<NBrightQueries);
    DWORD nPixels;
    if (_query[_oldestQuery][_currentResultIndex]->GetData(&nPixels, sizeof(DWORD), D3DGETDATA_FLUSH) != S_OK)
    {
      // if query is not ready, break and report what we already have
      break;
    }
    _results[_currentResultIndex] = (usePixelCount) ? nPixels : nPixels>0; // we know we are sampling 1x1 - either 1 or 0 can pass
    _currentResultIndex++;
    if (_currentResultIndex>=NBrightQueries)
    {
      for (int b=0; b<NBrightQueries; b++)
      {
        _mostRecentResult[b] = _results[b];
        _mostRecentResultValue[b] = _value[_oldestQuery][b];
      }
      _mostRecentMasterValue = _masterValue[_oldestQuery];
      // we picked up the result - report it
      // we assume we will be able to pick all other results as well now
      _queriesIssued--;
      _oldestQuery = Next(_oldestQuery);
      _currentResultIndex = 0;
    }
  }
  for (int b=0; b<NBrightQueries; b++)
  {
    if (value) value[b] = _mostRecentResultValue[b];
    npd[b] = _mostRecentResult[b];
  }
  return _mostRecentMasterValue;
}

//////////////////////////////////////////////////////////////////////////

EngineDD9 *EngineDD9::QueryManagerSinglePass::_engine;

EngineDD9::QueryManagerSinglePass::QueryManagerSinglePass()
{
#ifndef _X360SHADERGEN
  // Create the query objects
  for (int i = 0; i < NItems; i++)
  {
    if (FAILED(_engine->GetDirect3DDevice()->CreateQuery(D3DQUERYTYPE_OCCLUSION, _query[i].Init())))
    {
      LogF("Warning: Current HW doesn't support queries");
      goto Break;
    }
  }
  Break:
#endif

  // Initialize the startup values
  ResetQueries();
}

void EngineDD9::QueryManagerSinglePass::ResetQueries()
{
  // Initialize the startup values
  _newQuery = 0;
  _oldestQuery = 0;
  for (int b=0; b<NBrightQueries; b++)
  {
    _mostRecentResult[b] = -1;
  }
  _queriesIssued = 0;
}

bool EngineDD9::QueryManagerSinglePass::Issue(const float *value, float masterValue, IDirect3DTexture9 *alphaTex)
{
  Assert(!_engine->IsCBRecording());
  const int cb = -1;
  
  if (_query[_newQuery].IsNull()) return false;
  if (_queriesIssued>=NItems) return false;
  
  const int alphaStage = 1;
  PushPixelShaderConstant psc7(7);
  SetTextureTemp sampling(alphaStage,alphaTex);
  SetSamplerStateTemp samplingMagX(alphaStage,D3DSAMP_MAGFILTER,D3DTEXF_POINT);
  SetSamplerStateTemp samplingMinX(alphaStage,D3DSAMP_MINFILTER,D3DTEXF_POINT);
  SetSamplerStateTemp samplingMipX(alphaStage,D3DSAMP_MIPFILTER,D3DTEXF_POINT);
  SetSamplerStateTemp samplingSrgb(alphaStage,D3DSAMP_SRGBTEXTURE,FALSE);
  SetSamplerStateTemp samplingClampU(alphaStage,D3DSAMP_ADDRESSU,D3DTADDRESS_CLAMP);
  SetSamplerStateTemp samplingClampV(alphaStage,D3DSAMP_ADDRESSV,D3DTADDRESS_CLAMP);
  
  _query[_newQuery]->Issue(D3DISSUE_BEGIN);
  for (int b=0; b<NBrightQueries; b++)
  {
    _value[_newQuery][b] = (value) ? value[b] : -1.0f;
  }
  _engine->DrawPrimitive(cb,D3DPT_TRIANGLESTRIP, 0, 2);
  _query[_newQuery]->Issue(D3DISSUE_END);
  _masterValue[_newQuery] = masterValue;
  _queriesIssued++;
  DoAssert(_queriesIssued<=NItems);
  _newQuery = Next(_newQuery);
  return true;
}

static inline int AddSaturated(int a, int b)
{
  long long sum = (long long)a+b;
  if (sum>INT_MAX) sum = INT_MAX;
  return (int)sum;
}

// #ifdef _PROFILE
// #pragma optimize("", off)
// #endif

float EngineDD9::QueryManagerSinglePass::GetNumberOfPixelsDrawn(int *npd, float *value)
{
  #if _DEBUG
  // we may want to introduce artificial latency when debugging
  static int latency = 0;
  #else
  const int latency = 0;
  #endif
  // even with 0 frames ahead we expect to get query latency 1, hence latency here needs to be one frame higher
  int maxLatency = AddSaturated(_engine->_framesAhead,1); 
  
  PROFILE_SCOPE_EX(ppQWt,pp);
  while (_queriesIssued>latency)
  {
    DWORD nPixels;
    HRESULT ret;
    do 
    {
      // check oldest query first - it has to be completed first
      ret = _query[_oldestQuery]->GetData(&nPixels, sizeof(DWORD), D3DGETDATA_FLUSH);
      if (ret!=S_FALSE) break;
    } while (_queriesIssued>maxLatency); // prevent query latency being too high - this results in mouse latency
    // if query has succeeded, process it
    if (ret==S_OK)
    {
      // value nPixels means nPixels has succeeded, rest has failed
      for (int i=0; i<NBrightQueries; i++)
      {
        _mostRecentResultValue[i] = _value[_oldestQuery][i];
        _mostRecentResult[i] = i<(int)nPixels;
      }
      _mostRecentMasterValue = _masterValue[_oldestQuery];
      // we picked up the result - report it
      // we assume we will be able to pick all other results as well now
      _queriesIssued--;
      _oldestQuery = Next(_oldestQuery);
    }
    else
    {
      break;
    }
  }
  for (int b=0; b<NBrightQueries; b++)
  {
    if (value) value[b] = _mostRecentResultValue[b];
    if (npd) npd[b] = _mostRecentResult[b];
  }
  saturateMax(_engine->_framesAheadDetected,_queriesIssued-1);
  return _mostRecentMasterValue;
}

// #ifdef _PROFILE
// #pragma optimize("", on)
// #endif

/**
we are not interested about very small changes
on the other hand, GPU exposure control does big changes not very often
the change we do not adjust does gradually build up into a bigger change
*/

static const float HistInit[NBrightQueries]=
{
  0.1f,0.36f,0.50f,0.70f,0.80f,0.89f,0.93f,0.95f,
  1.05f,1.07f,1.11f,1.20f,1.30f,1.45f,1.64f,1.99f,
};

/*
// table could be approximated by following function.
// However, experiments led to numerical instability, and as it was for Arma2 patch,
// change in this area was considered too risky

static float HistInit(int x)
{
  // 0 1 2 3 4 5 6 7 8 9 A B C D E F
  //                 *
  //-7-6-5-4-3-2-1 0 1 2 3 4 5 6 7 8
  Assert(x>=0 && x<NBrightQueries);
  const int half = NBrightQueries/2;
  if (x>=half)
  {
    return 1+abs((x-(half-1))*(1.0f/half));
  }
  else
  {
    return 1-abs((x-(half-1))*(1.0f/half));
  }
}
*/
bool EngineDD9::PPGlow::Init(
  const LazyLoadFile &hlsl, const LazyLoadFile &vs, PPGaussianBlur *ppGaussianBlur
)
{
  if (!base::Init(hlsl,vs)) return false;
  
  _ppGaussianBlur = ppGaussianBlur;

  // Create the low resolution textures cascade
  
  // we need textures for 4x average decimation
  // and then some for 2x avg/min/max decimation
  // we want to start calculating min/max once the picture is coarse enough
  const int startMinMaxAtSize = 80;
  
  int fullWidth = _engine->Width();
  int fullHeight = _engine->Height();
  
  // decimation 2x uses point sampling and should therefore be very accurate
  // we will round to nearest bigger pow2
  int w = roundTo2PowerNCeil(fullWidth);
  int h = roundTo2PowerNCeil(fullHeight);

  int maxSize = max(w,h);
  int minSize = min(w,h);
  
  D3DFORMAT rtFormat = _engine->_downsampleFormat;
  int level = 0;
  // we need at least one 4x decimation step
  do
  {
    DecimationPass &dec = _dec[level];
    w = w>>2;
    h = h>>2;
    maxSize = maxSize>>2;
    minSize = minSize>>2;
    // create the supplementary RT
    int effW = max(w,1);
    int effH = max(h,1);
    LogF("Decimation 4x: %d,%d",effW,effH);
    HRESULT err = _engine->CreateTextureRT(effW, effH, 1, rtFormat, D3DPOOL_DEFAULT, dec._rt, dec._rts, true);
    if (FAILED(err))
    {
      _engine->DDError9("Post process Low resolution texture creation failed",err);
      return false;
    }
    dec._w = effW;
    dec._h = effH;
    // proceed to next level
    level++;
  } while (minSize>startMinMaxAtSize);
  _decimation4x = level;
  // we need at least one 2x decimation step
  do
  {
    DecimationPass &dec = _dec[level];
    w = w>>1;
    h = h>>1;
    maxSize = maxSize>>1;
    minSize = minSize>>1;
    // create the supplementary RT
    // never create less then 1x1
    int effW = max(w,1);
    int effH = max(h,1);
    LogF("Decimation 2x: %d,%d",effW,effH);
    HRESULT err = _engine->CreateTextureRT(effW, effH, 1, rtFormat, D3DPOOL_DEFAULT, dec._rt, dec._rts, true);
    if (FAILED(err))
    {
      _engine->DDError9("Post process Low resolution texture creation failed",err);
      return false;
    }
    dec._w = effW;
    dec._h = effH;
    // proceed to next level
    level++;
  } while (maxSize>1);
  _decimation2x = level;

  HRESULT err = _engine->CreateTextureRT(NBrightQueries, 1, 1, rtFormat, D3DPOOL_DEFAULT, _rtMeasure, _rtsMeasure, true);
  if (FAILED(err))
  {
    _engine->DDError9("Post process Low resolution texture creation failed",err);
    return false;
  }
  
  // stencil/depth buffer no longer needed here - we can disable it
  if (_engine->_depthBufferRT)
  {
    HRESULT err = _engine->CreateDepthStencilSurface(NBrightQueries, 1, D3DFMT_D24S8, D3DMULTISAMPLE_NONE, 0, TRUE, _rtsMeasureDepth, _rtsMeasure, NULL);
    if (FAILED(err))
    {
      _engine->DDError9("Measure depth buffer creation failed",err);
      return false;
    }
  }
  
  _rtApertureCurrent = 0;
  D3DFORMAT apFormat = _engine->_apertureFormat;
  for (int i=0; i<2; i++)  
  {
    // create the texture
    HRESULT err = _engine->CreateTextureRT(1, 1, 1, apFormat, D3DPOOL_DEFAULT, _rtAperture[i], _rtsAperture[i], true);
    if (FAILED(err))
    {
      _engine->DDError9("Post process aperture texture creation failed",err);
      return false;
    }
    
#ifndef _X360SHADERGEN
    // clear the target to avoid uninitialized value
    {
      // save RT
      ComRef<IDirect3DSurface9> rt;
      _engine->_d3DDevice->GetRenderTarget(0,rt.Init());
      // set and clear
      _engine->_d3DDevice->SetRenderTarget(0,_rtsAperture[i]);
      _engine->_d3DDevice->Clear(0,NULL,D3DCLEAR_TARGET,0,0,0);
      // restore RT
      _engine->_d3DDevice->SetRenderTarget(0,rt);
    }
#endif
  }

  {// init the histogram texture
    int histogramSize = 16;
    #ifdef _XBOX
    D3DFORMAT histFormat = (D3DFORMAT)MAKELINFMT(_engine->_apertureFormat);
    #else
    D3DFORMAT histFormat = _engine->_apertureFormat;
    #endif
    err = _engine->_d3DDevice->CreateTexture(histogramSize,1,1,0,histFormat,D3DPOOL_DEFAULT, _histogramTholds.Init(),NULL);
    if (FAILED(err))
    {
      _engine->DDError9("Post process histogram sampling texture creation failed",err);
      return false;
    }
  
	  ComRef<IDirect3DTexture9> tmpTexture;
    #if !XBOX_D3D
	  // temporary texture created in system pool used data filling
	  if ( FAILED(_engine->_d3DDevice->CreateTexture(
		  histogramSize, 1, /*levels*/ 1, /*usage*/ 0, histFormat, D3DPOOL_SYSTEMMEM, tmpTexture.Init(), NULL
		  )) ) return false;
		#else
		tmpTexture = _histogramTholds;
		#endif


	  // lock and fill texture by coefficients - convert to target format (float16/float32) as needed
	  D3DLOCKED_RECT lockRect;
	  if ( FAILED(tmpTexture->LockRect(0, &lockRect, 0, 0)) ) { return false; }
	  for (int i=0; i<histogramSize; i++)
	  {
	    float value = HistInit[i];
	    if (_engine->_apertureFormat==D3DFMT_R16F)
	    {
	      D3DXFloat32To16Array(((D3DXFLOAT16 *)lockRect.pBits)+i,&value, 1);
	    }
	    else if (_engine->_apertureFormat==D3DFMT_R32F)
	    {
	      ((float *)lockRect.pBits)[i] = value;
	    }
	    else
	    {
	      Fail("Aperture format %s not supported by queries");
	    }
	  }
	  tmpTexture->UnlockRect(0);

    #if !XBOX_D3D
	  // update texture
	  if ( FAILED(_engine->_d3DDevice->UpdateTexture(tmpTexture, _histogramTholds)) ) { return false; }
	  #endif
  }  

  #if _DEBUG && !defined _XBOX
  {
    HRESULT err = _engine->GetDirect3DDevice()->CreateOffscreenPlainSurface(1, 1, apFormat, D3DPOOL_SYSTEMMEM, _rtsDebugAp.Init(), NULL);
    if (FAILED(err))
    {
      // failed as a plain surface, try to create as a lockable render target
      err = _engine->GetDirect3DDevice()->CreateRenderTarget(1, 1, apFormat, D3DMULTISAMPLE_NONE, 0, TRUE, _rtsDebugAp.Init(), NULL);
      if (FAILED(err))
      {
        _engine->DDError9("Post process Low resolution texture creation failed",err);
        return false;
      }
    }
    err = _engine->GetDirect3DDevice()->CreateOffscreenPlainSurface(1, 1, rtFormat, D3DPOOL_SYSTEMMEM, _rtsDebugDec.Init(), NULL);
    if (FAILED(err))
    {
      err = _engine->GetDirect3DDevice()->CreateRenderTarget(1, 1, rtFormat, D3DMULTISAMPLE_NONE, 0, TRUE, _rtsDebugDec.Init(), NULL);
      if (FAILED(err))
      {
        _engine->DDError9("Post process Low resolution texture creation failed",err);
        return false;
      }
    }
  }
  #endif
  
  
  // stencil/depth buffer no longer needed here - we can disable it
  /*
  if (_engine->_depthBufferRT)
  {
    D3DSURFACE_DESC desc;
    _rtRough[0]->GetLevelDesc(0,&desc);
    int roughWidth = desc.Width;
    int roughHeight = desc.Height;
    if (
      FAILED(
        _engine->CreateDepthStencilSurface(
          roughWidth,roughHeight,D3DFMT_D24S8,
          D3DMULTISAMPLE_NONE,0,TRUE,_rtsRoughDepth,XXX
        )
      )
    )
    {
      LogF("PPGlow: Cannot create depth buffer");
      return false;
    }
  }
  */

  {
    // Create the VB + declaration
    ComRef<IDirect3DVertexBuffer9> vb;
    if (FAILED(_engine->GetDirect3DDevice()->CreateVertexBuffer(sizeof(VertexCustomEdge) * 4, D3DUSAGE_WRITEONLY, 0, D3DPOOL_DEFAULT, vb.Init(), NULL)))
    {
      LogF("Error: Vertex buffer creation failed.");
      return false;
    }
    void *data;
    vb->Lock(0, 0, &data, 0);
    VertexCustomEdge *v = (VertexCustomEdge *)data;
    v[0].pos = Vector3P(-0.5, -0.5, 0.5); v[0].index = 0;
    v[1].pos = Vector3P(-0.5,  0.5, 0.5); v[1].index = 1;
    v[2].pos = Vector3P( 0.5, -0.5, 0.5); v[2].index = 2;
    v[3].pos = Vector3P( 0.5,  0.5, 0.5); v[3].index = 3;
    vb->Unlock();

    _vbCustomEdge = VertexStaticData9::BufferFuture(vb);

    // Create the VD of the blur vertex
    const D3DVERTEXELEMENT9 vertexDeclCustomEdge[] =
    {
      {0,  0, D3DDECLTYPE_FLOAT3,   D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION, 0},
      {0, 12, D3DDECLTYPE_FLOAT1,   D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_BLENDINDICES, 0},
      D3DDECL_END()
    };
    if (FAILED(_engine->GetDirect3DDevice()->CreateVertexDeclaration(vertexDeclCustomEdge, _vdCustomEdge.Init())))
    {
      LogF("Error: Vertex declaration creation failed.");
      return false;
    }
  }

  // Create the shaders
  if (!CompileVertexShader(vs,"VSPostProcessCustomEdge4T",_vsCustomEdge))
  {
    return false;
  }
  
  if (!CompilePixelShader(hlsl,"PSPostProcessDownSampleLuminance",_psLuminance))
  {
    return false;
  }
  if (!CompilePixelShader(hlsl,"PSPostProcessDownSampleAvgLuminance",_psAvgLuminance))
  {
    return false;
  }
  if (!CompilePixelShader(hlsl,"PSPostProcessDownSampleMaxAvgMinLuminance",_psMaxAvgMinLuminance))
  {
    return false;
  }
  ComRef<IDirect3DPixelShader9> ppal;
  if (!CompilePixelShader(hlsl,"PSPostProcessAssumedLuminance",ppal,true))
  {
    return false;
  }
  ComRef<IDirect3DPixelShader9> ppalDirect;
  if (!CompilePixelShader(hlsl,"PSPostProcessAssumedLuminanceDirect",ppalDirect,false))
  {
    return false;
  }

  if (_engine->_apertureFormat==D3DFMT_A8R8G8B8)
  {
    /**
    if there is no float render target support,
    we cannot calculate change on the GPU
    */
    _psAssumedLuminance = ppalDirect;
  }
  else
  {
    _psAssumedLuminance = ppal;
  }

  if (!CompilePixelShader(hlsl,"PSPostProcessQuery",_psQuery))
  {
    return false;
  }


  if (!CompilePixelShader(hlsl,"PSPostProcessGlow",_psGlow))
  {
    return false;
  };

  if (!CompilePixelShader(hlsl,"PSPostProcessGlowNight",_psGlowNight))
  {
    return false;
  };
  if (!CompilePixelShader(hlsl,"PSPostProcessNVG",_psGlowNVG))
  {
    return false;
  };

  return true;
}


int EngineDD9::PPGlow::SetParams(const float *pars, int nPars)
{
  Assert(nPars==3);
  _deltaT = pars[0];
  _eyeAdaptMin = pars[1];
  _eyeAdaptMax = pars[2];

  return 0;
}

#if _DEBUG && !defined _XBOX
Color EngineDD9::PPGlow::GetContent(IDirect3DSurface9 *surf, IDirect3DSurface9 *temp, D3DFORMAT format)
{
  _engine->GetDirect3DDevice()->GetRenderTargetData(surf,temp);

  D3DLOCKED_RECT locked;
  temp->LockRect(&locked,NULL,D3DLOCK_READONLY);
  // assume ARGB8888 format
  Color color;
  switch (format)
  {
    case D3DFMT_A8R8G8B8:
      {
        PackedColor c = PackedColor(*(DWORD *)locked.pBits);
        color = Color(c);
      }
      break;
    case D3DFMT_R32F:
      {
        const float *c = (const float *)locked.pBits;
        color = Color(c[0],0,0,1);
      }
      break;
    case D3DFMT_G32R32F:
      {
        const float *c = (const float *)locked.pBits;
        color = Color(c[0],c[1],0,1);
      }
      break;
    case D3DFMT_A32B32G32R32F:
      {
        const float *c = (const float *)locked.pBits;
        color = Color(c[0],c[1],c[2],c[3]);
      }
      break;
    case D3DFMT_A16B16G16R16F:
      {
        const Float16b<10> *c = (const Float16b<10> *)locked.pBits;
        color = Color(c[0],c[1],c[2],c[3]);
      }
      break;
    case D3DFMT_G16R16F:
      {
        const Float16b<10> *c = (const Float16b<10> *)locked.pBits;
        color = Color(c[0],c[1],0,1);
      }
      break;
    case D3DFMT_R16F:
      {
        const Float16b<10> *c = (const Float16b<10> *)locked.pBits;
        color = Color(c[0],0,0,1);
      }
      break;
    case D3DFMT_L16:
      {
        const unsigned short *c = (const unsigned short *)locked.pBits;
        float l = c[0]*(1.0f/0xffff);
        color = Color(l,l,l,1);
      }
      break;
    case D3DFMT_G16R16:
      {
        const unsigned short *c = (const unsigned short *)locked.pBits;
        float r = c[0]*(1.0f/0xffff);
        float g = c[1]*(1.0f/0xffff);
        color = Color(r,g,0,1);
      }
      break;
    default:
      color = HBlack;
      break;
  }
  temp->UnlockRect();
  return color;
}

static Color DecodeApertureChangeF(Color color)
{
  return Color(
    DecodeApertureChange(color.R()),
    DecodeApertureChange(color.G()),
    DecodeApertureChange(color.B()),
    DecodeApertureChange(color.A())
  );
}

Color EngineDD9::PPGlow::GetContentAp(IDirect3DSurface9 *surf)
{
  // aperture change is stored as logarithm
  Color raw = GetContent(surf,_rtsDebugAp,_engine->_apertureFormat);
  return DecodeApertureChangeF(raw);
}
Color EngineDD9::PPGlow::GetContentDec(IDirect3DSurface9 *surf)
{
  return GetContent(surf,_rtsDebugDec,D3DFMT_A8R8G8B8);
}

#endif

// note: MS HDR uses sensitivity as follows:
//(0.2125, 0.7154, 0.0721);
//(0.2125, 0.7154, 0.0721);
#define R_EYE_DARK 0.05f
#define G_EYE_DARK 0.55f
#define B_EYE_DARK 0.40f
static const Color DaySens(R_EYE,G_EYE,B_EYE);

/*!
\patch 5129 Date 2/20/2007 by Ondra
- Improved: Improved night vision goggles rendering.
*/
static const Color NVGSens(0.6,0.2,0.2);
static const Color NightSens(R_EYE_DARK,G_EYE_DARK,B_EYE_DARK);

void EngineDD9::PPGlow::Do(bool isLast)
{
#if 1 // ndef _XBOX
  static const float zero9[9*4] =
  {
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
    0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
  };
  
  // glow is typically a last effect, but in some cases some other may follow
  PROFILE_SCOPE_GRF_EX(ppGlw,pp,SCOPE_COLOR_YELLOW);

  const int cb = -1;
  
  // Prepare drawing
  Prepare();

  IDirect3DTexture9 *origRt = _engine->_currRenderTarget;
  IDirect3DSurface9 *origRts = _engine->_currRenderTargetSurface;

  // We will need a source as a texture
  IDirect3DTexture9 *rt;
  
  // if possible, use previous render target directly for texturing
  if (isLast && _engine->_currRenderTarget)
  {
    rt = _engine->GetCurrRenderTargetAsTexture();
  }
  else
  {
    rt = _engine->CreateRenderTargetCopyAsTexture();
  }
  
  // depending on render target format it may or may not be SRGB
  Assert(_engine->_linearSpace);
  BOOL rtIsSRGB = _engine->_caps._rtIsSRGB && _engine->_caps._canReadSRGB;
  // Set the vertex shader + stream source
  _engine->SetStreamSource(cb, _vbCustomEdge, sizeof(VertexCustomEdge), 4);
  _engine->SetVertexDeclaration(cb, _vdCustomEdge);
  _engine->SetVertexShader(cb, _vsCustomEdge);


  {
    float hdrMultiply = _engine->GetPostHDRBrightness()/_engine->GetHDRFactor();
#if _ENABLE_CHEATS && _ENABLE_REPORT && _ENABLE_PERFLOG
    if (CHECK_DIAG(DEHDR) && CheckMainThread())
    {
      DIAG_MESSAGE(100,Format("Post HDR multiply %.2f",hdrMultiply));
    }
#endif
    SetPixelShaderConstantTemp spc7(PSC_HDRMultiply, D3DXVECTOR4(hdrMultiply,hdrMultiply,hdrMultiply,hdrMultiply));
    
    PushVertexShaderConstant pushC0(0);
    PushVertexShaderConstant pushC1(1);
    PushVertexShaderConstant pushC2(2);
    PushVertexShaderConstant pushC3(3);
    PushVertexShaderConstant pushC4(4);
    PushVertexShaderConstant pushC5(5);
    PushVertexShaderConstant pushC6(6);
    PushVertexShaderConstant pushC7(7);
    PushVertexShaderConstant pushC8(8);
    
    // decide if decimated surfaces are SRGB or not
    const bool decimatedSRGB = _engine->_downsampleFormat==D3DFMT_A8B8G8R8;
    
    // Decimate the original into rough texture and measure aperture
    {
      // Set temporary states
#ifndef _X360SHADERGEN      
      SetRenderStateTemp rsSRGBSRGBWRITEENABLE(D3DRS_SRGBWRITEENABLE, decimatedSRGB);
#endif
      SetRenderStateTemp rsCOLORWRITEENABLE(D3DRS_COLORWRITEENABLE, D3DCOLORWRITEENABLE_RED | D3DCOLORWRITEENABLE_GREEN | D3DCOLORWRITEENABLE_BLUE | D3DCOLORWRITEENABLE_ALPHA);
      // no alpha or fog done here
      SetRenderStateTemp rsALPHATESTENABLE(D3DRS_ALPHATESTENABLE, FALSE);
      SetRenderStateTemp rsALPHABLENDENABLE(D3DRS_ALPHABLENDENABLE, FALSE);
      // by default assume bi-linear filtering
      SetSamplerStateTemp ss0MINFILTER(0, D3DSAMP_MINFILTER, D3DTEXF_LINEAR);
      SetSamplerStateTemp ss0MAGFILTER(0, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR);
      SetSamplerStateTemp ss1MINFILTER(1, D3DSAMP_MINFILTER, D3DTEXF_LINEAR);
      SetSamplerStateTemp ss1MAGFILTER(1, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR);
      SetSamplerStateTemp ss2MINFILTER(2, D3DSAMP_MINFILTER, D3DTEXF_LINEAR);
      SetSamplerStateTemp ss2MAGFILTER(2, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR);
      SetSamplerStateTemp ss3MINFILTER(3, D3DSAMP_MINFILTER, D3DTEXF_LINEAR);
      SetSamplerStateTemp ss3MAGFILTER(3, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR);
      // tri-linear filtering never done 
      SetSamplerStateTemp ss0MIPFILTER(0, D3DSAMP_MIPFILTER, D3DTEXF_POINT);
      SetSamplerStateTemp ss1MIPFILTER(1, D3DSAMP_MIPFILTER, D3DTEXF_POINT);
      SetSamplerStateTemp ss2MIPFILTER(2, D3DSAMP_MIPFILTER, D3DTEXF_POINT);
      SetSamplerStateTemp ss3MIPFILTER(3, D3DSAMP_MIPFILTER, D3DTEXF_POINT);
      // clamping all stages
      SetSamplerStateTemp ss0ADDRESSU(0, D3DSAMP_ADDRESSU, D3DTADDRESS_CLAMP);
      SetSamplerStateTemp ss0ADDRESSV(0, D3DSAMP_ADDRESSV, D3DTADDRESS_CLAMP);
      SetSamplerStateTemp ss1ADDRESSU(1, D3DSAMP_ADDRESSU, D3DTADDRESS_CLAMP);
      SetSamplerStateTemp ss1ADDRESSV(1, D3DSAMP_ADDRESSV, D3DTADDRESS_CLAMP);
      SetSamplerStateTemp ss2ADDRESSU(2, D3DSAMP_ADDRESSU, D3DTADDRESS_CLAMP);
      SetSamplerStateTemp ss2ADDRESSV(2, D3DSAMP_ADDRESSV, D3DTADDRESS_CLAMP);
      SetSamplerStateTemp ss3ADDRESSU(3, D3DSAMP_ADDRESSU, D3DTADDRESS_CLAMP);
      SetSamplerStateTemp ss3ADDRESSV(3, D3DSAMP_ADDRESSV, D3DTADDRESS_CLAMP);


      
      #if 0 //_ENABLE_CHEATS
        D3DSURFACE_DESC desc;
        rt->GetLevelDesc(0,&desc);
        DoAssert(desc.Width==_engine->Width());
        DoAssert(desc.Height==_engine->Height());
      #endif

      {
        PROFILE_SCOPE_GRF_EX(ppGLu,pp,SCOPE_COLOR_YELLOW);
    
        // perform initial luminance calculation + 4x decimation
        EdgeParams pars;
        pars.SetupBilinear(_dec[0]._w,_dec[0]._h,_engine->Width(),_engine->Height());
        _engine->SetVertexShaderConstantF(cb, 0, pars.u[0], 9);

        // Set the pixel shader
        _engine->SetPixelShader(cb, _psLuminance);

        // Set the render target
        _engine->SetRenderTarget(_dec[0]._rts, _dec[0]._rt, NULL);

        // set the source as 4 textures    
        SetTextureTemp tTex0(0, rt);
        SetTextureTemp tTex1(1, rt);
        SetTextureTemp tTex2(2, rt);
        SetTextureTemp tTex3(3, rt);
#ifndef _X360SHADERGEN
        SetSamplerStateTemp tempSRGB0(0, D3DSAMP_SRGBTEXTURE, rtIsSRGB);
        SetSamplerStateTemp tempSRGB1(1, D3DSAMP_SRGBTEXTURE, rtIsSRGB);
        SetSamplerStateTemp tempSRGB2(2, D3DSAMP_SRGBTEXTURE, rtIsSRGB);
        SetSamplerStateTemp tempSRGB3(3, D3DSAMP_SRGBTEXTURE, rtIsSRGB);
#endif
        
        _engine->DrawPrimitive(cb, D3DPT_TRIANGLESTRIP, 0, 2);
        _engine->ResolveRenderTarget();
      }
    
      // source is always linear space since here
#ifndef _X360SHADERGEN
      SetSamplerStateTemp tempSRGB0(0, D3DSAMP_SRGBTEXTURE, decimatedSRGB);
      SetSamplerStateTemp tempSRGB1(1, D3DSAMP_SRGBTEXTURE, decimatedSRGB);
      SetSamplerStateTemp tempSRGB2(2, D3DSAMP_SRGBTEXTURE, decimatedSRGB);
      SetSamplerStateTemp tempSRGB3(3, D3DSAMP_SRGBTEXTURE, decimatedSRGB);
#endif
      
      { // perform 4x decimation + average calculation
        PROFILE_SCOPE_GRF_EX(ppGAv,pp,SCOPE_COLOR_YELLOW);
        for (int i=1; i<_decimation4x; i++)
        {
          // perform initial luminance calculation + 4x decimation
          EdgeParams pars;
          pars.SetupBilinear(_dec[i]._w,_dec[i]._h,_dec[i-1]._w,_dec[i-1]._h);
          
          _engine->SetVertexShaderConstantF(cb, 0, pars.u[0], 9);

          // Set the pixel shader
          _engine->SetPixelShader(cb, _psAvgLuminance);

          // Set the render target
          _engine->SetRenderTarget(_dec[i]._rts, _dec[i]._rt, NULL);

          IDirect3DTexture9 *tex = _dec[i-1]._rt;
          // set the source as 4 textures    
          SetTextureTemp tTex0(0, tex);
          SetTextureTemp tTex1(1, tex);
          SetTextureTemp tTex2(2, tex);
          SetTextureTemp tTex3(3, tex);

          _engine->DrawPrimitive(cb, D3DPT_TRIANGLESTRIP, 0, 2);
          _engine->ResolveRenderTarget();
        }
      }

      { // perform 2x decimation + min/max/avg calculation
        PROFILE_SCOPE_GRF_EX(ppGMM,pp,SCOPE_COLOR_YELLOW);
        // note: we know other temp setting is done, which will return original values once done
        for (int s=0; s<4; s++) 
        {
          _engine->D3DSetSamplerState(cb, s, D3DSAMP_MINFILTER, D3DTEXF_POINT);
          _engine->D3DSetSamplerState(cb, s, D3DSAMP_MAGFILTER, D3DTEXF_POINT);
        }
        
        // Set the pixel shader
        _engine->SetPixelShader(cb, _psMaxAvgMinLuminance);
        for (int i=_decimation4x; i<_decimation2x; i++)
        {
          EdgeParams pars;
          pars.SetupBilinear(_dec[i]._w,_dec[i]._h,_dec[i-1]._w,_dec[i-1]._h);
          
          _engine->SetVertexShaderConstantF(cb, 0, pars.u[0], 9);

          // Set the render target
          _engine->SetRenderTarget(_dec[i]._rts, _dec[i]._rt, NULL);

          IDirect3DTexture9 *tex = _dec[i-1]._rt;
          // set the source as 4 textures    
          SetTextureTemp tTex0(0, tex);
          SetTextureTemp tTex1(1, tex);
          SetTextureTemp tTex2(2, tex);
          SetTextureTemp tTex3(3, tex);
          
          _engine->DrawPrimitive(cb, D3DPT_TRIANGLESTRIP, 0, 2);
          _engine->ResolveRenderTarget();
        }
        
        // perform some calculations with 1x1 surfaces
        // this is to adjust aperture
        {
          _engine->SetPixelShader(cb, _psAssumedLuminance);
          
          Assert(_engine->_accomFactorThis>0);
          // values in last frame were converted to absolute by multiplying by:
          // _engine->_accomFactorPrev * lastFrameAp
          // values in this frame are converted to absolute by multiplying by:
          //  _engine->_accomFactorThis * thisFrameAp
          // absAp = lastFrameAp * _engine->_accomFactorPrev
          // absAp = thisFrameAp * _engine->_accomFactorThis

          // lastFrameAp * _engine->_accomFactorPrev/_engine->_accomFactorThis = thisFrameAp
          
          
          float eyeAccomLastToThis = _engine->_accomFactorPrev/_engine->_accomFactorThis;
          // this controls overall scene brightness
          // we need to know where the user selected midpoint is
          float desiredLuminance = _engine->_nightVisionThis ? 0.781f : (_engine->GetPreHDRBrightness() * _engine->GetHDRFactor());
          float invAccom=1/_engine->_accomFactorThis;

          // adaptation time depends on how we are adapting
          // if we are reacting with iris, the adaptations is very fast
          // if we are reacting with retina, it is much slower
          // even when retina is adapted, if light level is raised, the iris will react
          // this allows quite quick re-adaptation for higher light levels
          // we expect something like:
          const float irisUsedToDark = 1; // time to make change 2x
          const float irisUsedToLight = 0.5; // time to make change 0.5x
          
          const float retinaUsedToDark = 20; // time to make change 2x
          const float retinaUsedToLight = 1.0; // time to make change 0.5x
          
          // retina adaptation dynamic range is probably much smaller (100x?)
          // when retina adaptation is important:
          // 1) iris fully open
          // 2) once rods get adapted, they go down slowly
          // we do not model this, we assume under certain level retina acts
          // and above it iris acts
          // assume retina starts reacting once iris adaptation is 1.0
          const float crossBeg = 1.0f;
          const float crossEnd = 4.0f;
          
          float retinaFactor = InterpolativC(_engine->_accomFactorPrev,crossBeg,crossEnd,0,1);
          
          // time to make change 2x
          const float getUsedToDark = retinaFactor*retinaUsedToDark+(1-retinaFactor)*irisUsedToDark;
          // time to make change 0.5x
          const float getUsedToLight = retinaFactor*retinaUsedToLight+(1-retinaFactor)*irisUsedToLight;
          
          const float log2 = 0.69314718056f;
          
          
          float minRatio = exp(-_deltaT*(log2/getUsedToLight));
          float maxRatio = exp(+_deltaT*(log2/getUsedToDark));

          // make sure the number still can be represented with float16
          saturateMin(minRatio,0.999f);
          saturateMax(maxRatio,1.001f);
          
          // we need the value to be representable in float16 (m10e6) format
          float minAdapt = 1, maxAdapt = 1;
          
          if (!_engine->_accomFactorFixedThis)
          {
            minAdapt = _eyeAdaptMin*invAccom;
            maxAdapt = _eyeAdaptMax*invAccom;
          }

          const float pars[8]={
            eyeAccomLastToThis,_deltaT,desiredLuminance,0,
            minAdapt,maxAdapt,minRatio,maxRatio
          };
          
          #if _ENABLE_CHEATS
            if (CHECK_DIAG(DEHDR))
            {
              Log("Adapt. factor: last %g, this %g",_engine->_accomFactorPrev,_engine->_accomFactorThis);
            }
          #endif

          PushPixelShaderConstant psc8(8),psc(9);
          _engine->SetPixelShaderConstantF(cb, 8, pars, 2);
          
          _engine->SetVertexShaderConstantF(cb, 0, zero9, 9);

          int oldAperture = _rtApertureCurrent;
          // switch to next aperture texture
          _rtApertureCurrent = _rtApertureCurrent==0;
          // Set the render target
          _engine->SetRenderTarget(_rtsAperture[_rtApertureCurrent], _rtAperture[_rtApertureCurrent], NULL);

          // 1x1 maxAvgMin as source
          SetTextureTemp tTex0(0, _dec[_decimation2x-1]._rt);
          // old aperture value as another source
          SetTextureTemp tTex1(1, _rtAperture[oldAperture]);
          SetTextureTemp tTex2(2, NULL);
          SetTextureTemp tTex3(3, NULL);
          
          // render target is always linear
#ifndef _X360SHADERGEN
          SetRenderStateTemp rsSRGBSRGBWRITEENABLE(D3DRS_SRGBWRITEENABLE, FALSE);

          // old aperture is always linear
          // source down-sampled scene is SRGB
          SetSamplerStateTemp tempSRGB0(0, D3DSAMP_SRGBTEXTURE, decimatedSRGB);
          SetSamplerStateTemp tempSRGB1(1, D3DSAMP_SRGBTEXTURE, FALSE);
#endif
          
          _engine->DrawPrimitive(cb, D3DPT_TRIANGLESTRIP, 0, 2);
          
          _engine->ResolveRenderTarget();

          #if _DEBUG && !defined _XBOX
            if (CHECK_DIAG(DEHDR))
            {
              // lock textures and read debugging data from them
              Color dec = GetContentDec(_dec[_decimation2x-1]._rts);
              
              Color oldAp = GetContentAp(_rtsAperture[oldAperture]);
              Color newAp = GetContentAp(_rtsAperture[_rtApertureCurrent]);
              Color oldApAbs = oldAp*_engine->_accomFactorPrev;
              Color newApAbs = oldAp*_engine->_accomFactorThis;
              Log(
                "Aperture: %g,%g,%g,%g -> %g,%g,%g,%g",
                oldAp.R(),oldAp.G(),oldAp.B(),oldAp.A(),
                newAp.R(),newAp.G(),newAp.B(),newAp.A()
              );
              Log(
                "Aperture (abs): %g,%g,%g,%g -> %g,%g,%g,%g",
                oldApAbs.R(),oldApAbs.G(),oldApAbs.B(),oldApAbs.A(),
                newApAbs.R(),newApAbs.G(),newApAbs.B(),newApAbs.A()
              );
              Log("Dec: %g,%g,%g,%g",dec.R(),dec.G(),dec.B(),dec.A());
            }
          #endif
        }
      }
      
      // note: linear space temp goes off scope here (cf. tempNoSRGB0)
    }

    
    // once we have the aperture, we may perform the Bloom surface preparation
    if (_engine->_doBloom)
    {
      _engine->_pp._bloom->Do(false,rt);
    }
    
    
    // Measure the brightness of the image
    {
      PROFILE_SCOPE_GRF_EX(ppGQy,pp,SCOPE_COLOR_GREEN);

      PushVertexShaderConstant pushC0(0);
      PushVertexShaderConstant pushC1(1);
      PushVertexShaderConstant pushC2(2);
      PushVertexShaderConstant pushC3(3);
      PushVertexShaderConstant pushC4(4);
      PushVertexShaderConstant pushC5(5);
      PushVertexShaderConstant pushC6(6);
      PushVertexShaderConstant pushC7(7);
      PushVertexShaderConstant pushC8(8);
      
      // source is 1 x NBrightQueries image
      // set UV mapping so that we read pixels using point filtering

      static const float baseU[4] = {0, 0, 1, 1};
      static const float baseV[4] = {1, 0, 1, 0};
      
      const float tgtOffsetU = 0;
      const float tgtOffsetV = 0;
      const float offsetU = 0.5f / NBrightQueries;
      const float offsetV = 0;
      
      float U[4][4];
      float V[4][4];
      for (int vertex = 0; vertex < 4; vertex++)
      {
        // Texture 0 - source image - does not matter, not used
        U[vertex][0] = baseU[vertex] + offsetU;
        V[vertex][0] = baseV[vertex] + offsetV;
        // Texture 1 - histohram thresholds
        U[vertex][1] = baseU[vertex] + offsetU;
        V[vertex][1] = baseV[vertex] + offsetV;
        // Texture 2
        U[vertex][2] = 0;
        V[vertex][2] = 0;
        // Texture 3
        U[vertex][3] = 0;
        V[vertex][3] = 0;
      }
      const float posOffset[4] = {tgtOffsetU,tgtOffsetV,0,0};
      _engine->SetVertexShaderConstantF(cb, 0, U[0], 4);
      _engine->SetVertexShaderConstantF(cb, 4, V[0], 4);
      _engine->SetVertexShaderConstantF(cb, 8, posOffset, 1);
      
      // _engine->_lastKnownAvgIntensity is the average from the last frame
      // _engine->_accomFactorThis is current exposure settings
      
      float histPars[NBrightQueries+1];
      // CPU reads eyeAccom as determined by the GPU
      // we are using log representation in the pixels shaders for great precision and range
      
      // we want half of the samples to be under 1 and half above 1
      // sample position relative to the expected value
      // last one is always 1 and it is always ignored
      for (int i=0; i<NBrightQueries; i++)
      {
        histPars[i] = HistInit[i];
      }
      // always ignored
      histPars[NBrightQueries]=1;
      
      // we want most probes to be around the expected values
      // if the result it too high, the system will avoid too fast adaptation anyway
      
      // Draw
      // Set temporary render states
      SetRenderStateTemp rsCOLORWRITEENABLE(D3DRS_COLORWRITEENABLE, D3DCOLORWRITEENABLE_RED | D3DCOLORWRITEENABLE_GREEN | D3DCOLORWRITEENABLE_BLUE | D3DCOLORWRITEENABLE_ALPHA);
      //SetRenderStateTemp rsCOLORWRITEENABLE(D3DRS_COLORWRITEENABLE, 0);
      SetRenderStateTemp rsALPHATESTENABLE(D3DRS_ALPHATESTENABLE, FALSE);
#ifndef _X360SHADERGEN
      SetRenderStateTemp rsSRGBSRGBWRITEENABLE(D3DRS_SRGBWRITEENABLE, FALSE);
      SetSamplerStateTemp tempNoSRGB0(0, D3DSAMP_SRGBTEXTURE, FALSE);
#endif
      
      // Set the render target
      // note: we need depth buffer, otherwise query does not work at all
      _engine->SetRenderTarget(_rtsMeasure, _rtMeasure, _rtsMeasureDepth);
      _engine->SetTexture(cb, 0, _rtAperture[_rtApertureCurrent]);
      
      _engine->SetPixelShader(cb, _psQuery);
      
      // Drawing
      float values[NBrightQueries];
      for (int b = 0; b < NBrightQueries; b++)
      {
        values[b] = histPars[b]*_engine->_accomFactorThis;
      }
      _querySinglePass.Issue(values,_engine->_accomFactorThis,_histogramTholds);

      // Build the histogram and check the intensity of the image
      {

        // Each level includes all the following levels
        struct HistogramItem
        {
          float minAlpha;
          int pixels;
        } histogram[NBrightQueries+2];

        bool valid = true;
        int numberOfPixelsDrawn[NBrightQueries];
        float value[NBrightQueries];
        float masterValue = _querySinglePass.GetNumberOfPixelsDrawn(numberOfPixelsDrawn,value);
        (void)masterValue;
        for (int b=0; b<NBrightQueries; b++)
        {
          histogram[b+1].minAlpha = value[b];
          histogram[b + 1].pixels = numberOfPixelsDrawn[b];
          if (numberOfPixelsDrawn[b] < 0) valid = false;
        }

        if (valid)
        {
          // as we test 1x1 surface, "histogram" is actually one point only
          float totalAlpha = 0;
          // all pixels are over 0 limit
          histogram[0].minAlpha = 0;
          histogram[0].pixels = 1 * 1;
          // no pixels are over 1 limit
          histogram[NBrightQueries + 1].minAlpha = histogram[NBrightQueries].minAlpha;
          histogram[NBrightQueries + 1].pixels = 0;
          float totalError = 0;
          // verify we got expected results, i.e. line of 1s followed by line of 0s
          int edge = -1;
          for (int b = 0; b < NBrightQueries + 1; b++)
          {
            float alphaMin = histogram[b].minAlpha;
            float alphaMax = histogram[b+1].minAlpha;
            // exclude brighter levels
            int thisCount = histogram[b].pixels - histogram[b + 1].pixels;
            if (thisCount>0)
            {
              // verify the edge is 1 pixel only
              Assert(thisCount==1);
              // verify there is only one edge
              Assert(edge<0);
              edge = b;
            }
            float sumAlpha = thisCount * (alphaMin + alphaMax) * 0.5f;
            float error = thisCount * (alphaMax - alphaMin) * 0.5f;
            totalAlpha += sumAlpha;
            totalError += error;
          }

          //LogF("Avg %.3f", totalAlpha / ((float(MeasureWidth * MeasureHeight) * 255.0f) / 5.0f));

          #if _ENABLE_CHEATS && _ENABLE_REPORT && _ENABLE_PERFLOG
            if (CHECK_DIAG(DEHDR) && CheckMainThread())
            {
              DIAG_MESSAGE(
                500,Format(
                  "Avg expected measured %.4f+(%.4f), %.4f..%.4f",
                  totalAlpha,totalError,totalAlpha-totalError,totalAlpha+totalError
                )
              );
            }
          #endif
          #if _ENABLE_CHEATS
            if (CHECK_DIAG(DEHDR))
            {
              Log("Measured %g (was for eyeAccom %g)",totalAlpha,masterValue);
            }
          #endif
          // if the resulting value is negative, something went wrong
          DoAssert(totalAlpha>=0);
          /*
          // we cannot use masterValue to detect changes, as due to latency this could cause
          // two values alternating and never reaching a stable state
          // if the value did not change significantly from the master value, use the master value
          float newEyeAccom = masterValue;
          if (
            fabs(totalAlpha-masterValue)/totalAlpha>4e-2
          )
          {
            newEyeAccom = totalAlpha;
          }
          _engine->_lastKnownAvgIntensity = newEyeAccom;
          */
          // if the value did not change significantly, do not change it, as changing aperture estimate often can lead to oscillation
          //const float changeThold = 0.04f;
          static float changeThold = 0.1f;
          if (
            _engine->_lastKnownAvgIntensity<=0 ||
            fabs(totalAlpha-_engine->_lastKnownAvgIntensity)/totalAlpha>changeThold
          )
          {
            _engine->_lastKnownAvgIntensity = totalAlpha;
          }
        }
      }
    }

    // Final step - blending of original and the glow
    {
      // TODO: use EdgeParams
      //EdgeParams pars;
      //pars.SetupPoint(_engine->Width(),_engine->Height(),_engine->Width(),_engine->Height());
      static const float baseU[4] = {0, 0, 1, 1};
      static const float baseV[4] = {1, 0, 1, 0};

      // when rescaling, we want to allow filtering
      bool rescale = false;
      //static bool forcePoint = false;
      //if (forcePoint) rescale = false;
      
      // point sampling requires 0.5 offset to avoid numerical inaccuracy
      // previous version, well tuned
      const float tgtOffsetU = 0;
      const float tgtOffsetV = 0;
      const float srcOffsetU = rescale ? 0 : 0.5f / _engine->Width();
      const float srcOffsetV = rescale ? 0 : 0.5f / _engine->Height();

      IDirect3DTexture9 *glow = _engine->_pp._bloom->GetBlurredTexture();
  
      D3DSURFACE_DESC glowDesc;
      glow->GetLevelDesc(0, &glowDesc);

      const float glowOffsetU = 0.5f / glowDesc.Width;
      const float glowOffsetV = 0.5f / glowDesc.Height;
      float U[4][4];
      float V[4][4];
      for (int vertex = 0; vertex < 4; vertex++)
      {
        // Texture 0 - source image
        U[vertex][0] = baseU[vertex] + srcOffsetU;
        V[vertex][0] = baseV[vertex] + srcOffsetV;
        // Texture 1 - glow buffer
        U[vertex][1] = baseU[vertex] + glowOffsetU;
        V[vertex][1] = baseV[vertex] + glowOffsetV;
        // Texture 2
        U[vertex][2] = 0;
        V[vertex][2] = 0;
        // Texture 3
        U[vertex][3] = 0;
        V[vertex][3] = 0;
      }
      const float posOffset[4] = {tgtOffsetU,tgtOffsetV,0,0};
      _engine->SetVertexShaderConstantF(cb, 0, U[0], 4);
      _engine->SetVertexShaderConstantF(cb, 4, V[0], 4);
      _engine->SetVertexShaderConstantF(cb, 8, posOffset, 1);


      // Set temporary states
      SetRenderStateTemp rsCOLORWRITEENABLE(D3DRS_COLORWRITEENABLE, D3DCOLORWRITEENABLE_RED | D3DCOLORWRITEENABLE_GREEN | D3DCOLORWRITEENABLE_BLUE | D3DCOLORWRITEENABLE_ALPHA);
      SetSamplerStateTemp ss1ADDRESSU(1, D3DSAMP_ADDRESSU, D3DTADDRESS_CLAMP);
      SetSamplerStateTemp ss1ADDRESSV(1, D3DSAMP_ADDRESSV, D3DTADDRESS_CLAMP);
      SetSamplerStateTemp ss1MINFILTER(1, D3DSAMP_MINFILTER, D3DTEXF_LINEAR);
      SetSamplerStateTemp ss1MAGFILTER(1, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR);
      SetSamplerStateTemp ss2MINFILTER(2, D3DSAMP_MINFILTER, D3DTEXF_POINT);
      SetSamplerStateTemp ss2MAGFILTER(2, D3DSAMP_MAGFILTER, D3DTEXF_POINT);

      if (isLast)
      {
        // set backbuffer as target
        _engine->SwitchToBackBufferRenderTarget();
        // reset viewport z-range to default
        _engine->_minZRange = 0;
        _engine->_maxZRange = 1;
      }
      else
      {
        // Set the original render target as both target and texture 0
        _engine->SetRenderTarget(origRts,origRt, NULL);
        // reset viewport z-range to default
        _engine->_minZRange = 0;
        _engine->_maxZRange = 1;
      }
    

      SetTextureTemp tTex0(0, rt); // Set the original render target as texture 0
      SetTextureTemp tTex1(1, _engine->_doBloom ? glow : NULL); // glow texture here
      SetTextureTemp tTex2(2, _rtAperture[_rtApertureCurrent]); // aperture control texture
      SetTextureTemp tTex3(3, _dec[_decimation2x-1]._rt); // 1x1 maxAvgMin

      // note: stage0 is set to point sampling by default (see FilterPoint in EngineDD9::PostProcess::Prepare)
      D3DTEXTUREFILTERTYPE filter = rescale ? D3DTEXF_LINEAR : D3DTEXF_POINT;
      SetSamplerStateTemp sampFilterMag0(0, D3DSAMP_MAGFILTER, filter);
      SetSamplerStateTemp sampFilterMin0(0, D3DSAMP_MINFILTER, filter);
      SetSamplerStateTemp sampFilterMag1(1, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR);
      SetSamplerStateTemp sampFilterMin1(1, D3DSAMP_MINFILTER, D3DTEXF_LINEAR);

#ifndef _X360SHADERGEN
      SetSamplerStateTemp tempSRGB0(0, D3DSAMP_SRGBTEXTURE, rtIsSRGB);
      SetSamplerStateTemp tempSRGB1(1, D3DSAMP_SRGBTEXTURE, rtIsSRGB);
      SetSamplerStateTemp tempSRGB2(2, D3DSAMP_SRGBTEXTURE, FALSE);
      SetSamplerStateTemp tempSRGB3(3, D3DSAMP_SRGBTEXTURE, decimatedSRGB);
#endif

      // compute r,g,b eye sensitivity

      if (_engine->_nightEyeThis>1e-3f)
      {
        // Set the pixel shader
        _engine->SetPixelShader(cb, _psGlowNight);

        // Value designed for night blue shift
        static float coneAdjust = 60.0f;
        float lumScale = coneAdjust/_engine->_accomFactorThis;

        // Get cones
        float cones = Cones();

        // Get Eye sens
        Color eyeSens = EyeSens(cones);
        
        // Set the temporary shader constant
        SetPixelShaderConstantTemp spc6(26, D3DXVECTOR4(eyeSens.R(), eyeSens.G(), eyeSens.B(), 1) );
        SetPixelShaderConstantTemp spc8(28, D3DXVECTOR4(lumScale, cones, 0,0));

        // Drawing
        _engine->DrawPrimitive(cb, D3DPT_TRIANGLESTRIP, 0, 2);
      }
      else
      {
        // Set the pixel shader
        ColorVal sens = _engine->_nightVisionThis ? NVGSens : DaySens;
        // Set the temporary shader constant
        _engine->SetPixelShader(cb, _engine->_nightVisionThis ? _psGlowNVG : _psGlow);

        // Set the temporary shader constant
        // Drawing
        SetPixelShaderConstantTemp spc6(26, D3DXVECTOR4(sens.R(), sens.G(), sens.B(), 1));
        _engine->DrawPrimitive(cb, D3DPT_TRIANGLESTRIP, 0, 2);
      }
      
      if (!isLast)
      {
        _engine->ResolveRenderTarget();
      }

    }

    // unbind any textures to avoid any hanging states
    _engine->SetTexture(cb, 0, NULL);
    _engine->SetTexture(cb, 1, NULL);
    _engine->SetTexture(cb, 2, NULL);
    _engine->SetTexture(cb, 3, NULL);
    
    // remember properties of this frame
    _engine->_accomFactorPrev = _engine->_accomFactorThis;
  }
  Finish();
#else
  _engine->NoPostprocess();
#endif
  // Switching TL off
}

float EngineDD9::PPGlow::Cones() const
{
  // when adaptation is under 1, we assume iris is fully open
  // under certain level light is received only by rods
  // when accomFactor is high, we are receiving lower

  // subjectiveIntensity = realIntensity*_accomFactorThis
  // realIntensity = subjectiveIntensity/_accomFactorThis;
  static float coneFull = 0.5f;
  static float coneNo = 1.1f;
  float cones = InterpolativC(_engine->_accomFactorThis,coneFull,coneNo,1,-0.2);

  return cones;
}

Color EngineDD9::PPGlow::EyeSens(float cones) const
{
  // make sure we calculate luminance taking rods/cell sensitivity into account
  Color eyeSens = DaySens*cones+NightSens*(1-cones);

  return eyeSens;
}

bool EngineDD9::PPFlareIntensity::Init(const LazyLoadFile &hlsl, const LazyLoadFile &vs)
{

#if defined(_XBOX) || defined(_X360SHADERGEN)
  if (!base::Init(hlsl,vs,"VSPostProcess")) return false;
  // copy pixel shader is enough for the flare effect, we are downsizing only 4x
  if (!CompilePixelShader(hlsl,"PSPostProcessCopy", _ps))
  {
    return false;
  }
#else
  if (!base::Init(hlsl,vs)) return false;
#endif
  // Create flare intensity textures + render targets
  // Flare rendering is done in the back buffer - use same format as backbuffer
  for (int i = 0; i < RenderTargetNum; i++)
  {
    int size = 1 << (RenderTargetNum - i - 1);
    HRESULT err;
    err = _engine->CreateTextureRT(size, size, 1, _engine->_presentPars.BackBufferFormat, D3DPOOL_DEFAULT, _rtLightValues[i], _rtsLightValues[i], true);
    if (err != D3D_OK) {_engine->DDError9("Error: Render target texture creation failed.", err); return false;}
  }

  // Success
  return true;
}

void EngineDD9::PPFlareIntensity::Prepare()
{
  // base can be PostProcess (PC) or PPSimple (X360). PPSimple will set shaders
  base::Prepare();
}

static inline void saturate( LONG &a, LONG minV, LONG maxV )
{
  if( a<minV ) a=minV;
  if( a>maxV ) a=maxV;
}

void EngineDD9::PPFlareIntensity::Do(int x, int y, int sizeX, int sizeY)
{
  #ifndef _XBOX
  // TODOX360: flare visibility testing using visibility predication
  // Draw
  PROFILE_SCOPE_GRF_EX(ppFlr,pp,SCOPE_COLOR_YELLOW);

  const int cb = -1;
  // If scene is not being rendered initialize scene
  bool inScene = _engine->_d3dFrameOpen;
  if (!inScene) _engine->D3DBeginScene();

  // Prepare drawing
  Prepare();
  

  // Set temporary render states
  SetRenderStateTemp rsCOLORWRITEENABLE(D3DRS_COLORWRITEENABLE, D3DCOLORWRITEENABLE_RED | D3DCOLORWRITEENABLE_GREEN | D3DCOLORWRITEENABLE_BLUE | D3DCOLORWRITEENABLE_ALPHA);

  // Copy original render target surface into small texture
  RECT rect;
  rect.left = x - sizeX / 2;
  rect.right = x + sizeX / 2;
  rect.top = y - sizeY / 2;
  rect.bottom = y + sizeY / 2;
  // flares are rendered after Postprocess, therefore we are already running at backbuffer resolution
  int w = _engine->WidthBB();
  int h = _engine->HeightBB();
  saturate(rect.left,0,w);
  saturate(rect.right,0,w);
  saturate(rect.top,0,h);
  saturate(rect.bottom,0,h);
  if (rect.bottom<=rect.top || rect.right<=rect.left)
  {
    // singular case - no flare
    ComRef<IDirect3DSurface9> oldRT;
    _engine->GetDirect3DDevice()->GetRenderTarget(0,oldRT.Init());
    _engine->GetDirect3DDevice()->SetRenderTarget(0,_rtsLightValues[0]);
    // it should not matter much - the flare should be invisible anyway
    _engine->GetDirect3DDevice()->Clear(0,NULL,D3DCLEAR_TARGET,0,0,0);
    
    _engine->GetDirect3DDevice()->SetRenderTarget(0,oldRT);
  }
  else
  {
#ifdef _XBOX
    _engine->GetDirect3DDevice()->SetPixelShader(_ps);

    // store current render target
    ComRef<IDirect3DSurface9> oldRT;
    _engine->GetDirect3DDevice()->GetRenderTarget(0,oldRT.Init());
    // start by resolving a corresponding part of the screen
    _engine->GetDirect3DDevice()->Resolve(
      D3DRESOLVE_RENDERTARGET0|D3DRESOLVE_ALLFRAGMENTS,
      (const D3DRECT *)&rect,_rtLightValues[0],NULL,0,0,NULL,0,0,NULL
      );
#else
  #ifndef _X360SHADERGEN
    _engine->GetDirect3DDevice()->StretchRect(_engine->_currRenderTargetSurface, &rect, _rtsLightValues[0], NULL, D3DTEXF_LINEAR);
  #endif
#endif
  }

  // Downsize somewhat (16x for RenderTargetNum=4)
  for (int i = 1; i < RenderTargetNum; i++)
  {
    #if defined(_XBOX) || defined(_X360SHADERGEN)
    // render from _rtLightValues[i-1] into _rtsLightValues[i - 1]
    _engine->SetTexture(cb, 0, _rtLightValues[i-i]);
    _engine->GetDirect3DDevice()->SetRenderTarget(0,_rtsLightValues[i - 1]);
    // render a full-screen quad with a downscaling pixel shader 
    _engine->GetDirect3DDevice()->DrawPrimitive(D3DPT_TRIANGLELIST, 0, 2);

    _engine->SetTexture(cb, 0, NULL);

    // now resolve into _rtLightValues[i]
    _engine->GetDirect3DDevice()->Resolve(
      D3DRESOLVE_RENDERTARGET0|D3DRESOLVE_ALLFRAGMENTS,
      NULL,_rtLightValues[i],NULL,0,0,NULL,0,0,NULL
      );
    #else
    _engine->GetDirect3DDevice()->StretchRect(_rtsLightValues[i - 1], NULL, _rtsLightValues[i], NULL, D3DTEXF_LINEAR);
    #endif
  }

  // Set light values texture to stage 12
  _engine->SetTexture(cb, 12, _rtLightValues[RenderTargetNum - 1]);

  // If scene is not being rendered finish initialized scene
  if (!inScene) _engine->D3DEndScene();

  #ifdef _XBOX
  _engine->GetDirect3DDevice()->SetRenderTarget(0,oldRT);
  #endif
  
  // Restore usual rendering settings
  Finish();
#endif
}

//==================================================================================================
// ou, ov ...texcoords sampling offsets
static bool CreateScreenPlane( VertexP3T2 *v, int segsX, int segsY, float ou, float ov )
{
  float x0 = -1.0f; // -0.5
  float y0 = -1.0f;
  float z = 1.0f;
  float tu0 = 0.0f;
  float tv0 = 1.0f;
  float dx = 2.0 / segsX;
  float dy = 2.0 / segsY;
  float du = 1.0 / segsX;
  float dv = -1.0 / segsY;
  float ya = y0;
  float yb = y0 + dy;
  float tva = tv0 + ov;
  float tvb = tva + dv;
  float x;
  float tu;
  int i, j;
  for( j = 0; j < segsY; j++ )
  {
    x = x0;
    tu = tu0 + ou;
    for( i = 0; i <= segsX; i++ )
    {
      v->pos = Vector3P( x, ya, z ); v->tc.u = tu; v->tc.v = tva; ++v;
      v->pos = Vector3P( x, yb, z ); v->tc.u = tu; v->tc.v = tvb; ++v;
      if( i == segsX )
      {
        if( j != segsY - 1 )
        {
          v->pos = Vector3P( x, yb, z ); v->tc.u = tu; v->tc.v = tvb; ++v;
          v->pos = Vector3P( x0, yb, z ); v->tc.u = tu0; v->tc.v = tvb; ++v;
        }
      }
      else
      {
        x += dx;
        tu += du;
      }
    }
    ya = yb; yb += dy;
    tva = tvb; tvb += dv;
  }
  return true;
}

//==================================================================================================
static bool CreateScreenPlane2( VertexP3T2 *v, int segsX, int segsY )
{
  float randmaxr = 2 * H_PI / RAND_MAX;
  int n = 3; // num randoms per vertex
  int ls = (segsX+2) * n; // line size
  int size = ls * (segsY+2);

  AutoArray<float, MemAllocLocal<float, 4096> > a;
  a.Resize(size);
  for( int i = 0; i < size; i++ )
    a[i] = rand() * randmaxr - H_PI;
  
  bool even = true;
  float dx = 2.0 / segsX;
  float dy = 2.0 / segsY;
  float xa, xb, ya, yb;
  float xa0 = -1.0f - dx * 0.5f;
  float xb0 = -1.0f;
  int i, j;
  ya = -1.0f;
  yb = ya + dy;
  for( j = 0; j < segsY; j++ )
  {
    if( even )
    {
      xa = xa0; xb = xb0;
      for( i = 0; i <= (segsX+1); i++ )
      {
        v->pos = Vector3P( xa, ya, a[ls*j + i*n] ); v->tc.u = a[ls*j + i*n + 1]; v->tc.v = a[ls*j + i*n + 2]; ++v;
        v->pos = Vector3P( xb, yb, a[ls*(j+1) + i*n] ); v->tc.u = a[ls*(j+1) + i*n + 1]; v->tc.v = a[ls*(j+1) + i*n + 2]; ++v;
        xa += dx; xb += dx;
      }
    }
    else
    {
      xa = xb0 - dx; xb = xa0;
      for( i = 0; i <= (segsX+1); i++ )
      {
        v->pos = Vector3P( xa, ya, a[ls*j + (i-1)*n] ); v->tc.u = a[ls*j + (i-1)*n + 1]; v->tc.v = a[ls*j + (i-1)*n + 2]; ++v;
        v->pos = Vector3P( xb, yb, a[ls*(j+1) + i*n] ); v->tc.u = a[ls*(j+1) + i*n + 1]; v->tc.v = a[ls*(j+1) + i*n + 2]; ++v;
        xa += dx; xb += dx;
      }
    }
    ya = yb; yb += dy;
    even = !even;
  }
  return true;
}

//==================================================================================================
void EngineDD9::PPRotBlur::SetThresholdAngle( float angle )
{
  minCos = cos( angle * H_PI / 180 );
}
//==================================================================================================
bool EngineDD9::PPRotBlur::Init( const LazyLoadFile &hlsl, const LazyLoadFile &vs )
{
  //if( !base::Init( hlsl, vs, "VsPpRotBlur" ) ) return false;
  if (!base::Init(hlsl,vs)) return false;
  enabled = false;
  SetDefaultValues();

  // screen plane construction
  const int segsX = 16;
  const int segsY = 10;
  vertexCount = ( segsX + 1 + 1 ) * 2 * segsY - 2;
  stripSize = vertexCount - 2;
  // Create the VB
  ComRef<IDirect3DVertexBuffer9> vb;
  if (FAILED(_engine->GetDirect3DDevice()->CreateVertexBuffer(sizeof(VertexP3T2) * vertexCount, D3DUSAGE_WRITEONLY, 0, D3DPOOL_DEFAULT, vb.Init(), NULL)))
  {
    LogF("Error: Vertex buffer creation failed.");
    return false;
  }
  VertexP3T2 *v;
  // pixel offsets
  float ou = 1.0f / _engine->Width() * 0.5f; //See the Nearest-Point Sampling in the DX9 documentation
  float ov = 1.0f / _engine->Height() * 0.5f;
  vb->Lock(0, 0, (void**)&v, 0);
  CreateScreenPlane( v, segsX, segsY, ou, ov );
  vb->Unlock();

  // Create the VD
  const D3DVERTEXELEMENT9 vertexDecl[] =
  {
    {0,  0, D3DDECLTYPE_FLOAT3,   D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION, 0},
    {0, 12, D3DDECLTYPE_FLOAT2,   D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 0},
    D3DDECL_END()
  };
  ComRef<IDirect3DVertexDeclaration9> vd;
  if (FAILED(_engine->GetDirect3DDevice()->CreateVertexDeclaration(vertexDecl, vd.Init())))
  {
    LogF("Error: Vertex declaration creation failed.");
    return false;
  }
  // note: vertex shader is stored in PS.hlsl for simple effects
  if (!CompileVertexShader(hlsl,"VsPpRotBlur",_vs))
  {
    return false;
  }
  _vb = VertexStaticData9::BufferFuture(vb);
  _vd = vd;
  if( !CompilePixelShader( hlsl, "PsPpRotBlur", _ps ) ) return false;
  //_ppGaussianBlur = ppGaussianBlur;
  //cam = camera;
  
  _lastTime = Glob.time;

  _oldView = MIdentity;

  // angle upper bound
  _upperBound = 10.0f * (H_PI / 180.0f);

  return true;
}
//==================================================================================================
void EngineDD9::PPRotBlur::Prepare()
{
  const int cb = -1;
  base::Prepare();
  // Set the stream source
  _engine->SetStreamSource( cb, _vb, sizeof(VertexP3T2), vertexCount );
  // Set the vertex declaration
  _engine->SetVertexDeclaration(cb, _vd);
  // Set the vertex shader
  _engine->SetVertexShader(cb, _vs);
  // Set the pixel shader
  _engine->SetPixelShader(cb, _ps);
}

//==================================================================================================
void EngineDD9::PPRotBlur::Do( bool isLast )
{
  ///if( !enabled ) return;
  // Draw
  PROFILE_SCOPE_GRF_EX( ppRotBlur, pp, SCOPE_COLOR_YELLOW );

  const int cb = -1;
  // threshold
  // current cam matrix
  Matrix4 &m1 = _engine->CBState(cb)._view;

  //  last frame view matrix, inverse current view matrix
  Matrix3 final3 = _oldView.Orientation() * m1.Orientation().InverseGeneral();

  // convert matrix to axis-align form, for this we use quaternion
  Quaternion<float> quatern;
  quatern.FromMatrixRotation(final3);
  quatern.Normalize();

  float cosAngle = quatern.W();
  // rotation angle, which is used as lower and upper bound
  float angle = acos(cosAngle) * 2.0f;

  static float BPMult = 0.33f;
  float bPower = (_engine->_nightVisionThis) ? (blurPower * BPMult) : blurPower;

  // apply rotation blur only when rotation angle is greater than lower bound
  if (angle < minCos)
  {
    // If scene is not being rendered initialize scene
    bool inScene = _engine->_d3dFrameOpen;
    if (!inScene) _engine->D3DBeginScene();

    // Prepare drawing
    Prepare();

    // Set temporary render states
    SetRenderStateTemp rsCOLORWRITEENABLE(D3DRS_COLORWRITEENABLE, D3DCOLORWRITEENABLE_RED | D3DCOLORWRITEENABLE_GREEN | D3DCOLORWRITEENABLE_BLUE | D3DCOLORWRITEENABLE_ALPHA);

    // Stage 0 - base texture - do the copy
    IDirect3DTexture9 *rt = _engine->CreateRenderTargetCopyAsTexture();
    SetTextureTemp tTex0(0, rt);
#ifndef _X360SHADERGEN
    SetSamplerStateTemp ss0SRGBTEXTURE(0, D3DSAMP_SRGBTEXTURE, _engine->_caps._rtIsSRGB);
#endif
    SetSamplerStateTemp ss0MINFILTER(0, D3DSAMP_MINFILTER, D3DTEXF_LINEAR);
    SetSamplerStateTemp ss0MAGFILTER(0, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR);
    SetSamplerStateTemp ss0ADDRESSU(0, D3DSAMP_ADDRESSU, D3DTADDRESS_CLAMP);
    SetSamplerStateTemp ss0ADDRESSV(0, D3DSAMP_ADDRESSV, D3DTADDRESS_CLAMP);

    // Stage 1 - depth map
    SetTextureTemp tTex1(1, _engine->GetDepthAsTexture());
#ifndef _X360SHADERGEN
    SetSamplerStateTemp ss1SRGBTEXTURE(1, D3DSAMP_SRGBTEXTURE, FALSE);
#endif
    SetSamplerStateTemp ss1MINFILTER(1, D3DSAMP_MINFILTER, D3DTEXF_POINT);
    SetSamplerStateTemp ss1MAGFILTER(1, D3DSAMP_MAGFILTER, D3DTEXF_POINT);
    SetSamplerStateTemp ss1ADDRESSU(1, D3DSAMP_ADDRESSU, D3DTADDRESS_CLAMP);
    SetSamplerStateTemp ss1ADDRESSV(1, D3DSAMP_ADDRESSV, D3DTADDRESS_CLAMP);

    /*
    -----------------------------------------------------------------------------------------------------------
    Originally was motion vector determined by difference between position in current frame and last frame and 
    for this purpose were both matrices send into vertex shader ...
    
      posA = pos * matrixA ... old view matrix
      posB = pos * matrixB ... current view matrix
      dir = posB - posA
      dir = dir * inverseMatrixB
    
    Now we used improved method based on single matrix, way to determine this matrix is shown in next lines ...
    
      dir = (posB - posA) * inverseMatrixB
      dir = (pos * matrixB - pos * matrixA) * inverseMatrixB
      dir = pos - (pos * matrixA * inverseMatrixB);
    -----------------------------------------------------------------------------------------------------------
    */

    Vector3 vec(quatern.X(), quatern.Y(), quatern.Z());
    float dt = Glob.time - _lastTime;

    // fps > 30
    if (dt < 0.033333f)
    {
      if (angle >_upperBound)
      {
        final3.SetRotationAxis(vec, _upperBound);
      }
    }
    else
    {
      // rotation bound is select as min(maximum rotational angle, angular velocity with regard to 30fps)
      float currBound = min(_upperBound, (angle / dt) * 0.033333f);

      final3.SetRotationAxis(vec, currBound);
    }

    //tune opportunity
    //static float depthThreshold_ = 1.5f;
    //SetVertexShaderConstantTemp svc0( 0, D3DXVECTOR4( final3(0, 0), final3(0, 1), final3(0, 2), depthThreshold_ ) );
    SetVertexShaderConstantTemp svc0( 0, D3DXVECTOR4( final3(0, 0), final3(0, 1), final3(0, 2), depthThreshold ) );
    SetVertexShaderConstantTemp svc1( 1, D3DXVECTOR4( final3(1, 0), final3(1, 1), final3(1, 2), bPower ) );
    SetVertexShaderConstantTemp svc2( 2, D3DXVECTOR4( final3(2, 0), final3(2, 1), final3(2, 2), 0 ) );    

    float kx = 1.0f / _engine->CBState(cb)._proj( 0, 0 );
    float ky = 1.0f / _engine->CBState(cb)._proj( 1, 1 );
    float ktc = _engine->CBState(cb)._proj( 1, 1 )*kx;
    SetVertexShaderConstantTemp svc6( 6, D3DXVECTOR4( kx, ky, ktc, 0 ) );
    //SetVertexShaderConstantTemp svc6( 6, D3DXVECTOR4( 1.0, 1.0, 1.0, 0 ) );

    // Drawing
    _engine->DrawPrimitive( cb, D3DPT_TRIANGLESTRIP, 0, stripSize );

#ifdef _XBOX
    // Resolve content of the render target
    CALL_D3D_MEMBER(_engine,Resolve,D3DRESOLVE_RENDERTARGET0|D3DRESOLVE_ALLFRAGMENTS, NULL, _engine->_currRenderTarget, NULL, 0, 0, NULL, 1.0f, 0L, NULL);
#endif

    // If scene is not being rendered finish initialized scene
    if (!inScene) _engine->D3DEndScene();

    // Restore usual rendering settings
    Finish();
  }

  _lastTime = Glob.time;
  _oldView = m1;
}
//==================================================================================================
void EngineDD9::PPSimpleExt::Prepare()
{
  base::Prepare();
  const int cb = -1;
  const PPResourcesExt *res = Resources();
  if (res)
  {
    // Set the stream source
    _engine->SetStreamSource(cb,res->_vb, sizeof(Vertex), 4);

    // Set the vertex declaration
    _engine->SetVertexDeclaration(cb,res->_vd);

    // Set the vertex shader
    _engine->SetVertexShader(cb,res->_vs);

    // Set the pixel shader
    _engine->SetPixelShader(cb,res->_ps);
  }
}
//==================================================================================================
bool EngineDD9::PPSimpleExt::Init(const LazyLoadFile &hlsl, const LazyLoadFile &vs, const char *vsName)
{
  if (!base::Init(hlsl,vs)) return false;

  Ref<PPResourcesExt> res = new PPResourcesExt;

  // Create the VB
  ComRef<IDirect3DVertexBuffer9> vb;
  if (FAILED(_engine->GetDirect3DDevice()->CreateVertexBuffer(sizeof(Vertex) * 4, D3DUSAGE_WRITEONLY, 0, D3DPOOL_DEFAULT, vb.Init(), NULL)))
  {
    LogF("Error: Vertex buffer creation failed.");
    return false;
  }
  Vertex *v;
  float offsetU = 1.0f / _engine->Width() * 0.5f; //See the Nearest-Point Sampling in the DX9 documentation
  float offsetV = 1.0f / _engine->Height() * 0.5f;
  vb->Lock(0, 0, (void**)&v, 0);
  v[0].pos = Vector3P(-0.5, -0.5, 0.5); v[0].t0.u = 0 + offsetU; v[0].t0.v = 1 + offsetV;
  v[1].pos = Vector3P(-0.5,  0.5, 0.5); v[1].t0.u = 0 + offsetU; v[1].t0.v = 0 + offsetV;
  v[2].pos = Vector3P( 0.5, -0.5, 0.5); v[2].t0.u = 1 + offsetU; v[2].t0.v = 1 + offsetV;
  v[3].pos = Vector3P( 0.5,  0.5, 0.5); v[3].t0.u = 1 + offsetU; v[3].t0.v = 0 + offsetV;
  vb->Unlock();

  // Create the VD
  const D3DVERTEXELEMENT9 vertexDecl[] =
  {
    {0,  0, D3DDECLTYPE_FLOAT3,   D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION, 0},
    {0, 12, D3DDECLTYPE_FLOAT2,   D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 0},
    D3DDECL_END()
  };
  ComRef<IDirect3DVertexDeclaration9> vd;
  if (FAILED(_engine->GetDirect3DDevice()->CreateVertexDeclaration(vertexDecl, vd.Init())))
  {
    LogF("Error: Vertex declaration creation failed.");
    return false;
  }

  // note: vertex shader is stored in PS.hlsl for simple effects
  if (!CompileVertexShader(hlsl,vsName,res->_vs))
  {
    return false;
  }

  res->_vb = VertexStaticData9::BufferFuture(vb);
  res->_vd = vd;

  _resrc = res;

  return true;
}
//==================================================================================================
bool EngineDD9::PPSimpleExt::Init(PPResources *resources, bool initFirst)
{
  if (resources)
  {
    SetLevel(0xFF); // default level
    _resrc = resources;
    return true;
  }

  return false;
}

//==================================================================================================
bool EngineDD9::PPRadialBlur::Init( const LazyLoadFile &hlsl, const LazyLoadFile &vs, bool firstInit )
{
  if( !base::Init( hlsl, vs, "VSPostProcess" ) ) return false;
  PPResourcesExt *res = const_cast<PPResourcesExt *> (base::Resources());
  if( !CompilePixelShader( hlsl, "PsPpRadialBlur", res->_ps, true) ) return false;
  
  if (firstInit)
  {
    SetDefaultValues();
    enabled = false;
    _pInterp.Initialize();
  }

  return true;
}

bool EngineDD9::PPRadialBlur::Init(PPResources *resources, bool firstInit)
{
  if (!base::Init(resources, firstInit)) return false;

  if (firstInit)
  {
    SetDefaultValues();
    enabled = false;
    _pInterp.Initialize();
  }

  return true;
}

int EngineDD9::PPRadialBlur::SetParams(const AutoArray<float> &pars)
{
  if (pars.Size() == 4)
  {
    SetBlurPower( pars[0], pars[1] );
    SetBlurOffset( pars[2], pars[3] );

    return 0;
  }

  return 4;
}

int EngineDD9::PPRadialBlur::SetParams(const float* pars, int nPars)
{
  if (nPars == 4)
  {
    SetBlurPower( pars[0], pars[1] );
    SetBlurOffset( pars[2], pars[3] );

    return 0;
  }
  return 4;
}
//==================================================================================================
bool EngineDD9::PPRadialBlur::UnInit()
{
  if (_resrc) 
  { 
    _resrc->UnInit(); 
    return true;
  }

  return false;
}
//==================================================================================================
void EngineDD9::PPRadialBlur::Do( bool isLast )
{
  if( !enabled )
    return;

  // Draw
  PROFILE_SCOPE_GRF_EX( ppRadialBlur, pp, SCOPE_COLOR_YELLOW );

  // linearly interpolated parameters
  RadBlurPar pars = _pInterp.Simulate();
  const PPResourcesExt *res = Resources();
  const int cb = -1;

  if (!res) return;

  // If scene is not being rendered initialize scene
  bool inScene = _engine->_d3dFrameOpen;
  if (!inScene) _engine->D3DBeginScene();

  // Prepare drawing
  Prepare();

  // Set temporary render states
  SetRenderStateTemp rsCOLORWRITEENABLE(D3DRS_COLORWRITEENABLE, D3DCOLORWRITEENABLE_RED | D3DCOLORWRITEENABLE_GREEN | D3DCOLORWRITEENABLE_BLUE | D3DCOLORWRITEENABLE_ALPHA);

  // Stage 0 - base texture - do the copy (to make sure same data is both in source texture and destination render target)
  IDirect3DTexture9 *rt = _engine->CreateRenderTargetCopyAsTexture();
  SetTextureTemp tTex0(0, rt);
#ifndef _X360SHADERGEN
  SetSamplerStateTemp ss0SRGBTEXTURE(0, D3DSAMP_SRGBTEXTURE, _engine->_caps._rtIsSRGB);
#endif
  SetSamplerStateTemp ss0MINFILTER(0, D3DSAMP_MINFILTER, D3DTEXF_LINEAR);
  SetSamplerStateTemp ss0MAGFILTER(0, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR);
  SetSamplerStateTemp ss0ADDRESSU(0, D3DSAMP_ADDRESSU, D3DTADDRESS_CLAMP);
  SetSamplerStateTemp ss0ADDRESSV(0, D3DSAMP_ADDRESSV, D3DTADDRESS_CLAMP);

  // if effect is last then switch to back buffer
  if (isLast) _engine->SwitchToBackBufferRenderTarget();

  // Prepare PS constants
  //SetPixelShaderConstantTemp spc0( 0, D3DXVECTOR4( blurPowerX, blurPowerY, blurOffsetX, blurOffsetY ) );
  SetPixelShaderConstantTemp spc0( 0, D3DXVECTOR4( pars._powerX, pars._powerY, pars._offsetX, pars._offsetY ) );
  float kx = 1.0 / _engine->CBState(cb)._proj( 0, 0 );
  float ky = 1.0 / _engine->CBState(cb)._proj( 1, 1 );
  float ktc = _engine->CBState(cb)._proj( 1, 1 )*kx;
  SetPixelShaderConstantTemp spc6( 6, D3DXVECTOR4( kx, ky, ktc, 0 ) );

  // Drawing
  _engine->DrawPrimitive(cb, D3DPT_TRIANGLESTRIP, 0, 2);

#ifdef _XBOX
  // Resolve content of the render target
  CALL_D3D_MEMBER(_engine,Resolve,D3DRESOLVE_RENDERTARGET0|D3DRESOLVE_ALLFRAGMENTS, NULL, _engine->_currRenderTarget, NULL, 0, 0, NULL, 1.0f, 0L, NULL);
#endif

  // If scene is not being rendered finish initialized scene
  if (!inScene) _engine->D3DEndScene();

  // Restore usual rendering settings
  Finish();
}

LSError EngineDD9::PPRadialBlur::Serialize( ParamArchive &ar )
{
  CHECK(ar.Serialize("enabled",enabled,0,false));
  CHECK(ar.Serialize("interp", _pInterp, 0));
  CHECK(ar.Serialize("prior", _priority, 0, -1));

  return LSOK;
}
//==================================================================================================
bool EngineDD9::PPColors::Init(PPResources *resources, bool firstInit)
{
  if (resources)
  {
    _resrc = resources;

    if (firstInit)
    {
      enabled = false;
      SetDefaultValues();
      _pInterp.Initialize();
    }

    return true;
  }

  return false;
}
//==================================================================================================
bool EngineDD9::PPColors::Init( const LazyLoadFile &hlsl, const LazyLoadFile &vs, bool firstInit )
{
  if (!base::Init(hlsl,vs)) return false;
  // sepia test
  //SetColorizeColor( Color( 1.25f, 0.85f, 0.60f, 0.0f ) );

  Ref<PPColorResources> resrc = new PPColorResources;

  // screen plane construction
  const int segsX = 1;
  const int segsY = 1;
  resrc->_vertexCount = ( segsX + 1 + 1 ) * 2 * segsY - 2;
  resrc->_stripSize = resrc->_vertexCount - 2;

  // Create the VB
  ComRef<IDirect3DVertexBuffer9> vb;
  if (FAILED(_engine->GetDirect3DDevice()->CreateVertexBuffer(sizeof(VertexP3T2) * resrc->_vertexCount, D3DUSAGE_WRITEONLY, 0, D3DPOOL_DEFAULT, vb.Init(), NULL)))
  {
    LogF("Error: Vertex buffer creation failed.");
    return false;
  }
  VertexP3T2 *v;
  // pixel offsets
  float ou = 1.0f / _engine->Width() * 0.5f; //See the Nearest-Point Sampling in the DX9 documentation
  float ov = 1.0f / _engine->Height() * 0.5f;
  vb->Lock(0, 0, (void**)&v, 0);
  CreateScreenPlane( v, segsX, segsY, ou, ov );
  vb->Unlock();

  // Create the VD
  const D3DVERTEXELEMENT9 vertexDecl[] =
  {
    {0,  0, D3DDECLTYPE_FLOAT3,   D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION, 0},
    {0, 12, D3DDECLTYPE_FLOAT2,   D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 0},
    D3DDECL_END()
  };
  ComRef<IDirect3DVertexDeclaration9> vd;
  if (FAILED(_engine->GetDirect3DDevice()->CreateVertexDeclaration(vertexDecl, vd.Init())))
  {
    LogF("Error: Vertex declaration creation failed.");
    return false;
  }
  // note: vertex shader is stored in PS.hlsl for simple effects
  if (!CompileVertexShader(hlsl,"VsPpColors",resrc->_vs))
  {
    return false;
  }
  resrc-> _vb = VertexStaticData9::BufferFuture(vb);
  resrc->_vd = vd;
  if( !CompilePixelShader( hlsl, "PsPpColors", resrc->_ps ) ) return false;

  _resrc = resrc;

  if (firstInit)
  {
    SetDefaultValues();
    enabled = false;
    _pInterp.Initialize();
  }

  return true;
}
//==================================================================================================
void EngineDD9::PPColors::Do( bool isLast )
{
  if( !enabled )
    return;

  // Draw
  PROFILE_SCOPE_GRF_EX( ppColors, pp, SCOPE_COLOR_YELLOW );

  ColorPar pars = _pInterp.Simulate();
  const PPColorResources* res = Resources();
  const int cb = -1;

  if (!res) return;

  // If scene is not being rendered initialize scene
  bool inScene = _engine->_d3dFrameOpen;
  if (!inScene) _engine->D3DBeginScene();

  // Prepare drawing
  Prepare();

  // Set temporary render states
  SetRenderStateTemp rsCOLORWRITEENABLE(D3DRS_COLORWRITEENABLE, D3DCOLORWRITEENABLE_RED | D3DCOLORWRITEENABLE_GREEN | D3DCOLORWRITEENABLE_BLUE | D3DCOLORWRITEENABLE_ALPHA);

  // Stage 0 - base texture - do the copy (to make sure same data is both in source texture and destination render target)
  IDirect3DTexture9 *rt = _engine->CreateRenderTargetCopyAsTexture();
  SetTextureTemp tTex0(0, rt);

  // if effect is last then switch to back buffer
  if (isLast) _engine->SwitchToBackBufferRenderTarget();

#ifndef _X360SHADERGEN
  SetSamplerStateTemp ss0SRGBTEXTURE(0, D3DSAMP_SRGBTEXTURE, _engine->_caps._rtIsSRGB);
#endif
  SetSamplerStateTemp ss0MINFILTER(0, D3DSAMP_MINFILTER, D3DTEXF_LINEAR);
  SetSamplerStateTemp ss0MAGFILTER(0, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR);
  SetSamplerStateTemp ss0ADDRESSU(0, D3DSAMP_ADDRESSU, D3DTADDRESS_CLAMP);
  SetSamplerStateTemp ss0ADDRESSV(0, D3DSAMP_ADDRESSV, D3DTADDRESS_CLAMP);

  // Prepare PS constants
  SetPixelShaderConstantTemp ssPpColorsPars( 0, D3DXVECTOR4( pars._brightness, pars._contrast, pars._offset, 0 ) );
  SetPixelShaderConstantTemp ssLerpColor( 2, D3DXVECTOR4( pars._lerpColor.R(), pars._lerpColor.G(), pars._lerpColor.B(), pars._lerpColor.A() ) );
  SetPixelShaderConstantTemp ssColorizeColor( 3, D3DXVECTOR4( pars._colorizeColor.R(), pars._colorizeColor.G(), pars._colorizeColor.B(), pars._colorizeColor.A() ) );
  SetPixelShaderConstantTemp ssRgbWeights( 4, D3DXVECTOR4( pars._rgbWeights.R(), pars._rgbWeights.G(), pars._rgbWeights.B(), 0 ) );

  float kx = 1.0 / _engine->CBState(cb)._proj( 0, 0 );
  float ky = 1.0 / _engine->CBState(cb)._proj( 1, 1 );
  float ktc = _engine->CBState(cb)._proj( 1, 1 )*kx;
  SetPixelShaderConstantTemp spc6( 6, D3DXVECTOR4( kx, ky, ktc, 0 ) );


  // Drawing
  _engine->DrawPrimitive( cb, D3DPT_TRIANGLESTRIP, 0, res->_stripSize );

#ifdef _XBOX
  if (!isLast)
  {
    // Resolve content of the render target
    CALL_D3D_MEMBER(_engine,Resolve,D3DRESOLVE_RENDERTARGET0|D3DRESOLVE_ALLFRAGMENTS, NULL, _engine->_currRenderTarget, NULL, 0, 0, NULL, 1.0f, 0L, NULL);
  }
#endif

  // If scene is not being rendered finish initialized scene
  if (!inScene) _engine->D3DEndScene();

  // Restore usual rendering settings
  Finish();
}
//==================================================================================================
void EngineDD9::PPColors::Prepare()
{
  const int cb = -1;
  const PPColorResources *res = Resources();

  if (!res) return;

  base::Prepare();
  // Set the stream source
  _engine->SetStreamSource(cb, res->_vb, sizeof(VertexP3T2), res->_vertexCount);
  // Set the vertex declaration
  _engine->SetVertexDeclaration(cb, res->_vd);
  // Set the vertex shader
  _engine->SetVertexShader(cb, res->_vs);
  // Set the pixel shader
  _engine->SetPixelShader(cb, res->_ps);
}
//==================================================================================================
int EngineDD9::PPColors::SetParams(const AutoArray<float> &pars)
{
  if (pars.Size() == 15)
  {
    SetBrightness(pars[0]);
    SetContrast(pars[1]);
    SetOffset(pars[2]);
    SetBlendColor(Color( pars[3], pars[4], pars[5], pars[6] ));
    SetColorizeColor(Color( pars[7], pars[8], pars[9], pars[10] ));
    SetRGBWeights(Color( pars[11], pars[12], pars[13], pars[14] ));

    return 0;
  }

  return 15;
}

int EngineDD9::PPColors::SetParams(const float* pars, int nPars)
{
  if (nPars == 15)
  {
    SetBrightness(pars[0]);
    SetContrast(pars[1]);
    SetOffset(pars[2]);
    SetBlendColor(Color( pars[3], pars[4], pars[5], pars[6] ));
    SetColorizeColor(Color( pars[7], pars[8], pars[9], pars[10] ));
    SetRGBWeights(Color( pars[11], pars[12], pars[13], pars[14] ));

    return 0;
  }

  return 15;
}

//==================================================================================================
bool EngineDD9::PPColors::UnInit()
{
  if (_resrc) 
  {
    _resrc->UnInit();
    return true;
  }

  return false;
}
//==================================================================================================
LSError EngineDD9::PPColors::Serialize( ParamArchive &ar )
{
  CHECK(ar.Serialize("enabled",enabled,0,false));
  CHECK(ar.Serialize("pInterp",_pInterp,0));
  CHECK(ar.Serialize("prior", _priority, 0, -1));

  return LSOK;
}
//==================================================================================================
bool EngineDD9::PPChromAber::Init( const LazyLoadFile &hlsl, const LazyLoadFile &vs, bool firstInit )
{
  if( !base::Init( hlsl, vs, "VSPostProcess" ) ) return false;
  PPResourcesExt *res = const_cast<PPResourcesExt *> (base::Resources());
  if( !CompilePixelShader( hlsl, "PsPpChromAber", res->_ps, true) ) return false;

  if (firstInit)
  {
    SetDefaultValues();
    enabled = false;
    _pInterp.Initialize();
  }

  return true;
}

bool EngineDD9::PPChromAber::Init(PPResources *resources, bool firstInit)
{
  if (!base::Init(resources, firstInit)) return false;
  
  if (firstInit)
  {
    SetDefaultValues();
    enabled = false;
    _pInterp.Initialize();
  }

  return true;
}

//==================================================================================================
int EngineDD9::PPChromAber::SetParams(const AutoArray<float> &pars)
{
  if (pars.Size() == 3)
  {
    SetAberration( pars[0], pars[1] );
    SetAspectCorrection( pars[2] > 0.0f );

    return 0;
  }

  return 3;
}

int EngineDD9::PPChromAber::SetParams(const float* pars, int nPars)
{
  if (nPars == 3)
  {
    SetAberration( pars[0], pars[1] );
    SetAspectCorrection( pars[2] > 0.0f );

    return 0;
  }

  return 3;
}
//==================================================================================================
bool EngineDD9::PPChromAber::UnInit()
{
  if (_resrc)
  {
    _resrc->UnInit();
    return true;
  }

  return false;
}
//==================================================================================================
void EngineDD9::PPChromAber::Do( bool isLast )
{
  if( !enabled )
    return;

  // Draw
  PROFILE_SCOPE_GRF_EX( ppChromAber, pp, SCOPE_COLOR_YELLOW );
  
  // linearly interpolated parameters 
  ChromAberPar pars = _pInterp.Simulate();
  const int cb = -1;


  // If scene is not being rendered initialize scene
  bool inScene = _engine->_d3dFrameOpen;
  if (!inScene) _engine->D3DBeginScene();

  // Prepare drawing
  Prepare();

  // if effect is last then switch to back buffer
  if (isLast) _engine->SwitchToBackBufferRenderTarget();
  
  // Set temporary render states
  SetRenderStateTemp rsCOLORWRITEENABLE(D3DRS_COLORWRITEENABLE, D3DCOLORWRITEENABLE_RED | D3DCOLORWRITEENABLE_GREEN | D3DCOLORWRITEENABLE_BLUE | D3DCOLORWRITEENABLE_ALPHA);

  // Stage 0 - base texture - do the copy
  IDirect3DTexture9 *rt = _engine->CreateRenderTargetCopyAsTexture();
  SetTextureTemp tTex0(0, rt);
#ifndef _X360SHADERGEN
  SetSamplerStateTemp ss0SRGBTEXTURE(0, D3DSAMP_SRGBTEXTURE, _engine->_caps._rtIsSRGB);
#endif
  SetSamplerStateTemp ss0MINFILTER(0, D3DSAMP_MINFILTER, D3DTEXF_LINEAR);
  SetSamplerStateTemp ss0MAGFILTER(0, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR);
  SetSamplerStateTemp ss0ADDRESSU(0, D3DSAMP_ADDRESSU, D3DTADDRESS_CLAMP);
  SetSamplerStateTemp ss0ADDRESSV(0, D3DSAMP_ADDRESSV, D3DTADDRESS_CLAMP);

  float ac = 1;
  if( pars._aspectCorrection )
    ac = _engine->CBState(cb)._proj( 0, 0 ) / _engine->CBState(cb)._proj( 1, 1 );
  //SetPixelShaderConstantTemp spc6( 6, D3DXVECTOR4( ac, 0, 0, 0 ) );
  // Prepare PS constants
  SetPixelShaderConstantTemp spc0( 0, D3DXVECTOR4( pars._powerX * ac, pars._powerY, 0, 0 ) );

  // Drawing
  _engine->DrawPrimitive(cb, D3DPT_TRIANGLESTRIP, 0, 2);

#ifdef _XBOX
  // Resolve content of the render target
  CALL_D3D_MEMBER(_engine,Resolve,D3DRESOLVE_RENDERTARGET0|D3DRESOLVE_ALLFRAGMENTS, NULL, _engine->_currRenderTarget, NULL, 0, 0, NULL, 1.0f, 0L, NULL);
#endif

  
  // If scene is not being rendered finish initialized scene
  if (!inScene) _engine->D3DEndScene();

  // Restore usual rendering settings
  Finish();
}

LSError EngineDD9::PPChromAber::Serialize( ParamArchive &ar )
{
  CHECK(ar.Serialize("enabled",enabled,0,false));
  CHECK(ar.Serialize("interp", _pInterp, 0));
  CHECK(ar.Serialize("prior", _priority, 0, -1));

  return LSOK;
}

bool EngineDD9::PPWetDistort::Init(PPResources *resources, PPGaussianBlur *ppGaussianBlur, bool firstInit)
{
  if (resources)
  {
    _resrc = resources;
    _ppGaussianBlur = ppGaussianBlur;

    if (firstInit)
    {
      SetDefaultValues();
      enabled = false;
      _pInterp.Initialize();
    }

    return true;
  }

  return false;
}

//////////////////////////////////////////////////////////////////////////

bool EngineDD9::PPColorsInversion::Init( const LazyLoadFile &hlsl, const LazyLoadFile &vs, bool firstInit )
{
  if( !base::Init( hlsl, vs, "VSPostProcess" ) ) return false;
  PPResourcesExt *res = const_cast<PPResourcesExt *> (base::Resources());
  if( !CompilePixelShader( hlsl, "PsPpClrInversion", res->_ps, true) ) return false;

  if (firstInit)
  {
    SetDefaultValues();
    _enabled = false;
    _pInterp.Initialize();
  }

  return true;
}

bool EngineDD9::PPColorsInversion::Init(PPResources *resources, bool firstInit)
{
  if (!base::Init(resources, firstInit)) return false;

  if (firstInit)
  {
    SetDefaultValues();
    _enabled = false;
    _pInterp.Initialize();
  }

  return true;
}

bool EngineDD9::PPColorsInversion::UnInit()
{
  if (_resrc)
  {
    _resrc->UnInit();
    return true;
  }

  return false;
}

void EngineDD9::PPColorsInversion::Do(bool isLast)
{
  if (!_enabled) return;

  // Draw
  PROFILE_SCOPE_GRF_EX(ppColorInvers, pp, SCOPE_COLOR_YELLOW);

  // linearly interpolated parameters 
  ColorInversionPar pars = _pInterp.Simulate();
  const int cb = -1;

  // If scene is not being rendered initialize scene
  bool inScene = _engine->_d3dFrameOpen;
  if (!inScene) _engine->D3DBeginScene();

  // Prepare drawing
  Prepare();

  // Set temporary render states
  SetRenderStateTemp rsCOLORWRITEENABLE(D3DRS_COLORWRITEENABLE, D3DCOLORWRITEENABLE_RED | D3DCOLORWRITEENABLE_GREEN | D3DCOLORWRITEENABLE_BLUE | D3DCOLORWRITEENABLE_ALPHA);

  // Stage 0 - base texture - do the copy
  IDirect3DTexture9 *rt = _engine->CreateRenderTargetCopyAsTexture();
  SetTextureTemp tTex0(0, rt);

  // if effect is last then switch to back buffer
  if (isLast) _engine->SwitchToBackBufferRenderTarget();

#ifndef _X360SHADERGEN
  SetSamplerStateTemp ss0SRGBTEXTURE(0, D3DSAMP_SRGBTEXTURE, _engine->_caps._rtIsSRGB);
#endif
  SetSamplerStateTemp ss0MINFILTER(0, D3DSAMP_MINFILTER, D3DTEXF_LINEAR);
  SetSamplerStateTemp ss0MAGFILTER(0, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR);
  SetSamplerStateTemp ss0ADDRESSU(0, D3DSAMP_ADDRESSU, D3DTADDRESS_CLAMP);
  SetSamplerStateTemp ss0ADDRESSV(0, D3DSAMP_ADDRESSV, D3DTADDRESS_CLAMP);

  // Prepare PS constants
  SetPixelShaderConstantTemp spc0(0, D3DXVECTOR4(pars._invColor.R(), pars._invColor.G(), pars._invColor.B(), 0));

  // Drawing
  _engine->DrawPrimitive(cb, D3DPT_TRIANGLESTRIP, 0, 2);

#ifdef _XBOX
  // Resolve content of the render target
  CALL_D3D_MEMBER(_engine,Resolve,D3DRESOLVE_RENDERTARGET0|D3DRESOLVE_ALLFRAGMENTS, NULL, _engine->_currRenderTarget, NULL, 0, 0, NULL, 1.0f, 0L, NULL);
#endif


  // If scene is not being rendered finish initialized scene
  if (!inScene) _engine->D3DEndScene();

  // Restore usual rendering settings
  Finish();
}

int EngineDD9::PPColorsInversion::SetParams(const AutoArray<float> &pars)
{
  if (pars.Size() == 3)
  {
    SetInversion(Color(pars[0], pars[1], pars[2], 0));
    return 0;
  }

  return 1;
}

int EngineDD9::PPColorsInversion::SetParams(const float* pars, int nPars)
{
  if (nPars == 3)
  {
    SetInversion(Color(pars[0], pars[1], pars[2], 0));
    return 0;
  }

  return 1;
}

LSError EngineDD9::PPColorsInversion::Serialize( ParamArchive &ar )
{
  CHECK(ar.Serialize("enabled", _enabled,0,false));
  CHECK(ar.Serialize("interp", _pInterp, 0));
  CHECK(ar.Serialize("prior", _priority, 0, -1));

  return LSOK;
}

//==================================================================================================
bool EngineDD9::PPWetDistort::Init( const LazyLoadFile &hlsl, const LazyLoadFile &vs, PPGaussianBlur *ppGaussianBlur, bool firstInit )
{
  if( !base::Init(hlsl,vs) )
    return false;
  _ppGaussianBlur = ppGaussianBlur;	

  Ref<PPWetDistortResources> res = new PPWetDistortResources;

  // screen plane construction
  const int segsX = 80;
  const int segsY = 50;
  res->_vertexCount = ( segsX + 1 + 1 ) * 2 * segsY;
  res->_stripSize = res->_vertexCount - 2;
  // Create the VB
  ComRef<IDirect3DVertexBuffer9> vb;
  if (FAILED(_engine->GetDirect3DDevice()->CreateVertexBuffer(sizeof(VertexP3T2) * res->_vertexCount, D3DUSAGE_WRITEONLY, 0, D3DPOOL_DEFAULT, vb.Init(), NULL)))
  {
    LogF("Error: Vertex buffer creation failed.");
    return false;
  }
  VertexP3T2 *v;

  vb->Lock(0, 0, (void**)&v, 0);
  CreateScreenPlane2( v, segsX, segsY );
  vb->Unlock();


  // Create the VD
  const D3DVERTEXELEMENT9 vertexDecl[] =
  {
    {0,  0, D3DDECLTYPE_FLOAT3,   D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION, 0},
    {0, 12, D3DDECLTYPE_FLOAT2,   D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 0},
    D3DDECL_END()
  };
  ComRef<IDirect3DVertexDeclaration9> vd;
  if (FAILED(_engine->GetDirect3DDevice()->CreateVertexDeclaration(vertexDecl, vd.Init())))
  {
    LogF("Error: Vertex declaration creation failed.");
    return false;
  }
  // note: vertex shader is stored in PS.hlsl for simple effects
  if (!CompileVertexShader( hlsl,"VsPpWetDistort",res->_vs ) )
  {
    return false;
  }
  res->_vb = VertexStaticData9::BufferFuture(vb);
  res->_vd = vd;
  if( !CompilePixelShader( hlsl, "PsPpWetDistort", res->_ps ) ) return false;

  _resrc = res;

  if (firstInit)
  {
    SetDefaultValues();
    enabled = false;
    _pInterp.Initialize();
  }

  return true;

}

int EngineDD9::PPWetDistort::SetParams(const AutoArray<float> &pars)
{
  if (pars.Size() == 15)
  {
    SetBlurriness( pars[0] );
    SetEffectPower( pars[1], pars[2] );
    SetWaveSpeeds( pars[3], pars[4], pars[5], pars[6] );
    SetWaveAmplitudes( pars[7], pars[8], pars[9], pars[10] );
    SetPhaseCoefs( pars[11], pars[12], pars[13], pars[14] );

    return 0;
  }

  return 15;
}

int EngineDD9::PPWetDistort::SetParams(const float* pars, int nPars)
{
  if (nPars == 15)
  {
    SetBlurriness( pars[0] );
    SetEffectPower( pars[1], pars[2] );
    SetWaveSpeeds( pars[3], pars[4], pars[5], pars[6] );
    SetWaveAmplitudes( pars[7], pars[8], pars[9], pars[10] );
    SetPhaseCoefs( pars[11], pars[12], pars[13], pars[14] );

    return 0;
  }

  return 15;
}

bool EngineDD9::PPWetDistort::UnInit()
{
  _ppGaussianBlur.Free();
  
  if (_resrc) 
  {
    _resrc->UnInit();
    return true;
  }

  return false;
}
//==================================================================================================
void EngineDD9::PPWetDistort::Prepare()
{
  const int cb = -1;
  const PPWetDistortResources *res = Resources();
  if (res)
  {
    base::Prepare();
    // Set the stream source
    _engine->SetStreamSource( cb, res->_vb, sizeof(VertexP3T2), res->_vertexCount );
    // Set the vertex declaration
    _engine->SetVertexDeclaration(cb, res->_vd);
    // Set the vertex shader
    _engine->SetVertexShader(cb, res->_vs);
    // Set the pixel shader
    _engine->SetPixelShader(cb, res->_ps);
  }
}

//==================================================================================================
void EngineDD9::PPWetDistort::Do( bool isLast )
{
  if( !enabled )
    return;

  // Draw
  PROFILE_SCOPE_GRF_EX( ppWetDistort, pp, SCOPE_COLOR_YELLOW );
  
  WetDistortPar pars = _pInterp.Simulate();
  const int cb = -1;
  const PPWetDistortResources *res = Resources();

  if (!res) return;
 
  // If scene is not being rendered initialize scene
  bool inScene = _engine->_d3dFrameOpen;
  if (!inScene) _engine->D3DBeginScene();

  // Prepare drawing
  Prepare();

  // Set temporary render states
  SetRenderStateTemp rsCOLORWRITEENABLE(D3DRS_COLORWRITEENABLE, D3DCOLORWRITEENABLE_RED | D3DCOLORWRITEENABLE_GREEN | D3DCOLORWRITEENABLE_BLUE | D3DCOLORWRITEENABLE_ALPHA);

  // Stage 0 - base texture - do the copy
  IDirect3DTexture9 *rt = _engine->CreateRenderTargetCopyAsTexture();
  SetTextureTemp tTex0(0, rt);
#ifndef _X360SHADERGEN
  SetSamplerStateTemp ss0SRGBTEXTURE(0, D3DSAMP_SRGBTEXTURE, _engine->_caps._rtIsSRGB);
#endif
  SetSamplerStateTemp ss0MINFILTER(0, D3DSAMP_MINFILTER, D3DTEXF_LINEAR);
  SetSamplerStateTemp ss0MAGFILTER(0, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR);
  SetSamplerStateTemp ss0ADDRESSU(0, D3DSAMP_ADDRESSU, D3DTADDRESS_CLAMP);
  SetSamplerStateTemp ss0ADDRESSV(0, D3DSAMP_ADDRESSV, D3DTADDRESS_CLAMP);

  // Stage 1 - blurred texture
  SetTextureTemp tTex1(1, _ppGaussianBlur->GetBlurredTexture());
#ifndef _X360SHADERGEN
  SetSamplerStateTemp ss1SRGBTEXTURE(1, D3DSAMP_SRGBTEXTURE, _engine->_caps._rtIsSRGB);
#endif
  SetSamplerStateTemp ss1MINFILTER(1, D3DSAMP_MINFILTER, D3DTEXF_LINEAR);
  SetSamplerStateTemp ss1MAGFILTER(1, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR);
  SetSamplerStateTemp ss1ADDRESSU(1, D3DSAMP_ADDRESSU, D3DTADDRESS_CLAMP);
  SetSamplerStateTemp ss1ADDRESSV(1, D3DSAMP_ADDRESSV, D3DTADDRESS_CLAMP);

  float t = Glob.time.toFloat();
  SetVertexShaderConstantTemp svc0( 0, D3DXVECTOR4( t, pars._blurriness, pars._powerTop, pars._powerBottom) ); // time, blur, powerTop, powerBottom
  SetVertexShaderConstantTemp svc1( 1, D3DXVECTOR4( pars._waveSpeeds[0], pars._waveSpeeds[1], pars._waveSpeeds[2], pars._waveSpeeds[3] ) ); // speeds
  SetVertexShaderConstantTemp svc2( 2, D3DXVECTOR4( pars._waveAmps[0], pars._waveAmps[1], pars._waveAmps[2], pars._waveAmps[3] ) ); // amplitudes
  SetVertexShaderConstantTemp svc3( 3, D3DXVECTOR4( pars._phaseCoefs[0], pars._phaseCoefs[1], pars._phaseCoefs[2], pars._phaseCoefs[3] ) ); // randX, randY, posX, posY

  // chromatic aberration params
  SetPixelShaderConstantTemp spc4( 4, D3DXVECTOR4( 0.8, 1.0, 1.2, 0 ) );

  // if effect is last then switch to back buffer
  if (isLast) _engine->SwitchToBackBufferRenderTarget();
  
  float kx = 1.0 / _engine->CBState(cb)._proj( 0, 0 );
  float ky = 1.0 / _engine->CBState(cb)._proj( 1, 1 );
  float ktc = _engine->CBState(cb)._proj( 1, 1 ) *kx;
  SetVertexShaderConstantTemp svc6( 6, D3DXVECTOR4( kx, ky, ktc, 0 ) );
  // Drawing
  _engine->DrawPrimitive( cb, D3DPT_TRIANGLESTRIP, 0, res->_stripSize );

#ifdef _XBOX
  // Resolve content of the render target
  CALL_D3D_MEMBER(_engine,Resolve,D3DRESOLVE_RENDERTARGET0|D3DRESOLVE_ALLFRAGMENTS, NULL, _engine->_currRenderTarget, NULL, 0, 0, NULL, 1.0f, 0L, NULL);
#endif
  
  // If scene is not being rendered finish initialized scene
  if (!inScene) _engine->D3DEndScene();
  // Restore usual rendering settings
  Finish();
}

LSError EngineDD9::PPWetDistort::Serialize(ParamArchive &ar)
{
  CHECK(ar.Serialize("enabled",enabled,0,false));
  CHECK(ar.Serialize("pInterp",_pInterp,0));  
  CHECK(ar.Serialize("prior", _priority, 0, -1));

  return LSOK;
}

//==================================================================================================
bool EngineDD9::PPDynamicBlur::Init( const LazyLoadFile &hlsl, const LazyLoadFile &vs, PPGaussianBlur *ppGaussianBlur, bool firstInit )
{
  if( !base::Init( hlsl,vs ) )
    return false;
  _ppGaussianBlur = ppGaussianBlur;

  Ref<PPDynBlurResources> res = new PPDynBlurResources;

  // screen plane construction
  const int segsX = 1;
  const int segsY = 1;
  res->_vertexCount = ( segsX + 1 + 1 ) * 2 * segsY - 2;
  res->_stripSize = res->_vertexCount - 2;
  // Create the VB
  ComRef<IDirect3DVertexBuffer9> vb;
  if (FAILED(_engine->GetDirect3DDevice()->CreateVertexBuffer(sizeof(VertexP3T2) * res->_vertexCount, D3DUSAGE_WRITEONLY, 0, D3DPOOL_DEFAULT, vb.Init(), NULL)))
  {
    LogF("Error: Vertex buffer creation failed.");
    return false;
  }
  VertexP3T2 *v;
  vb->Lock(0, 0, (void**)&v, 0);
  CreateScreenPlane( v, segsX, segsY, 0, 0 ); // ou, ov
  vb->Unlock();
  res->_vb = VertexStaticData9::BufferFuture(vb);
  // Create the VD
  const D3DVERTEXELEMENT9 vertexDecl[] =
  {
    {0,  0, D3DDECLTYPE_FLOAT3,   D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_POSITION, 0},
    {0, 12, D3DDECLTYPE_FLOAT2,   D3DDECLMETHOD_DEFAULT, D3DDECLUSAGE_TEXCOORD, 0},
    D3DDECL_END()
  };
  ComRef<IDirect3DVertexDeclaration9> vd;
  if (FAILED(_engine->GetDirect3DDevice()->CreateVertexDeclaration(vertexDecl, vd.Init())))
  {
    LogF("Error: Vertex declaration creation failed.");
    return false;
  }
  res->_vd = vd;
  // create shaders (note: vertex shader is stored in PS.hlsl for simple effects)
  if( !CompileVertexShader( hlsl,"VsPpDynamicBlurFinal", res->_vsFinal ) )
    return false;
  if( !CompilePixelShader( hlsl, "PsPpDynamicBlurFinal", res->_psFinal ) )
    return false;
  if( !CompileVertexShader( hlsl,"VsPpDynamicBlur", res->_vsBlur ) )
    return false;
  if( !CompilePixelShader( hlsl, "PsPpDynamicBlur", res->_psBlur ) )
    return false;

  _resrc = res;

  if (firstInit)
  {
    SetDefaultValues();
    _enabled = false;
    _pInterp.Initialize();
  }

  return true;
}

bool EngineDD9::PPDynamicBlur::Init(PPResources *resources, PPGaussianBlur *ppGaussianBlur, bool firstInit)
{
  if (resources)
  {
    _ppGaussianBlur = ppGaussianBlur;
    _resrc = resources;

    if (firstInit)
    {
      SetDefaultValues();
      _enabled = false;
      _pInterp.Initialize();
    }

    return true;
  }

  return false;
}

int EngineDD9::PPDynamicBlur::SetParams(const AutoArray<float> &pars) 
{
  if (pars.Size() == 1)
  {
    SetBlurrines(pars[0]);

    return 0;
  }

  return 1;
}

int EngineDD9::PPDynamicBlur::SetParams(const float* pars, int nPars)
{
  if (nPars == 1)
  {
    SetBlurrines(pars[0]);
    return 0;
  }

  return 1;
}
//==================================================================================================
bool EngineDD9::PPDynamicBlur::UnInit()
{
  _ppGaussianBlur.Free();
  
  if (_resrc)
  {
    _resrc->UnInit();
  }

  return true;
}
//==================================================================================================
void EngineDD9::PPDynamicBlur::Do( bool isLast )
{
  if (!_enabled) return;

  DynamicBlurPar pars = _pInterp.Simulate();

  // Draw
  PROFILE_SCOPE_GRF_EX( ppDynamicBlur, pp, SCOPE_COLOR_YELLOW );

  const int cb = -1;
  const PPDynBlurResources *res = Resources();

  if (!res) return;

  // TESTING
  /*float t = Glob.time.toFloat();
  blurriness = ( sinf( t * 2 ) + 1 ) * 20 - 5 ;
  if( blurriness < 0 )
  blurriness = 0;
  if( blurriness > 30 )
  blurriness = 30;*/

  // If scene is not being rendered initialize scene
  bool inScene = _engine->_d3dFrameOpen;
  if (!inScene) _engine->D3DBeginScene();

  // Prepare drawing
  Prepare();

  // Protect VS constants from changing
  PushVertexShaderConstant pushC0(0);
  PushVertexShaderConstant pushC1(1);
  PushVertexShaderConstant pushC2(2);
  PushVertexShaderConstant pushC3(3);
  PushVertexShaderConstant pushC4(4);
  PushVertexShaderConstant pushC5(5);
  PushVertexShaderConstant pushC6(6);
  PushVertexShaderConstant pushC7(7);
  PushVertexShaderConstant pushC8(8);

  int fullWidth = _engine->Width();
  int fullHeight = _engine->Height();
  int partWidth = fullWidth / GBSizeDivisor;
  int partHeight = fullHeight / GBSizeDivisor;

  //int modPartH = fullHeight % GBSizeDivisor;
  //partHeight += modPartH / (GBSizeDivisor / 2);

  float invDiv = 1.0f / GBSizeDivisor;
  // blur direction
  float dirX = 2;
  float dirY = 1;
  // normalize to length  (0.5 / 100 / halfSteps)
  float d = 0.00091f / sqrtf( dirX * dirX + dirY * dirY );
  dirX *= d; dirY *= d;
  //
  float blurLim = 100 * 10.0f / fullWidth;
  // blur coef for blend phase
  float blur0 = pars._blurriness / blurLim;
  if( blur0 > 1 )
    blur0 = 1;
  // blur coef for blur phase
  float blur1 = pars._blurriness - blurLim;
  if( blur1 < 0 )
    blur1 = 0;
  dirX *= blur1; dirY *= blur1;

  float aspect = (float)fullWidth / (float)fullHeight;
  ComRef<IDirect3DSurface9> zeroDepth;

  // pixel offsets
  float ks = 0.5f;
  float ou = ks / fullWidth;
  float ov = ks / fullHeight;

  ////////////////////////////////////////////////
  // GAUSS BLUR PASSES
  if( blur1 > 0 )
  {
    const int kernelSize = 6;
    float weights[kernelSize][4];
    float suma = 0;
    for( int i = 0; i < kernelSize; i++ )
    {
      weights[i][0] = ComputeGaussianValue( (float)i, 0, 2.8f );
      if( i > 0 )
        suma += weights[i][0] * 2;
      else
        suma += weights[i][0];
    }
    // normalize kernel weights
    float divider = 1.0f/suma; //1.0f/660;
    for( int i = 0; i < kernelSize; i++ )
      weights[i][0] *= divider;
    
    // Set the stream source
    _engine->SetStreamSource( cb, res->_vb, sizeof(VertexP3T2), res->_vertexCount );
    // Set the vertex declaration
    _engine->SetVertexDeclaration(cb, res->_vd);
    // Set the vertex shader
    _engine->SetVertexShader(cb, res->_vsBlur);
    // Set the pixel shader
    _engine->SetPixelShader(cb, res->_psBlur);

    ////////////////////////////////////////////////
    // BLUR PASS 1
    {
      //_engine->SetTemporaryRenderTarget(_rtHorizontal, _rtsHorizontal, zeroDepth);
      _engine->SetTemporaryRenderTarget( _ppGaussianBlur->GetFirstRtTexture(), _ppGaussianBlur->GetFirstRt(), zeroDepth );
      // set viewport
      {
        D3DVIEWPORT9 viewData;
        memset(&viewData,0,sizeof(viewData));
        viewData.X = 0;
        viewData.Y = 0;
        viewData.Width  = partWidth;
        viewData.Height = partHeight;
        viewData.MinZ = 0.0f;
        viewData.MaxZ = 1.0f;
        CALL_D3D_MEMBER(_engine,SetViewport,viewData);
      }

      // Set temporary render states
      SetRenderStateTemp rsCOLORWRITEENABLE(D3DRS_COLORWRITEENABLE, D3DCOLORWRITEENABLE_RED | D3DCOLORWRITEENABLE_GREEN | D3DCOLORWRITEENABLE_BLUE | D3DCOLORWRITEENABLE_ALPHA);
#ifndef _X360SHADERGEN
      // Use SRGB conversion in texture read
      SetSamplerStateTemp ssSRGBTEXTURE(0, D3DSAMP_SRGBTEXTURE, _engine->_caps._rtIsSRGB);
      // Write in the same space as you read // _VBS3_TI
      SetRenderStateTemp rsSRGBSRGBWRITEENABLE(D3DRS_SRGBWRITEENABLE, _engine->_caps._rtIsSRGB);
#endif
      // source texture is second RT of PPGaussianBlur
      SetTextureTemp tTex0( 0, _ppGaussianBlur->GetBlurredTexture() );
      SetSamplerStateTemp ss0MINFILTER(0, D3DSAMP_MINFILTER, D3DTEXF_LINEAR);
      SetSamplerStateTemp ss0MAGFILTER(0, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR);
      SetSamplerStateTemp ss0ADDRESSU(0, D3DSAMP_ADDRESSU, D3DTADDRESS_CLAMP);
      SetSamplerStateTemp ss0ADDRESSV(0, D3DSAMP_ADDRESSV, D3DTADDRESS_CLAMP);

      // set shader params
      SetVertexShaderConstantTemp svc0( 0, D3DXVECTOR4( blur0, 1, 1, 0 ) ); // blurriness, 
      SetVertexShaderConstantTemp svc1( 1, D3DXVECTOR4( dirX, dirY * aspect, ou*GBSizeDivisor, ov*GBSizeDivisor) );
      _engine->SetPixelShaderConstantF( cb, 2, *weights, kernelSize );

      // Drawing
      _engine->DrawPrimitive(cb, D3DPT_TRIANGLESTRIP, 0, 2);

#ifdef _XBOX
      // Resolve content of the render target
      CALL_D3D_MEMBER(_engine,Resolve,D3DRESOLVE_RENDERTARGET0|D3DRESOLVE_ALLFRAGMENTS, NULL,
        _ppGaussianBlur->GetFirstRtTexture(), NULL, 0, 0, NULL, 1.0f, 0L, NULL);
#endif
    }
    //////////////////////////////////////////////
     //PASS 2
    {
      // render to second RT of PPGaussianBlur
      _engine->SetRenderTargetAndDepthStencil( _ppGaussianBlur->GetSecondRt(), _ppGaussianBlur->GetBlurredTexture(), zeroDepth );
      // source texture is first RT of PPGaussianBlur
      SetTextureTemp tTex0( 0, _ppGaussianBlur->GetFirstRtTexture() );
      SetSamplerStateTemp ss0MINFILTER(0, D3DSAMP_MINFILTER, D3DTEXF_LINEAR);
      SetSamplerStateTemp ss0MAGFILTER(0, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR);
      SetSamplerStateTemp ss0ADDRESSU(0, D3DSAMP_ADDRESSU, D3DTADDRESS_CLAMP);
      SetSamplerStateTemp ss0ADDRESSV(0, D3DSAMP_ADDRESSV, D3DTADDRESS_CLAMP);
      // set shader params
      SetVertexShaderConstantTemp svc0( 0, D3DXVECTOR4( blur0, invDiv, invDiv - 0.001f, 0 ) );
      SetVertexShaderConstantTemp svc1( 1, D3DXVECTOR4( -dirY, -dirX * aspect * invDiv, ou * GBSizeDivisor, ov) ); //  * invDiv
      _engine->SetPixelShaderConstantF( cb, 2, *weights, kernelSize );

      // Drawing
      _engine->DrawPrimitive(cb, D3DPT_TRIANGLESTRIP, 0, 2);

#ifdef _XBOX
      // Resolve content of the render target
      CALL_D3D_MEMBER(_engine,Resolve,D3DRESOLVE_RENDERTARGET0|D3DRESOLVE_ALLFRAGMENTS, NULL, _ppGaussianBlur->GetBlurredTexture(), NULL, 0, 0, NULL, 1.0f, 0L, NULL);
#endif
    }		
     // Restore the original render target and depth stencil buffer
    _engine->RestoreOldRenderTarget();
  }
  // FINAL PASS
  // pixel offsets
  ks = 0.34f;
  ou = ks / fullWidth;
  ov = ks / fullHeight;

  // Set the stream source
  _engine->SetStreamSource(cb, res->_vb, sizeof(VertexP3T2), res->_vertexCount);
  // Set the vertex declaration
  _engine->SetVertexDeclaration(cb, res->_vd);
  // Set the vertex shader
  _engine->SetVertexShader(cb, res->_vsFinal);
  // Set the pixel shader
  _engine->SetPixelShader(cb, res->_psFinal);

  // Set temporary render states
  SetRenderStateTemp rsCOLORWRITEENABLE(D3DRS_COLORWRITEENABLE, D3DCOLORWRITEENABLE_RED | D3DCOLORWRITEENABLE_GREEN | D3DCOLORWRITEENABLE_BLUE | D3DCOLORWRITEENABLE_ALPHA);

  // Stage 0 - base texture - do the copy
  IDirect3DTexture9 *rt = _engine->CreateRenderTargetCopyAsTexture();

  SetTextureTemp tTex0( 0, rt );
#ifndef _X360SHADERGEN
  SetSamplerStateTemp ss0SRGBTEXTURE(0, D3DSAMP_SRGBTEXTURE, _engine->_caps._rtIsSRGB);
#endif
  SetSamplerStateTemp ss0MINFILTER(0, D3DSAMP_MINFILTER, D3DTEXF_LINEAR);
  SetSamplerStateTemp ss0MAGFILTER(0, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR);
  SetSamplerStateTemp ss0ADDRESSU(0, D3DSAMP_ADDRESSU, D3DTADDRESS_CLAMP);
  SetSamplerStateTemp ss0ADDRESSV(0, D3DSAMP_ADDRESSV, D3DTADDRESS_CLAMP);

  // if effect is last then switch to back buffer
  if (isLast) _engine->SwitchToBackBufferRenderTarget();
  
  // Stage 1 - blurred texture
  SetTextureTemp tTex1(1, _ppGaussianBlur->GetBlurredTexture());
#ifndef _X360SHADERGEN
  SetSamplerStateTemp ss1SRGBTEXTURE(1, D3DSAMP_SRGBTEXTURE, _engine->_caps._rtIsSRGB);
#endif
  SetSamplerStateTemp ss1MINFILTER(1, D3DSAMP_MINFILTER, D3DTEXF_LINEAR);
  SetSamplerStateTemp ss1MAGFILTER(1, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR);
  SetSamplerStateTemp ss1ADDRESSU(1, D3DSAMP_ADDRESSU, D3DTADDRESS_CLAMP);
  SetSamplerStateTemp ss1ADDRESSV(1, D3DSAMP_ADDRESSV, D3DTADDRESS_CLAMP);
  /*SetVertexShaderConstantTemp svc0( 0, D3DXVECTOR4( blur0, 1, ou, ov ) ); // blurriness, 
  SetVertexShaderConstantTemp svc1( 1, D3DXVECTOR4( dirX * 0.5, dirY * 0.5 * aspect, ou, ov) ); // blurriness,*/
  SetVertexShaderConstantTemp svc0( 0, D3DXVECTOR4( blur0, aspect, ou, ov ) ); // blurriness, 
  SetVertexShaderConstantTemp svc1( 1, D3DXVECTOR4( dirX * 0.5, dirY * 0.5, ou*GBSizeDivisor, ov*GBSizeDivisor ) ); // blurriness,*/

  // Drawing
  _engine->DrawPrimitive( cb, D3DPT_TRIANGLESTRIP, 0, res->_stripSize );
#ifdef _XBOX
  // Resolve content of the render target
  CALL_D3D_MEMBER(_engine,Resolve,D3DRESOLVE_RENDERTARGET0|D3DRESOLVE_ALLFRAGMENTS, NULL, _engine->_currRenderTarget, NULL, 0, 0, NULL, 1.0f, 0L, NULL);
#endif

  // If scene is not being rendered finish initialized scene
  if (!inScene) _engine->D3DEndScene();
  // Restore usual rendering settings
  Finish();
}

LSError EngineDD9::PPDynamicBlur::Serialize(ParamArchive &ar)
{
  CHECK(ar.Serialize("enable", _enabled, 0, false));
  CHECK(ar.Serialize("pInterp",_pInterp,0));
  CHECK(ar.Serialize("prior", _priority, 0, -1));

  return LSOK;
}

//////////////////////////////////////////////////////////////////////////
void EngineDD9::ISpecialEffect::DestroyHndls()
{
  // suppressed assertion: ~Engine than _pp._effects are destroyed before ~ISpecialEffect - int hat case we try to 
  // destroy post effects which are destroyed yet
  if (_engine && _engine->_pp._effects.Size() > 0)
  {
    if (_handlesValid)
    {
      for (int i = 0; i< _hndls.Size(); i++)
      {
        if (_hndls[i] != InvalidPPHndl._hndl) _engine->DestroyPostprocess(_hndls[i]);
      }
    }
    Reset();
  }
  else
  {
    Reset();
  }
}

void EngineDD9::ISpecialEffect::Reset()
{
  _hndls.Clear();
  _handlesValid = false;
  _active = false;
  _timeStamp = Time(0);
  _lastUpdate = Time(0);
}

LSError EngineDD9::ISpecialEffect::Serialize(ParamArchive &ar)
{ 
  CHECK(ar.Serialize("active", _active, 0));
  CHECK(ar.Serialize("valid", _handlesValid, 0));
  CHECK(ar.Serialize("hndls", _hndls, 0));
  CHECK(::Serialize(ar, "timeStamp", _timeStamp, 0, Time(0)));
  CHECK(::Serialize(ar, "lastTime", _lastUpdate, 0, Time(0)));

  return LSOK; 
}

bool EngineDD9::HitSpecEffect::Init()
{
  Assert(!_handlesValid);

  _hndls.Realloc(1);
  _hndls.Resize(1);
  
  _hndls[0] = _engine->CreatePostprocess(PEDynamicBlur, DBPriority);

  _handlesValid = (_hndls[0] != InvalidPPHndl._hndl);

  _timeStamp = Glob.time;
  _active = false;

  return _handlesValid;
}

void EngineDD9::HitSpecEffect::Update()
{
  if (_active && (_timeStamp + 2 < Glob.time) && _hndls.Size() == 1)
  {
    _engine->EnablePostprocess(_hndls[0], false);    
    _active = false;
  }
}

void EngineDD9::HitSpecEffect::SetParams(const float *pars, int nPars)
{
  if (!_handlesValid || _hndls.Size() != 1) return;
  
  const static float fadeOut = 1.25f;

  AutoArray<float> locPars;
  locPars.Realloc(1);    
  locPars.Resize(1);
  locPars[0] = pars[0];

  _engine->EnablePostprocess(_hndls[0], true);
  _engine->SetPostprocessParams(_hndls[0], locPars);
  _engine->CommitPostprocess(_hndls[0], 0);

  locPars[0] = 0;

  _engine->SetPostprocessParams(_hndls[0], locPars);
  _engine->CommitPostprocess(_hndls[0], fadeOut);

  _timeStamp = Glob.time;
  _active = true;
}

bool EngineDD9::MovementSpecEffect::Init()
{
  Assert(!_handlesValid);

  _hndls.Realloc(1);
  _hndls.Resize(1);
  _hndls[0] = _engine->CreatePostprocess(PERadialBlur, RBPriority);

  _pars.Realloc(4);
  _pars.Resize(4);

  _pars[0] = 0; _pars[1] = 0; _pars[2] = 0.25; _pars[3] = 0.25;

  _handlesValid = (_hndls[0] != InvalidPPHndl._hndl);

  _timeStamp = Time(0) - 0.2;
  _active = false;

  return _handlesValid;
}

bool EngineDD9::TiredSpecEffect::Init()
{
  Assert(!_handlesValid);

  _hndls.Realloc(2);
  _hndls.Resize(2);

  // create post effects
  _hndls[0] = _engine->CreatePostprocess(PEColorCorrections, CCPriority); 
  _hndls[1] = _engine->CreatePostprocess(PEWetDistortion, WPriority);

  _handlesValid = (_hndls[0] != InvalidPPHndl._hndl && _hndls[1] != InvalidPPHndl._hndl);
  _timeStamp = Time(0);
  _lastUpdate = Time(0);
  _active = false;

  _pars.Realloc(15);
  _pars.Resize(15);
  // color correction pars
  _pars[0] = 1; _pars[1] = 1; _pars[2] = 0;
  _pars[3] = 0; _pars[4] = 0; _pars[5] = 0; _pars[6] = 0;
  _pars[7] = 0.9; _pars[8] = 0.8; _pars[9] = 0.7; _pars[10] = 0.75;
  _pars[11] = 0.199f; _pars[12] = 0.587f; _pars[13] = 0.144f; _pars[14] = 0;

  _parsCa.Realloc(15);
  _parsCa.Resize(15);
  // wet distortion
  _parsCa[0] = 0.1; _parsCa[1] = 0.15;_parsCa[2] = 0;
  _parsCa[3] = 2.1; _parsCa[4] = 3.7; _parsCa[5] = 2.5; _parsCa[6] = 1.85; 
  _parsCa[7] = 0.0051; _parsCa[8] = 0.0051; _parsCa[9] = 0.0051; _parsCa[10] = 0.0051; 
  _parsCa[11] = 0.5; _parsCa[12] = 0.3; _parsCa[13] = 10; _parsCa[14] = 4;

  return _handlesValid;
}

void EngineDD9::TiredSpecEffect::SetParams(const float *pars, int nPars)
{
  if (!_handlesValid || _hndls.Size() != 2) return;

  static const float tiredLB = 0.25f;
  static const float tiredUB = 1.0f;

  // low bound for applying effect
  if (pars[0] > tiredLB)
  {
    // update pars each 100ms
    if (_timeStamp < Glob.time)
    {
      static const float delay = 0.10;

      _pars[1] = InterpolativC(pars[0], tiredLB, tiredUB, 1, 1.5);   // 1 -> 1.5
      _pars[7] = InterpolativC(pars[0], tiredLB, tiredUB, 1, 0.9);   // 1 -> 0.9
      _pars[8] = InterpolativC(pars[0], tiredLB, tiredUB, 1, 0.8);   // 1 -> 0.8
      _pars[9] = InterpolativC(pars[0], tiredLB, tiredUB, 1, 0.7);   // 1 -> 0.7
      _pars[10] = InterpolativC(pars[0], tiredLB, tiredUB, 1, 0.75); // 1 -> 0.75

      _parsCa[0] = InterpolativC(pars[0], tiredLB, tiredUB, 0, 0.1);
      _parsCa[1] = InterpolativC(pars[0], tiredLB, tiredUB, 0, 0.15);

      _engine->SetPostprocessParams(_hndls[0], _pars);
      _engine->SetPostprocessParams(_hndls[1], _parsCa);

      _engine->CommitPostprocess(_hndls[0], 0);
      _engine->CommitPostprocess(_hndls[1], 0);

      _engine->EnablePostprocess(_hndls[0], true);
      _engine->EnablePostprocess(_hndls[1], true);

      _timeStamp = Glob.time + delay;
      _lastUpdate = Glob.time + 2*delay;

      _active = true;
    }
  }
  else
  {
    if (_active)
    {
      _engine->EnablePostprocess(_hndls[0], false);
      _engine->EnablePostprocess(_hndls[1], false);
      _active = false;
    }
  }
}

void EngineDD9::TiredSpecEffect::Update() 
{
  // when active and not updated then stop PPE
  if (_active && Glob.time > _lastUpdate && _hndls.Size() == 2)
  {
    _engine->EnablePostprocess(_hndls[0], false);
    _engine->EnablePostprocess(_hndls[1], false);
    _active = false;
  }
}

void EngineDD9::MovementSpecEffect::SetParams(const float *pars, int nPars)
{
  if (!_handlesValid || _hndls.Size() != 1) return;

  // update 1 times per 100ms
  if (_timeStamp < Glob.time)
  {
    static const float speedLB = 3.5f;
    static const float delay = 0.1;
    bool valid = (pars[0] > speedLB);

    if (valid)
    {
      //_pars[0] = InterpolativC(pars[0], speedLB, 10, 0, 0.005);
      _pars[0] = InterpolativC(pars[0], speedLB, 10, 0, 0.01);
      _pars[1] = _pars[0];

      _engine->SetPostprocessParams(_hndls[0], _pars);
      _engine->CommitPostprocess(_hndls[0], 0);
      _engine->EnablePostprocess(_hndls[0], true);

      // next update
      _timeStamp = Glob.time + delay;
      _active = true;
    }
    else
    {
      if (_active)
      {
        _pars[0] = 0;
        _pars[1] = 0;

        static const float fadeOut = 0.33f;

        // fade out
        _engine->SetPostprocessParams(_hndls[0], _pars);
        _engine->CommitPostprocess(_hndls[0], fadeOut);
        
        _timeStamp = Glob.time + fadeOut;
        _active = false;
      }
      else
      {
        if (Glob.time < _timeStamp)
        {
          // disable ppe
          _engine->EnablePostprocess(_hndls[0], false);
        }
      }
    }
  }
}

void EngineDD9::MovementSpecEffect::Update() {}

//////////////////////////////////////////////////////////////////////////

// post process enum
#define POSTFX_TYPE_ENUM(type,prefix,XX) \
  XX(type, prefix, RadialBlur) \
  XX(type, prefix, FilmGrain) \
  XX(type, prefix, SSAO) \
  XX(type, prefix, ChromAberration) \
  XX(type, prefix, WetDistortion) \
  XX(type, prefix, ColorCorrections) \
  XX(type, prefix, DynamicBlur) \
  XX(type, prefix, ColorInversion)

#ifndef DECL_ENUM_POSTFX_TYPE
#define DECL_ENUM_POSTFX_TYPE
DECL_ENUM(PostFxType)
#endif
DECLARE_DEFINE_ENUM(PostFxType,PFX,POSTFX_TYPE_ENUM)

#define CHECK_INTERNAL(command) \
  {LSError err = command; if (err != LSOK) return err;}
  
template <>
LSError Serialize(ParamArchive &ar, const RStringB &name, FourFloats &value, int minVersion)
{
  if (ar.GetArVersion() < minVersion) return LSOK;
  if (ar.IsSaving())
  {
    AUTO_STATIC_ARRAY(float, array, 4);
    array.Resize(4);
    array[0] = value[0];
    array[1] = value[1];
    array[2] = value[2];
    array[3] = value[3];
    CHECK_INTERNAL(ar.SerializeArray(name, array, minVersion))
  }
  else
  {
    if (ar.GetPass() != ParamArchive::PassFirst) return LSOK;
    AUTO_STATIC_ARRAY(float, array, 4);
    CHECK_INTERNAL(ar.SerializeArray(name, array, minVersion))
    if (array.Size() < 4)
      return LSNoEntry; // default value
    Assert(array.Size() == 4);
    value[0] = array[0];
    value[1] = array[1];
    value[2] = array[2];
    value[3] = array[3];
  }
  return LSOK;
}

/*!
  Enable/disable post process effect. Type of effect is given by the 1.st parameter.
*/
void EnableSinglePPEffect(PostFxType effectType, bool enable)
{
  switch (effectType)
  {
  case PFXRadialBlur:
    GEngine->EnableRadialBlur(enable);
    break;

  case PFXFilmGrain:
    GEngine->EnableFilmGrain(enable);
    break;

  case PFXSSAO:
    GEngine->EnableSSAO(enable);
    break;

  case PFXChromAberration:
    GEngine->EnableChromAberration(enable);
    break;

  case PFXWetDistortion:
    GEngine->EnableWetDistortion(enable);
    break;

  case PFXColorCorrections:
    GEngine->EnableColorCorrections(enable);
    break;

  case PFXDynamicBlur:
    GEngine->EnableDynamicBlur(enable);
    break;

  case PFXColorInversion:
    GEngine->EnableClrInvers(enable);
    break;
  }
}

/* Create post effect and return handle to effect, if creation failed return -1 */
int CreatePostprocess(PostFxType type, int priority)
{
  int hndl = InvalidPPHndl._hndl;

  switch (type)
  {
  case PFXRadialBlur:
    hndl = GEngine->CreatePostprocess(PERadialBlur, priority);    
    break;

  case PFXFilmGrain:
    hndl = GEngine->CreatePostprocess(PEFilmGrain, priority);
    break;

  case PFXSSAO:
    hndl = GEngine->CreatePostprocess(PESSAO, priority);
    break;

  case PFXChromAberration:
    hndl = GEngine->CreatePostprocess(PEChromAberration, priority);
    break;

  case PFXWetDistortion:
    hndl = GEngine->CreatePostprocess(PEWetDistortion, priority);
    break;

  case PFXColorCorrections:
    hndl = GEngine->CreatePostprocess(PEColorCorrections, priority);
    break;

  case PFXDynamicBlur:
    hndl = GEngine->CreatePostprocess(PEDynamicBlur, priority);
    break;

  case PFXColorInversion:
    hndl = GEngine->CreatePostprocess(PEColorInv, priority);
    break;
  }

  return hndl;
}


int ConvertGameScalarToInt(GameValuePar par)
{
  if (par.GetType() != GameScalar)
  {
    RptF("Invalid post effect parameter type.");
    return InvalidPPHndl._hndl;
  }

  float fHndl = par;
  return toInt(fHndl);
}

int CreatePPEffectHndl(const GameState *state, GameValuePar oper1)
{
  int hndl = InvalidPPHndl._hndl;

  if (oper1.GetType() == GameArray)
  {
    // array of effect
    const GameArrayType &array = oper1;

    if (array.Size() != 2) 
    {
      state->SetError(EvalDim, array.Size(), 2);
      return hndl; 
    }

    // effect type
    RString effectName = array[0];
    PostFxType effectType = GetEnumValue<PostFxType>(cc_cast(effectName));

    if (effectType == INT_MIN) return hndl;

    int priority = ConvertGameScalarToInt(array[1]);

    return CreatePostprocess(effectType, priority);
  }

  return hndl;
}

/* Create array of post effects */
GameValue PPEffectCreateArr(const GameState *state, GameValuePar oper1)
{
  // [hndl1, ..., hndlN] ppEffectCreate [["effect type1", priority1], .. ["effect typeN", priorityN]]
  const GameArrayType &srcArr = oper1;

  GameArrayType destArr;
  destArr.Realloc(srcArr.Size());
  destArr.Resize(srcArr.Size());

  int i = 0;
  for (; i < srcArr.Size(); i++)
  {
    if (srcArr[i].GetType() == GameArray)
    {
      int hndl = CreatePPEffectHndl(state, srcArr[i]);

      // check handle
      if (hndl == InvalidPPHndl._hndl)
      {
        RptF("Post process effect creation failed");
        break;
      }
      destArr[i] = (float)hndl;
    }
  }

  // if not all effects successfully created
  if (i < srcArr.Size())
  {
    // destroy successfully created post processes
    for (int j = 0; j < i; j++)
    {
      int hndl = ConvertGameScalarToInt(destArr[j]);
      GEngine->DestroyPostprocess(hndl);
    }

    destArr.Clear();

    return NOTHING;
  }

  return destArr;
}

/* Create post effect */
GameValue PPEffectCreate(const GameState *state, GameValuePar oper1)
{
  const GameArrayType &srcArr = oper1;

  // ["effect type", priority]
  if (srcArr.Size() == 2 && srcArr[0].GetType() == GameString)
  {
    int hndl = CreatePPEffectHndl(state, oper1);

    if (hndl == InvalidPPHndl._hndl) 
      RptF("Post process effect creation failed");

    return (float)hndl;
  }
  else
  {
    // [["effect type1", priority1], ..., ["effect typeN", priorityN]]
    return PPEffectCreateArr(state, oper1);
  }
}

/* Destroy effect given by hndl */
GameValue PPEffectDestroy(const GameState *state, GameValuePar oper1)
{
  int hndl = ConvertGameScalarToInt(oper1);

  if (hndl == InvalidPPHndl._hndl) 
  {
    RptF("Invalid post effect handle.");
    return NOTHING;
  }

  GEngine->DestroyPostprocess(hndl);

  return NOTHING;
}

/* Destroy array of given hndls */
GameValue PPEffectDestroyArr(const GameState *state, GameValuePar oper1)
{
  GameArrayType srcArr = oper1;

  for (int i = 0; i < srcArr.Size(); i++)
  {
    PPEffectDestroy(state, srcArr[i]);
  }

  return NOTHING;
}

/* Enable / disable post process effect */
GameValue PPEffectEnable1(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{  
  int hndl = ConvertGameScalarToInt(oper1);

  if (hndl == InvalidPPHndl._hndl) 
  {
    RptF("Invalid post effect handle.");
    return NOTHING;
  }

  GEngine->EnablePostprocess(hndl, oper2);

  return NOTHING;
}

int GetPPParsLenght(const GameArrayType &srcArr)
{
  int index = 0;
  for (int i = 0; i < srcArr.Size(); i++)
  {
    if (srcArr[i].GetType() == GameBool || srcArr[i].GetType() == GameScalar) 
    { 
      index++; 
    }
    else if (srcArr[i].GetType() == GameArray)
    {
      const GameArrayType &subArray = srcArr[i];
      for (int j = 0; j < subArray.Size(); j++)
      {        
        // array can contain only bool & float
        if (subArray[j].GetType() == GameBool || subArray[j].GetType() == GameScalar) 
        { 
          index++; 
          continue;
        }

        return 0;
      }
    }
    else
      return 0;
  }

  return index;
}

/* Adjust post effect params */
GameValue PPEffectAdjust1(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
  int hndl = ConvertGameScalarToInt(oper1);

  if (hndl == InvalidPPHndl._hndl) 
  {
    RptF("Invalid post effect handle.");
    return NOTHING;
  }

  const GameArrayType &pars = oper2;
  
  const int maxSize = 32;
  int length = GetPPParsLenght(pars);
  
  if (length == 0 || length > maxSize)
  {
    RptF("Too many post effect params set");
    state->SetError(EvalDim, pars.Size(), maxSize);
    return NOTHING;
  }

  AutoArray<float, MemAllocLocal<float, maxSize> > locPars;
  locPars.Resize(32);
  
  int index = 0;
  for (int i = 0; i < pars.Size(); i++)
  {
    if (pars[i].GetType() == GameScalar)
    {
      locPars[index++] = pars[i];
    }
    else if (pars[i].GetType() == GameBool)
    {
      bool bValue = pars[i];
      locPars[index++] = bValue ? 1.0f : -1.0f;
    }
    else if (pars[i].GetType() == GameArray)
    {
      const GameArrayType &subArray = pars[i];

      for (int j = 0; j < subArray.Size(); j++)
      {
        if (subArray[j].GetType() == GameScalar)
          locPars[index++] = subArray[j];
        else
        {
          state->TypeError(GameScalar, subArray[i].GetType());
          return NOTHING;
        }
      }
    }
    else
    {
      state->TypeError(GameScalar, pars[i].GetType());
      return NOTHING;
    }
  }

  // if return != 0 then number of params is incorrect
  int parsCount = GEngine->SetPostprocessParams(hndl, locPars.Data(), index);
  if (parsCount != 0)
  {
    RptF("Incorrect number of post effect params");
    state->SetError(EvalDim, pars.Size(), parsCount);
  }

  return NOTHING;
}

/* Set time when old pars are replaced by last set, if 0 set immediately */
GameValue PPEffectCommit1(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
  int hndl = ConvertGameScalarToInt(oper1);

  if (hndl == InvalidPPHndl._hndl) 
  {
    RptF("Invalid post effect handle.");
    return NOTHING;
  }

  float time = oper2;
  float comTime = max(0.0f, time);

  GEngine->CommitPostprocess(hndl, comTime);
 
  return NOTHING;
}

/* Overloaded version of PPEffectCommit1 */
GameValue PPEffectCommitArr(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
  // [hndl1, ..., hndlN] ppEffectCommit time
  GameArrayType srcArr = oper1;

  for (int i = 0; i < srcArr.Size(); i++)
  {
    PPEffectCommit1(state, srcArr[i], oper2);
  }

  return NOTHING;
}

// check if the specified post process effect is committed
GameValue PPEffectCommitted1(const GameState *state, GameValuePar oper1)
{
  int hndl = ConvertGameScalarToInt(oper1);

  // hndl ppEffectCommit time
  if (hndl == InvalidPPHndl._hndl) 
  {
    RptF("Invalid post effect handle.");
    return true;
  }

  return GEngine->CommittedPostprocess(hndl);
};

/*!
  Function enable/disable specified post process effect. Function may be called for different oper1 parameters,
  valid parameters are GameString and GameArray.
*/
GameValue PPEffectEnable(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
  // operator type ... valid types are GameString and GameArray
  const GameType &operType = oper1.GetType();

  // enable/disable
  bool enable = oper2;

  // single post process
  if (operType == GameString)
  {
    // effect type
    RString effectName = oper1;
    PostFxType effectType = GetEnumValue<PostFxType>(cc_cast(effectName));

    // is enum valid
    if (effectType == INT_MIN) return NOTHING;

    EnableSinglePPEffect(effectType, enable);

    return NOTHING;
  }

  // enable multiple post processes
  if (operType == GameArray)
  {
    // array of effect
    const GameArrayType &array = oper1;

    for (int i = 0; i < array.Size(); i++)
    {
      // effect specified by string
      if (array[i].GetType() == GameString)
      {
        // effect type
        RString effectName = array[i];
        PostFxType effectType = GetEnumValue<PostFxType>(cc_cast(effectName));

        // unrecognized effect is skip
        if (effectType == INT_MIN) continue;

        EnableSinglePPEffect(effectType, enable);
      }

      // effect specified by handle
      if (array[i].GetType() == GameScalar)
      {
        PPEffectEnable1(state, array[i], oper2);
      }
    }
  }

  return NOTHING;
}

/*!
  Function set parameters for specified post process effect. Effect is specified by first argument. 
  Second argument represent effect parameters. Parameters number depends on effect type.
*/
GameValue PPEffectAdjust( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
  // effect type
  RString effectName = oper1;
  PostFxType effectType = GetEnumValue<PostFxType>(cc_cast(effectName));
  // is enum valid
  if (effectType == INT_MIN) return NOTHING;

  // array of parameters
  const GameArrayType &array = oper2;

  // set params to specified post process, each post process has different parameters count
  switch (effectType)
  {
  case PFXRadialBlur:
    // params count test
    if (array.Size() != 4)
    {
      state->SetError(EvalDim, array.Size(), 4);
      return NOTHING;
    }

    // param types test
    for (int i = 0; i < 4; i++)
    {
      if (array[i].GetType() != GameScalar)
      {
        state->TypeError(GameScalar, array[i].GetType());
        return NOTHING; 
      }
    }

    // PowerX   - float array[0]
    // PowerY   - float array[1]
    // OffsetX  - float array[2]
    // OffsetY  - float array[3]
    GEngine->SetRadialBlurrParams(array[0], array[1], array[2], array[3]);
    break;

  case PFXWetDistortion:
    // params count test
    if (array.Size() != 15)
    {
      state->SetError(EvalDim, array.Size(), 4);
      return NOTHING;
    }

    // params type test
    for (int i = 0; i < 15; i++)
    {
      if (array[i].GetType() != GameScalar)
      {
        state->TypeError(GameScalar, array[i].GetType());
        return NOTHING; 
      }
    }

    // SetBlurriness  - float value
    // SetEffectPower - float top, float bottom
    // SetWaveSpeeds  - float horizontal1, float horizontal2, float vertical1, float vertical2
    // SetWaveAmplitudes  - float horizontal1, float horizontal2, float vertical1, float vertical2
    // SetPhaseCoefs  - float randX, float randY, float posX, float posY
    GEngine->SetWetDistortionParams(array[0], array[1], array[2], array[3], array[4], array[5], array[6],
      array[7], array[8], array[9], array[10], array[11], array[12], array[13], array[14]);

    break;

  case PFXFilmGrain:
    {
      const int FILMGRAIN_PARAMS = 6;

      // takes max five params
      if (array.Size() > FILMGRAIN_PARAMS || array.Size() < 1)
      {
        state->SetError(EvalDim, array.Size(), FILMGRAIN_PARAMS);
        return NOTHING;
      } 

      float params[FILMGRAIN_PARAMS];

      // params type test
      for (int i = 0; i < array.Size() && i < FILMGRAIN_PARAMS - 1; i++)
      {
        if ((array[i].GetType() != GameScalar))
        {
          state->TypeError(GameScalar, array[i].GetType());
          return NOTHING; 
        }

        params[i] = array[i];
      }

      if (array.Size() == FILMGRAIN_PARAMS)
      {
        if (array[FILMGRAIN_PARAMS - 1].GetType() != GameBool)
        {
          state->TypeError(GameBool, array[FILMGRAIN_PARAMS - 1].GetType());
          return NOTHING; 
        }

        params[FILMGRAIN_PARAMS - 1] = array[FILMGRAIN_PARAMS - 1] ? 0.0f : 1.0f;
      }

      // intensity        - float array[0]
      // sharpness        - float array[1]
      // grainsize        - float array[2]
      // intensity at X0  - float array[3]
      // intensity at X1  - float array[4]
      // monochrom        - bool array[5]
      GEngine->SetFilmGrainParams(params, array.Size());
    }
    break;

  case PFXSSAO:
    {
      const int SSAO_PARAMS = 11;

      if (array.Size() > SSAO_PARAMS || array.Size() < 1)
      {
        state->SetError(EvalDim, array.Size(), SSAO_PARAMS);
        return NOTHING;
      } 

      float params[SSAO_PARAMS];

      // params type test
      for (int i = 0; i < array.Size() && i < SSAO_PARAMS- 2; i++)
      {
        if ((array[i].GetType() != GameScalar))
        {
          state->TypeError(GameScalar, array[i].GetType());
          return NOTHING; 
        }

        params[i] = array[i];
      }

      //bool
      if (array.Size() >= 9)
      {
        for (int i = 9; i < array.Size(); i++)
        {
          if (array[i].GetType() != GameBool)
          {
           state->TypeError(GameBool, array[i].GetType());
           return NOTHING; 
          }

          params[i] = array[i] ? 0.0f : 1.0f;
        }
      }

      GEngine->SetSSAOParams(params, array.Size());
    }

    break;

  case PFXChromAberration:
    // takes three params
    if (array.Size() != 3)
    {
      state->SetError(EvalDim, array.Size(), 3);
      return NOTHING;
    } 

    // params type test
    if ((array[0].GetType() != GameScalar) || (array[1].GetType() != GameScalar))
    {
      state->TypeError(GameScalar, array[0].GetType());
      return NOTHING; 
    }

    if (array[2].GetType() != GameBool)
    {
      state->TypeError(GameScalar, array[2].GetType());
      return NOTHING; 
    }

    // aberationPowerX - float array[0]
    // aberationPowerY - float array[1]
    // aspectCorrection - bool array[2]
    GEngine->SetChromaticAberrationParams(array[0], array[1], array[2]);
    break;

  case PFXColorInversion:
    {
      const int reqSize = 3;

      if (array.Size() != reqSize)
      {
        state->SetError(EvalDim, array.Size(), reqSize);
        return NOTHING;
      }

      for (int i = 0; i < reqSize; i++)
      {
        // params type test
        if ((array[i].GetType() != GameScalar))
        {
          state->TypeError(GameScalar, array[i].GetType());
          return NOTHING; 
        }
      }

      Color invColor(array[0], array[1], array[2], 0);
      GEngine->SetClrInversParams(invColor);
    }
    break;

  case PFXDynamicBlur:
    // pars count test
    if (array.Size() != 1)
    {
      state->SetError(EvalDim, array.Size(), 1);
      return NOTHING;
    } 

    // type test 
    if (array[0].GetType() != GameScalar)
    {
      state->TypeError(GameScalar, array[0].GetType());
      return NOTHING; 
    }

    // blurrines - float array[0]
    GEngine->SetDynamicBlurParameters(array[0]);
    break;

  case PFXColorCorrections:
    // three floats and three colors
    if (array.Size() != 6)
    {
      state->SetError(EvalDim, array.Size(), 6);
      return NOTHING;
    }

    // first three param types test
    for (int i = 0; i < 3; i++)
    {
      if (array[i].GetType() != GameScalar)
      {
        state->TypeError(GameScalar, array[i].GetType());
        return NOTHING; 
      }
    }

    // test color parameters, color is set as array of four floats
    for (int i = 3; i < 6; i++)
    {
      if (array[i].GetType() != GameArray)
      {
        state->TypeError(GameArray, array[i].GetType());
        return NOTHING;         
      }

      // array of color components
      const GameArrayType &clrArray = array[i];
      
      // color consist from 4 floats
      if (clrArray.Size() != 4)
      {
        state->SetError(EvalDim, clrArray.Size(), 4);
        return NOTHING;
      }

      for (int j = 0; j < 4; j++)
      {
        if (clrArray[j].GetType() != GameScalar)
        {
          state->TypeError(GameScalar, clrArray[j].GetType());
          return NOTHING; 
        }
      }
    }

    const GameArrayType &tmp = array[3];
    Color blendColor(tmp[0], tmp[1], tmp[2], tmp[3]);

    const GameArrayType &tmp1 = array[4];
    Color colorizedColor(tmp1[0], tmp1[1], tmp1[2], tmp1[3]);

    const GameArrayType &tmp2 = array[5];
    Color weightsColor(tmp2[0], tmp2[1], tmp2[2], tmp2[3]);

    // SetBrightness  - float value
    // SetContrast    - float value
    // SetOffset      - float value
    // SetBlendColor  - Color color ... array of 3 x float
    // SetColorizeColor - Color color ... array of 3 x float
    // SetRGBWeights  - Color weights ... array of 3 x float
    GEngine->SetColorCorrectionsParameters(array[0], array[1], array[2], blendColor, colorizedColor, weightsColor);
    break;
  }

  return NOTHING;
}

/*
  Commit for specified post process effect.
*/
GameValue PPEffectCommit( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
  // effect type
  RString effectName = oper1;
  PostFxType effectType = GetEnumValue<PostFxType>(cc_cast(effectName));

  // is enum valid
  if (effectType == INT_MIN) return NOTHING;

  // positive values
  float time = oper2;
  float comTime = max(0.0f, time);

  switch(effectType)
  {
  case PFXRadialBlur:
      GEngine->CommitRadBlur(comTime);
    break;

  case PFXFilmGrain:
    GEngine->CommitFilmGrain(comTime);
    break;

  case PFXSSAO:
    GEngine->CommitSSAO(comTime);
    break;

  case PFXChromAberration:
    GEngine->CommitChromAberration(comTime);
    break;

  case PFXWetDistortion:
    GEngine->CommitWetDistortion(comTime);
    break;

  case PFXColorCorrections:
    GEngine->CommitColorCorrections(comTime);
    break;

  case PFXDynamicBlur:
    GEngine->CommitDynamicBlur(comTime);
    break;

  case PFXColorInversion:
    GEngine->CommitClrInvers(comTime);
    break;
  }

  return NOTHING;
}

// check if the specified post process effect is committed
GameValue PPEffectCommitted(const GameState *state, GameValuePar oper1)
{
  // effect type
  RString effectName = oper1;
  PostFxType effectType = GetEnumValue<PostFxType>(cc_cast(effectName));

  switch (effectType)
  {
  case PFXRadialBlur:
    return GEngine->CommittedRadBlur();
    break;
  case PFXFilmGrain:
    return GEngine->CommittedFilmGrain();
    break;
  case PFXSSAO:
    return GEngine->CommittedSSAO();
    break;
  case PFXChromAberration:
    return GEngine->CommittedChromAberration();
    break;
  case PFXWetDistortion:
    return GEngine->CommittedWetDistortion();
    break;
  case PFXColorCorrections:
    return GEngine->CommittedColorCorrections();
    break;
  case PFXDynamicBlur:
    return GEngine->CommittedDynamicBlur();
    break;
  case PFXColorInversion:
    return GEngine->CommittedClrInvers();
    break;
  }

  return true;
}

#else
#include <El/Evaluator/express.hpp>
#include <El/Modules/modules.hpp>

#define EMPTY_PP_IMPLEMENTATION {return NOTHING;}

GameValue PPEffectCommitted(const GameState *state, GameValuePar oper1) EMPTY_PP_IMPLEMENTATION
GameValue PPEffectCommitted1(const GameState *state, GameValuePar oper1) EMPTY_PP_IMPLEMENTATION
GameValue PPEffectCreate(const GameState *state, GameValuePar oper1) EMPTY_PP_IMPLEMENTATION
GameValue PPEffectDestroy(const GameState *state, GameValuePar oper1) EMPTY_PP_IMPLEMENTATION
GameValue PPEffectDestroyArr(const GameState *state, GameValuePar oper1) EMPTY_PP_IMPLEMENTATION
GameValue PPEffectEnable(const GameState *state, GameValuePar oper1, GameValuePar oper2) EMPTY_PP_IMPLEMENTATION
GameValue PPEffectEnable1(const GameState *state, GameValuePar oper1, GameValuePar oper2) EMPTY_PP_IMPLEMENTATION
GameValue PPEffectAdjust( const GameState *state, GameValuePar oper1, GameValuePar oper2 ) EMPTY_PP_IMPLEMENTATION
GameValue PPEffectAdjust1(const GameState *state, GameValuePar oper1, GameValuePar oper2) EMPTY_PP_IMPLEMENTATION
GameValue PPEffectCommit( const GameState *state, GameValuePar oper1, GameValuePar oper2 ) EMPTY_PP_IMPLEMENTATION
GameValue PPEffectCommit1(const GameState *state, GameValuePar oper1, GameValuePar oper2) EMPTY_PP_IMPLEMENTATION
GameValue PPEffectCommitArr(const GameState *state, GameValuePar oper1, GameValuePar oper2) EMPTY_PP_IMPLEMENTATION

#endif

const GameType GameScalarOrArray(GameScalar | GameArray);
// helper only for template instantiation
extern GameTypeType GTypeGameScalarOrArray;

template<>
class ConvertTypes<&GTypeGameScalarOrArray> : public ConvertTypes<&GTypeGameAny> {};

#define FUNCTIONS_VISUAL(XX, Category) \
  XX(GameBool, "ppEffectCommitted", PPEffectCommitted, GameString, "effect", "Check whether given post process effect is commited", "", "", "5501", "", Category) \
  XX(GameBool, "ppEffectCommitted", PPEffectCommitted1, GameScalar, "effect", "Check whether given post process effect is commited", "", "", "5501", "", Category) \
  XX(GameScalarOrArray, "ppEffectCreate", PPEffectCreate, GameArray, "effect", "Create post process effect specified by name and priority", "", "", "5501", "", Category) \
  XX(GameNothing, "ppEffectDestroy", PPEffectDestroy, GameScalar, "effect", "Destroy post process effect given by handle", "", "", "5501", "", Category) \
  XX(GameNothing, "ppEffectDestroy", PPEffectDestroyArr, GameArray, "effect", "Create post process effect specified by name and priority", "", "", "5501", "", Category)

FUNCTIONS_VISUAL(JNI_FUNCTION, "Visual")

static const GameFunction PPUnary[]=
{
  FUNCTIONS_VISUAL(REGISTER_FUNCTION, "Visual")
};

#define OPERATORS_VISUAL(XX, Category) \
  XX(GameNothing, "ppEffectEnable", function, PPEffectEnable, GameString, GameBool, "string", "bool", "Enable/disable specified post process effect, is overloaded form multiple effects.", "\"radialBlurr\" ppEffectEnable true, [\"chromAberration\", \"radialBlurr\"] ppEffectEnable true", "", "2.92", "", Category) \
  XX(GameNothing, "ppEffectEnable", function, PPEffectEnable, GameArray, GameBool, "array", "bool", "Enable/disable specified post process effect, is overloaded form multiple effects.", "\"radialBlurr\" ppEffectEnable true, [\"chromAberration\", \"radialBlurr\"] ppEffectEnable true", "", "2.92", "", Category) \
  XX(GameNothing, "ppEffectEnable", function, PPEffectEnable1, GameScalar, GameBool, "number", "bool", "Enable / disable post process effect", "hndl ppEffectEnable true", "", "2.92", "", Category) \
  XX(GameNothing, "ppEffectAdjust", function, PPEffectAdjust, GameString, GameArray, "string", "array", "Adjust parameters for specified post process effect", "\"radialBlurr\" ppEffectEnable []", "", "2.92", "", Category) \
  XX(GameNothing, "ppEffectAdjust", function, PPEffectAdjust1, GameScalar, GameArray, "number", "array", "Set post process effect parameters", "hndl ppEffectAdjust [0.0075, 0.0075, 0.1, 0.1]", "", "2.92", "", Category) \
  XX(GameNothing, "ppEffectCommit", function, PPEffectCommit, GameString, GameScalar, "string", "number", "Set time when old pars are replaced by last set, if 0 set immediately", "...", "", "2.92", "", Category) \
  XX(GameNothing, "ppEffectCommit", function, PPEffectCommit1, GameScalar, GameScalar, "number", "number", "Set time when old pars are replaced by last set, if 0 set immediately", "hndl ppEffectCommit 0", "", "2.92", "", Category) \
  XX(GameNothing, "ppEffectCommit", function, PPEffectCommitArr, GameArray, GameScalar, "array", "scalar", "Set time when old pars are replaced by last set, if 0 set immediately", "hndl ppEffectCommit 0", "", "2.92", "", Category)

OPERATORS_VISUAL(JNI_OPERATOR, "Visual")

static const GameOperator PPBinary[]=
{
  OPERATORS_VISUAL(REGISTER_OPERATOR, "Visual")
};

#if DOCUMENT_COMREF
static ComRefFunc PPComRefFunc[] =
{
  FUNCTIONS_VISUAL(COMREF_FUNCTION, "Visual")
  OPERATORS_VISUAL(COMREF_OPERATOR, "Visual")
};
#endif

INIT_MODULE(PostProcess, 2)
{
  GGameState.NewFunctions(PPUnary, lenof(PPUnary));
  GGameState.NewOperators(PPBinary, lenof(PPBinary));
#if DOCUMENT_COMREF
  for (int i=0; i<lenof(PPComRefFunc); i++)
  {
    GGameState.AddComRefFunction(PPComRefFunc[i]);
  }
#endif
};
