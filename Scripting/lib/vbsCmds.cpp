#include "wpch.hpp"

#if _VBS2

/*
\VBS_patch 1.19 Date [3/27/2008] by Mark
- Added: configClasses command
*/
/*
\VBS_patch 5631 Date [5/7/2008] by Mark
- Changed:  SetWeaponDirection uses degrees and smooth transition is default
- Added : firingSolution
*/

#include "hla/Tracking.hpp"
#include "gameStateExt.hpp"
#include "landscape.hpp"
#include "soldierOld.hpp"
#include "shots.hpp"
#include "camera.hpp"
#include "cameraHold.hpp"
#include "engine.hpp"
#include "arcadeTemplate.hpp"
#include "detector.hpp"
#include "helicopter.hpp"
#include "autoComplete.hpp"
#include "stringtableExt.hpp"
#include "UI/resincl.hpp"
#include "UI/chat.hpp"
#include "UI/uiControls.hpp"
#include "UI/dispMissionEditor.hpp"
#include "UI/uiMap.hpp"
#include "hla/AAR.hpp"
#include <Es/Common/win.h>
#include "morale.hpp"
#include <El/Evaluator/express.hpp>

#if _VBS3 // switchAllLights
#include "objectClasses.hpp"
#endif

#if _HLA
  #include"hla/HLA_base.hpp"
#else
  #include "Network/network.hpp"
#endif

const GameType GameObjectOrArrayOrGroup(GameObject|GameArray|GameGroup);
const GameType GameObjectOrArray(GameObject|GameArray);

#define OBJECT_NULL GameValueExt((Object *)NULL)

Object *GetObject( GameValuePar oper );
static AIGroup *GetGroup( GameValuePar oper )
{
  if (oper.GetNil()) return NULL;
  if( oper.GetType()==GameGroup )
  {
    return static_cast<GameDataGroup *>(oper.GetData())->GetGroup();
  }
  if( oper.GetType()==GameObject )
  {
    Object *obj = static_cast<GameDataObject *>(oper.GetData())->GetObject();
    EntityAI *vai = dyn_cast<EntityAI>(obj);
    if (!vai) return NULL;
    AIBrain *unit = vai->CommanderUnit();
    if (!unit) return NULL;
    return unit->GetGroup();
  }
  return NULL;
}

bool RStringCompI(const RString &first,const RString &second)
{
  return strcmpi(first.Data(),second.Data()) == 0 ? true : false;
}


bool CheckSize(const GameState *state, GameArrayType array, int size);
bool CheckType(const GameState *state, GameValuePar oper, const GameType &type);

bool GetPos(Vector3 &ret, GameValuePar oper);
bool GetPos( const GameState *state, Vector3 &ret, GameValuePar oper );

static bool GetRelPos( const GameState *state, Vector3 &ret, GameValuePar oper )
{
  Object *obj = GetObject(oper);
  if (obj)
  {   
    ret = obj->Position();
    // convert to relative position?
    ret[1] -= GLandscape->SurfaceYAboveWater(ret.X(),ret.Z());
    return true;
  }
  const GameArrayType &array = oper;
  if (array.Size() < 2 || array.Size() > 3)
  {
    if (state) state->SetError(EvalDim,array.Size(),3);
    return false;
  }
  if (array[0].GetType() != GameScalar)
  {
    if (state) state->TypeError(GameScalar,array[0].GetType());
    return false;
  }
  if (array[1].GetType() != GameScalar
  )
  {
    if (state) state->TypeError(GameScalar,array[1].GetType());
    return false;
  }

  float x = array[0];
  float z = array[1];
  float y = 0;
  if (array.Size()==3)
  {
    if (array[2].GetType() != GameScalar)
    {
      if (state) state->TypeError(GameScalar,array[2].GetType());
      return false;
    }
    y=array[2];
  }
  ret = Vector3(x,y,z);
  return true;
}

GameValue IsAVRS(const GameState *state)
{
#if _VBS3_AVRS
  return true;
#else
  return false;
#endif
}

GameValue IsMultiplayer(const GameState *state)
{
  return GWorld->GetMode() == GModeNetware;
}

GameValue IsLasershot(const GameState *state)
{
#if _LASERSHOT
  return true;
#else
  return false;
#endif
}

GameValue UnLockMap(const GameState *state)
{
  //GWorld->UnLockMap();

  return NOTHING;
}

GameValue GetMapScale(const GameState *state)
{
/*  DisplayMap *map = dynamic_cast<DisplayMap *>(GWorld->Map());

  if (map)
  {
    return map->GetMap()->GetScale();
  }
*/
  return NOTHING;
}

static GameConfigType GetConfig(GameValuePar oper)
{
  Assert(oper.GetType() == GameConfig);
  return static_cast<GameDataConfig *>(oper.GetData())->GetConfig();
}

GameValue ConfigClassDelete( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
  GameConfigType &parent = GetConfig(oper1);
  GameConfigType result;
  if (!parent.entry) return NOTHING;

  if (oper2.GetType() != GameString)
  {
    state->TypeError(GameString, oper2.GetType());
    return NOTHING;
  }

  ParamClassPtr cls = parent.entry.GetClass();
  if (!cls) return NOTHING;
  
  RString name = oper2;
  cls->Delete(name);

  return NOTHING;
}

// Functor to get a list of all the possible turret paths, takes two dimential array
// so the format is [[turretpath],[turretpath],....]
class GetAllTurrets : public ITurretFunc
{
protected:
  AutoArray< AutoArray<int> > &_turretPaths;
  Transport *_tran;

public:
  GetAllTurrets(AutoArray< AutoArray<int> > &turretPaths, Transport *tran ):_turretPaths(turretPaths),_tran(tran)
  {
  }
  bool operator () (EntityAIFull *entity, TurretContext &context)
  {
    if(!context._turret) return false;

    int index = _turretPaths.Add();
    _tran->GetTurretPath(_turretPaths[index],context._turret);

    return false; // continue
  }
};


GameValue ListAllTurrets(const GameState *state, GameValuePar oper1)
{
  if(!CheckType(state,oper1,GameObject))
    return NOTHING;

  Object *obj      = GetObject(oper1);
  Transport *tran = dyn_cast<Transport>(obj);

  // Get all turrets
  AutoArray< AutoArray<int> > turretPaths;
  if(tran)
  {
    GetAllTurrets turrets(turretPaths,tran);
    tran->ForEachTurret(turrets);
  }

  // parse and return as all the possible turrets
  GameValue value = state->CreateGameValue(GameArray);
  GameArrayType &array = value;
  for( int x = 0; x < turretPaths.Size(); ++x)
  {
    GameValue tPath = state->CreateGameValue(GameArray);
    GameArrayType &tPathArray = tPath;
    for( int y = 0; y < turretPaths[x].Size(); ++y)
      tPathArray.Add((float)turretPaths[x][y]);// convert to float

    array.Add(tPath);
  }

  return array;
}

GameValue GetWeaponCargo(const GameState *state, GameValuePar oper1)
{
  if(!CheckType(state,oper1,GameObject))
    return NOTHING;

  Object *obj = GetObject(oper1);
  GameValue value = state->CreateGameValue(GameArray);
  GameArrayType &array = value;
  
  EntityAI *entity = dyn_cast<EntityAI>(obj);
  AutoArray<RString> weaponNames;

  if(entity)
  {
    for( int i = 0; i < entity->GetWeaponCargoSize(); ++i )
    {
      const WeaponType *wType = entity->GetWeaponCargo(i);
      if(wType)
        weaponNames.Add(wType->GetName());
    }
  }

  // time to loop through and remove duplicants
  for( int i = 0; i < weaponNames.Size(); ++i)
  {
    int count = 1;// there is allways one!
    for( int x = i + 1; x < weaponNames.Size(); ++x)
    {
      if(RStringCompI(weaponNames[i],weaponNames[x]))
      {
        count +=1;
        weaponNames.Delete(x);
        x--;
      }
    }
    
    GameValue value = state->CreateGameValue(GameArray);
    GameArrayType &element = value;
    element.Add(weaponNames[i]);
    element.Add((float)count);

    array.Add(value);
  }

  return array;
}

struct MagPointer
{
  Magazine* ptr;
};
TypeIsBinaryZeroed(MagPointer);

GameValue ObjAddMagazineEx( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
  if(!CheckType(state,oper1, GameObject)) return NOTHING;
  if(!CheckType(state,oper2, GameArray)) return NOTHING;

  EntityAIFull *veh = dyn_cast<EntityAIFull>(GetObject(oper1));
  if (!veh) return NOTHING;

  const GameArrayType &array = oper2;
  
  if(!CheckSize(state,array,2)) return NOTHING;
  if(!CheckType(state,array[0], GameString)) return NOTHING;
  if(!CheckType(state,array[1], GameScalar)) return NOTHING;

  TurretContext context;
  if (veh->GetPrimaryGunnerTurret(context))
  {
    GameStringType magazine = array[0];
    int ammoCount = toInt(array[1]);

    int index = context._weapons->AddMagazine(veh, magazine);
    if(index != -1)
    {
      Magazine *mag = context._weapons->_magazines[index];
      mag->SetAmmo(ammoCount);
    }

    veh->AutoReloadAll();
  }

  return NOTHING;
}

GameValue ObjAddMagazineCargoEx( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
  Object *obj = GetObject(oper1);
  if (!obj) return NOTHING;
  if (obj->IsDamageDestroyed()) return NOTHING;
  EntityAI *veh = dyn_cast<EntityAI>(obj);
  if (!veh) return NOTHING;

  const GameArrayType &array = oper2;
  if (array.Size() != 2) return NOTHING;
  if
    (
    array[0].GetType() != GameString ||
    array[1].GetType() != GameScalar
    ) return NOTHING;

  RString name = array[0];
  Ref<MagazineType> magazineType = MagazineTypes.New(name);
  if (!magazineType) return NOTHING;
  int count = toInt((float)array[1]);

  Ref<Magazine> magazine = new Magazine(magazineType);
  magazine->SetAmmo(count);
  magazine->_reload = 0;
  magazine->_reloadDuration = 1;
  magazine->_reloadMagazine = 0;

  veh->AddMagazineCargo(magazine,true);

  return NOTHING;
}

GameValue GetMagazinesEx(const GameState *state, GameValuePar oper1)
{
  if(!CheckType(state,oper1,GameObject))
    return NOTHING;

  Object *obj = GetObject(oper1);
  GameValue value = state->CreateGameValue(GameArray);
  GameArrayType &array = value;

  EntityAI *entity = dyn_cast<EntityAI>(obj);
  AutoArray< AutoArray<MagPointer> > magazines;

  if(dyn_cast<Person>(entity))
  {
    TurretContext context;
    if (entity->GetPrimaryGunnerTurret(context))
    {
      for (int i=0; i<context._weapons->_magazines.Size(); i++)
      {
        const Magazine *mag = context._weapons->_magazines[i].GetRef();
        if(mag && mag->_type)
        {
          bool found = false;
          for( int x = 0; x < magazines.Size();++x)
          {
            if(magazines[x].Size()>0)
              if(RStringCompI(magazines[x][0].ptr->_type->GetName(),mag->_type->GetName()))
              {
                found = true;
                int index = magazines[x].Add();
                magazines[x][index].ptr = (Magazine*) mag;
              }
          }

          if(!found)// new magazine
          {
            int indexArray = magazines.Add();
            int indexMag = magazines[indexArray].Add();
            magazines[indexArray][indexMag].ptr = (Magazine*) mag;
          }
        }
      }
    }    
  }

  // time to loop through and remove duplicants
  for( int i = 0; i < magazines.Size(); ++i)
  {
    if(magazines[i].Size()==1 &&
      (magazines[i][0].ptr->GetAmmo()==0))
      continue;

    GameValue holderVal = state->CreateGameValue(GameArray);
    GameArrayType &holderArray = holderVal;

    GameValue ammoCountVal = state->CreateGameValue(GameArray);
    GameArrayType &ammoArray = ammoCountVal;

    for( int x = 0; x < magazines[i].Size(); ++x )
    {
      if(magazines[i][x].ptr->GetAmmo() != 0)
      ammoArray.Add((float)magazines[i][x].ptr->GetAmmo());
    }

    holderArray.Add(magazines[i][0].ptr->_type->GetName());
    holderArray.Add(ammoCountVal);

    array.Add(holderArray);
  }

  return array;
}

GameValue GetMagazinesCargo(const GameState *state, GameValuePar oper1)
{
  if(!CheckType(state,oper1,GameObject))
    return NOTHING;

  Object *obj = GetObject(oper1);
  GameValue value = state->CreateGameValue(GameArray);
  GameArrayType &array = value;

  EntityAI *entity = dyn_cast<EntityAI>(obj);
  AutoArray< AutoArray<MagPointer> > magazines;

  if(entity)
  {
    for( int i = 0; i < entity->GetMagazineCargoSize(); ++i)
    {
      const Magazine *mag = entity->GetMagazineCargo(i);
      if(mag && mag->_type)
      {
        bool found = false;
        for( int x = 0; x < magazines.Size();++x)
        {
          if(magazines[x].Size()>0)
            if(RStringCompI(magazines[x][0].ptr->_type->GetName(),mag->_type->GetName()))
            {
              found = true;
              int index = magazines[x].Add();
              magazines[x][index].ptr = (Magazine*) mag;
            }
        }
        
        if(!found)// new magazine
        {
          int indexArray = magazines.Add();
          int indexMag = magazines[indexArray].Add();
          magazines[indexArray][indexMag].ptr = (Magazine*) mag;
        }
      }
    }
  }

  // time to loop through and remove duplicants
  for( int i = 0; i < magazines.Size(); ++i)
  {
    GameValue holderVal = state->CreateGameValue(GameArray);
    GameArrayType &holderArray = holderVal;

    GameValue ammoCountVal = state->CreateGameValue(GameArray);
    GameArrayType &ammoArray = ammoCountVal;

    for( int x = 0; x < magazines[i].Size(); ++x )
      ammoArray.Add((float)magazines[i][x].ptr->GetAmmo());

    holderArray.Add(magazines[i][0].ptr->_type->GetName());
    holderArray.Add(ammoCountVal);

    array.Add(holderArray);
  }

  return array;
}

GameValue ObjGetPistolWeapon( const GameState *state, GameValuePar oper1 )
{
  Object *obj=GetObject(oper1);
  if( !obj ) return "";
  EntityAIFull *ai=dyn_cast<EntityAIFull>(obj);
  if( !ai ) return "";

  // TODO: which turret?
  TurretContext context;
  if (ai->GetPrimaryGunnerTurret(context))
  {
    for (int i=0; i<context._weapons->_weapons.Size(); i++)
    {
      const WeaponType *weapon = context._weapons->_weapons[i];
      if (!weapon) continue;
      if((weapon->_weaponType & MaskSlotHandGun) != 0)
        return weapon->GetName();
    }
  }

  return "";
}

GameValue RemoveWeaponCargo(const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
  // Check input parameters
  if(!CheckType(state,oper1,GameObject))
    return NOTHING;
  if(!CheckType(state,oper2,GameString))
    return NOTHING;

  Object *obj = GetObject(oper1);
  RString weaponClass = oper2;

  EntityAI *entity = dyn_cast<EntityAI>(obj);
  if(entity)
  {
    for( int i = 0; i < entity->GetWeaponCargoSize(); ++i )
    {
      const WeaponType *wType = entity->GetWeaponCargo(i);
      if(RStringCompI(wType->GetName(),weaponClass))
      {
        if (GWorld->GetMode() == GModeNetware)
          GetNetworkManager().RemoveWeaponCargo(entity,wType->GetName());

        entity->RemoveWeaponCargo(wType);
        break;
      }
    }
  }

  return NOTHING;
}

GameValue RemoveMagazineCargo(const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
  if(!CheckType(state,oper1,GameObject))
    return NOTHING;

  Object *obj = GetObject(oper1);
  const GameArrayType &array = oper2;
  RString magName;
  int     ammoCount = -1;

  if(oper2.GetType() == GameArray)
  {
    if(array.Size() != 2) return NOTHING;
    if(!CheckType(state,array[0],GameString)) return NOTHING;
    if(!CheckType(state,array[1],GameScalar)) return NOTHING;
    magName   = array[0];
    ammoCount = toInt((float)array[1]);
  }
  else if(oper2.GetType() == GameString)
    magName = oper2;
  else
    return NOTHING;
  
  EntityAI *entity = dyn_cast<EntityAI>(obj);
  if(entity)
  {
    for( int i = 0; i < entity->GetMagazineCargoSize(); ++i )
    {
      const Magazine *mag = entity->GetMagazineCargo(i);
      if(mag && mag->_type && RStringCompI(mag->_type->GetName(),magName))
      {
        if(ammoCount==-1)
        {
          if (GWorld->GetMode() == GModeNetware)
            GetNetworkManager().RemoveMagazineCargo(entity, mag->_type->GetName(), mag->GetAmmo());

          entity->RemoveMagazineCargo((Magazine*)mag);
          break;
        }
        else
        {
          if (GWorld->GetMode() == GModeNetware)
            GetNetworkManager().RemoveMagazineCargo(entity, mag->_type->GetName(), mag->GetAmmo());


          entity->RemoveMagazineCargo(magName,ammoCount);
          break;
        }       
      }
    }
  }

  return NOTHING;
}


GameValue ObjDisableGeo(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
  Object *obj = GetObject(oper1);
  const GameArrayType &array = oper2;

  if(array.Size() < 1) return NOTHING;
  int flags = 0;

  for(int i = 0; i< array.Size(); ++i)
    flags |= int(array[i]) << i;

  obj->_inVisibilityFlags = flags;

  return NOTHING;
}

GameValue ObjIsDisableGeo(const GameState *state, GameValuePar oper1)
{
  Object *obj = GetObject(oper1);
  if (!obj) return false;
  return obj->_inVisibilityFlags > 0;
}

GameValue ObjGetSelectionCenter(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
  GameValue value = state->CreateGameValue(GameArray);
  GameArrayType &array = value;
  array.Resize(3);
  array[0] = 0.0f; array[1] = 0.0f; array[2] = 0.0f;

  Object *obj = GetObject(oper1);
  RString name = oper2;

  if (!obj) return value;
  const LODShape *lodShape = obj->GetShape();
  if (!lodShape) return value;

  //uses only fire geo!
  int level = lodShape->FindFireGeometryLevel();
  ShapeUsed shapeLevel = lodShape->Level(level);

  AnimationContextStorageGeom storage;
  AnimationContext animContext(storage);
  shapeLevel->InitAnimationContext(animContext, lodShape->GetConvexComponents(level), false);
  obj->Animate(animContext, level, true, obj, -FLT_MAX);
  
  const Shape *fire = lodShape->FireGeometryLevel();

  int selIndex = lodShape->FindNamedSel(fire, name);
  if(selIndex <0) return value;
  const NamedSelection &sel = fire->NamedSel(selIndex);
  
  Vector3 minMax[2]; minMax[0] = minMax[1] = VZero;
  Vector3 center= VZero;

  if(sel.Size()>0)
  {
    minMax[0] = minMax[1] = fire->Pos(sel[0]);
  }
  for( int k = 1; k < sel.Size(); k++ )
  {
    SaturateMin(minMax[0],fire->Pos(sel[k]));
    SaturateMax(minMax[1],fire->Pos(sel[k]));
  }

  // Calculate the center
  center = minMax[0] + ((minMax[1] - minMax[0])/2.0f);
  
  array[0] = center.X(); array[1] = center.Z(); array[2] = center.Y();

  return value;
  
}

// MagazineTurret [obj,[turret path]]
GameValue GetMagazineTurret(const GameState *state, GameValuePar oper1)
{
  if(!CheckType(state,oper1,GameArray))
    return NOTHING;

  // Grab the array
  const GameArrayType &inputArray = oper1;
  if(!CheckSize(state, inputArray, 2)) return NOTHING;
  if(!CheckType(state, inputArray[0], GameObject)) return NOTHING;
  if(!CheckType(state, inputArray[1], GameArray )) return NOTHING;

  Object     *obj = GetObject(inputArray[0]);
  Transport *tran = dyn_cast<Transport>(obj);
  if(!tran) return NOTHING;

  const GameArrayType &turretPath = inputArray[1];
  int n = turretPath.Size();
  for (int i=0; i<n; i++)
    if (!CheckType(state, turretPath[i], GameScalar)) return NOTHING;

  // Get the path
  TurretPath path;
  path.Resize(n);
  for (int i=0; i<n; i++) path[i] = toInt((float)turretPath[i]);
  Turret *turret = tran->GetTurret(path.Data(), path.Size());
  if (!turret) return NOTHING;

  // Everthing validated, time to get all the magazine these thing has!
  TurretContext context;
  tran->FindTurret(turret,context);

  // Return array
  GameValue value = state->CreateGameValue(GameArray);
  GameArrayType &array = value;

  // Temp holder
  AutoArray< AutoArray<MagPointer> > magazines;

  for( int i = 0; i < context._weapons->_magazines.Size(); ++i)
  {
    const Magazine *mag = context._weapons->_magazines[i];
    if(mag && mag->_type)
    {
      bool found = false;
      for( int x = 0; x < magazines.Size();++x)
      {
        if(magazines[x].Size()>0)
          if(RStringCompI(magazines[x][0].ptr->_type->GetName(),mag->_type->GetName()))
          {
            found = true;
            int index = magazines[x].Add();
            magazines[x][index].ptr = (Magazine*) mag;
          }
      }

      if(!found)// new magazine
      {
        int indexArray = magazines.Add();
        int indexMag = magazines[indexArray].Add();
        magazines[indexArray][indexMag].ptr = (Magazine*) mag;
      }
    }
  }

  // Time to loop through and remove duplicants
  for( int i = 0; i < magazines.Size(); ++i)
  {
    GameValue holderVal = state->CreateGameValue(GameArray);
    GameArrayType &holderArray = holderVal;

    GameValue ammoCountVal = state->CreateGameValue(GameArray);
    GameArrayType &ammoArray = ammoCountVal;

    for( int x = 0; x < magazines[i].Size(); ++x )
      ammoArray.Add((float)magazines[i][x].ptr->GetAmmo());

    holderArray.Add(magazines[i][0].ptr->_type->GetName());
    holderArray.Add(ammoCountVal);

    array.Add(holderArray);
  }

  return array;   
}

#include "plugins.hpp"

GameValue PluginFunction(const GameState *state, GameValuePar oper1)
{
#ifdef _PLUGINSYSTEM
  // If source is not an array, finish
  if(!CheckType(state,oper1,GameArray)) return NOTHING;

  // Grab the array, note we care of type of the first item only (the plugin name), the second one will be converted to string in any case
  const GameArrayType &inputArray = oper1;
  if(!CheckSize(state, inputArray, 2)) return NOTHING;
  if(!CheckType(state, inputArray[0], GameString)) return NOTHING;

  // Get the name of the string
  RString pluginNameSource = inputArray[0];
  pluginNameSource.Lower();
  RStringB pluginName = pluginNameSource;

  // Convert second parameter to the string
  RString parameter = CheckType(state, inputArray[1], GameString) ? inputArray[1] : inputArray[1].GetText();

  // Locate the corresponding plugin and call the plugin function
  GameValue retValue = NOTHING;
  const char *pluginFunctionResult = NULL;
  for (int i = 0; i < Plugins.Size(); i++)
  {
    if (Plugins[i].NameMatches(pluginName))
    {
      // Execute the plugin function
      pluginFunctionResult = Plugins[i].PluginFunction(cc_cast(parameter));

      // If some value was returned, construct an array out of it (by evaluating the text) and finish
      if (pluginFunctionResult != NULL)
      {
        Plugins[i].BeginContext();
        retValue = state->Evaluate(pluginFunctionResult, GameState::EvalContext::_default, GWorld->GetMissionNamespace());
        if(!CheckType(state, retValue, GameArray)) retValue = NOTHING;
        Plugins[i].EndContext();
      }
      break;
    }
  }

  // Write the return value
  return retValue;
#else
  return NOTHING;
#endif
}

// car RemoveMagazineTurret ["ammo",turretPath]
GameValue RemoveMagazineTurret(const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
  Object *obj = GetObject(oper1);
  if (!obj) return NOTHING;

  Transport *veh = dyn_cast<Transport>(obj);
  if (!veh) return NOTHING;

  // Grab the array
  const GameArrayType &array = oper2;
  if(array.Size() < 2 || array.Size() > 3) return NOTHING;

  // Get the magazine name
  if(!CheckType(state,array[0], GameString)) return NOTHING;
  RString magazineName = array[0];

  // Get the weapon path
  if (!CheckType(state, array[1], GameArray)) return NOTHING;
  const GameArrayType &subarray = array[1];
  int n = subarray.Size();
  for (int i=0; i<n; i++)
    if (!CheckType(state, subarray[i], GameScalar)) return NOTHING;

  // check for extra parameter
  int ammoAmount = -1;
  if(array.Size() == 3) ammoAmount = toInt((float) array[2]);


  // get the path
  TurretPath path;
  path.Resize(n);
  for (int i=0; i<n; i++) path[i] = toInt((float)subarray[i]);
  Turret *turret = veh->GetTurret(path.Data(), path.Size());
  if (!turret) return NOTHING;

  TurretContext context;
  veh->FindTurret(turret,context);

  for(int i = 0; i < context._weapons->_magazines.Size(); ++i)
  {
    Ref<Magazine> magazine = context._weapons->_magazines[i];

    if (stricmp(magazine->_type->GetName(), magazineName) == 0) 
    {
      if(ammoAmount == -1)
      {
        context._weapons->RemoveMagazine(veh,magazine.GetRef());
        break;
      }
      else if(magazine->GetAmmo() == ammoAmount)
      {
        context._weapons->RemoveMagazine(veh,magazine.GetRef());
        break;
      }
    }
  }

  return NOTHING;
}

GameValue DisplayObjectText(const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
  if(!CheckType(state,oper1, GameObject))
    return NOTHING;

  if(!CheckType(state,oper2, GameArray))
    return NOTHING;

  Object *obj = GetObject(oper1);
  if(!obj) return NOTHING;

  const GameArrayType &array = oper2;
  if(array.Size() < 1) return NOTHING;
  

  if(!CheckType(state,array[0],GameString)) return NOTHING;
 
  //default values
  Vector3 relPosOffset(0,obj->GetShape() ? obj->GetShape()->Max().Y() : 0, 0);
  if(array.Size() > 1)
  {
    if(!CheckType(state,array[1],GameArray))  return NOTHING;
    const GameArrayType &relPosition = array[1];
    if(relPosition.Size() != 3) return NOTHING;
    for( int i = 0; i < relPosition.Size(); ++i )
      if(!CheckType(state,relPosition[i],GameScalar)) return NOTHING;

    relPosOffset = Vector3(relPosition[0],relPosition[2],relPosition[1]);
  }

  bool text3d = false;
  if(array.Size() > 2)
  {
    if(!CheckType(state,array[2],GameBool))   return NOTHING;
    text3d = array[2];
  }

  PackedColor colorText(255,255,255,255);
  if(array.Size() > 3)
  {
    if(!CheckType(state,array[3],GameArray))  return NOTHING;
    const GameArrayType &colorGT = array[3];
    if(colorGT.Size() != 4) return NOTHING;
    for( int i = 0; i < colorGT.Size(); ++i )
      if(!CheckType(state,colorGT[i],GameScalar)) return NOTHING;

    colorText = PackedColor(ColorP(colorGT[0],colorGT[1],colorGT[2],colorGT[3]));
  }

  float size = text3d ? 0.2 : 0.02;
  if(array.Size() > 4)
  {
    if(!CheckType(state,array[4],GameScalar)) return NOTHING;
    size = array[4];
  }
  
  DisplayText &objText = obj->GetDisplayText();
  
  objText.text = array[0];
  objText.comPosOffset = relPosOffset;
  objText.textSize = size;
  objText.color = colorText;
  objText.render3DText = text3d;

  return NOTHING;
}

GameValue ConfigClassAdd( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
  GameConfigType &parent = GetConfig(oper1);
  GameConfigType result;
  if (!parent.entry) return GameValueExt(result);

  if (oper2.GetType() != GameString)
  {
    state->TypeError(GameString, oper2.GetType());
    return GameValueExt(result);
  }

  ParamClassPtr cls = parent.entry.GetClass();
  if (!cls) return GameValueExt(result);
  
  RString name = oper2;
  ParamClassPtr newCls = cls->AddClass(name);

  result.entry = parent.entry.GetClass()->FindEntry(name);
  int n = parent.path.Size();
  result.path.Realloc(n + 1);
  result.path.Resize(n + 1);
  for (int i=0; i<n; i++) result.path[i] = parent.path[i];
  result.path[n] = result.entry->GetName();
  return GameValueExt(result);
}

void GameValueToEntry(ParamClass &cls, RString name, GameValuePar value);

GameValue ConfigValueAdd( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
  GameConfigType &parent = GetConfig(oper1);
  if (!parent.entry) return NOTHING;

  const GameArrayType &array = oper2;
  if (array.Size() != 2) return NOTHING;

  if (array[0].GetType() != GameString)
  {
    state->TypeError(GameString, array[0].GetType());
    return NOTHING;
  }

  ParamClassPtr cls = parent.entry.GetClass();
  if (!cls) return NOTHING;
  
  RString name = array[0];
  GameValueToEntry(*cls, name, array[1]);
  return NOTHING;
}

// operators
GameValue GetLOS( const GameState *state, GameValuePar oper1, GameValuePar op2 )
{
  Object *obj1=NULL;
  Object *obj2=NULL;

  Vector3 pos1;
  Vector3 pos2;

  float radius = 1;

  GameValue oper2 = op2;
  if (oper2.GetType() == GameArray)
  {
    const GameArrayType &array = oper2;
    if (array.Size() > 2)
    {
      if (array[0].GetType() == GameArray && array[1].GetType() == GameObject)
      {
        oper2 = array[0];
        obj1=GetObject(array[1]);
        if (array.Size() == 3)
        {      
          if (array[2].GetType() == GameObject)
          {
            obj2=GetObject(array[2]);
          }
        }
      }
    }
  }

  if (oper1.GetType() == GameObject)
  {
    obj1=GetObject(oper1);
    if( !obj1 ) return 1e10f;

    pos1 = obj1->VisiblePosition();
  }
  else
  {
    const GameArrayType &array1 = oper1;
    if (array1.Size() == 0) return NOTHING;

    // array of units?
    if (array1[0].GetType() == GameObject)
    {
      float los = 0;

      for (int i=0; i<array1.Size(); i++)
      {
        Object *unit = GetObject(array1[i]);
        if (!unit) continue;

        float value = GetLOS(state,GameValueExt(unit),oper2);
        if (value > los) los = value;
      }

      return los;
    }
    else
    {
      // position
      if (!CheckSize(state, array1, 3)) return NOTHING;
      for (int i=0; i<3; i++)
        if (!CheckType(state, array1[i], GameScalar)) return NOTHING;

      Vector3 tpos
      (
        array1[0],
        // convert AGL to ASL
        GLandscape->SurfaceYAboveWater(array1[0],array1[1]) + (float)array1[2],
        array1[1]
      );
      pos1 = tpos;
    }
  }

  if (oper2.GetType() == GameObject)
  {
    obj2=GetObject(oper2);
    if( !obj2 ) return 1e10f;

    pos2 = obj2->VisiblePosition();
    radius = obj2->VisibleSize();
    if (radius < 1) radius = 1;
  }
  else
  {
    const GameArrayType &array2 = oper2;
    if (array2.Size() == 0) return NOTHING;

    // array of units?
    if (array2[0].GetType() == GameObject)
    {
      float los = 0;

      for (int i=0; i<array2.Size(); i++)
      {
        Object *unit = GetObject(array2[i]);
        if (!unit) continue;

        float value = GLandscape->Visible(pos1,unit->VisiblePosition(),unit->VisibleSize(),obj1,unit);

        if (value > los) los = value;
      }

      return los;
    }
    else
    {
      // position
      if (!CheckSize(state, array2, 3)) return NOTHING;
      for (int i=0; i<3; i++)
        if (!CheckType(state, array2[i], GameScalar)) return NOTHING;

      Vector3 tpos
      (
        array2[0],
        // convert AGL to ASL
        GLandscape->SurfaceYAboveWater(array2[0],array2[1]) + (float)array2[2],
        array2[1]
      );
      pos2 = tpos;
    }
  }

  return GLandscape->Visible(pos1,pos2,radius,obj1,obj2);
}

GameValue OnShotHit(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{/*
  if (oper1.GetType() != GameObject)
  {
    state->TypeError(GameString, oper1.GetType());
    return NOTHING;
  }
  if (oper2.GetType() != GameString)
  {
    state->TypeError(GameString, oper2.GetType());
    return NOTHING;
  }
  Object *obj = GetObject(oper1);

  if (obj)
  {
    Shot *shot = dyn_cast<Shot>(obj);
    if (shot)
      shot->SetOnDelete(oper2);
  }
*/
  return NOTHING;
}

//Added possibility to use selection string as first argument
GameValue ObjSetTextureGlobal( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
  const GameArrayType &array = oper2;
  if (array.Size() < 2 || array.Size() > 3)
  {
    state->SetError(EvalDim, array.Size(), 2);
    return NOTHING;
  }
  if (array[0].GetType() != GameScalar 
#if _VBS3    
    && array[0].GetType() != GameString)
#endif
  {
    state->TypeError(GameScalar, array[0].GetType());
    return NOTHING;
  }
  if (array[1].GetType() != GameString)
  {
    state->TypeError(GameString, array[1].GetType());
    return NOTHING;
  }
  if (array.Size() == 3 && array[2].GetType() != GameString)
  {
    state->TypeError(GameString, array[2].GetType());
    return NOTHING;
  }

  Object *obj = GetObject(oper1);
  if (!obj) return NOTHING;

  EntityAI *veh = dyn_cast<EntityAI>(obj);
  if (!veh) return NOTHING;

  int index;
#if _VBS3
  if(array[0].GetType() == GameString)
  {
    RString selName = array[0];
    index = veh->GetType()->GetHiddenSelectionIndex(selName);
  }
  else
#endif
    index = toInt((float)array[0]);

  RString name = array[1];

  RString eval = "true";
  if (array.Size() == 3)
    eval = array[2];

  if(stricmp(name, "#reset") == 0)
  {
    veh->ResetObjectTexture(index);
  }
  else
  {
    RString FindPicture (RString name);
    name = FindPicture(name);
    Ref<Texture> texture = GlobLoadTexture(name);
    veh->SetObjectTexture(index, texture);
  }
  // pass over network
  if (GWorld->GetMode() == GModeNetware)
    GetNetworkManager().SetObjectTexture(veh,index,name);

  return NOTHING;
}

GameValue GetFaceTextures( const GameState *state, GameValuePar oper1)
{
  GameValue value = state->CreateGameValue(GameArray);
  GameArrayType &value_array = value;
  value_array.Resize(2);
  value_array[0] = "";
  value_array[1] = "";

  if(CheckType(state, oper1, GameObject))
  {
    Object *obj = GetObject(oper1);
    Man *man = dyn_cast<Man>(obj);
    if(man)
    {
#if _ENABLE_NEWHEAD
      const Head& head = man->GetHead();
#else
      const HeadOld& head = man->GetHead();
#endif
      value_array[0] = head.GetFace() ? head.GetFace()->GetName() : "";
      value_array[1] = head._glasses ? head._glasses->GetName() : "";
    }
  }
  return value_array;
}

GameValue GetObjTexture( const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
    RString retStr;
    if(CheckType(state, oper1, GameObject))
    {
      Object *obj = GetObject(oper1);
      EntityAI *veh = dyn_cast<EntityAI>(obj);
      if(veh)
      {
        int index = -1;
        if(oper2.GetType() == GameString)
        {
          RString selName = oper2;
          index = veh->GetType()->GetHiddenSelectionIndex(selName);
        }
        else
          if(oper2.GetType() == GameScalar)
            index = toInt((float)oper2);
        if(index >= 0)
          return veh->GetHiddenSelectionTexture(index);
      }
    }
    return retStr;
}

GameValue GroundIntercept( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
  Vector3 vFrom;

  GameValue rtn_value = state->CreateGameValue(GameArray);
  GameArrayType &rtn = rtn_value;
  rtn.Resize(3);

  rtn[0] = 0.0f;
  rtn[1] = 0.0f;
  rtn[2] = 0.0f;

  if (oper1.GetType() == GameArray)
  {
    const GameArrayType &from = oper1;
    if (!CheckSize(state, from, 3)) return rtn_value;

    vFrom[0] = from[0];
    vFrom[2] = from[1];
    // convert AGL to ASL
    vFrom[1] = GLandscape->SurfaceYAboveWater(vFrom[0],vFrom[2]) + (float)from[2];
  }
  else
  {
    Object *obj = GetObject(oper1);

    if (obj)
    {
      Vector3Val pos = obj->VisiblePosition();

      vFrom[0] = pos[0];
      vFrom[1] = pos[1];
      vFrom[2] = pos[2];
    }
    else
      return rtn_value;
  }

  const GameArrayType &dir = oper2;
  if (!CheckSize(state, dir, 3)) return rtn_value;

  Vector3 vDir(dir[0],dir[2],dir[1]);

  Vector3 lPos;
  GLandscape->IntersectWithGroundOrSea(&lPos,vFrom,vDir);

  rtn[0] = lPos[0];
  rtn[1] = lPos[2];
  rtn[2] = lPos[1];

  return rtn_value;
}

GameValue GetLOP( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
  // radius
  float radius = 1;

  // position from
  Object *obj1=NULL;
  Vector3 pos1;

  if (oper1.GetType() == GameObject)
  {
    obj1=GetObject(oper1);
    if( !obj1 ) return NOTHING;

    pos1 = obj1->VisiblePosition();
    radius = obj1->VisibleSize();
  }
  else
  {
    const GameArrayType &array1 = oper1;
    if (!CheckSize(state, array1, 3)) return NOTHING;
    for (int i=0; i<3; i++)
      if (!CheckType(state, array1[i], GameScalar)) return NOTHING;

    Vector3 tpos
    (
      array1[0],
      // convert AGL to ASL
      GLandscape->SurfaceYAboveWater(array1[0],array1[1]) + (float)array1[2],
      array1[1]
    );
    pos1 = tpos;
  }

  Object *obj2=NULL;
  Vector3 pos2;

  // position to
  if (oper2.GetType() == GameObject)
  {
    obj2=GetObject(oper2);
    if( !obj2 ) return NOTHING;

    pos2 = obj2->VisiblePosition();
  }
  else
  {
    const GameArrayType &array2 = oper2;
    if (array2.Size() == 0) return NOTHING;

    if (!CheckType(state, array2[0], GameObject))
      return NOTHING;

    GameValue value = state->CreateGameValue(GameArray);
    GameArrayType &array = value;
    array.Realloc(array2.Size());

    for (int i=0; i<array2.Size(); i++)
    {
      Object *unit = GetObject(array2[i]);
      if (!unit) continue;

      array.Add(GetLOP(state,oper1,GameValueExt(unit)));
    }

    return value;
  }

  if (!CheckType(state, oper2, GameObject))
    return NOTHING;

  Vector3Par dir = pos2 - pos1;

  Vector3 dirNorm = dir.Normalized();
  float maxDist = dir*dirNorm;

  Vector3 isect;
  float t = GLandscape->IntersectWithGroundOrSea(&isect,pos1,dirNorm,0,maxDist*1.1);

  //char buf[200];
  //sprintf(buf,"t: %f\\n\\nmaxDist: %f\\n\\ndir: %f",t,maxDist,atan2(dir.X(), dir.Z()) * (180.0f / H_PI));
  //GWorld->UI()->ShowHint(buf);

  // no intersect with ground?
  if (t > maxDist)
  {
    // test collision with objects
    CollisionBuffer collision;
    GLandscape->ObjectCollision(collision,obj1,obj2,pos1,pos2,radius);

    if( collision.Size() > 0 )
      return false;
  }
  else
    return false;

  return true;
}

// functions
GameValue ObjConvertToAGL(const GameState *state, GameValuePar oper1)
{
  const GameArrayType &array = oper1;
  if (!CheckSize(state, array, 3)) return NOTHING;
  for (int i=0; i<3; i++)
    if (!CheckType(state, array[i], GameScalar)) return NOTHING;

  GameValue value = state->CreateGameValue(GameArray);
  GameArrayType &value_array = value;
  value_array.Resize(3);

  float h = array[2];

  value_array[0] = array[0];
  value_array[1] = array[1];
  value_array[2] = h - GLandscape->SurfaceYAboveWater(array[0],array[1]);

  return value;
}

GameValue ObjConvertToASL(const GameState *state, GameValuePar oper1)
{
  const GameArrayType &array = oper1;
  if (!CheckSize(state, array, 3)) return NOTHING;
  for (int i=0; i<3; i++)
    if (!CheckType(state, array[i], GameScalar)) return NOTHING;

  GameValue value = state->CreateGameValue(GameArray);
  GameArrayType &value_array = value;
  value_array.Resize(3);

  value_array[0] = array[0];
  value_array[1] = array[1];
  value_array[2] = GLandscape->SurfaceYAboveWater(array[0],array[1]) + (float)array[2];

  return value;
}

GameValue ObjGetPosVisible(const GameState *state, GameValuePar oper1)
{
  Object *obj = GetObject(oper1);
  GameValue value = state->CreateGameValue(GameArray);
  GameArrayType &array = value;
  array.Resize(3);
  if (obj)
  {
    Vector3 pos = obj->VisiblePosition();
/*
    // is this a unit? do a death check
    Person *soldier = dyn_cast<Person>(obj);
    if (soldier)
      if (obj->IsDammageDestroyed())
        pos = obj->WorldPosition(obj->FutureVisualState());
*/
    array[0] = pos.X();
    array[1] = pos.Z();
    array[2] = pos.Y() - GLandscape->SurfaceYAboveWater(pos.X(),pos.Z());
  }
  else
  {
    array[0] = 0.0f; array[1] = 0.0f; array[2] = 0.0f;
  }
  return value;
}

GameValue IsPlayerControlled(const GameState *state, GameValuePar oper1)
{
  Object *obj = GetObject(oper1);
  if (!obj) return NOTHING;

  Person *soldier = dyn_cast<Person>(obj);
  if (!soldier) return NOTHING;

  return soldier->IsNetworkPlayer();
}


GameValue IsMapPlaced(const GameState *state, GameValuePar oper1)
{
  Object *obj = GetObject(oper1);
  if (!obj) return NOTHING;

  NetworkId id = obj->GetNetworkId();

  return id.creator == STATIC_OBJECT;
}

GameValue CanSpeak(const GameState *state, GameValuePar oper1)
{
  Object *obj = GetObject(oper1);
  Person *person = dyn_cast<Person>(obj);

  if(!person) return true;

  if(person->Brain())
    return person->Brain()->CanSpeak();

  return false;
}

GameValue AllowSpeech( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
  // Validate parameters
  Object *obj = GetObject(oper1);
  if(!obj) return NOTHING;
  if(oper2.GetType() != GameBool) return NOTHING;

  // Check person and brain
  Person* person = dyn_cast<Person>(obj);
  if(!person) return NOTHING;
  if(!person->Brain()) return NOTHING;

  // Disable/Enable AI or player ability to speak
  person->Brain()->SetAllowSpeech(oper2);

  return NOTHING;
}

GameValue IsTurnedOut(const GameState *state, GameValuePar oper1)
{
  //turned out = true is default value

  Object *obj = GetObject(oper1);
  if (!obj) return true;

  Person *person = dyn_cast<Person>(obj);
  if (!person) return true;
  if(!person->Brain()) return true;
  
  Transport* transport = person->Brain()->GetVehicleIn();
  if(!transport) return true;
  
  return !transport->IsPersonHidden(transport->FutureVisualState(), person);
}

GameValue IsKeyPressed(const GameState *state, GameValuePar oper1)
{
  if(oper1.GetType() != GameScalar) return false;
  int dik = int(float(oper1));
  if(dik <0 || dik > 255) return false;
  return GInput.keyPressed[dik] > 0;
}

GameValue IsJoyKeyPressed(const GameState *state, GameValuePar oper1)
{
  if(oper1.GetType() != GameScalar) return false;
  int dik = int(float(oper1));
  if(dik <0 || dik >= N_JOYSTICK_BUTTONS) return false;
  return GInput.stickButtonsPressed[dik];
}

GameValue BindKey(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
	if (oper1.GetType() != GameString && oper1.GetType() != GameScalar) return NOTHING;
	if (!CheckType(state, oper2, GameString)) return NOTHING;

  RString command = oper2;
  if(oper1.GetType() == GameString)
  {
	RString keystr = oper1;
	return (float) GWorld->AddKeyBinding(keystr,command);
  }
  if(oper1.GetType() == GameScalar)
  {
    int code = int(float(oper1));
    return (float) GWorld->AddKeyBinding(code,command);
  }
  return float(-1);
};

GameValue UnBindKey( const GameState *state, GameValuePar oper1 )
{
	if (!CheckType(state, oper1, GameScalar)) return NOTHING;

	GWorld->RemoveKeyBinding(toInt((float)oper1));

	return NOTHING;
}

GameValue IsWearingNVG(const GameState *state, GameValuePar oper1)
{
  Object *obj = GetObject(oper1);
  if (!obj) return NOTHING;

  Man *soldier = dyn_cast<Man>(obj);
  if (!soldier) return NOTHING;

  return soldier->IsWearingNVG();
}

int ConsoleF(const char *format, ...);
GameValue ConsoleLog(const GameState *state, GameValuePar oper1)
{
#if _ENABLE_CHEATS
  RString txt = oper1;
  LogF("consoleLog: %s",(const char *) txt);
  ConsoleF((const char *) txt);
#endif
  return NOTHING;
}


GameValue DoWarningMessage(const GameState *state, GameValuePar oper1)
{
  //TODO: Localize?
  RString txt = oper1;
  GWorld->CreateWarningMessage(txt);
  return NOTHING;
}

GameValue LockMapToObject( const GameState *state, GameValuePar oper1 )
{
  const GameArrayType &array = oper1;
  if (!CheckSize(state, array, 2)) return NOTHING;

  if (!CheckType(state, array[0], GameObject)) return NOTHING;
  if (!CheckType(state, array[1], GameString)) return NOTHING;

  Object *obj=GetObject(array[0]);
  if (!obj) return NOTHING;

  RString eval = array[1];

//  GWorld->SetLockMap(obj,eval);

  return NOTHING;
}

/// Function adding crew entries as part of the Script function getCrewPos
inline void AddCrewPosArrayEntry(const GameState *state, GameArrayType &value_array
                                 , RString posType, Vector3 posn, float posId, Person* person
                                 , GameValue pathvalue = NOTHING)
{
  GameValue subvalue = state->CreateGameValue(GameArray);
  GameArrayType &sub_array = subvalue;

  if(!pathvalue.GetNil())
  {
    sub_array.Resize(5);
    sub_array[4]=pathvalue;
  }
  else
    sub_array.Resize(4);

  sub_array[0] = posType;

  GameValue subvalue2 = state->CreateGameValue(GameArray);
  GameArrayType &pos_array = subvalue2;
  pos_array.Resize(3);
  pos_array[0] = posn.X(); pos_array[1] = posn.Z(); pos_array[2] = posn.Y();

  sub_array[1] = subvalue2;
  sub_array[2] = GameValue(posId);
  sub_array[3] = GameValueExt(person);

  value_array.Add(subvalue);
}

/// Functor adding gunner proxies as part of the Script function getCrewPos
class AddGunnerProxyPos : public ITurretFunc
{
protected:
  int _level;
  GameArrayType &_value_array;
  const GameState *_state;
  int _i;
public:
  AddGunnerProxyPos(const GameState *state, GameArrayType &value_array, int level)
    : _state(state), _value_array(value_array), _level(level), _i(0) {}

    bool operator () (EntityAIFull *entity, TurretContext &context)
    {
      if (!context._turret) return false; // continue
      Transport *veh=dyn_cast<Transport>(entity);
      DoAssert(veh);
      const ManProxy &proxy = context._turretType->_gunnerProxy[_level];
      if (proxy.Present())
      {
        Matrix4 transf = veh->TurretTransform(*context._turretType);
        Vector3 pos = transf.FastTransform(proxy.transform.Position());
        AutoArray<int, MemAllocLocal<int, 16> > path;
        veh->GetTurretPath(path,context._turret);

        GameValue pathvalue = _state->CreateGameValue(GameArray);
        GameArrayType &path_array = pathvalue;
        path_array.Resize(path.Size());
        for(int i=0; i < path.Size(); ++i)
          path_array[i]=float(path[i]);
        
        AddCrewPosArrayEntry(_state, _value_array, "gunner", pos, context._turretType->_commanding, context._turret->Gunner(), pathvalue);
      }
      return false; // continue
    }
};

/*!
\patch VBS 0.11 Date 17/10/2006 by Mark
 - Added: getCrewPos - get type/position of vehicle proxies
*/
GameValue GetCrewPos( const GameState *state, GameValuePar oper1 )
{
  GameValue value = state->CreateGameValue(GameArray);
  GameArrayType &value_array = value;

  Object *obj1=GetObject(oper1);
  if( !obj1 ) return value;

  Transport *veh=dyn_cast<Transport>(obj1);
  if(!veh)
    return value;

  int fireGeoIndex = veh->GetShape()->FindFireGeometryLevel();
  if(fireGeoIndex == -1)
  {
    LogF("Error: No firegeo found!");
    return value;
  }

  const TransportType *type = veh->Type();
  const LevelProxies *proxies = type->GetLevelProxies(fireGeoIndex);
  if(!proxies)
    return value;

  const ManProxy *proxy = NULL;
  
  for(int i=0; i < proxies->_cargoProxy.Size(); ++i)
  {
    proxy = &proxies->_cargoProxy[i];
    if(proxy->Present())
    {
      Person* CargoUnit = NULL;
      const ManCargo manCargo = veh->GetManCargo();
      if(i < manCargo.Size())
        CargoUnit = manCargo[i];
      AddCrewPosArrayEntry(state, value_array, "cargo", proxy->transform.Position(), i, CargoUnit);
    }
  }
  if(proxies->_driverProxy.Present())
  {
    proxy = &proxies->_driverProxy;
    AddCrewPosArrayEntry(state, value_array, "driver", proxy->transform.Position(), 0, veh->Driver());
  }

  AddGunnerProxyPos func(state, value_array, fireGeoIndex);
  veh->ForEachTurret(func);

  return value;
}

/*!
\patch VBS 0.1 Date 9/5/2006 by Mark
 - Added: Get Cargo Index for Unit in vehicle
*/
GameValue GetCargoPos( const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
  Object *obj1=GetObject(oper1);
  Object *obj2=GetObject(oper2);
  if( !obj1 || !obj2 ) return -1.0f;

  Transport *trans=dyn_cast<Transport>(obj1);
  if(!trans)
    return NOTHING;

  const ManCargo &cargo = trans->GetManCargo();
  for( float i=0; i<cargo.Size(); i++ )
    if(cargo[i] == obj2)
      return i;
  return -1.0f;
}

/*!
\patch VBS 0.1 Date 9/5/2006 by Mark
 - Added: Get Unit for given Cargo position
*/
GameValue GetUnitAtCargoPos( const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
  Object *obj1=GetObject(oper1);
  if(!obj1) return OBJECT_NULL;

  Transport *trans=dyn_cast<Transport>(obj1);
  if(!trans)
    return OBJECT_NULL;
  const ManCargo &cargo = trans->GetManCargo();

  float fIndex = oper2;
  int Index = fIndex;
  if(Index < 0 || Index >= cargo.Size())
    return OBJECT_NULL;

  Object *obj = cargo[Index];
  if(obj)
    return GameValueExt(obj);

  return OBJECT_NULL;
}

GameValue IsAgentsEnabled(const GameState *state)
{
#if _ENABLE_INDEPENDENT_AGENTS
  return true;
#else
  return false;
#endif
}

GameValue IsAdmin(const GameState *state)
{
  return Glob.vbsAdminMode;
}

GameValue SetAdmin(const GameState *state, GameValuePar oper1)
{
  if (oper1.GetType() != GameBool)
  {
    state->TypeError(GameBool, oper1.GetType());
    return NOTHING;
  }
  bool set = oper1;
  if (Glob.vbsAdminUser) Glob.vbsAdminMode = set;
  return NOTHING;
}

GameValue GetWorldCenter(const GameState *state)
{
  GameValue value = state->CreateGameValue(GameArray);
  GameArrayType &array = value;
  if (*Glob.header.worldname)
  {
    array.Resize(2);
    array[0] = (float)(Pars >> "CfgWorlds" >> Glob.header.worldname >> "centerPosition")[0]; 
    array[1] = (float)(Pars >> "CfgWorlds" >> Glob.header.worldname >> "centerPosition")[1];
  }
  return value;
}

GameValue EyeAccomCut(const GameState *state)
{
  GWorld->OnCameraChanged();
  return NOTHING;
}

GameValue ScreenResolution(const GameState *state)
{
  GameValue value = state->CreateGameValue(GameArray);
  GameArrayType &array = value;
  array.Resize(2);

  if (GEngine)
  {
    AspectSettings _aspect;
    array[0] = (float)GEngine->Width();
    array[1] = (float)GEngine->Height();
    return value;
  }

  return NOTHING;
}

GameValue AspectRatio(const GameState *state)
{
  GameValue value = state->CreateGameValue(GameArray);
  GameArrayType &array = value;
  array.Resize(2);

  if (GEngine)
  {
    AspectSettings _aspect;
    GEngine->GetAspectSettings(_aspect);
    array[0] = _aspect.leftFOV;
    array[1] = _aspect.topFOV;
    return value;
  }

  return NOTHING;
/*    
  bool ParseUserParams(ParamFile &cfg, RString name, GameDataNamespace *globals);
  RString GetLoginName();
  
  // read player name from registry
  RString playerName = GetLoginName();
  HKEY key;
  BYTE buf[256];
  DWORD bufSize = sizeof(buf);
  if
  (
    ::RegOpenKeyEx(HKEY_CURRENT_USER, ConfigApp, 0, KEY_READ, &key) == ERROR_SUCCESS &&
    ::RegQueryValueEx(key, "Player Name", NULL, NULL, buf, &bufSize) ==  ERROR_SUCCESS &&
    bufSize > 1
  )
  {
    ::RegCloseKey(key);
    playerName = (const char *)buf;
  }

  ParamFile cfg;
  ParseUserParams(cfg,playerName);
  float fovLeft = cfg.ReadValue("fovLeft",1.0f);
  float fovTop = cfg.ReadValue("fovTop",0.75f);
  array[0] = fovLeft;
  array[1] = fovTop;
  return value;
*/
}

GameValue ObjGetFlyingHeight(const GameState *state, GameValuePar oper1)
{
  Object *obj = GetObject(oper1);
  HelicopterAuto *veh = dyn_cast<HelicopterAuto>(obj);
  if (!veh) return 0.0f;
  return veh->GetFlyingHeight();
}

class BuildUnitsList
{
  GameArrayType &_units;
public:
  BuildUnitsList(GameArrayType &array):_units(array){}
  __forceinline bool operator () (Entity *veh) const
  {
    Person *person = dyn_cast<Person>(veh);
    if (person)
    {
      _units.Add(GameValueExt(person));
    }
    return false;
  }
};

GameValue GetUnits(const GameState *state)
{
  GameValue value = state->CreateGameValue(GameArray);
  GameArrayType &array = value;

  BuildUnitsList build(array);
  GWorld->ForEachVehicle(build);
  GWorld->ForEachOutVehicle(build);

  array.Compact();
  return value;
}

class GetTypeExists
{
  bool &_exists;
  RString _type;
public:
  GetTypeExists(bool &exists, RString type):_exists(exists),_type(type){}
  __forceinline bool operator () (Entity *veh) const
  {
    ObjectTyped *ent = dyn_cast<ObjectTyped>(veh);
    if (ent)
    {
      const EntityType *etype = ent->GetNonAIType();
      if (etype)
        if (stricmp(etype->GetName(),_type) == 0)
        {
          _exists = true;
          return true;
        }
    }
    return false;
  }
};

GameValue ObjTypeExists(const GameState *state, GameValuePar oper1)
{
  if (oper1.GetType() != GameString)
  {
    state->TypeError(GameString, oper1.GetType());
    return false;
  }
  
  RString type = oper1;
  bool exists = false;

  GetTypeExists func(exists,type);
  GWorld->ForEachVehicle(func);
  if (!exists) GWorld->ForEachOutVehicle(func);
  if (!exists) GWorld->ForEachSlowVehicle(func);

  return exists;
}

GameValue GetTriggers(const GameState *state)
{
  GameValue value = state->CreateGameValue(GameArray);
  GameArrayType &array = value;

  for (int i=0; i<sensorsMap.Size(); i++)
    array.Add(GameValueExt(sensorsMap[i]));

  array.Compact();
  return value;
}

RString GetUserDirectory();
void CreatePath(RString path);

//////////////////////////////////////////////////////////////////////////
// AAR Module
//////////////////////////////////////////////////////////////////////////
#if _AAR

GameValue AARLoad(const GameState *state,GameValuePar oper1)
{
  RString name = oper1;
  if(name.GetLength()==0)
  {
    LogF("String path length is zero");
    return false;
  }

  bool absolute = false;
  for( int i = 0; i < name.GetLength(); i++ )
    if( name[i] == '\\' )
    {
      absolute = true;
      break;
    }

  RString path;
  if(absolute)
  {
    path = name;
  }
  else
  {
    RString directory = GetUserDirectory() + RString("AAR\\");
    RString path = directory + name;    
  }

  if(GAAR.IsRecording()||GAAR.IsPlaying())
    return false;

  LSError ferror = GAAR.LoadFile(path);
  if(ferror!=LSOK) // check if file loading was successfull
    return false;

  return true;
}

GameValue AARIsLoaded(const GameState *state)
{
  return GAAR.IsLoaded();
}

GameValue AARSetTime(const GameState *state,GameValuePar oper1)
{
  if(GAAR.IsPlaying())
  {
    float moveto = oper1;
    
    if(moveto<=GAAR.GetDuration())
      GAAR.MoveToTime(moveto);
  }
  return NOTHING;
}

GameValue AARNextMessage(const GameState *state)
{
  if(GAAR.IsPlaying() && GAAR.CurrentMessageIndex() < GAAR.MessageCount())
    GAAR.NextMessage();

  return NOTHING;
}

GameValue AARPreviousMessage(const GameState *state)
{
  if(GAAR.IsPlaying() && GAAR.CurrentMessageIndex() > 0)
    GAAR.PreviousMessage();

  return NOTHING;
}

GameValue AARStopPlaying(const GameState *state)
{
  if(GAAR.IsPlaying()) GAAR.PlayStop();
  return NOTHING;
}

GameValue AARSave(const GameState *state,GameValuePar oper1)
{
  RString path = oper1;

  if(GAAR.IsPlaying()) return NOTHING;
   
  if(GetNetworkManager().IsServer())
    GAAR.RecordSave(path);
  else
    GetNetworkManager().AARAskUpdate(SaveFile,0,path);

  return NOTHING;
}

GameValue AARStopRecording(const GameState *state)
{ 
  if(GetNetworkManager().IsServer())
    GAAR.RecordStop();
  else
    GetNetworkManager().AARAskUpdate(StopRecord,0,RString(""));

  return NOTHING;
}

GameValue AARSaveState(const GameState *state)
{
  return (float)GAAR.GetSaveState();
}

GameValue AARRecord(const GameState *state)
{
  if(GetNetworkManager().IsServer())
    GAAR.Record();
  else
    GetNetworkManager().AARAskUpdate(StartRecord,0,RString(""));

  return NOTHING;
}

GameValue AARSetRepeat(const GameState *state,GameValuePar oper1)
{
  if(oper1.GetType()!= GameBool)
    return NOTHING;

  bool repeat = oper1;
  GAAR.SetRepeat(repeat);
  return NOTHING;
}

GameValue AARGetBMS(const GameState *state)
{ 
  return (float) GAAR.GetBookMarkSize();
}

/*
\VBS_patch_internal 1.00 Date [3/4/2008] by clion
- Fixed: Parameter fix, AARaddBookMark now records time correctly
*/
GameValue AARAddBM(const GameState *state,GameValuePar oper1)
{
  if(!CheckType(state,oper1,GameArray)) return NOTHING;
  
  const GameArrayType &newEntry = oper1;
  if (newEntry.Size() != 3) return NOTHING;

  if(newEntry[0].GetType() != GameScalar)
    state->TypeError(GameScalar, newEntry[0].GetType());
  if(newEntry[1].GetType() != GameString)
    state->TypeError(GameString,newEntry[1].GetType());
  if(newEntry[2].GetType() != GameString)
    state->TypeError(GameString,newEntry[2].GetType());
  
  // Finaly add the new entry and return its index;
  int index = GAAR.AddBookMark(newEntry[0],newEntry[1],newEntry[2]);
  return (float) index;   
}

GameValue AARRemBM(const GameState *state,GameValuePar oper1)
{
  if(!CheckType(state,oper1,GameScalar)) return NOTHING;
  
  int index = abs(oper1);
  if(index >= 0 && index < GAAR.GetBookMarkSize())
    GAAR.RemoveBookMark(index);

  return NOTHING;
}

GameValue AARGetBMN(const GameState *state,GameValuePar oper1)
{
  if(!CheckType(state,oper1,GameScalar)) return NOTHING;

  int index = abs(oper1);
  if(index >= 0 && index < GAAR.GetBookMarkSize())
  {
    BookMarkLifeSpan &bm = GAAR.GetBookMark(index);
    return bm.GetName();
  }

  return NOTHING;
}

GameValue AARGetBMJT(const GameState *state,GameValuePar oper1)
{
  if(!CheckType(state,oper1,GameScalar)) return NOTHING;

  int index = abs(oper1);
  if(index >= 0 && index < GAAR.GetBookMarkSize())
  {
    BookMarkLifeSpan &bm = GAAR.GetBookMark(index);
    return bm.GetJumpTo();
  }

  return 0.0f;
}

GameValue AARGetBMMsg(const GameState *state,GameValuePar oper1)
{
  if(!CheckType(state,oper1,GameScalar)) return NOTHING;

  int index = abs(oper1);
  if(index >= 0 && index < GAAR.GetBookMarkSize())
  {
    BookMarkLifeSpan &bm = GAAR.GetBookMark(index);
    return bm.GetMessage();
  }

  return NOTHING; 
}

GameValue AARUnload(const GameState *state)
{
  if(!GAAR.IsPlaying())
    GAAR.UnLoadFile();

  return NOTHING;
}

GameValue AARCurrentTime(const GameState *state)
{
  if(GAAR.IsPlaying())
    return GAAR.CurrentTime(); 
  else
    return GAAR.GetRecordTimeOffset();
}

GameValue AARIsPlaying(const GameState *state){ return GAAR.IsPlaying(); }
GameValue AARPlay(const GameState *state){ GAAR.Play(); return NOTHING;}
GameValue AARIsPaused(const GameState *state){ return GAAR.IsPlayPaused();}
GameValue AARIsRepeat(const GameState *state){ return GAAR.IsPlayRepeat(); }
GameValue AARPause(const GameState *state){ GAAR.PlayPause();  return NOTHING;}
GameValue AARLength(const GameState *state){ return GAAR.GetDuration();}
GameValue AARIsRecording(const GameState *state){ return GAAR.IsRecording() || GAAR.IsRemoteRecording(); }


// Behaviour related functions
GameValue GetFatigue(const GameState *state, GameValuePar oper1) 
{
  if (oper1.GetType() != GameObject) return -1.0f;
  Object *obj1 = GetObject(oper1);
  if (!obj1) return -1.0f;
  Man *man = dyn_cast<Man>(obj1);
  if (!man) return -1.0f;
  return man->GetFatigue();
}

GameValue GetAerobicFatigue(const GameState *state, GameValuePar oper1) 
{
  if (oper1.GetType() != GameObject) return -1.0f;
  Object *obj1 = GetObject(oper1);
  if (!obj1) return -1.0f;
  Man *man = dyn_cast<Man>(obj1);
  if (!man) return -1.0f;
  return man->GetAerobicExertion();
}

GameValue GetAnaerobicFatigue(const GameState *state, GameValuePar oper1) 
{
  if (oper1.GetType() != GameObject) return -1.0f;
  Object *obj1 = GetObject(oper1);
  if (!obj1) return -1.0f;
  Man *man = dyn_cast<Man>(obj1);
  if (!man) return -1.0f;
  return man->GetAnaerobicExertion();
}

GameValue GetMoraleLevel(const GameState *state, GameValuePar oper1) 
{
  if (oper1.GetType() != GameObject) return -1.0f;
  Object *obj1 = GetObject(oper1);
  if (!obj1) return -1.0f;
  Man *man = dyn_cast<Man>(obj1);
  if (!man) return -1.0f;
  Morale *mor = man->GetMorale();
  if (!mor) return -1.0f;
  return mor->GetMoraleLevel()/100.0f;
}


GameValue GetMoraleLongTerm(const GameState *state, GameValuePar oper1) 
{
  if (oper1.GetType() != GameObject) return -1.0f;
  Object *obj1 = GetObject(oper1);
  if (!obj1) return -1.0f;
  Man *man = dyn_cast<Man>(obj1);
  if (!man) return -1.0f;
  Morale *mor = man->GetMorale();
  if (!mor) return -1.0f;
  return mor->GetMoraleLongTerm();
}

GameValue GetSuppressionRadius(const GameState *state, GameValuePar oper1) 
{
  if (oper1.GetType() != GameObject) return -1.0f;
  Object *obj1 = GetObject(oper1);
  if (!obj1) return -1.0f;
  Man *man = dyn_cast<Man>(obj1);
  if (!man) return -1.0f;
  Morale *mor = man->GetMorale();
  if (!mor) return -1.0f;
  return mor->GetSuppressionRadius();
}

GameValue GetTraining(const GameState *state, GameValuePar oper1) 
{
  if (oper1.GetType() != GameObject) return -1.0f;
  Object *obj1 = GetObject(oper1);
  if (!obj1) return -1.0f;
  Man *man = dyn_cast<Man>(obj1);
  if (!man) return -1.0f;
  Morale *mor = man->GetMorale();
  if (!mor) return -1.0f;
  return mor->GetTraining();
}

GameValue GetExperience(const GameState *state, GameValuePar oper1) 
{
  if (oper1.GetType() != GameObject) return -1.0f;
  Object *obj1 = GetObject(oper1);
  if (!obj1) return -1.0f;
  Man *man = dyn_cast<Man>(obj1);
  if (!man) return -1.0f;
  Morale *mor = man->GetMorale();
  if (!mor) return -1.0f;
  return mor->GetExperience();
}

GameValue GetEndurance(const GameState *state, GameValuePar oper1) 
{
  if (oper1.GetType() != GameObject) return -1.0f;
  Object *obj1 = GetObject(oper1);
  if (!obj1) return -1.0f;
  Man *man = dyn_cast<Man>(obj1);
  if (!man) return -1.0f;
  Morale *mor = man->GetMorale();
  if (!mor) return -1.0f;
  return mor->GetEndurance();
}

GameValue GetLeadership(const GameState *state, GameValuePar oper1) 
{
  if (oper1.GetType() != GameObject) return -1.0f;
  Object *obj1 = GetObject(oper1);
  if (!obj1) return -1.0f;
  Man *man = dyn_cast<Man>(obj1);
  if (!man) return -1.0f;
  Morale *mor = man->GetMorale();
  if (!mor) return -1.0f;
  return mor->GetLeadership();
}

GameValue AddToMorale(const GameState *state, GameValuePar oper1, GameValuePar oper2) 
{
  if (oper1.GetType() != GameObject) return NOTHING;
  Object *obj1 = GetObject(oper1);
  if (!obj1) return NOTHING;
  Man *man = dyn_cast<Man>(obj1);
  if (!man) return NOTHING;
  Morale *mor = man->GetMorale();
  if (!mor) return NOTHING;
  float adjust = oper2;
  if (adjust>=-1 && adjust<=1)
      adjust *=100;
  mor->AddToMorale(adjust);
  return NOTHING;
}


GameValue AddToAerobicFatigue(const GameState *state, GameValuePar oper1, GameValuePar oper2) 
{
  if (oper1.GetType() != GameObject) return NOTHING;
  Object *obj1 = GetObject(oper1);
  if (!obj1) return NOTHING;
  Man *man = dyn_cast<Man>(obj1);
  if (!man) return NOTHING;
  float adjust = oper2;
  man->AddAerobicExertion(adjust);
  return NOTHING;
}

GameValue AddToAnaerobicFatigue(const GameState *state, GameValuePar oper1, GameValuePar oper2) 
{
  if (oper1.GetType() != GameObject) return NOTHING;
  Object *obj1 = GetObject(oper1);
  if (!obj1) return NOTHING;
  Man *man = dyn_cast<Man>(obj1);
  if (!man) return NOTHING;
  float adjust = oper2;
  man->AddAnaerobicExertion(adjust);
  return NOTHING;
}

GameValue AddToFatigue(const GameState *state, GameValuePar oper1, GameValuePar oper2) 
{
  if (oper1.GetType() != GameObject) return NOTHING;
  Object *obj1 = GetObject(oper1);
  if (!obj1) return NOTHING;
  Man *man = dyn_cast<Man>(obj1);
  if (!man) return NOTHING;
  float adjust = oper2;
  man->AddExertion(adjust);
  return NOTHING;
}


GameValue SetLeadership(const GameState *state, GameValuePar oper1, GameValuePar oper2) 
{
  if (oper1.GetType() != GameObject) return NOTHING;
  Object *obj1 = GetObject(oper1);
  if (!obj1) return NOTHING;
  Man *man = dyn_cast<Man>(obj1);
  if (!man) return NOTHING;
  Morale *mor = man->GetMorale();
  if (!mor) return NOTHING;
  float val = oper2;
  mor->SetLeadership(val);
  return NOTHING;
}

GameValue SetExperience(const GameState *state, GameValuePar oper1, GameValuePar oper2) 
{
  if (oper1.GetType() != GameObject) return NOTHING;
  Object *obj1 = GetObject(oper1);
  if (!obj1) return NOTHING;
  Man *man = dyn_cast<Man>(obj1);
  if (!man) return NOTHING;
  Morale *mor = man->GetMorale();
  if (!mor) return NOTHING;
  float val = oper2;
  mor->SetExperience(val);
  return NOTHING;
}

GameValue SetTraining(const GameState *state, GameValuePar oper1, GameValuePar oper2) 
{
  if (oper1.GetType() != GameObject) return NOTHING;
  Object *obj1 = GetObject(oper1);
  if (!obj1) return NOTHING;
  Man *man = dyn_cast<Man>(obj1);
  if (!man) return NOTHING;
  Morale *mor = man->GetMorale();
  if (!mor) return NOTHING;
  float val = oper2;
  mor->SetTraining(val);
  return NOTHING;
}

GameValue SetEndurance(const GameState *state, GameValuePar oper1, GameValuePar oper2) 
{
  if (oper1.GetType() != GameObject) return NOTHING;
  Object *obj1 = GetObject(oper1);
  if (!obj1) return NOTHING;
  Man *man = dyn_cast<Man>(obj1);
  if (!man) return NOTHING;
  Morale *mor = man->GetMorale();
  if (!mor) return NOTHING;
  float val = oper2;
  mor->SetEndurance(val);
  return NOTHING;
}
float GetStatData(const RString &name,const AARStat &stat)
{
  if(strcmpi(name,"roundsFired")==0)
    return stat.roundsFired;
  if(strcmpi(name,"enemyKilled")==0)
    return stat.enemyKilled;
  if(strcmpi(name,"friendlyKilled")==0)
    return stat.friendlyKilled;
  if(strcmpi(name,"enemyWounded")==0)
    return stat.enemyWounded;
  if(strcmpi(name,"friendlyWounded")==0)
    return stat.friendlyWounded;

  return 0.0f;
}

class SumStatCountrySide
{
  const TargetSide &_side;
  AARStat &_total;
public:
  SumStatCountrySide(AARStat &total,TargetSide &side):_total(total),_side(side){}
  __forceinline bool operator () (Entity *veh) const
  {
    EntityAI *obj = dyn_cast<EntityAI>(veh);
    if(obj && (obj->GetTargetSideAsIfAlive() == _side))
      _total += obj->GetStat();

    return false;
  }
};

GameValue AARGetStat(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
  if(!CheckType(state,oper2,GameString)) return NOTHING;
  const RString &dataType = oper2;

  if(oper1.GetType() == GameObject)
  {
    Object *obj = GetObject(oper1);
    EntityAI *entity = dyn_cast<EntityAI>(obj);

    // User passed in entity
    if(entity)
      return GetStatData(dataType,entity->GetStat());
  } 

  // User passed in group link
  if(oper1.GetType() == GameGroup )
  {
    AIGroup *grp = GetGroup(oper1);
    if(grp)
    {
      AARStat data;

      for(int i = 0; i < grp->NUnits(); ++i)
      {
        AIUnit *unit = grp->GetUnit(i);
        if(unit)
        {
          Person *person = unit->GetPerson();
          if(person)
            data += person->GetStat();// Add total group
        }
      }

      // Finaly return complete data;
      return GetStatData(dataType,data);
    }
  }

  // count up all the stats regarding the selected side
  if(oper1.GetType()==GameSide)
  {
    AARStat data;
    TargetSide sideWanted = GetSide(oper1);

    SumStatCountrySide sumSide(data,sideWanted);
    GWorld->ForEachVehicle(sumSide);
    GWorld->ForEachOutVehicle(sumSide);

    return GetStatData(dataType,data);
  }

  return 0.0f;
}
//////////////////////////////////////////////////////////////////////////
// End AAR Module
//////////////////////////////////////////////////////////////////////////

#endif

GameValue GetMarkers(const GameState *state)
{
  GameValue value = state->CreateGameValue(GameArray);
  GameArrayType &array = value;

  for (int i=0; i<markersMap.Size(); i++)
    array.Add(markersMap[i].name);

  array.Compact();
  return value;
}
/*
GameValue WaypointGetType( const GameState *state, GameValuePar oper1 )
{
  const GameArrayType &wp = oper1;
  if (wp.Size() != 2)
  {
    state->SetError(EvalGen);
    return RString();
  }

  AIGroup *grp = GetGroup(wp[0]);
  if (!grp) return RString();

  if (wp[1].GetType() != GameScalar)
  {
    state->SetError(EvalGen);
    return RString();
  }
  int index = toInt((float)wp[1]);

  if (index < 0 || index >= grp->NWaypoints()) return RString();
  WaypointInfo &wInfo = grp->GetWaypoint(index);
  const EnumName *types = GetEnumNames(wInfo.type);
  return types[wInfo.type].name;
}
*/
GameValue GroupSetVariable(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
  AIGroup *grp = GetGroup(oper1);
  if (!grp) return NOTHING;

  const GameArrayType &array = oper2;
  if (!CheckSize(state, array, 2)) return NOTHING;
  if (!CheckType(state, array[0], GameString)) return NOTHING;

  RString name = array[0];
  GameVarSpace *vars = grp->GetVars();
  if (vars) vars->VarSet(name, array[1]);

  return NOTHING;
}

GameValue GroupGetVariable(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
  AIGroup *grp = GetGroup(oper1);
  if (!grp) return NOTHING;

  RString name = oper2;
  GameVarSpace *vars = grp->GetVars();
  if (vars)
  {
    GameValue var;
    if (vars->VarGet(name, var)) return var;
  }
  return NOTHING;
}

static WaypointInfo *GetWaypoint(const GameState *state, GameValuePar oper, AIGroup **group = NULL, int *index = NULL)
{
  const GameArrayType &array = oper;
  if (!CheckSize(state, array, 2)) return NULL;
  if (!CheckType(state, array[0], GameGroup)) return NULL;
  if (!CheckType(state, array[1], GameScalar)) return NULL;

  AIGroup *grp = GetGroup(array[0]);
  if (!grp) return NULL;

  int i = toInt(safe_cast<float>(array[1]));
  if (i < 0) return NULL;
  if (i >= grp->NWaypoints()) return NULL;

  if (group) *group = grp;
  if (index) *index = i;

  return &grp->GetWaypoint(i);
}

GameValue WaypointSetVariable(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
  AIGroup *group = NULL;
  int index = -1;
  WaypointInfo *wp = GetWaypoint(state, oper1, &group, &index);
  if (!wp) return NOTHING;

  const GameArrayType &array = oper2;
  if (!CheckSize(state, array, 2)) return NOTHING;
  if (!CheckType(state, array[0], GameString)) return NOTHING;

  RString name = array[0];
  GameVarSpace *vars = &wp->vars;
  if (vars) vars->VarSet(name, array[1]);

  return NOTHING;
}

GameValue WaypointGetVariable(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
  AIGroup *group = NULL;
  int index = -1;
  WaypointInfo *wp = GetWaypoint(state, oper1, &group, &index);
  if (!wp) return NOTHING;

  RString name = oper2;
  GameVarSpace *vars = &wp->vars;
  if (vars)
  {
    GameValue var;
    if (vars->VarGet(name, var)) return var;
  }
  return NOTHING;
}
#if _VBS3
inline Detector *GetDetector(GameValuePar oper)
{
  Object *obj = GetObject(oper);
  return dyn_cast<Detector>(obj);
}
#endif

GameValue TriggerAttachObj(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
#if _VBS3 // quick attach objects to trigger
  Detector *det = GetDetector(oper1);
  Object *obj = GetObject(oper2);
  if (det && obj)
    det->AttachObject(obj);
#endif
  return NOTHING;
}

GameValue TriggerDetachObj(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
#if _VBS3 // quick attach objects to trigger
  Detector *det = GetDetector(oper1);
  Object *obj = GetObject(oper2);
  if (det && obj)
    det->DetachObject(obj);
#endif
  return NOTHING;
}

bool ParseUserParams(ParamFile &cfg, GameDataNamespace *globals);
static GameValue EntryToGameValue(const GameState *state, const IParamArrayValue &entry)
{
  if (entry.IsExpression())
  {
    GameVarSpace local(false); 
    state->BeginContext(&local);
    GameValue val = state->Evaluate(entry.GetValueRaw(), GameState::EvalContext::_default, GWorld->GetMissionNamespace());
    state->EndContext();
    return val;
  }
  else if (entry.IsTextValue())
    return entry.GetValueRaw();
  else if (entry.IsFloatValue() || entry.IsIntValue())
    return entry.GetFloat();
  else if (entry.IsArrayValue())
  {
    GameValue value = state->CreateGameValue(GameArray);
    GameArrayType &array = value;
    array.Resize(entry.GetItemCount());
    for (int i=0; i<entry.GetItemCount(); i++)
    {
      array[i] = EntryToGameValue(state, *entry.GetItem(i));
    }
    return value;
  }
  else
    return RString();
}
static GameValue EntryToGameValue(const GameState *state, ParamEntryPar entry)
{
  if (entry.IsExpression())
  {
    GameVarSpace local(false); 
    state->BeginContext(&local);
    GameValue val = state->Evaluate(entry.GetValueRaw(), GameState::EvalContext::_default, GWorld->GetMissionNamespace());
    state->EndContext();
    return val;
  }
  else if (entry.IsArray())
  {
    GameValue value = state->CreateGameValue(GameArray);
    GameArrayType &array = value;
    array.Resize(entry.GetSize());
    for (int i=0; i<entry.GetSize(); i++)
    {
      array[i] = EntryToGameValue(state, entry[i]);
    }
    return value;
  }
  else if (entry.IsClass())
    return RString();
  else if (entry.IsTextValue())
    return entry.GetValueRaw();
  else if (entry.IsFloatValue() || entry.IsIntValue())
    return (float)entry;
  else
    return RString();
}
GameValue ProfileLoadSetting(const GameState *state, GameValuePar oper1)
{
  if (!CheckType(state, oper1, GameString)) return NOTHING;
  GameDataNamespace globals(NULL, RString(), false); // save - no global namespace
  ParamFile cfg;
  ParseUserParams(cfg, &globals);
  RString toFind = oper1;
  ConstParamEntryPtr entry = cfg.FindEntry(toFind);
  if (entry)
    return EntryToGameValue(state, *entry);
  return NOTHING;
}

class EachCrewWeight : public ICrewFunc
{
public:
  float _totalWeight;
  EachCrewWeight():_totalWeight(0.0){};
  virtual bool operator () (Person *person)
  {
    if(person)
      _totalWeight += person->GetTotalWeightKg();

    return false;
  }
};

GameValue GetTotalWeightKg(const GameState *state, GameValuePar oper1)
{
  float totalWeightKg = 0.0f;
  if(!CheckType(state,oper1,GameObject)) return NOTHING;
  Object *object = GetObject(oper1);
  EntityAI  *entity = dyn_cast<EntityAI>(object);
  Transport *tran = dyn_cast<Transport>(entity);




  // get both the vehicle
  // and people inside the vehicle
  if(tran)
  {
    EachCrewWeight crew;
    tran->ForEachCrew(crew);

    totalWeightKg += tran->GetTotalWeightKg();
    totalWeightKg += crew._totalWeight;
  }
  else if(entity)
  {
    totalWeightKg += entity->GetTotalWeightKg();
  }

  return totalWeightKg; 
}


GameValue GetAttachedObj(const GameState *state, GameValuePar oper1)
{
  GameValue value = state->CreateGameValue(GameArray);
  GameArrayType &array = value;

  if (!CheckType(state, oper1, GameObject)) return NOTHING;

  Object *objHolder = GetObject(oper1);
  if (!objHolder) return value;

 const AutoArray< Ref<Object> >& attachedObjects = objHolder->GetAttachedObjs();
  for (int i=0; i<attachedObjects.Size(); i++)
  {
    Object *obj = attachedObjects[i];
    if (obj) array.Add(GameValueExt(obj));
  }
  return value;
}

GameValue TriggerGetAttachedObj(const GameState *state, GameValuePar oper1)
{
  GameValue value = state->CreateGameValue(GameArray);
  GameArrayType &array = value;

#if _VBS3 // quick attach objects to trigger
  Detector *det = GetDetector(oper1);
  if (!det) return value;

  OLinkArray(Object) attachedObjects = det->GetAttachedObjs();
  for (int i=0; i<attachedObjects.Size(); i++)
  {
    Object *obj = attachedObjects[i];
    if (obj) array.Add(GameValueExt(obj));
  }
#endif
  return value;
}

GameValue TriggerSetActivationEH( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
#if _VBS3
  if (!CheckType(state, oper1, GameObject)) return NOTHING;
  if (!CheckType(state, oper2, GameString)) return NOTHING;

  Detector *det = GetDetector(oper1);
  if (det)
    return (float)det->SetOnActivateEH(oper2);
#endif
  return NOTHING;
}

GameValue TriggerClearActivationEH( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
#if _VBS3
  if (!CheckType(state, oper1, GameObject)) return NOTHING;
  if (!CheckType(state, oper2, GameScalar)) return NOTHING;

  int i = toInt((float)oper2);

  Detector *det = GetDetector(oper1);
  if (det)
    det->ClearOnActivateEH(i);
#endif
  return NOTHING;
}


GameValue DisableGunnerInput(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
  Object *obj = GetObject(oper1);
  if (!obj) return NOTHING;

  Transport *veh = dyn_cast<Transport>(obj);
  if (!veh) return NOTHING;

  // Grab the array
  const GameArrayType &array = oper2;
  if (!CheckSize(state, array, 2)) return NOTHING;

  // Get the turret path
  if(!CheckType(state, array[0], GameArray)) return NOTHING;
  const GameArrayType &subarray = array[0];
  int n = subarray.Size();
  for(int i=0; i<n; i++)
    if (!CheckType(state, subarray[i], GameScalar)) return NOTHING;

  if(!CheckType(state, array[1], GameBool)) return NOTHING;

  // Get the path
  TurretPath path;
  path.Resize(n);
  for (int i=0; i<n; i++) path[i] = toInt((float)subarray[i]);
  Turret *turret = veh->GetTurret(path.Data(), path.Size());
  if (!turret) return NOTHING;

  if(turret->IsLocal())
  {
    turret->_disableGunnerInput = array[1];
  }

  return NOTHING;
}
GameValue ObjSetTurretLockOn(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
  Object *obj = GetObject(oper1);
  if (!obj) return NOTHING;

  Transport *veh = dyn_cast<Transport>(obj);
  if (!veh) return NOTHING;

  // Grab the array
  const GameArrayType &array = oper2;
  if(array.Size() < 2)
  {
    state->SetError(EvalDim, array.Size(), 2);
    return NOTHING;
  }

  // Get the weapon path
  Vector3 lockOnPos = VZero;
  Object* lockOn = NULL;

  if(!CheckType(state, array[0], GameArray)) return NOTHING;

  Turret *turret = NULL;
  {
    const GameArrayType &subarray = array[0];
    int n = subarray.Size();
    for(int i=0; i<n; i++)
      if (!CheckType(state, subarray[i], GameScalar)) return NOTHING;
    // Get the path
    TurretPath path;
    path.Resize(n);
    for (int i=0; i<n; i++) path[i] = toInt((float)subarray[i]);
    turret = veh->GetTurret(path.Data(), path.Size());
    if (!turret) return NOTHING;
  }

  if(array[1].GetType() == GameArray) 
  {
    const GameArrayType &subarray = array[1];
    int n = subarray.Size();
    if(n != 3) return NOTHING;
    for(int i=0; i<n; i++)
      if (!CheckType(state, subarray[i], GameScalar)) return NOTHING;
    lockOnPos[0] = subarray[0];
    lockOnPos[1] = subarray[2];
    lockOnPos[2] = subarray[1];
  }
  else
    if(array[1].GetType() == GameObject)
    {
      lockOn = GetObject(array[1]);
    }
    else
      return NOTHING;

  if(turret->IsLocal())
  {
    if(lockOn)
      turret->SetLockOn(lockOn);
    else
      turret->SetLockOn(lockOnPos);
  }
  return NOTHING;
}
// obj SetTurretWeaponDirection [[path],[azimuth, elevation]<,transition=true>]
GameValue ObjSetTurretWeaponDirection(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
  Object *obj = GetObject(oper1);
  if (!obj) return NOTHING;

  Transport *veh = dyn_cast<Transport>(obj);
  if (!veh) return NOTHING;

  // Grab the array
  const GameArrayType &array = oper2;
  if(array.Size() < 2)
  {
    state->SetError(EvalDim, array.Size(), 2);
    return NOTHING;
  }

  // Get the weapon path
  if(!CheckType(state, array[0], GameArray)) return NOTHING;
  const GameArrayType &subarray = array[0];
  int n = subarray.Size();
  for(int i=0; i<n; i++)
    if (!CheckType(state, subarray[i], GameScalar)) return NOTHING;

  if (!CheckType(state, array[1], GameArray)) return NOTHING;
  if (!CheckSize(state, array[1],2)) return NOTHING;
  const GameArrayType &weaponDir = array[1];
  for(int i=0; i<weaponDir.Size(); i++)
    if(!CheckType(state, weaponDir[i], GameScalar)) return NOTHING;

  bool transition = true;
  if(array.Size() > 2)
  {
    if (!CheckType(state, array[2], GameBool)) return NOTHING;
    transition = array[2];
  }
  // Get the path
  TurretPath path;
  path.Resize(n);
  for (int i=0; i<n; i++) path[i] = toInt((float)subarray[i]);
  Turret *turret = veh->GetTurret(path.Data(), path.Size());
  if (!turret) return NOTHING;

  if(turret->IsLocal())
  {
    float azimuth = float(weaponDir[0]) * PI / 180.0f;
    float elev = float(weaponDir[1]) * PI / 180.0f;
    turret->AimWorld(azimuth,elev,!transition);
  }
  return NOTHING;
}


GameValue ObjGetTurretWeaponDirection(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
  GameValue value = state->CreateGameValue(GameArray);
  GameArrayType &rArray = value;
  rArray.Resize(3);
  rArray[0] = 0.0f; 
  rArray[1] = 0.0f; 
  rArray[2] = 0.0f;

  Object *obj = GetObject(oper1);
  if (!obj) return NOTHING;
  
  Transport *veh = dyn_cast<Transport>(obj);
  if (!veh) return NOTHING;

  // Grab the array
  const GameArrayType &array = oper2;
  if(array.Size() < 1)
  {
    state->SetError(EvalDim, array.Size(), 1);
  }
  
    // Get the weapon path
  if (!CheckType(state, array[0], GameArray)) return NOTHING;
  const GameArrayType &subarray = array[0];
  int n = subarray.Size();
  for (int i=0; i<n; i++)
    if (!CheckType(state, subarray[i], GameScalar)) return NOTHING;

  // get the path
  TurretPath path;
  path.Resize(n);
  for (int i=0; i<n; i++) path[i] = toInt((float)subarray[i]);
  Turret *turret = veh->GetTurret(path.Data(), path.Size());
  if (!turret) return NOTHING;

  TurretContext context;
  veh->FindTurret(turret,context);

  if(array.Size() > 1)
  {
    // Get the weaponName
    if(!CheckType(state,array[1], GameString)) return NOTHING;
    RString weaponName = array[1];

  for (int i=0; i<context._weapons->_magazineSlots.Size(); i++)
  {
    const MagazineSlot &slot = context._weapons->_magazineSlots[i];
    if (stricmp(slot._weapon->GetName(), weaponName) == 0)
    {
      Vector3 dir = veh->GetWeaponDirection(context, i);    
      rArray[0] = dir.X(); 
      rArray[1] = dir.Z(); 
      rArray[2] = dir.Y();

      break;
    }
  }
  }
  else
  {
    Vector3 dir = veh->GetWeaponDirection(context, -1); 
    rArray[0] = dir.X(); 
    rArray[1] = dir.Z(); 
    rArray[2] = dir.Y();
  }
  return value;
}

#if _ENABLE_CHEATS
GameValue CommanderOverride(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{

  if(!CheckType(state,oper1, GameObject)) return NOTHING;

  Object *obj = GetObject(oper1);
  Person* person = dyn_cast<Person>(obj); //can be NULL!

  // Grab the array
  const GameArrayType &array = oper2;
  if(!CheckSize(state, array, 2)) return NOTHING;

  if(!CheckType(state, array[0], GameObject)) return NOTHING;

  Object *obj2 = GetObject(array[0]);
  Transport *veh = dyn_cast<Transport>(obj2);
  if (!veh) return NOTHING;

  // Get the turret path
  if (!CheckType(state, array[1], GameArray)) return NOTHING;
  const GameArrayType &subarray = array[1];
  int n = subarray.Size();
  for (int i=0; i<n; i++)
    if (!CheckType(state, subarray[i], GameScalar)) return NOTHING;

  // get the path
  TurretPath path; path.Resize(n);
  for (int i=0; i<n; i++) path[i] = toInt((float)subarray[i]);
  Turret *turret = veh->GetTurret(path.Data(), path.Size());
  if (!turret) return NOTHING;

  if(person)
    turret->StartOverride();
  else
    turret->StopOverride();

  return NOTHING;
}
#endif //_ENABLE_CHEATS

GameValue TriggerGetSynchronizedWith( const GameState *state, GameValuePar oper1 )
{
  GameValue value = state->CreateGameValue(GameArray);

#if _VBS3 && _VBS_WP_SYNCH // waypointSynchronizedWith
  Detector *det = GetDetector(oper1);
  if (!det) return NOTHING;

  GameArrayType &array = value;
  for (int i=0; i<synchronized.Size(); i++)
  {
    SynchronizedItem &sync = synchronized[i];
    DoAssert(sync.sensors.Size() <= 1);
    if (sync.sensors.Size() == 1 && sync.sensors[0].sensor == det && sync.groups.Size() == 1)
    {
      GameValue subValue = state->CreateGameValue(GameArray);
      array.Add(subValue);
      GameArrayType &subArray = subValue;
      subArray.Add(GameValueExt(sync.groups[0].group));
      subArray.Add((float)sync.wpIndex);
    }
  }
#endif
  return value;
}

GameValue WaypointGetSynchronizedWith( const GameState *state, GameValuePar oper1 )
{
  GameValue value = state->CreateGameValue(GameArray);  

#if _VBS3 && _VBS_WP_SYNCH // waypointSynchronizedWith
  // debug
  /*
  for (int i=0; i<synchronized.Size(); i++)
  {
    SynchronizedItem &sync = synchronized[i];
    
    if (sync.sensors.Size() == 1)
      LogF("[p] wp - sensor [0] %s | idx %d",sync.sensors[0].sensor->GetDebugName().Data(),sync.wpIndex);
 
    if (sync.groups.Size() == 1)
      LogF("[p] wp - wp     [0] %s | idx %d",sync.groups[0].group->GetDebugName().Data(),sync.wpIndex);
    else
    {
      if (sync.groups.Size() == 0)
        LogF("[p] wp - wp     ERROR 1!");
      else
        LogF("[p] wp - wp     [0] %s | [1] %s | idx %d",sync.groups[0].group->GetDebugName().Data(),sync.groups[1].group->GetDebugName().Data(),sync.wpIndex);
    }
  }
  */

  AIGroup *group = NULL;
  int index = -1;
  WaypointInfo *wp = GetWaypoint(state, oper1, &group, &index);
  if (!wp) return value;

  GameArrayType &array = value;

  for (int i=0; i<wp->synchronizations.Size(); i++)
  {
    int sIndex = wp->synchronizations[i];
    SynchronizedItem &sync = synchronized[sIndex];

    // synchronization waypoint - trigger
    if (sync.sensors.Size() == 1)
    {      
      array.Add(GameValueExt(sync.sensors[0].sensor));
      continue;
    }

    if (sync.groups.Size() < 2)
      continue;

    // synchronization waypoint - waypoint
      // maintained by second waypoint
    if (sync.groups[1].group != group)
    {      
      GameValue subValue = state->CreateGameValue(GameArray);
      array.Add(subValue);
      GameArrayType &subArray = subValue;
      subArray.Add(GameValueExt(sync.groups[1].group));
      subArray.Add((float)sync.wpIndex);
    }    
  }
  return value;
#else
  return value;
#endif
}

GameValue WaypointGetCurrentVBS( const GameState *state, GameValuePar oper1 )
{
  AIGroup *grp = GetGroup(oper1);
  if (!grp) return 0.0f;

  int &index = grp->GetCurrent()->_fsm->Var(0);
  return (float)index;
}

GameValue WaypointGetTotalNumber( const GameState *state, GameValuePar oper1 )
{
  AIGroup *grp = GetGroup(oper1);
  if (!grp) return 0.0f;

  return (float)grp->NWaypoints();
}


ControlsContainer *GetDisplay(GameValuePar oper);

GameValue UISetCursorType( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
  ControlsContainer *display = GetDisplay(oper1);
  if (!display) return NOTHING;
  
  if (oper2.GetType() != GameString)
  {
    state->TypeError(GameArray, oper2.GetType());
    return NOTHING;
  }

  RString type = oper2;
  if (type.GetLength() == 0)
    display->SetCursor(NULL);
  else
    display->SetCursor(type);

  display->DrawCursor();

  return NOTHING;
}

GameValue UIGet3DDir( const GameState *state, GameValuePar oper1 )
{
  GameValue value = state->CreateGameValue(GameArray);  
  GameArrayType &array = value;
  const GameArrayType &Array = oper1;
  if(Array.Size() < 2)
  {
      state->SetError(EvalDim, Array.Size(), 2);
    return OBJECT_NULL;
  }
  if (Array[0].GetType() != GameScalar)
  {
    state->TypeError(GameScalar, Array[0].GetType());
    return OBJECT_NULL;
  }
  if (Array[1].GetType() != GameScalar)
  {
    state->TypeError(GameScalar, Array[1].GetType());
    return OBJECT_NULL;
  }

  float x = Array[0];
  float y = Array[1];

  Camera& camera = *GScene->GetCamera();
  float posX = x * camera.Left();
  float posY = -y * camera.Top();

  Vector3 pos = Vector3(posX, posY, 1);
  Vector3 cursorDir = camera.DirectionModelToWorld(pos);

  array.Resize(3);
  array[0] = cursorDir.X();
  array[1] = cursorDir.Z();
  array[2] = cursorDir.Y();
  return value;
}

GameValue CreateShot( const GameState *state, GameValuePar oper1 )
{
#if _LASERSHOT
  const GameArrayType &array = oper1;
  if(array.Size() != 3)
  {
    state->SetError(EvalDim, array.Size(), 3);
    return OBJECT_NULL;
  }
  if (array[0].GetType() != GameString)
  {
    state->TypeError(GameString, array[0].GetType());
    return OBJECT_NULL;
  }
  if (array[1].GetType() != GameArray)
  {
    state->TypeError(GameArray, array[1].GetType());
    return OBJECT_NULL;
  }
  if (array[2].GetType() != GameArray)
  {
    state->TypeError(GameArray, array[2].GetType());
    return OBJECT_NULL;
  }

  RString typeName = array[0];

  Vector3 pos;
  GetPos(pos,array[1]);

  Vector3 vel;
  GetPos(vel,array[2]);
  
  Ref<EntityType> type = VehicleTypes.New(typeName);
  AmmoType *aType = dynamic_cast<AmmoType *>(type.GetRef());
  if (!aType) return OBJECT_NULL;

  Ref<Shot> shot = NewShot(NULL, aType, NULL);
  if (!shot) return OBJECT_NULL;

  shot->SetOrient(vel,VUp);
  shot->SetSpeed(vel);
  shot->SetPosition(pos);
  GWorld->AddFastVehicle(shot);
  GWorld->AddSupersonicSource(shot);

  if (GWorld->GetMode() == GModeNetware)
    GetNetworkManager().CreateObject(shot);

  Ref<Entity> veh = dyn_cast<Entity>(shot.GetRef());
  return GameValueExt(veh);
#else
  return NOTHING;
#endif
}

GameValue MarkerGetShape( const GameState *state, GameValuePar oper1 )
{
  GameStringType name = oper1;
  for (int i=0; i<markersMap.Size(); i++)
  {
    ArcadeMarkerInfo &mInfo = markersMap[i];
    if (stricmp(mInfo.name, name) == 0)
    {
      const EnumName *names = GetEnumNames(mInfo.markerType);
      for (int i=0; names[i].IsValid(); i++)
        if (names[i].value == mInfo.markerType) return names[i].name;
    }
  }
  return GameStringType("");
}

GameValue MarkerGetBrush( const GameState *state, GameValuePar oper1 )
{
  GameStringType name = oper1;
  for (int i=0; i<markersMap.Size(); i++)
  {
    ArcadeMarkerInfo &mInfo = markersMap[i];
    if (stricmp(mInfo.name, name) == 0)
      return mInfo.fillName;
  }
  return GameStringType("");
}

GameValue MarkerGetAutoSize( const GameState *state, GameValuePar oper1 )
{
  GameStringType name = oper1;
  for (int i=0; i<markersMap.Size(); i++)
  {
    ArcadeMarkerInfo &mInfo = markersMap[i];
    if (stricmp(mInfo.name, name) == 0)
      return mInfo.autosize;
  }
  return GameBoolType(true);
}

#if _VBS3 // marker EH
//! expression callback when a marker is created
RString GMarkerOnCreated;
//! expression callback when a marker is deleted
RString GMarkerOnDeleted;
//!
void ProcessCreateMarkerEH(RString name, bool updated = true)
{
  if (GMarkerOnCreated.GetLength() > 0)
  {
    GameState *state = GWorld->GetGameState();
    GameVarSpace vars(false);
    state->BeginContext(&vars);
    GameValue value = state->CreateGameValue(GameArray);
    GameArrayType &array = value;
    array.Realloc(2);
    array.Add(name); // marker name
    array.Add(updated); // updated?
    state->VarSetLocal("_this", value, true);
    state->EvaluateMultiple(GMarkerOnCreated, GameState::EvalContext::_default, GWorld->GetMissionNamespace());
    state->EndContext();
  }
}
//-!
void ProcessDeleteMarkerEH(RString name)
{
  if (GMarkerOnDeleted.GetLength() > 0)
  {
    GameState *state = GWorld->GetGameState();
    GameVarSpace vars(false);
    state->BeginContext(&vars);
    GameValue value = state->CreateGameValue(GameArray);
    GameArrayType &array = value;
    array.Realloc(1);
    array.Add(name); // marker name
    state->VarSetLocal("_this", value, true);
    state->EvaluateMultiple(GMarkerOnDeleted, GameState::EvalContext::_default, GWorld->GetMissionNamespace());
    state->EndContext();
  }
}
#define MARKER_EH(name) ProcessCreateMarkerEH(name);
#else
#define MARKER_EH(name)
#endif

#if _VBS3 // GVehicleOnCreated
//! expression callback when a vehicle or object is created
RString GVehicleOnCreated;
//-!
void ProcessCreateVehicleEH(Entity *veh, bool isSlowVehicle)
{
  if (GVehicleOnCreated.GetLength() > 0)
  {
    GameState *state = GWorld->GetGameState();
    GameVarSpace vars(false);
    state->BeginContext(&vars);
    GameValue value = state->CreateGameValue(GameArray);
    GameArrayType &array = value;
    array.Realloc(2);
    array.Add(GameValueExt(veh));
    array.Add(isSlowVehicle);
    state->VarSetLocal("_this", value, true);
    state->EvaluateMultiple(GVehicleOnCreated, GameState::EvalContext::_default, GWorld->GetMissionNamespace());
    state->EndContext();
  }
}
#endif

GameValue MapOnVehicleCreated( const GameState *state, GameValuePar oper1 )
{
#if _VBS3 // GVehicleOnCreated
  GVehicleOnCreated = oper1;
#endif
  return NOTHING;
}

GameValue MarkerSetAutoSize(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
  GameStringType name = oper1;
  for (int i=0; i<markersMap.Size(); i++)
    if (stricmp(markersMap[i].name, name) == 0)
    {
      markersMap[i].autosize = oper2; MARKER_EH(name)
      break;
    }
  return NOTHING;
}

GameValue MarkerSetAutoSizeLocal(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
  GameStringType name = oper1;
  for (int i=0; i<markersMap.Size(); i++)
    if (stricmp(markersMap[i].name, name) == 0)
    {
      markersMap[i].autosize = oper2; MARKER_EH(name)
      RefArray<NetworkObject> dummy;
      GetNetworkManager().MarkerCreate(CCGlobal, NULL, dummy, markersMap[i]);
      break;
    }
  return NOTHING;
}

GameValue MarkerGetCondition( const GameState *state, GameValuePar oper1 )
{
  GameStringType name = oper1;
  for (int i=0; i<markersMap.Size(); i++)
  {
    ArcadeMarkerInfo &mInfo = markersMap[i];
    if (stricmp(mInfo.name, name) == 0)
      return mInfo.condition;
  }
  return GameStringType("");
}

GameValue MarkerSetCondition(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
  GameStringType name = oper1;
  for (int i=0; i<markersMap.Size(); i++)
    if (stricmp(markersMap[i].name, name) == 0)
    {
      markersMap[i].condition = oper2; MARKER_EH(name)
      RefArray<NetworkObject> dummy;
      GetNetworkManager().MarkerCreate(CCGlobal, NULL, dummy, markersMap[i]);
      break;
    }
  return NOTHING;
}

GameValue MarkerGetLayers( const GameState *state, GameValuePar oper1 )
{
  GameValue value = state->CreateGameValue(GameArray);  
  GameArrayType &array = value;
  GameStringType name = oper1;
  for (int i=0; i<markersMap.Size(); i++)
    if (stricmp(markersMap[i].name, name) == 0)
    {
      ArcadeMarkerInfo &mInfo = markersMap[i];
      array.Resize(mInfo.layeredIcons.Size());
      for (int j=0; j<mInfo.layeredIcons.Size(); j++)
      {        
        LayeredMarkerInfo *layeredIcon = mInfo.layeredIcons[j];
        if (!layeredIcon->icon) continue;

        GameValue value = state->CreateGameValue(GameArray);  
        GameArrayType &layerArray = value;
        array[j] = value;
        layerArray.Resize(3);        

        // texture         
        layerArray[0] = "\\" + layeredIcon->icon->GetName();
        
        // size
        value = state->CreateGameValue(GameArray);  
        GameArrayType &sizeArray = value;
        layerArray[1] = value;
        sizeArray.Resize(2);
        sizeArray[0] = layeredIcon->a;
        sizeArray[1] = layeredIcon->b;

        // offset
        value = state->CreateGameValue(GameArray);  
        GameArrayType &offsetArray = value;
        layerArray[2] = value;
        offsetArray.Resize(2);
        offsetArray[0] = layeredIcon->x;
        offsetArray[1] = layeredIcon->y;
      }
      break;
    }
  return value;
}

RString GetPictureName( RString baseName );
GameValue MarkerSetLayersLocal(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
  GameStringType name = oper1;
  for (int i=0; i<markersMap.Size(); i++)
    if (stricmp(markersMap[i].name, name) == 0)
    {
      if (oper2.GetType() != GameArray)
      {
        state->TypeError(GameString, oper2.GetType());
        return NOTHING;
      }

      // type checking
      const GameArrayType &array = oper2;
      for (int j=0; j<array.Size(); j++)
      {
        if (!CheckType(state, array[j], GameArray)) 
        {
          state->TypeError(GameArray, array[j].GetType());
          return NOTHING;
        }
        const GameArrayType &subArray = array[j];
        if (!CheckSize(state, subArray, 3)) return NOTHING; // ["texture",[size],[offset]]
        if (!CheckType(state, subArray[0], GameString)) // "texture"
        {
          state->TypeError(GameString, subArray[0].GetType());
          return NOTHING;
        }
        for (int k=1; k<subArray.Size(); k++)   // [size],[offset]
        {
          if (!CheckType(state, subArray[k], GameArray)) 
          {            
            state->TypeError(GameArray, subArray[k].GetType());
            return NOTHING;
          }
          const GameArrayType &subSubArray = subArray[k];
          if (!CheckSize(state, subSubArray, 2)) return NOTHING;
          for (int l=0; l<subSubArray.Size(); l++)    // [scalar, scalar]
          {
            if (!CheckType(state, subSubArray[l], GameScalar)) 
            {            
              state->TypeError(GameScalar, subSubArray[l].GetType());
              return NOTHING;
            }
          }
        }
      }
      //-!

      markersMap[i].layeredIcons.Clear();
      for (int j=0; j<array.Size(); j++)
      {
        const GameArrayType &subArray = array[j];

        int k = markersMap[i].layeredIcons.Add(new LayeredMarkerInfo());
        LayeredMarkerInfo *newLayer = markersMap[i].layeredIcons[k];

        // texture
        newLayer->icon = GlobLoadTexture
        (
          GetPictureName(subArray[0])
        );

        // size
        const GameArrayType &sizeArray = subArray[1];
        newLayer->a = sizeArray[0];
        newLayer->b = sizeArray[1];

        // offset
        const GameArrayType &offsetArray = subArray[2];
        newLayer->x = offsetArray[0];
        newLayer->y = offsetArray[1];
      }

      MARKER_EH(name)

      break;
    }
  return NOTHING;
}

GameValue MarkerSetLayers(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
  MarkerSetLayersLocal(state,oper1,oper2);
  GameStringType name = oper1;
  for (int i=0; i<markersMap.Size(); i++)
    if (stricmp(markersMap[i].name, name) == 0)
    {
      RefArray<NetworkObject> dummy;
      GetNetworkManager().MarkerCreate(CCGlobal, NULL, dummy, markersMap[i]);
      break;
    }
  return NOTHING;
}

GameValue MarkerGetAttach( const GameState *state, GameValuePar oper1 )
{
  GameValue value = state->CreateGameValue(GameArray);  
  GameArrayType &array = value;
  GameStringType name = oper1;
  for (int i=0; i<markersMap.Size(); i++)
  {
    ArcadeMarkerInfo &mInfo = markersMap[i];
    if (stricmp(mInfo.name, name) == 0)
    {
      array.Resize(3);
      array[0] = mInfo.attachCondition;

      GameValue value = state->CreateGameValue(GameArray);  
      GameArrayType &pos = value;
      pos.Resize(2);
      pos[0] = mInfo.attachOffset.X();
      pos[1] = mInfo.attachOffset.Y();
      array[1] = value;

      GameValue valueObjs = state->CreateGameValue(GameArray);  
      GameArrayType &objs = valueObjs;
      for (int i=0; i<mInfo.attachCtx.Size(); i++)
      {
        Object *obj = mInfo.attachCtx[i];
        if (obj)
          objs.Add(GameValueExt(obj));
      }
      array[2] = valueObjs;

      break;
    }
  }
  return value;
}

GameValue MarkerSetAttachLocal(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
  GameStringType name = oper1;
  for (int i=0; i<markersMap.Size(); i++)
    if (stricmp(markersMap[i].name, name) == 0)
    {
      const GameArrayType &array = oper2;
      if(array.Size() < 2 || array.Size() > 3)
      {
        state->SetError(EvalDim, array.Size(), 2);
        return NOTHING;
      }
      if (array[0].GetType() != GameString)
      {
        state->TypeError(GameString, array[0].GetType());
        return NOTHING;
      }
      if (array[1].GetType() != GameArray)
      {
        state->TypeError(GameArray, array[1].GetType());
        return NOTHING;
      }
      const GameArrayType &offset = array[1];
      if (!CheckSize(state, offset, 2)) return NOTHING;
      for (int j=0; j<2; j++)
        if (!CheckType(state, offset[j], GameScalar)) return NOTHING;

      // check context objects
      if (array.Size() > 2)
      {
        if (array[2].GetType() != GameArray)
        {
          state->TypeError(GameArray, array[2].GetType());
          return NOTHING; 
        }
        const GameArrayType &objArray = array[2];
        for (int j=0; j<objArray.Size(); j++)
        {
          if (objArray[j].GetType() != GameObject)
          {
            state->TypeError(GameObject, objArray[j].GetType());
            return NOTHING; 
          }
        }
      }

      // context
      OLinkArray(Object) &objs = markersMap[i].attachCtx;
      objs.Clear();
      if (array.Size() > 2)
      {
        //! ** Prevention of ctd because of optimisation **
        if(!CheckType(state,array[2],GameArray)) return NOTHING;

        const GameArrayType &objArray = array[2];
        for (int j=0; j<objArray.Size(); j++)
        {
          int n = objs.Add();
          objs[n] = static_cast<GameDataObject *>(objArray[j].GetData())->GetObject();
        }
      }

      markersMap[i].attachOffset[0] = offset[0];
      markersMap[i].attachOffset[2] = offset[1];
      markersMap[i].attachCondition = array[0];
      MARKER_EH(name)
      break;
    }
  return NOTHING;
}

GameValue MarkerSetAttach (const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
  MarkerSetAttachLocal(state,oper1,oper2);
  GameStringType name = oper1;
  for (int i=0; i<markersMap.Size(); i++)
    if (stricmp(markersMap[i].name, name) == 0)
    {
      RefArray<NetworkObject> dummy;
      GetNetworkManager().MarkerCreate(CCGlobal, NULL, dummy, markersMap[i]);
      break;
    }
  return NOTHING;
}

GameValue MarkerCreateConditional(const GameState *state, GameValuePar oper1)
{
  const GameArrayType &array = oper1;
  if (!CheckSize(state, array, 3)) return NOTHING;
  if (!CheckType(state, array[0], GameString)) return NOTHING;

  GameStringType name = array[0];
  Vector3 pos;
  if (!GetPos(state, pos, array[1])) return RString();

  if (!CheckType(state, array[2], GameString)) return NOTHING;
  RString condition = array[2];

  for (int i=0; i<markersMap.Size(); i++)
    if (stricmp(markersMap[i].name, name) == 0) return RString();

  int index = markersMap.Add();
  ArcadeMarkerInfo &marker = markersMap[index];
  marker.name = name;
  marker.position = pos;
  marker.condition = condition;

  RefArray<NetworkObject> dummy;
  GetNetworkManager().MarkerCreate(CCGlobal, NULL, dummy, marker);

  return name;
}

static GroupEvent GetEvent(const GameValue &oper)
{
  RString name = oper;
  int event = GetEnumValue<GroupEvent>((const char *)name);
  if (event<0 || event>=NGroupEvent) return NGroupEvent;
  return (GroupEvent)event;
}

GameValue GrpAddEventHandler(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
  const GameArrayType &array = oper2;
  if (array.Size() != 2)
  {
    state->SetError(EvalDim,array.Size(),2);
    return -1.0f;
  }
  if (array[0].GetType() != GameString)
  {
    state->TypeError(GameString, array[0].GetType());
    return -1.0f;
  }
#if USE_PRECOMPILATION
  if (array[1].GetType() != GameString && array[1].GetType() != GameCode)
  {
    state->TypeError(GameString, array[1].GetType());
    return -1.0f;
  }
#else
  if (array[1].GetType() != GameString)
  {
    state->TypeError(GameString, array[1].GetType());
    return -1.0f;
  }
#endif
  AIGroup *grp = GetGroup(oper1);
  GroupEvent event = GetEvent(array[0]);
  RString handler = array[1];
  if (grp && event<NGroupEvent)
  {
    return (float)grp->AddEventHandler(event,handler);
  }
  return -1.0f;
  
}
GameValue GrpRemoveEventHandler(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
  const GameArrayType &array = oper2;
  if (array.Size() != 2)
  {
    state->SetError(EvalDim,array.Size(),2);
    return NOTHING; 
  }
  if (array[0].GetType() != GameString)
  {
    state->TypeError(GameString, array[0].GetType());
    return NOTHING; 
  }
  if (array[1].GetType() != GameScalar)
  {
    state->TypeError(GameScalar, array[1].GetType());
    return NOTHING; 
  }
  AIGroup *grp = GetGroup(oper1);
  GroupEvent event = GetEvent(array[0]);
  int handle = toInt((float)array[1]);
  if (grp && event<NGroupEvent)
  {
    grp->RemoveEventHandler(event,handle);
  }
  return NOTHING; 
}

GameValue GrpRemoveAllEventHandlers(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
  AIGroup *grp = GetGroup(oper1);
  GroupEvent event = GetEvent(oper2);
  if (grp && event<NGroupEvent)
  {
    grp->ClearEventHandlers(event);
  }
  return NOTHING;
}

static float GetMinY(LODShape *shape)
{
  if (!shape) return 0;
  const Shape *geom = shape->LandContactLevel() ? shape->LandContactLevel() : shape->GeometryLevel();
  if (geom)
  {
    return geom->Min().Y();
  }
  else if (shape->NLevels()>0)
  {
    ShapeUsed level0 = shape->Level(0);
    return level0->Min().Y();
  }
  else
  {
    return shape->Min().Y();
  }
}

//! find description of resource in configuration files
/*!
  Description is class in configuaration file
  Order of searching:
  - description.ext of current mission
  - description.ext of current campaign
  - global resource
  \param name name of class
  \return description class
*/
static ConstParamEntryPtr FindResource(RString name)
{
  // find in mission
  ConstParamEntryPtr entry = ExtParsMission.FindEntry(name);
  if (entry) return entry;
  
  // find in campaign
  entry = ExtParsCampaign.FindEntry(name);
  if (entry) return entry;

  // find in resource
  entry = Pars.FindEntry(name);
  if (entry) return entry;

  WarningMessage("Resource %s not found", (const char *)name);
  return ConstParamEntryPtr();
}

static ControlsContainer *GetUserDialog()
{
  ControlsContainer *dlg = dynamic_cast<ControlsContainer *>(GWorld->UserDialog());
  if (dlg)
  {
    while (dlg->Child()) dlg = dlg->Child();
  }
  return dlg;
}

GameValue EditorCreate( const GameState *state, GameValuePar oper1 )
{
#if _VBS2_LITE
  return NOTHING;
#else
  const GameArrayType &array = oper1;
  if (array.Size() != 3)
  {
    state->SetError(EvalDim,array.Size(),3);
    return NOTHING; 
  }

  // dlg name
  if (array[0].GetType() != GameString)
  {
    state->TypeError(GameString, array[0].GetType());
    return NOTHING; 
  }
  GameStringType name = array[0];
  ConstParamEntryPtr cls = FindResource(name);
  if (!cls) return NOTHING;

  // init script
  if (array[1].GetType() != GameString)
  {
    state->TypeError(GameString, array[1].GetType());
    return NOTHING; 
  }
  RString init = array[1];

  // import script
  if (array[2].GetType() != GameString)
  {
    state->TypeError(GameString, array[2].GetType());
    return NOTHING; 
  }
  RString import = array[2];

  ControlsContainer *parent = GetUserDialog();
  Display *editor = CreateMissionEditorRealTime(parent,name,init,import);
  GWorld->SetUserDialog(editor);

  return GameValueExt(editor);
#endif
}

GameValue CamResetTargets( const GameState *state, GameValuePar oper1 )
{
  CameraVehicle *cam = dyn_cast<CameraVehicle>(GetObject(oper1));
  if( !cam ) return NOTHING;
  cam->ResetTargets();
  return NOTHING;
}

IControl *GetControl(GameValuePar oper);

GameValue SetViewPar( const GameState *state, GameValuePar oper1 )
{
#if _AAR //otherwise we don't have history lines
  const GameArrayType &array = oper1;
  if (array.Size() < 2 || array.Size() > 3)
  {
    state->SetError(EvalDim,array.Size(),2);
    return NOTHING; 
  }

  if(!CheckType(state, array[0], GameString)) return NOTHING; 

  RString type = array[0]; type.Lower();

  if(strcmpi(type,"trail")==0)
  {
    if(!CheckType(state, array[1], GameBool)) return NOTHING; 
    
    bool enable = array[1];

    if(array.Size() == 3) //object was passed
    {
      Object *obj = GetObject(array[2]);
      EntityAI *veh = dyn_cast<EntityAI>(obj);
      if(veh)
        veh->GetHistoryLine().SetDraw(enable);
    }
    else //set property for all
    {
      GVBSVisuals.SetDrawTrail(enable);
    }
  }
  // show/hide satellite texture on map
  else if (strcmpi(type,"texture")==0)
  {
    if (array.Size() != 3)
    {
      state->SetError(EvalDim,array.Size(),3);
      return NOTHING; 
    }
    if(!CheckType(state, array[1], GameBool)) return NOTHING; 
    if(!CheckType(state, array[2], GameControl)) return NOTHING; 

    // the map control
    IControl *ctrl = static_cast<GameDataControl *>(array[2].GetData())->GetControl();
    CStaticMap *staticMap = dynamic_cast<CStaticMap *>(ctrl);
    if (staticMap)
    {
      staticMap->ShowScale(array[1]);
      GVBSVisuals.SetShowTextures(array[1]);
    }
  }
  else if (strcmpi(type,"grid")==0)
  {
    if (array.Size() < 2)
    {
      state->SetError(EvalDim,array.Size(),2);
      return NOTHING; 
    }
    if(!CheckType(state, array[1], GameBool)) return NOTHING; 
    GVBSVisuals.SetShowGrid(array[1]);
  }
  else if (strcmpi(type,"contour")==0)
  {
    if (array.Size() < 2)
    {
      state->SetError(EvalDim,array.Size(),2);
      return NOTHING; 
    }
    if(!CheckType(state, array[1], GameBool)) return NOTHING; 
    GVBSVisuals.SetShowContours(array[1]);
  }
  else
  {
    //change the way of drawing hitlines
    if(strcmpi(type,"hit")==0)
    {
      if(!CheckType(state, array[1], GameBool)) return NOTHING; 

      bool enable = array[1];
      GVBSVisuals.SetDrawHitLine(enable); //set property for all

/*  TODO: enable per object?      
      if(array.Size() == 3) //object was passed
      {
        Object *obj = GetObject(array[2]);
        EntityAI *veh = dyn_cast<EntityAI>(obj);
        if(veh)
          veh->GetHistoryLine().SetDraw(enable);
      }
      else //set property for all
      {
        GVBSVisuals.SetDrawTrail(enable);
      }
*/
    }
  }
#endif
  return NOTHING;
}

GameValue GetViewPar( const GameState *state, GameValuePar oper1 )
{
#if _AAR
  const GameArrayType &array = oper1;
  if (array.Size() < 1 || array.Size() > 2)
  {
    state->SetError(EvalDim,array.Size(),1);
    return NOTHING; 
  }

  if(!CheckType(state, array[0], GameString)) return NOTHING; 

  RString type = array[0];

  if(strcmpi(cc_cast(type),"trail") == 0)
  {
    LogF("is trail");
    Object *obj;
    if(array.Size() == 2) //object was passed
    {
      obj = GetObject(array[1]);
    }
    else
    {
      return GVBSVisuals.GetDrawTrail();
    }

    EntityAI *veh = dyn_cast<EntityAI>(obj);
    if(veh)
      return veh->GetHistoryLine().GetDraw();
  }
  else if (strcmpi(type,"texture")==0)
  {
    return GVBSVisuals.GetShowTextures();
  }
  else if (strcmpi(type,"grid")==0)
  {
    return GVBSVisuals.GetShowGrid();
  }
  else if (strcmpi(type,"contour")==0)
  {
    return GVBSVisuals.GetShowContours();
  }
  else if(strcmpi(type,"hit")==0)
  {
    return GVBSVisuals.GetDrawHitLine();
    //  TODO: enable per object?      
  }
#endif //_AAR
  return NOTHING;
}

GameValue PublicExec( const GameState *state, GameValuePar oper1 )
{
  const GameArrayType &array = oper1;
  if (array.Size() < 2 || array.Size() > 4)
  {
    state->SetError(EvalDim,array.Size(),2);
    return NOTHING; 
  }
  if (array[0].GetType() != GameString)
  {
    state->TypeError(GameString, array[0].GetType());
    return NOTHING; 
  }
  if (array[1].GetType() != GameString)
  {
    state->TypeError(GameString, array[1].GetType());
    return NOTHING; 
  }
  Object *obj = NULL;
  OLinkArray(Object) objs;
  if (array.Size() > 2)
  {
    if (array[2].GetType() != GameObject && array[2].GetType() != GameArray)
    {
      state->TypeError(GameObject, array[2].GetType());
      return NOTHING; 
    }
    if (array[2].GetType() == GameObject)
      obj = static_cast<GameDataObject *>(array[2].GetData())->GetObject();
    else
    {
      const GameArrayType &objArray = array[2];
      for (int i=0; i<objArray.Size(); i++)
      {
        if (objArray[i].GetType() != GameObject)
        {
          state->TypeError(GameObject, objArray[i].GetType());
          return NOTHING; 
        }
        int j = objs.Add();
        objs[j] = static_cast<GameDataObject *>(objArray[i].GetData())->GetObject();
      }
    }
  }
  bool executeLocally = true;
  if (array.Size() > 3)
  {
    if (array[3].GetType() != GameBool)
    {
      state->TypeError(GameBool, array[3].GetType());
      return NOTHING; 
    }
    executeLocally = array[3];
  }
  RString condition = array[0];
  RString command = array[1];
  //LogF("[p] publicExec: %d | %d | %d | %s",condition.GetLength(),command.GetLength(),executeLocally,command.Data());
  if (condition.GetLength() > 0 && executeLocally)
  {
    GameVarSpace vars(false);
    state->BeginContext(&vars);
    if (obj) 
      state->VarSetLocal("_this", GameValueExt(obj), true);
    else
    {
      GameValue value = state->CreateGameValue(GameArray);
      GameArrayType &array = value;
      array.Resize(objs.Size());
      for (int i=0; i<objs.Size(); i++)
        array[i] = GameValueExt(objs[i]);
      state->VarSetLocal("_this", value, true);
    }
    bool doCommand = state->EvaluateMultiple(condition, GameState::EvalContext::_default, GWorld->GetMissionNamespace());
    if (doCommand && command.GetLength() > 0)
      state->EvaluateMultiple(command, GameState::EvalContext::_default, GWorld->GetMissionNamespace());
    state->EndContext();
  }

  if (GWorld->GetMode() == GModeNetware)
    GetNetworkManager().PublicExec(condition,command,obj,objs);

  return NOTHING;
}

GameValue TriggerCreateLocal(const GameState *state, GameValuePar oper1)
{
  const GameArrayType &array = oper1;
  if (!CheckSize(state, array, 2)) return OBJECT_NULL;
  if (!CheckType(state, array[0], GameString)) return OBJECT_NULL;

  GameStringType type = array[0];
  Vector3 pos;
  if (!GetPos(state, pos, array[1])) return OBJECT_NULL;
  pos[1] = GLOB_LAND->RoadSurfaceYAboveWater(pos[0], pos[2]);

  Ref<Vehicle> vehicle = GWorld->NewNonAIVehicleWithID(type);
  if (!vehicle) return OBJECT_NULL;

  // position is on sea level now
  if (vehicle->GetShape())
  {
    pos += vehicle->GetShape()->BoundingCenter();
  }

  // TODO: random azimut
  Vector3 normal = VUp;
  Matrix4 transform;
  transform.SetUpAndDirection(normal, Vector3(0,0,1));
  transform.SetPosition(pos);
  vehicle->SetTransform(transform);

  // Add into World
  GWorld->AddSlowVehicle(vehicle);

  sensorsMap.Add(vehicle.GetRef());
  return GameValueExt(vehicle,GameValExtObject);
}

Person *CreateUnitLocal(AIGroup *group, RString type, Vector3Par position, RString init, float skill, Rank rank)
{
  Ref<EntityAI> veh = GWorld->NewVehicleWithID(type);
  Person *soldier = dyn_cast<Person>(veh.GetRef());
  if (!soldier) return NULL;

  AIBrain *agent = new AIUnit(soldier);
  soldier->SetBrain(agent);

  Vector3 normal, pos = position;
  if (AIUnit::FindFreePosition(pos, normal, true, veh->GetType()))
  {    
    pos[1] = GLOB_LAND->RoadSurfaceYAboveWater(pos[0], pos[2]);
  }

  Matrix3 dir;
  Matrix4 transform;

  transform.SetPosition(pos);
  dir.SetUpAndDirection(VUp,VForward);
  transform.SetOrientation(dir);

  veh->PlaceOnSurface(transform);

  veh->SetTransform(transform);
  veh->Init(transform, true);
  veh->OnEvent(EEInit);

  GameValue varValue = GameValueExt(veh,GameValExtObject);
  GameVarSpace local(false);
  GWorld->GetGameState()->BeginContext(&local);
  GWorld->GetGameState()->VarSet("this", varValue, false, false, GWorld->GetMissionNamespace());
  GWorld->GetGameState()->Execute(init, GameState::EvalContext::_default, GWorld->GetMissionNamespace());
  GWorld->GetGameState()->EndContext();

  // if vehicle is static, add it to building list
  GWorld->AddVehicle(veh);
#if 0
  if (GWorld->GetMode() == GModeNetware)
  {
    GetNetworkManager().CreateVehicle(veh, VLTVehicle, "", -1);

    VehicleInitMessage cmd;
    cmd._vehicle = veh.GetRef();
    cmd._init = init;
    GetNetworkManager().VehicleInit(cmd);
  }
#endif
  // soldier is free soldier - we should add sensor
  GWorld->AddSensor(soldier);

#if 0
  IdentityInfo info;
  center->NextSoldierIdentity(info, soldier);
  agent->Load(info);
#endif
  AIUnitInfo &aiInfo = soldier->GetInfo();
  aiInfo._rank = rank;
  aiInfo._initExperience = aiInfo._experience = AI::ExpForRank(rank);
  agent->SetRawAbility(skill);
#if 0
  AIUnit *leader = group->Leader();
  AIUnit *unit = agent->GetUnit();
  {
    // Only AIUnit implementation can be inserted to static AI structures
    // Add into AIGroup
    AISubgroup *subgroup = group->MainSubgroup();
    group->AddUnit(unit);
    if (!subgroup)
    {
      Assert(group->MainSubgroup());
      GetNetworkManager().CreateObject(group->MainSubgroup());
      // update group as well to let know about main subgroup
      GetNetworkManager().UpdateObject(group);
    }
    if (!group->Leader()) center->SelectLeader(group);
  }
  GetNetworkManager().CreateObject(agent);
  // update group as well to let know leader changed
  if (group->Leader() != leader)
  {
    GetNetworkManager().UpdateObject(group);
  }
#endif
  return soldier;
}

GameValue UnitCreateLocal( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
  GameStringType type = oper1;
  const GameArrayType &array = oper2;
  
  Vector3 pos;
  AIGroup *grp = NULL;
  float skill = 0.5;
  Rank rank = RankPrivate;
  RString init;
  switch (array.Size())
  {
    case 5:
      {
        if (array[4].GetType() != GameString)
        {
          state->TypeError(GameString, array[4].GetType());
          return OBJECT_NULL;
        }
        GameStringType rankName = array[4];
        rank = GetEnumValue<Rank>((const char *)rankName);
        if (rank==INT_MIN) rank = RankPrivate;
      }
    case 4:
      {
        if (array[3].GetType() != GameScalar)
        {
          state->TypeError(GameScalar, array[3].GetType());
          return OBJECT_NULL;
        }
        skill = array[3];
      }
    case 3:
      {
        if (array[2].GetType() != GameString)
        {
          state->TypeError(GameString, array[2].GetType());
          return OBJECT_NULL;
        }
        init = array[2];
      }
    case 2:
      {
        if (!GetRelPos(state,pos, array[0]))
        {
          //state->SetError(EvalGen);
          return OBJECT_NULL;
        }
        // no group for local units
        //grp = GetGroup(array[1]);
        //if (!grp) return OBJECT_NULL;
      }
      break;
    default:
      state->SetError(EvalDim, array.Size(), 5);
      return OBJECT_NULL;
  }

  Person *soldier = CreateUnitLocal(grp, type, pos, init, skill, rank);
  return GameValueExt(soldier);
}

GameValue TriggerGetActivation(const GameState *state, GameValuePar oper1)
{
  GameValue value = state->CreateGameValue(GameArray);
  GameArrayType &array = value;
  Detector *det = GetDetector(oper1);
  if (det)
  {
    array.Resize(3);

    const EnumName *actBy = GetEnumNames((ArcadeSensorActivation)det->GetActivationBy());
    RString activationBy = actBy[det->GetActivationBy()].name;

    const EnumName *actType = GetEnumNames((ArcadeSensorActivationType)det->GetActivationType());
    RString activationType = actType[det->GetActivationType()].name;

    bool repeat = det->IsRepeating();

    array[0] = activationBy;
    array[1] = activationType;
    array[2] = repeat;
  }
  return value;
}

GameValue TriggerGetArea(const GameState *state, GameValuePar oper1)
{
  GameValue value = state->CreateGameValue(GameArray);
  GameArrayType &array = value;
  Detector *det = GetDetector(oper1);
  if (det)
  {
    array.Resize(4);
    float a, b, angle;
    bool rect;
    det->GetArea(a, b, angle, rect);
    array[0] = a;
    array[1] = b;
    array[2] = angle;
    array[3] = rect;
  }
  return value;
}

GameValue TriggerGetStatements(const GameState *state, GameValuePar oper1)
{
  GameValue value = state->CreateGameValue(GameArray);
  GameArrayType &array = value;
  Detector *det = GetDetector(oper1);
  if (det)
  {
    array.Resize(3);
    RString cond, activ, desactiv;
    det->GetStatements(cond, activ, desactiv);
    array[0] = cond;
    array[1] = activ;
    array[2] = desactiv;
  }
  return value;
}

GameValue TriggerGetText(const GameState *state, GameValuePar oper1)
{
  Detector *det = GetDetector(oper1);
  if (det)
    return det->GetText();
  return NOTHING;
}

GameValue TriggerGetTimeout(const GameState *state, GameValuePar oper1)
{
  GameValue value = state->CreateGameValue(GameArray);
  GameArrayType &array = value;
  Detector *det = GetDetector(oper1);
  if (det)
  {
    array.Resize(4);
    float min, mid, max;
    bool interruptable;
    det->GetTimeout(min, mid, max, interruptable);
    array[0] = min;
    array[1] = mid;
    array[2] = max;
    array[3] = interruptable;
  }
  return value;
}

GameValue TriggerGetType(const GameState *state, GameValuePar oper1)
{
  Detector *det = GetDetector(oper1);
  if (det)
  {
    const EnumName *eType = GetEnumNames((ArcadeSensorType)det->GetAction());
    return eType[det->GetAction()].name;
  }
  return NOTHING;
}

GameValue TextLength( const GameState *state, GameValuePar oper1 )
{
  if (!CheckType(state, oper1, GameString)) return 0.0f;
  RString text = oper1;  
  return (float)text.GetLength();
}

GameValue TextFind( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
  if (!CheckType(state, oper1, GameString)) return false;
  if (!CheckType(state, oper2, GameString)) return false;
  RString str1 = oper1;
  RString str2 = oper2;
  return (float)str1.Find(str2);
}

GameValue TextInStr( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
  if (!CheckType(state, oper1, GameString)) return false;
  if (!CheckType(state, oper2, GameString)) return false;
  RString str1 = oper1;
  RString str2 = oper2;
  return str2.Find(str1) > -1;
}

GameValue TextAddQuotes( const GameState *state, GameValuePar oper1 )
{
  if (!CheckType(state, oper1, GameString)) return RString();
  RString text = oper1;
  RString result;
  const char *ptr = text.Data();
  const char *arg = fstrchr(ptr, '\"');
  while (arg)
  {
    result = result + RString(ptr, arg - ptr);
    result = RString(result, "\"\"");
    ptr = arg + 1;
    arg = fstrchr(ptr, '\"');
  }
  return RString(result, ptr);
}

GameValue TextTrim( const GameState *state, GameValuePar oper1 )
{
  if (!CheckType(state, oper1, GameArray)) return RString();
  const GameArrayType &array = oper1;
  if (!CheckSize(state, array, 3)) return RString();
  if (!CheckType(state, array[0], GameString)) return RString();
  if (!CheckType(state, array[1], GameScalar)) return RString();
  if (!CheckType(state, array[2], GameScalar)) return RString();

  RString text = array[0];
  
  float fleft = array[1];
  int left = fleft;

  float fright = array[2];
  int right = fright;

  const char *ptr = text;

  // trim right
  if (right > text.GetLength()) return RString();
  RString trim1 = RString(ptr,text.GetLength() - right);
  ptr = trim1;

  // trim left
  if (left > trim1.GetLength()) return RString();
  RString trim2 = RString(ptr + left,trim1.GetLength() - left);
  return trim2;
}

class BuildAllVehiclesList
{
  GameArrayType &_vehicles;
public:
  BuildAllVehiclesList(GameArrayType &array):_vehicles(array){}
  __forceinline bool operator () (Object *veh) const
  {
    if (veh)
    {
      Person *person = dyn_cast<Person>(veh);
      if (!person && !veh->Static())
      {
        _vehicles.Add(GameValueExt(veh));
      }
    }
    return false;
  }
};

GameValue GetAllVehicles(const GameState *state)
{
  GameValue value = state->CreateGameValue(GameArray);
  GameArrayType &array = value;

  BuildAllVehiclesList build(array);
  GWorld->ForEachVehicle(build);
  GWorld->ForEachSlowVehicle(build);

  array.Compact();
  return value;
}

class BuildStaticVehiclesList
{
  GameArrayType &_vehicles;
  RString _eval;
  const GameState *_state;
public:
  BuildStaticVehiclesList(GameArrayType &array, RString eval, const GameState *state):_vehicles(array),_eval(eval),_state(state){}
  __forceinline bool operator () (Object *veh) const
  {
    if (veh)
    {
      Person *person = dyn_cast<Person>(veh);
      if (!person && veh->Static())
      { 
        _state->VarSetLocal("_x", GameValueExt(veh), true);
        bool isValid = _state->EvaluateMultiple(_eval, GameState::EvalContext::_default, GWorld->GetMissionNamespace());
        if (isValid)
          _vehicles.Add(GameValueExt(veh));
      }
    }
    return false;
  }
};

GameValue GetAllStaticVehicles(const GameState *state, GameValuePar oper1)
{
  GameValue value = state->CreateGameValue(GameArray);
  GameArrayType &array = value;

  if (oper1.GetType() != GameString)
  {
    if (state) state->TypeError(GameString,oper1.GetType());
    return value;
  }
  RString eval = oper1;

  BuildStaticVehiclesList build(array, eval, state);
  GWorld->ForEachSlowVehicle(build);

  array.Compact();
  return value;
}

class BuildShotList
{
  GameArrayType &_vehicles;
  RString _eval;
  const GameState *_state;
public:
  BuildShotList(GameArrayType &array, RString eval, const GameState *state):_vehicles(array),_eval(eval),_state(state){}
  __forceinline bool operator () (Object *veh) const
  {
    if (veh)
    {
      _state->VarSetLocal("_x", GameValueExt(veh), true);
      bool isValid = _state->EvaluateMultiple(_eval, GameState::EvalContext::_default, GWorld->GetMissionNamespace());
      if (isValid)
        _vehicles.Add(GameValueExt(veh));
    }
    return false;
  }
};

GameValue GetAllShots(const GameState *state, GameValuePar oper1)
{
  GameValue value = state->CreateGameValue(GameArray);
  GameArrayType &array = value;

  if (oper1.GetType() != GameString)
  {
    if (state) state->TypeError(GameString,oper1.GetType());
    return value;
  }
  RString eval = oper1;

  BuildShotList build(array, eval, state);
  GWorld->ForEachFastVehicle(build);

  array.Compact();
  return value;
}

GameValue TitleEffectActive(const GameState *state)
{
  if (GWorld->GetTitleEffect())
    return true;
  return false;
}

GameValue TitleEffectProlong(const GameState *state, GameValuePar oper1)
{
  if (oper1.GetType() != GameScalar)
  {
    if (state) state->TypeError(GameScalar,oper1.GetType());
    return NOTHING;
  }
  float time = oper1;

  TitleEffect *titleEffect = GWorld->GetTitleEffect();
  if (titleEffect)
    titleEffect->Prolong(time);

  return NOTHING;
}

GameValue SeaLevelOffset(const GameState *state, GameValuePar oper1)
{
  if( oper1.GetType() != GameScalar)
  {
    if(state) state->TypeError(GameScalar,oper1.GetType());
    return NOTHING;
  }

  GLandscape->SetSeaLevelOffset(oper1);

  // Weather chanage done, update clients
  if(GWorld->GetMode() == GModeNetware)
    GLandscape->UpdateRemote();
    
  return NOTHING;
}

GameValue GetPlayableUnitNames(const GameState *state)
{
  GameValue value = state->CreateGameValue(GameArray);
  GameArrayType &array = value;

  for(int i=0; i < CurrentTemplate.groups.Size(); ++i)
  {
    AutoArray<ArcadeUnitInfo> &units = CurrentTemplate.groups[i].units;
    for(int i=0; i < units.Size(); ++i)
    {
      ArcadeUnitInfo &info = units[i];
      if (info.player > APNonplayable)
        if (info.name.GetLength() > 0)
          array.Add(info.name);
    }
  }

  array.Compact();
  return value;
}

GameValue ObjGetWeaponPosition(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
  GameValue value = state->CreateGameValue(GameArray);
  GameArrayType &array = value;
  array.Resize(3);
  array[0] = 0.0f; array[1] = 0.0f; array[2] = 0.0f;

  Object *obj = GetObject(oper1);
  EntityAIFull *veh = dyn_cast<EntityAIFull>(obj);
  if (veh)
  {
    RString weapon = oper2;
    // TODO: search in all turrets
    TurretContext context;
    if (veh->GetPrimaryGunnerTurret(context))
    {
      for (int i=0; i<context._weapons->_magazineSlots.Size(); i++)
      {
        const MagazineSlot &slot = context._weapons->_magazineSlots[i];
        if (stricmp(slot._weapon->GetName(), weapon) == 0)
        {
          Vector3 pos = veh->GetWeaponCenter(context, i);
          array[0] = pos.X(); array[1] = pos.Z(); array[2] = pos.Y();
          break;
        }
      }
    }
  }
  return value;
}

// Motioncontroller function active for LaserShot only
#include "hla/MotionController.hpp"

GameValue ObjSetMotionController( const GameState *state, GameValuePar oper1 )
{
#if !_MOTIONCONTROLLER
  return NOTHING;
#else
  Object *obj1 = GetObject(oper1);
  if (!obj1) return NOTHING;

  EntityAI *entAi=dyn_cast<EntityAI>(obj1);

  if (entAi && entAi->IsLocal())
  {
    GMotionController.controlledVehicle = entAi;
  }
  return NOTHING;
#endif
}

GameValue SetTracker( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
#if _VBS3 && _VBS_TRACKING
  Object *obj = GetObject(oper1);
  if(!obj) return NOTHING;

  const GameArrayType &array = oper2;
  if( array.Size() == 0 || array.Size() > 2 ) return NOTHING;

  for( int i = 0; i < array.Size(); ++i)
    if(!CheckType(state,array[i], GameScalar)) return NOTHING;
  
  int sensorId =toInt((float)array[0]);
  int appId = 1; // default appId
  if(array.Size() == 2)
    appId = toInt((float)array[1]);

  if(sensorId!=-1)
    GTracking.AddObject(obj,sensorId,appId);
  else
    GTracking.DisableTracking(obj);

#endif
  return NOTHING;
}

GameValue IsAttached(const GameState *state, GameValuePar oper1)
{
  Object *obj = GetObject(oper1);
  if (!obj) return NOTHING;

  return bool(obj->IsAttached());
}

GameValue AttachedTo(const GameState *state, GameValuePar oper1)
{
  Object *obj = GetObject(oper1);
  if (!obj) return NOTHING;

  return GameValueExt(obj->GetAttachedTo());
}

GameValue IsValidVarName( const GameState *state, GameValuePar oper1 )
{
  if (!CheckType(state, oper1, GameString))
  {
    state->TypeError(GameString, oper1.GetType());
    return NOTHING;
  }
  RString varname = oper1;
  return varname.GetLength() == 0 || state->IdtfGoodName(varname);
}

GameValue IsValidFileName( const GameState *state, GameValuePar oper1 )
{
  if (!CheckType(state, oper1, GameString))
  {
    state->TypeError(GameString, oper1.GetType());
    return NOTHING;
  }
  RString varname = oper1;
  return (strcspn(varname, "\\/:*?\"<>|") == strlen(varname));
}

GameValue SetUserChart( const GameState *state, GameValuePar oper1 )
{
  if (!CheckType(state, oper1, GameString))
  {
    state->TypeError(GameString, oper1.GetType());
    return NOTHING;
  }

#if _VBS3_UDEFCHART
  GWorld->SetUserDefinedChartName(oper1);
#endif

  return NOTHING;
}

GameValue SetUserChartDrawObjects(const GameState *state, GameValuePar oper1)
{
  if( oper1.GetType() != GameBool)
  {
    if(state) state->TypeError(GameBool,oper1.GetType());
    return NOTHING;
  }

#if _VBS3_UDEFCHART
  GWorld->SetDrawObjectsOnUserDefinedChart((bool)oper1);
#endif

  return NOTHING;
}

GameValue ForceEnd(const GameState *state, GameValuePar oper1)
{
  if( oper1.GetType() != GameScalar)
  {
    if(state) state->TypeError(GameScalar,oper1.GetType());
    return NOTHING;
  }
  int ending = toInt(oper1);
  if (ending < EMContinue || ending > EMEnd6) return NOTHING;
  GWorld->SetEndMode((EndMode)ending);
  GWorld->ForceEnd(true);
  return NOTHING;
}

GameValue UnitEnablePersonalItems( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
  // TODO: add animation and offset as param?
  Object *obj1 = GetObject(oper1);
  if (!obj1) 
  {
    state->TypeError(GameBool, oper1.GetType());
    return NOTHING;
  }
  if (!CheckType(state, oper2, GameBool))
  {
    state->TypeError(GameBool, oper2.GetType());
    return NOTHING;
  }
  bool enablePersonalItems = oper2;

  Person* pers = dyn_cast<Person>(obj1);
  if (!pers) return NOTHING;

  AIBrain* brain = pers->Brain();
  if(!brain) return NOTHING;

  if(!brain->GetVehicleIn()) //has to be in a vehicle!
     return NOTHING;
  
// TODO: only do that for local units?

  pers->SetEnablePersonalItems(enablePersonalItems);

  if (GWorld->GetMode() == GModeNetware)
    GetNetworkManager().EnablePersonalItems(pers,enablePersonalItems);

//  if(pers->IsLocal())
//    pers->SetEnablePersonalItems(enablePersonalItems);

  return NOTHING;
}

GameValue IsPersonalItemsEnabled( const GameState *state, GameValuePar oper1)
{
  Object *obj1 = GetObject(oper1);
  if (!obj1) 
  {
    state->TypeError(GameBool, oper1.GetType());
    return false;
  }

  Person* pers = dyn_cast<Person>(obj1);
  if (!pers) return false;
  
  return pers->IsPersonalItemsEnabled();
}

#pragma warning( push )
#pragma warning( disable : 4702 )
//uses currently selected weapon
//[<veh>,<turret-path> {,muzzle}] FiringSolution [targetPos]
//returns: [[azimuth, elevation], flightTime] //if flightTime < 0 then it's invalid
GameValue ObjGetFiringSolution(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
  GameValue value = state->CreateGameValue(GameArray);
  GameArrayType &outArray = value;

  if(!CheckType(state,oper1,GameArray)) return outArray;
  const GameArrayType &inArray1 = oper1;
  if(inArray1.Size() < 2) return outArray;
  if(!CheckType(state,inArray1[0], GameObject)) return outArray;

  Object *obj = GetObject(inArray1[0]);
  if (!obj) return outArray;
  Transport *veh = dyn_cast<Transport>(obj);
  if (!veh) return outArray;

  //TurretPath
  if(!CheckType(state,inArray1[1], GameArray)) return outArray;

  const GameArrayType &subarray = inArray1[1];
  int n = subarray.Size();
  for (int i=0; i<n; i++)
    if (!CheckType(state, subarray[i], GameScalar)) return NOTHING;

  // get the path
  TurretPath path; path.Resize(n);
  for (int i=0; i<n; i++) path[i] = toInt((float)subarray[i]);
  Turret *turret = veh->GetTurret(path.Data(), path.Size());
  if (!turret) return outArray;

  TurretContext context;
  veh->FindTurret(turret,context);

  int weapon = context._weapons->_currentWeapon;
  if(inArray1.Size() > 2)
  {
    // Get the muzzleName
    if(!CheckType(state,inArray1[2], GameString)) return outArray;
    RString weaponName = inArray1[2];

    for (int i=0; i<context._weapons->_magazineSlots.Size(); i++)
    {
      const MagazineSlot &slot = context._weapons->_magazineSlots[i];
      if (stricmp(slot._weapon->GetName(), weaponName) == 0)
      {
        weapon = i;
        break;
      }
    }
  }
  
  Vector3 aimDir;
  Vector3 targetPos;
  float leadTime;
  const GameArrayType &inArray2 = oper2;
  if(!CheckType(state,inArray2, GameArray)) return outArray;
  if(!CheckSize(state,inArray2, 3)) return outArray;
  targetPos[0] = inArray2[0]; targetPos[1] = inArray2[2]; targetPos[2] = inArray2[1];

  GameValue newArray = state->CreateGameValue(GameArray);
  GameArrayType &dirArray = newArray;

  if(veh->CalculateAimWeapon(context,weapon,aimDir,leadTime,targetPos))
  {
    aimDir.Normalize();
    float elev = asin(aimDir.Y()) * (180.0f / H_PI);
    
    float azimut = atan2(aimDir.X(), aimDir.Z())* (180.0f / H_PI);
    if (azimut < 0) azimut += 360.0f;
   
    dirArray.Add(azimut);
    dirArray.Add(elev);
    outArray.Add(dirArray);
    outArray.Add(leadTime);
  }
  else
  {
    dirArray.Add(0.0f);
    dirArray.Add(0.0f);
    outArray.Add(dirArray);
    outArray.Add(-1.0f);
  }
  return outArray;
}

GameValue ObjGetWeaponAimingAt(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
  GameValue value = state->CreateGameValue(GameArray);
  GameArrayType &array = value;
   
  if (oper1.GetType() != GameObject)
  {
    if (state) state->TypeError(GameObject,oper1.GetType());
    return value;
  }
  Object *obj = GetObject(oper1);

  if (oper2.GetType() != GameArray)
  {
    if (state) state->TypeError(GameArray,oper2.GetType());
    return value;
  }
  const GameArrayType &inArray = oper2;
  if (inArray.Size() < 3)
  {
    if (state) state->SetError(EvalDim,inArray.Size(),3);
    return value;
  }
  if (inArray[0].GetType() != GameString)
  {
    if (state) state->TypeError(GameString,inArray[0].GetType());
    return value;
  }
  if (inArray[1].GetType() != GameScalar)
  {
    if (state) state->TypeError(GameScalar,inArray[1].GetType());
    return value;
  }
  if (inArray[2].GetType() != GameScalar)
  {
    if (state) state->TypeError(GameScalar,inArray[2].GetType());
    return value;
  }
  RString weapon = inArray[0];
  float maxDis = inArray[1];
  float radius = inArray[2];

  EntityAIFull *veh = dyn_cast<EntityAIFull>(obj);
  if (veh)
  {    
    // TODO: search in all turrets
    TurretContext context;
    if (veh->GetPrimaryGunnerTurret(context) && context._turretType)
    {
      for (int i=0; i<context._weapons->_magazineSlots.Size(); i++)
      {
        const MagazineSlot &slot = context._weapons->_magazineSlots[i];
        if (stricmp(slot._weapon->GetName(), weapon) == 0)
        {
          Transport* transport = dyn_cast<Transport>(veh);
          Vector3 dir, pos;

          if(transport)
          {
            Matrix4Val shootTrans = transport->GunTurretTransform(*context._turretType);
            pos = transport->PositionModelToWorld(context._turretType->GetTurretPos(shootTrans));
            dir = transport->DirectionModelToWorld(context._turretType->GetTurretDir(shootTrans));
          }
          else
          {
            pos = veh->PositionModelToWorld(veh->GetWeaponPoint(context, i));
            dir = veh->GetWeaponDirection(context, i);
          }
          Vector3 dirNorm = dir.Normalized();

          Vector3 isect;
          float t = GLandscape->IntersectWithGroundOrSea(&isect,pos,dirNorm,0,maxDis*1.1);
          if(t<maxDis)
            maxDis = t;

            // test collision with objects
            CollisionBuffer collision;
            GLandscape->ObjectCollision(collision,obj,NULL,pos,pos+maxDis*dirNorm,radius);
            RefArray<Object> objs;

            for (int i=0; i<collision.Size(); i++)
            {
              Object *obj = collision[i].object;
              bool addObj = true;
              for (int j=0; j<objs.Size(); j++)
              {
                if (objs[j] == obj)
                {
                  addObj = false;
                  break;
                }
              }
              if (addObj) objs.Add(obj);
            }

            for (int i=0; i<objs.Size(); i++)
              array.Add(GameValueExt(objs[i]));

            return value;
          }
          else
            return value;
        }
//      }
    }
  }
  return value;
}

#pragma warning( pop ) 

GameValue CamSetAttachedLookDir(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
  CameraVehicle *cam = dyn_cast<CameraVehicle>(GetObject(oper1));
  if( !cam ) return NOTHING;

  Vector3 dir = VZero;
  if (oper2.GetType() != GameArray)
  {
    if (state) state->TypeError(GameArray,oper2.GetType());
    return NOTHING;
  }
  const GameArrayType &inArray = oper2;
  if (inArray.Size() != 3)
  {
    if (state) state->SetError(EvalDim,inArray.Size(),3);
    return NOTHING;
  }
  for (int i=0; i<3; i++)
    if (inArray[i].GetType() != GameScalar)
    {
      if (state) state->TypeError(GameScalar,inArray[i].GetType());
      return NOTHING;
    }
  dir[0] = inArray[0];
  dir[1] = inArray[2];
  dir[2] = inArray[1];

  cam->SetAttachedCameraLookDir(dir);

  return NOTHING;
}

GameValue SetFriendlyFireMessageScope(const GameState *state, GameValuePar oper1)
{
  if (oper1.GetType() != GameString)
  {
    if (state) state->TypeError(GameString,oper1.GetType());
    return NOTHING;
  }
  bool friendlyFireMsgSideOnly = false;
  RString mode = oper1;
  if (stricmp(mode,"SIDE") == 0)
    friendlyFireMsgSideOnly = true;
  GWorld->SetFriendlyFireMsgSideOnly(friendlyFireMsgSideOnly);
  return NOTHING;
}

GameValue CamGetFov(const GameState *state, GameValuePar oper1)
{
  CameraVehicle *cam = dyn_cast<CameraVehicle>(GetObject(oper1));
  if( !cam ) return NOTHING;

  GameValue value = state->CreateGameValue(GameArray);
  GameArrayType &array = value;
  array.Resize(2);
   
  float minFov, maxFov = 0.0f;
  cam->GetFOV(minFov,maxFov);
  array[0] = minFov;
  array[1] = maxFov;

  return value;
}

GameValue CamSetBankIfAttached(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
  CameraVehicle *cam = dyn_cast<CameraVehicle>(GetObject(oper1));
  if( !cam ) return NOTHING;

  if (oper2.GetType() != GameBool)
  {
    if (state) state->TypeError(GameBool,oper2.GetType());
    return NOTHING;
  }
  bool bank = oper2;
  cam->SetAttachedCameraBank(bank);
  return NOTHING;
}

GameValue ObjIsLookingAt(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
  if (oper1.GetType() != GameObject)
  {
    if (state) state->TypeError(GameObject,oper1.GetType());
    return false;
  }

  if (oper2.GetType() != GameArray)
  {
    if (state) state->TypeError(GameArray,oper2.GetType());
    return false;
  }

  const GameArrayType &inArray = oper2;
  if (inArray.Size() != 2)
  {
    if (state) state->SetError(EvalDim,inArray.Size(),2);
    return false;

  }
  if (inArray[0].GetType() != GameObject)
  {
    if (state) state->TypeError(GameObject,inArray[0].GetType());
    return false;
  }
  if (inArray[1].GetType() != GameScalar)
  {
    if (state) state->TypeError(GameScalar,inArray[1].GetType());
    return false;
  }

  Object *obj1 = GetObject(oper1);
  if (!obj1) return false;

  Man *man = dyn_cast<Man>(obj1);
  if (!man) return false;

  Object *obj2 = GetObject(inArray[0]);
  if (!obj2) return false;

  float checkAngle = inArray[1];

  Vector3 lookDir;
  Vector3 dirTo;

  if (man == GWorld->PlayerOn())
  {
    lookDir = GScene->GetCamera()->Direction();
    dirTo = obj2->Position() - GScene->GetCamera()->Position();
  }
  else
  {
    TurretContext context;
    lookDir = man->GetLookAroundDirection(context);
    dirTo = obj2->Position() - man->Position();

    // is the man in a vehicle?
      // TODO: person in turret
    EntityAI *ai=dyn_cast<EntityAI>(obj1);
    if (ai)
    {
      AIBrain *unit=ai->CommanderUnit();
      if (unit)
      {
        EntityAI *vehicle=unit->GetVehicle();
        if (ai != vehicle)
        {
          // works for cargo
          lookDir = vehicle->DirectionModelToWorld(man->GetCrewLookDirection());
          dirTo = obj2->Position() - vehicle->Position();
        }
      }
    }
  }

  float cosAngle = lookDir.CosAngle(dirTo);
  float angle = acos(cosAngle);

  return angle <= checkAngle * (H_PI / 180.0f);
}

GameValue SetEyeAccom(const GameState *state, GameValuePar oper1)
{
  if (oper1.GetType() != GameScalar)
  {
    if (state) state->TypeError(GameScalar,oper1.GetType());
    return NOTHING;
  }
  float accom = oper1;
  GWorld->SetEyeAccom(accom);
  return NOTHING;
}

GameValue AllowCreateShot(const GameState *state, GameValuePar oper1)
{
#if _LASERSHOT
  if (oper1.GetType() != GameBool)
  {
    if (state) state->TypeError(GameBool,oper1.GetType());
    return NOTHING;
  }
  bool allow = oper1;
  GWorld->SetCreateMouseShot(allow);
#endif
  return NOTHING;
}

GameValue AllowMovementControlsInDialog(const GameState *state, GameValuePar oper1)
{
  if (oper1.GetType() != GameBool)
  {
    if (state) state->TypeError(GameBool,oper1.GetType());
    return NOTHING;
  }
  bool allow = oper1;
  GWorld->SetAllowMovementCtrlsInDlg(allow);
  return NOTHING;
}

GameValue DriveOnRoadSide( const GameState *state, GameValuePar oper1 )
{
  GameStringType name = oper1;
  if(!stricmp(name, "left"))
    Glob.ActiveRoadSide = RSLeft;
  else
    if(!stricmp(name, "right"))
      Glob.ActiveRoadSide = RSRight;
    else
      if(stricmp(name, "center"))
        Glob.ActiveRoadSide = RSCenter;
  return NOTHING;
};

GameValue VehIsLightOn(const GameState *state, GameValuePar oper1)
{
  Object *obj = GetObject(oper1);
  EntityAI *veh = dyn_cast<EntityAI>(obj);
  if (!veh) return false;
  return veh->IsLightOn();
}

GameValue UICtrlSetAutoComplete(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
  IControl *ctrl = GetControl(oper1);
  if (!ctrl || ctrl->GetType() != CT_EDIT) return NOTHING;
  CEdit *edit = static_cast<CEdit *>(ctrl);
  if (oper2.GetType() != GameString)
  {
    if (state) state->TypeError(GameString,oper2.GetType());
    return NOTHING;
  }
  RString type = oper2;
  edit->SetAutoComplete(CreateAutoComplete(type));
  return NOTHING;
}

GameValue UICtrlGetFontHeight(const GameState *state, GameValuePar oper1)
{
#if _VBS3 // getFontHeight
  IControl *ctrl = GetControl(oper1);
  if (!ctrl) return 0.0f;
  return ctrl->GetFontHeight();
#else
  return 0.0f;
#endif
}

GameValue UICtrlSetRowHeight(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
  IControl *ctrl = GetControl(oper1);
  if (!ctrl) return NOTHING;
#if _VBS3 // set and getRowHeight
  float height = oper2;
  ctrl->SetRowHeight(height);
#endif
  return NOTHING;
}

void SetGroupsSubGroupDirection( AIGroup *grp, float direction )
{
  Assert(grp);
  Matrix3 rotY(MRotationY, -HDegree(direction));
  
  for( int i = 0; i < grp->NSubgroups(); ++i )
  {
    AISubgroup *subgroup = grp->GetSubgroup(i);
    if(subgroup)
      subgroup->SetFormationDirection(rotY.Direction());
  }
}

void SetPersonFormationDirection(Person *person,float dir)
{
  AIBrain *brain = person->Brain();
  if(brain && brain->GetGroup())
    SetGroupsSubGroupDirection(brain->GetGroup(),dir);
}

class EachCrew : public ICrewFunc
{
  float _dir;
public:
  EachCrew(float dir):_dir(dir){};
  virtual bool operator () (Person *person)
  {
    SetPersonFormationDirection(person,_dir);
    return false;
  }
};

GameValue SetFormationDir(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
  if(oper2.GetType() != GameScalar)  return NOTHING;

  // perform operation on group
  if( oper1.GetType() == GameGroup )
  {
    AIGroup *grp = GetGroup(oper1);
    if(grp)
      SetGroupsSubGroupDirection(grp,oper2);
  }

  // perform operation on objects types
  if( oper1.GetType() == GameObject )
  {
    Object *obj = GetObject(oper1);
    if(!obj) 
      return NOTHING;

    // person type
    Person *person = dyn_cast<Person>(obj);
    if(person)
    {
      SetPersonFormationDirection(person,oper2);
      return NOTHING;
    }

    // transport type
    Transport *transport = dyn_cast<Transport>(obj);
    if(transport)
    {
      EachCrew crew(oper2);
      transport->ForEachCrew(crew);
    }
  }

  return NOTHING;
}

class TurnOffLights
{
  StreetLamp::LightState _turnOn;
public:
  TurnOffLights(StreetLamp::LightState turnOn):_turnOn(turnOn){}
  __forceinline bool operator () (Object *veh) const
  {
    StreetLamp *lamp = dyn_cast<StreetLamp>(veh);
    if (lamp) lamp->SwitchLight(_turnOn);
    return false;
  }
};

GameValue switchAllLights(const GameState *state, GameValuePar oper1)
{
#if _VBS3 // switchAllLights
  if (oper1.GetType() != GameString)
  {
    if (state) state->TypeError(GameString,oper1.GetType());
    return NOTHING;
  }
  RString lampsOnStr = oper1;
  StreetLamp::LightState lightState = GetEnumValue<StreetLamp::LightState>((const char *)lampsOnStr);
  if (lightState==INT_MIN) lightState = StreetLamp::LSAuto;
  TurnOffLights build(lightState);
  GWorld->ForEachSlowVehicle(build);
#endif
  return NOTHING;
}

void PauseSimulation(bool pause)
{
  GWorld->PauseSimulation(pause);

  // pass over network
  if (GWorld->GetMode() == GModeNetware)
    GetNetworkManager().PauseSimulation(pause);
}


GameValue PauseSimulation(const GameState *state, GameValuePar oper1)
{
  if(oper1.GetType() != GameBool)
  {
    if (state) state->TypeError(GameBool,oper1.GetType());
    return NOTHING;
  }
  PauseSimulation(oper1);
  return NOTHING;
}

//command is local!
GameValue SetLightMode( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
  Object *obj1 = GetObject(oper1);
  if (!obj1) 
  {
    state->TypeError(GameBool, oper1.GetType());
    return NOTHING;
  }
  if (!CheckType(state, oper2, GameScalar)) return NOTHING;

  //only for characters atm
  Person* pers = dyn_cast<Person>(obj1);
  if (!pers) return NOTHING;

  pers->SetLightMode(float(oper2));
  
  return NOTHING;
}


GameValue IsPausedSimulation(const GameState *state)
{
  return GWorld->IsPaused();
}

GameValue UICtrlGetRowHeight(const GameState *state, GameValuePar oper1)
{
#if _VBS3 // set and getRowHeight
  IControl *ctrl = GetControl(oper1);
  if (!ctrl) return 0.0f;
  return ctrl->GetRowHeight();
#else
  return 0.0f;
#endif
}

GameValue UITreeGetCurSelTexture(const GameState *state, GameValuePar oper1)
{
  IControl *ctrl = GetControl(oper1);
  if (!ctrl || ctrl->GetType() != CT_TREE) return RString();
  CTree *tree = static_cast<CTree *>(ctrl);
  CTreeItem *selected = tree->GetSelected();
  if (!selected) return RString();
  RString name;
  if (selected->texture)
  {
    name = selected->texture->GetName();
    if (name.GetLength() > 0)
      name = "\\" + name;
  }
  return name;
}

GameValue UITreeGetCurSelData(const GameState *state, GameValuePar oper1)
{
  IControl *ctrl = GetControl(oper1);
  if (!ctrl || ctrl->GetType() != CT_TREE) return RString();
  CTree *tree = static_cast<CTree *>(ctrl);
  CTreeItem *selected = tree->GetSelected();
  if (!selected) return RString();
  return selected->data;
}

class FindTreeItemWithData
{
protected:
  RString _id;
  mutable const CTreeItem *_found;
public:
  FindTreeItemWithData(RString id) {_id = id; _found = NULL;}
  const CTreeItem *GetFound() const {return _found;}
  bool operator () (const CTreeItem *item) const
  {
    if (stricmp(item->data, _id) == 0)
    {
      _found = item;
      return true; // stop searching now
    }
    return false;
  }
};

GameValue UITreeSelectItemData(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
  IControl *ctrl = GetControl(oper1);
  if (!ctrl || ctrl->GetType() != CT_TREE) return NOTHING;
  CTree *tree = static_cast<CTree *>(ctrl);
  if (oper2.GetType() != GameString)
  {
    if (state) state->TypeError(GameString,oper2.GetType());
    return NOTHING;
  }
  RString toFind = oper2;
  FindTreeItemWithData func(toFind);
  tree->ForEachItem(func); 
  const CTreeItem *found = func.GetFound();
  if (found)
  {
    CTreeItem *ptr = unconst_cast(found);
    tree->SetSelected(ptr);
    while (ptr)
    {
      tree->Expand(ptr);
      ptr = ptr->parent;
    }       
    tree->EnsureVisible(ptr);
  }
  return NOTHING;
}

class GetAmmoFunc : public ITurretFunc
{
protected:
  float &_amount;
  float &_totalAmount;

public:
  GetAmmoFunc(float &amount, float &totalAmount) : _amount(amount), _totalAmount(totalAmount) {}
  bool operator () (EntityAIFull *entity, TurretContext &context)
  {
    for (int i=0; i<context._weapons->_magazines.Size(); i++)
    {
      Magazine *magazine = context._weapons->_magazines[i];
      if (!magazine) continue;
      MagazineType *type = magazine->_type;
      if (!type || type->_maxAmmo == 0) continue;
      _amount += magazine->GetAmmo();
      _totalAmount += type->_maxAmmo;
    }
    return false; // all turrets
  }
};

static float GetAmmo(EntityAIFull *veh)
{
  Assert(veh);
  float amount, totalAmount = 0;
  GetAmmoFunc func(amount,totalAmount);
  veh->ForEachTurret(func);
  return amount/totalAmount;
}

GameValue VehGetAmmo(const GameState *state, GameValuePar oper1)
{
  Object *obj = GetObject(oper1);
  EntityAIFull *veh = dyn_cast<EntityAIFull>(obj);
  if (!veh) return NOTHING;
  return GetAmmo(veh);
}

GameValue UILBSetText(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
  IControl *ctrl = GetControl(oper1);
  CListBoxContainer *lbox = GetListBoxContainer(ctrl);
  if (!lbox) return NOTHING;

  const GameArrayType &array = oper2;
  if (!CheckSize(state, array, 2) || !CheckType(state, array[0], GameScalar) || !CheckType(state, array[1], GameString))
    return NOTHING;

  lbox->SetText(toInt(safe_cast<float>(array[0])), array[1]);
  return NOTHING;
}

void UpdateCategory(CListBoxContainer *combo, const RString &selectedType, RString filter);
GameValue UILBFillVehicleClass(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
  IControl *ctrl = GetControl(oper1);
  CListBoxContainer *lbox = GetListBoxContainer(ctrl);
  if (!lbox) return NOTHING;

  const GameArrayType &array = oper2;
  if (!CheckSize(state, array, 2) || !CheckType(state, array[0], GameString) || !CheckType(state, array[1], GameString))
    return NOTHING;

  RString type = array[0];
  RString filter = array[1];

  UpdateCategory(lbox,type,filter);
  return NOTHING;
}

void UpdateObjectList(CListBoxContainer *combo, RString &vehicleClass);
GameValue UILBFillVehicleType(const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
  IControl *ctrl = GetControl(oper1);
  CListBoxContainer *lbox = GetListBoxContainer(ctrl);
  if (!lbox) return NOTHING;

  if (!CheckType(state, oper2, GameString))
    return NOTHING;
  
  RString vehicleClass = oper2;

  UpdateObjectList(lbox,vehicleClass);
  return NOTHING;
}

#if !_DEBUG
DEFINE_FAST_ALLOCATOR_EXPRESS(GameDataSubgroup)
#endif

RString GameDataSubgroup::GetText() const
{
  if( _value==NULL ) return "<NULL-group>";
  return _value->GetDebugName();
}

bool GameDataSubgroup::IsEqualTo(const GameData *data) const
{
  const AISubgroup *val1 = GetGroup();
  const AISubgroup *val2 = static_cast<const GameDataSubgroup *>(data)->GetGroup();
  return val1==val2;
}

LSError GameDataSubgroup::Serialize(ParamArchive &ar)
{
  CHECK(base::Serialize(ar));
  CHECK(ar.SerializeRef("value", _value, 1))
  return LSOK;
}

#define SUB_GROUP_NULL GameValueExt((AISubgroup *)NULL)

GameValue ObjSubGroup(const GameState *state, GameValuePar oper1)
{
  Object *obj1 = GetObject(oper1);
  EntityAI *ai = dyn_cast<EntityAI>(obj1);
  if( !ai ) return SUB_GROUP_NULL;
  AIBrain *brain = ai->CommanderUnit();
  if( !brain ) return SUB_GROUP_NULL;
  AIUnit *unit = brain->GetUnit();
  if( !unit ) return SUB_GROUP_NULL;
  AISubgroup *grp = unit->GetSubgroup();
  return GameValueExt(grp);
}

static AISubgroup *GetSubGroup( GameValuePar oper )
{
  if( oper.GetType()==GameSubgroup )
  {
    return static_cast<GameDataSubgroup *>(oper.GetData())->GetGroup();
  }
  return NULL;
}

GameValue SubGroupUnits( const GameState *state, GameValuePar oper1 )
{
  AISubgroup *grp1 = GetSubGroup(oper1);

  // create list of units
  GameValue value = state->CreateGameValue(GameArray);
  GameArrayType &array = value;
  if (grp1)
  {
    array.Realloc(grp1->NUnits());
    for (int i=0; i<grp1->NUnits(); i++)
    {
      AIUnit *unit = grp1->GetUnit(i);
      if (!unit) continue;
      EntityAI *veh = unit->GetPerson();
      array.Add(GameValueExt(veh));
    }
  }
  return value;
}

GameValue SubGroupLeader( const GameState *state, GameValuePar oper1 )
{
  AISubgroup *grp1 = GetSubGroup(oper1);
  if (!grp1) return OBJECT_NULL;
  AIUnit *leader = grp1->Leader();
  if (!leader) return OBJECT_NULL;
  EntityAI *leaderVeh = leader->GetPerson();
  return GameValueExt(leaderVeh);
}

static AIUnit *GetUnit( const GameState *state, GameValuePar oper1 )
{
  Object *obj = GetObject(oper1);
  EntityAI *veh = dyn_cast<EntityAI>(obj);
  if (!veh) return NULL;
  AIBrain *unit = veh->CommanderUnit();
  //if (!unit) return NULL;
  return unit ? unit->GetUnit() : NULL;
}

GameValue SubGroupCreate( const GameState *state, GameValuePar oper1 )
{
  OLinkPermNOArray(AIUnit) list;
  const GameArrayType &array = oper1;
  AIGroup *grp = NULL;
  for (int i=0; i<array.Size(); i++)
  {
    AIUnit *unit = GetUnit(state, array[i]);
    if (!unit) continue;
    AIGroup *g = unit->GetGroup();
    if (!g) continue;
    if (grp && g!=grp) continue; // different group
    grp = g;
    list.AddUnique(unit);
  }
  if (!grp) return SUB_GROUP_NULL;
  Command cmd;
  cmd._message = Command::NoCommand;
  cmd._discretion = Command::Undefined;
  cmd._context = Command::CtxMission;
  cmd._id = grp->GetNextCmdId();
  grp->IssueCommand(cmd,list);
  return GameValueExt(list[0]->GetSubgroup());
}

GameValue SubGroupSetLeader( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
  AISubgroup *grp1 = GetSubGroup(oper1);
  if (!grp1) return NOTHING;
  Object *obj=GetObject(oper2);
  if( !obj ) return NOTHING;
  EntityAI *ai = dyn_cast<EntityAI>(obj);
  if( !ai ) return NOTHING;
  AIBrain *brain = ai->CommanderUnit();
  if( !brain ) return NOTHING;
  AIUnit *unit = brain->GetUnit();
  if( !unit ) return NOTHING;
  for (int i=0; i<grp1->NUnits(); i++)
  {
    AIUnit *aUnit = grp1->GetUnit(i);
    if (!aUnit) continue;
    if (aUnit == unit)
    {
      grp1->SetLeader(unit);
      return NOTHING;
    }
  }  
  return NOTHING;
}

GameValue SubGroupSetFormation( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
  AISubgroup *subgrp = GetSubGroup(oper1);
  if (!subgrp) return NOTHING;
  GameStringType str = oper2;
  AI::Formation form = AI::FormColumn;  // avoid warning
  const EnumName *names = GetEnumNames(form);
  for (int i=0; names[i].IsValid(); i++)
  {
    if (stricmp(names[i].name, str) == 0)
    {
      form = (AI::Formation)names[i].value;
      subgrp->SetFormation(form);
      return NOTHING;
    }
  }
  return NOTHING;
}

GameValue SubGroupFormation( const GameState *state, GameValuePar oper1 )
{
  AISubgroup *subgrp = GetSubGroup(oper1);
  if (!subgrp) return "ERROR";
  AI::Formation form = subgrp->GetFormation();
  const EnumName *names = GetEnumNames(form);
  for (int i=0; names[i].IsValid(); i++)
  {
    if (names[i].value == form) return names[i].name;
  }
  return "ERROR";
}

GameValue GetPreviewPicture( const GameState *state, GameValuePar oper1 )
{
  if (oper1.GetType() != GameString)
  {
    if (state) state->TypeError(GameString,oper1.GetType());
    return RString();
  }
  RString type = oper1;
  if(type.IsEmpty()) return RString();

  ConstParamEntryPtr ptr = (Pars >> "CfgVehicles").FindEntry(type);
  if (ptr)
  {
    ParamEntryVal entry = *ptr;
    if (entry.FindEntry("preview") || entry.FindEntry("model"))
    {
      RString path = entry.ReadValue("preview",RString());
      if(path.GetLength()) //no config entry found, look for model
      {
        const char *ptr = strchr(path, '.');
        if (!ptr)
          path = path +".paa";
      }
      else
      {
        RString model = entry >> "model";
        RString modelNoExt = model;

        const char *ptr = strchr(model, '.');
        if (ptr)
          modelNoExt = RString(model, ptr - model);

        ptr = strrchr(modelNoExt, '\\');
        if (ptr)
        {
          RString textName(ptr + 1); 
          path = RString(modelNoExt, ptr - modelNoExt);

          // full path to texture
          path = path + "\\data\\ico\\preview_" + textName + "_ca.paa";
        }
      }
      if (path.GetLength() > 0 && QFBankQueryFunctions::FileExists(path + 1))
        return path;        
    }
  }
  return RString();
}


GameValue ObjectToNetworkId(const GameState *state,GameValuePar oper1)
{
  GameValue value = state->CreateGameValue(GameArray);
  GameArrayType &array = value;

  Object *obj = GetObject(oper1);
  if(!obj) return array;

  array.Resize(2);

  NetworkId id = obj->GetNetworkId();

  array[0] = float(id.creator);
  if(id.creator == STATIC_OBJECT)
  {
    //hack to pass an integer as float
    float *myid = (float*)&id.id;
    array[1] = *myid;
  }
  else //note that float precision could lead to problems here with high numbers!
  {
    array[1] = float(id.id);
  }

  return array;
}

class FindNetworkId
{
public:
  mutable Entity *&_entity;
  const NetworkId &_networkId;
  FindNetworkId(Entity *&entity,const NetworkId &networkId):_entity(entity),_networkId(networkId){};
  __forceinline bool operator () (Entity *veh) const
  {
    NetworkId id = veh->GetNetworkId();
    if(id==_networkId) 
    {
      _entity = veh;
      return true;
    }
    return false;
  };
};


GameValue NetworkIdToObject(const GameState *state,GameValuePar oper1)
{
  if(!CheckType(state,oper1,GameArray))
    return OBJECT_NULL;

  const GameArrayType &array1 = oper1;
  if (array1.Size() == 0) return OBJECT_NULL;

  if (!CheckSize(state, array1, 2) || !CheckType(state, array1[0], GameScalar) || !CheckType(state, array1[1], GameScalar))
    return OBJECT_NULL;

  NetworkId id;
  id.creator = int(float(array1[0]));

  if(id.creator == STATIC_OBJECT)
  {
    //hack to pass an integer as float
    float tmpid = array1[1];
    id.id      = *((int*)&tmpid);
    ObjectId oid;
    oid.Decode(id.id);
    return GameValueExt(GLandscape->GetObject(oid));
  }
  else
  { //only in network mode
    id.id = int(float(array1[1]));
    if (GWorld->GetMode() == GModeNetware)
    {
      NetworkObject* nwObj = GetNetworkManager().GetObject(id);
      Object* obj = dyn_cast<Object>(nwObj);
      return GameValueExt(obj);
    }
  }
  return OBJECT_NULL;
}

RString FindScript(RString name);
GameValue ForceCloseFile(const GameState *state,GameValuePar oper1)
{
  if(!CheckType(state,oper1,GameString))
  {
    state->TypeError(GameString,oper1.GetType());
    return NOTHING;
  }

  RString filename = FindScript(oper1);
  if (filename.GetLength() == 0)
    NOTHING;

  GFileServerFunctions->FlushReadHandle(filename);

  return NOTHING;
}

// Forward declaration.
//void PositionToCustomFormat(Vector3Val pos, char *buffer, const char *format);
void PositionToAA11(Vector3Val pos, char *buffer);

void GridConversion(RString &xgrid, RString &ygrid, float &xPos, float &yPos )
{
  // convert the strings over to the int representation
  int xIntPos = 0; 
  int yIntPos = 0;
  for( int i = 0; i < xgrid.GetLength(); ++i) 
    xIntPos = (xIntPos * 10) + (xgrid[i] - '0');
  for( int i = 0; i < ygrid.GetLength(); ++i) 
    yIntPos = (yIntPos * 10) + (ygrid[i] - '0');

  if (!GWorld) return;

  const GridInfo *info = GWorld->GetGridInfo(0);
  float offsetX = GWorld->GetGridOffsetX();
  float offsetY = GWorld->GetGridOffsetY();
  float sizeLand = LandGrid * LandRange;

  xPos = (xIntPos / info->invStepX) + offsetX; 
  yPos = -((yIntPos / info->invStepY) + offsetY - sizeLand);
}

GameValue GridToPosition(const GameState *state,GameValuePar oper1)
{
  RString xgrid,ygrid;
  
  // check if we're dealing with a string, or an array of strings?
  if(oper1.GetType() == GameString)
  {
    RString gridPos = oper1;
    int length = gridPos.GetLength()/2;
    if((length*2) != gridPos.GetLength())
      return NOTHING;
      
    xgrid = gridPos.Substring(0,length);
    ygrid = gridPos.Substring(length,gridPos.GetLength());
  }
  else if(oper1.GetType() == GameArray)
  {
    const GameArrayType &array = oper1;
    if(!CheckSize(state,array,2))
      return NOTHING;
    
    if(!CheckType(state,array[0],GameString))
    {
      state->TypeError(GameString,array[0].GetType());
      return NOTHING;
    }
    if(!CheckType(state,array[1],GameString))
    {
      state->TypeError(GameString,array[1].GetType());
      return NOTHING;
    }
    
    xgrid = array[0];
    ygrid = array[1];
  }
  else
    return NOTHING;

  if(xgrid.GetLength() != ygrid.GetLength())
    return NOTHING;

  if(xgrid.GetLength() < 3 ||
     ygrid.GetLength() < 3 ) 
     return NOTHING;

  // make sure its only 3 significant digits
//   xgrid = xgrid.Substring(0,xgrid.GetLength()-(xgrid.GetLength()-3));
//   ygrid = ygrid.Substring(0,ygrid.GetLength()-(ygrid.GetLength()-3));

  char buff[32] = {0};
  //const char *gridFormat = "000";
  PositionToAA11(VZero,buff);

  RString zeroPos(buff);
  float origX;
  float origY;
  float posX;
  float posY;

  int length = zeroPos.GetLength() / 2;
  GridConversion(zeroPos.Substring(0,length),zeroPos.Substring(length,length*2),origX,origY);
  GridConversion(xgrid,ygrid,posX,posY);

  posX -= origX;
  posY -= origY;

  GameValue returnArray = state->CreateGameValue(GameArray);
  GameArrayType &rtn = returnArray;
  rtn.Resize(2);

  rtn[0] = posX;
  rtn[1] = posY;
  
  return returnArray;
}


GameValue PositionToGrid(const GameState *state,GameValuePar oper1)
{
  if(!CheckType(state,oper1,GameArray))
  {
    state->TypeError(GameArray,oper1.GetType());
    return NOTHING;
  }

  const GameArrayType &array = oper1;
  if( array.Size() != 2 &&
      array.Size() != 3 )
  {
    state->SetError(EvalDim, array.Size(), 3);
    return NOTHING;
  }
    
  if(!CheckType(state,array[0],GameScalar))
  {
    state->TypeError(GameScalar,array[0].GetType());
    return NOTHING;
  }
  if(!CheckType(state,array[1],GameScalar))
  {
    state->TypeError(GameScalar,array[1].GetType());
    return NOTHING;
  }

  // Add to vector
  char buff[32] = {0};
  //const char *gridFormat = "000";
  Vector3 pos(array[0],0.0f,array[1]);
  
  PositionToAA11(pos,buff);
  RString gridPos(buff);

  return gridPos;
}



GameValue CreateIndirectDamage( const GameState *state, GameValuePar oper1 )
{
  const GameArrayType &array = oper1;
  if( !CheckSize(state, array, 3)
      || !CheckType(state, array[0], GameArray)    // position
      || !CheckType(state, array[1], GameScalar)   // indirectHit
      || !CheckType(state, array[2], GameScalar)   // indirectHitRange
    )   
    return NOTHING;

  Vector3 pos;
  GetPos(pos,array[0]);

  AmmoHitInfo type;
  
  type.hit              = 0;
  type.indirectHit      = array[1];
  type.indirectHitRange = array[2];
  type.explosive        = 1;
  type._type            = NULL;

  GLandscape->ExplosionDamage(NULL, NULL, NULL, pos, VZero, -1, type);
  return NOTHING;
}

#if _LASERSHOT
extern int laserShotWeaponCount;
extern AutoArray<RString> laserShotWeapons;
extern AutoArray<RString> laserShotAmmo;
#endif

GameValue SetLasershotWeapon( const GameState *state, GameValuePar oper1)
{
#if _LASERSHOT
  const GameArrayType &array = oper1;

  if( array.Size() < 2 || array.Size() > 3
    || !CheckType(state, array[0], GameScalar)    // position
    || !CheckType(state, array[1], GameString)   // indirectHit
    ) 
    return NOTHING;

  int id = float(array[0]);

  if(id < 0 || id >= laserShotWeaponCount)
    return NOTHING;

  RString weaponName = array[1];
  Ref<WeaponType> wType = WeaponTypes.New(weaponName);
  if(!wType)
    return NOTHING;

  if(laserShotWeapons.Size() < laserShotWeaponCount)
  {
    laserShotWeapons.Resize(laserShotWeaponCount);
    laserShotAmmo.Resize(laserShotWeaponCount);
  }

  RString& str = laserShotWeapons[id];
  str = weaponName;

  RString& ammoStr = laserShotAmmo[id];
  if(array.Size() == 3)
  {
    RString ammoName = array[2];

    if(ammoName.GetLength() > 0)
      ammoStr = ammoName;
  }
  else
    ammoStr = "";

#endif
  return NOTHING;
}

GameValue RoadSurfaceHeight(const GameState *state, GameValuePar oper1)
{
  Vector3 pos;
  if (!GetRelPos(state, pos, oper1)) return NOTHING;
  float hgt = GLandscape->RoadSurfaceY(pos[0],pos[2]);
  return hgt;
}

GameValue IsExternalControlled(const GameState *state, GameValuePar oper1)
{
#if _EXT_CTRL
  Object *obj = GetObject(oper1);
  if (!obj) return NOTHING;

  return obj->IsExternalControlled();
#else
  return false;
#endif
}

GameValue ObjCanFloat(const GameState *state, GameValuePar oper1)
{
	Object *obj = GetObject(oper1);
	if (!obj) return NOTHING;

  bool rtn = false;

  Entity *veh = dyn_cast<Entity>(obj);
  EntityAI *vehAI = dyn_cast<EntityAI>(veh);
  if (vehAI)
  {
    if (veh->CanFloat())
      rtn = true;
  }

	return rtn;
}

GameValue GroupSelectUnit(const GameState *state, GameValuePar oper1)
{
	Object *obj = GetObject(oper1);
	if (!obj) return NOTHING;

  EntityAI *veh = dyn_cast<EntityAI>(obj);
  if (!veh) return NOTHING;

  AIBrain *brain = veh->CommanderUnit();
  if (!brain) return NOTHING;

  AbstractUI *ui = GWorld->UI();

  AIUnit *unit = brain->GetUnit();
  ui->SelectUnit(unit);

  if (!ui->IsShownMenu())
    ui->ShowMenu(true);

	return NOTHING;
}

GameValue GroupUnSelectUnit(const GameState *state, GameValuePar oper1)
{
	Object *obj = GetObject(oper1);
	if (!obj) return NOTHING;

  EntityAI *veh = dyn_cast<EntityAI>(obj);
  if (!veh) return NOTHING;

  AIBrain *brain = veh->CommanderUnit();
  if (!brain) return NOTHING;

  AIUnit *unit = brain->GetUnit();
  GWorld->UI()->UnselectUnit(unit);

	return NOTHING;
}

GameValue GroupClearSelected(const GameState *state)
{
  GWorld->UI()->ClearSelectedUnits();
	return NOTHING;
}

GameValue GroupListSelected(const GameState *state)
{
  OLinkPermNOArray(AIUnit) list;
  GWorld->UI()->ListSelectedUnits(list);

  GameValue value = state->CreateGameValue(GameArray);
  GameArrayType &array = value;

  for (int i=0; i<list.Size(); i++)
  {
    AIUnit *unit = list[i];
    if (!unit) continue;

    Person *person = unit->GetPerson();
    if (!person) continue;

    array.Add(GameValueExt(person));
  }

	return value;
}

GameValue ObjRespawn(const GameState *state, GameValuePar oper1)
{
	Object *obj = GetObject(oper1);
	if (!obj) return NOTHING;

	Person *person = dyn_cast<Person>(obj);

	if (person)
		GetNetworkManager().AskForRespawn(person);

	return NOTHING;
}

GameValue ObjMakeAirborne(const GameState *state, GameValuePar oper1)
{
	Object *obj = GetObject(oper1);
	if (!obj) return 0.0f;

  EntityAI *vehAI = dyn_cast<EntityAI>(obj);
  if (!vehAI) return 0.0f;

  if (!vehAI->GetType()->IsKindOf(GWorld->Preloaded(VTypeAir)))
    return 0.0f;

  return vehAI->MakeAirborne();
}

GameValue ObjIsAirborne(const GameState *state, GameValuePar oper1)
{
	Object *obj = GetObject(oper1);
	if (!obj) return false;

  Transport *vehAI = dyn_cast<Transport>(obj);
  if (!vehAI) return false;

  if (!vehAI->GetType()->IsKindOf(GWorld->Preloaded(VTypeAir)))
    return false;

  return vehAI->Airborne();
}

GameValue ObjPlaceOnSurface(const GameState *state, GameValuePar oper1)
{
	Object *obj = GetObject(oper1);
	if (!obj) return NOTHING;

  if (obj->GetType() == Primary || obj->GetType() == Network) return NOTHING;

  Vector3 pos = obj->Position();
  Matrix4 trans = obj->Transform();
  trans.SetPosition(pos);

  EntityAI *veh = dyn_cast<EntityAI>(obj);
  if (veh)
  {  
    veh->PlaceOnSurface(trans);
    trans.SetPosition(pos);
    veh->IsMoved();

    obj->Move(trans);
    obj->OnPositionChanged();
    GScene->GetShadowCache().ShadowChanged(obj);
  }

  return NOTHING;
}

DEF_RSB(slope)
DEF_RSB(placement)
DEF_RSB(vertical)

GameValue ObjClipLandKeep(const GameState *state, GameValuePar oper1)
{
	Object *obj = GetObject(oper1);
	if (!obj) return false;

  Entity *objT = dyn_cast<Entity>(obj);
  if (!objT) return false;

  LODShape *shape = obj->GetShape();
  if (!shape) return false;

  return (
    shape->GetOrHints()&(ClipLandKeep|ClipLandOn)
    || shape->PropertyValue(RSB(placement))==RSB(slope)
    || shape->PropertyValue(RSB(placement))==RSB(vertical)
    || objT->Type()->_placement==PlacementVertical    
  );
}

GameValue ObjWatchMode(const GameState *state, GameValuePar oper1)
{
	Object *obj = GetObject(oper1);
	if (!obj) return "ERROR";

  EntityAI *vai = dyn_cast<EntityAI>(obj);
  if (!vai) return "ERROR";
  
  AIBrain *unit = vai->CommanderUnit();
  if (!unit) return "ERROR";

  AIBrain::WatchMode wmode = unit->GetWatchMode();
  const EnumName *names = GetEnumNames(wmode);
  for (int i=0; names[i].IsValid(); i++)
  {
    if (names[i].value == wmode) return names[i].name;
  }  

  return "ERROR";
}

void ExportWMF(const char *name, bool grid,bool contour);

GameValue ExportEmf(const GameState *state, GameValuePar oper1)
{
  const GameArrayType &array = oper1;

  if( array.Size() < 1 || array.Size() > 3
    || !CheckType(state, array[0], GameString)  // filename
    ) 
    return NOTHING;

  RString fileName = array[0];
  bool grid = true;
  bool contour = true;

  if(array.Size() > 1)
  {
    if(!CheckType(state, array[1], GameBool)) return NOTHING;
    grid = array[1];
    if(array.Size() > 2)
    {
      if(!CheckType(state, array[2], GameBool)) return NOTHING;
      contour = array[2];
    }
  }

  ExportWMF(cc_cast(fileName), grid, contour);

  return NOTHING;
}

GameValue SetOrigin(const GameState *state, GameValuePar oper1)
{
  const GameArrayType &array = oper1;
  if(!CheckSize(state, array,4))
    return NOTHING;

  if(!CheckType(state, array[0], GameScalar)) return NOTHING;
  float easting = array[0];

  if(!CheckType(state, array[1], GameScalar)) return NOTHING;
  float northing = array[1];

  if(!CheckType(state, array[2], GameScalar)) return NOTHING;
  float zone = array[2];

  if(!CheckType(state, array[3], GameString)) return NOTHING;
  RString hemisphere = array[3];
  hemisphere.Lower();

  if(hemisphere.GetLength() == 0) return NOTHING; //we should rather display an error?
  char h = hemisphere[0];

  GWorld->SetUtmInfo(UTMInfo(easting, northing, zone, h == 'n'));

  return NOTHING;
}

GameValue GetOrigin(const GameState *state)
{
  GameValue value = state->CreateGameValue(GameArray);
  GameArrayType &array = value;
  array.Add(float(Glob.header.utmInfo.easting));
  array.Add(float(Glob.header.utmInfo.northing));
  array.Add(float(Glob.header.utmInfo.zone));

  RString hemisphere = Glob.header.utmInfo.north ? "N" : "S";
  array.Add(hemisphere);
  
  return value;
}

GameValue GetFPS(const GameState *state)
{
  GameValue value = state->CreateGameValue(GameArray);
  GameArrayType &array = value;
  if(GEngine)
  {
    array.Add(1000.0f/GEngine->GetLastFrameDuration());
    array.Add(1000.0f/GEngine->GetAvgFrameDuration(16));
    array.Add(1000.0f/GEngine->GetMaxFrameDuration(16));
  }
  return value;
}

#include "hla/ShapeLayers.hpp"

GameValue LoadShape(const GameState *state,GameValuePar oper1)
{
#if _VBS3_SHAPEFILES
  const GameArrayType &array = oper1;

  float originX, originY;
  RString name;

  if(array.Size() == 1 && CheckType(state, array[0], GameString))
  {
    name = array[0];

    originX = Glob.header.utmInfo.easting;
    originY = Glob.header.utmInfo.northing;
  }
  else
  {
    if( 
          array.Size() != 3
          || !CheckType(state, array[0], GameScalar)    // position
          || !CheckType(state, array[1], GameScalar)    // position
          || !CheckType(state, array[2], GameString)    // file
      )
      return false;

    originX =  array[0];
    originY =  array[1];

    name = array[2];
  }
  if(name.GetLength()==0)
  {
    LogF("String path length is zero");
    return false;
  }
  
  int index = GShapeLayers.AddShape(name);
  ShapeLayer *shape = GShapeLayers.GetShape(index);
  if(shape)
  {
    shape->SetOrigin(originX, originY);
    if(!shape->Project())
    {
      LogF("[m] Projection of shape file failed");
      return false;
    }
  }else
    LogF("[m] Load of shape file failed");

  return GameValue(float(index));
#else
  return GameValue(-1.0f);
#endif
}

GameValue TriggerActivated(const GameState *state,GameValuePar oper1)
{
#if _VBS3
  if (!CheckType(state, oper1, GameObject)) return false;
  Detector *det = GetDetector(oper1);
  if (det) 
    return det->IsActive();
#endif
  return false;
}

GameValue UnLoadShape(const GameState *state,GameValuePar oper1)
{
#if _VBS3_SHAPEFILES

  if(!(oper1.GetType() & (GameScalar|GameString)))
  {
    state->TypeError(GameString, oper1.GetType());
    return NOTHING;
  }

  int index = -1;
  if(oper1.GetType() == GameString)
  {
    RString name = oper1;
    index = GShapeLayers.GetShapeIndex(name);
  }
  else
    index = int(float(oper1));

  if(index > -1)
    GShapeLayers.RemoveShape(index);
  else
    LogF("[m] UnLoadShape: Index = %d", index);
#endif

  return NOTHING;
}

GameValue SystemTime(const GameState *state)
{
  GameValue valueOut = state->CreateGameValue(GameArray);
  GameArrayType &arrayOut = valueOut;

  // Get the system time
  SYSTEMTIME sysTime;
  GetLocalTime(&sysTime);

  // Add the system time then return
  arrayOut.Add((float) sysTime.wYear);
  arrayOut.Add((float) sysTime.wMonth);
  arrayOut.Add((float) sysTime.wDay);
  arrayOut.Add((float) sysTime.wHour);
  arrayOut.Add((float) sysTime.wMinute);
  arrayOut.Add((float) sysTime.wSecond);
  arrayOut.Add((float) sysTime.wMilliseconds);

  return valueOut;
}

//Return array of loaded shape files
GameValue ListShapeFiles(const GameState *state)
{
  GameValue valueOut = state->CreateGameValue(GameArray);

#if _VBS3_SHAPEFILES
  GameArrayType &arrayOut = valueOut;
  int size = GShapeLayers.GetCount();
  for(int i=0; i<size; ++i)
  {
    arrayOut.Add(RString(GShapeLayers.GetShape(i)->GetName()));
  }
#endif
  return valueOut;
}

GameValue GetShapeProperty(const GameState *state,GameValuePar oper1)
{
  GameValue valueOut = state->CreateGameValue(GameArray);

#if _VBS3_SHAPEFILES
  GameArrayType &arrayOut = valueOut;
  const GameArrayType &array = oper1;

  if( CheckSize(state, array, 2) 
    && (array[0].GetType() & (GameScalar|GameString))    // index or string
    && CheckType(state, array[1], GameString)
    )  
  {
    ShapeLayer *shape;
    if(array[0].GetType() == GameScalar) 
      shape = GShapeLayers.GetShape(int(float(array[0])));
    else
      shape = GShapeLayers.GetShape(RString(array[0]));

    if(!shape)
    {
      LogF("[m] GetShapeProperty: no shape found");    
      return valueOut;
    }

    RString propertyName = array[1];
    propertyName.Lower();

    // Set color of shape
    if(strcmpi(cc_cast(propertyName), "color") ==0)
    {
      PackedColor color = shape->GetColor();
      Color col(color);
      arrayOut.Add(col.R());
      arrayOut.Add(col.G());
      arrayOut.Add(col.B());
      arrayOut.Add(col.A());
      LogF("[m] get shape Color: %f %f %f %f", col.R(), col.G(), col.B(), col.A());
    }
    else
      LogF("[m] property not found: %s", cc_cast(propertyName));
  }
  else
    LogF("[m] GetShapeProperty: invalid parameter");
#endif
  return valueOut;
}
// Set Shape properties like color
GameValue SetShapeProperty(const GameState *state,GameValuePar oper1)
{
#if _VBS3_SHAPEFILES
  const GameArrayType &array = oper1;

  if( !CheckSize(state, array, 3) 
    || !(array[0].GetType() & (GameScalar|GameString))    // index or string
    || !CheckType(state, array[1], GameString)           // string as property
    )    // property
    return false;

  ShapeLayer *shape;
  if(array[0].GetType() == GameScalar) 
    shape = GShapeLayers.GetShape(int(float(array[0])));
  else
    shape = GShapeLayers.GetShape(RString(array[0]));

  if(!shape)
    return false;

  RString propertyName = array[1];
  propertyName.Lower();

  // Set color of shape
  if(strcmpi(cc_cast(propertyName), "color") ==0)
  {
    if(!CheckType(state, array[2], GameArray)) return false;
    const GameArrayType &propertyValue = array[2];
    if(propertyValue.Size() <3)
    {
      state->SetError(EvalDim, propertyValue.Size(), 3);
      return false;
    }
    for (int i=0; i<3; i++)
      if (!CheckType(state, propertyValue[i], GameScalar)) return false;

    float alpha = 1;
    if(propertyValue.Size() > 3)
      alpha = propertyValue[3];
    Color color(propertyValue[0], propertyValue[1], propertyValue[2], alpha);
    shape->SetColor(PackedColor(color));
  }
  return true;
#else
  return false;
#endif
}

GameValue MapOnMarkerCreated( const GameState *state, GameValuePar oper1 )
{
#if _VBS3 // marker EH
  GMarkerOnCreated = oper1;
#endif
  return NOTHING;
}

GameValue MapOnMarkerDeleted( const GameState *state, GameValuePar oper1 )
{
#if _VBS3 // marker EH
  GMarkerOnDeleted = oper1;
#endif
  return NOTHING;
}

GameValue GetTerrainGrid(const GameState *state)
{
  return GLandscape->GetTerrainGrid();
}

/*
\VBS_patch 1.19 Date [3/27/2008] by Mark
- Added: normal is returned as well
*/
// CollisionDetection [getposasl start,getposasl end, radius, ignoreObject]
//returns [[obj,pos,norm],[..]]
GameValue CollisionDetection( const GameState *state, GameValuePar oper1 )
{
  GameValue rtn_array = state->CreateGameValue(GameArray);
  GameArrayType &rtn_arrayType = rtn_array;

  if(oper1.GetType() != GameArray ) return rtn_array;
  
  const GameArrayType &array = oper1;
  if(array.Size() < 3 || array.Size() > 4) return rtn_array;

  if(!CheckType(state,array[0],GameArray)  ||
     !CheckType(state,array[1],GameArray)  ||
     !CheckType(state,array[2],GameScalar))
     return rtn_array;

  const GameArrayType &from = array[0];
  const GameArrayType &to   = array[1];
  float   radius            = array[2];
  Object *ignoreObject = NULL;

  // check if ignoreobject is provided
  if(array.Size() == 4)
  {
    if(!CheckType(state,array[3],GameObject)) return rtn_array;

    ignoreObject = GetObject(array[3]);
  }

  if( from.Size() != 3 ||
      !CheckType(state,from[0],GameScalar) ||
      !CheckType(state,from[1],GameScalar) ||
      !CheckType(state,from[2],GameScalar))
    return rtn_array;

  if( to.Size() != 3 ||
      !CheckType(state,to[0],GameScalar) ||
      !CheckType(state,to[1],GameScalar) ||
      !CheckType(state,to[2],GameScalar))
    return rtn_array;

  Vector3 vFrom(from[0],from[2],from[1]);
  Vector3 vTo(to[0],to[2],to[1]);
  
  Vector3 dir = vTo - vFrom;
  float maxDist = dir.Size();
  dir.Normalize();

  // First check if we have a ground collection
  Vector3 isect;
  bool  worldCollision = false;
  float tGround = GLandscape->IntersectWithGroundOrSea(&isect,vFrom,dir,0,maxDist+50.0f);

  // we've hit the ground, check if there is an object collision
  if( tGround < maxDist )
  {
    vTo = vFrom + dir * tGround;
    worldCollision = true;
  }

  CollisionBuffer result;

  // collision with everthing
  for( int i = ObjIntersectFire; i < ObjIntersectNone; ++i )
    GLandscape->ObjectCollision(result,NULL,ignoreObject,vFrom,vTo,radius,(ObjIntersect) i);

  // do a linear search and remove duplicated objects
  for( int i = 0; i < result.Size(); ++i )
  {
    const CollisionInfo &first = result[i];

    for( int x = (i + 1); x < result.Size(); ++x)
    {
      if(x >= result.Size())
        continue;

      const CollisionInfo &second = result[x];
      if( first.object == second.object )
      {
        result.Delete(x,1);
        --x;
      }
    }
  }

  // Fill the found objects and then return
  // need to check all components types, fire,genometry,ect..
  for( int i = 0; i < result.Size(); ++i )
  {
    const CollisionInfo &col = result[i];

    GameValue element = state->CreateGameValue(GameArray);
    GameArrayType &atElement = element;
    
    GameValue gvPos = state->CreateGameValue(GameArray);
    GameArrayType &vPos = gvPos;
    vPos.Add(GameValue(col.pos[0]));
    vPos.Add(GameValue(col.pos[2]));
    vPos.Add(GameValue(col.pos[1]));

    GameValue gvNorm = state->CreateGameValue(GameArray);
    GameArrayType &vNorm = gvNorm;
    Vector3 norm = col.dirOutNotNorm.Normalized();

    col.object->DirectionModelToWorld(norm, norm);

#if 0 //_ENABLE_CHEATS
    GVBSVisuals.Add3dLine(col.pos, col.pos + norm * 0.3, PackedWhite);

    Vector3 norm2 = col.dirOut;
    col.object->DirectionModelToWorld(norm2, norm2);
    GVBSVisuals.Add3dLine(col.pos, col.pos + norm2 * 0.3, PackedBlack);
#endif

    vNorm.Add(GameValue(norm[0]));
    vNorm.Add(GameValue(norm[2]));
    vNorm.Add(GameValue(norm[1]));

    // Fill the elment with the information about the collision
    atElement.Add(GameValueExt(col.object));
    atElement.Add(gvPos);
    atElement.Add(gvNorm);

    // Add the element to the array
    rtn_arrayType.Add(element);
  }

  if(worldCollision) //add an extra element for world collision
  {
    GameValue element = state->CreateGameValue(GameArray);
    GameArrayType &atElement = element;

    //world collision
    atElement.Add(OBJECT_NULL); 

    //add impact position
    GameValue gvPos = state->CreateGameValue(GameArray);
    GameArrayType &vPos = gvPos;
    vPos.Add(GameValue(isect[0]));
    vPos.Add(GameValue(isect[2]));
    vPos.Add(GameValue(isect[1]));
    atElement.Add(gvPos);

    //add normal
    GameValue gvNorm = state->CreateGameValue(GameArray);
    GameArrayType &vNorm = gvNorm;
    float dX,dZ;
    Texture *tex;
    GLandscape->SurfaceY(isect.X(),isect.Z(),&dX,&dZ,&tex);
    Vector3 normal(-dX,1,-dZ);
    normal.Normalize();
    vNorm.Add(GameValue(normal[0]));
    vNorm.Add(GameValue(normal[2]));
    vNorm.Add(GameValue(normal[1]));
    atElement.Add(gvNorm);

    //add to return array
    rtn_arrayType.Add(element);
  }
  return rtn_array;
}

//entity,nameCode,functionCode,laserPower,laserWaveLength
GameValue FireLaserPointSimple(const GameState *state, GameValuePar oper1)
{
  if(oper1.GetType() != GameArray ) return NOTHING;
  const GameArrayType &array = oper1;

  if( array.Size() < 5 
    || !CheckType(state,array[0], GameObject)
    || !CheckType(state,array[1], GameScalar)
    || !CheckType(state,array[2], GameScalar)
    || !CheckType(state,array[3], GameScalar)
    || !CheckType(state,array[4], GameScalar))
    return NOTHING;


  Object *obj      = GetObject(array[0]);
  if(!obj) return NOTHING;

  Entity *shooter = dyn_cast<Entity>(obj);
  if(!shooter) return NOTHING;

  int nameCode     = toInt(array[1]);
  int functionCode = toInt(array[2]);
  float laserPower = array[3];
  float laserWaveL = array[4];
  Vector3 zeroVec = VZero;

  EntityAIFull *entity = dyn_cast<EntityAIFull>(obj);
  // no shooter
  if(!entity)
    return NOTHING;
  // Use the scene manager camera, fixes a lot of
  // problems relating to who and what shot the laser
  const Camera *camera = GScene->GetCamera();
  if(!camera) return NOTHING;

  Matrix4 camMatrix = *camera;

  // Convert the point to world corrdinates
  Vector3 dir    = camMatrix.Direction();
  Vector3 wPos   = camMatrix.Position();
  dir.Normalize();

  float maxDist = Glob.config.horizontZ;

  // First check if we have a ground collection
  Vector3 isect;
  bool  worldCollision = false;
  float tGround = GLandscape->IntersectWithGroundOrSea(&isect,wPos,dir,0,maxDist+50.0f);

  // we've hit the ground, check if there is an object collision
  if( tGround < maxDist )
  {
    maxDist         = tGround;
    worldCollision = true;
  }

  // check if we've collided with an object before the
  // the ground collision
  Vector3 endpos = wPos+(dir*maxDist);


  // collision with everthing
  CollisionBuffer result;
  for( int i = ObjIntersectFire; i < ObjIntersectNone; ++i )
    GLandscape->ObjectCollision(result,NULL,entity,wPos,endpos,0,(ObjIntersect) i);

  //dirty hack for Boeing
  OLinkArray(Object) results;
  results.Add();
  results[0] = obj;

  // find the closest object. From the result
  Object *closestObj = NULL;
  Vector3 closestPos;
  if(result.Size() > 0)
  {
    float minT=1e10; int n = result.Size();
    for (int j=0; j<n; j++)
    {
      CollisionInfo &info = result[j];
      if (minT > info.under && info.object)
      {    
        closestObj = info.object;
        closestPos = info.pos;
        minT=info.under;
      }
    }

    int nameCode     = toInt(array[1]);
    int functionCode = toInt(array[2]);
    float laserPower = array[3];
    float laserWaveL = array[4];

    Entity *target = dyn_cast<Entity>(closestObj);
    if(shooter && target)
    {
      Vector3 relativePos;
      target->PositionWorldToModel(relativePos,closestPos);

      Ref<Object> obj=new ObjectColored(GScene->Preloaded(SphereModel),VISITOR_NO_ID);
      obj->SetPosition(closestPos);
      obj->SetScale(0.5f);
      obj->SetConstantColor(PackedWhite);
      GScene->ShowObject(obj);

      LogF("[m] Obj hit Shoter:%s Target:%s NameCode:%d FunctionCode:%d LaserPower:%f LasterWL:%f RelPos:%f %f %f ClosetPos:%f %f %f",
        shooter->GetDebugName().Data(),
        target->GetDebugName().Data(),
        nameCode,
        functionCode,
        laserPower,
        laserWaveL,
        relativePos[0],relativePos[1],relativePos[2],
        closestPos[0],closestPos[1],closestPos[2]);

      if (GWorld->GetMode() == GModeNetware && !GetNetworkManager().IsServer())
      {
        results.Add();
        results[1] = target;

        GetNetworkManager().PublicExec("isServer", Format("fireLaserAdvanced[ObjNull, _this select 0, _this select 1, %d,%d,%f,%f,[%f,%f,%f],[%f,%f,%f],[0,0,0]]"
          ,nameCode,functionCode,laserPower,laserWaveL
          , relativePos.X(),relativePos.Z(), relativePos.Y()
          , closestPos.X(),closestPos.Z(), closestPos.Y())
          ,NULL, results);
      }
#if _HLA
      else
        _hla.SendLaserDesignatorCreate(NULL,shooter,target,nameCode,functionCode,laserPower,laserWaveL,relativePos,closestPos,zeroVec);
#endif
      return NOTHING;
    }
    else
    {
      // target object was no entity, treat like ground intersection
      isect = closestPos;
      worldCollision = true;
    }
  }
  if(worldCollision)
  {
    Ref<Object> obj=new ObjectColored(GScene->Preloaded(SphereModel),VISITOR_NO_ID);
    obj->SetPosition(isect);
    obj->SetScale(0.5f);
    obj->SetConstantColor(PackedWhite);
    GScene->ShowObject(obj);

    LogF("[m] Ground: Shoter:%s Target:%s NameCode:%d FunctionCode:%d LaserPower:%f LasterWL:%f InterSec:%f %f %f",
      shooter->GetDebugName().Data(),
      "",
      nameCode,
      functionCode,
      laserPower,
      laserWaveL,
      isect[0],isect[1],isect[2]);

    if (GWorld->GetMode() == GModeNetware && !GetNetworkManager().IsServer())
    {
      //fireLaserAdvanced[ObjNull, _this, ObjNull, %1,%2,%3,%4,%5,%6,%7]",_DesignatorNameCode, _DesignatorFunctionCode, _DesignatorPower, _DesignatorWavelength, _relativePos, _worldPos, _linAcc], _designatingId]
      GetNetworkManager().PublicExec("isServer", Format("fireLaserAdvanced[ObjNull, _this select 0, ObjNull, %d,%d,%f,%f,[0,0,0],[%f,%f,%f],[0,0,0]]"
        ,nameCode,functionCode,laserPower,laserWaveL
        , isect.X(),isect.Z(), isect.Y())
        ,NULL, results);
    }
#if _HLA
    else
      _hla.SendLaserDesignatorCreate(NULL,shooter,NULL,nameCode,functionCode,laserPower,laserWaveL,zeroVec,isect,zeroVec);
#endif //_HLA
  }
  //send nothing if it hits nothing

  return NOTHING;
}


GameValue FireLaserPointAdvanced(const GameState *state, GameValuePar oper1)
{
#if _HLA
  if(oper1.GetType() != GameArray) return NOTHING;
  const GameArrayType &array = oper1;

  if(array.Size() < 10 
    || !CheckType(state,array[0], GameObject)
    || !CheckType(state,array[1], GameObject)
    || !CheckType(state,array[2], GameObject)
    || !CheckType(state,array[3], GameScalar)
    || !CheckType(state,array[4], GameScalar)
    || !CheckType(state,array[5], GameScalar)
    || !CheckType(state,array[6], GameScalar)
    || !CheckType(state,array[7], GameArray )
    || !CheckType(state,array[8], GameArray )
    || !CheckType(state,array[9], GameArray ))
    return NOTHING;

  Entity *id = dyn_cast<Entity>(GetObject(array[0]));
  Entity *designatingId = dyn_cast<Entity>(GetObject(array[1]));
  Entity *designatedId  = dyn_cast<Entity>(GetObject(array[2]));

  int designatorNameCode     = toInt(array[3]);
  int designatorFunctionCode = toInt(array[4]);
  float designatorPower      = array[5];
  float designatorWavelength = array[6];

  const GameArrayType &relPos   = array[7];
  const GameArrayType &worldPos = array[8];
  const GameArrayType &linAcc   = array[9];

  if(relPos.Size()   != 3 ||
    worldPos.Size() != 3 ||
    linAcc.Size()   != 3)
    return NOTHING;

  _hla.SendLaserDesignatorCreate
  (
    id,
    designatingId,
    designatedId,
    designatorNameCode,
    designatorFunctionCode,
    designatorPower,
    designatorWavelength,
    Vector3(relPos[0],relPos[2],relPos[1]),
    Vector3(worldPos[0],worldPos[2],worldPos[1]),
    Vector3(linAcc[0],linAcc[2],linAcc[1])
  );

#endif //_HLA
  return NOTHING;
}


GameValue GetTerrainHeight(const GameState *state, GameValuePar oper1)
{
  const GameArrayType &array = oper1;
  if(array.Size() < 2
    || !CheckType(state, array[0], GameScalar)  // x
    || !CheckType(state, array[1], GameScalar)  // z
    )
    return -1.0f;

  float x = array[0];
  float z = array[1];

  float invTerraindGrid = GLandscape->GetInvTerrainGrid();

  int xx = toInt(x * invTerraindGrid);
  int zz = toInt(z * invTerraindGrid);

  if (TerrainInRange(zz, xx))
  {
    return GLandscape->GetHeight(zz, xx);
  }
  return -1.0f;
}

GameValue GetTerrainHeightArea(const GameState *state, GameValuePar oper1)
{
  const GameArrayType &array = oper1;

  GameValue value = state->CreateGameValue(GameArray);
  GameArrayType &arrayOut = value;

  if(!CheckSize(state,array,2)
    || !CheckType(state, array[0], GameArray)  // Corner 1
    || !CheckType(state, array[1], GameArray)  // Corner 2
    ) 
    return NOTHING;

  const GameArrayType &xz1 = array[0];
  const GameArrayType &xz2 = array[1];

  if(xz1.Size() < 2) return NOTHING;
  if(!CheckType(state, xz1[0], GameScalar)) return NOTHING;
  if(!CheckType(state, xz1[1], GameScalar)) return NOTHING;

  if(xz2.Size() < 2) return NOTHING;
  if(!CheckType(state, xz2[0], GameScalar)) return NOTHING;
  if(!CheckType(state, xz2[1], GameScalar)) return NOTHING;

  float xs = xz1[0]; float zs = xz1[1];
  float xe = xz2[0]; float ze = xz2[1];

  if(xs > xe) swap(xs, xe);
  if(zs > ze) swap(zs, ze);

  int l_xx = toInt(xs * GLandscape->GetInvTerrainGrid());
  int r_xx = toInt(xe * GLandscape->GetInvTerrainGrid());

  int b_zz = toInt(zs * GLandscape->GetInvTerrainGrid());
  int t_zz = toInt(ze * GLandscape->GetInvTerrainGrid());

  float min = FLT_MAX;
  float max = FLT_MIN;
  float mid = 0; //problem with float precision for giant areas?

  for(int x = l_xx; x <= r_xx; ++x)
  {
    for(int z = b_zz; z <= t_zz; ++z)
    {
      if (TerrainInRange(z, x))
      {
        float actHeight = GLandscape->GetHeight(z,x); //reversed order!!
        if(actHeight < min) min = actHeight;
        if(actHeight > max) max = actHeight;
        mid += actHeight;
      }
    }
  }
  int nodeCount = (r_xx - l_xx +1)*(t_zz - b_zz +1);
  if(nodeCount > 0)
  mid /= nodeCount;

  arrayOut.Add(min);
  arrayOut.Add(mid);
  arrayOut.Add(max);

  return arrayOut;
}

GameValue SetTerrainHeight(const GameState *state, GameValuePar oper1)
{
#if !_VBS3_CRATERS_DEFORM_TERRAIN
  return NOTHING;
#endif 

  const GameArrayType &array = oper1;

  if(array.Size() != 3
    || !CheckType(state, array[0], GameScalar)  // x
    || !CheckType(state, array[1], GameScalar)  // z
    || !CheckType(state, array[2], GameScalar)  // y
    ) 
    return NOTHING;

  float x = array[0];
  float z = array[1];
  float height = array[2];

//    Log("[m] apply new height (%d/%d): %.2f [LandGrid: %d/%d]", xx,zz,height, xxLg, zzLg);
  GLandscape->RT_HeightChange(x, z, height);
 
  return NOTHING;
}

//undo's any modification in the terrain heightfield
GameValue ResetHeight(const GameState *state)
{
  GLandscape->RestoreOldHeightLevels();
  return NOTHING;
}

GameValue GetUserChart(const GameState *state)
{
#if _VBS3_UDEFCHART
  return GWorld->GetUserDefinedChartName();
#else
  return NOTHING;
#endif
}

GameValue GetUserChartDrawObjects(const GameState *state)
{
#if _VBS3_UDEFCHART
  return GWorld->GetDrawObjectsOnUserDefinedChart();
#else
  return NOTHING;
#endif
}

GameValue SetTerrainHeightArea(const GameState *state, GameValuePar oper1)
{
#if !_VBS3_CRATERS_DEFORM_TERRAIN
  return NOTHING;
#endif 

  const GameArrayType &array = oper1;

  if(array.Size() < 3
    || !CheckType(state, array[0], GameArray)  // Corner 1
    || !CheckType(state, array[1], GameArray)  // Corner 2
    || !CheckType(state, array[2], GameScalar)  // y
    ) 
    return NOTHING;

  const GameArrayType &xz1 = array[0];
  const GameArrayType &xz2 = array[1];
  float height = array[2];
  
  if(!CheckType(state, xz1[0], GameScalar)) return NOTHING;
  if(!CheckType(state, xz1[1], GameScalar)) return NOTHING;

  if(!CheckType(state, xz2[0], GameScalar)) return NOTHING;
  if(!CheckType(state, xz2[1], GameScalar)) return NOTHING;
  
  bool changeLower = true;
  bool changeHigher = true;

  if(array.Size() > 3 && CheckType(state, array[3], GameBool))
    changeLower = array[3];

  if(array.Size() > 4 && CheckType(state, array[4], GameBool))
    changeHigher = array[4];

  GLandscape->RT_HeightChangeArea(xz1[0], xz2[0], xz1[1], xz2[1], height, changeLower, changeHigher);

  return NOTHING;
}

#include "es/Files/fileContainer.hpp"
extern RString GetMissionDirectory();

static bool AddFoundFile(const FileItem &file, AutoArray<RString> &list)
{
  list.Add(file.filename);
  return false; // continue with enumeration
}

GameValue ListFiles(const GameState *state, GameValuePar oper1)
{
  GameValue value = state->CreateGameValue(GameArray);
  GameArrayType &arrayOut = value;
#if _VBS3_SHAPEFILES

  //todo: prevent to go to higher directories than mission/ user folder
  const GameArrayType &arrayIn = oper1;

  if(arrayIn.Size() > 0 || CheckType(state, arrayIn[0], GameString))
  {

    RString mask = "*.*";
    if(arrayIn.Size() > 1 && CheckType(state, arrayIn[1], GameString))
      mask = arrayIn[1];

    bool userDir = true;
    if(arrayIn.Size() > 2 && CheckType(state, arrayIn[2], GameBool))
      userDir = arrayIn[2];


    AutoArray <RString> list;
    RString directoryBase, directory;
    if(userDir)
      directoryBase = GetUserDirectory();
    else
      directoryBase = GetMissionDirectory();

    directory = directoryBase + arrayIn[0];

    if(directory[directory.GetLength()-1] != '\\')
      directory = directory + "\\";

    ForMaskedFile(directory, mask, AddFoundFile, list);
    LogF("[m] Search files: %s%s Found: %d", cc_cast(directory), cc_cast(mask), list.Size());

    for(int i=0; i<list.Size();++i)
    {
      GameValue value = state->CreateGameValue(GameArray);
      GameArrayType &arr = value;
      arr.Add(list[i]);
      arr.Add(arrayIn[0]);
      arrayOut.Add(value);
    }
  }
#endif
  return arrayOut;
}

GameValue CreateMessageBox(const GameState *state, GameValuePar oper1)
{
  const GameArrayType &array = oper1;
  
  if (array.Size() != 2)
  {
    if (state) state->SetError(EvalDim,array.Size(),2);
    return GameValueExt((ControlsContainer *)NULL);
  }
  
  ControlsContainer *display = GetDisplay(array[0]);
  if (!display) return GameValueExt((ControlsContainer *)NULL);

  if (array[1].GetType() != GameString)
  {
    if (state) state->TypeError(GameString,array[1].GetType());
    return GameValueExt((ControlsContainer *)NULL);
  }
  RString text = array[1];

//   AutoArray<KeyHint> hints;
//   hints.Realloc(2);
//   hints.Resize(2);
//   hints[0].key = INPUT_DEVICE_XINPUT + XBOX_A;
//   hints[0].hint = LocalizeString(IDS_DISP_XBOX_HINT_YES);
//   hints[1].key = INPUT_DEVICE_XINPUT + XBOX_B;
//   hints[1].hint = LocalizeString(IDS_DISP_XBOX_HINT_NO);        
  display->CreateMsgBox(MB_BUTTON_OK | MB_BUTTON_CANCEL, text);

  return GameValueExt(display->GetMsgBox());
}

GameValue MergeConfigFile(const GameState *state, GameValuePar oper1)
{

  //todo: prevent to go to higher directories than mission/ user folder
  const GameArrayType &arrayIn = oper1;

  if(arrayIn.Size() == 0 || !CheckType(state, arrayIn[0], GameString))
    return NOTHING;

  RString name = arrayIn[0];

  bool userDir = true;
  if(arrayIn.Size() > 1 && CheckType(state, arrayIn[1], GameBool))
    userDir = arrayIn[1];


  RString directoryBase, fullName;
  if(userDir)
    directoryBase = GetUserDirectory();
  else
    directoryBase = GetMissionDirectory();

  //if it's an absolute path take the full name
  if(name.Find(":") == -1)
    fullName = directoryBase + name;
  else
  {
    fullName = name;
  }

  if (QFBankQueryFunctions::FileExists(fullName))
  {
    SRef<ParamFile> addon = new ParamFile;
    LogF("[m] parsing: %s", cc_cast(fullName));
    addon->Parse(fullName, NULL, NULL, GWorld->GetMissionNamespace());
    // TODO: no owner set!
    Pars.Update(*addon);
    Pars.SetFile(&Pars);
    //we clear the vehicletype cache to activate changes in our config classes, when creating a new vehicle
    VehicleTypes.ClearCache();
    VehicleTypes.CleanUp();
  }
  return NOTHING;
}

GameValue GetConfigClasses( const GameState *state, GameValuePar oper1, GameValuePar oper2 )
{
  const GameConfigType &parent = static_cast<GameDataConfig *>(oper2.GetData())->GetConfig();

  GameValue results = state->CreateGameValue(GameArray);
  GameArrayType &arrayOut = results;

  GameVarSpace local(state->GetContext(), false);
  state->BeginContext(&local);

  if (parent.entry && parent.entry->IsClass())
  {
    int n = parent.entry->GetEntryCount();
    for( int i=0; i< n ; i++ )
    {
      if(parent.entry->GetEntry(i).IsClass())
      {
        GameConfigType val;
        val.entry = &parent.entry->GetEntry(i);
        int n = parent.path.Size();
        val.path.Realloc(n + 1);
        val.path.Resize(n + 1);
        for (int i=0; i<n; i++) val.path[i] = parent.path[i];
        val.path[n] = val.entry->GetName();

        GameValue gVal = GameValueExt(val);
        // set local variable - will be deleted on EndContext
        state->VarSetLocal("_x",gVal,true);
        if(state->EvaluateMultiple((RString)oper1, GameState::EvalContext::_default, GWorld->GetMissionNamespace()))
        {
          arrayOut.Add(gVal);
        }

        if( state->GetLastError())
        {
          state->EndContext();
          return results;
        }
      }
    }
  }
  state->EndContext();
  return results;
}

GameValue SetTIBlurLevel(const GameState *state, GameValuePar oper1)
{
  if( oper1.GetType() != GameScalar)
  {
    if(state) state->TypeError(GameScalar,oper1.GetType());
    return NOTHING;
  }

#if _VBS3_TI
  GEngine->SetTIBlur(oper1);
#endif

  return NOTHING;
}

GameValue SetTIBrightness(const GameState *state, GameValuePar oper1)
{
  if( oper1.GetType() != GameScalar)
  {
    if(state) state->TypeError(GameScalar,oper1.GetType());
    return NOTHING;
  }

#if _VBS3_TI
  GEngine->SetTIBrightnessRelative(oper1);
#endif

  return NOTHING;
}

GameValue SetTIContrast(const GameState *state, GameValuePar oper1)
{
  if( oper1.GetType() != GameScalar)
  {
    if(state) state->TypeError(GameScalar,oper1.GetType());
    return NOTHING;
  }

#if _VBS3_TI
  GEngine->SetTIContrast(oper1);
#endif

  return NOTHING;
}

GameValue SetTIOverride(const GameState *state, GameValuePar oper1)
{
  if( oper1.GetType() != GameBool)
  {
    if(state) state->TypeError(GameBool,oper1.GetType());
    return NOTHING;
  }

#if _VBS3_TI
  extern bool thermalCheat;
  thermalCheat = oper1;
#endif

  return NOTHING;
}

GameValue SetTIMode(const GameState *state, GameValuePar oper1)
{
  if( oper1.GetType() != GameScalar)
  {
    if(state) state->TypeError(GameScalar,oper1.GetType());
    return NOTHING;
  }

#if _VBS3_TI
  GEngine->SetTIMode(toInt(oper1));
#endif

  return NOTHING;
}

GameValue SetTIAutoBC(const GameState *state, GameValuePar oper1)
{
  if( oper1.GetType() != GameBool)
  {
    if(state) state->TypeError(GameBool,oper1.GetType());
    return NOTHING;
  }

#if _VBS3_TI
  GEngine->SetTIAutoBC((bool)oper1);
#endif

  return NOTHING;
}

GameValue SetTIAutoBCContrastCoef(const GameState *state, GameValuePar oper1)
{
  if( oper1.GetType() != GameScalar)
  {
    if(state) state->TypeError(GameScalar,oper1.GetType());
    return NOTHING;
  }

#if _VBS3_TI
  GEngine->SetTIAutoBCContrastCoef(oper1);
#endif

  return NOTHING;
}

GameValue GetTIBlurLevel(const GameState *state)
{
#if _VBS3_TI
  return GEngine->GetTIBlur();
#else
  return -1.0f;
#endif
}

GameValue GetTIBrightness(const GameState *state)
{
#if _VBS3_TI
  return GEngine->GetTIBrightness();
#else
  return -1.0f;
#endif
}

GameValue GetTIContrast(const GameState *state)
{
#if _VBS3_TI
  return GEngine->GetTIContrast();
#else
  return -1.0f;
#endif
}

GameValue GetTIMode(const GameState *state)
{
#if _VBS3_TI
  return (float) GEngine->GetTIMode();
#else
  return -1.0f;
#endif
}

GameValue GetTIAutoBC(const GameState *state)
{
#if _VBS3_TI
  return GEngine->GetTIAutoBC();
#else
  return false;
#endif
}

GameValue GetTIAutoBCContrastCoef(const GameState *state)
{
#if _VBS3_TI
  return GEngine->GetTIAutoBCContrastCoef();
#else
  return -1.0f;
#endif
}

GameValue SetFlashBang(const GameState *state, GameValuePar oper1)
{
  if( oper1.GetType() != GameScalar)
  {
    if(state) state->TypeError(GameScalar,oper1.GetType());
    return NOTHING;
  }

#if _VBS3
  GEngine->SetFlashBang(oper1);
#endif

  return NOTHING;
}

static IControl *GetCtrl( const GameState *state, GameValuePar oper1)
{
  int idc = toInt((float)oper1);
  LogF("[m] idc = %d", idc);
  ControlsContainer *dlg = GetUserDialog();
  if (!dlg) return NULL;
  IControl *ctrl = dlg->GetCtrl(idc);
  return ctrl;
}

GameValue ListBoxGetPosIndex( const GameState *state, IControl *ctrl, GameValuePar oper2)
{
  if (!ctrl) {LogF("[m] no Control!"); return -1.0f;}
  CListBox *lb = static_cast<CListBox *>(ctrl);

  if (!lb) {LogF("[m] no ListBox!"); return -1.0f;}

  if(!CheckType(state, oper2, GameArray))
    return -1.0f;

  const GameArrayType &arrayIn = oper2;

  if(arrayIn.Size() < 2 
    || !CheckType(state, arrayIn[0], GameScalar)
    || !CheckType(state, arrayIn[1], GameScalar)
    )
  {
    LogF("[m] invalid array!"); 
    return -1.0f;
  }
  
  return (float)lb->GetPosIndex(arrayIn[0], arrayIn[1]);

}

GameValue LBGetPosIndex( const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
  IControl *ctrl = GetControl(oper1);
  return ListBoxGetPosIndex(state, ctrl, oper2);
}

GameValue UILBGetPosIndex( const GameState *state, GameValuePar oper1, GameValuePar oper2)
{
  IControl *ctrl = GetCtrl(state, oper1);
  return ListBoxGetPosIndex(state, ctrl, oper2);
}

GameValue VehGetForceSpeed(const GameState *state, GameValuePar oper1)
{
  Object *obj = GetObject(oper1);
  EntityAI *veh = dyn_cast<EntityAI>(obj);
  if (!veh) return NOTHING;
  AIBrain *unit = veh->PilotUnit();
  if (!unit) return NOTHING;
  return unit->GetForceSpeed();
}

GameValue Screenshot(const GameState *state, GameValuePar oper1)
{
#if _VBS3_SCREENSHOT
  const GameArrayType &array = oper1;
  if (!CheckSize(state, array, 2)) return NOTHING;
  if (!CheckType(state, array[0], GameString)) return NOTHING;
  if (!CheckType(state, array[1], GameString)) return NOTHING;
  GEngine->SaveScreenshot(array[0], array[1]);
#endif
  return NOTHING;
}
// -------------------------------------------------------------------------------
// Cheat only functions
// -------------------------------------------------------------------------------

GameValue SaveWorld(const GameState *state)
{
  GLandscape->SaveData("C:\\Output.wrp");
  //  GLandscape->SaveOptimized("C:\\Output.wrp");
  return NOTHING;
}

// -------------------------------------------------------------------------------
// /!- Cheat only functions
// -------------------------------------------------------------------------------

#define OPERATORS_VBS(XX, Category) \
  XX(GameScalar,              "getVisibility",          function, GetLOS,                     GameObjectOrArray,  GameObjectOrArray,  "obj|array of obj|AGL position",  "visibility",                     "Returns the visibility of an object to another from 0 (no vis) to 1 (full vis).", "player getVisibility house", "", "1.18", "", Category) \
  XX(GameNothing,             "getGroundIntercept",     function, GroundIntercept,            GameObjectOrArray,  GameArray,          "obj|AGL position",               "direction",                      "Returns the ground intercept point (position) along the specified direction.", "_intercept = player getGroundIntercept (player weaponDirection (primaryWeapon player))", "", "1.18", "", Category) \
  XX(GameBool,                "isCollision",            function, GetLOP,                     GameObjectOrArray,  GameObjectOrArray,  "obj|AGL position",               "obj|array of obj",               "Returns true if a fired projectile would impact an object (or objects) if flying from the specified position.", "_isImpact = player isCollision soldier", "", "1.18", "", Category) \
  XX(GameScalar,              "getCargoPos",            function, GetCargoPos,                GameObject,         GameObject,         "vehicle",                        "Unit",                           "Returns the position of the unit in Cargo", "_index = _Jeep getCargoPos player", "", "VBS 0.10", "", Category) \
  XX(GameObject,              "getUnitAtCargoPos",      function, GetUnitAtCargoPos,          GameObject,         GameScalar,         "vehicle",                        "CargoIndex",                     "Returns the unit for the Cargo Index", "_unit = _Jeep getCargoPos 1", "", "VBS 0.10", "", Category) \
  XX(GameNothing,             "setCursorType",          function, UISetCursorType,            GameDisplay,        GameString,         "display",                        "cursor type",                    "Set the type of the cursor. Cursor types are defined in config (class CfgWrapperUI::Cursors).", "_display setCursorType \"Arrow\"", "", "1.18", "", Category) \
  XX(GameNothing,             "setObjectTextureGlobal", function, ObjSetTextureGlobal,        GameObject,         GameArray,          "obj",                            "[selection,texture,evaluation]", "Set the texture of the given selection on all computers in a network session. Evaluation is a string expression executed on each client, if true on a client then the texture is set on that client.", "player setObjectTextureGlobal [0,\"\\MyAddon\\blue.pac\",\"side player == side _this\"]", "", "1.18", "", Category) \
  XX(GameNothing|GameScalar,  "addEventHandler",        function, GrpAddEventHandler,         GameGroup,          GameArray,          "group",                          "handler",                        "The format of handler is [type,command]. Check scripting topic Event handlers for more information. The index of the currently added handler is returned.", "(group player) addEventHandler [\"WaypointComplete\",{hint \"waypoint complete\"}]", "", "1.18", "", Category) \
  XX(GameNothing,             "removeEventHandler",     function, GrpRemoveEventHandler,      GameGroup,          GameArray,          "group",                          "handler",                        "Removes event handler added by <f>addEventHandler</f>. Format of handler is [type,index]. Index is returned by addEventHandler. When any handler is removed, all handler indices higher that the deleted one should be decremented.", "(group player) removeEventHandler [\"WaypointComplete\",0]", "", "1.18", "", Category) \
  XX(GameNothing,             "removeAllEventHandlers", function, GrpRemoveAllEventHandlers,  GameGroup,          GameString,         "group",                          "handlerType",                    "Removes all event handlers of the given type which were added by <f>addEventHandler</f>.", "(group player) removeAllEventHandlers \"WaypointComplete\"", "", "1.18", "", Category) \
  XX(GameNothing,             "setMarkerCondition",     function, MarkerSetCondition,         GameString,         GameString,         "name",                           "condition",                      "Sets the marker condition, which is game code that must evaluate to true or false. If true, the marker will be shown. The marker is modified on all computers in a network session.", "\"Marker1\" setMarkerCondition \"side player == WEST\"", "", "1.18", "", Category) \
  XX(GameNothing,             "setMarkerAttach",        function, MarkerSetAttach,            GameString,         GameArray,          "name",                           "[object,offset,_this]",          "Attaches the marker to an object. object is game code that must evaluate to an object, offset is positional offset from the object that the marker will be drawn. _this is an array of objects to pass into condition as _this. The marker is modified on all computers in a network session.", "\"Marker1\" setMarkerAttach [\"player\",[0,0]]", "", "1.18", "", Category) \
  XX(GameNothing,             "setMarkerAttachLocal",   function, MarkerSetAttachLocal,       GameString,         GameArray,          "name",                           "[object,offset,_this]",          "Attaches the marker to an object. object is game code that must evaluate to an object, offset is positional offset from the object that the marker will be drawn. _this is an array of objects to pass into condition as _this. The marker is only modified on the computer where the command is called.", "\"Marker1\" setMarkerAttachLocal [\"player\",[0,0]]", "", "1.18", "", Category) \
  XX(GameNothing,             "setMarkerLayers",        function, MarkerSetLayers,            GameString,         GameArray,          "name",                           "[layers]",                       "Sets marker layers (multiple icons all of which are drawn). The marker is modified on all computers in a network session.", "\"Marker1\" setMarkerLayers [\"pic1.paa\",\"pic2.paa\"]", "", "1.18", "", Category) \
  XX(GameNothing,             "setMarkerLayersLocal",   function, MarkerSetLayersLocal,       GameString,         GameArray,          "name",                           "[layers]",                       "Sets marker layers (multiple icons all of which are drawn). The marker is only modified on the computer where the command is called.", "\"Marker1\" setMarkerLayersLocal [\"pic1.paa\",\"pic2.paa\"]", "", "1.18", "", Category) \
  XX(GameNothing,             "setMarkerAutoSize",      function, MarkerSetAutoSize,          GameString,         GameBool,           "name",                           "autosize",                       "If autosize is true, markers will resize in proportion to current map zoom. If false (default), markers will always stay the same size. The marker is modified on all computers in a network session.", "\"startMarker\" setMarkerAutoSize true", "", "1.18", "", Category) \
  XX(GameNothing,             "setMarkerAutoSizeLocal", function, MarkerSetAutoSizeLocal,     GameString,         GameBool,           "name",                           "autosize",                       "If autosize is true, markers will resize in proportion to current map zoom. If false (default), markers will always stay the same size. The marker is only modified on the computer where the command is called.", "\"startMarker\" setMarkerAutoSizeLocal true", "", "1.18", "", Category) \
  XX(GameScalar,              "bindKey",                function, BindKey,                    GameString | GameScalar, GameString,         "keystroke",                      "command",                        "Assigns a key binding based on the action or the Code, returns the index of the assignment.", "", "", "1.18", "", Category) \
  XX(GameNothing,             "createUnitLocal",        function, UnitCreateLocal,            GameString,         GameArray,          "type",                           "unitInfo",                       "Creates a unit of the given type. The format of unitInfo is:\r\n[pos (<ar>Position</ar>), group (<t>Group</t>), init (<t>String</t>), skill (<t>Number</t>), rank (<t>String</t>)].\r\nNote: init, skill and rank are optional. Their default values are \"\", 0.5, \"PRIVATE\".", "\"SoldierWB\" createUnitLocal [getMarkerPos \"barracks\", groupAlpha]", "", "1.18", "", Category) \
  XX(GameNothing,             "enablePersonalItems",    function, UnitEnablePersonalItems,    GameObject,         GameBool,           "unit",                           "bool",                           "Enables personal items. Used for units inside vehicles to enable their personal equipment.", "player enablePersonalItems true", "", "1.18", "", Category) \
  XX(GameArray,               "weaponPosition",         function, ObjGetWeaponPosition,       GameObject,         GameString,         "vehicle",                        "weaponName",                     "Returns position of given weapon is model coordinates.", "_dir = _vehicle weaponPosition \"M16\"", "", "1.18", "", Category) \
  XX(GameArray,               "weaponAimingAt",         function, ObjGetWeaponAimingAt,       GameObject,         GameArray,          "vehicle",                        "[weaponName, max dis, radius]",  "Returns the objects the weapon is aiming at, or an empty array if none.", "_tgt = _vehicle weaponAimingAt [\"M16\",500,1]", "", "1.18", "", Category) \
  XX(GameBool,                "isLookingAt",            function, ObjIsLookingAt,             GameObject,         GameArray,          "unit",                           "[object,angle]",                 "Returns true if the first object is looking at the second object (within the angle specified).", "", "", "1.18", "", Category) \
  XX(GameNothing,             "camSetAttachedLookDir",  function, CamSetAttachedLookDir,      GameObject,         GameArray,          "camera",                         "direction",                      "Explicitly sets the direction the camera will look if it is attached to a vehicle (via attachTo). Changes are committed instantly.", "_camera camSetAttachedLookDir [0,0,0]", "", "1.18", "", Category) \
  XX(GameNothing,             "camSetBankIfAttached",   function, CamSetBankIfAttached,       GameObject,         GameBool,           "camera",                         "bool",                           "If true the camera will bank with an attached object. If false it will always be oriented upwards. Changes are committed instantly.", "_camera camSetBankIfAttached false", "", "1.18", "", Category) \
  XX(GameNothing,             "lbSetText",              function, UILBSetText,                GameControl,        GameArray,          "control",                        "[index, text]",                  "Sets the text in the item with the given index of the given listbox or combobox to the given data.", "_control lbSetText [1, \"#1\"]", "", "1.18", "", Category) \
  XX(GameBool,                "in",                     function, TextInStr,                  GameString,         GameString,         "string1",                        "string2",                        "Returns true if string1 is found within string2. Case sensitive.", "", "", "1.18", "", Category) \
  XX(GameScalar,              "find",                   function, TextFind,                   GameString,         GameString,         "string1",                        "string2",                        "Finds string1 in string2, returns the postion where found, or -1 if not found.", """test1"" find ""1""", "4", "2.92", "", Category) \
  XX(GameNothing,             "treeSelectItemData",     function, UITreeSelectItemData,       GameControl,        GameString,         "control",                        "string",                         "Selects the tree item containing the specified hidden text.", "_control treeSelectItemData \"somedata\"", "", "1.18", "", Category) \
  XX(GameNothing,             "ctrlSetAutoComplete",    function, UICtrlSetAutoComplete,      GameControl,        GameString,         "control",                        "autocomplete mode",              "Sets the autocomplete mode for the given edit box.", "_control ctrlSetAutoComplete \"scripting\"", "", "1.18", "", Category) \
  XX(GameNothing,             "setLeader",              function, SubGroupSetLeader,          GameSubgroup,       GameObject,         "subgroup",                       "object",                         "Sets the leader unit for a subgroup.", "", "", "1.18", "", Category) \
  XX(GameNothing,             "setFormation",           function, SubGroupSetFormation,       GameSubgroup,       GameString,         "subgroup",                       "formation",                      "Sets the group formation. Formation is one of: \"COLUMN\", \"STAG COLUMN\", \"WEDGE\", \"ECH LEFT\", \"ECH RIGHT\", \"VEE\" or \"LINE\".", "groupOne setFormation \"LINE\"", "", "", "", Category) \
  XX(GameNothing,             "setTracker",             function, SetTracker,                 GameObject,         GameArray,          "object",                         "[sensorId,AppId]",               "Set the entity to the tracker id, and application id. When used in conjunction with ObjAttachTo will move the pos/orien of the object", "", "", "1.18", "", Category) \
  XX(GameNothing,             "ctrlSetRowHeight",       function, UICtrlSetRowHeight,         GameControl,        GameScalar,         "control",                        "height",                         "Sets the row height of given control.", "", "", "1.18", "", Category) \
  XX(GameNothing,             "setFormationDir",        function, SetFormationDir,            GameObject|GameGroup, GameScalar,       "obj",                            "direction",                      "Sets the group formation direction. Accepts object|man|vehicle", "veh setformationdir 90", "", "1.18", "", Category) \
  XX(GameString,              "getObjTexture",          function, GetObjTexture,              GameObject,         GameScalar|GameString,"obj",                          "index or string",                "Gets the texture name for the hidden selection index or string", "_txt = obj getObjTexture 0", "", "1.18", "", Category) \
  XX(GameScalar,              "addTriggerOnActivated",  function, TriggerSetActivationEH,     GameObject,         GameString,         "trigger",                        "on activation EH",               "Sets optional script that is executed when the trigger activates. Returns a value that represents the EH.", "", "", "1.18", "", Category) \
  XX(GameNothing,             "removeTriggerOnActivated",function,TriggerClearActivationEH, GameObject,         GameScalar,         "trigger",                        "index",                          "Clears optional script that is executed when the trigger activates.", "", "", "1.18", "", Category) \
  XX(GameArray,               "turretWeaponDirection",  function, ObjGetTurretWeaponDirection,GameObject,         GameArray,          "vehicle",                        "[weaponPath,weaponName]",        "Returns direction of the turret. Weaponname is optional", "_dir = _vehicle weaponDirection [0]", "", "1.19", "", Category) \
  XX(GameNothing,             "setWeaponDirection",     function, ObjSetTurretWeaponDirection,GameObject,         GameArray,          "vehicle",                        "[turretPath,[azimuth, elevation]{,transition = true}]","Sets the direction of the turret. If the boolean value transition is false, the turret 'jumps' to it's destination.", "_vehicle SetWeaponDirection [[0],[_azi,_elev]]", "", "1.19", "", Category) \
  XX(GameNothing,             "DisableGunnerInput",     function, DisableGunnerInput         ,GameObject,         GameArray,          "vehicle",                        "[turretPath,disabled]",           "Disables Gunner input", "_vehicle DisableGunnerInput [[0],true]", "", "2.61", "", Category) \
  XX(GameConfig,              "addClass",               function, ConfigClassAdd,             GameConfig,         GameString,         "class",                          "classname",                      "Create new subclass of class.", "_class = configFile addClass \"NewClass\"", "", "1.19", "", Category) \
  XX(GameNothing,             "addValue",               function, ConfigValueAdd,             GameConfig,         GameArray,          "class",                          "[name, value]",                  "Add new value to class, if value exists then the value is updated.", "myClass addValue [\"NewValue\",4]", "", "1.19", "", Category) \
  XX(GameConfig,              "deleteClass",            function, ConfigClassDelete,          GameConfig,         GameString,         "class",                          "classname",                      "Delete subclass of class.", "_class = configFile deleteClass \"NewClass\"", "", "1.19", "", Category) \
  XX(GameScalar,              "lbPosIndex",             function, LBGetPosIndex,              GameControl,        GameArray,          "Listbox",                        "[x, y]",                         "Returns the index of the Listbox under the position x,y", "_index = _lb lbPosIndex [_posx, _posy]", "", "1.19", "", Category) \
  XX(GameScalar,              "lbPosIndex",             function, UILBGetPosIndex,            GameScalar,         GameArray,          "Listbox IDC",                    "[x, y]",                         "Returns the index of the Listbox under the position x,y", "_index = _lbIdc lbPosIndex [_posx, _posy]", "", "1.19", "", Category) \
  XX(GameNothing,             "DisplayText",            function, DisplayObjectText,          GameObject,         GameArray,          "obj",                            "[""text"",[RelPosition to COMPosition], true/false, [R,G,B,A],SIZE]", "Display text above the object, can use 2D mode that stays the same size or 3D mode that changes with distance", "player DisplayText [""Hello World"",[0,0,0],false,[0,0,0,1],0.05]", "", "", "", Category) \
  XX(GameNothing,             "removeWeaponCargo",      function, RemoveWeaponCargo,          GameObject,         GameString,         "obj",                            "vehicle",                        "Removes the weapon from objects cargo area.", "car removeWeaponCargo ""m16""", "", "", "", Category) \
  XX(GameNothing,             "removeMagazineCargo",    function, RemoveMagazineCargo,        GameObject,         GameString|GameArray,"obj",                           "vehicle",                        "Removes the magazine from objects cargo area.", "car RemoveMagazineCargo ""M16Mag""", "", "", "", Category) \
  XX(GameNothing,             "removeMagazineTurret",   function, RemoveMagazineTurret,       GameObject,         GameArray,          "obj",                            "vehicle turret",                 "Removes the magazine from vehicles turret", "car RemoveMagazineTurret [""M16Mag"",[turret path]]", "", "", "", Category) \
  XX(GameNothing,             "triggerAttachObj",       function, TriggerAttachObj,           GameObject,         GameObject,         "trigger",                        "obj",                            "Assigns any game object to the trigger.", "trigger triggerAttachObj player", "", "1.19", "", Category) \
  XX(GameNothing,             "triggerDetachObj",       function, TriggerDetachObj,           GameObject,         GameObject,         "trigger",                        "obj",                            "Detachs a game object from the trigger.", "trigger triggerDetachObj player", "", "1.19", "", Category) \
  XX(GameNothing,             "lbFillVehicleClass",     function, UILBFillVehicleClass,       GameControl,        GameArray,          "control",                        "[type,filter]",                  "Automatically fills a list box with vehicle classes matching the specified type and filter. Type may be ""VEH"", ""UNIT"" or ""OBJECT"".", "", "", "1.19", "", Category) \
  XX(GameNothing,             "lbFillVehicleType",      function, UILBFillVehicleType,        GameControl,        GameString,         "control",                        "vehicle class",                  "Automatically fills a list box with vehicle types of the specified vehicle class.", "", "", "1.19", "", Category) \
  XX(GameNothing,             "addMagazineEx",          function, ObjAddMagazineEx,           GameObject,         GameArray,          "unit",                           "[magazineName,ammoCount]",       "Adds a magazine to the unit with set ammo count. Note: you may create invalid combinations by using this function, for example by adding 20 grenades. When doing so, application behaviour is undefined.", "player addMagazine [\"M16\", 12]", "", "", "", Category) \
  XX(GameNothing,             "addMagazineCargoEx",     function, ObjAddMagazineCargoEx,      GameObject,         GameArray,          "unit",                           "[magazineName,ammoCount]",       "Adds magazines to the cargo space. The format of magazines is [magazineName, AmmoCount].", "rearmTruckOne addMagazineCargo [\"M16\", 5]", "", "", "", Category) \
  XX(GameNothing,             "SetLightMode",           function, SetLightMode,               GameObject,         GameScalar,         "unit",                           "Mode",                           "Set Light mode (Day=1, NV=2, TI=4, combinations possible)", "player setLightMode 3", "", "1.19", "", Category) \
  XX(GameNothing,             "setVariable",            function, WaypointSetVariable,        GameArray,          GameArray,          "waypoint",                       "[name, value]",                  "Set variable to given value in the variable space of given waypoint.", "", "", "1.19", "", Category) \
  XX(GameVoid,                "getVariable",            function, WaypointGetVariable,        GameArray,          GameString,         "waypoint",                       "name",                           "Return the value of variable in the variable space of given waypoint.", "", "", "1.19", "", Category) \
  XX(GameNothing,             "setVariable",            function, GroupSetVariable,           GameGroup,          GameArray,          "group",                          "[name, value]",                  "Set variable to given value in the variable space of given group.", "", "", "1.19", "", Category) \
  XX(GameVoid,                "getVariable",            function, GroupGetVariable,           GameGroup,          GameString,         "group",                          "name",                           "Return the value of variable in the variable space of given group.", "", "", "1.19", "", Category) \
  XX(GameArray,               "selectionCenter",        function, ObjGetSelectionCenter,      GameObject,         GameString,         "object",                         "selection name",                 "Search for selection in the object model (Fire Geo only). Returns center in model space.", "", "", "1.19", "", Category) \
  XX(GameArray,               "configClasses",          function, GetConfigClasses,           GameString,         GameConfig,         "condition",                      "config",                         "Returns an array of config entries which meet the criteria in the code.", """(getnumber _x >> ""vbs_entity"") == 1"" configClasses (configfile >> ""CfgWeapons"")", "", "2.35", "", Category) \
  XX(GameNothing,             "disableGeo",             function, ObjDisableGeo,              GameObject,         GameArray,          "object",                         "[vis, fire, view, geo]",         "Disable certain Lods by setting their values to true. Default values are false. (Prototype!)", "", "", "1.19", "", Category) \
  XX(GameNothing,             "allowSpeech",            function, AllowSpeech,                GameObject,         GameBool,           "object",                         "disable/enable voice",           "Disable or enable AI voice.", "", "", "1.19", "", Category) \
  XX(GameArray,               "FiringSolution",         function, ObjGetFiringSolution,       GameArray,          GameArray,          "[<veh>,<turret-path> {,muzzle}]","[target pos]",                   "Returns [[azimuth, elevation], time]. If time is -1 no solution was found. Muzzlename is optional", "_sol = [_vehicle,[0]] firingSolution [getpos _target]", "", "1.19", "", Category) \
  XX(GameNothing,             "TurretLockOn",           function, ObjSetTurretLockOn,         GameObject,         GameArray,          "vehicle",                        "[turretPath,Position or Object]","Locks the Turret onto an object or position. ", "_vehicle TurretLockOn [[0],_position]", "", "1.19 5631", "", Category) \

/*
  XX(GameNothing, "onShotHit", function, OnShotHit, GameObject, GameString, "obj", "command", "Eventhandler that executes a command when fired rounds impact. Command receives:<br/>\r\n<br/>\r\n_this (<t>object</t>) - shell that just impacted<br/>\r\n_pos (<t>array</t>) - position<br/>\r\n_vel (<t>array</t>) - velocity", "player onShotHit \"hint {round impacted}\"", "", "1.18", "", Category) \  
*/

#define OPERATORS_VBS_CHEAT(XX, Category) \
  XX(GameNothing,             "commanderOverride",      function, CommanderOverride,          GameObject,         GameArray,          "Person",                        "[veh, turretpath]",               "Activates Commander Override. Pass ObjNull to deactivate.", "player commanderOverride [vehicle, _turretPath]", "", "1.19", "", Category) \


#define FUNCTIONS_VBS(XX, Category) \
  XX(GameNothing,           "consoleLog",                     ConsoleLog, GameString, "text", "Outputs text to the dedicated server console.", "consoleLog text", "", "1.18", "", Category) \
  XX(GameNothing,           "warningMessage",                 DoWarningMessage, GameString, "text", "Outputs text as a warning message", "WarningMessage text", "", "1.08", "", Category) \
  XX(GameBool,              "isPlayerControlled",             IsPlayerControlled, GameObject, "obj", "Returns true if the given object is controlled by a player somewhere on the network.", "_isAPlayer = isPlayerControlled soldier", "", "1.18", "", Category) \
  XX(GameArray,             "getVisiblePos",                  ObjGetPosVisible, GameObject, "obj", "Returns the 'visible position' of an object (AGL).", "_visiblePos = getVisiblePos player", "", "1.18", "", Category) \
  XX(GameArray,             "convertToAGL",                   ObjConvertToAGL, GameArray, "position", "Converts the given position to height above ground level (AGL).", "_agl_hgt = convertToAGL (getPosASL player)", "", "1.18", "", Category) \
  XX(GameArray,             "convertToASL",                   ObjConvertToASL, GameArray, "position", "Converts the given position to height above sea level (ASL).", "_asl_hgt = convertToASL (getPos player)", "", "1.18", "", Category) \
  XX(GameArray,             "getCrewPos",                     GetCrewPos, GameObject, "vehicle", "Returns a list of vehicle proxies and their relative positions.", "_crewPositions = getCrewPos (vehicle player)", "", "1.18", "", Category) \
  XX(GameScalar,            "waypointCurrent",                WaypointGetCurrentVBS, GameGroup, "group", "Gets the index of the current waypoint for the group.", "waypointCurrent groupOne", "", "1.18", "", Category) \
  XX(GameArray,             "waypointSynchronizedWith",       WaypointGetSynchronizedWith, GameArray, "waypoint", "Gets the list of waypoints or triggers the specified waypoint is synchronized with.", "waypointSynchronizedWith [groupOne,1]", "", "1.18", "", Category) \
  XX(GameArray,             "triggerSynchronizedWith",        TriggerGetSynchronizedWith, GameObject, "trigger", "Gets the list of waypoints or triggers the specified trigger is synchronized with.", "triggerSynchronizedWith trig", "", "1.18", "", Category) \
  XX(GameScalar,            "nWaypoints",                     WaypointGetTotalNumber, GameGroup, "waypoint", "Gets the total number of waypoints belonging to the group.", "waypointTotalNum groupOne", "", "1.18", "", Category) \
  XX(GameBool,              "isWearingNVG",                   IsWearingNVG, GameObject, "obj", "Returns true if the given unit is currently wearing NVG equipment.", "_isWearingNVG = isWearingNVG player", "", "1.18", "", Category) \
  XX(GameArray,             "uiGet3DDir",                     UIGet3DDir, GameArray, "[x,y]", "Derives 3D direction from the viewpoint from the provided 2D position. 2D position is generally mouse coordinates (between -1 for left/top and 1 for right/down). Use (coord - 0.5) / 0.5 to convert display coordinates (0..1) to mouse coordinates (-1..1).", "_3DPos = uiGet3DDir [-0.7,0.6]", "", "1.18", "", Category) \
  XX(GameObject,            "createShot",                     CreateShot, GameArray, "[ammoType,position,velocity]", "Creates a shot of the specified type.", "", "", "1.18", "", Category) \
  XX(GameString,            "markerShape",                    MarkerGetShape, GameString, "markerName", "Gets the marker shape. See <f>setMarkerShape</f>.", "markerShape \"MarkerOne\"", "", "1.18", "", Category) \
  XX(GameString,            "markerBrush",                    MarkerGetBrush, GameString, "markerName", "Gets the marker brush. See <f>setMarkerBrush</f>.", "markerBrush \"MarkerOne\"", "", "1.18", "", Category) \
  XX(GameBool,              "markerAutosize",                 MarkerGetAutoSize, GameString, "markerName", "Gets the marker autosize value. See <f>setMarkerAutosize</f>.", "markerAutosize \"MarkerOne\"", "", "1.18", "", Category) \
  XX(GameString,            "createMarkerConditional",        MarkerCreateConditional, GameArray, "[name, position, condition]", "Creates a new marker on all computers where condition equals true, at the given position. The marker name has to be unique.", "marker = createMarkerConditional [""Marker1"", position player, ""side player == WEST""]", "", "1.18", "", Category) \
  XX(GameString,            "markerCondition",                MarkerGetCondition, GameString, "markerName", "Gets the marker condition. See <f>setMarkerCondition</f>.", "markerCondition \"MarkerOne\"", "", "1.18", "", Category) \
  XX(GameArray,             "markerAttach",                   MarkerGetAttach, GameString, "markerName", "Gets the marker attachment settings. See <f>setMarkerAttach</f>.", "markerAttach \"MarkerOne\"", "", "1.18", "", Category) \
  XX(GameArray,             "markerLayers",                   MarkerGetLayers, GameString, "markerName", "Gets the marker layers. See <f>setMarkerLayers</f>.", "markerLayers \"MarkerOne\"", "", "1.18", "", Category) \
  XX(GameDisplay,           "createEditor",                   EditorCreate, GameArray, "[dialog definition, init .sqf, import .sqf]", "Creates a mission editor display.", "", "", "1.18", "", Category) \
  XX(GameNothing,           "camResetTargets",                CamResetTargets, GameObject, "camera", "Clears a camera's targets (both object and position).", "camResetTargets _camera", "", "1.18", "", Category) \
  XX(GameNothing,           "unBindKey",                      UnBindKey, GameScalar, "index", "Clears key binding.", "", "", "1.18", "", Category) \
  XX(GameNothing,           "publicExec",                     PublicExec, GameArray, "[condition,code]", "Executes the code on all computers where condition equals true.", "publicExec [\"side player == EAST\",\"player setDammage 1\"]", "", "", "1.18", Category) \
  XX(GameObject,            "createTriggerLocal",             TriggerCreateLocal, GameArray, "[type, position]", "Creates a new trigger on the given position. An object of the given type is created; this type must be a class name in CfgNonAIVehicles or CfgVehicles with simulation=detector. The trigger is created locally.", "trigger = createTriggerLocal[\"EmptyDetector\", position player]", "", "1.18", "", Category) \
  XX(GameObject,            "trim",                           TextTrim, GameArray, "[string, left, right]", "Trims a string.", "\"ex\" = trim [\"text\",1,1]", "", "1.18", "", Category) \
  XX(GameNothing,           "titleEffectProlong",             TitleEffectProlong, GameScalar, "time", "Prolongs the active title effect.", "titleEffectProlong 5", "", "1.18", "", Category) \
  XX(GameNothing,           "setEyeAccom",                    SetEyeAccom, GameScalar, "eye accomodation", "Set HDR eye accomodation.", "setEyeAccom -1", "", "1.18", "", Category) \
  XX(GameBool,              "isAttached",                     IsAttached, GameObject, "obj", "Returns true if object is attached", "_isAttached = isAttached Cone", "", "1.08", "", Category) \
  XX(GameNothing,           "allowCreateShot",                AllowCreateShot, GameBool, "bool", "Enables create shot on click functionality.", "", "", "1.18", "", Category) \
  XX(GameNothing,           "allowMovementControlsInDialog",  AllowMovementControlsInDialog, GameBool, "bool", "Enables player control functions (fwd, left, back etc) to operate even if a dialog is present.", "", "", "1.18", "", Category) \
  XX(GameNothing,           "driveOnRoadSide",                DriveOnRoadSide, GameString, "RoadSide", "Define road side to drive on. Possible values are \"center\", \"left\", \"right\"", "driveOnRoadSide \"left\"", "", "1.18", "", Category) \
  XX(GameArray,             "camGetFov",                      CamGetFov, GameObject, "camera", "Get camera FOV in form [minFov, maxFov]", "", "", "1.18", "", Category) \
  XX(GameNothing,           "setFriendlyFireMessageScope",    SetFriendlyFireMessageScope, GameString, "scope", "Sets the scope of friendly fire messages. If \"FRIENDLY\" then killing any friendly unit (regardless of side) will trigger a friendly fire death message. If \"SIDE\" then the friendly fire death message will only be shown if the killer is of the same side as the killed unit.", "", "", "1.18", "", Category) \
  XX(GameScalar,            "strLen",                         TextLength, GameString, "string", "Returns the length of a text string.", "", "", "1.18", "", Category) \
  XX(GameBool,              "isLightOn",                      VehIsLightOn, GameObject, "vehicle", "Checks whether the light is on.", "on = isLightOn vehicle player", "", "1.07", "", Category) \
  XX(GameArray,             "triggerActivation",              TriggerGetActivation, GameObject, "trigger", "Returns trigger activation in the form [by, type, repeating]", "", "", "1.18", "", Category) \
  XX(GameArray,             "triggerArea",                    TriggerGetArea, GameObject, "trigger", "Returns trigger area in the form [a, b, angle, rectangle]", "", "", "1.18", "", Category) \
  XX(GameArray,             "triggerStatements",              TriggerGetStatements, GameObject, "trigger", "Returns trigger statements in the form [cond, activ, desactiv]", "", "", "1.18", "", Category) \
  XX(GameString,            "triggerText",                    TriggerGetText, GameObject, "trigger", "Returns trigger text.", "", "", "1.18", "", Category) \
  XX(GameArray,             "triggerTimeout",                 TriggerGetTimeout, GameObject, "trigger", "Returns trigger timeout in the form [min, mid, max, interruptable]", "", "", "1.18", "", Category) \
  XX(GameString,            "triggerType",                    TriggerGetType, GameObject, "trigger", "Returns trigger type.", "", "", "1.18", "", Category) \
  XX(GameString,            "treeCurSelData",                 UITreeGetCurSelData, GameControl, "control", "Returns the hidden text within the selected item of the given tree.", "_data = treeCurSelData _control", "", "1.18", "", Category) \
  XX(GameString,            "treeCurSelTexture",              UITreeGetCurSelTexture, GameControl, "control", "Returns the texture within the selected item of the given tree.", "_texture = treeCurSelTexture _control", "", "1.18", "", Category) \
  XX(GameScalar,            "getVehicleAmmo",                 VehGetAmmo, GameObject, "object", "Gets how much ammunition (compared to a full state defined by the vehicle type) the vehicle has. The value ranges from 0 to 1.", "getVehicleAmmo player", "", "1.18", "", Category) \
  XX(GameString,            "addQuotes",                      TextAddQuotes, GameString, "string", "Parses the given string and duplicates quote (\") characters.", "_text = addQuotes \"hint \"some text\";\"", "", "1.18", "", Category) \
  XX(GameNothing,           "setMotionController",            ObjSetMotionController, GameObject, "obj", "Use motioncontroller for that object", "setMotionController ship1", "", "1.10", "", Category) \
  XX(GameNothing,           "SetViewPar",                     SetViewPar, GameArray, "[ViewType,value[,obj]]", "Set viewparameter. Types are: 'trail'", "SetViewPar [\"trail\",true]", "", "", "1.10", Category) \
  XX(GameBool |GameScalar,  "GetViewPar",                     GetViewPar, GameArray, "[ViewType[,obj]]", "Get viewparameter. Types are: 'trail'", "GetViewPar [\"trail\", player]", "", "", "1.10", Category) \
  XX(GameSubgroup,          "subGroup",                       ObjSubGroup, GameObject, "object", "Returns the subgroup to which the object belongs.", "", "", "1.18", "", Category) \
  XX(GameArray,             "units",                          SubGroupUnits, GameSubgroup, "subgrp", "Returns an array with all the units in the subgroup.", "", "", "1.18", "", Category) \
  XX(GameObject,            "leader",                         SubGroupLeader, GameSubgroup, "subgrp", "Returns the subgroup leader for the given subgroup. For a dead unit, grpNull is returned.", "leader subgroup player == leader player", "", "1.18", "", Category) \
  XX(GameSubgroup,          "createSubgroup",                 SubGroupCreate, GameArray, "units", "Creates a subgroup from the given units. For a dead unit, grpNull is returned.", "", "", "1.18", "", Category) \
  XX(GameString,            "formation",                      SubGroupFormation, GameSubgroup, "subgrp", "Returns the formation of the subgroup (\"COLUMN\", \"STAG COLUMN\", \"WEDGE\", \"ECH LEFT\", \"ECH RIGHT\", \"VEE\" or \"LINE\").", "formation subgroup player", "", "", "", Category) \
  XX(GameString,            "previewPicture",                 GetPreviewPicture, GameString, "type", "Returns the preview picture for the specified type of vehicle, returns an empty string if none is found.", "", "", "", "", Category) \
  XX(GameNothing,           "createIndirectDamage",           CreateIndirectDamage, GameArray, "[position, indirectHit, indirectHitRange]", "Initiates an indirect damage at the specified position.", "createIndirectDamage [getpos player, 10, 5]", "", "1.13", "", Category) \
  XX(GameBool,              "isPersonalItemsEnabled",         IsPersonalItemsEnabled, GameObject,"Unit", "Returns true if personalItems are enabled for the unit", "_val = isPersonalItemsEnabled player", "", "1.13", "", Category) \
  XX(GameNothing,           "SetLasershotWeapon",             SetLasershotWeapon, GameArray,"", "Sets the weapon for Lasershot application", "SetLasershotWeapon [0,""VBS2_M2_HMG""]","", "1.14", "", Category) \
  XX(GameBool,              "IsExternalControlled",           IsExternalControlled, GameObject, "obj", "Returns true if the given object is external controlled (HLA or AAR).", "_isExtCtrl = isExternalControlled soldier","", "1.18", "", Category) \
  XX(GameScalar,            "roadSurfaceHeight",              RoadSurfaceHeight, GameArray, "position", "Returns the height above water of the road surface at the given position.", "","", "1.18", "", Category) \
  XX(GameNothing,           "respawn",                        ObjRespawn, GameObject, "obj", "Respawns the given object immediately.", "","", "1.18", "", Category) \
  XX(GameArray,             "allStaticVehicles",              GetAllStaticVehicles, GameString, "evaluation", "Return a list of all static objects in the current mission that match the given evaluation. _x is substituted for the actual object in the evaluation.","_static = allStaticVehicles \"true\"",  "", "1.18", "", Category) \
  XX(GameArray,             "allShots",                       GetAllShots, GameString, "evaluation", "Return a list of all shots in the current mission that match the given evaluation. _x is substituted for the actual object in the evaluation.","_shot = allShots \"true\"",  "", "1.18", "", Category) \
  XX(GameBool,              "canFloat",                       ObjCanFloat, GameObject, "obj", "Returns true if the object can float.","canFloat myVeh",  "", "1.18", "", Category) \
  XX(GameNothing,           "groupSelectUnit",                GroupSelectUnit, GameObject, "obj", "Select a unit in the players group (in the UI command bar). Only valid if the player is commander of a group.","groupSelectUnit (units group player select 1)",  "", "1.18", "", Category) \
  XX(GameNothing,           "groupUnSelectUnit",              GroupUnSelectUnit, GameObject, "obj", "Unselect a unit in the players group (in the UI command bar). Only valid if the player is commander of a group.","groupUnSelectUnit (units group player select 1)",  "", "1.18", "", Category) \
  XX(GameNothing,           "setAdmin",                       SetAdmin, GameBool, "bool", "Sets admin mode on/off.","setAdmin false;",  "", "1.18", "", Category) \
  XX(GameArray,             "ObjToId",                        ObjectToNetworkId,GameObject,"obj","Returns an array of network id","myId = ObjToId myObject", "", "1.18", "", Category) \
  XX(GameObject,            "IdToObj",                        NetworkIdToObject,GameArray,"networkid","Returns the object of the following network id","object = IdToObj [2,1]", "", "1.18", "", Category) \
  XX(GameNothing,           "forceClose",                     ForceCloseFile,GameString,"File Name","Forcefully closes the handels to a specific file","forceClose myFile", "", "1.18", "", Category) \
  XX(GameString,            "positionToGrid",                 PositionToGrid,GameArray,"Position","Converts the position to grid","gridString = positionToGrid [2200,2074,0]", "", "1.18", "", Category) \
  XX(GameArray,             "gridToPosition",                 GridToPosition,GameString|GameArray,"Grid Pos","Converts the grid position to ingame cords, return format [x,z]","pos = gridToPosition ""152182""", "", "1.18", "", Category) \
  XX(GameScalar,            "ctrlFontHeight",                 UICtrlGetFontHeight,GameControl,"control","Returns the main font size of the given control","", "", "1.18", "", Category) \
  XX(GameScalar,            "ctrlRowHeight",                  UICtrlGetRowHeight,GameControl,"control","Returns the row height of the given control","", "", "1.18", "", Category) \
  XX(GameNothing,           "switchAllLights",                switchAllLights,GameString,"bool","Toggles all streetlamps on/off. Receives ""ON"", ""OFF"", or ""AUTO"".","", "", "1.18", "", Category) \
  XX(GameScalar,            "makeAirborne",                   ObjMakeAirborne,GameObject,"obj","Instantly makes an aircraft start flying. Does not actually raise an aircraft into the air. Returns default fly height of the aircraft.","", "", "1.18", "", Category) \
  XX(GameBool,              "isAirborne",                     ObjIsAirborne,GameObject,"obj","Returns whether or not the aircraft is currently airborne.","", "", "1.18", "", Category) \
  XX(GameNothing,           "PauseSimulation",                PauseSimulation,GameBool, "bool","Toggles game simulation on/off. Accepts, true or false","","","1.18","", Category) \
  XX(GameNothing,           "exportEMF",                      ExportEmf, GameArray, "[\"FileName\", GridVisible, ContoursVisible]", "Exports the current map as EMF", "","", "1.18", "", Category) \
  XX(GameNothing,           "setWaterLevel",                  SeaLevelOffset,GameScalar,"offset","Raise/Lower sea level","setWaterLevel 5.0","","1.18","", Category) \
  XX(GameNothing,           "setOrigin",                      SetOrigin, GameArray, "[easting, northing, zone, hemisphere]", "Sets the origin for LVCGame", "SetOrigin [52045, 3479724, 38, ""N""]","", "1.18", "", Category) \
  XX(GameNothing,           "setTerrainHeight",               SetTerrainHeight, GameArray, "[x,z,height]", "Changes a terrain cell height", "","", "1.18", "", Category) \
  XX(GameNothing,           "setTerrainHeightArea",           SetTerrainHeightArea, GameArray, "[[x,z],[x,z],height,changeLower=true,changeHigher=true]", "Changes terrain cells height within an area. It can be controlled by setting the optional parameters if the lower or higher cells can be changed.", "","", "1.18b", "", Category) \
  XX(GameArray,             "terrainHeightArea",              GetTerrainHeightArea, GameArray, "[[x,z],[x,z]]", "Returns array with [min,mid,max] height of the specified area.", "","", "1.19", "", Category) \
  XX(GameScalar,            "terrainHeight",                  GetTerrainHeight, GameArray, "[x,z]", "Gets a terrain cell height", "","", "1.18", "", Category) \
  XX(GameArray,             "ShapeProperty",                  GetShapeProperty , GameArray, "", "Get Shapefile properties" , "","", "1.18", "", Category) \
  XX(GameNothing,           "UnLoadShape",                    UnLoadShape , GameScalar|GameString, "index|name", "Unloads a shape specified by id or name" , "","", "1.18", "", Category) \
  XX(GameScalar,            "loadShape",                      LoadShape , GameArray, "path", "_indx = LoadShape [_posx,_posy,""my.shp""]" , "", "", "1.18", "", Category) \
  XX(GameBool,              "setShapeProperty",               SetShapeProperty , GameArray, "", "Set Shapefile properties" , "", "", "1.18", "", Category) \
  XX(GameArray,             "ListFiles",                      ListFiles, GameArray, "[""DirName"",""mask"",UserDir]", "returns an array for all files found in the Userdir (default) or mission directory depending on the the value of UserDir", "_missionDirTxtFiles = ListFiles [""mine\\"",""*.txt"",false]","", "1.18", "", Category) \
  XX(GameBool,              "triggerActivated",               TriggerActivated, GameObject, "trigger", "Returns true if the trigger has been activated" , "","", "1.18", "", Category) \
  XX(GameNothing,           "fireLaserSimple",                FireLaserPointSimple, GameArray, "fireLaserSimple [entity,nameCode,functionCode,laserPower,laserWaveLength]","provided parameters will be given to DIS gateway. Entity weapon will be used to shot virtual laser", "","", "1.18", "", Category) \
  XX(GameNothing,           "fireLaserAdvanced",              FireLaserPointAdvanced,GameArray,"fireLaserAdvanced [desinator obj,controler obj,target obj, namecode, functioncode, laserpower, laserwavelength, relPos (x,z,y), worldPos (x,z,y), lineraccel (x,z,y)]", "Provides an advanced function, to pass directly to HLA gateway" , "","", "1.18", "", Category) \
  XX(GameNothing,           "CollisionDetection",             CollisionDetection,GameArray,"CollisionDetection [getposasl start,getposasl player, radius, ignoreObject]", "Returns an array of all objects collision from start pos to endpos in the form [[obj, position , normal],[..]]. Position should be in absolute positin not relative to ground, ignore object is optional" , "","", "1.18", "", Category) \
  XX(GameString,            "watchMode",                      ObjWatchMode,GameObject,"object", "Returns the watch mode of the given unit." , "","", "1.18", "", Category) \
  XX(GameBool,              "clipLandKeep",                   ObjClipLandKeep,GameObject,"object", "Returns true if the object is a fence or similar whereby vertical components stay vertical even on a slope." , "","", "1.18", "", Category) \
  XX(GameNothing,           "placeOnSurface",                 ObjPlaceOnSurface,GameObject,"object", "Executes place on surface function for the given object (used to ensure clipLandKeep objects wrap to ground correctly on remote computers)." , "","", "1.18", "", Category) \
  XX(GameNothing,           "mergeConfigFile",                MergeConfigFile, GameArray, "[""FileName"",UserDir]", "merges the given config file into the main config. Location is relative to the Userdir (default) or mission directory depending on the the value of UserDir", "mergeConfigFile [""myconfig"",false]","", "1.18", "", Category) \
  XX(GameDisplay,           "createMessageBox",               CreateMessageBox, GameArray, "[Parent,""Text""]", "Creates a yes/no message box as a child of the given display. Returns a reference to the new message box.", "","", "1.19", "", Category) \
  XX(GameNothing,           "setTIBlurLevel",                 SetTIBlurLevel, GameScalar, "blurLevel", "Sets the level of blur of the TI device", "setTIBlurLevel 1","", "1.18", "", Category) \
  XX(GameNothing,           "setTIBrightness",                SetTIBrightness, GameScalar, "brightness", "Sets the brightness of the TI device, in celsius", "setTIBrightness 20","", "1.18", "", Category) \
  XX(GameNothing,           "setTIContrast",                  SetTIContrast, GameScalar, "contrast", "Sets the contrast of the TI device, in celsius", "setTIContrast 60","", "1.18", "", Category) \
  XX(GameArray,             "FaceTextures",                   GetFaceTextures, GameObject, "obj", "Returns the texture used for face and glasses.", "_textures = faceTextures player", "", "1.19", "", Category) \
  XX(GameNothing,           "setTIOverride",                  SetTIOverride, GameBool, "override", "Forces TI to be shown instead of night vision, when night vision key is pressed", "setTIOverride true","", "1.19", "", Category) \
  XX(GameNothing,           "setTIMode",                      SetTIMode, GameScalar, "mode", "Sets the mode of the TI device, as index to the TIConversion texture", "setTIMode 2","", "1.18", "", Category) \
  XX(GameNothing,           "setTIAutoBC",                    SetTIAutoBC, GameBool, "autoBC", "Informs the engine to use the automatic setting of brightness and contrast in thermal vision", "setTIAutoBC true","", "1.18", "", Category) \
  XX(GameNothing,           "setTIAutoBCContrastCoef",        SetTIAutoBCContrastCoef, GameScalar, "contrastCoef", "Sets a coefficient the variance in the thermal image histogram is multiplied by", "setTIAutoBCContrastCoef 3","", "1.18", "", Category) \
  XX(GameNothing,           "onMarkerCreated",                MapOnMarkerCreated, GameString, "command", "Defines an action performed when a marker is created. Receives _this which is [marker,update?].", "", "", "1.19", "", Category) \
  XX(GameNothing,           "onMarkerDeleted",                MapOnMarkerDeleted, GameString, "command", "Defines an action performed just before a marker is deleted. Receives _this which is [marker].", "", "", "1.19", "", Category) \
  XX(GameNothing,           "onVehicleCreated",               MapOnVehicleCreated, GameString, "command", "Defines an action performed just after a vehicle or object is created. Receives _this which is [vehicle,slow vehicle?].", "", "", "1.19", "", Category) \
  XX(GameArray,             "listAllTurrets",                 ListAllTurrets,        GameObject, "obj", "Returns an array of all avaliable paths for the vehicle turrets", "listAllTurrets car or listallTurrets tank","", "", "", Category) \
  XX(GameArray,             "getWeaponCargo",                 GetWeaponCargo,        GameObject, "obj", "Returns a list of weapons avlaiable in objects cargo area. Return array is formated [[""weaponName"",count],[""WeaponName"",count]....]", "getWeaponCargo tank","", "", "", Category) \
  XX(GameArray,             "getMagazineCargo",               GetMagazinesCargo,     GameObject, "obj", "Returns all the magazines in objects cargo. Returns array formated is:[[""classname"", [bulletCount, bulletCount, bulletCount...]],[""classname"", [bulletCount,...],...]", "getMagazineCargo tank","", "", "", Category) \
  XX(GameArray,             "magazinesEx",                    GetMagazinesEx,        GameObject, "obj", "Returns all the magazines in more detailed information of the person. Returns array formated is:[[""classname"", [bulletCount, bulletCount, bulletCount...]],[""classname"", [bulletCount,...],...]", "MagazinesEx player","", "", "", Category) \
  XX(GameArray,             "getMagazinesTurret",             GetMagazineTurret,     GameArray , "obj", "Returns all magazines at turret path. Returns array formated is:[[""classname"", [bulletCount, bulletCount, bulletCount...]],[""classname"", [bulletCount,...],...]", "getMagazineTurret [<obj>, turret path]","", "", "", Category) \
  XX(GameString,            "PistolWeapon",                   ObjGetPistolWeapon,    GameObject, "obj", "Returns the pistol weapon name, if no pistol is found empty string is returned","_pistol = getPistolWeapon player" ,"", "1.18", "", Category) \
  XX(GameArray,             "pluginFunction",                 PluginFunction,        GameArray, "[""PluginName"", Value]", "Calls plugin PluginName and uses Value as it's parameter", "pluginFunction [""VBSPlugin"", 3.75]","", "1.18", "", Category) \
  XX(GameArray,             "triggerAttachedObj",             TriggerGetAttachedObj, GameObject, "trigger", "Returns all objects attached to the trigger.", "","", "1.19", "", Category) \
  XX(GameAny,               "loadProfileSetting",             ProfileLoadSetting,    GameString, "setting", "Loads a setting from the player profile file.", "","", "1.19", "", Category) \
  XX(GameScalar,            "getTotalWeightKg",               GetTotalWeightKg,      GameObject, "obj","Returns the total weight of the vehicle/person. Including cargo items, equipted weapons and ammo.","","", "1.19", "", Category) \
  XX(GameBool,              "IsTurnedOut",                    IsTurnedOut,           GameObject, "unit", "Returns false if the given unit is turned in. Default is true.", "_isTurnedOut = isTurnedOut soldier", "", "1.19", "", Category) \
  XX(GameNothing,           "setFlashBang",                   SetFlashBang,          GameScalar, "duration", "Invokes flashbang and sets its duration", "SetFlashBang 10","", "1.18", "", Category) \
  XX(GameArray,             "AttachedObjects",                GetAttachedObj,        GameObject, "object", "Returns all objects attached to the object.", "","", "1.19", "", Category) \
  XX(GameObject,            "attachedObject",                 AttachedTo,            GameObject, "obj", "Returns the object it's attached to", "_AttachedTo = attachedObject Cone", "", "1.08", "", Category) \
  XX(GameBool,              "typeExists",                     ObjTypeExists,         GameString, "type", "Returns true if an object of the given type exists in the mission.", "","", "1.19", "", Category) \
  XX(GameScalar,            "getFlyInHeight",                 ObjGetFlyingHeight,    GameObject, "obj", "Returns fly in height of aircraft.", "","", "1.19", "", Category) \
  XX(GameScalar,            "getForceSpeed",                  VehGetForceSpeed,      GameObject, "obj", "Returns forceSpeed of a vehicle.", "","", "1.19", "", Category) \
  XX(GameNothing,           "setUserChart",                   SetUserChart,          GameString, "chartName", "Set a user defined chart", "setUserChart 'ca\\sara\\userMap'", "", "1.19", "", Category) \
  XX(GameNothing,           "setUserChartDrawObjects",        SetUserChartDrawObjects, GameBool, "drawObjects", "Inform the engine objects should be drawn on the user defined map", "setUserChartDrawObjects true", "", "1.19", "", Category) \
  XX(GameNothing,           "forceEnding",                    ForceEnd,              GameScalar, "ending", "Forces the mission to end with the specified ending (values are 0=Continue, 1=Killed, 2=Loser, 3=End1, 4=End2, 5=End3, 6=End4, 7=End5, 8=End6)", "forceEnding 3", "", "1.19", "", Category) \
  XX(GameBool,              "isGeoDisabled",                  ObjIsDisableGeo,       GameObject, "obj", "Returns true if any geometery is disabled for the given object.", "", "", "1.19", "", Category) \
  XX(GameBool,              "isMapPlaced",                    IsMapPlaced,           GameObject, "obj", "Returns true if the given object is part of the map.", "_isMap = isMapPlaced object", "", "1.19", "", Category) \
  XX(GameNothing,           "Screenshot",                     Screenshot,            GameArray,  "[fileName, fileType]", "Function to save specified screenshot into file. If fileName is present and ends with '\\' or '/', then it is used as a folder and automatic name is to be generated, otherwise it is considered as a filename with path. If fileName is not present, then  folder 'Screenshots' is created in Documents and Settings folder of the VBS project and automatic name is to be used for the screenshot. fileType can have one of the following values: BMP JPG TGA PNG DDS PPM DIB HDR PFM. If not present or unknown, then jpg is going to be used.", "", "", "2.0", "", Category) \
  XX(GameBool,              "canSpeak",                       CanSpeak,              GameObject, "obj", "Returns true is unit can speak, false otherwise.","_voiceEnable = CanSpeak player", "", "1.19", "", Category) \
  XX(GameBool,              "isKeyPressed",                   IsKeyPressed,          GameScalar, "DIK_Code","Returns true if the key with the DIK code is pressed.","_keydown = isKeyPressed 0x2e","", "5632", "", Category) \
  XX(GameBool,              "isJoyBtnPressed",                IsJoyKeyPressed,       GameScalar, "Button Id","Returns true if the joystick button with the  code is pressed.","_buttonPressed = isJoyBtnPressed 1","", "5632", "", Category) \
  XX(GameBool,              "isValidVarName",                 IsValidVarName,        GameString, "variable name","Returns true if the given string is a valid variable name.","_isValid = isValidVarName ""myvar""","", "5636", "", Category) \
  XX(GameBool,              "isValidFileName",                IsValidFileName,       GameString, "file name","Returns true if the given string is a valid file name.","_isValid = isValidFileName ""myfile.txt""","", "5636", "", Category) \


/*
  XX(GameNothing, "respawnUnit", ObjRespawn, GameObject, "unit", "Instantly respawns the specified unit.", "respawnUnit unit1", "", "1.18", "", Category) \  
  XX(GameNothing, "slaveMapToObject", LockMapToObject, GameArray, "obj", "Returns a list of all the attribute names for the specified object.", "_attributes = listAttributes player", "", "1.18", "", Category)
*/

#define FUNCTIONS_VBS_CHEAT(XX, Category) \

#define NULARS_VBS(XX, Category) \
  XX(GameBool,    "isMultiplayer",      IsMultiplayer,        "Returns true if running a multiplayer game.",                          "",                         "", "1.18", "", Category) \
  XX(GameBool,    "isLasershot",        IsLasershot,          "Returns true if running a lasershot compatible version.",              "",                         "", "1.18", "", Category) \
  XX(GameArray,   "allUnits",           GetUnits,             "Return a list of all units in the current mission.",                   "_units = units",           "", "1.18", "", Category) \
  XX(GameArray,   "allVehicles",        GetAllVehicles,       "Return a list of all vehicles in the current mission.",                "_vehicles = allVehicles",  "", "1.18", "", Category) \
  XX(GameArray,   "markers",            GetMarkers,           "Return a list of all markers in the current mission.",                 "_markers = markers",       "", "1.18", "", Category) \
  XX(GameArray,   "triggers",           GetTriggers,          "Return a list of all triggers in the current mission.",                "_triggers = triggers",     "", "1.18", "", Category) \
  XX(GameArray,   "agentsEnabled",      IsAgentsEnabled,      "Return true if current build supports agents.",                        "",                         "", "1.18", "", Category) \
  XX(GameArray,   "aspectRatio",        AspectRatio,          "Returns aspect ratio as [leftFOV, topFOV].",                           "",                         "", "1.18", "", Category) \
  XX(GameArray,   "screenResolution",   ScreenResolution,     "Returns screen resolution as [width, height].",                        "",                         "", "1.18", "", Category) \
  XX(GameArray,   "isAdmin",            IsAdmin,              "Returns true if the current player is logged in as an administrator.", "",                         "", "1.18", "", Category) \
  XX(GameArray,   "playableUnitNames",  GetPlayableUnitNames, "Returns a list of playable unit names.",                               "",                         "", "1.18", "", Category) \
  XX(GameBool,    "titleEffectActive",  TitleEffectActive,    "Returns true if a title effect is currently active.",                  "",                         "", "1.18", "", Category) \
  XX(GameArray,   "getWorldCenter",     GetWorldCenter,       "Returns center position of the currently loaded world.",               "",                         "", "1.18", "", Category) \
  XX(GameNothing, "eyeAccomCut",        EyeAccomCut,          "Adjust eye accomodation immediately",                                  "",                         "", "1.18", "", Category) \
  XX(GameArray,   "groupListSelected",  GroupListSelected,    "List the units that are currently selected in the UI command bar.",    "",                         "", "1.18", "", Category) \
  XX(GameNothing, "groupClearSelected", GroupClearSelected,   "Clear currently selected units in the UI command bar.",                "",                         "", "1.18", "", Category) \
  XX(GameBool,    "isPaused",           IsPausedSimulation,   "Checks to see if the gameword simulation is paused by PauseSimulation","",                         "", "1.18", "", Category) \
  XX(GameArray,   "getOrigin",          GetOrigin,            "Get the current UTM origin",                                           "",                         "", "1.18", "", Category) \
  XX(GameArray,   "ListShapeFiles",     ListShapeFiles,       "Return array of loaded shape files",                                   "",                         "", "1.18", "", Category) \
  XX(GameArray,   "getFPS",             GetFPS,               "Returns an array containing FPS values. [Last, Avg, Min]",             "",                         "", "1.19", "", Category) \
  XX(GameArray,   "systemTime",         SystemTime,           "Returns array of [year,month,day,hour,minute,second,millisecond]",     "_sysTime = systemTime",    "", "1.18", "", Category) \
  XX(GameScalar,  "getTIBlurLevel",           GetTIBlurLevel,           "Returns level of blur of the TI device",                               "",                         "", "1.18", "", Category) \
  XX(GameScalar,  "getTIBrightness",    GetTIBrightness,      "Returns brightness of the TI device, in celsius",                      "",                         "", "1.18", "", Category) \
  XX(GameScalar,  "getTIContrast",      GetTIContrast,        "Returns contrast of the TI device, in celsius",                        "",                         "", "1.18", "", Category) \
  XX(GameScalar,  "getTIMode",                GetTIMode,                "Returns mode of the TI device, as index to the TIConversion texture",                            "",                         "", "1.18", "", Category) \
  XX(GameBool,    "getTIAutoBC",              GetTIAutoBC,              "Returns the engine uses automatic setting of brightness and contrast in thermal vision or not",  "",                         "", "1.18", "", Category) \
  XX(GameScalar,  "getTIAutoBCContrastCoef",  GetTIAutoBCContrastCoef,  "Returns the coefficient the variance in the thermal image histogram is multiplied by",           "",                         "", "1.18", "", Category) \
  XX(GameNothing, "resetTerrainHeights",      ResetHeight,              "Resets all height changes in the current map",                       "",                         "", "1.19", "", Category) \
  XX(GameString,  "getUserChart",             GetUserChart,             "Returns name of the current user defined chart",                       "",                         "", "1.19", "", Category) \
  XX(GameBool,    "getUserChartDrawObjects",  GetUserChartDrawObjects,  "Returns whether objects are being drawn on user defined chart or not", "",                         "", "1.19", "", Category) \
  XX(GameBool,    "isAVRS",                   IsAVRS,                   "Returns true if the current build supports AVRS.",                     "",                         "", "1.19", "", Category) \

//////////////////////////////////////////////////////////////////////////
///  After Action Review Commands
//////////////////////////////////////////////////////////////////////////
#define OPERATORS_VBS_AAR(XX, Category) \
  XX(GameScalar, "AARgetStat", function, AARGetStat, GameAny, GameString, "group|object|side","Statistic","Returns the requested statistic","", "", "1.18", "", Category)

#define FUNCTIONS_VBS_AAR(XX, Category) \
  XX(GameBool   ,"AARload",             AARLoad,      GameString, "path", "File to load for replaying.",                         "success = AARLoad ""FileName""","", "",     "", Category) \
  XX(GameNothing,"AARsave",             AARSave,      GameString, "path", "Save the recorded misssion to file.",                 "AARsave ""myPath""",       "", "",     "", Category) \
  XX(GameNothing,"AARsetTime",          AARSetTime,   GameScalar, "time", "Moves to the specified time in recording",            "AARSetTime 2.0",           "", "",     "", Category) \
  XX(GameNothing,"AARsetRepeate",       AARSetRepeat, GameBool  , "repeate?","Returns back to start and continues playing",      "AARSetRepeat false",       "", "",     "", Category) \
  XX(GameScalar, "AARaddBookMark",      AARAddBM,     GameArray , "array","Add bookmark into AAR",                               "index = AddBookMark [time,name,message]","","","",Category) \
  XX(GameNothing,"AARremoveBookMark",   AARRemBM,     GameScalar, "index","Remove selected bookmark from the list",              "AARRemoveBookMark    (message index)","","","",Category) \
  XX(GameString, "AARgetBookMarkName",  AARGetBMN,    GameScalar, "index","Get the bookmarks name",                              "AARGetBookMarkName   (message index)","","","",Category) \
  XX(GameScalar, "AARgetBookMarkJumpTo",AARGetBMJT,   GameScalar, "index","Get the bookmarks time to jump to",                   "AARGetBookMarkJumpTo (message index)","","","",Category) \
  XX(GameString, "AARgetBookMarkMsg"   ,AARGetBMMsg,  GameScalar, "index","Get the bookmarks message",                           "AARGetBookMarkMsg (message index)","","","",Category)

#define NULARS_VBS_AAR(XX, Category) \
  XX(GameScalar,  "AARsaveState",       AARSaveState,         "Returns the state of saving,MP compatable",                            "x = AARSaveState",         "", "",     "", Category) \
  XX(GameNothing, "AARrecord",          AARRecord,            "Starts recording the current machine",                                 "AARrecord",                "", "",     "", Category) \
  XX(GameBool   , "AARisRecording",     AARIsRecording,       "Returns if the recorder is recording",                                 "x = AARisRecording",       "", "",     "", Category) \
  XX(GameNothing, "AARstopRecording",   AARStopRecording,     "Stops the current recording of the mission",                           "AARStopRecording",         "", "",     "", Category) \
  XX(GameNothing, "AARplay",            AARPlay,              "Start playing the loaded recording",                                   "AARPlay",                  "", "",     "", Category) \
  XX(GameNothing, "AARunload",          AARUnload,            "Unloaded the currently loaded file",                                   "AARUnload",                "", "",     "", Category) \
  XX(GameBool   , "AARisLoaded",        AARIsLoaded,          "Returns true if an AAR file is loaded." ,                              "x = AARIsLoaded",          "", "",     "", Category) \
  XX(GameNothing, "AARreplayPause",     AARPause,             "Pauses the current playback. Call again to resume.",                   "AARPause",                 "", "",     "", Category) \
  XX(GameBool   , "AARisPlaying",       AARIsPlaying,         "Returns if AAR is playing",                                            "x = AARIsPlaying",         "", "",     "", Category) \
  XX(GameNothing, "AARstopPlaying",     AARStopPlaying,       "Stop the playing of the current recorded mission",                     "AARStopPlaying",           "", "",     "", Category) \
  XX(GameNothing, "AARnextMessage",     AARNextMessage,       "Go to the next message on the list(debugging)",                        "AARNextMessage",           "", "",     "", Category) \
  XX(GameNothing, "AARpreviousMessage", AARPreviousMessage,   "Go to the previous message on the list(debugging)",                    "AARPreviousMessage",       "", "",     "", Category) \
  XX(GameBool   , "AARisPaused",        AARIsPaused,          "Returns if the recorder is paused",                                    "isPaused = AARIsPaused",   "", "",     "", Category) \
  XX(GameNothing, "AARisRepeat",        AARIsRepeat,          "Returns if repeat mode is on",                                         "isrepeating = AARIsRepeat","", "",     "", Category) \
  XX(GameScalar , "AARreplayLength",    AARLength,            "Returns the length of the recorded mission",                           "length = AARLength",       "", "",     "", Category) \
  XX(GameScalar , "AARcurrentTime",     AARCurrentTime,       "Returns the current time within the AAR mission",                      "ctime = AARCurrentTime",   "", "",     "", Category) \
  XX(GameScalar , "AARgetBookMarkSize", AARGetBMS,            "Returns the total number of book marks available",                     "size = GetBookMarkSize",   "", "",     "", Category)

/*
  XX(GameNothing, "VBS_unSlaveMap", UnLockMap, "Unlock the map if it was slaved to an object.", "", "", "1.18", "", Category) \
  XX(GameScalar, "getMapScale", GetMapScale, "Returns the current scale of the map.", "", "", "1.18", "", Category)
*/

// Unit behaviour related script commands. That is things to do with fatigue, morale, soldier qualities, etc.
#define FUNCTIONS_VBS_BEHAVIOURS(XX, Category) \
  XX(GameScalar,  "getFatigue",					  GetFatigue,				    GameObject, "unit",		"Returns the fatigue level of the soldier",									              "howTired = getFatigue player",						          "", "1.18", "", Category) \
  XX(GameScalar,  "getAerobicFatigue",		GetAerobicFatigue,		GameObject, "unit",		"Returns the aerobic fatigue level of the soldier",							          "howTired = getAerobicFatigue player",				      "", "1.18", "", Category) \
  XX(GameScalar,  "getAnaerobicFatigue",	GetAnaerobicFatigue,	GameObject, "unit",		"Returns the anaerobic fatigue level of the soldier",						          "howTired = getAnaerobicFatigue player",			      "", "1.18", "", Category) \
  XX(GameScalar,  "getMoraleLevel",				GetMoraleLevel,			  GameObject, "unit",		"Returns the morale level of the unit [0-1]",								              "confidence = getMoraleLevel player",				        "", "1.18", "", Category) \
  XX(GameScalar,  "getMoraleLongTerm",		GetMoraleLongTerm,		GameObject, "unit",		"Returns the long-term (1 minute average) morale level of the unit [0-1]",	"_probSurrender = 1-(getMoraleLongTerm _unit)",		"", "1.18", "", Category) \
  XX(GameScalar,  "getLeadership",				GetLeadership,			  GameObject, "unit",		"Returns the soldier's leadership rating",									              "?(getLeadership player)>0.5:...",					        "", "1.18", "", Category) \
  XX(GameScalar,  "getExperience",				GetExperience,			  GameObject, "unit",		"Returns the soldier's experience rating",									              "_timeSpent = getExperience player",				        "", "1.18", "", Category) \
  XX(GameScalar,  "getTraining",				  GetTraining,			    GameObject, "unit",		"Returns the soldier's training rating",									                "_training = getTraining player",					          "", "1.18", "", Category) \
  XX(GameScalar,  "getEndurance",				  GetEndurance,			    GameObject, "unit",		"Returns the soldier's endurance rating",									                "_toughness = getEndurance player",					        "", "1.18", "", Category)


#define OPERATORS_VBS_BEHAVIOURS(XX, Category) \
  XX(GameNothing,  "addToMorale",			      function,	AddToMorale,			      GameObject,	GameScalar,	"unit",	"scalar",	"Adds the passed value to the unit's morale",				      "player addToMorale -0.3",			    "", "1.18", "", Category) \
  XX(GameNothing,  "addToAerobicFatigue",	  function,	AddToAerobicFatigue,	  GameObject,	GameScalar,	"unit",	"scalar",	"Adds the passed value to the unit's aerobic fatigue",		"player addToAerobicFatigue -0.15",	"", "1.18", "", Category) \
  XX(GameNothing,  "addToAnaerobicFatigue",	function,	AddToAnaerobicFatigue,	GameObject,	GameScalar,	"unit",	"scalar",	"Adds the passed value to the unit's anaerobic fatigue",	"player addToAnaerobicFatigue 0.2",	"", "1.18", "", Category) \
  XX(GameNothing,  "addToFatigue",			    function,	AddToFatigue,			      GameObject,	GameScalar,	"unit",	"scalar",	"Adds the passed value to the unit's total fatigue",		  "player addToFatigue _makeTired",	  "", "1.18", "", Category) \
  XX(GameNothing,  "setLeadership",			    function,	SetLeadership,			    GameObject,	GameScalar,	"unit",	"scalar",	"Changes the unit's leadership rating",						        "player setLeadership 0.2",			    "", "1.18", "", Category) \
  XX(GameNothing,  "setExperience",			    function,	SetExperience,			    GameObject,	GameScalar,	"unit",	"scalar",	"Changes the unit's experience rating",						        "player setExperience 0.7",			    "", "1.18", "", Category) \
  XX(GameNothing,  "setTraining",			      function,	SetTraining,			      GameObject,	GameScalar,	"unit",	"scalar",	"Changes the unit's training rating",						          "player setTraining 0.6",			      "", "1.18", "", Category) \
  XX(GameNothing,  "setEndurance",			    function,	SetEndurance,			      GameObject,	GameScalar,	"unit",	"scalar",	"Changes the unit's endurance rating",						        "player setEndurance 0.95",			    "", "1.18", "", Category)

#define NULARS_VBS_CHEAT(XX, Category) \
  XX(GameScalar,    "terrainGrid",      GetTerrainGrid,       "Returns the grid size of the current Terrain",                         "",                         "", "1.18", "", Category) \
  XX(GameNothing,   "saveWorld",        SaveWorld,            "",                                                                     "",                         "", "1.18", "", Category) \

NULARS_VBS(JNI_NULAR, "VBS")
#if _ENABLE_CHEATS
  NULARS_VBS_CHEAT(JNI_NULAR, "VBS")
#endif
#if _AAR
  NULARS_VBS_AAR(JNI_NULAR,"VBS_AAR")
#endif

static const GameNular ExtNular[]=
{
  NULARS_VBS(REGISTER_NULAR, "VBS")
#if _ENABLE_CHEATS
  NULARS_VBS_CHEAT(REGISTER_NULAR, "VBS")
#endif
#if _AAR
  NULARS_VBS_AAR(REGISTER_NULAR,"VBS_AAR")
#endif
  
};

FUNCTIONS_VBS(JNI_FUNCTION, "VBS")
FUNCTIONS_VBS_BEHAVIOURS(JNI_FUNCTION, "VBS")
#if _ENABLE_CHEATS
  FUNCTIONS_VBS_CHEAT(JNI_FUNCTION, "DEVELOPER_ONLY")
#endif
#if _AAR
  FUNCTIONS_VBS_AAR(JNI_FUNCTION, "VBS_AAR")
#endif

static const GameFunction ExtUnary[]=
{
  FUNCTIONS_VBS(REGISTER_FUNCTION, "VBS")
  FUNCTIONS_VBS_BEHAVIOURS(REGISTER_FUNCTION, "VBS")
#if _ENABLE_CHEATS
  FUNCTIONS_VBS_CHEAT(REGISTER_FUNCTION, "DEVELOPER_ONLY")
#endif
#if _AAR
  FUNCTIONS_VBS_AAR(REGISTER_FUNCTION, "VBS_AAR")
#endif
};

OPERATORS_VBS(JNI_OPERATOR, "VBS")
OPERATORS_VBS_BEHAVIOURS(JNI_OPERATOR, "VBS")
#if _ENABLE_CHEATS
  OPERATORS_VBS_CHEAT(JNI_OPERATOR, "DEVELOPER_ONLY")
#endif
#if _AAR
  OPERATORS_VBS_AAR(JNI_OPERATOR, "VBS_AAR")
#endif

static const GameOperator ExtBinary[]=
{
  OPERATORS_VBS(REGISTER_OPERATOR, "VBS")
  OPERATORS_VBS_BEHAVIOURS(REGISTER_OPERATOR, "VBS")
#if _ENABLE_CHEATS
  OPERATORS_VBS_CHEAT(REGISTER_OPERATOR, "DEVELOPER_ONLY")
#endif
#if _AAR
  OPERATORS_VBS_AAR(REGISTER_OPERATOR, "VBS_AAR")
#endif
};

#if DOCUMENT_COMREF
static ComRefFunc ExtComRefFunc0[] =
{
  NULARS_VBS(COMREF_NULAR, "VBS")
#if _ENABLE_CHEATS
  NULARS_VBS_CHEAT(COMREF_NULAR, "VBS")
#endif
#if _AAR
  NULARS_VBS_AAR(COMREF_NULAR,"VBS_AAR")
#endif
};
static ComRefFunc ExtComRefFunc1[] =
{
  FUNCTIONS_VBS(COMREF_FUNCTION, "VBS")
#if _ENABLE_CHEATS
  FUNCTIONS_VBS_CHEAT(COMREF_FUNCTION, "VBS")
#endif
#if _AAR
  FUNCTIONS_VBS_AAR(COMREF_FUNCTION, "VBS_AAR")
#endif
  FUNCTIONS_VBS_BEHAVIOURS(COMREF_FUNCTION, "VBS")
};
static ComRefFunc ExtComRefFunc2[] =
{
  OPERATORS_VBS(COMREF_OPERATOR, "VBS")
  OPERATORS_VBS_BEHAVIOURS(COMREF_OPERATOR, "DEVELOPER_ONLY")
#if _ENABLE_CHEATS
  OPERATORS_VBS_CHEAT(COMREF_OPERATOR, "VBS")
#endif
#if _AAR
  OPERATORS_VBS_AAR(COMREF_OPERATOR, "VBS_AAR")
#endif
};
#endif

#include <El/Modules/modules.hpp>

INIT_MODULE(VBS, 2)
{
  for (int i=0; i<lenof(ExtBinary); i++)
  {
    GGameState.NewOperator(ExtBinary[i]);
  }
  for( int i=0; i<sizeof(ExtUnary)/sizeof(*ExtUnary); i++ )
  {
    GGameState.NewFunction(ExtUnary[i]);
  }
  for( int i=0; i<sizeof(ExtNular)/sizeof(*ExtNular); i++ )
  {
    GGameState.NewNularOp(ExtNular[i]);
  }
#if DOCUMENT_COMREF
  for (int i=0; i<lenof(ExtComRefFunc0); i++)
  {
    GGameState.AddComRefFunction(ExtComRefFunc0[i]);
  }
  for (int i=0; i<lenof(ExtComRefFunc1); i++)
  {
    GGameState.AddComRefFunction(ExtComRefFunc1[i]);
  }
  for (int i=0; i<lenof(ExtComRefFunc2); i++)
  {
    GGameState.AddComRefFunction(ExtComRefFunc2[i]);
  }
#endif
}

#endif // _VBS2
