#include <Es/essencepch.hpp>
#include <Es/platform.hpp>
#include <Es/Framework/debugLog.hpp>
#include "debugAlloc.hpp"

#if defined(_XBOX) && !_SUPER_RELEASE
#include <Es/Common/win.h>
#include <XbDm.h>
#else
#include <Es/Common/win.h>
#endif

#if defined(_XBOX) && !_SUPER_RELEASE

// on X360 we need a fall-back in case DmAllocatePool failed
// not nice - Es module depending on El
// we should probably have some installable handler instead
// TODO: OnDebugAllocFailure(void (*func)(size_t size));

size_t FreeOnDemandSystemMemoryLowLevel(size_t size);

void *DmAllocatePoolHandleError(size_t size)
{
  for(;;)
  {
    void *mem = DmAllocatePool(size);
    if (mem) return mem;
    if (FreeOnDemandSystemMemoryLowLevel(size)==0)
    {
      Fail("DebugAlloc failed");
      return NULL;
    }
  }
}

#if ALLOC_DEBUGGER
void *DebugAlloc::Alloc( size_t &size, const char *file, int line, const char *postfix )
{
  return DmAllocatePoolHandleError(size);
}
#else
void *DebugAlloc::Alloc( size_t &size )
{
  return DmAllocatePoolHandleError(size);
}
#endif
void DebugAlloc::Free( void *mem, size_t size )
{
  DmFreePool(mem);
}
void *DebugAlloc::Realloc(void *mem, size_t oldSize, size_t size)
{
  return NULL;
}
void DebugAlloc::Unlink( void *mem )
{
}
#else
#if ALLOC_DEBUGGER
void *DebugAlloc::Alloc( size_t &size, const char *file, int line, const char *postfix )
{
#ifdef _WIN32
  return GlobalAlloc(GMEM_FIXED, size);
#else
  return malloc(size);
#endif
}
#else
void *DebugAlloc::Alloc( size_t &size )
{
#ifdef _WIN32
  return GlobalAlloc(GMEM_FIXED, size);
#else
  return malloc(size);
#endif
}
#endif
void DebugAlloc::Free( void *mem, size_t size )
{
#ifdef _WIN32
  GlobalFree(mem);
#else
  free(mem);
#endif
}

void *DebugAlloc::Realloc(void *mem, size_t oldSize, size_t size)
{
#ifdef _WIN32
  return GlobalReAlloc(mem, size, 0);
#else
  // there is no portable way to resize the memory block
  // realloc is allowed to move it, which we cannot
  return NULL;
#endif
}

void DebugAlloc::Unlink( void *mem )
{
}
#endif

