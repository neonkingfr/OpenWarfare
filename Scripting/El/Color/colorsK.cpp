#include <El/elementpch.hpp>

#include "colors.hpp"

#ifdef _COLORSKNI_HPP

#pragma optimize ("t",on)

#include "colorsK.hpp"

extern const ColorK HBlackK(0,0,0);
extern const ColorK HWhiteK(1,1,1);

#endif
