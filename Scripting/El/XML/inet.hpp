#ifdef _MSC_VER
#pragma once
#endif

#ifndef _INET_HPP
#define _INET_HPP


//! Download file
/*!
	\param url URL address of source file
	\param filename name of destination file
*/

bool DownloadFile(const char *url, const char *filename, const char *proxyServer);

#if defined(_WIN32) && !defined(_XBOX)
  struct InternetConnectInfo
  {
    char *serverName;
    char *userName;
    char *password;
    int   port;
    bool  secure;
    InternetConnectInfo() {secure = true;}
  };  
  char *DownloadFile(const char *url, size_t &size, const char *proxyServer, InternetConnectInfo *inetInfo=NULL);
#else
  char *DownloadFile(const char *url, size_t &size, const char *proxyServer);
#endif

#endif
