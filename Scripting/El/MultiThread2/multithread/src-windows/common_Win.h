#ifndef _THREAD_COMMON_HEADER_H_
#define _THREAD_COMMON_HEADER_H_



#pragma once

///defines common "internal" functions.
/**
Don't include this header into your project. It is used by Window implementation only
*/

#include <tchar.h>

#if !defined _WINDOWS_ && !defined _XBOX
#define WIN32_LEAN_AND_MEAN
#define _WIN32_WINNT 0x0501
#define WINVER 0x501
#include <windows.h>
#elif defined _XBOX && !defined _EL_WIN_H
  //#include <Es/Common/win.h>
  // use SSE optimized matrix math
  #define _USE_XGMATH 1
  // note: Verify is function name in XBox Direct3D, but we use it as macro
  #include <Es/Memory/normalNew.hpp> // some X360 libraries override new/delete
  #include <xtl.h>
  #include <xonline.h>
  #include <Es/Memory/debugNew.hpp>
  static DWORD WaitForInputIdle(HANDLE hProcess, DWORD dwMilliseconds) {return WAIT_FAILED;}
  static BOOL GetExitCodeProcess(HANDLE hProcess, LPDWORD lpExitCode) {*lpExitCode=1; return FALSE;}
  static HANDLE OpenProcess(DWORD dwDesiredAccess, BOOL bInheritHandle, DWORD dwProcessId) {return INVALID_HANDLE_VALUE;}
#endif //_WINDOWS_

#include <stdlib.h>
#include <process.h>

#ifdef _XBOX
#ifdef GetCurrentThread
#undef GetCurrentThread
// GetCurrentThread under XBox360 is defined to be NtCurrentThread 
// which is defined as: #define NtCurrentThread() ( (HANDLE) -2 )
// so redirect global GetCurrentThread to former implementation
static inline HANDLE GetCurrentThread() {return NtCurrentThread();}
#endif
#endif //_XBOX

#include "../MTCommon.h"
#include "../Interlocked.h"
#include "../Synchronized.h"
#include "../CriticalSection.h"

#include "../IBlocker.h"

namespace MultiThread
{


  class ThreadInternal
  {
  public:
    ///Provides waiting for single object.
    /**
    Implementation takes current thread instance and call one of overrided wait functions
    */

    static bool WaitForSingleObject(HANDLE object, DWORD timeout);

    ///Provides waiting for multiple objects
    /**
    Implementation takes current thread instance and call one of overrided wait functions
    */

    static int WaitForMultipleObjects(HANDLE *objects, int count, bool waitAll, DWORD timeout);
  };

  class BlockerInternal_InfoStruct
  {
  public:
    HANDLE hBlock;
  };


  class ThreadBase_InfoStruct: public IBlocker
  {
  public:
    unsigned long threadId;	///<thread ID
    HANDLE hThread;			///<thread handle
    MiniCriticalSection start; ///<mini critical section solves some MT collisions

    bool Acquire(unsigned long timeout=-1)
    {
      if (hThread==0) return true;
      return ThreadInternal::WaitForSingleObject(hThread,timeout);
    }

    virtual void Release() {}
  };

};
#endif