// DirectX sound system
// (C) 1996, SUMA
#ifndef _SAMPLES_HPP
#define _SAMPLES_HPP

// Function declarations
#if defined _WIN32
# if !defined _XBOX
#   include <mmsystem.h>
# elif _XBOX_VER>=2
#   include <Xauddefs.h>
# endif
#else
  //#define DUMMY_SOUND_FILE
  typedef struct
  {
      WORD        wFormatTag;         /* format type */
      WORD        nChannels;          /* number of channels (i.e. mono, stereo...) */
      DWORD       nSamplesPerSec;     /* sample rate */
      DWORD       nAvgBytesPerSec;    /* for buffer estimation */
      WORD        nBlockAlign;        /* block size of data */
      WORD        wBitsPerSample;     /* number of bits per sample of mono data */
      WORD        cbSize;             /* the count in bytes of the size of */
              /* extra information (after cbSize) */
  } WAVEFORMATEX;
#endif

#include <El/QStream/fileCompress.hpp>

#include <Es/Memory/normalNew.hpp>

class QFBank;

/// buffer providing access to wave data stored in a file
class WaveBuffer
{
  Ref<IFileBuffer> _buffer;
  /// context for repeated pre-loading
  mutable FileRequestCompletionHandle _request;
  int _skip;
  int _size;
  int _deltaPack;

  // enable streaming delta-pack
  mutable int _lastWriteOffset;
  mutable int _lastValue;

  public:
  WAVEFORMATEX _format; // carry wave format with it

  public:
  WaveBuffer(IFileBuffer *buf, const WAVEFORMATEX &format, int start, int size, int deltaPack);

  IFileBuffer *GetSource() const {return _buffer;}
  int GetSourceHeaderSize() const {return _skip;}

  //char *Data() {return (char *)_buffer->GetData()+_skip;}
  //const char *Data() const {return (char *)_buffer->GetData()+_skip;}
  int PackedSize() const {return _size;}

  // how many data bytes will be made from one source byte
  int GetCompressionRatio() const {return _deltaPack==0? 1 : 16/_deltaPack;}
  int GetFormat() const {return _deltaPack;}
  void GetWaveFormat(WAVEFORMATEX &format) const {format=_format;}

  bool IsFromBank(QFBank *bank) const;

  int UnpackedSize() const
  {
    int size=_size;
    if( _deltaPack==8 ) return size*2;
    if( _deltaPack==4 ) return size*4;
    return size;
  }
  // unpack whole buffer
  void Unpack( void *dest, int destSize ) const;
  /// unpack part of the buffer
  int UnpackPart(void *dest, int offset, int size) const;

  /// request data for UnpackPart to be loaded
  bool RequestPart(int offset, int size) const;
  bool RequestRaw(FileRequestPriority prio, QFileSize offset, QFileSize size) const;
  
  USE_FAST_ALLOCATOR
};

#include <Es/Memory/debugNew.hpp>

/// interface to stream data from any source
/**
implementation of some basic functions with sound file
*/

class WaveStream: public RefCount
{
  public:
  // get some specific part of data
  // note: function should return 0 when reading after end of the data
  // it should anyway fill data with silence

  virtual int GetUncompressedSize() const = 0;
  virtual void GetFormat(WAVEFORMATEX &format) const = 0;
  /// get data from file
  virtual int GetData(void *data, int offset, int size) const = 0;
  /// request data for GetData to be loaded
  virtual bool RequestData(FileRequestPriority prior, int offset, int size) const
  {
    return true;
  }
  virtual bool IsFromBank(QFBank *bank) const = 0;

  /// check if the sound is long enough to be streamed
  virtual bool StreamingWanted(bool looped) const = 0;
  
  /// clipped data retrieval - zeroes outside of valid range
  virtual int GetDataClipped(void *data, int offset, int size) const;
  /// when end is reached, loop
  virtual int GetDataLooped(void *data, int offset, int size) const;

  /// preload data for GetDataClipped
  virtual bool RequestDataClipped(FileRequestPriority prior, int offset, int size) const;
  /// preload data for GetDataLooped
  virtual bool RequestDataLooped(FileRequestPriority prior, int offset, int size) const;
  
};



// file sounds.cpp

//WaveBuffer *SSSLoadFile( const char *name );

// file wave.cpp

WaveStream *WaveLoadFile( const char *name );
bool WSSRequestFile(const char *name, Ref<WaveStream> &stream, bool sync=false);
bool WaveRequestFile(const char *name, Ref<WaveStream> &stream, bool sync=false);

bool SoundRequestFile(const char *name, Ref<WaveStream> &stream, bool sync=false);

#endif
