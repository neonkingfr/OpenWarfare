//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by UnInstall.rc
//
#define IDD_UNINSTALL_USER_FILES        9
#define IDS_APP_NAME                    1000
#define IDS_UNINSTALL_QUSTION           1001
#define IDS_FAIL_NOTFOUND               1002
#define IDS_CANNOT_COPYFILE             1003
#define IDS_UNINSTALL_END               1004

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        102
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1000
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
