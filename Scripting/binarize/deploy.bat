@echo off
call dbg.bat %0 Starting copy sequence  2>nul
call dbg.bat %0 (copy /y "W:\C_Branch\Poseidon\Arrowhead\binarize\Release\binarize.???" "b:\Binarize_EXP\")  2>nul

md b:\binarize_EXP 2>nul
md b:\binarize_EXP\bin 2>nul

set outputDir=B:\binarize_EXP

call dbg.bat %0 (copy /y "W:\C_Branch\Poseidon\Arrowhead\binarize\binarize.???" "%outputDir%\Bin")  2>nul

copy /y "W:\C_Branch\Poseidon\Arrowhead\binarize\Release\binarize.exe" %outputDir%
if errorlevel 1 exit /b %errorlevel%
call dbg.bat %0 1 2>nul

copy /y "W:\C_Branch\Poseidon\Arrowhead\binarize\Release\binarize.map" %outputDir%
if errorlevel 1 exit /b %errorlevel%
call dbg.bat %0 2 2>nul

copy /y "W:\C_Branch\Poseidon\Arrowhead\binarize\Release\binarize.pdb" %outputDir%
if errorlevel 1 exit /b %errorlevel%
call dbg.bat %0 3 2>nul

echo Deployed from %COMPUTERNAME% > %outputDir%\_binarize_deploy.txt
echo Deployed by %USERNAME% >> %outputDir%\_binarize_deploy.txt
echo Deployed at %DATE% %TIME%>> %outputDir%\_binarize_deploy.txt


call dbg.bat %0 (copy /y "W:\C_Branch\Poseidon\Arrowhead\cfg\Bin\*.???" "%outputDir%\Bin")  2>nul

copy /y "W:\C_Branch\Poseidon\Arrowhead\cfg\Bin\*.*" "%outputDir%\Bin"
if errorlevel 1 exit /b %errorlevel%
call dbg.bat %0 4 2>nul

copy /y "W:\C_Branch\Poseidon\Arrowhead\cfg\resincl.hpp" "%outputDir%\Bin"
if errorlevel 1 exit /b %errorlevel%
call dbg.bat %0 5 2>nul

copy /y "W:\C_Branch\Poseidon\Arrowhead\lib\dikCodes.h" "%outputDir%\Bin"
if errorlevel 1 exit /b %errorlevel%
call dbg.bat %0 6 2>nul

copy /y "W:\C_Branch\Poseidon\Arrowhead\lib\languages.hpp" "%outputDir%\Bin"
if errorlevel 1 exit /b %errorlevel%
call dbg.bat %0 Deploy end ...all ok 2>nul

echo Deployed from %COMPUTERNAME% > %outputDir%\bin\_binarizeBin_deploy.txt
echo Deployed by %USERNAME% >> %outputDir%\bin\_binarizeBin_deploy.txt
echo Deployed at %DATE% %TIME%>> %outputDir%\bin\_binarizeBin_deploy.txt





REM copy /y "W:\C_Branch\Poseidon\Arrowhead\binarize\Release\binarize.exe" "U:\Tools\Binarize_EXP\dist\"
REM if errorlevel 1 exit /b %errorlevel%
REM call dbg.bat %0 4 2>nul
REM copy /y "W:\C_Branch\Poseidon\Arrowhead\binarize\Release\binarize.map" "U:\Tools\Binarize_EXP\dist\"
REM if errorlevel 1 exit /b %errorlevel%
REM call dbg.bat %0 5 2>nul

REM call dbg.bat %0 (copy /y "O:\fp\bin\*.???" "U:\Tools\Binarize_EXP\dist\Bin")  2>nul
REM copy /y "O:\fp\bin\*.hpp" "U:\Tools\Binarize_EXP\dist\Bin"
REM if errorlevel 1 exit /b %errorlevel%
REM call dbg.bat %0 6 2>nul
REM
REM copy /y "O:\fp\bin\*.h" "U:\Tools\Binarize_EXP\dist\Bin"
REM if errorlevel 1 exit /b %errorlevel%
REM call dbg.bat %0 7 2>nul
REM
REM copy /y "O:\fp\bin\*.cpp" "U:\Tools\Binarize_EXP\dist\Bin"
REM if errorlevel 1 exit /b %errorlevel%
REM call dbg.bat %0 8 2>nul
REM
REM copy /y "O:\fp\bin\*.csv" "U:\Tools\Binarize_EXP\dist\Bin"
REM if errorlevel 1 exit /b %errorlevel%





