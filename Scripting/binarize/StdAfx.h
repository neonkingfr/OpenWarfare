// stdafx.h : include file for standard system include files,
//  or project specific include files that are used frequently, but
//      are changed infrequently
//

#if !defined(AFX_STDAFX_H__A4D6D00F_0CF0_4E13_8E1B_212549A96D46__INCLUDED_)
#define AFX_STDAFX_H__A4D6D00F_0CF0_4E13_8E1B_212549A96D46__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


// TODO: reference additional headers your program requires here
#include "../lib/wpch.hpp"
#include <io.h>

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_STDAFX_H__A4D6D00F_0CF0_4E13_8E1B_212549A96D46__INCLUDED_)
