#include <windows.h>
#include <winuser.h>
#include <Winable.h>
#include <stdio.h>
#include <stdarg.h>
#include <string>
#include <assert.h>
#include <vector>

#include "CubicPlugin.h"
#include "CubicVBS2Interface.h"
#include "dikCodes.h"


# if defined(_MSC_VER)
# ifndef _CRT_SECURE_NO_DEPRECATE
# define _CRT_SECURE_NO_DEPRECATE (1)
# endif
# pragma warning(disable : 4996)
# endif
#define _USE_MATH_DEFINES

#include <math.h>

#define ASSERT assert

// Command function declaration
typedef int (WINAPI * ExecuteCommandType)(const char *command, char *result, int resultLength);

// If the engine has the special unloadDLL available
// we can reload this on the fly.
bool unloadDllAvaliable = false;

// Command function definition
ExecuteCommandType ExecuteCommand = NULL;
CubicVBS2Interface *cInterface = NULL;

// Format the arguments into a std::string
// Pre-condition: fmt cannot be null.
std::string Format(const char *fmt, va_list args)
{
  ASSERT(fmt);
  int result = -1;
  int length = 256;
  char *buffer = NULL;
  
  while (result == -1)
  {
    if (buffer) delete [] buffer;
    buffer = new char [length];
    ASSERT(buffer);

    // Failed to succeed memory allocation request.
    if (!buffer) return std::string();
    ZeroMemory(buffer,length);
    result = _vsnprintf_s(buffer, length-1, _TRUNCATE, fmt, args);
    length *= 2;
  }

  std::string s(buffer);
  delete [] buffer;
  return s;
}

void LogF(const char* format, ...)
{
  va_list args;
  va_start(args, format);
  std::string outString = Format(format,args); 
  va_end(args);

  OutputDebugString(outString.data());
}

int ExecuteCommandFormat(char *result, int resultLength, const char* format, ...)
{
  if (!ExecuteCommand) return 0;

  va_list args;
  va_start(args, format);
  std::string command = Format(format,args);
  va_end(args);

  return ExecuteCommand(command.data(), result, resultLength);
}

// Function that will register the ExecuteCommand function of the engine
VBSPLUGIN_EXPORT void WINAPI RegisterCommandFnc(void *executeCommandFnc)
{
  LogF("Cubic RegisterCommandFnc called");
  ExecuteCommand = (ExecuteCommandType)executeCommandFnc;
  ASSERT(ExecuteCommand);
}

// Return the current mission time.
float GetMissionTime()
{
  static float lastMissionTime = 0.0f;
  float retVal = 0.0f;
  char timeStr[128] = {0};

  ExecuteCommand("time", timeStr, sizeof(timeStr));
  retVal = (float)atof(timeStr);

  if (retVal < lastMissionTime)
  {
    retVal = 0.0f;
  }
  lastMissionTime = retVal;
  return retVal;
}

const char deleminator[] = "[,] ";
void TokenizeString(char *string,std::vector<std::string> &tokensStrings)
{
  char *token = strtok(string,deleminator);
  while( token != NULL )
  {
    if (!isspace(*token)) tokensStrings.push_back(std::string(token));
    token = strtok( NULL, deleminator );
  }
}

//[active, zoom, visionmode, tiMode, selectedWeapon]
struct OpticsState
{
  bool  active;
  float zoom;
  float visionMode;
  float tiMode;
  std::string selectedWeapon;
};

bool GetOpticsState(OpticsState &optics)
{
  char buffer[256] = {0};
  std::vector<std::string> tokens;
  ExecuteCommand("opticsState",buffer, sizeof(buffer));
  TokenizeString(buffer,tokens);

  // Format of OpticsState, states that we should expect 5 return values.
  if (tokens.size()<5) return false;

  // Is optics state active?
  optics.active = tokens[0][0]=='t' || tokens[0][0]=='T';

  // Optics remaining optics state.
  optics.zoom           = (float)atof(tokens[1].data());
  optics.visionMode     = (float)atof(tokens[2].data());
  optics.tiMode         = (float)atof(tokens[3].data());
  optics.selectedWeapon = tokens[4];

  return true;
}


// The head direction is relative to the players
// body, because of this, when setting the head dir, we need 
// to add the players body direction
float GetPlayerDir()
{
  char direction[256] = {0};
  ExecuteCommand("getdir player",direction,sizeof(direction));
  return (float) atof(direction);
};

// Sends the DIKCODE into the windows raw input que. This is
// then read by directX and then received inside VBS2. Though this
// is only for the current active window.
void SendKeyboardCommandToVBS(unsigned int DIKCODE)
{
  INPUT input[2];
  ZeroMemory(&input,sizeof(input));

  // Default is key pressed down.
  input[0].type = INPUT_KEYBOARD;
  input[0].ki.wScan = DIKCODE;

  input[1].type = INPUT_KEYBOARD;
  input[1].ki.dwFlags |=  KEYEVENTF_KEYUP;
  input[1].ki.wScan = DIKCODE;

  // Send the input off to windows Global input que
  SendInput(2,&input[0],sizeof(INPUT));
}

bool GMissionRunning   = false;
bool InitSuccessfull   = false;
float previousMissionTime = 0;

// Function called when mission is starting.
void OnMissionStart(double missionStart, double deltaT)
{
  GMissionRunning = true;
  InitSuccessfull = true;

  LogF("Mission Start\r\n");
  
  // On mission start, we try to init the interface. We only do this once.
  // We cannot re-allocate the instance because the system will cause memory allocation problems.
  if (!cInterface)
  { 
    // Should create the instance.
    cInterface = CubicVBS2Interface::GetInstance();
    if (cInterface)
    {   
      // Set the entity start location as zero.
      cInterface->SetEntityStartLocation(0.0,0.0,0.0); 
      CubicVBS2Interface::EInitializeErrorReturn eReturn = cInterface->InitializeInstance(CubicVBS2Interface::READ_LOG_DL);
      if ( eReturn != CubicVBS2Interface::NO_ERROR_IER )
      {
        LogF("Error occurred well trying to init the Cubic interface:%d\r\n",(int)eReturn);
        InitSuccessfull = false;
        return;
      }
    }
  }

  // Start in free look mode.
  ExecuteCommandFormat(NULL,0,"\"LookAround\" setAction 1"); 
  ExecuteCommandFormat(NULL,0,"\"ToggleRaiseWeapon\" setaction 1");
  ExecuteCommandFormat(NULL,0,"setweaponswayfactor 0.0"); 

  // When mission start rewind the data.
  if (cInterface) cInterface->RewindDebugCommLog();
}

// Function called when mission is ending.
void OnMissionEnd(double missionStart, double deltaT)
{
  LogF("Mission End\r\n");
  GMissionRunning = false;
  
  // Only execute this when unloadDLL command is available.
  if (unloadDllAvaliable) ExecuteCommandFormat(NULL,0,"unloadplugins");
}

float Difference(float minuend,float subtrahend)
{
  double diff = minuend - subtrahend;
  while (diff < 0) diff += (2*M_PI);
  while (diff >= (2*M_PI)) diff -= (2*M_PI);
  diff = abs(diff);
  if (diff > M_PI) { diff -= (2*M_PI); }
  return (float)diff; 
}

float ConvertToNegPIToPI(float radangle)
{
  const float PI = (float)M_PI;
  while (radangle > PI) radangle = -(2*PI) + (radangle - (2*PI));
  while (radangle <-PI) radangle =  (2*PI) + (radangle + (2*PI));
  return radangle;
}

enum Sensor
{
  HeadSensor,
  BodySensor,
  WeaponSensor,
  LeftLegSensor,
  RightLegSensor,
  Limit
};

struct SensorSample
{
  // sin,cos of the euler angles
  float _x0;
  float _y0;
  float _x1;
  float _y1;
  float _x2; 
  float _y2;

  // position
  float _posX;
  float _posY;
  float _posZ;

  // Zero everything here, its just a data collector
  SensorSample()
  {
    ZeroMemory(this,sizeof(*this));
  }
};

// Smoothing is off by default.
size_t AveragingMaxSize = 0;

// Please note, just add extra enums this should automatically adjust itself.
std::vector<SensorSample> GSensors[Limit];

void CollectSenosrData(Sensor sensor,const CubicVBS2Interface::SMotionNode& data)
{
  ASSERT(sensor<Limit);
  
  // first do a check to make sure we're not going above max.
  while (GSensors[sensor].size()>0 && GSensors[sensor].size()>=AveragingMaxSize) GSensors[sensor].erase(GSensors[sensor].begin());

  SensorSample sample;
  sample._y0 = (float)sin(data.Yaw_angle);
  sample._x0 = (float)cos(data.Yaw_angle);
  sample._y1 = (float)sin(data.Pitch_angle);
  sample._x1 = (float)cos(data.Pitch_angle);
  sample._y2 = (float)sin(data.Roll_angle);
  sample._x2 = (float)cos(data.Roll_angle);
  sample._posX = (float)data.X;
  sample._posY = (float)data.Y;
  sample._posZ = (float)data.Z;

  // Push it back at the end of the array.
  GSensors[sensor].push_back(sample);
}

void GetSensorAveragedYawPitchRoll(Sensor sensor,CubicVBS2Interface::SMotionNode &result)
{
  // We rely on the CollectData to remove any un-needed frames.
  size_t maxCount = GSensors[sensor].size();
  if (!maxCount) return;

  SensorSample average;
  for (size_t x = 0; x < maxCount; ++x)
  {
      // Average the position
      average._posX += GSensors[sensor][x]._posX;
      average._posY += GSensors[sensor][x]._posY;
      average._posZ += GSensors[sensor][x]._posZ;

      // Average the sin,cos angles
      average._y0 += GSensors[sensor][x]._y0;
      average._y1 += GSensors[sensor][x]._y1;
      average._y2 += GSensors[sensor][x]._y2;
      average._x0 += GSensors[sensor][x]._x0;
      average._x1 += GSensors[sensor][x]._x1;
      average._x2 += GSensors[sensor][x]._x2;
  }

  // Average the position
  average._posX /= maxCount;
  average._posY /= maxCount;
  average._posZ /= maxCount;
  average._y0   /= maxCount;
  average._y1   /= maxCount;
  average._y2   /= maxCount;
  average._x0   /= maxCount;
  average._x1   /= maxCount;
  average._x2   /= maxCount;

  // Average into the result the angles
  result.Yaw_angle   = atan2(average._y0,average._x0);
  result.Pitch_angle = atan2(average._y1,average._x1);
  result.Roll_angle  = atan2(average._y2,average._x2);

  // Average into the result the position.
  result.X = average._posX;
  result.Y = average._posY;
  result.Z = average._posZ;
}

/**
 Newly Added simulation function added to the engine. Please note that spawn for
 the head,weapon,and body is no longer needed once an upgrade is done to the latest version.
*/
VBSPLUGIN_EXPORT void WINAPI OnAfterSimulation(float deltaT)
{

}

VBSPLUGIN_EXPORT void WINAPI OnSimulationStep(float deltaT)
{
  float missionTime = GetMissionTime();

  // Detect when he mission has started
  if ((missionTime > 0.0f) && !GMissionRunning) OnMissionStart(missionTime, deltaT);
  
  // Detect when the mission has finished
  if (GMissionRunning && (missionTime == 0.0f || missionTime < previousMissionTime)) OnMissionEnd(missionTime, deltaT);
  previousMissionTime = missionTime;
  
  if (!GMissionRunning) return;
  if (!cInterface) return;
  if (!cInterface->IsCalibrated()) return;

  CubicVBS2Interface::SMotionNode sHeadSensor = cInterface->GetEntityHeadSensorData();
  CubicVBS2Interface::SMotionNode sBodySensor = cInterface->GetEntityBodySensorData();
  CubicVBS2Interface::SMotionNode sWeaponSensor = cInterface->GetWeaponSensorData();
  CubicVBS2Interface::SMotionNode sLeftLegSensor = cInterface->GetEntityLeftLegSensorData();
  CubicVBS2Interface::SMotionNode sRightLegSensor = cInterface->GetEntityRightLegSensorData();

  // Please any other ones that you see fit.
  CollectSenosrData(HeadSensor,sHeadSensor);
  CollectSenosrData(BodySensor,sBodySensor);
  CollectSenosrData(WeaponSensor,sWeaponSensor);
  CollectSenosrData(LeftLegSensor,sLeftLegSensor);
  CollectSenosrData(RightLegSensor,sRightLegSensor);

  // Use the same data structures so we dont have to change the code.
  GetSensorAveragedYawPitchRoll(HeadSensor,sHeadSensor);
  GetSensorAveragedYawPitchRoll(BodySensor,sBodySensor);
  GetSensorAveragedYawPitchRoll(WeaponSensor,sWeaponSensor);
  GetSensorAveragedYawPitchRoll(LeftLegSensor,sLeftLegSensor);
  GetSensorAveragedYawPitchRoll(RightLegSensor,sRightLegSensor);

  // Because the weapon direction and head direction is relative to the body direction, we need to compute
  // the difference with the yaw angles.
  float relativeWeaponDir = (float)Difference((float)sWeaponSensor.Yaw_angle,(float)sBodySensor.Yaw_angle);
  float relativeHeadDir   = (float)Difference((float)sHeadSensor.Yaw_angle,(float)sBodySensor.Yaw_angle);
  
  // Now need to convert the relative if they're extreme, from -PI to PI range.
  relativeWeaponDir = ConvertToNegPIToPI(relativeWeaponDir);
  relativeHeadDir   = ConvertToNegPIToPI(relativeHeadDir);
 
  /*
  // we need to rotate the weapon at -90 to 90 degrees
  // out turn rate is 5* degrees per second.
  static float YRotAngle = 0.0f;
  static float XRotAngle = 0.0f;
  
  float YRotInc = 5.0f*deltaT;
  float XRotInc = 2.0f*deltaT;

  // work in degrees
  static bool reverseY = false;
  static bool reverseX = false;

  if (YRotAngle >= 90.0f) reverseY = true;
  if (YRotAngle <= -90.0f) reverseY = false;

  if (XRotAngle >= 30.0f) reverseX = true;
  if (XRotAngle <= -30.0f) reverseX = false;

  // Okay start reversing
  YRotAngle += reverseY ? -YRotInc : YRotInc;
  XRotAngle += reverseX ? -XRotInc : XRotInc;
  
  ExecuteCommandFormat
  (
    NULL,0,"[] spawn {player SetWeaponDirection [[%f,%f,%f],false]}",
     YRotAngle,
     0.0f,
     0.0f
  );
  return;
  */

  /*
    Some documentation how Eula angles work within the engine. The engine in question
    for matrix's uses the right hand coordinate System. Though Z+ Is into the screen.

    Visual Indicator of the matrix inside the engine is the following
    Y+
    |   Z+
    |  /
    | /
    |/______X+
    Rotation on the X Axis - Pitch
    Rotation on the Y Axis - Heading
    Rotation on the Z Axis - Roll

    Internaly in the engine it does holds the eular angles as the following. It does not
    convert these angles to matrix's until it passes this data to the Direct3D rendering engine. The
    order of the composition is given later.

    float _LookXRot;
    float _LookYRot;
    float _LookZRot;

    float _HeadXRot;
    float _HeadYRot;
    float _HeadZRot;

    float _WeaponXRot;
    float _WeaponYRot;
    float _WeaponZRot;

    The data type is float, and the convention is radian angles. The engine composes
    these angles into a matrix as the following.
    _orientation = ((Direction * Pitch) * Roll)

    // Example taken from the engine.
    _headTrans=
    (
      Matrix4(MRotationY,headYRot)*
      Matrix4(MRotationX,-headXRot)*
      Matrix4(MRotationZ,-headZRot)*
    );
  */

  ExecuteCommandFormat
  (
     NULL,0,"[] spawn {player setHeadDir [[%f,%f,%f],true]}",
     relativeHeadDir * (180.0f/M_PI),
    -sHeadSensor.Pitch_angle * (180.0f/M_PI),
    -sHeadSensor.Roll_angle  * (180.0f/M_PI)
  );
  // Players weapon direction, relative to the body.
  ExecuteCommandFormat
  (
     NULL,0,"[] spawn {player SetWeaponDirection [[%f,%f,%f],false]}",
     relativeWeaponDir * (180.0f/M_PI),
    -sWeaponSensor.Pitch_angle * (180.0f/M_PI),
    -sWeaponSensor.Roll_angle  * (180.0f/M_PI)
  );
  // Set the body direction.
  ExecuteCommandFormat
  (
     NULL,0,"[] spawn {player setdir %f}",
    -sBodySensor.Yaw_angle * (180.0f/M_PI)
  );

  bool aimed = cInterface->IsWeaponAimed();
  static bool prevIsAimedState = 0;
  if (aimed != prevIsAimedState)
  {
    prevIsAimedState = aimed;
    OpticsState opticsState;
    if (GetOpticsState(opticsState))
    { 
      // [active, zoom, visionMode, tiMode, selectedWeapon]*
      ExecuteCommandFormat
      (
        NULL,0,"setOpticsState [%s,%f,%f,%f]",
        aimed ? "true" : "false",
        opticsState.zoom,
        opticsState.visionMode,
        opticsState.tiMode
      );    
    }
  }

  
  std::string stance;
  CubicVBS2Interface::EEntityStance newStance = cInterface->GetEntityStance();
  static CubicVBS2Interface::EEntityStance previousStance = (CubicVBS2Interface::EEntityStance)0;
  if (newStance != previousStance)
  {
    previousStance = newStance;
    switch(newStance)
    {
      case CubicVBS2Interface::STAND_ES:
        stance = "Stand";
        break;
      case CubicVBS2Interface::CROUCH_ES:
      case CubicVBS2Interface::KNEEL_ES:
        stance = "Crouch";
        break;
      case CubicVBS2Interface::PRONE_ES:
      case CubicVBS2Interface::CRAWL_ES:
        stance = "Prone";
        break;
      default:
        ASSERT(false);
    }
    ExecuteCommandFormat(NULL,0,"\"%s\" setAction 1",stance.data());
  }


  // Fire weapon.
  if (cInterface->IsWeaponFireEvent()) ExecuteCommandFormat(NULL,0,"playerForceFire");
  
  // Reload
  if (cInterface->GetWeaponReloadState()) ExecuteCommandFormat(NULL,0,"\"ReloadMagazine\" setAction 1.0");

  // Movement via the joystick.
  ExecuteCommandFormat(NULL,0,"\"MoveForward\" setAction %f",cInterface->GetJoystickMoveForwardBack());
  ExecuteCommandFormat(NULL,0,"\"MoveRight\" setAction %f",cInterface->GetJoystickMoveRightLeft());
}

// Plugin data return
static char resultStr[256] = {0};

// This function will be executed every time the script in the engine calls the script function "pluginFunction"
// We can be sure in this function the ExecuteCommand registering was already done.
// Note that the Plugin takes responsibility for allocating and deleting the returned string
VBSPLUGIN_EXPORT const char* WINAPI PluginFunction(const char *input)
{
  // Data will be writen too.
  std::string temp(input);
  std::vector<std::string> tokens;
  TokenizeString(const_cast<char *>(temp.data()),tokens);

  if (tokens.size()>=1)
  {
    if (stricmp(tokens[0].data(),"setSmoothCount")==0)
    {
      if (tokens.size()==2)
      {
        int frames = (int)atof(tokens[1].data());
        if (frames>=0) AveragingMaxSize = frames;
      }
    }
    if (stricmp(tokens[0].data(),"getSmoothCount")==0)
    {
      sprintf(resultStr,"[%d]",AveragingMaxSize);
      // Make sure the tail is allways null terminated.
      resultStr[sizeof(resultStr)-1] = 0;
    }
  }

  return resultStr;
}

// Function that will register the ExecuteCommand function of the engine
VBSPLUGIN_EXPORT void WINAPI RegisterHWND(void *hwnd, void *hins)
{
}

BOOL APIENTRY DllMain
(
  HMODULE hModule,
  DWORD  ul_reason_for_call,
  LPVOID lpReserved
)
{
  LogF("DLL main called\r\n");

	switch (ul_reason_for_call)
	{
	  case DLL_PROCESS_ATTACH:
      LogF("Cubic Process attached\r\n");
      break;
    case DLL_PROCESS_DETACH:
      LogF("Cubic Process detached\r\n");
      break;
	  case DLL_THREAD_ATTACH:
      LogF("Cubic Thread attached\r\n");
      break;
	  case DLL_THREAD_DETACH:
      LogF("Cubic Thread detached\r\n");
      break;
	}

  return true;
}

// This function is used by a special version of VBS2
VBSPLUGIN_EXPORT void WINAPI SetCubicInterface(void *ptr)
{
  LogF("Registered cubic interface\r\n");
  cInterface = (CubicVBS2Interface*) ptr;
  
  unloadDllAvaliable = cInterface ? true : false;
  if (!cInterface) return;
}
