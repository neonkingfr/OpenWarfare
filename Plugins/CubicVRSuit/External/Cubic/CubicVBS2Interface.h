// *******************************************************************************
// Primary interface to the Cubic VBS2 interface DLL.  This header includes all
// references to libraries needed to link this program into your code.  Just
// include this header file.  Also, make sure the CubicVBS2Plugin.lib is available
// for compiling, and the CubicVBS2Plugin.dll is available when running.
//
// This class exposes all methods needed to get entity (head) location/orientation
// data, rotation/sensor data for the weapon, and, optionally, raw data related to the
// body, and leg sensors.
//
// (c) 2009 CUBIC
//
// Project: 
//
//     CubicVBS2Interface
//
// History:
//
//     GAZ 4/22/2009 - Initial implementation
//
// *******************************************************************************
#pragma once

#include <windows.h>

#ifdef CUBICVBS2_EXPORT
	#define CUBICVBS2 __declspec(dllexport)
#else
	#define CUBICVBS2 __declspec(dllimport)
#endif

#ifndef CUBICVBS2_EXPORT
#pragma comment(lib, "CubicVBS2Plugin.lib" )
#endif

#define USE_LOCAL_MATRIX

#define ANGLE_0_DEGREES     0.0           // angle values are in radians
#define ANGLE_1_DEGREE      0.017453
#define ANGLE_10_DEGREES    0.174533
#define ANGLE_25_DEGREES    0.436332
#define ANGLE_45_DEGREES    0.785398
#define ANGLE_50_DEGREES    0.872665
#define ANGLE_70_DEGREES    1.221731
#define ANGLE_90_DEGREES    1.570796
#define ANGLE_110_DEGREES   1.919862
#define ANGLE_130_DEGREES   2.268928
#define ANGLE_180_DEGREES   3.141593
#define ANGLE_230_DEGREES   4.014257
#define ANGLE_250_DEGREES   4.363323
#define ANGLE_270_DEGREES   4.712389
#define ANGLE_290_DEGREES   5.061455
#define ANGLE_310_DEGREES   5.410521
#define ANGLE_360_DEGREES   6.283185

#define X_INDEX 0
#define Y_INDEX 1
#define Z_INDEX 2

/**
 * This class exposes all methods needed to get cooked entity location
 * data, and raw sensor data for the weapon, head, body, and legs.  
 *
 * This is a singleton class design.  
 *
 * Initialization:
 *
 * To create the singleton and begin using it call:
 *
 * if ( CubicVBS2Interface::GetInstance() == NULL )
 * {
 *      // Document singleton creation error and exit.
 * }
 *
 * After an instance has been created, the application can update the entity 
 * location data.  For example, to place the entity into the world coordinate
 * system you can call:
 *
 * CubicVBS2Interface::GetInstance()->SetEntityStartLocation( dXaxis, dYaxis, dZaxis );
 *
 * Also just after initialization you can change the maximum velocity 
 * rates for the joystick control.  The maximum velocity is in meters/second.
 * It's likely Cubic will tweak this value internally, so it's here mostly
 * as a convenience if the default values don't work well for the application.
 *
 * CubicVBS2Interface::GetInstance()->SetJoystickMaxVelocityForwardBack( 20.0 );
 * CubicVBS2Interface::GetInstance()->SetJoystickMaxVelocityRightLeft( 20.0 );
 *
 * Once all initialization values are set then make a call to start the communication
 * threads that will update the sensor data for the entity, and weapon.  Keep in
 * mind that this might hang onto your thread for a while, so make this call somewhere
 * where timing is not critical.  Also, to use debug sensor data from the file
 * "DebugComm.dat", you must put the "dat" file in your bin directory, and you must 
 * pass in CubicVBS2Interface::READ_LOG_DL as a parameter to InitializeInstance().
 *
 * EInitializeReturn eReturn = CubicVBS2Interface::GetInstance()->InitializeInstance();
 * if ( eReturn != NO_ERROR_IER )
 * {
 *     // Document error and exit.
 * }
 * 
 * Once the communication threads are running, the application will sound a one
 * second beep on the PC internal speaker.  This will be followed by 5 short
 * beeps indicating the 5 sensors are sending data.  When the firer hears 
 * these beeps, he should stand at attention, eyes forward, with the weapon held 
 * so the barrel points straight out from the body.  For example, the firer can
 * tuck the weapon under their shoulder/arm with the barrel at 90 degree angle to
 * their chest and the floor.  The firer then presses the joystick button (hopefully 
 * within reach) to initiate the calibration.  After calibration is done, another one 
 * second beep should be heard.
 *
 * The application shouldn't do anything with the data until the DLL is calibrated.
 * A simple test like this will keep from getting bad data prior to calibration.
 *
 * if ( CubicVBS2Interface::GetInstance()->IsCalibrated() )
 * {
 *      // With the weapon calibrated the rest of the tests can be done
 *
 * Entity Location and Sensors:
 *
 * Calling GetEntityLocation() without parameters causes the DLL to do a simple
 * linear interpolation over time based on the last joystick information.  If 
 * you want the raw location information pass in false.  Also, by default the
 * estimated head sensor position information is added to the head location.  It
 * is possible one or both of these values will not be wanted so I've made their
 * use optional in both cases.
 *
 * sEntityLocation = CubicVBS2Interface::GetInstance()->GetEntityLocation();
 *
 * In general, the GetEntityLocation() call will be used to get the "head" location 
 * information.  Raw information on the head, body, weapon, and leg sensors comes 
 * from the raw sensor data calls (shown below).  In the case of the head, body, and 
 * weapon sensor data, we cook the acceleration to create sampled velocity and 
 * displacement data; the accuracy of this data depends on my internal sample rate
 * and from what I'm seeing the data will not be accurate.  In the case of the leg 
 * sensors, we only have acceleration data, and from that we generate the Pitch
 * orientation angle; all other data is zeroed out.
 *
 * sHeadSensor = CubicVBS2Interface::GetInstance()->GetEntityHeadSensorData();
 * sBodySensor = CubicVBS2Interface::GetInstance()->GetEntityBodySensorData();
 * sWeaponSensor = CubicVBS2Interface::GetInstance()->GetWeaponSensorData();
 * sLeftLegSensor = CubicVBS2Interface::GetInstance()->GetEntityLeftLegSensorData();
 * sRightLegSensor = CubicVBS2Interface::GetInstance()->GetEntityRightLegSensorData();
 *
 * Given the head location and orientation, the body will be attached as needed and
 * it's rotations will be adjusted by the rotations reported in the sBodySensor data;
 * you can use the local rotation matrix, local euler values, or global rotation matrix.
 * I would recommend ignoring the acceleration, velocity, and displacement data.  With 
 * the body placed, the legs can be oriented similarly by using the local rotation matrix;
 * the global rotation matrix and the euler data is not supported in the leg sensors.  I 
 * have no idea how the feet will be placed except to look at the basic stance and hope 
 * you get close.  With the entities limbs and body oriented to the head, the feet can 
 * then be ground clamped.  Obviously, as the feet either raise, lower, and/or rotate 
 * the entity, the view will have to be adjusted too.  This should give a good "view" 
 * for the firer.
 *
 * The plan is to have the local rotation matrix zeroed out at calibration.  Thus,
 * the sensors will be oriented consistently relative to a normal standing entity.  This
 * is the best starting point for orienting body parts.  If you try to use the global rotation 
 * matrix to place the body parts, you'll need to create an inverse matrix for each body 
 * part that will initially rotate the body parts into the same world coordinate system 
 * as the sensors (i.e. with the sensor at zero degrees the sensor top is facing up 
 * and the sensor wire is facing south).  The problem with this is trying to figure out
 * the inverse matrix for all the sensors since each sensor will be placed in a unique 
 * orientation on the body part.  So it's much easier to use the local rotation matrix; 
 * the body parts need to be initially in the same orientation as described in the sensor 
 * calibration step, above, then simply apply the local rotation matrix.  
 *
 * The local and global rotation matrixes are stored in a 16 element float array representing
 * a standard 4x4, row major, matrix in a right handed coordinate system.  The following 
 * pseudo code multiplies a point by a rotation matrix using this format.
 *
 *      for ( int row = 0; row < 3; row++ )
 *         for ( int col = 0; col < 3; col++ )
 *            dPoint3D[row] += RotationMatrix[row*4+col] * dPoint[col];
 *
 * When the entity is moving around the environment it's possible it will hit
 * an object such as a wall.  Obviously, having the entity ghost through walls
 * is not a good idea.  Unfortunately, I don't know where obstacles are, so we
 * need a way for the application to force a position update.  X and Z represent
 * the plane the entity moves across during normal updates.  The Y axis (altitude)
 * and orientation can not be changed by the application, since changing them would
 * break calibration.
 *
 * CubicVBS2Interface::GetInstance()->SetEntityLocation( dXaxis, dZaxis );
 * 
 * This will force a change of the current entity location.  If during 
 * initialization you passed in a valid entity start position using the call to
 * SetEntityStartLocation(), then just use your entity location values.  However, 
 * if you didn't give an initial start location for the world coordinate system, 
 * make sure you pass the data in using the (0,0,0) start based coordinate system.
 *
 * Weapon status data:
 *
 * At this point we should have a weapon connected, but since weapons might be
 * turned off by accident, it's best to double check before accessing the 
 * weapon sensor and status information.
 *
 * if ( CubicVBS2Interface::GetInstance()->IsWeaponConnected != CONNECTED_WCS )
 * {
 *     // access weapon status data here
 *
 * eWeaponType = CubicVBS2Interface::GetInstance()->GetWeaponType();
 * sWeaponLocation = CubicVBS2Interface::GetInstance()->GetWeaponLocation();
 *
 * There are a number of "weapon state" methods you can call.  For example, to
 * find out if a weapon fired you can use this:
 *
 *     bool bFired1 = CubicVBS2Interface::GetInstance()->IsWeaponFireEvent();
 *     bool bFired2 = false;
 *     if ( eWeaponType == M4_M320_WT )
 *        bFired2 = CubicVBS2Interface::GetInstance()->IsWeaponFireEvent( WEAPON_ID_2 );
 *
 * Some of the weapon methods, like IsWeaponFireEvent(), take a weapon identification
 * parameter.  In most cases you can take the default WEAPON_ID_1 weapon identification.
 * However, there are two exceptions.  In the case of the M4/M320 you use the default
 * value to get state information for the M4 part of the weapon and WEAPON_ID_2 for
 * the M320 part of the weapon.  Also, the M249 has both belt and magazines.  For 
 * ammunition data about the belt use the default value, and for ammunition data for
 * the magazine use WEAPON_ID_2.
 *
 * Also we've made an initial attempt at determining when the firer is aiming the
 * weapon.  If this returns true, there's a reasonable chance the firer is about
 * to aim and fire the weapon.  Unfortunately, I haven't had a chance to test
 * the logic of my approach.  I'm hoping this approach will be reasonably independent
 * of the stance, since it depends on the head and weapon sensor data only.  
 *
 * if ( CubicVBS2Interface::GetInstance()->IsWeaponAimed() )
 * {
 *    // Execute weapon is being aimed logic.
 *
 * (c) 2009 CUBIC
 */
class CUBICVBS2 CubicVBS2Interface
{
public:

   friend class CDebugComm;
   friend class CMotionNodeComm;
   friend class CWeaponComm;
   friend class CHostComm;

   /**
    * This enumeration contains the error return values for the 
    * InitializeInstance() call.
    *
    * @see CubicVBS2Interface::InitializeInstance()
    */
   typedef enum EInitializeErrorReturn
	{ 
      NO_ERROR_IER = 0,
      HOST_COMM_ERROR_IER,
      WEAPON_COMM_ERROR_IER,
      SENSOR_COMM_ERROR_IER,
      DEBUG_COMM_ERROR_IER
	};

   /**
    * This enumeration contains the parameter input for the 
    * InitializeInstance() call to indicate if the dll should
    * run normally, log the run, or read in a previously logged
    * run.
    *
    * @see CubicVBS2Interface::InitializeInstance()
    */
   typedef enum ELog
   {
      NORMAL_OPERATION_DL,
      WRITE_LOG_DL,
      READ_LOG_DL
	};

   /**
    * This enumeration contains the possible stances for the entity.
    */
	typedef enum EEntityStance
	{
      STAND_ES,
      KNEEL_ES,
      PRONE_ES,
      CROUCH_ES,   // Equivalent to Stand but logic allowed differentiation
      CRAWL_ES     // Equivalent to Stand but logic allowed differentiation
	};

   /**
    * This structure contains velocity information.  This will hold
    * cooked data that can be used to update positions based on delta
    * times.  Positive x-axis is to right of object.  Positive z-axis
    * is in front of object.  Positive y-axis is above object.
    *
	 * XYZ velocity is in meters a second
    */
	struct SVelocity
	{
		double X_vel;
		double Y_vel;
		double Z_vel;
	};

   /**
    * This structure contains MotionNode information.  This holds the 
    * raw data from the motion node sensors (the rotations and accelerations)
    * and it holds cooked data that can be used to update positions based on 
    * (the displacements and velocities) delta times.  Positive x-axis is to 
    * right of object, positive z-axis is in front of object, and positive 
    * y-axis is above object in the object coordinate system.
    * 
	 * Accelerations are in meters per second per second
	 * Angles are in radians
    * Displacements are in meters
    * Velocities are in meters per second
    */
	struct SMotionNode
	{
      double X;
      double X_vel;
		double X_acl;
      double Y;
      double Y_vel;
		double Y_acl;
      double Z;
      double Z_vel;
		double Z_acl;
		double Yaw_angle;
		double Pitch_angle;
		double Roll_angle;
      float RotationMatrixLocal[16];
      float RotationMatrixGlobal[16];
	};

   /**
    * This structure contains location, orientation, and stance
    * information.  This holds the cooked position and rotation data for 
    * the entity and the sensors.  The stance is a quick and dirty 
    * attempt to pass on our expected entity stance.
    *
    * Positive x-axis is to right of object, positive z-axis
    * is in front of object, and positive y-axis is above object in
    * the object coordinate system.
    *
	 * Locations (XYZ) are in meters
	 * Rotations (YPR) are in radians
    */
	struct SEntityLocation
	{
		double X;
		double Y;
		double Z;
		double Yaw;
		double Pitch;
		double Roll;
      EEntityStance Stance;
	};

   /**
    * This enumeration contains the available weapon connection states.  Used as a 
    * return value when testing if a weapon is available.  For internal use only
    * end user will see true/false value from IsWeaponConnected().
    *
    * @see IsWeaponConnected()
    */
	typedef enum EWeaponConnectionStatus
	{
		CONNECTED_WCS,
		DISCONNECTED_WCS,
		FAILURE_WCS
	};

   /**
    * This enumeration contains the available weapon types.
    *
    * @see GetWeaponType()
    */
   typedef enum EWeaponsType
	{ 
		M4_WT = 0,
      M249_WT,
		M4_M320_WT,
		NO_WEAPON_WT
	};

   /**
    * This enumeration contains the available weapon identifiers.  For now
    * each entity can have one weapon.  However, the M4/M320 is really
    * two weapons in one.  So when trying to determine the state of the M4 the
    * default value of WEAPON_ID_1 can be used.  When trying to determine the
    * state of the M320 you must pass in WEAPON_ID_2.  Also the M249 can have
    * either belts or magazines attached.  When setting or getting ammunition
    * information for the M249 you use the default value of WEAPON_ID_1 for
    * the belt and WEAPON_ID_2 for the magazine.
    */
	typedef enum EWeaponId
	{
		WEAPON_ID_1 = 0, // Primary weapon
		WEAPON_ID_2      // Secondary weapon
	};

   /**
    * This enumeration contains the available weapon modes.  Single shot
    * indicates the weapon will fire one round per trigger pull.  Three burst
    * indicates the weapon will fire three rounds per trigger pull.  Full auto
    * generally can be interpreted as a 5 to 7 round burst per trigger pull.
    */
	typedef enum EWeaponMode
	{
		SINGLE_SHOT_WM,
		THREE_RND_BURST_WM,
		FULL_AUTO_WM
	};

   /**
    * This enumeration contains the available weapon mount points for the avatar.
    * RIGHT_SHOULDER indicates the butt of the weapon is attached to the right shoulder.
    * LEFT_SHOULDER indicates the butt of the weapon is attached to the left shoulder.
    */
	typedef enum EWeaponMountPoint
   {
      RIGHT_SHOULDER,
      LEFT_SHOULDER
   };

   /**
    * This enumeration contains the weapon container state.  In order for a
    * weapon to be reloaded the old container must be removed and a new one 
    * inserted.  This is mostly used internally to determine if the weapon
    * has been reloaded.
    *
    * @see IsWeaponAmmoContainerOn()
    */
	typedef enum EWeaponAmmoContainerState
	{
		CONTAINER_OUT_WCS = 0,
		CONTAINER_IN_WCS
	};

   /**
    * This structure contains the raw weapon state information.  This is used
    * internally to store the state information for the weapon.  The index into
    * the values use the EWeaponId enumeration to differentiate between primary
    * and secondary weapon states.
    */
   struct SWeaponState
	{
      EWeaponConnectionStatus eConnectionState;
      EWeaponsType eType;
      EWeaponMode eMode[2];
      EWeaponAmmoContainerState eAmmoContainerState[2];
      bool bRoundInChamber[2];
      bool bCocked[2];
      bool bSafe[2];
      bool bBarrel[2];
      bool bCover[2];
      bool bReloaded[2];
      int nFireEvent[2];
      int nRoundsAvailablePerContainer[2];
      int nRoundsInCurrentContainer[2];
      int nNumberOfContainers[2];
      double dBattery;
	};

private:

   /**
    * The Cubic to VBS2 interface constructor.
    * This is a private constructor since this class in instantiated
    * using the singleton model.
    */
	CubicVBS2Interface();

public:

   // -----------------------------------------------------
   // DLL instantiation and initialization ...
   // -----------------------------------------------------

   /**
    * The Cubic to VBS2 interface class destructor.  This resets the instance
    * and allows a new CubicVBS2Interface class to be created.
    *
    * @see CubicVBS2Interface::CubicVBS2Interface()
    */
   ~CubicVBS2Interface();

   /**
    * The static method that creates the singleton instance of this class.
    * Always use this call to access the singleton instance before calling
    * methods in this class.
    */
   static CubicVBS2Interface *GetInstance();

   /**
    * This method initializes the communications to the weapons and sensors.
    * You should delay calling this method if you wish to initialize data value
    * etc.  Keep in mind this method will likely take control of the calling
    * thread for a while, so make this call where timing is not an issue.  Pay
    * close attention to the return value; if there were problems with the 
    * communications threads, this will be the only place the application
    * can hear about them.
    *
    * @see CubicVBS2Interface::EInitializeErrorReturn
    * @param eLogType Indicates if logging is being used, defaults to no logging
    * @return Initialization error results.
    */
   EInitializeErrorReturn InitializeInstance( ELog eLogType = NORMAL_OPERATION_DL );

   /**
    * This method closes and reopens the data file.  Starts replaying from the 
    * beginning of the data file.
    *
    * @see CubicVBS2Interface::InitializeInstance
    * @return True if rewind successful, false if not in read log mode.
    */
   bool RewindDebugCommLog();

   /**
    * This method determines if the system has been calibrated.  Used by the application
    * to insure the calibration steps are complete.  Do not use any sensor data before
    * this returns true; any returned values when this is false are undefined.
    *
    * @return true, if the system has been calibrated; false, otherwise
    */
   bool IsCalibrated();

   // -----------------------------------------------------
   // Entity & weapon state ...
   // -----------------------------------------------------

   /**
    * This method returns the entity ("head") location structure.  The entity location
    * is in a simple flat world where most of the changes are in the X and Z
    * axis.  The Y-axis (height) will only change with movement of the head
    * up or down.  The Y-axis can be used to adjust the view up or down
    * in order to have the view track the movement of the head.
    *
    * We intend to use the joystick to control forward/back, and right/left
    * position changes.  The body sensor Yaw angle will be used to change
    * the orientation.  This presents a problem since the head and body
    * can move independently.  So the implementation will let the body
    * Yaw angle simply change the effect the joystick has on the head
    * location.
    *
    * During testing we verified the head sensor estimate will not work; thus, 
    * we now default the bHeadSensorEstimate to false.  
    *
    * The joystick linear estimate seems to work fine so we left it at true.
    *
    * Also, we added a new estimate of head position based on the body and head 
    * rotation angles.  This has not been tested using visuals but it looked 
    * promising.  Basically, we use the body centroid as the rotation point
    * for the body, we assume the head attaches to a point 37.5cm above the 
    * centroid, and we assume the head rotates on this point and is 30cm above 
    * this point.  We then rotate the body and head, translate the head to
    * the body.  To zero out the standing stance we then translate by -67.5cm in 
    * the Y-axis so the Y-axis has zero displacement when the entity is standing.  
    * This new "top of head" point is the estimated head displacement.  It seems 
    * to work but more testing is needed.
    *
    * @warning This is a major change from the last version of the interface.
    * @param bJoystickLinearEstimate True, use delta time/velocity update location
    * @param bHeadSensorEstimate True, use the delta X,Y,Z values from the head sensor
    * @param bBodyHeadRotationEstimate True, use the head and body rotation matrix to estimate location
    * @return SEntityLocation Struct containing the entity location data
    */
	SEntityLocation GetEntityLocation( bool bJoystickLinearEstimate = true, 
                                      bool bHeadSensorEstimate = false,
                                      bool bBodyHeadRotationEstimate = true );

   /**
    * This method allows the main application to change the initial entity ("head") 
    * location.  We removed the orientation option since this will likely incur 
    * more calculation overhead then needed.  We've added to the MotionNode data
    * the rotation matrix for each sensor, and we added a call to get the raw
    * head sensor data; this simplifies the placement of the entity.  It was
    * assumed the application would rather use its entity orientation matrix to 
    * place the entity.
    *
    * @warning This is a major change from the last version of the interface.
    * @param dXaxis The new x axis value for the entity.
    * @param dYaxis The new y axis value for the entity.
    * @param dZaxis The new z axis value for the entity.
    */
   void SetEntityStartLocation( double dXaxis, double dYaxis, double dZaxis );

   /**
    * This method allows the main application to change the current entity ("head") 
    * location in the X Z plane.  The main reason for this call is to allow the application
    * to keep the entity from walking through obstacles.  It allows the application
    * to reset the position dynamically, without disturbing the calibration data.  Thus,
    * there is no Y-axis (altitude) or orientation changes, since these changes
    * would break calibration.
    *
    * @warning Make sure you use the correct coordinate system.
    * @see CubicVBS2Interface::SetEntityStartLocation()
    * @param dXaxis The new x axis value for the entity.
    * @param dZaxis The new z axis value for the entity.
    */
   void SetEntityLocation( double dXaxis, double dZaxis );

   /**
    * This method allows the main application to change just the entity stance.
    * This should only be used during initialization or disaster recovery.
    *
    * @param eStance Enumeration value of the new stance.
    */
   void SetEntityStance(EEntityStance eStance);

   /**
    * This method determines if the system is using the local or global motionnode 
    * rotation matrixes to do it's calculations.  This relates to the orientation
    * of the head, body, and weapon sensors only.  The leg sensors should always use 
    * the local rotation matrix, since this data is cooked.  
    *
    * The local rotation matrix has been fixed so it or the eular angles should always be used.
    *
    * The main difference between using the local matrix and the global is the way the
    * entity models will have to be initialized.  If we use the local matrix the calibrated
    * orientation will be with the entity standing normally.  However, if we have to use
    * the global matrix the different model parts will have to be oriented so the sensor
    * that's attached to them is pointing to the zero angle direction; this becomes much more
    * complicated for the application.  It will require the model for the body, head and
    * weapon be rotated from it's normal position to the position where the sensor is
    * oreinted correctly.  Then the global rotation matrix can be applied to place the
    * part into the world.
    *
    * @return True, use the local rotation matrix, False, use the global rotation matrix.
    */
   bool IsUsingLocalRotationMatrix();

   /**
    * This method returns the entity stance state.  The application should not use
    * this method too often - there is a slight calculation overhead.  Instead they
    * should get the data from the GetEntityLocation() call.
    *
    * @see EEntityStance
    * @return EEntityStance value containing the current entity stance
    */
   EEntityStance GetEntityStance();

   /**
    * This method returns the raw head sensor data structure.  The head sensor does
    * fill in all the data in the MotionNode data structure.
    *
    * Included in this data struct are estimates for position and velocity deltas.  
    * These values are based on a very ruff estimate of the cumulative effect of the 
    * acceleration data reported by the sensor.  These deltas should be used with 
    * caution as they're likely to drift from true over time.
    *
    * @see SMotionNode
    * @return SMotionNode Struct containing the raw head data
    */
   SMotionNode GetEntityHeadSensorData();

   /**
    * This method returns the body yaw angle in radians between -180 and 180 degrees.
    * The way the motion node eular angles rotate in arbitrary directions with the
    * yaw angle staying between -90 and 90 degrees, I found it helpful to have a 
    * relatively quick call that would give me clean yaw angles.  This was used to
    * give the legs a proper yaw rotation; the legs basically doesn't come with a 
    * yaw angle this will map them to the body properly.  
    *
    * This method uses a sqrt() call so be prudent in using it.  If the yaw angle is 
    * undefined it returns zero degrees.
    *
    * @return The Body yaw angle in radians between -180 and 180 degrees.
    */
   double GetBodyYawAngleRadians();

   /**
    * This method returns the body pitch angle in radians between -180 and 180 degrees.
    *
    * This method uses a sqrt() call so be prudent in using it.  If the pitch angle is 
    * undefined it returns zero degrees.
    *
    * @return The Body pitch angle in radians between -180 and 180 degrees.
    */
   double GetBodyPitchAngleRadians();

   /**
    * This method returns the raw body sensor data structure.  The body sensor does
    * fill in all the data in the MotionNode data structure.
    *
    * Included in this data struct are estimates for position and velocity deltas.  
    * These values are based on a very ruff estimate of the cumulative effect of the 
    * acceleration data reported by the sensor.  These deltas should be used with 
    * caution as they're likely to drift from true over time.
    *
    * @see SMotionNode
    * @return SMotionNode Struct containing the raw body data
    */
   SMotionNode GetEntityBodySensorData();

   /**
    * This method returns the head yaw angle in radians between -180 and 180 degrees.
    *
    * This method uses a sqrt() call so be prudent in using it.  If the yaw angle is 
    * undefined it returns zero degrees.
    *
    * @return The head yaw angle in radians between -180 and 180 degrees.
    */
   double GetHeadYawAngleRadians();

   /**
    * This method returns the head pitch angle in radians between -180 and 180 degrees.
    *
    * This method uses a sqrt() call so be prudent in using it.  If the pitch angle is 
    * undefined it returns zero degrees.
    *
    * @return The head pitch angle in radians between -180 and 180 degrees.
    */
   double GetHeadPitchAngleRadians();

   /**
    * This method returns the weapon yaw angle in radians between -180 and 180 degrees.
    * The way the motion node eular angles rotate in arbitrary directions with the
    * yaw angle staying between -90 and 90 degrees, I found it helpful to have a 
    * relatively quick call that would give me clean yaw angles.  
    *
    * This method uses a sqrt() call so be prudent in using it.  If the yaw angle is 
    * undefined it returns zero degrees.
    *
    * @return The weapon yaw angle in radians between -180 and 180 degrees.
    */
   double GetWeaponYawAngleRadians();

   /**
    * This method returns the weapon pitch angle in radians between -180 and 180 degrees.
    *
    * This method uses a sqrt() call so be prudent in using it.  If the pitch angle is 
    * undefined it returns zero degrees.
    *
    * @return The weapon pitch angle in radians between -180 and 180 degrees.
    */
   double GetWeaponPitchAngleRadians();

   /**
    * This method returns the weapon sensor data structure.  The weapon sensor does
    * fill in all the data in the MotionNode data structure.
    *
    * Included in this data struct are estimates for position and velocity deltas.  
    * These values are based on a very ruff estimate of the cumulative effect of the 
    * acceleration data reported by the sensor.  These deltas should be used with 
    * caution as they're likely to drift from true over time.
    *
    * @see SMotionNode
    * @return SMotionNode Struct containing the weapon data
    */
   SMotionNode GetWeaponSensorData();

   /**
    * This method returns the raw left leg sensor data.  We only support
    * the Pitch orientation data for the leg sensors.  We use the Yaw data
    * from the body sensor to determine the Yaw for the leg.
    *
    * @see GetBodyYawAngleRadians()
    * @return SMotionNode Struct containing the raw left leg data
    */
   SMotionNode GetEntityLeftLegSensorData();

   /**
    * This method returns the raw right leg sensor data.  We only support
    * the Pitch orientation data for the leg sensors.  We use the Yaw data
    * from the body sensor to determine the Yaw for the leg.
    *
    * @see GetBodyYawAngleRadians()
    * @return SMotionNode Struct containing the raw right leg data
    */
   SMotionNode GetEntityRightLegSensorData();

   /**
    * This method allows the main application to determine the state of
    * the weapons battery.
    *
    * @return 0.0 = Battery must be changed, 1.0 = Fully charged
    */
	double GetWeaponBatteryState();

   // -----------------------------------------------------
   // Joystick Data
   // -----------------------------------------------------

   /**
    * This method determines if the joystick button on the weapon was pressed.
    * Currently this has no know function.  In the future that may change.
    *
    * @return true, the joystick button1 is pressed; false, otherwise
    */
   bool IsJoystickButton1Pressed();

   /**
    * This method returns the current joystick position forward/back.  This is 
    * made public so the application can monitor this value for its own uses.
    * The joystick effects are already taken into account in the 
    * GetEnityLocation() call.
    *
    * @see CubicVBS2Interface::GetEntityLocation()
    * @return Range -1.0 to 1.0 Movement forward/back
    */
   float GetJoystickMoveForwardBack();

   /**
    * This method returns the current joystick position left/right.  This is 
    * made public so the application can monitor this value for its own uses.
    * The joystick effects are already taken into account in the 
    * GetEnityLocation() call.
    *
    * @see CubicVBS2Interface::GetEntityLocation()
    * @return Range -1.0 to 1.0 Movement left/right
    */
	float GetJoystickMoveRightLeft();

   /**
    * This method returns the current setting for the forward/back maximum 
    * movement in meters per second.  This is used to convert the joystick
    * changes during update into discreet movement values.  The delta time
    * between calls to the SetJoystickMoveForwardBack() is used to approximate
    * the movement during that sample period.  We multiply the delta time by
    * the percent of the max velocity that the joystick indicates to get a
    * meters offset.  This offset is adjusted by applying the current body yaw
    * angle, and accumulating the data in a local struct.  The result is later 
    * added to the entity "head" location when the GetEntityLocation() method 
    * is called.
    *
    * @see CubicVBS2Interface::GetEntityLocation()
    * @return Maximum velocity for the entity when moving forward or backward.
    */
   float GetJoystickMaxVelocityForwardBack();

   /**
    * This method sets the value for the forward/back maximum movement in 
    * meters per second.  This method can be called by the application to 
    * adjust the maximum velocity value.  Mostly put here in case the default
    * values don't work out.  This way the application can tweak the numbers
    *
    * @param fMetersPerSecond Maximum velocity for the entity when moving forward or backward.
    */
   void SetJoystickMaxVelocityForwardBack( float fMetersPerSecond );

   /**
    * This method returns the current setting for the left/right maximum 
    * movement in meters per second.  This is used to convert the joystick
    * changes during update into discreet movement values.  The delta time
    * between calls to the SetJoystickMoveForwardBack() is used to approximate
    * the movement during that sample period.  We multiply the delta time by
    * the percent of the max velocity that the joystick indicates to get a
    * meters offset.  This offset is adjusted by applying the current body yaw
    * angle, and accumulating the data in a local struct.  The result is later 
    * added to the entity "head" location when the GetEntityLocation() method 
    * is called.
    *
    * This will create a sliding effect in the view.  The body yaw angle 
    * will determine the rotation right and left.  The joystick always
    * changes the view by moving forward/back and sliding right/left.
    *
    * @see CubicVBS2Interface::GetEntityLocation()
    * @return Maximum velocity for the entity when moving left or right.
    */
   float GetJoystickMaxVelocityRightLeft();

   /**
    * This method sets the value for the left/right maximum movement in 
    * meters per second.  This method can be called by the application to 
    * adjust the maximum velocity value.  Mostly put here in case the default
    * values don't work out.  This way the application can tweak the numbers
    *
    * @param fMetersPerSecond Maximum velocity for the entity when moving forward or backward.
    */
   void SetJoystickMaxVelocityRightLeft( float fMetersPerSecond );

   // -----------------------------------------------------
   // Weapon State
   // -----------------------------------------------------

   /**
    * This method indicates when a weapon is connected to the system.  This should
    * be checked before accessing the weapon status information.
    *
    * @return True, indicates a weapon is attached, false, otherwise
    */
   bool IsWeaponConnected();

   /**
    * This method uses the fire stance calibration data to try to determine if
    * the firer is aiming the weapon.  There are two stages to this decision.
    * One, the relative angles of the head and weapon sensor are compared; if
    * all angles are within 25 degrees then the second stage is attempted.  If
    * the angles are outside 25 degrees this method returns false.  Two, we 
    * use the aim calibration data to estimate the distance between the head
    * and weapon sensors.  If this distance is within the distance of the
    * known fire orientation (+20%) then this method returns true; otherwise
    * it returns false.
    *
    * @return True, indicates a weapon is being aimed, false, otherwise
    */
   bool IsWeaponAimed();

   /**
    * After a weapon is detected this method will return the weapon type.  Generally,
    * once a weapon has been attached this data will remain until a new weapon
    * is attached or we get data from the belt indicating the weapon is no longer
    * connected and the status data is updated to indicate the new state.  Testing
    * for NO_WEAPON_WT from this method or double checking the IsWeaponConnected()
    * call before each cycle through the data is a good idea.
    *
    * @see CubicVBS2Interface::EWeaponsType
    * @see CubicVBS2Interface::IsWeaponConnected()
    * @return Weapon type as defined in EWeaponsType enumeration or NO_WEAPON_WT
    */
   EWeaponsType GetWeaponType();
	
   /**
    * After a weapon is detected this method will indicate when the weapon is reloaded.
    * You only get one chance to capture the reload state.  The first time it's called,
    * after the weapon has been reloaded, the reload flag is set back to false.
    *
    * @see CubicVBS2Interface::EWeaponsType
    * @see CubicVBS2Interface::IsWeaponConnected()
    * @param eWeaponId Optional weapon id, indicates primary or secondary weapon
    * @return True, new belt or magazine reloaded for eWeaponId, false otherwise
    */
   bool GetWeaponReloadState(EWeaponId eWeaponId = WEAPON_ID_1);
   
   /**
    * After a weapon is detected this method will indicate when the weapon has a magazine or
    * belt inserted.  Make sure IsWeaponConnected() returns true before sampling this data,
    * otherwise it will return the result of the last weapon status received.
    *
    * @see CubicVBS2Interface::IsWeaponConnected()
    * @param eWeaponId Optional weapon id, indicates primary or secondary weapon
    * @return True, new belt or magazine in weapon, false otherwise
    */
   bool IsWeaponAmmoContainerOn(EWeaponId eWeaponId = WEAPON_ID_1);

   /**
    * After a weapon is detected this method will indicate when the weapon is cocked.
    * Make sure IsWeaponConnected() returns true before sampling this data, otherwise 
    * it will return the result of the last weapon status received.
    *
    * @see CubicVBS2Interface::IsWeaponConnected()
    * @param eWeaponId Optional weapon id, indicates primary or secondary weapon
    * @return True, weapon is cocked, false otherwise
    */
   bool IsWeaponCocked(EWeaponId eWeaponId = WEAPON_ID_1);

   /**
    * After a weapon is detected this method will indicate when the weapons safety 
    * is engaged.  Make sure IsWeaponConnected() returns true before sampling this 
    * data, otherwise it will return the result of the last weapon status received.
    *
    * @see CubicVBS2Interface::IsWeaponConnected()
    * @param eWeaponId Optional weapon id, indicates primary or secondary weapon
    * @return True, weapon's safety is on, false otherwise
    */
   bool IsWeaponSafe(EWeaponId eWeaponId = WEAPON_ID_1);

   /**
    * After a weapon is detected this method will indicate when the weapons has a
    * round chambered.  Make sure IsWeaponConnected() returns true before sampling this 
    * data, otherwise it will return the result of the last weapon status received.
    *
    * @see CubicVBS2Interface::IsWeaponConnected()
    * @param eWeaponId Optional weapon id, indicates primary or secondary weapon
    * @return True, weapon has round chambered, false otherwise
    */
   bool IsWeaponRoundInChamber(EWeaponId eWeaponId = WEAPON_ID_1);

   /**
    * After a weapon is detected this method will indicate when the trigger on the 
    * weapon has been pulled.  This will indicate a valid fire event has taken place
    * since the weapons will control when they are ready to fire.  Pulling the trigger
    * on a weapon that is not ready to fire will not set this flag to true.  Make sure 
    * IsWeaponConnected() returns true before sampling this data, otherwise it will 
    * return the result of the last weapon status received.
    *
    * The M4/M320 can have one of two fire events.  Either the primary (WEAPON_ID_1 
    * i.e. the M4) weapon can have a fire event, or the secondary (WEAPON_ID_2 i.e.
    * the M320 can have a fire event.  For all other weapons only the primary weapon
    * fire event is valid.
    *
    * @see CubicVBS2Interface::IsWeaponConnected()
    * @param eWeaponId Optional weapon id, indicates primary or secondary weapon
    * @return True, weapon has fired, false otherwise
    */
   bool IsWeaponFireEvent(EWeaponId eWeaponId = WEAPON_ID_1);

   /**
    * This method will return the number of rounds available in each magazine or belt.  
    * This is the number of rounds that will be added to the current container when 
    * the weapon is reloaded.
    *
    * The M249 can have either belts or magazines.  In order to support this we've 
    * put the belt ammunition data in the primary weapon (WEAPON_ID_1) and the 
    * magazine information in the secondary weapon (WEAPON_ID_2).
    *
    * @param eWeaponId Optional weapon id, indicates primary or secondary weapon
    * @return The default number of rounds in the indicated container.
    */
   int GetWeaponRoundsAvailablePerContainer(EWeaponId eWeaponId = WEAPON_ID_1);

   /**
    * This method sets the default number of rounds available in the magazine or belt.  
    * This allows the application to control the number of rounds available when the 
    * firer reloads the weapon.  This number of rounds will automatically be added 
    * to the current container when the weapon is reloaded.  This value should be
    * set before the firer loads the weapon.
    *
    * The M249 can have either belts or magazines.  In order to support this we've 
    * put the belt ammunition data in the primary weapon (WEAPON_ID_1) and the 
    * magazine information in the secondary weapon (WEAPON_ID_2).
    *
    * @param nQuantity The new default number of rounds in the indicated container.
    * @param eWeaponId Optional weapon id, indicates primary or secondary weapon
    */
   void SetWeaponRoundsAvailablePerContainer(int nQuantity, EWeaponId eWeaponId = WEAPON_ID_1);

   /**
    * This method returns the number of rounds left in the current container.  The
    * number of rounds are automatically decremented on a valid fire event.  This
    * allows the application to get the number of rounds the DLL thinks it has.
    * 
    * The M249 can have either belts or magazines.  When decrementing the rounds
    * for the M249 I don't know if it was a belt or a magazine.  As indicated
    * before we shouldn't have both belts and magazines.  So technically the only
    * one with rounds is the one to decrement.  The logic as it stands now will 
    * decrement both the belt and the magazine on a fire event, if they both have 
    * rounds available in the current container.
    *
    * @param eWeaponId Optional weapon id, indicates primary or secondary weapon
    * @return The default number of rounds in the indicated container.
    */
   int GetWeaponRoundsInCurrentContainer(EWeaponId eWeaponId = WEAPON_ID_1);

   /**
    * This method sets the number of rounds available in the current magazine or belt.  
    * This allows the application to control the number of rounds in the current 
    * container.  
    *
    * The M249 can have either belts or magazines.  In order to support this we've 
    * put the belt ammunition data in the primary weapon (WEAPON_ID_1) and the 
    * magazine information in the secondary weapon (WEAPON_ID_2).
    *
    * @param nQuantity The new number of rounds in the current container.
    * @param eWeaponId Optional weapon id, indicates primary or secondary weapon
    */
   void SetWeaponRoundsInCurrentContainer(int nQuantity, EWeaponId eWeaponId = WEAPON_ID_1);

   /**
    * This method returns the fire mode of the weapon.  It indicates if the weapon
    * is in single shot, burst, or automatic fire mode.  This is used by the application
    * to determine how many rounds to fire on a fire event.  For single shot one
    * round is fired, for burst three rounds are fired, and for automatic generally
    * a burst of 5 - 7 rounds is fired.
    *
    * @see CubicVBS2Interface::EWeaponMode
    * @return EWeaponMode enumeration of the current weapon mode.
    * @param eWeaponId Optional weapon id, indicates primary or secondary weapon
    */
	EWeaponMode GetWeaponMode(EWeaponId eWeaponId = WEAPON_ID_1);

   /**
    * This method returns the mount point for the weapon.  It indicates if the weapon
    * is attached to the right or left shoulder
    *
    * @see CubicVBS2Interface::SetWeaponMountPoint()
    * @see CubicVBS2Interface::EWeaponMountPoint
    * @return EWeaponMountPoint enumeration of the weapon mount point.
    */
   int GetWeaponMountPoint();

protected:

   // -----------------------------------------------------
   // Log controls ... 
   // -----------------------------------------------------

   /**
    * This method begins the debug comm logging procedure.  It opens the
    * file for binary write, clears the file, and writes the binary header 
    * information to the file.  This is protected so the application can't
    * start and stop this log.  The only control the application has is to
    * start logging in the InitializeInstance() function call.  This method
    * was created to allow the remote IOS stations to start and stop logging.
    *
    * @see CubicVBS2Interface::CloseDebugCommLog()
    * @param sFilename The new filename for the log file.
    */
   bool OpenDebugCommLog( const char *sFilename );

   /**
    * This method stops the debug comm logging procedure.  It closes the
    * current log file and stops logging.  This method was created to allow 
    * the remote IOS stations to start and stop logging.
    *
    * @see CubicVBS2Interface::CloseDebugCommLog()
    */
   bool CloseDebugCommLog();

   // -----------------------------------------------------
   // Joystick controls ... 
   // -----------------------------------------------------

   /**
    * This method sets the raw state of the joystick movement.  This
    * method takes the raw joystick values (range 0 to 1023 where 511 is
    * assumed to be centered) and converts them to the percentage value
    * between 1.0 and -1.0 that is returned by the GetJoystickMoveForwardBack()
    * and GetJoystickMoveRightLeft() methods.  This is intended for 
    * internal use only.
    *
    * @see CubicVBS2Interface::GetJoystickMoveForwardBack()
    * @see CubicVBS2Interface::GetJoystickMoveRightLeft()
    * @param nMoveForwardBack The raw forward/back joystick value.
    * @param nMoveRightLeft The raw right/left joystick value.
    */
   void SetJoystickMovement( int nMoveForwardBack, int nMoveRightLeft );

   /**
    * This method sets the state of the joystick button1.  Currently, the
    * only action this has is to initiate sensor calibration.  After the
    * one second long beep, 5 short beeps will indicate the sensors are
    * sending data.  At that point the firer has to stand with eyes forward
    * and the weapon held at a 45 degree angle (left hand high, right hand
    * low).  After the joystick button is pressed and calibration is 
    * complete another long tone is heard.  After that the button can
    * be used for something else but so far there isn't another use.
    *
    * @param bButton1 True if button1 is pressed, false otherwise.
    */
   void SetJoystickButton1( bool bButton1 );

   // -----------------------------------------------------
   // Sensor controls ...
   // -----------------------------------------------------

   /**
    * This method is a callback method used by the data readers to update
    * the entity "head" sensor values.  This is an internal function and 
    * not intended for application use.  Thus, it is protected.
    *
    * @param sSensor Struct containing the current value of the motion node sensor.
    */
   void SetEntityHeadSensor( SMotionNode sSensor );
   
   /**
    * This method is a callback method used by the data readers to update
    * the entity "body" sensor values.  This is an internal function and 
    * not intended for application use.  Thus, it is protected.
    *
    * @param sSensor Struct containing the current value of the motion node sensor.
    */
   void SetEntityBodySensor( SMotionNode sSensor );
   
   /**
    * This method is a callback method used by the data readers to update
    * the entity "Right Leg" sensor values.  This is an internal function and 
    * not intended for application use.  Thus, it is protected.
    *
    * @param sSensor Struct containing the current value of the motion node sensor.
    */
   void SetEntityRightLegSensor( SMotionNode sSensor );

   /**
    * This method is a callback method used by the data readers to update
    * the entity "Left Leg" sensor values.  This is an internal function and 
    * not intended for application use.  Thus, it is protected.
    *
    * @param sSensor Struct containing the current value of the motion node sensor.
    */
   void SetEntityLeftLegSensor( SMotionNode sSensor );

   /**
    * This method is a callback method used by the data readers to update
    * the weapon sensor values.  This is an internal function and 
    * not intended for application use.  Thus, it is protected.
    *
    * @param sSensor Struct containing the current value of the motion node sensor.
    */
   void SetWeaponSensor( SMotionNode sSensor );

   /**
    * This method is a called when the joystick button is pressed the first
    * time to calibrate the sensors.  
    */
   void CallibrateSensors();

   // -----------------------------------------------------
   // Weapon controls ...
   // -----------------------------------------------------

   /**
    * This method is a callback method used by the data readers to update
    * the weapon state.  This is an internal update method and is not 
    * intended for application use.
    *
    * @param sWeaponState Struct containing the new state of the weapon.
    */
   void SetWeaponState( SWeaponState sWeaponState );

   /**
    * This method is a callback method used by the SetWeaponState() to update
    * the entity container state.  This method not only updates the state
    * of the container, but when a container is removed and reloaded this 
    * method also updates the reloaded flag for the given weapon.  This is
    * an internal update method and is not intended for application use.
    *
    * @param eState Enumeration containing the new state of the weapon.
    * @param eWeaponId Optional weapon id, indicates primary or secondary weapon
    */
   void SetWeaponAmmoContainerState(EWeaponAmmoContainerState eState, EWeaponId eWeaponId = WEAPON_ID_1);

   /**
    * This method sets the mount point for the weapon.  It indicates if the weapon
    * is attached to the right or left shoulder
    *
    * @see CubicVBS2Interface::GetWeaponMountPoint()
    * @see CubicVBS2Interface::EWeaponMountPoint
    * @param nWeaponMountPoint The new weapon mount point.
    */
   void SetWeaponMountPoint( int nWeaponMountPoint );

private:

   bool Lock( DWORD ulTimeout = INFINITE );
   bool Unlock();

   HANDLE m_hLock;

   int m_nNumberSensorsReady;

   bool m_bCalibrationComplete;

   SEntityLocation m_sEntityLocation;
   SEntityLocation m_sEntityStartLocation;
   SEntityLocation m_sWeaponLocation;

   SVelocity m_sJoystickVelocity;

   SMotionNode m_sEntityHeadSensor;
   SMotionNode m_sEntityBodySensor;
   SMotionNode m_sWeaponSensor;
   SMotionNode m_sEntityRightLegSensor;
   SMotionNode m_sEntityLeftLegSensor;

   int m_nWeaponMountPoint;

   double m_dEntityUpdateTime;
   double m_dHeadSensorUpdateTime;
   double m_dBodySensorUpdateTime;
   double m_dWeaponUpdateTime;

   SWeaponState m_sWeaponState;
   bool m_bJoystickButton1;
   float m_fJoystickForwardBack;
   float m_fJoystickRightLeft;

   float m_fMaxVelocityForwardBack;
   float m_fMaxVelocityRightLeft;
   double m_dStartTime;
};

