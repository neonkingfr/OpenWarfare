#define DYNAMICSPLUGINSAMPLE_EXPORT __declspec(dllexport)

DYNAMICSPLUGINSAMPLE_EXPORT void WINAPI RegisterHWND(void *hwnd, void *hins);
DYNAMICSPLUGINSAMPLE_EXPORT void WINAPI RegisterCommandFnc(void *executeCommandFnc);
DYNAMICSPLUGINSAMPLE_EXPORT void WINAPI OnSimulationStep(float deltaT);
DYNAMICSPLUGINSAMPLE_EXPORT const char* WINAPI PluginFunction(const char *input);