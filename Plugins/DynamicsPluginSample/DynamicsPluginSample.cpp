// D-BOX Sample plugin illustrating use of new script commands for D-BOX company

#include <windows.h>
#include <stdio.h>
#include <math.h>
#include "DynamicsPluginSample.h"

// Command function declaration
typedef int (WINAPI * ExecuteCommandType)(const char *command, char *result, int resultLength);

// Command function definition
ExecuteCommandType ExecuteCommand = NULL;

// Function that will register the ExecuteCommand function of the engine
DYNAMICSPLUGINSAMPLE_EXPORT void WINAPI RegisterHWND(void *hwnd, void *hins)
{
}

// Function that will register the ExecuteCommand function of the engine
DYNAMICSPLUGINSAMPLE_EXPORT void WINAPI RegisterCommandFnc(void *executeCommandFnc)
{
  ExecuteCommand = (ExecuteCommandType)executeCommandFnc;
}

struct Vector3
{
  float _x;
  float _y;
  float _z;

  Vector3():_x(0),_y(0),_z(0){}
  Vector3(float x, float y, float z ):_x(x),_y(y),_z(z){}

  float X() const{return _x;}
  float Y() const{return _y;}
  float Z() const{return _z;}

  float Size() const{return sqrt(X()*X()+Y()*Y()+Z()*Z());}

  float SizeXZ() const{return sqrt(X()*X()+Z()*Z());}

  Vector3 CrossProduct( Vector3 op ) const
  {
    float x=Y()*op.Z()-Z()*op.Y();
    float y=Z()*op.X()-X()*op.Z();
    float z=X()*op.Y()-Y()*op.X();
    return Vector3(x,y,z);
  }

  void Rotate( const Vector3 aside, const Vector3 up, const Vector3 dir, Vector3 o )
  { // vector rotation - 3x3 matrix (aside,up,dir) is used
    // u=M*v
    float o0=o.X(),o1=o.Y(),o2=o.Z();
    _x=aside.X()*o0+aside.Y()*o1+aside.Z()*o2;
    _y=up.X()*o0+up.Y()*o1+up.Z()*o2;
    _z=dir.X()*o0+dir.Y()*o1+dir.Z()*o2;
  }

  bool operator == ( Vector3 cmp ) const {return (cmp.X()==X() && cmp.Y()==Y() && cmp.Z()==Z());}
  bool operator != ( Vector3 cmp ) const {return (cmp.X()!=X() || cmp.Y()!=Y() || cmp.Z()!=Z());}

};

#define VZero Vector3(0,0,0)

////////////////// Script command helpers ////////////////////////////////////////////////////////////////////////

char buf[1024];
char *Format(const char *format, ...)
{
  va_list va;
  va_start(va,format);
  vsnprintf_s(buf,1024,sizeof(buf)-1,format,va);
  buf[sizeof(buf)-1]=0;
  va_end(va);
  return buf;
}

// in - array[handle,time] & text , out - handle
void ExecuteDiagMessage(int &handle, int time, char* text)
{
  if(!text) {handle = -1; return;}

  // fill command line
  char commandBuff[512];
  sprintf_s(commandBuff,512,"[%d,%d] diagMessage \"%s\"",handle,time,text);

  char resultBuff[512];

  // send command
  if(!ExecuteCommand(commandBuff, resultBuff, 512))
  {//failed
    OutputDebugString(resultBuff);
    handle = -1; 
    return;
  }

  //parse to handle
  handle = atoi(resultBuff);
  return;
}

// use Format function to preformat if necessary
#define DIAG_MESSAGE(time,text) do \
{	\
  static int handle=-1; \
  ExecuteDiagMessage(handle, time, text); \
} while (false);


// in - nothing, out - bool (0,1, -1=err )
bool ExecuteScriptCommand(bool &ret, char* command)
{
  if(!command) return false;

  // send command
  char resultBuff[256];
  if(!ExecuteCommand(command, resultBuff, 256))
  {//failed
    OutputDebugString(resultBuff);
    return false;
  }

  //parse to bool
  if(resultBuff[0])
  {
    if(!strcmp("true",resultBuff)) {ret = true; return true;}
    else if(!strcmp("false",resultBuff)) {ret = false; return true;}
  }

  return false;
}

// in - string, out - string
bool ExecuteScriptCommand(char *ret, char* command, char* text)
{
  if(!ret || !command || !text) return false;

  // fill command line
  char commandBuff[256];
  sprintf_s(commandBuff,256,"%s %s",command,text);

  // send command
  char resultBuff[256];
  if(!ExecuteCommand(commandBuff, resultBuff, 256))
  {//failed
    OutputDebugString(resultBuff);
    return false;
  }

  // parse to string
  int len = strlen(resultBuff);
  if(len > 0 && resultBuff[0] == '"' && resultBuff[len-1] == '"')// is in apostrophes
  {
    resultBuff[len-1] = 0;

    //copy to ret string without apostrophes
    strcpy_s(ret,256,&resultBuff[1]);
  }
  else
  {
    //copy to ret string
    strcpy_s(ret,256,resultBuff);
  }

  return true;
}

// in - string, out - float
bool ExecuteScriptCommand(float &ret, char* command, char* text)
{
  if(!command || !text) return false;

  // fill command line
  char commandBuff[256];
  sprintf_s(commandBuff,256,"%s %s",command,text);
 
  // send command
  char resultBuff[256];
  if(!ExecuteCommand(commandBuff, resultBuff, 256))
  {//failed
    OutputDebugString(resultBuff);
    return false;
  }

  //parse to float
  ret = (float)atof(resultBuff);

  return true;
}

// in - string, out - int
bool ExecuteScriptCommand(int &ret, char* command, char* text)
{
  if(!command || !text) return false;

  // fill command line
  char commandBuff[256];
  sprintf_s(commandBuff,256,"%s %s",command,text);

  // send command
  char resultBuff[256];
  if(!ExecuteCommand(commandBuff, resultBuff, 256))
  {//failed
    OutputDebugString(resultBuff);
    return false;
  }

  //parse to float
  ret = (int)atoi(resultBuff);

  return true;
}

// in - string, out - nothing
bool ExecuteScriptCommand(char* command)
{
  if(!command) return false;

  // send command
  char resultBuff[256];
  if(!ExecuteCommand(command, resultBuff, 256))
  {//failed
    OutputDebugString(resultBuff);
    return false;
  }

  return true;
}

// in - string, out - vector[x,z,y]
bool ExecuteScriptCommand(Vector3 &ret, char* command, char* text)
{
  if(!command || !text) return false;

  // fill command line
  char commandBuff[256];
  sprintf_s(commandBuff,256,"%s %s",command,text);

  // send command
  char resultBuff[256];
  if(!ExecuteCommand(commandBuff, resultBuff, 256))
  {//failed
    OutputDebugString(resultBuff);
    return false;
  }

  //parse to vector
  if(resultBuff[0] && resultBuff[0] == '[')
  {
    if(sscanf_s( resultBuff, "[%f,%f,%f]", &ret._x, &ret._z, &ret._y) != 3) return false;
  }

  return true;
}

// in - vector[x,z,y], out - float
bool ExecuteScriptCommand(float &ret, char* command, Vector3 vect)
{
  if(!command) return false;

  // fill command line
  char commandBuff[256];
  sprintf_s(commandBuff,256,"%s [%f,%f,%f]",command,vect.X(),vect.Z(),vect.Y());

  // send command
  char resultBuff[256];
  if(!ExecuteCommand(commandBuff, resultBuff, 256))
  {//failed
    OutputDebugString(resultBuff);
    return false;
  }

  //parse to float
  ret = (float)atof(resultBuff);

  return true;
}

// in - string, out - 2xvector [[x,z,y],[x,z,y]]
bool ExecuteScriptCommand(Vector3 &ret1, Vector3 &ret2, char* command, char *text)
{
  if(!command || !text) return false;

  // fill command line
  char commandBuff[256];
  sprintf_s(commandBuff,256,"%s %s",command,text);

  // send command
  char resultBuff[256];
  if(!ExecuteCommand(commandBuff, resultBuff, 256))
  {//failed
    OutputDebugString(resultBuff);
    return false;
  }

  //parse to 2xvector
  if(resultBuff[0] && resultBuff[0] == '[')
  {
    if(sscanf_s( resultBuff, "[[%f,%f,%f],[%f,%f,%f]]", &ret1._x, &ret1._z, &ret1._y, &ret2._x, &ret2._z, &ret2._y) != 6) return false;
  }

  return true;
}

// in - string & vector[x,z,y], out - vector [x,z,y]
bool ExecuteScriptCommand(Vector3 &ret, char* command, char *text, Vector3 vect)
{
  if(!command || !text) return false;

  // fill command line
  char commandBuff[256];
  sprintf_s(commandBuff,256,"%s %s [%f,%f,%f]",text,command,vect.X(),vect.Z(),vect.Y());

  // send command
  char resultBuff[256];
  if(!ExecuteCommand(commandBuff, resultBuff, 256))
  {//failed
    OutputDebugString(resultBuff);
    return false;
  }

  //parse to vector
  if(resultBuff[0] && resultBuff[0] == '[')
  {
    if(sscanf_s( resultBuff, "[%f,%f,%f]", &ret._x, &ret._z, &ret._y) != 3) return false;
  }

  return true;
}

////////////////// Script command helpers ////////////////////////////////////////////////////////////////////////

////////////////// Parsing Helpers ///////////////////////////////////////////////////////////////////////////////

// parses a parameter from an array and returns pointer to the next parameter
char *GetNextParamFromArray(char* output, int size, char* input)
{
  if(!output || !input || !input[0] || !size) return NULL;

  if(*input == '[' || *input == ',')input++; //skip these two characters if they are at the start of the string
  if(*input == '"')input++; //skip comma for strings (all parameters will be returned as strings)

  char *e = input;
  while(*e != ',' && *e != ']' && *e != '"' && *e != 0) e++; //find end of the parameter or array or string

  if(!e) return NULL; // array must end with ']' not 0

  *e=0; // s="param"
  e++;
  if(*e == ',' && *e == ']') e++; //skip these two characters at the end of the string (we need it if the parameter is in comma)

  strncpy_s(output,256,input,size);
  return e; // return pointer to next parameter
}

// parses a word from a sentence and returns pointer to the next word (words can be separated by " " or "_")
char* GetNextWord(char* output, int size, char* input)
{
  if(!output || !input || !input[0] || !size) return NULL;

  if(*input == ' ' || *input == '_')input++; //skip these two characters if they are at the start of the string

  char *e = input;
  while(*e != ' ' && *e != '_' && *e != 0) e++; //find end of the word or string
  *e=0; // mark end of the word as an end of the string
  
  strncpy_s(output,256,input,size);
  return e+1; // return pointer to next parameter
}

////////////////// Parsing Helpers ///////////////////////////////////////////////////////////////////////////////

int ehindexFired = -1; // "Fired" entity event handler index
int ehindexExplosions = -1; // "Explosions" entity event handler index

enum EntityEvent
{
  EE_Default,
  EE_Fired,
  EE_Explosions
};

void ProcessFireEvent(const char *unit, const char *weapon, const char *muzzle, const char *mode, const char *ammo, const char *magazine, const char *projectile)
{
  // Get ammo simulation type
  char ammoTypeStr[64];
  {
    //possible ammo simulation types:
    //shotBullet
    //shotShell
    //shotMissile
    //shotRocket
    //shotIlluminating
    //shotSmoke
    //shotTimeBomb
    //shotPipeBomb
    //shotMine
    //shotStroke
    //laserDesignate

    char command[256];
    sprintf_s(command,256,"(configFile >> \"cfgAmmo\" >> \"%s\" >> \"simulation\")",ammo);
    ExecuteScriptCommand(ammoTypeStr, "getText" , command);
  }

  //If you need to differentiate hand grenades, you can check if weapon is "Throw"
  if(!strncmp("shotShell",ammoTypeStr,64) && !strncmp("Throw",weapon,64)) strncpy_s(ammoTypeStr,64,"shotGrenade",64);

  DIAG_MESSAGE(1000, Format("Fired: type = %s", ammoTypeStr));
  DIAG_MESSAGE(1000, Format("Fired: unit = %s", unit));
  DIAG_MESSAGE(1000, Format("Fired: weapon = %s", weapon));
  DIAG_MESSAGE(1000, Format("Fired: muzzle = %s", muzzle));
  DIAG_MESSAGE(1000, Format("Fired: mode = %s", mode));
  DIAG_MESSAGE(1000, Format("Fired: ammo = %s", ammo));
  DIAG_MESSAGE(1000, Format("Fired: magazine = %s", magazine));
  DIAG_MESSAGE(1000, Format("Fired: projectile = %s", projectile));
}

void ProcessExplosionsEvent(Vector3 position, float indirectHit, float indirectHitRange, float energyFactor)
{
  //Distance to the player
  float dist = 1e10f;
  Vector3 mDistVect; // distance vector in absolute model coordinates (player)
  {
    ExecuteScriptCommand(mDistVect,"worldToModelASL2", "player", position);
    dist = mDistVect.Size();
  }
  
  //intensity
  float intensity = 0.0f;
  {
    //hit = indirectHit*energyFactor; // strenght of the explosion
    //damage drops with the square of distance (max distance for damage is indirectHitRange*4)
    // It's up to you if you want to drop intensity with square of distance or linearly and if you want to have it dependent on explosion strength or only on distance.

    //simple example
    float maxDist = indirectHitRange*10; // you can select max distance dependent on indirectHitRange or static
    if(maxDist>0 && dist<maxDist)
    {
      intensity = 1.0f - (dist/maxDist);
      
    }
  }

  DIAG_MESSAGE(100000, Format("Explosions: position = [%f,%f,%f]", position.X(),position.Y(),position.Z()));
  DIAG_MESSAGE(100000, Format("Explosions: indirectHit = %f", indirectHit));
  DIAG_MESSAGE(100000, Format("Explosions: indirectHitRange = %f", indirectHitRange));
  DIAG_MESSAGE(100000, Format("Explosions: energyFactor = %f", energyFactor));

  DIAG_MESSAGE(100000, Format("Explosions: mDirection = [%f,%f,%f]", mDistVect.X(),mDistVect.Y(),mDistVect.Z()));
  DIAG_MESSAGE(100000, Format("Explosions: distance = %f", dist));
  DIAG_MESSAGE(100000, Format("Explosions: intensity = %f", intensity));
}

// This function will be executed every time the script in the engine calls the script function "pluginFunction"
// We can be sure in this function the ExecuteCommand registering was already done.
// Note that the plugin takes responsibility for allocating and deleting the returned string
DYNAMICSPLUGINSAMPLE_EXPORT const char* WINAPI PluginFunction(const char *input)
{
  //Syntax: [event][params]
  char buff[512];
  strncpy_s(buff,512,input,512);

  char *s = buff; // string pointer

  //get event from input string, [event] - EntityEvent type
  EntityEvent event = EE_Default;
  {
    char eventString[32];
    if(*s != '[')
    {
      OutputDebugString("PluginFunction: Error in string!\n");
      return NULL;
    }

    s = GetNextParamFromArray(eventString,32,s);
    if(!strcmp("Fired",eventString)) event = EE_Fired;
    if(!strcmp("Explosions",eventString)) event = EE_Explosions;
  }

  switch(event)
  {
  case EE_Fired:
    {
      //"Fired" params: [unit, weapon, muzzle, mode, ammo, magazine, projectile]
      char unit[64]; //Object the event handler is assigned to
      char weapon[64]; //Fired weapon
      char muzzle[64]; //Muzzle which was used
      char mode[64]; //Current mode of the fired weapon
      char ammo[64]; //Ammo used
      char magazine[64]; //Magazine which was used
      char projectile[64]; //Object of the projectile that was shot

      s = GetNextParamFromArray(unit,64,s);
      s = GetNextParamFromArray(weapon,64,s);
      s = GetNextParamFromArray(muzzle,64,s);
      s = GetNextParamFromArray(mode,64,s);
      s = GetNextParamFromArray(ammo,64,s);
      s = GetNextParamFromArray(magazine,64,s);
      s = GetNextParamFromArray(projectile,64,s);

      ProcessFireEvent(unit, weapon, muzzle, mode, ammo, magazine, projectile);
    }
    break;
  case EE_Explosions:
    {
      //"Explosions" params: [posX,posZ,posY,indirectHit,indirectHitRange,energyFactor]
      Vector3 position;
      float indirectHit;
      float indirectHitRange;
      float energyFactor;

      char exBuff[64];

      s = GetNextParamFromArray(exBuff,64,s); //posX
      position._x = (float)atof(exBuff);
      s = GetNextParamFromArray(exBuff,64,s); //posZ
      position._z = (float)atof(exBuff);
      s = GetNextParamFromArray(exBuff,64,s); //posY
      position._y = (float)atof(exBuff);
      s = GetNextParamFromArray(exBuff,64,s); //indirectHit
      indirectHit = (float)atof(exBuff);
      s = GetNextParamFromArray(exBuff,64,s); //indirectHitRange
      indirectHitRange = (float)atof(exBuff);
      s = GetNextParamFromArray(exBuff,64,s); //energyFactor
      energyFactor = (float)atof(exBuff);

      ProcessExplosionsEvent( position, indirectHit, indirectHitRange, energyFactor);
    }
    break;
  default:
    {
      OutputDebugString("PluginFunction: Unsupported event!\n");
    }
    break;
  }

  return NULL; // we don't want to return anything
}

// This function will be executed every simulation step (every frame) and took a part in the simulation procedure.
// We can be sure in this function the ExecuteCommand registering was already done.
// deltaT is time in seconds since the last simulation step
DYNAMICSPLUGINSAMPLE_EXPORT void WINAPI OnSimulationStep(float deltaT)
{
  // check if simulation is running
  bool simRunning = false;
  if(!ExecuteScriptCommand(simRunning,"isSimulationEnabled")) return;

  // "Fired" entity event
  {
    bool changed;
    ExecuteScriptCommand("dVehicle = vehicle player");
    bool err = !ExecuteScriptCommand(changed,"dVehicle != dLastVehicle");
    if(changed || err)// status changed! (or dLastVehicle is not initialized)
    {
      // remove event handler when we change a vehicle
      {
        char ret[256];
        char removeEventCmd[128];
        sprintf_s(removeEventCmd,128,"removeEventHandler [\"Fired\",%d]",ehindexFired);
        if(ehindexFired != -1) ExecuteScriptCommand(ret, "dLastVehicle", removeEventCmd);
      }

      // store information about our last vehicle
      {
        ExecuteScriptCommand("dLastVehicle = dVehicle");
        DIAG_MESSAGE(1000, "Vehicle Changed");
      }

      // register new event handler for our new vehicle
      {
        ExecuteScriptCommand(ehindexFired, "dVehicle", "addEventHandler [\"Fired\",{pluginFunction [\"DynamicsPluginSample\",\"[Fired]\" + str _this]}]");
      }
    }
  }

  // "Explosions" entity event (will work only for player entity)
  {
    bool changed;
    ExecuteScriptCommand("dPlayer = player");
    bool err = !ExecuteScriptCommand(changed,"dPlayer != dLastPlayer");
    if(changed || err)// status changed! (or dLastPlayer is not initialized)
    {
      // remove event handler when we change a player entity
      {
        char ret[256];
        char removeEventCmd[128];
        sprintf_s(removeEventCmd,128,"removeEventHandler [\"Explosions\",%d]",ehindexExplosions);
        if(ehindexExplosions != -1) ExecuteScriptCommand(ret, "dLastPlayer", removeEventCmd);
      }

      // store information about last player entity
      {
        ExecuteScriptCommand("dLastPlayer = dPlayer");
        DIAG_MESSAGE(1000, "Player Changed");
      }

      // register new event handler for new player entity
      {
        ExecuteScriptCommand(ehindexExplosions, "dPlayer", "addEventHandler [\"Explosions\",{pluginFunction [\"DynamicsPluginSample\",\"[Explosions]\" + str _this]}]");
      }
    }
  }

  //Angular velocity
  Vector3 wAngVelocity = VZero;
  {
    ExecuteScriptCommand(wAngVelocity,"velocityAng", "vehicle player"); 
    DIAG_MESSAGE(1000, Format("speedAng = [%f,%f,%f]", wAngVelocity.X(),wAngVelocity.Y(),wAngVelocity.Z()));
  }

  //Acceleration
  Vector3 wAcceleration = VZero;
  {
    ExecuteScriptCommand(wAcceleration,"acceleration", "vehicle player"); 
    DIAG_MESSAGE(1000, Format("acceleration = [%f,%f,%f]", wAcceleration.X(),wAcceleration.Y(),wAcceleration.Z()));
  }

  //Surface Roughness
  float roughness = 0;
  {
    ExecuteScriptCommand(roughness,"getSurfRoughness", "getPos player"); 
    DIAG_MESSAGE(1000, Format("SurfRoughness = %f", roughness));
  }
}

void Init()
{

}

void DeInit()
{
}

// DllMain
BOOL WINAPI DllMain(HINSTANCE hDll, DWORD fdwReason, LPVOID lpvReserved)
{
   switch(fdwReason)
   {
      case DLL_PROCESS_ATTACH:
         Init();
         OutputDebugString("Called DllMain with DLL_PROCESS_ATTACH\n");
         break;
      case DLL_PROCESS_DETACH:
         DeInit();
         OutputDebugString("Called DllMain with DLL_PROCESS_DETACH\n");
         break;
      case DLL_THREAD_ATTACH:
         OutputDebugString("Called DllMain with DLL_THREAD_ATTACH\n");
         break;
      case DLL_THREAD_DETACH:
         OutputDebugString("Called DllMain with DLL_THREAD_DETACH\n");
         break;
   }
   return TRUE;
}
