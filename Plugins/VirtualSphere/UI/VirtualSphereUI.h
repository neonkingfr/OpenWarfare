///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version Jun 30 2011)
// http://www.wxformbuilder.org/
//
// PLEASE DO "NOT" EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#ifndef __VIRTUALSPHEREUI_H__
#define __VIRTUALSPHEREUI_H__

#include <wx/artprov.h>
#include <wx/xrc/xmlres.h>
#include <wx/gdicmn.h>
#include <wx/notebook.h>
#include <wx/string.h>
#include <wx/font.h>
#include <wx/colour.h>
#include <wx/settings.h>
#include <wx/sizer.h>
#include <wx/panel.h>
#include <wx/frame.h>
#include <wx/stattext.h>
#include <wx/slider.h>
#include <wx/statbox.h>
#include <wx/textctrl.h>
#include <wx/radiobut.h>
#include <wx/checkbox.h>

///////////////////////////////////////////////////////////////////////////


///////////////////////////////////////////////////////////////////////////////
/// Class VirtualSphereBase
///////////////////////////////////////////////////////////////////////////////
class VirtualSphereBase : public wxFrame 
{
	private:
	
	protected:
		wxPanel* m_panel1;
	
	public:
		wxNotebook* GNoteBook;
		
		VirtualSphereBase( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxT("Virtual - VirtualSphere"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 699,478 ), long style = wxDEFAULT_FRAME_STYLE|wxTAB_TRAVERSAL );
		
		~VirtualSphereBase();
	
};

///////////////////////////////////////////////////////////////////////////////
/// Class SensorPanelBase
///////////////////////////////////////////////////////////////////////////////
class SensorPanelBase : public wxPanel 
{
	private:
	
	protected:
		wxStaticText* m_staticText2;
		wxStaticText* m_staticText21;
		wxStaticText* m_staticText211;
		wxStaticText* m_staticText23;
		wxStaticText* m_staticText213;
		wxStaticText* m_staticText2112;
		wxStaticText* m_staticText22;
		wxStaticText* m_staticText212;
		wxStaticText* m_staticText2111;
		wxStaticText* m_staticText231;
	
	public:
		wxSlider* HeadingSlider;
		wxSlider* PitchSlider;
		wxSlider* RollSlider;
		wxSlider* XPos;
		wxSlider* YPos;
		wxSlider* ZPos;
		wxTextCtrl* OffsetXTextBox;
		wxTextCtrl* OffsetYTextBox;
		wxTextCtrl* OffsetZTextBox;
		wxTextCtrl* BoneID;
		wxRadioButton* RelativeAbsolute;
		wxRadioButton* RadioRelative;
		wxCheckBox* UseBoneSystemTickBox;
		
		SensorPanelBase( wxWindow* parent, wxWindowID id = wxID_ANY, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 655,332 ), long style = wxTAB_TRAVERSAL ); 
		~SensorPanelBase();
	
};

///////////////////////////////////////////////////////////////////////////////
/// Class WalkingPadBase
///////////////////////////////////////////////////////////////////////////////
class WalkingPadBase : public wxPanel 
{
	private:
	
	protected:
	
	public:
		wxSlider* ZSlider;
		wxSlider* XSlider;
		
		WalkingPadBase( wxWindow* parent, wxWindowID id = wxID_ANY, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 500,300 ), long style = wxTAB_TRAVERSAL ); 
		~WalkingPadBase();
	
};

#endif //__VIRTUALSPHEREUI_H__
