///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version Jun 30 2011)
// http://www.wxformbuilder.org/
//
// PLEASE DO "NOT" EDIT THIS FILE!
///////////////////////////////////////////////////////////////////////////

#include "VirtualSphereUI.h"

///////////////////////////////////////////////////////////////////////////

VirtualSphereBase::VirtualSphereBase( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxFrame( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxDefaultSize, wxDefaultSize );
	
	wxBoxSizer* bSizer1;
	bSizer1 = new wxBoxSizer( wxVERTICAL );
	
	m_panel1 = new wxPanel( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* bSizer35;
	bSizer35 = new wxBoxSizer( wxVERTICAL );
	
	GNoteBook = new wxNotebook( m_panel1, wxID_ANY, wxDefaultPosition, wxDefaultSize, 0 );
	
	bSizer35->Add( GNoteBook, 1, wxEXPAND | wxALL, 5 );
	
	m_panel1->SetSizer( bSizer35 );
	m_panel1->Layout();
	bSizer35->Fit( m_panel1 );
	bSizer1->Add( m_panel1, 1, wxEXPAND, 5 );
	
	this->SetSizer( bSizer1 );
	this->Layout();
	
	this->Centre( wxBOTH );
}

VirtualSphereBase::~VirtualSphereBase()
{
}

SensorPanelBase::SensorPanelBase( wxWindow* parent, wxWindowID id, const wxPoint& pos, const wxSize& size, long style ) : wxPanel( parent, id, pos, size, style )
{
	wxBoxSizer* bSizer5;
	bSizer5 = new wxBoxSizer( wxVERTICAL );
	
	wxStaticBoxSizer* sbSizer18;
	sbSizer18 = new wxStaticBoxSizer( new wxStaticBox( this, wxID_ANY, wxT("Virtual Sensor") ), wxVERTICAL );
	
	wxBoxSizer* bSizer6;
	bSizer6 = new wxBoxSizer( wxHORIZONTAL );
	
	wxBoxSizer* bSizer7;
	bSizer7 = new wxBoxSizer( wxVERTICAL );
	
	m_staticText2 = new wxStaticText( this, wxID_ANY, wxT("Heading"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText2->Wrap( -1 );
	bSizer7->Add( m_staticText2, 0, wxALL|wxALIGN_CENTER_HORIZONTAL, 5 );
	
	HeadingSlider = new wxSlider( this, wxID_ANY, 0, -180, 180, wxDefaultPosition, wxDefaultSize, wxSL_HORIZONTAL|wxSL_LABELS );
	bSizer7->Add( HeadingSlider, 0, wxALL|wxEXPAND, 5 );
	
	bSizer6->Add( bSizer7, 1, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer8;
	bSizer8 = new wxBoxSizer( wxVERTICAL );
	
	m_staticText21 = new wxStaticText( this, wxID_ANY, wxT("Pitch"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText21->Wrap( -1 );
	bSizer8->Add( m_staticText21, 0, wxALL|wxALIGN_CENTER_HORIZONTAL, 5 );
	
	PitchSlider = new wxSlider( this, wxID_ANY, 0, -180, 180, wxDefaultPosition, wxDefaultSize, wxSL_HORIZONTAL|wxSL_LABELS );
	bSizer8->Add( PitchSlider, 0, wxALL|wxEXPAND, 5 );
	
	bSizer6->Add( bSizer8, 1, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer9;
	bSizer9 = new wxBoxSizer( wxVERTICAL );
	
	m_staticText211 = new wxStaticText( this, wxID_ANY, wxT("Roll"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText211->Wrap( -1 );
	bSizer9->Add( m_staticText211, 0, wxALL|wxALIGN_CENTER_HORIZONTAL, 5 );
	
	RollSlider = new wxSlider( this, wxID_ANY, 0, -180, 180, wxDefaultPosition, wxDefaultSize, wxSL_HORIZONTAL|wxSL_LABELS );
	bSizer9->Add( RollSlider, 0, wxALL|wxEXPAND, 5 );
	
	bSizer6->Add( bSizer9, 1, wxEXPAND, 5 );
	
	sbSizer18->Add( bSizer6, 1, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer62;
	bSizer62 = new wxBoxSizer( wxHORIZONTAL );
	
	wxBoxSizer* bSizer72;
	bSizer72 = new wxBoxSizer( wxVERTICAL );
	
	m_staticText23 = new wxStaticText( this, wxID_ANY, wxT("X"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText23->Wrap( -1 );
	bSizer72->Add( m_staticText23, 0, wxALL|wxALIGN_CENTER_HORIZONTAL, 5 );
	
	XPos = new wxSlider( this, wxID_ANY, 0, -10, 10, wxDefaultPosition, wxDefaultSize, wxSL_HORIZONTAL|wxSL_LABELS );
	bSizer72->Add( XPos, 0, wxALL|wxEXPAND, 5 );
	
	bSizer62->Add( bSizer72, 1, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer82;
	bSizer82 = new wxBoxSizer( wxVERTICAL );
	
	m_staticText213 = new wxStaticText( this, wxID_ANY, wxT("Y"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText213->Wrap( -1 );
	bSizer82->Add( m_staticText213, 0, wxALL|wxALIGN_CENTER_HORIZONTAL, 5 );
	
	YPos = new wxSlider( this, wxID_ANY, 0, -10, 10, wxDefaultPosition, wxDefaultSize, wxSL_HORIZONTAL|wxSL_LABELS );
	bSizer82->Add( YPos, 0, wxALL|wxEXPAND, 5 );
	
	bSizer62->Add( bSizer82, 1, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer92;
	bSizer92 = new wxBoxSizer( wxVERTICAL );
	
	m_staticText2112 = new wxStaticText( this, wxID_ANY, wxT("Z"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText2112->Wrap( -1 );
	bSizer92->Add( m_staticText2112, 0, wxALL|wxALIGN_CENTER_HORIZONTAL, 5 );
	
	ZPos = new wxSlider( this, wxID_ANY, 0, -10, 10, wxDefaultPosition, wxDefaultSize, wxSL_HORIZONTAL|wxSL_LABELS );
	bSizer92->Add( ZPos, 0, wxALL|wxEXPAND, 5 );
	
	bSizer62->Add( bSizer92, 1, wxEXPAND, 5 );
	
	sbSizer18->Add( bSizer62, 1, wxEXPAND, 5 );
	
	bSizer5->Add( sbSizer18, 1, wxEXPAND, 5 );
	
	wxStaticBoxSizer* sbSizer1;
	sbSizer1 = new wxStaticBoxSizer( new wxStaticBox( this, wxID_ANY, wxT("Options") ), wxHORIZONTAL );
	
	wxBoxSizer* bSizer71;
	bSizer71 = new wxBoxSizer( wxVERTICAL );
	
	m_staticText22 = new wxStaticText( this, wxID_ANY, wxT("Offset X"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText22->Wrap( -1 );
	bSizer71->Add( m_staticText22, 0, wxALL, 5 );
	
	OffsetXTextBox = new wxTextCtrl( this, wxID_ANY, wxT("0.0"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer71->Add( OffsetXTextBox, 0, wxALL, 5 );
	
	sbSizer1->Add( bSizer71, 1, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer81;
	bSizer81 = new wxBoxSizer( wxVERTICAL );
	
	m_staticText212 = new wxStaticText( this, wxID_ANY, wxT("Offset Y"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText212->Wrap( -1 );
	bSizer81->Add( m_staticText212, 0, wxALL, 5 );
	
	OffsetYTextBox = new wxTextCtrl( this, wxID_ANY, wxT("0.0"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer81->Add( OffsetYTextBox, 0, wxALL, 5 );
	
	sbSizer1->Add( bSizer81, 1, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer91;
	bSizer91 = new wxBoxSizer( wxVERTICAL );
	
	m_staticText2111 = new wxStaticText( this, wxID_ANY, wxT("Offset Z"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText2111->Wrap( -1 );
	bSizer91->Add( m_staticText2111, 0, wxALL, 5 );
	
	OffsetZTextBox = new wxTextCtrl( this, wxID_ANY, wxT("0.0"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer91->Add( OffsetZTextBox, 0, wxALL, 5 );
	
	sbSizer1->Add( bSizer91, 1, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer28;
	bSizer28 = new wxBoxSizer( wxVERTICAL );
	
	m_staticText231 = new wxStaticText( this, wxID_ANY, wxT("Bone ID"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText231->Wrap( -1 );
	bSizer28->Add( m_staticText231, 0, wxALL, 5 );
	
	BoneID = new wxTextCtrl( this, wxID_ANY, wxT("0"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer28->Add( BoneID, 0, wxALL, 5 );
	
	sbSizer1->Add( bSizer28, 1, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer281;
	bSizer281 = new wxBoxSizer( wxVERTICAL );
	
	RelativeAbsolute = new wxRadioButton( this, wxID_ANY, wxT("Absolute"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer281->Add( RelativeAbsolute, 0, wxALL, 5 );
	
	RadioRelative = new wxRadioButton( this, wxID_ANY, wxT("Relative"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer281->Add( RadioRelative, 0, wxALL, 5 );
	
	UseBoneSystemTickBox = new wxCheckBox( this, wxID_ANY, wxT("Use Bone System"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer281->Add( UseBoneSystemTickBox, 0, wxALL, 5 );
	
	sbSizer1->Add( bSizer281, 1, wxEXPAND, 5 );
	
	bSizer5->Add( sbSizer1, 0, wxEXPAND, 5 );
	
	this->SetSizer( bSizer5 );
	this->Layout();
}

SensorPanelBase::~SensorPanelBase()
{
}

WalkingPadBase::WalkingPadBase( wxWindow* parent, wxWindowID id, const wxPoint& pos, const wxSize& size, long style ) : wxPanel( parent, id, pos, size, style )
{
	wxBoxSizer* bSizer17;
	bSizer17 = new wxBoxSizer( wxVERTICAL );
	
	ZSlider = new wxSlider( this, wxID_ANY, 0, 0, 5, wxDefaultPosition, wxDefaultSize, wxSL_AUTOTICKS|wxSL_INVERSE|wxSL_VERTICAL );
	bSizer17->Add( ZSlider, 1, wxALL, 5 );
	
	XSlider = new wxSlider( this, wxID_ANY, 0, 0, 5, wxDefaultPosition, wxDefaultSize, wxSL_AUTOTICKS|wxSL_HORIZONTAL|wxSL_TOP );
	bSizer17->Add( XSlider, 0, wxALL|wxEXPAND, 5 );
	
	this->SetSizer( bSizer17 );
	this->Layout();
}

WalkingPadBase::~WalkingPadBase()
{
}
