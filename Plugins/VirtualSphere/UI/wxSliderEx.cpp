#include "wxSliderEx.hpp"


wxSliderEx::wxSliderEx():wxSlider()
{
  _modifier = 1;
}

void wxSliderEx::SetModifier(int mod)
{
  _modifier =mod;
  int min = GetMin();
  int max = GetMax();
  SetRange(min*_modifier,max*_modifier);
  SetValue(GetValue()*_modifier);
}
int wxSliderEx::GetModifier()
{
  return _modifier;
}

void wxSliderEx::OnPaint( wxPaintEvent& event )
{
  int min = GetMin();
  int max = GetMax();
  SetRange(min/_modifier,max/_modifier);

  int val = GetValue();
  SetValue(val/_modifier);

  // Do the painting
  wxSlider::OnPaint(event);

  // Return the control back to its modifier
  SetModifier(_modifier);
}