#include <wx/slider.h>

#ifndef _SLIDER_EXT
#define _SLIDER_EXT

class wxControlRenderer;

class wxSliderEx : public wxSlider
{
public:
  int _modifier;

  wxSliderEx();

  wxSliderEx(wxWindow *parent,
    wxWindowID id,
    int value,
    int minValue,
    int maxValue,
    const wxPoint& pos = wxDefaultPosition,
    const wxSize& size = wxDefaultSize,
    long style = wxSL_HORIZONTAL,
    const wxValidator& validator = wxDefaultValidator,
    const wxString& name = wxSliderNameStr):wxSlider(parent,id,value,minValue,maxValue,pos,size,style,validator,name)
  {
    _modifier = 1;
  }

  bool Create(wxWindow *parent,
    wxWindowID id,
    int value,
    int minValue, int maxValue,
    const wxPoint& pos = wxDefaultPosition,
    const wxSize& size = wxDefaultSize,
    long style = wxSL_HORIZONTAL,
    const wxValidator& validator = wxDefaultValidator,
    const wxString& name = wxSliderNameStr)
  {
    _modifier = 1;
    return wxSlider::Create(parent,id,value,minValue,maxValue,pos,size,style,validator,name);
  }


  void SetModifier(int mod);
  int GetModifier();

  virtual void OnPaint( wxPaintEvent& event );
};


#endif