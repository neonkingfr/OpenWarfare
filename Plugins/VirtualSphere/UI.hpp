#include "wx/wx.h"
#include "wx/image.h"
#include "wx/xrc/xmlres.h"
#include "wx/notebook.h"
#include "wx/richtext/richtextctrl.h"
#include "wx/event.h"

#include "wx/stdpaths.h"
#include "wx/filedlg.h"
#include "wx/dirdlg.h"
#include "wx/timer.h"
#include "wx/taskbar.h"
#include "wx/statbmp.h"
#include "wx/grid.h"
#include "wx/treectrl.h"
#include "wx/aui/auibook.h"


#include "UI/VirtualSphereUI.h"

#ifndef _UI_VIRTUAL_SPHERE
#define _UI_VIRTUAL_SPHERE

#include "./Maths/MathUtil.hpp"
#include "./Maths/VBS2Types.hpp"

// Make a typedef for the VBS2 internal identifiers
typedef BI_Vector3 Vector3;

// Some defines to make the location
// inside the vector easier
#define XPOS    0
#define YPOS    1
#define ZPOS    2

#define HEADING 0
#define PITCH   1
#define ROLL    2

class SensorPanel : public SensorPanelBase
{
public:
  SensorPanel(wxWindow *parent);

  //! Setters to the sensor panel...
  void SetOrientation(const Vector3 &orien);
  void SetPos(const Vector3 &pos);

  //! Getters to the sensor panel...
  Vector3 GetOrientation();
  Vector3 GetPos();
  Vector3 GetPosOffset();

  int GetBoneIndex();
};


// Define a new application type, each program should derive a class from wxApp
class MyApp : public wxApp
{
  typedef wxApp base;
public:  

  MyApp();
  virtual ~MyApp();

  virtual bool OnInit();
  virtual int OnExit();
};


bool StartWxResources();
void CleanWxGUIResources();



//! Implement our main frame..
class MyFrame : public VirtualSphereBase
{
public:
  MyFrame();
};




#endif