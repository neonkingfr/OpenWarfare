#include "UI.hpp"
#include "VirtualSphere.hpp"

#include "../../Common/Essential/AppFrameWork.hpp"
#include "ui/wxSliderEx.hpp"


// Global pointer to the main parent window
MyFrame *GFrame;

#define MODIFER 1000
#define SET_CTRL_MODIFIER(XX)\
{\
  int min,max;\
  min = XX->GetMin();max = XX->GetMax();\
  XX->SetRange(min*MODIFER,max*MODIFER);\
}

SensorPanel::SensorPanel(wxWindow *parent):SensorPanelBase(parent)
{
  SET_CTRL_MODIFIER(XPos)
  SET_CTRL_MODIFIER(YPos)
  SET_CTRL_MODIFIER(ZPos)
}

//! Setters to the sensor panel...
void SensorPanel::SetOrientation(const Vector3 &orien)
{
  HeadingSlider->SetValue((int)orien[0]);
  PitchSlider->SetValue((int)orien[1]);
  RollSlider->SetValue((int)orien[2]);
}

void SensorPanel::SetPos(const Vector3 &pos)
{
  XPos->SetValue((int)pos[0]*MODIFER);
  YPos->SetValue((int)pos[1]*MODIFER);
  ZPos->SetValue((int)pos[2]*MODIFER);
}

//! Getters to the sensor panel...
Vector3 SensorPanel::GetOrientation()
{
  Vector3 orien;
  orien[0] = (float) HeadingSlider->GetValue();
  orien[1] = (float) PitchSlider->GetValue();
  orien[2] = (float) RollSlider->GetValue();
  return orien;
}

Vector3 SensorPanel::GetPos()
{
  Vector3 pos;
  pos[0] = ((float) XPos->GetValue())/MODIFER;
  pos[1] = ((float) YPos->GetValue())/MODIFER;
  pos[2] = ((float) ZPos->GetValue())/MODIFER;
  return pos;
}

Vector3 SensorPanel::GetPosOffset()
{
  Vector3 pos;
  pos[0] = atof(OffsetXTextBox->GetValue().data());
  pos[1] = atof(OffsetYTextBox->GetValue().data());
  pos[2] = atof(OffsetZTextBox->GetValue().data());
  return pos;
}

int SensorPanel::GetBoneIndex()
{
  return atoi(BoneID->GetValue().data());
}

// Constructor never called.
MyApp::MyApp()
{

}

MyApp::~MyApp()
{

}

int MyApp::OnExit()
{ 
  return base::OnExit();
}

bool MyApp::OnInit()
{
  wxInitAllImageHandlers();

  GFrame = new MyFrame();    

  return true;
}

bool WXResourcesAllocated;


#define USE_GUI 0

void CleanWxGUIResources()
{
  // Check to see if the wxResources have been allocated
  // and if we free them
  if (WXResourcesAllocated)
  {
#if USE_GUI
    // Tell the app that its closing down.
    wxTheApp->OnExit();
    // Will delete the wxApp, thus ending its life.
    wxEntryCleanup();
#endif
    WXResourcesAllocated = false;
  }
}

bool StartWxResources()
{
  bool initOk = true;

#if USE_GUI
  int args=0;
  WXResourcesAllocated = wxEntryStart(args,NULL);
  wxInitAllImageHandlers();

  if ( !wxTheApp || !wxTheApp->CallOnInit() )
  {
    // Report error
    LogF("Failed to create the plugin's script debugger app");

    // Clean up the resources.
    CleanWxGUIResources();
    initOk = false;
  }
#endif

  return initOk;
}

#if USE_GUI

IMPLEMENT_APP_NO_MAIN(MyApp)

#endif



const int GTabCount      =4;
const int GSensorTabCount=3; 
const int GWalkPadCount  =1;

const char *TabNames[GSensorTabCount] = { "Head","Body","Weapon"};

SensorPanel *GSensorPanel[GSensorTabCount];

MyFrame::MyFrame():VirtualSphereBase(NULL)
{
  GFrame = this;
  
  //! Set the window size.
  const float GRatio = 1.61803399f;
  const int Height = 600;
  const int Width  = (int)(Height*GRatio);
  SetSize(wxRect(-1,-1,Width,Height));
  
  
  // Add all the necessary tabs
  for (int i=0; i<GSensorTabCount; i++)
  {
    // Add to sensor pages to the notebook
    GSensorPanel[i] = new SensorPanel(GNoteBook);
    GNoteBook->AddPage(GSensorPanel[i],TabNames[i]);
  }
  //GNoteBook->AddPage(new WalkingPad(GNoteBook),"SpherePad");  

  // Show the window
  Show();
}

/*

VBSPLUGIN_EXPORT void WINAPI OnModifyUpperBodyTorso(Matrix4 &torso)
{
  int i = 2;

  Vector3 orienVec = GSensorPanel[i]->GetOrientation();
  Vector3 posVec = GSensorPanel[i]->GetPos();

  Matrix3 heading,pitch,roll;
  heading.SetRotationY(DEGREE_TO_RAD(orienVec[0]));
  pitch.SetRotationX(DEGREE_TO_RAD(orienVec[1]));
  roll.SetRotationZ(DEGREE_TO_RAD(orienVec[2]));

  torso._orientation = heading*pitch*roll;
  torso._pos = BI_Vector3D(posVec[0],posVec[1],posVec[2]);
}

VBSPLUGIN_EXPORT void WINAPI OnModifyUpperBodyView(Matrix4 &view)
{
  int i = 1;

  Vector3 orienVec = GSensorPanel[i]->GetOrientation();
  Vector3 posVec = GSensorPanel[i]->GetPos();

  Matrix3 heading,pitch,roll;
  heading.SetRotationY(DEGREE_TO_RAD(orienVec[0]));
  pitch.SetRotationX(DEGREE_TO_RAD(orienVec[1]));
  roll.SetRotationZ(DEGREE_TO_RAD(orienVec[2]));

  view._orientation = heading*pitch*roll;
  view._pos = BI_Vector3D(posVec[0],posVec[1],posVec[2]);
}

VBSPLUGIN_EXPORT void WINAPI OnModifyUpperBodyAim(Matrix4 &aim)
{
  int i = 0;

  Vector3 orienVec = GSensorPanel[i]->GetOrientation();
  Vector3 posVec = GSensorPanel[i]->GetPos();

  Matrix3 heading,pitch,roll;
  heading.SetRotationY(DEGREE_TO_RAD(orienVec[0]));
  pitch.SetRotationX(DEGREE_TO_RAD(orienVec[1]));
  roll.SetRotationZ(DEGREE_TO_RAD(orienVec[2]));

  aim._orientation = heading*pitch*roll;
  aim._pos = BI_Vector3D(posVec[0],posVec[1],posVec[2]);
}

VBSPLUGIN_EXPORT void WINAPI OnModifyCamera(Matrix4 &camera, int camType)
{
  int i = 0;

  if (camType==0)
  {
    Vector3 orienVec = GSensorPanel[i]->GetOrientation();
    Vector3 posVec = GSensorPanel[i]->GetPos();

    Matrix3 heading,pitch,roll;
    heading.SetRotationY(DEGREE_TO_RAD(orienVec[0]));
    pitch.SetRotationX(DEGREE_TO_RAD(orienVec[1]));
    roll.SetRotationZ(DEGREE_TO_RAD(orienVec[2]));

    camera._orientation = heading*pitch*roll;
    camera._pos = BI_Vector3D(posVec[0],posVec[1],posVec[2]);
  }
}
*/