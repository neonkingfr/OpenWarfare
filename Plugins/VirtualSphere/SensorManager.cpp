#include <Essential/AppFrameWork.hpp>
#include "SensorManager.hpp"

#include <Essential/LexicalAnalyser.hpp>
#include <Essential/SyntaxAnyaliser.hpp>
#include <Essential/Config.hpp>





extern ParamaFileConfig GConfig;

SensorManager::SensorManager()
{

}

void SensorManager::Init()
{
  // Init all the current sensors
  ConfigEntry *entry = GConfig.FindEntry("Config");
  if (!entry) return;

  // Init the head sensor
  _sensors[enumHead] = new HeadSensor;
  _sensors[enumHead]->Init(entry->FindEntry("HeadSensor"));
  _sensors[enumHead]->SetSensorsName("Head Sensor");

  // Init the weapon sensor
  _sensors[enumWeapon] = new WeaponSensor;
  _sensors[enumWeapon]->Init(entry->FindEntry("WeaponSensor"));
  _sensors[enumWeapon]->SetSensorsName("Weapon Sensor");

  // Init the body sensor
  _sensors[enumBody] = new BodySensor;
  _sensors[enumBody]->Init(entry->FindEntry("BodySensor"));
  _sensors[enumBody]->SetSensorsName("Body Sensor");

  // Init the feet sensor
  _sensors[enumFeet] = new MovementSensor;  
  _sensors[enumFeet]->Init(entry->FindEntry("MovementSensor"));
  _sensors[enumFeet]->SetSensorsName("Movement Sensor");
}

void SensorManager::ReloadConfig()
{
  // Init all the current sensors
  ConfigEntry *entry = GConfig.FindEntry("Config");
  if (!entry) return;

  // Init the head sensor
  _sensors[enumHead]->Init(entry->FindEntry("HeadSensor"));
  _sensors[enumWeapon]->Init(entry->FindEntry("WeaponSensor"));
  _sensors[enumBody]->Init(entry->FindEntry("BodySensor"));
  _sensors[enumFeet]->Init(entry->FindEntry("MovementSensor"));
}


void SensorManager::MainLoop()
{
  for (int i=0; i<enumBodyMax; i++)
    if (_sensors[i]) _sensors[i]->MainLoop();
}

void SensorManager::Simulate(float deltaT)
{
  for (int i=0; i<enumBodyMax; i++)
    if (_sensors[i]) _sensors[i]->Simulate(deltaT);
}

// Console output for debug mode
void SensorManager::Console(float deltaT)
{
  for (int i=0; i<enumBodyMax; i++)
    if (_sensors[i]) _sensors[i]->Console(deltaT);
}

void SensorManager::Calibrate()
{
  for (int i=0; i<enumBodyMax; i++)
    if (_sensors[i]) _sensors[i]->Calibrate();
}


SensorsCFG *SensorManager::GetSensor(int pos)
{
  Assert((BodyPositions)pos<enumBodyMax);
  return _sensors[pos];
}


int SensorManager::NSensors()
{
  return enumBodyMax;
}


SensorManager GSensorManager;



#define VBSPLUGIN_EXPORT __declspec(dllexport)

#include "./Maths/VBS2Types.hpp"


VBSPLUGIN_EXPORT void WINAPI OnModifyCamera(Matrix4 &mat, int camType)
{
  // person needs to be in the internal camera.
  if (camType==0)
  {
    for (int i=0; i<GSensorManager.NSensors();i++)
    {
      if (i!=0) continue; // Only work on the head sensor.

      SensorsCFG *sensor = GSensorManager.GetSensor(i);
      if (!sensor) continue;
      if (!sensor->IsExtended()) continue;
      
      const Vector3 &hpr = sensor->GetCalibratedData()._hprRad;
      const Vector3 &pos = sensor->GetCalibratedData()._pos;

      Matrix3 heading,pitch,roll;
      heading.SetRotationY(hpr[0]);
      pitch.SetRotationX(hpr[1]);
      roll.SetRotationZ(hpr[2]);

      mat._orientation = heading*pitch*roll;
      mat._pos = BI_Vector3(pos[0],pos[1],pos[2]);
    }
  }
}



/*
position x axis (right-left)
position y axis (closer-further)
position z axis (down-up)
*/
VBSPLUGIN_EXPORT void WINAPI OnModifyBone(Matrix4 &mat, int skeletonIndex)
{
  for (int i=0; i<GSensorManager.NSensors();i++)
  {
    SensorsCFG *sensor = GSensorManager.GetSensor(i);
    if (!sensor) continue;
    if (!sensor->IsExtended()) continue;
    if (!(sensor->GetBoneId()==skeletonIndex)) continue;

    const Vector3 &hpr = sensor->GetCalibratedData()._hprRad;
    const Vector3 &pos = sensor->GetCalibratedData()._pos;

    Matrix3 heading,pitch,roll;
    heading.SetRotationY(hpr[0]);
    pitch.SetRotationX(hpr[1]);
    roll.SetRotationZ(hpr[2]);

    mat._orientation = heading*pitch*roll;
    if (skeletonIndex==11)
    {
      #define M_PI 3.14159265358979323846
      Matrix3 offset;
      offset.SetRotationY(90*(M_PI/180.0f));
      mat._orientation = mat._orientation*offset;
    }
    mat._pos = BI_Vector3(pos[0],pos[1],pos[2]);
  }
}
