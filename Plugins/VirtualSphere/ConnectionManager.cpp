#include <Ext/vrpn/vrpn_Connection.h> // Missing this file?  Get the latest VRPN distro at
#include <Essential/AppFrameWork.hpp>
#include "ConnectionManager.hpp"



#include <Essential/LexicalAnalyser.hpp>
#include <Essential/SyntaxAnyaliser.hpp>
#include <Essential/Config.hpp>

extern ParamaFileConfig GConfig;

ConnectionEntry::ConnectionEntry():_connection(NULL){};
ConnectionEntry::ConnectionEntry(vrpn_Connection *connection, const char *name):_connection(connection),_name(name){};

vrpn_Connection *ConnectionEntry::GetConnection()
{
  return _connection;
}

const char *ConnectionEntry::GetName()
{
  return _name.data();
}

void ConnectionEntry::MainLoop()
{
  assert(_connection);
  _connection->mainloop();
}

ConnectionEntry *ConnectionManger::Find(const char *connectionName)
{
  for (unsigned int i=0; i<_connections.size(); i++)
    if (!stricmp(connectionName,_connections[i]->GetName())) return _connections[i];

  return NULL;
}

ConnectionEntry *ConnectionManger::ConnectTo(const char *name)
{
  ConnectionEntry *entry = Find(name);
  if (entry) return entry;

  // See about creating a new connection
  vrpn_Connection *vrpnCon = NULL;
  vrpnCon = vrpn_get_connection_by_name(name);

  if (vrpnCon) 
  {
    ConnectionEntry *newConnect = new ConnectionEntry(vrpnCon,name);
    _connections.push_back(newConnect);
#if _DEBUG
    Assert(Find(name));
#endif
    return newConnect;
  }
  
  return NULL;
}

void ConnectionManger::Init()
{
  ConfigEntry *entry = GConfig.FindEntry("Config");
  if (entry)
  {
    ConfigEntry *globalConnection = entry->FindEntry("ConnectTo");
    std::string address;
    if (globalConnection && globalConnection->GetVal(address))
      GConnection = GConnectionManager.ConnectTo(address.data());
  }
}

void ConnectionManger::MainLoop()
{
  for (int i=0; i<_connections.size(); i++)
    _connections[i]->MainLoop();
}

// Global connection
ConnectionEntry *GConnection;

// Connection manager, allocated in global scope.
ConnectionManger GConnectionManager;
