#include <Essential/RefCount.hpp>
#include <BIMatrix.h>
#include <BIVector.h>

//ftp://ftp.cs.unc.edu/pub/packages/GRIP/vrpn
#include <Ext/vrpn/vrpn_Tracker.h> 
#include "ConnectionManager.hpp"

#ifndef SENSOR_CFG
#define SENSOR_CFG

typedef BI_TVector3<float> Vector3;
typedef BI_TMatrix3<float> Matrix3;

class vrpn_Tracker_Remote;
class ConfigEntry;

typedef  double          vrpn_float64;

struct SensorsCFG : public RefCount
{
public:
  // Do all calculation based on world space, not local space
  bool _worldSpace;

  // Each SensorCFG has its own tracker
  vrpn_Tracker_Remote *_tracker;

  // Possible to share connection between trackers
  SmartRef<ConnectionEntry> _connection;

  // Debug name.
  std::string _name;

  // Accumulated time, used for console debug output for each sensor
  float _aTime;

  // Sensor number, given from vrpn
  int _sensorNum;

  // Does the system use the old functionality or the new functionality?
  // by default false, to use the old functionality
  bool _extended;

  // BoneID is the bone matrix that will be modified with the VRPN matrix and position data
  int _boneId;

  // Order to interpret decomposed vrpn quaternions
  int _hprIndex[3];
  // Option to have heading pitch roll inversed
  bool _hprInvIndex[3];
  // Orientation offset, in degree's
  float _hpr_offset[3];

  // Order to interpret vrpn position
  int _posIndex[3];
  // Option to have x,y,z axis inversed
  bool _posInvIndex[3];
  // Position offset to apply to position after sorting
  float _posOffset[3];

  // Message last received from the tracker
  struct SensorDataRaw
  {
    // Sensor num
    int _sensor;   
    // Resolution is in meters
    Vector3 _pos;
    // Orientation of the sensor in Quat...
    vrpn_float64 _quat[4];	

    SensorDataRaw()
    {
      // No sensor
      _sensor = -1;

      // set quat to identity
      _quat[0]=_quat[1]=_quat[2]=0.0;
      _quat[3]=1.0;
    }
  };
  struct SensorData
  {
    int     _sensor;
    Vector3 _pos;
    Vector3 _hprRad;
  };
  bool _messageRecieved;
  bool _sensorCalibrated;

  // snap shot when calibrated data is caught
  SensorDataRaw _calibrateSnapShot; 
  // last message receive uncalibrated
  SensorDataRaw _lastRecived;       

  // calibrated data, to use
  SensorData    _calibratedSensorData;

  SensorsCFG();

  bool Init(ConfigEntry *classEntry);

  // Virtual function defined so the necessary sensor update can be called.
  virtual void HandlePosUpdate(const int sensor, const Vector3 &hprRad, const Vector3 &pos, const struct timeval&	msg_time);

  virtual void Calibrate();
  virtual SensorData &GetCalibratedData();

  // Main loop is called every simulation step.
  virtual void MainLoop();
  virtual void Simulate(float deltaT);
  virtual void Console(float deltaT);

  // Check if the simulation can be simulated. If no message
  // if received there is not point simulating the body part.
  bool CanSimulate();
  // Set the sensor name
  void SetSensorsName(const char *name);
  // Use the bone animation system
  bool IsExtended();
  // Return the BoneId
  int GetBoneId();
};

struct HeadSensor : public SensorsCFG
{
  typedef SensorsCFG base;
  void Simulate(float deltaT);
};

struct WeaponSensor : public SensorsCFG
{
  typedef SensorsCFG base;
  void Simulate(float deltaT);
};

struct BodySensor : public SensorsCFG
{
  typedef SensorsCFG base;
  void Simulate(float deltaT);
};

struct MovementSensor : public SensorsCFG
{
  typedef SensorsCFG base;

  long _preTimeMS;
  MovementSensor();

  virtual void HandelPosUpdate(const int sensor, const Vector3 &hprRad, const Vector3 &pos, const struct timeval&	msg_time);
  void Simulate(float deltaT);
};



#endif