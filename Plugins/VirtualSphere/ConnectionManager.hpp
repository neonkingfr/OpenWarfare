#include <Essential/RefCount.hpp>
#include <vector>

#ifndef SHARED_CONNECTION
#define SHARED_CONNECTION

class vrpn_Connection;

class ConnectionEntry : public RefCount
{
  // Connection name
  std::string _name; 
  // VRPN connection..
  vrpn_Connection *_connection;
public:
  ConnectionEntry();
  ConnectionEntry(vrpn_Connection *connection, const char *name);

  vrpn_Connection *GetConnection();
  const char *GetName();

  void MainLoop(); 
};

class ConnectionManger
{
  std::vector< SmartRef<ConnectionEntry> > _connections;

  ConnectionEntry *Find(const char *connectionName);
public:
  void Init();

  ConnectionEntry *ConnectTo(const char *connect);
  
  void MainLoop();
};

// Global connection
extern ConnectionEntry *GConnection;
extern ConnectionManger GConnectionManager;

#endif