// Config class to specify the needed, trackers and how they will interact with the plugin
class Config
{
	ConnectTo="localhost:3883";

	// HeadSensor sensor, for looking around. so the plugin knows what is what. Only uses the 3DOF not 6DOF, can be implimented later
	class HeadSensor
	{
		DeviceName="";
		//ConnectTo="";
	};

	// Weapon sensor, uses the same type of convention, and uses only the 3DOF not 6DOF. So no X,Y,Z  movement
	class WeaponSensor
	{
		DeviceName="";
		//ConnectTo="";
	};

	// Movement sensor moves the player via the set velocity command. So no orientation data is required.
	class MovementSensor
	{
		DeviceName="";
		//ConnectTo="";
	};
	
};