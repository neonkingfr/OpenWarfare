#ifndef __MATHUTIL_H_INCLUDED__
#define __MATHUTIL_H_INCLUDED__

#define _USE_MATH_DEFINES
#include <math.h>

// "Wrap" an angle in range -pi...pi by adding the correct multiple of 2 pi
extern float wrapPi(float theta);

// "Safe" inverse trig functions
extern float safeAcos(float x);

// Convert between degrees and radians
inline float DegToRad(float deg){ return deg * M_PI * (1/180.0f); }
inline float RadToDeg(float rad){ return rad * 180.0f * M_1_PI; }

inline void sinCos(float *returnSin, float *returnCos, float theta) 
{
	// For simplicity, we'll just use the normal trig functions.
	// Note that on some platforms we may be able to do better
	*returnSin = sin(theta);
	*returnCos = cos(theta);
}

inline float	fovToZoom(float fov) { return 1.0f / tan(fov * .5f); }
inline float	zoomToFov(float zoom) { return 2.0f * atan(1.0f / zoom); }


struct Quaternion 
{
    float x;
    float y;
    float z;
    float w;
};

/**
 * Do an euler decomposition of quaternion q for an aerospace sequence
 * rotation (yaw => pitch => roll).  In the resulting vector, x is
 * roll, y is pitch, z is yaw.
 */
void QuaToEulerAngles
(
  const Quaternion &q,
  float &yaw, 
  float &pitch, 
  float &roll
);
       
void QuaNormalize(struct Quaternion* out, const struct Quaternion* q);
void QuaConjugate(struct Quaternion* out,const struct Quaternion* q);


/// maximum of two values
/** if a is NaN, b is used - helps in saturate */
inline float floatMax(float a, float b) {return ( a>b ? a : b);}
/// minimum of two values
/** if a is NaN, b is used - helps in saturate */
inline float floatMin(float a, float b) {return ( a<b ? a : b);}

/// if a is above b, saturate it : a = min(a,b)
__forceinline void saturateMin( float &a, float b ) {a = floatMin(a,b);}
/// if a is below b, saturate it : a = max(a,b)
__forceinline void saturateMax( float &a, float b ) {a = floatMax(a,b);}

/// return a saturated between minVal and maxVal
inline float floatMinMax(float a, float minVal, float maxVal)
{
  a = floatMax(a,minVal);
  a = floatMin(a,maxVal);
  return a;
}
/// saturate a between minVal and maxVal
inline void saturate(float &a, float minVal, float maxVal)
{
  a = floatMax(a,minVal);
  a = floatMin(a,maxVal);
}

/////////////////////////////////////////////////////////////////////////////
#endif // #ifndef __MATHUTIL_H_INCLUDED__
