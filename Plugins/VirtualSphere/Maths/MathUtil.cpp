#include "MathUtil.hpp"

//---------------------------------------------------------------------------
// "Wrap" an angle in range -pi...pi by adding the correct multiple
// of 2 pi
float wrapPi(float theta) 
{
	theta += M_PI;
	theta -= floor(theta * M_1_PI) * 2.0f * M_PI;
	theta -= M_PI;
	return theta;
}

//---------------------------------------------------------------------------
// safeAcos
//
// Same as acos(x), but if x is out of range, it is "clamped" to the nearest
// valid value.  The value returned is in range 0...pi, the same as the
// standard C acos() function

float safeAcos(float x) 
{
	// Check limit conditions
	if (x <= -1.0f) return M_PI;
	if (x >= 1.0f) return 0.0f;

	// Value is in the domain - use standard C function
	return acos(x);
}


/**
 * Do an euler decomposition of quaternion q for an aerospace sequence
 * rotation (yaw => pitch => roll).  In the resulting vector, x is
 * roll, y is pitch, z is yaw.
 */
void QuaToEulerAngles
(
  const Quaternion &q,
  float &yaw, 
  float &pitch, 
  float &roll
)
{
  float m11 = (2.0f * q.w * q.w) + (2.0f * q.x * q.x) - 1.0f;
  float m12 = (2.0f * q.x * q.y) + (2.0f * q.w * q.z);
  float m13 = (2.0f * q.x * q.z) - (2.0f * q.w * q.y);
  float m23 = (2.0f * q.y * q.z) + (2.0f * q.w * q.x);
  float m33 = (2.0f * q.w * q.w) + (2.0f * q.z * q.z) - 1.0f;

  roll = atan2f(m23, m33);
  pitch = asinf(-m13);
  yaw = atan2f(m12, m11);
}

float q_lengthSq(const struct Quaternion* q) 
{
    return ((q->w * q->w) + (q->x * q->x) + (q->y * q->y) + (q->z * q->z));
}

float q_length(const struct Quaternion* q) 
{
    return sqrtf(q_lengthSq(q));
}

void q_scale
(
  struct Quaternion* out,
  const struct Quaternion* q,
  float scale
) 
{
    out->w = q->w * scale;
    out->x = q->x * scale;
    out->y = q->y * scale;
    out->z = q->z * scale;
}

void QuaNormalize(struct Quaternion* out, const struct Quaternion* q) 
{
    float len = q_length(q);
    q_scale(out, q, 1.0f / len);
}

void QuaConjugate(struct Quaternion* out,const struct Quaternion* q) 
{
    out->w = q->w;
    out->x = -q->x;
    out->y = -q->y;
    out->z = -q->z;
}

