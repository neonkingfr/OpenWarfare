#include <BIMatrix.h>
#include <BIVector.h>

typedef BI_TVector3<float> Vector3;
typedef BI_TMatrix3<float> Matrix3;

// Template Matrix4 to duplicate 
// structure alignment of internal engines Matrix4
struct Matrix4
{
  Matrix3     _orientation;
  BI_Vector3 _pos;
};