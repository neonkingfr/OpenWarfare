// Config class to specify the needed, trackers and how they will interact with the plugin
class Config
{
	// By default, the plugin will search 1st for a Global connectTo, for the host sending the frames. If the Sensor class
	// defines its own ConnectTo, then the sensor will connect to that IP address instead.
	ConnectTo="localhost:3883";

	// HeadSensor is used to set the ingame head direction
	class HeadSensor
	{
		// Device name that is defined within the VRPN.cfg server config.
		//DeviceName="FreeSpaceHead";
		
		// By default, the plugin will search 1st for a Global connectTo, for the host sending the frames. If the Sensor class
		// defines its own ConnectTo, then the sensor will connect to that IP address instead.
		// ConnectTo="localhost:3883";		
	};	

	// Weapon sensor, is used to set the ingame weapon orientation. The orientation is provided in World Corrdinates
	// and the plugin takes care of calculating the relative orientation to the body. The WeaponSensor does NOT change direction.
	class WeaponSensor
	{
		// Device name that is defined within the VRPN.cfg server config.
		//DeviceName="FreeSpaceWeaponAndBody";
	};

	// Body sensor controls the person direction within the game world, it can be assigned another control or the same as
	// weapon sensor to reduce the number of sensors required. Though it is recommended to have 4 different sensors. 
	// 1) Head
	// 2) Weapon
	// 3) Hips
	// 4) Hammster ball for movement
	class BodySensor
	{
		// Device name that is defined within the VRPN.cfg server config.
		//DeviceName="FreeSpaceWeaponAndBody";
	};

	// Movement sensor controls the players move forward and side step left and right. This is still absolute and not 
	// relative to the direction the player is facing
	class MovementSensor
	{
		// Device name that is defined within the VRPN.cfg server config.
		DeviceName="WalkTracker0";
	};		
};