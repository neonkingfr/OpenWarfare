#include <windows.h>

#define _USE_MATH_DEFINES

# if defined(_MSC_VER)
# ifndef _CRT_SECURE_NO_DEPRECATE
# define _CRT_SECURE_NO_DEPRECATE (1)
# endif
# pragma warning(disable : 4996)
# endif

#define WX_UI 0

#include <Math.h>
#include <vector>

#include <Essential/AppFrameWork.hpp>
#include <Essential/Memory/MemDump.hpp>
#include <Essential/RefCount.hpp>
#include <Essential/LexicalAnalyser.hpp>
#include <Essential/SyntaxAnyaliser.hpp>
#include <Essential/Config.hpp>

/*
#include "../Ext/vrpn/vrpn_Connection.h" // Missing this file?  Get the latest VRPN distro at
#include "../Ext/vrpn/vrpn_Tracker.h" //    ftp://ftp.cs.unc.edu/pub/packages/GRIP/vrpn
*/

#include "VirtualSphere.hpp"
#include "SensorManager.hpp"


// Global config
ParamaFileConfig GConfig;

// Command function definition
// Function that will register the ExecuteCommand function of the engine
ExecuteCommandType ExecuteCommand = NULL;
int ExecuteCmd(const char* command, char *result = NULL, int resultLength = 0)
{
  if (!ExecuteCommand) return 0;
  return ExecuteCommand(command, result, resultLength);
}
VBSPLUGIN_EXPORT void WINAPI RegisterCommandFnc(void *executeCommandFnc)
{
  ExecuteCommand = (ExecuteCommandType)executeCommandFnc;
}


// Return the current mission time.
float GetMissionTime()
{
  static float lastMissionTime = 0.0f;
  float retVal = 0.0f;
  char timeStr[128] = {0};

  ExecuteCommand("time", timeStr, sizeof(timeStr));
  retVal = (float)atof(timeStr);

  if (retVal < lastMissionTime)
  {
    retVal = 0.0f;
  }
  lastMissionTime = retVal;
  return retVal;
}

bool GMissionRunning   = false;
bool InitSuccessfull   = false;
float previousMissionTime = 0;

// Function called when mission is starting.
void OnMissionStart(double missionStart, double deltaT)
{
  // First check to see if the player has been set, before sending commands to the game
  char buffer[256] = {0};
  ExecuteCmd("isNull player",buffer,255);
  if (stricmp(buffer,"true")==0) return;

  // Player has been set, time to start the mission and run internal script commands
  GMissionRunning = true;
  LogF("Mission Start\r\n");

  // Start in free look mode.
  ExecuteCmd("\"LookAround\" setAction 1"); 
  ExecuteCmd("\"ToggleRaiseWeapon\" setaction 1");
  ExecuteCmd("setweaponswayfactor 0.0");
  ExecuteCmd("player allowDammage false"); 
  ExecuteCmd("player setDisableAnimationMove true");

  // Turn on extended control of skeleton and head movements
  ExecuteCmd("player setExternalPose true");
  ExecuteCmd("player setExternalPoseSkeleton true");
  ExecuteCmd("player setExternalCamera true");
  
  
  // Parse the config for changes.
  void ParseConfig();
  ParseConfig();
  // Reload the new config.
  GSensorManager.ReloadConfig();

  // Calibrate all sensors
  GSensorManager.Calibrate();
}

// Function called when mission is ending.
void OnMissionEnd(double missionStart, double deltaT)
{
  LogF("Mission End\r\n");
  GMissionRunning = false;
  previousMissionTime = 0.0f;
}

// Function that will register the ExecuteCommand function of the engine
VBSPLUGIN_EXPORT void WINAPI RegisterHWND(void *hwnd, void *hins){}


bool IsPlayerAttached()
{
  char buffer[256] = {0};
  ExecuteCmd("isAttached (vehicle player)",buffer,256);
  return _stricmp(buffer,"true")==0;
}

std::string PlayerAttachTo()
{
  char buffer[256] = {0};
  ExecuteCmd("attachedObject (vehicle player)",buffer,256);
  return std::string(buffer);
}

VBSPLUGIN_EXPORT void WINAPI OnAfterSimulation(float deltaT)
{
  float missionTime = GetMissionTime();

  // Detect when he mission has started
  if ((missionTime > 0.0f) && !GMissionRunning) OnMissionStart(missionTime, deltaT);
  
  // Detect when the mission has finished
  if (GMissionRunning && (missionTime == 0.0f || missionTime < previousMissionTime)) OnMissionEnd(missionTime, deltaT);
  previousMissionTime = missionTime;
  
  if (!GMissionRunning) return;

  // Run the main loop of the connection and the trackers, for interrupts or
  // data processing.
  GConnectionManager.MainLoop();
  GSensorManager.MainLoop();
  GSensorManager.Simulate(deltaT);
}


VBSPLUGIN_EXPORT void WINAPI OnConsoleSimulation(float deltaT)
{
  // Map connection loop for all sensor connections
  GConnectionManager.MainLoop();
  
  GSensorManager.MainLoop();
  GSensorManager.Simulate(deltaT);
  GSensorManager.Console(deltaT);
}

void CreateFrameWork();
void DeleteFrameWork();

/*
 GHModule is used when the code base is either a DLL or Console project.
*/
HINSTANCE GHInstance = NULL;
std::string GetImageDirectory()
{
  std::string dir;
  char mydirectory[MAX_PATH] = {0};
  GetModuleFileName(GHInstance,mydirectory,MAX_PATH);

  // need to find the '/' at the end.
  char *find = strrchr(mydirectory,'\\');
  
  if (find)
  {
    *find = 0;
    dir = Format("%s",mydirectory);
  }

  return dir;
}


void ParseConfig()
{
  // Clear everything.
  GConfig.clear();

  std::string configPath = GetImageDirectory();
  configPath = Format("%s%s",configPath.data(),"\\config.cpp");

  LogF("Trying to open config file:%s\n",configPath.data());
  
  ifstream configFile;
  configFile.open(configPath.data(),ios::in | ios::binary);
  if (configFile.is_open())
  {
    LogF("Found config file:%s", configPath.data());

    // Copy the whole config file into our memory..
    std::string fullFile;
    char buffer[1024] = {0};
    while (configFile.good())
    {
      configFile.read(buffer,1024);
      fullFile.append(buffer,configFile.gcount());
    }

    configFile.close();

    LogF("Loaded config file into memory, parsing\n");

    // Time to parse the config file, create a syntax and lexical analyser
    SmartRef<LexicalAnalyser> lexier = new LexicalAnalyser(fullFile.data());
    SmartRef<SyntaxAnyaliser> syntax = new SyntaxAnyaliser;

    if (syntax->Parse(lexier,&GConfig)) LogF("Parsing completed\n");
    else LogF("Parsing of config error\n");
  }
}




void ConnectToVRPN()
{
  // Init the connection manager, setup the connection
  GConnectionManager.Init();
  // Init the Sensor manager
  GSensorManager.Init();
}

bool StartWxResources();
void CleanWxGUIResources();

int main(int argc, char * argv[])
{
  CreateFrameWork();

#if WX_UI
  StartWxResources();
#endif

  // Load and parse the config file.
  ParseConfig();

  // Init the connection and start communicating with remote trackers
  ConnectToVRPN();

  // While loop to simulate the normal VBS simulation cycle.
  float timeOut = (1.0f/1000.0f)*30.0f;
  while (true)
  {
    MSG msg;
    while (::PeekMessage(&msg, NULL, 0, 0, PM_REMOVE)) 
    {
      TranslateMessage(&msg);
      DispatchMessage(&msg);
    } 

    OnConsoleSimulation(timeOut);
    Sleep((int)timeOut);
  }

#if WX_UI
  CleanWxGUIResources();
#endif
  DeleteFrameWork();
 
  return 0;
}

// DllMain
BOOL WINAPI DllMain(HINSTANCE hDll, DWORD fdwReason, LPVOID lpvReserved)
{
   switch(fdwReason)
   {
      case DLL_PROCESS_ATTACH:
         // On attach create the framework for debugging and logging
         CreateFrameWork();
  
         // Store the Instance of the DLL.
         GHInstance = hDll;

#if WX_UI
         // Create UI if there is one.
         StartWxResources();
#endif

         // Load and parse the config file.
         ParseConfig();

          // Init the connection and start communicating with remote trackers
         ConnectToVRPN();

         LogF("Called DllMain with DLL_PROCESS_ATTACH\n");
         break;
      case DLL_PROCESS_DETACH:
#if WX_UI
         // Clean the wx init resources
         CleanWxGUIResources();
#endif
         // Remove the framework, and perform any needed clean up.
         DeleteFrameWork();

         OutputDebugString("Called DllMain with DLL_PROCESS_DETACH\n");
         break;
      case DLL_THREAD_ATTACH:
         OutputDebugString("Called DllMain with DLL_THREAD_ATTACH\n");
         break;
      case DLL_THREAD_DETACH:
         OutputDebugString("Called DllMain with DLL_THREAD_DETACH\n");
         break;
   }
   return TRUE;
}

VBSPLUGIN_EXPORT const char* WINAPI PluginFunction(const char *input)
{
  static const char result[]="VirtuSphere Plugin";
  return result;
}