#include <Windows.h>
#include <WinBase.h>
#include <Essential/AppFrameWork.hpp>

// Forward declaration, from wxWindow Library
class wxTextCtrl;

// Global variables are inited to 0.
AppFrameWorkBase *GFrameWork;

// Basic Win32 Framework provided, this can be modified so that 
// the user can have wxWindows running as a secondary window.
class Win32FrameWork : public AppFrameWorkBase
{
public:
  Win32FrameWork(){}

  void SetTextControl(wxTextCtrl *ctrl)
  {
  }

  // System status
  void LogF(const char *msg)
  {
    OutputDebugString(msg);
  }
  // Dialog box pops up
  void WarningMessage(const char *msg)
  {
    OutputDebugString(msg);
  }
  // Dialog pops up and the program does a memory dump and call stack.
  void ErrorMessage(const char *msg)
  {
    OutputDebugString(msg);
  }
};

void CreateFrameWork()
{
  GFrameWork = new Win32FrameWork;
}

void DeleteFrameWork()
{
  delete GFrameWork;
  GFrameWork = NULL;
}