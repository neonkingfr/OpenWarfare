#include <Essential/RefCount.hpp>
#include <Essential/AppFrameWork.hpp>

#include "Maths/MathUtil.hpp"

#include "SensorCFG.hpp"
#include "ConnectionManager.hpp"

#include <Essential/LexicalAnalyser.hpp>
#include <Essential/SyntaxAnyaliser.hpp>
#include <Essential/Config.hpp>

#include <Ext/vrpn/quat/quat.h>


SensorsCFG::SensorsCFG()
:_tracker(NULL)
,_worldSpace(true)
,_messageRecieved(false)
,_sensorCalibrated(false)
,_aTime(0.0f)
,_sensorNum(0)
{
  for (int i=0; i<3; i++)
  {
    _hprIndex[i]=i;
    _hprInvIndex[i]=false;
    _hpr_offset[i]=0.0f;

    _posIndex[i]=i;
    _posInvIndex[i]=false;
    _posOffset[i]=0.0f;
  }

  _extended = false;
  _boneId = 0;
}

// Forward declaration from VirtualSphere.cpp
int ExecuteCmd(const char* command, char *result = NULL, int resultLength = 0);


float Difference(float minuend,float subtrahend)
{
  double diff = minuend - subtrahend;
  while (diff < 0) diff += (2*M_PI);
  while (diff >= (2*M_PI)) diff -= (2*M_PI);
  diff = abs(diff);
  if (diff > M_PI) { diff -= (2*M_PI); }
  
  return (float)diff; 
}

float ConvertToNegPIToPI(float radangle)
{
  while (radangle > M_PI) radangle = -(2*M_PI) + (radangle - (2*M_PI));
  while (radangle <-M_PI) radangle =  (2*M_PI) + (radangle + (2*M_PI));
  return radangle;
}

// Returns the child's direction.
float CalculateRelativeDir(float child, float parent)
{
  float relDir = Difference(child,parent);
  return ConvertToNegPIToPI(relDir);
}

float GetPlayerDirRad()
{
  char playerDir[256] = {0};
  ExecuteCmd("getdir player",playerDir,255);

  // Use the player dir to calculate the relative position.
  return (float)((M_PI/180.0)*atof(playerDir));
}

/*
Example give how to take a [0.1,0.1,0.1] and using tokens to make it easier to parse
*/
bool GetVBSStringVector(LexicalAnalyser &lexer,Vector3 &vec)
{
  std::string numConvert;
  if (lexer.NextToken()._type != T_LSBRACKET) return false;

  while (lexer.NextToken()._type != T_COMMA && lexer.GetCurrentToken()._type != T_EOF)
  {
    numConvert = numConvert + lexer.GetCurrentToken()._identifier;
  }
  vec[0] = (float)atof(numConvert.data());
  numConvert = "";

  while (lexer.NextToken()._type != T_COMMA && lexer.GetCurrentToken()._type != T_EOF)
  {
    numConvert = numConvert + lexer.GetCurrentToken()._identifier;
  }
  vec[2] = (float)atof(numConvert.data());
  numConvert = "";

  while (lexer.NextToken()._type != T_RSBRACKET && lexer.GetCurrentToken()._type != T_EOF)
  {
    numConvert = numConvert + lexer.GetCurrentToken()._identifier;
  }
  vec[1] = (float)atof(numConvert.data());

  return true;
}

void q_invert(q_type destQuat, const q_type srcQuat);
void q_mult(q_type destQuat, const q_type qLeft, const q_type qRight);
void q_to_euler(q_vec_type yawPitchRoll, const q_type q);

// Call the co-responding handler for this pos update.
void	VRPN_CALLBACK handle_pos(void *userData, const vrpn_TRACKERCB t)
{
  // Check to make sure we're calling somebody
  if (!userData) return;

  SensorsCFG *sensorCfg = (SensorsCFG*) userData;
  if (sensorCfg->_sensorNum==t.sensor)
  {
    // Update the sensors last recieved.
    for (int i=0; i<3; i++)
      sensorCfg->_lastRecived._pos[i]  = t.pos[i];
    
    for (int i=0; i<4; i++)
      sensorCfg->_lastRecived._quat[i] = t.quat[i];

    sensorCfg->_lastRecived._sensor = t.sensor;


    // Perform the calibration, for quat.
    vrpn_float64 invQuat[4];
    vrpn_float64 calQuat[4];
    q_invert(invQuat,sensorCfg->_calibrateSnapShot._quat);
    q_mult(calQuat,invQuat,t.quat);


    // Convert quat to hpr.
    q_type quat;
    q_vec_type hprRad;
    for (int i=0; i<4; i++) quat[i]=calQuat[i];
    q_to_euler(hprRad,quat);
    

    // Perform any, adjustment to the hpr from the config file before
    // passing it onto the sensors handle pos update
    float heading,pitch,roll;
    heading = hprRad[sensorCfg->_hprIndex[0]];
    pitch   = hprRad[sensorCfg->_hprIndex[1]];
    roll    = hprRad[sensorCfg->_hprIndex[2]];

    heading = sensorCfg->_hprInvIndex[0] ? -heading : heading;
    pitch = sensorCfg->_hprInvIndex[1] ? -pitch : pitch;
    roll = sensorCfg->_hprInvIndex[2] ? -roll : roll;

    // Apply heading, pitch, roll offset
    heading += (sensorCfg->_hpr_offset[0]*(M_PI/180.0f));
    pitch   += (sensorCfg->_hpr_offset[1]*(M_PI/180.0f));
    roll    += (sensorCfg->_hpr_offset[2]*(M_PI/180.0f));


    Vector3 pos;
    for (int i=0; i<3; i++)
    {
      pos[i]  = (float) t.pos[sensorCfg->_posIndex[i]];
      pos[i]  = sensorCfg->_posInvIndex[i] ? -pos[i] : pos[i];
      pos[i] += sensorCfg->_posOffset[i];
    }   

    sensorCfg->HandlePosUpdate(t.sensor,Vector3(-heading,pitch,roll),pos,t.msg_time);
  }  
}


bool SensorsCFG::Init(ConfigEntry *classEntry)
{
  if (!classEntry) return false;

  ConfigEntry *entry = classEntry->FindEntry("ConnectTo");
  std::string address;
  if (entry && entry->GetVal(address))
  {
    _connection = GConnectionManager.ConnectTo(address.data());
  }
  else if (GConnection) // Check to see if there is a global connection
  {
    _connection = GConnection;
  }

  // No connection available cannot connect to nothing.
  if (!_connection) return false;

  // Get the sensor Number
  entry = classEntry->FindEntry("VRPN_SensorNum");
  if (entry) entry->GetVal(_sensorNum);

  /*
    hpr_order=[0,1,2];
    hpr_inv=[false,false,false];
  */
  entry = classEntry->FindEntry("hpr_order");
  if (entry&&entry->IsArray())
  {
    ConfigArray *array = entry->GetArray();
    for (int i=0; i<array->size() && i<3; i++)
    {
      if ((*array)[i]->GetVal(_hprIndex[i]))
        if (_hprIndex[i]<0||_hprIndex[i]>3) _hprIndex[i] = i;
    }
  }
  entry = classEntry->FindEntry("hpr_offset");
  if (entry&&entry->IsArray())
  {
    ConfigArray *array = entry->GetArray();
    for (int i=0; i<array->size() && i<3; i++)
      (*array)[i]->GetVal(_hpr_offset[i]);
  }
  entry = classEntry->FindEntry("hpr_inv");
  if (entry&&entry->IsArray())
  {
    ConfigArray *array = entry->GetArray();
    for (int i=0; i<array->size() && i<3; i++)
      (*array)[i]->GetVal(_hprInvIndex[i]);
  }

  entry = classEntry->FindEntry("pos_order");
  if (entry&&entry->IsArray())
  {
    ConfigArray *array = entry->GetArray();
    for (int i=0; i<array->size() && i<3; i++)
    {
      if ((*array)[i]->GetVal(_posIndex[i]))
        if (_posIndex[i]<0||_posIndex[i]>3) _posIndex[i] = i;
    }
  }
  entry = classEntry->FindEntry("posOffset");
  if (entry&&entry->IsArray())
  {
    ConfigArray *array = entry->GetArray();
    for (int i=0; i<array->size() && i<3; i++)
      (*array)[i]->GetVal(_posOffset[i]);
  }
  entry = classEntry->FindEntry("pos_inv");
  if (entry&&entry->IsArray())
  {
    ConfigArray *array = entry->GetArray();
    for (int i=0; i<array->size() && i<3; i++)
      (*array)[i]->GetVal(_posInvIndex[i]);
  }

  entry = classEntry->FindEntry("useBoneSystem");
  if (entry) entry->GetVal(_extended);

  entry = classEntry->FindEntry("boneId");
  if (entry) entry->GetVal(_boneId);

  // Get the device name
  std::string deviceName;
  entry = classEntry->FindEntry("VRPN_SensorName");
  if (entry && entry->GetVal(deviceName))
  {
    LogF("Connecting to device:%s",deviceName.data());
    _tracker = new vrpn_Tracker_Remote(deviceName.data(),_connection->GetConnection());
    if (_tracker)
    {
      // For the mean time register a handler
      _tracker->register_change_handler(this, handle_pos);

      return true;
    }
  }

  return false;
}


/*
Standard from VRPN.

position x axis (right-left)
position y axis (closer-further)
position z axis (down-up)
*/



// Virtual function defined so the necessary sensor update can be called.
void SensorsCFG::HandlePosUpdate(const int sensor, const Vector3 &hprRad, const Vector3 &pos, const struct timeval&	msg_time)
{
  _calibratedSensorData._sensor = sensor;
  _calibratedSensorData._hprRad = hprRad;
  _calibratedSensorData._pos    = pos;

  _messageRecieved=true;
}

void SensorsCFG::Calibrate()
{
  _calibrateSnapShot = _lastRecived;
}

SensorsCFG::SensorData &SensorsCFG::GetCalibratedData()
{ 
  return _calibratedSensorData; 
}

// Main loop is called every simulation step.
void SensorsCFG::MainLoop()
{
  if (_tracker) _tracker->mainloop();
}

void SensorsCFG::Simulate(float deltaT)
{

}

void SensorsCFG::Console(float deltaT)
{
  static float lastDeltaT = 0.0f;

  // Only output from a sensor that has received data
  if (_messageRecieved)
  {
    _aTime += deltaT;
    // only update every second, not to overload the output in the console
    if (_aTime>=20.0f)
    {
      _aTime = 0.0f;
      LogF("Sensor:%s\n",_name.data());
      LogF("Heading:%f Pitch:%f Roll:%f\n",_calibratedSensorData._hprRad[0],_calibratedSensorData._hprRad[1],_calibratedSensorData._hprRad[2]);
      LogF("Pos:%f %f %f",_lastRecived._pos[0],_lastRecived._pos[1],_lastRecived._pos[2]);
    }
  }
}

// Check if the simulation can be simulated. If no message
// if received there is not point simulating the body part.
bool SensorsCFG::CanSimulate(){return _messageRecieved;}


void SensorsCFG::SetSensorsName(const char *name)
{
  _name = std::string(name);
}

bool SensorsCFG::IsExtended()
{
  return _extended;
}

int SensorsCFG::GetBoneId()
{
  return _boneId;
}

/*
 Bellow is where all the magic happens when receiving updates from the devices.
 Each class virtual function is called that in turns modifies the internal game world.
*/
void HeadSensor::Simulate(float deltaT)
{
  // No simulation if the system is in extended mode
  if (_extended) return;  

  float heading = GetCalibratedData()._hprRad[0];
  float pitch = GetCalibratedData()._hprRad[1];
  float roll = GetCalibratedData()._hprRad[2];

  if (_worldSpace) heading = CalculateRelativeDir(-heading,GetPlayerDirRad());

  // Set the head head direction
  std::string cmd = ::Format("player setHeadDir [[%f,%f,%f],false]",-heading * (180.0f/M_PI),pitch * (180.0f/M_PI),roll  * (180.0f/M_PI));
  ExecuteCmd(cmd.data());
}


void WeaponSensor::Simulate(float deltaT)
{
  // No simulation if the system is in extended mode
  if (_extended) return;

  float heading = GetCalibratedData()._hprRad[0];
  float pitch = GetCalibratedData()._hprRad[1];
  float roll = GetCalibratedData()._hprRad[2];
  
  if (_worldSpace) heading = CalculateRelativeDir(-heading,GetPlayerDirRad());

  // Players weapon direction, relative to the body.
  std::string cmd = ::Format("player SetWeaponDirection [[%f,%f,%f],false]",heading * (180.0f/M_PI),pitch * (180.0f/M_PI),roll  * (180.0f/M_PI));
  ExecuteCmd(cmd.data());
}

void BodySensor::Simulate(float deltaT)
{
  // No simulation if the system is in extended mode
  if (_extended) return;

  float heading = GetCalibratedData()._hprRad[0];
  float pitch = GetCalibratedData()._hprRad[1];
  float roll = GetCalibratedData()._hprRad[2];

  // Players weapon direction, relative to the body.
  std::string cmd = ::Format("player setdir %f",-heading * (180.0f/M_PI));
  ExecuteCmd(cmd.data());
}



MovementSensor::MovementSensor():_preTimeMS(0){}

// Here we calculate the velocity of the mouse to input this velocity into the engine.
// How do we do this? 
//
// What is a velocity?
// Velocity = Distance / Time
// Velocity represents the ratio of displacement per unit time and involves a direction of motion.
//
// The internal engine takes a velocity and calculates the displacement of the linear position 
// by x = v*t
// 
// For simplicity sake, lets make t=0.25ms
// Assuming the screen size is 0-1, we assume 1 is 1 meter.
// Data we collect is, T=0 X=0 Y=0, T=0.25 X=1 Y=0 this results in a velocity of sqrt(x*x+y*y)= dist
// dist/time = to get a a ratio of displacement time of 4 meters per second.
//
// Engine takes the 4 meters per second (ratio of displacement time)
// it has the the deltaT of 0.25ms. To calculate its displacement inside the engine it uses
// x = v*t. We have a velocity of 4 mps, we times this by 0.25 to come to the conclusion of 0.25 distance.
//
// std::string moveFwdCmd   = ::Format("\"MoveForward\" setAction %f",t.pos[2]);
// std::string moveRightCmd = ::Format("\"TurnLeft\" setAction %f",t.pos[0]);
// ExecuteCommandFormat(NULL,0,moveFwdCmd.data());
// ExecuteCommandFormat(NULL,0,moveRightCmd.data());
void MovementSensor::HandelPosUpdate(const int sensor, const Vector3 &hprRad, const Vector3 &pos, const struct timeval&	msg_time)
{
  // Call the base class
  base::HandlePosUpdate(sensor,hprRad,pos,msg_time);

  long currentTime = msg_time.tv_usec;
  
  // Check that the message is new.
  if (currentTime==_preTimeMS) return;

  float playerHeading = 0;
  //playerHeading = GetPlayerDirRad();

  // Calculate the velocity
  if (_preTimeMS!=0)
  {
    // Here we make a very basic assumption. The tracker is suppose to
    // send updates at 60hz so any message greater than then typical 60hz update
    // rate is defaulted to it to prevent major warping.
    float deltaT = (currentTime-_preTimeMS)/1000.0f;
    if (deltaT > 1.0/60.0) deltaT = 1.0f/60.0f;

    float inv_deltaT = 1.0f/deltaT;
    Vector3 vel;
    vel[0] = (float)pos[0]*inv_deltaT;
    vel[2] = (float)pos[2]*inv_deltaT;

    Matrix3 playerOrien;
    playerOrien.SetRotationY(-playerHeading);

    vel = playerOrien * vel;

    // Set the velocity
    std::string cmd = ::Format("player setVelocity [%f,%f,%f]",vel[0],vel[2],vel[1]);
    ExecuteCmd(cmd.data());
  }

  _preTimeMS = currentTime;
}

void MovementSensor::Simulate(float deltaT)
{
  // No simulation if the system is in extended mode
  if (_extended) return;

  // Each simulation step needs to set the players velocity
  char buffer[256] = {0};
  float playerDir = 0.0f;
  Vector3 velocity;

  // speed in world coordinates
  ExecuteCmd("velocity player",buffer,255);
  // playerDir = GetPlayerDirRad();

  LexicalAnalyser lexer(buffer);
  if (GetVBSStringVector(lexer,velocity))
  {
    Matrix3 playerDirMatrix;
    playerDirMatrix.SetRotationY(playerDir);

    velocity = playerDirMatrix * velocity;

    // Here we put in a buffer zone of values above 0.1mps
    velocity[0] = abs(velocity[0])> 0.6f ? velocity[0] : 0.0f;
    velocity[2] = abs(velocity[2])> 0.6f ? velocity[2] : 0.0f;

    std::string moveRightCmd = ::Format("\"TurnLeft\" setAction %f",-velocity[0]);
    std::string moveFwdCmd   = ::Format("\"MoveForward\" setAction %f",velocity[2] );

    ExecuteCmd(moveRightCmd.data());
    ExecuteCmd(moveFwdCmd.data());
  }
}
