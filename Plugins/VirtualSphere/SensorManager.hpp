#include "SensorCFG.hpp"


#ifndef SENSOR_MANAGER
#define SENSOR_MANAGER

enum BodyPositions
{
  enumHead,
  enumWeapon,
  enumBody,
  enumFeet,
  enumBodyMax
};

/*
 Managers all the sensor cfgs, and the initialization of the head,body,feet sensors
 */
class SensorManager
{
  SmartRef<SensorsCFG> _sensors[enumBodyMax];
public:
  SensorManager();

  // Init the devices.
  void Init();

  // Reload the configuration file, possible updated since last mission
  void ReloadConfig();

  // Call the VRPN callback's on the devices
  // on received new input data
  void MainLoop();
  // Simulate function called when VBS2 world needs to be modified
  void Simulate(float deltaT);
  // Console output for debug mode
  void Console(float deltaT);
  // Calibrate the sensors
  void Calibrate();
  // Return the sensor config so the extended system can be used
  SensorsCFG *GetSensor(int pos);
  // Return the number of configured sensors
  int NSensors();
};

extern SensorManager GSensorManager;

#endif