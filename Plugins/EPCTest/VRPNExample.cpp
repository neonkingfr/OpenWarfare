
#include <windows.h>
#include "VRPNExample.h"

static float rotController = 0.0f;

// Command function definition
// Function that will register the ExecuteCommand function of the engine
ExecuteCommandType ExecuteCommand = NULL;
int ExecuteCmd(const char* command, char *result = NULL, int resultLength = 0)
{
  if (!ExecuteCommand) return 0;
  return ExecuteCommand(command, result, resultLength);
}

VBSPLUGIN_EXPORT void WINAPI RegisterCommandFnc(void *executeCommandFnc)
{
  ExecuteCommand = (ExecuteCommandType)executeCommandFnc;
}

// Function that will register the ExecuteCommand function of the engine
VBSPLUGIN_EXPORT void WINAPI RegisterHWND(void *hwnd, void *hins)
{
}

#define rad2Deg (180.0f/3.141592653589)

// This function will be executed every simulation step (every frame) and took a part in the simulation procedure.
// We can be sure in this function the ExecuteCommand registering was already done.
// deltaT is time in seconds since the last simulation step
VBSPLUGIN_EXPORT void WINAPI OnSimulationStep(float deltaT)
{
  rotController+=0.03f;
}

// This function will be executed every time the script in the engine calls the script function "pluginFunction"
// We can be sure in this function the ExecuteCommand registering was already done.
// Note that the plugin takes responsibility for allocating and deleting the returned string
VBSPLUGIN_EXPORT const char* WINAPI PluginFunction(const char *input)
{
  static const char result[]="EPC Test Plugin";

  if (!stricmp(input, "e"))
  {
    ExecuteCmd("player setExternalPose true");
    ExecuteCmd("player setExternalPoseSkeleton true");
  }
  else if (!stricmp(input, "t"))
  {
    ExecuteCmd("player setExternalPose true");
    ExecuteCmd("player setExternalPoseUpBody true");
    ExecuteCmd("player setExternalPoseSkeleton true");
  }
  else if (!stricmp(input, "d"))
  {
    ExecuteCmd("player setExternalPose false");
    ExecuteCmd("player setExternalPoseSkeleton false");
  }

  return result;
}

/*
position x axis (right-left)
position y axis (closer-further)
position z axis (down-up)
*/
#ifdef VBS2NG
VBSPLUGIN_EXPORT void WINAPI OnModifyBone(Matrix4DP& mat, int skeletonIndex)
#else
VBSPLUGIN_EXPORT void WINAPI OnModifyBone(Matrix4& mat, int skeletonIndex)
#endif
{
  mat = BI_IdentityMatrix4;
#if VBS2NG
  BI_TVector3<double> transl = mat._position; 
#else
  BI_TVector3<float> transl = mat.GetPos(); 
#endif
  transl[1] += 1.014f;
  mat._position = transl;

  if ( (skeletonIndex == eHEAD))
  {
    BI_TVector3<float> dir = mat.GetDirection();
    BI_TVector3<float> up = mat.GetUp();
    dir.SetX(dir.X() + sin(rotController));
    mat.SetDirectionAndUp(dir, up);
  }
}

VBSPLUGIN_EXPORT void WINAPI OnUnload (void)
{
  ExecuteCmd("player setExternalPose false");
  ExecuteCmd("player setExternalPoseSkeleton false");
}

// DllMain
BOOL WINAPI DllMain(HINSTANCE hDll, DWORD fdwReason, LPVOID lpvReserved)
{
  switch(fdwReason)
  {
  case DLL_PROCESS_ATTACH:
    {
      OutputDebugString("EPCTest Called DllMain with DLL_PROCESS_ATTACH\n");
      break;
    }
  case DLL_PROCESS_DETACH:
    OutputDebugString("EPCTest Called DllMain with DLL_PROCESS_DETACH\n");
    break;
  case DLL_THREAD_ATTACH:
    OutputDebugString("EPCTest Called DllMain with DLL_THREAD_ATTACH\n");
    break;
  case DLL_THREAD_DETACH:
    OutputDebugString("EPCTest Called DllMain with DLL_THREAD_DETACH\n");
    break;
  }
  return TRUE;
}
