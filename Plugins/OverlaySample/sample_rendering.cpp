// Sample rendering using d3dx library

#include <d3d9.h>
#include <d3dx9.h>

extern IDirect3DDevice9 *d3dd;  // in main.cpp

ID3DXFont *font = NULL;
ID3DXLine *line = NULL;

bool active = false; 

// Display width and height
int width = 0;
int height = 0;

// Creates font used for sample rendering - called when device is created or after reset
void render_sample_create()
{
  // Create d3dx font and line interfaces
  D3DXCreateFont(d3dd, 32, 0, FW_NORMAL, 1, FALSE, DEFAULT_CHARSET, OUT_DEFAULT_PRECIS, PROOF_QUALITY, DEFAULT_PITCH | FF_DONTCARE, "arial", &font);
  D3DXCreateLine(d3dd, &line);

  // Get screen width (Direct3D device backbuffer size)
  IDirect3DSurface9 *bb = NULL;
  d3dd->GetBackBuffer(0, 0, D3DBACKBUFFER_TYPE_MONO, &bb);
  if(bb)
  {
    D3DSURFACE_DESC d;
    bb->GetDesc(&d);
    width = d.Width;
    height = d.Height;
  }
  bb->Release();

  active = true;
}

// Release font object - called when device is reset or destroyed
void render_sample_release()
{
  if(font)
    font->Release();
  if(line)
    line->Release();
  font = NULL;
  line = NULL;
  active = false;
}

// Draw sample text on screen
void render_sample_text()
{
  if(!active)
    return;

  // Draw some lines
  line->SetPattern(0xffffffff); 
  line->SetWidth(5);  

  D3DXVECTOR2 p[4]= {
    D3DXVECTOR2(width/2.0f, height/3.0f),
    D3DXVECTOR2(width/3.0f, height/3.0f*2.0f),
    D3DXVECTOR2(width/3.0f*2.0f, height/3.0f*2.0f),
    D3DXVECTOR2(width/2.0f, height/3.0f),
  };  
  line->Draw(p, 4, D3DCOLOR_ARGB(255, 32, 32, 255)); 

  // Draw some text
  RECT r; 
  r.top = 0;
  r.left = 0;
  r.bottom = height;
  r.right = width;

  font->DrawText(NULL, "Hello World", -1, &r, DT_CENTER|DT_VCENTER, D3DCOLOR_ARGB(255, 255, 255, 255));

}