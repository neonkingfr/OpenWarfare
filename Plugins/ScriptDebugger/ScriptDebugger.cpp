#include <windows.h>
#include "ScriptDebugger.h"
#include "..\..\Common\iDebugEngineInterface.hpp"


// Command function declaration
typedef int (WINAPI * ExecuteCommandType)(const char *command, char *result, int resultLength);

// Command function definition
ExecuteCommandType ExecuteCommand = NULL;

// Function that will register the ExecuteCommand function of the engine
VBSPLUGIN_EXPORT void WINAPI RegisterHWND(void *hwnd, void *hins)
{
}

// Function that will register the ExecuteCommand function of the engine
VBSPLUGIN_EXPORT void WINAPI RegisterCommandFnc(void *executeCommandFnc)
{
  ExecuteCommand = (ExecuteCommandType)executeCommandFnc;
}

// This function will be executed after every in-game object simulation is run
// deltaT is time in seconds since the last simulation step, please note. Because this is run
// after all vehicles simulations deltaT is effected via the in-game time multiplier.
VBSPLUGIN_EXPORT void WINAPI OnAfterSimulation(float deltaT)
{
}

// This function will be executed every simulation step (every frame) and took a part in the simulation procedure.
// We can be sure in this function the ExecuteCommand registering was already done.
// deltaT is time in seconds since the last simulation step
VBSPLUGIN_EXPORT void WINAPI OnSimulationStep(float deltaT)
{
}

// This function will be executed every time the script in the engine calls the script function "pluginFunction"
// We can be sure in this function the ExecuteCommand registering was already done.
// Note that the plugin takes responsibility for allocating and deleting the returned string
VBSPLUGIN_EXPORT const char* WINAPI PluginFunction(const char *input)
{
  return NULL;
}

// DebugScriptInterface, is the implementation that will be called from
// the engine when connecting to the plugin with connect.
class DebugScriptInterface : public IToDll
{
public:
  DebugScriptInterface() {}
  ~DebugScriptInterface() {}

  virtual void Startup(){}
  virtual void Shutdown(){}

  virtual void ScriptLoaded(IDebugScript *script, const char *name){}
  virtual void ScriptEntered(IDebugScript *script){}
  virtual void ScriptTerminated(IDebugScript *script){}

  virtual void FireBreakpoint(IDebugScript *script, unsigned int bp) {} 
  virtual void Breaked(IDebugScript *script){}
  virtual void DebugEngineLog(const char *str){}

  virtual void OnError(int error, const char *content,const char *errorMsg, int line, const IGameState *gamestate){}

  virtual void GlobalGameState(IGameState *gameState){}
};

// Simple debug interface setup to test calling
DebugScriptInterface GScriptInterface;

// VBS2 connecting to us with the interface to dll
VBSPLUGIN_EXPORT IToDll *WINAPI ConnectToScriptDebugger(int abcVersion)
{
  return &GScriptInterface;
}

// DllMain
BOOL WINAPI DllMain(HINSTANCE hDll, DWORD fdwReason, LPVOID lpvReserved)
{
   switch(fdwReason)
   {
      case DLL_PROCESS_ATTACH:
         OutputDebugString("Called DllMain with DLL_PROCESS_ATTACH\n");
         break;
      case DLL_PROCESS_DETACH:
         OutputDebugString("Called DllMain with DLL_PROCESS_DETACH\n");
         break;
      case DLL_THREAD_ATTACH:
         OutputDebugString("Called DllMain with DLL_THREAD_ATTACH\n");
         break;
      case DLL_THREAD_DETACH:
         OutputDebugString("Called DllMain with DLL_THREAD_DETACH\n");
         break;
   }
   return TRUE;
}
