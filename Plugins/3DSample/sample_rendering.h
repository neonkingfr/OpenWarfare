#ifndef __SAMPLE_RENDERING_H__
#define __SAMPLE_RENDERING_H__

#include "vbsplugin.h"

void render_sample_create();
void render_sample_release();
void render_sample_model();
void render_sample_set_frustum(FrustumSpec *frustum);
void render_sample_progress_time(float t);
void render_sample_set_clipplanes(float zNear, float zFar);

#endif