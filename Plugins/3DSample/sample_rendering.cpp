// Sample rendering using d3dx library

#include <d3d9.h>
#include <d3dx9.h>
#include <stdio.h>

#include "sample_rendering.h"

static void setDefaultState();

extern IDirect3DDevice9 *d3dd;  // in main.cpp
extern ExecuteCommandType ExecuteCommand;   // in main.cpp

bool active = false; 
bool hasObjects = false;

ID3DXMesh *box; // Box model
ID3DXMesh *cylinder; // Sphere model
FrustumSpec frustum;  // Camera frustum data
float timeBase = 0; // Current base time, for animation

// Display width and height
int width = 0;
int height = 0;

// Defines an object in world
typedef struct {
  bool valid;
  float x,y,z;  // Position
  float spinRate;
  D3DMATERIAL9 mat;
  ID3DXMesh *mesh;
} OBJECT;

#define NUM_OBJECTS 40 // How many objects are created

OBJECT objects[NUM_OBJECTS];

// Creates meshes for sample rendering - called when device is created or after reset
void render_sample_create()
{
  // Create some simple meshes
  D3DXCreateBox(d3dd, 0.5, 0.5, 0.5, &box, NULL);
  D3DXCreateCylinder(d3dd, 0.5, 0.5, 1, 24, 24, &cylinder, NULL);

  active = true;
  hasObjects = false;
}

// Release D3D data - called when device is reset or destroyed
void render_sample_release()
{
  box->Release();
  cylinder->Release();
  active = false;
}

// Store camera frustum data
void render_sample_set_frustum(FrustumSpec *fd)
{
  memcpy(&frustum, fd, sizeof(FrustumSpec));
}

// Increase time
void render_sample_progress_time(float t)
{
  timeBase += t;
}

void render_sample_set_clipplanes(float zNear, float zFar)
{
  frustum._clipDistNear = zNear;
  frustum._clipDistFar = zFar;
}

// Create some objects around player
static void createObjects()
{
  // Get player position from simulation
  char res[256];
  ExecuteCommand("GetPosAsl player", res, 256);

  // Parse result, it is in form "[x,y,z]" with Z up
  float plrX, plrY, plrZ;
  sscanf(res, "[%f,%f,%f]", &plrX, &plrZ, &plrY); // For us, Y is up
  if(plrX+plrY+plrZ == 0)
    return; // Player position is zero - mission is not yet running properly

  //LogF("Player position: %f %f %f", plrX, plrY, plrZ);
  
  // Create a bunch of objects around player
  for(int i=0;i<NUM_OBJECTS;i++)
  {
    OBJECT *o = &objects[i];

    // Random color for material
    ZeroMemory(&o->mat, sizeof(o->mat));
    D3DXCOLOR c = D3DXCOLOR((float)rand()/RAND_MAX, (float)rand()/RAND_MAX, (float)rand()/RAND_MAX, 1.0f);
    o->mat.Diffuse = c;
    o->mat.Ambient = c;

    // Randomize position around player
    const int area = 30;  
    o->x = plrX-((float)area/2.0f)+(rand()%area);
    o->z = plrZ-((float)area/2.0f)+(rand()%area);
    o->y = plrY+0.5f;

    o->spinRate = -4 + (((float)rand()/RAND_MAX) * 8);

    // Random mesh
    o->mesh = rand()%2==0 ? box : cylinder;
    o->valid = true;
  }

  hasObjects = true;
}

// Draw sample text on screen
void render_sample_model()
{
  if(!active)
    return;

  // If objects have not yet been created, attempt to do so now
  if(!hasObjects)
    createObjects();

  // If objects were not created, return. Means player position was not yet initialized
  if(!hasObjects)
    return;

  // Store current device state
  IDirect3DStateBlock9 *sblock;
  if(d3dd->CreateStateBlock(D3DSBT_ALL, &sblock) != D3D_OK)
    return;
  sblock->Capture();

  // Set device to default state
  setDefaultState();

  // Create projection matrix from engine frustum data
  D3DXMATRIX proj;
  float m = frustum._clipDistNear;
  float left = frustum._projTanLeft * m;
  float right = frustum._projTanRight * m;
  float bottom = frustum._projTanBottom * m;
  float top = frustum._projTanTop * m;  
  D3DXMatrixPerspectiveOffCenterLH(&proj, -left, right, -bottom, top, frustum._clipDistNear, frustum._clipDistFar);
  d3dd->SetTransform(D3DTS_PROJECTION, &proj);

  // Create view matrix from engine camera data
  D3DXMATRIX view;  
  D3DXVECTOR3 cpoint = D3DXVECTOR3(frustum._pointPosX, frustum._pointPosY, frustum._pointPosZ);
  D3DXVECTOR3 ctarget = cpoint + D3DXVECTOR3(frustum._viewDirX, frustum._viewDirY, frustum._viewDirZ); // Convert camera direction vector to look-at-target
  D3DXVECTOR3 cup = D3DXVECTOR3(frustum._viewUpX, frustum._viewUpY, frustum._viewUpZ);
  D3DXMatrixLookAtLH(&view, &cpoint, &ctarget, &cup);
  d3dd->SetTransform(D3DTS_VIEW, &view);

  // Set a basic light source
  D3DLIGHT9 light;
  ZeroMemory(&light, sizeof(light));
  light.Type = D3DLIGHT_DIRECTIONAL;
  light.Diffuse = D3DXCOLOR(0.5f, 0.5f, 0.5f, 1.0f);
  light.Direction = D3DXVECTOR3(-0.5, -1, -0.5);
  d3dd->SetLight(0, &light);
  d3dd->LightEnable(0, TRUE);

  // Prepare for drawing
  d3dd->SetRenderState(D3DRS_AMBIENT, D3DCOLOR_XRGB(128, 128, 128));
  d3dd->SetRenderState(D3DRS_LIGHTING, TRUE);
  d3dd->SetRenderState(D3DRS_ZENABLE, TRUE);
  d3dd->SetVertexShader(NULL);
  d3dd->SetPixelShader(NULL);  
  d3dd->SetTexture(0, 0);

  // Draw objects randomized earlier
  // Note: Scene is already "open" by the engine at this point so no need to call BeginScene (and EndScene should not be called at end)
  for(int i=0;i<NUM_OBJECTS;i++)
  {
    OBJECT *o = &objects[i];
    if(!o->valid)
      continue;

    // Position in world - Y axis is up
    D3DXMATRIX world, pos, rot;
    D3DXMatrixTranslation(&pos, o->x, o->y, o->z);
    D3DXMatrixRotationAxis(&rot, &D3DXVECTOR3(0,1,0), timeBase*o->spinRate);  // Spin the models around based on simulation time
    world = rot * pos;
    d3dd->SetTransform(D3DTS_WORLD, &world);

    // Draw mesh
    d3dd->SetMaterial(&o->mat);
    d3dd->SetFVF(o->mesh->GetFVF());
    o->mesh->DrawSubset(0);
  }

  // Restore device state so we dont interfere with engine rendering
  sblock->Apply();
  sblock->Release();
}

static void setDefaultState()
{
  // Set D3D device properties to their default state
  // This is to ensure things are in a predictable state when coming from engine rendering code
  // TODO: This would be more efficient if it would be captured in a state block
  float zerof = 0.0f;
  float onef = 1.0f;
  #define ZEROf	*((DWORD*) (&zerof))
  #define ONEf	*((DWORD*) (&onef))

  d3dd->SetRenderState(D3DRS_ZENABLE, D3DZB_FALSE);
  d3dd->SetRenderState(D3DRS_FILLMODE, D3DFILL_SOLID);
  d3dd->SetRenderState(D3DRS_SHADEMODE, D3DSHADE_GOURAUD);
  d3dd->SetRenderState(D3DRS_ZWRITEENABLE, TRUE);
  d3dd->SetRenderState(D3DRS_ALPHATESTENABLE, FALSE);
  d3dd->SetRenderState(D3DRS_LASTPIXEL, TRUE);
  d3dd->SetRenderState(D3DRS_SRCBLEND, D3DBLEND_ONE);
  d3dd->SetRenderState(D3DRS_DESTBLEND, D3DBLEND_ZERO);
  d3dd->SetRenderState(D3DRS_CULLMODE, D3DCULL_CCW);
  d3dd->SetRenderState(D3DRS_ZFUNC, D3DCMP_LESSEQUAL);
  d3dd->SetRenderState(D3DRS_ALPHAREF, 0);
  d3dd->SetRenderState(D3DRS_ALPHAFUNC, D3DCMP_ALWAYS);
  d3dd->SetRenderState(D3DRS_ALPHABLENDENABLE, FALSE);
  d3dd->SetRenderState(D3DRS_FOGENABLE, FALSE);
  d3dd->SetRenderState(D3DRS_SPECULARENABLE, FALSE);
  d3dd->SetRenderState(D3DRS_FOGCOLOR, 0);
  d3dd->SetRenderState(D3DRS_FOGTABLEMODE, D3DFOG_NONE);
  d3dd->SetRenderState(D3DRS_FOGSTART, ZEROf);
  d3dd->SetRenderState(D3DRS_FOGEND, ONEf);
  d3dd->SetRenderState(D3DRS_FOGDENSITY, ONEf);
  d3dd->SetRenderState(D3DRS_RANGEFOGENABLE, FALSE);
  d3dd->SetRenderState(D3DRS_STENCILENABLE, FALSE);
  d3dd->SetRenderState(D3DRS_STENCILFAIL, D3DSTENCILOP_KEEP);
  d3dd->SetRenderState(D3DRS_STENCILZFAIL, D3DSTENCILOP_KEEP);
  d3dd->SetRenderState(D3DRS_STENCILPASS, D3DSTENCILOP_KEEP);
  d3dd->SetRenderState(D3DRS_STENCILFUNC, D3DCMP_ALWAYS);
  d3dd->SetRenderState(D3DRS_STENCILREF, 0);
  d3dd->SetRenderState(D3DRS_STENCILMASK, 0xffffffff);
  d3dd->SetRenderState(D3DRS_STENCILWRITEMASK, 0xffffffff);
  d3dd->SetRenderState(D3DRS_TEXTUREFACTOR, 0xffffffff);
  d3dd->SetRenderState(D3DRS_WRAP0, 0);
  d3dd->SetRenderState(D3DRS_WRAP1, 0);
  d3dd->SetRenderState(D3DRS_WRAP2, 0);
  d3dd->SetRenderState(D3DRS_WRAP3, 0);
  d3dd->SetRenderState(D3DRS_WRAP4, 0);
  d3dd->SetRenderState(D3DRS_WRAP5, 0);
  d3dd->SetRenderState(D3DRS_WRAP6, 0);
  d3dd->SetRenderState(D3DRS_WRAP7, 0);
  d3dd->SetRenderState(D3DRS_CLIPPING, TRUE);
  d3dd->SetRenderState(D3DRS_AMBIENT, 0);
  d3dd->SetRenderState(D3DRS_LIGHTING, TRUE);
  d3dd->SetRenderState(D3DRS_FOGVERTEXMODE, D3DFOG_NONE);
  d3dd->SetRenderState(D3DRS_COLORVERTEX, TRUE);
  d3dd->SetRenderState(D3DRS_LOCALVIEWER, TRUE);
  d3dd->SetRenderState(D3DRS_NORMALIZENORMALS, FALSE);
  d3dd->SetRenderState(D3DRS_DIFFUSEMATERIALSOURCE, D3DMCS_COLOR1);
  d3dd->SetRenderState(D3DRS_SPECULARMATERIALSOURCE, D3DMCS_COLOR2);
  d3dd->SetRenderState(D3DRS_AMBIENTMATERIALSOURCE, D3DMCS_MATERIAL);
  d3dd->SetRenderState(D3DRS_EMISSIVEMATERIALSOURCE, D3DMCS_MATERIAL);
  d3dd->SetRenderState(D3DRS_CLIPPLANEENABLE, 0);
  d3dd->SetRenderState(D3DRS_POINTSIZE_MIN, ONEf);
  d3dd->SetRenderState(D3DRS_POINTSPRITEENABLE, FALSE);
  d3dd->SetRenderState(D3DRS_MULTISAMPLEANTIALIAS, TRUE);
  d3dd->SetRenderState(D3DRS_MULTISAMPLEMASK, 0xffffffff);
  d3dd->SetRenderState(D3DRS_PATCHEDGESTYLE, D3DPATCHEDGE_DISCRETE);
  d3dd->SetRenderState(D3DRS_POINTSIZE_MAX, ONEf);
  d3dd->SetRenderState(D3DRS_COLORWRITEENABLE, 0x0000000f);
  d3dd->SetRenderState(D3DRS_BLENDOP, D3DBLENDOP_ADD);
  d3dd->SetRenderState(D3DRS_POSITIONDEGREE, D3DDEGREE_CUBIC);
  d3dd->SetRenderState(D3DRS_NORMALDEGREE, D3DDEGREE_LINEAR);
  d3dd->SetRenderState(D3DRS_SCISSORTESTENABLE, FALSE);
  d3dd->SetRenderState(D3DRS_SLOPESCALEDEPTHBIAS, 0);
  d3dd->SetRenderState(D3DRS_MINTESSELLATIONLEVEL, ONEf);
  d3dd->SetRenderState(D3DRS_MAXTESSELLATIONLEVEL, ONEf);
  d3dd->SetRenderState(D3DRS_ADAPTIVETESS_X, ZEROf);
  d3dd->SetRenderState(D3DRS_ADAPTIVETESS_Y, ZEROf);
  d3dd->SetRenderState(D3DRS_ADAPTIVETESS_Z, ONEf);
  d3dd->SetRenderState(D3DRS_ADAPTIVETESS_W, ZEROf);
  d3dd->SetRenderState(D3DRS_ENABLEADAPTIVETESSELLATION, FALSE);
  d3dd->SetRenderState(D3DRS_TWOSIDEDSTENCILMODE, FALSE);
  d3dd->SetRenderState(D3DRS_CCW_STENCILFAIL, D3DSTENCILOP_KEEP);
  d3dd->SetRenderState(D3DRS_CCW_STENCILZFAIL, D3DSTENCILOP_KEEP);
  d3dd->SetRenderState(D3DRS_CCW_STENCILPASS, D3DSTENCILOP_KEEP);
  d3dd->SetRenderState(D3DRS_CCW_STENCILFUNC, D3DCMP_ALWAYS);
  d3dd->SetRenderState(D3DRS_COLORWRITEENABLE1, 0x0000000f);
  d3dd->SetRenderState(D3DRS_COLORWRITEENABLE2, 0x0000000f);
  d3dd->SetRenderState(D3DRS_COLORWRITEENABLE3, 0x0000000f);
  d3dd->SetRenderState(D3DRS_BLENDFACTOR, 0xffffffff);
  d3dd->SetRenderState(D3DRS_SRGBWRITEENABLE, 0);
  d3dd->SetRenderState(D3DRS_DEPTHBIAS, 0);
  d3dd->SetRenderState(D3DRS_WRAP8, 0);
  d3dd->SetRenderState(D3DRS_WRAP9, 0);
  d3dd->SetRenderState(D3DRS_WRAP10, 0);
  d3dd->SetRenderState(D3DRS_WRAP11, 0);
  d3dd->SetRenderState(D3DRS_WRAP12, 0);
  d3dd->SetRenderState(D3DRS_WRAP13, 0);
  d3dd->SetRenderState(D3DRS_WRAP14, 0);
  d3dd->SetRenderState(D3DRS_WRAP15, 0);
  d3dd->SetRenderState(D3DRS_SEPARATEALPHABLENDENABLE, FALSE);
  d3dd->SetRenderState(D3DRS_SRCBLENDALPHA, D3DBLEND_ONE);
  d3dd->SetRenderState(D3DRS_DESTBLENDALPHA, D3DBLEND_ZERO);
  d3dd->SetRenderState(D3DRS_BLENDOPALPHA, D3DBLENDOP_ADD);
  d3dd->SetRenderState(D3DRS_DITHERENABLE, FALSE);
  d3dd->SetRenderState(D3DRS_VERTEXBLEND, D3DVBF_DISABLE);
  d3dd->SetRenderState(D3DRS_POINTSIZE, ONEf);
  d3dd->SetRenderState(D3DRS_POINTSCALEENABLE, FALSE);
  d3dd->SetRenderState(D3DRS_POINTSCALE_A, ONEf);
  d3dd->SetRenderState(D3DRS_POINTSCALE_B, ZEROf);
  d3dd->SetRenderState(D3DRS_POINTSCALE_C, ZEROf);
  d3dd->SetRenderState(D3DRS_INDEXEDVERTEXBLENDENABLE, FALSE);
  d3dd->SetRenderState(D3DRS_TWEENFACTOR, ZEROf);
  d3dd->SetRenderState(D3DRS_ANTIALIASEDLINEENABLE, FALSE);

  for(int i=0;i<4;i++) {
	  d3dd->SetSamplerState(i, D3DSAMP_MINFILTER, D3DTEXF_LINEAR);
	  d3dd->SetSamplerState(i, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR);
	  d3dd->SetSamplerState(i, D3DSAMP_MIPFILTER, D3DTEXF_NONE);
	  d3dd->SetSamplerState(i, D3DSAMP_MIPMAPLODBIAS, 0);
	  d3dd->SetSamplerState(i, D3DSAMP_MAXMIPLEVEL, 0);			
	  d3dd->SetSamplerState(i, D3DSAMP_MAXANISOTROPY, 1);
	  d3dd->SetSamplerState(i, D3DSAMP_SRGBTEXTURE, 0);			
	  d3dd->SetSamplerState(i, D3DSAMP_ELEMENTINDEX, 0);
	  d3dd->SetSamplerState(i, D3DSAMP_DMAPOFFSET, 0);			
	  d3dd->SetSamplerState(i, D3DSAMP_ADDRESSU, D3DTADDRESS_WRAP);
	  d3dd->SetSamplerState(i, D3DSAMP_ADDRESSV, D3DTADDRESS_WRAP);
	  d3dd->SetSamplerState(i, D3DSAMP_ADDRESSW, D3DTADDRESS_WRAP);		
  }

  for(int i=0;i<4;i++) {
	  d3dd->SetTextureStageState(i, D3DTSS_TEXCOORDINDEX, i);
	  d3dd->SetTextureStageState(i, D3DTSS_COLOROP, i==0?D3DTOP_MODULATE:D3DTOP_DISABLE);
	  d3dd->SetTextureStageState(i, D3DTSS_COLORARG1, D3DTA_TEXTURE);
	  d3dd->SetTextureStageState(i, D3DTSS_COLORARG2, D3DTA_CURRENT);
	  d3dd->SetTextureStageState(i, D3DTSS_ALPHAOP, i==0?D3DTOP_SELECTARG1:D3DTOP_DISABLE);
	  d3dd->SetTextureStageState(i, D3DTSS_ALPHAARG1, D3DTA_TEXTURE);
	  d3dd->SetTextureStageState(i, D3DTSS_ALPHAARG2, D3DTA_CURRENT);
	  d3dd->SetTextureStageState(i, D3DTSS_BUMPENVMAT00, ZEROf);
	  d3dd->SetTextureStageState(i, D3DTSS_BUMPENVMAT01, ZEROf);
	  d3dd->SetTextureStageState(i, D3DTSS_BUMPENVMAT10, ZEROf);
	  d3dd->SetTextureStageState(i, D3DTSS_BUMPENVMAT11, ZEROf);
	  d3dd->SetTextureStageState(i, D3DTSS_BUMPENVLSCALE, ZEROf);
	  d3dd->SetTextureStageState(i, D3DTSS_BUMPENVLOFFSET, ZEROf);
	  d3dd->SetTextureStageState(i, D3DTSS_COLORARG0, D3DTA_CURRENT);
	  d3dd->SetTextureStageState(i, D3DTSS_ALPHAARG0, D3DTA_CURRENT);
	  d3dd->SetTextureStageState(i, D3DTSS_RESULTARG, D3DTA_CURRENT);
	  d3dd->SetTextureStageState(i, D3DTSS_CONSTANT, 0);
	  d3dd->SetTextureStageState(i, D3DTSS_TEXTURETRANSFORMFLAGS, D3DTTFF_DISABLE);
  }
  	
  D3DXMATRIX mat;
  D3DXMatrixIdentity(&mat);
  d3dd->SetTransform(D3DTS_TEXTURE0, &mat);
  d3dd->SetTransform(D3DTS_TEXTURE1, &mat);
  d3dd->SetTransform(D3DTS_TEXTURE2, &mat);
  d3dd->SetTransform(D3DTS_TEXTURE3, &mat);
}