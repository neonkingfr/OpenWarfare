#include <windows.h>
#include "vbsplugin.h"
#include "../../Common/Essential/AppFrameWork.hpp"
#include "sample_rendering.h"

IDirect3DDevice9 *d3dd = NULL;  // Holds the d3d device created by VBS2
ExecuteCommandType ExecuteCommand = NULL;

VBSPLUGIN_EXPORT void WINAPI OnD3DeviceRestore();
VBSPLUGIN_EXPORT void WINAPI OnD3DeviceInvalidate();

// Onload - get the d3d device
VBSPLUGIN_EXPORT int WINAPI OnLoad(VBSParameters *params) 
{
  OutputDebugString("OnLoad");
  if (params)
  {
    if (params->_device)
    {
      d3dd = params->_device;

      // Call OnD3DeviceRestore - our Direct3D data is created there
      OnD3DeviceRestore();
    }
  }
  return 0;
}


VBSPLUGIN_EXPORT void WINAPI OnUnload() 
{
  OutputDebugString("OnUnload");

  // Call OnD3DeviceInvalidate - to release Direct3D data
  OnD3DeviceInvalidate();
}


// D3D device management, setting the engine D3D device
VBSPLUGIN_EXPORT void WINAPI OnD3DeviceInit(IDirect3DDevice9 *device)
{
  OutputDebugString("OnD3DeviceInit");
  d3dd = device;
}

// D3D device management, removing the engine D3D device: 
VBSPLUGIN_EXPORT void WINAPI OnD3DeviceDelete()
{
  OutputDebugString("OnD3DeviceDelete");
}


// D3D device management, before a reset
// All handles created on the Direct3D device must be freed here - otherwise VBS2 device reset will fail
VBSPLUGIN_EXPORT void WINAPI OnD3DeviceInvalidate()
{
  OutputDebugString("OnD3DeviceInvalidate");
  render_sample_release();
}

// D3D device management, after a reset
// Re-create data for Direct3D device here
VBSPLUGIN_EXPORT void WINAPI OnD3DeviceRestore()
{
  OutputDebugString("OnD3DeviceRestore");
  render_sample_create();
}

VBSPLUGIN_EXPORT void WINAPI OnDrawnPassTwo(float zNear, float zFar)
{
  // Update zNear and zFar - they may have been changed by floating near plane
  render_sample_set_clipplanes(zNear, zFar);

  // Render the models
  render_sample_model();
};

bool set;
float _projTanTop;
float _projTanBottom;
float _projTanLeft;
float _projTanRight;

VBSPLUGIN_EXPORT void WINAPI OnSetFrustum(FrustumSpec *frustum)
{
  // Here we have an opportunity to change the near clip plane by changing the value of frustum->clipNear_feedback
  // By default the engine will try to move the near clip plane as far as possible, but it cannot consider primitives
  // that are drawn by plugins. Therefore if plugins need to draw primitives very close to the camera, they need to
  // tell here how close they need the near clip plane to be set.

  // It is very important to not set the near clip plane too close, ie. you need to actually determine how close the nearest
  // primitive you are going to draw is and only set the clip to that distance. Simply setting a very low near clip plane
  // at all times will result in severe depth precision issues for the whole scene.  
  
  
  // Set frustum data
  if (!set)
  {
    set = true;
    _projTanTop= frustum->_projTanTop;
    _projTanBottom= frustum->_projTanBottom;
    _projTanLeft= frustum->_projTanLeft;
    _projTanRight=  frustum->_projTanRight;
  }
  frustum->_projTanTop= _projTanTop;
  frustum->_projTanBottom= _projTanBottom;
  frustum->_projTanLeft= _projTanLeft;
  frustum->_projTanRight= _projTanRight;
  render_sample_set_frustum(frustum);
};

// This function will be executed every simulation step (every frame) and took a part in the simulation procedure.
// We can be sure in this function the ExecuteCommand registering was already done.
// deltaT is time in seconds since the last simulation step
VBSPLUGIN_EXPORT void WINAPI OnSimulationStep(float deltaT) {
  // Tell sample renderer about time progress
  render_sample_progress_time(deltaT);
}

VBSPLUGIN_EXPORT const char* WINAPI PluginFunction(const char *inp)
{
  static const char resultDone[]="[true]";
  return resultDone;
}

BOOL WINAPI DllMain(HINSTANCE hDll, DWORD fdwReason, LPVOID lpvReserved)
{
   switch(fdwReason)
   {
      case DLL_PROCESS_ATTACH:
        OutputDebugString("DLL_PROCESS_ATTACH");
        break;
      case DLL_PROCESS_DETACH:
        OutputDebugString("DLL_PROCESS_DETACH");
        break;
   }
   return TRUE;
}


VBSPLUGIN_EXPORT void WINAPI RegisterHWND(void *hwnd, void *hins) {}

VBSPLUGIN_EXPORT void WINAPI RegisterCommandFnc(void *executeCommandFnc)
{
  ExecuteCommand = (ExecuteCommandType)executeCommandFnc;
}