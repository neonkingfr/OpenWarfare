#include "Shapes.h"
#include <string.h>

class IRoadTemplate
{
public:  
  virtual float ReadValue(const char* valueName, float defaultValue) const = 0;
  virtual int ReadValue(const char* valueName, int defaultValue) const = 0;
  virtual void ReadValue(char* result, const char* valueName, const char* defaultValue) const = 0;
  virtual bool ReadValue(const char* valueName, bool defaultValue) const = 0;
};


class IRoadNetwork
{
  public:
  struct RoadVectorData
  {
    float x,z;
  };

  virtual int AddRoadNode(const RoadVectorData& position, int nConnections, const bool* connectionMatrix) = 0;
  virtual void AddRoadSegment(int nodeFrom, int nodeTo, const IRoadTemplate* roadTemplate, int nPoints, const RoadVectorData* points) = 0;
  
  IRoadNetwork(void);
  ~IRoadNetwork(void);
};

class ShapesManagement : public IRoadTemplate
{
private:
  ShapeOrganizer *shape;
  unsigned int idShape;

public:
   ShapesManagement(ShapeOrganizer *shapeOrg) : 
     shape(shapeOrg),
     idShape(0){};

   ~ShapesManagement()
   {
     shape = NULL;
   }

  float ReadValue (const char* valueName, float defaultValue) const;
  int ReadValue (const char* valueName, int defaultValue) const;
  void ReadValue(char* result, const char* valueName, const char* defaultValue) const;
  bool ReadValue(const char* valueName, bool defaultValue) const;

  const std::vector<points> GetVectorPoints() {return shape->GetVectorPoints();}
  int GetShapesListSize () {return shape->GetListOfShapes().size();}
  const IShape * GetShape(int id) {return shape->GetListOfShapes().at(id);}
  void SetId(unsigned int id) {idShape = id;}
  const ShapeOrganizer* GetShapeOrganizer () {return shape;}

};