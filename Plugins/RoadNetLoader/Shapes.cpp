#include "Shapes.h"
#include "float.h"
#include <algorithm>

ShapeLayer::ShapeLayer()
{
  _shapeOrg = NULL;
}
ShapeLayer::~ShapeLayer()
{if (_shapeOrg) delete _shapeOrg;}
ShapeOrganizer::ShapeOrganizer()
{}
ShapeOrganizer::~ShapeOrganizer()
{
  for (int i = 0; i < _listOfShapes.size(); i++)
    DeleteShape(i);
  _listOfShapes.clear();
  _vxList.clear();
}

template<class T>
bool ReadBig(std::ifstream& input, T& t)
{
  char* c = reinterpret_cast<char*>(&t);
  for (int i = sizeof(t) - 1; i >= 0; i--)
  {
    input.read(c + i, 1);
    if (input.fail()) return false;
  }
  return true;
}

template<class T>
bool ReadLittle(std::ifstream& input, T& t)
{
  char* c = reinterpret_cast<char*>(&t);
  input.read(c, sizeof(t));
  if (input.fail()) return false;
  return true;
}

static unsigned int AddVertex(std::vector<points> &vxVector, double x, double y, std::vector<unsigned int> *vxIndexes)
{
  points val;
  val.x = x;
  val.y = y;
  val.m = val.z = DBL_MIN;
  vxVector.push_back(val);
  return (unsigned int)vxVector.size()-1;
}

static ShapeFileReader::ShapeReaderError ReadShapeHeader(std::ifstream &input,ShapeFileReader::ShapeHeader &hdr)
{
  char unused[5*4];
  if (!ReadBig(input,hdr.fileCode)) return ShapeFileReader::sheReadError;
  input.read(unused,sizeof(unused));
  if (!ReadBig(input,hdr.fileLength)) return ShapeFileReader::sheReadError;
  if (!ReadLittle(input,hdr.version)) return ShapeFileReader::sheReadError;
  if (!ReadLittle(input,hdr.shapeType)) return ShapeFileReader::sheReadError;
  if (!ReadLittle(input,hdr.Xmin)) return ShapeFileReader::sheReadError;
  if (!ReadLittle(input,hdr.Ymin)) return ShapeFileReader::sheReadError;
  if (!ReadLittle(input,hdr.Xmax)) return ShapeFileReader::sheReadError;
  if (!ReadLittle(input,hdr.Ymax)) return ShapeFileReader::sheReadError;
  if (!ReadLittle(input,hdr.Zmin)) return ShapeFileReader::sheReadError;
  if (!ReadLittle(input,hdr.Zmax)) return ShapeFileReader::sheReadError;
  if (!ReadLittle(input,hdr.Mmin)) return ShapeFileReader::sheReadError;
  if (!ReadLittle(input,hdr.Mmax)) return ShapeFileReader::sheReadError;    
  return ShapeFileReader::sheOk;  
}

struct RecordHeader
{
  unsigned long recordNumber;
  unsigned long contentLength;
  unsigned long shapeType;
};

static bool ReadRecordHeader(std::ifstream &input, RecordHeader &hdr)
{
  if (!ReadBig(input,hdr.recordNumber)) return false;
  if (!ReadBig(input,hdr.contentLength)) return false;
  if (!ReadLittle(input,hdr.shapeType)) return false;
  return true;
}

static ShapeFileReader::ShapeReaderError ReadMOrZ(std::ifstream &input, std::vector<points> &vxArray, const std::vector<unsigned int> &vxIndexes, bool readZ, bool readRange)
{
  if (readRange)
  {
    double range[2];
    if (!ReadLittle(input,range)) return ShapeFileReader::sheReadError;
  }
  if (readZ) 
  {
    for (int i=0;i<vxIndexes.size();i++) if (vxIndexes[i]!=-1)
    {
      points vect=vxArray[vxIndexes[i]];
      if (!ReadLittle(input,vect.z)) return ShapeFileReader::sheReadError;
      vxArray[vxIndexes[i]] = vect;
    }
    else
    {
      double unused;
      if (!ReadLittle(input,unused)) return ShapeFileReader::sheReadError;
    }
  }
  else
    for (int i=0;i<vxIndexes.size();i++) if (vxIndexes[i]!=-1)
    {
      points vect=vxArray[vxIndexes[i]];
      if (!ReadLittle(input,vect.m)) return ShapeFileReader::sheReadError;
      vxArray[vxIndexes[i]] = vect;

    }
    else
    {
      double unused;
      if (!ReadLittle(input,unused)) return ShapeFileReader::sheReadError;
    }
    return ShapeFileReader::sheOk;
}

static ShapeFileReader::ShapeReaderError ReadPoint(std::ifstream &input, std::vector<points> &vxArray, ShapePoint &pt, std::vector<unsigned int> *vxIndexes=0)
{
  double x,y;
  if (!ReadLittle(input,x)) return ShapeFileReader::sheReadError;
  if (!ReadLittle(input,y)) return ShapeFileReader::sheReadError;
  unsigned int id=AddVertex(vxArray,x,y,vxIndexes);
  pt=ShapePoint(id,&vxArray);
  return ShapeFileReader::sheOk;
}


static ShapeFileReader::ShapeReaderError ReadPolyLine(std::ifstream &input, std::vector<points> &vxArray, ShapePoly &pl, std::vector<unsigned int> *vxIndexes = 0)
{
  double box[4];
  if (!ReadLittle(input,box)) return ShapeFileReader::sheReadError;
  unsigned long numParts;
  unsigned long numPoints;
  std::vector<unsigned int> vxlist;
  std::vector<unsigned int> partlist;
  if (!ReadLittle(input,numParts)) return ShapeFileReader::sheReadError;
  if (!ReadLittle(input,numPoints)) return ShapeFileReader::sheReadError;
  partlist.resize(numParts);
  vxlist.resize(numPoints);
  for (unsigned int i=0;i<numParts;i++)
  {
    unsigned long v;
    if (!ReadLittle(input,v)) return ShapeFileReader::sheReadError;
    partlist[i]=v;
  }
  for (unsigned int i=0;i<numPoints;i++)
  {
    double x,y;
    if (!ReadLittle(input,x)) return ShapeFileReader::sheReadError;
    if (!ReadLittle(input,y)) return ShapeFileReader::sheReadError;
    vxlist[i]=AddVertex(vxArray,x,y,vxIndexes);
  }
  pl = ShapePoly(vxlist,partlist,&vxArray);
  return ShapeFileReader::sheOk;
}

static ShapeFileReader::ShapeReaderError ReadPolyLineM(std::ifstream &input, std::vector<points> &vxArray, ShapePoly &pl)
{
  std::vector<unsigned int> vxIndexes;
  ShapeFileReader::ShapeReaderError err=ReadPolyLine(input,vxArray,pl,&vxIndexes);
  if (err) return err;
  err=ReadMOrZ(input,vxArray,vxIndexes,false,true);
  return err;
}

static ShapeFileReader::ShapeReaderError ReadPolyLineZ(std::ifstream &input, std::vector<points> &vxArray, ShapePoly &pl)
{
  std::vector<unsigned int> vxIndexes;
  ShapeFileReader::ShapeReaderError err=ReadPolyLine(input,vxArray,pl,&vxIndexes);
  if (err) return err;
  err=ReadMOrZ(input,vxArray,vxIndexes,true,true);
  if (err) return err;
  err=ReadMOrZ(input,vxArray,vxIndexes,false,true);
  return err;
}

//-------------------------------------------DBFReader--------------------------------------------------

DBFReader::ReaderError DBFReader::ReadDBF(std::ifstream& input, ShapeDatabase& database, const std::vector<unsigned int>& shapeIndexes, IDBColumnNameConvert* t)
{
  input.read((char*)&_header, sizeof(_header));
  if (input.fail()) return errHeaderReadError;

  _fieldInfo.clear();
  unsigned char mark;
  input.read((char*)&mark, 1);
  if (input.fail()) return errHeaderReadError;
  while (mark != 0x0D)
  {
    FieldInfo finfo;
    finfo.fieldName[0] = mark;
    input.read((char*)&finfo + 1, sizeof(finfo) - 1);
    if (input.fail()) return errHeaderReadError;
    input.read((char*)&mark, 1);
    if (input.fail()) return errHeaderReadError;
    finfo.fieldName[10] = 0;
    int id = (int)_fieldInfo.size();
    _fieldInfo.push_back(finfo);
  }
  for (int i =0; i < _fieldInfo.size(); i++)
  {
    if (t) 
    {
      _fieldTranslated.push_back(t->TranslateColumnName(_fieldInfo[i].fieldName));
    }
    else 
    {
      _fieldTranslated.push_back(_fieldInfo[i].fieldName);
    }
  }
  input.seekg(_header.headersz);
  int totalRecords = __min(_header.records, (unsigned)shapeIndexes.size());
  char* buffer = (char*)alloca(__max(_header.reclen, 255) + 1);
  int indexpos = 0;
  for (int i = 0; i < totalRecords; i++)
  {
    char delmark;
    input.read(&delmark, 1);
    if (input.fail()) return errErrorReadingData;
    if (delmark == ' ')
    {      
      for (int j = 0; j < _fieldInfo.size(); j++)
      {
        input.read(buffer, _fieldInfo[j].length);
        if (input.fail()) return errErrorReadingData;
        buffer[_fieldInfo[j].length] = 0;
        switch (_fieldInfo[j].fieldType)
        {
        case 'B':
        case 'M':
        case 'G': buffer[0] = 0; break; //NO VALUE, this type is not supported
        case '@': buffer[0] = 0; break; //NO VALUE, this type is not supported
        case '+':
        case '|':
          {
            long* l = reinterpret_cast<long*>(buffer);
            if (*l & 0x80000000) *l = -(*l & 0x7FFFFFFF);
            _ltoa(*l, buffer, 10);
          }
          break;
        case 'O':
          {
            double* l = reinterpret_cast<double*>(buffer);
            sprintf(buffer, "%g", *l);
          }
          break;
        }
        while (buffer[0] == ' ') strcpy(buffer, buffer + 1);
        int l = (int)strlen(buffer);
        while (l > 0 && buffer[l - 1] == ' ') buffer[--l] = 0;

        unsigned int shapeId = shapeIndexes[indexpos];
        if (shapeId != -1)
        {
          database.SetField(shapeId, _fieldTranslated[j], buffer);
        }
      }
      indexpos++;
    }
  }
  return errOk;  
}


DBFReader::ReaderError DBFReader::ReadDBF(const std::string fname, ShapeDatabase& database, const std::vector<unsigned int>& shapeIndexes, IDBColumnNameConvert* t)
{
  std::ifstream input;
  input.open(fname.c_str(), std::ifstream::binary);
  if (input.fail()) return errFileOpenError;
  DBFReader::ReaderError res = ReadDBF(input, database, shapeIndexes, t);
  input.close();
  return res;
}

//-------------------------------------------DBFReader--------------------------------------------------

//-----------------------------------------Shape Database------------------------------------------------
bool ShapeDatabase::DeleteShape(unsigned int shapeId)
{
  bool res = false;
  BTreeIterator<DBItem> iter(_database);
  DBItem* found;
  iter.BeginFrom(DBItem(shapeId));
  while ((found = iter.Next()) != 0 && found->_shapeId == shapeId)
  {
    _database.Remove(*found);
    iter.BeginFrom(DBItem(shapeId));
    res = true;
  }
  return res;
}

std::string ShapeDatabase::ShareString(const std::string& text) const
{
  std::string* nw = _stringTable.Find(text);
  if (nw) 
  {
    return *nw;
  }
  else 
  {
    return text;
  }
}


bool ShapeDatabase::SetField(unsigned int shapeId, const char* fieldName, const char* value)
{
  std::string field(fieldName);
  std::transform(field.begin(), field.end(), field.begin(), toupper);
  field = ShareString(field);
  std::string v(value);
  v = ShareString(v);

  DBItem* i = _database.Find(DBItem(shapeId, field));
  if(i) 
  {
    i->_value = v;
    return true;
  }
  else
  {
    return _database.Add(DBItem(shapeId, field, v));
  }
}

const char* ShapeDatabase::GetField(unsigned int shapeId, const char* fieldName) const
{
  std::string field = fieldName;
  std::transform(field.begin(), field.end(), field.begin(), toupper);
  field = ShareString(field);

  const DBItem* found = _database.Find(DBItem(shapeId, field));
  if (found) 
  {
    return found->_value.c_str();
  }
  else 
  {
    return "";
  }
}
//-----------------------------------------Shape Database------------------------------------------------

//-------------------------------------- ShapeFileReader ----------------------------------------------

IShape *ShapeFileReader::CreatePoint(const ShapePoint &origShape)
{
  return new ShapePoint(origShape);
}

IShape *ShapeFileReader::CreatePolyLine(const ShapePoly &origShape)
{
  return new ShapePolyLine(origShape);
}

ShapeFileReader::ShapeReaderError ShapeFileReader::ReadShFile(std::ifstream& input, std::vector<points> &vxArray, std::vector<IShape*> &shapeList, std::vector<unsigned int> *indexList)
{
  ShapeFileReader::ShapeReaderError res;

  res = ReadShapeHeader(input,_header);
  if (res) return res;
  if (_header.fileCode!=9994) return sheFileTagMismach;
  if (_header.version!=1000) return sheVersionNotSupported;

  unsigned long curSize=100;  
  while (curSize<_header.fileLength*2)
  {
    RecordHeader rhdr;
    if (!ReadRecordHeader(input, rhdr)) return sheUnexceptedEndOfFile;

    IShape *shape=0;

    switch (rhdr.shapeType)
    {
    case 0:break; //null shape
    case 1:
      {
        ShapePoint sh;
        res=ReadPoint(input,vxArray,sh);        
        if (res) return res;
        shape=CreatePoint(sh);
      }
      break;
    case 3:
      {
        ShapePoly sh;
        res=ReadPolyLine(input,vxArray,sh);        
        if (res) return res;
        shape=CreatePolyLine(sh);
      }
      break;
    case 5:
      break;
    case 8:
      break;
    case 11:
      break;
    case 13:
      {
        ShapePoly sh;
        res=ReadPolyLineZ(input,vxArray,sh);        
        if (res) return res;
        shape=CreatePolyLine(sh);
      }
      break;
    case 15:
      break;
    case 18:
      break;
    case 21:
      break;
    case 23:
      {
        ShapePoly sh;
        res=ReadPolyLineM(input,vxArray,sh);        
        if (res) return res;
        shape=CreatePolyLine(sh);
      }
      break;
    case 25:
      break;
    case 28:
      break;
    }
    if (shape==0) ///shape has not been created. SkipIt
    {
      unsigned long skipOffset=rhdr.contentLength*2-4;
      if (skipOffset)
      {
        char *buff;
        input.read(buff,skipOffset);
        if (input.fail()) return sheReadError;
        //        if (input.GetDataSize()!=skipOffset) return sheUnexceptedEndOfFile;
        curSize+=skipOffset+12;
      }  
    }
    else
    {
      unsigned long skipOffset=rhdr.contentLength*2;
      unsigned long recordNumber=rhdr.recordNumber-1;
      shapeList.push_back(shape);
      unsigned long shapeId = (unsigned int)shapeList.size()-1;
      while ((unsigned)indexList->size()<recordNumber) 
        indexList->push_back(-1);

      indexList->push_back(shapeId);
      curSize+=skipOffset+8;
    }    
  }
  return sheOk;
}

//-------------------------------------- ShapeFileReader ----------------------------------------------

//----------------------------------------ShapeOrganizer-----------------------------------------------
  const char* ShapeOrganizer::cpFieldName = "~ConstParams";

bool ShapeOrganizer::DeleteShape(unsigned int ID)
{
  return _shapeData.DeleteShape(ID);
}

bool ShapeOrganizer::SetShapeCP(unsigned int ShapeID, unsigned int constantParamsID)
{
  char buff[50];
  sprintf(buff, "%X", constantParamsID);
  return _shapeData.SetField(ShapeID, cpFieldName, buff);
}


bool ShapeOrganizer::ReadShapeFile(std::string name, unsigned int constantParamsID, std::vector<unsigned int> *shapeIndexes, IDBColumnNameConvert* dbconvert)
{
  std::ifstream newMap;
  newMap.open (name.c_str(), std::ifstream::binary);
  if (newMap.is_open())
  {
    ShapeFileReader::ShapeReaderError err =  ReadShFile (newMap,_vxList,_listOfShapes,shapeIndexes);
    if (err)
    {

    }
    else
    {
      newMap.close();
      std::size_t pos = name.find(".shp");
      name.replace(pos, 4, ".dbf");
      DBFReader dbfreader;
      DBFReader::ReaderError derr = dbfreader.ReadDBF(name, _shapeData, *shapeIndexes, dbconvert);
      if (derr)
      {

      }
      else 
      {
        for (int i = 0; i < shapeIndexes->size(); i++)
          SetShapeCP(shapeIndexes->at(i), constantParamsID);

        return true;
      }
    }
    newMap.close();

    for(int i = 0; i < shapeIndexes->size(); i++)
    {
      _listOfShapes.erase(_listOfShapes.begin());
      DeleteShape((*shapeIndexes)[i]);
    }
    return false;
  }
  return false;
}

const char* ShapeOrganizer::GetShapeParameter(unsigned int ShapeID, const char* fieldname) const
{
  return _shapeData.GetField(ShapeID, fieldname);
}

//----------------------------------------ShapeOrganizer-----------------------------------------------

//------------------------------------------ShapeLayer-------------------------------------------------

bool ShapeLayer::Load(std::string name)
  {
    if (_shapeOrg)
      delete _shapeOrg;

    _shapeOrg = new ShapeOrganizer;

    std::string fullName;
    std::vector<unsigned int> shapeVect;
    /*if (missionDir)
      fullName = GetMissionDirectory() + name;
    else
      fullName = GetUserDirectory() + name;*/

    if(! _shapeOrg->ReadShapeFile(name,0,&shapeVect))
    {
      //LogF("[m] loading shapefile: %s failed!", cc_cast(fullName));
      delete _shapeOrg;  _shapeOrg = NULL;
      return false;
    }

    //_name = name;
    return true;
  }