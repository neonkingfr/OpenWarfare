
#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include "..\..\Common\BTree.h"
#pragma once

struct points 
{
  double x,y,z,m;
};

class IShape
	{
	protected:
		std::vector<points> *_vertices;
    //DBox _boundingBox;
	public:

		///Contructor
		/**
		 * @param vertices Pointer to instance of vertex array. Each shape must be connected to a vertex arrat
		 *  It is legal to construct shape not connected with vertex array, but you should call SetVertexArray 
		 *  brefore shape is used
		 */

    IShape(){}
		IShape(std::vector<points>* vertices) 
		{
      memcpy(&_vertices,&vertices,sizeof(vertices));
		}

		virtual ~IShape(void) 
		{
		}

		///Returns pointer to associated vertex array
    std::vector<points>* GetVertexArray() 
		{
			return _vertices;
		}
	};


  class ShapePoint : public IShape
	{
		unsigned int _index;

	public:
    ShapePoint(){}

		ShapePoint(unsigned int index, std::vector<points>* vertices)
		: _index(index)
		, IShape(vertices) 
		{
		}
    
		virtual unsigned int GetVertexCount() const 
		{
			return 1;
		}

		virtual unsigned int GetPartCount() const 
		{
			return 1;
		}

		virtual unsigned int GetIndex(unsigned int pos) const 
		{
			return _index;
		}
	};

class ShapeMultiPoint : public IShape
	{
	protected:
    std::vector<unsigned int> _points;

	public:

    ShapeMultiPoint(){}

    ShapeMultiPoint(const std::vector<unsigned int>& point, std::vector<points>* vx)
		: IShape(vx) 
		{
      for (int i = 0; i < point.size(); i++)
        _points.push_back(point[i]);
		}

		virtual unsigned int GetVertexCount() const 
		{
			return (unsigned int)_points.size();
		}

		virtual unsigned int GetIndex(unsigned int pos) const 
		{
			return _points[pos];
		}

	};


class ShapePoly : public ShapeMultiPoint
	{
	protected:
    std::vector<unsigned int> _parts;

	public:

    ShapePoly()
    {

    }
    ShapePoly(const std::vector<unsigned int>& point,
				  const std::vector<unsigned int>& parts,
          std::vector<points>* vx)
		: ShapeMultiPoint(point, vx) 
		{
			static unsigned int part0 = 0;
			if (parts.size() == 0 && point.size() != 0) 
			{
				//_parts. .Copy(Array<unsigned int>(&part0, 1).Data(), Array<unsigned int>(&part0, 1).Size());
			}
			else 
			{
				for (int i = 0; i < parts.size(); i++)
          _parts.push_back(parts[i]);
			}
		}
	};

class ShapePolyLine : public ShapePoly
	{
	public:

    ShapePolyLine(const std::vector<unsigned int>& point, 
      const std::vector<unsigned int>& parts, std::vector<points>* vx) 
		: ShapePoly(point, parts, vx) 
		{
		}

		ShapePolyLine(const ShapePoly& other) 
		: ShapePoly(other)
		{
		}

		virtual ShapePolyLine* NewInstance(const ShapePoly& other) const
		{
			return NewInstance(ShapePolyLine(other));
		}

		///Creates copy of the shape
		/**
		* @copydoc ShapeMultiPoint::NewInstance 
		*/
		virtual ShapePolyLine* NewInstance(const ShapePolyLine& other) const
		{
			return new ShapePolyLine(other);
		}
	};

class ShapeDatabase
{
  struct DBItem
  {
    unsigned int _shapeId;
    std::string _paramName;
    std::string _value;

    DBItem(unsigned int shapeId = 0, const std::string& paramName = std::string(), const std::string& value = std::string()):
    _shapeId(shapeId),
      _paramName(paramName),
      _value(value)
    {
    }

    int CompareWith(const DBItem& other) const
    {
      if (_shapeId == other._shapeId)
      {
        return _paramName.compare(other._paramName);
      }
      return (_shapeId > other._shapeId) - (_shapeId < other._shapeId);
    }

    bool operator == (const DBItem& other) const 
    {
      return CompareWith(other) == 0;
    }

    bool operator >= (const DBItem& other) const 
    {
      return CompareWith(other) >= 0;
    }

    bool operator <= (const DBItem& other) const 
    {
      return CompareWith(other) <= 0;
    }

    bool operator != (const DBItem& other) const 
    {
      return CompareWith(other) != 0;
    }

    bool operator > (const DBItem& other) const 
    {
      return CompareWith(other) > 0;
    }

    bool operator < (const DBItem& other) const 
    {
      return CompareWith(other) < 0;
    }
  };

  BTree<DBItem>   _database;
  BTree<std::string> _stringTable;

  std::string ShareString(const std::string& text) const;

public:
  bool SetField(unsigned int shapeId, const char* fieldName, const char* value);
  const char* GetField(unsigned int shapeId, const char* fieldName) const;

  template<class T>
  bool ForEachField(const T& functor, unsigned int shapeId = -1) const
  {
    BTreeIterator<DBItem> iter(_database);
    iter.BeginFrom(DBItem(shapeId == -1 ? 0 : shapeId));
    DBItem* found;
    while ((found = iter.Next()) != 0 && (shapeId == -1 || shapeId == found->_shapeId))
    {
      if (functor(found->_shapeId, found->_paramName, found->_value)) return true;
    }
    return false;
  }
  bool DeleteShape(unsigned int shapeId);
  bool DeleteField(unsigned int shapeId, const char* fieldName);
  void Clear();
  unsigned int GetNextUsedShapeID(int currentID) const;
};

class IDBColumnNameConvert
{
public:
  virtual const char* TranslateColumnName(const char* name) const = 0;
};

class DBFReader
{
public:

#pragma pack(1)
  struct FieldInfo
  {
    char fieldName[11];
    char fieldType;
    unsigned long disp;
    unsigned char length;
    unsigned char decimals;
    unsigned char flags;
    unsigned long nextAutoincrement;
    unsigned char step;
    unsigned long res5;
    unsigned long res6;
  };

  struct DBFHeader
  {
    unsigned char version;
    unsigned char lastupdate_yy;
    unsigned char lastupdate_mm;
    unsigned char lastupdate_dd;
    unsigned long records;
    unsigned short headersz;
    unsigned short reclen;
    unsigned short zeroes;
    char incomplette_transaction;
    char encription;
    char multiuser_reservation[12];
    char index;
    unsigned char language;
    char reserved[2];
  };

protected:
  std::vector<const char*> _fieldTranslated;
  DBFHeader _header;
  std::vector<FieldInfo> _fieldInfo;

public:
  enum ReaderError
  {
    errOk = 0,
    errFileOpenError,
    errHeaderReadError,
    errErrorReadingData
  };

  ReaderError ReadDBF(std::ifstream& input, ShapeDatabase& database, const std::vector<unsigned int>& shapeIndexes, IDBColumnNameConvert* t = 0);    
  ReaderError ReadDBF(const std::string fname, ShapeDatabase& database, const std::vector<unsigned int>& shapeIndexes, IDBColumnNameConvert* t = 0);
};

class ShapeFileReader
{

public:
  struct ShapeHeader
  {
    unsigned long fileCode;
    unsigned long fileLength;
    unsigned long version;
    unsigned long shapeType;
    double Xmin,Ymin,Xmax,Ymax,Zmin,Zmax,Mmin,Mmax;
  };
protected:
  ShapeHeader _header;

public:
  enum ShapeReaderError
  {
    sheOk=0,
    sheReadError=1,
    sheUnexceptedEndOfFile=2,
    sheOpenError=3,
    sheFileTagMismach=4,
    sheVersionNotSupported=5
  };

  //		ShapeReaderError ReadShapeFile(std::istream& input, VertexArray &vxArray, ShapeList &shapeList, AutoArray<unsigned int>* indexList = 0);
  
  const ShapeHeader& GetShapeFileHeader() const 
  {
    return _header;
  }

  ShapeReaderError ReadShFile(std::ifstream& input, std::vector<points> &vxArray /*VertexArray &vxArray*/, std::vector<IShape*> &shapeList, std::vector<unsigned int> *indexList);

  IShape* CreatePolyLine(const ShapePoly& origShape);
  IShape *CreatePoint(const ShapePoint &origShape);
};

class ShapeOrganizer : public ShapeFileReader
{
		std::vector<IShape*>      _listOfShapes;
    ShapeDatabase  _shapeData;
		unsigned int   _nextcpcountervalue; //next ID of constant parameters
		std::vector<points> _vxList; /*VertexArray _vxList;*/
	public:
		ShapeOrganizer();
		~ShapeOrganizer();

		//static const char* algorithmFieldName;
		static const char* cpFieldName; 

		// -------------------------------------------------------------------------- //

		///Reads shape file and database
		/**
		*  @param name name of shape file. Extension should be .shp. Different extension
		* is allowed, bud database name is always assumed with .dbf extension;
		*  @param constantParamsID ID of allocated constant params. Call AllocNewConstantParamsID 
		* to allocate new ID, or use an existing ID (depends on usage). For "nocp" use -1
		*  @param dbconvert pointer to class that provides column name conversion. Use NULL for no conversion
		*  @retval true, successfully loaded
		*  @retval false, error occurred. Error is reported through exception register.
		*/
    bool ReadShapeFile(std::string newMap, 
                           unsigned int constantParamsID, 
                           std::vector<unsigned int> *shapeIndexes, 
                           IDBColumnNameConvert* dbconvert = 0);

		///Changes CP of specified shape.
		bool SetShapeCP(unsigned int ShapeID, unsigned int constantParamsID);

		
		bool DeleteShape(unsigned int ID);

		// -------------------------------------------------------------------------- //    

    std::vector<IShape*> GetListOfShapes () {return _listOfShapes;}

    std::vector<points> GetVectorPoints() {return _vxList;}

    const char* ShapeOrganizer::GetShapeParameter(unsigned int ShapeID, const char* fieldname) const;
    
};



class ShapeLayer
{
private:
  double _origin[2];
  ShapeOrganizer* _shapeOrg;
  std::string _name;

public:

  ShapeLayer();
  ~ShapeLayer();

  ShapeOrganizer* GetShapeOrg() {return _shapeOrg;}

  //load a new shapefile
  bool Load(std::string name);
};
