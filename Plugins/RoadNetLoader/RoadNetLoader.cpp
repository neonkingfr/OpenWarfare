// RoadNetLoader.cpp : Defines the entry point for the DLL application.

#include "Shapes.h"
#include "RoadNetLoader.h"


// Function called from RoadNetLoader interface
VBSPLUGIN_EXPORT bool WINAPI LoadRoadNetwork(IRoadNetwork* roadNetInterface, const char* mapName)
{
  char text[128];
  const char *suffix = "_segments.shp";
  sprintf(text, "LoadRoadNetwork - map %s", mapName);
  OutputDebugString(text);
  
  ShapeLayer GShapePolyLines,GShapePoints;
  int size = (int)strlen(mapName)-4;
  //char * auxMapName = (char *) calloc((int)strlen(mapName)+6, sizeof(char));
  char* auxMapName = new char[(int)strlen(mapName)+strlen(suffix)+1];
  int i,j;

  if (auxMapName == NULL)
    return false;

  // Parsing the name of the file (.wrp to .shp)
  strcpy(auxMapName, mapName);
  strcpy(&auxMapName[0 + size],suffix);
  std::string newMap(auxMapName);

  // Loading all the polyline shapes from the file
  if (!GShapePolyLines.Load(newMap))
    return false;

  ShapesManagement *PolyLineShapes = new ShapesManagement(GShapePolyLines.GetShapeOrg());

  // Parsing the name for the points (.wrp to .shp)
  newMap.replace(newMap.length()-9, newMap.npos, "nodes.shp");

  // Loading all the points shapes from the file
  bool crossroads = GShapePoints.Load(newMap);

  ShapesManagement *PolyPointsShapes = crossroads ? new ShapesManagement (GShapePoints.GetShapeOrg()) : NULL;

  const std::vector<points> PolyLinePoints = PolyLineShapes->GetVectorPoints();
  std::vector<points> PolyPoints;
  if (PolyPointsShapes)
  {
    PolyPoints = PolyPointsShapes->GetVectorPoints();
    // Sending all Points shapes to NG side
    for (i = 0; i < PolyPointsShapes->GetShapesListSize(); i++)
    {
      PolyPointsShapes->SetId(i);
      const ShapePoint* Point =  dynamic_cast<const ShapePoint*> (PolyPointsShapes->GetShape(i));
      if (!Point) continue;
      IRoadNetwork::RoadVectorData Points;
      Points.x = PolyPoints[Point->GetIndex(i)].x;
      Points.z = PolyPoints[Point->GetIndex(i)].y;
      roadNetInterface->AddRoadNode(Points,0, NULL);
      Point = NULL;
    }
  }
  // Sending all Lines shapes to NG side
  for (i = 0; i < PolyLineShapes->GetShapesListSize(); i++)
  {
    PolyLineShapes->SetId(i);
    const ShapePolyLine* Line =  dynamic_cast<const ShapePolyLine*> (PolyLineShapes->GetShape(i));
    if (!Line) continue;
    IRoadNetwork::RoadVectorData *PointLines = (IRoadNetwork::RoadVectorData *) calloc(Line->GetVertexCount(), sizeof(IRoadNetwork::RoadVectorData));
    for (j = 0; j < Line->GetVertexCount(); j++)
    {
      PointLines[j].x = PolyLinePoints[Line->GetIndex(j)].x;
      PointLines[j].z = PolyLinePoints[Line->GetIndex(j)].y;
    }
    roadNetInterface->AddRoadSegment(-1,-1,PolyLineShapes,Line->GetVertexCount(),PointLines);
    free(PointLines);
    Line = NULL;
  }
  
  PolyPoints.clear();
  if (PolyPointsShapes)
    delete PolyPointsShapes;
  delete PolyLineShapes;
  delete[] auxMapName;

  return true;
}


BOOL WINAPI DllMain( HMODULE hModule, DWORD  fdwreason, LPVOID lpReserved)
{
  switch (fdwreason)
  {
    case DLL_PROCESS_ATTACH:
      break;
    case DLL_PROCESS_DETACH:
      break;
    case DLL_THREAD_ATTACH:
      break;
    case DLL_THREAD_DETACH:
      break;
  }
    return TRUE;
}

