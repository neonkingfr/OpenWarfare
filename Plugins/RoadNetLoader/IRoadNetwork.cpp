#include "IRoadNetwork.h"

IRoadNetwork::IRoadNetwork(void)
{
}

IRoadNetwork::~IRoadNetwork(void)
{
}

bool ShapesManagement::ReadValue(const char* valueName, bool defaultValue) const
{
  const char * ret = shape->GetShapeParameter(idShape, valueName);
  if (strcmp(ret,""))
    return atoi(ret) != 0;

  return defaultValue;
}

void ShapesManagement::ReadValue(char* result, const char* valueName, const char* defaultValue) const
{
  const char * ret = shape->GetShapeParameter(idShape, valueName);
  strcpy(result, ret && strlen(ret) ? ret : defaultValue);
}

int ShapesManagement::ReadValue(const char* valueName, int defaultValue) const
{
  const char * ret = shape->GetShapeParameter(idShape, valueName);
  if (strcmp(ret,""))
    return atoi(ret);

  return defaultValue;
}

float ShapesManagement::ReadValue(const char* valueName, float defaultValue) const
{
  const char * ret = shape->GetShapeParameter(idShape, valueName);
  if (strcmp(ret,""))
    return atof(ret);

  return defaultValue;
}