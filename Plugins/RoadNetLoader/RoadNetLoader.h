#define VBSPLUGIN_EXPORT __declspec(dllexport)


#include "IRoadNetwork.h"
#include "windows.h"
#include <iostream>
#include <fstream>
#include <stdlib.h>


VBSPLUGIN_EXPORT bool WINAPI LoadRoadNetwork(IRoadNetwork* roadNetInterface, const char* mapNam);