#include "./vrpn/vrpn/vrpn_Connection.h" // Missing this file?  Get the latest VRPN distro at
#include "./vrpn/vrpn/vrpn_Tracker.h"    //    ftp://ftp.cs.unc.edu/pub/packages/GRIP/vrpn

#include <windows.h>
#include "VRPNExample.h"


void VRPN_CALLBACK handle_pos (void *, const vrpn_TRACKERCB t);

char connectionName[128];
int  port = 3883;

vrpn_TRACKERCB Pos;
vrpn_Connection *connection;
vrpn_Tracker_Remote *tracker = NULL;

// Command function declaration
typedef int (WINAPI * ExecuteCommandType)(const char *command, char *result, int resultLength);

// Command function definition
ExecuteCommandType ExecuteCommand = NULL;

// Function that will register the ExecuteCommand function of the engine
VBSPLUGIN_EXPORT void WINAPI RegisterHWND(void *hwnd, void *hins)
{
}

// Function that will register the ExecuteCommand function of the engine
VBSPLUGIN_EXPORT void WINAPI RegisterCommandFnc(void *executeCommandFnc)
{
  ExecuteCommand = (ExecuteCommandType)executeCommandFnc;
}


//== Position/Orientation Callback ==--

void	VRPN_CALLBACK handle_pos (void *, const vrpn_TRACKERCB t)
{
  Pos = t;
/*  
    printf("Tracker Position:(%.4f,%.4f,%.4f) Orientation:(%.2f,%.2f,%.2f,%.2f)\n",
    t.pos[0], t.pos[1], t.pos[2],
    t.quat[0], t.quat[1], t.quat[2], t.quat[3]);
    */
}

#define rad2Deg 180.0f/3.141592653589

// This function will be executed every simulation step (every frame) and took a part in the simulation procedure.
// We can be sure in this function the ExecuteCommand registering was already done.
// deltaT is time in seconds since the last simulation step
VBSPLUGIN_EXPORT void WINAPI OnSimulationStep(float deltaT)
{
  if(!tracker) return;

  tracker->mainloop();
  connection->mainloop();
  char cmd[512];
  sprintf(cmd,"player setheadDir [[%f,%f]]", Pos.quat[0]*rad2Deg, Pos.quat[1]*rad2Deg);
  ExecuteCommand(cmd, NULL, 0);
  //!}
}

// This function will be executed every time the script in the engine calls the script function "pluginFunction"
// We can be sure in this function the ExecuteCommand registering was already done.
// Note that the plugin takes responsibility for allocating and deleting the returned string
VBSPLUGIN_EXPORT const char* WINAPI PluginFunction(const char *input)
{
  //{ Sample code:
  static const char result[]="[1.0, 3.75]";
  return result;
  //!}
}

// DllMain
BOOL WINAPI DllMain(HINSTANCE hDll, DWORD fdwReason, LPVOID lpvReserved)
{
   switch(fdwReason)
   {
      case DLL_PROCESS_ATTACH:
        {
         OutputDebugString("Called DllMain with DLL_PROCESS_ATTACH\n");

         sprintf(connectionName,"localhost:%d", port);

         connection = vrpn_get_connection_by_name(connectionName);

         tracker = new vrpn_Tracker_Remote("Head", connection);

         tracker->register_change_handler(NULL, handle_pos);

         break;
        }
      case DLL_PROCESS_DETACH:
         OutputDebugString("Called DllMain with DLL_PROCESS_DETACH\n");
         break;
      case DLL_THREAD_ATTACH:
         OutputDebugString("Called DllMain with DLL_THREAD_ATTACH\n");
         break;
      case DLL_THREAD_DETACH:
         OutputDebugString("Called DllMain with DLL_THREAD_DETACH\n");
         break;
   }
   return TRUE;
}
