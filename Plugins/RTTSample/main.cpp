//
// Saves contents of all RTT scenes to RTT_<scenename>.jpg every 5 seconds
//

#include <windows.h>
#include "vbsplugin.h"
#include "../../Common/Essential/AppFrameWork.hpp"
#include <d3dx9.h>

IDirect3DDevice9 *d3dd = NULL;  // Holds the d3d device created by VBS2
ExecuteCommandType ExecuteCommand = NULL;

VBSPLUGIN_EXPORT void WINAPI OnD3DeviceRestore();
VBSPLUGIN_EXPORT void WINAPI OnD3DeviceInvalidate();


VBSPLUGIN_EXPORT void WINAPI OnDrawnRttScenes(RttScene *scenes, int count)
{
  // Save RTT views to JPG files every 5 seconds
  static int lastSave = 0;  
  if(GetTickCount() - lastSave > 5000)
  {
    for(int i=0;i<count;i++)
    {
      if(!scenes[i].active)
      {
        continue;
      }

      OutputDebugString("Saving RTT scene");

      char name[256];
      sprintf(name, "RTT_%s.jpg", scenes[i].name);
      D3DXSaveSurfaceToFile(name, D3DXIFF_JPG, scenes[i].surface, NULL, NULL);
    }
    lastSave = GetTickCount();
  }
}

// Onload - get the d3d device
VBSPLUGIN_EXPORT int WINAPI OnLoad(VBSParameters *params) 
{
  OutputDebugString("OnLoad");
  if (params)
  {
    if (params->_device)
    {
      d3dd = params->_device;

      // Call OnD3DeviceRestore - our Direct3D data is created there
      OnD3DeviceRestore();
    }
  }
  return 0;
}


VBSPLUGIN_EXPORT void WINAPI OnUnload() 
{
  OutputDebugString("OnUnload");

  // Call OnD3DeviceInvalidate - to release Direct3D data
  OnD3DeviceInvalidate();
}


// D3D device management, setting the engine D3D device
VBSPLUGIN_EXPORT void WINAPI OnD3DeviceInit(IDirect3DDevice9 *device)
{
  OutputDebugString("OnD3DeviceInit");
  d3dd = device;
}

// D3D device management, removing the engine D3D device: 
VBSPLUGIN_EXPORT void WINAPI OnD3DeviceDelete()
{
  OutputDebugString("OnD3DeviceDelete");
}


// D3D device management, before a reset
// All handles created on the Direct3D device must be freed here - otherwise VBS2 device reset will fail
VBSPLUGIN_EXPORT void WINAPI OnD3DeviceInvalidate()
{
  OutputDebugString("OnD3DeviceInvalidate");
}

// D3D device management, after a reset
// Re-create data for Direct3D device here
VBSPLUGIN_EXPORT void WINAPI OnD3DeviceRestore()
{
  OutputDebugString("OnD3DeviceRestore");
}

VBSPLUGIN_EXPORT void WINAPI OnDrawnPassTwo(float zNear, float zFar)
{
};

VBSPLUGIN_EXPORT void WINAPI OnSetFrustum(FrustumSpec *frustum)
{
};

// This function will be executed every simulation step (every frame) and took a part in the simulation procedure.
// We can be sure in this function the ExecuteCommand registering was already done.
// deltaT is time in seconds since the last simulation step
VBSPLUGIN_EXPORT void WINAPI OnSimulationStep(float deltaT) {
}

VBSPLUGIN_EXPORT const char* WINAPI PluginFunction(const char *inp)
{
  static const char resultDone[]="[true]";
  return resultDone;
}

BOOL WINAPI DllMain(HINSTANCE hDll, DWORD fdwReason, LPVOID lpvReserved)
{
   switch(fdwReason)
   {
      case DLL_PROCESS_ATTACH:
        OutputDebugString("DLL_PROCESS_ATTACH");
        break;
      case DLL_PROCESS_DETACH:
        OutputDebugString("DLL_PROCESS_DETACH");
        break;
   }
   return TRUE;
}


VBSPLUGIN_EXPORT void WINAPI RegisterHWND(void *hwnd, void *hins) {}

VBSPLUGIN_EXPORT void WINAPI RegisterCommandFnc(void *executeCommandFnc)
{
  ExecuteCommand = (ExecuteCommandType)executeCommandFnc;
}