To generate a update view of the Script debugger documentation. Please download cppdoc from: http://www.cppdoc.com/

Install it to C:\cppDoc\CppDoc2, without installation and you should be set to go. Double click on the GenerateDoc.bat and it should be good to go.