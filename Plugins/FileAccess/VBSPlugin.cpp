#include <vector>
#include <direct.h>
#include <windows.h>
#include <stdio.h>
#include "VBSPlugin.h"

using namespace std;

// New structure type definition
struct SProcInfo
{
	FILE *fileHandle;
	char fileName[256];

	SProcInfo()
	{
		fileHandle=NULL;
		memset(fileName, 0, 256);
	}
};

vector<SProcInfo> arProc;

// Command function declaration
typedef int (WINAPI * ExecuteCommandType)(const char *command, char *result, int resultLength);

// Command function definition
ExecuteCommandType ExecuteCommand = NULL;

// Function that will register the ExecuteCommand function of the engine
VBSPLUGIN_EXPORT void WINAPI RegisterHWND(void *hwnd, void *hins)
{
}

// Function that will register the ExecuteCommand function of the engine
VBSPLUGIN_EXPORT void WINAPI RegisterCommandFnc(void *executeCommandFnc)
{
  ExecuteCommand = (ExecuteCommandType)executeCommandFnc;
}

// This function will be executed every simulation step (every frame) and took a part in the simulation procedure.
// We can be sure in this function the ExecuteCommand registering was already done.
// deltaT is time in seconds since the last simulation step
VBSPLUGIN_EXPORT void WINAPI OnSimulationStep(float deltaT)
{
  //{ Sample code:
  //ExecuteCommand("0 setOvercast 1", NULL, 0);
  //!}
}


const char *GetStrArgument(const char *input, int argumentIndex)
{
  // The output result, initialized to empty
  static char result[4096];
  sprintf_s(result, "");

  // Read the content of brackets
  const char *begin = strchr(input, '(');
  if (begin)
  {
    begin++;
    const char *end = strchr(begin, ')');
    if (end)
    {
      // Skip commas, update begin
      const char *limit = NULL;
      for (int i = 0; i <= argumentIndex; i++)
      {
        // Find the end limit (either comma or right bracket - end)
        limit = strchr(begin, ',');
        if (!limit || limit > end) limit = end;

        // If it is the last argument then finish now
        if (i == argumentIndex) break;

        // Advance to the next item
        begin = limit + 1;

        // If next item is after end, then we are out of bounds
        if (begin > end) return result;
      }

      // Read content between begin and limit
      strncpy_s(result, begin, limit - begin);
    }
  }

  // Return whatever is in the result
  return result;
}

// Write into file
const char *Write(const char *input, const char *mode, const char *suffix)
{
  // The output result
  static char result[4096];

  // Open the specified file and write the data
  FILE *f;
  errno_t err = fopen_s(&f, GetStrArgument(input, 0), mode);
  if (err == 0)
  {
    const char *data = strchr(input, '@');
    if (data)
    {
      data++;
      fprintf(f, "%s%s", data, suffix);
    }

    // Close the opened file
    fclose(f);

    // Empty string is success
    sprintf_s(result, "[]");
  }
  else
  {
    sprintf_s(result, "[\"{error} Cannot open the specified file, errno_t==%d\"]", err);
  }

  // Return whatever is in the result
  return result;
}

// Create/remove directory
const char *DirFunctions(const char *input,int (*ptFunc)(const char*))
{
  // The output result
  static char result[4096];

  // Create/remove dir
  int err = (*ptFunc)(GetStrArgument(input, 0));
  if (err == 0)
  {
    // Empty string is success
    sprintf_s(result, "[]");
  }
  else
  {
    sprintf_s(result, "[\"{error} Cannot create\remove specified directory, errno_t==%d\"]", err);
  }

  // Return whatever is in the result
  return result;
}

// Remove file
const char *Remove(const char *input)
{
  // The output result
  static char result[4096];

  // Remove file
  int err = remove(GetStrArgument(input, 0));
  if (err == 0)
  {
    // Empty string is success
    sprintf_s(result, "[]");
  }
  else
  {
    sprintf_s(result, "[\"{error} Cannot delete specified file, errno_t==%d\"]", err);
  }

  // Return whatever is in the result
  return result;
}

// File exists test
const char *Exists(const char *input)
{
  // The output result
  static char result[4096];

  // Try to open specified file for reading
  FILE *f;
  errno_t err = fopen_s(&f, GetStrArgument(input, 0), "r");
  if (err == 0)
  {
    // Close the opened file
    fclose(f);

    // File exists
    sprintf_s(result, "[true]");
  }
  else
  {
    // File not exists or cannot be opened for reading
    sprintf_s(result, "[false]");
  }

  // Return whatever is in the result
  return result;
}

// Read FILE id from the global array
int GetFileId(const char *input)
{
	// The read result
	int id = -1;

	// Check if file is already open, searching for the file name
	for(int i = 0;i < (int)arProc.size(); i++)
	{
		// Compare file names, if matching, assign file handleS
		if(_stricmp(arProc[i].fileName, input) == 0 && arProc[i].fileHandle)
		{
			id = i;
			break;
		}
	}
  return id;
}

// Closes the file if its present in processes array
const char *CloseFile(const char *input)
{
	// The read result
	static char result[4096];

	FILE *f = NULL;
	int id = -1;

	// Get the ID of file from the processes array
	id = GetFileId(GetStrArgument(input, 0));

	// Removes the process from the array
	if(id>=0)
	{
		f = arProc[id].fileHandle;
		arProc.erase(arProc.begin()+id);

		// Closes file and destroys pointer
		if(f)
		{
			errno_t err = fclose (f);
			f = NULL;

			// Empty string is EOF
			if (err == 0)
			{
				sprintf_s(result, "[]");
			}
			else
			{
				sprintf_s(result, "[\"{error} Cannot close specified file, errno_t==%d\"]", err);
			}
		}
		else
		{
			sprintf_s(result, "[\"{error} Cannot close specified file, invalid handle==%p\"]", f);
		}
	}
	else
	{
			sprintf_s(result, "[\"{error} Cannot close specified file, invalid file name==%s\"]", GetStrArgument(input, 0));
	}
	return result;
}

// Read file line by line untill the end
const char *ReadFile(const char *input)
{
	// ProcInfo structure
	SProcInfo fileInfo;

	// The read result
	static char result[4096];

	errno_t err = 0;
	FILE *f = NULL;
	int id = -1;

	// Get the ID of file from the processes array
	id = GetFileId(GetStrArgument(input, 0));

	// If process ID was found, retrieve file handle from processes array
	if(id>=0)
	{
		fileInfo = arProc[id];
		f = fileInfo.fileHandle;
	}

	// Check if given handle is valid
	if (!f)
	{
		// Try to open given file for reading
		err = fopen_s(&f, GetStrArgument(input, 0), "r");

		if (err == 0)
		{
			// Create fileInfo structure
			fileInfo.fileHandle = f;
			strcpy_s(fileInfo.fileName, 256 , GetStrArgument(input, 0));

			// Push the fileInfo structure into processes array
			arProc.push_back(fileInfo);
			id = arProc.size()-1;
		}
	}

	if (err == 0)
	{
		char oLine[4096] = {0};
		char *nTerm;

		// If EOF is encountered close file and remove process from array
		if(feof (f))
		{
			// Call function to close file, requires unfiltered input, function retruns arguments directly
			sprintf_s(result, "%s" , CloseFile(input));
		}
		else
		{
			// Read line to the first null pointer
			fgets(oLine, 4096, f);

			// Remove null terminator at the end of string
			nTerm = strchr(oLine, '\n');
			if (nTerm != NULL) *nTerm = '\0';

			if (strlen(oLine) > 0)
				sprintf_s(result, "[\"%s\"]", oLine); // Print string into result
			else
				sprintf_s(result, "[\"\"]");		 //print a an empty string
		}
	}
	else
	{
		sprintf_s(result, "[\"{error} Cannot open specified file, errno_t==%d\"]", err);
	}

	// Return whatever is in the result
	return result;
}

// Lookup in given file for given string, provide values after the string, comma separated
const char *Lookup(const char *input)
{
  // The lookup result, initialize
  static char result[4096];
  sprintf_s(result, "[]");

  // Try to open specified file for reading
  FILE *f;
  errno_t err = fopen_s(&f, GetStrArgument(input, 0), "r");
  if (err == 0)
  {
    // Get ID of the item to look for
    const char *id = GetStrArgument(input, 1);

    // Look for the particular line
    char sLine[4096];
    const char *index = NULL;
    do
    {
      fgets(sLine, 4096, f);
      if ((index = strstr(sLine, id)) != NULL) break;
    } while (!feof(f));

    // File can be closed now
    fclose(f);

    // If line was found, read list of integer parameters, return them in string separated by a comma
    if (index)
    {
      // Move after the string
      index += strlen(id);

      // Find first digit
      while (strlen(index) > 0 && !isdigit(index[0])) index++;

      // Read X integers
      const int x = 7;
      int params[x];
      for (int i = 0; i < x; i++) params[i] = 0;
      sscanf_s(index, "%d %d %d %d %d %d %d", &params[0], &params[1], &params[2], &params[3], &params[4], &params[5], &params[6]);

      // Print those numbers into output, comma delimited
      sprintf_s(result, "[\"%d,%d,%d,%d,%d,%d,%d\"]", params[0], params[1], params[2], params[3], params[4], params[5], params[6]);
    }
    else
    {
      // File exists
      sprintf_s(result, "[]");
    }
  }

  // Return whatever is in the result
  return result;
}

// Copy string, duplicate quotation marks (it's the way the VBS represents the quotation marks)
void CopyVBSString(char *result, int numberOfElements, const char *source)
{
  for (int i = 0; i < (int)strlen(source); i++)
  {
    strncat_s(result, numberOfElements, &source[i], 1);
    if (source[i] == '"') strncat_s(result, numberOfElements, "\"", 1);
  }
}

// Find a variable by a given name, return the value assigned
const char *GetVar(const char *input)
{
  // The result, initialize
  static char result[4096];
  sprintf_s(result, "[]");

  // Try to open specified file for reading
  FILE *f;
  errno_t err = fopen_s(&f, GetStrArgument(input, 0), "r");
  if (err == 0)
  {
    // Get ID of the item to look for
    const char *id = GetStrArgument(input, 1);

    // Look for the particular line
    char sLine[4096];
    const char *index = NULL;
    do
    {
      // Read line from file
      fgets(sLine, 4096, f);
      
      // Skip white spaces
      const char *pLine = sLine;
      while (isspace(pLine[0])) pLine++;

      // Compare string
      if (strncmp(pLine, id, strlen(id)) == 0)
      {
        index = &pLine[strlen(id)];
        break;
      }
    } while (!feof(f));

    // File can be closed now
    fclose(f);

    // If line was found, read the value and create the VBS string out of it
    if (index)
    {
      // Find first '='
      if ((index = strchr(index, '=')) != NULL)
      {
        // Skip the '='
        index++;

        // Skip white spaces after '='
        while (isspace(index[0])) index++;

        // Calculate size of the string, stop at first control character
        int indexSize = 0;
        for (int i = 0; i < (int)strlen(index); i++)
        {
          if (iscntrl(index[i])) break;
          indexSize++;
        }

        // Copy string to output
        strcpy_s(result, "[\"");
        CopyVBSString(result, 4096 - strlen(result) - 1, index);
        strcat_s(result, "\"]");
      }
    }
  }

  // Return whatever is in the result
  return result;
}

// Find a variable by a given name, return the value assigned
const char *SetVar(const char *input)
{
  // The result, initialize
  static char result[4096];
  sprintf_s(result, "[]");

  // Try to open specified file for reading
  FILE *f;
  errno_t err = fopen_s(&f, GetStrArgument(input, 0), "r");
  if (err == 0)
  {
    // Open file with the result
    FILE *fout;
    char tempFileName[4096];
    sprintf_s(tempFileName, "%s_tmp", GetStrArgument(input, 0));
    err = fopen_s(&fout, tempFileName, "w");
    if (err != 0) // Problem to create the output file
    {
      fclose(f);
      return result;
    }

    // Get ID of the item to look for
    const char *id = GetStrArgument(input, 1);

    // Look for the particular line
    char sLine[4096];
    do
    {
      // Read line from file
      char *res = fgets(sLine, 4096, f);

      // Break in case we could not read the line (like there is not any?)
      if (res == NULL)
      {
        break;
      }

      // Skip white spaces
      const char *pLine = sLine;
      while (isspace(pLine[0])) pLine++;

      // Compare string
      if (strncmp(pLine, id, strlen(id)) == 0)
      {
        break;
      }

      // Put line to the output file
      fputs(sLine, fout);
    } while (!feof(f));

    // Create new line entry, write it to the output
    const char *data = strchr(input, '@');
    if (data)
    {
      data++;
    }
    char newEntry[4096];
    sprintf_s(newEntry, "%s = %s\n", id, data);
    fputs(newEntry, fout);

    // Copy rest of the lines to the output
    while (!feof(f))
    {
      fgets(sLine, 4096, f);
      if (!feof(f))
      {
        fputs(sLine, fout);
      }
    }

    // Close files
    fclose(fout);
    fclose(f);

    // Delete the old file, replace it by the new one
    remove(GetStrArgument(input, 0));
    rename(tempFileName, GetStrArgument(input, 0));
  }

  // Return whatever is in the result
  return result;
}

// This function will be executed every time the script in the engine calls the script function "pluginFunction"
// We can be sure in this function the ExecuteCommand registering was already done.
// Note that the plugin takes responsibility for allocating and deleting the returned string
VBSPLUGIN_EXPORT const char* WINAPI PluginFunction(const char *input)
{
  // Write command
  static const char cmdWrite[] = "Write";
  if (_strnicmp(input, cmdWrite, strlen(cmdWrite)) == 0) return Write(&input[strlen(cmdWrite)], "w", "");

  // AppendLine command (must be placed prior "Append", otherwise that one would be used)
  static const char cmdAppendLine[] = "AppendLine";
  if (_strnicmp(input, cmdAppendLine, strlen(cmdAppendLine)) == 0) return Write(&input[strlen(cmdAppendLine)], "a", "\n");

  // Append command
  static const char cmdAppend[] = "Append";
  if (_strnicmp(input, cmdAppend, strlen(cmdAppend)) == 0) return Write(&input[strlen(cmdAppend)], "a", "");

  // CreateDir command
  static const char cmdCreateDir[] = "CreateDir";
  if (_strnicmp(input, cmdCreateDir, strlen(cmdCreateDir)) == 0) return DirFunctions(&input[strlen(cmdCreateDir)],&_mkdir);

  // RemoveDir command
  static const char cmdRemoveDir[] = "RemoveDir";
  if (_strnicmp(input, cmdRemoveDir, strlen(cmdRemoveDir)) == 0) return DirFunctions(&input[strlen(cmdRemoveDir)],&_rmdir);

  // Delete command
  static const char cmdDelete[] = "Delete";
  if (_strnicmp(input, cmdDelete, strlen(cmdDelete)) == 0) return Remove(&input[strlen(cmdDelete)]);

  // Exists command
  static const char cmdExists[] = "Exists";
  if (_strnicmp(input, cmdExists, strlen(cmdExists)) == 0) return Exists(&input[strlen(cmdExists)]);

	 // CloseFile command
  static const char cmdCloseFile[] = "CloseFile";
  if (_strnicmp(input, cmdCloseFile, strlen(cmdCloseFile)) == 0) return CloseFile(&input[strlen(cmdCloseFile)]);

  // ReadFile command
  static const char cmdReadFile[] = "ReadFile";
  if (_strnicmp(input, cmdReadFile, strlen(cmdReadFile)) == 0) return ReadFile(&input[strlen(cmdReadFile)]);

  // Lookup command
  static const char cmdLookup[] = "Lookup";
  if (_strnicmp(input, cmdLookup, strlen(cmdLookup)) == 0) return Lookup(&input[strlen(cmdLookup)]);

  // GetVar command (getting content of a variable)
  static const char cmdGetVar[] = "GetVar";
  if (_strnicmp(input, cmdGetVar, strlen(cmdGetVar)) == 0) return GetVar(&input[strlen(cmdGetVar)]);

  // SetVar command (setting a variable)
  static const char cmdSetVar[] = "SetVar";
  if (_strnicmp(input, cmdSetVar, strlen(cmdSetVar)) == 0) return SetVar(&input[strlen(cmdSetVar)]);

  // Report error
  static const char err[] = "[\"{error} Unrecognized command\"]";
  return err;
}

// DllMain
BOOL WINAPI DllMain(HINSTANCE hDll, DWORD fdwReason, LPVOID lpvReserved)
{
   switch(fdwReason)
   {
      case DLL_PROCESS_ATTACH:
         OutputDebugString("Called DllMain with DLL_PROCESS_ATTACH\n");
         break;
			case DLL_PROCESS_DETACH:
					// Close all the files that could be open
					for(unsigned i = 0; i < arProc.size(); i++)
					{
						if (arProc[i].fileHandle) fclose (arProc[i].fileHandle);
					}
					arProc.clear();
					OutputDebugString("Called DllMain with DLL_PROCESS_DETACH\n");
					break;
					
					/*
					  Debug output here disabled: It causes Direct3D PIX to deadlock on thread
					  initialization due to OutputDebugString hook.
					*/
					/*
      case DLL_THREAD_ATTACH:
         OutputDebugString("Called DllMain with DLL_THREAD_ATTACH\n");
         break;
      case DLL_THREAD_DETACH:
         OutputDebugString("Called DllMain with DLL_THREAD_DETACH\n");
         break;
         */
   }
   return TRUE;
}
