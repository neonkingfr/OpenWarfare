// Copyright (C) 2012 Bohemia Interactive Simulations - support@BISimulations.com
//
// Purpose:		Main window for render the ScriptDebuggerGUI
//
// Author: Chad Lion & Martin Kolombo
// Date: 17-07-2012
//

#include "../../Common/Essential/AppFrameWork.hpp"

#include "Util.hpp"
#include "MainWindow.hpp"
#include "ResourceLoader.hpp"
#include "resource.h"
#include "ScriptDebuggerGUI.h"
#include "ImgManager.hpp"
#include "UIController.hpp"
#include "UIEvents.hpp"
#include <assert.h>

#include "ui/FormBuilderGUI.h"
#include "../../Common/Essential/ForEach.hpp"

#include "DeveloperDriveTree.hpp"

#include "settings.hpp"
#include "EventHandler.hpp"

#include "DocumentManager.hpp" 
#include "DocumentTabManager.hpp"
#include "DocumentTreeListManager.hpp"
#include "BreakpointManager.hpp"

#include "DialogStyle.hpp"

// Global pointer to the main parent window
MyFrame *GFrame;
Settings GSettings;

#if _VBS3_SD_IMP_BREAKPOINTS
extern MissionDirInfo GCurrentMissionDir;
// Global pointer to the VBS IToApp interface
IToApp *GVBSApp = NULL;
#endif

// App framework and pointers at it
ScriptDebuggerFrameWork SDGWin32FrameWork;
ScriptDebuggerFrameWork* SDFramework = &SDGWin32FrameWork;
// Required by AppFrameWork.hpp
AppFrameWorkBase* GFrameWork = SDFramework;

#if _SD_CONFIG_DOCUMENTS
const TCHAR *GConfigFileName = L"ScriptDebugger.cfg";
std::wstring GConfigPath;
#else
const TCHAR *GConfigFileName = L"config.cpp";
#endif


BEGIN_EVENT_TABLE(MyFrame, DebugScriptWindow)

/// TM = Tree manager, events that come through the main window
#if _VBS3_DEBUG_DELAYCALL
EVT_MENU(TM_EVENT_DELAYED_BREAK, MyFrame::OnDelayBreak)
#endif
EVT_MENU(TM_EVENT_BREAK, MyFrame::OnBreak)
EVT_MENU(TM_EVENT_CONTINUE, MyFrame::OnContinue)
EVT_MENU(TM_EVENT_STEP_CONTINUE, MyFrame::OnContinue)
#if _SD_BREAK_ALL
EVT_MENU(TM_EVENT_BREAK_ALL, MyFrame::OnBreakAll)
EVT_MENU(TM_EVENT_CONTINUE_ALL, MyFrame::OnContinueAll)
#endif
EVT_MENU(TM_EVENT_STEP_INTO, MyFrame::OnStepInto)
#if _VBS_1428_STEP_FILE
EVT_MENU(TM_EVENT_STEP_FILE, MyFrame::OnStepFile)
#endif
EVT_MENU(TM_EVENT_STEP_OVER, MyFrame::OnStepOver)
EVT_MENU(TM_EVENT_STEP_OUT, MyFrame::OnStepOut)
EVT_MENU(TM_EVENT_CANCEL, MyFrame::OnCancel)

/// Debugging events, No longer used... See OnCommandEvent
EVT_TOOL(IDC_BtnContinueSingle, MyFrame::OnContinue)
#if _SD_BREAK_ALL
EVT_TOOL(IDC_BtnContinueAll, MyFrame::OnContinueAll)
EVT_TOOL(IDC_BtnBreakAll, MyFrame::OnBreakAll)
#endif
EVT_TOOL(IDC_BtnStepInto, MyFrame::OnStepInto)
#if _VBS_1428_STEP_FILE
EVT_TOOL(IDC_BtnStepFile, MyFrame::OnStepFile)
#endif
EVT_TOOL(IDC_BtnStepOver, MyFrame::OnStepOver)
EVT_TOOL(IDC_BtnStepOut, MyFrame::OnStepOut)

/**
  * New system, is that everything is routed through to the OnMenuEvent
  * because the event's are unique, there is no real reason why they need their own distinct routines.
  */

EVT_MENU(wxID_ANY, MyFrame::OnCommandEvent)
EVT_TOOL(wxID_ANY, MyFrame::OnCommandEvent)

EVT_COMMAND(wxID_ANY, SearchWindowEvent, MyFrame::OnSearchWindowEvt)

/// FlatNote book update
EVT_FLATNOTEBOOK_PAGE_CHANGED(IDC_LEFT_NOTE_BOOK,MyFrame::OnLeftPanelNoteBookChange)

EVT_CLOSE(MyFrame::OnClose)
EVT_ICONIZE(MyFrame::OnIconized)
#if _SD_UNPINNED_WINDOWS_SHOW_FIX
EVT_SHOW(MyFrame::OnShow)
#endif

END_EVENT_TABLE()



void MyFrame::OnIconized( wxIconizeEvent& event )
{
#if !_SD_UNPINNED_WINDOWS_SHOW_FIX
  Show(false);
#endif
  event.Skip();
}

void MyFrame::OnClose(wxCloseEvent& event)
{ 
#if _CONSOLE
  // Perform the standard process routine
  event.Skip();  
#else
  OnContinueAll(false);
  SaveWindowLayoutToSettings();
  GSettings.SaveConfigFile();

#if _VBS_15068_PROPER_SHUTDOWN
  if (event.CanVeto()) //when VBS itself is closing, don't veto the event
#endif
    event.Veto();
  Show(false);
#endif 
}

void MyFrame::OnQuit(wxCommandEvent& event)
{
#if _CONSOLE
  Close(true);
#else
  OnContinueAll(false);
  Show(false);
#endif

  SaveWindowLayoutToSettings();
  GSettings.SaveConfigFile();
  event.Skip();
}

#if _SD_UNPINNED_WINDOWS_SHOW_FIX
void MyFrame::OnShow(wxShowEvent& event)
{
  if (event.GetShow())
  {
#if _SD_FIX_BOTTOM_NTB_ARTIFACTS
    if (_debugConsole && !_debugConsole->IsDocked())
#else
    if (_debugConsole)
#endif
      _debugConsole->Show(true);
#if _SD_SCRIPT_LOG
  #if _SD_FIX_BOTTOM_NTB_ARTIFACTS
    if (_scriptLogWindow && !_scriptLogWindow->IsDocked())
  #else
    if (_scriptLogWindow)
  #endif
      _scriptLogWindow->Show(true);
#endif
#if _SD_DEBUG_LOG
  #if _SD_FIX_BOTTOM_NTB_ARTIFACTS
    if (_debugWindow && !_debugWindow->IsDocked())
  #else
    if (_debugWindow)
  #endif
      _debugWindow->Show(true);
#endif
  }
}
#endif

void MyFrame::OnTreeRightClick(wxCommandEvent &event)
{
  
}

void MyFrame::OnTreeItemRightClick(wxCommandEvent &event)
{

}




iMainWindow* GMainWindow;




MyFrame::MyFrame(wxWindow* parent):DebugScriptWindow(parent)
{
  // Set the GMainWindow interface to this
  GMainWindow = this;
  //! Task bar init
  _myTaskBar = new MyTaskBarIcon(this);
#if _SD_DEBUG_LOG
  _debugWindow = NULL;
#endif
#if _SD_SCRIPT_LOG
  _scriptLogWindow = NULL;
#endif
#if _VBS_14573_DEBUG_CONSOLE
  _debugConsole = NULL;
#endif
  _watchManager = NULL;
  _errorManager = NULL;
  _callStackManager = NULL;
  _perfWindow = NULL;
  _settingWindow = NULL;
  _docStyleWindow = NULL;
  _searchWindow = NULL;
#if _SD_CONFIG_DOCUMENTS
  //VBS2NG documents directory path
  GConfigPath = CharToUTF16(GUtil.GetEngineDirectory(USER_DIR));
#endif
  // Load the config file
  GSettings.LoadConfigFile();
  
  // Set window size and position from settings
  SetSize( GSettings.GetMainWindowSettings().size );
  Move( GSettings.GetMainWindowSettings().position );
  Maximize( GSettings.GetMainWindowSettings().maximized );
  // Set the tabs to be on the bottom, is not available in wxFormBuilder
  BottomNoteBook->SetWindowStyle(BottomNoteBook->GetWindowStyle()|wxAUI_NB_BOTTOM);

#if _DEVELOPER_DRIVE
  _developerDriveManager = new DeveloperDriveTree(DeveloperDriveTreeCtrl); 
#endif
#if _SD_VM_TREE
  _treeManager =  new VirtualMachineTree(VirtualMachineTreeCtrl);
#endif
#if _SD_EH_TAB
  _eventManager = new PropertyGridEventHandler(PropertyGrid);
#endif

   SetUpBottomNotebook(); //intializes all bottom notebook tabs with GSettings values

  // Remove the directory style
  // wxNO_BORDER
  long noBorderStyle = 0;

  noBorderStyle = DocumentSelector->GetWindowStyle();
  DocumentSelector->SetWindowStyle(noBorderStyle|wxNO_BORDER);

  GDocumentManager = new DocumentManager();
  GDocumentTabManager = new DocumentTabManager(DocumentFlatNoteBook);
  GDocumentTreeManager = new DocumentTreeListManager(DocumentSelector);

  // Main text area for the display
  _doc = GDocumentManager->CreateDocument(L"VirtualMachine");
  _doc->SetDocViewContent(DOC_VM,std::string()); // Set the document as virtual machine view

#if REMOVE_PAGE_FIX
  BottomNoteBook->Connect(wxEVT_COMMAND_FLATNOTEBOOK_PAGE_CLOSING,wxFlatNotebookEventHandler(MyFrame::OnNoteBookCloseTab),NULL,this);
#else
  BottomNoteBook->Connect(wxEVT_COMMAND_AUINOTEBOOK_PAGE_CLOSE,wxAuiNotebookEventHandler(MyFrame::OnNoteBookCloseTab),NULL,this);
#endif
  
  // Connect to the manager to hide instead of close panes
  m_mgr.Connect(wxEVT_AUI_PANE_CLOSE, wxAuiManagerEventHandler(MyFrame::OnClosePane), NULL, this);


  // Set the selection to be the first in the list,
  // this for some reason doest work
  LeftPanelNoteBook->SetSelection(0);

  ToggleTitleWindow();

  // Save the manager perspective defaults
  _auiMgnerView = m_mgr.SavePerspective();
  _auiDebugPaneDefaultView = m_mgr.SavePaneInfo(m_mgr.GetPane(BottomNoteBook));
#if _SD_EH_TAB
  _auiEHPaneDefaultView = m_mgr.SavePaneInfo(m_mgr.GetPane(PropertyGridPanel));
#endif
  _auiFilePaneDefaultView = m_mgr.SavePaneInfo(m_mgr.GetPane(LeftPanelNoteBook));
  //save the default view to Settings
  GSettings.GetMainWindowSettings().SetDefaultAuiPerspective(_auiMgnerView);
  //and load the saved one
  m_mgr.LoadPerspective(GSettings.GetMainWindowSettings().GetAuiPerspective());
  m_mgr.Update();

  // Set the window icon.
  wxIcon icon;
  if (GImgManager.GetWxIcon(ICON_TRAY,icon))  SetIcon(icon);
}

// Set the document's focus
DebugScript *MyFrame::GetFocus()
{
  return _focus;
}

void MyFrame::OnClosePane(wxAuiManagerEvent &event){}

#if REMOVE_PAGE_FIX
void MyFrame::OnNoteBookCloseTab(wxFlatNotebookEvent &event)
#else
void MyFrame::OnNoteBookCloseTab(wxAuiNotebookEvent &event)
#endif
{
  event.Veto();// Do not delete the page.

  wxWindow *wnd = BottomNoteBook->GetPage(event.GetSelection());
  if (wnd)
  {
    wnd->Show(false);
#if REMOVE_PAGE_FIX
    BottomNoteBook->RemovePage(event.GetSelection(),false);
#else
    BottomNoteBook->RemovePage(event.GetSelection());
#endif
  }

  // Skip the event
  event.Skip();
}

void MyFrame::SaveWindowLayoutToSettings()
{
#if _SD_DEBUG_LOG
  if (_debugWindow)
  {
    OutputWindowSettings& wndSettings = GSettings.GetOutputWindowSettings(WND_DEBUG_OUTPUT);
    wndSettings.position = _debugWindow->GetPosition();
    wndSettings.size = _debugWindow->GetSize();
    wndSettings.maximized = _debugWindow->IsMaximized();
  }
#endif
#if _SD_SCRIPT_LOG
  if (_scriptLogWindow)
  {
    OutputWindowSettings& wndSettings = GSettings.GetOutputWindowSettings(WND_SCRIPT_LOG);
    wndSettings.position = _scriptLogWindow->GetPosition();
    wndSettings.size = _scriptLogWindow->GetSize();
    wndSettings.maximized = _scriptLogWindow->IsMaximized();
  }
#endif
  GSettings.GetMainWindowSettings().SetAuiPerspective(m_mgr.SavePerspective());
  GSettings.GetMainWindowSettings().position = GetPosition();
  GSettings.GetMainWindowSettings().size = GetSize();
  GSettings.GetMainWindowSettings().maximized = IsMaximized();
}

void MyFrame::SetUpBottomNotebook(bool reset)
{
#if _SD_DEBUG_LOG
  //Debug output
  if (!_debugWindow)
  {
    DebugOutPutWindow * dbgWindow;
    if (reset)
    {
      //make sure the window starts docked
      GSettings.GetOutputWindowSettings(WND_DEBUG_OUTPUT).docked = true;
    }    
 #if _SD_UNPINNED_WINDOWS_SHOW_FIX
    wxWindow* parent = NULL;
    if (GSettings.GetOutputWindowSettings(WND_DEBUG_OUTPUT).docked)
      parent = this;
    
    dbgWindow = new DebugOutPutWindow(parent, wxT("Debug log"), WND_DEBUG_OUTPUT);
 #else
    dbgWindow = new DebugOutPutWindow(this, wxT("Debug log"), WND_DEBUG_OUTPUT);
 #endif
    RegisterDebugWindow(dbgWindow); //register the window with the app
  }
  else if (reset) //the window exists, but may not be in the notebook
  {
    if (BottomNoteBook->GetPageIndex(_debugWindow) == -1)
    {
      _debugWindow->pinWindow(GFrame,BottomNoteBook);
    }
  }
#endif
#if _SD_SCRIPT_LOG
  if (!_scriptLogWindow)
  {
    //add the script logging window
    if (reset)
    {
      //make sure the window starts docked
      GSettings.GetOutputWindowSettings(WND_SCRIPT_LOG).docked = true;
    }
  #if _SD_UNPINNED_WINDOWS_SHOW_FIX
    wxWindow* parent = NULL;
    if (GSettings.GetOutputWindowSettings(WND_SCRIPT_LOG).docked)
      parent = this;

    DebugOutPutWindow * scriptLogWindow = new DebugOutPutWindow(parent, wxT("Script log"), WND_SCRIPT_LOG);
  #else
    DebugOutPutWindow * scriptLogWindow = new DebugOutPutWindow(this, wxT("Script log"), WND_SCRIPT_LOG);
  #endif
    RegisterScriptLogWindow(scriptLogWindow);
  }
  else if (reset)
  {
    if (BottomNoteBook->GetPageIndex(_scriptLogWindow) == -1)
    {
      _scriptLogWindow->pinWindow(GFrame,BottomNoteBook);
    }
  }
#endif
#if _VBS_14573_DEBUG_CONSOLE
  if (!_debugConsole)
  {
    if (reset)
    {
      GSettings.GetDebugConsoleWindowSettings().docked = true;
    }
#if _SD_UNPINNED_WINDOWS_SHOW_FIX
    wxWindow* parent = NULL;
    if (GSettings.GetDebugConsoleWindowSettings().docked)
      parent = this;

    RegisterDebugConsoleWindow(new DebugConsoleWindow(parent,wxID_ANY,L"Debug Console"));
#else
    RegisterDebugConsoleWindow(new DebugConsoleWindow(this,wxID_ANY,L"Debug Console"));
#endif
  }
  else if (reset)
  {
    if (BottomNoteBook->GetPageIndex(_debugConsole) == -1)
    {
      _debugConsole->pinWindow(GFrame,BottomNoteBook);
    }
  }
#endif

  // Add the watch manager
  if (!_watchManager)
  {
    _watchManager = new WatchManagerUI(this);
    _watchManager->Connect(wxEVT_GRID_CELL_CHANGE, wxGridEventHandler(MyFrame::OnGridChangeWatchManager), NULL, this);
    BottomNoteBook->AddPage(_watchManager,L"Memory Watcher");
  }
  else
  {
    if (BottomNoteBook->GetPageIndex(_watchManager) == -1)
    {
      BottomNoteBook->AddPage(_watchManager,L"Memory Watcher");
    }
  }
  
  // Error manager
  if (!_errorManager)
  {
    // A bit ugly, but gets the job done
    _errorManager    = new
      wxTextCtrl
      ( 
        this,
        wxID_ANY,
        wxEmptyString,
        wxDefaultPosition,
        wxDefaultSize,
        wxNO_BORDER|wxHSCROLL|wxTE_LEFT|wxTE_MULTILINE|wxTE_PROCESS_ENTER|wxTE_PROCESS_TAB 
      );
    BottomNoteBook->AddPage(_errorManager,L"Error log");
  }
  else
  {
    if (BottomNoteBook->GetPageIndex(_errorManager) == -1)
    {
      BottomNoteBook->AddPage(_errorManager,L"Error log");
    }
  }

  // call stack manager
  if (!_callStackManager)
  {
    _callStackManager = new CallStackManagerUI(this);
    BottomNoteBook->AddPage(_callStackManager,L"Call Stack");
  }
  else
  {
    if (BottomNoteBook->GetPageIndex(_callStackManager) == -1)
    {
      BottomNoteBook->AddPage(_callStackManager,L"Call Stack");
    }
  }
}

MyFrame::~MyFrame()
{
  if (_searchWindow) //the scintilla control, which the search control is linked to may already be destroyed
  {
    _searchWindow->SetSearchWindow(NULL); //so make sure we let the search window know about that
  }

  GFrame = NULL;
  GMainWindow = NULL;
  wxXmlResource::Get()->ClearHandlers();

#if _SD_UNPINNED_WINDOWS_SHOW_FIX
  //if a window is unpinned, it isn't child of this, we need to destroy it explicitly
  //GFrame must be NULL by now
 #if _VBS_14573_DEBUG_CONSOLE
  if ( _debugConsole && !GSettings.GetDebugConsoleWindowSettings().docked)
  {
    _debugConsole->Destroy();
    _debugConsole = NULL;
  }
 #endif
 #if _SD_SCRIPT_LOG
  if ( _scriptLogWindow && !GSettings.GetOutputWindowSettings(WND_SCRIPT_LOG).docked)
  {
    _scriptLogWindow->Destroy();
    _scriptLogWindow = NULL;
  }
 #endif
 #if _SD_DEBUG_LOG
  if ( _debugWindow && !GSettings.GetOutputWindowSettings(WND_DEBUG_OUTPUT).docked)
  {
    _debugWindow->Destroy();
    _debugWindow = NULL;
  }
 #endif
#endif
#if _VBS_15068_PROPER_SHUTDOWN
  BottomNoteBook->DeleteAllPages();
  LeftPanelNoteBook->DeleteAllPages();
  //document flat notebook pages get deleted in GDocumentTabManager's destructor
  //set all the global smart pointers to null
  GDocumentTabManager = NULL;
  GDocumentManager = NULL;
  GDocumentTreeManager = NULL;
#endif
}

struct LineIndicator : public RefCount
{
  int _cLine;

  LineIndicator()
  {
    _cLine = -1;
  };

  void ChangeLine(int newLine){ _cLine = newLine; }
  bool GetLine(int &line)
  {
    if (_cLine!=-1)
    {
      line = _cLine;
      return true;
    }

    return false;
  }

  void Clear()
  {
    _cLine = -1;
  }

  bool IsValid(){ return _cLine>=0 ? true : false; }
};

bool GBreakAll;
SmartRef<LineIndicator> GLine;

void MyFrame::OnGridChangeWatchManager(wxGridEvent &event)
{
  OnAllStateChangeEvent(NULL,_focus);
  
  event.Skip();
}

wxWindow *wxGetWindowFromHWND(WXHWND hwnd);

/*
 To make things really simple and to reduce coupling between the system
 the parent window, will have to set the child's windows to null. To take
 advantage of this, we just cycle through all the windows and remove
 any that we know about.
*/
void MyFrame::RemoveChild( wxWindowBase *child )
{
  if (_perfWindow==child) _perfWindow = NULL;
  if (_settingWindow==child) _settingWindow = NULL;
  if (_docStyleWindow==child) _docStyleWindow = NULL;

  base::RemoveChild(child);
}

#include <algorithm>

// Register window to receive state changes
void MyFrame::RegisterWindow(iViewWindow *window)
{
  _iWindows.push_back(window);
}

void MyFrame::UnRegisterWindow(iViewWindow *window)
{
  _iWindows.erase(std::find(_iWindows.begin(), _iWindows.end(), window));
}


// Communicate to the main window, that all window's
// state change should be called
void MyFrame::OnAllStateChangeEvent(iViewWindow*caller,DebugScript*debugScript)
{
  // State change has come in update the script...
#if _VBS_14573_DEBUG_CONSOLE
  if (debugScript)
  {
    debugScript->Update();
    if (debugScript->IsBreaked() && _debugConsole)
    {
  #if _VBS_15079_RESTRICT_BISIM_SCRIPTS
      wxString filePath = debugScript->GetScriptFilePath();
      if (GUtil.IsProtectedPath(filePath))
        filePath = L"Breaked Script";
      _debugConsole->SetScope(wxString::Format(wxT("Scope : %s"), filePath));
  #else
      _debugConsole->SetScope(wxString::Format(wxT("Scope : %s"), debugScript->GetScriptFilePath()));
  #endif
    }
    else if (_debugConsole)
    {
      _debugConsole->SetScope(L"Global Scope");
    }
  }
  else if (_debugConsole)
  {
    _debugConsole->SetScope(L"Global Scope");
  }
#else
  if (debugScript) debugScript->Update();
#endif

  // Easy way to populate state changes between the windows
  for (size_t i=0; i<_iWindows.size(); i++)
    if (_iWindows[i]&&(_iWindows[i]!=caller)) _iWindows[i]->StateChange(debugScript);
}

void MyFrame::OnAllSimulate(float deltat)
{
  for (size_t i=0; i<_iWindows.size(); i++)
    if (_iWindows[i]) _iWindows[i]->SimulateUpdate(deltat);
}

template<class Callback>
bool ForEachMenu(wxMenu *menu,Callback &callback)
{
  wxMenuItemList& m_items = menu->GetMenuItems();
  wxMenuItem *item = NULL;

  for ( wxMenuItemList::compatibility_iterator node = m_items.GetFirst();
    node && !item;
    node = node->GetNext() )
  {
    item = node->GetData();
    // Callback with menuItem
    if (callback(item)) return true;

    if ( item->IsSubMenu() )
      if (ForEachMenu(item->GetSubMenu(),callback)) return true;

    item = NULL;
  }

  return false; // continue;
}
bool UnTick(wxMenuItem *item)
{
  if (item->IsCheckable()) item->Check(false);
  
  return false;// continue;
}

extern HWND GHWND; // Main VBS2 handle to the window

void MyFrame::DocumentClosing(DocumentViewer *doc)
{
  bool setForground = false;

  for (size_t i=0; i<_scripts.size(); i++)
  {
    DebugScript *script = _scripts[i];
    if (script && script->IsBreaked())
    {    
      DocumentViewer *currentDoc = script->GetDisplayDoc();
      if (currentDoc == doc)
      {
        // Set the script as no longer being breaked
        // and set the script to run
        script->SetBreak(false);
        if (script->IsRunning()) setForground = true;
        if (script->GetIDebugScript()) script->GetIDebugScript()->RunScript(true); // run the script
      }
    }
  }

  if (setForground) SetForegroundWindow(GHWND);
}


void MyFrame::DocumentToforeground(DocumentViewer *doc)
{
  ForEachMenu(EncodingMenu,UnTick);
  
  UINT encoding = FromEncodingToWxMenuID(doc->GetEncoding());
  wxMenu *menu = NULL;
  wxMenuItem *item = EncodingMenu->FindItem(encoding,&menu);
  if (item) item->Check(true);
  if (_searchWindow) //update the document the search window searches in
  {
    _searchWindow->SetSearchWindow(doc->GetDocument());
  }
}

void MyFrame::OnCommandEvent(wxCommandEvent &event)
{
  int id = event.GetId();
  bool resetNoteBook = false;
  switch (id)
  {
    case IDC_BtnNewFile:
    case MENU_ON_NEW_FILE:
      {
        // Add new document
        DocumentViewer *doc = GDocumentManager->CreateDocument(L"NewScript");
        GDocumentTabManager->BringIntoFocus(doc);
        break;
      }
    case IDC_BtnOpenFile:
    case IDC_BtnOpenMissionFile:
    case MENU_ON_OPEN:
      {
        // Option to open up the mission dir, or the user directory
        wxString defaultDir;
        if (id==IDC_BtnOpenMissionFile)
        {
          std::string defPath = GUtil.GetEngineDirectory(MISSION_DIR);
          if (!defPath.length()) defPath = GUtil.GetEngineDirectory(USER_DIR);

          defaultDir = CharToUTF16(defPath);
        }

        // Open the specified sqf or sqs file
        wxFileDialog OpenDialog
        (
		      this, 
          _("Choose a file to open"), defaultDir, wxEmptyString, 
		      _("Scripts (*.sqf, *.sqs)|*.sqs;*.sqf"),wxFD_OPEN|wxFD_FILE_MUST_EXIST|wxFD_MULTIPLE, wxDefaultPosition
         );
       
	      // Creates a "open file" dialog with 4 file types
	      if (OpenDialog.ShowModal() == wxID_OK)// if the user click "Open" instead of "Cancel"
        {
          wxArrayString files;
          OpenDialog.GetPaths(files);

          DocumentViewer *view = NULL;

          for (size_t i=0; i<files.size(); i++)
          {
            // First check to see if, the file already is open.
            DocumentViewer *openDoc = GDocumentManager->GetDocument(files[i]);
            Assert(openDoc);
            if (openDoc->GetDocContents()&DOC_VM)
            {
              // Open the file!
#if _VBS_15079_RESTRICT_BISIM_SCRIPTS
              view = GDocumentManager->OpenFile(files[i],NULL,false); //the file is not internal since we're opening it via the dialog
#else
              view = GDocumentManager->OpenFile(files[i]);
#endif
            }
            else
            {
              // File is dirty, ask if they want to overwrite it?
              if (openDoc->IsDirty())
              {
                int choice = wxMessageBox(wxString::Format(L"Document:%s is open do you want to overwrite the file?",openDoc->GetName().GetData()),L"Document all ready open",wxOK|wxNO|wxICON_INFORMATION);
                if (choice==wxOK) // Close the tab, and re-open again.
                  GDocumentManager->CloseDocument(openDoc);

                view = GDocumentManager->OpenFile(files[i]);
              }
              else
              {
                // Do nothing, for re-open files. No point in opening them again!
                view = openDoc;
              }
            }
          }

          if (files.size()==1) GDocumentTabManager->BringIntoFocus(view);
        }

        break;
      }
    case IDC_BtnSaveFile:
    case MENU_ON_SAVE:
      {
        bool breakOnSave = false;
        DocumentViewer *focusDoc = GDocumentManager->GetCurrentDocument();
        if (focusDoc)
        {
          // Check for relative paths
#if _SD_ENGINE_FILE_NOSAVE
          if ((focusDoc->GetDocView() & DOC_ORIGINAL) && focusDoc->GetFilePath().Length() && focusDoc->GetFilePath()[0]!='\\' )
#else
          if (focusDoc->GetFilePath().Length() && focusDoc->GetFilePath()[0]!='\\' )
#endif
          {
            focusDoc->SaveDocument(focusDoc->GetFilePath());
            breakOnSave = true;
          }
        }

        if (breakOnSave) break;
      }
    case IDC_BtnSaveAs:
    case MENU_ON_SAVEAS:
      {
        // Get the current selected document...
        DocumentViewer *focusDoc = GDocumentManager->GetCurrentDocument(); 
        wxString docName = focusDoc ? focusDoc->GetName() : wxEmptyString;
#if _SD_ENGINE_FILE_NOSAVE
        const TCHAR* saveDialogTitle = (focusDoc && (focusDoc->GetDocView()&DOC_ENGINE || focusDoc->GetDocView()&DOC_ENGINE_PP)) ? L"Save Engine File As" : L"Save File As";
#endif

        // Open the specified sqf or sqs file
        wxFileDialog SaveDialog
        (
		      this, 
#if _SD_ENGINE_FILE_NOSAVE
          saveDialogTitle, wxEmptyString, docName, 
#else
          _("Save File As"), wxEmptyString, docName, 
#endif
		      _("Scripts (*.sqf, *.sqs)|*.sqs;*.sqf"),wxFD_SAVE|wxFD_OVERWRITE_PROMPT, wxDefaultPosition
         );

        // Creates a Save Dialog with 4 file types
        if (SaveDialog.ShowModal() == wxID_OK) // If the user clicked "OK"
        {
          wxString saveTo = SaveDialog.GetPath();
          if (focusDoc) focusDoc->SaveDocument(saveTo);

          if (focusDoc && focusDoc->GetDocContents()&DOC_VM)
          {
            // Clear the contents and history...
            focusDoc->ClearCurrentView();

            // Open the new document
            DocumentViewer *newDoc = GDocumentManager->OpenFile(saveTo);
            if (newDoc) GDocumentTabManager->BringIntoFocus(newDoc);
          }
          else
          {
             if (focusDoc)  focusDoc->SetFilePath(saveTo);
          }
        }
      }
      break;
    case MENU_ON_EXIT: OnQuit(event); break;
#if _SD_DEBUG_LOG
    case MENU_ON_DEBUG: OnDebugWindow(event); break; //restore Debug output window
#endif
#if _SD_SCRIPT_LOG
    case MENU_ON_SCRIPTLOG: //Restore script log window
      {
        if (!_scriptLogWindow)
        {
          RegisterScriptLogWindow(new DebugOutPutWindow(this,wxT("Script Log"),WND_SCRIPT_LOG));
        }
        else
        {
          _scriptLogWindow->RenewWindow(BottomNoteBook);
        }

      }
      break;
#endif
#if _VBS_14573_DEBUG_CONSOLE
    case MENU_ON_DEBUGCONSOLE:
      {
        if (!_debugConsole)
        {
          RegisterDebugConsoleWindow(new DebugConsoleWindow(this, wxID_ANY, wxT("DebugConsole")));
        }
        else
        {
          _debugConsole->RenewWindow(BottomNoteBook);
        }
      }
      break;
#endif
    case MENU_ON_PERF: OnPerfWindow(event); break;
    case MENU_ON_VM: //restore the file tree panel
      {
        wxAuiPaneInfo& pane = m_mgr.GetPane(LeftPanelNoteBook);
        if (!pane.IsShown())
        {
          m_mgr.LoadPaneInfo(_auiFilePaneDefaultView,m_mgr.GetPane(LeftPanelNoteBook));
          m_mgr.Update();
        }
        break;
      }
#if _SD_EH_TAB 
    case MENU_ON_HANDLER: // EH pane restore
      {
        wxAuiPaneInfo& pane = m_mgr.GetPane(PropertyGridPanel);
        if (!pane.IsShown())
        {
          m_mgr.LoadPaneInfo(_auiEHPaneDefaultView,m_mgr.GetPane(PropertyGridPanel));
          m_mgr.Update();
        }
        break;
      }
#endif
    case MENU_ON_DEFAULT_VIEW:// Generic default display.
      m_mgr.LoadPerspective(_auiMgnerView); // Load the default perspective, all panels
      //and reset the bottom notebook
      SetUpBottomNotebook(true);
      m_mgr.Update();
      //also set default size and pos to the window
      SetSize(wxSize(800,600));
      Move(wxDefaultPosition);
      break;
    case MENU_ON_DEBUG_VIEW_DEFAULT: // reset the debug pane
        //reset the botom pane
        m_mgr.LoadPaneInfo(_auiDebugPaneDefaultView, m_mgr.GetPane(BottomNoteBook));
        //reset the bottom notebook and it's contents
        SetUpBottomNotebook(true);
        m_mgr.Update();
      break;
    case MENU_ON_VIEW_ERROR_MSG:
      if (BottomNoteBook->GetPageIndex(_errorManager)==-1) BottomNoteBook->AddPage(_errorManager,L"Error log");
      BottomNoteBook->SetSelection(BottomNoteBook->GetPageIndex(_errorManager));
      if (id == MENU_ON_VIEW_ERROR_MSG) break;
    case MENU_ON_VIEW_CALL_STACK:
      if (BottomNoteBook->GetPageIndex(_callStackManager)==-1) BottomNoteBook->AddPage(_callStackManager,L"Call Stack");
      BottomNoteBook->SetSelection(BottomNoteBook->GetPageIndex(_callStackManager));
      if (id == MENU_ON_VIEW_CALL_STACK) break;
    case MENU_ON_VIEW_WATCH:
      if (BottomNoteBook->GetPageIndex(_watchManager)==-1) BottomNoteBook->AddPage(_watchManager,L"Memory Watcher");
      BottomNoteBook->SetSelection(BottomNoteBook->GetPageIndex(_watchManager));
      if (id == MENU_ON_VIEW_WATCH) break;
      // Load the notebook default view
     // NoteBook->LoadPerspective(_auiTabMgnerView); 
      break;

    case IDC_BtnDisconnect:
      {
        // Forward declarations
        wxBitmap& disconnect_bmp_to_wx_bitmap();
        wxBitmap& connect_bmp_to_wx_bitmap();

        wxAuiToolBarItem *connect = ToolBarScriptDebugger->FindTool(IDC_BtnDisconnect);

        GSettings.EnableDebugging() = !GSettings.EnableDebugging();
#if _VBS3_SD_IMP_BREAKPOINTS
        if (GVBSApp)
          GVBSApp->EnableDebugging(GSettings.EnableDebugging());
#endif
        connect->SetBitmap(GSettings.EnableDebugging()?connect_bmp_to_wx_bitmap():disconnect_bmp_to_wx_bitmap());
        connect->SetLabel(GSettings.EnableDebugging()?wxT("Disconnect"):wxT("Connect"));
        connect->SetShortHelp(GSettings.EnableDebugging()?wxT("Disconnect Debugger"):wxT("Connect Debugger"));
        // If disconnect is pressed, the continue all script execution
        if (!GSettings.EnableDebugging()) OnContinueAll();

        ToggleTitleWindow();

        ToolBarScriptDebugger->Refresh();
      }
      break;
    case IDC_BtnScriptLog:
      {
        GSettings.SetEnableScriptLog(!GSettings.EnableScriptLog());

        wxAuiToolBarItem * scriptLog = ToolBarScriptDebugger->FindTool(IDC_BtnScriptLog);
        if (scriptLog)
        {
          scriptLog->SetBitmap(GSettings.EnableScriptLog()?wxMEMORY_BITMAP(icon_script_log_on):wxMEMORY_BITMAP(icon_script_log_off));
#if _VBS_8320_TOOLTIP_FIX
          scriptLog->SetLabel(GSettings.EnableScriptLog()?wxT("Disable Script Log"):wxT("Enable Script Log"));
          scriptLog->SetShortHelp(GSettings.EnableScriptLog()?wxT("Disable Script Log"):wxT("Enable Script Log"));
#else
          scriptLog->SetLabel(GSettings.EnableScriptLog()?wxT("Enable Script Log"):wxT("Disable Script Log"));
          scriptLog->SetShortHelp(GSettings.EnableScriptLog()?wxT("Enable Script Log"):wxT("Disable Script Log"));
#endif
        }
        ToolBarScriptDebugger->Refresh();
      }
      break;

    case MENU_ON_SETTINGS:
      if (!_settingWindow) _settingWindow = new DialogSettingsEx(this);
      else _settingWindow->Raise();
      break;
    case MENU_ON_DOC_STYLE:
      if (!_docStyleWindow) _docStyleWindow = new DialogStyleEx(this);
      else _docStyleWindow->Raise();
      break;
	case MENU_ON_ABOUT:
		{
			wxAboutDialogInfo info;
			info.SetName(INFO_APPNAME);
			info.SetVersion(APP_VERSION);
			info.SetCopyright(INFO_COPYRIGHT);
			info.AddDeveloper(INFO_DEVELOPER);
			wxAboutBox(info);
			break;
		}
    case TM_EVENT_TOGGLE_BP:
      OnToggleBPOnFocus();
      break;
    case MENU_ON_SAVE_LAYOUT:
      {
        SaveWindowLayoutToSettings();
        GSettings.SaveConfigFile();
        break;
      }
    default:
      // Handle the case when the document is changing its encoding
      if (id>=MENU_CP_CentralEurope && id<=MENU_CCP_UTF16)
      {
        // Safe now to change its encoding....
        Encoding newEnc = FromWxMenuIDToEncoding(id);
        DocumentViewer *doc = GDocumentManager->GetCurrentDocument();
        if (doc)
        {
          // Tell the document to change its underlying encoding, and update itself
          // Update the menu
          doc->SetEncoding(newEnc);
          DocumentToforeground(doc);          
        }
      }
    break;
  }

  // Stop the event from proper gating.
  event.Skip(false);
}


#define WXK_A 0x41
#define WXK_B 0x42
#define WXK_C 0x43
#define WXK_D 0x44
#define WXK_E 0x45
#define WXK_F 0x46
#define WXK_G 0x47
#define WXK_H 0x48
#define WXK_I 0x49
#define WXK_J 0x4A
#define WXK_K 0x4B
#define WXK_L 0x4C
#define WXK_M 0x4D
#define WXK_N 0x4E
#define WXK_O 0x4F
#define WXK_P 0x50
#define WXK_Q 0x51
#define WXK_R 0x52
#define WXK_S 0x53
#define WXK_T 0x54
#define WXK_U 0x55
#define WXK_V 0x56
#define WXK_W 0x57
#define WXK_X 0x58
#define WXK_Y 0x59
#define WXK_Z 0x5A

int MyApp::FilterEvent(wxEvent& event)
{
    if ((event.GetEventType() == wxEVT_KEY_DOWN))
    {
        wxKeyEvent& keyEvent = (wxKeyEvent&)event;

        if (keyEvent.GetModifiers() == wxMOD_CONTROL) //proper way of checking for key modifiers (takes care of all the possible combinations)
        {
          switch (keyEvent.GetKeyCode())
          {
          case WXK_O:
            {
              wxCommandEvent newEvent;
              newEvent.SetId(MENU_ON_OPEN); // Open file
              GFrame->OnCommandEvent(newEvent);
              return true; //fully processed
            }
          case WXK_S:
            {
              wxCommandEvent newEvent;
              newEvent.SetId(MENU_ON_SAVE); // save feature
              GFrame->OnCommandEvent(newEvent);
              return true;
            }
          case WXK_F5:
            {
              GFrame->OnContinueAll(); //continue all
              return true;
            }
#if _VBS_1428_STEP_FILE
          case WXK_F11:
            {
              GFrame->OnStepFile(wxCommandEvent());
              return true;
            }
#endif
          default:
            return -1; //not processed, leave evt processing to wxWidgets
          }
        }
        else if (keyEvent.GetModifiers() == wxMOD_SHIFT)
        {
          switch (keyEvent.GetKeyCode())
          {
          case WXK_F11:
            {
              GFrame->OnStepOut(wxCommandEvent());
              return true;
            }
          default:
            return -1;
          }
        }

        switch (keyEvent.GetKeyCode()) //remove unnecessary cast
        {
          case WXK_F12: 
            {
              wxCommandEvent newEvent;
              newEvent.SetId(MENU_ON_SAVEAS); // save as feature
              GFrame->OnCommandEvent(newEvent);
            }
            break;
          case WXK_F11:
            GFrame->OnStepInto(wxCommandEvent());
            break;
          case WXK_F10:
#if _SD_ALLOW_SYSKEY_OVERRIDE
            if (GFrame->IsScriptBreaked())
            {
              wxWindow* wnd = dynamic_cast<wxWindow*> (event.GetEventObject());
              if (wnd)
                wnd->SetSysKeyOverride(true);
#endif
              GFrame->OnStepOver(wxCommandEvent());
#if _SD_ALLOW_SYSKEY_OVERRIDE
              return true; //only mark the event has 'handled' when we actually stepped
            }
            return false;
#else
            break;
#endif
          case WXK_F9:
            GFrame->OnToggleBPOnFocus();
            break;
          case WXK_F5:
          {
            GFrame->OnContinue(wxCommandEvent());
            return true;
          }
#if _SD_BREAK_ALL
          case WXK_F4:
          {
            GFrame->OnBreakAll(wxCommandEvent());
            return true;
          }
#endif    
          default:
            return -1;
        }
        return true;
    }
 
    return -1;
}

void MyFrame::ToggleTitleWindow()
{
#if _VBS_14850_SHOW_FULL_FILE_PATH
  wxString filePath;
  if (GDocumentManager)
  #if _VBS_15079_RESTRICT_BISIM_SCRIPTS
  {
    if (GDocumentManager->GetCurrentDocument()->ContentsProtected() && GDocumentManager->GetCurrentDocument()->GetFilePath().length() )
      filePath = L"Internal Script";
    else
      filePath = GDocumentManager->GetCurrentDocument()->GetFilePath();
  }
  #else
    filePath = GDocumentManager->GetCurrentDocument()->GetFilePath();
  #endif
  
  wxString connected(L"Connected");
  if (!GSettings.EnableDebugging())
    connected = wxString(L"Disconnected");

#if _VBS3_SD_WND_TITLE
  wxString wndTitlePrefix = _vbsWindowTitle.Length() ? wxString::Format(L"[%s] ",_vbsWindowTitle) : L"";
  if (filePath.length() > 0)
    SetTitle(wxString::Format(wxT("%sScriptDebugger: %s - %s"), wndTitlePrefix, connected, filePath));
  else    
    SetTitle(wxString::Format(wxT("%sScriptDebugger: %s"), wndTitlePrefix, connected));   
#else
  if (filePath.length() > 0)
    SetTitle(wxString::Format(wxT("ScriptDebugger: %s - %s"), connected, filePath));
  else    
    SetTitle(wxString::Format(wxT("ScriptDebugger: %s"), connected));
#endif
#else
  if (GSettings.EnableDebugging())
    SetTitle(wxT("ScriptDebugger: Connected"));
  else
    SetTitle(wxT("ScriptDebugger: Disconnected - %s"));
#endif
}

#if _VBS3_SD_WND_TITLE
void MyFrame::WindowTitleChange(const char * title)
{
  _vbsWindowTitle = wxString::FromUTF8(title);
  ToggleTitleWindow();
}
#endif

void MyFrame::OnCancel(wxCommandEvent &event)
{
}

// Constructor never called.
MyApp::MyApp(){}
MyApp::~MyApp(){}

int MyApp::OnExit()
{ 
#if _VBS_15068_PROPER_SHUTDOWN
  GFrame->Close(true); //close the widgets normally before app exit
#endif
  return base::OnExit();
}


bool MyApp::OnInit()
{
  GFrame = new MyFrame();
  return true;
}

void MyFrame::Startup(){}
//called by VBS when it unloads the plugin
void MyFrame::Shutdown(){}

#if _VBS3_DEBUG_DELAYCALL
void MyFrame::OnDelayBreak(wxCommandEvent &event)
{
  VMTreeContextMenu * menu = dynamic_cast<VMTreeContextMenu*>(event.GetEventObject());

  if (menu)
  {   
    DebugScript* script = menu->script;
    if (script)
    {
      script->SetShouldBreak(!script->ShouldBreak()); //toggle the should break
      _treeManager->DelayBreakChanged(script);
      event.Skip(false);
    }
  }
}
#endif

void MyFrame::OnBreak(wxCommandEvent &event)
{
  LogF("Single script break\n");
  if (_focus && _focus->GetIDebugScript())
  {
    _focus->SetBreak(true);
    _focus->GetIDebugScript()->RunScript(false);
  }
}

void MyFrame::OnContinue(wxCommandEvent &event)
{
  LogF("Continue single script\n");
  if (_focus && _focus->GetIDebugScript())
  {
    _focus->SetBreak(false);
    _focus->GetIDebugScript()->RunScript(true);

    // Remove the display documents line number indicator
    DocumentViewer *doc = _focus->GetDisplayDoc();
    if (doc) doc->SetLineNumber();

#if _SD_VM_TREE
    // Update the tree manager
    _treeManager->BreakPointLeave(_focus);
#endif
  }
}

#if _SD_BREAK_ALL
void MyFrame::OnBreakAll(wxCommandEvent &event)
{
  SetBreakAll(!GBreakAll); //toggle the value
  LogF("BreakAll toggle \n");
}

void MyFrame::OnContinueAll(wxCommandEvent &event)
{
  OnContinueAll();
}
#endif

void MyFrame::UpdateLeftPanelNoteBookAUICaption()
{
  int index = LeftPanelNoteBook->GetSelection();
  if (index>=0)
  {
    wxString pageText = LeftPanelNoteBook->GetPageText(index);

    wxAuiPaneInfo &paneInfo = m_mgr.GetPane(LeftPanelNoteBook); 
    paneInfo.caption = pageText;
    m_mgr.Update();
  }
}

void MyFrame::OnLeftPanelNoteBookChange(wxFlatNotebookEvent &event)
{
  UpdateLeftPanelNoteBookAUICaption();
}


void MyFrame::OnContinueAll(bool breakAll)
{
  LogF("Continue all scripts\n");
  SetBreakAll(breakAll);

  // Tell all scripts to continue;
  for (size_t i=0; i<_scripts.size(); i++) 
  {
    // On continue all, reset the focus back to the main
    // window
    if (_scripts[i]->IsRunning()&&_scripts[i]->IsBreaked())
    {
        // Remove the display documents line number indicator
        DocumentViewer *doc = _scripts[i]->GetDisplayDoc();
        if (doc) doc->SetLineNumber();

        SetForegroundWindow(GHWND);
    }

#if _VBS3_DEBUG_DELAYCALL
    if (_scripts[i]->GetIDebugScript())
    {
#endif
      _scripts[i]->SetBreak(false);
      _scripts[i]->GetIDebugScript()->RunScript(true);
#if _VBS3_DEBUG_DELAYCALL
    }
#endif

#if _SD_VM_TREE
    // Update the tree manager
    _treeManager->BreakPointLeave(_scripts[i]);
#endif
  }
}

wxString MyFrame::GetScriptName(IDebugScript *script)
{
  const TCHAR *scriptName = L"ScriptVM";
  
  switch (script->GetType())
  {
    case STDirect:
      {
        if (script->GetId()==-1)
        {
          scriptName = L"SubScript";
        }
        else
        {
#if _SD_EH_TAB
          unsigned int eventGroup = HIWORD(script->GetId());
          unsigned int eventId    = LOWORD(script->GetId());

          // Get the event name
          scriptName =_eventManager->GetEventName(eventGroup,eventId);
#endif
        }
      }
      break;
    case STSQS:
      scriptName = L"SQS";
      break;
    case   STSQF:
      scriptName = L"SQF";
      break;
    default:
      break;
  }

  // Try to get the scripts file name.
  {
    // Get the file name.
    int size = MAX_PATH;
    char path[MAX_PATH] = {0};
    script->GetFilePath(path,size);

    wxString convertPath = CharToUTF16(path);
    if (convertPath.Length()) return wxString(scriptName) + wxString(L":") + convertPath;
  }

  return wxString(scriptName);
}

void MyFrame::ScriptCall(const char * filename)
{
#if _SD_SCRIPT_LOG
  if (GSettings.EnableScriptLog())
  {
#if _VBS_15079_RESTRICT_BISIM_SCRIPTS || _SD_DELAYCALL_BETTER_LOG
    LogScript(L"[Script Call]: ", wxString::FromUTF8(filename));
#else
    if (_scriptLogWindow)
    {
      wxString line = wxT("[Script Call]: ") + wxString::FromUTF8(filename) + wxT("\n");
      _scriptLogWindow->AppendDebugText(line,true);
    }
#endif
  }
#endif
}

#if _VBS3_DEBUG_DELAYCALL
void MyFrame::FlushDelayCalls()
{
  for (size_t i=0; i < _scripts.size(); ++i)
  {
    if (_scripts[i]->GetType() == DS_DELAYCALL)
    {
      _treeManager->RemoveControlTreeItem(_scripts[i]);
      _scripts[i]->SetDelayCall(NULL);
      Reduce(_scripts,i);
    }
  }
}

void MyFrame::OnDelayCall(IDebugDelayCall * delayCallPtr, bool added)
{
  if (added)
  {
    DebugScript* script = new DebugScript();
    DebugDelayCall* delayCall = new DebugDelayCall(delayCallPtr);
    script->SetDelayCall(delayCall);
#if _SD_SCRIPT_LOG
    if (_scriptLogWindow)
    {
#if _SD_DELAYCALL_BETTER_LOG
      wxString callType = delayCall->GetId() % 2 == 0 ? L"DelayCall" : L"DelayUICall";
      LogScript(wxString::Format(L"[%s, %f]: ",callType, delayCall->GetDelay()), delayCall->GetName());
#else
  #if _VBS_15079_RESTRICT_BISIM_SCRIPTS
      LogScript(wxString::Format(L"DelayCall :%f",delayCall->GetDelay()), delayCall->GetName());
  #else
      _scriptLogWindow->AppendDebugText(wxString::Format(wxT("[Delay call, %f]: "), delayCall->GetDelay()) + delayCall->GetName());
  #endif
#endif
    }
#endif
#if _SD_VM_TREE
    _treeManager->AddNewControlTreeItem(script,L"");
#endif

    _scripts.push_back(script);
  }
  else
  {
#if _SD_VM_TREE
    for (size_t i = 0; i < _scripts.size(); ++i)
    {
      if (_scripts[i]->GetDelayCall())
      {
        if (_scripts[i]->GetDelayCall()->GetIDebugDelayCall() == delayCallPtr)
        {
          _scripts[i]->GetDelayCall()->SetCodeExecuted();
          _treeManager->ForceUpdate(_scripts[i]);
          break;
        }
      }
    }
#endif
  }
}
#endif


void MyFrame::Simulate(float deltaT)
{
#if _VBS3_SD_IMP_BREAKPOINTS
  GCurrentMissionDir.Update(deltaT);
#endif
  OnAllSimulate(deltaT);
}

#if _SD_ALLOW_SYSKEY_OVERRIDE
bool MyFrame::IsScriptBreaked() const
{
  return (_focus && _focus.GetRef()->GetIDebugScript() && _focus.GetRef()->IsBreaked());
}
#endif

// Some script is loaded, attach to every script. This can be optimized
void MyFrame::ScriptLoaded(IDebugScript *script, const char *name)
{
  wxString scriptName = GetScriptName(script);
  if (GSettings.EnableScriptLog())
  {
    if (scriptName.Len() != 0)
    {
#if _SD_SCRIPT_LOG
  #if _VBS_15079_RESTRICT_BISIM_SCRIPTS || _SD_DELAYCALL_BETTER_LOG
    LogScript(L"[Script Loaded]: ", scriptName);
  #else
      if (_scriptLogWindow)
      {
        wxString line(wxT("[Script Loaded]: " + scriptName + wxT("\n")));
        _scriptLogWindow->AppendDebugText(line,true);
      }
  #endif
#endif
    }
  }
#if !_SD_TRACK_SCRIPTS_DC
  if (GSettings.EnableDebugging()) 
  {
#endif //!_SD_TRACK_SCRIPTS_DC
#if _VBS3_DEBUG_DELAYCALL
    //if the script is a delay call
    if (script->GetType() == STDelayCallDirect || script->GetType() == STDelayCallVM)
    {
      for (size_t i=0; i<_scripts.size(); ++i)
      {
        if (_scripts[i]->GetDelayCall())
        {
          DebugDelayCall* delayCall = _scripts[i]->GetDelayCall();
          if (delayCall->GetId() == script->GetDelayCallId())
          {
  #if _SD_VM_TREE
            _treeManager->ForceUpdate(_scripts[i]);
  #endif
            _scripts[i]->SetIDebugScript(script);
            break;
          }
        }
      }
    }
    else
    {
#endif
      // Every script gets it's own object
      DebugScript *newScript = new DebugScript(script);

      // Add to the global script and also to the tree manager..
      _scripts.push_back( newScript );

#if _SD_VM_TREE
      // Do not put script direct into the tree manager, they're run at a very fast rate
      if (script->GetType()!=STDirect)
        _treeManager->AddNewControlTreeItem(newScript,scriptName);
#endif
#if _VBS3_DEBUG_DELAYCALL
    }
#endif
#if !_SD_TRACK_SCRIPTS_DC
  } 
#endif //!_SD_TRACK_SCRIPTS_DC

  script->AttachScript();  
}

void MyFrame::SetupStepInto(IDebugScript *script, bool stepInto)
{
  // Only break into direct scripts, when they're selected from
  // the event handler. Or you cannot do anything!
  if (stepInto)
  {
    DebugScript *debugScript = GetDebugScript(script);
#if _SD_TRACK_SCRIPTS_DC
    if (!debugScript) return; //if for some reason we can't get the script handle, do nothing
#endif
#if _SD_VM_TREE
    // Direct scripts need to be added to the tree manager.      
    if (script->GetType()==STDirect)
    {
      if (!_treeManager->GetScriptTreeItem(debugScript).IsOk())
      {
        // Not present, add the new tree item.
        wxString scriptName = GetScriptName(script);
        _treeManager->AddNewControlTreeItem(debugScript,scriptName);
      }
    }
#endif
    // Script is set to breaked
    debugScript->SetBreak(true);

    // Select the item for the user in the pull down list.
    //_treeManager->SelectTreeScript(debugScript);

    // This will call the MyFrame::Breaked To indicate that the script stopped on our command
    script->Step(SKInto,SUInstruction);

    // Set the current breaked script as the focus script
    //SetFocus(debugScript);

    // Show the window as topmost, when there is a breakpoint
    //SetForegroundWindow((HWND)GetHWND());
  }
  else
  {
      // If no proccessing is required just tell the script to run normally.
      script->RunScript(true);
  }
}

/*
 The script we attached to is now entered, we can do some proccessing here
*/

// Sometimes there is a loopback into the engine that loop back to us...
int GRecursiveLoopProtection;
void MyFrame::ScriptEntered(IDebugScript *script)
{
  // Tell the script that it has been entered.
  script->EnterScript();

  bool breakScript = false;
  if (GSettings.EnableDebugging())
  {
    if (!GRecursiveLoopProtection)
    {
      GRecursiveLoopProtection = true;
#if !_VBS3_SD_IMP_BREAKPOINTS
      GDocumentManager->SetAllBreakPoints(script);
#endif
      GRecursiveLoopProtection = false;
    }

    breakScript = GBreakAll;
#if _VBS3_DEBUG_DELAYCALL
    DebugScript * debugScript = GetDebugScript(script);
    if (debugScript && debugScript->ShouldBreak())
    {
      breakScript = true;
      debugScript->SetShouldBreak(false); //break only once
    }
    if (!breakScript && script->GetType()==STDirect)
#else
    if (script->GetType()==STDirect)
#endif
    {
#if _SD_EH_TAB
      if (script->GetId()!=-1)
      {  
        unsigned int id = script->GetId();
        unsigned int eventGroup = HIWORD(id);
        unsigned int eventId= LOWORD(id);

        breakScript = _eventManager->IsEventSelected(eventGroup,eventId);
      }
      else
      {
        // Is a sub script
        // only break script if the sub script has a parent
        //breakScript = script->GetParent();
      }
#endif
    }
  }
  
  // The ScriptDebugger may be all-ready executing a execute command
  // do not break until that script command has finished.
  if (GRecursiveLoopProtection) breakScript = false;

  // Determine to break the script or not
  SetupStepInto(script,breakScript);
}

void MyFrame::ScriptSimulationEntered(IDebugScript *script)
{
   if (GSettings.EnableDebugging())
   {
     DebugScript* debugScript = GetDebugScript(script);
     
     // Tell the script its current running, can
     // evaluate its context stack
     if (debugScript)
     {
       debugScript->SetRunning(true);
#if _SD_VM_TREE
       if (debugScript->IsBreaked())
       {
         // Going to be debugged update the UI
         // its now been selected
         _treeManager->BreakPointEnter(debugScript);
#if _VBS3_SCRIPT_LINES
         debugScript->Update();
#endif
       }
#endif
     }
   }
}


void MyFrame::ScriptSimulationExited(IDebugScript *script)
{
  if (GSettings.EnableDebugging())
  {
    DebugScript* debugScript = GetDebugScript(script);
    if (debugScript)
    {
      // Only update the UI, for script's that have been instructed
      // to break
      if (debugScript->IsBreaked())
      {
#if _SD_VM_TREE
        _treeManager->BreakPointLeave(debugScript);
#endif
        // Return focus back to the main window if needed
        if (debugScript->GetVBSFocusToggle())
        {
          debugScript->SetVBSFocusToggle(false); //prevent VBS stealing focus from the same script over and over
          SetForegroundWindow((HWND)GHWND);
        }
      }

      // Script no longer is running.
      debugScript->SetRunning(false);
    }
  }
}

/// The script is now terminated do any clean up management here
void MyFrame::ScriptTerminated(IDebugScript *script)
{ 
  bool updateAll  = false;

  if (GSettings.EnableScriptLog())
  {
    wxString scriptName = GetScriptName(script);
    if (scriptName.Len() != 0)
    {
#if _SD_SCRIPT_LOG
  #if _VBS_15079_RESTRICT_BISIM_SCRIPTS || _SD_DELAYCALL_BETTER_LOG
    LogScript(L"[Script Terminated]: ", scriptName);
  #else
      if (_scriptLogWindow)
      {
        wxString line(wxT("[Script Terminated]: " + scriptName + wxT("\n")));
        _scriptLogWindow->AppendDebugText(line,true);
      }
  #endif
#endif
    }
  }

  SmartRef<DebugScript> debugScript = GetDebugScript(script);
  if (debugScript)
  {
    if (debugScript==_focus)
    {
      // Focus to nothing.
      SetFocus(NULL);
      updateAll = true;
    }
#if _SD_VM_TREE
    _treeManager->RemoveControlTreeItem(debugScript);
#endif

    // Remove the display documents line number indicator
    DocumentViewer *doc = debugScript->GetDisplayDoc();
    if (doc) doc->SetLineNumber();

    // Quick fix right now...
    bool found = false;
    for (size_t i=0; i<_scripts.size(); i++)
    {
      if (_scripts[i]==debugScript)
      {
        Reduce(_scripts,i);
        found = true;
        break;
      }
    }

    // Remove the script in-case somebody else has a reference count to it.
    debugScript->SetIDebugScript(NULL);
    debugScript->SetBreak(false);

    // Should all ways be found and deleted
    Assert(found);
  }

  if (updateAll) UpdateView();
}

#if _VBS3_SCRIPT_LINES
extern ScriptBreakpointsManager GBreakpointManager;
#endif
void MyFrame::FireBreakpoint(IDebugScript *script, unsigned int bp)
{
  // Setup the step into
  SetupStepInto(script,true);
  // Break the script, gain focus and display to the user
#if _VBS3_SCRIPT_LINES
  ScriptBreakpoint &bpStruct = GBreakpointManager.GetBreakpoint(bp);
  Breaked(script, bpStruct._filenameEngine.c_str(), bpStruct._sourceLineNumber+1);
#else
  Breaked(script);
#endif
}

/*
 This is a tricky one, because it has the following assumptions.
 
 When the script is placed into a  script->Step   SKInto or SKOver or SKOut it 
 will Initialize the script to DSStepInit. 

 When the script is run, and it hits DSStepInit, it will initialize the stepping system 
 as a state of DSStep internally. This will constantly hit the Breaked virtual
 function until the condition is not met inside the step. This does NOT stop the script
 instead it set's the next line to proceed to.

 You can do a message pump, this will enable you inside Breaked to halt the execution
 until the user is free to make a decision. 

 If inside break, or ScriptEnter, if the script is placed into a non running state
 with 
 script->RunScript(false);

 The engine will halt execution of the script and process messages. This allows
 the user to use the user interface to toggle to the next line. This is not recommended
 because it is better to 
 */
#if _VBS3_SCRIPT_LINES
void MyFrame::Breaked(IDebugScript *script, const char * filename, int line)
#else
void MyFrame::Breaked(IDebugScript *script)
#endif
{
  DebugScript *debugScript = GetDebugScript(script);
  // Check to see if the script was requested to be breaked.
  if (debugScript && debugScript->IsBreaked())
  {
#if _VBS3_SCRIPT_LINES
    //update the current instruction pointer in debugscript
    debugScript->SetCurrInstr(filename, line);
#endif
#if _SD_VM_TREE
    _treeManager->BreakPointEnter(debugScript);
#endif

    // The reason why, RunScript is used here. Is because
    // The internal engine calls its own IsAlive function to make
    // sure VBS2 doest crash.
    script->RunScript(false);    

    //get the context
    // See if the debugger is trying to step out/over some statement
    if (debugScript->IsStepping())
    {
      debugScript->SetStepping(false); //we finished the step by breaking
    }
    debugScript->SetVBSFocusToggle(true); //since we gain focus on breaked, we should give focus back to VBS after we unbreak

    // Breaked script should gain the focus
    SetFocus(debugScript);

    // Set the top level, to display the result
    if (_focus) _focus->SetTopLevelCallStack();

    // Update the view to reflect the new state!
    UpdateView();
  
    // Get the paths of the script....
#define _DEBUG_ON_SCRIPT_FILE 0
#if _DEBUG_ON_SCRIPT_FILE
    if (debugScript->GetScriptFilePath()==debugScript->GetCallStackFilePath())
    {
      // Set the focus, to the script debugger!
      SetForegroundWindow((HWND)GetHWND());
    }
    else
    {
      // Not equal, keep going.
      debugScript->GetIDebugScript()->Step(SKInto,SULine);
    }
#else
    if (!Show()) Show(); // Show the window if its hidden in the taskbar
    SetForegroundWindow((HWND)GetHWND());
#endif
  }
  else
  {
      // Set script to running
      script->RunScript(true);

      // Update the view to reflect the new state!
      UpdateView();
  }
}

void MyFrame::OnStepInto(wxCommandEvent &event)
{
  // This should really be inside the virtual machine from now on....
  if (_focus && _focus->GetIDebugScript() && _focus->IsBreaked())
  {
    _focus->GetIDebugScript()->Step(SKInto,SULine);
  }
  UpdateView();
}

#if _VBS_1428_STEP_FILE
void MyFrame::OnStepFile(wxCommandEvent &event)
{
  if (_focus && _focus->GetIDebugScript() && _focus->IsBreaked())
  {
    _focus->GetIDebugScript()->Step(SKFile,SULine);
    _focus->SetStepping(true);
    _focus->SetStepType(SKFile);
  }
  UpdateView();
}
#endif


void MyFrame::OnStepOver(wxCommandEvent &event)
{
  // This should really be inside the virtual machine from now on....
  if (_focus && _focus->GetIDebugScript() && _focus->IsBreaked())
  {
    _focus->GetIDebugScript()->Step(SKOver,SULine);
    _focus->SetStepping(true);
    _focus->SetStepType(SKOver);
  }
  UpdateView();
}

void MyFrame::OnStepOut(wxCommandEvent &event)
{
  // This should really be inside the virtual machine from now on....
  if (_focus && _focus->GetIDebugScript() && _focus->IsBreaked())
  {
    _focus->GetIDebugScript()->Step(SKOut,SULine);
    _focus->SetStepping(true);
    _focus->SetStepType(SKOut);
  }
  UpdateView();
}

 void MyFrame::OnToggleBPOnFocus()
 {
   // Toggle the BP on the current focus document line.
   GDocumentManager->ToggleBPOnFocusDocument();
 }

void MyFrame::OnError(int error, const char *content, const char *errorMsg, int line, const IGameState *gamestate)
{
  if (!GSettings.EnableDebugging()) return; // Check if the script debugger is enabled or not

  if (!GLine) GLine = new LineIndicator;

  GLine->ChangeLine(line);
  _doc->GetDocument()->SetTextRaw(content);

  _doc->GetDocument()->MarkerDeleteAll(wxSCI_MARK_ARROW);
  _doc->GetDocument()->MarkerAdd(line,wxSCI_MARK_ARROW);
  _doc->GetDocument()->MarkerDeleteAll(wxSCI_MARK_ARROW);

  // Clear the error msg
  _errorManager->Clear();

    wxString msg = wxString::Format(L"Line:%d Error:%s",line,CharToUTF16(errorMsg));
  _errorManager->SetValue(msg);

  // Control text area need to be updated with the new line.
  _doc->GetDocument()->Refresh();

  // Show the window when an error occurs.
  Show(true);
}

IGameState *GGameState;
void MyFrame::GlobalGameState(IGameState *gameState)
{
  GGameState = gameState;
  // Update the intellisense
  _doc->UpdateIntelliSense();
#if _SD_EH_TAB
  // Update the event manager with events
  _eventManager->PopulateEventHandler();
#endif
}

#if _SD_DEBUG_LOG
void MyFrame::OnDebugWindow(wxCommandEvent& event)
{
  // Create the window.
  if (!_debugWindow)
  {
    RegisterDebugWindow(new DebugOutPutWindow(this, wxT("Debug log"), WND_DEBUG_OUTPUT));
  }
  else
  {
    _debugWindow->RenewWindow(BottomNoteBook);
  };
}
#endif
#if _VBS_14074_DEV_PATH_UPDATE_FIX
void MyFrame::OnDevDriveChanged()
{
  GDocumentManager->UpdateRelativePaths();
#if _VBS_15079_RESTRICT_BISIM_SCRIPTS
  GDocumentManager->UpdateProtectedFiles();
#endif
}
#endif

void MyFrame::OnPerfWindow(wxCommandEvent& event)
{
  if (!_perfWindow) _perfWindow = new DiagFrameRateEx(this);
}

void MyFrame::SetFocus(DebugScript *script)
{
  if (_focus) _focus->LostFocus();
  _focus = script;
  if (_focus) _focus->GainFocus();
}


void MyFrame::SetCurrentDocTo(DebugScript *script)
{
  // Update the focus to reflect what currently has focus.
  bool docChng = _focus != script;
  
  SetFocus(script);

  if (GLine) GLine->Clear();
  UpdateView(docChng);
}

void MyFrame::SetCallStackIndex(int index)
{
  if (_focus)
  {
    IDebugScript *script = _focus->GetIDebugScript();
    if (script)
    {
      int callStackSize = script->CallStackSize();
      for (int i=0; i<callStackSize; i++)
      {
        ICallStackItem *cs = script->GetCallStackItem(i);
        if (!cs->IsInline()) index--;
        if (!index)
        {
          _focus->SetCallStackIndex(i);
          UpdateView(true);
          break;
        }
      }
    }
  }
}

wxString MyFrame::GetAuiPerspective()
{
  return m_mgr.SavePerspective();
}

wxString MyFrame::GetDefaultAuiPerspective() const
{
  return _auiMgnerView;
}

#if _SD_DEBUG_LOG
void MyFrame::RegisterDebugWindow(DebugOutPutWindow * debugWindow)
{
    assert(!_debugWindow);
    if (!_debugWindow && debugWindow)
    {
      _debugWindow = debugWindow;
      if (SDFramework)
        SDFramework->SetDebugWindow(_debugWindow);
    
      if (_debugWindow->IsDocked())
      {
        BottomNoteBook->AddPage(_debugWindow,_debugWindow->GetTitle(),true);
        
        wxUndockableEntry debugLogUndockableInfo;
        debugLogUndockableInfo.notebookTab = _debugWindow;
        debugLogUndockableInfo.windowPtr = _debugWindow;
        BottomNoteBook->registerUndockablePage(debugLogUndockableInfo);
      }
    }
}

void MyFrame::RemoveDebugWindow()
{
  assert(_debugWindow);
  if (_debugWindow)
  {
    _debugWindow->Destroy();
    _debugWindow = NULL;
    if (SDFramework)
      SDFramework->SetDebugWindow(NULL);
  }
}

DebugOutPutWindow * MyFrame::GetDebugWindow()
{
  return _debugWindow;
}
#endif

#if _SD_SCRIPT_LOG
void MyFrame::RegisterScriptLogWindow(DebugOutPutWindow* logWindow)
{
  assert (!_scriptLogWindow);
  _scriptLogWindow = logWindow;
   if (_scriptLogWindow->IsDocked())
  {
    BottomNoteBook->AddPage(_scriptLogWindow,wxT("Script Log"),true);
    
    wxUndockableEntry scriptLogUndockableInfo;
    scriptLogUndockableInfo.notebookTab = _scriptLogWindow;
    scriptLogUndockableInfo.windowPtr = _scriptLogWindow;
    BottomNoteBook->registerUndockablePage(scriptLogUndockableInfo);
  }
}

void MyFrame::RemoveScriptLogWindow()
{
  assert (_scriptLogWindow);
  if (_scriptLogWindow)
  {
    _scriptLogWindow->Destroy();
    _scriptLogWindow = NULL;
  }
}

DebugOutPutWindow* MyFrame::GetScriptLogWindow()
{
  return _scriptLogWindow;
}

  #if _VBS_15079_RESTRICT_BISIM_SCRIPTS || _SD_DELAYCALL_BETTER_LOG
void MyFrame::LogScript(const wxChar * message, wxString scriptFile)
{
#if _SD_DELAYCALL_BETTER_LOG
  if (!_scriptLogWindow || !GSettings.EnableScriptLog())
#else
  if (!_scriptLogWindow)
#endif
    return;

#if _VBS_15079_RESTRICT_BISIM_SCRIPTS
  if (GUtil.IsProtectedPath(scriptFile))
  {
    scriptFile = L"Internal Script";
  }
#else
  if (!scriptFile.Length())
    scriptFile = L"No file path";
#endif

#if _SD_DELAYCALL_BETTER_LOG
  _scriptLogWindow->AppendDebugText(wxString::Format(L"%s %s \n",message,scriptFile), true);
#else
  _scriptLogWindow->AppendDebugText(wxString::Format(L"%s : %s \n",message,scriptFile), true);
#endif
}
  #endif
#endif

#if _VBS_14573_DEBUG_CONSOLE
void MyFrame::RegisterDebugConsoleWindow(DebugConsoleWindow * consoleWindow)
{
  assert (!_debugConsole);
  _debugConsole = consoleWindow;
   if (_debugConsole->IsDocked())
  {
    BottomNoteBook->AddPage(_debugConsole,_debugConsole->GetTitle(),true);
    
    wxUndockableEntry debugConsoleUndockableInfo;
    debugConsoleUndockableInfo.notebookTab = _debugConsole;
    debugConsoleUndockableInfo.windowPtr = _debugConsole;
    BottomNoteBook->registerUndockablePage(debugConsoleUndockableInfo);
  }
}

void MyFrame::RemoveDebugConsoleWindow()
{
  assert (_debugConsole);
  if (_debugConsole)
  {
    _debugConsole->Destroy();
    _debugConsole = NULL;
  }
}

DebugConsoleWindow* MyFrame::GetDebugConsoleWindow()
{
  return _debugConsole;
}
#endif

DiagSearch* MyFrame::GetSearchWindow() const
{
  return _searchWindow;
}

void MyFrame::OnSearchWindowEvt(wxCommandEvent &evt)
{
  switch (evt.GetId())
  {
  case SearchDiagClose:
    {
      _searchWindow = NULL;
      break;
    }
  case SearchDiagUpdateHistory:
    {
      _searchHistory.push_back(_searchWindow->SearchString());
      break;
    }
  default:
    {}
  }
}

void MyFrame::OnSearchWindow(wxScintillaExt* focus)
{
  if (!_searchWindow)
  {
    _searchWindow = new DiagSearch(this,focus,wxID_ANY,wxT("Search file"),SD_SEARCH_CASE_SENS|SD_SEARCH_WHOLE_WORD,wxDefaultPosition,wxDefaultSize,wxDEFAULT_FRAME_STYLE|wxSIZE_AUTO_HEIGHT);
    _searchWindow->SetSearchHistory(_searchHistory);
    _searchWindow->Show();
#if !_SD_UI_IMPROVEMENTS
    _searchWindow->SetFocus();
#endif
  }
  else
  {
#if !_SD_UI_IMPROVEMENTS
    _searchWindow->SetFocus();
#endif
    _searchWindow->SetSearchWindow(focus);
  }
#if _SD_UI_IMPROVEMENTS
  _searchWindow->SetSearchString(focus->GetSelectedText());
  _searchWindow->SetFocus();
#endif
}

void MyFrame::UpdateView(bool docChange)
{
  OnAllStateChangeEvent(NULL,_focus);
}


void MyFrame::DebugEngineLog(const char *str)
{
#if _SD_DEBUG_LOG
  // Output the text to the debug log window
  if (_debugWindow)
  {
      //filter the str
      if (_debugWindow->FilterOutput(str))
      {
        GFrameWork->LogF(str);
      }
  }
#endif
}


DebugScript * MyFrame::GetDebugScript( IDebugScript *script )
{
  for (size_t i=0; i<_scripts.size(); i++)
    if (_scripts[i]->GetIDebugScript()==script) return _scripts[i].GetRef();

  return NULL;
}

void MyFrame::SetBreakAll(bool value)
{
  if (value == GBreakAll)
    return;
  GBreakAll = value;
  //change the button look so we know we're breaked
  wxAuiToolBarItem * breakAllTool = ToolBarScriptDebugger->FindTool(IDC_BtnBreakAll);
  if (breakAllTool)
  {
    if (GBreakAll)
    {
      breakAllTool->SetBitmap(wxMEMORY_BITMAP(icon_control_paused));
      breakAllTool->SetHoverBitmap(wxMEMORY_BITMAP(icon_control_paused));
    }
    else
    {
      breakAllTool->SetBitmap(wxMEMORY_BITMAP(icon_control_pause));
      breakAllTool->SetHoverBitmap(wxMEMORY_BITMAP(icon_control_pause));
    }
    ToolBarScriptDebugger->Refresh(); //repaint the toolbar
  }
}

enum 
{
    PU_SHOW = 10001,
    PU_HIDE,
    PU_EXIT,
};

BEGIN_EVENT_TABLE(MyTaskBarIcon, wxTaskBarIcon)
    EVT_TASKBAR_LEFT_DCLICK(MyTaskBarIcon::OnLeftButtonDClick)
    EVT_MENU(PU_SHOW, MyTaskBarIcon::OnShowWindow)
    EVT_MENU(PU_HIDE, MyTaskBarIcon::OnHideWiddow)
#if _CONSOLE
    EVT_MENU(PU_EXIT, MyTaskBarIcon::OnMenuExit)
#endif
END_EVENT_TABLE()

MyTaskBarIcon::MyTaskBarIcon(MyFrame *mainFrame)
{
  _frame = mainFrame;

  wxIcon icon;
  if (GImgManager.GetWxIcon(ICON_TRAY,icon))
    SetIcon(icon);
}

void MyTaskBarIcon::OnLeftButtonDClick(wxTaskBarIconEvent&)
{
  if (_frame->IsVisible()) _frame->Show(false);
  else _frame->Show(true);
}

#if _CONSOLE
void MyTaskBarIcon::OnMenuExit(wxCommandEvent& )
{
  _frame->Close(true);
}
#endif

void MyTaskBarIcon::OnHideWiddow(wxCommandEvent&)
{
  _frame->Show(false);
}
void MyTaskBarIcon::OnShowWindow(wxCommandEvent&)
{
  _frame->Show(true);
  _frame->Restore(); 
  SetForegroundWindow((HWND)_frame->GetHWND()); //and set focus to it
}

// Overridables
wxMenu *MyTaskBarIcon::CreatePopupMenu()
{
    wxMenu *menu = new wxMenu;
    menu->Append(PU_SHOW, _T("Show Window"));
    menu->Append(PU_HIDE, _T("Hide Window"));
#if _CONSOLE
    menu->AppendSeparator();
    menu->Append(PU_EXIT,    _T("Exit"));
#endif

    return menu;
}


bool MyApp::ProcessIdle()
{
#if _CONSOLE
  // This is a requirement that we call the base class!!!
  base::ProcessIdle();

  // Only do this when the application is a console project.
  EXPORT void WINAPI OnAfterSimulation(float deltaT);
  OnAfterSimulation(1.0f/60.0f);

  // Because of OnAfterSimulation
  // we still say there is messages to be processed
  return true;
#else
  return wxApp::ProcessIdle();
#endif
}

// For both the console, and the DLL version we implement no main method
IMPLEMENT_APP_NO_MAIN(MyApp)

//////////////////////////////////////////////////////////////////////////
// Here is the missing WinAPI dll functions
//////////////////////////////////////////////////////////////////////////
EXPORT void WINAPI OnAfterSimulation(float deltaT)
{
#if !_CONSOLE
  if (wxTheApp) wxTheApp->ProcessIdle();
#endif

  if (GFrame) GFrame->Simulate(deltaT);
}

// Supported version, if abcVersion is not a match then report to the user
#if _VBS3_SCRIPT_LINES
const static int GSupportedVersion = 8;
#elif _VBS3_SD_WND_TITLE
const static int GSupportedVersion = 7;
#elif _VBS3_DEBUG_DELAYCALL
const static int GSupportedVersion = 6; //The interface had to be changed again during BETA dev
#elif _VBS3_SD_IMP_BREAKPOINTS
const static int GSupportedVersion = 4;
#else
const static int GSupportedVersion = 3;
#endif

// VBS2 connecting to us with the interface to dll
#if _VBS3_SD_IMP_BREAKPOINTS
EXPORT IToDll *WINAPI ConnectToScriptDebugger(int abcVersion, IToApp * app)
#else
EXPORT IToDll *WINAPI ConnectToScriptDebugger(int abcVersion)
#endif
{
  wxString msg = wxString::Format(L"Engine interface:%d\r\nScript Debugger interface:%d", abcVersion, GSupportedVersion);

  if (abcVersion!=GSupportedVersion)
  {
    wxMessageBox(msg,L"ScriptDebugger: Incorrect version");

    void CleanWxGUIResources(); // Clean up debugger.
    CleanWxGUIResources();

    return NULL; // Don't return a ABC interface back
  }
#if _SD_LIVEFOLDER_PATHS
  std::wstring exePath = GUtil.GetExeFolderPath(); /* this is something like O:\ng_beta\plugins\ or O:\ng_beta\plugins64\ */
  #ifdef _WIN64
  exePath = exePath.substr(0, exePath.length() - 10); //9 is the length of 'plugins64\'
  #else
  exePath = exePath.substr(0, exePath.length() - 8); //7 is the length of 'plugins\'  
  #endif
  GSettings._liveFolderPath = wxString(exePath.c_str());
  GSettings._liveFolderPath.MakeLower();
#endif
#if _VBS3_SD_IMP_BREAKPOINTS
  GVBSApp = app;
#endif
  return GFrame;
}
