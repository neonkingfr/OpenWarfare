#pragma once

class DebugScript;
class DocumentViewer;

class iViewWindow
{
public:
/**
  * There was a state change update from the main window
  * any component can update itself from here
  * DebugScript can be NULL
  */
  virtual void StateChange(DebugScript*){}
/**  
  * Simulate update allows each drawing component to
  * do some proccessing, during the script simulation
  */
  virtual void SimulateUpdate(float deltaT){}
};

class iMainWindow
{
  /// Communicate to all the modules that their simulate function is called.
  virtual void OnAllSimulate(float deltaT) = 0;
public:
  /// Register window to receive state changes
  virtual void RegisterWindow(iViewWindow *)= 0;
  /// Un-register window
  virtual void UnRegisterWindow(iViewWindow *)=0;

  /// Document has come into view by the user
  virtual void DocumentToforeground(DocumentViewer *){}
  /// Document is about to close
  virtual void DocumentClosing(DocumentViewer *){}
  /// Communicate to the main window, that all window's state change should be called
  virtual void OnAllStateChangeEvent(iViewWindow*,DebugScript*debugScript=0) = 0;
};

/// External declaration of the main window
extern iMainWindow *GMainWindow;