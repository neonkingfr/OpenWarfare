//Lists macros for application info, so we don't have to change them in multiple places in the code

#define INFO_COPYRIGHT L"� Bohemia Interactive Simulations 2013"
#define INFO_APPNAME L"VBS2 Script debugger"
#define INFO_DEVELOPER L"Bohemia Interactive Simulations"