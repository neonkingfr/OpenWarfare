#include "DocumentTreeListManager.hpp"
#include "DocumentManager.hpp"

#include <windows.h>
#include <CommCtrl.h>

#include "UIEvents.hpp"

BEGIN_EVENT_TABLE(DocumentTreeListManager, wxEvtHandler)
EVT_LIST_ITEM_ACTIVATED(wxID_ANY, DocumentTreeListManager::OnListDoubleClick)
EVT_RIGHT_DOWN(DocumentTreeListManager::OnMouseRightUp)
EVT_MENU(wxID_ANY, DocumentTreeListManager::OnContextMenu)
END_EVENT_TABLE()

DocumentTreeListManager::DocumentTreeListManager(wxListCtrl *listCtrl)
{
  _showFileNames =  true;

  // Manage updating the list based off the Document Manager...
  _listCtrl = listCtrl;

  // Turn off heading and sorting the heading
  long style = _listCtrl->GetWindowStyle();
  _listCtrl->SetWindowStyle(style|wxLC_NO_HEADER);  

  _listCtrl->InsertColumn(0,L"Names",wxLIST_FORMAT_LEFT);
  _listCtrl->PushEventHandler(this);
}

DocumentViewer *GetDocFromID(wxListCtrl *listCtrl,long id)
{
  return (DocumentViewer*) listCtrl->GetItemData(id);
}

wxString GetFileFullPath(DocumentViewer *doc)
{
  return doc->GetFilePath().Length() ? doc->GetFilePath() : doc->GetName();
}

void DocumentTreeListManager::AddDocumentToList(DocumentViewer *doc)
{
    wxListItem info;
#if _VBS_15079_RESTRICT_BISIM_SCRIPTS
    if (doc->ContentsProtected())
    {
      info.m_text = L"Internal Script";
    }
    else
    {
      info.m_text = _showFileNames ? doc->GetName() : GetFileFullPath(doc);
    }
#else
    info.m_text = _showFileNames ? doc->GetName() : GetFileFullPath(doc);
#endif
    info.m_mask = wxLIST_MASK_TEXT|wxLIST_MASK_DATA;
    info.m_data = (wxUIntPtr) doc;
    info.m_itemId = 0;

  // Insert the document into the list
  _listCtrl->InsertItem(info);
  _listCtrl->SetColumnWidth(0,wxLIST_AUTOSIZE);

    // Refresh the list control
  _listCtrl->Refresh();
}

void DocumentTreeListManager::RemoveDocument(DocumentViewer *doc)
{
  for (int i=0; i <_listCtrl->GetItemCount(); i++)
    if (_listCtrl->GetItemData(i)==(wxUIntPtr)doc)
    {
      _listCtrl->DeleteItem(i); 
      return;
    }
}

void DocumentTreeListManager::OnListDoubleClick(wxListEvent &event)
{
  const wxListItem& item = event.GetItem();
  DocumentViewer *doc = (DocumentViewer *)item.m_data;
  Assert(doc);
  GDocumentManager->FocusDocument(doc);
}

// Toggle displaying the full path name or the file name
void DocumentTreeListManager::ToggleDisplayName()
{
  _showFileNames = !_showFileNames;

  for (int i=0; i <_listCtrl->GetItemCount(); i++)
  {
    DocumentViewer* doc = (DocumentViewer *)_listCtrl->GetItemData(i);
    _listCtrl->SetItemText(i,_showFileNames ? doc->GetName() : GetFileFullPath(doc));
  }

  _listCtrl->SetColumnWidth(0,wxLIST_AUTOSIZE);
}

void DocumentTreeListManager::OnContextMenu(wxCommandEvent &event)
{
  long id =  (long) static_cast<wxMenu *>(event.GetEventObject())->GetClientData();
  
  switch (event.GetId())
  {
    case DOC_TREE_CLOSE:
      GDocumentManager->CloseDocumentCheckLock(GetDocFromID(_listCtrl,id));
      break;
    case DOC_TREE_CLOSE_ALL:
      GDocumentManager->CloseAllDocuments();
      break;
    case DOC_TREE_SHOW_FILE_NAME:
    case DOC_TREE_SHOW_FULL_PATH:
        ToggleDisplayName();
      break;
  }
}

void DocumentTreeListManager::OnMouseRightUp(wxMouseEvent &event)
{
  event.Skip(false);

  int flags;
  long id = _listCtrl->HitTest(event.GetPosition(),flags);
  
  wxMenu rightClickMenu;
  rightClickMenu.SetClientData((void*)id);

  if (id>=0)
  {
    rightClickMenu.Append(DOC_TREE_CLOSE, wxT("Close File"));
    _listCtrl->PopupMenu(&rightClickMenu,event.GetPosition());
    return;
  }
  else
  {
    if (_showFileNames)
      rightClickMenu.Append(DOC_TREE_SHOW_FULL_PATH, wxT("Show full paths"));
    else
      rightClickMenu.Append(DOC_TREE_SHOW_FILE_NAME, wxT("Show file names"));

    rightClickMenu.AppendSeparator();
    rightClickMenu.Append(DOC_TREE_CLOSE_ALL, wxT("Close all files"));

    _listCtrl->PopupMenu(&rightClickMenu,event.GetPosition());
  }
}


SmartRef< DocumentTreeListManager > GDocumentTreeManager;