#include "../../Common/Essential/AppFrameWork.hpp"

#include <vector>
#include <string>

#include "VirtualMachineTree.hpp"
#include "UIController.hpp"
#include "MainWindow.hpp"
#include "ImgManager.hpp"
#include "resource.h"
#include "UIEvents.hpp"
#include "WatchManagerUI.hpp"
#include "Util.hpp"



// Identifier to make up the tables
enum 
{
  ColCallName,
  ColCallValue,
  ColSize
};

// -1 just means remaining space
// all other values are percentage
static int ColFormat[] = {50,-1};

WatchManagerUI::WatchManagerUI
(
  wxWindow *parent, 
  const wxPoint& pos,
  const wxSize& size,
  long style,
  const wxString& name
):wxGrid(parent,-1,pos,size,style,name)
{
  // Register this window
  GMainWindow->RegisterWindow(this);

  // Grid
  CreateGrid( 15, ColSize );
  EnableEditing( true );
  EnableGridLines( true );
  EnableDragGridSize( false );
  DisableDragRowSize();

  SetColLabelValue(ColCallName,L"Memory name");
  SetColLabelValue(ColCallValue,L"Value");

  SetScrollLineX(1);
  SetScrollLineY(1);
  SetMargins(0,0);

  SetColLabelAlignment( wxALIGN_CENTRE, wxALIGN_CENTRE );
  SetRowLabelSize(0);

  // Columns
  EnableDragColMove( false );
  EnableDragColSize( true );
  EnableDragRowSize( true );
  
  SetColLabelSize( 20 );
  SetColLabelAlignment( wxALIGN_CENTRE, wxALIGN_CENTRE );
  SetRowLabelSize( 0 );
  SetRowLabelAlignment( wxALIGN_CENTRE, wxALIGN_CENTRE );
}

void WatchManagerUI::DoSetSize(int x, int y,int width, int height, int sizeFlags)
{
  wxGrid::DoSetSize(x,y,width,height,sizeFlags);

  if (width<=0) return;

  const int CallNumWidth = 20;
  const int CallNumTextWidth = width-CallNumWidth > 0 ? width-CallNumWidth : width; 

  // Re-arrange the sizes of all the elements
  // Calculate the space, to resize everything.d
  wxSize winSize = GetSize();
  wxSize virt    = GetVirtualSize();

  // Resize the width to fit the vertical scroll bar.
  if (winSize.x>virt.x)
  {
    if (winSize.x-wxSystemSettings::GetMetric(wxSYS_VSCROLL_X)>0) 
      winSize.x -= wxSystemSettings::GetMetric(wxSYS_VSCROLL_X);
  }

  for (int i=0; i<ColSize; i++)
  {   
    int width;
    if (ColFormat[i]==-1)
    {
      // Place the remaining amount
      width = winSize.x;
    }
    else
    {
      float withPer = (float) ColFormat[i];
      withPer*=0.01f;
      width = (int)(withPer*(float)winSize.x);
      winSize.x -= width;
    }

    SetColSize(i,width);
  }
}


WatchManagerUI::~WatchManagerUI()
{
  // unregister the window
  if (GMainWindow)  GMainWindow->UnRegisterWindow(this);
}

void WatchManagerUI::SimulateUpdate(float deltaT)
{

}

void WatchManagerUI::StateChange(DebugScript *selScript)
{
  for (int row=0; row<GetNumberRows(); row++)
  {
    wxString val = GetCellValue(row,0).Trim();
    
    if (!val.length())
    {
      SetCellValue(L"",row,1);
      continue;
    }
    if 
    (
      val.length() 
      && val[0]=='_' 
      && GFrame->GetFocus() 
      && !GFrame->GetFocus()->IsRunning()
    )
    {
      SetCellValue(L"No script",row,1);
      continue;
    }

    std::string evalVal = UTF16ToChar(val);

    evalVal = GUtil.EvaulateValue(evalVal);
    SetCellValue(wxString::From8BitData(evalVal.data()),row,1);
  }
}

