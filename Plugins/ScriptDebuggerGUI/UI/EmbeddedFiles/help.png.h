#ifndef HELP_PNG_H
#define HELP_PNG_H

#include <wx/mstream.h>
#include <wx/image.h>
#include <wx/bitmap.h>

static const unsigned char help_png[] = 
{
	0x89, 0x50, 0x4E, 0x47, 0x0D, 0x0A, 0x1A, 0x0A, 0x00, 0x00, 
	0x00, 0x0D, 0x49, 0x48, 0x44, 0x52, 0x00, 0x00, 0x00, 0x10, 
	0x00, 0x00, 0x00, 0x10, 0x08, 0x06, 0x00, 0x00, 0x00, 0x1F, 
	0xF3, 0xFF, 0x61, 0x00, 0x00, 0x00, 0x19, 0x74, 0x45, 0x58, 
	0x74, 0x53, 0x6F, 0x66, 0x74, 0x77, 0x61, 0x72, 0x65, 0x00, 
	0x41, 0x64, 0x6F, 0x62, 0x65, 0x20, 0x49, 0x6D, 0x61, 0x67, 
	0x65, 0x52, 0x65, 0x61, 0x64, 0x79, 0x71, 0xC9, 0x65, 0x3C, 
	0x00, 0x00, 0x02, 0x9B, 0x49, 0x44, 0x41, 0x54, 0x78, 0xDA, 
	0x64, 0x53, 0x4D, 0x48, 0x54, 0x51, 0x14, 0xFE, 0xEE, 0x7B, 
	0xF3, 0xDE, 0x9D, 0x71, 0x9A, 0xB1, 0x18, 0xB3, 0xD0, 0x12, 
	0x4A, 0x8B, 0xC2, 0x84, 0x5A, 0x48, 0x10, 0x51, 0x12, 0x64, 
	0x41, 0x90, 0xBA, 0x6A, 0x93, 0x04, 0x46, 0x3F, 0x94, 0xBB, 
	0x92, 0x16, 0x2D, 0xFA, 0xD9, 0x85, 0xB6, 0x08, 0xAC, 0xA0, 
	0x45, 0x1B, 0x6B, 0xE1, 0x22, 0xD4, 0x45, 0x0B, 0x15, 0x2A, 
	0x87, 0x7E, 0x16, 0x62, 0x04, 0xF6, 0x83, 0x13, 0x0E, 0xE5, 
	0xD8, 0x80, 0xD3, 0x8C, 0x39, 0x33, 0x64, 0x6F, 0xC6, 0x99, 
	0xF7, 0x3A, 0xE7, 0xCE, 0x0F, 0x5A, 0x07, 0x2E, 0x97, 0x73, 
	0xCE, 0xF7, 0x7D, 0xF7, 0x9C, 0xF3, 0xDE, 0x11, 0xF8, 0xC7, 
	0xAA, 0x4E, 0x0F, 0xDD, 0xCF, 0xE7, 0x9D, 0x2E, 0xC7, 0x71, 
	0xDC, 0x0E, 0xF9, 0x82, 0x8F, 0x10, 0x96, 0xAE, 0x8B, 0xC7, 
	0xF1, 0x27, 0x1D, 0x97, 0x57, 0x63, 0x09, 0xA3, 0xF2, 0xCA, 
	0x36, 0x76, 0x0E, 0xB7, 0x1A, 0x42, 0x8C, 0xB6, 0x36, 0xD7, 
	0xE2, 0x60, 0x53, 0x35, 0xAA, 0xFC, 0x92, 0x00, 0x4C, 0x06, 
	0xE2, 0xA9, 0x0C, 0x5E, 0x4F, 0xC7, 0x30, 0x36, 0xF9, 0x03, 
	0x2B, 0x8E, 0x73, 0xEC, 0xE7, 0x40, 0xFB, 0xD8, 0x1A, 0x01, 
	0x26, 0x6F, 0xAA, 0x94, 0xA3, 0x57, 0x4F, 0x35, 0x21, 0xB1, 
	0x9C, 0xC3, 0xF8, 0xA7, 0x04, 0xBE, 0x44, 0xD3, 0x58, 0xCE, 
	0x3A, 0xA8, 0x30, 0x05, 0x76, 0xD7, 0xF8, 0x70, 0xB4, 0x31, 
	0x80, 0x40, 0x85, 0x0B, 0x7D, 0x83, 0xD3, 0x58, 0x48, 0x66, 
	0x94, 0x08, 0x0B, 0x68, 0x2C, 0xE0, 0xA2, 0x97, 0x6F, 0x9F, 
	0xD9, 0x87, 0x77, 0xE1, 0x34, 0x1E, 0xBE, 0x8C, 0x60, 0xFF, 
	0x36, 0x3F, 0xBE, 0xF5, 0x1E, 0x46, 0xEC, 0x5E, 0x8B, 0xBA, 
	0xD9, 0xE7, 0x38, 0xE7, 0x19, 0xC7, 0xF8, 0x52, 0xE5, 0x1A, 
	0xF7, 0xDC, 0x75, 0xBC, 0x01, 0x13, 0xB3, 0x49, 0x4C, 0xCD, 
	0x25, 0xE1, 0x36, 0x35, 0xDC, 0x6C, 0x6F, 0x40, 0x30, 0xB4, 
	0x88, 0xBA, 0x9E, 0x20, 0x5E, 0xCD, 0x2C, 0x2A, 0x5F, 0x1A, 
	0x9A, 0xCA, 0x33, 0x8E, 0xF1, 0xCC, 0x53, 0x02, 0xB6, 0x6D, 
	0x9F, 0xAD, 0xDD, 0x5C, 0x89, 0xF7, 0xDF, 0xD3, 0x54, 0xAE, 
	0x8B, 0x8E, 0xA1, 0x94, 0xFB, 0x46, 0x23, 0x08, 0xAC, 0x93, 
	0xE8, 0x7F, 0x31, 0xAF, 0x7C, 0x69, 0xE8, 0x2A, 0xCF, 0x38, 
	0xC6, 0x33, 0xAF, 0x50, 0x3D, 0x84, 0x9C, 0x4B, 0x66, 0xE1, 
	0xA1, 0x17, 0x68, 0xDA, 0x30, 0x29, 0xD2, 0xD2, 0x3B, 0xA5, 
	0x06, 0x68, 0x52, 0xEC, 0x56, 0x5B, 0xBD, 0x12, 0x60, 0xB2, 
	0xDB, 0x50, 0x1D, 0x83, 0xF1, 0xCC, 0x2B, 0xB6, 0x0F, 0x24, 
	0xAD, 0x1C, 0xA4, 0x74, 0x15, 0xBB, 0xD2, 0x21, 0x6D, 0x07, 
	0x99, 0x9C, 0x8D, 0x1B, 0x27, 0xB7, 0xA3, 0xB1, 0xC6, 0x8B, 
	0x8B, 0x4F, 0x43, 0xF0, 0x7A, 0x5C, 0x30, 0x74, 0x0D, 0x25, 
	0xBC, 0x28, 0x7E, 0x3F, 0xAE, 0x80, 0x02, 0x79, 0xA5, 0xCE, 
	0xAF, 0xB2, 0x11, 0x1F, 0x36, 0xDD, 0x47, 0x76, 0x6D, 0xC0, 
	0xF5, 0x91, 0x30, 0x22, 0xBF, 0x2C, 0x78, 0x4D, 0x1D, 0x9A, 
	0x26, 0x8A, 0x02, 0x79, 0xAE, 0xA0, 0x24, 0xE0, 0x64, 0xED, 
	0xBC, 0x6D, 0x4A, 0xEA, 0x9D, 0xFA, 0x2A, 0x0B, 0x30, 0xF2, 
	0xC4, 0x83, 0x8F, 0xF4, 0xAA, 0x80, 0xCF, 0x63, 0x40, 0xD7, 
	0x0A, 0x04, 0x4D, 0xD3, 0x90, 0xCB, 0x51, 0x05, 0xC4, 0x2B, 
	0x0C, 0xD1, 0x4A, 0x0C, 0x47, 0xA3, 0x4B, 0xA8, 0x90, 0x3A, 
	0x55, 0xA1, 0xD3, 0x0C, 0x34, 0x35, 0x07, 0x17, 0x11, 0x9F, 
	0x5F, 0xDA, 0x03, 0x49, 0x3E, 0x7F, 0x01, 0x8E, 0x73, 0x9E, 
	0x71, 0x8C, 0x67, 0x9E, 0x12, 0x88, 0x3F, 0x3B, 0xD7, 0xF3, 
	0x79, 0x66, 0x01, 0x16, 0xF5, 0xE5, 0xA1, 0x97, 0xDC, 0x34, 
	0x0B, 0x93, 0x06, 0x26, 0xCD, 0xC2, 0x4C, 0xF8, 0x66, 0x9F, 
	0xE3, 0x9C, 0x67, 0x1C, 0xE3, 0x99, 0xA7, 0x04, 0x78, 0xA8, 
	0xB9, 0xC8, 0x9B, 0xEE, 0xF1, 0x89, 0x19, 0x64, 0xAC, 0x15, 
	0x78, 0xDD, 0x06, 0xBC, 0x04, 0xF6, 0xD1, 0xD0, 0x2E, 0x0C, 
	0x86, 0xD5, 0xCD, 0x3E, 0xC7, 0x39, 0xCF, 0x38, 0xC6, 0x33, 
	0x0F, 0xC5, 0x5D, 0x61, 0xF3, 0xAF, 0x3F, 0x74, 0xA5, 0x53, 
	0xDF, 0x72, 0xA0, 0xBF, 0x79, 0xEF, 0x56, 0xEC, 0xAC, 0xAF, 
	0x86, 0xDF, 0x6B, 0x96, 0x77, 0x21, 0xF5, 0x3B, 0x8B, 0xD0, 
	0x6C, 0x0C, 0x93, 0x1F, 0x22, 0xC8, 0xCF, 0xBF, 0xED, 0x5E, 
	0x0A, 0xDE, 0x1D, 0x20, 0x4E, 0x6A, 0xCD, 0x32, 0xB1, 0x08, 
	0x9D, 0x1D, 0x81, 0x8E, 0x47, 0xD7, 0x84, 0x27, 0xD0, 0x46, 
	0x83, 0x34, 0xCB, 0xBF, 0xAB, 0x40, 0xD6, 0xF9, 0x93, 0x18, 
	0x49, 0x0C, 0x9D, 0xBF, 0x43, 0xEE, 0x57, 0x26, 0xFF, 0xB7, 
	0x8D, 0xAB, 0xAC, 0x8E, 0x4E, 0xA0, 0xD8, 0x5E, 0xC9, 0xF8, 
	0xF3, 0x24, 0x4A, 0x65, 0xAF, 0x5E, 0xE7, 0xBF, 0x02, 0x0C, 
	0x00, 0x02, 0xC8, 0x16, 0xFE, 0x5A, 0x11, 0xE0, 0xB3, 0x00, 
	0x00, 0x00, 0x00, 0x49, 0x45, 0x4E, 0x44, 0xAE, 0x42, 0x60, 
	0x82, 
};

wxBitmap& help_png_to_wx_bitmap()
{
	static wxMemoryInputStream memIStream( help_png, sizeof( help_png ) );
	static wxImage image( memIStream, wxBITMAP_TYPE_PNG );
	static wxBitmap bmp( image );
	return bmp;
};


#endif //HELP_PNG_H
