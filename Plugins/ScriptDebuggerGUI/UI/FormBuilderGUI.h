///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version Mar 17 2012)
// http://www.wxformbuilder.org/
//
// To allow custom controls:
// WILL BE EDITED MANUALLY FROM NOW ON - DO NOT ReGenerate in the wxFormBuilder
///////////////////////////////////////////////////////////////////////////

#ifndef __FORMBUILDERGUI_H__
#define __FORMBUILDERGUI_H__

#include "../ScriptDebuggerConfig.hpp"

#include <wx/artprov.h>
#include <wx/xrc/xmlres.h>
#include <wx/string.h>
#include <wx/bitmap.h>
#include <wx/image.h>
#include <wx/icon.h>
#include <wx/menu.h>
#include <wx/gdicmn.h>
#include <wx/font.h>
#include <wx/colour.h>
#include <wx/settings.h>
#include <wx/aui/auibar.h>
#include <wx/listctrl.h>
#include <wx/sizer.h>
#include <wx/panel.h>
#include <wx/treectrl.h>
#include <wx/wxFlatNotebook/wxFlatNotebook.h>
#ifdef __VISUALC__
#include <wx/link_additions.h>
#endif //__VISUALC__
#include <wx/propgrid/propgrid.h>
#include <wx/propgrid/advprops.h>
#include <wx/statusbr.h>
#include <wx/frame.h>
#include <wx/aui/aui.h>
#include <wx/plotctrl/plotctrl.h>
#include <wx/splitter.h>
#include <wx/textctrl.h>
#include <wx/button.h>
#include <wx/stattext.h>
#include <wx/filepicker.h>
#include <wx/statbox.h>
#include <wx/clrpicker.h>
#include <wx/choice.h>
#include <wx/checkbox.h>
#include <wx/bmpbuttn.h>

#include <list>

///////////////////////////////////////////////////////////////////////////

#define MENU_ON_NEW_FILE 1000
#define MENU_ON_OPEN 1001
#define MENU_ON_SAVE 1002
#define MENU_ON_SAVEAS 1003
#define MENU_ON_SETTINGS 1004
#define MENU_ON_EXIT 1005
#define MENU_ON_DOC_STYLE 1006
#define MENU_ON_DEFAULT_VIEW 1007
#define MENU_CP_CentralEurope 1008
#define MENU_CP_Cyrillic 1009
#define MENU_CP_LatinI 1010
#define MENU_CP_Greek 1011
#define MENU_CP_Turkish 1012
#define MENU_CP_Hebrew 1013
#define MENU_CP_Arabic 1014
#define MENU_CP_Baltic 1015
#define MENU_CP_Vietnam 1016
#define MENU_CP_Thai 1017
#define MENU_CP_OEM_US 1018
#define MENU_CP_OEM_Arabic 1019
#define MENU_CP_OEM_Greek 1020
#define MENU_CP_OEM_Baltic 1021
#define MENU_CP_OEM_MultilingualLI 1022
#define MENU_CP_OEM_LatinII 1023
#define MENU_CP_OEM_Cyrillic 1024
#define MENU_CP_OEM_Turkish 1025
#define MENU_CP_OEM_MultilingualLIEuro 1026
#define MENU_CP_OEM_Hebrew 1027
#define MENU_CP_OEM_Russian 1028
#define MENU_CP_ISO_8859_1 1029
#define MENU_CP_ISO_8859_2 1030
#define MENU_CP_ISO_8859_3 1031
#define MENU_CP_ISO_8859_4 1032
#define MENU_CP_ISO_8859_5 1033
#define MENU_CP_ISO_8859_6 1034
#define MENU_CP_ISO_8859_8 1035
#define MENU_CP_ISO_8859_9 1036
#define MENU_CP_ISO_8859_15 1037
#define MENU_CP_DB_JapaneseShiftJIS 1038
#define MENU_CP_DB_SimplifiedChineseGBK 1039
#define MENU_CP_DB_Korean 1040
#define MENU_CP_DB_TraditionalChineseBigFive 1041
#define MENU_CCP_UTF7 1042
#define MENU_CCP_UTF8 1043
#define MENU_CCP_UCS2LE 1044
#define MENU_CCP_UTF16 1045
#define TM_EVENT_BREAK_ALL 1046
#define TM_EVENT_STEP_INTO 1047
#define TM_EVENT_STEP_OVER 1048
#define TM_EVENT_STEP_OUT 1049
#define TM_EVENT_TOGGLE_BP 1050
#define MENU_ON_ABOUT 1051
#define MENU_ON_VIEW_ERROR_MSG 1052
#define MENU_ON_VIEW_CALL_STACK 1053
#define MENU_ON_VIEW_WATCH 1054
#define MENU_ON_DEBUG_VIEW_DEFAULT 1055
#define MENU_ON_HANDLER 1056
#define MENU_ON_ACTIVE_S 1057
#define MENU_ON_VM 1058
#define MENU_ON_PERF 1059
#define MENU_ON_DEBUG 1060
#define IDC_BtnNewFile 1061
#define IDC_BtnOpenFile 1062
#define IDC_BtnOpenMissionFile 1063
#define IDC_BtnSaveFile 1064
#define IDC_BtnSaveAs 1065
#define IDC_BtnBreakAll 1066
#define IDC_BtnContinueAll 1067
#define IDC_BtnStepInto 1068
#define IDC_BtnStepOver 1069
#define IDC_BtnStepOut 1070
#define IDC_BtnConnect 1071
#define IDC_BtnDisconnect 1072
#define IDC_LEFT_NOTE_BOOK 1073
#define IDC_EventHandler 1074
#define ConfigTextArea 1075
#define IDC_FONT_NAME 1076
#define IDC_FONT_SIZE 1077
#define IDC_STYLE_OK 1078
#define IDC_STYLE_CANCEL 1079
#define IDC_ADDBTN 1080
#define IDC_RMBTN 1081
#define CNTXT_MENU_UNPIN_TAB 1082
#define IDC_BOTTOM_NOTEBOOK_ID 1083
#define MENU_ON_SAVE_LAYOUT 1084
#define TM_EVENT_STEP_CONTINUE 1085
#define IDC_BtnContinueSingle 1086
#define IDC_BtnScriptLog 1087
#define MENU_ON_SCRIPTLOG 1088
#define MENU_ON_DEBUGCONSOLE 1089
#define IDC_BtnStepFile 1090
#define TM_EVENT_STEP_FILE 1091

class wxFlatNotebookExt;

///////////////////////////////////////////////////////////////////////////////
/// Class DebugScriptWindow
///////////////////////////////////////////////////////////////////////////////
class DebugScriptWindow : public wxFrame 
{
	private:
	
	protected:
		wxMenuBar* MenuBar;
		wxMenu* FileMenu;
		wxMenu* ViewMenu;
		wxMenu* SingleByte;
		wxMenu* ACP;
		wxMenu* OEM;
		wxMenu* ISO;
		wxMenu* DoubleByte;
		wxMenu* UniCode;
		wxMenu* DebugMenu;
		wxMenu* HelpMenu;
		wxMenu* DevloperMenu;
		wxMenu* DebugMenuDev;
		wxMenu* ViewMenuDev;
		wxListCtrl* DocumentSelector;
#if _SD_VM_TREE
		wxPanel* VirtualMachinePanel;
#endif
		wxPanel* HiddenPanel;
#if _SD_EH_TAB
		wxPanel* PropertyGridPanel;
#endif
		wxPanel* m_panel14;
		wxStatusBar* m_statusBar1;
	
	public:
		wxMenu* EncodingMenu;
		wxAuiToolBar* ToolBarScriptDebugger;
		wxFlatNotebook* LeftPanelNoteBook; 
		wxPanel* DocumentSelectorPanel;
		wxPanel* DeveloperDrivePanel;
		wxTreeCtrl* DeveloperDriveTreeCtrl;
#if _SD_VM_TREE
		wxTreeCtrl* VirtualMachineTreeCtrl;
#endif
#if _SD_EH_TAB
		wxPropertyGrid* PropertyGrid;
#endif
    wxFlatNotebook* DocumentFlatNoteBook; 
		wxFlatNotebookExt* BottomNoteBook; 
		wxAuiManager m_mgr;
		
		DebugScriptWindow( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxT("VBS2 Script Debugger"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 945,724 ), long style = wxDEFAULT_FRAME_STYLE|wxTAB_TRAVERSAL );
		~DebugScriptWindow();
	
};

///////////////////////////////////////////////////////////////////////////////
/// Class DiagFrameRate
///////////////////////////////////////////////////////////////////////////////
class DiagFrameRate : public wxFrame 
{
	private:
	
	protected:
		wxPanel* m_panel13;
		wxSplitterWindow* m_splitter5;
		wxPanel* m_panel11;
		wxPanel* m_panel12;
	
	public:
		wxPlotCtrl* PlotCtrl;
		wxPropertyGrid* PropGrid;
		
		DiagFrameRate( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxT("Frame Rate"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 892,640 ), long style = wxDEFAULT_FRAME_STYLE|wxTAB_TRAVERSAL );
		
		~DiagFrameRate();
		
		void m_splitter5OnIdle( wxIdleEvent& )
		{
			m_splitter5->SetSashPosition( 647 );
			m_splitter5->Disconnect( wxEVT_IDLE, wxIdleEventHandler( DiagFrameRate::m_splitter5OnIdle ), NULL, this );
		}
	
};

///////////////////////////////////////////////////////////////////////////////
/// Class DialogSettings
///////////////////////////////////////////////////////////////////////////////
class DialogSettings : public wxFrame 
{
	private:
	
	protected:
		wxPanel* m_panel8;
		wxStaticText* m_staticText2;
		wxDirPickerCtrl* DeveloperPath;
		wxButton* OkButton;
		wxStaticText* PPDefintionSText;
	
	public:
		wxPanel* PPDefTemplate;
		
		DialogSettings( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxT("Settings"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 357,300 ), long style = wxDEFAULT_FRAME_STYLE|wxRESIZE_BORDER|wxTAB_TRAVERSAL );
		
		~DialogSettings();
	
};

///////////////////////////////////////////////////////////////////////////////
/// Class DialogStyle
///////////////////////////////////////////////////////////////////////////////
class DialogStyle : public wxFrame 
{
	private:
	
	protected:
		wxPanel* m_panel17;
		wxStaticText* staticText;
		wxStaticText* staticText1;
		wxStaticText* m_staticText11;
		wxStaticText* m_staticText111;
		wxPanel* m_panel18;
	
	public:
		wxListCtrl* TypeSelector;
		wxColourPickerCtrl* TypeForgroundColour;
		wxColourPickerCtrl* TypeGroundColour;
		wxChoice* TypeFontType;
		wxChoice* TypeFontSize;
		wxCheckBox* TypeFontBold;
		wxCheckBox* TypeFontItalic;
		wxCheckBox* TypeFontUnderline;
		wxButton* BtnSave;
		wxButton* BtnCancel;
		
		DialogStyle( wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxT("Style"), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 430,507 ), long style = wxDEFAULT_FRAME_STYLE|wxRESIZE_BORDER|wxTAB_TRAVERSAL );
		
		~DialogStyle();
	
};

///////////////////////////////////////////////////////////////////////////////
/// Class DiagAddDefintionTemplate
///////////////////////////////////////////////////////////////////////////////
class DiagAddDefintionTemplate : public wxPanel 
{
	private:
	
	protected:
	
	public:
		wxTextCtrl* TextBoxPPDefintions;
		wxBitmapButton* BtnAdd;
		wxBitmapButton* BtnSub;
		
		DiagAddDefintionTemplate( wxWindow* parent, wxWindowID id = wxID_ANY, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxSize( 500,300 ), long style = wxTAB_TRAVERSAL ); 
		~DiagAddDefintionTemplate();
	
};

class MyFrame;

//Custom components
class UnpinnableWindow
{
public:
	virtual void unpinWindow(wxWindow * frame) = 0;
	virtual void pinWindow(wxWindow * frame, wxFlatNotebookExt * notebook) = 0;
};

struct wxUndockableEntry
{
	UnpinnableWindow * notebookTab;
	wxWindow * windowPtr;	
};

struct wxFlatNtbContextMenuEvtData
{
	wxFlatNotebookExt * notebook;
	size_t tabIndex;
};


//class for the right-click menus to safely handle user data
class wxFlatNotebookTabContextMenu : public wxMenu
{
public:
	wxFlatNtbContextMenuEvtData * eventData;
	virtual ~wxFlatNotebookTabContextMenu();
};

class wxFlatNotebook;
class wxFlatNotebookExt : public wxFlatNotebook
{
private:
	//int _tabBeingDragged = -1;
protected:
	//void onPageDrag(size_t pageNumber);
	std::list<wxUndockableEntry> * undockableTabs;
	void onPageRightDown(wxMouseEvent & event);
	void OnContextMenuSelect(wxCommandEvent & event);
public:
	wxFlatNotebookExt(wxWindow* parent, wxWindowID id, const wxPoint& pos, const wxSize& size, long style, const wxString& name) :
	  wxFlatNotebook(parent,id,pos,size,style,name)
	{
		undockableTabs = new std::list<wxUndockableEntry>();
		initEventHandlers();
	};

	virtual ~wxFlatNotebookExt()
	{
		delete undockableTabs;
	}

	void initEventHandlers();
	void registerUndockablePage(wxUndockableEntry & info);
	void UnpinPage(size_t pageIndex);
};

//create our own implementation of the wxPlotCtrl class to override key events
class wxPlotCtrlExt : public wxPlotCtrl
{
public:
  wxPlotCtrlExt() : wxPlotCtrl() {};
  wxPlotCtrlExt(wxWindow *parent, wxWindowID win_id = wxID_ANY,
                  const wxPoint &pos = wxDefaultPosition,
                  const wxSize &size = wxDefaultSize,
                  wxPlotCtrlAxis_Type flags = wxPLOTCTRL_DEFAULT,
                  const wxString& name = wxT("wxPlotCtrl") )
                  : wxPlotCtrl(parent,win_id, pos, size, flags, name)
  {};

  virtual void ProcessAreaEVT_CHAR(wxKeyEvent &event);
};
#endif //__FORMBUILDERGUI_H__
