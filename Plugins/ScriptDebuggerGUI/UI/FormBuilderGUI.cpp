///////////////////////////////////////////////////////////////////////////
// C++ code generated with wxFormBuilder (version Mar 17 2012)
// http://www.wxformbuilder.org/
//
// To allow custom controls:
// WILL BE EDITED MANUALLY FROM NOW ON - DO NOT ReGenerate in the wxFormBuilder
///////////////////////////////////////////////////////////////////////////

#include "FormBuilderGUI.h"
#include "../Icons/debuggerIcons.hpp"

#include "EmbeddedFiles/MissionFolder.bmp.h"
#include "EmbeddedFiles/Options.bmp.h"
#include "EmbeddedFiles/VSFolder_open.bmp.h"
#include "EmbeddedFiles/add.png.h"
#include "EmbeddedFiles/arrow_merge_down.png.h"
#include "EmbeddedFiles/arrow_turn_right_upside.png.h"
#include "EmbeddedFiles/connect.bmp.h"
#include "EmbeddedFiles/control_pause.png.h"
#include "EmbeddedFiles/control_play.png.h"
#include "EmbeddedFiles/disconnect.bmp.h"
#include "EmbeddedFiles/document.bmp.h"
#include "EmbeddedFiles/help.png.h"
#include "EmbeddedFiles/save.bmp.h"
#include "EmbeddedFiles/savenew.bmp.h"
#include "EmbeddedFiles/subtract.png.h"
#include "EmbeddedFiles/timeline_marker.png.h"
#include "EmbeddedFiles/title_window.png.h"

///////////////////////////////////////////////////////////////////////////

DebugScriptWindow::DebugScriptWindow( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxFrame( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxDefaultSize, wxDefaultSize );
	m_mgr.SetManagedWindow(this);
	
	MenuBar = new wxMenuBar( wxMB_DOCKABLE );
	FileMenu = new wxMenu();
	wxMenuItem* MenuNewFile;
	MenuNewFile = new wxMenuItem( FileMenu, MENU_ON_NEW_FILE, wxString( wxT("New \tCTRL+N") ) , wxEmptyString, wxITEM_NORMAL );
	#ifdef __WXMSW__
	MenuNewFile->SetBitmaps( document_bmp_to_wx_bitmap() );
	#elif defined( __WXGTK__ )
	MenuNewFile->SetBitmap( document_bmp_to_wx_bitmap() );
	#endif
	FileMenu->Append( MenuNewFile );
	
	FileMenu->AppendSeparator();
	
	wxMenuItem* MenuOpen;
	MenuOpen = new wxMenuItem( FileMenu, MENU_ON_OPEN, wxString( wxT("Open \tCTRL+O") ) , wxEmptyString, wxITEM_NORMAL );
	#ifdef __WXMSW__
	MenuOpen->SetBitmaps( VSFolder_open_bmp_to_wx_bitmap() );
	#elif defined( __WXGTK__ )
	MenuOpen->SetBitmap( VSFolder_open_bmp_to_wx_bitmap() );
	#endif
	FileMenu->Append( MenuOpen );
	
	wxMenuItem* MenuSave;
	MenuSave = new wxMenuItem( FileMenu, MENU_ON_SAVE, wxString( wxT("Save \tCTRL+S") ) , wxEmptyString, wxITEM_NORMAL );
	#ifdef __WXMSW__
	MenuSave->SetBitmaps( save_bmp_to_wx_bitmap() );
	#elif defined( __WXGTK__ )
	MenuSave->SetBitmap( save_bmp_to_wx_bitmap() );
	#endif
	FileMenu->Append( MenuSave );
	
	wxMenuItem* MenuSaveAs;
	MenuSaveAs = new wxMenuItem( FileMenu, MENU_ON_SAVEAS, wxString( wxT("Save As \tF12") ) , wxEmptyString, wxITEM_NORMAL );
	#ifdef __WXMSW__
	MenuSaveAs->SetBitmaps( savenew_bmp_to_wx_bitmap() );
	#elif defined( __WXGTK__ )
	MenuSaveAs->SetBitmap( savenew_bmp_to_wx_bitmap() );
	#endif
	FileMenu->Append( MenuSaveAs );
	
	FileMenu->AppendSeparator();
	
	wxMenuItem* MenuSettings;
	MenuSettings = new wxMenuItem( FileMenu, MENU_ON_SETTINGS, wxString( wxT("Settings") ) , wxEmptyString, wxITEM_NORMAL );
	#ifdef __WXMSW__
	MenuSettings->SetBitmaps( Options_bmp_to_wx_bitmap() );
	#elif defined( __WXGTK__ )
	MenuSettings->SetBitmap( Options_bmp_to_wx_bitmap() );
	#endif
	FileMenu->Append( MenuSettings );
	
	FileMenu->AppendSeparator();
	
	wxMenuItem* OnQuit;
	OnQuit = new wxMenuItem( FileMenu, MENU_ON_EXIT, wxString( wxT("Exit") ) , wxEmptyString, wxITEM_NORMAL );
	#ifdef __WXMSW__
	OnQuit->SetBitmaps( title_window_png_to_wx_bitmap() );
	#elif defined( __WXGTK__ )
	OnQuit->SetBitmap( title_window_png_to_wx_bitmap() );
	#endif
	FileMenu->Append( OnQuit );
	
	MenuBar->Append( FileMenu, wxT("File") ); 
	
	ViewMenu = new wxMenu();
	wxMenuItem* ViewDefaultViewMenu1;
	ViewDefaultViewMenu1 = new wxMenuItem( ViewMenu, MENU_ON_DOC_STYLE, wxString( wxT("Document Style") ) , wxEmptyString, wxITEM_NORMAL );
	ViewMenu->Append( ViewDefaultViewMenu1 );
	
	ViewMenu->AppendSeparator();
	
	wxMenuItem* ViewDefaultViewMenu;
	ViewDefaultViewMenu = new wxMenuItem( ViewMenu, MENU_ON_DEFAULT_VIEW, wxString( wxT("Default View") ) , wxEmptyString, wxITEM_NORMAL );
	ViewMenu->Append( ViewDefaultViewMenu );
	
	MenuBar->Append( ViewMenu, wxT("View") ); 
	
	EncodingMenu = new wxMenu();
	SingleByte = new wxMenu();
	ACP = new wxMenu();
	wxMenuItem* m_menuItem100;
	m_menuItem100 = new wxMenuItem( ACP, MENU_CP_CentralEurope, wxString( wxT("1250 (Central Europe)") ) , wxEmptyString, wxITEM_CHECK );
	ACP->Append( m_menuItem100 );
	
	wxMenuItem* m_menuItem101;
	m_menuItem101 = new wxMenuItem( ACP, MENU_CP_Cyrillic, wxString( wxT("1251 (Cyrillic)") ) , wxEmptyString, wxITEM_CHECK );
	ACP->Append( m_menuItem101 );
	
	wxMenuItem* m_menuItem102;
	m_menuItem102 = new wxMenuItem( ACP, MENU_CP_LatinI, wxString( wxT("1252 (Latin I)") ) , wxEmptyString, wxITEM_CHECK );
	ACP->Append( m_menuItem102 );
	
	wxMenuItem* m_menuItem103;
	m_menuItem103 = new wxMenuItem( ACP, MENU_CP_Greek, wxString( wxT("1253 (Greek)") ) , wxEmptyString, wxITEM_CHECK );
	ACP->Append( m_menuItem103 );
	
	wxMenuItem* m_menuItem104;
	m_menuItem104 = new wxMenuItem( ACP, MENU_CP_Turkish, wxString( wxT("1254 (Turkish)") ) , wxEmptyString, wxITEM_CHECK );
	ACP->Append( m_menuItem104 );
	
	wxMenuItem* m_menuItem105;
	m_menuItem105 = new wxMenuItem( ACP, MENU_CP_Hebrew, wxString( wxT("1255 (Hebrew)") ) , wxEmptyString, wxITEM_CHECK );
	ACP->Append( m_menuItem105 );
	
	wxMenuItem* m_menuItem106;
	m_menuItem106 = new wxMenuItem( ACP, MENU_CP_Arabic, wxString( wxT("1256 (Arabic)") ) , wxEmptyString, wxITEM_CHECK );
	ACP->Append( m_menuItem106 );
	
	wxMenuItem* m_menuItem107;
	m_menuItem107 = new wxMenuItem( ACP, MENU_CP_Baltic, wxString( wxT("1257 (Baltic)") ) , wxEmptyString, wxITEM_CHECK );
	ACP->Append( m_menuItem107 );
	
	wxMenuItem* m_menuItem108;
	m_menuItem108 = new wxMenuItem( ACP, MENU_CP_Vietnam, wxString( wxT("1258 (Vietnam)") ) , wxEmptyString, wxITEM_CHECK );
	ACP->Append( m_menuItem108 );
	
	wxMenuItem* m_menuItem109;
	m_menuItem109 = new wxMenuItem( ACP, MENU_CP_Thai, wxString( wxT("874 (Thai)") ) , wxEmptyString, wxITEM_CHECK );
	ACP->Append( m_menuItem109 );
	
	SingleByte->Append( -1, wxT("ACP"), ACP );
	
	OEM = new wxMenu();
	wxMenuItem* m_menuItem52;
	m_menuItem52 = new wxMenuItem( OEM, MENU_CP_OEM_US, wxString( wxT("437 (US)") ) , wxEmptyString, wxITEM_CHECK );
	OEM->Append( m_menuItem52 );
	
	wxMenuItem* m_menuItem53;
	m_menuItem53 = new wxMenuItem( OEM, MENU_CP_OEM_Arabic, wxString( wxT("720 (Arabic)") ) , wxEmptyString, wxITEM_CHECK );
	OEM->Append( m_menuItem53 );
	
	wxMenuItem* m_menuItem54;
	m_menuItem54 = new wxMenuItem( OEM, MENU_CP_OEM_Greek, wxString( wxT("737 (Greek)") ) , wxEmptyString, wxITEM_CHECK );
	OEM->Append( m_menuItem54 );
	
	wxMenuItem* m_menuItem55;
	m_menuItem55 = new wxMenuItem( OEM, MENU_CP_OEM_Baltic, wxString( wxT("775 (Baltic)") ) , wxEmptyString, wxITEM_CHECK );
	OEM->Append( m_menuItem55 );
	
	wxMenuItem* m_menuItem56;
	m_menuItem56 = new wxMenuItem( OEM, MENU_CP_OEM_MultilingualLI, wxString( wxT("850 (Multilingual Latin I)") ) , wxEmptyString, wxITEM_CHECK );
	OEM->Append( m_menuItem56 );
	
	wxMenuItem* m_menuItem57;
	m_menuItem57 = new wxMenuItem( OEM, MENU_CP_OEM_LatinII, wxString( wxT("852 (Latin II)") ) , wxEmptyString, wxITEM_CHECK );
	OEM->Append( m_menuItem57 );
	
	wxMenuItem* m_menuItem58;
	m_menuItem58 = new wxMenuItem( OEM, MENU_CP_OEM_Cyrillic, wxString( wxT("855 (Cyrillic)") ) , wxEmptyString, wxITEM_CHECK );
	OEM->Append( m_menuItem58 );
	
	wxMenuItem* m_menuItem59;
	m_menuItem59 = new wxMenuItem( OEM, MENU_CP_OEM_Turkish, wxString( wxT("857 (Turkish)") ) , wxEmptyString, wxITEM_CHECK );
	OEM->Append( m_menuItem59 );
	
	wxMenuItem* m_menuItem60;
	m_menuItem60 = new wxMenuItem( OEM, MENU_CP_OEM_MultilingualLIEuro, wxString( wxT("858 (Multilingual Latin I + Euro)") ) , wxEmptyString, wxITEM_CHECK );
	OEM->Append( m_menuItem60 );
	
	wxMenuItem* m_menuItem61;
	m_menuItem61 = new wxMenuItem( OEM, MENU_CP_OEM_Hebrew, wxString( wxT("862 (Hebrew)") ) , wxEmptyString, wxITEM_CHECK );
	OEM->Append( m_menuItem61 );
	
	wxMenuItem* m_menuItem62;
	m_menuItem62 = new wxMenuItem( OEM, MENU_CP_OEM_Russian, wxString( wxT("866 (Russian)") ) , wxEmptyString, wxITEM_CHECK );
	OEM->Append( m_menuItem62 );
	
	SingleByte->Append( -1, wxT("OEM"), OEM );
	
	ISO = new wxMenu();
	wxMenuItem* m_menuItem67;
	m_menuItem67 = new wxMenuItem( ISO, MENU_CP_ISO_8859_1, wxString( wxT("ISO-8859-1 (Latin 1)") ) , wxEmptyString, wxITEM_CHECK );
	ISO->Append( m_menuItem67 );
	
	wxMenuItem* m_menuItem68;
	m_menuItem68 = new wxMenuItem( ISO, MENU_CP_ISO_8859_2, wxString( wxT("ISO-8859-2 (Latin 2)") ) , wxEmptyString, wxITEM_CHECK );
	ISO->Append( m_menuItem68 );
	
	wxMenuItem* m_menuItem69;
	m_menuItem69 = new wxMenuItem( ISO, MENU_CP_ISO_8859_3, wxString( wxT("ISO-8859-3 (Latin 3)") ) , wxEmptyString, wxITEM_CHECK );
	ISO->Append( m_menuItem69 );
	
	wxMenuItem* m_menuItem70;
	m_menuItem70 = new wxMenuItem( ISO, MENU_CP_ISO_8859_4, wxString( wxT("ISO-8859-4 (Baltic)") ) , wxEmptyString, wxITEM_CHECK );
	ISO->Append( m_menuItem70 );
	
	wxMenuItem* m_menuItem71;
	m_menuItem71 = new wxMenuItem( ISO, MENU_CP_ISO_8859_5, wxString( wxT("ISO-8859-5 (Cyrillic)") ) , wxEmptyString, wxITEM_CHECK );
	ISO->Append( m_menuItem71 );
	
	wxMenuItem* m_menuItem72;
	m_menuItem72 = new wxMenuItem( ISO, MENU_CP_ISO_8859_6, wxString( wxT("ISO-8859-6 (Arabic)") ) , wxEmptyString, wxITEM_CHECK );
	ISO->Append( m_menuItem72 );
	
	wxMenuItem* m_menuItem73;
	m_menuItem73 = new wxMenuItem( ISO, MENU_CP_ISO_8859_8, wxString( wxT("ISO-8859-8 (Hebrew)") ) , wxEmptyString, wxITEM_CHECK );
	ISO->Append( m_menuItem73 );
	
	wxMenuItem* m_menuItem74;
	m_menuItem74 = new wxMenuItem( ISO, MENU_CP_ISO_8859_9, wxString( wxT("ISO-8859-9 (Turkish)") ) , wxEmptyString, wxITEM_CHECK );
	ISO->Append( m_menuItem74 );
	
	wxMenuItem* m_menuItem75;
	m_menuItem75 = new wxMenuItem( ISO, MENU_CP_ISO_8859_15, wxString( wxT("ISO-8859-15 (Latin 9)") ) , wxEmptyString, wxITEM_CHECK );
	ISO->Append( m_menuItem75 );
	
	SingleByte->Append( -1, wxT("ISO"), ISO );
	
	EncodingMenu->Append( -1, wxT("Single Byte"), SingleByte );
	
	DoubleByte = new wxMenu();
	wxMenuItem* m_menuItem63;
	m_menuItem63 = new wxMenuItem( DoubleByte, MENU_CP_DB_JapaneseShiftJIS, wxString( wxT("932 (Japanese Shift-JIS)") ) , wxEmptyString, wxITEM_CHECK );
	DoubleByte->Append( m_menuItem63 );
	
	wxMenuItem* m_menuItem65;
	m_menuItem65 = new wxMenuItem( DoubleByte, MENU_CP_DB_SimplifiedChineseGBK, wxString( wxT("936 (Simplified Chinese GBK)") ) , wxEmptyString, wxITEM_CHECK );
	DoubleByte->Append( m_menuItem65 );
	
	wxMenuItem* m_menuItem64;
	m_menuItem64 = new wxMenuItem( DoubleByte, MENU_CP_DB_Korean, wxString( wxT("949 (Korean)") ) , wxEmptyString, wxITEM_CHECK );
	DoubleByte->Append( m_menuItem64 );
	
	wxMenuItem* m_menuItem66;
	m_menuItem66 = new wxMenuItem( DoubleByte, MENU_CP_DB_TraditionalChineseBigFive, wxString( wxT("950 (Traditional Chinese Big5)") ) , wxEmptyString, wxITEM_CHECK );
	DoubleByte->Append( m_menuItem66 );
	
	EncodingMenu->Append( -1, wxT("Double Byte"), DoubleByte );
	
	UniCode = new wxMenu();
	wxMenuItem* m_menuItem28;
	m_menuItem28 = new wxMenuItem( UniCode, MENU_CCP_UTF7, wxString( wxT("UTF-7") ) , wxEmptyString, wxITEM_CHECK );
	UniCode->Append( m_menuItem28 );
	
	wxMenuItem* m_menuItem281;
	m_menuItem281 = new wxMenuItem( UniCode, MENU_CCP_UTF8, wxString( wxT("UTF-8") ) , wxEmptyString, wxITEM_CHECK );
	UniCode->Append( m_menuItem281 );
	
	wxMenuItem* m_menuItem26;
	m_menuItem26 = new wxMenuItem( UniCode, MENU_CCP_UCS2LE, wxString( wxT("UCS-2 LE") ) , wxEmptyString, wxITEM_CHECK );
	UniCode->Append( m_menuItem26 );
	
	wxMenuItem* m_menuItem27;
	m_menuItem27 = new wxMenuItem( UniCode, MENU_CCP_UTF16, wxString( wxT("UTF-16") ) , wxEmptyString, wxITEM_CHECK );
	UniCode->Append( m_menuItem27 );
	
	EncodingMenu->Append( -1, wxT("Unicode"), UniCode );
	
	MenuBar->Append( EncodingMenu, wxT("Encoding") ); 
	
	DebugMenu = new wxMenu();

#if _SD_BREAK_ALL
	wxMenuItem* OnBreakAll;
  OnBreakAll = new wxMenuItem( DebugMenu, TM_EVENT_BREAK_ALL, wxString( wxT("Break All\tF4") ) , wxEmptyString, wxITEM_NORMAL );
	#ifdef __WXMSW__
	OnBreakAll->SetBitmaps( control_pause_png_to_wx_bitmap() );
	#elif defined( __WXGTK__ )
	OnBreakAll->SetBitmap( control_pause_png_to_wx_bitmap() );
	#endif
	DebugMenu->Append( OnBreakAll );

  wxMenuItem* OnContinueAll;
  OnContinueAll = new wxMenuItem( DebugMenu, TM_EVENT_STEP_CONTINUE, wxString( wxT("Continue All\tCtrl+F5") ), wxEmptyString, wxITEM_NORMAL );
  OnContinueAll->SetBitmaps( wxMEMORY_BITMAP(icon_control_play_color) );
  DebugMenu->Append( OnContinueAll );

  DebugMenu->AppendSeparator();
#endif

  wxMenuItem* OnContinue;
  OnContinue = new wxMenuItem( DebugMenu, TM_EVENT_STEP_CONTINUE, wxString( wxT("Continue\tF5") ), wxEmptyString, wxITEM_NORMAL );
  #ifdef __WXMSW__
  OnContinue->SetBitmaps( control_play_png_to_wx_bitmap() );
  #elif defined( __WXGTK__ )
  OnContinue->SetBitmap( control_play_png_to_wx_bitmap() );
  #endif
  DebugMenu->Append( OnContinue );

	wxMenuItem* OnStepInto;
	OnStepInto = new wxMenuItem( DebugMenu, TM_EVENT_STEP_INTO, wxString( wxT("Step into\tF11") ) , wxEmptyString, wxITEM_NORMAL );
	#ifdef __WXMSW__
	OnStepInto->SetBitmaps( timeline_marker_png_to_wx_bitmap() );
	#elif defined( __WXGTK__ )
	OnStepInto->SetBitmap( timeline_marker_png_to_wx_bitmap() );
	#endif
	DebugMenu->Append( OnStepInto );
	
	wxMenuItem* OnStepOver;
	OnStepOver = new wxMenuItem( DebugMenu, TM_EVENT_STEP_OVER, wxString( wxT("Step over\tF10") ) , wxEmptyString, wxITEM_NORMAL );
	#ifdef __WXMSW__
	OnStepOver->SetBitmaps( arrow_turn_right_upside_png_to_wx_bitmap() );
	#elif defined( __WXGTK__ )
	OnStepOver->SetBitmap( arrow_turn_right_upside_png_to_wx_bitmap() );
	#endif
	DebugMenu->Append( OnStepOver );

#if _VBS_1428_STEP_FILE
  wxMenuItem* OnStepFile = new wxMenuItem(DebugMenu,TM_EVENT_STEP_FILE, wxString(L"Next file\tCtrl+F11"), wxEmptyString, wxITEM_NORMAL );
	OnStepFile->SetBitmaps( wxMEMORY_BITMAP(icon_step_file) );
  DebugMenu->Append( OnStepFile );
#endif

	wxMenuItem* OnStepOut;
	OnStepOut = new wxMenuItem( DebugMenu, TM_EVENT_STEP_OUT, wxString( wxT("Step out\tShift+F11") ) , wxEmptyString, wxITEM_NORMAL );

	#ifdef __WXMSW__
	OnStepOut->SetBitmaps( arrow_merge_down_png_to_wx_bitmap() );
	#elif defined( __WXGTK__ )
	OnStepOut->SetBitmap( arrow_merge_down_png_to_wx_bitmap() );
	#endif
	DebugMenu->Append( OnStepOut );
	
	DebugMenu->AppendSeparator();
	
	wxMenuItem* OnToggleBP;
	OnToggleBP = new wxMenuItem( DebugMenu, TM_EVENT_TOGGLE_BP, wxString( wxT("Toggle Break Point \tF9") ) , wxEmptyString, wxITEM_NORMAL );
	DebugMenu->Append( OnToggleBP );
	
	MenuBar->Append( DebugMenu, wxT("Debug") ); 

	HelpMenu = new wxMenu();
#if _VBS2_SD_HELP_ENTRY
	wxMenuItem* OnHelpFile;
	OnHelpFile = new wxMenuItem( HelpMenu, wxID_ANY, wxString( wxT("Help File") ) , wxEmptyString, wxITEM_NORMAL );
	#ifdef __WXMSW__
	OnHelpFile->SetBitmaps( help_png_to_wx_bitmap() );
	#elif defined( __WXGTK__ )
	OnHelpFile->SetBitmap( help_png_to_wx_bitmap() );
	#endif
	HelpMenu->Append( OnHelpFile );
	
	HelpMenu->AppendSeparator();
#endif

	wxMenuItem* OnAbout;
	OnAbout = new wxMenuItem( HelpMenu, MENU_ON_ABOUT, wxString( wxT("About") ) , wxEmptyString, wxITEM_NORMAL );
	HelpMenu->Append( OnAbout );
#if !_SD_UI_IMPROVEMENTS
	MenuBar->Append( HelpMenu, wxT("Help") ); 
#endif

	DevloperMenu = new wxMenu();
	DebugMenuDev = new wxMenu();
	wxMenuItem* m_menuItem8;
	m_menuItem8 = new wxMenuItem( DebugMenuDev, MENU_ON_VIEW_ERROR_MSG, wxString( wxT("Error Messages") ) , wxEmptyString, wxITEM_NORMAL );
	DebugMenuDev->Append( m_menuItem8 );
	
	wxMenuItem* m_menuItem10;
	m_menuItem10 = new wxMenuItem( DebugMenuDev, MENU_ON_VIEW_CALL_STACK, wxString( wxT("Call Stack") ) , wxEmptyString, wxITEM_NORMAL );
	DebugMenuDev->Append( m_menuItem10 );
	
	wxMenuItem* m_menuItem11;
	m_menuItem11 = new wxMenuItem( DebugMenuDev, MENU_ON_VIEW_WATCH, wxString( wxT("Watch Values") ) , wxEmptyString, wxITEM_NORMAL );
	DebugMenuDev->Append( m_menuItem11 );
	
	DebugMenuDev->AppendSeparator();
	
	wxMenuItem* m_menuItem12;
	m_menuItem12 = new wxMenuItem( DebugMenuDev, MENU_ON_DEBUG_VIEW_DEFAULT, wxString( wxT("Default View") ) , wxEmptyString, wxITEM_NORMAL );
	DebugMenuDev->Append( m_menuItem12 );
	
	DevloperMenu->Append( -1, wxT("Debug"), DebugMenuDev );
	
	ViewMenuDev = new wxMenu();
#if _SD_EH_TAB
	wxMenuItem* ViewEventHandlerMenu;
	ViewEventHandlerMenu = new wxMenuItem( ViewMenuDev, MENU_ON_HANDLER, wxString( wxT("Event Handler") ) , wxEmptyString, wxITEM_NORMAL );
	ViewMenuDev->Append( ViewEventHandlerMenu );
#endif

	wxMenuItem* ViewVmMenu;
#if _VBS2_SD_MENU_RM_ACTIVE_SCRIPT
	ViewVmMenu = new wxMenuItem( ViewMenuDev, MENU_ON_VM, wxString( wxT("Open Files") ) , wxEmptyString, wxITEM_NORMAL );
#else
	ViewVmMenu = new wxMenuItem( ViewMenuDev, MENU_ON_VM, wxString( wxT("Virtual Machine") ) , wxEmptyString, wxITEM_NORMAL );
#endif
	ViewMenuDev->Append( ViewVmMenu );
	
	ViewMenuDev->AppendSeparator();
	
	DevloperMenu->Append( -1, wxT("ViewMenu"), ViewMenuDev );
	
#if _SD_PERF_LOG
	wxMenuItem* PerformanceMenu;
	PerformanceMenu = new wxMenuItem( DevloperMenu, MENU_ON_PERF, wxString( wxT("Performance") ) , wxEmptyString, wxITEM_NORMAL );
	DevloperMenu->Append( PerformanceMenu );
	DevloperMenu->AppendSeparator();
#endif
#if _SD_DEBUG_LOG
	wxMenuItem* OnDebugWindow;
	OnDebugWindow = new wxMenuItem( DevloperMenu, MENU_ON_DEBUG, wxString( wxT("Debug Window") ) , wxEmptyString, wxITEM_NORMAL );
	DevloperMenu->Append( OnDebugWindow );
#endif
#if _SD_SCRIPT_LOG
  wxMenuItem* OnScriptLogWindow = new wxMenuItem(DevloperMenu, MENU_ON_SCRIPTLOG, wxT("Script Log Window"), wxEmptyString, wxITEM_NORMAL );
  DevloperMenu->Append( OnScriptLogWindow );
#endif

#if _VBS_14573_DEBUG_CONSOLE
  wxMenuItem* OnDebugConsoleWindow = new wxMenuItem(DevloperMenu, MENU_ON_DEBUGCONSOLE, wxT("Debug Console Window"), wxEmptyString, wxITEM_NORMAL );
  DevloperMenu->Append( OnDebugConsoleWindow );
#endif

  DevloperMenu->AppendSeparator();
  DevloperMenu->Append( new wxMenuItem(DevloperMenu, MENU_ON_SAVE_LAYOUT, wxString( wxT("Save Settings")), wxEmptyString, wxITEM_NORMAL));
	
	MenuBar->Append( DevloperMenu, wxT("Developer") ); 
	
#if _SD_UI_IMPROVEMENTS
  //help should be appended last
	MenuBar->Append( HelpMenu, wxT("Help") ); 
#endif

	this->SetMenuBar( MenuBar );
	
	ToolBarScriptDebugger = new wxAuiToolBar( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxAUI_TB_HORZ_LAYOUT|wxNO_BORDER ); 
	ToolBarScriptDebugger->AddTool( IDC_BtnNewFile, wxT("New File"), document_bmp_to_wx_bitmap(), wxNullBitmap, wxITEM_NORMAL, wxT("New File"), wxEmptyString, NULL ); 
	
	ToolBarScriptDebugger->AddTool( IDC_BtnOpenFile, wxT("Open File"), VSFolder_open_bmp_to_wx_bitmap(), wxNullBitmap, wxITEM_NORMAL, wxT("Open File"), wxEmptyString, NULL ); 
	
	ToolBarScriptDebugger->AddTool( IDC_BtnOpenMissionFile, wxT("Open Mission File"), MissionFolder_bmp_to_wx_bitmap(), wxNullBitmap, wxITEM_NORMAL, wxT("Open Mission File"), wxEmptyString, NULL ); 
	
	ToolBarScriptDebugger->AddTool( IDC_BtnSaveFile, wxT("Save File"), save_bmp_to_wx_bitmap(), wxNullBitmap, wxITEM_NORMAL, wxT("Save File"), wxEmptyString, NULL ); 
	
	ToolBarScriptDebugger->AddTool( IDC_BtnSaveAs, wxT("Save As"), savenew_bmp_to_wx_bitmap(), wxNullBitmap, wxITEM_NORMAL, wxT("Save As"), wxEmptyString, NULL ); 
	
	ToolBarScriptDebugger->AddSeparator(); 
	
#if _SD_BREAK_ALL
	ToolBarScriptDebugger->AddTool( IDC_BtnBreakAll, wxT("Break All"), control_pause_png_to_wx_bitmap(), wxNullBitmap, wxITEM_NORMAL, wxT("Break all scripts"), wxEmptyString, NULL ); 
  ToolBarScriptDebugger->AddTool( IDC_BtnContinueAll, wxT("Continue All Scripts"), wxMEMORY_BITMAP(icon_control_play_color), wxNullBitmap, wxITEM_NORMAL, wxT("Continue all scripts"), wxEmptyString, NULL ); 
  ToolBarScriptDebugger->AddSeparator(); 
#endif

	ToolBarScriptDebugger->AddTool( IDC_BtnContinueSingle, wxT("Continue Single Script"), control_play_png_to_wx_bitmap(), wxNullBitmap, wxITEM_NORMAL, wxT("Continue Single Script"), wxEmptyString, NULL ); 

	ToolBarScriptDebugger->AddTool( IDC_BtnStepInto, wxT("Next Command"), timeline_marker_png_to_wx_bitmap(), wxNullBitmap, wxITEM_NORMAL, wxT("Step into the new line"), wxEmptyString, NULL ); 
	
	ToolBarScriptDebugger->AddTool( IDC_BtnStepOver, wxT("Next Line"), arrow_turn_right_upside_png_to_wx_bitmap(), wxNullBitmap, wxITEM_NORMAL, wxT("Step over the current line"), wxEmptyString, NULL ); 
	
#if _VBS_1428_STEP_FILE
  ToolBarScriptDebugger->AddTool( IDC_BtnStepFile, wxT("Next File"), wxMEMORY_BITMAP(icon_step_file), wxNullBitmap, wxITEM_NORMAL, wxT("Step into next script file"), wxEmptyString, NULL);
#endif

	ToolBarScriptDebugger->AddTool( IDC_BtnStepOut, wxT("Step Out"), arrow_merge_down_png_to_wx_bitmap(), wxNullBitmap, wxITEM_NORMAL, wxT("Step out of the current statement"), wxEmptyString, NULL ); 
	
	ToolBarScriptDebugger->AddSeparator(); 
	
	ToolBarScriptDebugger->AddTool( IDC_BtnDisconnect, wxT("Connect"), disconnect_bmp_to_wx_bitmap(), wxNullBitmap, wxITEM_NORMAL, wxT("Connect Debugger"), wxEmptyString, NULL ); 
	
#if _SD_SCRIPT_LOG
  ToolBarScriptDebugger->AddTool( IDC_BtnScriptLog, wxT("Enable Script Log"), wxMEMORY_BITMAP(icon_script_log_off), wxNullBitmap, wxITEM_NORMAL, wxT("Enable Script Log"), wxEmptyString,NULL);
#endif

	ToolBarScriptDebugger->Realize();
  m_mgr.AddPane( ToolBarScriptDebugger, wxAuiPaneInfo().Name(wxT("ToolBarScriptDebugger")).Top().CaptionVisible( false ).PinButton( true ).PaneBorder( false ).Movable( false ).Dock().Fixed().Floatable( false ) );
	
	LeftPanelNoteBook = new wxFlatNotebook(this, IDC_LEFT_NOTE_BOOK, wxDefaultPosition, wxSize( 250,-1 ), wxFNB_BOTTOM|wxFNB_NO_NAV_BUTTONS|wxFNB_NO_X_BUTTON);
	
	LeftPanelNoteBook->SetCustomizeOptions( wxFNB_CUSTOM_ALL );
	LeftPanelNoteBook->SetBackgroundColour( wxSystemSettings::GetColour( wxSYS_COLOUR_BTNFACE ) );
	m_mgr.AddPane( LeftPanelNoteBook, wxAuiPaneInfo().Name(wxT("LeftPanelNoteBook")) .Left() .PinButton( true ).Dock().Resizable().FloatingSize( wxDefaultSize ).DockFixed( false ) );
	
	DocumentSelectorPanel = new wxPanel( LeftPanelNoteBook, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* bSizer19;
	bSizer19 = new wxBoxSizer( wxVERTICAL );
	
	DocumentSelector = new wxListCtrl( DocumentSelectorPanel, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxLC_REPORT );
	bSizer19->Add( DocumentSelector, 1, wxEXPAND, 5 );
	
	
	DocumentSelectorPanel->SetSizer( bSizer19 );
	DocumentSelectorPanel->Layout();
	bSizer19->Fit( DocumentSelectorPanel );
	LeftPanelNoteBook->AddPage( DocumentSelectorPanel, wxT("Open Files"), false ); 
	DeveloperDrivePanel = new wxPanel( LeftPanelNoteBook, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* bSizer16;
	bSizer16 = new wxBoxSizer( wxVERTICAL );
	
	DeveloperDriveTreeCtrl = new wxTreeCtrl( DeveloperDrivePanel, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTR_DEFAULT_STYLE|wxNO_BORDER );
	bSizer16->Add( DeveloperDriveTreeCtrl, 1, wxEXPAND, 5 );
	
	
	DeveloperDrivePanel->SetSizer( bSizer16 );
	DeveloperDrivePanel->Layout();
	bSizer16->Fit( DeveloperDrivePanel );
	LeftPanelNoteBook->AddPage( DeveloperDrivePanel, wxT("Mission Directory"), false ); 
#if _SD_VM_TREE
	VirtualMachinePanel = new wxPanel( LeftPanelNoteBook, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* bSizer14;
	bSizer14 = new wxBoxSizer( wxVERTICAL );
	
	VirtualMachineTreeCtrl = new wxTreeCtrl( VirtualMachinePanel, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTR_DEFAULT_STYLE|wxNO_BORDER );
	bSizer14->Add( VirtualMachineTreeCtrl, 1, wxEXPAND, 5 );
	
	VirtualMachinePanel->SetSizer( bSizer14 );
	VirtualMachinePanel->Layout();
	bSizer14->Fit( VirtualMachinePanel );
	LeftPanelNoteBook->AddPage( VirtualMachinePanel, wxT("Virtual Machine"), true ); 
#endif	
	
	HiddenPanel = new wxPanel( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	HiddenPanel->Hide();
	m_mgr.AddPane( HiddenPanel, wxAuiPaneInfo().Name(wxT("HiddenPanel")) .Left() .PinButton( true ).Hide().Dock().Resizable().FloatingSize( wxDefaultSize ).DockFixed( false ) );
	
	wxBoxSizer* bSizer161;
	bSizer161 = new wxBoxSizer( wxVERTICAL );
	
	
	HiddenPanel->SetSizer( bSizer161 );
	HiddenPanel->Layout();
	bSizer161->Fit( HiddenPanel );
#if _SD_EH_TAB
	PropertyGridPanel = new wxPanel( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxNO_BORDER|wxTAB_TRAVERSAL );
  m_mgr.AddPane( PropertyGridPanel, wxAuiPaneInfo().Name(wxT("PropertyGridPanel")) .Right() .Caption( wxT("Event Handler") ).PinButton( true ).Dock().Resizable().FloatingSize( wxSize( 150,46 ) ).DockFixed( false ).MinSize( wxSize( 150,-1 ) ) );
	
	wxBoxSizer* bSizer18;
	bSizer18 = new wxBoxSizer( wxVERTICAL );
	
	PropertyGrid = new wxPropertyGrid(PropertyGridPanel, IDC_EventHandler, wxDefaultPosition, wxDefaultSize, wxPG_DEFAULT_STYLE|wxNO_BORDER);
#if _SD_EH_SPLITTER_POS
  PropertyGrid->SetSplitterPosition( 500,true); //sets the splitter as far right as possible
#endif
	bSizer18->Add( PropertyGrid, 1, wxEXPAND, 5 );
	
	
	PropertyGridPanel->SetSizer( bSizer18 );
	PropertyGridPanel->Layout();
	bSizer18->Fit( PropertyGridPanel );
#endif
	m_panel14 = new wxPanel( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	m_mgr.AddPane( m_panel14, wxAuiPaneInfo().Name(wxT("MainScriptPane")) .Center() .Caption( wxT("Active Script") ).CaptionVisible( false ).PinButton( true ).Movable( false ).Dock().Resizable().FloatingSize( wxSize( 501,198 ) ).DockFixed( false ).Row( 0 ) );
	
	wxBoxSizer* bSizer17;
	bSizer17 = new wxBoxSizer( wxVERTICAL );
#if _SD_UI_IMPROVEMENTS
  DocumentFlatNoteBook = new wxFlatNotebook(m_panel14, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxFNB_FF2|wxFNB_NO_NAV_BUTTONS|wxFNB_X_ON_TAB|wxFNB_VC8|wxFNB_MOUSE_MIDDLE_CLOSES_TABS);
#else
  DocumentFlatNoteBook = new wxFlatNotebook(m_panel14, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxFNB_FF2|wxFNB_NO_NAV_BUTTONS|wxFNB_X_ON_TAB|wxFNB_VC8);
#endif
	
	DocumentFlatNoteBook->SetCustomizeOptions( wxFNB_CUSTOM_ALL );
	DocumentFlatNoteBook->SetBackgroundColour( wxSystemSettings::GetColour( wxSYS_COLOUR_BTNFACE ) );
	
	
	bSizer17->Add( DocumentFlatNoteBook, 1, wxEXPAND, 5 );
	
	
	m_panel14->SetSizer( bSizer17 );
	m_panel14->Layout();
	bSizer17->Fit( m_panel14 );
	BottomNoteBook = new wxFlatNotebookExt(this, IDC_BOTTOM_NOTEBOOK_ID, wxDefaultPosition, wxSize( -1,200 ), wxFNB_DEFAULT_STYLE|wxFNB_NO_NAV_BUTTONS|wxFNB_NO_X_BUTTON, wxT(""));
	
	BottomNoteBook->SetCustomizeOptions( wxFNB_CUSTOM_ALL );
	BottomNoteBook->SetBackgroundColour( wxSystemSettings::GetColour( wxSYS_COLOUR_BTNFACE ) );
	m_mgr.AddPane( BottomNoteBook, wxAuiPaneInfo().Name(wxT("BottomNoteBookPane")) .Bottom() .Caption( wxT("Debug") ).PinButton( true ).Dock().Resizable().FloatingSize( wxDefaultSize ).DockFixed( false ) );
	
	
	m_statusBar1 = this->CreateStatusBar( 1, wxST_SIZEGRIP|wxNO_BORDER, wxID_ANY );
	
	m_mgr.Update();
	this->Centre( wxBOTH );
}

DebugScriptWindow::~DebugScriptWindow()
{
	m_mgr.UnInit();
	
}

DiagFrameRate::DiagFrameRate( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxFrame( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxDefaultSize, wxDefaultSize );
	
	wxBoxSizer* bSizer18;
	bSizer18 = new wxBoxSizer( wxVERTICAL );
	
	m_panel13 = new wxPanel( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* bSizer13;
	bSizer13 = new wxBoxSizer( wxHORIZONTAL );
	
	m_splitter5 = new wxSplitterWindow( m_panel13, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxSP_3D );
	m_splitter5->SetSashGravity( 0.8 );
	m_splitter5->Connect( wxEVT_IDLE, wxIdleEventHandler( DiagFrameRate::m_splitter5OnIdle ), NULL, this );
	
	m_panel11 = new wxPanel( m_splitter5, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* bSizer14;
	bSizer14 = new wxBoxSizer( wxVERTICAL );
	
	PlotCtrl = new wxPlotCtrlExt(m_panel11, wxID_ANY, wxDefaultPosition, wxDefaultSize );
	PlotCtrl->SetScrollOnThumbRelease( false );
	PlotCtrl->SetDrawSymbols( false );
	PlotCtrl->SetDrawLines( false );
	PlotCtrl->SetDrawSpline( true );
	PlotCtrl->SetDrawGrid( true );
	PlotCtrl->SetAreaMouseFunction( wxPLOTCTRL_MOUSE_ZOOM );
	PlotCtrl->SetAreaMouseMarker( wxPLOTCTRL_MARKER_RECT );
	PlotCtrl->SetCrossHairCursor( false );
	PlotCtrl->SetShowXAxis( true );
	PlotCtrl->SetShowXAxisLabel( false );
	PlotCtrl->SetXAxisLabel( wxT("X Axis") );
	PlotCtrl->SetShowYAxis( true );
	PlotCtrl->SetShowYAxisLabel( false );
	PlotCtrl->SetYAxisLabel( wxT("Y Axis") );
	PlotCtrl->SetShowPlotTitle( false );
	PlotCtrl->SetPlotTitle( wxT("Title") );
	PlotCtrl->SetShowKey( true );
	PlotCtrl->SetKeyPosition( wxPoint( 100,100 ) );
	
	bSizer14->Add( PlotCtrl, 1, wxEXPAND, 5 );
	
	
	m_panel11->SetSizer( bSizer14 );
	m_panel11->Layout();
	bSizer14->Fit( m_panel11 );
	m_panel12 = new wxPanel( m_splitter5, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* bSizer16;
	bSizer16 = new wxBoxSizer( wxVERTICAL );
	
	PropGrid = new wxPropertyGrid(m_panel12, wxID_ANY, wxDefaultPosition, wxDefaultSize, 0);
	bSizer16->Add( PropGrid, 1, wxEXPAND, 5 );
	
	
	m_panel12->SetSizer( bSizer16 );
	m_panel12->Layout();
	bSizer16->Fit( m_panel12 );
	m_splitter5->SplitVertically( m_panel11, m_panel12, 647 );
	bSizer13->Add( m_splitter5, 1, wxEXPAND, 5 );
	
	
	m_panel13->SetSizer( bSizer13 );
	m_panel13->Layout();
	bSizer13->Fit( m_panel13 );
	bSizer18->Add( m_panel13, 1, wxEXPAND, 5 );
	
	
	this->SetSizer( bSizer18 );
	this->Layout();
	
	this->Centre( wxBOTH );
}

DiagFrameRate::~DiagFrameRate()
{
}

DialogSettings::DialogSettings( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxFrame( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxDefaultSize, wxDefaultSize );
	
	wxBoxSizer* bSizer1;
	bSizer1 = new wxBoxSizer( wxVERTICAL );
	
	m_panel8 = new wxPanel( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* bSizer12;
	bSizer12 = new wxBoxSizer( wxVERTICAL );
	
	m_staticText2 = new wxStaticText( m_panel8, wxID_ANY, wxT("Developer Path"), wxDefaultPosition, wxDefaultSize, 0 );
	m_staticText2->Wrap( -1 );
	bSizer12->Add( m_staticText2, 0, wxALL, 5 );
	
	DeveloperPath = new wxDirPickerCtrl( m_panel8, wxID_ANY, wxEmptyString, wxT("Select Developer Folder"), wxDefaultPosition, wxDefaultSize, wxDIRP_DEFAULT_STYLE );

  bSizer12->Add( DeveloperPath, 0, wxALL|wxEXPAND, 5 );

  OkButton = new wxButton(m_panel8, wxID_OK);
  bSizer12->Add( OkButton, 0, wxALIGN_CENTER, 0);
	
	PPDefTemplate = new wxPanel( m_panel8, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* bSizer19;
	bSizer19 = new wxBoxSizer( wxVERTICAL );
	
	PPDefintionSText = new wxStaticText( PPDefTemplate, wxID_ANY, wxT("Preprocessor Defintions"), wxDefaultPosition, wxDefaultSize, 0 );
	PPDefintionSText->Wrap( -1 );
	bSizer19->Add( PPDefintionSText, 0, wxALL|wxEXPAND, 5 );
	
	
	PPDefTemplate->SetSizer( bSizer19 );
	PPDefTemplate->Layout();
	bSizer19->Fit( PPDefTemplate );
	bSizer12->Add( PPDefTemplate, 1, wxEXPAND, 5 );
	
	
	m_panel8->SetSizer( bSizer12 );
	m_panel8->Layout();
	bSizer12->Fit( m_panel8 );
	bSizer1->Add( m_panel8, 1, wxEXPAND, 5 );
	
	
	this->SetSizer( bSizer1 );
	this->Layout();
	
	this->Centre( wxBOTH );
}

DialogSettings::~DialogSettings()
{
}

DialogStyle::DialogStyle( wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style ) : wxFrame( parent, id, title, pos, size, style )
{
	this->SetSizeHints( wxSize( 430,500 ), wxDefaultSize );
	
	wxBoxSizer* bSizer23;
	bSizer23 = new wxBoxSizer( wxVERTICAL );
	
	m_panel17 = new wxPanel( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* bSizer25;
	bSizer25 = new wxBoxSizer( wxHORIZONTAL );
	
	wxStaticBoxSizer* sbSizer5;
	sbSizer5 = new wxStaticBoxSizer( new wxStaticBox( m_panel17, wxID_ANY, wxT("Style type:") ), wxVERTICAL );
	
	TypeSelector = new wxListCtrl( m_panel17, wxID_ANY, wxDefaultPosition, wxSize( 160,-1 ), wxLC_LIST|wxLC_NO_HEADER|wxLC_REPORT|wxLC_SINGLE_SEL );
	sbSizer5->Add( TypeSelector, 1, wxALL, 5 );
	
	
	bSizer25->Add( sbSizer5, 0, wxEXPAND|wxALL, 5 );
	
	wxBoxSizer* bSizer32;
	bSizer32 = new wxBoxSizer( wxVERTICAL );
	
	wxStaticBoxSizer* sbSizer3;
	sbSizer3 = new wxStaticBoxSizer( new wxStaticBox( m_panel17, wxID_ANY, wxT("Color Style") ), wxVERTICAL );
	
	wxGridSizer* gSizer1;
	gSizer1 = new wxGridSizer( 2, 2, 0, 0 );
	
	staticText = new wxStaticText( m_panel17, wxID_ANY, wxT("Foreground colour"), wxDefaultPosition, wxDefaultSize, 0 );
	staticText->Wrap( -1 );
	gSizer1->Add( staticText, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	TypeForgroundColour = new wxColourPickerCtrl( m_panel17, wxID_ANY, *wxBLACK, wxDefaultPosition, wxDefaultSize, wxCLRP_DEFAULT_STYLE );
	gSizer1->Add( TypeForgroundColour, 0, wxALL|wxALIGN_CENTER_HORIZONTAL, 5 );
	
	staticText1 = new wxStaticText( m_panel17, wxID_ANY, wxT("Background colour"), wxDefaultPosition, wxDefaultSize, 0 );
	staticText1->Wrap( -1 );
	gSizer1->Add( staticText1, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	TypeGroundColour = new wxColourPickerCtrl( m_panel17, wxID_ANY, *wxBLACK, wxDefaultPosition, wxDefaultSize, wxCLRP_DEFAULT_STYLE );
	gSizer1->Add( TypeGroundColour, 0, wxALL|wxALIGN_CENTER_HORIZONTAL, 5 );
	
	
	sbSizer3->Add( gSizer1, 0, wxALL, 5 );
	
	
	bSizer32->Add( sbSizer3, 0, wxALL|wxEXPAND, 5 );
	
	wxStaticBoxSizer* sbSizer4;
	sbSizer4 = new wxStaticBoxSizer( new wxStaticBox( m_panel17, wxID_ANY, wxT("Font Style") ), wxVERTICAL );
	
	wxBoxSizer* bSizer28;
	bSizer28 = new wxBoxSizer( wxHORIZONTAL );
	
	m_staticText11 = new wxStaticText( m_panel17, wxID_ANY, wxT("Font name:"), wxDefaultPosition, wxSize( 60,-1 ), 0 );
	m_staticText11->Wrap( -1 );
	bSizer28->Add( m_staticText11, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	wxArrayString TypeFontTypeChoices;
	TypeFontType = new wxChoice( m_panel17, IDC_FONT_NAME, wxDefaultPosition, wxDefaultSize, TypeFontTypeChoices, 0 );
	TypeFontType->SetSelection( 0 );
	bSizer28->Add( TypeFontType, 0, wxALL|wxEXPAND, 5 );
	
	
	sbSizer4->Add( bSizer28, 0, wxEXPAND|wxRIGHT|wxLEFT, 5 );
	
	wxBoxSizer* bSizer281;
	bSizer281 = new wxBoxSizer( wxHORIZONTAL );
	
	m_staticText111 = new wxStaticText( m_panel17, wxID_ANY, wxT("Font size:"), wxDefaultPosition, wxSize( 60,-1 ), 0 );
	m_staticText111->Wrap( -1 );
	bSizer281->Add( m_staticText111, 0, wxALL|wxALIGN_CENTER_VERTICAL, 5 );
	
	wxArrayString TypeFontSizeChoices;
	TypeFontSize = new wxChoice( m_panel17, IDC_FONT_SIZE, wxDefaultPosition, wxSize( 40,-1 ), TypeFontSizeChoices, 0 );
	TypeFontSize->SetSelection( 0 );
	bSizer281->Add( TypeFontSize, 0, wxALL|wxEXPAND, 5 );
	
	
	sbSizer4->Add( bSizer281, 0, wxEXPAND|wxLEFT, 5 );
	
	wxBoxSizer* bSizer37;
	bSizer37 = new wxBoxSizer( wxHORIZONTAL );
	
	wxBoxSizer* bSizer38;
	bSizer38 = new wxBoxSizer( wxVERTICAL );
	
	bSizer38->SetMinSize( wxSize( 70,-1 ) ); 
	
	bSizer37->Add( bSizer38, 0, 0, 5 );
	
	wxBoxSizer* bSizer30;
	bSizer30 = new wxBoxSizer( wxVERTICAL );
	
	TypeFontBold = new wxCheckBox( m_panel17, wxID_ANY, wxT("Bold"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer30->Add( TypeFontBold, 0, wxALL, 5 );
	
	TypeFontItalic = new wxCheckBox( m_panel17, wxID_ANY, wxT("Italic"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer30->Add( TypeFontItalic, 0, wxALL, 5 );
	
	TypeFontUnderline = new wxCheckBox( m_panel17, wxID_ANY, wxT("Underline"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer30->Add( TypeFontUnderline, 0, wxALL, 5 );
	
	
	bSizer37->Add( bSizer30, 0, wxEXPAND|wxTOP|wxRIGHT|wxLEFT, 5 );
	
	
	sbSizer4->Add( bSizer37, 1, wxEXPAND, 5 );
	
	
	bSizer32->Add( sbSizer4, 1, wxALL|wxEXPAND, 5 );
	
	
	bSizer25->Add( bSizer32, 1, wxEXPAND, 5 );
	
	
	m_panel17->SetSizer( bSizer25 );
	m_panel17->Layout();
	bSizer25->Fit( m_panel17 );
	bSizer23->Add( m_panel17, 1, wxEXPAND, 5 );
	
	wxBoxSizer* bSizer41;
	bSizer41 = new wxBoxSizer( wxVERTICAL );
	
	m_panel18 = new wxPanel( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* bSizer43;
	bSizer43 = new wxBoxSizer( wxVERTICAL );
	
	wxBoxSizer* bSizer42;
	bSizer42 = new wxBoxSizer( wxHORIZONTAL );
	
	BtnSave = new wxButton( m_panel18, IDC_STYLE_OK, wxT("Save"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer42->Add( BtnSave, 0, wxALL, 5 );
	
	BtnCancel = new wxButton( m_panel18, IDC_STYLE_CANCEL, wxT("Cancel"), wxDefaultPosition, wxDefaultSize, 0 );
	bSizer42->Add( BtnCancel, 0, wxALL, 5 );
	
	
	bSizer43->Add( bSizer42, 1, wxALIGN_CENTER_HORIZONTAL, 5 );
	
	
	m_panel18->SetSizer( bSizer43 );
	m_panel18->Layout();
	bSizer43->Fit( m_panel18 );
	bSizer41->Add( m_panel18, 1, wxEXPAND, 5 );
	
	
	bSizer23->Add( bSizer41, 0, wxEXPAND, 5 );
	
	
	this->SetSizer( bSizer23 );
	this->Layout();
	
	this->Centre( wxBOTH );
}

DialogStyle::~DialogStyle()
{
}

DiagAddDefintionTemplate::DiagAddDefintionTemplate( wxWindow* parent, wxWindowID id, const wxPoint& pos, const wxSize& size, long style ) : wxPanel( parent, id, pos, size, style )
{
	wxBoxSizer* bSizer13;
	bSizer13 = new wxBoxSizer( wxHORIZONTAL );
	
	TextBoxPPDefintions = new wxTextCtrl( this, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0 );
	bSizer13->Add( TextBoxPPDefintions, 1, wxALL, 5 );
	
	BtnAdd = new wxBitmapButton( this, IDC_ADDBTN, add_png_to_wx_bitmap(), wxDefaultPosition, wxDefaultSize, wxBU_AUTODRAW );
	bSizer13->Add( BtnAdd, 0, wxTOP|wxBOTTOM, 5 );
	
	BtnSub = new wxBitmapButton( this, IDC_RMBTN, subtract_png_to_wx_bitmap(), wxDefaultPosition, wxDefaultSize, wxBU_AUTODRAW );
	bSizer13->Add( BtnSub, 0, wxTOP|wxBOTTOM|wxRIGHT, 5 );
	
	
	this->SetSizer( bSizer13 );
	this->Layout();
}

DiagAddDefintionTemplate::~DiagAddDefintionTemplate()
{
}

wxFlatNotebookTabContextMenu::~wxFlatNotebookTabContextMenu()
{
	if (eventData != NULL)
	{
		delete eventData;
	}
}

void wxFlatNotebookExt::initEventHandlers()
{
	m_pages->Connect(wxEVT_RIGHT_DOWN,wxMouseEventHandler(wxFlatNotebookExt::onPageRightDown));
};

void wxFlatNotebookExt::registerUndockablePage(wxUndockableEntry & info)
{
	undockableTabs->push_back(info);
};

//right click event for flat notebook tabs
void wxFlatNotebookExt::onPageRightDown(wxMouseEvent & event)
{
	//this should only be called from the wxPageContainer, but never hurts to be safe
	wxPageContainer * pgContainer = dynamic_cast<wxPageContainer*>(event.GetEventObject());
	if (pgContainer == NULL)
	{
		event.Skip();
		return;
	}

	wxPageInfo pgInfo;
	int tabIdx;
	//returns the tabIdx on which the user clicked
	int clickArea = pgContainer->HitTest(event.GetPosition(),pgInfo,tabIdx);

	wxFlatNotebookExt * notebook = static_cast<wxFlatNotebookExt*>(pgContainer->GetParent());

	//build the context menu
	wxFlatNotebookTabContextMenu contextMenu;
	//data to go to the menu click events
	wxFlatNtbContextMenuEvtData * data = new wxFlatNtbContextMenuEvtData();
	data->tabIndex = tabIdx;
	data->notebook = notebook;

	contextMenu.eventData = data;

	//check if this tab can be unpinned
	bool canUnpin = false;

	wxWindow * window = notebook->GetPage(tabIdx);

	for (std::list<wxUndockableEntry>::iterator i = notebook->undockableTabs->begin(); i != notebook->undockableTabs->end(); ++i)
	{
		if (i->windowPtr == window)
		{
			canUnpin = true;
			break;
		}
	};
	
	if (canUnpin)
	{
		contextMenu.Append(CNTXT_MENU_UNPIN_TAB,wxT("Unpin window"),wxT("Moves the window to a separate window"));
	};
	contextMenu.Connect(wxEVT_COMMAND_MENU_SELECTED,wxCommandEventHandler(wxFlatNotebookExt::OnContextMenuSelect));

	pgContainer->PopupMenu(&contextMenu);
	event.Skip();
};

//handles moving tabs from the notebook to their own windows
void wxFlatNotebookExt::UnpinPage(size_t pageIndex)
{
	wxWindow * window = this->GetPage(pageIndex);
	UnpinnableWindow * unpinnable = dynamic_cast<UnpinnableWindow*> (window);
	if (unpinnable)
	{
		//get top level parent (should be the frame)
		wxWindow * parent = window->GetParent();
		while ( parent->GetParent())
		{
			parent = parent->GetParent();
		};
	
		//remove the window from this notebook too
		for (std::list<wxUndockableEntry>::iterator i = undockableTabs->begin(); i != undockableTabs->end(); ++i)
		{
			if (i->windowPtr == window)
			{
				undockableTabs->erase(i);
				break;
			}
		};
		this->RemovePage(pageIndex);
		
    //call the unpinWindow procedure, which takes the frame as parameter -> handles registering the window with the main frame
		unpinnable->unpinWindow(parent);
	}
};


//handles the right-click context menu on flatNotebook tabs 
void wxFlatNotebookExt::OnContextMenuSelect(wxCommandEvent & event)
{
	int id = event.GetId();
	switch (id)
	{
	case CNTXT_MENU_UNPIN_TAB:
		{
			wxFlatNotebookTabContextMenu * contextMenu = static_cast<wxFlatNotebookTabContextMenu*> (event.GetEventObject());

			wxFlatNtbContextMenuEvtData * data = contextMenu->eventData;
			data->notebook->UnpinPage(data->tabIndex);
			break;
		}
	}
};

//let's override the zoom keys to something more reasonable
void wxPlotCtrlExt::ProcessAreaEVT_CHAR(wxKeyEvent &event)
{
  switch (event.GetKeyCode())
  {
  case wxT('+'):  //zoom in
    {
      wxRect2DDouble r = GetViewRect();
      r.Scale(.67); //zooms in to about 130%
      r.SetCentre(GetAreaMousePoint());
      SetViewRect(r, true);
      return; 
    };
  default: 
    {
      OnChar(event); //use the old method for handling key events if it's not one of our overriding keys
      return;
    };
  }
};