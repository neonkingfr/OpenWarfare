#include "wx/image.h"
#include "wx/imaglist.h"
#include "wx/icon.h"

#include "ImgManager.hpp"
#include "ResourceLoader.hpp"
#include "../../Common/Essential/AppFrameWork.hpp"
#include <wx/mstream.h>

ImgManager GImgManager;

ImgManager::ImgManager()
{
  _imgList = new wxImageList(16,16,true);
}

ImgManager::~ImgManager()
{
  delete _imgList;
}

wxImageList *ImgManager::GetImageList()
{ 
  return _imgList; 
}


bool ImgManager::GetWxImage(int idc, wxImage &image, wxBitmapType type)
{
  SmartRef<Resource> rsc = GResourceCache.LoadResource(idc);
  if (!rsc) return false;

  wxMemoryInputStream stream(rsc->_data, rsc->_size);
  image.LoadFile(stream, type);
  if (image.IsOk()) return true;

  // Failed to load the icon,bmp,png
  return false;
}

bool ImgManager::GetWxBitMap(int idc, wxBitmap &bmp, wxBitmapType type)
{
  wxImage img;
  if (GetWxImage(idc,img,type))
  {
    bmp = wxBitmap(img);
    return true;
  }

  return false;
}

bool ImgManager::GetWxIcon(int idc, wxIcon &icon, wxBitmapType type)
{
  wxBitmap bmp;
  if (GetWxBitMap(idc,bmp,type))
  {
    icon.CopyFromBitmap(bmp);
    return true;
  }

  return false;
}

Index ImgManager::LoadIdcIntoImageList(int idc, wxBitmapType type)
{
  // First search the cache to see the idc has all-ready been loaded
  for (size_t i=0; i<_syncList.size(); i++) 
    if (_syncList[i]._idc == idc)
    {
      int index = _syncList[i]._imgIndex;
      return index;
    }

  // Use wxImage to support ICO,GIF,BMP, all range of image formats
  // even though, the image list only supports Ico, and BMP
  wxImage theBitmap;
  if (GetWxImage(idc,theBitmap,type))
  {
    int index = _imgList->Add(theBitmap);

    // Add a new syncItem into the list, so
    // the image is not reloaded again
    SyncItem syncItem;
    syncItem._idc = idc;
    syncItem._imgIndex = index;
    _syncList.push_back(syncItem);

    return index;
  }

  return Index();
}