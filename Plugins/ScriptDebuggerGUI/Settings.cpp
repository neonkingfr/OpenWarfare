#include "Settings.hpp"
#include "Util.hpp"
#include ".../../Common/Essential/AppFrameWork.hpp"

#if _SD_EMPTY_PROFILE_FIX
#include "wx/filename.h"
#endif

std::string ToSTDString(wxString &wxstring)
{
  return UTF16ToChar(wxstring);
}

WindowSettings::WindowSettings()
{
  SetDefaults();
}

void WindowSettings::SetDefaults()
{
  maximized = false;
  docked = true;
  stayOnTop = false;
  position = wxDefaultPosition;
  size = wxSize(800,600);
}

void WindowSettings::SaveToClassEntry(ConfigClass * classEntry)
{
  classEntry->Add(new ConfigEntry("maximized",new ConfigBool(maximized)));
  classEntry->Add(new ConfigEntry("docked",new ConfigBool(docked)));
  classEntry->Add(new ConfigEntry("stayOnTop",new ConfigBool(stayOnTop)));
  classEntry->Add(new ConfigEntry("posX", new ConfigInt(position.x)));
  classEntry->Add(new ConfigEntry("posY", new ConfigInt(position.y)));
  classEntry->Add(new ConfigEntry("sizeX", new ConfigInt(size.x)));
  classEntry->Add(new ConfigEntry("sizeY", new ConfigInt(size.y)));
}

void WindowSettings::Save(const char * name, ParamaFileConfig* paramFile)
{
  SmartRef<ConfigEntry> entry = new ConfigEntry(name);
  ConfigClass *configClass = new ConfigClass(entry);
  SaveToClassEntry(configClass);
  entry->SetEntry(configClass);
  paramFile->Add(entry);
}

void WindowSettings::Load(ConfigEntry* entry)
{
 assert (entry);
 if (entry->IsClass())
 {
    ConfigClass *classEntry = entry->GetClass();

    classEntry->GetVal("maximized",maximized);
    classEntry->GetVal("docked",docked);
    classEntry->GetVal("stayOnTop",stayOnTop);
    classEntry->GetVal("posX",position.x);
    classEntry->GetVal("posY",position.y);
    classEntry->GetVal("sizeX",size.x);
    classEntry->GetVal("sizeY",size.y);
 }
}

//////////////////////////
// MAIN WINDOW SETTINGS //
//////////////////////////

//PROTECTED
void MainWindowSettings::SaveToClassEntry(ConfigClass * classEntry)
{
  WindowSettings::SaveToClassEntry(classEntry);
  if (classEntry)
  {
    classEntry->Add(new ConfigEntry("AuiPerspective",new ConfigString(ToSTDString(_auiPersp))));
  }
}

//PUBLIC
void MainWindowSettings::SetAuiPerspective(const wxString& perspective)
{
  _auiPersp = perspective;
}

void MainWindowSettings::SetDefaultAuiPerspective(const wxString& perspective)
{
  _defaultAuiPersp = perspective;
}

wxString MainWindowSettings::GetAuiPerspective() const
{
  if (_auiPersp.Len() > 0)
    return _auiPersp;
  else
    return _defaultAuiPersp;
}

//needs to call parent_class::SetDefaults in inherited classes
void MainWindowSettings::SetDefaults()
{
  WindowSettings::SetDefaults();
  _auiPersp = _defaultAuiPersp;
}
  //Needs to call parent_class::Load in inherited classes
void MainWindowSettings::Load(ConfigEntry* entry)
{
  WindowSettings::Load(entry);
  if (entry)
  {
    if (entry->IsClass())
    {
      ConfigClass * classEntry = entry->GetClass();
      std::string perspective;
      classEntry->GetVal("AuiPerspective",perspective);
      _auiPersp = wxString::FromUTF8(perspective.c_str());
    }
  }
}

  //Should never call the parent class method - this actually creates the class record in the configFile
void MainWindowSettings::Save(const char * name, ParamaFileConfig* paramFile)
{
  //create the config class entry
  SmartRef<ConfigEntry> entry = new ConfigEntry(name);
  ConfigClass *configClass = new ConfigClass(entry);
  //add the values to the class entry
  WindowSettings::SaveToClassEntry(configClass);
  SaveToClassEntry(configClass);
  //write the entry to file
  entry->SetEntry(configClass);
  paramFile->Add(entry);
}

////////////////////////////
// OUTPUT WINDOW SETTINGS //
////////////////////////////

//PROTECTED
void OutputWindowSettings::LoadFilters(ConfigClass* classEntry)
{
  if (!classEntry)
    return;
  
  std::string filter;
  
  classEntry->GetVal("IncludeFilter",filter);
  _filters.stringInclude = CharToUTF16(filter);
  
  classEntry->GetVal("ExcludeFilter",filter);
  _filters.stringExclude = CharToUTF16(filter);
 
  classEntry->GetVal("HighlightFilter",filter);
  _filters.stringHighlight = CharToUTF16(filter);
}

void OutputWindowSettings::UpdateFilterHistory(const wxString & newFilter, std::list<wxString> & filterHistory)
{
  bool found = false;
  std::list<wxString>::iterator it = filterHistory.begin();
  while (it != filterHistory.end() && !found)
  {
    if (*it == newFilter)
    {
      filterHistory.splice(filterHistory.begin(),filterHistory,it);
      found = true;
    }
    ++it;
  }
  if (!found)
  {
    //insert
    filterHistory.insert(filterHistory.begin(),newFilter);
  }
}

void OutputWindowSettings::SetDefaultFilters()
{
  _filters.stringInclude = SETTINGS_FILTER_INC_DEFAULT;
  _filters.stringExclude = SETTINGS_FILTER_EXC_DEFAULT;
  _filters.stringHighlight = SETTINGS_FILTER_HIG_DEFAULT;
  UpdateFilterHistory(SETTINGS_FILTER_INC_DEFAULT,_filters.includeHistory);
  UpdateFilterHistory(SETTINGS_FILTER_EXC_DEFAULT,_filters.excludeHistory);
  UpdateFilterHistory(SETTINGS_FILTER_HIG_DEFAULT,_filters.highlightHistory);
}

//PUBLIC
OutputWindowSettings::OutputWindowSettings()
{
  SetDefaultFilters();
  _autoscroll = true;
}

void OutputWindowSettings::SetFilters(wxString &include, wxString &exclude, wxString &highlight)
{
  _filters.stringInclude = include;
  _filters.stringExclude = exclude;
  _filters.stringHighlight = highlight;
  UpdateFilterHistory(include,_filters.includeHistory);
  UpdateFilterHistory(exclude,_filters.excludeHistory);
  UpdateFilterHistory(highlight,_filters.highlightHistory);
}

wxString OutputWindowSettings::GetIncludeFilter() const
{
  return _filters.stringInclude;
}

wxString OutputWindowSettings::GetExcludeFilter() const
{
  return _filters.stringExclude;
}

wxString OutputWindowSettings::GetHighlighting() const
{
  return _filters.stringHighlight;
}

const std::list<wxString> & OutputWindowSettings::GetIncludeHistory() const
{
  return _filters.includeHistory;
}

const std::list<wxString> & OutputWindowSettings::GetExcludeHistory() const
{
  return _filters.excludeHistory;
}

const std::list<wxString> & OutputWindowSettings::GetHighlightHistory() const
{
  return _filters.highlightHistory;
}

bool OutputWindowSettings::GetAutoscroll() const
{
  return _autoscroll;
}

void OutputWindowSettings::SetAutoscroll(bool enable)
{
  _autoscroll = enable;
}

#if _SD_LIMIT_OUTPUT_LINES
int OutputWindowSettings::GetMaxOutputLines() const
{
  return _maxOutputLines;
}

void OutputWindowSettings::SetMaxOutputLines(int n)
{
  _maxOutputLines = n;
}
#endif

//inherited from WindowSettings
//PROTECTED
void OutputWindowSettings::SaveToClassEntry(ConfigClass* classEntry)
{
  if (classEntry)
  {
    classEntry->Add(new ConfigEntry("IncludeFilter",new ConfigString(ToSTDString(_filters.stringInclude))));
    classEntry->Add(new ConfigEntry("ExcludeFilter",new ConfigString(ToSTDString(_filters.stringExclude))));
    classEntry->Add(new ConfigEntry("HighlightFilter",new ConfigString(ToSTDString(_filters.stringHighlight))));
#if _SD_LIMIT_OUTPUT_LINES
    classEntry->Add(new ConfigEntry("MaxOutputLines",new ConfigInt(_maxOutputLines)));
#endif
  }
}

//PUBLIC
void OutputWindowSettings::SetDefaults()
{
  WindowSettings::SetDefaults();
  SetDefaultFilters();
  _autoscroll = true;
#if _SD_LIMIT_OUTPUT_LINES
  _maxOutputLines = 10000;
#endif
}

void OutputWindowSettings::Save(const char * name, ParamaFileConfig* paramFile)
{
  //create the config class entry
  SmartRef<ConfigEntry> entry = new ConfigEntry(name);
  ConfigClass *configClass = new ConfigClass(entry);
  //add the values to the class entry
  WindowSettings::SaveToClassEntry(configClass);
  SaveToClassEntry(configClass);
  //write the entry to file
  entry->SetEntry(configClass);
  paramFile->Add(entry);
}

void OutputWindowSettings::Load(ConfigEntry* entry)
{
  if (entry)
  {
    WindowSettings::Load(entry);
    if (entry->IsClass())
    {
      ConfigClass * classEntry = entry->GetClass(); 
      LoadFilters(classEntry);
#if _SD_LIMIT_OUTPUT_LINES
      classEntry->GetVal("MaxOutputLines",_maxOutputLines);
#endif
      classEntry->GetVal("autoscroll",_autoscroll);
    }
  }
}

////////////
//SETTINGS//
////////////
Settings::Settings()
{
  _enableDebugging = false;
  _enableScriptLog = false;
}

void Settings::SetDefaults()
{
  SetDefaultWindowSettings();
}

void Settings::LoadParam(ParamaFileConfig* paramFile)
{
  ConfigEntry* entry;
  
  // Get the defined developer path, P drive is assumed
  entry = paramFile->FindEntry("DeveloperPath");
  
  if (entry && entry->IsString())
  {
    std::string devPath;
    entry->GetVal(devPath);
    _developerPath = CharToUTF16(devPath);
#if _SD_LIVEFOLDER_PATHS
    _developerPath.MakeLower();
#endif
  }

  entry = paramFile->FindEntry("PreprocessorDefinitions");
  if (entry)
  {
    ConfigArray *entryArray = entry->GetArray();
    if (entryArray)
    {
      for (size_t i=0; i<entryArray->size(); i++)
      {
        std::string defintion;
        if ((*entryArray)[i]->GetVal(defintion)) _ppDefinitions.push_back(CharToUTF16(defintion));
      }
    }
  }

  //window settings
  LoadWindowSettings(paramFile);

  for (int i=0; i<MAX_CONFIG_STYLES; i++)
  {
    ConfigEntry *find = paramFile->FindEntry(GStyleRemapper[i].ConfigName);
#if _SD_DEFAULT_SCI_STYLE
    StyleLoader *newStyle = new StyleLoader(GStyleRemapper[i].StyleID);
#endif
    if (find)
    {
#if !_SD_DEFAULT_SCI_STYLE
      StyleLoader *newStyle = new StyleLoader(GStyleRemapper[i].StyleID);
#endif
      _styles[GStyleRemapper[i].StyleID] = newStyle;

      newStyle->LoadParam(find);
    }
#if _SD_DEFAULT_SCI_STYLE
    else
    {
      newStyle->SetDefaults(GStyleRemapper[i].StyleID);
      newStyle->_className = GStyleRemapper[i].ConfigName;
      _styles[GStyleRemapper[i].StyleID] = newStyle;
    }
#endif
  }
}

extern const TCHAR *GConfigFileName;
#if _SD_CONFIG_DOCUMENTS
extern std::wstring GConfigPath;
#endif

///////////////////
// WINDOW LAYOUT //
///////////////////

#if _SD_DEBUG_LOG || _SD_SCRIPT_LOG
OutputWindowSettings & Settings::GetOutputWindowSettings(DebuggerWindows wndType)
{
  switch (wndType)
  {
 #if _SD_SCRIPT_LOG
  case (WND_SCRIPT_LOG):
    return _scriptLogWindowSettings;
 #endif
 #if _SD_DEBUG_LOG
  case (WND_DEBUG_OUTPUT):
    return _debugOutputWindowSettings;
  default:
    return _debugOutputWindowSettings;
 #else
  default:
    return _scriptLogWindowSettings;
 #endif
  }
}

void Settings::SetOutputWindowSettings(OutputWindowSettings settings, DebuggerWindows wndType)
{
  switch (wndType)
  {
 #if _SD_SCRIPT_LOG
  case (WND_SCRIPT_LOG):
    _scriptLogWindowSettings = settings;
    break;
 #endif
 #if _SD_DEBUG_LOG
  case (WND_DEBUG_OUTPUT):
    _debugOutputWindowSettings = settings;
    break;
 #endif
  default:
    return; //nothing to do here - shouldn't even get here
  }
}
#endif

#if _VBS_14573_DEBUG_CONSOLE
WindowSettings & Settings::GetDebugConsoleWindowSettings()
{
  return _debugConsoleWindowSettings;
}
#endif

MainWindowSettings & Settings::GetMainWindowSettings()
{
  return _mainWindowSettings;
}

void Settings::SetDefaultWindowSettings()
{
#if _SD_DEBUG_LOG
  _debugOutputWindowSettings.SetDefaults();
#endif
#if _SD_SCRIPT_LOG
  _scriptLogWindowSettings.SetDefaults();
#endif
#if _VBS_14573_DEBUG_CONSOLE
  _debugConsoleWindowSettings.SetDefaults();
#endif
  _mainWindowSettings.SetDefaults();
}

void Settings::LoadWindowSettings(ParamaFileConfig* paramFile)
{
  SetDefaultWindowSettings();
#if _SD_DEBUG_LOG
  ConfigEntry * entry = paramFile->FindEntry("DebugOutputWindow");
  if (entry)
  {
    _debugOutputWindowSettings.Load(entry);
  }
#else
  ConfigEntry *entry = NULL;
#endif
#if _SD_SCRIPT_LOG
  entry = paramFile->FindEntry("ScriptLogWindow");
  if (entry)
  {
    _scriptLogWindowSettings.Load(entry);
  }
#endif
  entry = paramFile->FindEntry("MainWindow");
  if (entry)
  {
    _mainWindowSettings.Load(entry);
  }
#if _VBS_14573_DEBUG_CONSOLE
  entry = paramFile->FindEntry("DebugConsoleWindow");
  if (entry)
  {
    _debugConsoleWindowSettings.Load(entry);
  }
#endif
}

void Settings::SaveToConfig(ofstream &outputstream)
{
  ConfigArray *ppArray = new ConfigArray;
  for (size_t i=0; i<_ppDefinitions.size(); i++)
  {
    ppArray->push_back(new ConfigString(ToSTDString(_ppDefinitions[i])));
  }

  // Add the developer path
  ParamaFileConfig fileConfig;
  fileConfig.Add(new ConfigEntry("DeveloperPath",new ConfigString(ToSTDString(_developerPath))));
  fileConfig.Add(new ConfigEntry("PreprocessorDefinitions", ppArray));

  //save the window settings
#if _SD_DEBUG_LOG
  _debugOutputWindowSettings.Save("DebugOutputWindow",&fileConfig);
#endif
#if _SD_SCRIPT_LOG
  _scriptLogWindowSettings.Save("ScriptLogWindow",&fileConfig);
#endif
  _mainWindowSettings.Save("MainWindow",&fileConfig);
#if _VBS_14573_DEBUG_CONSOLE
  _debugConsoleWindowSettings.Save("DebugConsoleWindow",&fileConfig);
#endif

  // If there is any styles, then save them..
  for (int i=0; i<32; i++)
    if (_styles[i]) _styles[i]->SaveToConfig(&fileConfig);

  // Write the file out to the output stream.
  fileConfig.SaveFile(outputstream);
}

void StyleLoader::SaveToConfig(ParamaFileConfig *config)
{
  SmartRef<ConfigEntry> entry = new ConfigEntry(_className);
  ConfigClass *configClass = new ConfigClass(entry);

  // Create the config class
  configClass->Add(new ConfigEntry("FontName", new ConfigString(_fontName)));
  configClass->Add(new ConfigEntry("BackGroundColor", new ConfigString(_bkcolor)));
  configClass->Add(new ConfigEntry("ForeGroundColor", new ConfigString(_fgcolor)));
  float fontSize = (float)_fontSize;
  configClass->Add(new ConfigEntry("FontSize", new ConfigScalar(fontSize)));
  configClass->Add(new ConfigEntry("FontBold", new ConfigBool(_bold)));
  configClass->Add(new ConfigEntry("FontItalic", new ConfigBool(_italic)));
  configClass->Add(new ConfigEntry("FontUnderline", new ConfigBool(_underline)));

  // Add the class to the config file..
  entry->SetEntry(configClass);
  config->Add(entry);
}

// Load Param 
void StyleLoader::LoadParam(ConfigEntry *entry)
{
  Assert(entry);
  
  if (entry->IsClass())
  {
      ConfigClass *classEntry = entry->GetClass();
      
      _className = classEntry->GetName(); // Get the class name, (dont know how useful this is, can be removed)
      classEntry->GetVal("FontName",_fontName);
      classEntry->GetVal("BackGroundColor",_bkcolor);
      classEntry->GetVal("ForeGroundColor",_fgcolor);

      // Quick fix
      float fontSize = 0.0f;
      classEntry->GetVal("FontSize",fontSize);
      if (fontSize>0.0f) _fontSize = (int) fontSize;

      classEntry->GetVal("FontBold",_bold);
      classEntry->GetVal("FontItalic",_italic);
      classEntry->GetVal("FontUnderline",_underline);
  }
}

#if _SD_DEFAULT_SCI_STYLE
void StyleLoader::SetDefaults(int ST)
{
  _fontName = "Courier New";
  _fontSize = 10;
  _bkcolor = "white";
  switch (ST)
  {
    case SCE_C_DEFAULT:
    case SCE_C_OPERATOR:
    case SCE_C_IDENTIFIER:
      {
       _fgcolor = "black";
       break;
      }
    case SCE_C_COMMENT:
    case SCE_C_COMMENTLINE:
    case SCE_C_COMMENTDOC:
    case SCE_C_STRING:
      {
        _fgcolor = "rgb(0, 128, 0)";
        break;
      }
    case SCE_C_PREPROCESSOR:
      {
        _fgcolor = "light grey";
        break;
      }
    case SCE_C_NUMBER:
    case SCE_C_WORD: //VBS script functions
    case SCE_C_WORD2: //VBS script functions
    case SCE_C_GLOBALCLASS: //VBS script functions
      {
        _fgcolor = "blue";
        break;
      }
    default:
      _fgcolor = "black";
  }
}
#endif

StyleLoader *Settings::GetStyle(int id) const
{
  return _styles[id].GetRef(); 
}

StyleLoader *Settings::CreateStyle(int id)
{
  _styles[id] = new StyleLoader(id);
  return _styles[id].GetRef();
}

void Settings::SetStyleLoader(StyleLoader *style)
{
  // Share the same style ID, so just overwrite it
  _styles[style->_styleID] = style;
}

bool Settings::EnableScriptLog()
{
  return _enableScriptLog;
}

void Settings::SetEnableScriptLog(bool enable)
{
  _enableScriptLog = enable;
}

bool &Settings::EnableDebugging()
{
  return _enableDebugging;
}

void Settings::LoadConfigFile()
{
  // Open the config file
#if _SD_CONFIG_DOCUMENTS
  std::wstring path = GConfigPath;
  if (!path.length())
    return;
  path += L"\\";
#else
  std::wstring path = GUtil.GetExeFolderPath();
#endif
  path += std::wstring(GConfigFileName);
  ifstream file;
  file.open(path.data(),ifstream::binary);
  // Read in the config file
  if (file.is_open())
  {
    ParamaFileConfig param;
    param.LoadFile(file);

    // Load the settings with the config file
    LoadParam(&param);
  }
  else
  {
    SetDefaults();
  }
}


void Settings::SaveConfigFile()
{
  // Write out the data
#if _SD_CONFIG_DOCUMENTS
  std::wstring path = GConfigPath;
  if (!path.length())
    return;
  path += L"\\";
#else
  std::wstring path = GUtil.GetExeFolderPath();
#endif

#if _SD_EMPTY_PROFILE_FIX
  wxFileName::Mkdir(path.c_str(), 511, wxPATH_MKDIR_FULL);
#endif

  path += std::wstring(GConfigFileName);

  ofstream out;
  out.open(path.data(),std::ios_base::binary);
  if (!out.fail())
  {
    // Overwrite the current existing file.
    SaveToConfig(out);
  }
  else
  {
    WarningMessage("Failed to save:%s",path.data());
  }
}