#include <windows.h>

#include "ScriptDebuggerConfig.hpp"
#include "DebugScript.hpp"
#include "Document.hpp"
#include "DocumentManager.hpp"
#include "DocumentTabManager.hpp"

#include "Util.hpp"

void DebugScript::Init()
{
  _script = NULL;
  _break = false;
  _isRunning = false;
  
  _isStepping = false;
  _focusOnVBS = false;
  _stepType = SKOut;

  // Call stack is always assumed to be the latest.
  _callStackIndex = INT_MAX;
#if _VBS3_DEBUG_DELAYCALL
  _shouldBreak = false;
  _delayCall = NULL;
  _type = DS_SCRIPT;
#endif
#if _VBS3_SCRIPT_LINES
  _currInstrSet = false;
  _lineNum = -1;
  _lineNumFilePath = L"";
#endif
}

DebugScript::DebugScript()
{
  Init();
}
DebugScript::DebugScript(IDebugScript *script)
{
  Init();
  _script=script;
}

DebugScript::~DebugScript()
{
  if (_prevDoc) _prevDoc->SetLineNumber(-1);
}

// Two things can happen with a script...
// First a break point can fire
// Or it is entered
// 
// it can exit the followings ways
// 1) It finishes it execution (easy)
// 2) Is execution is paused!
//
// The easiest is, a call to tell it its lost focus!
void DebugScript::GainFocus()
{
  
}

void DebugScript::LostFocus()
{
  if (_prevDoc) _prevDoc->SetLineNumber(-1);
}



wxString GetCallStackItemFilePath(ICallStackItem *callStack)
{
  char buffer[MAX_PATH] = {0};
  int maxPath = MAX_PATH;
  callStack->GetFilePath(buffer,maxPath);

  return CharToUTF16(buffer);
}

// Update the script.


// The preprocessor includes the original file name
// this is added to the return string as the first line
// as  #line 1 "vbs2\weapons\data\scripts\fakeToRealCrater.sqf"
// 
// Simply find the first line, then search for where the expression
// original came from
std::string GetPPFilePath(const char *buffer)
{
  // find the new line feed.
  const char *ptr = buffer;
  while (*ptr)
  {
    if (*ptr=='\r'||*ptr=='\n') break;
    ptr++;
  }
  // Get the first line
  std::string firstLine(buffer,ptr-buffer);
  
  int magicIdentfier = firstLine.find("#line");
  if (magicIdentfier==-1) return std::string(); // No magic identifier

  // Get the string between the quotation marks
  int fromChar =firstLine.find('"');
  int toChar = firstLine.rfind('"');
  fromChar++;
  toChar;
  if (fromChar>=0&&toChar>fromChar)
    return firstLine.substr(fromChar,toChar-fromChar);

  return std::string();
}

// Tell it to use the top level call stack, index
void DebugScript::SetTopLevelCallStack()
{
  _callStackIndex = INT_MAX;
}

void ResolvePPFile::ProccessPreProccessorContent(std::string &content)
{
  _ppLineEntry.clear();

  const char *startPtr = &content[0];
  const char *endPtr  = startPtr+content.length();
  
  typedef std::pair<int,std::string> LineNumStringPair;
  std::vector< LineNumStringPair > findDeliminator;

  int lineNumber = 1;
  while (startPtr<endPtr)
  {
    if (*startPtr=='\n')
    {
      lineNumber++;
      startPtr++;
      continue;
    }
        
    if (*startPtr=='#')
    {
      const char *tmp = startPtr;
      while (tmp<endPtr&&*tmp!='\n') tmp++;

      findDeliminator.push_back(LineNumStringPair(lineNumber,std::string(startPtr,tmp)));

      // Move the start pointer forward!
      startPtr = tmp;
      continue;
    }
    startPtr++;
  }

  static const char *prefix = "#line";
  static const int prefixLen = strlen(prefix);
  for (size_t i=0; i<findDeliminator.size(); i++)
  {
      int delLineNumber = findDeliminator[i].first;
      const char *start = findDeliminator[i].second.data();

      if (strncmp(start, prefix, prefixLen) != 0) continue;
      const char *ptr = start + prefixLen;

      if (!isspace(*ptr)) continue;
      while (isspace(*ptr)) ptr++;

      int sourceLine = 0;
      if (!isdigit(*ptr)) continue;
      while (isdigit(*ptr))
      {
        sourceLine *= 10;
        sourceLine += *ptr - '0';
        ptr++;
      }
      
      std::string filePath = GetPPFilePath(start);
      if (filePath.length()) _ppLineEntry.push_back(FileLineEntry(delLineNumber,sourceLine,filePath));
  }
}

//! Convert the source line, to its absolute position!
int ResolvePPFile::ConvertSoureLineToAbs(int sourceLine,std::string &path)
{
  FileLineEntry *entry = NULL;
  for (size_t i=0; i<_ppLineEntry.size(); i++)
  {
    if (_ppLineEntry[i]._filePath==path)
    {
      // If source line is less than the entry, then break
      if (entry && _ppLineEntry[i]._lineOffset>sourceLine) break;
      entry = &_ppLineEntry[i];
    }
  }

  if (entry) return entry->_lineNum+(sourceLine-entry->_lineOffset)+1;
  return -1;
}

//! Convert the absolute position to the original file
//! -1 - out of rang, not there yet!
int ResolvePPFile::ConvertAbsToSourceLine(int absSourceLine)
{
  FileLineEntry *entry = NULL;
  for (size_t i=0; i<_ppLineEntry.size(); i++)
    if (_ppLineEntry[i]._lineNum<absSourceLine) entry = &_ppLineEntry[i];

  if (entry) return(absSourceLine-entry->_lineNum)+(entry->_lineOffset-1);
  return -1;
}

//! Get the file the absolute line is from!
std::string ResolvePPFile::GetFileFromAbs(int absSourceLine)
{
  FileLineEntry *entry = NULL;
  for (size_t i=0; i<_ppLineEntry.size(); i++)
    if (_ppLineEntry[i]._lineNum<absSourceLine) entry = &_ppLineEntry[i];

  if (entry) return entry->_filePath;

  return std::string();
}

void CallStackBuffer::Update(ICallStackItem *callStack)
{
  //if (callStack!=_callStack)
  {
    _callStack = callStack;

    // Read in the source code from the call stack
    char tmp[] = "";
    int size = 0;
    _callStack->GetSourceCode(tmp,size);

    _ppContent.resize(size);
    _callStack->GetSourceCode(&_ppContent[0],size);

    _resolvePPFile.ProccessPreProccessorContent(_ppContent);
  }
}

DocumentViewer* DebugScript::OpenDocForLine(int absoluteLineNum)
{
  if (absoluteLineNum < 0) return GDocumentManager->GetVirtualMachineDoc();
  wxString wxFilePath = CharToUTF16(_resolvePP._resolvePPFile.GetFileFromAbs(absoluteLineNum));
  int sourceLineNum = _resolvePP._resolvePPFile.ConvertAbsToSourceLine(absoluteLineNum);
  
  DocumentViewer * virtualDoc = GDocumentManager->GetDocument(wxFilePath);
  if (virtualDoc->GetDocContents()&DOC_VM)  //If not opened yet
  {
    virtualDoc = GDocumentManager->OpenEngineFile(wxFilePath);
    virtualDoc->SetDocView(DOC_ENGINE);
    virtualDoc->SetLineNumber(sourceLineNum);
    virtualDoc->SetLineNumber(); //Discard arrows, we need just scroll
  }
  return virtualDoc;
}

DocumentViewer* DebugScript::OpenCompleteHiearchy(int absoluteLineNum)
{
   if (absoluteLineNum < 0) return GDocumentManager->GetVirtualMachineDoc();

  //First create complete hiearchy of all included files, down to the file, the script is currently in
  std::vector<const FileLineEntry*> hiearchy; //Expects non nulls
  std::string fileExecutingPath = _resolvePP._resolvePPFile.GetFileFromAbs(absoluteLineNum);
  const std::vector<FileLineEntry> & PPFiles = _resolvePP._resolvePPFile._ppLineEntry;
  for (int i = 0; i < PPFiles.size(); i++)
  {
    if (PPFiles[i]._lineNum > absoluteLineNum) break;
    const std::string & filePath = PPFiles[i]._filePath;
    hiearchy.push_back(&PPFiles[i]);
    for (int j = hiearchy.size()-2; j>=0; j--)
      if (hiearchy[j]->_filePath==filePath)
      {
#if _VBS3_SCRIPT_LINES
        hiearchy.erase(hiearchy.begin() + hiearchy.size()-1, hiearchy.end());//delete the last one (duplicate)
#else
        hiearchy.erase(hiearchy.begin()+j+1, hiearchy.end());
#endif
        break;
      }
  }

  //Second open all of them and return the last (which corresponds to absoluteLineNum) 
  DocumentViewer *virtualDoc = GDocumentManager->GetVirtualMachineDoc();
  for (int i = 0; i < hiearchy.size(); i++)
  {
    int lineNum = (i < hiearchy.size()-1)? hiearchy[i+1]->_lineNum: absoluteLineNum;
    virtualDoc = OpenDocForLine(lineNum);
  }
  return virtualDoc;
}

/*
  We have a couple situations that update occurs

  1) When a break point is hit
  2) Second, when stepping through the call stack
  3) Resolving files
       1) Check if the set calls tack has a filepath
       2) If document is all-ready open, set the line number
       3) if no document is open, and we have a path it exists inside the engine! Get the DocumentManager to open it for us!
       4) Otherwise, only display content in the Virtual Machine!
*/
void DebugScript::Update()
{
  // need to decide on the script
#if _VBS3_DEBUG_DELAYCALL
  if (_type == DS_DELAYCALL)
  {
    DocumentViewer *virtualDoc = GDocumentManager->GetDelayCallDoc();
    //DELAY call, display the code from the delay call
    std::string code(_delayCall->GetIDebugDelayCall()->GetStringCode());
    virtualDoc->SetDocViewContent(DOC_DELAYED,code);
    virtualDoc->SetDocView(DOC_DELAYED);
    PrepareAndShowTab(virtualDoc,-1);
    return;
  }
  else if (!_script)
  {
    return;
  }
#else
  if (!_script) return;
#endif

  // There is a situation, that there is NO call-stack, sqs files
  // for now we ignore SQS files...
  int stackSize = _script->CallStackSize();
  if (stackSize<=0) return;

  // All-ways consider the top level....
  if (_callStackIndex>=stackSize) _callStackIndex = stackSize-1;
  int selectedCallStackIndex = _callStackIndex;


#if _VBS3_SCRIPT_LINES
  /*
    The engine now works with relative line numbers which it passes in in the Breaked(IDebugScript *script, const char * filename, int line)
    function.

    The current line number & file name of the instruction is stored here in _lineNum & _lineNumFilePath
  */
  wxString filePath = _lineNumFilePath;
  bool filePathUpdated = false;
  int lineNum = _lineNum;

  //First find the appropriate callstackItem to retrieve data from
  ICallStackItem *parentCSItem = NULL; //callstack item to get lineNumber and filepath from
  ICallStackItem * PPContentCSItem = NULL; //callstack item to get PPFile from
  for (int callStackLevel=0; callStackLevel<stackSize; callStackLevel++)
  {
    ICallStackItem *callStackItem = _script->GetCallStackItem(callStackLevel);
    
    //We're looking for the most top-level csItem with a valid filePath
    //We however don't want a non-inline item above the selected CS level
    if (!parentCSItem)
    {
      parentCSItem = callStackItem;
      PPContentCSItem = parentCSItem;
    }
    if (callStackItem->IsInline() || callStackLevel <= selectedCallStackIndex)
    {
      wxString tmpFilePath(L"");
      char filePathBuf[256];
      int size = 256;
      //go through
      //callstack items up to the highest non-inline one under selected level
      if (callStackItem->GetFilePath(&filePathBuf[0],size))
      {
        tmpFilePath = wxString::FromAscii(&filePathBuf[0]);
        //if the callstack level has a valid filePath, use it
        //The last part of the condition is for stepping into call { ... code ... } statements, they have no filepath, but valid code PP content!
        if (tmpFilePath.Cmp(L"NoFilePath") != 0 && (tmpFilePath.Length() || (_currInstrSet && !_lineNumFilePath.Length())))
        {
          if (!_currInstrSet) //means the update was called either from callstack or clicking on VM tab
          {
            filePath = tmpFilePath;
            filePathUpdated = true;
            lineNum = -1;
            parentCSItem = callStackItem;
          }
          else if (tmpFilePath.Cmp(_lineNumFilePath) == 0) //for breaked events, we actually want the last callstack level where the filePaths match
            parentCSItem = callStackItem;
          if (!callStackItem->IsInline())
          {
            PPContentCSItem = callStackItem;
          }
        }
      }
      if (selectedCallStackIndex == 0 && !filePathUpdated && !_currInstrSet) //we're looking at the top-most callstack level - that's usually VM content - if we weren't able to get the filepath in previous step, display that
      {
        filePathUpdated = true;
        filePath = L"";
      }
    }
    else
    {
      break;
    }
  }
  _resolvePP.Update(PPContentCSItem); //update the PP'd file from the callstack item contents

  //open the file + the include hierarchy + call stack hierarchy (if applicable)
  DocumentViewer *docViewer = NULL;
  //We may have changed files
  if (filePathUpdated)
  {
    if (_callStackIndex == (stackSize-1))
    {
      _scriptFilePath = filePath;
      _lineNumFilePath = _scriptFilePath;
      _lineNum = -1;
    }
  }
  //only get current instruction number if we're breaked
  if (IsBreaked() && lineNum == -1)
  {
    parentCSItem->GetDocumentPos(lineNum);

	//if we're on the last callstack level, update the actual instruction line
    if (_callStackIndex == (stackSize-1))
    {
      _lineNum = lineNum;
    }
  }

  if (filePath.Length()) // means this is an actual file
  {
    //Open the include hierarchy - if the file was breaked in
    bool opened = false;
    if (_resolvePP._resolvePPFile._ppLineEntry.size() && (lineNum > 0))
    {
      //Needs abs line for opening the hierarchy
      int absLine = _resolvePP._resolvePPFile.ConvertSoureLineToAbs(lineNum,std::string(filePath.ToAscii()));
      if (absLine > -1) //-1 happens when running a function (code in a variable) defined in an include file
      {
        docViewer = OpenCompleteHiearchy(absLine);
        opened = true;
      }
    }
    if (!opened)
    {
      //try to retrieve document
      docViewer = GDocumentManager->GetDocument(filePath);
      if (docViewer->GetDocContents()&DOC_ORIGINAL) //file already open
      {
        if (!(docViewer->GetDocView()&DOC_ORIGINAL))
          docViewer->SetDocView(DOC_ORIGINAL);
      }
      else if (docViewer->GetDocContents()&DOC_ENGINE) //file open and in ENGINE view 
      {
        if (!(docViewer->GetDocView()&DOC_ENGINE))
          docViewer->SetDocView(DOC_ENGINE);
      }
      else if (docViewer->GetDocContents()&DOC_VM) //means we got just the VM doc -> the _lineNumFilePath doc is not open
      {
        docViewer = GDocumentManager->OpenEngineFile(filePath); //open the engine file contents
        if (!(docViewer->GetDocView()&DOC_ENGINE))
          docViewer->SetDocView(DOC_ENGINE);
      }
    }
  }
  else
  {
    //VM code
    // If all fails display, the raw VM content...
    docViewer = GDocumentManager->GetVirtualMachineDoc();
    docViewer->SetDocViewContent(DOC_VM,_resolvePP._ppContent);
    docViewer->SetDocView(DOC_VM);
  }
  _currInstrSet = false;

  //Bring the tab into focus and display line marker
  PrepareAndShowTab(docViewer,lineNum);
#else
   /*
  Line number convention.
  It is assumed all code is inline by default only 'compile' statements  are not in-lined

  Examples of in-lined
  {
  #include "myFile"
  }

  Examples of non in-lined
  [] call compile preprocessFile

  Change to the system. All callstackItems line is now relative to its parent. The 
  only exception to this, is non-lined call stack items. non-inline call stack items
  reset their line number to zero. Any call after follow the same procedure. For
  example, a non inline function reset the line number to zero, but any other calls
  follow the same procedure.
  */
  int lineNum = 0;
  ICallStackItem *parentCS = NULL;
  for (int callStackLevel=0; callStackLevel<stackSize; callStackLevel++)
  {
    ICallStackItem *callStack = _script->GetCallStackItem(callStackLevel);

    // Only display the parent code stack bellow the display index.
    if (!callStack->IsInline()&& callStackLevel>selectedCallStackIndex) break;
   
    // New parent code stack found, reset the line number
    // and the parentCS, to the non inline cs.
    if (!callStack->IsInline()||!parentCS)
    {
      parentCS = callStack;
    }

    // sleep statements and other commands have no line number.
    // so we need to go from the previous parent, and wait until the execution of them
    // have finished... eg.. sleep!
    int tmpLine;
    callStack->GetDocumentPos(tmpLine);
    if (tmpLine!=0) lineNum = tmpLine;
  }
  Assert(lineNum!=0);

  // Resolve the parent code stack.
  _resolvePP.Update(parentCS);

  // (Most) scripts first line refer to the file path.
  if (_resolvePP._resolvePPFile._ppLineEntry.size())
    _scriptFilePath = CharToUTF16(_resolvePP._resolvePPFile._ppLineEntry[0]._filePath);


  // Only display the line number relative to the parent
  // 
  if (parentCS)
  {
    // First, check the file path from the engine.. sometimes they do not exists
    // situations are 'spawn' commands!
    wxString parentCSFilePath = GetCallStackItemFilePath(parentCS);

    DocumentViewer *virtualDoc = GDocumentManager->GetVirtualMachineDoc();
    if (parentCSFilePath.Length() && parentCSFilePath.CompareTo(L"NoFilePath")!=0)
    {
      // Try to get the document.
      virtualDoc = GDocumentManager->GetDocument(parentCSFilePath);
      if (virtualDoc->GetDocContents()&DOC_VM)
      {
        // Path has not been opened, open the engine file and update the tab
        virtualDoc = GDocumentManager->OpenEngineFile(parentCSFilePath);
        virtualDoc->SetDocView(DOC_ENGINE);
      }
    }

    // Resolve all the file paths from the call stack!
    // if the virtualDoc has been resolved. Then skip this section.
    if (_resolvePP._resolvePPFile._ppLineEntry.size())
    {
      Assert(lineNum!=-1);
      virtualDoc = OpenCompleteHiearchy(lineNum); 
      _lineNumFilePath = CharToUTF16(_resolvePP._resolvePPFile.GetFileFromAbs(lineNum));
      lineNum = _resolvePP._resolvePPFile.ConvertAbsToSourceLine(lineNum);
    }
    else if (!_resolvePP._resolvePPFile._ppLineEntry.size() && (virtualDoc->GetDocContents() & (DOC_ENGINE|DOC_ENGINE_PP))   )
    {
      // For inline compiled script game code, that is spawned with
      // [] spawn { // do stuff } over multiple lines. This is executed within
      // the original file, but no call stack information
      // 
      // Assumption is the document is all-ready open
      // I get the engine PP file, then parse it through the same
      // process to get the line number...
      lineNum = virtualDoc->GetEngineResolvedPPFile().ConvertAbsToSourceLine(lineNum);
      
      // For Gamecode that is executed using spawn will not have the PP file path resolved
      // the only path we can use it from is the doc that was resolved when getting the
      // GetCallStackItemFilePath from the engine. Both the line number and script file
      // path are the same in this instance.
      _lineNumFilePath = CharToUTF16(virtualDoc->GetEngineResolvedPPFile().GetFileFromAbs(lineNum));
      _scriptFilePath = _lineNumFilePath;

      Assert(lineNum!=-1);
    }
    else
    {
      // If all fails display, the raw content...
      virtualDoc->SetDocViewContent(DOC_VM,_resolvePP._ppContent);
      virtualDoc->SetDocView(DOC_VM);
    }
    PrepareAndShowTab(virtualDoc,lineNum);
  }
#endif
}

void DebugScript::PrepareAndShowTab(DocumentViewer * virtualDoc, int lineNum)
{
  // Set the line number...
  virtualDoc->SetLineNumber();
  // Update all relevant markers in the document view
  virtualDoc->SetLineNumber(lineNum,_isStepping,_stepType);

  // Set the tab to be red, to indicate stopped
  wxColour selectedColour;
  selectedColour.Set(255,0,0,0);
  //GDocumentTabManager->SetTabColor(virtualDoc,selectedColour);


  // Remove any, line number indication in the previous documents
  if (_prevDoc && _prevDoc.GetRef()!=virtualDoc) 
  {
    _prevDoc->SetLineNumber(-1);
    //GDocumentTabManager->ResetTabColor(_prevDoc);
  }

  // Update the document.
  _prevDoc = virtualDoc;
  _curDoc =  virtualDoc;


  GDocumentTabManager->BringIntoFocus(virtualDoc);
}

// Set the state of the debug script if its breaked or not
bool DebugScript::IsBreaked() const
{
  return _break;
}
void DebugScript::SetBreak(bool value)
{
  _break = value;
#if _VBS3_SCRIPT_LINES
  _lineNum = -1;
#endif
}

bool DebugScript::IsRunning() const
{
  return _isRunning;
}
void DebugScript::SetRunning(bool value)
{
  _isRunning = value;
}

void DebugScript::SetStepping(bool value)
{
  _isStepping = value;
}

bool DebugScript::IsStepping() const
{
  return _isStepping;
}

void DebugScript::SetStepType(StepKind value)
{
  _stepType = value;
}

bool DebugScript::GetVBSFocusToggle() const
{
  return _focusOnVBS;
}

void DebugScript::SetVBSFocusToggle(bool value)
{
  _focusOnVBS = value;
}

const wxString &DebugScript::GetScriptFilePath() const
{
  return _scriptFilePath;
}

const wxString &DebugScript::GetCallStackFilePath() const
{
  return _lineNumFilePath;
}

DocumentViewer *DebugScript::GetDisplayDoc() const
{
  return _curDoc;
}

IDebugScript * DebugScript::GetIDebugScript() const
{
  return _script;
}

void DebugScript::SetIDebugScript( IDebugScript *script )
{
  _script = script;
#if _VBS3_DEBUG_DELAYCALL
  //when we have IDebugScript, the delayCall code was executed
  if (_type == DS_DELAYCALL)
  {
    _type = DS_SCRIPT;
    _delayCall = NULL; //SmartRef should take care of cleaning up
  }
#endif
}

int DebugScript::GetCallStackIndex() const
{
  return _callStackIndex;
}

void DebugScript::SetCallStackIndex( int index )
{
  _callStackIndex = index;
}

#if _VBS3_SCRIPT_LINES
void DebugScript::SetCurrInstr(const char * path, int line)
{
  _lineNum = line;
  _lineNumFilePath = CharToUTF16(path);
  _currInstrSet = true;
}
#endif

#if _VBS3_DEBUG_DELAYCALL
void DebugScript::SetDelayCall(DebugDelayCall *delayCall)
{
  _delayCall = delayCall;
  _type = DS_DELAYCALL;
}

DebugDelayCall *DebugScript::GetDelayCall() const
{
  return _delayCall;
}

DebugDelayCall::DebugDelayCall(IDebugDelayCall * delayCall)
{
  _delayCall = delayCall;
  _name = GUtil.ConvertToRelativePath(wxString::FromUTF8(delayCall->GetFilename()));
  _id = delayCall->GetId();
}

const wxString& DebugDelayCall::GetName() const
{
  return _name;
}

const float DebugDelayCall::GetDelay() const
{
  if (_delayCall)
    return _delayCall->GetWaitTime();
  return 0;
}

unsigned int DebugDelayCall::GetId() const
{
  return _id;
}

void DebugDelayCall::SetCodeExecuted()
{
  _delayCall = NULL;
}

#endif