#pragma once

#include <vector>
#include "wx\wx.h"
#include "wx/artprov.h"
#include "wx\gdicmn.h"
#include "wx\msw\imaglist.h"

#define INVALID_INDEX -1

struct SyncItem
{
  int _idc;
  int _imgIndex;
};

class Index
{
  int _index;
public:
  Index()
  {
    _index = -1;
  }
  Index(int index)
  {
    _index = index;
  }
   operator int()
  {
    return _index;
  }
   operator size_t()
  {
    return (size_t)_index;
  }
   operator bool()
  {
    return _index>=0;
  }
};

/**
  * GImage manager is used to load images within the context of wxImageList
  */
class ImgManager
{
  wxImageList* _imgList;
  std::vector<SyncItem> _syncList;

public:
  ImgManager();
  ~ImgManager();

  /// Get the global ImgManger image list
  wxImageList *GetImageList();
  /// LoadIdcIntoImageList, assumes BitMap because of native
  /// windows API with BMP's
  Index LoadIdcIntoImageList(int idc,wxBitmapType type = wxBITMAP_TYPE_BMP);
  
  /// WxImage is assumed to be PNG format
  bool GetWxImage(int idc, wxImage &image, wxBitmapType type = wxBITMAP_TYPE_PNG);
  /// WxBitMap is assumed to be BMP format
  bool GetWxBitMap(int idc, wxBitmap &bmp, wxBitmapType type = wxBITMAP_TYPE_BMP);
  /// WxIcon is assumed to be ICO format
  bool GetWxIcon(int idc, wxIcon &icon, wxBitmapType type = wxBITMAP_TYPE_ICO);
};

extern ImgManager GImgManager;