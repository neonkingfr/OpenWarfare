#include "Document.hpp"
#include "DebugScript.hpp"
#include "DeveloperDriveTree.hpp"
#include "DocumentTabManager.hpp"
#include "DocumentManager.hpp"
#include "BreakpointManager.hpp"

#include "../../Common/Essential/AppFrameWork.hpp"
#include "settings.hpp"

#include "MainWindow.hpp"

#include "../../Common/Essential/crc/crc.hpp"
#include "../../Common/Essential/Suffix/SuffixArray.hpp"
#include "../../Common/Essential/ForEach.hpp"
#include "Util.hpp"

#include <wx/tokenzr.h>

#include "XMLParser.hpp"
#include <algorithm>

#include "UIEvents.hpp"
#include <ctype.h>

#if _VBS3_SD_IMP_BREAKPOINTS
extern ScriptBreakpointsManager GBreakpointManager;
#endif

BEGIN_EVENT_TABLE(wxScintillaExt,wxScintilla)
EVT_KEY_DOWN(wxScintillaExt::OnKeyDown)
EVT_COMMAND(wxID_ANY, SearchWindowEvent, wxScintillaExt::OnSearchWindowEvent)
EVT_SET_FOCUS(wxScintillaExt::OnGainFocus)
END_EVENT_TABLE()

wxScintillaExt::wxScintillaExt(wxWindow *parent)
    : wxScintilla(parent)
{
  _startingSearchPos = -1;
  _prevSearchString = wxEmptyString;
  _prevSearchMods = -1;
};

#define WXK_F 0x46

void wxScintillaExt::OnKeyDown(wxKeyEvent &evt)
{
  if (evt.GetModifiers() == wxMOD_CONTROL)
  {
    switch ( evt.GetKeyCode() )
    {
    case (WXK_F):
      {
        GFrame->OnSearchWindow(this);
        evt.Skip(false);
        return;
      }
    default:
      {}
    }
  }
  evt.Skip();
}

//from wxScintilla implementation (editor.cxx)
#define SCFIND_MATCHCASE 4
#define SCFIND_WHOLEWORD 2

void wxScintillaExt::OnSearchWindowEvent(wxCommandEvent &evt)
{
  switch (evt.GetId())
  {
  case SearchDiagFindNext:
    {
      int selStart = GetSelectionStart();
      int selEnd = GetSelectionEnd();
      bool searchChanged = false;
      SetSelectionStart(GetSelectionEnd()); //clear selection and move the caret to end of it
      SearchAnchor(); //caret as the start point for search

      //search window must exist because it sent the event
      wxString& searchString = GFrame->GetSearchWindow()->SearchString();
      //get the search modifiers from the window 
      int searchMods = 0;
      if (GFrame->GetSearchWindow()->MatchCase())
      {
        searchMods = searchMods | SCFIND_MATCHCASE;
        if (searchString.Cmp(_prevSearchString) != 0)
        {
          searchChanged = true;
          _prevSearchString = searchString;
        }
      }
      else
      {
        if (searchString.CmpNoCase(_prevSearchString) != 0)
        {
          searchChanged = true;
          _prevSearchString = searchString;
        }
      }
      if (GFrame->GetSearchWindow()->WholeWordOnly())
      {
        searchMods = searchMods | SCFIND_WHOLEWORD;
      }
      if (searchMods != _prevSearchMods)
      {
        searchChanged = true;
        _prevSearchMods = searchMods;
      }

      int pos = SearchNext(searchMods, searchString); //the actual search
      if (pos == -1) //not found, try again from start of document
      {
        SetSelectionStart(0);
        SearchAnchor();
        pos = SearchNext(searchMods, searchString);
      }
      if (pos != -1 && (pos != selStart) && (selEnd != (pos + searchString.length()))) //new result
      {
        GotoPos(pos + searchString.length()); //move the caret to search pos end
        SetSelection(pos,pos + searchString.length()); //highlight the result
        if (pos == _startingSearchPos) //we have reached the starting search pos
        {
          wxMessageDialog message(GFrame->GetSearchWindow(),wxT("The starting point of search has been reached."),wxT("Search result"),wxOK|wxICON_INFORMATION);
          message.ShowModal();
          _startingSearchPos = -1;
        }
        else if (_startingSearchPos == -1 || searchChanged)
        {
          //this is the first result with these search parameters, store it's position as the end point for the search next cycle
          _startingSearchPos = pos; 
        }
      }
      else //no occurences in file at all, notify the user
      {
        SetSelection(selStart,selEnd); //keep the old selection
        wxMessageDialog message(GFrame->GetSearchWindow(),wxString::Format(wxT("The string \'%s\' was not found in the current file."),searchString),wxT("Search result"),wxOK|wxICON_INFORMATION);
        message.ShowModal();
      }
#if !_SD_UI_IMPROVEMENTS
      this->SetFocus(); //set focus on the scintilla
#endif
      break;
    }
  case SearchDiagClose:
    {
      _startingSearchPos = -1;
      _prevSearchString = wxEmptyString;
      _prevSearchMods = -1;
      break;
    }
  default:
    {}
  }
}

void wxScintillaExt::OnGainFocus(wxFocusEvent &evt)
{
  GDocumentManager->CheckForFileChanges();
  evt.Skip();
}

BEGIN_EVENT_TABLE(DocumentViewer, wxEvtHandler)

EVT_SCI_MARGINCLICK(wxID_ANY, DocumentViewer::OnMarginClick )
EVT_SCI_CHARADDED(wxID_ANY,DocumentViewer::OnScintillaCharAdded)
EVT_SCI_UPDATEUI(wxID_ANY,DocumentViewer::OnScintillaUpdatedUI)
EVT_SCI_MODIFIED(wxID_ANY,DocumentViewer::OnScintillaModified)
EVT_SCI_AUTOCOMP_SELECTION(wxID_ANY,DocumentViewer::OnAutoCompleteSelected)

EVT_MOTION(DocumentViewer::OnMouseWindowMove)
EVT_ENTER_WINDOW(DocumentViewer::OnMouseWindowEnter)
EVT_LEAVE_WINDOW(DocumentViewer::OnMouseWindowExit)

EVT_MOUSEWHEEL(DocumentViewer::OnAnyEvent)
EVT_KILL_FOCUS(DocumentViewer::OnAnyEvent)

#define EVT_SCROLLWIN(func) \
    EVT_SCROLLWIN_TOP(func) \
    EVT_SCROLLWIN_BOTTOM(func) \
    EVT_SCROLLWIN_LINEUP(func) \
    EVT_SCROLLWIN_LINEDOWN(func) \
    EVT_SCROLLWIN_PAGEUP(func) \
    EVT_SCROLLWIN_PAGEDOWN(func) \
    EVT_SCROLLWIN_THUMBTRACK(func) \
    EVT_SCROLLWIN_THUMBRELEASE(func)

EVT_SCROLLWIN(DocumentViewer::OnAnyEvent)
EVT_SIZE(DocumentViewer::OnAnyEvent)

END_EVENT_TABLE()

#include <wx/popupwin.h>
struct CodeHinting : public RefCount
{
  //wxSashWindow *_slashWindow;
  wxPopupWindow *_frame;
  wxScintilla *_text;

  CodeHinting()
  {
      // Simple window
      _frame = new wxPopupWindow(GFrame,wxBORDER_SIMPLE);

      // Create the text editor
      //////////////////////////////////////////////////////////////////////////
      _text = new wxScintilla(_frame);
      int _lexerID = _text->GetLexer();
      long exstyle = GetWindowLong((HWND)_text->GetHWND(), GWL_EXSTYLE);
      exstyle &= ~WS_EX_CLIENTEDGE;
      SetWindowLong((HWND)_text->GetHWND(), GWL_EXSTYLE, exstyle);
    
      // R:255 G:255 B:225
      _text->StyleSetBackground(wxSCI_STYLE_DEFAULT,wxColour(255,255,225));
      _text->SetWrapMode(true);

      // Make all styles, the same as the default.
      _text->StyleClearAll();

      /// Margins are ordered: Line Numbers, Selection Margin, Spacing Margin
      for (int i =0; i<5; i++)
      _text->SetMarginWidth(i,0);
      //////////////////////////////////////////////////////////////////////////

      wxBoxSizer *frameSizer = new wxBoxSizer(wxVERTICAL);   
      frameSizer->Add(_text, 1, wxEXPAND, 5 );
      
      _frame->SetSizer(frameSizer); // always last   
      _frame->SetSize(200,100);
      _frame->Layout();
      _frame->Show(false);
  }

  wxWindow *MainWindow(){ return _frame; }
  
  void Show(bool show =true)
  {
    // Clear the text box, and show the dialog.
    _text->ClearAll();
    _frame->Show(show);
  }

  virtual ~CodeHinting()
  {
  }
};

SmartRef<CodeHinting> GCodeHinting;
bool IsCodeHintingBox()
{
  return GCodeHinting.GetRef()>0;
}
CodeHinting *GetCodeHinting()
{
  if (!GCodeHinting) GCodeHinting = new CodeHinting();
  return GCodeHinting;
}



unsigned int DocumentViewer::GetDocContents()
{
  return _docContent;
}


/*
Documentation on encodings, code pages and code points.

To make things simply a code page simply is the mapping between the binary
value to the character. There are different code pages for 1byte, 2btyes then there
is Unicode, that is one has only one code page, and represent the 

For 1 byte representation you have the following encodings:

ASCII (7 bit, but stored as 1 byte in modern machines. Anything above 0x7f
requires a correct code page for rendering)

ACP - Windows to refer to various code pages considered as native: Very close to ISO-8859-15, but is not considered a ISO standard
Code Page 1250 Windows Latin 2 (Central Europe)
Code Page 1251 Windows Cyrillic (Slavic)
Code Page 1252 Windows Latin 1
Code Page 1253 Windows Greek
Code Page 1254 Windows Latin 5 (Turkish)
Code Page 1255 Windows Hebrew
Code Page 1256 Windows Arabic
Code Page 1257 Windows Baltic Rim

ANSI: (1 byte, the remaining data from 127 where used by different country and have the following
ISO standards standards and representation.)
ISO-8859-1
ISO-8859-2
ISO-8859-4
ISO-8859-9
ISO-8859-13
ISO-8859-15

Unicode is an effort to include all characters from previous code pages into a single 
character enumeration that can be used with a number of encoding schemes.

Unicode Encoding schemes.
UCS-2,UCS-4, and UTF-8, UTF-16

As a rule:
UTF-8, is considered to be valid ASCII (not extended) because of the mapping
between 0 and 0x7f being lower ASCII.
*/
void DocumentViewer::SaveDocument(wxString filePath)
{
  _dirty = false; // Document written
  GDocumentTabManager->SetTabName(this);

  // Get the scintilla documents,,, note (engine files, or PP files have their \r removed)
  // only \n
  wxString docContents = _doc->GetText();
  Encoding docEncoding = CP_Default;
  if (_docView&DOC_ORIGINAL)
  {
    // For the original document, set the open file encoding
    // to be what the user selected, even if its changed.
    docEncoding = _rawFileEncoding;
  }
  else
  {
    // For now save all engine documents as UTF-8
    docEncoding = CCP_UTF8;

#if !_SD_ENGINE_FILE_NOSAVE //why was this in there??
    // Engine files are read only, so this quick
    // fix will do the job.
    docContents.Replace(L"\n",L"\r\n");
#endif
  }

  // If the original file was UCS2 write out the contents
  // directly, and save the BOM for UCS-2
  if (docEncoding==CCP_UCS2LE || docEncoding==CCP_UTF16)
  {
    // write out the Unicode directly.
    ofstream output;
    output.open (filePath.data(),std::ios_base::binary);
    if (!output.fail())
    {
      output.put((unsigned char)0xff);
      output.put((unsigned char)0xfe);
      output.write((char*)docContents.data(), docContents.length()*2);

      int byteLen = docContents.length()*2;
      _rawFileContents.resize(byteLen+2); // +2 Null terminator
      _rawFileContents[byteLen] = 0;
      _rawFileContents[byteLen+1] = 0;
      memcpy(&_rawFileContents[0],&docContents[0],docContents.length()*2);
    }
  }
  else
  {
    // Create the file, ready to write data out to it.
    ofstream output;
    output.open (filePath.data(),std::ios_base::binary);
    if (!output.fail())
    {
      // Convert the UTF-16 document to the specified open file encoding, or current encoding
      std::string dst = UTF16ToChar(docContents,docEncoding);
      if (dst.size()) output.write(&dst[0], dst.size());

      // Save the data to the _rawFile
      _rawFileContents.resize(dst.size()+1); // +1 Null terminator
      _rawFileContents[dst.size()] = 0;
      memcpy(&_rawFileContents[0],&dst[0],dst.size());
    }
  }

#if _SD_LIVEFOLDER_IMP
  ApplyBPUpdates();
#endif
  CreateCRCForRawContent();
  //update the last modified time
  GetLastDiskWriteTime(_lastWriteTime);
}


void DocumentViewer::CreateCRCForRawContent()
{
  // No engine file to compare against
  if (!_engineFileContent.size()) return;

  wxString docContents = _doc->GetText();
  int codePage = _engineFileEncoding ==EN_SBCS ? CP_Default : CCP_UTF8;
  std::string text = UTF16ToChar(docContents,codePage);

  CRCCalculator crcRawFile;
  _rawFileCRC = crcRawFile.CRC(&text[0],text.size());
}

// Get the documents encoding
Encoding DocumentViewer::GetEncoding()
{
  return _rawFileEncoding;
}

bool DocumentViewer::SetEncoding(Encoding newEncoding)
{
  if (_dirty)
  {
    wxMessageBox(L"Document is unsaved Please save the document before changing encoding");
    return false;
  }
  
  _rawFileEncoding = newEncoding;
  if (!_rawFileContents.size()) return true; 
  
#if 0
  switch (newEncoding)
  {
    case CCP_UCS2LE:
    case CCP_UTF16:
      if (_rawFileContents.size())
      {
        /*
         Write the data direction into the open file contents, as double byte from the file
         this is then encoded to UTF-8 that is passed to Scintilla that then converts UTF-8 to
         UTF-16 ready for rendering.
        
        _openFileContent.clear();
        _openFileContent.resize(_rawFileContents.size()/2);
        memcpy(&_openFileContent[0],&_rawFileContents[0],_rawFileContents.size());
        */
      }
      break;
    default:
      // Use the user selected encoding for the rawFileContents.
      _openFileContent = CharToUTF16(&_rawFileContents[0],_rawFileContents.size(),newEncoding);
      break;
  }

  // Update the open file encoding..
  _openFileEncoding = newEncoding;
#endif

  // Before switching encodings remember the current scroll position, and selection
  int topLine = _doc->GetTopLine();
  int cpos = _doc->GetCurrentPos();
  
  if (_docView==DOC_ORIGINAL) SetDocView(DOC_ORIGINAL);

  _doc->SetCurrentPos(cpos);
  _doc->SetSelection(cpos,cpos);
  _doc->ScrollToLine(topLine);

  _doc->Refresh();

  return true;
}

// Determine if the contents is ASCII
bool isASCII(const char *ptr)
{
  while (*ptr)
    if ((unsigned char)*ptr++>0x7e) return false;

  return true;
}

bool isUTF8(const char * string)
{
    if(!string) return 0;

    const unsigned char * bytes = (const unsigned char *)string;

    // Check for null terminator
    if (!bytes[0]||!bytes[1]||!bytes[2]) return 0;
    while(*bytes)
    {
        if(     (// ASCII
                        bytes[0] == 0x09 ||
                        bytes[0] == 0x0A ||
                        bytes[0] == 0x0D ||
                        (0x20 <= bytes[0] && bytes[0] <= 0x7E)
                )
        ) {
                bytes += 1;
                continue;
        }

        if(     (// non-overlong 2-byte
                        (0xC2 <= bytes[0] && bytes[0] <= 0xDF) &&
                        (0x80 <= bytes[1] && bytes[1] <= 0xBF)
                )
        ) {
                bytes += 2;
                continue;
        }

        if(     (// excluding overlongs
                        bytes[0] == 0xE0 &&
                        (0xA0 <= bytes[1] && bytes[1] <= 0xBF) &&
                        (0x80 <= bytes[2] && bytes[2] <= 0xBF)
                ) ||
                (// straight 3-byte
                        ((0xE1 <= bytes[0] && bytes[0] <= 0xEC) ||
                                bytes[0] == 0xEE ||
                                bytes[0] == 0xEF) &&
                        (0x80 <= bytes[1] && bytes[1] <= 0xBF) &&
                        (0x80 <= bytes[2] && bytes[2] <= 0xBF)
                ) ||
                (// excluding surrogates
                        bytes[0] == 0xED &&
                        (0x80 <= bytes[1] && bytes[1] <= 0x9F) &&
                        (0x80 <= bytes[2] && bytes[2] <= 0xBF)
                )
        ) {
                bytes += 3;
                continue;
        }

        if((// planes 1-3
              bytes[0] == 0xF0 &&
              (0x90 <= bytes[1] && bytes[1] <= 0xBF) &&
              (0x80 <= bytes[2] && bytes[2] <= 0xBF) &&
              (0x80 <= bytes[3] && bytes[3] <= 0xBF)
            ) ||
            (// planes 4-15
               (0xF1 <= bytes[0] && bytes[0] <= 0xF3) &&
               (0x80 <= bytes[1] && bytes[1] <= 0xBF) &&
               (0x80 <= bytes[2] && bytes[2] <= 0xBF) &&
               (0x80 <= bytes[3] && bytes[3] <= 0xBF)
            ) ||
            (// plane 16
                bytes[0] == 0xF4 &&
                (0x80 <= bytes[1] && bytes[1] <= 0x8F) &&
                (0x80 <= bytes[2] && bytes[2] <= 0xBF) &&
                (0x80 <= bytes[3] && bytes[3] <= 0xBF)
            )) 
        {
                bytes += 4;
                continue;
        }

        return 0;
    }

    return 1;
}



// Return the doc view names
const TCHAR *DocumentViewer::GetDocViewName()
{  
  if (_docView&DOC_ORIGINAL) return L"";
  else if (_docView&DOC_ORIGINAL_PP) return L"PP:";
  else if (_docView&DOC_ENGINE) return L"Engine:";
  else if (_docView&DOC_ENGINE_PP) return L"Engine PP:";
  else if (_docView&DOC_VM) return L"VM:";

  return L"";
}

unsigned int DocumentViewer::GetDocView()
{
  return _docView;
}

void DocumentViewer::PopulateWxMenu(wxMenu *menu)
{
#if _ORIGINAL_FILE_CTD_FIX
  unsigned int orignalMask = DOC_ORIGINAL;
#else
#if _DEBUG
  unsigned int orignalMask = DOC_ORIGINAL;
#else
 #if !_SD_LIVEFOLDER_IMP
  unsigned int orignalMask = 0; // Dont show the original file.
 #else
  unsigned int orignalMask = (-1&~DOC_ORIGINAL); // Dont show the original file.
 #endif
#endif
#endif
  if (_docContent&orignalMask) menu->Append(TAB_DOC_ORIGINAL_FILE, wxT("Original file"));  
  if (_docContent&DOC_ORIGINAL_PP) menu->Append(TAB_DOC_ORIGINAL_FILE_PP, wxT("View pp file"));
#if !_VBS_15079_RESTRICT_BISIM_SCRIPTS //allow engine files only for devs
  if (_docContent&DOC_ENGINE) menu->Append(TAB_DOC_ENGINE_FILE, wxT("View engine file"));  
  if (_docContent&DOC_ENGINE_PP) menu->Append(TAB_DOC_ENGINE_FILE_PP, wxT("View engine PP file"));
#endif
#if _VBS_14850_SHOW_FULL_FILE_PATH
  #if _VBS_15079_RESTRICT_BISIM_SCRIPTS
  if (_filePath.length() > 0 && !ContentsProtected()) //only allow copying file for unprotected files
  #else
  if (_filePath.length() > 0)
  #endif
  {
    menu->AppendSeparator();
    menu->Append(TAB_DOC_COPY_FULL_PATH, wxT("Full file path to clipboard"));
  }
#endif
}

ResolvePPFile &DocumentViewer::GetEngineResolvedPPFile()
{
  return this->_enginePPFileResolver;
}

void DocumentViewer::ClearCurrentView()
{
  SetDocViewContent(_docView,std::string());
  SetDocView(_docView);
  _dirty = false;
  GDocumentTabManager->SetTabName(this);
}

bool DocumentViewer::GetLastDiskWriteTime(FILETIME & ftime)
{
  HANDLE hFile;
  //windows file handle
  hFile = CreateFile(_filePath.wc_str(),GENERIC_READ, FILE_SHARE_READ, NULL, OPEN_EXISTING, 0, NULL);
  if (hFile == INVALID_HANDLE_VALUE)
    return false;
  if (!GetFileTime(hFile,NULL,NULL,&ftime)) //test for fail
  {
    CloseHandle(hFile);
    return false;
  }
  CloseHandle(hFile);
  return true;
}

bool DocumentViewer::FileChanged()
{
  if (_reloading)
    return false;
  if (_filePath.length() > 0)
  {
    FILETIME lastWriteTime;
    
    if (!GetLastDiskWriteTime(lastWriteTime))
    {
      return false;
    }
#if _SD_FILETIME_INIT
    bool ret = false;
    if (_lastWriteTime.dwHighDateTime || _lastWriteTime.dwLowDateTime) //only return changed when the file already had valid timestamps
#else
    bool ret = (CompareFileTime(&lastWriteTime,&_lastWriteTime) == 1);
#endif
    ret = (CompareFileTime(&lastWriteTime,&_lastWriteTime) == 1);
    //update the last modified time so we don't prompt the user over and over with the same file modification
    _lastWriteTime = lastWriteTime;
    return ret;
  }
  return false;
}

void DocumentViewer::SetReloading(bool value)
{
  _reloading = value;
}

void DocumentViewer::SetDocViewContent(unsigned int view, std::string &data, bool UCS2, FILETIME lastWriteTime)
{
  SetDocViewContent(view,&data[0],data.size(),UCS2,lastWriteTime);
}

void DocumentViewer::SetDocView(unsigned int view)
{
  // Virtual machine is allowed to update itself with new content.
  if (_docView==view&&_docView&~DOC_VM)
  {
  }
  else
  {
#if _VBS3_SD_IMP_BREAKPOINTS
    RemoveAllBreakpoints(); // When contents change, remove all break points for now
#else
    RemoveBreakPoint(-1);// When content change, remove all break points for now
#endif
  }

  _docView = view;
  _doc->SetReadOnly(false);
  _doc->ClearAll();

  // Empty the undo buffer.
  _doc->EmptyUndoBuffer(); 

#if _VBS_15079_RESTRICT_BISIM_SCRIPTS
  IsRestricted(true); //force restriction update
  if (ContentsProtected())
  {
    SetProtectedContent();
  }
  else
  {
#endif
    if (view&DOC_ORIGINAL)
    {
      /*
       Some choices here.
       If the document is UCS-2LE, then convert it to UTF-8, and set it raw
       If the document is ASCII, then set the contents raw.
       If the document is UTF-8, then set the contents raw (Native scintilla format)
      */   
      // If the file is UCS-2LE, convert it to UTF-8 and set it directly
      if (_rawFileEncoding==CCP_UCS2LE || _rawFileEncoding==CCP_UTF16)
      {
        std::string text = UTF16ToChar((wchar_t*)&_rawFileContents[0],_rawFileContents.size()/2, CCP_UTF8);
        _doc->SetTextRaw(&text[0]);
      }
      else
      {
        if (_rawFileEncoding==CCP_UTF8) // Can also be valid ASCII
        {
          _doc->SetTextRaw(&_rawFileContents[0]);
        }
        else
        {
          // ANSI (different code pages), convert to UTF-16
          wxString text = CharToUTF16(&_rawFileContents[0],_rawFileContents.size(),_rawFileEncoding);
          _doc->AddText(text);
        }
      }
    }
#if _VBS_15079_RESTRICT_BISIM_SCRIPTS
    else if (IsRestricted()) //never show engine contents for restricted scripts
    {
      if (_rawFileContents.size()) //either show whatever the user has on his HDD
      {
        SetDocView(DOC_ORIGINAL);
      }
      else //or just notify that this is a protected script
      {
        _internal = true;
        _restricted = true;
        SetProtectedContent();
      }
    }
#endif
    else if (view&DOC_ORIGINAL_PP)
    {
      Assert(false); // Original PP is not supported
    }
    else if (view&DOC_ENGINE)
    {
      if (_engineFileEncoding==EN_UTF8)
      {
        _doc->SetTextRaw(&_engineFileContent[0]);
      }
      else
      {
        wxString text = CharToUTF16(&_engineFileContent[0],_engineFileContent.size(),CP_Default);
        _doc->AddText(text);
      }
    }
    else if (view&DOC_ENGINE_PP)
    {
      // Set it directly, because all comments are stripped out
      _doc->SetTextRaw(&_enginePPFileContent[0]);
    }
    else if (view&DOC_VM)
    {
#if _VBS_15079_RESTRICT_BISIM_SCRIPTS     
      //vm should always be protected for the customer
      _internal = true;
      _restricted = true;
      SetProtectedContent();
#else
      _doc->AddText(CharToUTF16(_virtualMachine));
#endif
    }
#if _VBS3_DEBUG_DELAYCALL
    else if (view&DOC_DELAYED)
    {
#if _VBS_15079_RESTRICT_BISIM_SCRIPTS     
      //Delayed calls same as VM should always be protected for the customer
      _internal = true;
      _restricted = true;
      SetProtectedContent();
#else
      _doc->AddText(CharToUTF16(_virtualMachine));
#endif 
    }
#endif
#if _VBS_15079_RESTRICT_BISIM_SCRIPTS
  }
  bool isReadOnly = ((view&~DOC_ORIGINAL)>0) || ContentsProtected(); //protected files should be read-only
#else
  bool isReadOnly = (view&~DOC_ORIGINAL)>0;
#endif
  _doc->SetReadOnly(isReadOnly);

  // Set the tab manager, tab index name.
  GDocumentTabManager->SetTabName(this);

  _doc->Refresh();
}

void  DocumentViewer::SetDocViewContent(unsigned int view, char *src, size_t len, bool UCS2, FILETIME lastWriteTime)
{
  _docContent |= view;
  if (view&DOC_ORIGINAL)
  {
    if (UCS2&&len) 
    {
      // Remove the UCS-2LE header
      const int headerBytes = 2;
      src+=headerBytes;
      len-=headerBytes;
    }

    _lastWriteTime = lastWriteTime;
    
    // Copy the data across to our raw file contents
    _rawFileContents.resize(len);
    memcpy(&_rawFileContents[0],src,len);
    // Null terminator, when doing crc take this into consideration
    _rawFileContents.push_back(0);

    // Write directly the UCS-2 into the open file content.
    if (UCS2)
    {
      _rawFileEncoding = CCP_UCS2LE;
      _rawFileContents.push_back(0);
    }
    else
    {
      _rawFileEncoding = (Encoding) GetACP();
#if _VBS_15145_SD_ENCODING_FIX
      if (!isASCII(&_rawFileContents[0]) && isUTF8(&_rawFileContents[0])) _rawFileEncoding = CCP_UTF8;
#else
      if (!isASCII(src) && isUTF8(src)) _rawFileEncoding = CCP_UTF8;
#endif
    }
  }
  else if (view&DOC_ORIGINAL_PP)
  {
    Assert(false); // Haven t implemented any code for this
  }
  else if (view&DOC_ENGINE)
  {
    // Engine content can be the following
    // ANSI (code pages), UTF-8, ASCII
    _engineFileContent.resize(len);
    memcpy(&_engineFileContent[0],src,len);
    _engineFileContent.push_back(0);

    _engineFileEncoding = EN_SBCS;
    if (isUTF8(src)) _engineFileEncoding = EN_UTF8;

    CRCCalculator crcEngine;
    _engineFileCRC = crcEngine.CRC(&_engineFileContent[0],len);

    CreateCRCForRawContent();
  }
  else if (view&DOC_ENGINE_PP)
  {
    _enginePPFileContent.resize(len);
    memcpy(&_enginePPFileContent[0],src,len);
    _engineFileContent.push_back(0);

    // Pre-process the contents
    _enginePPFileResolver.ProccessPreProccessorContent(std::string(src,len));
  }
#if _VBS3_DEBUG_DELAYCALL
  else if (view&DOC_VM || view&DOC_DELAYED)
#else
  else if (view&DOC_VM)
#endif
  {
    _virtualMachine = std::string(src,len);
  }
}

#if _VBS_15079_RESTRICT_BISIM_SCRIPTS
void DocumentViewer::SetProtectedContent()
{
  _doc->SetText(L"Internal script.\nThe debugger only shows script contents for:\na) Scripts open from disk source. \nb) Scripts in vbs2\customer folder and its subfolders.\nc) Scripts from the current mission folder.");
}
#endif

wxString &DocumentViewer::GetRelativePath()
{
  return _relativePath;
}

#if _VBS_14074_DEV_PATH_UPDATE_FIX
void DocumentViewer::UpdateRelativePath()
{
#if _SD_LIVEFOLDER_PATHS
  //the _filePath can  be:
  // from the dev drive ( P:\vbs2\... )
  // from live folder ( O:\ng_beta\vbs2...)
  // already relative ( \vbs2\... , or simply init.sqf - mission scripts)
  // absolute mission path

  _relativePath = L"";
  if (_filePath.Length()) //it makes sense to search for substrings
  {
    if (GSettings._developerPath.Length()) //see if it's on the developer path
    {
      if (_filePath.Find(GSettings._developerPath) == 0)
      {
        _relativePath = _filePath.SubString(GSettings._developerPath.Length(), _filePath.Length());
        return;
      }
    }
    if (GSettings._liveFolderPath.Length()) //see if the path is in live folder
    {
      if (_filePath.Find(GSettings._liveFolderPath) == 0)
      {
        _relativePath = _filePath.SubString(GSettings._liveFolderPath.Length(), _filePath.Length());
        return;
      }
    }
    if (_filePath.Find(L":") == wxString::npos) //check if we have an absolute path
    {
      //no drive indicator means, that we already have a relativePath
      if (_filePath[0] == '\\') //we don't want the _relPath to start with a backslash
      {
        _relativePath = _filePath.SubString(1,_filePath.Length());
      }
      else
      {
        _relativePath = _filePath;
      }
    }
  }
#else
  // determine if the path is relative or not
  std::string devPath = UTF16ToChar(GSettings._developerPath);
  _relativePath = L"";
  // First, check the developer drive letter is the same.
  // if it is, then chances are the content was built from the 
  // absolute path of \\vbs2\\Customer
  // otherwise, its either, user path, that is not supported right now.
  bool insideDevPath = false;
  if (devPath.length()&&_filePath.length()) insideDevPath = tolower(_filePath[0])==tolower(devPath[0]);
  
  // If the file is inside the dev path, then it is relative within VBS2
  // otherwise, the path could be non deb path, or relative
  // determine this by  
  // absolute = c:\\
  // relative = \\vbs2\\path 
  if (insideDevPath)
  {
    const TCHAR *backSlash = wcschr(_filePath.data(),'\\');
    if (backSlash) 
    {
      backSlash++;
      _relativePath = wxString(backSlash);
    }
  }
  else
  {
    const TCHAR *driveIndicator = wcschr(_filePath.data(),':');
    if (!driveIndicator)
    {
      // file path, is actual a relative path
      _relativePath = _filePath;
    }
  }
#endif
}
#endif

#if _VBS_15079_RESTRICT_BISIM_SCRIPTS
bool DocumentViewer::GetStoredIsRestricted()
{
  return _restricted;
}

bool DocumentViewer::IsRestricted(bool update)
{
  bool isRestricted = GUtil.IsProtectedPath(_relativePath);
  if (update)
    _restricted = isRestricted;
  return isRestricted; //return the current value
}

bool DocumentViewer::ContentsProtected() const
{
  return (_restricted && _internal);
}

bool DocumentViewer::IsInternal() const
{
  return _internal;
}

void DocumentViewer::SetInternal(bool isInternal)
{
  _internal = isInternal;
}
#endif

DocumentViewer::DocumentViewer(const wxString& name, const wxString &filePath, wxWindow* parent)
{
    _name = name;  
    _filePath = filePath;
#if _VBS3_SD_IMP_BREAKPOINTS
    _filePath.MakeLower();
#endif
#if _SD_FILETIME_INIT
    //init file timestamps with 0s to prevent prompting a reload when the file is first loaded
    memset(&_lastWriteTime,0,sizeof _lastWriteTime);
#endif
    _reloading = false;
    _doc = new wxScintillaExt(parent);
    _dirty = false;

    _rawFileCRC = 0;
    _engineFileCRC = 0;
  
    // Get the default encoding
    //_openFileEncoding = (Encoding) GetACP();
    _rawFileEncoding = (Encoding) GetACP();
    _engineFileEncoding = EN_SBCS;

#if _VBS_14074_DEV_PATH_UPDATE_FIX
    UpdateRelativePath();
#else
    // determine if the path is relative or not
    std::string devPath = UTF16ToChar(GSettings._developerPath);

    // First, check the developer drive letter is the same.
    // if it is, then chances are the content was built from the 
    // absolute path of \\vbs2\\Customer
    // otherwise, its either, user path, that is not supported right now.
    bool insideDevPath = false;
    if (devPath.length()&&filePath.length()) insideDevPath = tolower(filePath[0])==tolower(devPath[0]);
    
    // If the file is inside the dev path, then it is relative within VBS2
    // otherwise, the path could be non deb path, or relative
    // determine this by  
    // absolute = c:\\
    // relative = \\vbs2\\path 
    if (insideDevPath)
    {
      const TCHAR *backSlash = wcschr(_filePath.data(),'\\');
      if (backSlash) 
      {
        backSlash++;
        _relativePath = wxString(backSlash);
      }
    }
    else
    {
      const TCHAR *driveIndicator = wcschr(_filePath.data(),':');
      if (!driveIndicator)
      {
        // file path, is actual a relative path
        _relativePath = _filePath;
      }
    }
#endif

#if _VBS_15079_RESTRICT_BISIM_SCRIPTS
    _internal = true;
    _restricted = IsRestricted(true);
#endif

  _docContent = 0;
  _docView = 0;

  // Break point circle, and also current line execution
  _doc->SetMarginType(COL_DEBUG_POINT, wxSCI_MARGIN_SYMBOL );
  _doc->SetMarginMask(COL_DEBUG_POINT, ~wxSCI_MASK_FOLDERS);
  _doc->SetMarginWidth(COL_DEBUG_POINT, 16 );
  _doc->SetMarginSensitive(COL_DEBUG_POINT, true);

  // Line Numbers
  int lineNumberMask = ~wxSCI_MASK_FOLDERS & ~(1<<wxSCI_MARK_CIRCLE) & ~(1<<wxSCI_MARK_ARROW);
  _doc->SetMarginType(COL_LINE_NUMBERS, wxSCI_MARGIN_NUMBER ); 
  _doc->SetMarginMask(COL_LINE_NUMBERS,  lineNumberMask );
  _doc->SetMarginWidth(COL_LINE_NUMBERS, _doc->TextWidth (wxSCI_STYLE_LINENUMBER, wxT("_99999"))  );
  _doc->SetMarginSensitive(COL_LINE_NUMBERS, false);

  /*
  _doc->SetMarginType(COL_DOTTED_SEP, wxSCI_MARGIN_SYMBOL);
  _doc->SetMarginMask(COL_DOTTED_SEP,  ~wxSCI_MASK_FOLDERS);
  _doc->SetMarginWidth(COL_DOTTED_SEP, 1);
  _doc->SetMarginSensitive(COL_DOTTED_SEP, false);
  */

  // Folder folding
  _doc->SetMarginType(COL_FOLDING_MARGIN, wxSCI_MARGIN_SYMBOL);
  _doc->SetMarginMask(COL_FOLDING_MARGIN, wxSCI_MASK_FOLDERS);
  _doc->SetMarginWidth(COL_FOLDING_MARGIN, 10);
  _doc->SetMarginSensitive(COL_FOLDING_MARGIN, true);

  _doc->SetProperty( wxT("fold"), wxT("1") );
  _doc->SetFoldFlags( wxSCI_FOLDFLAG_LINEBEFORE_CONTRACTED | wxSCI_FOLDFLAG_LINEAFTER_CONTRACTED );

  // marker generic define
  for (int i=0; i<wxSCI_MARKER_MAX; i++)
    _doc->MarkerDefine(i,i);


  // Set the circle as red, to indicate debug line
  _doc->MarkerSetBackground (wxSCI_MARK_CIRCLE, wxColour (wxT("RED")));
  _doc->MarkerSetForeground (wxSCI_MARK_CIRCLE, wxColour (wxT("RED")));


  // markers
  _doc->MarkerDefine (wxSCI_MARKNUM_FOLDER, wxSCI_MARK_BOXMINUS);
  _doc->MarkerSetBackground (wxSCI_MARKNUM_FOLDER, wxColour (wxT("BLACK")));
  _doc->MarkerSetForeground (wxSCI_MARKNUM_FOLDER, wxColour (wxT("WHITE")));
  _doc->MarkerDefine (wxSCI_MARKNUM_FOLDEROPEN, wxSCI_MARK_BOXMINUS);
  _doc->MarkerSetBackground (wxSCI_MARKNUM_FOLDEROPEN, wxColour (wxT("BLACK")));
  _doc->MarkerSetForeground (wxSCI_MARKNUM_FOLDEROPEN, wxColour (wxT("WHITE")));
  _doc->MarkerDefine (wxSCI_MARKNUM_FOLDERSUB, wxSCI_MARK_EMPTY);
  _doc->MarkerDefine (wxSCI_MARKNUM_FOLDEREND, wxSCI_MARK_BOXPLUS);
  _doc->MarkerSetBackground (wxSCI_MARKNUM_FOLDEREND, wxColour (wxT("BLACK")));
  _doc->MarkerSetForeground (wxSCI_MARKNUM_FOLDEREND, wxColour (wxT("WHITE")));
  _doc->MarkerDefine (wxSCI_MARKNUM_FOLDEROPENMID, wxSCI_MARK_BOXMINUS);
  _doc->MarkerSetBackground (wxSCI_MARKNUM_FOLDEROPENMID, wxColour (wxT("BLACK")));
  _doc->MarkerSetForeground (wxSCI_MARKNUM_FOLDEROPENMID, wxColour (wxT("WHITE")));
  _doc->MarkerDefine (wxSCI_MARKNUM_FOLDERMIDTAIL, wxSCI_MARK_EMPTY);
  _doc->MarkerDefine (wxSCI_MARKNUM_FOLDERTAIL, wxSCI_MARK_EMPTY);

  // Set the default charset to be ANSI, and ASCII
  _doc->StyleSetCharacterSet(wxSCI_STYLE_DEFAULT,wxSCI_CHARSET_ANSI);

  // Reset all styles to the same default value. eg.. STYLE_DEFAULT
  _doc->StyleClearAll();
  
#if _SD_DEFAULT_SCI_STYLE
  _doc->SetIndent(4); //same indentation as set in scintilla's document
  _doc->SetUseTabs(true);
#endif
  
  // Scintilla internal representation is UTF-8. When rendering it will convert
  // UTF8 to the native platform format. This is UTF16 on windows. Though
  // this means, with the current version we cannot change the underlying
  // encoding.
  _doc->SetCodePage(wxSCI_CP_UTF8);

  // Set the lexer to SQF
  _doc->SetLexer(wxSCI_LEX_SQF);

  // Set the debug point, and line number style background color
  _doc->SetMarginStyle(COL_DEBUG_POINT,COL_DEBUG_POINT_STYLE);
  _doc->StyleSetBackground(COL_DEBUG_POINT_STYLE,wxColour(211,211,211));

  /*
  _doc->SetMarginStyle(COL_DOTTED_SEP,COL_DOTTED_SET_STYLE);
  _doc->StyleSetBackground(COL_DOTTED_SET_STYLE,wxColour(43,145,175));
  */

  // Set the line number to be white
  _doc->StyleSetBackground(wxSCI_STYLE_LINENUMBER,wxColour(255,255,255));
  _doc->StyleSetForeground(wxSCI_STYLE_LINENUMBER,wxColour(43,145,175));

  // Set the style for the background for folding
  _doc->SetFoldMarginColour(true,wxColour(255,255,255));
  _doc->SetFoldMarginHiColour(true,wxColour(255,255,255));

  // Set auto complete so it doest show a single entry
  _doc->AutoCompSetChooseSingle(false);
  _doc->AutoCompSetIgnoreCase(true);
  _doc->AutoCompSetAutoHide(false);
  _doc->AutoCompSetDropRestOfWord(true);

  // Set the document so we can receive event's
  _doc->PushEventHandler(this);

  // Remove the border around the scintilla
  long exstyle = GetWindowLong((HWND)_doc->GetHWND(), GWL_EXSTYLE);
  exstyle &= ~WS_EX_CLIENTEDGE;
  SetWindowLong((HWND)_doc->GetHWND(), GWL_EXSTYLE, exstyle);

  // Update the document IntelliSense
  UpdateDocIntelliSense();

  _lineNum = -1;
}

extern IGameState *GGameState;

#define CONVERT(XX) \
    XX = CharToUTF16(ptr->XX);

struct IGameValue
{
  SmartRef<SuffixArray> name;
  wxString description;
  wxString example;
  wxString exampleResult;
  wxString since;
  wxString changed;
  wxString category;

  virtual SuffixArray *GetSuffixArray()
  {
    return name.GetRef();
  }
  // Draw yourself to the text field, do any coloring or anything that is required
  virtual void Draw(wxScintilla *text)
  {
    text->ClearAll();
    text->AppendText(description);
    text->AppendText(L"\n\n");
    text->AppendText(example);
    text->AppendText(L"\n\n");
    text->AppendText(exampleResult);
  }

  // Return the char pointer to the name, used very often
  operator const char*() 
  {
    return name->string.data();
  }
};


struct iiFunction : public IGameValue
{
  wxString loper;
  wxString roper;
  
  iiFunction(iFunction *ptr)
  {
      name = suffixArrayCreate(ptr->name);
      CONVERT(loper);
      CONVERT(roper);
      CONVERT(description);
      WxStringTagCleaner parser;
      description = parser.Parse(description);
      CONVERT(example);
      CONVERT(exampleResult);
      CONVERT(since);
      CONVERT(changed);
      CONVERT(category);
  }
};
struct iiOperator  : public IGameValue
{
  wxString loper;
  wxString roper;
  
  iiOperator(iOperator *ptr)
  {
      name = suffixArrayCreate(ptr->name);
      CONVERT(loper);
      CONVERT(roper);
      CONVERT(description);
      WxStringTagCleaner parser;
      description = parser.Parse(description);
      CONVERT(example);
      CONVERT(exampleResult);
      CONVERT(since);
      CONVERT(changed);
      CONVERT(category);
  }
};
struct iiNuler  : public IGameValue
{
  iiNuler(iNuler *ptr)
  {
      name = suffixArrayCreate(ptr->name);
      CONVERT(description);
      WxStringTagCleaner parser;
      description = parser.Parse(description);
      CONVERT(example);
      CONVERT(exampleResult);
      CONVERT(since);
      CONVERT(changed);
      CONVERT(category);
  }
};

struct iiPPDirective : public IGameValue
{
  iiPPDirective(const char *ppName, const char *ppDes, const char *ppEx)
  {
      name = suffixArrayCreate(ppName);
      description = CharToUTF16(ppDes);
      example = CharToUTF16(ppEx);
  }
};

std::vector< iiFunction > GFunctions;
std::vector< iiOperator > GOperators;
std::vector< iiNuler > GNulers;
std::vector< iiPPDirective > GPPDefines;

typedef std::vector< IGameValue * > VecIGameValue;
VecIGameValue GCodeHints; // All functions, operators, and nulers for code hints.


/*
  Implement the auto complete, somehow?
*/
inline bool IsAlphabetic(unsigned int ch) {
	return ((ch >= 'A') && (ch <= 'Z')) || ((ch >= 'a') && (ch <= 'z'));
}

std::string buffer;
wxString autoCmpList;

bool IsAlphaOrMember(int character)
{
#if _VBS_15193_MOUSOVER_VARS_FIX
  return isalnum(character)||character=='#'||character=='_';
#else
  return IsAlphabetic(character)||character=='#'||character=='_';
#endif
}
bool CompLowerFunc(IGameValue *c1, IGameValue *c2)
{
    return strcmp(*c1,*c2) < 0;
}
bool CompFunc(IGameValue *c1, IGameValue *c2)
{
    return strcmp(*c1, *c2)==0;
}
struct CompLowerWrapper
{
  bool  operator() (IGameValue *c1, IGameValue *c2) const
  {
      return strcmp(*c1, *c2) < 0;
  }
  bool  operator() (const char *c1, IGameValue *c2) const
  {
      return strcmp(c1, *c2) < 0;
  }
  bool  operator() (IGameValue *c1, const char *c2) const
  {
      return strcmp(*c1, c2) < 0;
  }
};


// When any modification other than insert happens, clear the accumulated buffer
int GLastPosition;

void DocumentViewer::OnAnyEventImp()
{
  // Clear the call tips, and anything else.
   _doc->CallTipCancel();
}

int GMousePosPrev;
int GMousePosCurrent;

void DocumentViewer::OnScintillaUpdatedUI(wxScintillaEvent &event)
{
  int currentPos = _doc->GetCurrentPos();
  if (abs(currentPos-GLastPosition)>1) buffer.clear();

  GLastPosition = currentPos;
}

int GMousePrevMinMax[2];

wxString DocumentViewer::CallTipSizeCalculator(wxString &content, int maxWidth)
{
    // Calculate the string height-width
    wxString faceName = _doc->StyleGetFaceName(wxSCI_STYLE_CALLTIP);
    wxFont font(faceName);
    
    wxClientDC dc(_doc);
    dc.SetFont(font);
    if (dc.GetTextExtent(content).GetX()<maxWidth) return content;// Everything okay, its bellow the max width


    // Composed new line
    wxString newLine;     
    // Returned text
    wxString newText;

    wxStringTokenizer tkz(content, wxT(" "));
    while (tkz.HasMoreTokens() )
    {
      wxString token = wxString(L" ") + tkz.GetNextToken();

      wxSize tokenSize = dc.GetTextExtent(token);
      wxSize newLineSize = dc.GetTextExtent(newLine);
      
      if (tokenSize.GetX()+newLineSize.GetX()>maxWidth)
      {
        newText = newText + newLine + wxString( L"\r\n");
        newLine = wxString();
      }
      newLine = newLine + token;
    }
    newText = newText + newLine;
    
    return newText;
}


void DocumentViewer::OnMouseUpdate()
{
  // Make sure the application is in focus.
  if (!GetActiveWindow()) return;

  // Get the current caret position
  // select the identifier or word.
  int currentPos = GMousePosCurrent;
  if (currentPos!=GMousePosPrev)
  {
    // Move back a character from the caret
    int cPos = currentPos;
    int cPosMin = -1;
    int cPosMax = -1;

    std::string identifier;
    while (cPos>0 && cPos<_doc->GetLength() && IsAlphaOrMember(_doc->GetCharAt(cPos-1))) cPos--;
    // Update pos min
    cPosMin = cPos;

    while 
    (
      cPos>=0 && 
      cPos<_doc->GetLength() && 
      IsAlphaOrMember(_doc->GetCharAt(cPos))
    ) cPos++;
    // Update posMax
    cPosMax = cPos;

   
    // Some validation, and also to make sure the current pos is within the min max.    
    if (cPosMin!=-1 && cPosMax!=-1 && currentPos>=cPosMin && currentPos<=cPosMax)
    {
      wxString ident = _doc->GetTextRange(cPosMin,cPosMax);
      identifier = UTF16ToChar(ident);
    }

    if (cPosMin == GMousePrevMinMax[0] && cPosMax == GMousePrevMinMax[1])
    {
      return; // No update required.
    }
    else 
    {
      _doc->CallTipCancel(); // Clear the tool tip, ready for the new one
    }
    
    // Remember the previous min max
    GMousePrevMinMax[0] = cPosMin;
    GMousePrevMinMax[1] = cPosMax;
    
    std::transform(identifier.begin(), identifier.end(), identifier.begin(), ::tolower); // Convert identifier to lower

    // Show the documentation
    if (identifier.length() && identifier[0]!='_')
    {
      //comparisonFunc
      CompLowerWrapper cmp;
      VecIGameValue::iterator lbounds = std::lower_bound(GCodeHints.begin(),GCodeHints.end(),identifier.data(),cmp);
      VecIGameValue::iterator hbounds = std::upper_bound(GCodeHints.begin(),GCodeHints.end(),identifier.data(),cmp);
      
      
      VecIGameValue found;
      if (lbounds!=GCodeHints.end() && hbounds!=GCodeHints.end() && lbounds<hbounds)
      {
        for (; lbounds < hbounds; lbounds++)
           if (strcmp(*(*lbounds),identifier.data())==0)
            found.push_back(*lbounds);
      }
      else if (lbounds!=GCodeHints.end())
      {
        if (strcmp(*(*lbounds),identifier.data())==0) found.push_back(*lbounds);
      }

      // There is a couple of situation's here
      // firstly, Nuler, can be evaulted on the fly. Because they return back a value
      // So we accept the following.
      // 1) Nuler's
      // 2) GlobalVars
      // 3) _localVars... [x]
      if (found.size())
      {
        iiNuler *inuller = dynamic_cast<iiNuler*>(found[0]);
        if (inuller)
        {
          std::string nulerVal = GUtil.EvaulateValue(identifier);
          wxString desc = found[0]->description;
          wxString nulerValwx = CharToUTF16(nulerVal.data());
          wxString msg = wxString::Format(L"%s\r\n\r\nValue:%s",desc.data(),nulerValwx.data());

          _doc->CallTipShow(currentPos,msg);
        }
        else
        {
          wxString newDesc = CallTipSizeCalculator(found[0]->description);

          _doc->CallTipShow(cPosMax,newDesc);
        }
      }
      else
      {
        // Could be a global variable update it. Script needs to be in focus to prevent a error
        if (identifier.length() && GFrame->GetFocus() )
        {
          std::string val = GUtil.EvaulateValue(identifier);
          _doc->CallTipShow(cPosMax,CharToUTF16(val.data()));
        }
        else
        {
          _doc->CallTipCancel();
        }
      }
    }
    // Deal with _local vars!, make sure the script is in breaked state
    else if 
    (
      identifier.length() && identifier[0]=='_' 
      && GFrame->GetFocus() 
      && GFrame->GetFocus()->IsRunning()
    ) 
    {
      std::string val = GUtil.EvaulateValue(identifier);
      _doc->CallTipShow(cPosMax,CharToUTF16(val));
    }
    else
    {
      _doc->CallTipCancel();
    }
  }
}

void DocumentViewer::OnMouseWindowExit(wxMouseEvent &event)
{
  GMousePosPrev = GMousePosCurrent = 0;
  _doc->CallTipCancel();
  
  event.Skip();
}
void DocumentViewer::OnMouseWindowEnter(wxMouseEvent &event)
{
  event.Skip();
}


void DocumentViewer::OnMouseWindowMove(wxMouseEvent &event)
{
  event.GetPosition();

  GMousePosPrev = GMousePosCurrent;
  GMousePosCurrent = _doc->PositionFromPoint(event.GetPosition());

  OnMouseUpdate();

  event.Skip();
}

void DocumentViewer::OnScintillaModified(wxScintillaEvent &evt)
{
#if _SD_LIVEFOLDER_IMP
  int modType = evt.GetModificationType();
  // wxSCI_MOD_BEFOREINSERT , wxSCI_MOD_BEFOREDELETE , wxSCI_MOD_CHANGEMARKER
  if (modType & wxSCI_MOD_INSERTTEXT || modType & wxSCI_MOD_DELETETEXT)
  {
    int linesChange = evt.GetLinesAdded();
    if (linesChange != 0)
    {
      int evtLine = _doc->LineFromPosition(evt.GetPosition());
      //move all BPs after this line up/down
      for (size_t i = 0; i < _bpUpdates.size(); ++i)
      {
        ScriptBreakpoint& bp = _bpUpdates[i];
        if (bp._sourceLineNumber > evtLine)
        {
          bp._sourceLineNumber += linesChange;
          bp._absSouceLineNumber += linesChange;
        }
      }
    }
  }
#endif
  evt.Skip();
}

#if _SD_LIVEFOLDER_IMP
void DocumentViewer::ApplyBPUpdates()
{
  std::vector<DWORD> bpsToAdd; //modified breakpoints to re-add
  for (size_t i = 0; i < _bpUpdates.size(); ++i)
  {
    DWORD bpId = _bpUpdates[i]._engineBPNum;
    ScriptBreakpoint& bp = GBreakpointManager.GetBreakpoint(bpId);

    if (bp != _bpUpdates[i])
    {
      //To move a breakpoint, we remove it first and then readd on a different line
      GBreakpointManager.RemoveBreakpoint(bpId);
      DWORD newId = GBreakpointManager.AddBreakpoint(_bpUpdates[i]._filenameEngine.c_str(), _bpUpdates[i]._filenameDoc.c_str(), _bpUpdates[i]._handle,
        _bpUpdates[i]._sourceLineNumber, _bpUpdates[i]._absSouceLineNumber, _bpUpdates[i]._enabled);
      bpsToAdd.push_back(newId);
      
      _bpUpdates[i]._engineBPNum = newId;
      _breakPoints.erase(_breakPoints.begin());
    }
  }
  
  //Add all the 'new' breakpoints
  for (size_t i = 0; i < bpsToAdd.size(); ++i)
  {
    _breakPoints.push_back(bpsToAdd[i]);
  }
}
#endif

template<class SuffixAType,class Container>
void GetAllSuffix(SuffixAType &found,Container &container, const char *enteredChar)
{
    // Do a search for functions with this sub string
    for (size_t i=0; i<container.size(); i++)
    {
      size_t ignore = 0;
      if (suffixArraySearch(container[i].name.GetRef(),enteredChar,&ignore))
        found.push_back(&(container[i]));
    }
}


std::vector<IGameValue*> GfoundSuffix;

void DocumentViewer::OnAutoCompleteSelected(wxScintillaEvent &event)
{
  // Message will contain the index that the current message is highlighted
  if (event.GetMessage()>=0)
  {
    int index = event.GetMessage();
    if (index < (int)GfoundSuffix.size())
    {
      // Very bad but oh well...
      GetCodeHinting()->Show(true);
      GfoundSuffix[index]->Draw(GetCodeHinting()->_text);
    }
  }
  else
  {
    // On shutdown, scintilla sends a notification to close the auto-complete.
    // do not create a code hinting box. Through the skeleton interface!
    if (IsCodeHintingBox()) GetCodeHinting()->Show(false);
  }
  event.Skip();
}

void DocumentViewer::Notify()
{
  bool isPPDirective = buffer.size()&&buffer[0]=='#';
  if (buffer.size()>2||isPPDirective)
  {
    autoCmpList.clear();
    GfoundSuffix.clear();

    if (isPPDirective)
    {
      GetAllSuffix(GfoundSuffix,GPPDefines,buffer.data());
    }
    else
    {
      GetAllSuffix(GfoundSuffix,GFunctions,buffer.data());
      GetAllSuffix(GfoundSuffix,GNulers,buffer.data());
      GetAllSuffix(GfoundSuffix,GOperators,buffer.data());
    }
    
    // Quick sort the found items, make it easier for the user to find the
    // one they're after
    std::sort(GfoundSuffix.begin(),GfoundSuffix.end(),CompLowerFunc);

    // Make a append list.
    for (size_t i=0; i<GfoundSuffix.size(); i++)
    {
        std::string append = GfoundSuffix[i]->GetSuffixArray()->string;

        if (autoCmpList.size()) autoCmpList.append(1,' ');
        autoCmpList = autoCmpList + wxString::From8BitData(append.data());
    }

    if (autoCmpList.size())
    {
      long listHwnd = _doc->AutoCompShow(buffer.size(),autoCmpList);

      // Okay just a quick example create a window where the auto complete pops up.
      if (listHwnd)
      {
        RECT winSize;
        if (GetWindowRect((HWND)listHwnd,&winSize))
        {
          CodeHinting *hint = GetCodeHinting();
          hint->MainWindow()->Move(wxPoint(winSize.left+(winSize.right-winSize.left)-1,winSize.top),wxSIZE_NO_ADJUSTMENTS);
          //hint->Show(true);
        }      
      }
    }
  }
}

void DocumentViewer::OnScintillaCharAdded(wxScintillaEvent &event)
{
  TCHAR key = event.GetKey();
  if ((IsAlphabetic(key) && key != ' ') || key == '#')
  {
    buffer.append(1,(char)tolower(key));
  }
  else 
  {
    // Cancel auto complete, on any characters other than text
    _doc->AutoCompCancel();
    buffer.clear();
  }

  if (buffer.size()) Start(200,true);

  // Update the tab...
#if _SD_ENGINE_FILE_NOSAVE
  if (_dirty!=true && (_docView&DOC_ORIGINAL || _docView&DOC_ORIGINAL_PP))
#else
  if (_dirty!=true)
#endif
  {
    _dirty = true;
    GDocumentTabManager->SetTabName(this);
  }

  event.Skip();
}

void DocumentViewer::UpdateDocIntelliSense()
{
  wxFont font(10, wxMODERN, wxNORMAL, wxNORMAL);
  _doc->StyleSetFont(wxSCI_STYLE_DEFAULT, font );
  
  _doc->StyleSetForeground(wxSCI_C_STRING, wxColour(0, 128, 0));
  _doc->StyleSetForeground(wxSCI_C_STRINGEOL, wxColour(0, 128, 0));
  _doc->StyleSetForeground(wxSCI_C_PREPROCESSOR, *wxLIGHT_GREY);
  _doc->StyleSetForeground(wxSCI_C_COMMENT, wxColour(0, 128, 0));
  _doc->StyleSetForeground(wxSCI_C_COMMENTLINE, wxColour(0, 128, 0));
  _doc->StyleSetForeground(wxSCI_C_COMMENTDOC, wxColour(0, 128, 0));
  _doc->StyleSetForeground(wxSCI_C_COMMENTLINEDOC, wxColour(0, 128, 0));
  _doc->StyleSetForeground(wxSCI_C_NUMBER, *wxBLUE );
  _doc->SetSelBackground(true, wxSystemSettings::GetColour(wxSYS_COLOUR_HIGHLIGHT));
  _doc->SetSelForeground(true, wxSystemSettings::GetColour(wxSYS_COLOUR_HIGHLIGHTTEXT));
  _doc->SetCaretWidth(2);
  /*
   0 - {"VBSOperators",SCE_C_WORD}
   1 - {"VBSFunctions",SCE_C_WORD2}
   3 - {"VBSNulers",SCE_C_GLOBALCLASS}
  */
  wxString allOperator;
  for (size_t i=0; i<GOperators.size(); i++)
  {
    if (i) allOperator.append(1,' ');
    allOperator = allOperator + CharToUTF16(GOperators[i].name->string);
  }
  wxString allFunctions;
  for (size_t i=0; i<GFunctions.size(); i++)
  {
    if (i) allFunctions.append(1,' ');
    allFunctions = allFunctions + CharToUTF16(GFunctions[i].name->string);
  }

  wxString allNulers;
  for (size_t i=0; i<GNulers.size(); i++)
  {
    if (i) allNulers.append(1,' ');
    allNulers = allNulers + CharToUTF16(GNulers[i].name->string);
  }

  // Operators
  _doc->StyleSetBold(wxSCI_C_WORD, false);
  _doc->StyleSetForeground(wxSCI_C_WORD, *wxBLUE);
  _doc->SetKeyWords(0, allOperator);

  // Functions
  _doc->StyleSetBold(SCE_C_WORD2, false);
  _doc->StyleSetForeground(SCE_C_WORD2, *wxBLUE);
  _doc->SetKeyWords(1, allFunctions);

    // Nuler's
  _doc->StyleSetBold(SCE_C_GLOBALCLASS, false);
  _doc->StyleSetForeground(SCE_C_GLOBALCLASS, *wxBLUE);
  _doc->SetKeyWords(3, allNulers);

  // Read from the settings, and apply and saved styles to the document.
  for (int styleID = 0; styleID<MAX_STYLES; styleID++)
  {
    StyleLoader *style = GSettings.GetStyle(styleID);
    if (style) style->PushScintillaStyleState(_doc);
  }
}

//GCodeHints
void AddToCodeHints(IGameValue& val)
{
  GCodeHints.push_back(&val);
}

void DocumentViewer::UpdateIntelliSense()
{
  if (!GGameState) return;

  // Compile a list of all keywords 
  int nNum = GGameState->FunctionsSize();
  for (int i=0; i<nNum; i++)
  {
    iFunction funct;
    GGameState->GetFunction(i,funct);
    GFunctions.push_back(  iiFunction(&funct) );
  }
  nNum = GGameState->OperatorSize();
  for (int i=0; i<nNum; i++)
  {
    iOperator oper;
    GGameState->GetOperator(i,oper);
    GOperators.push_back(  iiOperator(&oper) );
  }
  nNum = GGameState->NulersSize();
  for (int i=0; i<nNum; i++)
  {
    iNuler nuler;
    GGameState->GetNuler(i,nuler);
    GNulers.push_back(  iiNuler(&nuler) );
  }

  GPPDefines.push_back( iiPPDirective("#include","A preprocessor include directive causes the preprocessor to replace the directive with the contents of the specified file.", "#include \"myIncludefile.hpp\""));
  GPPDefines.push_back( iiPPDirective("#define","A preprocessor define directive directs the preprocessor to replace all subsequent occurrences of a macro with specified replacement tokens.","#define _TECH_RELEASE 0"));
  GPPDefines.push_back( iiPPDirective("#ifdef","The #ifdef directive checks for the existence of macro definitions.\r\n If the identifier specified is defined as a macro, the lines of code that immediately follow the condition are passed on to the compiler.", "#ifdef _TECH_RELEASE\r\n//SomeTechnolog\r\n#endif"));
  GPPDefines.push_back( iiPPDirective("#ifndef","The #ifndef directive checks whether a macro is not defined.\r\n If the identifier specified is not defined as a macro, the lines of code immediately follow the condition are passed on to the compiler.", "#ifndef _TECH_RELEASE\r\n//SomeTechnolog\r\n#endif"));
  GPPDefines.push_back( iiPPDirective("#else","If the condition specified in the #ifdef, or #ifndef directive evaluates to 0, and the conditional compilation directive contains a preprocessor #else directive, the lines of code located between the preprocessor #else directive and the preprocessor #endif directive is selected by the preprocessor to be passed on to the compiler.", ""));
  GPPDefines.push_back( iiPPDirective("#endif","The preprocessor #endif directive ends the conditional compilation directive.", ""));
  GPPDefines.push_back( iiPPDirective("#","The # (single number sign) operator converts a parameter of a function-like macro into a character string literal. For example, if macro ABC is defined using the following directive:", "  #define ABC(x)   #x"));
  GPPDefines.push_back( iiPPDirective("##","The ## (double number sign) operator concatenates two tokens in a macro invocation (text and/or arguments) given in a macro definition.", " #define XY(x,y)    x##y"));

  // Add all all the Functions, operators, nulers
  // to the global code hints. This is then qsorted
  ForEach(GFunctions,AddToCodeHints);
  ForEach(GOperators,AddToCodeHints);
  ForEach(GNulers,AddToCodeHints);
  ForEach(GPPDefines,AddToCodeHints);

  std::sort(GCodeHints.begin(),GCodeHints.end(),CompLowerFunc);

  // Update the documents intelli-sense
  if (GDocumentManager->GetVirtualMachineDoc())
    if (GDocumentManager->GetVirtualMachineDoc()->GetDocument())
      GDocumentManager->GetVirtualMachineDoc()->UpdateDocIntelliSense();
}


void DocumentViewer::UnHook()
{
  // Remove any of the registered break points.
#if _VBS3_SD_IMP_BREAKPOINTS
  RemoveAllBreakpoints();
#else
  RemoveBreakPoint(-1);
#endif

  _doc->RemoveEventHandler(this);
}

// Unregister ourself
DocumentViewer::~DocumentViewer()
{
}

void DocumentViewer::SetLineNumber(int lineNumber, bool isStepping, StepKind stepKind)
{
  // Delete all placed markers related to current execution line
  _doc->MarkerDeleteAll(wxSCI_MARK_ARROW);
  _doc->MarkerDeleteAll(wxSCI_MARK_VLINE);
  _doc->MarkerDeleteAll(wxSCI_MARK_LCORNER);
  _lineNum = lineNumber;

  // If line number is -1, then nothing to do but return.
  if (lineNumber==-1) return;

  /*
    wxScitilla uses the following convention
    Lines start from 0

    VBS2 Script debugger use the following convention
    Lines start from 1

    Offset, by VBS2 line number by -1, for wxScittila
    There can be a problem, that 0 is returned.
  */
  if (_lineNum) _lineNum--;

#if _VBS_15079_RESTRICT_BISIM_SCRIPTS
  if (!ContentsProtected() && (_lineNum <= _doc->GetLineCount())) //only show real line number for unprotected files
    _doc->MarkerAdd(_lineNum,wxSCI_MARK_ARROW);
  else
    _doc->MarkerAdd(0,wxSCI_MARK_ARROW); //first line otherwise
#else
  _doc->MarkerAdd(_lineNum,wxSCI_MARK_ARROW);
#endif
  //If we're trying to step out/over, we show a marker indicating that
  if (isStepping)
  {
    switch (stepKind)
    {
    case SKOver:
      {
        _doc->MarkerAdd(_lineNum + 1,wxSCI_MARK_LCORNER);
        break;
      }
    case SKOut:
      {
        //figure out how far out are we stepping
        int parent = _doc->GetFoldParent(_lineNum); //the line starting current scope
        int endline;
        if (parent == -1) //whole file
        {
          endline = _doc->GetLineCount();
        }
        else
        {
          endline = _doc->GetLastChild(parent,-1); //line ending current scope
        }
        for (int i = _lineNum + 1; i < endline; ++i)
        {
          _doc->MarkerAdd(i,wxSCI_MARK_VLINE);
        }
        _doc->MarkerAdd(endline,wxSCI_MARK_LCORNER); //mark the scope we're stepping out of
        break;
      }
#if _VBS_1428_STEP_FILE
    case SKFile:
      {
        //show the step-out marker to the end of file, because we don't know where new file starts (may be any of #include, call, spawn commands)
        for (int i = _lineNum + 1; i < _doc->GetLineCount(); ++i)
        {
          _doc->MarkerAdd(i, wxSCI_MARK_VLINE);
        }
        break;
      }
#endif
    default:
      {}
    }
  }

  int scrollPos = _doc->GetScrollPos(wxVERTICAL);
  int scrollRange = _doc->GetScrollRange(wxVERTICAL);
  int linesOnScreenVisible = _doc->LinesOnScreen();
  if (scrollRange)
  {
    int scrollRatio = _doc->GetLineCount()/scrollRange;
    int visStart = scrollRatio * scrollPos;
    int visEnd = visStart+linesOnScreenVisible;

    if (_lineNum<visStart||_lineNum>visEnd)
    {
      // For try to have the _linenum, in the middle of the screen
      // this helps to try to help the user figure out where the hell
      // they're relative to everything else
      int middle = _lineNum - (linesOnScreenVisible/2);
      if (middle<0) middle = 0;
      _doc->ScrollToLine(middle); // Scroll to the line       
    }
  }

  // Control text area need to be updated with the new line.
  _doc->Refresh();
}

void DocumentViewer::StateChange(DebugScript*focus){}
void DocumentViewer::SimulateUpdate(float deltaT){}

bool DocumentViewer::IsEngineFileSameAsOpenFile()
{
  return _engineFileCRC==_rawFileCRC;
}

#if !_VBS3_SD_IMP_BREAKPOINTS
bool DocumentViewer::IsBreakPointAt(int line)
{
  for (size_t i=0; i<_breakPoints.size(); i++)
    if (line==-1||_breakPoints[i]._sourceLineNumber==line)
      return true;

  return false;
}


bool RemoveBreapPoints(DebugScript *script, DWORD engineBPNum )
{
  if (script && script->GetIDebugScript())
    script->GetIDebugScript()->RemoveBreakpoint(engineBPNum);
  return false;
}
#endif

#if _VBS3_SD_IMP_BREAKPOINTS
void DocumentViewer::RemoveBreakpoint(DWORD bpId)
#else
void DocumentViewer::RemoveBreakPoint(int line)
#endif
{
#if _VBS3_SD_IMP_BREAKPOINTS
  ScriptBreakpoint& bp = GBreakpointManager.GetBreakpoint(bpId);
  
  _doc->MarkerDeleteHandle(bp._handle); //remove the marker
  GBreakpointManager.RemoveBreakpoint(bpId); //remove the bp from the engine
#if _SD_LIVEFOLDER_IMP
  for (size_t i=0; i<_breakPoints.size(); ++i)
  {
    if (_breakPoints[i]==bpId)
    {
      _breakPoints.erase(_breakPoints.begin()+i);
      _bpUpdates.erase(_bpUpdates.begin()+i); //_breakPoints and _bpUpdates should match in contents
      break;
    }
  }
#endif
#else
  for (size_t i=0; i<_breakPoints.size(); i++)
  {
    if (line==-1||_breakPoints[i]._sourceLineNumber==line)
    {
      _doc->MarkerDeleteHandle(_breakPoints[i]._handle);
      // Tell each, debug script to remove the select break point
      // if they've registered it.
      GFrame->ForEachDebugScript(RemoveBreapPoints,_breakPoints[i]._engineBPNum);

      _breakPoints.erase(_breakPoints.begin()+i);
      --i;
      if (line!=-1) return;
    }
  }
#endif
}


#if !_VBS3_SD_IMP_BREAKPOINTS
DWORD GBPNum = 0;
BreakPointLine::BreakPointLine(int handle, int lineNumber)
{
    _handle = handle;
    _sourceLineNumber = lineNumber;
    _absSouceLineNumber = -1; // needs to be resolved so the engine can break on the file
    _engineBPNum = GBPNum++;
}
#endif

void DocumentViewer::ToggleCurrentLineBP()
{
  if (!_doc) return;
  SetBreakPointOnLine(_doc->LineFromPosition(_doc->GetCurrentPos()));
}

#if !_VBS3_SD_IMP_BREAKPOINTS
bool AllDebugScript(DebugScript *script, int ignore)
{
  if (script && script->GetIDebugScript())
    GDocumentManager->SetAllBreakPoints(script->GetIDebugScript());

    return false; // Keep going
}
#endif

void DocumentViewer::SetBreakPointOnLine(int line)
{  
  if (!_doc) return;

  if (_rawFileCRC!=_engineFileCRC && _docView&DOC_ORIGINAL)
  {
#if _SD_ENGINE_CRC_RELOAD
    GDocumentManager->OpenEngineFile(_relativePath, this);
    if (_rawFileCRC!=_engineFileCRC)
    {
#endif
#if _SD_LIVEFOLDER_IMP
  #if _VBS_15079_RESTRICT_BISIM_SCRIPTS
      wxString msg(L"Cannot place a breakpoint, the current file and engine file do not match. If you have recently modified the script file, repack the addon or reload the mission if this is a mission script.");
  #else
      wxString msg(L"Cannot place a breakpoint, the current file and engine file do not match. Right click the document tab, and select engine file to place a break point.");
  #endif
      PlaySound(TEXT("SystemExclamation"),NULL,SND_ALIAS|SND_ASYNC);
      GFrame->GetStatusBar()->SetStatusText(msg,0);
#else
#if _VBS_15079_RESTRICT_BISIM_SCRIPTS
      wxMessageBox(L"Cannot place a breakpoint, the current file and engine file do not match. If you have recently modified the script file, repack the addon or reload the mission if this is a mission script.");
#else
      wxMessageBox(L"Cannot place a breakpoint, the current file and engine file do not match. Right click the document tab, and select engine file to place a break point.");
#endif
#endif //_SD_LIVEFOLDER_IMP
      return;
#if _SD_ENGINE_CRC_RELOAD
    }
#endif
#if _SD_LIVEFOLDER_IMP
  }
  else if (_dirty)
  {
    PlaySound(TEXT("SystemExclamation"),NULL,SND_ALIAS|SND_ASYNC);
    GFrame->GetStatusBar()->SetStatusText(L"Cannot edit breakpoints while the file is dirty. Save the file to modify breakpoints.",0);
    return;
#endif
  }


#if _VBS3_SD_IMP_BREAKPOINTS
  std::string docPath;
  std::string enginePath = GBreakpointManager.GetEngineBPPath(_relativePath, _filePath);
  if (_relativePath.Length()) 
    docPath = UTF16ToChar(_relativePath);
  else 
    docPath = UTF16ToChar(_filePath);

  DWORD engineBpId = GBreakpointManager.GetBreakpointId(enginePath.c_str(), line);
  if (engineBpId != -1) //the BP already exists, remove it
#if _SD_LIVEFOLDER_IMP
  {
    GFrame->GetStatusBar()->SetStatusText(wxString::Format(L"Breakpoint removed on line : %i",line+1),0);
    return RemoveBreakpoint(engineBpId);
  }
#else
    return RemoveBreakpoint(engineBpId);
#endif
#else
  // Check for existing break point to remove it
  if (IsBreakPointAt(line)) return RemoveBreakPoint(line);
#endif

  // Nothing found add a new marker
  int markerHandle = _doc->MarkerAdd(line,wxSCI_MARK_CIRCLE);
#if _VBS3_SD_IMP_BREAKPOINTS
  int absSouceLineNumber = _enginePPFileResolver.ConvertSoureLineToAbs(line, docPath); //the PP uses doc paths
  engineBpId = GBreakpointManager.AddBreakpoint(enginePath.c_str(), docPath.c_str(), markerHandle, line, absSouceLineNumber, true);
  _breakPoints.push_back(engineBpId);
#if _SD_LIVEFOLDER_IMP
  _bpUpdates.push_back(ScriptBreakpoint(GBreakpointManager.GetBreakpoint(engineBpId))); //add a copy to pending bp updates as well for live-folder changes
#endif
#else
  _breakPoints.push_back(BreakPointLine(markerHandle,line));

  // Update the DocumentManager.
  GFrame->ForEachDebugScript(AllDebugScript,0);
#endif
#if _SD_LIVEFOLDER_IMP
  GFrame->GetStatusBar()->SetStatusText(wxString::Format(L"Breakpoint added on line : %i",line+1),0);
#endif
}

#if _VBS3_SD_IMP_BREAKPOINTS
void DocumentViewer::RemoveAllBreakpoints()
{
  for (std::vector<DWORD>::iterator it = _breakPoints.begin(); it != _breakPoints.end(); ++it)
  {
    ScriptBreakpoint& bp = GBreakpointManager.GetBreakpoint(*it);
    if (bp._engineBPNum != -1) //make sure the bp is valid
    {
      _doc->MarkerDeleteHandle(bp._handle);
    }
    GBreakpointManager.RemoveBreakpoint(*it);
  }
}
#endif

void DocumentViewer::OnMarginClick( wxScintillaEvent& event )
{
  if (_doc)
  {
    int lineClick = _doc->LineFromPosition( event.GetPosition() );
    switch (event.GetMargin())
    {
      case COL_DEBUG_POINT:
        SetBreakPointOnLine(lineClick);
        break;
      case COL_LINE_NUMBERS:
        break;
      case COL_FOLDING_MARGIN:
        {
          int levelClick = _doc->GetFoldLevel( lineClick );
          if ( ( levelClick & wxSCI_FOLDLEVELHEADERFLAG ) > 0 )
          {
            _doc->ToggleFold( lineClick );
          }
        }
        break;
    }
  }
  event.Skip();
}

#if !_VBS3_SD_IMP_BREAKPOINTS
std::vector<BreakPointLine>& DocumentViewer::GetBreakPoints()
{
  // The break point needs to be converted from relative position to absolute position so the engine can break at the correct line.
  std::string path;
  
  if (_relativePath.Length()) 
    path = UTF16ToChar(_relativePath);
  else 
    path = UTF16ToChar(_filePath);

  for (size_t i=0; i<_breakPoints.size(); i++)   
   _breakPoints[i]._absSouceLineNumber = _enginePPFileResolver.ConvertSoureLineToAbs(_breakPoints[i]._sourceLineNumber,path);
  
  return _breakPoints;
}
#endif
