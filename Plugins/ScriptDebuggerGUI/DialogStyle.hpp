#pragma once

#include <stdio.h>

#include <Windows.h>
#include <iostream>

#include <string>
#include <vector>
#include <fstream>

#include "../../Common/Essential/LexicalAnalyser.hpp"
#include "../../Common/Essential/SyntaxAnyaliser.hpp"
#include "../../Common/Essential/Config.hpp"

// #include the form builder, that we're going to extend
#include "ui/FormBuilderGUI.h"


class DialogStyleEx : public DialogStyle
{
  typedef DialogStyle base;
  bool _okClicked;

  typedef std::pair<long,int> LBStyleIDPair;
  std::vector< LBStyleIDPair > _listBoxPair;

  static std::vector<wxString> _fontList;
public:
  /// Create the DialogStyleEx window
  /// Also used to populate the available fonts
  DialogStyleEx(wxWindow *parent);
  /// Close setting to verify the new settings
  void CloseSetting(wxCloseEvent &event);

  /// Control updated, push the update to scintilla
  void OnControlsUpdates();
  
  /// Pulls, from the GUI, and updates the state.
  void PullFromGUI(int style);
  /// Pushes the selected style to the GUI
  void PushToGUI(int style);
  /// Update the UI, with the default selections.
  void InitDefaultSelection();

  void OnListItemClicked(wxListEvent &event);
  void ColourPickerEvent(wxColourPickerEvent&);

  void EventFontNameChoice(wxCommandEvent& event);
  void EventFontSizeChoice(wxCommandEvent& event);

  void EventCheckBox(wxCommandEvent &event);

  void OnButtonOk(wxCommandEvent& event);
  void OnButtonCancel(wxCommandEvent& event);

  DECLARE_EVENT_TABLE()
};
