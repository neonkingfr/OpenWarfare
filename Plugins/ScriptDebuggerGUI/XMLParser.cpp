#include "XMLParser.hpp"

WxStringTagCleaner::WxStringTagCleaner()
{
  _insideTag = false;
  _insideSymbol = false;

  _currTag.Alloc(128);
  _currSymbol.Alloc(8);
  _currText.Alloc(2048);
  _result.Alloc(2048);
};

//Protected methods
void WxStringTagCleaner::onDocumentStart()
{
  onElementStart();
};

void WxStringTagCleaner::onDocumentEnd()
{
  onElementEnd();
};

void WxStringTagCleaner::onElementStart()
{
  _currText.clear();
};

void WxStringTagCleaner::onElementEnd()
{
  _result.Append(_currText);
};

void WxStringTagCleaner::onTagStart()
{
  _insideTag = true;
  _currTag.clear();
  onElementEnd();
};

void WxStringTagCleaner::onTagEnd()
{
  _insideTag = false;
  onElementStart();
};

void WxStringTagCleaner::onSymbolStart()
{
  _insideSymbol = true;
  _currSymbol.clear();
};

void WxStringTagCleaner::onSymbolEnd()
{
  _insideSymbol = false;
  if (_currSymbol.Cmp( wxT("amp") ))
    _currText.Append(wxT("&"));
  else if (_currSymbol.Cmp( wxT("lt") ))
    _currText.Append(wxT("<"));
  else if (_currSymbol.Cmp( wxT("gt") ))
    _currText.Append(wxT(">"));
  else if (_currSymbol.Cmp( wxT("quot") ))
    _currText.Append(wxT("\""));
  else if (_currSymbol.Cmp( wxT("apos") ))
    _currText.Append(wxT("\'"));
};

void WxStringTagCleaner::onTextChar(wxChar c)
{
  if (_insideTag)
  {
    _currTag.Append(c);
  }
  else if (_insideSymbol)
  {
    _currSymbol.Append(c);
  }
  else
  {
    _currText.Append(c);
  }
};

//public methods
wxString WxStringTagCleaner::Parse(wxString &input)
{  
  onDocumentStart();
  for (wxString::iterator i = input.begin(); i != input.end(); ++i)
  {
    switch (*i)
    {
    case '<':
      {
        onTagStart();
        break;
      }
    case '>':
      {
        onTagEnd();
        break;
      }
    case '&':
      {
        onSymbolStart();
        break;
      }
    case ';':
      {
        if (_insideSymbol)
        {
          onSymbolEnd();
        }
        else
        {
          onTextChar(*i);
        };
        break;
      }
    default:
      {
        onTextChar(*i);
      }
    }
  }
  onDocumentEnd();
  return _result;
};