#include "DocumentTabManager.hpp"
#include "DocumentManager.hpp"
#include "MainWindow.hpp"
#include "wx/clipbrd.h"
#include "UIEvents.hpp"

// This is ONLY from the GUI, as such we take the closing request and then process it through
// our internal structure, that does not notify that its about to close.
BEGIN_EVENT_TABLE(DocumentTabManager, wxEvtHandler)
EVT_MENU(wxID_ANY, DocumentTabManager::OnContextMenu)
EVT_FLATNOTEBOOK_PAGE_CLOSING(wxID_ANY, DocumentTabManager::OnPageClosing )
EVT_FLATNOTEBOOK_PAGE_CHANGED(wxID_ANY,DocumentTabManager::OnPageChanged)
EVT_CLOSE(DocumentTabManager::OnClose)
END_EVENT_TABLE()

//EVT_FLATNOTEBOOK_PAGE_CLOSED(wxID_ANY, DocumentTabManager::PageClosed )
//EVT_RIGHT_DOWN(DocumentTabManager::OnRightDown)

DocumentTabManager::DocumentTabManager(wxFlatNotebook *noteBook)
{
  _flatNoteBook = noteBook;
  _flatNoteBook->PushEventHandler(this);
  _flatNoteBook->GetPages()->Connect(wxEVT_RIGHT_DOWN,wxMouseEventHandler(DocumentTabManager::OnRightDown),NULL,this);
}

void DocumentTabManager::OnClose(wxCloseEvent &event)
{
  _flatNoteBook->GetPages()->Disconnect(wxEVT_RIGHT_DOWN,wxMouseEventHandler(DocumentTabManager::OnRightDown),NULL,this);
}

DocumentTabManager::~DocumentTabManager()
{
#if _VBS_15068_PROPER_SHUTDOWN
  _flatNoteBook->DeleteAllPages(); //not deleting all pages causes the fnb to send events to already destroyed widgets
  _flatNoteBook->RemoveEventHandler(this);
#endif  
}

void DocumentTabManager::OnContextMenu(wxCommandEvent &event)
{
  wxMenu *menu = (wxMenu *) event.GetEventObject();

  DocumentViewer *view = (DocumentViewer*) menu->GetClientData();
#if _VBS_14850_SHOW_FULL_FILE_PATH
  if (event.GetId() == TAB_DOC_COPY_FULL_PATH)
  {
    wxClipboard clipboard;
    if (clipboard.Open())
    {
      clipboard.SetData( new wxTextDataObject(GDocumentManager->GetCurrentDocument()->GetFilePath()) );
      clipboard.Flush(); //enables the OS to use the clipboard data
      clipboard.Close(); //release the clipboard object
    }
  }
  else
#endif
    view->SetDocView((1<<event.GetId()));
}

void DocumentTabManager::SetTabName(DocumentViewer *doc)
{
#if _VBS_15079_RESTRICT_BISIM_SCRIPTS
  wxString docName;
  //only show "Internal script" for all restricted files (VM should still show "Virtual Machine"
  if (doc->ContentsProtected() && !(doc->GetDocContents() & DOC_VM))
  {
    docName = L"Internal Script";
  }
  else
  {
#else
    wxString docName = doc->GetName();
#endif
    const TCHAR *viewName = doc->GetDocViewName();
#if _SD_ENGINE_FILE_NOSAVE
    //only show dirty flag when the file is dirty and we're looking at the original content, not engine
    const TCHAR *dirtyStr = doc->IsDirty() && (doc->GetDocView()&DOC_ORIGINAL || doc->GetDocView()&DOC_ORIGINAL_PP) ? L"*" : L"";
#else
    const TCHAR *dirtyStr = doc->IsDirty() ? L"*" : L"";
#endif
#if _VBS_15079_RESTRICT_BISIM_SCRIPTS
    docName = wxString::Format(L"%s %s%s",viewName,dirtyStr,doc->GetName().GetData());
  }
#else
  docName = wxString::Format(L"%s %s%s",viewName,dirtyStr,docName.GetData());
#endif

  int tabIndex = _flatNoteBook->GetPageIndex(doc->GetDocument());
  _flatNoteBook->SetPageText(tabIndex,docName);
}

void DocumentTabManager::OnRightDown(wxMouseEvent &event)
{
  wxPageInfo pgInfo;
	int tabIdx;
  int where = _flatNoteBook->GetPages()->HitTest(event.GetPosition(),pgInfo,tabIdx);
	switch(where)
  {
	  case wxFNB_TAB:
	  case wxFNB_TAB_X:
      {
        // Get the document tab
        DocumentViewer *doc = GDocumentManager->GetDocumentViewer(_flatNoteBook->GetPage(tabIdx));

        //  Set the menu viewer as the client data
        wxMenu menu;
        menu.SetClientData(doc);

        // Get the updated menu from the document
        doc->PopulateWxMenu(&menu);
        
        // Popup the menu from the flat notebook, its going to recieve the event
        _flatNoteBook->GetPages()->PopupMenu(&menu,event.GetPosition());          
      }
      break;
  }  
}

void DocumentTabManager::OnPageChanged(wxFlatNotebookEvent &event)
{
  int selection = event.GetSelection();
  DocumentViewer *doc = GDocumentManager->GetDocumentViewer(_flatNoteBook->GetPage(selection));

  GDocumentManager->DocumentTabSelected(doc);

#if _VBS_14850_SHOW_FULL_FILE_PATH
  if (GFrame)
    GFrame->ToggleTitleWindow();
#endif
  
  event.Skip();
}

void DocumentTabManager::OnPageClosing(wxFlatNotebookEvent &event)
{
  int selection = event.GetSelection();
  DocumentViewer *doc = GDocumentManager->GetDocumentViewer(_flatNoteBook->GetPage(selection));
  
  // Veto, closing the tab, if its locked.
  if (doc->GetDocContents()&DOC_VM) return event.Veto();

  // Close the document
  GDocumentManager->TabCloseingDocument(doc);

  // Continue normal proccessing
  event.Skip();
}



void DocumentTabManager::AddTab(DocumentViewer *document)
{
  Assert(document);
  _flatNoteBook->AddPage(document->GetDocument(),document->GetName());
}

wxWindow *DocumentTabManager::SelectedTab()
{
  return _flatNoteBook->GetCurrentPage();
}



void DocumentTabManager::BringIntoFocus(DocumentViewer *doc)
{
  int tabIndex = _flatNoteBook->GetPageIndex(doc->GetDocument());
  if (tabIndex!=-1) _flatNoteBook->SetSelection(tabIndex);
}

void DocumentTabManager::RemoveTab(DocumentViewer *doc)
{
  int tabIndex = _flatNoteBook->GetPageIndex(doc->GetDocument());
  if (tabIndex!=-1) _flatNoteBook->DeletePage(tabIndex,false);
}


void DocumentTabManager::SetTabColor( DocumentViewer *doc,const wxColour& colour)
{
  int tabIndex = _flatNoteBook->GetPageIndex(doc->GetDocument());
  if (tabIndex!=-1) _flatNoteBook->SetTabAreaColour(colour);
}

void DocumentTabManager::ResetTabColor(DocumentViewer *doc)
{
  SetTabColor(doc,wxSystemSettings::GetColour(wxSYS_COLOUR_BTNFACE));
}

SmartRef< DocumentTabManager > GDocumentTabManager;
