#pragma once

#include "wx/wx.h"
#include "wx/image.h"
#include "wx/xrc/xmlres.h"
#include "wx/notebook.h"
#include "wx/richtext/richtextctrl.h"
#include "wx/event.h"

#include "wx/stdpaths.h"
#include "wx/filedlg.h"
#include "wx/dirdlg.h"
#include "wx/timer.h"
#include "wx/taskbar.h"
#include "wx/statbmp.h"
#include "wx/aui/auibook.h"


wxWindow* FindIDC(wxWindow *window, int idc);

/**
  * Template class used for wxTypes. Seeing that UI are normally full of 
  * Controls that need to be found from the parent with wxWidgets this just makes
  * control and managing many different controls easier.
 */
template<class TYPE>
class wxType
{
  TYPE *_control;
public:
  wxType():_control(NULL){}
  void FindCtrl(wxWindow *parent, const char *ctrlName)
  {
    wxWindow *window = FindIDC(parent,wxXmlResource::GetXRCID(wxT(ctrlName)));
    _control = dynamic_cast<TYPE*>(window);
  }
  TYPE *GetRef()
  {
    return _control;
  }
  TYPE *operator->()
  {
    return _control;
  }
  TYPE *operator&()
  {
    return _control;
  }
  operator TYPE *() const
  {
    return _control;
  }
  operator bool() const
  {
    return _control != NULL;
  }
};

//! All windows that wish to be simulated, inherit from this base
//! class. They then each implement their own simulation
class WindowSimulator
{
public:
  virtual void Simulate(float deltaT) = 0;
  virtual void UpdateView(){};
};