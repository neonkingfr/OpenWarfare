#include "DialogStyle.hpp"
#include "ImgManager.hpp"
#include "resource.h"
#include "settings.hpp"
#include "Util.hpp"

#include "Document.hpp"
#include "DocumentManager.hpp"

// Include the wxScintilla document
#include <wx/wxScintilla/wxscintilla.h>

#include <algorithm>

BEGIN_EVENT_TABLE(DialogStyleEx,DialogStyle)

// List clicked...
EVT_LIST_ITEM_SELECTED(wxID_ANY, OnListItemClicked)

// Color picker update
//typedef void (wxEvtHandler::*wxColourPickerEventFunction)(wxColourPickerEvent&);
EVT_COLOURPICKER_CHANGED(wxID_ANY, ColourPickerEvent)

EVT_CHOICE(IDC_FONT_NAME, EventFontNameChoice)
EVT_CHOICE(IDC_FONT_SIZE, EventFontSizeChoice)

EVT_CHECKBOX(wxID_ANY,EventCheckBox)



EVT_COMMAND(IDC_STYLE_OK,wxEVT_COMMAND_BUTTON_CLICKED,OnButtonOk)
EVT_COMMAND(IDC_STYLE_CANCEL,wxEVT_COMMAND_BUTTON_CLICKED,OnButtonCancel)
EVT_CLOSE(CloseSetting)
END_EVENT_TABLE()

typedef wxString generic_string;
using namespace std;


int CALLBACK EnumFontFamExProc(ENUMLOGFONTEX *lpelfe, NEWTEXTMETRICEX *s, int d, LPARAM lParam) 
{
	vector<generic_string> *pStrVect = (vector<generic_string> *)lParam;

  size_t vectSize = pStrVect->size();

	for(int i = vectSize - 1 ; i >= 0 ; i--) 
  {
		if ( !lstrcmp((*pStrVect)[i].c_str(), (const TCHAR *)lpelfe->elfLogFont.lfFaceName) )
			return 1;	//we already have seen this typeface, ignore it
	}

  BYTE wantedFont = TMPF_VECTOR|TMPF_FIXED_PITCH;
  BYTE fontFamily = lpelfe->elfLogFont.lfPitchAndFamily;
  if (!fontFamily||fontFamily&wantedFont) 
	  pStrVect->push_back(lpelfe->elfLogFont.lfFaceName);
	
  return 1; // I want to get all fonts
}

std::vector<wxString> DialogStyleEx::_fontList;

#define MAX_STATES 32
SmartRef<StyleLoader> GStylePrevState[32];
SmartRef<StyleLoader> GCurrentState[32];


struct FontSize
{
  int Size;
  const TCHAR *Text;
};
const FontSize GFontSize[]  = 
{
  {0,L""}
  ,{8,L"8"}
  ,{9,L"9"}
  ,{10,L"10"}
  ,{11,L"11"}
  ,{12,L"12"}
  ,{14,L"14"}
  ,{16,L"16"}
  ,{18,L"18"}
  ,{20,L"20"}
  ,{22,L"22"}
  ,{24,L"24"}
  ,{26,L"26"}
  ,{28,L"28"}
};

// Pull the current state into the store state..
StyleLoader *StyleLoader::PullScintillaStyleState(wxScintilla *doc)
{
  _fgcolor = UTF16ToChar(doc->StyleGetForeground(_styleID).GetAsString());
  _bkcolor = UTF16ToChar(doc->StyleGetBackground(_styleID).GetAsString());
  _fontName = UTF16ToChar(doc->StyleGetFaceName(_styleID));

  _fontSize = doc->StyleGetSize(_styleID);
  _bold = doc->StyleGetBold(_styleID);
  _italic = doc->StyleGetItalic(_styleID);
  _underline = doc->StyleGetUnderline(_styleID);

  return this;
}

// Push the current state
StyleLoader *StyleLoader::PushScintillaStyleState(wxScintilla *doc)
{
  wxString fgColor = CharToUTF16(_fgcolor);
  wxString bgColor = CharToUTF16(_bkcolor);

  // Set the foreground and background color.
  wxColour wxColFG;wxColFG.Set(fgColor);
  wxColour wxColBG;wxColBG.Set(bgColor);
  doc->StyleSetForeground(_styleID,wxColFG);
  doc->StyleSetBackground(_styleID,wxColBG);

  // Set the necessary states  
  doc->StyleSetFaceName(_styleID,CharToUTF16(_fontName));
  doc->StyleSetSize(_styleID,_fontSize);
  doc->StyleSetBold(_styleID,_bold);
  doc->StyleSetItalic(_styleID,_italic);
  doc->StyleSetUnderline(_styleID,_underline);

  return this;
}



DialogStyleEx::DialogStyleEx( wxWindow *parent ):base(parent)
{
  _okClicked = false;

  // Populate the available id's
  // GStyleRemapper
  for (int i=0; i<MAX_CONFIG_STYLES; i++)
  {
    wxListItem info;
    info.m_text = CharToUTF16(GStyleRemapper[i].ConfigName);
    info.m_mask = wxLIST_MASK_TEXT|wxLIST_MASK_DATA;
    info.m_data = GStyleRemapper[i].StyleID;
    info.m_itemId = i;

    // Insert the document into the list
    //GStyleRemapper[i].ListItemID =;
    long longID = TypeSelector->InsertItem(info);
    TypeSelector->SetColumnWidth(0,wxLIST_AUTOSIZE);

    _listBoxPair.push_back( LBStyleIDPair(longID,GStyleRemapper[i].StyleID) );
  }

  // Refresh the list control
  TypeSelector->Refresh();


  // Do not load from settings, the values will all-ready exist from the virtual machine doc.
  DocumentViewer *vmView = GDocumentManager->GetVirtualMachineDoc();
  for (int i=0; i<MAX_STATES; i++)
  {
    StyleLoader *style0 = new StyleLoader(i);
    StyleLoader *style1 = new StyleLoader(i);
    style0->PullScintillaStyleState(vmView->GetDocument());
    style1->PullScintillaStyleState(vmView->GetDocument());

    GStylePrevState[i] = style0;
    GCurrentState[i] = style1;
  }

  // Add it to the available fonts.
  if (!_fontList.size())
  {
    _fontList.push_back(L"");

    LOGFONT lf;
    lf.lfCharSet = DEFAULT_CHARSET;
    lf.lfFaceName[0]='\0';
    lf.lfPitchAndFamily = 0;
    HDC hDC = ::GetDC((HWND)GetHWND()); // get the HWND of the window

    // Enumerate the number of fonts available
    ::EnumFontFamiliesEx(hDC, &lf, (FONTENUMPROC) EnumFontFamExProc, (LPARAM) &_fontList, 0);
      // Sort the list
    std::sort(_fontList.begin(),_fontList.end());
  }

  // Add in the FontNames, and font sizes. These dont change!
  for (size_t i=0; i<_fontList.size(); i++)
    TypeFontType->Insert(_fontList[i],i);
  
  int NFontSize = sizeof(GFontSize)/sizeof(GFontSize[0]);
  for (int i=0; i<NFontSize; i++)
    TypeFontSize->Insert(GFontSize[i].Text,i);

  // Set the default icon, should of been inherited somewhere it is very common
  wxIcon icon;
  if (GImgManager.GetWxIcon(ICON_TRAY,icon))  SetIcon(icon);

  // Select the first style, that is the default.
  long firstItem = _listBoxPair[0].first;
  TypeSelector->SetItemState(firstItem, wxLIST_STATE_SELECTED, wxLIST_STATE_SELECTED);

  // Show it
  Show();
}


bool PushToDoc(DocumentViewer *view, StyleLoader *styleLoader)
{
  styleLoader->PushScintillaStyleState(view->GetDocument());
  return false;
}

// Pulls, from the GUI, and update the style state
void DialogStyleEx::PullFromGUI(int style)
{
  // The style for current styles.
  StyleLoader *currentStyle = GCurrentState[style];
  
  // Keep the default, style dont overwrite it with a pull state...
  if (TypeFontType->GetSelection()!=0) currentStyle->_fontName = UTF16ToChar(_fontList[TypeFontType->GetSelection()]);

  currentStyle->_fontSize = GFontSize[TypeFontSize->GetSelection()].Size;
  currentStyle->_bkcolor = UTF16ToChar(TypeGroundColour->GetColour().GetAsString());
  currentStyle->_fgcolor = UTF16ToChar(TypeForgroundColour->GetColour().GetAsString());

  // Set the bold, italic, underline properties
  currentStyle->_bold = TypeFontBold->GetValue();
  currentStyle->_italic = TypeFontItalic->GetValue();
  currentStyle->_underline = TypeFontUnderline->GetValue();

  // push he new style to all documents.
  GDocumentManager->ForEachDocumentView(PushToDoc,currentStyle);
}

/*
 Do some validation on the window close.
*/
void DialogStyleEx::CloseSetting( wxCloseEvent &event )
{ 
  // Do the saving here...
  if (_okClicked)
  {
    // Do a dirty check
    for (int i=0; i<MAX_CONFIG_STYLES; i++)
    {
      // Save the current style, class name
      int styleIndex = GStyleRemapper[i].StyleID;
      GCurrentState[styleIndex]->_className = GStyleRemapper[i].ConfigName;
      GSettings.SetStyleLoader(GCurrentState[styleIndex]);
    }

    // Call the save config file.
    GSettings.SaveConfigFile();
  }
  else
  {
#if _SD_DEFAULT_SCI_STYLE
    //More efficient to just iterate over the actually used styles
    for (int i=0; i<MAX_CONFIG_STYLES; i++)
    {
      // Save the current style, class name
      int styleIndex = GStyleRemapper[i].StyleID;
      GCurrentState[styleIndex] = GStylePrevState[styleIndex];     
      GDocumentManager->ForEachDocumentView(PushToDoc,GCurrentState[styleIndex]);
    }
#else
    // Push the previous state, to the current state.
    for (int i=0; i<MAX_STATES; i++)
    {
      // Set the previous state as the current state.
      GCurrentState[i] = GStylePrevState[i];     
      GDocumentManager->ForEachDocumentView(PushToDoc,GCurrentState[i]);
    }
#endif
  }

  event.Skip();
}



// Need to verify, if the style is actually in the drop down menu.
// The font size, should bet there also. 
void DialogStyleEx::PushToGUI(int style)
{
  // The style for current styles.
  StyleLoader *currentStyle = GCurrentState[style];
  
  // Select the font name
  wxString fontName = CharToUTF16(currentStyle->_fontName);
  int index = TypeFontType->FindString(fontName);
  if (index==-1) TypeFontType->Select(0);
  else TypeFontType->Select(index);

  // Select the font size, if one is available.
  int fontSize = currentStyle->_fontSize;
  int nFontSize = sizeof(GFontSize)/sizeof(GFontSize[0]);
  for (int i=0; i<nFontSize; i++)
  {
    if (fontSize==GFontSize[i].Size)
    {
      TypeFontSize->Select(i);
      break;
    }
  }

  TypeGroundColour->SetColour(CharToUTF16(currentStyle->_bkcolor));
  TypeForgroundColour->SetColour(CharToUTF16(currentStyle->_fgcolor));

  // Set the bold, italic, underline properties
  TypeFontBold->SetValue(currentStyle->_bold);
  TypeFontItalic->SetValue(currentStyle->_italic);
  TypeFontUnderline->SetValue(currentStyle->_underline);
}

void  DialogStyleEx::InitDefaultSelection()
{
  TypeFontBold->SetValue(false);
  TypeFontItalic->SetValue(false);
  TypeFontUnderline->SetValue(false);
}

// Pull from the GUI, and then update the scintilla document!
void DialogStyleEx::OnControlsUpdates()
{
  for (int i=0; i<TypeSelector->GetItemCount(); i++)
    if (TypeSelector->GetItemState(i,wxLIST_STATE_SELECTED)==wxLIST_STATE_SELECTED) 
      return PullFromGUI(TypeSelector->GetItemData(i));
}

void DialogStyleEx::EventFontNameChoice(wxCommandEvent& event)
{
  OnControlsUpdates();
  event.Skip();  
}
void DialogStyleEx::EventFontSizeChoice(wxCommandEvent& event)
{
  OnControlsUpdates();
  event.Skip();
}

void DialogStyleEx::EventCheckBox(wxCommandEvent &event)
{
  OnControlsUpdates();
  event.Skip();
}

void DialogStyleEx::ColourPickerEvent(wxColourPickerEvent &event)
{
  OnControlsUpdates();
  event.Skip();
}

// Style selected, update all the drop downs and list.
void DialogStyleEx::OnListItemClicked(wxListEvent &event)
{
  for (int i=0; i<TypeSelector->GetItemCount(); i++)
    if (TypeSelector->GetItemState(i,wxLIST_STATE_SELECTED)==wxLIST_STATE_SELECTED)
      return PushToGUI(TypeSelector->GetItemData(i));

  event.Skip();
}


void DialogStyleEx::OnButtonOk(wxCommandEvent& event)
{
  // Try to save
  _okClicked = true;

  Close();
  event.Skip();
}

void DialogStyleEx::OnButtonCancel(wxCommandEvent& event)
{

  Close();
  event.Skip();
}