#pragma once

#include <wx/listctrl.h>

#include "../../Common/Essential/RefCount.hpp"
#include "../../Common/Essential/AppFrameWork.hpp"

#include "Document.hpp"

class DocumentTreeListManager : public wxEvtHandler, public RefCount
{
  wxListCtrl *_listCtrl;
  bool _showFileNames;
public:
  DocumentTreeListManager(wxListCtrl *listCtrl);

  void AddDocumentToList(DocumentViewer *doc);
  void RemoveDocument(DocumentViewer *doc);

  wxListCtrl *GetListCtrl(){return _listCtrl;}

  /// Toggle displaying the full path name or the file name
  void ToggleDisplayName();

void OnContextMenu(wxCommandEvent &event);

  void OnListDoubleClick(wxListEvent &event);
  void OnMouseRightUp(wxMouseEvent &event);  


  DECLARE_EVENT_TABLE()
};

extern SmartRef< DocumentTreeListManager > GDocumentTreeManager;
