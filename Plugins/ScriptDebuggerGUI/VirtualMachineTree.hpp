#pragma once

#include <vector>

#include "wx/treectrl.h"

#include "../../Common/Essential/RefCount.hpp"

#include "DebugScript.hpp"
#include "wxUtil.hpp"
#include "DeveloperDriveTree.hpp"


struct TreeItemSync
{
  SmartRef<DebugScript> _script;
  wxTreeItemId _item;
};

#if _VBS3_DEBUG_DELAYCALL
class VMTreeContextMenu : public wxMenu
{
public:
  VMTreeContextMenu() {script = NULL;};
  DebugScript* script;
};

class VirtualMachineTree : public wxEvtHandler, public RefCount, public iViewWindow
#else
class VirtualMachineTree : public wxEvtHandler, public RefCount
#endif
{
  bool GetTreeItemId( DebugScript *script, wxTreeItemId &item);

  void SetTreeItemImg(wxTreeItemId &item, int imgId, wxBitmapType type = wxBITMAP_TYPE_PNG );
#if _VBS3_DEBUG_DELAY_UI_CALL
  wxString GetDelayCallText(DebugDelayCall* call) const;
#endif
#if _VBS3_DEBUG_DELAYCALL
  void UpdateDelayCallName(int itemIndex);
#endif
public:
  /// MainFrame Controls
  wxTreeCtrl *_controlTree;
  
  /// Main elements
  wxTreeItemId _treeRoot;
  wxTreeItemId _scriptVMParent;
#if _VBS3_DEBUG_DELAYCALL
  wxTreeItemId _delayCallParent;
#endif
  wxTreeItemId _eventParent;
  wxTreeItemId _errorParent;

  std::vector<TreeItemSync> _items;
  wxTreeItemId  _focus;
  

  VirtualMachineTree(wxTreeCtrl *tree);
#if _VBS3_DEBUG_DELAYCALL
  virtual ~VirtualMachineTree();
#endif

  /**
    * Set's the parameter focus to the focused grid item and 
    * true is returned. If no grid item is in focus then false is returned.
    */
  bool GetCurrentFocus(wxTreeItemId &focus);
  /// Access from TreeItemId to data item
  TreeItemSync *GetItem(wxTreeItemId &treeItem);


  void SelectTreeScript(DebugScript *);
  void UpdateDocumentViewer(wxTreeItemId);

  /// Public functions
  void AddNewControlTreeItem(DebugScript *script, wxString name);
  void RemoveControlTreeItem(DebugScript *script);

  wxTreeItemId& GetScriptTreeItem(DebugScript *script);

#if _VBS3_DEBUG_DELAYCALL
  void SimulateUpdate(float dt);
  void ForceUpdate(DebugScript* script);
  void DelayBreakChanged(DebugScript* script);
#endif

  /// Break point leaving and entering scripts update the
  /// tree icon to reflect the current executed item.
  void BreakPointEnter(DebugScript *script);
  void BreakPointLeave(DebugScript *script);

  /// Events, in constructor used Connect
  void OnMouseLeftDoubleClick(wxMouseEvent &event);  
  void OnMouseRightUp(wxMouseEvent &event);  
  void OnMouseItemRightClick(wxTreeEvent &event);  
  void OnGetToolTip(wxTreeEvent &event);

  DECLARE_EVENT_TABLE()
};
