#include <string>
#include <map>
#include <vector>
#include <assert.h>
#include "ScriptDebuggerConfig.hpp"
#include "wx/wx.h"
#include "wx/utils.h"
#include <list>

template<typename CType>
struct ACFSMState
{
  int back;
  int shortcut;
  
  int terminal;
  std::map<CType,int> next;

  ACFSMState():
    back(-1),shortcut(-1),terminal(-1)
  {};
};

typedef std::pair<int,int> searchResult;
#define EMPTY_RESULT std::pair<int,int>(-1,-1);

//implementation of the AhoCorasick string search algorithm
template <typename CType, typename SType>
class AhoCorasick
{
private:
  static const int _noState = -1;
  static const int _root = 0;

  int _lastState;

  std::vector<SType> _needles;
  std::vector<ACFSMState<CType>> _states;

  CType ToLower(CType c) const;
  int NextNode(int s, CType c) const;
  int Step(CType c, int s) const;
  void AddWord(SType& word,int wordN);

public:
  AhoCorasick();

  void AddNeedle(const CType * needle);
  void AddNeedle(const SType needle);

  void ClearNeedles();
  void ComputeFSM();
  bool Search(const SType& haystack) const;
  bool Search(const CType * haystack) const;
  void FindAll(SType& haystack, std::vector<searchResult> & result) const;
  searchResult FindNext(SType& haystack, int pos) const;
};


template<typename CType, typename SType>
AhoCorasick<CType,SType>::AhoCorasick() :
  _lastState(1)
{
  _states.push_back(ACFSMState<CType>()); //add root
};

//PRIVATE functions

//if c is an upper-case letter, this returns a lowercase
template<>
inline wxChar AhoCorasick<wxChar, wxString>::ToLower(wxChar c) const
{
  return wxTolower(c);
}

template<>
inline char AhoCorasick<char, std::string>::ToLower(char c) const
{
  if (c >= 65 && c <= 90)
  {
    c+=32;
  }
  return c;
}
  
template<typename CType, typename SType>
CType AhoCorasick<CType, SType>::ToLower(CType c) const
{
  return c;
}

//returns successor of s after reading c
template<typename CType, typename SType>
int AhoCorasick<CType, SType>::NextNode(int s, CType c) const
{
  std::map<CType,int>::const_iterator it = _states[s].next.find(c);
  if (it != _states[s].next.end())
  {
    return it->second;
  };
  return _noState;
};

//one search step -> reading one char from input
template<typename CType, typename SType>
int AhoCorasick<CType, SType>::Step(CType c, int s) const
{
  while (NextNode(s,c) == _noState && s != _root)
  {
    s = _states[s].back;
  };
  if (NextNode(s,c) != _noState)
  {
    s = NextNode(s,c);
  };
  return s;
};

//adds a word to partially built FSM 
template<typename CType, typename SType>
void AhoCorasick<CType, SType>::AddWord(SType& word, int wordN)
{
  //first the forward edges
  int s = 0; //root
  for (size_t i=0;i<word.length();++i)
  {
    CType c = word[i];
    c = ToLower(c);
    if (NextNode(s,c) == -1)
    {
      //the edge is not in the graph    
      //add a new state
      _states.push_back(ACFSMState<CType>());
      _states[s].next[c] = _lastState;
      ++_lastState;
    };
    s = NextNode(s,c);
  };
  _states[s].terminal = wordN; //set this state as terminal for current word
}
//PUBLIC functions

//adds another needle to search for, note that this does not change the FSM directly - ComputeFSM must be called for that
template<typename CType, typename SType>
void AhoCorasick<CType, SType>::AddNeedle(const CType * needle)
{
  SType strNeedle(needle);
  _needles.push_back(strNeedle);
}

template<typename CType, typename SType>
void AhoCorasick<CType, SType>::AddNeedle(SType needle)
{
  _needles.push_back(needle);
}

//removes all needles
template<typename CType, typename SType>
void AhoCorasick<CType, SType>::ClearNeedles()
{
  _needles.clear();
};

//creates the finite-state-machine for reading the input
template<typename CType, typename SType>
void AhoCorasick<CType, SType>::ComputeFSM()
{
  _states.clear(); //remove all states
  _states.push_back(ACFSMState<CType>()); //add the root

  _lastState = 1;

  for (unsigned int i=0; i<_needles.size(); ++i)
  {
    AddWord(_needles[i],i); //build the forward edges
  }

  std::list<int> q;
  //iterate over all children of root node
  for (std::map<CType,int>::iterator it = _states[0].next.begin(); it != _states[0].next.end(); ++it)
  {
    int s = it->second;
    _states[s].back = 0;
    q.push_back(s);
  }
  
  while (!q.empty())
  {
    int i = q.front();
    q.pop_front();
    for (std::map<CType,int>::iterator it = _states[i].next.begin(); it != _states[i].next.end(); ++it) //iterate over all children of i
    {
      int s = it->second;
      int z = Step(it->first,_states[i].back); //
      _states[s].back = z;

      if (_states[z].terminal != -1)
        _states[s].shortcut = z;
      else
        _states[s].shortcut = _states[z].shortcut;
      q.push_back(s);
    }
  }
}

template<typename CType, typename SType>
bool AhoCorasick<CType, SType>::Search(const SType& haystack) const
{  
  if (_needles.empty())
  {
     return false;
  };
  int s = 0; //current state
  int i = 0; //index in haystack
  CType c = wxTolower(haystack[0]);

  while (c != 0)
  {    
    s = Step(c,s);
    int j = s; //needle candidate state
    while (j != -1)
    {
      if (_states[j].terminal != -1)
      {
        return true; //found a terminal -> the needle was in the haystack
      }
      j = _states[j].shortcut;
    };
    c = ToLower(haystack[++i]);
  };
  return false; //didn't find a single needle
}

template<typename CType, typename SType>
bool AhoCorasick<CType, SType>::Search(const CType* haystack) const
{  
  if (_needles.empty())
  {
     return false;
  };
  int s = 0; //current state
  int i = 0; //index in haystack
  CType c = wxTolower(haystack[0]);

  while (c != 0)
  {    
    s = Step(c,s);
    int j = s; //needle candidate state
    while (j != -1)
    {
      if (_states[j].terminal != -1)
      {
        return true; //found a terminal -> the needle was in the haystack
      }
      j = _states[j].shortcut;
    };
    c = ToLower(haystack[++i]);
  };
  return false; //didn't find a single needle
}

template<typename CType, typename SType>
void AhoCorasick<CType, SType>::FindAll(SType& haystack, std::vector<searchResult> & result) const
{
  if (_needles.empty())
  {
     return;
  };
  int s = 0; //current state
  int i = 0; //index in haystack
  CType c = ToLower(haystack[0]);

  while (c != 0)
  {    
    s = Step(c,s);
    int j = s; //needle candidate state
    while (j != -1)
    {
      if (_states[j].terminal != -1)
      {
        result.push_back(searchResult(i,_states[j].terminal)); //found a terminal -> the needle was in the haystack
      }
      j = _states[j].shortcut;
    };
    c = ToLower(haystack[++i]);
  }
}

template<typename CType, typename SType>
searchResult AhoCorasick<CType, SType>::FindNext(SType& haystack, int pos) const
{
  if (_needles.empty())
  {
     return searchResult(-1,-1);
  };
  int s = 0; //current state
  int i = pos; //index in haystack
  CType c = ToLower(haystack[i]);

  while (c != 0)
  {    
    s = Step(c,s);
    int j = s; //needle candidate state
    while (j != -1)
    {
      if (_states[j].terminal != -1)
      {
        return searchResult(i,_states[j].terminal); //found a terminal -> the needle was in the haystack
      }
      j = _states[j].shortcut;
    };
    c = ToLower(haystack[++i]);
  }
  return searchResult(-1,-1);
}