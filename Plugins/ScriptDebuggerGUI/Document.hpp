#pragma once

#include <vector>
#include <map> //_VBS3_SD_IMP_BREAKPOINTS
#include <wx/wxScintilla/wxscintilla.h>

#include "../../Common/Essential/RefCount.hpp"

#include "ui/FormBuilderGUI.h"
#include "iMainWindow.hpp"
#include "DebugScript.hpp"

// Includes both debug point and current execution point
#define COL_DEBUG_POINT 0 
#define COL_LINE_NUMBERS 1
#define COL_DOTTED_SEP 2
#define COL_FOLDING_MARGIN 3

#define COL_DEBUG_POINT_STYLE 40
#define COL_LINE_NUMBER_STYLE 41
#define COL_DOTTED_SET_STYLE 42


#define DOC_ORIGINAL (1<<0)
#define DOC_ORIGINAL_PP (1<<1)
#define DOC_ENGINE (1<<2)
#define DOC_ENGINE_PP (1<<3)
#define DOC_VM (1<<4)
#if _VBS3_DEBUG_DELAYCALL
#define DOC_DELAYED (1<<5)
#endif

#if !_VBS3_SD_IMP_BREAKPOINTS
struct BreakPointLine
{
  int _handle;
  int _sourceLineNumber;
  int _absSouceLineNumber;
  DWORD _engineBPNum;

  BreakPointLine(int handle, int lineNumber);

  BreakPointLine &SetScriptBPNum(DWORD bp)
  {
    return *this;
  }
  BreakPointLine &GetScriptBPNum(DWORD &bp)
  {
    bp = _engineBPNum;
    return *this;
  }
};
#endif

// Windows code pages
#define ACP_ENCODING_LIST(XX)\
  /* Windows OS local ACP code page*/\
  XX(CP_Default,0)\
  /*Single byte encoding (Windows CP)*/\
  XX(CP_CentralEurope,1250)\
  XX(CP_Cyrillic,1251)\
  XX(CP_LatinI,1252)\
  XX(CP_Greek,1253)\
  XX(CP_Turkish,1254)\
  XX(CP_Hebrew,1255)\
  XX(CP_Arabic,1256)\
  XX(CP_Baltic,1257)\
  XX(CP_Vietnam,1258)\
  XX(CP_Thai,874)\
  /*Single byte encoding (Windows OEM CP)*/\
  XX(CP_OEM_US,437)\
  XX(CP_OEM_Arabic,720)\
  XX(CP_OEM_Greek,737)\
  XX(CP_OEM_Baltic,775)\
  XX(CP_OEM_MultilingualLI,850)\
  XX(CP_OEM_LatinII,852)\
  XX(CP_OEM_Cyrillic,855)\
  XX(CP_OEM_Turkish,857)\
  XX(CP_OEM_MultilingualLIEuro,858)\
  XX(CP_OEM_Hebrew,862)\
  XX(CP_OEM_Russian,866)\
  /*Single byte encoding (Windows ISO code pages)*/\
  XX(CP_ISO_8859_1, 28591)\
  XX(CP_ISO_8859_2,28592)\
  XX(CP_ISO_8859_3,28593)\
  XX(CP_ISO_8859_4,28594)\
  XX(CP_ISO_8859_5,28595)\
  XX(CP_ISO_8859_6,28596)\
  XX(CP_ISO_8859_8,28598)\
  XX(CP_ISO_8859_9,28599)\
  XX(CP_ISO_8859_15,28605)\
  /*Double byte encodings (Windows DB code pages)*/\
  XX(CP_DB_JapaneseShiftJIS,932)\
  XX(CP_DB_SimplifiedChineseGBK,936)\
  XX(CP_DB_Korean,949)\
  XX(CP_DB_TraditionalChineseBigFive,950)\
  /*Unicode encoding*/\
  XX(CCP_UTF7,65000)\
  XX(CCP_UTF8,65001)\
  /*Document enums*/\
  XX(CCP_UCS2LE,32)\
  XX(CCP_UTF16,33)\


// Here because there is no define for CP_DEFAULT
#define MENU_CP_Default  0

/// Enum used to identify the current encoding of the data
enum Encoding
{
  #define ENUM_DEC(NAME,ID) NAME = ID,
  ACP_ENCODING_LIST(ENUM_DEC)
};


static Encoding FromWxMenuIDToEncoding(unsigned int id)
{
  switch(id)
  {
    #define CONVERT_WX_TO_ENC(NAME,ID) case MENU_##NAME: return NAME;
    ACP_ENCODING_LIST(CONVERT_WX_TO_ENC)
  default:
    Assert(false);
  }
  return CP_Default;
}

static unsigned int FromEncodingToWxMenuID(Encoding enc)
{
  switch(enc)
  {
    #define CONVERT_ENC_TO_WX(NAME,ID) case NAME:return MENU_##NAME;
    ACP_ENCODING_LIST(CONVERT_ENC_TO_WX)
  default:
    Assert(false);
  }
  return MENU_CP_LatinI;
}

/// Internally the engine representation is either
enum EngineEncoding
{
  /// Encoding is Single Byte Character set eg.. any code page
  EN_SBCS,
  /** 
   * Encoding is UTF-8 format, ASCII is treated as valid UTF-8
   * @see isUTF8
   */
  EN_UTF8
};

class wxScintillaExt : public wxScintilla
{
private:
  int _startingSearchPos;
  int _prevSearchMods;
  wxString _prevSearchString;
public:
  wxScintillaExt(wxWindow *parent);

  void OnKeyDown(wxKeyEvent &evt);
  void OnSearchWindow();
  void OnSearchWindowEvent(wxCommandEvent &evt);
  void OnGainFocus(wxFocusEvent &evt);

  DECLARE_EVENT_TABLE()
};

#if _SD_LIVEFOLDER_IMP
struct ScriptBreakpoint;
#endif

/**
  * Document viewer is responsible for rendering the document to the user
  */
class DocumentViewer : public wxTimer, public iViewWindow,public RefCount, public WeakLink
{
  /// Documents name
  wxString _name;
  /// Documents absolute file path
  wxString _filePath;
  /// Documents relative file path
  wxString _relativePath;
   
  bool _reloading;
  FILETIME _lastWriteTime;

#if _VBS_15079_RESTRICT_BISIM_SCRIPTS
  //internal scripts are scripts, that the user did not explicitely open (he got them from VBS somehow) - the debugger opened them
  bool _internal;
  //restricted scripts do not show the contents if they are internal - they only show contents from disk ( all scripts in /vbs2/ except for /vbs2/customer are restricted)
  bool _restricted;
#endif

  /// Raw file contents encodings
  Encoding  _rawFileEncoding;
  /// Original open file contents (raw data)
  std::vector<char> _rawFileContents;
  /// Raw file contents CRC value
  unsigned long _rawFileCRC;
 
  /// Engines encoding for the file contents, can be UTF-8 or any SBCS code page
  EngineEncoding _engineFileEncoding;
  /// Engines file content, via LoadFile
  std::vector<char> _engineFileContent; 
  /// Engine PreProccess data contents
  std::vector<char> _enginePPFileContent; 
  /// Raw EngineFileContent CRC value
  unsigned long _engineFileCRC;
  /// Parser contents from the PPFileContents. ASCII,SBCS (code page),UTF-8 encoded
  ResolvePPFile _enginePPFileResolver; 

  /// Name to the virtual machine
  std::string _virtualMachine;

  /// What document contents are available
  unsigned int _docContent;
  /// What document view is currently selected <b>(See DOC_ define)</b>
  unsigned int _docView;

  /// Current displayed line number
  int _lineNum;
  
  /// Scintilla rendering document
  wxScintillaExt *_doc; 
  /// Is the current document dirty?
  bool _dirty; 

  /// User placed break points
#if _VBS3_SD_IMP_BREAKPOINTS
  std::vector<DWORD> _breakPoints; // Breakpoints
#else 
  std::vector<BreakPointLine> _breakPoints; // Breakpoints
#endif
#if _SD_LIVEFOLDER_IMP
  std::vector<ScriptBreakpoint> _bpUpdates; //pending updates to breakpoints
#endif

public:
  DocumentViewer(const wxString& name, const wxString &filePath = wxString(), wxWindow* parent = NULL);
  virtual ~DocumentViewer();

  /// Populates the wxMenu with the current document state
  void PopulateWxMenu(wxMenu *);

  /// Unhook the link to the scintilla document
  void UnHook();

  /// Saves the document to the filePath from the selected view.
  void SaveDocument(wxString filePath);
 
  /// Returns the current Documents content, can be (virtualdoc,engine,enginepp,openfile)
  unsigned int GetDocContents();
  /// Returns the virtual document name.
  const TCHAR *GetDocViewName();
  /// Sets the Documents view, depending on its contents
  void SetDocView(unsigned int view);
  /// Returns the current document view.
  unsigned int GetDocView();

  //returns true if the file's version on disk is newer than the last time we checked
  bool FileChanged();
  //sets the flag marking that the check for filechange has been started preventing multiple prompts
  void SetReloading(bool value);
  //gets the last modification time of this file on disk (returns false if the procedure fails for whatever reason)
  bool GetLastDiskWriteTime(FILETIME& ftime);

  /// Set's the the contents, depending on the view. Null terminated string or data is required
  void SetDocViewContent(unsigned int view, std::string &data, bool UCS2 = false, FILETIME lastWriteTime = FILETIME());
  void SetDocViewContent(unsigned int view, char *src, size_t len, bool UCS2 = false, FILETIME lastWriteTime = FILETIME());

  /// Clears the current view
  void ClearCurrentView();

#if _VBS_15079_RESTRICT_BISIM_SCRIPTS
  //shows the restricted content message
  void SetProtectedContent();
#endif

  /// Return the documents, engine pre-processed file
  ResolvePPFile &GetEngineResolvedPPFile();

  // Get the documents encoding
  Encoding GetEncoding();
  // Will cause the document to lose its current un-saved
  // work and be converted
  bool SetEncoding(Encoding newEncoding);

  void CreateCRCForRawContent();

  /// Update IntelliSense global cache
  static void UpdateIntelliSense();
  /// Causes the document to clear its keywords and do a force IntelliSense update
  void UpdateDocIntelliSense();

  /// Toggles the current line in the document to have a break point
  void ToggleCurrentLineBP();
  /// Sets a break point on a specific line
  void SetBreakPointOnLine(int line);
  /// Check to see if a break point is currently at the indicated line
  bool IsBreakPointAt(int line);
#if _VBS3_SD_IMP_BREAKPOINTS
  /// Remove the breakpoint with the specific handle from the file
  void RemoveBreakpoint(DWORD bpId);
  /// Removes all file breakpoints
  void RemoveAllBreakpoints();
#else
  /// Removes the break point from the line
  void RemoveBreakPoint(int line);
#endif

#if _SD_LIVEFOLDER_IMP
  //Applies all breakpoint updates to existing breakpoints
  void ApplyBPUpdates();
#endif

  /// Shows the current line execution markers (arrow + step scope)
  void SetLineNumber(int lineNumber = -1, bool isStepping = false, StepKind stepKind = SKOver);

  /// Does nothing
  void StateChange(DebugScript*);
  void SimulateUpdate(float deltaT);

  /// Checks to see if the engine file is the same as the opened file
  bool IsEngineFileSameAsOpenFile();

  /// Return the Document name
  wxString &GetName(){ return _name; }
  /// Return the file path
  wxString &GetFilePath(){ return _filePath; }
  /// Return the relative path
  wxString &GetRelativePath();
#if _VBS_14074_DEV_PATH_UPDATE_FIX
  /// Compute the relative path based on current dev path
  void UpdateRelativePath();
#endif
  /// Set the doc file path.
#if _VBS3_SD_IMP_BREAKPOINTS
  void SetFilePath(wxString path)
  {
    _filePath = path.Lower();
  }
#else
 void SetFilePath(wxString path){ _filePath = path; }
#endif
  /// Converts the input contents ready for the CallTip. Will insert new line feeds into the document if above maxWidth
  wxString CallTipSizeCalculator(wxString &content, int maxWidth=400);
  
#if _VBS_15079_RESTRICT_BISIM_SCRIPTS
  //returns previously stored isRestricted value
  bool GetStoredIsRestricted();
  //computes if the file should be restricted - returns the new value
  //update : if true, override the stored value with the new one
  bool IsRestricted(bool update = false);

  bool ContentsProtected() const;

  //internal scripts were not opened by the user from the disk
  bool IsInternal() const;
  void SetInternal( bool isInternal );
#endif

  /// Unknown what this does
  void Notify();

#if !_VBS3_SD_IMP_BREAKPOINTS
  /// Return the users placed break points.
  std::vector<BreakPointLine>& GetBreakPoints();
#endif

  /// Check if the document has had any modification done to it
  bool IsDirty(){ return _dirty; }
  /// Return the Scintilla document
  wxScintillaExt *GetDocument(){return _doc;}

  // -------------- Event's Start -------------- 
  /// Called when the user selects a auto complete entry
  void OnAutoCompleteSelected(wxScintillaEvent &);
  /// User clicked on the break point margin
  void OnMarginClick(wxScintillaEvent &);
  /// Process any Scintilla updated UI events
  void OnScintillaUpdatedUI(wxScintillaEvent &);
  /// Process any Scintilla updated UI events
  void OnScintillaModified(wxScintillaEvent &evt);
  /// Process any Scintilla updated UI events
  void OnScintillaCharAdded(wxScintillaEvent &);
  
  /// Clear the CallTip when mouse exits
  void OnMouseWindowExit(wxMouseEvent &);
  /// Does nothing, can be ignored
  void OnMouseWindowEnter(wxMouseEvent &);
  /// On mouse move calls OnMouseUpdate
  void OnMouseWindowMove(wxMouseEvent &);
  /// Scan the Documents content, for any identifiers, nulers, or variables to display to the set via CallTip's or Memory watch
  void OnMouseUpdate();

  /// Generic event handling to update the state of Scintilla
  template<class Type>
  void OnAnyEvent(Type type)
  {
    // Call the generic event binding...
    OnAnyEventImp();
    // Call the generic skip...
    type.Skip();
  }
  /// Called by OnAnyEvent template event handler, to avoid having to hunt down the Parameter Type
  void OnAnyEventImp();

  /// Event table
  DECLARE_EVENT_TABLE()
  // -------------- Event's End --------------
};
