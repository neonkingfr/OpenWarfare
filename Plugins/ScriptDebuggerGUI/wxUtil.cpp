#include "wxUtil.hpp"

// Search all the sizers,objects,contains for the idc....

// Every window can have a sizer. Once a sizer has been hit then the normal
// wxWindow *wxWindowMSW::FindItem(long id) const fails... 
//
// Because of this, if all elements in the sizer fail. Then we search the windows children
// So,,, its Window -> Sizer | window children...
wxWindow* FindIDC(wxWindow *window, int idc)
{
  // Check the windows id to see if it matches.
  if (!window) return NULL; 
  if (window->GetId()==idc) return window;


  // Nope check all its sizers
  wxSizer* sizer = window->GetSizer(); 
  if (sizer)
  {
    wxSizerItemList& rows = sizer->GetChildren();
    for ( wxSizerItemList::iterator i = rows.begin(),end = rows.end();i != end; ++i )
    {
        wxSizerItem * const item = *i;

        // Recursive decent
        wxWindow *foundWindow = FindIDC(item->GetWindow(),idc);
        if (foundWindow) return foundWindow;
    }
  }

  wxWindowList::compatibility_iterator current = window->GetChildren().GetFirst();
  while (current)
  {
    wxWindow *childWin = current->GetData();

    wxWindow *wnd = FindIDC(childWin,idc);
    if ( wnd ) return wnd;

    current = current->GetNext();
  }

  return NULL;
}
