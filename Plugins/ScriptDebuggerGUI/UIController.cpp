#include "../../Common/Essential/AppFrameWork.hpp"
#include "../../Common/Essential/ForEach.hpp"


#include "UIEvents.hpp"
#include "UIController.hpp"
#include "MainWindow.hpp"


void UIController::SearchMainElements(MyFrame *frame)
{
  // Set the default frame.
  _frame = frame;

  // Control init.
  _controlTree = frame->ControlTree;
  _controlTextArea = frame->ControlTextArea;

  _controlCallStack = frame->ControlCallStack;
  _controlWatch = frame->ControlWatch;
   // Assert all the controls exist
  _btnContinue = frame->BtnContinue;
  _btnBreak = frame->BtnBreak;
  _btnNextLine = frame->BtnNextLine;
}

void UIController::Simulate(float deltaT)
{
  RemoveSimulateWindows();
  for (size_t i=0; i<_simWindows.size(); i++) _simWindows[i]->Simulate(deltaT);
}

void UIController::RemoveSimulateWindows()
{
  std::vector<size_t> simIndexs;
  for (size_t i=0; i<_remWindows.size(); i++)
  {
    for (size_t x=0; x<_simWindows.size(); x++)
    {
      if (_remWindows[i] == _simWindows[x]) 
      {
        Reduce(_simWindows,x);
        break;
      }        
    }
  }

  // All windows should be removed now.
  _remWindows.clear();
}

void UIController::RequestAddSimulate(WindowSimulator *obj)
{
  _simWindows.push_back(obj);
}

void UIController::RequestRemoveSimulate(WindowSimulator *obj)
{
  _remWindows.push_back(obj);
}

UIController GUIController;
