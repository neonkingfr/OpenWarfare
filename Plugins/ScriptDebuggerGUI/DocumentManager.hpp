#pragma once

#include <vector>
#include <string>

#include "Document.hpp"
#include "../../Common/Essential/RefCount.hpp"
#include "../../Common/Essential/AppFrameWork.hpp"


class IDebugScript;
class DebugScript;

/**
 * Document manager main responsibility to to help the communication between
 * DocumentTabManager, and DocumentView. It contains many DocumentViewer
 * and only one DocumentTab manager.
 */
class DocumentManager : public RefCount, public iViewWindow
{
  std::vector< SmartRef< DocumentViewer > > _documents;
public:
    
  DocumentManager();

  /// Open the file, return a document viewer, is automatically added to the tab list.
#if _VBS_15079_RESTRICT_BISIM_SCRIPTS
  DocumentViewer *OpenFile(wxString filePath, DocumentViewer* update = NULL, bool isInternal = true);
#else
  DocumentViewer *OpenFile(wxString filePath, DocumentViewer* update = NULL);
#endif

  /**
  * Open engine file opens the file from the VBS2 engine,
  * and also loads the pre-processed version. If view is provided
  * it is appended to the current view
  */
  DocumentViewer* OpenEngineFile(wxString relOrAbsPath,DocumentViewer *view =NULL);

  void CloseAllDocuments();
  void CloseDocumentCheckLock(DocumentViewer *doc);
  void CloseDocument(DocumentViewer *doc);
/**
  * Tab closing document tell's us that a double click or somebody wanted
  * to close the document so, we do the necessary clean up. Remove it from
  * the tree, then unhook the docViewer from the event's the finally delete 
  * the document, leaving the scintilla to be cleaned up by the flatnote book
  */
  void TabCloseingDocument(DocumentViewer *doc);

  /// Return the current focused document
  DocumentViewer * GetCurrentDocument();

  void ToggleBPOnFocusDocument();
  void FocusDocument(DocumentViewer *doc);

  /// Document that has come into focus from the tab manager
  void DocumentTabSelected(DocumentViewer *doc);

  DocumentViewer *GetDocumentViewer(wxWindow *scintilla);

  /// Creates a document that is not based of a file path.
#if _VBS_15079_RESTRICT_BISIM_SCRIPTS
  DocumentViewer *CreateDocument(const wxString &name, const wxString &filePath = wxString(), bool isInternal = true);
#else
  DocumentViewer *CreateDocument(const wxString &name, const wxString &filePath = wxString());
#endif
  
  /// Get the document that reflects the debug script
  DocumentViewer *GetDocument(DebugScript *debugScript);
  /// Get the document that has the same path
  DocumentViewer *GetDocument(wxString path);

  void StateChange(DebugScript *debugScript);
  void SimulateUpdate(float deltaT);

  //Checks if any of the open files have been changed and prompts the user to reload them if they were
  void CheckForFileChanges();

#if _VBS_14074_DEV_PATH_UPDATE_FIX
  //Updates the relative file path on all open files
  void UpdateRelativePaths();
#endif
#if _VBS_15079_RESTRICT_BISIM_SCRIPTS
  //Is run when dev drive is changed - recomputes restricted scripts
  void UpdateProtectedFiles();
#endif

#if !_VBS3_SD_IMP_BREAKPOINTS
  /// Pass in the IDebug script so it can set all the break points.
  void SetAllBreakPoints(IDebugScript *script);
#endif

  /// Get the virtual machine doc, editable
  DocumentViewer *GetVirtualMachineDoc();
#if _VBS3_DEBUG_DELAYCALL
  DocumentViewer *GetDelayCallDoc();
#endif

  /// For each for each document for setting their scintilla style
  template <class Function, class UserData>
  void ForEachDocumentView(Function ptr,UserData userData)
  {
    for (size_t i=0; i<_documents.size(); i++)
      if (ptr(_documents[i].GetRef(),userData)) break;
  }
};

extern SmartRef< DocumentManager > GDocumentManager;
