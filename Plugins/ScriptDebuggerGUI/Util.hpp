#pragma once

#include <Windows.h>
#include <string>
#include "settings.hpp"
#include "../../Common/Essential/AppFrameWork.hpp"


enum VBSFolderType
{
  USER_DIR = 0,
  MISSION_DIR
};

int ExecuteCommandRP(const char *command, char *result, int resultLength);
bool IsExecuteCommandRP();

// Utilities header, used to contain information that is via all
// parts of the application.

struct Util
{
  // Can be HINSTANCE or HMODULE
  void *_handle;

  Util()
  {
   _handle = NULL;
  }

  void Init(void *handle=NULL)
  {
    if (handle) GUtil._handle = handle;
    else GUtil._handle = GetModuleHandle(NULL);
  }

  // All are automatically in-lined
  HINSTANCE GetHINSTANCE() {return (HINSTANCE)_handle;}
  HMODULE GetHModule()   {return (HMODULE)_handle;}

  std::wstring GetExePath()
  {
    TCHAR buffer[MAX_PATH] = {0};
    DWORD length = GetModuleFileName((HINSTANCE)_handle,buffer,MAX_PATH);
    Assert(length>0);

    // Return the exe Path
    return std::wstring(buffer);
  }
  
  // This includes the backslash
  std::wstring GetExeFolderPath()
  {
    TCHAR buffer[MAX_PATH] = {0};
    DWORD length = GetModuleFileName((HINSTANCE)_handle,buffer,MAX_PATH);
    Assert(length>0);

    std::wstring path;
    const TCHAR *backSlash = wcsrchr(buffer,L'\\');
    if (backSlash) path = std::wstring(buffer,(backSlash-buffer)+1);

    return path;
  }

  std::string EvaulateValue(std::string val)
  {
    if (GSettings.EnableDebugging() && IsExecuteCommandRP())
    {
      #define BUFFER_SIZE_EVAL 1024
      char buffer[1024] = {0};        
      ExecuteCommandRP(val.data(),buffer,BUFFER_SIZE_EVAL-1);

      // return a stripped version to display
      return std::string(buffer);
    }

    return std::string("Not connected");
  }

  std::string GetEngineDirectory(VBSFolderType type)
  {
    if (IsExecuteCommandRP())
    {
      char path[MAX_PATH] = {0};

      // Do a check if the mission directory has changed
      // getDirectory 1
      std::string cmd = ::Format("getDirectory %d",type);
      ExecuteCommandRP(cmd.data(),path,MAX_PATH);
      
      int pathLen = strlen(path);
      if (path>0)
      {
        // Directors are like
        // c:\User\Documents\VBS2\Missions\Mission.intro\
        // Remove the trailing \, that should give us the correct directory to scan in.
        char *start = &path[1];
        char *end = strrchr(path,'\\');
        if (!end) return std::string(); // some error occurred " is not there

        // do some simple bound checking
        std::string missionPath;
        if (pathLen>1&&end>start) 
        {
          *end = 0;
          return std::string(start);
        }
      }
    }

    return std::string();
  }


  // Set the applications current directory, used for developer path
  void SetCurrentDirectory(std::wstring path)
  {
    ::SetCurrentDirectory(path.data());
  }
  // Return the current directory
  std::wstring GetCurrentDirectory()
  {
    TCHAR buffer[MAX_PATH] = {0};
    ::GetCurrentDirectory(MAX_PATH,buffer);
    return std::wstring(buffer);
  }

#if _VBS_15079_RESTRICT_BISIM_SCRIPTS
  //returns true if the script path should be hidden from the customer
  bool IsProtectedPath(const wxString& path) const
  {
    if (path.Find(L"vbs2\\") == 0 || path.Find(L"SQF:vbs2\\") == 0)//means it's an addon script
    {
      if (path.Find(L"vbs2\\customer\\") == wxNOT_FOUND) 
      {
        return true; //vbs2 addon
      }
    }
    return false; //mission file or customer folder
  }
#endif

#if _VBS3_DEBUG_DELAYCALL
  //converts to a relative vbs2 or mission path
  //if the path doesn't fit any of the criteria, the absolute path is returned
  wxString ConvertToRelativePath(wxString &absolutePath) const;
#endif
} extern GUtil;


void CreateFrameWork(void *handle = NULL);

#if UNICODE
// Utility for converting from and to UTF16 and ANSI Code pages
wxString CharToUTF16(const char *src, size_t len = 0, unsigned int codePage = CP_ACP );
std::string UTF16ToChar(const wchar_t *src, size_t len = 0, unsigned int codePage = CP_ACP);

wxString CharToUTF16(const std::string &src, unsigned int codePage = CP_ACP );
std::string UTF16ToChar(const wxString &src, unsigned int codePage = CP_ACP);
#else
std::wstring CharToUTF16(char *src, size_t len = 0, unsigned int codePage = CP_ACP );
std::string UTF16ToChar(wchar_t *src, size_t len = 0, unsigned int codePage = CP_ACP);

std::string UTF16ToChar(std::wstring src, unsigned int codePage = CP_ACP);
std::wstring CharToUTF16(std::string, unsigned int codePage = CP_ACP );
#endif