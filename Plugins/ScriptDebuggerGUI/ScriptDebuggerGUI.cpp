// Copyright (C) 2012 Bohemia Interactive Simulations - support@BISimulations.com
//
// Purpose: Main entry where VBS2(NG) loads the ScriptDebugger plugin into memory for running
//
// Author: Chad Lion
// Date: 17-07-2012


#include <windows.h>

#include "ScriptDebuggerGUI.h"
#include "Util.hpp"

#include "../../Common/Essential/AppFrameWork.hpp"
#include "../../Common/iDebugEngineInterface.hpp"
#include "../../Common/Essential/RefCount.hpp"

#include "ResourceLoader.hpp"
#include "resource.h"

#include "wx/wx.h"
#include "wx/msw/private.h"

#include "ActivationContext.h"


typedef int (WINAPI * CallBackExecuteCommand)(const char *command, char *result, int resultLength);
CallBackExecuteCommand ExecuteCommand = NULL;

// Protection from self recursive loops, when events are used!
extern int GRecursiveLoopProtection;
int ExecuteCommandRP(const char *command, char *result, int resultLength)
{
  GRecursiveLoopProtection = true;
  int exCmdResult = ExecuteCommand(command,result,resultLength);
  GRecursiveLoopProtection = false;
  
  return exCmdResult;
}
bool IsExecuteCommandRP()
{
  return ExecuteCommand>0;
}

EXPORT void WINAPI RegisterCommandFnc(void *executeCommandFnc)
{
  ExecuteCommand = (CallBackExecuteCommand)executeCommandFnc;
}

EXPORT const char* WINAPI PluginFunction(const char *input)
{
  return "";
}


bool WXResourcesAllocated;

void CleanWxGUIResources()
{
  // Check to see if the wxResources have been allocated
  // and if we free them
  if (WXResourcesAllocated)
  {
    // Tell the app that its closing down.
    wxTheApp->OnExit();

    // Will delete the wxApp, thus ending its life.
    wxEntryCleanup();
    WXResourcesAllocated = false;
  }
}

bool StartWxResources(int argc = 0, char **argv = NULL )
{
  bool initOk = true;

  wxInitAllImageHandlers();
  WXResourcesAllocated = wxEntryStart(argc, argv); 
  
  if ( !wxTheApp || !wxTheApp->CallOnInit() )
  {
    // Report error
    LogF("Failed to create the plugins script debugger app");

    // Clean up the resources.
    CleanWxGUIResources();    
    initOk = false;
  }

  return initOk;
}

EXPORT void WINAPI OnSimulationStep(float deltaT)
{
}

/**
  * Note: hInst is not the dll's instance but the VBS2 instances.
  */
HINSTANCE GDLLHINST;
HWND GHWND; // Main VBS2 handle to the window

EXPORT void WINAPI RegisterHWND(void *hwnd, void *hins)
{
  DLL_MANIFEST;

#if _SD_SDK_COMPAT
  GHWND = GetAncestor((HWND) hwnd, GA_ROOT);
#else
  GHWND = (HWND) hwnd;
#endif
  CreateFrameWork(GDLLHINST);
  wxSetInstance(GDLLHINST); 
  StartWxResources();
}

EXPORT void WINAPI OnUnload()
{
  DLL_MANIFEST;

  CleanWxGUIResources();
}


/**
  * From the MSDN it is not recommended that any memory, or heap allocation be done
  * on DLL_PROCESS_ATTACH. There is a hybrid of reasons why. Because of this RegisterHWND
  * performs the allocation of the WxWidgets UI. Also for this reason, OnUnload clear's all resources from
  * the main application execution.
  * 
  * DLLMain best practices:
  * @link http://msdn.microsoft.com/en-us/windows/hardware/gg487379.aspx
  */
BOOL WINAPI DllMain(HINSTANCE hDll, DWORD fdwReason, LPVOID lpvReserved)
{
  switch(fdwReason)
  {   
    case DLL_PROCESS_ATTACH: 
      {
#if _DEBUG
        LogF("DLL_PROCESS_ATTACH version of commCtrl:%d\n", wxApp::GetComCtl32Version() );
#endif
        REMEMBER_DLL_MANIFEST;
        GDLLHINST = hDll;
      }
        break;
    case DLL_PROCESS_DETACH:break;
    case DLL_THREAD_ATTACH:break;
    case DLL_THREAD_DETACH:break;
  }

  return TRUE;
}

#if _CONSOLE 

/**
  * Main used in console mode to init the main window for testing outside of VBS2
  */
int main(int argc, char * argv[])
{
  LogF("Main version of commCtrl:%d\n", wxApp::GetComCtl32Version() );

  // Allocate wxResources, and then go into wxEntry
  if (StartWxResources(argc,argv))
  {
    class CallOnExit
    {
    public:
      ~CallOnExit() { wxTheApp->OnExit(); }
    } callOnExit;
    wxTheApp->OnRun();
  }
  // Does a check if resources where allocated
  CleanWxGUIResources();

  return 0;
}

#endif