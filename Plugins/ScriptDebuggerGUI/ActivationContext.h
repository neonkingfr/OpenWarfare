#pragma once

#include <windows.h>

class ActContextScope
{
  /// The Dll's manifest context. We need to set this context when entering in any of the functions.
  static HANDLE _dllManifestAContext;
  static ULONG_PTR _dllManifestCookie;
  /// Activate our current dll's manifest activation context
  static void Activate();
  /// Release our current dll's manifest activation context
  static void DeActivate();
public:
  static void SetDllActivateContext();
  static void ForceActivateActivationContext();
  static void ForceUnloadDllActivationContext();

  ActContextScope();
  ~ActContextScope();
};

#define DLL_MANIFEST ActContextScope _activationContext
#define REMEMBER_DLL_MANIFEST ActContextScope::SetDllActivateContext()