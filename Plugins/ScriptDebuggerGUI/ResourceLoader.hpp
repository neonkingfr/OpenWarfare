#include <Windows.h>
#include "../../Common/Essential/RefCount.hpp"
#include <vector>


#pragma once

//! Simple resource that is loaded from load resource.
struct Resource : public RefCount
{
  int _size;
  WORD _resourceID;
  void *_data;
  
  //! Simple resource that is loaded from load resource.
  //! its up to the the app 
  Resource():_size(0),_data(NULL),_resourceID(0){}
  virtual ~Resource(){delete _data;}
};

class ResourceCache 
{
  std::vector< SmartRef<Resource> > _resourceCache;
public:
  // Load the resource, and return a refCounted resource
  // By default, the resource file we define everything as DATA
  // Its the application responsibility to know what its loading from 
  // the resource file and how large.
  Resource *LoadResource(WORD resourceID, LPCSTR type = "DATA");
} extern GResourceCache;
