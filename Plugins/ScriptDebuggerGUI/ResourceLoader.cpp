#include "../../Common/Essential/AppFrameWork.hpp"
#include "ResourceLoader.hpp"
#include "Util.hpp"

ResourceCache GResourceCache;

// Load the resource, and return a refCounted resource
Resource *ResourceCache::LoadResource(WORD resourceID, LPCSTR type)
{
  HMODULE module = GUtil.GetHModule();

  for (size_t i=0; i<_resourceCache.size(); i++)
    if (resourceID==_resourceCache[i]->_resourceID) return _resourceCache[i];

  HRSRC rsrc = FindResourceA(module, MAKEINTRESOURCEA(resourceID), type); 
  if (!rsrc) return NULL;

  DWORD size = SizeofResource(module, rsrc);
  HGLOBAL memoryHandle = ::LoadResource(module,rsrc);
  if (!memoryHandle) return NULL;

  void *ptr = LockResource(memoryHandle);
  if (!ptr) return NULL;

  // Make a resource stub. 
  Resource *res = new Resource;
  res->_resourceID = resourceID;
  res->_data = new char[size];
  res->_size = size;
  // Copy the data across...
  memcpy(res->_data,ptr,size);

  // Place resource into cache for easy 
  // loading.
  _resourceCache.push_back(res);

  return res;
}