#include "wx/grid.h"
#include "wx/treectrl.h"
#include "wx/aui/auibook.h"

#include "../../Common/Essential/RefCount.hpp"
#include "../../Common/iDebugEngineInterface.hpp"

#include "DebugScript.hpp"
#include "VirtualMachineTree.hpp"
#include "WatchManagerUI.hpp"
#include "CallStackManager.hpp"

#pragma once

//! UIController is a simple development process
//! This is a payload used by almost all managers
//! and other controllers. Its main focus is to have a central
//! UI element access area instead of UI elements being spread and
//! passed around.
//!
//! When UI elements normally are removed from this class they go into their
//! own managers. This then becomes a message passing system between them all.
class MyFrame;
class UIController : public RefCount
{
  void RemoveSimulateWindows();

public:
  /// Simulation window, windows add and remove them-selves from the UIController.
  std::vector<WindowSimulator*> _simWindows;
  /// Buffers for removing, items from the simulation
  std::vector<WindowSimulator*> _remWindows;
  
  /// Higher construct system
  SmartRef<VirtualMachineTree>      _treeManager; 
  SmartRef<WatchManagerUI>     _watchManager;
  SmartRef<CallStackManagerUI> _callStack;
  
  // Main control frame
  MyFrame *_frame;
  // Basic elements
  wxTreeCtrl  *_controlTree; 
  wxRichTextCtrl  *_controlTextArea;  
  wxGrid      *_controlWatch;
  wxGrid      *_controlCallStack;
  // BtnContinue
  // BtnBreak
  // BtnNextLine
  wxButton *_btnContinue;
  wxButton *_btnBreak;
  wxButton *_btnNextLine;

  // Search for elements in the main frame
  void SearchMainElements(MyFrame *frame);
  //! Simulate the windows
  void Simulate(float deltaT);
  //! Add and remove window simulation
  void RequestAddSimulate(WindowSimulator *obj);
  void RequestRemoveSimulate(WindowSimulator *obj);

  UIController *operator->(){return this;}
} extern GUIController;