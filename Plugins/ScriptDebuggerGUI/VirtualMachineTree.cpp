#include "../../Common/Essential/AppFrameWork.hpp"
#include "VirtualMachineTree.hpp"
#include "UIController.hpp"
#include "MainWindow.hpp"
#include "ImgManager.hpp"
#include "resource.h"
#include "UIEvents.hpp"
#include "../../Common/Essential/ForEach.hpp"

#include "wx/wxscintilla/wxscintilla.h"

BEGIN_EVENT_TABLE(VirtualMachineTree, wxEvtHandler)
EVT_LEFT_DCLICK(VirtualMachineTree::OnMouseLeftDoubleClick)
EVT_RIGHT_DOWN(VirtualMachineTree::OnMouseRightUp)
EVT_TREE_ITEM_RIGHT_CLICK(-1,OnMouseItemRightClick)
//EVT_TREE_ITEM_GETTOOLTIP(-1,OnGetToolTip)
END_EVENT_TABLE()


void VirtualMachineTree::OnGetToolTip(wxTreeEvent &event)
{
  event.SetLabel(L"");
  event.Allow();
}

bool VirtualMachineTree::GetCurrentFocus(wxTreeItemId &focus)
{
  if (_focus.IsOk())
  {
    focus = _focus;
    return true;
  }
  
  return false;
}

TreeItemSync *VirtualMachineTree::GetItem(wxTreeItemId &treeItem)
{
  for (size_t i=0; i<_items.size(); i++)
    if (treeItem == _items[i]._item) return &_items[i];

  return NULL;
}

void VirtualMachineTree::OnMouseRightUp(wxMouseEvent &event)
{
  wxTreeItemId id = _controlTree->HitTest(event.GetPosition());
  
#if _VBS3_DEBUG_DELAYCALL
  VMTreeContextMenu menu;
#else
  wxMenu menu;
#endif
  if (id)
  {
    _controlTree->SelectItem(id);
#if _VBS3_DEBUG_DELAYCALL
    //get the actual item
#if _VBS3_SCRIPT_LINES
    TreeItemSync *item = NULL;
#else
    TreeItemSync *item;
#endif
    for (size_t i=0; i<_items.size();++i)
    {
      if (_items[i]._item == id)
        item = &_items[i];
    }
#if _VBS3_SCRIPT_LINES
    if (item && item->_script->GetType() == DS_DELAYCALL)
#else
    if (item->_script->GetType() == DS_DELAYCALL)
#endif
    {
      menu.script = item->_script;
      menu.script->ShouldBreak() ? menu.Append(TM_EVENT_DELAYED_BREAK, wxT("Unset Break")) : menu.Append(TM_EVENT_DELAYED_BREAK, wxT("Set Break")); 
    }
    else
    {
#endif
#if _VBS3_SCRIPT_LINES
      if (GSettings.EnableDebugging())
      {
#endif
        menu.Append(TM_EVENT_BREAK, wxT("Break"));
        menu.Append(TM_EVENT_CONTINUE, wxT("Continue"));
        menu.Append(TM_EVENT_STEP_OVER, wxT("Step over"));
#if _VBS3_SCRIPT_LINES
      }
#endif
#if _VBS3_DEBUG_DELAYCALL
    }
#endif
    menu.AppendSeparator();
    menu.Append(TM_EVENT_CANCEL, wxT("Cancel"));
  }
  else
  {
    menu.Append(TM_EVENT_BREAK_ALL, wxT("Break All"));
    menu.Append(TM_EVENT_CONTINUE_ALL, wxT("Continue All"));
    menu.AppendSeparator();
    menu.Append(TM_EVENT_CANCEL, wxT("Cancel"));
  }

  //! Set the pop up menu on the control tree instead
  //! of GFrame, this is to fix alignment problems.
  _controlTree->PopupMenu(&menu,event.GetPosition());
  event.Skip();
}

void VirtualMachineTree::OnMouseItemRightClick(wxTreeEvent &event)
{
  event.Skip();
}

void VirtualMachineTree::OnMouseLeftDoubleClick(wxMouseEvent &event)
{
  wxTreeItemId id = _controlTree->HitTest(event.GetPosition());
  UpdateDocumentViewer(id);
  event.Skip();
}

VirtualMachineTree::VirtualMachineTree(wxTreeCtrl *tree)
{
  _controlTree = tree;

  // Hide the root node
  long style = tree->GetWindowStyleFlag();
  tree->SetWindowStyle(style|wxTR_HIDE_ROOT);

  // Disable tooltips
  long styleReal = ::GetWindowLong((HWND)tree->GetHWND(), GWL_STYLE);
  styleReal |= 0x80;
  ::SetWindowLong((HWND)tree->GetHWND(), GWL_STYLE, styleReal);


  _controlTree->SetImageList(GImgManager.GetImageList());

  //! Add the root, and if available load the png img
  _treeRoot       = _controlTree->AddRoot(wxT("Hidden"));
  _scriptVMParent = _controlTree->AppendItem(_treeRoot,wxT("ScriptVM"));
#if _VBS3_DEBUG_DELAYCALL
  _delayCallParent = _controlTree->AppendItem(_treeRoot,wxT("Delayed Calls"));
#endif
  _eventParent    = _controlTree->AppendItem(_treeRoot,wxT("Events"));
  _errorParent    = _controlTree->AppendItem(_treeRoot,wxT("Error"));
  
  // Set treeItems images
  SetTreeItemImg(_treeRoot,IDR_PNG_LIST);
  SetTreeItemImg(_scriptVMParent,IDR_PNG_LIST);
  SetTreeItemImg(_eventParent,IDR_PNG_LIST);
  SetTreeItemImg(_eventParent,IDR_PNG_LIST);
  SetTreeItemImg(_errorParent,IDR_PNG_LIST);
#if _VBS3_DEBUG_DELAYCALL
  SetTreeItemImg(_delayCallParent,IDR_PNG_LIST);
#endif

  // Allow VirtualMachineTree to receive events
  _controlTree->PushEventHandler(this);
#if _VBS3_DEBUG_DELAYCALL
  GMainWindow->RegisterWindow(this);
#endif
}

#if _VBS3_DEBUG_DELAYCALL
VirtualMachineTree::~VirtualMachineTree()
{
  if (GMainWindow) GMainWindow->UnRegisterWindow(this);
}
#endif

#include "../../Common/Essential/Hash/Hasher.hpp"
#include <hash_map>


int counter; 
void VirtualMachineTree::AddNewControlTreeItem(DebugScript *script, wxString name)
{
  Assert(script);
  Assert(!GetScriptTreeItem(script).IsOk());
#if _VBS3_DEBUG_DELAYCALL
  wxString itemName;
  wxTreeItemId parent;
  Index imgIndex;
  
  switch (script->GetType())
  {
  case DS_SCRIPT :
    {
#if _VBS_15079_RESTRICT_BISIM_SCRIPTS
      if (GUtil.IsProtectedPath(name))
        name = L"Internal Script";
#endif
      imgIndex = GImgManager.LoadIdcIntoImageList(IDR_PNG_PLAY, wxBITMAP_TYPE_PNG);
      itemName = wxString::Format(L"%s",name.data());
      bool isEvent = false;
      if (script->GetIDebugScript() && script->GetIDebugScript()->GetType()==STDirect) isEvent = true;
      
      parent = isEvent ? _eventParent : _scriptVMParent;
      break;
    }
  case DS_DELAYCALL :
    {
      parent = _delayCallParent;
      DebugDelayCall * delayCall = script->GetDelayCall();
      imgIndex = GImgManager.LoadIdcIntoImageList(IDR_PNG_HOURGLASS, wxBITMAP_TYPE_PNG);
      if (delayCall)
      {
#if _VBS3_DEBUG_DELAY_UI_CALL
        itemName = GetDelayCallText(delayCall);
#else
  #if _VBS_15079_RESTRICT_BISIM_SCRIPTS
        wxString name = GUtil.IsProtectedPath(delayCall->GetName()) ? L"Internal Script" : delayCall->GetName();
        itemName = wxString::Format(L"[%i] %s [%f]",delayCall->GetId(), name,delayCall->GetDelay());
  #else
        itemName = wxString::Format(L"[%i] %s [%f]",delayCall->GetId(), delayCall->GetName(),delayCall->GetDelay());
  #endif
#endif
      }
      break;
    }
  }

  TreeItemSync sync;
  sync._script = script;
  sync._item   = _controlTree->AppendItem(parent,itemName,imgIndex);

#else
  bool isEvent = false;
  if (script->GetIDebugScript() && script->GetIDebugScript()->GetType()==STDirect) isEvent = true;
  
#if _VBS_15079_RESTRICT_BISIM_SCRIPTS
  if (GUtil.IsProtectedPath(name))
    name = L"Internal Script";
#endif
  wxString itemName = wxString::Format(L"%s",name.data());

  Index imgIndex =GImgManager.LoadIdcIntoImageList(IDR_PNG_PLAY, wxBITMAP_TYPE_PNG);

  TreeItemSync sync;
  sync._script = script;

  wxTreeItemId id = isEvent ? _eventParent : _scriptVMParent;
  sync._item   = _controlTree->AppendItem(id,itemName,imgIndex);
#endif

  // Push back the item
  _items.push_back(sync);
}

void VirtualMachineTree::SelectTreeScript(DebugScript *script)
{
  // select the current item.
  for (size_t i=0; i<_items.size(); i++)
  {
    if (_items[i]._script.GetRef() == script)
    {
      wxTreeItemId child = _items[i]._item;
      
      // Select the child element
      _controlTree->SelectItem(child);

      // Expand the parent
      wxTreeItemId parent = _controlTree->GetItemParent(child);
      _controlTree->Expand(parent);

      // Update the document viewer
      UpdateDocumentViewer(child);

      break;
    }
  }
}

void VirtualMachineTree::UpdateDocumentViewer(wxTreeItemId id )
{
  if (id.IsOk())
  {
    _focus = id;    // Set the focus

    TreeItemSync *treeItem = GetItem(id);
    if (treeItem) GFrame->SetCurrentDocTo(treeItem->_script.GetRef());
  }
}

void VirtualMachineTree::RemoveControlTreeItem(DebugScript *script)
{
  Assert(script);

  for (size_t i=0; i<_items.size(); i++)
  {
    if (_items[i]._script.GetRef() == script)
    {
      // Unset the focused item.
      if (_items[i]._item == _focus) _focus.Unset();

      _controlTree->Delete(_items[i]._item);
      
      Reduce(_items,i);
      break;
    }
  }
}

void VirtualMachineTree::BreakPointEnter( DebugScript *script )
{
  // There can only be one break point entered
  for (size_t i=0; i<_items.size(); i++)
    SetTreeItemImg(_items[i]._item,IDR_PNG_PLAY);

  // Break point entered is currently being debugged. so use the play button.
  wxTreeItemId id;
  if (GetTreeItemId(script,id))
  {
    // Select the child element
    _controlTree->SelectItem(id);

    SetTreeItemImg(id,IDR_PNG_PAUSE);
  }
}


void VirtualMachineTree::BreakPointLeave( DebugScript *script )
{
  wxTreeItemId id;
  if (GetTreeItemId(script,id)) SetTreeItemImg(id,IDR_PNG_PLAY);
}

wxTreeItemId &VirtualMachineTree::GetScriptTreeItem(DebugScript *script)
{
  static wxTreeItemId nulTreeItem;

  for (size_t i=0; i<_items.size(); i++)
    if (_items[i]._script.GetRef()==script) return _items[i]._item;

  return nulTreeItem;
}

#if _VBS3_DEBUG_DELAYCALL
void VirtualMachineTree::DelayBreakChanged(DebugScript* script)
{
  wxTreeItemId id = GetScriptTreeItem(script);
  if (script->ShouldBreak())
    SetTreeItemImg(id,IDR_PNG_EXCLAMATION_MARK);
  else
    SetTreeItemImg(id,IDR_PNG_HOURGLASS);
}

void VirtualMachineTree::SimulateUpdate(float dt)
{
  for (size_t i=0; i<_items.size(); ++i)
    if (_items[i]._script->GetType() == DS_DELAYCALL)
      UpdateDelayCallName(i);
}

void VirtualMachineTree::ForceUpdate(DebugScript* script)
{
  for (size_t i=0; i<_items.size(); ++i)
    if (_items[i]._script.GetRef() == script)
      if (_items[i]._script->GetDelayCall())
      {
        if (script->GetDelayCall()->GetDelay() == 0)
          SetTreeItemImg(_items[i]._item,IDR_PNG_PLAY);
        UpdateDelayCallName(i);
      }
}

void VirtualMachineTree::UpdateDelayCallName(int itemIndex)
{
  DebugDelayCall *delayCall = _items[itemIndex]._script->GetDelayCall();
#if _VBS3_DEBUG_DELAY_UI_CALL
  wxString& name = GetDelayCallText(delayCall);
  _controlTree->SetItemText(_items[itemIndex]._item, name);
#else
  #if _VBS_15079_RESTRICT_BISIM_SCRIPTS
  wxString name = GUtil.IsProtectedPath(delayCall->GetName()) ? L"Protected Script" : delayCall->GetName();
  _controlTree->SetItemText(_items[itemIndex]._item, wxString::Format(L"[%i] %s [%f]",delayCall->GetId(),name,delayCall->GetDelay()));
  #else
  _controlTree->SetItemText(_items[itemIndex]._item, wxString::Format(L"[%i] %s [%f]",delayCall->GetId(),delayCall->GetName(),delayCall->GetDelay()));
  #endif
#endif
}
#endif

bool VirtualMachineTree::GetTreeItemId( DebugScript *script, wxTreeItemId &item)
{
  for (size_t i=0; i<_items.size(); i++)
  {
    if (_items[i]._script.GetRef()==script)
    {
      item = _items[i]._item;
      return true;
    }
  }

  return false;
}


void VirtualMachineTree::SetTreeItemImg( wxTreeItemId &item, int imgId, wxBitmapType type )
{
  Index pngManIndex = GImgManager.LoadIdcIntoImageList(imgId,type);
  _controlTree->SetItemImage(item,pngManIndex);
}

#if _VBS3_DEBUG_DELAY_UI_CALL
wxString VirtualMachineTree::GetDelayCallText( DebugDelayCall* call ) const
{
  //UI calls use odd numbers for IDs
  wxString& type = call->GetId() % 2 == 0 ? wxString(L"") : wxString(L"[UI]");
#if _VBS_15079_RESTRICT_BISIM_SCRIPTS
  wxString name = GUtil.IsProtectedPath(call->GetName()) ? L"Protected Script" : call->GetName();
  return wxString::Format(L"[%i]%s %s [%f]",call->GetId(),type,name,call->GetDelay());
#else
  return wxString::Format(L"[%i]%s %s [%f]",call->GetId(),type,call->GetName(),call->GetDelay());
#endif
}
#endif
