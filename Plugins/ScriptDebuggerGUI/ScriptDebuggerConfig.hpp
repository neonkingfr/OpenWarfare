//Technology naming system - defines prefixed _VBS3_ have a corresponding technology in engine
//Defines prefixed _SD_ are plugin-only
//There are some old technologies not following this convention (prefixed with task number)

//////////////////
//IN DEVELOPMENT//
//////////////////
#define _SD_ESC_CLOSE_SEARCH _DEV //[VBS-22640] Search dialog can be closed by pressing ESC
#define _VBS2_SD_HELP_ENTRY 0 //disabled until we have a working help file
//Performance graph - not working now
#define _SD_PERF_LOG _DEV && 0
/////////////////////////
//FINISHED TECHNOLOGIES//
/////////////////////////

//INTERNAL (dev version only or restrictions required)
//Debug log - release version doesn't produce debug output anyway
#define _SD_DEBUG_LOG _DEV 
//Break all & Continue all functionality
#define _SD_BREAK_ALL (_DEV || _VBS_15079_RESTRICT_BISIM_SCRIPTS) && 1
//EH panel
#define _SD_EH_TAB (_DEV || _VBS_15079_RESTRICT_BISIM_SCRIPTS) && 1
//Virtual machine tree control
#define _SD_VM_TREE (_DEV || _VBS_15079_RESTRICT_BISIM_SCRIPTS) && 1
//Script log
#define _SD_SCRIPT_LOG (_DEV || _VBS_15079_RESTRICT_BISIM_SCRIPTS) && 1

//RELEASE only
//Restricts debugging BISIM scripts
#define _VBS_15079_RESTRICT_BISIM_SCRIPTS !_DEV && _VBS_14074_DEV_PATH_UPDATE_FIX

//PUBLIC (in release)
#define _ORIGINAL_FILE_CTD_FIX 1 //[VBS-22640] "View Original File" option now only shows up when original file is actually loaded.
#define _SD_DELAYCALL_BETTER_LOG 1 //[VBS-23802] Improves logging of delayCalls and centralizes script logging
#define _SD_EMPTY_PROFILE_FIX 1 //[VBS-23642] Fixed crash when exitting VBS with empty user directory
#define _SD_LIVEFOLDER_IMP 1 //[VBS-20885] Improves behavior of breakpoints when live-editing files
#define _VBS3_DEBUG_DELAY_UI_CALL 1 //[VBS-22166] Adds capabilty to debug delayUI calls
//#define _SD_EXECVM_BP_FIX 1 //Merged with _VBS3_SCRIPT_LINES on release [VBS-23710] Fixed VM content being shown instead of file content when breaking in execVM'd file - Merge with _VBS3_SCRIPT_LINES
//#define _SD_SAFER_FILE_PATH 1 //Merged with _VBS3_SCRIPT_LINES on release [VBS-22640] DebugScript will no longer store invalid filePaths
//#define _VBS3_MISSION_BP_LINES 1 //merged with _VBS3_SCRIPT_LINES on release [VBS-22246] Fixes breakpoints not working in mission preview if they were set in OME.
#define _VBS3_SCRIPT_LINES 1 //[VBS-20275] Changes script instructions pos to use relative line numbers (non-PP'd)
#define _VBS3_SD_WND_TITLE 1 //[VBS-21909] The script debugger window changes title based on VBS2 title 
#define _VBS3_DEBUG_DELAYCALL 1 //[VBS-20178] Enables debugging of delayCalls
#define _SD_ALLOW_SYSKEY_OVERRIDE 1 //[VBS-19618] Debugger will now override the F10-selects menu system behavior when stepping in files
#define _SD_ENGINE_FILE_NOSAVE 1 //[VBS-20612] Engine files can't be dirty and don't overwrite on save anymore
#define _SD_DEV_TREE_FIX 1 //[VBS-20206] The dev tree now lists files correctly.
// [VBS-20890] ScriptDebugger can now bring VBS to foreground in the new SDK builds
#define _SD_SDK_COMPAT 1
//[VBS-18072] Engine version of a script file gets reloaded upon detecting a CRC mismatch to account for live folder
#define _SD_ENGINE_CRC_RELOAD 1
//[VBS-18542] The debugger now collects script handles even when it is not connected, prevents crash when breaking on a script that started before the debugger connected
#define _SD_TRACK_SCRIPTS_DC 1
//[VBS-18138] The prompt to reload files doesn't show the first time a file is opened
#define _SD_FILETIME_INIT 1
//[VBS-18036] A couple of minor tweaks (mainly for search dialog handling) that should ease the work with the debugger a bit
#define _SD_UI_IMPROVEMENTS 1
//[VBS-17641] The debugger now recognizes live folder script files at all times
#define _SD_LIVEFOLDER_PATHS 1 
//[VBS-16838] Fixes debug log overlapping other windows in bottom notebook when showing the debugger window for the first time
#define _SD_FIX_BOTTOM_NTB_ARTIFACTS 1
//Sets the default font to be 10pt Courier New, adjusts indentation
#define _SD_DEFAULT_SCI_STYLE 1
#define _SCANDIR_CRASH_FIX 1 //[VBS-16812] fixes a random crash in release when updating mission directory
//Rework of breakpoints - they are no longer evaluated every frame -> a global manager is used to track them instead
#define _VBS3_SD_IMP_BREAKPOINTS 1 //[VBS-16603]
//Unpinned debugger windows are no longer shown before the main debugger window
#define _SD_UNPINNED_WINDOWS_SHOW_FIX 1 //[VBS-15901]
//Widgets destroy themselves properly when VBS closes
#define _VBS_15068_PROPER_SHUTDOWN 1
//Moves the debugger configuration to /Documents/VBS2NG/ScriptDebugger.cfg 
#define _SD_CONFIG_DOCUMENTS 1 //[VBS-15848]
//Number of lines in debug output is limited
#define _SD_LIMIT_OUTPUT_LINES 1 //[VBS-15509]
//Sets the splitter in the EH tab as far right as possible
#define _SD_EH_SPLITTER_POS 1 //[VBS-15337]
// Optimizes the algorithm for highlighting the current debug output.
#define _VBS_14551_OUTPUT_HIGHLIGHT_OPT 1
//Adds a new function to step into next script file [VBS-14287] 
#define _VBS_1428_STEP_FILE 1
//Fixes the mouseover variable value hints to also work with varnames containing numbers
#define _VBS_15193_MOUSOVER_VARS_FIX 1
// All breakpoints will be removed after a file is reloaded (preventing invisible breakpoints)
#define _VBS_14491_REMOVE_BREAKPOINTS_AFTER_RELOAD 1
//The scope label in debug console is now properly centered
#define _VBS_15260_SCOPE_LABEL_CENTERING_FIX 1
//Fixes a freeze when searching for an empty string
#define _VBS_15235_EMPTY_SEARCH_FREEZE 1
// Fixes scripts not breaking when changing dev path with opened scripts
#define _VBS_14074_DEV_PATH_UPDATE_FIX 1
//Fixes script log tooltip
#define _VBS_8320_TOOLTIP_FIX 1
//Fixed a bug in recognizing encoding in script files
#define _VBS_15145_SD_ENCODING_FIX 1
// The debugger shows full path to current script in the title bar and allows copying it to clipboard
#define _VBS_14850_SHOW_FULL_FILE_PATH 1
// Fixes a crash when closing VBS after loading a mission and going back to main menu
#define _VBS_14991_FILE_LINK_CRASH 1
// Adds a debug console that user can run scripts from
#define _VBS_14573_DEBUG_CONSOLE 1
//Removes the unused Active Script menu entry from Debug -> View menu
#define _VBS2_SD_MENU_RM_ACTIVE_SCRIPT 1