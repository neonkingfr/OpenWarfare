#pragma once

#include <vector>
#include <wx/string.h>

#include "ScriptDebuggerConfig.hpp"
#include "../../Common/iDebugEngineInterface.hpp"
#include "../../Common/Essential/RefCount.hpp"
#include "../../Common/Essential/AppFrameWork.hpp"


#ifndef MAX_PATH
    #define MAX_PATH  260
#endif

/**
 * File Line Entry is used by the ResolvePPFile, its used as a index for each entry found.
 */
struct FileLineEntry
{
  /// Line number the <b>#line</b> was on
  int _lineNum; 
  /// Offset from <b>#line</b> indicator
  int _lineOffset; 
  /// File path, will all-ways be relative and never absolute
  std::string _filePath;

  FileLineEntry(int lineNum,int lineOffset,std::string &filePath)
  {  
    _lineNum = lineNum;
    _lineOffset = lineOffset;
    _filePath = filePath;
  }
};


struct ResolvePPFile
{
  std::vector<FileLineEntry> _ppLineEntry;
  /// Process the PreProccessed call stack content to resolve all file line entry's
  void ProccessPreProccessorContent(std::string &content);
  /// Convert the source line, to its absolute position!
  int ConvertSoureLineToAbs(int sourceLine,std::string &path);
  /**
    * Convert the absolute position to the original file -1 - out of rang, not there yet!
    */
  int ConvertAbsToSourceLine(int absSourceLine);
  /// Get the file the absolute line is from!
  std::string GetFileFromAbs(int absSourceLine);
};

struct CallStackBuffer
{
  std::string _ppContent;
  ResolvePPFile _resolvePPFile;
  ICallStackItem *_callStack;

  CallStackBuffer()
  {
    _callStack = NULL;
  }

  void Update(ICallStackItem *callStack);
};

class DocumentViewer;

#if _VBS3_DEBUG_DELAYCALL
enum DebugScriptType
{
  DS_DELAYCALL, //the debug script is just a delay call wrapper
  DS_SCRIPT //the debug script actually refers to a script being run by the engine
};

class DebugDelayCall;
#endif

/**
  * The DebugScript acts as a Proxy object that hold onto the IDebugScript pointer. It's main 
  * responsibility is to ensure that all other components use DebugScript that is referenced counted
  * instead of the the IDebugScript that is manually deleted and created by the engine.
  * 
  * Because of this, when accessing the IDebugScript, to take into consideration that it may be NULL.
  */
class DebugScript : public RefCount 
{
  /// True, script is breaked, false script is not breaked.
  bool _break;
  /// True if script is running (being simulated), False if script is not running (paused state, waiting to run)
  bool _isRunning;
  /// True if the script is trying to step out from the current scope
  bool _isStepping;
  /// Should the focus go to VBS after this script's simulation is exited?
  bool _focusOnVBS;
  /// What step are we performing?
  StepKind _stepType;
  /// Call stack, to resolve the line number
  CallStackBuffer _resolvePP;
  /// Previous document the user was interacting with. (Related to callstack)
  WeakRef<DocumentViewer> _prevDoc;
  /// Get the current doc that the user interacts with 
  WeakRef<DocumentViewer> _curDoc;
  /// Script file path, (does not change)
  wxString _scriptFilePath;
#if _VBS3_SCRIPT_LINES
  /// Current instruction line num
  int _lineNum;
  //Tells the update script that _lineNum and _lineNumFilepath were just set by engine
  bool _currInstrSet;
#endif
  /// Current call stack file (does change)
  wxString _lineNumFilePath;
  /// The _script we're attached too
  IDebugScript *_script;  
  /// The current call stack index of the _script
  int _callStackIndex;
#if _VBS3_DEBUG_DELAYCALL
  // true - script should break on startup - only makes sense with delayCalls
  bool _shouldBreak;
  /// Type of this debug script (delayCall / script)
  DebugScriptType _type;
  /// The delay call this originates from
  SmartRef<DebugDelayCall> _delayCall;
#endif

  /// Initializes the private fields of the ScriptDebugger
  void Init();

public:
  /// Main constructor can start without any IDebugScript being attached
  DebugScript();
  /// Main constructor with IDebugScript that will be attached
  DebugScript(IDebugScript *script);
  /// De-constructor will be called when the <b>RefCount</b> of DebugScript reaches zero
  virtual ~DebugScript();

  /// User has clicked on the UI that has caused DebugScript to GainFocus
  void GainFocus();
  /// User has clicked on the UI that has caused DebugScript to LoseFocus
  void LostFocus();
  /// Update is called that updates the internal state of the ScriptDebugger
  void Update();
  /// Set marker for given lineNum in given virtualDoc and set focus on it
  void PrepareAndShowTab(DocumentViewer * virtualDoc, int lineNum);
  /// Opens document (if not open yet) for given lineNum and returns it
  DocumentViewer* OpenDocForLine(int absoluteLineNum);
  /// Opens documents for whole hierarchy of included files, useful for EH, otherwise mostly only some incl.hpp is shown
  DocumentViewer* OpenCompleteHiearchy(int lineNum);
  
  /**
   * Sets the top level of the call stack to be displayed to the user, Update() will need to be
   * called to display this to the user
   */
  void SetTopLevelCallStack();
  /// Returns the current call stack level, can be INT_MAX
  int GetCallStackIndex() const;
  /// Set the top level call stack
  void SetCallStackIndex(int index);

#if _VBS3_SCRIPT_LINES
  //Sets the path of the current instruction (passed in by engine in ::Breaked())
  void SetCurrInstr(const char * path, int line);
#endif
  /// Returns the scripts relative file path, <b>never</b> absolute
  const wxString &GetScriptFilePath() const;
  /// Return the current call stack file path
  const wxString &GetCallStackFilePath() const;

  /// Script has been broken, meaning that the script is running, and a breakpoint or has been paused to be debugged
  bool IsBreaked() const;
  /// Set break, indicates that the script is breaked or not
  void SetBreak(bool value);

  /// Running indicates that the current script is being run by the Virtual Machine
  bool IsRunning() const;
  /// Set the current running state. Indicate if the script is being run by the Virtual Machine or is waiting
  void SetRunning(bool value);

  /// Stepping out indicates that the script is trying to get out of the current scope -> might take a while (or forever) because the scope might contain a loop with sleeps/infinite loop
  bool IsStepping() const;
  /// Marks the script as trying to step out (or not).
  void SetStepping(bool value);
  /// Sets the step type we're performing (StepOut or StepOver)
  void SetStepType(StepKind value);
  /// Determines if VBS should gain focus after this script's simulation is exited -> Used to prevent VBS stealing focus while the script runs in a loop where
  /// it constantly enters and exits simulation
  void SetVBSFocusToggle(bool value);
  bool GetVBSFocusToggle() const;

  /// Gets the current DebugScript document that is rendering its current state.
  DocumentViewer *GetDisplayDoc() const;
  /// Returns the DebugScript IDebugScript member. The return value can be NULL
  IDebugScript *GetIDebugScript() const;
  /// Sets the DebugScript, it is valid this can be NULL to indicate that it's currently been unloaded by the engine
  void SetIDebugScript(IDebugScript*);

#if _VBS3_DEBUG_DELAYCALL
  //if the script is a delayCall then this flag controls if it should break when the delay runs out and the code runs
  bool ShouldBreak() const {return _shouldBreak;};
  void SetShouldBreak(bool val) {_shouldBreak = val;};
  //for now just delay call/script
  DebugScriptType GetType() const { return _type; };
  //pointer on the delayCall wrapper
  void SetDelayCall(DebugDelayCall* call);
  DebugDelayCall *GetDelayCall() const;
#endif
};

#if _VBS3_DEBUG_DELAYCALL
//This class keeps track of DelayCalls that are either waiting to be executed
//Or were already executed (in that case, the _delayCall ptr will be NULL)
class DebugDelayCall : public RefCount
{
private:
  IDebugDelayCall * _delayCall;
protected:
  wxString _name;
  DebugScript _script;
  unsigned int _id;
public:
  DebugDelayCall(IDebugDelayCall * delayCall);
  // The filename (or "")
  const wxString & GetName() const;
  // Remaining time to execute
  const float GetDelay() const;
  //The engine handle at the DelayCallNode (can be NULL)
  IDebugDelayCall * GetIDebugDelayCall() {return _delayCall;};
  //ID of the delayCallNode -> used for pairing up the delayCalls and executed scripts
  unsigned int GetId() const;
  //NULLS the pointer for _delayCall -> engine is responsible for destroying the object
  void SetCodeExecuted();
};
#endif