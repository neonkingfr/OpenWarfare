#include "ScriptDebuggerConfig.hpp"
#include <windows.h>

#include <map>
#include <string>
#include "wx/wx.h"

#if _VBS3_SD_IMP_BREAKPOINTS
class MissionDirInfo
{
private:
  bool _inTmpPreview; //internally tracks if we're currently in __tmppreview dir
  bool _changed;
  std::string _missionDir;
#if _VBS3_DEBUG_DELAYCALL
  //the current tmp preview dir, can be "" if we're not in tmp preview
  std::string _tmpPreviewDir;
#endif
  
public:
  MissionDirInfo() : _changed(false), _missionDir(""), _inTmpPreview(false)
  {}

  void Update(float deltaT);
  bool HasChanged() const; //did the mission change? By this we mean the actual mission -> not just directory changing from mission dir to __tmppreview
  const std::string& GetCurrentMissionDir() const; //current mission dir -> again means actual mission dir, not __tmppreview
#if _VBS3_DEBUG_DELAYCALL
  //the current tmp preview dir, can be "" if we're not in tmp preview
  const std::string& GetTmpPreviewDir() const;
#endif
};

//used instead of BreakPointLine for greater clarity
struct ScriptBreakpoint
{
  int _handle; //handle of the BP marker in scintilla
  int _sourceLineNumber; //remember that the number is offset by 1 as scintilla's indices start from 0, debugger indices from 1
  int _absSouceLineNumber; 
  bool _enabled; //same as enabling/disabling BPs in VS - unused now
  std::string _filenameEngine; //always relative path of the script file (even for missions!)
  std::string _filenameDoc; //relative (addons) or abs (missions) path of the script file
  DWORD _engineBPNum; //the engine handle for this breakpoint

  ScriptBreakpoint() : _handle(-1),_sourceLineNumber(-1),_absSouceLineNumber(-1),_enabled(false),_filenameEngine(""),_filenameDoc(""),_engineBPNum(-1)
  {}

  ScriptBreakpoint(const char *filenameEngine, const char *filenameDoc, int handle, int lineNumber, bool enabled);

#if _SD_LIVEFOLDER_IMP
  ScriptBreakpoint(const ScriptBreakpoint& other) : 
    _handle(other._handle), _sourceLineNumber(other._sourceLineNumber), _absSouceLineNumber(other._absSouceLineNumber),
      _enabled(other._enabled), _filenameEngine(other._filenameEngine), _filenameDoc(other._filenameDoc), _engineBPNum(other._engineBPNum)
  {}
#endif

  //compares source file & line number
  bool IsSameAs(const ScriptBreakpoint& other) const
  {
    return ( (strcmp(_filenameEngine.c_str(), other._filenameEngine.c_str()) == 0) && (_sourceLineNumber == other._sourceLineNumber) );
  }

  //also compares the engine handle
  bool operator==(const ScriptBreakpoint& other) const
  {
    return IsSameAs(other) && _engineBPNum == other._engineBPNum && _absSouceLineNumber == other._absSouceLineNumber && _handle == other._handle
      && (strcmp(_filenameDoc.c_str(), other._filenameDoc.c_str()) == 0);
  }

#if _SD_LIVEFOLDER_IMP
  bool operator!=(const ScriptBreakpoint& other) const
  {
    return !(operator==(other));
  }
#endif
};

//a class used for storing all breakpoint-related information
class ScriptBreakpointsManager
{
private:
  ScriptBreakpoint _nullBp; // a null-like value to compare against
  std::map<DWORD, ScriptBreakpoint> _breakpoints; //mapping of engine BP numbers to actual BP objects
public:
  ScriptBreakpointsManager()
  {}

  //O(logN) complexity
  ScriptBreakpoint& GetBreakpoint(DWORD id);

  //returns -1 if bp doesn't exists - O(N) complexity
  DWORD GetBreakpointId(const char *filenameEngine, int lineNumber) const;
  DWORD AddBreakpoint(const char *filenameEngine, const char *filenameDoc, int markerHandle, int lineNumber, int absLineNumber, bool enabled);

  bool RemoveBreakpoint(DWORD bp);
  bool EnableBreakpoint(DWORD bp, bool enabled);

  //Returns a filepath that can be used by the engine to compare breakpoints
  std::string GetEngineBPPath(const wxString& fileRelPath, const wxString& fileAbsPath) const;
  //recomputes mission breakpoints -> updates the enginePath for them
  void UpdateMissionBreakpoints(const std::string& storedMissionDir, const std::string& currMissionDir);
};
#endif