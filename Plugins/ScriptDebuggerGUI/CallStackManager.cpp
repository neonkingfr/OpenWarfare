#include <vector>
#include <string>

#include "../../Common/Essential/AppFrameWork.hpp"

#include "CallStackManager.hpp"

#include "UIEvents.hpp"
#include "MainWindow.hpp"

#include "Util.hpp"
#include "DocumentManager.hpp"

// Identifier to make up the tables
enum 
{
  ColCallNum,
  ColCallText,
  ColCallFile,
  ColSize
};

// -1 just means remaining space
// all other values are percentage
static int ColFormat[] = {3,50,-1};

CallStackManagerUI::CallStackManagerUI
(
  wxWindow *parent,
  const wxPoint& pos,
  const wxSize& size,
  long style,
  const wxString& name
):wxGrid(parent,-1,pos,size,style,name)
{
	// Grid
	CreateGrid( 1, ColSize );
	EnableEditing( true );
	EnableGridLines( true );
	EnableDragGridSize( false );

  SetColLabelValue(ColCallNum,L"#");
  SetColLabelValue(ColCallText,L"Call Stack");
  SetColLabelValue(ColCallFile,L"FilePath");

  SetScrollLineX(1);
  SetScrollLineY(1);
  SetMargins( 0, 0 );

  // This should be in the constructor.
  Connect(wxEVT_GRID_CELL_LEFT_DCLICK,wxGridEventHandler(CallStackManagerUI::OnLeftClickDouble), NULL, this);

  // Columns
  EnableDragColMove( false );
  EnableDragColSize( true );
  EnableDragRowSize( true );

  SetColLabelSize( 20 );
  SetColLabelAlignment( wxALIGN_CENTRE, wxALIGN_CENTRE );
  SetRowLabelSize( 0 );
  SetRowLabelAlignment( wxALIGN_CENTRE, wxALIGN_CENTRE );

  // Register the window, to receive state changes
  GMainWindow->RegisterWindow(this);
}


void CallStackManagerUI::DoSetSize(int x, int y,int width, int height, int sizeFlags)
{
  wxGrid::DoSetSize(x,y,width,height,sizeFlags);

  if (width<=0) return;

  const int CallNumWidth = 20;
  const int CallNumTextWidth = width-CallNumWidth > 0 ? width-CallNumWidth : width; 

  // Re-arrange the sizes of all the elements
  // Calculate the space, to resize everything.d
  wxSize winSize = GetSize();
  wxSize virt    = GetVirtualSize();
  for (int i=0; i<ColSize; i++)
  {   
    int width;
    if (ColFormat[i]==-1)
    {
      // Place the remaining amount
      width = winSize.x;
    }
    else
    {
      float withPer = (float) ColFormat[i];
      withPer*=0.01f;
      width = (int)(withPer*(float)winSize.x);
      winSize.x -= width;
    }

    SetColSize(i,width);
  }
}



void CallStackManagerUI::OnLeftClickDouble(wxGridEvent &event)
{
  int selRow = event.GetRow();
  GFrame->SetCallStackIndex(selRow);
}


CallStackManagerUI::~CallStackManagerUI()
{
  if (GMainWindow)  GMainWindow->UnRegisterWindow(this);
}

void CallStackManagerUI::SimulateUpdate(float deltaT)
{

}


void CallStackManagerUI::StateChange(DebugScript *selScript)
{
  // Lost focus
  if (!selScript ||  !selScript->GetIDebugScript())
  {
    ClearGrid();
    return;
  }

  IDebugScript *script = selScript->GetIDebugScript();
  int callStackSize = script->CallStackSize();
  if (callStackSize==0) return;

  // only render nonInline call stack
  std::vector<int> nonInline;
  nonInline.push_back(0);  
  for (int i=0; i<callStackSize; i++)
  {
    ICallStackItem *cs = script->GetCallStackItem(i);
    if (!cs->IsInline()) nonInline.push_back(i);
  }

  //! Only update the view when the focus
  wxSize sizer = GetSize();
  DeleteRows(0,GetNumberRows());
  AppendRows(nonInline.size());

  // Set the call stack grid value
  for (unsigned int i=0; i<nonInline.size(); i++)
  {
    ICallStackItem *callStack = script->GetCallStackItem(nonInline[i]);
    Assert(callStack);

    wxString num = wxString::Format(L"%d",i);
    wxString val = CharToUTF16(callStack->GetCallStackType());

    char filePathBuf[MAX_PATH] = {0};
    int filePathBufSize = MAX_PATH;
    callStack->GetFilePath(filePathBuf,filePathBufSize);
    
    SetCellValue(num,i,ColCallNum);
    SetCellValue(val,i,ColCallText);
#if _VBS_15079_RESTRICT_BISIM_SCRIPTS
    wxString pathWx = wxString::FromUTF8(filePathBuf);
    bool internalScript = GUtil.IsProtectedPath(pathWx);
    if (internalScript && GDocumentManager) //it's an internal path, see if maybe we have the script open which wouldn't restrict it
    {
      DocumentViewer* view = GDocumentManager->GetDocument(pathWx);
      if (view)
        internalScript = view->ContentsProtected(); //if the script is open, check if the contents (which includes the path) are protected
    }
    if (internalScript)
      SetCellValue(L"Internal Script",i,ColCallFile);
    else
#endif
      SetCellValue(CharToUTF16(filePathBuf),i,ColCallFile);
  }

  // Highlight the call stack level.
  int dispIndex = selScript->GetCallStackIndex();
  if (dispIndex>=callStackSize) dispIndex = callStackSize-1;

  SelectBlock(dispIndex,0,dispIndex,1);
}

