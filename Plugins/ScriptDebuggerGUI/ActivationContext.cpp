#ifndef _WIN32_WINNT
#define _WIN32_WINNT 0x0600
#endif

#include "ActivationContext.h"

// The Dll's manifest context. We need to set
// this context when entering in any of the 
// dll's functions.

// Activate our current dll's manifest activation context
void ActContextScope::Activate()
{
  if (_dllManifestAContext)
    ActivateActCtx(_dllManifestAContext, &_dllManifestCookie);
}
// Release our current dll's manifest activation context
void ActContextScope::DeActivate()
{
  if (_dllManifestAContext)
    DeactivateActCtx(0, _dllManifestCookie); 
}

void ActContextScope::SetDllActivateContext()
{
  GetCurrentActCtx(&_dllManifestAContext);
}
void ActContextScope::ForceActivateActivationContext()
{
  Activate();
}
void ActContextScope::ForceUnloadDllActivationContext()
{
  DeActivate();
}
ActContextScope::ActContextScope()
{
  Activate();
}
ActContextScope::~ActContextScope()
{
  DeActivate();
}

HANDLE ActContextScope::_dllManifestAContext;
ULONG_PTR ActContextScope::_dllManifestCookie;

#if 0
// Version control, to check the commCtrl that is being used by the application.
#pragma comment( lib, "comctl32.lib")
#include <commctrl.h >


#define PACKVERSION(major,minor) MAKELONG(minor,major)
#include "windows.h"
#include "windef.h"
#include "winbase.h"
#include "shlwapi.h"

DWORD GetVersion(LPCTSTR lpszDllName)
{
  HINSTANCE hinstDll;
  DWORD dwVersion = 0;

  /* For security purposes, LoadLibrary should be provided with a fully qualified 
  path to the DLL. The lpszDllName variable should be tested to ensure that it 
  is a fully qualified path before it is used. */
  hinstDll = LoadLibrary(lpszDllName);

  if(hinstDll)
  {
    DLLGETVERSIONPROC pDllGetVersion;
    pDllGetVersion = (DLLGETVERSIONPROC)GetProcAddress(hinstDll, "DllGetVersion");

    if(pDllGetVersion)
    {
      DLLVERSIONINFO dvi;
      HRESULT hr;

      ZeroMemory(&dvi, sizeof(dvi));
      dvi.cbSize = sizeof(dvi);

      hr = (*pDllGetVersion)(&dvi);

      if(SUCCEEDED(hr))
      {
        dwVersion = PACKVERSION(dvi.dwMajorVersion, dvi.dwMinorVersion);
      }
      FreeLibrary(hinstDll);
    }
  }
  return dwVersion;
}
#endif