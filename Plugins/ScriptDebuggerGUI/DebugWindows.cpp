#include "DebugWindows.hpp"

#define WXK_A 0x41
#define WXK_B 0x42
#define WXK_C 0x43
#define WXK_D 0x44
#define WXK_E 0x45
#define WXK_F 0x46
#define WXK_G 0x47
#define WXK_H 0x48
#define WXK_I 0x49
#define WXK_J 0x4A
#define WXK_K 0x4B
#define WXK_L 0x4C
#define WXK_M 0x4D
#define WXK_N 0x4E
#define WXK_O 0x4F
#define WXK_P 0x50
#define WXK_Q 0x51
#define WXK_R 0x52
#define WXK_S 0x53
#define WXK_T 0x54
#define WXK_U 0x55
#define WXK_V 0x56
#define WXK_W 0x57
#define WXK_X 0x58
#define WXK_Y 0x59
#define WXK_Z 0x5A

void UnpinnableWxFrame::PinWindow(wxFlatNotebookExt *notebook, WindowSettings &wndSettings)
{
  wndSettings.docked = true;

  _isDocked = true;
  SetWindowStyleFlag(wxBORDER_NONE | wxCLIP_CHILDREN); //remove the window resizable border and the _,X buttons
  notebook->AddPage(this, GetTitle());
  
  wxUndockableEntry info;
  info.notebookTab = this;
  info.windowPtr = this;

  notebook->registerUndockablePage(info);
  Refresh();
}

void UnpinnableWxFrame::UnpinWindow(WindowSettings &wndSettings)
{
  wndSettings.docked = false;
  wndSettings.size = wxSize(800,600);
  wndSettings.position = wxDefaultPosition;

  _isDocked = false;
 
  Reparent(NULL);
  Move(wndSettings.position);
  SetSize(wndSettings.size);
  //reset the style to default
  long wndStyle = wxDEFAULT_FRAME_STYLE;
  //and make it stay on top if checked
  if (wndSettings.stayOnTop)
  {
    wndStyle = wndStyle | wxSTAY_ON_TOP;
  }
  SetWindowStyleFlag(wndStyle);
  Refresh();
}

void UnpinnableWxFrame::LoadFromSettings(WindowSettings &wndSettings)
{
  SetSize(wndSettings.size);
  Move(wndSettings.position);

  //set window styles based on settings
  _isDocked = wndSettings.docked;
  if (_isDocked)
  {
      SetWindowStyleFlag(wxBORDER_NONE|wxCLIP_CHILDREN);
  }
  else //if it's not docked, then it might be maximized
  {
    Maximize(wndSettings.maximized);
    if (wndSettings.stayOnTop)
    {
      SetWindowStyleFlag(wxDEFAULT_FRAME_STYLE | wxSTAY_ON_TOP);
    }
  }
}

void UnpinnableWxFrame::RenewWindow(wxFlatNotebookExt * notebook)
{
  if (_isDocked && notebook)
  {
    int pgIndex = notebook->GetPageIndex(this);
    if (pgIndex == -1) //window was removed from the notebook
    {
      notebook->AddPage(this,GetTitle(),true);
    }
    else
    {
      notebook->SetSelection(pgIndex);
    }
  }
  else //set focus
  {
    SetFocus();
  }
}

#if _SD_DEBUG_LOG || _SD_SCRIPT_LOG
///////////////////////////////
// DEBUG OUTPUT TEXT CONTROL //
///////////////////////////////

BEGIN_EVENT_TABLE(wxDebugTextCtrl, wxTextCtrl)
  EVT_KEY_DOWN(wxDebugTextCtrl::OnKeyDown)
END_EVENT_TABLE()

wxDebugTextCtrl::wxDebugTextCtrl( DebugOutPutWindow* dbgWindow, wxWindow* parent, wxWindowID id, const wxString& value, const wxPoint& pos, const wxSize& size, long style, const wxValidator& validator, const wxString& name)
  : wxTextCtrl(parent,id,value,pos,size,style,validator,name)
{
  _dbgWindow = dbgWindow;
}

// We want to override the regular textCtrl key events and pass them up to the DebugWindow for handling
void wxDebugTextCtrl::OnKeyDown(wxKeyEvent & evt)
{
  if (_dbgWindow) //shouldn't be 0, but better be safe
    _dbgWindow->OnKeyEvent(evt);
  else
    evt.Skip();
}

#if _SD_LIMIT_OUTPUT_LINES
void wxDebugTextCtrl::RemoveFirstLines(int n)
{
  int pos = -1;

  wxString& text = GetValue();

  for (int i = 0; i < n; ++i)
  {
    pos = text.find(L"\n", pos+1);
    if (pos == -1)
      return;
  }
  Remove(0,pos+1);
}

BEGIN_EVENT_TABLE(wxMaxLinesTextCtrl, wxTextCtrl)
  EVT_SET_FOCUS(wxMaxLinesTextCtrl::OnFocusEvent)
  EVT_KILL_FOCUS(wxMaxLinesTextCtrl::OnFocusEvent)
END_EVENT_TABLE()

wxMaxLinesTextCtrl::wxMaxLinesTextCtrl(DebugOutPutWindow* dbgWindow, wxWindow* parent, wxWindowID id, const wxString& value, const wxPoint& pos, const wxSize& size, long style, const wxValidator& validator, const wxString& name)
  : wxTextCtrl(parent,id,value,pos,size,style,validator,name)
{
  _dbgWindow = dbgWindow;
}

void wxMaxLinesTextCtrl::OnFocusEvent(wxFocusEvent& evt)
{
  if (_dbgWindow)
    _dbgWindow->OnMaxLinesFocusChange(evt); //TODO check app exit
  evt.Skip();
}
#endif

///////////////////////
//DEBUG OUTPUT WINDOW//
///////////////////////
BEGIN_EVENT_TABLE(DebugOutPutWindow, wxFrame)
  EVT_KEY_DOWN(DebugOutPutWindow::OnKeyEvent)
  EVT_MENU(-1, DebugOutPutWindow::OnMenuSelected)
  EVT_BUTTON(-1, DebugOutPutWindow::OnClear)
  EVT_COMMAND(wxID_ANY, SearchWindowEvent, DebugOutPutWindow::OnSearchWindowEvent)
#if _SD_LIMIT_OUTPUT_LINES
  EVT_TEXT(ID_MAX_LINES_TEXT_CTRL, DebugOutPutWindow::OnMaxLinesCtrlChange)
  EVT_TEXT_ENTER(ID_MAX_LINES_TEXT_CTRL, DebugOutPutWindow::OnMaxLinesEnter)
#endif
END_EVENT_TABLE()

DebugOutPutWindow::DebugOutPutWindow(wxWindow* parent, wxString title, DebuggerWindows wndType):UnpinnableWxFrame(parent,wxID_ANY,title)
{
  //GUI
  CreateGUI();
  // Set the window icon.
  wxIcon icon;
  if (GImgManager.GetWxIcon(ICON_WINDOW,icon))
    SetIcon(icon);
  
  //size & pos setting
  _wndType = wndType;
  OutputWindowSettings& wndSettings = GSettings.GetOutputWindowSettings(_wndType);
  UnpinnableWxFrame::LoadFromSettings(wndSettings);
#if _SD_LIMIT_OUTPUT_LINES
  _lines = 0;
  _maxLines = wndSettings.GetMaxOutputLines();
  MaxLinesTextCtrl->ChangeValue(wxString::Format(L"%i",_maxLines));
#endif
  _filterDialog = NULL;
  _searchDialog = NULL;
  _searchData.Init();
  InitMenuBar();
  InitFilters();
#if _SD_UNPINNED_WINDOWS_SHOW_FIX
  if (!wndSettings.docked && GFrame && GFrame->IsShown())
#endif
    Show(true);
}

DebugOutPutWindow::~DebugOutPutWindow()
{
  if (GFrame)
  {
#if _SD_DEBUG_LOG
    if (_wndType == WND_DEBUG_OUTPUT)
    {
      GFrame->RemoveDebugWindow();
    }
 #if _SD_SCRIPT_LOG
    else
 #endif
#endif
#if _SD_SCRIPT_LOG
 #if !_SD_DEBUG_LOG
    if (_wndType == WND_SCRIPT_LOG)
 #endif
    {
      GFrame->RemoveScriptLogWindow();
    }
#endif
  }
}

void DebugOutPutWindow::CreateGUI()
{
  this->SetSizeHints( wxDefaultSize, wxDefaultSize );
	
	wxBoxSizer* bSizer69;
	bSizer69 = new wxBoxSizer( wxVERTICAL );
	
	m_panel6 = new wxPanel( this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL );
	wxBoxSizer* bSizer71;
	bSizer71 = new wxBoxSizer( wxVERTICAL );
	
	DebugOutputTextArea = new wxDebugTextCtrl(this, m_panel6, ConfigTextArea, wxEmptyString, wxDefaultPosition, wxSize( 800,600 ), wxHSCROLL|wxTE_LEFT|wxTE_MULTILINE|wxTE_PROCESS_ENTER|wxTE_PROCESS_TAB|wxTE_READONLY|wxTE_RICH|wxTE_NOHIDESEL );
	bSizer71->Add( DebugOutputTextArea, 1, wxEXPAND, 5 );
	
	Clear = new wxButton( m_panel6, wxID_ANY, wxT("Clear"), wxDefaultPosition, wxDefaultSize, 0 );
#if _SD_LIMIT_OUTPUT_LINES
  //bottom row (clear button + max lines limit)
  wxBoxSizer* bottomRowSizer = new wxBoxSizer(wxHORIZONTAL);
  bottomRowSizer->Add(Clear, 0, wxALIGN_LEFT|wxLEFT, 0);
#else
  bSizer71->Add( Clear, 0, wxALL, 5 );
#endif

#if _SD_LIMIT_OUTPUT_LINES
  //lines limit + label
  wxStaticText* linesLabel = new wxStaticText(m_panel6,wxID_ANY,wxT("Max lines : "));
  MaxLinesTextCtrl = new wxMaxLinesTextCtrl(this, m_panel6,ID_MAX_LINES_TEXT_CTRL, wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_PROCESS_ENTER);

  wxBoxSizer* bottomRowRightSizer = new wxBoxSizer( wxVERTICAL );
  wxBoxSizer* bottomRowRightSizer_h = new wxBoxSizer( wxHORIZONTAL );
  wxBoxSizer* bottomRowMiddleSizer = new wxBoxSizer ( wxVERTICAL );

  bottomRowRightSizer_h->Add(linesLabel, 0, wxALIGN_CENTER_VERTICAL, 0);
  bottomRowRightSizer_h->Add(MaxLinesTextCtrl, 0, wxRIGHT|wxALIGN_CENTER_VERTICAL, 0);

  bottomRowRightSizer->Add(bottomRowRightSizer_h, 0, wxALIGN_RIGHT, 0);
  bottomRowSizer->Add(bottomRowRightSizer, 1, wxEXPAND, 0);
  bSizer71->Add(bottomRowSizer,0,wxEXPAND,0);
#endif
	
	
	m_panel6->SetSizer( bSizer71 );
	m_panel6->Layout();
	bSizer71->Fit( m_panel6 );
	bSizer69->Add( m_panel6, 1, wxEXPAND, 5 );
	
	
	this->SetSizer( bSizer69 );
	this->Layout();
}

void DebugOutPutWindow::InitFilters()
{
  _defaultTextStyle = wxTextAttr(*wxBLACK);
  _highlightTextStyle = wxTextAttr(*wxRED);

  //load filters from settings
  OutputWindowSettings& wndSettings = GSettings.GetOutputWindowSettings(_wndType);
  InitACFilter(wndSettings.GetIncludeFilter(), _filterFSMs.include);
  InitACFilter(wndSettings.GetIncludeFilter(), _filterFSMs.include_wx);
  InitACFilter(wndSettings.GetExcludeFilter(), _filterFSMs.exclude);
  InitACFilter(wndSettings.GetExcludeFilter(), _filterFSMs.exclude_wx);
  InitACFilter(wndSettings.GetHighlighting(), _filterFSMs.highlight);
}

void DebugOutPutWindow::InitMenuBar()
{
  wxMenuBar * menuBar = new wxMenuBar(wxMB_DOCKABLE);
  _windowMenu = new wxMenu();
  _utilsMenu = new wxMenu();

  _windowMenu->Append(DEBUG_OUTPUT_WINDOW_MENU_PIN,wxT("Dock window"),wxT("Docks window to the bottom area in the main window"));
  _windowMenu->AppendCheckItem(DEBUG_OUTPUT_WINDOW_MENU_ONTOP,wxT("Stay on Top"),wxT(""));
  _windowMenu->Check(DEBUG_OUTPUT_WINDOW_MENU_ONTOP, GSettings.GetOutputWindowSettings(_wndType).stayOnTop);
  
  _utilsMenu->Append(DEBUG_OUTPUT_WINDOW_MENU_SEARCH,wxT("Search\tCtrl+F"),wxT(""));
  _utilsMenu->Append(DEBUG_OUTPUT_WINDOW_MENU_FILTER,wxT("Filters"),wxT("Opens the filtering dialog"));
  _utilsMenu->AppendCheckItem(DEBUG_OUTPUT_WINDOW_MENU_AUTOSCROLL,wxT("Disable auto scrolling\tCtrl+A"),wxT(""));
  _utilsMenu->Check(DEBUG_OUTPUT_WINDOW_MENU_AUTOSCROLL, !GSettings.GetOutputWindowSettings(_wndType).GetAutoscroll());
  _utilsMenu->SetClientData(this);
  _windowMenu->SetClientData(this);

  if (_isDocked) 
  {
    _windowMenu->Enable(DEBUG_OUTPUT_WINDOW_MENU_PIN,false);
  }

  menuBar->Append(_windowMenu,wxT("Window"));
  menuBar->Append(_utilsMenu,wxT("Utils"));
  this->SetMenuBar(menuBar);
}

//////////////////////////////////
//Debug output related functions//
//////////////////////////////////

DebuggerWindows DebugOutPutWindow::GetWindowType()
{
  return _wndType;
}

void DebugOutPutWindow::AppendDebugText(wxString & text, bool runFilter)
{
  wxStringTokenizer lineTokenizer(text,wxT("\n"),wxTOKEN_RET_DELIMS);
  
  while (lineTokenizer.HasMoreTokens())
  {
    wxString line = lineTokenizer.GetNextToken();
    //assume that the input was filtered already
    bool shouldPrint = true;
    if (runFilter) //but if it wasn't, filter each line
    {
      shouldPrint = FilterOutput(line.mb_str());
    }
    if (shouldPrint)
    {
#if _SD_LIMIT_OUTPUT_LINES
      ++_lines;
#endif
      #ifdef __WXMSW__ //disabling autoscrolling only works under windows
      bool autoscroll = GSettings.GetOutputWindowSettings(_wndType).GetAutoscroll();
      int pos, newpos;
      if (!autoscroll) //autoscroll disabled
      {
        DebugOutputTextArea->Freeze();
        //this disable autoscrolling trick is taken from wxWidgets docs. Apparently there is no better way to do it.
        pos = ::SendMessage( (HWND)DebugOutputTextArea->GetHWND(), EM_GETFIRSTVISIBLELINE, 0, 0 );
      }
      #endif
#if _SD_LIMIT_OUTPUT_LINES
      if (_lines > _maxLines + 100) //remove excess lines if at least 100 over the maximum
      {
        DebugOutputTextArea->RemoveFirstLines(_lines - _maxLines);
        _lines = _maxLines;
      }
#endif

      if (_filterFSMs.highlight.Search(line))
      {
        DebugOutputTextArea->SetDefaultStyle(_highlightTextStyle);
        DebugOutputTextArea->AppendText(line);
        DebugOutputTextArea->SetDefaultStyle(_defaultTextStyle);
      }
      else
      {
        DebugOutputTextArea->AppendText(line);
      }
      #ifdef __WXMSW__
      if (!autoscroll)
      {
        newpos = ::SendMessage( (HWND)DebugOutputTextArea->GetHWND(), EM_GETFIRSTVISIBLELINE, 0, 0 );
        ::SendMessage( (HWND)DebugOutputTextArea->GetHWND(), EM_LINESCROLL, 0, pos-newpos );
        DebugOutputTextArea->Thaw();
      }
      #endif
    }
  }
}

void DebugOutPutWindow::InitACFilter(wxString &stringFilter, AhoCorasick<char, std::string> & filter)
{
  filter.ClearNeedles();
  //parse the filter string and add the needles to the AC filter
  wxStringTokenizer tokenizer(stringFilter, wxT(";"));
  while (tokenizer.HasMoreTokens())
  {
    wxString needle = tokenizer.GetNextToken();
    if (needle == wxT("*"))
    {
      needle = wxT(""); //empty needle will match all
    }
    filter.AddNeedle(needle.mb_str(wxConvUTF8));
  };
  //compute the FSM
  filter.ComputeFSM();
}

void DebugOutPutWindow::InitACFilter(wxString &stringFilter, AhoCorasick<wxChar, wxString> & filter)
{
  filter.ClearNeedles();
  //parse the filter string and add the needles to the AC filter
  wxStringTokenizer tokenizer(stringFilter, wxT(";"));
  while (tokenizer.HasMoreTokens())
  {
    wxString needle = tokenizer.GetNextToken();
    if (needle == wxT("*"))
    {
      needle = wxT(""); //empty needle will match all
    }
    filter.AddNeedle(needle);
  };
  //compute the FSM
  filter.ComputeFSM();
}

void DebugOutPutWindow::SetFilters(wxString &include, wxString &exclude, wxString &highlight)
{
#if _VBS_14551_OUTPUT_HIGHLIGHT_OPT
  wxString oldHighlight = GSettings.GetOutputWindowSettings(_wndType).GetHighlighting();
#endif
  //store the current values
  GSettings.GetOutputWindowSettings(_wndType).SetFilters(include,exclude,highlight);
  //and save the settings
  GSettings.SaveConfigFile();

  //update the filters themselves
  InitACFilter(include, _filterFSMs.include);
  InitACFilter(exclude, _filterFSMs.exclude);
  InitACFilter(include, _filterFSMs.include_wx);
  InitACFilter(exclude, _filterFSMs.exclude_wx);
  InitACFilter(highlight, _filterFSMs.highlight);

#if _VBS_14551_OUTPUT_HIGHLIGHT_OPT
  //highlight current text if highlighting changed
  if (!oldHighlight.IsSameAs(highlight))
#endif
    HighlightAllOutput();
}

bool DebugOutPutWindow::FilterOutput(const char * str) const
{
  return (_filterFSMs.include.Search(str) && !_filterFSMs.exclude.Search(str));
}

void DebugOutPutWindow::HighlightAllOutput()
{
  DebugOutputTextArea->Freeze();
#if _VBS_14551_OUTPUT_HIGHLIGHT_OPT
  wxString& text = DebugOutputTextArea->GetValue();
  
  //init a FSM to search for all line breaks
  const wxString lineBreak(L"\n");

   //find all matches of the highlight strings, the algorithm guarantees to return them sorted
  std::vector<searchResult> highlights;
  _filterFSMs.highlight.FindAll(text,highlights);

  //clear old highlighting
  DebugOutputTextArea->SetStyle(0, text.length(),_defaultTextStyle);

  //now color the text
  int currBreakPos = text.find(lineBreak); //the classic find is faster for just a single string
  int lineLen = 0;
  if (currBreakPos != wxNOT_FOUND)
  {
    lineLen = currBreakPos;
    currBreakPos = 0;
  }
  else
  {
    lineLen = text.length();
  }
  int lineEnd = lineLen;

  for (size_t currResult = 0; currResult < highlights.size(); ++currResult)
  {
    //position of a matched highlight string 
    int pos = highlights[currResult].first;
    //make sure the currBreakPos is behind current highlight's pos
    while (currBreakPos != wxNOT_FOUND && pos > lineEnd)
    {
      int prevBreakPos = currBreakPos;
      currBreakPos = text.find(lineBreak,currBreakPos+1);
      if (currBreakPos != wxNOT_FOUND)
      {
        lineEnd = currBreakPos;
      }
      else //if this was the last lineBreak, the end is the end of entire text
      {
        lineEnd = text.length();
      }
      lineLen = lineEnd - prevBreakPos;
    }
    //color the entire line
    DebugOutputTextArea->SetStyle(lineEnd - lineLen, lineEnd, _highlightTextStyle);
  }
#else
  long charPos = 0;
  for (int line = 0; line < DebugOutputTextArea->GetNumberOfLines(); ++line)
  {
    //check if the line matches the highlight filter
    if (_filterFSMs.highlight.Search(DebugOutputTextArea->GetLineText(line)))
    {
      DebugOutputTextArea->SetStyle(charPos, charPos + (DebugOutputTextArea->GetLineText(line).Length()),_highlightTextStyle);
    }
    else //set other text style to default
    {
      DebugOutputTextArea->SetStyle(charPos, charPos + (DebugOutputTextArea->GetLineText(line).Length()),_defaultTextStyle);
    }
    charPos += DebugOutputTextArea->GetLineText(line).Length()+1;
  }
#endif
  DebugOutputTextArea->Thaw();
}

bool DebugOutPutWindow::ToggleAutoScroll()
{
  //actually change the setting for the application
  bool autoScroll = !GSettings.GetOutputWindowSettings(_wndType).GetAutoscroll(); //get the NEW value (invert the old)
  GSettings.GetOutputWindowSettings(_wndType).SetAutoscroll(autoScroll);
  //set the menu element to match (might have been toggled by a keypress)
  _utilsMenu->Check(DEBUG_OUTPUT_WINDOW_MENU_AUTOSCROLL, !autoScroll); //the value in the menu is inverted
  return autoScroll; //return the new value
}

#if _SD_LIMIT_OUTPUT_LINES
int DebugOutPutWindow::GetMaxLines() const
{
  return _maxLines;
}

void DebugOutPutWindow::SetMaxLines(int n, bool updateText)
{
  _maxLines = n;
  GSettings.GetOutputWindowSettings(_wndType).SetMaxOutputLines(n);
  if (updateText)
    MaxLinesTextCtrl->ChangeValue(wxString::Format(L"%i",n));
}
#endif

void DebugOutPutWindow::OnFindNext()
{
  if (_searchDialog)
  {
    //compute the search FSM first
    wxString searchString = _searchDialog->SearchString();
#if _VBS_15235_EMPTY_SEARCH_FREEZE
    if (!searchString.length()) //no point in searching for empty string
      return;
#endif
#if _VBS_14551_OUTPUT_HIGHLIGHT_OPT
    bool matchCase = _searchDialog->MatchCase();
    wxString text = DebugOutputTextArea->GetValue();
    if (!matchCase)
    {
      text.LowerCase(); //make everything lowerCase if we don't match case
      searchString.LowerCase();
    }
#endif
    //if the search differs from the last search, recompute the FSM and search from beginning
    if (!searchString.IsSameAs(_searchData.searchString,false))
    {
      _searchData.InitWith(searchString);
    }

#if _VBS_14551_OUTPUT_HIGHLIGHT_OPT
    int pos = text.find(searchString, _searchData.lastSearchPos);
#else
    searchResult res = _searchData.FSM.FindNext(DebugOutputTextArea->GetValue(),_searchData.lastSearchPos);
    int pos = res.first;
#endif
    if (pos != -1) //found something
    {
#if _VBS_14551_OUTPUT_HIGHLIGHT_OPT
      DebugOutputTextArea->SetSelection(pos, pos+searchString.length());
#else
      DebugOutputTextArea->SetSelection(pos - searchString.length(), pos + 1);
#endif
      DebugOutputTextArea->ShowPosition(pos);
#if _VBS_14551_OUTPUT_HIGHLIGHT_OPT
      _searchData.lastSearchPos = pos+1;
#else
      _searchData.lastSearchPos = pos;
#endif
    }
    else
    {
      wxMessageDialog message(_searchDialog,wxString::Format(wxT("There are no more occurences of \'%s\'."),_searchData.searchString),wxT("Search result"),wxOK|wxICON_INFORMATION);
      message.ShowModal();
      _searchData.lastSearchPos = 0;
    }
  }
}

void DebugOutPutWindow::OnFindAll()
{
  if (_searchDialog)
  {
    wxString searchString = _searchDialog->SearchString();
#if _VBS_15235_EMPTY_SEARCH_FREEZE
    if (!searchString.length()) //no point searching for an empty string
      return;
#endif
#if _VBS_14551_OUTPUT_HIGHLIGHT_OPT
    bool matchCase = _searchDialog->MatchCase();
    wxString text = DebugOutputTextArea->GetValue();
    if (!matchCase)
    {
      text.LowerCase(); //make everything lowerCase if we don't match case
      searchString.LowerCase();
    }
#endif
    if (!searchString.IsSameAs(_searchData.searchString,false))
    {
      _searchData.InitWith(searchString);
    }
    int prevResultsCount = _searchData.foundPositions.size();
#if _VBS_14551_OUTPUT_HIGHLIGHT_OPT
    int searchLen = searchString.length();
    int currPos = text.find(searchString, 0);
    while (currPos != wxNOT_FOUND)
    {
      _searchData.foundPositions.push_back(searchResult(currPos,searchLen));
      currPos = text.find(searchString, currPos + 1);
    }

    DebugOutputTextArea->Freeze();
    wxTextAttr orig;
    for (int i = prevResultsCount; i < _searchData.foundPositions.size(); ++i)
    {
      //clear the background color indicating the highlighted text
      searchResult& res = _searchData.foundPositions[i];
      DebugOutputTextArea->GetStyle(res.first, orig);
      wxTextAttr textStyle(orig.GetTextColour(),*wxGREEN);
      DebugOutputTextArea->SetStyle(res.first, res.first + searchLen, textStyle);
    }
#else
    _searchData.FSM.FindAll(DebugOutputTextArea->GetValue(), _searchData.foundPositions);
    DebugOutputTextArea->Freeze();
    int searchLen = searchString.length();
    for (int i = prevResultsCount; i < _searchData.foundPositions.size(); ++i)
    {
      //clear the background color indicating the highlighted text
      searchResult& res = _searchData.foundPositions[i];
      wxTextAttr orig;
      DebugOutputTextArea->GetStyle(res.first - searchLen + 1, orig);
      wxTextAttr textStyle(orig.GetTextColour(),*wxGREEN);
      DebugOutputTextArea->SetStyle(res.first - searchLen + 1, res.first + 1, textStyle);
      //store the length of the search string (the second entry is normally used to identify which search string occured at the pos, but we don't need that now)
      res.second = searchLen;
    }
#endif
    DebugOutputTextArea->Thaw();
  }
}

void DebugOutPutWindow::OnClearSearch()
{
  DebugOutputTextArea->Freeze();
  for (std::vector<searchResult>::iterator it = _searchData.foundPositions.begin(); it != _searchData.foundPositions.end(); ++it)
  {
    //clear the background color indicating the highlighted text
    wxTextAttr orig;
#if _VBS_14551_OUTPUT_HIGHLIGHT_OPT
    DebugOutputTextArea->GetStyle(it->first, orig);
#else
    DebugOutputTextArea->GetStyle(it->first - it->second + 1, orig);
#endif
    wxTextAttr textStyle(orig.GetTextColour(),*wxWHITE);
#if _VBS_14551_OUTPUT_HIGHLIGHT_OPT
    DebugOutputTextArea->SetStyle(it->first, it->first + it->second, textStyle);
#else
    DebugOutputTextArea->SetStyle(it->first - it->second + 1, it->first +1, textStyle);
#endif
  }
  _searchData.foundPositions.clear();
  DebugOutputTextArea->Thaw();
}

void DebugOutPutWindow::OnFocusNextSearchResult()
{
  if (!_searchData.foundPositions.empty())
  {
    long int pos = _searchData.foundPositions[_searchData.lastFocus].first;
    DebugOutputTextArea->ShowPosition(pos);
    ++_searchData.lastFocus;
    if (_searchData.lastFocus >= _searchData.foundPositions.size())
      _searchData.lastFocus = 0;
  }
}

void DebugOutPutWindow::OnUpdateSearchHistory()
{
  _searchData.prevSearchStrings.push_back(_searchDialog->SearchString());
}

/////////////////////
//MISC UI FUNCTIONS//
/////////////////////

// Clear the debug window.
void DebugOutPutWindow::OnClear(wxCommandEvent& event)
{
#if _SD_LIMIT_OUTPUT_LINES
  _lines = 0;
#endif
  if (DebugOutputTextArea) DebugOutputTextArea->Clear();
}

void DebugOutPutWindow::unpinWindow(wxWindow * frame)
{
	MyFrame * myFrame = dynamic_cast<MyFrame *> (frame);	
	if (myFrame == NULL) return;
  
  WindowSettings& wndSettings = GSettings.GetOutputWindowSettings(_wndType);
  //update the window and settings
  UnpinnableWxFrame::UnpinWindow(wndSettings);
  //enable the menu
  _windowMenu->Enable(DEBUG_OUTPUT_WINDOW_MENU_PIN,true);
  Show(true);
}

void DebugOutPutWindow::pinWindow(wxWindow * frame, wxFlatNotebookExt * notebook)
{
	MyFrame * myFrame  = dynamic_cast<MyFrame *> (frame);
	if( myFrame == NULL) return;

  WindowSettings& wndSettings = GSettings.GetOutputWindowSettings(_wndType);
  //update the window and settings
  UnpinnableWxFrame::PinWindow(notebook, wndSettings);
  //disable the pin action
  _windowMenu->Enable(DEBUG_OUTPUT_WINDOW_MENU_PIN,false);
}

void DebugOutPutWindow::ShowSearchDialog()
{
  if (!_searchDialog)
  {
#if _VBS_14551_OUTPUT_HIGHLIGHT_OPT
    _searchDialog = new DiagSearch(this,this,wxID_ANY,wxString::Format(wxT("Search %s"),GetTitle()),SD_SEARCH_FIND_ALL|SD_SEARCH_CASE_SENS,wxDefaultPosition,wxDefaultSize,wxDEFAULT_FRAME_STYLE|wxSIZE_AUTO_HEIGHT);
#else
    _searchDialog = new DiagSearch(this,this,wxID_ANY,wxString::Format(wxT("Search %s"),GetTitle()),SD_SEARCH_FIND_ALL,wxDefaultPosition,wxDefaultSize,wxDEFAULT_FRAME_STYLE|wxSIZE_AUTO_HEIGHT);
#endif
    _searchDialog->SetSearchHistory(_searchData.prevSearchStrings);
  }
  else
  {
    _searchDialog->SetFocus();
  }
#if _SD_UI_IMPROVEMENTS
  _searchDialog->SetSearchString(DebugOutputTextArea->GetStringSelection());
#endif
}

//EVT handling methods

//Must only be used for menus
void DebugOutPutWindow::OnMenuSelected(wxCommandEvent & event)
{
	int evtId = event.GetId();

  //grab the DebugOutPutWindow handle, we'll need it later
  DebugOutPutWindow * window = static_cast<DebugOutPutWindow*> (event.GetEventObject());
  switch (evtId)
  {
  case DEBUG_OUTPUT_WINDOW_MENU_PIN :
    {
      //get the window
      window->pinWindow(GFrame, GFrame->BottomNoteBook);
      break;
    }
  case DEBUG_OUTPUT_WINDOW_MENU_SEARCH :
    {
      ShowSearchDialog();
      break;
    }
  case DEBUG_OUTPUT_WINDOW_MENU_FILTER:
    {
      if (!_filterDialog) //only create new filter window if we don't have any
      {
        _filterDialog = new DiagFilter(this,wxID_ANY,wxT("Filter"),wxDefaultPosition,wxDefaultSize,wxDEFAULT_FRAME_STYLE|wxSIZE_AUTO_HEIGHT);
      }
      else //otherwise just set the focus to the opened one
      {
        _filterDialog->SetFocus();
      }
      break;
    }
  case DEBUG_OUTPUT_WINDOW_MENU_ONTOP:
    {
      WindowSettings& wndSettings = GSettings.GetOutputWindowSettings(_wndType);
      wndSettings.stayOnTop = !wndSettings.stayOnTop;
      window->ToggleWindowStyle(wxSTAY_ON_TOP);
      break;
    }
  case DEBUG_OUTPUT_WINDOW_MENU_AUTOSCROLL:
    {
      //toggle the autoscrolling
      window->ToggleAutoScroll();
      break;
    }
  }
}

void DebugOutPutWindow::OnKeyEvent(wxKeyEvent & event)
{
  bool caught = false;
  if (event.GetEventType() == wxEVT_KEY_DOWN)
  {
    if (event.GetModifiers() == wxMOD_CONTROL)
    {
      switch (event.GetKeyCode())
      {
      case (WXK_A): //ctrl+A -> toggle autoscroll
        {
          ToggleAutoScroll();
          caught = true;
          break;
        }
      case (WXK_X): //ctrl+X -> clear window
        {
          DebugOutputTextArea->Clear();
          caught = true;
          break;
        }
      case (WXK_F): //ctrl+F -> search
        {
          ShowSearchDialog();
          caught = true;
          break;
        }
      }
    }
    else
    {
      switch (event.GetKeyCode())
      {
      case (WXK_F2):
        {
          OnFocusNextSearchResult();
          caught = true;
          break;
        }
      default:
        {}
      }
    }
  }
  if (!caught)
    event.Skip();
}

void DebugOutPutWindow::OnSearchWindowEvent( wxCommandEvent& evt)
{
  switch (evt.GetId())
  {
  case SearchDiagClose :
    {
      _searchDialog = NULL;
      break;
    }
  case SearchDiagFindNext :
    {
      OnFindNext();
      break;
    }
  case SearchDiagFindAll:
    {
      OnFindAll();
      break;
    }
  case SearchDiagClear:
    {
      OnClearSearch();
      break;
    }
  case SearchDiagUpdateHistory:
    {
      OnUpdateSearchHistory();
      break;
    }
  default:
    {};
  }
}

#if _SD_LIMIT_OUTPUT_LINES
void DebugOutPutWindow::OnMaxLinesCtrlChange( wxCommandEvent& evt )
{
  long n;
  if ( MaxLinesTextCtrl->GetValue().ToLong(&n) )
  {
    _maxLinesWanted = n;
  }
  else if (MaxLinesTextCtrl->GetValue().length())
  {
    MaxLinesTextCtrl->ChangeValue(wxString::Format(L"%i", _maxLinesWanted));
  }
}

void DebugOutPutWindow::OnMaxLinesFocusChange(wxFocusEvent& evt)
{
  if (evt.GetEventType() == wxEVT_SET_FOCUS)
  {
    _maxLinesWanted = _maxLines;
  }
  else
  {
    SetMaxLines(_maxLinesWanted, false);
  }
}

void DebugOutPutWindow::OnMaxLinesEnter(wxCommandEvent& evt)
{
  SetMaxLines(_maxLinesWanted, false);
}
#endif

void DebugOutPutWindow::FilterDialogClosed()
{
  _filterDialog = NULL;
}

//////////////////////////////////
//FILTER DIALOG for DEBUG OUTPUT//
//////////////////////////////////

BEGIN_EVENT_TABLE(DiagFilter,wxFrame)
  EVT_BUTTON(wxID_OK,DiagFilter::OnButtonPressed)
  EVT_BUTTON(wxID_CANCEL,DiagFilter::OnButtonPressed)
#if _SD_UI_IMPROVEMENTS
  EVT_TEXT_ENTER(wxID_ANY,DiagFilter::OnEnter)
#endif
END_EVENT_TABLE()

DiagFilter::DiagFilter(DebugOutPutWindow* parent,wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style) : wxFrame(parent,id,title,pos,size,style)
{
  _debugWindow = parent;
  //panel filling the whole dialog + it's sizer
  wxPanel * mainPanel = new wxPanel(this);
  wxBoxSizer * panelSizer = new wxBoxSizer(wxVERTICAL);
  //sizer for the whole window
  wxBoxSizer * mainSizer = new wxBoxSizer(wxVERTICAL);
  //create the comboBoxes
  OutputWindowSettings& wndSettings = GSettings.GetOutputWindowSettings(_debugWindow->GetWindowType());
#if _SD_UI_IMPROVEMENTS
  _comboInclude = new wxComboBox(mainPanel, wxID_ANY, wndSettings.GetIncludeFilter(), wxDefaultPosition, wxDefaultSize, 0, 0, wxCB_DROPDOWN|wxTE_PROCESS_ENTER);
  _comboExclude = new wxComboBox(mainPanel, wxID_ANY, wndSettings.GetExcludeFilter(), wxDefaultPosition, wxDefaultSize, 0, 0, wxCB_DROPDOWN|wxTE_PROCESS_ENTER);
  _comboHighlight = new wxComboBox(mainPanel, wxID_ANY, wndSettings.GetHighlighting(), wxDefaultPosition, wxDefaultSize, 0, 0, wxCB_DROPDOWN|wxTE_PROCESS_ENTER);
#else
  _comboInclude = new wxComboBox(mainPanel, wxID_ANY, wndSettings.GetIncludeFilter(), wxDefaultPosition, wxDefaultSize, 0, 0, wxCB_DROPDOWN);
  _comboExclude = new wxComboBox(mainPanel, wxID_ANY, wndSettings.GetExcludeFilter(), wxDefaultPosition, wxDefaultSize, 0, 0, wxCB_DROPDOWN);
  _comboHighlight = new wxComboBox(mainPanel, wxID_ANY, wndSettings.GetHighlighting(), wxDefaultPosition, wxDefaultSize, 0, 0, wxCB_DROPDOWN);
#endif
  //fill them in with the history
  std::list<wxString>::const_iterator it;
  for (it = wndSettings.GetIncludeHistory().begin(); it != wndSettings.GetIncludeHistory().end(); ++it)
  {
    _comboInclude->Append(*it);
  }
  for (it = wndSettings.GetExcludeHistory().begin(); it != wndSettings.GetExcludeHistory().end(); ++it)
  {
    _comboExclude->Append(*it);
  }
  for (it = wndSettings.GetHighlightHistory().begin(); it != wndSettings.GetHighlightHistory().end(); ++it)
  {
    _comboHighlight->Append(*it);
  }
  //buttons
  _okButton = new wxButton(mainPanel,wxID_OK);
  _cancelButton = new wxButton(mainPanel,wxID_CANCEL);
  //labels
  wxStaticText * textInc = new wxStaticText(mainPanel,wxID_ANY,wxT("Include :"),wxDefaultPosition,wxDefaultSize,wxALIGN_RIGHT|wxST_NO_AUTORESIZE);
  wxStaticText * textExc = new wxStaticText(mainPanel,wxID_ANY,wxT("Exclude :"),wxDefaultPosition,wxDefaultSize,wxALIGN_RIGHT|wxST_NO_AUTORESIZE);
  wxStaticText * textHig = new wxStaticText(mainPanel,wxID_ANY,wxT("Highlight :"),wxDefaultPosition,wxDefaultSize,wxALIGN_RIGHT|wxST_NO_AUTORESIZE);
  wxSize labelSize(70,-1);
  textInc->SetMinSize(labelSize);
  textExc->SetMinSize(labelSize);
  textHig->SetMinSize(labelSize);

  //top row (include)
  wxBoxSizer * topRowSizer =  new wxBoxSizer(wxHORIZONTAL);
  topRowSizer->Add(textInc,0,wxALIGN_RIGHT|wxALIGN_CENTER_VERTICAL,0);
  topRowSizer->Add(_comboInclude,1,wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL,0);
  //middle row (exclude)
  wxBoxSizer * middleRowSizer = new wxBoxSizer(wxHORIZONTAL);
  middleRowSizer->Add(textExc,0,wxALIGN_RIGHT|wxALIGN_CENTER_VERTICAL,0);
  middleRowSizer->Add(_comboExclude,1,wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL,0);
  //highlight row
  wxBoxSizer * highlightRowSizer = new wxBoxSizer(wxHORIZONTAL);
  highlightRowSizer->Add(textHig,0,wxALIGN_RIGHT|wxALIGN_CENTER_VERTICAL,0);
  highlightRowSizer->Add(_comboHighlight,1,wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL,0);
  //bottom row (buttons)
  wxBoxSizer * bottomRowSizer = new wxBoxSizer(wxHORIZONTAL);
  bottomRowSizer->Add(_okButton,0,wxLEFT,5);
  bottomRowSizer->Add(_cancelButton,0,wxLEFT,5);
    
  panelSizer->Add(topRowSizer,1,wxALL|wxEXPAND|wxALIGN_CENTER,10);
  panelSizer->Add(middleRowSizer,1,wxALL|wxEXPAND|wxALIGN_CENTER,10);
  panelSizer->Add(highlightRowSizer,1,wxALL|wxEXPAND|wxALIGN_CENTER,10);
  panelSizer->Add(bottomRowSizer,1,wxALL|wxEXPAND|wxALIGN_CENTER,10);

  mainPanel->SetSizer(panelSizer);
  mainPanel->Layout();
  panelSizer->Fit(mainPanel);
  
  mainSizer->Add(mainPanel,1,wxEXPAND);
  SetSizer(mainSizer);
  Layout();

  Show();
}

DiagFilter::~DiagFilter()
{
  if (_debugWindow) //make sure to unregister the filter window in the debugOutPutWindow to prevent memory corruption
  {
    _debugWindow->FilterDialogClosed();
  }
}

void DiagFilter::OnButtonPressed(wxCommandEvent & evt)
{
  switch (evt.GetId())
  {
  case (wxID_OK):
    {
      //set the filters
      _debugWindow->SetFilters(_comboInclude->GetValue(),_comboExclude->GetValue(),_comboHighlight->GetValue());
      Destroy();
      break;
    }
  case (wxID_CANCEL):
    {
      Destroy();
      break;
    }
  }
}

#if _SD_UI_IMPROVEMENTS
void DiagFilter::OnEnter(wxCommandEvent& evt)
{
  wxCommandEvent buttonOkEvt;
  buttonOkEvt.SetId(wxID_OK);
  OnButtonPressed(buttonOkEvt);
}
#endif

#endif
DEFINE_EVENT_TYPE(SearchWindowEvent)

//////////////////
//SEARCH DIALOG //
//////////////////
BEGIN_EVENT_TABLE(DiagSearch, wxFrame)
EVT_BUTTON(wxID_ANY,DiagSearch::OnButtonClick)
#if _SD_UI_IMPROVEMENTS
EVT_SET_FOCUS(DiagSearch::OnFocus)
EVT_TEXT_ENTER(wxID_ANY,DiagSearch::OnEnter)
#endif
END_EVENT_TABLE()

DiagSearch::DiagSearch(wxFrame *parentWindow, wxWindow* searchWindow, wxWindowID id, const wxString& title, int dialogOptions, const wxPoint& pos, const wxSize& size, long style)
  :wxFrame(parentWindow,id,title,pos,size,style)
{
  _parentWindow = parentWindow;
  _searchWindow = searchWindow;
  //panel filling the whole dialog + it's sizer
  wxPanel* mainPanel = new wxPanel(this);
  wxBoxSizer* panelSizer = new wxBoxSizer(wxVERTICAL);
  //sizer for the whole window
  wxBoxSizer* mainSizer = new wxBoxSizer(wxVERTICAL);

  //search comboBox
#if _SD_UI_IMPROVEMENTS
  _comboSearch = new wxComboBox(mainPanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0, 0, wxCB_DROPDOWN|wxTE_PROCESS_ENTER);
#else
  _comboSearch = new wxComboBox(mainPanel, wxID_ANY, wxEmptyString, wxDefaultPosition, wxDefaultSize, 0, 0, wxCB_DROPDOWN);
#endif

  //search controls
  if (dialogOptions & SD_SEARCH_CASE_SENS)
    _checkMatchCase = new wxCheckBox(mainPanel, wxID_ANY, wxT("Case-sensitive"), wxDefaultPosition, wxDefaultSize, wxALIGN_RIGHT);
  else
    _checkMatchCase = NULL;
  if (dialogOptions & SD_SEARCH_WHOLE_WORD)
    _checkWholeWord = new wxCheckBox(mainPanel, wxID_ANY, wxT("Whole word only"), wxDefaultPosition, wxDefaultSize, wxALIGN_RIGHT);
  else
    _checkWholeWord = NULL;

  //buttons
  _buttonFindNext = new wxButton(mainPanel, IDC_BTN_FIND_NEXT, wxT("Find next"));
  if (dialogOptions & SD_SEARCH_FIND_ALL)
  {
    _buttonFindAll = new wxButton(mainPanel, IDC_BTN_FIND_ALL, wxT("Mark all"));
    _buttonClear = new wxButton(mainPanel, wxID_CLEAR, wxT("Clear marked"));
  }
  else
  {
    _buttonFindAll = NULL;
    _buttonClear = NULL;
  }
  _buttonClose = new wxButton(mainPanel, wxID_CLOSE, wxT("Close"));

  //ADD ctrls to sizers
  //buttons
  wxBoxSizer* buttonsSizer = new wxBoxSizer(wxVERTICAL);
  buttonsSizer->Add(_buttonFindNext,0, wxALIGN_LEFT);
  if (_buttonFindAll && _buttonClear)
  {
    buttonsSizer->Add(_buttonFindAll,0, wxALIGN_LEFT);
    buttonsSizer->Add(_buttonClear,0, wxALIGN_LEFT);
  }
  buttonsSizer->Add(_buttonClose,0, wxALIGN_LEFT);
  //search modifiers
  wxBoxSizer* modifiersSizer = new wxBoxSizer(wxVERTICAL);
  if (_checkMatchCase)
    modifiersSizer->Add(_checkMatchCase,0, wxALIGN_RIGHT);
  if (_checkWholeWord)
    modifiersSizer->Add(_checkWholeWord,0, wxALIGN_RIGHT);
  //add sizers to main sizers
  wxBoxSizer* bottomSizer = new wxBoxSizer(wxHORIZONTAL);
  int modSizerExpand = 0;
  if ( _checkMatchCase || _checkWholeWord )
    modSizerExpand = 1;

  bottomSizer->Add(modifiersSizer,modSizerExpand,wxALIGN_RIGHT|wxLEFT|wxRIGHT,5);
  bottomSizer->Add(buttonsSizer,1,wxALIGN_LEFT|wxLEFT|wxRIGHT,5);
  //search combobox
  wxBoxSizer* comboSizer = new wxBoxSizer(wxVERTICAL);
  comboSizer->Add(_comboSearch,0,wxALIGN_LEFT|wxEXPAND|wxALL,10);
  //search|controls
  panelSizer->Add(comboSizer,0,wxALIGN_LEFT|wxEXPAND);
  panelSizer->Add(bottomSizer,0,wxALIGN_LEFT|wxEXPAND);

  mainPanel->SetSizer(panelSizer);
  mainPanel->Layout();
  panelSizer->Fit(mainPanel);
  
  mainSizer->Add(mainPanel,1,wxEXPAND);
  SetSizer(mainSizer);
  Layout();
#if _SD_ESC_CLOSE_SEARCH
  ConnectEvents();
#endif

  Show();
  _comboSearch->SetFocus();
}

#if _SD_ESC_CLOSE_SEARCH
void DiagSearch::ConnectEvents()
{
  //_comboSearch is always there
  _comboSearch->Connect(wxEVT_KEY_DOWN, wxKeyEventHandler(DiagSearch::OnKeyDown), NULL, this);
  if (_checkWholeWord)
    _checkWholeWord->Connect(wxEVT_KEY_DOWN, wxKeyEventHandler(DiagSearch::OnKeyDown), NULL, this);
  if (_checkMatchCase)
    _checkMatchCase->Connect(wxEVT_KEY_DOWN, wxKeyEventHandler(DiagSearch::OnKeyDown), NULL, this);
  if (_buttonFindNext)
    _buttonFindNext->Connect(wxEVT_KEY_DOWN, wxKeyEventHandler(DiagSearch::OnKeyDown), NULL, this);
  if (_buttonFindAll)
    _buttonFindAll->Connect(wxEVT_KEY_DOWN, wxKeyEventHandler(DiagSearch::OnKeyDown), NULL, this);
  if (_buttonClear)
    _buttonClear->Connect(wxEVT_KEY_DOWN, wxKeyEventHandler(DiagSearch::OnKeyDown), NULL, this);
  if (_buttonClose)
    _buttonClose->Connect(wxEVT_KEY_DOWN, wxKeyEventHandler(DiagSearch::OnKeyDown), NULL, this);
}
#endif

DiagSearch::~DiagSearch()
{
  //just notify the parent that we're being destroyed
  wxPostEvent(_parentWindow,wxCommandEvent(SearchWindowEvent,SearchDiagClose));
  //the parent window is responsible for clearing the search window if _searchWindow is child of _parentWindow
  if (_parentWindow != _searchWindow && _searchWindow) //if the search window is different, notify it as well as it might want to clear history
  {
    wxPostEvent(_searchWindow,wxCommandEvent(SearchWindowEvent,SearchDiagClose));
  }
}

void DiagSearch::UpdateSearchHistory()
{
  if (_comboSearch->FindString(_comboSearch->GetValue()) == wxNOT_FOUND) 
  {
    wxPostEvent(_parentWindow, wxCommandEvent(SearchWindowEvent,SearchDiagUpdateHistory));
    _comboSearch->Append(_comboSearch->GetValue());
  }
}

bool DiagSearch::WholeWordOnly() const
{
  if (_checkWholeWord)
    return _checkWholeWord->GetValue();
  else
    return false;
}

bool DiagSearch::MatchCase() const
{
  if (_checkMatchCase)
    return _checkMatchCase->GetValue();
  else
    return false;
}

wxString DiagSearch::SearchString() const
{
  return _comboSearch->GetValue();
}

#if _SD_UI_IMPROVEMENTS
void DiagSearch::SetSearchString(const wxString& str)
{
  _comboSearch->SetValue(str);
}

void DiagSearch::OnFocus(wxFocusEvent& evt)
{
  if (evt.GetEventType() == wxEVT_SET_FOCUS)
  {
    _comboSearch->SetFocus();
  }
}

void DiagSearch::OnEnter(wxCommandEvent& evt)
{
  wxCommandEvent searchEvt;
  searchEvt.SetId(IDC_BTN_FIND_NEXT);
  OnButtonClick(searchEvt);
}
#endif

#if _SD_ESC_CLOSE_SEARCH
void DiagSearch::OnKeyDown(wxKeyEvent& evt)
{
  switch (evt.GetKeyCode())
  {
  case (WXK_ESCAPE):
    {
      Close(true);
      evt.Skip(false);
    }
    default:
    {
      evt.Skip(true);
    }
  }
}
#endif

void DiagSearch::OnButtonClick(wxCommandEvent& evt)
{
  switch (evt.GetId())
  {
  case (IDC_BTN_FIND_NEXT):
    {
      wxPostEvent(_searchWindow,wxCommandEvent(SearchWindowEvent,SearchDiagFindNext));
      UpdateSearchHistory();
      break;
    }
  case (IDC_BTN_FIND_ALL):
    {
      wxPostEvent(_searchWindow,wxCommandEvent(SearchWindowEvent,SearchDiagFindAll));
      UpdateSearchHistory();
      break;
    }
  case (wxID_CLEAR):
    {
      wxPostEvent(_searchWindow,wxCommandEvent(SearchWindowEvent,SearchDiagClear));
      break;
    }
  case (wxID_CLOSE):
    {
      Close();
      break;
    }
  default:
    {};
  }
}

void DiagSearch::SetSearchWindow(wxWindow* wnd)
{
  _searchWindow = wnd;
}

void DiagSearch::SetSearchHistory(std::vector<wxString> &prev)
{
  for (std::vector<wxString>::const_iterator it = prev.begin(); it != prev.end(); ++it)
  {
    _comboSearch->Append(*it);
  }
}

#include "../../Common/iperfLog.hpp"

IPerfCounters *GPerfCounter;
void WINAPI SetPerformanceCounter(IPerfCounters *perf)
{
  GPerfCounter = perf;
}

BEGIN_EVENT_TABLE(DiagFrameRateEx, DiagFrameRate)
END_EVENT_TABLE()

wxPlotData *m_PlotData;
DiagFrameRateEx::DiagFrameRateEx(wxWindow* parent):DiagFrameRate(parent)
{
  // Set the window icon.
  SetIcon(wxIcon(wxT("ICON_WINDOW")));
  Show(true);

  //EVENT HANDLING
  //add a right click event handler to the plot area - displaying the context menu
  PlotCtrl->GetPlotArea()->Connect(wxEVT_RIGHT_DOWN, wxMouseEventHandler(DiagFrameRateEx::showContextMenu));
  PlotCtrl->SetDefaultBoundingRect(wxRect2DDouble(-1.0,-1.0,2,2));

  _perfGraphCat = PropGrid->AppendCategory(L"Perf");
  m_PlotData = new wxPlotData(1000,true);
  PlotCtrl->AddCurve(m_PlotData);
}

int counters;
void DiagFrameRateEx::Simulate(float deltaT)
{
  if (counters>=1000) counters = 0;
  
  m_PlotData->SetXValue(counters,(double)counters);
  m_PlotData->SetYValue(counters,deltaT);

  counters++;

  // Update the window
  PlotCtrl->Redraw(wxPLOTCTRL_REDRAW_WHOLEPLOT);
}

void DiagFrameRateEx::showContextMenu( wxMouseEvent & event)
{
	wxMenu contextMenu;

	contextMenu.Append(PLOT_CONTEXT_MENU_ZOOM_IN,wxT("Zoom in"),wxT("Zoom In"));
	contextMenu.Append(PLOT_CONTEXT_MENU_ZOOM_OUT,wxT("Zoom out"),wxT("Zoom Out"));
	contextMenu.Append(PLOT_CONTEXT_MENU_ZOOM_DEFAULT,wxT("Default zoom"),wxT("Reset zoom to default"));
	contextMenu.AppendSeparator();
	contextMenu.Append(PLOT_CONTEXT_MENU_DEFAULT_ORIGIN,wxT("Reset to origin"),wxT("Centers the view on [0,0]."));

	//we have to add a pointer to the plotCtrl to the menu, otherwise the menuSelected eventHandler will have no way to access the control
	contextMenu.SetClientData(((wxWindow *)event.GetEventObject())->GetParent());
	contextMenu.Connect(wxEVT_COMMAND_MENU_SELECTED, wxCommandEventHandler(DiagFrameRateEx::onContextMenuSelect));

	//show the pop-up menu at cursor position
	wxPoint menuPos = event.GetPosition();
	PopupMenu(&contextMenu,menuPos);
}

void DiagFrameRateEx::onContextMenuSelect(wxCommandEvent & event)
{
	int id = event.GetId();
	//the plot control is passed in the eventObject's data
	wxPlotCtrl * plotCtrl = static_cast<wxPlotCtrl*>(static_cast<wxMenu*>(event.GetEventObject())->GetClientData());
	
	switch (id)
	{
	case PLOT_CONTEXT_MENU_ZOOM_IN :
		{
			wxRect2DDouble r = plotCtrl->GetViewRect();
			r.Scale(.67);
			r.SetCentre(plotCtrl->GetAreaMousePoint());
			plotCtrl->SetViewRect(r, true);
			break;
		}
	case PLOT_CONTEXT_MENU_ZOOM_OUT :
		{
			wxRect2DDouble r = plotCtrl->GetViewRect();
			r.Scale(1.5);
			r.SetCentre(plotCtrl->GetAreaMousePoint());
			plotCtrl->SetViewRect(r, true);
			break;
		}
	case PLOT_CONTEXT_MENU_ZOOM_DEFAULT :
		{
			wxRect2DDouble r = plotCtrl->GetDefaultBoundingRect();
			r.SetCentre(plotCtrl->GetAreaMousePoint());
			plotCtrl->SetViewRect(r,true);
			break;
		}
	case PLOT_CONTEXT_MENU_DEFAULT_ORIGIN :
		{
			plotCtrl->SetViewRect( plotCtrl->GetDefaultBoundingRect(),true);
			break;
		}
	}
};

DiagFrameRateEx::~DiagFrameRateEx()
{
  counters = 0;
  m_PlotData = NULL;
}

#if _VBS_14573_DEBUG_CONSOLE
//////////////////////////
// DEBUG CONSOLE WINDOW //
//////////////////////////

BEGIN_EVENT_TABLE(DebugConsoleWindow, wxFrame)
EVT_BUTTON(wxID_ANY, DebugConsoleWindow::OnButtonPressed)
EVT_MENU(wxID_ANY, DebugConsoleWindow::OnMenuSelected)
END_EVENT_TABLE()

DebugConsoleWindow::DebugConsoleWindow(wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style) :
  UnpinnableWxFrame(parent,id,title,pos,size,style)
{
  //create GUI
  //panel filling the whole dialog + it's sizer
  wxPanel* mainPanel = new wxPanel(this);
  wxBoxSizer* panelSizer = new wxBoxSizer(wxVERTICAL);
  //sizer for the whole window
  wxBoxSizer* mainSizer = new wxBoxSizer(wxVERTICAL);
  
  wxBoxSizer* buttonsSizer = new wxBoxSizer(wxHORIZONTAL);

  _btnRun = new wxButton(mainPanel, wxID_OK, wxT("Run"));
  _btnClear = new wxButton(mainPanel, wxID_CLEAR, wxT("Clear"));
  
  buttonsSizer->Add(_btnRun,0, wxALIGN_LEFT);
  buttonsSizer->Add(_btnClear,0, wxALIGN_LEFT);
  
  _code = new wxScintilla(mainPanel);
  _code->StyleClearAll();
  _code->SetUseHorizontalScrollBar(false);

#if _VBS_15260_SCOPE_LABEL_CENTERING_FIX
  _textScope = new wxStaticText(mainPanel,wxID_ANY,wxT("Global Scope"),wxDefaultPosition,wxDefaultSize,wxALIGN_CENTER|wxST_NO_AUTORESIZE);  
#else
  _textScope = new wxStaticText(mainPanel,wxID_ANY,wxT("Global Scope"),wxDefaultPosition,wxDefaultSize,wxALIGN_CENTER);  
#endif
  //make the font a bit larger
  wxFont font = _textScope->GetFont();
  font.SetWeight(wxFONTWEIGHT_BOLD);
  _textScope->SetFont(font);

  _textReturn = new wxTextCtrl(mainPanel, wxID_ANY,wxEmptyString, wxDefaultPosition, wxDefaultSize, wxTE_READONLY);
  wxStaticText* labelReturn = new wxStaticText(mainPanel, wxID_ANY,wxT("Return Value :"),wxDefaultPosition,wxDefaultSize, wxALIGN_RIGHT|wxST_NO_AUTORESIZE);

  wxBoxSizer* returnSizer = new wxBoxSizer(wxHORIZONTAL);
  returnSizer->Add(labelReturn, 0, wxALIGN_LEFT|wxALIGN_CENTER_VERTICAL );
  returnSizer->Add(_textReturn, 1, wxALIGN_LEFT);
  //the main layout setup
#if _VBS_15260_SCOPE_LABEL_CENTERING_FIX
  panelSizer->Add(_textScope, 0, wxALIGN_CENTER|wxTOP|wxEXPAND, 5);
#else
  panelSizer->Add(_textScope, 0, wxALIGN_CENTER|wxTOP, 5);
#endif
  panelSizer->Add(_code, 1, wxALIGN_CENTER|wxEXPAND|wxALL, 5);
  panelSizer->Add(returnSizer, 0, wxALIGN_CENTER|wxEXPAND|wxLEFT|wxRIGHT, 5);
  panelSizer->Add(buttonsSizer, 0, wxALIGN_CENTER|wxALL,5);

  mainPanel->SetSizer(panelSizer);
  panelSizer->Fit(mainPanel);
  mainSizer->Add(mainPanel, 1, wxEXPAND);
  SetSizer(mainSizer);
  Layout();

  //load window settings from settings
  // Set the window icon.
  wxIcon icon;
  if (GImgManager.GetWxIcon(ICON_WINDOW,icon))
    SetIcon(icon);
  
  //size & pos setting
  WindowSettings& wndSettings = GSettings.GetDebugConsoleWindowSettings();
  UnpinnableWxFrame::LoadFromSettings(wndSettings);

  //Init menu
  wxMenuBar * menuBar = new wxMenuBar(wxMB_DOCKABLE);
  _windowMenu = new wxMenu();

  _windowMenu->Append(DEBUG_CONSOLE_MENU_PIN,wxT("Dock window"),wxT("Docks window to the bottom area in the main window"));
  _windowMenu->AppendCheckItem(DEBUG_CONSOLE_MENU_ONTOP,wxT("Stay on Top"),wxT(""));
  _windowMenu->Check(DEBUG_CONSOLE_MENU_ONTOP, GSettings.GetDebugConsoleWindowSettings().stayOnTop);
  
  _windowMenu->SetClientData(this);

  if (_isDocked) 
  {
    _windowMenu->Enable(DEBUG_CONSOLE_MENU_PIN,false);
  }

  menuBar->Append(_windowMenu,wxT("Window"));
  this->SetMenuBar(menuBar);
#if _SD_UNPINNED_WINDOWS_SHOW_FIX
  if (!wndSettings.docked && GFrame && GFrame->IsShown())
#endif
    Show(true);
}

DebugConsoleWindow::~DebugConsoleWindow()
{
  if (GFrame)
    GFrame->RemoveDebugConsoleWindow();
}

void DebugConsoleWindow::ExecuteCode()
{
  std::string toEval = UTF16ToChar(_code->GetText());

  std::string returnVal;
  returnVal = GUtil.EvaulateValue(toEval);

  _textReturn->SetValue( wxString(returnVal.c_str(), wxConvUTF8) );
}

void DebugConsoleWindow::unpinWindow(wxWindow * frame)
{
  WindowSettings& wndSettings = GSettings.GetDebugConsoleWindowSettings();
  UnpinnableWxFrame::UnpinWindow(wndSettings);
  _windowMenu->Enable(DEBUG_CONSOLE_MENU_PIN,true);
  Show(true);
}

void DebugConsoleWindow::pinWindow(wxWindow * frame, wxFlatNotebookExt * notebook)
{
  WindowSettings& wndSettings = GSettings.GetDebugConsoleWindowSettings();
  UnpinnableWxFrame::PinWindow(notebook, wndSettings);
  _windowMenu->Enable(DEBUG_CONSOLE_MENU_PIN,false);
}

void DebugConsoleWindow::OnButtonPressed(wxCommandEvent& evt)
{
  switch (evt.GetId())
  {
  case (wxID_OK):
    {
      ExecuteCode();
      break;
    }
  case (wxID_CLEAR):
    {
      _code->SetText(L"");
      break;
    }
  default:
    {
      evt.Skip();
    }
  }
}

void DebugConsoleWindow::SetScope(const wxString & scope)
{
  _textScope->SetLabel(scope);
}

void DebugConsoleWindow::OnMenuSelected(wxCommandEvent &evt)
{
  switch (evt.GetId())
  {
  case (DEBUG_CONSOLE_MENU_PIN):
    {
      pinWindow(GFrame,GFrame->BottomNoteBook);
      break;
    }
  case (DEBUG_CONSOLE_MENU_ONTOP):
    {
      WindowSettings& wndSettings = GSettings.GetDebugConsoleWindowSettings();
      wndSettings.stayOnTop = !wndSettings.stayOnTop;
      if (!_isDocked)
        ToggleWindowStyle(wxSTAY_ON_TOP);
      break;
    }
  }
}
#endif
