#include "Util.hpp"
#include "BreakpointManager.hpp"

Util GUtil;

// Create the necessary frame work variables.
void CreateFrameWork(void *handle)
{
  GUtil.Init(handle);
}

// Convert the char (can be UTF-8, ACP code pages) to unified wide char Unicode
wxString CharToUTF16(const char *src, size_t len, unsigned int codePage )
{
  Assert(src);
  Assert(len!=-1);
  if (!len) len = strlen(src);

  int result = MultiByteToWideChar(codePage,0,src,len,NULL,0);
  if (!result) return wxString();

  wxString tmp;
  tmp.resize(result);
  result = MultiByteToWideChar(codePage,0,src,len,&tmp[0],result);
  if (!result) return wxString();

  return tmp;
}

std::string UTF16ToChar(const wchar_t *src, size_t len, unsigned int codePage)
{
  Assert(src);
  Assert(len!=-1);
  if (!len) len = wcslen(src);

  const int NoBestFit[] = 
  { 
    42,
    50220,
    50221,
    50222,
    50225,
    50227,
    50229,
    57002,
    57003,
    57004,
    57005,
    57006,
    57007,
    57008,
    57009,
    57010,
    57011,
    65000,
    65001,
    54936,
    0
  };
  const char *defaultChar = "?";
  int flag = WC_NO_BEST_FIT_CHARS;
  for (int i=0; NoBestFit[i]; i++)
  {
    if (codePage == NoBestFit[i])
    {
      defaultChar = NULL;
      flag = 0;
      break;
    }
  }

  int result = WideCharToMultiByte
  (
    codePage,
    flag,
    src,
    len,
    NULL,
    0,
    defaultChar,
    NULL
  );
  if (!result) return std::string();

  std::string tmp;
  tmp.resize(result);

  result = WideCharToMultiByte
  (
    codePage,
    flag,
    src,
    len,
    &tmp[0],
    result,
    defaultChar,
    NULL
  );
  if (!result) return std::string();

  return tmp;
}

std::string UTF16ToChar(const wxString &src, unsigned int codePage)
{
  return UTF16ToChar((const wchar_t*) &src[0],(size_t) src.size(),codePage);
}
wxString CharToUTF16(const std::string &src, unsigned int codePage )
{
  return CharToUTF16((const char*)&src[0],src.size(),codePage);
}

#if _VBS3_DEBUG_DELAYCALL
extern MissionDirInfo GCurrentMissionDir;

wxString Util::ConvertToRelativePath( wxString &absolutePath ) const
{
  if (absolutePath.Length()) //it makes sense to search for substrings
  {
    if (GSettings._developerPath.Length()) //see if it's on the developer path
    {
      if (absolutePath.Find(GSettings._developerPath) == 0)
      {
        return absolutePath.SubString(GSettings._developerPath.Length(), absolutePath.Length());
      }
    }
    if (GSettings._liveFolderPath.Length()) //see if the path is in live folder
    {
      if (absolutePath.Find(GSettings._liveFolderPath) == 0)
      {
        return absolutePath.SubString(GSettings._liveFolderPath.Length(), absolutePath.Length());
      }
    }
    if (absolutePath.Find(L":") == wxString::npos) //check if we have an absolute path
    {
      //no drive indicator means, that we already have a relativePath
      if (absolutePath[0] == '\\') //we don't want the _relPath to start with a backslash
      {
        return absolutePath.SubString(1,absolutePath.Length());
      }
#if _SD_DELAYCALL_BETTER_LOG
      else
        return absolutePath;
#endif
    }
    else
    {
      //it can still be a mission
      wxString missionDir(wxString::FromUTF8(GCurrentMissionDir.GetCurrentMissionDir().c_str()));
      if (absolutePath.Find(missionDir) == 0)
        return (absolutePath.SubString(missionDir.Length(),absolutePath.Length()));
      else
      {
        //can still be in tmp preview dir
        missionDir = wxString::FromUTF8(GCurrentMissionDir.GetTmpPreviewDir().c_str());
        if (missionDir.Length())
          if (absolutePath.Find(missionDir) == 0)
            return (absolutePath.SubString(missionDir.Length(),absolutePath.Length()));
      }
      return absolutePath;
    }
  }
  return L"";
}
#endif
