#pragma once

#include <vector>
#include <hash_map>

#include "wx/treectrl.h"

#include "../../Common/Essential/RefCount.hpp"
#include "../../Common/Essential/Hash/Hasher.hpp"

#include "DebugScript.hpp"
#include "wxUtil.hpp"

#include "iMainWindow.hpp"


// Developer files are all referenced counted...
class DeveloperFile : public RefCount
{
public:
  std::string _fileName;
  bool _isPP;
  std::string _preProccessed;
  
  // Filename is used as the key
  DeveloperFile(std::string fileName):_fileName(fileName),_isPP(false)
  {
  }

  // return the key used to identify the Developer file
  const char *GetKey()
  {
    return _fileName.data();
  }
};

// Some typedef for the mapping of files to their key
// Some typedef for the mapping of files to their key
typedef stdext::hash_map<const char*,SmartRef<DeveloperFile>,GenericHasher<const char*> > DeveloperMap;
typedef DeveloperMap::iterator DeveloperMapIterator;
typedef std::pair<const char*,SmartRef<DeveloperFile> > DeveloperMapPair;

#define _ONE_SEC 1.0f;
#define _REFRESH_TIME  3.0f*_ONE_SEC



class DeveloperDriveTree : public wxEvtHandler, public RefCount, public iViewWindow
{
public:
  // MainFrame Controls
  wxTreeCtrl *_tree;
  //! Main root 
  wxTreeItemId _rootId;

  float _delayCheck;
  std::string _missionDirectory;

  // Files found during population, hash table for finding and pre-proccessing the file
  DeveloperMap _developerMap;
  
  DeveloperDriveTree(wxTreeCtrl *tree);
  virtual ~DeveloperDriveTree();

  void RefreshDrive();
  bool ArePathTheSame();

  virtual void SimulateUpdate(float deltaT);

  void OnMouseLeftDoubleClick(wxMouseEvent &event);  

  DECLARE_EVENT_TABLE()
};