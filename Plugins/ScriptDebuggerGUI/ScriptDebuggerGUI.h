#define EXPORT __declspec(dllexport)

EXPORT void WINAPI RegisterHWND(void *hwnd, void *hins);
EXPORT void WINAPI RegisterCommandFnc(void *executeCommandFnc);
EXPORT void WINAPI OnSimulationStep(float deltaT);
EXPORT void WINAPI OnAfterSimulation(float deltaT);
EXPORT const char* WINAPI PluginFunction(const char *input);
EXPORT void WINAPI OnUnload();

class IToDll;
EXPORT IToDll * WINAPI ConnectToScriptDebugger(int abcVersion);

class IPerfCounters;
EXPORT void WINAPI SetPerformanceCounter(IPerfCounters *perf);