#define _CRT_SECURE_NO_WARNINGS

#include "../../Common/Essential/AppFrameWork.hpp"

#include "DeveloperDriveTree.hpp"
#include "DocumentManager.hpp"
#include "DocumentTabManager.hpp"

#include "UIController.hpp"
#include "MainWindow.hpp"
#include "BreakpointManager.hpp"
#include "ImgManager.hpp"
#include "resource.h"
#include "UIEvents.hpp"
#include "Util.hpp"

#include "ScriptDebuggerConfig.hpp"

#include "../../Common/Essential/ForEach.hpp"

#include "Settings.hpp"

#ifndef _WIN32
#define DIR_STR "/"
#define DIR_CHR '/'
#else
#define DIR_STR L"\\"
#define DIR_CHR L'\\'
#endif

BEGIN_EVENT_TABLE(DeveloperDriveTree, wxEvtHandler)
EVT_LEFT_DCLICK(DeveloperDriveTree::OnMouseLeftDoubleClick)
END_EVENT_TABLE()


struct FileLink : public std::vector< SmartRef<FileLink> >, public RefCount
{
  // Link to the parent
  FileLink *_parent;
  // Name of file/folder
  wxString _name;

  void Init()
  {
    _parent = NULL;
  }
  FileLink()
  {
      Init();
  }
  FileLink(FileLink *parent, wxString name)
  {
    Init();
    _parent = parent;
    _name = name;
  }

  int FindLink(const TCHAR *name)
  {
    int index = -1;
    for (size_t i=0;i<size(); i++)
      if (!_wcsicmp(name,(*this)[i]->_name.data())) return i;

    return index;
  }

  // Returns the file link, if it doesn't exists it creates it
  FileLink* Find(const TCHAR *name)
  {
    int index = FindLink(name);
    if (index==-1) 
    {
        index = size();
        push_back(new FileLink(this,name));
    }
    return (*this)[index];
  }

  // Return if its a file or folder
  bool IsFile() const
  {
    return !size();
  }

  template <typename Type>
  bool ForEach(const Type &functor) const
  {
    for (size_t i=0; i<size(); i++)
    {
      FileLink *link = (*this)[i].GetRef();
      if (functor(link)) return true;
    }
    return false;
  }

#if _VBS_14991_FILE_LINK_CRASH
  void RemoveChild(FileLink* child)
  {
    for (FileLink::iterator i = begin(); i != end(); ++i)
    {
      if ((*i).GetRef() == child)
      {
        erase(i);
        return;
      }
    }
  }

  ~FileLink()
  {
    if (_parent)
    {
      //make sure we remove ourselves from up the tree
      _parent->RemoveChild(this);
    }
    for (FileLink::iterator i = begin(); i!= end(); ++i)
    {
      //also make sure we clear the parent for our children
      (*i)->_parent = NULL;
    }
  }
#endif
};

SmartRef<FileLink> GDevRoot;

// Files extension to use
const TCHAR *FilesExt[] =
{
  L".sqf",
  L".sqs",
  NULL
};

/*
Couple situations here
p: 
p://VBS2//Customers
*/
void DeconstructPath(const TCHAR *path)
{
  TCHAR buffer[MAX_PATH] = {0};
  wcscpy_s(buffer,MAX_PATH-1,path);

  // Search from the base root
  FileLink *fileLink = GDevRoot;

  const TCHAR *pch = wcstok(buffer,L"\\");
  while (pch != NULL)
  {
    // search for file, if there is a file stop
    fileLink = fileLink->Find(pch);
    pch = wcstok(NULL, L"\\");
  }
}


#if _SCANDIR_CRASH_FIX
void ScanDir(wxString path)
{
  wxString wild=wxString(path)+wxString(L"\\*");
  WIN32_FIND_DATAW fInfo;

  HANDLE hFile = FindFirstFile(wild.data(),&fInfo);

  if( hFile != INVALID_HANDLE_VALUE )
  {
    do
    {
      wxString newPath= path + wxString(DIR_STR)+ wxString(fInfo.cFileName);
      if( fInfo.dwFileAttributes&FILE_ATTRIBUTE_DIRECTORY )
      {
        if( fInfo.cFileName[0]=='.' ) 
        {
          //LogF("\nhFile0 : %ld, fInfo %ld \n", hFile, &fInfo);
          continue;
        }

        ScanDir(newPath);
        //LogF("\nhFile1 : %ld, fInfo %ld \n", hFile, &fInfo);
        continue;
      }

      int i =0;
      bool match = false;
      do 
      {
        match = wcsstr(fInfo.cFileName,FilesExt[i])>0;
        i++;
      } while (FilesExt[i] && !match);
      if (!match)
      {
        //LogF("\nhFile2 : %ld, fInfo %ld \n", hFile, &fInfo);
        continue;
      }

      //LogF("\nhFile3 : %ld, fInfo %ld \n", hFile, &fInfo);
      DeconstructPath(newPath.data());
    }
#if _SD_DEV_TREE_FIX
    while(FindNextFile(hFile,&fInfo));
#else
    while( FindNextFile(hFile,&fInfo)==0 );
#endif
    FindClose( hFile );
  }
}
#else
void ScanDir(wxString path)
{
    wxString wild=wxString(path)+wxString(L"\\*");

    _wfinddata_t fInfo;
    long hFile=_wfindfirst(wild.data(),&fInfo);
    if( hFile!=-1 )
    {
      do
      {
        wxString newPath=path+ wxString(DIR_STR)+ wxString(fInfo.name);
        if( fInfo.attrib&_A_SUBDIR )
        {
          if( fInfo.name[0]=='.' ) continue;

          ScanDir(newPath);
          continue;
        }

        int i =0;
        bool match = false;
        do 
        {
          match = wcsstr(fInfo.name,FilesExt[i])>0;
          i++;
        } while (FilesExt[i] && !match);
        if (!match) continue;

        DeconstructPath(newPath.data());
      }
      while( _wfindnext(hFile,&fInfo)==0 );
      _findclose( hFile );
    }
}
#endif


DeveloperDriveTree::DeveloperDriveTree(wxTreeCtrl *tree)
{
  _tree = tree;

  // Add the root, all the roots from here.
  _tree->SetImageList(GImgManager.GetImageList());
  _rootId = _tree->AddRoot(L"Mission Directory");

  // Load the root icon
  _tree->SetItemImage(_rootId,GImgManager.LoadIdcIntoImageList(BMP_FOLDER));

  // Register the window, to receive state changes
  GMainWindow->RegisterWindow(this);

  _tree->PushEventHandler(this);

  // Refresh the drive.
  RefreshDrive();
}

void DeveloperDriveTree::RefreshDrive()
{
  // Create a new root node.
  GDevRoot = new FileLink(NULL,L"RootNode");

  // do a search path, and populate the entry.....
  wxString devPath  = CharToUTF16(_missionDirectory);
  if (devPath.size())
  {
    // Two possibility, p:\, or p:\myPath
    size_t pos = devPath.find_last_of('\\');
    if (pos!=-1)
      if (pos+1>=devPath.size()) devPath = devPath.substr(0,devPath.size()-1);

    // Scan the directory
    ScanDir(devPath);
  }

  // Populate the tree structure.
  struct PopulateTree
  {
    wxTreeCtrl *_tree;
    wxTreeItemId _parentId;
    
    PopulateTree(wxTreeCtrl *tree, wxTreeItemId parentId):
    _tree(tree)
    ,_parentId(parentId)
    {
    }

    bool operator()(const FileLink *link) const
    {
      wxTreeItemId appendedId = _tree->AppendItem(_parentId,wxString(link->_name.data()));
      _tree->SetItemData(appendedId,(wxTreeItemData *)link);

      // check if its it a file
      int idc = link->IsFile() ? BMP_DOC_DOC : BMP_FOLDER;
      _tree->SetItemImage(appendedId,GImgManager.LoadIdcIntoImageList(idc));

      // Add the item to the tree.
      PopulateTree funct(_tree,appendedId);
      link->ForEach(funct);
      return false;
    }
  };
  struct FindFileLink
  {
    wxString& _name;
    FileLink *&_found;
    
    FindFileLink(wxString& name, FileLink *&found):
    _name(name)
    ,_found(found)
    {
    }

    bool operator()(const FileLink *link) const
    {
      if (_wcsicmp(link->_name.data(),_name.data())==0)
      {
        _found = (FileLink*)link;
        return true;
      }

      // Add the item to the tree.
      FindFileLink funct(_name,_found);
      return link->ForEach(funct);
    }
  };

  // Delete the root node children.
  _tree->DeleteChildren(_rootId);

  char *getLeadingBackSlash = strrchr(&_missionDirectory[0],'\\');
  if (getLeadingBackSlash)
  {
    getLeadingBackSlash++;

    wxString missionDirName;
    missionDirName = CharToUTF16(getLeadingBackSlash);
  
    FileLink *dir = NULL;
    FindFileLink findFunct(missionDirName,dir);
    if (GDevRoot->ForEach(findFunct)) // found it
    {
      // set the mission directory name
      _tree->SetItemText(_rootId,missionDirName);

      // Perform a scan again
      PopulateTree functor(_tree,_rootId);

      // Populate from the directory
      dir->ForEach(functor);
    }
  }
}

#if _VBS3_SD_IMP_BREAKPOINTS
extern MissionDirInfo GCurrentMissionDir;
#endif

bool DeveloperDriveTree::ArePathTheSame()
{
#if _VBS3_SD_IMP_BREAKPOINTS
  //slightly more efficient version
  if (GCurrentMissionDir.HasChanged())
  {
    _missionDirectory = GCurrentMissionDir.GetCurrentMissionDir();
#else
  std::string path = GUtil.GetEngineDirectory(MISSION_DIR);

  if (_stricmp(_missionDirectory.data(),path.data())!=0)
  {
    // Dont switch, if the path contains
    if (path.find("__tmpPreviewDir")!=-1) return true;

    _missionDirectory = path;
#endif
    return false; // Paths are different update.
  }
    
  // Paths are the same
  return true;
}

void DeveloperDriveTree::SimulateUpdate(float deltaT)
{
#if _VBS3_SD_IMP_BREAKPOINTS
  //the check is way more effective now
  if (!ArePathTheSame()) RefreshDrive();
#else
  static float timeOut = _REFRESH_TIME;

  if (timeOut<=0.0f)
  {
    if (!ArePathTheSame()) RefreshDrive();

    timeOut = _REFRESH_TIME;
  }
  else
  {
    timeOut -= deltaT;
  }
#endif
}

DeveloperDriveTree::~DeveloperDriveTree()
{
  // Should free everything
  GDevRoot = NULL;
}

void DeveloperDriveTree::OnMouseLeftDoubleClick(wxMouseEvent &event)
{
  wxTreeItemId id = _tree->HitTest(event.GetPosition());
  if (id.IsOk())
  {
    // Get the file link
    FileLink *fileLink = (FileLink *)_tree->GetItemData(id);
    
    if (fileLink && !fileLink->size())
    {
      wxString reversePath;
      while(fileLink->_parent && fileLink->_parent!=GDevRoot.GetRef())
      {
        reversePath = wxString(L"\\") + fileLink->_name + reversePath;
        fileLink = fileLink->_parent;
      }
      reversePath =  fileLink->_name + reversePath;
      
      // First try to open a pre-existing file
      DocumentViewer *openDoc = GDocumentManager->GetDocument(reversePath);
      Assert(openDoc);
      if (openDoc->GetDocContents()&DOC_VM) openDoc = GDocumentManager->OpenFile(reversePath);

      // Bring the document into focus!
      if (openDoc) GDocumentTabManager->BringIntoFocus(openDoc);
    }
  }
  
  event.Skip();
}


