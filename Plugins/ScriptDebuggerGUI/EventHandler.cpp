#include "EventHandler.hpp"
#include "UIEvents.hpp"

#include <wx/propgrid/propgrid.h>
#include <wx/menu.h>

#include "../../Common/iDebugEngineInterface.hpp"
#include "Util.hpp"

EventHandlerList::EventHandlerList()
{
}

// Define the event handler
EventHandlerList GEventHandlerList;

void BindCat::SelectAll(wxPropertyGrid* grid)
{
  wxVariant var;
  var = true;

  for (unsigned int i=0; i<_enumList.size(); i++)
    grid->SetPropertyValue(_enumList[i],var);
}
bool BindCat::IsSelected(wxPropertyGrid* grid, unsigned int index)
{
  return grid->GetPropertyValue(_enumList[index]).GetBool();
}

PropertyGridEventHandler::PropertyGridEventHandler(wxPropertyGrid *propertyGrid)
{
  _propertyGrid = propertyGrid;

  //! Connect to the property grid, and add the event for the right mouse click
  _propertyGrid->Connect(wxEVT_RIGHT_UP,wxMouseEventHandler(PropertyGridEventHandler::OnRightMouseUp),NULL,this);
  _propertyGrid->Connect(wxEVT_COMMAND_MENU_SELECTED,wxCommandEventHandler(PropertyGridEventHandler::OnContextMenu),NULL,this);
}

extern IGameState *GGameState;

// Update the event manager with events
void PropertyGridEventHandler::PopulateEventHandler()
{
  int eventGroupSize = GGameState->GetNumEvents();
  int currentEventMask = -1;
  int currentEventGroupIndex = 0;
  for (int i=0; i<eventGroupSize; i++)
  {
    iEvent event;
    GGameState->GetEvent(i,event);

    // New add it
    if (currentEventMask!=event.groupMask)
    {
      currentEventGroupIndex = GEventHandlerList.size();
      currentEventMask = event.groupMask;
      
      EventGroup eventGroup;
      eventGroup._name = CharToUTF16(event.groupName);
      GEventHandlerList.push_back(eventGroup);
    }
    
    // Add the event
    GEventHandlerList[currentEventGroupIndex].push_back(CharToUTF16(event.eventName));
  }

  for (size_t i = 0; i<GEventHandlerList.size(); i++)
  {
    wxPGId cat = _propertyGrid->AppendCategory(GEventHandlerList[i]._name.data());

    BindCat newEnumCat;
    newEnumCat._cat = cat;

    for (size_t x=0; x<GEventHandlerList[i].size(); x++)\
    {
      wxPGId id = _propertyGrid->AppendIn(cat, wxBoolProperty(GEventHandlerList[i][x].data(), wxPG_LABEL, false) );
      _propertyGrid->SetPropertyAttribute(id,wxPG_BOOL_USE_CHECKBOX,1L,wxPG_RECURSE);

      newEnumCat._enumList.push_back(id);
    }

    _eventGrid.push_back(newEnumCat);
    _propertyGrid->Sort(cat);
    _propertyGrid->Collapse(cat);
  }
  _propertyGrid->Sort();
}


PropertyGridEventHandler::~PropertyGridEventHandler()
{
  // Disconnect the right up, event handler for the property grid.
  _propertyGrid->Disconnect(wxEVT_RIGHT_UP,wxMouseEventHandler(PropertyGridEventHandler::OnRightMouseUp),NULL,this);
}

void PropertyGridEventHandler::OnRightMouseUp(wxMouseEvent& event)
{
  event.Skip();
  _lastRightClickPos = event.GetPosition();

  wxMenu menu;
  menu.Append(EH_EVENT_SELECT_ALL,wxT("Select all"));
  menu.Append(EH_EVENT_SELECT_NONE,wxT("Select none"));
  _propertyGrid->PopupMenu(&menu,event.GetPosition());
}

void PropertyGridEventHandler::OnContextMenu(wxCommandEvent& menu)
{
  bool selectAll = menu.GetId() == EH_EVENT_SELECT_ALL ? true: false;

  wxPoint unScrolledPos = _propertyGrid->CalcUnscrolledPosition(_lastRightClickPos);
  wxPGProperty *propAtY = _propertyGrid->GetItemAtY(unScrolledPos.y);
  if (propAtY)
  {
    wxPGPropertyWithChildren *parent = dynamic_cast<wxPGPropertyWithChildren *>(propAtY);
    if (!parent) parent = propAtY->GetParent();
    // parent found, cycle the list and select all
    if (parent)
    {
      for (size_t i=0; i<parent->GetCount(); i++)
      {
        wxPGProperty *child = parent->Item(i);
        _propertyGrid->SetPropertyValue(child,selectAll);
      }
      return;
    }
  }

  // No parent and or property found just select all.
  wxPGId prop = _propertyGrid->GetFirstProperty();
  while (prop)
  {
    _propertyGrid->SetPropertyValue(prop,selectAll);
    prop = _propertyGrid->GetNextProperty(prop);
  }
}

const TCHAR *PropertyGridEventHandler::GetEventName(unsigned int eventGroup, unsigned int eventId)
{
   return GEventHandlerList[eventGroup][eventId].data();
}

bool PropertyGridEventHandler::IsEventSelected(unsigned int eventGroup, unsigned int eventId)
{
  return _eventGrid[eventGroup].IsSelected(_propertyGrid,eventId);
}
