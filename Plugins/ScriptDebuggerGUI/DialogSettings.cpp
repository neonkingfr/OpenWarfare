#include "DialogSettings.hpp"
#include "Util.hpp"
#include "settings.hpp"

#include "ImgManager.hpp"
#include "resource.h"
#include "MainWindow.hpp"

BEGIN_EVENT_TABLE(DialogSettingsEx,DialogSettings)
EVT_COMMAND(IDC_ADDBTN,wxEVT_COMMAND_BUTTON_CLICKED,OnButtonAddClicked)
EVT_COMMAND(IDC_RMBTN,wxEVT_COMMAND_BUTTON_CLICKED,OnButtonRemoveClicked)
EVT_COMMAND(wxID_OK, wxEVT_COMMAND_BUTTON_CLICKED,OnButtonOkClicked)
EVT_COMMAND(wxID_ANY, wxEVT_COMMAND_TEXT_ENTER,OnEnterPressed)
EVT_CLOSE(CloseSetting)
END_EVENT_TABLE()


#include "..\..\Common\Essential\PreprocC\preprocC.hpp"

#define _ENABLE_PP_DEFINITION 0


DialogSettingsEx::DialogSettingsEx( wxWindow* parent ):base(parent)
{
  // Set the Settings developer path, into the control.
  DeveloperPath->GetTextCtrl()->SetValue(GSettings._developerPath);
  // Make the text ctrl notify us about ENTER being pressed
  DeveloperPath->GetTextCtrl()->SetWindowStyle(DeveloperPath->GetTextCtrl()->GetWindowStyle() | wxTE_PROCESS_ENTER);
  
#if _ENABLE_PP_DEFINITION
  // For each, ppdefine add a new pp definition entry
  for (int i=0; i<GSettings._ppDefinitions.size(); i++)
  {
    AddNewPPDefintionEntry();
    _ppDefintions[i]->TextBoxPPDefintions->SetValue(GSettings._ppDefinitions[i]);
  }

  // If there is no, config with any ppDefintion's then add the default entry
  if (!GSettings._ppDefinitions.size()) AddNewPPDefintionEntry();
#else
  PPDefintionSText->Show(false);
#endif

  wxIcon icon;
  if (GImgManager.GetWxIcon(ICON_TRAY,icon))  SetIcon(icon);
  
  Show(true);
}

void DialogSettingsEx::CloseSetting(wxCloseEvent& event)
{
  // Place all settings into the settings.
#if _SD_LIVEFOLDER_PATHS
  GSettings._developerPath = DeveloperPath->GetTextCtrlValue().Lower();
#else
  GSettings._developerPath = DeveloperPath->GetTextCtrlValue();
#endif

#if _VBS_14074_DEV_PATH_UPDATE_FIX
  if (GFrame)
    GFrame->OnDevDriveChanged();
#endif

  GSettings._ppDefinitions.clear();
  for (size_t i=0; i<_ppDefintions.size(); i++)
    GSettings._ppDefinitions.push_back(_ppDefintions[i]->TextBoxPPDefintions->GetValue());

  GSettings.SaveConfigFile();


  // Do the data exchange here, the main program will know what to do
  event.Skip();
}

wxWindow *DialogSettingsEx::AddNewPPDefintionEntry()
{
  // First thing we do, is find the PPDefintionSText. To get the sizer...
  wxSizer *sizer = PPDefTemplate->GetSizer();
  
  // Next add to the sizer with new Add and remove PP directives.
  DiagAddDefintionTemplate *newEntry = new DiagAddDefintionTemplate(PPDefTemplate);
  sizer->Add( newEntry,0, wxEXPAND ,5);
  sizer->Layout();

  // Add the new definition to options menu.
  _ppDefintions.push_back(newEntry);

  return newEntry;
}

void DialogSettingsEx::OnButtonAddClicked(wxCommandEvent& event)
{
  AddNewPPDefintionEntry();
}

void DialogSettingsEx::OnButtonRemoveClicked(wxCommandEvent& event)
{
  // Do not delete the last one...
  if (_ppDefintions.size()==1) return;

  for (size_t i=0; i<_ppDefintions.size(); i++)
    if (_ppDefintions[i]->BtnSub==event.GetEventObject())
    {
      // now can destroy it safety
      _ppDefintions[i]->Destroy();
      _ppDefintions.erase (_ppDefintions.begin()+i);
      break;
    }

  PPDefTemplate->GetSizer()->Layout();
}

//close the window on enter
void DialogSettingsEx::OnEnterPressed(wxCommandEvent &event)
{
  Close();
}

//ok button also just closes the window
void DialogSettingsEx::OnButtonOkClicked(wxCommandEvent &event)
{
  Close();
}