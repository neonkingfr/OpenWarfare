#include <wx\defs.h>

// menu and control ids
enum
{
    EVENT_QUIT = wxID_EXIT,
    TM_EVENT_BREAK = wxID_HIGHEST,
#if _VBS3_DEBUG_DELAYCALL
    TM_EVENT_DELAYED_BREAK,
#endif
    TM_EVENT_CONTINUE,
    TM_EVENT_CONTINUE_ALL,
    TM_EVENT_CANCEL,
    TM_EVENT_TOGGLE_CACHE_MNGER,
    EH_EVENT_SELECT_ALL,
    EH_EVENT_SELECT_NONE,
    DOC_TREE_CLOSE,
    DOC_TREE_CLOSE_ALL,
    DOC_TREE_SHOW_FULL_PATH,
    DOC_TREE_SHOW_FILE_NAME,
    DOC_MNGER_CLOSING_DOC
};

enum
{
  TAB_DOC_ORIGINAL_FILE = 0,
  TAB_DOC_ORIGINAL_FILE_PP,
  TAB_DOC_ENGINE_FILE,
  TAB_DOC_ENGINE_FILE_PP,
#if _VBS_14850_SHOW_FULL_FILE_PATH
  TAB_DOC_COPY_FULL_PATH,
#endif
  TAB_DOC_END
};