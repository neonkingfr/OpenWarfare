#pragma once

#include <vector>
#include "wx/grid.h"

#include "../../Common/Essential/RefCount.hpp"
#include "DebugScript.hpp"
#include "iMainWindow.hpp"
#include "wxUtil.hpp"

class CallStackManagerUI : public wxGrid, public RefCount, public iViewWindow
{
public:
  // Normal constructor, inits all the controls.
  CallStackManagerUI
  (
    wxWindow *parent, 
    const wxPoint& pos = wxDefaultPosition,
    const wxSize& size = wxDefaultSize,
    long style = wxWANTS_CHARS|wxBORDER_NONE,
    const wxString& name = wxGridNameStr
  );
  virtual ~CallStackManagerUI();

  virtual void DoSetSize(int x, int y,int width, int height, int sizeFlags = wxSIZE_AUTO);

  //! UI Controls
  void OnLeftClickDouble(wxGridEvent &event);

  // Call from other controls
  void StateChange(DebugScript *);
  //! Simulation for populating the field.
  void SimulateUpdate(float deltaT);
};