#pragma once

#include "wx/treectrl.h"
#include "../../Common/Essential/RefCount.hpp"
#include "../../Common/iDebugEngineInterface.hpp"
#include <vector>
#include "wxUtil.hpp"
#include "DebugScript.hpp"

#include "iMainWindow.hpp"

class WatchManagerUI : public wxGrid, public RefCount, iViewWindow
{
public:
  // Normal constructor, inits all the controls.
  WatchManagerUI
  (
    wxWindow *parent, 
    const wxPoint& pos = wxDefaultPosition,
    const wxSize& size = wxDefaultSize,
    long style = wxWANTS_CHARS|wxBORDER_NONE,
    const wxString& name = wxGridNameStr
  );

  virtual void DoSetSize(int x, int y,int width, int height, int sizeFlags = wxSIZE_AUTO);


  WatchManagerUI();
  virtual ~WatchManagerUI();


  void StateChange(DebugScript*);
  void SimulateUpdate(float deltaT);
};