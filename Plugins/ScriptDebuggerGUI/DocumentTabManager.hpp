#pragma once


#include <vector>
#include <string>
#include <wx/wxFlatNotebook/wxFlatNotebook.h>

#include "../../Common/Essential/RefCount.hpp"
#include "../../Common/Essential/AppFrameWork.hpp"


#include "Document.hpp"


class DocumentTabManager : public wxEvtHandler, public RefCount
{
  wxFlatNotebook *_flatNoteBook;
public:
  /// Constructor for the notebook
  DocumentTabManager(wxFlatNotebook *noteBook); 
  virtual ~DocumentTabManager();

  void OnClose(wxCloseEvent &event);

  /// Add the following document to the tab
  void AddTab(DocumentViewer *document);
  wxWindow *SelectedTab();

  void OnPageClosing(wxFlatNotebookEvent &event);
  void OnPageChanged(wxFlatNotebookEvent &event);
  void OnRightDown(wxMouseEvent &event);
  void OnContextMenu(wxCommandEvent &event);

  void SetTabName(DocumentViewer *);
  void SetTabColor(DocumentViewer *,const wxColour&);
  void ResetTabColor(DocumentViewer *);

  void BringIntoFocus(DocumentViewer *);
  void RemoveTab(DocumentViewer *);

  wxFlatNotebook *GetNoteBook(){return _flatNoteBook;}

  DECLARE_EVENT_TABLE()
};

extern SmartRef< DocumentTabManager > GDocumentTabManager;