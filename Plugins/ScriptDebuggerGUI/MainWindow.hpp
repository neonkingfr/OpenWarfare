// Copyright (C) 2012 Bohemia Interactive Simulations - support@BISimulations.com
//
// Purpose:		Main window for render the ScriptDebuggerGUI
//
// Author: Chad Lion & Martin Kolombo
// Date: 17-07-2012
//

#pragma once
#include "Icons/debuggerIcons.hpp"

#include "wx/wx.h"
#include "wx/image.h"
#include "wx/xrc/xmlres.h"
#include "wx/notebook.h"
#include "wx/richtext/richtextctrl.h"
#include "wx/event.h"

#include "wx/stdpaths.h"
#include "wx/filedlg.h"
#include "wx/dirdlg.h"
#include "wx/timer.h"
#include "wx/taskbar.h"
#include "wx/statbmp.h"
#include "wx/grid.h"
#include "wx/treectrl.h"
#include "wx/aui/auibook.h"
#include "wx/aboutdlg.h"
#include "wx/tokenzr.h"

#include <vector>

#include "../../Common/Essential/RefCount.hpp"
#include "DebugScript.hpp"
#include "wxUtil.hpp"

#include "iMainWindow.hpp"

#include "Document.hpp"
#include "appInfo.hpp"
#include "appVersion.hpp"

#include <Windows.h>
#include <WinBase.h>
#include "../../Common/Essential/AppFrameWork.hpp"
#include "Util.hpp"

///! Auto Generated file
#include "ui/FormBuilderGUI.h"
#include "DialogSettings.hpp"
#include "DebugWindows.hpp"


#define REMOVE_PAGE_FIX 1

class  MyFrame;
class MyTaskBarIcon : public wxTaskBarIcon , public RefCount
{
  MyFrame *_frame;
public:
  MyTaskBarIcon(MyFrame *mainFrame);

  void OnLeftButtonDClick(wxTaskBarIconEvent&);
  void OnHideWiddow(wxCommandEvent&);
  void OnShowWindow(wxCommandEvent&);
#if _CONSOLE
  void OnMenuExit(wxCommandEvent&);
#endif
  virtual wxMenu *CreatePopupMenu();

  DECLARE_EVENT_TABLE()
};

class DebugOutPutWindow;
class DiagFrameRateEx;

class DeveloperDriveTree;
#if _SD_VM_TREE
class VirtualMachineTree;
#endif
class WatchManagerUI;
class CallStackManagerUI;
#if _SD_EH_TAB
class PropertyGridEventHandler;
#endif
class DialogStyleEx;
class DiagSearch;
#if _VBS_14573_DEBUG_CONSOLE
class DebugConsoleWindow;
#endif

#define _DEVELOPER_DRIVE 1

class MyFrame : public DebugScriptWindow, public RefCount, public IToDll, public iMainWindow
{ 
  typedef DebugScriptWindow base;

  SmartRef<MyTaskBarIcon> _myTaskBar;
 
  /// Smart Ref, counting of scripts
  std::vector< SmartRef<DebugScript> > _scripts;

  /// The script that has focus..
  SmartRef<DebugScript> _focus;

  std::vector<iViewWindow*> _iWindows;

#if _SD_DEBUG_LOG
  DebugOutPutWindow *_debugWindow;
#endif
#if _SD_SCRIPT_LOG
  DebugOutPutWindow *_scriptLogWindow;
#endif
  DiagFrameRateEx *_perfWindow;
  DialogSettingsEx *_settingWindow;
  DialogStyleEx *_docStyleWindow;
#if _VBS_14573_DEBUG_CONSOLE
  DebugConsoleWindow *_debugConsole;
#endif

#if _DEVELOPER_DRIVE
  DeveloperDriveTree *_developerDriveManager;
#endif

#if _SD_VM_TREE
  VirtualMachineTree *_treeManager;
#endif
#if _SD_EH_TAB
  PropertyGridEventHandler *_eventManager;
#endif
  WatchManagerUI *_watchManager;
  CallStackManagerUI *_callStackManager;
  wxTextCtrl * _errorManager;

  DiagSearch* _searchWindow;
  std::vector<wxString> _searchHistory;

#if _VBS3_SD_WND_TITLE
  wxString _vbsWindowTitle;
#endif
  /// Active document
  DocumentViewer *_doc; 

  /// AUI manager perspective
  wxString _auiMgnerView;
  wxString _auiTabMgnerView;
  wxString _auiDebugPaneDefaultView;
  wxString _auiEHPaneDefaultView;
  wxString _auiFilePaneDefaultView;

  //UI
  void SaveWindowLayoutToSettings();
  //initializes the bottom notebook tabs
  //reset will reset all the tabs to default settings overriding GSettigns (window properties only -> does not reset filters)
 void SetUpBottomNotebook(bool reset = false);

  DebugScript *GetDebugScript(IDebugScript *script);
  wxString GetScriptName(IDebugScript *script);
  void SetBreakAll(bool value);
public:
  MyFrame( wxWindow* parent=(wxWindow *)NULL);
  virtual ~MyFrame();

  void Startup();
  void Shutdown();

/**
  * Set the document in focus to the current script
  * previous script is called with LostFocus, and new one
  * is called with GainFocus.
  */
  void SetFocus(DebugScript *script);
  /// Set and retrieve the focus.
  DebugScript *GetFocus();

#if _SD_ALLOW_SYSKEY_OVERRIDE
  bool IsScriptBreaked() const;
#endif

  //-------------- Start: IDebugScript API --------------
  void ScriptLoaded(IDebugScript *script, const char *name);
  void ScriptEntered(IDebugScript *script);
  void ScriptSimulationEntered(IDebugScript *script);
  void ScriptSimulationExited(IDebugScript *script);
  void SetupStepInto(IDebugScript *script, bool stepInto = false);
  void ScriptTerminated(IDebugScript *script);
  void FireBreakpoint(IDebugScript *script, unsigned int bp);
#if _VBS3_SCRIPT_LINES
  void Breaked(IDebugScript *script, const char * filename, int line);
#else
  void Breaked(IDebugScript *script);
#endif
  void OnError(int error, const char *content, const char *errorMsg, int line, const IGameState *gamestate);
  void GlobalGameState(IGameState *gameState);
  void ScriptCall(const char* filename);
#if _VBS3_DEBUG_DELAYCALL
  void FlushDelayCalls();
  void OnDelayCall(IDebugDelayCall * delayCall, bool added);
#endif
  //-------------- End: IDebugScript API --------------

  void ToggleTitleWindow();
#if _VBS3_SD_WND_TITLE
  void WindowTitleChange(const char * title);
#endif

  // -------------- Event's Start -------------- 
  void OnIconized( wxIconizeEvent& event );
  void OnClose(wxCloseEvent& event);
  void OnQuit(wxCommandEvent& event);
#if _SD_UNPINNED_WINDOWS_SHOW_FIX
  void OnShow(wxShowEvent& event);
#endif

  void OnTreeRightClick(wxCommandEvent &event);
  void OnTreeItemRightClick(wxCommandEvent &event);
    
  void OnClosePane(wxAuiManagerEvent &event);
#if REMOVE_PAGE_FIX
  void OnNoteBookCloseTab(wxFlatNotebookEvent &event);
#else
  void OnNoteBookCloseTab(wxAuiNotebookEvent &event);
#endif

#if _VBS3_DEBUG_DELAYCALL
  //setting break on the delayCall before it actually runs
  void OnDelayBreak(wxCommandEvent &event);
#endif
  void OnBreak(wxCommandEvent &event);
  void OnContinue(wxCommandEvent &event);
#if _SD_BREAK_ALL
  void OnBreakAll(wxCommandEvent &event);
  void OnContinueAll(wxCommandEvent &event);
#endif

  void OnContinueAll(bool breakAll = false);

  void UpdateLeftPanelNoteBookAUICaption();
  void OnLeftPanelNoteBookChange(wxFlatNotebookEvent &event);

  void OnStepInto(wxCommandEvent &event);
#if _VBS_1428_STEP_FILE
  void OnStepFile(wxCommandEvent &event);
#endif
  void OnStepOver(wxCommandEvent &event);
  void OnStepOut(wxCommandEvent &event);

  void OnToggleBPOnFocus();

  void OnCancel(wxCommandEvent &event);
  void OnToggleVMCacheMnger(wxCommandEvent &);

  /// Generic menu event.
  void OnCommandEvent(wxCommandEvent &event);

  ///! WatchManager
  void OnGridChangeWatchManager(wxGridEvent &event);

#if _SD_DEBUG_LOG
  void OnDebugWindow(wxCommandEvent& event);
#endif
  void OnPerfWindow(wxCommandEvent& event);

  void DebugEngineLog(const char *str);
  void OnMarginClick( wxScintillaEvent& event );

  //UI events
  void OnNoteBookLClick(wxMouseEvent& event);
#if _VBS_14074_DEV_PATH_UPDATE_FIX
  //Application events (not actual wxEvents) 
 void OnDevDriveChanged();
#endif
  // -------------- Event's End --------------

  /// Update the UI to the current doc...
  void SetCurrentDocTo(DebugScript *script);

  /// Document has come into view by the user
  virtual void DocumentToforeground(DocumentViewer *);
  /// Document is about to close
  virtual void DocumentClosing(DocumentViewer *);
  /// Set's and updates the view with the new call stack item
  void SetCallStackIndex(int index);
 
  void Simulate(float deltaT);
  void UpdateView(bool docChange=false);

  void MyCommandEvent(wxCommandEvent &event);

  virtual void RemoveChild( wxWindowBase *child );

  template <class Function, class UserData>
  void ForEachDebugScript(Function functor,UserData userData)
  {
    for (size_t i=0; i<_scripts.size(); i++)
      if (functor(_scripts[i].GetRef(),userData)) break;
  }

  wxString GetAuiPerspective();
  wxString GetDefaultAuiPerspective() const;

  //Window management
#if _SD_DEBUG_LOG
  //Debug log
  void RegisterDebugWindow(DebugOutPutWindow* debugWindow);
  void RemoveDebugWindow();
  DebugOutPutWindow* GetDebugWindow();
#endif
#if _SD_SCRIPT_LOG
  //Script log
  void RegisterScriptLogWindow(DebugOutPutWindow* logWindow);
  void RemoveScriptLogWindow();
  DebugOutPutWindow* GetScriptLogWindow();

  #if _VBS_15079_RESTRICT_BISIM_SCRIPTS || _SD_DELAYCALL_BETTER_LOG
  //adds an entry to the script log - takes restricted scripts into account
  void LogScript(const wxChar * message, wxString scriptFile);
  #endif
#endif
#if _VBS_14573_DEBUG_CONSOLE
  //Debug console
  void RegisterDebugConsoleWindow(DebugConsoleWindow* console);
  void RemoveDebugConsoleWindow();
  DebugConsoleWindow* GetDebugConsoleWindow();
#endif

  DiagSearch* GetSearchWindow() const;
  void OnSearchWindowEvt(wxCommandEvent& evt);
  void OnSearchWindow(wxScintillaExt* focus);

  /// Register window to receive state changes
  virtual void RegisterWindow(iViewWindow *);
  /// Un-register window
  virtual void UnRegisterWindow(iViewWindow *);
  /// Communicate to the main window, that all window's
  /// state change should be called
  virtual void OnAllStateChangeEvent(iViewWindow*,DebugScript*debugScript=NULL);
private:
  /// Communicate to all the modules that their simulate 
  /// function is called.
  virtual void OnAllSimulate(float deltat);
public:
  /// any class wishing to process wxWidgets events must use this macro
  DECLARE_EVENT_TABLE()
};
extern MyFrame *GFrame;
#if _VBS3_SD_IMP_BREAKPOINTS
extern IToApp *GVBSApp;
#endif

/// Define a new application type, each program should derive a class from wxApp
class MyApp : public wxApp
{
  typedef wxApp base;
public:  

  MyApp();
  virtual ~MyApp();

  int FilterEvent(wxEvent& event);

  virtual bool ProcessIdle();

  virtual bool OnInit();
  virtual int OnExit();
};
