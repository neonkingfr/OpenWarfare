#include "BreakpointManager.hpp"
#include "../../Common/iDebugEngineInterface.hpp"
#include "Util.hpp"

#if _VBS3_SD_IMP_BREAKPOINTS
extern IToApp *GVBSApp;
ScriptBreakpointsManager GBreakpointManager;
MissionDirInfo GCurrentMissionDir;

void MissionDirInfo::Update(float deltaT)
{
  std::string& currDir = GUtil.GetEngineDirectory(MISSION_DIR);
  for (int i=0; i<currDir.length();++i)
  {
    currDir[i] = tolower(currDir[i]); //every path needs to be in lowercase
  }
  if (strcmp(currDir.c_str(), _missionDir.c_str()) != 0)
  {
    bool updMissionBP = false; //to track for changing to __tmppreviewdir
    if (currDir.find("__tmppreviewdir") == -1)
    {
#if _VBS3_DEBUG_DELAYCALL
      if (_inTmpPreview) //if we just got out of the tmpPreview dir
        _tmpPreviewDir = ""; //clear the previewDir var
#endif
      _inTmpPreview = false;
      _changed = true;
      _missionDir = currDir;
    }
    else
    {
      if (!_inTmpPreview) //only update the mission path if we just got into the __tmppreview dir
#if _VBS3_DEBUG_DELAYCALL
      {
        _tmpPreviewDir = currDir; //store the preview dir as well
#endif
        updMissionBP = true;
#if _VBS3_DEBUG_DELAYCALL
      }
#endif
      _inTmpPreview = true;
    }
    if (updMissionBP || _changed) //mission changed, or preview started
      GBreakpointManager.UpdateMissionBreakpoints(_missionDir, currDir);
  }
  else
  {
    _changed = false;
  }
}

bool MissionDirInfo::HasChanged() const
{
  return _changed;
}

const std::string& MissionDirInfo::GetCurrentMissionDir() const
{
  return _missionDir;
}

#if _VBS3_DEBUG_DELAYCALL
const std::string& MissionDirInfo::GetTmpPreviewDir() const
{
  return _tmpPreviewDir;
}
#endif

ScriptBreakpoint::ScriptBreakpoint(const char *filenameEngine, const char *filenameDoc, int handle, int lineNumber, bool enabled)
{
  _filenameEngine = filenameEngine;
  _filenameDoc = filenameDoc;
  _handle = handle;
  _sourceLineNumber = lineNumber;
  _absSouceLineNumber = -1; // needs to be resolved so the engine can break on the file
  _engineBPNum = 0;
#if _SD_LIVEFOLDER_IMP
  _enabled = enabled;
#endif
}

#pragma region ScriptBreakpointsManagerReg

ScriptBreakpoint& ScriptBreakpointsManager::GetBreakpoint(DWORD id)
{
  if (_breakpoints.count(id) > 0)
  {
    return _breakpoints[id];
  }
  return _nullBp;
}

DWORD ScriptBreakpointsManager::GetBreakpointId(const char *filenameEngine, int lineNumber) const
{
  for (std::map<DWORD, ScriptBreakpoint>::const_iterator it = _breakpoints.begin(); it != _breakpoints.end(); ++it)
  {
    if ( _strcmpi(it->second._filenameEngine.c_str(), filenameEngine) == 0 && it->second._sourceLineNumber == lineNumber)
    {
      return (it->first); //return the DWORD ID
    }
  }
  return -1;
}

DWORD ScriptBreakpointsManager::AddBreakpoint(const char *filenameEngine, const char *filenameDoc, int markerHandle, int lineNumber, int absLineNumber, bool enabled)
{
  //first see if the bp exists already
  DWORD minUnusedId = GetBreakpointId(filenameEngine, lineNumber);
  if (minUnusedId != -1)
    return minUnusedId; //if it does, return the handle
  else
    minUnusedId = 0; //otherwise start counting from 0 up

  ScriptBreakpoint bp(filenameEngine, filenameDoc, markerHandle, lineNumber, enabled);
  while (_breakpoints.count(minUnusedId) == 1)
  {
    ++minUnusedId;
  }

  //resolve absolute line number
  bp._absSouceLineNumber = absLineNumber;

  bp._engineBPNum = minUnusedId;
  _breakpoints[minUnusedId] = bp;

  //Add the breakpoint in VBS
  if (GVBSApp)
#if _VBS3_SCRIPT_LINES
    GVBSApp->AddBreakpoint(filenameEngine, lineNumber, minUnusedId, enabled);
#else
    GVBSApp->AddBreakpoint(filenameEngine, absLineNumber, minUnusedId, enabled);
#endif
  return minUnusedId;
}

bool ScriptBreakpointsManager::RemoveBreakpoint(DWORD bp)
{
  if (_breakpoints.count(bp) == 0)
    return false;
  //remove the breakpoint in VBS
  if (GVBSApp)
    GVBSApp->RemoveBreakpoint(bp);
  _breakpoints.erase(bp);
    return true;
}

bool ScriptBreakpointsManager::EnableBreakpoint(DWORD bp, bool enabled)
{
  if (_breakpoints.count(bp) == 0)
    return false;
  //Enable the breakpoint in VBS
  if (GVBSApp)
    GVBSApp->EnableBreakpoint(bp,enabled);
  _breakpoints[bp]._enabled = enabled;
  return true;
}

std::string ScriptBreakpointsManager::GetEngineBPPath(const wxString &fileRelPath, const wxString &fileAbsPath) const
{
  std::string enginePath("");
  if (fileRelPath.Length()) //for addon scripts, the engine works with relative paths
  {
    enginePath = UTF16ToChar(fileRelPath.Lower());
  }
  else
  {
    //for everything else, abs paths must be used. There is a problem with mission scripts though, which get copied over to a __tmpPreviewDir
    //trouble is, that we don't know the mission path until the mission is loaded, so we need to check if the folder is the current mission folder and if it isn't use the abs path
    //otherwise, we'll reinterpret the path to use the relative one

    const std::string& missionFolder = GCurrentMissionDir.GetCurrentMissionDir();
    if (missionFolder.length())
    {
      std::string path = UTF16ToChar(fileAbsPath);
      int findRes = path.find(missionFolder.c_str());
      if (findRes != -1)
      {      
        enginePath = GUtil.GetEngineDirectory(MISSION_DIR) + path.substr(missionFolder.length(), path.length());
      }
    }
  }
  if ( enginePath.length() == 0)
  {
    enginePath = UTF16ToChar(fileAbsPath);
  }

  //convert to lowercase
  for (size_t i = 0; i < enginePath.length(); ++i)
  {
    enginePath[i] = tolower(enginePath[i]);
  }
  return enginePath;
}

void ScriptBreakpointsManager::UpdateMissionBreakpoints(const std::string& storedMissionDir, const std::string& currMissionDir)
{
  for (std::map<DWORD,ScriptBreakpoint>::iterator it = _breakpoints.begin(); it != _breakpoints.end(); ++it)
  {
    ScriptBreakpoint& bp = it->second;
    int findRes = -1;
    if (storedMissionDir.length())
      findRes = bp._filenameDoc.find(storedMissionDir.c_str());

    if (strcmp(bp._filenameDoc.c_str(), bp._filenameEngine.c_str()) != 0) //means this was a mission breakpoint -> the _filenameEngine is modified to match engine path
    {
      //see if it still is      
      if (findRes == -1) //no longer in the current mission folder
      {
        bp._filenameEngine = bp._filenameDoc;
        //update the BP in the engine (remove and add it again) -> since filename plays part in the hashcode, it's safer to do it this way
        GVBSApp->RemoveBreakpoint(bp._engineBPNum);
#if _VBS3_SCRIPT_LINES
        GVBSApp->AddBreakpoint(bp._filenameEngine.c_str(), bp._sourceLineNumber, bp._engineBPNum, bp._enabled);
#else
        GVBSApp->AddBreakpoint(bp._filenameEngine.c_str(), bp._absSouceLineNumber, bp._engineBPNum, bp._enabled);
#endif
      }
    }
    else //not a mission breakpoint
    {
      if (findRes == 0) //but it should be
      {
        //get the engine mission path (can be __tmpMissionDir...)
        bp._filenameEngine = currMissionDir + bp._filenameDoc.substr(storedMissionDir.length(), bp._filenameDoc.length());
        GVBSApp->RemoveBreakpoint(bp._engineBPNum);
#if _VBS3_SCRIPT_LINES
        GVBSApp->AddBreakpoint(bp._filenameEngine.c_str(), bp._sourceLineNumber, bp._engineBPNum, bp._enabled);
#else
        GVBSApp->AddBreakpoint(bp._filenameEngine.c_str(), bp._absSouceLineNumber, bp._engineBPNum, bp._enabled);
#endif
      }
    }
  }
}

#pragma endregion
#endif