#pragma once

#include <vector>

#include <wx/event.h>
#include <wx/propgrid/propgrid.h>

#include "../../Common/Essential/RefCount.hpp"

typedef wxString EventName;
struct EventGroup : public std::vector<EventName>
{
  wxString _name;
};
struct EventHandlerList : public std::vector<EventGroup>
{
  EventHandlerList();
};
extern EventHandlerList GEventHandlerList;

struct BindCat
{
  wxPGId _cat;
  std::vector<wxPGId> _enumList;

  void SelectAll(wxPropertyGrid* grid);
  bool IsSelected(wxPropertyGrid* grid, unsigned int index);
};


class wxPropertyGrid;
class PropertyGridEventHandler : public wxEvtHandler, public RefCount
{
  /// Property grid that is used to list all the Event handlers
  wxPropertyGrid *_propertyGrid;
  
  /// Bind the eventList
  std::vector<BindCat> _eventGrid;

  /// Last right click position.
  wxPoint _lastRightClickPos;
public:
  PropertyGridEventHandler(wxPropertyGrid *propertyGrid);
  ~PropertyGridEventHandler();

  /// Update the event manager with events
  void PopulateEventHandler();

  void OnRightMouseUp(wxMouseEvent& event);
  void OnContextMenu(wxCommandEvent& menu);

  const TCHAR *GetEventName(unsigned int eventGroup, unsigned int eventId);
  bool IsEventSelected(unsigned int eventGroup, unsigned int eventId);
};