#include "iDebugEngineInterface.hpp"

// DebugScriptInterface, is the implementation that will be called from
// the engine when connecting to the plugin with connect.
class DebugScriptInterface : public IToDll
{
public:
  DebugScriptInterface() {}
  ~DebugScriptInterface() {}

  virtual void Startup(){}
  virtual void Shutdown(){}

  virtual void ScriptCall(const char * filename) {};
#if _VBS3_DEBUG_DELAYCALL
  virtual void OnDelayCall(IDebugDelayCall * delayCall, bool added) {};
#endif

  virtual void ScriptLoaded(IDebugScript *script, const char *name){}
  virtual void ScriptEntered(IDebugScript *script){};
  virtual void ScriptTerminated(IDebugScript *script){};

  virtual void ScriptSimulationEntered(IDebugScript *script){};
  virtual void ScriptSimulationExited(IDebugScript *script){};

  virtual void FireBreakpoint(IDebugScript *script, unsigned int bp) {} 
#if _VBS3_SCRIPT_LINES
  virtual void Breaked(IDebugScript *script, const char * filename, int line) {};
#else
  virtual void Breaked(IDebugScript *script){}
#endif
  virtual void DebugEngineLog(const char *str){}
};


// Simple debug interface setup to test calling
DebugScriptInterface GScriptInterface;