#include "DocumentManager.hpp"
#include "DocumentTabManager.hpp"
#include "DocumentTreeListManager.hpp"

#include "DebugScript.hpp"

#include "../../Common/iDebugEngineInterface.hpp"
#include "../../Common/Essential/PreprocC/preprocC.hpp"
#include "MainWindow.hpp"

#include "Util.hpp"

#include <algorithm>

wxString GetFileName(const TCHAR *filePath)
{
  // Read till the last \\, then read from that char + 1
  // till the null terminator into the path buffer
  const TCHAR *endChar = wcsrchr(filePath,'\\');
  if (!endChar) endChar = filePath;
  
  TCHAR fileNameBuffer[MAX_PATH] = {0};
  wcsncpy_s(fileNameBuffer,!endChar? filePath : endChar+1,MAX_PATH-1);

  return wxString(fileNameBuffer);
}

// All close request come through here
void DocumentManager::CloseAllDocuments()
{
  for (size_t i=0; i<_documents.size(); i++)
  {
    DocumentViewer *doc = _documents[i].GetRef();
    if (doc->GetDocContents()&DOC_VM) continue;
    
    CloseDocument(doc);
    i--;
  }
}


void DocumentManager::DocumentTabSelected(DocumentViewer *doc)
{
  // Tell the main window, that a window some how has come into the foreground
  GMainWindow->DocumentToforeground(doc);
}


void DocumentManager::FocusDocument(DocumentViewer *doc)
{
  // Update the menu with the current selected encoding for the document....
  GDocumentTabManager->BringIntoFocus(doc);
}

DocumentViewer *DocumentManager::GetCurrentDocument()
{
  return GetDocumentViewer(GDocumentTabManager->SelectedTab());
}

void DocumentManager::ToggleBPOnFocusDocument()
{
  DocumentViewer *view = GetCurrentDocument();
  if (view) view->ToggleCurrentLineBP();
}


void DocumentManager::TabCloseingDocument(DocumentViewer *doc)
{
  // Remove the document from the tree
  GDocumentTreeManager->RemoveDocument(doc);

  // Send the message to the main window that the document is closing
  GMainWindow->DocumentClosing(doc);

  // Unhook the document view, about to be deleted
  doc->UnHook();
  for (size_t i=0; i<_documents.size(); i++)
  {
    if (doc==_documents[i])
    {
      _documents.erase(_documents.begin()+i);
      break;
    }
  }
}

void DocumentManager::CloseDocumentCheckLock(DocumentViewer *doc)
{
  // Cannot remove...
  if (doc->GetDocContents()&DOC_VM)
  {
    wxMessageBox(wxString::Format(L"Cannot close virtual machine tab",doc->GetName()));
    return;
  }

  CloseDocument(doc);
}


/*
 Close document is normally a tree close all documents, or some API
 calling directly just to remove it.
*/
void DocumentManager::CloseDocument(DocumentViewer *doc)
{
  // Unhook the document view, about to be deleted
  doc->UnHook();

  // Remove the document from the tree
  GDocumentTreeManager->RemoveDocument(doc);

  // Removing the document from the tab manager will delete the scintilla document
  GDocumentTabManager->RemoveTab(doc);

  // Send the message to the main window that the document is closing
  GMainWindow->DocumentClosing(doc);
  
  // Delete the document view now, safety
  for (size_t i=0; i<_documents.size(); i++)
  {
    if (doc==_documents[i])
    {
      _documents.erase(_documents.begin()+i);
      break;
    }
  }
}

bool IsExecuteCommandRP();
int ExecuteCommandRP(const char *command, char *result, int resultLength);

// 5 meg should be sufficient
#define FILE_TEMP_SIZE 5*1024*1024

// File buffer.
std::vector<char> LargeFileBuffer;

DocumentManager::DocumentManager()
{
  LargeFileBuffer.resize(FILE_TEMP_SIZE);

  // Register with the main window so this receives updates so it can process the events correctly
  GMainWindow->RegisterWindow(this);
}

 void DocumentManager::StateChange(DebugScript *script)
 {
  //DocumentViewer *view = GetDocument(script);
  //view->StateChange(script);
 }

void DocumentManager::SimulateUpdate(float deltaT)
{
}

DocumentViewer* DocumentManager::OpenEngineFile(wxString relOrAbsPath,DocumentViewer *view)
{
  if (!view)
  {
    wxString fileName = GetFileName(relOrAbsPath);
    view = CreateDocument(fileName,relOrAbsPath);
  }

  if (IsExecuteCommandRP())
  {
    {
      std::string cmd = ::Format("loadFile \"%s\"",UTF16ToChar(relOrAbsPath).data());
      int success = ExecuteCommandRP(cmd.data(),&LargeFileBuffer[0],LargeFileBuffer.size());
      Assert(success==1);
      
      // Check if there is any data that was written?
      size_t stringLength = strnlen(&LargeFileBuffer[0],LargeFileBuffer.size());
      if (stringLength>2)
      {
        // The engine appends " to the start and end
        char *startChar = &LargeFileBuffer[1];
        LargeFileBuffer[stringLength-1] = 0;

        view->SetDocViewContent(DOC_ENGINE,std::string(startChar));
      }
    }

    {
      std::string cmd = ::Format("preprocessFile \"%s\"",UTF16ToChar(relOrAbsPath).data());
      int success = ExecuteCommandRP(cmd.data(),&LargeFileBuffer[0],LargeFileBuffer.size());
      Assert(success==1);
      
      // Check if there is any data that was written?
      size_t stringLength = strnlen(&LargeFileBuffer[0],LargeFileBuffer.size());
      if (stringLength>2)
      {
        // The engine appends " to the start and end
        char *startChar = &LargeFileBuffer[1];
        LargeFileBuffer[stringLength-1] = 0;

        view->SetDocViewContent(DOC_ENGINE_PP,std::string(startChar));
      }
    }
  }

  return view;
}

#if _VBS_15079_RESTRICT_BISIM_SCRIPTS
DocumentViewer* DocumentManager::OpenFile(wxString filePath, DocumentViewer* update, bool isInternal)
#else
DocumentViewer* DocumentManager::OpenFile(wxString filePath, DocumentViewer* update)
#endif
{
  bool ucs2 = false;
  std::istream *ifStream = FilePreprocessor::OpenFileNoPreProccessing(filePath.data(),ucs2);
  if (ifStream)
  {
    ifStream->seekg(0,std::ios_base::end);
    size_t length = ifStream->tellg();
    if (length==-1) length = 0;
    ifStream->seekg(0,std::ios_base::beg);

    // Read the complete file in, and call
    // For this, we need to insert the null terminating character
    std::vector<char> buffer;
    if (length)
    {   
      buffer.resize(length);
      ifStream->read(&buffer[0],length);
    }
    else
    {
      // Nothing..
      buffer.push_back(0);
    }
    
    wxString fileName = GetFileName(filePath);

    DocumentViewer *view;
    if (update)
      view = update;
    else
#if _VBS_15079_RESTRICT_BISIM_SCRIPTS
      view = CreateDocument(fileName,filePath,isInternal);
#else
      view = CreateDocument(fileName,filePath);
#endif

    //get the last modified time
    FILETIME writeTime;
    view->GetLastDiskWriteTime(writeTime);

    view->SetDocViewContent(DOC_ORIGINAL,&buffer[0],buffer.size(),ucs2,writeTime);
#if _VBS_14491_REMOVE_BREAKPOINTS_AFTER_RELOAD
    if (update)
  #if _VBS3_SD_IMP_BREAKPOINTS
      view->RemoveAllBreakpoints(); //remove all breakpoints
  #else
      view->RemoveBreakPoint(-1); //remove all breakpoints
  #endif
#endif
    view->SetDocView(DOC_ORIGINAL);   
       
    FilePreprocessor::DeleteStream(ifStream);
    ifStream  = NULL;

    // Either absolute or relative path, if relative is within
    // the vbs2 packaged content
    wxString relOrAbsPath = view->GetFilePath();
    if (view->GetRelativePath().size()) relOrAbsPath = view->GetRelativePath();
    
    // Open the engine file, and append it to the open file
    OpenEngineFile(relOrAbsPath,view);
    return view;
  }

  // Add the documentation code here
  return NULL;
}

void DocumentManager::CheckForFileChanges()
{
  //simply iterate through all documents and compare the last write time with the stored one, reload if they don't match
  for( std::vector<SmartRef<DocumentViewer>>::iterator it = _documents.begin(); it != _documents.end(); ++it )
  {
    SmartRef<DocumentViewer> doc = *it;
#if _VBS_15079_RESTRICT_BISIM_SCRIPTS
    if (doc->FileChanged() && !doc->ContentsProtected())
#else
    if (doc->FileChanged())
#endif
    {
      doc->SetReloading(true);
      //prompt the user
      wxMessageDialog prompt(GFrame,wxString::Format(wxT("%s has been modified, do you wish to reload it?"),doc->GetName()),wxT("Reload"),wxYES_NO);
      if (prompt.ShowModal() == wxID_YES)
      {
#if _VBS_15079_RESTRICT_BISIM_SCRIPTS
        OpenFile(doc->GetFilePath(),doc, doc->IsInternal()); //reload if yes
#else
        OpenFile(doc->GetFilePath(),doc); //reload if yes
#endif
      }
      doc->SetReloading(false);
    }
  }
}

#if _VBS_14074_DEV_PATH_UPDATE_FIX
void DocumentManager::UpdateRelativePaths()
{
  for( std::vector<SmartRef<DocumentViewer>>::iterator it = _documents.begin(); it != _documents.end(); ++it)
  {
    SmartRef<DocumentViewer> doc = *it;
    doc->UpdateRelativePath();
    // Either absolute or relative path, if relative is within
    // the vbs2 packaged content
    wxString relOrAbsPath = doc->GetFilePath();
    if (doc->GetRelativePath().size()) relOrAbsPath = doc->GetRelativePath();
    
    // Open the engine file, and append it to the open file
    if (relOrAbsPath.size())
      OpenEngineFile(relOrAbsPath,doc);
  }
}
#endif

#if _VBS_15079_RESTRICT_BISIM_SCRIPTS
void DocumentManager::UpdateProtectedFiles()
{
  for (size_t i=0; i < _documents.size(); ++i)
  {
    bool prevRestricted = _documents[i]->GetStoredIsRestricted();
    if (prevRestricted != _documents[i]->IsRestricted(true)) //refresh the file if protection changed (internal can't change)
    {
      _documents[i]->SetDocView(_documents[i]->GetDocView());
    }
  }
}
#endif

DocumentViewer *DocumentManager::GetDocumentViewer(wxWindow *scintilla)
{
  for (size_t i=0; i<_documents.size(); i++)
    if (_documents[i]->GetDocument()==scintilla) return _documents[i];

  return NULL;
}

#if _VBS_15079_RESTRICT_BISIM_SCRIPTS
DocumentViewer *DocumentManager::CreateDocument(const wxString &name, const wxString &filePath, bool isInternal)
#else
DocumentViewer *DocumentManager::CreateDocument(const wxString &name, const wxString &filePath)
#endif
{
    DocumentViewer *view = new DocumentViewer(name,filePath,GDocumentTabManager->GetNoteBook());
#if _VBS_15079_RESTRICT_BISIM_SCRIPTS
    view->SetInternal(isInternal);
#endif
    _documents.push_back(view);
    
    GDocumentTabManager->AddTab(view);
    GDocumentTreeManager->AddDocumentToList(view);
    
    return view;
}

DocumentViewer *DocumentManager::GetVirtualMachineDoc()
{
  for (size_t i=0; i<_documents.size(); i++)
    if (_documents[i]->GetDocContents()&DOC_VM) return _documents[i];

  return NULL;
}


#if _VBS3_DEBUG_DELAYCALL
DocumentViewer * DocumentManager::GetDelayCallDoc()
{
  for (size_t i=0; i<_documents.size(); ++i)
    if (_documents[i]->GetDocContents()&DOC_DELAYED) return _documents[i];

  return CreateDocument(L"Delay call");
}
#endif

// Empty string is returned if nothing is found
wxString GetFileName(wxString &path)
{
  size_t lastPos = path.find_last_of('\\');
  if (lastPos!=-1) return path.substr(lastPos+1);
  return wxString();
}

/*
  This needs to work in the situation that the path that is being reported
  is from the tmp mission directory.

  In our case, it is in format __tmpPREVIEWsave.Intro where the documents 
  are saved, before previewing. 
 
  C:\Users\Chad\Documents\VBS2NG\mpmissions\__tmpPreviewDir\__tmpPREVIEWsave.Intro\
    
  So, I need to grab the file name, comapre to see if any other files contain the name. Then I need
  to open the engine file, and then compare it against each other documents open file... great...
*/
DocumentViewer *DocumentManager::GetDocument(wxString path)
{
  for (size_t i=0; i<_documents.size(); i++)
  {
    wxString relPath = _documents[i]->GetRelativePath();
    if (relPath.size())
      if (relPath.CompareTo(path,wxString::ignoreCase)==0) return _documents[i];

    wxString debugPath = _documents[i]->GetFilePath();
    if (debugPath.CompareTo(path,wxString::ignoreCase)==0) return _documents[i];
  }

  // If the path contains __tmpPreviewDir, then assume the user is previewing a mission
  // and do a quick hack to return his/her original open mission files that should be open based
  // off the file name.
#if _VBS3_SD_IMP_BREAKPOINTS
  if (path.Contains(wxString(L"__tmppreviewdir")))
#else
  if (path.Contains(wxString(L"__tmpPreviewDir")))
#endif
  {
    wxString fileName = GetFileName(path);
    for (size_t i=0; i<_documents.size(); i++)
    {
      wxString docFileName = GetFileName(_documents[i]->GetFilePath());
      if (!_wcsicmp(fileName.data(),docFileName.data())) return _documents[i];
    }
  }

  return GetVirtualMachineDoc();
}

DocumentViewer *DocumentManager::GetDocument(DebugScript *debugScript)
{ 
  return NULL;
}

static wxString GetFullPath(wxString name)
{
  TCHAR buffer[_MAX_PATH];
  if (GetFullPathName(name, _MAX_PATH, buffer, NULL) == 0) return name;
  return wxString(buffer);
}

#if !_VBS3_SD_IMP_BREAKPOINTS
void DocumentManager::SetAllBreakPoints(IDebugScript *script)
{
  for (size_t i=0; i<_documents.size(); i++)
  {
    if (_documents[i]->GetBreakPoints().size())
    {
      DocumentViewer *doc = _documents[i].GetRef();

      wxString path = doc->GetFilePath();
      if (doc->GetRelativePath().size())
      {
        // Convert the relative path to a absolute path
        path = GetFullPath(doc->GetRelativePath());
      }
      else
      {
        std::string engineMissionDir = GUtil.GetEngineDirectory(MISSION_DIR);
        wxString missionDir = CharToUTF16(engineMissionDir);    

        path = missionDir + wxString(L"\\") + doc->GetName();
      }

      std::string breakPointPath = UTF16ToChar(path);
      for (size_t x = 0; x<doc->GetBreakPoints().size(); x++)
      {
        BreakPointLine &bp = doc->GetBreakPoints()[x];

        // Set the break point to fire.
        if (bp._absSouceLineNumber!=-1)
        {

          script->SetBreakpoint(breakPointPath.data(),bp._absSouceLineNumber,bp._engineBPNum,true);
        }
      }
    }
  }
}
#endif



SmartRef< DocumentManager > GDocumentManager;
