#pragma once

#include <stdio.h>

#include <Windows.h>
#include <iostream>

#include <string>
#include <vector>
#include <fstream>
#include <list>

#include "ScriptDebuggerConfig.hpp"
#include "../../Common/Essential/LexicalAnalyser.hpp"
#include "../../Common/Essential/SyntaxAnyaliser.hpp"
#include "../../Common/Essential/Config.hpp"

#include "wx/string.h"
#include <wx/gdicmn.h>

// Ripped from Widgets\contrib\src\scintilla\include\SciLexer.h
#define SCE_C_DEFAULT 0
#define SCE_C_COMMENT 1
#define SCE_C_COMMENTLINE 2
#define SCE_C_COMMENTDOC 3
#define SCE_C_NUMBER 4
#define SCE_C_WORD 5
#define SCE_C_STRING 6
#define SCE_C_CHARACTER 7
#define SCE_C_UUID 8
#define SCE_C_PREPROCESSOR 9
#define SCE_C_OPERATOR 10
#define SCE_C_IDENTIFIER 11
#define SCE_C_STRINGEOL 12
#define SCE_C_VERBATIM 13
#define SCE_C_REGEX 14
#define SCE_C_COMMENTLINEDOC 15
#define SCE_C_WORD2 16
#define SCE_C_COMMENTDOCKEYWORD 17
#define SCE_C_COMMENTDOCKEYWORDERROR 18
#define SCE_C_GLOBALCLASS 19

// Forward dec for wxScintilla, See DialogStyle for implementation
class wxScintilla;

// Have three different states...
// 1) When loading, load from the config
// 2) When Scintilla loads, it find and sets the style based off the GStyleRemapper
// 3) When changing, it changes Scintilla directly for all documents!
// 4) The DialogStyle, does all the version checking
class StyleLoader : public RefCount
{
public:
  std::string _className; // Class name saved as ASCII for now

  std::string _fontName; // Font name, saved as string for now, ASCII
  std::string _bkcolor; // font color, saved as string for now, ASCII
  std::string _fgcolor; // font color saved as string for now, ASCII

  unsigned int _fontSize; // not set is 0

  bool _bold;
  bool _italic;
  bool _underline;

  int _styleID;

  StyleLoader(int styleID):_bold(false),_italic(false),_underline(false),_fontSize(0),_styleID(styleID){}

StyleLoader *PullScintillaStyleState(wxScintilla *doc);
StyleLoader *PushScintillaStyleState(wxScintilla *doc);

  void SaveToConfig(ParamaFileConfig *config);
  void LoadParam(ConfigEntry *entry);

#if _SD_DEFAULT_SCI_STYLE
  //Set the default font 
  void SetDefaults(int ST);
#endif
};

struct ReMapper
{
  const char *ConfigName;
  int StyleID;
};

// Simple re-mapper, for config name to the actual scintilla styleID
const ReMapper  GStyleRemapper[] = 
{
    {"Default",SCE_C_DEFAULT}
   ,{"Identifier",SCE_C_IDENTIFIER}
  ,{"Comment",SCE_C_COMMENT}
  ,{"CommentLine",SCE_C_COMMENTLINE}
  ,{"CommentDoc",SCE_C_COMMENTDOC}
  ,{"String Literal",SCE_C_STRING}
  ,{"Numbers",SCE_C_NUMBER}
  ,{"PreProcessor",SCE_C_PREPROCESSOR}
  ,{"Operator",SCE_C_OPERATOR}
  ,{"VBSOperators",SCE_C_WORD}
  ,{"VBSFunctions",SCE_C_WORD2}
  ,{"VBSNulers",SCE_C_GLOBALCLASS}
  //,{"Character",SCE_C_CHARACTER}
  //,{"UUID",SCE_C_UUID} 
  //,{"Identifier",SCE_C_IDENTIFIER}
  //,{"StringEOL",SCE_C_STRINGEOL}
  //,{"Verbatim",SCE_C_VERBATIM}
  //,{"RegEx",SCE_C_REGEX}
  //,{"CommentLineDoc",SCE_C_COMMENTLINEDOC}
  //,{"CommentDockeyWord",SCE_C_COMMENTDOCKEYWORD}
  //,{"CommentDockeyError",SCE_C_COMMENTDOCKEYWORDERROR}
};

class WindowSettings
{
protected:
  //This method should always call the same method of the parent class (only the parent class 1 level up)
  void SaveToClassEntry(ConfigClass * classEntry);
public:
  WindowSettings();

  //Data members
  bool docked;
  bool maximized;
  bool stayOnTop;
  wxPoint position;
  wxSize size;

  //needs to call parent_class::SetDefaults in inherited classes
  void SetDefaults();

  //Needs to call parent_class::Load in inherited classes
  void Load(ConfigEntry* entry);

  //Should never call the parent class method - this actually creates the class record in the configFile
  void Save(const char * name, ParamaFileConfig* paramFile);
};

//Stores all settings relevatnt to the main window UI
class MainWindowSettings : public WindowSettings
{
private:
  wxString _auiPersp;
  wxString _defaultAuiPersp;
protected:
  void SaveToClassEntry(ConfigClass * classEntry);
public:
  MainWindowSettings() {};

  //sets the perspective that will be saved to config/loaded on startup
  void SetAuiPerspective(const wxString& perspective);
  //sets the perspective we'll default to when no other perspective is saved
  void SetDefaultAuiPerspective(const wxString& perspective);
  //get the currently saved perspective
  wxString GetAuiPerspective() const;
  
  //needs to call parent_class::SetDefaults in inherited classes
  void SetDefaults();
  //Needs to call parent_class::Load in inherited classes
  void Load(ConfigEntry* entry);
  //Should never call the parent class method - this actually creates the class record in the configFile
  void Save(const char * name, ParamaFileConfig* paramFile);
};

struct DebugOutputFilter
{
  //for filling in the comboBoxes
  wxString stringInclude;
  wxString stringExclude;
  wxString stringHighlight;

  std::list<wxString> includeHistory;
  std::list<wxString> excludeHistory;
  std::list<wxString> highlightHistory; 
};

class OutputWindowSettings : public WindowSettings
{
private:
  DebugOutputFilter _filters;
  bool _autoscroll;
#if _SD_LIMIT_OUTPUT_LINES
  int _maxOutputLines;
#endif
protected:
  void LoadFilters(ConfigClass* classEntry);
  void UpdateFilterHistory(const wxString & newFilter, std::list<wxString> & filterHistory);
  void SetDefaultFilters();
  //overrides from WindowSettings 
  void SaveToClassEntry(ConfigClass * classEntry);
public:
  OutputWindowSettings();

  void SetDefaults();

  void SetFilters(wxString &include, wxString &exclude, wxString &highlight);
  
  wxString GetIncludeFilter() const;
  wxString GetExcludeFilter() const;
  wxString GetHighlighting() const;
  const std::list<wxString> & GetIncludeHistory() const;
  const std::list<wxString> & GetExcludeHistory() const;
  const std::list<wxString> & GetHighlightHistory() const;

  bool GetAutoscroll() const;
  void SetAutoscroll(bool enable);
#if _SD_LIMIT_OUTPUT_LINES
  int GetMaxOutputLines() const;
  void SetMaxOutputLines(int n);
#endif

 //overrides from WindowSettings
  void Save(const char * name, ParamaFileConfig* paramFile);
  void Load(ConfigEntry* entry);
};

#define MAX_CONFIG_STYLES (sizeof(GStyleRemapper)/sizeof(GStyleRemapper[i]))
#define MAX_STYLES 32

#define SETTINGS_FILTER_INC_DEFAULT wxT("*")
#define SETTINGS_FILTER_EXC_DEFAULT wxT("")
#define SETTINGS_FILTER_HIG_DEFAULT wxT("")

enum DebuggerWindows
{
  WND_SCRIPT_LOG,
  WND_DEBUG_OUTPUT
};

class Settings
{
private:
  bool _enableScriptLog;

  //window management//
#if _SD_DEBUG_LOG
  OutputWindowSettings _debugOutputWindowSettings;
#endif
#if _SD_SCRIPT_LOG
  OutputWindowSettings _scriptLogWindowSettings;
#endif
#if _VBS_14573_DEBUG_CONSOLE
  WindowSettings _debugConsoleWindowSettings;
#endif
  MainWindowSettings _mainWindowSettings;

  //helper load settings functions
  void LoadWindowSettings(ParamaFileConfig* paramFile);

public:
  Settings();

  void SetDefaults();
  void SetDefaultWindowSettings();

#if _SD_DEBUG_LOG || _SD_SCRIPT_LOG
  OutputWindowSettings & GetOutputWindowSettings(DebuggerWindows wndType);
  void SetOutputWindowSettings(OutputWindowSettings settings, DebuggerWindows wndType);
#endif
#if _VBS_14573_DEBUG_CONSOLE
  WindowSettings & GetDebugConsoleWindowSettings();
#endif

  MainWindowSettings & GetMainWindowSettings();

  /// Developer path
  wxString _developerPath;
#if _SD_LIVEFOLDER_PATHS
  wxString _liveFolderPath;
#endif
  /// Preprocessor definitions
  std::vector<wxString> _ppDefinitions;

  /// For now, limit the style loader to a 1-1 relationship
  SmartRef<StyleLoader> _styles[MAX_STYLES];

  bool _enableDebugging;

  /// Get the style per the id..
  /// NULL return if it doesn't exist
  StyleLoader *GetStyle(int id) const;
  /// Creates, the style for the specific ID in the setting.
  StyleLoader *CreateStyle(int id);
  /// Overwrite the old style with a new one
  void SetStyleLoader(StyleLoader *style);
  
  bool &EnableDebugging();

  void SetEnableScriptLog(bool enable);
  bool EnableScriptLog();

  void LoadConfigFile();
  void SaveConfigFile();

  void LoadParam(ParamaFileConfig *paramFile);
  void SaveToConfig(ofstream &outputstream);
};

extern Settings GSettings;