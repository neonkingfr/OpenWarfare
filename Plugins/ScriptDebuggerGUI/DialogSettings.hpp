#pragma once

#include "ui/FormBuilderGUI.h"
#include <vector>

class DialogSettingsEx : public DialogSettings
{
  typedef DialogSettings base;

  std::vector<DiagAddDefintionTemplate*> _ppDefintions;
public:
  DialogSettingsEx( wxWindow* parent );

  void CloseSetting(wxCloseEvent&);


  wxWindow *AddNewPPDefintionEntry();
  void RemovePPDefintionEntry(wxWindow *window);

  void OnButtonAddClicked(wxCommandEvent& event);
  void OnButtonRemoveClicked(wxCommandEvent& event);
  void OnButtonOkClicked(wxCommandEvent& event);
  void OnEnterPressed(wxCommandEvent &event);

  DECLARE_EVENT_TABLE()
};

