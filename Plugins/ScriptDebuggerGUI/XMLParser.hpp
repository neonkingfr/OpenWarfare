#include <wx/string.h>
#include <wx/wxchar.h>


//essentialy a SAX parser that just removes all tags and translates all symbols
class WxStringTagCleaner
{
private:
  bool _insideTag;
  bool _insideSymbol;

  wxString _currTag;
  wxString _currSymbol;
  wxString _currText;
  wxString _result;
protected:
  //fired at the beginning, before first char is read
  void onDocumentStart();
  //fired after the last char is read
  void onDocumentEnd();
  void onElementStart();
  void onElementEnd();
  void onTagStart();
  void onTagEnd();
  void onSymbolStart();
  void onSymbolEnd();
  void onTextChar(wxChar c);
public:
  WxStringTagCleaner();
  wxString Parse(wxString &input);
};