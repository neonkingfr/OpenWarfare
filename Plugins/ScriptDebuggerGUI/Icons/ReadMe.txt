The icons get included into the script debugger as part of the debuggerIcons.hpp header, this allows us to have the debugger as just the single dll file.

To add a new icon, just copy the png into this folder and run the convertIcons batch script. The debuggerIcons.hpp headers will be regenerated automatically.

For more information about how images are included into wxWidgets, see http://wiki.wxwidgets.org/Embedding_PNG_Images .

note:
There are also some icons included in the /UI/Images (png sources) and /UI/EmbeddedFiles folders. These were generated by wxFormBuilder when Chad was developing the debugger.
Since we're no longer using the Form builder, we had to choose a new way of adding new icons into the application. The old icons should also be converted when there's time.
