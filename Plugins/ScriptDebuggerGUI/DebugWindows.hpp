/***
Contains definitions of all 'minor' script debugger windows such as the debug log, script log, debug console.
I moved them out of the MainWindow.hpp file as it was just growing too much with the number of support windows that that were added to the debugger.
*/

#pragma once

#include "wx/wx.h"

#include "AhoCorasick.hpp"
#include "DebugWindows.hpp"
#include "ui/FormBuilderGUI.h"
#include "ScriptDebuggerConfig.hpp"
#include "ImgManager.hpp"
#include "Settings.hpp"
#include "Util.hpp"

#include "resource.h"

#include "MainWindow.hpp"

#include "../../Common/Essential/RefCount.hpp"
#include "../../Common/Essential/AppFrameWork.hpp"


#define DEBUG_OUTPUT_WINDOW_MENU_PIN 2101
#define DEBUG_OUTPUT_WINDOW_MENU_FILTER 2102
#define DEBUG_OUTPUT_WINDOW_MENU_ONTOP 2103
#define DEBUG_OUTPUT_WINDOW_MENU_AUTOSCROLL 2104
#define DEBUG_OUTPUT_WINDOW_MENU_SEARCH 2105

struct FilterFSMs
{
 //the actual filtering FSMs
  AhoCorasick<char, std::string> include; //debug log messages come in std::string format
  AhoCorasick<char, std::string> exclude;
  AhoCorasick<wxChar, wxString> include_wx; //the script log works with wxStrings
  AhoCorasick<wxChar, wxString> exclude_wx; 
  AhoCorasick<wxChar, wxString> highlight; //only ever called on lines that will be added to wx ctrls -> wxString only
};

struct SearchData
{
  long int lastSearchPos;
  unsigned int lastFocus;
  std::vector<searchResult> foundPositions;
  std::vector<wxString> prevSearchStrings;
  wxString searchString;
#if !_VBS_14551_OUTPUT_HIGHLIGHT_OPT
  AhoCorasick<wxChar,wxString> FSM;
#endif

  void Init()
  {
    lastSearchPos = 0;
    lastFocus = 0;
  }

  void InitWith(const wxString& str)
  {
    searchString = str;
    lastSearchPos = 0;
    lastFocus = 0;
#if !_VBS_14551_OUTPUT_HIGHLIGHT_OPT
    FSM.ClearNeedles();
    FSM.AddNeedle(searchString);
    FSM.ComputeFSM();
#endif
  }
};

//A common ancestor for all debugger windows that can be docked in one of the notebooks or undocked as separate frames
//Handles setting the frame properties based on WindowSettings
//Also handles the pinning and unpinning logic
class UnpinnableWxFrame : public wxFrame, public UnpinnableWindow
{
protected:
  bool _isDocked;
  
  //pins/unpins the window to the notebook and sets all variables and windows styles accordingly
  void PinWindow(wxFlatNotebookExt *notebook, WindowSettings& wndSettings);
  void UnpinWindow(WindowSettings& wndSettings);
  //used to initialize the window according to provided WindowSettings
  void LoadFromSettings(WindowSettings& wndSettings);
public:
  UnpinnableWxFrame(wxWindow* parent, wxWindowID id = wxID_ANY, const wxString& title = wxT(""), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = wxDEFAULT_FRAME_STYLE):
      wxFrame(parent,id,title,pos,size,style)
  {
    _isDocked = false;
  }

  //re-adds the window to notebook (if docked), or sets focus on the window (if not, or already added)
  void RenewWindow(wxFlatNotebookExt * notebook);

  bool IsDocked() const
  {
    return _isDocked;
  }
};

#if _SD_DEBUG_LOG || _SD_SCRIPT_LOG
class DebugOutPutWindow;
class DiagFilter;
class DiagSearch;

//Text control for the output windows (actually skips it's key events)
class wxDebugTextCtrl : public wxTextCtrl
{
private:
  DebugOutPutWindow* _dbgWindow;
public:
  wxDebugTextCtrl(DebugOutPutWindow* dbgWindow, wxWindow* parent, wxWindowID id, const wxString& value = wxT(""), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = 0, const wxValidator& validator = wxDefaultValidator, const wxString& name = wxTextCtrlNameStr );

  //Evt handling methods
  void OnKeyDown(wxKeyEvent & evt);

#if _SD_LIMIT_OUTPUT_LINES
  // removes the first n lines from the control
  void RemoveFirstLines(int n);
#endif

  DECLARE_EVENT_TABLE()
};

#if _SD_LIMIT_OUTPUT_LINES
//the text control used for setting and displaying the line limit in output windows
class wxMaxLinesTextCtrl : public wxTextCtrl
{
private:
  DebugOutPutWindow* _dbgWindow;
public:
  wxMaxLinesTextCtrl(DebugOutPutWindow* dbgWindow, wxWindow* parent, wxWindowID id, const wxString& value = wxT(""), const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = 0, const wxValidator& validator = wxDefaultValidator, const wxString& name = wxTextCtrlNameStr );
  
  void OnFocusEvent(wxFocusEvent& evt);

  DECLARE_EVENT_TABLE()
};
#endif

#define ID_MAX_LINES_TEXT_CTRL 2000

//A window for displaying some sort of output from VBS
//Has necessary methods for filtering the output and searching in it
class DebugOutPutWindow : public UnpinnableWxFrame, public RefCount
{
private:
  wxTextAttr _highlightTextStyle;
  wxTextAttr _defaultTextStyle;

#if _SD_LIMIT_OUTPUT_LINES
  int _lines; //number of lines currently displayed in the textControl
  int _maxLines; //maximum number of lines that the debug output can contain
  int _maxLinesWanted; //used in the logic for checking if the maxLines input is valid
#endif

  FilterFSMs _filterFSMs;
  DiagFilter* _filterDialog;
  DiagSearch* _searchDialog;

  SearchData _searchData;

  DebuggerWindows _wndType;

  wxMenu * _windowMenu;
  wxMenu * _utilsMenu;

  void CreateGUI(); //adds all the wx controls
protected:
  wxPanel* m_panel6;

  void InitFilters();
  void InitACFilter(wxString & stringFilter, AhoCorasick<char, std::string> & filter);
  void InitACFilter(wxString & stringFilter, AhoCorasick<wxChar, wxString> & filter);
public:
  DebugOutPutWindow(wxWindow* parent, wxString title, DebuggerWindows windowType);
  virtual ~DebugOutPutWindow();

  wxDebugTextCtrl* DebugOutputTextArea;
#if _SD_LIMIT_OUTPUT_LINES
  wxMaxLinesTextCtrl* MaxLinesTextCtrl;
#endif
  wxButton* Clear;

  /// Clear the debug window.
  void OnClear(wxCommandEvent& event);

  //Debug output handling
  DebuggerWindows GetWindowType();
  void AppendDebugText(wxString & text, bool runFilter = false);
  void SetFilters (wxString & include, wxString & exclude, wxString & highlight);
  bool FilterOutput(const char * str) const;
  void HighlightAllOutput();
  bool ToggleAutoScroll(); //returns the value after the change
#if _SD_LIMIT_OUTPUT_LINES
  int GetMaxLines() const; //returns the maximum number of lines that the output can hold
  void SetMaxLines(int n, bool updateText = true); //if updateText is true, the textCtrl will also be updated with the new value
#endif
  //Search window events
  void OnFindNext();
  void OnFindAll();
  void OnClearSearch();
  void OnFocusNextSearchResult();
  void OnUpdateSearchHistory();

  //inherited from UnpinnableWindow
  void unpinWindow(wxWindow * frame);
  void pinWindow(wxWindow * frame, wxFlatNotebookExt * notebook);

  //UI related methods
  void InitMenuBar();
  void ShowSearchDialog();

  //EVENT HANDLING
  void OnMenuSelected(wxCommandEvent & event);
  void OnKeyEvent( wxKeyEvent & event);
  void FilterDialogClosed(); //called directly by the filter dialog
  void OnSearchWindowEvent( wxCommandEvent& evt);
#if _SD_LIMIT_OUTPUT_LINES
  void OnMaxLinesCtrlChange(wxCommandEvent& evt); //used to prevent the user from inputting invalid characters into the max lines ctrl
  void OnMaxLinesFocusChange(wxFocusEvent& evt);
  void OnMaxLinesEnter(wxCommandEvent& evt);
#endif

  /// any class wishing to process wxWidgets events must use this macro
  DECLARE_EVENT_TABLE()
};
#endif

//override of application framework as we need different output handling
class ScriptDebuggerFrameWork : public AppFrameWorkBase
{
private:
  wxString _LogFbuffer;
  DWORD _UIThreadID;
#if _SD_DEBUG_LOG
  DebugOutPutWindow *_debugWindow; 
#endif
  CRITICAL_SECTION _cs;
public:
  ScriptDebuggerFrameWork()
  {
    InitializeCriticalSection(&_cs);
#if _SD_DEBUG_LOG
    _debugWindow = NULL;
#endif
  }

  void SetTextControl(wxTextCtrl *ctrl)
  {}
#if _SD_DEBUG_LOG
  void SetDebugWindow(DebugOutPutWindow *wnd)
  {
    _UIThreadID = GetCurrentThreadId();
    _debugWindow =  wnd;
  }
#endif

  // System status
  void LogF(const char *msg)
  {   
#if _SD_DEBUG_LOG
    if (_debugWindow)
    {
      DWORD threadID = GetCurrentThreadId();
      //! LogF statement is from another thread
      //! than the UI thread. So we have to buffer this.
      if (threadID!=_UIThreadID)
      {
EnterCriticalSection(&_cs);
          _LogFbuffer = _LogFbuffer + CharToUTF16(msg);
LeaveCriticalSection(&_cs);
        return;
      }

      // Append any outstanding buffers.
      wxString newText = CharToUTF16(msg);
EnterCriticalSection(&_cs);
        newText = newText + _LogFbuffer;     
        _LogFbuffer.Clear();
LeaveCriticalSection(&_cs);
      _debugWindow->AppendDebugText(newText); 
    }
    else
    {
      // Output debug string is thread safe.
      OutputDebugStringA(msg);
    }
#else
    // Output debug string is thread safe.
    OutputDebugStringA(msg);
#endif
  }
  // Dialog box pops up
  void WarningMessage(const char *msg)
  {
    wxMessageBox(CharToUTF16(msg), L"Warning", wxOK ,NULL);
  }
  // Dialog pops up and the program does a memory dump and call stack.
  void ErrorMessage(const char *msg)
  {
    wxMessageBox(CharToUTF16(msg), L"Critical error cannot recover from. Application closing down.\r\n\r\n%s", wxOK ,NULL);
    exit(1);
  }
};

#if _SD_DEBUG_LOG || _SD_SCRIPT_LOG
class DiagFilter : public wxFrame
{
protected:
  DebugOutPutWindow * _debugWindow;

  wxComboBox* _comboInclude;
  wxComboBox* _comboExclude;
  wxComboBox* _comboHighlight;

  wxButton* _okButton;
  wxButton* _cancelButton;
public:
  DiagFilter(DebugOutPutWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style);
  ~DiagFilter();
  void OnButtonPressed(wxCommandEvent & evt);

#if _SD_UI_IMPROVEMENTS
  //The text enter event
  void OnEnter(wxCommandEvent& evt);
#endif

  DECLARE_EVENT_TABLE()
};
#endif

//id's of the buttons in search dialog (some have wx IDs)
#define IDC_BTN_FIND_NEXT 2001
#define IDC_BTN_FIND_ALL 2002

DECLARE_EVENT_TYPE(SearchWindowEvent,wxID_ANY)
//enumerates the possible search events
enum
{
  SearchDiagClose = 1,
  SearchDiagFindAll,
  SearchDiagFindNext,
  SearchDiagClear,
  SearchDiagUpdateHistory
};

//modifiers for the search dialog (enable certain controls)
#define SD_SEARCH_FIND_ALL  1
#define SD_SEARCH_CASE_SENS 2
#define SD_SEARCH_WHOLE_WORD 4

class DiagSearch : public wxFrame
{
protected:
  wxFrame* _parentWindow;
  wxWindow* _searchWindow;
  wxComboBox* _comboSearch;
  wxCheckBox* _checkWholeWord;
  wxCheckBox* _checkMatchCase;

  wxButton* _buttonFindNext;
  wxButton* _buttonFindAll;
  wxButton* _buttonClear;
  wxButton* _buttonClose;
   
  void UpdateSearchHistory();
#if _SD_ESC_CLOSE_SEARCH
  //Connects all events that need to be specified at control-level
  void ConnectEvents();
#endif
public:
  DiagSearch(wxFrame* parentWindow, wxWindow* searchWindow, wxWindowID id, const wxString& title, int dialogOptions, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = 0);
  ~DiagSearch();

  //is whole word only checked?
  bool WholeWordOnly() const;
  //is match case checked?
  bool MatchCase() const;
  //the string to search for
  wxString SearchString() const;

#if _SD_UI_IMPROVEMENTS
  //sets the string to search for into the combo box
  void SetSearchString(const wxString& str);

  void OnFocus(wxFocusEvent& evt);
  //text enter events
  void OnEnter(wxCommandEvent& evt);
#endif
#if _SD_ESC_CLOSE_SEARCH
  void OnKeyDown(wxKeyEvent& evt);
#endif
  void OnButtonClick(wxCommandEvent& evt);
  //sets the window that will receive the search events
  void SetSearchWindow(wxWindow* wnd);
  void SetSearchHistory(std::vector<wxString>& prev);

  DECLARE_EVENT_TABLE()
};

#define PLOT_CONTEXT_MENU_ZOOM_IN 2001
#define PLOT_CONTEXT_MENU_ZOOM_OUT 2002
#define PLOT_CONTEXT_MENU_ZOOM_DEFAULT 2003
#define PLOT_CONTEXT_MENU_DEFAULT_ORIGIN 2004

class DiagFrameRateEx : public DiagFrameRate, public RefCount
{
  wxPGId _perfGraphCat;
public:
  DiagFrameRateEx(wxWindow* parent);
  virtual ~DiagFrameRateEx();

  void Simulate(float deltaT);

  void showContextMenu( wxMouseEvent & event);
  void onContextMenuSelect(wxCommandEvent & event);
  
  /// any class wishing to process wxWidgets events must use this macro
  DECLARE_EVENT_TABLE()
};

#if _VBS_14573_DEBUG_CONSOLE
#define DEBUG_CONSOLE_MENU_PIN 2201
#define DEBUG_CONSOLE_MENU_ONTOP 2202

class DebugConsoleWindow : public UnpinnableWxFrame, public RefCount
{
private:
  wxMenu * _windowMenu;

  wxButton* _btnRun; //executes the code
  wxButton* _btnClear; //clears the code
  
  wxTextCtrl* _textReturn; //text control for displaying the result of the code
  wxScintilla* _code; //control for typing the code in
  
  wxStaticText* _textScope; //label showing the current scope in which the code will be executed
protected:
  //runs the code from _code in VBS
  void ExecuteCode();
public:
  DebugConsoleWindow(wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos = wxDefaultPosition, const wxSize& size = wxDefaultSize, long style = wxDEFAULT_FRAME_STYLE);
  virtual ~DebugConsoleWindow();

  //inherited from UnpinnableWindow
  void unpinWindow(wxWindow * frame);
  void pinWindow(wxWindow * frame, wxFlatNotebookExt * notebook);
  //Event handling
  void OnButtonPressed(wxCommandEvent& evt);
  void OnMenuSelected(wxCommandEvent& evt);
  //Updates the scope label
  void SetScope(const wxString & scope);

  DECLARE_EVENT_TABLE()
};
#endif