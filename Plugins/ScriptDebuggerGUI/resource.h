//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by ScriptDebuggerGUI.rc
//
#define IDR_PNG_PAUSE                   105
#define IDR_PNG_PLAY                    106
#define IDR_PNG_STOP                    107
#define IDR_PNG_LIST                    108
#define ICON_TRAY                       121
#define ICON_WINDOW                     122
#define BMP_FOLDER                      123
#define BMP_DOC_UNKNOWN                 124
#define BMP_DOC_SQF                     125
#define BMP_DOC_SQS                     126
#define BMP_DOC_DOC                     127
#define IDR_DATA1                       128
#define IDR_FILE1                       130
#define IDB_BITMAP2                     132
#define IDR_DATA2                       136
#define IDR_DATA3                       137
#define IDR_PNG_FOLDER                  137
#define IDR_PNG_EXCLAMATION_MARK        138
#define IDR_PNG_HOURGLASS               139

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        140
#define _APS_NEXT_COMMAND_VALUE         40002
#define _APS_NEXT_CONTROL_VALUE         1001
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
