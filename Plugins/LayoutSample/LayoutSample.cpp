#include "stdafx.h"

#include "wx/wx.h"
#include "wx/msw/private.h"
#include "wx/evtloop.h"

// Essential framework for main application development
#include "../../Common/Essential/AppFrameWork.hpp"

// Include the template for the layout
#include "./ui/LayoutSample.h"

class MyFrameExtended : public MyFrame
{
public:
  MyFrameExtended():MyFrame(NULL)
  {
    Show(true);
  }
};

// Declaration of the map and the main frame that we will
// be using for the main testing
class MyApp : public wxApp
{
  typedef wxApp base;
  bool _isActive;
public:  
  // Main constructor, nothing serious happens here
  MyApp();
  virtual ~MyApp();

  // OnInit called to Initialize all the windows
  virtual bool OnInit();

  virtual int MainLoop()
  {
    return base::MainLoop();
  }
  virtual void ExitMainLoop()
  {
    base::ExitMainLoop();
    _isActive = false;
  }
  bool HasMainLoop()
  {
    return _isActive;
  }
};

MyApp::MyApp()
{
  _isActive = true;
      //wxEventLoopTiedPtr mainLoop(&m_mainLoop, new wxEventLoop);
    //  m_mainLoop = new wxEventLoop;
}
MyApp::~MyApp()
{
 // delete m_mainLoop;
}

MyFrameExtended *GFrame;
bool MyApp::OnInit()
{
  GFrame = new MyFrameExtended;
  return true;
}

DECLARE_APP(MyApp)
IMPLEMENT_APP_NO_MAIN(MyApp)

bool WXResourcesAllocated;
void CleanWxGUIResources()
{
  // Check to see if the wxResources have been allocated
  // and if we free them
  if (WXResourcesAllocated)
  {
    // Tell the app that its closing down.
    wxTheApp->OnExit();

    // Will delete the wxApp, thus ending its life.
    wxEntryCleanup();
    WXResourcesAllocated = false;
  }
}

bool StartWxResources(int argc = 0, char **argv = NULL )
{
  bool initOk = true;

  wxInitAllImageHandlers();
  WXResourcesAllocated = wxEntryStart(argc, argv); 

  if ( !wxTheApp || !wxTheApp->CallOnInit() )
  {
    // Report error
    LogF("Failed to create the plug ins script debugger app");

    // Clean up the resources.
    CleanWxGUIResources();    
    initOk = false;
  }

  return initOk;
}

#if 1
int main(int argc, char * argv[])
{
  LogF("Main version of commCtrl:%d\n", wxApp::GetComCtl32Version() );

  if (StartWxResources(argc,argv))
  {
      wxTheApp->SetExitOnFrameDelete(true);
      //class CallOnExit
      //{
      //public:
      //~CallOnExit() { wxTheApp->OnExit(); }
      //} callOnExit;
      //wxTheApp->OnRun();

      MSG msg;
      while (true)
      {
        ZeroMemory(&msg,sizeof(msg));
        while (PeekMessage(&msg,NULL, 0, 0, PM_REMOVE))
        { 
          TranslateMessage(&msg); 
          DispatchMessage(&msg); 
        }
        // Process idle messages, this will contain the message to 
        // destroy the top window
        wxTheApp->ProcessIdle();

        // If there is no more top level windows then quit
        //if (!wxTheApp->GetTopWindow()) return 0;
        if (!wxGetApp().HasMainLoop()) break;
      }
  }
  CleanWxGUIResources();

  return 0;
}
#endif