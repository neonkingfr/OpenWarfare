#include <Windows.h>
#include <WinBase.h>

#include "wx/wx.h"
#include "../../Common/Essential/AppFrameWork.hpp"


class wxTextCtrl;

wxString ASCIICharToWChar(const char *msg)
{
#if _UNICODE
  WCHAR wBuf[512];
  MultiByteToWideChar(CP_ACP, 0, msg, -1, wBuf, lenof(wBuf));
  return wxString(wBuf);
#else
  return wxString(msg);
#endif
}

class Win32WxWidgetFrameWork : public AppFrameWorkBase
{
  wxTextCtrl *_textControl; 
public:
  Win32WxWidgetFrameWork()
  {
    _textControl = NULL;
  }

  void SetTextControl(wxTextCtrl *ctrl)
  {
    _textControl =  ctrl;
  }

  // System status
  void LogF(const char *msg)
  {   
    wxString wxMsg = ASCIICharToWChar(msg);

    if (_textControl) _textControl->AppendText(wxMsg); 
    else OutputDebugString(wxMsg.data());
  }
  // Dialog box pops up
  void WarningMessage(const char *msg)
  {
    wxString wxMsg = ASCIICharToWChar(msg);
    wxMessageBox(wxMsg, L"Warning", wxOK ,NULL);
  }
  // Dialog pops up and the program does a memory dump and call stack.
  void ErrorMessage(const char *msg)
  {
    wxString wxMsg = ASCIICharToWChar(msg);
    wxMessageBox(wxMsg,L"Critical error cannot recover from. Application closing down.\n%s", wxOK ,NULL);
    exit(1);
  }
};

Win32WxWidgetFrameWork GWin32FrameWrok;
AppFrameWorkBase* GFrameWork = &GWin32FrameWrok;
