// D-BOX Sample plugin illustrating connection to D-BOX motion library and some basic comunication between VBS and D-BOX plugin.

#include <windows.h>
#include <stdio.h>
#include <math.h>
#include "DBOXPlugin.h"

// TODO: Set APP_KEY to the key that was assigned to you by D-BOX.
#define APP_KEY			"VBS2"
// TODO: Set APP_BUILD to an integer representing your build number.
#define APP_BUILD		1

char lastVehicleType[256];

#include "LiveMotionSdk/dboxLiveMotion.h"
#pragma comment(lib, DBOX_LIVEMOTION_LIB)

/// These are your unique event ids that you'll use when calling PostEvent.
/// You can use the numbers that are the most appropriate for your application.
/// These values should be unique and not change in future releases.
enum DboxAppEvents {
  WEAPON_FIRE = 1, // not used now
  IMPACT_EVENT = 2, // not used now
  VEHICLE_CONFIG = 3,
  GENERAL_TELEMETRY = 4,
  ENGINE_STARTED = 5,
  ENGINE_STOPPED = 6,
  IMPULSE_EVENT = 7,
};

/// Structure for impulses (weapon fires, etc.)
struct DboxImpulseEvent {
  dbox::XyzFloat32	Impulse;
  DBOX_STRUCTINFO_BEGIN()
    DBOX_STRUCTINFO_FIELD(DboxImpulseEvent, Impulse, dbox::FT_XYZ_FLOAT32, dbox::FM_FRAME_CUSTOM_01)
  DBOX_STRUCTINFO_END()
};

/// Structure for vehicle config (only sent once when entering a new vehicle or returning on foot)
struct DboxVehicleConfig {
  dbox::PCHAR VehicleType;

  DBOX_STRUCTINFO_BEGIN()
    DBOX_STRUCTINFO_FIELD(DboxVehicleConfig, VehicleType, dbox::FT_CHAR_PTR32, dbox::FM_VEHICLE_TYPE)
  DBOX_STRUCTINFO_END()
};

/// Sample real-time frame structure for FRAME_UPDATE event.
struct DboxGeneralTelemetry {
  dbox::I32 EngineRpm;
  dbox::I32 TransmissionGear;
  dbox::QuadFloat32 SuspensionTravel; //< If given in a relative value, you can omit the SuspensionMaxTravel field in the GroundVehicleConfig struct.
  dbox::XyzFloat32 Velocity;
  dbox::F32 HeadingRad;
  dbox::F32 PitchRad;
  dbox::F32 RollRad;

  DBOX_STRUCTINFO_BEGIN()
    DBOX_STRUCTINFO_FIELD(DboxGeneralTelemetry, EngineRpm, dbox::FT_INT32, dbox::FM_ENGINE_RPM)
    DBOX_STRUCTINFO_FIELD(DboxGeneralTelemetry, TransmissionGear, dbox::FT_INT32, dbox::FM_ENGINE_TRANSMISSION_GEAR)
    DBOX_STRUCTINFO_FIELD(DboxGeneralTelemetry, SuspensionTravel, dbox::FT_QUAD_FLOAT32, dbox::FM_SUSPENSION_TRAVEL_QUAD)
    DBOX_STRUCTINFO_FIELD(DboxGeneralTelemetry, Velocity, dbox::FT_XYZ_FLOAT32, dbox::FM_VELOCITY_XYZ)
    DBOX_STRUCTINFO_FIELD(DboxGeneralTelemetry, HeadingRad, dbox::FT_FLOAT32, dbox::FM_YAW_RAD)
    DBOX_STRUCTINFO_FIELD(DboxGeneralTelemetry, PitchRad, dbox::FT_FLOAT32, dbox::FM_PITCH_RAD)
    DBOX_STRUCTINFO_FIELD(DboxGeneralTelemetry, RollRad, dbox::FT_FLOAT32, dbox::FM_ROLL_RAD)
  DBOX_STRUCTINFO_END()
};

// Command function declaration
typedef int (WINAPI * ExecuteCommandType)(const char *command, char *result, int resultLength);

// Command function definition
ExecuteCommandType ExecuteCommand = NULL;

// Function that will register the ExecuteCommand function of the engine
DBOXPLUGIN_EXPORT void WINAPI RegisterHWND(void *hwnd, void *hins)
{
}

// Function that will register the ExecuteCommand function of the engine
DBOXPLUGIN_EXPORT void WINAPI RegisterCommandFnc(void *executeCommandFnc)
{
  ExecuteCommand = (ExecuteCommandType)executeCommandFnc;
}

struct Vector3
{
  float _x;
  float _y;
  float _z;

  Vector3():_x(0),_y(0),_z(0){}
  Vector3(float x, float y, float z ):_x(x),_y(y),_z(z){}

  float X() const{return _x;}
  float Y() const{return _y;}
  float Z() const{return _z;}

  float SizeXZ() const{return sqrt(X()*X()+Z()*Z());}

  Vector3 CrossProduct( Vector3 op ) const
  {
    float x=Y()*op.Z()-Z()*op.Y();
    float y=Z()*op.X()-X()*op.Z();
    float z=X()*op.Y()-Y()*op.X();
    return Vector3(x,y,z);
  }

  void Rotate( const Vector3 aside, const Vector3 up, const Vector3 dir, Vector3 o )
  { // vector rotation - 3x3 matrix (aside,up,dir) is used
    // u=M*v
    float o0=o.X(),o1=o.Y(),o2=o.Z();
    _x=aside.X()*o0+aside.Y()*o1+aside.Z()*o2;
    _y=up.X()*o0+up.Y()*o1+up.Z()*o2;
    _z=dir.X()*o0+dir.Y()*o1+dir.Z()*o2;
  }

  bool operator == ( Vector3 cmp ) const {return (cmp.X()==X() && cmp.Y()==Y() && cmp.Z()==Z());}
  bool operator != ( Vector3 cmp ) const {return (cmp.X()!=X() || cmp.Y()!=Y() || cmp.Z()!=Z());}

};

#define VZero Vector3(0,0,0)

char buf[1024];
char *Format(const char *format, ...)
{
  va_list va;
  va_start(va,format);
  vsnprintf(buf,sizeof(buf)-1,format,va);
  buf[sizeof(buf)-1]=0;
  va_end(va);
  return buf;
}

// in - array[handle,time] & text , out - handle
void ExecuteDiagMessage(int &handle, int time, char* text)
{
  if(!text) {handle = -1; return;}

  // fill command line
  char commandBuff[256];
  sprintf(commandBuff,"[%d,%d] diagMessage \"%s\"",handle,time,text);

  char resultBuff[256];

  // send command
  if(!ExecuteCommand(commandBuff, resultBuff, 256))
  {//failed
    OutputDebugString(resultBuff);
    handle = -1; 
    return;
  }

  //parse to handle
  handle = atoi(resultBuff);
  return;
}

// use Format function to preformat if necessary
#define DIAG_MESSAGE(time,text) do \
{	\
  static int handle=-1; \
  ExecuteDiagMessage(handle, time, text); \
} while (false);


// in - nothing, out - bool (0,1, -1=err )
bool ExecuteScriptCommand(bool &ret, char* command)
{
  if(!command) return false;

  // send command
  char resultBuff[256];
  if(!ExecuteCommand(command, resultBuff, 256))
  {//failed
    OutputDebugString(resultBuff);
    return false;
  }

  //parse to bool
  if(resultBuff[0])
  {
    if(!strcmp("true",resultBuff)) {ret = true; return true;}
    else if(!strcmp("false",resultBuff)) {ret = false; return true;}
  }

  return false;
}

// in - string, out - string
bool ExecuteScriptCommand(char *ret, char* command, char* text)
{
  if(!ret || !command || !text) return false;

  // fill command line
  char commandBuff[256];
  sprintf(commandBuff,"%s %s",command,text);

  // send command
  char resultBuff[256];
  if(!ExecuteCommand(commandBuff, resultBuff, 256))
  {//failed
    OutputDebugString(resultBuff);
    return false;
  }

  // parse to string
  int len = strlen(resultBuff);
  if(len > 0 && resultBuff[0] == '"' && resultBuff[len-1] == '"')// is in apostrophes
  {
    resultBuff[len-1] = 0;

    //copy to ret string without apostrophes
    strcpy(ret,&resultBuff[1]);
  }
  else
  {
    //copy to ret string
    strcpy(ret,resultBuff);
  }

  return true;
}

// in - string, out - float
bool ExecuteScriptCommand(float &ret, char* command, char* text)
{
  if(!command || !text) return false;

  // fill command line
  char commandBuff[256];
  sprintf(commandBuff,"%s %s",command,text);
 
  // send command
  char resultBuff[256];
  if(!ExecuteCommand(commandBuff, resultBuff, 256))
  {//failed
    OutputDebugString(resultBuff);
    return false;
  }

  //parse to float
  ret = (float)atof(resultBuff);

  return true;
}

// in - string, out - vector[x,z,y]
bool ExecuteScriptCommand(Vector3 &ret, char* command, char* text)
{
  if(!command || !text) return false;

  // fill command line
  char commandBuff[256];
  sprintf(commandBuff,"%s %s",command,text);

  // send command
  char resultBuff[256];
  if(!ExecuteCommand(commandBuff, resultBuff, 256))
  {//failed
    OutputDebugString(resultBuff);
    return false;
  }

  //parse to vector
  if(resultBuff[0] && resultBuff[0] == '[')
  {
    if(sscanf( resultBuff, "[%f,%f,%f]", &ret._x, &ret._z, &ret._y) != 3) return false;
  }

  return true;
}

// in - vector[x,z,y], out - float
bool ExecuteScriptCommand(float &ret, char* command, Vector3 vect)
{
  if(!command) return false;

  // fill command line
  char commandBuff[256];
  sprintf(commandBuff,"%s [%f,%f,%f]",command,vect.X(),vect.Z(),vect.Y());

  // send command
  char resultBuff[256];
  if(!ExecuteCommand(commandBuff, resultBuff, 256))
  {//failed
    OutputDebugString(resultBuff);
    return false;
  }

  //parse to float
  ret = (float)atof(resultBuff);

  return true;
}

// in - string, out - 2xvector [[x,z,y],[x,z,y]]
bool ExecuteScriptCommand(Vector3 &ret1, Vector3 &ret2, char* command, char *text)
{
  if(!command || !text) return false;

  // fill command line
  char commandBuff[256];
  sprintf(commandBuff,"%s %s",command,text);

  // send command
  char resultBuff[256];
  if(!ExecuteCommand(commandBuff, resultBuff, 256))
  {//failed
    OutputDebugString(resultBuff);
    return false;
  }

  //parse to 2xvector
  if(resultBuff[0] && resultBuff[0] == '[')
  {
    if(sscanf( resultBuff, "[[%f,%f,%f],[%f,%f,%f]]", &ret1._x, &ret1._z, &ret1._y, &ret2._x, &ret2._z, &ret2._y) != 6) return false;
  }

  return true;
}

// in - string & vector[x,z,y], out - vector [x,z,y]
bool ExecuteScriptCommand(Vector3 &ret, char* command, char *text, Vector3 vect)
{
  if(!command || !text) return false;

  // fill command line
  char commandBuff[256];
  sprintf(commandBuff,"%s %s [%f,%f,%f]",text,command,vect.X(),vect.Z(),vect.Y());

  // send command
  char resultBuff[256];
  if(!ExecuteCommand(commandBuff, resultBuff, 256))
  {//failed
    OutputDebugString(resultBuff);
    return false;
  }

  //parse to vector
  if(resultBuff[0] && resultBuff[0] == '[')
  {
    if(sscanf( resultBuff, "[%f,%f,%f]", &ret._x, &ret._z, &ret._y) != 3) return false;
  }

  return true;
}

// This function will be executed every simulation step (every frame) and took a part in the simulation procedure.
// We can be sure in this function the ExecuteCommand registering was already done.
// deltaT is time in seconds since the last simulation step
DBOXPLUGIN_EXPORT void WINAPI OnSimulationStep(float deltaT)
{
  // check whether simulation and D-BOX is running 
  {
    bool simRunning = false;
    if(!ExecuteScriptCommand(simRunning,"isSimulationEnabled")) return;

    // if simulation is running and motion isn't running, fade in motion
    if(simRunning && !dbox::LiveMotion::IsStarted()) dbox::LiveMotion::Start();

    // if simulation isn't running and motion is running, fade out motion
    if(!simRunning && dbox::LiveMotion::IsStarted()) dbox::LiveMotion::Stop();

    // if simulation or D-BOX not running, return
    if(!simRunning || !dbox::LiveMotion::IsStarted()) return;
  }

  // D-BOX config
  {
    // vehicle type
    char vehicleType[256];
    ExecuteScriptCommand(vehicleType, "getText" , "(configFile >> \"CfgVehicles\" >> (typeof vehicle player) >> \"simulation\")");

    if(strncmp(lastVehicleType,vehicleType,256))// status changed!
    {
      strncpy(lastVehicleType,vehicleType,256); // remember last vehicle

      DboxVehicleConfig config;
      config.VehicleType = vehicleType;

      DIAG_MESSAGE(100000, Format("Vehicle = %s", config.VehicleType));

      dbox::LiveMotion::PostEvent(VEHICLE_CONFIG,config);
    }
  }

  //D-BOX telemetry
  {
    //position and orientation in world coords
    Vector3 wPosition = VZero;
    Vector3 wDirection = VZero;
    Vector3 wUp = VZero;
    Vector3 wAside = VZero;

    ExecuteScriptCommand(wPosition, "position", "vehicle player");
    ExecuteScriptCommand(wDirection, "vectorDir", "vehicle player"); 
    ExecuteScriptCommand(wUp, "vectorUp", "vehicle player"); 
    wAside = wUp.CrossProduct(wDirection);

    // velocity in world coords
    Vector3 wVelocity = VZero;
    ExecuteScriptCommand(wVelocity,"velocity", "vehicle player"); 

    // velocity in model coords
    Vector3 mVelocity = VZero;
    mVelocity.Rotate(wAside,wUp,wDirection,wVelocity);

    //HPR
    float heading = atan2(wDirection.X(), wDirection.Z());
    float pitch = atan2(wDirection.Y(),wDirection.SizeXZ());
    float roll = atan2(wAside.Y(),wAside.SizeXZ());

    // engine RPM
    float rpm = 0;
    static float rpmMultiply = 6000;
    ExecuteScriptCommand(rpm,"getRpm", "vehicle player"); 

    // Gear
    float gear = 0;
    ExecuteScriptCommand(gear,"getGear", "vehicle player"); 


    // Bounding box
    Vector3 mMin = VZero;
    Vector3 mMax = VZero;
    ExecuteScriptCommand(mMin,mMax,"boundingBox", "vehicle player");

    // wheel positions //< i think i will be able to give you some dumper informations, but this will be fine for first approach.
    static float WheelPosCoef = 0.8f;
    Vector3 wFLWheelPos = VZero;
    Vector3 wFRWheelPos = VZero;
    Vector3 wBLWheelPos = VZero;
    Vector3 wBRWheelPos = VZero;
    ExecuteScriptCommand(wFLWheelPos,"modelToWorld", "vehicle player", Vector3(mMin.X()*WheelPosCoef,mMin.Y(),mMax.Z()*WheelPosCoef));
    ExecuteScriptCommand(wFRWheelPos,"modelToWorld", "vehicle player", Vector3(mMax.X()*WheelPosCoef,mMin.Y(),mMax.Z()*WheelPosCoef));
    ExecuteScriptCommand(wBLWheelPos,"modelToWorld", "vehicle player", Vector3(mMin.X()*WheelPosCoef,mMin.Y(),mMin.Z()*WheelPosCoef));
    ExecuteScriptCommand(wBRWheelPos,"modelToWorld", "vehicle player", Vector3(mMax.X()*WheelPosCoef,mMin.Y(),mMin.Z()*WheelPosCoef));

    // Wheel SurfaceY
    float wFLWheelSurfY = 0;
    float wFRWheelSurfY = 0;
    float wBLWheelSurfY = 0;
    float wBRWheelSurfY = 0;
    ExecuteScriptCommand(wFLWheelSurfY,"roadSurfaceHeight", wFLWheelPos); 
    ExecuteScriptCommand(wFRWheelSurfY,"roadSurfaceHeight", wFRWheelPos);
    ExecuteScriptCommand(wBLWheelSurfY,"roadSurfaceHeight", wBLWheelPos);
    ExecuteScriptCommand(wBRWheelSurfY,"roadSurfaceHeight", wBRWheelPos);

    // Roughness
    static float maxHeightConst = 0.1f; // Max height above surface which we consider as a land contact
    static float minRoughness = 0.01f; // minimal roughness if we have a land contact
    float wFLWheelRoughness = 0;
    float wFRWheelRoughness = 0;
    float wBLWheelRoughness = 0;
    float wBRWheelRoughness = 0;
    if(wFLWheelPos.Y() < (wFLWheelSurfY+maxHeightConst)) 
    {
      ExecuteScriptCommand(wFLWheelRoughness,"getSurfRoughness", wFLWheelPos); 
      wFLWheelRoughness +=minRoughness;
    }
    if(wFRWheelPos.Y() < (wFRWheelSurfY+maxHeightConst)) 
    {
      ExecuteScriptCommand(wFRWheelRoughness,"getSurfRoughness", wFRWheelPos);
      wFRWheelSurfY +=minRoughness;
    }
    if(wBLWheelPos.Y() < (wBLWheelSurfY+maxHeightConst)) 
    {
      ExecuteScriptCommand(wBLWheelRoughness,"getSurfRoughness", wBLWheelPos);
      wBLWheelSurfY +=minRoughness;
    }
    if(wBRWheelPos.Y() < (wBRWheelSurfY+maxHeightConst)) 
    {
      ExecuteScriptCommand(wBRWheelRoughness,"getSurfRoughness", wBRWheelPos);
      wBRWheelSurfY +=minRoughness;
    }


    // Send frame update telemetry
    DboxGeneralTelemetry telemetry;
    telemetry.EngineRpm = (int)(rpm*rpmMultiply); //< our rpm value is strange (in most cases in <0,1>, but not everywhere). Unfortunately we don't have anything better or else. Some multiplier will work fine i hope :)
    telemetry.TransmissionGear = (int)gear;
    telemetry.SuspensionTravel.FrontLeft = wFLWheelRoughness; //< i give you roughness here, but you can randomize it there to suspension travel, if needed (or on your side, if you want)
    telemetry.SuspensionTravel.FrontRight = wFRWheelRoughness;
    telemetry.SuspensionTravel.BackLeft = wBLWheelRoughness;
    telemetry.SuspensionTravel.BackRight = wBRWheelRoughness;
    telemetry.Velocity.X = mVelocity.X();
    telemetry.Velocity.Y = mVelocity.Y();
    telemetry.Velocity.Z = mVelocity.Z();
    telemetry.HeadingRad = heading;
    telemetry.PitchRad = pitch;
    telemetry.RollRad = roll;

    /*DIAG_MESSAGE(200, Format("rpm = %d", telemetry.EngineRpm));
    DIAG_MESSAGE(200, Format("gear = %d", telemetry.TransmissionGear));
    DIAG_MESSAGE(200, Format("FL Wheel = %f", telemetry.SuspensionTravel.FrontLeft));
    DIAG_MESSAGE(200, Format("FR Wheel = %f", telemetry.SuspensionTravel.FrontRight));
    DIAG_MESSAGE(200, Format("BL Wheel = %f", telemetry.SuspensionTravel.BackLeft));
    DIAG_MESSAGE(200, Format("BR Wheel = %f", telemetry.SuspensionTravel.BackRight));
    DIAG_MESSAGE(200, Format("velocityX = %f", telemetry.Velocity.X));
    DIAG_MESSAGE(200, Format("velocityY = %f", telemetry.Velocity.Y));
    DIAG_MESSAGE(200, Format("velocityZ = %f", telemetry.Velocity.Z));
    DIAG_MESSAGE(200, Format("heading = %f", telemetry.HeadingRad));
    DIAG_MESSAGE(200, Format("pitch = %f", telemetry.PitchRad));
    DIAG_MESSAGE(200, Format("roll = %f", telemetry.RollRad));*/

    dbox::LiveMotion::PostEvent(GENERAL_TELEMETRY,telemetry);
  }

  //D-BOX Impulse event
  {
    Vector3 recoilImpulse = VZero; //[angX, angY, Z]
    ExecuteScriptCommand(recoilImpulse,"getRecoilImpulse", "player"); // player's weapon recoil
    
    if(recoilImpulse != VZero)// some impulse is applied
    {
      DboxImpulseEvent impulse;
      impulse.Impulse.X = recoilImpulse.X();
      impulse.Impulse.Y = recoilImpulse.Y();
      impulse.Impulse.Z = recoilImpulse.Z();

  
      DIAG_MESSAGE(1000, Format("impulseAngX = %f", impulse.Impulse.X));
      DIAG_MESSAGE(1000, Format("impulseAngY = %f", impulse.Impulse.Y));
      DIAG_MESSAGE(1000, Format("impulseZ = %f", impulse.Impulse.Z));

      dbox::LiveMotion::PostEvent(IMPULSE_EVENT,impulse);
    }
  }
}

void InitDbox()
{
  dbox::ELiveMotionResult ret = dbox::LiveMotion::Initialize(APP_KEY, APP_BUILD);
  if(dbox::LMR_SUCCESS == ret)
  {
    dbox::LiveMotion::RegisterEvent(IMPULSE_EVENT, dbox::EM_IMPACT, DboxImpulseEvent::GetStructInfo());
    dbox::LiveMotion::RegisterEvent(VEHICLE_CONFIG, dbox::EM_CONFIG_UPDATE, DboxVehicleConfig::GetStructInfo());
    dbox::LiveMotion::RegisterEvent(GENERAL_TELEMETRY, dbox::EM_FRAME_UPDATE, DboxGeneralTelemetry::GetStructInfo());
    dbox::LiveMotion::RegisterEvent(ENGINE_STARTED, dbox::EM_ENGINE_START);
    dbox::LiveMotion::RegisterEvent(ENGINE_STOPPED, dbox::EM_ENGINE_STOP);

    // Registration completed, open motion output device.
    // Open can be called once, after end of registration. 
    dbox::LiveMotion::Open();
  }
  else
  {
    OutputDebugString("DBoxLiveMotion initialization failed!");
  }
}

void DeInitDbox()
{
  // Close motion output device.
  dbox::LiveMotion::Close();
  // Terminate
  dbox::LiveMotion::Terminate();
}

// DllMain
BOOL WINAPI DllMain(HINSTANCE hDll, DWORD fdwReason, LPVOID lpvReserved)
{
   switch(fdwReason)
   {
      case DLL_PROCESS_ATTACH:
         InitDbox();
         OutputDebugString("Called DllMain with DLL_PROCESS_ATTACH\n");
         break;
      case DLL_PROCESS_DETACH:
         DeInitDbox();
         OutputDebugString("Called DllMain with DLL_PROCESS_DETACH\n");
         break;
      case DLL_THREAD_ATTACH:
         OutputDebugString("Called DllMain with DLL_THREAD_ATTACH\n");
         break;
      case DLL_THREAD_DETACH:
         OutputDebugString("Called DllMain with DLL_THREAD_DETACH\n");
         break;
   }
   return TRUE;
}
