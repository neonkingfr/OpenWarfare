#define DBOXPLUGIN_EXPORT __declspec(dllexport)

DBOXPLUGIN_EXPORT void WINAPI RegisterHWND(void *hwnd, void *hins);
DBOXPLUGIN_EXPORT void WINAPI RegisterCommandFnc(void *executeCommandFnc);
DBOXPLUGIN_EXPORT void WINAPI OnSimulationStep(float deltaT);