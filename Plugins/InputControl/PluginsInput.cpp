/*
*
* Copyright(c) 2012 Bohemia Interactive Simulations, Inc.
* http://www.bisimulations.com
*
* For information about the licensing and copyright of this software please
* contact Bohemia Interactive Simulations, Inc. at contact @ bisimulations.com.
*
*/

#include "PluginsInput.h"

#include <sstream>

#include <Windowsx.h>
#include <Wincon.h>

#include <windows.h>
#include <iostream>



#include <fstream>

#define SLEEP_MS	50


HANDLE _handle;


struct TLVertex
{
	float x, y, z, rhw;
  float u, v;
};

struct D3DVERTEX
{
   float x, y, z, rhw;
   DWORD color;
};

volatile bool started = false;

volatile bool enableInput = false;
HRESULT            hr   = S_OK;

std::string g_resFolder = "/plugins/";

unsigned int g_windowHeight = 0;
unsigned int g_windowWidth = 0;

unsigned NextPow2( unsigned x ) {    --x;    x |= x >> 1;    x |= x >> 2;    x |= x >> 4;    x |= x >> 8;    x |= x >> 16;    return ++x;} 

void log(const char *str)
{
  std::ofstream outputFile;
  outputFile.open("PluginsInput.log",std::ofstream::app);
  outputFile << str << std::endl;
	outputFile.close();
}

/*!
Following function is called by the VBS2 engine in each an every frame

\param deltatT is an floating point argument which returns times
since the last call.
*/
VBSPLUGIN_EXPORT void WINAPI OnSimulationStep(float deltaT)
{
  
};

/**
 @brief Intercepts all Windows messages to VBS2

 @param hwnd   Handle of the VBS2 window.
 @param code   The Windows message code (WM_INPUT, WM_KEYDOWN, etc).
 @param wParam The wParam field of the message.
 @param lParam The lParam field of the message.

 @return true to pass the message to VBS2, false to kill the message
 */

VBSPLUGIN_EXPORT bool WINAPI OnKeyInput(const RAWKEYBOARD* keyData, DWORD keyTime)
{

  bool retValue = true;

  if(GetAsyncKeyState(VK_LCONTROL) != 0)
  {
    retValue = false;
    printf("=============BLOCKING==============\n\n");
  }
  else
  {
    printf("=============NOT-BLOCKING==============\n\n");
  }

  if(GetAsyncKeyState(VK_RCONTROL) != 0)
  {
    // Slow down console Dump
    Sleep(100);
  }


  if(keyData)
  {
    printf("===========KEY================\n");
    printf("Make Code: %d\n",keyData->MakeCode);
    printf("Flags: %d\n",keyData->Flags);
    printf("Reserved: %d\n",keyData->Reserved);
    printf("VKey: %d\n",keyData->VKey);
    printf("Message: %d\n",keyData->Message);
    printf("ExtraInformation: %d\n",keyData->ExtraInformation);
    printf("===========================\n\n");
  }
  else
  {
    printf("===Mouse DATA NULL");
  }
 

  return retValue;
  
}


VBSPLUGIN_EXPORT bool WINAPI OnMouseInput(const RAWMOUSE* keyData, DWORD keyTime)
{
  bool retValue = true;
 
  if(GetAsyncKeyState(VK_LCONTROL) != 0)
  {
    retValue = false;
    printf("=============BLOCKING==============\n\n");
  }
  else
  {
    printf("=============NOT-BLOCKING==============\n\n");
  }


  if(GetAsyncKeyState(VK_RCONTROL) != 0)
  {
    // Slow down console Dump
    Sleep(100);
  }


  if ( keyData )
  {
    printf("===========Mouse================\n");
    printf("Raw Button: %d\n",keyData->ulRawButtons);
    printf("Last x: %d\n",keyData->lLastX);
    printf("Last y: %d\n",keyData->lLastY);
    printf("===========================\n\n");
  } 
  else
  {
    printf("===Mouse DATA NULL\n");
  }


  return retValue;
}

/*!
The plugin function is called at the initialization of the plugin.

\param input is  an character which controls the execution of the
plugin
*/

VBSPLUGIN_EXPORT const char* WINAPI PluginFunction(const char *input)
{
  static std::string result = "";

 

  return result.c_str();
};



/**
This is called after the user hits the continue button.
*/
VBSPLUGIN_EXPORT void WINAPI OnMissionStart()
{
  if(!started) 
  {
    // this only needs to run once
    started = true;

  }
  else
  {
  }
}


VBSPLUGIN_EXPORT void WINAPI OnMissionEnd()
{
}

// D3D device management, after a reset
// Re-create data for Direct3D device here
VBSPLUGIN_EXPORT void WINAPI OnD3DeviceRestore()
{
 
}

// D3D device management, before a reset
// All handles created on the Direct3D device must be freed here - otherwise VBS2 device reset will fail
VBSPLUGIN_EXPORT void WINAPI OnD3DeviceInvalidate()
{
 
}


// D3D device management, setting the engine D3D device
VBSPLUGIN_EXPORT void WINAPI OnD3DeviceInit(IDirect3DDevice9 *device)
{
 
}

// D3D device management, removing the engine D3D device: 
VBSPLUGIN_EXPORT void WINAPI OnD3DeviceDelete()
{
  
}

#include <direct.h>

//DEBUG CODE
VBSPLUGIN_EXPORT int WINAPI OnLoad(VBSParameters *params)
{
  if(AllocConsole())
  {
    AllocConsole() ;
    AttachConsole( GetCurrentProcessId() ) ;
    freopen( "CON", "w", stdout ) ; 

    printf("Console Window Intialized\n");

  }
  return 0;
}

VBSPLUGIN_EXPORT void WINAPI OnUnload()
{
 

}


/*!
Do not modify this code. Defines the DLL main code fragment
*/

BOOL WINAPI DllMain(HINSTANCE hDll, DWORD fdwReason, LPVOID lpvReserved)
{
	switch(fdwReason)
	{
	case DLL_PROCESS_ATTACH:
		break;
	case DLL_PROCESS_DETACH:
		break;
	case DLL_THREAD_ATTACH:
		break;
	case DLL_THREAD_DETACH:
		break;
	}

	return TRUE;
};