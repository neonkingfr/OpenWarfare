/*
 *
 * Copyright(c) 2011 Bohemia Interactive Simulations, Inc.
 * http://www.bisimulations.com
 *
 * For information about the licensing and copyright of this software please
 * contact Bohemia Interactive Simulations, Inc. at contact @ bisimulations.com.
 *
 */

#ifndef __FUSION_OSM_H__
#define __FUSION_OSM_H__

#include <windows.h>

struct IDirect3DDevice9;

struct VBSParameters
{
	const char *_dirInstall;  // VBS installation directory
	const char *_dirUser;     // user's VBS directory
	IDirect3DDevice9 *_device; // rendering D3D device; is NULL if !_VBS3_PLUGIN_DEVICE
};

#define VBSPLUGIN_EXPORT __declspec(dllexport)

// FUSION
// Functions exported by this plug-in to the FusionHandler
VBSPLUGIN_EXPORT void WINAPI RegisterCommandFnc(void *executeCommandFnc);
VBSPLUGIN_EXPORT void WINAPI OnSimulationStep(float deltaT);
VBSPLUGIN_EXPORT bool WINAPI OnWindowInput(HWND hwnd, UINT code,WPARAM wParam,LPARAM lParam);
VBSPLUGIN_EXPORT bool WINAPI OnKeyInput(const RAWKEYBOARD* keyData, DWORD keyTime);
VBSPLUGIN_EXPORT bool WINAPI OnMouseInput(const RAWMOUSE* mouseData, DWORD mouseTime);
VBSPLUGIN_EXPORT const char* WINAPI PluginFunction(const char *input);
VBSPLUGIN_EXPORT int WINAPI OnLoad(VBSParameters *params);
VBSPLUGIN_EXPORT void WINAPI OnUnload();
// FUSION

#endif
