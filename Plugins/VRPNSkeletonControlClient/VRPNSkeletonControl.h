#define VBSPLUGIN_EXPORT __declspec(dllexport)

#include "SkeletonDefinition.h"

// Command function declaration
typedef int (WINAPI * ExecuteCommandType)(const char *command, char *result, int resultLength);

VBSPLUGIN_EXPORT void WINAPI RegisterHWND(void *hwnd, void *hins);
VBSPLUGIN_EXPORT void WINAPI RegisterCommandFnc(void *executeCommandFnc);
VBSPLUGIN_EXPORT void WINAPI OnSimulationStep(float deltaT);
VBSPLUGIN_EXPORT const char* WINAPI PluginFunction(const char *input);
#ifdef VBS2NG
VBSPLUGIN_EXPORT void WINAPI OnModifyBone(Matrix4DP &mat, int skeletonIndex);
#else
VBSPLUGIN_EXPORT void WINAPI OnModifyBone(Matrix4 &mat, int skeletonIndex);
#endif