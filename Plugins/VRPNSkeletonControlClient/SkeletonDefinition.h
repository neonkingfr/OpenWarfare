#ifndef __SKEL_DEF__
#define __SKEL_DEF__

#include <vector>
#include <map>
#include <string>
using namespace std;

#include "..\..\Common\BIMatrixDP.h"
typedef BI_TMatrix4<float> Matrix4;
typedef BI_TMatrix4DP<float> Matrix4DP; // double-precision matrix
typedef BI_TVector3<float> Vector3;

#define VForward Vector3(0,0,1)
#define VUp Vector3(0,1,0)

#ifdef VBS2NG
static const unsigned NUM_BONES = 145;
typedef enum
{
  ePELVIS = 0,
  eCAMERA,
  eSPINE,
  eSPINE1,
  eWEAPON,
  eLAUNCHER,
  eSPINE2,
  eSPINE3,
  eNECK,
  eNECK1,
  eHEAD,
  eLBROW,
  eMBROW,
  eRBROW,
  eLMOUTH,
  eMMOUTH,
  eRMOUTH,
  eEYELIDS,
  eLLIP,
  eLEFT_SHOULDER = 84,
  eLEFT_ARM,
  eLEFT_ARM_ROLL,
  eLEFT_FOREARM,
  eLEFT_FOREARM_ROLL,
  eLEFT_HAND,
  eLEFT_HAND_RING,
  eLEFT_HAND_RING1,
  eLEFT_HAND_RING2,
  eLEFT_HAND_RING3,
  eLEFT_HAND_PINKY1,
  eLEFT_HAND_PINKY2,
  eLEFT_HAND_PINKY3,
  eLEFT_HAND_MIDDLE1,
  eLEFT_HAND_MIDDLE2,
  eLEFT_HAND_MIDDLE3,
  eLEFT_HAND_INDEX1,
  eLEFT_HAND_INDEX2,
  eLEFT_HAND_INDEX3,
  eLEFT_HAND_THUMB1,
  eLEFT_HAND_THUMB2,
  eLEFT_HAND_THUMB3,
  eRIGHT_SHOULDER,     
  eRIGHT_ARM,
  eRIGHT_ARM_ROLL,
  eRIGHT_FOREARM,
  eRIGHT_FOREARM_ROLL,
  eRIGHT_HAND,
  eRIGHT_HAND_RING,
  eRIGHT_HAND_RING1,
  eRIGHT_HAND_RING2,
  eRIGHT_HAND_RING3,
  eRIGHT_HAND_PINKY1,
  eRIGHT_HAND_PINKY2,
  eRIGHT_HAND_PINKY3,
  eRIGHT_HAND_MIDDLE1,
  eRIGHT_HAND_MIDDLE2,
  eRIGHT_HAND_MIDDLE3,
  eRIGHT_HAND_INDEX1,
  eRIGHT_HAND_INDEX2,
  eRIGHT_HAND_INDEX3,
  eRIGHT_HAND_THUMB1,
  eRIGHT_HAND_THUMB2,
  eRIGHT_HAND_THUMB3,
  eLEFT_UP_LEG,        
  eLEFT_UP_LEG_ROLL,
  eLEFT_LEG,
  eLEFT_LEG_ROLL,
  eLEFT_FOOT,
  eLEFT_TOE_BASE,
  eRIGHT_UP_LEG,       
  eRIGHT_UP_LEG_ROLL,
  eRIGHT_LEG,
  eRIGHT_LEG_ROLL,
  eRIGHT_FOOT,
  eRIGHT_TOE_BASE,
  eFP_CAMERA
} EBones;
#else
static const unsigned NUM_BONES = 76;
typedef enum
{
  // UPPER BODY 
  eNECK     = 0,
  eNECK1    = 1,     
  eHEAD     = 2,
  eLBROW    = 3,//unused
  eMBROW    = 4,//unused
  eRBROW    = 5,//unused
  eLMOUTH   = 6,//unused
  eMMOUTH   = 7,//unused
  eRMOUTH   = 8,//unused
  eEYELIDS  = 9,//unused
  eLLIP     = 10,//unused
  eWEAPON   = 11,
  eLAUNCHER = 12,//unused
  eCAMERA   = 13,//unused
  eSPINE    = 14,
  eSPINE1   = 15,
  eSPINE2   = 16,
  eSPINE3   = 17,
  ePELVIS   = 18,
  // LEFT ARM
  eLEFT_SHOULDER      = 19,      
  eLEFT_ARM           = 20,
  eLEFT_ARM_ROLL      = 21,
  eLEFT_FOREARM       = 22,
  eLEFT_FOREARM_ROLL  = 23,
  eLEFT_HAND          = 24,
  eLEFT_HAND_RING     = 25,
  eLEFT_HAND_RING1    = 26,
  eLEFT_HAND_RING2    = 27,
  eLEFT_HAND_RING3    = 28,
  eLEFT_HAND_PINKY1   = 29,
  eLEFT_HAND_PINKY2   = 30,
  eLEFT_HAND_PINKY3   = 31,
  eLEFT_HAND_MIDDLE1  = 32,
  eLEFT_HAND_MIDDLE2  = 33,
  eLEFT_HAND_MIDDLE3  = 34,
  eLEFT_HAND_INDEX1   = 35,
  eLEFT_HAND_INDEX2   = 36,
  eLEFT_HAND_INDEX3   = 37,
  eLEFT_HAND_THUMB1   = 38,
  eLEFT_HAND_THUMB2   = 39,
  eLEFT_HAND_THUMB3   = 40,
  // RIGHT ARM
  eRIGHT_SHOULDER       = 41,     
  eRIGHT_ARM            = 42,
  eRIGHT_ARM_ROLL       = 43,
  eRIGHT_FOREARM        = 44,
  eRIGHT_FOREARM_ROLL   = 45,
  eRIGHT_HAND           = 46,
  eRIGHT_HAND_RING      = 47,
  eRIGHT_HAND_RING1     = 48,
  eRIGHT_HAND_RING2     = 49,
  eRIGHT_HAND_RING3     = 50,
  eRIGHT_HAND_PINKY1    = 51,
  eRIGHT_HAND_PINKY2    = 52,
  eRIGHT_HAND_PINKY3    = 53,
  eRIGHT_HAND_MIDDLE1   = 54,
  eRIGHT_HAND_MIDDLE2   = 55,
  eRIGHT_HAND_MIDDLE3   = 56,
  eRIGHT_HAND_INDEX1    = 57,
  eRIGHT_HAND_INDEX2    = 58,
  eRIGHT_HAND_INDEX3    = 59,
  eRIGHT_HAND_THUMB1    = 60,
  eRIGHT_HAND_THUMB2    = 61,
  eRIGHT_HAND_THUMB3    = 62,
  // LEFT LEG
  eLEFT_UP_LEG      = 63,        
  eLEFT_UP_LEG_ROLL = 64,
  eLEFT_LEG         = 65,
  eLEFT_LEG_ROLL    = 66,
  eLEFT_FOOT        = 67,
  eLEFT_TOE_BASE    = 68,
  // RIGHT LEG
  eRIGHT_UP_LEG       = 69,       
  eRIGHT_UP_LEG_ROLL  = 70,
  eRIGHT_LEG          = 71,
  eRIGHT_LEG_ROLL     = 72,
  eRIGHT_FOOT         = 73,
  eRIGHT_TOE_BASE     = 74,
  eFP_CAMERA          = 75
} EBones;
#endif // VBS2NG

// indices of parent transformations of uncontrolled bones
typedef enum
{
  eLBROW_PARENT               = eHEAD,
  eMBROW_PARENT               = eHEAD,
  eRBROW_PARENT               = eHEAD,
  eLMOUTH_PARENT              = eHEAD,
  eMMOUTH_PARENT              = eHEAD,
  eRMOUTH_PARENT              = eHEAD,
  eEYELIDS_PARENT             = eHEAD,
  eLLIP_PARENT                = eHEAD,
  eCAMERA_PARENT              = eHEAD,
} EParentBones;

//////////////////////////////////////////////////////////////////////////
// Skeleton definition
// 
typedef vector<Matrix4> BonesArray;
typedef map<string, int> BonesNameIndexMap;


class CSkeletonDefinition
{
public:

  BonesArray Bones;             // bones array
  BonesNameIndexMap BonesMap;   // map bone names to indices 

  CSkeletonDefinition() { DefineSkeleton(); }

  void DefineSkeleton()
  {
    // bones array
    Bones.resize(NUM_BONES);

    // map bone-names to indices (used for config-tracker mapping)

    // UPPER BODY
    BonesMap.insert(pair<string, int>("NECK",               eNECK));
    BonesMap.insert(pair<string, int>("NECK1",              eNECK1));
    BonesMap.insert(pair<string, int>("HEAD",               eHEAD));
    //BonesMap.insert(pair<string, int>("LBROW",              eLBROW));
    //BonesMap.insert(pair<string, int>("MBROW",              eMBROW));
    //BonesMap.insert(pair<string, int>("RBROW",              eRBROW));
    //BonesMap.insert(pair<string, int>("LMOUTH",             eLMOUTH));
    //BonesMap.insert(pair<string, int>("MMOUTH",             eMMOUTH));
    //BonesMap.insert(pair<string, int>("RMOUTH",             eRMOUTH));
    //BonesMap.insert(pair<string, int>("EYELIDS",            eEYELIDS));
    //BonesMap.insert(pair<string, int>("LLIP",               eLLIP));
    BonesMap.insert(pair<string, int>("WEAPON",             eWEAPON));
    //BonesMap.insert(pair<string, int>("LAUNCHER",           eLAUNCHER));
    //BonesMap.insert(pair<string, int>("CAMERA",             eCAMERA));
    BonesMap.insert(pair<string, int>("SPINE",              eSPINE));
    BonesMap.insert(pair<string, int>("SPINE1",             eSPINE1));
    BonesMap.insert(pair<string, int>("SPINE2",             eSPINE2));
    BonesMap.insert(pair<string, int>("SPINE3",             eSPINE3));
    BonesMap.insert(pair<string, int>("PELVIS",             ePELVIS));

    // LEFT ARM
    BonesMap.insert(pair<string, int>("LEFT_SHOULDER",      eLEFT_SHOULDER));
    BonesMap.insert(pair<string, int>("LEFT_ARM",           eLEFT_ARM));
    BonesMap.insert(pair<string, int>("LEFT_ARM_ROLL",      eLEFT_ARM_ROLL));
    BonesMap.insert(pair<string, int>("LEFT_FOREARM",       eLEFT_FOREARM));
    BonesMap.insert(pair<string, int>("LEFT_FOREARM_ROLL",  eLEFT_FOREARM_ROLL));
    BonesMap.insert(pair<string, int>("LEFT_HAND",          eLEFT_HAND));
    BonesMap.insert(pair<string, int>("LEFT_HAND_RING",     eLEFT_HAND_RING));
    BonesMap.insert(pair<string, int>("LEFT_HAND_RING1",    eLEFT_HAND_RING1));
    BonesMap.insert(pair<string, int>("LEFT_HAND_RING2",    eLEFT_HAND_RING2));
    BonesMap.insert(pair<string, int>("LEFT_HAND_RING3",    eLEFT_HAND_RING3));
    BonesMap.insert(pair<string, int>("LEFT_HAND_PINKY1",   eLEFT_HAND_PINKY1));
    BonesMap.insert(pair<string, int>("LEFT_HAND_PINKY2",   eLEFT_HAND_PINKY2));
    BonesMap.insert(pair<string, int>("LEFT_HAND_PINKY3",   eLEFT_HAND_PINKY3));
    BonesMap.insert(pair<string, int>("LEFT_HAND_MIDDLE1",  eLEFT_HAND_MIDDLE1));
    BonesMap.insert(pair<string, int>("LEFT_HAND_MIDDLE2",  eLEFT_HAND_MIDDLE2));
    BonesMap.insert(pair<string, int>("LEFT_HAND_MIDDLE3",  eLEFT_HAND_MIDDLE3));
    BonesMap.insert(pair<string, int>("LEFT_HAND_INDEX1",   eLEFT_HAND_INDEX1));
    BonesMap.insert(pair<string, int>("LEFT_HAND_INDEX2",   eLEFT_HAND_INDEX2));
    BonesMap.insert(pair<string, int>("LEFT_HAND_INDEX3",   eLEFT_HAND_INDEX3));
    BonesMap.insert(pair<string, int>("LEFT_HAND_THUMB1",   eLEFT_HAND_THUMB1));
    BonesMap.insert(pair<string, int>("LEFT_HAND_THUMB2",   eLEFT_HAND_THUMB2));
    BonesMap.insert(pair<string, int>("LEFT_HAND_THUMB3",   eLEFT_HAND_THUMB3));

    // RIGHT ARM
    BonesMap.insert(pair<string, int>("RIGHT_SHOULDER",     eRIGHT_SHOULDER));
    BonesMap.insert(pair<string, int>("RIGHT_ARM",          eRIGHT_ARM));
    BonesMap.insert(pair<string, int>("RIGHT_ARM_ROLL",     eRIGHT_ARM_ROLL));
    BonesMap.insert(pair<string, int>("RIGHT_FOREARM",      eRIGHT_FOREARM));
    BonesMap.insert(pair<string, int>("RIGHT_FOREARM_ROLL", eRIGHT_FOREARM_ROLL));
    BonesMap.insert(pair<string, int>("RIGHT_HAND",         eRIGHT_HAND));
    BonesMap.insert(pair<string, int>("RIGHT_HAND_RING",    eRIGHT_HAND_RING));
    BonesMap.insert(pair<string, int>("RIGHT_HAND_RING1",   eRIGHT_HAND_RING1));
    BonesMap.insert(pair<string, int>("RIGHT_HAND_RING2",   eRIGHT_HAND_RING2));
    BonesMap.insert(pair<string, int>("RIGHT_HAND_RING3",   eRIGHT_HAND_RING3));
    BonesMap.insert(pair<string, int>("RIGHT_HAND_PINKY1",  eRIGHT_HAND_PINKY1));
    BonesMap.insert(pair<string, int>("RIGHT_HAND_PINKY2",  eRIGHT_HAND_PINKY2));
    BonesMap.insert(pair<string, int>("RIGHT_HAND_PINKY3",  eRIGHT_HAND_PINKY3));
    BonesMap.insert(pair<string, int>("RIGHT_HAND_MIDDLE1", eRIGHT_HAND_MIDDLE1));
    BonesMap.insert(pair<string, int>("RIGHT_HAND_MIDDLE2", eRIGHT_HAND_MIDDLE2));
    BonesMap.insert(pair<string, int>("RIGHT_HAND_MIDDLE3", eRIGHT_HAND_MIDDLE3));
    BonesMap.insert(pair<string, int>("RIGHT_HAND_INDEX1",  eRIGHT_HAND_INDEX1));
    BonesMap.insert(pair<string, int>("RIGHT_HAND_INDEX2",  eRIGHT_HAND_INDEX2));
    BonesMap.insert(pair<string, int>("RIGHT_HAND_INDEX3",  eRIGHT_HAND_INDEX3));
    BonesMap.insert(pair<string, int>("RIGHT_HAND_THUMB1",  eRIGHT_HAND_THUMB1));
    BonesMap.insert(pair<string, int>("RIGHT_HAND_THUMB2",  eRIGHT_HAND_THUMB2));
    BonesMap.insert(pair<string, int>("RIGHT_HAND_THUMB3",  eRIGHT_HAND_THUMB3));

    // LEFT LEG
    BonesMap.insert(pair<string, int>("LEFT_UP_LEG",        eLEFT_UP_LEG));
    BonesMap.insert(pair<string, int>("LEFT_UP_LEG_ROLL",   eLEFT_UP_LEG_ROLL));
    BonesMap.insert(pair<string, int>("LEFT_LEG",           eLEFT_LEG));
    BonesMap.insert(pair<string, int>("LEFT_LEG_ROLL",      eLEFT_LEG_ROLL));
    BonesMap.insert(pair<string, int>("LEFT_FOOT",          eLEFT_FOOT));
    BonesMap.insert(pair<string, int>("LEFT_TOE_BASE",      eLEFT_TOE_BASE));

    // RIGHT LEG
    BonesMap.insert(pair<string, int>("RIGHT_UP_LEG",       eRIGHT_UP_LEG));
    BonesMap.insert(pair<string, int>("RIGHT_UP_LEG_ROLL",  eRIGHT_UP_LEG_ROLL));
    BonesMap.insert(pair<string, int>("RIGHT_LEG",          eRIGHT_LEG));
    BonesMap.insert(pair<string, int>("RIGHT_LEG_ROLL",     eRIGHT_LEG_ROLL));
    BonesMap.insert(pair<string, int>("RIGHT_FOOT",         eRIGHT_FOOT));
    BonesMap.insert(pair<string, int>("RIGHT_TOE_BASE",     eRIGHT_TOE_BASE));

    // FIRST-PERSON CAMERA
    BonesMap.insert(pair<string, int>("FP_CAMERA", eFP_CAMERA));

    // INIT SKELETON

    // Init root
    Bones[ePELVIS] = BI_IdentityMatrix4;
    //Bones[ePELVIS].SetTranslation(Vector3(0.0f, 1.014f, 0.0f));
    Bones[ePELVIS].SetTranslation(Vector3(0.0f, 0.0f, 0.0f));

    // Offsets (parent relative)
    Vector3 offsetLUpperLeg     (-0.114f, -0.060f, +0.000f); // Left leg
    Vector3 offsetLUpperLegRoll (-0.013f, -0.203f, +0.009f);
    Vector3 offsetLLowerLeg     (-0.013f, -0.204f, +0.010f);
    Vector3 offsetLLowerLegRoll (-0.014f, -0.194f, -0.047f);
    Vector3 offsetLFoot         (-0.016f, -0.227f, -0.055f);
    Vector3 offsetLToes         (-0.004f, -0.108f, +0.113f);
    Vector3 offsetLToesEnd      (-0.000f, -0.017f, +0.125f);

    Vector3 offsetRUpperLeg     (+0.114f, -0.060f, +0.000f);  // Right leg
    Vector3 offsetRUpperLegRoll (+0.013f, -0.203f, +0.009f);
    Vector3 offsetRLowerLeg     (+0.013f, -0.204f, +0.010f);
    Vector3 offsetRLowerLegRoll (+0.014f, -0.194f, -0.047f);
    Vector3 offsetRFoot         (+0.016f, -0.227f, -0.055f);
    Vector3 offsetRToes         (+0.004f, -0.108f, +0.113f);
    Vector3 offsetRToesEnd      (+0.000f, -0.017f, +0.125f);

    Vector3 offsetSpine1     (+0.000f, +0.000f, +0.000f); // Upper body - spine
    Vector3 offsetSpine2     (+0.002f, +0.156f, -0.012f);
    Vector3 offsetSpine3     (-0.002f, +0.130f, -0.015f);
    Vector3 offsetSpine4     (+0.000f, +0.152f, -0.006f);
    Vector3 offsetNeck1      (+0.000f, +0.117f, +0.008f);
    Vector3 offsetNeck2      (+0.000f, +0.071f, +0.017f);
    Vector3 offsetSkull      (+0.000f, +0.041f, +0.002f);
    Vector3 offsetSkullEnd   (+0.000f, +0.147f, +0.001f);
    Vector3 offsetLeftEye    (-0.040f, +0.020f, +0.050f);
    Vector3 offsetRightEye   (+0.040f, +0.020f, +0.050f);

    Vector3 offsetLClavicle     (-0.064f, +0.062f, +0.028f); // Upper body - left arm
    Vector3 offsetLUpperArm     (-0.139f, -0.023f, -0.045f);
    Vector3 offsetLUpperArmRoll (-0.078f, -0.087f, -0.009f);
    Vector3 offsetLLowerArm     (-0.097f, -0.107f, -0.010f);
    Vector3 offsetLLowerArmRoll (-0.096f, -0.103f, -0.001f);
    Vector3 offsetLHand         (-0.113f, -0.114f, -0.004f);
    Vector3 offsetLThumb1       (-0.003f, -0.004f, +0.027f);
    Vector3 offsetLThumb2       (-0.023f, -0.025f, +0.039f);
    Vector3 offsetLThumb3       (-0.020f, -0.023f, +0.017f);
    Vector3 offsetLThumbEnd     (-0.013f, -0.015f, +0.016f);
    Vector3 offsetLIndex1       (-0.064f, -0.061f, +0.036f);
    Vector3 offsetLIndex2       (-0.028f, -0.027f, +0.010f);
    Vector3 offsetLIndex3       (-0.016f, -0.016f, +0.006f);
    Vector3 offsetLIndexEnd     (-0.015f, -0.015f, +0.005f);
    Vector3 offsetLMiddle1      (-0.068f, -0.066f, +0.013f);
    Vector3 offsetLMiddle2      (-0.030f, -0.031f, +0.002f);
    Vector3 offsetLMiddle3      (-0.018f, -0.019f, +0.001f);
    Vector3 offsetLMiddleEnd    (-0.016f, -0.016f, +0.001f);
    Vector3 offsetLRing         (-0.046f, -0.047f, -0.005f);
    Vector3 offsetLRing1        (-0.064f, -0.065f, -0.008f);
    Vector3 offsetLRing2        (-0.025f, -0.024f, -0.008f);
    Vector3 offsetLRing3        (-0.017f, -0.017f, -0.005f);
    Vector3 offsetLRingEnd      (-0.015f, -0.014f, -0.004f);
    Vector3 offsetLPinky1       (-0.008f, -0.015f, -0.027f);
    Vector3 offsetLPinky2       (-0.019f, -0.018f, -0.006f);
    Vector3 offsetLPinky3       (-0.015f, -0.013f, -0.005f);
    Vector3 offsetLPinkyEnd     (-0.013f, -0.012f, -0.004f);

    Vector3 offsetRClavicle     (+0.064f, +0.062f, +0.028f); // Upper body - right arm
    Vector3 offsetRUpperArm     (+0.139f, -0.023f, -0.045f);
    Vector3 offsetRUpperArmRoll (+0.078f, -0.087f, -0.009f);
    Vector3 offsetRLowerArm     (+0.097f, -0.107f, -0.010f);
    Vector3 offsetRLowerArmRoll (+0.096f, -0.103f, -0.001f);
    Vector3 offsetRHand         (+0.113f, -0.114f, -0.004f);
    Vector3 offsetRThumb1       (+0.003f, -0.004f, +0.027f);
    Vector3 offsetRThumb2       (+0.023f, -0.025f, +0.039f);
    Vector3 offsetRThumb3       (+0.020f, -0.023f, +0.017f);
    Vector3 offsetRThumbEnd     (+0.013f, -0.015f, +0.016f);
    Vector3 offsetRIndex1       (+0.064f, -0.061f, +0.036f);
    Vector3 offsetRIndex2       (+0.028f, -0.027f, +0.010f);
    Vector3 offsetRIndex3       (+0.016f, -0.016f, +0.006f);
    Vector3 offsetRIndexEnd     (+0.015f, -0.015f, +0.005f);
    Vector3 offsetRMiddle1      (+0.068f, -0.066f, +0.013f);
    Vector3 offsetRMiddle2      (+0.030f, -0.031f, +0.002f);
    Vector3 offsetRMiddle3      (+0.018f, -0.019f, +0.001f);
    Vector3 offsetRMiddleEnd    (+0.016f, -0.016f, +0.001f);
    Vector3 offsetRRing         (+0.046f, -0.047f, -0.005f);
    Vector3 offsetRRing1        (+0.064f, -0.065f, -0.008f);
    Vector3 offsetRRing2        (+0.025f, -0.024f, -0.008f);
    Vector3 offsetRRing3        (+0.017f, -0.017f, -0.005f);
    Vector3 offsetRRingEnd      (+0.015f, -0.014f, -0.004f);
    Vector3 offsetRPinky1       (+0.008f, -0.015f, -0.027f);
    Vector3 offsetRPinky2       (+0.019f, -0.018f, -0.006f);
    Vector3 offsetRPinky3       (+0.015f, -0.013f, -0.005f);
    Vector3 offsetRPinkyEnd     (+0.013f, -0.012f, -0.004f);

    // Absolute positions
    Vector3 poseLUpperLeg     = Bones[ePELVIS].GetPos()+ offsetLUpperLeg; // Left leg
    Vector3 poseLUpperLegRoll = poseLUpperLeg         + offsetLUpperLegRoll;
    Vector3 poseLLowerLeg     = poseLUpperLegRoll     + offsetLLowerLeg;
    Vector3 poseLLowerLegRoll = poseLLowerLeg         + offsetLLowerLegRoll;
    Vector3 poseLFoot         = poseLLowerLegRoll     + offsetLFoot;
    Vector3 poseLToes         = poseLFoot             + offsetLToes;
    Vector3 poseLToesEnd      = poseLToes             + offsetLToesEnd;

    Vector3 poseRUpperLeg     = Bones[ePELVIS].GetPos()+ offsetRUpperLeg; // Right leg
    Vector3 poseRUpperLegRoll = poseRUpperLeg         + offsetRUpperLegRoll;
    Vector3 poseRLowerLeg     = poseRUpperLegRoll     + offsetRLowerLeg;
    Vector3 poseRLowerLegRoll = poseRLowerLeg         + offsetRLowerLegRoll;
    Vector3 poseRFoot         = poseRLowerLegRoll     + offsetRFoot;
    Vector3 poseRToes         = poseRFoot             + offsetRToes;
    Vector3 poseRToesEnd      = poseRToes             + offsetRToesEnd;

    Vector3 poseSpine1        = Bones[ePELVIS].GetPos()+ offsetSpine1; // Upper body - spin
    Vector3 poseSpine2        = poseSpine1            + offsetSpine2;
    Vector3 poseSpine3        = poseSpine2            + offsetSpine3;
    Vector3 poseSpine4        = poseSpine3            + offsetSpine4;
    Vector3 poseNeck1         = poseSpine4            + offsetNeck1;
    Vector3 poseNeck2         = poseNeck1             + offsetNeck2;
    Vector3 poseSkull         = poseNeck2             + offsetSkull;
    Vector3 poseSkullEnd      = poseSkull             + offsetSkullEnd;
    Vector3 poseLeftEye       = poseSkull             + offsetLeftEye;
    Vector3 poseRightEye      = poseSkull             + offsetRightEye;

    Vector3 poseLClavicle     = poseSpine4            + offsetLClavicle; // Upper body - left arm
    Vector3 poseLUpperArm     = poseLClavicle         + offsetLUpperArm;
    Vector3 poseLUpperArmRoll = poseLUpperArm         + offsetLUpperArmRoll;
    Vector3 poseLLowerArm     = poseLUpperArmRoll     + offsetLLowerArm;
    Vector3 poseLLowerArmRoll = poseLLowerArm         + offsetLLowerArmRoll;
    Vector3 poseLHand         = poseLLowerArmRoll     + offsetLHand;
    Vector3 poseLThumb1       = poseLHand             + offsetLThumb1;
    Vector3 poseLThumb2       = poseLThumb1           + offsetLThumb2;
    Vector3 poseLThumb3       = poseLThumb2           + offsetLThumb3;
    Vector3 poseLThumbEnd     = poseLThumb3           + offsetLThumbEnd;
    Vector3 poseLIndex1       = poseLHand             + offsetLIndex1;
    Vector3 poseLIndex2       = poseLIndex1           + offsetLIndex2;
    Vector3 poseLIndex3       = poseLIndex2           + offsetLIndex3;
    Vector3 poseLIndexEnd     = poseLIndex3           + offsetLIndexEnd;
    Vector3 poseLMiddle1      = poseLHand             + offsetLMiddle1;
    Vector3 poseLMiddle2      = poseLMiddle1          + offsetLMiddle2;
    Vector3 poseLMiddle3      = poseLMiddle2          + offsetLMiddle3;
    Vector3 poseLMiddleEnd    = poseLMiddle3          + offsetLMiddleEnd;
    Vector3 poseLRing         = poseLHand             + offsetLRing;
    Vector3 poseLRing1        = poseLHand             + offsetLRing1;
    Vector3 poseLRing2        = poseLRing1            + offsetLRing2;
    Vector3 poseLRing3        = poseLRing2            + offsetLRing3;
    Vector3 poseLRingEnd      = poseLRing3            + offsetLRingEnd;
    Vector3 poseLPinky1       = poseLRing             + offsetLPinky1;
    Vector3 poseLPinky2       = poseLPinky1           + offsetLPinky2;
    Vector3 poseLPinky3       = poseLPinky2           + offsetLPinky3;
    Vector3 poseLPinkyEnd     = poseLPinky3           + offsetLPinkyEnd;

    Vector3 poseRClavicle     = poseSpine4            + offsetRClavicle; // Upper body - right arm
    Vector3 poseRUpperArm     = poseRClavicle         + offsetRUpperArm;
    Vector3 poseRUpperArmRoll = poseRUpperArm         + offsetRUpperArmRoll;
    Vector3 poseRLowerArm     = poseRUpperArmRoll     + offsetRLowerArm;
    Vector3 poseRLowerArmRoll = poseRLowerArm         + offsetRLowerArmRoll;
    Vector3 poseRHand         = poseRLowerArmRoll     + offsetRHand;
    Vector3 poseRThumb1       = poseRHand             + offsetRThumb1;
    Vector3 poseRThumb2       = poseRThumb1           + offsetRThumb2;
    Vector3 poseRThumb3       = poseRThumb2           + offsetRThumb3;
    Vector3 poseRThumbEnd     = poseRThumb3           + offsetRThumbEnd;
    Vector3 poseRIndex1       = poseRHand             + offsetRIndex1;
    Vector3 poseRIndex2       = poseRIndex1           + offsetRIndex2;
    Vector3 poseRIndex3       = poseRIndex2           + offsetRIndex3;
    Vector3 poseRIndexEnd     = poseRIndex3           + offsetRIndexEnd;
    Vector3 poseRMiddle1      = poseRHand             + offsetRMiddle1;
    Vector3 poseRMiddle2      = poseRMiddle1          + offsetRMiddle2;
    Vector3 poseRMiddle3      = poseRMiddle2          + offsetRMiddle3;
    Vector3 poseRMiddleEnd    = poseRMiddle3          + offsetRMiddleEnd;
    Vector3 poseRRing         = poseRHand             + offsetRRing;
    Vector3 poseRRing1        = poseRHand             + offsetRRing1;
    Vector3 poseRRing2        = poseRRing1            + offsetRRing2;
    Vector3 poseRRing3        = poseRRing2            + offsetRRing3;
    Vector3 poseRRingEnd      = poseRRing3            + offsetRRingEnd;
    Vector3 poseRPinky1       = poseRRing             + offsetRPinky1;
    Vector3 poseRPinky2       = poseRPinky1           + offsetRPinky2;
    Vector3 poseRPinky3       = poseRPinky2           + offsetRPinky3;
    Vector3 poseRPinkyEnd     = poseRPinky3           + offsetRPinkyEnd;

    // Absolute binding pose

    //LEFT LEG
    Bones[eLEFT_UP_LEG].SetTranslation(     poseLUpperLeg);       Bones[eLEFT_UP_LEG].SetDirectionAndUp(      (poseLUpperLegRoll  - poseLUpperLeg),     -VForward);
    Bones[eLEFT_UP_LEG_ROLL].SetTranslation(poseLUpperLegRoll);   Bones[eLEFT_UP_LEG_ROLL].SetDirectionAndUp( (poseLLowerLeg      - poseLUpperLegRoll), -VForward);
    Bones[eLEFT_LEG].SetTranslation(        poseLLowerLeg);       Bones[eLEFT_LEG].SetDirectionAndUp(         (poseLLowerLegRoll  - poseLLowerLeg),     -VForward);
    Bones[eLEFT_LEG_ROLL].SetTranslation(   poseLLowerLegRoll);   Bones[eLEFT_LEG_ROLL].SetDirectionAndUp(    (poseLFoot          - poseLLowerLegRoll), -VForward);
    Bones[eLEFT_FOOT].SetTranslation(       poseLFoot);           Bones[eLEFT_FOOT].SetDirectionAndUp(        (poseLToes          - poseLFoot),         -VUp);
    Bones[eLEFT_TOE_BASE].SetTranslation(   poseLToes);           Bones[eLEFT_TOE_BASE].SetDirectionAndUp(    (poseLToesEnd       - poseLToes),         -VUp);

    //RIGHT LEG
    Bones[eRIGHT_UP_LEG].SetTranslation(     poseRUpperLeg);      Bones[eRIGHT_UP_LEG].SetDirectionAndUp(      (poseRUpperLegRoll - poseRUpperLeg),     -VForward);
    Bones[eRIGHT_UP_LEG_ROLL].SetTranslation(poseRUpperLegRoll);  Bones[eRIGHT_UP_LEG_ROLL].SetDirectionAndUp( (poseRLowerLeg     - poseRUpperLegRoll), -VForward);
    Bones[eRIGHT_LEG].SetTranslation(        poseRLowerLeg);      Bones[eRIGHT_LEG].SetDirectionAndUp(         (poseRLowerLegRoll - poseRLowerLeg),     -VForward);
    Bones[eRIGHT_LEG_ROLL].SetTranslation(   poseRLowerLegRoll);  Bones[eRIGHT_LEG_ROLL].SetDirectionAndUp(    (poseRFoot         - poseRLowerLegRoll), -VForward);
    Bones[eRIGHT_FOOT].SetTranslation(       poseRFoot);          Bones[eRIGHT_FOOT].SetDirectionAndUp(        (poseRToes         - poseRFoot),         -VUp);
    Bones[eRIGHT_TOE_BASE].SetTranslation(   poseRToes);          Bones[eRIGHT_TOE_BASE].SetDirectionAndUp(    (poseRToesEnd      - poseRToes),         -VUp);

    // UPPER BODY
    Bones[eSPINE].SetTranslation(   poseSpine1);    Bones[eSPINE].SetDirectionAndUp(  (poseSpine2   - poseSpine1),   VForward);
    Bones[eSPINE1].SetTranslation(  poseSpine2);    Bones[eSPINE1].SetDirectionAndUp( (poseSpine3   - poseSpine2),   VForward);
    Bones[eSPINE2].SetTranslation(  poseSpine3);    Bones[eSPINE2].SetDirectionAndUp( (poseSpine4   - poseSpine3),   VForward);
    Bones[eSPINE3].SetTranslation(  poseSpine4);    Bones[eSPINE3].SetDirectionAndUp( (poseNeck1    - poseSpine4),   VForward);
    Bones[eNECK].SetTranslation(    poseNeck1);     Bones[eNECK].SetDirectionAndUp(   (poseNeck2    - poseNeck1),    VForward);
    Bones[eNECK1].SetTranslation(   poseNeck2);     Bones[eNECK1].SetDirectionAndUp(  (poseSkull    - poseNeck2),    VForward);
    Bones[eHEAD].SetTranslation(    poseSkull);     Bones[eHEAD].SetDirectionAndUp(   (poseSkullEnd - poseSkull),    VForward);
    //Bones[eLBROW].SetTranslation(   poseSkull);     Bones[eLBROW].SetDirectionAndUp(  (poseSkullEnd - poseSkull),  VForward);
    //Bones[eMBROW].SetTranslation(   poseSkull);     Bones[eMBROW].SetDirectionAndUp(  (poseSkullEnd - poseSkull),  VForward);
    //Bones[eRBROW].SetTranslation(   poseSkull);     Bones[eRBROW].SetDirectionAndUp(  (poseSkullEnd - poseSkull),  VForward);
    //Bones[eLMOUTH].SetTranslation(  poseSkull);     Bones[eLMOUTH].SetDirectionAndUp( (poseSkullEnd - poseSkull),  VForward);
    //Bones[eMMOUTH].SetTranslation(  poseSkull);     Bones[eMMOUTH].SetDirectionAndUp( (poseSkullEnd - poseSkull),  VForward);
    //Bones[eRMOUTH].SetTranslation(  poseSkull);     Bones[eRMOUTH].SetDirectionAndUp( (poseSkullEnd - poseSkull),  VForward);
    //Bones[eEYELIDS].SetTranslation( poseSkull);     Bones[eEYELIDS].SetDirectionAndUp((poseSkullEnd - poseSkull),  VForward);
    //Bones[eLLIP].SetTranslation(    poseSkull);     Bones[eLLIP].SetDirectionAndUp(   (poseSkullEnd - poseSkull),  VForward);
    Bones[eWEAPON] = BI_IdentityMatrix4;
    Bones[eLAUNCHER] = BI_IdentityMatrix4;
    Bones[eCAMERA] = BI_IdentityMatrix4;

    // LEFT ARM
    Bones[eLEFT_SHOULDER].SetTranslation(       poseLClavicle);     Bones[eLEFT_SHOULDER].SetDirectionAndUp(    (poseLUpperArm      - poseLClavicle),     -VUp);
    Bones[eLEFT_ARM].SetTranslation(            poseLUpperArm);     Bones[eLEFT_ARM].SetDirectionAndUp(         (poseLUpperArmRoll  - poseLUpperArm),     -VUp);  
    Bones[eLEFT_ARM_ROLL].SetTranslation(       poseLUpperArmRoll); Bones[eLEFT_ARM_ROLL].SetDirectionAndUp(    (poseLLowerArm      - poseLUpperArmRoll), -VUp);
    Bones[eLEFT_FOREARM].SetTranslation(        poseLLowerArm);     Bones[eLEFT_FOREARM].SetDirectionAndUp(     (poseLLowerArmRoll  - poseLLowerArm),     -VUp);
    Bones[eLEFT_FOREARM_ROLL].SetTranslation(   poseLLowerArmRoll); Bones[eLEFT_FOREARM_ROLL].SetDirectionAndUp((poseLHand          - poseLLowerArmRoll), -VUp);
    Bones[eLEFT_HAND].SetTranslation(           poseLHand);         Bones[eLEFT_HAND].SetDirectionAndUp(        (poseLMiddle1       - poseLHand),         -VUp);
    Bones[eLEFT_HAND_THUMB1].SetTranslation(    poseLThumb1);       Bones[eLEFT_HAND_THUMB1].SetDirectionAndUp( (poseLThumb2        - poseLThumb1),       -VUp);
    Bones[eLEFT_HAND_THUMB2].SetTranslation(    poseLThumb2);       Bones[eLEFT_HAND_THUMB2].SetDirectionAndUp( (poseLThumb3      - poseLThumb2),         -VUp);
    Bones[eLEFT_HAND_THUMB3].SetTranslation(    poseLThumb3);       Bones[eLEFT_HAND_THUMB3].SetDirectionAndUp( (poseLThumbEnd    - poseLThumb3),         -VUp);
    Bones[eLEFT_HAND_INDEX1].SetTranslation(    poseLIndex1);       Bones[eLEFT_HAND_INDEX1].SetDirectionAndUp( (poseLIndex2      - poseLIndex1),         -VUp);
    Bones[eLEFT_HAND_INDEX2].SetTranslation(    poseLIndex2);       Bones[eLEFT_HAND_INDEX2].SetDirectionAndUp( (poseLIndex3      - poseLIndex2),         -VUp);
    Bones[eLEFT_HAND_INDEX3].SetTranslation(    poseLIndex3);       Bones[eLEFT_HAND_INDEX3].SetDirectionAndUp( (poseLIndexEnd    - poseLIndex3),         -VUp);
    Bones[eLEFT_HAND_MIDDLE1].SetTranslation(   poseLMiddle1);      Bones[eLEFT_HAND_MIDDLE1].SetDirectionAndUp((poseLMiddle2     - poseLMiddle1),        -VUp);
    Bones[eLEFT_HAND_MIDDLE2].SetTranslation(   poseLMiddle2);      Bones[eLEFT_HAND_MIDDLE2].SetDirectionAndUp((poseLMiddle3     - poseLMiddle2),        -VUp);
    Bones[eLEFT_HAND_MIDDLE3].SetTranslation(   poseLMiddle3);      Bones[eLEFT_HAND_MIDDLE3].SetDirectionAndUp((poseLMiddleEnd   - poseLMiddle3),        -VUp);
    Bones[eLEFT_HAND_RING].SetTranslation(      poseLRing);         Bones[eLEFT_HAND_RING].SetDirectionAndUp(   (poseLPinky1      - poseLRing),           -VUp);
    Bones[eLEFT_HAND_RING1].SetTranslation(     poseLRing1);        Bones[eLEFT_HAND_RING1].SetDirectionAndUp(  (poseLRing2       - poseLRing1),          -VUp);
    Bones[eLEFT_HAND_RING2].SetTranslation(     poseLRing2);        Bones[eLEFT_HAND_RING2].SetDirectionAndUp(  (poseLRing3       - poseLRing2),          -VUp);
    Bones[eLEFT_HAND_RING3].SetTranslation(     poseLRing3);        Bones[eLEFT_HAND_RING3].SetDirectionAndUp(  (poseLRingEnd     - poseLRing3),          -VUp);
    Bones[eLEFT_HAND_PINKY1].SetTranslation(    poseLPinky1);       Bones[eLEFT_HAND_PINKY1].SetDirectionAndUp( (poseLPinky2      - poseLPinky1),         -VUp);
    Bones[eLEFT_HAND_PINKY2].SetTranslation(    poseLPinky2);       Bones[eLEFT_HAND_PINKY2].SetDirectionAndUp( (poseLPinky3      - poseLPinky2),         -VUp);
    Bones[eLEFT_HAND_PINKY3].SetTranslation(    poseLPinky3);       Bones[eLEFT_HAND_PINKY3].SetDirectionAndUp( (poseLPinkyEnd    - poseLPinky3),         -VUp);  

    // RIGHT ARM
    Bones[eRIGHT_SHOULDER].SetTranslation(       poseRClavicle);    Bones[eRIGHT_SHOULDER].SetDirectionAndUp(    (poseRUpperArm     - poseRClavicle),     -VUp);
    Bones[eRIGHT_ARM].SetTranslation(            poseRUpperArm);    Bones[eRIGHT_ARM].SetDirectionAndUp(         (poseRUpperArmRoll - poseRUpperArm),     -VUp);  
    Bones[eRIGHT_ARM_ROLL].SetTranslation(       poseRUpperArmRoll);Bones[eRIGHT_ARM_ROLL].SetDirectionAndUp(    (poseRLowerArm     - poseRUpperArmRoll), -VUp);
    Bones[eRIGHT_FOREARM].SetTranslation(        poseRLowerArm);    Bones[eRIGHT_FOREARM].SetDirectionAndUp(     (poseRLowerArmRoll - poseRLowerArm),     -VUp);
    Bones[eRIGHT_FOREARM_ROLL].SetTranslation(   poseRLowerArmRoll);Bones[eRIGHT_FOREARM_ROLL].SetDirectionAndUp((poseRHand         - poseRLowerArmRoll), -VUp);
    Bones[eRIGHT_HAND].SetTranslation(           poseRHand);        Bones[eRIGHT_HAND].SetDirectionAndUp(        (poseRMiddle1      - poseRHand),         -VUp);
    Bones[eRIGHT_HAND_THUMB1].SetTranslation(    poseRThumb1);      Bones[eRIGHT_HAND_THUMB1].SetDirectionAndUp( (poseRThumb2       - poseRThumb1),       -VUp);
    Bones[eRIGHT_HAND_THUMB2].SetTranslation(    poseRThumb2);      Bones[eRIGHT_HAND_THUMB2].SetDirectionAndUp( (poseRThumb3       - poseRThumb2),       -VUp);
    Bones[eRIGHT_HAND_THUMB3].SetTranslation(    poseRThumb3);      Bones[eRIGHT_HAND_THUMB3].SetDirectionAndUp( (poseRThumbEnd     - poseRThumb3),       -VUp);
    Bones[eRIGHT_HAND_INDEX1].SetTranslation(    poseRIndex1);      Bones[eRIGHT_HAND_INDEX1].SetDirectionAndUp( (poseRIndex2       - poseRIndex1),       -VUp);
    Bones[eRIGHT_HAND_INDEX2].SetTranslation(    poseRIndex2);      Bones[eRIGHT_HAND_INDEX2].SetDirectionAndUp( (poseRIndex3       - poseRIndex2),       -VUp);
    Bones[eRIGHT_HAND_INDEX3].SetTranslation(    poseRIndex3);      Bones[eRIGHT_HAND_INDEX3].SetDirectionAndUp( (poseRIndexEnd     - poseRIndex3),       -VUp);
    Bones[eRIGHT_HAND_MIDDLE1].SetTranslation(   poseRMiddle1);     Bones[eRIGHT_HAND_MIDDLE1].SetDirectionAndUp((poseRMiddle2      - poseRMiddle1),      -VUp);
    Bones[eRIGHT_HAND_MIDDLE2].SetTranslation(   poseRMiddle2);     Bones[eRIGHT_HAND_MIDDLE2].SetDirectionAndUp((poseRMiddle3      - poseRMiddle2),      -VUp);
    Bones[eRIGHT_HAND_MIDDLE3].SetTranslation(   poseRMiddle3);     Bones[eRIGHT_HAND_MIDDLE3].SetDirectionAndUp((poseRMiddleEnd    - poseRMiddle3),      -VUp);
    Bones[eRIGHT_HAND_RING].SetTranslation(      poseRRing);        Bones[eRIGHT_HAND_RING].SetDirectionAndUp(   (poseRPinky1       - poseRRing),         -VUp);
    Bones[eRIGHT_HAND_RING1].SetTranslation(     poseRRing1);       Bones[eRIGHT_HAND_RING1].SetDirectionAndUp(  (poseRRing2        - poseRRing1),        -VUp);
    Bones[eRIGHT_HAND_RING2].SetTranslation(     poseRRing2);       Bones[eRIGHT_HAND_RING2].SetDirectionAndUp(  (poseRRing3        - poseRRing2),        -VUp);
    Bones[eRIGHT_HAND_RING3].SetTranslation(     poseRRing3);       Bones[eRIGHT_HAND_RING3].SetDirectionAndUp(  (poseRRingEnd      - poseRRing3),        -VUp);
    Bones[eRIGHT_HAND_PINKY1].SetTranslation(    poseRPinky1);      Bones[eRIGHT_HAND_PINKY1].SetDirectionAndUp( (poseRPinky2       - poseRPinky1),       -VUp);
    Bones[eRIGHT_HAND_PINKY2].SetTranslation(    poseRPinky2);      Bones[eRIGHT_HAND_PINKY2].SetDirectionAndUp( (poseRPinky3       - poseRPinky2),       -VUp);
    Bones[eRIGHT_HAND_PINKY3].SetTranslation(    poseRPinky3);      Bones[eRIGHT_HAND_PINKY3].SetDirectionAndUp( (poseRPinkyEnd     - poseRPinky3),       -VUp);  

    Bones[eFP_CAMERA] = Bones[eHEAD];
  }
};

static const CSkeletonDefinition SkeletonDefinition;

#endif