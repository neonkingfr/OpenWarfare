#pragma once

#ifndef VRPNClient_h__
#define VRPNClient_h__

#include <map>
#include "./vrpn/vrpn/vrpn_Tracker.h"

typedef enum
{
  AXIS_DEFAULT = 0,
  UP_TO_NEGATIVE_UP,
  UP_TO_DIR,
  UP_TO_NEGATIVE_DIR,
  UP_TO_ASIDE,
  UP_TO_NEGATIVE_ASIDE,
  DIR_TO_ASIDE,
  DIR_TO_NEGATIVE_ASIDE
} eAXIS_ADJUSTMENT;

typedef enum
{
  ROTATION_DEFAULT = 0,
  ROTATEX_HALF_PI,
  ROTATEX_NEGATIVE_HALF_PI,
  ROTATEX_PI,
  ROTATEY_HALF_PI,
  ROTATEY_NEGATIVE_HALF_PI,
  ROTATEY_PI,
  ROTATEZ_HALF_PI,
  ROTATEZ_NEGATIVE_HALF_PI,
  ROTATEZ_PI
} eROTATION_ADJUSTMENT;

struct Tracker 
{
  vrpn_Tracker_Remote* _remote;
  vrpn_TRACKERCB _data;
  eAXIS_ADJUSTMENT _axisAdjustment;
  eROTATION_ADJUSTMENT _rotationAdjustment;
};

typedef std::map<std::string, Tracker> TrackerList;

struct AdjustmentPair
{
  eAXIS_ADJUSTMENT _axis;
  eROTATION_ADJUSTMENT _rotation;
};

typedef std::map<std::string, AdjustmentPair> AdjustmentList;

typedef enum
{
  UP_AXIS_Y,
  UP_AXIS_Z,
  UP_AXIS_DEFAULT = UP_AXIS_Y
} eDATA_UP_AXIS;

// tracker-initialization flag
typedef enum
{
  eTRACKER_UNINITIALIZED = 0,
  eTRACKER_INITIALIZED
}ETrackerInitFlag;

typedef std::map<int,std::string> BoneTrackerMap;


class VRPNClient
{
  friend class PoseControlUnit;

public:
  //! connection string (server:port)
  char _connectionName[1024];

  //! vrpn connection
  vrpn_Connection* _pConnection;

  //! incoming data up axis
  eDATA_UP_AXIS _upAxis;

  //! list of VRPN trackers
  TrackerList _trackers;

  //! list of tracker adjustments
  AdjustmentList _axisAdjustments;

  //! bone index to tracker map
  BoneTrackerMap _boneTrackerMap;
  
public:
  VRPNClient();
  ~VRPNClient();

  bool InitConnection();
  void OnSimStep();
  void SetPoseControlEnabled(bool enabled);

private:
  bool Initialize();
  bool InitConnection( std::string &line, std::ifstream& inConfigFile );
  void ReadAdjustments( std::string &line, std::ifstream &inConfigFile );
  void InitUnit( std::string &line, std::ifstream &inConfigFile );
  void CreateControlledUnit( std::string &unitName );
  void LoadServerConfig(std::ifstream &inConfigFile, std::string &line);
  std::string GetConfValue(std::string confString);
  void GetUncommentedLine(std::ifstream& confFile, std::string& line);
  bool NewConfigEntry(const string &line);
  bool GetConfigValues(const string &confString, string &confName_out, string &confValue_out);
};

//! tracker handler (updates tracker data from remote tracker)
void	VRPN_CALLBACK HandleVRPNTracker (void *userdata, const vrpn_TRACKERCB t);

#endif // VRPNClient_h__
