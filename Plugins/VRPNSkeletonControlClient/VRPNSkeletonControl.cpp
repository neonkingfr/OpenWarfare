
#include <windows.h>
#include "VRPNSkeletonControl.h"
#include "VRPNClient.h"
#include ".\vrpn\quat\quat.h"

VRPNClient* g_pClient = NULL;

static Matrix4 lastLeftHandBoneTransformation;
static Matrix4 lastRightHandBoneTransformation;
static Matrix4 lastSpineBoneTransformation;
static Matrix4 lastHeadBoneTransformation;
static Matrix4 cameraTransformation;

static float playerPosX, playerPosY, playerPosZ; // current player position
static float origPlayerPosX, origPlayerPosY, origPlayerPosZ; // original player position
static float pelvisPrevXOffset, pelvisPrevYOffset, pelvisXoffset, pelvisYoffset; // pelvis offsets

const double	PI = 3.14159265358979323846;
const double	HALF_PI = 1.57079632679489661923;

//////////////////////////////////////////////////////////////////////////
void TrackerToMatrix(q_type quat, float pos[3], Matrix4& mat)
{
  float x = quat[0];
  float y = quat[1];
  float z = quat[2];
  float w = quat[3];

  float wx = w*x*2;
  float wy = w*y*2;
  float wz = w*z*2;
  float xx = x*x*2;
  float xy = x*y*2;
  float xz = x*z*2;
  float yy = y*y*2;
  float yz = y*z*2;
  float zz = z*z*2;

  mat.Set(0, 0) = 1 - yy - zz;
  mat.Set(0, 1) = xy - wz;
  mat.Set(0, 2) = xz + wy;
  mat.Set(1, 0) = xy + wz;
  mat.Set(1, 1) = 1 - xx - zz;
  mat.Set(1, 2) = yz - wx;
  mat.Set(2, 0) = xz - wy;
  mat.Set(2, 1) = yz + wx;
  mat.Set(2, 2) = 1 - xx - yy;

  mat.SetTranslation(Vector3(pos[0], pos[1], pos[2]));
}

// Command function definition
// Function that will register the ExecuteCommand function of the engine
ExecuteCommandType ExecuteCommand = NULL;
int ExecuteCmd(const char* command, char *result = NULL, int resultLength = 0)
{
  if (!ExecuteCommand) return 0;
  return ExecuteCommand(command, result, resultLength);
}

// get current player position
void GetPlayerPos(float& x, float& y, float&z)
{
  // get original position
  // Set orig pos
  char command[256];
  sprintf(command, "getPosASL2 player");

  // send command
  char resultBuff[256];
  if(!ExecuteCommand(command, resultBuff, 256))
  {
    char debugText[256];
    sprintf(debugText, "GetPos failed: %s\n", resultBuff);
    OutputDebugString(debugText);
    return;
  }

  // parse results
  int parse = sscanf(resultBuff, "[%f,%f,%f]", &x, &y, &z);
  if (parse != 3)
  {
    OutputDebugString("GetPos failed: can't parse command result\n");
    return;
  }
}

// set player position
void SetPlayerPos(float x, float y, float z)
{
  char buf[512];
  sprintf(buf, "player setPosASL2 [ %f, %f, %f]", x, y, z);
  ExecuteCmd(buf);
}

VBSPLUGIN_EXPORT void WINAPI RegisterCommandFnc(void *executeCommandFnc)
{
  ExecuteCommand = (ExecuteCommandType)executeCommandFnc;
}

// Function that will register the ExecuteCommand function of the engine
VBSPLUGIN_EXPORT void WINAPI RegisterHWND(void *hwnd, void *hins)
{
}

inline void EnableEPC(bool enable=true)
{
  if (enable)
  {
    ExecuteCmd("player setExternalPose true");
    ExecuteCmd("player setExternalPoseUpBody true");
    ExecuteCmd("player setExternalPoseSkeleton true");
    ExecuteCmd("player setExternalCamera true");
  }
  else
  {
    ExecuteCmd("player setExternalPose false");
    ExecuteCmd("player setExternalPoseUpBody false");
    ExecuteCmd("player setExternalPoseSkeleton false");
    ExecuteCmd("player setExternalCamera false");
  }
}

#define rad2Deg (180.0f/3.141592653589)

// This function will be executed every simulation step (every frame) and took a part in the simulation procedure.
// We can be sure in this function the ExecuteCommand registering was already done.
// deltaT is time in seconds since the last simulation step
VBSPLUGIN_EXPORT void WINAPI OnSimulationStep(float deltaT)
{
  if (g_pClient)
  {
    g_pClient->OnSimStep();
  }
}

// This function will be executed every time the script in the engine calls the script function "pluginFunction"
// We can be sure in this function the ExecuteCommand registering was already done.
// Note that the plugin takes responsibility for allocating and deleting the returned string
VBSPLUGIN_EXPORT const char* WINAPI PluginFunction(const char *input)
{
  static const char result[]="VRPNSkeletonControlPlugin";

  if (input[0] == 'i')
  {	
    if (!g_pClient)
    {
      g_pClient = new VRPNClient();
      EnableEPC();
    }

    //ExecuteCmd("\"LookAround\" setAction 1"); 
    ExecuteCmd("player allowDammage false"); 
    ExecuteCmd("player setDisableAnimationMove true");
    ExecuteCmd("player setDir 0"); //reset player orientation

    GetPlayerPos(origPlayerPosX, origPlayerPosY, origPlayerPosZ);

    //init offsets
    pelvisXoffset = 0.0f;
    pelvisYoffset = 0.0f;
  }
  else if (!stricmp(input, "e"))
  {
    EnableEPC();
    //init offsets
    pelvisXoffset = 0.0f;
    pelvisYoffset = 0.0f;
    ExecuteCmd("player setDir 0"); //reset player orientation
  }
  else if (!stricmp(input, "d"))
  {
    EnableEPC(false);
    SetPlayerPos(origPlayerPosX, origPlayerPosY, origPlayerPosZ);
    ExecuteCmd("setCamFrustum [true, tan 35]");
  }

  return result;
}

//
#ifdef VBS2NG
VBSPLUGIN_EXPORT void WINAPI OnModifyCamera(Matrix4DP &mat, int camType)
#else
VBSPLUGIN_EXPORT void WINAPI OnModifyCamera(Matrix4 &mat, int camType)
#endif
{
  // person needs to be in the internal camera.
  if (camType==0)
  {
    BI_TVector3<float> pos = cameraTransformation.GetPos();

#ifdef VBS2NG
    Matrix4DP relativeCameraTransformation;
#else
    Matrix4 relativeCameraTransformation;
#endif
    OnModifyBone(relativeCameraTransformation, eFP_CAMERA);

    // relative rotation, absolute translation?
    mat=relativeCameraTransformation;
#ifdef VBS2NG
    mat._position[0] = pos[0];
    mat._position[1] = pos[1];
    mat._position[2] = pos[2];
#else
    mat.SetTranslation(pos);
#endif

    ExecuteCmd("setCamFrustum [true, tan 30]");
  }
  else
  {
    ExecuteCmd("setCamFrustum [true, tan 35]");
  }
}

/*
position x axis (right-left)
position y axis (closer-further)
position z axis (down-up)
*/
#ifdef VBS2NG
VBSPLUGIN_EXPORT void WINAPI OnModifyBone(Matrix4DP& mat, int skeletonIndex)
#else
VBSPLUGIN_EXPORT void WINAPI OnModifyBone(Matrix4& mat, int skeletonIndex)
#endif
{
  if (!g_pClient) { return; }

  //-----------------------------------------------------------------------
  // check for uncontrolled bones  
  switch (skeletonIndex)
  {
  case eLBROW:                { return; }
  case eMBROW:                { return; }
  case eRBROW:                { return; }
  case eLMOUTH:               { return; }
  case eMMOUTH:               { return; }
  case eRMOUTH:               { return; }
  case eEYELIDS:              { return; }
  case eLLIP:                 { return; }
  case eCAMERA:               { return; }

                              // FIXED LEFT-HAND BONES
  case eLEFT_HAND_RING:       { mat = lastLeftHandBoneTransformation; return; }
  case eLEFT_HAND_RING1:      { mat = lastLeftHandBoneTransformation; return; }
  case eLEFT_HAND_RING2:      { mat = lastLeftHandBoneTransformation; return; }
  case eLEFT_HAND_RING3:      { mat = lastLeftHandBoneTransformation; return; }
  case eLEFT_HAND_PINKY1:     { mat = lastLeftHandBoneTransformation; return; }
  case eLEFT_HAND_PINKY2:     { mat = lastLeftHandBoneTransformation; return; }
  case eLEFT_HAND_PINKY3:     { mat = lastLeftHandBoneTransformation; return; }
  case eLEFT_HAND_MIDDLE1:    { mat = lastLeftHandBoneTransformation; return; }
  case eLEFT_HAND_MIDDLE2:    { mat = lastLeftHandBoneTransformation; return; }
  case eLEFT_HAND_MIDDLE3:    { mat = lastLeftHandBoneTransformation; return; }
  case eLEFT_HAND_INDEX1:     { mat = lastLeftHandBoneTransformation; return; }
  case eLEFT_HAND_INDEX2:     { mat = lastLeftHandBoneTransformation; return; }
  case eLEFT_HAND_INDEX3:     { mat = lastLeftHandBoneTransformation; return; }
  case eLEFT_HAND_THUMB1:     { mat = lastLeftHandBoneTransformation; return; }
  case eLEFT_HAND_THUMB2:     { mat = lastLeftHandBoneTransformation; return; }
  case eLEFT_HAND_THUMB3:     { mat = lastLeftHandBoneTransformation; return; }

                              // FIXED RIGHT-HAND BONES
  case eRIGHT_HAND_RING:       { mat = lastRightHandBoneTransformation; return; }
  case eRIGHT_HAND_RING1:      { mat = lastRightHandBoneTransformation; return; }
  case eRIGHT_HAND_RING2:      { mat = lastRightHandBoneTransformation; return; }
  case eRIGHT_HAND_RING3:      { mat = lastRightHandBoneTransformation; return; }
  case eRIGHT_HAND_PINKY1:     { mat = lastRightHandBoneTransformation; return; }
  case eRIGHT_HAND_PINKY2:     { mat = lastRightHandBoneTransformation; return; }
  case eRIGHT_HAND_PINKY3:     { mat = lastRightHandBoneTransformation; return; }
  case eRIGHT_HAND_MIDDLE1:    { mat = lastRightHandBoneTransformation; return; }
  case eRIGHT_HAND_MIDDLE2:    { mat = lastRightHandBoneTransformation; return; }
  case eRIGHT_HAND_MIDDLE3:    { mat = lastRightHandBoneTransformation; return; }
  case eRIGHT_HAND_INDEX1:     { mat = lastRightHandBoneTransformation; return; }
  case eRIGHT_HAND_INDEX2:     { mat = lastRightHandBoneTransformation; return; }
  case eRIGHT_HAND_INDEX3:     { mat = lastRightHandBoneTransformation; return; }
  case eRIGHT_HAND_THUMB1:     { mat = lastRightHandBoneTransformation; return; }
  case eRIGHT_HAND_THUMB2:     { mat = lastRightHandBoneTransformation; return; }
  case eRIGHT_HAND_THUMB3:     { mat = lastRightHandBoneTransformation; return; }

                               // PELVIS
  case ePELVIS: 
    {
      mat = lastSpineBoneTransformation;
      return;
    }

  default:
    break;
  }

  //-----------------------------------------------------------------------
  // get tracker data
  vrpn_TRACKERCB trackerData;
  bool dataInitialized = false;
  eAXIS_ADJUSTMENT axisAdjustment = AXIS_DEFAULT;
  eROTATION_ADJUSTMENT rotationAdjustment = ROTATION_DEFAULT;

  BoneTrackerMap::const_iterator itTrackerData = g_pClient->_boneTrackerMap.find(skeletonIndex);
  if (itTrackerData != g_pClient->_boneTrackerMap.end())
  {
    TrackerList::iterator itTracker = g_pClient->_trackers.find((*itTrackerData).second);
    if (itTracker != g_pClient->_trackers.end())
    {
      trackerData = (*itTracker).second._data;
      axisAdjustment = (*itTracker).second._axisAdjustment;
      rotationAdjustment = (*itTracker).second._rotationAdjustment;
      dataInitialized = (trackerData.sensor == eTRACKER_INITIALIZED); // sensor used as initialization flag
    }
  }

  //-----------------------------------------------------------------------
  // process tracker data
  // (if the tracker isn't receiving data from server, identity matrix is set)
  if (dataInitialized)
  {
    q_type boneRotation = { trackerData.quat[0], trackerData.quat[1], trackerData.quat[2], trackerData.quat[3] };

    // get position and convert to VBS coord system
    float bonePosition[3];
    if (g_pClient->_upAxis == UP_AXIS_Z)
    {
      bonePosition[0] = -trackerData.pos[0];
      bonePosition[1] = trackerData.pos[2];
      bonePosition[2] = -trackerData.pos[1];
    }
    else
    {
      bonePosition[0] = -trackerData.pos[0];
      bonePosition[1] = trackerData.pos[1];
      bonePosition[2] = trackerData.pos[2];
    }

    // apply rotation adjustment from config
    q_type rotation;
    switch (rotationAdjustment)
    {
    case ROTATEX_HALF_PI:
      {
        q_from_axis_angle(rotation, 1.0, 0.0, 0.0, HALF_PI);
        q_mult(boneRotation, boneRotation, rotation);
        break;
      }
    case ROTATEX_NEGATIVE_HALF_PI:
      {
        q_from_axis_angle(rotation, 1.0, 0.0, 0.0, -HALF_PI);
        q_mult(boneRotation, boneRotation, rotation);
        break;
      }
    case ROTATEX_PI:
      {
        q_from_axis_angle(rotation, 1.0, 0.0, 0.0, PI);
        q_mult(boneRotation, boneRotation, rotation);
        break;
      }
    case ROTATEY_HALF_PI:
      {
        q_from_axis_angle(rotation, 0.0, 0.0, 1.0, HALF_PI);
        q_mult(boneRotation, boneRotation, rotation);
        break;
      }
    case ROTATEY_NEGATIVE_HALF_PI:
      {
        q_from_axis_angle(rotation, 0.0, 0.0, 1.0, -HALF_PI);
        q_mult(boneRotation, boneRotation, rotation);
        break;
      }
    case ROTATEY_PI:
      {
        q_from_axis_angle(rotation, 0.0, 0.0, 1.0, PI);
        q_mult(boneRotation, boneRotation, rotation);
        break;
      }
    case ROTATEZ_HALF_PI:
      {
        q_from_axis_angle(rotation, 0.0, 1.0, 0.0, HALF_PI);
        q_mult(boneRotation, boneRotation, rotation);
        break;
      }
    case ROTATEZ_NEGATIVE_HALF_PI:
      {
        q_from_axis_angle(rotation, 0.0, 1.0, 0.0, -HALF_PI);
        q_mult(boneRotation, boneRotation, rotation);
        break;
      }
    case ROTATEZ_PI:
      {
        q_from_axis_angle(rotation, 0.0, 1.0, 0.0, PI);
        q_mult(boneRotation, boneRotation, rotation);
        break;
      }
    }

    // get absolute transform matrix from tracker
    Matrix4 absoluteTransform = BI_IdentityMatrix4;
    TrackerToMatrix(boneRotation, bonePosition, absoluteTransform);

    // convert rotation matrix to VBS coord. system
    if (g_pClient->_upAxis == UP_AXIS_Z)
    {
      // Z-up RH to Y-up LH
      BI_Vector3 dir = absoluteTransform.GetDirection();
      dir.SetX(-absoluteTransform.GetDirection().X());
      dir.SetY(absoluteTransform.GetDirection().Z());
      dir.SetZ(-absoluteTransform.GetDirection().Y());
      BI_Vector3 up = absoluteTransform.GetUp();
      up.SetX(-absoluteTransform.GetUp().X());
      up.SetY(absoluteTransform.GetUp().Z());
      up.SetZ(-absoluteTransform.GetUp().Y());
      absoluteTransform.SetDirectionAndUp(dir, up);
    }
    else
    {
      // Y-up RH to Y-up LH
      BI_Vector3 dir = absoluteTransform.GetDirection();
      dir.SetX(-dir.X());
      BI_Vector3 up = absoluteTransform.GetUp();
      up.SetX(-up.X());
      absoluteTransform.SetDirectionAndUp(dir, up);
    }

    // Apply axis adjustment from config
    {
      BI_Vector3 dir = absoluteTransform.GetDirection();
      BI_Vector3 up = absoluteTransform.GetUp();
      BI_Vector3 aside = absoluteTransform.GetAside();

      switch (axisAdjustment)
      {
      case UP_TO_NEGATIVE_UP: { absoluteTransform.SetDirectionAndUp(dir, -up); break; }
      case UP_TO_DIR: { absoluteTransform.SetDirectionAndUp(up, dir); break; }      
      case UP_TO_NEGATIVE_DIR: { absoluteTransform.SetDirectionAndUp(up, -dir); break; }
      case UP_TO_ASIDE: { absoluteTransform.SetDirectionAndUp(dir, aside); break; }      
      case UP_TO_NEGATIVE_ASIDE: { absoluteTransform.SetDirectionAndUp(dir, -aside); break; }
      case DIR_TO_ASIDE: { absoluteTransform.SetDirectionAndAside(aside, dir); break; }
      case DIR_TO_NEGATIVE_ASIDE: { absoluteTransform.SetDirectionAndAside(aside, -dir); break; }
      default:
        break;
      }
    }

    // adjust player position in the game
    if (skeletonIndex == eSPINE)
    {
      pelvisPrevXOffset = pelvisXoffset;
      pelvisPrevYOffset = pelvisYoffset;

      pelvisXoffset = absoluteTransform.GetPos().X();
      pelvisYoffset = absoluteTransform.GetPos().Z();

      float deltaX = pelvisXoffset - pelvisPrevXOffset;
      float deltaY = pelvisYoffset - pelvisPrevYOffset;

      GetPlayerPos(playerPosX, playerPosY, playerPosZ);
      SetPlayerPos(playerPosX + deltaX, playerPosY + deltaY, playerPosZ);
    }

    BI_TVector3<float> pos = absoluteTransform.GetPos();
    pos.SetX(pos.X() - pelvisXoffset);
    pos.SetZ(pos.Z() - pelvisYoffset);
    absoluteTransform.SetTranslation(pos);

    // current bone's binding pose
    Matrix4 bindingPose = SkeletonDefinition.Bones[skeletonIndex];

    // inverse binding pose
    Matrix4 invBindingPose;
    invBindingPose.SetInvertGeneral4x4(bindingPose);

    // compute relative-to-binding-pose transform
    Matrix4 relativeToBP = BI_IdentityMatrix4;
    relativeToBP.SetMultiply(absoluteTransform, invBindingPose);

    mat = relativeToBP;

    // save transformations for fixed bones
    switch (skeletonIndex)
    {
    case eLEFT_HAND: { lastLeftHandBoneTransformation = relativeToBP; break; }
    case eRIGHT_HAND: { lastRightHandBoneTransformation = relativeToBP; break; }
    case eSPINE: { lastSpineBoneTransformation = relativeToBP; break; }
    case eHEAD: { lastHeadBoneTransformation = absoluteTransform; break; }
    case eFP_CAMERA: { cameraTransformation = absoluteTransform; break; }
    default:
      break;
    }
  }
  else { mat = BI_IdentityMatrix4; }
}

VBSPLUGIN_EXPORT void WINAPI OnUnload (void)
{
  EnableEPC(false);
  if ( (origPlayerPosX==0.0f) && (origPlayerPosY==0.0f) && (origPlayerPosZ==0.0f) )
    return;
  SetPlayerPos(origPlayerPosX, origPlayerPosY, origPlayerPosZ);
  ExecuteCmd("setCamFrustum [true, tan 35]");
}

// DllMain
BOOL WINAPI DllMain(HINSTANCE hDll, DWORD fdwReason, LPVOID lpvReserved)
{
  switch(fdwReason)
  {
  case DLL_PROCESS_ATTACH:
    {
      OutputDebugString("EPCTest Called DllMain with DLL_PROCESS_ATTACH\n");
      break;
    }
  case DLL_PROCESS_DETACH:
    OutputDebugString("EPCTest Called DllMain with DLL_PROCESS_DETACH\n");
    break;
  case DLL_THREAD_ATTACH:
    OutputDebugString("EPCTest Called DllMain with DLL_THREAD_ATTACH\n");
    break;
  case DLL_THREAD_DETACH:
    OutputDebugString("EPCTest Called DllMain with DLL_THREAD_DETACH\n");
    break;
  }
  return TRUE;
}
