// EmfToTga.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"


#define STEP 3000
void DrawEmfToBitmap(HENHMETAFILE meta, Picture &pic, const RECT &src, const RECT &trg, const RECT &bounding)
{
  RECT mapRect;
  mapRect.left=0;
  mapRect.top=0;
  mapRect.right=src.right-src.left;
  mapRect.bottom=src.bottom-src.top;

  unsigned long lineoffset=(mapRect.right*3+3)& ~0x3;

  BITMAPINFO bmi;

  ZeroMemory(&bmi,sizeof(bmi));
  bmi.bmiHeader.biSize=sizeof(bmi.bmiHeader);
  bmi.bmiHeader.biWidth=mapRect.right;
  bmi.bmiHeader.biHeight=mapRect.bottom;
  bmi.bmiHeader.biPlanes=1;
  bmi.bmiHeader.biSizeImage=mapRect.bottom*lineoffset;
  bmi.bmiHeader.biBitCount=24;
  bmi.bmiHeader.biCompression=BI_RGB;  
  puts("...alocating temporally bitmap");

  HWND wnd=GetDesktopWindow();
  HDC ddc=GetDC(wnd);
  HDC dc=CreateCompatibleDC(ddc);
  HBITMAP bmp=CreateCompatibleBitmap(ddc,mapRect.right,mapRect.bottom);
  if (bmp==0)
  {
    puts("ERROR: Create bitmap failed");
    return;
  }
  ReleaseDC(wnd,ddc);
  HBITMAP old=(HBITMAP)SelectObject(dc,bmp);
  SetMapMode(dc,MM_ANISOTROPIC);
  SetViewportExtEx(dc,mapRect.right,mapRect.bottom,0);
  SetWindowExtEx(dc,mapRect.right,mapRect.bottom,0);
  SetWindowOrgEx(dc,src.left,src.top,0);
  SetViewportOrgEx(dc,0,0,0);

  puts("...rendering vectors into the bitmap");
  PlayEnhMetaFile(dc,meta,&bounding);

  /*SelectObject(dc,GetStockObject(18));
  SetDCBrushColor(dc,RGB(0,255,255));
  Rectangle(dc,0,0,500,500);*/


  COLORREF px=GetPixel(dc,10,10);

  SelectObject(dc,old);

  puts("...converting pixel format");

  unsigned char *buff=new unsigned char[lineoffset*mapRect.bottom];
  if (buff==0) 
  {
    puts("ERROR: Unable to allocate memory");
    return;
  }
    

  GetDIBits(dc,bmp,0,mapRect.bottom,buff,&bmi,DIB_RGB_COLORS);



  puts("...merging temporally into the result");
  for (int y=0;y<mapRect.bottom;y++)
  {
    for (int x=0;x<mapRect.right;x++)
    {
      unsigned long v=*reinterpret_cast<unsigned long *>(buff+x*3+y*lineoffset);
      v |= 0xFF000000;
      pic.SetPixel(x+trg.left,(mapRect.bottom-y-1)+trg.top,v);
    }
  }
  puts("...cleanup");
  DeleteDC(dc);
  DeleteObject(bmp);

  delete [] buff;
}

int _tmain(int argc, _TCHAR* argv[])
{

if (argc<2)
{
  puts("Usage: EmfToTga source_file [zoom]");
  return 0;
}

  Pathname name(argv[1]);
  HENHMETAFILE meta=GetEnhMetaFile(name);
  if (meta==0 || meta==INVALID_HANDLE_VALUE)
  {
    puts("Error: Open metafile error");
    return -1;
  }

  double zoom=1;
  if (argc>2)
  {
    zoom=atof(argv[2]);
    if (zoom<0.0001)
    {
      puts("Error: Invalid parameter zoom");
      return -2;
    }
  }

  ENHMETAHEADER hdr;
  GetEnhMetaFileHeader(meta,sizeof(hdr),&hdr);

  puts("Loading vector format into the memory");

  BYTE *buff=new BYTE[hdr.nBytes];
  GetEnhMetaFileBits(meta,hdr.nBytes,buff);
  DeleteEnhMetaFile(meta);

  meta=SetEnhMetaFileBits(hdr.nBytes,buff);
  delete [] buff;

  RECT bounds;
  bounds.left=toLargeInt(hdr.rclBounds.left*zoom);
  bounds.top=toLargeInt(hdr.rclBounds.top*zoom);
  bounds.right=toLargeInt((hdr.rclBounds.right+1)*zoom);
  bounds.bottom=toLargeInt((hdr.rclBounds.bottom+1)*zoom);


  puts("Allocating the bitmap");
  Picture pic;
  pic.Create(bounds.right-bounds.left,bounds.bottom-bounds.top);

  for (int yt=bounds.bottom-bounds.top-STEP,ys=bounds.bottom-STEP;ys>-STEP;ys-=STEP,yt-=STEP)
  {    
    for (int xt=bounds.right-bounds.left-STEP,xs=bounds.right-STEP;xs>-STEP;xs-=STEP,xt-=STEP)
    {
      RECT srcRect;
      RECT trgRect;
      srcRect.left=xs;
      srcRect.top=ys;
      srcRect.right=xs+STEP;
      srcRect.bottom=ys+STEP;
      trgRect.left=xt;
      trgRect.top=yt;
      trgRect.right=xt+STEP;
      trgRect.bottom=yt+STEP;
      if (srcRect.left<bounds.left)
      {
        int diff=bounds.left-srcRect.left;
        srcRect.left+=diff;
        trgRect.left+=diff;
      }
      if (srcRect.top<bounds.top)
      {
        int diff=bounds.top-srcRect.top;
        srcRect.top+=diff;
        trgRect.top+=diff;
      }
      printf("Processing position X %d/%d,  Y %d/%d\n",srcRect.left,bounds.right-bounds.left,srcRect.top,bounds.bottom-bounds.top);
      DrawEmfToBitmap(meta,pic,srcRect,trgRect,bounds);
    }
  }


  puts("Saving the bitmap");
  name.SetExtension(".tga");
  pic.SaveTGA(name);
  DeleteEnhMetaFile(meta);
	return 0;
}

