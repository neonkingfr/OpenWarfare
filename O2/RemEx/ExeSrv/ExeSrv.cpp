// ExeSrv.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <stdio.h>
#include <winsock.h>


SOCKET establish(unsigned short portnum)
{
  char   myname[256];
  SOCKET s;
  struct sockaddr_in sa;
  struct hostent *hp;

  memset(&sa, 0, sizeof(struct sockaddr_in)); /* clear our address */
  int a = gethostname(myname, sizeof(myname));        /* who are we? */
  hp = gethostbyname(myname);                 /* get our address info */
  if (hp == NULL)                             /* we don't exist !? */
    return(INVALID_SOCKET);
  sa.sin_family = hp->h_addrtype;             /* this is our host address */
  sa.sin_port = htons(portnum);               /* this is our port number */
  s = socket(AF_INET, SOCK_STREAM, 0);        /* create the socket */
  if (s == INVALID_SOCKET)
    return INVALID_SOCKET;

  /* bind the socket to the internet address */
  if (bind(s, (struct sockaddr *)&sa, sizeof(struct sockaddr_in)) ==
      SOCKET_ERROR) {
    closesocket(s);
    return(INVALID_SOCKET);
  }
  listen(s, SOMAXCONN);                               /* max # of queued connects */
  return(s);
}

#define BUFFER_LENGTH 4096

#define MAX_APPLICATIONS 1024

void ProcessSocket(SOCKET s, char *app[MAX_APPLICATIONS][2], int appCount)
{
  // Retrieve the data
  char buf[BUFFER_LENGTH];
  int bytesRecv = recv(s, buf, BUFFER_LENGTH, 0);
  if (bytesRecv < 0)
  {
    printf("Error: An error occurred during socket retrieving: %d\n", WSAGetLastError());
    return;
  }
  if (bytesRecv >= BUFFER_LENGTH)
  {
    printf("Error: Received data too long\n");
    return;
  }

  // Finish the string
  buf[bytesRecv] = 0;

  // Find the 2 strings
  char *client = &buf[0];
  int cStart = strlen(client) + 1; if (cStart >= bytesRecv) {printf("Error: Command string not specified\n"); return;}
  char *command = &buf[cStart];
  int aStart = cStart + strlen(command) + 1; if (aStart >= bytesRecv) {printf("Error: Command string not specified\n"); return;}
  char *arguments = &buf[aStart];

  // Convert argument list to a string
  char argumentList[BUFFER_LENGTH];
  strcpy(argumentList, "");
  int strStart = 0;
  int strLen;
  while ((strLen = strlen(&arguments[strStart])) > 0)
  {
    strcat(argumentList, &arguments[strStart]);
    strStart += strLen + 1;
    strcat(argumentList, " ");
  }

  // Log the command & arguments
  printf("Client: %s\n", client);
  printf("Command: %s\n", command);
  printf("Arguments: %s\n\n", argumentList);

  // Find the command
  int cIndex;
  for (cIndex = 0; cIndex < appCount; cIndex++)
  {
    if (strcmpi(command, app[cIndex][0]) == 0) break;
  }
  if (cIndex >= appCount)
  {
    printf("Error: Command is not in the application list\n");
  }

  // Execute the command
  STARTUPINFO si;
  si.cb = sizeof(STARTUPINFO);
  si.lpReserved = NULL;
  si.lpDesktop = NULL;
  si.lpTitle = NULL;
  si.dwX = 0;
  si.dwY = 0;
  si.dwXSize = 0;
  si.dwYSize = 0;
  si.dwXCountChars = 0;
  si.dwYCountChars = 0;
  si.dwFillAttribute = 0;
  si.dwFlags = 0;
  si.wShowWindow = 0;
  si.cbReserved2 = 0;
  si.lpReserved2 = 0;
  si.hStdInput = 0;
  si.hStdOutput = 0;
  si.hStdError = 0;
  PROCESS_INFORMATION pi;
  char commandLine[BUFFER_LENGTH];
  _snprintf(commandLine, BUFFER_LENGTH - 1, "\"%s\" %s", app[cIndex][1], argumentList);
  if (CreateProcess(NULL, commandLine, NULL, NULL, true, NORMAL_PRIORITY_CLASS, NULL, NULL, &si, &pi))
  {
    WaitForSingleObject(pi.hProcess, INFINITE);
    CloseHandle(pi.hProcess);
    CloseHandle(pi.hThread);
  }
}


#define PORTNUM 50000 /* random port number, we need something */

#define MAX_APPRECORDSIZE 1024

char *app[MAX_APPLICATIONS][2];
int appCount = 0;

int LoadList(const char * name)
{
  // Read the application list into internal structure
  {
    appCount = 0;
    FILE *f = fopen(name, "r");
    if (f == NULL)
    {
      printf("Error: Cannot open %s file\n",name);
      return 1;
    }
    do
    {
      // Read the application name
      char s[MAX_APPRECORDSIZE];
      int c = 0;
      {
        // Skip free spaces
        while ((s[c] = fgetc(f)) == ' ') {}

        // If end of file then terminate this line
        if (feof(f)) break;

        // Read first word until a space or end of line or eof
        do
        {
          s[++c] = fgetc(f);
        } while ((s[c] != ' ') && (s[c] != '\n') && (!feof(f)));

        // If end of file then terminate this line
        if (feof(f)) break;

        // Terminate string
        s[c] = 0;
      }

      // Read the application path
      char ps[MAX_APPRECORDSIZE];
      int pc = 0;
      {
        // Skip free spaces
        while ((ps[pc] = fgetc(f)) == ' ') {}

        // If end of file then terminate this line
        if (feof(f)) break;

        // Read second word until end of line or eof
        do
        {
          ps[++pc] = fgetc(f);
        } while ((ps[pc] != '\n') && (!feof(f)));

        // Terminate string
        ps[pc] = 0;
      }
      if (appCount>=MAX_APPRECORDSIZE)
      {
        printf("Error: record in %s is to long \n",name);
        return 2;
      }

      // Lower both strings
      strlwr(s);
      strlwr(ps);

      // Add new record to the internal structure
      app[appCount][0] = new char[MAX_APPRECORDSIZE];
      app[appCount][1] = new char[MAX_APPRECORDSIZE];
      strcpy(app[appCount][0], s);
      strcpy(app[appCount][1], ps);
      appCount++;

    } while(!feof(f));

    fclose(f);
  }
  return 0;
}

int main(int argc, char* argv[])
{
  char appListName[] = "appList.txt";
  int ret = LoadList(appListName);
  if (ret!=0) return ret;

  // Start the sockets
  WSADATA info;
  if (WSAStartup(MAKEWORD(1,1), &info) == 0)
  {

    // Establish connection
    SOCKET s;
    if ((s = establish(PORTNUM)) != INVALID_SOCKET)
    {

      // Socket retrieving loop
      while (1)
      {

        // Accept the socket and if it's valid then process it and close it
        SOCKET new_sock = accept(s, NULL, NULL);
        if (s != INVALID_SOCKET)
        {
          int ret = LoadList(appListName);
          if (ret!=0) return ret;
          ProcessSocket(new_sock, app, appCount);
          closesocket(new_sock);
        }
      }

      // Close the communication socket
      closesocket(s);
    }

    // Finish with sockets
    WSACleanup();
  }

  // Destroy internal structure
  for (int i = 0; i < appCount; i++)
  {
    delete app[i][0];
    delete app[i][1];
  }

	return 0;
}
