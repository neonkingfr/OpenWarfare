// ExeCln.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <stdio.h>
#include <winsock.h>

SOCKET call_socket(const char *hostname, unsigned short portnum)
{ struct sockaddr_in sa;
  struct hostent     *hp;
  SOCKET s;

  hp = gethostbyname(hostname);
  if (hp == NULL) /* we don't know who this host is */
    return INVALID_SOCKET;

  memset(&sa,0,sizeof(sa));
  memcpy((char *)&sa.sin_addr, hp->h_addr, hp->h_length);   /* set address */
  sa.sin_family = hp->h_addrtype;
  sa.sin_port = htons((u_short)portnum);

  s = socket(hp->h_addrtype, SOCK_STREAM, 0);
  if (s == INVALID_SOCKET)
    return INVALID_SOCKET;

  /* try to connect to the specified socket */
  if (connect(s, (struct sockaddr *)&sa, sizeof sa) == SOCKET_ERROR) {
    closesocket(s);
    return INVALID_SOCKET;
  }
  return s;
}

#define BUFFER_LENGTH 4096

int main(int argc, char* argv[])
{
  WSADATA info;
  SOCKET s;

  // Determine number of actual parameters
  if (argc < 4)
  {
    MessageBox(NULL, "Not enough actual parameters.", "Startup", MB_OK);
    return 1;
  }

  /* initialize the socket library */
  if (WSAStartup(MAKELONG(1, 1), &info) == SOCKET_ERROR)
  {
    MessageBox(NULL, "Could not initialize socket library.",
               "Startup", MB_OK);
    return 1;
  }

  /* connect to the server */
  s = call_socket(argv[1], 50000);
  if (s == INVALID_SOCKET) {
    MessageBox(NULL, "Could not connect.", "Connect", MB_OK);
    return 1;
  }

  // Get the name of this client
  char myname[256];
  gethostname(myname, sizeof(myname));

  // Put command and argument in one array
  char buf[BUFFER_LENGTH];
  int dataLength = 0;
  {
    // Copy name of client station
    strcpy(&buf[0], myname);
    dataLength += strlen(myname) + 1;

    // Copy name of the server application to run
    strcpy(&buf[dataLength], argv[2]);
    dataLength += strlen(argv[2]) + 1;

    // Copy all application arguments
    for (int i = 3; i < argc; i++)
    {
      strcpy(&buf[dataLength], argv[i]);
      dataLength += strlen(argv[i]) + 1;
    }

    // Add one more zero at the end (an empty string) to tell the server we have finished
    buf[dataLength] = 0;
    dataLength += 1;
  }

  // Send the array
  int bw = send(s, buf, dataLength, 0);
  if (bw < dataLength)
  {
    printf("Error: Only %d out of %d bytes was sent", bw, dataLength);
  }

  closesocket(s);

  WSACleanup();

	return 0;
}
