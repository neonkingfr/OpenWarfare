#if !defined(AFX_DLGSESTAVASPRAVA_H__74A34619_B7B5_4DF4_98C5_54385BE4E4FC__INCLUDED_)
#define AFX_DLGSESTAVASPRAVA_H__74A34619_B7B5_4DF4_98C5_54385BE4E4FC__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DlgSestavaSprava.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// DlgSestavaSprava dialog



class DlgSestavaSprava : public CDialog
{
// Construction
   float _maxZapl;
public:
	void SetupTermin();
	DlgSestavaSprava(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(DlgSestavaSprava)
	enum { IDD = IDD_SPRAVASESTAVA };
	CButton	wBZaplatit;
	CListCtrl	wSeznamObjSuma;
	CListCtrl	wSeznamObj;
	CDateTimeCtrl	wNovyDatum;
	CListCtrl	wTerminyList;
	CEdit	wAktUziv;
	CEdit   wCastka;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(DlgSestavaSprava)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(DlgSestavaSprava)
	virtual BOOL OnInitDialog();
	afx_msg void OnBnastavitnovydatum();
	afx_msg void OnItemchangedTerminylist(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnItemchangedSeznamobj(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnBzaplatit();
	afx_msg void OnSestavaproobj();
	afx_msg void OnRozpis();
	afx_msg void OnKompletnivypis();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DLGSESTAVASPRAVA_H__74A34619_B7B5_4DF4_98C5_54385BE4E4FC__INCLUDED_)
