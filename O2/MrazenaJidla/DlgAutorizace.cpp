// DlgAutorizace.cpp : implementation file
//

#include "stdafx.h"
#include "MrazenaJidla.h"
#include "DlgAutorizace.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// DlgAutorizace dialog


DlgAutorizace::DlgAutorizace(CWnd* pParent /*=NULL*/)
	: CDialog(DlgAutorizace::IDD, pParent)
{
	//{{AFX_DATA_INIT(DlgAutorizace)
	vJmeno = _T("");
	vHeslo = _T("");
	//}}AFX_DATA_INIT
}


void DlgAutorizace::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(DlgAutorizace)
	DDX_Text(pDX, IDC_JMENO, vJmeno);
	DDX_Text(pDX, IDC_HESLO, vHeslo);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(DlgAutorizace, CDialog)
	//{{AFX_MSG_MAP(DlgAutorizace)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// DlgAutorizace message handlers
