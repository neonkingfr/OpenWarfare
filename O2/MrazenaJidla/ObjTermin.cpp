// ObjTermin.cpp: implementation of the ObjTermin class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "MrazenaJidla.h"
#include "ObjTermin.h"
#include "ArchiveExt.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

void ArchiveVariableExchange(IArchive &arch,Cenik &data)
  {
  ArchiveArrayExchange(arch,data);
  }

void ObjTermin::DataExchange(IArchive &arch)
  {
  if (arch.Check("MJ_OBJTERMIN")==false && arch.RequestVer(FILEVERSIONS)==false) 
	{
	arch.SetError(-1);return;
	}
  arch("Cenik",_cenik);
  arch("DeadLine",_deadline.asLong);  
  }
