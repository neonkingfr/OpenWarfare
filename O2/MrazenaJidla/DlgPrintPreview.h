#if !defined(AFX_DLGPRINTPREVIEW_H__5CF526CE_A3FD_4B37_9E35_C100FE6064E1__INCLUDED_)
#define AFX_DLGPRINTPREVIEW_H__5CF526CE_A3FD_4B37_9E35_C100FE6064E1__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DlgPrintPreview.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// DlgPrintPreview dialog

#include "CtiCenikyBrowser.h"


class DlgPrintPreview : public CDialog
{
// Construction
	CCtiCenikyBrowser _browser;
public:
	CString vPrintReportFile;
	DlgPrintPreview(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(DlgPrintPreview)
	enum { IDD = IDD_PRINTPREVIEW };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(DlgPrintPreview)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(DlgPrintPreview)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DLGPRINTPREVIEW_H__5CF526CE_A3FD_4B37_9E35_C100FE6064E1__INCLUDED_)
