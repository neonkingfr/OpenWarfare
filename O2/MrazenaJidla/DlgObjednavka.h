// DlgObjednavka.h : header file
//

#if !defined(AFX_DLGOBJEDNAVKA_H__684A6730_5D12_4450_9CD2_25511F7FF4BD__INCLUDED_)
#define AFX_DLGOBJEDNAVKA_H__684A6730_5D12_4450_9CD2_25511F7FF4BD__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

/////////////////////////////////////////////////////////////////////////////
// DlgObjednavka dialog

#include "UzivatelskaObj.h"
#include "ObjTermin.h"

class DlgObjednavka : public CDialog
{
// Construction
	UzivatelskaObj *_curobj;
	unsigned char _heslo[16];
	CString _curuserfile;
	ObjTermin aktTerm;
	int _lastTermin;
	bool _disabledPage;
public:
	void OnCancel();
	void OnOK();
	void UpdateCalcs();
	void ObjednejSiKs(int pocet);
	void DialogRules();
	void UpdateList();
	void LoadObjednavku(const char *termin);
	void LoadTerminy();
	void Reset();
	DlgObjednavka(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	//{{AFX_DATA(DlgObjednavka)
	enum { IDD = IDD_MRAZENAJIDLA_DIALOG };
	CButton	wSubmit;
	CButton	wBZrusit;
	CButton	wBObjednat;
	CEdit	wPocetKusu;
	CListCtrl	wNabidka;
	CComboBox wTerminyList;
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(DlgObjednavka)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(DlgObjednavka)
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg LRESULT OnStartDlg(WPARAM wParam, LPARAM lParam);
	afx_msg void OnDestroy();
	afx_msg void OnSelchangeTerminylist();
	afx_msg void OnItemchangedNabidka(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnDblclkNabidka(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnBobjednat();
	afx_msg void OnBzrusit();
	afx_msg void OnEndlabeleditNabidka(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnBnastaveni();
	afx_msg void OnSubmit();
	afx_msg void OnObjsestavit();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////
//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DLGOBJEDNAVKA_H__684A6730_5D12_4450_9CD2_25511F7FF4BD__INCLUDED_)
