// enikItem.cpp: implementation of the CenikItem class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "MrazenaJidla.h"
#include "CenikItem.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CenikItem::CenikItem()
{
id=name=NULL;
}

CenikItem::~CenikItem()
{
free(id);
free(name);
}

CenikItem::CenikItem(const CenikItem &other)
  {
  memcpy(this,&other,sizeof(other));
  id=NULL;SetID(other.id);
  name=NULL;SetName(other.name);
  }

CenikItem &CenikItem::operator=(const CenikItem &other)
  {
  CenikItem::~CenikItem();
  memcpy(this,&other,sizeof(other));
  id=NULL;SetID(other.id);
  name=NULL;SetName(other.name);
  return *this;
  }


static const char *Hledani(const char *from, const char *&maskout)
  {
  const char *maska=maskout;
  while (*maskout=='[')
	{
	while (*maskout && *maskout!=']') 
	  {
	  if (*maskout=='\\') maskout++;
	  maskout++;
	  }
	maskout++;
	}
  while (*from)
	{
	const char *searcher=maska;
	int pos2=0;
	while (*searcher && *searcher!='*' && *searcher!='%')
	  {
	  while (*searcher=='[')
		{
		int pos=0;
		searcher++;
		while (*searcher && *searcher!=']' )
		  {
		  if (*searcher=='\\')
			searcher++;
  		 if (toupper(*searcher)!=toupper(from[pos])) 
		   {pos=-1;break;}
		  searcher++;
		  pos++;
		  }
		if (pos!=-1) return NULL;
		if (*searcher==0) return NULL;
		searcher=strchr(searcher,']');
		if (searcher==NULL) return NULL;
		searcher++;
		}
	  if (*searcher=='\\') searcher++;
	  if (toupper(*searcher)!=toupper(from[pos2])) {pos2=-1;break;}
	  searcher++;
	  pos2++;
	  }
	if (pos2!=-1) 
	  return from;
	from++;
	}
  return NULL;
  }

static void RemoveTags(char *data)
  {
  char *p1,*p2;
  bool ignore=false;
  bool space=true;
  p1=data;
  p2=data;
  while (*p1)
	{
	if (ignore)
	  {
	  if (*p1=='>') ignore=false;
	  }
	else
	  {
	  if (*p1=='<') ignore=true;
	  else
		{
		if (*p1>=0 && isspace(*p1))
		  {
		  if (!space) *p2++=' ';
		  space=true;
		  }
		else
		  {
		  space=false;			
		  if (strncmp(p1,"&nbsp;",6)==0)
			{
			*p2++=32;
			p1+=5;
			}
		  else if (strncmp(p1,"&lt;",4)==0)
			{
			*p2++='<';
			p1+=3;
			}
		  else if (strncmp(p1,"&gt;",4)==0)
			{
			*p2++='>';
			p1+=3;
			}
		  else 
			{
			*p2++=*p1;
			}
		  }
		}
	  }
	p1++;	  
	}
  while (p2>data && isspace(p2[-1])) p2--;
  *p2=0;
  }

static const char *TeckaCarka(char *buff)
  {
  char *c;
  while ((c=strchr(buff,','))!=NULL) *c='.';
  return buff;
  }

static bool CtiItemPodleMasky(const char *buffer, const char *maska, CenikItem &item)
  {
  char zero=0;
  ArchiveStreamMemoryOut bufsave;
  while (*buffer && *maska)
	{
	if (*maska=='%')
	  {
	  char mode=maska[1];
	  maska+=2;
	  const char *till=Hledani(buffer,maska);
	  if (till==NULL) return false;
	  bufsave.Rewind();
	  bufsave.DataExchange((void *)buffer,till-buffer);
	  bufsave.ExSimple(zero);
	  char *data=bufsave.GetBuffer();
	  RemoveTags(data);
	  switch (mode)
		{
		case 'i': item.SetID(data);break;
		case 'n': item.SetName(data);break;
		case 'g': if (sscanf(data,"%d",&item.hmotnost)!=1) return false;break;
		case 'c': if (sscanf(TeckaCarka(data),"%f",&item.cena)!=1) return false;break;
		case 'd': if (sscanf(TeckaCarka(data),"%f",&item.cenadph)!=1) return false;break;
		}
	  buffer=till;
	  }
	else if (*maska=='*')
	  {
	  maska++;
	  buffer=Hledani(buffer,maska);
	  if (buffer==NULL) return false;
	  }
	else if (*maska=='\\')
	  {
	  maska++;
	  if (toupper(*buffer)!=toupper(*maska)) return false;
	  maska++;
	  buffer++;
	  }
	else 
	  {
	  if (toupper(*buffer)!=toupper(*maska)) return false;
	  maska++;
	  buffer++;
	  }
	}
  return *maska==0;
  }

static int QSortCompare( const void *v0, const void *v1 )
  {
  const CenikItem *it1=(const CenikItem *)v0;
  const CenikItem *it2=(const CenikItem *)v1;
  return strcmp(it1->id,it2->id);
  }

static void CtiCenikyZBufferu(char *buffer, const char *maska, Cenik &cenik)
  {
  int len=strlen(buffer);
  CenikItem item;
  for (int i=len-1;i>=0;i--)
	{
	if (CtiItemPodleMasky(buffer+i,maska,item)==true)	  
	  {
	  cenik.Append()=item;	  
	  buffer[i]=0;
	  }
	}
  cenik.QSortBin(QSortCompare);
  for (int i=0;i<cenik.Size()-1;i++)
	{
	if (strcmp(cenik[i].id,cenik[i+1].id)==0)
	  {
	  cenik.Delete(i+1);
	  i--;
	  }
	}
  }

bool CtiCenikyURL(const char *url, const char *maska, Cenik &cenik)
  {
  char zero=0;
  CInternetSession session;
  CStdioFile *page=session.OpenURL(url);
  if (page==NULL) return false;
  ArchiveStreamMemoryOut buffer;
  char block[256];
  UINT size;
  while ((size=page->Read(block,256))!=0)
	buffer.DataExchange(block,size);
  buffer.DataExchange(&zero,1);
  CtiCenikyZBufferu(buffer.GetBuffer(),maska,cenik);
  page->Close();
  delete page;
  return true;
  }