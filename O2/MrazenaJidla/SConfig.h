// SConfig.h: interface for the SConfig class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_SCONFIG_H__6EF5B25D_BD1B_4B2F_8A62_F30AA9AF58B3__INCLUDED_)
#define AFX_SCONFIG_H__6EF5B25D_BD1B_4B2F_8A62_F30AA9AF58B3__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

struct SConfig  
{
  CString _serverPath;
  CString _cenikyURL;
  float _priplatek;
  float _doprava;
  void DataExchange(IArchive &arch);

  void Load();
  void Save();

};


extern SConfig GConfig;

#endif // !defined(AFX_SCONFIG_H__6EF5B25D_BD1B_4B2F_8A62_F30AA9AF58B3__INCLUDED_)
