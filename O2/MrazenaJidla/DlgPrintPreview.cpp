// DlgPrintPreview.cpp : implementation file
//

#include "stdafx.h"
#include "MrazenaJidla.h"
#include "DlgPrintPreview.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// DlgPrintPreview dialog


DlgPrintPreview::DlgPrintPreview(CWnd* pParent /*=NULL*/)
	: CDialog(DlgPrintPreview::IDD, pParent)
{
	//{{AFX_DATA_INIT(DlgPrintPreview)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void DlgPrintPreview::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(DlgPrintPreview)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(DlgPrintPreview, CDialog)
	//{{AFX_MSG_MAP(DlgPrintPreview)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// DlgPrintPreview message handlers

BOOL DlgPrintPreview::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	_browser.Create(this,IDC_HTMLBROWSER);
	_browser.Navigate2(vPrintReportFile);

	
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void DlgPrintPreview::OnOK() 
  {
  CWnd &wnd2=*_browser.GetWindow(GW_CHILD);
  CWnd &wnd3=*wnd2.GetWindow(GW_CHILD);
  CWnd &wnd1=*wnd3.GetWindow(GW_CHILD);
  wnd1.PostMessage(WM_COMMAND,MAKEWPARAM(27,1),0);
  }
