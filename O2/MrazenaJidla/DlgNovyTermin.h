#if !defined(AFX_DLGNOVYTERMIN_H__1C455596_0464_46A3_9445_4973AECBE634__INCLUDED_)
#define AFX_DLGNOVYTERMIN_H__1C455596_0464_46A3_9445_4973AECBE634__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DlgNovyTermin.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// DlgNovyTermin dialog

class ObjTermin;

class DlgNovyTermin : public CDialog, public MassProcess
{
// Construction
public:
	void UpdateDialogData(ObjTermin *term);
	DlgNovyTermin(CWnd* pParent = NULL);   // standard constructor

	ObjTermin *cur;

// Dialog Data
	//{{AFX_DATA(DlgNovyTermin)
	enum { IDD = IDD_NOVYTERMIN };
	CComboBox	wTerminList;
	CListCtrl	wCenikList;
	CDateTimeCtrl	wSetTermin;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(DlgNovyTermin)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

	virtual bool ProcessFile(const char *filename);

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(DlgNovyTermin)
	virtual BOOL OnInitDialog();
	afx_msg void OnDestroy();
	virtual void OnOK();
	afx_msg void OnReadcenik();
	afx_msg void OnSelchangeTermin();
	afx_msg void OnDeletetermin();
	afx_msg void OnDatetimechangeSettermin(NMHDR* pNMHDR, LRESULT* pResult);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DLGNOVYTERMIN_H__1C455596_0464_46A3_9445_4973AECBE634__INCLUDED_)
