// UzivatelskaObj.cpp: implementation of the UzivatelskaObj class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "MrazenaJidla.h"
#include "UzivatelskaObj.h"
#include "ArchiveExt.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

template <>
void ArchiveVariableExchange(IArchive &arch,UzivatelskaObjItem &data)
  {
  data.DataExchange(arch);
  }

UzivatelskaObjItem::UzivatelskaObjItem()
{
_kod=NULL;
_pockus=0;
}

UzivatelskaObjItem::~UzivatelskaObjItem()
{ 
free(_kod);
}

void UzivatelskaObjItem::DataExchange(IArchive &arch)
  {
  arch("ID",_kod);
  arch("PocetKusu",_pockus);
  }

int UzivatelskaObj::Add(const char *id, int pockus)
  {
  int i;
  SetDirty();
  for (i=0;i<Size();i++)
	if (stricmp(id,(*this)[i]._kod)==0)
	  {(*this)[i].Objednat(pockus);return  (*this)[i]._pockus;}
  Access(i);
  (*this)[i].SetKod(id);
  (*this)[i].Objednat(pockus);
  return (*this)[i]._pockus;
  }

void UzivatelskaObj::Reset(const char *id)
  {
  int i;
  for (i=0;i<Size();i++)
	if (stricmp(id,(*this)[i]._kod)==0)
	  {this->Delete(i);break;}
	SetDirty();
  }

void UzivatelskaObj::ResetAll()
  {
  Clear();
  SetDirty();
  }

int UzivatelskaObj::Find(const char *id)
  {
  int i;
  for (i=0;i<Size();i++)
	if (stricmp(id,(*this)[i]._kod)==0)
	  return (*this)[i]._pockus;
  return 0;
  }

int UzivatelskaObj::CalcPocetKusu()
  {
  int s=0;
  for (int i=0;i<Size();i++)
	s+=(*this)[i]._pockus;
  return s;
  }

float UzivatelskaObj::CalcCena(bool DPH, Cenik &cenik)
  {
  float cena=0;
  for (int i=0;i<cenik.Size();i++)
	{
	float celkcena=DPH?cenik[i].cenadph:cenik[i].cena;
	celkcena*=Find(cenik[i].id);
	cena+=celkcena;
	}
  return cena;
  }

void UzivatelskaObj::SetDirty()
  {
  _dirty=true;
  }

bool UzivatelskaObj::Save()
  {
  ArchiveStreamFileMappingOut file(CreateFile(_filename,GENERIC_WRITE|GENERIC_READ,FILE_SHARE_READ,NULL,CREATE_ALWAYS,0,NULL),true);
  if (file.IsError()) return false;
  ArchiveSimpleBinary arch(file);
  DataExchange(arch);
  _dirty=!arch;
  return !_dirty;
  }

bool UzivatelskaObj::Load(const char *filename)
  {  
  _dirty=false;
  _filename=filename;
  ArchiveStreamFileMappingIn file(CreateFile(filename,GENERIC_READ,FILE_SHARE_READ,NULL,OPEN_EXISTING,0,NULL),true);
  if (file.IsError()) return false;
  ArchiveSimpleBinary arch(file);
  DataExchange(arch);
  return !(!arch);
  }
  
void UzivatelskaObj::DataExchange(IArchive &arch)
  {
  if (arch.Check("UZIVATELSKA_OBJEDNAVKA")==false || arch.RequestVer(FILEVERSIONS)==false)
	{
	arch.SetError(-1);
	return;
	}
  arch.Binary("Password",_heslo,sizeof(_heslo));
  ArchiveArrayExchange(arch,*this);
  if (arch.IsVer(FILEVERSIONS))
    arch("Zaplaceno",_zaplaceno);
  else if (-arch) _zaplaceno=0;
  }
