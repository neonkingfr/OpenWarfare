// DlgSestavaSprava.cpp : implementation file
//

#include "stdafx.h"
#include "mrazenajidla.h"
#include "DlgSestavaSprava.h"
#include "ObjTermin.h"
#include "SConfig.h"
#include "UzivatelskaObj.h"
#include <fstream>

using namespace std;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// DlgSestavaSprava dialog

#ifndef ListView_SetCheckState
   #define ListView_SetCheckState(hwndLV, i, fCheck) \
      ListView_SetItemState(hwndLV, i, \
      INDEXTOSTATEIMAGEMASK((fCheck)+1), LVIS_STATEIMAGEMASK)
#endif

DlgSestavaSprava::DlgSestavaSprava(CWnd* pParent /*=NULL*/)
	: CDialog(DlgSestavaSprava::IDD, pParent)
{
	//{{AFX_DATA_INIT(DlgSestavaSprava)
	//}}AFX_DATA_INIT
}


void DlgSestavaSprava::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(DlgSestavaSprava)
	DDX_Control(pDX, IDC_BZAPLATIT, wBZaplatit);
	DDX_Control(pDX, IDC_SEZNAMOBJSUMA, wSeznamObjSuma);
	DDX_Control(pDX, IDC_SEZNAMOBJ, wSeznamObj);
	DDX_Control(pDX, IDC_NOVYDATUM, wNovyDatum);
	DDX_Control(pDX, IDC_TERMINYLIST, wTerminyList);
	DDX_Control(pDX, IDC_AKTUZIV, wAktUziv);
	DDX_Control(pDX, IDC_CASTKA, wCastka);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(DlgSestavaSprava, CDialog)
	//{{AFX_MSG_MAP(DlgSestavaSprava)
	ON_BN_CLICKED(IDC_BNASTAVITNOVYDATUM, OnBnastavitnovydatum)
	ON_NOTIFY(LVN_ITEMCHANGED, IDC_TERMINYLIST, OnItemchangedTerminylist)
	ON_WM_TIMER()
	ON_NOTIFY(LVN_ITEMCHANGED, IDC_SEZNAMOBJ, OnItemchangedSeznamobj)
	ON_BN_CLICKED(IDC_BZAPLATIT, OnBzaplatit)
	ON_BN_CLICKED(IDC_SESTAVAPROOBJ, OnSestavaproobj)
	ON_BN_CLICKED(IDC_ROZPIS, OnRozpis)
	ON_BN_CLICKED(IDC_KOMPLETNIVYPIS, OnKompletnivypis)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// DlgSestavaSprava message handlers

class DlgSestavaSpravaTerminyList:public MassProcess
      {
      public:
        CListCtrl &_list;
        DlgSestavaSpravaTerminyList(CListCtrl &list):_list(list)
          {
          list.DeleteAllItems();          
          }
        bool ProcessFile(const char *filename);
      };

static bool LoadObjTermin(ObjTermin &objterm, const char *filename)
  {  
  ArchiveStreamFileMappingIn file(CreateFile(filename,GENERIC_READ,FILE_SHARE_READ,NULL,OPEN_EXISTING,0,NULL),true);
  ArchiveSimpleBinary arch(file);
  if (!arch) return false;
  objterm.DataExchange(arch);
  if (!arch) return false;
  return true;
  }
static bool SaveObjTermin(ObjTermin &objterm, const char *filename)
  {  
  ArchiveStreamFileMappingOut file(CreateFile(filename,GENERIC_READ|GENERIC_WRITE,FILE_SHARE_READ,NULL,CREATE_ALWAYS,0,NULL),true);
  ArchiveSimpleBinary arch(file);
  if (!arch) return false;
  objterm.DataExchange(arch);
  if (!arch) return false;
  return true;
  }


bool DlgSestavaSpravaTerminyList::ProcessFile(const char *filename)
  {
  char buff[50];
  ObjTermin term;
  if (LoadObjTermin(term,filename)==false) return true;
  COleDateTime curtime=COleDateTime::GetCurrentTime();
  Date curdate;
  curdate.day=curtime.GetDay();
  curdate.month=curtime.GetMonth();
  curdate.year=curtime.GetYear();
  if (curdate.asLong<term._deadline.asLong) return true;
  sprintf(buff,"%08X",term._deadline.asLong);
  int p=_list.InsertItem(0,buff,0);
  sprintf(buff,"%d. %d. %d",term._deadline.day,term._deadline.month,term._deadline.year);
  _list.SetItemText(p,1,buff);
  _list.SetItemText(p,2,filename);
  return true;
  }

BOOL DlgSestavaSprava::OnInitDialog() 
{
	CDialog::OnInitDialog();
    
    CRect rc;
    wTerminyList.GetClientRect(&rc);

    wTerminyList.InsertColumn(0,"",LVCFMT_LEFT,0,0);
    wTerminyList.InsertColumn(1,"",LVCFMT_LEFT,rc.right-GetSystemMetrics(SM_CXVSCROLL)-2,1);
    wTerminyList.InsertColumn(2,"",LVCFMT_LEFT,0,2);

    wSeznamObj.InsertColumn(0,StrRes[IDS_TITLEUSERNAME],LVCFMT_LEFT,150,0);
    wSeznamObj.InsertColumn(1,StrRes[IDS_JIDLOPOCETOBJ],LVCFMT_CENTER,50,1);
    wSeznamObj.InsertColumn(2,StrRes[IDS_JIDLOCENADPH],LVCFMT_RIGHT,80,2);
    wSeznamObj.InsertColumn(3,StrRes[IDS_TITLEZAPLACENO],LVCFMT_RIGHT,100,3);
    wSeznamObj.InsertColumn(4,"",LVCFMT_LEFT,0,4);

    wSeznamObjSuma.InsertColumn(0,"",LVCFMT_LEFT,150,0);
    wSeznamObjSuma.InsertColumn(1,"",LVCFMT_CENTER,50,1);
    wSeznamObjSuma.InsertColumn(2,"",LVCFMT_RIGHT,80,2);
    wSeznamObjSuma.InsertColumn(3,"",LVCFMT_RIGHT,100,3);

    ListView_SetExtendedListViewStyleEx(wTerminyList,LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES,LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES);
    ListView_SetExtendedListViewStyleEx(wSeznamObj,LVS_EX_CHECKBOXES|LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES,LVS_EX_FULLROWSELECT|LVS_EX_CHECKBOXES|LVS_EX_GRIDLINES);
    ListView_SetExtendedListViewStyleEx(wSeznamObjSuma,LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES,LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES);
    
    DlgSestavaSpravaTerminyList readTerm(wTerminyList);
    readTerm.ProcessFolders(GConfig._serverPath,TERMININFOEXT,true);  

    SetTimer(100,2000,NULL);
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void DlgSestavaSprava::OnBnastavitnovydatum() 
  {
  int cursel=wTerminyList.GetNextItem(-1,LVNI_FOCUSED);
  if (cursel!=-1)
    {
    CString fname=wTerminyList.GetItemText(cursel,2);
    ObjTermin objterm;
    if (LoadObjTermin(objterm,fname)==true)
      {
      COleDateTime newdate;
      wNovyDatum.GetTime(newdate);
      objterm._deadline.day=newdate.GetDay();
      objterm._deadline.month=newdate.GetMonth();
      objterm._deadline.year=newdate.GetYear();
      if (SaveObjTermin(objterm,fname)==true)
        {
        DlgSestavaSpravaTerminyList readTerm(wTerminyList);
        readTerm.ProcessFolders(GConfig._serverPath,TERMININFOEXT,true);  
        return;
        }
      }
    }
  AfxMessageBox(IDS_NASTALANEJAKACHYBA,MB_ICONEXCLAMATION);
  }

class DlgSestavaSpravaUserList: public MassProcess
  {
  public:
    CListCtrl &_list;
    ObjTermin &_term;
    int pockusu;
    float cena;
    float zaplaceno;
    DlgSestavaSpravaUserList(CListCtrl &list,ObjTermin &term):_list(list),_term(term)
      {
      list.DeleteAllItems();     
      cena=zaplaceno=(float)(pockusu=0);
      }
    bool ProcessFile(const char *filename);
  };

bool DlgSestavaSpravaUserList::ProcessFile(const char *filename)
  {
  UzivatelskaObj obj;
  if (obj.Load(filename)==false) return true;
  const char *uname=Pathname::GetNameFromPath(filename);
  uname=strcpy((char *)alloca(strlen(uname)+1),uname);
  char *ext=const_cast<char *>(Pathname::GetExtensionFromPath(uname));
  *ext=0;
  int p=_list.InsertItem(0,uname,0);
  char buff[50];
  int poc=obj.CalcPocetKusu();
  float ccena=obj.CalcCena(true,_term._cenik)+poc*GConfig._priplatek;
  float zplc=obj.Zaplaceno();
  _list.SetItemText(p,1,itoa(poc,buff,10));
  sprintf(buff,"%.2f",ccena);
  _list.SetItemText(p,2,buff);
  sprintf(buff,"%.2f",zplc);
  _list.SetItemText(p,3,buff);
  _list.SetItemText(p,4,filename);
  ListView_SetCheckState(_list,p,1);
  pockusu+=poc;
  cena+=ccena;
  zaplaceno+=zplc;
  return true;
  }

void DlgSestavaSprava::SetupTermin()
  {  
  int cursel=wTerminyList.GetNextItem(-1,LVNI_SELECTED);
  if (cursel!=-1)
    {
    Pathname tfolder=(LPCTSTR)wTerminyList.GetItemText(cursel,2);
    ObjTermin term;
    if (LoadObjTermin(term,tfolder))
      {
      wNovyDatum.SetTime(COleDateTime(term._deadline.year,term._deadline.month,term._deadline.day,0,0,0));

      DlgSestavaSpravaUserList ulist(wSeznamObj,term);
      ulist.ProcessFolders(tfolder.GetDirectoryWithDrive(),"*"OBJEDNAVKAEXT,false);

      char buff[50];
      wSeznamObjSuma.DeleteAllItems();
      wSeznamObjSuma.InsertItem(0,StrRes[IDS_CELKEM],0);
      wSeznamObjSuma.SetItemText(0,1,itoa(ulist.pockusu,buff,10));
      sprintf(buff,"%.2f",ulist.cena);
      wSeznamObjSuma.SetItemText(0,2,buff);
      sprintf(buff,"%.2f",ulist.zaplaceno);
      wSeznamObjSuma.SetItemText(0,3,buff);
      return;
      }
    }  
  AfxMessageBox(IDS_NASTALANEJAKACHYBA,MB_ICONEXCLAMATION);
  } 

void DlgSestavaSprava::OnItemchangedTerminylist(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_LISTVIEW* pNMListView = (NM_LISTVIEW*)pNMHDR;
	// TODO: Add your control notification handler code here
	if (pNMListView->uNewState & 1) SetupTermin();
	*pResult = 0;
}

void DlgSestavaSprava::OnTimer(UINT nIDEvent) 
{
    for (int i=0;i<4;i++)
  	  wSeznamObjSuma.SetColumnWidth(i,wSeznamObj.GetColumnWidth(i));
  	
	CDialog::OnTimer(nIDEvent);
}

void DlgSestavaSprava::OnItemchangedSeznamobj(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_LISTVIEW* pNMListView = (NM_LISTVIEW*)pNMHDR;
	if (pNMListView->uNewState & 1) 
      {
      int cursel=wSeznamObj.GetNextItem(-1,LVNI_SELECTED);
      if (cursel!=-1)
        {
        CString s=wSeznamObj.GetItemText(cursel,2);
        float cena,zplc;
        sscanf(s,"%f",&cena);
        s=wSeznamObj.GetItemText(cursel,3);
        sscanf(s,"%f",&zplc);
        s=wSeznamObj.GetItemText(cursel,0);
        wAktUziv.SetWindowText(s);
        s.Format("%.2f",_maxZapl=cena-zplc);
        wCastka.SetWindowText(s);
        wCastka.EnableWindow(true);
        wBZaplatit.EnableWindow(true);
        /*if (cena>zplc) */return;
        }
      }

    wCastka.EnableWindow(false);
    wBZaplatit.EnableWindow(false);
	
	*pResult = 0;
}

void DlgSestavaSprava::OnBzaplatit() 
{
    
	UzivatelskaObj objd;
    CString s;
    wCastka.GetWindowText(s);
    float cena=0;
    if (sscanf(s,"%f",&cena)==1)
      {           
      int cursel=wSeznamObj.GetNextItem(-1,LVNI_SELECTED);
      s=wSeznamObj.GetItemText(cursel,4);
      if (objd.Load(s)==true)
        {
        if (cena>=_maxZapl) cena=_maxZapl;
        objd.Zaplatit(cena);
        objd.Save();
        wCastka.EnableWindow(false);
        wBZaplatit.EnableWindow(false);
        SetupTermin();
        return;
        }
      }

  AfxMessageBox(IDS_NASTALANEJAKACHYBA,MB_ICONEXCLAMATION);	
}

static void PripravaSestavy(Pathname &cesta, ofstream &echo)
  {
  cesta.SetTempDirectory();  
  cesta.SetFilename("sestava.htm");
/*  echo.
  echo*/
  }

void DlgSestavaSprava::OnSestavaproobj() 
  {
  Pathname tmp;

  }

void DlgSestavaSprava::OnRozpis() 
{
	// TODO: Add your control notification handler code here
	
}

void DlgSestavaSprava::OnKompletnivypis() 
{
	// TODO: Add your control notification handler code here
	
}
