// DlgNovyTermin.cpp : implementation file
//

#include "stdafx.h"
#include "MrazenaJidla.h"
#include "DlgNovyTermin.h"
#include "DlgCtiCeniky.h"
#include "ObjTermin.h"
#include "SConfig.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// DlgNovyTermin dialog


DlgNovyTermin::DlgNovyTermin(CWnd* pParent /*=NULL*/)
	: CDialog(DlgNovyTermin::IDD, pParent)
{
	//{{AFX_DATA_INIT(DlgNovyTermin)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void DlgNovyTermin::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(DlgNovyTermin)
	DDX_Control(pDX, IDC_TERMIN, wTerminList);
	DDX_Control(pDX, IDC_CENIKY, wCenikList);
	DDX_Control(pDX, IDC_SETTERMIN, wSetTermin);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(DlgNovyTermin, CDialog)
	//{{AFX_MSG_MAP(DlgNovyTermin)
	ON_WM_DESTROY()
	ON_BN_CLICKED(IDC_READCENIK, OnReadcenik)
	ON_CBN_SELCHANGE(IDC_TERMIN, OnSelchangeTermin)
	ON_BN_CLICKED(IDC_DELETETERMIN, OnDeletetermin)
	ON_NOTIFY(DTN_DATETIMECHANGE, IDC_SETTERMIN, OnDatetimechangeSettermin)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// DlgNovyTermin message handlers

bool DlgNovyTermin::ProcessFile(const char *filename)
  {
  ArchiveStreamFileMappingIn file(CreateFile(filename,GENERIC_READ,FILE_SHARE_READ,NULL,OPEN_EXISTING,FILE_FLAG_SEQUENTIAL_SCAN,NULL),true);
  if (file.IsError()) return true;
  ArchiveSimpleBinary arch(file);
  ObjTermin *newobjterm=new ObjTermin;
  newobjterm->DataExchange(arch);
  COleDateTime cur=COleDateTime::GetCurrentTime();
  Date testdate;
  testdate.year=cur.GetYear();
  testdate.month=cur.GetMonth();
  testdate.day=cur.GetDay();
  if (testdate.asLong<=newobjterm->_deadline.asLong)
	{
	char title[50];
	sprintf(title,"%d. %d. %d",newobjterm->_deadline.day,newobjterm->_deadline.month,newobjterm->_deadline.year);
	int pos=wTerminList.AddString(title);
	wTerminList.SetItemDataPtr(pos,newobjterm);
	char *z=strcpy((char *)alloca(strlen(filename)+1),filename);
	char *p=const_cast<char *>(Pathname::GetNameFromPath(z));
	p[-1]=0;
	newobjterm->_diskname=Pathname::GetNameFromPath(z);
	}
  else
	delete newobjterm;
  return true;
  }	

BOOL DlgNovyTermin::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	ProcessFolders(GConfig._serverPath,TERMININFOEXT ,true);
	int pos=wTerminList.AddString(StrRes[IDS_NOVYTERMIN]);
	ObjTermin *newobjterm=new ObjTermin;
	newobjterm->_deadline.asLong=0;
	wTerminList.SetItemDataPtr(pos,newobjterm);
	wTerminList.SetCurSel(pos);

	wCenikList.InsertColumn(0,StrRes[IDS_JIDLOID],LVCFMT_CENTER,50,0);
	wCenikList.InsertColumn(1,StrRes[IDS_JIDLOJMENO],LVCFMT_LEFT,200,1);
	wCenikList.InsertColumn(2,StrRes[IDS_JIDLOVAHA],LVCFMT_CENTER,50,2);
	wCenikList.InsertColumn(3,StrRes[IDS_JIDLOCENA],LVCFMT_RIGHT,50,3);
	wCenikList.InsertColumn(4,StrRes[IDS_JIDLOCENADPH],LVCFMT_RIGHT,50,4);
	
	UpdateDialogData(newobjterm);
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void DlgNovyTermin::OnDestroy() 
  {
	int cnt=wTerminList.GetCount();
	for (int i=0;i<cnt;i++)
	  {
	  ObjTermin *p=(ObjTermin *)wTerminList.GetItemDataPtr(i);
	  delete p;
	  }  
	CDialog::OnDestroy();
  }
	// TODO: Add your message handler code here
	


void DlgNovyTermin::OnOK() 
  {
	int cnt=wTerminList.GetCount();
	for (int i=0;i<cnt;i++)
	  {
	  ObjTermin *p=(ObjTermin *)wTerminList.GetItemDataPtr(i);
	  if (p->_cenik.Size() && p->_deadline.asLong)
		{
		if (p->_diskname=="") p->_diskname.Format("%d",time(NULL));
		CString dir=GConfig._serverPath;
		dir+=p->_diskname;
		CreateDirectory(dir,NULL);
		dir+="\\";
		dir+=TERMININFOEXT;
		ArchiveStreamFileMappingOut file(CreateFile(dir,GENERIC_WRITE|GENERIC_READ,FILE_SHARE_READ,NULL,CREATE_ALWAYS,FILE_FLAG_SEQUENTIAL_SCAN,NULL),true);
		ArchiveSimpleBinary arch(file);
		p->DataExchange(arch);
		}
	  }  	
  CDialog::OnOK();
  }

void DlgNovyTermin::UpdateDialogData(ObjTermin *term)
  {
  cur=term;
  if (cur==NULL || cur==(ObjTermin *)0xFFFFFFFF)
	{
	EndDialog(IDCANCEL);
	return;
	}
  if (cur->_deadline.asLong==0)
	{
	COleDateTime dnes=COleDateTime ::GetCurrentTime();
	wSetTermin.SetTime(dnes);
	}
  else
	{
	SYSTEMTIME stim;
	memset(&stim,0,sizeof(stim));
	stim.wYear=cur->_deadline.year;
	stim.wMonth=cur->_deadline.month;
	stim.wDay=cur->_deadline.day;
	wSetTermin.SetTime(&stim);
	}
  wCenikList.DeleteAllItems();
  for (int s=0;s<cur->_cenik.Size();s++)
	{
	char buff[40];
	int item=wCenikList.InsertItem(s,cur->_cenik[s].id);
	wCenikList.SetItemText(item,1,cur->_cenik[s].name);
	wCenikList.SetItemText(item,2,itoa(cur->_cenik[s].hmotnost,buff,10));
	sprintf(buff,"%.2f",cur->_cenik[s].cena);
	wCenikList.SetItemText(item,3,buff);
	sprintf(buff,"%.2f",cur->_cenik[s].cenadph);
	wCenikList.SetItemText(item,4,buff);
	}
  }

void DlgNovyTermin::OnReadcenik() 
  {
  DlgCtiCeniky dlgcti;
  dlgcti.cenik=&cur->_cenik;
  if (dlgcti.DoModal()==IDOK)
	{
	UpdateDialogData(cur);
	}
  }

void DlgNovyTermin::OnSelchangeTermin() 
  {
  int sel=wTerminList.GetCurSel();
  ObjTermin *term=(ObjTermin *)wTerminList.GetItemDataPtr(sel);
  UpdateDialogData(term);
  }


class MassDeleteFolder: public MassProcess
  {
  public:
	bool ProcessFile(const char *filename)
	  {
	  DeleteFile(filename);
	  return true;
	  }
  };

void DlgNovyTermin::OnDeletetermin() 
  {
  if (cur->_diskname!="")
	{
	if (AfxMessageBox(IDS_DELETETERMASK,MB_YESNO|MB_ICONQUESTION)==IDNO) return;
	MassDeleteFolder del;
	CString dir=GConfig._serverPath+cur->_diskname;
	del.ProcessFolders(dir,"*.*",false);
	RemoveDirectory(dir);
	}
  delete cur;
  wTerminList.DeleteString(wTerminList.GetCurSel());
  wTerminList.SetCurSel(0);
  ObjTermin *term=(ObjTermin *)wTerminList.GetItemDataPtr(0);
  UpdateDialogData(term);
  }




void DlgNovyTermin::OnDatetimechangeSettermin(NMHDR* pNMHDR, LRESULT* pResult) 
{
  SYSTEMTIME stim;
  Date sel;
  wSetTermin.GetTime(&stim);
  sel.day=(char)stim.wDay;
  sel.month=(char)stim.wMonth;
  sel.year=stim.wYear;  	
  COleDateTime curd=COleDateTime::GetCurrentTime();
  Date testdate;
  testdate.year=curd.GetYear();
  testdate.month=curd.GetMonth();
  testdate.day=curd.GetDay();
  *pResult = 0;
  cur->_deadline.asLong=sel.asLong;
}
