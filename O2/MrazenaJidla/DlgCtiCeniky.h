#if !defined(AFX_DLGCTICENIKY_H__590538FF_15F1_4FCD_98A9_2E4DDB11D748__INCLUDED_)
#define AFX_DLGCTICENIKY_H__590538FF_15F1_4FCD_98A9_2E4DDB11D748__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DlgCtiCeniky.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// DlgCtiCeniky dialog

#include "CtiCenikyBrowser.h"
#include "CenikItem.h"



class DlgCtiCeniky : public CDialog
{
// Construction
	CCtiCenikyBrowser _browser;
public:
	DlgCtiCeniky(CWnd* pParent = NULL);   // standard constructor

   Cenik *cenik;

// Dialog Data
	//{{AFX_DATA(DlgCtiCeniky)
	enum { IDD = IDD_CTICENIKY };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(DlgCtiCeniky)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(DlgCtiCeniky)
	virtual BOOL OnInitDialog();
	afx_msg LRESULT OnAddressUpdate(WPARAM wParam, LPARAM lParam);
	afx_msg void OnBack();
	afx_msg void OnCti();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DLGCTICENIKY_H__590538FF_15F1_4FCD_98A9_2E4DDB11D748__INCLUDED_)
