// DlgCtiCeniky.cpp : implementation file
//

#include "stdafx.h"
#include "MrazenaJidla.h"
#include "DlgCtiCeniky.h"
#include "CenikItem.h"
#include "SConfig.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define MASKA "<tr*>*[</tr>][</table>]<td*>%i[</tr>][</table>]<td*>%n[</tr>][</table>]<td*>%g[</tr>][</table>]<td*>%c[</tr>][</table>]<td*>%d</tr"

/////////////////////////////////////////////////////////////////////////////
// DlgCtiCeniky dialog


DlgCtiCeniky::DlgCtiCeniky(CWnd* pParent /*=NULL*/)
	: CDialog(DlgCtiCeniky::IDD, pParent)
{
	//{{AFX_DATA_INIT(DlgCtiCeniky)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void DlgCtiCeniky::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(DlgCtiCeniky)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(DlgCtiCeniky, CDialog)
	//{{AFX_MSG_MAP(DlgCtiCeniky)
	ON_MESSAGE(BM_ADDRESSUPDATE,OnAddressUpdate)
	ON_BN_CLICKED(IDC_BACK, OnBack)
	ON_BN_CLICKED(IDC_CTI, OnCti)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// DlgCtiCeniky message handlers

BOOL DlgCtiCeniky::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	_browser.Create(this,IDC_HTMLBROWSER);
	_browser.Navigate2(GConfig._cenikyURL);

	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

LRESULT DlgCtiCeniky::OnAddressUpdate(WPARAM wParam, LPARAM lParam)
  {
  const char *address=(const char *)lParam;
  SetDlgItemText(IDC_ADRESA,address);
  return 0;
  }

void DlgCtiCeniky::OnBack() 
  {
  _browser.GoBack();
  }

BOOL DlgCtiCeniky::PreTranslateMessage(MSG* pMsg) 
  {
  if (GetFocus()==GetDlgItem(IDC_ADRESA) && pMsg->message==WM_KEYDOWN && pMsg->wParam==VK_RETURN)
	{
	CString adr;
	GetDlgItemText(IDC_ADRESA,adr);
	_browser.Navigate2(adr);
	return TRUE;
	}
  return CDialog::PreTranslateMessage(pMsg);
  }

void DlgCtiCeniky::OnCti() 
  {  
  CString url;
  GetDlgItemText(IDC_ADRESA,url);
  const char *mask=MASKA;
  cenik->Clear();
  CtiCenikyURL(url,mask,*cenik);
  EndDialog(IDOK);
  }
