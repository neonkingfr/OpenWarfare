// DlgAdminPage.cpp : implementation file
//

#include "stdafx.h"
#include "MrazenaJidla.h"
#include "DlgAdminPage.h"
#include "DlgConfig.h"
#include "SConfig.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// DlgAdminPage dialog


DlgAdminPage::DlgAdminPage(CWnd* pParent /*=NULL*/)
	: CDialog(DlgAdminPage::IDD, pParent)
{
	//{{AFX_DATA_INIT(DlgAdminPage)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void DlgAdminPage::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(DlgAdminPage)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(DlgAdminPage, CDialog)
	//{{AFX_MSG_MAP(DlgAdminPage)
	ON_BN_CLICKED(IDC_NEWTERMIN, OnNewtermin)
	ON_BN_CLICKED(IDC_SESTAVY, OnSestavy)
	ON_BN_CLICKED(IDC_OBJEDNATSI, OnObjednatsi)
	ON_BN_CLICKED(IDC_OPENCONFIG, OnOpenconfig)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// DlgAdminPage message handlers

void DlgAdminPage::OnNewtermin() 
{
EndDialog(IDC_NEWTERMIN);	
}

void DlgAdminPage::OnSestavy() 
{
EndDialog(IDC_SESTAVY);	
}

void DlgAdminPage::OnObjednatsi() 
{
EndDialog(IDC_OBJEDNATSI);
}

void DlgAdminPage::OnOK()
{

}

void DlgAdminPage::OnOpenconfig() 
{
DlgConfig dlg;
GConfig.Load();
dlg.vServerPath=GConfig._serverPath;
dlg.vUrlCenik=GConfig._cenikyURL;
dlg.vPriplatek=GConfig._priplatek;
dlg.vDoprava=GConfig._doprava;
if (dlg.DoModal()==IDOK)
  {
  GConfig._cenikyURL=dlg.vUrlCenik;
  GConfig._serverPath=dlg.vServerPath;
  GConfig._doprava=dlg.vDoprava;
  GConfig._priplatek=dlg.vPriplatek;
  GConfig.Save();  
  }
}
