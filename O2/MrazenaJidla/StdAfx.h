// stdafx.h : include file for standard system include files,
//  or project specific include files that are used frequently, but
//      are changed infrequently
//

#if !defined(AFX_STDAFX_H__3DDA6134_13DE_41D2_8969_5CC3DECA3C71__INCLUDED_)
#define AFX_STDAFX_H__3DDA6134_13DE_41D2_8969_5CC3DECA3C71__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#define VC_EXTRALEAN		// Exclude rarely-used stuff from Windows headers

#include <afxwin.h>         // MFC core and standard components
#include <afxext.h>         // MFC extensions
#include <afxdtctl.h>		// MFC support for Internet Explorer 4 Common Controls
#ifndef _AFX_NO_AFXCMN_SUPPORT
#include <afxcmn.h>			// MFC support for Windows Common Controls
#endif // _AFX_NO_AFXCMN_SUPPORT
#include <projects/bredy.libs/Archive/ArchiveStreamMemory.h>
#include <projects/bredy.libs/Archive/ArchiveStreamFileMapping.h>
#include <projects/bredy.libs/Archive/ArchiveStreamWindowsFile.h>
#include <projects/bredy.libs/Archive/IArchive.h>
#include <projects/bredy.libs/Archive/Archive.h>
#include <projects/bredy.libs/Archive/ArchiveStreamBase64Filter.h>
#include <El/Pathname/Pathname.h>
#include <projects/Bredy.Libs/StrRes/StringRes.h>
#include <afxinet.h>
#include "../Objektiv2/NewMat/MassProcess.h"

#define FILEVERSIONS 0x101

extern CStringRes StrRes;


#define TERMININFOEXT "termin.nfo"
#define OBJEDNAVKAEXT ".usr"

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_STDAFX_H__3DDA6134_13DE_41D2_8969_5CC3DECA3C71__INCLUDED_)
