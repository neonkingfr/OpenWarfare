#if !defined(AFX_DLGADMINPAGE_H__4A0E4FF2_1B05_4CE5_9874_840ECCCAA867__INCLUDED_)
#define AFX_DLGADMINPAGE_H__4A0E4FF2_1B05_4CE5_9874_840ECCCAA867__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DlgAdminPage.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// DlgAdminPage dialog

class DlgAdminPage : public CDialog
{
// Construction
public:
	void OnOK();
	DlgAdminPage(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(DlgAdminPage)
	enum { IDD = IDD_ADMINPAGE };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(DlgAdminPage)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(DlgAdminPage)
	afx_msg void OnNewtermin();
	afx_msg void OnSestavy();
	afx_msg void OnObjednatsi();
	afx_msg void OnOpenconfig();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DLGADMINPAGE_H__4A0E4FF2_1B05_4CE5_9874_840ECCCAA867__INCLUDED_)
