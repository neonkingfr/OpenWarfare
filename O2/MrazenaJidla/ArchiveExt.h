template <class T, class P>
void ArchiveArrayExchange(IArchive &arch,AutoArray<T,P> &data)
  {
  int s=data.Size();
  arch("ItemCount",s);
  if (-arch) 
	{
	data.Clear();
	data.Access(s-1);
	}
  for (int i=0;i<s;i++)
	arch("Item",data[i]);
  }

template <>
inline void ArchiveVariableExchange(IArchive &arch,CString &data)
  {
  if (+arch)
	{
	char *c=data.LockBuffer();
	arch(c);
	data.UnlockBuffer();
	}
  else
	{
	char *c=NULL;
	arch(c);
	data=c;
	delete [] c;
	}
  }