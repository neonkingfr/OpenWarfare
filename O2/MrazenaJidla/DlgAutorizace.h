#if !defined(AFX_DLGAUTORIZACE_H__A0519B89_959E_4900_BEC0_4D00E2E4D0C1__INCLUDED_)
#define AFX_DLGAUTORIZACE_H__A0519B89_959E_4900_BEC0_4D00E2E4D0C1__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DlgAutorizace.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// DlgAutorizace dialog

class DlgAutorizace : public CDialog
{
// Construction
public:
	DlgAutorizace(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(DlgAutorizace)
	enum { IDD = IDD_AUTORIZACE };
	CString	vJmeno;
	CString	vHeslo;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(DlgAutorizace)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(DlgAutorizace)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DLGAUTORIZACE_H__A0519B89_959E_4900_BEC0_4D00E2E4D0C1__INCLUDED_)
