// DlgConfig.cpp : implementation file
//

#include "stdafx.h"
#include "MrazenaJidla.h"
#include "DlgConfig.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// DlgConfig dialog


DlgConfig::DlgConfig(CWnd* pParent /*=NULL*/)
	: CDialog(DlgConfig::IDD, pParent)
{
	//{{AFX_DATA_INIT(DlgConfig)
	vServerPath = _T("");
	vUrlCenik = _T("");
	vDoprava = 0.0f;
	vPriplatek = 0.0f;
	//}}AFX_DATA_INIT
}


void DlgConfig::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(DlgConfig)
	DDX_Text(pDX, IDC_SERVERPATH, vServerPath);
	DDX_Text(pDX, IDC_URLCENIK, vUrlCenik);
	DDX_Text(pDX, IDC_DOPRAVA, vDoprava);
	DDX_Text(pDX, IDC_PRIPLATEK, vPriplatek);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(DlgConfig, CDialog)
	//{{AFX_MSG_MAP(DlgConfig)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// DlgConfig message handlers
