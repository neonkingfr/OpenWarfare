# Microsoft Developer Studio Project File - Name="MrazenaJidla" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Application" 0x0101

CFG=MrazenaJidla - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "MrazenaJidla.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "MrazenaJidla.mak" CFG="MrazenaJidla - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "MrazenaJidla - Win32 Release" (based on "Win32 (x86) Application")
!MESSAGE "MrazenaJidla - Win32 Debug" (based on "Win32 (x86) Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""$/Projects/MrazenaJidla", AJXAAAAA"
# PROP Scc_LocalPath "."
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "MrazenaJidla - Win32 Release"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 5
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MD /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /D "_AFXDLL" /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /MT /W3 /GX /O2 /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D "MFC_NEW" /Yu"stdafx.h" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x405 /d "NDEBUG" /d "_AFXDLL"
# ADD RSC /l 0x405 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /machine:I386
# ADD LINK32 /nologo /subsystem:windows /machine:I386

!ELSEIF  "$(CFG)" == "MrazenaJidla - Win32 Debug"

# PROP BASE Use_MFC 6
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 6
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MDd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /D "_AFXDLL" /Yu"stdafx.h" /FD /GZ /c
# ADD CPP /nologo /MDd /W3 /Gm /GX /ZI /Od /D "_DEBUG" /D "WIN32" /D "_WINDOWS" /D "_AFXDLL" /D "_MBCS" /D "MFC_NEW" /Yu"stdafx.h" /FD /GZ /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0x405 /d "_DEBUG" /d "_AFXDLL"
# ADD RSC /l 0x405 /d "_DEBUG" /d "_AFXDLL"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept
# ADD LINK32 /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept

!ENDIF 

# Begin Target

# Name "MrazenaJidla - Win32 Release"
# Name "MrazenaJidla - Win32 Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=.\MrazenaJidla.cpp
# End Source File
# Begin Source File

SOURCE=..\Bredy.Libs\archive\archivestream.cpp
# SUBTRACT CPP /YX /Yc /Yu
# End Source File
# Begin Source File

SOURCE=..\Bredy.Libs\archive\archivestreambase64filter.cpp
# SUBTRACT CPP /YX /Yc /Yu
# End Source File
# Begin Source File

SOURCE=..\Bredy.Libs\archive\archivestreamfilemapping.cpp
# SUBTRACT CPP /YX /Yc /Yu
# End Source File
# Begin Source File

SOURCE=..\Bredy.Libs\archive\archivestreammemory.cpp
# SUBTRACT CPP /YX /Yc /Yu
# End Source File
# Begin Source File

SOURCE=..\Bredy.Libs\archive\archivestreamwindowsfile.cpp
# SUBTRACT CPP /YX /Yc /Yu
# End Source File
# Begin Source File

SOURCE=.\CenikItem.cpp
# End Source File
# Begin Source File

SOURCE=.\CtiCenikyBrowser.cpp
# End Source File
# Begin Source File

SOURCE=.\DlgAdminPage.cpp
# End Source File
# Begin Source File

SOURCE=.\DlgAutorizace.cpp
# End Source File
# Begin Source File

SOURCE=.\DlgConfig.cpp
# End Source File
# Begin Source File

SOURCE=.\DlgCtiCeniky.cpp
# End Source File
# Begin Source File

SOURCE=.\DlgNovyTermin.cpp
# End Source File
# Begin Source File

SOURCE=.\DlgObjednavka.cpp
# End Source File
# Begin Source File

SOURCE=.\DlgPrintPreview.cpp
# End Source File
# Begin Source File

SOURCE=.\DlgSestavaSprava.cpp
# End Source File
# Begin Source File

SOURCE=.\DlgZmenaHesla.cpp
# End Source File
# Begin Source File

SOURCE=..\Objektiv2\NewMAT\MassProcess.cpp
# SUBTRACT CPP /YX /Yc /Yu
# End Source File
# Begin Source File

SOURCE=.\MD5.cpp
# SUBTRACT CPP /YX /Yc /Yu
# End Source File
# Begin Source File

SOURCE=..\Bredy.Libs\archive\minimal.cpp
# End Source File
# Begin Source File

SOURCE=.\MrazenaJidla.rc
# End Source File
# Begin Source File

SOURCE=.\ObjTermin.cpp
# End Source File
# Begin Source File

SOURCE=..\..\EL\Pathname\Pathname.cpp
# SUBTRACT CPP /YX /Yc /Yu
# End Source File
# Begin Source File

SOURCE=.\SConfig.cpp
# End Source File
# Begin Source File

SOURCE=.\StdAfx.cpp
# ADD CPP /Yc"stdafx.h"
# End Source File
# Begin Source File

SOURCE=..\Bredy.Libs\StrRes\StringRes.cpp
# SUBTRACT CPP /YX /Yc /Yu
# End Source File
# Begin Source File

SOURCE=.\UzivatelskaObj.cpp
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=..\Bredy.Libs\archive\archive.h
# End Source File
# Begin Source File

SOURCE=.\ArchiveExt.h
# End Source File
# Begin Source File

SOURCE=..\Bredy.Libs\archive\archivestream.h
# End Source File
# Begin Source File

SOURCE=..\Bredy.Libs\archive\archivestreambase64filter.h
# End Source File
# Begin Source File

SOURCE=..\Bredy.Libs\archive\archivestreamfilemapping.h
# End Source File
# Begin Source File

SOURCE=..\Bredy.Libs\archive\archivestreammemory.h
# End Source File
# Begin Source File

SOURCE=..\Bredy.Libs\archive\archivestreamwindowsfile.h
# End Source File
# Begin Source File

SOURCE=..\..\ES\Containers\array.hpp
# End Source File
# Begin Source File

SOURCE=..\Bredy.Libs\StrRes\BTree.h
# End Source File
# Begin Source File

SOURCE=.\CenikItem.h
# End Source File
# Begin Source File

SOURCE=.\CtiCenikyBrowser.h
# End Source File
# Begin Source File

SOURCE=.\DlgAdminPage.h
# End Source File
# Begin Source File

SOURCE=.\DlgAutorizace.h
# End Source File
# Begin Source File

SOURCE=.\DlgConfig.h
# End Source File
# Begin Source File

SOURCE=.\DlgCtiCeniky.h
# End Source File
# Begin Source File

SOURCE=.\DlgNovyTermin.h
# End Source File
# Begin Source File

SOURCE=.\DlgObjednavka.h
# End Source File
# Begin Source File

SOURCE=.\DlgPrintPreview.h
# End Source File
# Begin Source File

SOURCE=.\DlgSestavaSprava.h
# End Source File
# Begin Source File

SOURCE=.\DlgZmenaHesla.h
# End Source File
# Begin Source File

SOURCE=..\Bredy.Libs\archive\IArchive.h
# End Source File
# Begin Source File

SOURCE=..\Objektiv2\NewMAT\MassProcess.h
# End Source File
# Begin Source File

SOURCE=.\MD5.h
# End Source File
# Begin Source File

SOURCE=.\MrazenaJidla.h
# End Source File
# Begin Source File

SOURCE=.\ObjTermin.h
# End Source File
# Begin Source File

SOURCE=..\..\EL\Pathname\Pathname.h
# End Source File
# Begin Source File

SOURCE=.\Resource.h
# End Source File
# Begin Source File

SOURCE=.\SConfig.h
# End Source File
# Begin Source File

SOURCE=.\StdAfx.h
# End Source File
# Begin Source File

SOURCE=..\Bredy.Libs\StrRes\StringRes.h
# End Source File
# Begin Source File

SOURCE=.\UzivatelskaObj.h
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# Begin Source File

SOURCE=.\res\MrazenaJidla.ico
# End Source File
# Begin Source File

SOURCE=.\res\MrazenaJidla.rc2
# End Source File
# End Group
# Begin Source File

SOURCE=.\ReadMe.txt
# End Source File
# End Target
# End Project
