// DlgObjednavka.cpp : implementation file
//


#include "stdafx.h"
#include <fstream.h>
//using namespace std;
#include "MrazenaJidla.h"
#include "DlgObjednavka.h"
#include "DlgAutorizace.h"
#include "MD5.h"
#include "ObjTermin.h"
#include "DlgZmenaHesla.h"
#include "DlgPrintPreview.h"
#include "SConfig.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About

#define MSG_STARTDLG (WM_APP+10)

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Dialog Data
	//{{AFX_DATA(CAboutDlg)
	enum { IDD = IDD_ABOUTBOX };
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAboutDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	//{{AFX_MSG(CAboutDlg)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
	//{{AFX_DATA_INIT(CAboutDlg)
	//}}AFX_DATA_INIT
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAboutDlg)
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	//{{AFX_MSG_MAP(CAboutDlg)
		// No message handlers
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// DlgObjednavka dialog

DlgObjednavka::DlgObjednavka(CWnd* pParent /*=NULL*/)
	: CDialog(DlgObjednavka::IDD, pParent)
{
	//{{AFX_DATA_INIT(DlgObjednavka)
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void DlgObjednavka::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(DlgObjednavka)
	DDX_Control(pDX, IDC_SUBMIT, wSubmit);
	DDX_Control(pDX, IDC_BZRUSIT, wBZrusit);
	DDX_Control(pDX, IDC_BOBJEDNAT, wBObjednat);
	DDX_Control(pDX, IDC_OBJKUSU, wPocetKusu);
	DDX_Control(pDX, IDC_NABIDKA, wNabidka);
	DDX_Control(pDX, IDC_TERMINYLIST, wTerminyList);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(DlgObjednavka, CDialog)
	//{{AFX_MSG_MAP(DlgObjednavka)
	ON_WM_SYSCOMMAND()
	ON_MESSAGE(MSG_STARTDLG,OnStartDlg)
	ON_WM_DESTROY()
	ON_CBN_SELCHANGE(IDC_TERMINYLIST, OnSelchangeTerminylist)
	ON_NOTIFY(LVN_ITEMCHANGED, IDC_NABIDKA, OnItemchangedNabidka)
	ON_NOTIFY(NM_DBLCLK, IDC_NABIDKA, OnDblclkNabidka)
	ON_BN_CLICKED(IDC_BOBJEDNAT, OnBobjednat)
	ON_BN_CLICKED(IDC_BZRUSIT, OnBzrusit)
	ON_NOTIFY(LVN_ENDLABELEDIT, IDC_NABIDKA, OnEndlabeleditNabidka)
	ON_BN_CLICKED(IDC_BNASTAVENI, OnBnastaveni)
	ON_BN_CLICKED(IDC_SUBMIT, OnSubmit)
	ON_BN_CLICKED(IDC_OBJSESTAVIT, OnObjsestavit)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// DlgObjednavka message handlers

BOOL DlgObjednavka::OnInitDialog()
{
	CDialog::OnInitDialog();

	_curobj=new UzivatelskaObj;

	// Add "About..." menu item to system menu.

	// IDM_ABOUTBOX must be in the system command range.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	
	wNabidka.InsertColumn(0,StrRes[IDS_JIDLOPOCETOBJ],LVCFMT_CENTER,50,0);
	wNabidka.InsertColumn(1,StrRes[IDS_JIDLOID],LVCFMT_CENTER,60,1);
	wNabidka.InsertColumn(2,StrRes[IDS_JIDLOJMENO],LVCFMT_LEFT,400,2);
	wNabidka.InsertColumn(3,StrRes[IDS_JIDLOVAHA],LVCFMT_CENTER,50,3);
	wNabidka.InsertColumn(4,StrRes[IDS_JIDLOCENA],LVCFMT_RIGHT,0,4);
	wNabidka.InsertColumn(5,StrRes[IDS_JIDLOCENADPH],LVCFMT_RIGHT,80,5);	

	ListView_SetExtendedListViewStyleEx(wNabidka,LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES|LVS_EX_UNDERLINEHOT,LVS_EX_FULLROWSELECT|LVS_EX_GRIDLINES|LVS_EX_UNDERLINEHOT);

	PostMessage(MSG_STARTDLG);
	
	return TRUE;  // return TRUE  unless you set the focus to a control
}

void DlgObjednavka::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}


static CString GetUserName()
  {
  char buff[256];
  DWORD size=sizeof(buff);
  GetUserName(buff,&size);
  return CString(buff);
  }

class FindNewestFile: public MassProcess
  {
	FILETIME latest;
  public:
	CString newest;	
	FindNewestFile() {memset(&latest,0,sizeof(latest));newest="";}
	bool ProcessFile(const char *filename);
  };

bool FindNewestFile::ProcessFile(const char *filename)
  {
  WIN32_FIND_DATA *fdata=(WIN32_FIND_DATA *)GetCurFileSpecificInfo(sizeof(WIN32_FIND_DATA));
  if (fdata==NULL) return false;
  if (fdata->ftLastWriteTime.dwHighDateTime>latest.dwHighDateTime ||
	fdata->ftLastWriteTime.dwHighDateTime==latest.dwHighDateTime &&
	fdata->ftLastWriteTime.dwLowDateTime>=latest.dwLowDateTime)
	{
	latest=fdata->ftLastAccessTime;
	newest=filename;
	}
  return true;
  }

LRESULT DlgObjednavka::OnStartDlg(WPARAM wParam, LPARAM lParam)
  {
  DlgAutorizace dlg;
  dlg.vJmeno=GetUserName();
  if (dlg.DoModal()==IDCANCEL) EndDialog(IDCANCEL);
  else
	{
	FindNewestFile nwfile;
	nwfile.ProcessFolders(GConfig._serverPath,dlg.vJmeno+OBJEDNAVKAEXT,true);
	if (nwfile.newest=="")
	  {
	  DlgZmenaHesla zmdlg;
	  zmdlg.disableOld=true;
	  zmdlg.vNoveHeslo=dlg.vHeslo;
	  CString psw=dlg.vHeslo;	  
	  if (zmdlg.DoModal()==IDCANCEL) 
		{EndDialog(IDCANCEL);return 0;}
  	  MD5::Calc((unsigned char *)(LPCTSTR)zmdlg.vNoveHeslo,zmdlg.vNoveHeslo.GetLength(),_heslo);
	  }
	else
	  {	  
	  MD5::Calc((unsigned char *)(LPCTSTR)dlg.vHeslo,dlg.vHeslo.GetLength(),_heslo);
	  _curobj->Load(nwfile.newest);
	  if (_curobj->CompareHeslo(_heslo)==false)
		{
		AfxMessageBox(IDS_PASSWORDISNOTCORRECT,MB_OK|MB_ICONSTOP);
		EndDialog(IDCANCEL);
		return 0;
		}
	  Reset();
	  }
	_curuserfile=dlg.vJmeno+OBJEDNAVKAEXT;
	LoadTerminy();
	}
  return 0;
  }

void DlgObjednavka::OnDestroy() 
 {
	int c=wTerminyList.GetCount();
	for (int i=0;i<c;i++) free(wTerminyList.GetItemDataPtr(i));
	CDialog::OnDestroy();
	delete _curobj;
	_curobj=NULL;
	
}

void DlgObjednavka::Reset()
  {
  delete _curobj;
  _curobj=new UzivatelskaObj;
  }

class LoadTerminyMassProcess:public MassProcess
  {
  public:
	CComboBox &toload;
	LoadTerminyMassProcess(CComboBox &cb):toload(cb) {}
	bool ProcessFile(const char *filename);
  };

void DlgObjednavka::LoadTerminy()
  {
  LoadTerminyMassProcess ld(wTerminyList);
  ld.ProcessFolders(GConfig._serverPath,TERMININFOEXT,true); 
  wTerminyList.SetCurSel(0);
  OnSelchangeTerminylist();
  }

bool LoadTerminyMassProcess::ProcessFile(const char *filename)
  {
  ObjTermin term;
  ArchiveStreamFileMappingIn file(CreateFile(filename,GENERIC_READ,FILE_SHARE_READ,NULL,OPEN_EXISTING,0,NULL),true);
  ArchiveSimpleBinary arch(file);
  if (!arch) return true;
  term.DataExchange(arch);
  if (!arch) return true;
  char buff[512];  
  sprintf(buff,StrRes[IDS_TERMINDESC],term._deadline.day,term._deadline.month,term._deadline.year);
  int cnt=toload.GetCount();
  int pos;
  for (pos=0;pos<cnt;pos++)
	{
	unsigned long order=*(unsigned long *)toload.GetItemDataPtr(pos);
	if (order<=term._deadline.asLong)
	  break;
	}
  if (pos==cnt) pos=toload.AddString(buff);
  else toload.InsertString(pos,buff);
  char *nfo=(char *)malloc(strlen(filename)+10);
  memcpy(nfo,&term._deadline.asLong,sizeof(term._deadline.asLong));
  strcpy(nfo+4,filename);
  toload.SetItemDataPtr(pos,nfo);  
  return true;
  }

void DlgObjednavka::LoadObjednavku(const char *termin)
  { 
  Reset();
  Pathname objfile=termin;
  objfile.SetFilename(_curuserfile);
  _curobj->Load(objfile);
  _curobj->SetHeslo(_heslo);
  ObjTermin &term=aktTerm;
  ArchiveStreamFileMappingIn file(CreateFile(termin,GENERIC_READ,FILE_SHARE_READ,NULL,OPEN_EXISTING,0,NULL),true);
  ArchiveSimpleBinary arch(file);
  if (!arch) return;
  term.DataExchange(arch);
  if (!arch) return;
  wNabidka.DeleteAllItems();
  for (int i=0;i<term._cenik.Size();i++)
	{
	char buff[40];
	int item=wNabidka.InsertItem(i,"");
	wNabidka.SetItemText(item,1,term._cenik[i].id);
	wNabidka.SetItemText(item,2,term._cenik[i].name);
	wNabidka.SetItemText(item,3,itoa(term._cenik[i].hmotnost,buff,10));
	sprintf(buff,"%.2f",term._cenik[i].cena);
	wNabidka.SetItemText(item,4,buff);
	sprintf(buff,"%.2f",term._cenik[i].cenadph);
	wNabidka.SetItemText(item,5,buff);
	}
  UpdateList();
  COleDateTime curtime=COleDateTime ::GetCurrentTime();
  Date curtm;
  curtm.day=curtime.GetDay();
  curtm.month=curtime.GetMonth();
  curtm.year=curtime.GetYear();
  _disabledPage=curtm.asLong>aktTerm._deadline.asLong;
  }

void DlgObjednavka::UpdateList()
  {
  int cnt=wNabidka.GetItemCount();
  for (int i=0;i<cnt;i++)
	{
	CString s=wNabidka.GetItemText(i,1);
	int ks=_curobj->Find(s);
	if (ks==0) wNabidka.SetItemText(i,0,"");
	else
	  {
	  char buff[50];
	  sprintf(buff,"%dx",ks);
	  wNabidka.SetItemText(i,0,buff);
	  }
	}
  UpdateCalcs();
  }

void DlgObjednavka::OnSelchangeTerminylist() 
  {
  int cursel=wTerminyList.GetCurSel();
  const char *p=(const char *)wTerminyList.GetItemDataPtr(cursel);
  p+=4;
  if (_curobj->IsDirty())
	{
	int res=AfxMessageBox(IDS_ULOZITDOTAZ,MB_YESNOCANCEL|MB_ICONQUESTION);
	if (res==IDCANCEL) 
	  {
	  wTerminyList.SetCurSel(_lastTermin);
	  return;
	  }
	if (res==IDYES)
	  while (_curobj->Save()==false)
		if (AfxMessageBox(IDS_SAVERROR,MB_RETRYCANCEL,MB_ICONEXCLAMATION)==IDCANCEL) break;
	}
  LoadObjednavku(p);
  DialogRules();
  _lastTermin=cursel;
  }

void DlgObjednavka::OnItemchangedNabidka(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_LISTVIEW* pNMListView = (NM_LISTVIEW*)pNMHDR;
	DialogRules();
	*pResult = 0;
}

void DlgObjednavka::DialogRules()
  {
  bool enable=wNabidka.GetSelectedCount()!=0 && !_disabledPage;
  wPocetKusu.EnableWindow(enable );
  wBObjednat.EnableWindow(enable );
  wBZrusit.EnableWindow(enable ); 
  wSubmit.EnableWindow(!_disabledPage);
  }

void DlgObjednavka::OnDblclkNabidka(NMHDR* pNMHDR, LRESULT* pResult) 
  {
  OnBobjednat();  	
  *pResult = 0;
  }

void DlgObjednavka::ObjednejSiKs(int pocet)
  {
  if (_disabledPage)
	{
	AfxMessageBox(IDS_UZAVRENAOBJEDNAVKA);
	return;
	}
  POSITION pos=wNabidka.GetFirstSelectedItemPosition();
  while (pos)
	{
	int i=wNabidka.GetNextSelectedItem(pos);
	CString s=wNabidka.GetItemText(i,1);
	int pockus=_curobj->Add(s,pocet);
	s.Format("%dx",pockus);
	wNabidka.SetItemText(i,0,s);
	}
  UpdateCalcs();
  }

void DlgObjednavka::OnBobjednat() 
  {
  int p=GetDlgItemInt(IDC_OBJKUSU,NULL,FALSE);
  if (p==0) p=1;
  if (p>99) 
	AfxMessageBox(IDS_HODNOTAJEPRILISVELKA,MB_OK|MB_ICONEXCLAMATION);
  else
	{
	SetDlgItemInt(IDC_OBJKUSU,p);
	ObjednejSiKs(p);
	}
  }

void DlgObjednavka::OnBzrusit() 
  {
  if (_disabledPage)
	{
	AfxMessageBox(IDS_UZAVRENAOBJEDNAVKA);
	return;
	}
  POSITION pos=wNabidka.GetFirstSelectedItemPosition();
  while (pos)
	{
	int i=wNabidka.GetNextSelectedItem(pos);
	CString s=wNabidka.GetItemText(i,1);
  	_curobj->Reset(s);
	wNabidka.SetItemText(i,0,"");
	}
  UpdateCalcs();
  }

void DlgObjednavka::OnEndlabeleditNabidka(NMHDR* pNMHDR, LRESULT* pResult) 
{
	LV_DISPINFO* pDispInfo = (LV_DISPINFO*)pNMHDR;
    if (_disabledPage)
  	  {
	  AfxMessageBox(IDS_UZAVRENAOBJEDNAVKA);
	  *pResult = 1;	
	  return;
	  }
	if (pDispInfo->item.pszText!=NULL) 
	  {
	  int pocet=-1;
	  CString s=wNabidka.GetItemText(pDispInfo->item.iItem,1);
	  sscanf(pDispInfo->item.pszText,"%d",&pocet);
	  _curobj->Reset(s);	
	  if (pocet!=-1)
		_curobj->Add(s,pocet);
	  UpdateList();
	  }
	*pResult = 0;	
}

BOOL DlgObjednavka::PreTranslateMessage(MSG* pMsg) 
  {
  if (pMsg->hwnd==wNabidka && pMsg->message==WM_KEYDOWN && (pMsg->wParam==VK_RETURN || pMsg->wParam==VK_SPACE))
	{
	int item=wNabidka.GetNextItem(-1,LVNI_FOCUSED);
	if (item!=-1)
	  {
	  CEdit *edit=wNabidka.EditLabel(item);	  
	  return TRUE;
	  }
	}
  return CDialog::PreTranslateMessage(pMsg);
  }

void DlgObjednavka::UpdateCalcs()
  {
  int pockus=_curobj->CalcPocetKusu();
  float celkcena=_curobj->CalcCena(true,aktTerm._cenik);
  float priplatek=pockus*GConfig._priplatek;
  CString form;
  form.Format(IDS_FORMATKUSU,pockus);
  SetDlgItemText(IDC_CELKEMKUSU,form);
  form.Format(IDS_FORMATCENA,priplatek);
  SetDlgItemText(IDC_MANPRIPLATEK,form);
form.Format(IDS_FORMATCENA,celkcena+priplatek);
  SetDlgItemText(IDC_CELKEMCENA,form);
  }

void DlgObjednavka::OnBnastaveni() 
  {
  DlgZmenaHesla dlg;
  if (dlg.DoModal()==IDOK)
	{
	unsigned char oldp[16];
	unsigned char newp[16];
	MD5::Calc((unsigned char *)(LPCTSTR)dlg.vStareHeslo,dlg.vStareHeslo.GetLength(),oldp);
	MD5::Calc((unsigned char *)(LPCTSTR)dlg.vNoveHeslo,dlg.vStareHeslo.GetLength(),newp);
	if (memcmp(_heslo,oldp,sizeof(_heslo)))
	  AfxMessageBox(IDS_STAREHESLONEBYLOZADANESPRAVNE,MB_OK|MB_ICONEXCLAMATION);
	else
	  {
	  memcpy(_heslo,newp,sizeof(_heslo));
	  _curobj->SetHeslo(_heslo);
	  AfxMessageBox(IDS_ZMENAHESLAOK,MB_OK|MB_ICONINFORMATION);	
	  }
	}
  }

void DlgObjednavka::OnSubmit() 
  {
   while (_curobj->Save()==false)
  	if (AfxMessageBox(IDS_SAVERROR,MB_RETRYCANCEL,MB_ICONEXCLAMATION)==IDCANCEL) return;
  OnCancel();	
  }

void DlgObjednavka::OnOK()
  {

  }

void DlgObjednavka::OnObjsestavit() 
  {
  Pathname tmpfile;
  tmpfile.SetTempDirectory();
  tmpfile.SetFilename("sestava.htm");  
  ofstream ofile(tmpfile,ios::out);
  ofile<<"<html>"
	   <<"<head>"
	   <<"<style>\n"
		<<"td{	border-top: 1px dotted Black; padding : 3px 20px;}	   \n"
		<<"td.suma{	border-top: 3px double Black; padding : 3px 20px;}	   \n"
		<<"</style>"
	   <<"<meta http-equiv=\"Content-Type\" content=\"text/html; charset=windows-1250\">"
	   <<"<title>"<<StrRes[IDS_HTMLOBJEDNAVKA]<<(int)aktTerm._deadline.day<<". "
		  <<(int)aktTerm._deadline.month<<". "<<aktTerm._deadline.year<<"</title>"
	   <<"</head>"
	   <<"<body>"
	   <<"<h1 align=\"center\">"<<StrRes[IDS_HTMLOBJEDNAVKA]<<"<nobr>"<<(int)aktTerm._deadline.day<<". "
		  <<(int)aktTerm._deadline.month<<". "<<aktTerm._deadline.year<<"</nobr></h1>"
	   <<"<table align=\"center\" cellpadding=0 cellspacing=0>"
	   <<"<th>"<<StrRes[IDS_JIDLOPOCETOBJ]<<"</th>"
	   <<"<th>"<<StrRes[IDS_JIDLOID]<<"</th>"
	   <<"<th>"<<StrRes[IDS_JIDLOJMENO]<<"</th>"
	   <<"<th>"<<StrRes[IDS_JIDLOCENADPH]<<"</th>";
  ofile.precision(2);
  ofile.setf(ios::fixed,ios::fixed|ios::scientific );
  float priplatek;
  for (int i=0;i<aktTerm._cenik.Size();i++)
	{
	int p=_curobj->Find(aktTerm._cenik[i].id);
	if (p!=0)
	  {
	  ofile<<"<tr>"
		<<"<td>"<<p<<"x</td>"
		<<"<td align=\"center\">"<<aktTerm._cenik[i].id<<"</td>"
		<<"<td>"<<aktTerm._cenik[i].name<<"</td>"
		<<"<td align=\"right\">"<<aktTerm._cenik[i].cenadph<<"</td>"
		<<"</tr>";		
	  }
	}
	if (GConfig._priplatek)  ofile<<"<tr>"
		<<"<td>&nbsp;</td>"
		<<"<td align=\"center\">&nbsp;</td>"
		<<"<td>"<<StrRes[IDS_PRIPLATEKSTR]<<"</td>"
        <<"<td align=\"right\">"<<(priplatek=_curobj->CalcPocetKusu()*GConfig._priplatek)<<"</td>"
		<<"</tr>";		
//  ofile<<"<tr><td colspan=5><hr></td></tr>";
  ofile<<"<tr>"
	<<"<td class=\"suma\">"<<_curobj->CalcPocetKusu()<<"x</td>"
	<<"<td class=\"suma\">&nbsp;</td>"
	<<"<td class=\"suma\">"<<StrRes[IDS_CELKEMSESTAVA]<<"</td>"
	<<"<td  class=\"suma\" align=\"right\">"<<_curobj->CalcCena(true,aktTerm._cenik)+priplatek<<"</td>"
	<<"</tr>";
  ofile<<"</table>";
  ofile.close();
  DlgPrintPreview dlg;
  dlg.vPrintReportFile=tmpfile;
  dlg.DoModal();
  DeleteFile(tmpfile);
  }

void DlgObjednavka::OnCancel()
  {
  if (_curobj->IsDirty())
	{
	int res=AfxMessageBox(IDS_ULOZITDOTAZ,MB_YESNOCANCEL|MB_ICONQUESTION);
	if (res==IDCANCEL) 
	  return;
	if (res==IDYES)
	  while (_curobj->Save()==false)
		if (AfxMessageBox(IDS_SAVERROR,MB_RETRYCANCEL,MB_ICONEXCLAMATION)==IDCANCEL) break;
	}
  CDialog::OnCancel();
  }
