// ChildView.cpp : implementation of the HTMLEditPad class
//

#include "common.h"
#include "HTMLEditPad.h"
#include ".\htmleditpad.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// HTMLEditPad

HTMLEditPad::HTMLEditPad()
{

}

HTMLEditPad::~HTMLEditPad()
{
}


BEGIN_MESSAGE_MAP(HTMLEditPad, CWnd)
  ON_WM_SIZE()
END_MESSAGE_MAP()



// HTMLEditPad message handlers

BOOL HTMLEditPad::PreCreateWindow(CREATESTRUCT& cs) 
{
	if (!CWnd::PreCreateWindow(cs))
		return FALSE;

//	cs.dwExStyle |= WS_EX_CLIENTEDGE;
	cs.style &= ~WS_BORDER;
	cs.lpszClass = AfxRegisterWndClass(CS_HREDRAW|CS_VREDRAW|CS_DBLCLKS, 
		::LoadCursor(NULL, IDC_ARROW), reinterpret_cast<HBRUSH>(COLOR_WINDOW+1), NULL);

	return TRUE;
}

void HTMLEditPad::CreateEditor(const _TCHAR *templateUrl)
{
  editorView.Create(::AfxRegisterWndClass(0,0,0,0),"",WS_VISIBLE,CRect(0,0,0,0),this,1);
  editorView.Navigate2(templateUrl);  
  PHTMLDocument2 pdoc=editorView.GetDocument();
  while (pdoc.IsNull())
  {
    AfxPumpMessage();
    pdoc=editorView.GetDocument();
  }
  pdoc->put_designMode(L"On");
}

void HTMLEditPad::OnSize(UINT nType, int cx, int cy)
{
  int top=0;  
  if (editorView.GetSafeHwnd())
  {
    editorView.MoveWindow(0,top,cx,cy-top);
  }
  CWnd::OnSize(nType, cx, cy);
}
