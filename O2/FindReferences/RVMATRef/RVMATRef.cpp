// RVMATRef.cpp : Defines the entry point for the DLL application.
//

#include "stdafx.h"
#include "../FindReferences/PluginApi.h"
#include <Projects/ObjektivLib/ObjToolsMatLib.h>
#include <es/strings/rstring.hpp>
#include <el/Pathname/pathname.h>

BOOL APIENTRY DllMain( HANDLE hModule, 
                       DWORD  ul_reason_for_call, 
                       LPVOID lpReserved
					 )
{
    return TRUE;
}

void EnumAllReferences(const char *filename,EnumRefCallback callback,void *context)
{
  BTree<RString> container;
  ObjToolMatLib::ExploreMaterial(filename,container);
  BTreeIterator<RString> iter(container);
  RString *itm;
  const char *type;

  while (itm=iter.Next())
  {
    type=Pathname::GetExtensionFromPath(*itm);
    if (*type=='.') type++;
    if (itm->GetLength()) callback(*itm,type,RefBased,context);
  }
}