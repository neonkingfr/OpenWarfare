// P3DRef.cpp : Defines the entry point for the DLL application.
//

#include "stdafx.h"
#include "P3DRef.h"
#include "../FindReferences/PluginApi.h"
#include <el/Pathname/pathname.h>
#include <Projects/ObjektivLib/LODObject.h>
#include <Projects/ObjektivLib/ObjToolsMatLib.h>
#include <Projects/ObjektivLib/ObjToolProxy.h>


BOOL APIENTRY DllMain( HANDLE hModule, 
                       DWORD  ul_reason_for_call, 
                       LPVOID lpReserved
					 )
{
	switch (ul_reason_for_call)
	{
	case DLL_PROCESS_ATTACH:
	case DLL_THREAD_ATTACH:
	case DLL_THREAD_DETACH:
	case DLL_PROCESS_DETACH:
		break;
	}
    return TRUE;
}

void EnumAllReferences(const char *filename,EnumRefCallback callback,void *context)
{
  LODObject lodobj;
  if (lodobj.Load(Pathname(filename),0,0)==0)
  {
    BTree<ObjMatLibItem> container;
    for (int i=0;i<lodobj.NLevels();i++)
    {
      ObjectData *obj=lodobj.Level(i);
      ObjToolMatLib matLib=obj->GetTool<ObjToolMatLib>();
      matLib.ReadMatLib(container,ObjToolMatLib::ReadTextures);
      matLib.ReadMatLib(container,ObjToolMatLib::ReadMaterials);
      ObjToolProxy prox=obj->GetTool<ObjToolProxy>();
      prox.EnumProxies(container,0,true,"");
    }

    BTreeIterator<ObjMatLibItem> iter(container);
    ObjMatLibItem *itm;
    Pathname pth=filename;
    const char *type;
    while (itm=iter.Next())
    {
      type=Pathname::GetExtensionFromPath(itm->name);
      if (*type=='.') type++;
      if (itm->name.GetLength()) callback(itm->name,type,RefBased,context);
    }
  }
}