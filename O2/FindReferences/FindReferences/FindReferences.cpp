// FindReferences.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <el/Pathname/pathname.h>
#include <El/BTree/Btree.h>
#include <Es/containers/array.hpp>
#include <es/strings/rstring.hpp>
#include <iostream>
#include "PluginApi.h"
#include <io.h>

using namespace std;

struct ResourceInfo
{
  RString resource;
  RString type;
  ResourceInfo(RString resource,RString type):resource(resource),type(type) {}
  ResourceInfo() {}
};

TypeIsMovable(ResourceInfo);

static AutoArray<ResourceInfo> foundResources;

struct PluginApiDesc
{
  RString _extension;
  HMODULE _module;
  type_EnumAllReferences _function;  

  bool operator>=(const PluginApiDesc &other) const {return stricmp(_extension,other._extension)>=0;}  
  bool operator==(const PluginApiDesc &other) const {return stricmp(_extension,other._extension)==0;}  
  bool operator<(const PluginApiDesc &other) const {return stricmp(_extension,other._extension)<0;}  
  bool operator<=(const PluginApiDesc &other) const {return stricmp(_extension,other._extension)<0;}  
  int operator=(int z) {return z;}  
};

struct RefDatabaseItem
{
  RString srcfile;
  RString usedby;
  bool isUnknown; ///<srcfile reference is unknown (not a file)
  bool displayed;
  ReferenceMode refMode; ///<defines mode for unknown 

  RefDatabaseItem():isUnknown(false),displayed(false),refMode(RefAbsolute) {}

  int Compare(const RefDatabaseItem& other)const
  {
    int comp=stricmp(srcfile,other.srcfile);
    if (comp==0)
    {
      return stricmp(usedby,other.usedby);
    }
    return comp;
  }
  bool operator>=(const RefDatabaseItem& other) const {return Compare(other)>=0;}  
  bool operator==(const RefDatabaseItem& other) const {return Compare(other)==0;}  
  bool operator<(const RefDatabaseItem& other) const {return Compare(other)<0;}  
  bool operator<=(const RefDatabaseItem& other) const {return Compare(other)<0;}  
  int operator=(int z) {return z;}  
};



static BTree<PluginApiDesc> GPlugApi;
static BTree<RefDatabaseItem> GRefDatabase;
static RString basePath;
static RString pathsList;
static bool DisableBottomTop=false;
static bool DontShowOwners=false;
static bool ExcelForm=false;
static bool ScriptForm=false;

struct SearchContext
{
  RString srcFile;
};

static void AddToRefList(const char *reffile,const char *type, ReferenceMode mode, void *context)
{
  RefDatabaseItem search,*fnd;
  bool unknown=false;

  SearchContext *ct=(SearchContext *)context;
  if (mode==RefRelative)
  {
    Pathname abspth(reffile,Pathname(ct->srcFile));
    search.srcfile=abspth.GetFullPath();
  }
  else if (mode==RefAbsolute)
  {
    search.srcfile=reffile;
  }
  else if (mode==RefBased)
  {
    if (basePath.GetLength())
    {
      search.srcfile=basePath+reffile;
    }
    else if (!DisableBottomTop)
    {
      Pathname ff=ct->srcFile;
      ff.SetFilename(reffile);
      if (ff.BottomTopSearchExist())
        search.srcfile=ff.GetFullPath();
      else
      {
        search.isUnknown=true;
        search.srcfile=reffile;
      }
    }
    else
    {
      search.isUnknown=true;
      search.srcfile=reffile;
    }
  }
  else if (mode==RefBottomTopSearch)
    {
      Pathname ff=ct->srcFile;
      ff.SetFilename(reffile);
      if (!DisableBottomTop && ff.BottomTopSearchExist())
        search.srcfile=ff.GetFullPath();
      else
      {
        search.isUnknown=true;
        search.srcfile=reffile;
      }
    }
  else if (mode==RefPaths)
  {
    Pathname ff;
    if (ff.SearchPaths(pathsList,reffile))
    {
      search.isUnknown=true;
      search.srcfile=reffile;
    }
    else
      search.srcfile=ff.GetFullPath();
  }
  else if (mode==RefNotRealFile)
  {
    search.srcfile=reffile;
    search.displayed=true; //mark displayed, so this reference will not displayed in the list.
    search.refMode=mode;
  }
  else
  {
    search.isUnknown=true;
    search.srcfile=reffile;
  }
  if (search.isUnknown) search.refMode=mode;
  
  search.usedby=ct->srcFile;
  fnd=GRefDatabase.Find(search);
  if (fnd==0)
  {
    search.usedby="";
    BTreeIterator<RefDatabaseItem> iter(GRefDatabase);
    iter.BeginFrom(search);
    fnd=iter.Next();
    if (fnd==0 || stricmp(fnd->srcfile,search.srcfile)!=0)
    {
      if (!search.isUnknown)
      {
        if (search.refMode==RefNotRealFile || _access(search.srcfile,0)==0)
        {
          
          foundResources.Append(ResourceInfo(search.srcfile,type));      
        }
      }
    }
    search.usedby=ct->srcFile;
    GRefDatabase.Add(search);    
  }
}

static void FindAndAddReference(const char *file,const char *type)
{  
  PluginApiDesc desc;
  desc._extension=type;

  PluginApiDesc *plug=GPlugApi.Find(desc);
  if (plug==0)
  {
    Pathname libName;
    libName=libName.GetExePath();
    libName.SetFiletitle(RString(desc._extension,".dw"));
    libName.SetExtension(".DLL");
    if (libName.BottomTopSearchExist())
    {
      desc._module=LoadLibrary(libName);
      if (desc._module)
      {
        type_EnumAllReferences f=(type_EnumAllReferences)GetProcAddress(desc._module,"EnumAllReferences");        
        desc._function=f;
      }
      else
        desc._function=0;
    }
    else
      desc._function=0;    
    GPlugApi.Add(desc);
    plug=&desc;
  }
  if (plug->_function)
  {
    SearchContext cnt;
    cnt.srcFile=file;
    plug->_function(file,AddToRefList,&cnt);
  }
}


static void FindRef(const char *file,const char *type)
{
  RefDatabaseItem itm;
  itm.srcfile=file;
  itm.displayed=true;
  if  (GRefDatabase.Find(itm)) return;
  GRefDatabase.Add(itm);

  int ptr=foundResources.Size();
  foundResources.Append()=ResourceInfo(RString(file),RString(type));


  while (ptr<foundResources.Size())
  {
    FindAndAddReference(foundResources[ptr].resource,foundResources[ptr].type);     
    ptr++;
  }
}

static void HelperShowPartialRes(RefDatabaseItem *item)
{
  if (item->isUnknown) 
  {
    cout<<'?';
    cout<<((int)(item->refMode));
    cout<<':';
  }
  cout<<item->srcfile.Data();
}

static void DisplayResults()
{
  BTreeIterator<RefDatabaseItem> iter(GRefDatabase);
  RefDatabaseItem *item;
  const char *lastitem=0;
  while (item=iter.Next())
  {
    if (!item->displayed)
    {  
      if (DontShowOwners)
      {
        if (lastitem==0 || stricmp(lastitem,item->srcfile))
        {
          lastitem=item->srcfile;
          HelperShowPartialRes(item);
          cout<<"\n";
        }                  
      }
      else
      {        
        if (ScriptForm)
        {
          cout<<"[\"";
          HelperShowPartialRes(item);
          cout<<"\",\""<<item->usedby.Data()<<"\"]\n";
        }
        else if (ExcelForm)          
        {
          cout<<"\"";
          HelperShowPartialRes(item);
          cout<<"\",\""<<item->usedby.Data()<<"\"\n";
        }
        else
        {        
          HelperShowPartialRes(item);
          cout<<" => "<<item->usedby.Data();
          cout<<"\n";
        }
      }
    item->displayed=true;
    }
  }
  cout<<".\n";
}

static void ShowHelp()
{
  puts("DepWalker detects all dependencies of given files");
  puts("Usage:\r\n"
    "------\r\n"
    "\r\n"
    "DepWalker [<switches>] [file]\r\n"
    "\r\n"
    "Switches:\r\n"
    "-P\tSpecify path list for search files using RefPaths\r\n"
    "-B\tSpecify base folder path, for search files relative to base\r\n"
    "\tIf not specified, BottomTopSearch will be used\r\n"
    "-b\tOutput only depend files with no relationships (brief)\r\n"
    "-u\tDisable BottomTopSearch. Any reference, that needs \r\n"
    "\tthis type of search will be reported as unknonw\r\n"
    "-c\tExcel (CSV) form (not in -b mode)\r\n"
    "-s\tScript form (not in -b mode) [\"xx\",\"yy\"]\r\n"
    "\r\n"
    "If file specified, DepWalker processes only given file and\r\n"
    "all depend files. If no file specified, files are read from \r\n"
    "the standard input. Results are displayed when eof is sent.\r\n"
    "Partial result can be displayed every time, when single dot (.)\r\n"
    "alone at line is entered.");  
}

static RString ParseParameters(int argc, char *argv[])
{
  RString input;
  for (int i=1;i<argc;i++)
  {
    if (argv[i][0]=='-' || argv[i][0]=='/')
      switch (argv[i][1])
      {
      case 'b': DontShowOwners=true;break;
      case 'u': DisableBottomTop=true;break;
      case 'P': i++; if (i<argc) pathsList=argv[i];break;
      case 'B': i++; if (i<argc) basePath=argv[i];break;
      case '?': ShowHelp();exit(1);
      case 'c': ExcelForm=true;break;
      case 's': ScriptForm=true;break;
      }
    else
      input=argv[i];
  }
  return input;
}


int _tmain(int argc, _TCHAR* argv[])
{
  char pathbuff[2048];
  RString tosearch;

  tosearch=ParseParameters(argc,argv);

 if (tosearch.GetLength())
  {
    const char *type=Pathname::GetExtensionFromPath(tosearch);
    if (*type) type++;
    FindRef(tosearch,type);
  }
  else
  while (!cin.eof())
  {
    const char *type;
    cin.getline(pathbuff,2048);
    if (strcmp(pathbuff,".")==0)
      DisplayResults();
    else
    {    
      char *c=strchr(pathbuff,'*');
      if (c!=0)
      {
        *c=0;
        type=c+1;
      }
      else
      {
        type=Pathname::GetExtensionFromPath(pathbuff);
        if (*type) type++;
      }
      FindRef(pathbuff,type);
    }
  }

  DisplayResults();
 return 0;
}

