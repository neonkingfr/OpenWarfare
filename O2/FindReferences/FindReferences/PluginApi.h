
enum ReferenceMode
{
  RefAbsolute=0,  ///<returned reference is absolute pathname
  RefRelative=1,  ///<returned reference is relative pathname
  RefBased=2,     ///<returned reference is relative from base (can use bottom/top to find base)
  RefBottomTopSearch=3, ///<returned reference is subject of bottom/top search
  RefPaths=4,      ///<returned reference is relative to list of path (cannot use bottom/top)
  RefNotRealFile=5 ///<reference is not file, it can be any string, that can be parsed by another dep plugin.
};

extern "C"
{

///Each plugin must call this function to add new file into dependence list
/**
@param reffile - reference to file
@param type - type of file - if file has no specific type, use extension withou dot.
@param mode - reference mode, @see ReferenceMode
@param context - pointer context passed to EnumAllReferences function. Plugin has no permission to change this pointer or data inside.
*/
typedef void (*EnumRefCallback)(const char *reffile, const char *type, ReferenceMode mode, void *context);

#ifdef REFTOOL_EXPORTS
__declspec(dllexport) void EnumAllReferences(const char *filename,EnumRefCallback callback,void *context);
#else
  typedef void (* type_EnumAllReferences)(const char *filename,EnumRefCallback callback,void *context);
#endif
}