// TextFind.cpp: implementation of the CTextFind class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "newsreader.h"
#include "SmartFind.h"
#include <El/Text/wordCompare.hpp>

CSmartFind::CSmartFind()
{
	_substrings = false;
}

static const char SpaceExtChars[]=".,:()-{}?!\"\'*/\\;^&*$#@";

inline bool IsSpaceExt(char c)
{
	if (c>=9 && c<=0xd) // CR/LF/FF/TAB
	{
		return true;
	}
	if (c==' ') return true;
	return strchr(SpaceExtChars,c)!=NULL;
}

static const char *SkipSpaces(const char *text)
{
	while (*text && IsSpaceExt(*text))
	{
		text++;
	}
	return text;
}

static const char *SkipWord(const char *text)
{
	while (*text && !IsSpaceExt(*text))
	{
		text++;
	}
	return text;
}

bool CSmartFind::Init(const char *wordList)
{
	// scan source for separate words
	for(;;)
	{
		const char *wordBeg = SkipSpaces(wordList);
		const char *wordEnd = SkipWord(wordBeg);
		if (wordBeg<wordEnd)
		{
			_words.Add(RString(wordBeg,wordEnd-wordBeg));
		}
		if (!*wordEnd) break;
		wordList = wordEnd;
	}
	return _words.Size()>0;
}


float CSmartFind::GetScore(const char *intext)
{
	float totalDiff = 0;
	int wordsMatched = 0;
	for (int i=0; i<_words.Size(); i++)
	{
		// scan input text
		const char *word = _words[i];
		const char *wordList = intext;
		bool found = false;
		for(;;)
		{
			const char *wordBeg = SkipSpaces(wordList);
			const char *wordEnd = SkipWord(wordBeg);
			if (wordBeg<wordEnd)
			{
				// word found - check it
				RString inWord(wordBeg,wordEnd-wordBeg);
				float diff = 0;
				if (_substrings)
				{
					diff = WordDifferenceSubstring(inWord,word,2.0f);
				}
				else
				{
					diff = WordDifference(inWord,word,2.0f);
				}
				if (diff<0.3f)
				{
					totalDiff += diff;
					wordsMatched++;
					break;
				}
			}
			if (!*wordEnd) break;
			wordList = wordEnd;
		}
	}
	if (wordsMatched<=0) return 0;
	int missing = _words.Size()-wordsMatched;
	float missingFactor = 1-float(missing)/_words.Size();
	float avgDiff = totalDiff/wordsMatched;
	// we want to give score 100 for difference 0
	// we want to give score 50 for difference 0.3
	// we want to give score 90 for difference 0.3 and lower

	float diffScore = 100-avgDiff*(50/0.3f);
	if (diffScore<0) diffScore = 0;
	float totalScore = diffScore*missingFactor;
	if (totalScore<40) return 0;
	return totalScore/100.0f;
}


bool CSmartFind::Find(const char *intext)
{
	float score = GetScore(intext);
	return (score>=50);
}

CSmartFind::~CSmartFind()
{
}
