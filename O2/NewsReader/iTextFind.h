// TextFind.h: interface for the CTextFind class.
//
//////////////////////////////////////////////////////////////////////

#ifndef RR_I_TEXT_FIND_H
#define NR_I_TEXT_FIND_H

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

//! text find interface
class ITextFind
{
	public:
	virtual ~ITextFind() {}

	virtual float GetScore(const char *intext)
	{
		bool match = Find(intext);
		return match ? 1.0f : 0;
	}
	//! perform a search
	virtual bool Find(const char *intext) = NULL;
	//! check if the seach is valid
	virtual bool IsCompiled() = NULL;
};

ITextFind *NewFind(bool substrings, bool booleanSearch, const char *text);

#endif
