// CondCalc.cpp: implementation of the CCondCalc class.
//
//////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>
#include <ctype.h>
#include <iostream>
#include <strstream>
#include "CondCalc.h"
using namespace std;


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CCondCalc::CCondCalc()
  {
  compiled=NULL;
  }

CCondCalc::~CCondCalc()
  {
  delete compiled;
  }

static void ReadToken(istream &str, char *buff, int maxsize)
  {
  ws(str);
  int i=str.get();
  maxsize--;
  while ((isalnum(i)) || i=='_' ) 
	{
	if (maxsize>0)
	  {
  	  *buff++=i;
	  maxsize--;
	  }
	i=str.get();
	}
  *buff=0;
  if (i!=EOF) str.putback((char)i);
  }

static void ReadSymbol(istream &str, char *buff)
  {
  ws(str);
  int i=str.get();
 *buff=0;
  if (i==EOF) return;
  if ((isalnum(i)) || i=='_')
	{
	str.putback((char)i);
   *buff=0;
	return;
	}
  *buff++=i;
  if (i=='=' || i=='!')
	{
	i=str.get();
	if (i!='=') str.putback((char)i);else *buff++=i;
	}
  *buff=0;
  }


static void ReadNext(istream &in, CCondCalc::SymbInfo &smbinfo)
  {
  ReadSymbol(in,smbinfo.name);
  if (smbinfo.name[0]==0)
	{
	ReadToken(in,smbinfo.name,CCC_SYMBSIZE);
	if (smbinfo.name[0]==0)
	  {
	  int n=in.get();
	  if (n==EOF) smbinfo.type=CCondCalc::fin;
	  else smbinfo.type=CCondCalc::error;
	  }
	else
	  smbinfo.type=CCondCalc::token;
	}
  else
	smbinfo.type=CCondCalc::symbol;
  }

CCondCalc::_Operator *TraverseE(istream& in, CCondCalc::SymbInfo &smbinfo);
CCondCalc::_Operator *TraverseF(istream& in, CCondCalc::SymbInfo &smbinfo);


static CCondCalc::_Operator *TraverseT(istream& in, CCondCalc::SymbInfo &smbinfo)
  {
  if (smbinfo.type==CCondCalc::fin || smbinfo.type==CCondCalc::error) return NULL;
  if (smbinfo.type==CCondCalc::symbol)
	{
	if (strcmp(smbinfo.name,CCC_INVERT)==0)
	  {
	  ReadNext(in,smbinfo);
	  CCondCalc::_Operator *out=TraverseT(in,smbinfo);
	  if (out==NULL) {out=new CCondCalc::Error();smbinfo.type=CCondCalc::error;}
	  return new CCondCalc::NegateOp(out);
	  }
	if (strcmp(smbinfo.name,CCC_ZAVORKA1)==0)
	  {
	  ReadNext(in,smbinfo);
	  CCondCalc::_Operator *out=TraverseE(in,smbinfo);
	  if (smbinfo.type==CCondCalc::error) return out;
	  if (smbinfo.type!=CCondCalc::symbol || strcmp(smbinfo.name,CCC_ZAVORKA2)!=0)
		{smbinfo.type=CCondCalc::error;return new CCondCalc::ErrorOp(out);}
	  else 
		{
		ReadNext(in,smbinfo);
		return out;
		}	 
	  }	
	smbinfo.type=CCondCalc::error;
	return new CCondCalc::Error();
	}
  char *buff=(char *)alloca(strlen(smbinfo.name)+1);
  strcpy(buff,smbinfo.name);
  ReadNext(in,smbinfo);
  bool equal;
  if (smbinfo.type==CCondCalc::fin) 
	return  new CCondCalc::SingleParm(buff);
  if (smbinfo.type==CCondCalc::symbol)
	if (strcmp(smbinfo.name,CCC_EQUAL)==0)
	  equal=true;
	else if (strcmp(smbinfo.name,CCC_NEQUAL)==0)
	  equal=false;
	else
	  {
	  return new CCondCalc::SingleParm(buff);
	  }
  ReadNext(in,smbinfo);
  if (smbinfo.type!=CCondCalc::token)
	{
	smbinfo.type=CCondCalc::error;
	return new CCondCalc::Error();
	}
  CCondCalc::_Operator *out=new CCondCalc::SingleParmValue(buff,smbinfo.name,equal);
  ReadNext(in,smbinfo);
  return out;
  }

static CCondCalc::_Operator *TraverseF(istream& in, CCondCalc::SymbInfo &smbinfo)
  {
  if (smbinfo.type==CCondCalc::fin || smbinfo.type==CCondCalc::error) return NULL;
  CCondCalc::_Operator *left=TraverseT(in,smbinfo);
  if (left==NULL) return NULL;  
  if (smbinfo.type!=CCondCalc::symbol || strcmp(smbinfo.name,CCC_AND)!=0) return left;
  ReadNext(in,smbinfo);
  CCondCalc::_Operator *right=TraverseF(in,smbinfo);
  if (right==NULL)  return left;
  return new CCondCalc::AndOp(left,right);
  }
static CCondCalc::_Operator *TraverseE(istream& in, CCondCalc::SymbInfo &smbinfo)
  {
  if (smbinfo.type==CCondCalc::fin || smbinfo.type==CCondCalc::error) return NULL;
  CCondCalc::_Operator *left=TraverseF(in,smbinfo);
  if (left==NULL) return NULL;
  if (smbinfo.type!=CCondCalc::symbol || strcmp(smbinfo.name,CCC_OR)!=0) return left;
  ReadNext(in,smbinfo);
  CCondCalc::_Operator *right=TraverseE(in,smbinfo);
  if (right==NULL)  return left;
  return new CCondCalc::OrOp(left,right);
  }

bool CCondCalc::Compile(const char *line)
  {
  istrstream in(const_cast<char *>(line));
  CCondCalc::SymbInfo smbinfo;
  ReadNext(in,smbinfo);
  delete compiled;
  compiled=TraverseE(in,smbinfo);  
  if (smbinfo.type!=CCondCalc::fin && smbinfo.type!=CCondCalc::error )
	{
	smbinfo.type=CCondCalc::error;
	compiled=new ErrorOp(compiled);
	}
  return smbinfo.type!=CCondCalc::error;
  }

void CCondCalc::Print(ostream &out)
  {
  if (compiled==NULL) return;
  compiled->Output(out);
  }

//Porovna dva retezce, druhy je ukonecny nulou a prvni muze byt ukonceny taky znaky v mnozine endchr
static int stristr_ex(const char *src, const char *src2, char *endchr)
  {
  while (tolower(*src)==tolower(*src2) && *src) 
	{
	src++;
	src2++;
	}
  if (strchr(endchr,*src)!=0 && *src2==0) return 0;
  return (*src2>*src)-(*src2<*src);
  }

bool CCondCalc::FindParam(const char *line, const char *param)
  {  
  int sz=strlen(line);
  while (sz)
	{
	if (stristr_ex(line,param,":")==0) return true;
	line=(const char *)((char *)line+sz+1);
	sz=strlen(line);
	}
  return false;
  }


bool  CCondCalc::CheckParamValue(const char *line, const char *param, const char *value)
  {
  char *compare=(char *)alloca(strlen(param)+strlen(value)+2);
  sprintf(compare,"%s:%s",param,value);
  int sz=strlen(line);
  while (sz)
	{
	if (stricmp(line,compare)==0) return true;
	line=(const char *)((char *)line+sz+1);
	sz=strlen(line);
	}
  return false;
  }

int CCondCalc::Evalute(const char *line) 
  {
  int sz=strlen(line)*2;
  char *s=(char *)alloca(sz+2);
  char *p=s;
  istrstream in((char *)line);
  ws(in);
  if (line[0])
	do
	  {
	  ReadToken(in,p,sz);
  //	if (in.eof()) break;
	  int ln=strlen(p);
	  if (ln==0) return -2;
	  ws(in);
	  int i=in.get();
  //	if (i==EOF) break;
	  if (i==':') 
		{
		sz-=ln+1;
		p+=ln;
		*p++=i;
		ReadToken(in,p,sz);
		ln=strlen(p);
		}
	  else
		in.putback((char)i);
	  sz-=ln+1;
	  p+=ln+1;
	  ws(in);
	  i=in.get();
	  if (i==EOF) break;
	  if (i!=',') 
		return -2;
	  }
    while (true);
  else 
	s[0]=0;
  *p++=0;
  if (compiled==NULL) return -3;
  bool ok=compiled->Calculate(s);
    if (ok) return 1;
  else return 0;
  }