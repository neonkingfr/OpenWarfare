// ThreadPropExDlg.cpp : implementation file
//

#include "stdafx.h"
#include "newsreader.h"
#include "ThreadPropExDlg.h"
using namespace std;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CThreadPropExDlg dialog

#define HSPACE 7
#define VSPACE 2

CThreadPropExDlg::CThreadPropExDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CThreadPropExDlg::IDD, pParent)
{
  vallist=NULL;
  load=true;
	//{{AFX_DATA_INIT(CThreadPropExDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
  _delet=false;
}


void CThreadPropExDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);  
    for (int i=0;i<CTPD_LINECOUNT;i++)
    {
    DDX_Control(pDX, IDC_VARIBLE1+i, wVarible[i]);
    DDX_Control(pDX, IDC_VALUE1+i, wValue[i]);
    DDX_CBString(pDX, IDC_VALUE1+i, value[i]);
    DDX_CBString(pDX, IDC_VARIBLE1+i, varible[i]);
    }

	//{{AFX_DATA_MAP(CThreadPropExDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CThreadPropExDlg, CDialog)
	//{{AFX_MSG_MAP(CThreadPropExDlg)
	ON_WM_SIZE()
	ON_WM_PAINT()
	ON_WM_TIMER()
ON_CBN_KILLFOCUS(IDC_VARIBLE1, OnEditchangeVarible1)
ON_CBN_KILLFOCUS(IDC_VARIBLE2, OnEditchangeVarible1)
ON_CBN_KILLFOCUS(IDC_VARIBLE3, OnEditchangeVarible1)
ON_CBN_KILLFOCUS(IDC_VARIBLE4, OnEditchangeVarible1)
ON_CBN_KILLFOCUS(IDC_VARIBLE5, OnEditchangeVarible1)
ON_CBN_KILLFOCUS(IDC_VARIBLE6, OnEditchangeVarible1)
ON_CBN_KILLFOCUS(IDC_VARIBLE7, OnEditchangeVarible1)
ON_CBN_KILLFOCUS(IDC_VARIBLE8, OnEditchangeVarible1)
ON_CBN_KILLFOCUS(IDC_VARIBLE9, OnEditchangeVarible1)
ON_CBN_KILLFOCUS(IDC_VARIBLE10, OnEditchangeVarible1)
ON_CBN_KILLFOCUS(IDC_VARIBLE11, OnEditchangeVarible1)
ON_CBN_KILLFOCUS(IDC_VARIBLE12, OnEditchangeVarible1)
ON_CBN_KILLFOCUS(IDC_VARIBLE13, OnEditchangeVarible1)
ON_CBN_KILLFOCUS(IDC_VARIBLE14, OnEditchangeVarible1)
ON_CBN_KILLFOCUS(IDC_VARIBLE15, OnEditchangeVarible1)
ON_CBN_EDITUPDATE(IDC_VARIBLE1, OnEditupdateVarible1)
ON_CBN_EDITUPDATE(IDC_VARIBLE2, OnEditupdateVarible1)
ON_CBN_EDITUPDATE(IDC_VARIBLE3, OnEditupdateVarible1)
ON_CBN_EDITUPDATE(IDC_VARIBLE4, OnEditupdateVarible1)
ON_CBN_EDITUPDATE(IDC_VARIBLE5, OnEditupdateVarible1)
ON_CBN_EDITUPDATE(IDC_VARIBLE6, OnEditupdateVarible1)
ON_CBN_EDITUPDATE(IDC_VARIBLE7, OnEditupdateVarible1)
ON_CBN_EDITUPDATE(IDC_VARIBLE8, OnEditupdateVarible1)
ON_CBN_EDITUPDATE(IDC_VARIBLE9, OnEditupdateVarible1)
ON_CBN_EDITUPDATE(IDC_VARIBLE10, OnEditupdateVarible1)
ON_CBN_EDITUPDATE(IDC_VARIBLE11, OnEditupdateVarible1)
ON_CBN_EDITUPDATE(IDC_VARIBLE12, OnEditupdateVarible1)
ON_CBN_EDITUPDATE(IDC_VARIBLE13, OnEditupdateVarible1)
ON_CBN_EDITUPDATE(IDC_VARIBLE14, OnEditupdateVarible1)
ON_CBN_EDITUPDATE(IDC_VARIBLE15, OnEditupdateVarible1)
ON_CBN_SELENDOK(IDC_VARIBLE1, OnEditupdateVarible1)
ON_CBN_SELENDOK(IDC_VARIBLE2, OnEditupdateVarible1)
ON_CBN_SELENDOK(IDC_VARIBLE3, OnEditupdateVarible1)
ON_CBN_SELENDOK(IDC_VARIBLE4, OnEditupdateVarible1)
ON_CBN_SELENDOK(IDC_VARIBLE5, OnEditupdateVarible1)
ON_CBN_SELENDOK(IDC_VARIBLE6, OnEditupdateVarible1)
ON_CBN_SELENDOK(IDC_VARIBLE7, OnEditupdateVarible1)
ON_CBN_SELENDOK(IDC_VARIBLE8, OnEditupdateVarible1)
ON_CBN_SELENDOK(IDC_VARIBLE9, OnEditupdateVarible1)
ON_CBN_SELENDOK(IDC_VARIBLE10, OnEditupdateVarible1)
ON_CBN_SELENDOK(IDC_VARIBLE11, OnEditupdateVarible1)
ON_CBN_SELENDOK(IDC_VARIBLE12, OnEditupdateVarible1)
ON_CBN_SELENDOK(IDC_VARIBLE13, OnEditupdateVarible1)
ON_CBN_SELENDOK(IDC_VARIBLE14, OnEditupdateVarible1)
ON_CBN_SELENDOK(IDC_VARIBLE15, OnEditupdateVarible1)
	ON_WM_DESTROY()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CThreadPropExDlg message handlers

BOOL CThreadPropExDlg::OnInitDialog() 
{
int i;

    if (load)
      {
      SThreadProp_TName::Load(&vallist,SThreadProp_VALSNAME1,false);
      SThreadProp_TName::Load(&vallist,SThreadProp_VALSNAME2,true);
      load=false;
      }
    CRect rc;
    GetClientRect(&rc);
    _originsz=rc.Size();
    GetWindowRect(&rc);
    _orwndsz=rc.Size();

    CString s;
    s.LoadString(IDS_THREADPROPERTIESCAPTION);
    SetWindowText(s);

    CDialog::OnInitDialog();

    SetTimer(100,500,NULL);
    for (i=0;i<CTPD_LINECOUNT;i++) LoadNamesCombo(wVarible[i]);
    ParseProperties();
    for (i=0;i<CTPD_LINECOUNT;i++) 
      if (varible[i].GetLength()!=0)
        LoadValuesCombo(wValue[i],varible[i],false);
    HideLines();
    
    
	return FALSE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void SThreadProp_TName::Load(SThreadProp_TName **list, const char *file, bool global)
  {
  const char *name=theApp.FullPath(file,global);
  ifstream in(name,ios::in);
  if (!in) return;
  char buff[256];
  while (!in.eof())
    {
    buff[0]=0;
    buff[255]=0;
    ws(in);
    in.getline(buff,sizeof(buff),'~');
    if (buff[255]>=32) in.ignore(0x7fffffff,'~');
    if (!buff[0]) break;
    SThreadProp_TName *nm=(*list)->Find(buff);
    if (nm==NULL)
      {
      nm=new SThreadProp_TName(buff);
      nm->next=*list;*list=nm;
      }
    int zn=in.get();
    while (zn!='#')
      {
      buff[0]=0;
      buff[255]=0;
      in.putback((char)zn);
      in.getline(buff,sizeof(buff),'~');
      if (buff[255]>=32) in.ignore(0x7fffffff,'~');
      if (!buff[0]) break;
      SThreadProp_TName::Add(&nm,nm->name,buff,global);
      zn=in.get();
      }
    }
  }

void SThreadProp_TName::Save(SThreadProp_TName *list, const char *file)
  {
  const char *name=theApp.FullPath(file,false);
  ofstream out(name,ios::out|ios::trunc);
  if (!out) return;
  SThreadProp_TName *nm=list;
  while (nm)
    {
    bool frst=true;
    SThreadProp_TValue *vl=nm->values;
    while (vl)
      {
      if (!vl->nostore) 
        {
        if (frst) 	
          {out<<nm->name<<'~';frst=false;}
        out<<vl->value<<'~';
        }
      vl=vl->next;
      }
    out<<"#\n";
    nm=nm->next;
    }
  }

CRect CThreadPropExDlg::GetTwinRect(int twin)
  {
  CRect rc1;
  CRect rc2;
  GetDlgItem(IDC_VARIBLE1+twin)->GetWindowRect(&rc1);
  GetDlgItem(IDC_VALUE1+twin)->GetWindowRect(&rc2);
  ScreenToClient(&rc1);
  ScreenToClient(&rc2);
  return CRect(rc1.left,rc1.top,rc2.right,rc2.bottom);
  }

void CThreadPropExDlg::RecalcLayout(int cx, int cy)
  {
  CPoint pt;
  CRect twrc;
  CSize twsz;
  int lmargin,rmargin,i;

  twrc=GetTwinRect(0);
  pt.x=twrc.left;
  pt.y=twrc.top;
  twsz=twrc.Size();
  
  lmargin=twrc.left;
  rmargin=_originsz.cx-twrc.right;

  HDWP dwp=BeginDeferWindowPos(CTPD_LINECOUNT*2+5);

  for (i=1;i<CTPD_LINECOUNT;i++)
    {
    pt.x+=twsz.cx+HSPACE;
    if (pt.x+twsz.cx+HSPACE+rmargin>cx)
      {
      pt.x=lmargin;
      pt.y+=twsz.cy+VSPACE;
      }
    MoveTwin(i,pt,dwp);
    }
/*  GetDlgItem(IDC_DELPAIR)->GetWindowRect(&twrc);
  twsz=twrc.Size();
  DeferWindowPos(dwp,*GetDlgItem(IDC_DELPAIR),NULL,cx-twsz.cx-rmargin,cy-twsz.cy-rmargin,0,0,SWP_NOSIZE|SWP_NOZORDER);*/
  EndDeferWindowPos(dwp);
  }

void CThreadPropExDlg::MoveTwin(int twin, CPoint pt, HDWP dwp)
  {
  CRect rc1;
  CRect rc2;
  GetDlgItem(IDC_VARIBLE1+twin)->GetWindowRect(&rc1);
  GetDlgItem(IDC_VALUE1+twin)->GetWindowRect(&rc2);
  ScreenToClient(&rc1);
  ScreenToClient(&rc2);
  pt.x=pt.x-rc1.left;
  pt.y=pt.y-rc1.top;
  rc1.OffsetRect(pt);
  rc2.OffsetRect(pt);
  if (dwp)
    {
    DeferWindowPos(dwp,*GetDlgItem(IDC_VARIBLE1+twin),NULL,rc1.left,rc1.top,0,0,SWP_NOSIZE|SWP_NOZORDER);
    DeferWindowPos(dwp,*GetDlgItem(IDC_VALUE1+twin),NULL,rc2.left,rc2.top,0,0,SWP_NOSIZE|SWP_NOZORDER);
    }
  else
    {
    GetDlgItem(IDC_VARIBLE1+twin)->SetWindowPos(NULL,rc1.left,rc1.top,0,0,SWP_NOZORDER|SWP_NOSIZE);
    GetDlgItem(IDC_VALUE1+twin)->SetWindowPos(NULL,rc2.left,rc2.top,0,0,SWP_NOZORDER|SWP_NOSIZE);
    }
  }

void CThreadPropExDlg::OnSize(UINT nType, int cx, int cy) 
  {  
  CDialog::OnSize(nType, cx, cy);
  if (nType!=SIZE_MINIMIZED && GetDlgItem(IDC_VARIBLE1)!=NULL) RecalcLayout(cx,cy);	
  }

void CThreadPropExDlg::PlaceLeft(CPoint &pt, int ys)
  {
  CRect rc;
  rc.left=pt.x-_orwndsz.cx;
  rc.top=pt.y;
  rc.right=pt.x;
  rc.bottom=pt.y+ys;
  _yresize=false;
  MoveWindow(&rc);
  }

void CThreadPropExDlg::PlaceBottom(CPoint &pt, int xs)
  {
  CRect rc;  
  rc.left=pt.x;
  rc.top=pt.y;
  rc.right=pt.x+xs;
  rc.bottom=pt.y+_lasty;
  _yresize=true;
  MoveWindow(&rc);
  HideLines();
  rc.bottom=pt.y+_lasty;
  MoveWindow(&rc);
  }

void CThreadPropExDlg::PlaceRight(CPoint &pt, int ys)
  {
  CRect rc;  
  rc.left=pt.x;
  rc.top=pt.y;
  rc.right=pt.x+_orwndsz.cx;
  rc.bottom=pt.y+ys;
  _yresize=false;
  MoveWindow(&rc);
  }

void CThreadPropExDlg::OnPaint() 
{
	CPaintDC dc(this); // device context for painting
    CRect rc;
    GetClientRect(&rc);
    rc.bottom=rc.top+GetSystemMetrics(SM_CYSMCAPTION);
	  DrawCaption(&dc,&rc,DC_ACTIVE|DC_TEXT|DC_SMALLCAP);
}

int CThreadPropExDlg::GetDefaultWidth()
  {
  return _orwndsz.cx;
  }

void CThreadPropExDlg::OnEditchangeVarible1()
  {
//  if (_update)
    {
    const MSG *msg=GetCurrentMessage();
    int id=LOWORD(msg->wParam);
    SyncLists(id);
    _update=false;
    }
  }

void CThreadPropExDlg::OnEditupdateVarible1()
  {
  _update=true;	
  }

void CThreadPropExDlg::OnTimer(UINT nIDEvent) 
  {
  HideLines();	
  CDialog::OnTimer(nIDEvent);
  }

void CThreadPropExDlg::HideLines()
  {  
  int i;
  for (i=CTPD_LINECOUNT-1;i>0;i--)
    {
    if (wVarible[i-1].GetWindowTextLength()==0)
      {
      wVarible[i].ShowWindow(SW_HIDE);
      wValue[i].ShowWindow(SW_HIDE);
      }  
    else
      break;
    }
  int oldy=_lasty;
  CRect twrc=GetTwinRect(i);
  _lasty=twrc.bottom+VSPACE+2*GetSystemMetrics(SM_CXDLGFRAME);
  if (_lasty!=oldy) 
    GetOwner()->PostMessage(SDM_READJUSTWINDOW);
  for (;i>=0;i--)
    {
    wVarible[i].ShowWindow(SW_SHOW);
    wValue[i].ShowWindow(SW_SHOW);
    }
  if (_yresize) AdjustYSize();
  }

void CThreadPropExDlg::AdjustYSize()
  {
  CRect wpos;
  GetWindowRect(&wpos);
  wpos.bottom=wpos.top+_lasty;
  MoveWindow(&wpos);
  }

int CThreadPropExDlg::GetDefaultHeight()
  {
  CRect twrc=GetTwinRect(0);
  return _lasty;
  }

void CThreadPropExDlg::ParseProperties()
  {
  CStringArray arr;
  ParseProperties(properties,arr);
  for (int i=0;i<arr.GetSize();i++)
    {
    int pos=i/2;
    if (i & 1) 
      {
      wValue[pos].SetWindowText(arr.GetAt(i));
      value[pos]=arr.GetAt(i);
      }
    else
      {
      wVarible[pos].SetWindowText(arr.GetAt(i));
      varible[pos]=arr.GetAt(i);
      }
    }  
  }

//--------------------------------------------------

void CThreadPropExDlg::ParseProperties(const char *line, CStringArray &array)
  {
  char buff[256];
  int c;
  istrstream in((char *)line);
  bool data=false;
  CString comd,dta;
  do
    {
    ostrstream out(buff,256);
    c=in.get();
    while (c!=':' && c!=',' && c!=EOF)
      {
      out.put((char)c);
      c=in.get();
      }
    out.put((char)0);
    if (c==':')
      {
      data=true;
      comd=buff;
      }
    else
      {
      if (data)
        {
        dta=buff;data=false;
        }
      else
        {
        comd=buff;dta="";
        }
      comd.TrimLeft();
      comd.TrimRight();
      dta.TrimLeft();
      dta.TrimRight();
      array.Add(comd);
      array.Add(dta);
      }
    }
    while (c!=EOF);
  }

//--------------------------------------------------

CString CThreadPropExDlg::BuildProperties(CStringArray &array)
  {
  CString out;
  for(int i=0;i<array.GetSize();i++)
    {
    CString txt=array.GetAt(i);
    if (i==0) out=txt;
    else if (i & 1 ) 
      {if (txt!="") out=out+":"+txt;}
    else out=out+","+txt;
    }
  return out;
  }

//--------------------------------------------------

void CThreadPropExDlg::SyncLists(int id)
  {
  CString var;
  CComboBox	*ccb=(CComboBox	*)GetDlgItem(id);
  ccb->GetWindowText(var);
  CComboBox	*ncb=(CComboBox	*)GetDlgItem(id+20);
  LoadValuesCombo(*ncb, var);
  }

//--------------------------------------------------

void CThreadPropExDlg::SyncLists()
  {
  for (int i=0;i<CTPD_LINECOUNT;i++) SyncLists(IDC_VARIBLE1+i);
  }

//--------------------------------------------------

void CThreadPropExDlg::LoadNamesCombo(CComboBox &cb)
  {
  SThreadProp_TName *pr=vallist;
  cb.ResetContent();
  while (pr)
    {
    cb.AddString(pr->name);
    pr=pr->next;
    }
  }

//--------------------------------------------------

void CThreadPropExDlg::LoadValuesCombo(CComboBox &cb, const char *name, bool reset)
  {
  SThreadProp_TValue *vl=vallist->FindValues(name);
  CString cont;
  cb.GetWindowText(cont);
  if (reset) cb.ResetContent();
  while (vl)
    {
    cb.AddString(vl->value);
    vl=vl->next;
    }  
  cb.SetWindowText(cont);
  }


void CThreadPropExDlg::OnOK()
{

}

void CThreadPropExDlg::OnCancel()
{

}

bool CThreadPropExDlg::Save(bool saveini)
  {
  if (UpdateData()==FALSE) return false;
  CStringArray arr;
  for (int i=0;i<CTPD_LINECOUNT;i++)
    {
    value[i].TrimLeft();
    value[i].TrimRight();
    varible[i].TrimLeft();
    varible[i].TrimRight();	
    if (varible[i]!="")
      {
      int pos;
      while ((pos=varible[i].Find(' '))>=0) varible[i].SetAt(pos,'_');
      while ((pos=value[i].Find(' '))>=0) value[i].SetAt(pos,'_');
      if (value[i]!="") vallist->Add(&vallist,varible[i],value[i],false);
      arr.Add(varible[i]);
      arr.Add(value[i]);
      }
    }
  properties=BuildProperties(arr);
  if (saveini) 
  {
	SThreadProp_TName::Save(vallist,SThreadProp_VALSNAME1 );
	_delet=false;
  }
  return true;
  }

BOOL CThreadPropExDlg::PreTranslateMessage(MSG* pMsg) 
  {
  if (pMsg->message==WM_CHAR && pMsg->wParam==20)
    {
    GetOwner()->SetFocus();
    return TRUE;
    }
  if (pMsg->message==WM_KEYDOWN && pMsg->wParam==VK_DELETE)
    {
    int i;
    CWnd *wnd=GetFocus();
    if (wnd!=NULL) wnd=wnd->GetParent();
    CComboBox *cb=dynamic_cast<CComboBox *>(wnd);
    for (i=0;i<CTPD_LINECOUNT;i++) if (&wValue[i]==cb) break;
    if (cb && i!=CTPD_LINECOUNT && cb->GetDroppedState())
      {
      int p=cb->GetCurSel();
      if (p!=-1)
        {
        CString var;
        wVarible[i].GetWindowText(var);
        CString val;
        cb->GetLBText(p,val);
        SThreadProp_TName *nam=vallist->Find(var);
        if (nam)
          {
          SThreadProp_TValue *p=nam->values;
          if (p && stricmp(p->value,val)==0)
            {
            nam->values=p->next;
            delete p;
            }
          else
            {
            while (p->next!=NULL)
              {
              if (stricmp(p->next->value,val)==0)
                {
                SThreadProp_TValue *q=p->next;
                p->next=q->next;
                delete q;
                break;
                }
              p=p->next;			
              }
            }
          if (nam->values==NULL)
            {
            SThreadProp_TName *q=vallist;
            if (q==nam) 
              {
              vallist=q->next;
              }
            else
              {
              while (q && q->next!=nam) q=q->next;
              if (q->next==nam)
                {
                q->next=nam->next;				
                }
              }
            nam->next=NULL;
            delete nam;
            }
          }
        for (i=0;i<CTPD_LINECOUNT;i++) 
          {
          wVarible[i].GetWindowText(var);
          LoadNamesCombo(wVarible[i]);	
          wVarible[i].SetWindowText(var);
          }
        SyncLists();
        cb->SetWindowText("");
		_delet=true;
      return TRUE;
      }
    }
  }
if (IsDialogMessage(pMsg)) return TRUE;
return CDialog::PreTranslateMessage(pMsg);
}


CThreadPropExDlg::~CThreadPropExDlg()
  {
  delete vallist;

  }

void CThreadPropExDlg::OnDestroy() 
{
  if (_delet && AfxMessageBox(IDS_PROPWNDDELETEQUESTION,MB_YESNO|MB_ICONQUESTION)==IDYES)
  {
	Save(true);
  }
  CDialog::OnDestroy();
	
	
}
