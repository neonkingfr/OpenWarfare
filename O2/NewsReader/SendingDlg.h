#if !defined(AFX_SENDINGDLG_H__6E465D20_BA9B_11D5_B3A0_0050FCFE63F1__INCLUDED_)
#define AFX_SENDINGDLG_H__6E465D20_BA9B_11D5_B3A0_0050FCFE63F1__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SendingDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CSendingDlg dialog

class CSendingDlg : public CDialog
{
// Construction
  CImageListEx animace;
  int frame;
public:
	void OnOK();
	CSendingDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CSendingDlg)
	enum { IDD = IDD_SENDING };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSendingDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CSendingDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnTimer(UINT nIDEvent);
	virtual void OnCancel();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SENDINGDLG_H__6E465D20_BA9B_11D5_B3A0_0050FCFE63F1__INCLUDED_)
