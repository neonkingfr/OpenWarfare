// NewsTerminal.h: interface for the CNewsTerminal class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_NEWSTERMINAL_H__F78E21FF_B267_430B_87AB_C5CD5E118C67__INCLUDED_)
#define AFX_NEWSTERMINAL_H__F78E21FF_B267_430B_87AB_C5CD5E118C67__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "LineTerminal2.h"
#include "ArchiveStreamTCP.h"

#define CLT_OK 0
#define CLT_READTIMEOUT -1
#define CLT_WRITETIMEOUT -2
#define CLT_CONNECTIONRESET -3
#define CLT_INVALIDREPLY -5
#define CLT_CONNECTFAILED -6
#define CLT_INTERRUPTED -7
#define CNW_ACCESDENIED -100
#define CNW_INVALIDPARAMS -101
#define CNW_COMMANDREJECTED -102
#define CNW_GROUPHASBEENDELETED -103
#define CNW_INVALIDREPLY -999

class CNewsGroup;

#define NRTIMEOUT 30000
#define NRSMALLTIMEOUT 500
#define NRIDLELOOPS ((NRSMALLTIMEOUT)/(NRSMALLTIMEOUT))



class CNewsTerminal : public LineTerminal
  {
  friend long CNewsTerminal_TimeoutFunction(ArchiveStreamTCP *caller, void *context, int counter);
  protected:
	Socket _s; ///< Internal socket handle
	ArchiveStreamTCPIn _tcpin; ///<Input tcp stream (it use the same socket)
	ArchiveStreamTCPOut _tcpout; ///<Output tcp stream (it use the same socket)
	int _timeoutquants; ///<Number of timeoutquants to drop connections
	

	class StreamScan: public ArchiveStream
	  {
	  friend long CNewsTerminal_TimeoutFunction(ArchiveStreamTCP *caller, void *context, int counter);
	  public:
		static HANDLE debugout;	  
		static bool StopAll;	//Static member to stop all trafic
		ArchiveStream &_next;

		StreamScan(ArchiveStream &next):_next(next) {}
	    int DataExchange(void *buffer, int maxsize);
		int IsError() {return _next.IsError();}
		void SetError(int err) {_next.SetError(err);}
		void Reset() {_next.Reset();}
		__int64 Tell() {return _next.Tell();}
		void Seek(__int64 lOff, SeekOp seekop)  {_next.Seek(lOff,seekop);}
		bool IsStoring() {return _next.IsStoring();}
		bool IsCounting() {return _next.IsCounting();}
		void Reserved(int bytes)  {_next.Reserved(bytes);}
		void Flush()  {_next.Flush();}
	  };

	StreamScan _scanin;
	StreamScan _scanout;

  public:

	CNewsTerminal():
		_tcpin(_s,0,false,CNewsTerminal_TimeoutFunction,(void *)&_timeoutquants),
		_tcpout(_s,0,false,CNewsTerminal_TimeoutFunction,(void *)&_timeoutquants),
		_scanin(_tcpin),
		_scanout(_tcpout),
		LineTerminal(_scanin,_scanout),
		_s(SOCK_STREAM),
		_timeoutquants(NRIDLELOOPS)
		  {}

	CNewsTerminal(const CNewsTerminal& _other):
		_tcpin(_s,0,false,CNewsTerminal_TimeoutFunction,(void *)&_timeoutquants),
		_tcpout(_s,0,false,CNewsTerminal_TimeoutFunction,(void *)&_timeoutquants),
		_scanin(_tcpin),
		_scanout(_tcpout),
		LineTerminal(_scanin,_scanout),
		_timeoutquants(_other._timeoutquants),
		_s(_other._s) {}

	CNewsTerminal(const Socket other):
		_s(other),
		_tcpin(_s,0,false,CNewsTerminal_TimeoutFunction,(void *)&_timeoutquants),
		_tcpout(_s,0,false,CNewsTerminal_TimeoutFunction,(void *)&_timeoutquants),
		_scanin(_tcpin),
		_scanout(_tcpout),
		LineTerminal(_scanin,_scanout),
		_timeoutquants(NRIDLELOOPS)
		  {}
	
	int ReceiveNumReply();


  void SetTimeoutQuants(int quants) {_timeoutquants=quants;}
  static void StopTrafic();
  static bool IsStopped() {return StreamScan::StopAll;}
  int ShowError(int result);
  operator Socket() {return _s;}
  bool IsConnected() {return _s.IsValid() && _s.IsConnected();}
  bool Quit();
  int Authorize(const char *username, const char *password);
  int GetListOfGroups(CTime last, CNewsGroup *addend);
  int Connect(const char *address, int port);



  static bool DebugOpened();
  static void CloseDebug();
  static void OpenDebug();

  };

/*
class CNewsTerminal : public CLineTerminal  
{
  CString line;
public:
	int ShowError(int result);
	void Close();
	const char * GetTextReply() {return line;}
	int Authorize(const char *username, const char *password);
	CNewsTerminal();
	virtual ~CNewsTerminal();

};
*/


#endif // !defi0ned(AFX_NEWSTERMINAL_H__F78E21FF_B267_430B_87AB_C5CD5E118C67__INCLUDED_)
