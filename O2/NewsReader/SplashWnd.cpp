// SplashWnd.cpp : implementation file
//

#include "stdafx.h"
#include "newsreader.h"
#include "SplashWnd.h"
#include "NewsAccount.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSplashWnd

#ifndef WS_EX_LAYERED
#define WS_EX_LAYERED           0x00080000
#define LWA_COLORKEY            0x00000001
#define LWA_ALPHA               0x00000002
#endif


typedef BOOL (_stdcall *def_SetLayeredWindowAttributes)(          HWND hwnd,
														COLORREF crKey,
														BYTE bAlpha,
														DWORD dwFlags
														);

static def_SetLayeredWindowAttributes mySetLayeredWindowAttributes=NULL;


CSplashWnd::CSplashWnd()
{
}

CSplashWnd::~CSplashWnd()
{
}


BEGIN_MESSAGE_MAP(CSplashWnd, CWnd)
//{{AFX_MSG_MAP(CSplashWnd)
ON_WM_PAINT()
ON_WM_TIMER()
	ON_WM_CLOSE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// CSplashWnd message handlers

void CSplashWnd::Create(CWnd *parent)
{
  pic.LoadBitmap(IDB_SPLASH);
  BITMAP bp;
  pic.GetBitmap(&bp);
  CRect scr;
  GetDesktopWindow()->GetWindowRect(&scr);
  CRect rc(0,0,bp.bmWidth+2,bp.bmHeight+2);
  animx=bp.bmWidth;
  rc+=CSize(scr.right/2-bp.bmWidth/2,scr.bottom/2-bp.bmHeight/2);
  this->CreateEx(WS_EX_LAYERED,AfxRegisterWndClass(0),"Splash",
	WS_POPUP|WS_BORDER,rc,parent,0,NULL);  
  imglst.Create(IDB_SPLASHANIM,40,0,RGB(255,255,255));
  SetTimer(1,100,NULL);
  cntr=0;
  tmr=0;
  HMODULE mod=GetModuleHandle("User32.dll");
  if (mod!=NULL)
  {
	mySetLayeredWindowAttributes=(def_SetLayeredWindowAttributes)GetProcAddress(mod,"SetLayeredWindowAttributes");
  }
  if (mySetLayeredWindowAttributes)
  {
	DWORD tm=GetTickCount();
	DWORD curtm=tm;
	while (curtm-tm<1000)
	{
	  int alpha=(curtm-tm)*255/1000;
	  mySetLayeredWindowAttributes(*this,0,alpha,LWA_ALPHA);
	  if (alpha==0) {ShowWindow(SW_SHOW);UpdateWindow();}
	  Sleep(1);
	  curtm=GetTickCount();
	}
	mySetLayeredWindowAttributes(*this,0,255,LWA_ALPHA);
  }
}

void CSplashWnd::OnPaint() 
{
  CPaintDC dc(this); // device context for painting
  OnDraw(dc);
  // Do not call CWnd::OnPaint() for painting messages
}


void CSplashWnd::OnTimer(UINT nIDEvent) 
{
  DWORD res;
  if (tmr==0)
  {
	if (::SendMessageTimeout(*theApp.m_pMainWnd,WM_COMMAND,0xFFFF,0,SMTO_NORMAL,1,&res)==TRUE)
	{
	  PostQuitMessage(0);
	  theApp.m_pMainWnd->ShowWindow(SW_SHOW);
	  theApp.m_pMainWnd->SetForegroundWindow();
	}
	tmr=10;
  }
  else
	tmr--;
  CRect rpc;
  GetClientRect(&rpc);
  animx-=5;
  if (animx<130) animx=rpc.right;
  rpc.left=animx;
  rpc.top=5;
  rpc.right=animx+50;
  rpc.bottom=25;
  if (rpc.left<200) rpc.left=200;
  InvalidateRect(&rpc);
  imglst.SetBkColor(CLR_NONE);
  cntr++;
  if (cntr>=imglst.GetImageCount()) cntr=0;
  runflag=true;
}
/////////////////////////////////////////////////////////////////////////////
// CAboutWindow

CAboutWindow::CAboutWindow()
{
}

CAboutWindow::~CAboutWindow()
{
}


BEGIN_MESSAGE_MAP(CAboutWindow, CSplashWnd)
//{{AFX_MSG_MAP(CAboutWindow)
ON_WM_PAINT()
ON_COMMAND(IDOK,OnOK)
//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// CAboutWindow message handlers

//DEL void CAboutWindow::OnLButtonDown(UINT nFlags, CPoint point) 
//DEL 	{
//DEL 	DestroyWindow();
//DEL 	
//DEL 	CSplashWnd::OnLButtonDown(nFlags, point);
//DEL 	}

//DEL void CAboutWindow::OnRButtonDown(UINT nFlags, CPoint point) 
//DEL 	{
//DEL 	DestroyWindow();
//DEL 
//DEL 	CSplashWnd::OnRButtonDown(nFlags, point);
//DEL 	}

void CAboutWindow::Create(CWnd *owner)
{
  CSplashWnd::Create(owner);
  CenterWindow(owner);
  tmr=1000;
  CRect rc;
  GetClientRect(&rc);
  CreateWindow("BUTTON","X",WS_VISIBLE|BS_PUSHBUTTON|BS_TEXT|WS_CHILD,rc.right-25,5,20,20,
	*this,(HMENU)IDOK,AfxGetInstanceHandle(),NULL);
  KillTimer(1);
}	  

//DEL void CAboutWindow::OnActivate(UINT nState, CWnd* pWndOther, BOOL bMinimized) 
//DEL {
//DEL 	CSplashWnd::OnActivate(nState, pWndOther, bMinimized);
//DEL 	
//DEL 	if (nState==WA_INACTIVE) DestroyWindow();
//DEL 	
//DEL }

void CAboutWindow::OnOK()
{
  DestroyWindow();
}

void CSplashWnd::PostNcDestroy() 
{
  pic.DeleteObject();
  imglst.DeleteImageList();
  CWnd::PostNcDestroy();	
}

void CSplashWnd::OnDraw(CDC& dc)
{
  BITMAP bp;	
  pic.GetBitmap(&bp);
  
  CDC bitmapdc;
  bitmapdc.CreateCompatibleDC(&dc);
  CBitmap *old=bitmapdc.SelectObject(&pic);
  dc.BitBlt(0,0,bp.bmWidth,bp.bmHeight,&bitmapdc,0,0,SRCCOPY);
  bitmapdc.SelectObject(old);	
  CRect rc;
  GetClientRect(&rc);
  imglst.Draw(&dc,cntr,CPoint(animx,5),ILD_NORMAL);
}

void CAboutWindow::OnPaint() 
{
  CPaintDC dc(this); // device context for painting
  
  CRect rc;
  CSplashWnd::OnDraw(dc);
  GetClientRect(&rc);
  rc.top=rc.bottom-28;
  rc.left=5;
  rc.right=200;
  CFont fnt,*sysfnt;
  fnt.CreateFont(12,0,0,0,400,0,0,0,1,OUT_DEFAULT_PRECIS,CLIP_DEFAULT_PRECIS,
	DEFAULT_QUALITY ,DEFAULT_PITCH|FF_DONTCARE,"Arial");
  sysfnt=dc.SelectObject(&fnt);	
  CString dver=theApp.GetVersionString();	
  dc.DrawTextA(dver,-1,&rc,DT_LEFT |DT_NOPREFIX |DT_TOP |DT_WORDBREAK );
  dc.SelectObject(sysfnt);
  // Do not call CSplashWnd::OnPaint() for painting messages
}
/////////////////////////////////////////////////////////////////////////////
// CSplashOut

CSplashOut::CSplashOut()
{
}

CSplashOut::~CSplashOut()
{
}


BEGIN_MESSAGE_MAP(CSplashOut, CWnd)
//{{AFX_MSG_MAP(CSplashOut)
ON_WM_PAINT()
ON_WM_TIMER()
//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// CSplashOut message handlers

void CSplashOut::Create()
{
  CreateEx(WS_EX_TOPMOST|WS_EX_TRANSPARENT,AfxRegisterWndClass(0,0,0),"",WS_VISIBLE|WS_POPUP,
	5,5,52,52,NULL,NULL,NULL);
  imglst.Create(IDB_DOGANIM3,50,0,RGB(255,0,255));
  pos=0;
  imglst.SetBkColor(CLR_NONE);
  SetTimer(1,150,NULL);
}

void CSplashOut::OnPaint() 
{
  CPaintDC dc(this); // device context for painting
  
  imglst.Draw(&dc,pos,CPoint(1,1),ILD_NORMAL);
  // Do not call CWnd::OnPaint() for painting messages
}

void CSplashOut::OnTimer(UINT nIDEvent) 
{
  pos++;
  if (pos>=imglst.GetImageCount()) pos=0;
  Invalidate();	
  CWnd::OnTimer(nIDEvent);
}

void CSplashWnd::Close() 
{
  if (mySetLayeredWindowAttributes)
  {
	DWORD tm=GetTickCount();
	DWORD curtm=tm;
	while (curtm-tm<1000)
	{
	  int alpha=(curtm-tm)*255/1000;
	  mySetLayeredWindowAttributes(*this,0,255-alpha,LWA_ALPHA);
	  Sleep(1);
	  curtm=GetTickCount();
	}	
	  DestroyWindow();
  }
}
