#if !defined(AFX_SPLITWINDOW_H__1F4DEBDA_480E_4F1C_BB69_675C63CBC919__INCLUDED_)
#define AFX_SPLITWINDOW_H__1F4DEBDA_480E_4F1C_BB69_675C63CBC919__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SplitWindow.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CSplitWindow window

#define CSPW_RESIZE 0x1
#define CSPW_AUTODESTROY 0x2


class CSplitWindow : public CWnd
{
// Construction
protected:
  bool spliten;
  int splitpos;
  int splitpospx;
  float splitposper;
  int touchsize;
  CWnd *first;
  CWnd *second;
  long fflags;
  long sflags;
  static LPCTSTR classname;  
  
public:
  bool autodelete;
	CSplitWindow();

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSplitWindow)
	public:
	virtual BOOL OnCmdMsg(UINT nID, int nCode, void* pExtra, AFX_CMDHANDLERINFO* pHandlerInfo);
	protected:
	virtual LRESULT WindowProc(UINT message, WPARAM wParam, LPARAM lParam);
	virtual void PostNcDestroy();
	//}}AFX_VIRTUAL

// Implementation
public:
	CWnd * FindChild(UINT uid);
	int GetSplitPos() {return splitpos;}
	float GetSplitPosF() {return splitposper;}
	virtual void SetSplitPos(float pos)=0;
	virtual void SetSplitPos(int pos)=0;
	virtual void RecalcLayout()=0;
	void Create(CWnd *parent, int splitpos=100, UINT id=0);
	void Create(CWnd *parent,float splitpos=0.5f,UINT uid=0);
	virtual ~CSplitWindow();
	void EnableResize1(bool enable)
	  {if (enable) fflags|=CSPW_RESIZE;else fflags&=~CSPW_RESIZE;}
	void EnableResize2(bool enable)
	  {if (enable) sflags|=CSPW_RESIZE;else sflags&=~CSPW_RESIZE;}
	void AutoDestroy1(bool enable)
	  {if (enable) fflags|=CSPW_AUTODESTROY;else fflags&=~CSPW_AUTODESTROY;}
	void AutoDestroy2(bool enable)
	  {if (enable) sflags|=CSPW_AUTODESTROY;else sflags&=~CSPW_AUTODESTROY;}
	void EnableChangePos(bool en) {spliten=en;}
	void ChangeFrame(int frmnum, CWnd *frame);
	DWORD GetFlags1() {return fflags;}
	DWORD GetFlags2() {return sflags;}
	int GetFrameId(CWnd *wnd)
	  {return (int)(first==wnd)+2*(int)(second==wnd);}
	virtual char Identify()=0;

	// Generated message map functions
protected:
	void LoadChilds();
	//{{AFX_MSG(CSplitWindow)
	afx_msg void OnPaint();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

/////////////////////////////////////////////////////////////////////////////
// CSplitWindowVert window

class CSplitWindowVert : public CSplitWindow
{
	int lastcx;
	int lastmsx;
// Construction
public:
	CSplitWindowVert();

// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSplitWindowVert)
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CSplitWindowVert();

	virtual void SetSplitPos(float pos);
	virtual void SetSplitPos(int pos);
	virtual void RecalcLayout();
	virtual char Identify() {return 'v';}

	// Generated message map functions
protected:
	//{{AFX_MSG(CSplitWindowVert)
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg BOOL OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////
// CSplitWindowHorz window

class CSplitWindowHorz : public CSplitWindow
{

  int lastcy;
  int lastmsy;
// Construction
public:
	CSplitWindowHorz();

// Attributes
public:

// Operations
public:

	virtual void SetSplitPos(float pos);
	virtual void SetSplitPos(int pos);
	virtual void RecalcLayout();
	virtual char Identify() {return 'h';}

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSplitWindowHorz)
	//}}AFX_VIRTUAL

// Implementation
public:
	virtual ~CSplitWindowHorz();

	// Generated message map functions
protected:
	//{{AFX_MSG(CSplitWindowHorz)
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnLButtonUp(UINT nFlags, CPoint point);
	afx_msg void OnMouseMove(UINT nFlags, CPoint point);
	afx_msg BOOL OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

#endif
