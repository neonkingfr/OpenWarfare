// NewsAccount.cpp: implementation of the CNewsAccount class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "newsreader.h"
#include "MainFrm.h"
#include "NewsAccount.h"
#include "NewsArticle.h"
#include "Archive.h"
#include "ArchiveInline.h"
#include "NewsTerminal.h"
using namespace std;


#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

#include "EditAccountDlg.h"

#define ACCELERATE theApp.config.acc_cmd

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////



BTree<CString> sharedTable;


int CNewsGroup::maxarticles=300;

CNewsAccount::CNewsAccount()
  {
  grlist=NULL;
  last=CTime(1980,1,1,0,0,0);
  port=119;
  offline=0;
  viewastree=false;
  newgr=NULL;refcount=1;  
  COleDateTime tmq=COleDateTime::GetCurrentTime();
  fname=tmq.Format("%y%m%d%H%M%S");
  daysarchive=60;
  daysdelete=0;
  delcheck=0;
  compacto=50;
  autocompact=false;
  extsync=false;
  lastread.SetStatus(COleDateTime::null);
  loadstartup=true;
  startuptime=true;
  loaded=true;
  newondemand=false;
  nonewnotify=false;
  forcedownloadnew=false;
  timeout=30000;
  fullTextIgnoredWords="> re to a se na je v - by ze jsem s ale tak do i z pro si mi o 1 co tam uz";
  fullTextOpt=0;
  fullTextInclude=FTXI_DEFAULT;
  saveinpreviousver=false;
  fullTextOptAppend=false;
  mutex.Lock();
  mutex.Unlock();
  }

//--------------------------------------------------

CNewsAccount::~CNewsAccount()
  {
  delete grlist;  
  }

//--------------------------------------------------

void CNewsAccount::RemoveIndexedMark()
{
  CNewsGroup *grp=grlist;
  while (grp)
  {
	CNewsArtHolder *hld=grp->GetList();
	while (hld)
	{
	  hld->indexed=false;
	  hld=hld->EnumNext();
	}
	grp=grp->GetNext();
  }
}

static void EnableIndex(CNewsGroup *grp)
{
  while (grp)
  {
    if (grp->fullload==NGDM_DOWNLOADDISABLED) grp->fullload=NGDM_INDEXONLY;
    else if (grp->fullload==NGDM_STOREBODIES) grp->fullload=NGDM_STOREBODIESINDEX;
    grp=grp->GetNext();
  }
}

bool CNewsAccount::EditInDialog()
  {
  CEditAccountDlg dlg;
  
  dlg.address=address;
  dlg.authorize=needauthorize;
  dlg.username=username;
  dlg.password=password;
  dlg.port=port;
  dlg.timeout=timeout/10000;
  dlg.email=myemail;
  dlg.name=name;
  dlg.myname=myname;
  dlg.time=last.Format("%d.%m.%y %H:%M:%S");
  dlg.urlpic=urlpic;
  dlg.prop.autocompact=autocompact;
  dlg.prop.enablecache=offline!=0;
  dlg.prop.cachesize=offline/1024;
  dlg.prop.compacto=compacto;
  dlg.prop.archdate=daysarchive==0?60:daysarchive;
  dlg.prop.archcreate=daysarchive==0?FALSE:TRUE;
  dlg.prop.deldate=daysdelete==0?120:daysdelete;
  dlg.prop.enabledel=daysdelete==0?FALSE:TRUE;
  dlg.prop.delcheck=2-delcheck;
  dlg.prop.loadstartup=loadstartup;
  dlg.prop.urgent=urgentntf;
  dlg.prop.nonewnotify=nonewnotify;
  dlg.prop.downloadondemand=newondemand;
  dlg.prop.cachepath=cachepath;
  dlg.prop.fulltextopt=fullTextOpt;
  dlg.prop.vIgnoredWords=fullTextIgnoredWords;
  dlg.prop.vFTIndex=&fulltext;
  dlg.prop.fulltextflgs=fullTextInclude;
  dlg.prop.vSaveInPreviousVersion=saveinpreviousver;
  if (dlg.DoModal()==IDCANCEL) return false;
  if (dlg.prop.fulltextopt!=fullTextOpt && dlg.prop.fulltextopt!=0)
  {
    if (NrMessageBox(IDS_FULLTEXTAUTOSET,MB_YESNO)==IDYES)
      EnableIndex(grlist);
  }
  fullTextOpt=dlg.prop.fulltextopt;
  if (fullTextOpt && (dlg.prop.fulltextflgs!=fullTextInclude || dlg.prop.vFullTextReg || dlg.prop.vIgnoredWords!=fullTextIgnoredWords))
  {
	if (((dlg.prop.fulltextflgs | fullTextInclude)!=fullTextInclude) && !dlg.prop.vFullTextReg && dlg.prop.vIgnoredWords==fullTextIgnoredWords)
	{
	  if (NrMessageBox(IDS_FULLTEXTOPTEXTENDED,MB_YESNO)==IDYES) 
	  {
		RemoveIndexedMark();
		fullTextOptAppend=true;
	  }
	}
	else
	{
	  if (NrMessageBox(IDS_FULLTEXTREGENERATE,MB_YESNO)==IDYES) 
	  {
		CNewsTerminal::StopTrafic();
		RemoveIndexedMark();
		fulltext.Reset();
	  }
	}
  }
  fullTextInclude=dlg.prop.fulltextflgs;
  saveinpreviousver=dlg.prop.vSaveInPreviousVersion!=FALSE;
  fullTextIgnoredWords=dlg.prop.vIgnoredWords;
  newondemand=dlg.prop.downloadondemand!=FALSE;
  nonewnotify=dlg.prop.nonewnotify!=FALSE;
  urgentntf=dlg.prop.urgent;
  delcheck=2-dlg.prop.delcheck;
  daysarchive=dlg.prop.archcreate?dlg.prop.archdate:0;
  daysdelete=dlg.prop.enabledel?dlg.prop.deldate:0;
  compacto=dlg.prop.compacto;
  address=dlg.address;
  autocompact=dlg.prop.autocompact!=FALSE;
  needauthorize=dlg.authorize!=0;
  username=dlg.username;
  password=dlg.password;
  port=dlg.port;
  timeout=dlg.timeout*10000;
  myemail=dlg.email;
  myname=dlg.myname;
  name=dlg.name;
  if (cachepath!=dlg.prop.cachepath)
    if (MoveCache(dlg.prop.cachepath)==false)
      NrMessageBox(IDS_UNABLEMOVECACHE);
  urlpic=dlg.urlpic;
  loadstartup=dlg.prop.loadstartup!=FALSE;
  offline=dlg.prop.enablecache?(dlg.prop.cachesize?dlg.prop.cachesize*1024:1024):0;
  InitCache();
  int d,m,r,H,M,S;
  char c1,c2,c3,c4,c5;
  if (sscanf(dlg.time,"%d%c%d%c%d%c%d%c%d%c%d",
  &d,&c1,&m,&c2,&r,&c3,&H,&c4,&M,&c5,&S)==11)
    if (c1=='.' && c2=='.' && c3==' ' && c4==':' && c5==':')
      {
      last=CTime(r+(r<80?2000:1900),m,d,H,M,S);
      }
  Compact(false);
  /*  theApp.Lock();
  ActivateKeywords();
  CNewsGroup *grp=grlist;
  while (grp)
	{
	if (grp->GetList()) 
	  {
	  grp->GetList()->CalcHierarchy();
      grp->GetNews(true);
	  }
	grp=grp->GetNext();
	}
  theApp.Unlock();*/
  return true;
  }

//--------------------------------------------------

bool CNewsAccount::AddGroup(CNewsGroup *grp)
  {
  if (grp==NULL) return false;
  grp->SetNext(grlist);
  grlist=grp;
  return true;
  }

//--------------------------------------------------

bool CNewsAccount::UnlinkGroup(CNewsGroup *grp)
  {
  if (grp==NULL) return false;
  if (grlist=grp)	
    grlist=grp->GetNext();	
  else
    {
    CNewsGroup *gr=grlist;
    while (gr!=NULL && gr->GetNext()!=grp) gr=gr->GetNext();
    if (gr==NULL) return false;
    gr->Unlink();
    gr->SetNext(grp->GetNext());
    }
  grp->Unlink();    
  return true;
  }

//--------------------------------------------------

bool CNewsAccount::AppendGrouplist(CNewsGroup *grp)
  {
  if (grp==NULL) return false;
  CNewsGroup *g=grlist;
  while (g!=NULL && g->GetNext()!=NULL) 
    {
    g=g->GetNext();
    }
  if (g==NULL) 
    {
    grlist=grp;
    }
  else
    {
    g->SetNext(grp);
    }
  return true;
  }

//--------------------------------------------------

void CNewsAccount::DeleteGroups()
  {
  delete grlist;grlist=NULL;
  }

//--------------------------------------------------

CNewsGroup * CNewsGroup::FindGroup(const char *name)
  {
  CNewsGroup *cur=this;
  while (cur)
    {
    if (cur->name==name) return cur;
    cur=cur->next;
    }
  return NULL;
  }

//--------------------------------------------------

void CNewsAccount::Serialize(CGenArchive &arch)
  {
    CString keywords;
    int curFilter;
    int curHighlight;
  arch.version=ACC_VERSION1;
  arch.VarSerial(arch.version);
  arch.Serialize(name);
  arch.Serialize(address);
  arch.VarSerial(port);
  arch.Serialize(username);
  arch.Serialize(password);
  arch.Serialize(myname);
  arch.Serialize(myemail);
  arch.Serialize(last);
  CTimeSpan timeout;
  arch.Serialize(timeout);  
  arch.VarSerial(needauthorize);	  
  arch.VarSerial(grlist);
  arch.Serialize(urlpic);
  arch.VarSerial(offline);
  arch.Serialize(keywords);
  arch.VarSerial(viewastree);
  arch.VarSerial(compacto);
  if (arch.version<0x106 && !arch.IsStoring) compacto=50;
  if (arch.version>=0x107) arch.VarSerial(autocompact);
  if (arch.version>=0x109) arch.VarSerial(loadstartup);
  if (arch.version>=0x110) arch.Serialize(urgentntf);
  if (arch.version>=0x111) 
    {
    arch.VarSerial(newondemand);
    arch.VarSerial(nonewnotify);
    }
  if (arch.version>=0x113)
    {
    arch.Serialize(cachepath);
    }
  if (arch.version>=0x104)
    {
    arch.VarSerial(daysarchive);
    arch.VarSerial(daysdelete);
    arch.VarSerial(curFilter);
    arch.VarSerial(delcheck);
    arch.VarSerial(curHighlight);
    arch.Reserve(7,0);
    }
  else
    arch.Reserve(2,0);
  if ((startuptime && loadstartup) || !startuptime || arch.IsStoring)
    {
    if (grlist && !arch.IsStoring)
      {
      grlist=new CNewsGroup();
      }
    //  theApp.Lock();
    //  ActivateKeywords();
    if (grlist) grlist->Serialize(arch);
    //  theApp.Unlock();
    loaded=true;
    dirty=false;
    if (!arch.IsStoring) 
      {
      CNewsGroup *grp=grlist;
      COleDateTime dtma=COleDateTime::GetCurrentTime();
      COleDateTime dtmd=COleDateTime::GetCurrentTime();
      dtma-=COleDateTimeSpan(daysarchive,0,0,0);
      dtmd-=COleDateTimeSpan(daysdelete,0,0,0);
      while (grp)
        {
        if (grp->GetList())
          {
          grp->Compact(NULL,0,true);	  
          grp->CountNewsAndHighlights();
          //	  if (daysarchive) grp->Archivate(dtma);
          //	  if (daysdelete) grp->GetList()->DeleteOlder(dtmd);
          }
        grp=grp->GetNext();
        }
      ApplyCond(false);
      ApplyHighlight(false);
      PostApplyConds();
      }
    }
  else
    {
    loaded=false;
    grlist=NULL;
    }
  InitCache();
  startuptime=false;
  }

//--------------------------------------------------

void CNewsGroup::Serialize(CGenArchive &arch)
  {
  CRWLockGuard guard(articleLock,!arch.IsStoring);guard.LockPM();
  CNewsGroup *p=this;
  while (p)
    {
    if (!p->deleted)
      {
      if (arch.IsStoring)
        {
        if (!p->sign)
          {
          /*  if (p->list) p->list->Release();
		  p->list=NULL;*/
          }
        else
          p->list=p->list->RebuildRemoveOld(p->minindex);
        if (p->list==NULL) p->nlindex=p->minindex;
        }	
      arch.Serialize(p->name);
      arch.VarSerial(p->minindex);
      arch.VarSerial(p->maxindex);
      if (arch.version>=0x102)
        arch.VarSerial(p->nlindex);
      else
        p->nlindex=p->maxindex;
      if (arch.version>=0x112) arch.VarSerial(p->notify);
      arch.VarSerial(p->next);  
      arch.VarSerial(p->sign);
      bool sign;
      if (arch.IsStoring && !p->sign)
        {
        CNewsArtHolder *null=NULL;
        arch.VarSerial(null);
        sign=false;
        }
      else
        {
        arch.VarSerial(p->list);
        sign=p->list!=NULL;
        }
      arch.VarSerial(p->fullload);
      arch.VarSerial(p->_sortitem);
      arch.Reserve(11,0);
      if (!arch.IsStoring)
        {
        if (p->next) p->next=new CNewsGroup();	  
        if (p->list) p->list=new CNewsArtHolder();
        p->newcount=0;
        }	  
      if (sign) 
        p->list->Serialize(arch,p->minindex);
      }
    p=p->next;
    }
  }

//--------------------------------------------------

int CNewsAccount::OpenConnection(CNewsTerminal &term)
  {
  theApp.Lock();
  CString addr=GetAddress();
  int port=GetPort();
  CString username=GetUsername();
  CString password=GetPassword();
  bool needauth=GetAuthNeed();
  theApp.Unlock();
  term.SetTimeoutQuants(timeout/NRSMALLTIMEOUT);
  int p=term.ShowError(term.Connect(addr,port));		
  if (p) return p;
  if (needauth)
    {
    p=term.ShowError(term.Authorize(username,password));
    if (p) return p;
    }
  return 0;
  }

//--------------------------------------------------

static int __cdecl hldl_compare(const void *elm1, const void *elm2)
  {
  CNewsArtHolder **h1=(CNewsArtHolder **)elm1;
  CNewsArtHolder **h2=(CNewsArtHolder **)elm2;
  return ((*h1)->GetIndex()>(*h2)->GetIndex())*2-1;
  }

//--------------------------------------------------

int CNewsGroup::ReadGroup(CNewsTerminal &term,bool extended,const char *name,CMsgIdList &hash,long *urgent)
  {
  int result;
  int spos=theApp.config.newontop?0:9999999;  
  term.SendFormatted("GROUP %s",GetName());
  result=term.ReceiveNumReply();
  if (result/10==41) 
    {
    deleted=true;
    sign=false;
    return CNW_GROUPHASBEENDELETED;
    }
  if (result/10!=21) return result;
  int a,b,c;
  int min,max;
  sscanf(term.GetLastMessage()+4,"%d %d %d",&a, &b, &c);
  min=b;
  max=c;
  bool acc=false;
  int lw=extended?0:nlindex;
  if (lw>max) return 0;
  if (lw<min) lw=min;
  if (theApp.config.maxdownload)
    {
    if (max-lw>(signed)theApp.config.maxcount) lw=max-theApp.config.maxcount;
    }
  CNewsArtHolder *seznam=NULL, *ap=NULL;
  term.SendFormatted("XHDR Message-ID %d-%d",lw,max);
  int err=term.ReceiveNumReply()/10;
  bool xhdr=false;
  CString msgid;
  if (err==22) xhdr=true;
  else 
    {
    term.SendFormatted("STAT %d",lw);
    err=term.ReceiveNumReply()/10;
    }
  if (err!=22 && lw<max) 
    {
    term.SendFormatted("NEXT");
    err=term.ReceiveNumReply()/10;
    }
  if (err==22)
    {
	CString line;
    int i;
    int art;
    int acccount=ACCELERATE;
    int msgcntr=0;
    if (xhdr) 
      line=term.ReadLine();
    else
      for (i=0;i<acccount;i++) term.SendLine("NEXT");	  	      
    while (err==22 && line!="." && line!="")
      {
      sscanf(line,"%d",&art);
      bool load;
      if (extended)
        {
        int beg=line.Find('<');
        int end=line.Find('>');
        msgid=line.Mid(beg,end-beg+1);
        load=hash.Find(msgid)==NULL;
        if (load)
          load=list->FindMessage(msgid)==NULL;;
        }
      else
        load=art>=nlindex;
      if (load)
        {
        CNewsArtHolder *hld=new CNewsArtHolder(art);
        if (ap) 
          {ap->SetNext(hld);ap=hld;}
        else seznam=ap=hld;
        msgcntr++;
        }
      if (xhdr) 
        line=term.ReadLine();        
      else
        {
        term.SendFormatted("NEXT");
        err=term.ReceiveNumReply()/10;	  
        }
      }
    DogSetMax(msgcntr);msgcntr=0;
    if (!xhdr) for (i=0;i<acccount;i++) term.ReceiveNumReply();
    CNewsArtHolder *q=seznam;
    ap=seznam;
    for (i=-1;i<ACCELERATE && q!=NULL;i++)
      {
      term.SendFormatted("HEAD %d",q->GetIndex());
      q=q->GetNext();
      }
    while (ap!=NULL)
      {	  
      err=term.ReceiveNumReply();
      if (err<0)
        {
        delete seznam;
        return err;
        }
      if (err/10==22) 
        {
        CNewsArticle *artp=new CNewsArticle();
        art=-1;
        err=artp->DownloadArticle(art,term,false);
        ap->RescanHeader(artp);
        if (artp) artp->Release();
        if (name && ap->from.Find(name)!=-1) ap->MarkAsRead(5);
        msgcntr++;
        DogSetPos(msgcntr);
        if (automark) 
          ap->MarkAsRead(12);
        }
      ap=ap->GetNext();;
      if (q)
        {
        term.SendFormatted("HEAD %d",q->GetIndex());
        q=q->GetNext();
        }
      }
    }
  
  if (extended && seznam && list)
    {
    ::DogShowStatusV(IDS_REBUILDINGSTRUCT,GetName());
    ap=list;
    int cnt=0;
    while (ap!=NULL) 
      {cnt++;ap=ap->EnumNext();}
    ap=seznam;
    while (ap!=NULL) 
      {cnt++;ap=ap->GetNext();}
    CNewsArtHolder **hldl=new CNewsArtHolder *[cnt];
    cnt=0;
    ap=list;while (ap!=NULL) 
      {hldl[cnt++]=ap;ap=ap->EnumNext();}
    ap=seznam;while (ap!=NULL) 
      {hldl[cnt++]=ap;ap=ap->GetNext();}
    qsort(hldl,cnt,sizeof(hldl[0]),hldl_compare);
    seznam=ap=hldl[0];
    ap->Unlink();	
    ap->UnlinkChild();
    for (int i=1;i<cnt;i++) 
      {
      ap->SetNext(hldl[i]);
      ap=ap->GetNext();
      ap->Unlink();
      ap->UnlinkChild();
      }
    list=NULL;
    delete hldl;
    }
  if (seznam && notify) *urgent |= true;
  while (seznam)
    {
    bool edited=false;
    CNewsArtHolder *ptr=seznam;
    seznam=seznam->GetNext();
    ptr->Unlink();
    if (list==NULL)
      {
      list=ptr;	  
      }
    else 
      {
      CNewsArtHolder *trash;
      if (ptr->Xcancel.GetLength()!=0 && (trash=list->FindMessage(ptr->Xcancel))!=NULL)
        trash->SetTrash(true);
      CNewsArtHolder *parent=NULL;
      if (ptr->refid.GetLength())
        {
        parent=list->FindMessage(ptr->refid);	  //Najdi rodice prispevku        
        if (parent==NULL) 
          parent=list->FindMessage(ptr->refid,true);  //hledej ve vymazanych
        }
      if (parent!=NULL)                   //rodic byl nalezen
        {
        ptr->SetParent(parent);             //nastav rodice prispevku skutecneho rodice                
        if (parent->GetChild()==NULL)   //pokud rodic nema zadne potomky
        parent->SetChild(ptr);      //pak nastav tento prispevek jako prvniho potomka
        else                            // ^^^ Prispevek byl pridan
          {
          CNewsArtHolder *chld=parent->GetChild();  //zacni prvnim potomkem
          while (chld->GetNext()!=NULL) chld=chld->GetNext(); //najdi posledniho potomka
          chld->SetNext(ptr);        //PRIDEJ PRISPEVEK NA KONEC SEZNAMU
          }
        ptr->state=parent->state;      //prevezmi stav rodice
        ptr->SetHighlighted(parent->GetHighlighted()); 
        if ((ptr->state==EAImportant || ptr->GetHighlighted()) && urgent) 
          {
          *urgent |= 0x1;
          }
        parent->StateChange();
        }
      else 
        {		
        //POZOR: Zde ptr->parent == NULL
        parent=list;                  
        if (theApp.config.newontop)   //tady se vkladaji prispevky na zacatek
          {
          ptr->sortpos=--spos;
          ptr->SetNext(list);
          list=ptr;
          }
        else                          //tady se vkladaji prispevky na konec
          {
          ptr->sortpos=++spos;
          while (parent->GetNext()!=NULL) parent=parent->GetNext();
          parent->SetNext(ptr);
          }
        }
      ptr->ParentPropagate();
      parent=ptr->GetParent();
      while (parent) 
        {
        parent->ParentPropagate();
        parent=parent->GetParent();
        }
      
      
      }
    hash.Add(ptr);
    }
  minindex=min;
  maxindex=max;
  nlindex=max+1;
  SortItemsLater();
  return 0;  
  }

//--------------------------------------------------

static HTREEITEM FindInChilds(CTreeCtrl &tree, HTREEITEM root, const char *tx)
  {
  HTREEITEM pp;
  pp=tree.GetNextItem(root,TVGN_CHILD);
  CString s;
  while (pp)
    {
    s=tree.GetItemText(pp);
    int img;
    tree.GetItemImage(pp,img,img);
    if (s==tx && img==ICON_NWFOLDER) return pp;
    pp=tree.GetNextItem(pp,TVGN_NEXT);
    }
  return pp;
  }

//--------------------------------------------------

static HTREEITEM CreatePath(CTreeCtrl &tree, HTREEITEM root,const char *path, LPARAM lParam, int news)
  {
  char *pp=(char *)alloca(strlen(path)+20);
  strcpy(pp,path);
  char *c;
  HTREEITEM p=root,r;
  c=strchr(pp,'.');
  while (c!=NULL)
    {
    *c=0;
    r=FindInChilds(tree,p,pp);
    if (r==NULL)
      {
      r=tree.InsertItem(pp,ICON_NWFOLDER,ICON_NWFOLDER,p,TVI_SORT);	  
      }
    pp=c+1;
    p=r;
    c=strchr(pp,'.');
    }
  char num[10];  
  if (news)
    {
    sprintf(num," (%d)",news);
    strcat(pp,num);
    }
  p=tree.InsertItem(pp,ICON_NWGROUP,ICON_NWGROUP,p,TVI_SORT);
  tree.SetItemData(p,lParam);  
  if (news)
    tree.SetItemState(p,TVIS_BOLD,TVIS_BOLD);
  return p;
  }

//--------------------------------------------------

void CNewsGroup::LoadToTree(CTreeCtrl &tree,HTREEITEM root, bool astree)
  {
  CNewsGroup *lst=this;
  while (lst)
    {
    if (lst->sign || lst->display) 
      {
      if (lst->iteminlist==NULL)
        {
        if (astree)
          lst->iteminlist=CreatePath(tree,root,lst->GetName(),(LPARAM)lst,newcount);
        else
          {
          HTREEITEM p=tree.InsertItem(lst->GetName(),ICON_NWGROUP,ICON_NWGROUP,root,TVI_SORT);
          tree.SetItemData(p,(LPARAM)lst);  
          lst->iteminlist=p;
          }
        }
      theApp.m_pMainWnd->SendMessage(NRM_GRPREADDONE,0,(LPARAM)lst->iteminlist);
      }
    else
      {
      if (lst->iteminlist) tree.DeleteItem(lst->iteminlist);
      lst->iteminlist=NULL;
      }
    lst=lst->next;
    }
  
  }

//--------------------------------------------------

int CNewsGroup::GetGroupCount()
  {
  int p=0;
  CNewsGroup *lst=this;
  while (lst) 
    {p++;lst=lst->next;}
  return p;
  }

//--------------------------------------------------

void CNewsGroup::CatchUp()
  {
  if (list)
	{
	CRWLockGuard guard(articleLock,false);guard.LockPM();
  
	list->CatchUp();
  
	CountNewsAndHighlights();
	}
  }

//--------------------------------------------------

void CNewsGroup::FindInGroup(GRPFINDINFO &fnd, CNewsTerminal &term)
  {
  CRWLockGuard guard(articleLock,false);guard.LockPM();
  CNewsArtHolder *list=GetList();
  if (term.IsConnected())
    if (SetActiveGroup(term)==false) return;
  fnd.grp=this;
  list->Search(fnd,term);
  }


//--------------------------------------------------

//DEL void CNewsGroup::WantSortItems(int by, bool forceSort)
//DEL {
//DEL 	if (forceSort)
//DEL 	{
//DEL 		// assume no sorting was done yet
//DEL 		// this is used when sorting criteria is changed globally
//DEL 		sortitemDone = -1;	
//DEL 	}
//DEL 	if (by>=0)
//DEL 	{
//DEL 		sortitem = by;
//DEL 	}
//DEL }

void CNewsGroup::SortItems(int by)
  {
  CRWLockGuard guard(articleLock,true);
  if (guard.LockPM(1000)==FALSE) return;
  if (by!=-1) _sortitem=by;
  if (this && list)
    {
    list = list->Sort(_sortitem);
    }
  _needresort=false;
  }

//--------------------------------------------------

bool CNewsGroup::SetActiveGroup(CNewsTerminal &term)
  {
  term.SendFormatted("GROUP %s",GetName());
  CString line;
  int res=term.ReceiveNumReply();
  if (res<0 || res/10!=21) 
    {term.ShowError(res);return false;}
  return true;
  }

//--------------------------------------------------

void CNewsAccount::Compact(bool close, CNewsGroup *grp)
  {
  if (grp) grp->Compact("",-1,close);
  else	  
    for (grp=grlist;grp;grp=grp->GetNext())
      if (grp->sign) grp->Compact(0,-1,close);
  ApplyCond(false);
  }

//--------------------------------------------------

bool CNewsAccount::CheckDel()
  {
  if (delcheck==0) return theApp.config.delcheck!=FALSE;
  else
    if (delcheck==1) return true;
  else return false;
  }

//--------------------------------------------------

void CNewsAccount::InitCache()
  {    
  CString ss;
  GetCachePath(ss);
  CreateDirectory(ss,NULL);
  if (!fulltext.IsCreated())
    if (fulltext.Create(this->GetFName(),ss)==false) 
      if (AfxMessageBox(IDS_FULLTEXTRECREATEASK,MB_YESNO)==IDYES)
        if (fulltext.Create(this->GetFName(),ss,true)==false)
         AfxMessageBox(IDS_FULLTEXTRECREATEFAILED,MB_YESNO);
  GetCacheFileName(ss);
  cache.InitCache(ss,offline);	  
  }

//--------------------------------------------------

#include "NewAccWiz.h"

bool CNewsAccount::CreateByWizard(const char *name, const char *email, BOOL &sock, BOOL &smtp)
  {
  CNewAccWiz1 w1;
  w1.m_email=email;
  w1.m_name=name;
  CNewAccWiz2 w2;
  w2.m_login=FALSE;
  w2.m_port=119;
  CNewAccWiz3 w3;
  CNewAccWiz4 w4;
  w4.m_smtp=smtp;
  w4.m_sock4=sock;
  w4.m_tune=FALSE;
  CNewAccWiz5 w5;  
  if (RunNewAccWizard(w1,w2,w3,w4,w5)==false) return false;
  address=w2.m_name;
  myemail=w1.m_email;
  myname=w1.m_name;
  this->name=w5.m_name;
  needauthorize=w2.m_login!=FALSE;
  password=w3.m_password;
  port=w2.m_port;
  username=w3.m_name;
  sock=w4.m_sock4;
  smtp=w4.m_smtp;
  if (w4.m_tune) EditInDialog();
  return true;
  }

//--------------------------------------------------

bool CNewsAccount::GetKeyword(int index, CString &text)
  {
    if (index<_threadRules.GetRuleIdMin() || index>_threadRules.GetRuleIdMax()) return false;
    text=_threadRules.GetRule(index).GetCond();
    return true;
  }
/*
  const char *kwd=keywords;
  char *l,*r,*e=strchr(kwd,0);
  l=(char *)kwd;
  r=strchr(kwd,'\n'); if (r==NULL) r=e;
  while (index--)
    {	
    if (r==e) return false;	
    l=r+1;
    r=strchr(l,'\n'); if (r==NULL) r=e;
    }
  int s=r-l;
  e=(char *)alloca((s+1)*sizeof(char));
  strncpy(e,l,s);
  e[s]=0;
  text=e;
  return true;
  }*/

//--------------------------------------------------

bool CNewsAccount::SetKeyword(int index, CString &text)
  {
/*    _threadRules.
  ostrstream out;
  istrstream in((char *)((LPCTSTR)keywords));
  while (index)
    {
    int p=in.get();
    if (p==EOF) break;
    out.put((char)p);
    if (p=='\n') index--;
    }
  if (text!="")  out<<text<<'\n';
  in.ignore(0xFFFF,'\n');
  int p=in.get();
  while (p!=EOF) 
    {out.put((char)p);p=in.get();}
  out.put((char)0);
  keywords=out.str();
  out.rdbuf()->freeze(0);*/
  return false;
  }

//--------------------------------------------------

void CNewsAccount::EnDeCodeCond(CString &kwd,  CString &name, CString &cond, bool encode)
  {
  if (encode)
    {
    kwd=name+"~"+cond;
    }
  else
    {
    int len=kwd.GetLength();
    int z=kwd.Find('~');if (z==-1) z=len;
    name=kwd.Mid(0,z);
    if (z<len)
      {
      int zz=len;
      z++;
      cond=kwd.Mid(z,zz-z);
      }
    else
      {
      cond="";
      }
    }
  }

//--------------------------------------------------

void CNewsGroup::ApplyHighlight(CCondCalc &cond,bool all)
  {
  CRWLockGuard guard(articleLock,false);guard.LockPM();
  CNewsArtHolder *ptr=list;
  while (ptr!=NULL)
    {	
    if (all || ptr->Changed(false))
    {
      if (ptr->IsArchive() && ptr->GetChild()) 
        ptr=ptr->GetChild();
      bool mark = false;
      bool localProp = false;
      if (cond.IsCompiled())
        {
        CString line=ptr->GetThreadProperties(true,&localProp);
        mark=(cond.Evalute(line)!=0);
        }
      // first apply thread-based highlight
      ptr->HighlightThread(mark);
      // if any local properties were detected, apply per-post settings
      if (localProp && cond.IsCompiled())
        {
        CNewsArtHolder *end = ptr->EnumNextNoChild();
        for (CNewsArtHolder *walk = ptr; walk && walk!=end; walk=walk->EnumNext())
          {
          // do not check local properties on the thread itself - 
          // such setting is wrong, as local properties are applied on the parent
          if (walk==ptr || !walk->GetParent()) continue;
          // check if there are any "local" properties
          // if yes, they should be applied directly to the parent
          CString prop = walk->GetLocalProperties(true);
          if (!prop.IsEmpty())
            {
            // some local properties found - apply them to the parent
            bool mark = (cond.Evalute(prop)!=0);
            walk->GetParent()->HighlightSubthread(mark);
            }
          }
        }
    }    
    ptr=ptr->EnumNextNoChild();
    }
  }

//--------------------------------------------------

void CNewsGroup::ApplyCond(CCondCalc &cond,bool all)
  {
  CRWLockGuard guard(articleLock,false);guard.LockPM();
  CNewsArtHolder *ptr=list;
  while (ptr!=NULL)
    {	
    if (all || ptr->Changed(false))
    {
      bool hide = false;
      bool localProp = false;
      if (cond.IsCompiled())
        {
        CString line=ptr->GetThreadProperties(true,&localProp);
        hide=(cond.Evalute(line)==0);
        }
      ptr->HideThread(hide);
      #if 0
      if (localProp && cond.IsCompiled())
        {
        CNewsArtHolder *end = ptr->EnumNextNoChild();
        for (CNewsArtHolder *walk = ptr; walk && walk!=end; walk=walk->EnumNext())
          {
          // do not check local properties on the thread itself - 
          // such setting is wrong, as local properties are applied on the parent
          if (walk==ptr || !walk->GetParent()) continue;
          // check if there are any "local" properties
          // if yes, they should be applied directly to the parent
          CNewsArtHolder *parent = walk->GetParent();
          CString prop = parent->GetLocalProperties(true);
          if (!prop.IsEmpty())
            {
            // some local properties found - apply them to the parent
            bool hide = (cond.Evalute(prop)==0);
            parent->HideSubthread(hide);
            }
          }
        }
      #endif
    }    
    ptr=ptr->EnumNextNoChild();
    }
  }

//--------------------------------------------------

static CMutex showerrorthr;

static UINT ShowCompileThread(LPVOID param)
  {
  showerrorthr.Lock();
  char *text=(char *)param;
  NrMessageBox(text,MB_OK);
  free(text);
  showerrorthr.Unlock();
  return 0;
  }

//--------------------------------------------------

bool CNewsAccount::ApplyHighlight(bool refresh, CNewsGroup *grp)
  {
  bool all=refresh;  
  CString kwd;
  if (_threadRules.GetHighlight()>=0) 
    GetKeyword(_threadRules.GetHighlight(),kwd);
  else
    kwd="";
  CCondCalc eval;
  bool ok=eval.Compile(kwd);
  if (!ok)
    {
    if (showerrorthr.Lock(0)==TRUE)
      {
      ostrstream str;
      eval.Print(str);
      str<<'\0';
      CString z;
      AfxFormatString1(z,IDS_COMPILEERROR,str.str());
      str.rdbuf()->freeze(0);
      char *sdup=strdup(z);
      AfxBeginThread((AFX_THREADPROC)ShowCompileThread,(void *)sdup);
      showerrorthr.Unlock();
      }
    }
  else
    {
    if (grp)
      {
      grp->ApplyHighlight(eval,all);
      }
    else
      {
      CNewsGroup *z=grlist;
      while (z!=NULL) 
        {
        if (z->GetList())
          {
          z->ApplyHighlight(eval,all);
          }
        z=z->GetNext();	 
        }
      }
    }
  return ok;
  }

//--------------------------------------------------

bool CNewsAccount::ApplyCond(bool refresh, CNewsGroup *grp)
  {
  bool all=refresh;
  CString kwd;
  if (_threadRules.GetFilter()>=0) 
    GetKeyword(_threadRules.GetFilter(),kwd);
  else
    kwd="";
  CCondCalc eval;
  bool ok=eval.Compile(kwd);
  if (!ok)
    {
    if (showerrorthr.Lock(0)==TRUE)
      {
      ostrstream str;
      eval.Print(str);
      str<<'\0';
      CString z;
      AfxFormatString1(z,IDS_COMPILEERROR,str.str());
      str.rdbuf()->freeze(0);
      char *sdup=strdup(z);
      AfxBeginThread((AFX_THREADPROC)ShowCompileThread,(void *)sdup);
      showerrorthr.Unlock();
      }
    }
  else
    {
    if (grp)
      {
      grp->ApplyCond(eval,all);	  
      }
    else
      {
      CNewsGroup *z=grlist;
      while (z!=NULL) 
        {
        if (z->GetList())
          {
          z->ApplyCond(eval,all);
          }
        z=z->GetNext();	 
        }
      }
    }
  return ok;
  }

void CNewsAccount::PostApplyConds(CNewsGroup *grp)
{
    if (grp)
      {      
      grp->CountNewsAndHighlights();
      grp->ClearChanged();
      }
    else
      {
      CNewsGroup *z=grlist;
      while (z!=NULL) 
        {
        if (z->GetList())
          {
          z->CountNewsAndHighlights();
          z->ClearChanged();
          }
        z=z->GetNext();	 
        }
      }
}

//--------------------------------------------------

void CNewsAccount::Repair()
  {
  CNewsTerminal::StopTrafic();
  HANDLE mx=mutex;
  while (MsgWaitForMultipleObjects(1,&mx,FALSE,INFINITE,QS_SENDMESSAGE)!=WAIT_OBJECT_0)
    {
    MSG msg;
    while (::PeekMessage(&msg,0,0,0,PM_REMOVE | (QS_SENDMESSAGE<<16))) DispatchMessage(&msg);
    }
  CNewsGroup *grp=grlist;
  while (grp!=NULL)
    {
    grp->Repair();
    grp=grp->GetNext();
    Compact(true,grp);
    }
  mutex.Unlock();
  }

//--------------------------------------------------

static CNewsArtHolder *ZaradAlone(CNewsArtHolder *list)
  {
  CNewsArtHolder *trace=list;
  CNewsArtHolder *prev=NULL;
  while (trace)
    {
    if (trace->refid.GetLength()) //odkaz na neexistujici prispevek
      {
      CNewsArtHolder *fnd=list->FindMessage(trace->refid);
      if (fnd==NULL) 
        fnd=list->FindMessage(trace->refid,true);
      if (fnd)
        {
        CNewsArtHolder *next=trace->GetNext();
        if (prev) 
          {
          prev->Unlink();
          prev->SetNext(trace->GetNext());
          }
        else
          list=trace->GetNext();
        trace->Unlink();
        trace->SetNext(fnd->GetChild());
        fnd->UnlinkChild();
        fnd->SetChild(trace);
        trace=next;
        }
      else
        {
        prev=trace;
        trace=trace->GetNext();
        }
      }
    else
      {
      prev=trace;
      trace=trace->GetNext();
      }
    }
  return list;
  }

static void ZnovuZarad(CNewsArtHolder *sublist, CNewsArtHolder **nwlist)
  {
  CNewsArtHolder *next,*child,*fnd;
  while (sublist)
    {
    next=sublist->GetNext();
    child=sublist->GetChild();
    sublist->Unlink();
    sublist->UnlinkChild();
    fnd=NULL;
    if (sublist->refid.GetLength())
      {
      fnd=(*nwlist)->FindMessage(sublist->refid);
      if (fnd==NULL) 
        fnd=(*nwlist)->FindMessage(sublist->refid,true);
      }
    if (fnd==NULL)
      {
      sublist->SetNext(*nwlist);
      *nwlist=sublist;
      }
    else 
      {
      sublist->SetNext(fnd->GetChild());
      fnd->UnlinkChild();
      fnd->SetChild(sublist);
      }
    if (child) ZnovuZarad(child,nwlist);
    sublist=next;
    }
  }

//--------------------------------------------------

void CNewsGroup::Repair()
  {
  if (name=="") 
    {deleted=true;sign=false;}
  if (sign==false) 
    {if (list) list->Release();list=NULL;return;}
  CRWLockGuard guard(articleLock,true);guard.LockPM();
  list=ZaradAlone(list);
  CNewsArtHolder *nwlist=NULL;
  ZnovuZarad(list,&nwlist);
  list=nwlist;
  SortItems(_sortitem);
  }

//--------------------------------------------------

void CNewsAccount::BuildHash()
  {
  CRWLockGuard guard(articleLock,false);guard.LockPM();
  msgidlist.Reset();
  CNewsGroup *grp=grlist;
  while (grp!=NULL) 
    {
    if (grp->sign || grp->display)
      {
      CNewsArtHolder *hld=grp->GetList();
      while (hld)
        {
        if (msgidlist.Add(hld)==false) 
          {
          ASSERT(FALSE);
          return;
          }
        hld=hld->EnumNext();
        }
      }
    grp=grp->GetNext();
    }
  }

//--------------------------------------------------

/*!
\return date and time of the last message posted by "me" (current user)
*/

COleDateTime CNewsAccount::GetMyLastMessageTime()
  {
  CRWLockGuard guard(articleLock,false);guard.LockPM();
  bool datetimeValid = false;
  COleDateTime date;
  date.SetStatus(COleDateTime::invalid);
  for (CNewsGroup *grp=grlist; grp; grp=grp->GetNext())
    {
    for (CNewsArtHolder *hld=grp->GetList(); hld; hld=hld->EnumNext())
      {
      if (hld->from.Find(GetMyEmail())<0) continue;
      if (!datetimeValid || hld->date>date)
        {
        datetimeValid = true;
        date = hld->date;
        }
      }
    //grp->CountNewsAndHighlights();
    }
  return date;
  }

//--------------------------------------------------

void CNewsAccount::ChangeLastReadSmartPerGroup()
  {
  CRWLockGuard guard(articleLock,false);guard.LockPM();
  for (CNewsGroup *grp=grlist; grp; grp=grp->GetNext())
    {
    bool datetimeValid = false;
    COleDateTime date;
    // determine the treshold
    for (CNewsArtHolder *hld=grp->GetList(); hld; hld=hld->EnumNext())
      {
      if (hld->from.Find(GetMyEmail())<0) continue;
      if (!datetimeValid || hld->date>date)
        {
        datetimeValid = true;
        date = hld->date;
        }
      }
    
    if (datetimeValid)
      {
      // mark as read
      for (CNewsArtHolder *hld=grp->GetList(); hld; hld=hld->EnumNext())
        {
        if (hld->GetUnread() && date>=hld->date)
          {
          hld->MarkAsRead(6);
          }
        }
      grp->CountNewsAndHighlights();
      theApp.m_pMainWnd->PostMessage(NRM_GRPREADDONE,0,(LPARAM)grp->iteminlist);
      }
    }
  }

//--------------------------------------------------

void CNewsAccount::ChangeLastRead(COleDateTime date)
  {
  CRWLockGuard guard(articleLock,false);guard.LockPM();
  if (date.GetStatus()==COleDateTime::valid)
    {
    for (CNewsGroup *grp=grlist; grp; grp=grp->GetNext())
      {
      for (CNewsArtHolder *hld=grp->GetList(); hld; hld=hld->EnumNext())
        {
        if (hld->GetUnread() && date>=hld->date)
          {
          hld->MarkAsRead(7);
          }
        }
      grp->CountNewsAndHighlights();
      theApp.m_pMainWnd->PostMessage(NRM_GRPREADDONE,0,(LPARAM)grp->iteminlist);
      }
    }
  lastread=date;
  }

//--------------------------------------------------

void CNewsAccount::Unload()
  {
  DeleteGroups();
  msgidlist.Reset();
  loaded=false;
  }

//--------------------------------------------------

#include <mmsystem.h>

void CNewsAccount::PlayUrgent()
  {
  if (urgentntf!="")
    {
    PlaySound(urgentntf,NULL,SND_ASYNC);
    }
  }

//--------------------------------------------------

void CNewsAccount::GetCachePath(CString &path)
  {
  if (cachepath=="")
    {
    path.LoadString(IDS_CACHE);
    path=theApp.FullPath(path);
    }
  else 
    {
    path=cachepath;
    }
  }

//--------------------------------------------------

void CNewsAccount::GetCacheFileName(CString &fn)
  {
  GetCachePath(fn);
  fn=fn+"\\"+this->fname+".dat";
  }

//--------------------------------------------------

void CNewsAccount::DeleteCache()
  {
  cache.CloseCache();
  fulltext.Close();
  CString cachepath;
  GetCachePath(cachepath);
  CFileFind fnd;  
  BOOL nxt=fnd.FindFile(cachepath+"\\"+fname+".*");
    while (nxt)
    {
      nxt=fnd.FindNextFile();
      DeleteFile(fnd.GetFilePath());
    }
  RemoveDirectory(cachepath);
  }

//--------------------------------------------------

bool CNewsAccount::MoveCache(CString newlocation)
  {
  bool res;
  cache.CloseCache();
  fulltext.Close();
  CString oldpos;
  Pathname from;
  Pathname to;
  GetCachePath(oldpos);
  from.SetDirectory(oldpos);
  from.SetFiletitle(GetFName());
  from.SetExtension(".*");
  to.SetDirectory(newlocation);
  CFileFind fnd;
  BOOL found=fnd.FindFile(from);
  res=true;
  while (found)    
  {
    found=fnd.FindNextFile();
    if (!fnd.IsDirectory() && !fnd.IsDots())
    {
      to.SetFilename(fnd.GetFileName());
      from.SetFilename(fnd.GetFileName());
      if (MoveFile(from,to)==false) 
      {
         GetCachePath(newlocation);
         res=false;
         break;
      }
    }
  }
  RemoveDirectory(oldpos);
  cachepath=newlocation;
  InitCache();
  return res;
  }

//--------------------------------------------------

struct LoadHeaderInfo
  {
  CString group;  
  };

//--------------------------------------------------


//--------------------------------------------------

bool CNewsAccount::LoadArticle(const CString &msgid, CNewsArtHolder **article, CNewsGroup **grp,CString *message)
  {

  CRWLockGuard guard(articleLock,false);guard.LockPM();
  CNewsGroup *gp=NULL;
  bool out;
  CNewsTerminal term;
  if (OpenConnection(term)) return false;
  out=LoadArticleRecursive(term,msgid,article,&gp);
  if (out) gp->GetList()->CalcHierarchy2(gp);
  if (grp) *grp=gp;
  if (message) *message=term.GetLastMessage();
  return out;
  }

//--------------------------------------------------

bool CNewsAccount::LoadArticleRecursive(CNewsTerminal &term,const CString &msgid, CNewsArtHolder **article, CNewsGroup **group)
  {  
    class HeaderEnum: public INewsArticleEnumCallback
    {
      CString &group;
      virtual void HdrResult(const char *var, const char *value)
      {
        if (stricmp(var,ARTGROUPS)==0)
        {
          group=value;
          int p=group.Find(' ');
          if (p!=-1) group=group.Left(p);
        }
      }
    public:
      HeaderEnum(CString &saveTo):group(saveTo) {}
    };

  CString ngroup;
  CNewsGroup *grp;
  CNewsArtHolder *hld;
    {
    CNewsArticle art;
    term.SendFormatted("HEAD %s",msgid);
    if (term.ReceiveNumReply()/10!=22) return false;
	int sidx;
    sscanf(term.GetLastMessage()+4,"%d",&sidx);
    int idx=-1;
    art.DownloadArticle(idx,term,true);		
    HeaderEnum hdrenum(ngroup);
    art.EnumHeader(&hdrenum);
    grp=GetGroups()->FindGroup(ngroup);
    if (grp==NULL)
      {
      grp=new CNewsGroup(ngroup,0,0);
      grp->SetNext(grlist);
      grlist=grp;
      }
    grp->display=true;
    hld=new CNewsArtHolder(sidx);
    hld->LoadArticle(&art,false);
    }
  if (hld->refid.GetLength()!=0)
    {
    CNewsArtHolder *parent=grp->GetList()->FindMessage(hld->refid);
    if (parent)
      {
      hld->SetNext(parent->GetChild());
      parent->UnlinkChild();
      parent->SetChild(hld);
      }
    else if (LoadArticleRecursive(term,hld->refid,&parent,&grp)==true)
      {
      hld->SetNext(parent->GetChild());
      parent->UnlinkChild();
      parent->SetChild(hld);
      }
    else 
      {
      hld->SetNext(grp->GetList());
      grp->SetList(hld);
      }
    }
  else
    {
    hld->SetNext(grp->GetList());
    grp->SetList(hld);
    }
  this->msgidlist.Add(hld);
  if (group) *group=grp;
  if (article) *article=hld;
  return true;
  }

//--------------------------------------------------

void CNewsAccount::Streaming(IArchive &arch)
  {
    CString keywords;
    int curFilter,curHighlight;
  if (arch.Check("NEWSREADER ACCOUNT")==false) {arch.SetError(-111);return;}    
    if (+arch && saveinpreviousver) arch.RequestVer(ACC_VERSION2-1);
  else
    if (arch.RequestVer(ACC_VERSION2)==false) {arch.SetError(-112);return;}  
  ArchiveSection archsect(arch);
  while (archsect.Block("Account"))
    {
    arch("name",name);
    arch("address",address);
    arch("port",port);
    arch("username",username);
    arch("password",password);
    arch("myname",myname);
    arch("myemail",myemail);
    arch("lastcheck",last);
    if (arch.IsVer(0x201))
	  arch("timeout",timeout);
    else
	  {
	  CTimeSpan timeout;
	  arch("timeout",timeout);
	  }
	if (arch.IsVer(0x202))
	{
	  arch("fulltextopt",fullTextOpt);
	  arch("fullTextIgnoredWords",fullTextIgnoredWords);
	  arch("fullTextflags",fullTextInclude);
	}	
    arch("needauthorize",needauthorize);	    
    arch("urlpic",urlpic);
    arch("cachesize",offline);
    if (arch.IsVer(0x206))
    {
      keywords="";
    }
    else
    {
      arch("rules",keywords);
    }
    arch("viewastree",viewastree);
    arch("compactcache",compacto);
    arch("autocompact",autocompact);
    arch("loadstartup",loadstartup);
    arch("urgentntf",urgentntf);  
    arch("newondemand",newondemand);
    arch("nonewnotify",nonewnotify);
    arch("cachepath",cachepath);
    arch("daysarchive",daysarchive);
    arch("daysdelete",daysdelete);
    if (arch.IsVer(0x206))
    {
      curFilter=curHighlight=-1;
    }
    else
    {    
      arch("curFilter",curFilter);
      arch("curHighlight",curHighlight);
    }
    arch("delcheck",delcheck);  
    if (arch.IsVer(0x206))
    {
      _threadRules.Streaming(arch);
    }
    else
    {
      _threadRules.LoadFromKwd(keywords,curFilter,curHighlight);
    }
    if (!arch) return;
    }
  CRWLockGuard guard(articleLock,arch.IsReading());guard.LockPM();
  if (arch.IsStoring() || (startuptime && loadstartup) || !startuptime )
    {
    startuptime=false;
    if (arch.IsReading()) {delete grlist;grlist=NULL;}
    CNewsGroup *grp,*pos;
    int cnt=0;
    for (grp=grlist;grp;grp=grp->GetNext()) if (!grp->deleted) cnt++;
    arch("CountGroups",cnt);
	if (!arch) return;	
	pos=grlist;
	for (int i=0;i<cnt;i++)
	  {
      arch.vSectionData=i;
	  if (arch.IsReading()) 
		{
		grp=new CNewsGroup();
		grp->deleted=false;
		if (grlist==NULL) grlist=pos=grp;
		else pos->SetNext(grp);		
		pos=grp;
		grp->Streaming(arch);
		}	  
	  else
		{
		pos->Streaming(arch);
		pos=pos->GetNext();
		}
	  if (!arch) return;
	  }
    if (arch.Check("END-OF-FILE")==false) {arch.SetError(-113);return;}
    loaded=true;
    dirty=false;
    if (arch.IsReading()) 
      OnPostLoad();
	issaved=true;
    }
  else
    {
    loaded=false;
    delete grlist;
    grlist=NULL;
    startuptime=false;
    }
  if (arch.IsReading()) InitCache();
  sharedTable.Clear();
  }

void CNewsGroup::Streaming(IArchive &arch)
  {
  ArchiveSection archsect(arch);
  while (archsect.Block("Group%04X"))
    {
    arch("name",name);
    arch("minindex",minindex);
    arch("maxindex",maxindex);
    arch("nlindex",nlindex);
    arch("notify",notify);
    arch("loadmode",fullload);
    arch("sortitem",_sortitem);
    if (arch.IsVer(0x204))
    {
      arch("automark",automark);
      arch("autodeletedays",autodeletedays);
      arch("autodeletefilter",autodeletefilter);
    }
    arch("sign",sign);
    if (sign)
      {    
      list=CNewsArtHolder::Streaming(list,arch,minindex);
      }
    }
  }


void CNewsAccount::CompletteThread(CNewsGroup *grp, CNewsArtHolder *beg, HWND notify)
  {
  CNewsTerminal term;
  if (term.ShowError(OpenConnection(term))) return;
  if (!grp->SetActiveGroup(term)) return;
  CompletteThread(term,beg,beg->GetIndex(),grp->GetMax(),notify);
  term.SendFormatted("QUIT");
  if (grp->special) beg->CalcHierarchy2(grp);
  }

void CNewsAccount::CompletteThread(CNewsTerminal &term, CNewsArtHolder *parent, int from, int to, HWND notify)
  {
  CNewsArtHolder *frst=NULL,*iter,*ptriter;  
  term.SendFormatted("XPAT references %d-%d %s",from,to,parent->messid);
  int repl=term.ReceiveNumReply();
  if (repl/10!=22) return;
  const char *line=term.ReadLine();
  while (line && strcmp(line,"."))
    {
    int index;
    sscanf(line,"%d",&index);
    CNewsArtHolder *newhld=new CNewsArtHolder(index);
    newhld->SetNext(frst);;
    frst=newhld;    
    line=term.ReadLine();
    }
  iter=frst;
  ptriter=NULL;
  while (iter)
    {    
    CNewsArticle art;
    int index=iter->GetIndex();
    if (term.ShowError(art.DownloadArticle(index,term,false))==0)              
      {
      iter->LoadArticle(&art,false);
      CNewsArtHolder *q=parent->FindMessage(iter->messid);
      if (q==NULL)
        {
        ptriter=iter;
        iter=iter->GetNext();
        if (notify) SendMessage(notify,WM_APP,0,(LPARAM)iter);
        continue;
        }
      else
        q->SetIndex(iter->GetIndex());
      }
    if (ptriter==NULL) frst=iter->GetNext();
    else 
      {
      ptriter->Unlink();
      ptriter->SetNext(iter->GetNext());
      }
    iter->Unlink();
    delete iter;      
    if (ptriter) iter=ptriter->GetNext();else iter=frst;
    }
  if (frst)
    {
    iter=parent->GetChild();
    if (iter)
      {
      while (iter->GetNext()!=NULL) iter=iter->GetNext();
      iter->SetNext(frst);
      }
    else
      parent->SetChild(frst);
    }
  iter=parent->GetChild();
  while (iter)
    {
    CompletteThread(term,iter,iter->GetIndex(),to,notify);
    iter=iter->GetNext();
    }
  }

void CNewsAccount::SortAll(int sort)
  {
  CNewsGroup *grp=grlist;
  while (grp)    
    {
    if (grp->sign && grp->GetList())grp->SortItems(sort);
    grp=grp->GetNext();
    }
  }

void CNewsAccount::SortAllIfNeeded()
  {
  CNewsGroup *grp=grlist;
  while (grp)    
    {
    if (grp->sign && grp->GetList())grp->SortIfNeeded();
    grp=grp->GetNext();
    }
  }

void CNewsAccount::DownloadAllArticles()
{
  CNewsGroup *grp=grlist;
  while (grp)    
    {
    if (grp->sign && grp->GetList())grp->_downloadall=true;
    grp=grp->GetNext();
    }

}

void CNewsAccount::DeleteAccount()
{
  mutex.Lock(INFINITE);
  while (this->refcount>1)
  {
    Sleep(100);
  }
  DeleteCache();
  remove(theApp.FullPath(GetFName()+CString(ACCNEWEXT)));
  remove(theApp.FullPath(GetFName()+CString(ACCEXT)));
  remove(theApp.FullPath(GetFName()+CString(ACCNEWEXT)+".bak"));  
  mutex.Unlock();
  Release();
}

void CNewsGroup::AutoDeleteThread(CCondCalc &cond, int days)
{
  CNewsArtHolder *article=GetList();
  COleDateTime deadline=COleDateTime::GetCurrentTime()-COleDateTimeSpan(days,0,0,0);
  while (article)
  {
    if (article->newestdate<deadline)
    {
      if (cond.IsCompiled())
      {
        bool localProp = false;
        CString line=article->GetThreadProperties(true,&localProp );
        bool deleteit=(cond.Evalute(line)==0);
        if (deleteit) article->DeleteThread();
      }
    }
    article=article->GetNext();
  }
}

void CNewsAccount::OnPostLoad()
{
  CNewsGroup *grp=grlist;
  COleDateTime dtma=COleDateTime::GetCurrentTime();
  COleDateTime dtmd=COleDateTime::GetCurrentTime();
  dtma-=COleDateTimeSpan(daysarchive,0,0,0);
  dtmd-=COleDateTimeSpan(daysdelete,0,0,0);
  while (grp)
  {
    if (grp->GetList())
    {
      grp->Compact(NULL,0,true);	  
      grp->CountNewsAndHighlights();
      //	  if (daysarchive) grp->Archivate(dtma);
      //	  if (daysdelete) grp->GetList()->DeleteOlder(dtmd);
      if (grp->autodeletedays)
      {
        CCondCalc eval;
        CString name=grp->autodeletefilter;
        if (name!="")
        {            
          int filter=_threadRules.FindByName(name);
          if (filter>=0)
          {
            if (eval.Compile(_threadRules.GetRule(filter).GetName()))
            {
              grp->AutoDeleteThread(eval,grp->autodeletedays);
            }
          }
        }
        else
          grp->AutoDeleteThread(eval,grp->autodeletedays);
      }
    }
    grp=grp->GetNext();
  }
  ApplyCond(false);
  ApplyHighlight(false);
  PostApplyConds();
}
