//{{AFX_INCLUDES()
#include "webbrowser2.h"
//}}AFX_INCLUDES
#if !defined(AFX_WEB_H__9CE3E70C_2540_4E26_B856_FAA2DEEAA005__INCLUDED_)
#define AFX_WEB_H__9CE3E70C_2540_4E26_B856_FAA2DEEAA005__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// Web.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// Web dialog

class Web : public CDialog
{
// Construction
public:
	CString url;
	Web(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(Web)
	enum { IDD = IDD_SMALLWEB };
	CWebBrowser2	m_web;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(Web)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(Web)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_WEB_H__9CE3E70C_2540_4E26_B856_FAA2DEEAA005__INCLUDED_)
