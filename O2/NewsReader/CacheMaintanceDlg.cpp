// CacheMaintanceDlg.cpp : implementation file
//

#include "stdafx.h"
#include "newsreader.h"
#include "CacheMaintanceDlg.h"
#include "CompactCacheDlg.h"
#include "NewsTerminal.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCacheMaintanceDlg dialog


CCacheMaintanceDlg::CCacheMaintanceDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CCacheMaintanceDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CCacheMaintanceDlg)
	mode = 0;
	m_forcesize = 0;
	//}}AFX_DATA_INIT
}


void CCacheMaintanceDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCacheMaintanceDlg)
	DDX_Control(pDX, IDC_PROGRESS1, progress);
	DDX_Control(pDX, IDC_FORCESIZE, m_wForceSize);
	DDX_Control(pDX, IDC_TARGETSIZE, m_targetsize);
	DDX_Control(pDX, IDC_MAXSIZE, m_maxsize);
	DDX_Control(pDX, IDC_CURSIZE, m_cursize);
	DDX_Radio(pDX, IDC_COMPACT1, mode);
	DDX_Text(pDX, IDC_FORCESIZE, m_forcesize);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CCacheMaintanceDlg, CDialog)
	//{{AFX_MSG_MAP(CCacheMaintanceDlg)
	ON_WM_TIMER()
	ON_BN_CLICKED(IDC_COMPACT1, OnCompactClick)
	ON_BN_CLICKED(IDC_COMPACT2, OnCompactClick)
	ON_BN_CLICKED(IDC_COMPACT3, OnCompactClick)
	ON_BN_CLICKED(IDC_COMPACT4, OnCompactClick)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCacheMaintanceDlg message handlers

BOOL CCacheMaintanceDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	UpdateCacheStatistcs();
	SetTimer(2000,2000,NULL);
	// TODO: Add extra initialization here
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CCacheMaintanceDlg::UpdateCacheStatistcs()
  {
  max=acc->cache.GetCacheMaxSize()/1024;
  cur=acc->cache.GetCacheSize()/1024;
  halfmax=max*acc->compacto/100;
  halfcur=cur*acc->compacto/100;
  char buff[50];
  m_targetsize.SetWindowText(itoa(halfmax,buff,10));
  if (IsDlgButtonChecked(IDC_COMPACT3)==0)
	{
	m_wForceSize.SetWindowText(itoa(halfcur,buff,10));
	m_wForceSize.EnableWindow(FALSE);
	}
  else
	m_wForceSize.EnableWindow(TRUE);
  GetDlgItem(IDC_COMPACT2)->EnableWindow(halfmax<cur);
  m_maxsize.SetWindowText(itoa(max,buff,10));
  m_cursize.SetWindowText(itoa(cur,buff,10));
  progress.SetRange32(0,max);
  progress.SetPos(cur);
  }

void CCacheMaintanceDlg::OnTimer(UINT nIDEvent) 
  {
  UpdateCacheStatistcs();		
  CDialog::OnTimer(nIDEvent);
  }

void CCacheMaintanceDlg::OnCompactClick() 
  {
  UpdateCacheStatistcs();	
  }

void CCacheMaintanceDlg::OnOK() 
  {
  if (UpdateData(TRUE)==FALSE) return;
  if (mode==0) m_forcesize=cur+1;
  if (mode==1) m_forcesize=halfmax;
  if (mode==3) m_forcesize=0;
  CCompactCacheDlg dlg;
  dlg.it=it;
  dlg.acclist=acclist;
  dlg.acc=acc;
  dlg.targetsize=m_forcesize*1024+1;
  CNewsTerminal::StopTrafic();
  if (dlg.DoModal()==IDOK) CDialog::OnOK();
  }
