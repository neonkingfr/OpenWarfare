#if !defined(AFX_DLGFULLTEXTWIZARD_H__587676CA_7F8A_498B_9F56_A99F200B57C9__INCLUDED_)
#define AFX_DLGFULLTEXTWIZARD_H__587676CA_7F8A_498B_9F56_A99F200B57C9__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DlgFullTextWizard.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// DlgFullTextWizard1 dialog

class DlgFullTextWizard1 : public CDialog
{
// Construction
public:
	DlgFullTextWizard1(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(DlgFullTextWizard1)
	enum { IDD = IDD_FULLTEXTWIZ1 };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(DlgFullTextWizard1)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(DlgFullTextWizard1)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DLGFULLTEXTWIZARD_H__587676CA_7F8A_498B_9F56_A99F200B57C9__INCLUDED_)
