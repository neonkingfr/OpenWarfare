// NRInstall.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "NRInstall.h"
#include <StreamParser.tli>

CStringRes GStrRes;


#define DEFAULT_INSTALL_PATH _T("c:\\bis\\newsreader")
#define SETUPDATA _T(".\\InstallData\\")
#define NREXE _T("NewsReader.exe")
#define NRMAP _T("NewsReader.map")
#define DISTRIBUTION _T(".\\dist\\")
#define PROFILESFILE _T("profiles")


struct InstallInfo
{
  Text installPath;
  Text firstName;
  Text lastName;
  Text nickName;
  Text serverLogin;
  Text password;
  Text sourceFile;
  Text email;
  bool userProfiles;
};

static Text SaveDlgValue(HWND hDlg, int uid)
{
  Text buff;
  int len;
  HWND hWnd=GetDlgItem(hDlg,uid);

  len=GetWindowTextLength(hWnd);
  GetWindowText(hWnd,buff.CreateBuffer(len+1),len+1);
  buff.UnlockBuffer();
  return buff;
}

static Text SaveDlgComboValue(HWND hDlg, int uid)
{
  Text buff;
  int len;
  HWND hWnd=GetDlgItem(hDlg,uid);

  int curSel=ComboBox_GetCurSel(hWnd);
  if (curSel==-1) return Text();
  len=ComboBox_GetLBTextLen(hWnd,curSel);
  ComboBox_GetLBText(hWnd,curSel,buff.CreateBuffer(len+1));
  buff.UnlockBuffer();
  return buff;
}

static InstallInfo SInstallInfo;

static int WINAPI PosBrowseCallbackProc(HWND hwnd,UINT uMsg,LPARAM lParam,LPARAM lpData)
{
  const _TCHAR *curpath=(const _TCHAR *)lpData;  
  if (uMsg == BFFM_INITIALIZED)
  {
    if (curpath && curpath[0])
    {
      ::SendMessage(hwnd,BFFM_SETSELECTION,TRUE,(LPARAM)((LPCSTR)curpath));
      ::SendMessage(hwnd,BFFM_SETSTATUSTEXT,0,(LPARAM)((LPCSTR)curpath));
    }
  }
  else if (uMsg == BFFM_SELCHANGED)
  {
    _TCHAR buff[_MAX_PATH];
    if (SHGetPathFromIDList((LPITEMIDLIST)lParam,buff))
    {        
      ::SendMessage(hwnd,BFFM_SETSTATUSTEXT,0,(LPARAM)buff);
    }
  }
  return 0;
};

static bool PathBrowser(HWND hWnd,_TCHAR *path /* MAX_PATH size */)
{
  BROWSEINFO brw;
  memset(&brw,0,sizeof(brw));
  brw.hwndOwner=hWnd;
  brw.pidlRoot=NULL;
  brw.pszDisplayName=path;
  brw.lParam=(LPARAM)path;
  brw.ulFlags= BIF_RETURNONLYFSDIRS |BIF_STATUSTEXT|BIF_USENEWUI ; 	
  brw.lpfn = (BFFCALLBACK)(PosBrowseCallbackProc);
  LPITEMIDLIST il=SHBrowseForFolder( &brw );
  if (il==NULL) return false;
  SHGetPathFromIDList(il,path);
  IMalloc *shmalloc;
  SHGetMalloc(&shmalloc);
  shmalloc->Free(il);
  if (path[0]==0) return false;
  return true;
}



static LRESULT WINAPI InstallDlgProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
  switch(message)
  {
  case WM_COMMAND: 
    switch (LOWORD(wParam))
    {
    case IDCANCEL: EndDialog(hDlg,IDCANCEL);return 1;
    case IDOK: if (MessageBox(hDlg,GStrRes[IDS_STARTINSTALLSTRING],_T(""),MB_OKCANCEL)==IDOK) 
      {      
        SInstallInfo.installPath=SaveDlgValue(hDlg,IDC_INSTALLPATH);
        SInstallInfo.firstName=SaveDlgValue(hDlg,IDC_FIRSTNAME);
        SInstallInfo.lastName=SaveDlgValue(hDlg,IDC_SECONDNAME);
        SInstallInfo.nickName=SaveDlgValue(hDlg,IDC_NICKNAME);
        SInstallInfo.serverLogin=SaveDlgValue(hDlg,IDC_SERVERLOGIN);
        SInstallInfo.password=SaveDlgValue(hDlg,IDC_SERVERPASSWORD);
        SInstallInfo.sourceFile=SaveDlgComboValue(hDlg,IDC_PROFILE);
        SInstallInfo.email=SaveDlgValue(hDlg,IDC_EMAIL);
        SInstallInfo.userProfiles=IsDlgButtonChecked(hDlg,IDC_USERPROF)==BST_CHECKED;
        EndDialog(hDlg,IDOK);
        return 1;  
      } 
      break;
    case IDC_BROWSEPATH:
      {
        _TCHAR buff[MAX_PATH]; 
        GetDlgItemText(hDlg,IDC_INSTALLPATH,buff,MAX_PATH);
        if (PathBrowser(hDlg,buff))
        {
          SetDlgItemTextA(hDlg,IDC_INSTALLPATH,buff);
        }
        return 1;
      }      
    };
    if (HIWORD(wParam)==EN_CHANGE || HIWORD(wParam)==CBN_SELCHANGE)
    {
      BOOL enable=GetWindowTextLength(GetDlgItem(hDlg,IDC_INSTALLPATH))!=0 &&
        GetWindowTextLength(GetDlgItem(hDlg,IDC_FIRSTNAME))!=0 &&
        GetWindowTextLength(GetDlgItem(hDlg,IDC_SECONDNAME))!=0 &&
        GetWindowTextLength(GetDlgItem(hDlg,IDC_NICKNAME))!=0 &&
        GetWindowTextLength(GetDlgItem(hDlg,IDC_SERVERLOGIN))!=0 &&
        GetWindowTextLength(GetDlgItem(hDlg,IDC_SERVERPASSWORD))!=0 &&
        GetWindowTextLength(GetDlgItem(hDlg,IDC_EMAIL))!=0 &&
        ComboBox_GetCurSel(GetDlgItem(hDlg,IDC_PROFILE))!=-1;
      EnableWindow(GetDlgItem(hDlg,IDOK),enable);
    }
    return 0;
  case WM_INITDIALOG:
    {
      SetDlgItemText(hDlg,IDC_INSTALLPATH,DEFAULT_INSTALL_PATH);
      WIN32_FIND_DATA fdata;
      HANDLE h=FindFirstFile(TextRef(SETUPDATA)+TSC("*.nr"),&fdata);
      HWND hCbx=GetDlgItem(hDlg,IDC_PROFILE);
      if (h!=INVALID_HANDLE_VALUE) do 
      {     
        ComboBox_AddString(hCbx,fdata.cFileName);
      }
      while(FindNextFile(h,&fdata));
      FindClose(h);
      CheckDlgButton(hDlg,IDC_USERPROF,BST_CHECKED);
      return 0;
    }
  }
  return 0;
}

struct SimpleFileStream
{
  istream *stream;

  bool ReadStream(char *buffer,size_t sz,size_t &rd)
  {
    stream->read(buffer,sz);
    rd=stream->gcount();
    return true;
  }
};

//_XXX_login_XXX_ _XXX_password_XXX_ _XXX_nick_XXX_ (_XXX_fname_XXX_ _XXX_lname_XXX_) _XXX_email_XXX_

static bool CreateAccountByTemplate(const Text &srcfile,const Text &trgfile, const InstallInfo &nfo)
{
  ifstream inacc(srcfile,ios::in|ios::binary);
  ofstream outacc(trgfile,ios::out|ios::binary|ios::trunc);
  SimpleFileStream stream;
  stream.stream=&inacc;
  StreamParser<char,SimpleFileStream> parser(&stream,false);
  bool rep=true;

  do {
    const char *selectedItem=0;
    
    char z;
    size_t rd;
    if (parser.Read(&z,1,rd,true)==true && rd==1)
    {
      if (z!='_') selectedItem=0;
      else if (parser.Test("^\\_XXX\\_login\\_XXX\\_")) selectedItem=MBText(nfo.serverLogin);
      else if (parser.Test("^\\_XXX\\_password\\_XXX\\_")) selectedItem=MBText(nfo.password);
      else if (parser.Test("^\\_XXX\\_nick\\_XXX\\_")) selectedItem=MBText(nfo.nickName);
      else if (parser.Test("^\\_XXX\\_fname\\_XXX\\_")) selectedItem=MBText(nfo.firstName);
      else if (parser.Test("^\\_XXX\\_lname\\_XXX\\_")) selectedItem=MBText(nfo.lastName);
      else if (parser.Test("^\\_XXX\\_email\\_XXX\\_")) selectedItem=MBText(nfo.email);
      if (selectedItem)
      {
        outacc.write(selectedItem,strlen(selectedItem));
        parser.Acknowledge();
      }
      else
      {
        parser.Pop(1);
        outacc.write(&z,1);
      }
    }
    else
      rep=false;
    
  } while(rep);
  return !(!outacc);
}



static bool TryRetry(const Text &what)
{
  Text msg;
  msg.Format(GStrRes[IDS_UNABLETOWRITEINTOFILE],what.GetString());
  return MessageBox(0,msg,0,MB_RETRYCANCEL|MB_ICONEXCLAMATION)==IDRETRY;
}

int APIENTRY _tWinMain(HINSTANCE hInstance,
                     HINSTANCE hPrevInstance,
                     LPTSTR    lpCmdLine,
                     int       nCmdShow)
{
  GStrRes.SetInstance(hInstance);

  if (DialogBox(hInstance,MAKEINTRESOURCE(IDD_INSTALLDLG),0,(DLGPROC)InstallDlgProc)==IDOK)
  {
    Pathname srcfile(SETUPDATA,Pathname());
    Pathname trgfile;
    _TCHAR accname[256];
    _stprintf(accname,_T("%08X.nr"),GetTickCount());
    srcfile.SetFilename(SInstallInfo.sourceFile);
    if (SInstallInfo.userProfiles)
    {
      trgfile.SetDirectorySpecial(CSIDL_APPDATA);
      trgfile.SetFilename(_T("NewsReader"));
      CreateDirectory(trgfile,0);
      trgfile.SetFilename(TextRef(trgfile.GetFilename())+TSC("\\")+TextRef(accname));
    }
    else
    {
      trgfile.SetDirectory(SInstallInfo.installPath);
      trgfile.SetFilename(accname);
    }
    CreateDirectory(SInstallInfo.installPath,0);
    while (CopyFile(TextRef(DISTRIBUTION)+TextRef(NREXE),SInstallInfo.installPath+TSC("\\")+TextRef(NREXE),FALSE)==FALSE)
      if (!TryRetry(SInstallInfo.installPath+TSC("\\")+TextRef(NREXE))) break;
    while (CopyFile(TextRef(DISTRIBUTION)+TextRef(NRMAP),SInstallInfo.installPath+TSC("\\")+TextRef(NRMAP),FALSE)==FALSE)
      if (!TryRetry(SInstallInfo.installPath+TSC("\\")+TextRef(NRMAP))) break;

    if (SInstallInfo.userProfiles)
    {
      HANDLE h=CreateFile(SInstallInfo.installPath+TSC("\\")+TextRef(PROFILESFILE),GENERIC_WRITE,0,0,CREATE_ALWAYS,0,0);
      CloseHandle(h);
    }
    else
    {
      DeleteFile(SInstallInfo.installPath+TSC("\\")+TextRef(PROFILESFILE));
    }
    while (!CreateAccountByTemplate(TextRef(srcfile),TextRef(trgfile),SInstallInfo))
      if (!TryRetry(TextRef(trgfile))) break;

    if (MessageBox(0,GStrRes[IDS_INSTALATIONFINISHED],GStrRes[IDS_INSTALATIONFINISHED_TITLE],MB_OKCANCEL)==IDOK)
    {
      ShellExecute(0,0,SInstallInfo.installPath+TSC("\\")+TextRef(NREXE),0,SInstallInfo.installPath,SW_SHOWNORMAL);
    }
  }
  return 0;
}