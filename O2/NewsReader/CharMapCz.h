// CharMapCz.h: interface for the CCharMapCz class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_CHARMAPCZ_H__268915C7_7A09_4A5A_9A25_3DB644BDE449__INCLUDED_)
#define AFX_CHARMAPCZ_H__268915C7_7A09_4A5A_9A25_3DB644BDE449__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CCharMapCz  
{
  char map[256];
public:
	CCharMapCz();
	virtual ~CCharMapCz();
	char operator() (char znak) {return map[(unsigned char)znak];}
	char *operator() (char *text) {char *beg=text;while (*text) *text++=map[(unsigned char)(*text)];return beg;}

};


#endif // !defined(AFX_CHARMAPCZ_H__268915C7_7A09_4A5A_9A25_3DB644BDE449__INCLUDED_)
