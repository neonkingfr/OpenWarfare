// NewsAccount.h: interface for the CNewsAccount class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_NEWSACCOUNT_H__30574AC6_962A_4690_BC83_E5337FB70B61__INCLUDED_)
#define AFX_NEWSACCOUNT_H__30574AC6_962A_4690_BC83_E5337FB70B61__INCLUDED_

#include "MessCache.h"	// Added by ClassView
#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "IArchive.h"

#include "CondCalc.h"
#include "ThreadRules.h"

#define ACC_VERSION1 0x115
#define ACC_VERSION2 0x206

#define ACC_NOFILTER 9999

#define FTXI_ARTICLEBODIES 0x1 //Article bodies (recomended)
#define FTXI_SUBJECTLINE 0x2 //Subject line
#define FTXI_SUBJECTWITHOUTPROP 0x4 //Subject without properties
#define FTXI_FROMLINE 0x8 //From line
#define FTXI_HASATTCHMENT 0x10 //Has attachment (?a)
#define FTXI_HASPICTUREATTACH 0x20 //Has picture at.(?img)
#define FTXI_HASTEXTATTACHMENT 0x40 //Has text at.(?txt)
#define FTXI_HASBINARYATTACHMENT 0x80 //Has binary at.(?bin)
#define FTXI_TEXTATTACHMENTBODY 0x100 // Text attachment body
#define FTXI_ATTACHMENTFNAME 0x200 //Att. filename (with ext)
#define FTXI_ATTACHMENTEXT 0x400 //Att. extension
#define FTXI_COUNTATTACHMENTS 0x800 //Count attachments (?Na)
#define FTXI_SIZEFLAGS 0x1000 //Size flags (?s,?l,?xl)

#define FTXI_DOWNLOADBODY (FTXI_ARTICLEBODIES|FTXI_HASPICTUREATTACH|FTXI_HASTEXTATTACHMENT|FTXI_HASBINARYATTACHMENT|FTXI_TEXTATTACHMENTBODY|FTXI_ATTACHMENTFNAME|FTXI_ATTACHMENTEXT|FTXI_COUNTATTACHMENTS)
#define FTXI_DETECTATTACHMENT (FTXI_TEXTATTACHMENTBODY|FTXI_HASBINARYATTACHMENT|FTXI_HASTEXTATTACHMENT)
#define FTXI_DEFAULT (FTXI_ARTICLEBODIES|FTXI_SUBJECTWITHOUTPROP|FTXI_HASATTCHMENT|FTXI_ATTACHMENTFNAME )

#define NGDM_DOWNLOADDISABLED  0
#define NGDM_STOREBODIES  1 
#define NGDM_STOREBODIESINDEX 2
#define NGDM_INDEXONLY 3



class CNewsGroup;
class CNewsTerminal;
#include "MessageHistory.h"	// Added by ClassView
#include "NewsArticle.h"
#include "MsgIdList.h"
#include <El/Pathname/Pathname.h>
#include "FullTextIndex.h"

class Archive;

class CNewsAccount  
  { 
  CString fname;
  CString name,address,username,password,urlpic;
  CString urgentntf;
  int port;
  CNewsGroup *grlist;
  CNewsGroup *newgr;
  CTime last;
  DWORD timeout;
  CString myname,myemail;
//  CString keywords;
  CString cachepath;
  bool needauthorize;
  bool autocompact;
  bool loadstartup;
  bool startuptime;
  bool loaded;
  bool newondemand;
  bool nonewnotify;
  bool forcedownloadnew; //stahne nove skupiny a upozorni na ne i presto ze jsou funkce potlaceny
  bool issaved;  //account has been saved; CNewsAccount only set this flag. Use ResetSaveState to reset state
  bool saveinpreviousver; //account will be saved in previous version
  long refcount;
  int daysarchive;
  int daysdelete;
  char delcheck;
  //! current filter index
/*  int curFilter;
  //! current highlight index
  int curHighlight;
  */
  //! fulltext options 0 - no fulltext, 1 - fulltext once, 2 - fulltext on
  int fullTextOpt;
  //! true, if fulltext was once generated
  bool fullTextGenerated;
  //! true, if fulltext options has been extended, and new items should be appended
  bool fullTextOptAppend;
  //! list of ignored words
  CString fullTextIgnoredWords;
  //! what is included in fulltext
  unsigned long fullTextInclude;

  ThreadRules _threadRules;
  
  bool LoadArticleRecursive(CNewsTerminal &term, const CString &msgid, CNewsArtHolder **article, CNewsGroup **grp);
  public:  
	  void OnPostLoad();
	  void DeleteAccount();
    void SortAll(int sort);
    void SortAllIfNeeded();
    void CompletteThread(CNewsGroup *grp, CNewsArtHolder *beg, HWND notify);
    bool LoadArticle(const CString &msgid, CNewsArtHolder **article, CNewsGroup **grp, CString *message=0);
    bool MoveCache(CString newlocation);
    void DeleteCache();
    void GetCacheFileName(CString &fname);
    void GetCachePath(CString &path);
    void PlayUrgent();
    void Unload();
    COleDateTime GetMyLastMessageTime();
    void ChangeLastRead(COleDateTime date);
    void ChangeLastReadSmartPerGroup();
    void BuildHash();
    void Repair();
    bool GetKeyword(int index, CString& text);
    bool SetKeyword(int index, CString& text);
    bool CreateByWizard(const char *name, const char *email, BOOL &sock, BOOL &smtp);
    void InitCache();
    CMutex mutex;
    CMsgIdList msgidlist;
    COleDateTime lastread;
    CMessageHistory history;
    CMessCache cache;
    FullTextIndex fulltext;
    bool dirty;	
    HTREEITEM treeitem;
    bool extsync; //extended synchronize in progress
    int offline; //size of cache on disk
    unsigned char compacto; //true, if cache need to be checked for integrity.
    bool viewastree;  //true, if groups are displayd as tree
    int OpenConnection(CNewsTerminal& term);
    void Lock() 
      {InterlockedIncrement(&refcount);TRACE2("ACCOUNT: %s --- Lock ref %d\n",name,refcount);}
    void Release() 
      {TRACE2("ACCOUNT: %s --- Unlock ref %d\n",name,refcount);if (refcount==1) delete this;else InterlockedDecrement(&refcount);;}
    void Serialize(CGenArchive &arch);
    void Streaming(IArchive &arch);
    bool AppendGrouplist(CNewsGroup *grp);
    bool UnlinkGroup(CNewsGroup *grp);
    bool AddGroup(CNewsGroup *grp);
    bool EditInDialog();
    void SetNew(CNewsGroup *grp) 
      {newgr=grp;}
    CNewsGroup *GetNew() 
      {return newgr;}
    CNewsGroup *GetGroups() 
      {return grlist;}
    CNewsAccount();
    virtual ~CNewsAccount();
    const char *GetAccName() 
      {return name;}  
    const char *GetAddress() 
      {return address;}
    const char *GetUsername() 
      {return username;}
    const char *GetPassword() 
      {return password;}
    void SetAccName(const char *name) 
      {this->name=name;}
    void SetAccAddr(const char *name) 
      {this->address=name;}
    int GetPort() 
      {return port;}
    bool GetAuthNeed() 
      {return needauthorize;}
    CTime GetLastCheck() 
      {return last;}
    void SetLastCheck(CTime& last) 
      {this->last=last;}
    void DeleteGroups();
    const char *GetMyName() 
      {return myname;}
    const char *GetMyEmail() 
      {return myemail;}
    const char *GetIcon() 
      {return urlpic;}
    const char *GetFName() 
      {return fname;}
    void DisableStartupTime() {startuptime=false;}
    //  void ActivateKeywords() {CNewsArtHolder::keywords=keywords;}
    void SetFName(const char *fn) 
      {fname=fn;}
    void Compact(bool close,CNewsGroup *grp=NULL);
    bool CheckDel();
    ThreadRules &GetRules() {return _threadRules;}
    int GetCurFilter() 
        {return _threadRules.GetFilter();}
    int GetCurHighlight() 
      {return _threadRules.GetHighlight();}
    bool Autocompact() 
      {return autocompact;}
    // apply filter conditions
    bool ApplyCond(bool refresh, CNewsGroup *grp=NULL);
    // apply highlight conditions
    bool ApplyHighlight(bool refresh, CNewsGroup *grp=NULL);
    void PostApplyConds(CNewsGroup *grp=NULL);
    bool IsLoaded() 
      {return loaded;}
    bool IsNotifySuppressed() 
      {return nonewnotify;}
    bool IsLoadOnDemand() 
      {return newondemand;}
    void SetForceDownload(bool fc) 
      {forcedownloadnew=fc;} //stahne nove skupiny a upozorni na ne i presto ze jsou funkce potlaceny
    bool IsDownloadForced() 
      {return forcedownloadnew;}
    static void EnDeCodeCond(CString &kwd,  CString &name, CString &cond, bool encode);
	void ResetSaveState() {issaved=false;}
	bool IsSaved() {return issaved;}
    
    unsigned long GetFTInclude() {return fullTextInclude;}
    unsigned long GetFTGenerateOpt() {return fullTextOpt;}
    bool FTWasGenerated() {return fullTextGenerated;}
	bool FTNeedRescan() {return fullTextOptAppend;}
    void FTMarkGenerated() {fullTextGenerated=true;fullTextOptAppend=false;}
    void FTSetupIgnoredWords() {fulltext.LoadIgnoredWords(fullTextIgnoredWords);}
	void RemoveIndexedMark();
    void DownloadAllArticles();
	void RestoreInvalidAccount();
    
  protected:
	  void CompletteThread(CNewsTerminal &term, CNewsArtHolder *parent, int from, int to, HWND notify);
  };

//--------------------------------------------------

class CFindList;
class CNewsGroup
  {
  CNewsGroup *next;
  int minindex;
  int maxindex;
  int nlindex; //not loaded index;
  int newcount;
  int highlightcount;
  CString name;
  CNewsArtHolder * list; 
  // which item we want to sort by
  int _sortitem;
  // which item list is really sorted by
  bool _needresort;
  public:  
	  void AutoDeleteThread(CCondCalc &cond, int days);
    void Repair();
    bool SetActiveGroup(CNewsTerminal &term);
    void Archivate(COleDateTime &dtma) 
      {list=list->Archivate(dtma);}
    bool delcheck;
    //! if items are not sorted by desired sorting order, sort them
    int GetCurrentSort() const 
      {return _sortitem;}
    //! set desired sorting order, can enforce sorting in first SortItemsIfNeeded
    void SortItems(int by);
    void SortItemsLater(int by=-1) 
      {
      if (by!=-1) _sortitem=by;
      _needresort=true;
      }
    void SortIfNeeded()
      {
      if (_needresort) SortItems(-1);
      }
    bool deleted;
    void FindInGroup(GRPFINDINFO &fnd, CNewsTerminal &term);
    void CatchUp();
    static int maxarticles;
    int GetGroupCount();
    CNewsArtHolder *GetList() 
      {return list;}
    void LoadToTree(CTreeCtrl &tree, HTREEITEM root, bool astree);
    void Serialize(CGenArchive& arch);
    void Streaming(IArchive& arch);
    CNewsGroup * FindGroup(const char *name);
    bool sign;
    bool display;
    bool special;
    bool isnew;
    bool notify;
    bool automark;  //true, all new articles in this group will be mareked as read
    unsigned int autodeletedays; //nonzero - days to delete older articles;
    CString autodeletefilter; //
    char fullload; //true, if articles should be loaded with their bodies
    bool _downloadall; //forces to download all articles.
    HTREEITEM iteminlist;
    virtual ~CNewsGroup() 
      {
      CRWLockGuard guard(articleLock,true);guard.LockPM();
      if (list)
      {
        list->Release();
      }
      while (next) 
        {
        CNewsGroup *grp=next;
        next=grp->next;
        grp->Unlink();	  
        delete grp;
        }	
      }
    CNewsGroup()
      :nlindex(0),iteminlist(0),_sortitem(1)
        {
        next=NULL,minindex=maxindex=newcount=highlightcount=0;
        sign=false;fullload=isnew=display=false;list=NULL;special=deleted=false;notify=false;
        _needresort=true;_downloadall=false;
        automark=false;autodeletedays=0;
        }
    CNewsGroup(const CNewsGroup& other)
      :nlindex(0),iteminlist(0),_sortitem(1)
        {
        next=NULL,minindex=other.minindex;maxindex=other.maxindex;
        newcount=other.newcount;highlightcount=other.highlightcount;
        name=other.name;fullload=sign=display=false;list=NULL;isnew=false;special=deleted=false;notify=false;
        _needresort=true;_downloadall=false;
        automark=false;autodeletedays=0;
        }
    CNewsGroup(const char *name, int min, int max):
    name(name),nlindex(min-1),iteminlist(0),_sortitem(1)
      {
      next=NULL,minindex=min;maxindex=max;
      newcount=highlightcount=0;sign=display=false;list=NULL;fullload=isnew=false;special=deleted=false;notify=false;
      _needresort=true;_downloadall=false;
      automark=false;autodeletedays=0;
      }
    void SetNext(CNewsGroup *n) 
      {delete next;next=n;}
    CNewsGroup *GetNext() 
      {return next;}  
    bool Unlink() 
      {next=NULL;return true;}
    const char *GetName() 
      {return name;}
    int GetMin() 
      {return minindex;}
    int GetMax() 
      {return maxindex;}  
    int GetNotLoaded() 
      {return nlindex;}
    void CountNewsAndHighlights()
      {
      CRWLockGuard guard(articleLock,false);guard.LockPM();
      newcount=list->GetNewCount();      
      highlightcount=list->GetHighlightedCount();      
      }
    int GetNews() const 
      {return newcount;}
    int GetHighlights() const 
      {return highlightcount;}
    
    void DecNews() 
      {if (newcount) newcount--;}
    int ReadGroup(CNewsTerminal &term, bool extended,const char *name,CMsgIdList &hash, long *urgent);
    bool Compact(const char *kwdlist, int setindex, bool close)
      {
      CRWLockGuard guard(articleLock,false);guard.LockPM();
      if (list && !special) return list->CalcHierarchy2(this,NULL,setindex,close);else return false;
      }
    void ApplyCond(CCondCalc &cond,bool all=false);
    void ApplyHighlight(CCondCalc &cond,bool all=false);
    void SetList(CNewsArtHolder *hld) 
      {list=hld;}
    void ClearChanged()
    {
      CNewsArtHolder *hld=list;
      while (hld) {hld->Changed(true);hld=hld->GetNext();}
    }
  };

//--------------------------------------------------

class CFindList
  {
  public:
    CFindList *next;
    CNewsArtHolder *hlda;
    CNewsGroup *grp;
    float accuracy; //! relevance score (0..100)
    const char *msgid; //! message id, if message is unknown	
    CFindList()
      {next=NULL;msgid=NULL;}
    ~CFindList()
      {
      free(const_cast<char *>(msgid));
	  if (hlda) hlda->Release();
      CFindList *nx=next;
      while (nx)
        {
        CFindList *q=nx;
        nx=q->next;
        q->next=NULL;
        delete q;
        }	  
      }
  };

//--------------------------------------------------

#endif // !defined(AFX_NEWSACCOUNT_H__30574AC6_962A_4690_BC83_E5337FB70B61__INCLUDED_)
