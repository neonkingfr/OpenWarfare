// MessageHistory.h: interface for the CMessageHistory class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MESSAGEHISTORY_H__FE5061E0_8DDC_11D5_B39F_00C0DFAE7D0A__INCLUDED_)
#define AFX_MESSAGEHISTORY_H__FE5061E0_8DDC_11D5_B39F_00C0DFAE7D0A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


#include <es/strings/rstring.hpp>

struct SMessHistoryItem
  {
  unsigned long cntr;
  CString messid;  
  static unsigned long idcntr;
  SMessHistoryItem() 
	{
	cntr=++idcntr;
	}
  };

#define CMSGHISTORYSIZE 32

class CMessageHistory  
{
  SMessHistoryItem list[CMSGHISTORYSIZE];
  int pos;
  bool blank;
public:
	void BlankPage();
	bool disable;
	CString GetForward();
	CString GetBack();
	bool CanForward();
	bool CanBack();
	void AddToHistory(const CString& msgref);
	CMessageHistory();
	virtual ~CMessageHistory();

};

#endif // !defined(AFX_MESSAGEHISTORY_H__FE5061E0_8DDC_11D5_B39F_00C0DFAE7D0A__INCLUDED_)
