#if !defined(AFX_SENDOPTIONSDLG_H__11A3B17D_CBE2_48C2_8733_D9460A971EA4__INCLUDED_)
#define AFX_SENDOPTIONSDLG_H__11A3B17D_CBE2_48C2_8733_D9460A971EA4__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// SendOptionsDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CSendOptionsDlg dialog

class CSendOptionsDlg : public CDialog
{
// Construction
public:
	void SaveConfig();
	void LoadConfig();
	CSendOptionsDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CSendOptionsDlg)
	enum { IDD = IDD_SMTPSERVER };
	CString	server;
	UINT	port;
	BOOL	cctome;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CSendOptionsDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CSendOptionsDlg)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_SENDOPTIONSDLG_H__11A3B17D_CBE2_48C2_8733_D9460A971EA4__INCLUDED_)
