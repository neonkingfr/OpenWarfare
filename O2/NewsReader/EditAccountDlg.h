#if !defined(AFX_EDITACCOUNTDLG_H__B675A83B_72BC_464A_AEA0_CCF3457DF0A9__INCLUDED_)
#define AFX_EDITACCOUNTDLG_H__B675A83B_72BC_464A_AEA0_CCF3457DF0A9__INCLUDED_

#include "AccountPropDlg.h"	// Added by ClassView
#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// EditAccountDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CEditAccountDlg dialog

class CEditAccountDlg : public CDialog
{
// Construction
public:
	CAccountPropDlg prop;
	CTimeSpan spantime;
	void ShowTimeout(CTimeSpan& p);
	CEditAccountDlg(CWnd* pParent = NULL);   // standard constructor
	CString namesaved;

// Dialog Data
	//{{AFX_DATA(CEditAccountDlg)
	enum { IDD = IDD_ACCOUNT };
	CEdit	wndAddress;
	CEdit	wndPassword;
	CButton	wndAuthorize;
	CEdit	wndUsername;
	CStatic	wndTimeoutTime;
	CSliderCtrl	wndTimeout;
	CString	address;
	BOOL	authorize;
	CString	myname;
	CString	name;
	CString	password;
	int		timeout;
	CString	username;
	CString	email;
	int		port;
	CString	time;
	CString	urlpic;
	int		limit;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CEditAccountDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	void CheckAccountValidity();

	// Generated message map functions
	//{{AFX_MSG(CEditAccountDlg)
	afx_msg void OnAuthorize();
	virtual BOOL OnInitDialog();
	afx_msg void OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar);
	virtual void OnOK();
	afx_msg void OnChangeName();
	afx_msg void OnChangeAddress();
	afx_msg void OnChangeMyemail();
	afx_msg void OnPreview();
	afx_msg void OnProperties();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_EDITACCOUNTDLG_H__B675A83B_72BC_464A_AEA0_CCF3457DF0A9__INCLUDED_)
