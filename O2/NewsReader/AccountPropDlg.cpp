// AccountPropDlg.cpp : implementation file
//

#include "stdafx.h"
#include "newsreader.h"
#include "AccountPropDlg.h"
#include "PathBrowser.h"
#include <el/Pathname/Pathname.h>
#include "FullTextIndex.h"
#include "DogWindow.h"
#include "NewsTerminal.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CAccountPropDlg dialog


CAccountPropDlg::CAccountPropDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CAccountPropDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CAccountPropDlg)
	archcreate = FALSE;
	enablecache = FALSE;
	cachesize = 0;
	keywrds = _T("");
	deldate = 0;
	enabledel = FALSE;
	archdate = 0;
	delcheck = FALSE;
	compacto = 0;
	autocompact = FALSE;
	loadstartup = FALSE;
	urgent = _T("");
	nonewnotify = FALSE;
	downloadondemand = FALSE;
	cachepath = _T("");
	fulltextopt = 0;
	vIgnoredWords = _T("");
	vSaveInPreviousVersion = FALSE;
	vFullTextReg = FALSE;
	//}}AFX_DATA_INIT
	vFTIndex=NULL;
}


void CAccountPropDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAccountPropDlg)
	DDX_Control(pDX, IDC_FULLTEXTINCLUDE, wFullTextInclude);
	DDX_Control(pDX, IDC_CACHEPATH, wCachePath);
	DDX_Control(pDX, IDC_AUTOCOMPACT, wAutocompact);
	DDX_Control(pDX, IDC_COMPACTTO, wCompacto);
	DDX_Control(pDX, IDC_ARCHDATE, wArchDate);
	DDX_Control(pDX, IDC_OLDDELDATE, wDelDate);
	DDX_Control(pDX, IDC_CACHESIZE, wCache);
	DDX_Check(pDX, IDC_ARCHCREATE, archcreate);
	DDX_Check(pDX, IDC_CACHE, enablecache);
	DDX_Text(pDX, IDC_CACHESIZE, cachesize);
	DDX_Text(pDX, IDC_KWRDS, keywrds);
	DDX_Text(pDX, IDC_OLDDELDATE, deldate);
	DDX_Check(pDX, IDC_OLDDELETE, enabledel);
	DDX_Text(pDX, IDC_ARCHDATE, archdate);
	DDX_Check(pDX, IDC_DELCHECK, delcheck);
	DDX_Text(pDX, IDC_COMPACTTO, compacto);
	DDV_MinMaxUInt(pDX, compacto, 5, 95);
	DDX_Check(pDX, IDC_AUTOCOMPACT, autocompact);
	DDX_Check(pDX, IDC_STARTUP, loadstartup);
	DDX_Text(pDX, IDC_URGENT, urgent);
	DDX_Check(pDX, IDC_NONEWNOTIFY, nonewnotify);
	DDX_Check(pDX, IDC_DOWNLOADMAN, downloadondemand);
	DDX_Text(pDX, IDC_CACHEPATH, cachepath);
	DDX_Radio(pDX, IDC_FULLTEXTOPT, fulltextopt);
	DDX_Text(pDX, IDC_IGNOREDWORDS, vIgnoredWords);
	DDX_Check(pDX, IDC_SAVEINPREVIOUS, vSaveInPreviousVersion);
	DDX_Check(pDX, IDC_FULLTEXTREG, vFullTextReg);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CAccountPropDlg, CDialog)
	//{{AFX_MSG_MAP(CAccountPropDlg)
	ON_BN_CLICKED(IDC_CACHE, OnCache)
	ON_BN_CLICKED(IDC_ARCHCREATE, OnArchcreate)
	ON_BN_CLICKED(IDC_OLDDELETE, OnOlddelete)
	ON_BN_CLICKED(IDC_URGENTBROWSE, OnUrgentbrowse)
	ON_BN_CLICKED(IDC_PLAY, OnPlay)
	ON_BN_CLICKED(IDC_CACHEBROSE, OnCachebrose)
	ON_BN_CLICKED(IDC_FULLTEXTDUMP, OnFulltextdump)
	ON_WM_DRAWITEM()
	ON_CBN_SELENDOK(IDC_FULLTEXTINCLUDE, OnSelendokFulltextinclude)
	ON_BN_CLICKED(IDC_ENABLEINCLUDEFULLTEX, OnEnableincludefulltex)
	ON_BN_CLICKED(IDC_SAVEINPREVIOUS, OnSaveinprevious)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CAccountPropDlg message handlers

void CAccountPropDlg::OnCache() 
  {
  BOOL p=IsDlgButtonChecked(IDC_CACHE);
  wCache.EnableWindow(p);	
  wCompacto.EnableWindow(p);
  wAutocompact.EnableWindow(p);
  wCachePath.EnableWindow(p);
  GetDlgItem(IDC_CACHEBROSE)->EnableWindow(p);
  GetDlgItem(IDC_RADIO2)->EnableWindow(p);
  GetDlgItem(IDC_RADIO3)->EnableWindow(p);
  GetDlgItem(IDC_FULLTEXTREG)->EnableWindow(p);
  GetDlgItem(IDC_FULLTEXTDUMP)->EnableWindow(p);
  GetDlgItem(IDC_IGNOREDWORDS)->EnableWindow(p);
 }


void CAccountPropDlg::OnArchcreate() 
  {
  BOOL p=IsDlgButtonChecked(IDC_ARCHCREATE);
  wArchDate.EnableWindow(p);	
  }

void CAccountPropDlg::OnOlddelete() 
  {
  BOOL p=IsDlgButtonChecked(IDC_OLDDELETE);
  wDelDate.EnableWindow(p);	
  }

BOOL CAccountPropDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
  OnCache();	
  OnArchcreate();
  OnOlddelete();
  wFullTextInclude.SetCurSel(0);
  OnSelendokFulltextinclude();
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CAccountPropDlg::OnOK() 
{	
  BOOL p1=IsDlgButtonChecked(IDC_ARCHCREATE);
  BOOL p2=IsDlgButtonChecked(IDC_OLDDELETE);
  if (p1==TRUE && p2==TRUE)
	{
	UINT arch=GetDlgItemInt(IDC_ARCHDATE);
	UINT del=GetDlgItemInt(IDC_OLDDELDATE);
	if (arch>del) 
	  {
	  NrMessageBox(IDS_DELDATELESSARCHDATE);
	  wArchDate.SetFocus();
	  return;
	  }
	}

  if (UpdateData()==FALSE) return;	
 DestroyWindow();
}

void CAccountPropDlg::OnCancel() 
{
	// TODO: Add extra cleanup here
  DestroyWindow();
}

void CAccountPropDlg::OnUrgentbrowse() 
  {
  CString filter;
  CString fname;
  filter.LoadString(IDS_WAVEFILTERS);
  GetDlgItemText(IDC_URGENT,fname);
  CFileDialog dlg(TRUE,"wav",fname,OFN_FILEMUSTEXIST|OFN_NOCHANGEDIR|OFN_NONETWORKBUTTON|OFN_HIDEREADONLY,filter,this);
  if (dlg.DoModal()==IDOK)
	{
	SetDlgItemText(IDC_URGENT,dlg.GetPathName());
	}
  }

#include "mmsystem.h"

void CAccountPropDlg::OnPlay() 
  {
  CString fname;
  GetDlgItemText(IDC_URGENT,fname);
  if (fname!="") PlaySound(fname,NULL,SND_FILENAME|SND_ASYNC);
  }



void CAccountPropDlg::OnCachebrose() 
  {
  CString pth;
  GetDlgItemText(IDC_CACHEPATH,pth);
  if (AskForPath(pth))
	{
	SetDlgItemText(IDC_CACHEPATH,pth);
	}
  } 

static bool DumpProgress(unsigned long cur, bool total)
{
  if (total) {::DogResetProgress();::DogSetMax(cur);}else ::DogSetPos(cur);
  runflag=true;
  return true;
}

static UINT DumpThread(LPVOID data)
{
  FullTextIndex *vFTIndex=(FullTextIndex *)data;
  ::DogShowStatusV(IDS_DUMPINGSTATISTICS);
  vFTIndex->DumpStatistics(DumpProgress);
  ::DogShowStatusV(IDS_DONE);  
  return 0;  
}

void CAccountPropDlg::OnFulltextdump() 
{
CWaitCursor curs;
ASSERT(vFTIndex!=NULL);
if (vFTIndex->IsCreated())
{
  if (AfxMessageBox(IDS_FULLTEXTDUMPWILLBECREATED,MB_OKCANCEL|MB_ICONINFORMATION)==IDOK)  AfxBeginThread((AFX_THREADPROC)DumpThread,vFTIndex);    
}
else
  AfxMessageBox(IDS_FULLTEXTDATABASENOTCREATED,MB_OK|MB_ICONSTOP);
}

void CAccountPropDlg::OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct) 
{
  if (nIDCtl==IDC_FULLTEXTINCLUDE)
  {
    CDC dc;
    dc.Attach(lpDrawItemStruct->hDC);
    COLORREF bg,fg;
    if (lpDrawItemStruct->itemState & ODS_SELECTED)
    {
      bg=GetSysColor(COLOR_HIGHLIGHT);
      fg=GetSysColor(COLOR_HIGHLIGHTTEXT);
    }
    else
    {
      bg=GetSysColor(COLOR_WINDOW);
      fg=GetSysColor(COLOR_WINDOWTEXT);
    }
    CFont *curfont=NULL;
    if (fulltextflgs & (1<<lpDrawItemStruct->itemID))
    {
      if (boldFont.m_hObject==NULL)
      {
        LOGFONT lg;        
        CFont *fnt=dc.GetCurrentFont();
        fnt->GetLogFont(&lg);
        lg.lfWeight=800;
        lg.lfUnderline=TRUE;
        boldFont.CreateFontIndirect(&lg);
      }
    curfont=dc.SelectObject(&boldFont);
    }
    dc.FillSolidRect(&lpDrawItemStruct->rcItem,bg);
    dc.SetBkColor(bg);
    dc.SetTextColor(fg);
    char *buff=(char *)alloca(wFullTextInclude.GetLBTextLen(lpDrawItemStruct->itemID)+1);
    wFullTextInclude.GetLBText(lpDrawItemStruct->itemID,buff);
    dc.DrawTextA(buff,strlen(buff),&lpDrawItemStruct->rcItem,DT_SINGLELINE |DT_VCENTER|DT_LEFT|DT_NOPREFIX);
    if (curfont) dc.SelectObject(curfont);
    dc.Detach();
  }
else  
	CDialog::OnDrawItem(nIDCtl, lpDrawItemStruct);
}

void CAccountPropDlg::OnSelendokFulltextinclude() 
  {
  int p=wFullTextInclude.GetCurSel();
  CheckDlgButton(IDC_ENABLEINCLUDEFULLTEX,(fulltextflgs & (1<<p))!=0);
  }

void CAccountPropDlg::OnEnableincludefulltex() 
{
  int p=wFullTextInclude.GetCurSel();
  fulltextflgs  ^=(1<<p);
  wFullTextInclude.Invalidate();
  if (fulltextflgs==0) fulltextflgs=0x1;
  if ((fulltextflgs & 0x6)==0x6)
  {
    if ((p<<1)==0x4) fulltextflgs&=~0x2;
    else fulltextflgs&=~0x4;
  }
}


void CAccountPropDlg::OnSaveinprevious() 
{
  if (IsDlgButtonChecked(IDC_SAVEINPREVIOUS))
    AfxMessageBox(IDS_SAVEINPREVIOUSVERWARNING);
}


