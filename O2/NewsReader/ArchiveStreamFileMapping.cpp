// ArchiveStreamFileMapping.cpp: implementation of the ArchiveStreamFileMapping class.
//
//////////////////////////////////////////////////////////////////////

#include <windows.h>
#include "ArchiveStreamFileMapping.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

ArchiveStreamFileMapping::ArchiveStreamFileMapping(HANDLE filehandle,DWORD protect,DWORD granuality, bool autoclose, const char *objectname)
  {
  SYSTEM_INFO nfo;
  GetSystemInfo(&nfo);
  _mapobject=0;
  _curview=0;
  _file=filehandle;
  _granuality=((granuality-1)/nfo.dwAllocationGranularity+1)*nfo.dwAllocationGranularity;
  _mempos=0;
  _autoclose=autoclose;
  _protect=protect;
  DWORD high;
  DWORD low;
  low=GetFileSize(_file,&high);
  _filesize=(__int64)low+((__int64)high<<32);
  _filepos=0;
  Reset();
  RemapFile(objectname);  
  }

ArchiveStreamFileMapping::ArchiveStreamFileMapping(DWORD size, DWORD protect, const char *objectname)
  {
  _mapobject=0;
  _curview=0;
  _file=0;
  _granuality=size;
  _filesize=size;
  _mempos=0;
  _autoclose=false;
  _protect=protect;
  _filepos=0;
  Reset();
  RemapFile(objectname);
  }


ArchiveStreamFileMapping::~ArchiveStreamFileMapping()
  {
  CloseMapping();
  if (_autoclose) CloseHandle(_file);
  }

void ArchiveStreamFileMapping::RemapFile(const char *name)
  {
  if (_mapobject) CloseMapping();
  if (_filesize==0) return;
  _mapobject=CreateFileMapping(_file,NULL,_protect,(DWORD)(_filesize>>32),(DWORD)(_filesize & 0xFFFFFFFF),name);
  if (_mapobject==NULL) _err=ASFM_UNABLETOCREATEMAPPING;    
  }

void ArchiveStreamFileMapping::CloseMapping()
  {
  if (_curview) Unmap();
  CloseHandle(_mapobject);
  _mapobject=NULL;
  }

void ArchiveStreamFileMapping::Unmap()
  {
  if (UnmapViewOfFile(_curview)==FALSE) //unmap file
    {
    _err=GetLastError();
    return;
    }
  _curview=NULL;      //set that file is unmapped
  _filepos+=_mempos; //update file position
  _mempos=0;         //reset memory position
  }

void ArchiveStreamFileMapping::Map(DWORD dwDesiredAccess)
  {
  if (_curview) return;             //already mapped, do nothing
  _mempos=(DWORD)(_filepos % _granuality);   //calculate new memory position
  _filepos-=_mempos;                //calculate begin of view
  DWORD mapsz;                      
  if (_filepos+_granuality>_filesize) mapsz=(DWORD)(_filesize-_filepos); //remain size;
  else mapsz=_granuality;   
  if (mapsz)
    {
    //create view of file
    _curview=MapViewOfFile(_mapobject,dwDesiredAccess,(DWORD)(_filepos>>32),(DWORD)(_filepos & 0xFFFFFFFF),mapsz);   
    if (_curview==NULL) _err=ASFM_UNABLETOMAPVIEWOFFILE;
    }
  else
    _curview=NULL;
  }

__int64 ArchiveStreamFileMapping::Tell()
  {
  if (_err) return -1;
  return _filepos+_mempos;
  }

void ArchiveStreamFileMapping::Seek(__int64 lOff, SeekOp seekop)
  {
  if (_err) return;
  Unmap();
  switch (seekop)
    {
    case begin: _filepos=lOff;break;
    case current: _filepos+=lOff;break;
    case end: _filepos=_filesize+lOff;break;
    }
  if (_filepos<0) _filepos=0;
  if (_filepos>=_filesize) _filepos=_filesize;
  }

int ArchiveStreamFileMappingIn::DataExchange(void *buffer, int maxsize)
  {  
  int uncomplette=0;
  if (_filepos+_mempos+maxsize>_filesize) 
    {
	uncomplette=maxsize=(int)(_filesize-(_filepos+_mempos));
	if (uncomplette==0) 
	  {
	  return ASTRERR_UNCOMPLETEDATA;
	  }
	};
  if (_err) return _err;
  if (_curview==NULL) Map(FILE_MAP_READ);
  while (maxsize && !_err)
    {    
    int cursize;
    bool remap=_mempos+maxsize>_granuality;
    if (remap) cursize=_granuality-_mempos;else cursize=maxsize;
    memcpy(buffer,(char *)_curview+_mempos,cursize);
    _mempos+=cursize;
    maxsize-=cursize;
    buffer=(void *)((char *)buffer+cursize);
    if (remap)
      {
      Unmap();
      Map(FILE_MAP_READ);
      }
    }
  if (uncomplette) ReportUncomplette(uncomplette);
  return _err;
  }

void ArchiveStreamFileMappingIn::Reserved(int bytes)
  {
  Seek(bytes,current);
  }

int ArchiveStreamFileMappingOut::DataExchange(void *buffer, int maxsize)
  {
  if (_err) return _err;
  if (_file && _filepos+_mempos+maxsize>_filesize)   //file grows
    {
    _filesize=_filepos+_mempos+_grow*(maxsize/_grow+1);     //set new filesize
    RemapFile();
    }
  if (_curview==NULL) Map(FILE_MAP_WRITE);
  while (maxsize && !_err)
    {    
    int cursize;
    bool remap=_mempos+maxsize>_granuality;
    if (remap) cursize=_granuality-_mempos;else cursize=maxsize;
    memcpy((char *)_curview+_mempos,buffer,cursize);
    _mempos+=cursize;
    maxsize-=cursize;
    buffer=(void *)((char *)buffer+cursize);
    if (remap)
      {
      Unmap();
      Map(FILE_MAP_WRITE);
      }
    }
  return _err;
  }

ArchiveStreamFileMappingOut::~ArchiveStreamFileMappingOut()
  {
  CloseMapping();
  if (_file)
    {
    LONG high=(DWORD)(_filepos>>32);
    SetFilePointer(_file,(DWORD)(_filepos & 0xFFFFFFFF),&high,FILE_BEGIN);
    if (SetEndOfFile(_file)==FALSE) _err=ASFM_UNABLETOSETENDOFFILE;    
    }
  }