// DlgSortModeAsk.cpp : implementation file
//

#include "stdafx.h"
#include "newsreader.h"
#include "DlgSortModeAsk.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDlgSortModeAsk dialog


CDlgSortModeAsk::CDlgSortModeAsk(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgSortModeAsk::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDlgSortModeAsk)
	m_SortMode = -1;
	//}}AFX_DATA_INIT
}


void CDlgSortModeAsk::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDlgSortModeAsk)
	DDX_Radio(pDX, IDC_SORTMODE, m_SortMode);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDlgSortModeAsk, CDialog)
	//{{AFX_MSG_MAP(CDlgSortModeAsk)
	ON_BN_CLICKED(IDC_SORTMODE, OnSortmode)
	ON_BN_CLICKED(IDC_SORTMODE2, OnSortmode)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDlgSortModeAsk message handlers

void CDlgSortModeAsk::OnSortmode() 
{
GetDlgItem(IDOK)->EnableWindow(IsDlgButtonChecked(IDC_SORTMODE)!=0 || IsDlgButtonChecked(IDC_SORTMODE2)!=0);
}

BOOL CDlgSortModeAsk::OnInitDialog() 
{
	CDialog::OnInitDialog();
	OnSortmode();

	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}
