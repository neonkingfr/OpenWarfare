//{{AFX_INCLUDES()
#include "webbrowser2.h"
//}}AFX_INCLUDES
#if !defined(AFX_MSGPREVEWDLG_H__60B8BAA0_A073_11D5_B3A0_00C0DFAE7D0A__INCLUDED_)
#define AFX_MSGPREVEWDLG_H__60B8BAA0_A073_11D5_B3A0_00C0DFAE7D0A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// MsgPrevewDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CMsgPrevewDlg dialog

class CNewsArtHolder;
class CNewsAccount;
class CNewsGroup;
class CMsgPrevewDlg : public CDialog
{
// Construction
public:
	void LoadContent(CNewsAccount *acc, CNewsGroup *grp, CNewsArtHolder *hld);
	void Create(CWnd *parent);
	virtual void OnCancel();
	CMsgPrevewDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CMsgPrevewDlg)
	enum { IDD = IDD_PREVIEW };
	CButton	bUp;
	CButton	bDown;
	CButton	bSeen;
	CStatic	pPos;
	CButton	bReplyMail;
	CButton	bReply;
	CButton	bPrint;
	CButton	bGoto;
	CButton	bDelete;
	//}}AFX_DATA
	CSize dfsz;
	CSize minsz;
	CNewsArtHolder *hld;
	CNewsAccount *acc;
	CNewsGroup *grp;
	CString user;
	CWebBrowser2 browser;


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMsgPrevewDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual void PostNcDestroy();
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CMsgPrevewDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnGetMinMaxInfo(MINMAXINFO FAR* lpMMI);
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg BOOL OnReply(UINT cmd);
	afx_msg BOOL OnDelete(UINT cmd);
	afx_msg void OnPrint();
	afx_msg void OnGoto();
	afx_msg void OnSeen();
	afx_msg void OnUp();
	afx_msg void OnDown();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MSGPREVEWDLG_H__60B8BAA0_A073_11D5_B3A0_00C0DFAE7D0A__INCLUDED_)
