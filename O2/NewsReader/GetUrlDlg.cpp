// GetUrlDlg.cpp : implementation file
//

#include "stdafx.h"
#include "newsreader.h"
#include "GetUrlDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CGetUrlDlg dialog


CGetUrlDlg::CGetUrlDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CGetUrlDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CGetUrlDlg)
	m_url = _T("");
	//}}AFX_DATA_INIT
}


void CGetUrlDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CGetUrlDlg)
	DDX_Control(pDX, IDOK, wOk);
	DDX_Control(pDX, IDC_URL, wUrl);
	DDX_Text(pDX, IDC_URL, m_url);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CGetUrlDlg, CDialog)
	//{{AFX_MSG_MAP(CGetUrlDlg)
	ON_EN_CHANGE(IDC_URL, OnChangeUrl)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CGetUrlDlg message handlers

void CGetUrlDlg::OnChangeUrl() 
{
	wOk.EnableWindow(wUrl.GetWindowTextLength()!=0);
	// send this notification unless you override the CDialog::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.
	
	// TODO: Add your control notification handler code here
	
}

BOOL CGetUrlDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	OnChangeUrl();	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}
