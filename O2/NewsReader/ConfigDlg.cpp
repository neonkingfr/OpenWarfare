// ConfigDlg.cpp : implementation file
//

#include "stdafx.h"
#include "newsreader.h"
#include "ConfigDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CConfigDlg dialog



CConfigDlg::CConfigDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CConfigDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CConfigDlg)
	autoRead = FALSE;
	newExpand = FALSE;
	showLines = FALSE;
	async = FALSE;
	msync = FALSE;
	syncmin = 0;
	acc_cmd = 0;
	cache = 0;
	shwimgs = FALSE;
	showCancel = FALSE;
	betweenThreads = FALSE;
	newontop = FALSE;
	dwLines = 0;
	lgWarm = FALSE;
	delcheck = FALSE;
	owned = FALSE;
	nrnotify = FALSE;
	bigicons = FALSE;
	maxdownload = FALSE;
	maxcount = 0;
	userprof = FALSE;
	sentread = FALSE;
	shwhtml = FALSE;
	useoe = FALSE;
	grayIcons = FALSE;
	//}}AFX_DATA_INIT
	for (int i=0;i<16;i++) usercolors[i]=RGB(rand() & 0xFF,rand() & 0xFF, rand() & 0xFF);
  }


void CConfigDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CConfigDlg)
	DDX_Control(pDX, IDC_USERPROF, bUserProf);
	DDX_Control(pDX, IDC_CACHE, wcache);
	DDX_Control(pDX, IDC_ACCSLDR, wacc_cmd);
	DDX_Check(pDX, IDC_AUTOREAD, autoRead);
	DDX_Check(pDX, IDC_NEWEXPAND, newExpand);
	DDX_Check(pDX, IDC_SHOWLINES, showLines);
	DDX_Check(pDX, IDC_ASYNC, async);
	DDX_Check(pDX, IDC_MSYNC, msync);
	DDX_Text(pDX, IDC_SYNCMIN, syncmin);
	DDV_MinMaxUInt(pDX, syncmin, 1, 999);
	DDX_Slider(pDX, IDC_ACCSLDR, acc_cmd);
	DDX_Slider(pDX, IDC_CACHE, cache);
	DDX_Check(pDX, IDC_SHWIMAGES, shwimgs);
	DDX_Check(pDX, IDC_SHOWCANCEL, showCancel);
	DDX_Check(pDX, IDC_SHOWLINES2, betweenThreads);
	DDX_Check(pDX, IDC_NEWONTOP, newontop);
	DDX_Text(pDX, IDC_DWLINES, dwLines);
	DDX_Check(pDX, IDC_LARGEWARM, lgWarm);
	DDX_Check(pDX, IDC_CHECKFORDEL, delcheck);
	DDX_Check(pDX, IDC_WINOWNED, owned);
	DDX_Check(pDX, IDC_NRNOTIFY, nrnotify);
	DDX_Check(pDX, IDC_BIGICONS, bigicons);
	DDX_Check(pDX, IDC_MAXDOWNLOAD, maxdownload);
	DDX_Text(pDX, IDC_MAXCOUNT, maxcount);
	DDV_MinMaxUInt(pDX, maxcount, 10, 9999999);
	DDX_Check(pDX, IDC_USERPROF, userprof);
	DDX_Check(pDX, IDC_SENTREAD, sentread);
	DDX_Check(pDX, IDC_SHWHTML, shwhtml);
	DDX_Check(pDX, IDC_USEOE, useoe);
	DDX_Check(pDX, IDC_GRAYICONS, grayIcons);
	//}}AFX_DATA_MAP
	if (pDX->m_bSaveAndValidate)
	  {
	  col_highlighted=highlighted;
	  col_important=important;
	  }
	else 
	  {
	  highlighted=col_highlighted;
	  important=col_important;
	  }


}


BEGIN_MESSAGE_MAP(CConfigDlg, CDialog)
	//{{AFX_MSG_MAP(CConfigDlg)
	ON_BN_CLICKED(IDC_MSYNC, OnMsync)
	ON_WM_SETCURSOR()
	ON_BN_CLICKED(IDC_SHOWLINES, OnShowlines)
	ON_BN_CLICKED(IDC_LARGEWARM, OnLargewarm)
	ON_BN_CLICKED(IDC_BPREVIEW, OnBpreview)
	ON_BN_CLICKED(IDC_MAXDOWNLOAD, OnMaxdownload)
	ON_WM_DRAWITEM()
	ON_BN_CLICKED(IDC_IMPORTANTCOLOR, OnChangeColor)
	ON_BN_CLICKED(IDC_HIGHLIGHTED, OnChangeColor)
	ON_BN_CLICKED(IDC_REGISTERNR, OnRegisternr)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CConfigDlg message handlers

#define	GROUP   "Config"
#define AUTOEXPAND "AutoExpand"
#define AUTOREAD "AutoRead"
#define NEWEXPAND "NewExpand"
#define SHOWLINES "ShowLines"
#define ASYNC "AutoSync"
#define MSYNC "SyncMinEn"
#define MSYNCVAL "SyncMin"
#define ACCCOM "Write Ahead Commands"
#define CACHE "Cache"
#define SHOWIMGS "Show Images"
#define SHOWCANCEL "Show Canceled"
#define BETWEENTHD "Between Threads"
#define NEWONTOP "New On Top"
#define LARGEWARN "Ask for large"
#define DWLINES "Max lines download"
#define DETECTATT "Detect Attachment"
#define DELCHECK "Enable Removing"
#define SMARTWRAP "Smart Wrap"
#define OWNED "Owned"
#define NRNOTIFY "NRNotify"
#define BIGICONS "Big Icons"
#define MAXCOUNT "Max Count"
#define MAXDOWNLOAD "Max Download"
#define SENTREAD "SentRead"
#define SHOWHTML "ShowHTML"
#define USEOE "UseOE"
#define HIGHLIGHTEDCOLOR "Highlighted color"
#define IMPORTANTCOLOR "Important color"
#define USERCOLORS "User colors"
#define GRAYICONS "Gray Icons"


void CConfigDlg::LoadConfig()
  {
//  autoExpand=theApp.GetProfileInt(GROUP,AUTOEXPAND,1);
  autoRead=theApp.GetProfileInt(GROUP,AUTOREAD,1);
  newExpand=theApp.GetProfileInt(GROUP,NEWEXPAND,1);
  showLines=theApp.GetProfileInt(GROUP,SHOWLINES,1);
  async=theApp.GetProfileInt(GROUP,ASYNC,1);
  msync=theApp.GetProfileInt(GROUP,MSYNC,1);
  syncmin=theApp.GetProfileInt(GROUP,MSYNCVAL,10);
  acc_cmd=theApp.GetProfileInt(GROUP,ACCCOM,16);
  cache=theApp.GetProfileInt(GROUP,CACHE,32);
  shwimgs=theApp.GetProfileInt(GROUP,SHOWIMGS,1);
  showCancel=theApp.GetProfileInt(GROUP,SHOWCANCEL,0);
  betweenThreads=theApp.GetProfileInt(GROUP,BETWEENTHD,0);
  newontop=theApp.GetProfileInt(GROUP,NEWONTOP,1);
  lgWarm=theApp.GetProfileInt(GROUP,LARGEWARN,0);
/*  smartWrp=theApp.GetProfileInt(GROUP,SMARTWRAP,0); */
  dwLines=theApp.GetProfileInt(GROUP,DWLINES,1000);
  delcheck=theApp.GetProfileInt(GROUP,DELCHECK,0);
  owned=theApp.GetProfileInt(GROUP,OWNED,0);
  nrnotify=theApp.GetProfileInt(GROUP,NRNOTIFY,0);
  bigicons=theApp.GetProfileInt(GROUP,BIGICONS,0);
  maxcount=theApp.GetProfileInt(GROUP,MAXCOUNT,300);
  maxdownload=theApp.GetProfileInt(GROUP,MAXDOWNLOAD,0);
  bigicons=theApp.GetProfileInt(GROUP,BIGICONS,0);
  sentread=theApp.GetProfileInt(GROUP,SENTREAD,0);
  shwhtml=theApp.GetProfileInt(GROUP,SHOWHTML,1);
  useoe=theApp.GetProfileInt(GROUP,USEOE,0);
  col_highlighted=theApp.GetProfileInt(GROUP,HIGHLIGHTEDCOLOR ,RGB(0,0,224));
  col_important=theApp.GetProfileInt(GROUP,IMPORTANTCOLOR ,RGB(224,0,0));
  grayIcons=theApp.GetProfileInt(GROUP,GRAYICONS ,TRUE);

  LPBYTE buff;
  UINT sz;
  
  theApp.GetProfileBinary(GROUP,USERCOLORS,&buff,&sz);
  if (buff)
	{
	memcpy(usercolors,buff,__min(sz,sizeof(usercolors)));
	delete [] buff;
	}

  dispprop.LoadSettings();
  }

void CConfigDlg::SaveConfig()
  {
//  theApp.WriteProfileInt(GROUP,AUTOEXPAND,autoExpand);
  theApp.WriteProfileInt(GROUP,AUTOREAD,autoRead);
  theApp.WriteProfileInt(GROUP,NEWEXPAND,newExpand);
  theApp.WriteProfileInt(GROUP,SHOWLINES,showLines);
  theApp.WriteProfileInt(GROUP,ASYNC,async);
  theApp.WriteProfileInt(GROUP,MSYNC,async);
  theApp.WriteProfileInt(GROUP,MSYNCVAL,syncmin);
  theApp.WriteProfileInt(GROUP,ACCCOM,acc_cmd);
  theApp.WriteProfileInt(GROUP,CACHE,cache);
  theApp.WriteProfileInt(GROUP,SHOWIMGS,shwimgs);
  theApp.WriteProfileInt(GROUP,SHOWCANCEL,showCancel);
  theApp.WriteProfileInt(GROUP,BETWEENTHD,betweenThreads);
  theApp.WriteProfileInt(GROUP,NEWONTOP,newontop);
  theApp.WriteProfileInt(GROUP,LARGEWARN,lgWarm);
/*  theApp.WriteProfileInt(GROUP,SMARTWRAP,smartWrp); */
  theApp.WriteProfileInt(GROUP,DWLINES,dwLines);
  theApp.WriteProfileInt(GROUP,DELCHECK,delcheck);
  theApp.WriteProfileInt(GROUP,OWNED,owned);
  theApp.WriteProfileInt(GROUP,NRNOTIFY,nrnotify);
  theApp.WriteProfileInt(GROUP,BIGICONS,bigicons);
  theApp.WriteProfileInt(GROUP,MAXDOWNLOAD,maxdownload);
  theApp.WriteProfileInt(GROUP,MAXCOUNT,maxcount);
  theApp.WriteProfileInt(GROUP,SENTREAD,sentread);
  theApp.WriteProfileInt(GROUP,SHOWHTML,shwhtml);
  theApp.WriteProfileInt(GROUP,USEOE,useoe);
  theApp.WriteProfileInt(GROUP,HIGHLIGHTEDCOLOR ,col_highlighted);
  theApp.WriteProfileInt(GROUP,IMPORTANTCOLOR ,col_important);
  theApp.WriteProfileBinary(GROUP,USERCOLORS,(LPBYTE)usercolors,sizeof(usercolors));
  theApp.WriteProfileInt(GROUP,GRAYICONS, grayIcons);
  }

void CConfigDlg::OnMsync() 
  {
  // TODO: Add your control notification handler code here
  GetDlgItem(IDC_SYNCMIN)->EnableWindow(IsDlgButtonChecked(IDC_MSYNC));
  }

BOOL CConfigDlg::OnInitDialog() 
{
	userprof=theApp.useprofiles;
	CDialog::OnInitDialog();
	
	wacc_cmd.SetRangeMin(0);	
	wacc_cmd.SetRangeMax(32);	
	wacc_cmd.SetPos(acc_cmd);
	wacc_cmd.Invalidate();
	wacc_cmd.PostMessage(WM_LBUTTONUP,0,0);
	OnMsync();	
	wcache.SetRange(2,256);
    wcache.SetTicFreq(16);
	wcache.SetPos(cache);
	wcache.PostMessage(WM_LBUTTONUP,0,0);
	OnShowlines();
	OnLargewarm();
	OnMaxdownload();
	bUserProf.EnableWindow(theApp.enable_update|| !theApp.useprofiles);

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

BOOL CConfigDlg::OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message) 
{
	SetTooltip(this,pWnd);
	return CDialog::OnSetCursor(pWnd, nHitTest, message);
}

void CConfigDlg::OnShowlines() 
  {
  GetDlgItem(IDC_SHOWLINES2)->EnableWindow(IsDlgButtonChecked(IDC_SHOWLINES)!=0);
  }

void CConfigDlg::OnLargewarm() 
  {
  GetDlgItem(IDC_DWLINES)->EnableWindow(IsDlgButtonChecked(IDC_LARGEWARM));	
  }

void CConfigDlg::OnBpreview() 
  {
  dispprop.DoModal();	
  }


void CConfigDlg::OnMaxdownload() 
  {
  GetDlgItem(IDC_MAXCOUNT)->EnableWindow(IsDlgButtonChecked(IDC_MAXDOWNLOAD));	
  }

void CConfigDlg::OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct) 
  {
  if (nIDCtl==IDC_IMPORTANTCOLOR || nIDCtl==IDC_HIGHLIGHTED)
	{
	COLORREF color=nIDCtl==IDC_IMPORTANTCOLOR?important:highlighted;
	CDC dc;
	char buff[100];
	dc.Attach(lpDrawItemStruct->hDC);
	CRect rc=lpDrawItemStruct->rcItem;
	
	int bw=(GetRValue(color)*250+GetGValue(color)*500+GetBValue(color)*100)/(250+500+100);

	if (bw<128) 
	  dc.SetTextColor(RGB(255,255,255));
	else
	  dc.SetTextColor(RGB(0,0,0));
	if (lpDrawItemStruct->itemState & ODS_SELECTED)		
	  dc.DrawEdge(&lpDrawItemStruct->rcItem,EDGE_RAISED,BF_ADJUST|BF_RECT|BF_FLAT);		
	else
	   dc.DrawEdge(&lpDrawItemStruct->rcItem,EDGE_RAISED,BF_RECT);			
	rc-=CRect(3,3,3,3);
	dc.FillSolidRect(&rc,color);
	::GetWindowText(lpDrawItemStruct->hwndItem,buff,100);
	dc.DrawTextA(buff,&rc,DT_CENTER|DT_VCENTER|DT_SINGLELINE);
	}

  CDialog::OnDrawItem(nIDCtl, lpDrawItemStruct);
  }

void CConfigDlg::OnChangeColor() 
  {
  int id=GetFocus()->GetDlgCtrlID();
  CColorDialog cdlg(id==IDC_IMPORTANTCOLOR?important:highlighted,CC_ANYCOLOR|CC_RGBINIT);
  cdlg.m_cc.lpCustColors=usercolors;
  if (cdlg.DoModal()==IDOK)
	{
	if (id==IDC_IMPORTANTCOLOR) important=cdlg.GetColor();
	else highlighted=cdlg.GetColor();
	}
  Invalidate();  
  }

static HKEY OpenKey(HKEY base, const char *path)
  {
  char *buff=(char *)alloca(strlen(path));
  strcpy(buff,path);
  HKEY last=base;
  HKEY cur;
  DWORD temp;
  char *c=buff;
  do
    {
    c=strchr(buff,'\\');
    if (c!=NULL) *c=0;
    int res=RegCreateKeyEx(last,buff,0,"",REG_OPTION_NON_VOLATILE,KEY_ALL_ACCESS,NULL,&cur,&temp);
    if (last!=base) RegCloseKey(last);
    last=cur;
    if (res!=ERROR_SUCCESS) return NULL;
    buff=c+1;
    }
  while (c);
  return cur;
  }



void CConfigDlg::OnRegisternr() 
  {
  HKEY key=OpenKey(HKEY_CLASSES_ROOT,"news\\shell\\open\\command");
  if (key!=NULL) 
    {
    int size=4*MAX_PATH;
    char *pathname=(char *)alloca(size);
    ::GetModuleFileName(AfxGetInstanceHandle(),pathname,size);
    strcat(pathname," %1");
    if (RegSetValueEx(key,NULL,0,REG_SZ,(BYTE *)pathname,size)==ERROR_SUCCESS)
      {
      RegCloseKey(key);
      GetDlgItem(IDC_REGISTERNR)->EnableWindow(FALSE);
      return;
      }
    RegCloseKey(key);
    }
  AfxMessageBox("Operation has failed");
  }
