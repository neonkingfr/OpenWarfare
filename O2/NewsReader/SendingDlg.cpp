// SendingDlg.cpp : implementation file
//

#include "stdafx.h"
#include "newsreader.h"
#include "SendingDlg.h"
#include "NewsTerminal.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSendingDlg dialog


CSendingDlg::CSendingDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CSendingDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CSendingDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CSendingDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSendingDlg)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CSendingDlg, CDialog)
	//{{AFX_MSG_MAP(CSendingDlg)
	ON_WM_TIMER()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSendingDlg message handlers

BOOL CSendingDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	if (animace.m_hImageList==NULL) animace.Create(IDB_SENDMAIL,32,1,RGB(255,0,255));
	frame=0;
	SetTimer(7601,100,0);
	animace.SetBkColor(GetSysColor(COLOR_3DFACE));	
	// TODO: Add extra initialization here
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CSendingDlg::OnTimer(UINT nIDEvent) 
{
	// TODO: Add your message handler code here and/or call default
	POINT pt={0,0};
	GetWindow(GW_CHILD)->MapWindowPoints(this,&pt,1);
	CDC *dc=GetDC();
	animace.Draw(dc,frame,pt,ILD_NORMAL);
	ReleaseDC(dc);
	if (frame || runflag) frame++;
	runflag=false;
	if (frame>=animace.GetImageCount()) frame=0;
	CDialog::OnTimer(nIDEvent);
}

void CSendingDlg::OnCancel() 
  {
  CNewsTerminal::StopTrafic();
  }

void CSendingDlg::OnOK()
  {

  }
