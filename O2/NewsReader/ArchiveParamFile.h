// ArchiveParamFile.h: interface for the ArchiveParamFile class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_ARCHIVEPARAMFILE_H__8A6FB41F_1E46_4BDB_8C9C_FCB13BD4371B__INCLUDED_)
#define AFX_ARCHIVEPARAMFILE_H__8A6FB41F_1E46_4BDB_8C9C_FCB13BD4371B__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "IArchive.h" 

#define APF_ADDCLASSFAILED -100
#define APF_CLASSNOTFOUND -101
#define APF_VARIABLEISTOOLONG -102
#define APF_UNABLETOGETVARIABLE -103



#define APF_MAXVARIABLENAME 255

class ParamFile;
class ParamEntry;
class ArchiveParamFileClassStack;

#define ARCHIVEDECLAREINTEXCHANGE(type) \
	protected: virtual int OverrideDoExchange(type &data) \
      { \
      int p=(int)data; \
      int ret=OverrideDoExchange(p); \
      data=(type)p;return ret; \
      }

#define ARCHIVEDECLAREFLOATEXCHANGE(type) \
	protected: virtual int OverrideDoExchange(type &data) \
      { \
      float p=(float)data; \
      int ret=OverrideDoExchange(p); \
      data=(type)p;return ret; \
      }


#include "BTreeDB.h"

struct _APFAutoIndex
  {
  ParamEntry *entryptr;
  int index;
  bool oldver;
  int GetKeyPart(int bit, int sz) const {return (((unsigned long)entryptr)>>bit) & ((1<<sz)-1);}
  int GetBitLen() const {return sizeof(entryptr)*8;}
  bool KeySame(const _APFAutoIndex &other) const {return entryptr==other.entryptr;}
  };

TypeIsSimple(_APFAutoIndex);


class ArchiveParamFile : public IArchive  
{
    ParamFile *_pfile;    
    ArchiveParamFileClassStack *_class_stack;    
    BTreeDB<_APFAutoIndex> _autoIndex;
    int _err;
    char curVarName[APF_MAXVARIABLENAME+1];    
    char curIndexedName[APF_MAXVARIABLENAME+1];    
    int _unnamed_index;
    char *_internalBuff;
    int _savedIndex;
public:
	void CloseSection(const char *name, int type);
    bool OpenSection(const char *name, int type);
	ArchiveParamFile(ParamFile *pfile, bool store);
    ~ArchiveParamFile();
    void SetMode(bool _storemode);
    void Reserved(int bytes) {}; //Reserved cannot be implemented here;
protected:
	void CloseVariable();
	bool OpenVariable(const char *name);
	bool OpenClass(const char *name);
	void CloseClass();
	void GetCurVarName(char *buff);

    int OverrideDoExchange(int &p);
    int OverrideDoExchange(float &p);
	ARCHIVEDECLAREINTEXCHANGE(bool)
	ARCHIVEDECLAREINTEXCHANGE(char)
	ARCHIVEDECLAREINTEXCHANGE(unsigned char)
	ARCHIVEDECLAREINTEXCHANGE(short)
	ARCHIVEDECLAREINTEXCHANGE(unsigned short)
	ARCHIVEDECLAREINTEXCHANGE(unsigned int)
	ARCHIVEDECLAREINTEXCHANGE(long)
	ARCHIVEDECLAREINTEXCHANGE(unsigned long)
	ARCHIVEDECLAREINTEXCHANGE(__int64)
	ARCHIVEDECLAREINTEXCHANGE(unsigned __int64)	
	ARCHIVEDECLAREFLOATEXCHANGE(double)    	
    virtual int OverrideDoBinaryExchange(void *ptr, unsigned long size);
    int OverrideDoExchange(char *& );
    int OverrideDoExchange(short *& );
    int OverrideDoExchange(unsigned short *& );
    int OverrideDoExchange(unsigned char *&val)
      {
      char *temp=(char *)val;
      OverrideDoExchange(temp);
      val=(unsigned char *)temp;
      return _err;
      }
    bool HandleDefaultValue() {return true;}
    bool Check(const char *text);
    void OverrideSetError(int err) {_err=err;}
    int OverrideGetError() {return _err;}
    void *OverideGetInstanceData(int) {return NULL;};
	bool OverrideExchangeVersion(int &val);
    const char *GetIndexedName(ParamEntry *level, const char *srcname);
    void DecIndexedName(ParamEntry *level, const char *srcname);
};



#endif // !defined(AFX_ARCHIVEPARAMFILE_H__8A6FB41F_1E46_4BDB_8C9C_FCB13BD4371B__INCLUDED_)
