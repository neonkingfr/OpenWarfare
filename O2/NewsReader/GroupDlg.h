#if !defined(AFX_GROUPDLG_H__BFC7DC29_21C0_4AA1_9363_88C59991560F__INCLUDED_)
#define AFX_GROUPDLG_H__BFC7DC29_21C0_4AA1_9363_88C59991560F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// GroupDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CGroupDlg dialog

#include "NewsAccount.h"

class CNewsArticle;
class CNewsArtHolder;

class CGroupDlg : public CDialog
{
// Construction
	CString modes[4];
    bool selscan;
	int viewmode;
	bool dupscan;
	CNewsGroup *checker;
	void LoadToList();
	unsigned int tmr;
	int filterop;
	bool recurse;
public:
	CNewsAccount *curacc;
	CGroupDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CGroupDlg)
	enum { IDD = IDD_SIGNUPDLG };
	CComboBox	wMode;
	CTabCtrl	wTab;
	CButton	signup;
	CListCtrl	grlist;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CGroupDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	void ReformatList(int perc);
	int CheckDupFaster();
	int ReadGroup(CNewsTerminal &term);
	void SetViewMode(int vm);

	// Generated message map functions
	//{{AFX_MSG(CGroupDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnReload();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnReleaseList(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnItemchangedList(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnSignup();
	afx_msg void OnDestroy();
	virtual void OnCancel();
	afx_msg void OnChangeFilter();
	afx_msg BOOL OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message);
	afx_msg void OnSelchangeTab(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnSelendokCombomode();
	afx_msg void OnDblclkGroups(NMHDR* pNMHDR, LRESULT* pResult);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_GROUPDLG_H__BFC7DC29_21C0_4AA1_9363_88C59991560F__INCLUDED_)
