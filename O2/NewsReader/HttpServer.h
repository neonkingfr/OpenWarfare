// HttpServer.h: interface for the CHttpServer class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_HTTPSERVER_H__93D69E23_1F05_4249_AC3B_AC2F753E977F__INCLUDED_)
#define AFX_HTTPSERVER_H__93D69E23_1F05_4249_AC3B_AC2F753E977F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "LineTerminal.h"



#endif // !defined(AFX_HTTPSERVER_H__93D69E23_1F05_4249_AC3B_AC2F753E977F__INCLUDED_)
#include "webbrowser2.h"	// Added by ClassView

class CNewsArtHolder;
class CNewsAccount;
class CNewsGroup;

#define LHS_MAXOBJECTS 128

/////////////////////////////////////////////////////////////////////////////
// CLocalHttpServer command target

class CLocalHttpServer : public CAsyncSocket
	{
	CWebBrowser2 *browser;
	struct SObjectList
	  {
	  CNewsAccount *acc;
	  CNewsGroup *grp;
	  CNewsArtHolder *hld;
	  int msgpart;
	  };

	SObjectList objects[LHS_MAXOBJECTS];
	int index;
	
// Attributes
public:

// Operations
public:

	void SetBrowser(CWebBrowser2 *b) {browser=b;}
	CLocalHttpServer();
	virtual ~CLocalHttpServer();

// Overrides
public:
		const char * BuildUrl(int objectid);
		bool GetContentType(const char *ext, char *buff, int size);
		void SendError(CLineTerminal &);
		void SendObject(CLineTerminal &term,int index);
		int RegisterObject(CNewsAccount *acc, CNewsGroup *grp, CNewsArtHolder *hldr, int messagePart);
	void FlushObjects();
	BOOL Init();
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CLocalHttpServer)
	public:
	virtual void OnAccept(int nErrorCode);
	//}}AFX_VIRTUAL

	// Generated message map functions
	//{{AFX_MSG(CLocalHttpServer)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG

// Implementation
protected:
};

/////////////////////////////////////////////////////////////////////////////
