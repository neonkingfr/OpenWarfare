#if !defined(AFX_COMPLETTETHREADDLG_H__E53A81E0_4B14_42FC_8844_8B0E480A3706__INCLUDED_)
#define AFX_COMPLETTETHREADDLG_H__E53A81E0_4B14_42FC_8844_8B0E480A3706__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// CompletteThreadDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CCompletteThreadDlg dialog

class CCompletteThreadDlg : public CDialog
{
// Construction
public:
	void OnOK();
	void Thread();
    CNewsAccount *acc;
    CNewsGroup *grp;
    CNewsArtHolder *beg;
    int cntmsg;
	CCompletteThreadDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CCompletteThreadDlg)
	enum { IDD = IDD_COMPLETTETHREAD };
	CStatic	status;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCompletteThreadDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CCompletteThreadDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnStop();
    afx_msg LRESULT OnCountMessages(WPARAM wParam,LPARAM lParam);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_COMPLETTETHREADDLG_H__E53A81E0_4B14_42FC_8844_8B0E480A3706__INCLUDED_)
