// FindDlg.cpp : implementation file
//

#include "stdafx.h"
#include "newsreader.h"
#include "FindDlg.h"
#include "textFind.h"
#include "AccountList.h"
#include "MessageList.h"
#include "MainFrm.h"
#include "WindowPlacement.h"

#define SECTION "FindDlg"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CFindDlg dialog

#define NRFN_RESETLIST (WM_APP+4587)
#define NRFN_UPDATEBYHLDA (WM_APP+4588)

#define _SORTBYTOPIC 0
#define _SORTBYUSER 1
#define _SORTBYDATE 2
#define _SORTBYGROUP 3
#define _SORTBYRANK 4

CFindDlg::CFindDlg(CWnd* pParent /*=NULL*/)
: CDialog(CFindDlg::IDD, pParent)
  {
  //{{AFX_DATA_INIT(CFindDlg)
  from = _T("");
  message = _T("");
  subject = _T("");
  allgroups = FALSE;
  connserver = FALSE;
  attch = FALSE;
  searchWizard = FALSE;
  resInGrp = FALSE;
  vSearchMode = 0;
	vDateNewest = COleDateTime::GetCurrentTime();
	vDateOldest = COleDateTime::GetCurrentTime();
	vCheckDateOldest = FALSE;
	vCheckDateNewest = FALSE;
	//}}AFX_DATA_INIT
  fndlist=NULL;
  findthrd=NULL;  
  substrings = FALSE;
  ft_minScore=25;
  ft_unknown=true;
  LoadSettings();  
  }

void CFindDlg::DoDataExchange(CDataExchange* pDX)
  {
  CDialog::DoDataExchange(pDX);
  //{{AFX_DATA_MAP(CFindDlg)
	DDX_Control(pDX, IDC_DATEOLDEST, wDateOldest);
	DDX_Control(pDX, IDC_DATENEWEST, wDateNewest);
	DDX_Control(pDX, IDC_SEARCHUSING, wSearchUsing);
  DDX_Control(pDX, IDC_NEWSEARCH, bnewsearch);
  DDX_Control(pDX, IDC_FINDNOW, bfind);
  DDX_Control(pDX, IDC_STOPFIND, bstop);
  DDX_Control(pDX, IDC_LIST, list);
  DDX_Text(pDX, IDC_FROM, from);
  DDX_Text(pDX, IDC_MESSAGE, message);
  DDX_Text(pDX, IDC_SUBJECT, subject);
  DDX_Check(pDX, IDC_ALLGROUPS, allgroups);
  DDX_Check(pDX, IDC_CONNECTSERVER, connserver);
  DDX_Check(pDX, IDC_HASATTCHM, attch);
	DDX_CBIndex(pDX, IDC_SEARCHUSING, vSearchMode);
	DDX_DateTimeCtrl(pDX, IDC_DATENEWEST, vDateNewest);
	DDX_DateTimeCtrl(pDX, IDC_DATEOLDEST, vDateOldest);
	DDX_Check(pDX, IDC_CHECKOLDEST, vCheckDateOldest);
	DDX_Check(pDX, IDC_CHECKNEWEST, vCheckDateNewest);
	//}}AFX_DATA_MAP
  }

BEGIN_MESSAGE_MAP(CFindDlg, CDialog)
//{{AFX_MSG_MAP(CFindDlg)
ON_WM_DESTROY()
ON_BN_CLICKED(IDC_STOPFIND, OnStopfind)
ON_BN_CLICKED(IDC_FINDNOW, OnFindnow)
ON_WM_SETCURSOR()
ON_NOTIFY(LVN_ITEMCHANGED, IDC_LIST, OnItemchangedList)
ON_WM_ACTIVATE()
ON_NOTIFY(NM_DBLCLK, IDC_LIST, OnDblclkList)
ON_BN_CLICKED(IDC_ALLGROUPS, OnAllgroups)
ON_NOTIFY(LVN_COLUMNCLICK, IDC_LIST, OnColumnclickList)
ON_BN_CLICKED(IDC_NEWSEARCH, OnNewsearch)
ON_BN_CLICKED(IDC_SEARCHWIZARD, OnSearchwizard)
ON_BN_CLICKED(IDC_BOOLSEARCH, OnBoolsearch)
ON_EN_CHANGE(IDC_FROM, OnChangeCriteria)
ON_BN_CLICKED(IDC_HASATTCHM, OnHasattchm)
ON_MESSAGE(NRM_FILLLIST, OnFillList)
ON_WM_TIMER()
ON_WM_GETMINMAXINFO()
ON_WM_SIZE()
ON_MESSAGE(NRFN_RESETLIST,OnResetList)
	ON_BN_CLICKED(IDC_OPTIONS, OnOptions)
	ON_CBN_SELCHANGE(IDC_SEARCHUSING, OnSelchangeSearchusing)
ON_MESSAGE(NRM_SHOWWAITDLG, ShowWaitMsg)
ON_MESSAGE(NRFN_UPDATEBYHLDA , UpdateByHLDA)
	ON_BN_CLICKED(IDC_CHECKNEWEST, OnChecknewest)
	ON_BN_CLICKED(IDC_CHECKOLDEST, OnCheckoldest)
ON_EN_CHANGE(IDC_SUBJECT, OnChangeCriteria)
ON_EN_CHANGE(IDC_MESSAGE, OnChangeCriteria)
ON_EN_CHANGE(IDC_DATEFROM, OnChangeCriteria)
ON_EN_CHANGE(IDC_DATETO, OnChangeCriteria)
	ON_WM_CONTEXTMENU()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CFindDlg message handlers

void CFindDlg::Open()
  {
  if (m_hWnd) 
	{
	ShowWindow(SW_RESTORE);
    ShowWindow(SW_SHOW);
	return;
	}
  Create(IDD,NULL);
  }

void CFindDlg::OnOK()
  {
  
  }

void CFindDlg::OnCancel()
  {  
  OnStopfind();
  CWindowPlacement wp;
  wp.Retrieve(*this);
  wp.Store(SECTION);
  DestroyWindow();
  }

BOOL CFindDlg::OnInitDialog() 
  {
  CDialog::OnInitDialog();
  CString s;
  

  CRect rc;
  GetWindowRect(&rc);
  _minsize=rc.Size();
  GetClientRect(&rc);
  _lastsize=rc.Size();

  CenterWindow();
  s.LoadString(IDS_TEXTSUBJECT);
  list.InsertColumn(0,s,LVCFMT_LEFT,200,0);
  s.LoadString(IDS_TEXTFROM);
  list.InsertColumn(1,s,LVCFMT_CENTER,100,1);
  s.LoadString(IDS_TEXTDATE);
  list.InsertColumn(2,s,LVCFMT_CENTER,120,2);
  s.LoadString(IDS_TEXTGROUP);
  list.InsertColumn(3,s,LVCFMT_LEFT,200,3);
  
  s.LoadString(IDS_FIND_SCORE);
  list.InsertColumn(4,s,LVCFMT_LEFT,100,4);
  
  
  fndlist=NULL;	
  ListView_SetExtendedListViewStyleEx(list,LVS_EX_FULLROWSELECT,LVS_EX_FULLROWSELECT);
  forceclose=false;
  DialogRules();
  OnSelchangeSearchusing();
  CWindowPlacement wp;
  wp.Load(SECTION);
  wp.Restore(*this,EWP_both,false);
  _lastsort=MAKELPARAM(_SORTBYRANK,0);
  return TRUE;  // return TRUE unless you set the focus to a control
  // EXCEPTION: OCX Property Pages should return FALSE
  }

static int CALLBACK ListCompareFuncRank(LPARAM lParam1, LPARAM lParam2,LPARAM lParamSort)
  {
  CFindList *ff1=(CFindList *)lParam1;	  
  CFindList *ff2=(CFindList *)lParam2;	  	  
  float diff = ff1->accuracy-ff2->accuracy;
  if (diff<0) return +1;
  if (diff>0) return -1;
  return 0;
  }

void CFindDlg::LoadFindList(CFindList *fl)
  {
  if (searchGrp)
    {    
    if (fl)
      if (fl->grp!=searchGrp)
        {
        CNewsArtHolder *ptr;
        CNewsArtHolder *top=NULL;
        while (fl)
		{
		  if (fl->hlda)
          {
          if (!fl->hlda->IsTrash() && fl->accuracy>=ext_minScore)
            {            
            if (top==NULL) top=ptr=new CNewsArtHolder(0);
            else 
              {
              ptr->SetNext(new CNewsArtHolder(0));
              ptr=ptr->GetNext();
              }
            ptr->subject=fl->hlda->subject;
            ptr->date=fl->hlda->date;
            ptr->from=fl->hlda->from;
            ptr->messid=fl->hlda->messid;
            ptr->lines=fl->hlda->lines;
            ptr->state=fl->hlda->state;
            ptr->refid=fl->hlda->refid;
            ptr->attachment=fl->hlda->attachment;
  /*          ptr->SetChild(fl->hlda->GetChild());
            if (ptr->GetChild()) 
              ptr->GetChild()->AddRef();*/
            ptr->MarkAsRead(0,!fl->hlda->GetUnread());
            ptr->CalcHierarchy2(fl->grp,NULL,0,TRUE);
            ptr->newestdate=fl->hlda->date;            
            }
		  }
          CFindList *a=fl->next;
          fl->next=NULL;
          delete fl;
          fl=a;        
          }
        if (top)
          {
          int cnt=0;
          if (searchGrp->GetList()==NULL)
            searchGrp->SetList(top);
          else
            {
            CNewsArtHolder *p=searchGrp->GetList();
            while (p->GetNext()!=NULL) {cnt++;p=p->GetNext();}
            p->SetNext(top);
            }
          if (cnt>5000)
            {
            stop=true; 
            AfxMessageBox(IDS_TOMANYRESULTS,MB_ICONSTOP);
            }
          _needresort=true;
          }
        }
      else
        delete fl;
      
    }
  else
    {
    //  list.DeleteAllItems();
    CFindList *q=fl;
    CString b;
    CString ss;
    DWORD ntm=GetTickCount();
    bool hide=false;
    int count = list.GetItemCount();
    if (count>10000) 
	  {
      delete fl;
      stop=true; //stop searching
      return;
	  }
    while (q)
	  {
      CFindList *z;
      LVITEM it;
    
      // search a suitable position based on accuracy
      float acc = q->accuracy;
      if (acc<0) acc = 0;
      if (acc>1) acc = 1;
      ss.Format("%.0f %%",acc*100);
    
      /*
	  int pos = count;
	  for (int i=0; i<count; i++)
	  {
	  CString text = list.GetItemText(i,4);
	  //float score = atof(text);
	  if (text<ss)
	  {
	  pos = i;
	  break;
	  }
	  }*/
      if (q->accuracy*100>=ft_minScore)
        {
        it.iItem=count;
        it.iSubItem=0;
        it.mask=TVIF_PARAM|TVIF_TEXT;
        it.lParam=(DWORD)q;
        CString s=q->hlda?q->hlda->GetSubject():q->msgid;
        it.pszText=(char *)(LPCTSTR)s;
        int a=list.InsertItem(&it);
        if (q->hlda) GetNameFrom(q->hlda->from,b);
        else b="<unknown>";
        list.SetItemText(a,1,b);
        if (q->hlda) list.SetItemText(a,2,(q->hlda->date+theApp.timezone).Format());
        if (q->grp) list.SetItemText(a,3,q->grp->GetName());
        list.SetItemText(a,4,ss);
        }
      z=q->next;
      q->next=NULL;
      q=z;	
      if (!hide && GetTickCount()-ntm>1000)
	    {
        list.ShowWindow(SW_HIDE);
        hide=true;
	    }   
      count++;
      if (count>10000)
	    {
        AfxMessageBox(IDS_TOMANYRESULTS,MB_ICONSTOP);
        delete q;
        stop=true;
        return;
	    }
	  }
    if (hide) list.ShowWindow(SW_SHOW);
    _needresort=true;
    }
  }

CFindDlg::~CFindDlg()
  {
  delete fndlist;
  }

void CFindDlg::OnDestroy() 
  {
  OnStopfind();
  ResetList();
  CDialog::OnDestroy();
  
  delete fndlist;
  fndlist=NULL;
  SaveSettings();
  }

void CFindDlg::OnStopfind() 
  {
  if (findthrd)
	{
    UINT state;
opak:;
    stop=true;
    SetCursor(LoadCursor(NULL,IDC_WAIT));
    do
	  {
      state=MsgWaitForMultipleObjectsEx(1,&findthrd->m_hThread,10000,QS_SENDMESSAGE,0);
      if (state==WAIT_OBJECT_0+1)
		{
        MSG msg;
        while (PeekMessage(&msg,0,0,0,PM_REMOVE | QS_SENDMESSAGE<<16)) DispatchMessage(&msg);
		}
	  }
	  while (state==WAIT_OBJECT_0+1);
	  if (state==WAIT_TIMEOUT)  
		{
        if (AfxMessageBox(IDS_FINDDONTSTOP,MB_RETRYCANCEL|MB_ICONEXCLAMATION)==IDRETRY)        
          goto opak;
        else
          return;
		}
      SearchMode(false);
	  delete findthrd;
	  findthrd=NULL;
	  KillTimer(_repeatSortTimer);
	  //	SortList();
	}
  }

static UINT FindNowThread(LPVOID data)
  {
#ifndef _AFXDLL	
  AFX_MODULE_THREAD_STATE *state=AfxGetModuleThreadState();
  if (state->m_pmapSocketHandle==NULL)
	{
    state->m_pmapSocketHandle=new CMapPtrToPtr();
    state->m_pmapDeadSockets=new CMapPtrToPtr();
    state->m_plistSocketNotifications=new CPtrList();
	}
#endif
  CFindDlg *dlg=(CFindDlg *)data;
  dlg->NewFindThread();
  return 0;
  }

void CFindDlg::OnFindnow() 
  {  
  resInGrp=FALSE;
  if (UpdateData(TRUE)==FALSE) return;
  OnStopfind();
  vDateNewest.SetDateTime(vDateNewest.GetYear(),vDateNewest.GetMonth(),vDateNewest.GetDay(),23,59,59);
  vDateOldest.SetDateTime(vDateOldest.GetYear(),vDateOldest.GetMonth(),vDateOldest.GetDay(),0,0,0);
  if (vDateOldest>vDateNewest)
  {
	  COleDateTime tmp;
	  tmp=vDateOldest;
	  vDateOldest=vDateNewest;
	  vDateNewest=tmp;
	  UpdateData(FALSE);
  }
  _repeatSortTimer=SetTimer(1000,250,NULL);
  findthrd=AfxBeginThread((AFX_THREADPROC)FindNowThread,this,THREAD_PRIORITY_NORMAL,0,CREATE_SUSPENDED);
  findthrd->m_bAutoDelete=FALSE;
  findthrd->ResumeThread();
  }

void CFindDlg::FindExternalCall()
  {
  OnStopfind();
  _repeatSortTimer=SetTimer(1000,250,NULL);
  findthrd=AfxBeginThread((AFX_THREADPROC)FindNowThread,this,THREAD_PRIORITY_NORMAL,0,CREATE_SUSPENDED);
  findthrd->m_bAutoDelete=FALSE;
  findthrd->ResumeThread();
  }

BOOL CFindDlg::OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message) 
  {
  // TODO: Add your message handler code here and/or call default
  SetTooltip(this,pWnd);
  /*	if (bstop.IsWindowEnabled() && pWnd!=&bstop) 
  {
  SetCursor(::LoadCursor(NULL,IDC_WAIT));
  return TRUE;
  }*/
  return CDialog::OnSetCursor(pWnd, nHitTest, message);
  }

void CFindDlg::OnItemchangedList(NMHDR* pNMHDR, LRESULT* pResult) 
  {
  NMLISTVIEW* pNMListView = (NMLISTVIEW*)pNMHDR;
  if (pNMListView->uNewState & LVIS_FOCUSED)
	{
    CFindList *ff=(CFindList *)pNMListView->lParam;
    ::UpdateWindow(pNMListView->hdr.hwndFrom);
    SetCursor(::LoadCursor(NULL,IDC_WAIT));
    theApp.m_pMainWnd->SendMessage(NRM_SELECTITEM,0,(LPARAM)ff);
	}
  
  *pResult = 0;
  }

void CFindDlg::OnActivate(UINT nState, CWnd* pWndOther, BOOL bMinimized) 
  {
  CDialog::OnActivate(nState, pWndOther, bMinimized);
  if (nState==WA_INACTIVE)
	{
    int p=list.GetNextItem(-1,LVIS_FOCUSED);
    list.SetItemState(p,0,LVIS_FOCUSED);
	}
  else 
  {
    if (wSearchUsing.GetSafeHwnd())  OnSelchangeSearchusing();
  }
  }

void CFindDlg::OnDblclkList(NMHDR* pNMHDR, LRESULT* pResult) 
  {
  OnCancel();  	
  *pResult = 0;
  }

static int CALLBACK ListCompareFunctFinal(LPARAM lParam1, LPARAM lParam2, LPARAM item)
  {
  int sortitem=LOWORD(item);
  int order=HIWORD(item);
  CFindList *ff1=(CFindList *)lParam1;	  
  CFindList *ff2=(CFindList *)lParam2;	  	  
  if (sortitem!=_SORTBYRANK && (ff1->hlda==NULL || ff2->hlda==NULL)) return (ff1->hlda<ff2->hlda)-(ff1->hlda>ff2->hlda);
  char res=0;
  switch (sortitem)
    {
    case _SORTBYTOPIC: res=stricmp(ff1->hlda->GetSubject(),ff2->hlda->GetSubject());break;
    case _SORTBYUSER:  res=stricmp(ff1->hlda->from,ff2->hlda->from);break;
    case _SORTBYDATE:  res=ff1->hlda->CompareItems(ff1->hlda,ff2->hlda,16);break;
    case _SORTBYGROUP: res=stricmp(ff1->grp?ff1->grp->GetName():"",ff2->grp?ff2->grp->GetName():"");break;
    case _SORTBYRANK:  res=(ff1->accuracy<ff2->accuracy)-(ff1->accuracy>ff2->accuracy);
    }  
  if (order) res=-res;
  if (res==0) 
    res=(ff1->accuracy<ff2->accuracy)-(ff1->accuracy>ff2->accuracy); 
  if (res==0)
    if (ff1->hlda!=NULL && ff2->hlda!=NULL)
      res=ff1->hlda->CompareItems(ff1->hlda,ff2->hlda,16);
    else
      res=(ff1->hlda<ff2->hlda)-(ff1->hlda>ff2->hlda);
  return res;
  }

void CFindDlg::SortList()
  {
  CWaitCursor wait;
  if (_needresort)
    {
    if (searchGrp) 
      {    
      if (msglist->GetCurGroup()==searchGrp) 
        {
        searchGrp->SortItems(-1);
        msglist->UpdateList();
        }
      }
    list.SortItems(ListCompareFunctFinal,_lastsort);
    }
  _needresort=false;
  }

void CFindDlg::ResetList()
  {
  int cnt=list.GetItemCount();
  for (int i=0;i<cnt;i++)
	{
    CFindList *ff=(CFindList *)list.GetItemData(i);
    delete ff;	
	}
  list.DeleteAllItems();
  // 	list.SortItems(ListCompareFuncRank,0);
  }


void CFindDlg::DialogRules()
  {
  int p=GetDlgItem(IDC_FROM)->GetWindowTextLength()+
    GetDlgItem(IDC_SUBJECT)->GetWindowTextLength()+
	GetDlgItem(IDC_MESSAGE)->GetWindowTextLength()+
	IsDlgButtonChecked(IDC_CHECKNEWEST)+
	IsDlgButtonChecked(IDC_CHECKOLDEST);
  GetDlgItem(IDC_FINDNOW)->EnableWindow(p!=0 || IsDlgButtonChecked(IDC_HASATTCHM)!=0);
  OnChecknewest();
  OnCheckoldest();
  }

void CFindDlg::OnAllgroups() 
  {	
  DialogRules();	
  }
static DWORD lastmsg=0;
static bool FindHook(GRPFINDINFO& fnd)
  {
  //  MSG msg;
  CFindDlg *dlg=(CFindDlg *)fnd.context;    
  dlg->SendMessage(NRM_FILLLIST,0,(LPARAM)fnd.firstfnd);
  //  dlg->LoadFindList(fnd.firstfnd);
  if (lastmsg+100<GetTickCount())
    {
    char buff2[256],buff3[256];
    dlg->GetDlgItemText(IDC_TOOLTIP,buff3,sizeof(buff3)); 
    if (fnd.grp!=NULL)
	  {
	  sprintf(buff2,dlg->fndstatbuf,fnd.grp->GetName(),fnd.msgindex);
      if (strncmp(buff2,buff3,sizeof(buff2))) 
        {
        dlg->SetDlgItemText(IDC_TOOLTIP,buff2);
        if (!dlg->IsWindowVisible())
          DogShowStatusV(buff2);
        }
	  }
    lastmsg=GetTickCount();
    }
  fnd.firstfnd=NULL;
  return !dlg->stop;
  }

void CFindDlg::DoFindRecurse(HTREEITEM it,GRPFINDINFO &grpinfo,CNewsTerminal &term)
  {  
  if (acclist->GetItemType(it)==Itm_Group)
	{
    CNewsGroup *grp=acclist->GetGroup(it);
    grpinfo.msgindex=0;
    if  (grp)  grp->FindInGroup(grpinfo,term);	  
	}
  HTREEITEM p=lefttree->GetNextItem(it,TVGN_CHILD);
  if (p)
	{
    while (p && !stop) 
	  {
      DoFindRecurse(p,grpinfo,term);
      p=lefttree->GetNextItem(p,TVGN_NEXT);
	  }	
	}
  }

static ITextFind *NewFind(const GRPFINDINFO &fnd, CString text)
  {
  return NewFind(fnd.substrings,fnd.booleanSearch,text);
  }

void CFindDlg::NewFindThread()
  {
  
  if (vSearchMode==2 && message.GetLength()) //FULLTEXT
  {
    FindFulltextThread();
    return;
  }

  GRPFINDINFO fnd;
  
  fnd.FndCallback=FindHook;
  HTREEITEM it=lefttree->GetSelectedItem();
  ItemType tt=acclist->GetItemType(it);
  fnd.en_after=vCheckDateNewest!=FALSE;
  fnd.after=vDateNewest;
  fnd.en_before=vCheckDateOldest!=FALSE;
  fnd.before=vDateOldest;
  fnd.booleanSearch = vSearchMode==0;
  fnd.substrings = substrings!=FALSE;
  fnd.maxAcceptableMatch = 0.3f;  
  
  if (!from.IsEmpty())
	{
    fnd.from = NewFind(fnd,from);
    if (!fnd.from)
	  {NrMessageBox(IDS_FINDCRITERIAERROR);GetDlgItem(IDC_FROM)->SetFocus();return;}
	}
  else
	{
    fnd.from = NULL;
	}
  
  if (!subject.IsEmpty())
	{
    fnd.subject = NewFind(fnd,subject);
    if (!fnd.subject)
	  {NrMessageBox(IDS_FINDCRITERIAERROR);GetDlgItem(IDC_SUBJECT)->SetFocus();return;}
	}
  else
	{
    fnd.subject = NULL;
	}
  
  if (!message.IsEmpty())
	{
    fnd.message = NewFind(fnd,message);
    if (!fnd.message)
	  {NrMessageBox(IDS_FINDCRITERIAERROR);GetDlgItem(IDC_MESSAGE)->SetFocus();return;}
	}
  else
	{
    fnd.message = NULL;
	}
  
  CNewsAccount *acc=acclist->GetAccountEx(it);
  if (acc==NULL) return;
  if (acclist->EnsureLoaded(acc)==false) return;
  searchGrp=resInGrp?CreateSearchGrp(acc):NULL;
  fnd.cache=acc->offline?&(acc->cache):NULL;
  fnd.attach=attch!=FALSE;
  SetCursor(::LoadCursor(NULL,IDC_WAIT));
  SearchMode(true);
  CNewsTerminal term;
  if (!theApp.offline && connserver)
    if (term.ShowError(acc->OpenConnection(term))!=0) goto end;
	delete fndlist;
	fndlist=NULL;
	stop=false;
	fnd.context=this;
	fnd.grp=NULL;
	LoadString(theApp.m_hInstance,IDS_LOOKINGIN,fndstatbuf,sizeof(fndstatbuf));
	SendMessage(NRFN_RESETLIST,0,0);
	fnd.firstfnd=NULL;  
	if (allgroups)
	  {
	  CNewsGroup *grp=acc->GetGroups();
	  while (grp && !stop)
		{
		fnd.msgindex=0;
		if (grp->sign) grp->FindInGroup(fnd,term);
		grp=grp->GetNext();
		}
	  }
	else  DoFindRecurse(it,fnd,term);
	if (term.IsConnected()) term.Quit();
	//LoadFindList();
	FindHook(fnd);
end:
    SearchMode(false);
	SendMessage(WM_TIMER,_repeatSortTimer,0);
	if (forceclose) PostMessage(WM_CLOSE,0,0);
 
  }
void CFindDlg::OnColumnclickList(NMHDR* pNMHDR, LRESULT* pResult) 
  {
  NMLISTVIEW* pNMListView = (NMLISTVIEW*)pNMHDR;
  if (LOWORD(_lastsort)==pNMListView->iSubItem)     
    _lastsort=MAKELPARAM(LOWORD(_lastsort),1-HIWORD(_lastsort));
  else    
    _lastsort=pNMListView->iSubItem;
  _needresort=true;
  SortList();
  *pResult = 0;
  }

void CFindDlg::OnNewsearch() 
  {
  delete fndlist;
  fndlist=NULL;
  ResetList();
  from="";
  subject="";
  message="";
  vCheckDateOldest=false;
  vCheckDateNewest=false;
  UpdateData(FALSE);
  OnCheckoldest();
  OnChecknewest();
  }

void CFindDlg::OnSearchwizard() 
  {
  // TODO: Add your control notification handler code here
  
  }

void CFindDlg::OnSmartwords() 
  {
  // TODO: Add your control notification handler code here
  
  }

void CFindDlg::OnBoolsearch() 
  {
  // TODO: Add your control notification handler code here
  // substrings are always active with boolean search
  if (IsDlgButtonChecked(IDC_BOOLSEARCH))
	{
    //EnableControl();
    CWnd *wnd = GetDlgItem(IDC_SUBSTRINGS);
    wnd->EnableWindow(false);
    //CheckDlgButton(IDC_SUBSTRINGS,1);
	}
  else
	{
    //CheckDlgButton(IDC_SUBSTRINGS,0);
    CWnd *wnd = GetDlgItem(IDC_SUBSTRINGS);
    wnd->EnableWindow(true);
	}
  
  }

void CFindDlg::OnChangeCriteria() 
  {
  // TODO: If this is a RICHEDIT control, the control will not
  // send this notification unless you override the CDialog::OnInitDialog()
  // function and call CRichEditCtrl().SetEventMask()
  // with the ENM_CHANGE flag ORed into the mask.
  
  // TODO: Add your control notification handler code here
  DialogRules();	
  }

void CFindDlg::OnHasattchm() 
  {
  DialogRules();	
  }

LRESULT CFindDlg::OnFillList(WPARAM wParam, LPARAM lParam)
  {
  LoadFindList((CFindList *)lParam);  
  return 0;
  }

void CFindDlg::OnTimer(UINT nIDEvent) 
  {
  if (nIDEvent==_repeatSortTimer)
	{
    SortList();      
    if (WaitForSingleObject(findthrd->m_hThread,0)==WAIT_OBJECT_0)
	  {
      KillTimer(_repeatSortTimer);
	  }
	}
  
  CDialog::OnTimer(nIDEvent);
  }

BOOL CFindDlg::PreTranslateMessage(MSG* pMsg) 
  {
  if (pMsg->message==WM_KEYDOWN && pMsg->wParam=='C' && GetKeyState(VK_CONTROL) & 0x80)
	{
    int p=list.GetNextItem(-1,LVNI_FOCUSED);
    if (p!=-1)
	  {
      CFindList *flist=(CFindList *)list.GetItemData(p);
      CMessageList::CopyShortcut(flist->hlda);
	  }
    return TRUE;
	}
  return CDialog::PreTranslateMessage(pMsg);
  }

void CFindDlg::OnGetMinMaxInfo(MINMAXINFO FAR* lpMMI) 
  {
  // TODO: Add your message handler code here and/or call default	
  CDialog::OnGetMinMaxInfo(lpMMI);
  lpMMI->ptMinTrackSize.x=_minsize.cx;
  lpMMI->ptMinTrackSize.y=_minsize.cy;
  
  }

void CFindDlg::OnSize(UINT nType, int cx, int cy) 
  {
  CDialog::OnSize(nType, cx, cy);
  if (nType==SIZE_MINIMIZED) return;
  if ((HWND)bfind==NULL) return;
  int rszx=cx-_lastsize.cx;
  int rszy=cy-_lastsize.cy;
  ::RelMoveWnd(&bnewsearch,rszx,0);
  ::RelMoveWnd(&bstop,rszx,0);
  ::RelMoveWnd(&bfind,rszx,0);
  ::RelSizeWnd(&list,rszx,rszy);
  ::RelMoveWnd(GetDlgItem(IDC_TOOLTIP),0,rszy);
  ::RelSizeWnd(GetDlgItem(IDC_TOOLTIP),rszx,0);
  ::RelSizeWnd(GetDlgItem(IDC_FROM),rszx,0);
  ::RelSizeWnd(GetDlgItem(IDC_SUBJECT),rszx,0);
  ::RelSizeWnd(GetDlgItem(IDC_MESSAGE),rszx,0);
  _lastsize.cx=cx;
  _lastsize.cy=cy;
  Invalidate(TRUE);
  // TODO: Add your message handler code here
  
  }

CNewsGroup *CFindDlg::CreateSearchGrp(CNewsAccount *acc)
  {  
  CNewsGroup *cur=acc->GetGroups();
  if (cur==NULL) return NULL;
  while (cur && strcmp(cur->GetName(),"Search")) cur=cur->GetNext();
  if (cur==NULL)
    {
    cur=new CNewsGroup("Search",0,0x7FFFFFFF);
    CNewsGroup *last=acc->GetGroups();
    while (last->GetNext()) last=last->GetNext();
    last->SetNext(cur);    
//    cur->SortItems(4);
    }
  cur->display=true;
  cur->LoadToTree(*lefttree,acc->treeitem,acc->viewastree);
  cur->deleted=true;
  cur->special=true;
  msglist->LoadList(acc,cur,false);
  return cur;
  }

LRESULT CFindDlg::OnResetList(WPARAM wParam, LPARAM lParam)
  {
  if (searchGrp)
    {
    CNewsAccount *acc=msglist->GetCurAccount();
    CNewsGroup *grp=msglist->GetCurGroup();
    if (grp==searchGrp) msglist->LoadList(NULL,NULL,false);
    if (searchGrp->GetList()) searchGrp->GetList()->Release();
    searchGrp->SetList(NULL);
    if (grp==searchGrp) msglist->LoadList(acc,grp,false);
    memcache.FlushCache();
    }
  else
    ResetList();
  return 0;
  }

bool CFindDlg::IsBusy()
  {
  if (GetSafeHwnd())
    {
    if (bstop.IsWindowEnabled()) return true;
    }
  return false;
  }

void CFindDlg::OnOptions() 
{
  CMenu mnu;
  mnu.LoadMenu(IDR_POPUPS);
  CMenu *findmenu=mnu.GetSubMenu(4);
  int curFind=wSearchUsing.GetCurSel();
  CMenu *optMenu=findmenu->GetSubMenu(curFind);
  CPoint pt(GetMessagePos());
  int cnt=optMenu->GetMenuItemCount();
  for (int i=0;i<cnt;i++)
  {
    int id=optMenu->GetMenuItemID(i);
    switch(id)
    {
      case ID_SEARCHOPT_SIMILAR_MATCHSUBSTRINGS:
        if (substrings) optMenu->CheckMenuItem(i,MF_BYPOSITION|MF_CHECKED);
        break;
      case ID_FULLTEXTSETTINGS_NOMINIMALSCORE:
        if (ft_minScore==0) optMenu->CheckMenuItem(i,MF_BYPOSITION|MF_CHECKED);
        break;
      case ID_FULLTEXTSETTINGS_MINIMALSCORE25:
        if (ft_minScore==25) optMenu->CheckMenuItem(i,MF_BYPOSITION|MF_CHECKED);
        break;
      case ID_FULLTEXTSETTINGS_MINIMALSCORE50:
        if (ft_minScore==50) optMenu->CheckMenuItem(i,MF_BYPOSITION|MF_CHECKED);
        break;
      case ID_FULLTEXTSETTINGS_MINIMALSCORE70:
        if (ft_minScore==70) optMenu->CheckMenuItem(i,MF_BYPOSITION|MF_CHECKED);
        break;
      case ID_FULLTEXTSETTINGS_MINIMALSCORE80:
        if (ft_minScore==80) optMenu->CheckMenuItem(i,MF_BYPOSITION|MF_CHECKED);
        break;
      case ID_FULLTEXTSETTINGS_MINIMALSCORE90:
        if (ft_minScore==90) optMenu->CheckMenuItem(i,MF_BYPOSITION|MF_CHECKED);
        break;
      case ID_FULLTEXTSETTINGS_EXACTPHRASES:
        if (ft_minScore==100) optMenu->CheckMenuItem(i,MF_BYPOSITION|MF_CHECKED);
        break;
      case ID_FULLTEXTSETTINGS_INCLUDEUNKNOWNARTICLES:
        if (ft_unknown) optMenu->CheckMenuItem(i,MF_BYPOSITION|MF_CHECKED);
        break;
    }
  }
  int i=optMenu->TrackPopupMenu(TPM_LEFTBUTTON|TPM_RETURNCMD|TPM_NONOTIFY,pt.x,pt.y,this,NULL);
  switch (i)
  {
    case ID_SEARCHOPT_BOOLEAN_HELP:
        {
        CString text;
        text.LoadString(IDS_NRFINDHELP);
        MessageBox(text,"Help",MB_OK);
        }
        break;
    case ID_SEARCHOPT_SIMILAR_MATCHSUBSTRINGS:
        substrings=!substrings;
        break;
    case ID_FULLTEXTSETTINGS_NOMINIMALSCORE: ft_minScore=0;break;
    case ID_FULLTEXTSETTINGS_MINIMALSCORE25: ft_minScore=25;break;
    case ID_FULLTEXTSETTINGS_MINIMALSCORE50: ft_minScore=50;break;
    case ID_FULLTEXTSETTINGS_MINIMALSCORE70: ft_minScore=70;break;
    case ID_FULLTEXTSETTINGS_MINIMALSCORE80: ft_minScore=80;break;
    case ID_FULLTEXTSETTINGS_MINIMALSCORE90: ft_minScore=90;break;
    case ID_FULLTEXTSETTINGS_EXACTPHRASES: ft_minScore=100;break;
    case ID_FULLTEXTSETTINGS_INCLUDEUNKNOWNARTICLES: ft_unknown=!ft_unknown;break;
  }

}

static UINT prepareFulltext(LPVOID fulltext)
{
  FullTextIndex *idx=(FullTextIndex *)fulltext;
  idx->ForceIndex();
  return 0;
}

void CFindDlg::OnSelchangeSearchusing() 
{
  BOOL nfulltext=wSearchUsing.GetCurSel()!=2;
  GetDlgItem(IDC_CONNECTSERVER)->EnableWindow(nfulltext);
  if (!nfulltext)
  {
    CNewsAccount *acc=acclist->GetAccountEx(lefttree->GetSelectedItem());
	if (acc && acc->fulltext.IsCreated())
	  AfxBeginThread((AFX_THREADPROC)prepareFulltext,&acc->fulltext);
  }
}


LRESULT CFindDlg::ShowWaitMsg(WPARAM wParam, LPARAM lParam)
{
  if (lParam)
  {
	CRect rc;
	list.GetClientRect(&rc);
	CPoint center=rc.CenterPoint();
	CRect nrc(center.x-250,center.y-20,center.x+250,center.y+20);
	CString text;
	text.LoadString(wParam);
	msgwnd.Create("STATIC",text,WS_VISIBLE|SS_CENTER|SS_CENTERIMAGE|WS_BORDER,nrc,&list,0);
	msgwnd.UpdateWindow();
  }
  else
  {
	msgwnd.DestroyWindow();
  }
  return 0;
}

void CFindDlg::FindFulltextThread()
{
  GRPFINDINFO fnd;
  fnd.booleanSearch=false;
  if (!from.IsEmpty())
	{
    fnd.from = NewFind(fnd,from);
    if (!fnd.from)
	  {NrMessageBox(IDS_FINDCRITERIAERROR);GetDlgItem(IDC_FROM)->SetFocus();return;}
	}
  else
	{
    fnd.from = NULL;
	}
  
  if (!subject.IsEmpty())
	{
    fnd.subject = NewFind(fnd,subject);
    if (!fnd.subject)
	  {NrMessageBox(IDS_FINDCRITERIAERROR);GetDlgItem(IDC_SUBJECT)->SetFocus();return;}
	}
  else
	{
    fnd.subject = NULL;
	}
  CNewsGroup *fndgrp=allgroups?NULL:acclist->GetGroup(lefttree->GetSelectedItem());
  fnd.en_before=vCheckDateNewest!=FALSE;
  fnd.before=vDateNewest;
  fnd.en_after=vCheckDateOldest!=FALSE;
  fnd.after=vDateOldest-COleDateTimeSpan(0,23,59,59);

  stop=false;
  SearchMode(true);
  CNewsAccount *acc=acclist->GetAccountEx(lefttree->GetSelectedItem());
  searchGrp=resInGrp?CreateSearchGrp(acc):NULL;
  SendMessage(NRFN_RESETLIST,0,0);
  {
	SendMessage(NRM_SHOWWAITDLG,IDS_WAITBUILDINGINDEX,1);
	acc->FTSetupIgnoredWords();
	FullTextSearch fsch(acc->fulltext);
	SendMessage(NRM_SHOWWAITDLG,0,0);
	SendMessage(NRM_SHOWWAITDLG,IDS_WAITFULLTEXTSEARCH,1);
	CString sstring=message;
	if (attch) sstring+=" +?a";
	fsch.SearchByString(sstring);
	SendMessage(NRM_SHOWWAITDLG,0,0);
    CFindList *finfo=NULL;
	for (int i=-1;(i=fsch.GetNextMsgIDIndex(i,ft_minScore))>=0 && !stop;)
	{
		if (finfo==NULL) finfo=new CFindList;
		finfo->accuracy=fsch.GetScore(i)*0.01;
		finfo->hlda=acc->msgidlist.Find(fsch.GetMessageID(i));        
		if (finfo->hlda)
		{          
          if (finfo->hlda->group!=fndgrp && fndgrp!=NULL) continue;
          if (fnd.en_after && finfo->hlda->date<fnd.after) continue;
          if (fnd.en_before && finfo->hlda->date>fnd.before) continue;
          if (fnd.subject!=NULL) finfo->accuracy*=fnd.subject->GetScore(finfo->hlda->subject);
          if (fnd.from!=NULL) finfo->accuracy*=fnd.from->GetScore(finfo->hlda->from);
          if (finfo->accuracy<ft_minScore*0.01f) continue;
		  finfo->grp=finfo->hlda->group;
		  finfo->hlda->AddRef();
		}
        else if (!this->ft_unknown) continue;
		else 
		{
		  finfo->grp=NULL;
		  finfo->msgid=strdup(fsch.GetMessageID(i));
		}
   	    SendMessage(NRM_FILLLIST,0,(LPARAM)finfo);
        finfo=NULL;
	}
    if (finfo)
    {
      if (finfo->hlda) finfo->hlda->AddRef();
      delete finfo;
    }
  }
  SearchMode(false);
  CNewsTerminal term;
  int i=0;
  if (!theApp.offline)
  {
	SendMessage(WM_TIMER,_repeatSortTimer,0);
	while (!stop && i<list.GetItemCount())
	{
  	  bool next=true;
	  CFindList *finfo=(CFindList *)list.GetItemData(i);
	  if (finfo->hlda==NULL)
	  {
		if (!term.IsConnected())
		{
		  if (acc->OpenConnection(term)!=0) return;
		}
		CNewsArtHolder *hld;
		CNewsArticle art;
		int index=-1;
		term.SendFormatted("HEAD %s",finfo->msgid);
		int p=term.ReceiveNumReply()/10;  
		if (p==22 && art.DownloadArticle(index,term,false)==0) 
		{
		  hld=new CNewsArtHolder;
		  hld->LoadArticle(&art);
		  CFindList flst;
		  flst.hlda=hld;
          flst.accuracy=finfo->accuracy;
		  hld->AddRef();
		  flst.msgid=strdup(finfo->msgid);          
          if (fnd.en_after && hld->date<fnd.after) flst.accuracy=0;
          if (fnd.en_before && hld->date>fnd.before) flst.accuracy=0;
          if (fnd.subject!=NULL) flst.accuracy*=fnd.subject->GetScore(hld->subject);
          if (fnd.from!=NULL) flst.accuracy*=fnd.from->GetScore(hld->from);
          if (flst.accuracy<ft_minScore*0.01f) 
          {
            flst.hlda=NULL;
            hld->Release();
            hld->Release();
		    SendMessage(NRFN_UPDATEBYHLDA,i,(LPARAM)&flst);		  
  	  	    next=false;		  
          }
          else
          {
		    if (SendMessage(NRFN_UPDATEBYHLDA,i,(LPARAM)&flst)==0) 
		    {
			  hld->Release();
		    }
		    else 
            {
			  finfo->hlda=hld;
              finfo->accuracy=flst.accuracy;
            }
            next=false;	
          }
		}
        else
        {
		  CFindList flst;
		  flst.hlda=NULL;
          flst.msgid=strdup(finfo->msgid);
		  SendMessage(NRFN_UPDATEBYHLDA,i,(LPARAM)&flst);		  
		  next=false;		  
        }
	  }
	  if (next) i++;else i=0;
	}
  }
}

LRESULT CFindDlg::UpdateByHLDA(WPARAM wParam, LPARAM lParam)
{
  CFindList *flst=(CFindList *)  lParam;
  CFindList *llst=(CFindList *)  list.GetItemData(wParam);
  if (llst->msgid && strcmp(flst->msgid,llst->msgid)==0)
  {
    if (flst->hlda==NULL)
    {
      delete llst;
      list.DeleteItem(wParam);
    }
    else
    {
	  CString tmp;
	  GetNameFrom(flst->hlda->from,tmp);
	  list.SetItemText(wParam,0,flst->hlda->GetSubject());
	  list.SetItemText(wParam,1,tmp);
	  list.SetItemText(wParam,2,(flst->hlda->date+theApp.timezone).Format());
      tmp.Format("%.0f %%",flst->accuracy*100);
	  list.SetItemText(wParam,4,tmp);
	  _needresort=true;
    }
	return 1;
  }
  return 0;
}
  



void CFindDlg::SearchMode(bool search)
{
	bstop.EnableWindow(search);
	bfind.EnableWindow(!search);
	bnewsearch.EnableWindow(!search);
}

void CFindDlg::SaveSettings()
{
  theApp.WriteProfileInt("FindDlg","MinScore",ft_minScore);
  theApp.WriteProfileInt("FindDlg","Substrings",substrings);
  theApp.WriteProfileInt("FindDlg","UnknownMsg",ft_unknown);
  theApp.WriteProfileInt("FindDlg","AllGroups",allgroups);
  theApp.WriteProfileInt("FindDlg","ConnectServer",connserver);
  theApp.WriteProfileInt("FindDlg","SearchMode",vSearchMode);
}


void CFindDlg::LoadSettings()
{
  ft_minScore=theApp.GetProfileInt("FindDlg","MinScore",25);
  substrings=theApp.GetProfileInt("FindDlg","Substrings",0)!=0;
  ft_unknown=theApp.GetProfileInt("FindDlg","UnknownMsg",1)!=0;
  allgroups=theApp.GetProfileInt("FindDlg","AllGroups",0)!=0;
  connserver=theApp.GetProfileInt("FindDlg","ConnectServer",0)!=0;
  vSearchMode=theApp.GetProfileInt("FindDlg","SearchMode",0);
}

void CFindDlg::OnChecknewest() 
{
  wDateNewest.EnableWindow(IsDlgButtonChecked(IDC_CHECKNEWEST)!=0);
}

void CFindDlg::OnCheckoldest() 
{
  wDateOldest.EnableWindow(IsDlgButtonChecked(IDC_CHECKOLDEST)!=0);
	
}

void CFindDlg::OnContextMenu(CWnd* pWnd, CPoint point) 
{
	if (pWnd==&wDateNewest || pWnd==&wDateOldest)
	{
		CMenu mnu;
		mnu.LoadMenu(IDR_POPUPS);
		CMenu *popup=mnu.GetSubMenu(5);
		CRect rc;
		pWnd->GetWindowRect(&rc);
		int ret=popup->TrackPopupMenu(TPM_LEFTALIGN|TPM_NONOTIFY|TPM_RETURNCMD,rc.left,rc.bottom,this);
		COleDateTimeSpan spn;
		switch (ret)
		{
		case ID_CALENDAR_TODAY:spn=COleDateTimeSpan (0,0,0,0);break;
		case ID_CALENDAR_LASTWEEK:spn=COleDateTimeSpan (7,0,0,0);break;
		case ID_CALENDAR_LAST2WEEKS:spn=COleDateTimeSpan (14,0,0,0);break;
		case ID_CALENDAR_LAST3WEEKS:spn=COleDateTimeSpan (21,0,0,0);break;
		case ID_CALENDAR_LASTMONTH:spn=COleDateTimeSpan (31,0,0,0);break;
		case ID_CALENDAR_LAST3MONTHS:spn=COleDateTimeSpan (93,0,0,0);break;
		case ID_CALENDAR_LASTHALF:spn=COleDateTimeSpan (182,0,0,0);break;
		case ID_CALENDAR_YEAR:spn=COleDateTimeSpan (365,0,0,0);break;
		case ID_CALENDAR_TWOYEARS:spn=COleDateTimeSpan (730,0,0,0);break;
		case ID_CALENDAR_THREEYEARS:spn=COleDateTimeSpan (1096,0,0,0);break;
		case ID_CALENDAR_FROMBEGINNING:spn=COleDateTimeSpan (36500,0,0,0);break;
		default: return;
		}
		COleDateTime curr=COleDateTime::GetCurrentTime()-spn;
		CDateTimeCtrl &date=static_cast<CDateTimeCtrl &>(*pWnd);
		date.SetTime(curr);
	}	
}
