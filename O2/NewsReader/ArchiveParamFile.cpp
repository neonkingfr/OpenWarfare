// ArchiveParamFile.cpp: implementation of the ArchiveParamFile class.
//
//////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include "ArchiveParamFile.h"
#include <El/ParamFile/ParamFile.hpp>
#include <assert.h>

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

class ArchiveParamFileClassStack
  {
  int _alloc;
  int _len;
  ParamClassPtr *_class_stack;
  ParamClassPtr _static_class_stack[32];
public:
  ArchiveParamFileClassStack()
    {
    _class_stack=_static_class_stack;
    _len=0;
    _alloc=32;
    }
  ~ArchiveParamFileClassStack()
    {
    if (_class_stack!=_static_class_stack) delete [] _class_stack;
    }
  void Push(const ParamClassPtr &ptr)
    {
    if (_len==_alloc)
      {
      ParamClassPtr *_new_stack=new ParamClassPtr[_alloc*2];
      for (int i=0;i<_len;i++) _new_stack[i]=_class_stack[i];
      if (_class_stack!=_static_class_stack) delete [] _class_stack;
      _alloc*=2;
      _class_stack=_new_stack;
      }
    _class_stack[_len++]=ptr;
    }

  ParamClassPtr Top()
    {
    assert(_len>0);
    return _class_stack[_len-1];
    }

  void Pop()
    {
    assert(_len>0);
    _class_stack[_len-1]=0;
    _len--;    
    }

  };

ArchiveParamFile::ArchiveParamFile(ParamFile *pfile, bool store):
  _pfile(pfile)
  {
  InitInterface(store);
  _class_stack=new ArchiveParamFileClassStack;
  _class_stack->Push(ParamClassPtr(pfile));
  _err=0;
  curVarName[0]=0;
  _unnamed_index=0;
  _internalBuff=NULL;
  }

 ArchiveParamFile::~ArchiveParamFile()
   {
   delete _class_stack;
   free(_internalBuff);
   }
  
void ArchiveParamFile::SetMode(bool _storemode)
  {
  InitInterface(_storemode);
  Reset();  
  }

bool ArchiveParamFile::OpenClass(const char *name)
  {
  if (_err) return false;
  if (curVarName[0]!=0)
    {
    _class_stack->Pop();    
    char *buff=strcpy((char *)alloca(strlen(curVarName)+20),curVarName);
    curVarName[0]=0;
    DecIndexedName(_class_stack->Top().GetPointer(),buff);
    if (OpenClass(buff)==false) return false;
    }
  ParamClassPtr cur=_class_stack->Top();
  const char *indexedName=GetIndexedName(cur.GetPointer(),name);
  if (IsStoring())    
    {        
    ParamClassPtr newclass=cur->AddClass(indexedName);
    if (newclass.IsNull()) {_err=APF_ADDCLASSFAILED;return false;}
    _class_stack->Push(newclass);
    }
  else
    {
    ParamClassPtr newclass=cur->GetClass(indexedName);
    if (newclass.IsNull()) {_err=APF_CLASSNOTFOUND;return false;}
    _class_stack->Push(newclass);
    }
  curVarName[0]=0;
  return true;
  }

void ArchiveParamFile::CloseClass()
  {
  _class_stack->Pop();
  curVarName[0]=0;
  }

bool ArchiveParamFile::OpenVariable(const char *name)
  {  
  if (_err) return false;
  if (strlen(name)+20>APF_MAXVARIABLENAME) 
    {
    _err=APF_VARIABLEISTOOLONG;
    return false;
    }
  if (curVarName[0]!=0)
    {
    _class_stack->Pop();    
    char *buff=strcpy((char *)alloca(strlen(curVarName)+20),curVarName);
    curVarName[0]=0;
    DecIndexedName(_class_stack->Top().GetPointer(),buff);
    if (OpenClass(buff)==false) return false;
    }  
  _class_stack->Push(_class_stack->Top());    
  strncpy(curVarName,name,APF_MAXVARIABLENAME);
  curVarName[APF_MAXVARIABLENAME]=0;
  strncpy(curIndexedName,GetIndexedName(_class_stack->Top().GetPointer(),name),APF_MAXVARIABLENAME);
  curIndexedName[APF_MAXVARIABLENAME]=0;
  if (!IsStoring()) 
    {    
    ParamEntryPtr ptr=_class_stack->Top()->FindEntry(curIndexedName);
    if (ptr.IsNull()) CloseVariable();
    return ptr.NotNull();
    }
  return true;
  }

void ArchiveParamFile::CloseVariable()
  {
  _class_stack->Pop();    
  curVarName[0]=0;
  }

void ArchiveParamFile::GetCurVarName(char *buff)
  {
  if (curVarName[0]) strcpy(buff,curIndexedName);
  else sprintf(buff,"_unnamed_%08X",_unnamed_index++);
  }

int ArchiveParamFile::OverrideDoExchange(int &p)
  {
  if (_err) return _err;
  ParamClassPtr cur=_class_stack->Top();
  char buff[APF_MAXVARIABLENAME+1];
  GetCurVarName(buff);
  if (IsStoring())
    {
    cur->Add(buff,p);
    }
  else
    {
    ParamEntryPtr ptr=cur->FindEntry(buff);
    if (ptr.IsNull())
      {
      _err=APF_UNABLETOGETVARIABLE; return _err;
      }
    p=(*ptr);
    }
  return _err;
  }

int ArchiveParamFile::OverrideDoExchange(float &p)
  {
  if (_err) return _err;
  ParamClassPtr cur=_class_stack->Top();
  char buff[APF_MAXVARIABLENAME+1];
  GetCurVarName(buff);
  if (IsStoring())
    {
    cur->Add(buff,p);
    }
  else
    {
    ParamEntryPtr ptr=cur->FindEntry(buff);
    if (ptr.IsNull())
      {
      _err=APF_UNABLETOGETVARIABLE; return _err;
      }
    p=(*ptr);
    
    }
  return _err;
  }

static RString QuoteMagic(const char *text)
  {
  RString out;
  char buff[20];
  const unsigned char *c=(const unsigned char *)text;
  while (*c)
    {
     switch (*c)
        {
        case 10:out=out+"\\n";break;
        case 13:out=out+"\\r";break;
        case 9:out=out+"\\t";break;
        case '"':sprintf(buff,"\\x%02X",*c);out=out+buff;break;
        case '\'':out=out+"\\'";break;
        case '\\':out=out+"\\\\";break;
        default:
          if (*c>=32) {buff[0]=*c;buff[1]=0;out=out+buff;}
          else
            {sprintf(buff,"\\x%02X",*c);out=out+buff;}
        break;    
        }
      c++;
    }
  return out;
  }

static RString StripMagic(const char *text)
  {
  RString out;
  const char *c=text;
  int val;
  while (*c)
    {    
    if (*c=='\\')
      {
      c++;
      switch (*c++)
        {
        case 0: c--;break;
        case 'x': 
          sscanf(c,"%2X", &val);
                {
                char p[2];
                p[0]=val;
                p[1]=0;
                out=out+p;
                c++;
                c++;
                }

                  
        case '\'': out=out+"'";break;
        case 'n': out=out+"\n";break;
        case 'r': out=out+"\r";break;
        case 't': out=out+"\t";break;
        case '\\': out=out+"\\";break;
        }
      }
    else
      {
      char p[2];
      p[0]=*c;
      p[1]=0;
      out=out+p;
      c++;
      }
    }
  return out;
  }

int ArchiveParamFile::OverrideDoExchange(char *& str)
  {
  if (_err) return _err;
  ParamClassPtr cur=_class_stack->Top();
  char buff[APF_MAXVARIABLENAME+1];
  GetCurVarName(buff);
  if (IsStoring())
    {
    cur->Add(buff,QuoteMagic(str));
    }
  else
    {
    ParamEntryPtr ptr=cur->FindEntry(buff);
    if (ptr.IsNull())
      {
      _err=APF_UNABLETOGETVARIABLE; return _err;
      }
    RStringB rs=StripMagic(ptr->GetValue());
    delete [] str;
    str=new char[rs.GetLength()+1];
    strcpy(str,rs);
    }
  return _err;
  }

int ArchiveParamFile::OverrideDoBinaryExchange(void *ptr, unsigned long size)
  {
  if (_err) return _err;
  ParamClassPtr cur=_class_stack->Top();
  char buff[APF_MAXVARIABLENAME+1];
  GetCurVarName(buff);
  if (IsStoring())
    {    
    char *temp;
    if (size<1024) temp=(char *)alloca(size*2+20); else temp=new char[size*2+20];
    strcpy(temp,"BIN:");
    char *c=temp+4;
    for (int i=0;i<size;i++) sprintf(c+i*2,"%02X",*((unsigned char *)ptr+i));    
    cur->Add(buff,RStringB(temp));
    if (size>=1024) delete [] temp;
    }
  else
    {
    ParamEntryPtr eptr=cur->FindEntry(buff);
    if (eptr.IsNull())
      {
      _err=APF_UNABLETOGETVARIABLE; return _err;
      }
    RStringB rs=StripMagic(eptr->GetValue());
    const char *c=rs;
    if (strncmp(rs,"BIN:",4))
      {
      _err=APF_UNABLETOGETVARIABLE; return _err;
      }
    c+=4;
    for (int i=0;i<size && c[0] && c[1];i++) 
      {
      int byte;
      sscanf(c,"%2X",&byte);
      *((unsigned char *)ptr+i)=(unsigned char)byte;
      c+=2;
      }
    }
  return _err;
  }

int ArchiveParamFile::OverrideDoExchange(short *& str)
  {
  if (_err) return _err;
  ParamClassPtr cur=_class_stack->Top();
  char buff[APF_MAXVARIABLENAME+1];
  GetCurVarName(buff);
  if (IsStoring())
    {
    ParamEntryPtr ptr=cur->AddArray(buff);
    while (*str)
      {
      ptr->AddArrayValue()->SetValue(*str++);
      }
    }
  else
    {
    ParamEntryPtr ptr=cur->FindEntry(buff);
    if (ptr.IsNull())
      {
      _err=APF_UNABLETOGETVARIABLE; return _err;
      }
    int cnt=0;
    while ((*ptr)[cnt].IsIntValue()) cnt++;
    delete [] str;
    str=new short[cnt];
    for (int i=0;i<cnt;i++) str[i]=(int)((*ptr)[cnt]);
    }
  return _err;
  }

int ArchiveParamFile::OverrideDoExchange(unsigned short *& str)
  {
  if (_err) return _err;
  ParamClassPtr cur=_class_stack->Top();
  char buff[APF_MAXVARIABLENAME+1];
  GetCurVarName(buff);
  if (IsStoring())
    {
    ParamEntryPtr ptr=cur->AddArray(buff);
    while (*str)
      {
      ptr->AddArrayValue()->SetValue(*str++);
      }
    }
  else
    {
    ParamEntryPtr ptr=cur->FindEntry(buff);
    if (ptr.IsNull())
      {
      _err=APF_UNABLETOGETVARIABLE; return _err;
      }
    int cnt=0;
    while ((*ptr)[cnt].IsIntValue()) cnt++;
    delete [] str;
    str=new unsigned short[cnt];
    for (int i=0;i<cnt;i++) str[i]=(int)((*ptr)[cnt]);
    }
  return _err;
  }

bool ArchiveParamFile::Check(const char *text)
  {
  if (IsStoring())
    {
    char *z=(char *)text;
    *this->RValue("_CheckMark",z);
    }
  else
    {
    char *z=NULL;
    (*this)("_CheckMark",z);
    if (!(*this)) return false;
    bool ok=strcmp(z,text)!=0;
    delete [] z;
    return ok;
    }
  return !(!(*this));
  }

bool ArchiveParamFile::OpenSection(const char *name, int type)
  {
  if (_err) return false;
  if (type==ARCHIVESECTION_VARIABLE)
    {
    return OpenVariable(name) || IsStoring();
    }
  else
    {
    return OpenClass(name) || IsStoring();   
    }
  }

void ArchiveParamFile::CloseSection(const char *name, int type)
  {
  if (type==ARCHIVESECTION_VARIABLE)
    {
    CloseVariable();
    }
  else
    {
    CloseClass();
    }
  }

bool ArchiveParamFile::OverrideExchangeVersion(int &val)
  {
  (*this)("_Version",val);
  return _err==0;
  }


const char *ArchiveParamFile::GetIndexedName(ParamEntry *level, const char *srcname)
  {
  ParamEntryPtr ptr=level->FindEntry(srcname);  
  if (ptr.IsNull()) return srcname;
  _APFAutoIndex aindex,*fnd;
  aindex.entryptr=ptr.GetPointer();
  aindex.index=1+IsStoring();
  fnd=_autoIndex.FindKey(aindex);
  if (fnd==NULL)
    fnd=_autoIndex.AddKey(aindex);
  if (fnd->index<2) 
    {
    fnd->index++;
    return srcname;
    }
  int cursz=(_internalBuff==0)?0:(_msize(_internalBuff));
  int needsz=strlen(srcname)+16;
  if (needsz>cursz) 
    {
    char *hlp=(char *)realloc(_internalBuff,needsz);
    if (hlp==NULL) 
      {
      _err=-1;
      return srcname;
      }
    _internalBuff=hlp;
    }
  sprintf(_internalBuff,"%s_%d",srcname,fnd->index);
  fnd->index++;
  return _internalBuff;
  }


void ArchiveParamFile::DecIndexedName(ParamEntry *level, const char *srcname)
  {
  ParamEntryPtr ptr=level->FindEntry(srcname);
  if (ptr.IsNull()) return;
  _APFAutoIndex aindex,*fnd;
  aindex.entryptr=ptr.GetPointer();
  fnd=_autoIndex.FindKey(aindex);
  if (fnd==NULL) return;
  assert(fnd->index>=1+IsStoring());
  fnd->index--;
  }
