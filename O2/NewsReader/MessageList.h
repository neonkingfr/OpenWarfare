#if !defined(AFX_MESSAGELIST_H__9FFE4D86_7B00_448F_A8A6_E5B6EE55C9FA__INCLUDED_)
#define AFX_MESSAGELIST_H__9FFE4D86_7B00_448F_A8A6_E5B6EE55C9FA__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// MessageList.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CMessageList window


#define MAXQSEARCHWAITTIME 10000 //10sec

#include "NewsTerminal.h"
#include "NewsArticle.h"

class CNewsGroup;
class CNewsAccount;
class CWebBrowser2;

#include "WebBrowser2.h"
#include "HTMLDocument2.h"
#include "HTMLElement.h"

#define SORTMODE_GROUP 0
#define SORTMODE_ALL 1
#define SORTMODE_ASK -1

class CNRNotify;
class CMessageList : public CListCtrl
{
// Construction
	UINT idtimer;
    UINT idnosel;
	int distance;
	int topbar;
	int lastsort;
	bool focus;
	CNewsArtHolder *selected;	
	CNewsArtHolder *grayed;
	CNRNotify *ntf;
	CString qsearch;
    DWORD qsearchtime;
	bool inupdate;
    char _sortmode;
    DWORD startupdate;
    bool showatend;
public:
	CMessageList(CNRNotify *ntf);
	CNewsGroup *curgroup;
	CNewsAccount *curaccount;
	CWebBrowser2 *browse;
	CHTMLDocument2 webdoc;
	CHTMLElement body;
	CNewsArtHolder *lastmsg;
	CFont normalfont;
	CFont boldfont;
	HICON plus,minus;
	EArticleState state_filter;
	int fthread;
	int lthread;
	bool selmark; //TRUE kdykoliv uzivatel vybere neco v seznamu zprav.
	bool hidetrash; //true to hide trash when update(). 
					//After update, it's resets to false;


// Attributes
public:

// Operations
public:

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMessageList)
	//}}AFX_VIRTUAL

// Implementation
public:
	static void CopyShortcut(CNewsArtHolder *hlda);
	void SelectNewestMsg();
	void InvalidateItems(int min, int max);
	void SelectItem(CNewsArtHolder *hlda);
	void UpdateList();
	void Reload();
	CNewsArtHolder * GetCurMessage();
	void PartDelete(CNewsArtHolder *hld, int afteritem);
	int IsMouseAtButton(CPoint &pt);
	int Mouse2Column(CPoint &pt, int *w1=NULL, int *w2=NULL);
	int Mouse2Item(CPoint& pt);
	virtual void DrawItem( LPDRAWITEMSTRUCT lpDrawItemStruct );
//	void PrepareMessage(CNewsArtHolder *hlda,CHTMLDocument2 *ihtml=NULL);
	void LoadList(CNewsAccount *acc, CNewsGroup *grp, bool autoselect=true);
	void Create(CWnd *parent);
	void SetBrowser(CWebBrowser2 *ww) {browse=ww;}
	virtual ~CMessageList();
	CNewsGroup *GetCurGroup() {return curgroup;}
	CNewsAccount *GetCurAccount() {return curaccount;}
	int QuickSearch(int from, const char *text, bool downtop);
    bool InQSearch() {return qsearch!="" && GetTickCount()-qsearchtime<MAXQSEARCHWAITTIME;}
	void ChangeLastSort(int by);
 
	// Generated message map functions
protected:
	bool ShuldVisible(CNewsArtHolder *hld);
	int UpdateList(CNewsArtHolder *start, int item);
	int UpdateItem(CNewsArtHolder *hld, int shuldbe);
	int PartLoadList(CNewsArtHolder *hlda, int afteritem);
	int LoadItem(int item, CNewsArtHolder *p);
	//{{AFX_MSG(CMessageList)
	afx_msg void OnItemchanged(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnLButtonDown(UINT nFlags, CPoint point);
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnColumnclick(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnKillFocus(CWnd* pNewWnd);
	afx_msg void OnSetFocus(CWnd* pOldWnd);
	afx_msg void OnDblclk(NMHDR* pNMHDR, LRESULT* pResult);
	afx_msg void OnChar(UINT nChar, UINT nRepCnt, UINT nFlags);
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
private:
	void InvalidateThreadMark();
    void BeginPossibleLongOp()
      {
      startupdate=GetTickCount();
      showatend=false;
      }
    void CheckPossibleLongOp()
      {
      if (startupdate && !showatend && GetTickCount()-startupdate>250)
        FasterLongOp();
      }
    void EndLongOp()
      {
      inupdate=false;
      ShowWindow(SW_SHOW);
      startupdate=0;
      }
    void FasterLongOp();

};


void GetNameFrom(const CString &in, CString &out);
void AddReSubject(const char *subject, CString& out);

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MESSAGELIST_H__9FFE4D86_7B00_448F_A8A6_E5B6EE55C9FA__INCLUDED_)
