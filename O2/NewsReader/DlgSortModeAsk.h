#if !defined(AFX_DLGSORTMODEASK_H__C164D732_EF0B_4261_B540_C768B4591DAC__INCLUDED_)
#define AFX_DLGSORTMODEASK_H__C164D732_EF0B_4261_B540_C768B4591DAC__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DlgSortModeAsk.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CDlgSortModeAsk dialog

class CDlgSortModeAsk : public CDialog
{
// Construction
public:
	CDlgSortModeAsk(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CDlgSortModeAsk)
	enum { IDD = IDD_SORTMODEASK };
	int		m_SortMode;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDlgSortModeAsk)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CDlgSortModeAsk)
	afx_msg void OnSortmode();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DLGSORTMODEASK_H__C164D732_EF0B_4261_B540_C768B4591DAC__INCLUDED_)
