// MailList.cpp: implementation of the CMailList class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "MailList.h"
#include <strstream>

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

using namespace std;
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMailMessage::CMailMessage(const char *header, const char *text):
header(header),text(text)
  {

  }

void CMailMessage::GetMailProperty(const char *name, CString &str)
  {
  istrstream s((char *)((LPCTSTR)header));
  char buffer[256];
  str="";
  do
	{
	ScanHeader(s,buffer,sizeof(buffer));
    LoadHeaderValue(s,str);
	if (stricmp(buffer,name)==0) return;	  
	}
  while (!s.eof());
  str="";
  }

void CMailMessage::ScanHeader(istream &in, char *buffer, int size)
  {
  in.getline(buffer,size,':');  
  int i=in.get();
  while (i>=0 && i<33 && i!='\n') i=in.get();
  if (i!=EOF) in.putback(i);
  }

void CMailMessage::LoadHeaderValue(istream &in, CString &data)
  {
  data="";
  int i;
  char buffer[256];
  do
	{
	in.getline(buffer,sizeof(buffer),'\n');
	while ((i=in.peek())!='\n' && i!=EOF)
	  {
    if (buffer[0] && buffer[strlen(buffer)-1]=='\r')
      buffer[strlen(buffer)-1]=0;
	  data+=buffer;
	  in.getline(buffer,sizeof(buffer),'\n');
	  }
  if (buffer[0] && buffer[strlen(buffer)-1]=='\r')
    buffer[strlen(buffer)-1]=0;
	data+=buffer;
	in.get();
	i=in.peek();
	if (i>32 || i==EOF) return	;
	while (i>=0 && i<=33 && i!='\n') i=in.get();
	in.putback(i);
	if (i==EOF) return;
	data+=" ";
	}
  while(1);
  }

CString CMailMessage::operator [](const char *val)
  {
  CString out;
  GetMailProperty(val,out);
  return out;
  }

void CMailMessage::EnumProperties(CMM_ENUMPROC proc, void *context)
  {
  istrstream s((char *)((LPCTSTR)header));
  char buffer[256];
  CString value;
  do
	{
	ScanHeader(s,buffer,sizeof(buffer));
	LoadHeaderValue(s,value);
	if (proc(this,buffer,value,context)==false) break;
	}
  while (!s.eof());
  }


CMailList::CMailList(int newcount) 
  {
  count=newcount;
  ids=new long[count];
  memset(ids,0,sizeof(long)*count);
  list=new CMailMessage[count];
  }

bool CMailList::CopyMessByID(int mailpos, long ID, CMailList &src)
  {
  if (mailpos<0 || mailpos>=count) return false;
  CMailMessage *fnd=src.GetMessage(ID);
  if (fnd==NULL) {ids[mailpos]=0;return false;}
  else
	{
	ids[mailpos]=ID;
	list[mailpos]=*fnd;	
	}
  return true;
  }

CMailMessage *CMailList::GetMessage(long id)
  {
  for (int i=0;i<count;i++)
	if (ids[i]==id) return list+i;
  return NULL;
  }

void CMailList::SetMailMessage(int mailpos,CMailMessage *msg,long ID)
  {
  if (mailpos<0 || mailpos>=count) return;
  ids[mailpos]=ID;
  list[mailpos]=*msg;
  }

void CMailList::RemoveDuplicity()
  {
  for (int i=0;i<count;i++)
	{
	for (int j=i+1;j<count;j++)
	  if (ids[i]==ids[j]) ids[i]=ids[j]=0;
	}
  }

int CMailList::GetFreeMailpos()
  {
  for (int i=0;i<count;i++)
	if (ids[i]==0) return i;
  return -1;
  }


