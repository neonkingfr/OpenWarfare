#include "stdafx.h"
#include ".\mailhdrparser.h"


void CMailHdrParser::CorrectOneLine(char *ln)
{
  char *c=ln;
  char *a=strchr(ln,'\r');
  while (a) {strcpy(a,a+1);a=strchr(a,'\r');}
  a=strstr(ln,"\n\t");
  while (a) {strcpy(a,a+2);a=strstr(a,"\n\t");}
  a=strstr(ln,"\n ");
  while (a) {strcpy(a,a+2);a=strstr(a,"\n ");}
}

bool CMailHdrParser::ParseOneLine(char *ln)
{
  char *x=strchr(ln,':');
  if (x==0) return false;
  *x=0;
  char *x1=x-1;
  while (x1>ln && *x1!=0 && isspace((unsigned char)*x1)) *x1--=0;
  while (ln[0]!=0 && isspace((unsigned char)ln[0])) ln++;

  x++;
  char *x2=strchr(x,0)-1;
  while (x2>x && *x2!=0 && isspace((unsigned char)*x2)) *x2--=0;
  while (x[0]!=0 && isspace((unsigned char)x[0])) x++;

  HdrResult(ln,x);
  return true;
}


void CMailHdrParser::ParseHeader(const char *hdr)
{
  char *tempbuff=(char *)alloca(sizeof(char)*(strlen(hdr)+1));

  strcpy(tempbuff,hdr);

  char *beg=tempbuff;
  char *c=beg;
  do 
  {
opak:
    while (*c!='\r' && *c!='\n' && *c!=0) c++;
    if (*c=='\r' || *c=='\n')
    {
      char *d=c;
      while (*d=='\r' || *d=='\n') d++;
      if (*d!='\t' && *d!=' ')
      {
        *c=0;
        CorrectOneLine(beg);
        if (ParseOneLine(beg)==false) break;
        beg=d;
        c=beg;
      }      
      else 
      {
        c=d;
      }
    }
  } 
  while(*c!=0);
  

  
}