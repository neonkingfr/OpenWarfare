// ArticleDownload.cpp: implementation of the CArticleDownloadControl class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "newsreader.h"
#include "ArticleDownload.h"
//#include "WebBrowser2.h"
#include "NewsTerminal.h"
#include "NewsArticle.h"
#include "ScanGroupsThread.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CArticleDownloadControl::CArticleDownloadControl()
  {
  memset(downloadArticles,0,sizeof(downloadArticles));
  browselist=NULL;
  browsecount=0;
  }

bool CArticleDownloadControl::RegisterBrowser(CWebBrowser2 *brw)
  {
  int newcnt=browsecount+1;
  void *p=realloc(browselist,newcnt*sizeof(*browselist));
  if (p==NULL) return false;
  browselist=(CWebBrowser2 **)p;
  browselist[browsecount]=brw;
  browsecount=newcnt;
  return true;
  }

void CArticleDownloadControl::UnregisterBrowser(CWebBrowser2 *brw)
  {
  int i;
  int j;
  for (i=0,j=0;i<browsecount;i++,j++)
	{
	if (browselist[i]==brw) j--;
	else if (i!=j) browselist[j]=browselist[i];	
	}
  void *p=realloc(browselist,j*sizeof(*browselist));
  if (p) browselist=(CWebBrowser2 **)p;
  browsecount=j;
  }

void CArticleDownloadControl::NotifyBrowsers(CNewsArtHolder *hld, EDownloadResult result)
  {
  int i;
  for (i=0;i<MAXDOWNLOADNOTIFYPOS;i++) 
	if (downloadArticles[i]==hld) downloadArticles[i]=NULL;
  for (int i=0;i<browsecount;i++)
	browselist[i]->DownloadNotify(hld,result);
  }

CArticleDownloadControl::EDownloadState CArticleDownloadControl::PrepareArticle(CNewsAccount *acc, CNewsGroup *grp, CNewsArtHolder *hlda, bool download_long)
  {
  if (hlda->GetArticle()) return ED_IsReady;
  int i;
  for (i=1;i<MAXDOWNLOADNOTIFYPOS;i++) 
	if (downloadArticles[i]==hlda) return ED_InProgress;	
  CDownloadArticleThread *downthr=new CDownloadArticleThread(theApp.m_pMainWnd,hlda,acc,grp,download_long);
  for (i=1;i<TOTALTHREADS;i++)
	{
	if (theApp.RunBackground(i,downthr,0,false)==true)
	  {
	  downloadArticles[i]=hlda;
	  return ED_DowloadStarted;
	  }
	}
  delete downthr;
  return ED_Full;
  }
