// CloseThreadDlg.cpp : implementation file
//

#include "stdafx.h"
#include "newsreader.h"
#include "CloseThreadDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCloseThreadDlg dialog


CCloseThreadDlg::CCloseThreadDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CCloseThreadDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CCloseThreadDlg)
	kwrds = _T("");
	//}}AFX_DATA_INIT
	subjectwarning=false;
}


void CCloseThreadDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCloseThreadDlg)
	DDX_Text(pDX, IDC_EDIT1, kwrds);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CCloseThreadDlg, CDialog)
	//{{AFX_MSG_MAP(CCloseThreadDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCloseThreadDlg message handlers

void CCloseThreadDlg::OnOK() 
  {
  if (subjectwarning)
	if (NrMessageBox(IDS_SUBJECTWARNING,MB_YESNO|MB_ICONWARNING)==IDNO) return;
  CDialog::OnOK();
  }
