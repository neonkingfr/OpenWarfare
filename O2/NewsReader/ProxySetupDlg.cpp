// ProxySetupDlg.cpp : implementation file
//

#include "stdafx.h"
#include "newsreader.h"
#include "ProxySetupDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CProxySetupDlg dialog


CProxySetupDlg::CProxySetupDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CProxySetupDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CProxySetupDlg)
	sockenable = FALSE;
	port = 0;
	address = _T("");
	//}}AFX_DATA_INIT
}


void CProxySetupDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CProxySetupDlg)
	DDX_Control(pDX, IDC_ADDRESS, wAdr);
	DDX_Control(pDX, IDC_PORT, wPort);
	DDX_Check(pDX, IDC_SOCK4ENABLE, sockenable);
	DDX_Text(pDX, IDC_PORT, port);
	DDX_Text(pDX, IDC_ADDRESS, address);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CProxySetupDlg, CDialog)
	//{{AFX_MSG_MAP(CProxySetupDlg)
	ON_BN_CLICKED(IDC_SOCK4ENABLE, OnSock4enable)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()



/////////////////////////////////////////////////////////////////////////////
// CProxySetupDlg message handlers

#define GROUP "Proxy"
#define ENABLE "Enable"
#define ADDRESS "Address"
#define PORT "Port"


void CProxySetupDlg ::LoadConfig()
  {
  address=theApp.GetProfileString(GROUP,ADDRESS,"");
  port=theApp.GetProfileInt(GROUP,PORT,1080);
  sockenable=theApp.GetProfileInt(GROUP,ENABLE,0);
  }

void CProxySetupDlg::SaveConfig()
  {
  theApp.WriteProfileInt(GROUP,ENABLE,sockenable);
  theApp.WriteProfileInt(GROUP,PORT,port);
  theApp.WriteProfileString(GROUP,ADDRESS,address);
  }

BOOL CProxySetupDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	CheckRules();
		
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CProxySetupDlg::CheckRules()
  {
  BOOL p = IsDlgButtonChecked(IDC_SOCK4ENABLE);
  wPort.EnableWindow(p);
  wAdr.EnableWindow(p);
  }

void CProxySetupDlg::OnSock4enable() 
  {
  CheckRules();	
  }

void CProxySetupDlg::OnOK() 
  {
	CDialog::OnOK();
	SaveConfig();
  }
