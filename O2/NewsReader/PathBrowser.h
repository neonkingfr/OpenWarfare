// PathBrowser.h: interface for the CPathBrowser class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_PATHBROWSER_H__516F4A08_FFEA_4921_8719_777856A89777__INCLUDED_)
#define AFX_PATHBROWSER_H__516F4A08_FFEA_4921_8719_777856A89777__INCLUDED_

bool AskForPath(CString &curpath);

#endif // !defined(AFX_PATHBROWSER_H__516F4A08_FFEA_4921_8719_777856A89777__INCLUDED_)
