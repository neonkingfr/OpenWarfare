// DownloadProgress.cpp : implementation file
//

#include "stdafx.h"
#include "newsreader.h"
#include "DownloadProgress.h"
using namespace std;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDownloadProgress dialog


CDownloadProgress::CDownloadProgress(CWnd* pParent /*=NULL*/)
  : CDialog(CDownloadProgress::IDD, pParent)
  {
    //{{AFX_DATA_INIT(CDownloadProgress)
    // NOTE: the ClassWizard will add member initialization here
    //}}AFX_DATA_INIT
  }

void CDownloadProgress::DoDataExchange(CDataExchange* pDX)
{
  CDialog::DoDataExchange(pDX);
  //{{AFX_DATA_MAP(CDownloadProgress)
  DDX_Control(pDX, IDC_MSGNAME, msgname);
  DDX_Control(pDX, IDC_PROGRESS1, progress);
  //}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CDownloadProgress, CDialog)
  //{{AFX_MSG_MAP(CDownloadProgress)
  //}}AFX_MSG_MAP
  END_MESSAGE_MAP()
    
    /////////////////////////////////////////////////////////////////////////////
    // CDownloadProgress message handlers
    
    class CDownloadArticleThread2: public CRunner
    {
      public:
        CNewsArtHolder *hld;
        CNewsAccount *acc;
        Socket _s;
        HWND button;
       public:
        CDownloadArticleThread2(CNewsArtHolder *hld,Socket s,CNewsAccount *acc,HWND button):hld(hld),_s(s),acc(acc),button(button) 
        {}
        virtual void Run();
        
    };

void CDownloadArticleThread2::Run()
{  
  CNewsTerminal term(_s);
  CButton bt;
  bt.Attach(button);
  bt.SetCheck(1);
  int i=hld->ReloadArticle(term,NULL,false);
  bt.SetCheck(0);
  if (i==-1)
  {
    if (term.IsStopped()==false) hld->SetTrash();
  }
  else
    if (acc->offline) hld->SaveToCache(acc->cache);
  bt.Detach();
}

BOOL CDownloadProgress::OnInitDialog() 
{
  CDialog::OnInitDialog();
  
  progress.SetRange(0,itemtree->GetSelectedCount());
  CenterWindow();
  ShowWindow(SW_SHOW);
  UpdateWindow();
  stopd=false;
  MSG msg;
  int i;
  
  /*
	for (i=0;i<3;i++)
	  while (theApp.RunBackground(i,NULL,100)==false && stopd==false) 
		while (::PeekMessage(&msg,0,0,0,PM_REMOVE)) DispatchMessage(&msg);
	*/
  if (stopd) 
  {PostMessage(WM_COMMAND,IDCANCEL);return FALSE;}
  CNewsTerminal term[3];  
  for (i=0;i<3;i++)
  {
    threads[i]=false;
    int err=acc->OpenConnection(term[i]);
    if (term[i].IsStopped()) 
    {PostMessage(WM_COMMAND,IDCANCEL);return FALSE;}
    if (err) continue;
    term[i].SendFormatted("GROUP %s",grp->GetName());
    err=term[i].ReceiveNumReply();
    if (err/10!=21) continue;
    threads[i]=true;
  }
  for (i=0;i<3;i++) if (threads[i]) break;
  if (i==3) 
  {term[0].ShowError(CLT_CONNECTFAILED);PostMessage(WM_COMMAND,IDCANCEL);return FALSE;}	  
  POSITION pos=itemtree->GetFirstSelectedItemPosition();
  swapper=1;
  i=0;
  first=NULL;
  SetTimer(100,1000,NULL);
  warning=false;
  while (pos && stopd==false)
  {
    int p=itemtree->GetNextSelectedItem(pos);
    CNewsArtHolder *hlda=(CNewsArtHolder *)itemtree->GetItemData(p);
    Download(term,hlda);
    i++;
    progress.SetPos(i);
  }
  for (i=0;i<3;i++) 
    if (threads[i])	
    {
      while (theApp.RunBackground(i,NULL,100)==false) 
        while (::PeekMessage(&msg,0,0,0,PM_REMOVE)) DispatchMessage(&msg);
      term[i].SendLine("QUIT");
    }
  PostMessage(WM_COMMAND,IDCANCEL);
  return TRUE;  // return TRUE unless you set the focus to a control
  // EXCEPTION: OCX Property Pages should return FALSE
}

void CDownloadProgress::OnCancel() 
{
  stopd=true;	
  CNewsTerminal::StopTrafic();
  CDialog::OnCancel();
}

void CDownloadProgress::Download(CNewsTerminal *term, CNewsArtHolder *hld, bool recurse)
{
  acc->Lock();
  
  int i;
  do
  {
    for (i=0;i<3;i++) if (threads[i])
      if (theApp.RunBackground(i,NULL,0)==true) break;
    if (i==3) 
    {
      MSG msg;
      while (GetMessageA(&msg,0,0,0) && msg.message!=WM_TIMER)
        DispatchMessage(&msg);
      while (PeekMessage(&msg,0,0,0,PM_REMOVE)) DispatchMessage(&msg);
    }	
    if (first && warning==false && i!=3)
    {
      iostream *q=acc->cache.OpenMessage(first->messid);
      if (q==NULL)
      {
        NrMessageBox(IDS_CACHESMALLWARNING);
        warning=true;
      }
      else
        acc->cache.CloseMessage();
    }
  }
  while (i==3 && !stopd);
  if (!stopd)
  {
    if (hld->GetArticle()==NULL)
    {
      iostream *q=acc->cache.OpenMessage(hld->messid);
      if (q==NULL)
      {		
        if (first==NULL) first=hld;
        theApp.RunBackground(i,new CDownloadArticleThread2(hld,term[i],acc,::GetDlgItem(*this,IDC_THRD1+i)),INFINITE);
      }
      else
        acc->cache.CloseMessage();
    }
    msgname.SetWindowText(hld->GetSubject());  
    bool p=hld->GetOpened();
    hld=hld->GetChild();
    while (hld && (p==false || recurse))
    {
      Download(term,hld,true);
      hld=hld->GetNext();
    }
  }
  acc->Release();
}

