#if !defined(AFX_CONTITIONSDLG_H__59FB2653_09C5_4A7F_9B85_47DC5A2E6C53__INCLUDED_)
#define AFX_CONTITIONSDLG_H__59FB2653_09C5_4A7F_9B85_47DC5A2E6C53__INCLUDED_

#include "CondCalc.h"	// Added by ClassView
#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ContitionsDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CContitionsDlg dialog

class CContitionsDlg : public CDialog
{
// Construction
public:
	CCondCalc clc;
	CContitionsDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CContitionsDlg)
	enum { IDD = IDD_PROFILEVALUES };
	CString	cond;
	CString	name;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CContitionsDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CContitionsDlg)
	virtual void OnOK();
	afx_msg void OnChangeName();
	virtual BOOL OnInitDialog();
	afx_msg void OnDelete();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CONTITIONSDLG_H__59FB2653_09C5_4A7F_9B85_47DC5A2E6C53__INCLUDED_)
