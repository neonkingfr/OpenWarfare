#if !defined(AFX_PROGESSDLG_H__CAC71500_A8FB_11D5_B3A0_0050FCFE63F1__INCLUDED_)
#define AFX_PROGESSDLG_H__CAC71500_A8FB_11D5_B3A0_0050FCFE63F1__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ProgessDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CProgessDlg dialog

class CProgessDlg : public CDialog
{
// Construction
	UINT max;
	UINT cur;
	UINT maxsettime;
	UINT cursettime;
	int cntr;
	BOOL passive;
public:
	virtual void OnCancel();
	void Create();
	void SetMax(UINT m) {max=m;maxsettime=GetTickCount();}
	void SetCur(UINT c) {cur=c;cursettime=GetTickCount();}
	CProgessDlg(CWnd* pParent = NULL);   // standard constructor
	BOOL Active() {return !passive;}

// Dialog Data
	//{{AFX_DATA(CProgessDlg)
	enum { IDD = IDD_PROGESSDLG };
	CProgressCtrl	bar;
	CStatic	value;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CProgessDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CProgessDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnClose();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
private:
	void PlaceWindow();
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PROGESSDLG_H__CAC71500_A8FB_11D5_B3A0_0050FCFE63F1__INCLUDED_)
