#pragma once

class ISharedMemoryTarget
{
public:
  virtual unsigned long GetTargetType() const=0;
  virtual void *GetTarget() const=0;
};


class ISharedMemory
{
public: 
  /// Allocates shared memory. 
  /** Memory allocated using ISharedMemory::Allocate() can be shared with other applications
  @param size size of memory to allocate
  @return function returns true, if memory has been succesfully allocated
  */
  virtual bool Allocate(size_t size)=0;

  /// Locks region of shared memory. 
  /** Most of implementations can locks only one region at time. Region may be locked
  for all applications, that currently shares the memory, depend on exclusive parametr specified.
  @param offset specified offset in shared memory to lock.
  @param size specified size of region to lock. specify zero to lock region from offset till end.
  @param exclusive function will ensure, that memory is exclusive locked, none of other applications
  can access the memory. Depend on implementation, lock may be global or local. For example, 
  in case windows file mapping, Lock will locks memory exclusive against to all other
  applications, that using it. On the other hand, shared memory that using TCP/IP will be locked
  locally, because each application has own copy of memory.
  @param waittime time in miliseconds to wait for lock.
  @return function returns valid pointer to locked region. If an error occured (e.g. in case of timeout)
  the function returns NULL; If lock is multiple times, without Unlock, function failed
  */
  virtual void *Lock(size_t offset=0, size_t size=0, bool exclusive=true, DWORD waittime=0xFFFFFFFF)=0;
  ///Unlocks region, release exclusive lock
  virtual bool Unlock()=0;

  ///Sets target application, which will share memory.
  /** This will not start memory sharing, only set target for Share function
  @param target target identifier of application that will share memory
  @return true if target has been changed
  */
  virtual bool SetTarget(const ISharedMemoryTarget &target)=0;

  ///Returns current target. 
  /**Do not try to remove const from target, for change target use SetTarget. */

  virtual const ISharedMemoryTarget &GetCurrentTarget() const=0;
  ///Sends content of shared memory to another application
  /** Depens on implementation, function sends whole content, or identifier only. 
  @param messageID message identifier associated with content.  Implementation should support this value.
  @param userData unsigned user data associated with content. Implementation doesn't need to support this value.  
  @return true, if operation was succesful
  */
  virtual bool Share(unsigned int messageId, unsigned long userData=0) const =0;

  ///Frees memory. If memory is locked, unlocks it first. Destructor calls Free()
  virtual void Free()=0;
  ///Returns size of allocated memory.
  virtual size_t Size()const=0;
  ///Returns lock status
  /**  
  @param offset optional parameter, if it is specified, address is filled with offset of current locked region
  @param size optional parameter, if it is specified, address is filled with size of current locked region
  @return true, if memory is locked. if result is false, content of offset and size remains untouched.
  True is returned, only if memory is locked for current thread. To get state of lock of another process or thread,
  call IsBusy.
  */
  virtual bool IsLocked(size_t *offset=NULL, size_t *size=NULL) const =0;

  ///Returns lock status
  /** Function returns true, if memory is exclusive locked for another application. Otherwise it returns false, including 
  the case memory is locked for current thread.  
  */
  virtual bool IsBusy() const =0;


  ///Discovers how memory is shared
  /**
  @return true, if sharing is simulated by copiing memory. In this case, each application
  has own copy of memory. Application must call Share function each time it wants to
  update content at oposite side.*/
  virtual bool ShareSimulateCopy() const =0;

  ///Use this function to send packages through ISharedMemory class
  /**Function allocates new memory to hold data. Then data is sent
  using ShareWith, and after it, data is destroyed. Some implementations can optimize this 
  function and sent data directly without copying into temporally buffer

  @param messageID message identifier associated with content.  Implementation should support this value.
  @param data pointer to datablock to send.
  @param size size of datablock to send.
  @param userData unsigned user data associated with content. Implementation doesn't need to support this value.  
  @return true, if operation was succesful. If any memory is allocated in object, function will fail.
  */
  virtual bool SendPackage( unsigned int messageId, const void *data, size_t size, unsigned long userData=0)=0;

  ///Synchronizes shared memory from incoming message
  /** Incoming message is platform depend object, it is handled by implementation.
  When message incomed, construct this object from message data, cast it to ISharedMemoryTarget
  and use for Synchronize. Depend on implementation it can copy content of shared memory into
  object, or create reference to shared memory at another application. 
  if memory is locked, function will wait infinite time until unlock.
  @param message, object created from incomed message. 
  @return true, if operation was succesful
  */
  virtual bool Synchronize(const ISharedMemoryTarget &message)=0;


  virtual ~ISharedMemory(void)=0 {}
};
