#include "stdafx.h"
//#include <mshtml.h>
//#include <atlbase.h>
#include "NewsReader.h"                                    
#include "webbrowser2.h"
#include "NewsTerminal.h"
#include "NewsAccount.h"
#include "NewsArticle.h"
#include "ScanGroupsThread.h"
using namespace std;


static bool CheckRoundBuffer(const char *what, const char *buff, int bfindex, int bfsize)
  {
  int l=bfindex;
  int q;
  while (*what)
	{
	if (l>=bfsize) q=l-bfsize;else q=l;
	if (toupper(*what)!=toupper(buff[q])) return false;
	l++;
	what++;
	}
  return true;
  }

static int OutputWord(const char *sbuff, int idx, int size,ostream& str)
  {
  unsigned char *buff=(unsigned char *)sbuff;
  while (buff[idx]>32 && buff[idx]!='>' && buff[idx]!=')')
	{
	int nxt=idx+1;
	if (nxt>=size) nxt=0;
	if (!isalnum(buff[idx]) && buff[idx]!='/' && buff[idx]!='\\'  && buff[idx]<128 && buff[nxt]<33) return idx;
	str<<buff[idx];
	idx=nxt;
	}
  return idx;
  }

static bool CheckInsideWord(const char *what, const char *buff, int bfindex, int bfsize)
  {
  int l=bfindex;
  int q;
  const char *c=what;
  while (*c)
	{
	if (l>=bfsize) q=l-bfsize;else q=l;
	if (buff[q]<33) return false;
	if (toupper(*c)!=toupper(buff[q])) c=what;else c++;
	l++;
	}
  return true;
  }

static char OutputURL(ostream& out, const char *buff, int index, int size)
  {
  if (!CheckRoundBuffer("<URL>",buff,index,size)) return ' ';
  index+=5;if (index>=size) index-=size;
  int beg=index;
  if (CheckRoundBuffer("</URL>",buff,index,size)==false)
	{
	out<<"<a href=\""<<buff[index];
	index++;if (index>=size) index-=size;
	int end=index-10;
	if (end<0) end+=size;
	while (CheckRoundBuffer("</URL>",buff,index,size)==false && index!=end)
	  {
	  if (buff[index]>32) out<<buff[index];
	  index++;if (index>=size) index-=size;
	  }
	out<<"\">";
	}
  return ' ';
  }

void Text2Html(istream &in, ostream& out, CMsgIdList *idhash)
  {  
  char buff[1024];
  int idx=0;
  memset(buff,0,sizeof(buff));
  char *endtext=NULL;
  int endindex=-1;
  bool chrs=false;
  int chc1=0, chs=0, chl=0, chcm=0;
  char last;
  bool nospace=false;
  int skip=0;
  while (!in.eof() || buff[idx] || chrs)
	{
	int i=in.get(),c;
	if (i==EOF) i=0;else chrs=true;
	if (skip==0)
	  {
	  if (idx==endindex) {out<<endtext;endtext=NULL;endindex=-1;}
  //	if ((buff[idx]<33 || buff[idx]=='>' || buff[idx]==')') && endtext) {out<<endtext;endtext=NULL;}	
	  chc1++;
	  switch (buff[idx])
		{
		case 0:chc1--;break;
		case ' ': if (buff[(idx+1)&1023]==' ') 
					out<<"&nbsp;";
				  else 
					out<<' ';
                  break;
        case '\t': out<<"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; ";
		case '\r':break;
/*		case '\t':
		  {
		  if (CheckRoundBuffer("\t\r\n",buff, idx, sizeof(buff)))
			{
			out<<" ";skip=2;
			}
		  else if (CheckRoundBuffer("\t\n",buff, idx, sizeof(buff)))
			{
			out<<" ";skip=1;
			}
		  break;
		  }*/
		case '\n': 
		  {
		  chs+=chc1;
		  chl++;
		  chcm=chs/chl;
		  char nx;
		  nx=buff[(idx+1)&1023];
/*		  if (theApp.config.smartWrp && (chc1>(chcm-chcm/3)))
			{
			if ((isalpha(last) || last==',' || last==';') && (isalpha(nx) || nx=='(' || nx==')')) out<<" ";
			else if ((last=='.' || last=='!' || last=='?') && isalpha(nx) && toupper(nx)==nx) out<<" ";
			else out<<"<br>";
			}
		  else*/ out<<"<br>\r\n";
		  chc1=0;
		  break;
		  }
		case '<':
		  if (CheckRoundBuffer("<URL>",buff, idx, sizeof(buff)))
			{
			OutputURL(out,buff,idx,sizeof(buff));skip=4;endtext="";endindex=-1;
			break;
			}
		  if (CheckRoundBuffer("</URL>",buff, idx, sizeof(buff)))
			{
			out<<"</a>";skip=5;endtext=NULL;
			break;
			}
		  out<<"&lt;";break;
		case '>':out<<"&gt;";break;
		case '&':out<<"&amp;";break;
		default: 
		  c=(unsigned char)buff[idx];
		  if (endtext==NULL && (isalnum(c) || c=='\\'))
		  if (CheckRoundBuffer("file://",buff, idx, sizeof(buff)) ||
			  CheckRoundBuffer("\\\\",buff, idx, sizeof(buff)) ||
			  CheckRoundBuffer("file:\\\\",buff, idx, sizeof(buff)))
			{
			out<<"<a href=\"";
			endindex=OutputWord(buff,idx,sizeof(buff),out);
			out<<"\" target=\"NewsReader\">";		  
			endtext="</a>";
			}
		  else if (CheckRoundBuffer("http://",buff, idx, sizeof(buff)) ||
            CheckRoundBuffer("https://",buff, idx, sizeof(buff)) )
          {
			out<<"<a href=\"";
			endindex=OutputWord(buff,idx,sizeof(buff),out);
			out<<"\">";		  
			endtext="</a>";
          }
		  else if (CheckRoundBuffer("ftp://",buff, idx, sizeof(buff)) ||
				  CheckRoundBuffer("vss://",buff, idx, sizeof(buff)) ||
				  CheckRoundBuffer("vcs://",buff, idx, sizeof(buff)) ||				  
				  CheckRoundBuffer("gopher://",buff, idx, sizeof(buff)) ||
				  CheckRoundBuffer("mailto:",buff, idx, sizeof(buff)) ||		  				  		  
				  CheckRoundBuffer("telnet://",buff, idx, sizeof(buff)))		  
			{
			out<<"<a href=\"";
			endindex=OutputWord(buff,idx,sizeof(buff),out);
			out<<"\" target=\"NewsReader\">";		  
			endtext="</a>";
			}
		  else if (CheckRoundBuffer("www.",buff, idx, sizeof(buff)))
			{
			out<<"<a href=\"http://";
			endindex=OutputWord(buff,idx,sizeof(buff),out);
			out<<"\">";		  
			endtext="</a>";
			}	
		  else if (CheckRoundBuffer("news:",buff, idx, sizeof(buff)))
			{
			out<<"<a href=\"news:";
			idx+=5;if (idx>=sizeof(buff)) idx-=sizeof(buff);
			endindex=OutputWord(buff,idx,sizeof(buff),out);
			char temp[256];
			ostrstream tempstr(temp,256);
			tempstr<<"<";
			OutputWord(buff,idx,sizeof(buff),tempstr);
			tempstr<<">";
			tempstr<<'\0';
            CNewsArtHolder *hld=idhash?idhash->Find(temp):0;
			out<<"\" >";
			endtext="</a>";		
			if (hld)
			  {
			  skip=(endindex-1-idx);
			  if (skip<0) skip+=sizeof(buff);
			  skip+=5;
			  out<<hld->subject;
			  c=EOF;
			  }
		    idx-=5;if (idx<0) idx+=sizeof(buff);
			} 
		  else if (CheckInsideWord("@",buff, idx, sizeof(buff)))
			{
			if (theApp.config.useoe)			  
			  out<<"<a href=\"mailto:";
			else
			  out<<"<a href=\"nrmail://";
			endindex=OutputWord(buff,idx,sizeof(buff),out);
			out<<"\">";
			endtext="</a>";
			}
		  if (c!=EOF)
	/*		if (c>128 || c<32) 
			  {
			  char buff[10];
			  sprintf(buff,"&#%d;",c);
			  out<<buff;
			  }
			else */
			  out<<(char)c;
		  last=c;
		  break;
		}
	  }
	else
	  skip--;
	buff[idx]=i;
	idx++;
	if (idx>=sizeof(buff)) {idx=0;chrs=false;}
	}	  
	
/*
   if (i>32) i=AnalyzeWord(in,out);
	else i=in.get();
	switch (i)
	  {
	  case EOF:break;
	  case '\r':break;
	  case '\n':out<<"<BR>";break;
	  default:out<<(char)i;
	  }
	}*/
  }




static void OutColorProp(ostream &out, char *propname, COLORREF color)
  {
  out<<propname<<"=\"#";
  char buff[10];
  sprintf(buff,"%06X",htonl(color)>>8);
  out<<buff<<"\" ";
  }
