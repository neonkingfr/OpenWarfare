//{{AFX_INCLUDES()
#include "webbrowser2.h"
//}}AFX_INCLUDES
#if !defined(AFX_IEDISPPROPDLG_H__804AEB60_9E55_11D5_B3A0_00C0DFAE7D0A__INCLUDED_)
#define AFX_IEDISPPROPDLG_H__804AEB60_9E55_11D5_B3A0_00C0DFAE7D0A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// IEDispPropDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CIEDispPropDlg dialog

#include "resource.h"

class CIEDispPropDlg : public CDialog
{
// Construction
	static LONG GetColorFromButton(CButton &butt);
public:
	void LoadSettings();
	void SaveSettings();
	void CreateFontEndSeq(CString &end, bool fromdlg=false);
	void CreateFontBeginSeq(CString &beg,bool fromdlg=false);
	void OnDrawPreview();
	CIEDispPropDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CIEDispPropDlg)
	enum { IDD = IDD_IEFONTSELECT };
	CButton	bColLink;
	CButton	bColBg;
	CButton	bColText;
	CListBox	wSize;
	CListBox	wFont;
	BOOL	bold;
	BOOL	en_bgcolor;
	BOOL	italic;
	CString	font;
	BOOL	en_linkcol;
	CString	size;
	BOOL	en_texcolor;
	//}}AFX_DATA
	LONG textcol;
	LONG bgcol;
	LONG linkcol;
	CWebBrowser2 preview;


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CIEDispPropDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CIEDispPropDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct);
	afx_msg void OnRules();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnColor();
	afx_msg void OnPreview();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_IEDISPPROPDLG_H__804AEB60_9E55_11D5_B3A0_00C0DFAE7D0A__INCLUDED_)
