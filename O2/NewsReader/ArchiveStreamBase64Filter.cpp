// ArchiveStreamBase64Filter.cpp: implementation of the ArchiveStreamBase64Filter class.
//
//////////////////////////////////////////////////////////////////////


#include <stdlib.h>
#include <string.h>
#include "ArchiveStreamBase64Filter.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

unsigned char ArchiveStreamBase64Filter::alphabet[65] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
unsigned char ArchiveStreamBase64Filter::inalphabet[256] = "?";
unsigned char ArchiveStreamBase64Filter::decoder[256];



ArchiveStreamBase64Filter::ArchiveStreamBase64Filter(ArchiveStream *input)
  {
  _input=input;
  _wrap=true;
  if (inalphabet[0]=='?')
    {
    int i;
    memset(inalphabet,0,sizeof(inalphabet));
    for (i = 0;i < 64 ; i++) 
      {
	  inalphabet[alphabet[i]] = 1;
	  decoder[alphabet[i]] = i;
      }
    }
  _bufpos=0;
  Reset();
  }

ArchiveStreamBase64Filter::~ArchiveStreamBase64Filter()
  {
  Reset();
  }

void ArchiveStreamBase64Filter::Flush()
  {
  if (!IsError() && IsStoring())
    {    
    if (_bufpos) FlushWrite();
    }
  _input->Flush();
  }


void ArchiveStreamBase64Filter::Reset()
  {
  Flush();
  _input->Reset();  
  if (IsStoring()) _bufpos=0;else _bufpos=3;
  _cols=0;
  }

int ArchiveStreamBase64Filter::DataExchange(void *buffer, int maxsize)
  {
  if (!IsError()) 
    {
    if (IsStoring()) Encode(buffer,maxsize);
    else Decode(buffer,maxsize);
    }
  return IsError();
  }

void ArchiveStreamBase64Filter::Decode(void *data, int size)
  {
  char *datapos=(char *)data;
  while (size)
    {
    if (_bufpos>=3)
      {
      int bits=0;      
      int i;
      for (i=0;i<4;i++)
        {
        unsigned char c;
        _input->ExSimple(c);
        if (c=='=') break;
        while (!inalphabet[c]) 
          {
          if (IsError()) return;
          _input->ExSimple(c);
          }
        if (IsError()) return;
        bits=(bits<<6) | decoder[c];
        }      
      switch (i)
        {
        case 0: continue;
        case 1: SetError(ARCHBASE64_STREAMERROR);return;
        case 2: _smallbuf[2]=bits>>4;
                _bufpos=2;                
                _input->Reserved(1);
                break;
        case 3: _smallbuf[1]=bits>>10; 
                _smallbuf[2]=(bits>>2) & 0xFF;
                _bufpos=1;
                break;
        case 4: _smallbuf[0]=bits>>16; 
                _smallbuf[1]=(bits>>8) & 0xFF;
                _smallbuf[2]=bits & 0xFF;
                _bufpos=0;
                break;
        }
      }
    int remain=__min(size,3-_bufpos);
    memcpy(datapos,_smallbuf+_bufpos,remain);
    size-=remain;
    _bufpos+=remain;
    datapos+=remain;
    }
  }

void ArchiveStreamBase64Filter::Encode(void *data, int size)
  {
  char *datapos=(char *)data;
  while (size)
    {
    int remain=__min(size,3-_bufpos);
    memcpy(_smallbuf+_bufpos,datapos,remain);
    size-=remain;
    _bufpos+=remain;
    datapos+=remain;
    if (_bufpos>=3) FlushWrite();
    }
  }

void ArchiveStreamBase64Filter::FlushWrite()
  {
  unsigned char c;
  unsigned long data=((unsigned long)_smallbuf[0]<<16) | 
                     ((unsigned long)_smallbuf[1]<<8) | 
                     ((unsigned long)_smallbuf[2]);
  
  c=(unsigned char)((data>>18) & 0x3F);_input->ExSimple(alphabet[c]);  
  c=(unsigned char)((data>>12) & 0x3F);_input->ExSimple(alphabet[c]);

  switch (_bufpos)
    {
    case 1: c='='; _input->ExSimple(c);
            c='='; _input->ExSimple(c);
            break;
    case 2: c=(unsigned char)((data>>6) & 0x3F);_input->ExSimple(alphabet[c]);
            c='='; _input->ExSimple(c);
            break;
    case 3: c=(unsigned char)((data>>6) & 0x3F);_input->ExSimple(alphabet[c]);
            c=(unsigned char)((data) & 0x3F);_input->ExSimple(alphabet[c]);
            break;

    }
  if (_wrap)
    {
    _cols+=4;
    if (_cols>72) 
      {
      char *nl="\r\n";
      _input->DataExchange(nl,2);
      _cols=0;    
      }
    _bufpos=0;
    }
  }

