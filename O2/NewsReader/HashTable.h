// HashTable.h: interface for the CHashTable class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_HASHTABLE_H__48213B65_5B53_4F13_B342_F4C11EE38F7F__INCLUDED_)
#define AFX_HASHTABLE_H__48213B65_5B53_4F13_B342_F4C11EE38F7F__INCLUDED_

#include "NewsTerminal.h"
#include "NewsArticle.h"	// Added by ClassView
#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CHashTable  
  {
  CNewsArtHolder **hldlist;
  DWORD tblsize;
  DWORD itemcount;
  CCriticalSection locker;
	CNewsArtHolder * _Find(const char *msgid);
	static DWORD HashName(const char *name, DWORD module);
	bool _Add(CNewsArtHolder *hld);
	bool Expand();
  public:
	  void Reset();
	CNewsArtHolder * Find(const char *msgid);
	bool Add(CNewsArtHolder *hld);
	CHashTable();
	virtual ~CHashTable();
	bool IsEmpty() {return itemcount==0;}

  };

#endif // !defined(AFX_HASHTABLE_H__48213B65_5B53_4F13_B342_F4C11EE38F7F__INCLUDED_)
