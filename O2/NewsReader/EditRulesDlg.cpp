// EditRulesDlg.cpp : implementation file
//

#include "stdafx.h"
#include "newsreader.h"
#include "EditRulesDlg.h"
#include "ContitionsDlg.h"
#include "NewsAccount.h"
#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

using namespace std;
/////////////////////////////////////////////////////////////////////////////
// CEditRulesDlg dialog


CEditRulesDlg::CEditRulesDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CEditRulesDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CEditRulesDlg)
	//}}AFX_DATA_INIT
}


void CEditRulesDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CEditRulesDlg)
	DDX_Control(pDX, IDC_LIST, list);
	DDX_Control(pDX, IDC_MOVEUP, moveup);
	DDX_Control(pDX, IDC_MOVEDOWN, movedown);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CEditRulesDlg, CDialog)
	//{{AFX_MSG_MAP(CEditRulesDlg)
	ON_BN_CLICKED(IDC_MOVEUP, OnMoveup)
	ON_BN_CLICKED(IDC_MOVEDOWN, OnMovedown)
	ON_BN_CLICKED(IDC_NEWRULE, OnNewrule)
	ON_BN_CLICKED(IDC_EDIT, OnEdit)
	ON_LBN_DBLCLK(IDC_LIST, OnDblclkList)
	ON_BN_CLICKED(IDC_DELETE, OnDelete)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CEditRulesDlg message handlers

BOOL CEditRulesDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	moveup.SetIcon(theApp.LoadIcon(IDI_MOVEUP));	
	movedown.SetIcon(theApp.LoadIcon(IDI_MOVEDOWN));
	istrstream in((char *)((LPCTSTR)rules));
	char buff[256];
	int pos;
	while (!in.eof())
	  {
	  buff[0]=0;
	  pos=in.tellg();
	  in.get(buff,sizeof(buff),'~');
	  if (buff[0]==0) break;
	  int i=list.AddString(buff);
	  list.SetItemData(i,pos);
	  in.ignore(0x7FFFFFFF,'\n');
	  }
	list.SetCurSel(0);
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CEditRulesDlg::OnMoveup() 
  {
  int p=list.GetCurSel();
  if (p>0)
	{
	DWORD dta=list.GetItemData(p-1);
	CString s;
	list.GetText(p-1,s);
	list.DeleteString(p-1);
	int it=list.InsertString(p,s);
	list.SetItemData(it,dta);
	}
  }

void CEditRulesDlg::OnMovedown() 
  {
  int p=list.GetCurSel();
  if (p+1<list.GetCount())
	{
	DWORD dta=list.GetItemData(p+1);
	CString s;
	list.GetText(p+1,s);
	list.DeleteString(p+1);
	int it=list.InsertString(p,s);
	list.SetItemData(it,dta);
	}
  }

void CEditRulesDlg::OnNewrule() 
  {
  CContitionsDlg dlg;
  if (dlg.DoModal()==IDOK)
	{
	CString cond;
	CNewsAccount::EnDeCodeCond(cond,dlg.name,dlg.cond,true);
	int l=rules.GetLength();
	rules+=cond+'\n';
	int it=list.AddString(dlg.name);
	list.SetItemData(it,l);
	}  	
  }

void CEditRulesDlg::OnEdit() 
  {
  CContitionsDlg dlg;
  int p=list.GetCurSel();
  if (p<0) return;
  istrstream in((char *)((LPCTSTR)rules));
  int l=list.GetItemData(p);
  in.seekg(l);
  CString s="";
  int i=in.get();
  while(i!=EOF && i!='\n')
	{
	s=s+(char)i;i=in.get();
	}  
  CNewsAccount::EnDeCodeCond(s,dlg.name,dlg.cond,false);
  if (dlg.DoModal()==IDOK)
	{	
	CNewsAccount::EnDeCodeCond(s,dlg.name,dlg.cond,true);
	list.DeleteString(p);
	int l=rules.GetLength();
	rules+=s+'\n';
	int it=list.InsertString(p,dlg.name);
	list.SetItemData(it,l);
	list.SetCurSel(p);
	}
  BuildRules();
  }

void CEditRulesDlg::OnDblclkList() 
  {
  OnEdit();	
  }

void CEditRulesDlg::OnDelete() 
  {
  int p=list.GetCurSel();
  if (p<0) return;
  if (AfxMessageBox(IDS_ASKRULEDELETE,MB_OKCANCEL)==IDCANCEL) return;
  list.DeleteString(p);
  BuildRules();
  }

void CEditRulesDlg::BuildRules()
  {
  CString s;
  int cnt=list.GetCount();
  s="";
  for (int i=0;i<cnt;i++)
  	{
	istrstream in((char *)((LPCTSTR)rules));
	int l=list.GetItemData(i);
	list.SetItemData(i,s.GetLength());
	in.seekg(l);
    int j=in.get();
    while(j!=EOF && i!='\n')
  	  {
	  s=s+(char)j;j=in.get();
	  }
	s=s+'\n';	
	}
  rules=s;
  }

void CEditRulesDlg::OnOK() 
  {
  BuildRules();	
  CDialog::OnOK();
  }
