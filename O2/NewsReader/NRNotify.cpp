// NRNotify.cpp : implementation file
//

#include "stdafx.h"
#include "newsreader.h"
#include "NRNotify.h"
#include "NewsTerminal.h"
#include "NewsArticle.h"
#include "NewsAccount.h"
#include "MsgPrevewDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CNRNotify

CNRNotify::CNRNotify()
{
}

CNRNotify::~CNRNotify()
{
}


BEGIN_MESSAGE_MAP(CNRNotify, CWnd)
	//{{AFX_MSG_MAP(CNRNotify)
	ON_WM_DESTROY()
	ON_MESSAGE(WM_APP,OnShellNotify)
	ON_MESSAGE(NRN_NEWMESSAGE ,OnNRNNewMessage)
	ON_WM_TIMER()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// CNRNotify message handlers

bool CNRNotify::Create(int deficon, int msgicon,int stopicon, CWnd *parent)
  {
  this->deficon=AfxGetApp()->LoadIcon(deficon);
  this->ntficon=AfxGetApp()->LoadIcon(msgicon);
  this->stpicon=AfxGetApp()->LoadIcon(stopicon);
  this->emptyicn=AfxGetApp()->LoadIcon(IDI_EMPTY);
  if (CWnd::Create(NULL,"NewsReaderNotify",0,CRect(0,0,0,0),parent,0)==FALSE)
	{
	NrMessageBox(IDS_NOTIFYFAILED);
	return false;
	}
  memset(hldlist,0,sizeof(hldlist));
  totalcnt=0;
  nid.cbSize=sizeof(nid);
  nid.hWnd=*this;
  nid.uID=sizeof(*this);
  nid.uFlags=NIF_ICON|NIF_MESSAGE|NIF_TIP;
  nid.uCallbackMessage=WM_APP;
  SetTooltipText();
  Shell_NotifyIcon(NIM_ADD,&nid);
  SetTimer(1,5000,NULL);
  blinkevent=-1;
  return true;
  }

void CNRNotify::SetTooltipText()
  {
  char buff[256];
  HICON def=deficon;
  LoadString(AfxGetInstanceHandle(),IDS_NRNOTIFYTOOLTIP,buff,sizeof(buff));
  if (totalcnt==0)
	{
	if (theApp.offline)		
	  {LoadString(AfxGetInstanceHandle(),IDS_NRNWORKOFL,buff,sizeof(buff));def=stpicon;}
	else if (theApp.config.msync==false)
	  {LoadString(AfxGetInstanceHandle(),IDS_NRNSTOPED,buff,sizeof(buff));def=stpicon;}
	}
  nid.hIcon=totalcnt?this->ntficon:def;  
  sprintf(nid.szTip,buff,totalcnt);
  }

void CNRNotify::OnDestroy() 
  {
  Shell_NotifyIcon(NIM_DELETE,&nid);
  for (int i=0;i<MAXINMENU;i++) if (hldlist[i]) {hldlist[i]->Release();acclist[i]->Release();}
  CWnd::OnDestroy();
  // TODO: Add your message handler code here
  }

void CNRNotify::NewArticleIncome(CNewsArtHolder *hlda)
  {
	int i;
  for (i=0;i<MAXINMENU;i++) if (hldlist[i]==NULL) break;
  if (i!=MAXINMENU)
	{
	hldlist[i]=hlda;
	hlda->AddRef();
	acclist[i]=curacc;
	grplist[i]=curgrp;
	if (curacc) curacc->Lock();
	}
  totalcnt++;
  dirtystate=true;
  }

void CNRNotify::DeleteArticle(CNewsArtHolder *hld)
  {
  if (totalcnt==0 && hldlist[0]==NULL) return;
	int i;
  for (i=0;i<MAXINMENU;i++) if (hldlist[i]==hld) break;
  if (i<MAXINMENU)
	{
	hldlist[i]->Release();
	acclist[i]->Release();
	if (i+1<MAXINMENU) 
	  {
	  memcpy(hldlist+i,hldlist+i+1,(MAXINMENU-i-1)*sizeof(*hldlist));
	  memcpy(grplist+i,grplist+i+1,(MAXINMENU-i-1)*sizeof(*grplist));
	  memcpy(acclist+i,acclist+i+1,(MAXINMENU-i-1)*sizeof(*acclist));
	  }
	hldlist[MAXINMENU-1]=NULL;
    if (totalcnt>0)   totalcnt--;else totalcnt=1;
    dirtystate=true;
	}  
  }

LRESULT CNRNotify::OnShellNotify(WPARAM wParam, LPARAM lParam)
  {    
  int i;
  if (lParam==WM_RBUTTONUP)
	{
	CMenu mnu;
	SetForegroundWindow();
	mnu.CreatePopupMenu();
	bool urgent=ContainUrgent();
	for (i=0;i<MAXINMENU;i++) if (hldlist[i] && (!urgent || hldlist[i]->state==EAImportant || grplist[i]->notify))
	  {
	  mnu.AppendMenu(MF_STRING,1000+i,hldlist[i]->GetSubject());
	  acclist[i]->ResetSaveState();
	  }
	mnu.AppendMenu(MF_SEPARATOR,0,(LPCTSTR)0);
	CString nm;
	if (totalcnt)
	  {
	  nm.LoadString(IDS_CATCHUP);
	  mnu.AppendMenu(MF_STRING,1,nm);
	  }
	nm.LoadString(IDS_WORKOFFLINE);
	mnu.AppendMenu(MF_STRING|(theApp.offline?MF_CHECKED:0),3,nm);
	mnu.AppendMenu(MF_SEPARATOR,0,(LPCTSTR)0);
	nm.LoadString(IDS_RESTOREAPP);
	mnu.AppendMenu(MF_STRING,2,nm);
	nm.LoadString(IDS_EXITAPP);
	mnu.AppendMenu(MF_STRING,4,nm);
	CPoint pt;
	GetCursorPos(&pt);
	int id=mnu.TrackPopupMenu(TPM_RIGHTALIGN|TPM_NONOTIFY|TPM_RETURNCMD,pt.x,pt.y,this);
	if (id==0) return 0;    
	else if (id==2) lParam=WM_LBUTTONDBLCLK;
	else if (id==1) Reset();
	else if (id==3) {theApp.offline=!theApp.offline;Repaint();}
	else if (id==4) 	  
	  theApp.m_pMainWnd->PostMessage(WM_CLOSE,0,0);	  
	else
	  {
	  id-=1000;
	  CMsgPrevewDlg *dlg=new CMsgPrevewDlg;
	  dlg->Create(NULL);
	  dlg->LoadContent(acclist[id],grplist[id],hldlist[id]);
	  DeleteArticle(hldlist[id]);
	  }
	OnTimer(0);
	}
   if (lParam==WM_LBUTTONDBLCLK)
	{
	bool urgent=ContainUrgent();
	int i;
	for (i=0;i<MAXINMENU;i++) if (hldlist[i] && (!urgent || hldlist[i]->state==EAImportant || grplist[i]->notify)) break;
	if (i<MAXINMENU)
	  {
	  CFindList fnd;
	  fnd.grp=grplist[i];
	  fnd.hlda=hldlist[i];
      fnd.hlda->AddRef();
	  theApp.m_pMainWnd->SendMessage(WM_APP+1003,0,(LPARAM)(&fnd));
	  }

    if (!theApp.m_pMainWnd->IsWindowVisible()) theApp.m_pMainWnd->ShowWindow(SW_SHOW);
	if (theApp.m_pMainWnd->IsIconic()) theApp.m_pMainWnd->OpenIcon();
	theApp.m_pMainWnd->SetActiveWindow();
	theApp.m_pMainWnd->SetForegroundWindow();	
	}
  return 0;
  }

LRESULT CNRNotify::OnNRNNewMessage(WPARAM wParam, LPARAM lParam)
  {
  switch (wParam)
	{
	case 0:
	  {
	  CNewsArtHolder *hld=(CNewsArtHolder *)lParam;
	  if (hld) 
		{
		NewArticleIncome(hld);	
		hld->Release();
		}
	  break;
	  }
	case 1: curgrp=(CNewsGroup *)lParam;break;
	case 2: curacc=(CNewsAccount *)lParam;break;
	}
  return 0;
  }

void CNRNotify::Reset()
  {
  for (int i=0;i<MAXINMENU;i++) if (hldlist[i]) {hldlist[i]->Release();hldlist[i]=NULL;acclist[i]->Release();}
  totalcnt=0;
  dirtystate=true;
  }
  

void CNRNotify::OnTimer(UINT nIDEvent) 
  {
  if (nIDEvent==blinkevent)
	{
	nid.hIcon=blink?emptyicn:(totalcnt?this->ntficon:(theApp.offline?stpicon:deficon));  
	blink=!blink;
	}
  if (dirtystate)
	{
	SetTooltipText();
	Blink();
	dirtystate=false;
	}
  if (Shell_NotifyIcon(NIM_MODIFY,&nid)==FALSE)
	Shell_NotifyIcon(NIM_ADD,&nid);
  CWnd::OnTimer(nIDEvent);
 }

bool CNRNotify::ContainUrgent()
  {
  	for (int i=0;i<MAXINMENU;i++) if (hldlist[i] && (hldlist[i]->state==EAImportant || grplist[i]->notify)) return true;
	return false;
  }

void CNRNotify::Blink()
  {
  bool urgent=ContainUrgent();
  if (blinkevent==-1 && urgent) blinkevent=SetTimer(2,250,NULL);
  else if (blinkevent!=-1 && !urgent) {KillTimer(blinkevent);blinkevent=-1;}
  }

void CNRNotify::CheckAfterCatchUp()
  {
  CNewsArtHolder *hld;
  bool testcomplette;
  for (int i=0;i<MAXINMENU;i++)
    {
	if (i==0) testcomplette=false;
    hld=hldlist[i];
	if (hld) testcomplette=true;
    if (hld && hld->GetUnread()==false) {DeleteArticle(hld);--i;}
    }
  if (!testcomplette) Reset();
  }
