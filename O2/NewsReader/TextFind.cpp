// TextFind.cpp: implementation of the CTextFind class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "newsreader.h"
#include "TextFind.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////




CTextFind::CTextFind()
{
tree=NULL;
wordtab=NULL;
wpsize=0;
}

CTextFind::~CTextFind()
{
delete tree;
delete wordtab;
}

inline int IsSymb(char p)
  {
  switch (p)
	{
	case '&':
	case '|':return 2;
	case '!':return 1;
	case '(':return 1;
	case ')':return 2;
	}
  return 0;
  }

CTextFind::FindWord *CTextFind::InsertWord(char **text)
  {
  char *beg=*text;
  char *end;
  char *p;  
  bool uhozy=false;
  while (*beg && *beg<33) beg++;
  if (IsSymb(*beg) || *beg==0) 
	{
	*text=beg;return NULL;
	}
  end=p=beg;
  if (*beg=='"') {uhozy=true;p++;end++;}
  while (*p && (uhozy || IsSymb(*p)!=2))
	{
	if (*p=='"') uhozy=false;
	if (*p>32) end=p;
	p++;
	}
  if (*beg=='"' && *end=='"' && beg!=end) {beg++;end--;};
  end++;
  *text=p;
  int sz=end-beg;
  char *nwtab=(char *)realloc(wordtab,wpsize+sz+2);
  if (nwtab==NULL) return NULL;
  wordtab=nwtab;
  char *wps=wordtab+wpsize;
  *wps=-1;
  memcpy(wps+1,beg,sz);
  wps[sz+1]=0;
  FindWord *fw=new FindWord(&wordtab,wpsize);
  wpsize+=sz+2;
  return fw;
  }

inline char GetOp(char **text)
  {
  char c=**text;
  if (c) text[0]++;
  return c;
  }

CTextFind::FindOp * CTextFind::DoOrOp(char **text)
  {
  FindOp *left=DoAndOp(text);
  if (left==NULL) return NULL;
  if (**text!='|') return left;
  GetOp(text);
  FindOp *right=DoOrOp(text);
  if (right==NULL) {delete left;return NULL;}
  return new FindOr(left,right);
  }


CTextFind::FindOp * CTextFind::DoAndOp(char **text)
  {
  FindOp *left=DoNotOp(text);
  if (left==NULL) return NULL;
  if (**text!='&') return left;
  GetOp(text);
  FindOp *right=DoAndOp(text);
  if (right==NULL) {delete left;return NULL;}
  return new FindAnd(left,right);
  }

CTextFind::FindOp * CTextFind::DoNotOp(char **text)
  {
  FindWord *p=InsertWord(text);
  if (p==NULL)
	{
	char c=GetOp(text);
	if (c=='!')
	  {
	  FindOp *p=DoNotOp(text);
	  if (p==NULL) return NULL;
	  return new FindNot(p);
	  }
	else if (c=='(')
	  {
	  FindOp *p=DoOrOp(text);
	  if (GetOp(text)!=')') {delete p;p=NULL;}
	  return p;
	  }
	else return NULL;
	}
  return p;
  }

/*
static bool stristr(const char *source, const char *fnd)
  {
  if (fnd[0]==0) return true;
  do
	{
	while (*source && toupper(czmap(*source))!=(toupper (czmap(*fnd)))) source++;
	if (*source)
	  {
	  int q=1;
	  while (fnd[q] && source[q]) 
		if (toupper(czmap(source[q]))!=toupper(czmap(fnd[q]))) break;else q++;
	  if (fnd[q]==0) return true;
	  source++;
	  }
	}
  while (*source);
  return false;
  }
*/

void CTextFind::FindInText(char *wpst, const char *text)
  {
  char *q=wpst+1;
  *wpst=stristr(text,q)!=0;
  }

bool CTextFind::CompileCriteria(const char *criteria)
  {
  delete tree;
  delete wordtab;
  wpsize=0;
  wordtab=NULL;
  tree=DoOrOp((char **)&criteria);
  if (tree==NULL || *criteria) {delete tree;tree=NULL;return false;}
  return true;
  }

bool CTextFind::Find(const char *intext)
  {
  if (tree==NULL) return true;
  tree->Calculate(NULL);
  return tree->Calculate(intext);
  }
