#if !defined(AFX_THREADPROPDLG_H__27286DA5_1193_4384_8BAF_5FABEE170B53__INCLUDED_)
#define AFX_THREADPROPDLG_H__27286DA5_1193_4384_8BAF_5FABEE170B53__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ThreadPropDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CThreadPropDlg dialog
#define CTPD_LINECOUNT 15


class CThreadPropDlg : public CDialog
  {
  // Construction
  
  
  struct TValue
    {
    CString value;
    TValue *next;
    bool nostore; //don't store it
    TValue(const char *name, bool nostore=false):value(name),nostore(nostore) 
      {next=NULL;}	
    };
  struct TName
    {
    CString name;
    TName *next;
    TValue *values;
    TName(const char *name, TValue *val=NULL):name(name) 
      {next=NULL;values=val;}
    ~TName() 
      {while (values) 
        {TValue *x=values;values=values->next;delete x;};delete next;}
    TName *Find(const char *name)
      {TName *t=this;while (t && stricmp(t->name,name)) t=t->next;return t;}
    TValue *FindValues(const char *name)
      {TName *t=Find(name);return (t==NULL)?NULL:t->values;}
    static void Add(TName **list, const char *name, const char *value,bool nostore=false)
      {
      TName *x=(*list)->Find(name);if (x==NULL) 
        {x=new TName(name,NULL);x->next=*list;*list=x;}
      TValue *y=x->values;
      if (y==NULL) x->values=new TValue(value,nostore);
      else 
        {
        while (y->next!=NULL && stricmp(y->value,value)) y=y->next;
        if (stricmp(y->value,value)) y->next=new TValue(value,nostore);
        }
      }
    };
  bool update;
  int lastid;
  public:
    void HideLines();
    static CString BuildProperties(CStringArray& array);
    static void ParseProperties(const char *line,CStringArray &array);
    ~CThreadPropDlg();
    void LoadValuesCombo(CComboBox& cb, const char *name, bool reset=true);
    void LoadNamesCombo(CComboBox& cb);
    void SaveValues(const char *filename);
    void LoadValues(const char *file, bool global);
    CThreadPropDlg(CWnd* pParent = NULL);   // standard constructor
    TName *vallist;
    bool load;
    
    CString properties;
    
    // Dialog Data
    //{{AFX_DATA(CThreadPropDlg)
    enum 
      { IDD = IDD_THREADPROP };
    BOOL	comment;
    //}}AFX_DATA
    CComboBox	wVarible[CTPD_LINECOUNT];
    CComboBox	wValue[CTPD_LINECOUNT];
    CString	value[CTPD_LINECOUNT];
    CString	varible[CTPD_LINECOUNT];
    
    
    // Overrides
    // ClassWizard generated virtual function overrides
    //{{AFX_VIRTUAL(CThreadPropDlg)
  protected:
    virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
    //}}AFX_VIRTUAL
    
    // Implementation
  protected:
    void ParseProperties();
    
    // Generated message map functions
    //{{AFX_MSG(CThreadPropDlg)
    virtual BOOL OnInitDialog();
    afx_msg void OnTimer(UINT nIDEvent);
    virtual void OnOK();
    afx_msg void OnDelpair();
    afx_msg void OnEditchangeVarible1();
    afx_msg void OnEditupdateVarible1();
    //}}AFX_MSG
    DECLARE_MESSAGE_MAP()
    private:
    void SyncLists();
    void SyncLists(int id);
  };

//--------------------------------------------------

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_THREADPROPDLG_H__27286DA5_1193_4384_8BAF_5FABEE170B53__INCLUDED_)
