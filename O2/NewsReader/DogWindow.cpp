// DogWindow.cpp : implementation file
//

#include "stdafx.h"
#include "newsreader.h"
#include "DogWindow.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDogWindow

CDogWindow *statusinstance=NULL;

CDogWindow::CDogWindow()
{
ResetProgress();
}

CDogWindow::~CDogWindow()
{
if (statusinstance==this) statusinstance=NULL;
}


BEGIN_MESSAGE_MAP(CDogWindow, CWnd)
	//{{AFX_MSG_MAP(CDogWindow)
	ON_WM_PAINT()
	ON_WM_TIMER()
	ON_WM_LBUTTONDOWN()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


/////////////////////////////////////////////////////////////////////////////
// CDogWindow message handlers

void CDogWindow::Create(CWnd *parent, CRect &rc, UINT nID)
  {
  CWnd::Create(NULL,"",WS_CHILD|WS_VISIBLE,rc,parent,nID);
  anims[0].Create(IDB_DOGANIM1,50,1,RGB(255,0,255));
  anims[1].Create(IDB_DOGANIM2,50,1,RGB(255,0,255));
  anims[2].Create(IDB_DOGANIM3,50,1,RGB(255,0,255));
  anims[3].Create(IDB_DOGANIM4,50,1,RGB(255,0,255));
  anims[4].Create(IDB_DOGANIM5,50,1,RGB(255,0,255));
  anim=2;
  frame=0;
  repeat=true;
  end=false;
  SetTimer(100,200,NULL);
  curspeed=speed=1;
  timeout=100;
  errtimeout=0;
  base.CreateFont(-10,0,0,0,400,FALSE,FALSE,FALSE,EASTEUROPE_CHARSET,0,0,0,0,"MS Sans Sherif");
  }

void CDogWindow::OnPaint() 
  {   
  CPaintDC dc(this); // device context for painting
  CFont *old;
  CRect rc;
  old=dc.SelectObject(&base);
  GetClientRect(&rc);  
  if (dc.m_ps.fErase)
	dc.FillSolidRect(rc,GetSysColor(COLOR_BTNFACE));
  DrawDog(&dc);
  int xbeg=rc.left;
  int xend=rc.right;
  rc.left+=60;
  dc.SetTextColor(GetSysColor(COLOR_BTNTEXT));
  if (anim==4)
	dc.DrawTextA(error,error.GetLength(),&rc,DT_WORDBREAK);
  else
	dc.DrawTextA(status,status.GetLength(),&rc,DT_WORDBREAK);
  dc.SelectObject(old);
  if (progresspos>0 && progressmax>0)
	{
	CPen pen(PS_SOLID,3,GetSysColor(COLOR_BTNTEXT));
	CPen *oldpen=dc.SelectObject(&pen);
	int len;
	if (progressmax>10000) len=progresspos/(progressmax/(xend-xbeg));
	else len=(progresspos*(xend-xbeg))/progressmax;
	dc.MoveTo(xbeg,rc.bottom-2);
	dc.LineTo(xbeg+len,rc.bottom-2);
	dc.SelectObject(oldpen);
	}
  
  }

void CDogWindow::OnTimer(UINT nIDEvent) 
  {
  curspeed--;
  if (!curspeed)
	{
	frame++;
	int ncount=anims[anim].GetImageCount();
	if (frame>=ncount)
	  if (repeat) frame=0;
	  else {frame=ncount-1;end=true;}
	curspeed=speed;
	DrawDog(NULL);
	InvalidateProgress();
	}
  if (errtimeout) errtimeout--;
  else error="";
	if (timeout==0) 
	  if (error!="")
		{
		PlayAnim(4,5);
		ResetProgress();
		}
	  else
		{
		PlayAnim(1,10);
		ResetProgress();
		}
	else	
	  timeout--;
  }

bool CDogWindow::PlayAnim(int anim,int speed)
  {
  if (errtimeout) return false;
  this->speed=speed;
  if (this->anim==anim && !end) return false;
  frame=0;
  this->anim=anim;
  this->curspeed=1;
  repeat=anim!=0;
  timeout=100;
  end=false;
  return true;
  }

void CDogWindow::SetStatus(const char *text)
  {
  status=text;
  if (anim==1) PlayAnim(2);
  timeout=100;
  Invalidate(TRUE);
  }


void CDogWindow::SetError(const char *error)
  {
  this->error=error;
  anim=4;
  speed=2;
  errtimeout=100;
  repeat=true;
  MessageBeep(MB_ICONEXCLAMATION);
  Invalidate(TRUE);
  }

void CDogWindow::OnLButtonDown(UINT nFlags, CPoint point) 
  {
  if (error!="")
	{
	NrMessageBox(error,MB_OK);
	errtimeout=0;
	PlayAnim(2);
	error="";
	}
  }

void DogShowError(const char *error)
  {
  if (statusinstance)
	statusinstance->SetError(error);
  else
	NrMessageBox(error,MB_OK);
  }

void DogShowStatusV(const char *text,...)
  {
  va_list arg;
  va_start(arg,text);
  CString s;s.FormatV(text,arg);
  DogShowStatus(s);
  }

void DogShowStatusV(int ids,...)
  {
  CString s;s.LoadString(ids);
  va_list arg;
  va_start(arg,ids);
  CString t;t.FormatV(s,arg);
  DogShowStatus(t);
  }



void DogSetStatusWindow(CDogWindow *status)
  {
  statusinstance=status;
  }

void DogShowStatus(const char *text)
  {
  if (statusinstance)
	{
	statusinstance->SetStatus(text);
	}  
  }

void DogUpdateWindow()
  {
  statusinstance->UpdateWindow();
  }

void DogShowErrorV(const char *error,...)
  {
  va_list arg;
  va_start(arg,error);
  CString s;s.FormatV(error,arg);
  DogShowError(s);
  }

void DogShowErrorV(int ids,...)
  {
  CString s;s.LoadString(ids);
  va_list arg;
  va_start(arg,ids);
  CString t;t.FormatV(s,arg);
  DogShowError(t);
  }

void DogSetPos(int pos)
  {
  statusinstance->SetProgressPos(pos);
  }

void DogSetMax(int max)
  {
  statusinstance->SetProgressMax(max);
  }

void DogResetProgress()
  {
  statusinstance->ResetProgress();
  }

void CDogWindow::DrawDog(CDC *dc)
  {
  bool rels=false;
  if (dc==NULL) {dc=GetDC();rels=true;}
  anims[anim].SetBkColor(GetSysColor(COLOR_3DFACE));
  anims[anim].Draw(dc,frame,CPoint(0,0),ILD_NORMAL);
  if (rels) {ReleaseDC(dc);}
  }

void CDogWindow::ResetProgress()
  {
  progressmax=0;
  progresspos=0;
  if (this->m_hWnd) InvalidateProgress();
  }

void CDogWindow::SetProgressMax(int max)
  {
  progressmax=max;
  progresspos=0;
  progresslk=GetCurrentThreadId();
  progresstm=GetTickCount();
  if (this->m_hWnd) InvalidateProgress();
  }

void CDogWindow::SetProgressPos(int pos)
  {
  if (GetTickCount()-progresstm>1000 && GetCurrentThreadId()==progresslk)
	{
	progresspos=pos;
	}
  }





void CDogWindow::InvalidateProgress()
{
  CRect rc;
  GetClientRect(&rc);  
  rc.top=rc.bottom-3;
  InvalidateRect(&rc);
}
