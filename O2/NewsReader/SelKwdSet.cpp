// SelKwdSet.cpp : implementation file
//

#include "stdafx.h"
#include "newsreader.h"
#include "SelKwdSet.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSelKwdSet dialog


CSelKwdSet::CSelKwdSet(CWnd* pParent /*=NULL*/)
	: CDialog(CSelKwdSet::IDD, pParent)
{
	//{{AFX_DATA_INIT(CSelKwdSet)
	kwList = _T("");
	//}}AFX_DATA_INIT
}


void CSelKwdSet::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CSelKwdSet)
	DDX_Control(pDX, IDC_LIST, wKwList);
	DDX_LBString(pDX, IDC_LIST, kwList);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CSelKwdSet, CDialog)
	//{{AFX_MSG_MAP(CSelKwdSet)
	ON_LBN_DBLCLK(IDC_LIST, OnDblclkList)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSelKwdSet message handlers

BOOL CSelKwdSet::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	char *c=new char[kwrds.GetLength()+1];
	strcpy(c,kwrds);
	char *d=c;
	char *e=strchr(d,'\n');
	while (e)
	  {
	  if (e[-1]='\r') e[-1]=0;
	  e[0]=0;
	  wKwList.AddString(d);
	  d=e+1;
	  e=strchr(d,'\n');
	  }
    wKwList.AddString(d);
	wKwList.SetCurSel(cursel);
	delete [] c;
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CSelKwdSet::OnDblclkList() 
  {
  OnOK();
  }

void CSelKwdSet::OnOK()
  {
  int i=wKwList.GetCurSel();
  if (i<0) return;
  cursel=i;
  EndDialog(IDOK);
  }
