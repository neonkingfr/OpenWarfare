#if !defined(AFX_THREADPROPEXDLG_H__417149BC_1125_4E8E_8848_30398A26E2B5__INCLUDED_)
#define AFX_THREADPROPEXDLG_H__417149BC_1125_4E8E_8848_30398A26E2B5__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ThreadPropExDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CThreadPropExDlg dialog

#define CTPD_LINECOUNT 15

#define SThreadProp_VALSNAME1 "thrdvals.ini"
#define SThreadProp_VALSNAME2 "thrdvalsg.ini"

#define SDM_READJUSTWINDOW (WM_APP+1212)


  struct SThreadProp_TValue
    {
    CString value;
    SThreadProp_TValue *next;
    bool nostore; //don't store it
    SThreadProp_TValue(const char *name, bool nostore=false):value(name),nostore(nostore) 
      {next=NULL;}	
    };
  struct SThreadProp_TName
    {
    CString name;
    SThreadProp_TName *next;
    SThreadProp_TValue *values;
    SThreadProp_TName(const char *name, SThreadProp_TValue *val=NULL):name(name) 
      {next=NULL;values=val;}
    ~SThreadProp_TName() 
      {while (values) 
        {SThreadProp_TValue *x=values;values=values->next;delete x;};delete next;}
    SThreadProp_TName *Find(const char *name)
      {SThreadProp_TName *t=this;while (t && stricmp(t->name,name)) t=t->next;return t;}
    SThreadProp_TValue *FindValues(const char *name)
      {SThreadProp_TName *t=Find(name);return (t==NULL)?NULL:t->values;}
    static void Add(SThreadProp_TName **list, const char *name, const char *value,bool nostore=false)
      {
      SThreadProp_TName *x=(*list)->Find(name);if (x==NULL) 
        {x=new SThreadProp_TName(name,NULL);x->next=*list;*list=x;}
      SThreadProp_TValue *y=x->values;
      if (y==NULL) x->values=new SThreadProp_TValue(value,nostore);
      else 
        {
        while (y->next!=NULL && stricmp(y->value,value)) y=y->next;
        if (stricmp(y->value,value)) y->next=new SThreadProp_TValue(value,nostore);
        }
      }
    public:
	    static void Load(SThreadProp_TName **list, const char *file, bool global);
	    static void Save(SThreadProp_TName *list, const char *file);
    };
 

class CThreadPropExDlg : public CDialog
{
  // Construction
  SThreadProp_TName *vallist;
  bool load;
  CSize _originsz;
  CSize _orwndsz;
  bool _yresize;
  int _lasty;
  bool _update;
  bool _delet;

  void SyncLists(int id);
  void LoadNamesCombo(CComboBox &cb);
  void LoadValuesCombo(CComboBox &cb, const char *name, bool reset=true);


public:
	~CThreadPropExDlg();
	bool Save(bool saveini);
	void OnCancel();
	void OnOK();
	int GetDefaultHeight();
	void AdjustYSize();
	void HideLines();
	void OnEditupdateVarible1();
	void OnEditchangeVarible1();
	int GetDefaultWidth();
	void PlaceRight(CPoint &pt,int ys);
	void PlaceBottom(CPoint &pt, int xs);
	void PlaceLeft(CPoint &pt, int ys); ///<pt point of left-upper point, ys - length);
	void MoveTwin(int twin, CPoint pt, HDWP dwp);
	void RecalcLayout(int cx, int cy);
	CRect GetTwinRect(int twin);
	CThreadPropExDlg(CWnd* pParent = NULL);   // standard constructor
    void ParseProperties(const char *line, CStringArray &array);
    CString BuildProperties(CStringArray &array);
    void ParseProperties();
    void SyncLists();

// Dialog Data
	//{{AFX_DATA(CThreadPropExDlg)
	enum { IDD = IDD_THREADPROPEX };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA

    CComboBox	wVarible[CTPD_LINECOUNT];
    CComboBox	wValue[CTPD_LINECOUNT];
    CString	value[CTPD_LINECOUNT];
    CString	varible[CTPD_LINECOUNT];
    CString properties;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CThreadPropExDlg)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CThreadPropExDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnSize(UINT nType, int cx, int cy);
	afx_msg void OnPaint();
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnDestroy();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_THREADPROPEXDLG_H__417149BC_1125_4E8E_8848_30398A26E2B5__INCLUDED_)
