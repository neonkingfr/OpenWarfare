#if !defined(AFX_CONFIGDLG_H__FF690463_1FB4_465B_95B5_382F4F053C18__INCLUDED_)
#define AFX_CONFIGDLG_H__FF690463_1FB4_465B_95B5_382F4F053C18__INCLUDED_

#include "IEDispPropDlg.h"	// Added by ClassView
#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ConfigDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CConfigDlg dialog

#include "Resource.h"

class CConfigDlg : public CDialog
{
// Construction
	COLORREF highlighted; //highlighted color;
	COLORREF important;	//important color;
public:
	DWORD usercolors[16]; //list of user defined colors

	CIEDispPropDlg dispprop;
	void SaveConfig();
	void LoadConfig();
	CConfigDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CConfigDlg)
	enum { IDD = IDD_CFGDLG };
	CButton	bUserProf;
	CSliderCtrl	wcache;
	CSliderCtrl	wacc_cmd;
	BOOL	autoRead;
	BOOL	newExpand;
	BOOL	showLines;
	BOOL	async;
	BOOL	msync;
	UINT	syncmin;
	int		acc_cmd;
	int		cache;
	BOOL	shwimgs;
	BOOL	showCancel;
	BOOL	betweenThreads;
	BOOL	newontop;
	UINT	dwLines;
	BOOL	lgWarm;
	BOOL	delcheck;
	BOOL	owned;
	BOOL	nrnotify;
	BOOL	bigicons;
	BOOL	maxdownload;
	UINT	maxcount;
	BOOL	userprof;
	BOOL	sentread;
	BOOL	shwhtml;
	BOOL	useoe;
	BOOL	grayIcons;
	//}}AFX_DATA

	COLORREF col_highlighted;
	COLORREF col_important;
	

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CConfigDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CConfigDlg)
	afx_msg void OnMsync();
	virtual BOOL OnInitDialog();
	afx_msg BOOL OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message);
	afx_msg void OnShowlines();
	afx_msg void OnLargewarm();
	afx_msg void OnBpreview();
	afx_msg void OnMaxdownload();
	afx_msg void OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct);
	afx_msg void OnChangeColor();
	afx_msg void OnRegisternr();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CONFIGDLG_H__FF690463_1FB4_465B_95B5_382F4F053C18__INCLUDED_)
