#if !defined(AFX_MSGBOX_H__FE165172_EBF6_4D9D_BAE6_54902AAB8F1F__INCLUDED_)
#define AFX_MSGBOX_H__FE165172_EBF6_4D9D_BAE6_54902AAB8F1F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// MsgBox.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CMsgBox dialog

class CMsgBox : public CDialog
{
// Construction
public:
	CMsgBox(CWnd* pParent = NULL);   // standard constructor
	struct _tagFlags
	  {
	  unsigned okbutt:1;
	  unsigned cancelbutt:1;
	  unsigned yesbutt:1;
	  unsigned nobutt:1;
	  unsigned noagain:1;
	  }Flags;	
	DWORD msgBeep;
// Dialog Data
	//{{AFX_DATA(CMsgBox)
	enum { IDD = IDD_MSGBOX };
	CButton	noagainbutt;
	CButton	cancel;
	CButton	no;
	CButton	ok;
	CButton	yes;
	BOOL	noagain;
	CString	message;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMsgBox)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CMsgBox)
	virtual void OnCancel();
	afx_msg void OnNo();
	virtual void OnOK();
	afx_msg void OnYes();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MSGBOX_H__FE165172_EBF6_4D9D_BAE6_54902AAB8F1F__INCLUDED_)
