// MessagePartCache.cpp: implementation of the CMessagePartCache class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "newsreader.h"
#include "MessagePartCache.h"
#include "MessagePart.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

static bool InitNRTemp()
{
  char buff[4*MAX_PATH];
  ::GetTempPath(sizeof(buff),buff);
  strcat(buff,"NRTEMP_*");
  CFileFind fnd;
  BOOL f=fnd.FindFile(buff);
  while (f)
  {
	f=fnd.FindNextFile();
	if (fnd.IsDirectory()) 
	{
	  CString folder=fnd.GetFilePath();
	  {
	  CFileFind fenum;
	  BOOL g=fenum.FindFile(folder+"\\*.*");
	  while (g)
	  {
		g=fenum.FindNextFile();
		if (!fenum.IsDots() && !fenum.IsDirectory()) 
		  DeleteFile(fenum.GetFilePath());
	  }
	  }
	  RemoveDirectory(folder);
	}
  }
  return true;
}

CMessagePartCache::CMessagePartCache()
  {
   memset(cache,0,sizeof(cache)); 
  _globcntr=1;
  _curcachesize=32;
  }

CMessagePartCache::~CMessagePartCache()
  {
  CSingleLock(&_lock,TRUE);
  for (int i=0;i<_curcachesize;i++) RemoveFromCache(i);  
  }


void CMessagePartCache::AddToCache(CMessagePart *message,unsigned long owner)
  {
  CSingleLock(&_lock,TRUE);
  int fnd=FindObject(owner);
  if (fnd==-1)
    {
    fnd=FindEmptyPos();
    while (fnd==-1)
      {
      ASSERT(_curcachesize+1<CMC_MAXCACHESIZE);
      _curcachesize++;
      fnd=FindEmptyPos();
      }
    RemoveFromCache(fnd);
    cache[fnd].lru_info=++_globcntr;
    cache[fnd].msg=message;
    cache[fnd].owner=owner;
    }
  else
    {
    if (cache[fnd].msg!=message)
      {
      delete cache[fnd].msg;
      cache[fnd].msg=message;
      cache[fnd].lru_info=++_globcntr;
      }
    }
  }

void CMessagePartCache::RemoveFromCache(int index)
  {
  CSingleLock(&_lock,TRUE);
  delete cache[index].msg;
  memset(cache+index,0,sizeof(cache[index]));
  }

int CMessagePartCache::FindEmptyPos()
  {
  CSingleLock(&_lock,TRUE);
  unsigned long best=0;
  unsigned long pos=-1;
  for (int i=0;i<_curcachesize;i++) if (cache[i].locks==0)
    {
    unsigned long value=_globcntr-cache[i].lru_info;
    if (value>best) 
      {
      best=value;
      pos=i;
      }
    }
  return pos;
  }

int CMessagePartCache::FindObject(unsigned long owner)
  {
  CSingleLock(&_lock,TRUE);
  for (int i=0;i<_curcachesize;i++)
    {
    if (cache[i].owner==owner) return i;
    }
  return -1;
  }

void CMessagePartCache::LockObject(int index)
  {
  CSingleLock(&_lock,TRUE);
  cache[index].locks++;  
  cache[index].lru_info=++_globcntr;
  }

void CMessagePartCache::UnlockObject(int index)
  {
  CSingleLock(&_lock,TRUE);
  ASSERT(cache[index].locks);
  if (cache[index].locks)  cache[index].locks--;    
  if ((index>=_curcachesize || cache[index].owner==0) && cache[index].locks==0 )
    RemoveFromCache(index);
  }


PMessagePart CMessagePartCache::LockObject(unsigned long owner)
  {
  CSingleLock(&_lock,TRUE);
  int fnd=FindObject(owner);
  if (fnd==-1) 
    return PMessagePart(NULL,NULL,0);
  else
    {
    LockObject(fnd);
    return PMessagePart(&cache[fnd].msg,this,fnd);
    }
  }

PMessagePart::PMessagePart(const PMessagePart &other)
  {
  _msg=other._msg;
  _cache=other._cache;
  _index=other._index;
  _cache->LockObject(_index);
  }

PMessagePart &PMessagePart::operator=(const PMessagePart &other)
  {
  if (other._msg!=_msg)
    {
    if (!IsNull())
      {
      _cache->UnlockObject(_index);
      }
    _msg=other._msg;
    _cache=other._cache;
    _index=other._index;
    _cache->LockObject(_index);
    }
  return *this;
  }

PMessagePart::~PMessagePart()
  {
  if (!IsNull())
    {
    _cache->UnlockObject(_index);
    }
  }

void CMessagePartCache::SetCacheSize(int items)
  {
  ASSERT(items>0 && items<=CMC_MAXCACHESIZE);
  _curcachesize=items;  
  }

void CMessagePartCache::Reset(unsigned long owner)
  {
  int fnd=FindObject(owner);
  if (fnd==-1) return;
  cache[fnd].owner=0;
  }

void CMessagePartCache::FlushCache()
{
  CSingleLock(&_lock,TRUE);
  for (int i=0;i<_curcachesize;i++) 
  {
	if (cache[i].locks==0) RemoveFromCache(i);  
	else cache[i].owner=0;
  }
}

void CMessagePartCache::InitCache()
{
  InitNRTemp();
}