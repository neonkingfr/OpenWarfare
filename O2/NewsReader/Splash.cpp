// Splash.cpp : implementation file
//

#include "stdafx.h"
#include "newsreader.h"
#include "Splash.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CSplash

IMPLEMENT_DYNCREATE(CSplash, CWinThread)

CSplash::CSplash()
{
splashout=false;
kill=false;
}

CSplash::~CSplash()
{
if (GetCurrentThreadId()!=m_nThreadID)
  {  
  kill=true;
  WaitForSingleObject(m_hThread,INFINITE);
  }
}

BOOL CSplash::InitInstance()
{
TRACE0("Starting splash screen\n");
if (splashout) spout.Create();
else spwnd.Create();
m_pMainWnd=NULL;
return TRUE;
}

int CSplash::ExitInstance()
{
if (splashout) spout.DestroyWindow();
else 
  spwnd.Close();
TRACE0("Exiting splash screen\n");
	return CWinThread::ExitInstance();
}

BEGIN_MESSAGE_MAP(CSplash, CWinThread)
	//{{AFX_MSG_MAP(CSplash)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CSplash message handlers

BOOL CSplash::OnIdle(LONG lCount)
  {
  if (kill) 
	PostQuitMessage(0);
  return FALSE;
  }
