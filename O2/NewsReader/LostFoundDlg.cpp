// LostFoundDlg.cpp : implementation file
//

#include "stdafx.h"
#include "newsreader.h"
#include "LostFoundDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CLostFoundDlg dialog


CLostFoundDlg::CLostFoundDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CLostFoundDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CLostFoundDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CLostFoundDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CLostFoundDlg)
	DDX_Control(pDX, IDC_PREVIEW, preview);
	DDX_Control(pDX, IDC_LIST, list);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CLostFoundDlg, CDialog)
	//{{AFX_MSG_MAP(CLostFoundDlg)
	ON_NOTIFY(LVN_ITEMCHANGED, IDC_LIST, OnItemchangedList)
	ON_NOTIFY(NM_DBLCLK, IDC_LIST, OnDblclkList)
	ON_WM_SETCURSOR()
	ON_WM_LBUTTONDOWN()
	ON_WM_LBUTTONUP()
	ON_WM_MOUSEMOVE()
	ON_WM_CTLCOLOR()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CLostFoundDlg message handlers


BOOL CLostFoundDlg::OnInitDialog() 
  {
	CDialog::OnInitDialog();
	
	snd=new CSendDlg;
	CFileFind ff;
	CString nn;
	CRect rc;
	list.GetClientRect(&rc);
	rc.left=rc.right/2;
	rc.right-=GetSystemMetrics(SM_CXVSCROLL)+GetSystemMetrics(SM_CXEDGE);
	nn.LoadString(IDS_TEXTSUBJECT);list.InsertColumn(0,nn,LVCFMT_LEFT,rc.left,0);
	nn.LoadString(IDS_GROUPTO);list.InsertColumn(1,nn,LVCFMT_LEFT,rc.right-rc.left,1);
	list.InsertColumn(2,"",LVCFMT_LEFT,0,2);
	list.SetImageList(&theApp.ilist,LVSIL_SMALL);
	for (BOOL q=ff.FindFile(theApp.FullPath("*.msg"));q;)
	  {
	  q=ff.FindNextFile();
	  CString s=ff.GetFilePath();
	  const char *sp=strrchr(s,'\\');
	  if (sp==NULL) sp=(char *)((LPCTSTR)s);else sp++;
	  snd->LoadMessage(s);
	  int icon;	  
	  if (strncmp(sp,"send",4)==0) icon=ICON_SENDLATER;else icon=ICON_INCACHE;
	  int z=list.InsertItem(0,snd->subject,icon);
	  list.SetItemText(z,1,snd->to);
	  list.SetItemText(z,2,ff.GetFilePath());
	  }
	list.SetExtendedStyle(list.GetExtendedStyle()|LVS_EX_FULLROWSELECT|(theApp.config.showLines?LVS_EX_GRIDLINES:0));
	ff.Close();
	br.CreateSolidBrush(GetSysColor(COLOR_WINDOW));
  	return TRUE;  // return TRUE unless you set the focus to a control
             // EXCEPTION: OCX Property Pages should return FALSE
  }

void CLostFoundDlg::OnItemchangedList(NMHDR* pNMHDR, LRESULT* pResult) 
  {
	NMLISTVIEW* pNMListView = (NMLISTVIEW*)pNMHDR;
	
	if (pNMListView->uNewState & LVIS_FOCUSED)
	  {
	  CString q=list.GetItemText(pNMListView->iItem,2);
	  snd->LoadMessage(q);
	  preview.SetWindowText(snd->msgbody);
	  }
	*pResult = 0;
  }

void CLostFoundDlg::OnCancel()
  {
  delete snd;
  CDialog::OnCancel();
  }

void CLostFoundDlg::OnDblclkList(NMHDR* pNMHDR, LRESULT* pResult) 
  {
  int i=list.GetNextItem(-1,LVNI_FOCUSED);
  OpenMessage(i);
  *pResult = 0;
  }

BOOL CLostFoundDlg::PreTranslateMessage(MSG* pMsg) 
  {
  if (pMsg->message==WM_KEYDOWN)	
	{
	switch (pMsg->wParam)
	  {
	  case 13:OpenMessage(list.GetNextItem(-1,LVNI_FOCUSED));return TRUE;
	  case 46: DeleteMessage(list.GetNextItem(-1,LVNI_FOCUSED));return TRUE;
	  }
	}
  return CDialog::PreTranslateMessage(pMsg);
  }

void CLostFoundDlg::OpenMessage(int i)
  {
  if (i==-1) return;
  CString q=list.GetItemText(i,2);
  snd->msgbody="";
  snd->Create();
  MSG msg;
  while (::PeekMessage(&msg,0,0,0,PM_REMOVE)) DispatchMessage(&msg);
  snd->LoadMessage(q);
  snd=NULL;
  PostMessage(WM_COMMAND,IDCANCEL,0);  
  }

void CLostFoundDlg::DeleteMessage(int i)
  {
  if (i==-1) return;
  if (NrMessageBox(IDS_ASKDELETEMSG,MB_YESNO|MB_ICONQUESTION)==IDNO) return;
  CString q=list.GetItemText(i,2);
  DeleteFile(q);
  list.DeleteItem(i);
  }

bool CLostFoundDlg::IsSeparator(CPoint &pt)
  {
  CRect r1,r2;
  list.GetWindowRect(&r1);ScreenToClient(&r1);
  preview.GetWindowRect(&r2);ScreenToClient(&r2);
  return (pt.y>=r1.bottom && pt.y<=r2.top);
  }

BOOL CLostFoundDlg::OnSetCursor(CWnd* pWnd, UINT nHitTest, UINT message) 
  {
  if (pWnd==this)
	{
	CPoint pt(GetMessagePos());
	ScreenToClient(&pt);
	if (IsSeparator(pt))
	  {
	  SetCursor(LoadCursor(NULL,IDC_SIZENS));
	  return TRUE;
	  }
	}
  return CDialog::OnSetCursor(pWnd, nHitTest, message);
  }

void CLostFoundDlg::OnLButtonDown(UINT nFlags, CPoint point) 
{
	if (IsSeparator(point))
	  {
	  lasty=point.y;
	  SetCapture();
	  }
	CDialog::OnLButtonDown(nFlags, point);
}

void CLostFoundDlg::OnLButtonUp(UINT nFlags, CPoint point) 
{
	if (GetCapture()==this) 
	  ReleaseCapture();
	
	CDialog::OnLButtonUp(nFlags, point);
}

void CLostFoundDlg::OnMouseMove(UINT nFlags, CPoint point) 
{
	if (GetCapture()==this) 
	  {
	  CRect r1,r2;
	  list.GetWindowRect(&r1);ScreenToClient(&r1);
	  preview.GetWindowRect(&r2);ScreenToClient(&r2);
	  r1.bottom+=point.y-lasty;
	  r2.top+=point.y-lasty;
	  if (r1.bottom-r1.top>10 && r2.bottom-r2.top>10)
		{
		list.MoveWindow(&r1);
		preview.MoveWindow(&r2);
		lasty=point.y;
		}
	  }
	
	CDialog::OnMouseMove(nFlags, point);
}

HBRUSH CLostFoundDlg::OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor) 
{
	HBRUSH hbr = CDialog::OnCtlColor(pDC, pWnd, nCtlColor);
	
	if (pWnd==&preview) 
	  {
	  hbr=br;	
	  pDC->SetBkColor(GetSysColor(COLOR_WINDOW));
	  }
	// TODO: Return a different brush if the default is not desired
	return hbr;
}
