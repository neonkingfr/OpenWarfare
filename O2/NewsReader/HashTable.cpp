// HashTable.cpp: implementation of the CHashTable class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "newsreader.h"
#include "HashTable.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CHashTable::CHashTable()
  {
  hldlist=NULL;
  tblsize=0;
  itemcount=0;
  }

CHashTable::~CHashTable()
  {
  delete [] hldlist;
  }

class CMySingleLock
  {
  CSyncObject *guard;
  public:
    CMySingleLock(CSyncObject *obj)
      {
      obj->Lock();
      guard=obj;
      }
    ~CMySingleLock()
      {
      guard->Unlock();
      }
  };

bool CHashTable::Expand()
  {
  CMySingleLock sl(&locker);
  DWORD oldsz=tblsize;
  if (tblsize<101) tblsize=101;else tblsize=tblsize*2-1;
  CNewsArtHolder **oldptr=hldlist;
  hldlist=new CNewsArtHolder *[tblsize];
  if (hldlist==NULL) 
	{
	hldlist=oldptr;
	tblsize=oldsz;
	return false;
	}
  memset(hldlist,0,sizeof(CNewsArtHolder *)*tblsize);
  for (DWORD i=0;i<oldsz;i++) if (oldptr[i])
	_Add(oldptr[i]);
  delete [] oldptr;
  return true;
  }

bool CHashTable::_Add(CNewsArtHolder *hld)
  {    
  if (hld==NULL) return false;
  if (_Find(hld->messid)!=NULL) return true;
  if (itemcount+(itemcount>>2)>=tblsize) 
	if (Expand()==false) return false;
  DWORD hash=HashName(hld->messid,tblsize);
  while (hldlist[hash]!=NULL) 
	{
	hash++;
	if (hash>=tblsize) hash-=tblsize;
	}
  hldlist[hash]=hld;
  itemcount++;
  return true;
  }

DWORD CHashTable::HashName(const char *name, DWORD module)
  {
  DWORD suma=0;
  while (*name)
	{
	if (*name!='<' && *name!='>')
	  {
	  DWORD val=(*(const unsigned char *)name);
	  suma=suma*32+val;
	  if (suma>module) suma%=module;
	  }
	name++;
	}
  return suma;
  }

CNewsArtHolder * CHashTable::_Find(const char *msgid)
  { 
  if (tblsize<1) return NULL;
  DWORD hash=HashName(msgid,tblsize);
  while (hldlist[hash]!=NULL) 
	{
	if (strcmp(hldlist[hash]->messid,msgid)==0) return hldlist[hash];
	hash++;
	if (hash>=tblsize) hash-=tblsize;
	}
  return NULL;
  }

bool CHashTable::Add(CNewsArtHolder *hld)
  {
  CMySingleLock sl(&locker);
  return _Add(hld);
  }

CNewsArtHolder * CHashTable::Find(const char *msgid)
  {
  CMySingleLock sl(&locker);
  return _Find(msgid);
  }

void CHashTable::Reset()
  {
  CMySingleLock sl(&locker);
  memset(hldlist,0,sizeof(CNewsArtHolder *)*tblsize);
  itemcount=0;
  }
