// MessageList.cpp : implementation file
//

#include "stdafx.h"
#include "newsreader.h"
#include "MessageList.h"
#include "NewsTerminal.h"
#include "NewsArticle.h"
#include "NewsAccount.h"
#include "WebBrowser2.h"
#include "MainFrm.h"
#include "MsgPrevewDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

#define COLUMNARRAY 10

static inline int LEVELOFFSET(int level, int sz)
{
  int k=(sz/2-40)/10;
  int res;
  if (level>=k)
  {
    res=k*10+40;
    res=res+(sz-res)*(level-k)/(level+1);
  }
  else
    res=level*10+40;
  return res;
}
//#define LEVELOFFSET(level,sz) ((((level)*10+40)>sz/2)?

/////////////////////////////////////////////////////////////////////////////
// CMessageList

#define SORTMODE "SORTMODE"

CMessageList::CMessageList(CNRNotify *ntf)
  {
  idtimer=-1;
  curaccount=0;
  curgroup=0;
  state_filter=EANormal;
  fthread=0;
  lthread=0;
  lastsort=-1;
  lastmsg=NULL;
  selected=NULL;
  this->ntf=ntf;
  hidetrash=true;
  }

//--------------------------------------------------

CMessageList::~CMessageList()
  {
  }

//--------------------------------------------------

BEGIN_MESSAGE_MAP(CMessageList, CListCtrl)
  //{{AFX_MSG_MAP(CMessageList)
  ON_NOTIFY_REFLECT(LVN_ITEMCHANGED, OnItemchanged)
    ON_WM_TIMER()
      ON_WM_LBUTTONDOWN()
        ON_WM_KEYDOWN()
          ON_NOTIFY_REFLECT(LVN_COLUMNCLICK, OnColumnclick)
            ON_WM_KILLFOCUS()
              ON_WM_SETFOCUS()
                ON_NOTIFY_REFLECT(NM_DBLCLK, OnDblclk)
                  ON_WM_CHAR()
                    //}}AFX_MSG_MAP
                    END_MESSAGE_MAP()
                      
                      /////////////////////////////////////////////////////////////////////////////
                      // CMessageList message handlers
                      
                      /*!
\param col column index
\param sort sort direction - 0 = no sort
*/
                      static CString HeaderItem(int col, int sort)
                        {
                        CString s;
                        switch (col)
                          {
                          case 0: case 1:
                          break;
                          case 2:
                          s.LoadString(IDS_TEXTSUBJECT);
                          break;
                          case 3:
                          s.LoadString(IDS_TEXTFROM);
                          break;
                          case 4:
                          s.LoadString(IDS_TEXTDATE);
                          break;
                          }
                        return s;
                        }

//--------------------------------------------------

static void InsertMessageHeader(CMessageList &list, int col, CString text, int cx, int subi)
  {
  LVCOLUMN column;
  column.mask = LVCF_TEXT|LVCF_FMT|LVCF_SUBITEM;
  column.pszText = (LPTSTR)(const char *)text;
  column.fmt = LVCFMT_LEFT;
  column.iImage = -1;
  column.iSubItem = subi;
  column.cx = cx;
  
  //	LVCOLUMN col;
  //	col.mask = LVCF_IMAGE;
  //	col.iImage = 13;
  //list.InsertColumn(col,HeaderItem(col,0),LVCFMT_LEFT|LVCFMT_IMAGE,cx,subi);
  list.InsertColumn(col,&column);
  }

//--------------------------------------------------

void CMessageList::Create(CWnd *parent)
  {
  CListCtrl::Create(LVS_REPORT|LVS_SHAREIMAGELISTS|LVS_OWNERDRAWFIXED|LVS_SHOWSELALWAYS|WS_VISIBLE|WS_CHILD|WS_BORDER,
  CRect(0,0,0,0),parent,0);
  //CString s;
  SetImageList(&theApp.ilist,LVSIL_SMALL);
  //s.LoadString(IDS_TEXTSUBJECT);
  InsertMessageHeader(*this,0,HeaderItem(0,0),20,0);
  InsertMessageHeader(*this,1,HeaderItem(1,0),20,0);
  InsertMessageHeader(*this,2,HeaderItem(2,0),300,1);
  InsertMessageHeader(*this,3,HeaderItem(3,0),200,2);
  InsertMessageHeader(*this,4,HeaderItem(4,0),100,3);
  /*
  InsertColumn(0,HeaderItem(0,0),LVCFMT_LEFT|LVCFMT_IMAGE,20,0);
  InsertColumn(1,HeaderItem(1,0),LVCFMT_LEFT|LVCFMT_IMAGE,20,0);
  InsertColumn(2,HeaderItem(2,0),LVCFMT_LEFT|LVCFMT_IMAGE,300,1);
  InsertColumn(3,HeaderItem(3,0),LVCFMT_LEFT|LVCFMT_IMAGE,200,2);
  InsertColumn(4,HeaderItem(4,0),LVCFMT_LEFT|LVCFMT_IMAGE,100,3);
	*/
  plus=theApp.LoadIcon(IDI_PLUS);
  minus=theApp.LoadIcon(IDI_MINUS);
  
  _sortmode=theApp.GetRegInt(SORTMODE,SORTMODE_ASK);
  }

//--------------------------------------------------

void CMessageList::LoadList(CNewsAccount *acc, CNewsGroup *grp, bool autoselect)
  {
  CRWLockGuard guard(articleLock,false);guard.LockPM();
  if (curgroup==grp && curaccount==acc) return;
  DeleteAllItems();    
  qsearch="";
  selected=NULL;
  curaccount=acc;
  curgroup=grp;
  if (acc==NULL || grp==NULL) 
    {
    idnosel=SetTimer(9987,200,NULL);
    return;
    }
  ChangeLastSort(grp->GetCurrentSort());
  ShowWindow(SW_HIDE);
  PartLoadList(grp->GetList(),-1);
  lastmsg=NULL;  
  fthread=lthread=-1;
  if (autoselect) SelectNewestMsg();
  ShowWindow(SW_SHOW);
  }

//--------------------------------------------------

void GetNameFrom(const RString &in, CString &out)
{
  CString csin=(LPCTSTR)in;
  GetNameFrom(csin,out);
}

void GetNameFrom(const CString &in, CString &out)
  {
  out="";  
  const char *c=strchr(in,'"'); //basic quotes (Microsoft Outlook)
  if (c==NULL)
    {
    c=strchr(in,'('); // Some newsreaders
    if (c==NULL) 
      {
      c=strchr(in,'<');  //no quotes
      if (c==NULL) out=in; //only raw adress
      else
        {
        const char *x=in;
        while (x!=c) out+=*x++;        
        }
      }
    else
      {
      c++;
      while (*c && *c!=')') out+=*c++;
      }
    }
  else
    {
    c++;
    while (*c && *c!='"')
      {
      if (*c=='\\') 
        {
        c++;
        int p=32;
        if (*c=='x' || *c=='X')
          {
          c++;
          sscanf(c,"%X",&p);
          }
        else
          {
          int i=sscanf(c,"%o",&p);
          if (i==1) out+=(char)p;
          else out+=*c;
          }
        }
      else out+=*c;
      *c++;
      }
    }
  int iso1=out.Find("=?");
  if (iso1!=-1)
    {
    char *c=out.GetBuffer(out.GetLength()+1);
    char mark[4];	
    c+=iso1+2;
    int norma;
    int codep;
    char mrk;	
    strncpy(mark,c,3);
    mark[3]=0;c+=3;
    if (stricmp(mark,"iso")==0 && sscanf(c,"-%d-%d?%c?",&norma,&codep,&mrk)!=-1 && mrk=='Q')
      {
      char *d=c-5;
      c=strchr(c,'Q')+2;
      while (c[0] && (c[0]!='?' || c[1]!='='))
        {
        if (*c=='=')
          {
          c++;
          int code;
          sscanf(c,"%2x",&code);
          c+=2;
          *d=(char)code;
          }
        else
          *d=*c++;
        d++;
        }
      *d=0;
      out.ReleaseBuffer();
      }
    }
  }

//--------------------------------------------------

int CMessageList::PartLoadList(CNewsArtHolder *hlda, int afteritem)
  {
  selected=0;
  CString s;
  while (hlda)
    {		  
    int q=afteritem;
    afteritem=LoadItem(q,hlda);	  
    if (q==lthread) lthread=afteritem;
    if (q!=afteritem && hlda->GetChild() && hlda->GetOpened())
      afteritem=PartLoadList(hlda->GetChild(),afteritem);
    hlda=hlda->GetNext();;
    }	
  return afteritem;
  }

//--------------------------------------------------

void CMessageList::OnItemchanged(NMHDR* pNMHDR, LRESULT* pResult) 
  {
  if (inupdate) return;
  NM_LISTVIEW* pNMListView = (NM_LISTVIEW*)pNMHDR;
  if (pNMListView->uNewState & LVIS_SELECTED || pNMListView->uOldState & LVIS_SELECTED)
    {
    int p=pNMListView->iItem;
    if (p!=-1)
      {
      if (!GetItemState(p,LVIS_SELECTED))         
        p=GetNextItem(-1,LVNI_SELECTED);        
      if (p<0) idnosel=SetTimer(9987,200,NULL);
      else
        {
        CNewsArtHolder *hlda=p<0?NULL:(CNewsArtHolder *)GetItemData(p);
      
        if (lastmsg!=hlda)
          {
          KillTimer(idnosel);
          browse->NavigateArticle(curaccount,curgroup,hlda);
          //		  if (lastmsg) lastmsg->GrayThread(false);
          lastmsg=hlda;        
          //		  if (lastmsg) lastmsg->GrayThread(true);
          if (idtimer) KillTimer(idtimer);
          idtimer=SetTimer(100,1000,NULL);	  
          curaccount->history.AddToHistory(hlda->messid);
          if (ntf->IsEnabled())
            {
            ntf->DeleteArticle(hlda);
            }
          }
        if (p<fthread || p>lthread)
          {
          InvalidateThreadMark();
          CNewsArtHolder *q=hlda;
          fthread=lthread=p;
          while (q && q->GetLevel() && fthread>0)
            {
            fthread--;
            q=(CNewsArtHolder *)GetItemData(fthread);
            }
          int cnt=GetItemCount();
          do
            {
            lthread++;
            if (lthread<cnt) q=(CNewsArtHolder *)GetItemData(lthread);else q=NULL;
            }
          while (q && q->GetLevel());
          InvalidateThreadMark();
          lthread--;
          }
        }
      }
    selmark=true;
    }
  selected=NULL;
  *pResult = 0;
  /*	// group changed  - forget what the last sort was
	if (curgroup)
	{
		curgroup->SortItemsIfNeeded(_globalSortItem);
		ChangeLastSort(curgroup->GetCurrentSort());
	}
	else
	{
		ChangeLastSort(-1);
	}
	//SortItemsIfNeeded();*/
  }

//--------------------------------------------------

void CMessageList::OnTimer(UINT nIDEvent) 
  {
  if (nIDEvent==idtimer)
    {
    int p=GetNextItem(-1,LVNI_SELECTED);
    if (p!=-1)
      {
      CNewsArtHolder *hlda=(CNewsArtHolder *)GetItemData(p);
      if (hlda->GetArticle())	
        {
        KillTimer(nIDEvent);
        if (hlda->GetUnread() && theApp.config.autoRead)
          {
          hlda->MarkAsRead(3);
          curgroup->DecNews();
          curaccount->dirty=true;
          theApp.m_pMainWnd->PostMessage(NRM_GRPREADDONE,1,(LPARAM)curgroup->iteminlist);
          CRect rc;
          GetItemRect(p,&rc,LVIR_BOUNDS);
          InvalidateRect(&rc);
                    }
        }

      }
    }
  if (nIDEvent==idnosel)
    {
    if (GetCurMessage()==NULL)
      {
      browse->ResNavigate("NOSELECT.HTM");
      if (curaccount) curaccount->history.BlankPage();
      lastmsg=NULL;
      fthread=-1;
      lthread=-1;
      Invalidate(FALSE);
      }
    KillTimer(idnosel);
    idnosel=-1;
    }

  }

//--------------------------------------------------

/*static void AnalyzeToken(istream &in, int ff, ostream &out)
  {
  CString s;  
  int i=ff;
  while (i>32) 
	{
	s+=(char)i;
	i=in.get();
	}
  if (strncmp(s,"file:///",8)==0)
	{
	out<<"<a href=\""<<s<<"\" target=\"NewsReader\">"<<(const char *)s+8<<"</a>";
	}
  else
	{
	out<<s[0];
	istrstream ss(s.LockBuffer()+1);
	Text2Html(ss,out);
	s.UnlockBuffer();
	}
  }

static int AnalyzeWord(istream &in, ostream &out)
  {
  char buffer[1024];

  int ip=0;
  int i=in.get();
  while (i>32 && ip<1023)
	{
	buffer[ip++]=i;
	i=in.get();
	}
  buffer[ip]=0;
  bool odkaz=false;
  if (strncmp(buffer,"file:///",8)==0 || strncmp(buffer,"http://",7)==0 || strncmp(buffer,"ftp://",6)==0 ||
	 strncmp(buffer,"\\\\",2)==0 || strncmp(buffer,"file://",7)==0 || strncmp(buffer,"file:\\\\",7)==0 ||
	 strncmp(buffer,"file:\\\\\\",8)==0)
	{
	out<<"<a href=\""<<buffer<<"\" target=\"NewsReader\">";
	odkaz=true;
	}
  else
  if (strchr(buffer,'@')!=NULL) 
	{
	out<<"<a href=\"mailto:"<<buffer<<"\">";
	odkaz=true;
	}
  else
  if (strncmp(buffer,"www.",4)==0)
	{
	out<<"<a href=\"http://"<<buffer<<"\" target=\"NewsReader\">";
	odkaz=true;
	}
  for (int z=0;z<ip;z++)
	{
	unsigned char i;
	switch (i=buffer[z])
	  {
	  case '<':out<<"&lt;";break;
	  case '>':out<<"&gt;";break;
	  case '&':out<<"&amp;";break;
	  default: 
		if (i>128 || i<32) 
		  {
		  char buff[10];
		  sprintf(buff,"&#%d;",i);
		  out<<buff;
		  }
		else
		  out<<(char)i;
		break;
	  }
	}
  if (odkaz)
	out<<"</a>";
  return i;
  }
*/
void CMessageList::DrawItem(LPDRAWITEMSTRUCT lpDrawItemStruct)
  {
  int wx1=0,wx2=0;
  CRect rc;
  int clmidx=0;
  int carr[COLUMNARRAY];
  
  CNewsArtHolder *hld=(CNewsArtHolder *)lpDrawItemStruct->itemData;
  if (hld==NULL) return;
  CDC dc;
  dc.Attach(lpDrawItemStruct->hDC);
  CFont *ff;
  if (normalfont.m_hObject==NULL)
    {
    ff=dc.GetCurrentFont();
    LOGFONT lg;
    ff->GetLogFont(&lg);
    lg.lfCharSet=EASTEUROPE_CHARSET;
    normalfont.CreateFontIndirect(&lg);
    lg.lfWeight=700;
    boldfont.CreateFontIndirect(&lg);
    }
  memset(carr,0xff,sizeof(carr));
  GetColumnOrderArray(carr,-1);
  int ci=carr[clmidx];
  rc=lpDrawItemStruct->rcItem;  
  wx2+=rc.left;
  wx1+=rc.left;
  CString text;
  int txtcolor=-1;  
  if (lpDrawItemStruct->itemState & ODS_SELECTED)
    {
    txtcolor=dc.GetTextColor();
    dc.FillSolidRect(&lpDrawItemStruct->rcItem,focus?GetSysColor(COLOR_HIGHLIGHT):GetSysColor(COLOR_BTNFACE));
    dc.SetTextColor(focus?GetSysColor(COLOR_HIGHLIGHTTEXT):GetSysColor(COLOR_BTNTEXT));
    }
  else
    {
    switch (hld->state)
      {
      case EANormal:
      // check highligthed color
      if (hld->GetHighlighted())
        {
        dc.SetTextColor(theApp.config.col_highlighted);
        }
      else
        {
        dc.SetTextColor(GetSysColor(COLOR_BTNTEXT));
        }
      break;
      case EAImportant:dc.SetTextColor(theApp.config.col_important);break;
      case EAHidden:dc.SetTextColor(GetSysColor(COLOR_GRAYTEXT));break;
      }
    }	
  if (hld->GetHidden()) dc.SetTextColor(GetSysColor(COLOR_GRAYTEXT));  
  if ((lpDrawItemStruct->itemState & ODS_SELECTED)==0)
    {
    if ((signed)lpDrawItemStruct->itemID<=lthread && (signed)lpDrawItemStruct->itemID>=fthread)
      {
      dc.FillSolidRect(&lpDrawItemStruct->rcItem,ShadeSysColor(COLOR_WINDOW,95));
      }
    else if (lpDrawItemStruct->itemAction=ODA_SELECT)
      {
      dc.FillSolidRect(&lpDrawItemStruct->rcItem,GetSysColor(COLOR_WINDOW));
      }
    }
  
  if (hld->GetUnread() || (hld->GetOpened()==false && hld->GetChildNew()!=0))
    ff=dc.SelectObject(&boldfont);
  else
    ff=dc.SelectObject(&normalfont);
  CPen pen(PS_SOLID,1,GetSysColor(COLOR_GRAYTEXT)),*old=dc.SelectObject(&pen);
  if (lpDrawItemStruct->itemState & ODS_FOCUS)
    {
    dc.FillSolidRect(lpDrawItemStruct->rcItem.left,lpDrawItemStruct->rcItem.top,
      3,lpDrawItemStruct->rcItem.bottom-lpDrawItemStruct->rcItem.top,GetSysColor(COLOR_WINDOWTEXT));
    }
  if (theApp.config.showLines)
    {
    bool p;
    if (theApp.config.betweenThreads)
      {
      int cnt=GetItemCount();
      if ((signed)lpDrawItemStruct->itemID+1>=cnt) p=true;
      else 
        {
        CNewsArtHolder *hld=(CNewsArtHolder *)GetItemData(lpDrawItemStruct->itemID+1);
        p=hld->GetLevel()==0;
        }
      }
    else p=true;
    if (p)
      {
      dc.MoveTo(rc.left,rc.bottom-1);
      dc.LineTo(rc.right-1,rc.bottom-1);
      }
    }
  while (ci>=0)
    {
    wx2+=GetColumnWidth(ci);
    rc.left=wx1+2;
    rc.right=wx2-2;
    if (ci==2)
      {
      rc.left+=LEVELOFFSET(hld->GetLevel(),rc.right-rc.left);
      if (hld->GetChild())
        dc.DrawIcon(CPoint(rc.left-40,rc.top),hld->GetOpened()?minus:plus);
      int iconId = ICON_INCACHE;
      char incache=hld->IsInCache();
      if (incache==-1) 
        {
        incache=curaccount->cache.FindInCache(hld->messid)==true;
        hld->SetInCache(incache);
        }
      if (!theApp.config.grayIcons || hld->GetUnread() || (hld->GetOpened()==false && hld->GetChildNew()!=0))
        {
        if (hld->GetArticle() || incache)
          iconId = ICON_INCACHE;
        else
          iconId = ICON_NOTINCACHE;
        }
      else
        {
        if (hld->GetArticle()  || incache)
          iconId = ICON_INCACHEREAD;
        else
          iconId = ICON_NOTINCACHEREAD;
        }
      theApp.ilist.Draw(&dc,iconId,CPoint(rc.left-20,rc.top),ILD_NORMAL);
      }
    if (ci==1)
      {
      int icon;
      switch (hld->state)
        {
        case EANormal: icon=ICON_NWEMPTY;break;
        case EAImportant: icon=ICON_STATEIMPORTANT;break;
        case EAHidden: icon=ICON_STATEHIDE ;break;
        }
      theApp.ilist.Draw(&dc,icon,CPoint(rc.left,rc.top),ILD_TRANSPARENT);
      }
    if (ci==0)
      {
      int icon;
      switch (hld->attachment)
        {
        case NHATT_NONE: icon=ICON_NWEMPTY;break;
        case NHATT_ISATTACH: icon=ICON_ATTACHMENT;break;
        case NHATT_MAYBE: icon=ICON_ATTACHMENTQUESTION;break;
        case NHATT_ARCHIVE: icon=ICON_ARCHIVE;break;
        }
      theApp.ilist.Draw(&dc,icon,CPoint(rc.left,rc.top),ILD_TRANSPARENT);
      }
    text=GetItemText(lpDrawItemStruct->itemID,ci);
    DrawTextA(lpDrawItemStruct->hDC,text,text.GetLength(),&rc,DT_LEFT|DT_TOP|DT_END_ELLIPSIS|DT_NOPREFIX);
    clmidx++;
    wx1=wx2;	
    ci=carr[clmidx];	
    if (theApp.config.showLines)
      {
      dc.MoveTo(rc.right,rc.bottom-1);
      dc.LineTo(rc.right,rc.top-1);
      }
    }
  if (txtcolor!=-1) dc.SetTextColor(txtcolor);
  if (hld->IsTrash())
    {
    CPen pen(PS_SOLID,1,RGB(255,0,0));
    dc.SelectObject(&pen);
    dc.MoveTo(lpDrawItemStruct->rcItem.left,(lpDrawItemStruct->rcItem.top+lpDrawItemStruct->rcItem.bottom)>>1);
    dc.LineTo(lpDrawItemStruct->rcItem.right,(lpDrawItemStruct->rcItem.top+lpDrawItemStruct->rcItem.bottom)>>1);
    dc.SelectObject(old);
    }
  dc.SelectObject(old);
  dc.SelectObject(ff);
  dc.Detach();
  }

//--------------------------------------------------

int CMessageList::Mouse2Item(CPoint &pt)
  {
  CRect rc;
  int topi=GetTopIndex();  
  GetItemRect(topi,&rc,LVIR_BOUNDS);
  int size=rc.bottom-rc.top;  
  if (size==0) return -1;
  int top=rc.top;
  int x=(pt.y-top)/size+topi;
  if (x<0 || x>=GetItemCount()) x=-1;
  return x;
  }

//--------------------------------------------------

int CMessageList::Mouse2Column(CPoint &pt,int *w1, int *w2)
  {
  int carr[COLUMNARRAY];
  memset(carr,0xff,sizeof(carr));
  GetColumnOrderArray(carr,-1);
  int clmidx=0;
  CRect rc;
  int topi=GetTopIndex();  
  GetItemRect(topi,&rc,LVIR_BOUNDS);
  int wx1=rc.left,wx2=wx1;
  int ci=carr[clmidx];
  while (ci>=0)
    {
    wx2+=GetColumnWidth(ci);  
    if (pt.x>=wx1 && pt.x<wx2) break;
    wx1=wx2;
    clmidx++;
    ci=carr[clmidx];
    }
  if (w1) *w1=wx1;
  if (w2) *w2=wx2;
  return ci;
  }

//--------------------------------------------------

int CMessageList::IsMouseAtButton(CPoint &pt)
  {
  int item=Mouse2Item(pt);
  if (item==-1) return -1;
  int wx1,wx2;
  int column=Mouse2Column(pt,&wx1,&wx2);
  if (column!=2) return -1;
  int dist=pt.x-wx1;
  CNewsArtHolder *hld=(CNewsArtHolder *)GetItemData(item);
  if (hld==NULL) return -1;
  int needspc=LEVELOFFSET(hld->GetLevel(),GetColumnWidth(column))-dist;
  return (needspc>20 && needspc<40)?item:-1;
  }

//--------------------------------------------------

static void RecursiveSetState(CNewsArtHolder *hld, EArticleState state)
  {
  hld->state=state;
  if (hld->GetChild()) RecursiveSetState(hld->GetChild(),state);
  if (hld->GetNext()) RecursiveSetState(hld->GetNext(),state);
  }

//--------------------------------------------------

void CMessageList::OnLButtonDown(UINT nFlags, CPoint point) 
  {
  int item=IsMouseAtButton(point);
  if (item>=0)
    {
    CNewsArtHolder *hld=(CNewsArtHolder *)GetItemData(item);	
    hld->OpenClose();
    bool open=hld->GetOpened();
    if (open)
      {
      PartLoadList(hld->GetChild(),item);
      EnsureVisible(item+1,FALSE);
      }
    else
      {
      CNewsArtHolder *hp=hld->GetChild();
      int p=item+1;
      while (hp && hp!=hld->GetNext())
        {
        SetItemState(p,0,LVIS_FOCUSED|LVIS_SELECTED);		
        if (hp->GetChild())
          if (hp->GetOpened())hp=hp->EnumNext();else hp=hp->GetNext();
        else hp=hp->EnumNext();
        p++;
        }
      PartDelete(hld->GetChild(),item);
      }
    InvalidateItems(item,item);
    }
  else
    {
    int line=Mouse2Item(point);
    int col=Mouse2Column(point);
    if (line>=0 && col==1)
      {
      CNewsArtHolder *hld=(CNewsArtHolder *)GetItemData(line);
      EArticleState nwstate;
      switch (hld->state)
        {
        case EANormal: nwstate=EAImportant;break;
        case EAImportant: nwstate=EAHidden;break;
        case EAHidden: nwstate=EANormal;break;
        }
      CNewsArtHolder *p=hld;;
      while (p->GetParent()!=NULL && p->GetLevel()>0) p=p->GetParent();
      p->state=nwstate;
      if (p->GetChild()) 
        RecursiveSetState(p->GetChild(),nwstate);
      Invalidate(FALSE);
      }
    else
      CListCtrl::OnLButtonDown(nFlags,point);
    }
  CMainFrame *frame = (CMainFrame *)theApp.m_pMainWnd;
  if (GetCurGroup())
    {
    GetCurGroup()->CountNewsAndHighlights();
    frame->AlterGroup(GetCurGroup());
    }
  
  }

//--------------------------------------------------

void CMessageList::PartDelete(CNewsArtHolder *hld, int afteritem)
  {  
  while (hld)
    {
    if (ShuldVisible(hld))
      {
      DeleteItem(afteritem+1);
      if (afteritem+1<=lthread) lthread--;
      if (afteritem+1<=fthread) fthread--;
      if (hld->GetOpened())
        {
        PartDelete(hld->GetChild(),afteritem);
        }
      }
    hld=hld->GetNext();
    }
  }

//--------------------------------------------------

CNewsArtHolder * CMessageList::GetCurMessage()
  {  
  if (selected) return selected;
  int p=GetNextItem(-1,LVNI_SELECTED);
  if (p==-1) return NULL;
  return selected=(CNewsArtHolder *)GetItemData(p);
  }

//--------------------------------------------------

void CMessageList::Reload()
  {
  CNewsAccount *acc=curaccount;
  CNewsGroup *grp=curgroup;
  curgroup=NULL;
  curaccount=NULL;
  LoadList(acc, grp);
  }

//--------------------------------------------------



void CMessageList::	UpdateList()
  {  
  if (curgroup==NULL) return;
  BeginPossibleLongOp();
  CRWLockGuard guard(articleLock,false);guard.LockPM();
  inupdate=true;
  selected=0;
  /*	curgroup->SortItemsIfNeeded(_globalSortItem);

	ChangeLastSort(curgroup->GetCurrentSort());*/
  
  CNewsArtHolder *p=curgroup->GetList();
  int q=UpdateList(p,0);
  int cnt=GetItemCount();
  for (int i=q;i<cnt;i++) DeleteItem(q);
  Invalidate(FALSE);
  EndLongOp();
  }

//--------------------------------------------------

int CMessageList::	UpdateList(CNewsArtHolder *start, int item)
  {
  CNewsArtHolder *p=start;
  while (p) 
    {
    CheckPossibleLongOp();
    if (ShuldVisible(p))
      {
      item=UpdateItem(p,item);
      if (p->GetChild() && p->GetOpened())
        {
        item=UpdateList(p->GetChild(),item);
        }
      }
    p=p->GetNext();
    }
  return item;
  }

//--------------------------------------------------

int CMessageList::UpdateItem(CNewsArtHolder *hld, int shuldbe)
  {
  int cnt=GetItemCount();
  CNewsArtHolder *cur;
  if (cnt<=shuldbe) cur=NULL;
  else
    cur=(CNewsArtHolder *)GetItemData(shuldbe);
  if (cur!=hld)
    {
    CheckPossibleLongOp();    
    int i;
    for (i=shuldbe;i<cnt;i++) if (GetItemData(i)==(DWORD)hld) break;
    if (i<cnt)
      {
      for (int j=shuldbe;j<i;j++) 
        {
        if (shuldbe<=lthread) lthread--;
        if (shuldbe<=fthread) fthread--;
        DeleteItem(shuldbe);
        }
      shuldbe++;
      }
    else	
      shuldbe=LoadItem(shuldbe-1,hld)+1;
    }
  else
    shuldbe++;
  return shuldbe;
  }

//--------------------------------------------------

int CMessageList::LoadItem(int item, CNewsArtHolder *hlda)
  {
  CString s;
  if (ShuldVisible(hlda))
    {
    int p=InsertItem(++item,"",0);
    if (item<=lthread) lthread++;
    if (item<=fthread) fthread++;
    SetItemData(p,(DWORD)hlda);	
    SetItemText(p,2,hlda->GetSubject());
    GetNameFrom(hlda->from,s);
    SetItemText(p,3,s);
    if (hlda->date.GetStatus()==COleDateTime::valid) SetItemText(p,4,(hlda->date+theApp.timezone).Format());
    }    
  return item;
  }

//--------------------------------------------------

bool CMessageList::ShuldVisible(CNewsArtHolder *hlda)
  {
  bool isgrayed=false;
  CNewsArtHolder *tmp=hlda;
  if (grayed!=NULL)
    {
    while (tmp->GetParent()!=NULL) tmp=tmp->GetParent();
    if (tmp==grayed) isgrayed=true;
    }
  return (state_filter==EAHidden ||
    state_filter==EANormal && hlda->state!=EAHidden ||
      state_filter==EAImportant && hlda->state==EAImportant) && (!(hlda->GetHidden()) || theApp.config.showCancel)
        && (!hidetrash || !(hlda->IsTrash())) || isgrayed;
  }

//--------------------------------------------------

void CMessageList::SelectItem(CNewsArtHolder *hlda)
  {
  if (curaccount==NULL || curgroup==NULL) return;
  if (hlda==NULL)
    {
    int sel=GetNextItem(-1,LVIS_SELECTED);
    SetItemState(sel,0,LVIS_FOCUSED|LVIS_SELECTED);
    return;
    }  
  CNewsArtHolder *tmp;
  grayed=NULL;
  tmp=this->ShuldVisible(hlda)?NULL:hlda;
  grayed=tmp;
  CNewsArtHolder *q=hlda->GetParent();
  while (q) 
    {
    if (!q->GetOpened())q->OpenClose();
    grayed=q;
    q=q->GetParent();
    }
  UpdateList();
  LVFINDINFO fnd;
  fnd.flags=LVFI_PARAM;
  fnd.lParam=(DWORD)hlda;  
  int item=FindItem(&fnd);  
  int sel=GetNextItem(-1,LVIS_SELECTED);
  SetItemState(sel,0,LVIS_FOCUSED|LVIS_SELECTED);
  SetItemState(item,LVIS_FOCUSED|LVIS_SELECTED,LVIS_FOCUSED|LVIS_SELECTED);
  EnsureVisible(item,TRUE);
  browse->NavigateArticle(curaccount,curgroup,hlda);
  idnosel=SetTimer(9987,200,NULL);
  }

//--------------------------------------------------

static CNewsArtHolder *FndNew(CNewsArtHolder *p)
  {
  int lidx=-1;
  CNewsArtHolder *z=NULL;  
  while (p)
    {
    if (p->GetUnread()) return p;
    if (p->GetChild()) 
      {
      CNewsArtHolder *q=FndNew(p->GetChild());
      if (q) return q;
      }
    if (p->GetLevel()==0 && p->GetIndex()>lidx) 
      {lidx=p->GetIndex();z=p;}
    p=p->GetNext();
    }
  return z;
  }

//--------------------------------------------------

void CMessageList::InvalidateThreadMark()
  {
  InvalidateItems(fthread,lthread);
  }

//--------------------------------------------------

void CMessageList::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags) 
  {
  if (nChar>=VK_LEFT && nChar<=VK_DOWN) qsearch="";
  if (nChar==VK_RIGHT)
    {
    int p=GetNextItem(-1,LVNI_FOCUSED);
    if (p!=-1)
      {
      CNewsArtHolder *hld=(CNewsArtHolder *)GetItemData(p);
      if (hld->GetChild())
        if (hld->GetOpened()==false)
          {
          hld->OpenClose();		 
          PartLoadList(hld->GetChild(),p);
          }
      else
        {
        if (p+1<GetItemCount())
          {
          SetItemState(p,0,LVIS_FOCUSED|LVIS_SELECTED);
          SetItemState(p+1,LVIS_FOCUSED|LVIS_SELECTED,LVIS_FOCUSED|LVIS_SELECTED);
          EnsureVisible(p+1,FALSE);
          }
        }
      InvalidateItems(p,p);
      }
    }
  else if (nChar==VK_LEFT)
    {
    int p=GetNextItem(-1,LVNI_FOCUSED);
    if (p!=-1)
      {
      CNewsArtHolder *hld=(CNewsArtHolder *)GetItemData(p);
      if (hld->GetChild() && hld->GetOpened()==true)
        {
        hld->OpenClose();		 
        PartDelete(hld->GetChild(),p);
        }
      else
        {
        hld=hld->GetParent();
        if (hld) SelectItem(hld);
        }
      }
    InvalidateItems(p,p);
    }
  else
    if ((nChar==VK_PRIOR || nChar==VK_NEXT) && InQSearch())
      {
      qsearchtime=GetTickCount();
      int sel=GetNextItem(-1,LVIS_SELECTED);
      int qs=QuickSearch(sel,qsearch,nChar==VK_PRIOR);
      if (qs!=-1)
        {
        SetItemState(sel,0,LVIS_FOCUSED|LVIS_SELECTED);
        SetItemState(qs,LVIS_FOCUSED|LVIS_SELECTED,LVIS_FOCUSED|LVIS_SELECTED);
        EnsureVisible(qs,FALSE);
        }
      return;
      }
  if ((nChar=='C' || nChar==VK_INSERT) && GetKeyState(VK_CONTROL) & 0x80)
    {
    CNewsArtHolder *hld=GetCurMessage();
    CopyShortcut(hld);
    }
  else CListCtrl::OnKeyDown(nChar, nRepCnt, nFlags);
  }

//--------------------------------------------------

void CMessageList::InvalidateItems(int min, int max)
  {
  CRect qrc;bool first=true;
  for (int i=min;i<=max;i++)
    {
    CRect qc;
    BOOL suc=GetItemRect(i,&qc,LVIR_BOUNDS);
    if (suc) 
      if (first) 
        {qrc=qc;first=false;}
    else qrc.bottom=qc.bottom;
    }
  if (!first) InvalidateRect(&qrc,FALSE);
  
  }

//--------------------------------------------------

void CMessageList::ChangeLastSort(int by) 
  {
  int iLastsort = lastsort>=0 ? (lastsort&0x7f) : -1;
  int iBy = by>=0 ? (by&0x7f) : -1;
  
  // display sorting order
  
  // reset lastsort column
  if (iBy!=iLastsort && iLastsort>=0)
    {
    LVCOLUMN col;
    col.mask = LVCF_IMAGE|LVCF_FMT;
    col.fmt = LVCFMT_LEFT;
    col.iImage = -1;
    SetColumn(iLastsort,&col);
    }
  if (iBy>=0)
    {
    // set newly set column
    LVCOLUMN col;
    col.mask = LVCF_IMAGE|LVCF_FMT;
    col.fmt = LVCFMT_LEFT|LVCFMT_IMAGE;
    col.iImage = by>=0x80 ? ICON_SORT_UP : ICON_SORT_DOWN;
    SetColumn(iBy,&col);
    }
  
  
  
  lastsort=by;
  }

//--------------------------------------------------

#include "DlgSortModeAsk.h"

void CMessageList::OnColumnclick(NMHDR* pNMHDR, LRESULT* pResult) 
  {
  NMLISTVIEW* pNMListView = (NMLISTVIEW*)pNMHDR;
  
  *pResult = 0;
  int by=pNMListView->iSubItem;
  
  if (_sortmode==SORTMODE_ASK || curgroup==NULL)
    {
    ReleaseCapture();
    CDlgSortModeAsk dlg;
    dlg.m_SortMode=_sortmode;
    if (dlg.DoModal()==IDCANCEL) return;
    _sortmode=dlg.m_SortMode;
    theApp.SetRegInt(SORTMODE,_sortmode);
    }

  if (by==lastsort) by |= 0x80;

  SetCursor(::LoadCursor(NULL,IDC_WAIT));
  ::DogShowStatusV(IDS_SORTINGINPROGRESS);  
  ::DogUpdateWindow();
  if (articleLock.Lock(true,10000)==FALSE)
    {
    NrMessageBox(IDS_LOCKFAILED);
    }
  else
    {
    bool sortall=_sortmode==SORTMODE_ALL;
    if (GetKeyState(VK_CONTROL) & 0x80) sortall=!sortall;
    if (sortall)
      {
      if (curaccount!=NULL) curaccount->SortAll(by);
      }
    else  if (curgroup!=NULL) 
      curgroup->SortItems(by);
    ChangeLastSort(by);
    articleLock.Unlock();
    }
     
  fthread=lthread=-1;
 
  DeleteAllItems();
  UpdateList();	
  ::DogShowStatusV(IDS_DONE);
  }

//--------------------------------------------------

void CMessageList::SelectNewestMsg()
  {  
  if (curgroup==NULL) return;
  CRWLockGuard guard(articleLock,false);guard.LockPM();
  int p=GetNextItem(-1,LVNI_FOCUSED);
  CNewsArtHolder *str,*hld=NULL,*arch=NULL;
  if (p==-1) 
    {str=curgroup->GetList();}
  else
    {
    str=(CNewsArtHolder *)GetItemData(p);
    hld=str;
    if (str->IsArchive()) arch=str;
    str=str->EnumNext();
    }
  if (str)
    {
    int pmx=hld?hld->GetIndex():0;
    while (str)
      {
      if (!str->GetHidden())
        {
        if (str->GetUnread()) break;
        if (str->IsArchive()) arch=str;	  
        if (arch==NULL && str->GetLevel()==0)
          {
          if (str->GetIndex()>pmx) 
            {hld=str;pmx=str->GetIndex();}
          }		
        str=str->EnumNext();
        if (arch && str==arch->GetNext()) arch=NULL;		
        }
      else
        str=str->GetNext();
      }	
    if (str) hld=str;
    if (hld) SelectItem(hld);
    }
  }

//--------------------------------------------------

void CMessageList::OnKillFocus(CWnd* pNewWnd) 
  {
  CListCtrl::OnKillFocus(pNewWnd);
  qsearch="";
  focus=false;
  int ip=GetTopIndex()-1;
  while ((ip=GetNextItem(ip,LVNI_SELECTED))!=-1)  InvalidateItems(ip,ip);
  
  }

//--------------------------------------------------

void CMessageList::OnSetFocus(CWnd* pOldWnd) 
  {
  CListCtrl::OnSetFocus(pOldWnd);	
  focus=true;
  int ip=GetTopIndex()-1;
  while ((ip=GetNextItem(ip,LVNI_SELECTED))!=-1)  InvalidateItems(ip,ip);
  
  }

//--------------------------------------------------

void CMessageList::OnDblclk(NMHDR* pNMHDR, LRESULT* pResult) 
  {
  CPoint point(GetMessagePos());
  ScreenToClient(&point);
  int item=IsMouseAtButton(point);
  if (item>=0) return;
  int line=Mouse2Item(point);
  int col=Mouse2Column(point);
  if (line>=0 && col==1) return;
  if (curgroup==NULL) return ;
  if (curgroup->special)
    {
    theApp.m_pMainWnd->PostMessage(WM_COMMAND,ID_MESSAGES_GOTOCONTEXT,0);
    return;
    }
  CNewsArtHolder *hld=GetCurMessage();
  CMsgPrevewDlg *dlg=new CMsgPrevewDlg;
  dlg->Create(NULL);
  dlg->LoadContent(curaccount,curgroup,hld);
  *pResult = 0;
  }

//--------------------------------------------------

void CMessageList::CopyShortcut(CNewsArtHolder *hlda)
  {
  opakuj:
  if (::OpenClipboard(NULL)==TRUE)
    {
    CString shortcut="news:"+hlda->messid.Mid(1,hlda->messid.GetLength()-2);
    ::EmptyClipboard();
    HGLOBAL mem=GlobalAlloc(GMEM_MOVEABLE|GMEM_DDESHARE,shortcut.GetLength()+1);
    void *ptr=GlobalLock(mem);
    strcpy((char *)ptr,shortcut);
    GlobalUnlock(mem);
    ::SetClipboardData(CF_TEXT,mem);
    ::CloseClipboard();
    }
  else    
    if (AfxMessageBox(IDS_UNABLEOPENCLIPBOARD,MB_RETRYCANCEL|MB_ICONEXCLAMATION )==IDRETRY)
      goto opakuj;
  }

//--------------------------------------------------

void CMessageList::OnChar(UINT nChar, UINT nRepCnt, UINT nFlags) 
  {
  if (isalnum(nChar) || nChar==32 || nChar==8)
    {
    if (!InQSearch()) qsearch="";
    if (nChar==8) qsearch.Delete(qsearch.GetLength()-1);else qsearch+=char(nChar);
    int sr=QuickSearch(-1,qsearch,false);
    int sel=GetNextItem(-1,LVIS_SELECTED);
    SetItemState(sel,0,LVIS_FOCUSED|LVIS_SELECTED);
    if (sr==-1)
      {
      MessageBeep(MB_ICONASTERISK);
      qsearch.Delete(qsearch.GetLength()-1);	  
      }
    else
      {
      SetItemState(sr,LVIS_FOCUSED|LVIS_SELECTED,LVIS_FOCUSED|LVIS_SELECTED);
      EnsureVisible(sr,FALSE);
      }
    qsearchtime=GetTickCount();
    return;
    }
  qsearch="";
  CListCtrl::OnChar(nChar, nRepCnt, nFlags);
  }

//--------------------------------------------------

int CMessageList::QuickSearch(int from, const char *text, bool downtop)
  {
  int count=GetItemCount();
  from+=downtop?-1:1;
  int tsize=strlen(text);
  char *comp=(char *)alloca(tsize+1);
  while (from>=0 && from<count)
    {	
    CNewsArtHolder *item=(CNewsArtHolder *)GetItemData(from);
    if (item && item->GetParent()==NULL) 
      {
      char p[4];
      strncpy(p,item->GetSubject(),4);
      p[3]=0;
      const char *c;
      if (stricmp(p,"re:")==0) c=((LPCSTR)item->GetSubject()+3);
      else c=item->GetSubject();
      while (isspace(*c)) c++;
      strncpy(comp,c,tsize);
      comp[tsize]=0;	  
      czmap(comp);
      if (stricmp(comp,text)==0) return from;
      }
    from+=downtop?-1:1;	
    }
  return -1;
  }

//--------------------------------------------------


void CMessageList::FasterLongOp()
        {
        ShowWindow(SW_HIDE);
        showatend=true;
        ::SetCursor(::LoadCursor(NULL,IDC_WAIT));
        ::DogShowStatusV(IDS_UPDATINGLIST);
        ::DogUpdateWindow();
        }
