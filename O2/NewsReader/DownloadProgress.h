#if !defined(AFX_DOWNLOADPROGRESS_H__0A568E82_8FF2_11D5_B39F_00C0DFAE7D0A__INCLUDED_)
#define AFX_DOWNLOADPROGRESS_H__0A568E82_8FF2_11D5_B39F_00C0DFAE7D0A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DownloadProgress.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CDownloadProgress dialog

#include "NewsTerminal.h"
#include "NewsArticle.h"
#include "NewsAccount.h"


class CDownloadProgress : public CDialog
{
// Construction
	int swapper;
	bool threads[3];
	CNewsArtHolder *first;
	bool warning;
public:
	void Download(CNewsTerminal *term, CNewsArtHolder *hld,bool recurse=false);
	CListCtrl *itemtree;
	CNewsAccount *acc;
	CNewsGroup *grp;	
	
	CDownloadProgress(CWnd* pParent = NULL);   // standard constructor
	bool stopd;

// Dialog Data
	//{{AFX_DATA(CDownloadProgress)
	enum { IDD = IDD_DOWNLOAD };
	CStatic	msgname;
	CProgressCtrl	progress;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDownloadProgress)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CDownloadProgress)
	virtual BOOL OnInitDialog();
	virtual void OnCancel();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DOWNLOADPROGRESS_H__0A568E82_8FF2_11D5_B39F_00C0DFAE7D0A__INCLUDED_)
