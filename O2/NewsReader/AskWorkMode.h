#if !defined(AFX_ASKWORKMODE_H__A1B3F3E2_96E5_11D5_B39F_00C0DFAE7D0A__INCLUDED_)
#define AFX_ASKWORKMODE_H__A1B3F3E2_96E5_11D5_B39F_00C0DFAE7D0A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// AskWorkMode.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CAskWorkMode dialog

class CAskWorkMode : public CDialog
{
// Construction
public:
	CAskWorkMode(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CAskWorkMode)
	enum { IDD = IDD_OFFLINE };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAskWorkMode)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CAskWorkMode)
	afx_msg void OnRetry();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ASKWORKMODE_H__A1B3F3E2_96E5_11D5_B39F_00C0DFAE7D0A__INCLUDED_)
