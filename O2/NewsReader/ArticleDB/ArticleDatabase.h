// ArticleDatabase.h: interface for the ArticleDatabase class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_ARTICLEDATABASE_H__66DDAB16_04FD_4345_B411_131BCCC64D81__INCLUDED_)
#define AFX_ARTICLEDATABASE_H__66DDAB16_04FD_4345_B411_131BCCC64D81__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "ArticleTable.h"
#include "ArticleDBIndexes.h"

typedef ASimpleIndexRecord<char,CmpSubstring> AStringIndex;
typedef ASimpleIndexRecord<ArticleDateTime,CmpDateTime> ADateIndex;
typedef ASimpleIndexRecord<DMMHANDLE,CmpDMMHandle,true> ADMMHandleIndex;


class ArticleDatabase: public ArticleTable
{	
  BTree<AStringIndex> _fieldFrom;
  BTree<AStringIndex> _fieldSubject;
  BTree<AStringIndex> _fieldMessageID;
  BTree<AStringIndex> _fieldReferences;
  BTree<AStringIndex> _fieldGroups;
  BTree<ADMMHandleIndex> _fieldThread;  
  BTree<ADateIndex> _fieldReadDate;
  BTree<ADateIndex> _fieldDate;

  void IndexSubstrings(BTree<AStringIndex> &indexTree, const char *text, DMMHANDLE article);
  void IndexNewsgroups(BTree<AStringIndex> &indexTree, const char *text, DMMHANDLE article);
public:
	enum QueryStringEnum {QueryFrom,QuerySubject,QueryMessageID,QueryReferences,QueryGroups};

	void IndexDatabase();
	void IndexArticle(ArticleHeader &hdr);
	ArticleDatabase();
	virtual ~ArticleDatabase();

	ArticleRecordset<AStringIndex> QueryString(QueryStringEnum querywhat, const char *minValue, const char *maxValue=NULL, bool exact=false);
	bool GetParentArticle(ArticleHeader &ahdr,ArticleHeader *parent);
	ArticleRecordset<AStringIndex> GetChilds(ArticleHeader &ahdr);
	ArticleRecordset<ADMMHandleIndex> GetThread(ArticleHeader &ahdr);

protected:
    BTree<AStringIndex> *GetStringIndex(QueryStringEnum enm);

};

#endif // !defined(AFX_ARTICLEDATABASE_H__66DDAB16_04FD_4345_B411_131BCCC64D81__INCLUDED_)
