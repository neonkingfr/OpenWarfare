// ArticleDB.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "ArticleDatabase.h"
#include "ArticleTableReader.h"
#include <EL\TCPIPBasics\IPA.h>
#include <EL\TCPIPBasics\Socket.h>
#include <EL\TCPIPBasics\WSA.h>
#include "../LineTerminal2.h"
#include "../ArchiveStreamTCP.h"
#include <conio.h>
#include "..\CharMapCz.h"

CCharMapCz CzechCharMap;

void SetProgress(unsigned long cur, unsigned long max)
  {
  static unsigned long last;
  static DWORD lasttime;
  if (cur<last || cur-last>10000) 
	{
	DWORD curtime=GetTickCount();
	printf("In progress %d from %d (%d)\r", cur, max, curtime-lasttime);
	last=cur;
	lasttime=curtime;
	}
  }

void ShowArticle(ArticleHeader &hdr, int begspaces=0)
  {
  for (int i=0;i<begspaces;i++) putchar(' ');
  printf("%s\t%s\t%d.%d.%d %02d:%02d:%02d\n",hdr.subject,hdr.from,
	hdr.datePost->day,
	hdr.datePost->month,
	hdr.datePost->year+1900,
	hdr.datePost->hour,
	hdr.datePost->minute,
	hdr.datePost->second);
  }

void ShowArticleExt(ArticleHeader &hdr, int begspaces=0)
  {
  for (int i=0;i<begspaces;i++) putchar(' ');
  printf("%.20s\t%.20s\t%d\n",hdr.msgid,hdr.reference,(int)hdr.threadID);
  }


void TreeViewTest(ArticleDatabase &db, ArticleRecordset<AStringIndex> &rcrdset, int level)
  {
  ArticleHeader hdr;
  while (rcrdset.FindNext(hdr))
	{
	ShowArticleExt(hdr,level);
	ArticleRecordset<AStringIndex>sub=db.GetChilds(hdr);	
	TreeViewTest(db,sub,level+1);
	}
  }

int main(int argc, char* argv[])
  {
  ArticleDatabase atable;
  atable.Create("articles.db");

  /*  CWSA wsa(MAKEWORD(1,1));
  Socket s(SOCK_STREAM);
  CIPA ip("e2.bistudio.com",119);
  s.SetNonBlocking(true);  
  s.Connect(ip);
  if (s.Wait(5000,Socket::WaitWrite)!=Socket::WaitWrite) 
	abort();
  ArchiveStreamTCPIn tcpin(s,30000,false);
  ArchiveStreamTCPOut tcpout(s,30000,false);
  LineTerminal lterm(tcpin,tcpout);
  lterm.ReadLine();
  lterm.SendLine("AUTHINFO USER bredy");
  lterm.ReadLine();
  lterm.SendLine("AUTHINFO PASS 1-MXQst\\51");
  lterm.ReadLine();
  atable.FormatMemory();

  ArticleTableReader reader(atable);
  reader.ReadOverviewHeader(lterm);
  reader.DownloadGroup("bistudio.general",lterm,1);
  
  */


  atable.IndexDatabase();
/*  ArticleRecordset<AStringIndex> tree=atable.QueryString(ArticleDatabase::QueryReferences,"",NULL,true);
  TreeViewTest(atable,tree,0);*/
  ArticleHeader hdr;  
  DMMHANDLE hndl= 1487480;
  hdr.threadID.SetRef(hndl);
  ArticleRecordset<ADMMHandleIndex> data=atable.GetThread(hdr);
  while (data.FindNext(hdr))
	ShowArticle(hdr,0);
  getche();
  return 0;
  }

