# Microsoft Developer Studio Project File - Name="ArticleDB" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Console Application" 0x0103

CFG=ArticleDB - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "ArticleDB.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "ArticleDB.mak" CFG="ArticleDB - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "ArticleDB - Win32 Release" (based on "Win32 (x86) Console Application")
!MESSAGE "ArticleDB - Win32 Debug" (based on "Win32 (x86) Console Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""$/Projects/NewsReader/ArticleDB", JNUAAAAA"
# PROP Scc_LocalPath "."
CPP=cl.exe
RSC=rc.exe

!IF  "$(CFG)" == "ArticleDB - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_CONSOLE" /D "_MBCS" /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /W3 /GX /O2 /D "NDEBUG" /D "WIN32" /D "_CONSOLE" /D "_MBCS" /D "USESOCKETCLASS" /YX"stdafx.h" /FD /c
# ADD BASE RSC /l 0x405 /d "NDEBUG"
# ADD RSC /l 0x405 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /machine:I386
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib ws2_32.lib /nologo /subsystem:console /machine:I386

!ELSEIF  "$(CFG)" == "ArticleDB - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_CONSOLE" /D "_MBCS" /Yu"stdafx.h" /FD /GZ /c
# ADD CPP /nologo /W3 /Gm /GX /ZI /Od /D "_DEBUG" /D "WIN32" /D "_CONSOLE" /D "_MBCS" /D "USESOCKETCLASS" /YX"stdafx.h" /FD /GZ /c
# ADD BASE RSC /l 0x405 /d "_DEBUG"
# ADD RSC /l 0x405 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib /nologo /subsystem:console /debug /machine:I386 /pdbtype:sept
# ADD LINK32 kernel32.lib user32.lib gdi32.lib winspool.lib comdlg32.lib advapi32.lib shell32.lib ole32.lib oleaut32.lib uuid.lib odbc32.lib odbccp32.lib ws2_32.lib /nologo /subsystem:console /debug /machine:I386 /pdbtype:sept

!ENDIF 

# Begin Target

# Name "ArticleDB - Win32 Release"
# Name "ArticleDB - Win32 Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=..\archivestream.cpp
# End Source File
# Begin Source File

SOURCE=..\archivestreammemory.cpp
# End Source File
# Begin Source File

SOURCE=..\ArchiveStreamTCP.cpp
# End Source File
# Begin Source File

SOURCE=.\ArticleDatabase.cpp
# End Source File
# Begin Source File

SOURCE=.\ArticleDB.cpp
# End Source File
# Begin Source File

SOURCE=.\ArticleDBIndexes.cpp
# End Source File
# Begin Source File

SOURCE=.\ArticleTable.cpp
# End Source File
# Begin Source File

SOURCE=.\ArticleTableReader.cpp
# End Source File
# Begin Source File

SOURCE=..\charmapcz.cpp
# End Source File
# Begin Source File

SOURCE=.\DiskMappedMemory.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\EL\TCPIPBasics\IPA.cpp
# End Source File
# Begin Source File

SOURCE=..\LineTerminal2.cpp
# End Source File
# Begin Source File

SOURCE=..\..\..\EL\TCPIPBasics\SharedResource.cpp
# End Source File
# Begin Source File

SOURCE=.\StdAfx.cpp
# ADD CPP /Yc"stdafx.h"
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=..\ArchiveStream.h
# End Source File
# Begin Source File

SOURCE=..\ArchiveStreamMemory.h
# End Source File
# Begin Source File

SOURCE=..\ArchiveStreamTCP.h
# End Source File
# Begin Source File

SOURCE=.\ArticleDatabase.h
# End Source File
# Begin Source File

SOURCE=.\ArticleDBIndexes.h
# End Source File
# Begin Source File

SOURCE=.\ArticleIndex.h
# End Source File
# Begin Source File

SOURCE=.\ArticleTable.h
# End Source File
# Begin Source File

SOURCE=.\ArticleTableReader.h
# End Source File
# Begin Source File

SOURCE=.\BTree.h
# End Source File
# Begin Source File

SOURCE=.\BTreeMemory.h
# End Source File
# Begin Source File

SOURCE=..\CharMapCz.h
# End Source File
# Begin Source File

SOURCE=.\DiskMappedMemory.h
# End Source File
# Begin Source File

SOURCE=..\..\..\EL\TCPIPBasics\IPA.h
# End Source File
# Begin Source File

SOURCE=..\LineTerminal2.h
# End Source File
# Begin Source File

SOURCE=..\..\..\EL\TCPIPBasics\SharedResource.h
# End Source File
# Begin Source File

SOURCE=..\..\..\EL\TCPIPBasics\Socket.h
# End Source File
# Begin Source File

SOURCE=.\StdAfx.h
# End Source File
# Begin Source File

SOURCE=..\..\..\EL\TCPIPBasics\WSA.h
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# End Group
# Begin Source File

SOURCE=.\ReadMe.txt
# End Source File
# End Target
# End Project
