// ArticleTableReader.cpp: implementation of the ArticleTableReader class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "../LineTerminal2.h"
#include "ArticleTableReader.h"
#include "ArticleTable.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

ArticleTableReader::ArticleTableReader(ArticleTable &table):_table(table)
{

}

char *ArticleTableReader::HeaderNames[ArticleTableReader::OVEnd]=
  {
  "Subject",
	"From",
	"Date",
	"Message-ID",
	"References",
	"Lines",
	"Content-type",
	"X-Delete",
	"Icon",
	"Newsgroups"
  };
  

int ReadStatus(LineTerminal &lTerm, char **msg=NULL);

static int ReadStatus(LineTerminal &lTerm, char **msg)
  {
  int ret=-1;
  const char *c=lTerm.ReadLine();
  if (c==NULL) return ret;
  sscanf(c,"%d",&ret);
  if (msg) lTerm.SplitAt(c,NULL,msg,' ');
  return ret;
  }

bool ArticleTableReader::ReadOverviewHeader(LineTerminal &lTerm)
  {
  if (!lTerm.SendLine("LIST OVERVIEW.FMT")) return false;
  memset(_OVStatus,0,sizeof(_OVStatus));
  if (ReadStatus(lTerm)!=215) return false;
  const char *line=lTerm.ReadLine();
  int order=0;
  while (line && line[0]!='.')
	{
	char *left,*right;
	lTerm.SplitAt(line,&left,&right,':');
	int fnd;
	for (fnd=OVSubject;fnd<OVEnd;fnd++)
	  if (stricmp(HeaderNames[fnd],left)==0) break;
	if (fnd!=OVEnd)
	  {
	  _OVStatus[fnd]=stricmp(right,"full")==0?OVS_Full:OVS_Standard;
	  _OVOrder[fnd]=order;
	  }
	order++;
	line=lTerm.ReadLine();
	}
  return true;
  }

ArticleTableReader::ArticleInfo::ArticleInfo()
  {
  memset(this,0,sizeof(*this));
  }


ArticleTableReader::ArticleInfo::~ArticleInfo()
  {
  free(subject);
  free(from);
  free(messageid);
  free(references);
  free(content);
  free(xdelete);
  free(icon);
  }

int ArticleTableReader::DownloadGroup(const char *group, LineTerminal &lTerm, int from)
  {
  lTerm.SendLine("GROUP bistudio.general");
  const char *line=lTerm.ReadLine();
  int st,cnt,min,max;
  sscanf(line,"%d %d %d %d",&st,&cnt,&min, &max);
  max;
  cnt=(max+1)-from;
  if (cnt<=0) return from;  
  ArticleInfo *aatmp=new ArticleInfo[cnt];
  int realcnt=0;
  int i;
  lTerm.SendFormatted("XOVER %d-%d",from,max);
  for (i=0;i<OVEnd;i++)
	if (_OVStatus[i]==OVS_Missing)
	  lTerm.SendFormatted("XHDR %s %d-%d",HeaderNames[i],from,max);
  if (ReadStatus(lTerm)==224) 
	{
	while ((line=lTerm.ReadLine())!=NULL && line[0]!='.')
	  {
	  ArticleInfo &nfo=aatmp[realcnt];
	  char *param,*remain=const_cast<char *>(line);
	  int order=0;	  
	  lTerm.SplitAt(remain,&param,&remain,'\t',false);
	  nfo.index=atoi(param);
	  LoadProgress(nfo.index-from,cnt);
	  while (remain)
		{
		lTerm.SplitAt(remain,&param,&remain,'\t',false);
		int pos;
		for (pos=0;pos<OVEnd;pos++)
		  if (_OVOrder[pos]==order) break;
		if (pos!=OVEnd)		  
		  SetArticleParam(nfo, (OverViewHeaders)pos, param);
		order++;
		}
	  realcnt++;
	  }
	}
  for (i=0;i<OVEnd;i++)
	if (_OVStatus[i]==OVS_Missing)
	  {
	  if (ReadStatus(lTerm)==221) 
		{
		int curindex=0;		
		while ((line=lTerm.ReadLine())!=NULL && line[0]!='.')
		  {
		  char *param,*remain=const_cast<char *>(line);
		  lTerm.SplitAt(remain,&param,&remain,' ');
		  int index=atoi(param);
		  while (aatmp[curindex].index<index && curindex<realcnt)
			curindex++;
		  if (aatmp[curindex].index==index)
			{
			SetArticleParam(aatmp[curindex],(OverViewHeaders)i,remain);
			}
		  }
		}
	  }
  if (realcnt==0) return -1;
  for (i=0;i<realcnt;i++)
	{
	if (OnBeforeLoad(aatmp[i].messageid,group)==true) continue;
	ArticleInfo &nfo=aatmp[i];
	ArticleHeaderFlags flags;
	flags._storage=0;
	flags.dwnnew=1;
	flags.ntnew=1;
	flags.unread=1;

	ArticleDateTime datetime;
	datetime.ParseUnixTime(nfo.date);

	ArticleAttachmentMark attachment=attchNone;		
	ArticleWatchMark watch=aWatchNormal;

	DMMHANDLE handle=0;

	ArticleHeader ahdr;
	ahdr.attachment.SetRef(attachment);
	ahdr.dateNewest.SetRef(datetime);
	ahdr.datePost.SetRef(datetime);
	ahdr.dateRead.SetRef(datetime);
	ahdr.flags.SetRef(flags);
	ahdr.watchMark.SetRef(watch);
	ahdr.lines.SetRef(nfo.lines);
	ahdr.threadID.SetRef(handle);
	
	if (nfo.content) ahdr.content=nfo.content;
	if (nfo.from) ahdr.from=nfo.from;
	if (nfo.subject) ahdr.subject=nfo.subject;
	if (nfo.messageid) ahdr.msgid=nfo.messageid;
	if (nfo.groups) ahdr.grouplist=nfo.groups;
	if (nfo.icon) ahdr.icon=nfo.icon;
	if (nfo.references) ahdr.reference=nfo.references;
	if (nfo.xdelete) ahdr.xdelete=nfo.xdelete;
	char *prop=strstr(nfo.subject,"#$");
	if (prop!=NULL)
	  {
	  *prop=0;
	  ahdr.properties=prop+2;
	  }
	prop=strrchr(ahdr.reference,' ');
	if (prop) 
	  ahdr.reference=prop+1;
	ArticleHeader nwart;
	handle=_table.CreateArticle(ahdr,&nwart);
	OnAfterLoad(handle,nwart);
	}

  delete [] aatmp;
  return max+1;  
  }

void ArticleTableReader::SetArticleParam(ArticleInfo &nfo, OverViewHeaders hdr, const char *param)
{
		  switch (hdr)
			{
			case OVSubject:free(nfo.subject);nfo.subject=strdup(param);break;
			case OVFrom:free(nfo.from);nfo.from=strdup(param);break;
			case OVDate:free(nfo.date);nfo.date=strdup(param);break;
			case OVMessageID:free(nfo.messageid);nfo.messageid=strdup(param);break;
			case OVReferences:free(nfo.references);nfo.references=strdup(param);break;
			case OVLines:nfo.lines=-1;sscanf(param,"%d",&nfo.lines);break;
			case OVContentType:free(nfo.content);nfo.content=strdup(param);break;			  
			case OVXDelete:free(nfo.xdelete);nfo.xdelete=strdup(param);break;			  
			case OVIcon:free(nfo.icon);nfo.icon=strdup(param);break;
			case OVGroups:free(nfo.groups);nfo.groups=strdup(param);break;
			}			  
}
