// ArticleTable.cpp: implementation of the ArticleTable class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ArticleTable.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

ArticleHeader::ArticleHeader(const ArticleHeader &other)
  {
  if (other._handle)
	{
	flags.SetRef(other.flags);
	datePost.SetRef(other.datePost);
	dateRead.SetRef(other.dateRead);
	dateNewest.SetRef(other.dateNewest);
	attachment.SetRef(other.attachment);
	watchMark.SetRef(other.watchMark);
	lines.SetRef(other.lines);
	threadID.SetRef(other.threadID);
	msgid=other.msgid;
	reference=other.reference;
	from=other.from;
	subject=other.subject;
	properties=other.properties;
	xdelete=other.xdelete;
	icon=other.icon;
	content=other.content;
	grouplist=other.grouplist;
	_handle=other._handle;
	_base=other._base;
	}
  else
	{
	other.CreateCopy(*this);
	}
  }

void ArticleHeader::LoadHeader(void *data)
  {
  const char *ptr=(char *)data;
  flags.SetRef((ArticleHeaderFlags *)ptr);ptr+=flags.Size();
  lines.SetRef((size_t *)ptr);ptr+=lines.Size();
  threadID.SetRef((DMMHANDLE *)ptr);ptr+=threadID.Size();
  datePost.SetRef((ArticleDateTime *)ptr);ptr+=datePost.Size();
  dateRead.SetRef((ArticleDateTime *)ptr);ptr+=dateRead.Size();
  dateNewest.SetRef((ArticleDateTime *)ptr);ptr+=dateNewest.Size();
  attachment.SetRef((ArticleAttachmentMark *)ptr);ptr+=attachment.Size();
  watchMark.SetRef((ArticleWatchMark *)ptr);ptr+=watchMark.Size();
  from=ptr;ptr=strchr(ptr,0)+1;
  subject=ptr;ptr=strchr(ptr,0)+1;
  msgid=ptr;ptr=strchr(ptr,0)+1;
  reference=ptr;ptr=strchr(ptr,0)+1;
  properties=ptr;ptr=strchr(ptr,0)+1;
  xdelete=ptr;ptr=strchr(ptr,0)+1;
  icon=ptr;ptr=strchr(ptr,0)+1;
  content=ptr;ptr=strchr(ptr,0)+1;
  grouplist=ptr;ptr=strchr(ptr,0)+1;
  _base=data;
  _handle=0;
  }

void ArticleHeader::CreateCopy(ArticleHeader &target,void *allocSpace) const
  {
  char *buff=(char *)(allocSpace?allocSpace:malloc(CalculateMemUsage()));
  char *ptr=buff;
  target.flags.SetRef(*(ArticleHeaderFlags *)ptr=flags);ptr+=flags.Size();
  target.lines.SetRef(*(size_t *)ptr=(int)lines);ptr+=lines.Size();
  target.threadID.SetRef(*(DMMHANDLE *)ptr=(DMMHANDLE)threadID);ptr+=threadID.Size();
  target.datePost.SetRef(*(ArticleDateTime *)ptr=datePost);ptr+=datePost.Size();
  target.dateRead.SetRef(*(ArticleDateTime *)ptr=dateRead);ptr+=dateRead.Size();
  target.dateNewest.SetRef(*(ArticleDateTime *)ptr=dateNewest);ptr+=dateNewest.Size();
  target.attachment.SetRef(*(ArticleAttachmentMark *)ptr=attachment);ptr+=attachment.Size();
  target.watchMark.SetRef(*(ArticleWatchMark *)ptr=watchMark);ptr+=watchMark.Size();
  target.from=strcpy(ptr,from);ptr=strchr(ptr,0)+1;
  target.subject=strcpy(ptr,subject);ptr=strchr(ptr,0)+1;
  target.msgid=strcpy(ptr,msgid);ptr=strchr(ptr,0)+1;
  target.reference=strcpy(ptr,reference);ptr=strchr(ptr,0)+1;
  target.properties=strcpy(ptr,properties);ptr=strchr(ptr,0)+1;
  target.xdelete=strcpy(ptr,xdelete);ptr=strchr(ptr,0)+1;
  target.icon=strcpy(ptr,icon);ptr=strchr(ptr,0)+1;
  target.content=strcpy(ptr,content);ptr=strchr(ptr,0)+1;
  target.grouplist=strcpy(ptr,grouplist);ptr=strchr(ptr,0)+1;
  target._base=buff;
  target._handle=0;
  }

ArticleTable::ArticleTable()
{

}

ArticleTable::~ArticleTable()
{

}


DMMHANDLE ArticleTable::CreateArticle(ArticleHeader &article,ArticleHeader *acopy)
  {
  size_t sz=article.CalculateMemUsage();
  DMMHANDLE handle=AllocMem(sz);
  void *mem;
  Map(handle,mem);
  ArticleHeader acopytmp(false);
  if (acopy==NULL) acopy=&acopytmp;
  article.CreateCopy(*acopy,mem);
  acopy->_handle=handle;
  return handle;
  }

ArticleHeader ArticleTable::GetArticle(DMMHANDLE handle)
  {
  ArticleHeader hdr(false);
  GetArticle(handle,hdr);
  return hdr;
  }

void ArticleTable::GetArticle(DMMHANDLE handle, ArticleHeader &hdr)
  {
  void *mem;
  Map(handle,mem);
  hdr.LoadHeader(mem);
  hdr._handle=handle;
  hdr._base=mem;
  }


bool ArticleTable::DeleteArticle(ArticleHeader &article)
  {
  if (article._handle)
	DeallocMem(article._handle);
  else if (article._base)
	free(article._base);
  article._handle=0;
  article._base=NULL;
  return true;
  }

bool ArticleTable::EnumArticles(ArticleHeader &article)
  {
  DMMHANDLE next;
  do
	{
	next=EnumBlocks(article._handle);
	if (next==0)
	  {
	  article._base=NULL;
	  article._handle=next;
	  return false;
	  }
	}
  while (!this->GetBlockUsed(next));
  GetArticle(next,article);
  return true;
  }

//--------------------------------------------------

static char Months[12][4]=
  {"Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"};

//--------------------------------------------------

static int Dayz[12]=
  {31,29,31,30,31,30,31,31,30,31,30,31};

//--------------------------------------------------

static char Days[7][4]=
  {"Sun","Mon","Tue","Wen","Thr","Fri","Sat"};

//--------------------------------------------------
bool ArticleDateTime::ParseUnixTime(const char *date)
  {
  while (*date && !isdigit(*date)) date++;
  int day=0;
  while (*date && isdigit(*date)) day=day*10+(*(date++)-'0');
  char mnthstr[4];
  int i;
  while (*date && !isalnum(*date)) date++;
  for (i=0;*date && isalnum(*date) && i<3;i++) mnthstr[i]=*date++;
  mnthstr[3]=0;
  for (i=0;i<12;i++) if (stricmp(Months[i],mnthstr)==0) break;
  if (i==12) i=0;
  int month=i+1;
  while (*date && !isdigit(*date)) date++;
  int year=0;
  while (*date && isdigit(*date)) year=year*10+(*(date++)-'0');
  if (year<100)
    if (year<50) year+=2000;
  else year+=1900;
  while (*date && !isdigit(*date)) date++;
  int hour=0;
  while (*date && isdigit(*date)) hour=hour*10+(*(date++)-'0');
  while (*date && !isdigit(*date)) date++;
  int min=0;
  while (*date && isdigit(*date)) min=min*10+(*(date++)-'0');
  while (*date && !isdigit(*date)) date++;
  int sec=0;
  while (*date && isdigit(*date)) sec=sec*10+(*(date++)-'0');
  bool plus=true;
  while (*date && *date==' ') date++;
  if (*date=='+') plus=true;
  if (*date=='-') plus=false;
  int offset=0;  
  date++;
  sscanf(date,"%d",&offset);
  if (!plus)
    {
    min+=offset%100;
    hour+=offset/100;
    while (min>59) 
      {hour++;min-=60;}
    while (hour>23) 
      {day++;hour-=24;}
    while (day>Dayz[month-1]) 
      {day-=Dayz[month-1];month++;}
    while (month>12) 
      {year++;month-=12;}
    }
  else
    {
    min-=offset%100;
    hour-=offset/100;
    while (min<0) 
      {hour--;min+=60;}
    while (hour<0) 
      {day--;hour+=24;}
    while (day<1) 
      {month--;day+=Dayz[month-1];}
    while (month<1) 
      {year--;month+=12;}
    }
  this->year=year-1900;
  this->month=month;
  this->day=day;
  this->hour=hour;
  this->minute=min;
  this->second=sec;
  return true;
  }