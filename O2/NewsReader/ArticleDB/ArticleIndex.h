// ArchiveIndex.h: interface for the ArchiveIndex class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_ARCHIVEINDEX_H__D055809B_1267_4BF5_8C80_719C211D14C8__INCLUDED_)
#define AFX_ARCHIVEINDEX_H__D055809B_1267_4BF5_8C80_719C211D14C8__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "ArticleDBHeader.h"
#include "ArticleTable.h"
#include "BTreeMemory.h"
#include "BTree.h"
#include "IArticleRecordSet.h"


extern THREAD_DECL(ArticleTable) *currentDB;

template<class T, class CmpTraits, bool reverse=false>
class ASimpleIndexRecord
  {  
  DMMHANDLE handle;	//handle of article indexed - if zeroed or 1, dataptr is defined
  union
	{
	unsigned long offset; //when handle is defined, contain offset of indexed value from begin of article 
	const T *dataptr;		  //when handle is zero or 1, contain pointer to data being found
	};
  public:
	ASimpleIndexRecord():handle(0),offset(0) {}
	ASimpleIndexRecord(DMMHANDLE handle, DMMHANDLE field):handle(handle),offset(field-handle) {}
	ASimpleIndexRecord(const ArticleTable &table, const ArticleHeader &hdr, const T *field):
            handle(hdr.GetHandle()),offset(table.Map(field)-handle) {}
	ASimpleIndexRecord(const T *data, bool exact=false):handle(exact!=false),dataptr(data) {}
	DMMHANDLE GetResult() {return handle;}
	void GetResult(ArticleHeader &hdr) 
	  {
	  DMMHANDLE handle=GetResult();
	  currentDB->GetArticle(handle,hdr);	  
	  }
	bool operator>(const ASimpleIndexRecord<T,CmpTraits,reverse> &other) const {return Compare(other)>0;}
	bool operator<(const ASimpleIndexRecord<T,CmpTraits,reverse> &other) const {return Compare(other)<0;}
	bool operator==(const ASimpleIndexRecord<T,CmpTraits,reverse> &other) const {return Compare(other)==0;}
	bool operator<=(const ASimpleIndexRecord<T,CmpTraits,reverse> &other) const {return Compare(other)<=0;}
	bool operator>=(const ASimpleIndexRecord<T,CmpTraits,reverse> &other) const {return Compare(other)>=0;}
	bool operator!=(const ASimpleIndexRecord<T,CmpTraits,reverse> &other) const {return Compare(other)!=0;}
	const T *GetIndex() const
	  {
	  T *ret;
	  if (handle<2) return dataptr;
	  else currentDB->Map(handle+offset,ret);
	  return ret;
	  }
	int Compare(const ASimpleIndexRecord<T,CmpTraits,reverse> &other) const;
	int CompareNoUnique(const ASimpleIndexRecord<T,CmpTraits,reverse> &other) const;
  };


template<class T, class CmpTraits, bool reverse>
int ASimpleIndexRecord<T,CmpTraits,reverse>::Compare(const ASimpleIndexRecord<T,CmpTraits,reverse> &other) const
	  {
	  if (handle==other.handle && handle>1 && offset==other.offset) return 0;
	  if (handle==0 && offset==0) return -1;
	  if (other.handle==0 && other.offset==0) return 1;
	  ASSERT(handle>1 || other.handle>1);
	  if (handle<2) return -other.Compare(*this);

	  const T *t1,*t2;
	  t1=GetIndex();
	  t2=other.GetIndex();
	  int res=CmpTraits::Compare(t1,t2,other.handle!=0);
	  if (res) return res;
	  if (other.handle<2) return 1;
  	  ArticleHeader hdr1;
	  ArticleHeader hdr2;
	  currentDB->GetArticle(handle,hdr1);
	  currentDB->GetArticle(other.handle,hdr2);
	  res=hdr1.ahst->datePost.Compare(hdr2.ahst->datePost);
	  if (res) return reverse?-res:res;
	  if (reverse)
		return (offset>other.offset)-(offset<other.offset);
	  else
		return -(offset>other.offset)+(offset<other.offset);
	  }

template<class T, class CmpTraits, bool reverse>
int ASimpleIndexRecord<T,CmpTraits,reverse>::CompareNoUnique(const ASimpleIndexRecord<T,CmpTraits,reverse> &other) const
	  {
	  ASSERT(handle>1 || other.handle>1);
	  if (handle==0) return other.Compare(*this);

	  const T *t1,*t2;
	  t1=GetIndex();
	  t2=other.GetIndex();
	  return CmpTraits::Compare(t1,t2,other.handle!=0);
	  }



template<class T> //T is type of ASimpleIndexRecord
class ArticleRecordset:public BTreeIterator<T>,public IArticleRecordset
    {
	T _itemMin;
	T _itemMax;
public:
	ArticleRecordset(const BTree<T> &indexFile, ArticleTable *curDB)
	  :BTreeIterator<T>(indexFile)
	  {
	  currentDB=curDB;
	  }

	ArticleRecordset(const BTree<T> &indexFile, ArticleTable *curDB, T &search)
	  :BTreeIterator<T>(indexFile)
	  {
	  currentDB=curDB;
	  BeginSearch(search);
	  }
	
	ArticleRecordset(const BTree<T> &indexFile, ArticleTable *curDB, T &minRange, T &maxRange)
	  :BTreeIterator<T>(indexFile)
	  {
	  currentDB=curDB;
	  BeginSearch(minRange,maxRange);
	  }
	
	void BeginSearch(T &search)
	  {
	  _itemMin=search;
	  _itemMax=search;
	  BeginFrom(_itemMin);
	  }
	void BeginSearch(T &rangeMin, T &rangeMax)
	  {
	  _itemMin=rangeMin;
	  _itemMax=rangeMax;
	  BeginFrom(_itemMin);
	  }
	T *FindNext()
	  {
	  T *next=Next();
	  if (next==NULL) return NULL;
	  else if (next->CompareNoUnique(_itemMax)>0) return NULL;
	  if (next==CurrentTree().Largest() && next->CompareNoUnique(_itemMax)) 
		return NULL;
	  return next;
	  }
	bool FindNext(ArticleHeader &hdr)
	  {
	  T *found=FindNext();
	  if (found==NULL) return false;
	  found->GetResult(hdr);
	  return true;
	  }
};

#endif // !defined(AFX_ARCHIVEINDEX_H__D055809B_1267_4BF5_8C80_719C211D14C8__INCLUDED_)
