// ArticleDatabase.cpp: implementation of the ArticleDatabase class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ArticleDatabase.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

void SetProgress(unsigned long cur, unsigned long max);

ArticleTable *currentDB=NULL;

ArticleDatabase::ArticleDatabase():
  _fieldFrom(50),
  _fieldSubject(50),
  _fieldMessageID(50),
  _fieldReferences(50),
  _fieldThread (50),
  _fieldReadDate(50),
  _fieldDate(50),
  _fieldGroups(50)
{
}

ArticleDatabase::~ArticleDatabase()
{

}

void ArticleDatabase::IndexArticle(ArticleHeader &hdr)
  {
  ASSERT(hdr._handle!=0);
  currentDB=this;   
  
  AStringIndex FieldMessageID(hdr._handle,Map(hdr.msgid));
  AStringIndex FieldReferences(hdr._handle,Map(hdr.reference));
  ADateIndex FieldReadDate(hdr._handle,Map(hdr.dateRead.Ptr()));
  ADateIndex NewestDate(hdr._handle,Map(hdr.dateNewest.Ptr()));;

  IndexSubstrings(_fieldFrom,hdr.from,hdr._handle);
  IndexSubstrings(_fieldSubject,hdr.subject,hdr._handle);
  IndexNewsgroups(_fieldGroups,hdr.grouplist,hdr._handle);

  _fieldMessageID.Add(FieldMessageID);  
  _fieldReadDate.Add(FieldReadDate);
  _fieldDate.Add(NewestDate);

  ArticleRecordset<AStringIndex> refrcs(_fieldMessageID,this);
  hdr.threadID=hdr._handle;
  ArticleHeader thread=hdr;
  while (thread.reference[0])
	if (GetParentArticle(thread,&thread)==false) break;	
  if (hdr._handle==thread._handle && hdr.reference[0]) 
	FieldReferences=AStringIndex(hdr._handle,Map(strchr(hdr.reference,0)));
 _fieldReferences.Add(FieldReferences);  

  hdr.threadID=thread.threadID;

  _fieldThread.Add(ADMMHandleIndex(hdr._handle,Map(hdr.threadID.Ptr())));

  if (thread.dateNewest()<hdr.dateNewest())
	{	
	ADateIndex threadDate(thread._handle,Map(thread.dateNewest.Ptr()));
	_fieldDate.Remove(threadDate);	
	thread.dateNewest()=hdr.dateNewest();
	_fieldDate.Add(threadDate);
	}
  }

void ArticleDatabase::IndexDatabase()
  {
  for (ArticleHeader hdr;EnumArticles(hdr);)
	{
	IndexArticle(hdr);
	SetProgress(hdr._handle,GetUsed());
	}
  }

void ArticleDatabase::IndexSubstrings(BTree<AStringIndex> &indexTree, const char *text, DMMHANDLE article)
  {
  const char *trv=text;
  while (*trv && isspace(*trv)) trv++;
  do
	{
	AStringIndex stridx(article,Map(trv));
	indexTree.Add(stridx);
	while (*trv && isalnum(*trv)) trv++;
    while (*trv && !isalnum(*trv)) trv++;
	}
  while (*trv);
  }

BTree<AStringIndex> *ArticleDatabase::GetStringIndex(QueryStringEnum enm)
  {
  switch (enm)
	{	
	case QueryFrom: return &_fieldFrom;
	case QuerySubject: return &_fieldSubject;
	case QueryMessageID: return &_fieldMessageID;
	case QueryReferences: return &_fieldReferences;
	case QueryGroups: return &_fieldGroups;
	}
  return NULL;
  }	

void ArticleDatabase::IndexNewsgroups(BTree<AStringIndex> &indexTree, const char *text, DMMHANDLE article)
  {
  const char *trv=text;
  while (trv)
	{
	AStringIndex stridx(article,Map(trv));
	indexTree.Add(stridx);
	trv=strrchr(trv,',');
	if (trv) 
	  trv++;
	}
  }

ArticleRecordset<AStringIndex> ArticleDatabase::QueryString(QueryStringEnum querywhat, const char *minValue, const char *maxValue, bool exact)
  {
  BTree<AStringIndex> *index=GetStringIndex(querywhat);
  if (maxValue==NULL) maxValue=minValue;
  AStringIndex itemMin(minValue,exact);
  AStringIndex itemMax(maxValue,exact);
  return ArticleRecordset<AStringIndex>(*index,this,itemMin,itemMax);
  }

bool ArticleDatabase::GetParentArticle(ArticleHeader &ahdr,ArticleHeader *parent)
  {
  AStringIndex stridx(ahdr.reference,true),*found;
  ArticleRecordset<AStringIndex> rcrdset(_fieldMessageID,this,stridx);
  found=rcrdset.FindNext();
  if (found==NULL) return false;
  if (parent) found->GetResult(*parent);
  return true;
  }

ArticleRecordset<AStringIndex> ArticleDatabase::GetChilds(ArticleHeader &ahdr)
  {
  AStringIndex stridx(ahdr.msgid,true);
  return ArticleRecordset<AStringIndex>(_fieldReferences,this,stridx);
  }

ArticleRecordset<ADMMHandleIndex> ArticleDatabase::GetThread(ArticleHeader &ahdr)
  {
  ADMMHandleIndex index(ahdr.threadID.Ptr(),true);
  return ArticleRecordset<ADMMHandleIndex>(_fieldThread,this,index);
  }
