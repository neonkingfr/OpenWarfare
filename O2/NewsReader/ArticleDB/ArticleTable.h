// ArticleTable.h: interface for the ArticleTable class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_ARTICLETABLE_H__BB423E9C_EB4E_40B6_9E90_945124114006__INCLUDED_)
#define AFX_ARTICLETABLE_H__BB423E9C_EB4E_40B6_9E90_945124114006__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "DiskMappedMemory.h"

  union ArticleHeaderFlags
	{
	DWORD _storage;
	struct
	  {
	  bool opened:1; //!<article childs is opened
	  bool trash:1; //!<article is marked as deleted
	  bool hidden:1; //!<article cannot be seen
	  bool highlighted:1; //!< article is highlighted (as a result of filtering)
	  bool lgwarn:1; //!<article can be downloaded, if it is too long
	  bool ntnew:1; //!<notify new;
	  bool dwnnew:1; //!<new info for download
	  bool hidtoshow:1; //!<if true, hidden thread will be showen grayed
	  bool unread:1; //!<article is unread
	  };
	};

enum ArticleAttachmentMark {attchNone, attchDoKnow, attchOk};
enum ArticleWatchMark {aWatchLow=-1,aWatchNormal=0,aWatchHigh=1};

struct ArticleDateTime //In GMT
  {
  unsigned char year,month,day,hour,minute,second; //base for year is 1900 
  bool ParseUnixTime(const char *text);
  int Compare(const ArticleDateTime& other) const
	{
	int res;
	res=(year>other.year)-(year<other.year);if (res) return res;
	res=(month>other.month)-(month<other.month);if (res) return res;
	res=(day>other.day)-(day<other.day);if (res) return res;
	res=(hour>other.hour)-(hour<other.hour);if (res) return res;
	res=(minute>other.minute)-(minute<other.minute);if (res) return res;
	res=(second>other.second)-(second<other.second);return res;
	}
  bool operator==(const ArticleDateTime &other) const {return Compare(other)==0;}
  bool operator>=(const ArticleDateTime &other) const {return Compare(other)>=0;}
  bool operator<=(const ArticleDateTime &other) const {return Compare(other)<=0;}
  bool operator!=(const ArticleDateTime &other) const {return Compare(other)!=0;}
  bool operator>(const ArticleDateTime &other) const {return Compare(other)>0;}
  bool operator<(const ArticleDateTime &other) const {return Compare(other)<0;}
  };
#pragma warning( disable : 4284)

template<class T>
class XRefExt
  {
  T *_ptr;
  public:
	XRefExt(T *ptr=NULL): _ptr(ptr) {}
	XRefExt(T &obj): _ptr(&obj) {}
	XRefExt(const XRefExt<T>& other): _ptr(other._ptr) {}

	XRefExt &operator=(const XRefExt<T>& other)
	  {
	  *_ptr=*other._ptr;
	  return *this;
	  }
	
	XRefExt &operator=(const T& other)
	  {
	  *_ptr=other;
	  return *this;
	  }

	void SetRef(T *ptr) {_ptr=ptr;}
	void SetRef(T &ptr) {_ptr=&ptr;}
	void SetRef(const XRefExt<T> &other) {_ptr=other._ptr;}

	T& operator() () {return *_ptr;}
	const T& operator() () const {return *_ptr;}
	operator T& () {return *_ptr;}
	operator const T&() const {return *_ptr;}
	T *operator->() {return _ptr;}
	const T *operator->() const {return _ptr;}

	T& Ref() {return *_ptr;}
	const T& Ref() const {return *_ptr;}
	T* Ptr() {return _ptr;}
	const T* Ptr() const {return _ptr;}

	bool operator==(const XRefExt<T> &other) const {return _ptr==other._ptr;}
	bool operator!=(const XRefExt<T> &other) const {return _ptr!=other._ptr;}
	bool IsNull() const {return _ptr==NULL;}
	static size_t Size() {return sizeof(T);}
  };


struct ArticleHeader
  {
  XRefExt<ArticleHeaderFlags> flags;
  const char *msgid;	 //!current article message id
  const char *reference; //!current article reference id
  const char *from;		 //!from email
  const char *subject;	 //!subject name (without properties)
  const char *properties;//!subject name (without properties)
  const char *xdelete;	 //!xdelete header value
  const char *icon;		 //!icon value
  const char *content;	 //!content type;
  const char *grouplist; //groups, that contains this article;
  XRefExt<ArticleDateTime> datePost; //!date posted
  XRefExt<ArticleDateTime> dateRead; //!date read
  XRefExt<ArticleDateTime> dateNewest; //!<date of newest article in thread
  XRefExt<ArticleAttachmentMark> attachment;
  XRefExt<ArticleWatchMark> watchMark;
  XRefExt<size_t> lines;
  XRefExt<DMMHANDLE> threadID;	//id first article of this thread - calculates during indexing
 
  void *_base;		  
  DMMHANDLE _handle;
  
  ArticleHeader(bool init=true): _base(NULL),_handle(NULL)
	{
	if (init)
	  {	  
	  msgid=reference=from=subject=properties=xdelete=icon=content=grouplist="";
	  }
	}
  ArticleHeader(void *base): _base(base),_handle(NULL) {}
  ArticleHeader(const CDiskMappedMemory &dmm, unsigned long handle): _handle(handle) 
	{dmm.Map(_handle,_base);}

  ~ArticleHeader()
	{
	if (_handle==0)
	  if (_base)
		free(_base);
	}  
  size_t CalculateMemUsage() const
	{
	size_t sz=
	sz=flags.Size()+datePost.Size()+dateRead.Size()+dateNewest.Size()+attachment.Size()+
		watchMark.Size()+lines.Size()+threadID.Size()+
		strlen(msgid)+1+
		strlen(properties)+1+
		strlen(reference)+1+
		strlen(from)+1+
		strlen(subject)+1+
		strlen(xdelete)+1+
		strlen(icon)+1+
		strlen(content)+1+
		strlen(grouplist)+1;
	sz=(sz+4) & ~0x3;
	return sz;
	}
  ArticleHeader(const ArticleHeader &other);
  void LoadHeader(void *data);
  void CreateCopy(ArticleHeader &target, void *allocSpace=NULL) const;  
  };


#define AT_MAXHEADERS 10
 
class ArticleTable : public CDiskMappedMemory  
{
public:
	ArticleHeader GetArticle(DMMHANDLE handle);
	void GetArticle(DMMHANDLE handle, ArticleHeader &hdr);
	DMMHANDLE CreateArticle(ArticleHeader &article, ArticleHeader *acopy=NULL);
	bool DeleteArticle(ArticleHeader &article);

	ArticleHeader operator() (DMMHANDLE handle)
	  {return GetArticle(handle);}

	bool EnumArticles(ArticleHeader &article);
	

	ArticleTable();
	virtual ~ArticleTable();

};

#endif // !defined(AFX_ARTICLETABLE_H__BB423E9C_EB4E_40B6_9E90_945124114006__INCLUDED_)
