#ifndef _ARTICLEDBINDEXES_H_
#define _ARTICLEDBINDEXES_H_

#include "ArticleIndex.h"

struct ArticleDateTime;


class CmpSubstring
  {
  public:
	static int Compare(const char *src, const char *substr, bool exact);
  };



class CmpDateTime
  {
  public:
	static int Compare(const ArticleDateTime *src, const ArticleDateTime *test, bool exact);
  };


class CmpDMMHandle
  {
  public:
	static int Compare(const DMMHANDLE *src, const DMMHANDLE *test, bool exact)
	  {
	  return (*src>*test)-(*src<*test);
	  }
  };

#endif