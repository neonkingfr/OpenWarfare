#if !defined(AFX_GETURLDLG_H__D97EB535_F53F_4822_B0DD_80E1495F342A__INCLUDED_)
#define AFX_GETURLDLG_H__D97EB535_F53F_4822_B0DD_80E1495F342A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// GetUrlDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CGetUrlDlg dialog

class CGetUrlDlg : public CDialog
{
// Construction
public:
	CGetUrlDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CGetUrlDlg)
	enum { IDD = IDD_GETURL };
	CButton	wOk;
	CEdit	wUrl;
	CString	m_url;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CGetUrlDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CGetUrlDlg)
	afx_msg void OnChangeUrl();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_GETURLDLG_H__D97EB535_F53F_4822_B0DD_80E1495F342A__INCLUDED_)
