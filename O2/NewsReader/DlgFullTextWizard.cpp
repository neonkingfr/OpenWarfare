// DlgFullTextWizard.cpp : implementation file
//

#include "stdafx.h"
#include "newsreader.h"
#include "DlgFullTextWizard.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// DlgFullTextWizard1 dialog


DlgFullTextWizard1::DlgFullTextWizard1(CWnd* pParent /*=NULL*/)
	: CDialog(DlgFullTextWizard1::IDD, pParent)
{
	//{{AFX_DATA_INIT(DlgFullTextWizard1)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void DlgFullTextWizard1::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(DlgFullTextWizard1)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(DlgFullTextWizard1, CDialog)
	//{{AFX_MSG_MAP(DlgFullTextWizard1)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// DlgFullTextWizard1 message handlers
