// CompletteThreadDlg.cpp : implementation file
//

#include "stdafx.h"
#include "newsreader.h"
#include "CompletteThreadDlg.h"
#include "NewsTerminal.h"
#include "NewsAccount.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CCompletteThreadDlg dialog


CCompletteThreadDlg::CCompletteThreadDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CCompletteThreadDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CCompletteThreadDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CCompletteThreadDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCompletteThreadDlg)
	DDX_Control(pDX, IDC_STATUS, status);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CCompletteThreadDlg, CDialog)
	//{{AFX_MSG_MAP(CCompletteThreadDlg)
	ON_BN_CLICKED(IDC_STOP, OnStop)
    ON_MESSAGE(WM_APP,OnCountMessages)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCompletteThreadDlg message handlers

static UINT CallThread(LPVOID pp)
  {
  #ifndef _AFXDLL	
  AFX_MODULE_THREAD_STATE *state=AfxGetModuleThreadState();
  if (state->m_pmapSocketHandle==NULL)
    {
    state->m_pmapSocketHandle=new CMapPtrToPtr();
    state->m_pmapDeadSockets=new CMapPtrToPtr();
    state->m_plistSocketNotifications=new CPtrList();
    }
  #endif
  CCompletteThreadDlg *dlg=(CCompletteThreadDlg *)pp;
  dlg->Thread();
  dlg->PostMessage(WM_COMMAND,IDCANCEL);
  return 0;
  }

BOOL CCompletteThreadDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
    cntmsg=0;
	
    AfxBeginThread(CallThread,this);
    
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CCompletteThreadDlg::Thread()
  {
  CSingleLock savelock(&acc->mutex);
  if (savelock.Lock(10000)==FALSE)
    {NrMessageBox(IDS_LOCKFAILED);return;}
  CRWLockGuard guard(articleLock,true);
  if (guard.LockPM(10000)==FALSE)
    {NrMessageBox(IDS_LOCKFAILED);return;}
  acc->CompletteThread(grp,beg,*this);
  }

void CCompletteThreadDlg::OnStop() 
{
CNewsTerminal::StopTrafic();
}

LRESULT CCompletteThreadDlg::OnCountMessages(WPARAM wParam,LPARAM lParam)
  {
  cntmsg++;
  CString s;
  s.Format(IDS_LOADEDMESSAGES,cntmsg);
  this->status.SetWindowText(s);
  return 0;
  }

void CCompletteThreadDlg::OnOK()
  {

  }
