// AutoDeleteDlg.cpp : implementation file
//

#include "stdafx.h"
#include "newsreader.h"
#include "AutoDeleteDlg.h"
#include "MainFrm.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CAutoDeleteDlg dialog


CAutoDeleteDlg::CAutoDeleteDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CAutoDeleteDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CAutoDeleteDlg)
	vDelEnabled = FALSE;
	vDayInterval = 360;
	vAutoMark = FALSE;
	//}}AFX_DATA_INIT
}


void CAutoDeleteDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAutoDeleteDlg)
	DDX_Control(pDX, IDC_DAYINTERVAL, wDelInterval);
	DDX_Control(pDX, IDC_GROUPLIST, wGroupList);
	DDX_Control(pDX, IDC_FILTER, wFilter);
	DDX_Control(pDX, IDC_DELETEENABLED, wDelEnabled);
	DDX_Check(pDX, IDC_DELETEENABLED, vDelEnabled);
	DDX_Text(pDX, IDC_DAYINTERVAL, vDayInterval);
	DDV_MinMaxUInt(pDX, vDayInterval, 0, 100000);
	DDX_Check(pDX, IDC_AUTONEW, vAutoMark);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CAutoDeleteDlg, CDialog)
	//{{AFX_MSG_MAP(CAutoDeleteDlg)
	ON_NOTIFY(LVN_ITEMCHANGED, IDC_GROUPLIST, OnItemchangedGrouplist)
	ON_BN_CLICKED(IDC_DELETEENABLED, OnDeleteenabled)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CAutoDeleteDlg message handlers

BOOL CAutoDeleteDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	ASSERT(vAcc);
    vGroupList=vAcc->GetGroups();

    CString title;
    title.LoadString(IDS_GROUPNAME);
    wGroupList.InsertColumn(0,title);
    title.LoadString(IDS_GROUPDELETEINTERVAL);
    wGroupList.InsertColumn(1,title,LVCFMT_CENTER);
    title.LoadString(IDS_GROUPDELETEFILTER);
    wGroupList.InsertColumn(2,title);
    title.LoadString(IDS_GROUPAUTOMARK);
    wGroupList.InsertColumn(3,title);
    wGroupList.SetImageList(&theApp.ilist,LVSIL_SMALL);
    
    CNewsGroup *p=vGroupList;
    int idx=0;
    while (p!=0) 
    {
      if (p->sign)
      {
        char buff[100];
        int item=wGroupList.InsertItem(idx++,"",p->autodeletedays?ICON_RECYCLER:ICON_NWGROUP);
        SetItemTextAutoArrange(item,0,p->GetName());
        SetItemTextAutoArrange(item,1,itoa(p->autodeletedays,buff,10));
        SetItemTextAutoArrange(item,2,p->autodeletefilter);
        SetItemTextAutoArrange(item,3,p->automark?"A":0);
        wGroupList.SetItemData(item,(DWORD)p);
      }
      p=p->GetNext();
    }
    CMainFrame::LoadFiltersToCombo(wFilter,vAcc,9999);
    DialogRules();
    return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}


void CAutoDeleteDlg::SetItemTextAutoArrange(int line, int col, const char *text)
{
  int w1=wGroupList.GetStringWidth(text)+20;
  int w2=wGroupList.GetColumnWidth(col);
  if (w1>w2) wGroupList.SetColumnWidth(col,w1);
  wGroupList.SetItemText(line,col,text);
}

void CAutoDeleteDlg::EditItem(int item)
{
  CString helper;
  helper=wGroupList.GetItemText(item,1);
  vDayInterval=atoi(helper);
  helper=wGroupList.GetItemText(item,3);
  vAutoMark=helper=="A";
  helper=wGroupList.GetItemText(item,2);
  vDelEnabled=vDayInterval!=0;
  int p=wFilter.FindStringExact(-1,helper);
  if (p!=-1)
  {
    wFilter.SetCurSel(p);
  }  
  UpdateData(FALSE);
  DialogRules();
}

void CAutoDeleteDlg::OnItemchangedGrouplist(NMHDR* pNMHDR, LRESULT* pResult) 
{
	NM_LISTVIEW* pNMListView = (NM_LISTVIEW*)pNMHDR;
    EditItem(pNMListView->iItem);	
	*pResult = 0;
}

void CAutoDeleteDlg::DialogRules()
{
  bool enabled=(wDelEnabled.GetCheck()!=0);
  wFilter.EnableWindow(enabled);
  wDelInterval.EnableWindow(enabled);  
}

void CAutoDeleteDlg::OnDeleteenabled() 
{
  DialogRules();	
  if (wDelEnabled.GetCheck() && vDayInterval==0)
  {
    SetDlgItemInt(wDelInterval.GetDlgCtrlID(),360);
  }
    
}

void CAutoDeleteDlg::OnOK() 
{
  UpdateData(TRUE);
  POSITION pos=wGroupList.GetFirstSelectedItemPosition();
  CString filter;
  while (pos)
  {
    char buff[100];
    int index=wGroupList.GetNextSelectedItem(pos);
    CNewsGroup *grp=(CNewsGroup *)wGroupList.GetItemData(index);
    grp->autodeletedays=vDelEnabled?vDayInterval:0;
    int p=wFilter.GetCurSel();
    if (p>0) wFilter.GetLBText(p,filter);else filter="";
    grp->autodeletefilter=filter;
    grp->automark=vAutoMark!=FALSE;
    SetItemTextAutoArrange(index,1,itoa(grp->autodeletedays,buff,10));
    SetItemTextAutoArrange(index,2,filter);
    SetItemTextAutoArrange(index,3,grp->automark?"A":0);
    wGroupList.SetItem(index,0,LVIF_IMAGE,0,grp->autodeletedays?ICON_RECYCLER:ICON_NWGROUP,0,0,0);
  }
}
