// LastReadDlg.cpp : implementation file
//

#include "stdafx.h"
#include "newsreader.h"
#include "LastReadDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CLastReadDlg dialog


CLastReadDlg::CLastReadDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CLastReadDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CLastReadDlg)
	date = COleDateTime::GetCurrentTime();
	time = COleDateTime::GetCurrentTime();
	//}}AFX_DATA_INIT
}


void CLastReadDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CLastReadDlg)
	DDX_DateTimeCtrl(pDX, IDC_DATETIMEPICKER2, date);
	DDX_DateTimeCtrl(pDX, IDC_TIMEPICKER, time);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CLastReadDlg, CDialog)
	//{{AFX_MSG_MAP(CLastReadDlg)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CLastReadDlg message handlers
