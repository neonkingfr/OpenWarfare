#include "stdafx.h"
#include "ImageListEx.h"

BOOL CImageListEx::Create(UINT bitmap,int cx, int nGrow, COLORREF crMask) {

    CBitmap bmp;
    bmp.LoadBitmap(bitmap);
    BITMAP binfo;
    bmp.GetBitmap(&binfo);

    if (!CImageList::Create(cx, binfo.bmHeight, ILC_MASK | ILC_COLOR32, 0, 0))
        return FALSE;

    // Load your imagelist bitmap (bmp) here, however
    //   you feel like (e.g. CBitmap::LoadBitmap)   
    // Add the bitmap into the image list
    Add(&bmp, crMask);        
    return TRUE;
}
