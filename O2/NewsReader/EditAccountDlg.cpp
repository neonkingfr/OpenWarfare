// EditAccountDlg.cpp : implementation file
//

#include "stdafx.h"
#include "newsreader.h"
#include "EditAccountDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CEditAccountDlg dialog


CEditAccountDlg::CEditAccountDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CEditAccountDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CEditAccountDlg)
	address = _T("");
	authorize = FALSE;
	myname = _T("");
	name = _T("");
	password = _T("");
	timeout = 4;
	username = _T("");
	email = _T("");
	port = 0;
	time = _T("");
	urlpic = _T("");
	limit = 0;
	//}}AFX_DATA_INIT
}


void CEditAccountDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CEditAccountDlg)
	DDX_Control(pDX, IDC_ADDRESS, wndAddress);
	DDX_Control(pDX, IDC_PASSWORD, wndPassword);
	DDX_Control(pDX, IDC_AUTHORIZE, wndAuthorize);
	DDX_Control(pDX, IDC_USERNAME, wndUsername);
	DDX_Control(pDX, IDC_TIMEOUTTIME, wndTimeoutTime);
	DDX_Control(pDX, IDC_TIMEOUT, wndTimeout);
	DDX_Text(pDX, IDC_ADDRESS, address);
	DDX_Check(pDX, IDC_AUTHORIZE, authorize);
	DDX_Text(pDX, IDC_MYNAME, myname);
	DDX_Text(pDX, IDC_NAME, name);
	DDX_Text(pDX, IDC_PASSWORD, password);
	DDX_Slider(pDX, IDC_TIMEOUT, timeout);
	DDX_Text(pDX, IDC_USERNAME, username);
	DDX_Text(pDX, IDC_MYEMAIL, email);
	DDX_Text(pDX, IDC_PORT, port);
	DDX_Text(pDX, IDC_TIME, time);
	DDX_Text(pDX, IDC_MYPICTURE, urlpic);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CEditAccountDlg, CDialog)
	//{{AFX_MSG_MAP(CEditAccountDlg)
	ON_BN_CLICKED(IDC_AUTHORIZE, OnAuthorize)
	ON_WM_HSCROLL()
	ON_EN_CHANGE(IDC_NAME, OnChangeName)
	ON_EN_CHANGE(IDC_ADDRESS, OnChangeAddress)
	ON_EN_CHANGE(IDC_MYEMAIL, OnChangeMyemail)
	ON_BN_CLICKED(IDC_PREVIEW, OnPreview)
	ON_BN_CLICKED(IDC_PROPERTIES, OnProperties)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CEditAccountDlg message handlers

void CEditAccountDlg::OnAuthorize() 
  {
  BOOL p=wndAuthorize.GetCheck()!=0;
  wndUsername.EnableWindow(p);
  wndPassword.EnableWindow(p);

  }

static CTimeSpan GetTimeFromInt(int i)
  {
  return CTimeSpan(0,0,i/6,(i%6)*10);
  }

static int GetIntFromTime(CTimeSpan &span)
  {
  return span.GetMinutes()*6+span.GetSeconds()/10;
  }

BOOL CEditAccountDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	OnAuthorize();
	wndTimeout.SetRangeMin(0);
	wndTimeout.SetRangeMax(3*6);	
	wndTimeout.SetPos(timeout);
	wndTimeout.PostMessage(WM_LBUTTONUP,0,0);
	ShowTimeout(GetTimeFromInt(wndTimeout.GetPos()));
	CheckAccountValidity();
	namesaved="";
	wndAddress.SetFocus();
//	OnCache();
	return FALSE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}


void CEditAccountDlg::OnHScroll(UINT nSBCode, UINT nPos, CScrollBar* pScrollBar) 
  {
  if (nSBCode==SB_THUMBTRACK && (CWnd *)pScrollBar==(CWnd *)&wndTimeout)
	ShowTimeout(GetTimeFromInt(nPos));
  CDialog::OnHScroll(nSBCode, nPos, pScrollBar);
  }

void CEditAccountDlg::ShowTimeout(CTimeSpan &p)
  {
  CString text=p.Format("%M min %S sec");
  wndTimeoutTime.SetWindowText(text);
  }

void CEditAccountDlg::OnOK() 
  {
  if (prop.m_hWnd) prop.SendMessage(WM_COMMAND,IDOK);
  if (prop.m_hWnd) return;
  CDialog::OnOK();
  }

void CEditAccountDlg::OnChangeName() 
  { 
	// TODO: If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialog::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.
	
	// TODO: Add your control notification handler code here
  CheckAccountValidity();	
  }

void CEditAccountDlg::CheckAccountValidity()
  {
  int i=GetDlgItem(IDC_NAME)->GetWindowTextLength();
  int j=GetDlgItem(IDC_MYEMAIL)->GetWindowTextLength();
  GetDlgItem(IDOK)->EnableWindow(i!=0 && j!=0);
  }

void CEditAccountDlg::OnChangeAddress() 
  {
  CString curr;
  GetDlgItemText(IDC_NAME,curr);
  if (curr==namesaved)
	{
	GetDlgItemText(IDC_ADDRESS,curr);
	SetDlgItemText(IDC_NAME,curr);
	namesaved=curr;
	CheckAccountValidity();
	}
  GetDlgItemText(IDC_ADDRESS,namesaved);
  }

void CEditAccountDlg::OnChangeMyemail() 
{
	// TODO: If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialog::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.
	
	// TODO: Add your control notification handler code here
  CheckAccountValidity();	
 	
}

#include "Web.h"

void CEditAccountDlg::OnPreview() 
  {
  Web w;
  GetDlgItemText(IDC_MYPICTURE,w.url);
  w.DoModal();
  }



void CEditAccountDlg::OnProperties() 
  {
  if (prop.m_hWnd==NULL)
	prop.Create(prop.IDD);
  }
