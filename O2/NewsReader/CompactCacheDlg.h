#if !defined(AFX_COMPACTCACHEDLG_H__1DD1B0AA_3863_413F_9E5C_205D341B2378__INCLUDED_)
#define AFX_COMPACTCACHEDLG_H__1DD1B0AA_3863_413F_9E5C_205D341B2378__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// CompactCacheDlg.h : header file
//

#include "AccountList.h"

/////////////////////////////////////////////////////////////////////////////
// CCompactCacheDlg dialog

class CCompactCacheDlg : public CDialog
{
// Construction
   bool stop;
   CImageListEx lp;
   int animpos;
public:
	virtual void OnOK();
	void RunCompact();
	bool RunEvents();
	void SetMode(int mode);
	void SetProgress(int value, int max);
	CCompactCacheDlg(CWnd* pParent = NULL);   // standard constructor

	DWORD targetsize;
	CNewsAccount *acc;
	HTREEITEM it;
	CAccountList *acclist;
// Dialog Data
	//{{AFX_DATA(CCompactCacheDlg)
	enum { IDD = IDD_CACHECOMPACT };
	CProgressCtrl	progress;
	int		mode;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCompactCacheDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CCompactCacheDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnTimer(UINT nIDEvent);
	virtual void OnCancel();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_COMPACTCACHEDLG_H__1DD1B0AA_3863_413F_9E5C_205D341B2378__INCLUDED_)
