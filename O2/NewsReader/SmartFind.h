// TextFind.h: interface for the CTextFind class.
//
//////////////////////////////////////////////////////////////////////

#ifndef _NR_SMART_FIND_H
#define _NR_SMART_FIND_H

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "iTextFind.h"

#include <Es/essencePch.hpp>

#include <Es/Containers/array.hpp>
#include <Es/Strings/rString.hpp>

//! boolean search expression
class CSmartFind: public ITextFind  
{  
	// list of words we search for
	AutoArray<RString> _words;

	bool _substrings;
  
public:
	CSmartFind();
	bool Init(const char *wordList);

	void SetSubstrings(bool substrings)
	{
		_substrings = substrings;
	}
	float GetScore(const char *intext);
	bool Find(const char *intext);
	bool IsCompiled() {return _words.Size()>0;}
	virtual ~CSmartFind();
};


#endif
