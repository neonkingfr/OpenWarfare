// DlgViewSource.cpp : implementation file
//

#include "stdafx.h"
#include "newsreader.h"
#include "DlgViewSource.h"
#include "LineTerminal2.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDlgViewSource dialog


CDlgViewSource::CDlgViewSource(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgViewSource::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDlgViewSource)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CDlgViewSource::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDlgViewSource)
	DDX_Control(pDX, IDC_SOURCE, wSource);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDlgViewSource, CDialog)
	//{{AFX_MSG_MAP(CDlgViewSource)
	ON_WM_SIZE()
	ON_WM_TIMER()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDlgViewSource message handlers

BOOL CDlgViewSource::OnInitDialog() 
{
	CDialog::OnInitDialog();
	CRect rc;
	GetClientRect(&rc);
	OnSize(0,rc.right,rc.bottom);
	
	SetTimer(1,1,NULL);	
	curier.CreateFont(15,0,0,0,400,0,0,0,EASTEUROPE_CHARSET,0,0,0,0,"Courier");
	wSource.SetFont(&curier,TRUE);
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CDlgViewSource::OnSize(UINT nType, int cx, int cy) 
  {  
  if (wSource.GetSafeHwnd()) wSource.MoveWindow(0,0,cx,cy,TRUE);
  }

void CDlgViewSource::OnTimer(UINT nIDEvent) 
  {
  const char *line=lterm->ReadLine();
  int cursz=wSource.GetWindowTextLength();
  if (strcmp(line,".")==0) 
	{
	KillTimer(nIDEvent);
	return;
	}
  msg=msg+line+"\r\n";
  DWORD s=wSource.GetSel();
  DWORD p=wSource.GetFirstVisibleLine();
  wSource.SetWindowText(msg);
  wSource.SetSel(s);
  wSource.LineScroll(p);
  }


