// Machine generated IDispatch wrapper class(es) created with ClassWizard
/////////////////////////////////////////////////////////////////////////////
// CHTMLElement wrapper class

class CHTMLElement : public COleDispatchDriver
{
public:
	CHTMLElement() {}		// Calls COleDispatchDriver default constructor
	CHTMLElement(LPDISPATCH pDispatch) : COleDispatchDriver(pDispatch) {}
	CHTMLElement(const CHTMLElement& dispatchSrc) : COleDispatchDriver(dispatchSrc) {}

// Attributes
public:

// Operations
public:
	void setAttribute(LPCTSTR strAttributeName, const VARIANT& AttributeValue, long lFlags);
	VARIANT getAttribute(LPCTSTR strAttributeName, long lFlags);
	BOOL removeAttribute(LPCTSTR strAttributeName, long lFlags);
	void SetClassName(LPCTSTR lpszNewValue);
	CString GetClassName();
	void SetId(LPCTSTR lpszNewValue);
	CString GetId();
	CString GetTagName();
	LPDISPATCH GetParentElement();
	LPDISPATCH GetStyle();
	void SetOnhelp(const VARIANT& newValue);
	VARIANT GetOnhelp();
	void SetOnclick(const VARIANT& newValue);
	VARIANT GetOnclick();
	void SetOndblclick(const VARIANT& newValue);
	VARIANT GetOndblclick();
	void SetOnkeydown(const VARIANT& newValue);
	VARIANT GetOnkeydown();
	void SetOnkeyup(const VARIANT& newValue);
	VARIANT GetOnkeyup();
	void SetOnkeypress(const VARIANT& newValue);
	VARIANT GetOnkeypress();
	void SetOnmouseout(const VARIANT& newValue);
	VARIANT GetOnmouseout();
	void SetOnmouseover(const VARIANT& newValue);
	VARIANT GetOnmouseover();
	void SetOnmousemove(const VARIANT& newValue);
	VARIANT GetOnmousemove();
	void SetOnmousedown(const VARIANT& newValue);
	VARIANT GetOnmousedown();
	void SetOnmouseup(const VARIANT& newValue);
	VARIANT GetOnmouseup();
	LPDISPATCH GetDocument();
	void SetTitle(LPCTSTR lpszNewValue);
	CString GetTitle();
	void SetLanguage(LPCTSTR lpszNewValue);
	CString GetLanguage();
	void SetOnselectstart(const VARIANT& newValue);
	VARIANT GetOnselectstart();
	void scrollIntoView(const VARIANT& varargStart);
	BOOL contains(LPDISPATCH pChild);
	long GetSourceIndex();
	VARIANT GetRecordNumber();
	void SetLang(LPCTSTR lpszNewValue);
	CString GetLang();
	long GetOffsetLeft();
	long GetOffsetTop();
	long GetOffsetWidth();
	long GetOffsetHeight();
	LPDISPATCH GetOffsetParent();
	void SetInnerHTML(LPCTSTR lpszNewValue);
	CString GetInnerHTML();
	void SetInnerText(LPCTSTR lpszNewValue);
	CString GetInnerText();
	void SetOuterHTML(LPCTSTR lpszNewValue);
	CString GetOuterHTML();
	void SetOuterText(LPCTSTR lpszNewValue);
	CString GetOuterText();
	void insertAdjacentHTML(LPCTSTR where, LPCTSTR html);
	void insertAdjacentText(LPCTSTR where, LPCTSTR text);
	LPDISPATCH GetParentTextEdit();
	BOOL GetIsTextEdit();
	void click();
	LPDISPATCH GetFilters();
	void SetOndragstart(const VARIANT& newValue);
	VARIANT GetOndragstart();
	CString toString();
	void SetOnbeforeupdate(const VARIANT& newValue);
	VARIANT GetOnbeforeupdate();
	void SetOnafterupdate(const VARIANT& newValue);
	VARIANT GetOnafterupdate();
	void SetOnerrorupdate(const VARIANT& newValue);
	VARIANT GetOnerrorupdate();
	void SetOnrowexit(const VARIANT& newValue);
	VARIANT GetOnrowexit();
	void SetOnrowenter(const VARIANT& newValue);
	VARIANT GetOnrowenter();
	void SetOndatasetchanged(const VARIANT& newValue);
	VARIANT GetOndatasetchanged();
	void SetOndataavailable(const VARIANT& newValue);
	VARIANT GetOndataavailable();
	void SetOndatasetcomplete(const VARIANT& newValue);
	VARIANT GetOndatasetcomplete();
	void SetOnfilterchange(const VARIANT& newValue);
	VARIANT GetOnfilterchange();
	LPDISPATCH GetChildren();
	LPDISPATCH GetAll();
};
