// ArchiveStreamFileMapping.h: interface for the ArchiveStreamFileMapping class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_ARCHIVESTREAMFILEMAPPING_H__2020184B_028B_4048_8EBC_29ADEEF7424D__INCLUDED_)
#define AFX_ARCHIVESTREAMFILEMAPPING_H__2020184B_028B_4048_8EBC_29ADEEF7424D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "ArchiveStream.h"

#define ASFM_UNABLETOCREATEMAPPING -1000
#define ASFM_UNABLETOMAPVIEWOFFILE -1001
#define ASFM_EOFREACHED -1002
#define ASFM_UNABLETOSETENDOFFILE -1003

class ArchiveStreamFileMapping : public ArchiveStream  
{
protected:
  HANDLE _mapobject;  //object for file mapping
  HANDLE _file;       //file handle
  __int64 _filepos;   //current position in file (begin of view)
  __int64 _filesize;  //current file size
  DWORD _granuality;  //map granuality (size of view)
  void *_curview;     //pointer to current view, or NULL, if not defined;
  DWORD _mempos;      //current position in view
  bool _autoclose;    //automaticly closes file when destructs
  DWORD _protect;     //copy of protect filed for filemapping
  int _err;           //error code
public:
	ArchiveStreamFileMapping(HANDLE filehandle,DWORD protect, DWORD granuality, bool autoclose, const char *objectname=NULL);
	ArchiveStreamFileMapping(DWORD size, DWORD protect,const char *objectname=NULL);
	virtual ~ArchiveStreamFileMapping();
    virtual int IsError() {return _err;}
    virtual void Reset() {_err=0;}
    virtual void SetError(int err) {_err=err;}
    virtual __int64 Tell();
    virtual void Seek(__int64 lOff, SeekOp seekop);    

protected:
	void Unmap();
	void Map(DWORD dwDesiredAccess);
	void CloseMapping();
  	void RemapFile(const char *_name=NULL);
    void Flush() {Unmap();}


};

class ArchiveStreamFileMappingIn: public ArchiveStreamFileMapping
{
public:
    ArchiveStreamFileMappingIn(HANDLE handle, bool autoclose, DWORD granuality=65536, const char *objectname=NULL):
      ArchiveStreamFileMapping(handle,PAGE_READONLY,granuality,autoclose,objectname) {}
    ArchiveStreamFileMappingIn(DWORD size,const char *objectname):
      ArchiveStreamFileMapping(size,PAGE_READONLY,objectname) {}
    virtual bool IsStoring() {return false;}
    virtual int DataExchange(void *buffer, int maxsize);
    virtual void Reserved(int bytes);

};

class ArchiveStreamFileMappingOut : public ArchiveStreamFileMapping
{
  DWORD _grow;
public:
    ArchiveStreamFileMappingOut(HANDLE handle, bool autoclose, DWORD grow=64*1024, DWORD granuality=16384, const char *objectname=NULL):
      ArchiveStreamFileMapping(handle,PAGE_READWRITE,granuality,autoclose,objectname),_grow(grow) {}
    ArchiveStreamFileMappingOut(DWORD size,  const char *objectname=NULL):
      ArchiveStreamFileMapping(size,PAGE_READWRITE,objectname) {}
    ~ArchiveStreamFileMappingOut ();
    virtual bool IsStoring() {return true;}
    virtual int DataExchange(void *buffer, int maxsize);

};


#endif // !defined(AFX_ARCHIVESTREAMFILEMAPPING_H__2020184B_028B_4048_8EBC_29ADEEF7424D__INCLUDED_)

