// ProgessDlg.cpp : implementation file
//

#include "stdafx.h"
#include "newsreader.h"
#include "ProgessDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CProgessDlg dialog


CProgessDlg::CProgessDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CProgessDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CProgessDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
max=cur=0;
}


void CProgessDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CProgessDlg)
	DDX_Control(pDX, IDC_PROGRESS1, bar);
	DDX_Control(pDX, IDC_PERCENT, value);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CProgessDlg, CDialog)
	//{{AFX_MSG_MAP(CProgessDlg)
	ON_WM_TIMER()
	ON_WM_CLOSE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CProgessDlg message handlers

void CProgessDlg::Create()
  {
  CDialog::Create(IDD);
  PlaceWindow();
  }

void CProgessDlg::PlaceWindow()
  {
  CRect rc1,rc2;
  GetWindowRect(rc1);
  GetParent()->GetWindowRect(rc2);
  rc2.right-=GetSystemMetrics(SM_CXVSCROLL)+4*GetSystemMetrics(SM_CXEDGE );
  rc2.bottom-=4*GetSystemMetrics(SM_CYEDGE );
  CPoint pt(rc2.right-rc1.right,rc2.bottom-rc1.bottom);
  if (pt.x==0 && pt.y==0) return;
  rc1+=pt;
  MoveWindow(&rc1);
  }

BOOL CProgessDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	SetTimer(5,5000,NULL);	
	passive=true;
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CProgessDlg::OnTimer(UINT nIDEvent) 
  {
  if (max==0) return;
  if (passive)
	{
	UINT p=GetTickCount();
	if (p-cursettime<cursettime-maxsettime && cur && GetForegroundWindow()==GetOwner())
	  {
	  ShowWindow(SW_SHOW);
	  passive=false;
	  KillTimer(5);
	  SetTimer(5,500,NULL);
	  cntr=5;
	  }
	}
  if (!passive)
	{
	UINT p=GetTickCount();
	if (p-cursettime<500) cntr=5;else cntr--;
	if (cntr==0) 
	  {
	  ShowWindow(SW_HIDE);
	  passive=true;
	  KillTimer(5);
	  SetTimer(5,5000,NULL);
	  cur=0;
	  }
	bar.SetRange32(0,max);
	bar.SetPos(cur);
	UINT pr;
	if (max>10000) pr=cur/(max/100);
	else pr=(cur*100)/max;
	char buff[25];
	sprintf(buff,"%d%%",pr);
	value.SetWindowText(buff);
	PlaceWindow();
	}
  CDialog::OnTimer(nIDEvent);
  }

void CProgessDlg::OnClose() 
  {
	
  CDialog::OnClose();
  }

void CProgessDlg::OnCancel()
  {
  ShowWindow(SW_HIDE);
  }
