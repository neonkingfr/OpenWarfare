// HttpServer.cpp: implementation of the CHttpServer class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "newsreader.h"
#include "HttpServer.h"
#include "NewsTerminal.h"
#include "NewsArticle.h"
#include "NewsAccount.h"
#include "ScanGroupsThread.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////



void PrepareMessage(CNewsAccount *curacc, CNewsGroup *curgrp, CNewsArtHolder *current, strstream &str, CWebBrowser2 *bwrs)
  {  
//  CHTMLDocument2 webdoc;
  str.rdbuf()->freeze(0);
  str.seekp(0);

  CString frm;
  CString eml;
  bool multipart;

  str.rdbuf()->setbuf(NULL,16384);  
  ASSERT(current!=NULL);
  str<<"<html><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=windows-1250\"></title></head>";
  str<<"<body ";
  if (theApp.config.dispprop.en_bgcolor) OutColorProp(str,"bgcolor",theApp.config.dispprop.bgcol);
  if (theApp.config.dispprop.en_texcolor) OutColorProp(str,"text",theApp.config.dispprop.textcol);
  if (theApp.config.dispprop.en_linkcol) 
	{
	OutColorProp(str,"link",theApp.config.dispprop.linkcol);
	OutColorProp(str,"alink",~theApp.config.dispprop.linkcol);
	OutColorProp(str,"vlink",theApp.config.dispprop.linkcol);
	}
  str<<" topmargin=\"0\" leftmargin=\"0\">";
  theApp.config.dispprop.CreateFontBeginSeq(frm);
  multipart=(strncmp(current->content,"multipart",9)==0);
  str<<frm;
  	{
	GetNameFrom(current->from,frm);
	GetEmailFromSender(current->from,eml);
	char col[10];
	DWORD dw;
	if (theApp.config.dispprop.en_bgcolor)
	  dw=ShadeColor(theApp.config.dispprop.bgcol,90);
	else
	  dw=ShadeColor(GetSysColor(COLOR_WINDOW),90);
/*	webdoc=GetHtmlDocument();
	else
	  {
	  VARIANT col=webdoc.GetBgColor();
	  swscanf(col.puiVal+1,(unsigned short *)"%\0X\0\0\0",&dw);	
	  dw=ShadeColor(htonl(dw)>>8,90);
	  }*/
	sprintf(col,"%06X",htonl(dw)>>8);
	str<<"<table bgcolor=\"#"<<col;
	str<<"\" width=100% border=0><tr><td>From: <B>";
	if (theApp.config.useoe)
	  str<<"<a href=\"mailto:"<<eml<<"\">";
	else
	  str<<"<a href=\"nrmail://"<<eml<<"\">";
	str<< frm <<"</a></B><BR>";
	str<<"Subject: <B>" << current->GetSubject() <<"</B><br>";
	str<<"Posted: " << (current->date+theApp.timezone).Format("%A, %d. %B %Y %H:%M:%S");
	frm=current->icon;
	if (multipart)
	  str<<"<td><a href=\""COMM_MSGHTML"\">&gt;HTML&lt;</a></td>";
	if (frm!="") str<<"<td><img src=\""<<frm<<"\" align=right>";
	str<<"</table>";
	}
  if (!current->IsTrash() && current->GetArticle()!=NULL)
	{
	glob_lock.Lock();
	int partid=-1;
	CMessagePart *part=current->GetArticle();
	while (part)
	  {
	  partid++;
	  if (multipart)
		{
		str<<"<iframe src=\"file://"<<part->CreateAsFile(current->content)<<"\" width=\"100%\" height=\"80%\"></iframe>";
		}
	  else
		{
		if (part->GetType()==CMessagePart::text && part->GetSize()<50000)
		{
		str<<"<table border=0 cellspacing=10><tr><td>";
	    theApp.config.dispprop.CreateFontBeginSeq(frm);str<<frm;
		const char *dd=part->GetData();
		if (dd==NULL) dd="Internal error!";
		istrstream in((char *)dd);
		if (curacc->msgidlist.IsEmpty()) curacc->BuildHash();
		Text2Html(in,str,&(curacc->msgidlist));
		theApp.config.dispprop.CreateFontEndSeq(frm);
		str<<frm;
		str<<"</td></tr></table>";
		}
	  else
		{
		char *ext=(char *)part->GetFilename();
		if (ext!=NULL) ext=strrchr(ext,'.');
		if (ext!=NULL) 
		  {
		  ext++;
  		  if (theApp.config.shwhtml && (stricmp(ext,"HTM")==0 || stricmp(ext,"HTML")==0 || stricmp(ext,"SHTML")==0 || stricmp(ext,"MHT")==0))
			str<<"<p align=center><IFRAME SRC=\""<<theApp.server.BuildUrl(theApp.server.RegisterObject(curacc,curgrp,current,partid))<<"\" width=\"90%\" height=\"90%\"></IFRAME></p>";
		  if (stricmp(ext,"JPG") && stricmp(ext,"JPEG") && stricmp(ext,"GIF") && stricmp(ext,"BMP") &&
			stricmp(ext,"PNG")) ext=NULL;
		  if (!theApp.config.shwimgs) ext=NULL;
		  }
		if (ext!=NULL)		  
		    str<<"<p align=center><IMG SRC=\""<<theApp.server.BuildUrl(theApp.server.RegisterObject(curacc,curgrp,current,partid))<<"\"></p>";
		else
		  str<<"<p align=center><a href=\""<<theApp.server.BuildUrl(theApp.server.RegisterObject(curacc,curgrp,current,partid))<<"\" target=\"_blank\">"<<(part->GetFilename()==NULL?"Article text":part->GetFilename())<<"</a></p>";
		}
		}
	  part=part->GetNext();
      if (part) str<<"<hr>";
	  }
	 glob_lock.Unlock();
	}
  frm="";
  theApp.config.dispprop.CreateFontEndSeq(frm);
  str<<frm<<"</body></html>";
  str.put((char)0);
  /*
  SetAddressBar(TRUE);
  IHTMLDocument2 *document = (IHTMLDocument2 *)GetHtmlDocument();
  CComBSTR url("http://localhost/nr.html");
  VARIANT null;
  memset(&null,0,sizeof(null));
  document->open(url,null,null,null,NULL);
  document->clear();
  CComBSTR bstr(str.str());
  // Creates a new one-dimensional array
  VARIANT *param;
  SAFEARRAY *sfArray = SafeArrayCreateVector(VT_VARIANT, 0, 1);
  HRESULT hresult = SafeArrayAccessData(sfArray,(LPVOID*) & param);
  param->vt = VT_BSTR;
  param->bstrVal = bstr;
  hresult = SafeArrayUnaccessData(sfArray);
  hresult = document->write(sfArray);  
  document->close();
  /*
  CHTMLElement body=webdoc.GetBody();
  if (theApp.config.dispprop.en_bgcolor) webdoc.SetBgColor(theApp.config.dispprop.bgcol);
  if (theApp.config.dispprop.en_texcolor) webdoc.SetFgColor(theApp.config.dispprop.textcol);
  if (theApp.config.dispprop.en_linkcol) 
	{
	webdoc.SetLinkColor(theApp.config.dispprop.linkcol);
	webdoc.SetAlinkColor(~theApp.config.dispprop.linkcol);
	webdoc.SetVlinkColor(theApp.config.dispprop.linkcol);
	}  
  body.SetInnerHTML(str.str());
  */
  str.rdbuf()->freeze(0);
  }

/////////////////////////////////////////////////////////////////////////////
// CLocalHttpServer

CLocalHttpServer::CLocalHttpServer()
  {
  index=0;
  FlushObjects();
  }

CLocalHttpServer::~CLocalHttpServer()
{
}


// Do not edit the following lines, which are needed by ClassWizard.
#if 0
BEGIN_MESSAGE_MAP(CLocalHttpServer, CAsyncSocket)
	//{{AFX_MSG_MAP(CLocalHttpServer)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()
#endif	// 0

/////////////////////////////////////////////////////////////////////////////
// CLocalHttpServer member functions

BOOL CLocalHttpServer::Init()
  {
  if (!Create(2000,SOCK_STREAM,FD_ACCEPT,"127.0.0.1")) return FALSE;
  Listen(10);
  return TRUE;
  }

void CLocalHttpServer::OnAccept(int nErrorCode) 
  {
  CLineTerminal lterm;
  SOCKADDR_IN sin;
  int len=sizeof(sin);
  lterm.Init();
  Accept(lterm,(SOCKADDR *)&sin,&len);
  lterm.AsyncSelect(FD_READ|FD_CLOSE);
  CAsyncSocket::OnAccept(nErrorCode);
  if (len!=sizeof(SOCKADDR_IN) || sin.sin_addr.S_un.S_addr!=0x0100007f) return;
  strstream str;
  str.rdbuf()->setbuf(NULL,16384);
  CString line;
  TRACE0("-------- HTTP SERVER STARTS ------------\n");
  lterm.ReceiveLine(line);
  int pos=line.Find(' ');
  if (pos!=-1)
	{
	line.Delete(0,pos+1);
	pos=line.Find(' ');
	if (pos!=-1) 
	  {
	  char *z=line.LockBuffer();
	  z[pos]=0;
	  line.UnlockBuffer();
	  }
	}
  else
	{
	SendError(lterm);
	lterm.Close();
	return ;
	}
  char *ll=strrchr(line,'/');
  if (ll==NULL)
	{
	SendError(lterm);
	lterm.Close();
	return;
	}
  ll++;
  int objid;
  if (sscanf(ll,"%d",&objid)==0) 
	{
	SendError(lterm);
	lterm.Close();
	return;
	}
  while (line!="")
	{
	str<<line<<"\r\n";
    if (lterm.ReceiveLine(line)<0) return;
	}
  SendObject(lterm,objid);
/*  lterm.SendLine("HTTP/1.1 200 OK");
  lterm.SendLine("Server: NewsReader local http server");
  lterm.SendLine("Content-Type: text/html");
  lterm.SendLine("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
  lterm.SendLine("Cache-Control: no-cache, must-revalidate");
  lterm.SendLine("Pragma: no-cache");
  lterm.SendLine("");
  lterm.SendLine(browser->GetCurrentMessage());*/
  lterm.Close();
  }

void CLocalHttpServer::FlushObjects()
  {
  memset(objects,0,sizeof(objects));
  }

int CLocalHttpServer::RegisterObject(CNewsAccount *acc, CNewsGroup *grp, CNewsArtHolder *hldr, int messagePart)
  {
  SObjectList obj;
  obj.acc=acc;
  obj.grp=grp;
  obj.hld=hldr;
  obj.msgpart=messagePart;
  objects[index]=obj;
  int ret=index;
  index++;
  if (index>=LHS_MAXOBJECTS) index=0;
  return ret;
  }

void CLocalHttpServer::SendObject(CLineTerminal &term, int index)
  {
  if (index<0 || index>LHS_MAXOBJECTS-1) {SendError(term);return;}
  CNewsArtHolder *hld=objects[index].hld;
  const char *fname;
  char *dot;
  char buff[256];

  strcpy(buff,"Content-Type: ");  
  if (hld==NULL) {SendError(term);return;}
  int part=objects[index].msgpart;
  term.SendLine("HTTP/1.1 200 OK");
  term.SendLine("Server: NewsReader local http server");    
  if (part)
	{
	char *c;
	int q=part;
	CMessagePart *p=hld->GetArticle();
	while (q-- && p) p=p->GetNext();
	if (p==NULL) {SendError(term);return;}  
	if (p->GetData()==NULL) {SendError(term);return;}  

	fname=p->GetFilename();
    dot=strrchr(fname,'.');
	if (dot)
	  {
	  c=strchr(buff,0);
	  if (GetContentType(dot,c,sizeof(buff)-(c-buff))==false)
		{
		strcpy(c,"application/octet-stream");
		}
	  }
	else
	  strcat(buff,"text/plain");
	if (p->GetFilename())
	  {
	  term.SendFormatted("Content-Disposition: inline; filename=\"%s\"",p->GetFilename());
	  }
	}
  else
    strcat(buff,"text/html");  
  term.SendLine(buff);
  term.SendLine("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
  term.SendLine("Cache-Control: no-cache, must-revalidate");
  term.SendLine("Pragma: no-cache");  
  term.SendLine("");  
  if (objects[index].msgpart==0)
	{
	strstream str;
	PrepareMessage(objects[index].acc,objects[index].grp,objects[index].hld,str,browser);
	term.SendLine(str.str());
	str.rdbuf()->freeze(false);
	
	}
  else
	{
	CMessagePart *p=hld->GetArticle();
	int q=part;
	while (q-- && p) p=p->GetNext();
	if (p)
	  {
	  char *pos=p->GetData();
	  int size=p->GetSize();
	  while (size)
		{
		int rd=term.Send(pos,size);
		size-=rd;
		pos+=rd;
		}
	  }
	}
  }

void CLocalHttpServer::SendError(CLineTerminal &lterm)
  {
  lterm.SendLine("HTTP/1.1 404 Not Found");
  lterm.SendLine("Server: NewsReader local http server");  
  lterm.SendLine("Content-Type: text/html");
  lterm.SendLine("");
  lterm.SendLine("Article index has expired");
  }

bool CLocalHttpServer::GetContentType(const char *ext, char *buff, int buffsize)
  {
  HKEY key;
  if (RegOpenKeyEx(HKEY_CLASSES_ROOT,ext,0,KEY_QUERY_VALUE,&key)==ERROR_SUCCESS)
	{
	BYTE *data=NULL;
	DWORD size=0;
	RegQueryValueEx(key,"Content Type",NULL,NULL,data,&size);
	data=(BYTE *)alloca(size+1);
	if (RegQueryValueEx(key,"Content Type",NULL,NULL,data,&size)==ERROR_SUCCESS)
	  {
	  strncpy(buff,(char *)data,buffsize-1);
	  buff[buffsize-1]=0;
	  RegCloseKey(key);
	  return true;
	  }
	}
  RegCloseKey(key);
  return false;
  }

const char * CLocalHttpServer::BuildUrl(int objectid)
  {
  static char buff[256];
  sprintf(buff,"http://localhost:2000/%d",objectid);
  return buff;
  }
