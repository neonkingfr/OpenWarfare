// ArticleDBStrings.cpp: implementation of the ArticleDBStrings class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ArticleDBStrings.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

ArticleDBStrings::ArticleDBStrings()
{

}

void ArticleDBStrings::Init()
  {
  bool newmem;
  if (GetUsed()<8)    
    {
    FormatMemory();
    AllocMem(sizeof(unsigned long));    
    newmem=true;
    }
  else
    newmem=false;
  freeindex=NULL;
  freeindex=EnumBlocks(freeindex);
  *freeindex=0;
  }

DMMHANDLE ArticleDBStrings::BeginNewDBString(DMMHANDLE hndl)
  {
  DMMHANDLE nw=AllocIndex();
  SingleStringInfo *info=GetItem(nw);
  info->nexthandle=0;
  info->curhandle=hndl;
  return nw;
  }

DMMHANDLE ArticleDBStrings::AllocIndex()
  {
  if (GetFreeIndex()==0)  
    {
    DMMHANDLE hndl=EnumBlocks(0);
    unsigned long size=GetBlockSize(hndl);
    DeallocMem(hndl);
    hndl=AllocMem(size+sizeof(SingleStringInfo));
    hndl+=size;
    return hndl;
    }
  else
    {
    DMMHANDLE p=*freeindex;
    *freeindex=GetNextIndex(p);            
    return p;
    }
  }

SingleStringInfo *ArticleDBStrings::GetItem(DMMHANDLE index)
  {
  SingleStringInfo *nfo;
  Map(index,nfo);
  return nfo;
  }

DMMHANDLE ArticleDBStrings::AppendNewItem(DMMHANDLE string, DMMHANDLE hndl, bool append_begin)
  {
  if (string==0) return BeginNewDBString(hndl);
  if (append_begin)
    {
    DMMHANDLE item=AllocIndex();
    SingleStringInfo *nfo=GetItem(item);
    nfo->nexthandle=string;
    nfo->curhandle=hndl;
    return item;
    }
  else
    {
    DMMHANDLE item=AllocIndex();
    SingleStringInfo *nfo=GetItem(string);
    while (nfo->nexthandle) nfo=GetItem(nfo->nexthandle);
    nfo->nexthandle=item;
    nfo=GetItem(item);
    nfo->curhandle=hndl;
    nfo->nexthandle=0;
    return item;
    }
  }

void ArticleDBStrings::FreeIndex(DMMHANDLE index)
  {
  if (index==0) return;
  SingleStringInfo *nfo=GetItem(index);
  this->GetFreeIndex();
  nfo->nexthandle=*freeindex;
  *freeindex=index;
  }

void ArticleDBStrings::FreeString(DMMHANDLE hndl)
  {
  if (hndl==0) return;
  while (hndl)
    {
    DMMHANDLE next=GetItem(hndl)->nexthandle;
    FreeIndex(hndl);
    hndl=next;
    }
  }

bool ArticleDBStrings::FreeItemInString(DMMHANDLE hndl,DMMHANDLE &string)
  {
  if (string==hndl)
    {
    string=GetItem(hndl)->nexthandle;
    FreeIndex(hndl);
    return true;
    }
  SingleStringInfo *nfo=GetItem(string);
  while (nfo->nexthandle && nfo->nexthandle!=hndl) nfo=GetItem(nfo->nexthandle);
  if (nfo->nexthandle!=hndl) return false;
  nfo->nexthandle=GetItem(hndl)->nexthandle;
  FreeIndex(hndl);
  return true;
  }

bool ArticleDBStrings::IsInString(DMMHANDLE hndl, DMMHANDLE string)
  {
  if (string==0) return false;
  if (hndl==0) return false;
  if (hndl==string) return true;
  SingleStringInfo *nfo=GetItem(string);
  while (nfo->nexthandle && nfo->nexthandle!=hndl) nfo=GetItem(nfo->nexthandle);
  return nfo->nexthandle==hndl;
  }

DMMHANDLE ArticleDBStrings::EnumString(DMMHANDLE &stringenm)
  {
  if (stringenm==0) return 0;
  SingleStringInfo *nfo=GetItem(stringenm);
  stringenm=nfo->nexthandle;
  return nfo->curhandle;  
  }

int ArticleDBStrings::GetStringLength(DMMHANDLE string)
  {
  int sz=0;
  for (;EnumString(string);sz++);
  return sz;
  }

DMMHANDLE ArticleDBStrings::InsertNewItem(DMMHANDLE insafter, DMMHANDLE hndl)
  {
  if (insafter==0) return BeginNewDBString(hndl);
  DMMHANDLE item=AllocIndex();
  SingleStringInfo *nfo=GetItem(insafter);
  SingleStringInfo *insert=GetItem(item);
  insert->curhandle=hndl;
  insert->nexthandle=nfo->nexthandle;
  nfo->nexthandle=item;
  return item;
  }