// IArticleRecordSet.h: interface for the IArticleRecordSet class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_IARTICLERECORDSET_H__8722BA7C_692F_4153_96FB_676FCDFA7E9C__INCLUDED_)
#define AFX_IARTICLERECORDSET_H__8722BA7C_692F_4153_96FB_676FCDFA7E9C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class ArticleHeader;

class IArticleRecordset  
{
public:
    virtual bool FindNext(ArticleHeader &hdr)=0;
};

#endif // !defined(AFX_IARTICLERECORDSET_H__8722BA7C_692F_4153_96FB_676FCDFA7E9C__INCLUDED_)
