// ArticleDBStrings.h: interface for the ArticleDBStrings class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_ARTICLEDBSTRINGS_H__8A38E207_2C7E_4178_BABF_49D8D166D4FF__INCLUDED_)
#define AFX_ARTICLEDBSTRINGS_H__8A38E207_2C7E_4178_BABF_49D8D166D4FF__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "ArticleTable.h"
#include "DiskMappedMemory.h"



struct SingleStringInfo
  {
  DMMHANDLE curhandle;  ///<user handle for identify data
  DMMHANDLE nexthandle; ///<handle to next index
  };


class ArticleDBStrings : public CDiskMappedMemory
{
DMMHANDLE *freeindex;

public:
	int GetStringLength(DMMHANDLE string);
	ArticleDBStrings();
    
    void Init();

	DMMHANDLE BeginNewDBString(DMMHANDLE hndl);    
    DMMHANDLE AppendNewItem(DMMHANDLE string, DMMHANDLE hndl, bool append_begin=true);
    DMMHANDLE InsertNewItem(DMMHANDLE insafter, DMMHANDLE hndl);
    DMMHANDLE GetNextIndex(DMMHANDLE index) {return GetItem(index)->nexthandle;}
    DMMHANDLE GetItemValue(DMMHANDLE index) {return GetItem(index)->curhandle;}
    SingleStringInfo *GetItem(DMMHANDLE index);

    void FreeString(DMMHANDLE hndl);
    bool FreeItemInString(DMMHANDLE hndl,DMMHANDLE &string);
    bool IsInString(DMMHANDLE hndl, DMMHANDLE string);
    DMMHANDLE EnumString(DMMHANDLE &stringenm);    
    void FreeIndex(DMMHANDLE index);
    
private:    
    DMMHANDLE GetFreeIndex() 
      {
      freeindex=EnumBlocks((DMMHANDLE *)NULL);
      return *freeindex;
      }
    DMMHANDLE AllocIndex();
};

#endif // !defined(AFX_ARTICLEDBSTRINGS_H__8A38E207_2C7E_4178_BABF_49D8D166D4FF__INCLUDED_)
