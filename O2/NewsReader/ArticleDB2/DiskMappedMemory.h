// DiskMappedMemory.h: interface for the CDiskMappedMemory class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_DISKMAPPEDMEMORY_H__440C29B9_7EAF_428D_8D00_CABE97852F46__INCLUDED_)
#define AFX_DISKMAPPEDMEMORY_H__440C29B9_7EAF_428D_8D00_CABE97852F46__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <windows.h>

class CDiskMappedMemory;
typedef void (*DiskMappedMemory_AllocError)(CDiskMappedMemory *obj, unsigned long size);

typedef unsigned long DMMHANDLE;

class CDiskMappedMemory  
{
	HANDLE _mapObject;	  //handle to mapped object
	void *_base;	  //base adress of mapped area
	unsigned long _alloc;	  //size allocated for mapped area
	unsigned long *_used;	  //pointer to unsigned long contain number of bytes used.
	unsigned long _grow;	  //number of bytes allocated while file is grow	
	HANDLE _fileHandle;	
	bool _autoclose;
	DiskMappedMemory_AllocError _allocError;
	bool OptimizeFile(bool remap);
	bool UnMapFile();
	bool MapFile(unsigned long alloc);

public:
	void FormatMemory();
	CDiskMappedMemory(int grow=65536);
	virtual ~CDiskMappedMemory();

	bool Create(HANDLE handle,bool autoclose);
	bool Create(const char *fname);
	bool Close();


	template <class T>
	  void Map(DMMHANDLE handle, T *&out) const {out=(T *)((char *)_base+handle);}
	template <class T>
	  DMMHANDLE Map(const T *ptr) const {return (char *)ptr-(char *)_base;}

	DMMHANDLE AllocMem(unsigned long size);
	void DeallocMem(DMMHANDLE handle);
	void Optimize();
	DMMHANDLE EnumBlocks(DMMHANDLE enumoffs);
	bool GetBlockUsed(DMMHANDLE handle);
	unsigned long GetBlockSize(DMMHANDLE handle);
	unsigned long CalcFragmentation();
	unsigned long GetUsed() {return *_used;}
	bool OptimizeFile() {OptimizeFile(true);}

	template <class T>
	  T *EnumBlocks(T *ptr)
	  {
	  unsigned long ret=EnumBlocks(ptr==NULL?0:Map(ptr));
      if (ret==0) ptr=0;else Map(ret,ptr);
	  return ptr;
	  }
  

	DiskMappedMemory_AllocError SetHandler(DiskMappedMemory_AllocError handler)
	  {
	  DiskMappedMemory_AllocError old=_allocError;
	  _allocError=handler;
	  return old;
	  }
  };

#endif // !defined(AFX_DISKMAPPEDMEMORY_H__440C29B9_7EAF_428D_8D00_CABE97852F46__INCLUDED_)
