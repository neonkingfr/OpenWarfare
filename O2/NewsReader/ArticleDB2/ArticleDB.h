// ArticleDB.h: interface for the ArticleDB class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_ARTICLEDB_H__521664EB_0412_4957_98C1_6009A746E68A__INCLUDED_)
#define AFX_ARTICLEDB_H__521664EB_0412_4957_98C1_6009A746E68A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "ArticleHeader.h"
#include "ArticleDBStrings.h"
#include "ArticleTable.h"
#include "BTreeMemory.h"
#include "BTree.h"
#include "ArticleIndex.h"
#include "ArticleDBIndexes.h"
#include <es/types/pointers.hpp>
#include <es/containers/array.hpp>



class ArticleDBView;

typedef ASimpleIndexRecord<char,CmpSubstring> AStringIndex;
typedef ASimpleIndexRecord<ArticleDateTime,CmpDateTime> ADateIndex;
typedef ASimpleIndexRecord<DMMHANDLE,CmpDMMHandle,true> ADMMHandleIndex;


class ArticleDB  
{
    AutoArray<SRef<ArticleDBView> > _views;
    ArticleDBStrings _strings;
    ArticleTable _table;

	BTree<AStringIndex> _fieldMessageID;
	BTree<AStringIndex> _fieldReferences;
	BTree<AStringIndex> _fieldGroups;
	
public:
	bool AddView(ArticleDBView *view);
	void RecreateDatabase();
	void IndexViews(ArticleDBView *view=NULL);
	void ReindexDatabase();
	bool OpenDatabase(const char *name);
	ArticleDB();
	virtual ~ArticleDB();

    ArticleTable &GetTable() {return _table;}
    ArticleDBStrings &GetStrings() {return _strings;}
    const BTree<AStringIndex> &GetReferenceIndex() {return _fieldReferences;}
    const BTree<AStringIndex> &GetMessageIDIndex() {return _fieldMessageID;}
    const BTree<AStringIndex> &GetGroupsIndex() {return _fieldGroups;}

protected:
	void IndexBTree();
	void IndexSubstrings(BTree<AStringIndex> &indexTree, const char *text, DMMHANDLE article);
	void IndexNewsgroups(BTree<AStringIndex> &indexTree, const char *text, DMMHANDLE article);
	void IndexBTreeArticle(const ArticleHeader &hdr);
	bool GetParentArticle(ArticleHeader &ahdr,ArticleHeader *parent);


};

#endif // !defined(AFX_ARTICLEDB_H__521664EB_0412_4957_98C1_6009A746E68A__INCLUDED_)
