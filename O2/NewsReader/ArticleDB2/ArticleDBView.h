// ArticleDBView.h: interface for the ArticleDBView class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_ARTICLEDBVIEW_H__A869264C_3EB9_4073_A148_CF59D28FE4BE__INCLUDED_)
#define AFX_ARTICLEDBVIEW_H__A869264C_3EB9_4073_A148_CF59D28FE4BE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


#include "ArticleHeader.h"
#include "IArticleRecordset.h"
#include "ArticleIndex.h"
#include "ArticleDB.h"


struct ArticleDBViewCond
  {
  enum Item 
    {
    ItDynItem, //any dynamic item
    ItFlag, //one of flags
    ItDate, //date posted
    ItLines, //number of lines
    ItGroup, //set of groups, where AHS_GROUPLIST cannot be used
    };
  enum ItemFlag 
    {
    ItFAttachm, //item has attachment (or possible has attachment)
    ItFHighlighted, //item is highlighted
    ItFUnread, //item is unread
    };
  enum ItemOp 
    {
    Equal,  //equal, for ItDynItem, ItDate, ItLines, negate means no equal
    Less, //Less,  for ItDynItem, ItDate, ItLines, negate means GreatOrEqual
    Great, //Great,  for ItDynItem, ItDate, ItLines, negate means LessOrEqual
    Contain, //finds string in item, for ItDynItem, negate means NotContain
              //with ItDate, test only day, not time, returns articles in specific day
              //with ItGroup, test presence of group
    IsSet,  //for ItFlag tests if flag is set, negate means NotSet
    TPMatch //Test, if Thread Properties Match, negate means TPNotMatch
    };

  Item itemType;
  union 
    {
    int AHSIndex; //for ItDynItem
    ItemFlag flag; // for ItFlags
    int valInt; //for ItLines test value
    ArticleDateTime valDate; //for ItDate
    };
  ItemOp op:6;
  bool negate:1;    //if one, result is negated
  bool final:1;     //if true, this is last condition in sub-expression &&
  char szData[1]; //Varible Text Data, for ItDynItem


protected:
  ArticleDBViewCond(const ArticleDBViewCond& other) {}
  bool EvaluateDynItem(const ArticleHeader &hdr) const;
  bool EvaluateFlag(const ArticleHeader &hdr) const;
  bool EvaluateDate(const ArticleHeader &hdr) const;
  bool EvaluateInt(const ArticleHeader &hdr) const;
  bool EvaluateGroup(const ArticleHeader &hdr) const; 
public:
  void *operator new(size_t sz,const char *szText);
  void *operator new(size_t sz);
  void operator delete(void *sz,const char *szText);
  void operator delete(void *sz);
  ArticleDBViewCond() {}

  bool Evaluate(const ArticleHeader &hdr) const;
  };


class ArticleDBViewRecordset: public IArticleRecordset
  {
  DMMHANDLE _next;
  ArticleDB & _currDB;
  int _remainItems;
  public:
    ArticleDBViewRecordset(ArticleDB &currDB,DMMHANDLE ifrst,int remain);
    virtual bool FindNext(ArticleHeader &hdr);
  };

class ArticleSubthreadRecordset: public ArticleRecordset<AStringIndex>
  {
  ArticleDB & _currDB;
  const ArticleDBView &_currView;  
  public:
    ArticleSubthreadRecordset(ArticleDB & currDB,const ArticleDBView &currView,const ArticleHeader &childof);
    virtual bool FindNext(ArticleHeader &hdr);
  };

class ArticleWholeThreadRecordset: public IArticleRecordset
  {
  ArticleDB & _currDB;
  const ArticleDBView &_currView;
  DMMHANDLE _current;
  void RecursiveBuild(const ArticleHeader &childOf, DMMHANDLE &last);
  void CreateSubThreadList(const ArticleHeader &childOf, DMMHANDLE addafter);
  public:
    ArticleWholeThreadRecordset(ArticleDB & currDB,const ArticleDBView &currView,const ArticleHeader &childof):
      _currView(currView),_currDB(currDB),_current(0xFFFFFFFF)
      {
/*      DMMHANDLE last;
      RecursiveBuild(indexof,last);*/
      }
      ~ArticleWholeThreadRecordset() 
        {
        _currDB.GetStrings().FreeString(_current);
        }
    virtual bool FindNext(ArticleHeader &hdr);
  };


#define DBVIEW_MAXCONDS 16
#define DBVIEW_MAXNAME 50


class ArticleDBView  
{
friend ArticleSubthreadRecordset;
	ArticleDB & _currDB;
    DMMHANDLE _stringBeg;

    ArticleDBViewCond *_conds[DBVIEW_MAXCONDS];
    int _condsUsed;

    char _name[DBVIEW_MAXNAME];

public:
    enum SortItem {sortMark,sortAttach,sortSubject,sortFrom,sortDate};

    SortItem _sortItem;
    bool _sortDirty;
    bool _reverseSort;    

    bool _onlyThreads; //if true, only threads are appended (default)
    int _maxItems;    //maximum items loaded into view once - positive - top items, negative - tail items
    bool _testSubthreads; //tests subthreads, if passed with conditions. If not, they aren't enumerated
    bool _testParents; //tests parents, if failed with conditions. If they, child articles are used as first in thread

    mutable int _totalInView; //total articles in view, calculates from strings; if -1, must be recalculated



public:    
	bool SetName(const char *name);
    const char *GetName() const {return _name;}
	void RemoveAll(bool reindex=false); ///<if reindex=true, content of table strings already deleted, no cleaning performed

	ArticleDBView(ArticleDB & currDB);

    bool AddCondition(ArticleDBViewCond *cond);
    void DeleteAllConditions();

    bool TestConditions(const ArticleHeader &hdr) const;

    void SetSorting(SortItem item, bool reverse=false)
      {_sortItem=item;_reverseSort=reverse;_sortDirty=true;}
    SortItem GetSort(bool *reversed=NULL)
      {if (reversed) *reversed=_reverseSort;return _sortItem;}
    
    void UpdateSorting();

    bool Get_onlyThreads() const {return _onlyThreads;}
    bool Get_testSubthreads() const {return _testSubthreads;}
    bool Get_testParents() const {return _testParents;}
    int Get_maxItems() const  {return _maxItems;}
    
    void Set_onlyThreads(bool p) {_onlyThreads=p;}
    void Set_testSubthreads(bool p) {_testSubthreads=p;}
    void Set_testParents(bool p) {_testParents=p;}
    void Set_maxItems(int i)   {_maxItems=i;}
    

    void AppendNewArticle(const ArticleHeader &hdr); //pridava novy article, pokud projde testem
	bool RetestArticle(const ArticleHeader &hdr); //vrac� false = article nepro�el testem a byl vymaz�n z view, true = testem pro�el a z�stal

    ArticleDBViewRecordset EnumArticlesInView(bool all=false) const;
    ArticleWholeThreadRecordset EnumArticlesInThread(const ArticleHeader &childof) const  
      {  return ArticleWholeThreadRecordset(_currDB,*this,childof);}
    ArticleSubthreadRecordset EnumChildArticles(const ArticleHeader &childof) const  
      {  return ArticleSubthreadRecordset(_currDB,*this,childof);}

	virtual ~ArticleDBView();

};

#endif // !defined(AFX_ARTICLEDBVIEW_H__A869264C_3EB9_4073_A148_CF59D28FE4BE__INCLUDED_)
