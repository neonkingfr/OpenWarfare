// IArchive.h: interface for the IArchive class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_IARCHIVE_H__975F0F32_79AB_416B_A50C_0AF2EDCC4749__INCLUDED_)
#define AFX_IARCHIVE_H__975F0F32_79AB_416B_A50C_0AF2EDCC4749__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

enum ArchiveRParamType {ArchiveRParam};

#include <malloc.h>
#include <stdio.h>

#ifndef ASSERT
#ifdef _DEBUG
#include <assert.h>
#define ASSERT(x) assert(x)
#else
#define ASSERT(x)
#endif
#endif


#define ARCHIVEDECLAREDATAEXCHANGE(type) \
	protected: virtual int OverrideDoExchange(type &data)=0; \
	public: void DoExchange(type &data) {if (!_error) _error=OverrideDoExchange(data);} 

#define ARCHIVESECTION_DEFAULT 0
#define ARCHIVESECTION_VARIABLE 1
#define ARCHIVESECTION_BLOCKSECTION 2
#define ARCHIVESECTION_CLASS 3
#define ARCHIVESECTION_FOLDER 4

class IArchive;

template <class T>
void ArchiveVariableExchange(IArchive &arch,T &data);



template <class T>
bool IArchive_CompareItems(const T &item1,const T &item2) 
  {return item1==item2;}


class ArchiveSection;
class SC_ClassRegistratorBase;
class SerializedClassInstanceWatcher;
class IArchive  
{
friend ArchiveSection;
protected:
  bool _IsStoring;    ///<Archive is storing data
  bool _IsCounting;   ///<Archive is currently simulates writting to counting space usage  
  int _version;       ///<Current archive version
  int _error;
  int _orderIndex;    ///index of next element in order. Archive can use this appends index to element name
  
public:
    IArchive(bool store=false, bool count=false):_version(0)  
	  {InitInterface(false);_error=0;classSerializationInfo=NULL;instanceWatcher=NULL;}
	virtual ~IArchive()=0	///<pure virtual destructor
	  {
	  }

	
    ///Make data exchange with unnamed variable
    template<class T>
	  void operator() (T &data)
	  {
	    ArchiveVariableExchange(*this,data);
	  }

    template<class T>
	  T RValue(T data)
	  {
	  ArchiveVariableExchange(*this,data);
      return data;
	  }

    template<class T>
	  T *RValuePtr(const T *data)
	  {   
      T *x=IsStoring()?const_cast<T *>(data):NULL;
	  ArchiveVariableExchange(*this,x);
      return x;
	  }

		
    ///Make data exchange with signle named variable
	template <class T>
    bool operator() (const char *name, T &data)
	  {
		if (OpenSection(name,ARCHIVESECTION_VARIABLE)==false) return false;
		ArchiveVariableExchange(*this,data);
		CloseSection(name,ARCHIVESECTION_VARIABLE);
		return true;
	  }	
    
    ///Make data exchange with signle named variable and support default values
    template <class T>
    bool operator() (const char *name, T &data, const T &defvalue)
	  {
		if (OpenSection(name,ARCHIVESECTION_VARIABLE)==false) 
		  {
		  data=defvalue;
		  return false;
		  }
        if (!IsStoring() || !IArchive_CompareItems(data,defvalue) || !HandleDefaultValue()) 
		  ArchiveVariableExchange(*this,data);
		CloseSection(name,ARCHIVESECTION_VARIABLE);
		return true;
	  }	
    ///Make data exchange with signle named variable or direct value    
	template <class T>
    T RValue (const char *name, T data)
	  {
		if (OpenSection(name,ARCHIVESECTION_VARIABLE)==false) return false;
		ArchiveVariableExchange(*this,data);
		CloseSection(name,ARCHIVESECTION_VARIABLE);
		return data;
	  }	

	template <class T>
    T *RValuePtr (const char *name, const T *data)
	  {
        T *x=IsStoring()?const_cast<T *>(data):NULL;
		if (OpenSection(name,ARCHIVESECTION_VARIABLE)==false) return false;
		ArchiveVariableExchange(*this,x);
		CloseSection(name,ARCHIVESECTION_VARIABLE);
		return x;
	  }	

    ///Make data exchange with single named variable or direct value and support default values
	template <class T>
    T RValue(const char *name, T data, const T &defvalue)
	  {
		if (OpenSection(name,ARCHIVESECTION_VARIABLE)==false) return defvalue;
		if (!IsStoring() || !IArchive_CompareItems(data,defvalue) || !HandleDefaultValue()) 
   		  ArchiveVariableExchange(*this,data);
		CloseSection(name,ARCHIVESECTION_VARIABLE);
		return data;
	  }	

	template <class T>
    T *RValuePtr(const char *name, const T *data, const T &defvalue)
	  {
        T *x=IsStoring()?const_cast<T *>(data):NULL;
		if (OpenSection(name,ARCHIVESECTION_VARIABLE)==false) return defvalue;		  
		if (!IsStoring() || !IArchive_CompareItems(data,defvalue) || !HandleDefaultValue()) 
   		  ArchiveVariableExchange(*this,x);
		CloseSection(name,ARCHIVESECTION_VARIABLE);
		return x;
	  }	


    ///Function returns true, if archive is storing data into stream
	bool IsReading() {return !_IsStoring;}
    ///Function returns true, if archive is reading data from stream
	bool IsStoring() {return _IsStoring;}
    ///Function returns true, if archive is only counting space for data
    /** Counting is simulated storing operation, so if IsCounting returns true,
    IsStoring should also return true.*/
	bool IsCounting() {return _IsCounting;}
    ///Operator returns true, if archive is storing data into stream
    bool operator+() {return IsStoring();}
    ///Operator returns true, if archive is reading data from stream
    bool operator-() {return IsReading();}
    ///Operator returns true, if any error has occured
    /**If an error occured, DataExchange is stoped, no data is transfered until 
    ResetError() is called */
    bool operator!() {return _error!=0;}
    ///Operator error code, if any error has occured, or 0.
    /**If an error occured, streaming is stoped, no data is transfered until 
    ResetError() is called */
    int GetError() {return _error;}
    ///Resets error state, streaming is resume
    void Reset() {OverrideSetError(0);_error=0;}
    void SetError(int err) {OverrideSetError(err);_error=err;}

	ARCHIVEDECLAREDATAEXCHANGE(bool);
	ARCHIVEDECLAREDATAEXCHANGE(char);
	ARCHIVEDECLAREDATAEXCHANGE(unsigned char);
	ARCHIVEDECLAREDATAEXCHANGE(short);
	ARCHIVEDECLAREDATAEXCHANGE(unsigned short);
	ARCHIVEDECLAREDATAEXCHANGE(int);
	ARCHIVEDECLAREDATAEXCHANGE(unsigned int);
	ARCHIVEDECLAREDATAEXCHANGE(long);
	ARCHIVEDECLAREDATAEXCHANGE(unsigned long);
	ARCHIVEDECLAREDATAEXCHANGE(__int64);
	ARCHIVEDECLAREDATAEXCHANGE(unsigned __int64);
	ARCHIVEDECLAREDATAEXCHANGE(float);
	ARCHIVEDECLAREDATAEXCHANGE(double);
	ARCHIVEDECLAREDATAEXCHANGE(unsigned char *);
	ARCHIVEDECLAREDATAEXCHANGE(char *);
	ARCHIVEDECLAREDATAEXCHANGE(unsigned short *);
	ARCHIVEDECLAREDATAEXCHANGE(short *);
       
	
	virtual void Reserved(int bytes)=0;

    virtual int OverrideDoBinaryExchange(void *ptr, unsigned long size)=0;
    int Binary(void *ptr, unsigned long size) {if (!_error) _error=OverrideDoBinaryExchange(ptr,size);return _error;} 
    bool Binary(const char *name, void *ptr, unsigned long size) 
	  {
		if (OpenSection(name,ARCHIVESECTION_VARIABLE)==false) return false;
		Binary(ptr,size);
		CloseSection(name,ARCHIVESECTION_VARIABLE);
		return true;
	  }	
	


    ///Function reads/writes array.
    /** Function reads/writes array with specifiets items
    @param array pointer at begin of array
    @param numItems number items to read/write
    @return number of items has been fully reads/writes
    */
	template <class T>
	int Array(T *array, int numItems)
      {
      int i;
        for (i=0;i<numItems && !(*this);i++)                
          (*this)(array[i]);                
      return i;
      }

    template <class T>
	int Array(T *array, int numItems, char *nameMask)
      {
      int i;
      if (nameMask)
        {
        char *buff=(char *)alloca(strlen(nameMask)+16);
        for (i=0;i<numItems && !(!(*this));i++)                
          {
          sprintf(buff,nameMask,i);  
          if ((*this)(buff,array[i])==false) return i;
          }
        }
      return i;
      }
    ///Function reads/writes array terminated by specified item
    /** 
    @param array pointer at begin of array
    @param terminator an item, that will stop exchange
    @param numItems maximum number items to read/write
    @return number of items has been fully reads/writes (including terminator)<p>
    Function will exchange data with archive until numItems will reached, or the
    terminating item reached. The terminating item is also exchanged. 
    If numItems is set to -1, function will skip data until termination item reached,
    and it will also skipped.
    */
	template <class T>
	int Array(T *array, const T &terminator, int numItems, char *nameMask)
      {      
      int i=0;
      char *buff=(char *)alloca(strlen(nameMask)+16);
      if (numItems<0)        
        for (i=0;i<numItems && !(!(*this));i++)
          {
          sprintf(buff,nameMask,i);  
          if ((*this)(buff,array[i])==false) return i;
          if (IsError()) return i;
          if (IArchive_CompareItems(array[i],terminator)) return i+1;
          }
      else
        {
        T dump;
        sprintf(buff,nameMask,i);  
        if ((*this)(buff,dump)==false) return i;
        while (!(!(*this)))
          {          
          if (CompareTraits::Compare(array[i],terminator)) return i+1;
          i++;
          sprintf(buff,nameMask,i);  
          if ((*this)(buff,dump)==false) return i;
          }
        }
      return i;
      }

    template <class T>
	int Array(T *array, const T &terminator, int numItems)
      {
      int i;
      if (numItems<0)        
        for (i=0;i<numItems && !(!(*this));i++)
          {
          (*this)(array[i]);
          if (IsError()) return i;
          if (IArchive_CompareItems(array[i],terminator)) return i+1;
          }
      else
        {
        T dump;
        (*this)(dump);
        while (!(!(*this)))
          {          
          if (CompareTraits::Compare(array[i],terminator)) return i+1;
          i++;
          (*this)(dump);
          }
        }
      return i;
      }
      

    template <class T>
   	T *GetInstanceData(int dataId) {return (T *)OverideGetInstanceData(int dataId);}

    
    ///Test current archive version and return true, if version is higher
    bool IsVer(int ver) {return _version>=ver;}
    
    ///Requests version for archive part
    bool RequestVer(int ver) 
      {
      _version=ver;
      if (OverrideExchangeVersion(_version)==false) return false;
      return _version<=ver;
      }

    ///Checks archive signature
    virtual bool Check(const char *text)=0;

    void SetIndex(int index) {_orderIndex=index;}

    unsigned long vUserData;  ///<Archive specific data for whole archive
    void *ptrUserData;        ///<Archive specific pointer for whole archive
    unsigned long vSectionData; ///<Section specific data.
    void *ptrSectionData;     ///<Section specific pointer.    
	SC_ClassRegistratorBase *classSerializationInfo; ///<Information for ClassSerialization
	SerializedClassInstanceWatcher *instanceWatcher; ///<Information for InstanceWaitcher;

    

  ///Overrides
protected:

  ///Overriding class should take response for new error state
  virtual void OverrideSetError(int error)=0;
  
  ///Overriding class should return current error state
  virtual int OverrideGetError()=0;  

  ///Inicializes interface
  /**Overriding class should call this function, when archive is inicialized or
  everytime mode has been changed.
  @param write Set true, if archive will write data from archive
  @param count Set true, if archive will simulate data writting<p>
  NOTE: count is set only when write is set to true. Otherwise is ignored
  */
  void InitInterface(bool write, bool count=false)
	{
	_IsStoring=write;
	_IsCounting=count && write;
	} 
  
  ///Overriding class should open section with name and type.
  /**
  @param name name of section to open
  @param type one of ARCHIVESECTION_XXXXX type constant, or any user defined type
  @return If archive storing, function must return true, even if an error occured. Any error
    must be reported by internal error state. If archive is reading, then function can
    return true, if section exist in archive, or false, is not. 
  */
  virtual bool  OpenSection(const char *name,int type=0)=0;
  
  ///Overriding class should close section with name and type
  /**
  @param name name of section to close
  @param type type of section to close
  <p>
  NOTE: Each OpenSection should have matching CloseSection. 
  */
  virtual void CloseSection(const char *name,int type=0)=0;

  ///Overriding class can handle default value, if archive supported. 
  /**Function is called, when default value check has been requested and detected. 
  @return function return true, if situation has been handled, and value can be skipped,
    or false, if default value is not supported. Function can use information about variable
    from last OpenSection calling. <p>
  This function is called only when archive is storing
  */
  virtual bool HandleDefaultValue() {return false;}

  ///Overriding class should determine section state, and return it
  /**
  @return Function returns true, if section can be closed, all data has been read or written. Function
    should returns false, if data is remaining in section or none of data has been written. 
    Trio OpenSection/CloseSection/CanCloseSection is usualy binded in cycle. First archive enters
    the section, and cycles. At the end of each cycle, it calls CanCloseSection(), if function returns
    true, function leaves section and call CloseSection(). 

    If archive reading, this feature can be used when readed section has another order, then it is readed,
    so each next cycle, remaining data is readed until section end. If archive is writting, overriding
    class can add extra cycles to collect all informations about data that are subject to write. Then 
    final cycle finish writting.

    This trio is used in function ArchiveSection::Block;
    */


  virtual bool CanCloseSection() {return true;}

  
  ///Overriding class must define, how version mark is presented in archive.
  /** Makes standard exchange for version mark*/
  virtual bool OverrideExchangeVersion(int &ver)=0;



  ///Overriding class can use this function to present own data adressed by type
  virtual void *OverideGetInstanceData(int dataId)=0;

};

template <class T>
void ArchiveVariableExchange(IArchive &arch,T &data)
  {
  //If a compile error occured in this section, 
  //you trying serialize own type.
  //Declare ArchiveVariableExchange function for own type with own implementation
  arch.DoExchange(data); 
  }


#undef ARCHIVEDECLAREDATAEXCHANGE

class ArchiveSection
  {
  unsigned long _vSectionData; ///<Section specific data.
  void *_ptrSectionData;     ///<Section specific pointer.    
  const char *_opened;        ///<if current section is opened, it contains name of section
  IArchive &_arch;
  public:
    ArchiveSection(IArchive &arch):_arch(arch) {_opened=NULL;}
    ~ArchiveSection() {ASSERT(_opened==NULL);}

   ///Defined block - complete section
   /** usage:<pre>
     IArchive &anArchive=...
     ArchiveSection archsect(anArchive);
     while (archsect.Block("SomeBlock"))
       {
       ... block defined ...
       }
       </pre>
   @param name name of following block
   @return function returns true, if block should be processed, or false, if not. result should
   be used as condition in "while" structure.
   */
   bool Block(const char *name)
      {
      if (_opened==NULL) //curretn section descriptor is closed, so we can open new section
        {
        if (!_arch.OpenSection(name)) return false;
        _opened=name;
        return true;
        }
      else if (_opened==name) //current section is opened, and name is equal to specified, we will check section
        {
        if (_arch.CanCloseSection()==false) return true; //we should continue in section, return true
        _arch.CloseSection(name);
        _opened=NULL;
        return false;  //section has been closed, false will leave cycle
        }
      else 
        {
        int invalid_reuse_of_section=0; 
        ASSERT(invalid_reuse_of_section);//we trying reuse object, but section another section is open
        return false;
        }
      }
    ///Drops current section, if leaved unexceptly. 
    /**
    @param true, if section has been closed, false if no section is opened.
    NOTE: Drop function must be called in block, where pointer "name" used in function Block is still
    valid. */
    bool Drop()
      {
      if (_opened==NULL) return false;
      _arch.CloseSection(_opened);
      _opened=NULL;
      return true;
      }
  };


#endif // !defined(AFX_IARCHIVE_H__975F0F32_79AB_416B_A50C_0AF2EDCC4749__INCLUDED_)
