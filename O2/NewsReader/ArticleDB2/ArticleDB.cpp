// ArticleDB.cpp: implementation of the ArticleDB class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ArticleDB.h"
#include "../CharMapCz.h"
#include "ArticleDBView.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

#define ADB_TABLEEXT ".tbl"
#define ADB_STRINGSEXT ".tbs"

THREAD_DECL(ArticleTable) *currentDB;

CCharMapCz CzechCharMap;

ArticleDB::ArticleDB():
  _fieldMessageID(50),
	_fieldReferences(50),
	_fieldGroups(50)
{

}

ArticleDB::~ArticleDB()
{

}

bool ArticleDB::OpenDatabase(const char *name)
  {  
  int lname=strlen(name);
  char *c=(char *)alloca(lname+10);
  char *ext=c+lname;
  strcpy(c,name);
  strcpy(ext,ADB_TABLEEXT);
  if (_table.Create(c)==false) return false;
  int res=_table.CheckTable();
  if (res==-1) return false;

  strcpy(ext,ADB_STRINGSEXT);
  if (_strings.Create(c)==false) return false;
  if (res==1) 
	ReindexDatabase();
  return true;
  }

void ArticleDB::ReindexDatabase()
{
_strings.FormatMemory();
_strings.Init();
_fieldMessageID.RemoveAll();
_fieldReferences.RemoveAll();
_fieldGroups.RemoveAll();
for (int i=0;i<_views.Size();i++) _views[i]->RemoveAll(true);
IndexBTree();
IndexViews();
}

void ArticleDB::IndexBTree()
  {
  currentDB=&_table;  
  for(ArticleHeader hdr;_table.EnumArticles(hdr);)	
	{
	IndexBTreeArticle(hdr);	
	ADBProgressBar(hdr.GetHandle(),_table.GetUsed());
	}
  }

void ArticleDB::IndexSubstrings(BTree<AStringIndex> &indexTree, const char *text, DMMHANDLE article)
  {
  const char *trv=text;
  while (*trv && isspace(*trv)) trv++;
  do
	{
	AStringIndex stridx(article,_table.Map(trv));
	indexTree.Add(stridx);
	while (*trv && isalnum(*trv)) trv++;
    while (*trv && !isalnum(*trv)) trv++;
	}
  while (*trv);
  }

void ArticleDB::IndexNewsgroups(BTree<AStringIndex> &indexTree, const char *text, DMMHANDLE article)
  {
  const char *trv=text;
  while (trv)
	{
	AStringIndex stridx(article,_table.Map(trv));
	indexTree.Add(stridx);
	trv=strrchr(trv,',');
	if (trv) 
	  trv++;
	}
  }

void ArticleDB::IndexBTreeArticle(const ArticleHeader &hdr)
  {
  AStringIndex FieldMessageID(hdr.GetHandle(),_table.Map(hdr.ahdm[AHS_MSGID]));
  AStringIndex FieldReferences(hdr.GetHandle(),_table.Map(hdr.ahdm[AHS_REFERENCE]));

  _fieldMessageID.Add(FieldMessageID);  
  IndexNewsgroups(_fieldGroups,hdr.ahdm[AHS_GROUPLIST],hdr.GetHandle());
  ArticleRecordset<AStringIndex> refrcs(_fieldMessageID,&_table);
  hdr.ahst->threadID=hdr.GetHandle();
  ArticleHeader thread=hdr;
  while (thread.ahdm[AHS_REFERENCE][0])
	if (GetParentArticle(thread,&thread)==false) break;	
  if (hdr.GetHandle()==thread.GetHandle() && hdr.ahdm[AHS_REFERENCE][0]) 
	FieldReferences=AStringIndex(hdr.GetHandle(),_table.Map(strchr(hdr.ahdm[AHS_REFERENCE],0)));
 _fieldReferences.Add(FieldReferences);  

  hdr.ahst->threadID=thread.ahst->threadID;

  if (thread.ahst->dateNewest<hdr.ahst->datePost)
	{	
	thread.ahst->dateNewest=hdr.ahst->datePost;
	} 
  }

void ArticleDB::IndexViews(ArticleDBView *view)
  {
  for (ArticleHeader hdr;_table.EnumArticles(hdr);)
    {
    ADBProgressBar(hdr.GetHandle(),_table.GetUsed());
    if (view) 
      view->AppendNewArticle(hdr);
    else
      for (int i=0;i<_views.Size();i++)
        _views[i]->AppendNewArticle(hdr);
    }
  }
bool ArticleDB::GetParentArticle(ArticleHeader &ahdr,ArticleHeader *parent)
  {
  AStringIndex stridx(ahdr.ahdm[AHS_REFERENCE],true),*found;
  ArticleRecordset<AStringIndex> rcrdset(_fieldMessageID,&_table,stridx);
  found=rcrdset.FindNext();
  if (found==NULL) return false;
  if (parent) found->GetResult(*parent);
  return true;
  }

void ArticleDB::RecreateDatabase()
  {
  _table.FormatMemory();
  _strings.FormatMemory();
  _table.CheckTable();
  _strings.Init();
  ReindexDatabase();
  }

bool ArticleDB::AddView(ArticleDBView *view)
  {
  int i;
  ASSERT(view!=NULL);
  if (view==NULL) return false;

  const char *name=view->GetName();
  ASSERT(name[0]);
  if (name[0]==0) return false;

  for (i=0;i<_views.Size();i++)
    if (stricmp(_views[i]->GetName(),name)==0) return false;

  _views.Append()=view;
  return true;
  }


