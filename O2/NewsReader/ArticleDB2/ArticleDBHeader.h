#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>
#include <iostream.h>
#include <strstrea.h>
#include <windows.h>
#include <assert.h>

void ADBProgressBar(int cur, int max);


#ifndef ASSERT
#ifdef _DEBUG
#define ASSERT(x) assert(x)
#else
#define ASSERT(x)
#endif
#endif



#define THREAD_DECL(type) __declspec( thread ) type