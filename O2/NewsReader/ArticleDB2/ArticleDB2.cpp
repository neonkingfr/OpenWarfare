// ArticleDB2.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <windows.h>
#include "ArticleTable.h"
#include "ArticleTableReader.h"
#include <EL\TCPIPBasics\IPA.h>
#include <EL\TCPIPBasics\WSA.h>
#include "..\Socket.h"
#include "../LineTerminal2.h"
#include "../ArchiveStreamTCP.h"
#include <conio.h>
#include "..\CharMapCz.h"
#include "BTreeIOArch.h"
#include "ArticleDB.h"
#include "ArticleDBView.h"


void ADBProgressBar(int cur, int max)
  {
  static DWORD lasttm=GetTickCount();
  DWORD curtm=GetTickCount();
  if (curtm-lasttm>500)
	{
	printf("In progress: %8.2f\r",cur*100.0f/(float)max);
	lasttm=curtm;
	}
  }

int main(int argc, char* argv[])
  {
  ArticleDB database;
  
  database.OpenDatabase("pokus");

  database.RecreateDatabase();

  CWSA wsa(MAKEWORD(1,1));
  Socket s(SOCK_STREAM);
  CIPA ip("e2.bistudio.com",119);
  s.SetNonBlocking(true);  
  s.Connect(ip);
  if (s.Wait(5000,Socket::WaitWrite)!=Socket::WaitWrite) 
	abort();
  ArchiveStreamTCPIn tcpin(s,30000,false);
  ArchiveStreamTCPOut tcpout(s,30000,false);
  LineTerminal lterm(tcpin,tcpout);
  lterm.ReadLine();
  lterm.SendLine("AUTHINFO USER bredy");
  lterm.ReadLine();
  lterm.SendLine("AUTHINFO PASS 1-MXQst\\51");
  lterm.ReadLine();

  ArticleTableReader reader(database.GetTable());
  reader.ReadOverviewHeader(lterm);
  reader.DownloadGroup("bistudio.general",lterm,1);
  reader.DownloadGroup("bistudio.tools2",lterm,1);

  ArticleDBView *view=new ArticleDBView(database);
  ArticleDBViewCond *cond=new("Bredy") ArticleDBViewCond();
  cond->itemType=ArticleDBViewCond::ItDynItem;
  cond->op=ArticleDBViewCond::Contain;
  cond->AHSIndex=AHS_FROM;    

  view->AddCondition(cond); 
  view->SetName("Find Bredy");
  database.AddView(view);
  database.ReindexDatabase();
  
  ArticleDBViewRecordset fnd=view->EnumArticlesInView();
  ArticleHeader hdr;
  while (fnd.FindNext(hdr))
    {
    printf("From: %s Subject: %s\n",hdr.ahdm[AHS_FROM],hdr.ahdm[AHS_SUBJECT]);    
    ArticleWholeThreadRecordset thread=view->EnumArticlesInThread(hdr);
    while (thread.FindNext(hdr))      
      printf("*** From: %s Subject: %s\n",hdr.ahdm[AHS_FROM],hdr.ahdm[AHS_SUBJECT]);          
    }

  return 0;
  }
