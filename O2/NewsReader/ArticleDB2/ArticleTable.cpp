// ArticleTable.cpp: implementation of the ArticleTable class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ArticleTable.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
#define TABLEMAGICVALUE ('A' | ('R'<<8) | ('T'<<16) | ('B'<<24))
#define TABLEDESCRIPTION "Article Table - NewsReader (C)2004 Bohemia Interactive Studio"


ArticleTable::ArticleTable()
{

}

ArticleTable::~ArticleTable()
{

}

DMMHANDLE ArticleTable::CreateArticle(ArticleHeader &ahdr)
  {
  unsigned long size=ahdr.GetSize();
  _critLock.CriticalLock();
  DMMHANDLE newmem=AllocMem(size);
  void *ptr;
  Map(newmem,ptr);
  ArticleHeader newhdr;
  newhdr.Clone(ahdr,ptr,newmem);
  _critLock.CriticalUnlock();
  return newmem;
  }

void ArticleTable::GetArticle(DMMHANDLE handle, ArticleHeader &hdr)
  {
  if (handle==0) 
	{
	hdr=ArticleHeader();
	return;
	}
  void *ptr;  
  Map(handle,ptr);
  hdr.AttachArticle(ptr,handle);
  hdr.SetLock(&_critLock);
  }

bool ArticleTable::DeleteArticle(ArticleHeader &article)
  {
  DMMHANDLE hndl=article.GetHandle();
  if (hndl==0) return false;
  DeallocMem(hndl);
  return true;
  }

bool ArticleTable::EnumArticles(ArticleHeader &article)
  {
  DMMHANDLE hndl=article.GetHandle();  
  if (hndl==0) hndl=EnumBlocks(hndl);
  hndl=EnumBlocks(hndl);
  GetArticle(hndl,article);
  return hndl!=0;
  }

char ArticleTable::CheckTable()
  {
  ArticleDBFileHeader *hdr=GetTableHeader();
  if (hdr==NULL)
	{
	CreateTable();
	return 1;
	}
  if (hdr->DBVersion==ADB_CURRENTVERSION) return 0;
  if (hdr->DBVersion>ADB_CURRENTVERSION) return -1;
  DMMHANDLE hndl;
  DMMHANDLE used=GetUsed();
  for (hndl=EnumBlocks(NULL);hndl=EnumBlocks(hndl) && hndl<used;)
	{
	ArticleHeader ahdr;
	void *ptr;
	Map(hndl,ptr);
	ahdr.AttachArticle(ptr,hndl,hdr->DBVersion);
	CreateArticle(ahdr);
	DeallocMem(hndl);
	}
  Optimize();
  return 1;
  }

void ArticleTable::CreateTable()
  {
  DMMHANDLE hndl=AllocMem(sizeof(ArticleDBFileHeader)+strlen(TABLEDESCRIPTION));
  DMMHANDLE pos=GetUsed();
  ArticleDBFileHeader *fhdr;
  Map(hndl,fhdr);
  fhdr->DBMagicMark=TABLEMAGICVALUE;
  fhdr->DBFirstFree=0;
  fhdr->DBStart=pos;
  fhdr->DBVersion=ADB_CURRENTVERSION;
  memset(fhdr->Reserver,0,sizeof(fhdr->Reserver));
  strcpy(fhdr->description,TABLEDESCRIPTION);
  }

ArticleDBFileHeader * ArticleTable::GetTableHeader()
  {
  if (GetUsed()<sizeof(ArticleDBFileHeader)) return NULL;
  DMMHANDLE hndl=EnumBlocks(0);
  ArticleDBFileHeader *hdr;
  Map(hndl,hdr);
  return hdr;
  }
