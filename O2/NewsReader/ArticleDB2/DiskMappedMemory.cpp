// DiskMappedMemory.cpp: implementation of the CDiskMappedMemory class.
//
//////////////////////////////////////////////////////////////////////

#include <assert.h>
#include "DiskMappedMemory.h"

#ifdef _DEBUG
#define ASSERT(x) assert(x)
#else
#define ASSERT(x)
#endif


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

#define MAGICVALUE ('D' | ('M'<<8) | ('M'<<16) | ('B'<<24))
#define RESERVEDAREA (2*sizeof(unsigned long))
#define FREEMASK 0x80000000

static void DefaultDiskMappedMemory_AllocError(CDiskMappedMemory *obj,unsigned long size)
  {    
  }

CDiskMappedMemory::CDiskMappedMemory(int grow)
{
_mapObject=NULL;
_base=NULL;
_alloc=0;
_used=0;
_grow=grow;
_fileHandle=NULL;
_autoclose=false;
_allocError=DefaultDiskMappedMemory_AllocError;
}

CDiskMappedMemory::~CDiskMappedMemory()
{
if (_fileHandle) Close();
}

bool CDiskMappedMemory::Create(HANDLE handle,bool autoclose)
  {
  _fileHandle=handle;
  unsigned long size=GetFileSize(_fileHandle,NULL);
  _autoclose=autoclose;
  if (MapFile(size==0?_grow:size)==false) return false;  
  if (size==0) FormatMemory();
  else _used=(unsigned long *)_base;  
  return true;
  }
bool CDiskMappedMemory::Create(const char *fname)
  {
  HANDLE h=CreateFile(fname,GENERIC_READ|GENERIC_WRITE,0,NULL,OPEN_ALWAYS,
	FILE_ATTRIBUTE_NORMAL|FILE_FLAG_RANDOM_ACCESS,NULL);
  return Create(h,true);
  }

bool CDiskMappedMemory::Close()
  {
  if (OptimizeFile(false)==false) return false;
  if (_autoclose) CloseHandle(_fileHandle);
  _fileHandle=NULL;
  return true;
  }

bool CDiskMappedMemory::MapFile(unsigned long alloc)
  {
  if (_mapObject) UnMapFile();
  _mapObject=CreateFileMapping(_fileHandle,NULL,PAGE_READWRITE,0,alloc,NULL);
  if (_mapObject==INVALID_HANDLE_VALUE) return false;
  _base=MapViewOfFile(_mapObject,FILE_MAP_ALL_ACCESS,0,0,0);
  if (_base==NULL)
	{
	CloseHandle(_mapObject);
	_mapObject=NULL;
	return false;
	}
  _used=(unsigned long *)_base;
  _alloc=alloc;
  return true;
  }

bool CDiskMappedMemory::UnMapFile()
  {
  ASSERT(_mapObject!=NULL);
  ASSERT(_base!=NULL);
  ASSERT(_fileHandle!=NULL);
  
  if (UnmapViewOfFile(_base)==FALSE) return false;
  CloseHandle(_mapObject);
  return true;
  }


unsigned long CDiskMappedMemory::AllocMem(unsigned long size)
  {
  ASSERT(_base!=NULL);
  size=(size+7) & ~0x7;
  if (_used[0]+size+16>_alloc)
	if (MapFile(_alloc+__max(_grow,size))==false) 
	  {
	  _allocError(this,size);
	  return 0;
	  }
  unsigned long *binfo;
  Map(*_used,binfo);
  binfo[0]=size;  
  binfo[1]=MAGICVALUE;
  unsigned long ret=_used[0]+=RESERVEDAREA;
  _used[0]+=size;
  return ret;
  }

void CDiskMappedMemory::DeallocMem(unsigned long offset)
  {
  ASSERT(_base!=NULL);
  unsigned long *binfo;
  Map(offset-RESERVEDAREA,binfo);
  ASSERT((binfo[0] & FREEMASK)==0);
  ASSERT(binfo[1]==MAGICVALUE);
  if (offset+*binfo==*_used) 
	*_used=Map(binfo);
  else
	binfo[0] |= FREEMASK;
  }
unsigned long CDiskMappedMemory::EnumBlocks(unsigned long enumoffs)
  {
  ASSERT(_base!=NULL);
  if (enumoffs==0)
	{
	enumoffs+=RESERVEDAREA+sizeof(DWORD);
	return enumoffs;
	}
  else
	{	
	unsigned long *binfo;
	Map(enumoffs-RESERVEDAREA,binfo);
	enumoffs+=(binfo[0] & ~FREEMASK)+RESERVEDAREA;
	if (enumoffs>=*_used) return 0;
	return enumoffs;
	}
  }

bool CDiskMappedMemory::GetBlockUsed(unsigned long offset)
  {
  ASSERT(_base!=NULL);
  unsigned long *binfo;
  Map(offset-RESERVEDAREA,binfo);
  return (*binfo & FREEMASK)==0;
  }

void CDiskMappedMemory::Optimize()
  {
  ASSERT(_base!=NULL);
  unsigned long p1,p2;
  unsigned long *ptr1,*ptr2;
  p1=EnumBlocks(0);
  p2=p1;
  while (p1)
	{
	if (GetBlockUsed(p1))
	  {
	  if (p1!=p2)
		{
		Map(p1-RESERVEDAREA,ptr1);
		Map(p2-RESERVEDAREA,ptr2);
		p1=EnumBlocks(p1);
		memcpy(ptr2,ptr1,*ptr1+RESERVEDAREA);
		p2=EnumBlocks(p2);
		}
	  else
		{
		p1=EnumBlocks(p1);
		p2=EnumBlocks(p2);
		}
	  }
	else
  	  p1=EnumBlocks(p1);	  
	}
  *_used=p2-RESERVEDAREA;
  }

unsigned long CDiskMappedMemory::GetBlockSize(unsigned long offset)
  {
  ASSERT(_base!=NULL);
  unsigned long *ptr;
  Map(offset-RESERVEDAREA,ptr);
  return *ptr & ~FREEMASK;
  }

unsigned long CDiskMappedMemory::CalcFragmentation()
  {
  ASSERT(_base!=NULL);
  unsigned long suma=0;
  for (unsigned long p=0;p=EnumBlocks(p);)
	{
	if (!GetBlockUsed(p)) suma+=GetBlockSize(p);
	}
  return suma;
  }

bool CDiskMappedMemory::OptimizeFile(bool remap)	
  {
  ASSERT(_base!=NULL);
  unsigned long used=GetUsed();
  if (!UnMapFile()) return false;
  SetFilePointer(_fileHandle,used,NULL,FILE_BEGIN);
  SetEndOfFile(_fileHandle);
  if (remap) 
	if (MapFile(used)==false) return false;
  return true;
  }

void CDiskMappedMemory::FormatMemory()
  {
  _used=(unsigned long *)_base;  
  *_used=sizeof(*_used);
  }
