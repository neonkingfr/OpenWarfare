// LineTerminalNr.h: interface for the CLineTerminalNr class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_LINETERMINALNR_H__4F9EF42D_13AE_4611_82E7_0789454A0C31__INCLUDED_)
#define AFX_LINETERMINALNR_H__4F9EF42D_13AE_4611_82E7_0789454A0C31__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "ArchiveStreamTCP.h"
#include "ArchiveStreamMemory.h"
#include "LineTerminal2.h"

#define CLT_OK 0
#define CLT_READTIMEOUT -1
#define CLT_WRITETIMEOUT -2
#define CLT_CONNECTIONRESET -3
#define CLT_INVALIDREPLY -5
#define CLT_CONNECTFAILED -6
#define CLT_INTERRUPTED -7


class CLineTerminalNr : public LineTerminal  
{
    ArchiveStreamTCPIn _tcpin;
    ArchiveStreamTCPOut _tcpout;
	Socket _conn;

public:
	HWND stopwnd; //window to handle STOP button
	static HANDLE debugout;
	static bool stop;


	bool GetHostByName(const char *name, SOCKADDR_IN &host);
	static bool DebugOpened();
	static void CloseDebug();
	static void OpenDebug();

public:
	CLineTerminalNr();
	virtual ~CLineTerminalNr();

	void Init();
	int Connect(const char *address, unsigned int port);
	bool IsConnected() {return _conn.IsValid();}

};

#endif // !defined(AFX_LINETERMINALNR_H__4F9EF42D_13AE_4611_82E7_0789454A0C31__INCLUDED_)
