// LineTerminal.cpp: implementation of the LineTerminal class.
//
//////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <winsock.h>
#include "LineTerminal2.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

LineTerminal::LineTerminal(ArchiveStream &input,ArchiveStream &output):
_input(input),_output(output)
  {

  }

LineTerminal::~LineTerminal()
  {

  }

const char * LineTerminal::ReadLine()
  {
  if (!_input.IsReading()) return NULL;
   _line.Rewind();
  char zn;
  _input.ExSimple(zn);
  while (!_input.IsError())
    {    
    if (zn!='\r')
      {
      if (zn=='\n')
        {
        zn=0;
        _line.ExSimple(zn);
        return _line.GetBuffer();
        }
      else
        _line.ExSimple(zn);
      }
    _input.ExSimple(zn);
    }
  return NULL;
  } 

bool LineTerminal::SendLine(const char *text, int len, bool sendcrlf)
  {
  if (len==-1) len=strlen(text);
  if (len!=0)
    _output.DataExchange((void *)text,len);
  if (sendcrlf)
    {
    text="\r\n";
    _output.DataExchange((void *)text,2);
    }
  return !_output.IsError();
  }

bool LineTerminal::SendFormattedV(const char *format, va_list args)
  {
  unsigned int buffersize=256;
  int res=0;
  do
    {
    if (_line.GetBufferSize()<buffersize) _line.Reserved(buffersize-_line.GetBufferSize()+10);
    res=_vsnprintf(_line.GetBuffer(),buffersize,format,args);
    if (res==-1) buffersize*=2;
    }
  while (res==-1);
  SendLine(_line.GetBuffer(),res);
  return !_output.IsError();
  }

bool LineTerminal::SendFormatted(const char *format,...)
  {
  va_list args;
  va_start(args,format);
  return SendFormattedV(format,args);
  }

bool LineTerminal::SplitAt(const char *part, char **left, char **right, char splitter,bool dupsplitter)
  {
  if (part==NULL) part=_line.GetBuffer();
  *left=const_cast<char *>(part);
  *right=NULL;
  if (part<_line.GetBuffer() || part>=(_line.GetBuffer()+_line.GetBufferSize())) return false;
  char *z=const_cast<char *>(strchr(part,splitter));
  if (z==NULL) return false;
  *z=0;
  z++;
  if (dupsplitter) while (*z && *z==splitter) z++;
  *right=z;
  return true;
  }


