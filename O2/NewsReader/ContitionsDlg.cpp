// ContitionsDlg.cpp : implementation file
//

#include "stdafx.h"
#include "newsreader.h"
#include "ContitionsDlg.h"
using namespace std;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CContitionsDlg dialog


CContitionsDlg::CContitionsDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CContitionsDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CContitionsDlg)
	cond = _T("");
	name = _T("");
	//}}AFX_DATA_INIT
}


void CContitionsDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CContitionsDlg)
	DDX_Text(pDX, IDC_CONDITION, cond);
	DDX_Text(pDX, IDC_NAME, name);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CContitionsDlg, CDialog)
	//{{AFX_MSG_MAP(CContitionsDlg)
	ON_EN_CHANGE(IDC_NAME, OnChangeName)
	ON_EN_CHANGE(IDC_CONDITION, OnChangeName)
	ON_BN_CLICKED(IDC_DELETE, OnDelete)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CContitionsDlg message handlers

void CContitionsDlg::OnOK() 
  { 
  UpdateData();
  bool ok=clc.Compile(cond);  
  ostrstream out;
  clc.Print(out); 
  out.put((char)0);
  if (ok)
	{
	cond=out.str();
	UpdateData(FALSE);
	CDialog::OnOK();
	}
  else
	{
	CString p;
	AfxFormatString1(p,IDS_COMPILEERROR,out.str());
	MessageBox(p,NULL,MB_OK);
	}
  out.rdbuf()->freeze(0);
  }

void CContitionsDlg::OnChangeName() 
  {
  int sz=GetDlgItem(IDC_NAME)->GetWindowTextLength()+
	GetDlgItem(IDC_CONDITION)->GetWindowTextLength();
  GetDlgItem(IDOK)->EnableWindow(sz);
  }

BOOL CContitionsDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CContitionsDlg::OnDelete() 
{
EndDialog(IDC_DELETE);	
}
