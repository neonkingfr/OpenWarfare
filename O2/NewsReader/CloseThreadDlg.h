#if !defined(AFX_CLOSETHREADDLG_H__BB511960_8EA5_11D5_B39F_00C0DFAE7D0A__INCLUDED_)
#define AFX_CLOSETHREADDLG_H__BB511960_8EA5_11D5_B39F_00C0DFAE7D0A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// CloseThreadDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CCloseThreadDlg dialog

class CCloseThreadDlg : public CDialog
{
// Construction
public:
	bool subjectwarning;
	CCloseThreadDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CCloseThreadDlg)
	enum { IDD = IDD_CLOSETHREAD };
	CString	kwrds;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCloseThreadDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CCloseThreadDlg)
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CLOSETHREADDLG_H__BB511960_8EA5_11D5_B39F_00C0DFAE7D0A__INCLUDED_)
