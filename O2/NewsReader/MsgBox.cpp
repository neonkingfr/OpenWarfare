// MsgBox.cpp : implementation file
//

#include "stdafx.h"
#include "newsreader.h"
#include "MsgBox.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMsgBox dialog


CMsgBox::CMsgBox(CWnd* pParent /*=NULL*/)
	: CDialog(CMsgBox::IDD, pParent)
{
	//{{AFX_DATA_INIT(CMsgBox)
	noagain = FALSE;
	message = _T("");
	//}}AFX_DATA_INIT
	memset(&Flags,0,sizeof(Flags));
}


void CMsgBox::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CMsgBox)
	DDX_Control(pDX, IDC_NOAGAIN, noagainbutt);
	DDX_Control(pDX, IDCANCEL, cancel);
	DDX_Control(pDX, IDNO, no);
	DDX_Control(pDX, IDOK, ok);
	DDX_Control(pDX, IDYES, yes);
	DDX_Check(pDX, IDC_NOAGAIN, noagain);
	DDX_Text(pDX, IDC_MESSAGE, message);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CMsgBox, CDialog)
	//{{AFX_MSG_MAP(CMsgBox)
	ON_BN_CLICKED(IDNO, OnNo)
	ON_BN_CLICKED(IDYES, OnYes)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMsgBox message handlers

void CMsgBox::OnCancel() 
  {
  UpdateData();
  if (Flags.cancelbutt) EndDialog(IDCANCEL);
  else if (Flags.nobutt) EndDialog(IDNO);
  else EndDialog(IDCANCEL);
  }

void CMsgBox::OnNo() 
  {
  UpdateData();
  EndDialog(IDNO);	
  }

void CMsgBox::OnOK() 
  {
  UpdateData();
  EndDialog(IDOK);	
  }

void CMsgBox::OnYes() 
  {
  UpdateData();
  EndDialog(IDYES);	
  }

BOOL CMsgBox::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	if (Flags.cancelbutt) cancel.ShowWindow(SW_SHOW);
	if (Flags.noagain) noagainbutt.ShowWindow(SW_SHOW);
	if (Flags.nobutt) no.ShowWindow(SW_SHOW);
	if (Flags.yesbutt) yes.ShowWindow(SW_SHOW);
	if (Flags.okbutt) ok.ShowWindow(SW_SHOW);
	if (Flags.noagain && Flags.cancelbutt)
	  {
	  CRect rc;
	  no.GetWindowRect(&rc);ScreenToClient(&rc);
	  cancel.MoveWindow(&rc);
	  }
	MessageBeep(msgBeep);
	return TRUE;  // return TRUE unless you set the focus to a control
              // EXCEPTION: OCX Property Pages should return FALSE
}
