// NewsReader.cpp : Defines the class behaviors for the application.
//
//#define COMPILE_MULTIMON_STUBS

#include "stdafx.h"
#include "NewsReader.h"
#include <typeinfo.h>
#include <io.h>

#include "MainFrm.h"
#include <initguid.h>
#include <El/Pathname/Pathname.h>
#include "FreezeDlg.h"

//#include "NewsReader_i.c"
#include "MsgBox.h"

//#define COMPILE_MULTIMON_STUBS
#include <multimon.h>
#include "FullTextIndex.h"

#include "KeyCheck.h"

using namespace std;


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CNewsReaderApp

BEGIN_MESSAGE_MAP(CNewsReaderApp, CWinApp)
  //{{AFX_MSG_MAP(CNewsReaderApp)
  ON_COMMAND(ID_APP_ABOUT, OnAppAbout)
    // NOTE - the ClassWizard will add and remove mapping macros here.
    //    DO NOT EDIT what you see in these blocks of generated code!
    //}}AFX_MSG_MAP
    END_MESSAGE_MAP()
      
      /////////////////////////////////////////////////////////////////////////////
      // CNewsReaderApp construction
      
      CNewsReaderApp::CNewsReaderApp()
        {
        backgrnd[0]=NULL;
        backgrnd[1]=NULL;
        backgrnd[2]=NULL;
        fullpathbuff=NULL;
        fpblen=0;
        // TODO: add construction code here,
        // Place all significant initialization in InitInstance
        }

//--------------------------------------------------

/////////////////////////////////////////////////////////////////////////////
// The one and only CNewsReaderApp object

CNewsReaderApp theApp;
bool runflag=false;

UINT RunHTTPServer(LPVOID data);

/////////////////////////////////////////////////////////////////////////////
// CNewsReaderApp initialization

CCharMapCz czmap;

#include "Splash.h"

#include "DiskIndex2.h"


BOOL CNewsReaderApp::InitInstance()
  {

/*
  KeyCheckClass checkKey;
  if (checkKey.VerifyKey()==false)
  {
    AfxMessageBox(IDS_INVALID_CD_KEY,MB_OK|MB_ICONSTOP);
    exit(-1);
  }
  */
  
  //  InitMultipleMonitorStubs();

  

  GetModuleFileName(AfxGetInstanceHandle(),appPath,MAX_PATH);
    {
    char *q=strrchr(appPath,'\\');
    q[1]=0;
    }
  userPath[0]=0;
  SHGetSpecialFolderPath(NULL,userPath,CSIDL_APPDATA ,TRUE);
  if (userPath[0])  strcat(userPath,"\\NewsReader");	
  
  SomeChecking();
  profchange=false;
  
  AfxEnableControlContainer();
  
  
  
  
  CCommandLineInfo cmdInfo;
  ParseCommandLine(cmdInfo);
  
  InitializeCriticalSection(&sect);
  
  if (cmdInfo.m_bRunEmbedded || cmdInfo.m_bRunAutomated)
    {
    return TRUE;
    }
  
  HWND ww=::FindWindow("STATIC",NREADERWINDOW);
  if (ww)
    {
    ww=::GetWindow(ww,GW_OWNER);
    if (IsIconic(ww)) OpenIcon(ww);
    if (!IsWindowVisible(ww)) ShowWindow(ww,SW_SHOWNORMAL);
    SetForegroundWindow(ww);
    BringWindowToTop(ww);
    if (__argc>1)
      {
      SECURITY_ATTRIBUTES sattr;
      memset(&sattr,0,sizeof(sattr));
      sattr.bInheritHandle=TRUE;
      sattr.nLength=sizeof(sattr);
      HANDLE mem=CreateFileMapping(NULL,NULL,PAGE_READWRITE,0,strlen(__argv[1])+1,NULL);
      if (mem)
        {
        char *shortcut=(char *)MapViewOfFile(mem,FILE_MAP_ALL_ACCESS,0,0,0);
        if (shortcut)
          {
          strcpy(shortcut,__argv[1]);
          UnmapViewOfFile((LPCVOID)shortcut);
          HANDLE event=CreateEvent(NULL,FALSE,FALSE,NULL);
          if (event)
            {
            HANDLE dup_mem,dup_event,process;
            DWORD processId;
            GetWindowThreadProcessId(ww,&processId);
            process=OpenProcess(PROCESS_DUP_HANDLE,FALSE,processId);
            DuplicateHandle(GetCurrentProcess(),mem,process,&dup_mem,0,FALSE,DUPLICATE_SAME_ACCESS);
            DuplicateHandle(GetCurrentProcess(),event,process,&dup_event,0,FALSE,DUPLICATE_SAME_ACCESS);
            PostMessage(ww,NRM_GOTOMESSAGE,(WPARAM)dup_event,(LPARAM)dup_mem);
            WaitForSingleObject(event,5000);
            CloseHandle(event);
            }
          }
        CloseHandle(mem);
        }
      }
    return FALSE;
    }
  
  CSplash *spls=new CSplash();
  spls->CreateThread(0,16384,NULL);
  spls->m_bAutoDelete=TRUE;
  TIME_ZONE_INFORMATION zone;
  DWORD zoneres=GetTimeZoneInformation(&zone);
  timezone.SetDateTimeSpan(0,0,-zone.Bias-(zoneres==TIME_ZONE_ID_DAYLIGHT?zone.DaylightBias:zone.StandardBias),0);
  
  WSADATA wsaData;
  WSAStartup(0x101,&wsaData);
  
/*  if (!AfxSocketInit())
    {
    AfxMessageBox(IDP_SOCKETS_INIT_FAILED);
    return FALSE;
    }*/
  
  /*
	if (server.Init()==FALSE)
	  {
	  AfxMessageBox(IDS_UNABLE_TO_CREATE_LOCAL_SERVER);
	  return FALSE;
	  }*/
  
  
  messfont.CreateFont(-12,0,0,0,FW_REGULAR,FALSE,FALSE,FALSE,DEFAULT_CHARSET,OUT_RASTER_PRECIS,CLIP_DEFAULT_PRECIS,
  DEFAULT_QUALITY,FF_DONTCARE,"Courier");
  
  
  // Standard initialization
  // If you are not using these features and wish to reduce the size
  //  of your final executable, you should remove from the following
  //  the specific initialization routines you do not need.

  /*
  #ifdef _AFXDLL
  Enable3dControls();			// Call this when using MFC in a shared DLL
  #else
  Enable3dControlsStatic();	// Call this when linking to MFC statically
  #endif
  */
  
  // Change the registry key under which our settings are stored.
  // TODO: You should modify this string to be something appropriate
  // such as the name of your company or organization.
  SetRegistryKey(_T("Bohemia Interactive Studio"));
  
  ilist.Create(IDB_LISTICONS,16,1,RGB(255,0,255));
  // To create the main window, this code creates a new frame window
  // object and then sets it as the application's main window object.
  
  CMainFrame* pFrame = new CMainFrame;
  m_pMainWnd = pFrame;
  
  // create and load the frame with its resources
  
  sendopt.LoadConfig();
  config.LoadConfig();
  proxy.LoadConfig();
  memcache.SetCacheSize(config.cache);
  
  pFrame->LoadFrame(IDR_MAINFRAME,
  WS_OVERLAPPEDWINDOW | FWS_ADDTOTITLE, NULL,
  NULL);
  
  offline=false;
  
  
  // The one and only window has been initialized, so show and update it.
  pFrame->ShowWindow(SW_SHOW);
  pFrame->UpdateWindow();
  mainthread=GetCurrentThreadId();
  DeleteFile("NewsReader.exe.old");
  CWinThread *thr=AfxBeginThread((AFX_THREADPROC)WatchDog,m_pMainWnd->GetSafeHwnd(),THREAD_PRIORITY_HIGHEST,0,0,NULL);  
  return TRUE;
  }

//--------------------------------------------------

/////////////////////////////////////////////////////////////////////////////
// CNewsReaderApp message handlers





/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
  {
  public:
    CAboutDlg();
    
    // Dialog Data
    //{{AFX_DATA(CAboutDlg)
    enum 
      { IDD = IDD_ABOUTBOX };
    //}}AFX_DATA
    
    // ClassWizard generated virtual function overrides
    //{{AFX_VIRTUAL(CAboutDlg)
  protected:
    virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
    //}}AFX_VIRTUAL
    
    // Implementation
  protected:
    //{{AFX_MSG(CAboutDlg)
    afx_msg void OnSelchangedTree1(NMHDR* pNMHDR, LRESULT* pResult);
    afx_msg void OnClickTree1(NMHDR* pNMHDR, LRESULT* pResult);
    afx_msg void OnDblclkTree1(NMHDR* pNMHDR, LRESULT* pResult);
    //}}AFX_MSG
    DECLARE_MESSAGE_MAP()
  };

//--------------------------------------------------

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
  {
  //{{AFX_DATA_INIT(CAboutDlg)
  //}}AFX_DATA_INIT
  }

//--------------------------------------------------

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
  {
  CDialog::DoDataExchange(pDX);
  //{{AFX_DATA_MAP(CAboutDlg)
  //}}AFX_DATA_MAP
  }

//--------------------------------------------------

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
  //{{AFX_MSG_MAP(CAboutDlg)
  ON_NOTIFY(TVN_SELCHANGED, IDC_TREE1, OnSelchangedTree1)
    ON_NOTIFY(NM_CLICK, IDC_TREE1, OnClickTree1)
      ON_NOTIFY(NM_DBLCLK, IDC_TREE1, OnDblclkTree1)
        //}}AFX_MSG_MAP
        END_MESSAGE_MAP()
          
          // App command to run the dialog
          void CNewsReaderApp::OnAppAbout()
            {
            //	CAboutWindow aboutDlg;
            if ((HWND)about==NULL)
              about.Create(m_pMainWnd);
            else
              about.BringWindowToTop();
            }

//--------------------------------------------------

/////////////////////////////////////////////////////////////////////////////
// CNewsReaderApp message handlers


void CAboutDlg::OnSelchangedTree1(NMHDR* pNMHDR, LRESULT* pResult) 
  {
  NM_TREEVIEW* pNMTreeView = (NM_TREEVIEW*)pNMHDR;
  // TODO: Add your control notification handler code here
  
  *pResult = 0;
  }

//--------------------------------------------------

void CAboutDlg::OnClickTree1(NMHDR* pNMHDR, LRESULT* pResult) 
  {
  // TODO: Add your control notification handler code here
  
  *pResult = 0;
  }

//--------------------------------------------------

void CAboutDlg::OnDblclkTree1(NMHDR* pNMHDR, LRESULT* pResult) 
  {
  
  *pResult = 0;
  }

//--------------------------------------------------

static UINT RunThread( LPVOID pParam)
  {
  CRunner *run=(CRunner *)pParam;
/*  if (!AfxSocketInit(NULL)) return 1;
  
  #ifndef _AFXDLL	
  AFX_MODULE_THREAD_STATE *state=AfxGetModuleThreadState();
  if (state->m_pmapSocketHandle==NULL)
    {
    state->m_pmapSocketHandle=new CMapPtrToPtr();
    state->m_pmapDeadSockets=new CMapPtrToPtr();
    state->m_plistSocketNotifications=new CPtrList();
    }
  #endif
  */
  try
    {
    run->Run();
    }
  catch (...)
    {
    AfxMessageBox(IDS_THREADEXCEPTION);
    }
  delete run;  
  return 0;
  }

//--------------------------------------------------

bool CNewsReaderApp::RunBackground(int index,CRunner *run, DWORD timetowait,bool lowpriority)
  {
  if (backgrnd[index]==BGRNCLOSEDTH) return false;
  if (backgrnd[index]!=NULL)
    {
    if (timetowait!=0) TRACE1("Waiting for thread to terminate: %d\n",index);
    DWORD p=GetTickCount();
    DWORD res;
    do
      {
      res=MsgWaitForMultipleObjects(1,&(backgrnd[index]->m_hThread),FALSE,timetowait,QS_SENDMESSAGE);
      if (res==WAIT_OBJECT_0 + 1) 
        {
        if (timetowait==0) return false;
        MSG msg;
        while (::PeekMessage(&msg,0,0,0,PM_REMOVE))
          DispatchMessage(&msg);
        }
      if (timetowait!=INFINITE)
        {
        timetowait-=(GetTickCount()-p);
        if (timetowait>0x7FFFFFFF)  timetowait=0;
        }
      if (res==WAIT_TIMEOUT ) return false;	  
      }
    while (res!=WAIT_OBJECT_0 && backgrnd[index]!=NULL);
    delete backgrnd[index];
    }
  if (run==BGRNCLOSED) backgrnd[index]=BGRNCLOSEDTH;
  else
    if (run!=NULL)
      {
      int priority=lowpriority?THREAD_PRIORITY_BELOW_NORMAL:THREAD_PRIORITY_NORMAL;
      backgrnd[index]=AfxBeginThread(RunThread,(LPVOID)run,priority,0,CREATE_SUSPENDED,NULL);
      backgrnd[index]->m_bAutoDelete=FALSE;
      TRACE3("Starting thread: %d 0x%08X %s\n",index,backgrnd[index]->m_nThreadID,typeid(*run).name());
      backgrnd[index]->ResumeThread();
      }
  else
    backgrnd[index]=NULL;
  return true;
  }

//--------------------------------------------------

BOOL CALLBACK WndKillProc( HWND hwnd, LPARAM lParam)
  {
  PostMessage(hwnd,WM_COMMAND,IDCANCEL,0);
  return TRUE;
  }

//--------------------------------------------------

int CNewsReaderApp::ExitInstance() 
  {
  if (chkver) 
    {
    EnumThreadWindows(chkver->m_nThreadID,WndKillProc,0);
    WaitForSingleObject(chkver->m_hThread,INFINITE);
    }
  DeleteCriticalSection(&sect);
  splashthrd.kill=true;
  WaitForSingleObject(splashthrd.m_hThread,INFINITE);
  TRACE0("Cleanup... Done\n");
  if (profchange)
    if (useprofiles)
      ToSingleMode();
  else
    ToMultiMode();
  StringBank.Clear();
  return CWinApp::ExitInstance();
  }

//--------------------------------------------------

void RelMoveWnd(CWnd *wnd, int relx, int rely)
  {
  CRect rc;
  wnd->GetWindowRect(&rc);
  if (wnd->GetParent()) 
    wnd->GetParent()->ScreenToClient(&rc);
  rc.OffsetRect(relx,rely);
  wnd->MoveWindow(rc,FALSE);
  }

//--------------------------------------------------

void RelSizeWnd(CWnd *wnd, int relx, int rely)
  {
  CRect rc;
  wnd->GetWindowRect(&rc);
  if (wnd->GetParent()) 
    wnd->GetParent()->ScreenToClient(&rc);
  rc.bottom+=rely;
  rc.right+=relx;
  wnd->MoveWindow(rc,FALSE);
  }

//--------------------------------------------------

static char Months[12][4]=
  {"Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"};

//--------------------------------------------------

static int Dayz[12]=
  {31,29,31,30,31,30,31,31,30,31,30,31};

//--------------------------------------------------

static char Days[7][4]=
  {"Sun","Mon","Tue","Wen","Thr","Fri","Sat"};

//--------------------------------------------------

COleDateTime ParseStringDate(const char *date)
  {
  while (*date && !isdigit(*date)) date++;
  int day=0;
  while (*date && isdigit(*date)) day=day*10+(*(date++)-'0');
  char mnthstr[4];
  int i;
  while (*date && !isalnum(*date)) date++;
  for (i=0;*date && isalnum(*date) && i<3;i++) mnthstr[i]=*date++;
  mnthstr[3]=0;
  for (i=0;i<12;i++) if (stricmp(Months[i],mnthstr)==0) break;
  if (i==12) i=0;
  int month=i+1;
  while (*date && !isdigit(*date)) date++;
  int year=0;
  while (*date && isdigit(*date)) year=year*10+(*(date++)-'0');
  if (year<100)
    if (year<50) year+=2000;
  else year+=1900;
  while (*date && !isdigit(*date)) date++;
  int hour=0;
  while (*date && isdigit(*date)) hour=hour*10+(*(date++)-'0');
  while (*date && !isdigit(*date)) date++;
  int min=0;
  while (*date && isdigit(*date)) min=min*10+(*(date++)-'0');
  while (*date && !isdigit(*date)) date++;
  int sec=0;
  while (*date && isdigit(*date)) sec=sec*10+(*(date++)-'0');
  bool plus=true;
  while (*date && *date==' ') date++;
  if (*date=='+') plus=true;
  if (*date=='-') plus=false;
  int offset=0;  
  date++;
  sscanf(date,"%d",&offset);
  if (!plus)
    {
    min+=offset%100;
    hour+=offset/100;
    while (min>59) 
      {hour++;min-=60;}
    while (hour>23) 
      {day++;hour-=24;}
    while (day>Dayz[month-1]) 
      {day-=Dayz[month-1];month++;}
    while (month>12) 
      {year++;month-=12;}
    }
  else
    {
    min-=offset%100;
    hour-=offset/100;
    while (min<0) 
      {hour--;min+=60;}
    while (hour<0) 
      {day--;hour+=24;}
    while (day<1) 
      {month--;day+=Dayz[month-1];}
    while (month<1) 
      {year--;month+=12;}
    }
  COleDateTime p(year,month,day,hour,min,sec);
  return p;
  }

//--------------------------------------------------

void DateToString(COleDateTime &odt,CString &s)
  {
  s.Format("%s, %02d %s %04d %02d:%02d:%02d GMT",
  Days[odt.GetDayOfWeek()-1],odt.GetDay(),Months[odt.GetMonth()],
  odt.GetYear(),odt.GetHour(),odt.GetMinute(),odt.GetSecond());
  }

//--------------------------------------------------

void GetEmailFromSender(const char *name,CString& out)
  {  
  const char *p=strchr(name,'<');
  const char *q=p?strchr(p,'>'):NULL;
  if (p==NULL || q==NULL) 
    {
    p=strchr(name,32);
    q=strchr(name,'(');
    if (p==NULL && q==NULL) out=name;
    else
      {
      if (p==NULL) p=q;
      if (q!=NULL && q<p) p=q;
      char *z=(char *)alloca(p-name+1);
      strncpy(z,name,p-name);
      z[p-name]=0;
      out=z;
      }	  
    }
  else
    {
    char *z=(char *)alloca(q-p);
    strncpy(z,p+1,q-p);
    z[q-p-1]=0;
    out=z;
    }  
  }

//--------------------------------------------------

void SetTooltip(CWnd *dlg,CWnd *cursorwnd)
  {
  static HWND lastcur, lastww;
  if (dlg==NULL) return;
  CWnd *ww=dlg->GetDlgItem(IDC_TOOLTIP);
  if (ww==NULL) return;
  CString text;
  if (cursorwnd)
    {
    if (lastcur==*cursorwnd && lastww==*ww) return;
    int id=cursorwnd->GetDlgCtrlID();
    text.LoadString(id);
    lastcur=*cursorwnd;
    }
  else
    {
    if (lastcur==NULL && lastww==*ww) return;
    lastcur=NULL;
    }
  ww->SetWindowText(text);
  lastww=*ww;  
  }

//--------------------------------------------------

void ExpandFilename(CString &filename, CString &out)
  {
  const char *name=theApp.FullPath(filename);
  out=name;
  
  /*
#define sz 2*MAX_PATH
  char buff[sz];
  char *part;
  GetFullPathName(filename,sz,buff,&part);
  out=buff;
#undef sz*/
  }

//--------------------------------------------------

#include "AskWorkMode.h"

bool CNewsReaderApp::OfflineWarn()
  {
  if (offline)
    {
    CAskWorkMode wm(CWnd::GetActiveWindow());
    int id=wm.DoModal();
    if (id==IDOK) 
      {offline=false;return true;}
    if (id==IDCANCEL) return false;
    }
  return true;
  }

//--------------------------------------------------

int NrMessageBox(const char *text,UINT type)
  {
  CMsgBox msgbox(CWnd::GetActiveWindow());
  msgbox.msgBeep=type;
  type&=0xF;
  if (type==MB_OK) msgbox.Flags.okbutt=1;
  if (type==MB_OKCANCEL) msgbox.Flags.okbutt=msgbox.Flags.cancelbutt=1;
  if (type==MB_YESNO) msgbox.Flags.yesbutt=msgbox.Flags.nobutt=1;
  if (type==MB_YESNOCANCEL) msgbox.Flags.yesbutt=msgbox.Flags.nobutt=msgbox.Flags.cancelbutt=1;
  msgbox.message=text;  
  return msgbox.DoModal();
  }

//--------------------------------------------------

int NrMessageBox(int id,UINT type)
  {
  char buff[1024];
  LoadString(theApp.m_hInstance,id,buff,sizeof(buff));
  return NrMessageBox(buff,type);
  }

//--------------------------------------------------

static HWND *wndlist=NULL;
static int wndallc=0;

bool NrRegWindow(CWnd *wnd)
  {
  theApp.Lock();
  int i;
  for (i=0;i<wndallc;i++) if (wndlist[i]==NULL) break;
  if (i==wndallc)
    {
    int nc=wndallc*2;if (nc==0) nc=16;
    HWND *q=(HWND *)realloc(wndlist,sizeof(HWND)*nc);
    if (q)
      {
      memset(q+wndallc,0,sizeof(HWND )*(nc-wndallc));
      wndallc=nc;
      wndlist=q;
      theApp.Unlock();
      return NrRegWindow(wnd);
      }
    else
      {
      theApp.Unlock();
      return false;
      }
    }
  wndlist[i]=wnd->GetSafeHwnd();
  theApp.Unlock();
  return true;
  }

//--------------------------------------------------

void NrUnregWindow(CWnd *wnd)
  {
  theApp.Lock();
  for (int i=0;i<wndallc;i++) if (wndlist[i]==wnd->GetSafeHwnd()) wndlist[i]=NULL;
  theApp.Unlock();
  }

//--------------------------------------------------

void NrEnableWindows(BOOL enable)
  {
  for (int i=0;i<wndallc;i++) if (wndlist[i])
    EnableWindow(wndlist[i],enable);
  theApp.m_pMainWnd->EnableWindow(enable);
  }

//--------------------------------------------------

void NrRemoveInvalidWindows()
  {
  for (int i=0;i<wndallc;i++) if (wndlist[i] && !IsWindow(wndlist[i])) wndlist[i]=NULL;
  }

//--------------------------------------------------

bool NrKillWindows()
  {
  for (int i=0;i<wndallc;i++)
    {
    HWND ww=wndlist[i];
    MSG msg;
    if (wndlist[i]) SendMessage(wndlist[i],WM_CLOSE,0,0);
    while (::PeekMessage(&msg,ww,0,0,PM_REMOVE)) DispatchMessage(&msg);
    if (wndlist[i] && !IsWindow(wndlist[i])) wndlist[i]=NULL;
    if (wndlist[i] ) return false;
    }
  return true;
  }

//--------------------------------------------------

static CCriticalSection IsolateSection;

void CNewsReaderApp::IsolateThread()
  {
  IsolateSection.Lock();
  if (mainthread!=GetCurrentThreadId())
    {
    SuspendThread();
    }
  for (int i=0;i<TOTALTHREADS;i++)
    {
    if (backgrnd[i] && backgrnd[i]!=BGRNCLOSEDTH && backgrnd[i]->m_nThreadID!=GetCurrentThreadId())
      backgrnd[i]->SuspendThread();
    }
  }

//--------------------------------------------------

void CNewsReaderApp::UnisolateThread()
  {
  if (mainthread!=GetCurrentThreadId())
    {
    ResumeThread();
    }
  for (int i=0;i<TOTALTHREADS;i++)
    {
    if (backgrnd[i] && backgrnd[i]!=BGRNCLOSEDTH && backgrnd[i]->m_nThreadID!=GetCurrentThreadId())
      backgrnd[i]->ResumeThread();
    }
  IsolateSection.Unlock();
  }

//--------------------------------------------------

#include "ScanGroupsThread.h"
void CNewsReaderApp::CheckForNewVersion(BOOL ask)
  {
  AfxMessageBox("Function is disabled");
/*  if (chkver)
    {
    if (WaitForSingleObject(chkver->m_hThread,0)==WAIT_TIMEOUT) return;
    delete chkver;
    }
  chkver=AfxBeginThread((AFX_THREADPROC)CheckNewerVersionThread,(LPVOID)ask,THREAD_PRIORITY_LOWEST,16384,CREATE_SUSPENDED);
  chkver->m_bAutoDelete=false;
  chkver->ResumeThread();  */
  }

//--------------------------------------------------

bool CNewsReaderApp::EnableCheckForNewVersion()
  {
  if (chkver==NULL) return true;
  if (WaitForSingleObject(chkver->m_hThread,0)==WAIT_TIMEOUT) return false;
  delete chkver;
  chkver=NULL;
  return true;
  }

//--------------------------------------------------

#ifdef DEMO
void DemoWarn()
  {
  NrMessageBox("This feature is disabled in DEMO version");
  }

//--------------------------------------------------

#endif

void CNewsReaderApp::SomeChecking()
  {
  CString p=appPath;
  p+="profiles";
  useprofiles=(_access(p,0)==0);
  if (useprofiles) 
    {
    fstream out(p,ios::out|ios::trunc);
    if (!out) enable_update=false;
    else enable_update=true;
	CreateDirectory(userPath,0);
    }
  else 
    enable_update=true;
  
  fstream str(FullPath("version.nfo",true),ios::in);
  if (!str) 
    strcpy(version,"Sat, 10 Aug 2002 23:59:59 GMT");
  else
    str.getline(version,sizeof(version));
  }

//--------------------------------------------------

const char * CNewsReaderApp::FullPath(const char *filename,bool apppath)
  { 
  const char *path=(apppath | !useprofiles)?appPath:userPath;
  int len1=strlen(path);
  int len2=strlen(filename);
  int lenc=len1+len2+5;
  if (fpblen<lenc) 
    {
    char *c=(char *)realloc(fullpathbuff,lenc*sizeof(char));
    if (c==NULL) 
      {
      NrMessageBox("Out of memory",MB_OK);
      abort();
      }
    fullpathbuff=c;
    fpblen=lenc;
    }
  strcpy(fullpathbuff,path);
  if (fullpathbuff[len1-1]!='\\') strcat(fullpathbuff,"\\");
  strcat(fullpathbuff,filename);
  return fullpathbuff;
  }

//--------------------------------------------------

static void MassMoveFiles(const char *extension, const char *from, const char *to)
{
  CFileFind fnd;
  Pathname pfrom;
  Pathname pto;
  pfrom.SetDirectory(from);
  pto.SetDirectory(to);
  pfrom.SetExtension(extension);
  pfrom.SetFiletitle("*");
  CreateDirectory(pto.GetDirectoryWithDriveWLBS(),NULL);
  BOOL nxt=fnd.FindFile(pfrom);
  while (nxt)
  {
    nxt=fnd.FindNextFile();
    pfrom.SetFilename(fnd.GetFileName());
    pto.SetFilename(pfrom.GetFilename());
    MoveFileEx(pfrom,pto,MOVEFILE_COPY_ALLOWED|MOVEFILE_REPLACE_EXISTING);
  }
}


void CNewsReaderApp::ToMultiMode()
  {
  if (AfxMessageBox(IDS_ASKPROFILESENABLE,MB_OKCANCEL)==IDOK)
    {
    MassMoveFiles(".nr",appPath,userPath);
    MassMoveFiles(".bak",appPath,userPath);
    MassMoveFiles(".acc",appPath,userPath);
    MassMoveFiles(".msg",appPath,userPath);
    MassMoveFiles(".ini",appPath,userPath);
    MassMoveFiles(".*",CString(appPath)+"\\Cache",CString(userPath)+"\\Cache");
    fstream out(theApp.FullPath("profiles"),ios::out|ios::trunc);
    char buff[MAX_PATH];
    GetModuleFileName(AfxGetInstanceHandle(),buff,sizeof(buff));
    ShellExecute(NULL,NULL,buff,NULL,NULL,SW_SHOWNORMAL);
    }
  }

//--------------------------------------------------

void CNewsReaderApp::ToSingleMode()
  {
  if (AfxMessageBox(IDS_TOSINGLEMODE,MB_OKCANCEL)==IDOK)
    {
    MassMoveFiles(".nr",userPath,appPath);
    MassMoveFiles(".bak",userPath,appPath);
    MassMoveFiles(".acc",userPath,appPath);
    MassMoveFiles(".msg",userPath,appPath);
    MassMoveFiles(".ini",userPath,appPath);
    MassMoveFiles(".*",CString(userPath)+"\\Cache",CString(appPath)+"\\Cache");
    DeleteFile(theApp.FullPath("profiles",true));
    char buff[MAX_PATH];
    GetModuleFileName(AfxGetInstanceHandle(),buff,sizeof(buff));
    ShellExecute(NULL,NULL,buff,NULL,NULL,SW_SHOWNORMAL);
    }
  }

//--------------------------------------------------

void CNewsReaderApp::GetVersionTime(CString &ver)
  {
  ver=version;
  }

//--------------------------------------------------

void CNewsReaderApp::SetVersionTime(const char *ver)
  {
  fstream str(FullPath("version",true),ios::out|ios::trunc);
  str<<ver<<"\n";
  int l=sizeof(version)/sizeof(version[0]);
  strncpy(version,ver,l);
  version[l-1]=0;
  }

//--------------------------------------------------

CString CNewsReaderApp::GetVersionString()
  {
  CString p,q;
  p.LoadString(IDS_VERINFO);
  q.Format(p,ACC_VERSION2>>8,ACC_VERSION2&0xFF,version);
  return q;
  }

//--------------------------------------------------

int CNewsReaderApp::GetRegInt(const char *name,int defval)
  {
  return GetProfileInt("Misc",name,defval);
  }

//--------------------------------------------------

void CNewsReaderApp::SetRegInt(const char *name,int val)
  {
  WriteProfileInt("Misc",name,val);
  }

//--------------------------------------------------

bool CNewsReaderApp::CleanThreads(DWORD timeout)
  {
  for (int i=0;i<TOTALTHREADS ;i++)
    if (RunBackground(i,NULL,timeout)==false) return false;
  return true;
  }

//--------------------------------------------------

bool CancelMessage(CNewsTerminal &term, CNewsArtHolder *hld, const char *groups)
  {  
  term.SendLine("POST");
  int err=term.ReceiveNumReply()/10;
  if (err!=34) 
   {term.ShowError(err);return false;}
  term.SendFormatted("From: %s",hld->from);
  term.SendFormatted("Control: cancel %s",hld->messid);
  term.SendFormatted("NewsGroups: %s",groups);
  term.SendFormatted("Subject: %s",hld->subject);
  term.SendLine("");
  term.SendLine("cancel");
  term.SendLine(".");
  err=term.ReceiveNumReply()/10;
  if (err!=24) 
    {term.ShowError(err);return false;}
  return true;
  }

#define MARKREADLOG "markread.rpt"

void LogMarkAsRead(const char *messageid, bool state, int section)
  {
/*  const char *pathname=theApp.FullPath(MARKREADLOG,true);
  FILE *f=fopen(pathname,"a+");  
  struct tm *newtime;
  time_t aclock;

  time( &aclock );                 
  newtime = localtime( &aclock );
  int msgidlen=strlen(messageid);
  char *msgshort=(char *)alloca(msgidlen+1);
  memcpy(msgshort,messageid+1,msgidlen-2);
  msgshort[msgidlen-2]=0;

  const char *statename;
  switch (section)
    {
    case 1:statename="User Mark As Read";break;
    case 2:statename="User Mark As Unread";break;
    case 3:statename="Mark by Timer";break;
    case 4:statename="Marked in Preview window";break;
    case 5:statename="Mark my sent articles";break;
    case 6:statename="LastRead-Smart";break;
    case 7:statename="LastRead";break;
    case 8:statename="Archive";break;
    case 9:statename="LastRead in sync";break;
    case 10:statename="User Catch Up";break;
    case 99:statename="Display";break;
    }

  const MSG *msg=CMainFrame::GetCurrentMessage();
  fprintf(f,"<< %s\tnews:%s %d (%s)\n"
    "\tMessage: 0x%X WPARAM 0x%X LPARAM 0x%d %s %s %s THREAD 0x%X Sync %s\n",asctime(newtime),msgshort,state,statename,
    msg->message,msg->wParam,msg->lParam,
    (GetKeyState(VK_SHIFT) & 0x80)?"SHIFT":"",
    (GetKeyState(VK_CONTROL) & 0x80)?"CTRL":"",
    (GetKeyState(VK_MENU) & 0x80)?"ALT":"",
    GetCurrentThreadId(),theApp.RunBackground(0,NULL,0)==false?"YES":"NO");
  fclose(f);*/
  }

void LogWindowMessage(UINT msg, WPARAM wParam, LPARAM lParam)
  {
  /*
  const char *msgname;
  switch (msg)
    {
    case WM_KEYDOWN:
      if (lParam & (1<<30)) return;
      msgname="WM_KEYDOWN"; break;
    case WM_KEYUP: msgname="WM_KEYUP"; break;
    case WM_LBUTTONDOWN: msgname="WM_LBUTTONDOWN";break;
    case WM_RBUTTONDOWN: msgname="WM_RBUTTONDOWN";break;
    case WM_LBUTTONUP: msgname="WM_LBUTTONUP";break;
    case WM_RBUTTONUP: msgname="WM_RBUTTONUP";break;
    case 0: if (wParam==1) msgname="!!Start synchronize";
            else if (wParam==2) msgname="!!Start small synchronize";
            else msgname="!!Stop synchronize";
            break;
    default: return;
    }
  const char *pathname=theApp.FullPath(MARKREADLOG,true);
  FILE *f=fopen(pathname,"a+");  
  struct tm *newtime;
  time_t aclock;

  time( &aclock );                 
  newtime = localtime( &aclock );
  fprintf(f,"+ %s\tMESSAGE: %s WPARAM %d(%x)[%d,%d] LPARAM %d(%x)[%d,%d]\n",
    asctime(newtime),msgname,wParam,wParam,LOWORD(wParam),HIWORD(wParam),
        lParam,lParam,LOWORD(lParam),HIWORD(lParam));
  fclose(f);
  */
  }

void CompactLogMarkAsRead()
  {
  /*
  const char *pathname=theApp.FullPath(MARKREADLOG,true);
  CFileFind fnd;
  if (fnd.FindFile(pathname))
    {
    fnd.FindNextFile();    
    if (fnd.GetLength()>1024*1024)
      {
      char *newname=strcpy((char *)alloca(strlen(pathname)+1),pathname);
      const char *bak=theApp.FullPath(MARKREADLOG".bag",true);
      DeleteFile(bak);
      MoveFile(newname,bak);
      }      
    }
   */
  }

