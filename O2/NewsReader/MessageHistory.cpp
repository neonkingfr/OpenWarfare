// MessageHistory.cpp: implementation of the CMessageHistory class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "newsreader.h"
#include "MessageHistory.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

unsigned long SMessHistoryItem::idcntr=0;

CMessageHistory::CMessageHistory()
  {
  pos=0;
  disable=false;
  blank=false;
  }

CMessageHistory::~CMessageHistory()
  {

  }

void CMessageHistory::AddToHistory(const CString &msgref)
  {
  if (disable)
	{
	disable=false;
	return;
	}
  if (list[pos].messid==msgref) return;
  pos++;
  if (pos>=CMSGHISTORYSIZE) pos-=CMSGHISTORYSIZE;
  list[pos]=SMessHistoryItem();
  list[pos].messid=msgref;
  TRACE2("History %d %s\n",pos,list[pos].messid);
  blank=false;
  }

bool CMessageHistory::CanBack()
  {
  int p=pos-1;
  if (p<0) p+=CMSGHISTORYSIZE;
  return (list[p].cntr<list[pos].cntr);
  }

bool CMessageHistory::CanForward()
  {
  int p=pos+1;
  if (p>=CMSGHISTORYSIZE) p-=CMSGHISTORYSIZE;
  return (list[p].cntr>list[pos].cntr);

  }

CString CMessageHistory::GetBack()
  {
  if (blank)
    {
    blank=false;
    return list[pos].messid;
    }
  int p=pos-1;
  if (p<0) p+=CMSGHISTORYSIZE;
  if (list[p].cntr<list[pos].cntr)
	{
	pos=p;
	return list[pos].messid;
	}
  return "";
  }

CString CMessageHistory::GetForward()
  {
  int p=pos+1;
  if (p>=CMSGHISTORYSIZE) p-=CMSGHISTORYSIZE;
  if (list[p].cntr>list[pos].cntr)
	{
	pos=p;
	return list[pos].messid;
	}
  return "";
  }

void CMessageHistory::BlankPage()
  {
  blank=true;
  }
