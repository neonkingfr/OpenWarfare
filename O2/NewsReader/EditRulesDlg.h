#if !defined(AFX_EDITRULESDLG_H__2DBF8A22_93B1_4A94_9675_DE4720351C76__INCLUDED_)
#define AFX_EDITRULESDLG_H__2DBF8A22_93B1_4A94_9675_DE4720351C76__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// EditRulesDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CEditRulesDlg dialog

class CEditRulesDlg : public CDialog
{
// Construction
public:
	CString rules;	
	CEditRulesDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CEditRulesDlg)
	enum { IDD = IDD_EDITCOND };
	CListBox	list;
	CButton	moveup;
	CButton	movedown;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CEditRulesDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	void BuildRules();

	// Generated message map functions
	//{{AFX_MSG(CEditRulesDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnMoveup();
	afx_msg void OnMovedown();
	afx_msg void OnNewrule();
	afx_msg void OnEdit();
	afx_msg void OnDblclkList();
	afx_msg void OnDelete();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_EDITRULESDLG_H__2DBF8A22_93B1_4A94_9675_DE4720351C76__INCLUDED_)
