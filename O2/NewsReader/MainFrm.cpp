// MainFrm.cpp : implementation of the CMainFrame class
//

#include "stdafx.h"
#include "NewsReader.h"
#include "ScanGroupsThread.h"
#include "MainFrm.h"
#include <direct.h>
#include "PathBrowser.h"
#include "SendDlg.h"
#include "LostFoundDlg.h"
#include "SelKwdSet.h"
#include "CloseThreadDlg.h"
#include "Splash.h"
#include "ContitionsDlg.h"
#include "EditRulesDlg.h"
#include "ThreadPropDlg.h"
#include "MsgBox.h"
#include "LastReadDlg.h"
#include <io.h>
#include "CompletteThreadDlg.h"
//#include "ArchiveParamFile.h"
#include "DlgViewSource.h"
#include "WinSharedMemory.h"
#include "AutoDeleteDlg.h"
#include "DlgEditRules.h"
using namespace std;



#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMainFrame

IMPLEMENT_DYNAMIC(CMainFrame, CFrameWnd)

BEGIN_MESSAGE_MAP(CMainFrame, CFrameWnd)
//{{AFX_MSG_MAP(CMainFrame)
ON_WM_CREATE()
ON_WM_SETFOCUS()
ON_COMMAND(ID_ACCOUNTS_NEW, OnAccountsNew)
ON_NOTIFY(NM_DBLCLK, 100, OnClickTree)
ON_NOTIFY(TVN_ITEMEXPANDING, 100, OnItemExpanding)
ON_NOTIFY(NM_RCLICK, 100, OnRClickTree)

ON_COMMAND_EX(ID_VIEW_FILTER,OnBarCheck)
ON_COMMAND_EX(ID_VIEW_HIGHLIGHT,OnBarCheck)
ON_COMMAND_EX(ID_VIEW_FASTFIND,OnBarCheck)
ON_UPDATE_COMMAND_UI(ID_VIEW_FILTER, OnUpdateControlBarMenu)
ON_UPDATE_COMMAND_UI(ID_VIEW_HIGHLIGHT, OnUpdateControlBarMenu)
ON_UPDATE_COMMAND_UI(ID_VIEW_FASTFIND, OnUpdateControlBarMenu)

ON_WM_CONTEXTMENU()
ON_UPDATE_COMMAND_UI(ID_ACCOUNTS_DELETE, OnUpdateAccounts)
ON_COMMAND(ID_ACCOUNTS_DELETE, OnAccountsDelete)
ON_COMMAND(ID_ACCOUNTS_EDIT, OnAccountsEdit)
ON_COMMAND(ID_ACCOUNTS_SIGNUP, OnAccountsSignup)
ON_WM_CLOSE()
ON_MESSAGE(NRM_FINDACCOUNT,OnFindAccount)

ON_MESSAGE(NRM_AUTOSIGNUP,OnAutoSignup)
ON_MESSAGE(NRM_GRPREADDONE,OnGroupUpdate)
ON_MESSAGE(NRM_SELECTITEM,OnSelectMessage)
ON_MESSAGE(NRM_SAVEACCSTATE,OnSaveAccState)
ON_MESSAGE(NRM_DOWNLOADCOMPLETTED,OnNrmDownloadComplette)
ON_MESSAGE(NRM_GOTOMESSAGE,OnGotoMessageShortcut)
ON_NOTIFY(TVN_SELCHANGED, 100, OnSelchangedTree)
ON_WM_TIMER()
ON_WM_SIZE()
ON_COMMAND(ID_VIEW_DEBUGCONSOLE, OnViewDebugconsole)
ON_UPDATE_COMMAND_UI(ID_VIEW_DEBUGCONSOLE, OnUpdateViewDebugconsole)
ON_COMMAND(ID_ACCOUNTS_SYNCHRONIZE, OnAccountsSynchronize)
ON_COMMAND(ID_ACCOUNTS_EXTENDEDSYNCHRONIZE,OnExtendedSynchronize)
ON_UPDATE_COMMAND_UI(ID_ACCOUNTS_SYNCHRONIZE, OnUpdateAccountsSynchronize)
ON_UPDATE_COMMAND_UI(ID_ACCOUNTS_DOWNLOADALLARTICLES, OnUpdateAccountsSynchronize)
ON_UPDATE_COMMAND_UI(ID_GROUPS_DOWNLOADALLARTICLES, OnUpdateAccountsSynchronize)
ON_UPDATE_COMMAND_UI(ID_ACCOUNTS_EXTENDEDSYNCHRONIZE, OnUpdateAccountsSynchronize)
ON_COMMAND(ID_ACCOUNTS_STOP, OnAccountsStop)
ON_COMMAND(ID_ACCOUNTS_UNLOADACCOUNT,OnAccountsUnload)
ON_UPDATE_COMMAND_UI(ID_ACCOUNTS_UNLOADACCOUNT, OnUpdateAccounts)	
ON_UPDATE_COMMAND_UI(ID_ACCOUNTS_STOP, OnUpdateAccountsStop)	
ON_UPDATE_COMMAND_UI(ID_ACCOUNTS_REPAIR, OnUpdateAccounts)	
ON_UPDATE_COMMAND_UI(ID_MESSAGE_SENDNEW, OnUpdateMessageSendnew)
ON_UPDATE_COMMAND_UI(ID_MESSAGE_REPLYTOSENDER, OnUpdateMessageReplyto)
ON_COMMAND(ID_FILE_CONFIGURESMTP, OnFileConfiguresmtp)
ON_COMMAND(ID_ACCOUNTS_REPAIR, OnAccountRepair)
ON_UPDATE_COMMAND_UI(ID_MESSAGE_SENDEMAIL, OnUpdateMessageSendemail)
ON_COMMAND(ID_VIEW_MESSAGEFILTER_DISPLAYALLMESSAGES, OnViewMessagefilterDisplayallmessages)
ON_UPDATE_COMMAND_UI(ID_VIEW_MESSAGEFILTER_DISPLAYALLMESSAGES, OnUpdateViewMessagefilterDisplayallmessages)
ON_COMMAND(ID_VIEW_MESSAGEFILTER_DISPLAYONLYIMPORTANT, OnViewMessagefilterDisplayonlyimportant)
ON_UPDATE_COMMAND_UI(ID_VIEW_MESSAGEFILTER_DISPLAYONLYIMPORTANT, OnUpdateViewMessagefilterDisplayonlyimportant)
ON_COMMAND(ID_VIEW_MESSAGEFILTER_HIDEHIDDEN, OnViewMessagefilterHidehidden)
ON_UPDATE_COMMAND_UI(ID_VIEW_MESSAGEFILTER_HIDEHIDDEN, OnUpdateViewMessagefilterHidehidden)
ON_COMMAND(ID_VIEW_GROUPSASTREE, OnViewGroupsastree)
ON_UPDATE_COMMAND_UI(ID_VIEW_GROUPSASTREE, OnUpdateViewGroupsastree)
ON_COMMAND_EX(ID_VIEW_SYNCHRONIZEBUTTON, OnBarCheck)
ON_UPDATE_COMMAND_UI(ID_VIEW_SYNCHRONIZEBUTTON, OnUpdateControlBarMenu)
ON_COMMAND(ID_GROUPS_CATCHUP, OnGroupsCatchup)
ON_COMMAND(ID_ACCOUNTS_CHECKFORNEWGROUPS, OnCheckfornewgroups)
ON_COMMAND(ID_MESSAGES_SELECTION_CANCELMESSAGES, OnMessagesSelectionCancelmessages)
ON_COMMAND(ID_GROUPS_UNSUBSCRIBE, OnGroupsUnsubscribe)
ON_COMMAND(ID_FILE_OPTIONS, OnFileOptions)
ON_COMMAND(ID_GROUPS_COMPACT, OnGroupsCompact)
ON_COMMAND(ID_FILE_EXPORT, OnFileExport)
ON_COMMAND(ID_FILE_IMPORT, OnFileImport)
ON_COMMAND(ID_MESSAGES_SELECTALL, OnMessagesSelectall)
ON_COMMAND(ID_LISTPOPUP_PROPERTIES, OnListpopupProperties)
ON_COMMAND(ID_LISTPOPUP_SHOWPARENT, OnListpopupShowparent)
ON_COMMAND(ID_FILE_PROXYOPTIONS,OnFileProxyoptions)
ON_COMMAND(ID_GROUPS_FIND, OnGroupsFind)
ON_COMMAND(ID_MESSAGES_CLOSETHREAD, OnMessagesClosethread)
ON_COMMAND(ID_HELP_CHECKVERSION, OnCheckVersion)
ON_UPDATE_COMMAND_UI(ID_HELP_CHECKVERSION, OnUpdateCheckVersion)
ON_WM_QUERYENDSESSION()
ON_COMMAND_RANGE(ID_MESSAGE_SENDNEW,ID_MESSAGE_SENDEMAIL,OnMessageSendnew)
ON_COMMAND_EX(ID_MESSAGES_CANCELTHREAD,OnMessageSendnewBool)
ON_UPDATE_COMMAND_UI(ID_MESSAGES_REREADFROMSERVER,OnUpdateMessagesSelection)
ON_UPDATE_COMMAND_UI(ID_MESSAGES_COPYSHORTCUT,OnUpdateMessagesSelection)
ON_UPDATE_COMMAND_UI_RANGE(ID_MESSAGES_SELECTION_MARKASREAD,ID_MESSAGES_SELECTION_CANCELMESSAGES,OnUpdateMessagesSelection)
ON_COMMAND_RANGE(ID_MESSAGES_SELECTION_MARKASREAD, ID_MESSAGES_SELECTION_UNDELETE  ,OnMessagesSelection)
ON_COMMAND_RANGE(ID_MESSAGES_MARKASIMPORTANT, ID_MESSAGES_DELETEMARK, OnMessagesMark)
ON_UPDATE_COMMAND_UI_RANGE(ID_GROUPS_CATCHUP, ID_GROUPS_FIND ,OnUpdateGroups)
ON_WM_ENDSESSION()
ON_COMMAND(ID_MESSAGES_REREADFROMSERVER, OnMessagesRereadfromserver)
ON_MESSAGE(NRM_GETSELMESSAGEREF,OnNrmGetselmessageref)

ON_COMMAND(ID_MESSAGES_BACK, OnMessagesBack)
ON_UPDATE_COMMAND_UI(ID_MESSAGES_BACK, OnUpdateMessagesBack)
ON_COMMAND(ID_MESSAGES_FORWARD, OnMessagesForward)
ON_UPDATE_COMMAND_UI(ID_MESSAGES_FORWARD, OnUpdateMessagesForward)
ON_COMMAND(ID_MESSAGES_SELECTION_DOWNLOAD, OnMessagesSelectionDownload)
ON_COMMAND(ID_MESSAGES_LOSTSAVED, OnMessagesLostsaved)
ON_COMMAND(ID_NEXT_WND, OnNextWnd)
ON_COMMAND(ID_PREV_WND, OnPrevWnd)
ON_COMMAND(ID_LISTPOPUP_ADDTONOTIFYLIST, OnMsgNtTest)
ON_COMMAND(ID_ACCOUNTS_CACHEMAINTANCE, OnCacheMaintance)
ON_UPDATE_COMMAND_UI(ID_ACCOUNTS_CACHEMAINTANCE, OnUpdateAccounts)
ON_UPDATE_COMMAND_UI(ID_MESSAGES_SELECTION_DOWNLOAD,OnUpdateMessagesSelection)
ON_UPDATE_COMMAND_UI(ID_MESSAGE_REPLYTOGROUP, OnUpdateMessageReplyto)
ON_UPDATE_COMMAND_UI(ID_ACCOUNTS_EDIT, OnUpdateAccounts)
ON_UPDATE_COMMAND_UI(ID_ACCOUNTS_SIGNUP, OnUpdateAccounts)
ON_UPDATE_COMMAND_UI(ID_FILE_EXPORT, OnUpdateAccounts)
ON_UPDATE_COMMAND_UI(ID_MESSAGES_CLOSETHREAD, OnUpdateMessageReplyto)
ON_COMMAND(ID_FILE_OFFLINE, OnFileOffline)
ON_UPDATE_COMMAND_UI(ID_FILE_OFFLINE, OnUpdateFileOffline)
ON_COMMAND(ID_GROUPS_FULLLOAD, OnFullLoad)
ON_UPDATE_COMMAND_UI(ID_GROUPS_FULLLOAD, OnUpdateFullLoad)
ON_COMMAND(ID_MESSAGES_GOTO,OnMessagesGoto)

ON_COMMAND(ID_ACCOUNTS_DOWNLOADALLARTICLES,OnAccountsDownladAllArticles)
ON_COMMAND(ID_GROUPS_DOWNLOADALLARTICLES,OnGroupsDownladAllArticles)


ON_COMMAND(ID_VIEW_KWDSET,OnViewKwdSet)

ON_COMMAND_EX(ID_HELP_DISPLAYVERSION,OnDisplayVersion)

ON_COMMAND(ID_MESSAGES_COPYSHORTCUT,OnCopyShortcut)
ON_COMMAND(ID_THREADS_NEWPROFILE,OnThreadsAdd)
ON_COMMAND(ID_THREADS_SETTHREADPROPERTIES,OnSetThreadProp)
ON_UPDATE_COMMAND_UI(ID_THREADS_SETTHREADPROPERTIES,OnUpdateMessagesSelection)
ON_UPDATE_COMMAND_UI(ID_THREADS_NOFILTERING,OnUpdateAccounts)
ON_UPDATE_COMMAND_UI(ID_THREADS_NOHIGHLITING,OnUpdateAccounts)
ON_UPDATE_COMMAND_UI(ID_THREADS_NEWPROFILE,OnUpdateAccounts)
ON_WM_INITMENUPOPUP( )
ON_COMMAND_RANGE(ID_THREADS,ID_THREADSEND,OnUseEditCond)
ON_COMMAND_RANGE(ID_THREADS_HIGHLIGHT,ID_THREADS_HIGHLIGHTEND,OnUseEditHighlight)
ON_COMMAND_EX(ID_THREADS_NOFILTERING,OnUseEditCondBool)
ON_COMMAND_EX(ID_THREADS_NOHIGHLITING,OnUseEditHighlightBool)


ON_COMMAND(ID_ACCOUNTS_SETLASTREAD,OnAccountSetLastRead)
ON_UPDATE_COMMAND_UI(ID_ACCOUNTS_SETLASTREAD,OnUpdateAccounts)
ON_COMMAND(ID_ACCOUNTS_MARKREAD_BEFOREMYLASTPOST,OnAccountSetLastReadSmart)
ON_UPDATE_COMMAND_UI(ID_ACCOUNTS_MARKREAD_BEFOREMYLASTPOST,OnUpdateAccounts)
ON_UPDATE_COMMAND_UI(ID_ACCOUNTS_CHECKFORNEWGROUPS, OnUpdateCheckfornewgroups)

ON_COMMAND(ID_GROUPS_NOTIFY,OnGroupsNotify)
ON_UPDATE_COMMAND_UI(ID_GROUPS_NOTIFY,OnUpdateGroupsNotify)
ON_COMMAND(ID_MESSAGES_NEXTNEW,OnMessagesNextNew)
ON_CBN_SELCHANGE( IDC_FILTER, OnFilterChange )
ON_COMMAND(ID_HOTKEY_FILTEROPEN,OnFilterOpen)	
ON_CBN_SELCHANGE( IDC_HIGHLIGHT, OnHighlightChange )
ON_COMMAND(ID_HOTKEY_HIGHLIGHTOPEN,OnHighlightOpen)	
ON_UPDATE_COMMAND_UI(ID_MESSAGES_SELECTION_SAVEMESSAGES,OnUpdateMessagesSelection)
ON_COMMAND(ID_MESSAGES_SELECTION_SAVEMESSAGES,OnSelectionSaveMessages)
ON_UPDATE_COMMAND_UI(ID_GROUPS_SUBSCRIBE,OnUpdateGroupsSubscribe)
ON_COMMAND(ID_GROUPS_SUBSCRIBE,OnGroupsSubscribe)

ON_COMMAND(ID_LISTPOPUP_EDITTEXT,OnListpopupEdittext)

ON_UPDATE_COMMAND_UI(ID_LISTPOPUP_EDITTEXT,OnUpdateMessagesSelection)
ON_COMMAND(ID_THREADS_COMPLETTE,OnCompletteThread)
ON_UPDATE_COMMAND_UI(ID_THREADS_COMPLETTE,OnUpdateMessagesSelection)
	ON_WM_ACTIVATE()
ON_COMMAND(ID_FILE_EXPORTACCOUNT_PARAMFILE,OnExportParamFile)
ON_COMMAND(ID_VIEW_MESSAGESOURCE,OnViewMessageSource)
ON_UPDATE_COMMAND_UI(ID_VIEW_MESSAGESOURCE,OnUpdateMessageReplyto)
ON_COMMAND(ID_VIEW_DELETEDMESSAGE,OnViewDeletedMessages)
ON_UPDATE_COMMAND_UI(ID_VIEW_DELETEDMESSAGE,OnUpdateViewDeletedMessages)
ON_COMMAND(ID_MESSAGES_REPLYTOKECAL,OnMessagesReplyToKecal)
ON_UPDATE_COMMAND_UI(ID_MESSAGES_REPLYTOKECAL,OnUpdateMessagesReplyToKecal)
ON_COMMAND(IDC_QFSEARCHNOW,OnQFFindNow)
ON_COMMAND(IDC_QFSTOP,OnQFStop)
ON_UPDATE_COMMAND_UI(IDC_QFSEARCHNOW,OnUpdateQFFindNow)
ON_UPDATE_COMMAND_UI(IDC_QFSTOP,OnUpdateQFStop)
ON_COMMAND(IDC_ENTERPRESS,EnterPress)
ON_COMMAND(ID_HOTKEY_FINDTOOL,OnHotkeyF7)
ON_COMMAND(IDC_QFCANCEL,OnQFCancel)
ON_UPDATE_COMMAND_UI(IDC_QFCANCEL,OnUpdateQFCancel)
ON_COMMAND(ID_MESSAGES_GOTOCONTEXT,OnGotoContext)
ON_UPDATE_COMMAND_UI(ID_MESSAGES_GOTOCONTEXT,OnUpdateGotoContext)
ON_COMMAND(ID_VIEW_MESSAGEFILTER_MYTASKS,OnFilterMyTasks)
ON_UPDATE_COMMAND_UI(ID_VIEW_MESSAGEFILTER_MYTASKS,OnUpdateAccounts)
ON_COMMAND(ID_MESSAGES_FLUSHMEMORYCACHE, OnFlushMemoryCache)
ON_COMMAND(ID_ACCOUNTS_AUTODELETETHREADS, OnAutodeleteThreads)
ON_UPDATE_COMMAND_UI(ID_ACCOUNTS_AUTODELETETHREADS,OnUpdateAccounts)
ON_COMMAND_EX(ID_GROUPS_FINDSIMILAR,OnGroupsFindSimilar)
ON_COMMAND_EX(ID_GROUPS_FINDSIMILAR2,OnGroupsFindSimilar)
ON_UPDATE_COMMAND_UI(ID_GROUPS_FINDSIMILAR,OnUpdateMessagesSelection)

	//}}AFX_MSG_MAP
  ON_COMMAND(ID_MESSAGES_OPENRELATEDURL, OnMessagesOpenrelatedurl)
  ON_UPDATE_COMMAND_UI(ID_MESSAGES_OPENRELATEDURL, OnUpdateMessagesOpenrelatedurl)
  END_MESSAGE_MAP()


#define AFTER_CLICK_TIME_WAIT 1000


static UINT indicators[] =
  {
  ID_SEPARATOR,           // status line indicator
    ID_INDICATOR_CAPS,
    ID_INDICATOR_NUM,
    ID_INDICATOR_SCRL,
  };

//--------------------------------------------------

/////////////////////////////////////////////////////////////////////////////
// CMainFrame construction/destruction

CMainFrame::CMainFrame():acclist(lefttree),meslist(&notify)
  {
  lastcheck=NULL;
  // TODO: add member initialization code here
  checkelapse=10;	
  }

//--------------------------------------------------

static bool incleanup;
class CNRCleanUp
  {
  bool &flag;
  bool ok;
  public:
    CNRCleanUp(bool &f):flag(f) 
      {ok=!flag;flag=true;}
    ~CNRCleanUp() 
      {if (ok) flag=false;}
    bool operator!() 
      {return !ok;}
  };

//--------------------------------------------------

CMainFrame::~CMainFrame()
  {
  wFilters.Detach();
  wHighlights.Detach();
  }

//--------------------------------------------------

int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct)
  {    
  if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
    return -1;
  CreateWindow("STATIC",NREADERWINDOW,0,0,0,100,100,*this,NULL,AfxGetInstanceHandle(),NULL);
  CompactLogMarkAsRead();
  // create a view to occupy the client area of the frame
  if (theApp.config.nrnotify) notify.Create(IDR_MAINFRAME,IDI_NOTIFY,IDI_NOTIFYSTOP,this);
  SetIcon(theApp.LoadIcon(IDR_MAINFRAME),FALSE);
  SetIcon(theApp.LoadIcon(IDR_MAINFRAME),TRUE);
//  ShowWindow(SW_HIDE);	
  if (LoadToolbars(theApp.config.bigicons!=FALSE)==false) return -1;
  splitw.Create(this,0.2f,AFX_IDW_PANE_FIRST);
  splith2.Create(&splitw,100);
  splith2.EnableResize2(false);
  splith2.EnableResize1(true);
  splith2.EnableChangePos(false);
  CRect rc;
  splith2.GetClientRect(&rc);
  splith2.SetSplitPos(rc.bottom-50);
  lefttree.Create(TVS_HASLINES|TVS_LINESATROOT|TVS_HASBUTTONS|TVS_SHOWSELALWAYS|
    TVS_DISABLEDRAGDROP|WS_VISIBLE|WS_TABSTOP|WS_CHILD|WS_BORDER,CRect(0,0,0,0),&splith2,100);	
  lefttree.SetImageList(&theApp.ilist,TVSIL_NORMAL);
  dogwnd.Create(&splith2,CRect(0,0,0,0));
  DogSetStatusWindow(&dogwnd);
  splith.Create(&splitw,0.5f);
  splith.EnableResize2(true);
  splith.EnableResize1(true);
  meslist.Create(&splith);	
  msgtext.Create(AfxRegisterWndClass(0),"",WS_CHILD|WS_VISIBLE,CRect(0,0,0,0),&splith,AFX_IDW_PANE_FIRST);
  SetWindowLong(msgtext,GWL_EXSTYLE,GetWindowLong(msgtext,GWL_EXSTYLE) | WS_EX_CLIENTEDGE);
  //	msgtext.Create("",WS_CHILD|WS_VISIBLE,CRect(0,0,0,0),&splith,0);
  msgtext.ResNavigate("blank.htm");
//  msgtext.Navigate2(_T("http://vyletnik.jinak.cz"));
  SetWindowLong(meslist,GWL_EXSTYLE,GetWindowLong(meslist,GWL_EXSTYLE)|WS_EX_CLIENTEDGE);
  SetWindowLong(lefttree,GWL_EXSTYLE,GetWindowLong(lefttree,GWL_EXSTYLE)|WS_EX_CLIENTEDGE);
  //	SetWindowLong(msgtext,GWL_EXSTYLE,GetWindowLong(msgtext,GWL_EXSTYLE)|WS_EX_CLIENTEDGE);
  meslist.SetBrowser(&msgtext);
  //	msgtext.SetResizable(true);	
  acclist.InitList();
  lastenum=NULL;
  checktimer=SetTimer(CHECKTIMER,CHECKTIMER,NULL);
  checktiming=theApp.config.syncmin;
  acclist.SerializeList(false);
  LoadWndState();
  splitw.RecalcLayout();
  theApp.RunBackground(0,new CAccountCompactThread(&acclist),INFINITE);
  OnDisplayVersion(0);
  if (_access(theApp.FullPath(NEWINSTNAME,true),0)==0)
    {
    char *old=strdup(theApp.FullPath(INSTALLNAME,true));
    char *nw=strdup(theApp.FullPath(NEWINSTNAME,true));
    for (int i=0;i<100;i++)
      {
      DeleteFile(old);
      if (MoveFile(nw,old)==TRUE) break;
      Sleep(10);
      }
    delete old;
    }
#ifndef DEMO	
//  theApp.CheckForNewVersion(FALSE);
#else
  NrMessageBox("This is a DEMO version.\n  Some features are disabled.\n  You cannot send articles.\n  Only one account can be created.");
#endif
  if (__argc>1) 
  {
    CString message;
    if (GotoNewsShortcut(__argv[1],&message)==false)
    {
        CString msg;
        AfxFormatString1(msg,IDS_ARTICLEHASNOTBEENFOUND,message);
            NrMessageBox(msg,MB_OK);
    }
  }
  PostMessage(WM_KEYDOWN,VK_ESCAPE);
  CString kbdl=theApp.GetProfileString("Keyboard","SavedLayout");
  if (kbdl.GetLength()) LoadKeyboardLayout(kbdl,KLF_ACTIVATE|KLF_SETFORPROCESS);
  CMessagePartCache::InitCache();
  return TRUE;
  }

//--------------------------------------------------

BOOL CMainFrame::PreCreateWindow(CREATESTRUCT& cs)
  {
  if( !CFrameWnd::PreCreateWindow(cs) )
    return FALSE;
  // TODO: Modify the Window class or styles here by modifying
  //  the CREATESTRUCT cs
  
  cs.dwExStyle &= ~WS_EX_CLIENTEDGE;
  cs.lpszClass = AfxRegisterWndClass(0);
  return TRUE;
  }

//--------------------------------------------------

/////////////////////////////////////////////////////////////////////////////
// CMainFrame diagnostics

#ifdef _DEBUG
void CMainFrame::AssertValid() const
  {
  CFrameWnd::AssertValid();
  }

//--------------------------------------------------

void CMainFrame::Dump(CDumpContext& dc) const
  {
  CFrameWnd::Dump(dc);
  }

//--------------------------------------------------

#endif //_DEBUG

/////////////////////////////////////////////////////////////////////////////
// CMainFrame message handlers
void CMainFrame::OnSetFocus(CWnd* pOldWnd)
  {
  // forward focus to the view window
  }

//--------------------------------------------------

BOOL CMainFrame::OnCmdMsg(UINT nID, int nCode, void* pExtra, AFX_CMDHANDLERINFO* pHandlerInfo)
  {
  // let the view have first crack at the command
  if (splitw.OnCmdMsg(nID, nCode, pExtra, pHandlerInfo))
    return TRUE;
  
  // otherwise, do default handling
  return CFrameWnd::OnCmdMsg(nID, nCode, pExtra, pHandlerInfo);
  }

//--------------------------------------------------

#include "EditAccountDlg.h"

#ifdef DEMO  
void CMainFrame::OnAccountsNew() 
  {
  HTREEITEM it=lefttree.GetRootItem();
  while (acclist.GetAccount(it)==NULL && it!=NULL) it=lefttree.GetNextItem(it,TVGN_NEXT);
  if (it==NULL)
    {
    BOOL sock,smtp;
    sock=FALSE;
    smtp=theApp.sendopt.server=="(none)";
    CNewsAccount *acc;
    acc=new CNewsAccount();
    if (acc->CreateByWizard("","",sock,smtp)==false)
      delete acc;
    else
      {
      acclist.InsertAccount(acc);
      if (sock) 
        {
        theApp.proxy.DoModal();
        theApp.proxy.SaveConfig();
        }
      if (smtp) 
        {
        theApp.sendopt.DoModal();
        theApp.sendopt.SaveConfig();
        }
      }
    }
  else DemoWarn();	
  }

//--------------------------------------------------

#else
void CMainFrame::OnAccountsNew() 
  {
  CNewsAccount *act=acclist.GetAccountEx(lefttree.GetSelectedItem());
  if (act==NULL)
    act=acclist.GetAccountEx(lefttree.GetRootItem());
  const char *name;
  const char *email;
  if (act)
    {
    name=act->GetMyName();
    email=act->GetMyEmail();
    }
  else
    {
    name="";
    email="";
    }
  BOOL sock,smtp;
  sock=FALSE;
  smtp=theApp.sendopt.server=="(none)";
  CNewsAccount *acc;
  acc=new CNewsAccount();
  if (acc->CreateByWizard(name,email,sock,smtp)==false)
    delete acc;
  else
    {
    acclist.InsertAccount(acc);
    if (sock) 
      {
      theApp.proxy.DoModal();
      theApp.proxy.SaveConfig();
      }
    if (smtp) 
      {
      theApp.sendopt.DoModal();
      theApp.sendopt.SaveConfig();
      }
    }
  }

//--------------------------------------------------

#endif


void CMainFrame::OnClickTree(NMHDR* pNMHDR, LRESULT* pResult)
  {
    CPoint pos=GetMessagePos();
    UINT flags;
    HTREEITEM itm=lefttree.HitTest(pos,&flags);
    if (itm==0 || (flags & TVHT_ONITEMLABEL)==0) return;

  NMTREEVIEW * pNMTreeView = (NM_TREEVIEW*)pNMHDR;
  HTREEITEM it=lefttree.GetSelectedItem();
  CNewsAccount *acc=acclist.GetAccountEx(it);
  CNewsGroup *grp=acclist.GetGroup(it);
  if (acc==NULL && grp==NULL)
    {
    OnAccountsNew();
    }
  else	if (grp==NULL)
    {
    if (!acc->IsLoaded())
      {
      acclist.EnsureLoaded(acc);
      }
    else
      if (acc->EditInDialog()==true) 
        {
        acclist.AlterAccount(it);	
        CheckNewGroups(true,true);
        }
    }
  else 
    OnAccountsSignup();
  *pResult=-1;
  }

//--------------------------------------------------

void CMainFrame::OnRClickTree(NMHDR* pNMHDR, LRESULT* pResult)
  {
  NMTREEVIEW * pNMTreeView = (NM_TREEVIEW*)pNMHDR;
  UINT flags;
  DWORD ww=::GetMessagePos();
  CPoint pt(ww);
  lefttree.ScreenToClient(&pt);
  HTREEITEM it=lefttree.HitTest(pt,&flags);
  lefttree.SelectItem(it);
  *pResult=0;
  }

//--------------------------------------------------

void CMainFrame::OnContextMenu(CWnd* pWnd, CPoint point) 
  {
  if (pWnd==&lefttree)
    {
    HTREEITEM it=lefttree.GetSelectedItem();	
    ItemType itm=acclist.GetItemType(it);	
    CMenu menu;
    menu.LoadMenu(IDR_MAINFRAME);
    CMenu *sub;
    if (itm==Itm_Account)sub=menu.GetSubMenu(1);
    else sub=menu.GetSubMenu(2);
    sub->TrackPopupMenu(TPM_LEFTALIGN|TPM_LEFTBUTTON|TPM_RIGHTBUTTON,point.x,point.y,this,NULL);
    }
  if (pWnd==&meslist)
    {
    CMenu menu;
    menu.LoadMenu(IDR_POPUPS);
    CMenu *sub=menu.GetSubMenu(0);
    sub->TrackPopupMenu(TPM_LEFTALIGN|TPM_LEFTBUTTON|TPM_RIGHTBUTTON ,point.x-200,point.y,this,NULL);
    }
  }

//--------------------------------------------------

void CMainFrame::OnUpdateAccounts(CCmdUI* pCmdUI) 
  {
  HTREEITEM it=lefttree.GetSelectedItem();
  CNewsAccount *acc=acclist.GetAccountEx(it);
  pCmdUI->Enable(acc!=NULL);
  
  }

//--------------------------------------------------

static UINT DeleteAccountThread(LPVOID pacc)
{
  CNewsAccount *acc=(CNewsAccount *)pacc; 
  CString name=acc->GetAccName();
  ::DogShowStatusV(IDS_DELETINGACCOUNT,name);
  acc->DeleteAccount();
  ::DogShowStatusV(IDS_DELETINGACCOUNTDONE,name);
  return 0;
}

void CMainFrame::OnAccountsDelete() 
  {
  HTREEITEM it=lefttree.GetSelectedItem();
  if (it!=NULL) 
    if (NrMessageBox(IDS_REMOVEACCOUNTASK,MB_YESNO)==IDYES)
      {
      meslist.LoadList(NULL,NULL);
      CNewsAccount *acc=acclist.GetAccountEx(it);
      acc->Lock();
      acclist.DeleteAccount(it);
      AfxBeginThread((AFX_THREADPROC)DeleteAccountThread,acc);
      }
  }

//--------------------------------------------------

void CMainFrame::OnAccountsEdit() 
  {
  HTREEITEM it=lefttree.GetSelectedItem();
  it=acclist.GetAccountItem(it);
  CNewsAccount *acc=acclist.GetAccount(it);
  if (acc!=NULL)
    {
    if (acclist.EnsureLoaded(acc)==true)
    if (acc->EditInDialog()==true) 
      {
      acclist.AlterAccount(it);	
      CheckNewGroups(true,true);
      }
    }
  
  }

//--------------------------------------------------

#include "GroupDlg.h"

void CMainFrame::OnAccountsSignup() 
  {
  HTREEITEM it=lefttree.GetSelectedItem();
  while (lefttree.GetNextItem(it,TVGN_PARENT)) it=lefttree.GetNextItem(it,TVGN_PARENT);
  CNewsAccount *acc=acclist.GetAccountEx(it);
  if (acclist.EnsureLoaded(acc)==true)
  if (acc!=NULL)
    {
    grpdlg.curacc=acc;
    grpdlg.DoModal();	
    acclist.AlterAccount(it);
    CheckNewGroups(true,true);
    }
  }

//--------------------------------------------------

void CMainFrame::OnClose() 
  {
  CNRCleanUp clr(incleanup);
  if (!clr) return;
  if (NrKillWindows()==false) return;
  CleanUp();
  CFrameWnd::OnClose();
  }

//--------------------------------------------------

LRESULT CMainFrame::OnAutoSignup(WPARAM wParam, LPARAM lParam)
  {
  CNewsAccount *acc=(CNewsAccount *)lParam;
  if (!acc->IsNotifySuppressed() || acc->IsDownloadForced())
    {
    acc->SetForceDownload(false);
    CString s;
    AfxFormatString1(s,IDS_NEWGROUPS,acc->GetAccName());
    if (NrMessageBox(s,MB_YESNO|MB_ICONQUESTION|MB_TASKMODAL)==IDYES)
      {
      if (grpdlg.m_hWnd==NULL)
        {
        grpdlg.curacc=acc;
        grpdlg.DoModal();
        acclist.AlterAccount(acclist.FindAccount(acc->GetAddress()));
        CheckNewGroups(true,true);
        }
      }
    }
  acc->Release();
  return 0;
  }

//--------------------------------------------------

void CMainFrame::OnSelchangedTree(NMHDR* pNMHDR, LRESULT* pResult) 
  {  
  CheckNewGroups(false,true);
  //  if (notify.IsEnabled()) notify.Reset();
  }

//--------------------------------------------------

void CMainFrame::AlterGroup(CNewsGroup *grp)
  {
  acclist.AlterGroup(grp);
  }

//--------------------------------------------------

void CMainFrame::CheckNewGroups(bool force, bool sauto)
  {
  CNewsAccount *nw;
  bool sort=true;
  HTREEITEM it=lefttree.GetSelectedItem();
  HTREEITEM gr=it;
  while (acclist.GetItemType(it)!=Itm_Account && it!=NULL) 
    it=lefttree.GetNextItem(it,TVGN_PARENT);	
  nw=acclist.GetAccount(it);  
  if (nw!=NULL && (!sauto || (theApp.config.async && !theApp.offline)) && nw->IsLoaded())
    {
    if (nw!=lastcheck || force)
      {
      SetCursor(::LoadCursor(NULL,IDC_WAIT));
      if (force) CNewsTerminal::StopTrafic();
      CScanGroupsThreadEx *thr=new CScanGroupsThreadEx(nw,nw->CheckDel());
      if (notify.IsEnabled()) thr->nwnotify=notify;
      if (theApp.RunBackground(0,thr,force?5000:1)==FALSE) delete thr; 	 
      lastcheck=nw;
      sort=false;
      }
    }
  if (nw)
    {
    if (nw!=lastcur)
      {
      LoadFiltersToCombo(nw);
      lastcur=nw;
      }
    CNewsGroup *grp=acclist.GetGroup(gr);
    if (grp) grp->SortIfNeeded();
    meslist.LoadList(nw,grp);
    if (grp && grp!=chkgroup && !theApp.offline && grp->sign)
      {
      CScanNewerOlderThread *thr=new CScanNewerOlderThread(nw,grp,grp->delcheck);
      if (theApp.RunBackground(0,thr,0)==FALSE) delete thr; else chkgroup=grp;
      }
    }
  }

//--------------------------------------------------

void CMainFrame::OnTimer(UINT nIDEvent) 
  {
  watchdogalive=TRUE;
  if (nIDEvent==dogtimer)
    if (runflag)
      {
      dogwnd.PlayAnim(CNewsTerminal::IsStopped()?3:0);
      /*	  CWnd *mm;
      mm=&meslist;	 
      dogcntr=(dogcntr+1)%DOGFRAMES;
      CBitmap &cur=dog[dogcntr];
      BITMAP bb;
      cur.GetBitmap(&bb);
      CRect rc;
      mm->GetClientRect(&rc);
      CDC *dc=mm->GetDC();
      CDC mem;
      mem.CreateCompatibleDC(dc);
      CBitmap *old=mem.SelectObject(&cur);
      dc->BitBlt(rc.right-bb.bmWidth,rc.bottom-bb.bmHeight,bb.bmWidth,bb.bmHeight,&mem,0,0,SRCCOPY);
      mem.SelectObject(old);
      mm->ReleaseDC(dc);*/
      cleardog=1;
      runflag=false;
      }
    else
      if ((theApp.RunBackground(0,NULL,0)==true && theApp.RunBackground(1,NULL,0)==true && theApp.RunBackground(2,NULL,0)==true )&& cleardog) 
        {dogwnd.PlayAnim(2);cleardog=false;dogwnd.ResetProgress();}
      if (nIDEvent==checktimer )
        {
        if (theApp.config.msync && !theApp.offline && --checktiming==0)
          {
          HTREEITEM nw=acclist.EnumAccounts(lastenum);		
          if (nw==NULL) nw=acclist.EnumAccounts(nw);
opak:
          if (nw!=NULL)
            {
            CNewsAccount *acc=acclist.GetAccount(nw);
            if (acc->IsLoaded())
              {
              CScanGroupsThreadEx *thr=new CScanGroupsThreadEx(acc,acc->CheckDel());
              if (notify) thr->nwnotify=notify;
              if (theApp.RunBackground(0,thr,1)==FALSE) delete thr; 	 
              else 
				{
				lastenum=nw;
				meslist.selmark=false;
				}
              checktiming=theApp.config.syncmin;
              }
            else
              {
              do 
                {
                nw=acclist.EnumAccounts(nw);
                if (nw==NULL) nw=acclist.EnumAccounts(nw);
                CNewsAccount *acc=acclist.GetAccount(nw);
                if (acc->IsLoaded()) goto opak;
                }
                while (nw!=lastenum) ;
                checktiming=theApp.config.syncmin;			
              }
            }
          else 
            {
            checktiming=theApp.config.syncmin;
            lastenum=nw;
            }
          }
        if (meslist.selmark && GetForegroundWindow()!=this)
          {
          CSaveNewsThread *thr=new CSaveNewsThread(&acclist);
          if (theApp.RunBackground(0,thr,1)==FALSE) delete thr;
		  else
			{
			SaveWndState();
			meslist.selmark=false;
			}
          }
        }
      CFrameWnd::OnTimer(nIDEvent);
      /*	switch (msgtext.GetAction())
      {
      case WaEmail:
      {
      CSendDlg *dlg=new CSendDlg();
      if (meslist.GetCurAccount()!=NULL)
      {
      dlg->from=meslist.GetCurAccount()->GetMyEmail();
      dlg->to=msgtext.GetActionData();
      dlg->Create();		  
      }
      }
      break;
      case WaBrowseNews:
      {
      CString nw="<";
      nw+=(LPCTSTR)msgtext.GetActionData();
      nw+=">";
      GotoMessageId(nw);
      break;
      }
      case WaReloadMsg:msgtext.GetToNotify()->PostMessage(WM_APP,0,0);
      break;
      case WaKillLong:
      {
      CNewsArtHolder *hld=meslist.GetCurMessage();
      if (hld) hld->KillLgWarn();
      msgtext.GetToNotify()->PostMessage(WM_APP,0,0);
      break;
      }
      }*/
  }
  
  //--------------------------------------------------
  
  void CMainFrame::OnViewDebugconsole() 
    {
    if (CNewsTerminal::DebugOpened())
      CNewsTerminal::CloseDebug();
    else
      CNewsTerminal::OpenDebug();
    }
  
  //--------------------------------------------------
  
  void CMainFrame::OnUpdateViewDebugconsole(CCmdUI* pCmdUI) 
    {
    pCmdUI->SetCheck(CNewsTerminal::DebugOpened()==true);  
    }
  
  //--------------------------------------------------
  
  LRESULT CMainFrame::OnGroupUpdate(WPARAM wParam, LPARAM lParam)
    {
    if (wParam==0 && lParam==0) 
      {
      meslist.UpdateList();
      }
    else
      {
      HTREEITEM ll=(HTREEITEM)lParam;
      acclist.AlterGroup(ll);
      CNewsGroup *gp=acclist.GetGroup(ll);
      if (gp==meslist.GetCurGroup() && wParam==0)	
        meslist.UpdateList();
      }
    return 0;
    }
  
  //--------------------------------------------------
  
  void CMainFrame::OnAccountsSynchronize() 
    {
    if (theApp.OfflineWarn()) 
      {
      CFileFind fnd;
      if (fnd.FindFile(theApp.FullPath("send*.msg"))==TRUE)
        {
        SetCursor(LoadCursor(NULL,IDC_WAIT));
        BOOL nx;
        do
          {
          nx=fnd.FindNextFile();
          CSendDlg *snd=new CSendDlg();
          if (snd->OutboxSend(fnd.GetFilePath())==false) 
            {delete snd;break;}
          delete snd;
          }
          while (nx);
        }		
      if (acclist.EnsureLoaded(lefttree.GetSelectedItem())==true)
      CheckNewGroups(true,false);  
      }
    }
  
  //--------------------------------------------------
  
  void CMainFrame::OnUpdateAccountsSynchronize(CCmdUI* pCmdUI) 
    {
    CNewsAccount *acc=acclist.GetAccountEx(lefttree.GetSelectedItem());
    pCmdUI->Enable(!cleardog && acc);
    }
  
  //--------------------------------------------------
  
  void CMainFrame::OnAccountsStop() 
    {
	CNewsTerminal::StopTrafic();
    dogwnd.PlayAnim(3);
    dogwnd.SetStatus("");
    }
  
  //--------------------------------------------------
  
  void CMainFrame::OnUpdateAccountsStop(CCmdUI* pCmdUI) 
    {
    pCmdUI->Enable(cleardog);	
    }
  
  //--------------------------------------------------
  
  void AddReSubject(const char *subject, CString& out)
    {
    char p[4];
    strncpy(p,subject,3);
    p[3]=0;
    if (stricmp(p,"Re:")!=0) 	
      out="Re: ";	
    else
      out="";
    out+=subject;
    }
  
static void RemoveHtml(char *text)
{
  char *beg=const_cast<char *>(stristr(text,"<body"));
  if (beg!=0) beg=strchr(beg,'>');
  if (beg!=0) beg++;else beg=text;
  char *end=const_cast<char *>(stristr(text,"</body>"));
  if (end!=0) *end=0;
  char *src=beg;
  char *trg=text;
  bool intag=false;
  bool space=true;
  while (*src)
  {
    if (!intag && (strnicmp(src,"<br>",4)==0 || strnicmp(src,"<br ",4)==0 || strnicmp(src,"<tr>",4)==0 || strnicmp(src,"<tr ",4)==0 ||strnicmp(src,"</table>",8)==0))
    {
      *trg++='\r';
      *trg++='\n';
      src+=2;
      intag=true;
      space=true;
    }
    if (!intag && (strnicmp(src,"<td>",4)==0 || strnicmp(src,"<td ",4)==0 || strnicmp(src,"</tr>",5)==0))
    {
      *trg++='\t';
      *trg++='|';
      *trg++=' ';
      src+=2;
      intag=true;
      space=true;
    }
    else if (!intag && (strnicmp(src,"<hr>",4)==0 || strnicmp(src,"<hr ",4)==0))
    {
      src+=2;
      *trg++='\r';
      *trg++='\n';
      for (int i=0;i<50 && trg+2<src;i++) *trg++='-';
      *trg++='\r';
      *trg++='\n';
      space=true;
      intag=true;
    }
    else if (!intag && (strnicmp(src,"<p>",3)==0 || strnicmp(src,"<p ",3)==0 || (strnicmp(src,"<h",2)==0 && isdigit(src[2])) || (strnicmp(src,"</h",3)==0 && isdigit(src[3]))))
    {
      src+=1;
      *trg++='\r';
      *trg++='\n';
      if (src+1>trg) 
      {
        *trg++='\r';
        *trg++='\n';
      }
      intag=true;
      space=true;
    }    
    else if (*src=='<') intag=true;
    else if (*src=='>' && intag) intag=false;
    else if (!intag) 
      if (isspace(*src) && *src>0) {if (!space) {*trg++=32;space=true;}}
      else {*trg++=*src;    space=false;}
    
    src++;
  }
  *trg=0;
}

  //--------------------------------------------------
  
#include "mapisend.h"
  
  bool CMainFrame::CallSend(int ID)
    {
    CNewsArtHolder *hld=meslist.GetCurMessage();
    CNewsAccount *acc=acclist.GetAccountEx(lefttree.GetSelectedItem());
    if (acc==NULL) return false;
    CSendDlg *dlg=new CSendDlg();
    dlg->from=CString("\"")+acc->GetMyName()+"\" <"+acc->GetMyEmail()+">";
    if (ID==ID_MESSAGE_REPLYTOSENDER && hld!=NULL)	
      dlg->to=hld->from;	
    else	if (ID!=ID_MESSAGE_SENDEMAIL)
      {
      CNewsGroup *grp=meslist.GetCurGroup();
      if (grp)
        {
        if (grp->special)
          {
          grp=acclist.GetGroup(lefttree.GetSelectedItem());
          if (grp!=NULL)
            if (grp->special) grp=NULL;
            else
              {
              CNewsArtHolder *hlda=grp->GetList()->FindMessage(hld->messid);
              if (hlda==NULL) grp=NULL;
              }
          if (grp==NULL)
            {
            CNewsArtHolder *hlda=NULL;
            grp=acc->GetGroups();
            while (grp && hlda==NULL)
              {
              if (!grp->special) hlda=grp->GetList()->FindMessage(hld->messid);
              if (hlda==NULL) grp=grp->GetNext();
              }
            }
          if (grp==NULL)
            return false;
          }
        dlg->SetNewsTarget(meslist.GetCurAccount(),grp);
        }
      }
    if (ID==ID_MESSAGE_REPLYTOGROUP || ID==ID_MESSAGE_REPLYTOSENDER)  
      {
      AddReSubject(hld->GetSubject(),dlg->subject);
      dlg->thrdprop=hld->GetThreadProperties(false);
      PMessagePart msgtext=hld->GetArticle();
      CMessagePart *part=msgtext;
      if (part==NULL)
        {
        hld->LoadFromCache(acc->cache);
        msgtext=hld->GetArticle();
        part=msgtext;
        if (part==NULL)
          {
          if (NrMessageBox(IDS_NOARTICLETEXT,MB_OKCANCEL)==IDCANCEL)
            {
            delete dlg;
            return false;
            }
          }
        }	
      dlg->msgbody="";	
      if (strncmp(hld->content,"multipart",9))
        while (part) 
          {
          if (part->GetType()==CMessagePart::text)
            if (strncmp(hld->content,"text/html",9))
              dlg->msgbody+=CString(part->GetData());
            else
            {
              CString text=part->GetData();
              RemoveHtml(text.LockBuffer());
              text.UnlockBuffer();
              dlg->msgbody+=text;
            }

          part=part->GetNext();
          }
        if (hld!=NULL && ID==ID_MESSAGE_REPLYTOGROUP) dlg->ref=hld->messid;
        if (ID==ID_MESSAGE_REPLYTOSENDER)
          dlg->msgbody="( news:"+hld->messid.Mid(1,hld->messid.GetLength()-2)+" )\r\n\r\n"+dlg->msgbody;
      }  
    if (ID==ID_MESSAGES_CANCELTHREAD)
      {	
      CCloseThreadDlg	pp;
      pp.subjectwarning=false;	
      if (pp.DoModal()==IDCANCEL)
        {
        delete dlg;
        return false;
        }
      dlg->subject=CLOSETHREADSUBJECT+pp.kwrds;
      if (hld!=NULL) dlg->ref=hld->messid;
      }
    if (theApp.config.useoe && (ID==ID_MESSAGE_REPLYTOSENDER || ID==ID_MESSAGE_SENDEMAIL))
      {    
      CString to;
      CString text;
      if (dlg->msgbody!="") 
        {
        text.LoadString(IDS_ORIGINALMESSAGE);
        text+=dlg->msgbody;
        }
      if (hld) GetEmailFromSender(hld->from,to);
      SetCursor(::LoadCursor(NULL,IDC_WAIT));
      if (DoSendMail(acc->GetMyEmail(),to,dlg->subject,text)!=FALSE)
        {
        delete dlg;
        return false;
        }
      else
        AfxMessageBox(IDS_MAPIERROR);
      }
    dlg->Create();
    if (ID==ID_MESSAGES_CANCELTHREAD) 
      {
      dlg->PostMessage(WM_COMMAND,IDC_SEND);
      }
    return true;   
    }
  
  //--------------------------------------------------
  
  void CMainFrame::OnMessageSendnew(UINT cmd) 
    {
    CallSend(cmd);
    }
  
  //--------------------------------------------------
  
  BOOL CMainFrame::OnMessageSendnewBool(UINT cmd) 
    {
    OnMessageSendnew(cmd);
    return TRUE;
    }
  
  //--------------------------------------------------
  
  void CMainFrame::OnUpdateMessagesSelection(CCmdUI* pCmdUI) 
    {
      pCmdUI->Enable(meslist.GetCurMessage()!=NULL);  
    }
  
  //--------------------------------------------------
  
  void CMainFrame::OnMessagesSelection(UINT cmd) 
    {
    POSITION pos=meslist.GetFirstSelectedItemPosition();
    while (pos)
      {
      int i=meslist.GetNextSelectedItem(pos);
      CNewsArtHolder *hld=(CNewsArtHolder *)meslist.GetItemData(i),*q=hld,*e=hld->EnumNextNoChild();
      while (q && e!=q)
        {
        switch (cmd)
          {
          case ID_MESSAGES_SELECTION_MARKASREAD:q->MarkAsRead(1);break;
          case ID_MESSAGES_SELECTION_DELETE:q->SetTrash();break;
          case ID_MESSAGES_SELECTION_MARKASUNREAD:q->MarkAsRead(2,false);break;
          case ID_MESSAGES_SELECTION_UNDELETE:q->SetTrash(false);break;
          }
        q=q->EnumNextNoChild();
//        if (hld->GetOpened()) q=q->EnumNextNoChild();else q=q->EnumNext();
        }
      meslist.InvalidateItems(i,i);
      }
    meslist.GetCurGroup()->CountNewsAndHighlights();
    acclist.AlterGroup(meslist.GetCurGroup()->iteminlist);
    meslist.GetCurAccount()->dirty=true;
    if (notify.IsEnabled())notify.CheckAfterCatchUp();
    }
  
  //--------------------------------------------------
  
  void CMainFrame::OnUpdateMessageSendnew(CCmdUI* pCmdUI) 
    {
    pCmdUI->Enable(meslist.GetCurGroup()!=NULL);
    }
  
  //--------------------------------------------------
  
  void CMainFrame::OnUpdateMessageReplyto(CCmdUI* pCmdUI) 
    {
    pCmdUI->Enable(meslist.GetCurMessage()!=NULL);	
    }
  
  //--------------------------------------------------
  
  LRESULT CMainFrame::OnFindAccount(WPARAM wParam, LPARAM lParam)
    {
    const char *name=(const char *)lParam;
    HTREEITEM it=acclist.FindAccount(name);
    if (it==NULL) return 0;
    return (LRESULT) acclist.GetAccount(it);
    }
  
  //--------------------------------------------------
  
  void CMainFrame::OnFileConfiguresmtp() 
    {
    if (theApp.sendopt.DoModal()==IDOK) theApp.sendopt.SaveConfig();
    }
  
  //--------------------------------------------------
  
  void CMainFrame::OnUpdateMessageSendemail(CCmdUI* pCmdUI) 
    {
    pCmdUI->Enable(acclist.GetAccountEx(lefttree.GetSelectedItem())!=NULL);	
    }
  
  //--------------------------------------------------
  
  void CMainFrame::OnMessagesMark(UINT id) 
    {
    EArticleState state;
    switch (id)
      {
      case ID_MESSAGES_MARKASIMPORTANT: state=EAImportant;break;
      case ID_MESSAGES_MARKASHIDDEN: state=EAHidden;break;
      case ID_MESSAGES_DELETEMARK: state=EANormal;break;
      }
    POSITION pos=meslist.GetFirstSelectedItemPosition();
    while (pos)
      {
      int i=meslist.GetNextSelectedItem(pos);
      CNewsArtHolder *hld=(CNewsArtHolder *)meslist.GetItemData(i);
      hld->state=state;
      if (!hld->GetOpened()) 
        {
        CNewsArtHolder *q=hld,*e=hld->EnumNextNoChild();
        while (q!=e && q) 
          {q->state=state;q=q->EnumNext();}
        }
      meslist.InvalidateItems(i,i);
      }
    if (meslist.GetCurGroup())
      {
      acclist.AlterGroup(meslist.GetCurGroup());
      }
    }
  
  //--------------------------------------------------
  
  void CMainFrame::OnViewMessagefilterDisplayallmessages() 
    {
    meslist.state_filter=EAHidden;	
    meslist.UpdateList();
    }
  
  //--------------------------------------------------
  
  void CMainFrame::OnUpdateViewMessagefilterDisplayallmessages(CCmdUI* pCmdUI) 
    {
    pCmdUI->SetCheck(meslist.state_filter==EAHidden);
    }
  
  //--------------------------------------------------
  
  void CMainFrame::OnViewMessagefilterDisplayonlyimportant() 
    {
    meslist.state_filter=EAImportant;	
    meslist.UpdateList();
    }
  
  //--------------------------------------------------
  
  void CMainFrame::OnUpdateViewMessagefilterDisplayonlyimportant(CCmdUI* pCmdUI) 
    {
    pCmdUI->SetCheck(meslist.state_filter==EAImportant);	
    }
  
  //--------------------------------------------------
  
  void CMainFrame::OnViewMessagefilterHidehidden() 
    {
    meslist.state_filter=EANormal;	
    meslist.UpdateList();
    }
  
  //--------------------------------------------------
  
  void CMainFrame::OnUpdateViewMessagefilterHidehidden(CCmdUI* pCmdUI) 
    {
    pCmdUI->SetCheck(meslist.state_filter==EANormal);	
    }
  
  //--------------------------------------------------
  
  void CMainFrame::OnViewGroupsastree() 
    {
    HTREEITEM it=acclist.GetAccountItem(lefttree.GetSelectedItem());
    if (it) 
      {
      CNewsAccount *acc=acclist.GetAccount(it);
      if (acc)
        {
        acc->viewastree=!acc->viewastree;
        acclist.AlterAccount(it,true);
        }
      }
    }
  
  //--------------------------------------------------
  
  void CMainFrame::OnUpdateViewGroupsastree(CCmdUI* pCmdUI) 
    {
    CNewsAccount *acc=acclist.GetAccountEx(lefttree.GetSelectedItem());
    pCmdUI->Enable(acc!=NULL);
    if (acc)
      pCmdUI->SetCheck(acc->viewastree);  	
    }
  
  //--------------------------------------------------
  
  static void DoRecursive(CTreeCtrl &tree, CAccountList &acc, HTREEITEM it, int what)
    {
    ItemType itm=acc.GetItemType(it);
    CNewsAccount *acnt=acc.GetAccountEx(it);
    if (itm==Itm_Folder)
      {
      it=tree.GetNextItem(it,TVGN_CHILD);
      while (it)
        {
        DoRecursive(tree,acc,it,what);
        it=tree.GetNextItem(it,TVGN_NEXT);
        }
      }
    else
      {
      CNewsGroup *grp=acc.GetGroup(it);	
      if (grp) 
        switch (what)
        {
        case 0: grp->CatchUp();acc.AlterGroup(it);break;
        case 1: grp->sign=false;break;
        case 2: acnt->Compact(true,grp);break;
        }
      }	
    }
  
  //--------------------------------------------------
  
  void CMainFrame::OnGroupsCatchup() 
    {
    HTREEITEM it=lefttree.GetSelectedItem();
    DoRecursive(lefttree,acclist,it,0);
    meslist.Invalidate();
    if (notify.IsEnabled()) notify.CheckAfterCatchUp();
    }
  
  //--------------------------------------------------
  
  void CMainFrame::OnUpdateGroups(CCmdUI* pCmdUI) 
    {
    HTREEITEM it=lefttree.GetSelectedItem();	
    ItemType itm=acclist.GetItemType(it);	
    pCmdUI->Enable(itm==Itm_Group || itm==Itm_Folder);
    }
  
  //--------------------------------------------------
  
  void CMainFrame::OnGroupsUnsubscribe() 
    {
    HTREEITEM it=lefttree.GetSelectedItem();
    DoRecursive(lefttree,acclist,it,1);
    while (acclist.GetItemType(it)!=Itm_Account) 
      it=lefttree.GetNextItem(it,TVGN_PARENT);
    acclist.AlterAccount(it);
    }
  
  //--------------------------------------------------
  
#ifdef DEMO
  void CMainFrame::OnMessagesSelectionCancelmessages() 
    {
    DemoWarn();
    }
  
  //--------------------------------------------------
  
#else
  void CMainFrame::OnMessagesSelectionCancelmessages() 
    {  
    if (NrMessageBox(IDS_CANCELMESSAGEASK,MB_YESNO)==IDNO) return;
    if (!theApp.OfflineWarn()) return;
    CNewsTerminal term;
    CNewsAccount *acc=meslist.GetCurAccount();
    if (acc==NULL) return;
    EnableWindow(FALSE);
    SetCursor(::LoadCursor(NULL,IDC_WAIT));
    POSITION pos;
    CString line;
	int lastreply=IDCANCEL;
	bool dontask=false;

    if (term.ShowError(acc->OpenConnection(term))) goto end;
    pos=meslist.GetFirstSelectedItemPosition();
    while (pos)
      {
	  bool candel=true;
      int i=meslist.GetNextSelectedItem(pos);
      CNewsArtHolder *hld=(CNewsArtHolder *)meslist.GetItemData(i);
	  ::GetEmailFromSender(hld->from,line);
	  if (stricmp(line,acc->GetMyEmail())!=0)
		{
		if (!dontask) 
		  {
		  line.Format(IDS_CANCELWARNING,hld->subject);
		  CMsgBox box;
		  box.Flags.yesbutt=box.Flags.nobutt=box.Flags.noagain=1;
		  box.noagain=FALSE;
		  box.message=line;
		  lastreply=box.DoModal();
		  dontask=box.noagain!=FALSE;
		  }
		if (lastreply==IDCANCEL) break;
		if (lastreply==IDNO) candel=false;		
		}
	  if (candel)
		{
		if (CancelMessage(term,hld,meslist.GetCurGroup()->GetName())==false) break;
		hld->SetTrash(true);
		}
      meslist.Invalidate();
      }
    term.SendLine("QUIT");
end:
    EnableWindow(TRUE);
    }
  
  //--------------------------------------------------
  
#endif
  
  void CMainFrame::OnFileOptions() 
    {
    if (theApp.config.DoModal()==IDOK)
      {
      theApp.config.SaveConfig();
      memcache.SetCacheSize(theApp.config.cache);
      HTREEITEM it;
      for (it=NULL;it=acclist.EnumAccounts(it);)
        {
        CNewsAccount *acc=acclist.GetAccount(it);
        acc->Compact(false);
        /*	  theApp.Lock();
        CNewsGroup *grp=acc->GetGroups();
        acc->ActivateKeywords();
        while (grp)
        {
        if (grp->GetList())grp->GetList()->CalcHierarchy(false,0,NULL);
        grp=grp->GetNext();
        }
        theApp.Unlock();*/
        }
      meslist.UpdateList();
      if (notify.IsEnabled())
        {
        notify.Repaint();
        if (!theApp.config.nrnotify) notify.DestroyWindow();
        }
      else if (!notify.IsEnabled() && theApp.config.nrnotify) notify.Create(IDR_MAINFRAME,IDI_NOTIFY,IDI_NOTIFYSTOP,this);
      LoadToolbars(theApp.config.bigicons!=FALSE);
      if (theApp.useprofiles!=(theApp.config.userprof!=FALSE))
        {
        if (NrMessageBox(IDS_ASKRESTARTNR,MB_OKCANCEL)==IDOK)
          {
          if (::NrKillWindows()==true)
            {
            theApp.profchange=true;
            OnClose();
            }
          }
        }
      }
    }
  
  //--------------------------------------------------
  
  void CMainFrame::OnGroupsCompact() 
    {
    HTREEITEM it=lefttree.GetSelectedItem();
    CNewsAccount *acc=acclist.GetAccountEx(it);
    if (acc==NULL) return;
    //  theApp.Lock();
    //  acc->ActivateKeywords();
    DoRecursive(lefttree,acclist,it,2);
    meslist.UpdateList();
    //  theApp.Unlock();
    }
  
  //--------------------------------------------------
  
  void CMainFrame::OnFileExport() 
    {
    HTREEITEM it=lefttree.GetSelectedItem();
    if (it==NULL) return;
    char *p=getcwd(NULL,0);
    CString path(p);free(p);
    if (AskForPath(path)==false) return;
    SetCursor(::LoadCursor(NULL,IDC_WAIT));
    CNewsAccount *acc=acclist.GetAccountEx(it);
    CScoopMutex scoop(acc->mutex,5000);
    if (!scoop) 
      {NrMessageBox(IDS_INEX_FAILED);return;}
    acclist.AccountStreaming(it,true);
    if (path[path.GetLength()-1]!='\\') path+='\\';
    if (ExportAccount(theApp.FullPath(acc->GetFName()+CString(ACCNEWEXT)),path+acc->GetFName()+ACCEXTX)==false)
      NrMessageBox(IDS_INEX_FAILED);
    }
  
  //--------------------------------------------------
  
  void CMainFrame::OnFileImport() 
    {
    CFileDialog dlg(TRUE,NULL,NULL,OFN_FILEMUSTEXIST|OFN_NOCHANGEDIR|OFN_HIDEREADONLY,"Newsreader account|*"ACCEXTX"||");
    if (dlg.DoModal()==IDCANCEL) return;
    SetCursor(::LoadCursor(NULL,IDC_WAIT));
    CString fname=dlg.GetFileName();
    CString pname=dlg.GetPathName();
    char *c=strrchr(fname.LockBuffer(),'.');
    strcpy(c,ACCNEWEXT);
    fname.UnlockBuffer();
    if (ImportAccount(pname,theApp.FullPath(fname))==false)
      {NrMessageBox(IDS_INEX_FAILED);return;}
    meslist.LoadList(NULL,NULL);
    HTREEITEM it;
    fname.Delete(fname.GetLength()-strlen(ACCNEWEXT)-1,strlen(ACCNEWEXT)+1);
    for (it=NULL;it=acclist.EnumAccounts(it);)
      {
      CNewsAccount *acc=acclist.GetAccount(it);
      if (acc->GetFName()==fname) break;
      }
    if (it==NULL)
      {
      CNewsAccount *acc=new CNewsAccount();
      acc->SetFName(fname);
      acclist.InsertAccount(acc,false);
      it=acclist.FindItem(NULL,(DWORD)acc);
      }
    CNewsAccount *acc=acclist.GetAccount(it);
    CScoopMutex scoop(acc->mutex,5000);
    if (!scoop) 
      {NrMessageBox(IDS_INEX_FAILED);return;}
    if (acclist.AccountStreaming(it,false)==false)
	  {
	  NrMessageBox(IDS_INEX_FAILED);
	  lefttree.DeleteItem(it);
	  return;
	  }
    acclist.AlterAccount(it,true);
    }
  
  //--------------------------------------------------
  
#define WINDOW "Window"
#define LAYOUT "Layout"
  
  void CMainFrame::SaveWndState()
    {
    ostrstream p;
    WINDOWPLACEMENT wp;
    GetWindowPlacement(&wp);
    p.write((char *)&wp,sizeof(wp));
    p.write((char *)&acclist.astree,sizeof(acclist.astree));
    p.write((char *)&meslist.state_filter,sizeof(meslist.state_filter));
    int i;
    i=splith.GetSplitPos();
    p.write((char *)&i,sizeof(i));
    i=splith2.GetSplitPos();
    p.write((char *)&i,sizeof(i));
    i=splitw.GetSplitPos();
    p.write((char *)&i,sizeof(i));
    for (i=0;i<5;i++)
      {
      int sz=meslist.GetColumnWidth(i);
      p.write((char *)&sz,sizeof(i));
      }
    theApp.WriteProfileBinary(WINDOW,LAYOUT,(BYTE *)p.str(),p.tellp());
    p.rdbuf()->freeze(false);
    SaveBarState("BARS");
    }
  
  //--------------------------------------------------
  
  void CMainFrame::LoadWndState()
    {
    BYTE *data;
    UINT size;
    LoadBarState("BARS");
    if (theApp.GetProfileBinary(WINDOW,LAYOUT,&data,&size)==FALSE) return;
    istrstream p((char *)data,size);
    WINDOWPLACEMENT wp;
    p.read((char *)&wp,sizeof(wp));
    p.read((char *)&acclist.astree,sizeof(acclist.astree));
    p.read((char *)&meslist.state_filter,sizeof(meslist.state_filter));
    int i;
    MoveWindow(&wp.rcNormalPosition);
    ShowWindow(SW_SHOW);
    if (wp.showCmd==SW_SHOWMAXIMIZED) ShowWindow(SW_SHOWMAXIMIZED);
    p.read((char *)&i,sizeof(i));
    splith.SetSplitPos(i);
    p.read((char *)&i,sizeof(i));
    //  splith2.SetSplitPos(i);
    p.read((char *)&i,sizeof(i));
    splitw.SetSplitPos(i);
    for (i=0;i<5;i++)
      {
      int sz=i<2?20:200;
      p.read((char *)&sz,sizeof(i));
      meslist.SetColumnWidth(i,sz);
      }
    free(data);
    }
  
  //--------------------------------------------------
  
  void CMainFrame::OnMessagesSelectall() 
    {
    int cnt=meslist.GetItemCount();
    for (int i=0;i<cnt;i++)
      meslist.SetItemState(i,LVIS_SELECTED,LVIS_SELECTED);
    
    }
  
  //--------------------------------------------------
  
  void CMainFrame::OnListpopupProperties() 
    {
    CNewsArtHolder *hld=meslist.GetCurMessage();
    CString s;
    s.Format("Message id :%s\nReference to %s",hld->messid,hld->refid);
    MessageBox(s);
    }
  
  //--------------------------------------------------
  
  void CMainFrame::OnListpopupShowparent() 
    {
    CNewsArtHolder *hld=meslist.GetCurMessage();
    CNewsGroup *grp=meslist.GetCurGroup();
    CNewsArtHolder *top=grp->GetList();
    CNewsArtHolder *fnd=top->FindMessage(hld->refid);
    if (fnd) MessageBox(fnd->subject);
    }
  
  //--------------------------------------------------
  
  void CMainFrame::OnGroupsFind() 
    {
    fnddlg.acclist=&acclist;
    fnddlg.lefttree=&lefttree;
    fnddlg.msglist=&meslist;
    fnddlg.Open();
    }
  
  //--------------------------------------------------
  
  LRESULT CMainFrame::OnSelectMessage(WPARAM wParam, LPARAM lParam)
    {
	  CFindList *ls=(CFindList *)lParam;
	  if (ls->msgid)
	  {
		this->GotoMessageId(ls->msgid);
	  }
	  else
	  {
		HTREEITEM it=acclist.FindItem(NULL,(LPARAM)(ls->grp));
		lefttree.SelectItem(it);
		meslist.SelectItem(ls->hlda);
	  }
    return 0;
    }
  
  //--------------------------------------------------
  
  void CMainFrame::OnFileProxyoptions() 
    {
    theApp.proxy.DoModal();	
    }
  
  //--------------------------------------------------
  
  void CMainFrame::OnMessagesClosethread() 
    {
    if (NrMessageBox(IDS_ASKCANCEL,MB_YESNO)==IDNO) return;
    CNewsTerminal term;
    CNewsAccount *acc=meslist.GetCurAccount();
    if (acc==NULL) return;
    if (term.ShowError(acc->OpenConnection(term))) return;
    int err;
    POSITION pos=meslist.GetFirstSelectedItemPosition();
    while (pos)
      {
      term.SendLine("POST");
      err=term.ReceiveNumReply()/10;
      if (err!=34) 
        {term.ShowError(err);return;}
      int i=meslist.GetNextSelectedItem(pos);
      CNewsArtHolder *hld=(CNewsArtHolder *)meslist.GetItemData(i);
      term.SendFormatted("From: %s",acc->GetMyEmail());
      term.SendFormatted("NewsGroups: %s",meslist.GetCurGroup()->GetName());
      term.SendFormatted("References: %s",hld->messid);
      term.SendFormatted("Subject: "CANCELTHREADSUBJECT);
      term.SendLine("");
      term.SendLine(CANCELTHREADSUBJECT);
      term.SendLine(".");
      err=term.ReceiveNumReply()/10;
      if (err!=24) 
        {term.ShowError(err);return ;}
      meslist.Invalidate();
      }
    term.SendLine("QUIT");
    ::DogShowErrorV(IDS_CANCELDONE);
    }
  
  //--------------------------------------------------
  
  BOOL CMainFrame::OnQueryEndSession() 
    {
    CNRCleanUp clr(incleanup);
    if (!clr) return FALSE;
    if (!CFrameWnd::OnQueryEndSession())
      return FALSE;	
    if (NrKillWindows()==false) return FALSE;
    return TRUE;
    }
  
  //--------------------------------------------------
  
  void CMainFrame::CleanUp()
    {
    if (fnddlg.m_hWnd) fnddlg.DestroyWindow();
    TRACE0("CleanUp\n");
    CWaitCursor wait;
    theApp.splashthrd.splashout=true;
    theApp.splashthrd.CreateThread(0,16384,NULL);
    theApp.splashthrd.m_bAutoDelete=FALSE;
    if (notify.IsEnabled()) notify.DestroyWindow();
	CNewsTerminal::StopTrafic();
    //  DogShowStatusV(IDS_EXITTING);
	watchdogalive=TRUE;
    theApp.RunBackground(0,BGRNCLOSED,10000);
	watchdogalive=TRUE;
    theApp.RunBackground(1,BGRNCLOSED,10000);
	watchdogalive=TRUE;
    theApp.RunBackground(2,BGRNCLOSED,10000);
	watchdogalive=TRUE;
    SaveWndState();
    ShowWindow(SW_HIDE);  
    acclist.SerializeList(true);
	watchdogalive=TRUE;
    MSG msg;
    while (::PeekMessage(&msg,*this,NRM_AUTOSIGNUP,NRM_SAVEACCSTATE,PM_REMOVE))
      {
      if (msg.message==NRM_AUTOSIGNUP || msg.message==NRM_SAVEACCSTATE)
        {
        CNewsAccount *acc=(CNewsAccount *)msg.lParam;
        acc->Release();
        }
      }
	watchdogalive=TRUE;
    acclist.DeleteAll();
    //  msgtext.Quit();
	watchdogalive=TRUE;    

    char kbdl[KL_NAMELENGTH];
    GetKeyboardLayoutName(kbdl);
    theApp.WriteProfileString("Keyboard","SavedLayout",kbdl);

    theApp.m_pMainWnd=NULL; //prevent posting WM_QUIT
    CFrameWnd::OnClose();
    theApp.ExitInstance();
    exit(0);
    }
  
  //--------------------------------------------------
  
  void CMainFrame::OnEndSession(BOOL bEnding) 
    {
    if (bEnding) CleanUp();
    CFrameWnd::OnEndSession(bEnding);
    }
  
  //--------------------------------------------------
  
  void CMainFrame::OnMessagesRereadfromserver() 
    {
    if (!theApp.OfflineWarn()) return;
    CNewsAccount *acc=meslist.GetCurAccount();
    POSITION pos=meslist.GetFirstSelectedItemPosition();
    while (pos)
      {
      int i=meslist.GetNextSelectedItem(pos);
      CNewsArtHolder *hld=(CNewsArtHolder *)meslist.GetItemData(i);
      hld->RemoveFromCache();    
      acc->cache.DeleteMessage(hld->messid);
      }
    msgtext.NavigateArticle(meslist.GetCurAccount(),meslist.GetCurGroup(),meslist.GetCurMessage());
    }
  
  //--------------------------------------------------
  
  LRESULT CMainFrame::OnNrmGetselmessageref(WPARAM wParam,LPARAM lParam)
    {
    char *buff=(char *)lParam;
    CNewsArtHolder *hld=meslist.GetCurMessage();
    if (hld==NULL) buff[0]=0;
    else
      {
      if ((WPARAM)hld->messid.GetLength()>=wParam-1) return 0;
      strcpy(buff,hld->messid);
      }
    return 1;
    }
  
  //--------------------------------------------------
  
  void CMainFrame::OnMessagesBack() 
    {
    CNewsAccount *acc=meslist.GetCurAccount();
    if (acc) 
      {
      acc->history.disable=true;
      GotoMessageId(acc->history.GetBack());
      }
    }
  
  //--------------------------------------------------
  
  void CMainFrame::OnUpdateMessagesBack(CCmdUI* pCmdUI) 
    {
    CNewsAccount *acc=meslist.GetCurAccount();
    pCmdUI->Enable(acc && acc->history.CanBack());
    }
  
  //--------------------------------------------------
  
  void CMainFrame::OnMessagesForward() 
    {
    CNewsAccount *acc=meslist.GetCurAccount();
    CNewsGroup *grp=meslist.GetCurGroup();
    if (acc) 	
      {
      if (acc->history.CanForward())
        {
        acc->history.disable=true;
        GotoMessageId(acc->history.GetForward());
        }
      else
        {
        meslist.SelectNewestMsg();
        }
      }
    }
  
  //--------------------------------------------------
  
  void CMainFrame::OnUpdateMessagesForward(CCmdUI* pCmdUI) 
    {
    CNewsAccount *acc=meslist.GetCurAccount();
    CNewsGroup *grp=meslist.GetCurGroup();
    pCmdUI->Enable((acc && acc->history.CanForward())||(grp && grp->GetNews()));
    }
  
  //--------------------------------------------------
  
  void CMainFrame::GotoMessageId(const CString &msgid, bool ignoreSpecial)
    {
    SetCursor(::LoadCursor(NULL,IDC_WAIT));
    CNewsAccount *acc=acclist.GetAccountEx(lefttree.GetSelectedItem());
    if (acc!=NULL)
      {		  		  
      CNewsGroup *grp=acc->GetGroups();
      CNewsArtHolder *hld=NULL;
      while (grp!=NULL && hld==NULL)
        {
        if (!ignoreSpecial || !grp->special)
            hld=grp->GetList()->FindMessage(msgid);
        if (hld==NULL) grp=grp->GetNext();
        }
      if (hld)
        {
        //	  acc->history.disable=true;
        meslist.LoadList(acc,grp,false);
        lefttree.SelectItem(acclist.FindItem(NULL,(DWORD)grp));
        meslist.SelectItem(hld);
        }
      else if (theApp.OfflineWarn())
        {        
        CDialog dlg;
        dlg.Create(IDD_WAITRETRIEVE,this);
        dlg.ShowWindow(SW_SHOW);
        dlg.SetActiveWindow();
        dlg.CenterWindow();
        dlg.UpdateWindow();
        EnableWindow(false);
        SetCursor(LoadCursor(NULL,IDC_WAIT));
        grp=NULL;
        CString message;
        if (acc->LoadArticle(msgid,&hld,&grp,&message))
          {
          acc->GetGroups()->LoadToTree(lefttree,acclist.FindItem(NULL,(DWORD)acc),acc->viewastree);
          meslist.LoadList(acc,grp,false);
          HTREEITEM it=acclist.FindItem(NULL,(DWORD)grp);
          lefttree.EnsureVisible(it);
          lefttree.SelectItem(it);
          meslist.SelectItem(hld);	  
          }
        else
          {
            CString xx;
            AfxFormatString1(xx,IDS_ARTICLEHASNOTBEENFOUND,message);
          NrMessageBox(xx);
          }
        EnableWindow(true);
        }
      }		
    }
  
  //--------------------------------------------------
  
#include "DownloadProgress.h"
  
  void CMainFrame::OnMessagesSelectionDownload() 
    {  	
    if (!theApp.OfflineWarn()) return;
    CDownloadProgress dlg;
    dlg.acc=meslist.GetCurAccount();
    dlg.grp=meslist.GetCurGroup();
    dlg.itemtree=&meslist;
    if (dlg.acc==NULL || dlg.grp==NULL) return;
    if (dlg.acc->offline==0)
      NrMessageBox(IDS_NOCACHE);
    else
      dlg.DoModal();
    }
  
  //--------------------------------------------------
  
  void CMainFrame::OnMessagesLostsaved() 
    {
    CLostFoundDlg dlg;
    dlg.DoModal();
    }
  
  //--------------------------------------------------
  
  void CMainFrame::OnNextWnd() 
    {
    CWnd *p=GetFocus();
    if (p==&meslist) p=&msgtext;
    else if (p==&msgtext) p=&lefttree;
    else if (p==&lefttree) p=&meslist;
    else p=&lefttree;
    p->SetFocus();
    }
  
  //--------------------------------------------------
  
  void CMainFrame::OnPrevWnd() 
    {
    CWnd *p=GetFocus();
    if (p==&meslist) p=&lefttree;
    else if (p==&msgtext) p=&meslist;
    else if (p==&lefttree) p=&msgtext;
    else p=&lefttree;
    p->SetFocus();
    }
  
  //--------------------------------------------------
  
  void CMainFrame::OnFileOffline() 
    {
    theApp.offline=!theApp.offline;
    if (theApp.offline) CNewsTerminal::StopTrafic();
    if (notify.IsEnabled()) notify.Repaint();
    }
  
  //--------------------------------------------------
  
  void CMainFrame::OnUpdateFileOffline(CCmdUI* pCmdUI) 
    {
    pCmdUI->SetCheck(theApp.offline);	
    }
  
  //--------------------------------------------------
  
  void CMainFrame::OnFullLoad() 
    {
    CNewsGroup *grp=acclist.GetGroup(lefttree.GetSelectedItem());
    if (grp) grp->fullload=!grp->fullload;
    }
  
  //--------------------------------------------------
  
  void CMainFrame::OnUpdateFullLoad(CCmdUI* pCmdUI) 
    {
    CNewsGroup *grp=acclist.GetGroup(lefttree.GetSelectedItem());
    CNewsAccount *acc=acclist.GetAccountEx(lefttree.GetSelectedItem());
    if (acc!=NULL && grp!=NULL && acc->offline)
      {
      pCmdUI->Enable(TRUE);
      pCmdUI->SetCheck(grp->fullload);	
      }
    else
      {
      pCmdUI->Enable(FALSE);
      }
    }
  
  //--------------------------------------------------
  
#include "Gotorefdlg.h"
  
  void CMainFrame::OnMessagesGoto()
    {
    CGotoRefDlg dlg;
    dlg.acc=meslist.GetCurAccount();
    if (dlg.DoModal()==IDOK)
      {
      dlg.ref="<"+dlg.ref+">";
      GotoMessageId((LPCTSTR)dlg.ref);
      }
    }
  
  //--------------------------------------------------
  
  LRESULT CMainFrame::OnSaveAccState(WPARAM wParam, LPARAM lParam)
    {
    CNewsAccount *acc=(CNewsAccount *)lParam;
    if (acc)
      {
	  if (!acc->IsSaved())
		{
		HTREEITEM it=acclist.FindItem(NULL,lParam);
		CSaveAccThread *sacc=new CSaveAccThread(&acclist,it);
		if (theApp.RunBackground(0,sacc,1000)==false) delete sacc;
		}
      acc->Release();
      }
    return 0;
    }
  
  //--------------------------------------------------
  
  BOOL CMainFrame::PreTranslateMessage(MSG* pMsg) 
    { 
    ::LogWindowMessage(pMsg->message,pMsg->wParam,pMsg->lParam);
    if (pMsg->message==WM_MOUSEWHEEL)
      {
      CWnd *target=WindowFromPoint(pMsg->pt);
      if (target) target->SendMessage(pMsg->message,pMsg->wParam,pMsg->lParam);
      return TRUE;
      }
    if (pMsg->message==WM_KEYDOWN && pMsg->wParam>=VK_F1 && pMsg->wParam<=VK_F12) 
      return CFrameWnd::PreTranslateMessage(pMsg);
    if (pMsg->message==WM_KEYDOWN && pMsg->wParam==VK_ESCAPE) 
      {
      SetFocus();
      return TRUE;
      }
    CWnd *focus=GetFocus();
    if (focus && focus->GetParent()==&m_wndFindBar)
      if (m_wndFindBar.IsDialogMessage(pMsg)) return TRUE;
    return CFrameWnd::PreTranslateMessage(pMsg);
    }
  
  //--------------------------------------------------
  
  void CMainFrame::OnMsgNtTest()
    {
    CNewsArtHolder *hld=meslist.GetCurMessage(); 
    if (hld && notify.IsEnabled()) 
      {
      notify.PostMessage(NRN_NEWMESSAGE,2,(LPARAM)meslist.GetCurAccount());
      notify.PostMessage(NRN_NEWMESSAGE,1,(LPARAM)meslist.GetCurGroup());
      hld->AddRef();
      notify.PostMessage(NRN_NEWMESSAGE,0,(LPARAM)hld);
      }
	TestMutex();
    }
  
  //--------------------------------------------------
  
static UINT ResetFulltextIndex(LPVOID fulltext)
{
  FullTextIndex *pfulltext=(FullTextIndex *)fulltext;
  pfulltext->ResetIndex();
  return 0;
}

  void CMainFrame::OnSize(UINT type, int cx, int cy)
    {
    CFrameWnd::OnSize(type,cx,cy);
    if (type==SIZE_MINIMIZED && notify.IsEnabled()) 
      {
      ShowWindow(SW_HIDE);
      SetProcessWorkingSetSize(GetCurrentProcess(),0xffffffff,0xffffffff);
      for (HTREEITEM item=NULL;item=acclist.EnumAccounts(item);)
        {
          CNewsAccount *acc=acclist.GetAccount(item);
          if (acc && acc->IsLoaded() && acc->fulltext.IsCreated()) 
            AfxBeginThread((AFX_THREADPROC)ResetFulltextIndex,&acc->fulltext);
        }
      }
    
    }
  
  //--------------------------------------------------
  
#include <afxadv.h>
  
  bool CMainFrame::LoadToolbars(bool big)
    {
    bool rld=m_wndToolBar.m_hWnd!=NULL;
    CDockState st1;
    if (rld)
      {
      m_wndToolBar.LoadToolBar(big?IDR_BIGMAINFRAME:IDR_MAINFRAME);
      m_wndSyncTB.LoadToolBar(big?IDR_BIGSYNCHRONIZE:IDR_SYNCHRONIZE);
      RecalcLayout( );
      }
    else
      {
      dogtimer=SetTimer(DOGTIMER,DOGTIMER,NULL);
      if (!m_wndToolBar.CreateEx(this,  TBSTYLE_FLAT, WS_CHILD | WS_VISIBLE | CBRS_TOP
        |/* CBRS_GRIPPER | */CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC) ||
        !m_wndToolBar.LoadToolBar(big?IDR_BIGMAINFRAME:IDR_MAINFRAME))
        {
        TRACE0("Failed to create toolbar\n");
        return false;      // fail to create
        }
      if (!m_wndSyncTB.CreateEx(this, TBSTYLE_FLAT, WS_CHILD | WS_VISIBLE | CBRS_TOP
        |/* CBRS_GRIPPER | */CBRS_TOOLTIPS | CBRS_FLYBY | CBRS_SIZE_DYNAMIC,CRect(0,0,0,0),ID_VIEW_SYNCHRONIZEBUTTON) ||
        !m_wndSyncTB.LoadToolBar(big?IDR_BIGSYNCHRONIZE:IDR_SYNCHRONIZE))
        {
        TRACE0("Failed to create toolbar\n");
        return false;      // fail to create
        }
      if (!m_wndFilter.Create(this,IDD_FILTERTOOL,CBRS_TOP,ID_VIEW_FILTER))
        {
        TRACE0("Failed to create dialogbar\n");
        return false;      // fail to create
        }
      if (!m_wndHighlight.Create(this,IDD_HIGHLIGHTOOL,CBRS_TOP,ID_VIEW_HIGHLIGHT))
        {
        TRACE0("Failed to create dialogbar\n");
        return false;      // fail to create
        }
      
      if (!m_wndFindBar.Create(this,IDD_FINDTOOL,CBRS_TOP,ID_VIEW_FASTFIND))
        {
        TRACE0("Failed to create dialogbar\n");
        return false;      // fail to create
        }
      m_wndToolBar.EnableDocking(CBRS_ALIGN_ANY);
      m_wndSyncTB.EnableDocking(CBRS_ALIGN_ANY);
      m_wndFilter.EnableDocking(CBRS_ALIGN_TOP|CBRS_ALIGN_BOTTOM);
      m_wndHighlight.EnableDocking(CBRS_ALIGN_TOP|CBRS_ALIGN_BOTTOM);
      m_wndFindBar.EnableDocking(CBRS_ALIGN_TOP|CBRS_ALIGN_BOTTOM);
      EnableDocking(CBRS_ALIGN_ANY);
      DockControlBar(&m_wndToolBar);
      DockControlBar(&m_wndSyncTB);
      DockControlBar(&m_wndFilter);
      DockControlBar(&m_wndHighlight);
      DockControlBar(&m_wndFindBar);
      ShowControlBar(&m_wndFindBar,FALSE,FALSE);
      wFilters.Attach(m_wndFilter.GetDlgItem(IDC_FILTER)->GetSafeHwnd());
      wHighlights.Attach(m_wndHighlight.GetDlgItem(IDC_HIGHLIGHT)->GetSafeHwnd());
      
      m_wndFindBar.CheckDlgButton(IDC_QF_SUBJECT,1);
      m_wndFindBar.CheckDlgButton(IDC_QF_12MONTHS,1);
      }
    m_wndFilter.m_sizeDefault.cy=big?49:32;  
    m_wndHighlight.m_sizeDefault.cy=big?49:32;  
    m_wndFindBar.m_sizeDefault.cy=big?49:32;  
    return true;
    }
  
  //--------------------------------------------------
  
#ifdef DEMO
  void CMainFrame::OnCheckVersion()
    {
    ::DemoWarn();
    }
  
  //--------------------------------------------------
  
#else
  void CMainFrame::OnCheckVersion()
    {
    theApp.CheckForNewVersion(TRUE);
    }
  
  //--------------------------------------------------
  
#endif
  void CMainFrame::OnUpdateCheckVersion(CCmdUI* pCmdUI)
    {
    pCmdUI->Enable(theApp.EnableCheckForNewVersion());
    }
  
  //--------------------------------------------------
  
  void CMainFrame::OnViewKwdSet()
    {
    CSelKwdSet dlg;
    HTREEITEM it=lefttree.GetSelectedItem();
    if (it==NULL) return;
    CNewsAccount *acc=acclist.GetAccountEx(it);
    if (acc==NULL) return;
/*    dlg.kwrds=acc->GetKeywords();
    dlg.cursel=acc->GetCurFilter();
    if (dlg.DoModal()==IDOK)
      {
      acc->GetCurFilter()=dlg.cursel;
      acc->Compact(false,NULL);
      meslist.UpdateList();
      }*/
    }
  
  //--------------------------------------------------
  
#include "CacheMaintanceDlg.h"
#include ".\mainfrm.h"

  void CMainFrame::OnCacheMaintance()
    {
    HTREEITEM it=lefttree.GetSelectedItem();
    if (it==NULL) return;
    CNewsAccount *acc=acclist.GetAccountEx(it);
    if (acc==NULL) return;
    CCacheMaintanceDlg dlg;
    dlg.it=it;
    dlg.acc=acc;
    dlg.acclist=&acclist;
    dlg.DoModal();
    }
  
  //--------------------------------------------------
  
  BOOL CMainFrame::OnDisplayVersion(UINT cmd)
    {
    CString dver(theApp.GetVersionString());
    ::DogShowStatusV(dver);
    return TRUE;
    }
  
  //--------------------------------------------------
  
  void CMainFrame::OnExtendedSynchronize()
    {
    HTREEITEM it=lefttree.GetSelectedItem();
    if (it==NULL) return;
    CNewsAccount *acc=acclist.GetAccountEx(it);
    if (acc==NULL) return;
    const char *askid="ExtendedSync";
    BOOL nask=theApp.GetRegInt(askid,0);
    if (!nask)
      {
      CMsgBox box;
      box.message.LoadString(IDS_ASKEXTENDEDSYNC);
      box.Flags.cancelbutt=box.Flags.okbutt=box.Flags.noagain=1;
      box.msgBeep=MB_ICONQUESTION;
      if (box.DoModal()==IDCANCEL) return;
      theApp.SetRegInt(askid,box.noagain);
      }
    acc->extsync=true;  
    OnAccountsSynchronize();
    }
  
  //--------------------------------------------------
  
  void CMainFrame::OnCopyShortcut()
    {
    CNewsArtHolder *h=meslist.GetCurMessage();
    if (h)	
      CMessageList::CopyShortcut(h);	
    }
  
  //--------------------------------------------------
  
  void CMainFrame::InitFilterSubmenu( CMenu* pPopupMenu, int idCmd, int idNone, int kwpos)
    {
    CNewsAccount *acc=acclist.GetAccountEx(lefttree.GetSelectedItem());
    if (acc==NULL) return;
    // submenu identified - filtering
    int cnt=pPopupMenu->GetMenuItemCount();
    for (int del=1;del<cnt;del++) 
      pPopupMenu->RemoveMenu(1,MF_BYPOSITION);
    ThreadRules &rules=acc->GetRules();
    for (int i=rules.GetRuleIdMin();i<=rules.GetRuleIdMax();i++)
    {
      pPopupMenu->AppendMenu(MF_STRING|(kwpos==i?MF_CHECKED:0),i+idCmd,rules.GetRule(i).GetName());
    }
    pPopupMenu->CheckMenuItem(idNone,(kwpos==-1)?MF_CHECKED:MF_UNCHECKED);  
    }
  
  //--------------------------------------------------
  
  void CMainFrame::OnInitMenuPopup( CMenu* pPopupMenu, UINT nIndex, BOOL bSysMenu )
    {
    CFrameWnd::OnInitMenuPopup(pPopupMenu,nIndex,bSysMenu);
    // check which submenu is initalized
    if (pPopupMenu->GetMenuItemCount()<=0) return;
    CNewsAccount *acc=acclist.GetAccountEx(lefttree.GetSelectedItem());
    if (acc==NULL) return;
    // check first item id
    if (pPopupMenu->GetMenuItemID(0)==ID_THREADS_NOFILTERING)
      {
      // submenu identified - filtering
      InitFilterSubmenu(pPopupMenu,ID_THREADS,ID_THREADS_NOFILTERING,acc->GetCurFilter());
      }
    else if (pPopupMenu->GetMenuItemID(0)==ID_THREADS_NOHIGHLITING)
      {
      // submenu identified - filtering
      InitFilterSubmenu(pPopupMenu,ID_THREADS_HIGHLIGHT,ID_THREADS_NOHIGHLITING,acc->GetCurHighlight());
      }
    }
  
  //--------------------------------------------------
  
  void CMainFrame::OnUseEditCond(UINT cmdid)
    {
    CNewsAccount *acc=acclist.GetAccountEx(lefttree.GetSelectedItem());
    if (acc==NULL) return;
    int pt=cmdid- ID_THREADS ;
    if (cmdid==ID_THREADS_NOFILTERING) pt=-1;
    SetCursor(::LoadCursor(NULL,IDC_WAIT));
/*    if (GetKeyState(VK_SHIFT) & 0x80)
      {
      CContitionsDlg dlg;
      CString cond;
      acc->GetKeyword(pt,cond);
      CNewsAccount::EnDeCodeCond(cond,dlg.name,dlg.cond,false);
      int res=dlg.DoModal();
      if (res==IDC_DELETE)	cond="";
      else	
        {
        CNewsAccount::EnDeCodeCond(cond,dlg.name,dlg.cond,true);
        }
      acc->SetKeyword(pt,cond);
      
      }  */
    acc->GetRules().SetFilter(pt);
    acc->ApplyCond(true);
    LoadFiltersToCombo(acc);
    
    meslist.UpdateList(); 
    }
  
  //--------------------------------------------------
  
  void CMainFrame::OnUseEditHighlight(UINT cmdid)
    {
    CNewsAccount *acc=acclist.GetAccountEx(lefttree.GetSelectedItem());
    if (acc==NULL) return;
    int pt=cmdid - ID_THREADS_HIGHLIGHT;
    if (cmdid==ID_THREADS_NOHIGHLITING) pt=-1;
    SetCursor(::LoadCursor(NULL,IDC_WAIT));
/*    if (GetKeyState(VK_SHIFT) & 0x80)
      {
      CContitionsDlg dlg;
      CString cond;
      acc->GetKeyword(pt,cond);
      CNewsAccount::EnDeCodeCond(cond,dlg.name,dlg.cond,false);
      int res=dlg.DoModal();
      if (res==IDC_DELETE)	cond="";
      else	
        {
        CNewsAccount::EnDeCodeCond(cond,dlg.name,dlg.cond,true);
        }
      acc->SetKeyword(pt,cond);
      } */
    acc->ApplyHighlight(true);  
    acc->PostApplyConds(NULL);
    LoadFiltersToCombo(acc);
    
    // mark all groups as unsorted
/*    
    CNewsGroup *grp=acclist.GetGroup(lefttree.GetSelectedItem());
    if (grp)
      {
      // sort items in active group
      grp->SortItemsIfNeeded(_globalSortItem);
      meslist.ChangeLastSort(grp->GetCurrentSort());
      }
    */
    meslist.Invalidate();
    
    //meslist.UpdateList(); 
    }
  
  //--------------------------------------------------
  
  BOOL CMainFrame::OnUseEditHighlightBool(UINT cmdid)
    {
    OnUseEditHighlight(cmdid);
    return TRUE;
    }
  
  //--------------------------------------------------
  
  BOOL CMainFrame::OnUseEditCondBool(UINT cmdid)
    {
    OnUseEditCond(cmdid);
    return TRUE;
    }
  
  //--------------------------------------------------
  
  void CMainFrame::OnThreadsAdd()
    {
    CNewsAccount *acc=acclist.GetAccountEx(lefttree.GetSelectedItem());
    if (acc==NULL) return;
    DlgEditRules dlg(acc->GetRules());
    if (dlg.DoModal()==IDOK)
    {
      SetCursor(::LoadCursor(0,IDC_WAIT));
      acc->GetRules()=dlg.GetRules();
      acc->ApplyCond(true);
      acc->ApplyHighlight(true); 
      acc->PostApplyConds();
      LoadFiltersToCombo(acc);
    }
/*    CEditRulesDlg dlg;
    dlg.rules=acc->GetKeywords();
    if (dlg.DoModal()==IDOK)
      {
      SetCursor(::LoadCursor(NULL,IDC_WAIT));
      acc->SetKeywords(dlg.rules);
      acc->ApplyCond();
      meslist.UpdateList();
      LoadFiltersToCombo(acc);
      }
      */
    }
  
  //--------------------------------------------------
  
  void CMainFrame::OnAccountRepair()
    {
    HTREEITEM it=lefttree.GetSelectedItem();  
    CNewsAccount *acc=acclist.GetAccountEx(it);
    if (acc==NULL) return;
    CNewsGroup *grp=acclist.GetGroup(it);
    meslist.LoadList(NULL,NULL);
    SetCursor(::LoadCursor(NULL,IDC_WAIT));
    ::DogShowStatusV(IDS_REPAIRING);
    dogwnd.Invalidate(FALSE);
    dogwnd.UpdateWindow();
    acc->Repair();
    if (grp) meslist.LoadList(acc,grp);
    }
  
  //--------------------------------------------------
  
  void CMainFrame::OnSetThreadProp()
    {
    CNewsArtHolder *hld=meslist.GetCurMessage();
    CNewsAccount *acc=acclist.GetAccountEx(lefttree.GetSelectedItem());
    if (hld==NULL) return;
    if (acc==NULL) return;
    CString thp=hld->GetThreadProperties(false);
    thrdpropdlg.properties=thp;
    if (thrdpropdlg.DoModal()==IDOK && stricmp(thp,thrdpropdlg.properties))
      {  
      CSendDlg *dlg=new CSendDlg();
      dlg->from=CString("\"")+acc->GetMyName()+"\" <"+acc->GetMyEmail()+">";
      dlg->SetNewsTarget(meslist.GetCurAccount(),meslist.GetCurGroup());
      dlg->thrdprop=thrdpropdlg.properties;
      AddReSubject(hld->subject,dlg->subject);
      if (hld!=NULL) dlg->ref=hld->messid;	
      dlg->Create();
      dlg->thrdprop="";
      if  (thrdpropdlg.comment==FALSE) dlg->PostMessage(WM_COMMAND,IDC_SEND);
      }
    }
  
  //--------------------------------------------------
  
  // return date only from the time and date
  static COleDateTime DateOnly(COleDateTime date)
    {
    COleDateTime dateOnly;
    dateOnly.SetDate(date.GetYear(),date.GetMonth(),date.GetDay());
    return dateOnly;
    }
  
  //--------------------------------------------------
  
  // return time only from the time and date
  
  static COleDateTime TimeOnly(COleDateTime date)
    {
    COleDateTime timeOnly;
    timeOnly.SetTime(date.GetHour(),date.GetMinute(),date.GetSecond());
    return timeOnly;
    }
  
  //--------------------------------------------------
  
  static COleDateTime DateAndTime(COleDateTime date, COleDateTime time)
    {
    COleDateTime time0;
    time0.SetTime(0,0,0);
    return date + (time-time0);
    }
  
  //--------------------------------------------------
  
  void CMainFrame::OnAccountSetLastRead()
    {
    CLastReadDlg dlg;
    CNewsAccount *acc=acclist.GetAccountEx(lefttree.GetSelectedItem());
    if (acc==NULL) return ;
    dlg.date=acc->GetMyLastMessageTime();
    if (dlg.date.GetStatus()!=COleDateTime::valid)
      {
      dlg.date=acc->lastread;
      if (dlg.date.GetStatus()!=COleDateTime::valid)
        {
        dlg.date = COleDateTime::GetCurrentTime();
        }
      }
    dlg.date = dlg.date + theApp.timezone;
    
    dlg.time = TimeOnly(dlg.date);
    dlg.date = DateOnly(dlg.date);
    if (dlg.DoModal()==IDOK)
      {
      COleDateTime datetime = DateAndTime(dlg.date,dlg.time);
      // read both date and time
      datetime = datetime - theApp.timezone;
      acc->ChangeLastRead(datetime);
      }
    meslist.UpdateList();
	if (notify.IsEnabled()) notify.CheckAfterCatchUp();
    }
  
  //--------------------------------------------------
  
  void CMainFrame::OnAccountSetLastReadSmart()
    {
    CNewsAccount *acc=acclist.GetAccountEx(lefttree.GetSelectedItem());
    if (acc==NULL) return ;
    /*
    acc->ChangeLastReadSmartPerGroup();
    meslist.UpdateList();	
    */
    COleDateTime lastReadTime = acc->GetMyLastMessageTime();
    acc->ChangeLastRead(lastReadTime);
    meslist.UpdateList();	
	if (notify.IsEnabled()) notify.CheckAfterCatchUp();
    }
  
  //--------------------------------------------------
  
  void CMainFrame::OnItemExpanding(NMHDR* pNMHDR, LRESULT* pResult)
    {
    NMTREEVIEW * pNMTreeView = (NM_TREEVIEW*)pNMHDR;
    if (pNMTreeView ->action==TVE_EXPAND)
      {
      CNewsAccount *acc=acclist.GetAccount(pNMTreeView->itemNew.hItem);
      if (acc) 
        if (acclist.EnsureLoaded(acc)==false) {*pResult=-1;return;}
      }
    *pResult=0;
    }
  
  //--------------------------------------------------
  
  void CMainFrame::OnAccountsUnload()
    {
    HTREEITEM it=lefttree.GetSelectedItem();  
    CNewsAccount *acc=acclist.GetAccountEx(it);
    if (acc)
      {
      it=acc->treeitem;
	  CNewsTerminal::StopTrafic();
      SetCursor(::LoadCursor(NULL,IDC_WAIT));
      if (theApp.CleanThreads(5000)==false || acc->mutex.Lock(10000)==FALSE )
        {
        NrMessageBox(IDS_LOCKFAILED);
        return;
        }
      if (meslist.GetCurAccount()==acc)	  
        meslist.LoadList(NULL,NULL,false);	  
      if (notify.IsEnabled()) notify.Reset();
      acclist.AccountStreaming(it,true);
      lefttree.SelectItem(it);
      lefttree.Expand(it,TVE_COLLAPSE);
      UpdateWindow();
      acc->Unload();
      acclist.AlterAccount(it,true);
      acc->mutex.Unlock();
      memcache.FlushCache();
      }
    }
  
  //--------------------------------------------------
  
  void CMainFrame::OnCheckfornewgroups()
    {
    HTREEITEM it=lefttree.GetSelectedItem();  
    CNewsAccount *acc=acclist.GetAccountEx(it);
    if (acc)
      {
      acc->SetForceDownload(true);
      CheckNewGroups(true, false);
      }	
    }
  
  //--------------------------------------------------
  
  void CMainFrame::OnUpdateCheckfornewgroups(CCmdUI* pCmdUI)
    {
    HTREEITEM it=lefttree.GetSelectedItem();  
    CNewsAccount *acc=acclist.GetAccountEx(it);
    if (acc)
      {
      pCmdUI->Enable(acc->IsLoadOnDemand() && !cleardog);
      pCmdUI->SetCheck(acc->IsDownloadForced());
      }
    else
      pCmdUI->Enable(false);
    
    }
  
  //--------------------------------------------------
  
  void CMainFrame::OnGroupsNotify()
    {
    CNewsGroup *grp=acclist.GetGroup(lefttree.GetSelectedItem());
    if (grp) grp->notify=!grp->notify;
    acclist.AlterGroup(lefttree.GetSelectedItem());
    }
  
  //--------------------------------------------------
  
  void CMainFrame::OnUpdateGroupsNotify(CCmdUI* pCmdUI)
    {
    CNewsGroup *grp=acclist.GetGroup(lefttree.GetSelectedItem());
    if (grp) pCmdUI->SetCheck(grp->notify);
    }
  
  //--------------------------------------------------
  
  void CMainFrame::OnMessagesNextNew()
  {
	int cnt=2;
    CNewsGroup *grp=acclist.GetGroup(lefttree.GetSelectedItem());
	do
	{
	  if (grp==NULL)
		{
		CNewsAccount *acc=acclist.GetAccountEx(lefttree.GetSelectedItem());
		if (acc==NULL) return;
		grp=acc->GetGroups();	
		}
	  else if (grp->GetNews())
		{
		CNewsArtHolder *hld=meslist.GetCurMessage();
		meslist.SelectNewestMsg();
		CNewsArtHolder *hld2=meslist.GetCurMessage();
		if (hld2 && hld!=hld2) return;
		grp=grp->GetNext();
		}
	  while (grp)
		{
		if (grp->sign && grp->GetNews())
		  {
		  HTREEITEM it=acclist.FindItem(NULL,(LPARAM)(grp));
		  lefttree.SelectItem(it);
		  return;
		  }
		grp=grp->GetNext();	  
		}	
	  cnt--;
	}while (cnt);

  }
  
  //--------------------------------------------------
  
  void CMainFrame::LoadFiltersToCombo(CComboBox &combo, CNewsAccount *acc, int kwpos)
    {
    combo.ResetContent();
    CString s;
    s.LoadString(IDS_NOFILTER);
    combo.AddString(s);
    combo.SetItemData(0,-1);
    if (acc==NULL) 
      {
      combo.SetCurSel(0);
      combo.EnableWindow(FALSE);
      return;
      }
    combo.EnableWindow(TRUE);;
    ThreadRules &rules=acc->GetRules();
    for (int i=rules.GetRuleIdMin();i<=rules.GetRuleIdMax();i++)
    {
      int p=combo.AddString(rules.GetRule(i).GetName());
      combo.SetItemData(p,i);	
    }
    for (int p=0,cnt=combo.GetCount();p<=cnt;p++)
      {
      if (combo.GetItemData(p)==(DWORD)kwpos) 
        {
        combo.SetCurSel(p);break;
        }
      }
    }
  
  //--------------------------------------------------
  
  void CMainFrame::LoadFiltersToCombo(CNewsAccount *acc)
    {
    LoadFiltersToCombo(wFilters,acc,acc ? acc->GetCurFilter() : ACC_NOFILTER);
    LoadFiltersToCombo(wHighlights,acc,acc ? acc->GetCurHighlight() :ACC_NOFILTER);
    }
  
  //--------------------------------------------------
  
  void CMainFrame::OnFilterChange()
    {
    CNewsAccount *acc=acclist.GetAccountEx(lefttree.GetSelectedItem());
    if (acc==NULL) return;
    int pt=wFilters.GetItemData(wFilters.GetCurSel());
    acc->GetRules().SetFilter(pt);
    acc->ApplyCond(true);  
    acclist.AlterAllGroups(acc);
    meslist.UpdateList(); 
    }
  
  //--------------------------------------------------
  
  void CMainFrame::OnFilterOpen()
    {
    wFilters.SetFocus();
    wFilters.ShowDropDown(TRUE);
    }
  
  //--------------------------------------------------
  
  void CMainFrame::OnHighlightChange()
    {
    CNewsAccount *acc=acclist.GetAccountEx(lefttree.GetSelectedItem());
    if (acc==NULL) return;
    int pt=wFilters.GetItemData(wHighlights.GetCurSel());
    acc->GetRules().SetHighlight(pt);
    acc->ApplyHighlight(true);  
    acc->PostApplyConds(NULL);
    acclist.AlterAllGroups(acc);
    
    // update message list - includes sorting if necessary
    meslist.UpdateList(); 
    meslist.Invalidate();
    }
  
  //--------------------------------------------------
  
  void CMainFrame::OnHighlightOpen()
    {
    wHighlights.SetFocus();
    wHighlights.ShowDropDown(TRUE);
    }
  
  //--------------------------------------------------
  
  void CMainFrame::OnSelectionSaveMessages()
    {
    CNewsTerminal nterm;
    bool ntermconn=false;
    char *buff=NULL;
    CString frm;
    CString eml;
    CNewsAccount *acc=meslist.GetCurAccount();
    CNewsGroup *grp=meslist.GetCurGroup();
    
    EnableWindow(false);
    SetCursor(LoadCursor(NULL,IDC_WAIT));
    DogSetMax(meslist.GetSelectedCount());
    int selcnt=0;
    DWORD sz=GetTempPath(0,buff);
    buff=(char *)alloca(sz+50);
    GetTempPath(sz,buff);
    strcat(buff,"\\~NRExportedMessages.html");
    ofstream str(buff,ios::out|ios::trunc);
    str<<"<html><head><meta http-equiv=\"Content-Type\" content=\"text/html; charset=windows-1250\"></title></head>";
    str<<"<body>";
    POSITION pos=meslist.GetFirstSelectedItemPosition();
    while (pos)
      {
      int i=meslist.GetNextSelectedItem(pos);
      CNewsArtHolder *hld=(CNewsArtHolder *)meslist.GetItemData(i),*q=hld,*e=hld->EnumNextNoChild();
      while (q && e!=q)
        {
        GetNameFrom(q->from,frm);
        GetEmailFromSender(q->from,eml);
        str<<"<table ";
        str<<" width=100% border=1 cellspacing=0 cellpadding=3><tr><td>From: <B>";
        str<< frm <<" &lt;"<<eml<<"&gt;"<<"</B><BR>";
        str<<"Subject: <B>" << q->GetSubject() <<"</B><br>";
        str<<"Posted: " << (q->date+theApp.timezone).Format("%A, %d. %B %Y %H:%M:%S");
        str<<"</table>";
        bool ok=true;
        if (q->GetArticle()==NULL)
          {
          if (q->LoadFromCache(acc->cache)==false)
            {
            if (ntermconn==false)
              {
              if (acc->OpenConnection(nterm)!=0) ok=false;
              else if (grp->SetActiveGroup(nterm)==false) ok=false;
              ntermconn=ok;
              }
            if (ok && q->ReloadArticle(nterm,NULL,false)) ok=false;
            }
          }
        if (!q->IsTrash() && ok)
          {
          glob_lock.Lock();
          PMessagePart msgtext=q->GetArticle();
          CMessagePart *part=msgtext;
          while (part)
            {
            if (part->GetType()==CMessagePart::text)
              {
              str<<"<table border=0 cellspacing=10><tr><td>";
              //			theApp.config.dispprop.CreateFontBeginSeq(frm);str<<frm;
              const char *dd=part->GetData();
              if (dd==NULL) dd="Internal error!";
              istrstream in((char *)dd);
              Text2Html(in,str,&(acc->msgidlist));
              //			theApp.config.dispprop.CreateFontEndSeq(frm);
              str<<frm;
              str<<"</td></tr></table>";
              }
            else
              {
              char *ext=(char *)part->GetFilename();
              if (ext!=NULL) ext=strrchr(ext,'.');
              if (ext!=NULL) 
                {
                ext++;
                if (theApp.config.shwhtml && (stricmp(ext,"HTM")==0 || stricmp(ext,"HTML")==0 || stricmp(ext,"SHTML")==0 || stricmp(ext,"MHT")==0))
                  str<<"<p align=center><IFRAME SRC=\""<<part->CreateAsFile(q)<<"\" width=\"80%\" height=\"80%\"></IFRAME></p>";
                if (stricmp(ext,"JPG") && stricmp(ext,"JPEG") && stricmp(ext,"GIF") && stricmp(ext,"BMP") &&
                  stricmp(ext,"PNG")) ext=NULL;
                if (!theApp.config.shwimgs) ext=NULL;
                }
              if (ext!=NULL)		  
                str<<"<p align=center><IMG SRC=\""<<part->CreateAsFile(q)<<"\"></p>";
              else
                str<<"<p align=center><a href=\""<<part->CreateAsFile(q)<<"\" target=_blank>"<<(part->GetFilename()==NULL?"Article text":part->GetFilename())<<"</a></p>";
              }
            part=part->GetNext();
            if (part) str<<"<hr width=\"50%\">";
            }
          }
        glob_lock.Unlock();		
        if (hld->GetOpened()) q=q->GetNext();else q=q->EnumNext();
        }
      selcnt++;
      DogSetPos(selcnt);
      }
    str<<"</body></html>";
    str.close();
    EnableWindow(true);
    ShellExecute(*this,NULL,buff,NULL,NULL,SW_SHOW);
    }
    
    //--------------------------------------------------
    
    LRESULT CMainFrame::OnNrmDownloadComplette(WPARAM wParam,LPARAM lParam)
      {
      CArticleDownloadControl::EDownloadResult result=(CArticleDownloadControl::EDownloadResult)wParam;
      CNewsArtHolder *hld=(CNewsArtHolder *)lParam;
      theApp.downloadCtrl.NotifyBrowsers(hld,result);
      return 0;
      }
    
    //--------------------------------------------------
    
    void CMainFrame::OnUpdateGroupsSubscribe(CCmdUI* pCmdUI)
      {
      HTREEITEM it=lefttree.GetSelectedItem();
      CNewsGroup *grp=acclist.GetGroup(it);
      pCmdUI->Enable(grp && !grp->sign);
      }
    
    //--------------------------------------------------
    
    void CMainFrame::OnGroupsSubscribe()
      {
      HTREEITEM it=lefttree.GetSelectedItem();
      CNewsGroup *grp=acclist.GetGroup(it);
      grp->sign=true;
      }
    
    //--------------------------------------------------
    
	void CMainFrame::OnListpopupEdittext()
    {
	  CNewsArtHolder *hld=meslist.GetCurMessage();
	  CNewsGroup *grp=meslist.GetCurGroup();
	  if (grp==NULL) return;
	  if (grp->special)
      {
		OnGotoContext();
		grp=meslist.GetCurGroup();
		if (grp==NULL || grp->special) return;
      }      
	  CNewsAccount *acc=acclist.GetAccountEx(lefttree.GetSelectedItem());
	  CString email;
	  ::GetEmailFromSender(hld->from,email);
	  if (stricmp(email,acc->GetMyEmail())!=0)
	  {
		NrMessageBox(IDS_EDITACCESSDENIED,MB_OK);
		return;
	  }
	  if (acc==NULL || hld==NULL) return;
	  if (hld->GetChild() && AfxMessageBox(IDS_EDITWARNING,MB_YESNO|MB_ICONEXCLAMATION|MB_DEFBUTTON2)==IDNO) return;      
	  CSendDlg *dlg=new CSendDlg();
	  dlg->from=CString("\"")+acc->GetMyName()+"\" <"+acc->GetMyEmail()+">";
	  dlg->SetNewsTarget(meslist.GetCurAccount(),meslist.GetCurGroup());
	  dlg->subject=hld->GetSubject();
    dlg->wikiLink=hld->XWikiLink;
	  PMessagePart msgtext=hld->GetArticle();
	  CMessagePart *part=msgtext;
	  if (part==NULL) hld->LoadFromCache(acc->cache);
	  part=msgtext=hld->GetArticle();
	  int prop=hld->subject.Find(THREADPROPMARK);
	  dlg->thrdprop=prop==-1?"":((LPCTSTR)(hld->subject)+prop+strlen(THREADPROPMARK));
	  dlg->msgbody="";	
	  if (strncmp(hld->content,"multipart",9))
		while (part) 
		{
		  if (part->GetType()==CMessagePart::text)
			dlg->msgbody+=CString(part->GetData());
		  part=part->GetNext();
		}
	  dlg->ref=hld->refid;
	  dlg->cancelid=hld->messid;
	  dlg->Create();
	  dlg->thrdprop="";	
	  part=msgtext;
	  while (part)
	  {
	    if (part->GetType()==CMessagePart::binary_uue)
	  	  dlg->AddToAttachList(part->CreateAsFile(hld));
	    part=part->GetNext();
	  }
	  dlg->lockedPart=msgtext;
    }


void CMainFrame::OnActivate(UINT nState, CWnd* pWndOther, BOOL bMinimized) 
{
	CFrameWnd::OnActivate(nState, pWndOther, bMinimized);
	if (nState==WA_ACTIVE || nState==WA_CLICKACTIVE)
	  {
	  HTREEITEM it;
	  if (acclist.GetTree().GetSafeHwnd()!=NULL)
		for (it=NULL;it=acclist.EnumAccounts(it);)
		  {
		  CNewsAccount *acc=acclist.GetAccount(it);
		  acc->ResetSaveState();
		  }
	  }
	
	// TODO: Add your message handler code here
	
}

void CMainFrame::OnCompletteThread()
  {
  CNewsAccount *acc=meslist.GetCurAccount();
  CNewsGroup *grp=meslist.GetCurGroup();
  CNewsArtHolder *hld=meslist.GetCurMessage();
  if (acc==NULL || grp==NULL || hld==NULL) return;
  if (theApp.OfflineWarn())
    {
    CCompletteThreadDlg dlg;
    while (hld->GetParent()) hld=hld->GetParent();
    dlg.acc=acc;
    dlg.grp=grp;
    dlg.beg=hld;
    dlg.DoModal();
    meslist.UpdateList();
    }
  }

void CMainFrame::OnExportParamFile()
  {
  NrMessageBox("Function is turned off",MB_OK);
/*  CNewsAccount *acc=meslist.GetCurAccount();
  CFileDialog fdlg(FALSE,".txt");
  if (fdlg.DoModal()==IDOK)
    {
    ParamFile paramf;
    ArchiveParamFile arch(&paramf,true); 
    acc->Streaming(arch);
    paramf.Save(fdlg.GetPathName());
    }*/
  }

void CMainFrame::OnViewMessageSource()
  {
  CNewsAccount *acc=meslist.GetCurAccount();
  CNewsArtHolder *hld=meslist.GetCurMessage();
  if (acc==NULL) return;
//  acc->fulltext.ProcessText(hld->GetArticle()->GetData(),hld->messid);
  CNewsTerminal term;
  if (term.ShowError(acc->OpenConnection(term))) return;
  term.SendFormatted("ARTICLE %s",hld->messid);
  if (term.ReceiveNumReply()!=220) 
	{
	term.ShowError(999);
	return;
	}
  CDlgViewSource dlg;
  dlg.lterm=&term;
  dlg.DoModal();
  term.Quit();
  }

void CMainFrame::OnViewDeletedMessages()
  {
  meslist.hidetrash=!meslist.hidetrash;
  meslist.UpdateList();
  }

void CMainFrame::OnUpdateViewDeletedMessages(CCmdUI* pCmdUI)
  {
  pCmdUI->SetCheck(!meslist.hidetrash);
  }


bool CMainFrame::GotoNewsShortcut(const char *shortcut,CString *message)
  {  
  char *msgid=(char *)alloca(strlen(shortcut)+1);
  strcpy(msgid,shortcut);
  if (strncmp(msgid,"news:",5)) return false;
  if (strncmp(msgid+5,"//",2)==0) strcpy(msgid+5,msgid+7);
  while (msgid[strlen(msgid)-1]=='/')
    msgid[strlen(msgid)-1]=0;
  sprintf(msgid,"<%s>",msgid+5);
  for (HTREEITEM item=NULL;item=acclist.EnumAccounts(item);)
    {
    CNewsAccount *acc=acclist.GetAccount(item);
    if (acc->IsLoaded())
      {
      if (acc->msgidlist.IsEmpty()) acc->BuildHash();
      if (acc->msgidlist.Find(msgid)!=NULL)
        {
        if (acclist.GetAccountEx(lefttree.GetSelectedItem())!=acc)
          lefttree.SelectItem(item);
//        msgid[strlen(msgid)-1]=0;
//        GotoMessageId(CString(msgid+1));
        GotoMessageId(msgid);
        BringWindowToTop();
        return true;
        }
      }
    }
  CDialog dlg;
  dlg.Create(IDD_WAITRETRIEVE,this);
  dlg.ShowWindow(SW_SHOW);
  dlg.SetActiveWindow();
  dlg.CenterWindow();
  dlg.UpdateWindow();
  for (HTREEITEM item=NULL;item=acclist.EnumAccounts(item);)
  {	
	  CNewsAccount *acc=acclist.GetAccount(item);
	  CNewsTerminal term;
	  if (acc->OpenConnection(term)==0)
	  {
		term.SendFormatted("STAT %s",msgid);
		if (term.ReceiveNumReply()/10==22)
		{
		  acclist.EnsureLoaded(acc);
          if (acclist.GetAccountEx(lefttree.GetSelectedItem())!=acc)
	          lefttree.SelectItem(item);
		  dlg.DestroyWindow();
		  GotoMessageId(msgid);
		  BringWindowToTop();
		  return true;
		}
    else
    {
      if (message)
      {
        (*message)+=CString("\r\n\t")+acc->GetAccName()+": "+term.GetLastMessage();
      }
    }
	  }
  }
  return false;
  }


LRESULT CMainFrame::OnGotoMessageShortcut(WPARAM wParam, LPARAM lParam)
  {
  HANDLE mem=(HANDLE)lParam;
  HANDLE event=(HANDLE)wParam;
  const char *shortcut=(const char *)MapViewOfFile(mem,FILE_MAP_READ,0,0,0);
  if (shortcut)
    {
      CString message;
    if (GotoNewsShortcut(shortcut,&message)==false)
    {
      CString msg;
      AfxFormatString1(msg,IDS_ARTICLEHASNOTBEENFOUND,message);
      NrMessageBox(msg,MB_OK);
    }
    UnmapViewOfFile((LPCVOID)shortcut);
    }
  SetEvent(event);
  CloseHandle(event);
  CloseHandle(mem);
  return 0;
  }

#define MESSENGERWINDOWNAME "BISMessangerSocketWindow"
static int messengerRun=-1;
#define KM_REPLYTOKECAL (WM_APP+6) //LPARAM a WPARAM parametry pro winSharedMemory

void CMainFrame::OnMessagesReplyToKecal()
  {
  HWND messenger=::FindWindow(MESSENGERWINDOWNAME,NULL);
    if (messenger) 
      {
      CNewsArtHolder *toreply=meslist.GetCurMessage();
      if (toreply)
        {
        ostrstream outdata;
        outdata.write(toreply->from,toreply->from.GetLength()+1);
        CString message=toreply->subject+" news:"+toreply->messid.Mid(1,toreply->messid.GetLength()-2);
        outdata.write(message,message.GetLength()+1);
        bool shift=(GetKeyState(VK_SHIFT) & 0x80)!=0;
        outdata.write((char *)&shift,1);
        WinSharedMemory sharedMem;
        if (sharedMem.SetTarget(WinS_HWNDTarget(messenger)) && 
          sharedMem.SendPackage(KM_REPLYTOKECAL,outdata.str(),outdata.pcount())) return;          
        }
      }
   AfxMessageBox(IDS_SENDTOKECALFAILED);
   messengerRun=-1;
  }

void CMainFrame::OnUpdateMessagesReplyToKecal(CCmdUI* pCmdUI)
  {
  if (meslist.GetCurMessage()==NULL) 
    {
    pCmdUI->Enable(FALSE);
    return;
    }
  if (pCmdUI->m_pMenu!=NULL) messengerRun=-1;
  if (messengerRun==-1)    
    messengerRun=FindWindow(MESSENGERWINDOWNAME,NULL)!=NULL;
  pCmdUI->Enable(messengerRun);
  }

void CMainFrame::OnQFFindNow()
  {
  COleDateTime mintim=COleDateTime::GetCurrentTime();  
  if (fnddlg.GetSafeHwnd()==NULL)
    {
    OnGroupsFind() ;
    fnddlg.ShowWindow(SW_HIDE);
    }
  fnddlg.vCheckDateNewest=FALSE;
  fnddlg.vCheckDateOldest=FALSE;
  fnddlg.from=fnddlg.subject=fnddlg.message="";  
  fnddlg.resInGrp=TRUE;
  if (!m_wndFindBar.IsDlgButtonChecked(IDC_QF_INFINITE))
    {
    if (m_wndFindBar.IsDlgButtonChecked(IDC_QF_1MONTH))
      mintim-=COleDateTimeSpan(31,0,0,0);
    else if (m_wndFindBar.IsDlgButtonChecked(IDC_QF_3MONTHS))
      mintim-=COleDateTimeSpan(92,0,0,0);
    else if(m_wndFindBar.IsDlgButtonChecked(IDC_QF_6MONTHS))
      mintim-=COleDateTimeSpan(366/2,0,0,0);
    else if (m_wndFindBar.IsDlgButtonChecked(IDC_QF_12MONTHS))
      mintim-=COleDateTimeSpan(366,0,0,0);    
    fnddlg.vDateOldest=mintim;
	fnddlg.vCheckDateOldest=TRUE;
    }
  fnddlg.allgroups=false;
  fnddlg.connserver=m_wndFindBar.IsDlgButtonChecked(IDC_QF_SERVER)!=0;
  if (m_wndFindBar.IsDlgButtonChecked(IDC_QF_FROM)!=0)
    m_wndFindBar.GetDlgItemText(IDC_QF_EDIT,fnddlg.from);
  if (m_wndFindBar.IsDlgButtonChecked(IDC_QF_SUBJECT)!=0)
    m_wndFindBar.GetDlgItemText(IDC_QF_EDIT,fnddlg.subject);
  if (m_wndFindBar.IsDlgButtonChecked(IDC_QF_MESSAGE)!=0)
    m_wndFindBar.GetDlgItemText(IDC_QF_EDIT,fnddlg.message);
  fnddlg.ext_minScore=0.75f;
  fnddlg.FindExternalCall();
  MSG msg;
  while (PeekMessage(&msg,0,0,0,PM_REMOVE)) DispatchMessage(&msg);
  meslist.SetFocus();
  }

void CMainFrame::OnQFStop()
  {
  if (fnddlg.IsBusy()) fnddlg.PostMessage(WM_COMMAND,IDC_STOPFIND);
  }

void CMainFrame::OnUpdateQFFindNow(CCmdUI* pCmdUI)
  {
  _findBusy=fnddlg.IsBusy();
  if (!_findBusy)
    {
    this->m_wndFindBar.GetDlgItem(pCmdUI->m_nID)->ShowWindow(SW_SHOW);
    }
  else
    this->m_wndFindBar.GetDlgItem(pCmdUI->m_nID)->ShowWindow(SW_HIDE);
  pCmdUI->Enable(lefttree.GetSelectedItem()!=NULL);
  }

void CMainFrame::OnUpdateQFStop(CCmdUI* pCmdUI)
  {
  if (_findBusy)
    {
    this->m_wndFindBar.GetDlgItem(pCmdUI->m_nID)->ShowWindow(SW_SHOW);
    }
  else
    this->m_wndFindBar.GetDlgItem(pCmdUI->m_nID)->ShowWindow(SW_HIDE);  
  }
  
void CMainFrame::EnterPress()
  {
  if (GetFocus()->GetDlgCtrlID()==IDC_QF_EDIT)
    OnQFFindNow();
  else if (GetFocus()==&meslist)
    {
    CNewsGroup *grp=meslist.GetCurGroup();
    if (grp && grp->special)
      {
      OnQFCancel();
      }
    }
  }

void CMainFrame::OnHotkeyF7()
  {
  CWnd *focus=GetFocus();
    if (focus && focus->GetParent()==&m_wndFindBar)
      {
      meslist.SetFocus();
      ShowControlBar(&m_wndFindBar,FALSE,TRUE);
      }
    else
      {
      ShowControlBar(&m_wndFindBar,TRUE,TRUE);
      this->m_wndFindBar.GetDlgItem(IDC_QF_EDIT)->SetFocus();
      this->m_wndFindBar.GetDlgItem(IDC_QF_EDIT)->SendMessage(EM_SETSEL,0,-1);
      }
  }

void CMainFrame::OnQFCancel()
  {
  if (fnddlg.IsBusy())
    {
    fnddlg.PostMessage(WM_COMMAND,IDC_STOPFIND);
    }
  OnGotoContext();
  m_wndFindBar.SetDlgItemText(IDC_QF_EDIT,"");
  }
  
void CMainFrame::OnUpdateQFCancel(CCmdUI* pCmdUI)
  {
  CNewsGroup *grp=meslist.GetCurGroup();
  pCmdUI->Enable(grp && grp->special);
  }

void CMainFrame::OnGotoContext()
  {
  CNewsArtHolder *hld=meslist.GetCurMessage();
  CNewsGroup *grp=acclist.GetGroup(lefttree.GetSelectedItem());
  CNewsAccount *acc=acclist.GetAccountEx(lefttree.GetSelectedItem());
  if (hld)
    {
    this->GotoMessageId(hld->messid,true);
    }  
  else    
    meslist.LoadList(acc,grp);    
  }
void CMainFrame::OnUpdateGotoContext(CCmdUI* pCmdUI)
  {
  CNewsGroup *grp=meslist.GetCurGroup();
  pCmdUI->Enable(grp && grp->special);
  }

void CMainFrame::OnFilterMyTasks()
  {
  CNewsAccount *acc=acclist.GetAccountEx(lefttree.GetSelectedItem());
  if (acc==NULL) return;
  CNewsGroup *grp=acc->GetGroups();
  while (grp && stricmp(grp->GetName(),"My Tasks"))
    {
    if (grp->GetNext()==NULL)
      {
      CNewsGroup *mytsk=new CNewsGroup("My Tasks",0,0);
      mytsk->display=mytsk->deleted=mytsk->special=true;
      grp->SetNext(mytsk);      
      }
    grp=grp->GetNext();
    }
  if (grp==NULL) return;
  CNewsGroup *mytaskgrp=grp;
  grp->display=grp->deleted=grp->special=true;
  acc->GetGroups()->LoadToTree(lefttree,acc->treeitem,acc->viewastree);
  lefttree.SelectItem(NULL);
  if (mytaskgrp->GetList()) mytaskgrp->GetList()->Release();
  mytaskgrp->SetList(NULL);
  grp=acc->GetGroups();
  while (grp) 
    {
    if (grp->sign)
      {
      CNewsArtHolder *hld=grp->GetList();
      while (hld)
        {
        if (!hld->GetHidden() && hld->GetHighlighted())
          {
          CNewsArtHolder *ptr=new CNewsArtHolder(*hld,false,NULL);
          ptr->SetNext(mytaskgrp->GetList());
          ptr->SetIndex(0);
          mytaskgrp->SetList(ptr);
          }    
        hld=hld->GetNext();
        }
      }
    grp=grp->GetNext();
    }
  mytaskgrp->SortItems(-1);
  lefttree.SelectItem(mytaskgrp->iteminlist);
  meslist.LoadList(NULL,NULL,false);
  meslist.LoadList(acc,mytaskgrp,true);
  }

void CMainFrame::OnAccountsDownladAllArticles()
{
     CNewsAccount *acc=meslist.GetCurAccount();
     if (acc)
     {
     acc->DownloadAllArticles();
     OnAccountsSynchronize(); 
     }
}

void CMainFrame::OnGroupsDownladAllArticles()
{
     CNewsGroup *grp=meslist.GetCurGroup();
     if (grp)
     {
     grp->_downloadall=!grp->_downloadall;
     chkgroup=NULL;
     CheckNewGroups(false,true);
     }
}


void CMainFrame::OnFlushMemoryCache()
{
  memcache.FlushCache();
}

void CMainFrame::OnAutodeleteThreads()
{
  CAutoDeleteDlg dlg;
  dlg.vAcc=acclist.GetAccountEx(lefttree.GetSelectedItem());
  if (dlg.vAcc)
  {
    dlg.DoModal();
  }
}

BOOL CMainFrame::OnGroupsFindSimilar(UINT cmd)
{
   CNewsArtHolder *hld=meslist.GetCurMessage();
	if (hld==0) return TRUE;
    CNewsAccount *acc=acclist.GetAccountEx(lefttree.GetSelectedItem());
    if (acc==0) return TRUE;
	CString search=hld->GetSubject();	
	int pos=search.Find(THREADPROPMARK);
	if (pos!=-1) 
		search.Delete(pos,search.GetLength()-pos);
	search.TrimRight();
	search.TrimLeft();
	bool space=true;
	for (int i=0;i<search.GetLength();i++)
	{
		int c=(unsigned char) search[i];
		if (!(c>='0' && c<='9' || c>='a' && c<='z' || c>='A' && c<='Z' || c>=128))
			c=32;
		if (c==32)
			if (space) {search.Delete(i);i--;}
			else
				{
				search.SetAt(i,c);
				space=true;
			}
		else
			space=false;
	}
	if (cmd==ID_GROUPS_FINDSIMILAR2)
	{
		m_wndFindBar.SetDlgItemText(IDC_QF_EDIT,search);
		OnQFFindNow();		
	}
	else
	{	
		OnGroupsFind();
		fnddlg.message=search;
		fnddlg.subject="";
		fnddlg.from="";
		fnddlg.UpdateData(FALSE);
		fnddlg.OnStopfind();
		fnddlg.OnFindnow();	
	}
  return TRUE;
}
void CMainFrame::OnMessagesOpenrelatedurl()
{
  CNewsArtHolder *hld=meslist.GetCurMessage();
  if (hld==0) return;
  CString url=hld->GetWikiLink();
  ShellExecute(0,0,url,0,0,SW_NORMAL);
}

void CMainFrame::OnUpdateMessagesOpenrelatedurl(CCmdUI *pCmdUI)
{
  CNewsArtHolder *hld=meslist.GetCurMessage();
  pCmdUI->Enable(hld!=0 && hld->GetWikiLink().GetLength()>0);
}
