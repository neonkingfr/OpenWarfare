// NewsTerminal.cpp: implementation of the CNewsTerminal class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "newsreader.h"
#include "NewsTerminal.h"
#include "NewsAccount.h"
#include "DogWindow.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

HANDLE CNewsTerminal::StreamScan::debugout=FALSE;
bool CNewsTerminal::StreamScan::StopAll=false;
void CNewsTerminal::StopTrafic()
  {
  StreamScan::StopAll=true;
  }

long CNewsTerminal_TimeoutFunction(ArchiveStreamTCP *caller, void *context, int counter)
  {
  MSG msg;
  while (PeekMessage(&msg,NULL,WM_KEYDOWN,WM_KEYDOWN,PM_REMOVE))	
	if (msg.wParam==VK_ESCAPE) return -1;
  if (CNewsTerminal::StreamScan::StopAll) return -1;
  int max=*(int *)context;
  if (counter>max) return -1;
  return NRSMALLTIMEOUT;
  }

int CNewsTerminal::StreamScan::DataExchange(void *buffer,int maxsize)
  {
  runflag=true;
  if (StopAll) {_next.SetError(CLT_INTERRUPTED);return IsError();}
  if (debugout)
	if (_next.IsStoring()) 
	  {
	  DWORD written;
	  WriteFile(debugout,buffer,maxsize,&written,NULL);
	  _next.DataExchange(buffer,maxsize);
	  }
	else	  
	  {
	  DWORD written;
	  _next.DataExchange(buffer,maxsize);
	  WriteFile(debugout,buffer,maxsize,&written,NULL);
	  }
  else
   _next.DataExchange(buffer,maxsize); 
  if (StopAll) {_next.SetError(CLT_INTERRUPTED);return IsError();}
  return IsError();
  }


int CNewsTerminal::ReceiveNumReply()
  {
  const char *line=ReadLine();
  if (line==NULL)
	return _tcpin.IsError();
  int res=CNW_INVALIDREPLY;
  sscanf(line,"%d",&res);
  return res;
  }

int CNewsTerminal::ShowError(int result)
  {
  if (result!=0)
	{
	const char *p;
	switch (result)
	  {
	  case CLT_READTIMEOUT:p="Connection read timeout!";break;
	  case CLT_WRITETIMEOUT:p="Connection write timeout!";break;
	  case CLT_CONNECTIONRESET: p="Connection has been reset by the peer!";break;
	  case CLT_INVALIDREPLY: p="Invalid reply by remote computer.";break;
	  case CLT_CONNECTFAILED: p="Connect failed. Check address, port and try it again later. Server may be down.";break;
	  case CLT_INTERRUPTED: p="Transfer Interrupted";break;
	  case CNW_COMMANDREJECTED:p="News server rejected current command.";break;
	  case CNW_INVALIDPARAMS:p="Invalid params passed to the function";break;
	  case CNW_ACCESDENIED:p="Access has been denied. Check username or password";break;
	  case CNW_GROUPHASBEENDELETED: p="Group has been deleted";break;
	  default: p=GetLastMessage();break;
	  }
	DogShowError(p);
	}
  return result;  
  }


bool CNewsTerminal::Quit()
  {
  SendLine("QUIT");
  int p=ReceiveNumReply();
  _s.Close();
  return p/10==22;
  }

static UINT WINAPI ExitConsole(LPVOID data)
{
  CNewsTerminal::CloseDebug();
  Sleep(200);
  PostMessage(GetForegroundWindow(),WM_KEYDOWN,VK_ESCAPE,0);
  return 0;
}

static BOOL WINAPI ConsoleHandlerRoutine(DWORD code)
{
  AfxBeginThread((AFX_THREADPROC)ExitConsole,THREAD_PRIORITY_NORMAL);
  return TRUE;
}


void CNewsTerminal::OpenDebug()
  {
  AllocConsole();
  SetConsoleCtrlHandler(ConsoleHandlerRoutine,TRUE);
  StreamScan::debugout=GetStdHandle(STD_OUTPUT_HANDLE);
  }
  

void CNewsTerminal::CloseDebug()
  {
  SetConsoleCtrlHandler(ConsoleHandlerRoutine,FALSE);
  FreeConsole();
  StreamScan::debugout=NULL;
  }

bool CNewsTerminal::DebugOpened()
  {
  return StreamScan::debugout!=NULL;
  }

int CNewsTerminal::Authorize(const char *username, const char *password)
  {
  int res;
  SendFormatted("AUTHINFO USER %s",username);
  SendFormatted("AUTHINFO PASS %s",password);
  res=ReceiveNumReply();
  if (res!=381) return res;
  res=ReceiveNumReply();
  if (res!=281) return res;
  return 0;
  }

int CNewsTerminal::GetListOfGroups(CTime last, CNewsGroup *addend)
  {
  CString nln;
  DWORD tck=GetTickCount();
  DWORD cnt=0;
  int res;
  if (addend==NULL) return CNW_INVALIDPARAMS;
  nln=last.Format("%y%m%d %H%M%S");
  if (nln=="") 
	{
	last=CTime::GetCurrentTime();
	nln=last.Format("%y%m%d %H%M%S");
	}
  if (last<CTime(1981,1,1,0,0,0)) SendFormatted("LIST");else SendFormatted("NEWGROUPS %s",nln);
  res=ReceiveNumReply();
  if (res!=231 && res!=215) return CNW_COMMANDREJECTED;
  nln=ReadLine();
  while (nln!="" && nln!=".")
	{
	char buffer[512];
	int min,max;
	sscanf(nln,"%s %d %d",buffer,&max,&min);
	CNewsGroup *grp=new CNewsGroup(buffer,min-1,max);
	grp->isnew=true;
	cnt++;
	addend->SetNext(grp);
	addend=grp;
	nln=ReadLine();
	DWORD tp=GetTickCount();
    if (tp-tck>2000)
	  {
	   DogShowStatusV(IDS_CHECKNEWSCNT,cnt);
	   tck+=250;
	  }
	}
  return CLT_OK;
  }


static bool ConnectSocket(Socket s, const char *address, int port, DWORD timeout)
  {
  s.SetNonBlocking(true);
  CIPA target(address,port);
  if (theApp.proxy.sockenable)
	{
	CIPA proxy(theApp.proxy.address,theApp.proxy.port);
	s.Connect(proxy);    
	if (s.Wait(timeout,Socket::WaitWrite)!=Socket::WaitWrite) return false;
	ArchiveStreamTCPOut tcpout(s,timeout,false);
	ArchiveStreamTCPIn tcpin(s,timeout,false);
    LineTerminal ln(tcpin, tcpout);
    ln.SendFormatted("CONNECT %s:%d HTTP/1.1",address, port);
    ln.SendLine("",0);
    const char *line = ln.ReadLine();
    float protoVer;
    int status;
    if (sscanf(line,"HTTP/%g %d",&protoVer,&status) != 2) return false;

    if (status != 200) return false;
    while (line[0] != 0)
        line = ln.ReadLine();

  }
  else
	{
	s.Connect(target);
	if (s.Wait(timeout,Socket::WaitWrite)!=Socket::WaitWrite) return false;
	}
  return true;
  }

int CNewsTerminal::Connect(const char *address, int port)
  {
  if (ConnectSocket(_s,address,port,NRTIMEOUT)==false)
	{
	_s.Close()  ;
	return CLT_CONNECTFAILED;
  	}
  CNewsTerminal::StreamScan::StopAll=false;
  int p=ReceiveNumReply();
  if (p/100!=2) return p;
  return 0;
  }

/*
CNewsTerminal::CNewsTerminal()
{

}

CNewsTerminal::~CNewsTerminal()
{

}

int CNewsTerminal::Connect(const char *address, int port)
  {
  int res=CLineTerminal::Connect(address,port);
  if (res!=CLT_OK) return res;
  int p=ReceiveNumReply(line);
  if (p/100!=2) return p;
  return 0;
  }


int CNewsTerminal::GetListOfGroups(CTime last, CNewsGroup *addend)
  {
  CString nln;
  DWORD tck=GetTickCount();
  DWORD cnt=0;
  int res;
  if (addend==NULL) return CNW_INVALIDPARAMS;
  nln=last.Format("%y%m%d %H%M%S");
  if (nln=="") 
	{
	last=CTime::GetCurrentTime();
	nln=last.Format("%y%m%d %H%M%S");
	}
  if (last<CTime(1981,1,1,0,0,0)) SendFormatted("LIST");else SendFormatted("NEWGROUPS %s",nln);
  res=ReceiveNumReply(line);
  if (res!=231 && res!=215) return CNW_COMMANDREJECTED;
  ReceiveLine(nln);
  while (nln!="" && nln!=".")
	{
	char buffer[512];
	int min,max;
	sscanf(nln,"%s %d %d",buffer,&max,&min);
	CNewsGroup *grp=new CNewsGroup(buffer,min-1,max);
	grp->isnew=true;
	cnt++;
	addend->SetNext(grp);
	addend=grp;
	int q=ReceiveLine(nln);
	if (q) return q;
	DWORD tp=GetTickCount();
    if (tp-tck>2000)
	  {
	   DogShowStatusV(IDS_CHECKNEWSCNT,cnt);
	   tck+=250;
	  }
	}
  return CLT_OK;
  }

void CNewsTerminal::Close()
  {
  SendLine("QUIT");
  CLineTerminal::Close();
  }

int CNewsTerminal::ShowError(int result)
  {
  if (result!=0)
	{
	const char *p;
	switch (result)
	  {
	  case CNW_COMMANDREJECTED:p="News server rejected current command.";break;
	  case CNW_INVALIDPARAMS:p="Invalid params passed to the function";break;
	  case CNW_ACCESDENIED:p="Access has been denied. Check username or password";break;
	  case CNW_GROUPHASBEENDELETED: p="Group has been deleted";break;
	  default: return CLineTerminal::ShowError(result);break;
	  }
	DogShowError(p);
	}
  return result;
  }
*/