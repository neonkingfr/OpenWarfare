// NewsReader.h : main header file for the NEWSREADER application
//

#if !defined(AFX_NEWSREADER_H__D81270E4_B116_4A5B_91DD_31B641A210E0__INCLUDED_)
#define AFX_NEWSREADER_H__D81270E4_B116_4A5B_91DD_31B641A210E0__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#define TOTALTHREADS 3


#include "resource.h"       // main symbols
//#include "NewsReader_i.h"
#include "SendOptionsDlg.h"	// Added by ClassView
#include "ProxySetupDlg.h"	// Added by ClassView
#include "SplashWnd.h"
#include "Splash.h"
#include "CharMapCz.h"
//#include "HttpServer.h"	// Added by ClassView
#include "ArticleDownload.h"
#include "ConfigDlg.h"	// Added by ClassView

/////////////////////////////////////////////////////////////////////////////
// CNewsReaderApp:
// See NewsReader.cpp for the implementation of this class
//

class CRunner
  {
  public:
	virtual void Run()=0;
	virtual ~CRunner() {}
  };

#define BGRNCLOSED (CRunner *)1
#define BGRNCLOSEDTH (CWinThread *)1



class CNewsReaderApp : public CWinApp
{ 
	CRITICAL_SECTION sect;
	CWinThread *chkver;
	char *fullpathbuff;	
	int fpblen;
	void ToSingleMode();
	void ToMultiMode();
public:
	CImageListEx ilist;
	CNewsReaderApp();	
	CWinThread *backgrnd[TOTALTHREADS];
	CFont messfont;
	DWORD mainthread;
	COleDateTimeSpan timezone;
	CAboutWindow about;
	CSplash splashthrd;
	char appPath[MAX_PATH];
	char userPath[MAX_PATH];
	char version[MAX_PATH];
	bool useprofiles; //using profiles
	bool enable_update; //auto update is enabled
	bool profchange; //change profile settings


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CNewsReaderApp)
	public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();
	//}}AFX_VIRTUAL

// Implementation

public:
	CArticleDownloadControl downloadCtrl;

	bool CleanThreads(DWORD timeout);
	int GetRegInt(const char *name,int defval=0);
	void SetRegInt(const char *name,int val);
	CString GetVersionString();
	void SetVersionTime(const char *str);
	void GetVersionTime(CString &ver);
	const char * FullPath(const char *filename, bool apppath=false);
	void SomeChecking();
	bool EnableCheckForNewVersion();
	void CheckForNewVersion(BOOL ask);
	void UnisolateThread();
	void IsolateThread();
	bool OfflineWarn();
	bool offline;
	CProxySetupDlg proxy;
	CConfigDlg config;
	CSendOptionsDlg sendopt;
	void Lock()
	  {
	  EnterCriticalSection(&sect);
	  }
	void Unlock()
	  {
	  LeaveCriticalSection(&sect);
	  }
	bool RunBackground(int index,CRunner *run, DWORD timetowait, bool lowpriority=true);
	//{{AFX_MSG(CNewsReaderApp)
	afx_msg void OnAppAbout();
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP();
private:
	BOOL m_bATLInited;
private:
};

extern CNewsReaderApp theApp;
extern CCharMapCz czmap;
extern bool runflag;

void RelMoveWnd(CWnd *wnd, int relx, int rely);
void RelSizeWnd(CWnd *wnd, int relx, int rely);
COleDateTime ParseStringDate(const char *date);
void GetEmailFromSender(const char *name,CString& out);

void SetTooltip(CWnd *dlg,CWnd *cursorwnd);

void GetNameFrom(const CString &in, CString &out);
void GetNameFrom(const RString &in, CString &out);
void ExpandFilename(CString &filename, CString &out);
void DateToString(COleDateTime &odt,CString &s);

int NrMessageBox(const char *text,UINT type=MB_OK);
int NrMessageBox(int id,UINT type=MB_OK);

bool NrRegWindow(CWnd *wnd);
void NrUnregWindow(CWnd *wnd);
bool NrKillWindows();
void NrEnableWindows(BOOL enable);
void NrRemoveInvalidWindows();

#define ICON_NWSERVER 0
#define ICON_NWGROUP 1
#define ICON_NWEMPTY 2
#define ICON_NWFOLDER 3
#define ICON_STATEHIDE 4
#define ICON_STATEIMPORTANT 5
#define ICON_NOTINCACHE 6
#define ICON_INCACHE 7
#define ICON_ATTACHMENT 8
#define ICON_ATTACHMENTQUESTION 9
#define ICON_SENDLATER 10
#define ICON_ARCHIVE 11
#define ICON_SORT_UP 12
#define ICON_SORT_DOWN 13
#define ICON_NWGROUPMARK 14
#define ICON_INCACHEREAD 15
#define ICON_NOTINCACHEREAD 16
#define ICON_RECYCLER 17

#define NREADERWINDOW "Bredy@Newsreader@BohemiaInteractive"


#ifdef DEMO
void DemoWarn();
#endif
class CNewsTerminal;
class CNewsArtHolder;
bool CancelMessage(CNewsTerminal &term, CNewsArtHolder *msgid, const char *groups);
const char *stristr(const char *source, const char *fnd);

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_NEWSREADER_H__D81270E4_B116_4A5B_91DD_31B641A210E0__INCLUDED_)

