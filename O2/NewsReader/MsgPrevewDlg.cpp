// MsgPrevewDlg.cpp : implementation file
//

#include "stdafx.h"
#include "newsreader.h"
#include "MsgPrevewDlg.h"
#include "NewsTerminal.h"
#include "NewsArticle.h"
#include "NewsAccount.h"
#include "SendDlg.h"
#include "MessageList.h"
#include "MainFrm.h"
#include "WindowPlacement.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMsgPrevewDlg dialog

#define SECTION "PreviewDlg"

CMsgPrevewDlg::CMsgPrevewDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CMsgPrevewDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CMsgPrevewDlg)
	//}}AFX_DATA_INIT
	hld=NULL;
	acc=NULL;
}


void CMsgPrevewDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CMsgPrevewDlg)
	DDX_Control(pDX, IDC_UP, bUp);
	DDX_Control(pDX, IDC_DOWN, bDown);
	DDX_Control(pDX, IDC_SEEN, bSeen);
	DDX_Control(pDX, IDC_PREVIEWPOS, pPos);
	DDX_Control(pDX, IDC_REPLYMAIL, bReplyMail);
	DDX_Control(pDX, IDC_REPLY, bReply);
	DDX_Control(pDX, IDC_PRINT, bPrint);
	DDX_Control(pDX, IDC_GOTO, bGoto);
	DDX_Control(pDX, IDC_DELETE, bDelete);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CMsgPrevewDlg, CDialog)
	//{{AFX_MSG_MAP(CMsgPrevewDlg)
	ON_WM_SIZE()
	ON_WM_GETMINMAXINFO()
	ON_WM_TIMER()
	ON_COMMAND_EX(IDC_REPLY, OnReply)
	ON_COMMAND_EX(IDC_DELETE, OnDelete)
	ON_BN_CLICKED(IDC_PRINT, OnPrint)
	ON_BN_CLICKED(IDC_GOTO, OnGoto)
	ON_BN_CLICKED(IDC_SEEN, OnSeen)
	ON_BN_CLICKED(IDC_UP, OnUp)
	ON_COMMAND_EX(IDC_REPLYMAIL, OnReply)
	ON_BN_CLICKED(IDC_DOWN, OnDown)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMsgPrevewDlg message handlers

BOOL CMsgPrevewDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	CenterWindow();
	bReplyMail.SetIcon(theApp.LoadIcon(IDI_REPLYMAIL));
	bReply.SetIcon(theApp.LoadIcon(IDI_REPLY));
	bGoto.SetIcon(theApp.LoadIcon(IDI_GOTO));
	bPrint.SetIcon(theApp.LoadIcon(IDI_PRINT));
	bDelete.SetIcon(theApp.LoadIcon(IDI_DELETE));
	bSeen.SetIcon(theApp.LoadIcon(IDI_SEE));
	bDown.SetIcon(theApp.LoadIcon(IDI_DOWNICON));
	bUp.SetIcon(theApp.LoadIcon(IDI_UPICON));
	// TODO: Add extra initialization here
	CRect rc,rc2;
	pPos.GetWindowRect(&rc);
	ScreenToClient(&rc);
	browser.Create(AfxRegisterWndClass(0),"",WS_CHILD|WS_VISIBLE,rc,this,AFX_IDW_PANE_FIRST);
	CHTMLDocument2 doc=browser.GetHtmlDocument();
	if (doc.m_lpDispatch==NULL) browser.Navigate("about:blank",NULL,NULL,NULL,NULL);
	GetClientRect(&rc);
	dfsz=rc.Size();	
	GetWindowRect(&rc);
	browser.GetWindowRect(&rc2);
	minsz.cy=rc2.top-rc.top+20;
	bSeen.GetWindowRect(&rc2);
	minsz.cx=rc2.right-rc.left+20;
	SetTimer(2,2000,NULL);
	CWindowPlacement wp;
	wp.Load(SECTION);
	wp.Restore(*this,EWP_normal,true);
//	SetWindowPos(NULL,rc.left+(rand() & 0x7f),rc.top+(rand() & 0x7f),0,0,SWP_NOSIZE|SWP_NOZORDER);
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void CMsgPrevewDlg::PostNcDestroy() 
  {
  NrUnregWindow(this);
   if (hld) hld->Release();	
  if (acc) acc->Release();
  delete this;
  }

void CMsgPrevewDlg::OnSize(UINT nType, int cx, int cy) 
  {
  CDialog::OnSize(nType, cx, cy);
  if (nType==SIZE_MINIMIZED) return;
  if (bReply.m_hWnd==NULL) return;
  int dfx=cx-dfsz.cx;
  int dfy=cy-dfsz.cy;
  RelSizeWnd(&browser,dfx,dfy);
  dfsz.cx=cx;
  dfsz.cy=cy;
  
  // TODO: Add your message handler code here
	
  }

void CMsgPrevewDlg::OnGetMinMaxInfo(MINMAXINFO FAR* lpMMI) 
  {
  if (bReply.m_hWnd==NULL) return;
  lpMMI->ptMinTrackSize.x=minsz.cx;
  lpMMI->ptMinTrackSize.y=minsz.cy;
  CDialog::OnGetMinMaxInfo(lpMMI);
  } 

void CMsgPrevewDlg::OnTimer(UINT nIDEvent) 
  {
  CDialog::OnTimer(nIDEvent);
  if (hld && hld->IsTrash()) DestroyWindow();	
  
  }

void CMsgPrevewDlg::OnCancel()
  {
  CWindowPlacement wp;
  wp.Retrieve(*this);
  wp.Store(SECTION);
  DestroyWindow();
  }

BOOL CMsgPrevewDlg::OnReply(UINT cmd) 
  {
  CSendDlg *dlg=new CSendDlg();
  AddReSubject(hld->GetSubject(),dlg->subject);
  dlg->from=user;
  if (cmd==IDC_REPLY) dlg->SetNewsTarget(acc,grp);
  else dlg->to=hld->from;
  dlg->ref=hld->messid;
  PMessagePart msgtext=hld->GetArticle();
  CMessagePart *part=msgtext;
  dlg->msgbody="";
//  dlg->to=hld->from;	
  while (part) 
    {
    if (part->GetType()==CMessagePart::text)
		dlg->msgbody+=CString(part->GetData())+'\n';
	part=part->GetNext();
	}
  dlg->Create();
  OnCancel();
  return TRUE;
  }

BOOL CMsgPrevewDlg::OnDelete(UINT cmd) 
  {
  hld->SetTrash();	
  return TRUE;
  }

void CMsgPrevewDlg::OnPrint() 
  {
  CWnd &wnd2=*browser.GetWindow(GW_CHILD);
  CWnd &wnd3=*wnd2.GetWindow(GW_CHILD);
  CWnd &wnd1=*wnd3.GetWindow(GW_CHILD);
  wnd1.PostMessage(WM_COMMAND,MAKEWPARAM(27,1),0);
  }

void CMsgPrevewDlg::OnGoto() 
  {
  CFindList fnd;
  fnd.hlda=hld;
  fnd.grp=grp;
  fnd.next=NULL;
  theApp.m_pMainWnd->SendMessage(NRM_SELECTITEM,0,(LPARAM)&fnd);
  if (theApp.m_pMainWnd->IsIconic())	
	theApp.m_pMainWnd->PostMessage(WM_SYSCOMMAND,SC_RESTORE,0);	
  }

void CMsgPrevewDlg::Create(CWnd *parent)
  {
  CDialog::Create(IDD,theApp.config.owned?NULL:GetDesktopWindow());
  NrRegWindow(this);
  SetIcon(theApp.LoadIcon(IDI_NOTIFY),FALSE);
  SetIcon(theApp.LoadIcon(IDI_NOTIFY),TRUE);
  }

void CMsgPrevewDlg::LoadContent(CNewsAccount *acc, CNewsGroup *grp, CNewsArtHolder *hld)
  {
  if (hld==NULL || grp==NULL || acc==NULL) return;
  CHTMLDocument2 doc=browser.GetHtmlDocument();
  MSG msg;
  while (doc.m_lpDispatch==NULL || ::PeekMessage(&msg,0,0,0,PM_NOREMOVE)!=FALSE)
	{	
	while (::PeekMessage(&msg,0,0,0,PM_REMOVE)) {DispatchMessage(&msg);}
	Sleep(10);
	doc=browser.GetHtmlDocument();
	}  
  browser.NavigateArticle(acc,grp,hld);  
  this->hld=hld;  
  hld->AddRef();
  user=CString('"')+acc->GetMyName()+"\" <"+acc->GetMyEmail()+">";
  this->acc=acc;acc->Lock();
  this->grp=grp;
  SetWindowText(hld->GetSubject());
  if (theApp.config.autoRead) OnSeen();
  }

void CMsgPrevewDlg::OnSeen() 
  {
  if (hld->GetUnread())
	{
	hld->MarkAsRead(4);
	grp->DecNews();
	theApp.m_pMainWnd->PostMessage(NRM_GRPREADDONE,1,(LPARAM)grp->iteminlist);
	}
  bSeen.ShowWindow(SW_HIDE);
  }

void CMsgPrevewDlg::OnUp() 
  {
  CRWLockGuard guard(articleLock,false);guard.LockPM();

  CNewsArtHolder *hq=hld,*h;
opak:
  h=hq->GetParent();
  if (h==NULL) 
	{
	h=grp->GetList();
	if (h==hld) return;		
	while (h && h->GetNext()!=hq) h=h->GetNext();
	if (h==NULL) return;
	}
  else
	{
	if (h->GetChild()!=hld)
	  {
	  h=h->GetChild();
	  while (h && h->GetNext()!=hq) h=h->GetNext();
	  }
	if (h==NULL) h=hq->GetParent();
	}
  if (h->GetHidden()) goto opak;
  if (hld) hld->Release();	
  LoadContent(acc,grp,h);
  if (acc) acc->Release();
  }

void CMsgPrevewDlg::OnDown() 
  {
  CNewsArtHolder *h=hld;
opak:
  h=(GetKeyState(VK_SHIFT) & 0x80)?h->EnumNextNoChild():h->EnumNext();
  if (h)
	{
	if (h->GetHidden()) goto opak;
    if (hld) hld->Release();	
	LoadContent(acc,grp,h);
	if (acc) acc->Release();
	}
  }
