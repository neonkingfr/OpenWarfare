// MailMessage.h: interface for the MailHeadersScanner class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MAILMESSAGE_H__FFED7DE1_371C_4C2F_866D_3BD2160EBCEE__INCLUDED_)
#define AFX_MAILMESSAGE_H__FFED7DE1_371C_4C2F_866D_3BD2160EBCEE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class ArchiveStream;
class MailHeadersScanner  
{
public:
  bool ScanHeader(ArchiveStream &in);
  virtual void ParseLine(const char *command, const char *text)=0;

};

class IMailHeaderParserResult
{
public:
  virtual void HdrInfo(const char *command, const char *text)
};

class MailHeaderParser
{

};

void MimeHeader(char *name);

#endif // !defined(AFX_MAILMESSAGE_H__FFED7DE1_371C_4C2F_866D_3BD2160EBCEE__INCLUDED_)
