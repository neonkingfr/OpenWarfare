// SimplePhotoshopFile.cpp: implementation of the CSimplePhotoshopFile class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "scanner3D.h"
#include "SimplePhotoshopFile.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

void FWrite(ostream &out, void *data, int size, int pad)
{
  char *z=(char *)data;
  for (int i=0;i<size;i++)
  {
    for (int j=pad-1;j>=0;j--) out.write(z+j,1);
    z+=pad;
  }
}

CSimplePhotoshopFile::CSimplePhotoshopFile()
{
  
}

CSimplePhotoshopFile::~CSimplePhotoshopFile()
{
  
}

bool CSimplePhotoshopFile::CreateFile(const char *fname)
{
  file.open(fname,ios::binary|ios::out|ios::trunc);
  return (!(!file));
}

void CSimplePhotoshopFile::WriteHeader(unsigned short channels, unsigned long width, unsigned long height,  unsigned short depth, unsigned short mode)
{
  file.write("8BPS",4);
  FWrite(file,(short)1);
  FWrite(file,(long)0);
  FWrite(file,(short)0);
  FWrite(file,channels);
  FWrite(file,height);
  FWrite(file,width);
  FWrite(file,depth);
  FWrite(file,mode);
}

void CSimplePhotoshopFile::WriteColorModel(const char *data, unsigned long size)
{
  FWrite(file,size);
  FWrite(file,(void *)data,size,1);
}

void CSimplePhotoshopFile::LayerDef(CRect layerrc, short channels)
{
  FWrite(file,(long)layerrc.top);  //layer left
  FWrite(file,(long)layerrc.left);  //layer top
  FWrite(file,layerrc.bottom);  //layer bottom
  FWrite(file,layerrc.right);  //layer right
  FWrite(file,channels); //number of channels (3)
  for (int i=0;i<channels;i++)
  {
    FWrite(file,(short)i); //RED
    FWrite(file,(long)0); //RED
  }
  file.write("8BIM",4);	  //Blend mode sign
  file.write("norm",4);	  //Blend mode
  FWrite(file,(char)255);	//opacity
  FWrite(file,(char)0);	//clipping base
  FWrite(file,(char)0);	//flags
  FWrite(file,(char)0);	//padding
  FWrite(file,(long)4);	//mask extra data
  FWrite(file,(long)0);	//nomask
  
}

void CSimplePhotoshopFile::WriteRGBChannelImageData(CRect rect, unsigned long *data)
{
  FWrite(file,(short)0); //RAW data (no compression)
  unsigned long mask=0xFF0000;
  char shift=16;  
  unsigned long totalsize=rect.Size().cx*rect.Size().cy;
  for (int i=0;i<3;i++)
  {
    for (unsigned long j=0;j<totalsize;j++)
    {
      unsigned long p=(data[j] & mask)>>shift;
      file.write((const char *)&p,1);
    }
    mask>>=8;
    shift-=8;
  }
}

void CSimplePhotoshopFile::BeginLayerSection(short nlayers)
{
  FWrite(file,nlayers);
}

void CSimplePhotoshopFile::WriteGlobalLayerMaskInfo()
{
  SectionWriter sf(file);
  FWrite(file,(short)0);
  FWrite(file,(long)0);
  FWrite(file,(long)0);
  FWrite(file,(short)100);
  FWrite(file,(char)0);
  FWrite(file,(char)0);
}

