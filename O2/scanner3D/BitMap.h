// BitMap.h: interface for the BitMap class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_BITMAP_H__04A26257_B1D9_41ED_9F00_643EA48EDCF4__INCLUDED_)
#define AFX_BITMAP_H__04A26257_B1D9_41ED_9F00_643EA48EDCF4__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

template <class T>
class BitMap  
{
  int iWidth;
  int iHeight;
  T *data;
  public:
    BitMap();
    virtual ~BitMap();
    
    void Create(int width, int height);	
    void Destroy();
    T *operator[] (int row);
    T &operator() (int row, int col);
    void Set(int row, int col, T value)
    {(*this)(row,col)=value;}
    const T &Get(int row, int col) const
    {
      ASSERT(row>=0 && row<iHeight);
      ASSERT(col>=0 && col<iWidth);
      return data[row*iWidth+col];
    }
    
    int W() const 
    {return iWidth;}
    int H() const 
    {return iHeight;}
    bool IsCreated() 
    {return data!=NULL;}
    T *GetData() 
    {return data;}
};

#include "BitMap.tli"

#endif // !defined(AFX_BITMAP_H__04A26257_B1D9_41ED_9F00_643EA48EDCF4__INCLUDED_)
