// scanner3DDlg.cpp : implementation file
//

#include "stdafx.h"
#include "scanner3D.h"
#include "scanner3DDlg.h"
#include "SimplePhotoshopFile.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

bool SaveBitmap(BitMap<unsigned long>& src,const char *filename);
bool SaveBitmap(BitMap<unsigned char>& src,const char *filename);

/////////////////////////////////////////////////////////////////////////////
// CAboutDlg dialog used for App About

class CAboutDlg : public CDialog
{
  public:
    CAboutDlg();
    
    // Dialog Data
    //{{AFX_DATA(CAboutDlg)
    enum 
      { IDD = IDD_ABOUTBOX };
    //}}AFX_DATA
    
    // ClassWizard generated virtual function overrides
    //{{AFX_VIRTUAL(CAboutDlg)
  protected:
    virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
    //}}AFX_VIRTUAL
    
    // Implementation
  protected:
    //{{AFX_MSG(CAboutDlg)
    //}}AFX_MSG
    DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
  //{{AFX_DATA_INIT(CAboutDlg)
  //}}AFX_DATA_INIT
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
  CDialog::DoDataExchange(pDX);
  //{{AFX_DATA_MAP(CAboutDlg)
  //}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
  //{{AFX_MSG_MAP(CAboutDlg)
  // No message handlers
  //}}AFX_MSG_MAP
  END_MESSAGE_MAP()
    
    /////////////////////////////////////////////////////////////////////////////
    // CScanner3DDlg dialog
    
    CScanner3DDlg::CScanner3DDlg(CWnd* pParent /*=NULL*/)
      : CDialog(CScanner3DDlg::IDD, pParent)
      {
        //{{AFX_DATA_INIT(CScanner3DDlg)
        bAutodetectDepth = TRUE;
        fDepthMax = 1000.0f;
        fDepthMin = 0.0f;
        bSaveAlpha = TRUE;
        bSaveDepth = TRUE;
        bSaveNormal = TRUE;
        bSaveTex = TRUE;
        bInvertDepth = FALSE;
        bNormInvertX = FALSE;
        bNormInvertY = FALSE;
        bNormInvertZ = FALSE;
        iNormOrder = 5;
        bTexVisible = FALSE;
        b24Depth = FALSE;
        bAlphaInvert = FALSE;
        //}}AFX_DATA_INIT
        // Note that LoadIcon does not require a subsequent DestroyIcon in Win32
        m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
      }

void CScanner3DDlg::DoDataExchange(CDataExchange* pDX)
{
  CDialog::DoDataExchange(pDX);
  //{{AFX_DATA_MAP(CScanner3DDlg)
  DDX_Control(pDX, IDC_VIEWTAB, wTab);
  DDX_Control(pDX, IDC_STATUS, wStatus);
  DDX_Control(pDX, IDC_STATISTICS, wStatistics);
  DDX_Control(pDX, IDC_MINDEPTH, wDepthMin);
  DDX_Control(pDX, IDC_MAXDEPTH, wDepthMax);
  DDX_Control(pDX, IDC_CONVPROGRESS, wProgress);
  DDX_Check(pDX, IDC_AUTODETECTDEPTH, bAutodetectDepth);
  DDX_Text(pDX, IDC_MAXDEPTH, fDepthMax);
  DDX_Text(pDX, IDC_MINDEPTH, fDepthMin);
  DDX_Check(pDX, IDC_SAVEALPHA, bSaveAlpha);
  DDX_Check(pDX, IDC_SAVEDEPTH, bSaveDepth);
  DDX_Check(pDX, IDC_SAVENORMAL, bSaveNormal);
  DDX_Check(pDX, IDC_SAVETEX, bSaveTex);
  DDX_Check(pDX, IDC_INVERTDEPTH, bInvertDepth);
  DDX_Check(pDX, IDC_INVERTX, bNormInvertX);
  DDX_Check(pDX, IDC_INVERTY, bNormInvertY);
  DDX_Check(pDX, IDC_INVERTZ, bNormInvertZ);
  DDX_Radio(pDX, IDC_ORDER, iNormOrder);
  DDX_Check(pDX, IDC_TEXVISIBLE, bTexVisible);
  DDX_Check(pDX, IDC_24DEPTH, b24Depth);
  DDX_Check(pDX, IDC_ALPHAINVERT, bAlphaInvert);
  //}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CScanner3DDlg, CDialog)
  //{{AFX_MSG_MAP(CScanner3DDlg)
  ON_WM_SYSCOMMAND()
    ON_WM_PAINT()
      ON_WM_QUERYDRAGICON()
        ON_BN_CLICKED(IDC_AUTODETECTDEPTH, OnAutodetectdepth)
          ON_BN_CLICKED(IDC_START, OnStart)
            ON_WM_DRAWITEM()
              ON_NOTIFY(TCN_SELCHANGE, IDC_VIEWTAB, OnSelchangeViewtab)
                ON_WM_TIMER()
                  ON_BN_CLICKED(IDC_SAVEDEPTH, OnSaveXXXX)
                    ON_BN_CLICKED(IDC_SAVETEX, OnSaveXXXX)
                      ON_BN_CLICKED(IDC_SAVEALPHA, OnSaveXXXX)
                        ON_BN_CLICKED(IDC_SAVENORMAL, OnSaveXXXX)
                          //}}AFX_MSG_MAP
  END_MESSAGE_MAP()
    
    /////////////////////////////////////////////////////////////////////////////
    // CScanner3DDlg message handlers
    
    BOOL CScanner3DDlg::OnInitDialog()
    {
      CDialog::OnInitDialog();
      
      // Add "About..." menu item to system menu.
      
      // IDM_ABOUTBOX must be in the system command range.
      ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
      ASSERT(IDM_ABOUTBOX < 0xF000);
      
      CMenu* pSysMenu = GetSystemMenu(FALSE);
      if (pSysMenu != NULL)
      {
        CString strAboutMenu;
        strAboutMenu.LoadString(IDS_ABOUTBOX);
        if (!strAboutMenu.IsEmpty())
        {
          pSysMenu->AppendMenu(MF_SEPARATOR);
          pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
        }
      }
      
      // Set the icon for this dialog.  The framework does this automatically
      //  when the application's main window is not a dialog
      SetIcon(m_hIcon, TRUE);			// Set big icon
      SetIcon(m_hIcon, FALSE);		// Set small icon
      
      // TODO: Add extra initialization here
      DialogRules();
      
      wTab.InsertItem(0,StrRes[IDS_TABORIGIN],0);
      wTab.InsertItem(1,StrRes[IDS_TABVISIBLE],0);
      wTab.InsertItem(2,StrRes[IDS_TABALPHA],0);
      wTab.InsertItem(3,StrRes[IDS_TABDEPTH8],0);
      wTab.InsertItem(4,StrRes[IDS_TABDEPTH24],0);
      wTab.InsertItem(5,StrRes[IDS_TABNORMAL],0);
      
      return TRUE;  // return TRUE  unless you set the focus to a control
    }

void CScanner3DDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
  if ((nID & 0xFFF0) == IDM_ABOUTBOX)
  {
    CAboutDlg dlgAbout;
    dlgAbout.DoModal();
  }
  else
  {
    CDialog::OnSysCommand(nID, lParam);
  }
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CScanner3DDlg::OnPaint() 
{
  if (IsIconic())
  {
    CPaintDC dc(this); // device context for painting
    
    SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);
    
    // Center icon in client rectangle
    int cxIcon = GetSystemMetrics(SM_CXICON);
    int cyIcon = GetSystemMetrics(SM_CYICON);
    CRect rect;
    GetClientRect(&rect);
    int x = (rect.Width() - cxIcon + 1) / 2;
    int y = (rect.Height() - cyIcon + 1) / 2;
    
    // Draw the icon
    dc.DrawIcon(x, y, m_hIcon);
  }
  else
  {
    CDialog::OnPaint();
  }
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CScanner3DDlg::OnQueryDragIcon()
{
  return (HCURSOR) m_hIcon;
}

void CScanner3DDlg::DialogRules()
{ 
  BOOL stat1,stat2;
  stat1=IsDlgButtonChecked(IDC_AUTODETECTDEPTH)!=0;
  stat2=IsDlgButtonChecked(IDC_SAVEDEPTH)!=0;
  GetDlgItem(IDC_AUTODETECTDEPTH)->EnableWindow(stat2);
  GetDlgItem(IDC_24DEPTH)->EnableWindow(stat2);
  wDepthMin.EnableWindow(stat2 && !stat1);
  wDepthMax.EnableWindow(stat2 && !stat1);
  GetDlgItem(IDC_INVERTDEPTH)->EnableWindow(stat2);
  stat1=IsDlgButtonChecked(IDC_SAVETEX);
  GetDlgItem(IDC_TEXVISIBLE)->EnableWindow(stat1);
  stat1=IsDlgButtonChecked(IDC_SAVEALPHA);
  GetDlgItem(IDC_ALPHAINVERT)->EnableWindow(stat1);
  stat1=IsDlgButtonChecked(IDC_SAVENORMAL);
  CWnd *wnd=GetDlgItem(IDC_ORDER);
  for (int i=0;i<6;i++,wnd=wnd->GetNextWindow())
    wnd->EnableWindow(stat1);
  GetDlgItem(IDC_INVERTX)->EnableWindow(stat1);
  GetDlgItem(IDC_INVERTY)->EnableWindow(stat1);
  GetDlgItem(IDC_INVERTZ)->EnableWindow(stat1);
}

void CScanner3DDlg::OnSaveXXXX() 
{
  DialogRules();	
}

void CScanner3DDlg::OnAutodetectdepth() 
{
  DialogRules();		
}

void CDlgScene::Status(int phase)
{
  switch (phase)
  {
    case 0: wnd->SetStatus(StrRes[IDS_LOADEDBITMAP],tex.W(),tex.H());
    wnd->SetBitmap(tex);
    wnd->GetDlgItem(IDC_TEXTURE)->Invalidate(TRUE);
    wnd->SetStatistics(StrRes[IDS_STATIMAGE],tex.W(),tex.H());
    break;
    case 1: 
    wnd->SetStatus(StrRes[IDS_LOADEDTEXCOORDS],tuv.GetCount());
    wnd->SetStatistics(StrRes[IDS_STATNUMTX],tuv.GetCount());
    break;
    case 2: 	  
    wnd->SetStatus(StrRes[IDS_LOADEDVERTICES],vlist.GetCount());
    wnd->SetStatistics(StrRes[IDS_STATNUMVX],vlist.GetCount());
    break;
    case 3: 
    wnd->SetStatus(StrRes[IDS_LOADEDFACES],faces.GetCount());
    wnd->SetStatistics(StrRes[IDS_STATNUMFC],faces.GetCount());
    break;
    case 4: wnd->SetStatus(StrRes[IDS_LOADEDTEXINDEX],uvindex.GetCount());break;
  }
  wnd->SetProgress(++ppos,5);
  wnd->UpdateWindow();
}

static unsigned long NormalToRGB(geVector4 &normal)
{
  int r,g,b;
  r=(int)((normal.x+1.0f)*127.5f);
  g=(int)((normal.y+1.0f)*127.5f);
  b=(int)((normal.z+1.0f)*127.5f);
  int rrbest,ggbest,bbbest;
  int rrmax=r==255?1:2;
  int ggmax=g==255?1:2;
  int bbmax=g==255?1:2;
  float dist=2.0f;
  for (int rr=r!=0?-1:0;rr<rrmax;rr++)
    for (int gg=g!=0?-1:0;gg<ggmax;gg++)
      for (int bb=b!=0?-1:0;bb<bbmax;bb++)
      {
        geVector4 nw((float)(rr+r)-127.5f,(float)(gg+g)-127.5f,(float)(bb+b)-127.5f);
        nw/=nw.Module();
        float d=nw.Distance2(normal);
        if (d<dist)
        {
          dist=d;
          rrbest=rr;
          ggbest=gg;
          bbbest=bb;
        }
      }
  r+=rrbest;
  g+=ggbest;
  b+=bbbest;
  return RGB(r,g,b);
}

void CDlgScene::PointHit(int fc, int x, int y, float depth, geVector4 &normal)
{
  MSG msg;
  this->totlpts++;
  while (PeekMessage(&msg,0,0,0,PM_REMOVE))
    if (msg.message!=WM_CLOSE) DispatchMessage(&msg);
  if ((fc & 1023)==0)
    wnd->SetProgress(fc,faces.GetCount());
  if (tex.IsCreated()) wnd->tex.Set(y,x,tex.Get(y,x));
  if (wnd->alpha.Get(y,x)!=0) ovrpts++;else wnd->alpha.Set(y,x,255);
  float fdp=(depth-wnd->fDepthMin)/(wnd->fDepthMax-wnd->fDepthMin);
  if (fdp<0.0f) fdp=0.0f;
  if (fdp>1.0f) fdp=1.0f;
  wnd->depth.Set(y,x,(int)(0xFFFFFF*fdp+0.5f));  
  unsigned long cnormal=NormalToRGB(normal.Norm())  ^ normalinvert;
  switch (wnd->iNormOrder)
  {
    case 0:break;
    case 1:cnormal=RGB(GetGValue(cnormal),GetBValue(cnormal),GetRValue(cnormal));break;
    case 2:cnormal=RGB(GetBValue(cnormal),GetRValue(cnormal),GetGValue(cnormal));break;
    case 3:cnormal=RGB(GetRValue(cnormal),GetBValue(cnormal),GetGValue(cnormal));break;
    case 4:cnormal=RGB(GetGValue(cnormal),GetRValue(cnormal),GetBValue(cnormal));break;
    case 5:cnormal=RGB(GetBValue(cnormal),GetGValue(cnormal),GetRValue(cnormal));break;
  }
  wnd->normal.Set(y,x,cnormal);
}

void CScanner3DDlg::OnStart() 
{
  CWnd *wnd=GetWindow(GW_CHILD);
  while (wnd!=NULL)
  {
    int id=wnd->GetDlgCtrlID();
    if (id!=IDC_VIEWTAB && id!=IDC_STATISTICS && id!=IDCANCEL && id!=IDC_STATUS) wnd->EnableWindow(FALSE);
    wnd=wnd->GetNextWindow();
  }
  GetDlgItem(IDCANCEL)->ShowWindow(SW_SHOW);
  GetDlgItem(IDC_START)->ShowWindow(SW_HIDE);
  SetTimer(10,1000,NULL);
  Start();
  KillTimer(10);
  GetDlgItem(IDCANCEL)->ShowWindow(SW_HIDE);
  GetDlgItem(IDC_START)->ShowWindow(SW_SHOW);
  wnd=GetWindow(GW_CHILD);
  while (wnd!=NULL)
  {
    if (wnd->GetDlgCtrlID()!=IDC_TEXTURE) wnd->EnableWindow(TRUE);
    wnd=wnd->GetNextWindow();
  }
  DialogRules();
}

void CScanner3DDlg::SetProgress(int cur, int max)
{
  wProgress.SetRange32(0,max);
  wProgress.SetPos(cur);
}

void CScanner3DDlg::OnDrawItem(int nIDCtl, LPDRAWITEMSTRUCT lpDrawItemStruct) 
{
  if (nIDCtl==IDC_TEXTURE)
  {
    if (curbmp.GetSafeHandle()==NULL) return;
    CDC memdc;
    CDC *wnd=CDC::FromHandle(lpDrawItemStruct->hDC);
    CBitmap *saved;
    memdc.CreateCompatibleDC(wnd);
    saved=memdc.SelectObject(&curbmp);	
    BITMAP bp;
    curbmp.GetBitmap(&bp);
    wnd->SetStretchBltMode(HALFTONE);
    //	wnd->SetBrushOrg(CPoint(0,0));
    wnd->StretchBlt(0,0,lpDrawItemStruct->rcItem.right,lpDrawItemStruct->rcItem.bottom,&memdc,0,0,bp.bmWidth,bp.bmHeight,SRCCOPY);
    memdc.SelectObject(saved);
    memdc.DeleteDC();
  }
  CDialog::OnDrawItem(nIDCtl, lpDrawItemStruct);
}

void CScanner3DDlg::SetBitmap(BitMap<unsigned long> &src)
{
  curbmp.DeleteObject();
  if (src.IsCreated()==false) return;
  BITMAPINFO binfo;
  memset(&binfo,0,sizeof(binfo));
  binfo.bmiHeader.biSize=sizeof(binfo.bmiHeader);
  binfo.bmiHeader.biWidth=src.W();
  binfo.bmiHeader.biHeight=src.H();
  binfo.bmiHeader.biPlanes=1;
  binfo.bmiHeader.biBitCount=32;
  binfo.bmiHeader.biCompression=BI_RGB;
  CDC *wnd=GetDC();
  CDC memdc;
  memdc.CreateCompatibleDC(wnd);
  curbmp.CreateCompatibleBitmap(wnd,src.W(),src.H());
  ReleaseDC(wnd);
  CBitmap *saved;
  saved=memdc.SelectObject(&curbmp);	
  SetDIBitsToDevice(memdc,0,0,src.W(),src.H(),0,0,0,src.H(),src.GetData(),&binfo,0);
  memdc.SelectObject(saved);
  memdc.DeleteDC();
  GetDlgItem(IDC_TEXTURE)->Invalidate(FALSE); 
}

void CScanner3DDlg::SetBitmap(BitMap<unsigned char> &src)
{
  curbmp.DeleteObject();
  if (src.IsCreated()==false) return;
  char buff[sizeof(BITMAPINFO)+sizeof(RGBQUAD)*256];
  BITMAPINFO &binfo=*(BITMAPINFO *)buff;
  memset(&binfo,0,sizeof(binfo));
  binfo.bmiHeader.biSize=sizeof(binfo.bmiHeader);
  binfo.bmiHeader.biWidth=src.W();
  binfo.bmiHeader.biHeight=src.H();
  binfo.bmiHeader.biPlanes=1;
  binfo.bmiHeader.biBitCount=8;
  binfo.bmiHeader.biCompression=BI_RGB;
  for (int i=0;i<256;i++) 
  {
    binfo.bmiColors[i].rgbBlue=binfo.bmiColors[i].rgbGreen=binfo.bmiColors[i].rgbRed=i;
    binfo.bmiColors[i].rgbReserved=0;
  }
  CDC *wnd=GetDC();
  CDC memdc;
  memdc.CreateCompatibleDC(wnd);
  curbmp.DeleteObject();
  curbmp.CreateCompatibleBitmap(wnd,src.W(),src.H());
  ReleaseDC(wnd);
  CBitmap *saved;
  saved=memdc.SelectObject(&curbmp);	
  SetDIBitsToDevice(memdc,0,0,src.W(),src.H(),0,0,0,src.H(),src.GetData(),&binfo,0);
  memdc.SelectObject(saved);
  memdc.DeleteDC();
  GetDlgItem(IDC_TEXTURE)->Invalidate(FALSE);
}

void CScanner3DDlg::PrepareMaps()
{
  if (depth.IsCreated())depth.Destroy();
  if (depth8.IsCreated())depth8.Destroy();
  if (alpha.IsCreated())alpha.Destroy();
  if (normal.IsCreated())normal.Destroy();
  if (tex.IsCreated())tex.Destroy();
  int w=scene.tex.W();
  int h=scene.tex.H();
  if (w==0 || h==0) w=h=2048;
  depth.Create(w,h);
  depth8.Create(w,h);
  alpha.Create(w,h);
  normal.Create(w,h);
  tex.Create(w,h);
  for (int y=0;y<h;y++)
  {
    memset(depth[y],0,sizeof(depth[y][0])*w);
    memset(alpha[y],0,sizeof(alpha[y][0])*w);
    for (int x=0;x<w;x++) normal[y][x]=RGB(0x80,0x80,0x80);
    memset(tex[y],0,sizeof(tex[y][0])*w);
  }
}

void CScanner3DDlg::SelectBitmap(int tab)
{
  if (tab==-1) tab=wTab.GetCurSel();
  if (tab==-1) tab=0;
  switch (tab)
  {
    case 0: SetBitmap(scene.tex);break;
    case 1: SetBitmap(tex);break;
    case 2: SetBitmap(alpha);break;
    case 3: SetBitmap(depth8);break;
    case 4: SetBitmap(depth);break;
    case 5: SetBitmap(normal);break;
  }
}

void CScanner3DDlg::OnSelchangeViewtab(NMHDR* pNMHDR, LRESULT* pResult) 
{
  // TODO: Add your control notification handler code here
  SelectBitmap();
  *pResult = 0;
}

void CScanner3DDlg::PostProcessing()
{
  int w=tex.W();
  int h=tex.H();
  for (int y=0;y<h;y++)
  {
    unsigned long *p=depth[y];
    unsigned char *q=depth8[y];
    for (int x=0;x<w;x++)
    {
      if (this->bInvertDepth) *p=*p ^ 0xFFFFFF;
      *q++=(unsigned char)((*p++)>>16);
    }
    if (this->bAlphaInvert)
    {
      unsigned char *a=alpha[y];
      for (int x=0;x<w;x++) a[x]=~a[x];
    }
  }
}

void CScanner3DDlg::Start()
{
  bool reload=true;
  UpdateData(TRUE);
  CFileDialog openfname(TRUE,NULL,(input.GetLength()==0)?NULL:(LPCTSTR)input,
  OFN_FILEMUSTEXIST|OFN_HIDEREADONLY|OFN_READONLY|OFN_SHAREAWARE,StrRes[IDS_WRLFILTER]);
  if (openfname.DoModal()==IDCANCEL) return;
  CFileDialog savepth(FALSE,NULL,outpath.GetLength()==0?NULL:(LPCTSTR)outpath,OFN_PATHMUSTEXIST|OFN_HIDEREADONLY,StrRes[IDS_BITMAPS]);
  if (savepth.DoModal()==IDCANCEL) return;
  {
    CString nw=openfname.GetPathName();
    CFileFind fnd;
    if (fnd.FindFile(nw)==TRUE )
    {
      CTime t1;
      fnd.FindNextFile();
      fnd.GetLastWriteTime(t1);
      fnd.Close();
      if (t1==inputtime && input==nw) reload=false;
      inputtime=t1;
    }
    input=nw;
  }
  input=openfname.GetPathName();
  outpath=savepth.GetPathName();
  int p=outpath.ReverseFind('_');
  int q=outpath.ReverseFind('\\');
  if (p>q) outpath.Delete(p,outpath.GetLength()-p);
  else
  {
    p=outpath.ReverseFind('.');
    if (p>q) outpath.Delete(p,outpath.GetLength()-p);
  }
  begtime=GetTickCount();
  scene.wnd=this;
  scene.ppos=0;
  scene.normalinvert=(bNormInvertX?0xFF:0) | (bNormInvertY?0xFF00:0) | (bNormInvertZ?0xFF0000:0);
  scene.totlpts=0;
  scene.ovrpts=0;
  SetStatus(StrRes[IDS_LOADINGSCENE]);
  SetProgress(0);
  SetCursor(LoadCursor(NULL,IDC_WAIT));
  wStatistics.SetWindowText("");
  SetStatistics(StrRes[IDS_STATINPUT],(LPCTSTR)input);
  SetStatistics(StrRes[IDS_STATOUTPUT],(LPCTSTR)outpath);
  if (reload)
  {
    scene.Reset();
    ifstream ifile(input,ios::in|ios::nocreate);
    if (!ifile)
    {AfxMessageBox(IDS_OPENERROR,MB_OK|MB_ICONEXCLAMATION);return;}
    int err=scene.LoadScene(ifile);
    if (err)
    {
      switch (err)
      {
        case 1:err=IDS_LOADFAILEDBITMAP;break;
        case 2:err=IDS_LOADFAILEDTEXTURECOORDS;break;
        case 3:err=IDS_LOADFAILEDVERTICES;break;
        case 4:err=IDS_LOADFAILEDFACES;break;
        case 5:err=IDS_LOADFAILEDINDEX;break;
      }
      MessageBox(StrRes[err],StrRes[IDS_LOADFAILED],MB_OK|MB_ICONEXCLAMATION);
      return;
    }
  }
  else
  {
    for (int i=0;i<5;i++) scene.Status(i);
  }
  if (scene.CheckScene()==false)
  {
    MessageBox(StrRes[IDS_CHECKERROR],StrRes[IDS_LOADFAILED],MB_OK|MB_ICONEXCLAMATION);
    return;
  }
  SetStatistics(StrRes[IDS_STATMINMAXZ],scene.minz,scene.maxz);
  if (bAutodetectDepth) 
  {
    fDepthMax=scene.maxz;
    fDepthMin=scene.minz;
    UpdateData(FALSE);
  }
  SetStatus(StrRes[IDS_PROCESSINGFACES]);
  SetProgress(0);
  PrepareMaps();
  scene.ProcessPoints();
  if (!scene.halt)
  {
    SetStatus(StrRes[IDS_FINALPROCESSING]);
    PostProcessing();
    if (this->bSaveTex)
      if (this->bTexVisible) 
      {if (SaveBitmap(tex,outpath+"_tex.bmp")==false)
          MessageBox(StrRes[IDS_SAVETEXERROR]);}
    else 
    {if (SaveBitmap(scene.tex,outpath+"_tex.bmp")==false)
        MessageBox(StrRes[IDS_SAVETEXERROR]);}
    
    if (this->bSaveDepth)
      if (this->b24Depth) 
      {if (SaveBitmap(this->depth,outpath+"_depth.bmp")==false)
          MessageBox(StrRes[IDS_SAVEDEPTHFAILED]);}
    else 
    {if (SaveBitmap(this->depth8,outpath+"_depth.bmp")==false)
        MessageBox(StrRes[IDS_SAVEDEPTHFAILED]);}
    
    if (this->bSaveNormal) 
      if (SaveBitmap(this->normal,outpath+"_normal.bmp")==false)
        MessageBox(StrRes[IDS_SAVENORMALFAILED]);
    
    if (this->bSaveAlpha) 
      if (SaveBitmap(this->alpha,outpath+"_alpha.bmp")==false)
        MessageBox(StrRes[IDS_SAVEALPHAFAILED]);
    
  }
  else
  {
    SetStatistics(StrRes[IDS_PROCESSINGCANCELED]);
  }
  SetStatus(StrRes[IDS_DONE]);
  SetProgress(0);  
  SelectBitmap();
  begtime=GetTickCount()-begtime;
  {
    int max=tex.W()*tex.H();
    SetStatistics(StrRes[IDS_STATTOTALPT],scene.totlpts,max,scene.totlpts/(max/100));
    SetStatistics(StrRes[IDS_STATPTOVERLAP],scene.ovrpts);
    SetStatistics(StrRes[IDS_STATFCERR],scene.statmaperrors);
    SetStatistics(StrRes[IDS_STATPTPERFC],(float)scene.totlpts/(float)scene.faces.GetCount());
    SetStatistics(StrRes[IDS_STATTIME],begtime/60000,(begtime/1000)%60);	
  }
}

void CScanner3DDlg::SetStatus(const char *text, ...)
{
  char buff[512];
  va_list als;
  va_start(als,text);
  _vsnprintf(buff,sizeof(buff),text,als);
  SetDlgItemText(IDC_STATUS,buff);
}

void CScanner3DDlg::SetStatistics(const char *text, ...)
{
  char buff[512];
  va_list als;
  va_start(als,text);
  _vsnprintf(buff,sizeof(buff),text,als);
  int l=wStatistics.GetWindowTextLength();
  wStatistics.SetSel(l,l);
  wStatistics.ReplaceSel(buff);
  wStatistics.ReplaceSel("\r\n");
}

void CScanner3DDlg::OnTimer(UINT nIDEvent) 
{
  SelectBitmap();	
  CDialog::OnTimer(nIDEvent);
}

void CScanner3DDlg::WritePSD()
{
  CSimplePhotoshopFile psd;
  psd.CreateFile("test.psd");
  psd.WriteHeader(3,tex.W(),tex.H(),8,PSPM_RGB);  //Header
  psd.WriteColorModel(NULL,0); //Color model
  psd.WriteColorModel(NULL,0); //No resources
  {
    CSimplePhotoshopFile::SectionWriter beglayer(psd.file); //Layers
    {
      psd.BeginLayerSection(1);	
      psd.LayerDef(CRect(0,0,tex.W(),tex.H()),3);
      psd.WriteRGBChannelImageData(CRect(0,0,tex.W(),tex.H()),tex.GetData());
      psd.WriteGlobalLayerMaskInfo();
    }
  }
  psd.WriteRGBChannelImageData(CRect(0,0,tex.W(),tex.H()),tex.GetData()); //Image data
}

static bool SaveBitmap(BitMap<unsigned long>& src,const char *filename)
{
  ofstream bmp(filename,ios::out|ios::binary|ios::trunc);
  if (!bmp) return false;
  
  BITMAPINFO binfo;
  memset(&binfo,0,sizeof(binfo));
  binfo.bmiHeader.biSize=sizeof(binfo.bmiHeader);
  binfo.bmiHeader.biWidth=src.W();
  binfo.bmiHeader.biHeight=src.H();
  binfo.bmiHeader.biPlanes=1;
  binfo.bmiHeader.biBitCount=32;
  binfo.bmiHeader.biCompression=BI_RGB;
  
  BITMAPFILEHEADER hdr;
  hdr.bfType=0x4d42;
  hdr.bfReserved1=0;
  hdr.bfReserved2=0;
  hdr.bfOffBits=sizeof(hdr)+sizeof(binfo.bmiHeader);
  hdr.bfSize=hdr.bfOffBits+src.W()*src.H()*4;
  
  bmp.write((char *)&hdr,sizeof(hdr));
  bmp.write((char *)&binfo,binfo.bmiHeader.biSize);
  for (int y=0;y<src.H();y++)	
    bmp.write((char *)src[y],src.W()*4);  
  return true;
}

static bool SaveBitmap(BitMap<unsigned char>& src,const char *filename)
{
  ofstream bmp(filename,ios::out|ios::binary|ios::trunc);
  if (!bmp) return false;
  
  BITMAPINFO binfo;
  memset(&binfo,0,sizeof(binfo));
  binfo.bmiHeader.biSize=sizeof(binfo.bmiHeader);
  binfo.bmiHeader.biWidth=src.W();
  binfo.bmiHeader.biHeight=src.H();
  binfo.bmiHeader.biPlanes=1;
  binfo.bmiHeader.biBitCount=8;
  binfo.bmiHeader.biCompression=BI_RGB;
  
  BITMAPFILEHEADER hdr;
  hdr.bfType=0x4d42;
  hdr.bfReserved1=0;
  hdr.bfReserved2=0;
  hdr.bfOffBits=sizeof(hdr)+sizeof(binfo.bmiHeader)+256*sizeof(RGBQUAD);
  hdr.bfSize=hdr.bfOffBits+src.W()*src.H();
  
  bmp.write((char *)&hdr,sizeof(hdr));
  bmp.write((char *)&binfo.bmiHeader,binfo.bmiHeader.biSize);
  for (int i=0;i<256;i++)
  {
    RGBQUAD p;
    p.rgbBlue=i;
    p.rgbGreen=i;
    p.rgbRed=i;
    p.rgbReserved=0;
    bmp.write((char *)&p,sizeof(p));
  }
  for (int y=0;y<src.H();y++)	
  {
    bmp.write((char *)src[y],src.W());  
    bmp.write("padding",((src.W()+3) & ~0x3)-src.W());
  }
  return true;
}

void CScanner3DDlg::OnCancel() 
{
  scene.halt=true;	
  if (GetDlgItem(IDC_START)->IsWindowEnabled()) EndDialog(0);
}

