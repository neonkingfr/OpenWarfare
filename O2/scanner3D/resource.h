//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by scanner3D.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_SCANNER3D_DIALOG            102
#define IDS_WRLFILTER                   102
#define IDS_BITMAPS                     105
#define IDS_LOADEDBITMAP                106
#define IDS_LOADEDTEXCOORDS             107
#define IDS_LOADEDVERTICES              108
#define IDS_LOADEDFACES                 109
#define IDS_LOADEDTEXINDEX              110
#define IDS_LOADFAILEDBITMAP            111
#define IDS_LOADFAILEDVERTICES          112
#define IDS_LOADFAILEDFACES             113
#define IDS_LOADFAILEDINDEX             114
#define IDS_LOADFAILEDTEXTURECOORDS     115
#define IDS_LOADFAILED                  116
#define IDS_CHECKERROR                  117
#define IDS_OPENERROR                   118
#define IDS_DONE                        119
#define IDS_LOADINGSCENE                120
#define IDS_PROCESSINGFACES             121
#define IDS_TABORIGIN                   122
#define IDS_TABVISIBLE                  123
#define IDS_TABALPHA                    124
#define IDS_TABDEPTH8                   125
#define IDS_TABDEPTH24                  126
#define IDS_TABNORMAL                   127
#define IDR_MAINFRAME                   128
#define IDS_FINALPROCESSING             128
#define IDS_STATIMAGE                   129
#define IDS_STATNUMVX                   130
#define IDS_STATNUMFC                   131
#define IDS_STATNUMTX                   132
#define IDS_STATINPUT                   133
#define IDS_STATOUTPUT                  134
#define IDS_STATTIME                    135
#define IDS_STATMINMAXZ                 136
#define IDS_STATTOTALPT                 137
#define IDS_STATPTOVERLAP               138
#define IDS_STATPTPERFC                 139
#define IDS_STATFCERR                   140
#define IDS_SAVETEXERROR                141
#define IDS_SAVEDEPTHFAILED             142
#define IDS_SAVENORMALFAILED            143
#define IDS_SAVEALPHAFAILED             144
#define IDS_PROCESSINGCANCELED          145
#define IDC_TEXTURE                     1000
#define IDC_INVERTDEPTH                 1001
#define IDC_SAVETEX                     1002
#define IDC_SAVEDEPTH                   1003
#define IDC_SAVEALPHA                   1004
#define IDC_VIEWTAB                     1006
#define IDC_TEXVISIBLE                  1007
#define IDC_AUTODETECTDEPTH             1008
#define IDC_MINDEPTH                    1009
#define IDC_MAXDEPTH                    1010
#define IDC_SAVENORMAL                  1011
#define IDC_STATUS                      1012
#define IDC_CONVPROGRESS                1013
#define IDC_START                       1014
#define IDC_24DEPTH                     1015
#define IDC_STATISTICS                  1016
#define IDC_ORDER                       1017
#define IDC_RADIO2                      1018
#define IDC_RADIO3                      1019
#define IDC_RADIO4                      1020
#define IDC_RADIO5                      1021
#define IDC_RADIO6                      1022
#define IDC_INVERTX                     1023
#define IDC_INVERTY                     1024
#define IDC_INVERTZ                     1025
#define IDC_ALPHAINVERT                 1026
#define IDC_CUSTOM1                     1027

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        129
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1028
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
