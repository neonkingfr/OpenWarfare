// Searcher.cpp: implementation of the Searcher class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "scanner3D.h"
#include "Searcher.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

Searcher::Searcher(CString &searchtext):text(searchtext)
{
  index=0;
}

bool Searcher::Advance(char znak)
{
  if (text[index]==znak) index++;else index=0;
  if (index>=text.GetLength())
  {
    index=0;
    return true;
  }
  return false;
}

int FileSearch(istream &in, Searcher **list, int count)
{
  int p=in.get();
  while (p!=EOF)
  {
    if (!isspace(p))
    {
      for (int i=0;i<count;i++)
      {
        if (list[i]->Advance(p)==true) return i;
      }	 
    }
    p=in.get();
  }
  return -1;
}

