#ifndef _G3DMODEL_H_
#define _G3DMODEL_H_

#ifdef __cplusplus
extern "C" {
#endif

typedef struct _AnimModelFrame
  {
  struct _AnimModelFrame *next,*alias;
  int framenum;
  int aliasframe;
  TSCENEDEF model;
  int flags;
  float xr,yr,zr;
  float rotx,roty,rotz;
  float scalx,scaly,scalz;
  int refpoint;
  }TANIMMODELFRAME;

typedef struct _AnimModelAdd
  {
  int flags;
  int aliasframe;
      float xr,yr,zr;
      float rotx,roty,rotz;
      float scalx,scaly,scalz;
  int refpoint;
  }TG3DM_FRAME;

#define G3DMF_TRANSLATE 0x1
#define G3DMF_ROTATEX 0x2
#define G3DMF_ROTATEY 0x4
#define G3DMF_ROTATEZ 0x8
#define G3DMF_SCALE 0x10
#define G3DMF_ALIAS 0x20
#define G3DMF_SCENE 0x0
#define G3DMF_TRANSFORM (G3DMF_TRANSLATE|G3DMF_ROTATEX|G3DMF_ROTATEY|G3DMF_ROTATEZ|G3DMF_SCALE)
#define G3DMF_SMOOTH 0x40

typedef struct _AnimPhaser
  {
  struct _AnimPhaser *next;
  int phasenumber;
  TANIMMODELFRAME *frames;
  int framecount;
  int logiccount;
  long flags;
  int clones;  //pocet klonu teto animace;
  }TANIMPHASER;

typedef struct _ModelHandle
  {
  int UID; //unique identifikator - potreba v pripade tvorby databaze objektu.
  //system uid nevyuziva, lze jej naplnit jakoukoliv hodnotou.
  TANIMPHASER *mainphaser;
  TANIMPHASER *curphase;
  TANIMMODELFRAME *curframe;
  int curphasenum;
  int curframenum;
  unsigned long curanimtick; //cislo posledniho animticku;
  unsigned short *texture_xlat;
  int xlatcount;
  float speed;
  long flags;
  TANIMMODELFRAME *lastframe;
  int smoothlen;
  char animdir;
  }TMODELHANDLE,*HMODEL,TMODEL;

#define G3DMH_ALLOCATED 0x1    //paleta textur je alokovana
#define G3DMH_AUTOANIMATE 0x2  //model se bude sam animovat
#define G3DMH_SPEED 0x4        //polozka speed je povolena
#define G3DMH_SMOOTHPHASE 0x8    //prechod z jedne faze do jine
#define G3DMP_NOLOOP 0x2       //faze se bude animovat jen jednou, pak se zastavi
#define G3DMP_NOTIFYEND 0x4    //funkce Load vrati status G3DM_LASTFRAME na konci
//Errors

#define G3DM_OK 0
#define G3DM_NOMEMORY -21        //Dosla pamet
#define G3DM_PHASENOTEXISTS -22  //Faze s timto cislem neexistuje
#define G3DM_PHASEEXISTS -23     //Pokus o vytvoreni existujici faze
#define G3DM_NOCURRENTPHASE -24  //Zadna faze nebyla vybrana.
#define G3DM_PHASELOST -25       //Nejaky jiny klon vymazal aktualni fazi. Pouzijte g3dmChangePhase
#define G3DM_USEDESTROYMODEL -26 //Ke zniceni modelu pouzijte funkci g3dmDestroyModel.
#define G3DM_FRAMEOUTRANGE -27   //Cislo frame je mimo logicky rozsah
#define G3DM_DESTROYED 21        //Objekt byl znicen. Info pouze
#define G3DM_LASTFRAME 22        //Informuje, ze bylo zobrazeno posledni frame.
#define G3DM_CANNOTINTERPOLATE -28//Linearni interpolaci modelu nelze provest
#define G3DM_G3DERROR -29        //G3D vratil nespecifikovanou chybu pri jeho volani
#define G3DM_SAMEOBJECT -30     //Vraci, kdyz pri interpolaci se zjisti ze dva objekty jsou jeden
#define G3DM_NOCURRENTFRAME -31 //Zadny frame neni aktivni
#define G3DM_FRAMENOTFOUND -32  //fyzicke cislo frame nebylo nalezeno
#define G3DM_NOMATRIX -33       //Pri pokusu ziskat matici modelu se zjistilo ze nema definovany pohyb.

#define G3DMD_FORWARD 1
#define G3DMD_BACKWARD -1
#define G3DMD_DEFAULT 0

char g3dmCreateModel(HMODEL model); //vytvori obal pro animaci
char g3dmChangePhaseEx(HMODEL model,int phase,char dir); //zmeni aktualni fazi animace
#define g3dmChangePhase(model,phase) g3dmChangePhaseEx(model,phase,G3DMD_FORWARD);
char g3dmCreatePhase(HMODEL model,int phase); //vytvori fazi animace
char g3dmChangePhaseSmooth(HMODEL model, int phase, int animcount);
char g3dmSetFrameCount(HMODEL model,int framecount); //nastavi logicky animacnich pocet policek
char g3dmDeletePhase(HMODEL model,int phase); //maze nejakou fazi
char g3dmAddFrame(HMODEL model,TSCENEDEF *scene); //Pridava policko do faze
char g3dmAttachTextureXlat(HMODEL,unsigned short *texture_xlat,int xlatcount,char copy); //nastavuje tabulku textur pro model
char g3dmSetFrameCountEx(HMODEL model,int fps);
char g3dmCloneModel(HMODEL model,HMODEL clone); //Klonuje model pro zmenu zakladnich vlastnosti
char g3dmDestroyModel(HMODEL model); //Nici model - ke zniceni dojde az kdyz se znici vsechny klony
char g3dmLoadModelFrameEx(HMODEL model,HMATRIX mm, char mode); //nacita model do sceny
#define G3DML_DISPLAY 0
#define G3DML_GETMATRIX 1
char g3dmAnimate(HMODEL model); //Pricte 1 k aktualni frame.
char g3dmChangeFrame(HMODEL model,int frame); //skokove zmeni aktualni frame
char g3dmAddFrameEx2(HMODEL model, TSCENEDEF *scene, int frame, TG3DM_FRAME *finfo); //prida frame specifickeho cisla.
    //pokud frame existuje, prepise jej
char g3dmAddLoopBack(HMODEL model, int frame); //nastavuje fyzicke frame loopbacku
char g3dmChangePhaseFlags(HMODEL model, long setflags, long resetflags);
char g3dmCalcRefMatrix(HMODEL model, int rpref, HMATRIX mref,HMATRIX res);
    //vraci matici pro transformaci. Pokud je mref=NULL pak res obsahuje maticic
    //Pokud neni pak res obsahuje resn*mref, kde resn je vysledek pri mref=NULL
    //(pouzijte po g3dmLoadModelFrame);
#define g3dmLoadModelMatrix(model,mm) g3dmLoadModelFrameEx(model,mm,G3DML_GETMATRIX)
    //vraci transformaci pro model. Lze pouzit, pokud model neobsahuje scenu
    //jen tranformace. Kontroluje AUTOANIM jako g3dLoadModelFrame.
#define g3dmLoadModelFrame(model,mm) g3dmLoadModelFrameEx(model,mm,G3DML_DISPLAY)

long g3dmAnimTick(int speed);
long g3dmGetAnimTick();

#define g3dmAddFrameEx(model,scene,frame) g3dmAddFrameEx2(model,scene,frame,NULL);



#define g3dmChangeModelFlags(model,setflags,resetflags) ((model)->flags=((model)->flags & ~((resetflags)&~G3DMH_ALLOCATED)) | ((setflags)&~G3DMH_ALLOCATED))

#define g3dSetUID(model,id) ((model)->UID=id)
#define g3dGetUID(model) ((model)->UID)


char g3dmGetInterFrame(HMODEL model, TANIMMODELFRAME **fr1, TANIMMODELFRAME **fr2,  float *pp, HMATRIX hm);
//Internal! Don't use, if you don't know, what you do.


#ifdef __cplusplus
}
#endif
#endif

