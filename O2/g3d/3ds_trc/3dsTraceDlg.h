#if !defined(AFX_3DSTRACEDLG_H__091D1F66_4382_11D4_90D5_00C0DFAE7D0A__INCLUDED_)
#define AFX_3DSTRACEDLG_H__091D1F66_4382_11D4_90D5_00C0DFAE7D0A__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000
// 3dsTraceDlg.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// C3dsTraceDlg dialog

#include "resource.h"
#include <afxwin.h>
#include <afxcmn.h>
#include "3dsfile2.h"

class C3dsTraceDlg : public CDialog
{
  istream &str;
  C3dsChunk *main;
  CImageList ilist;
// Construction
public:
	char * GetChunkName(CHUNKID id);
	void CreateChunkTree(HTREEITEM upper, C3dsChunk *chunk);
	C3dsTraceDlg(C3dsChunk *m, istream &s, CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(C3dsTraceDlg)
	enum { IDD = IDD_3DSVIEW };
	CTreeCtrl	tree;
	CString	m_Data;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(C3dsTraceDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(C3dsTraceDlg)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Developer Studio will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_3DSTRACEDLG_H__091D1F66_4382_11D4_90D5_00C0DFAE7D0A__INCLUDED_)
