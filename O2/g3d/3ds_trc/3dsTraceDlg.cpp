// 3dsTraceDlg.cpp : implementation file
//

#include "3dsTraceDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// C3dsTraceDlg dialog


C3dsTraceDlg::C3dsTraceDlg(C3dsChunk *m, istream &s,CWnd* pParent /*=NULL*/)
	: CDialog(C3dsTraceDlg::IDD, pParent),main(m),str(s)
{
	//{{AFX_DATA_INIT(C3dsTraceDlg)
	m_Data = _T("");
	//}}AFX_DATA_INIT
}


void C3dsTraceDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(C3dsTraceDlg)
	DDX_Control(pDX, IDC_STRUCT, tree);
	DDX_Text(pDX, IDC_DATA, m_Data);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(C3dsTraceDlg, CDialog)
	//{{AFX_MSG_MAP(C3dsTraceDlg)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// C3dsTraceDlg message handlers

void C3dsTraceDlg::CreateChunkTree(HTREEITEM upper, C3dsChunk *chunk)
  {
  char *chunkname=GetChunkName(chunk->GetChunkID());
  int chunktype=chunk->GetChunkType();
  HTREEITEM it=tree.InsertItem(TVIF_IMAGE|TVIF_PARAM|TVIF_SELECTEDIMAGE|TVIF_TEXT,
	chunkname,chunktype,chunktype,0,0,(LPARAM)chunk,upper,TVI_LAST);
  if (chunk->NextChunk()) CreateChunkTree(upper,chunk->NextChunk());
  if (chunk->SubChunk()) CreateChunkTree(it,chunk->SubChunk());
  }

char * C3dsTraceDlg::GetChunkName(CHUNKID id)
  {
  static char szBuff[256];
  sprintf(szBuff,"Chunk %4X",id);
  return szBuff;
  }

BOOL C3dsTraceDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here

/*	ilist.Create(16,16,ILC_COLOR4|ILC_MASK,0,3);
	ilist.Add(::LoadIcon(GetModuleHandle(NULL),MAKEINTRESOURCE(IDI_DATA)));
	ilist.Add(::LoadIcon(GetModuleHandle(NULL),MAKEINTRESOURCE(IDI_SUBDIRDATA)));
	ilist.Add(::LoadIcon(GetModuleHandle(NULL),MAKEINTRESOURCE(IDI_SUBDIR)));
	tree.SetImageList(&ilist,TVSIL_NORMAL);
	CreateChunkTree(NULL,main);*/

	return FALSE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}
