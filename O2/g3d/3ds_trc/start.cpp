//#include "globals.h"
//#include "3dsfile.h"
#include "3dsfile2.h"
#include "resource.h"

#include <fstream.h>
#include <windows.h>
#include <commctrl.h>
#include <string.h>
#include <stdio.h>
#include <strstrea.h>
#include <malloc.h>

char *names[65536];
static char szBuff[1024];
static char streambuff[65536];

static char *nameof3ds;

static void ReadNames()
  {
  FILE *f=fopen("3dsnames.txt","r");
  memset(names,0,sizeof(names));
  if (f==NULL) return;
  int i;
  do
	{
	i=-1;
	fscanf(f,"%s %X", szBuff,&i);
	if (i>=0) names[i]=strdup(szBuff);	
	}
  while (i>=0);
  fclose(f);
  }

char *GetChunkName(CHUNKID id)
  {
  if (names[(int)id]!=NULL) return names[(int)id];
  sprintf(szBuff,"Chunk %04X",id);
  return szBuff;
  }

void CreateChunkTree(HWND tree, HTREEITEM upper, C3dsChunk *chunk)
  {
  char *chunkname=GetChunkName(chunk->GetChunkID());
  int chunktype=chunk->GetChunkType();

  static TV_INSERTSTRUCT is;

  is.hParent=upper;
  is.hInsertAfter=TVI_LAST;
  is.item.mask=TVIF_IMAGE|TVIF_PARAM|TVIF_SELECTEDIMAGE|TVIF_TEXT;
  is.item.pszText=chunkname;
  is.item.iImage=is.item.iSelectedImage=chunktype;
  is.item.lParam=(LPARAM)chunk;
  HTREEITEM it=TreeView_InsertItem(tree,&is);	
  if (chunk->NextChunk()) CreateChunkTree(tree,upper,chunk->NextChunk());
  if (chunk->SubChunk()) CreateChunkTree(tree,it,chunk->SubChunk());
  }

HIMAGELIST ilist;

void OnChangeTree(HWND dlg, HWND tree)
  {
  HTREEITEM tr=TreeView_GetSelection(tree);
  static HTREEITEM trl=NULL;
  TV_ITEM it;
  if (tr==NULL || trl==tr) return;
  trl=tr;
  it.mask=TVIF_HANDLE ;
  it.hItem=tr;
  TreeView_GetItem(tree,&it);
  strstream st(streambuff,sizeof(streambuff)-1,ios::out);
  C3dsChunk *ch=(C3dsChunk *)it.lParam;
  if (ch==NULL) return;
  SetCursor(LoadCursor(NULL,IDC_WAIT));
  ch->Inspect(st);
  int i=0;
  st.write((char *)&i,1);
  streambuff[sizeof(streambuff)-1]=0;
  SetDlgItemText(dlg,IDC_DATA,streambuff);
  }

HFONT FixedFont()
  {
  LOGFONT lf;
  memset(&lf,0,sizeof(lf));
  lf.lfHeight = -12;
  lf.lfWeight=400;
  lf.lfCharSet=ANSI_CHARSET;
  lf.lfOutPrecision=OUT_DEFAULT_PRECIS ;
  lf.lfClipPrecision=CLIP_DEFAULT_PRECIS;
  lf.lfQuality=DEFAULT_QUALITY;
  lf.lfPitchAndFamily=FIXED_PITCH;
  strcpy(lf.lfFaceName,"Courier");
  return CreateFontIndirect(&lf);
  }

static void SetTopicName(HWND hWnd)
  {
  int size=strlen(nameof3ds)+100;
  char *c=(char *)alloca(size);
  GetWindowText(hWnd,c,size);
  strcat(c," - ");
  strcat(c,nameof3ds);
  SetWindowText(hWnd,c);
  }

LRESULT DlgProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
  {
  static HFONT viewfont;  
  switch (msg)
	{
	case WM_INITDIALOG:
	  {
	  ilist=ImageList_Create(16,16,ILC_COLOR4|ILC_MASK,0,3);
	  ImageList_AddIcon(ilist,LoadIcon(GetModuleHandle(NULL),MAKEINTRESOURCE(IDI_DATA)));
	  ImageList_AddIcon(ilist,LoadIcon(GetModuleHandle(NULL),MAKEINTRESOURCE(IDI_SUBDIRDATA)));
	  ImageList_AddIcon(ilist,LoadIcon(GetModuleHandle(NULL),MAKEINTRESOURCE(IDI_SUBDIR)));
	  HWND tree=GetDlgItem(hWnd,IDC_STRUCT);
	  TreeView_SetImageList(tree,ilist,TVSIL_NORMAL);
	  CreateChunkTree(tree,NULL,(C3dsChunk *)lParam);
	  viewfont=FixedFont();
	  SendDlgItemMessage(hWnd,IDC_DATA,WM_SETFONT,(WPARAM)viewfont,0);
	  SetTopicName(hWnd);
	  return 0;
	  }
	case WM_COMMAND:
	  switch (LOWORD(wParam))
		{
		case IDCANCEL: EndDialog(hWnd,0);DeleteObject(viewfont);break;
		default: return 0;
		}
	case WM_NOTIFY:
	  if (wParam==IDC_STRUCT)
		{
		NMHDR *p=(NMHDR *)lParam;
		if (p->code=TVN_SELCHANGED) OnChangeTree(hWnd,GetDlgItem(hWnd,IDC_STRUCT));break;
		}	  
	  return 0;
	default: return 0;
	}
  return 1;
  }

char *Open3DS()
  {
   static char szFile[256],szFileTitle[256];
   char szFilter[]=
          "3DS\0*.3ds\0"
          "All files\0*.*\0";
      OPENFILENAME OpenFileName;

      lstrcpy( szFile, TEXT(""));
      lstrcpy( szFileTitle, TEXT(""));

      OpenFileName.lStructSize       = sizeof(OPENFILENAME);
      OpenFileName.hwndOwner         = NULL;
      OpenFileName.hInstance         = NULL;
      OpenFileName.lpstrFilter       = szFilter;
      OpenFileName.lpstrCustomFilter = (LPTSTR) NULL;
      OpenFileName.nMaxCustFilter    = 0L;
      OpenFileName.nFilterIndex      = 1L;
      OpenFileName.lpstrFile         = szFile;
      OpenFileName.nMaxFile          = sizeof(szFile);
      OpenFileName.lpstrFileTitle    = szFileTitle;
      OpenFileName.nMaxFileTitle     = sizeof(szFileTitle);
      OpenFileName.lpstrInitialDir   = NULL;
      OpenFileName.lpstrTitle        = TEXT("Open 3DS");
      OpenFileName.nFileOffset       = 0;
      OpenFileName.nFileExtension    = 0;
      OpenFileName.lpstrDefExt       = TEXT("*.3ds");
      OpenFileName.lCustData         = 0;
      OpenFileName.Flags             = OFN_PATHMUSTEXIST
                                       | OFN_FILEMUSTEXIST
                                       | OFN_HIDEREADONLY;

      GetOpenFileName(&OpenFileName);
      if (szFile[0]) return szFile;
	  return NULL;
  }


  

int  APIENTRY WinMain (HINSTANCE hInstance, HINSTANCE hPrevInstance,
                       LPSTR lpCmdLine, int nCmdShow)
  {
  
  ReadNames();
  nameof3ds=Open3DS();
  if (nameof3ds==NULL) return 0;
  ifstream in(nameof3ds,ios::in|ios::nocreate|ios::binary);
  if (!in.is_open()) abort();
  C3dsMaster *main=new C3dsMaster();
  main->Read3ds(in);
  C3dsPivot *p=dynamic_cast<C3dsPivot *>(main->FindChunkSub(PIVOT));
  InitCommonControls();
  DialogBoxParam(GetModuleHandle(NULL),MAKEINTRESOURCE(IDD_3DSVIEW),NULL,(DLGPROC)DlgProc,(LPARAM)main);
  delete main;
  return 0;
  }
