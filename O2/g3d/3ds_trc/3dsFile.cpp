// 3dsFile.cpp: implementation of the C3dsFile class.
//
//////////////////////////////////////////////////////////////////////



#include "globals.h"
#include "3dsFile.h"
#include "assert.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

static char szBuff[32768];

C3dsFile::C3dsFile(FILE *f)
{
this->f=f;
}

C3dsFile::~C3dsFile()
{

}

static char *ReadString(FILE *f)
  {
  char *c=szBuff;
  do	
	*c=getc(f);
  while (*c++);
  return szBuff;
  }	

char C3dsFile::ReadChunkHeader(S3DSCHUNKINFO& nfo)
  {
  long pos;
  nfo.chunkid=NULL_CHUNK;
  pos=ftell(f);
  if (!fread(&nfo.chunkid,2,1,f)) return -1;
  if (!fread(&nfo.nextchunk,4,1,f)) return -1;
  nfo.nextchunk+=pos;
  return 0;
  }

char C3dsFile::TraceChunks(long chunkend, void * data)
{
S3DSCHUNKINFO info;
while (ftell(f)<chunkend)
  {  
  ReadChunkHeader(info);
  printf("* %04X begin",(int)info.chunkid);
  switch(info.chunkid)
	{
	case OBJECT_NODE_TAG:
	case N_TRI_OBJECT:
	case N_AMBIENT_LIGHT:
	case N_CAMERA:
	case N_DIRECT_LIGHT:
	case KFDATA:
	case MAT_ENTRY:
	case MDATA:
	case MAT_SHADING:
	case M3DMAGIC: TraceChunks(info.nextchunk,NULL);break;
	case MAT_NAME: printf("# -- Material name: %s",ReadString(f));break;
	case MAT_AMBIENT: MatColor("Ambient",info.nextchunk);break;
	case MAT_DIFFUSE: MatColor("Diffuse",info.nextchunk);break;
	case MAT_SPECULAR: MatColor("Specular",info.nextchunk);break;
	case MAT_SHININESS: MatShininess("Shininess",info.nextchunk);break;
	case MAT_SHIN2PCT:MatShininess("Shin2pct",info.nextchunk);break;
	case MAT_SHIN3PCT:MatShininess("Shin3pct",info.nextchunk);break;
	case MAT_TRANSPARENCY:MatShininess("Transparency",info.nextchunk);break;
	case NAMED_OBJECT:NamedObject(info.nextchunk);break;
	case POINT_ARRAY: ReadVertexList(info.nextchunk);break;
	case TEX_VERTS: ReadTexCoords(info.nextchunk);break;
	case FACE_ARRAY: PolygonList(info.nextchunk);break;
    case MESH_MATRIX: ObjectMatrix();break;
	case FLOAT_PERCENTAGE:
	case LIN_COLOR_F:
	case COLOR_F: fread(data,sizeof(float),1,f);break;
	case LIN_COLOR_24:
	case COLOR_24: fread(data,3,1,f);break;
	case INT_PERCENTAGE: fread(data,2,1,f);break;	
	case KFSEG: FrameInfo();break;
	case KFCURTIME: CurTime();break;
	case NODE_HDR: NodeHeader();break;
	case PIVOT: Pivot();break;
	case INSTANCE_NAME: InstanceName(info.nextchunk);break;
	case POS_TRACK_TAG:PosTrack();break;
	case ROT_TRACK_TAG:RotTrack();break;
	case SCL_TRACK_TAG:SclTrack();break;
	case MAT_TEXMAP: MatMap("Texture",info.nextchunk);break;
	case MAT_REFLMAP: MatMap("Reflection",info.nextchunk);break;
	case MAT_BUMPMAP: MatMap("Bump",info.nextchunk);break;
	case MAT_SPECMAP: MatMap("Specular",info.nextchunk);break;
	case MAT_MAPNAME: {char *p=ReadString(f);memcpy(data,&p,sizeof(p));}break;
	case NODE_ID: AttachMaterial();break;
	case MSH_MAT_GROUP: MatGroup(info.nextchunk);break;
	case SMOOTH_GROUP: SmoothGroup(info.nextchunk);break;
	}
  printf("- end %04X",(int)info.chunkid);
  fseek(f,info.nextchunk,SEEK_SET);
  }
return 0;
}


char C3dsFile::NamedObject(long chunkend)
{
printf("# -- Object Name: %s",ReadString(f));
TraceChunks(chunkend,NULL);
return 0;
}

char C3dsFile::InstanceName(long chunkend)
{
printf("# -- Instance Name: %s",ReadString(f));
TraceChunks(chunkend,NULL);
return 0;
}

char C3dsFile::MatColor(char * colname, long chunkend)
{
S3DSRGB rgb;
TraceChunks(chunkend,&rgb);
printf("# -- Material %s Color r:%d g:%d b:%d",colname,rgb.r,rgb.g,rgb.b);
return 0;
}

char C3dsFile::MatShininess(char * name, long chunkend)
{
unsigned short value;
TraceChunks(chunkend,&value);
printf("# -- Material %s: %d",name,value);
return 0;
}

char C3dsFile::ReadVertexList(long chunkend)
{
unsigned short count;
unsigned int i;
fread(&count,2,1,f);
for (i=0;i<count;i++)
  {
  S3DSVECTOR v;
  fread(&v,sizeof(v),1,f);
  printf("# -- Vertex: x:%10.2f y:%10.2f z:%10.2f",v.x,v.y,v.z);
  }
return 0;
}

char C3dsFile::ReadTexCoords(long chunkend)
  {
  unsigned short count;
  unsigned int i;
  fread(&count,2,1,f);
  for (i=0;i<count;i++)
	{
	S3DSTEXCOORD v;
	fread(&v,sizeof(v),1,f);
	printf("# -- TexCoord: tu:%10.2f tv:%10.2f",v.tu,v.tv);
	}
  return 0;
  }

char C3dsFile::ObjectMatrix(void)
  {
  int i;
  for (i=0;i<4;i++)
	{
	S3DSVECTOR v;
	fread(&v,sizeof(v),1,f);
	printf("# -- MatrixRow: x:%10.2f y:%10.2f z:%10.2f",v.x,v.y,v.z);
	}
  return 0;
  }

char C3dsFile::PolygonList(long chunkend)
{
unsigned short count;
unsigned int i;
fread(&count,2,1,f);
for (i=0;i<count;i++)
  {
  S3DSTRIANGLE v;
  fread(&v,sizeof(v),1,f);
  printf("# -- Polygon: v1:%6d  v2:%6d  v2:%6d flags: %02X",v.v1,v.v2,v.v3,v.uk1);
  }
TraceChunks(chunkend,NULL);
return 0;
}


char C3dsFile::FrameInfo()
{
S3DSFRAMEINFO finfo;
fread(&finfo,sizeof(finfo),1,f);
printf("# -- KF_FrameInfo: start: %d end: %d",finfo.start,finfo.end);
return 0;
}

char C3dsFile::CurTime(void)
  {
  int i;
  fread(&i,sizeof(i),1,f);
  printf("# -- KF_CurTime: %d",i);
  return 0;
  }

char C3dsFile::NodeHeader()
{
char *objname=ReadString(f);
unsigned short u1,u2;
short hierarchy;
fread(&u1,2,1,f);
fread(&u2,2,1,f);
fread(&hierarchy,2,1,f);
printf("# -- NODE: \"%s\" hierarchy: %d, unknown1: %d, unknown2: %d",objname,hierarchy,u1,u2);
return 0;
}

char C3dsFile::Pivot()
{
S3DSPIVOT pivot;
fread(&pivot,sizeof(pivot),1,f);
printf("# -- PIVOT: %8.2f %8.2f %8.2f",
		pivot.pivotx,
		pivot.pivoty,
		pivot.pivotz
		);
return 0;
}

char C3dsFile::PosTrack()
{
S3DSTRACKHD trackhd;
S3DSPOSTRACK postrack;
unsigned long i;
fread(&trackhd,sizeof(trackhd),1,f);
for (i=0;i<trackhd.records;i++)
  {
 fread(&postrack,sizeof(postrack),1,f);
  printf("# -- POSTRACK: %d %8.2f %8.2f %8.2f frame: %d",
		postrack.uk1,
		postrack.xpos,
		postrack.ypos,
		postrack.zpos,
		postrack.frame);
  }
return 0;
}

static int RotFrame[]=
  {0,4,4,8,4,8,8,12,4,8,8,12,8,12,12,16};

char C3dsFile::RotTrack()
{
S3DSTRACKHD trackhd;
S3DSROTTRACK rottrack;
unsigned long i;
unsigned short l;
//float g;
fread(&trackhd,sizeof(trackhd),1,f);
for (i=0;i<trackhd.records;i++)
  {
  fread(&rottrack.frame,sizeof(rottrack.frame),1,f);
  fread(&l,2,1,f);
  assert(l<16);
  fseek(f,RotFrame[l],SEEK_CUR);
  fread(&rottrack.radius,sizeof(rottrack.radius),1,f);
  fread(&rottrack.xpos,sizeof(rottrack.xpos),1,f);
  fread(&rottrack.ypos,sizeof(rottrack.ypos),1,f);
  fread(&rottrack.zpos,sizeof(rottrack.zpos),1,f);
  printf("# -- ROTTRACK: %8.2f %8.2f %8.2f radius: %8.2f frame: %d",
		rottrack.xpos,
		rottrack.ypos,
		rottrack.zpos,
		rottrack.radius,
		rottrack.frame);
  }
return 0;
}

char C3dsFile::SclTrack()
{
S3DSTRACKHD trackhd;
S3DSPOSTRACK postrack;
unsigned long i;
fread(&trackhd,sizeof(trackhd),1,f);
for (i=0;i<trackhd.records;i++)
  {
 fread(&postrack,sizeof(postrack),1,f);
  printf("# -- SCALETRACK: %d %8.2f %8.2f %8.2f frame: %d",
		postrack.uk1,
		postrack.xpos,
		postrack.ypos,
		postrack.zpos,
		postrack.frame);
  }
return 0;
}

char C3dsFile::MatMap(char * matname, long chunkend)
{
char *buff;
TraceChunks(chunkend,&buff);
printf("# -- %s Map: %s",matname,buff);
return 0;

}

void C3dsFile::AttachMaterial()
{
unsigned short matid;

fread(&matid,2,1,f);
printf("# -- Material id: %d", matid);
}

char C3dsFile::MatGroup(long chunkend)
{
unsigned short count,i;		
printf("# -- Material group: %s", ReadString(f));
fread(&count,2,1,f);
for (i=0;i<count;i++)
  {
  unsigned short ushort;
  fread(&ushort,2,1,f);
  printf("# -- Face: %d",ushort);
  }
TraceChunks(chunkend,NULL);
return 0;
}

char C3dsFile::SmoothGroup(long chunkend)
{
while (ftell(f)<chunkend)
  {
  unsigned long group;
  fread(&group,4,1,f);
  printf("# -- Smooth group: %08X",group);
  }
return 0;
}
