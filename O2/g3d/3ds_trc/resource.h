//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by 3dsview.rc
//
#define IDD_3DSVIEW                     101
#define IDI_MAIN                        102
#define IDI_SUBDIRDATA                  103
#define IDI_DATA                        104
#define IDI_SUBDIR                      106
#define IDC_STRUCT                      1000
#define IDC_DATA                        1001

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        106
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1002
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
