#ifndef _G3D_MODULE_H_INCLUDED_
#define _G3D_MODULE_H_INCLUDED_

#ifdef __cplusplus
extern "C" {
#endif


typedef struct _tag_initstruct
  {
  long size;  //length of this struct
  int xres;	  //xresolution
  int yres;	  //yresolution
  int bpp;	  //bits per pixel (always 16)
  int freq;	  //vertical freq.
  long wnd;	  //window handle.  
  void **ImportUni;  //pointer to pointer to function for load UniFileFormat
  void *CreateUni;	//pointer to function for create-uni
  void *CreateMipMap;
  void *malloc;		//pointer to classic malloc function
  void *free;		//pointer to classic free function
  void *GetImportName;	//Returns name of imported file
  }TG3DINITSTRUCT;

typedef struct _tag_g3d_control
  {
  long size;
  long hInstance;
  char *pathname;
  struct _tag_g3d_control *next;
  char (*g3dplgInitHardware)(TG3DINITSTRUCT *initinfo);
  char (*g3dplgDoneHardware)(TG3DINITSTRUCT *initinfo);
  }TG3DPLUGINIT;

#define G3DPE_OK 0
#define G3DPE_FAILED -1
#define G3DPE_DROPINIT -2
#define G3DPE_DROPAPP -3

#ifdef __G3D_DEF_IMPORT
IMPORT(char,g3dInitPlugin,(TG3DPLUGINIT *loadinfo));
#else
char g3dInitPlugin(TG3DPLUGINIT *loadinfo);
#endif

#ifdef __cplusplus
  }
#endif
#endif