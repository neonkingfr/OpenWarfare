#ifndef _G3DIO_H_
#define _G3DIO_H_

#ifdef __cplusplus
extern "C" {
#endif


#define G3DIO_OK       0
#define G3DIO_NOMEMORY -41
#define G3DIO_DISKWRITEERROR -42
#define G3DIO_DISKREADERROR -43
#define G3DIO_INVALIDFORMAT -44
#define G3DIO_ERROR         -45
#define G3DIO_FILEOPENERROR -46
#define G3DIO_FILENOTFOUND -47
#define G3DIO_UNSUPPORTEDVERSION -48

//G3DIO dedi chyby z G3DMODEL a z G3D

int g3dioLoadObject(TSCENEDEF *out,FILE *f);
  //RAW cteni objektu. Funkce nekontroluje hlavicky ani pozice.
  //Z aktualni pozice souboru f se pokusi precist scenu.
  //Kdyz se to nepodari, tak se program zhrouti.
  //Doporucuji zabezpecit format, pred pouzitim, tj zajisti aby se necetly
  //nesmysly
int g3dioSaveObject(TSCENEDEF *def,FILE *f);
  //RAW zapis objektu. Funkce zapisuje scenu/objekt bez hlavicek na aktualni
  //pozici v souboru f.
char g3dioLoadModelEx(HMODEL model, char *filename, char *desc);
char g3dioSaveModelEx(HMODEL model, char *filename, char *desc);
char g3dioLoadModel(HMODEL model, FILE *f, char *desc);
char g3dioSaveModel(HMODEL model, FILE *f, char *desc);

char g3dioLoadModel2(HMODEL model,FILE *f,char *desc);
char g3dioSaveModel2(HMODEL model,FILE *f,char *desc);
char g3dioSaveModelEx2(HMODEL model, char *filename, char *desc);
char g3dioLoadModelEx2(HMODEL model, char *filename, char *desc);


#ifdef __cplusplus
}
#endif
#endif


