#include <stdlib.h>
#include <stdio.h>
#include <g3dlib.h>
#include <malloc.h>
#include "GScene2.h"
#include <stdarg.h>
#include "g3dpp.h"

struct SVertexStackItem  //32 bytes
  {
  float vert[3];
  char clipregion;
  bool transformed;
  short valid;
  float tvert[4];
  };

struct SNormalStackItem
  {
  float vector[3];
  };

struct SLightCacheItem
  {
  int pos,norm;
  float r[3];
  short valid;
  };

/////////////////////////////////

#define DEFLIGHTCACHESIZE 256

class CLightCache
  {
  protected:
	SLightCacheItem *lit;
	int size;
	short valid;
  public:
	CLightCache() {lit=NULL;SetLightCache(DEFLIGHTCACHESIZE);}
	void SetLightCache(int size) {this->size=size;delete [] lit;lit=NULL;}
	bool Invalidate();
	long HashValue(int pos, int norm)
	  {
	  return (pos+100001*norm)&(size-1);
	  }
	float *FindItem(int pos, int norm)
	  {
	  SLightCacheItem *it=lit+HashValue(pos,norm);
	  if (it->valid==valid && it->pos==pos && it->norm==norm) return it->r;
	  else return NULL;
	  }
	void CacheItem(int pos, int norm, float *color)
	  {
	  SLightCacheItem *it=lit+HashValue(pos,norm);
	  it->valid=valid;
	  it->pos=pos;
	  it->norm=norm;
	  memcpy(it->r,color,sizeof(it->r));
	  }
  };

bool CLightCache::Invalidate()
  {
  if (lit==NULL) 
	{
	lit=new SLightCacheItem[size];
	if (lit==NULL) return false;
	memset(lit,0,sizeof(SLightCacheItem)*size);
	valid=0;
	}
  valid++;
  if (valid==0)
	{
	memset(lit,0,sizeof(SLightCacheItem)*size);
	valid=1;
	};
  return true;
  }

struct SLightMapItem
  {
  float r,g,b;
  HVECTOR pos;
  HVECTOR dir;
  HVECTOR tpos;
  HVECTOR tdir;
  float radmin;
  float radmax;
  float range;
  float range2;
  long flags;
  };


static SLightMapItem *lmap=NULL;
static int lmapsize;
static int lmapalloc=lmapsize=0;

static SVertexStackItem *vstack=NULL;
static int vstacksize;
static int vstackalloc=vstacksize=0;

static float (*nstack)[4]=NULL;
static int nstacksize;
static int nstackalloc=nstacksize=0;

static const void **cstack=NULL;
static int cstacksize;
static int cstackalloc=cstacksize=0;

static unsigned long *coltab=NULL;
static int coltabsize;
static int coltaballoc=coltabsize=0;

static G3D_TLVERTEX2 *tlvertextab=NULL;
static int tltabsize;
static int tltaballoc=tltabsize=0;

_inline G3D_TLVERTEX2 *AllocVertexTab(int vcount)
  {
  if (tltaballoc>=vcount) return tlvertextab;
  while (tltaballoc<vcount)
	if (g3dExpandTable((void **)&tlvertextab,&tltaballoc,sizeof(G3D_TLVERTEX2))) return NULL;
  return tlvertextab;  
  }

_inline unsigned long *AllocColTab(int vcount)
  {
  unsigned long *p;
  while (coltaballoc<=coltabsize+vcount)
	if (g3dExpandTable((void **)&coltab,&coltaballoc,sizeof(unsigned long))) return NULL;
  p=coltab+coltabsize;
  coltabsize+=vcount;
  return p;
  }

static CLightCache lcache;

_inline bool AddNormal(HVECTOR v)
  {
  if (nstackalloc<=nstacksize)
	if (g3dExpandTable((void **)&nstack,&nstackalloc,sizeof(float)*4)) return false;
  CopyVektor(nstack[nstacksize],v);
  nstacksize++;
  return true;
  }

_inline bool AddColor(const void *col)
  {
  if (cstackalloc<=cstacksize)
	if (g3dExpandTable((void **)&cstack,&cstackalloc,sizeof(void *))) return false;
  cstack[cstacksize]=col;
  cstacksize++;
  return true;
  }

static float *_fastcall LightVertex(int pos, int norm)
  {
  float *f=lcache.FindItem(pos,norm);
  if (f) return f;
  static float lt[3];
  lt[0]=lt[1]=lt[2]=0.0f;
  float intensity,cosf;
  float l,l2;
  HVECTOR v;memcpy(v,vstack[pos].vert,sizeof(vstack[pos].vert));
  HVECTOR n;
  if (norm>=0) CopyVektor(n,nstack[norm]);
  SLightMapItem *s=lmap;
  for(int i=0;i<lmapsize;i++,s++)
	{
	do { if (s->flags & LGH_POINTLIGHT)
	  {
	  HVECTOR r;
	  
	  r[XVAL]=v[XVAL]-s->tpos[XVAL];
	  r[YVAL]=v[YVAL]-s->tpos[YVAL];
	  r[ZVAL]=v[ZVAL]-s->tpos[ZVAL];
	  l2=ModulVektoru2(r);
	  if (l2>s->range2) break;
	  cosf=-Scalar1(r,n); 
	  if (cosf<0.0f && norm>=0) break;
	  l=(float)sqrt(l2);
	  if (norm<0) cosf=1.0f;else cosf/=l;
	  intensity=(1.0f-l/s->range)*cosf;
	  }
	else if (s->flags & LGH_DIRECTLIGHT)
	  {
	  if (norm>=0) cosf=1.0f;else cosf=-Scalar1(s->tdir,n);
	  if (cosf<0.0f) break;
	  intensity=cosf;
	  }
	else if (s->flags & LGH_SPOTLIGHT)
	  {
	  HVECTOR r;
	  float spotf;
	  
	  r[XVAL]=v[XVAL]-s->tpos[XVAL];
	  r[YVAL]=v[YVAL]-s->tpos[YVAL];
	  r[ZVAL]=v[ZVAL]-s->tpos[ZVAL];
	  l2=ModulVektoru2(r);
	  if (l2>s->range2) break;
	  cosf=-Scalar1(r,n);
	  if (cosf<0.0f && norm<0) break;
	  l=(float)sqrt(l2);
	  if (norm>=0) cosf=1.0f;else cosf/=l;
	  spotf=Scalar1(s->tdir,r)/l;
	  spotf=(spotf-s->radmin)/(s->radmax-s->radmin);
	  if (spotf<0.0f) break;
	  if (spotf>1.0f) spotf=1.0f;
	  intensity=(1.0f-l/s->range)*cosf*spotf;
	  }
	else if (s->flags & LGH_AMBIENTLIGHT) intensity=1.0f;
	 lt[0]+=s->r*intensity;
	 lt[1]+=s->g*intensity;
	 lt[2]+=s->b*intensity;	
	}
	while (0);
	}
  lcache.CacheItem(pos,norm,lt);      
  return lt;
  }

#define CLIP_TOP 0x1
#define CLIP_LEFT 0x2
#define CLIP_BOTTOM 0x4
#define CLIP_RIGHT 0x8
#define CLIP_FRONT 0x10
#define CLIP_BACK 0x20

static void _fastcall ProcessVertex(SVertexProcessInfo& pi, SVertexStackItem& vw, LPHVECTOR tv=NULL)
  {
  float x,y,z,w;  
  char clipr;
  if (tv==NULL)
	{
	vw.vert[3]=1.0f;
	TransformVector(pi.mm,vw.vert,vw.tvert);
	w=1.0f/vw.tvert[WVAL];
	x=vw.tvert[XVAL]*=w;
	y=vw.tvert[YVAL]*=w;
	z=vw.tvert[ZVAL]*=w;
	vw.tvert[WVAL]=w;
	}
  else
	{
	x=vw.tvert[XVAL]=tv[XVAL];
	y=vw.tvert[YVAL]=tv[YVAL];
	z=vw.tvert[ZVAL]=tv[ZVAL];
	w=vw.tvert[WVAL]=tv[WVAL];
	}
  if (w<0.0f)
	{
	clipr=CLIP_FRONT;
	if (x<0.0) clipr|=CLIP_RIGHT;else clipr|=CLIP_LEFT;
	if (y<0.0) clipr|=CLIP_BOTTOM;else clipr|=CLIP_TOP;
	}
  else 
	{
	clipr=0;
	if (x<pi.lclip) clipr|=CLIP_LEFT;
	if (y<pi.tclip) clipr|=CLIP_TOP;
	if (x>pi.rclip) clipr|=CLIP_RIGHT;
	if (y>pi.bclip) clipr|=CLIP_BOTTOM;
	if (z<pi.nclip) clipr|=CLIP_FRONT;
	if (z>pi.fclip) clipr|=CLIP_BACK;
	}	
  vw.clipregion=clipr;
  vw.transformed=true;
  vw.valid=0;
  }

static int _fastcall AddVertexToStack(HVECTOR v3)
  {
  if (vstacksize>=vstackalloc)
	if (g3dExpandTable((void **)&vstack,&vstackalloc,sizeof(SVertexStackItem))) return -1;
  memcpy(vstack[vstacksize].vert,v3,sizeof(float)*3);
  vstack[vstacksize].transformed=false;
  return vstacksize++;
  }

#define ResetVertexStack() (vstacksize=0)

static HMATRIX mproj, mview, miworld;
static CGMatrixStack mworld;

extern "C" {int *g3dmGetAnimTickPointer();}

static bool _fastcall AddLight(const TLIGHTDEF& ldef)
  {
  if (!(ldef.light_flags & LGH_ACTIVE)) return true;
  SLightMapItem *li;
  if (lmapsize>=lmapalloc)
	if (g3dExpandTable((void **)&lmap,&lmapalloc,sizeof(SLightMapItem))) return false;
  li=lmap+lmapsize;
  lmapsize++;
  CopyVektor(li->pos,ldef.position);
  CopyVektor(li->dir,ldef.direction);
  CopyVektor(li->tpos,ldef.position);
  CopyVektor(li->tdir,ldef.direction);
  li->radmin=ldef.minradius;
  li->radmax=ldef.maxradius;
  li->range=ldef.range;
  li->range2=ldef.range*ldef.range;
  li->r=ldef.col_r;
  li->g=ldef.col_g;
  li->b=ldef.col_b;
  li->flags=ldef.light_flags;
  if (li->flags & LGH_SINEANIM)
	{
    int *tick=g3dmGetAnimTickPointer();
	float alpha=ldef.freq*tick[0];
	alpha=alpha-(float)floor(alpha);
	float fr,fg,fb;
	alpha*=2*FPI;
	fr=(float)sin(alpha+ldef.offs_r);
	if (ldef.offs_r!=ldef.offs_g) fg=(float)sin(alpha+ldef.offs_g);else fg=fr;
	if (ldef.offs_r!=ldef.offs_b) 
	  if (ldef.offs_g!=ldef.offs_b)
		fb=(float)sin(alpha+ldef.offs_b);
	  else
		fb=fg;
	else
	  fb=fr;
	li->r*=ldef.ampl_r*fr;
	li->g*=ldef.ampl_g*fg;
	li->b*=ldef.ampl_b*fb;
	}
  if (li->flags & LGH_TURNONOFFANIM)
	{
    int *tick=g3dmGetAnimTickPointer();
	float alpha=ldef.freq*tick[0];
	float fr=alpha+ldef.offs_r;
	float fg=alpha+ldef.offs_g;
	float fb=alpha+ldef.offs_b;
	fb=fb-(float)floor(fb);
	fg=fg-(float)floor(fg);
	fr=fr-(float)floor(fr);
	if (fr>0.5f) li->r+=ldef.ampl_r;
	if (fg>0.5f) li->g+=ldef.ampl_g;
	if (fb>0.5f) li->b+=ldef.ampl_b;
 	}
  return true;
  }

static void _fastcall TransformLights(LPHMATRIX mm)
  {
  SLightMapItem *li=lmap;
  for (int i=0;i<lmapsize;i++,li++)
	{
	if (li->flags & LGH_POINTLIGHT)
	  {
	  TransformVector(mm,li->pos,li->tpos);
	  CorrectVectorM(li->tpos);
	  }
	else if (li->flags & LGH_DIRECTLIGHT)
	  {
	  TransformVector(mm,li->dir,li->tdir);
	  CorrectVectorM(li->tdir);
	  }
	else  if (li->flags & LGH_SPOTLIGHT)
	  {
	  TransformVector(mm,li->pos,li->tpos);
	  CorrectVectorM(li->tpos);
	  TransformVector(mm,li->dir,li->tdir);
	  CorrectVectorM(li->tdir);
	  }
	}

  }

#include "GFaceSort.h"

class CGRenderSort: public CGFaceSort
  {
	CGRenderParser *notify;
  public:
	CGRenderSort(int c=32,float n=0.0f,float x=1.0f):
	  CGFaceSort(c,n,x){}
	void SetNotify(CGRenderParser *draw) {notify=draw;}
	virtual bool Enumerate(SGFaceInfo *finfo)
	  {
	  if (notify->SortedFaces(finfo)!=G3D_OK) return false;
	  return true;
	  }
  };

static CGRenderSort *sort=NULL;

static unsigned long GetVertexColor(void *mat, float *light)
  {
  TVERTEXCOLOR *vcol=(TVERTEXCOLOR *)mat;
  int r,g,b,a;
  if (mat)
	{
	r=(int)((light[0]*vcol->pohlc_r+vcol->emit_r)*256.0f);
	g=(int)((light[1]*vcol->pohlc_g+vcol->emit_g)*256.0f);
	b=(int)((light[2]*vcol->pohlc_b+vcol->emit_b)*256.0f);
	}
  else
	{
	r=(int)(light[0]*256.0f);
	g=(int)(light[1]*256.0f);
	b=(int)(light[2]*256.0f);
	}
  r=__min(r,255);
  r=__max(r,0);
  g=__min(g,255);
  g=__max(g,0);
  b=__min(b,255);
  b=__max(b,0);
  if (vcol->flags & TVCOL_ALPHA) 
	{
	a=(int)(vcol->alpha*256.0f);
	a=__min(a,255);
	a=__max(a,0);
	}
  else
	a=0xFF;
  return (a<<24)|(r<<16)|(g<<8)|b;
  }

int CGRenderParser::PrepareToRender()
  {
  if (!lcache.Invalidate()) return G3D_NOMEMORY;
  cam.FlushStack();
  coltabsize=lmapsize=vstacksize=nstacksize=cstacksize=0;
  si.lclip=(float)g3dQuerySettingInt(G3DQ_CLIPLEFT);
  si.rclip=(float)g3dQuerySettingInt(G3DQ_CLIPRIGHT);
  si.tclip=(float)g3dQuerySettingInt(G3DQ_CLIPTOP);
  si.bclip=(float)g3dQuerySettingInt(G3DQ_CLIPBOTTOM);
  si.nclip=(float)g3dQuerySettingInt(G3DQ_ZMIN);
  si.fclip=(float)g3dQuerySettingInt(G3DQ_ZMAX);
  CopyMatice(si.mm,cam.GetTransform());
  HMATRIX mc;
  SoucinMatic(cam.GetProjection(),cam.GetCanvas(),camtoscreen);
  CopyMatice(mc,camtoscreen);
  InverzeMatice(mc,screentocam);
 // SoucinMatic(screentocam,camtoscreen,mc);
  lglevel=0;
  dblsided=false;
  sort->Reset();
  matcallback=(void *)GetVertexColor;
  matsize=sizeof(TVERTEXCOLOR);
  txi=NULL;
  return G3D_OK;
  }

LPHVECTOR TransformVertex(SVertexProcessInfo& si,int index)
  {
  SVertexStackItem *it=vstack+vstacksize-index;
  if (!it->transformed) ProcessVertex(si,*it);
  return it->tvert;
  }

char GetClipFlags(SVertexProcessInfo& si, int index)
  {
  SVertexStackItem *it=vstack+vstacksize-index;
  if (!it->transformed) ProcessVertex(si,*it);
  return it->clipregion;
  }

int CGRenderParser::AddPosition(const LPHVECTOR v, int count)
  {
  LPHVECTOR t=(LPHVECTOR )v;
  while (count--) 
	{
	if (AddVertexToStack(t)<0) return G3D_NOMEMORY;
	t+=3;
	}
  return G3D_OK;
  }

int CGRenderParser::AddNormal(const LPHVECTOR v, int count)
  {
  LPHVECTOR t=(LPHVECTOR )v;
  while (count--)
	{
	if (::AddNormal(t)==false) return G3D_NOMEMORY;
	t+=3;
	}
  return G3D_OK;
  }

int CGRenderParser::PushMatrix(const LPHMATRIX m,bool left)
  {
  cam.PushMatrix(m,!left);
  lglevel++;
  return G3D_OK;
  }

int CGRenderParser::PopMatrices(int count)
  {
  while (count--) {cam.PopMatrix();lglevel--;}
  if(lglevel<0) lglevel=-0x7fffffff;
  return G3D_OK;
  }

int CGRenderParser::AddMaterial(const void *material)
  {
  if (AddColor(material)==false) return G3D_NOMEMORY;
  return G3D_OK;
  }

int CGRenderParser::AddBillboard(int v, int count, float *data, bool trns)	
  {
  LPHVECTOR curvert,vos;
  HVECTOR trnp;
  HVECTOR nx;
  HVECTOR vtrn;
  curvert=TransformVertex(si,v);
  vos=vstack[vstacksize-v].vert;
  CopyVektor(nx,curvert);
  nx[WVAL]=1.0f;
  TransformVector(screentocam,nx,vtrn);
  CorrectVectorM(vtrn);
  for (int i=0;i<count;i++)
	{
	int pos;
	nx[XVAL]=vtrn[XVAL]+*data++;
	nx[YVAL]=vtrn[YVAL]+*data++;
	nx[ZVAL]=vtrn[ZVAL];
	nx[WVAL]=1.0f;
	if (trns)
	  {
	  TransformVector(camtoscreen,nx,trnp);
	  CorrectVectorM(trnp);
	  }
	pos=AddVertexToStack(vos);
	if (pos<0) return G3D_NOMEMORY;
	ProcessVertex(si,vstack[pos],trnp);
	}
  return G3D_OK;
  }

int CGRenderParser::AddBillboardPos(int v, int count, float *data)
  {
  return AddBillboard(v,count,data,true);
  }

int CGRenderParser::AddBillboardScr(int v, int count, float *data)
  {
  return AddBillboard(v,count,data,false);
  }

int CGRenderParser::SetTPosition(const LPHVECTOR v, int count)
  {
  LPHVECTOR p=(LPHVECTOR)v;
  while (count--)
	{
	int pos=AddVertexToStack(p);
	if (pos<0) return G3D_NOMEMORY;
	p+=4;
	ProcessVertex(si,vstack[pos],p);
	p+=4;
	}
  return G3D_OK;
  }

int CGRenderParser::SetTXITable(HTXITABLE txi)
  {
  this->txi=txi;
  return G3D_OK;
  }

int CGRenderParser::Light(const TLIGHTDEF *def)
  {
  if (AddLight(*def)==false) return G3D_NOMEMORY;
  return G3D_OK;
  }

static int sorttable[]=
  {
  CGFF_FORCESORT,
  CGFF_FORCESORT|CGFF_ALPHAFACE|CGFF_ANTIALIASED,
  0,
  0,
  };


int CGRenderParser::Face(const FaceInfo *face,const FaceVertex *fvert)
  {
  int i;
  const FaceVertex *p;
  char clipflags;

  if (lglevel) 
	{
	TransformLights(cam.GetLightTransform());
	CopyMatice(si.mm,cam.GetTransform());
	lglevel=0;
	}
  int offset=sizeof(FaceVertex)+2*sizeof(float)*(face->tcount-1);
  if (sorttable[flags & 0x3] & face->flags || flags & G3DR_SORTALWAYS)
	{
	float minz=(float)si.fclip;
	float maxz=(float)si.nclip;
	for (i=0,p=fvert,clipflags=-1;i<face->vcount;i++,p=(const FaceVertex *)((char *)p+offset))
	  {
	  LPHVECTOR vv;	  
      vv=TransformVertex(si,p->pos);	  
	  clipflags&=GetClipFlags(si,p->pos);
	  if (minz>vv[ZVAL]) minz=vv[ZVAL];
	  if (maxz<vv[ZVAL]) maxz=vv[ZVAL];
	  }
	if (clipflags!=0) return G3D_OK;
	SGFaceInfo *f=sort->AddFace(maxz);
	if (f!=NULL)
	  {
	  unsigned long *lp=AllocColTab(face->vcount+1);
	  if (lp!=NULL)	  
		{
		f->lights=lp-coltab;
		*lp++=vstacksize;
		for (i=0,p=fvert;i<face->vcount;i++,p=(const FaceVertex *)((char *)p+offset))
		  {
		  float *svetlo=LightVertex(vstacksize-p->pos,p->norm<0?-1:nstacksize-p->norm);
		  *lp++=((GETVERTEXCOLORCALLBACK)matcallback)(p->col>=0?cstack[cstacksize-p->col]:NULL,svetlo);
		  }
		f->streamptr=(void *)face;
		f->minz=minz;
		f->maxz=maxz;
		f->inside=NULL;
		f->txi=txi;
		return G3D_OK;
		}
	  }
	}
  for (i=0,p=fvert,clipflags=-1;i<face->vcount;i++,p=(FaceVertex *)((char *)p+offset))
	if ((clipflags&=GetClipFlags(si,p->pos))==0) break;
  if (clipflags) return G3D_OK;
  char mode=face->face & 0xF;
  if (txi==NULL)
	for (i=0;i<face->tcount;i++) g3dtSelectTexture(i,face->texlist[i]);
  else
	for (i=0;i<face->tcount;i++) g3dtSelectTexture(i,g3dTxiGetTexture(txi,face->texlist[i]));
  if (face->face & G3D_DOUBLESIDE)
	{if (!dblsided) curcull=g3dSetCullMode(0);}
  else
	if (dblsided) g3dSetCullMode(curcull);
  G3D_TLVERTEX2 *tl2=AllocVertexTab(face->vcount),*tb=tl2;
  if (tl2==NULL) return G3D_NOMEMORY;
  for (i=0,p=fvert;i<face->vcount;i++,p=(FaceVertex *)((char *)p+offset))
	{
	float *svetlo=LightVertex(vstacksize-p->pos,p->norm<0?-1:nstacksize-p->norm);
	CopyVektor(tl2->coord,TransformVertex(si,p->pos));
    tl2->color=((GETVERTEXCOLORCALLBACK)matcallback)(p->col>=0?cstack[cstacksize-p->col]:NULL,svetlo);
	memcpy(tl2->tuv,p->tuv,face->tcount*sizeof(float)*2);
	tl2++;
	}
  g3dDrawPrimitive(face->tcount|G3DDM_DIFFUSE|G3DDM_XYZQ,mode,tb,face->vcount);
  return G3D_OK;
  }

void CGRenderParser::PostRender()
  {
  flags=G3DR_NEVERSORT;
  sort->Scan();    
  }

int CGRenderParser::SortedFaces(SGFaceInfo * fi)
  {
  FaceInfo *face=(FaceInfo *)fi->streamptr;
  FaceVertex *fv=(FaceVertex *)((char *)(face+1)+sizeof(long)*(face->tcount-1));
  int i;
  char mode=face->face & 0xF;
  if (fi->txi==NULL)
	for (i=0;i<face->tcount;i++) g3dtSelectTexture(i,face->texlist[i]);
  else
	for (i=0;i<face->tcount;i++) g3dtSelectTexture(i,g3dTxiGetTexture(fi->txi,face->texlist[i]));
  if (face->face & G3D_DOUBLESIDE)
	{if (!dblsided) curcull=g3dSetCullMode(0);}
  else
	if (dblsided) g3dSetCullMode(curcull);
  unsigned long col=fi->lights;
  vstacksize=coltab[col++];
  FaceVertex *p;
  int offset=sizeof(FaceVertex)+2*sizeof(float)*(face->tcount-1);
  G3D_TLVERTEX2 *tl2=AllocVertexTab(face->vcount),*tb=tl2;
  if (tl2==NULL) return G3D_NOMEMORY;
  for (i=0,p=fv;i<face->vcount;i++,p=(FaceVertex *)((char *)p+offset))
	{
	CopyVektor(tl2->coord,TransformVertex(si,p->pos));
    tl2->color=coltab[col++];
	memcpy(tl2->tuv,p->tuv,face->tcount*sizeof(float)*2);
	tl2++;
	}
  g3dDrawPrimitive(face->tcount|G3DDM_DIFFUSE|G3DDM_XYZQ,mode,tb,face->vcount);
  return G3D_OK;
  }


int g3dRender(CGExecBuffer &scn, CGSceneEnv &env, CGCamera2 &cam, unsigned long flags)
  {
  CGRenderParser rnd(scn,env,cam,flags);
  if (sort==NULL) sort=new CGRenderSort(32,(float)g3dQuerySettingInt(G3DQ_ZMIN),(float)g3dQuerySettingInt(G3DQ_ZMAX));
  sort->SetNotify(&rnd);
  int res=rnd.PrepareToRender();
  if (res) return res;
  res=rnd.Parse(env);
  if (res) return res;
  rnd.PostRender();
  return res;
  }