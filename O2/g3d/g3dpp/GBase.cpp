// GBase.cpp: implementation of the CGLight class.
//
//////////////////////////////////////////////////////////////////////

#include "GBase.h"


//////////////////////////////////////////////////////////////////////
// CGVertexCol Class
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////


void CGVertexCol::SetColor(TLIGHTDEF& l)
  {
  col.range=l.range;
  col.minradius=l.minradius;
  col.maxradius=l.maxradius;
  col.emit_r=l.col_r;
  col.emit_g=l.col_g;
  col.emit_b=l.col_b;
  col.offs_r=l.offs_r;
  col.offs_g=l.offs_g;
  col.offs_b=l.offs_b;
  col.ampl_r=l.ampl_r;
  col.ampl_g=l.ampl_g;
  col.ampl_b=l.ampl_b;
  col.freq=l.freq;
  col.flags=TVCOL_LIGHT;
  col.light_flags=l.light_flags;
  }
CGVertexCol::CGVertexCol(TLIGHTDEF& l)
  {
  SetColor(l);
  }

CGVertexCol::CGVertexCol(CGLight& l)
  {
  HLIGHTDEF ld=l;
  SetColor(*ld);
  }

//////////////////////////////////////////////////////////////////////
// CGCamera Class
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

void CGCamera::LookAt(HVECTOR pos, float rotz, float rotx, float distance)
  {
  HMATRIX m1,m2,m3;
  Translace(m1,pos[XVAL],pos[YVAL],pos[ZVAL]);
  RotaceZ(m2,rotz);
  SoucinMatic(m1,m2,m3);
  RotaceX(m2,rotx);
  SoucinMatic(m3,m2,m1);
  Translace(m2,0,0,distance);
  SoucinMatic(m1,m2,curview);
  }



void CGCamera::ObjectMatrix(HMATRIX matr)
{
HMATRIX copy;
CopyMatice(copy,matr);
InverzeMatice(copy,curview);
}


void CGCamera2::SetCanvas(float xmid, float ymid, float scalex, float scaley)
  {
  LPHMATRIX matx=canvas;
  JednotkovaMatice(matx);
  float minz,maxz;
  if  (g3dIsImplemented(g3dQuerySettingInt))
	{
	minz=(float)g3dQuerySettingInt(G3DQ_ZMIN);
	maxz=(float)g3dQuerySettingInt(G3DQ_ZMAX);
	}
  else
	{
	minz=0.0f;
	maxz=1.0f;
	}
  matx[3][0]=xmid;
  matx[3][1]=ymid;
  matx[3][2]=minz;
  matx[0][0]=scalex;
  matx[1][1]=scaley;
  matx[2][2]=maxz-minz;
  }


void CGCamera2::FlushStack()
  {
  all.Flush();
  light_inv.Flush();
  CGMatrix btw(curproj,canvas);
  SoucinMatic(curview,btw,all.PushL());
  }

char CGCamera2::PushMatrix(LPHMATRIX mm, bool pushright)
  {
  HMATRIX mt;
  CopyMatice(mt,mm);
  if (pushright) 
	{
	all.PushR(mm);
	return InverzeMatice(mt,light_inv.PushR());
	}
  else 
	{
	all.PushL(mm);  
	return InverzeMatice(mt,light_inv.PushL());
	}  
  }