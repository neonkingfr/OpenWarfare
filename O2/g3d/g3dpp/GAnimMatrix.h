// GAnimMatrix.h: interface for the CGAnimMatrix class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_GANIMMATRIX_H__FF97B5A5_42EA_11D4_90D5_00C0DFAE7D0A__INCLUDED_)
#define AFX_GANIMMATRIX_H__FF97B5A5_42EA_11D4_90D5_00C0DFAE7D0A__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include "GAnim.h"

#define CGAM_MATRIX 0x0
#define CGAM_QKEY 0x1

class CGAnimMatrix : public CGAnim  
{
	int mtype;
public:
	int parent;
	HMATRIX temp;
	bool FindFrame(int frame, LPHMATRIX mm);
	bool GetFrame(SGAnimState& st, LPHMATRIX mm);	
	LPHMATRIX GetMatrix(int index);
	int GetMatrixType() {return mtype;}
	bool Add(int frame, LPHMATRIX mm) {return CGAnim::Add(frame,(void *)mm);}		
    CGAnimMatrix(int matrixtype, int initial=0, int grow=256):
	  CGAnim(sizeof(HMATRIX),initial,grow),mtype(matrixtype) {parent=-1;}
	friend ostream& operator<< (ostream& out, const CGAnimMatrix& data);
	friend istream& operator>> (istream& in, CGAnimMatrix& data);
	

};

#endif // !defined(AFX_GANIMMATRIX_H__FF97B5A5_42EA_11D4_90D5_00C0DFAE7D0A__INCLUDED_)
