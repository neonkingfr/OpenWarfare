// GFaceSort.cpp: implementation of the CGFaceSort class.
//
//////////////////////////////////////////////////////////////////////

#include <malloc.h>
#include <string.h>
#include "GFaceSort.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CGFaceSort::CGFaceSort(int c,float n,float x)
  {
  facelist=NULL;
  lines=0;
  cols=c;
  min=n;
  max=x;
  allocated=0;
  }

CGFaceSort::~CGFaceSort()
  {
  for (int i=0;i<lines;i++) delete [] facelist[i];
  free(facelist);
  }


SGFaceInfo *CGFaceSort::AddFace(float z)
  {
  if (lines==0) 
	if (AddLine()==NULL) return NULL;
  return AddFace(facelist[0],z,min,max);
  }

SGFaceInfo * CGFaceSort::AddLine()
  {
  if (lines>=allocated)
	{
	SGFaceInfo **p=(SGFaceInfo **)realloc(facelist,sizeof(SGFaceInfo *)*(allocated+1));
	if (p==NULL) return NULL;
	facelist=p;
	if ((facelist[lines]=new SGFaceInfo[cols])==NULL) return false;
	allocated++;
	}
  memset(facelist[lines],0,sizeof(SGFaceInfo)*cols);
  return facelist[lines++];
  }

SGFaceInfo *CGFaceSort::AddFace(SGFaceInfo * fc, float z, float min, float max)
  {
  float p=(max-min)/cols;
  int index=(int)(((z-min)/(max-min))*cols);
  if (index<0 || index>=cols) return NULL;
  SGFaceInfo &q=fc[index];
  if (q.streamptr==NULL)
	{
	return &q;
	}
  else 
	{
	if (q.inside==NULL)
	  if ((q.inside=AddLine())==NULL) return NULL;
	return AddFace(fc[index].inside,z,min+p*index,min+p*(index+1));
	}
  return NULL;
  }


bool CGFaceSort::Scan(SGFaceInfo *prvek,SGFaceInfo *midl)
  {  
  prvek+=cols-1;
  for (int i=cols-1;i>=0;i--,prvek--) if (prvek->streamptr)
	{
	if (midl && prvek->maxz<midl->maxz) 
	  {
	  if (!Enumerate(midl)) return false;
	  midl=NULL;
	  }
	if (prvek->inside) 	  
	  Scan(prvek->inside,prvek);
	else 
	  if (!Enumerate(prvek)) return false;
	}
  if (midl) if (!Enumerate(midl)) return false;
  return true;
  }

