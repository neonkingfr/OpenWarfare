// MsgObjects.h: interface for the CGInt class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MSGOBJECTS_H__70B49D25_041B_11D4_90D5_00C0DFAE7D0A__INCLUDED_)
#define AFX_MSGOBJECTS_H__70B49D25_041B_11D4_90D5_00C0DFAE7D0A__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include "Messager.h"
#include "Message.h"

class CGInt : public CGMsgLink  
  {
	int value;
public:	
	CGInt(int val=0) {value=val;}
	int operator=(int val) {value=val;SendMsg(&(CGMsgInt(value)));return value;}
	operator int() {return value;}
	virtual void Input(CGMessage *msg, int numids, ...) 
	  {if (msg->GetMsgID()==CGMSG_GENERAL) (*this)=msg->AsInt();}
	virtual void NeedData(CGMsgLink *sender, int numids, ...) 
	  {SendMsgTo(sender,&(CGMsgInt(value)));}	
  };

class CGFloat: public CGMsgLink
  {
	float value;
public:	
	CGFloat(float val=0) {value=val;}
	float operator=(float val) {value=val;SendMsg(&(CGMsgFloat(value)));return val;}
	operator float() {return value;}
	virtual void Input(CGMessage *msg, int numids, ...) 
	  {if (msg->GetMsgID()==CGMSG_GENERAL) (*this)=msg->AsFloat();}
	virtual void NeedData(CGMsgLink *sender, int numids, ...) 
	  {SendMsgTo(sender,&(CGMsgFloat(value)));}	
  };

class CGString: public CGMsgLink
  {
	int grow;
	int len;
	int alloc;
	char *text;
    bool Expand(int how);
  public:	
	int GetLength() {return len-1;}
	void SetAt(int index, char what) 
	  {if (index<len) {text[index]=what;SendMsg(&(CGMsgString(text)));}}
	char GetAt(int index) {if (index>=len) return 0;else return text[index];}
	char operator[] (int index) {return GetAt(index);}
	const char *operator+=(const char *);
	operator const char *() {return (const char *)text;}
	const char *operator= (const char *txt);
	CGString(const CGString &other);
	CGString(char *text=NULL, int grow=16, int prealloc=0);
	virtual  ~CGString();
	virtual void Input(CGMessage *msg, int numids, ...);
	virtual void NeedData(CGMsgLink *sender, int numids, ...);
  };

class CGEvent: public CGMsgLink
  {
	int pulse;
  public:
  	CGEvent(CGMsgLink *call, int pulse) {AttachToClient(call);this->pulse=pulse;}
	virtual void Input(CGMessage *msg, int numids,...);
  	virtual void Income(CGMsgLink *link,CGMessage *msg)
	  {SendMsg(&CGMessage(CGMSG_COMMAND));}
  };

#endif // !defined(AFX_MSGOBJECTS_H__70B49D25_041B_11D4_90D5_00C0DFAE7D0A__INCLUDED_)
