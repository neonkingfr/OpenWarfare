// GParser.h: interface for the CGParser class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_GPARSER_H__18785462_33C5_11D4_90D5_00C0DFAE7D0A__INCLUDED_)
#define AFX_GPARSER_H__18785462_33C5_11D4_90D5_00C0DFAE7D0A__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include "G3DObject.h"
#include "GScene2.h"

#define CGP_UNKNOWNCOMMAND -1000
#define CGP_OK 0

#pragma pack(1)
class CGParser : public CGSceneStructs  
  {
  protected:
	void *matcallback;
	long  matsize;
	CGExecBuffer *scene;
  public:
	virtual int DoCommand(unsigned char command, unsigned char *data, CGSceneEnv& env, int& size);
	virtual int AddPosition(const LPHVECTOR v,int count) {return 0;}
	virtual int AddNormal(const LPHVECTOR v,int count) {return 0;}
	virtual int PushMatrix(const LPHMATRIX m,bool left) {return 0;}
	virtual int PopMatrices(int count) {return 0;}
	virtual int AddMaterial(const void *material) {return 0;}
	virtual int AddBillboardPos(int v, int count, float *data) {return 0;}
	virtual int AddBillboardScr(int v, int count, float *data) {return 0;}
	virtual int SetTPosition(const LPHVECTOR v, int count) {return 0;}
	virtual int SetTXITable(HTXITABLE txi) {return 0;}
	virtual int Light(const TLIGHTDEF *def) {return 0;}
	virtual int Face(const FaceInfo *face,const FaceVertex *fvert) {return 0;}	
	int Parse(CGSceneEnv& env);
	CGParser(CGExecBuffer& scn):scene(&scn) {}
  };
#pragma pack()

#endif // !defined(AFX_GPARSER_H__18785462_33C5_11D4_90D5_00C0DFAE7D0A__INCLUDED_)
