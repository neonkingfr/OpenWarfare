// Message.h: interface for the CMessage class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MESSAGE_H__56B6F1E2_FF46_11D3_90D5_00C0DFAE7D0A__INCLUDED_)
#define AFX_MESSAGE_H__56B6F1E2_FF46_11D3_90D5_00C0DFAE7D0A__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include "G3DObject.h"

class CGMessage : public CG3DObject  
  {
	int messageid;
  public:
	int GetMsgID() {return messageid;}
	virtual int AsInt() {return 0;}
	virtual float AsFloat() {return 0.0f;}
	virtual char AsChar() {return 0;}
	virtual double AsDouble() {return 0.0;}
	virtual void *AsPtr() {return NULL;}
	virtual const char *AsString(char *buffer, int bufsize) {return "";}
	operator int() {return AsInt();}
	operator float()  {return AsFloat();}
	operator char() {return AsChar();}
	operator double()  {return AsDouble();}
	operator void *() {return AsPtr();}
	CGMessage(int msg=1) {messageid=msg;}
  };

class CGMsgInt : public CGMessage  
{
  int value;
public:
  virtual const char *AsString(char *buffer, int bufsize);
  CGMsgInt(int value,int msg=1): CGMessage(msg) {this->value=value;}
  virtual int AsInt() {return value;}
  virtual float AsFloat() {return (float)value;}
  virtual char AsChar() {return (char)value;}
  virtual double AsDouble() {return (double)value;}
  virtual void *AsPtr() {return &value;}  
};

class CGMsgFloat : public CGMessage  
{
  float value;
public:
  CGMsgFloat(float value, int msg=1): CGMessage(msg) {this->value=value;}
  virtual const char *AsString(char *buffer, int bufsize);
  virtual int AsInt() {return (int)value;}
  virtual float AsFloat() {return (float)value;}
  virtual char AsChar() {return (char)value;}
  virtual double AsDouble() {return (double)value;}
  virtual void *AsPtr() {return &value;}  
};

class CGMsgString : public CGMessage  
  {
	char *text;
  public:
	virtual ~CGMsgString();
	virtual double AsDouble();
	virtual float AsFloat();
	virtual int AsInt();
	virtual const char *AsString(char *buffer, int bufsize);
    virtual void *AsPtr() {return (void *)text;}  
    virtual char AsChar() {return (char)AsInt();}
	CGMsgString(char *textmsg, int msg=1);	
  };



#endif // !defined(AFX_MESSAGE_H__56B6F1E2_FF46_11D3_90D5_00C0DFAE7D0A__INCLUDED_)
