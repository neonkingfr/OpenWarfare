// GAnim.h: interface for the CGAnim class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_GANIM_H__FF97B5A2_42EA_11D4_90D5_00C0DFAE7D0A__INCLUDED_)
#define AFX_GANIM_H__FF97B5A2_42EA_11D4_90D5_00C0DFAE7D0A__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include <iostream>
using namespace std;
#include "G3DObject.h"

struct SGAnimState
  {
  int framenum;
  int indexnum;
  };

#define CGAN_NEXTFRAME 0x1
#define CGAN_PREVFRAME 0x2
#define CGAN_LASTFRAME 0x4
#define CGAN_FIRSTFRAME 0x8
#define CGAN_LOOP 0x10

class CGAnim : public CG3DObject  
{
  protected:
	struct TFrame
	  {
	  int frame;
	  char data[4];
	  };
	int datastride;
	int allocated;
	int used;
	int grow;
	int count;
	void *flist;
	bool Expand(int needtoalloc);
    bool FindFrame(int frame, TFrame **frm);
	void (*destroy)(void *ptr);
	static int compare(const void *elem1, const void *elem2);
  public:
	  bool GetFrame(SGAnimState& st, void **data1, void **data2, int *ffrm1, int *ffrm2);
	  bool NextFrame(int flags, SGAnimState& st);
	  bool GotoFrame(int frame, SGAnimState& st);
	  void SetDestructor(void (*d)(void *)) {destroy=d;}
	  bool FindFrame(int frame, void **data1, void **data2, int *ffrm1, int *ffrm2);
	  void FreeExtra();
	  void Prepare();
	  void *GetData(int index, int *fnum=NULL);
	  bool Add(int frame, void *data);
	  CGAnim(int stride=4, int initial=0, int grow=256);
	  virtual ~CGAnim();
	  friend ostream& operator<< (ostream& out, const CGAnim& data);
	  friend istream& operator>> (istream& in, CGAnim& data);
};

#endif // !defined(AFX_GANIM_H__FF97B5A2_42EA_11D4_90D5_00C0DFAE7D0A__INCLUDED_)
