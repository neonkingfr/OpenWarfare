// Messager.cpp: implementation of the CMsgLink class.
//
//////////////////////////////////////////////////////////////////////

#include "Messager.h"
#include "..\hash.h"

#define DEFTABLESIZE 101

struct SGClassItem
  {
  CGMsgLink *client;
  CGMsgLink *server;
  int msgid;
  };

class CGClassDb  
{
	THASHTABLE *tbl;
public:		
	CGClassDb(THASHTABLE *tb);
	virtual ~CGClassDb();
};


CGClassDb::CGClassDb(THASHTABLE *tb)
  {
  tbl=tb;
  Hash_InitEx(tbl,DEFTABLESIZE,SGClassItem,client,HASHF_SPEEDADD);
  }

CGClassDb::~CGClassDb()
  {
  Hash_Done(tbl);
  }

static THASHTABLE msgtable;
static CGClassDb msgtablecontroler(&msgtable);

static void RemoveClientFromTable(CGMsgLink *clss)
  {
  SGClassItem *itm;

  while ((itm=(SGClassItem *)Hash_FindKey(&msgtable,&clss))==NULL)
	{
	itm->server->ClientDetach(clss);
	Hash_DeleteExact(&msgtable,itm,0);
	}
  }


static void AddServerToTable(CGMsgLink *client, CGMsgLink *server, int listen)
  {
  SGClassItem itm;
  itm.client=client;
  itm.server=server;
  itm.msgid=listen;
  Hash_Add(&msgtable,&itm);
  }

static void RemoveServerFromTable(CGMsgLink *server)
  {
  SGClassItem *itm,*itl=NULL;

  while ((itm=(SGClassItem *)Hash_EnumData(&msgtable,itl))==NULL)
	if (itm->server==server)
	  Hash_DeleteExact(&msgtable,itm,0);
	else
	  itl=itm;
  }

static void BroadCastMessage(CGMsgLink *sender, CGMessage *msg)
  {
  SGClassItem *itm;

  itm=(SGClassItem *)Hash_FindKey(&msgtable,&sender);
  while (itm!=NULL)
	{
	if (itm->msgid==msg->GetMsgID() || itm->msgid==CGMSG_ALLMSG) itm->server->Income(sender,msg);
	itm=(SGClassItem *)Hash_FindNext(&msgtable,itm);
	}
  }


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CGMsgLink::CGMsgLink()
  {
  attached=false;
  }

CGMsgLink::~CGMsgLink()
  {
  if (attached) RemoveClientFromTable(this);
  }


void CGMsgLink::SendMsg(CGMessage *msg)
  {
  BroadCastMessage(this,msg);
  }


void CGMsgLink::AttachToClient(CGMsgLink * client,int listen)
  {
  attached=true;
  AddServerToTable(client, this,listen);
  }

