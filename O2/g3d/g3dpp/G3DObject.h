// G3DObject.h: interface for the CG3DObject class.
//
//////////////////////////////////////////////////////////////////////


#if !defined(AFX_G3DOBJECT_H__097E6B85_FCB0_11D3_90D5_00C0DFAE7D0A__INCLUDED_)
#define AFX_G3DOBJECT_H__097E6B85_FCB0_11D3_90D5_00C0DFAE7D0A__INCLUDED_

#include <stdio.h>
#include "..\g3dlib.h"

#ifndef ASSERT
  #ifdef _DEBUG
	#define ASSERT(x) assert(x)
  #else
	#define ASSERT(x)
  #endif
#endif

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#define G3DCH_ERROR 0xFFFFFFFF


class CG3DObject  
  {
	unsigned long dwRefCount;
    virtual void AddReference() {dwRefCount++;}
  public:	
	void Release() {if ((--dwRefCount)==0) delete this;}
	virtual bool Save(FILE *file) {return false;}
	virtual bool Load(FILE *file) {return false;}
	virtual void Attach(CG3DObject *what) {what->AddReference();}
	virtual void Detach(CG3DObject *what) {what->Release();}
	CG3DObject *Clone() {AddReference();return this;}
	CG3DObject(const CG3DObject& from)
	  {dwRefCount=1;}	
	CG3DObject()
	  {dwRefCount=1;}
	virtual ~CG3DObject()
	  //Nelze uvolni objekt, protoze je attachnuty na jiny objekt
	  //Pouzij metodu Release() nebo Detach();
	  {ASSERT(dwRefCount<2);}

  };


class CG3DDispObject : public CG3DObject  
  {
  public:
	virtual int LoadToScene(HMATRIX mm,HTXITABLE txi=NULL) {return 0;}
	virtual int Animate(int howmany) {return 0;}
  };

#endif // !defined(AFX_G3DOBJECT_H__097E6B85_FCB0_11D3_90D5_00C0DFAE7D0A__INCLUDED_)
