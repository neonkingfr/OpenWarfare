// GWin.cpp: implementation of the CGWin class.
//
//////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <windows.h>
#include "GWin.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CGWin *desktop=NULL;

bool g3dgSetDesktop(CGWin *dt)
  {
  if (dt==NULL) return false;
  desktop=dt;
  return true;
  }

bool g3dgKillDesktop()
  {
  delete desktop;
  return true;
  }

CGWin::CGWin(int winid, HG3DCONTEXT ctx, unsigned long flags, CGWin *parent)
  {
  int centerx=0,centery=0;
  winctx=*ctx;
  CGWin **wptr;
  next=NULL;
  dependedwin=NULL;
  childwin=NULL;
  focus=NULL;
  this->flags=flags | CGWF_ENABLED;
  this->winid=winid;
  if (parent==NULL) parent=desktop;  
  this->parent=parent;
  forceredraw=0;
  if (parent!=NULL)
	{
	if (flags & CGWF_CHILD) wptr=&(parent->childwin);
	else wptr=&(parent->dependedwin);
	wptr=GetTopWindowReference(wptr,flags);
	if (flags & CGWF_CENTERPARENT) 
	  {
	  centerx=parent->winctx.wl+parent->winctx.wr>>1;
	  centery=parent->winctx.wt+parent->winctx.wb>>1;
	  }
	next=*wptr;
	*wptr=this;
	if (!(flags & CGWF_ABSOLUTEPOSX))
	  {
	  winctx.wl+=parent->winctx.wl;
	  winctx.wr+=parent->winctx.wl;
	  }
	if (!(flags & CGWF_ABSOLUTEPOSY))
	  {
	  winctx.wt+=parent->winctx.wt;
	  winctx.wb+=parent->winctx.wt;
	  }
	parent->RecalcZ();
	if (flags & CGWF_CHILD) 
	  parent->Invalidate();
	else
	  desktop->Invalidate();
	}
  bordersize=0;
  if (flags & CGWF_RAISED) {bordersize=2;borderface=0;}
  if (flags & CGWF_SUNKED) {bordersize=2;borderface=3;}
  if (flags & CGWF_RAISEDFRAME) {bordersize=2;borderface=2;}
  if (flags & CGWF_SUNKEDFRAME) {bordersize=2;borderface=1;}
  if (flags & CGWF_CENTERSCREEN) 
	{
	centerx=desktop->winctx.wl+desktop->winctx.wr>>1;
	centery=desktop->winctx.wt+desktop->winctx.wb>>1;
	}
  TMSSTATE mst;
  g3dGetMouseStatus(&mst);
  if (flags & CGWF_CENTERMOUSE)
	{
	centerx=mst.msx;
	centery=mst.msy;
	}
  if (flags & (CGWF_CENTERSCREEN | CGWF_CENTERMOUSE | CGWF_CENTERPARENT))
	{	
	int nwsizx=(winctx.wr-winctx.wr);
	int nwsizy=(winctx.wb-winctx.wt);
	int nwposx=centerx-nwsizx>>1;
	int nwposy=centery-nwsizy>>1;	
	if (desktop)
	  {
	  if (nwposx<desktop->winctx.wl) nwposx=desktop->winctx.wl;
	  if (nwposx+nwsizx>desktop->winctx.wr) nwposx=desktop->winctx.wr-nwsizx;
	  if (nwposy<desktop->winctx.wt) nwposy=desktop->winctx.wt;
	  if (nwposy+nwsizx>desktop->winctx.wb) nwposy=desktop->winctx.wb-nwsizy;
	  }
	}
  mouseid=mst.cursor;
  }

CGWin::CGWin(int winid, int xpos, int ypos, int xsize, int ysize, unsigned long flags, CGWin *parent)
  {
  HG3DCONTEXT cnt;

  cnt=g3dCreateWindow2(xpos, ypos, xsize, ysize); 
  CGWin::CGWin(winid,cnt,flags,parent);
  }

CGWin::~CGWin()
  {
  CGWin *f,**top;
  while (childwin)
	{
	f=childwin;
	childwin=f->next;
	delete f;
	}
  while (dependedwin)
	{
	f=dependedwin;
	dependedwin=f->next;
	delete f;
	}
  if (parent!=NULL)
	{
	if (flags & CGWF_CHILD) top=&(parent->childwin);
	else top=&(parent->dependedwin);
	while (*top!=this || *top!=NULL) top=&((*top)->next);
	if (*top==this) *top=next;
	GetWindow(DrawParent)->Invalidate();
	}
  }

void CGWin::InvalidateDown()
  {
  CGWin *p;

  for(p=childwin;p!=NULL;p=p->next)
	if (p->forceredraw!=GFD_FULLREDRAW) 
	  {p->forceredraw=GFD_FULLREDRAW;
	   p->InvalidateDown();
	  }
  if (this==desktop)
  for(p=dependedwin;p!=NULL;p=p->next)
	if (p->forceredraw!=GFD_FULLREDRAW) 
	  {p->forceredraw=GFD_FULLREDRAW;
	   p->InvalidateDown();
	  }
  }

void CGWin::Invalidate()
  {
  InvalidateDown();
  CGWin *p;
  forceredraw=GFD_FULLREDRAW;
  p=GetWindow(DrawParent);
  while (p!=NULL) 
	{
	if (p->forceredraw!=GFD_NODRAW) break;
	p->forceredraw=GFD_SOMECHILDS;
	p=p->parent;
	}  
  }

void CGWin::RecalcZ()
  {
  float zdep;
  CGWin *wnd;
  if (dependedwin==NULL) zdep=zmin;
  else
	{
	int wcount;
	float step;
	float pos;
	zdep=(zorder+zmin)/2.0f;
	for (wnd=dependedwin,wcount=1;wnd!=NULL;wnd=wnd->next,wcount++);
	step=(zdep-zmin)/wcount;
	pos=zmin;
	for (wnd=dependedwin;wnd!=NULL;wnd=wnd->next)
	  {
	  HG3DCONTEXT old;
	  wnd->zmin=pos;
	  pos+=step;
	  wnd->zorder=pos;
	  old=g3dSetActive(&(wnd->winctx));
	  g3dSetContextZ(pos);
	  g3dSetActive(old);
	  }	
	}
  if (childwin!=NULL)
	{
	int wcount;
	float step;
	float pos;
	for (wnd=childwin,wcount=1;wnd!=NULL;wnd=wnd->next,wcount++);
	step=(zorder-zdep)/wcount;
	pos=zdep;
	for (wnd=childwin;wnd!=NULL;wnd=wnd->next)
	  {
	  HG3DCONTEXT old;
	  wnd->zmin=pos;
	  pos+=step;
	  wnd->zorder=pos;
	  old=g3dSetActive(&(wnd->winctx));
	  g3dSetContextZ(pos);
	  g3dSetActive(old);
	  }	
	}
  }

void CGWin::Update()
  {
  if (forceredraw==GFD_NODRAW) return;
  CGWin *wnd;
  bool drawdepended;
  if (forceredraw==GFD_FULLREDRAW) 
	PrepareDraw();
  if (forceredraw!=GFD_NODRAW)
	{
	for (wnd=childwin;wnd!=NULL;wnd=wnd->next)
	  if (forceredraw==GFD_FULLREDRAW || wnd->forceredraw!=GFD_NODRAW)
		{wnd->Update();}
	forceredraw==GFD_NODRAW;
	}
  drawdepended=desktop && desktop->forceredraw==GFD_FULLREDRAW;
  for (wnd=dependedwin;wnd!=NULL;wnd=wnd->next)
	  if (drawdepended || wnd->forceredraw!=GFD_NODRAW)
		wnd->Update();	
  forceredraw=0;
  }

CGWin **CGWin::GetTopWindowReference(CGWin **ref,unsigned long flags)
  {
  if (flags & CGWF_TOPMOST) return ref;
  while (ref[0]!=NULL && ref[0]->flags & CGWF_TOPMOST) ref=&((*ref)->next);
  return ref;
  }

void CGWin::PrepareDraw()
  {
  HG3DCONTEXT old=g3dGetActive();
  if (!(flags & CGWF_VISIBLE)) return;
  if (desktop!=this)
	{
	if (flags & CGWF_CHILD)
	  {
	  if (parent==NULL) g3dDebugPoint(Child_object_hasnt_parent_Error);
	  g3dSetActive(&parent->winctx);
	  }
	else 
	  {
	  if (desktop==NULL) g3dDebugPoint(No_desktop_Error);
	  g3dSetActive(&desktop->winctx);
	  }
	g3dClipToWindow();
    g3dSetActive(&winctx);
    OnDrawBorder();
	}
  g3dSetActive(&winctx);
  OnDraw();
  g3dSetActive(old);
  }

void CGWin::OnDrawBorder()
  {
  if (bordersize==0) return;
  int wl,wt,wb,wr;
  unsigned long curcolor=g3dGetColor();
  float r,g,b,a;
  unsigned long col,col1,col2;
  wl=winctx.wl-1-g3dGetLeft();
  wt=winctx.wt-1-g3dGetTop();
  wb=winctx.wb+1-g3dGetTop();
  wr=winctx.wr+1-g3dGetLeft();
  if (flags & CGWF_BORDERPARENTBGR && parent!=NULL)
	col=parent->winctx.bgcolor;
  else if (flags & CGWF_BORDERPAINTCOLOR)
	col=winctx.color;
  else if (flags & CGWF_BORDER50ALPHA)
	{
	a=G3D_GETCOLOR_A(winctx.color)*0.5f;
	col1=G3D_RGBA(0.0f,0.0f,0.0f,a);
	col2=G3D_RGBA(1.0f,1.0f,1.0f,a);
	}
  else if (flags & CGWF_FLAT) 
	col1=col2=col=winctx.color;
  else 
	col=winctx.bgcolor;
  if ((flags & (CGWF_BORDER50ALPHA | CGWF_FLAT))==0)
	{
	r=G3D_GETCOLOR_R(col);
	g=G3D_GETCOLOR_G(col);
	b=G3D_GETCOLOR_B(col);
	a=G3D_GETCOLOR_A(col);
	col1=G3D_RGBA(r*0.5f,g*0.5f,b*0.5f,a);
	col2=G3D_RGBA((r+1.0f)*0.5f,(g+1.0f)*0.5f,(b+1.0f)*0.5f,a);
	}
  for(int i=0;i<bordersize;i++)
	{
	if (borderface & (1<<i)) g3dSetColor(col1);else g3dSetColor(col2);
	g3dLine(wl,wt,wr,wt);
	g3dLine(wl,wt,wl,wb);
	if (borderface & (1<<i)) g3dSetColor(col2);else g3dSetColor(col1);
	g3dLine(wr,wt,wr,wb);
	g3dLine(wl,wb,wr,wb);
	wl--;
	wt--;
	wr++;
	wb++;
	}
  }

void CGWin::OnDraw()
  {
  g3dClearContext();
  }


void CGWin::SetForeground()
  {
  CGWin **trc,**trp;
  if (parent==NULL) 
	g3dDebugPoint(No_parent_window_Error);
  if (flags & CGWF_CHILD) trc=&(parent->childwin);else trc=&(parent->dependedwin);
  trp=trc;
  for (;*trp!=NULL && *trp!=this;trp=&(trp[0]->next));
  *trp=next;
  trp=GetTopWindowReference(trc,flags);
  next=*trp;
  *trp=this;
  parent->RecalcZ();
  if (!(flags & CGWF_CHILD)) parent->Invalidate();
  }

bool CGWin::OnNotify(CGWin * child, CGMessage *msg)
  {
  return true;
  }

bool CGWin::SetFocus()
  {
  if (this==desktop) return true;
  if (flags & CGWF_CHILD)
	{
	if (parent==NULL) return false;
	return parent->SetFocus(this);
	}
  else
	{
	if (desktop==NULL) return false;
	return desktop->SetFocus(this);
	}
  }

bool CGWin::SetFocus(CGWin * to)
  {
  if (to==this) SetFocus();
  CGWin *last=focus;
  if (focus!=NULL) 
	if (focus->OnKillFocus()==false) return false;
  focus==NULL;
  if (to!=NULL)
	if (to->OnSetFocus()==false) 
	  {SetFocus(last);return false;}
	else to->SetForeground();
  focus=to;  
  return true;
  }

void CGWin::SetVisible(bool visible)
  {
  if (visible) flags|=CGWF_VISIBLE;else flags&=~CGWF_VISIBLE;
  if (flags & CGWF_CHILD || visible==true) 
	{
	if (parent==NULL) g3dDebugPoint(No_parent_window_Error);
	parent->Invalidate();
	}
  else 
	{
	if (desktop==NULL) g3dDebugPoint(No_desktop_Error);
	desktop->Invalidate();
	}
  }

void CGWin::Enable(bool enable)
  {
  if (enable) flags|=CGWF_ENABLED;else flags&=~CGWF_ENABLED;
  Invalidate();
  }


void CGWin::OnMove(int rx, int ry)
  {
  CGWin *wnd;
  winctx.wl+=rx;
  winctx.wr+=rx;
  winctx.wt+=ry;
  winctx.wb+=ry;
  for (wnd=childwin;wnd!=NULL;wnd=wnd->next) wnd->OnMove(rx,ry);  
  GetWindow(DrawParent)->Invalidate();
  }

void CGWin::OnResize(int sx, int sy)
  {
  CGWin *wnd;
  winctx.wr+=sx;
  winctx.wb+=sy;
  for (wnd=childwin;wnd!=NULL;wnd=wnd->next) 
	{
	if (wnd->flags & (CGWF_AUTORESIZEX | CGWF_AUTORESIZEY))
	  wnd->OnResize(
	  wnd->flags & CGWF_AUTORESIZEX?sx:0,
	  wnd->flags & CGWF_AUTORESIZEY?sy:0
				);
	if (wnd->flags & (CGWF_ALIGNRIGHT | CGWF_ALIGNBOTTOM))
	  wnd->OnMove(
	  wnd->flags & CGWF_ALIGNRIGHT?sx:0,
	  wnd->flags & CGWF_ALIGNBOTTOM?sy:0
				);
	}
  GetWindow(DrawParent)->Invalidate();
  }



void CGWin::OnKeyDown(int vKey, long flags)
  {
  if (focus!=NULL) focus->OnKeyDown(vKey,flags);
  }

void CGWin::OnKeyUp(int vKey)
  {
  if (focus!=NULL) focus->OnKeyDown(vKey,flags);
  }

void CGWin::OnChar(int nChar, int nRepCnt, int nFlags)
  {
  if (focus!=NULL) focus->OnKeyDown(nChar,flags);
  }

CGWin * CGWin::GetWindow(CGWDEF def)
  {
  CGWin *tr,*f;  
  if (parent==NULL) return NULL;
  if (flags & CGWF_CHILD) tr=parent->childwin;else tr=parent->dependedwin;  
  if (tr==NULL) return NULL;
  switch(def)
	{
	case NextWin: return next;
	case PrevWin: if (tr==this) return NULL;
				  for (;tr!=NULL && tr->next!=this;tr=tr->next);
				  return tr;
	case NextIdWin:f=NULL;
				   for(;tr!=NULL;tr=tr->next)
					 if (tr->winid>winid && (f==NULL || tr->winid<f->winid))
					   f=tr;
				   return f;
	case PrevIdWin:f=NULL;
				   for(;tr!=NULL;tr=tr->next)
					 if (tr->winid<winid && (f==NULL || tr->winid<f->winid))
					   f=tr;
				   return f;
	case ParentWin:return parent;
	case ChildWin: return childwin;
	case DependedWin: return dependedwin;
	case DesktopWin: return desktop;
	case DrawParent:if (flags & CGWF_CHILD) return parent;else return this==desktop?NULL:desktop;
	case FirstWin: return tr;
	case LastWin: if (tr!=NULL) for (;tr->next!=NULL;tr=tr->next);
				  return tr;
	case FirstIdWin:f=tr;
				  for(;tr!=NULL;tr=tr->next)
					if (tr->winid<f->winid) {f=tr;}
				  return tr;
	case LastIdWin:f=tr;
				  for(;tr!=NULL;tr=tr->next)
					if (tr->winid>f->winid) {f=tr;}
				  return tr;

	}
  return NULL;
  }

CGWin *CGWin::FindWindow(int winid)
  {
  CGWin *res=childwin;
  while(res)
	if (res->winid==winid) return res;
	else res=res->next;
  res=dependedwin;
  while(res)
	if (res->winid==winid) return res;
	else res=res->next;
  return NULL;
  }

CGWin *CGWin::MouseHitTest(int x, int y)
  {
  CGWin *win,*tr;
  bool setthis=false;
  if (flags & CGWF_MOUSECAPTURE) {return this;}
  if (focus) win=focus->MouseHitTest(x,y);else win=NULL;
  if (win) return win;
  if (((flags & (CGWF_ENABLED | CGWF_VISIBLE))==(CGWF_ENABLED | CGWF_VISIBLE)) && winctx.wl<=x && winctx.wr>=x && winctx.wt<=y && winctx.wb>=y)
	{	
	setthis=true;
	for (tr=childwin;tr!=NULL;tr=tr->next)
	  {
	  win=tr->MouseHitTest(x,y);
	  if (win!=NULL) 
		{
		return win;
		}
	  }	
	}
  for (tr=dependedwin;tr!=NULL;tr=tr->next)
	{
	win=tr->MouseHitTest(x,y);
	if (win!=NULL)  return win;	  
	}
  if (setthis)
	{
	return this;
	}
  return NULL;
  }

void CGWin::OnMouseEvent(TMSSTATE * msev)
  {
  if (flags & CGWF_CHILD) 
	{if (parent!=NULL) parent->OnMouseEvent(msev);}
  else
	{if (desktop!=NULL && desktop!=this) desktop->OnMouseEvent(msev);}

  }

void CGWin::SetCapture(bool set)
  {
  if (set) flags|=CGWF_MOUSECAPTURE;
  else flags&=~CGWF_MOUSECAPTURE;
  }

bool CGWin::Activate()
  {
  CGWin *tt=GetWindow(DrawParent);
  if (tt!=NULL) if (tt->Activate()==false) return false;
  return SetFocus();
  }

bool CGWin::HasFocus()
  {
  CGWin *tt=GetWindow(DrawParent);
  if (tt!=NULL) return tt->focus==this;
  return false;
  }

void CGWin::MouseActivate(TMSSTATE * mst)
  {
  if (mst->event & G3DMS_TL1D && HasFocus()==false)	
	if (Activate()==false) return;
  OnMouseEvent(mst);
  }


//--------------------- MISC ----------------------------

bool g3dguiOnKeyEvent(UINT msg, WPARAM wParam, LPARAM lParam)
  {
  if (desktop==NULL) return false;
  switch (msg)
	{
	case WM_KEYDOWN: desktop->OnKeyDown(wParam,lParam);break;
	case WM_KEYUP: desktop->OnKeyUp(wParam);break;
	case WM_CHAR: desktop->OnChar(wParam,lParam & 0xFFFF, lParam>>16);break;
	}
  return true;
  }

bool g3dguiOnMouseEvent()
  {
  TMSSTATE mss;
  CGWin *wnd;

  if (desktop==NULL) return false;
  g3dGetMouseStatus(&mss);
  wnd=desktop->MouseHitTest(mss.msx,mss.msy);
  if (wnd) {wnd->MouseActivate(&mss);return true;}
  return false;
  }

bool g3dguiRedrawDesktop(int buffer)
  {
  if (desktop==NULL) return false;
  if (desktop->NeedRepaint()==false) return false;
  g3dBeginPaint(buffer);
  if (buffer==G3D_FRONTBUFFER) g3dMouseHide();
  desktop->Update();
  if (buffer==G3D_FRONTBUFFER) 
	{
	g3dMouseShow();
	g3dMouseDisplay();
	}
  g3dEndPaint();
  g3dClipToScreen();
  g3dFinish();
  return true;
  }


