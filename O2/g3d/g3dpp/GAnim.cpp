// GAnim.cpp: implementation of the CGAnim class.
//
//////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "GAnim.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CGAnim::CGAnim(int stride, int initial, int grow):
  allocated(initial),grow(grow),used(0),datastride(stride)
  {
  flist=malloc(allocated);
  if (flist==NULL) allocated=0;
  count=0;
  destroy=NULL;
  }

CGAnim::~CGAnim()
  {
  if (destroy)	
	for (int i=0;i<count;i++) destroy(GetData(i));
  free(flist);
  }

bool CGAnim::Add(int frame, void *data)
  {
  int cnt=sizeof(TFrame)+datastride;
  while (used+cnt>allocated) 
	if (Expand(cnt)==false) return false;
  char *c=(char *)flist;
  c+=used;
  TFrame *p=(TFrame *)c;
  p->frame=frame;
  memcpy(p->data,data,datastride);
  used+=cnt;
  count++;
  return true;
  }

bool CGAnim::Expand(int needtoalloc)
  {
  int exp=__max(needtoalloc,grow);
  void *p=realloc(flist,allocated+exp);
  if (p==NULL) return false;
  allocated+=exp;
  flist=p;
  return true;
  }

void *CGAnim::GetData(int index, int *fnum)
  {
  char *c=(char *)flist;
  int i=index*(datastride+sizeof(TFrame));
  if (i>=used) return NULL;
  if (i<0) return NULL;
  TFrame *f=(TFrame *)(c+i);
  if (fnum) *fnum=f->frame;
  return (void *)f->data;
  }

int CGAnim::compare (const void *elem1, const void *elem2)
  {
  const TFrame *frm1=(const TFrame *)elem1;
  const TFrame *frm2=(const TFrame *)elem2;
  return (frm1->frame>frm2->frame)-(frm1->frame<frm2->frame);
  }

void CGAnim::Prepare()
  {
  int stride=(datastride+sizeof(TFrame));
  qsort(flist,count,stride,compare);  
  }

void CGAnim::FreeExtra()
  {
  void *p=realloc(flist,used);
  if (p==NULL) return;
  flist=p;
  allocated=used;
  }


bool CGAnim::FindFrame(int frame, void **data1, void **data2, int *ffrm1, int *ffrm2)
  {
  TFrame *f1,*f2;
  if (FindFrame(frame,&f1)==false) return false;
  if (frame<f1->frame)
	{
	*data1=(void *)f1->data;
  	*data2=(void *)f1->data;
	if (ffrm1) *ffrm1=f1->frame;
	if (ffrm2) *ffrm2=f1->frame;
	return true;
	}
  f2=(TFrame *)((char *)f1+sizeof(TFrame *)+datastride);
  if ((char *)f2-(char *)f1>used)
	{
	*data1=(void *)f1->data;
  	*data2=(void *)f1->data;
	if (ffrm1) *ffrm1=f1->frame;
	if (ffrm2) *ffrm2=f1->frame;
	return true;
	}
  *data1=(void *)f1->data;
  *data2=(void *)f2->data;
  if (ffrm1) *ffrm1=f1->frame;
  if (ffrm2) *ffrm2=f2->frame;
  return true;
  }

bool CGAnim::FindFrame(int frame, TFrame **frm)
  {
  if (count==0) return false;
  int l,r,lv,rv;
  int idx,iv;

  l=0;
  r=count-1;
  GetData(l,&lv);
  GetData(r,&rv);
  while (l<r)
	{	
	idx=(r-l)*(frame-lv)/(rv-lv)+l;
	if (idx<l) idx=l;
	if (idx>r) idx=r;
	GetData(idx,&iv);
	if (iv==frame) {l=idx;lv=iv;break;}
	if (iv<frame)
	  {
	  l=idx+1;
	  GetData(l,&lv);
	  }
	if (iv>frame)
	  {
	  r=idx-1;
	  GetData(r,&rv);
	  }
	}
  if (lv>frame && l>0 || l>=count) l--;
  *frm=(TFrame *)((char *)flist+l*(sizeof(TFrame)+datastride));
  return true;
  }

bool CGAnim::GotoFrame(int frame, SGAnimState & st)
  {
  TFrame *f;
  if (FindFrame(frame, &f)==false) return false;
  st.framenum=frame;
  st.indexnum=((char *)f-(char *)flist)/(sizeof(TFrame)+datastride);
  return true;
  }

bool CGAnim::NextFrame(int flags, SGAnimState & st)
  {
  int frm;
  if (flags & CGAN_FIRSTFRAME)
	{
	GetData(0,&st.framenum);
	st.indexnum=0;	
	}
  else if (flags & CGAN_LASTFRAME)
	{
	GetData(count-1,&st.framenum);
	st.indexnum=count-1;
	}
  else if (flags & CGAN_NEXTFRAME)
	{
	if (st.indexnum+1>count)
	  {
	  if (flags & CGAN_LOOP) return NextFrame(CGAN_FIRSTFRAME,st);
	  return true;
	  }
	GetData(st.indexnum+1,&frm);
	st.framenum++;
	if (st.framenum>=frm) st.indexnum++;
	}
  else
	{
	if (st.indexnum-1<0)
	  {
	  if (flags & CGAN_LOOP) return NextFrame(CGAN_LASTFRAME,st);
	  return true;
	  }
	GetData(st.indexnum-1,&frm);
	st.framenum--;
	if (st.framenum<frm) st.indexnum++;
	}
  return true;
  }

bool CGAnim::GetFrame(SGAnimState& st, void **data1, void **data2, int *ffrm1, int *ffrm2)
  {
  *data1=GetData(st.indexnum,ffrm1);
  if (st.indexnum+1<count) *data2=GetData(st.indexnum+1,ffrm2);
  else *data2=GetData(st.indexnum,ffrm2);
  return true;
  }

ostream& operator<< (ostream& out, const CGAnim& data)
  {
  out.write((char *)&data.datastride,sizeof(data.datastride));
  out.write((char *)&data.used,sizeof(data.used));
  out.write((char *)data.flist,data.used);
  return out;
  }

istream& operator>> (istream& in, CGAnim& data)
  {
  in.read((char *)&data.datastride,sizeof(data.datastride));
  in.read((char *)&data.used,sizeof(data.used));
  if (data.allocated<data.used) 
	if (data.Expand(data.used-data.allocated)==false)
	  throw int(-1);
  in.read((char *)data.flist,data.used);
  return in;
  }
