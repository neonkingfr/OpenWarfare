// GFaceSort.h: interface for the CGFaceSort class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_GFACESORT_H__C06BC6E2_407F_11D4_90D5_00C0DFAE7D0A__INCLUDED_)
#define AFX_GFACESORT_H__C06BC6E2_407F_11D4_90D5_00C0DFAE7D0A__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include "G3DObject.h"

struct SGFaceInfo
   {
   void *streamptr; //ukazatel na face do streamu
   SGFaceInfo *inside; //ukazatel na pole podradky
   float minz,maxz; //minimalni a maximalni Z
   unsigned long lights; //ukazatel na pole barev k vertexum
   HTXITABLE txi;
   float a,b,c,d;   //souradnice roviny 
  };

class CGFaceSort : public CG3DObject  
{
protected:
   SGFaceInfo **facelist;
   int lines;
   int cols;
   int allocated;
   float min,max;
   SGFaceInfo *AddLine();
   SGFaceInfo *AddFace(SGFaceInfo *fc, float z, float min, float max);
   bool Scan(SGFaceInfo *line,SGFaceInfo *midl);
public:
   bool Scan() {if (!lines) return false;else return Scan(facelist[0],NULL);}
   SGFaceInfo *AddFace(float z);
   CGFaceSort(int c=32,float n=0.0f,float x=1.0f);
   virtual bool Enumerate(SGFaceInfo *f)=0;
   virtual ~CGFaceSort();
   void Reset() {lines=0;}

};

#endif // !defined(AFX_GFACESORT_H__C06BC6E2_407F_11D4_90D5_00C0DFAE7D0A__INCLUDED_)
