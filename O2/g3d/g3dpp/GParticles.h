// GParticles.h: interface for the CGParticles class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_GPARTICLES_H__129E6C02_420C_11D4_90D5_00C0DFAE7D0A__INCLUDED_)
#define AFX_GPARTICLES_H__129E6C02_420C_11D4_90D5_00C0DFAE7D0A__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include "G3DObject.h"

struct SGParticle
  {
  void *startcol;
  void *endcol;
  float startsize;
  float endsize;
  HVECTOR pos;
  HVECTOR dir;
  HVECTOR rot;
  HVECTOR rotofs;
  float phase;
  float phasestep;
  unsigned long flags;
  CGExecBuffer *scene;
  void SetLifetime(int frames) {phasestep=1.0f/frames;}
  void KillParticle() {flags=0;}
  }

#define SGPP_NOTUSED 0x0
 //particle entry neni pouzito
#define SGPP_BILLBOARDS 0x1 
 //do zasobniku vlozi vertex na pozici particle
 //scena musi referencovat tento vertex a z 
 //neho vyrobit face
#define SGPP_MODELS 0x2
 //do zasobniku vlozi transformacni matici particle
 //scena muze byt libovolna
 //je pomalejsi, ale pro castice tvorene 3D scenou jedina moznost

typedef void *(*CGCALCCOLORBETWEEN)(void *col1, void *col2, float factor);

void *CGCalcDefMaterialBetween(void *col1, void *col2, float factor);

class CGParticles : public CG3DObject  
{
  protected:
	int partcount;
	SGParticle *array;
	SGParticle vychozi;
	CGCALCCOLORBETWEEN ccol;
	CGParticles *nextsystem;
	int pindex;
  public:
	bool AddParticle(HMATRIX modf=NULL);
	void AttachSystem(CGParticles *next);
	float genradius;
	float minspeed;
	float maxspeed;
	HMATRIX genmatrix;
	HVECTOR rotspeedmin; //rychlost otaceni v kazdem smeru
	HVECTOR rotspeedmax; //rychlost otaceni v kazdem smeru
	CGParticles(void *arr, int size, SGParticle def, CGCALCOLORBETWEEN cc=CGCalcDefMaterialBetween)
	virtual ~CGParticles();

};

#endif // !defined(AFX_GPARTICLES_H__129E6C02_420C_11D4_90D5_00C0DFAE7D0A__INCLUDED_)
