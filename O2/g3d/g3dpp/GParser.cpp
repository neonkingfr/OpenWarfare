// GParser.cpp: implementation of the CGParser class.
//
//////////////////////////////////////////////////////////////////////


#include "GParser.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

static int RecurseLights(CGLightBuilder& lb,CGParser& ps, CGSceneEnv& env)
  {
  int index=0;
  int res;
  do
	{
	TLIGHTDEF *lg=lb.GetNextLight(index,env);
	if (lg==NULL)
	  {
	  CGExecBuffer *ex=lb.GetSubtable(index,env);
  	  if (ex==NULL)break; else 
		{
		CGLightBuilder lb2(*ex);
		if (res=RecurseLights(lb2,ps,env)) return res;
		}
	  }
	else
	  if (res=ps.Light(lg)) return res;
	}
  while(1);
  return 0;  
  }

int CGParser::Parse(CGSceneEnv& env)
  {
  int res=0;
  unsigned char *c=(unsigned char *)scene->GetAdr(0);
  unsigned char *end=(unsigned char *)scene->GetAdr(scene->GetBufSize());
  while (c<end)
	{
	int size;
	if (*c) res=DoCommand(*c,(c+1),env,size);else size=0;
	c+=size+1;
	if (res) return res;
	}
  return 0;
  }

int CGParser::DoCommand(unsigned char command, unsigned char * data, CGSceneEnv & env, int & size)
  {
  int res=0;
  static int recursecounter=0;
  switch (command)
	  {
	  case 0:size=0;return 0;
	  case EGC_SetMaterialClass:
		{
		SMatClass *mc=(SMatClass *)data;
		matcallback=mc->matclass;
		matsize=mc->matstructsize;
		size=sizeof(SMatClass);
		break;
		}
	  case EGC_AddMultPos:
		{
		long *mv=(long *)data;
		if (res=AddPosition((LPHVECTOR)(mv+1),*mv)) return res;
		size=sizeof(*mv)+*mv*3*sizeof(float);
		break;
		}
	  case EGC_AddMultNorm:
		{
		long *mv=(long *)data;
		if (res=AddNormal((LPHVECTOR)(mv+1),*mv)) return res;
		size=sizeof(*mv)+*mv*3*sizeof(float);
		break;
		}
	  case EGC_PushMatrixL:
	  case EGC_PushMatrixR:
		{		
		LPHMATRIX mm=(LPHMATRIX)data;
		if (res=PushMatrix(mm,command==EGC_PushMatrixL)) return res;
		size=sizeof(HMATRIX);
		break;
		}
	  case EGC_PopMatrices:
		{
		short *cnt=(short *)data;
		if (res=PopMatrices(*cnt))return res;
		size=sizeof(short);
		break;
		}
	  case EGC_AddMaterial:
		{		
		if (res=AddMaterial(env[(void *)data])) return res;
		size=matsize;
		break;
		}
	  case EGC_BillboardPos:
		{
		int *p=(int *)data;
		if (res=AddBillboardPos(p[0],p[1],(float *)(p+2))) return res;
		size=2*sizeof(*p)+(p[1]*2*sizeof(float));
		break;
		}
	  case EGC_BillboardPosScr:
		{
		int *p=(int *)data;
		if (res=AddBillboardScr(p[0],p[1],(float *)(p+2))) return res;
		size=2*sizeof(*p)+(p[1]*2*sizeof(float));
		break;
		}
	  case EGC_SetTPosition:
		{
		long *mv=(long*)data;
		if (res=SetTPosition((LPHVECTOR)(mv+1),*mv)) return res;
		size=sizeof(long)+*mv*8*sizeof(float);
		break;
		}
	  case EGC_SetTXITable:
		{
		HTXITABLE cc=(HTXITABLE)(env[*(void **)data]);
		if (res=SetTXITable(cc)) return res;
		size=sizeof(HTXITABLE);
		break;
		}
	  case EGC_Include:
		{
		CGExecBuffer *eb=(CGExecBuffer *)(env[*(void **)data]);
		CGExecBuffer *old=scene;		
		size=sizeof(eb);		
		scene=eb;
		recursecounter++;
		if (res=Parse(env)) return res;
		recursecounter--;
		scene=old;
		break;
		}
	  case EGC_LightTable:
		{
		CGLightBuilder lb(*scene);
		unsigned char *skip=(unsigned char *)lb.GetCommandAfter();
		if (skip==NULL) return -1;
		if (recursecounter==0) 
		  if (res=RecurseLights(lb,*this,env)) return res;
		size=skip-data;
		break;
		}
	  case EGC_AddFace:
		{
		FaceInfo *f=(FaceInfo *)data;
		unsigned char *c=(unsigned char *)(f+1);
		c+=sizeof(long)*(f->tcount-1);
		FaceVertex *fv=(FaceVertex *)c;
		if (res=Face(f,fv)) return res;
		c=(unsigned char *)fv;
		c+=(sizeof(*fv)+2*sizeof(float)*(f->tcount-1))*f->vcount;
		size=c-data;
		break;
		}
	  default: 		
		ASSERT("Unknown or unimplemented command"==NULL);
		break;
	  }
  return res;
  }
