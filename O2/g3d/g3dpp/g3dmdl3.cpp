// g3dmdl3.cpp: implementation of the CGSceneList class.
//
//////////////////////////////////////////////////////////////////////

#include "g3dmdl3.h"
#include "..\lzwc.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CGSceneList::CGSceneList(int count)
  {
  if (count) field=new CGScene* [count];else field=NULL;
  if (field==NULL) this->count=0;
  else 
	{
	this->count=count;
	memset(field,0,sizeof(CGScene *)*count);
	}
  current=0;
  }

CGSceneList::~CGSceneList()
  {
  int i;
  for (i=0;i<count;i++) if (field[i]) Detach(field[i]);
  delete [] field;
  }

int CGSceneList::LoadToScene(HMATRIX mm, HTXITABLE txi)
  {
  ASSERT(current<count);
  if (field[current]!=NULL) 
	return field[current]->LoadToScene(mm,txi);
  else return 0;
  } 

int CGSceneList::Move(int howmany)
  {
  current+=howmany;
  if (current<0) current+=count;
  if (current>=count) current-=count;
  return 0;
  }

void CGSceneList::SetScene(int index, CGScene * scene)
  {
  if (index<0 || index>=count) return;
  if (field[index]) Detach(field[index]);
  field[index]=(CGScene *)scene->Clone();
  }

bool CGSceneList::Expand(int count)
  {
  if (count<1) return false;
  count+=this->count;
  CGScene **p=new CGScene*[count];
  if (p==NULL) return false;
  memcpy(p,field,this->count*sizeof(CGScene *));
  memset(p+this->count,0,(count-this->count)*sizeof(CGScene *));
  delete [] field;
  field=p;
  this->count=count;
  return true;
  }

bool CGSceneList::Save(FILE * f)
  {
  int i;
  if (!fwrite(&count,2,1,f)) return false;
  for (i=0;i<count;i++) if (field[i])
	{
	if (!fwrite(&i,2,1,f)) return false;
	if (!field[i]->Save(f)) return false;
	}
  i=-1;
  if (!fwrite(&i,2,1,f)) return false;
  return true;
  }

bool CGSceneList::Load(FILE * f)
  {
  int i=0;
  if (!fread(&i,2,1,f)) return false;
  if (count>i) count=i;
  else Expand(i-count);
  if (!fread(&i,2,1,f)) return false;
  while (i!=0xffff)
	{
	CGScene *scn=new CGScene;
	if (!scn->Load(f)) {scn->Release();return false;}
	SetScene(i,scn);
    if (!fread(&i,2,1,f)) return false;
	}
  return true;
  }


//////////////////////////////////////////////////////////////////////
// CGAnimation Class
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CGAnimation::CGAnimation(int grow)
  {
  list=NULL;
  count=0;
  seqid=0;
  objectref=0;
  allocated=0;
  this->grow=grow;
  }

CGAnimation::~CGAnimation()
  {
  delete list;
  }

bool CGAnimation::AddFrame(SGFrame & frm)
  {
  if (allocated==count)	
	if (Expand(grow)==false) return false;
  list[count]=frm;
  count++;
  return true;
  }

bool CGAnimation::DeleteFrame(int number)
  {
  int index=FindFrame(number);
  if (index<0) return false;
  if (index+1<count)
	memcpy(list+index,list+index+1,(count-index-1)*sizeof(*list));
  count--;
  return true;
  }

int CGAnimation::FindFrame(int frame)
  {
  for (int i=0;i<count;i++) if (list[i].fnum==frame) return i;
  return -1;
  }

static void MatrixInterpolate(HMATRIX mm1, HMATRIX mm2, float factor, HMATRIX mmr)
  {
  for (int i=0;i<4;i++)
	for (int j=0;j<4;j++)
	  {
	  float a=mm1[i][j],b=mm2[i][j];
	  mmr[i][j]=a+(b-a)*factor;
	  }
  }

void CGAnimation::Interpolate(int index1, int index2, float factor, SGFrame & frame)
  {
  SGFrame *frm1=list+index1;
  SGFrame *frm2=list+index2;
  if (frm1->flags & G3DFF_MATRIX || frm2->flags & G3DFF_MATRIX)
	{
	HMATRIX mm;
	if (frm1->flags & G3DFF_KEY)
	  {
	  mxKeyToMatrix(frm1->key,mm);
	  MatrixInterpolate(mm,frm2->key,factor,frame.key);
	  }
	else if (frm2->flags & G3DFF_KEY)
	  {
	  mxKeyToMatrix(frm2->key,mm);
	  MatrixInterpolate(frm1->key,mm,factor,frame.key);
	  }
	else
	  MatrixInterpolate(frm1->key,frm2->key,factor,frame.key);
	frame.flags=G3DFF_MATRIX;
	}
  else	
	{
	mxInterpolateKey(frm1->key, frm2->key, factor,frame.key);
	frame.flags=G3DFF_KEY;
	}
  frame.scene=frm1->scene;  
  }

bool CGAnimation::GetFrame(int index, SGFrame & frame)
  {
  int left, right;
  int lindex, rindex;
  int diff;
  left=-0x7fffffff;
  right=0x7fffffff;
  lindex=-1;
  rindex=-1;
  for(int i=0;i<count;i++)
	{
	diff=index-list[i].fnum;
	if (diff==0) 
	  {
	  frame=list[i];
	  return true;
	  }
	else if (diff<0 && left<diff)
	  {
	  left=diff;
	  lindex=i;
	  }
	else if (diff>0 && right>diff)
	  {
	  right=diff;
	  rindex=i;
	  }
	}
  if (lindex==-1)
	{
	if (rindex==-1) return false;
	frame=list[rindex];
	}
  else if (rindex==-1) frame=list[lindex];
  else
	{
    float f;
	f=(float)left/(float)(right-left);
	Interpolate(lindex,rindex,f,frame);
	frame.fnum=index;
	}
  return true;
  }

int CGAnimation::LookForIndex(int frame)
  {
  int l=0,r=count-1;
  int fl=list[l].fnum;
  int fr=list[r].fnum;
  int a=frame-fl;  
  if (frame<=fl) return l;
  if (frame>=fr) return r;
  while (true)
	{
	int b=fr-fl;
	int i=(r-l)*a/b+l;
	if (list[i].fnum==frame) return i;
	if (list[i].fnum<frame)
	  {
	  l=i+1;
	  fl=list[l].fnum;
	  a=frame-fl;
	  }
	else //(list[i].fnum>frame)
	  {
	  r=i-1;
	  fr=list[r].fnum;
	  }
	if (frame<=fl) return l-1;
	if (frame>=fr) return r;
	}
  return __min(fr,fl);
  }

static int __cdecl compare(const void *e1, const void *e2)
  {
  int diff;
  SGFrame *a1=(SGFrame *)e1,*a2=(SGFrame *)e2;
  diff=a1->fnum-a2->fnum;
  return (diff>0)-(diff<0);
  }

void CGAnimation::Prepare()
  {
  qsort(list,count,sizeof(SGFrame),compare);
  }

SGFrame CGAnimation::operator [](int index)
  {
  int i=LookForIndex(index);
  if (i+1>=count)
	{
	return list[i];
	}
  else if (list[i].fnum>=index) return list[i];  
	{
	SGFrame frm;
	int diff=index-list[i].fnum;
	float f=(float)diff/(float)(list[i+1].fnum-list[i].fnum);
	Interpolate(i,i+1,f,frm);
	frm.fnum=index;
	return frm;
	}
  }

bool CGAnimation::Save(FILE * f)
  {
  if (!fwrite(&count,2,1,f)) return false;
  if (!fwrite(&objectref,2,1,f)) return false;
  if (!fwrite(&seqid,4,1,f)) return false;
  int sgsize=sizeof(SGFrame)*count;
  char *lzwblock=new char[sgsize+16];
  bool packed=true;
  long packsize;
  if (lzwblock==NULL) packed=false;
  if (packed)
	{
	init_lzw_compressor(8);
	memset(lzwblock,0,sgsize);
	packsize=lzw_encode((unsigned char *)list,lzwblock,sgsize);
	if (packsize==-1) 
	  {
	  packed=false;
	  delete [] lzwblock;
	  }
	}
  if (packed)
	{
	fwrite(&packsize,4,1,f);
	if (!fwrite(lzwblock,packsize,1,f))
	  {delete [] lzwblock;return false;}
	delete [] lzwblock;
	}
  else
	{
	packsize=-1;
	fwrite(&packsize,4,1,f);
	if (!fwrite(list,sgsize,1,f)) return false;
	}
  return true;
  }

bool CGAnimation::Load(FILE * f)
  {
  int cnt=0;
  
  if (!fread(&cnt,2,1,f)) return false;
  if (cnt>allocated) 
	if (Expand(cnt-allocated)==false) return false;
  count=cnt;
  objectref=0;
  if (!fread(&objectref,2,1,f)) return false;
  if (!fread(&seqid,4,1,f)) return false;
  int sgsize=sizeof(SGFrame)*count;
  long packsize;
  char *lzwblock;
  if (!fread(&packsize,4,1,f)) return false;
  if (packsize>0)
	{
	lzwblock=new char[packsize];
	if (lzwblock==NULL) return false;
	if (!fread(lzwblock,packsize,1,f)) 
	  {delete [] lzwblock;return false;}
	init_lzw_compressor(8);
	lzw_decode(lzwblock,(unsigned char *)list);
	}
  else
	if (!fread(list,sgsize,1,f)) return false;
  return true;
  }

bool CGAnimation::Expand(int grow)
  {
  if (grow+allocated<1) return false;
  SGFrame *nwfrm=new SGFrame[grow+allocated];
  if (nwfrm==NULL) return false;
  allocated+=grow;
  count=__min(count,allocated);
  if (count) memcpy(nwfrm,list,sizeof(SGFrame)*count);
  delete [] list;
  list=nwfrm;
  return true;
  }

int CGAnimation::GetLastFrame()
  {
  return list[count-1].fnum;
  }

//// Matrix frames ///////////////////////////////////

struct SGMatrixFrame
  {
  SGMatrixFrame *next;
  HMATRIX mm;
  };

class CMatrixFrameControl
  {
	SGMatrixFrame *mf_start;
	SGMatrixFrame **mf_nxt;
	SGMatrixFrame *mf_empty;
  public:
	CMatrixFrameControl()
	  {
	  mf_start=NULL;
	  mf_nxt=&mf_start;
	  mf_empty=NULL;
	  }
	LPHMATRIX AllocFrame()
	  {
	  SGMatrixFrame *frm;
	  if (mf_empty) 
		{
		frm=mf_empty;
		mf_empty=mf_empty->next;
		}
	  else
		{
		frm=new SGMatrixFrame();
		}
	  frm->next=NULL;
	  *mf_nxt=frm;
	  mf_nxt=&(frm->next);
	  return frm->mm;
	  }
	void FreeFrames()
	  {
	  while (mf_start)
		{
		SGMatrixFrame *nx=mf_start;
		mf_start=nx->next;
		nx->next=mf_empty;
		mf_empty=nx;
		}
	  mf_nxt=&mf_start;
	  }
	~CMatrixFrameControl()
	  {
	  FreeFrames();
	  while (mf_empty)
		{
		SGMatrixFrame *nx=mf_empty;
		mf_empty=nx->next;
		delete nx;
		}
	  }
  };


static CMatrixFrameControl mxframe;

/// ObjectInfo ///////////////////////////////////////

bool SGObjectInfo::SetName(const char *name)
{
if (name==NULL)
  {
  delete [] (void *)this->name;
  this->name=NULL;
  }
else
  {
  int s=strlen(name);
  if (s>63) return false;
  char *c=new char[s+1];
  if (c==NULL) return false;
  memcpy(c,name,s+1);
  delete [] (void *)this->name;
  this->name=c;
  }
return true;
}

LPHMATRIX SGObjectInfo::CalcMatrix(HMATRIX anim, SGObjectInfo *parents, HMATRIX def)
  {
  frame=mxframe.AllocFrame();
  if (parent>=0)
	{
	SGObjectInfo *prn=parents+parent;
	SoucinMatic(anim,prn->frame,frame);
	}
  else
	if (def==NULL) CopyMatice(frame,anim);
	else SoucinMatic(anim,def,frame);
  return frame;
  }

bool SGObjectInfo::Display(CGSceneList *sl,int altscene, HTXITABLE txi)
  {
  if (altscene==-1) altscene=sceneref;
  if (altscene==-1) return false;
  return ((*sl)[altscene])->LoadToScene(frame,txi)==G3D_OK;
  }

bool SGObjectInfo::Save(FILE * f)
  {
  if (!fwrite(&parent,2,1,f)) return false;
  if (!fwrite(&sceneref,2,1,f)) return false;
  if (name==NULL)
	{if (!fwrite(&name,1,1,f)) return false;}
  else
	{if (!fwrite(name,strlen(name)+1,1,f)) return false;}
  return true;
  }

bool SGObjectInfo::Load(FILE * f)
  {
  short p;
  char c[64],*d;
  int i;
  memset(this,0,sizeof(this));
  if (!fread(&p,2,1,f)) return false;
  parent=p;
  if (!fread(&p,2,1,f)) return false;
  sceneref=p;
  d=c;
  while ((i=getc(f))!=0 && i!=EOF)
	{
	if (d-c<63) *d++=i;
	}
  if (i==EOF) return false;
  c[63]=0;
  if (c[0]) SetName(c);
  return true;
  }


SG3DObjectList *SG3DObjectList::ExpandSelf()
{
int count=this==NULL?0:this->count;
SG3DObjectList *nw=(SG3DObjectList *)realloc(this,CalcReqSize(count+1));
if (nw==NULL) return this;
memset(nw->list+count,0,sizeof(SGObjectInfo));
nw->count=count+1;
return nw;
}

SG3DObjectList *SG3DObjectList::Alloc(int count)
  {
  int size=CalcReqSize(count);
  SG3DObjectList *nw=(SG3DObjectList *)malloc(size);
  if (nw)
	{
	memset(nw,0,size);
	nw->count=count;
	}
  return nw;
  }

SG3DObjectList *SG3DObjectList::Load(FILE *f)
  {
  short count;
  SG3DObjectList *nw;
  if (!fread(&count,2,1,f)) return NULL;
  nw=Alloc(count);
  if (nw==NULL) return nw;
  for (int i=0;i<nw->count;i++)
	nw->list[i].Load(f);
  return nw;
  }

bool SG3DObjectList::Save(FILE *f)
  {
  if (!fwrite(&count,2,1,f)) return false;
  for (int i=0;i<count;i++)
	if (!list[i].Save(f)) return false;
  return true;
  }

void SG3DObjectList::Release()
  {
  for (int i=0;i<count;i++) list[i].SetName(NULL);
  free(this);
  }

//////////////////////////////////////////////////////////////////////
// CGModelInfo Class
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CGModelInfo::CGModelInfo()
  {
  slist=NULL;
  olist=NULL;
  alist=NULL;
  name=0;
  seqcount=0;
  camera=NULL;
  camobject=-1;
  drawcamobject=true;
  }

CGModelInfo::~CGModelInfo()
  {
  CG3DObject::~CG3DObject();
  for (int i=0;i<seqcount;i++) delete alist[i];
  free(alist);
  if (slist) slist->Release();
  if (olist) olist->Release();
  free(name);
  }

void CGModelInfo::Attach(CGSceneList *sl)
  {
  CGSceneList *temp;
  if (sl==NULL) temp=sl;else temp=(CGSceneList *)sl->Clone();
  if (slist) slist->Release();
  slist=temp;
  }

int CGModelInfo::AddObject(int sceneref, int parent, const char *name)
  {
  int cnt=olist==NULL?0:olist->count;
  olist=olist->ExpandSelf();
  if (cnt==olist->count) return -1;
  cnt=olist->count-1;
  olist->list[cnt].sceneref=sceneref;
  olist->list[cnt].parent=parent;
  olist->list[cnt].SetName(name);
  return cnt;
  }


int *CGModelInfo::BuildSeqIndexs(int seqnum, int & lastframe)
  {
  int cnt=0,j;
  int *list;
  int i;
  
  lastframe=0;
  for(i=0;i<seqcount;i++) 
	if (alist[i]->seqid==seqnum) cnt++;
  list=new int[cnt+1];
  if (list==NULL) return NULL;
  j=0;
  for(i=0;i<seqcount;i++) 
	if (alist[i]->seqid==seqnum) 
	  {
	  int lfrm=alist[j]->GetLastFrame();
	  if (lfrm>lastframe) lastframe=lfrm;
	  list[j++]=i;
	  }
  list[j]=-1;
  return list;
  }


bool CGModelInfo::Attach(CGAnimation *al, int object, int seqid)
  {
  int i;
  CGAnimation **nl;
  CGAnimation *cl;
  if (object<0 || object>olist->count) return false;
  for(i=0;i<seqcount;i++)
	if (alist[i]->seqid==seqid && alist[i]->objectref==object) break;
  if (i==seqcount)
	{
	nl=(CGAnimation **)realloc(alist,sizeof(CGAnimation *)*(seqcount+1));
	if (nl==NULL) return false;
	alist=nl;
	seqcount++;
    cl=(CGAnimation *)al->Clone();
	}
  else
	{
    cl=(CGAnimation *)al->Clone();
	alist[i]->Release();
	}
  alist[i]=cl;
  cl->seqid=seqid;
  cl->objectref=object;
  return true;
  }

bool CGModelInfo::Display(int *seqlist, int *scnxlat, HMATRIX def, HTXITABLE txi, int frame)
  { 
  if (slist==NULL || seqlist==NULL) return false;
  bool dw;
  while (*seqlist>=0)
	{
	SGFrame frm=(*(alist[*seqlist]))[frame];
	LPHMATRIX refm,nw;
	HMATRIX mm;
	int scnref;
	int objectref=alist[*seqlist]->objectref;
	if (frm.flags & G3DFF_KEY) 
	  {
	  mxKeyToMatrix(frm.key,mm);
	  refm=mm;
	  }
	else
  	  refm=frm.key;
	if (scnxlat!=NULL && olist->list[objectref].sceneref>=0) scnref=scnxlat[olist->list[objectref].sceneref];
	else scnref=olist->list[objectref].sceneref;
	if (scnref<0) scnref=olist->list[objectref].sceneref;
	nw=olist->list[objectref].CalcMatrix(refm,olist->list,def);
	if (camobject==objectref) {camera->ObjectMatrix(refm);dw=drawcamobject;}else dw=true;
	if (dw) 
	  if (!olist->list[objectref].Display(slist,scnref,txi)) return false;
	seqlist++;
  	}
  mxframe.FreeFrames();
  return true;
  }

bool CGModelInfo::SetName(const char *name)
  {
  if (name==NULL)
	{
	delete [] this->name;
	this->name=NULL;	
	}
  else
	{
	int len=strlen(name);
	if (len>63) return false;
	char *c=new char[len+1];
	if (c==NULL) return false;
	strcpy(c,name);
	delete [] this->name;
	this->name=c;
	}
  return true;
  }

int CGModelInfo::FindObject(const char *name)
  {
  if (olist==NULL) return -1;
  for (int i=0;i<olist->count;i++)
	if (strcmp(olist->list[i].name,name)==0) return i;
  return -1;
  }

const SGObjectInfo* CGModelInfo::operator[] (int index)
  {
  if (olist==NULL) return NULL;
  if (index<0 || index>=olist->count) return NULL;
  return olist->list+index;
  }

int CGModelInfo::EnumChild(int parent,int last)
  {
  if (olist==NULL) return -1;
  last=__max(parent,last);
  for (int i=last;i<olist->count;i++) if (olist->list[i].parent==parent) return i;
  return -1;
  }

bool CGModelInfo::Save(FILE * f)
  {
  if (name)
	{
	int s=strlen(name)+1;
    if (!fwrite(name,s,1,f)) return false;
	}
  else
	if (!fwrite(&name,1,1,f)) return false;
  bool ex;
  ex=slist!=NULL; fwrite(&ex,1,1,f);
  if (ex)
	if (!slist->Save(f)) return false;
  ex=olist!=NULL; fwrite(&ex,1,1,f);
  if (ex)
  if (!olist->Save(f)) return false;
  if (!fwrite(&seqcount,4,1,f)) return false;
  for (int i=0;i<seqcount;i++) if (!alist[i]->Save(f)) return false;
  return true;
  }

bool CGModelInfo::Load(FILE * f)
  {
  char szBuff[64],*c=szBuff;
  int i;
  bool ex;

  CGModelInfo::~CGModelInfo();
  CGModelInfo::CGModelInfo();
  while ((i=getc(f))!=0 && i!=EOF)
	if (c-szBuff<63) *c++=i;
  szBuff[63]=0;
  if (i==EOF) return false;
  if (c[0]) if (!SetName(c)) return false;
  fread(&ex,1,1,f);
  if (ex) 
	{
	slist=new CGSceneList();
	if (!slist->Load(f)) return false;
	}
  else
	slist=NULL;
  fread(&ex,1,1,f);
  if (ex) {if ((olist=SG3DObjectList::Load(f))==NULL) return false;}
  else olist=NULL;
  if (!fread(&i,4,1,f)) return false;
  alist=(CGAnimation **)malloc(i*sizeof(CGAnimation *));
  for (seqcount=0;seqcount<i;seqcount++)
	{
	alist[seqcount]=new CGAnimation();
	if (!alist[seqcount]->Load(f))
	  {
	  delete alist[seqcount];
	  return false;
	  }
	}
  return true;
  } 

bool CGModelInfo::Attach(CGCamera * cam, int object, bool draw)
  {
  CGCamera *clon;
  if (cam==NULL) clon=cam,object=-1;else clon=(CGCamera *)cam->Clone();
  if (camera!=NULL) camera->Release();
  camera=clon;
  camobject=object;
  drawcamobject=draw;
  return true;
  }

int *CGModelInfo::Link(CGModelInfo * linkgr)
  {
  SG3DObjectList *ol=linkgr->olist;
  int maxnum=-1;
  int *res;
  int i;
  for (i=0;i<olist->count;i++)
	maxnum=__max(maxnum,olist->list[i].sceneref);
  maxnum++;
  if (maxnum==-1) return NULL;
  res=new int[maxnum];
  if (res==NULL) return NULL;
  for (i=0;i<ol->count;i++) if (ol->list[i].name)
	{
	int j=FindObject(ol->list[i].name);
	res[olist->list[i].sceneref]=ol->list[j].sceneref;
	}
  return res;
  }

//////////////////////////////////////////////////////////////////////
// CGModel Class
//////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

extern "C" int *g3dmGetAnimTickPointer();

CGModel::CGModel(CGModel *toclone)
  {
  seqlist=NULL;
  txi=NULL;
  if (toclone)	
	model=(CGModelInfo *)toclone->model->Clone();
  else
	model=new CGModelInfo();
  scnlist=NULL;
  curframe=0;
  trueframe=0;
  lastframe=0;
  cursequence=0;
  curobject=-1;
  frame1000=25;
  direction=1;
  lastticker=0;
  ticker=g3dmGetAnimTickPointer();
  scnxlat=NULL;
  }

CGModel::~CGModel()
  {
  if (scnlist) scnlist->Release();
  if (model) model->Release();
  if (txi) if (owntxi) free(txi);
  if (seqlist) delete [] seqlist;
  if (scnxlat) delete [] scnxlat;
  }


bool CGModel::Attach(HTXITABLE tbl, bool own)
  {
  if (owntxi) free(txi);
  txi=tbl;
  owntxi=own;
  return true;
  }


int CGModel::AddObject(int sceneref, int parent, const char * name)
  {
  int obj=model->AddObject(sceneref,parent,name);
  if (obj==-1) return -1;
  curobject=obj;
  return curobject;
  }


int CGModel::AddSequence(CGAnimation * anim, int seqid)
  {
  bool ok=model->Attach(anim,curobject,seqid);
  if (ok) return seqid;else return -1;
  }

int CGModel::SetCurObject(int object)
  {
  int parent=model->GetParentObject(object);
  if (parent==G3DM_NOTFOUND) return parent;
  curobject=object;
  return parent;
  }

int CGModel::SetCurSequence(int seqid, int direction, int flags)
  {
  delete [] seqlist;
  int i;
  int *sql;
  sql=model->BuildSeqIndexs(seqid,lastframe);
  if (sql==NULL) return -1;
  if (sql[0]==-1) {delete [] sql;return G3DM_SEQNOTFOUND;}
  delete [] seqlist;
  seqlist=sql;
  if (direction>0) curframe=0;
  else curframe=lastframe;
  i=cursequence;
  cursequence=seqid;
  this->flags=flags;
  return i;
  }


int CGModel::SetCurFrame(int framenum)
  {
  int last=curframe;
  framenum=(signed)(((unsigned)framenum)%(lastframe+1));
  curframe=framenum;
  return last;
  }


int CGModel::Animate(int howmany)
  {
  int res=0;
  int jmp=1000*howmany/frame1000;
  jmp+=curframe*direction;
  if (jmp>lastframe || jmp<0)
	{
	if (flags & G3DMT_NOTIFYEND) res=G3DM_LASTFRAME;
	if (flags & G3DMT_DONTLOOP) 
	  {
	  if (jmp<0) jmp=0;
	  if (jmp>lastframe) jmp=lastframe;	  
	  }
	}
  SetCurFrame(jmp);
  return G3D_OK;
  }

int CGModel::LoadToScene(HMATRIX mm, HTXITABLE txi)
  {
  bool res;
  if (txi==NULL) txi=this->txi;
  if (seqlist==NULL) return G3DM_NOCURRENTFRAME;
  if (flags & G3DMT_AUTOANIMATE)
	{
	int diff=*ticker-lastticker;
	Animate(diff);
	lastticker+=diff;
	}
  if (model->slist==NULL && scnlist==NULL) return G3D_INVALIDPARAMS;
  CGSceneList *save=model->slist;
  if (scnlist) model->slist=scnlist;
  res=model->Display(seqlist,scnxlat,mm,txi,curframe);
  model->slist=save;
  return res?0:-1;
  }

#define MAGIC "G3DMDL3"

bool CGModel::Save(FILE * f)
  {
  char *c=MAGIC;
  fwrite(c,8,1,f);
  return model->Save(f);
  }

bool CGModel::Load(FILE * f)
  {
  char c[8];
  fread(c,8,1,f);
  if (strcmp(c,MAGIC)) return false;
  return model->Load(f);
  }

bool CGModel::Load(char * filename)
  { 
  FILE *f=fopen(filename,"rb");
  bool res;
  if (f==NULL) return false;
  res=Load(f);
  fclose(f);
  return res;
  }

bool CGModel::Save(char * filename)
  { 
  FILE *f=fopen(filename,"wb");
  bool res;
  if (f==NULL) return false;
  res=Save(f);
  fclose(f);
  return res;
  }

bool CGModel::Link(CGModel *graphics)
  {
  if (graphics==NULL || graphics->model->slist==NULL) return false;
  if (scnxlat) {delete [] scnxlat;scnxlat=NULL;}
  scnlist->Release();
  scnlist=NULL;
  CGModelInfo *mdl=model;
  scnlist=(CGSceneList *)mdl->slist->Clone();
  scnxlat=model->Link(graphics->model);
  if (scnxlat==NULL) return false;
  return true;
  }

