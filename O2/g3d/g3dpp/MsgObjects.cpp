// MsgObjects.cpp: implementation of MessageObjectes
//
//////////////////////////////////////////////////////////////////////

#include <string.h>
#include <stdlib.h>
#include "MsgObjects.h"


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

static char zero=0;

CGString::CGString(char *text, int grow, int prealloc)
  {
  this->text=&zero;
  len=1;
  alloc=0;
  this->grow=grow;
  if (text)
	{
	int curalloc;
	len=strlen(text)+1;
	curalloc=__max(len,prealloc);
	if (Expand(curalloc))
	  strcpy(this->text,text);
	}
  }

bool CGString::Expand(int how)
  {
  int max=how+alloc;
  char *nw;  
  max=((max+grow-1)/grow)*grow;
  if (max<0) max=0;
  if (max==0) nw=NULL;else 
	{
	nw=new char[max];
	if (nw==NULL) return false;
	}
  if (nw==NULL) {alloc=0;nw=&zero;}else alloc=max;
  strncpy(nw,text,alloc);
  if (alloc) nw[alloc-1]=0;
  if (text!=NULL && text!=&zero) delete [] text;  
  text=nw;
  return true;
  }

CGString::CGString(const CGString & other)
  {
  CGString::CGString(other.text,other.grow);
  }

CGString::~CGString()
  {
  if (text!=&zero) delete [] text;
  }

const char *CGString::operator =(const char * txt)
  {
  if (txt==NULL) txt=&zero;
  len=strlen(txt)+1;
  if (len>alloc) if (Expand(len-alloc)==false) return text;
  strcpy(text,txt);
  SendMsg(&(CGMsgString(text)));
  return text;
  }

const char * CGString::operator +=(const char *other)
  {
  if (other==NULL) return text;
  int olen=strlen(other);
  int clen=olen+len;  
  if (clen>alloc) if (Expand(clen-alloc)==false) return text;
  strcpy(text+len,other);
  len=clen;
  SendMsg(&(CGMsgString(text)));
  return text;
  }

void CGString::Input(CGMessage *msg, int numids, ...)
  {
  if (msg->GetMsgID()!=CGMSG_GENERAL) return;
  if (numids==0) (*this)=msg->AsString(NULL,0);
  else
	{
	int id=(&numids)[1];
	SetAt(id,msg->AsString(NULL,0)[0]);
	}	
  }

void CGString::NeedData(CGMsgLink *sender, int numids, ...)
  {  
  if (numids>0) 
	{
	int id=(&numids)[1];
	if (id<0) SendMsgTo(sender,&CGMsgInt(len,CGMSG_GETCOUNT0));
	if (id<=len) SendMsgTo(sender,&CGMsgInt(text[id],CGMSG_GENERAL));
	}
  else	
	SendMsgTo(sender,&CGMsgString(text));	
  }

void CGEvent::Input(CGMessage *msg, int numids,...)
  {
  if (msg->GetMsgID()!=CGMSG_GENERAL) return;
  if (msg->AsInt()==pulse) SendMsg(&CGMessage(CGMSG_COMMAND));
  }

