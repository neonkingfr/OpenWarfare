// Messager.h: interface for the CMsgLink class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_MESSAGER_H__56B6F1E9_FF46_11D3_90D5_00C0DFAE7D0A__INCLUDED_)
#define AFX_MESSAGER_H__56B6F1E9_FF46_11D3_90D5_00C0DFAE7D0A__INCLUDED_

#if _MSC_VER >= 1000
#pragma once
#endif // _MSC_VER >= 1000

#include "G3DObject.h"
#include "Message.h"

#define CGMSG_CHANGETITLE 7
#define CGMSG_COMMAND 6
#define CGMSG_GETCOUNT3 5
#define CGMSG_GETCOUNT2 4
#define CGMSG_GETCOUNT1 3
#define CGMSG_GETCOUNT0 2
#define CGMSG_GENERAL 1
#define CGMSG_ALLMSG 0

class CGMsgLink : public CG3DObject  
  {	
	bool attached;
  public:	
	virtual void AttachToClient(CGMsgLink *client, int listen=CGMSG_GENERAL);
	virtual void Input(CGMessage *msg, int numids, ...) {}
	virtual void NeedData(CGMsgLink *sender, int numids, ...) {}
	virtual void Income(CGMsgLink *link,CGMessage *msg) {};
	void SendMsg(CGMessage *msg);	
	void SendMsgTo(CGMsgLink *link, CGMessage *msg) {link->Income(this,msg);}
	virtual void ClientDetach(CGMsgLink *client) {}
	CGMsgLink();
	virtual ~CGMsgLink();
  };

/*
Input 
	je volan od controleru, pokud byly zmenena vstupni data
	Slouzi k synchronizaci vnitrnich promennych 

NeedData
	je volan od view, pokud jsou potreba data k vykresleni
	Model zjisti souradnice dat a vysledek zasle jako zpravu
	SendMsgTo

IncomeFrom
	Je odpoved klienta serveru (this) na SendMsgFrom

Income
	Je volana k prijmuti broadcast zpravy SendMsg

SendMsg
	Posila broadcast zpravu
	
*/

#endif // !defined(AFX_MESSAGER_H__56B6F1E9_FF46_11D3_90D5_00C0DFAE7D0A__INCLUDED_)
