#ifndef _G3D_COLMAN_H_
#define _G3D_COLMAN_H_

#ifdef __cplusplus
extern "C" {
#endif

#define COLULONG unsigned long

typedef struct _tagColInfo
  {
  char name;		//name of item
  char bits;		//count of bits
  char baseshift;	//base shift right;
  COLULONG orgmask;	//original mask 
  COLULONG srcmask;	//source mask (shifted by baseshift)
  }TCOLINFO;

typedef struct _tagColModel
  {
  char srcpixelsize; //size of source pixel
  char trgpixelsize; //size of target pixel
  unsigned char palflags;	//one bit represent, that thiese item is searched in palete
  char palitem;	  //count of bytes of one palete entry
  int colcount;	   //count of color parts in model
  COLULONG templat;	//template color (if there is any unused item)
  TCOLINFO part[8];  //maximum is 8 parts
  TCOLINFO index; //how to find palette index in pixel
  struct _tagColModel *next;
  }TCOLMODEL;

typedef struct _tagPictInfo
  {
  int xsize,ysize;	//dimension of picture
  int srclinelen;	//source length of line (in bytes)
  int trglinelen;	//target length of line	(in bytes);
  int reduce;	  //factor to reducing the target resolution (useful for textures)
  void *srcpal;	  //source palette
  void *src;	  //source data
  void *trg;	  //space for target data
  unsigned char palentries; //entries in palette. 0 for 256  
  }TPICINFO;

int cmSetPaletteFormat(char *format);
int cmCreateColorModel(char *format, TCOLMODEL *mdl);
int cmCreateDiffModel(TCOLMODEL *mdl1, TCOLMODEL *mdl2,TCOLMODEL *pal, TCOLMODEL *diff);
int cmCreateConvModel(char *srcformat,char *trgformat, char *palformat, TCOLMODEL *conv);
int cmDoConvert(TPICINFO *picinfo, TCOLMODEL *conv);
int cmDoConvertRaw(TCOLMODEL *conv,void *src, void *trg, void *pal, int len,int palentries);
unsigned long cmCalcSpace(TPICINFO *info, TCOLMODEL *conv, int flags);

TCOLMODEL *cmAddFormatToList(TCOLMODEL *mdl, char *format);
void cmPurgeFormatList(TCOLMODEL *mdl);
TCOLMODEL *cmCreateBestConvModel(TCOLMODEL *list,char *format, char *palformat,  TCOLMODEL *conv);
TCOLMODEL *cmCreateBestConvModelEx(TCOLMODEL *list,char *format, char *palformat,  TCOLMODEL *conv, int hint);

#ifdef __DDRAW_INCLUDED__
int cmFormat2DDX(char *format, LPDDPIXELFORMAT lpddpx);
int cmFormat2DDX2(TCOLMODEL *mdl, LPDDPIXELFORMAT lpddpx);
int cmDDX2Format(LPDDPIXELFORMAT lpddpx, char *format);
#endif

typedef struct _tagUniPicture
  {
  unsigned long xsize;		// xsize
  unsigned long ysize;		// ysize
  unsigned long xlinelen;	//len of line
  unsigned long dataofset;	//begin of data (relative from start)
  unsigned long totaldata;	//total data len 
  unsigned short palettelen;//length of palete
  unsigned short mipmapcount; //count of mipmaps
  unsigned long transparent;//transparent item in pixel format
  char format[1];			//format folowed by data and palette
  }TUNIPICTURE;

TUNIPICTURE *cmLoadTGA(char *name);

#define cmGetData(unipic) ((void *)((char *)(unipic)+(unipic)->dataofset))
#define cmGetPalette(unipic) ((unipic)->palettelen?((void *)((char *)(unipic)+(unipic)->dataofset+(unipic)->totaldata)):NULL)
#define cmGetFormat(unipic) ((unipic)->format)
#define cmGetXSize(unipic) ((unipic)->xsize)
#define cmGetYSize(unipic) ((unipic)->ysize)
#define cmGetXlen(unipic) ((unipic)->xlinelen)
#define cmGetPalFormat(unipic) (strchr((unipic)->format,'|')!=NULL?strchr((unipic)->format,'|')+1:NULL)
#define cmGetSize(unipic) ((unipic)->dataofset+(unipic)->totaldata+(unipic)->palettelen)
#define cmGetMipMap(unipic,mp) ((void *)((char *)(unipic)+(unipic)->dataofset+cmCalcMipMapLen(unipic,mp)))

TUNIPICTURE *cmLoadUni(char *filename);
int cmSaveUni(char *filename, TUNIPICTURE *pic);

int cmGetMaxMipMapCount(int xsize, int ysize);
TUNIPICTURE *cmCreateUni(int xsize, int ysize, char *format, int align, int pallen);
TUNIPICTURE *cmCreateMipMap(int xsize, int ysize, char *format, int count, int pallen);
void *cmUni2BitMap(TUNIPICTURE *pic, char checker, int xmax, int ymax);
TUNIPICTURE *cmConvertUni(TUNIPICTURE *pic, const char *newformat, int reduce);

_inline int cmCalcMipMapLen(TUNIPICTURE *unipic,int mp)
  {
  int size=unipic->ysize*unipic->xlinelen;
  int len=0;
  while (mp--)
	{
    len+=size;
	size>>=2;
	}
  return len;
  }


#define CMC_DONTCARE 0
#define CMC_NOALIGN 1
#define CMC_ALIGNWORD 2
#define CMC_ALIGNDWORD 4
#define CMC_ALIGNQWORD 8

#define CME_OK 0
#define CME_NOMEMORY	0xC001
#define CME_FORMATERROR 0xC002
#define CME_PICTUREFORMATERROR 0xC003
#define CME_NOFORMATSINLIST 0xC004
#define CME_INVALIDPIXELFORMAT 0xC005

typedef struct
  {
  int xsize;
  int ysize;
  int bits;
  unsigned long bitmask;
  void *source;
  void *target;
  long sourcestride;
  long targetstride;
  char flags;
  }TMIPMAPGENSTRUCT;

#define CMMG_NEAREST 0
#define CMMG_LINEAR 1

char cmMipMapGen(TMIPMAPGENSTRUCT *mmgs);
char cmGetMipMapMask(TCOLMODEL *mdl, TMIPMAPGENSTRUCT *mmgs);

#ifdef __cplusplus
  }
#endif

#endif