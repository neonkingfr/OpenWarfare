#include <stdio.h>
#include <windows.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>
#include "g3dlib.h"

#define DLLNAME "G3DHWCFG"

typedef int (* tg3dConfigDlg)(HINSTANCE self, HWCONFIG2 *cfg);

char g3dConfigureHardware(HWCONFIG2 *cfg,unsigned long autoinit, ...)
  {
  HMODULE handle;
  unsigned long window=(&autoinit)[1];
  tg3dConfigDlg g3dConfigDlg;
  int res;
  char *c=cfg->szDeviceName;
  int dllsize=20;
  if (cfg->szDllPath!=NULL) dllsize+=strlen(cfg->szDllPath)+1;
  if (c==NULL) c=(char *)alloca(dllsize);
  if (c==NULL) return G3D_NOMEMORY;
  if (cfg->szDllPath!=NULL) strcpy(c,cfg->szDllPath);else c[0]=0;
  strcat(c,DLLNAME);
  handle=LoadLibrary(c);
  if (handle==NULL) return G3D_OBJECTNOTFOUND;
  g3dConfigDlg=(tg3dConfigDlg)GetProcAddress(handle,MAKEINTRESOURCE(1));
  if (g3dConfigDlg==NULL)
    {
    FreeLibrary(handle);
    return G3D_OBJECTNOTFOUND;
    }
  if (cfg->szDeviceName==NULL) cfg->szDeviceName=c;
  res=g3dConfigDlg((HINSTANCE)handle,cfg);
  FreeLibrary(handle);
  if (res!=IDOK) return 1;
  if (autoinit & HWA_LOADDEVICE)
    {
    G3D_HWINFO *info;
    int i;
    if (g3dInitDevice(cfg->szDeviceName)) return G3D_OBJECTNOTFOUND;
    if (autoinit & HWA_SELECTHARDWARE)
      {
      i=g3dQueryHardware(&info);
      if (i<=cfg->iHwResult) return 1;
      g3dSelectHardware(info+cfg->iHwResult);
      if (autoinit & HWA_INITSCREEN)
        {
        return g3dInitHardware3D(window,G3DRES(cfg));
        }
      }
    }
  return 0;
  }

