//------------------- G3D FILE SYSTEM ---------------

_inline long gread(void *buf, long size, long count, HANDLE file)
  {
  DWORD readen;
  ReadFile(file,buf,size*count,&readen,NULL);
  return readen/size;
  }

_inline long gseek(HANDLE file, long pos, int mode)
  {
  int seekmode;

  switch (mode)
	{
	case SEEK_SET:seekmode=FILE_BEGIN;break;
	case SEEK_CUR: seekmode=FILE_CURRENT;break;
	case SEEK_END: seekmode=FILE_END;break;
	}
  return SetFilePointer(file,pos,NULL,seekmode);  
  } 

_inline long gtell(HANDLE file)
  {
  return SetFilePointer(file,0,NULL,FILE_CURRENT);
  }

_inline HANDLE gopenread(char *filename)
  {
  return CreateFile(filename,GENERIC_READ,FILE_SHARE_READ,NULL,OPEN_EXISTING,FILE_ATTRIBUTE_NORMAL|FILE_FLAG_RANDOM_ACCESS,NULL);
  }

_inline void gclose(HANDLE f)
  {
  CloseHandle(f);
  }

_inline int ggetc(HANDLE f)
  {
  unsigned char c;
  if (gread(&c,1,1,f)==0) return -1;
  return c;
  }