#ifndef _G3D_2D_H_
#define _G3D_2D_H_

#ifdef __cplusplus
extern "C" {
#endif



typedef struct _tagG3DFChar
  {
  float tv1,tu1;
  float tv2,tu2;
  long handle;
  }G3DCHARDEF;

typedef struct _tagFont
  {
  G3DCHARDEF chardef[256];
  int bitmapsize;	//sizes of bitmap (for calculating the basic font size)
  }G3DFONT,*HG3DFONT;


typedef struct _g3dFontSpec
  {
  float xx,xy,yx,yy;	//directions of writting
  int align;	  //align flags
  float size;	  //font size
  float aspect;	  //aspec ration
  unsigned long color;  //font color;  
  HG3DFONT curfont;	  //curent font
  unsigned long shadowcolor;
  int shadowdistancex;
  int shadowdistancey;
  int bmpmul;	//multiply for font size
  }G3DFONTSPEC;

typedef struct _g3dDrawContext
  {
  int wl,wr,wt,wb;	  //window
  unsigned long color; //drawing color;
  unsigned long bgcolor;
  float basezet;	//base zet pos;
  int xcur, ycur;
  G3DFONTSPEC font;
  }G3DCONTEXT,* HG3DCONTEXT;

extern HG3DCONTEXT g3dActiveContext;

HG3DFONT g3dLoadFont(char *filename);
void g3dDestroyFont(HG3DFONT font);

void g3d2dInit();
void g3d2dDone();

HG3DCONTEXT g3dCreateContext();
HG3DCONTEXT g3dCreateWindow(int wl,int wt, int wr, int wb);
#define g3dCreateWindow2(x,y,xs,ys) g3dCreateWindow(x,y,(x)+(xs)-1,(y)+(ys)-1)

#define g3dDeleteContext(cnt) free(cnt)

#define G3DF_ALEFT 0x1
#define G3DF_ARIGHT 0x2
#define G3DF_ATOP 0x4
#define G3DF_ABOTTOM 0x8
#define G3DF_ACENTER 0x0

HG3DCONTEXT g3dSetActive(HG3DCONTEXT cnt);
#define g3dGetActive() g3dActiveContext

#define g3dSetColor(scolor) g3dActiveContext->color=scolor
#define g3dSetBkColor(scolor) g3dActiveContext->bgcolor=scolor
#define g3dSetAlign(salign) g3dActiveContext->font.align=salign
#define g3dSetFontSize(ssize) g3dActiveContext->font.size=ssize
#define g3dSetFontAspect(saspect) g3dActiveContext->font.aspect=saspect
#define g3dSetFontColor(scolor) g3dActiveContext->font.color=scolor
#define g3dSetShadowDistance(sx,sy) g3dActiveContext->font.shadowdistancex=sx,g3dActiveContext->font.shadowdistancey=sy
#define g3dSetShadowColor(scolor) g3dActiveContext->font.shadowcolor=scolor
#define g3dMoveTo(sx,sy) g3dActiveContext->xcur=sx,g3dActiveContext->ycur=sy

#define g3dGetColor() g3dActiveContext->color
#define g3dGetBkColor() g3dActiveContext->bgcolor
#define g3dGetFont() g3dActiveContext->font.curfont
#define g3dGetAlign() g3dActiveContext->font.align
#define g3dGetFontSize() g3dActiveContext->font.size
#define g3dGetFontAspect() g3dActiveContext->font.aspect
#define g3dGetFontColor() g3dActiveContext->font.color
#define g3dGetShadowDistanceX() g3dActiveContext->font.shadowdistancex
#define g3dGetShadowDistanceY() g3dActiveContext->font.shadowdistancey
#define g3dGetShadowColor(scolor) g3dActiveContext->font.shadowcolor=scolor
#define g3dWhereX() g3dActiveContext->xcur
#define g3dWhereY() g3dActiveContext->ycur
#define g3dGetLeft() g3dActiveContext->wl
#define g3dGetRight() g3dActiveContext->wr
#define g3dGetTop() g3dActiveContext->wt
#define g3dGetBottom() g3dActiveContext->wb
#define g3dGetSizeX() (g3dGetRight()-g3dGetLeft()+1)
#define g3dGetSizeY() (g3dGetBottom()-g3dGetTop()+1)

void g3dSetFont(HG3DFONT sfont);
void g3dSetContextZ(float z);

#define G3D_GETCOLOR_R(color) ((((color)>>16)&0xFF)/255.0f)
#define G3D_GETCOLOR_G(color) ((((color)>>8)&0xFF)/255.0f)
#define G3D_GETCOLOR_B(color) (((color)&0xFF)/255.0f)
#define G3D_GETCOLOR_A(color) (((color)>>24)/255.0f)

void g3dSetWriteDirection(float radx);
void g3dSetFontSkew(float radx);
void g3dClipToWindow();
void g3dClipToScreen();
void g3dLine(int x1, int y1, int x2, int y2);
void g3dClearContext();
void g3dFillTexture(long handle, float wl, float wt, float wr, float wb);

void g3dBeginPaint(int buffer);
void g3dEndPaint();

char g3dGetCharExtent(int chr, int *xs, int *ys);
char g3dGetTextExtent(char *text,int chrs, int *x, int *y);
char g3dTestPoint(int xp, int yp);
void g3dOutText(char *text, int len);

void g3dRectangle(int wl,int wt, int wr, int wb);

void g3d2dShowMouse(int x, int y, int handle, TSCREENCLIP *back);

#ifdef __cplusplus
  }
#endif


#endif