#include <windows.h>
#include <stdio.h>
#include <process.h>

static HWND dWnd=NULL;
static char debugenabled=1;

#define lpszAppName "BREDYS DEBUG WINDOW"

#define appName "debugwnd.exe"

static char szBuff[1024];

static char AttachDebugWin()
  {
  dWnd=FindWindow(lpszAppName,NULL);
  if (dWnd==NULL)
      {
      debugenabled=0;
      return -1;
      }
  return 0;
  }

void DebugPrint(char *text, ... )
  {
  ATOM aa;
  DWORD res;
  static int volatile zavora=0;

  while (zavora) Sleep(1000);
  if (!debugenabled) return;
  if (!IsWindow(dWnd))
    if (AttachDebugWin()==-1) return;
  zavora=1;
  _vsnprintf(szBuff,sizeof(szBuff),text,(char *)((&text)+1));
  szBuff[1023]=0;
  aa=GlobalAddAtom(szBuff);
  SendMessageTimeout(dWnd,WM_USER,0,(LPARAM)aa, SMTO_ABORTIFHUNG | SMTO_BLOCK, 2000, &res);
  GlobalDeleteAtom(aa);
  if (res==1234)
    {
    while (SendMessageTimeout(dWnd,WM_USER,1,0, SMTO_ABORTIFHUNG | SMTO_BLOCK, 2000, &res)==TRUE)
      if (res!=1234) break;else Sleep(1000);
    }
  if (res==4321)
    {
    __asm     //Make user interrupt - asserttation
      {
      int 3
      }
    }
  zavora=0;
  }
 