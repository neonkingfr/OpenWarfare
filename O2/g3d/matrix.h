#ifndef _TRANSFORMATIONS_H_

#define _TRANSFORMATIONS_H_

#include <math.h>

#ifdef __cplusplus
extern "C" {
#endif

#define XVAL 0
#define YVAL 1
#define ZVAL 2
#define WVAL 3

typedef float HMATRIX[4][4];
typedef float HVECTOR[4];
#define HQUATER HVECTOR
typedef float *LPHVECTOR;
typedef float (*LPHMATRIX)[4];
#define LPHQUATER LPHVECTOR

typedef struct _hvertex
  {
  HVECTOR pozice;
  HVECTOR normala;
  float tu;
  float tv;
  }HVERTEX;

#define FPI 3.14159265f
#define round(x) (int)(floor((x)+0.5))
#define torad(x) ((x)*FPI/180.0f)
#define todec(x) ((x)*180.0f/FPI)

#define ModulVektoru2(v) ((v)[XVAL]*(v)[XVAL]+(v)[YVAL]*(v)[YVAL]+(v)[ZVAL]*(v)[ZVAL])
#define ModulVektoru(v) ((float)sqrt(ModulVektoru2(v)))
#define Scalar1(v1,v2) ((v1)[XVAL]*(v2)[XVAL]+(v1)[YVAL]*(v2)[YVAL]+(v1)[ZVAL]*(v2)[ZVAL])
#define VektorSoucin(v1,v2,vn) (\
  (vn)[XVAL]=(v1)[YVAL]*(v2)[ZVAL]-(v1)[ZVAL]*(v2)[YVAL],\
  (vn)[YVAL]=(v1)[ZVAL]*(v2)[XVAL]-(v1)[XVAL]*(v2)[ZVAL],\
  (vn)[ZVAL]=(v1)[XVAL]*(v2)[YVAL]-(v1)[YVAL]*(v2)[XVAL]\
  )

#define SestavMatici(v1,v2,v3,pos,mm) \
  (memcpy(mm[0],v1,sizeof(HVECTOR)),\
   memcpy(mm[1],v2,sizeof(HVECTOR)),\
   memcpy(mm[2],v3,sizeof(HVECTOR)),\
   memcpy(mm[3],pos,sizeof(HVECTOR)))

#define SmerMatice(mm) mm[3]

#define CosMezi(v1,v2) (Scalar1(v1,v2)/(float)sqrt(ModulVektoru2(v1)*ModulVektoru2(v2)))
#define ModulVektoru42(v) ((v)[XVAL]*(v)[XVAL]+(v)[YVAL]*(v)[YVAL]+(v)[ZVAL]*(v)[ZVAL]+(v)[WVAL]*(v)[WVAL])
#define ModulVektoru4(v) ((float)sqrt(ModulVektoru42(v)))
#define Scalar4(v1,v2) ((v1)[XVAL]*(v2)[XVAL]+(v1)[YVAL]*(v2)[YVAL]+(v1)[ZVAL]*(v2)[ZVAL]+(v1)[WVAL]*(v2)[WVAL])
#define CosMezi4(v1,v2) (Scalar4(v1,v2)/(float)sqrt(ModulVektoru42(v1)*ModulVektoru42(v2)))
#define SoucetVektoru(v1,v2) (\
  (v1)[XVAL]+=(v2)[XVAL],\
  (v1)[YVAL]+=(v2)[YVAL],\
  (v1)[ZVAL]+=(v2)[ZVAL])
#define SoucetVektoru4(v1,v2) (\
  (v1)[XVAL]+=(v2)[XVAL],\
  (v1)[YVAL]+=(v2)[YVAL],\
  (v1)[ZVAL]+=(v2)[ZVAL]),\
  (v1)[WVAL]+=(v2)[WVAL])
#define RozdilVektoru(v1,v2) (\
  (v1)[XVAL]-=(v2)[XVAL],\
  (v1)[YVAL]-=(v2)[YVAL],\
  (v1)[ZVAL]-=(v2)[ZVAL])
#define RozdilVektoru4(v1,v2) (\
  (v1)[XVAL]-=(v2)[XVAL],\
  (v1)[YVAL]-=(v2)[YVAL],\
  (v1)[ZVAL]-=(v2)[ZVAL],\
  (v1)[WVAL]-=(v2)[WVAL])
#define NasobVektor(v1,k) (\
  (v1)[XVAL]*=k,(v1)[YVAL]*=k, (v1)[ZVAL]*=k)
#define NasobVektor4(v1,k) (\
  (v1)[XVAL]*=k,(v1)[YVAL]*=k, (v1)[ZVAL]*=k,(v1)[WVAL]*=k)
#define CorrectVectorM(v1) do \
                            { \
                            if ((v1)[WVAL]!=1.0f) \
                              {                    \
                              float f=1/(v1)[WVAL]; \
                              (v1)[XVAL]*=f;         \
                              (v1)[YVAL]*=f;          \
                              (v1)[ZVAL]*=f;           \
                              (v1)[WVAL]=1.0f;          \
                              }                          \
                            }                             \
                            while (0)

#define XYZW2XYZQ(v1) do \
                            { \
                            if ((v1)[WVAL]!=1.0f) \
                              {                    \
                              float f=1/(v1)[WVAL]; \
                              (v1)[XVAL]*=f;         \
                              (v1)[YVAL]*=f;          \
                              (v1)[ZVAL]*=f;           \
                              (v1)[WVAL]=f;            \
                              }                          \
                            }                             \
                            while (0)

#define CopyVektor(v1,v2) memcpy(v1,v2,sizeof(HVECTOR))
#define CopyMatice(mm1,mm2) memcpy(mm1,mm2,sizeof(HMATRIX))


typedef int TPOINT[2];

void JednotkovaMatice(LPHMATRIX m); //vytvari jednotkovou matici
void NulovaMatice(LPHMATRIX m); //vytvari nulovou matici
void SoucinMatic(LPHMATRIX m1,LPHMATRIX m2,LPHMATRIX vysledek);
  //nasobi matice. Vysledek muze byt prirazen do m1 pak se provadi operace
  // m1=m1*m2
void MocninaMatice(LPHMATRIX m,LPHMATRIX vysledek);
  //pocita m^2. Pokud je vysledek=m pak pocita m=m^2

void Translace(LPHMATRIX m,float xs,float ys,float zs);
  //vytvari matici pro translaci (posun)
void RotaceX(LPHMATRIX m,float uhel);
void RotaceY(LPHMATRIX m,float uhel);
void RotaceZ(LPHMATRIX m,float uhel);
  //vytvari matice pro rotace podle urcitych os
void Zoom(LPHMATRIX m,float Sx,float Sy,float Sz);
  //vytvari matice pro zoom a deformaci zvetsenim

void TransformVector(LPHMATRIX m,LPHVECTOR v,LPHVECTOR z);
  //transformuje vektor podle urcite matice, vysledek se ulozi do z
void TransformVectorNoTranslate(LPHMATRIX m,LPHVECTOR v,LPHVECTOR z);
  //transformuje vektor podle urcite matice, vysledek se ulozi do z. Netransformuje posunuti
  //Hodi pri transformaci relativnich vektoru
//void InvertujMatici(HMATRIX m); /* v karterskych souradnicich!!! */


void CorrectVector(LPHVECTOR v);
void NormalizeVector(LPHVECTOR v);
void CreateProjection(LPHMATRIX m,float front,float back, float half);
void CreateProjection2(LPHMATRIX m,float front,float back, float fov);
//Vytvari projekci pomoci uhlu vyhledu
char InverzeMatice(LPHMATRIX in,LPHMATRIX out);
void PohledovaMatice(LPHVECTOR fromvect, LPHVECTOR tovect, LPHVECTOR up, LPHMATRIX mm);

extern HVECTOR _hmatrix_static_vector;

_inline LPHVECTOR mxInline_SetVector4(float x, float y, float z, float w)
  {
  _hmatrix_static_vector[XVAL]=x;
  _hmatrix_static_vector[YVAL]=y;
  _hmatrix_static_vector[ZVAL]=z;
  _hmatrix_static_vector[WVAL]=w;
  return _hmatrix_static_vector;
  }

#define Vector4(x,y,z,w) mxInline_SetVector4(x,y,z,w)
#define Vector3(x,y,z) mxInline_SetVector4(x,y,z,1.0f)
#define Vector2(x,y) mxInline_SetVector4(x,y,0.0f,1.0f)

#define Pudorys(mm) do {JednotkovaMatice(mm);mm[2][2]=0.0f;} while (0)

//----------------- MATRIX STACKS ------------------------

typedef struct _matrixstackitem
  {
  struct _matrixstackitem *next;
  HMATRIX matrix;
  }TMATRIXSTACKITEM;

typedef struct _matrixstackdef
  {
  TMATRIXSTACKITEM *top;
  HMATRIX temp;
  char forcecalc;
  char errorcounter;
  }TMATRIXSTACK,*HMATRIXSTACK;

void mxsInitStack(HMATRIXSTACK stk);
LPHMATRIX mxsPushL(HMATRIXSTACK stk);
LPHMATRIX mxsPushR(HMATRIXSTACK stk);
LPHMATRIX mxsTop(HMATRIXSTACK stk);
LPHMATRIX mxsPop(HMATRIXSTACK stk, HMATRIX mm);
void mxsFreeStack(HMATRIXSTACK stk);
void mxsResetTables(void);

LPHMATRIX mxQuatToMatrix(HQUATER q, HMATRIX mat);
LPHQUATER mxMatrixToQuat(HMATRIX mat, HQUATER q);
LPHQUATER mxSlerpQuat(HQUATER p, HQUATER q, float t, HQUATER qt);
LPHQUATER mxSquadQuat(HQUATER p, HQUATER q, HQUATER r, HQUATER s, float t, HQUATER res);
LPHQUATER mxMultQuat(HQUATER q, HQUATER p, HQUATER res);
LPHQUATER mxMakeQuat(HVECTOR axis, float rotate, HQUATER res);
LPHQUATER mxMakeRelativeRotation(HQUATER cur, HVECTOR axis, float rotate);
LPHMATRIX mxBuildKey(HVECTOR pivot, HVECTOR pos, HQUATER rot, HVECTOR scale, HMATRIX res);
LPHMATRIX mxInterpolateKey(HMATRIX mat1, HMATRIX mat2, float t,HMATRIX res);
LPHMATRIX mxRotateAroundAxis(HVECTOR vect, float fi, HMATRIX res);
LPHVECTOR mxBezierInterpolate(HMATRIX bezier, float factor, HVECTOR res);
LPHMATRIX mxKeyToMatrix(HMATRIX quat, HMATRIX mat);

#define MXKEY_ROTTRACK 0
#define MXKEY_PIVOT 2
#define MXKEY_SCLTRACK 1
#define MXKEY_POSTRACK 3

#ifdef __cplusplus
}
#endif
#endif

