#define EXPANDCOUNT 32
#include <stdio.h>
#include <malloc.h>
#include <math.h>
#include <string.h>
#include "g3dlib.h"


#define BodySize(items) (sizeof(TG3DBODY)+sizeof(TG3DBONE)*items)

#define ProccesVector(vv,ln,nm) \
  (vv)[XVAL]+=(nm)[XVAL]*ln, \
  (vv)[YVAL]+=(nm)[YVAL]*ln, \
  (vv)[ZVAL]+=(nm)[ZVAL]*ln

HG3DBODY g3dbCreateBody(int preres)
  {
  HG3DBODY bd;

  bd=malloc(BodySize(preres));
  bd->count=0;
  bd->listlen=preres;
  return bd;
  }

#define NeedExpand(bd) ((bd)->count==(bd)->listlen)

int g3dbAddBone(HG3DBODY bd, POINT3D beg, POINT3D end, POINT3D normal)
  {
  HG3DBONE bn;
  HVECTOR vc;
  if (bd==NULL) return G3D_INVALIDPARAMS;
  if (NeedExpand(bd)) return G3DB_NEEDEXPAND;
  bn=bd->list+bd->count++;
  memcpy(bn->base,beg,sizeof(POINT3D));
  vc[XVAL]=end[XVAL]-beg[XVAL];
  vc[YVAL]=end[YVAL]-beg[YVAL];
  vc[ZVAL]=end[ZVAL]-beg[ZVAL];
  vc[WVAL]=bn->length=ModulVektoru(vc);
  CorrectVector(vc);
  memcpy(bn->direction,vc,sizeof(POINT3D));
  memcpy(bn->key1,normal,sizeof(POINT3D));
  VektorSoucin(bn->direction,bn->key1,bn->key2);
  return bd->count-1;
  }

int g3dbAddBoneRel(HG3DBODY bd, int root, POINT3D direction, float len, POINT3D normal)
  {
  HG3DBONE bn,br;
  if (bd==NULL) return G3D_INVALIDPARAMS;
  if (root>=bd->count) return G3D_INVALIDPARAMS;
  if (NeedExpand(bd)) return G3DB_NEEDEXPAND;
  bn=bd->list+bd->count++;
  br=bd->list+root;
  memcpy(bn->base,br->base,sizeof(POINT3D));
  memcpy(bn->direction,direction,sizeof(POINT3D));
  memcpy(bn->key1,normal,sizeof(POINT3D));
  VektorSoucin(bn->direction,bn->key1,bn->key2);
  ProccesVector(bn->base,br->length,br->direction);
  return bd->count-1;
  }

int g3dbAddKnee(HG3DBODY bd, int b1, int b2, float factor)
  {
  HG3DBONE bn,bn1,bn2;
  float m,f1=factor,f2=1-factor;
  if (bd==NULL) return G3D_INVALIDPARAMS;
  if (b1>=bd->count || b2>=bd->count) return G3D_INVALIDPARAMS;
  if (NeedExpand(bd)) return G3DB_NEEDEXPAND;
  bn=bd->list+bd->count++;
  bn1=bd->list+b1;
  bn2=bd->list+b2;
  memcpy(bn->base,bn2->base,sizeof(POINT3D));
  bn->direction[XVAL]=bn1->direction[XVAL]*f1+bn2->direction[XVAL]*f2;
  bn->direction[YVAL]=bn1->direction[YVAL]*f1+bn2->direction[YVAL]*f2;
  bn->direction[ZVAL]=bn1->direction[ZVAL]*f1+bn2->direction[ZVAL]*f2;
  m=1.0f/ModulVektoru(bn->direction);
  bn->direction[XVAL]*=m;
  bn->direction[YVAL]*=m;
  bn->direction[ZVAL]*=m;
  bn->key1[XVAL]=bn1->key1[XVAL]*f1+bn2->key1[XVAL]*f2;
  bn->key1[YVAL]=bn1->key1[YVAL]*f1+bn2->key1[YVAL]*f2;
  bn->key1[ZVAL]=bn1->key1[ZVAL]*f1+bn2->key1[ZVAL]*f2;
  m=1.0f/ModulVektoru(bn->key1);
  bn->key1[XVAL]*=m;
  bn->key1[YVAL]*=m;
  bn->key1[ZVAL]*=m;
  VektorSoucin(bn->direction,bn->key1,bn->key2);
  bn->length=1.0f;
  return bd->count-1;
  }

char g3dbExpandBody(HG3DBODY *bd,int items)
  {
  int celk=(*bd)->listlen+items;
  HG3DBODY bd2;

  if (celk==0) celk=0;
  bd2=realloc(*bd,BodySize(celk));
  if (bd2==NULL) return G3DB_NOMEMORY;
  bd2->listlen=celk;
  *bd=bd2;
  return G3DB_OK;
  }

char g3dbDoneCreating(HG3DBODY *bd)
  {
  HG3DBODY bd2;

  bd2=realloc(*bd,BodySize((*bd)->count));
  if (bd2==NULL) return G3DB_NOMEMORY;
  bd2->listlen=bd2->count;
  *bd=bd2;
  return G3DB_OK;
  }

static void AttachToBone(HG3DBONE bn, unsigned short bonindex, HVECTOR vect, char corr)
  {
  POINT3D vc1,vc2;
  float m;
  TBODYVECTOR vc;

  CorrectVectorM(vect);
  memcpy(vc1,vect,sizeof(POINT3D));
  RozdilVektoru(vc1,bn->base);
  m=Scalar1(vc1,bn->direction);
  memcpy(vc2,bn->direction,sizeof(POINT3D));
  NasobVektor(vc2,m);
  vc2[XVAL]+=bn->base[XVAL];
  vc2[YVAL]+=bn->base[YVAL];
  vc2[ZVAL]+=bn->base[ZVAL];
  vc2[XVAL]=vect[XVAL]-vc2[XVAL];
  vc2[YVAL]=vect[YVAL]-vc2[YVAL];
  vc2[ZVAL]=vect[ZVAL]-vc2[ZVAL];
  if (corr) vc.pt[ZVAL]=m/bn->length;else vc.pt[ZVAL]=m;
  vc.pt[XVAL]=Scalar1(vc2,bn->key1);
  vc.pt[YVAL]=Scalar1(vc2,bn->key2);
  vc.bone=bonindex;
  vc.flags=0;
  memcpy(vect,&vc,sizeof(HVECTOR));
  }

int g3dbAddPosition(HG3DBODY bd, unsigned short bonindex,HVECTOR vect)
  {
  HG3DBONE bn;
  HVECTOR vc1;

  if (bd==NULL) return G3D_INVALIDPARAMS;
  if (bonindex>=bd->count) return G3D_INVALIDPARAMS;
  bn=bd->list+bonindex;
  memcpy(vc1,vect,sizeof(HVECTOR));
  AttachToBone(bn,bonindex,vc1,1);
  return g3dAddPosition(vc1);
  }

int g3dbAddNormal(HG3DBODY bd, unsigned short boneindex,HVECTOR vect)
  {
  HG3DBONE bn;
  HVECTOR vc1;

  if (bd==NULL) return G3D_INVALIDPARAMS;
  if (boneindex>=bd->count) return G3D_INVALIDPARAMS;
  bn=bd->list+boneindex;
  vc1[XVAL]=vect[XVAL]+bn->base[XVAL];
  vc1[YVAL]=vect[YVAL]+bn->base[YVAL];
  vc1[ZVAL]=vect[ZVAL]+bn->base[ZVAL];
  vc1[WVAL]=1.0f;
  AttachToBone(bn,boneindex,vc1,0);
  return g3dAddNormal(vc1);
  }

char g3dbLoadObjectEx(HG3DBODY bd, TSCENEDEF *object, HMATRIX matrix, unsigned short *texture_table, int count)
  {
  TSCENEDEF temp;
  HG3DBONE bn;
  int i;
  TWORLDVERTEX *wv;
  TNORMALVERTEX *nv;
  HVECTOR vv;
  TBODYVECTOR *vc;
  float f;
  int res;

  memcpy(&temp,object,sizeof(TSCENEDEF));
  temp.nsize=0;
  temp.wsize=0;
  res=g3dLoadObjectEx(&temp,matrix,texture_table,count);
  if (res<0) return res;
  wv=object->wtable;
  nv=object->ntable;
  vv[WVAL]=1.0f;
  for (i=0;i<object->wsize;i++,wv++)
    {
    vc=(TBODYVECTOR *)wv->vector;
    bn=bd->list+vc->bone;
    memcpy(vv,bn->base,sizeof(POINT3D));
    f=vc->pt[ZVAL]*bn->length;
    ProccesVector(vv,f,bn->direction);
    ProccesVector(vv,vc->pt[XVAL],bn->key1);
    ProccesVector(vv,vc->pt[YVAL],bn->key2);
    res=g3dAddPosition(vv);
    if (res<0) return res;
    }
  for (i=0;i<object->nsize;i++,nv++)
    {
    vc=(TBODYVECTOR *)nv->vector;
    bn=bd->list+vc->bone;
    memset(vv,0,sizeof(POINT3D));
    f=vc->pt[ZVAL];
    ProccesVector(vv,f,bn->direction);
    ProccesVector(vv,vc->pt[XVAL],bn->key1);
    ProccesVector(vv,vc->pt[YVAL],bn->key2);
    res=g3dAddNormal(vv);
    if (res<0) return res;
    }
  return 0;
  }
      
char g3dbAttachObject(HG3DBODY bd, unsigned short boneindex, TSCENEDEF *object)
  {
  TSCENEDEF temp;
  TWORLDVERTEX *wv;
  TNORMALVERTEX *nv;
  int i;
  int res;

  memcpy(&temp,object,sizeof(TSCENEDEF));
  temp.nsize=0;
  temp.wsize=0;
  res=g3dLoadObject(&temp,NULL);
  wv=object->wtable;
  nv=object->ntable;
  for (i=0;i<object->wsize && res>=0;i++,wv++) res=g3dbAddPosition(bd,boneindex,wv->vector);
  for (i=0;i<object->nsize && res>=0;i++,nv++) res=g3dbAddNormal(bd,boneindex,nv->vector);
  return res;
  }

