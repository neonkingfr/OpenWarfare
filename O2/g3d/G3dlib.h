//Tento hlavickovy soubor shrnuje vsechny knihovny systemu g3d
//staci jej tedy vlozit a mate k dispozici vsechny funkce g3d.

#include "matrix.h"
#include "colman.h"
#include "texman.h"
#include "g3d_dev.h"
#include "g3d.h"
#include "g3dmodel.h"
#include "g3dio.h"
#include "g3d2d.h"
#include "g3dbody.h"
#include "dbgwnd.h"
#include "g3dmdl2.h"
#ifdef _WINUSER_
#include "g3dcfg.h"
#include "g3dsys.h"
#endif

