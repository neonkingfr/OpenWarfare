#define G3DMC_3DDEVICE 0x80000001
#define G3DMC_TEXINPLUGIN 0x80000002
#define G3DMC_MATHPLUGIN 0x80000003
#define G3DMC_SELHWPLUGIN 0x80000004

typedef char  (_stdcall * G3DENUMMODESCALLBACK)(int xsize, int ysize, int bpp, int freq, void *context);

#ifndef __G3D_DEF_IMPORT
#define IMPORT(a,b,c) a ##b##c
#endif

IMPORT(char *,g3dGetSelfName,());
IMPORT(unsigned long,g3dGetModuleClass,());
IMPORT(char,g3dEnumDisplayModes,(G3DENUMMODESCALLBACK callback,void *context));

#ifndef __G3D_DEF_IMPORT
#undef IMPORT
#endif