#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>
#include "g3dlib.h"
#include "g3dmdl2.h"
#include "lzwc.h"

#define MAGIC "MDL2"
#define MAGICLEN 4
#define VER 0x200

#ifdef frm1
#undef frm1
#undef frm2
#undef frm3
#endif

static int animTicker;

int g3dmAnimTick2(int howmany)
  {
  int i=animTicker;
  animTicker+=howmany;
  return i;
  }

int *g3dmGetAnimTickPointer()
  {
  return &animTicker;
  }

static char g3dmLoadScnlEx(HG3DSCENELIST ls, HMATRIX mm, HTXITABLE txi)
  {
  int i;
  char res;

  for (i=0;i<ls->count;i++)
    if ((res=g3dLoadEx(ls->scenes[i],mm,txi))!=G3D_OK) return res;
  return G3D_OK;
  }

static char g3dmScnlDupl(HG3DSCENELIST ls, HG3DSCENELIST *nw)
  {
  *nw=g3dmAllocSceneList(ls->count);
  if (*nw==NULL) return G3D_NOMEMORY;
  memcpy(*nw,ls,sizeof(TG3DSCENELIST)+ls->count*sizeof(TSCENEDEF *));
  return G3D_OK;
  }

static void *scnl_vt[]=
  {
  NULL,
  g3dmDestroySceneList,
  g3dmLoadScnlEx,
  NULL,
  g3dmScnlDupl,
  g3dmSaveSceneList,
  g3dmLoadSceneList
  };

HG3DSCENELIST g3dmAllocSceneList(int scenes)
  {
  HG3DSCENELIST scn;
  int s;

  scn=malloc(s=sizeof(TSCENEDEF *)*scenes+sizeof(TG3DSCENELIST));
  if (scn==NULL) return NULL;
  memset(scn,0,s);
  scn->count=scenes;
  scn->vtptr=scnl_vt;
  return scn;
  }

int g3dmAddScene(HG3DSCENELIST *scnl,TSCENEDEF *def)
  {
  HG3DSCENELIST scn;
  int s,c;

  scn=*scnl;c=scn->count;
  s=sizeof(TSCENEDEF *)*(c+1)+sizeof(TG3DSCENELIST);
  scn=realloc(scn,s);
  if (scn==NULL) return G3D_NOMEMORY;
  if (def!=NULL)
    {
    def=g3dCompactScene(def);
    if (def==NULL) return G3D_NOMEMORY;
    }
  scn->count++;
  scn->scenes[c]=def;
  *scnl=scn;
  return c;
  }

char g3dmSetScene(HG3DSCENELIST scnl, int index, TSCENEDEF *def)
  {
  if (def!=NULL)
    {
    def=g3dCompactScene(def);
    if (def==NULL) return G3D_NOMEMORY;
    }
  if (scnl->scenes[index]!=NULL) g3dDestroy(scnl->scenes[index]);
  scnl->scenes[index]=def;
  return G3D_OK;
  }

char g3dmDestroySceneList(HG3DSCENELIST scnl)
  {
  int i;
  if (scnl==NULL) return G3D_OK;
  for (i=0;i<scnl->count;i++) g3dDestroy(scnl->scenes[i]);
  free(scnl);
  return G3D_OK;
  }

char g3dmSaveSceneList(HG3DSCENELIST scnl, FILE *f)
  {
  char res;
  int ires;
  char existflag;
  int i;
  for (i=0;i<scnl->count;i++)
    {
    existflag=scnl->scenes[i]!=NULL;
    ires=fwrite(&existflag,1,1,f);
    if (ires!=1) return G3DIO_DISKWRITEERROR;
    if (existflag)
      if ((res=g3dioSave(scnl->scenes[i],f))!=G3D_OK) return res;
    }
  return G3D_OK;
  }

char g3dmLoadSceneList(HG3DSCENELIST scnl, FILE *f)
  {
  TSCENEDEF def;
  int i;
  char res;
  int ires;
  char existflag;
  for (i=0;i<scnl->count;i++)
    {
    g3dCreateScene(&def);
    ires=fread(&existflag,1,1,f);if (ires!=1) return res;
    if (existflag)
      {
      res=g3dioLoad(&def,f);if (res!=G3D_OK) return res;
      res=g3dmSetScene(scnl,i,&def);
      }
    else
      res=g3dmSetScene(scnl,i,NULL);
    if (res!=G3D_OK) return res;
    }
  return G3D_OK;
  }
//----------------- frame list -----------------------

static char g3dmFrmlDupl(HG3DFRAMELIST ls, HG3DFRAMELIST *nw)
  {
  *nw=g3dmAllocFrameList(ls->count);
  if (*nw==NULL) return G3D_NOMEMORY;
  memcpy(*nw,ls,sizeof(TG3DFRAMELIST)+ls->count*sizeof(TG3DMFRAME *));
  return G3D_OK;
  }

static void *frml_vt[]=
  {
  NULL,
  free,
  NULL,
  NULL,
  g3dmFrmlDupl,
  g3dmSaveFrameList,
  g3dmLoadFrameList,
  };


HG3DFRAMELIST g3dmAllocFrameList(int frames)
  {
  HG3DFRAMELIST frml;
  int s;

  frml=malloc(s=sizeof(TG3DMFRAME)*frames+sizeof(TG3DFRAMELIST));
  if (frml==NULL) return NULL;
  memset(frml,0,s);
  frml->preallocated=frames;
  frml->count=0;
  frml->vtptr=frml_vt;
  return frml;
  }

int g3dmAddFrameToList(HG3DFRAMELIST frml, HG3DMFRAME frame)
  {
  int indx;

  indx=frml->count;
  if (indx>=frml->preallocated) return G3D_NOMEMORY;
  frml->frames[indx]=*frame;
  frml->count++;
  return indx;
  }

int g3dmPreallocFrames(HG3DFRAMELIST *frm, int count)
  {
  HG3DFRAMELIST frml=*frm;
  int s;

  s=sizeof(TG3DMFRAME)*(count+frml->count)+sizeof(TG3DFRAMELIST);
  frml=realloc(frml,s);
  if (frml==NULL) return G3D_NOMEMORY;
  frml->preallocated=count+frml->count;
  *frm=frml;
  return G3D_OK;
  }


static int frmcompare(const void *e1,const void *e2)
  {
  int a=((HG3DMFRAME)e1)->frmnum-((HG3DMFRAME)e2)->frmnum;
  return (a>0)-(a<0);
  }

int g3dmPrepareFrames(HG3DFRAMELIST *frm)
  {
  HG3DFRAMELIST frml=*frm;
  int s;

  s=sizeof(TG3DMFRAME)*frml->count+sizeof(TG3DFRAMELIST);
  frml=realloc(frml,s);
  if (frml==NULL) return G3D_NOMEMORY;
  qsort(frml->frames,frml->count,sizeof(TG3DMFRAME),frmcompare);
  *frm=frml;
  return G3D_OK;
  }

int g3dmSaveFrameList(HG3DFRAMELIST frm,FILE *f)
  {
  int res;
  void *pack;
  long size,psiz;
  pack=malloc((size=frm->count*sizeof(TG3DMFRAME))+16);
  if (pack==NULL) return G3D_NOMEMORY;
  memset(pack,0,size);
  init_lzw_compressor(8);
  psiz=lzw_encode((char *)frm->frames,pack,size);
  fwrite(&psiz,sizeof(psiz),1,f);
  if (psiz==-1) res=fwrite(frm->frames,size,1,f);
  else res=fwrite(pack,psiz,1,f);
  free(pack);
  if (res!=1) return G3DIO_DISKWRITEERROR;
  return G3D_OK;
  }

int g3dmLoadFrameList(HG3DFRAMELIST frm,FILE *f)
  {
  int res;
  int psiz;
  void *pack;
  int size=frm->count*sizeof(TG3DMFRAME);
  res=fread(&psiz,sizeof(psiz),1,f);
  if (res!=1) return G3DIO_DISKREADERROR;
  if (psiz==-1)
    res=fread(frm->frames,size,1,f);
  else
    {
    pack=malloc(size);
    init_lzw_compressor(8);
    res=fread(pack,psiz,1,f);
    if (res==1) psiz=lzw_decode(pack, (unsigned char *)frm->frames);
    if (psiz==-1) res=-1;
    free(pack);
    }
  if (res!=1) return G3DIO_DISKREADERROR;
  return G3D_OK;
  }

char g3dmFindFrame(HG3DFRAMELIST frml,int frm,HG3DMFRAME *frm1,HG3DMFRAME *frm2)
  {
  int down,up,pos,val;

  down=0;up=frml->count-1;
  if (frm>=frml->frames[up].frmnum)
    {
    *frm1=*frm2=&(frml->frames[up]);
    return G3D_OK;
    }
  if (down>up) return G3DM_FRAMENOTFOUND;
  while (down<=up)
    {
    pos=(down+up)>>1;
    val=frml->frames[pos].frmnum;
    if (val==frm)
      {
      *frm1=*frm2=&(frml->frames[pos]);
      return G3D_OK;
      }
    if (val>frm) up=pos-1;
    if (val<frm) down=pos+1;
    }
  *frm1=frml->frames+up;
  *frm2=frml->frames+down;
  return G3D_OK;
  }

//------------- models -------------------------

static void *model_vt[]=
  {
  NULL,
  g3dmDestroyMdl2,
  g3dmLoadModelEx,
  g3dmMove,
  g3dmDuplicModel,
  g3dmSaveModelDisk,
  g3dmLoadModelDisk
  };


char g3dmCreateEmptyModel(HG3DMODEL mdl)
  {
  HMODELDEF def;

  def=malloc(sizeof(TMODELDEF));
  if (def==NULL) return G3D_NOMEMORY;
  memset(def,0,sizeof(TMODELDEF));
  def->refcount=1;
  memset(mdl,0,sizeof(TG3DMODEL));
  mdl->modeldef=def;
  mdl->vtptr=model_vt;
  mdl->ticker=&animTicker;
  return G3D_OK;
  }

int g3dmAddObject(HG3DMODEL mdl,int parent)
  {
  HMODELDEF def=mdl->modeldef;
  HG3DMSEQUENCER sqn;
  int s,c;

  if (def==NULL) return G3D_INVALIDPARAMS;
  if (parent>=def->count) return G3D_INVALIDPARAMS;
  sqn=malloc(sizeof(TG3DMSEQUENCER));
  if (sqn==NULL) return G3D_NOMEMORY;
  memset(sqn,0,sizeof(TG3DMSEQUENCER));
  sqn->parent=parent;
  c=def->count;
  s=sizeof(TMODELDEF)+(c+1)*sizeof(HG3DMSEQUENCER);
  def=realloc(def,s);
  if (def==NULL) {free(sqn);return G3D_NOMEMORY;}
  def->objects[c]=sqn;
  def->count++;
  mdl->modeldef=def;
  mdl->curobject=c;
  return c;
  }

int g3dmSetCurObject(HG3DMODEL mdl,int object)
  {
  HMODELDEF def=mdl->modeldef;

  if (def==NULL) return G3D_INVALIDPARAMS;
  if (object>=def->count) return G3D_INVALIDPARAMS;
  mdl->curobject=object;
  return (def->objects[object])->parent;
  }

static int FindSequence(HG3DFRAMELIST *lst,int count,int num)
  {
  int down=0,up=count-1,pos=0;

  while (down<=up)
    {
    pos=(up+down)/2;
    if (lst[pos]->seqnumber==num) return pos;
    if (lst[pos]->seqnumber<num) down=pos+1;
    else if (lst[pos]->seqnumber>num) up=pos-1;
    }
  if (pos<=up && lst[pos]->seqnumber==num) return pos;
  return -1;
  }


int g3dmAddSequence(HG3DMODEL mdl,int seqnum, HG3DFRAMELIST frm)
  {
  HMODELDEF def=mdl->modeldef;
  HG3DFRAMELIST frml;
  HG3DMSEQUENCER sqc;
  int frmindex;

  if (def==NULL) return G3D_INVALIDPARAMS;
  sqc=def->objects[mdl->curobject];
   frm->seqnumber=seqnum;
  frmindex=FindSequence(sqc->seqs,sqc->count,seqnum);
  if (frmindex!=-1)
    {
    frml=sqc->seqs[frmindex];
    g3dDestroy(frml);
    sqc->seqs[frmindex]=frm;
    }
  else
    {
    int s,c;
    int i;
    c=sqc->count;
    s=sizeof(*sqc)+(c+1)*sizeof(HG3DFRAMELIST);
    sqc=realloc(sqc,s);
    if (sqc==NULL) return G3D_NOMEMORY;
    def->objects[mdl->curobject]=sqc;
    for (i=c;i>0;i--)
      if ((sqc->seqs[i-1])->seqnumber>seqnum) sqc->seqs[i]=sqc->seqs[i-1];
      else break;
    sqc->seqs[i]=frm;
    sqc->count++;
    }
  return G3D_OK;
  }

int g3dmSetCurSequence(HG3DMODEL mdl, int seqnum)
  {
  HMODELDEF def=mdl->modeldef;
  int frames=-1;
  int frmlast=0;
  int *indx,i;

  if (def==NULL) return G3D_INVALIDPARAMS;
  indx=mdl->indexes;
  if (indx==NULL) mdl->indexes=indx=malloc(sizeof(int)*def->count);
  if (indx==NULL) return G3D_NOMEMORY;
  for (i=0;i<def->count;i++)
    {
    HG3DMSEQUENCER sqc=def->objects[i];
    HG3DFRAMELIST lpfrm;
    int frm;

    if (frmlast>=sqc->count || (sqc->seqs[frmlast])->seqnumber!=seqnum)
      {
      frm=FindSequence(sqc->seqs,sqc->count,seqnum);
      if (frm!=-1)
        {
        frmlast=frm;
        lpfrm=sqc->seqs[frm];
        frames=max(frames,lpfrm->frames[lpfrm->count-1].frmnum);
        indx[i]=frm;
        }
      else indx[i]=-1;
      }
    else
      {
      lpfrm=sqc->seqs[frmlast];
      frames=max(frames,lpfrm->frames[lpfrm->count-1].frmnum);
      indx[i]=frmlast;
      }
    }
  if (frames==-1) return G3DM_SEQNOTFOUND;
  frmlast=mdl->cursequence;
  mdl->cursequence=seqnum;
  mdl->lastframe=frames*mdl->frame1000/1000;
  return G3D_OK;
  }


int g3dmSetCurSequenceEx(HG3DMODEL mdl, int seqnum, int dir, int flags)
  {
  int res;

  res=g3dmSetCurSequence(mdl,seqnum);
  if (res<0) return res;
  mdl->direction=dir;
  mdl->flags=flags;
  if (dir<0) g3dmSetCurFrame(mdl,mdl->lastframe);
  else g3dmSetCurFrame(mdl,0);
  return res;
  }

int g3dmSetCurFrame(HG3DMODEL mdl, int framenum)
  {
  int lf=mdl->curframe;
  if (mdl->flags & G3DMT_DONTLOOP)
    {
    if (framenum<0) framenum=0;
    if (framenum>mdl->lastframe) framenum=mdl->lastframe;
    }
  else
    {
    while (framenum<0) framenum+=mdl->lastframe+1;
    while (framenum>mdl->lastframe) framenum-=mdl->lastframe+1;
    }
  mdl->curframe=framenum;
  mdl->trueframe=framenum*1000/mdl->frame1000;
  return lf;
  }

int g3dmAttachSceneList(HG3DMODEL mdl, HG3DSCENELIST scnl, int object)
  {
  HMODELDEF def=mdl->modeldef;
  HG3DSCENELIST *attch;

  if (def==NULL) return G3D_INVALIDPARAMS;
  if (object==-1) attch=&def->scnlist;
  else
    if (object<def->count) attch=&(def->objects[object])->scnlist;
    else return G3D_INVALIDPARAMS;
  if (*attch!=NULL) g3dDestroy(*attch);
  *attch=scnl;
  return G3D_OK;
  }

int g3dmAttachTxi(HG3DMODEL mdl, HTXITABLE txi)
  {
  HMODELDEF def=mdl->modeldef;
  HTXITABLE *attch;

  if (def==NULL) return G3D_INVALIDPARAMS;
  attch=&mdl->txitable;
  if (*attch!=NULL) free(*attch);
  *attch=txi;
  return G3D_OK;
  }

int g3dmMove(HG3DMODEL mdl, int howmany)
  {
  return g3dmSetCurFrame(mdl,mdl->curframe+howmany);
  }

int g3dmDuplicModel(HG3DMODEL mdl, HG3DMODEL newmdl)
  {
  HTXITABLE txi;
  HMODELDEF def=mdl->modeldef;

  if (def==NULL) return G3D_INVALIDPARAMS;
  if (mdl->txitable!=NULL)
    {
    txi=g3dTxiAlloc(mdl->txitable->count);
    memcpy(txi->texturearr,mdl->txitable->texturearr,txi->count*sizeof(int));
    }
  else txi=NULL;
  *newmdl=*mdl;
  newmdl->txitable=txi;
  newmdl->indexes=NULL;
  def->refcount++;
  return G3D_OK;
  }

static void g3dDestroyObject(HG3DMSEQUENCER seq)
  {
  int i;
  for (i=0;i<seq->count;i++) g3dDestroy(seq->seqs[i]);
  if (seq->scnlist!=NULL) g3dDestroy(seq->scnlist);
  free(seq->name);
  free(seq);
  }

static void DestroyTotal(HMODELDEF def)
  {
  int i;
  for (i=0;i<def->count;i++) g3dDestroyObject(def->objects[i]);
  if (def->scnlist!=NULL) g3dDestroy(def->scnlist);
  free(def->name);
  free(def);
  }

int g3dmDestroyMdl2(HG3DMODEL mdl)
  {
  HMODELDEF def=mdl->modeldef;

  if (def==NULL) return G3D_INVALIDPARAMS;
  free(mdl->txitable);
  free(mdl->indexes);
  def->refcount--;
  if (def->refcount<1) DestroyTotal(def);
  return G3D_OK;
  }


static char Interpolations1(int curframe,HG3DMFRAME frm1,HG3DMFRAME frm2,HMATRIX mm,float *p)
  {
  float f,z;
  int i;

  if (frm1->flags & G3DMF_HIDEOBJECT || frm2->flags & G3DMF_HIDEOBJECT)
    {
    *p=0.0f;
    return G3DMF_HIDEOBJECT;
    }
  f=((float)curframe-(float)frm1->frmnum)/((float)frm2->frmnum-(float)frm1->frmnum);
  z=1-f;
  *p=f;
  if (frm1->flags & G3DMF_NOMATRIX && frm2->flags & G3DMF_NOMATRIX)
    {
    return G3DMF_NOMATRIX;
    }
  for (i=0;i<16;i++) mm[0][i]=frm1->mm[0][i]*z+frm2->mm[0][i]*f;
  return 0;
  }

static TSCENEDEF *GetScene(HMODELDEF def,HG3DMSEQUENCER seq, HG3DMFRAME frm,int index, int flag)
  {
  if (index==-1) return NULL;
  if (frm->flags & flag)
    return g3dmQueryScene(seq->scnlist,index);
  else
    return g3dmQueryScene(def->scnlist,index);
  }

typedef struct _DisplayParms
  {
  HMODELDEF def;
  HG3DMSEQUENCER seq;
  HTXITABLE txi;
  HG3DMFRAME frm1,frm2;
  float f;
  }TDISPPARM;

static void DisplayScene(TDISPPARM *parm, HMATRIX mm)
  {
  TSCENEDEF *def1,*def2;
  TSCENEDEF tmp;

  def1=GetScene(parm->def,parm->seq,parm->frm1,(parm->frm1)->points,G3DMF_LOCALSCENELISTP);
  def2=GetScene(parm->def,parm->seq,parm->frm1,(parm->frm1)->scene,G3DMF_LOCALSCENELIST);
  if (def2==NULL) return;
  if (def1!=NULL)
    {
    tmp=*def2;
    if (def1->wsize) tmp.wsize=0;
    if (def1->nsize) tmp.nsize=0;
    if (def1->cvsize) tmp.cvsize=0;
    if (def1->ssize) tmp.ssize=0;
    g3dLoadEx(def1,mm,parm->txi);
    g3dLoadEx(&tmp,mm,parm->txi);
    }
  else
    g3dLoadEx(def2,mm,parm->txi);
  }

int g3dmLoadModelEx(HG3DMODEL mdl, HMATRIX mm, HTXITABLE txi)
  {
  HMODELDEF def=mdl->modeldef;
  int i;
  float p;
  TDISPPARM parm;

  if (txi==NULL) txi=mdl->txitable;
  if (def==NULL) return G3D_INVALIDPARAMS;
  if (mdl->indexes==NULL) return G3DM_NOCURRENTFRAME;
  parm.def=def;
  parm.txi=txi;
  for (i=0;i<def->count;i++) if (mdl->indexes[i]!=-1)
    {
    HG3DMSEQUENCER seq=def->objects[i];
    HG3DFRAMELIST frml=seq->seqs[mdl->indexes[i]];
    HG3DMFRAME frm1,frm2;
    HG3DMSEQUENCER parent;
    HMATRIX vlastni;
    int res;

    parm.seq=seq;
    if (mdl->flags & G3DMT_AUTOANIMATE)
      {
      int t=mdl->ticker[0];
      g3dmMove(mdl,t-mdl->lastticker);
      mdl->lastticker=t;
      }
    g3dmFindFrame(frml,mdl->trueframe,&frm1,&frm2);
    if (seq->parent!=-1) parent=def->objects[seq->parent];else parent=NULL;
    if (frm1!=frm2)
      res=Interpolations1(mdl->trueframe,frm1,frm2,vlastni,&p);
    else
      p=0.0f;
    parm.frm1=frm1;
    parm.frm2=frm2;
    parm.f=p;
    if (p==0.0f)
      {
      if (frm1->flags & G3DMF_HIDEOBJECT) continue;
      if (frm1->flags & G3DMF_NOMATRIX)
        {
        if (parent==NULL || parent->nomatrix)
          {
          seq->nomatrix=1;
          DisplayScene(&parm,mm);
          }
        else
          {
          seq->nomatrix=0;
          DisplayScene(&parm,parent->tmp);
          }
        }
      else
        {
        seq->nomatrix=0;
        if (seq->parent==-1 || parent->nomatrix)
          if (mm!=NULL) SoucinMatic(frm1->mm,mm,seq->tmp);
          else CopyMatice(seq->tmp,frm1->mm);
        else SoucinMatic(frm1->mm,parent->tmp,seq->tmp);
        DisplayScene(&parm,seq->tmp);
        }
      continue;
      }
    if (res==G3DMF_HIDEOBJECT) continue;
    if (res==G3DMF_NOMATRIX)
      {
      if (parent==NULL || parent->nomatrix)
        {
        seq->nomatrix=1;
        DisplayScene(&parm,mm);
        }
      else
        {
        seq->nomatrix=0;
        DisplayScene(&parm,parent->tmp);
        }
      }
    else
      {
      seq->nomatrix=0;
      if (seq->parent==-1 || parent->nomatrix)
        if (mm!=NULL) SoucinMatic(vlastni,mm,seq->tmp);
        else CopyMatice(seq->tmp,vlastni);
      else SoucinMatic(vlastni,parent->tmp,seq->tmp);
      DisplayScene(&parm,seq->tmp);
      }
    }
  return 0;
  }


char g3dmSaveModelDisk(HG3DMODEL mdl, FILE *f)
  {
  int iores=0; //input/output result
  char res;  //G3D result
  HMODELDEF def=mdl->modeldef;
  HG3DSCENELIST scnlst;
  long zero=0;
  int i,j;

  if (def==NULL) return G3D_INVALIDPARAMS;
  iores=fwrite(MAGIC,MAGICLEN,1,f);
  if (iores!=1) return G3DIO_DISKWRITEERROR;
  j=VER;iores=fwrite(&j,2,1,f);
  if (iores!=1) return G3DIO_DISKWRITEERROR;
  if (def->name==NULL)
    iores=fwrite(&def->name,1,1,f);
  else
    iores=fwrite(def->name,strlen(def->name)+1,1,f);
  if (iores!=1) return G3DIO_DISKWRITEERROR;
  iores=fwrite(&def->count,sizeof(def->count),1,f);
  if (iores!=1) return G3DIO_DISKWRITEERROR;
    //First, save object count in model.
  //Now, we'll save all sequences for each object
  for (i=0;i<def->count;i++) //for each object do:
    {
    HG3DMSEQUENCER seq=def->objects[i];
    iores=0;

    iores+=fwrite(&seq->count,sizeof(seq->count),1,f);
     //saves count of sequences for object
    iores+=fwrite(&seq->parent,sizeof(seq->parent),1,f);
     //saves parent reference for object
    for (j=0;j<seq->count;j++) //for each sequence do:
      {
      HG3DFRAMELIST frml=seq->seqs[j];
      iores+=fwrite(&frml->count,sizeof(frml->count),1,f);
       //saves count of frames for sequence;
      iores+=fwrite(&frml->seqnumber,sizeof(frml->seqnumber),1,f);
       //saves sequence number
      res=g3dioSave(frml,f);
       //saves all frames;
      if (res!=G3D_OK) return res;
      }
    scnlst=seq->scnlist;
    if (scnlst==NULL) iores+=fwrite(&zero,sizeof(scnlst->count),1,f);
     //if scnlist is not used, save zero mark to notifi loader.
    else
      {
      iores+=fwrite(&scnlst->count,sizeof(scnlst->count),1,f);
      res=g3dioSave(scnlst,f);
      //saves scene-list
      if (res!=G3D_OK) return res;
      }
    if (seq->name==NULL) fwrite(&zero,1,1,f);
    else fwrite(seq->name,strlen(seq->name)+1,1,f);
    if (iores!=3+seq->count*2) return G3DIO_DISKWRITEERROR;
    }
  scnlst=def->scnlist;
  if (scnlst==NULL) iores=fwrite(&zero,sizeof(scnlst->count),1,f);
   //if scnlist is not used, save zero mark to notifi loader.
  else
    {
    iores=fwrite(&scnlst->count,sizeof(scnlst->count),1,f);
    res=g3dioSave(scnlst,f);
    if (res!=G3D_OK) return res;
    }
  if (iores!=1) return G3DIO_DISKWRITEERROR;
  return G3D_OK;
  }

/*format:
<object count>
  object
    <seq-count>
    <parent>
      sequence
         <number>
         <frame count>
          frame
           <packed size or -1>
           <lzw-framelist>
          frame
          frame
          ...
      sequence
      sequence
      ...
    <scenecount or 0>
      scenelist
       <exist flag>
        scene - if exists
       <exist flag>
        scene - if exists
       <exist flag>
        scene - if exists
       <exist flag>
        scene - if exists
  object
  object
  object
  ...
<scenecount or 0>
  scenelist
   ...
*/

char g3dmLoadModelDisk(HG3DMODEL mdl, FILE *f)
  {
  HMODELDEF def=mdl->modeldef;
  HG3DSCENELIST scnlst;
  int iores=0; //input/output result
  char res;  //G3D result
  int count=0;
  int size;
  int i,j;
  char c[256];

  if (def!=NULL)
    {
    g3dDestroy(mdl);
    }
  mdl->modeldef=NULL;
  //now, its sure, that prepared model is empty.
  iores=fread(c,MAGICLEN,1,f);
  if (iores!=1) return G3DIO_DISKREADERROR;
  if (strncmp(c,MAGIC,MAGICLEN)) return G3DIO_INVALIDFORMAT;
  j=0;iores=fread(&j,2,1,f);
  if (iores!=1) return G3DIO_DISKREADERROR;
  if (j<VER) return G3DIO_INVALIDFORMAT;
  j=0;
  fread(c+j,1,1,f);j++;
  while (c[j-1]!=0)
    if (fread(c+(j++),1,1,f)!=1) return G3DIO_DISKREADERROR;
  if (c[0])
    {
    def->name=malloc(strlen(c)+1);
    strcpy(def->name,c);
    }
  iores=fread(&count,sizeof(count),1,f);
  if (iores!=1) return G3DIO_DISKREADERROR;
  size=count*sizeof(HG3DMSEQUENCER)+sizeof(TMODELDEF);
  def=malloc(size);
  if (def==NULL) return G3D_NOMEMORY;
  memset(def,0,size);
  mdl->modeldef=def;
  def->refcount=1;
  for (i=0;i<count;i++)
    {
    HG3DMSEQUENCER seq;
    int count;

    iores=fread(&count,sizeof(count),1,f);
    if (iores!=1) return G3DIO_DISKREADERROR;
    size=sizeof(TG3DMSEQUENCER)+sizeof(HG3DFRAMELIST)*count;
    seq=malloc(size);
    if (seq==NULL) return G3D_NOMEMORY;
    memset(seq,0,size);
    def->objects[i]=seq;
    def->count++;
    iores=fread(&seq->parent,sizeof(seq->parent),1,f);
    if (iores!=1) return G3DIO_DISKREADERROR;
    for (j=0;j<count;j++)
      {
      HG3DFRAMELIST frml;
      int count;

      iores=fread(&count,sizeof(count),1,f);
      if (iores!=1) return G3DIO_DISKREADERROR;
      frml=g3dmAllocFrameList(count);
      if (frml==NULL) return G3D_NOMEMORY;
      seq->seqs[j]=frml;
      seq->count++;
      iores=fread(&frml->seqnumber,sizeof(frml->seqnumber),1,f);
      if (iores!=1) return G3DIO_DISKREADERROR;
      frml->count=count;
      res=g3dioLoad(frml,f);
      if (res!=G3D_OK) return res;
      }
    iores=fread(&count,sizeof(count),1,f);
    if (iores!=1) return G3DIO_DISKREADERROR;
    if (count!=0)
      {
      scnlst=g3dmAllocSceneList(count);
      if (scnlst==NULL) return G3DIO_NOMEMORY;
      seq->scnlist=scnlst;
      g3dioLoad(scnlst,f);
      }
    j=0;
    fread(c+j,1,1,f);j++;
    while (c[j-1]!=0)
      if (fread(c+(j++),1,1,f)!=1) return G3DIO_DISKREADERROR;
    if (c[0])
      {
      seq->name=malloc(strlen(c)+1);
      strcpy(seq->name,c);
      }
    }
  iores=fread(&count,sizeof(count),1,f);
  if (iores!=1) return G3DIO_DISKREADERROR;
  if (count!=0)
    {
    scnlst=g3dmAllocSceneList(count);
    if (scnlst==NULL) return G3DIO_NOMEMORY;
    def->scnlist=scnlst;
    g3dioLoad(scnlst,f);
    }
  return G3D_OK;
  }

char g3dmSaveModelDiskEx(HG3DMODEL mdl, char *filename)
  {
  FILE *f;
  char res;

  f=fopen(filename,"wb");
  if (f==NULL) return G3DIO_FILEOPENERROR;
  res=g3dmSaveModelDisk(mdl,f);
  fclose(f);
  return res;
  }

char g3dmLoadModelDiskEx(HG3DMODEL mdl, char *filename)
  {
  FILE *f;
  char res;

  f=fopen(filename,"rb");
  if (f==NULL) return G3DIO_FILENOTFOUND;
  res=g3dmLoadModelDisk(mdl,f);
  fclose(f);
  return res;
  }

int g3dmEnumSequences(HG3DMODEL mdl, G3DMSEQCALLBACK callback, void *user)
  {
  HMODELDEF def=mdl->modeldef;
  HG3DMSEQUENCER sq;
  int i;

  if (def==NULL) return G3D_INVALIDPARAMS;
  if (mdl->curobject<0 || mdl->curobject>=def->count) return G3D_INVALIDPARAMS;
  sq=def->objects[mdl->curobject];
  for (i=0;i<sq->count;i++)
    if (callback(mdl,sq->seqs[i],user)==0) return G3DM_ENUMSTOPPED;
  return G3D_OK;
  }

HG3DFRAMELIST g3dmGetSequence(HG3DMODEL mdl)
  {
  HMODELDEF def=mdl->modeldef;
  HG3DMSEQUENCER sq;

  if (def==NULL) return NULL;
  if (mdl->curobject<0 || mdl->curobject>=def->count) return NULL;
  if (mdl->indexes==NULL) return NULL;
  sq=def->objects[mdl->curobject];
  return sq->seqs[mdl->indexes[mdl->curobject]];
  }

int g3dmDeleteSequence(HG3DMODEL mdl)
  {
  HMODELDEF def=mdl->modeldef;
  HG3DMSEQUENCER sq;
  int i;

  if (def==NULL) return G3D_INVALIDPARAMS;
  if (mdl->curobject<0 || mdl->curobject>=def->count) return G3D_INVALIDPARAMS;
  if (mdl->indexes==NULL) return G3DM_SEQNOTFOUND;
  sq=def->objects[mdl->curobject];
  i=mdl->indexes[mdl->curobject];
  g3dDestroy(sq->seqs[i]);
  for (i=i+1;i<sq->count;i++) sq->seqs[i-1]=sq->seqs[i];
  sq->count--;
  return G3D_OK;
  }

HG3DSCENELIST g3dmGetSceneList(HG3DMODEL mdl,int object)
  {
  HMODELDEF def=mdl->modeldef;
  HG3DMSEQUENCER sq;

  if (def==NULL) return NULL;
  if (object<-1 || object>=def->count) return NULL;
  if (object==-1) return def->scnlist;
  sq=def->objects[object];
  return sq->scnlist;
  }


static char copycallback(HG3DMODEL mdl, HG3DFRAMELIST frml,void *user)
  {
  HG3DMODEL newmdl=user;
  HG3DFRAMELIST frmn;

  if (g3dDuplicate(frml,&frmn)!=G3D_OK) return 0;
  if (g3dmAddSequence(newmdl,frmn->seqnumber,frmn)!=G3D_OK)
    {
    g3dDestroy(frml);
    return 0;
    }
  return 1;
  }

int g3dmCopyObject(HG3DMODEL mdl, HG3DMODEL newmdl, int newparent)
  {
  HMODELDEF def=mdl->modeldef;
  HG3DSCENELIST scnlst,nsl;
  int i,j;

  if (def==NULL) return G3D_INVALIDPARAMS;
  if (mdl->curobject<0 || mdl->curobject>=def->count) return G3D_INVALIDPARAMS;
  j=g3dmAddObject(newmdl,newparent);
  g3dmSetObjectName(newmdl,g3dmGetObjectName(mdl));
  i=g3dmEnumSequences(mdl,copycallback,newmdl);
  if (i==G3DM_ENUMSTOPPED) return G3D_NOMEMORY;
  scnlst=g3dmGetSceneList(mdl,mdl->curobject);
  if (scnlst==NULL) g3dmAttachSceneList(newmdl,scnlst,j);
  else
    {
    if (g3dDuplicate(scnlst,&nsl)!=G3D_OK) return G3D_NOMEMORY;
    g3dmAttachSceneList(newmdl,scnlst,j);
    }
  return G3D_OK;
  }

int g3dmFindObject(HG3DMODEL mdl, int parent, int from)
  {
  HMODELDEF def=mdl->modeldef;
  int i;

  if (def==NULL) return G3D_INVALIDPARAMS;
  if (from<-1 || from>=def->count) return G3D_INVALIDPARAMS;
  for (i=from+1;i<def->count;i++)
    if ((def->objects[i])->parent==parent) return i;
  return G3DM_NOTFOUND;
  }

int g3dmSetObjectName(HG3DMODEL mdl, char *name)
  {
  HMODELDEF def=mdl->modeldef;
  HG3DMSEQUENCER sqc;
  char *c;
  int size;

  if (def==NULL) return G3D_INVALIDPARAMS;
  if (mdl->curobject<0 || mdl->curobject>=def->count) return G3D_INVALIDPARAMS;
  sqc=def->objects[mdl->curobject];
  if (name!=NULL)
    {
    size=strlen(name);
    if (size>255) return G3D_INVALIDPARAMS;
    c=malloc(size+1);
    if (c==NULL) return G3D_NOMEMORY;
    strcpy(c,name);
    }
  else c=NULL;
  if (sqc->name!=NULL) free(sqc->name);
  sqc->name=c;
  return G3D_OK;
  }

char *g3dmGetObjectName(HG3DMODEL mdl)
  {
  HMODELDEF def=mdl->modeldef;
  HG3DMSEQUENCER sqc;

  if (def==NULL) return NULL;
  if (mdl->curobject<0 || mdl->curobject>=def->count) return NULL;
  sqc=def->objects[mdl->curobject];
  return sqc->name;
  }

int g3dmFindObjectByName(HG3DMODEL mdl, char *name, int from)
  {
  HMODELDEF def=mdl->modeldef;
  HG3DMSEQUENCER sqc;
  int i;

  if (def==NULL) return G3D_INVALIDPARAMS;
  if (name==NULL) return G3D_INVALIDPARAMS;
  if (from<0 || from>=def->count) return G3D_INVALIDPARAMS;
  for (i=from;i<def->count;i++)
    {
    sqc=def->objects[i];
    if (sqc->name!=NULL && strcmp(sqc->name,name)==0) return i;
    }
  return G3DM_NOTFOUND;
  }

int g3dmGetObjectParent(HG3DMODEL mdl, int object)
  {
  HMODELDEF def=mdl->modeldef;
  HG3DMSEQUENCER sqc;

  if (def==NULL) return G3D_INVALIDPARAMS;
  if (object<0 || object>=def->count) return G3D_INVALIDPARAMS;
  sqc=def->objects[object];
  return sqc->parent;
  }

int g3dmGetObjectCount(HG3DMODEL mdl)
  {
  HMODELDEF def=mdl->modeldef;

  if (def==NULL) return G3D_INVALIDPARAMS;
  return def->count;
  }

int g3dmSetModelName(HG3DMODEL mdl, char *name)
  {
  HMODELDEF def=mdl->modeldef;
  char *c;
  int size;

  if (def==NULL) return G3D_INVALIDPARAMS;
  if (name!=NULL)
    {
    size=strlen(name);
    if (size>255) return G3D_INVALIDPARAMS;
    c=malloc(size+1);
    strcpy(c,name);
    }
  else
    c=NULL;
  if (def->name!=NULL) free(def->name);
  def->name=c;
  return G3D_OK;
  }

char *g3dmGetModelName(HG3DMODEL mdl)
  {
  HMODELDEF def=mdl->modeldef;

  if (def==NULL) return NULL;
  return def->name;
  }

