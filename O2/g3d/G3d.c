#define __MSC__
#include <assert.h>
#include <io.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <malloc.h>
#include <math.h>
#include "lzwc.h"
#include "matrix.h"
#include "g3dlib.h"
#include "dbgwnd.h"


#define TABLE_EXPAND 256

typedef struct _litvertex
  {
  float r,g,b;
  unsigned long cashed;
  }LITVERTEX;

#define CLP_LEFT 0x1
#define CLP_RIGHT 0x2
#define CLP_TOP 0x4
#define CLP_BOTTOM 0x8
#define CLP_NOTRANSFORM 0xE
#define CLP_ALL 0xF

static unsigned long lcashcounter=0;
static G3D_STATISTICS draw_stat;

static TWORLDVERTEX *wvtable=NULL;
static TNORMALVERTEX *nvtable=NULL;
static TVERTEXCOLOR *cvtable=NULL;
static TSCENEVERTEX *sctable=NULL;
static LITVERTEX *lttable=NULL;
static TFACEDRAWORDER *fctable=NULL;
static TFACEDRAWORDER **order1=NULL;
static TFACEDRAWORDER **order2=NULL;
static TFACEDRAWORDER **strlights=NULL;

static unsigned char *posclip=NULL;
static TWORLDVERTEX *transfv=NULL;

static short lenscount;
static int framecounter=0;

static int wvsize=0;
static int nvsize=0;
static int cvsize=0;
static int scsize=0;
static int ltsize=0;
static int fcsize=0;
static int slsize=0;

static int wvpos=0;
static int nvpos=0;
static int cvpos=0;
static int scpos=0;
static int slpos=0;
static int fcpos=0;
static int ordsize=0;
static int ltpos=0;

static int transfvsize=0;

static float ambient_light=0.0f;

static float sinetab[256];
static char sinetabok=0;

static HMATRIX world_t;
//static HMATRIX view_t;
//static HMATRIX proj_t;
static HMATRIX main_t;
static HMATRIX viewport_t;
static HMATRIX light_t;
static HMATRIX scene_t;
static HMATRIX projvp_t;
static HMATRIX viewsc_t;

static TRAYTRACECALLBACK callback=NULL;

static char world_transform_enabled;

static TLIGHTDEF *lights=NULL;
static TLIGHTDEF *actlits=NULL;
static int maxlights=0;
static int curlights=0;
static int lttsize=0;

//static char fast_lighting;

static float half_height;
static float half_width;

static long class1[256];
static long class2[256];

static char clipenabled=0;;
static HVECTOR clipmin;
static HVECTOR clipmax;

static char g3dVertexFormat=0;
static int animcnt;

static void *scenevt[]=
  {
  NULL,
  g3dDestroyScene,
  g3dLoadObjectEx2,
  NULL,
  g3dDuplScene,
  g3dioSaveObject,
  g3dioLoadObject,
  };

static void *scenecompactvt[]=
  {
  NULL,
  free,
  g3dLoadObjectEx2,
  NULL,
  g3dDuplScene,
  g3dioSaveObject,
  g3dioLoadObject,
  };

char g3dDuplParticles(void *from,void *to);
int g3dMoveParticlesEx2(TPARTICLES *from, int howmany)
  {
  int i,sum=0;
  for (i=0;i<howmany;i++) sum+=g3dMoveParticles(from);
  return sum;
  }

static void *particlevt[]=
  {
  NULL,
  free,
  g3dLoadParticles,
  g3dMoveParticlesEx2,
  g3dDuplParticles,
  NULL,
  NULL
  };

static void build_sine_tab()
  {
  int i;float f;

  for (i=0;i<256;i++)
    {
    f=(float)sin((float)i*3.14159265f/512.0f);
    sinetab[i]=f;
    }
  sinetabok=1;
  }

static float G3D_FASTCALL GetSine(int cnt,float freq,float ofs)
  {
  freq*=cnt;
  cnt=(int)(fmod(freq+ofs,1.0f)*1024);
  if (cnt>511)
    {
    cnt=1023-cnt;
    if (cnt>255) return -sinetab[511-cnt]; else return -sinetab[cnt];
    }
  else
    {
    if (cnt>255) return sinetab[511-cnt]; else return sinetab[cnt];
    }
  }

void g3dSetTraceRayProc(TRAYTRACECALLBACK proc)
  {
  callback=proc;
  }

void g3dResetTables()
	{
	free(wvtable);
	free(nvtable);
	free(cvtable);
	free(sctable);
    free(lttable);
	free(fctable);
    free(posclip);
    free(transfv);
    free(strlights);
    free(actlits);
    wvsize=0;
 	nvsize=0;
	cvsize=0;
	scsize=0;
    ltsize=0;
	fcsize=0;
    slsize=0;
	transfvsize=0;
	wvpos=0;
	nvpos=0;
	cvpos=0;
	scpos=0;
    ltpos=0;
	fcpos=0;
    slpos=0;
    lttsize=0;
	wvtable=NULL;
	nvtable=NULL;
	cvtable=NULL;
	sctable=NULL;
	lttable=NULL;
	fctable=NULL;
	posclip=NULL;
	transfv=NULL;
	actlits=NULL;
	strlights=NULL;
	clipenabled=0;
	}

char G3D_FASTCALL g3dExpandTable(void **table, int *count, int polozka)
	{
	void *p;
	int nwc=*count+TABLE_EXPAND;

	p=realloc(*table,nwc*polozka);
	if (p==NULL) return 1;
	*table=p;
	*count=nwc;
	return 0;
	}



char g3dResizeLightTable(int lc)
	{
	void *p;

  if (lights==NULL)
    {
    JednotkovaMatice(light_t);
    JednotkovaMatice(scene_t);
    }
	p=realloc(lights,lc*sizeof(TLIGHTDEF));
	if (p==NULL) return 1;
	lights=p;
  if (lc>maxlights) memset(lights+maxlights,0,sizeof(TLIGHTDEF)*(lc-maxlights));
	maxlights=lc;
//  free(actlits);
//  actlits=malloc(lc*sizeof(TLIGHTDEF));
  return 0;
	}


static char g3dCalcLights()
  {
  TLIGHTDEF *ld,*sd;
  int i;

  if (!sinetabok) build_sine_tab();
  i=maxlights+slpos;
  if (i>lttsize)
    {
    ld=realloc(actlits,sizeof(TLIGHTDEF)*i);
    if (ld!=NULL) actlits=ld,lttsize=i;
    else return G3D_NOMEMORY;
    }
  ld=lights;sd=actlits;
  curlights=0;
  for (i=0;i<maxlights;i++,ld++) if (ld->light_flags & LGH_ACTIVE)
    {
    *sd=*ld;
    TransformVector(light_t,ld->position,sd->position);
    TransformVectorNoTranslate(light_t,ld->direction,sd->direction);
    CorrectVector(sd->position);
    CorrectVector(sd->direction);
    if (sd->light_flags & LGH_SINEANIM)
      {
      sd->col_r+=sd->ampl_r*GetSine(framecounter,sd->freq,sd->offs_r);
      sd->col_g+=sd->ampl_g*GetSine(framecounter,sd->freq,sd->offs_g);
      sd->col_b+=sd->ampl_b*GetSine(framecounter,sd->freq,sd->offs_b);
      }
    sd++;
    curlights++;
    }
  for (i=0;i<slpos;i++)
    {
    TFACEDRAWORDER *fc;
    TVERTEXCOLOR *vx;
    int vert;
    fc=strlights[i];
    fc++;
    vx=cvtable+fc->dwVertexColor;
    if (vx->light_flags & LGH_ACTIVE && vx->flags & TVCOL_LIGHT)
      {
      vert=fc->dwVertex;
      memcpy(sd->position,wvtable[sctable[vert].wvindex].vector,sizeof(HVECTOR));
      memcpy(sd->direction,nvtable[sctable[vert].nvindex].vector,sizeof(HVECTOR));
      memcpy(&sd->col_r,&vx->emit_r,10*sizeof(float));
      sd->light_flags=vx->light_flags;
      sd->range=vx->range;
      sd->maxradius=vx->maxradius;
      sd->minradius=vx->minradius;
    if (sd->light_flags & LGH_SINEANIM)
      {
      sd->col_r+=vx->ampl_r*GetSine(framecounter,vx->freq,vx->offs_r);
      sd->col_g+=vx->ampl_g*GetSine(framecounter,vx->freq,vx->offs_g);
      sd->col_b+=vx->ampl_b*GetSine(framecounter,vx->freq,vx->offs_b);
      }
      sd++;
      curlights++;
      }
    }
  return 0;
  }

char g3dSetLightTransformation(HMATRIX m,char mode)
  {
  HMATRIX prac;
  switch (mode)
    {
    case G3D_CLEARLT:JednotkovaMatice(light_t);
                     JednotkovaMatice(scene_t);
                     break;
    case G3D_LIGHTLT:memcpy(light_t,m,sizeof(HMATRIX));
                     JednotkovaMatice(scene_t);
                     break;
    case G3D_SCENELT:memcpy(prac,m,sizeof(HMATRIX));
                     memcpy(scene_t,prac,sizeof(HMATRIX));
                     InverzeMatice(prac,light_t);
                     break;
    default: return G3D_INVALIDPARAMS;
    }
  return G3D_OK;
  }

void g3dBeginScene(TSCENEDEF *load)
	{
	if (load!=NULL)
		{
		free(wvtable);
		free(nvtable);
		free(sctable);
		free(cvtable);
		free(fctable);
		wvtable=load->wtable;
		nvtable=load->ntable;
		sctable=load->stable;
		cvtable=load->cvtable;
		fctable=load->fctable;
		wvpos=wvsize=load->wsize;
		nvpos=nvsize=load->nsize;
		scpos=scsize=load->ssize;
		cvpos=cvsize=load->cvsize;
		fcpos=fcsize=load->fcsize;
		}
	else
		{
		wvpos=0;
		nvpos=0;
		cvpos=0;
		scpos=0;
    fcpos=0;
    }
  slpos=0;
  }

void G3D_FASTCALL g3dBeginObject(HMATRIX wtrns)
		{
  if (wtrns==NULL) world_transform_enabled=0;
  else
    {
    memcpy(world_t,wtrns,sizeof(HMATRIX));
    world_transform_enabled=1;
    }
	}

static char g3dAddStreamLight(TFACEDRAWORDER *fc)
  {
  if (slpos>=slsize)
    if (g3dExpandTable((void **)&strlights,&slsize,sizeof(TFACEDRAWORDER *))) return -1;
  strlights[slpos++]=fc;
  return 0;
  }

int G3D_FASTCALL g3dAddPosition(HVECTOR v)
	{
  if (wvpos>=wvsize)
		if (g3dExpandTable((void **)&wvtable,&wvsize,sizeof(TWORLDVERTEX))) return -1;
	if (world_transform_enabled)
		TransformVector(world_t,v,wvtable[wvpos].vector);
	else
		memcpy(wvtable[wvpos].vector,v,sizeof(HVECTOR));
	return wvpos++;
	}

int G3D_FASTCALL g3dAddTPosition(HVECTOR v, HVECTOR vt)
  {
  if (wvpos>=wvsize)
	if (g3dExpandTable((void **)&wvtable,&wvsize,sizeof(TWORLDVERTEX))) return -1;	
  if (wvpos>=transfvsize)
	{
	void *pp;
	pp=realloc(transfv,wvsize*sizeof(TWORLDVERTEX));
	if (pp==NULL) return -1;
	transfv=pp;
	pp=realloc(posclip,(wvsize>>1)+2);
	if (pp==NULL) return -1;
	posclip=pp;
	transfvsize=wvsize;
	}
  memcpy(wvtable[wvpos].vector,v,sizeof(HVECTOR));
  memcpy(transfv[wvpos].vector,vt,sizeof(HVECTOR));
  if (wvpos & 1) posclip[wvpos>>1]=CLP_NOTRANSFORM<<4;else posclip[wvpos>>1]=CLP_NOTRANSFORM;
  return wvpos++;
  }

int G3D_FASTCALL g3dAddNormal(HVECTOR v)
	{
	if (nvpos>=nvsize)
		if (g3dExpandTable((void **)&nvtable,&nvsize,sizeof(TNORMALVERTEX))) return -1;
  if (world_transform_enabled)
    {
		TransformVectorNoTranslate(world_t,v,nvtable[nvpos].vector);
    NormalizeVector(nvtable[nvpos].vector);
    }
	else
		memcpy(nvtable[nvpos].vector,v,sizeof(HVECTOR));
	return nvpos++;
	}

int G3D_FASTCALL g3dAddColor(TVERTEXCOLOR *tdef)
	{
	if (cvpos>=cvsize)
		if (g3dExpandTable((void **)&cvtable,&cvsize,sizeof(TVERTEXCOLOR))) return -1;
	memcpy(&cvtable[cvpos],tdef,sizeof(TVERTEXCOLOR));
	return cvpos++;
	}

int G3D_FASTCALL g3dAddFace(int vertices,int flags,long handle)
	{
  TFACEDRAWORDER *fcs;
	if (fcpos>=fcsize)
		if (g3dExpandTable((void **)&fctable,&fcsize,sizeof(TFACEDRAWORDER))) return -1;
  fcs=fctable+fcpos;
  fcs->wFlags=flags;
  fcs->vcount=vertices;
  fcs->texture=handle;
  if ((flags & 0xF)==G3D_STREAMLIGHT) g3dAddStreamLight(fcs);
	return fcpos++;
	}

int G3D_FASTCALL g3dAddFaceVertex(int vertex,int color,float tu, float tv)
	{
  TFACEDRAWORDER *fcs;
  if (fcpos>=fcsize)
		if (g3dExpandTable((void **)&fctable,&fcsize,sizeof(TFACEDRAWORDER))) return -1;
  fcs=fctable+fcpos;
  fcs->dwVertex=vertex;
  fcs->tu=tu;
  fcs->tv=tv;
  fcs->dwVertexColor=color;
	return fcpos++;
	}

int G3D_FASTCALL g3dAddVertex(int posindex,int normalindex)
	{
	if (scpos>=scsize)
		if (g3dExpandTable((void **)&sctable,&scsize,sizeof(TSCENEVERTEX))) return -1;
	assert(posindex<wvpos);
    sctable[scpos].nvindex=normalindex;
	sctable[scpos].wvindex=posindex;
	return scpos++;
	}

char G3D_FASTCALL g3dSetLight(int index, TLIGHTDEF *ldef)
	{
	if (index>maxlights) return 1;
	lights[index]=*ldef;
	return 0;
	}

char  g3dGetLight(int index, TLIGHTDEF *def)
	{
	if (index>maxlights) return 1;
	*def=lights[index];
	return 0;
	}

TLIGHTDEF *g3dGetLightPtr(int index)
	{
	if (index>maxlights) return NULL;
	return lights+index;
	}

static void G3D_FASTCALL g3dLitVertex(TSCENEVERTEX *sc, LITVERTEX *outvert, TWORLDVERTEX *transformed)
	{
  float cr=0.0f,cg=0.0f,cb=0.0f;
	float intensity;
  float r2,r,cosm;
  HVECTOR relpos;
	TWORLDVERTEX *wv=&wvtable[sc->wvindex];
	TNORMALVERTEX *nv=&nvtable[sc->nvindex];
	int i;
  register char rcalc=0;
	TLIGHTDEF *lg;

  transformed+=sc->wvindex;
  for (i=0,lg=actlits;i<curlights;i++,lg++) if (lg->light_flags & LGH_ACTIVE)
    {
    char shadowmode=LRT_LIGHT;
    if (lg->light_flags & (LGH_POINTLIGHT | LGH_SPOTLIGHT))
      {
      float mr=lg->range;
      relpos[XVAL]=lg->position[XVAL]-wv->vector[XVAL];
      relpos[YVAL]=lg->position[YVAL]-wv->vector[YVAL];
      relpos[ZVAL]=lg->position[ZVAL]-wv->vector[ZVAL];
      if (fabs(relpos[XVAL])<mr && fabs(relpos[YVAL])<mr && fabs(relpos[ZVAL])<mr)
        {
        r2=ModulVektoru2(relpos);
        if (mr*mr>r2)
          {
          rcalc=1;
          if (lg->light_flags & LGH_SPOTLIGHT)
            {
            cosm=-Scalar1(relpos,lg->direction);
            r=1.0f/(float)(sqrt(r2));rcalc=0;
            cosm*=r;
            if (cosm<(lg->minradius)) continue;
            if (cosm>(lg->maxradius)) intensity=1.0f;
            else intensity=(cosm-lg->minradius)/(lg->maxradius-lg->minradius);
            }
          else intensity=1.0f;
          if (sc->nvindex==-1) {r=1.0f/(cosm=(float)sqrt(r2));rcalc=0;}
          else cosm=Scalar1(relpos,nv->vector);
          if (cosm>=0)
            {
            if (callback!=NULL) shadowmode=callback(LRTM_FROMTO,wv->vector,lg->position);
            if (shadowmode==LRT_SHADOW) continue;
            if (rcalc) r=1.0f/(float)(sqrt(r2));
            intensity*=cosm*r-cosm/mr;
            if (shadowmode==LRT_HALFSHADOW) intensity/=2.0f;
            }
          else intensity=0.0f;
          }
        else intensity=0.0f;
        }
      else intensity=0.0f;
      }
		else if (lg->light_flags & LGH_DIRECTLIGHT)
			{
      if (sc->nvindex==-1) cosm=1.0f;else  cosm=-CosMezi(lg->direction,nv->vector);
      if (cosm>0)
        {
        if (callback!=NULL) shadowmode=callback(LRTM_FROMDIR,wv->vector,lg->direction);
        if (shadowmode==LRT_SHADOW) continue;
        intensity=cosm;
        }
      else intensity=0.0f;
      }
    if (intensity!=0.0f)
      {
      cr+=intensity*lg->col_r;
      cg+=intensity*lg->col_g;
      cb+=intensity*lg->col_b;
      }
    }
  cr+=ambient_light;
  cg+=ambient_light;
  cb+=ambient_light;
  outvert->r=cr;
  outvert->g=cg;
  outvert->b=cb;
  outvert->cashed=lcashcounter;
  }

static TWORLDVERTEX *G3D_FASTCALL g3dTransformVertices(int index, TWORLDVERTEX *wv, int count)
  {
  TWORLDVERTEX *wt,*wx;
  int i;
  float q;
  float xmin,xmax,ymin,ymax,zmin,zmax;
  unsigned char *clipped;

  i=(index+count);
  if (transfvsize<i)
	{
	wx=wt=realloc(transfv,sizeof(TWORLDVERTEX)*i);
	if (wt==NULL) return NULL;
	transfv=wx;
    clipped=realloc(posclip,(i+2)>>1);
    if (clipped==NULL) return NULL;
    posclip=clipped;
	transfvsize=i;
	}
  else
	{
	wx=wt=transfv;
	clipped=posclip;
	}
  xmin=(float)g3dQuerySettingInt(G3DQ_CLIPLEFT);
  xmax=(float)g3dQuerySettingInt(G3DQ_CLIPRIGHT);
  ymin=(float)g3dQuerySettingInt(G3DQ_CLIPTOP);
  ymax=(float)g3dQuerySettingInt(G3DQ_CLIPBOTTOM);
  zmin=(float)g3dQuerySettingInt(G3DQ_ZMIN);
  zmax=(float)g3dQuerySettingInt(G3DQ_ZMAX);
  wx+=index;
  for (i=0;i<count;i++,wx++,wv++)
	{
	register unsigned char *curclip=clipped+(i>>1);
	register unsigned char mask=0xf<<((i & 1)<<2);
    register unsigned char clipstat=*curclip & mask;	
	if (clipstat!=((CLP_NOTRANSFORM | (CLP_NOTRANSFORM<<4)) & mask)) 
	  {
	  TransformVector(main_t,wv->vector,wx->vector);
	  q=1/wx->vector[WVAL];
	  wx->vector[XVAL]*=q;
	  wx->vector[YVAL]*=q;
	  wx->vector[ZVAL]*=q;
	  wx->vector[WVAL]=q;
	  }
	clipstat=0;
    if (wx->vector[ZVAL]<zmin || wx->vector[ZVAL]>zmax) clipstat=CLP_ALL;
    else
      {
      if (wx->vector[XVAL]<xmin) clipstat|=CLP_LEFT | (CLP_LEFT<<4);
      else if (wx->vector[XVAL]>xmax) clipstat|=CLP_RIGHT | (CLP_RIGHT<<4);
      if (wx->vector[YVAL]<ymin) clipstat|=CLP_TOP | (CLP_TOP<<4);
      else if (wx->vector[YVAL]>ymax) clipstat|=CLP_BOTTOM | (CLP_BOTTOM<<4);
      }
	*curclip&=~mask;
	*curclip|=mask & clipstat;
    }
	return wt;
	}


void G3D_FASTCALL g3dEndScene(TSCENEDEF *tostore)
	{
  if (tostore!=NULL)
		{
    long s;
    tostore->wtable=malloc(s=wvpos*sizeof(*wvtable));
    memcpy(tostore->wtable,wvtable,s);
    tostore->ntable=malloc(s=nvpos*sizeof(*nvtable));
    memcpy(tostore->ntable,nvtable,s);
    tostore->stable=malloc(s=scpos*sizeof(*sctable));
    memcpy(tostore->stable,sctable,s);
    tostore->cvtable=malloc(s=cvpos*sizeof(*cvtable));
    memcpy(tostore->cvtable,cvtable,s);
    tostore->fctable=malloc(s=fcpos*sizeof(*fctable));
    memcpy(tostore->fctable,fctable,s);
		tostore->wsize=wvpos;
		tostore->nsize=nvpos;
		tostore->ssize=scpos;
		tostore->cvsize=cvpos;
    tostore->fcsize=fcpos;
    tostore->vtptr=scenevt;
    }
	}

void G3D_FASTCALL g3dDestroyScene(TSCENEDEF *todestroy)
	{
  assert(todestroy->wtable!=wvtable);  //Neni mozne likvidovat scenu, kdyz je prave kreslena.
	free(todestroy->wtable);
	free(todestroy->ntable);
	free(todestroy->stable);
	free(todestroy->cvtable);
  free(todestroy->fctable);
  todestroy->nsize=0;
	todestroy->wsize=0;
	todestroy->ssize=0;
	todestroy->cvsize=0;
  todestroy->fcsize=0;
  }

#define NODRAW 1
#define OK 0
#define ISERROR -1

#define MAXVRTS 128
#define MAXLENS 128

typedef struct _lensinfo
  {
  G3D_TLVERTEX vt;
  int texture;
  char flags;
  }LENSINFO;

static G3D_TLVERTEX face[MAXVRTS];
static LENSINFO lensflare[MAXLENS];

static void G3D_FASTCALL g3dCorrectBillboard(G3D_TLVERTEX *face,TWORLDVERTEX *wv,HMATRIX view_t, HMATRIX proj_t)
  {
  TWORLDVERTEX vc,vc2;
  float w;
  vc=*wv;
  TransformVector(view_t,wv->vector,vc.vector);
  w=vc.vector[WVAL];
  if (w!=1.0) vc.vector[ZVAL]/=w;
  vc.vector[XVAL]=face->tu;
  vc.vector[YVAL]=face->tv;
  vc.vector[WVAL]=0.0f;
  TransformVector(proj_t,vc.vector,vc2.vector);
  CorrectVector(vc2.vector);
  face->tu=vc2.vector[XVAL]*half_width;
  face->tv=vc2.vector[YVAL]*half_height;
  }

static char G3D_FASTCALL AddVertexToPrim(int index,TFACEDRAWORDER *fc,LITVERTEX *vl,TWORLDVERTEX *wv)
  {
  float r,g,b;
  TVERTEXCOLOR *vc=cvtable+fc->dwVertexColor;
  G3D_TLVERTEX *vt=face+index;
  TSCENEVERTEX *sc=sctable+fc->dwVertex;
  static LITVERTEX black;
  static char blackzero=0;

  if (vc>=cvtable && vc->flags & TVCOL_NODIFFUSE)
    {
    if (!blackzero)
      {
      memset(&black,0,sizeof(black));
      blackzero=1;
      }
    vl=&black;
    }
  else if (vl->cashed!=lcashcounter) g3dLitVertex(sc, vl, wv);
  if (vc>=cvtable)
    {
    if (vc->flags & TVCOL_ANIMATED)
      {
      float ar,ag,ab;
      ar=vc->ampl_r*GetSine(framecounter,vc->freq,vc->offs_r);
      ag=vc->ampl_g*GetSine(framecounter,vc->freq,vc->offs_g);
      ab=vc->ampl_b*GetSine(framecounter,vc->freq,vc->offs_b);
      if (vc->flags & TVCOL_ANIMDIFF)
        r=vl->r*(vc->pohlc_r+ar)+vc->emit_r,
        g=vl->g*(vc->pohlc_g+ag)+vc->emit_g,
        b=vl->b*(vc->pohlc_b+ab)+vc->emit_b;
      else
        r=vl->r*vc->pohlc_r+vc->emit_r+ar,
        g=vl->g*vc->pohlc_g+vc->emit_g+ag,
        b=vl->b*vc->pohlc_b+vc->emit_b+ab;
      }
    else
      {
      r=vl->r*vc->pohlc_r+vc->emit_r;
      g=vl->g*vc->pohlc_g+vc->emit_g;
      b=vl->b*vc->pohlc_b+vc->emit_b;
      }
    }
  else
    {
    r=vl->r;
    g=vl->g;
    b=vl->b;
    vc=NULL;
    }
  memcpy(vt->coord,wv->vector,sizeof(HVECTOR));
  vt->tu=fc->tu;
  vt->tv=fc->tv;
  r=max(r,0.0f);g=max(g,0.0f);b=max(b,0.0f);
  r=min(r,1.0f);g=min(g,1.0f);b=min(b,1.0f);
  if (vc!=NULL && vc->flags & TVCOL_ALPHA) vt->color=G3D_RGBA(r,g,b,vc->alpha);
  else vt->color=G3D_RGB(r,g,b);
  return 0;
  }

static void G3D_FASTCALL AddLensflare(int primtype,int primIndex,int texture, G3D_TLVERTEX *face)
  {
  LENSINFO *inf=lensflare+lenscount;
  while (primIndex)
    {
    if (lenscount>=MAXLENS) return;
    inf->vt=*face++;
    inf->flags=(char)primtype;
    inf->texture=texture;
    lenscount++;
    primIndex--;
    }
  }

static void G3D_FASTCALL DisplayLensflares()
  {
  int i;
  LENSINFO *fc;

  for (i=0,fc=lensflare;i<lenscount;i++,fc++)
    if (g3dGetZBufferValue((int)fc->vt.coord[XVAL],(int)fc->vt.coord[YVAL],(char)(i==0?G3D_ZBWAIT:0))>=fc->vt.coord[ZVAL])
      fc->vt.coord[ZVAL]=(float)g3dQuerySettingInt(G3DQ_ZMIN);
    else
      if (!(fc->flags & G3D_OPTIALWAYS)) fc->flags=0;
  for (i=0,fc=lensflare;i<lenscount;i++,fc++)
    if (fc->flags)
      {
      g3dtSelectTexture(0,fc->texture);
      g3dDrawVertexArray(G3D_LPOINT,1,&fc->vt);
      }
  lenscount=0;
  }

char SortPrimitives(TWORLDVERTEX *tv);

void G3D_FASTCALL g3dCalcFinalMatrix(HMATRIX view_t, HMATRIX proj_t)
  {
  if (view_t!=NULL) SoucinMatic(scene_t,view_t,viewsc_t);
  if (proj_t!=NULL) SoucinMatic(proj_t,viewport_t,projvp_t);
  SoucinMatic(viewsc_t,projvp_t,main_t);
  }

int G3D_FASTCALL g3dDrawScene(HMATRIX view_t, HMATRIX proj_t, char flags)
	{
	TWORLDVERTEX	*wx;
  LITVERTEX *ltt=lttable;
//  TSCENEVERTEX *sc;
  TFACEDRAWORDER *fco=fctable,*fcs;
	int i,j;
  static HMATRIX m;
  int primIndex=0;
  int primtype,fother;
  int sortindex=0;
  int ltexture;
  char nodraw;
  char sort=flags & G3DD_SORT;
  char dblside=0;
  int cull;

  if (!sinetabok) build_sine_tab();
  g3dVertexFormat=g3dQueryVertexFormat();
  g3dCalcFinalMatrix(view_t,proj_t);
  wx=g3dTransformVertices(0,wvtable,wvpos); //transformuj vertices
  if (g3dCalcLights()==-1) return -1;
  if (wx==NULL) return G3D_NOMEMORY;
  if (lttable==NULL || ltsize<scpos)
		{
    ltt=realloc(lttable,sizeof(LITVERTEX)*scpos);
    if (ltt==NULL)
			{
			free(wx);
			return G3D_NOMEMORY;
			}
    lttable=ltt;
    ltsize=scpos;
		}
  lcashcounter++;
  if (sort) sort=!SortPrimitives(wx);
  for(i=0,j=0;i<fcpos;i++,fco++)
		if (j==0)
			{
      int prm,k;
      unsigned char clps,lsclps;
      int indx;

      nodraw=0;
      if (sort) fco=order2[sortindex++];
      j=fco->vcount;
      if (j<0 || j>0x1000000) return G3D_SCENEERROR;
      prm=fco->wFlags;
      primtype=prm & 0xf;
      fother=prm & 0xf0;
	  if (fother & G3D_DOUBLESIDE)
		{
		if (!dblside) {cull=g3dSetCullMode(0);dblside=1;}
		}
	  else if (dblside) {g3dSetCullMode(cull);dblside=0;}
      if (j>=MAXVRTS) j=MAXVRTS-1;
      primIndex=0;
      if (primtype==G3D_STREAMLIGHT)
        {
        prm=G3D_LPOINT;
        if (fco->texture==0) {fco=fco+j;i+=j;j=0; goto skipit;}
        }
      lsclps=CLP_ALL;
      for (k=-1,fcs=fco+j;k<j;k++,fcs++)
        {
        indx=sctable[fcs->dwVertex].wvindex;
        if (indx & 1)
          clps=posclip[indx>>1]>>4;else clps=posclip[indx>>1]&0xf;
        if ((clps & lsclps)==0) break;
        lsclps=clps;
        if (k==-1) fcs=fco;
        }
      if (k==j)
        {i+=j;j=0;fco=fcs-1;}
      else
        if (primtype==G3D_OPTICEFF)
          ltexture=fco->texture;
        else
          g3dtSelectTexture(0,fco->texture);
      skipit:;
      }
		else
      {
      if (!nodraw)
        {
        TWORLDVERTEX *wv=wx+sctable[fco->dwVertex].wvindex;
        nodraw|=AddVertexToPrim(primIndex,fco,fco->dwVertex+lttable,wv);
        if (primtype==G3D_BILLBOARD || primtype==G3D_OPTICEFF)
          {
          wv=wvtable+sctable[fco->dwVertex].wvindex;
          g3dCorrectBillboard(face+primIndex,wv,viewsc_t,proj_t);
          }
        primIndex++;
        }
      j--;
      if (j==0)
        {
        if (!nodraw)
          {
          if (primtype==G3D_BILLBOARD) primtype=G3D_LPOINT;
          if (primtype==G3D_OPTICEFF) AddLensflare(primtype|fother,primIndex,ltexture,face);
          else g3dDrawVertexArray(primtype, primIndex, face);
          }
        primIndex=0;
        }
			}
  if (dblside) {g3dSetCullMode(cull);dblside=0;}
  if (j!=0) return G3D_SCENEERROR;
  if (lenscount) DisplayLensflares();
	return G3D_OK;
	}

void G3D_FASTCALL g3dSetAmbientLight(float intensity)
	{
	ambient_light=intensity;
	}

static long wleft,wtop,wright,wbottom;

void G3D_FASTCALL g3dSetViewport(float ZeroX,float ZeroY,float Width, float Height,char Clip)
  {
  Width/=2;
  Height/=2;
  half_width=Width;
  half_height=Height;
  JednotkovaMatice(viewport_t);
  viewport_t[3][0]=ZeroX;
  viewport_t[3][1]=ZeroY;
  viewport_t[0][0]=Width;
  viewport_t[1][1]=Height;
  viewport_t[2][2]=(float)g3dQuerySettingInt(G3DQ_ZMAX)-(float)g3dQuerySettingInt(G3DQ_ZMIN);
  viewport_t[3][2]=(float)g3dQuerySettingInt(G3DQ_ZMIN);
  if (Clip)
    {
    wleft=(long)(ZeroX-Width);
    wtop=(long)(ZeroY-Height);
    wright=(long)(ZeroX+Width);
    wbottom=(long)(ZeroY+Height);
    wleft=max(wleft,0);
    wtop=max(wtop,0);
    g3dClipWindow(wleft,wtop,wright,wbottom);
    }
  }


static int SortStep1(TWORLDVERTEX *tv)
//1. Vypocita Z hodnoty;
//2. Rozdeli tabulky class1 a class2
//3. Urci pocet primitiv, ktere se budou kreslit
  {
  int j=0,prim=0;
  float minz,z;
  unsigned short wz;
  unsigned char wzh,wzl;
  TFACEDRAWORDER *fc=fctable,*hd;
  int i;
  float zmx,zmn,zdf;

  zmn=(float)g3dQuerySettingInt(G3DQ_ZMIN);
  zmx=(float)g3dQuerySettingInt(G3DQ_ZMAX);
  zdf=zmx-zmn;
  for(i=0;i<fcpos;i++,fc++)
    {
    if (j==0)
      {
      hd=fc;
      j=fc->vcount;
      prim++;minz=zmx;
      }
    else
      {
      z=tv[sctable[fc->dwVertex].wvindex].vector[ZVAL];
      if (z<zmn) z=zmn;
      if (z>zmx) z=zmx;
      minz=min(minz,z);
      j--;
      if (j==0)
        {
        wz=hd->wZorder=65535-(unsigned short)((minz-zmn)/zdf*65535.0f);
        wzh=wz>>8;
        wzl=wz & 0xff;
        class1[wzl]++;
        class2[wzh]++;
        }
      }
    }
  return prim;
  }

static int Recalcul_table(long *l)
  //1. Vypocte offsety v tabulce
  //2. Vraci celkovy pocet entit
  {
  int i;
  long s=0,s2;
  for (i=0;i<256;i++)
    {
    s2=s;s+=*l;*l=s2;l++;
    }
  return s;
  }

static void SortPointers()
  //1. tridi ukazatele na facesy
  {
  TFACEDRAWORDER *fc=fctable,*ft;
  TFACEDRAWORDER *end=fctable+fcpos;
  TFACEDRAWORDER **topo1=order1;
  unsigned char lz,hz;

  while (fc<end)
    {
    lz=fc->wZorder & 0xff;
    order1[class1[lz]++]=fc;
    while (*topo1!=NULL)
      {
      ft=*topo1;
      hz=ft->wZorder>>8;
      order2[class2[hz]++]=ft;
      topo1++;
      }
    fc+=fc->vcount+1;
    }
  }

char SortPrimitives(TWORLDVERTEX *tv)
  {
  int primc;

  TFACEDRAWORDER **f;
  memset(class1,0,sizeof(class1));
  memset(class2,0,sizeof(class2));
  SortStep1(tv);
  primc=Recalcul_table(class1);
  Recalcul_table(class2);
  if (ordsize<primc+1)
    {
    ordsize=primc+1;
    f=(TFACEDRAWORDER **)realloc(order1,ordsize*sizeof(TFACEDRAWORDER *));
    if (f==NULL) return -1;else order1=f;
    f=(TFACEDRAWORDER **)realloc(order2,ordsize*sizeof(TFACEDRAWORDER *));
    if (f==NULL) return -1;else order2=f;
    }
 memset(order1,0,sizeof(TFACEDRAWORDER *)*primc);
 memset(order2,0,sizeof(TFACEDRAWORDER *)*primc);
 order2[primc]=NULL;
  order1[primc]=NULL;
  SortPointers();
  return 0;
  }

int G3D_FASTCALL g3dLoadObjectEx2(TSCENEDEF *def, HMATRIX mm, HTXITABLE txi)
  {
  int i,j;
  TVERTEXCOLOR *cvt=def->cvtable;
  TNORMALVERTEX *nt=def->ntable;
  TWORLDVERTEX *wt=def->wtable;
  TSCENEVERTEX *st=def->stable;
  TFACEDRAWORDER *ft=def->fctable;
  int cvtoff=cvpos;
  int ntoff=nvpos;
  int wtoff=wvpos;
  int stoff=scpos;
  int ftoff=fcpos;
  int txt;
  g3dBeginObject(mm);
  for(i=0;i<def->cvsize;i++,cvt++) if (g3dAddColor(cvt)==-1) goto error;
  for(i=0;i<def->nsize;i++,nt++) if (g3dAddNormal(nt->vector)==-1) goto error;
  for(i=0;i<def->wsize;i++,wt++) if (g3dAddPosition(wt->vector)==-1) goto error;
  for(i=0;i<def->ssize;i++,st++)
    if (g3dAddVertex(st->wvindex+wtoff,st->nvindex+(st->nvindex==-1?0:ntoff))==-1) goto error;
  for(i=0,j=0;i<def->fcsize;i++,ft++)
    if (j==0)
      {
      if (txi!=NULL) txt=g3dTxiGetTexture(txi,ft->texture);else txt=ft->texture;
      if (g3dAddFace(j=ft->vcount,ft->wFlags,txt)==-1) goto error;
      if (j<0 || j>0x1000000) goto error;
      }
    else
      {
      int i=ft->dwVertexColor;
      if (i>=0) i+=cvtoff;
      if (g3dAddFaceVertex(ft->dwVertex+stoff,i,ft->tu,ft->tv)==-1) goto error;
      j--;
      }
  return 0;
error:
  fcpos=ftoff;
  scpos=ftoff;
  wvpos=wtoff;
  nvpos=ntoff;
  cvpos=cvtoff;
  return -1;
  }


/*int g3dLoadObject(TSCENEDEF *def, HMATRIX mm)
  {
  int i,j;
  TVERTEXCOLOR *cvt=def->cvtable;
  TNORMALVERTEX *nt=def->ntable;
  TWORLDVERTEX *wt=def->wtable;
  TSCENEVERTEX *st=def->stable;
  TFACEDRAWORDER *ft=def->fctable;
  int cvtoff=cvpos;
  int ntoff=nvpos;
  int wtoff=wvpos;
  int stoff=scpos;
  int ftoff=fcpos;
  g3dBeginObject(mm);
  for(i=0;i<def->cvsize;i++,cvt++) if (g3dAddColor(cvt)==-1) goto error;
  for(i=0;i<def->nsize;i++,nt++) if (g3dAddNormal(nt->vector)==-1) goto error;
  for(i=0;i<def->wsize;i++,wt++) if (g3dAddPosition(wt->vector)==-1) goto error;
  for(i=0;i<def->ssize;i++,st++)
    if (g3dAddVertex(st->wvindex+wtoff,st->nvindex+ntoff,st->cvindex+cvtoff)==-1) goto error;
  for(i=0,j=0;i<def->fcsize;i++,ft++)
    if (j==0)
      {
      if (g3dAddFace(j=ft->vcount,ft->wFlags,ft->texture)==-1) goto error;
      }
    else
      {
      if (g3dAddFaceVertex(ft->dwVertex+stoff,ft->tu,ft->tv)==-1) goto error;
      j--;
      }
  return 0;
error:
  fcpos=ftoff;
  scpos=ftoff;
  wvpos=wtoff;
  nvpos=ntoff;
  cvpos=cvtoff;
  return -1;
  }
*/

//----- scene optimizator -----

typedef struct _tree4d
  {
  union
    {
    HVECTOR vector;
    TSCENEVERTEX sv;
    };
  int valcomp;
  int index;
  struct _tree4d *left,*right,*midl;
  }TREE4D;

static TREE4D *koren;
static float tolerance;

static int InsertTree(HVECTOR vv,int index)
  {
  int valcomp=0;
  TREE4D *ptr,**chng;

  ptr=koren;chng=&koren;
  while (ptr!=NULL)
    {
    if (fabs(ptr->vector[XVAL]-vv[XVAL])<tolerance &&
        fabs(ptr->vector[YVAL]-vv[YVAL])<tolerance &&
        fabs(ptr->vector[ZVAL]-vv[ZVAL])<tolerance &&
        fabs(ptr->vector[WVAL]-vv[WVAL])<tolerance)
        return ptr->index;
    if (fabs(ptr->vector[valcomp]-vv[valcomp])<tolerance && valcomp<3)
      chng=&ptr->midl,valcomp++;
    else if (ptr->vector[valcomp]>vv[valcomp])
      chng=&ptr->left;
    else if (ptr->vector[valcomp]<=vv[valcomp])
      chng=&ptr->right;
    ptr=*chng;
    }
  ptr=malloc(sizeof(TREE4D));
  memcpy(ptr->vector,vv,sizeof(HVECTOR));
  ptr->valcomp=0;
  ptr->index=index;
  ptr->left=NULL;
  ptr->right=NULL;
  ptr->midl=NULL;
  *chng=ptr;
  return index;
  }

static int InsertTree2(TSCENEVERTEX *sv,int index)
  {
  int valcomp=0;
  TREE4D *ptr,**chng;
  int *comp2=&sv->wvindex;

  ptr=koren;chng=&koren;
  while (ptr!=NULL)
    {
    int *comp1;
    if (ptr->sv.wvindex==sv->wvindex && ptr->sv.nvindex==sv->nvindex) return ptr->index;
    if (valcomp==0) comp1=&ptr->sv.wvindex;else comp1=&ptr->sv.nvindex;
    if (*comp2==*comp1)
      {
      if (valcomp==1) return ptr->index;
      valcomp=1;
      comp2=&sv->nvindex;
      chng=&ptr->midl;
      }
    else if (*comp1>*comp2) chng=&ptr->left;
    else chng=&ptr->right;
    ptr=*chng;
    }
  ptr=malloc(sizeof(TREE4D));
  memcpy(&ptr->sv,sv,sizeof(TSCENEVERTEX));
  ptr->valcomp=0;
  ptr->index=index;
  ptr->left=NULL;
  ptr->right=NULL;
  ptr->midl=NULL;
  *chng=ptr;
  return index;
  }


static void FreeTree(TREE4D *x)
  {
  if (x==NULL) return;
  FreeTree(x->left);
  FreeTree(x->right);
  FreeTree(x->midl);
  free(x);
  }

int g3dOptimizeNormals(float precision)
  {
  TNORMALVERTEX *new;
  int ind,indexn,indz;
  TSCENEVERTEX *w;
  int i;
  long inv=-1;

  koren=NULL;
  tolerance=precision;
  new=malloc(sizeof(*new)*nvpos);
  if (new==NULL) return G3D_NOMEMORY;
  w=sctable;
  for (i=0,indexn=0;i<scpos;i++,w++)
    {
    indz=w->nvindex;
	dbgprintf("%f %f %f",nvtable[indz].vector[XVAL],nvtable[indz].vector[YVAL],nvtable[indz].vector[ZVAL]);
    ind=InsertTree(nvtable[indz].vector,indexn);
    if (ind==indexn) new[indexn++]=nvtable[indz];
    w->nvindex=ind;
    }
  free(nvtable);
  nvtable=new;
  nvsize=nvpos=indexn;
  FreeTree(koren);
  return G3D_OK;
  }

int g3dOptimizePositions(float precision)
  {
  TWORLDVERTEX *new;
  int ind,indexn,indz;
  TSCENEVERTEX *w;
  int i;
  long inv=-1;

  koren=NULL;
  tolerance=precision;
  new=malloc(sizeof(*new)*wvpos);
  if (new==NULL) return G3D_NOMEMORY;
  w=sctable;
  for (i=0,indexn=0;i<scpos;i++,w++)
    {
    indz=w->wvindex;
    ind=InsertTree(wvtable[indz].vector,indexn);
    if (ind==indexn) new[indexn++]=wvtable[indz];
    w->wvindex=ind;
    }
  free(wvtable);
  wvtable=new;
  wvsize=wvpos=indexn;
  FreeTree(koren);
  return G3D_OK;
  }

int g3dOptimizeVertices()
  {
  TFACEDRAWORDER *fc;
  int ind,indexn,indz;
  TSCENEVERTEX *new;
  int i,j;
  long inv=-1;

  koren=NULL;
  new=malloc(scpos*sizeof(*new));
  if (new==NULL) return G3D_NOMEMORY;
  fc=fctable;j=0;
  for (i=0,indexn=0;i<fcpos;i++,fc++)
    {
    if (j==0) j=fc->vcount;
    else
      {
      indz=fc->dwVertex;
      ind=InsertTree2(&sctable[indz],indexn);
      if (ind==indexn) new[indexn++]=sctable[indz];
      fc->dwVertex=ind;
      j--;
      }
    }
  free(sctable);
  sctable=new;
  scsize=scpos=indexn;
  FreeTree(koren);
  return G3D_OK;
  }

int g3dOptimizeScene(float posprec, float normprec)
  {
  int i;

  i=g3dOptimizePositions(posprec);
  if (i==G3D_OK) i=g3dOptimizeNormals(normprec);
  if (i==G3D_OK) i=g3dOptimizeVertices();
  return i;
  }

void g3dSetClipRange(HVECTOR min,HVECTOR max, char enabled)
  {
  if (enabled)
    {
    memcpy(clipmin,min,sizeof(clipmin));
    memcpy(clipmax,max,sizeof(clipmax));
    }
  clipenabled=enabled;
  }
/*
int FindVertexFromPoint(float x,float y,int from)
  {
  float minx,minz,dif1,dif2,dif3;
  int i,last=-1;
  TWORLDVERTEX *v=d3dtable;

  if (from<0) minz=-1.0f;else minz=d3dtable[from].coord[ZVAL];
  minx=65536.0f;
  for (i=0;i<scpos;i++,v++)
    {
    dif1=(float)fabs(v->coord[XVAL]-x);
    dif2=(float)fabs(v->coord[YVAL]-y);
    dif2+=dif1;
    dif3=v->coord[ZVAL]-minz;
    if (dif3>0)
      {
      if (dif2<=minx) minx=dif2,last=i;
      }
    }
  return last;
  }
/*
int FindFaceFromPoint(float x,float y,int start)
  {
  TFACEDRAWORDER *fc=fctable;
  int ffc,vw;
  int ths,maxths=0;
  float min,minlast;

  ffc=-1;vw=-1;
  minlast=min=65536.0f;
  maxths=0;
  for (i=0;i<fcpos;i++)
    {
    if (j==0)
      {
      minlast=min;
      ths=0;ths
      }
    }
  }
*/

void g3dGetTransformMatrix(HMATRIX m)
  {
  memcpy(m,main_t,sizeof(main_t));
  }

unsigned int g3dAnimTick(unsigned int speed)
  {
  static unsigned int delayter=0;
  if (speed<=delayter)
    {
    framecounter++;
    delayter=0;
    }
  else
    {
    delayter++;
    }
  return framecounter;
  }

//-------------------- MISC ------------------------------

char g3dGetSceneInfo(TSCENEDEF *scn,G3DSCENEINFO *info)
  {
  TFACEDRAWORDER *fc;
  int i,skp;
  if (scn==NULL) return G3D_INVALIDPARAMS;
  memset(info,0,sizeof(*info));
  info->colors_defined=scn->cvsize;
  for (i=0,skp=0,fc=scn->fctable;i<scn->fcsize;i++,fc++)
    {
    if (skp==0)
      {
      if (i==0)
        {
        info->min_tex_index=info->max_tex_index=fc->texture;
        }
      else
        {
        if (info->min_tex_index>fc->texture)
          info->min_tex_index=fc->texture;
        else if (info->max_tex_index<fc->texture)
          info->max_tex_index=fc->texture;
        }
      info->faces++;
      skp=fc->vcount;
      if (skp<0 || skp>0x1000000) return G3D_SCENEERROR;
      }
    else
      {
      info->colors_total=max(info->colors_total,fc->dwVertexColor);
      skp--;
      }
    }
  info->textures_total=info->max_tex_index-info->min_tex_index+1;
  if (info->colors_defined<info->colors_total)
    info->colors_undefined=info->colors_total-info->colors_defined;
  return G3D_OK;
  }

char g3dInitParticles(TPARTICLES *system)
  {
  memset(system->parray,0,system->pcount*sizeof(TPARTICLEITEM));
  system->vtptr=particlevt;
  return 0;
  }

static void CreateRandomColor(TVERTEXCOLOR *tg, TVERTEXCOLOR *one, TVERTEXCOLOR *two, unsigned long flags)
  {
  memcpy(tg,one,sizeof(TVERTEXCOLOR));
  if (flags & G3DPF_RNDDIFFCOLOR)
    {
    tg->pohlc_r=rndmmf(one->pohlc_r,two->pohlc_r);
    tg->pohlc_g=rndmmf(one->pohlc_g,two->pohlc_g);
    tg->pohlc_b=rndmmf(one->pohlc_b,two->pohlc_b);
    }
  if (flags & G3DPF_RNDEMITCOLOR)
    {
    tg->emit_r=rndmmf(one->emit_r,two->emit_r);
    tg->emit_g=rndmmf(one->emit_g,two->emit_g);
    tg->emit_b=rndmmf(one->emit_b,two->emit_b);
    }
  if (flags & G3DPF_RNDALPHA)
    tg->alpha=rndmmf(one->alpha,two->alpha);
  if (flags & G3DPF_RNDEMITCOLOR)
    {
    tg->offs_r=rndmmf(one->offs_r,two->offs_r);
    tg->offs_g=rndmmf(one->offs_g,two->offs_g);
    tg->offs_b=rndmmf(one->offs_b,two->offs_b);
    tg->ampl_r=rndmmf(one->ampl_r,two->ampl_r);
    tg->ampl_g=rndmmf(one->ampl_g,two->ampl_g);
    tg->ampl_b=rndmmf(one->ampl_b,two->ampl_b);
    tg->freq=rndmmf(one->freq,two->freq);
    }
  }

static void CalculateDiffs(TPARTICLEITEM *itm,unsigned long flags)
  {
  float div=1.0f/itm->lifetime;
    if (flags & G3DPF_CHGSIZE)
      itm->rsiz=(itm->rsiz-itm->size)*div;
    else
      itm->rsiz=0.0f;
    if (flags & G3DPF_CHGDIFFCOLOR)
      {
      itm->rcol.pohlc_r=(itm->rcol.pohlc_r-itm->col.pohlc_r)*div;
      itm->rcol.pohlc_g=(itm->rcol.pohlc_g-itm->col.pohlc_g)*div;
      itm->rcol.pohlc_b=(itm->rcol.pohlc_b-itm->col.pohlc_b)*div;
      }
    else
      {
      itm->rcol.pohlc_r=0.0f;
      itm->rcol.pohlc_g=0.0f;
      itm->rcol.pohlc_b=0.0f;
      }
    if (flags & G3DPF_CHGEMITCOLOR)
      {
      itm->rcol.emit_r=(itm->rcol.emit_r-itm->col.emit_r)*div;
      itm->rcol.emit_g=(itm->rcol.emit_g-itm->col.emit_g)*div;
      itm->rcol.emit_b=(itm->rcol.emit_b-itm->col.emit_b)*div;
      }
    else
      {
      itm->rcol.emit_r=0.0f;
      itm->rcol.emit_g=0.0f;
      itm->rcol.emit_b=0.0f;
      }
    if (flags & G3DPF_CHGALPHA)
      itm->rcol.alpha+=(itm->rcol.alpha-itm->col.alpha)*div;
    else
      itm->rcol.alpha=0.0f;
    if (flags & G3DPF_CHGCOLOTHER)
      {
      itm->rcol.ampl_r=(itm->rcol.ampl_r-itm->col.ampl_r)*div;
      itm->rcol.ampl_g=(itm->rcol.ampl_g-itm->col.ampl_g)*div;
      itm->rcol.ampl_b=(itm->rcol.ampl_b-itm->col.ampl_b)*div;
      itm->rcol.offs_r=(itm->rcol.offs_r-itm->col.offs_r)*div;
      itm->rcol.offs_g=(itm->rcol.offs_g-itm->col.offs_g)*div;
      itm->rcol.offs_b=(itm->rcol.offs_b-itm->col.offs_b)*div;
      itm->rcol.freq=(itm->rcol.freq-itm->col.freq)*div;
      }
    else
      {
      itm->rcol.ampl_r=0.0f;
      itm->rcol.ampl_g=0.0f;
      itm->rcol.ampl_b=0.0f;
      itm->rcol.offs_r=0.0f;
      itm->rcol.offs_g=0.0f;
      itm->rcol.offs_b=0.0f;
      itm->rcol.freq=0.0f;
      }

  }

static int g3dMoveParticlesEx(TPARTICLES *system,char onlycreat)
  {
  //Moves and creates new particles;
  int crtremain=system->create;  //remain particles;
  int i;
  int totalcnt=0;
  int nscreate=0; //particles to create in  next system
  TPARTICLEITEM *itm=system->parray;
  TPARTICLES *nextsystem=system->nextsystem;

  for (i=0;i<system->pcount;i++,itm++) if (itm->lifetime!=0)
    {
    if (!onlycreat)
    {
    totalcnt++;
    itm->position[XVAL]+=itm->direction[XVAL];
    itm->position[YVAL]+=itm->direction[YVAL];
    itm->position[ZVAL]+=itm->direction[ZVAL];
    if (system->flags & G3DPF_CHGSIZE)
      itm->size+=itm->rsiz;
    if (system->flags & G3DPF_CHGDIFFCOLOR)
      {
      itm->col.pohlc_r+=itm->rcol.pohlc_r;
      itm->col.pohlc_g+=itm->rcol.pohlc_g;
      itm->col.pohlc_b+=itm->rcol.pohlc_b;
      }
    if (system->flags & G3DPF_CHGEMITCOLOR)
      {
      itm->col.emit_r+=itm->rcol.emit_r;
      itm->col.emit_g+=itm->rcol.emit_g;
      itm->col.emit_b+=itm->rcol.emit_b;
      }
    if (system->flags & G3DPF_CHGALPHA)
      itm->col.alpha+=itm->rcol.alpha;
    if (system->flags & G3DPF_CHGCOLOTHER)
      {
      itm->col.ampl_r+=itm->rcol.ampl_r;
      itm->col.ampl_g+=itm->rcol.ampl_g;
      itm->col.ampl_b+=itm->rcol.ampl_b;
      itm->col.offs_r+=itm->rcol.offs_r;
      itm->col.offs_g+=itm->rcol.offs_g;
      itm->col.offs_b+=itm->rcol.offs_b;
      itm->col.freq+=itm->rcol.freq;
      }
    itm->lifetime--;
    if (itm->lifetime==0 && nextsystem!=NULL)
      {
      g3dPtclSetProp(nextsystem,create,rndmmi(system->countmin,system->countmax));
      g3dPtclSetValue(nextsystem,emitor[XVAL],itm->position[XVAL],itm->position[XVAL]);
      g3dPtclSetValue(nextsystem,emitor[YVAL],itm->position[YVAL],itm->position[YVAL]);
      g3dPtclSetValue(nextsystem,emitor[ZVAL],itm->position[ZVAL],itm->position[ZVAL]);
      g3dPtclSetValue(nextsystem,emitor[WVAL],itm->position[WVAL],itm->position[WVAL]);
      g3dMoveParticlesEx(nextsystem,1);
      }
    }
    }
  else if (crtremain)
    {
    float azimut,zenit,cz;
    totalcnt++;
    memset(itm,0,sizeof(*itm));
    itm->lifetotal=itm->lifetime=rndmmi(system->stops[0].lifetime,system->stops[1].lifetime);
    azimut=rndmmf(system->stops[0].azimut,system->stops[1].azimut);
    zenit=rndmmf(system->stops[0].zenit,system->stops[1].zenit);
    cz=(float)cos(zenit);
    itm->direction[XVAL]=(float)cos(azimut)*cz;
    itm->direction[YVAL]=(float)sin(azimut)*cz;
    itm->direction[ZVAL]=(float)sin(zenit);
    itm->direction[WVAL]=1.0f;
    NasobVektor(itm->direction,rndmmf(system->stops[0].speed,system->stops[1].speed));
    if (system->flags & G3DPF_RNDPOSITION)
      {
      itm->position[XVAL]=rndmmf(system->stops[0].emitor[XVAL],system->stops[1].emitor[XVAL]);
      itm->position[YVAL]=rndmmf(system->stops[0].emitor[YVAL],system->stops[1].emitor[YVAL]);
      itm->position[ZVAL]=rndmmf(system->stops[0].emitor[ZVAL],system->stops[1].emitor[ZVAL]);
      }
    else
      memcpy(itm->position,system->stops[0].emitor,sizeof(HVECTOR));
    itm->position[WVAL]=1.0f;
    if (system->flags & G3DPF_RNDSIZE)
      {
      itm->size=rndmmf(system->stops[0].sizebeg,system->stops[1].sizebeg);
      itm->rsiz=rndmmf(system->stops[0].sizeend,system->stops[1].sizeend);
      }
    CreateRandomColor(&itm->col,&system->stops[0].val1,&system->stops[1].val1,system->flags);
    CreateRandomColor(&itm->rcol,&system->stops[0].val2,&system->stops[1].val2,system->flags);
    CalculateDiffs(itm,system->flags);
    crtremain--;
    }
  if (system->nextsystem!=NULL)
    {
    g3dPtclStart(system->nextsystem,0);
    totalcnt+=g3dMoveParticlesEx(system->nextsystem,0);
    }
  return totalcnt;
  }

int g3dMoveParticles(TPARTICLES *system)
  {
  return g3dMoveParticlesEx(system,0);
  }


char g3dLoadParticles(TPARTICLES *system, HMATRIX mm)
  {
  int i;
  TPARTICLEITEM *itm=system->parray;
  int tex;


  g3dBeginObject(mm);
  for (i=0;i<system->pcount;i++,itm++) if (itm->lifetime!=0)
    {
    int p,c,v;
    if (system->texbeg==system->texend) tex=system->texbeg;
    else tex=system->texend-(system->texend-system->texbeg)*itm->lifetime/itm->lifetotal;
    p=g3dAddPosition(itm->position);
    if (p<0) return p;
    c=g3dAddColor(&itm->col);
    if (c<0) return c;
    v=g3dAddVertex(p,-1);
    if (v<0) return c;
    g3dAddFace(1,system->primtype,tex);
    g3dAddFaceVertex(v,c,itm->size,itm->size);
    }
  return 0;
  }

static char g3dDuplParticles(void *from,void *to)
  {
  memcpy(to,from,sizeof(TPARTICLES));
  return 0;
  }


float *g3dAskPosition(int position)
  {
  return wvtable[position].vector;
  }

void g3dCalcNormal(int p1,int p2, int p3, HVECTOR norm)
  {
  HVECTOR pp,v1,v2;

  CopyVektor(pp,g3dAskPosition(p1));
  CopyVektor(v1,g3dAskPosition(p2));
  CopyVektor(v2,g3dAskPosition(p3));
  CorrectVectorM(v1);
  CorrectVectorM(v2);
  CorrectVectorM(pp);
  RozdilVektoru(v1,pp);
  RozdilVektoru(v2,pp);
  VektorSoucin(v1,v2,norm);
  norm[WVAL]=1.0f;
  NormalizeVector(norm);
  }

#define DuplCascade(ol,nw,ptr,sz,type)\
  if ((nw->ptr=malloc(ol->sz*sizeof(type)))==NULL) \
    {g3dDestroyScene(nw);return G3D_NOMEMORY;} \
  else\
    {memcpy(nw->ptr,ol->ptr,ol->sz*sizeof(type));nw->sz=ol->sz;}

char g3dDuplScene(TSCENEDEF *scene, TSCENEDEF *newscene)
  {
  memset(newscene,0,sizeof(*newscene));
  DuplCascade(scene,newscene,cvtable,cvsize,TVERTEXCOLOR);
  DuplCascade(scene,newscene,wtable,wsize,TWORLDVERTEX);
  DuplCascade(scene,newscene,ntable,nsize,TNORMALVERTEX);
  DuplCascade(scene,newscene,stable,ssize,TSCENEVERTEX);
  DuplCascade(scene,newscene,fctable,fcsize,TFACEDRAWORDER);
  return 0;
  }

TSCENEDEF *g3dCompactScene(TSCENEDEF *scn)
  {
  unsigned long total,cvs,ws,ns,ss,fcs;
  TSCENEDEF *nd;
  char *ptr;

  total=(cvs=scn->cvsize*sizeof(TVERTEXCOLOR))+
        (ws=scn->wsize*sizeof(TWORLDVERTEX))+
        (ns=scn->nsize*sizeof(TNORMALVERTEX))+
        (ss=scn->ssize*sizeof(TSCENEVERTEX))+
        (fcs=scn->fcsize*sizeof(TFACEDRAWORDER))+
        sizeof(TSCENEDEF);
  nd=malloc(total);
  if (nd==NULL) return NULL;
  ptr=(char *)nd+sizeof(TSCENEDEF);
  nd->cvtable=(TVERTEXCOLOR *)ptr;nd->cvsize=scn->cvsize;
  memcpy(ptr,scn->cvtable,cvs);ptr+=cvs;
  nd->wtable=(TWORLDVERTEX *)ptr;nd->wsize=scn->wsize;
  memcpy(ptr,scn->wtable,ws);ptr+=ws;
  nd->ntable=(TNORMALVERTEX *)ptr;nd->nsize=scn->nsize;
  memcpy(ptr,scn->ntable,ns);ptr+=ns;
  nd->stable=(TSCENEVERTEX *)ptr;nd->ssize=scn->ssize;
  memcpy(ptr,scn->stable,ss);ptr+=ss;
  nd->fctable=(TFACEDRAWORDER *)ptr;nd->fcsize=scn->fcsize;
  memcpy(ptr,scn->fctable,fcs);ptr+=fcs;
  nd->vtptr=scenecompactvt;
  return nd;
  }

TSCENEDEF *g3dSceneInicialize(TSCENEDEF *scn)
  {
  scn->vtptr=scenevt;
  return scn;
  }

int G3D_FASTCALL g3dLoadObjectEx(TSCENEDEF *def, HMATRIX mm, unsigned short *txit, int count)
  {
  HTXITABLE txi;
  int i;

  if (txit!=NULL)
    {
    txi=alloca(sizeof(TTXITABLE)+sizeof(TXITEXTURE)*count);
    for (i=0;i<count;i++) g3dTxiSetTexture(txi,i,txit[i]);
    txi->count=count;
    }
  else txi=NULL;
  return g3dLoadObjectEx2(def,mm,txi);
  }



void g3dClearStatistics()
  {
  memset(&draw_stat,0,sizeof(draw_stat));
  }

void g3dGetStatistics(G3D_STATISTICS *stat)
  {
  draw_stat.vertices+=scpos;
  draw_stat.normals+=nvpos;
  draw_stat.positions+=wvpos;
  draw_stat.points+=g3dGetStat(0);
  draw_stat.lines+=g3dGetStat(1);
  draw_stat.triangles+=g3dGetStat(2);
  g3dClearStat(0);
  g3dClearStat(1);
  g3dClearStat(2);
  *stat=draw_stat;
  }

LPHMATRIX g3dGetSystemMatrix(char matrix)
  {
  static LPHMATRIX matrixptr[]=
	{world_t,main_t,light_t,scene_t,projvp_t,viewsc_t,viewport_t};
  return matrixptr[matrix];
  }

float *g3dGetVertexPart(int vertex, int part)
  {
  TSCENEVERTEX *sc=sctable+vertex;
  TNORMALVERTEX *nv;
  TWORLDVERTEX *wv;
  TWORLDVERTEX *tv;
  switch (part)
  	{
	case G3DVP_POSITION: wv=wvtable+sc->wvindex;return wv->vector;
	case G3DVP_NORMAL: nv=nvtable+sc->nvindex;return nv->vector;
	case G3DVP_TPOSITION: tv=transfv+sc->wvindex;return tv->vector;
	default: return NULL;
	}
  }
