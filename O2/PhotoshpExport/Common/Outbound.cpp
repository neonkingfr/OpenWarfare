/*******************************************************************/
/*                                                                 */
/*                      ADOBE CONFIDENTIAL                         */
/*                   _ _ _ _ _ _ _ _ _ _ _ _ _                     */
/*                                                                 */
/* Copyright 1990 - 1999 Adobe Systems Incorporated                */
/* All Rights Reserved.                                            */
/*                                                                 */
/* NOTICE:  All information contained herein is, and remains the   */
/* property of Adobe Systems Incorporated and its suppliers, if    */
/* any.  The intellectual and technical concepts contained         */
/* herein are proprietary to Adobe Systems Incorporated and its    */
/* suppliers and may be covered by U.S. and Foreign Patents,       */
/* patents in process, and are protected by trade secret or        */
/* copyright law.  Dissemination of this information or            */
/* reproduction of this material is strictly forbidden unless      */
/* prior written permission is obtained from Adobe Systems         */
/* Incorporated.                                                   */
/*                                                                 */
/*******************************************************************/
//-------------------------------------------------------------------
//-------------------------------------------------------------------------------
//
//	File:
//		Outbound.c
//
//	Copyright 1990-1992, Thomas Knoll.
//	All Rights Reserved.
//
//	Description:
//		This file contains the functions and source
//		for the Export module Outbound, a module that
//		creates a file and stores raw pixel data in it.
//
//	Use:
//		This module shows how to export raw data to a file.
//		It uses a simple "FileUtilities" library that comes
//		with the SDK.  You use it via File>>Export>>Outbound.
//
//	Version history:
//		Version 1.0.0.	4/1/1990	Created for Photoshop 2.0.
//			Written by Thomas Knoll.
//
//		Version 1.5.0	5/1/1993	Update for Photoshop 3.0.
//			Cleaned up suites, documentation.
//
//		Version 2.0.0.	6/1/1996	Update for Photoshop 4.0.
//			Added scripting, new parameters.
//
//		Version 2.0.1.	7/11/1997	Updated for Photoshop 4.0.1.
//			CodeWarrior Pro release 1.
//
//-------------------------------------------------------------------------------

//-------------------------------------------------------------------------------
//	Includes -- Use precompiled headers if compiling with CodeWarrior.
//-------------------------------------------------------------------------------

#include "../PIDefines.h"

#ifdef __PIMWCWMacPPC__
	#include "Outbound-PPC.ch"
#elif defined(__PIMWCWMac68K__)
	#include "Outbound-68k.ch"
	// We're using Metrowerks and they have an A4 library, so set that up:
	#include <SetupA4.h>
	#include <A4Stuff.h>
	
	#define UseA4 	1	// We've now defined this for quick checks later.
#else
	#include "Outbound.h"
#endif

#include "../win/Outbound-sym.h"
#include "../win/StringRes.h"
#include "..\..\..\IMG\PICTURE\picture.hpp"
#include "../win/ExportDlg.h"
#include <es\Files\filenames.hpp>
#include <COMMCTRL.H>

//-------------------------------------------------------------------------------
//	Prototypes.
//-------------------------------------------------------------------------------



// Everything comes in and out of ENTRYPOINT. It must be first routine in source:
MACPASCAL void ENTRYPOINT (const short selector,
					  	   ExportRecord *exportParamBlock,
						   long *data,
						   short *result);
void InitGlobals (Ptr globalPtr);		  	// Initialize globals.
void ValidateParameters (GPtr globals);		// Validate parameters.
void DoPrepare (GPtr globals);
void DoStart (GPtr globals);				// Main routine.
void DoContinue (GPtr globals);
void DoFinish (GPtr globals);
void DoInitialRect (GPtr globals);
Boolean DoNextRect (GPtr globals);
void DoExportRect (GPtr globals);

//-------------------------------------------------------------------------------
//	Globals -- Define global variables for plug-in scope.
//-------------------------------------------------------------------------------

// Windows global for plug-in (considered a Dynamically Linked Library).
// Leave NULL for Macintosh; many cross-platform library utilities require
// something passed in for hDllInstance:
HANDLE hDllInstance = NULL;
CStringRes StrRes;

//-------------------------------------------------------------------------------
//
//	ENTRYPOINT / main
//
//	All calls to the plug-in module come through this routine.
//	It must be placed first in the resource.  To achieve this,
//	most development systems require this be the first routine
//	in the source.
//
//	The entrypoint will be "pascal void" for Macintosh,
//	"void" for Windows.
//
//	Inputs:
//		const short selector						Host provides selector indicating
//													what command to do.
//
//		ExportRecord *exportParamBlock				Host provides pointer to parameter
//													block containing pertinent data
//													and callbacks from the host.
//													See PIExport.h.
//
//	Outputs:
//		ExportRecord *exportParamBlock				Host provides pointer to parameter
//													block containing pertinent data
//													and callbacks from the host.
//													See PIExport.h.
//
//		long *data									Use this to store a handle to our
//													global parameters structure, which
//													is maintained by the host between
//													calls to the plug-in.
//
//		short *result								Return error result or noErr.  Some
//													errors are handled by the host, some
//													are silent, and some you must handle.
//													See PIGeneral.h.
//
//-------------------------------------------------------------------------------

HINSTANCE hInstance;

BOOL WINAPI DllMain(
  HINSTANCE hinstDLL,  // handle to DLL module
  DWORD fdwReason,     // reason for calling function
  LPVOID lpvReserved   // reserved
)
  {
  hInstance=hinstDLL;
  return DLLInit(hinstDLL,fdwReason,lpvReserved);
  }



MACPASCAL void ENTRYPOINT (const short selector,
						   ExportRecord *exportParamBlock,
						   long *data,
						   short *result)
{
	//---------------------------------------------------------------------------
	//	(1) Enter code resource if Mac 68k.
	//---------------------------------------------------------------------------
	StrRes.SetInstance((HINSTANCE)hDllInstance);
	char s[256];
	LoadString((HINSTANCE)hDllInstance,IDS_INVALIDNUMBEROFPLANES,s,256);


	
	#ifdef UseA4				// Are we in 68k Mac MW?
		EnterCodeResource(); 	// A4-globals
	#endif
	
	//---------------------------------------------------------------------------
	//	(2) Check for about box request.
	//
	// 	The about box is a special request; the parameter block is not filled
	// 	out, none of the callbacks or standard data is available.  Instead,
	// 	the parameter block points to an AboutRecord, which is used
	// 	on Windows.
	//---------------------------------------------------------------------------

	if (selector == exportSelectorAbout)
	{
		DoAbout((AboutRecordPtr)exportParamBlock);
	}
	else
	{ // do the rest of the process as normal:

		//-----------------------------------------------------------------------
		//	(3) Initialize function and global pointers.
		//
		// 	Initialize function pointer routine dispatcher. We use this to jump
		// 	to the different routines, instead of a long obnoxious switch
		// 	statement.  All routines are expected to have the same prototype
		// 	of "void RoutineName (globals)".  Returns any errors in gResult.
		//
		// 	WARNING: These better be in the order of the selector AND there
		// 	better be a routine for every selector call.
		//-----------------------------------------------------------------------
		
	 	static const FProc routineForSelector [] =
		{
			/* exportSelectorAbout 	 		DoAbout, */
			/* exportSelectorStart */		DoStart,
			/* exportSelectorContinue */	DoContinue,
			/* exportSelectorFinish */		DoFinish,
			/* exportSelectorPrepare */		DoPrepare
		};
		
		Ptr globalPtr = NULL;		// Pointer for global structure
		GPtr globals = NULL; 		// actual globals

		//-----------------------------------------------------------------------
		//	(4) Allocate and initalize globals.
		//
		// 	AllocateGlobals requires the pointer to result, the pointer to the
		// 	parameter block, a pointer to the handle procs, the size of our local
		// 	"Globals" structure, a pointer to the long *data, a Function
		// 	Proc (FProc) to the InitGlobals routine.  It automatically sets-up,
		// 	initializes the globals (if necessary), results result to 0, and
		// 	returns with a valid pointer to the locked globals handle or NULL.
		//-----------------------------------------------------------------------
		
		globalPtr = AllocateGlobals ((uint32)result,
									 (uint32)exportParamBlock,
									 exportParamBlock->handleProcs,
									 sizeof(Globals),
						 			 data,
						 			 InitGlobals);
		
		if (globalPtr == NULL)
		{ // Something bad happened if we couldn't allocate our pointer.
		  // Fortunately, everything's already been cleaned up,
		  // so all we have to do is report an error.
		  
		  *result = memFullErr;
		  return;
		}
		
		// Get our "globals" variable assigned as a Global Pointer struct with the
		// data we've returned:
		globals = (GPtr)globalPtr;

		//-----------------------------------------------------------------------
		//	(5) Dispatch selector.
		//-----------------------------------------------------------------------

		if (selector > exportSelectorAbout && selector <= exportSelectorPrepare)
			(routineForSelector[selector-1])(globals); // dispatch using jump table
		else
			gResult = exportBadParameters;
			
		//-----------------------------------------------------------------------
		//	(6) Unlock data, and exit resource.
		//
		//	Result is automatically returned in *result, which is
		//	pointed to by gResult.
		//-----------------------------------------------------------------------	
		
		// unlock handle pointing to parameter block and data so it can move
		// if memory gets shuffled:
		if ((Handle)*data != NULL)
			PIUnlockHandle((Handle)*data);
	
	} // about selector special		
	
	#ifdef UseA4			// Are we in 68k Mac MW?
		ExitCodeResource(); // A4-globals
	#endif
	
} // end ENTRYPOINT

//-------------------------------------------------------------------------------
//
//	InitGlobals
//	
//	Initalize any global values here.  Called only once when global
//	space is reserved for the first time.
//
//	Inputs:
//		Ptr globalPtr		Standard pointer to a global structure.
//
//	Outputs:
//		Initializes any global values with their defaults.
//
//-------------------------------------------------------------------------------

void InitGlobals (Ptr globalPtr)
{	
	// create "globals" as a our struct global pointer so that any
	// macros work:
	GPtr globals = (GPtr)globalPtr;
	
	// Initialize global variables:
	ValidateParameters (globals);
	
} // end InitGlobals


//-------------------------------------------------------------------------------
//
//	ValidateParameters
//
//	Initialize parameters to default values.
//
//	Inputs:
//		GPtr globals		Pointer to global structure.
//
//	Outputs:
//		gCurrentHistory		Default: 1.
//
//-------------------------------------------------------------------------------

void ValidateParameters (GPtr globals)
{
	gQueryForParameters = true;
	gAliasHandle = nil; // no handle, yet

} // end ValidateParameters

//-------------------------------------------------------------------------------
//
//	DoPrepare
//
//	Initialize parameters to default values.
//
//	Inputs:
//
//	Outputs:

/*****************************************************************************/

/* Prepare to export an image.	If the plug-in module needs only a limited
   amount of memory, it can lower the value of the 'maxData' field. */

void DoPrepare (GPtr globals)
{
	
	if (gStuff->maxData > 0x80000)
		gStuff->maxData = 0x80000;
	
}

/*****************************************************************************/

#define RANGE_ITER(lower,upper,first,last,step)								  \
	for (lower = (first);                         							  \
		 (upper = (((lower) + (short)(step) < (last)) ? (lower) + (short)(step) : (last))), \
		 lower < (last);													  \
		 lower = upper)

/*****************************************************************************/
/* Requests pointer to the first part of the image to be filtered. */
TextureHints *LoadTextureConfig()
  {
  static TextureHints Def;
  // load texture hits config
  RString retString;
  LoadPictureConfigRetVal ret = LoadPictureConfig(retString);
  if (ret!=LoadPictureConfigOK)
	{
		char msgs[1024],buf[1024],app[1024];
		LoadString(hInstance,IDS_WARNING,app,sizeof(app));
        if (ret==LoadPictureConfigOldExe)
          LoadString(hInstance,IDS_MSG_OLDVERSION,msgs,sizeof(msgs));
        else
		  LoadString(hInstance,IDS_MSG_INVALIDCONFIG,msgs,sizeof(msgs));
		snprintf(buf,sizeof(buf),msgs,(const char *)retString);
		MessageBox(GetForegroundWindow(),buf,app,MB_OK|MB_ICONERROR);
		return NULL;
	}
	return &Def;
  }

void DoStart (GPtr globals)
  {
  HWND photoshp=GetForegroundWindow();
  if (LoadTextureConfig()==NULL) return;
  ValidateParameters(globals);
  int iWidth=globals->exportParamBlock->imageSize.h;
  int iHeight=globals->exportParamBlock->imageSize.v;
  int iPlanes=globals->exportParamBlock->planes;
  if (iPlanes<3 || iPlanes>4)
	{
	MessageBox(GetForegroundWindow(),StrRes[IDS_INVALIDNUMBEROFPLANES],StrRes[IDS_EXPORTTITLE],MB_OK|MB_ICONSTOP);
	return;
	}
  CExportDlg dlg;
  dlg.paasave=true;
  dlg.key=GetFilenameExt((const char *)globals->exportParamBlock->filename);
  if (dlg.DoModal(IDD_EXPORTDIALOG,photoshp)==IDOK)
	{
	Picture pic;
	pic.CreateOpaque(iWidth,iHeight);

	long chunk = gStuff->maxData / gStuff->imageSize.h / gStuff->planes;
	ExportRegion region;

	region.rect.left = 0;
	region.rect.right = gStuff->imageSize.h;
	region.loPlane = 0;
	region.hiPlane = gStuff->planes - 1;
	  
	  RANGE_ITER (region.rect.top, region.rect.bottom,
				  0, gStuff->imageSize.v, chunk)
		  {
		  
		  int16 row;
  //		long rowCount = gStuff->imageSize.h * (long) gStuff->planes;
		  void *data = 0;
		  int32 rowBytes = 0;
		  unsigned8 *rowData;
		  
		  if (!TSC (TestAbort ())) return ;
			  
		  if (!TSR (FetchData (gStuff, &region, &data, &rowBytes))) return ;
		  
		  for (row = region.rect.top, rowData = (unsigned8 *) data;
			   row < region.rect.bottom;
			   ++row, rowData += rowBytes)
			  {
			  
  //			long count = rowCount;
			  
			  UpdateProgress (row, gStuff->imageSize.v);
			  
			  if (!TSC (TestAbort ())) return ;

			  for (int col=0;col<iWidth;col++)
				{
				PPixel p=0xFF000000;
				memcpy(&p,rowData+iPlanes*col,iPlanes);
				_asm 
				  {
				  mov eax,p
				  bswap eax
				  ror eax,8
				  mov p,eax
				  }			
				pic.SetPixel(col,row,p);
				}
			  
			  }
		  
		  }
  	  
	if (dlg.kenable)
	  {
	  CDialogClass dcls;
	  dcls.Create(IDD_GENINFO,photoshp);
	  ShowWindow(dcls,SW_SHOW);
	  UpdateWindow(dcls);
	  dcls.SendDlgItemMessage(IDC_PROGRESS,PBM_SETRANGE32,0,1000);
	  dcls.SendDlgItemMessage(IDC_PROGRESS,PBM_SETPOS,0,0);

	  Picture temp=pic;
	  Picture *p1=&pic,*p2=&temp;
	  DWORD kcolor;
				_asm 
				  {
				  mov eax,dlg.kcolor;
				  bswap eax
				  ror eax,8
				  mov kcolor,eax
				  }		
	int a=1;
	int max=0;
    int minrow=0,maxrow=iHeight-1,mincol=0,maxcol=iWidth-1;

	while (a)
	   {
	   a=0;
       int mr=maxrow+1;
       int mc=maxcol+1;
       int cc=mincol;
       int mm=minrow;
        minrow=iHeight,maxrow=0,mincol=iWidth,maxcol=0;
	  for (int row=mm;row<mr;row++)
		for (int col=cc;col<mc;col++)
		  {
	  	  COLORREF k=p1->GetPixelARGB(col,row);
		  if ((k & 0xFFFFFF) == kcolor)
			{
		    a++;

            if (row<minrow) minrow=row;
            if (row>maxrow) maxrow=row;
            if (col<mincol) mincol=col;
            if (col>maxcol) maxcol=col;

			int rotx[]={-1,0,1,0};
			int roty[]={0,1,0,-1};
 		    int r=0,g=0,b=0,c=0;
			for (int i=0;i<4;i++)
			  {
			  int cl=col+rotx[i];
			  int rw=row+roty[i];
			  if (cl>=0 && cl<iWidth && rw>=0 && rw<iHeight)
				{
				COLORREF p=p1->GetPixelARGB(cl,rw);
		        if ((p & 0xFFFFFF) != kcolor)
				  {
				  r+=(p>>16)&0xFF;
				  g+=(p>>8) & 0xFF;
				  b+=p & 0xFF;
				  c++;
				  }
				}
			  }
  		    if (c) 
			  {
			  r/=c;g/=c;b/=c;
			  p2->SetPixel(col,row,RGB(b,g,r));
			  }
			else ;//p2->SetPixel(col,row,k);
			}		
		  else
			p2->SetPixel(col,row,k);
		  }
	   Picture *p3=p1;p1=p2;p2=p3;
	   if (max<a) max=a;
	   dcls.SendDlgItemMessage(IDC_PROGRESS,PBM_SETPOS,(max-a)/(max/1000+1),0);
	   UpdateWindow(dcls.GetDlgItem(IDC_PROGRESS));
	   if (!TSC (TestAbort ())) return ;
	   }
	  if (p1!=&pic) pic=temp;
	  }
//	pic.DisableDithering();
if (pic.Save(dlg.filename)!=0)
	  MessageBox(GetForegroundWindow(),StrRes[IDS_SAVEPAAERROR],NULL,MB_OK);
	}
/*	if (dlg.paasave)
	  {
	  pic.ConvertARGB();
	  if (pic.IsGray()) pic.SaveMipmaps88(dlg.filename,DefHints);
	  else if (pic.IsMasked())
		{
		if (iWidth>4 && iHeight>4 && dlg.compress) pic.SaveMipmapsDXTOpaque(dlg.filename,DefHints);
		else pic.SaveMipmaps1555(dlg.filename,DefHints);
		}
	  else pic.SaveMipmaps4444(dlg.filename,DefHints);
	  }
	else
	  {
	  if (iWidth>4 && iHeight>4 && dlg.compress) pic.SaveMipmapsDXTOpaque(dlg.filename,DefHints);
	  else pic.SaveMipmaps(dlg.filename,DefHints);
	  }
	}*/
  MarkExportFinished (gStuff);
  }
 
/*
void DoStart (GPtr globals)
{
	
	
//	/* check with the scripting system whether to pop our dialog 
	
	gQueryForParameters = ReadScriptParams (globals);
	
	if (!DoUI (globals))
		return;
		
	if (!CreateExportFile (globals))
		return;
	
	WriteExportFile (globals);
	
	CloseExportFile (globals);
	
	MarkExportFinished (gStuff);

}
*/
/*****************************************************************************/

/* Filters the area and requests the next area. */

void DoContinue (GPtr globals)
{
	
	/* We shouldn't get here because we did all of the work during the
	   start phase, but we add some code just in case. */
	
	MarkExportFinished (gStuff);
	
	gResult = userCanceledErr;

}

/*****************************************************************************/

/* This routine will always be called if DoStart does not return an error
   (even if DoContinue returns an error or the user aborts the operation).
   This allows the module to perform any needed cleanup.  None is required
   in this example. */

void DoFinish (GPtr globals)
{
	WriteScriptParams (globals);
}

/*****************************************************************************/


Boolean WriteExportFile (GPtr globals)
{
	/* We write out the file as an interleaved raw file. */ 
	
	/* We need to figure out how many rows to write at one time. */
	
	long chunk = gStuff->maxData / gStuff->imageSize.h / gStuff->planes;
	
	ExportRegion region;
	
	region.rect.left = 0;
	region.rect.right = gStuff->imageSize.h;
	region.loPlane = 0;
	region.hiPlane = gStuff->planes - 1;
	
	RANGE_ITER (region.rect.top, region.rect.bottom,
				0, gStuff->imageSize.v, chunk)
		{
		
		int16 row;
		long rowCount = gStuff->imageSize.h * (long) gStuff->planes;
		void *data = 0;
		int32 rowBytes = 0;
		unsigned8 *rowData;
		
		if (!TSC (TestAbort ())) return FALSE;
			
		if (!TSR (FetchData (gStuff, &region, &data, &rowBytes))) return FALSE;
		
		for (row = region.rect.top, rowData = (unsigned8 *) data;
			 row < region.rect.bottom;
			 ++row, rowData += rowBytes)
			{
			
			long count = rowCount;
			
			UpdateProgress (row, gStuff->imageSize.v);
			
			if (!TSC (TestAbort ())) return FALSE;
			
			if (!TSR (FSWrite (gFRefNum, &count, rowData))) return FALSE;
			
			}
		
		}

		
	return TRUE;
	
}

/*****************************************************************************/

OSErr FetchData (ExportRecord *stuff,
				 ExportRegion *region,
				 void **data,
				 int32 *rowBytes)
{
	
	OSErr result;
	
	if (!WarnHostAdvanceStateAvailable (stuff->advanceState, hDllInstance))
		return userCanceledErr;
		
	stuff->theRect = region->rect;
	stuff->loPlane = region->loPlane;
	stuff->hiPlane = region->hiPlane;
	
	result = (*(stuff->advanceState)) ();
	
	if (result != noErr)
		{
		*data = NULL;
		*rowBytes = 0;
		}
	else
		{
		*data = stuff->data;
		*rowBytes = stuff->rowBytes;
		}
		
	return result;
	
}

/*****************************************************************************/

void MarkExportFinished (ExportRecord *stuff)
	{
	
	PISetRect (&stuff->theRect, 0, 0, 0, 0);
	
	}

//-------------------------------------------------------------------------------

// end Outbound.c
