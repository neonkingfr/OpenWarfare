// StringRes.cpp: implementation of the CStringRes class.
//
//////////////////////////////////////////////////////////////////////

#include "StringRes.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CStringRes::CStringRes()
{

}

CStringRes::~CStringRes()
  {
  for (int i=0;i<strlist.GetSize();i++)
	{
	char *p=(char *)(strlist.GetValueAtIndex(i));
	if (p) delete [] p;
	}
  }

const char *CStringRes::operator[](int idc)
  {
  char *p=(char *)(strlist[idc]);
  if (p) return p;

  char fx[10];
  char *buff=fx;
  int size=10;
  int s=LoadString(hInst,idc,buff,size);
  while (size-	1==s)
	{
	if (buff!=fx) delete [] buff;
	size*=2;
	buff=new char[size];	  	
	s=LoadString(hInst,idc,buff,size);
	}
  int nwsz=strlen(buff)+1;
  p=new char[nwsz];
  memcpy(p,buff,nwsz);
  strlist.Add(idc,(long)p);
  if (buff!=fx) delete [] buff;

  return p;
  }

