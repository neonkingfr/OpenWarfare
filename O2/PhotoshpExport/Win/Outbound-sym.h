//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by Outbound.rc
//
#define IDS_INVALIDNUMBEROFPLANES       1
#define IDS_EXPORTTITLE                 2
#define IDS_PAAFILTER                   3
#define IDS_PAAEXT                      4
#define IDS_PACFILTER                   5
#define IDS_PACEXT                      6
#define IDS_MSG_INVALIDCONFIG           7
#define IDS_WARNING                     8
#define IDS_SAVEPAAERROR                9
#define IDS_FILTER1                     10
#define IDS_FILTER2                     11
#define IDS_FILTER3                     12
#define IDS_MSG_OLDVERSION              13
#define IDD_EXPORTDIALOG                101
#define IDD_ABOUT                       102
#define IDD_GENINFO                     103
#define IDC_EXPORTPAA                   1002
#define IDC_EXPORTPAC                   1003
#define IDC_COMPRESS                    1004
#define IDC_DYNARANGE                   1005
#define IDC_KKEY                        1006
#define IDC_COLKEY                      1007
#define IDC_OUTFILE                     1009
#define IDC_FADEOUT                     1010
#define IDC_LIMITSIZE                   1011
#define IDC_PROGRESS                    1013
#define IDC_FILTER                      1014
#define IDC_STATIC                      -1

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NO_MFC                     1
#define _APS_NEXT_RESOURCE_VALUE        104
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1015
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
