/*******************************************************************/
/*                                                                 */
/*                      ADOBE CONFIDENTIAL                         */
/*                   _ _ _ _ _ _ _ _ _ _ _ _ _                     */
/*                                                                 */
/* Copyright 1992 - 1999 Adobe Systems Incorporated                */
/* All Rights Reserved.                                            */
/*                                                                 */
/* NOTICE:  All information contained herein is, and remains the   */
/* property of Adobe Systems Incorporated and its suppliers, if    */
/* any.  The intellectual and technical concepts contained         */
/* herein are proprietary to Adobe Systems Incorporated and its    */
/* suppliers and may be covered by U.S. and Foreign Patents,       */
/* patents in process, and are protected by trade secret or        */
/* copyright law.  Dissemination of this information or            */
/* reproduction of this material is strictly forbidden unless      */
/* prior written permission is obtained from Adobe Systems         */
/* Incorporated.                                                   */
/*                                                                 */
/*******************************************************************/
//-------------------------------------------------------------------
//
//	File:
//		PIAbout.h
//
//	Distribution:
//		PUBLIC
//
//	Description:
//		This file describes version 4.0 of Photoshop's plug-in module
//		interface for the about box selector.
//
//	Use:
//		Use in all types of Photoshop plug-ins.
//
//	Version history:
//		1.0.0	10/9/1997	Ace		Initial compilation.
//		
//-------------------------------------------------------------------

#ifndef __PIAbout__
#define __PIAbout__

#include "SPBasic.h"
#include "SPMData.h"

/******************************************************************************/
/* Pragma to byte align structures; only for Borland C */

#if defined(__BORLANDC__)
#pragma option -a-
#endif

/******************************************************************************/
#define plugInSelectorAbout 	 0

typedef struct AboutRecord 
	{

	void *		platformData;		/* Platform specific information. */
	
	SPBasicSuite *sSPBasic;			/* SuitePea basic suite */
	void		*plugInRef;			/* plugin reference used by SuitePea*/

	char		reserved [244]; 	/* Set to zero */

	}
AboutRecord, *AboutRecordPtr;

/******************************************************************************/
/* turn off the byte align pragma back to its original state; only for Borland C */

#if defined(__BORLANDC__)
#pragma option -a.
#endif

/******************************************************************************/

#endif // __PIAbout_h__ 
