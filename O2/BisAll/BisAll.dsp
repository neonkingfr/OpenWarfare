# Microsoft Developer Studio Project File - Name="BisAll" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Static Library" 0x0104

CFG=BisAll - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "BisAll.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "BisAll.mak" CFG="BisAll - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "BisAll - Win32 Release" (based on "Win32 (x86) Static Library")
!MESSAGE "BisAll - Win32 Debug" (based on "Win32 (x86) Static Library")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""$/Projects/BisAll", YLRAAAAA"
# PROP Scc_LocalPath "."
CPP=cl.exe
RSC=rc.exe

!IF  "$(CFG)" == "BisAll - Win32 Release"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_MBCS" /D "_LIB" /YX /FD /c
# ADD CPP /nologo /W3 /GX /O2 /D "NDEBUG" /D "WIN32" /D "_MBCS" /D "_LIB" /D "MFC_NEW" /YX"..\..\EL\elementpch.hpp" /FD /c
# ADD BASE RSC /l 0x405 /d "NDEBUG"
# ADD RSC /l 0x405 /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LIB32=link.exe -lib
# ADD BASE LIB32 /nologo
# ADD LIB32 /nologo

!ELSEIF  "$(CFG)" == "BisAll - Win32 Debug"

# PROP BASE Use_MFC 0
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 0
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Target_Dir ""
# ADD BASE CPP /nologo /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_MBCS" /D "_LIB" /YX /FD /GZ /c
# ADD CPP /nologo /W3 /Gm /GR /GX /ZI /Od /D "_DEBUG" /D "WIN32" /D "_MBCS" /D "_LIB" /D "MFC_NEW" /YX"..\..\EL\elementpch.hpp" /FD /GZ /c
# ADD BASE RSC /l 0x405 /d "_DEBUG"
# ADD RSC /l 0x405 /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LIB32=link.exe -lib
# ADD BASE LIB32 /nologo
# ADD LIB32 /nologo

!ENDIF 

# Begin Target

# Name "BisAll - Win32 Release"
# Name "BisAll - Win32 Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=..\..\ES\Framework\appFrame.cpp
# End Source File
# Begin Source File

SOURCE=..\..\ES\Memory\debugAlloc.cpp
# End Source File
# Begin Source File

SOURCE=..\..\ES\Memory\fastAlloc.cpp
# End Source File
# Begin Source File

SOURCE=..\..\EL\QStream\fileCompress.cpp
# End Source File
# Begin Source File

SOURCE=..\..\EL\QStream\fileMapping.cpp
# End Source File
# Begin Source File

SOURCE=..\..\EL\FreeOnDemand\memFreeReq.cpp
# End Source File
# Begin Source File

SOURCE=..\..\EL\FreeOnDemand\memFreeReqUseDefault.cpp
# End Source File
# Begin Source File

SOURCE=..\..\EL\ParamFile\paramFile.cpp
# End Source File
# Begin Source File

SOURCE=..\..\EL\ParamFile\paramFileCtx.cpp
# End Source File
# Begin Source File

SOURCE=..\..\EL\ParamFile\paramFileUseEvalDefault.cpp
# End Source File
# Begin Source File

SOURCE=..\..\EL\ParamFile\paramFileUseLocalizeDefault.cpp
# End Source File
# Begin Source File

SOURCE=..\..\EL\ParamFile\paramFileUsePreprocDefault.cpp
# End Source File
# Begin Source File

SOURCE=..\..\EL\ParamFile\paramFileUseSumCalcDefault.cpp
# End Source File
# Begin Source File

SOURCE=..\..\EL\QStream\qbstream.cpp
# End Source File
# Begin Source File

SOURCE=..\..\EL\QStream\qstream.cpp
# End Source File
# Begin Source File

SOURCE=..\..\EL\QStream\qstreamUseBankDefault.cpp
# End Source File
# Begin Source File

SOURCE=..\..\EL\QStream\qstreamUseFServerDefault.cpp
# End Source File
# Begin Source File

SOURCE=..\..\ES\Strings\rString.cpp
# End Source File
# Begin Source File

SOURCE=..\..\EL\QStream\serializeBin.cpp
# End Source File
# Begin Source File

SOURCE=..\..\EL\QStream\ssCompress.cpp
# End Source File
# Begin Source File

SOURCE=..\..\ES\Framework\useAppFrameDefault.cpp
# End Source File
# Begin Source File

SOURCE=..\..\EL\AppInfo\useAppInfoDefault.cpp
# End Source File
# Begin Source File

SOURCE=..\..\es\memory\useMallocMemFunctions.cpp
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=..\..\ES\Framework\appFrame.hpp
# End Source File
# Begin Source File

SOURCE=..\..\ES\Containers\array.hpp
# End Source File
# Begin Source File

SOURCE=..\..\ES\Containers\bankArray.hpp
# End Source File
# Begin Source File

SOURCE=..\..\ES\Strings\bstring.hpp
# End Source File
# Begin Source File

SOURCE=..\..\ES\Memory\checkMem.hpp
# End Source File
# Begin Source File

SOURCE=..\..\ES\Containers\compactBuf.hpp
# End Source File
# Begin Source File

SOURCE=..\..\EL\CorelAnalys\corelAnalys.hpp
# End Source File
# Begin Source File

SOURCE=..\..\ES\Memory\debugAlloc.hpp
# End Source File
# Begin Source File

SOURCE=..\..\ES\Framework\debugLog.hpp
# End Source File
# Begin Source File

SOURCE=..\..\ES\Memory\debugNew.hpp
# End Source File
# Begin Source File

SOURCE=..\..\EL\elementpch.hpp
# End Source File
# Begin Source File

SOURCE=..\..\ES\Types\enum_decl.hpp
# End Source File
# Begin Source File

SOURCE=..\..\ES\essencepch.hpp
# End Source File
# Begin Source File

SOURCE=..\..\EL\PCH\ext_options.hpp
# End Source File
# Begin Source File

SOURCE=..\..\ES\Memory\fastAlloc.hpp
# End Source File
# Begin Source File

SOURCE=..\..\EL\QStream\fileCompress.hpp
# End Source File
# Begin Source File

SOURCE=..\..\EL\QStream\fileinfo.h
# End Source File
# Begin Source File

SOURCE=..\..\EL\QStream\fileMapping.hpp
# End Source File
# Begin Source File

SOURCE=..\..\ES\Files\filenames.hpp
# End Source File
# Begin Source File

SOURCE=..\..\ES\Common\fltopts.hpp
# End Source File
# Begin Source File

SOURCE=..\..\ES\Containers\hashMap.hpp
# End Source File
# Begin Source File

SOURCE=..\..\EL\Interfaces\iAppInfo.hpp
# End Source File
# Begin Source File

SOURCE=..\..\EL\Interfaces\iEval.hpp
# End Source File
# Begin Source File

SOURCE=..\..\EL\Interfaces\iLocalize.hpp
# End Source File
# Begin Source File

SOURCE=..\..\EL\Interfaces\iPreproc.hpp
# End Source File
# Begin Source File

SOURCE=..\..\EL\QStream\iQFBank.hpp
# End Source File
# Begin Source File

SOURCE=..\..\EL\Interfaces\iSumCalc.hpp
# End Source File
# Begin Source File

SOURCE=..\..\ES\Common\langExt.hpp
# End Source File
# Begin Source File

SOURCE=..\..\EL\PCH\libIncludes.hpp
# End Source File
# Begin Source File

SOURCE=..\..\ES\Containers\list.hpp
# End Source File
# Begin Source File

SOURCE=..\..\ES\Containers\listBidir.hpp
# End Source File
# Begin Source File

SOURCE=..\..\ES\Types\lLinks.hpp
# End Source File
# Begin Source File

SOURCE=..\Objektiv2.5\MeshGenProject\math3dp.hpp
# End Source File
# Begin Source File

SOURCE=..\Objektiv2.5\MeshGenProject\mathdefs.hpp
# End Source File
# Begin Source File

SOURCE=..\Objektiv2.5\MeshGenProject\mathopt.hpp
# End Source File
# Begin Source File

SOURCE=..\..\ES\Memory\memAlloc.hpp
# End Source File
# Begin Source File

SOURCE=..\..\EL\FreeOnDemand\memFreeReq.hpp
# End Source File
# Begin Source File

SOURCE=..\..\ES\Types\memtype.h
# End Source File
# Begin Source File

SOURCE=..\..\EL\PCH\normalConfig.h
# End Source File
# Begin Source File

SOURCE=..\..\ES\Memory\normalNew.hpp
# End Source File
# Begin Source File

SOURCE=..\..\EL\ParamFile\paramFile.hpp
# End Source File
# Begin Source File

SOURCE=..\..\EL\ParamFile\paramFileCtx.hpp
# End Source File
# Begin Source File

SOURCE=..\..\EL\ParamFile\paramFileDecl.hpp
# End Source File
# Begin Source File

SOURCE=..\..\EL\Common\perfLog.hpp
# End Source File
# Begin Source File

SOURCE=..\..\ES\platform.hpp
# End Source File
# Begin Source File

SOURCE=..\..\ES\Types\pointers.hpp
# End Source File
# Begin Source File

SOURCE=..\..\EL\QStream\QBStream.hpp
# End Source File
# Begin Source File

SOURCE=..\..\ES\Algorithms\qsort.hpp
# End Source File
# Begin Source File

SOURCE=..\..\EL\QStream\QStream.hpp
# End Source File
# Begin Source File

SOURCE=..\..\ES\Types\removeLinks.hpp
# End Source File
# Begin Source File

SOURCE=..\..\ES\Strings\rString.hpp
# End Source File
# Begin Source File

SOURCE=..\..\ES\Containers\rStringArray.hpp
# End Source File
# Begin Source File

SOURCE=..\..\EL\QStream\serializeBin.hpp
# End Source File
# Begin Source File

SOURCE=..\..\ES\Types\softLinks.hpp
# End Source File
# Begin Source File

SOURCE=..\..\ES\Containers\staticArray.hpp
# End Source File
# Begin Source File

SOURCE=..\..\EL\PCH\stdIncludes.h
# End Source File
# Begin Source File

SOURCE=..\..\ES\Containers\typeDefines.hpp
# End Source File
# Begin Source File

SOURCE=..\..\ES\Containers\typeOpts.hpp
# End Source File
# Begin Source File

SOURCE=..\..\ES\Common\win.h
# End Source File
# End Group
# End Target
# End Project
