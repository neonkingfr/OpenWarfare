#include "Common.hpp"
#include "LODObject.h"
#include "ObjectData.h"
#include "BiFXTImport.h"
#include "ObjToolProxy.h"

#include <el/math/math3d.hpp>

namespace ObjektivLib {

bool CBiFXTImport::Import(const char * _txtName,LODObject * into)
{
  ObjectData *objTarget=into->Active();

  FILE *f = fopen(_txtName,"r");
  if (!f)
    return false;

  Matrix3 transFromMaxCoord1 = M3Zero;

  transFromMaxCoord1(0,0) = 1;
  transFromMaxCoord1(1,2) = 1;
  transFromMaxCoord1(2,1) = 1; 

  while(!feof(f))
  {
    char  name[256];
    char  path[256];
   
    Vector3 pos;
    Vector3 dir;
    Vector3 up;
    Vector3 aside;
    Vector3 pos2; 

    if (17 != fscanf(f,"%s %s %f %f %f %f %f %f %f %f %f %f %f %f %f %f %f",
      name, path, 
      &(pos[0]), &(pos[2]), &(pos[1]), 
      &(aside[0]), &(aside[1]), &(aside[2]),
      &(up[0]), &(up[1]), &(up[2]),
      &(dir[0]), &(dir[1]), &(dir[2]),  
      &(pos2[0]), &(pos2[1]), &(pos2[2])))
      break;    

    Matrix4 trans = M4Zero;
    trans(0,0) = aside[0];
    trans(0,1) = aside[2];
    trans(0,2) = aside[1];

    trans(1,0) = dir[0];
    trans(1,1) = dir[2];
    trans(1,2) = dir[1];

    trans(2,0) = up[0];
    trans(2,1) = up[2];
    trans(2,2) = up[1];

    trans.SetPosition(pos/1000);
    objTarget->GetTool<ObjToolProxy>().CreateProxy(path + 2, trans);// TODO somehow better path
  }

  fclose(f);
  return true;
}

}