// Interface for FBX.dll FBX import/export

#include "..\common.hpp"
#include "..\LODObject.h"
#include "gdLODObject.h"
#include "GdFBXplugin.h"

#include <es/strings/rString.hpp>

using namespace ObjektivLib;
#define Category "O2Scripts::FBXplugin"

typedef bool (__cdecl*FBXDLL_EXPORTPROC)(LODObject *p3d, const char* filename, const char *flags, float scale, int animFPS);	// "exportLODobjectToFBX" in fbx.dll
typedef bool (__cdecl*FBXDLL_IMPORTPROC)(LODObject *p3d, const char* filename, const char *flags, float scale, int animFPS);	// "exportLODobjectToFBX" in fbx.dll

static HINSTANCE FBXdll = NULL;
static FBXDLL_EXPORTPROC FBXexport;
static FBXDLL_IMPORTPROC FBXimport;

// Loads the FBX dll
static bool initFBX() {
	if(FBXdll)
		return true;
	FBXdll = LoadLibrary("FBX.dll");
	if(!FBXdll) {
		RptF("Error: Cannot load FBX.dll");
		return false;
	}
	
	FBXexport = (FBXDLL_EXPORTPROC) GetProcAddress(FBXdll, "exportLODobjectToFBX");
	FBXimport = (FBXDLL_IMPORTPROC) GetProcAddress(FBXdll, "importFBXtoLODobject");
	if(!FBXexport || !FBXimport) {
		RptF("Error: exportLODobjectToFBX/importFBXtoLODobject export not found in FBX.dll");
		FreeLibrary(FBXdll);
		FBXdll = NULL;
		return false;
	}
	return true;
}

static GameValue exportFBX(const GameState *gs, GameValuePar oper1, GameValuePar oper2) {
	if(!initFBX())
		return false;

	/* possible flags:
	allLods, currentLod, currentSelection
	skinSkipOrdinary, noSkinSkipOrdinary
	noSkinning, liteSkinning, fullSkinning
	textures, noTextures
	splitObjects, noSplitObjects
	reconstructNames, noReconstructNames
	exportEdges, noExportEdges
	exportNormals, noExportNormals
	formatASCII, formatBinary
	extractRVMATref, noExtractRVMATref
	extractRVMAT, noExtractRVMAT
	*/

	const GameArrayType &arr = oper2;
	if(arr.Size() < 3) return false;
	RString filename = arr[0];
	RString flags = arr[1];
	float scale = arr[2];
	int animFPS = toInt((float)arr[3]);

	Ref<LODObjectRef>lodobj = static_cast<GdLODObject *>(oper1.GetData())->GetObject();
	bool r = FBXexport(lodobj->_ref, filename.Data(), flags.Data(), scale, animFPS);
	return r;
}

static GameValue importFBX(const GameState *gs, GameValuePar oper1, GameValuePar oper2) {
	if(!initFBX())
		return false;
	
	const GameArrayType &arr = oper2;
	if(arr.Size() < 3) return false;
	RString filename = arr[0];
	RString flags = arr[1];
	float scale = arr[2];
	int animFPS = toInt((float)arr[3]);

	Ref<LODObjectRef>lodobj = static_cast<GdLODObject *>(oper1.GetData())->GetObject();
	bool r = FBXimport(lodobj->_ref, filename.Data(), flags.Data(), scale, animFPS);
	return r;
}

#define OPERATORS_DEFAULT(XX, Category) \
  XX(GameBool,"exportFBX", function, exportFBX, EvalType_LODObject, GameArray,"p3d","[filename, flags, scale, animation FPS]", "Exports LODObject as FBX file. Requires FBX.dll.\nFlags is comma separated string with the following:\nallLods, currentLod, currentSelection (Default: allLods)\nnoSkinning, liteSkinning, fullSkinning (Default: noSkinning)\nskinSkipOrdinary, noTextures, noSplitObjects, noReconstructNames, noExportEdges, noExportNormals, formatASCII, extractRVMATref, noExtractRVMAT", "_p3d = newLODObject;_p3d loadP3D \"box.p3d\";_p3d exportFBX[\"box.fbx\", \"formatASCII, noSkinning, noTextures\", 1.0, 0];", "true, if file has been successfully exported. false, if there was an error. ", "", "", Category) \
  XX(GameBool,"importFBX", function, importFBX, EvalType_LODObject, GameArray,"p3d","[filename, flags, scale, animation FPS]", "Loads (imports) FBX file into LODObject. Requires FBX.dll. Flags is comma separated string with the following (By default all properties are imported): noMesh, noTextures, noUVSets, noSmoothing, noSkinning, noLODs, noRVMATRefs, noSkeleton, noAnimation, DontCreateObjSel", "_p3d = newLODObject;_p3d importFBX[\"model.FBX\", \"noSmoothing, noAnimation\", 0.01, 0];", "true, if file has been successfully parsed and imported. false, if there was an error.", "", "", Category) \

static GameOperator BinaryFuncts[]=
{
  OPERATORS_DEFAULT(REGISTER_OPERATOR, Category)
};

#if DOCUMENT_COMREF
static ComRefFunc DefaultComRefFunc[] =
{
  OPERATORS_DEFAULT(COMREF_OPERATOR, Category)
};
#endif

void GdFBXplugin::RegisterToGameState(GameState *gState)
{
  gState->NewOperators(BinaryFuncts,lenof(BinaryFuncts));
#if DOCUMENT_COMREF
  gState->AddComRefFunctions(DefaultComRefFunc,lenof(DefaultComRefFunc));

#endif
}