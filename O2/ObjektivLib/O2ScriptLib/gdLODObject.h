#pragma once
#include "GdObjectData.h"

#define TYPES_LOD_OBJECT(XX, Category) \
  XX("LODObject",EvalType_LODObject,CreateLodObject,"@LODObject","LODObject","This type represents whole P3D object. It is set of levels which is organised same way like in Objektiv2",Category)

TYPES_LOD_OBJECT(DECLARE_TYPE, "Objektiv")

class GdLODObject : public GameData
{
public:
  Ref<LODObjectRef> _lodobj;
  Pathname _objname;    

  GdLODObject(void);
  GdLODObject(LODObjectRef *object);
  GdLODObject(LODObjectRef *object, const Pathname &name);
  GdLODObject(const GdLODObject &other);

  const GameType &GetType() const {return EvalType_LODObject;}
  RString GetText() const;

  virtual GameBoolType GetBool() const {return _objname.IsNull()!=0;}
  virtual GameScalarType GetScalar() const {return (float)_lodobj->_ref->NLevels();}
  virtual GameStringType GetString() const {return GameStringType(_objname.GetFullPath());}
/*  virtual const GameArrayType &GetArray() const;*/

  bool IsEqualTo(const GameData *data) const;
  const char *GetTypeName() const {return "LODObject";}
  GameData *Clone() const {return new GdLODObject (*this);}
  Ref<LODObjectRef> GetObject() {return _lodobj;}
  USE_FAST_ALLOCATOR;

  static void RegisterToGameState(GameState *gState);
};

