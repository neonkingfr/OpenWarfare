#include "..\common.hpp"
#include "..\LODObject.h"
#include "..\ObjectData.h"
#include "gdLODObject.h"
#include "gdObjectData.h"
#include "gdSelection.h"
#include "gdObjToolJoinProxy.h"
#include "..\ObjToolJoinProxy.h"
#include "..\ObjToolTopology.h"



#define Category "O2Scripts::ObjToolJoinProxy"

#define GET_OBJECTDATA(oper) (static_cast<GdObjectData *>(oper.GetData())->_object)
#define GET_FACE(oper) FaceT(static_cast<GdFaceT *>(oper.GetData())->_object,static_cast<GdFaceT *>(oper.GetData())->_face)
#define GET_SELECTION(oper) (static_cast<GdSelection *>(oper.GetData())->_selection)

static GameValue getProxiesFromSelection(const GameState *gs, GameValuePar oper1)
{
  SelectionRef *sel1=GET_SELECTION(oper1);
  const ObjektivLib::ObjectData *owner=sel1->GetOwner();

  int nProxies = owner->GetTool<ObjektivLib::ObjToolJoinProxy>().EnumProxiesFromSelection(sel1, NULL);
  if (nProxies == 0)
    return GameValue(GameArrayType());

  ObjektivLib::NamedSelection ** list = new ObjektivLib::NamedSelection*[nProxies];
  owner->GetTool<ObjektivLib::ObjToolJoinProxy>().EnumProxiesFromSelection(sel1, list);

  GameArrayType arr;
  for (int i=0;i<nProxies;i++)
  {    
    arr.Append()=list[i]->Name();
  }

  return arr;
}

static GameValue modelFromProxies(const GameState *gs, GameValuePar oper1, GameValuePar oper2)
{
  ObjektivLib::ObjectData * lod = GET_OBJECTDATA(oper1);
  const GameArrayType &arr=oper2;  
  
  if (arr.Size() < 2)
    return GameValue((GameScalarType) NULL);

  const GameArrayType &proxies=arr[0];  

  ObjektivLib::NamedSelection ** list = new ObjektivLib::NamedSelection*[proxies.Size()];
  for(int i = 0; i < proxies.Size(); i++)
  {
    list[i] = lod->GetNamedSel((GameStringType)proxies[i]);
  }

  Matrix4 resTrans;
  GdLODObject * out = new GdLODObject();
  lod->GetTool<ObjektivLib::ObjToolJoinProxy>().CreateModelFromProxies(*(out->GetObject()->_ref), list, proxies.Size(), resTrans,(GameStringType) arr[1], (arr.Size() > 2) ? (GameBoolType) arr[2] : true,(arr.Size() > 3) ? (GameBoolType) arr[3] : false);

  return GameValue(out);
}

static GameValue createProxiesGroup(const GameState *gs, GameValuePar oper1, GameValuePar oper2)
{
  ObjektivLib::ObjectData * lod = GET_OBJECTDATA(oper1);
  const GameArrayType &arr=oper2; 
  if (arr.Size() < 3)
    return GameValue((GameScalarType) 0);

  return (GameScalarType) lod->GetTool<ObjektivLib::ObjToolJoinProxy>().CreateProxiesGroup((GameStringType)arr[0],(int) (GameScalarType) arr[1], (int) (GameScalarType) arr[2]);
}

static GameValue replaceGroupsByProxies(const GameState *gs, GameValuePar oper1, GameValuePar oper2)
{
  ObjektivLib::ObjectData * lod = GET_OBJECTDATA(oper1);
  const GameArrayType &arr=oper2; 
  if (arr.Size() < 2)
    return GameValue(GameArrayType());

  AutoArray<RString> out; 
  lod->GetTool<ObjektivLib::ObjToolJoinProxy>().ReplaceGroupsByProxies(out,(GameStringType)arr[0],(GameStringType)arr[1]);  

  GameArrayType result;
  result.Reserve(out.Size(),out.Size());
  for(int i = 0; i < out.Size(); i++)
    result.Add(out[i]);

  return result;
}

static GameValue cleanDoublePoly(const GameState *gs, GameValuePar oper1)
{
  ObjektivLib::ObjectData * lod = GET_OBJECTDATA(oper1);    
  ObjektivLib::Selection sel(lod);

  lod->GetTool<ObjektivLib::ObjToolTopology>().SelectDoublePolygons(lod->GetSelection(),sel);
  lod->SelectionDelete(&sel);
  return GameValue(true);
}

static GameValue selectDoublePoly(const GameState *gs, GameValuePar oper1)
{
  SelectionRef *sel1=GET_SELECTION(oper1);

  sel1->GetOwner()->GetTool<ObjektivLib::ObjToolTopology>().SelectDoublePolygons(sel1,*sel1);
  return GameValue(true);
}

class FilterTexture
{
protected:
  const char * _name;
public:
  FilterTexture(const char * name) : _name(name) {};
  bool operator()(const ObjektivLib::O2Polygon& pol0, 
                const ObjektivLib::O2Polygon& pol1) const
  {
    return (strstr(pol0._textureName, _name) != NULL && strstr(pol1._textureName, _name) != NULL);
  }

  unsigned long  FindPolyFlags() const {return ObjektivLib::ObjToolTopology::fpSameTextue | ObjektivLib::ObjToolTopology::fpSameMaterial 
    | ObjektivLib::ObjToolTopology::fpSameMapping | ObjektivLib::ObjToolTopology::fpPrecisionMedium | ObjektivLib::ObjToolTopology::fpSameFlags;};  
};
static GameValue selectDoublePolyWithTexture(const GameState *gs, GameValuePar oper1, GameValuePar oper2)
{
  SelectionRef *sel1=GET_SELECTION(oper1);

  FilterTexture f((GameStringType) oper2);
  sel1->GetOwner()->GetTool<ObjektivLib::ObjToolTopology>().SelectDoublePolygons(sel1,*sel1,f);
  return GameValue();
}

static GameValue simplifyPoly(const GameState *gs, GameValuePar oper1)
{
  ObjektivLib::ObjectData * lod = GET_OBJECTDATA(oper1);    

  lod->GetTool<ObjektivLib::ObjToolTopology>().RetesellatePolygons();
  return GameValue(true);
}

static GameValue UVInto01(const GameState *gs, GameValuePar oper1)
{
  ObjektivLib::ObjectData * lod = GET_OBJECTDATA(oper1);    

  lod->GetTool<ObjektivLib::ObjToolTopology>().MoveUVInInterval01();
  return GameValue(true);
}

static GameValue UVFromPoly(const GameState *gs, GameValuePar oper1)
{
  ObjektivLib::ObjectData * lod = GET_OBJECTDATA(oper1);    

  lod->GetTool<ObjektivLib::ObjToolTopology>().MoveUVInPolygons();
  return GameValue(true);
}

static GameValue materialChanges(const GameState *gs, GameValuePar oper1, GameValuePar oper2)
{
  ObjektivLib::ObjectData * lod = GET_OBJECTDATA(oper1);
  ObjektivLib::ObjectData * lodPattern = GET_OBJECTDATA(oper2);

  FindArray<ObjektivLib::UnigueMaterial> from;
  FindArray<ObjektivLib::UnigueMaterial> to;
  lod->GetTool<ObjektivLib::ObjToolJoinProxy>().FindMaterialChanges(from,to,*lodPattern);

  GameArrayType out;
  for(int i = 0; i < from.Size(); i++)
  {
    GameArrayType item;
    item.Append() = from[i]._texture;
    item.Append() = from[i]._material;
    item.Append() = to[i]._texture;
    item.Append() = to[i]._material;

    out.Append() = item;
  }
  
  return out;
}

static GameValue instParamFile(const GameState *gs, GameValuePar oper1, GameValuePar oper2)
{
  ObjektivLib::ObjectData * lod = GET_OBJECTDATA(oper1);
  const GameArrayType &arr=oper2;  

  if (arr.Size() < 4)
    return GameValue(false);

  const GameArrayType &proxies=arr[2];  

  ObjektivLib::NamedSelection ** list = new ObjektivLib::NamedSelection*[proxies.Size()];
  for(int i = 0; i < proxies.Size(); i++)
  {
    list[i] = lod->GetNamedSel((GameStringType)proxies[i]);
  }
  
  lod->GetTool<ObjektivLib::ObjToolJoinProxy>().SaveInstParamFile((GameStringType) arr[0], *GET_OBJECTDATA(arr[1]),  list, proxies.Size(), (GameStringType) arr[3]);   

  return GameValue(true);
}

static GameValue instPartParamFile(const GameState *gs, GameValuePar oper2)
{
  const GameArrayType &arr=oper2;  

  if (arr.Size() < 3)
    return GameValue(false);  

  //lod->GetTool<ObjektivLib::ObjToolJoinProxy>().SavePartInstParamFile((GameStringType) arr[0], (GameStringType) arr[1],  (GameStringType) arr[2]);   
  ObjektivLib::ObjToolJoinProxy::SavePartInstParamFile((GameStringType) arr[0], (GameStringType) arr[1],  (GameStringType) arr[2]); 

  return GameValue(true);
}

static GameValue breakableHouse(const GameState *gs, GameValuePar oper1, GameValuePar oper2)
{  
  ObjektivLib::ObjectData * lod = GET_OBJECTDATA(oper1);
  const GameArrayType &arr=oper2;  

  if (arr.Size() < 3)
    return GameValue(GameArrayType());  

  AutoArray<RString> out; 
  lod->GetTool<ObjektivLib::ObjToolJoinProxy>().BreakableHouse(out,(GameStringType) arr[0], (GameStringType) arr[1], (GameStringType) arr[2]);

  GameArrayType result;
  result.Reserve(out.Size(),out.Size());
  for(int i = 0; i < out.Size(); i++)
    result.Add(out[i]);

  return result;
}

static GameValue unbreakableHouse(const GameState *gs, GameValuePar oper1)
{   
  const GameArrayType &arr=oper1;  

  if (arr.Size() < 2)
    return GameValue();  

  GdLODObject * out = new GdLODObject();
  ObjektivLib::ObjToolJoinProxy::UnbreakableHouse(*(out->GetObject()->_ref),(GameStringType) arr[0], (GameStringType) arr[1]);
 
  return GameValue(out);
}

static GameValue centerProxies(const GameState *gs, GameValuePar oper1, GameValuePar oper2)
{
  ObjektivLib::ObjectData * lod = GET_OBJECTDATA(oper1);
  const GameArrayType &arr=oper2;  

  if (arr.Size() < 2)
    return GameValue(false);

  const GameArrayType &proxies=arr[0];  

  ObjektivLib::NamedSelection ** list = new ObjektivLib::NamedSelection*[proxies.Size()];
  for(int i = 0; i < proxies.Size(); i++)
  {
    list[i] = lod->GetNamedSel((GameStringType)proxies[i]);
  }

  lod->GetTool<ObjektivLib::ObjToolJoinProxy>().CenterProxies( list, proxies.Size(), (GameStringType) arr[1]);   

  return GameValue(true);
}

static GameValue shadowLod(const GameState *gs, GameValuePar oper1, GameValuePar oper2)
{   
  ObjektivLib::ObjectData * lod = GET_OBJECTDATA(oper1);

  lod->GetTool<ObjektivLib::ObjToolJoinProxy>().ShadowLod((GameBoolType) oper2);

  return GameValue();
}


#define OPERATORS_DEFAULT(XX, Category) \
  XX(EvalType_LODObject, "createLODObject",function, modelFromProxies,EvalType_ObjectData, GameArray, "object","[proxies, project root, allow shift, make selections]", "Creates new LODObject, by merging all proxies specified by their names. The result object will be created in the coordinate system of the object owning proxies if \"allow shift\" is false. Otherwise the result object will be created in the coordinate system of the first proxy. If \"make selection\" is true, the content of each proxy will in separate selection in the result model (the names of created selections corresponds to the proxy filenames). " , "_object createLODObject [[\"proxy:\\model.01\",\"proxy:\\model.02\"], \"P:\\\", false];", "created LODObject", "", "", Category) \
  XX(GameScalar, "createProxyGroups",function, createProxiesGroup,EvalType_ObjectData, GameArray, "object","[group name,min members in group, max members in group]", "Creates selections containing currently selected proxies. Those selections are called groups. Functions tries to create compact groups, where proxies in group are close to each other. ", "_object createProxyGroups [\"-sbp.group\",5,10];", "number of created groups", "", "", Category) \
  XX(GameArray, "createProxiesFromGroups",function, replaceGroupsByProxies,EvalType_ObjectData, GameArray, "object","[group prefix, project root]", "Creates LODObjects by merging proxies in groups (a named selections containing proxies). Each group is replaced by the proxy referencing to the created LODObjects. The LODObjects are saved into file defined by \"project root\" and the group name without the group prefix path. Only groups with name starting with \"group prefix\" are processed.", "_object createProxiesFromGroups [\"-sbp.\",\"P:\\\"];", "true, if no error", "", "", Category) \
  XX(GameBool, "materialChanges",function, materialChanges,EvalType_ObjectData, EvalType_ObjectData, "object","[group prefix, project root]", "Creates LODObjects by merging proxies in groups (a named selections containing proxies). Each group is replaced by the proxy referencing to the created LODObjects. The LODObjects are saved into file defined by \"project root\" and the group name without the group prefix path. Only groups with name starting with \"group prefix\" are processed.", "_object createProxiesFromGroups [\"-sbp.\",\"P:\\\"];", "true, if no error", "", "", Category) \
  XX(GameBool, "instParamFile",function, instParamFile,EvalType_ObjectData, GameArray, "object","[group prefix, project root]", "Creates LODObjects by merging proxies in groups (a named selections containing proxies). Each group is replaced by the proxy referencing to the created LODObjects. The LODObjects are saved into file defined by \"project root\" and the group name without the group prefix path. Only groups with name starting with \"group prefix\" are processed.", "_object createProxiesFromGroups [\"-sbp.\",\"P:\\\"];", "true, if no error", "", "", Category) \
  XX(GameBool, "centerProxies",function, centerProxies,EvalType_ObjectData, GameArray, "object","[proxies, project root]", "Centers models referenced by proxies and shifts proxies to compensate the centralization.", "_object centerProxies [[\"proxy:\\model.01\",\"proxy:\\model.02\"], \"P:\\\"];", "true, if no error", "", "", Category) \
  XX(GameArray, "breakableHouse",function, breakableHouse,EvalType_ObjectData, GameArray, "object","[group prefix, project root]", "Creates LODObjects by merging proxies in groups (a named selections containing proxies). Each group is replaced by the proxy referencing to the created LODObjects. The LODObjects are saved into file defined by \"project root\" and the group name without the group prefix path. Only groups with name starting with \"group prefix\" are processed.", "_object createProxiesFromGroups [\"-sbp.\",\"P:\\\"];", "true, if no error", "", "", Category) \
  XX(GameNothing, "shadowLod", function, shadowLod, EvalType_ObjectData,GameBool, "object", "split components", "Optimalizates shadow lod be joining components and retesellating faces.", "_level shadowLod true;", "", "", "", Category) \
  XX(GameNothing, "selectDoublePolyWithTexture", function, selectDoublePolyWithTexture, EvalType_Selection,GameString, "selecton", "texture name fragment.", "Selects all double polygons in selection (polygons are chosen without respect to mapping), thats textures on both sides contains given name fragment.", "_sel selectDoublePolyWithTexture \"_breakwall_\";", "", "", "", Category)

#define FUNCTIONS_DEFAULT(XX, Category) \
  XX(GameArray, "getProxies", getProxiesFromSelection, EvalType_Selection, "selection", "Returns array with the names of proxies selected by the selection.", "getProxies _sel;", "array with name of proxies selected by the selection", "", "", Category) \
  XX(GameBool, "cleanDoublePoly", cleanDoublePoly, EvalType_ObjectData, "object", "Removes all currently selected double polygons (polygons are chosen without respect to mapping). ", "cleanDoublePoly _level;", "true, if no error", "", "", Category) \
  XX(GameNothing, "selectDoublePoly", selectDoublePoly, EvalType_Selection, "selection", "Selects all double polygons in selection (polygons are chosen without respect to mapping). ", "selectDoublePoly _sel;", "", "", "", Category) \
  XX(GameBool, "simplifyPoly", simplifyPoly, EvalType_ObjectData, "object", "Re-tesellates all currently selected regions. It respects mapping and polygons are simplified by removing of the unnecessary points.", "simplifyPoly _level;", "true, if no error", "", "", Category) \
  XX(GameBool, "UVInto01", UVInto01, EvalType_ObjectData, "object", "Shifts UV on selected faces. UVs are shifted by integer number so that minimum for U or V is in interval &lt;0,1).", "UVInto01 _level;", "true, if no error", "", "", Category) \
  XX(GameBool, "instPartParamFile",instPartParamFile,GameArray,"[group prefix, project root]", "Creates LODObjects by merging proxies in groups (a named selections containing proxies). Each group is replaced by the proxy referencing to the created LODObjects. The LODObjects are saved into file defined by \"project root\" and the group name without the group prefix path. Only groups with name starting with \"group prefix\" are processed.", "_object createProxiesFromGroups [\"-sbp.\",\"P:\\\"];", "true, if no error", "", "", Category) \
  XX(GameBool, "UVFromPoly", UVFromPoly, EvalType_ObjectData, "object", "Shifts UV on selected faces. UVs are shifted by integer number. It tries to build the largest polygon with continuous UV.", "UVFromPoly _level;", "true, if no error", "", "", Category) \
  XX(EvalType_LODObject, "unbreakableHouse", unbreakableHouse, GameArray, "object", "Shifts UV on selected faces. UVs are shifted by integer number. It tries to build the largest polygon with continuous UV.", "UVFromPoly _level;", "true, if no error", "", "", Category)
  

static GameOperator BinaryFuncts[]=
{
  OPERATORS_DEFAULT(REGISTER_OPERATOR, Category)
};

static GameFunction UnaryFuncts[]=
{
  FUNCTIONS_DEFAULT(REGISTER_FUNCTION, Category)
};

#if DOCUMENT_COMREF
static ComRefFunc DefaultComRefFunc[] =
{
  FUNCTIONS_DEFAULT(COMREF_FUNCTION, Category)
  OPERATORS_DEFAULT(COMREF_OPERATOR, Category)
};

#endif


void GdObjToolJoinProxy::RegisterToGameState(GameState *gState)
{

  gState->NewOperators(BinaryFuncts,lenof(BinaryFuncts));
  gState->NewFunctions(UnaryFuncts,lenof(UnaryFuncts));

#if DOCUMENT_COMREF
  gState->AddComRefFunctions(DefaultComRefFunc,lenof(DefaultComRefFunc));
#endif
}



