#pragma once  
#include <El\Evaluator\express.hpp>
#include <es\types\pointers.hpp>
#include <El\Pathname\Pathname.h>
#include "GdObjectData.h"
#include "GdLODObject.h"

#define TYPES_SELECTION(XX, Category) \
  XX("Selection",EvalType_Selection,CreateSelection,"@Selection","Selection","This object represents general selection on mesh",Category)\

TYPES_SELECTION(DECLARE_TYPE, Category)

class SelectionRef:public ObjektivLib::Selection, public RefCount
{
public:
  SelectionRef(ObjektivLib::ObjectData *owner):ObjektivLib::Selection(owner) {}
  SelectionRef(const ObjektivLib::Selection &src, ObjektivLib::ObjectData *owner):ObjektivLib::Selection(src,owner) {}
};


class GdSelection :  public GameData
{
public:
  Ref<SelectionRef> _selection;  
  Ref<LODObjectRef> _owner;
  RString _name;

  GdSelection(void);
  GdSelection(SelectionRef *sel, LODObjectRef *owner);
  GdSelection(const ObjektivLib::Selection *sel, LODObjectRef *owner);
  GdSelection(const GdSelection &other);

  const GameType &GetType() const {return EvalType_Selection;}
  RString GetText() const;

  bool IsEqualTo(const GameData *data) const;

  const char *GetTypeName() const {return "Selection";}
  GameData *Clone() const {return new GdSelection (*this);}

  USE_FAST_ALLOCATOR;

  static void RegisterToGameState(GameState *gState);
};
