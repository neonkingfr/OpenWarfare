#include "../common.hpp"
#include "../LODObject.h"
#include "../ObjectData.h"
#include <El/elementpch.hpp>
#include <El/Evaluator/express.hpp>
#include <el\preprocc\preproc.h>
#include <el\pathname\pathname.h>
#include <malloc.h>
#include ".\o2scriptclass.h"
#include "gdlodobject.h"
#include "GdSelection.h"
#include "StringsArrays.h"
#include "GdMatrix.h"
#include "dialogs.h"
#include "GdIOStream.h"
#include "GdBiTXTImport.h"
#include "GdBiFXTImport.h"
#include "gdFBXplugin.h"
#include "GdSourceSafe.h"
#include "gdObjToolJoinProxy.h"
#include "GdBIANIMImport.h"
#include <El/MultiThread/Tls.h>
#include <el/Evaluator/Addons/PreprocCmd/PreprocCmd.h>


O2ScriptStream *O2ScriptClass::FindLibrary(const char *filename)
{
  Pathname dir;    
  for (int i=0;i<searchPaths.Size();i++)
  {
    O2ScriptStream *str=new O2ScriptStream();
    dir.SetDirectory(searchPaths[i]);
    Pathname out(filename,dir);
    str->open(out);
    if (!str->fail()) 
    {
      str->restoreScriptName=curScriptName;
      curScriptName=out;
      return str;    
    }
    delete str;
  }
  return NULL;
}


QIStream *O2ScriptClass::OnEnterInclude(const char *filename)
{
  O2ScriptStream *stream=new O2ScriptStream ();
  stream->restoreScriptName=curScriptName;
  curScriptName=Pathname(filename,curScriptName);
  if (curScriptName.IsNull()) curScriptName=filename;
  stream->open(curScriptName);
  if (stream->fail()) 
  {    
    curScriptName=stream->restoreScriptName;
    delete stream;
    stream=FindLibrary(filename);
    return stream;
  }  
  return stream;
}

void O2ScriptClass::OnExitInclude(QIStream *str)
{
  O2ScriptStream *stream=static_cast<O2ScriptStream  *>(str);
  curScriptName=stream->restoreScriptName;  
  delete stream;
}


bool O2ScriptClass::LoadScript(const char *filename)
{
  script.rewind();
  if (Process(&script,filename)==false) return false;
  script.put(0);
  return true;
}

void O2ScriptClass::LoadScriptDirect(const char *_script)
{
  script.rewind();
  script<<_script;
  script.put(0);
}



RString O2ScriptClass::GetPreprocError()
{
  const char *errorStr;
  switch (Preproc::error)
  {
    case prNoError:errorStr="0 OK";break;
    case prStreamOpenError: errorStr="1 #include file not found";break;
    case prIncludeError: errorStr="2 #include command syntax error";break;
    case prIncludeMaxRecursion: errorStr="3 #include maximal recursion reached";break;
    case prDefineError: errorStr="4 #define command syntax error";break;
    case prDefineParamError: errorStr="5 #define parametters syntax error";break;
    case prParseExit: errorStr="6 Parser exitted (internal error)";break;
    case prInvalidPreprocessorCommand: errorStr="7 Unknown preprocessor command";break;
    case prUnexceptedEndOfFile: errorStr="8 Unexcepted end of script (didn't you forget parenthesis?)";break;
    case prToManyParameters: errorStr="9 Too many parameters";break;
    case prToFewParameters: errorStr="10 Too few parameters";break;
    case prUnexceptedSymbol: errorStr="11 Unexcepted symbol";break;
    case prEndIfExcepted: errorStr="12 #EndIf excepted";break;
    default: errorStr=(char *)alloca(100);
      sprintf(const_cast<char *>(errorStr),"13 Unexcepted error (%d)", Preproc::error);
      break;
  }
  const char *mask="Preprocess error: %s at %s line %d";
  int len=strlen(errorStr)+strlen(mask)+strlen(Preproc::filename.Data())+50;
  RString res;
  sprintf(res.CreateBuffer(len),mask,errorStr,Preproc::filename.Data(),Preproc::curline);
  return res;
}

void O2ScriptClass::SetLibraryPaths(const char *libs) //paths is separated by ;
{
  const char *begin=libs;
  const char *it=strchr(begin,';');
  searchPaths.Clear();
  while (it)
  { 
    RString lib(begin,it-begin);
    begin=it+1;
    it=strchr(begin,';');
    if (lib.GetLength()>0) searchPaths.Append()=lib;
  }
  if (begin[0])
  {
    RString lib=begin;
    searchPaths.Append()=lib;
  }
  char buff[MAX_PATH*4];
  GetModuleFileName(NULL,buff,sizeof(buff));
  *(const_cast<char *>(Pathname::GetNameFromPath(buff))-1)=0;
  searchPaths.Append()=buff;
  GetCurrentDirectory(sizeof(buff),buff);
  searchPaths.Append()=buff;
}


#include <DbgHelp.h>

static TLS(GameState) curGameState;
static Pathname dumpPath;

typedef BOOL (WINAPI  *MiniDumpWriteDump_Type)(
  HANDLE hProcess,
  DWORD ProcessId,
  HANDLE hFile,
  MINIDUMP_TYPE DumpType,
  PMINIDUMP_EXCEPTION_INFORMATION ExceptionParam,
  PMINIDUMP_USER_STREAM_INFORMATION UserStreamParam,
  PMINIDUMP_CALLBACK_INFORMATION CallbackParam
);

static MiniDumpWriteDump_Type MiniDumpWriteDumpFn=0;


#ifdef _SCRIPT_DEBUGGER
static LONG ScriptUnhandledExceptionFilter(EXCEPTION_POINTERS* ExceptionInfo)
{    
    MINIDUMP_EXCEPTION_INFORMATION expinfo;    
    expinfo.ClientPointers=FALSE;
    expinfo.ExceptionPointers=ExceptionInfo;
    expinfo.ThreadId=GetCurrentThreadId();
    HANDLE hDumpFile=CreateFile(dumpPath,GENERIC_WRITE,0,NULL,CREATE_ALWAYS,0,NULL);    
    if (hDumpFile && MiniDumpWriteDumpFn)
    {      
      MiniDumpWriteDumpFn(GetCurrentProcess(),GetCurrentProcessId(),hDumpFile,MiniDumpWithFullMemory,&expinfo,NULL,NULL);
      CloseHandle(hDumpFile);
    }
    if (curGameState)
    {
      IScriptDebugger *dbg=curGameState->AttachDebugger(NULL);
      curGameState->AttachDebugger(dbg);
      if (dbg)
      {
        dbg->OnError(*curGameState,"Internal Error: Unhanded Exception");
        ExitProcess(0);
      }
    }
    return EXCEPTION_CONTINUE_SEARCH;
};
#endif

GameValue O2ScriptClass::RunScript(GameState &gState)
{
  gState.BeginContext(this);
  GameValue result;
#ifdef _SCRIPT_DEBUGGER
  if (MiniDumpWriteDumpFn==0)
  {
    HMODULE lib=LoadLibrary("DbgHelp.dll");
    MiniDumpWriteDumpFn=(MiniDumpWriteDump_Type)GetProcAddress(lib,"MiniDumpWriteDump");
  }
  LPTOP_LEVEL_EXCEPTION_FILTER topFilter=SetUnhandledExceptionFilter((LPTOP_LEVEL_EXCEPTION_FILTER)ScriptUnhandledExceptionFilter);
#endif
  dumpPath=curScriptName;
  curGameState=&gState;
  dumpPath.SetFilename("o2Script.mdmp");
  result=gState.EvaluateMultiple(script.str(),false);     
#ifdef _SCRIPT_DEBUGGER
  SetUnhandledExceptionFilter(topFilter);
#endif
  gState.EndContext();
  return result;
}

void O2ScriptClass::PrepareGameState(GameState &gState)
{
    GdLODObject::RegisterToGameState(&gState);
    GdObjectData::RegisterToGameState(&gState);
    GdSelection::RegisterToGameState(&gState);
    GdMatrix::RegisterToGameState(&gState);
    GdStringsArrays::RegisterToGameState(&gState);
    GdDialogs::RegisterToGameState(&gState);
    GdIOStreamV::RegisterToGameState(&gState);
    GdBiTXTImport::RegisterToGameState(&gState);
    GdBiFXTImport::RegisterToGameState(&gState);
    GdSourceSafe::RegisterToGameState(&gState);
    GdObjToolJoinProxy::RegisterToGameState(&gState);
    GdBIANIMImport::RegisterToGameState(&gState);
    GdPreprocCmd::RegisterToGameState(&gState);
	GdFBXplugin::RegisterToGameState(&gState);

#if DOCUMENT_COMREF
    if (!gState.ComRefDocument("P:\\doc\\Objektiv2\\Manuals\\ComRef\\data\\comref.xml"))
      puts("Unable to generate COMREF");
    if (!gState.GenerateUltraeditWordFile("p:\\doc\\WORDFILE.TXT","BIS O2Script","BIO2S INC",true))
      puts("Unable to generate WORDFILE");
    exit(0);

#endif
}