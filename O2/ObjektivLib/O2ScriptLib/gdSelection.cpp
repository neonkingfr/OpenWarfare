#include "..\common.hpp"
#include "..\LODObject.h"
#include "..\ObjToolProxy.h"
#include "..\Selection.h"
#include "GdSelection.h"
#include "..\GlobalFunctions.h"
#include ".\gdlodobject.h"
#include "GdMatrix.h"
#include <malloc.h>

using namespace ObjektivLib;

#define Category "O2ScriptLib::Selection"

DEFINE_FAST_ALLOCATOR(GdSelection)

GdSelection::GdSelection(void)
{

}
GdSelection::GdSelection(SelectionRef *sel, LODObjectRef *owner):_selection(sel),_owner(owner)
{
  
}

GdSelection::GdSelection(const ObjektivLib::Selection *sel, LODObjectRef *owner):_selection(new SelectionRef(*sel,const_cast<ObjektivLib::ObjectData *>(sel->GetOwner()))),_owner(owner)
{
}
GdSelection::GdSelection(const GdSelection &other):_selection(other._selection),_owner(other._owner)
{

}

RString GdSelection::GetText() const
{
  return _name;
}

bool GdSelection::IsEqualTo(const GameData *data) const
{
  const GdSelection *seldata=dynamic_cast<const GdSelection *>(data);
  return (seldata!=NULL && seldata->_selection==_selection);
}

#define GET_SELECTION(oper) (static_cast<GdSelection *>(oper.GetData())->_selection)
#define GET_SELECTIONOBJ(oper) (static_cast<GdSelection *>(oper.GetData()))
#define GET_OBJECTDATA(oper) (static_cast<GdObjectData *>(oper.GetData())->_object)
#define GET_OBJECTDATAOBJ(oper) (static_cast<GdObjectData *>(oper.GetData()))
#define GET_MATRIX(oper) (static_cast<GdMatrix &>(*oper.GetData()))
#define GET_FACE(oper) FaceT(static_cast<GdFaceT *>(oper.GetData())->_object,static_cast<GdFaceT *>(oper.GetData())->_face)

static GameValue setSelectionName(const GameState *gs, GameValuePar oper1, GameValuePar oper2)
{
  GdSelection *sel=GET_SELECTIONOBJ(oper1);
  GdSelection *nw=new GdSelection(*sel);
  nw->_name=oper2;
  return GameValue(nw);
}

static GameValue newSelection(const GameState *gs, GameValuePar oper1)
{
  GdObjectData *obj=GET_OBJECTDATAOBJ(oper1);
  return GameValue(new GdSelection(new SelectionRef(obj->_object),obj->_owner));
}

static GameValue copySelection(const GameState *gs, GameValuePar oper1)
{
  GdSelection *obj=GET_SELECTIONOBJ(oper1);
  return GameValue(new GdSelection(new SelectionRef(*obj->_selection),obj->_owner));
}

static GameValue nameOfSel(const GameState *gs, GameValuePar oper1)
{
  GdSelection *sel=GET_SELECTIONOBJ(oper1);
  return sel->_name;
}

static GameValue saveSel(const GameState *gs, GameValuePar oper1, GameValuePar oper2)
{
  ObjektivLib::ObjectData *obj=GET_OBJECTDATA(oper1);
  GdSelection *sel=GET_SELECTIONOBJ(oper2);
  SelectionRef *rsel=sel->_selection;
  if (sel->_name.GetLength()==0)
  {
    gs->SetError(EvalForeignError,"Unnamed selection cannot be saved");
    return oper2;
  }
  if (sel->_name=="_current")
  {
    Selection *objsel=const_cast<Selection *>(obj->GetSelection());
    *objsel=*(sel->_selection);
  }
  else if (sel->_name=="_hidden")
  {
    Selection *objsel=const_cast<Selection *>(obj->GetHidden());
    *objsel=*(sel->_selection);
  }
  else if (sel->_name=="_locked")
  {
    Selection *objsel=const_cast<Selection *>(obj->GetLocked());
    *objsel=*(sel->_selection);
  }
  else
     obj->SaveNamedSel(sel->_name,sel->_selection);
  return oper2;
}

static GameValue loadSel(const GameState *gs, GameValuePar oper1, GameValuePar oper2)
{
  GdObjectData *obj=GET_OBJECTDATAOBJ(oper1);
  GameStringType name=oper2;
  const Selection *sel;
  if (name=="_current")  
    sel=obj->_object->GetSelection();
  else if (name=="_hidden")
    sel=obj->_object->GetHidden();
  else if (name=="_locked")
    sel=obj->_object->GetLocked();
  else
    sel=obj->_object->GetNamedSel(name);
  if (sel==NULL) return GameValue();
  else return GameValue(new GdSelection(sel,obj->_owner));
}

static GameValue faceSelected(const GameState *gs, GameValuePar oper1, GameValuePar oper2)
{
  SelectionRef *sel=GET_SELECTION(oper1);
  int index=toInt(oper2);
  const ObjektivLib::ObjectData *owner=sel->GetOwner();
  if (index<0 || index>=owner->NFaces())
  {
    gs->SetError(EvalForeignError,"Face index out of range");
    return GameValue();
  }
  return sel->FaceSelected(index);
}

static GameValue pointSelected(const GameState *gs, GameValuePar oper1, GameValuePar oper2)
{
  SelectionRef *sel=GET_SELECTION(oper1);
  sel->Validate();
  int index=toInt(oper2);
  const ObjektivLib::ObjectData *owner=sel->GetOwner();
  if (index<0 || index>=owner->NPoints())
  {
    gs->SetError(EvalForeignError,"Point index out of range");
    return GameValue();
  }
  return sel->PointSelected(index);
}
static GameValue pointWeight(const GameState *gs, GameValuePar oper1, GameValuePar oper2)
{
  SelectionRef *sel=GET_SELECTION(oper1);
  sel->Validate();
  int index=toInt(oper2);
  const ObjektivLib::ObjectData *owner=sel->GetOwner();
  if (index<0 || index>=owner->NPoints())
  {
    gs->SetError(EvalForeignError,"Point index out of range");
    return GameValue();
  }
  return sel->PointWeight(index);
}

static GameValue pointFaceSelOp(int op, const GameState *gs, GameValuePar oper1, GameValuePar oper2)
{
  SelectionRef *sel=GET_SELECTION(oper1);
  sel->Validate();
  int index;
  float weight;
  if (oper2.GetType()==GameArray)
  {
        const GameArrayType &arr=oper2;
        if (arr.Size()<2) 
        {
          gs->SetError(EvalType);
          return GameValue();
        }
        index=toInt(arr[0]);
        weight=arr[1];
  }
  else {index=toInt(oper2);weight=1;}
  const ObjektivLib::ObjectData *owner=sel->GetOwner();
  int max=op>1?owner->NFaces():owner->NPoints();
  if (index<0 || index>=max)
  {
    gs->SetError(EvalForeignError,"Index out of range");    
  }
  else
    switch (op)
    {
      case 0: sel->SetPointWeight(index,weight);break;
      case 1: sel->PointSelect(index,false);break;
      case 2: sel->FaceSelect(index,true);break;
      case 3: sel->FaceSelect(index,false);break;
    }
  return oper1;
}

static GameValue selectFace(const GameState *gs, GameValuePar oper1, GameValuePar oper2)
{
  return pointFaceSelOp(2,gs,oper1,oper2);
}

static GameValue unselectFace(const GameState *gs, GameValuePar oper1, GameValuePar oper2)
{
  return pointFaceSelOp(3,gs,oper1,oper2);
}

static GameValue selectPoint(const GameState *gs, GameValuePar oper1, GameValuePar oper2)
{
  return pointFaceSelOp(0,gs,oper1,oper2);
}

static GameValue unselectPoint(const GameState *gs, GameValuePar oper1, GameValuePar oper2)
{
  return pointFaceSelOp(1,gs,oper1,oper2);
}

static GameValue PointWeightArr(const GameState *gs, GameValuePar oper1, GameValuePar oper2)
{
  GameArrayType arr;
  arr.Append()=oper1;
  arr.Append()=oper2;
  return arr;
}

static GameValue SumSelection(const GameState *gs, GameValuePar oper1, GameValuePar oper2)
{  
  SelectionRef *sel1=GET_SELECTION(oper1);
  SelectionRef *sel2=GET_SELECTION(oper2);
  (*sel1)+=*sel2;  
  return oper1;
}

static GameValue DifSelection(const GameState *gs, GameValuePar oper1, GameValuePar oper2)
{  
  SelectionRef *sel1=GET_SELECTION(oper1);
  SelectionRef *sel2=GET_SELECTION(oper2);
  (*sel1)-=*sel2;  
  return oper1;
}

static GameValue IntersectionSelection(const GameState *gs, GameValuePar oper1, GameValuePar oper2)
{  
  SelectionRef *sel1=GET_SELECTION(oper1);
  SelectionRef *sel2=GET_SELECTION(oper2);  
  (*sel1)&=*sel2;    
  return oper1;
}

static GameValue CompleteSelection(const GameState *gs, GameValuePar oper1, GameValuePar oper2)
{  
  SelectionRef *sel1=GET_SELECTION(oper1);
  SelectionRef *sel2=GET_SELECTION(oper2);  
  (*sel1)|=*sel2;    
  return oper1;
}

static GameValue selectAll(const GameState *gs, GameValuePar oper1)
{
  SelectionRef *sel1=GET_SELECTION(oper1);
  const ObjektivLib::ObjectData *owner=sel1->GetOwner();
  int i;
  for (i=0;i<owner->NFaces();i++) sel1->FaceSelect(i);
  for (i=0;i<owner->NPoints();i++) sel1->PointSelect(i);
  return oper1;
}

static GameValue unselectAll(const GameState *gs, GameValuePar oper1)
{
  SelectionRef *sel1=GET_SELECTION(oper1);
  const ObjektivLib::ObjectData *owner=sel1->GetOwner();
  int i;
  for (i=0;i<owner->NFaces();i++) sel1->FaceSelect(i,false);
  for (i=0;i<owner->NPoints();i++) sel1->PointSelect(i,false);
  return oper1;
}

static GameValue selectPointsFromFaces(const GameState *gs, GameValuePar oper1)
{
  SelectionRef *sel1=GET_SELECTION(oper1);
  sel1->SelectPointsFromFaces();
  return oper1;
}

static GameValue selectFacesFromPoints(const GameState *gs, GameValuePar oper1)
{
  SelectionRef *sel1=GET_SELECTION(oper1);
  sel1->SelectFacesFromPoints();
  return oper1;
}

static GameValue getPointWeights(const GameState *gs, GameValuePar oper1)
{
  SelectionRef *sel1=GET_SELECTION(oper1);
  sel1->Validate();
  GameArrayType arr;
  const ObjektivLib::ObjectData *obj=sel1->GetOwner();
  for (int i=0;i<obj->NPoints();i++) arr.Append()=sel1->PointWeight(i);
  return arr;
}

static GameValue getSelFaces(const GameState *gs, GameValuePar oper1)
{
  SelectionRef *sel1=GET_SELECTION(oper1);
  sel1->Validate();
  GameArrayType arr;
  const ObjektivLib::ObjectData *obj=sel1->GetOwner();
  for (int i=0;i<obj->NFaces();i++) if (sel1->FaceSelected(i))
    arr.Append()=(float)i;
  return arr;
}
static GameValue getSelPoints(const GameState *gs, GameValuePar oper1)
{
  SelectionRef *sel1=GET_SELECTION(oper1);
  sel1->Validate();
  GameArrayType arr;
  const ObjektivLib::ObjectData *obj=sel1->GetOwner();
  for (int i=0;i<obj->NPoints();i++) if (sel1->PointSelected(i))
    arr.Append()=(float)i;
  return arr;
}

static GameValue deleteSelected(const GameState *gs, GameValuePar oper1)
{
  SelectionRef *sel1=GET_SELECTION(oper1);
  ObjektivLib::ObjectData *obj=const_cast<ObjektivLib::ObjectData *>(sel1->GetOwner());
  obj->SelectionDelete(sel1);
  return oper1;
}

static GameValue setPointsWeights(const GameState *gs, GameValuePar oper1, GameValuePar oper2)
{
  SelectionRef *sel1=GET_SELECTION(oper1);
  const GameArrayType &arr=oper2;
  ObjektivLib::ObjectData *obj=const_cast<ObjektivLib::ObjectData *>(sel1->GetOwner());
  int count=__max(obj->NFaces(),arr.Size());
  for (int i=0;i<count;i++) sel1->SetPointWeight(i,arr[i]);
  return oper1;
}

static GameValue selectPoints(const GameState *gs, GameValuePar oper1, GameValuePar oper2)
{
  SelectionRef *sel1=GET_SELECTION(oper1);
  const GameArrayType &arr=oper2;
  int max=sel1->GetOwner()->NPoints();
  for (int i=0;i<arr.Size();i++)
  {
    int index=toInt(arr[i]);
    if (index>=0 && max>index)  sel1->PointSelect(index,true);
  }
  return oper1;
}

static GameValue selectFaces(const GameState *gs, GameValuePar oper1, GameValuePar oper2)
{
  SelectionRef *sel1=GET_SELECTION(oper1);
  const GameArrayType &arr=oper2;
  int max=sel1->GetOwner()->NFaces();
  for (int i=0;i<arr.Size();i++)
  {
    int index=toInt(arr[i]);
    if (index>=0 && max>index)  sel1->FaceSelect(index,true);
  }
  return oper1;
}


static GameValue checkSelectionName(const GameState *gs, GameValuePar oper1, GameValuePar oper2)
{
  ObjektivLib::ObjectData *obj=GET_OBJECTDATA(oper1);
  const GameStringType &name=oper2;
  return GameValue(obj->GetNamedSel(name)!=NULL);
}

static GameValue getSelections(const GameState *gs, GameValuePar oper1)
{
  ObjektivLib::ObjectData *obj=GET_OBJECTDATA(oper1);
  GameArrayType arr;
  for (int i=0;i<MAX_NAMED_SEL;i++)
  {
    NamedSelection *sel=obj->GetNamedSel(i);
    if (sel) 
      arr.Append()=sel->Name();
  }
  return arr;
}
static GameValue deleteSelection(const GameState *gs, GameValuePar oper1,GameValuePar oper2)
{
  ObjektivLib::ObjectData *obj=GET_OBJECTDATA(oper1);
  const GameStringType &name=oper2;  
  return obj->DeleteNamedSel(name);
}

static GameValue mergePoints(const GameState *gs, GameValuePar oper1,GameValuePar oper2)
{
  SelectionRef *sel1=GET_SELECTION(oper1);
  float distance=oper2;
  ObjektivLib::ObjectData *obj=const_cast<ObjektivLib::ObjectData *>(sel1->GetOwner());
  obj->MergePoints(distance,true,sel1);
  return oper1;
}

static GameValue transformSelection(const GameState *gs, GameValuePar oper1,GameValuePar oper2)
{
  SelectionRef *sel1=GET_SELECTION(oper1);
  sel1->Validate();
  GdMatrix &mx=GET_MATRIX(oper2);
  ObjektivLib::ObjectData *obj=const_cast<ObjektivLib::ObjectData *>(sel1->GetOwner());
  for (int i=0;i<obj->NPoints();i++) if (sel1->PointSelected(i))
  {
    PosT &pos=obj->Point(i);
    Vector3 res=mx.FastTransform(pos);    
    pos.SetPoint(pos+(res-pos)*sel1->PointWeight(i));
  }
  return oper1;
}

static GameValue transformFrame(const GameState *gs, GameValuePar oper1,GameValuePar oper2)
{
  SelectionRef *sel1=GET_SELECTION(oper1);
  sel1->Validate();
  const GameArrayType arr=oper2;
  if (arr.Size()<2 || arr[0].GetType()!=GameScalar || arr[1].GetType()!=EvalType_Matrix)
  {
    gs->SetError(EvalType);
    return oper1;
  }
  int index=toInt(arr[0]);
  GdMatrix &mx=GET_MATRIX(arr[1]);
  ObjektivLib::ObjectData *obj=const_cast<ObjektivLib::ObjectData *>(sel1->GetOwner());  
  if (index<0 || index>=obj->NAnimations())
  {
    gs->SetError(EvalForeignError,"Index out of range");
    return oper2;
  }
  AnimationPhase &frame=*obj->GetAnimation(index);
  for (int i=0;i<obj->NPoints();i++) if (sel1->PointSelected(i))
  {
    Vector3 &pos=frame[i];
    Vector3 res=mx.FastTransform(pos);    
    frame[i]=pos+(res-pos)*sel1->PointWeight(i);
  }
  return oper1;
}

static GameValue selCenter(const GameState *gs, GameValuePar oper1)
{
  SelectionRef *sel1=GET_SELECTION(oper1);
  Vector3 vx=sel1->GetCenter();
  GameArrayType arr;
  arr.Append()=vx[0];
  arr.Append()=vx[1];
  arr.Append()=vx[2];
  return arr;
}

static GameValue countFaces(const GameState *gs, GameValuePar oper1)
{
  SelectionRef *sel1=GET_SELECTION(oper1);
  return (float)sel1->NFaces();
}

static GameValue countPoints(const GameState *gs, GameValuePar oper1)
{
  SelectionRef *sel1=GET_SELECTION(oper1);
  return (float)sel1->NPoints();
}

static GameValue proxyFace(const GameState *gs, GameValuePar oper1)
{
  RString name=oper1;
  GdObjectData *obj=GET_OBJECTDATAOBJ(oper1);
  ObjToolProxy &proxy=obj->_object->GetTool<ObjToolProxy>();
  int i=proxy.FindProxyFace(name);
  if (i==-1) return GameValue();
  return GameValue(new GdFaceT(obj->_object,obj->_owner,i));
}

static GameValue proxyMatrix(const GameState *gs, GameValuePar oper1)
{
  FaceT fc=GET_FACE(oper1);
  ObjektivLib::ObjectData *obj=fc.GetObject();
  ObjToolProxy &proxy=obj->GetTool<ObjToolProxy>();
  GdMatrix *mx=new GdMatrix;
  proxy.GetProxyTransform(fc,*mx);  
  return GameValue(mx);
}

static GameValue ownerOfSel(const GameState *gs, GameValuePar oper1)
{
  GdSelection *sel=GET_SELECTIONOBJ(oper1);
  ObjektivLib::ObjectData *obj=const_cast<ObjektivLib::ObjectData *>(sel->_selection->GetOwner());
  return GameValue(new GdObjectData(obj,sel->_owner));
}

#define OPERATORS_DEFAULT(XX, Category) \
  XX(EvalType_Selection, "as",functionFirst, setSelectionName,EvalType_Selection, GameString, "sel","name", "Returns selection with name", "_object save _sel as \"my selection\";", "", "", "", Category) \
  XX(EvalType_Selection, "save", function, saveSel,EvalType_ObjectData, EvalType_Selection, "object","selection", "Saves selection. Selection must have name. If selection with name exists, it is replaced."\
    "There are three special selections named: _current, _hidden, _locked. Use this names to save as current selection, current hidden selection, or current locked selection.", "_object save _sel as \"my selection\";", "", "", "", Category) \
  XX(EvalType_Selection, "loadSelection", function,loadSel, EvalType_ObjectData, GameString, "object","name","Loads selection from object with name. If selection doesn't exists, returns nil."\
    "There are three special selections named: _current, _hidden, _locked. Use this names to select current selection, current hidden selection, or current locked selection.","_sel=_object loadSelection \"Component1\";","","","",Category) \
  XX(GameBool, "isFaceSelected", function,faceSelected, EvalType_Selection, GameScalar, "sel","index","Returns true, if face is selected. If index is out of range, reports error","","","","",Category) \
  XX(GameBool, "isPointSelected", function,pointSelected, EvalType_Selection, GameScalar, "sel","index","Returns true, if point is selected. If index is out of range, reports error. Points with weight>0 are selected","","","","",Category) \
  XX(GameBool, "weight", function,pointWeight, EvalType_Selection, GameScalar, "sel","index","Returns weight of point. If index is out of range, reports error","","","","",Category) \
  XX(EvalType_Selection, "selectFace", function,selectFace, EvalType_Selection, GameScalar, "sel","index","Selects face","_sel selectFace _index","","","",Category) \
  XX(EvalType_Selection, "unselectFace", function,unselectFace, EvalType_Selection, GameScalar, "sel","index","Unselects face","_sel unselectFace _index","","","",Category) \
  XX(EvalType_Selection, "selectPoint", function,selectPoint, EvalType_Selection, GameScalar, "sel","index","Selects point","_sel selectPoint _index","","","",Category) \
  XX(EvalType_Selection, "selectPoint", function,selectPoint, EvalType_Selection, GameArray, "sel","index weight value","Select point and sets point's weight","_sel selectPoint _index weight _value","","","",Category) \
  XX(EvalType_Selection, "unselectPoint", function,unselectPoint, EvalType_Selection, GameScalar, "sel","index","Selects point","_sel unselectPoint _index","","","",Category) \
  XX(GameArray, "weight", functionFirst,PointWeightArr, GameScalar, GameScalar, "index","weight","See select point","","","","",Category) \
  XX(EvalType_Selection, "+=", mocnina,SumSelection, EvalType_Selection, EvalType_Selection, "a","b","Adds b to a. Selection 'a' is changed","","","","",Category) \
  XX(EvalType_Selection, "-=", mocnina,DifSelection, EvalType_Selection, EvalType_Selection, "a","b","Removes b from a. Selection 'a' is changed","","","","",Category) \
  XX(EvalType_Selection, "|=", mocnina,CompleteSelection, EvalType_Selection, EvalType_Selection, "a","b","Create union of selection 'a' and selection 'b'. Result is stored into selection 'a'. There is little difference between |= and +=. |= takes maximum from two weights, += takes suma of two weights.","","","","",Category) \
  XX(EvalType_Selection, "&=", mocnina,IntersectionSelection, EvalType_Selection, EvalType_Selection, "a","b","Create intersection of selection 'a' and selection 'b'. Result is stored into selection 'a'.","","","","",Category) \
  XX(EvalType_Selection, "setPointsWeights", function,setPointsWeights, EvalType_Selection, GameArray, "sel","weights","Copies weights in array into selection from index 0","","","","",Category) \
  XX(EvalType_Selection, "selectPoints", function,selectPoints, EvalType_Selection, GameArray, "sel","indices","Select points specified by indices in array","","","","",Category) \
  XX(EvalType_Selection, "selectFaces", function,selectFaces, EvalType_Selection, GameArray, "sel","indices","Select faces specified by indices in array","","","","",Category) \
  XX(GameBool,"checkSelectionName",function,checkSelectionName,EvalType_ObjectData,GameString,"object","name","Checks that selection 'name' exists in 'object'","_result=_object checkSelectionName \"machine gun\";","","","",Category)\
  XX(GameBool,"deleteSelection",function,deleteSelection,EvalType_ObjectData,GameString,"object","name","Deletes selection with name in object. It does'n affect selections in variables","_result=_object deleteSelection \"machine gun\";","","","",Category)\
  XX(EvalType_Selection,"transform",function,transformSelection,EvalType_Selection,EvalType_Matrix,"sel","mx","Transforms selection by matrix","_selection transform _matrix;","","","",Category)\
  XX(EvalType_Selection,"transformFrame",function,transformFrame,EvalType_Selection,GameArray,"sel","[frame,mx]","Transforms selection by matrix. Transform is applied to the specified. frame is Number, mx is Matrix","_selection transformFrame [2,_matrix];","","","",Category)\
  XX(EvalType_Selection,"mergePoints",function,mergePoints,EvalType_Selection,GameScalar,"sel","distance","Merges points in selection. Parameter distance specified distance between points to merge. Specify some BIG value to merge all points in selection into one","_selection mergePoints 0.0001;","","","",Category)\




#define FUNCTIONS_DEFAULT(XX, Category) \
  XX(EvalType_Selection, "newSelection", newSelection, EvalType_ObjectData, "object", "Creates selection at object", "_newsel=newSelection _object", "", "", "", Category) \
  XX(EvalType_Selection, "copy", copySelection, EvalType_ObjectData, "sel", "Creates copy of selection", "_newsel=copy _sel", "", "", "", Category) \
  XX(EvalType_Selection, "+", copySelection, EvalType_ObjectData, "sel", "Same as copy", "_newsel= +_sel", "", "", "", Category) \
  XX(GameString, "nameOf", nameOfSel, EvalType_Selection, "sel", "Returns name of selection.", "_name=nameof _sel", "", "", "", Category) \
  XX(EvalType_Selection, "selectAll", selectAll, EvalType_Selection, "sel", "Select everything in selection.", "selectAll _sel", "", "", "", Category) \
  XX(EvalType_Selection, "unselectAll", unselectAll, EvalType_Selection, "sel", "Unselect all in selection.", "selectAll _sel", "", "", "", Category) \
  XX(EvalType_Selection, "selectPointsFromFaces", selectPointsFromFaces, EvalType_Selection, "sel", "Select points from selected faces.", "selectPointFromFaces _sel", "", "", "", Category) \
  XX(EvalType_Selection, "selectFacesFromPoints", selectFacesFromPoints, EvalType_Selection, "sel", "Select points from selected faces.", "selectPointFromFaces _sel", "", "", "", Category) \
  XX(GameArray, "getPointWeights", getPointWeights, EvalType_Selection, "sel", "Creates array with contain weight for each point by index. NOTE: not all values may be assigned to valid points. If index is unused, weight is set to zero", "_weights=getPointWeights _sel", "", "", "", Category) \
  XX(GameArray, "getSelectedFaces", getSelFaces, EvalType_Selection, "sel", "Creates array with contain indicies with selected faces", "_arr=getSelectedFaces _sel", "", "", "", Category) \
  XX(GameArray, "getSelectedPoints", getSelPoints, EvalType_Selection, "sel", "Creates array with contain indicies with selected points", "_arr=getSelectedPoints _sel", "", "", "", Category) \
  XX(EvalType_Selection, "deleteSelected", deleteSelected, EvalType_Selection, "sel", "Deletes points and faces on source model", "_arr=getSelectedPoints _sel", "", "", "", Category) \
  XX(GameArray,"getSelections",getSelections,EvalType_ObjectData,"object","Returns array of selections exists in object'","{..code...} forEach getSelections _object;","","","",Category)\
  XX(GameArray,"centerOf",selCenter,EvalType_Selection,"sel","Calculates center of selection","","","","",Category)\
  XX(GameScalar,"countFaces",countFaces,EvalType_Selection,"sel","Returns count of selected faces","","","","",Category)\
  XX(GameScalar,"countPoints",countPoints,EvalType_Selection,"sel","Returns count of selected points","","","","",Category)\
  XX(EvalType_FaceT,"getProxyFace",proxyFace,EvalType_Selection, "selection","Finds proxy face of proxy selection","","","","",Category) \
  XX(EvalType_Matrix,"getProxyMatrix",proxyMatrix,EvalType_FaceT, "face","Calculates proxy transformation","","","","",Category) \
  XX(EvalType_ObjectData,"ownerOf",ownerOfSel,EvalType_Selection, "sel","Returns object that owns (created) this selection","","","","",Category) \


static GameOperator BinaryFuncts[]=
{
  OPERATORS_DEFAULT(REGISTER_OPERATOR, Category)
};

static GameFunction UnaryFuncts[]=
{
  FUNCTIONS_DEFAULT(REGISTER_FUNCTION, Category)
};






static GameData *CreateSelection(ParamArchive *ar) {return new GdSelection;}

TYPES_SELECTION(DEFINE_TYPE, Category)




#if DOCUMENT_COMREF
static ComRefFunc DefaultComRefFunc[] =
{
//  NULARS_DEFAULT(COMREF_NULAR, Category)
  FUNCTIONS_DEFAULT(COMREF_FUNCTION, Category)
  OPERATORS_DEFAULT(COMREF_OPERATOR, Category)
};

static ComRefType DefaultComRefType[] =
{
  TYPES_DEFAULT(COMREF_TYPE, Category)
};
#endif


void GdSelection::RegisterToGameState(GameState *gState)
{
  GameState &state = *gState; // helper to make macro works
  TYPES_SELECTION(REGISTER_TYPE, Category)

  gState->NewOperators(BinaryFuncts,lenof(BinaryFuncts));
  gState->NewFunctions(UnaryFuncts,lenof(UnaryFuncts));
//  gState->NewNularOps(NularyFuncts,lenof(NularyFuncts));
#if DOCUMENT_COMREF
  gState->AddComRefFunctions(DefaultComRefFunc,lenof(DefaultComRefFunc));
  gState->AddComRefTypes(DefaultComRefType,lenof(DefaultComRefType));
#endif
}
