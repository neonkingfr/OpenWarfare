#include "..\common.hpp"
#include "..\LODObject.h"
#include "gdLODObject.h"
#include "gdbifxtimport.h"
#include "..\BiFXTImport.h"


#define Category "O2Scripts::ImportBiFXT"

static GameValue importBiFXT(const GameState *gs, GameValuePar oper1,GameValuePar oper2)
{
  Ref<LODObjectRef>lodobj=static_cast<GdLODObject *>(oper1.GetData())->GetObject();
   
  return GameValue(ObjektivLib::CBiFXTImport::Import((RString)oper2,lodobj->_ref));
};


#define OPERATORS_DEFAULT(XX, Category) \
  XX(GameBool,"importBiFXT", function, importBiFXT, EvalType_LODObject, GameString,"p3d","name", "Loads (imports) BiFXT file into LODObject. It doesn't change internal name of object.", "p3d=newLODObject;result=p3d importBiFXT \"shape.fxt\";", "true, if file has been successfully parsed and imported. false, if there was an error.", "", "", Category)


static GameOperator BinaryFuncts[]=
{
  OPERATORS_DEFAULT(REGISTER_OPERATOR, Category)
};

#if DOCUMENT_COMREF
static ComRefFunc DefaultComRefFunc[] =
{
  OPERATORS_DEFAULT(COMREF_OPERATOR, Category)    
};
#endif

void GdBiFXTImport::RegisterToGameState(GameState *gState)
{
  gState->NewOperators(BinaryFuncts,lenof(BinaryFuncts)); 
#if DOCUMENT_COMREF
  gState->AddComRefFunctions(DefaultComRefFunc,lenof(DefaultComRefFunc));
#endif
}

