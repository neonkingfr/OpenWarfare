#ifndef _O2SCRIPTCLASS_HEADER_6760654981AC55A_BREDY_
#define _O2SCRIPTCLASS_HEADER_6760654981AC55A_BREDY_
#include <el\preprocc\preproc.h>
#include <el\pathname\pathname.h>




class O2ScriptStream: public QIFStream
{
public:
  Pathname restoreScriptName;
};


class O2ScriptClass: public GameVarSpace, private Preproc
{
  QOStrStream script;


public:
  //! Constructor
  O2ScriptClass() : GameVarSpace(false) {}

  Pathname curScriptName;
  AutoArray<RString> searchPaths;


  virtual QIStream *OnEnterInclude(const char *filename);
  virtual void OnExitInclude(QIStream *stream);
  O2ScriptStream *FindLibrary(const char *filename);

  static void PrepareGameState(GameState &gState);
  bool LoadScript(const char *filename);
  void LoadScriptDirect(const char *script);

  RString GetPreprocError();
  PreprocessError GetPreprocErrorCode() {return error;}
  void SetLibraryPaths(const char *libs); //paths is separated by ;

  GameValue RunScript(GameState &gState);


};



#endif